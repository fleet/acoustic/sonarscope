:imagesdir: ../../images/Pames-Images/
:sectnums:
:toc: macro
:toc-title: Table of contents
:author: Jean-Marie Augustin
:email: sonarscope@ifremer.fr
:revnumber: 1.0
:revdate: Created 2017/04/05, Updated 2017/04/05
:revremark: Not reviewed

image:../../SscHelp-Images/Bandeau_entete.JPG[]

// Author : {author} {email}
// Created : {revnumber}, updated : {revremark}

[IMPORTANT]
====
If you have any remark, suggestion, correction, etc, concerning this document, please email to {author} {email}
====

toc::[]

// ------------------------------------------

= References =

[1] Urick R.,"Principles of Underwater Sound", McGraw-Hill (1983)

[2] Burdic W.S., _Underwater Sound System Analysis,_ Prentice-Hall
(1984)

[3] Lurton X. "Eléments d'analyse des systèmes acoustiques sous-marins",
internal document IFREMER (1998)

[4] Lurton X., "Précision de mesure de bathymétrie par interférométrie -
Comparaison sonar latéral- sondeur multifaisceaux", rapport IFREMER
DITI/SM/ASM/Br-98/116 (1998)

[5] Pohner F., "Model for computationation of uncertainty in multibeam
depth soundings", Doc. Simrad 93.09.09/FP (1993)

[6] Hammerstad E., "Multibeam echosounder technical note - Performance
effects of multibeam beamwidth", doc.Simrad (1994)

[7] Hare R. & Godin A. & Mayer L., "Accuracy estimation of Canadian
Swath (multibeam) and Sweep (multitransducer) Sounding Systems "
Canadian Hydrographic Service Internal Report (1995)

[8] Allenou J.P. "Bulles d'air et transducteurs de coque", rapport
interne GENAVIR SA-86-09 (1986)

[9] Carpentier M., _Radar - Bases modernes_, Masson (1984)

[10] Augustin J.M., "Etude théorique de l?influence de l?attitude and de
la stabilité du poisson support d?un sonar latéral sur l?imagery",
Rapport IFREMER DITI/DSI/DEt-ASV/JMA/91-031 (1994)

[11] Arzéliès P. "Acoustic Equipment on AUVs: vector's navigation
requirements", rapport IFREMER DITI/SM/93/124

[12] Hughes-Clarke J. "
file:///I:/IfremerToolbox/ifremer/acoustics/pames/@cli_pames/index/_filter6.html[Are
you really getting full bottom coverage?] ", Ocean Mapping Group,
University of New Brunswick (1997)

[13] IHO standards for Hydrographic surveys - 4th edition - Special
Publication no 44 - International Hydrographic Bureau , Monaco (1998)

[14] Lurton X. " Acoustique sous-marine: présentation et applications ",
file:///I:/IfremerToolbox/ifremer/acoustics/pames/@cli_pames/index/_filter9.html[Editions
IFREMER] (1998)

[15] AUGUSTIN J.M. & LURTON X., "Maximum swath width obtainable with
multibeam echosounders - Comparison and discussion of experiments vs
predictions", IEEE Oceans'98 Conference proceedings, pp 234-238 (1998) 

'''

*_Go back to :_*

<<Top,Top of the page>>

link:index.html[PAMES Help]

link:../SScHelp-ADOC/Accueil.html[SonarScope Help]
