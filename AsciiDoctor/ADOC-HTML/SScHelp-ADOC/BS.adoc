:imagesdir: ../../images/SScHelp-Images/
:sectnums:
:toc: macro
:toc-title: Table of contents
:author: Jean-Marie Augustin
:email: sonarscope@ifremer.fr
:revnumber: 1.0
:revdate: Created 2017/04/05, Updated 2017/04/05
:revremark: Not reviewed

image:Bandeau_entete.jpg[]

// Author : {author} {email}
// Created : {revnumber}, updated : {revremark}


[IMPORTANT]
====
If you have any remark, suggestion, correction, etc, concerning this document, please email to {email}
====

toc::[]

// ------------------------------------------

= Backscatter

== Calibration : program mode - Backscatter ==

This menu deals with backscatter estimation and how to compensate from it.

The backscattering strength , seafloor dependent, also strongly depends on the signal frequency and moreover on the incident angle _&theta(t)_ on the seafloor.

|====
|__Backscatter__ *menu items*|__Backscatter__ *menu overview*

a|*Levels*

* <<Curves, Curves>>
** Belle image+
** Full compensation

* <<Belle Image,Image>>

** Belle Image

*** Lambert

*** Lurton

** Full compensation

** Analytical

** Lookup table

*Compensation*

* None

* <<Belle Image,Belle Image>>

** Lambert

** Lurton

* <<Full compensation, Full compensation>>

** Analytical

** Lookup table


|image:8/8b/Sonarproc_calibration_BS_menuOverview.jpg[]
|====
=== Levels ===

TODO : Lier ce menu au menu backscatter

==== Curves ====

==== Belle Image ====

==== Full compensation ====

==== Image ====

==== Belle Image ====

Lambert

Lurton

==== Full compensation ====

Analytical

Lookup table

=== Compensation ===

* None

* Belle Image

** Lambert

** Lurton

* Full compensation

** Analytical

** Lookup table

'''

*_Go back to :_*

<<Top,Top of the page>>

link:Accueil.html[SonarScope Help]
