:imagesdir: ../../images/SScHelp-Images/
:sectnums:
:toc: macro
:toc-title: Table of contents
:author: Jean-Marie Augustin
:email: sonarscope@ifremer.fr
:revnumber: 1.0
:revdate: Created 2017/04/05, Updated 2017/04/05
:revremark: Not reviewed

image:Bandeau_entete.jpg[]

// Author : {author} {email}
// Created : {revnumber}, updated : {revremark}


[IMPORTANT]
====
If you have any remark, suggestion, correction, etc, concerning this document, please email to {email}
====

toc::[]

// ------------------------------------------

= Backscatter modelization tool

== Introduction ==

This module proposed in SonarScope offers many functionalities accessible through the graphic interface.

image:4/46/Sonarproc_calibrationTool1.jpg[]

Those functionalities are gathered into two groups:

* functions linked to the optimisation process (data and model supplied, selected algorithm, optimisation options, results, ...)

* functions attached to the data management (savings, manual modifications of model parameters, ...) and diagrams (zoom, results displaying, ...)

Beside data and models supplied by the user (hence defined externally), most of the parameters can be managed under the various menus which make up the interface. To facilitate the handle of the latter, tooltips are attached to each buttons.

== Algorithm menu ==

The "Algorithm" menu, located on top of the interface, makes it possible to modify every parameter linked to the optimisation process.

image:8/87/Sonarproc_calibrationTool2.jpg[]

Among those, one will note :

* Model choice :

image:f/f7/Sonarproc_calibrationTool3.jpg[]

* Algorithm choice :

image:6/6b/Sonarproc_calibrationTool4.jpg[]

* Simulated annealing :

image:7/70/Sonarproc_calibrationTool5.jpg[]

* Dependence between parameters :

image:b/b6/Sonarproc_calibrationTool6.jpg[]

* Parameter weighting :

image:b/bc/Sonarproc_calibrationTool7.jpg[]

* Iterations number :

image:b/b2/Sonarproc_calibrationTool8.jpg[]

Moreover, the algorithm can only work on a certain range of data supplied if necessary :

image:0/0d/Sonarproc_calibrationTool9.jpg[]

image:7/79/Calibr6.jpg[]

One will notice that the interval on which the algorithm is running is wrapped by two vertical black lines. The area of data removed by the algorithm is shaded with light grey. The optimization curve resulting from the process is only valid for the work area (hence the higher interpolation error inside the rejected part).

== Memory menu ==

For the good management of results, a memory system has been developed :

image:6/65/Sonarproc_calibrationTool10.jpg[]

In addition to a manual memorisation (and suppression) of standard models, an automatic system permits to save the best result (in term of average quadratic error) for each model supplied by the user.

Functions related to this menu are described in the following menu :
|====
|Buttons|Action|Description

|image:e/e2/Sonarproc_calibrationTool11.jpg[]|image:5/55/Left-click.jpg[]|Save current backscatter curve

|image:2/2b/Sonarproc_calibrationTool12.jpg[]|image:6/61/Right-click.jpg[]|Load one of the previously saved backscatter curve

|image:b/b7/Sonarproc_calibrationTool13.jpg[]|image:6/61/Right-click.jpg[]|Delete one of the previously saved backscatter curve

|image:8/89/Sonarproc_calibrationTool14.jpg[]|image:5/55/Left-click.jpg[]| Set model's parameters randomly
|====
Finally, it is possible to display the average quadratic error between data and curves stemmed from the optimisation of each of the model into their standard configuration.

== Parameters management ==

Before being able to launch the optimisation process, the interval on which each parameter can evolve has to be defined by the user.

Although they are supplied at the GUI launching, processing limits can be modified manually during the execution.

Parameters values can as well be edited and freezed (in that case the algorithm cannot modify them, even by pressing "random"), through the following panel :

image:e/e8/Sonarproc_calibrationTool15.jpg[]

Parameter setting is done through the graphical component described below :

image:e/e2/SWW_overview_parameterTuning.jpg[]

* *State* : paremeter can either be editable (green check mark) or not (red cross). Pushing this _state_ button will turn parameter edition on or off .

* *Parameter name :* this button features parameter name. The associated callback, when activated, displays help related to the parameter.

* *Min* and *Max values :* parameter's minimum and maximum values are editable in edition mode.

* *Slider :* parameter value is set depending on cursor's position (between user-defined min and max value)

* *Current value :* value is updated depending on slider movement, and can also be directly typed in by user. 

[NOTE]
====

Value cannot be smaller (resp. greater) than min (resp. max) value. However, the user can manually define min and max value in edition mode.
====
If necessary, a parameters random choice of standard model is possible using the "Random" button.

== Visualisation ==

The left part of the main window gathers the visualisation tools.

The upper part features visualisation settings :

image:9/91/Sonarproc_calibrationTool16.jpg[]

The user can :

* adjust abscissa limits of lower graphics (_Xmin_, _Xmax_), working jointly with the zoom function,

* display each curve used to modelize backscatter (_details_ checked) or only overall modelized backscatter curve (_d*'etails_ unchecked),

* clean up averaged backscatter curve (_clean _button) using the link:SWW_Curves_Interactions.html#Edition[curve edition tool],

* reset curve display (_reset_ button)

The lower part features two graphics :

* the upper one bringing together data and curves display (with or without component) stemmed from employed optimisation models

* the lower one featuring weights used by the error function.

Moreover, a zoom functionality has been integrated :

image:5/55/Left-click.jpg[] : zoom in

image:6/61/Right-click.jpg[] : zoom out

image:d/d6/Calibr12.jpg[]

image:4/4c/Calibr11.jpg[]

== Models ==

=== Lurton model ===

The Lurton model is designed to heuristically model the angular reflectivity according to three components : specular, transitory and lambert.

image:0/0b/Calibr16.jpg[]

Example of the Lurton model fitting

Each curve is defined by a couple of parameters :
|====
|Parameter name|Unity

|Specular level|image:9/94/Calibr17.gif[]

|Specular half-width|degree  

|Transitory level|image:3/31/Calibr18.gif[]

|Transitory half-width|degree  
|Lambert reference|image:2/24/Calibr19.gif[]
|Lambert decreasing|none  
|====
This model has the advantage of being less complex that most of encounter models because its parameters are in direct relation with supplied data. However it does not provide an explicit relation with the physical properties of studied media.

=== Jackson model ===

The Jackson model is widely used nowadays into the sediment backscattering modelling. It is based on several models, each one defined for given angular interval, and linked by interpolation functions.

image:7/71/Calibr20.jpg[]

Example of configuration of Jackson model

The input parameters are as follows :
|====
|Parameter name|Description|Unity 

|Frequency|Frequency of the transmitted signal|image:4/4e/Calibr21.gif[]
|Seawater velocity|The sound speed in water|image:2/2a/Calibr22.gif[]
|Density ratio|Ratio of the sediment and of water|none |Speed ratio|Sound speed ratio between sediment and water|none  
|Attenuation|The wave attenuation inside the sediment|image:2/2e/Calibr23.gif[]
|Spectral strength|Spectral strength of the seafloor relief|image:3/34/Calibr24.gif[]
|Spectral exponent|Spectral exponent  of the seafloor relief|none
|Volume parameter|Ratio of the volume spreading parameter to the sediment attenuation coefficient|image:8/80/Calibr25.gif[]
|====
The fundamental difference lies into the fact that these parameters are linked to the physic properties of the studied areas, which may be interesting for some applications. However, it is not simple to link such parameters to experimental data ; also the physical model used in the Jackson model is quite limitative and may lack to represent many current seafloor configurations.


'''

*_Go back to :_*

<<Top,Top of the page>>

link:Accueil.html[SonarScope Help]
