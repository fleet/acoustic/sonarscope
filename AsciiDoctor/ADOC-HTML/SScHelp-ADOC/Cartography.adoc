:imagesdir: ../../images/SScHelp-Images/
:sectnums:
:toc: macro
:toc-title: Table of contents
:author: Jean-Marie Augustin
:email: sonarscope@ifremer.fr
:revnumber: 1.0
:revdate: Created 2017/04/05, Updated 2017/04/05
:revremark: Not reviewed

image:Bandeau_entete.jpg[]

// Author : {author} {email}
// Created : {revnumber}, updated : {revremark}

[IMPORTANT]
====
If you have any remark, suggestion, correction, etc, concerning this document, please email to {email}
====

toc::[]

// ------------------------------------------

= Cartography =

This menu provides user various functionalities meant to rotate current image.
|====
| _Cartography_ *menu overview*|_Cartography_ *menu items*

|image:0/09/Gnrlproc_cartography_menuOverview.jpg[]
a|
* <<GeoYX to LatLong>>

* <<LatLong to GeoYX>>

* <<Etopo Extraction>>

* <<Include into Etopo>>
|====

== GeoYX to LatLong ==

Transforms projected coordinates of current image into geographic coordinates, provided its geometry is of type GeoXY and its cartographic parameters are known or have been previously defined.

Let's take an example with demo data _EM3000_Ex2.mnt_ :

_EM3000_Ex2.mnt_

image:3/3b/Gnrlproc_cartography_geoXY2LatLong.jpg[]

_EM3000_Ex2.mnt_:geographic coordinates

image:f/f1/Gnrlproc_cartography_geoXY2LatLong_2.jpg[]

<<Top,Top of the page>>

== LatLong to GeoYX ==

Projects current image, provided its geometry is of type LatLong, according to a user-defined projection.

First, the user must input a grid step through the following dialog box :

image:e/e8/Gnrlproc_cartography_latlong2geoXY.jpg[]

Then, the user should manually define cartographic parameters, or use a cartographic parameters definition file (_*.def_) :

image:9/9b/Gnrlproc_cartography_latlong2geoXY_1.jpg[]

In case of a manual definition, the user must choose an ellipsoid to use among the available one:

image:f/f3/Gnrlproc_cartography_latlong2geoXY_2.jpg[]

And choose the projection among those displayed below :

image:f/f3/Gnrlproc_cartography_latlong2geoXY_3.jpg[]

Resulting image will be named "originalImageName* _GeoXY*".

Let's take an example with demo data _PRISMED125_LatLong_SunColor.ers_, projecting using Lambert European projection :

_PRISMED125_LatLong_SunColor.ers:_

image:4/4d/Gnrlproc_cartography_latlong2geoXY_5.jpg[]

_PRISMED125_GeoXY_SunColor.ers_: WGS84, Lambert, Europe

image:f/f8/Gnrlproc_cartography_latlong2geoXY_4.jpg[]

<<Top,Top of the page>>

== Etopo Extraction ==

Extracts ETOPO data corresponding to current extent of displayed image, provided its geometry is of type LatLong, and displays it within the SonarScope work window.

The user must choose the Etopo data resolution he wants to extract through the following list box :

image:5/5e/Gnrlproc_cartography_etopoExtract81.jpg[]

Resulting image will be named "ExtractionEtopo(m)_LatLong_Bathymetry".

[NOTE]
====
Etopo values present a geographic offset : do not use these data for scientific purpose.
==== 

Let's take an example with demo data _EM3000_Ex2.mnt_ :

_EM3000_Ex2.mnt_:

Corresponding Etopo data (1"x1")

image:9/9e/Gnrlproc_cartography_etopoExtract_0.jpg[]

image:f/fe/Gnrlproc_cartography_etopoExtract.jpg[]

<<Top,Top of the page>>

== Include into Etopo ==

Inserts current image data into user's local Etopo database (../SonarScopeData/Etopo).

Next link:Etopo_Extraction,[etopo extraction] over this area will feature the user's image data.

Let's take an example with demo data _GolfeDuLion_LatLong_Bathymetry.ers_ :

_GolfeDuLion_LatLong_Bathymetry.ers_ :

image:c/c5/Gnrlproc_cartography_etopoIncl1.jpg[]

Etopo extraction after insertion of _GolfeDuLion_LatLong_Bathymetry.ers_

image:c/c5/Gnrlproc_cartography_etopoIncl2.jpg[]

'''

*_Go back to :_*

<<Top,Top of the page>>

link:General_processings.html[General processings Help]

link:Accueil.html[SonarScope Help]
