:imagesdir: ../../images/SScHelp-Images/
:sectnums:
:toc: macro
:toc-title: Table of contents
:author: Jean-Marie Augustin
:email: sonarscope@ifremer.fr
:revnumber: 1.0
:revdate: Created 2017/04/05, Updated 2017/04/05
:revremark: Not reviewed

image:Bandeau_entete.jpg[]

// Author : {author} {email}
// Created : {revnumber}, updated : {revremark}


[IMPORTANT]
====
If you have any remark, suggestion, correction, etc, concerning this document, please email to {email}
====

toc::[]

// ------------------------------------------

= Complete this image with another one =

Menu : Arithmetic functions : Merge Images

Fills current image void areas (i.e. that contains NaNs) with another image, to be selected among available images.

Let's take an example with demo data _earth_LatLong_bathymetry.ers_ :

* Creation of a mask to remove europe and north african bathymetry (see link:Region_of_interest.html[regions of interest])

* Creation of an inverse earth bathymetry (see link:Multiply.html[Multiply by a scalar])

* Insertion of inverse earth bathymetry into earth bathymetry.
|====
|_earth_LatLong_bathymetry.ers_|Mask :

|image:e/e0/Gnrlproc_arithmetic_complete1.jpg[]|image:3/37/Gnrlproc_arithmetic_complete2.jpg[]
|====
|====
|earth masked : part 1 removed|- earth bathymetry :

|image:8/8c/Gnrlproc_arithmetic_complete3.jpg[]|image:0/04/Gnrlproc_arithmetic_complete5.jpg[]
|====
|====
|Insertion of -earth bathymetry in void area of earth bathymetry:

|image:4/45/Gnrlproc_arithmetic_complete4.jpg[]
|====

'''

*_Go back to :_*

<<Top,Top of the page>>

link:Accueil.html[SonarScope Help]
