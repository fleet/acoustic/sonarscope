:imagesdir: ../../images/SScHelp-Images/
:sectnums:
:toc: macro
:toc-title: Table of contents
:author: Jean-Marie Augustin
:email: sonarscope@ifremer.fr
:revnumber: 1.0
:revdate: Created 2017/04/05, Updated 2017/04/05
:revremark: Not reviewed

image:Bandeau_entete.jpg[]

// Author : {author} {email}
// Created : {revnumber}, updated : {revremark}


[IMPORTANT]
====
If you have any remark, suggestion, correction, etc, concerning this document, please email to {email}
====

toc::[]

// ------------------------------------------

= Correlation =

This menu gathers self- and cross-correlation routines :
|====
_Correlation_ menu overview*|_Correlation_ menu items*

|image:6/6b/Gnrlproc_correlationMenuOverview.jpg[]|<<Self-correlation,Self-correlation>>

<<Cross-correlation,Cross-correlation>>
|====

== Self-correlation ==

Computes the http://en.wikipedia.org/wiki/Autocorrelation[self-correlation] of the current image.

Resulting image is named : "originalImageName_ *Correlation*".

Let's take an example with demo data file _TextureSonar01.png_: 
|====
|_TextureSonar01.png_|_TextureSonar01.png_: *self-correlated*

|image:8/8c/Gnrlproc_fourier_textureSonar.jpg[]|image:c/cc/Gnrlproc_correlationSelf.jpg[]
|====

We can notice, on the right thumbnail above, that there is clearly a periodicity along the horizontal direction, while there is not along vertical direction.

Zooming on the first ripple and clicking on it gives the following results :

* Location of the point

* Wavelength (lambda)

* Direction of propagation

* Azimuth of the propagation direction

image:b/b4/Gnrlproc_correlationSelf2.jpg[]

image:8/8f/Gnrlproc_correlationSelf3.jpg[]

In this particular case, the wavelength of sand ripples is 12.076891 m.

<<Top,Top of the page>>

== Cross-correlation ==

Computes the http://en.wikipedia.org/wiki/Cross-correlation[cross-correlation] of current image with another user-defined one.

Let's take an example with two versions of demo data file _TextureSonar01.png_: the original one and a shifted one (offset to be determined). 
|====
|_TextureSonar01.png_|

|image:8/8c/Gnrlproc_fourier_textureSonar.jpg[]|image:7/7d/Gnrlproc_correlationCross1.jpg[]

|cross-correlation : maximum at x ="596" height= "37"|cross-correlation : maximum at x = 5 
|====

Offset between those two images

image:8/82/Gnrlproc_correlationCross2.jpg[]

'''

*_Go back to :_*

<<Top,Top of the page>>

link:General_processings.html[General processings Help]

link:Accueil.html[SonarScope Help]
