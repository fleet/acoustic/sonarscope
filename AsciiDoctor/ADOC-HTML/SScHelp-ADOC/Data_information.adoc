:imagesdir: ../../images/SScHelp-Images/
:sectnums:
:toc: macro
:toc-title: Table of contents
:author: Jean-Marie Augustin
:email: sonarscope@ifremer.fr
:revnumber: 1.0
:revdate: Created 2017/04/05, Updated 2017/04/05
:revremark: Not reviewed

image:Bandeau_entete.jpg[]

// Author : {author} {email}
// Created : {revnumber}, updated : {revremark}


[IMPORTANT]
====
If you have any remark, suggestion, correction, etc, concerning this document, please email to {email}
====

toc::[]

// ------------------------------------------

=  Data information =

You can access a variety of information about images loaded, sonar characteristics, or SonarScope through both the following button and menu item:
|====

|*Button*|*SWW Menu item*

|image:a/a3/Data_m3.jpg[]|image:3/32/Data_M3_m.jpg[] 
|====

Right-clicking on the button or choosing the *info* item in the SWW menu gives access to the following menu:  (click on the menu items to reach corresponding help) 
|====
|*Info Menu*|*Type of Information*

|image:b/ba/Data_info_menu.jpg[] a|* Image information 

** <<Loaded Images Summary,Loaded Images Summary>> 

** <<Current Image Info,Current Image Info>> 

** <<Original File Info,Original File Info>> 

** <<Parameters Edition,Parameters Edition>> 

** <<Cartographic Parameters Edition,Cartographic Parameters Edition>> 

** <<Paper dimensions of Image,Paper dimensions of Image>>

* Sonar information 

** <<Sonar Data Info,Sonar Data Info >> 

** <<Sonar Characteristics Informations,Sonar Characteristics Information>> 

** <<Sonar Identification Edition,Sonar Identification Edition>>

* SonarScope Information 

** <<Info shortcuts,Info shortcuts>> 

** <<Version,Version>> 

** <<Preferences,Preferences>> 

** <<Close windows,Close windows>> 

** <<Benchmark,Benchmark>>
|====

== Image information ==

This section deals with summarizing information on images loaded into SonarScope 

=== Loaded Images Summary ===

This option creates a _Microsoft Excel_ file (_.xls_), saved in the temporary directory, summarizing information on each loaded file, and automaticaly launches _Excel_ with this file as input: 
[NOTE]
====
This functionality is only available if _Microsoft Excel_ is installed on your computer. 
====

image:0/0c/Info_excel.jpg[] 

Informations retrieved for each file are listed in the following chart: 

*ImageName* 

The image name as it appears inside the "Image Name" field

*NbRows* 

Pixel lines number of image

*NbColumns* 

Pixel columns number of image

*Unit* 

Image measurement units  (e.g. _"m"_ for Bathymetry, _"dB"_ for reflectivity, _"deg"_ for angles...)

*DataType* 

Data type according to data type list

*GeometryType* 

Geometry type according to geometry type list

*ImageType* 

Image type according to image type list

*TagSynchroX* 

Tag used to synchronize layers in abcissa

*Synchro in X* 

Value used to synchronize layers in abcissa

*TagSynchroY* 

Tag used to synchronize layers in ordinate

*Synchro in Y* 

Value used to synchronize layers in abcissa

*Synchro in X&amp;Y* 

Value used to synchronize layers in abcissa and ordinate

*Xlabel* 

Abscissa axis label

*XStepMetric* 

Metric equivalent step in abscissa

*XStep* 

Abscissa step and unit

*XLim* 

abscissa Min and Max values (in abscissa unit)

*XRange* 

Range in abscissa unit

*YLabel* 

Ordinate axis label

*YStepMetric* 

Metric equivalent step in ordinate

*YStep* 

Ordinate step and unit

*YLim* 

Ordinate Min and Max values (in ordinate unit)

*YRange* 

Range in ordinate unit

*Class* 

Type of encoding (ex: _"single"_ for single precision floating point number encoding)

*Colormap* 

Colormap name

*Memory* 

Indicates if the image is RAM stored or if it is saved in a file with "memory mapping" process (the memapfile name is then printed)

*ValNaN* 

Value specifying missing data

*InitialFileFormat* 

The initial file format (ex: _"SimradAll"_ for Simrad .all file)

*InitialFileName* 

The initial file name

*InitialImageName* 

The initial image name (usefull for processed image lifecycle monitoring)

*Moyenne* 

Mean value of image

*Sigma* 

Standard deviation of image

*Min* 

Min value of image

*Max* 

Max value of image

<<top,Top of the page>> 

=== Current Image Info ===

There are two ways to visualize information about current image: 

* *Classic way* This opens a dialog box where image information are printed in a list box. 

* *In an XML file* This writes information in an XML file (user named), and displays it in the default web browser.

For a detailed view of images information link:Image_properties.html[see this example]. 

For a detailed view of images properties meanings link:Role_of_properties.html[see this page] 

<<top,Top of the page>> 

=== Original File Info ===

The output of this functionality depends on original file format: 

* *[underline]#MBES files (".all", ".s7k")*#

This option scans original file and displays datagrams information within _DataView_: 

image:c/cd/Original_file_info.jpg[] 

_DataView_is a Reson software meant to read S7K files (_.s7k_), but it also reads ALL files (_.all_) from Kongsberg sounders. 

* *[underline]#NetCdf files (".mbb", ".imo", ".mbg")#*

In case of NetCdf file, this option scans file structure and displays it within the NetCdf inspector: 

image:c/ce/Original_file_info_2.png[] 

This tool enables user to explore the NetCdf structure, visualize attributes and variables type, values and dimensions. 

Moreover, additional buttons featuring in the bottom panel allow the user to: 

*Get a detailed view of the file's skeleton (Full description)*, 

*Find similar variables in the file, which share dimensions for instance (Similar variables)*, 

*Plot selected variables within a new figure (Display variable)*.


*[underline]#Raster file (".image:", ".gif", ".bmp")#*



In case of a raster file, this functionality sums up image file properties in a new figure: 

image:4/44/Original_file_info_3.png[] 

<<top,Top of the page>> 

=== Parameters Edition ===

Allows the user to edit image parameters such as: 

* *Unit* 

* *Xunit* 

* *Yunit* 

* *TagSynchroX*(name of the reference image used for abscissa synchronisation) 

* *TagSynchroY* (name of the reference image used for ordinates synchronisation) 

* *TagSynchroContrast*

It displays the following dialog box: 

image:0/03/Parameters_edition.jpg[] 

<<top,Top of the page>> 

=== Cartographic Parameters Edition ===

Allows user to edit (or load from a file) cartographic parameters for current image. 

User can define: 

* *Projection*: Geodetic, Mercator, UTM or Lambert) 

* *Ellipsoïde*: chosen among a predefined set, or manually defining one

<<top,Top of the page>> 

=== Paper dimensions of Image ===

Estimates possible print size, as a function of image dimension, and printer resolution in DPI. 

[NOTE]
====
This option is available for a LatLon geometry type only.
==== 

First you have to define printer's resolution: 

image:c/cb/Paper_dimension_1.jpg[] 

It will then display results for different image resolution: 

image:2/20/Paper_dimension_2.jpg[] 

<<top,Top of the page>> 

==  Sonar Information   ==

This section deals with summarizing sonar characteristics of images loaded into SonarScope. 

For a detailed view of sonar data link:sonar_data_properties.html[see this example]. 

For a detailed view of sonar characteristics link:sonar_characteristic-info.html[see this page] 

=== Sonar Data Info   ===

Displays sonar data information in a dialog box, as below: 

image:7/73/Sonar_data_info.jpg[] 

<<top,Top of the page>> 

=== Sonar Characteristics Informations   ===

====  *Light version*   ====

Displays Sonar characteristics, excluding transmitter an receiver characteristics, in a dialog box. 

====  *Complete version*   ====

Displays Sonar characteristics, including transmitter an receiver characteristics, in a dialog box. 

====  *Tx sectors manufacturer model*   ====

This functionality will be fully described later. 

====  *Tx sectors Ifremer model*   ====

This functionality will be fully described later. 

====  *Rx beams manufacturer model*   ====

This functionality will be fully described later. 

====  *Rx beams Ifremer model*   ====

This functionality will be fully described later. 

<<top,Top of the page>> 

=== Sonar Identification Edition ===

Allows user to redefine sonar identification, step by step, through dialog boxes. 

<<top,Top of the page>> 

== SonarScope Information ==

This section deals with customizing SonarScope's parameters, and retrieving software information. 

=== Info shortcuts ===

Displays SonarScope's keyboard shortcuts on the following dialog box: 

image:6/6b/Shortcuts.jpg[] 

For a detailed list of keyboard shortcuts link:shortcuts_page.html[see here] 

<<top,Top of the page>> 

=== Version ===

Displays SonarScope's historic. 

Current released version is v1.7 

<<top,Top of the page>> 

=== Preferences ===
Sets your preferences for the following items: 

*Language* 

_French / English_

*Use of Java for Input/Output* 

_Yes / No_

*Use of new SonarScope internal formats* 

_Yes / No_

*Number of computational threads* 

_One / Auto_

*Question level* 

_Beginner / Intermediate / Confirmed_

*Log file* 

_On / Off_

<<top,Top of the page>> 

=== Close windows ===

Close children windows open by SonarScope to display curves, profiles, etc. 

Displays a dialog box listing open windows. one may highlight the name of window to delete (or click *Select All*) and then click *ok* to validate his choice. 

<<top,Top of the page>> 

=== Benchmark   ===

Runs Matlab Benchmark to evalutate your computer performances, and displays results in the following two windows: 

image:Benchmark.jpg[] 

Computing performance as a function of different algorithms: 

image:f/fa/Benchmark1.jpg[] 

Result depends on loaded applications on the machine. Do not hesitate to repeat this test to obtain a significant result. 

'''

*_Go back to :_*

<<Top,Top of the page>> 

link:Accueil.html[SonarScope Help]
