:imagesdir: ../../images/SScHelp-Images/
:sectnums:
:toc: macro
:toc-title: Table of contents
:author: Jean-Marie Augustin
:email: sonarscope@ifremer.fr
:revnumber: 1.0
:revdate: Created 2017/04/05, Updated 2017/04/05
:revremark: Not reviewed

image:Bandeau_entete.jpg[]

// Author : {author} {email}
// Created : {revnumber}, updated : {revremark}


[IMPORTANT]
====
If you have any remark, suggestion, correction, etc, concerning this document, please email to {email}
====

toc::[]

// ------------------------------------------

= Elementary statistics =

Elementary statistics

= Current image Statistics : Elementary Statistics =

Computes and displays several classical statistical descriptors such as :

* mean,

* variance,

* standard deviation,

* median,

* minimum,

* maximum,

* range,

* quantile 1%,

* quantile 3%,

* quantile 25%,

* percentage of NaN,

* sampling,

* skewness,

* kurtosis,

* entropy.

The resulting values are displayed in a pop-up window as shown below :

image:b/bb/Statistics_overImage_Elem.jpg[]

'''

*_Go back to :_*

<<Top,Top of the page>>

link:Accueil.html[SonarScope Help]
