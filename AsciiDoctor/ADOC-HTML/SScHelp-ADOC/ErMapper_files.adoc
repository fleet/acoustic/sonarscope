:imagesdir: ../../images/SScHelp-Images/
:sectnums:
:toc: macro
:toc-title: Table of contents
:author: Jean-Marie Augustin
:email: sonarscope@ifremer.fr
:revnumber: 1.0
:revdate: Created 2017/04/05, Updated 2017/04/05
:revremark: Not reviewed

image:Bandeau_entete.jpg[]

// Author : {author} {email}
// Created : {revnumber}, updated : {revremark}


[IMPORTANT]
====
If you have any remark, suggestion, correction, etc, concerning this document, please email to {email}
====

toc::[]

// ------------------------------------------

= ErMapper files

".ERS" : Er-Mapper

= Er-Mapper ".ers" files =

This menu gathers functions meant to process a set of "_.s7k_" files :
|====

|*.ers menu overview*|*ers menu items*

|image:b/b5/Surveyproc_ERS_MenuOverView.png[] a|*Navigation Display*

* link:ERMapper_navigation.html#trueplot-navigation-time[Plot Navigation & Time]

* link:ERMapper_navigation.html#trueplot-navigation-signal[Plot Navigation & Signal]

*Geometric transformation*

* link:ERMapper_geometric.html#truepingsamples-pingrange-pingswath[PingSamples, PingRange -> PingSwath]

* link:ERMapper_geometric.html#truesidescan-sonar-pingswath-latlong[SideScan sonar PingSwath -> LatLong]

* link:ERMapper_geometric.html#truembes-pingbeam-latlong[MBES PingBeam -> LatLong]

*Utilities*

* link:ERMapper_filter.html#Filter_a_signal[Filter a signal]
|====

'''

*_Go back to :_*

<<Top,Top of the page>>

link:Survey_specific_processings.html[Survey processings]

link:Accueil.html[SonarScope top]
