:imagesdir: ../../images/SScHelp-Images/
:sectnums:
:toc: macro
:toc-title: Table of contents
:author: Jean-Marie Augustin
:email: sonarscope@ifremer.fr
:revnumber: 1.0
:revdate: Created 2017/04/05, Updated 2017/04/05
:revremark: Not reviewed

image:Bandeau_entete.jpg[]

// Author : {author} {email}
// Created : {revnumber}, updated : {revremark}


[IMPORTANT]
====
If you have any remark, suggestion, correction, etc, concerning this document, please email to {email}
====

toc::[]

// ------------------------------------------

= Arithmetic functions : Formula =

This functionality provides the user the ability to execute a Matlab statement with the various images loaded.

First, the user should type in his formula, using variables I1, I2, I3,...,In, for images 1,2,3,...,n respectively, and optionnaly V1,V2,V3,...,Vn, for vertical signal (i.e. ping dependent signal for sonar data) 1,2,3,...,n.

Then, the user sequentially associates variables to images or signal through the standard image chooser.

For example, let's try the following formula :

image:8/80/Gnrlproc_arithmetic_formula1.jpg[]

Where I1, I2 and I3 stands for :

I1 = _texture01.png_

I2 = _texture02.png_

I3 =_ Lena.tif_

image:2/24/Gnrlproc_arithmetic_add1.jpg[]

image:4/49/Gnrlproc_arithmetic_formula3.jpg[]

image:5/59/Gnrlproc_arithmetic_nan1.jpg[]

I1 + 2*I2 - 3*I3 =

image:8/88/Gnrlproc_arithmetic_formula4.jpg[]

'''

*_Go back to :_*

<<Top,Top of the page>>

link:Accueil.html[SonarScope Help]
