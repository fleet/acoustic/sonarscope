:imagesdir: ../../images/SScHelp-Images/
:sectnums:
:toc: macro
:toc-title: Table of contents
:author: Jean-Marie Augustin
:email: sonarscope@ifremer.fr
:revnumber: 1.0
:revdate: Created 2017/04/05, Updated 2017/04/05
:revremark: Not reviewed

image:Bandeau_entete.jpg[]

// Author : {author} {email}
// Created : {revnumber}, updated : {revremark}

[IMPORTANT]
====
If you have any remark, suggestion, correction, etc, concerning this document, please email to {email}
====

toc::[]

// ------------------------------------------

= Image production =

= Calibration : Image production =

This calibration menu provides the user processings needed to *retrieve accurate backscattering strength values*.

This menu is divided into two different parts :

* *Program mode* : meant to estimate each term of the sonar equation (transmission loss, antenna directivity, etc.), one at a time, and compensate from it. Hence, this section is aimed at expert users.

* *Result mode* and *Image production* : globally the same functionalities, but split into sequential stages, thus more user-friendly, and aimed at intermediate users.

This page deals with the later items :<<Image Production,image production>>.

== Image Production ==

This section provides user functionalities meant to apply compensation (Backscatter, Tx directivity, etc...) on reflectivity data.

Compensation is based on estimated backscatter and Tx directivity, that should have been already computed through the three sequential stages of link:Image_initialisation.html[result mode].

These estimations are stored in SonarScope, and linked to the processed image in result mode. Thus, compensations must be applied on an image that has already been processed through result mode. Otherwise a message will indicate that no backscatter curve nor directivity diagram is attached to current image.

Available compensations are listed below :
|====
.3+^.^|*Stage* 6+^.^|Output image compensation state:
2+|*Transmission Loss* 2+|*Backscatter* 2+|*Tx Directivity*

|yes|no|yes|no|yes|no

|*<<Image initialisation>>*|X| | |X|X(manufacturer)| 
|*<<Directivity compensation>>*|X| | |X|X (ifremer)| 
|*<<BS & Directivity compensation>>*|X| |X(ifremer)| |X(ifremer)|X
|*<<Received signal level>>*| | | | | | | 
|====


=== Image initialisation ===

Initializes image for backscatter and transmitter's directivity compensation : compensates reflectivity image from transmitter's directivity diagram, using manufacturer's directivity diagram models.

This function works like link:Result_mode.html#step_1[Result mode step 1] of the result mode.

=== Directivity compensation ===

Compensates reflectivity image from transmitter's directivity, estimated in link:DiagTx_estimationstep_3.html[DiagTx estimation step 3] of the result mode.

Resulting image is named "originalImage *- Data BS Ifremer*".

Let's take an example with demo data file _hydratech.imo_ :
|====
|Original reflectivity image:|Reflectivity image compensated from Tx directivity :

|image:5/5f/Sonarproc_calibration_Result_imageInit2.jpg[]|image:a/ae/Sonarproc_calibration_ImProd_BS.jpg[]
|====

The resulting compensated image (left thumbnail below) is to be compared with the one compensated with manufacturer's Tx directivity diagram model (right thumbnail below) :
|====
|Tx directivity from *Ifremer*|Tx directivity from *manufacturer*

|image:a/ae/Sonarproc_calibration_ImProd_BS.jpg[]|image:c/c5/Sonarproc_calibration_Result_imageInit3.jpg[]
|====

=== BS & Directivity compensation ===

Compensates reflectivity image from backscatter estimated in link:Backscatter_estimation.html[Backscatter estimation step 2] of the result mode, and transmitter's directivity, estimated in link:DiagTx_estimation.html[DiagTx estimation step 3].

Resulting image is named "originalImage *- Data Compensation*".

Let's take an example with demo data file _hydratech.imo_ :
|====
|Original reflectivity image:|Reflectivity image compensated from Tx directivity and backscatter :

|image:5/5f/Sonarproc_calibration_Result_imageInit2.jpg[]|image:1/1b/Sonarproc_calibration_ImProd_DiagTxBS.jpg[]
|====

=== Received signal level ===

This functionality is meant to computed received signal level on receivers, i.e. apply transmission loss on current reflectivity image

'''

*_Go back to :_*

link:Accueil.html[SonarScope Help]
