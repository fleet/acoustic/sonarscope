:imagesdir: ../../images/SScHelp-Images/
:sectnums:
:toc: macro
:toc-title: Table of contents
:author: Jean-Marie Augustin
:email: sonarscope@ifremer.fr
:revnumber: 1.0
:revdate: Created 2017/04/05, Updated 2017/04/05
:revremark: Not reviewed

image:Bandeau_entete.jpg[]

// Author : {author} {email}
// Created : {revnumber}, updated : {revremark}


[IMPORTANT]
====
If you have any remark, suggestion, correction, etc, concerning this document, please email to {email}
====

toc::[]

// ------------------------------------------

= Import-export =

SonarScope can import and export data in various formats: 

image:7/77/ImportExport.jpg[] 

===  SonarScope internal formats   ===

In order to speed up and homogenize data file reading, SonarScope features a new file loading approach.  

This new loading algorithm is based on splitting appart the original file into as many directories as datagram types featured in the loaded file. These directories are filled with as many binary files as variables contained into the corresponding datagram. 

Each binary file contains the whole range of data: sequence of data contained in each datagram of the same type are concatenated into a single binary file. 

Each datagram (i.e. each directory) is fully described in an XML file, enabling SonarScope (and user) to have a fast and easy access to the data. 

This decoding method is currently available for the following file types:

|====
|File extension|Manufacturer
|*.all*| Kongsberg

|*.s7k*| Reson

|*.rdf*| GeoAcoustics

|*.xtf*| EdgeTech
|====

The process is depicted in the following image: we use a kongsberg All file to illustrate the process 

image:d/d0/Decoding_synoptic_1.png[]

If you are interested in this approach, or willing to apply it to your own file format, feel free to contact Jean-Marie Augustin (+33 (0)2 98 22 47 03, [mailto:augustin@ifremer.fr augustin@ifremer.fr]) who will provide you source code for all the above listed file types. 

'''

*_Go back to :_*

<<Top,Top of the page>> 

link:Accueil.html[SonarScope Help]
