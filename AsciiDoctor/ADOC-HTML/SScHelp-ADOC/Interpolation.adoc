:imagesdir: ../../images/SScHelp-Images/
:sectnums:
:toc: macro
:toc-title: Table of contents
:author: Jean-Marie Augustin
:email: sonarscope@ifremer.fr
:revnumber: 1.0
:revdate: Created 2017/04/05, Updated 2017/04/05
:revremark: Not reviewed

image:Bandeau_entete.jpg[]

// Author : {author} {email}
// Created : {revnumber}, updated : {revremark}

[IMPORTANT]
====
If you have any remark, suggestion, correction, etc, concerning this document, please email to {email}
====

toc::[]

// ------------------------------------------

= Interpolation =

Interpolates and extrapolates missing images values (NaNs).

This functionality can be directly triggered by pressing *i* on the keyboard (see link:shortcuts_page.html[shortcuts page])

User must first defines a processing window :

image:4/49/Gnrlproc_interpolation_1.jpg[]

And then choose the operator used for interpolation :

image:1/17/Gnrlproc_interpolation_2.jpg[]

Let's take an example with demo data file _GolfeDuLion_LatLong_Bathymetry.ers_:

_GolfeDuLion_LatLong_Bathymetry.ers_

image:1/1a/Gnrlproc_interpolation_5.jpg[]

|====
|Zoom on NaN areas|interpolation (5x5 pixel, mean)

|image:0/0d/Gnrlproc_interpolation_4.jpg[]|image:8/82/Gnrlproc_interpolation_3.jpg[]
|====

'''

*_Go back to :_*

<<Top,Top of the page>>

link:General_processings.html[General processings Help]

link:Accueil.html[SonarScope Help]
