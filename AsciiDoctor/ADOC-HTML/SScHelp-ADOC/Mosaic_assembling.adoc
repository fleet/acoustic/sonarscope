:imagesdir: ../../images/SScHelp-Images/
:sectnums:
:toc: macro
:toc-title: Table of contents
:author: Jean-Marie Augustin
:email: sonarscope@ifremer.fr
:revnumber: 1.0
:revdate: Created 2017/04/05, Updated 2017/04/05
:revremark: Not reviewed

image:Bandeau_entete.jpg[]

// Author : {author} {email}
// Created : {revnumber}, updated : {revremark}

[IMPORTANT]
====
If you have any remark, suggestion, correction, etc, concerning this document, please email to {email}
====

toc::[]

// ------------------------------------------

= Mosaic : Mosaic assembling =

Merges several mosaics, and manages overlapping.

== Overlapping issues ==

Overlapping zones can be problematic in a mosaic of bathymetric data, as two or more depth values can contribute to a single location. But knowing the accuracy of data decreases with increasing beam angle, the DTM routines that indistinctly sum up all the contributing soundings would degrade (probabilistically speaking) a correct value by a more uncertain one. SonarScope uses a simple algorithm to overcome this problem: it uses the beam angle to decide which measurements, in the overlapping area, will be retained to make the DTM.

image:7/7f/RecouvrementProfils01.jpg[]

Digital terrain model built following the principle: each new profile supersedes the previous data.

image:0/00/RecouvrementProfils02.jpg[]

*_NIWA, New-Zealand_*

One distinguishes perfectly the left edge of overlapping areas which are of very poor quality.

Digital terrain model built by _SonarScope_: the center of the recovering zone is automatically found. This DTM has been created using only the least transmit beam angle for a given point in the overlaps.

image:5/5d/RecouvrementProfils03.jpg[]

*_NIWA, New-Zealand_*

Transmission angle mosaic (in absolute value): this image is the decision support for the line overlapping management, one clearly distinguishes places where lines moved together and those where lines went apart .

image:7/7a/RecouvrementProfils04.jpg[]

*_NIWA, New-Zealand_*

To manage the lines overlapping we could have chosen another metric as for example the acrosstrack distance. We chose the transmission angles because we know measurement accuracy is directly linked to this parameter. On flat seafloor, acrosstrack distances would give the same result, but on tilted seafloor our method is better.

Reflectivity mosaic: this image is realised according to the same principle than the DTM (it is the same functional module than the DTM creation, there is no distinction between "_bathy_ processing chain" and "Reflectivity processing chain" with _SonarScope.)_

image:b/bf/RecouvrementProfils05.jpg[]

*_NIWA, New-Zealand_*

== Mosaic merging in practice ==

First, the user must choose which mosaic he wants to merge from a list of existing mosaics :

image:3/3d/Sonarproc_MBES_mosaic_Merge_8.png[]

The user can turn on or off overlapping management :

image:b/b0/Sonarproc_MBES_mosaic_Merge_9.png[]

If the user chooses to use overlapping management, he can then give priority to :

* profile's center = vertical Tx angle

* profile's borders = grazing incidence Tx angle

image:4/47/Sonarproc_MBES_mosaic_Merge_12.png[]

The user must finally define geographical limit of mosaic :

image:a/a2/Sonarproc_MBES_mosaic_Merge_11.png[]

Let's take an example with EM3002 data comming from the common data set of [http://www.shallowsurvey2008.org/index.php?option= 5&Itemid =blogsection&id= 5&Itemid =27 UNH shallow survey 2008] :

* File 1 : _0008_20070919_130013_ShipName_2.all_

* File 2 : _0007_20070919_124524_ShipName_2.all_
|====
|File 1 : bathymetry PingBeam|File 2 : bathymetry PingBeam

|image:e/ec/Sonarproc_MBES_mosaic_Merge_1.png[]|image:1/1d/Sonarproc_MBES_mosaic_Merge_2.png[]
|====
|====
|File 1 : bathymetry mosaic|File 2 : bathymetry mosaic

|image:f/f8/Sonarproc_MBES_mosaic_Merge_4.png[]|image:c/c7/Sonarproc_MBES_mosaic_Merge_5.png[]
|====
File 1 & 2 : swath and overlapping

Mosaic merging of file 1 & 2 (priority center) : bathymetry and Tx angle

image:d/d2/Sonarproc_MBES_mosaic_Merge_3.png[]

image:d/d1/Sonarproc_MBES_mosaic_Merge_6.png[]

image:f/f6/Sonarproc_MBES_mosaic_Merge_7.png[]


Mosaic merging of file 1 & 2 (priority center) : shaded mosaic viewed in link:Export_data.html#fledermaus[fledermaus]

image:0/06/Sonarproc_MBES_mosaic_Merge_13.png[]

Here is an example that highlights the difference between overlapping management method.

Contrarily to bathymetry, where accuracy decreases with TxAngle, imagery is of poor quality for vertical angles. When overlapping is important, one might take advantage of it to improve imagery mosaic, by giving priority to soundings coming from grazing incidence instead of vertical angles.

Mosaic merging of imagery : priority to *vertical angles* (center)

image:0/08/Sonarproc_MBES_mosaic_Merge_14.png[]

Mosaic merging of imagery : priority to *grazing incidence *(borders)

image:b/b8/Sonarproc_MBES_mosaic_Merge_15.png[]

'''

*_Go back to :_*

<<Top,Top of the page>>

link:Accueil.html[SonarScope Help]
