:imagesdir: ../../images/SScHelp-Images/
:sectnums:
:toc: macro
:toc-title: Table of contents
:author: Jean-Marie Augustin
:email: sonarscope@ifremer.fr
:revnumber: 1.0
:revdate: Created 2017/04/05, Updated 2017/04/05
:revremark: Not reviewed

image:Bandeau_entete.jpg[]

// Author : {author} {email}
// Created : {revnumber}, updated : {revremark}


[IMPORTANT]
====
If you have any remark, suggestion, correction, etc, concerning this document, please email to {email}
====

toc::[]

// ------------------------------------------

= Multibeam echosounder =

This menu provides the user functionalities meant to process multibeam echosounder data.
|====
|_Multibeam Echosounder_ *menu overview*|_Multibeam Echosounder_ *menu items*
|image:2/20/Sonarproc_MBES_menuOverview.jpg[] a|* <<Import layers from PingBeam,Import layers from PingBeam>>

* <<truepingbeam_pingsamples_pingswath, PingBeam, PingSamples -> PingSwath>>

* <<truerange_angle_depth,Range, Angle -> Depth>>

* <<Mask from Depth,Mask from Depth>>

* <<Mask & Probability from Depth,Mask & Probability from Depth>>

* <<Bathymetry spike removal by JMA,Bathymetry spike removal by JMA>>

* <<truepingsnippets_pingsamples,PingSnippets -> PingSamples>>
|====

Mask recovering beams for a dual sounder

Wedge Image

== Import layers from PingBeam ==

TODO

<<Top,Top of the page>>

== PingBeam, PingSamples -> PingSwath ==

Computes swath from ping-beam or ping-samples geometry data files.

SonarScope proposes automatically to convert other related layers while processing the current image. The user must choose additional layers to convert within the following list :

image:5/5d/Sonarproc_MBES_2pingswath_1.jpg[]

The user should input the wanted swath resolution used for geometric conversion through the following pop-up :

image:9/9a/Sonarproc_MBES_2pingswath_2.jpg[]

As choosing a swath resolution is not trivial, SonarScope automatically displays an help window, updated according to current image data, meant to guide the user in his choice :

image:3/37/Sonarproc_MBES_2pingswath_12.jpg[]

Resulting image's name contains _"PingSwath"_ instead of _"PingBeam"_.

Let's take an example with demo data file _NIWA_MultiModes.all_ :
|====
|_NIWA_MultiModes.all_|ping/beam -> ping/swath

|image:7/79/Sonarproc_MBES_2pingswath_3.jpg[]|image:6/6f/Sonarproc_MBES_2pingswath_4.jpg[]
|====

<<Top,Top of the page>>

== Range, Angle -> Depth ==

Computes bathymetry using range and angle data from multibeam echosounder.

Bathymetry is computed by ray tracing, taking into account local sound speed profile for refraction, and ship's attitude for angular correction.

This functionality produces three images :

* computed bathymetry, named "originalImageName_ *Computed _[...]_bathymetry*"

* computed range along , named "originalImageName_ *Computed_[...]_MbesRangeAlong*"

* computed range across, named "originalImageName_ *Computed_[...]_MbesRangeAcross*"

Let's take an example with demo data file _NIWA_MultiModes.all_:
|====
|_NIWA_MultiModes.all_:original bathymetry|computed bathymetry:

|image:7/79/Sonarproc_MBES_2pingswath_3.jpg[]|image:0/07/Sonarproc_MBES_2depth_1.jpg[]
|_NIWA_MultiModes.all_ : original range across|computed range across :

|image:b/b0/Sonarproc_MBES_2depth_2.jpg[]|image:3/31/Sonarproc_MBES_2depth_3.jpg[]
|====

<<Top,Top of the page>>

== Mask from Depth ==

Creates a mask from bathymetry's histogram analysis.

This functionality splits bathymetric image into several smaller blocks, and computes the histogram of each block, that can be cleaned up using the link:SWW_Curves_Interactions.html#trueedition[curve editor]. When the process is complete, a mask is generated with two classes, one corresponding to erased data, the other to kept data. This mask can be further used for processing data : erase erroneous soundings, remove spikes, etc.

Resulting mask is named : "originalImageName_ *MasquefromHisto*".

The user must input the maximum size of blocks as a number of ping ("_nb lig per blocks_" item), and the histogram's step ("_treshold_" item):

image:7/75/Sonarproc_MBES_maskFromDepth_1.jpg[]

Let's take an example with demo data file _EM1002_0001_20040916_054013_raw_4.all_

_EM1002_0001_20040916_054013_raw_4.all_

image:8/83/Sonarproc_MBES_maskFromDepth_01.jpg[]
|====
|First block:|Corresponding histogram (cleaned):

|image:4/4d/Sonarproc_MBES_maskFromDepth_2.jpg[]|image:3/3a/Sonarproc_MBES_maskFromDepth_3.jpg[]

|Zoom on a bathymetry:|corresponding mask after processing:

|image:5/59/Sonarproc_MBES_maskFromDepth_5.jpg[]|image:7/76/Sonarproc_MBES_maskFromDepth_4.jpg[]
|====

<<Top,Top of the page>>

== Mask & Probability from Depth ==

Computes a probability image from bathymetry's histogram analysis.

This functionality is similar to <<Mask from Depth,mask from depth>> : it splits bathymetric image into several smaller blocks, and computes the histogram of each block, that can be cleaned up using the link:SWW_Curves_Interactions.html#trueedition[curve editor].

Each histogram is then normalized, and fitted with a gaussian, to obtain something similar to a probability density. Resulting image's pixels represent the probability for a sounding to be representative of the whole block bathymetric data which it comes from.

Resulting mask is named : "originalImageName_ *MasquefromHisto [...]_ProbafromPhase/Amplitude*".

Let's take an example with demo data file _EM1002_0001_20040916_054013_raw_4.all_

_EM1002_0001_20040916_054013_raw_4.all_

image:8/83/Sonarproc_MBES_maskFromDepth_01.jpg[]
|====
|First block:|Corresponding histogram (cleaned):

|image:4/4d/Sonarproc_MBES_maskFromDepth_2.jpg[]|image:3/3a/Sonarproc_MBES_maskFromDepth_3.jpg[]
|====

Corresponding probability image after processing :

image:9/9a/Sonarproc_MBES_mask&probaFromDepth_01.jpg[]

<<Top,Top of the page>>

== Bathymetry spike removal by JMA ==

Removes spikes from bathymetric data, using a locally adaptative process.

This function processes image block by block. It removes spikes from bathymetric data by substracting a fitted polynomial surface from local block bathymetric values, and masking only remaining values greater than a user-defined threshold.

This processing produces 4 different images :

* Filtered bathymetry, named "originalImageName_ *Spike*"

* Fitted polynomial surface, named "originalImageName_ *SpikePolyval*"

* Residuals, named "originalImageName_ *SpikeResidu*"

* Mask, named "originalImageName_ *Spike [...] _mask*"

The user must specify threshold and margin through the following listbox :

image:6/63/Sonarproc_MBES_spikeRemoval_01.jpg[]

Let's take an example with demo data file _EM1002_BELGICA_005053_raw.all_:
|====
|Zoom on a sand ripple|Corresponding computed polynomial surface :

|image:4/43/Sonarproc_MBES_spikeRemoval_02.jpg[]|image:4/45/Sonarproc_MBES_spikeRemoval_03.jpg[]

|bathymetry - fitted surface = residuals:|corresponding mask (threshold = 3m):

|image:7/7d/Sonarproc_MBES_spikeRemoval_04.jpg[]|image:7/79/Sonarproc_MBES_spikeRemoval_05.png[]

|masked bathymetry:|masked bathymetry link:Image_synthesis.html#trueinpainting[Inpainting]:

|image:8/8c/Sonarproc_MBES_spikeRemoval_06.png[]|image:4/4c/Sonarproc_MBES_spikeRemoval_07.png[]
|====

Notice on the vertical profile displayed on left of SWW, that spikes has actually been filtered from bathymetry (compare upper-left thumbnail and lower last thumbnails).

The gain of this processing is even more visible in 3D viewing, using link:Export_data.html#fledermaus[fledermaus] :

Original bathymetry

image:9/9e/Sonarproc_MBES_spikeRemoval_08.png[]

Spike-filtered bathymetry, inpainted :

image:a/a4/Sonarproc_MBES_spikeRemoval_09.png[]

Another example can be seen on link:Spikes_filtering.html[this page].

== PingSnippets -> PingSamples ==

TODO

'''

*_Go back to :_*

<<Top,Top of the page>>

link:Sonar_specific_processings.html[Sonar processings Help]

link:Accueil.html[SonarScope Help]
