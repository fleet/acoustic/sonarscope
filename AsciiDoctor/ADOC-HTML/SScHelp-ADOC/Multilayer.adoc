:imagesdir: ../../images/SScHelp-Images/
:sectnums:
:toc: macro
:toc-title: Table of contents
:author: Jean-Marie Augustin
:email: sonarscope@ifremer.fr
:revnumber: 1.0
:revdate: Created 2017/04/05, Updated 2017/04/05
:revremark: Not reviewed

image:Bandeau_entete.jpg[]

// Author : {author} {email}
// Created : {revnumber}, updated : {revremark}


[IMPORTANT]
====
If you have any remark, suggestion, correction, etc, concerning this document, please email to {email}
====

toc::[]

// ------------------------------------------

= Multilayer =

== Introduction ==

For a given sounding, the multibeam system provides the following information: depth, across and along-track distance, received beam angle, transmit and receive beam numbers, two-way travel time of the acoustic pulse, bottom detection mode, etc... 

SonarScope is particularly well adapted to manage and process data organized in a multi-layers environment: with a simple point and click action, one can get the information contained in each layer at the same location. A synchronization mechanism allows one to browse through all layers, while keeping the same selected geographical area. Statistical analyses involving different layers are also possible, such as getting the mean of an image as a function of grazing angle. 

image:3/38/Multicouche01.jpg[] 

==  Example of multilayers data   ==

image:6/6f/Multicouche02.jpg[] 

This is an example showing 4 layers of information from a single set:  

* Depth 

* Reflectivity 

* TxAngle transmit angle 

* TxBeamIndex transmit beam number

Images are displayed in PingAcrossDistance geometry: 

* Horizontal axis: across-track distance 

* Vertical axis: ping cycle

'''

*_Go back to :_*

<<Top,Top of the page>> 

link:Accueil.html[SonarScope Help]
