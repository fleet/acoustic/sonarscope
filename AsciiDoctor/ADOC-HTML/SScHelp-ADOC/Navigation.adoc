:imagesdir: ../../images/SScHelp-Images/
:sectnums:
:toc: macro
:toc-title: Table of contents
:author: Jean-Marie Augustin
:email: sonarscope@ifremer.fr
:revnumber: 1.0
:revdate: Created 2017/04/05, Updated 2017/04/05
:revremark: Not reviewed

image:Bandeau_entete.jpg[]

// Author : {author} {email}
// Created : {revnumber}, updated : {revremark}


[IMPORTANT]
====
If you have any remark, suggestion, correction, etc, concerning this document, please email to {email}
====

toc::[]

// ------------------------------------------

= Navigation =

_SonarScope_ features a navigation map with possible complementary information display. Upon mouse pointing, the date and the hour of the closest navigation point are written into a dedicated field. As well the nearest profile number is given with its beginning and ending hours; this profile is highlighted. 

*Navigation and water height* 

image:0/07/Navigation05.jpg[] 

*Navigation and survey date* 

image:7/73/Navigation06.jpg[] 

*Navigation and working mode of the sounder.* This representation is very useful within for data quality control. 

image:7/7f/Navigation01.jpg[] 

*Navigation and ship speed (m/s)* 

On this representation we can see at a glance seismic acquisition profiles as well as transit lines.

image:7/7c/Navigation02.jpg[] 

*Navigation and ship heading*

This representation is useful to locate at a glance upward and downward profiles.

image:8/8c/Navigation03.jpg[] 

*Navigation and the speed of sound at the water surface*

This display shows the speed of sound at the array, with a velocity change linked to the two periods of acquisition. This can be useful in a littoral context with spring water flows.

image:4/47/Navigation04.jpg[] 

'''

*_Go back to :_*

<<Top,Top of the page>> 

link:Accueil.html[SonarScope Help]
