:imagesdir: ../../images/SScHelp-Images/
:sectnums:
:toc: macro
:toc-title: Table of contents
:author: Jean-Marie Augustin
:email: sonarscope@ifremer.fr
:revnumber: 1.0
:revdate: Created 2017/04/05, Updated 2017/04/05
:revremark: Not reviewed

image:Bandeau_entete.jpg[]

// Author : {author} {email}
// Created : {revnumber}, updated : {revremark}


[IMPORTANT]
====
If you have any remark, suggestion, correction, etc, concerning this document, please email to {email}
====

toc::[]

// ------------------------------------------

= Classical filters : Other filters =

This menu gathers various classical image filters :
|====
|_classical Filters_ Menu|_Other Filters_
|image:3/3f/Gnrlproc_filtersClassic_menu3.jpg[] a|(click on menu item)

*Other*

* <<Unsharp,Unsharp>>

* <<Median,Median>>

* <<Wiener,Wiener>>

* <<Wallis,Wallis>>

* <<Laplacian,Laplacian>>

* <<Spike,Spike>>

* <<Standard deviation,Standard deviation>>

* <<Entropy,Entropy>>
|====
These filters can also be sorted according to their cut-off frequency :

*Low-pass*

*Band-pass*

*Hi-pass*

<<Median,*Median*>>

<<Wiener,*Wiener*>>

<<Wallis,*Wallis*>>

<<Unsharp,*Unsharp*>>

<<Laplacian,*Laplacian*>>

<<Spike,*Spike*>>

<<Standard deviation,*Standard deviation*>>

<<Entropy,*Entropy* >>

== Unsharp ==

Enhances current image's edges (and other high frequency components in an image) using the unsharp filter.

The unsharp filter is a 3-by-3 pixel contrast enhancement filter, created from the negative of the Laplacian filter with parameter alpha. Alpha controls the shape of the Laplacian and must be in the range 0.0 to 1.0. The default value for alpha is 0.2.

Its name comes from the fact that it enhances edges via a procedure which subtracts an unsharp, or smoothed, version of an image from the original image.

Resulting image is named : "originalImageName_ *Unsharp x_y*", where *x* and *y* are respectively alpha's integer and decimal part.

The user should input Alpha through the following dialog box :

image:a/aa/Gnrlproc_filtersClassic_unsharp1.jpg[]

Let's take an example with demo data _lena.tif_

Lena.tif 

image:8/84/Gnrlproc_filtersClassic_lena.jpg[]

Lena.tif:Unsharp filter

image:5/5f/Gnrlproc_filtersClassic_unsharp2.jpg[]

<<Top,Top of the page>>

== Median ==

Smooths current image using a median filter.

Like the link:linear.html#Rectangular_average[rectangular] and link:linear.html#Circular_average[circular average filter] the median filter considers each pixel in the image in turn and looks at its nearby neighbors to decide whether or not it is representative of its surroundings. Instead of simply replacing the pixel value with the mean of neighboring pixel values, it replaces it with the median of those values.

Median filtering is a nonlinear operation particularly useful to reduce *speckle noise* and *salt and pepper* noise, while preserving edges.

Resulting image is named : "originalImageName_ *Median [h w]*", where *h* and *w* are filter *h* eight and *w* idth in pixel.

The user should input filter size through the following dialog box :

image:0/09/Gnrlproc_filtersClassic_rectangle1.jpg[]

Let's take an example with demo data _ImageTestSpeckle.image_: 

ImageTestSpeckle.image:

image:3/38/Gnrlproc_filtersClassic_testOrig.jpg[]

ImageTestSpeckle.image: : Median Filter (3x3 pix)

image:4/44/Gnrlproc_filtersClassic_median.jpg[]

<<Top,Top of the page>>

== Wiener ==

Smoothes current image using a Wiener filter.

The wiener filter is an adaptive low-pass filter which tailors itself to be the best possible filter for a given image.  Where the local image variance is large, wiener filter performs little smoothing. On the other hand, where the local variance is small, wiener filter performs more smoothing.  In practice, the Wiener filter considers each pixel in the image in turn and looks at its nearby neighbors, estimates the local mean and variance around each pixel, and creates a pixelwise filter using these estimates.

This adaptive filter is more selective than a comparable linear filter, *preserving edges* and other high-frequency parts of an image.

Resulting image is named : "originalImageName_ *Wiener[h w]*", where *h* and *w* are filter *h* eight and *w* idth in pixel.

The user should input filter size through the following dialog box:

image:0/09/Gnrlproc_filtersClassic_rectangle1.jpg[]

Let's take an example with demo data _ImageTestSpeckle.image_: 

ImageTestSpeckle.image:

image:3/38/Gnrlproc_filtersClassic_testOrig.jpg[]

ImageTestSpeckle.image: : Wiener Filter (3x3 pix)

image:0/0f/Gnrlproc_filtersClassic_wiener.jpg[]

<<Top,Top of the page>>

== Wallis ==

Enhances current image's contrast using the wallis filter.

The Wallis filter applies a locally-adaptive contrast enhancement. In practice, the Wallis filter adjusts brightness values in local areas so that the local mean and standard deviation match user-specified values.  This enhancement *produces good local contrast* throughout the image, while *reducing the overall contrast* between bright and dark areas.

It is particularly useful for images in which there are significant areas of bright and dark tones, where a typical global contrast enhancement cannot simultaneously produce good local contrast at both ends of the brightness range. A global contrast stretch designed to provide detail in dark areas will likely saturate the bright areas, and vice versa.

Resulting image is named : "originalImageName_ *Wallis_x_y*", where *x* and *y* are respectively alpha's integer and decimal part.

The user should input filter parameters through the following dialog box :

image:b/b1/Gnrlproc_filtersClassic_wallis1.jpg[]

*Height* : height of the processing window

*Width* : width of the processing window

*Alpha* : weight of mean correction

*Beta* : weight of sigma correction

*Ref mean* : desired mean

*Sigma ref* : desired standard deviation

Let's take an example with demo data _ImageTestSpeckle.image_: 

ImageTestSpeckle.image:

image:3/38/Gnrlproc_filtersClassic_testOrig.jpg[]

ImageTestSpeckle.image: : Wallis Filter (parameters as shown above)

image:9/9b/Gnrlproc_filtersClassic_wallis2.jpg[]

<<Top,Top of the page>>

== Laplacian ==

Emphasizes current image's edges using the laplacian operator.

The Laplacian is a 2-D isotropic measure of the 2nd spatial derivative of an image.  Hence, the Laplacian _L(x,y)_ of an image with pixel intensity values _I(x,y)_ is given by :

image:1/10/Gnrlproc_derived_laplacian.gif[]

The Laplacian of an image highlights regions of rapid intensity change and is therefore often used for *edge detection*.

The Laplacian filter actually used is a 3-by-3 pixel filter approximating the shape of the 2-D Laplacian operator. The parameter alpha controls the shape of the Laplacian and must be in the range 0.0 to 1.0. The default value for alpha is 0.2

Resulting image is named : "originalImageName_ *Laplacien x_y*", where *x* and *y* are respectively alpha's integer and decimal part.

The user should input alpha parameter through the following dialog box:

image:8/83/Gnrlproc_filtersClassic_laplac1.jpg[]

Let's take an example with demo data _ImageTestSpeckle.image_: 

ImageTestSpeckle.image:

image:3/38/Gnrlproc_filtersClassic_testOrig.jpg[]

ImageTestSpeckle.image: : Laplacian Filter (alpha  = 0.2)

image:b/bc/Gnrlproc_filtersClassic_laplac2.jpg[]

<<Top,Top of the page>>

== Spike ==

Emphasizes current image's edges using spike filter.
|====
|spike filter

|1 -2 1 
|-2 4 -2
|1 -2 1
|====

Resulting image is named : "originalImageName_ *Spike*".

Let's take an example with demo data _ImageTestSpeckle.image_: 

ImageTestSpeckle.image:

image:3/38/Gnrlproc_filtersClassic_testOrig.jpg[]

ImageTestSpeckle.image: : Spike Filter

image:d/dc/Gnrlproc_filtersClassic_spike.jpg[]

<<Top,Top of the page>>

== Standard deviation ==

Filters speckle in current image using a standard deviation filter.

This filter considers each pixel in the image in turn, looks at its nearby neighbors enclosed in the processing window (height and width to be defined by user) and replaces the pixel value with the absolute value of the standard deviation of neighboring pixel values.

Resulting image is named : "originalImageName_ *Std*".

The user should input filter size and number of iterations through the following dialog box :

image:3/30/Gnrlproc_filtersClassic_Std1.jpg[]

Let's take an example with demo data _ImageTestSpeckle.image_: 

ImageTestSpeckle.image:

image:3/38/Gnrlproc_filtersClassic_testOrig.jpg[]

ImageTestSpeckle.image: : Standard deviation filter (3x3 pix)

image:e/eb/Gnrlproc_filtersClassic_Std2.jpg[]

<<Top,Top of the page>>

== Entropy ==

Filters speckle in current image using an entropy filter.

Entropy is a statistical measure of randomness that can be used to characterize the texture of the image.

This filter considers each pixel in the image in turn, looks at its nearby neighbors enclosed in the processing window (height and width to be defined by user), and replaces the pixel value with the entropy value of neighboring pixel values.

Resulting image is named : "originalImageName_ *Entropy*".

The user should input filter size and number of iterations through the following dialog box :

image:1/1b/Gnrlproc_filtersClassic_Entropy1.jpg[]

Let's take an example with demo data _ImageTestSpeckle.image_: 

ImageTestSpeckle.image:

image:3/38/Gnrlproc_filtersClassic_testOrig.jpg[]

ImageTestSpeckle.image: : Entropy filter (7x7 pix)

image:2/28/Gnrlproc_filtersClassic_Entropy2.jpg[]

'''

*_Go back to :_*

<<Top,Top of the page>>

link:Accueil.html[SonarScope Help]
