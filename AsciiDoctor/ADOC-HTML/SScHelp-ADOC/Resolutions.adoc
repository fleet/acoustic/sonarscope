:imagesdir: ../../images/SScHelp-Images/
:sectnums:
:toc: macro
:toc-title: Table of contents
:author: Jean-Marie Augustin
:email: sonarscope@ifremer.fr
:revnumber: 1.0
:revdate: Created 2017/04/05, Updated 2017/04/05
:revremark: Not reviewed

image:Bandeau_entete.jpg[]

// Author : {author} {email}
// Created : {revnumber}, updated : {revremark}


[IMPORTANT]
====
If you have any remark, suggestion, correction, etc, concerning this document, please email to {email}
====

toc::[]

// ------------------------------------------

= Resolutions =

*Acrosstrack and alongtrack resolutions for bathymetry* 

image:7/7f/Resolution01.jpg[] 

*Acrosstrack and alongtrack resolutions for imagery* 

image:3/3e/Resolution02.jpg[] 

*Bathymetry in PingBeam geometry: the following images show corresponding acrosstrack and alongtrack resolutions* 

image:a/a5/Resolution03.jpg[] 

*Alongtrack Resolution (m)* 

image:0/0e/Resolution05.jpg[] 

*Acrosstrack resolution of bathymetry (m)* 

image:2/2e/Resolution06.jpg[] 

*Acrosstrack resolution of imagery (m)* 

image:0/0c/Resolution07.jpg[] 

'''

*_Go back to :_*

<<Top,Top of the page>> 

link:Accueil.html[SonarScope Help]
