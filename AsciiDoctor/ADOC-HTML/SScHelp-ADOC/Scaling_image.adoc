:imagesdir: ../../images/SScHelp-Images/
:sectnums:
:toc: macro
:toc-title: Table of contents
:author: Jean-Marie Augustin
:email: sonarscope@ifremer.fr
:revnumber: 1.0
:revdate: Created 2017/04/05, Updated 2017/04/05
:revremark: Not reviewed

image:Bandeau_entete.jpg[]

// Author : {author} {email}
// Created : {revnumber}, updated : {revremark}


[IMPORTANT]
====
If you have any remark, suggestion, correction, etc, concerning this document, please email to {email}
====

toc::[]

// ------------------------------------------

= Scaling image

Left-clicking on image:f/fb/BasicFullExtent.jpg[] retrieves the full-size current image ("Quick-look" function). This will fit full extent of current image within frame.

[NOTE]
====
This functionality can be directly triggered by typing "*q*" (see link:Shortcuts_page.html[Shortcuts page]).
====

Left-clicking on image:e/e1/Basic1x1.jpg[] scales abscissa/ordinate axes according to 1 pixel / 1 pixel ratio : 1pixel of image corresponds to 1 pixel on screen.

Let's take an example with demo data _Bretagne.ers_ :

image:8/81/BasicBreizhOrigPan.jpg[]+ image:f/fb/BasicFullExtent.jpg[]= image:d/d1/BasicBreizhOrig.jpg[]

image:8/81/BasicBreizhOrigPan.jpg[]+ image:e/e1/Basic1x1.jpg[]= image:2/29/BasicBreizh1x1.jpg[]

'''

*_Go back to :_*

<<Top,Top of the page>>

link:Accueil.html[SonarScope Help]
