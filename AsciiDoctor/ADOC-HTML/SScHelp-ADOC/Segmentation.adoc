:imagesdir: ../../images/SScHelp-Images/
:sectnums:
:toc: macro
:toc-title: Table of contents
:author: Jean-Marie Augustin
:email: sonarscope@ifremer.fr
:revnumber: 1.0
:revdate: Created 2017/04/05, Updated 2017/04/05
:revremark: Not reviewed

image:Bandeau_entete.jpg[]

// Author : {author} {email}
// Created : {revnumber}, updated : {revremark}


[IMPORTANT]
====
If you have any remark, suggestion, correction, etc, concerning this document, please email to {email}
====

toc::[]

// ------------------------------------------

= Texture : Segmentation =

Computes current image segmentation using link:Training.html#truedisplay[previously computed textural signatures].
[NOTE]
====
Textural signatures are mandatory to segment an image.

====

The user must select the textural signatures to use among those available, through a classic list box dialog, and then define the processing window size.
The processing window represents one pixel on the resulting segmented image. Thus segmented image is smaller than the original one by a factor depending on processing window size.

Resulting segmented image will be named : "originalImageName _ *Segmentation*".

Let's take an example with demo data file _TexturesSonarMontage01.tif_ :

_TexturesSonarMontage01.tif_

image:7/73/Gnrlproc_Texture_training_1.jpg[]

32 x 32 processing window : perfect !

16 x 16 processing window : slight differences... but expected !

image:f/fa/Gnrlproc_Texture_segmentation_1.jpg[]

image:2/2d/Gnrlproc_Texture_segmentation_22.jpg[]

image:2/25/Gnrlproc_Texture_segmentation_2.jpg[]

image:2/24/Gnrlproc_Texture_segmentation_12.jpg[]

Reducing the size of the processing window generates a different segmentation, corresponding to blurred regions in the texture sample (highlighted in red on the right thumbnail below) that don't really match texture signature for this area :
|====
|16 x16 segmentation zoom on the upper-left corner|_TexturesSonarMontage01.tif_zoom_ on the upper-left corner

|image:e/e1/Gnrlproc_Texture_segmentation_3.jpg[]|image:d/d1/Gnrlproc_Texture_segmentation_4.jpg[]
|====


Let's take another more realistic example with demo data file _DF1000_REBENT_112.imo_ :

_DF1000_REBENT_112.imo_

image:c/c0/Gnrlproc_Texture_computation_1.jpg[]

|====
|Mask used for training : 3 classes sand (_yellow_), rocks(_red_), maerl(_cyan_)|Resulting image segmented (16x16 pix)

|image:b/b2/Gnrlproc_Texture_computation_2.jpg[]|image:3/34/Gnrlproc_Texture_computation_3.jpg[]
|====

|====
|segmented image, <<Homogenization,homogenized>>|Segmented image, homogenized + <<Contour_smoothed>>

|image:a/aa/Gnrlproc_Texture_computation_4.jpg[]|image:3/3f/Gnrlproc_Texture_computation_5.jpg[]
|====

'''

*_Go back to :_*

<<Top,Top of the page>>

link:Accueil.html[SonarScope Help]
