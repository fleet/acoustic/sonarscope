:imagesdir: ../../images/SScHelp-Images/
:sectnums:
:toc: macro
:toc-title: Table of contents
:author: Jean-Marie Augustin
:email: sonarscope@ifremer.fr
:revnumber: 1.0
:revdate: Created 2017/04/05, Updated 2017/04/05
:revremark: Not reviewed

image:Bandeau_entete.jpg[]

// Author : {author} {email}
// Created : {revnumber}, updated : {revremark}

[IMPORTANT]
====
If you have any remark, suggestion, correction, etc, concerning this document, please email to {email}
====

toc::[]

// ------------------------------------------

= Statistics =

The statistics toolbox is accessible through both the following button and menu item :
|====
|*Button*|*SWW Menu item*

|image:5/5b/ProcessingTools_But_stats.jpg[]|image:f/fb/Statistics_menu_overview.jpg[]
|====

Right-clicking on the button or choosing the *statistics* item in the SWW menu gives access to the following menu :

|====
|_Statistics_ *Menu*|*Type of Statistics*

|image:b/b5/Statistics_menu.jpg[]|link:Current_image_statistics.html[Current image statistics]

link:Inter-images_statistics.html[Inter-images statistics]

link:Root_mean_square_error.html[Root mean square error]

link:Bidimensional_histogram.html[Bidimensional histogram]
|====

'''

*_Go back to :_*

<<Top,Top of the page>>

link:Processings.html[Menus Help]

link:Accueil.html[SonarScope Help]
