:imagesdir: ../../images/SScHelp-Images/
:sectnums:
:toc: macro
:toc-title: Table of contents
:author: Jean-Marie Augustin
:email: sonarscope@ifremer.fr
:revnumber: 1.0
:revdate: Created 2017/04/05, Updated 2017/04/05
:revremark: Not reviewed

image:Bandeau_entete.jpg[]

// Author : {author} {email}
// Created : {revnumber}, updated : {revremark}


[IMPORTANT]
====
If you have any remark, suggestion, correction, etc, concerning this document, please email to {email}
====

toc::[]

// ------------------------------------------

= TXDiag compensation =

Compensation

= Calibration : program mode - Tx sectors =

Compensates, or not, current reflectivity image from transmitter's directivity pattern, according to user's choice :

|====
|Type of Tx directivity pattern compensation:|Output image name:

|*None* : no compensation for Tx directivity pattern|"originalImageName-*DiagEmi No Compensation*"

|*Manufacturer* : Tx directivity pattern is compensated using manufacturer's model or measurement.|"originalImageName- *DiagEmi Compensation Constructeur Analytique*"

|*Ifremer* : Tx directivity pattern compensated using Ifremer's model or measurement.|"originalImageName- *DiagEmi Compensation Ifremer Analytique*"
|====

The type of compensation applied to the current image is prefixed by a check mark in the compensation menu.

'''

*_Go back to :_*

<<Top,Top of the page>>

link:Accueil.html[SonarScope Help]
