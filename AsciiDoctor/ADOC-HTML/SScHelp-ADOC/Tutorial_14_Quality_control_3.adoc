:imagesdir: ../../images/SScHelp-Images/
:sectnums:
:toc: macro
:toc-title: Table of contents
:author: Jean-Marie Augustin
:email: sonarscope@ifremer.fr
:revnumber: 1.0
:revdate: Created 2017/04/05, Updated 2017/04/05
:revremark: Not reviewed

image:Bandeau_entete.jpg[]

// Author : {author} {email}
// Created : {revnumber}, updated : {revremark}


[IMPORTANT]
====
If you have any remark, suggestion, correction, etc, concerning this document, please email to {email}
====

toc::[]

// ------------------------------------------

= Tutorial 14 :Quality control 3 =

image:5/52/Tuto11_thumb.png[]

== Objectives ==

Display data, analyze artefact.

== SonarScope's functionality encompassed ==

* Multi-layer data,

* Bottom detection,

This tutorial introduces quality control of multibam echosounder data with SonarScope. It emphasizes the use of multi-layered data to highlights data's weakness.

=== Step 1 : Loading ===

Load the ".all" file considered :

image:8/89/Tuto2_mosaicBathy_5.png[]

image:8/82/Tuto11_step01_01.png[]

Load the *depth* datagram :

image:6/64/Tuto9_step1_02.png[]

Select the layers you want to import into SonarScope (here acknowledge default layers) :

image:f/f4/Tuto9_step1_03.png[]

The bathymetry layer in PingBeam geometry is displayed below :

image:e/ef/Tuto11_step01_02.png[]

*Problem* : We can witness a bathymetric gap on port side (beam #1 to beam #75), and also around beam # 200 on starboard.

Compute a vertical mean, by directly typing "*v*"on the keyboard, or :

*Statistics > Current Image Statistics > Vertical direction > Average*

image:b/b3/Tuto11_step01_03.png[]

image:4/4f/Tuto11_step01_04.png[]

One can clearly notice the two bathymetry gaps on the curve above.

We are going to use the powerful multi-layer manager of SonarScope, to inquire on this problem.

Press the "*Displayed image selection*" button, and use the *image browser* to switch between the different data layers loaded :

image:b/bd/Tuto9_step1_11.png[]

image:c/c9/Tuto11_step01_08.png[]

We have found the following relevant data layers :

Bathymetry artefact

*Bathymetry* Vs *Detection type*

image:d/d0/Tuto11_step01_06.png[]

image:2/27/Tuto11_step01_05.png[]

* Bathymetry artefact *:

Apparently, bathymetry gap around beam #200 is due to a wrong bottom detection : System should have used a phase detection for these beams, but it actualy used an amplitude detection. The best way to investigate further this issue would be to [underline]#inspect water column data#, which, in this case, are not available.

But we can still load a complementary layers to find out the root of the problem.

To load a complementary data layer from a previously loaded file, process as follows :

*File > Import > Complementary Layer*

image:4/48/Tuto9_step2_01.png[]

Then, choose the *depth* datagram :

image:a/a8/Tuto9_step2_02.png[]

Select the missing layers you want to import into SonarScope :

Here choose every "*detection layer*" available :

* DetecAmplitudeNbSamples

* DetecPhaseOrdrePoly

* DetecPhaseVariance

image:e/e4/Tuto11_step01_07.png[]

Now, we can witness on the *DetecAmplitudeNbSamples *layer that the system has more difficulties to detect the bottom, as it scans more samples to find the maximum. It should have logically switch to a phase detection.

Bathymetry artefact

*Bathymetry* Vs ( *Detection type *& *DetecAmplitudeNbSamples *)

image:d/d0/Tuto11_step01_06.png[]

image:2/27/Tuto11_step01_05.png[]

image:b/b3/Tuto11_step01_09.png[]

This may be due to the sonar mounting : the transducer arrays are mounted on a stick, dipped through a moon pool on port side of the surveying vessel. Thus the starboard array may receive a reflected signal from the vessel's keel, depending on the Tx beam angle. As the keel and the transducer arrays are relatively close, there is a little time difference between the two echoes, confusing the bottom detector, and causing this artefact.

'''

*_Go back to :_*

<<Top,Top of the page>>

link:Tuto.html[Tutorials]

link:Accueil.html[SonarScope Help]
