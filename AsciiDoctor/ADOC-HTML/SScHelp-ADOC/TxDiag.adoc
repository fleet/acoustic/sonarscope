:imagesdir: ../../images/SScHelp-Images/
:sectnums:
:toc: macro
:toc-title: Table of contents
:author: Jean-Marie Augustin
:email: sonarscope@ifremer.fr
:revnumber: 1.0
:revdate: Created 2017/04/05, Updated 2017/04/05
:revremark: Not reviewed

image:Bandeau_entete.jpg[]

// Author : {author} {email}
// Created : {revnumber}, updated : {revremark}


[IMPORTANT]
====
If you have any remark, suggestion, correction, etc, concerning this document, please email to {email}
====

toc::[]

// ------------------------------------------

= Calibration : program mode - Tx sectors =

This menu deals with *transmitter's directivity diagram*, and how to compensate from it.

|====
|_Tx sector_ *menu overview*|_Tx sector_ *menu items*

|image:0/0b/Sonarproc_calibration_Tx_menuOverview.jpg[] a|* link:TxDiag_levels.html[*Levels*]

* Manufacturer

** Curves

** Image

*** Analytical

*** Lookup table

* Ifremer

** Curves

** Image

*** Analytical

*** Lookup table

*link:TXDiag_compensation.html[Compensation]*

* None

* Manufacturer

** Analytical

** Lookup table

* Ifremer

** Analytical

** Lookup table

*link:TxDiag_Analysis.html[Analysis]*

* Computation

* Display

* Cleaning

* Filter

* Delete

* Export

* Import

* Modeling

* Define as an Ifremer model
|====

'''

*_Go back to :_*

<<Top,Top of the page>>

link:TVG.html[Calibration Help]

link:Accueil.html[SonarScope Help]
