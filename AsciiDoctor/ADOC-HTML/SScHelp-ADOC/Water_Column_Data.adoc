:imagesdir: ../../images/SScHelp-Images/
:sectnums:
:toc: macro
:toc-title: Table of contents
:author: Jean-Marie Augustin
:email: sonarscope@ifremer.fr
:revnumber: 1.0
:revdate: Created 2017/04/05, Updated 2017/04/05
:revremark: Not reviewed

image:Bandeau_entete.jpg[]

// Author : {author} {email}
// Created : {revnumber}, updated : {revremark}


[IMPORTANT]
====
If you have any remark, suggestion, correction, etc, concerning this document, please email to {email}
====

toc::[]

// ------------------------------------------

= Water Column Data =

Most of the newly released multibeam echosounders now feature water-column data exports, but only few processing softwares can handle those. 

_SonaScope_, alongside with its link:3D_viewer.html[3D viewer], can manage such data coming from Kongsberg or Reson MBES through a complete processing chain, coming from data import, quality control, compensation, and finaly 3D exports. 

The following screenshot shows water column data for a single ping, in two different geometries: 

* Beam Vs Samples 

* Depth Vs across track distance

|====
|WC data in BeamSample geometry|WC data in DepthAccrosDist geometry

|image:e/e4/WC_polar_0.png[]|image:1/1d/WC_polar_1.png[] 
|====

_SonarScope_ features all the following processing tools: 

* water column data display and movie making, 

* water column data echo-integration, 

* water column features pointing.

The next picture shows compensated water wolumn data imported into link:SSC-3DViewer_Help.html[SonarScope 3D viewer], along with corresponding bathymetry and vessel navigation. 

*Water column data in SonarScope 3D-viewer*

image:a/ae/WC_polar_2.png[] 

'''

*_Go back to :_*

<<Top,Top of the page>> 

link:Accueil.html[SonarScope Help]
