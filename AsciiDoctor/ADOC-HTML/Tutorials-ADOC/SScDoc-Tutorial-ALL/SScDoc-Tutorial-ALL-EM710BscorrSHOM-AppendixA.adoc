:imagesdir: images/Tutorial-EM710BscorrSHOM
:sectnums:
:toc: macro
:toc-title: Table of contents
:author: Jean-Marie Augustin
:email: sonarscope@ifremer.fr
:revnumber: 1.0
:revdate: Created 2017/04/05, Updated 2017/04/05
:revremark: Not reviewed

image:../../../images/Bandeau_entete.jpg[]

= Bscorr for a Kongsberg EM710 MBES - Appendix A =

// Author : {author} {email}
// Created : {revnumber}, updated : {revremark}

This tutorial illustrates how to create a bscorr.txt file for the _La Pérouse_ EM710 MBES (SHOM)

Some very interesting material (with theorical introduction) for bscorr production for EM302 can be found 
link:https://forge.ifremer.fr/docman/?group_id=126&view=listfile&dirid=839[here.]

include::../SScDoc-Tutorial-RemarkSuggestion.adoc[]

toc::[]

---

// ------------------------------------------

== Compute the angular backscatter model from the current image

[NOTE]
====
For the area "Carré Renard" we will use the BS model measured by an EK60 at 200 kHz. Nevertheless, we are going to illustrate how to get a BS model from the raw data, in case of you do not have acces to a calibrated measurement.
====

There are two ways to compute the BS model :

* From the current image
* From a set of .all files

The first one can be used if the data of the line is rich enough to describe the data of the whole survey
The second one will produce a curve for every line and will average the curves to deliver a curve which will be more representative of the area. This technique can erase asysmetrical artefacts, etc ... 

*The current image must be the one we computed as "Step 1"* (__0012_20151125_130147_LPO_FromSnippets_Step1_PingBeam_Reflectivity__ here).

==== Command ====

 Sonar processings / BS calibration / Step2 : BS estimation / Step 2a): Compute and Model
 
image:ScreenshotBscorrLaPerouse27.png[]

* A window appears here informing the configurations of the sounder ans asking if we want to set a Tag for this specific configuration. For the BS estimation, it is better to answer *No* because it would prevent any merge/average of several BS curbes obtained on different configurations.

image:ScreenshotBscorrLaPerouse28.png[]

* Accept the limits of the histogram (see link TO BE DONE) tutoriel sur le calcul des courbes

==== Define the curve name ====

image:ScreenshotBscorrLaPerouse29.png[]

==== Clean up the curve ====

The curve is computed then a data cleaning window is displayed.

image:ScreenshotBscorrLaPerouse30.png[]

// TODO (Tutorial TO BE DONE)

Here, only one point on the right has been removed.

==== Fit the model ====

The curve fitting model window now opens. You can use the Lauch button to run the algorithm. 

// (Tutorial TO BE DONE)

image:ScreenshotBscorrLaPerouse31.png[]

* Result 

image:ScreenshotBscorrLaPerouse32.png[]

[CAUTION]
====
The model obtained here  could be used as the angular backscatter curve for the rest of the Bscorr processing chain. Nevertheless, we are going to use the parameters obtained from the EK60 data. This model is illustrated here under.
====

// TODO : lien vers doc sur OptimDialog

=== Save the fitted model in a .mat file ===

==== Command ====

 Sonar processing / BS Calibration / Step 2 : BS estimation / Step 2 b) : Export
 
image:ScreenshotBscorrLaPerouse53.png[]

==== Choose the cleaned curve ====

image:ScreenshotBscorrLaPerouse54.png[]

[NOTE]
====
The data cleaner tool delivers two curves, the raw one and the cleaned one. It is possible to redo a data cleaning from the raw curve or to xxx the cleaned one with the Statistics / Curves / Cleaning menu
====

[NOTE]
====
If you have selected the raw one, it doesn't matter. It is the model that will be used latter on.
====

==== Define the output file name ====

image:ScreenshotBscorrLaPerouse55.png[]

=== Use the model obtained from the EK60 data ===

Change the values of the parameters manually. The values are :

.Parameters of BSLurton mode of the EK60 at 200 kHz
[width="50%",cols="2,1,1",options="header,footer"]
|====================
|  Parameter name|  Value|  unit
|  Specular level|  -9|  dB
|  Specular width|  20|  deg
|  Lambert law ref|  -12.6|  dB
|  Lambert law dec|  1.4|  
|  Transitory level|  -Inf|  dB
|  Transitory width|  -Inf|  deg
|====================

image:ScreenshotBscorrLaPerouse33.png[]

It is not possible with the current SSc version to zoom or pan on the curve graph.

Press on *OK* on the fitting tool window to validate the parameters.

* SonarScope plots a window of the data and the model

image:ScreenshotBscorrLaPerouse34.png[]

anchor:SaveFittedBSModel[]

=== Save the fitted model in a .mat file ===

anchor:SaveFittedBSModel[]

==== Command ====

 Sonar processing / BS Calibration / Step 2 : BS estimation / Step 2 b) : Export
 
image:ScreenshotBscorrLaPerouse53.png[]

==== Choose the cleaned curve ====

image:ScreenshotBscorrLaPerouse54.png[]

[NOTE]
====
If you have selected the raw one, it doesn't matter. It is the model that will be used latter on.
====

==== Define the output file name ====

image:ScreenshotBscorrLaPerouse56.png[]

Let us call this file _BS_CarreRenard_FromEK60_200kHz.mat_

// --------------------------------------------
