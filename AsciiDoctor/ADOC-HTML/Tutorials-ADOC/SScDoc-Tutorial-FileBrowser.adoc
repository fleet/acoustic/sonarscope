:imagesdir: images/Tutorial-FileBrowser
:sectnums:
:toc: macro 
:toc-title: Table of contents
:author: Jean-Marie Augustin
:email: sonarscope@ifremer.fr
:revnumber: 1.0
:revdate: Created 2017/04/05, Updated 2017/04/05
:revremark: Not reviewed

image:../../images/Bandeau_entete.jpg[]

= SonarScope File Browser =

This tutorial illustrates how use the SonarScope file browser.

include::SScDoc-Tutorial-RDF-RemarkSuggestion.adoc[]

toc::[ ]

---

== Window ==

The File Browser allows to select a set of files to be displayed or processed by SonarScope. It is possible to select the files one by one, by group of files, by directories, by lists, etc. It is possible to add filters such as Geomety names, Data Types, With or without specific strings, etc …

image:ScreenshotFileBrowser01.png[]

== File lists ==

We quite often need to select the same list of files to process the data (summary, plot navigation ...). If we want to process all the files of the directory, we just need to push the dedicated button. If we have to select a part of the files (on a single directory or in several ones), the task can be time consuming and the risk of error is high. To facilitate this task, SSc offers the possibility to save the list of files that are in the basket in a .txt ASCII file and to load this file (put the names into the basket) the next time we have to redo the selection.

This file list can olso be produced or used in the link:./SScDoc-Tutorial-NavigationDisplay.html[navigation display window].

=== Export a file list ===

Press Save File List button

image:ScreenshotFileBrowser02.png[]

Define the output .txt file

image:ScreenshotFileBrowser03.png[]

Content of the .txt list file in notepad++

image:ScreenshotFileBrowser04.png[]

=== Import a file list ===

Press Load File List button

image:ScreenshotFileBrowser05.png[]

Select the .txt file

image:ScreenshotFileBrowser06.png[]

* Remark 1 : the import of a file list adds the files to the ones that are already in the basket
* Remark 2 : SSc removes automatically the duplicated file names from the basket, it is so impossible to process a same file twice.
* Remark 3 : it is of course possible to merge file names coming from different directories in the same list file. 