:imagesdir: ./images
:sectnums:
:toc: macro
:toc-title: Table of contents
:author: Jean-Marie Augustin
:email: sonarscope@ifremer.fr
:revnumber: 1.0

image:Bandeau_entete.jpg[]

= SBP data - Plot the navigation =

include::../SScDoc-Tutorial-RemarkSuggestion.adoc[]

toc::[]

---

// ------------------------------------------

== Plot the navigation ==

=== Command ===

 Survey processings / ".seg" files (Seismics & SBP) / Navigation / Plot Navigation

image:ScreenshotSEG03.png[]

// -----------------------------------------------

include::./SScDoc-Tutorial-SEG-SelectTheFiles.adoc[]

// -----------------------------------------------

=== Result ===

image:ScreenshotSEG04.png[]

link:../SScDoc-Tutorial-NavigationDisplay.html[Access to the navigation display tutorial]

* Using the menus of this navigation window, it is possible to export the navigation into Google Earth (.kmz) :

image:ScreenshotSEG02.png[]

// ------------------------------------------

== Edit the navigation ==

=== Command ===

 Survey processings / ".seg" files (Seismics & SBP) / Navigation / Edit Navigation

image:ScreenshotSEG05.png[]

// -----------------------------------------------

include::./SScDoc-Tutorial-SEG-SelectTheFiles.adoc[]

// -----------------------------------------------

=== Result ===

The displayed GUI is the "**NavigationDialog**" tool of SonarScope. It is a common tool for all navigation data (MBES, SBES, SEGY, CINNA, Caraibes, Globe, ...)

image:ScreenshotSEG14.png[]

=== NavigationDialog features ===

See the link 
link:../SScDoc-Tutorial-RAW/SScDoc-Tutorial-RAW-PlotTheNavigation.html#NavigationDialogFeatures[here] for NavigationDialog GUI features (example from EK60 tutorial).

// -----------------------------------------------

include::SScDoc-Tutorial-SEG-End.adoc[]
