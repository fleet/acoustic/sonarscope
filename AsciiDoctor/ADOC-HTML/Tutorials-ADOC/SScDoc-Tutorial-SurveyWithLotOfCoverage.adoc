:imagesdir: ../../images/Tutorials-Images/Tutorial-SurveyWithLotOfCoverage
:sectnums:
:toc: macro
:toc-title: Table of contents
:author: Jean-Marie Augustin
:email: sonarscope@ifremer.fr
:revnumber: 1.0
:revdate: Created 2017/04/05, Updated 2017/04/05
:revremark: Not reviewed

image:../../SScHelp-Images/Bandeau_entete.jpg[]

= Survey report for a Kongsberg MBES =

// Author : {author} {email}
// Created : {revnumber}, updated : {revremark}

This tutorial illustrates how to create an automatic survey report for a Kongsberg MultiBeam Echo Sounder.

include::SScDoc-Tutorial-RDF-RemarkSuggestion.adoc[]

toc::[]

---

// ------------------------------------------

== About the data used for this tutorial ==

The data we are using in this tutorial comes from NIWA (NZ) https://www.niwa.co.nz/services/vessels/niwa-vessels/rv-tangaroa/[__R/V Tangaroa__].

The Multibeam Echo Sounder is a Kongsberg EM302. The data was specifically acquired for plumes detection in the Water Column (WC).

image:tangaroa_with_penguins_niwa.jpg[]

// ------------------------------------------

== Summary & Navigation ==

=== Command ===

 Survey processings / .all files (Kongsberg) / Summary / Survey Report / Summary & Navigation

image:./ScreenshotSurveyReport01.png[] 

=== Inputs ===

==== Select the .all files ====

image:./ScreenshotSurveyReport02.png[] 

See link:./SScDoc-Tutorial-FileBrowser.html[SSc file browser] to discover this tool.

==== Select or create the directory that will host the survey report ====

image:./ScreenshotSurveyReport03.png[] 

==== Define the name of the survey report ====

image:./ScreenshotSurveyReport04.png[]

[NOTE]
====
A .adoc file will be produced from this SurveyName. Il is thus recommanded not to use special characters and blanks.

".adoc" is the extention for asciidoctor (the software tool used for the SonarScope documentation). When SonarScope will have terminated to create the summary and navigation plots, it will open automatically AsciidocFX (the asciidoc editor). You will just need to click on the "HTML" button then button "Save" and a ".html" file will be created in the same directory. You can then open this .html file with your favorite internet browser.
====

==== Define the name of the survey report ====

image:./ScreenshotSurveyReport05.png[]

A list by default is proposed. For this tutorial we have selected the entire list.

==== Questions about parallel processing ====

image:./ScreenshotSurveyReport06.png[]

image:./ScreenshotSurveyReport07.png[]

Sonarscope proposes to create the summary using the parallel processing toolbox. If you answer No, a single .all file will be processed at a time. If you answer Yes, Matlab will create a number of workers that will take in charge the .all processing in parallel. The number of workers proposed in the second figure depends of the computer hardware (number of processors).

=== Results

SonarScope opens lot of figures, creates snapshots, export the navigation into GoogleEarth, etc. Once the processing of one figure is done, the figure is closed. this can be long, have a rest.

==== Message window ====

When SonarScope has finished the processing, you get this window :

image:./ScreenshotSurveyReport08.png[]

This message gives a description of the action you must do to create a .html version of the document.

[NOTE]
====
A solution to do this task automatically will be investigated.
====

==== AsciidocFX window ====

image:./ScreenshotSurveyReport10.png[]

[NOTE]
====
In case this editor was not found on your computer, you can install it using the "SetUpSonarScope_External_yyyymmdd_Rxxxxy.exe" setup.
====

==== HTML document ====

This link:./SurveyReport/Calypso.html[link] redirects to the HTML report page.

In addition to the HTML document, SonarScope has created numbers of files that are available for end users. These files are reachable from the HTML document. You can also access directly to these documents in the sundirectories PNG, KMZ, FIG, ... of the here named SurveyReport/Calypso directory.

SonarScope opens the summary file (Calypso-SummaryLines.csv) in your favorite speadsheet editor. It also opens the navigation in GoogleEarth

The figure of the InterPingDistances is not closed by SonarScope in order you can look at it :

image:./ScreenshotSurveyReport13.png[]

In this particular case, one can see that there are two populations of values. One set of files could be mosaiked with a 3m grid size and the other at 6m.A simple "sort" action in Excel on column "Dist inter ping (m)" higlits the two set of tiles : "kkkk_20151023_hhmmss.all" (grid size 3m) "kkkk_20151023_hhmmss.all (grid size 6m).


== Global DTM & Mosaics ==

=== Command ===

 Survey processings / .all files (Kongsberg) / Summary / Survey Report / Summary & Navigation

=== Result

image:ScreenshotBscorrLaPerouse02.png[]

link:../../Files/DocWord-SummaryLinesSHOM-LPO.xlsx[Excell file]