%{

% % Here is a copy of my "startup.m" file which must be copied on the matlab
% % path. The function "startup" is called when you launch Matlab. For me, it
% % is placed in directory "D:\perso-jma\Documents\MATLAB"
% % You must adapt this setup according to the place where you have copied
% % the SScTbx source directory ("C:\SScTbx" for me)

nomRootSrc  = 'C:'; % Where to find SScTbx
nomRootData = 'C:'; % Where to find SonarScopeData (installed with R20yyx_SetUpSonarScope_Data_yyyymmdd.exe

global IfrTbx
if ~isdeployed
    IfrTbx = [nomRootSrc '\SScTbx'];
    path(path, IfrTbx);
end

IfremerToolboxStartup(nomRootSrc, nomRootData)
%}




function IfremerToolboxStartup(nomRootSrc, nomRootData)
 
warning('off', 'MATLAB:ui:javacomponent:FunctionToBeRemoved')
warning('off', 'MATLAB:ui:javaframe:PropertyToBeRemoved')

datetime.setDefaultFormats('default', 'yyyy-MM-dd HH:mm:ss.SSS')

nomDirData = fullfile(nomRootData, 'SonarScopeData');

global useCacheNetcdf %#ok<GVMIS> 

global nomRoot         %#ok<GVMIS> 
global IfrTbx          %#ok<GVMIS> 
global IfrTbxResources %#ok<GVMIS> 
global SonarScopeData  %#ok<GVMIS> 
global IfrTbxLang      %#ok<GVMIS> 

global IfrTbxUseClickRightForFlag %#ok<GVMIS> 
global IfrTbxLevelQuestions       %#ok<GVMIS> 
global IfrTbxGraphicsOnTop        %#ok<GVMIS> 
global IfrTbxUseLogFile           %#ok<GVMIS> 
global IfrDpy                     %#ok<GVMIS> 
global IfrTbxTempDir              %#ok<GVMIS> 
global SonarScopeDocumentation    %#ok<GVMIS> 
global IfrTbxFlagContrasteAuto    %#ok<GVMIS> 

global VisualisateurImageExterne %#ok<GVMIS> 
global Panoply                   %#ok<GVMIS> 
global Visualisateur3D           %#ok<GVMIS> 
global IfrTbxRepDefInput         %#ok<GVMIS> 
global IfrTbxRepDefOutput        %#ok<GVMIS> 

global SizePhysTotalMemory  %#ok<GVMIS> 
global NUMBER_OF_PROCESSORS %#ok<GVMIS> 
global logFileId            %#ok<GVMIS> 
global sessionId            %#ok<GVMIS> 

IfrTbxFlagContrasteAuto = 0;
useCacheNetcdf = 0;
sessionId = randi(1000000,1);

if ~ismcc % Cas de la compilation
    if isdeployed
        IfrTbxForJava = pwd; % C:\SonarScope-R2018b-64Bits-2019-12-15 -> C:\SonarScope-R2018b-64Bits-2019-12-15\extern\java\lib\graphicsLib.jar
    else
        IfrTbxForJava = fullfile(IfrTbx, 'ifremer'); % C:\SScTbx\ifremer -> C:\SScTbx\ifremer\extern\java\lib\graphicsLib.jar
    end
    nomDirJava = fullfile(IfrTbxForJava, 'extern', 'java', 'lib', 'graphicsLib.jar');
    javaaddpath(nomDirJava);
end

% TRACE DEBUG INFO WARN ERROR FATAL
           
if isdeployed
    return
end

%% If not deployed

IfrDpy          = fullfile(nomRootSrc, 'IfremerDeploy');
IfrTbxResources = fullfile(IfrTbx,      'SScTbxResources');
SonarScopeData  = nomDirData;

SonarScopeDocumentation = fullfile(nomRootSrc, 'SScTbx', 'AsciiDoctor');

IfrTbxLang                 = 'US'; % 'FR' ou 'US'
IfrTbxUseClickRightForFlag = 'Off';
IfrTbxLevelQuestions       = '2';
IfrTbxGraphicsOnTop        = '2';
IfrTbxUseLogFile           = 'Off';
IfrTbxRepDefInput          = '';   % R�pertoire par d�faut en entr�e
IfrTbxRepDefOutput         = '';   % R�pertoire par d�faut en sortie
IfrTbxTempDir              = fullfile(nomRootSrc, 'Temp');   % R�pertoire par d�faut en sortie
nomRoot                    = nomRootSrc;

% TODO : Attention, cette variable est positionn�e correctement � l'int�rieur de SonarScope, c.a.d qu'il faut lancer une fois SonarScope pour qu'elle soit
% affect�e. Je ne me souviens pas pourquoi je l'ai impos�e � 0 ici.
% C'est dans creer_cpn_Info.m que cette affectation est r�alis�e :
NUMBER_OF_PROCESSORS = 0; %str2double(getenv('NUMBER_OF_PROCESSORS'));
ifrStartup(IfrTbx)
logFileId = getLogFileId;

if exist('mexPCGetPhysicalMemory', 'file') % #ok<EXIST>
    [SizePhysTotalMemory, SizePhysFreeMemory] = mexPCGetPhysicalMemory; %#ok<ASGLU>
    SizePhysTotalMemory = SizePhysTotalMemory / 8; % Modifi� par JMA le 31/01/2014 � bord du PP?, mission BICOSE pour pb mosaique totale OTUS
else
    SizePhysTotalMemory  = 500000; %1e6; % Kilo octets
end
SizePhysTotalMemory = min(SizePhysTotalMemory, 500000); % Kilo octets

% Donnez ici le nom complet du visualisateur externe d'images que vous utilisez d'habitude
% Moi, j'utilise le visualisateur d'ImageMagick, il est tr�s bien,
% vous pouvez le t�l�charger sur le net, il est gratuit. Une copie pour windows
% est presente dans ce r�pertoire (Istallation\ImageMagick-7.0.9-7-Q16-HDRI-x64-dll

VisualisateurImageExterne{1} = 'C:\Program Files (x86)\Earth Resource Mapping\ER Viewer 7.0\bin\ERViewer.exe';

try
    verImageMagick = '7.0.9';
    pathDirReg   = fullfile('SOFTWARE', 'ImageMagick', verImageMagick, 'Q:16');
    imdisplayExe = winqueryreg('HKEY_LOCAL_MACHINE', pathDirReg, 'BinPath');
    if isempty(imdisplayExe)
        imdisplayExe = 'C:\Program Files (x86)\ImageMagick-7.0.9-Q16\imdisplay.exe';
        if ~exist(imdisplayExe, 'file')
            imdisplayExe = 'C:\Program Files (x86)\ImageMagick-7.0.8-Q16\imdisplay.exe';
            if ~exist(imdisplayExe, 'file')
                imdisplayExe = 'C:\Program Files (x86)\ImageMagick-6.1.5-Q16\imdisplay.exe';
            end
        end
    end
    if exist(imdisplayExe, 'file')
        VisualisateurImageExterne{2} = imdisplayExe;
    end
catch
    VisualisateurImageExterne{2} = 'C:\Program Files (x86)\ImageMagick-6.1.5-Q16\imdisplay.exe';
end

Panoply         = 'C:\Program Files\PanoplyWin\panoply.exe';
Visualisateur3D = 'C:\Program Files\iVS7\bin\iview4D.exe';

warning('off', 'MATLAB:hg:uicontrol:ParameterValuesMustBeValid');

%{
% TODO : ne s'affiche que quand on sort de SSc !!! if isdeployed, return, end plus haut
Astuce = get_Astuce;
my_warndlg(Astuce, 0, 'FigName', 'Tip of the day', 'displayItEvenIfSSc', 1, 'TimeDelay', 60); % , 'Timer', 1
%}
