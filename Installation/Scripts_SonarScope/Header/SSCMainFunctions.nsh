; Fichier de fonctions, de macros de l'application d'installation SonarScope
; Developpé sous NSIS avec l'éditeur HM Edit de HM Soft.
;
; Date      : 18/04/2008
; Auteur    : GLU (ATL)
; Modification : -
; Création : 09/2005 - GLU, ATL (02 98 05 43 21).
; ================================================================================================
!ifndef _CustomFunction_nsh
!define _CustomFunction_nsh

!include Header\SSCMessages.nsh

; --------------------------------
; Contrôle des champs paramètres saisis
!macro InstallData un
Function ${un}InstallData

  ; Lecture du Handle de Window courante.
  FindWindow $varHdlWindow "#32770" "" $HWNDPARENT

  ; Lecture du .ini pour interagir sur la fenêtre sur les boutons activé ou non.
  ReadINIStr $0 "$PLUGINSDIR\SSCData.ini" "Settings" "State"
  ${If} $0 != 0
     ReadINIStr $R1 "$PLUGINSDIR\SSCData.ini" "Field 4" "state"

     ReadINIStr $0 "$PLUGINSDIR\SSCData.ini" "Field 10" "state"
     StrCpy $varChoixAction $0 1 ; Copie du 1er caractère uniquement.
     System::Int64Op $varChoixAction - 1
     Pop $varChoixAction

     ; Vérification du répertoire des données.
     ReadINIStr $R2 "$PLUGINSDIR\SSCData.ini" "Field 7" "state"
     ReadINIStr $R3 "$PLUGINSDIR\SSCData.ini" "Field 8" "state"
     ${If} $R2 != 0
     ${OrIf} $R3 != 0
          ; On active le répertoire de destination et le choix de l'opération
         ; à mener.
         GetDlgItem $2 $varHdlWindow 1203 ; 1200 + FieldNumber
         EnableWindow $2 1
         GetDlgItem $2 $varHdlWindow 1204 ; 1200 + FieldNumber
         EnableWindow $2 1
         GetDlgItem $2 $varHdlWindow 1205 ; 1200 + FieldNumber
         EnableWindow $2 1
         GetDlgItem $2 $varHdlWindow 1210 ; 1200 + FieldNumber
         EnableWindow $2 1
         ; Si mise à jour des chemins
         ${If} $varChoixAction == 0
             GetDlgItem $2 $varHdlWindow 1212 ; 1200 + FieldNumber
             EnableWindow $2 0
         ${Else} ; Si copie des données
             GetDlgItem $2 $varHdlWindow 1212 ; 1200 + FieldNumber
             EnableWindow $2 1
         ${EndIf}
         ${If} $R1 == ""
               MessageBox MB_OK|MB_ICONSTOP  $(err_input_varRepCibleData)
               Abort
         ${EndIf}
     ${EndIf}

     ${If} $R2 == 0
     ${AndIf} $R3 == 0
         GetDlgItem $2 $varHdlWindow 1203 ; 1200 + FieldNumber
         EnableWindow $2 0
         GetDlgItem $2 $varHdlWindow 1204 ; 1200 + FieldNumber
         EnableWindow $2 0
         ; Gestion des champs Nature de l'opération
         GetDlgItem $2 $varHdlWindow 1210 ; 1200 + FieldNumber
         EnableWindow $2 0
         GetDlgItem $2 $varHdlWindow 1205 ; 1200 + FieldNumber
         EnableWindow $2 0
         GetDlgItem $2 $varHdlWindow 1212 ; 1200 + FieldNumber
         EnableWindow $2 0
     ${EndIf}
  ${EndIf}

  ; Récupération de la touche sélectionné.
  ReadINIStr $0 "$PLUGINSDIR\SSCData.ini" "Settings" "State"
  ${If} $0 != 0
     Abort ; Return to the page
  ${EndIf}


FunctionEnd
!macroend     ; Detect MatLab Version

; --------------------------------
; Contrôle des champs paramètres saisis
!macro IdentifyMatLabVersion un
Function ${un}IdentifyMatLabVersion

  Pop $0
  ${select} "$0"
    ${case} "R2017b"
        StrCpy $0 "v93"
        StrCpy $1 "9.3"
        StrCpy $2 "MATLAB Runtime"
    ${case} "R2018a"
        StrCpy $0 "v94"
        StrCpy $1 "9.4"
        StrCpy $2 "MATLAB Runtime"
     ${case} "R2018b"
        StrCpy $0 "v95"
        StrCpy $1 "9.5"
        StrCpy $2 "MATLAB Runtime"
    ${case} "R2019a"
        StrCpy $0 "v96"
        StrCpy $1 "9.6"
        StrCpy $2 "MATLAB Runtime"
    ${case} "R2019b"
        StrCpy $0 "v97"
        StrCpy $1 "9.7"
        StrCpy $2 "MATLAB Runtime"
    ${case} "R2020a"
        StrCpy $0 "v98"
        StrCpy $1 "9.8"
        StrCpy $2 "MATLAB Runtime"
    ${case} "R2020b"
        StrCpy $0 "v99"
        StrCpy $1 "9.9"
        StrCpy $2 "MATLAB Runtime"
    ${case} "R2021a"
        StrCpy $0 "v910"
        StrCpy $1 "9.10"
        StrCpy $2 "MATLAB Runtime"
    ${case} "R2021b"
        StrCpy $0 "v911"
        StrCpy $1 "9.11"
        StrCpy $2 "MATLAB Runtime"
    ${case} "R2022a"
        StrCpy $0 "v912"
        StrCpy $1 "9.12"
        StrCpy $2 "MATLAB Runtime"
    ${case} "R2022b"
        StrCpy $0 "v913"
        StrCpy $1 "9.13"
        StrCpy $2 "MATLAB Runtime"
    ${case} "R2023a"
        StrCpy $0 "v914"
        StrCpy $1 "9.14"
        StrCpy $2 "MATLAB Runtime"
    ${case_else}
      MessageBox MB_OK "Problem with MCR Version"
      Abort
  ${endselect}

FunctionEnd
!macroend     ; IdentifyMatLabVersion

; --------------------------------
; Lancement de l'application si l'utilisateur le demande
!macro Launch
Function Launch
    SetOutPath "$INSTDIR\"
    Exec "$INSTDIR\SonarScope.exe"
FunctionEnd
!macroend

; --------------------------------
; Visualisation du ReadMe si l'utilisateur le demande
!macro ReadMe
Function ReadMe
    SetOutPath "$INSTDIR\"
    ${If} $LANGUAGE == ${LANG_FRENCH}
        Exec "notepad.exe $INSTDIR\readme_fr.txt"
    ${Else}
        Exec "notepad.exe $INSTDIR\readme_us.txt"
    ${EndIf}
FunctionEnd
!macroend

; --------------------------------
; Fonction de saisie du chemin des modules déjà installés.
!macro ExternalPathPage
Function ExternalPathPage

Var /GLOBAL varFlagExternalFolder

  ; Lecture du Handle de Window courante.
  FindWindow $varHdlWindow "#32770" "" $HWNDPARENT

  ; Lecture du .ini pour interagir sur la fenêtre sur les boutons activé ou non.
  ReadINIStr $0 "$PLUGINSDIR\SSCExternalPath.ini" "Settings" "State"

  ; Gestion des champs lus
  ReadINIStr $R0 "$PLUGINSDIR\SSCExternalPath.ini" "Field 6" "state"
  ; Lecture des champs de l'IHM
  ReadINIStr $varExternalFolder "$PLUGINSDIR\SSCExternalPath.ini" "Field 4" "State"
  ; Observation de l'existence du répertoire.
  IfFileExists $varExternalFolder\*.* ExternalFolderExists ExternalFolderDoesNotExist
          ExternalFolderDoesNotExist:
               StrCpy $varFlagExternalFolder "0"
               goto Suite
          ExternalFolderExists:
               StrCpy $varFlagExternalFolder "1"

          Suite:
  ${If} $R0 == 1
         ; On active le répertoire de destination et le choix de l'opération
         ; à mener.
         GetDlgItem $2 $varHdlWindow 1203 ; 1200 + FieldNumber
         EnableWindow $2 1

  ${Else}
         ; On active le répertoire de destination et le choix de l'opération
         ; à mener.
         GetDlgItem $2 $varHdlWindow 1203 ; 1200 + FieldNumber
         EnableWindow $2 0
  ${EndIf}

  ; Récupération de la touche sélectionné.
  ReadINIStr $0 "$PLUGINSDIR\SSCExternalPath.ini" "Settings" "State"
  ${If} $0 != 0
        Abort ; Return to the page
  ${EndIf}
  
  ${If} $R0 == "0"
     Strcpy $varExternalFolder ""
  ${Else}
     ${If} $R0 == "1"
           ${If} $varFlagExternalFolder == "0"
                 MessageBox MB_OK|MB_ICONEXCLAMATION "Directory does not exist"
                 Abort ; Return to the page
           ${EndIf}
     ${EndIf}
  ${EndIf}
FunctionEnd
!macroend

; Fonction de link
Function openLinkNewWindow
  Push $3
  Exch
  Push $2
  Exch
  Push $1
  Exch
  Push $0
  Exch

  ReadRegStr $0 HKCR "http\shell\open\command" ""
# Get browser path
    DetailPrint $0
  StrCpy $2 '"'
  StrCpy $1 $0 1
  StrCmp $1 $2 +2 # if path is not enclosed in " look for space as final char
    StrCpy $2 ' '
  StrCpy $3 1
  loop:
    StrCpy $1 $0 1 $3
    ;DetailPrint $1
    StrCmp $1 $2 found
    StrCmp $1 "" found
    IntOp $3 $3 + 1
    Goto loop

  found:
    StrCpy $1 $0 $3
    StrCmp $2 " " +2
      StrCpy $1 '$1"'

  Pop $0
  Exec '$1 $0'
  Pop $0
  Pop $1
  Pop $2
  Pop $3
FunctionEnd

!macro _OpenURL URL
Push "${URL}"
Call openLinkNewWindow
!macroend

!endif