; Fichier de messages de l'application d'installation SonarScope
; Developpé sous NSIS avec l'éditeur HM Edit de HM Soft.
;
; Date      : 18/04/2008
; Auteur    : GLU (ATL)
; Modification : -
; Création : 09/2005 - GLU, ATL (02 98 05 43 21).
; ================================================================================================

!ifndef _Custominfo_nsh
!define _Custominfo_nsh


!macro CustomMsg

; Dictionnaire des labels en anglais et français.
; Ces labels vont s'afficher au fil des pages d'installation.
;---------------------------------------------------------------------------
; textes en anglais
LicenseLangString info_copyright ${LANG_ENGLISH} ".\Textes\copyright_us.txt"
LangString info_welcomeSSCTitle ${LANG_ENGLISH} "Installation wizard"
LangString info_welcomeSSC ${LANG_ENGLISH} "This wizard will install locally the SonarScope Software.\r\n\r\n\
                                                WARNING!\r\n\
                                                Before running this wizard, the following setup programs must have been executed:\r\n\r\n\
                                                SonarScopeExternal\r\n\
                                                Installs the required and optional third-party programs.\r\n\r\n\
                                                SonarScopeData\r\n\
                                                Installs the SonarScope sample datasets.\r\n\r\n\
                                                SonarScopeDoc\r\n\
                                                Installs the SonarScope documentation\r\n\r\n\
                                                This installation wizard was produced with NSIS (by Nullsoft)."

LangString info_welcomeSSCExternal ${LANG_ENGLISH} "This wizard installs the external tools useful for SonarScope application on your computer.\r\n\
                                                   These tools are MatLab Component Runtime, mandatory for SonarScope application, and the optionnal viewers.\r\n\r\n\
                                                   \t They are installed since their own SetUp Wizard : \r\n\r\n\
                                                   \t -ER Viewer: to visualize big size images, recommended for DTM or Mosaic SONAR,\r\n\r\n\
                                                   \t-ImageMagic:  for SONAR or bathymetric images,\r\n\r\n\t-iView4D: for files with the FelderMaus format\r\n\r\n\
                                                   \t-GMT: for cartographic posting.\r\n\r\nThis assistant of installation was carried out with NSIS (of NullSoft)."

LangString info_welcomeSSCData ${LANG_ENGLISH} "This wizard will install locally the SonarScope sample data.\r\n\r\n\
                                                Warning : Before running this wizard, the ZIP archive SonarScopeData.zip should first have been extracted to a local directory (for example C:\). \
                                                The resulting SonarScopeData directory contains the following sub-directories:\r\n\r\n\
                                                \t-Public:\t\tContains generic sample datasets\r\n\r\n\
                                                \t-Sonar:\t\tContains sonar specific sample datasets\r\n\r\n\
                                                \t-Levitus:\tContains the Levitus dataset in MatLab format.\r\n\r\n\
                                                \r\n\r\nThis installation wizard was produced with NSIS (by Nullsoft)."
                                                
LangString info_welcomeSSCDoc ${LANG_ENGLISH} "This wizard will install locally the SonarScope Documentation.\r\n\r\n\
                                              The documentation is accessible with a Web Browser.\r\n\r\n\
                                              This installation wizard was produced with NSIS (by Nullsoft)"
                                              
LangString info_dirSoftTop ${LANG_ENGLISH} "Please indicate the location where the SonarScope Software should be installed."
LangString info_dirSoftDest ${LANG_ENGLISH} "Software Installation folder : "
LangString info_dirSoftDestLabel ${LANG_ENGLISH} "Software Installation folder : "
LangString info_dirExternalTop ${LANG_ENGLISH} "Please, point the folder where are already installed the external applications."
LangString info_dirExternalDest ${LANG_ENGLISH} "Installation folder : "
LangString info_dirTempTop ${LANG_ENGLISH} "Please indicate the location for temporary files."
LangString info_dirTempDest ${LANG_ENGLISH} "Temporary files folder : "
LangString info_dirTempDestLabel ${LANG_ENGLISH} "Temporary files folder : "
LangString info_finTitre ${LANG_ENGLISH} "SetUp completed"
LangString info_finTexte ${LANG_ENGLISH} "You have successfully completed installation Wizard for"
LangString info_finLaunch ${LANG_ENGLISH} "Launch SonarScope"
LangString info_finBtn ${LANG_ENGLISH} "Exit"
LangString info_sec_sonarscope ${LANG_ENGLISH} "SonarScope"
LangString info_sec_doc ${LANG_ENGLISH} "Documentation : User Manual and some presentations"
LangString error_softDir ${LANG_ENGLISH} "Error during the creation of install directory. Please, enter a valid one."
LangString error_tempDir ${LANG_ENGLISH} "Error during the creation of temporary directory. Please, enter a valid one."
LangString error_diskspace ${LANG_ENGLISH} "Not enough diskspace for the installation."
LangString info_run ${LANG_ENGLISH} "Launch"
LangString info_info ${LANG_ENGLISH} "Welcome to the SonarScope Setup wizard.$\r$\nPlease select a language for installation"
LangString components_desc ${LANG_ENGLISH} "Drag the cursor on the part to install"
LangString components_top ${LANG_ENGLISH} "Choose which features of SonarScope you want to install"
LangString components_title ${LANG_ENGLISH} "Choose the components"
LangString components_comlist ${LANG_ENGLISH} "Check the components you want to install and uncheck the components you don't want to install. Click next to continue"
LangString info_readme  ${LANG_ENGLISH} "View the readme file"
LangString info_configMailSSC ${LANG_ENGLISH} ": Config e-mail for sending to JMA-IFR"
LangString info_key_custom ${LANG_ENGLISH} ": Record keys input"
LangString info_key_title ${LANG_ENGLISH} "Record keys"
LangString info_key_subtitle ${LANG_ENGLISH} "Recording Corporation/Access Key"
LangString info_wrong_pwd ${LANG_ENGLISH} "Wrong user and/or Password, please Try Again !"
LangString info_3trys_pwd ${LANG_ENGLISH} "Three wrong trials. Please Contact Jean-Marie AUGUSTIN $\r$\n-IFREMER - NSE/AS - augustin@ifremer.fr - 02 98 22 47 03!"
LangString info_keybadfile_pwd ${LANG_ENGLISH} "The input Key file is corrupted. Please Contact Jean-Marie AUGUSTIN $\r$\n-IFREMER - NSE/AS - augustin@ifremer.fr - 02 98 22 47 03"


LangString info_sec_MCR ${LANG_ENGLISH} "MatLab Component Runtime: it must be installed at least once so that SonarScope could run.$\r$\nMCR is downloaded (since your usual web-browser) once you finish install External component then you'll have to follow indicated steps."
LangString info_sec_ERViewer ${LANG_ENGLISH} "recommended for big size images (DTM or Mosaic SONAR)"

LangString info_sec_MSVCR ${LANG_ENGLISH} "Microsoft Visual Studio C++ Run Time: it must be installed at least once so that SonarScope $\r$\nsoftware accelarations could run."

LangString info_ImageMagick ${LANG_ENGLISH} "for images SONAR or bathymetric"
;OLD LangString info_IView3D ${LANG_ENGLISH} "mandatory for Fledermaus format images"
LangString info_IView4D ${LANG_ENGLISH} "mandatory for IView4D format images"
LangString info_GoogleEarth ${LANG_ENGLISH} "The famous 3D Globe Viewer"
LangString info_Cortona3D ${LANG_ENGLISH} "Cortona 3D is a VRML viewer included in your Web browser (Firefox, IE ...)"
LangString info_Kongsberg ${LANG_ENGLISH} "Application for analyse content of Kongsberg files."
LangString info_NWW ${LANG_ENGLISH} "A NASA viewer 3D Globe with associated tools for tiling (Web Server for BIL Files request)"
LangString info_NWW_Java ${LANG_ENGLISH} "SonarScope 3D viewer (developed with Nasa World Wind Java - SDK). $\r$\n For more informations, please contact Jean-Marie AUGUSTIN -IFREMER - NSE/AS - $\r$\n augustin@ifremer.fr - 02 98 22 47 03"
LangString info_AdobeReader ${LANG_ENGLISH} "Acrobat Reader for PDF files - v8.1.1 US"
LangString info_Panoply ${LANG_ENGLISH} "Panoply for NetCDF and HSF files"
LangString info_GMT ${LANG_ENGLISH} "GMT Tools : installation of mandatory directories to visualize data with GMT"
LangString info_AsciiDocFx ${LANG_ENGLISH} "AsciiDoc_Fx : tools for upgrade SonarScope documentation by yourself"
LangString info_GhostTools ${LANG_ENGLISH} "Ghost Tools :generation and visualisation of PostScript"
LangString info_GhostScript ${LANG_ENGLISH} "GhostScript : for generating PostScript Files"
LangString info_GhostView ${LANG_ENGLISH} "GhostView : for visualising PostScript Files"
LangString info_email_text ${LANG_ENGLISH} "For security reasons, please type your email address :"
LangString info_RepOrigData_text ${LANG_ENGLISH} "Please, select the data source directory"
LangString info_email ${LANG_ENGLISH} "Email address"
LangString info_RepOrigData ${LANG_ENGLISH} "Data Source Directory"
LangString info_GDAL ${LANG_ENGLISH} "GDAL Library : installation of mandatory binaries to process Images"
LangString info_data_custom ${LANG_ENGLISH} ": Data Setup options"
LangString info_Doc_custom ${LANG_ENGLISH} ": Doc Setup options"
LangString info_data_title ${LANG_ENGLISH} "DOS Batches for data installation"
LangString info_data_subtitle ${LANG_ENGLISH} "Data copying from <Source> to <Destination>"
LangString info_dotNet ${LANG_ENGLISH} "Mandatory installation of DotNet FrameWork for Nasa World Wind"
LangString info_unInstallTitre ${LANG_ENGLISH} "SonarScope automatic uninstall"
LangString info_unInstallBtnTxt ${LANG_ENGLISH} "Uninstall"
LangString info_PlatFormInstallDoc ${LANG_ENGLISH} "Is a Doc installation for a SonarScope-32 bits Release ? $\r$\n (Click Yes for 32 Bits - No for 64 bits)"
LangString info_PlatFormInstallData ${LANG_ENGLISH} "Is a Data installation for a SonarScope-32 bits Release ? $\r$\n (Click Yes for 32 Bits - No for 64 bits)"
LangString info_manually_GDAL ${LANG_ENGLISH} "To reference GDAL in your environment, you have to proceed : \
$\r$\n$\r$\n- add to the path directory of GDAL as the following example : PATH = %path%;C:\Program Files\GDAL \
$\r$\n$\r$\n- create a variable environment system GDAL_DATA with the content (for example) : C:\Program Files\GDAL\gdal-data"

LangString error_input_email ${LANG_ENGLISH} "Please enter your email."
LangString error_input_RepCibleData ${LANG_ENGLISH} "Please enter the data source directory."

LangString err_public_diskspace ${LANG_ENGLISH} "Not enough diskspace for the installation of IfremerToolBoxDataPublic.$\r$\nRe-install this data by choosing an other directory."
LangString err_private_diskspace ${LANG_ENGLISH} "Not enough diskspace for the installation of IfremerToolBoxDataPrivate.$\r$\nRe-install this data by choosing an other directory."
LangString err_etopo_diskspace ${LANG_ENGLISH} "Not enough diskspace for the installation of IfremerToolBoxDataEtopo.$\r$\nRe-install this data by choosing an other directory."
LangString err_publicdelao_diskspace ${LANG_ENGLISH} "Not enough diskspace for the installation of IfremerToolBoxDataPublicDelao.$\r$\nRe-install this data by choosing an other directory."
LangString err_publicsonar_diskspace ${LANG_ENGLISH} "Not enough diskspace for the installation of IfremerToolBoxDataPublicSonar.$\r$\nRe-install this data by choosing an other directory."
LangString err_input_varRepCibleData ${LANG_ENGLISH} "The destination directory of the data isn't correct.$\r$\nPlease, input an other."
LangString err_UserProfile ${LANG_ENGLISH} "The user doesn't belongs to the administrators group$\r$\n. Please, connect you with an Administrator prfile."
LangString warn_MCR ${LANG_ENGLISH} "Warning: component MCR is already installed,$\r$\n SSc will unselect this software in the check box to come. "
LangString warn_NO_MCR ${LANG_ENGLISH} "Warning : MCR component not previously installed or not in good version.$\r$\nIt's mandatory to install this component once."
LangString warn_MSVCR ${LANG_ENGLISH} "Warning: component Microsoft Runtime is already installed,$\r$\n SSc will unselect this component in the check box to come. "
LangString warn_NO_MSVCR ${LANG_ENGLISH} "Warning : Microsoft Runtime component not previously installed.$\r$\nIt's mandatory to install this component once for sotware accelerations."
LangString warn_ERViewer ${LANG_ENGLISH} "Warning : ER-Viewer ${ERViewerVers} is already installed,$\r$\n SSc will unselect this software in the check box to come."
LangString warn_NO_ERViewer ${LANG_ENGLISH} "Warning : component ERViewer ${ERViewerVers} not previously installed.$\r$\nHowever, it's recommended to install this component."
LangString warn_ImageMagick ${LANG_ENGLISH} "Warning : componentImageMagick ${ImageMagickVers} is already installed.$\r$\nIf you want to reinstall it, please select the component in the following window."
LangString warn_NO_ImageMagick ${LANG_ENGLISH} "Warning : component ImageMagick ${ImageMagickVers} not previously installed.$\r$\nHowever, it's recommended to install this component."
LangString warn_iView4D ${LANG_ENGLISH} "Warning : component iView4D ${iView4dVers} is already installed.$\r$\nIf you want to reinstall it,  select the component in the following window."
LangString warn_NO_iView4D ${LANG_ENGLISH} "Warning : component iView4D ${iView4dVers} not previously installed.$\r$\nHowever, it's recommended to install this component."
LangString warn_GMT ${LANG_ENGLISH} "Warning : component GMT and its utilities is already installed.$\r$\nIf you want to reinstall it,  select the component in the following window."
LangString warn_NO_GMT ${LANG_ENGLISH} "Warning : component GMT and its utilities not previously installed.$\r$\nHowever, it's recommended to install this component."
LangString warn_path_GMT5 ${LANG_ENGLISH} "Warning : if you choose to install the GMT 5 component, please configure yourself the OS path about GMT5 with '%path%; <path_GMT5\bin>'."
LangString warn_GhostScript ${LANG_ENGLISH} "Warning : component GhostScript is already installed$\r$\n, SSc will unselect this software in the check box to come."
LangString warn_NO_GhostScript ${LANG_ENGLISH} "Warning : component GhostScript not previously installed.$\r$\nHowever, it's recommended to install this component."
LangString warn_GhostView ${LANG_ENGLISH} "Warning : component GhostView is already installed.$\r$\nIf you want to reinstall it,  select the component in the following window."
LangString warn_NO_GhostView ${LANG_ENGLISH} "Warning : component GhostView not previously installed.$\r$\nHowever, it's recommended to install this component."
LangString warn_GoogleEarth ${LANG_ENGLISH} "Warning : component GoogleEarth ${GoogleEarthVers} is already installed.$\r$\nIf you want to reinstall it, please select the component in the following window."
LangString warn_NO_GoogleEarth ${LANG_ENGLISH} "Warning : component GoogleEarth ${GoogleEarthVers} not previously installed.$\r$\nHowever, it's useful to install this component."
LangString warn_Kongsberg ${LANG_ENGLISH} "Warning : component Kongsberg is already installed.$\r$\nIf you want to reinstall it, please select the component in the following window."
LangString warn_NO_Kongsberg ${LANG_ENGLISH} "Warning : component Kongsberg  not previously installed.$\r$\nHowever, it's useful to install this component."
LangString warn_NASAWW ${LANG_ENGLISH} "Warning : component NASA World Wind ${NASAWWVers} is already installed.$\r$\nIf you want to reinstall it, please select the component in the following window."
LangString warn_NO_NASAWW ${LANG_ENGLISH} "Warning : component NASA World Wind  ${NASAWWVers} not previously installed.$\r$\nHowever, it's useful to install this component."
LangString warn_FileSd ${LANG_ENGLISH} "During the installation of IView4D, Link the .sd files to the application."
LangString warn_AcrobatReader ${LANG_ENGLISH} "Warning : component Acrobat Reader is already installed,$\r$\n SSc will unselect this software in the check box to come."
LangString warn_NO_AcrobatReader ${LANG_ENGLISH} "Warning : component Acrobat Reader not previously installed.$\r$\nIt's recommended to install this component for reading Doc files."
LangString warn_DocInstalled ${LANG_ENGLISH} "Directory does not exist or is empty.$\r$\nPlease, check that Document is really installed under : "
LangString warn_GDAL ${LANG_ENGLISH} "Warning : library GDAL and its utilities is already installed,$\r$\n SSc will unselect this software in the check box to come."
LangString warn_NO_GDAL ${LANG_ENGLISH} "Warning : component GDAL and its utilities not previously installed.$\r$\nHowever, it's recommended to install this component."
LangString warn_DataInstalled ${LANG_ENGLISH} "Directory does not exist or is empty.$\r$\nPlease, check that references Data are really installed under : "
LangString warn_pathTooLong ${LANG_ENGLISH} "Your computer's System Environment Path is too long to be automatically updated by the installation process. \
$\r$\n$\r$\nAt the end of the installation process, you will have to manually update the System Environment Path with the path to the newly installed applications (GDAL or Ghostview). \
$\r$\n$\r$\nPlease refer to http://support.microsoft.com/kb/310519/en-us for further details or contact SonarScope support:$\r$\n       JMA - IFREMER - augustin@ifremer.fr , tel : (+33)298 22 47 03)."
LangString warn_instance_installer ${LANG_ENGLISH} "The installer is already running."
LangString warn_UninstallSSC ${LANG_ENGLISH} "After uninstall SSC, you could manually delete Environment variable MCR_CACHE_ROOT  \
$\r$\n if you want to suppress definitively SSC from your computer"


;FRFRFRFRFRFRFRFRFRRFRF
; Messages en français
;FRFRFRFRFRFRFRFRFRRFRF

LicenseLangString info_copyright ${LANG_FRENCH} ".\Textes\copyright_fr.txt"
LangString info_welcomeSSCTitle ${LANG_FRENCH} "Assistant d'installation"
LangString info_AccueilSSCTexte ${LANG_FRENCH} "Ce programme va installer l'application SonarScope de l'IFREMER.\r\nLes viewers suivants sont proposés dans un autre Installateur comme outils optionnels:\r\n\r\n\t- ER Viewer : recommandé pour visualiser des images de grande taille soit MNT soit\t\t\tMosaïque SONAR,\r\n\r\n\t-ImageMagick : pour des images SONAR ou bathymétriques,\r\n\r\n\t-iView4D : pour des fichiers au format FlederMaus \r\n\r\n\t-GMT : pour l'affichage cartographique\r\n\r\nCet assistant d'installation a été réalisé avec NSIS (de NullSoft)."
LangString info_welcomeSSC ${LANG_FRENCH} "Cet assistant installe SonarScope sur votre ordinateur.\r\n\r\n\r\nNB:\r\n\r\nAfin que SonarScope fonctionne, vous devez avoir installer le moteur RunTime de MatLab (MCR-MatLab Component Runtime).\r\nPour cela, vous devez disposer de l'installateur des outils externes de SonarScope.\r\n\r\n\r\n\r\nCet assistant a été créé avec la plate-forme NSIS (of NullSoft)."
LangString info_welcomeSSCExternal ${LANG_FRENCH} "Ce programme va installer les outils externes à SonarScope de l'IFREMER.\r\nLes viewers suivants sont proposés dans un autre Installateur comme outils optionnels:\r\n\r\n\t- ER Viewer : recommandé pour visualiser des images de grande taille soit MNT soit\t\t\tMosaïque SONAR,\r\n\r\n\t-ImageMagick : pour des images SONAR ou bathymétriques,\r\n\r\n\t-iView4D : pour des fichiers au format FlederMaus \r\n\r\n\t-GMT : pour l'affichage cartographique\r\n\r\nCet assistant d'installation a été réalisé avec NSIS (de NullSoft)."
LangString info_welcomeSSCData ${LANG_FRENCH} "Cet assistant installe les données échantillons fournies avec SonarScope de l'IFREMER sur votre ordinateur.\r\n\r\n\
                                                Les données sont décomposées de la façon suivante:\r\n\r\n\
                                                \t-Public:\t\tcontiennent de la donnée hétérogènes (images, pas spécifiques aux Sonar),\r\n\r\n\
                                                \t-Sonar:\t\tcontient des données Publiques issues d'études spécifiques SONAR,\r\n\r\n\
                                                \t-Levitus:\tconntiennent les données LIVUSTUS en format MatLab.\r\n\r\n\r\n\
                                                This assistant of installation was carried out with NSIS (of NullSoft)."
LangString info_welcomeSSCDoc ${LANG_FRENCH} "Cet assistant installe la documentation de SonarScope sur votre PC.\r\n\r\nCette documentation est accessible via un navigateur Web.\r\n\r\n\r\n\r\nCet assistant d'installation a été réalisé avec NSIS (de NullSoft)."
LangString info_dirSoftTop ${LANG_FRENCH} "Merci d'indiquer le dossier où vous souhaitez installer SonarScope"
LangString info_dirSoftDest ${LANG_FRENCH} "Dossier d'installation de SonarScope: "
LangString info_dirSoftDestLabel ${LANG_FRENCH} "Dossier d'installation de SonarScope: "
LangString info_dirExternalTop ${LANG_FRENCH} "Merci d'indiquer le dossier où sont les applications déjà installées préalablement."
LangString info_dirExternalDest ${LANG_FRENCH} "Dossier d'installation : "
LangString info_dirTempTop ${LANG_FRENCH} "Merci d'indiquer le répertoire des données temporaires."
LangString info_dirTempDest ${LANG_FRENCH} "Répertoire des données temporaires : "
LangString info_dirTempDestLabel ${LANG_FRENCH} "Répertoire des données temporaires : "
LangString info_finTitre ${LANG_FRENCH} "Installation terminée"
LangString info_finTexte ${LANG_FRENCH} "Vous avez terminé avec succés l'installation de l'application :"
LangString info_finLaunch ${LANG_FRENCH} "Lancer SonarScope"
LangString info_finBtn ${LANG_FRENCH} "Sortie"
LangString info_sec_MCR ${LANG_FRENCH} "MatLab Component Runtime: doit être installé au moins une fois pour faire tourner SonarScope"
LangString info_sec_sonarscope ${LANG_FRENCH} "SonarScope"
LangString info_sec_doc ${LANG_FRENCH} "Documentation : Manuel Utlisateur et quelques présentations résumées"
LangString info_sec_ERViewer ${LANG_FRENCH} "recommandé pour des images de grande taille (MNT ou mosaïque SONAR)"
LangString info_ImageMagick ${LANG_FRENCH} "pour images SONAR ou bathymétriques"
;OLD LangString info_IView3D ${LANG_FRENCH} "obligatoire pour des images au format Fledermaus"
LangString info_IView4D ${LANG_FRENCH} "obligatoire pour des images au format Fledermaus"
LangString info_GoogleEarth ${LANG_FRENCH} "Le célèbre visualiteur 3D de la Terre"
LangString info_Cortona3D ${LANG_FRENCH} "Cortona 3D est un visualisateur VRML accessible depuis votre navigateur Web (Firefox, IE ...)"
LangString info_Kongsberg ${LANG_FRENCH} "Application d'analyse des datagrammes des fichiers Kongsberg."
LangString info_NWW ${LANG_FRENCH} "Un visualisateur 3D Open Source de la NASA avec les outils de traitement de tuiles associés (Serveur Web pour l'interrogation les fichiers BIL)"
LangString info_NWW_Java ${LANG_FRENCH} "SonarScope 3D viewer (developpé avec Nasa World Wind Java - SDK). $\r$\n Pour plus d'informations, contacter SVP Jean-Marie AUGUSTIN -IFREMER - NSE/AS - $\r$\n augustin@ifremer.fr - 02 98 22 47 03"
LangString info_AdobeReader ${LANG_FRENCH} "Acrobat Reader pour les fichiers PDF - v8.1.1 US"
LangString info_GMT ${LANG_FRENCH} "GMT Outils : installation des répertoires nécessaires pour manipuler les données avec GMT"
LangString info_AsciiDocFx ${LANG_FRENCH} "AsciiDoc_Fx : outil pour mettre à jour la documentation par vous-même."
LangString info_GDAL ${LANG_FRENCH} "Librairie GDAL : installation des répertoires nécessaires pour traiter des données avec GDAL"
LangString info_GhostTools ${LANG_FRENCH} "Ghost Tools :génération et visualisation de PostScript"
LangString info_GhostScript ${LANG_FRENCH} "GhostScript : pour générer des fichiers PostScript"
LangString info_GhostView ${LANG_FRENCH} "GhostView : pour visualiser des fichiers PostScript"
LangString info_email_text ${LANG_FRENCH} "Pour des raisons de suivi, SVP, entrer votre adresse :"
LangString info_RepOrigData_text ${LANG_FRENCH} "SVP, saisissez le répertoire d'entrée des données :"
LangString info_email ${LANG_FRENCH} "Adresse e-mail"
LangString info_RepOrigData ${LANG_FRENCH} "Répertoire d'entrée des données"
LangString info_run ${LANG_FRENCH} "Lancer"
LangString info_info ${LANG_FRENCH} "Welcome to the SonarScope setup wizard$\r$\nPlease select a language for installation."
LangString info_readme ${LANG_FRENCH} "Ouvrir le fichier Read Me"
LangString info_data_custom ${LANG_FRENCH} ": Options d'installation des Data"
LangString info_doc_custom ${LANG_FRENCH} ": Options d'installation de la documentation"
LangString info_data_title ${LANG_FRENCH} "Commandes DOS pour l'installation des Data"
LangString info_data_subtitle ${LANG_FRENCH} "Copie des data de <Source> vers <Destination>"
LangString info_configMailSSC ${LANG_FRENCH} ": Configuration de l'envoi de mail to JMA-IFR"
LangString info_key_custom ${LANG_FRENCH} ": Saisie des clés d'enregistrement"
LangString info_key_title ${LANG_FRENCH} "Clés d'enregistrement"
LangString info_key_subtitle ${LANG_FRENCH} "Enregistrement Etablissement/Clé d'accés"
LangString info_wrong_pwd ${LANG_FRENCH} "Mauvaise saisie de l'établissement et/ou du mot de passe ! Veuillez réessayer."
LangString info_3trys_pwd ${LANG_FRENCH} "Trois essais infructueux. Veuillez contacter Jean-Marie AUGUSTIN $\r$\nIFREMER - NSE/AS - augustin@ifremer.fr - 02 98 22 47 03!"
LangString info_keybadfile_pwd ${LANG_FRENCH} "Le fichier des mots de passe est erroné. Veuillez contacter Jean-Marie AUGUSTIN $\r$\nIFREMER - NSE/AS - augustin@ifremer.fr - 02 98 22 47 03"
LangString info_dotNet ${LANG_FRENCH} "Installation obligatoire du FrameWork DotNet pour Nasa World Wind"
LangString info_unInstallTitre ${LANG_FRENCH} "Désinstallation automatique de SonarScope"
LangString info_unInstallBtnTxt ${LANG_FRENCH} "Désinstaller"
LangString info_PlatFormInstallDoc ${LANG_FRENCH} "Est-ce une installation de Doc pour une version 32 Bits de SonarScope ? $\r$\n (Cliquer sur Yes pour 32 Bits - sur No pour 64 bits)"
LangString info_PlatFormInstallData ${LANG_FRENCH} "Est-ce une installation de Data pour une version 32 Bits de SonarScope ? $\r$\n (Cliquer sur Yes pour 32 Bits - sur No pour 64 bits)"
LangString info_manually_GDAL ${LANG_FRENCH} "Pour référencer GDAL dans votre environnement, vous devez : \
$\r$\n$\r$\n- ajouter au chemin Windows, la référence à GDAL comme dans l'exemple suivant : PATH = % PATH%;C:\Program Files\GDAL . \
$\r$\n$\r$\n- créer une variable système d'environnement GDAL_DATA avec le contenu exemple suivant : avec le C:\Program Files\GDAL\gdal-data."

LangString warn_instance_installer ${LANG_FRENCH} "L'installeur est déjà en cours d'exécution."
LangString error_input_email ${LANG_FRENCH} "Veuillez saisir une adresse e-mail."
LangString error_input_RepCibleData ${LANG_FRENCH} "Veuillez saisir le répertoire d'entrée des données."
LangString error_softDir ${LANG_FRENCH} "Erreur lors de la création du répertoire d'installation."
LangString error_tempDir ${LANG_FRENCH} "Erreur lors de la création du répertoire temporaire."
LangString error_diskspace ${LANG_FRENCH} "Pas assez d'espace disque pour l'installation."
LangString err_public_diskspace ${LANG_FRENCH} "Pas assez d'espace disque pour l'installation de IfremerToolBoxDataPublic.$\r$\nChoisissez un autre espace puis réinstaller ce répertoire."
LangString err_private_diskspace ${LANG_FRENCH} "Pas assez d'espace disque pour l'installation de IfremerToolBoxDataPrivate.$\r$\nChoisissez un autre espace puis réinstaller ce répertoire."
LangString err_etopo_diskspace ${LANG_FRENCH} "Pas assez d'espace disque pour l'installation de IfremerToolBoxDataEtopo.$\r$\nChoisissez un autre espace puis réinstaller ce répertoire."
LangString err_publicdelao_diskspace ${LANG_FRENCH} "Pas assez d'espace disque pour l'installation de IfremerToolBoxDataPublicDelao.$\r$\nChoisissez un autre espace puis réinstaller ce répertoire."
LangString err_publicsonar_diskspace ${LANG_FRENCH} "Pas assez d'espace disque pour l'installation de IfremerToolBoxDataPublicSonar.$\r$\nChoisissez un autre espace puis réinstaller ce répertoire."
LangString err_input_varRepCibleData ${LANG_FRENCH} "Le répertoire de destination des data est incorrect.$\r$\nSVP, saisissez un autre."
LangString err_UserProfile ${LANG_FRENCH} "L'utilisateur n'appartient pas au groupe des Administrateurs. $\nSVP, connectez-vous avec un profil d'administrateur."
LangString warn_MCR ${LANG_FRENCH} "Attention : composant MCR préalablement installé. $\r$\nSi vous voulez le réinstaller, veuillez sélectionner le composant dans la fenêtre suivante."
LangString warn_NO_MCR ${LANG_FRENCH} "Attention : composant MCR non-installé préalablement ou dans une version incorrecte.$\r$\nCe composant doit être installé au moins une fois."
LangString warn_ERViewer ${LANG_FRENCH} "Attention : composant ERViewer ${ERViewerVers} préalablement installé. $\r$\nSi vous voulez le réinstaller, veuillez sélectionner le composant dans la fenêtre suivante."
LangString warn_NO_ERViewer ${LANG_FRENCH} "Attention : composant ERViewer ${ERViewerVers} non-installé préalablement.$\r$\nPourtant, ce composant est recommandé."
LangString warn_ImageMagick ${LANG_FRENCH} "Attention : composant Image Magick ${ImageMagickVers} préalablement installé. $\r$\nSi vous voulez le réinstaller, veuillez sélectionner le composant dans la fenêtre suivante."
LangString warn_NO_ImageMagick ${LANG_FRENCH} "Attention : composant Image Magick ${ImageMagickVers} non-installé préalablement.$\r$\nPourtant, ce composant est recommandé."
;OLD LangString warn_iView3D ${LANG_FRENCH} "Attention : composant iView3D ${iView3dVers} préalablement installé. $\r$\nSi vous voulez le réinstaller, veuillez sélectionner le composant dans la fenêtre suivante."
;OLD LangString warn_NO_iView3D ${LANG_FRENCH} "Attention : composant iView3D ${iView3dVers} non-installé préalablement.$\r$\nPourtant, ce composant est recommandé."
LangString warn_iView4D ${LANG_FRENCH} "Attention : composant iView4D ${iView4dVers} préalablement installé. $\r$\nSi vous voulez le réinstaller, veuillez sélectionner le composant dans la fenêtre suivante."
LangString warn_NO_iView4D ${LANG_FRENCH} "Attention : composant iView4D ${iView4dVers} non-installé préalablement.$\r$\nPourtant, ce composant est recommandé."
LangString warn_GMT ${LANG_FRENCH} "Attention : composant GMT et ses utilitaires préalablement installé. $\r$\nSi vous voulez le réinstaller, veuillez sélectionner le composant dans la fenêtre suivante."
LangString warn_NO_GMT ${LANG_FRENCH} "Attention : composant GMT et ses utilitaires non-installé préalablement.$\r$\nPourtant, ce composant est recommandé."
LangString warn_path_GMT5 ${LANG_FRENCH} "Attention : si vous décidez d'installer le composant GMT5, merci d'enrichir votre PATH (variables d'environnement) par '%path%; <path_GMT5\bin>'."
LangString warn_GhostScript ${LANG_FRENCH} "Attention : composant GhostScript préalablement installé. $\r$\nSi vous voulez le réinstaller, veuillez sélectionner le composant dans la fenêtre suivante."
LangString warn_NO_GhostScript ${LANG_FRENCH} "Attention : composant GhostScript et ses utilitaires non-installé préalablement.$\r$\nPourtant, ce composant est recommandé."
LangString warn_GhostView ${LANG_FRENCH} "Attention : composant GhostView préalablement installé. $\r$\nSi vous voulez le réinstaller, veuillez sélectionner le composant dans la fenêtre suivante."
LangString warn_NO_GhostView ${LANG_FRENCH} "Attention : composant GhostView et ses utilitaires non-installé préalablement.$\r$\nPourtant, ce composant est recommandé."
LangString warn_AcrobatReader ${LANG_FRENCH} "Attention : composant Acrobat Reader installé préalablement.$\r$\nSi vous voulez le réinstaller, veuillez sélectionner le composant dans la fenêtre suivante."
LangString warn_NO_AcrobatReader ${LANG_FRENCH} "Attention : composant Acrobat Reader non-installé préalablement.$\r$\nPourtant, ce composant est recommandé pour la lecture de ficihers PDF (docs)."
LangString warn_GoogleEarth ${LANG_FRENCH} "Attention : composant GoogleEarth ${GoogleEarthVers} préalablement installé. $\r$\nSi vous voulez le réinstaller, veuillez sélectionner le composant dans la fenêtre suivante."
LangString warn_NO_GoogleEarth ${LANG_FRENCH} "Attention : composant GoogleEarth ${GoogleEarthVers} non-installé préalablement.$\r$\nPourtant, ce composant est utile."
LangString warn_Kongsberg ${LANG_FRENCH} "Attention : composant Kongsberg préalablement installé. $\r$\nSi vous voulez le réinstaller, veuillez sélectionner le composant dans la fenêtre suivante."
LangString warn_NO_Kongsberg  ${LANG_FRENCH} "Attention : composant Kongsberg non-installé préalablement.$\r$\nPourtant, ce composant est utile."
LangString warn_NASAWW ${LANG_FRENCH} "Attention : composant NASA World Wind ${NASAWWVers} préalablement installé. $\r$\nSi vous voulez le réinstaller, veuillez sélectionner le composant dans la fenêtre suivante."
LangString warn_NO_NASAWW ${LANG_FRENCH} "Attention : composant NASA World Wind ${NASAWWVers} non-installé préalablement.$\r$\nPourtant, ce composant est utile."
LangString warn_FileSd ${LANG_FRENCH} "Durant l'installation de IView4D, associez les fichiers .sd systématiquement à IView4D."
LangString warn_pathTooLong ${LANG_FRENCH} "Il est impossible d'enrichir directement le chemin d'exploitation de votre machine (trop long) pour les outils que vous avez choisi d'installer.\
$\r$\n$\r$\nVous devez, à la fin de cette installation et manuellement, enrichir le System Environment Path par le chemin d'exploitation de vos applications.\
$\r$\nSi vous avez une session ouverte de SonarScope, fermez et relancez la après mise à jour du Path.\
$\r$\n$\r$\nSVP, pour cela, se référer à http://support.microsoft.com/kb/310519/fr ou contactez :$\r$\n      JMA - IFREMER - augustin@ifremer.fr , tel : (+33)298 22 47 03).\
$\r$\nPar exemple : vous pouvez ajouter manuellement à la fin de votre path la localisation de GDAL de la façon suivante : PATH = %path%;C:\Program Files\GDAL. "
LangString warn_DocInstalled ${LANG_FRENCH} "Répertoire non-existant or empty.$\r$\nVP, vérifier que la documentation est bien présente sous : "
LangString warn_GDAL ${LANG_FRENCH} "Attention : librairie GDAL et ses utilitaires préalablement installé. $\r$\nSi vous voulez le réinstaller, veuillez sélectionner le composant dans la fenêtre suivante."
LangString warn_NO_GDAL ${LANG_FRENCH} "Attention : librairie GDAL et ses utilitaires non-installé préalablement.$\r$\nPourtant, ce composant est recommandé."
LangString warn_DataInstalled ${LANG_FRENCH} "Répertoire non-existant or empty.$\r$\nSVP, vérifier que les données de référence se sont installées sous : "
LangString warn_UninstallSSC ${LANG_FRENCH} "Après désinstallation de SSC, vous pouvez effacer la variable d'environnement MCR_CACHE_ROOT  \
$\r$\n si vous voulez supprimer définitivement de votre PC"


LangString components_desc ${LANG_FRENCH} "Glisser le curseur sur l'élément à installer"
LangString components_title ${LANG_FRENCH} "Choisissez les composants"
LangString components_top ${LANG_FRENCH} "Choisissez les composants de SonarScope que vous voulez installer"
LangString components_comlist ${LANG_FRENCH} "Cochez les composants que vous voulez installer et décocher les composants que vous ne voulez pas installer. Cliquer sur Installer pour démarrer l'installation."

!macroend

!endif