!ifndef _Utils_nsh
!define _Utils_nsh

!include LogicLib.nsh               ; Provides the use of various logic statements

;+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
; Déclarations de sous-fonctions
; Sources : http://nsis.sourceforge.net.
;+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


; Détermination si l'utilisateur est Administrateur.
; -------------------------------------------------
; $0 = 1 if the user belongs to the administrator's group
; $0 = 0 if not
; $0 = -1 if there was an error (only for the 1st Macro)
!macro IsUserAdmin RESULT
 !define Index "Line${__LINE__}"
   StrCpy ${RESULT} 0
   System::Call '*(&i1 0,&i4 0,&i1 5)i.r0'
   System::Call 'advapi32::AllocateAndInitializeSid(i r0,i 2,i 32,i 544,i 0,i 0,i 0,i 0,i 0, \
   i 0,*i .R0)i.r5'
   System::Free $0
   System::Call 'advapi32::CheckTokenMembership(i n,i R0,*i .R1)i.r5'
   StrCmp $5 0 ${Index}_Error
   StrCpy ${RESULT} $R1
   Goto ${Index}_End
 ${Index}_Error:
   StrCpy ${RESULT} -1
 ${Index}_End:
   System::Call 'advapi32::FreeSid(i R0)i.r5'
 !undef Index
!macroend


; Fonction de remplacement de chaîne de caractères.
; -------------------------------------------------
!macro StrRep un
Function ${un}StrRep
  ;Written by dirtydingus 2003-02-20 04:30:09
  ; USAGE
  ;Push String to do replacement in (haystack)
  ;Push String to replace (needle)
  ;Push Replacement
  ;Call StrRep
  ;Pop $R0 result

  Exch $R4 ; $R4 = Replacement String
  Exch
  Exch $R3 ; $R3 = String to replace (needle)
  Exch 2
  Exch $R1 ; $R1 = String to do replacement in (haystack)
  Push $R2 ; Replaced haystack
  Push $R5 ; Len (needle)
  Push $R6 ; len (haystack)
  Push $R7 ; Scratch reg
  StrCpy $R2 ""
  StrLen $R5 $R3
  StrLen $R6 $R1
loop:
  StrCpy $R7 $R1 $R5
  StrCmp $R7 $R3 found
  StrCpy $R7 $R1 1 ; - optimization can be removed if U know len needle=1
  StrCpy $R2 "$R2$R7"
  StrCpy $R1 $R1 $R6 1
  StrCmp $R1 "" done loop
found:
  StrCpy $R2 "$R2$R4"
  StrCpy $R1 $R1 $R6 $R5
  StrCmp $R1 "" done loop
done:
  StrCpy $R3 $R2
  Pop $R7
  Pop $R6
  Pop $R5
  Pop $R2
  Pop $R1
  Pop $R4
  Exch $R3

FunctionEnd
!macroend

; Fonction de vérification d'emplacement de disque dur.
; -------------------------------------------------
!macro CheckSpaceFunc un
Function ${un}CheckSpaceFunc
; Appel :
; $0 - space required in kb
; $1 - path to check
; $2 - 0 = ignore quotas, 1 = obey quotas
; trashes $2
  IntCmp $2 0 ignorequota
  ; obey quota
  System::Call '${sysGetDiskFreeSpaceEx}(r1,.r2,,.)'
  goto converttokb
  ; ignore quota
  ignorequota:
  System::Call '${sysGetDiskFreeSpaceEx}(r1,.,,.r2)'
  converttokb:
  ; convert the large integer byte values into managable kb
  System::Int64Op $2 / 1024
  Pop $2
  ; check space
  System::Int64Op $2 > $0
  Pop $2
Functionend
!macroend

; Fonction de récupération des natures et identités des drives de la machine.
; ---------------------------------------------------------------------------
!macro GetDrivesCallback un
Function ${un}GetDrivesCallback
        StrCmp $9 "" FinGetDrives
        ;MessageBox MB_YESNO '$$8={$8}$\n$$9={$9}$\n$\nContinue?' IDYES +2
        ;MessageBox MB_OK '$$8={$8}$\n$$9={$9}$\n'
        StrCpy $R4 '$9  [$8 Drive]'
        StrCpy $R5 '$R5$9  [$8 Drive]|'
        Goto LoopGetDrives
        FinGetDrives:
        StrCpy $0 StopGetDrives
	Push $R5
	Push $R4
	LoopGetDrives:
	Push $0
FunctionEnd
!macroend

; --------------------------------
; Fonction de vérification des espaces (caractéres blanc) sur les répertoires d'installation
!macro CheckForSpaces un
Function ${un}CheckForSpaces
 Exch $R0
 Push $R1
 Push $R2
 Push $R3
 StrCpy $R1 -1
 StrCpy $R3 $R0
 StrCpy $R0 0
 loop:
   StrCpy $R2 $R3 1 $R1
   IntOp $R1 $R1 - 1
   StrCmp $R2 "" done
   StrCmp $R2 " " 0 loop
   IntOp $R0 $R0 + 1
 Goto loop
 done:
 Pop $R3
 Pop $R2
 Pop $R1
 Exch $R0
FunctionEnd
!macroend


; Fonction de récupération des infos de mémoire physique (RAM).
; ------------------------------------------------------------
!macro GetPhysicalMemory un
Function ${un}GetPhysicalMemory
   System::Alloc 32
   Pop $1
   System::Call "Kernel32::GlobalMemoryStatus(i) v (r1)"
   System::Call "*$1(&i4 .r2, &i4 .r3, &i4 .r4, &i4 .r5, \
                     &i4 .r6, &i4.r7, &i4 .r8, &i4 .r9)"
   System::Free $1
   System::Int64Op $4 / 1024
   Pop $4
   System::Int64Op $5 / 1024
   Pop $5
   ;MessageBox MB_OK "Memory load: $3%"
   ; DetailPrint "Total physical memory: $4 bytes"
   ; DetailPrint "Free physical memory: $5 bytes"
FunctionEnd
!macroend

; Fonction de récupération des infos de mémoire physique (RAM).
; ------------------------------------------------------------
!macro GetPhysicalMemory64 un
Function ${un}GetPhysicalMemory64
   System::Alloc 64
   Pop $1
   # init
   System::Call "*$1(i64)"
   # call
   System::Call "Kernel32::GlobalMemoryStatusEx(i r1)"
   # get
   System::Call "*$1(i.r2, i.r3, l.r4, l.r5, l.r6, l.r7, l.r8, l.r9, l.r10)"
   # free
   System::Free $1
   System::Int64Op $4 / 1024
   Pop $4
   System::Int64Op $5 / 1024
   Pop $5
   # print
   ; DetailPrint "Structure size: $2 bytes"
   ; DetailPrint "Memory load: $3%"
   DetailPrint "Total physical memory: $4 bytes"
   DetailPrint "Free physical memory: $5 bytes"
   ; DetailPrint "Total page file: $6 bytes"
   ; DetailPrint "Free page file: $7 bytes"
   ; DetailPrint "Total virtual: $8 bytes"
   ; DetailPrint "Free virtual: $9 bytes"
FunctionEnd
!macroend

; Fonction de cryptage des chaînes de caractères.
; -------------------------------------------------
!macro CryptString un
Function ${un}CryptString

VAR /GLOBAL varInput
VAR /GLOBAL indexI
VAR /GLOBAL indexJ

  ; Written by rgallou 2009-04-17
  ; USAGE
  ; Push cle à crypter
  ; Push N° d'indice de ligne dans le fichier de mots de passe (entête de ligne compris).
  ; Push N° d'indice de colonne du mot à crypter.
  ; Call CryptString
  ; Pop $R0 $4
   Pop $3
   Pop $2
   Pop $1
   ; Copie de la valeur de la chaine
   StrCpy $varInput $1
   StrCpy $indexI $2
   StrCpy $indexJ $3
   ; Récupération de la longueur de la chaîne.
   StrLen $R4 $varInput
   ; Initialisation du pointeur de caractères.
   StrCpy $R3 0
   ; Cryptage de la chaîne qui sera décryptée par MatLab
   ; Val. i à la puis. 3
   IntOp $1 $indexI * $indexI
   IntOp $1 $1 * $indexI
   ; Initialisation de la variable de sortie
   StrCpy $4 ""
   ${Do}
        ; Extraction du pointeur de caractères
        StrCpy $R2 $varInput 1 $R3
        ; Incrémentation du pointeur de caractères.
        IntOp $R3 $R3 + 1
        ; Transcodage en ASCII de la chaîne.
        ${CharToASCII} $0 $R2
        ; Prise en compte du j
        IntOp $0 $0 + $indexJ
        IntOp $0 $0 * 2
        ; Formattage complet du car.
        Intop $0 $0 - $1
        ; MessageBox MB_OK "Caractère crypté: $0"
        ; Concaténation de la chaîne en sortie.
        StrCpy $4 "$4 $0"
   ${LoopUntil} $R3 >= $R4
   Push $4
FunctionEnd
!macroend

!endif ; _Utils_nsh
