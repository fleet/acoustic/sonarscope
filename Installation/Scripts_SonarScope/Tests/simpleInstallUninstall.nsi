
!include "C:\SonarScope\SScTbx\Installation\Scripts_SonarScope\Header\MacroFindProc.nsh" ; Declarations de fonctions particulieres

# define installer name
OutFile "installer.exe"

# set desktop as install directory
InstallDir C:\Temp\testDetectExe


# default section start
Section

Strcpy $1 "Thanks to download MCR since the link MCR MatLab support. Your configuration for the Release is : R2017a(v92) -Win64."
MessageBox MB_OK $1
; MessageBox MB_YESNO|MB_ICONQUESTION "Open MCR MatLab support website?" IDNO +2
; ExecShell open "https://fr.mathworks.com/products/compiler/matlab-runtime.html"


# define output path
SetOutPath $INSTDIR

# specify file to go in output path
File test.txt

# define uninstaller name
WriteUninstaller $INSTDIR\uninstaller.exe


#-------
# default section end
SectionEnd

# create a section to define what the uninstaller does.
# the section will always be named "Uninstall"
Section "Uninstall"


Var /GLOBAL processFound
!insertmacro FindProc $processFound "SonarScope.exe"
StrCmp $processFound 1 notRunning
    MessageBox MB_OK|MB_ICONEXCLAMATION "SonarScope.exe is running. Please close it first" /SD IDOK
    Abort
notRunning:

# Always delete uninstaller first
Delete $INSTDIR\uninstaller.exe

# now delete installed file
Delete $INSTDIR\test.txt

SectionEnd