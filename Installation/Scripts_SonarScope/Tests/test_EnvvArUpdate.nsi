!include "LogicLib.nsh"
!include "..\Header\EnvVarUpdate.nsh" #download http://nsis.sourceforge.net/mediawiki/images/a/ad/EnvVarUpdate.7z


Section "Add to path"
  Push $INSTDIR
  Call AddToPath

  Push "LIB"
  Push "c:\Mylibrary\lib"
  Call AddToEnvVar

  ;likewise AddToPath could be
  Push "PATH"
  Push $INSTDIR
  Call AddToEnvVar

SectionEnd

# ...

Section "uninstall"
  # ...
  Push $INSTDIR
  Call un.RemoveFromPath
  # ...
  Push "LIB"
  Push "c:\Mylibrary\lib"
  Call un.RemoveFromEnvVar

  ;Likewise RemoveFromPath could be
  Push "PATH"
  Push $INSTDIR
  Call un.RemoveFromEnvVar

SectionEnd