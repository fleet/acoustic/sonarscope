; example1.nsi
;
; This script is perhaps one of the simplest NSIs you can make. All of the
; optional settings are left to their default settings. The installer simply 
; prompts the user asking them where to install, and drops a copy of example1.nsi
; there. 

;--------------------------------
VAR varFlagMCR             ; D�tection ou non du bon MCR
VAR varMCRVersion          ; N� de Version du MCR, fonction de la version MATLAB (forme vxy).
VAR varMCRVersion2         ; N� de Version du MCR, fonction de la version MATLAB (forme x.y).
VAR pathMCRVersion         ; Chemin du MCr dans la base de registre.

;GLUGLUGLU !include Header\AddToPath.nsh       ; D�clarations sp�cifiques pour l'installation de GMT.
!include ..\Header\EnvVarUpdate.nsh
!include ..\Header\WriteToFile.nsh
!include ..\Header\SSCMainFunctions.nsh
!include ..\Header\Utils.nsh               ; D�clarations d'utilitaires
!include FileFunc.nsh
!include C:\SonarScope\SScTbx\Installation\Scripts_SonarScope\Header\SSCMessages.nsh      ; D�clarations des messages multi-langues
!include ..\Header\GetWindowsVersion.nsh   ; Permet de l'identification d ela version Windows.
!include LogicLib.nsh                   ; Provides the use of various logic statements
!include WordFunc.nsh

!insertmacro GetTime
!insertmacro IdentifyMatLabVersion ""

; The name of the installer
Name "Example1"

; The file to write
OutFile "test_SETPATH.exe"

; The default installation directory
InstallDir "C:\SonarScopeTbx\Installation\Scripts_SonarScope\Tests"

; Request application privileges for Windows Vista
; RequestExecutionLevel user

;--------------------------------

; Pages

Page directory
Page instfiles


; The stuff to install
Section "" ;No components page, name is not important

        ; Identification de la version MatLab et recopie.
        Push ${VER_MATLAB}
MessageBox MB_OK "Dollar  {VER_MATLAB} :  ${VER_MATLAB}"
        Call identifyMatLabVersion
        Pop $0
        StrCpy $varMCRVERSION $0
        Pop $1
        StrCpy $varMCRVERSION2 $1
        Pop $2
        StrCpy $pathMCRVersion $2

    ; Identification de la version Windows (Ex : Windows 8 --> $R0 = 8.1)
    ${GetWindowsVersion} $R1

      SetRegView 64
    ; Comparaison case-insensitive string tests (si la version est sup�rieure � Windows Seven)
    ${If} $R1 == "XP"
           ReadRegStr $0 HKLM "SOFTWARE\MATLAB\MCRPathDir\" "MCRPathDir"
           ; Comptage de l'occurence du num de version dans le path
           ; Retourne $0 si il ne trouve pas l'occurrence.
           ${WordFind} "$0" $varMCRVersion "#" $varFlagMCR
    ${Else}
          ReadRegStr $0 HKLM "SOFTWARE\MathWorks\$pathMCRVersion\$varMCRVERSION2" "MATLABROOT"
          ${If} $0 == ""
               ; For�age du Test pour dire que MCR est � installer.
               StrCpy $varFlagMCR ""
          ${EndIf}
    ${EndIf}

    SectionGetFlags ${SEC_MCR} $R0
    IntOp $R0 $R0 & ${SF_SELECTED}
    ${If} $R0 == ${SF_SELECTED}
        ${If} $0 == $varFlagMCR
           MessageBox MB_ICONEXCLAMATION  "Dollar Warn No MCR $(warn_NO_MCR)"
        ${ElseIf} $0 != ""
           MessageBox MB_ICONEXCLAMATION  "Dollar Warn MCR $(warn_MCR)"
           SectionSetFlags ${SEC_MCR} 0
        ${EndIf}
    ${EndIf}

  ; Utilit� de ce test ???
  ; Test de pr�sence du r�pertoire de Data avant mise � jour en base de registre.
 
/*
MessageBox MB_OK "Avant RemoveFromPath"
        ; Effacement pr�alable des variables du path.
        Push $INSTDIR
        Call RemoveFromPath

MessageBox MB_OK "Avant AddToPath"
        Push $INSTDIR
        Call AddToPath
MessageBox MB_OK "Avant RemoveFromEnvVar"
        StrCpy $R2 "MCR_CACHE_ROOT"
        StrCpy $R0 "C:\Temp"
        Push $R2
        Push $R0
        Call RemoveFromEnvVar
*/
; Tentative d'�criture du PATH (si celui-ci est inf�rieur � 1024.
;GLUGLUGLU ${EnvVarUpdate} $0 "PATH" "A" "HKLM" "C:\Program Files\Windows Resource Kits\Tools"

; Ecriture de la mise � jour du PATH dans un fichier .bat.
${GetTime} "" "L" $0 $1 $2 $3 $4 $5 $6

/*
ReadRegStr $0 HKLM "SYSTEM\CurrentControlSet\Control\Session Manager\Environment" "Path"
ExecWait "cmd /k; del $TEMP\sscExternal_setPATH.bat & Exit"
${If} $0 == ""
      ${WriteToFile} "$TEMP\sscExternal_setPATH.bat" "rem Update Path after SSC External install$\r$\n"
      ${WriteToFile} "$TEMP\sscExternal_setPATH.bat" "rem ======================================$\r$\n"
      ${WriteToFile} "$TEMP\sscExternal_setPATH.bat" "rem Date : $3 $2/$1/$0  $4:$5:$6$\r$\n"

${EndIf}

${WriteToFile} "$TEMP\sscExternal_setPATH.bat" "SET path=C:\Program Files\Windows Resource Kits\Tools$\r$\n"
${WriteToFile} "$TEMP\sscExternal_setPATH.bat" "$\r$\n"

${If} $0 == ""
MessageBox MB_OK $0
        ExecWait "cmd /k;$TEMP\sscExternal_setPATH.bat"
${EndIf}
*/

/* MessageBox MB_OK "Avant RemoveFromEnvVar"
        StrCpy $R0 "C:\Temp"
        StrCpy $R2 "MCR_CACHE_ROOT"
        Push $R2
        Push $R0
        Call AddToEnvVar
        */

SectionEnd ; end the section
