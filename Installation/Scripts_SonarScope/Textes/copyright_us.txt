Please visit : 
http://flotte.ifremer.fr/fleet/Presentation-of-the-fleet/On-board-software/GLOBE/Download/Academic-Licence

Content of the academic Licence
-------------------------------

Conditions of the licence

The user of the Software pledges to comply with the conditions defined hereinafter in the software licence.

SOFTWARE LICENCE

The Licensee is granted a non-exclusive and non-transferable right to use the Software (hereafter called �The Software�).

The present Software Licence is limited to the Licensee which accepts the Software �as is�. The Software must be used onto Licensee�s equipments only and can not be used by persons other than these working for and who are employed by the Licensee.

By the present Licence , the Licensee shall have access to the only executable program. In no case, the present licence agreement shall give access right to the source code.

The Licensee is not entitled to modify, adapt, translate or disassemble the Software.

The Licensee acknowledges that all rights in the Software, including the rights to modify the Software and to incorporate in it other programs, remain with IFREMER.

The Software shall be used by the Licensee for internal research purposes only. Any commercial use or exploitation of the software is not allowed without IFREMER's authorisation.

The licence is granted free of charge.

Under no circumstances shall the Agreement imply that IFREMER agrees to provide free technical assistance or maintenance services for the Software.

The Software is subject to Intellectual Property rights which belong exclusively to IFREMER. The Licensee acquires under the present agreement no title, right or interest in the Software other than rights granted therein. The Licensee recognises those rights and the property of IFREMER, and thus warranties IFREMER from any action in breach of those rights. The Licensee acknowledges that all rights in the Software, including the rights to modify the Software and to incorporate it in other programs, remain with IFREMER.

The Licensee is not entitled to modify, adapt, translate or disassemble the Software. The Licensee shall not remove or cover any proprietary rights such as trademarks, copyrights symbols or any other logo which are displayed on the screen of the computer onto which the Software shall be installed.

IFREMER shall be mentioned in all publications showing results obtained by using the Software, and in all publications or communications based on these results, the Licensee shall mention: �Ifremer Software� written in a readable manner.

Duration

The present Licence shall be effective from the date of acceptance by the Licensee and remain in force until copyright on the Software run out, or following one month delay after communication of the licensee decision to give up any use of the Software.

Immediately upon termination of this licence for any reason, the Licensee shall cease using the Software in any form.

Assistance services

Upon request of the Licensee, IFREMER may provide:
- email assistance during installation in the Licensee�s premises,
- technical support by email,
- training session,
- next upgrades of the Software.

The financial terms and conditions of such assistance, maintenance and/or training sessions, shall then be set forth in a specific agreement.

Confidentiality

The Software shall be delivered to the Licensee on a confidential basis and the Licensee is responsible for taking all necessary steps to ensure the confidentiality of the Software programme. In particular, the Licensee shall not disclose or otherwise make available the Software programme to a third party, other than Licensee�s employee for the use permitted under the agreement, without written consent from IFREMER.

Liability

THE SOFTWARE IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED. IFREMER FURTHER DISCLAIMS ALL WARRANTIES INCLUDING BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NON-INFRINGEMENT.
THE ENTIRE RISK ARISING OUT OF THE USE OF THE SOFTWARE AND DOCUMENTATION REMAINS WITH THE LICENSEE.

IN NO EVENT SHALL IFREMER OR ANYONE INVOLVED IN THE CREATION OR DOCUMENTATION OF THE SOFTWARE BE LIABLE FOR ANY DAMAGES WHATSOEVER (INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF BUSINESS PROFITS, INTERRUPTION OF BUSINESS ACTIVITY, LOSS OF BUSINESS INFORMATION, OR OTHER MONETARY LOSS) ARISING OUT OF THE USE OR INABILITY TO USE THE SOFTWARE OR DOCUMENTATION.

This Licence shall be governed by and construed in accordance with French Laws.
Any litigation shall be brought before French Courts.
