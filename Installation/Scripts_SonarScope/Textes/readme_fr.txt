SonarScope R2011b
===============


Quelques unes des modifications principales depuis la version pr�c�dente
======================================================
Depuis 1.7
- R�f�rencement de Sondeurs via nouvelle architecture (fichiers XML dans IfremerToolboxResources)
- Fonctionnalit�s nouvelles sur les Figures et Plot (modif cde titre) ...
- Export-g�n�ration des donn�es multidata pour 3DViewer
- Fonctions de fusion, split, merge des fichiers ALL et WCD + fonction de Split d'un fichier SDF.
- d�ploiement du GMT 5.0
- SEGY  : d�modulation
- traitement des donn�es de BSCoor en ent�te des fichiers ALL
- ajout de datagrammes NetWork Attitude, Qualify Factor

Depuis 1.6
- Introduction de proc�dures de lecture en Java des formats RESON, XTF, SimRAD,
- Modification de la biblioth�que graphique (composants et applications),
- Ajout de Pames dans les menus,
- Lecture des formats de sondeurs RDF
- Installation de Google Earth et Nasa World Wind (et les librairies de tuilage).

Depuis V1.5
- Lecture des formats XTF
- Relocalisation du Resources.ini
- Modification des dialogues GMT,
- Int�gration de la librairie CIB_DOU qui appelle la librarie NetCDF d'UNIDATA,
- modification des scripts d'installation (r�f�rence par version install�e, non d�installation 
  de GMT),
- cette version a �t� r�alis� avec MatLab2006b. La version du RunTime (MatLab component runTime) est 7.5. 

Depuis V1.2
- Cr�ation du r�pertoire IfremerToolBoxResources contenant les fichiers n�cessaires au 
fonctionnement de SonarScope (ic�nes, .html, ...)
- D�solidarisation de MCR de SonarScope
- Sonarscope : changements de noms de propri�t�, ...

Depuis V1.1
- Int�gration du Module GMT,
- diff�rentes fonctionnalit�s impl�ment�es pour le NIWA


Installation SonarScope
=======================

Si l'installation s'est correctement termin�e, vous devez retrouver les �l�ments 
suivants :

- l'arborescence des data ToolBox comprenant les r�pertoires Public, Etopo, Private,
PublicDelao, Public Sonar,

- un r�pertoire d'installation SonarScope (par d�faut C:\SonarScope) comprenant
l'ex�cutable SonarScope, l'ic�ne associ� et ce fichier ReadMe. La premi�re utilisation
de SonarScope va d�boucher sur la cr�ation d'un r�pertoire SonarScope_MCR (MatLab component runTime).
Les resources sont install�es sous le r�pertoire d'installation par d�faut.

- sous le Menu "D�marrer de Windows", le sous-menu SonarScope propose de lancer
le logiciel ou de le d�sinstaller.

- sous C:\TEMP, un fichier ReIniMatLab.ini qui contient des variables d'environnement 
utilis� sous MatLab qui permettent la configuration du logiciel (langage utilis�, 
les visualisateurs) ou la localisation des r�pertoires de data.
Exemple de contenu :
	[ENV]
	IfrTbx=D:\SonarScopeTbx\
	IfrTbxResources=C:\SonarScope-R2011b-64Bits-2013-03-22\IfremerToolboxResources
	IfrTbxLang=US
	IfrTbxUseNewFormatsS7k=
	IfrTbxUseClickRightForFlag=Off
	IfrTbxLevelQuestions=2
	IfrTbxGraphicsOnTop=2
	IfrTbxUseLogFile=Off
	IfrTbxRepDefInput=F:\SonarScopeData\Tests_Format_Kongsberg\2040C
	IfrTbxRepDefOutput=C:\Temp
	IfrTbxTempDir=C:\Temp
	[SYSTEM]
	SizePhysTotalMemory=500000
	NUMBER_OF_PROCESSORS=7

 La variable IfrTbxResources n'est pas significative car ne contient pas la bonne valeur de r�pertoire de resources dans les 
 versions d�ploy�es.
 - les modules et les fonctions GMT se retrouvent sous C:\GMT. En cons�quence, le chemin global ainsi
	que des variables d'environnement ont �t� impact�es.

	Path = ...; %GMTHOME%\BIN;%NETCDF% ; C:\Program Files\Ghostgum\gsview
	GMTHOME= C:\GMT
	HOME= C:\GMT

Pr�-requis
==========
V�rifiez si la Machine Virtuelle Java est install�e sur votre machine. Si
elle ne l'est pas, installez la sans quoi l'installation du MCR ne se
fera pas.


Probl�mes �ventuels
===================

- V�rifier l'installation sous le r�pertoire d'installation choisi des fichiers et des r�pertoires 
n�cessaires au fonctionnement.

- Si �a ne marche toujours pas, Cr�er une fen�tre de commande DOS (ex�cuter
>cmd), aller sur le r�pertoire du fichier SonarScope.exe � l'aide des
commandes cd et dir. Taper SonarScope.exe, faire une copie des messages
d'erreur et me les envoyer (augustin@ifremer.fr)

- si GMT ne fonctionne pas, v�rifier son installation sous C:\GMT (ou une arborescence �quivalente).
  Attention: v�rifier la compatibilit� de GMT avec une version existante pr�alable.
