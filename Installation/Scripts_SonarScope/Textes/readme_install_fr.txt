================ 
= Installation =
================

Contact :
---------
Jean-Marie AUGUSTIN
IFREMER - NSI/AS
FRANCE
02 98 22 47 03



L'installation compl�te de SonarScope passe par le d�ploiement de plusieurs parties:

- SonarScope		: version ex�cutable de d�ploiement

- SonarScope-Data	: donn�es de SonarScope, n�cessaires � un fonctionnement complet de SonarScope (mod�le de formats de fichiers)

- SonarScope-External   : moteur Matlab Component RunTime + outils annexes.

- SonarScope-Doc	: documentation de SonarScope (en cours d'am�nagement).


1. SonarScope s'installe dans un r�pertoire dont le nom reprend la version et la date d'installation.

Ce r�pertoire comprend �galement un r�pertoire de ressources (icones, ...) qui contient lui-m�me le 
fichier de configuration de SonarScope. Voici un exemple ci-dessous:

***************************************************
[ENV]
IfrTbx=C:\SonarScopeTbx
IfrTbxResources=C:\SonarScope-R2010a-64Bits-2011-11-26\SScTbxResources
IfrTbxLang=FR
IfrTbxUseNewFormatsS7k=Off
IfrTbxUseNewFormatsXtf=Off
IfrTbxUseJava=Off
IfrTbxUseClickRightForFlag=Off
IfrTbxLevelQuestions=2
IfrTbxGraphicsOnTop=2
IfrTbxUseLogFile=Off
IfrTbxRepDefInput=I:\HAC\Fichiers_HAC
IfrTbxRepDefOutput=C:\Temp\A
IfrTbxTempDir=C:\Temp
IfrDpy=C:\IfremerDeploy
[SYSTEM]
SizePhysTotalMemory=500000
NUMBER_OF_PROCESSORS=4
***************************************************

Sont install�s �galement diff�rents raccourcis d'acc�s direct � SonarScope (bureau, programmes, ...)
La documentation est localis�e par ce fichier de ressources (env. LINUX) ou en base de
registre (WIn 32 et 64).

2. 
Les donn�es s'installent depuis leur Set Up et une unit� de stockage (disque, cl� , ...) comprenant les donn�es fournies par IFREMER.
Selon l'environnement, les ressources sont localis�es depuis la base des registres.

3.
Le MatLab Component RunTime, MCR (obligatoire) et les outils annexes s'installent via le SetUp d'installation des annexes.
Les annexes comprennent des visualisateurs, des outils de scripts. (Voir Documentation pour le d�tail).

Le MCR d�pend de la plate-forme cible d'exploitation de SonarScope : ex, 32 bits ou 64 bits, LINUX ou Windows.

4.
La documentation est essentielle � SonarScope pour l'assistance � l'utilisateur.
 
5. 
La d�sinstallation est r�alis�e dans via l'ex�cutable uninstall SonarScope.exe qu'il suffit d'activer. 
Nb : les logiciels annexes ind�pendant de SonarScope une fois install�s ne sont pas compris dans cette d�sinstallation.


