================ 
= Installation =
================

Contact :
---------
Jean-Marie AUGUSTIN
IFREMER - NSI/AS
FRANCE
02 98 22 47 03


The complete installation of SonarScope passes through the deployment of several parts:

- SonarScope: actual deployment

- SonarScope-Data: SonarScope data necessary to complete an operation SonarScope (model file formats)

- SonarScope-External: Matlab engine Component RunTime + tools annexes.

- SonarScope-Doc: documentation SonarScope.


1. SonarScope moves into a directory whose name takes the version and date of installation.

This directory also includes a directory of resources (icons, ...) which contains itself
configuration file SonarScope. Here is an example below:


***************************************************
[ENV]
IfrTbx=C:\SonarScopeTbx
IfrTbxResources=C:\SonarScope-R2010a-64Bits-2011-11-26\SScTbxResources
IfrTbxLang=FR
IfrTbxUseNewFormatsS7k=Off
IfrTbxUseNewFormatsXtf=Off
IfrTbxUseJava=Off
IfrTbxUseClickRightForFlag=Off
IfrTbxLevelQuestions=2
IfrTbxGraphicsOnTop=2
IfrTbxUseLogFile=Off
IfrTbxRepDefInput=I:\HAC\Fichiers_HAC
IfrTbxRepDefOutput=C:\Temp\A
IfrTbxTempDir=C:\Temp
IfrDpy=C:\IfremerDeploy
[SYSTEM]
SizePhysTotalMemory=500000
NUMBER_OF_PROCESSORS=4
***************************************************

Are also installed, shortcuts direct access to SonarScope (desktop, programs, ...)
The documentation is localised from this resource file (approx. LINUX) or base
Register (Win 32 and 64).


2. 
Data and resources are localised since Base Register are installed from the duplication of Data disk from support provided by IFREMER.

The SetUP installation data allows to update the base.


3.
The Component MatLab RunTime, MCR (mandatory) and tools annexes SetUp settled through the installation of annexes.
The annexes include viewers, tools scripts. (See documentation for details).

The MCR depends on the target platform operating SonarScope: ex, 32 bit or 64 bit, Linux or Windows.
Fran�ais
		
4.
The documentation is essential to SonarScope to assist the user.
This section is under development.

 
5. 
Uninstallation is performed in via the executable uUninstall SonarScope.exe.
Nb: SonarScope annexes softwares , independent once installed, are not included in this uninstallation.

