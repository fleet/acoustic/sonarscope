SonarScope R2011b
===============


Few principals modifications since the previous version
========================================
Since 1.7
- Referencing Sounders via new architecture (XML files in IfremerToolboxResources)
- New features in Figures and Plot (Modified title) ...
- Export-generation multidata for 3DViewer
- Functions split, merge of ALL files and WCD + function Split an SDF file.
- Deployment GMT 5.0
- SEGY: demodulation
- Data processing BSCoor on the header of ALL files
- Added datagrams NetWork Attitude Qualify Factor
Since V1.6
- Introduction of procedures for reading in Java files formats such RESON, XTF, SimRAD,
- Modification of the graphic library (components and applications),
- Addition of Pames in the menus,
- Format RDF sounder reading.
- Installation de Google Earth and Nasa World wind (and tiling librairies)


Since V1.5
  - Format XTF reading.
  - Change name and place of Resources.ini (ex-ReinitMatLab.ini)
  - Modification of dialogues GMT,
  - Integration of the libraryCIB_DOU which calls the library NetCDF of UNIDATA,
  - modification of scripts of installation (reference by version installed, not uninstallation of GMT),
  - this version was carried out with MatLab2006b. The version of RunTime (MatLab component runTime) is 7.5. 

Since V1.2
- Creation of the IfremerToolBoxResources repertory containing the files mandatory
for the operation of SonarScope (icons, html...) , 
- Dissociation of MCR of SonarScope ,
- Sonarscope:  renamings of properties...

Since V1.1
- GMT Module integration,
- some fonctions developed for NIWA organism.


SonarScope Installation 
=======================
If the installation correctly finished, you must find the following elements:
 - the tree structure of dated ToolBox including the directories  
   Public, Etopo, Private, PublicDelao, Public Sonar,

 - a Directory of SonarScope installation (by C:\SonarScope defect) including achievable
   SonarScope, the icon associated and this ReadMe file. The first use of SonarScope will 
   lead to the creation of a SonarScope_MCR repertory (MatLab component runTime).
   The resources are installed under the default directory of installation.

 - under the Menu "To start of Windows", the SonarScope sub-menu proposes 
   to launch the software or of the uninstaller.

 - under C:\TEMP, a ReIniMatLab.ini file which contains variables of environment 
   used under MatLab which allow the configuration of the software (language used, 
   visual displays) or the localization of the repertories of dated.  
   Example of contents:
	[ENV]
	IfrTbx=D:\SonarScopeTbx\
	IfrTbxResources=C:\SonarScope-R2011b-64Bits-2013-03-22\SScTbxResources
	IfrTbxLang=US
	IfrTbxUseNewFormatsS7k=
	IfrTbxUseClickRightForFlag=Off
	IfrTbxLevelQuestions=2
	IfrTbxGraphicsOnTop=2
	IfrTbxUseLogFile=Off
	IfrTbxRepDefInput=F:\SonarScopeData\Tests_Format_Kongsberg\2040C
	IfrTbxRepDefOutput=C:\Temp
	IfrTbxTempDir=C:\Temp
	[SYSTEM]
	SizePhysTotalMemory=500000
	NUMBER_OF_PROCESSORS=7

IfrTbxResources variable is not significant because it does not have the good content in deployment version of SonarScope. 
 versions d�ploy�es.
 
 - GMT modules and functions under C:\GMT. for it, the global Path and some environment variables has 
	been impacted:
	Path = ...; %GMTHOME%\BIN;%NETCDF% ; C:\Program Files\Ghostgum\gsview
	GMTHOME= C:\GMT
	HOME= C:\GMT



Pre-queries
===========
Before launching the MCR installer, control if the Virtual Machine Java is 
installed on your machine.  If it is not it, install it without what the 
installation of the MCR will not be done.



Issue to problems
=================

- Check the installation under the selected repertory of installation of the files and the repertories
  necessary to operation.

- If that still does not go, open a window of DOS (to carry out > cmd), 
   to go on the directory of the SonarScope.exe file using the orders Cd and to dir.
   Type SonarScope.exe, to make a copy of the error messages and to send them to me :
   augustin@ifremer.fr

- if GMT does not function, to check its installation under C:\GMT (or an equivalent tree structure).
  Caution: to check the compatibility of GMT with a preliminary existing version. 
