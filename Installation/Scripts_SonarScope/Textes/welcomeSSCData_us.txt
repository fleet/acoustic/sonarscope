This wizard installs the SonarScope Samples Data on your computer.

Data is composed like below:
-Public:containing heterogenous data (images, not specific for Sonar),
-Private: containing Private data issues from specific studies,
-Sonar:containing Public Data issues from specific studies,
-Etopo:containing morphological data in diffrents resolutions.
-Levitus:containing Levitus Data in NetCdf or MatLab format.

Each Directory is decomposed in 2 levels : generic (1) and detailed (2), except for Levitus which 1 is for .mat, and 2 for .netcdf.

This assistant of installation was carried out with NSIS (of NullSoft).