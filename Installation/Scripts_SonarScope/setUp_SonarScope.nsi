 ; Script d'installation NSIS de SonarScope et sa documentation.
; Ce script installe :
;  - le logiciel SonarScope,
;  - la documentation de SonarScope
; Developpe sous NSIS avec l'editeur HM Edit de HM Soft.
;
; Date      : 18/04/2008
; Auteur    : GLU (ATL)
; Modification : -
; Creation : 09/2005 - GLU, ATL (02 98 05 43 21).
; ================================================================================================

; Definition de Macros de compilation
!define PRODUCT_NAME      "SetUpSonarScope"
!define PRODUCT_VERSION   "${VER_MATLAB}"     ; VER_MATLAB est defini dans les profils de compilation.
!define PRODUCT_PUBLISHER "IFREMER NSE-AS"
!define PRODUCT_WEB_SITE  "http://www.ifremer.fr"
!define GDAL              "1.7"
!define ASCIIDOC_EXE      "1.6.2"

VAR varNowTitre         ; Formattage de la date pour le titre des fenetres.
VAR varDateValid        ; Date de validite de l'application.
VAR varDestinataire     ; Nom du destinataire.
VAR varService          ; Service du destinataire.
VAR varCle              ; Cle cryptee associee au destiantaire.
VAR varDummy            ; Variable utile.
VAR varDummy2           ; Variable utile.
VAR varSoftDirDest      ; Repertoire d'installation du logiciel
VAR varTempDirDest      ; Repertoire des donnees temporaires
VAR varFlagAcceptMsg    ; Flag d'acceptation par l'utilisateur de l'envoi de message
VAR varServerAddress    ; Adresse du serveur sortant de l'utilisateur
VAR varPortNumber       ; N� de port du serveur sortant de l'utilisateur
VAR varHdlWindow        ; Handle du window en cours.

; Test du profil pour eviter d'etre en mode Defaut. Ce profil ne peut etre supprime.
!if ${TARGET} == Default
    !echo "Veuillez choisir un profil de compilation valide !"
    !error "Arret de compilation forcee!"
!endif

; Message Bas de page des fenetres
BrandingText "$(info_welcomeSSCTitle) ${PRODUCT_NAME}-${PRODUCT_VERSION}"


; Declarations des Include
!include FileFunc.nsh
!include Header\SSCMessages.nsh      ; Declarations des messages multi-langues
!include Header\SSCMainFunctions.nsh ; Declarations de fonctions particulieres
!include Header\Utils.nsh           ; Declarations d'utilitaires
;!include Header\AddToPath.nsh      ; Declarations specifiques pour la modification du Path.
!include Header\EnvVarUpDate.nsh    ; Declarations specifiques pour la modification du Path.
!include Header\CharToASCII.nsh     ; Declarations specifiques le decodage de chaines.
!include Header\FileAssociation.nsh ; Permet l'association entre Extension et Application.
!include Header\MacroFindProc.nsh   ; Macro de detection de processus.

!include WordFunc.nsh               ; Utilitaires de gestion des strings
!include LogicLib.nsh               ; Provides the use of various logic statements

; Macro de WordFunc.nsh
!insertmacro WordFind

; Macro de FileFunc.nsh
!insertmacro GetTime

; Macro de Utils.nsh
;!insertmacro StrRep ""
!insertmacro GetPhysicalMemory64 ""
!insertmacro GetPhysicalMemory ""

; Macro de Utils.nsh
!insertmacro CryptString ""

!define /date NOW "%Y%m%d"           ; Voir formats possibles dans strftime de MSDN.
!define /date NOW_us "%Y-%m-%d"
!define /date NOW_fr "%Y-%m-%d"

; *******************************************************************
; UMUI 1.00b1 compatible -------
!include "UMUI.nsh"
!include "Sections.nsh"


; Definition pour la modernisation des pages
!insertmacro MUI_DEFAULT UMUI_LEFTIMAGE_BMP ".\Resources\SONARSCOPE_UMUI.bmp"

; Definition pour la page de bienvenue.
!define MUI_WELCOME_BITMAP ".\Resources\SONARSCOPE.bmp"
!define MUI_WELCOME_BITMAP_NOSTRETCH
!define MUI_WELCOMEFINISHPAGE_BITMAP ".\Resources\SONARSCOPE.bmp"
!define MUI_WELCOMEFINISHPAGE_BITMAP_NOSTRETCH

; Page d'accueil
!define MUI_ICON "${NSISDIR}\Contrib\Graphics\Icons\modern-install.ico"
!define MUI_WELCOMEPAGE_TITLE "$(info_welcomeSSCTitle) - SonarScope Software"
!define MUI_WELCOMEPAGE_TEXT $(info_welcomeSSC)
!define MUI_PAGE_CUSTOMFUNCTION_PRE welcome_pre
; Suppression du sous-titre de chaque page - redondant avec le titre.
!define MUI_PAGE_HEADER_SUBTEXT ""

!if ${TARGET} != Test
    !insertmacro MUI_PAGE_WELCOME
!endif

;1ere Page 
!if ${TARGET} != Test
     Page custom FctInputGrantKey "" $(info_key_custom)
!endif

; Parametrage Modern Interface
!define MUI_ABORTWARNING
!define MUI_HEADERIMAGE
!define MUI_HEADERIMAGE_BITMAP ".\Resources\SONARSCOPE_header.bmp"
!define MUI_HEADERIMAGE_BITMAP_NOSTRETCH
!define MUI_HEADERIMAGE_UNBITMAP ".\Resources\SONARSCOPE_header.bmp"
!define MUI_HEADERIMAGE_UNBITMAP_NOSTRETCH
!define MUI_HEADERIMAGE_RIGHT


; Page dossier d'installation
!if ${TARGET} != Test
    !define MUI_DIRECTORYPAGE
    !define MUI_DIRECTORYPAGE_TEXT_TOP $(info_dirSoftTop)
    !define MUI_DIRECTORYPAGE_TEXT_DESTINATION $(info_dirSoftDest)
    !define MUI_DIRECTORYPAGE_VERIFYONLEAVE
    !define MUI_PAGE_CUSTOMFUNCTION_PRE DirectoryPre
    !define MUI_PAGE_CUSTOMFUNCTION_SHOW DirectoryShow
    !define MUI_PAGE_CUSTOMFUNCTION_LEAVE DirectoryLeave
    !insertmacro MUI_PAGE_DIRECTORY
!endif

; Page de saisie de mail et envoi de message
Page custom Pre_SSCConfigMail_Page SSCConfigMail_Page $(info_SSCconfigMail)


!if ${TARGET} != Test
    !define MUI_DIRECTORYPAGE_TEXT_TOP $(info_dirTempTop)
    !define MUI_DIRECTORYPAGE_TEXT_DESTINATION $(info_dirTempDest)
    !define MUI_DIRECTORYPAGE_VERIFYONLEAVE
    !define MUI_PAGE_CUSTOMFUNCTION_PRE DirectoryPre
    !define MUI_PAGE_CUSTOMFUNCTION_SHOW DirectoryShow
    !define MUI_PAGE_CUSTOMFUNCTION_LEAVE DirectoryLeave
    !insertmacro MUI_PAGE_DIRECTORY
!endif

; Page installation
!insertmacro MUI_PAGE_INSTFILES

; Desinstalleur
!insertmacro MUI_UNPAGE_CONFIRM
!insertmacro MUI_UNPAGE_INSTFILES

; Page de fin
!define MUI_FINISHPAGE_NOAUTOCLOSE
!define MUI_FINISHPAGE_TITLE $(info_finTitre)
!define MUI_FINISHPAGE_LINK "http://www.ifremer.fr"
!define MUI_FINISHPAGE_LINK_LOCATION "http://www.ifremer.fr"
!define MUI_FINISHPAGE_TEXT "$(info_finTexte) ${PRODUCT_NAME}\r\nEnjoy using this application !"
!define MUI_FINISHPAGE_RUN_NOTCHECKED


;!define MUI_FINISHPAGE_RUN
!define MUI_FINISHPAGE_RUN_TEXT $(info_finLaunch)
!define MUI_FINISHPAGE_BUTTON $(info_finBtn)
!insertmacro MUI_PAGE_FINISH

; Selection des langages
!insertmacro MUI_LANGUAGE "English"
!insertmacro MUI_LANGUAGE "French"
!define MUI_LANGDLL_WINDOWTITLE "${PRODUCT_NAME} ${PRODUCT_VERSION} ${NOW_us}"
!define MUI_LANGDLL_INFO $(info_info)
!insertmacro MUI_RESERVEFILE_LANGDLL

; References a� tous les messages Operateurs.
; Macro de SSCMessages.nsh
!insertmacro CustomMsg
; MUI end -------------------


Name "${PRODUCT_NAME} ${PRODUCT_VERSION}, $varNowTitre"
!if ${TARGET} == Test
    OutFile  "${DIR_DEPLOY}\SetUp\Version${PRODUCT_VERSION}\${PRODUCT_VERSION}_${PRODUCT_NAME}_Software_${NOW}_test.exe"   ; nom de l'executable
    InstallDir "C:\SonarScope"   ; repertoire par defaut d'installation
!else
     OutFile  "${DIR_DEPLOY}\SetUp\Version${PRODUCT_VERSION}\${PRODUCT_VERSION}_${PRODUCT_NAME}_Software_${NOW}.exe"   ; nom de l'executable
     InstallDir "C:\SonarScope"   ; repertoire par defaut d'installation
!endif

ShowInstDetails show
ShowUnInstDetails show

Icon          ".\Resources\SonarScope.ico"
UninstallIcon ".\Resources\SonarScope.ico"


; *******************************************************************
; On initialization
Function .onInit

  ; Suppression de la derniere trace.
  Delete $INSTDIR\Install.log

  ; Si TARGET == 64Bit ou Test
  WriteINIstr "$INSTDIR\Install.log" "INSTALL" "Target" "64 Bit"
  SetRegView 64
    
  SetOverwrite try

  ; Verification de l'instance unique de l'installateur
  System::Call 'kernel32::CreateMutexA(i 0, i 0, t "install_SonarScope") i .r1 ?e'
  Pop $R0
  ${If} $R0 != 0
     MessageBox MB_OK $(warn_instance_installer)
     Abort
  ${EndIf}


  !insertmacro IsUserAdmin $0
  ${If} $0 != 1 ; n'appartient pas aux administrateurs
       MessageBox MB_ICONSTOP $(err_UserProfile)
       Quit
  ${Endif}


  ;Affichage de la boa�te de dialogue des Langages avec fora�age de la langue par defaut.
  strCpy $Language ${LANG_ENGLISH}
  ;On force l'sintallation en Anglais !insertmacro MUI_LANGDLL_DISPLAY

  InitPluginsDir
  ${If} $LANGUAGE == ${LANG_FRENCH}
     StrCpy $varNowTitre "${NOW_fr}"
     ; Saisie de la cle
     File /oname=$PLUGINSDIR\SSCInputGrantKey.ini ".\Resources\SSCInputGrantKey_fr.ini"
  ${Else}
     StrCpy $varNowTitre "${NOW_us}"
     ; Saisie de la cle
     File /oname=$PLUGINSDIR\SSCInputGrantKey.ini ".\Resources\SSCInputGrantKey_us.ini"
  ${EndIf}

  ; Saisie de la config d'envoi de mails
  File /oname=$PLUGINSDIR\SSCconfigMail.ini ".\Resources\SSCconfigMail_us.ini"

  StrCpy $INSTDIR "C:\SonarScope-${PRODUCT_VERSION}-${TARGET}-$varNowTitre"   ; repertoire par defaut d'installation

  ; Integration des mots de passe pour l'utilisation de SonarScope
  File /oname=$TEMP\ListePLP.csv ".\Resources\ListePLP_SonarScope.csv"

  ; Recuperation de la date du jour.
  ${GetTime} "" "L" $0 $1 $2 $3 $4 $5 $6

  WriteINIstr "$INSTDIR\Install.log" "INSTALL" "Date" "$3 $2/$1/$0  $4:$5:$6"
  WriteINIstr "$INSTDIR\Install.log" "INSTALL" "Version" "FULL  ${PRODUCT_VERSION}"


FunctionEnd



; *******************************************************************
; Programmation des sections
Section "SonarScope-Application" SEC_SonarScope

        ; Conservation du repertoire d'installation du logiciel.
        SetOutPath $varSoftDirDest
        StrCpy $INSTDIR $varSoftDirDest
        StrCpy $7 $INSTDIR "" 3
        CreateDirectory $INSTDIR

        ; Autorisations a� tous les utilisateurs sur le repertoire d'installation.
        AccessControl::GrantOnFile "$INSTDIR" "(BU)" "FullAccess"
	WriteINIstr "$INSTDIR\Install.log" "INSTALL" "SonarScope" "Yes"

        !if ${TARGET} != Test
            File /r "${DIR_DEPLOY}\${VER_MATLAB}\SonarScope.exe"

	    File /r ".\Resources\SonarScope.ico"
	    File /r "${SRC_SSC}\${VER_SSC}\Installation\Viewers\winopen.exe"
	    File /r "${SRC_SSC}\${VER_SSC}\Installation\Viewers\DataView.exe"

           ; Creation de l'association entre Fichiers S7K et
            ${registerExtension} "$INSTDIR\DataView.exe" ".s7k" "S7K_File"

	    ;Creation des raccourcis de l'application
	    setShellVarContext all ; Propagation du contexte a� tous les utilisateurs.

	    ;Creation des raccourcis de l'application
            CreateShortcut "$DESKTOP\$7.lnk" "$INSTDIR\SonarScope.exe" "" "$INSTDIR\SonarScope.ico"
	    CreateShortcut "$QUICKLAUNCH\$7.lnk" "$INSTDIR\SonarScope.exe" "" "$INSTDIR\SonarScope.ico"
	    CreateDirectory "$SMPROGRAMS\SonarScope\"
	    CreateShortcut "$SMPROGRAMS\SonarScope\$7.lnk" "$INSTDIR\SonarScope.exe" "" "$INSTDIR\SonarScope.ico"

            ; Copie des developpements Java.
            CreateDirectory "$INSTDIR\extern\java\lib"
	    SetOutPath "$INSTDIR\extern\java\lib"
            
            File /r "${SRC_SSC}\${VER_SSC}\ifremer\extern\java\lib\graphicsLib.jar"
            !if ${VER_MATLAB} == R2008a
                File /r "${SRC_SSC}\${VER_SSC}\ifremer\extern\java\lib\nc2.2.jar"
            !endif

            ; Copie des executables de conversion XML-Binaires vers SSC.
            CreateDirectory "$INSTDIR\extern\C"
            SetOutPath "$INSTDIR\extern\C"
            ; File /r "C:\SonarScope${VER_SSC}\ifremer\extern\C\CONVERT_exe\Release\CONVERT_ARC.exe"
            ; File /r "C:\SonarScope${VER_SSC}{VER_MATLAB}\ifremer\extern\C\CONVERT_exe\Release\CONVERT_ARC_W7.exe"
            File /r "${SRC_SSC}\${VER_SSC}\ifremer\extern\C\CONVERT_exe\Release\CONVERT_SDF.exe"
            File /r "${SRC_SSC}\${VER_SSC}\ifremer\extern\C\CONVERT_exe\Release\CONVERT_ALL.exe"
            File /r "${SRC_SSC}\${VER_SSC}\ifremer\extern\C\CONVERT_exe\Release\CONVERT_HAC.exe"
            File /r "${SRC_SSC}\${VER_SSC}\ifremer\extern\C\CONVERT_exe\Release\CONVERT_XTF.exe"
            File /r "${SRC_SSC}\${VER_SSC}\ifremer\extern\C\CONVERT_exe\Release\CONVERT_RDF.exe"
            File /r "${SRC_SSC}\${VER_SSC}\ifremer\extern\C\CONVERT_exe\Release\libgcc_s_dw2-1.dll"
            File /r "${SRC_SSC}\${VER_SSC}\ifremer\extern\C\CONVERT_exe\Release\mingwm10.dll"
            File /r "${SRC_SSC}\${VER_SSC}\ifremer\extern\C\CONVERT_exe\Release\progressdialog.dll"
            File /r "${SRC_SSC}\${VER_SSC}\ifremer\extern\C\CONVERT_exe\Release\QtCore4.dll"
            File /r "${SRC_SSC}\${VER_SSC}\ifremer\extern\C\CONVERT_exe\Release\QtGui4.dll"
            
            ; Copie du package AsciiDoctor4Scc
            CreateDirectory "$INSTDIR\extern\AsciiDoctor4Ssc"
            SetOutPath "$INSTDIR\extern\AsciiDoctor4Ssc"

            File /r "${SRC_SSC}\${VER_SSC}\ifremer\extern\AsciiDoctor4Ssc\*"

        !endif

        ; Copie des ressources de SonarScope (fichier icone, .html, ...).
        CreateDirectory "$INSTDIR\SScTbxResources"
	SetOutPath "$INSTDIR\SScTbxResources"
        !if ${TARGET} != Test
            ; Exclusion des raccourcis et des fichiers SVN.
            File /r /x *.lnk /x *.git "${DIR_RSC}\*"

        !endif
        StrCpy $R0 "$INSTDIR\SScTbxResources"
        WriteINIstr "$R0\Config.ini" "ENV" "IfrTbxResources" "$INSTDIR\SScTbxResources"
        WriteINIstr "$R0\Config.ini" "ENV" "IfrTbxTempDir" "$varTempDirDest"

        WriteINIstr "$R0\Config.ini" "USER" "AcceptSendingMsg" "$varFlagAcceptMsg"
        WriteINIstr "$R0\Config.ini" "USER" "EmailServerAddress" "$varServerAddress"
        WriteINIstr "$R0\Config.ini" "USER" "EmailPortNumber" "$varPortNumber"

        ; Effacement du chemin d'installation.
        ; On ne rajoute plus le chemin de SonarScope car les raccourcis Bureau, ... pointent directement sur
        ; le repertoire et la gestion du path peut poser pb (chaine de 1024 car.).
        ;OLD ${EnvVarUpdate} $0 "PATH" "R" "HKLM" $INSDIR
        ; Rajout du chemin d'installation.
        ;OLD ${EnvVarUpdate} $0 "PATH" "A" "HKLM" $INSDIR

        ; Positionnement des variables apres effacement prealable (sinon la variable
        ; cumule les valeurs.)
        ; Suppression des valeurs anciennes de localisation du Cache de SonarScope
        ReadEnvStr $0 "MCR_CACHE_ROOT"
        
        ; Test de l'existence prealable de la variable. Si elle existe deja�, on evite
        ; les lignes suivantes qui entraa�nent des effets de bord sur le PC de JMA.
        ; (Windows 7 03/02/2015)
        ${If} $0 == ""
              ;${EnvVarUpdate} $0 "MCR_CACHE_ROOT" "R" "HKLM" "$varTempDirDest\MCR-SonarScope"
              ;${EnvVarUpdate} $0 "MCR_CACHE_ROOT" "R" "HKLM" "$varTempDirDest\SonarScope"
              ; Ajout
              ${EnvVarUpdate} $0 "MCR_CACHE_ROOT" "A" "HKLM" "$varTempDirDest\MCR-SonarScope"

              ; Positionnement des variables apres effacement prealable (sinon la variable
              ; cumule les valeurs.)
              ;${EnvVarUpdate} $0 "MCR_INHIBIT_CTF_LOCK" "R" "HKLM" "1"
              ; Ajout
              ${EnvVarUpdate} $0 "MCR_INHIBIT_CTF_LOCK" "A" "HKLM" "1"
        ${EndIf}
              

SectionEnd

Section -FinishSection

        ; Copie systematique des fichiers ReadMe.
        CreateDirectory $INSTDIR
        SetOutPath "$INSTDIR\"
	File /r ".\Textes\readme_fr.txt"
	File /r ".\Textes\readme_us.txt"

	; Enregistrement du chemin d'install.
        WriteINIstr "$R0\Config.ini" "ENV" "IfrTbx" "${SRC_SSC}\${VER_SSC}"
        WriteINIstr "$R0\Config.ini" "ENV" "IfrDpy" "C:\IfremerDeploy"

	; Ecriture du fichier Langage dans le .ini local.
        ${If} $LANGUAGE == ${LANG_FRENCH}
              WriteINIstr "$INSTDIR\Install.log" "CONFIG" "Language" "FR"
              WriteINIstr "$R0\Config.ini" "ENV" "IfrTbxLang" "FR"
        ${Else}
              WriteINIstr "$INSTDIR\Install.log" "CONFIG" "Language" "US"
              WriteINIstr "$R0\Config.ini" "ENV" "IfrTbxLang" "US"
        ${EndIf}
        WriteINIstr "$INSTDIR\Install.log" "CONFIG" "TempDirectory" $varTempDirDest

        ; Ecriture des tailles memoire RAM (totales et disponibles)
        Call GetPhysicalMemory64
        Pop $4

        WriteINIstr "$R0\Config.ini" "SYSTEM" "SizePhysTotalMemory" $4
        WriteINIstr "$R0\Config.ini" "SYSTEM" "AcceptSendingMsg" "$varFlagAcceptMsg"

	; Enregistrement du chemin d'install.
        WriteINIstr "$INSTDIR\Install.log" "CONFIG" "PATH" "$INSTDIR"

        ; Ecriture des cles de registre.
        ReadRegStr $0 HKLM "SoftWare\Microsoft\Windows\CurrentVersion" "ProductId"
        Push $0       ; Recuperation de l'identification du systeme pour transformation
        Push "-"      ; Caractere a� substituer
        Push "glu"    ; Chaa�ne de subtitution (c'est bibi)
        Call StrRep   ; Fonction decrite a� la fin
        Pop $R9       ; Recuperation du resultat.

        ; Recuperation de la date du jour.
	${GetTime} "" "L" $0 $1 $2 $3 $4 $5 $6
        ; La cle de registre depend du repertoire d'installation
        StrCpy $7 $INSTDIR "" 3
        ;WriteRegStr HKLM "SOFTWARE\Ifremer\$7" "Pwd" "$R9"
        WriteRegStr HKLM "SOFTWARE\Ifremer\$7" "Nom" ${PRODUCT_NAME}
        WriteRegStr HKLM "SOFTWARE\Ifremer\$7" "Version" ${PRODUCT_VERSION}
        WriteRegStr HKLM "SOFTWARE\Ifremer\$7" "Date" "$2/$1/$0"
        WriteRegStr HKLM "SOFTWARE\Ifremer\$7" "License" $varDateValid
        WriteRegStr HKLM "SOFTWARE\Ifremer\$7" "Partenaire" $varDestinataire
        WriteRegStr HKLM "SOFTWARE\Ifremer\$7" "User" $varService
        WriteRegStr HKLM "SOFTWARE\Ifremer\$7" "CleActivation" $varCle
        WriteRegStr HKLM "SOFTWARE\Ifremer\$7" "Install" $INSTDIR

	WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Product" "DisplayName" "IFREMER-SonarScope"
	WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Product" "UninstallString" "$INSTDIR\Uninstall SonarScope"
	WriteUninstaller "$INSTDIR\Uninstall SonarScope.exe"

        ; Lecture du repertoire de destination indique par l'operateur.
        ; StrCpy $R0 "C:\Temp"
        StrCpy $R0 "$INSTDIR\SScTbxResources"
        CreateDirectory "$R0"
 	SetOutPath "$R0"

        WriteINIstr "$R0\Config.ini" "ENV" "IfrTbx" "${SRC_SSC}\${VER_SSC}"
        WriteINIstr "$R0\Config.ini" "ENV" "IfrDpy" "C:\IfremerDeploy"
        ;WriteINIstr "$R0\Config.ini" "ENV" "IfrTbxDocumentation" "C:\SonarScopeDoc"

	; Ecriture du fichier Langage dans le .ini local.
        ${If} $LANGUAGE == ${LANG_FRENCH}
              WriteINIstr "$INSTDIR\Install.log" "CONFIG" "Language" "FR"
              WriteINIstr "$R0\Config.ini" "ENV" "IfrTbxLang" "FR"
        ${Else}
              WriteINIstr "$INSTDIR\Install.log" "CONFIG" "Language" "US"
              WriteINIstr "$R0\Config.ini" "ENV" "IfrTbxLang" "US"
        ${EndIf}


        ; Autorisations a� tous les utilisateurs (Builtin/users) sur le fichier Config.ini
        ; (double Securite par rapport au GrantFile du INSTDIR.
        AccessControl::GrantOnFile "$R0\Config.ini" "(BU)" "FullAccess"


	; Appel de l'ecriture de la configuration d'installation.
        FlushINI "$R0\Config.ini"
	FlushINI "$INSTDIR\Installlog"

        ; Suppression des executables copies sous C:\TEMP
        StrCpy $R0 $varTempDirDest

        ; Suppression du fichier de gestion des mots de passe a� l'installation en mode silencieux.
        SetDetailsPrint none
        ExecWait "cmd /k; del $TEMP\ListePLP.csv & Exit"


SectionEnd

;Uninstall section
Section Uninstall

        Call un.Supprime

        ;OLD ${un.EnvVarUpdate} $0 "PATH" "R" "HKLM" "$INSTDIR"
        ; On evite la ligne suivante qui entraa�nent des effets de bord sur le PC de JMA.
        ; (Windows 7 03/02/2015)
        ;${un.EnvVarUpdate} $0 "MCR_CACHE_ROOT" "R" "HKLM" "$varTempDirDest\SonarScope"
        ; Inhibition du message a� la demande de JMA.
        ; MessageBox MB_OK $(warn_UninstallSSC)
        ; Suppression de l'association entre les fichiers s7K et DataView.exe
        ${unregisterExtension} ".s7k" "S7K File"


        Var /GLOBAL processFound
        FindProcDLL::FindProc "SonarScope.exe"
        IntCmp $R0 1 0 notRunning
            MessageBox MB_OK|MB_ICONEXCLAMATION "SonarScope.exe is running. Please close it first" /SD IDOK
            Abort
        notRunning:

        # Always delete uninstaller first
        Delete "$INSTDIR\Uninstall SonarScope.exe"

SectionEnd

; ---- Positionnement du Flag de la section.
Function .onSelChange

  ; Flag=1, fora�age de l'installation
  ;SectionSetFlags ${SEC06} 1

FunctionEnd

; *******************************************************************
; Fonctions
Function un.Supprime

Var /GLOBAL pathInstall

        ; Identification de l'architecture de la machine
        SetRegView 64

        ; Effacement limite a� la cle de la version installee tel jour.
        StrCpy $pathInstall $INSTDIR "" 3
        RMDir /r "$INSTDIR\"
	Delete "$INSTDIR\Uninstall SonarScope.exe"

        DeleteRegKey HKLM "Software\Ifremer\$pathInstall"

	DeleteRegValue HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Product" "DisplayName"
	DeleteRegValue HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Product" "UninstallString"

	;Creation des raccourcis de l'application
        setShellVarContext all ; Propagation du contexte a� tous les utilisateurs.

        StrCpy $pathInstall $INSTDIR "" 3
        Delete "$DESKTOP\$pathInstall.lnk"
	Delete "$QUICKLAUNCH\$pathInstall.lnk"
	Delete "$SMPROGRAMS\SonarScope\$pathInstall.lnk"

FunctionEnd


; *******************************************************************

; Description des composants
!insertmacro MUI_FUNCTION_DESCRIPTION_BEGIN
	!insertmacro MUI_DESCRIPTION_TEXT ${SEC_SonarScope} $(info_sec_sonarscope)
!insertmacro MUI_FUNCTION_DESCRIPTION_END


; --------------------------------
; Fonction de saisie des cles d'activation
Function FctInputGrantKey

  WriteINIStr "$PLUGINSDIR\SSCInputGrantKey.ini" "Settings" "RTL" "0"
  ; Pratique pour DEBUG
  /*
  WriteINIStr "$PLUGINSDIR\SSCInputGrantKey.ini" "Field 4"  "State" "IFREMER"
  WriteINIStr "$PLUGINSDIR\SSCInputGrantKey.ini" "Field 6"  "State" "JM Augustin"
  */

  ; Lecture du Handle de Window courante.
  FindWindow $varHdlWindow "#32770" "" $HWNDPARENT
  ; Invalidation du bouton Next
  ; TODO GetDlgItem $2 $HWNDPARENT 1 ; 1200 + FieldNumber
  ; TODO EnableWindow $2 0

  !insertmacro MUI_HEADER_TEXT "$(info_key_title)" "$(info_key_subtitle)"

  ; Initialisation du FLAG Resultat.
  Push $R0
  InstallOptions::initdialog "$PLUGINSDIR\SSCInputGrantKey.ini"
  Pop $R0
  ; Lecture de la premi�re ligne d'ent�te
  strcpy $varDummy 0
  
  ${While} $varDummy == 0

     InstallOptions::show "$PLUGINSDIR\SSCInputGrantKey.ini"
     Pop $R0
     ${If} $R0 == "cancel"
           Return
     ${ElseIf} $R0 == "back"
           Return
     ${EndIf}

     ; Lecture des champs de l'IHM
     ReadINIStr $0 "$PLUGINSDIR\SSCInputGrantKey.ini" "Field 4" "State"
     ReadINIStr $1 "$PLUGINSDIR\SSCInputGrantKey.ini" "Field 6" "State"
 
     ${If} $0 == 0
        MessageBox MB_OK "Merci de remplir le champ Company"
     ${EndIf}
     ${If} $1 == 0
        MessageBox MB_OK "Merci de remplir le champ User"
     ${EndIf}
     ${If} $0 != 0
         ${If} $1 != 0
           strcpy $varDummy 1
           EnableWindow $2 0
         ${EndIf}
     ${EndIf}

    ${EndWhile}
    
FunctionEnd

; Fonctions de recuperation des repertoires d'installation et temporaires.
; Inspire de :
; http://nsis.sourceforge.net/Setting_Default_Location_for_2nd_(Data)_Directory_Page
; -----------------------------------------------------------------------

Function welcome_pre
         StrCpy $9 "0"
Functionend

Function DirectoryPre
         ; Sans objet pour l'instant
Functionend

Function DirectoryShow
         ; ID des controles d'une fenetre :
         ; --> 1208 = branding Text Control
         ; --> 2 = bouton cancel
         ; --> 3 = bouton retour
        ; Si on vient de la page licence ou on revient de la page Temporary Directory.
         ${If} $9 == "0"
         ${OrIf} $9 == "3"
            StrCpy $9 "1"
             !insertmacro MUI_INNERDIALOG_TEXT 1020 $(info_dirSoftDestLabel)
             ${If} $varSoftDirDest == ""
                   StrCpy $varSoftDirDest $INSTDIR
             ${EndIf}
             !insertmacro MUI_INNERDIALOG_TEXT 1019 $varSoftDirDest
         ${ElseIf} $9 == "2"
             StrCpy $9 "3"
             !insertmacro MUI_INNERDIALOG_TEXT 1020 $(info_dirTempDestLabel)
             ${If} $varTempDirDest == ""
                StrCpy $varTempDirDest "C:\Temp"
             ${EndIf}
             !insertmacro MUI_INNERDIALOG_TEXT 1019 $varTempDirDest
             ;On repositionne sur la page d'installation de SonarScope.
             ;StrCpy $9 "0"
         ${Endif}

FunctionEnd

; La fonction DirectoryLeave n'est appelee qu'a� la sortie vers l'avant de la page courante.
Function DirectoryLeave

         ${If} $9 == "1"
            StrCpy $9 "2"
            ; Controle de la validite du repertoire d'installation
             GetInstDirError $0
             ${Switch} $0
                ${Case} 1
                   MessageBox MB_ICONEXCLAMATION $(error_softDir)
                   ${Break}
                ${Case} 2
                   MessageBox MB_ICONEXCLAMATION $(error_diskspace)
                   ${Break}
             ${EndSwitch}
             StrCpy $varSoftDirDest $INSTDIR
          ${ElseIf} $9 == "3"
             StrCpy $9 "4"
             ; Controle de la validite du repertoire d'installation
             GetInstDirError $0
             ${Switch} $0
                ${Case} 1
                   MessageBox MB_ICONEXCLAMATION $(error_tempDir)
                   ${Break}
                ${Case} 2
                   MessageBox MB_ICONEXCLAMATION $(error_diskspace)
                   ${Break}
             ${EndSwitch}

             StrCpy $varTempDirDest $INSTDIR

             CreateDirectory $varTempDirDest
        ${EndIf}

FunctionEnd

; --------------------------------
; Affiche de la page de saisie des la config d'exploitation SSC
Function Pre_SSCConfigMail_Page

   InstallOptions::initdialog "$PLUGINSDIR\SSCconfigMail.ini"

   ; In this mode InstallOptions returns the window handle so we can use it
   Pop $varHdlWindow
   ; Now show the dialog and wait for it to finish
   InstallOptions::show
   ; Finally fetch the InstallOptions status value (we don't care what it is though)
   Pop $0


FunctionEnd ; fin de PreFct_InputConfigSSC_Page

; --------------------------------
; Fcontions de gestion de la page de saisie des la config d'exploitation SSC
Function SSCConfigMail_Page

  ; Lecture du Handle de Window courante.
  FindWindow $varHdlWindow "#32770" "" $HWNDPARENT
  ReadINIStr $1 "$PLUGINSDIR\SSCconfigMail.ini" "Settings" "State"

  ; Lecture du .ini pour interagir sur la fen�tre sur les boutons activ� ou non.
  ReadINIStr $varFlagAcceptMsg "$PLUGINSDIR\SSCconfigMail.ini" "Field 8" "State"

  ${If} $1 != 0
     ${If} $varFlagAcceptMsg != 0
         GetDlgItem $2 $varHdlWindow 1205 ; 1200 + FieldNumber
         EnableWindow $2 1
         GetDlgItem $2 $varHdlWindow 1204 ; 1200 + FieldNumber
         EnableWindow $2 1
         GetDlgItem $2 $varHdlWindow 1208 ; 1200 + FieldNumber
         EnableWindow $2 1
         GetDlgItem $2 $varHdlWindow 1209 ; 1200 + FieldNumber
         EnableWindow $2 1

     ${Else}
         GetDlgItem $2 $varHdlWindow 1205 ; 1200 + FieldNumber
         EnableWindow $2 0
         GetDlgItem $2 $varHdlWindow 1204 ; 1200 + FieldNumber
         EnableWindow $2 0
         GetDlgItem $2 $varHdlWindow 1208 ; 1200 + FieldNumber
         EnableWindow $2 0
         GetDlgItem $2 $varHdlWindow 1209 ; 1200 + FieldNumber
         EnableWindow $2 0
     ${EndIf}
  ${EndIf}

  ; R�cup�ration de la touche s�lectionn�e.
  ${If} $1 != 0
     Abort ; Return to the page
  ${Else}
     ${If} $varFlagAcceptMsg != 0
       ; Lecture du chemin temporaire d'installation des ex�cutables.
       ReadINIStr $varServerAddress "$PLUGINSDIR\SSCconfigMail.ini" "Field 9" "state"

       ; Lecture du chemin temporaire d'installation des ex�cutables.
       ReadINIStr $varPortNumber "$PLUGINSDIR\SSCconfigMail.ini" "Field 8" "state"

     ${EndIf}
  ${EndIf}


FunctionEnd ; Fin de Fct_InputConfigSSC_Page
;--------------------------------
;Uninstaller Functions

Function un.onInit
         ; On force la desintallation en anglais.
         StrCpy $LANGUAGE ${LANG_ENGLISH}
         ;!insertmacro MUI_UNGETLANGUAGE
FunctionEnd


