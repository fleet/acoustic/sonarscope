; Script d'installation NSIS des donn�es de SonarScope.
;
; Ce script installe tous les �l�ments de donn�es n�cessaires � SonarScope.
; (squelettes NetCDf, donn�es exemples, Levitus).
; Developp� sous NSIS avec l'�diteur HM Edit de HM Soft.
;
; Date      : 18/04/2008
; Auteur    : GLU (ATL)
; Cr�ation : 04/2008 - GLU, ATL (02 98 05 43 21).
; ================================================================================================

; D�finition de Macros de compilation
!define PRODUCT_NAME "SetUpSonarScope_Data"
!define PRODUCT_VERSION "${VER_MATLAB}"     ; VER_MATLAB est d�fini dans les profils de compilation.
!define PRODUCT_PUBLISHER "IFREMER NSE-AS"
!define PRODUCT_WEB_SITE "http://www.ifremer.fr"

VAR varNowTitre            ; Formattage de la date pour le titre des fen�tres.
VAR varRepCibleData        ; R�pertoire o� seront install�es les Data.
VAR varHdlWindow           ; Handle du window en cours.
VAR varRootDirData         ; Variable de la localisation du sous-r�pertoire de data.
VAR varChoixAction         ; Indicateur de choix de l'action (copie ou maj des chemins de la Doc).

; Test du profil pour �viter d'�tre en mode D�faut. Ce profil ne peut �tre supprim�.
!if ${TARGET} == Default
    !echo "Veuillez choisir un profil de compilation valide !"
    !error "Arr�t de compilation forc�e!"
!endif

!if ${SRC_DATA} == ''
    !echo "Pensez � pointer le r�pertoire du zip SonarScopeData dans la macro SRC_DATA!"
    !error "Arr�t de compilation forc�e!"
!endif


; Message Bas de page des fen�tres
BrandingText "$(info_welcomeSSCTitle) ${PRODUCT_NAME}-${PRODUCT_VERSION}"

; D�clarations des Include
!include FileFunc.nsh
!include Header\SSCMessages.nsh    ; D�clarations des messages multi-langues
!include Header\SSCMainFunctions.nsh ; D�clarations de fonctions particuli�res
!include Header\Utils.nsh          ; D�clarations d'utilitaires

!include Header\zipdll.nsh         ; D�clarations sp�cifiques pour la d�claration de GMT.
                                    ; Installer zipdll.nsh et zipdll.dll dans les r�pertoires
                                    ; des Plugs-ins
!include WordFunc.nsh               ; Utilitaires de gestion des strings
!include LogicLib.nsh               ; Provides the use of various logic statements
!include FileFunc.nsh               ; Utilitaires d'acc�s aux fichiers.

; Macro de SSCMainFunctions.nsh
!insertmacro ReadMe

; Macro de Utils.nsh
!insertmacro GetDrivesCallback ""
!insertmacro DirState

!define /date NOW "%Y%m%d"           ; Voir formats possibles dans strftime de MSDN.
!define /date NOW_us "%Y-%B-%d"
!define /date NOW_fr "%d-%B-%Y "

; *******************************************************************
; MUI 1.67 compatible -------
!include UMUI.nsh

; D�finition pour la modernisation des pages
!insertmacro MUI_DEFAULT UMUI_LEFTIMAGE_BMP ".\Resources\SONARSCOPE_UMUI_Data.bmp"

; D�finition pour la page de bienvenue.
!define MUI_WELCOME_BITMAP ".\Resources\SONARSCOPE.bmp"
!define MUI_WELCOME_BITMAP_NOSTRETCH
; D�finition pour la page de fin
!define MUI_WELCOMEFINISHPAGE_BITMAP ".\Resources\SONARSCOPE.bmp"
!define MUI_WELCOMEFINISHPAGE_BITMAP_NOSTRETCH
; Page d'accueil
!define MUI_ICON "${NSISDIR}\Contrib\Graphics\Icons\modern-install.ico"
!define MUI_WELCOMEPAGE_TITLE "$(info_welcomeSSCTitle) - SonarScope Data"
!define MUI_WELCOMEPAGE_TEXT $(info_welcomeSSCData)
; Suppression du sous-titre de chaque page - redondant avec le titre.
!define MUI_PAGE_HEADER_SUBTEXT ""

!if ${TARGET} != Test
    !insertmacro MUI_PAGE_WELCOME
!endif

!warning "1. Pour JMA : SonarScopeData.zip est embarqu� dans ce SetUp."
!warning "2. Pour JMA : Veillez � pointer la bonne version dans les variables de profil de compilation ."

; Param�trage Modern Interface
!define MUI_ABORTWARNING
!define MUI_HEADERIMAGE
!define MUI_HEADERIMAGE_BITMAP ".\Resources\SONARSCOPE_header.bmp"
!define MUI_HEADERIMAGE_BITMAP_NOSTRETCH
!define MUI_HEADERIMAGE_UNBITMAP ".\Resources\SONARSCOPE_header.bmp"
!define MUI_HEADERIMAGE_UNBITMAP_NOSTRETCH
!define MUI_HEADERIMAGE_RIGHT

;Page customis�e d'installation des donn�es
Page custom PreInstallData "" $(info_data_custom)

; Page installation
!insertmacro MUI_PAGE_INSTFILES

; Desinstalleur
!insertmacro MUI_UNPAGE_CONFIRM
!insertmacro MUI_UNPAGE_INSTFILES

; Page de fin
!define MUI_FINISHPAGE_NOAUTOCLOSE
!define MUI_FINISHPAGE_TITLE "${PRODUCT_NAME} Installation Complete"
!define MUI_FINISHPAGE_LINK "http://www.ifremer.fr"
!define MUI_FINISHPAGE_LINK_LOCATION "http://www.ifremer.fr"
!define MUI_FINISHPAGE_TEXT "$(info_finTexte) ${PRODUCT_NAME}"
!define MUI_FINISHPAGE_RUN_NOTCHECKED

;!define MUI_FINISHPAGE_RUN
!define MUI_FINISHPAGE_RUN_TEXT $(info_finLaunch)
!define MUI_FINISHPAGE_RUN_FUNCTION "Launch"
!define MUI_FINISHPAGE_BUTTON $(info_finBtn)
;OLD !define MUI_FINISHPAGE_SHOWREADME "..\textes\readme_fr.txt"
;OLD!define MUI_FINISHPAGE_SHOWREADME_TEXT $(info_readme)
;OLD!define MUI_FINISHPAGE_SHOWREADME_FUNCTION "ReadMe"
;OLD!define MUI_FINISHPAGE_SHOWREADME_NOTCHECKED
!insertmacro MUI_PAGE_FINISH

; Selection des langages
!insertmacro MUI_LANGUAGE "English"
!insertmacro MUI_LANGUAGE "French"
!define MUI_LANGDLL_WINDOWTITLE "${PRODUCT_NAME} ${PRODUCT_VERSION} ${NOW_us}"
!define MUI_LANGDLL_INFO $(info_info)
!insertmacro MUI_RESERVEFILE_LANGDLL

; R�f�rences � tous les messages Op�rateurs.
; Macro de SSCMessages.nsh
!insertmacro CustomMsg

; MUI end -------------------

ShowInstDetails show
ShowUnInstDetails show

Icon          ".\Resources\SonarScope.ico"
UninstallIcon ".\Resources\SonarScope.ico"


; *******************************************************************
; Sections
Name "${PRODUCT_NAME} ${PRODUCT_VERSION}, $varNowTitre"

!if ${TARGET} == Test
    OutFile  "${DIR_DEPLOY}\SetUp\Version${PRODUCT_VERSION}\${PRODUCT_VERSION}_${PRODUCT_NAME}_${NOW}_test.exe"   ; nom de l'ex�cutable
!else
    OutFile  "${DIR_DEPLOY}\SetUp\Version${PRODUCT_VERSION}\${PRODUCT_VERSION}_${PRODUCT_NAME}_${NOW}.exe"   ; nom de l'ex�cutable
!endif


; *******************************************************************
; On initialization
Function .onInit

  SetOverwrite try
  ; verification de l'instance unique de l'installateur
  System::Call 'kernel32::CreateMutexA(i 0, i 0, t "install_SonarScope") i .r1 ?e'
  Pop $R0
  ${If} $R0 != 0
     MessageBox MB_OK $(warn_instance_installer)
     Abort
  ${EndIf}
  
  ;Affichage de la bo�te de dialogue des Langages avec for�age de la langue par d�faut.
  strCpy $Language ${LANG_ENGLISH}
  ;GLU !insertmacro MUI_LANGDLL_DISPLAY

  ${GetDrives} 'ALL' 'GetDrivesCallback'
  
  !insertmacro IsUserAdmin $0
  ${If} $0 != 1 ; n'appartient pas aux administrateurs
        MessageBox MB_ICONSTOP $(err_UserProfile)
  Quit
  ${Endif}

  InitPluginsDir
  ${If} $LANGUAGE == ${LANG_FRENCH}
     StrCpy $varNowTitre "${NOW_fr}"

     ; Pour l'affichage de la s�lection des Datas
     File /oname=$PLUGINSDIR\SSCData.ini ".\Resources\SSCData_fr.ini"

     ; R�cup�ration des lecteurs de donn�es.
     WriteINIstr "$PLUGINSDIR\SSCData.ini" "Field 12" "ListItems" "$R5"
     WriteINIstr "$PLUGINSDIR\SSCData.ini" "Field 12" "State" "$R4"
     WriteINIstr "$PLUGINSDIR\SSCData.ini" "Field 6" "ListItems" "1: Installation des donn�es |2: Mise � jour des chemins"
     WriteINIstr "$PLUGINSDIR\SSCData.ini" "Field 6" "State" "1: Installation des donn�es"

  ${Else}
     StrCpy $varNowTitre "${NOW_us}"
     ; Pour l'affichage de la s�lection des Datas
     File /oname=$PLUGINSDIR\SSCData.ini ".\Resources\SSCData_us.ini"

     ; R�cup�ration des lecteurs de donn�es.
     WriteINIstr "$PLUGINSDIR\SSCData.ini" "Field 12" "ListItems" "$R5"
     WriteINIstr "$PLUGINSDIR\SSCData.ini" "Field 12" "State" "$R4"
     WriteINIstr "$PLUGINSDIR\SSCData.ini" "Field 6" "ListItems" "1: Install data|2: Update the Data path"
     WriteINIstr "$PLUGINSDIR\SSCData.ini" "Field 6" "State" "1: Install data"

  ${EndIf}
  InstallOptions::dialog "$PLUGINSDIR\SSCData.ini"

  FlushIni "$PLUGINSDIR\SSCData.ini"
  
  ; Identification de l'architecture de la machine
  SetRegView 64

FunctionEnd
; *******************************************************************
Section -FinishSection

        StrCpy $varRootDirData "SonarScopeData"

        ; Analyse des choix de l'op�rateur.
        ReadINIStr $varRepCibleData "$PLUGINSDIR\SSCData.ini" "Field 4" "state"
        ; Lecture de l'action � mener (traitement du 1er caract�re)
        ReadINIStr $0 "$PLUGINSDIR\SSCData.ini" "Field 6" "state"
        StrCpy $varChoixAction $0 1

        ;Suppression du dernier caract�re si celui-ci est '\'
        StrCpy $R1 $varRepCibleData 1 -1
        ${If} $R1 == "\"
              ; Recopie du r�pertoire cible en supprimant le dernier caract�re.
              StrCpy $varRepCibleData $varRepCibleData -1
        ${EndIf}

        ;Suppression de la chaine SonarScopeData si l'op�rateur l'a point� malgr� tout.
        StrCpy $R1 $varRepCibleData "" -14
        ${If} $R1 == "SonarScopeData"
              ; Recopie du r�pertoire cible en supprimant les 13 derniers char + '\'
              StrCpy $varRepCibleData $varRepCibleData -15
        ${EndIf}

        ; Copie du r�pertoire si il y a assez de place
        ${If} $varChoixAction == 1
              SetOutPath "$varRepCibleData\$varRootDirData"
              !if ${TARGET} == Test
                  ;Juste un fichier.
                  File /r /x *.lnk "${SRC_DOC}\Copyright.htm"
              !else
                  File /oname=$PLUGINSDIR\SonarScopeData.zip "${SRC_DATA}\SonarScopeData.zip"
                  ; Le chemin d'extraction ne doit pas comporter la cha�ne SonarScopeData
                  ; qui sera ajout�e par la d�compression.
                  ZipDLL::extractall "$PLUGINSDIR\SonarScopeData.zip" "$varRepCibleData"
              !endif
        ${EndIf}

        ; Concat�nation de la cha�ne finale de d�ploiement.
        StrCpy $varRepCibleData "$varRepCibleData\$varRootDirData"

        ; Test de pr�sence du r�pertoire de Data avant mise � jour en base de registre.
        ${DirState} "$varRepCibleData" $R0
        ${If} $R0 == -1
              MessageBox MB_ICONEXCLAMATION  "$(warn_DataInstalled) $varRepCibleData"
              ; TODO : A finaliser : StrCpy $R9 "(-1" ;Relative page number. See below.
              ; TODO : A finaliser : Call RelGotoPage
        ${EndIf}

        ; Identification de l'architecture de la machine
        ; Mise � jour qque soit le choix Op�rateur de la localisation du r�pertoire Doc Cible.
        ; Ecriture successive dans la Base 32 puis 64 Bits si la machine + OS sont en 64 bits
        SetRegView 32
        ; Indication de la cl� de registre du r�pertoire d'installation de la doc.
        WriteRegStr HKLM "SOFTWARE\Ifremer\SonarScope-Resource" "SonarScopeData" "$varRepCibleData"
        SetRegView 64
        ; Indication de la cl� de registre du r�pertoire d'installation de la doc.
        WriteRegStr HKLM "SOFTWARE\Ifremer\SonarScope-Resource" "SonarScopeData" "$varRepCibleData"

SectionEnd


;Uninstall section
Section "Uninstall"

        Call un.Supprime

        Push $INSTDIR
        Call un.RemoveFromPath
        

SectionEnd

Function PreInstallData

  ;If not using Modern UI use InstallOptions::dialog "iofile.ini"
  ; Ecriture du mode justifie ou non des champs.
  WriteINIStr "$PLUGINSDIR\SSCData.ini" "Settings" "RTL" "0"
  InstallOptions::initDialog /NOUNLOAD "$PLUGINSDIR\SSCData.ini"
  ; In this mode InstallOptions returns the window handle so we can use it
  Pop $varHdlWindow
  ; Now show the dialog and wait for it to finish
  InstallOptions::show
  ; Finally fetch the InstallOptions status value (we don't care what it is though)
  Pop $0
  
FunctionEnd

; *******************************************************************
; Fonctions

;--------------------------------
;Uninstaller Functions

Function un.onInit
         ; On force la d�sintallation en anglais.
         StrCpy $LANGUAGE ${LANG_ENGLISH}
         ;!insertmacro MUI_UNGETLANGUAGE
FunctionEnd


