 ; Script d'installation NSIS de SonarScope et sa documentation.
; Ce script installe :
;  - le logiciel SonarScope,
;  - la documentation de SonarScope
; Developp� sous NSIS avec l'�diteur HM Edit de HM Soft.
;
; Date      : 18/04/2008
; Auteur    : GLU (ATL)
; Modification : -
; Cr�ation : 09/2005 - GLU, ATL (02 98 05 43 21).
; ================================================================================================

; D�finition de Macros de compilation
!define PRODUCT_NAME "SetUpSonarScope_Doc"
!define PRODUCT_VERSION "${VER_MATLAB}"     ; VER_MATLAB est d�fini dans les profils de compilation.
!define PRODUCT_PUBLISHER "IFREMER NSE-AS"
!define PRODUCT_WEB_SITE "http://www.ifremer.fr"

VAR varNowTitre         ; Formattage de la date pour le titre des fen�tres.
VAR varHdlWindow        ; Handle du window en cours.
VAR varRepCibleDoc      ; R�pertoire o� seront install�es les Doc.
VAR varRootDirDoc       ; Variable de la localisation du sous-r�pertoire de doc.
VAR varChoixAction      ; Indicateur de choix de l'action (copie ou maj des chemins de la Doc).

; Test du profil pour �viter d'�tre en mode D�faut. Ce profil ne peut �tre supprim�.
!if ${TARGET} == Default
    !echo "Veuillez choisir un profil de compilation valide !"
    !error "Arr�t de compilation forc�e!"
!endif

; Message Bas de page des fen�tres
BrandingText "$(info_welcomeSSCTitle) ${PRODUCT_NAME}-${PRODUCT_VERSION}"

; D�clarations des Include
!include FileFunc.nsh
!include Header\SSCMessages.nsh    ; D�clarations des messages multi-langues
!include Header\SSCMainFunctions.nsh ; D�clarations de fonctions particuli�res
!include Header\Utils.nsh          ; D�clarations d'utilitaires

!include WordFunc.nsh               ; Utilitaires de gestion des strings
!include LogicLib.nsh               ; Provides the use of various logic statements

; Macro de WordFunc.nsh
!insertmacro WordFind
; Macro de SSCMainFunctions.nsh
!insertmacro ReadMe
!insertmacro Launch
; Macro de FileFunc.nsh
!insertmacro GetTime

; Macro de Utils.nsh
!insertmacro StrRep ""
!insertmacro GetPhysicalMemory ""
!insertmacro CheckSpaceFunc ""


!define /date NOW "%Y%m%d"           ; Voir formats possibles dans strftime de MSDN.
!define /date NOW_us "%Y-%B-%d"
!define /date NOW_fr "%d-%B-%Y "

; *******************************************************************
; MUI 1.67 compatible -------
!include "UMUI.nsh"

; D�finition pour la modernisation des pages
!insertmacro MUI_DEFAULT UMUI_LEFTIMAGE_BMP ".\Resources\SONARSCOPE_UMUI_Doc.bmp"

; D�finition pour la page de bienvenue.
!define MUI_WELCOME_BITMAP ".\Resources\SONARSCOPE.bmp"
!define MUI_WELCOME_BITMAP_NOSTRETCH
; D�finition pour la page de fin
!define MUI_WELCOMEFINISHPAGE_BITMAP ".\Resources\SONARSCOPE.bmp"
!define MUI_WELCOMEFINISHPAGE_BITMAP_NOSTRETCH

; Page d'accueil
!define MUI_ICON "${NSISDIR}\Contrib\Graphics\Icons\modern-install.ico"
!define MUI_WELCOMEPAGE_TITLE "$(info_welcomeSSCTitle) - SonarScope Doc"
!define MUI_WELCOMEPAGE_TEXT $(info_welcomeSSCDoc)
; Suppression du sous-titre de chaque page - redondant avec le titre.
!define MUI_PAGE_HEADER_SUBTEXT ""

!if ${TARGET} != Test
    !insertmacro MUI_PAGE_WELCOME
!endif

; Param�trage Modern Interface
!define MUI_ABORTWARNING
!define MUI_HEADERIMAGE
!define MUI_HEADERIMAGE_BITMAP ".\Resources\SONARSCOPE_header.bmp"
!define MUI_HEADERIMAGE_BITMAP_NOSTRETCH
!define MUI_HEADERIMAGE_UNBITMAP ".\Resources\SONARSCOPE_header.bmp"
!define MUI_HEADERIMAGE_UNBITMAP_NOSTRETCH
!define MUI_HEADERIMAGE_RIGHT

;Page customis�e d'installation des donn�es - pas de proc�dure Custom associ�e.
Page custom PreInstallDoc "" $(info_Doc_custom)

; Page installation
!insertmacro MUI_PAGE_INSTFILES

; Desinstalleur
!insertmacro MUI_UNPAGE_CONFIRM
!insertmacro MUI_UNPAGE_INSTFILES

; Page de fin
!define MUI_FINISHPAGE_NOAUTOCLOSE
!define MUI_FINISHPAGE_TITLE "${PRODUCT_NAME} Installation Complete"
!define MUI_FINISHPAGE_LINK "http://www.ifremer.fr"
!define MUI_FINISHPAGE_LINK_LOCATION "http://www.ifremer.fr"
!define MUI_FINISHPAGE_TEXT "$(info_finTexte) ${PRODUCT_NAME}"
!define MUI_FINISHPAGE_RUN_NOTCHECKED

;!define MUI_FINISHPAGE_RUN
!define MUI_FINISHPAGE_RUN_TEXT $(info_finLaunch)
!define MUI_FINISHPAGE_RUN_FUNCTION "Launch"
!define MUI_FINISHPAGE_BUTTON $(info_finBtn)
;OLD !define MUI_FINISHPAGE_SHOWREADME "..\textes\readme_fr.txt"
;OLD!define MUI_FINISHPAGE_SHOWREADME_TEXT $(info_readme)
;OLD!define MUI_FINISHPAGE_SHOWREADME_FUNCTION "ReadMe"
;OLD!define MUI_FINISHPAGE_SHOWREADME_NOTCHECKED
!insertmacro MUI_PAGE_FINISH

; Selection des langages
!insertmacro MUI_LANGUAGE "English"
!insertmacro MUI_LANGUAGE "French"
!define MUI_LANGDLL_WINDOWTITLE "${PRODUCT_NAME} ${PRODUCT_VERSION} ${NOW_us}"
!define MUI_LANGDLL_INFO $(info_info)
!insertmacro MUI_RESERVEFILE_LANGDLL

; R�f�rences � tous les messages Op�rateurs.
; Macro de SSCMessages.nsh
!insertmacro CustomMsg
; MUI end -------------------


ShowInstDetails show
ShowUnInstDetails show

Icon          ".\Resources\SonarScope.ico"
UninstallIcon ".\Resources\SonarScope.ico"

; *******************************************************************
; Sections
Name "${PRODUCT_NAME} ${PRODUCT_VERSION}, $varNowTitre"

!if ${TARGET} == Test
    OutFile  "${DIR_DEPLOY}\SetUp\Version${PRODUCT_VERSION}\${PRODUCT_VERSION}_${PRODUCT_NAME}_${NOW}_test.exe"   ; nom de l'ex�cutable
!else
    OutFile  "${DIR_DEPLOY}\SetUp\Version${PRODUCT_VERSION}\${PRODUCT_VERSION}_${PRODUCT_NAME}_${NOW}.exe"   ; nom de l'ex�cutable
!endif


; *******************************************************************
; On initialization
Function .onInit

  SetOverwrite try
  ; Verification de l'instance unique de l'installateur
  System::Call 'kernel32::CreateMutexA(i 0, i 0, t "install_SonarScope") i .r1 ?e'
  Pop $R0
  ${If} $R0 != 0
     MessageBox MB_OK $(warn_instance_installer)
     Abort
  ${EndIf}

  ;Affichage de la bo�te de dialogue des Langages avec for�age de la langue par d�faut.
  strCpy $Language ${LANG_ENGLISH}
  ;Affichage de la bo�te de dialogue des Langages.
  ;GLU !insertmacro MUI_LANGDLL_DISPLAY

  !insertmacro IsUserAdmin $0
  ${If} $0 != 1 ; n'appartient pas aux administrateurs
       MessageBox MB_ICONSTOP $(err_UserProfile)
       Quit
  ${Endif}

  InitPluginsDir
  ${If} $LANGUAGE == ${LANG_FRENCH}
     StrCpy $varNowTitre "${NOW_fr}"

     ; Pour l'affichage de la s�lection de la Documentation
     File /oname=$PLUGINSDIR\SSCDoc.ini ".\Resources\SSCDoc_fr.ini"

     ; R�cup�ration des lecteurs de donn�es.
     WriteINIstr "$PLUGINSDIR\SSCDoc.ini" "Field 12" "ListItems" "$R5"
     WriteINIstr "$PLUGINSDIR\SSCDoc.ini" "Field 12" "State" "$R4"
     WriteINIstr "$PLUGINSDIR\SSCDoc.ini" "Field 6" "ListItems" "1: Installation de la documentation|2: Mise � jour des chemins|"
     WriteINIstr "$PLUGINSDIR\SSCDoc.ini" "Field 6" "State" "1: Installation de la documentation"
  ${Else}
     StrCpy $varNowTitre "${NOW_us}"
     ; Pour l'affichage de la s�lection de la Documentation
     File /oname=$PLUGINSDIR\SSCDoc.ini ".\Resources\SSCDoc_us.ini"
     ; R�cup�ration des lecteurs de donn�es.
     WriteINIstr "$PLUGINSDIR\SSCDoc.ini" "Field 12" "ListItems" "$R5"
     WriteINIstr "$PLUGINSDIR\SSCDoc.ini" "Field 12" "State" "$R4"
     WriteINIstr "$PLUGINSDIR\SSCDoc.ini" "Field 6" "ListItems" "1: Install documentation|2: Update the Documentation path"
     WriteINIstr "$PLUGINSDIR\SSCDoc.ini" "Field 6" "State" "1: Install documentation"

  ${EndIf}
  InstallOptions::dialog "$PLUGINSDIR\SSCDoc.ini"

  FlushIni "$PLUGINSDIR\SSCDoc.ini"
  
  
  StrCpy $INSTDIR "D:\SonarScopeDoc"   ; r�pertoire par d�faut d'installation

  ; Int�gration des mots de passe pour l'utilisation de SonarScope
  ; File /oname=$TEMP\ListePLP.csv ".\Resources\ListePLP_SonarScope.csv"

  ; R�cup�ration de la date du jour.
  ;${GetTime} "" "L" $0 $1 $2 $3 $4 $5 $6


FunctionEnd

; *******************************************************************
; Programmation des sections
Section "Empty"
SectionEnd


Section -FinishSection


        StrCpy $varRootDirDoc "SonarScopeDoc"
        
        ; Analyse des choix de l'op�rateur.
        ReadINIStr $varRepCibleDoc "$PLUGINSDIR\SSCDoc.ini" "Field 4" "state"
        ; Lecture de l'action � mener (traitement du 1er caract�re)
        ReadINIStr $0 "$PLUGINSDIR\SSCDoc.ini" "Field 6" "state"
        StrCpy $varChoixAction $0 1
        
        ;Suppression du dernier caract�re si celui-ci est '\'
        StrCpy $R1 $varRepCibleDoc 1 -1
        ${If} $R1 == "\"
              ; Recopie du r�pertoire cible en supprimant le dernier caract�re.
              StrCpy $varRepCibleDoc $varRepCibleDoc -1
        ${EndIf}

        ;Suppression de la chaine SonarScopeDoc si l'op�rateur l'a point� malgr� tout.
        StrCpy $R1 $varRepCibleDoc "" -13
        ${If} $R1 == "SonarScopeDoc"
              ; Recopie du r�pertoire cible en supprimant les 13 derniers char + '\'
              StrCpy $varRepCibleDoc $varRepCibleDoc -14
        ${EndIf}

        ; Concat�nation de la cha�ne finale de d�ploiement.
        StrCpy $varRepCibleDoc "$varRepCibleDoc\$varRootDirDoc"

        ; Copie du r�pertoire si il y a assez de place
        ${If} $varChoixAction == 1
              SetOutPath "$varRepCibleDoc"
              !if ${TARGET} == Test
                  ;Juste un fichier.
                  File /r /x *.lnk "${SRC_DOC}\Copyright.htm"
              !else
                  File /r /x *.lnk "${SRC_DOC}\*"
              !endif
        ${EndIf}

        ; Test de pr�sence du r�pertoire de Data avant mise � jour en base de registre.
        ${DirState} "$varRepCibleDoc" $R0
        ${If} $R0 == -1
              MessageBox MB_ICONEXCLAMATION  "$(warn_DataInstalled) $varRepCibleDoc"
        ${EndIf}


        ; Identification de l'architecture de la machine
        ; Mise � jour qque soit le choix Op�rateur de la localisation du r�pertoire Doc Cible.
        ; Ecriture successive dans la Base 32 puis 64 Bits si la machine + OS sont en 64 bits
        SetRegView 32
        ; Indication de la cl� de registre du r�pertoire d'installation de la doc.
        WriteRegStr HKLM "SOFTWARE\Ifremer\SonarScope-Resource" "Doc" "$varRepCibleDoc"
        SetRegView 64
        ; Indication de la cl� de registre du r�pertoire d'installation de la doc.
        WriteRegStr HKLM "SOFTWARE\Ifremer\SonarScope-Resource" "Doc" "$varRepCibleDoc"

SectionEnd

;Uninstall section
Section Uninstall

        Push $INSTDIR
        Call un.RemoveFromPath

SectionEnd

; ---- Positionnement du Flag de la section.
Function .onSelChange

  ; Flag=1, for�age de l'installation
  ;SectionSetFlags ${SEC06} 1

FunctionEnd

; *******************************************************************


;--------------------------------
;Uninstaller Functions

Function un.onInit
         ; On force la d�sintallation en anglais.
         StrCpy $LANGUAGE ${LANG_ENGLISH}
         ;!insertmacro MUI_UNGETLANGUAGE
FunctionEnd

; --------------------------------
Function PreInstallDoc

  ; Ecriture du mode justifie ou non des champs.
  WriteINIStr "$PLUGINSDIR\SSCDoc.ini" "Settings" "RTL" "0"
  InstallOptions::initDialog /NOUNLOAD "$PLUGINSDIR\SSCDoc.ini"
  ; In this mode InstallOptions returns the window handle so we can use it
  Pop $varHdlWindow
  ; Now show the dialog and wait for it to finish
  InstallOptions::show
  ; Finally fetch the InstallOptions status value (we don't care what it is though)
  Pop $0
FunctionEnd ;PreInstallDoc
