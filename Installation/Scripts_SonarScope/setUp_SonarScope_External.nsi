; Script d'installation NSIS des modules Externes � SonarScope.
;
; Ce script installe tous les �l�ments adjacents et utiles � SonarScope.
; Developp� sous NSIS avec l'�diteur HM Edit de HM Soft.
;
; Cr�ation : 05/2008 - glu, QO (09 82 28 21 23).
; ================================================================================================

; D�finition de Macros de compilation
!define PRODUCT_NAME "SetUpSonarScope_External"
!define PRODUCT_VERSION "${VER_MATLAB}"

!define KongsbergViewerVers "3.9.4"
!define ERViewerVers    "2014"
!define GhostScriptVers "9.25"
!define GhostViewVers   "5.0"
!define ImageMagickVers "7.0.9"
!define MSVCRVers       "2008" ; Microsoft Runtime 2008 https://www.microsoft.com/fr-fr/download/confirmation.aspx?id=26368
!define iView4DVers     "7.1.0a"
!define CouldCompare    "2.12Beta"
!define NASAWWVers      "1.4"
!define GoogleEarthVers "7.x"
!define SSC3DVers       "20100715"
!define GMTVers         "6.0.0"
!define CortonaVers     "7.0"
!define GDALVers        "1.10.1"
!define PanoplyVers     "4.9.5"
!define ASCIIDOC_FX     "1.5.5"
!define OpenURL         '!insertmacro "_OpenURL"'

VAR varNowTitre            ; Formattage de la date pour le titre des fen�tres.
VAR varFlagGMTInstall      ; Flag d'installation de GMT (OK) ou non.
VAR varFlagGDALInstall     ; Flag d'installation de GDAL (OK) ou non.
VAR varRepCibleRsc         ; R�pertoire o� sont mises � jour les ressources.
VAR varExternalFolder      ; D�signation du dossier o� sont install�s les Externes
VAR varHdlWindow           ; Handle du window en cours.
VAR varFlagMCR             ; D�tection ou non du bon MCR
VAR varInstallMCR          ; Choix d'installation du MCR
VAR varHTTPLinkMCR         ; Adresse HTTP des MCR MatLab
VAR varHTTPLinkCloudComp   ; Adresse HTTP du viewer Cloud Compare
VAR varInstallCloudCompare ; Choix d'installation du MCR
VAR varMCRVersion          ; N� de Version du MCR, fonction de la version MATLAB (forme vxy).
VAR varMCRVersion2         ; N� de Version du MCR, fonction de la version MATLAB (forme x.y).
VAR varMCRUpdate           ; N� de release du MCR, n� update, existe � partir de R2019a
VAR pathMCRVersion         ; Chemin du MCr dans la base de registre.
VAR varFlagMSVCR           ; D�tection ou non du bon kit Runtime MS
VAR varWindowsPath         ; R�cup�ration du Path Windows (complet si len < 1024, vide sinon).
VAR varTempDirDest         ; R�pertoire des installations temporaires
VAR varSRVPath             ; R�pertoire d'installation pour Sonar Record Viewer

; Test du profil pour �viter d'�tre en mode D�faut. Ce profil ne peut �tre supprim�.
!if ${TARGET} == Default
    !echo "Veuillez choisir un profil de compilation valide !"
    !error "Arr�t de compilation forc�e!"
!endif

; Message Bas de page des fen�tres
BrandingText "$(info_welcomeSSCTitle) ${PRODUCT_NAME}-${PRODUCT_VERSION}"

; D�clarations des Include
!include FileFunc.nsh
!include Header\WriteToFile.nsh         ; Fonction Ecriture de fichiers
!include Header\Utils.nsh               ; D�clarations d'utilitaires
!include Header\SSCMessages.nsh         ; D�clarations des messages multi-langues
!include Header\SSCMainFunctions.nsh    ; D�clarations de fonctions particuli�res
;OLD !include Header\AddToPath.nsh      ; D�clarations sp�cifiques pour la modification du Path.
!include Header\EnvVarUpDate.nsh        ; D�clarations sp�cifiques pour la modification du Path : Rmq limit� � 1024 car.
!include Header\zipdll.nsh              ; D�clarations sp�cifiques pour la d�claration de GDAL
                                        ; Installer zipdll.nsh et zipdll.dll dans les r�pertoires
                                        ; des Plugs-ins
!include InstallOptions.nsh

!include Header\StrRep.nsh
!include Header\ReplaceInFile.nsh
!include Header\GetWindowsVersion.nsh   ; Permet de l'identification d ela version Windows.
!include LogicLib.nsh                   ; Provides the use of various logic statements
!include WordFunc.nsh

; Macro de WordFunc.nsh
!insertmacro WordFind

; Macro de SSCMainFunctions.nsh
!insertmacro ExternalPathPage
!insertmacro IdentifyMatLabVersion ""

; Macro de FileFunc.nsh
!insertmacro GetDrives
!insertmacro GetTime

; Macro de Utils.nsh
!insertmacro GetDrivesCallback ""
!insertmacro GetPhysicalMemory ""


!define /date NOW "%Y%m%d"           ; Voir formats possibles dans strftime de MSDN.
!define /date NOW_us "%Y-%B-%d"
!define /date NOW_fr "%d-%B-%Y "

; *******************************************************************
; MUI 1.67 compatible -------
!include UMUI.nsh
!include Sections.nsh

; D�finition pour la modernisation des pages
!insertmacro MUI_DEFAULT UMUI_LEFTIMAGE_BMP ".\Resources\SONARSCOPE_UMUI_External.bmp"

; D�finition pour la page de bienvenue.
!define MUI_WELCOME_BITMAP ".\Resources\SONARSCOPE.bmp"
!define MUI_WELCOME_BITMAP_NOSTRETCH
!define MUI_WELCOMEFINISHPAGE_BITMAP ".\Resources\SONARSCOPE.bmp"
!define MUI_WELCOMEFINISHPAGE_BITMAP_NOSTRETCH


; Page d'accueil
!define MUI_ICON "${NSISDIR}\Contrib\Graphics\Icons\modern-install.ico"
!define MUI_WELCOMEPAGE_TITLE "$(info_welcomeSSCTitle) - SonarScope External"
!define MUI_WELCOMEPAGE_TEXT $(info_welcomeSSCExternal)

; Suppression du sous-titre de chaque page - redondant avec le titre.
!define MUI_PAGE_HEADER_SUBTEXT ""
!if ${TARGET} != "Test"
    !insertmacro MUI_PAGE_WELCOME
!endif

; Param�trage Modern Interface
!define MUI_ABORTWARNING
!define MUI_HEADERIMAGE
!define MUI_HEADERIMAGE_BITMAP "SONARSCOPE_header.bmp"
!define MUI_HEADERIMAGE_BITMAP_NOSTRETCH
!define MUI_HEADERIMAGE_UNBITMAP "SONARSCOPE_header.bmp"
!define MUI_HEADERIMAGE_UNBITMAP_NOSTRETCH
!define MUI_HEADERIMAGE_RIGHT


; Composants
!define MUI_COMPONENTSPAGE_SMALLDESC
!define MUI_COMPONENTSPAGE_TEXT_TOP $(components_top)
!define MUI_COMPONENTSPAGE_TEXT_COMPLIST $(components_comlist)
!define MUI_COMPONENTSPAGE_TEXT_DESCRIPTION_TITLE $(components_title)
!define MUI_COMPONENTSPAGE_TEXT_DESCRIPTION_INFO $(components_desc)

;Page customis�e d'installation des composants
Page custom Pre_SSCExternalPath_Page SSCExternalPath_Page "External Path since Previous Installation"
Page custom Pre_InstallComponents_Page InstallComponents_Page
!insertmacro MUI_PAGE_COMPONENTS

; Page installation
!insertmacro MUI_PAGE_INSTFILES


; Desinstalleur
!insertmacro MUI_UNPAGE_CONFIRM
!insertmacro MUI_UNPAGE_INSTFILES

; Page de fin
!define MUI_FINISHPAGE_NOAUTOCLOSE
!define MUI_FINISHPAGE_TITLE "${PRODUCT_NAME} Installation Complete"
!define MUI_FINISHPAGE_LINK "http://www.ifremer.fr"
!define MUI_FINISHPAGE_LINK_LOCATION "http://www.ifremer.fr"
!define MUI_FINISHPAGE_TEXT "$(info_finTexte) ${PRODUCT_NAME}"
!define MUI_FINISHPAGE_RUN_NOTCHECKED

;!define MUI_FINISHPAGE_RUN
!define MUI_FINISHPAGE_BUTTON $(info_finBtn)
!define MUI_FINISHPAGE_SHOWREADME "readme_fr.txt"
!define MUI_FINISHPAGE_SHOWREADME_TEXT $(info_readme)
!define MUI_FINISHPAGE_SHOWREADME_FUNCTION "ReadMe"
!define MUI_FINISHPAGE_SHOWREADME_NOTCHECKED
!insertmacro MUI_PAGE_FINISH

; Selection des langages
!insertmacro MUI_LANGUAGE "English"
!insertmacro MUI_LANGUAGE "French"
!define MUI_LANGDLL_WINDOWTITLE "${PRODUCT_NAME} ${PRODUCT_VERSION} ${NOW_us}"
!define MUI_LANGDLL_INFO $(info_info)
!insertmacro MUI_RESERVEFILE_LANGDLL

; R�f�rences � tous les messages Op�rateurs.
; Macro de SSCMessages.nsh
!insertmacro CustomMsg
; MUI end -------------------

; *******************************************************************
; Sections
Name "${PRODUCT_NAME} ${PRODUCT_VERSION}, $varNowTitre"

!if ${TARGET} == Test
        OutFile  "${DIR_DEPLOY}\SetUp\Version${PRODUCT_VERSION}\${PRODUCT_VERSION}_${PRODUCT_NAME}_${NOW}_Test.exe"   ; nom de l'ex�cutable
!else if ${TARGET} == 64Bit
        OutFile  "${DIR_DEPLOY}\SetUp\Version${PRODUCT_VERSION}\${PRODUCT_VERSION}_${PRODUCT_NAME}_${NOW}.exe"   ; nom de l'ex�cutable
!endif
InstallDir "C:\SonarScope"


ShowInstDetails show
ShowUnInstDetails show


Icon             ".\Resources\SonarScope.ico"
UninstallIcon    ".\Resources\SonarScope.ico"


; *******************************************************************
; On initialization
Function .onInit

  ; Identification de la version MatLab et recopie.
  Push ${VER_MATLAB}
  Call identifyMatLabVersion
  Pop $0
  StrCpy $varMCRVERSION $0
  Pop $1
  StrCpy $varMCRVERSION2 $1
  Pop $2
  StrCpy $pathMCRVersion $2

  ; For�age de la release MCR depuis la version R2019a
  StrCpy $varMCRUpdate "1"

  SetOverwrite try
  ; verification de l'instance unique de l'installateur
  System::Call 'kernel32::CreateMutexA(i 0, i 0, t "install_SonarScope") i .r1 ?e'
  Pop $R0
  ${If} $R0 != 0
     MessageBox MB_OK $(warn_instance_installer)
     Abort
  ${EndIf}
  ;Affichage de la bo�te de dialogue des Langages avec for�age de la langue par d�faut.
  strCpy $Language ${LANG_ENGLISH}
  ; For�age en anglais !insertmacro MUI_LANGDLL_DISPLAY

  ${GetDrives} 'ALL' 'GetDrivesCallback'

  !insertmacro IsUserAdmin $0
  ${If} $0 != 1 ; n'appartient pas aux administrateurs
       MessageBox MB_ICONSTOP $(err_UserProfile)
       Quit
  ${Endif}

  InitPluginsDir
  ${If} $LANGUAGE == ${lang_french}
     StrCpy $varNowTitre "${NOW_fr}"
     File /oname=$PLUGINSDIR\SSCExternalPath.ini ".\Resources\SSCExternalPath_fr.ini"
  ${Else}
     StrCpy $varNowTitre "${NOW_us}"
     File /oname=$PLUGINSDIR\SSCExternalPath.ini ".\Resources\SSCExternalPath_us.ini"
  ${EndIf}


  ; Suppression de la derni�re trace.
  Delete $INSTDIR\Install.log

  ; R�cup�ration de la date du jour.
  ${GetTime} "" "L" $0 $1 $2 $3 $4 $5 $6

  WriteINIstr "$INSTDIR\Install.log" "INSTALL" "Date" "$3 $2/$1/$0  $4:$5:$6"
  WriteINIstr "$INSTDIR\Install.log" "INSTALL" "Version" "FULL  ${PRODUCT_VERSION}"

  ; Lecture du Path Windows
  ReadRegStr $varWindowsPath HKLM "SYSTEM\CurrentControlSet\Control\Session Manager\Environment" "Path"
  /*
  Solution pb car le path ne peut �tre mis � jour de fa�on permanente.
  ExecWait "cmd /k; del $TEMP\sscExternal_setPATH.bat & Exit"
  ; Initialisation de la mise � jour semi-automatique du Path dans le cas de chaines longues.
  ${If} $varWindowsPath == ""
      ${WriteToFile} "$TEMP\sscExternal_setPATH.bat" "goto START$\r$\n"
      ${WriteToFile} "$TEMP\sscExternal_setPATH.bat" "rem -----------------------------------------$\r$\n"
      ${WriteToFile} "$TEMP\sscExternal_setPATH.bat" "rem    sscExternal_setPATH.bat$\r$\n"
      ${WriteToFile} "$TEMP\sscExternal_setPATH.bat" "rem    Update Path after SSC External install$\r$\n"
      ${WriteToFile} "$TEMP\sscExternal_setPATH.bat" "rem    Date : $3 $2/$1/$0  $4:$5:$6$\r$\n"
      ${WriteToFile} "$TEMP\sscExternal_setPATH.bat" "rem -----------------------------------------$\r$\n"
      ${WriteToFile} "$TEMP\sscExternal_setPATH.bat" "$\r$\n"
      ${WriteToFile} "$TEMP\sscExternal_setPATH.bat" ":START$\r$\n"
  ${EndIf}
  */
  
  ; D�ploiement du fichier rmpath.bat pour la mise � jour des chemins si besoin
  SetOutPath $TEMP
  File /r "${SRC_SSC}\${VER_SSC}\Installation\Scripts_SonarScope\Resources\rmpath.bat"

  SetRegView 64

FunctionEnd
; *******************************************************************
; Programmation des sections
SectionGroup "Viewers"

Section "ERViewer" SEC_ERViewer
        StrCpy $R0 $varTempDirDest             ; pour l'extraction des installateurs de Viewers
        CreateDirectory "$R0"
	SetOutPath "$R0"
        ${If} ${TARGET} != Test
	    File /r "${SRC_SSC}\${VER_SSC}\Installation\Viewers\ERViewer${ERViewerVers}.exe"
	    ExecWait '"$R0\ERViewer${ERViewerVers}.exe"'
        ${EndIf}
        WriteINIstr "$INSTDIR\Install.log" "INSTALL" "ERViewer" "Yes"
SectionEnd

Section "Image Magic" SEC_ImageMagick
        StrCpy $R0 $varTempDirDest             ; pour l'extraction des installateurs de Viewers
        CreateDirectory "$R0"
	SetOutPath "$R0"
        ${If} ${TARGET} != Test
	    File /r "${SRC_SSC}\${VER_SSC}\Installation\Viewers\ImageMagick-${ImageMagickVers}-7-Q16-HDRI-x64-dll.exe"
            ExecWait '"$R0\ImageMagick-${ImageMagickVers}-7-Q16-HDRI-x64-dll.exe"'
        ${EndIf}
        WriteINIstr "$INSTDIR\Install.log" "INSTALL" "Image Magic" "Yes"
SectionEnd

Section "IView4D" SEC_IView4D
        StrCpy $R0 $varTempDirDest             ; pour l'extraction des installateurs de Viewers
        CreateDirectory "$R0"
	SetOutPath "$R0"
        ${If} ${TARGET} != Test
	    File /r "${SRC_SSC}\${VER_SSC}\Installation\Viewers\iView4D\win64_iview4d710-b481.exe"
	    ExecWait '"$R0\win64_iview4d710-b481.exe"'
        ${EndIf}

        WriteINIstr "$INSTDIR\Install.log" "INSTALL" "IView4D" "Yes"
SectionEnd

Section "Google Earth" SEC_GoogleEarth
        StrCpy $R0 $varTempDirDest             ; pour l'extraction des installateurs de Viewers
        CreateDirectory "$R0"
	SetOutPath "$R0"
        ${If} ${TARGET} != Test
	    File /r "${SRC_SSC}\${VER_SSC}\Installation\Viewers\GoogleEarthWin.exe"
	    ExecWait '"$R0\GoogleEarthWin.exe"'
        ${EndIf}
        WriteINIstr "$INSTDIR\Install.log" "INSTALL" "GoogleEarth" "Yes"
SectionEnd

Section "Cortona" SEC_Cortona3D
        StrCpy $R0 $varTempDirDest             ; pour l'extraction des installateurs de Viewers
        CreateDirectory "$R0"
	SetOutPath "$R0"
        ${If}  ${TARGET} != Test
                File /r "${SRC_SSC}\${VER_SSC}\Installation\Viewers\VRML\cortona3d-viewer-64bit.msi"
        	ExecWait '"$R0\cortona3d-viewer-64bit.msi"'
        ${EndIf}
        WriteINIstr "$INSTDIR\Install.log" "INSTALL" "Cortona Web Viewer" "Yes"
SectionEnd


Section "Konsgberg Install " SEC_Kongsberg
        StrCpy $R0 $varTempDirDest             ; pour l'extraction des installateurs de Viewers
        CreateDirectory "$R0"
	SetOutPath "$R0"
        ${If} ${TARGET} != Test
            ; Test de pr�sence du logiciel WinPCap (capture de trames r�seau embarqu� avec Datagram Viewer)
            ; La version 4.1.2 correspond � la version d�ploy� par SonarRecordViewer.
            ReadRegStr $R3 HKLM "SOFTWARE\Wow6432Node\WinPcap" ""
            ${If} $R3 == ""
	          File /r "${SRC_SSC}\${VER_SSC}\Installation\Viewers\WinPcap_4_1_3.exe"
                  ExecWait '"$R0\WinPcap_4_1_3.exe"'
            ${EndIf}

            File /r "${SRC_SSC}\${VER_SSC}\Installation\Viewers\SonarRecordViewer\Install Sonar Record Viewer V3.9.4.exe" ; DatagramViewerSetup.exe"
            ExecWait '"$R0\Install Sonar Record Viewer V3.9.4.exe"'

            ; Copie du fichier d'adaptation pour l'EM304.
            ; Lecture du Path Windows
            ReadRegStr $varSRVPath HKLM "SOFTWARE\Classes\Km.Em.All\shell\open\command" ""
            StrCmp $R0 "" KM_SRV_notInstalled
            Push $varSRVPath ; Input string
            ${WordFind} $varSRVPath "\Sonar Record Viewer.exe" "-02" $R1
            ; Suppression du 1er caract�re = ".
            StrCpy $R1 $R1 "" 1
	    SetOutPath $R1
            File /r "${SRC_SSC}\${VER_SSC}\Installation\Viewers\SonarRecordViewer\SRVConfig.xml"

            KM_SRV_notInstalled:

        ${EndIf}
SectionEnd

Section "Acrobat Reader" SEC_AdobeReader
        StrCpy $R0 $varTempDirDest             ; pour l'extraction du SetUp d'aCrobat Reader
        CreateDirectory "$R0"
	SetOutPath "$R0"
        ${If} ${TARGET} != Test
	    File /r "${SRC_SSC}\${VER_SSC}\Installation\Viewers\AdbeRdr811_en_US.exe"
	    ExecWait '"$R0\AdbeRdr811_en_US.exe"'
	${EndIf}
        WriteINIstr "$INSTDIR\Install.log" "INSTALL" "Acrobat Reader 8.1.1" "Yes"

SectionEnd

Section "Panoply" SEC_Panoply
        StrCpy $R0 $varTempDirDest             ; pour l'extraction des installateurs de Viewers
        CreateDirectory "$R0"
	SetOutPath "$R0"
        ${If} ${TARGET} != Test
            ; Version 64 bits
            File /r "${SRC_SSC}\${VER_SSC}\Installation\Viewers\PanoplyWin-${PanoplyVers}.zip"
	    ZipDLL::extractall "$varTempDirDest\PanoplyWin-${PanoplyVers}.zip" "$varExternalFolder"
            ExecWait "cmd /k; del $varTempDirDest\PanoplyWin-${PanoplyVers}.zip & Exit"
        ${EndIf}

        WriteRegStr HKLM "SOFTWARE\Ifremer\SonarScope-Resource" "Panoply" "$varExternalFolder\PanoplyWin\Panoply.exe"

        WriteINIstr "$INSTDIR\Install.log" "INSTALL" "Panoply" "Yes"
        
        WriteRegStr HKCR ".mbg" "" "Caraibes.Bathymetry"
        WriteRegStr HKCR "Caraibes.Bathymetry\shell\open\command" "" '"$varExternalFolder\PanoplyWin\panoply.exe" "%1"'
        WriteRegStr HKCR "Caraibes.Bathymetry\DefaultIcon" "" "$varExternalFolder\PanoplyWin\panoply.exe,0"
        WriteRegStr HKCR ".mnt" "" "Caraibes.ModeleNumeriqueTerrain"
        WriteRegStr HKCR "Caraibes.ModeleNumeriqueTerrain\shell\open\command" "" '"$varExternalFolder\PanoplyWin\panoply.exe" "%1"'
        WriteRegStr HKCR "Caraibes.ModeleNumeriqueTerrain\DefaultIcon" "" "$varExternalFolder\PanoplyWin\panoply.exe,0"
        WriteRegStr HKCR ".nvi" "" "Caraibes.Navigation"
        WriteRegStr HKCR "Caraibes.Navigation\shell\open\command" "" '"$varExternalFolder\PanoplyWin\panoply.exe" "%1"'
        WriteRegStr HKCR "Caraibes.Navigation\DefaultIcon" "" "$varExternalFolder\PanoplyWin\panoply.exe,0"
        WriteRegStr HKCR ".mos" "" "Caraibes.Mosaic"
        WriteRegStr HKCR "Caraibes.Mosaic\shell\open\command" "" '"$varExternalFolder\PanoplyWin\panoply.exe" "%1"'
        WriteRegStr HKCR "Caraibes.Mosaic\DefaultIcon" "" "$varExternalFolder\PanoplyWin\panoply.exe,0"
        WriteRegStr HKCR ".dtm" "" "Caraibes.DTM"
        WriteRegStr HKCR "Caraibes.DigitalTerrestrialModel\shell\open\command" "" '"$varExternalFolder\PanoplyWin\panoply.exe" "%1"'
        WriteRegStr HKCR "Caraibes.DigitalTerrestrialModel\DefaultIcon" "" "$varExternalFolder\PanoplyWin\panoply.exe,0"
        WriteRegStr HKCR ".grd" "" "Grid.RectangularRegular"
        WriteRegStr HKCR "Grid.RectangularRegular\shell\open\command" "" '"$varExternalFolder\PanoplyWin\panoply.exe" "%1"'
        WriteRegStr HKCR "Grid.RectangularRegular\DefaultIcon" "" "$varExternalFolder\PanoplyWin\panoply.exe,0"

SectionEnd


Section "ASCIIDoc_Fx" SEC_AsciiDoc_FX
        StrCpy $R0 $varTempDirDest             ; pour l'extraction des installateurs
        CreateDirectory "$R0"
	SetOutPath "$R0"
        ${If} ${TARGET} != Test
                File /r "${SRC_SSC}\${VER_SSC}\Installation\Viewers\AsciidocFX_Windows.exe"
        	ExecWait '"$R0\AsciidocFX_Windows.exe"'
        ${EndIf}

        WriteINIstr "$INSTDIR\Install.log" "INSTALL" "ASCIIDoc_FX" "Yes"

SectionEnd

Section "CloudCompare" SEC_CloudCompare
        StrCpy $R0 $varTempDirDest  ; pour l'extraction des installateurs de Viewers
        CreateDirectory "$R0"
	SetOutPath "$R0"
        ExecWait '"$R0\CloudCompare_v2.12.beta_setup_x64.exe"'
        WriteINIstr "$INSTDIR\Install.log" "INSTALL" "CloudCompare" "Yes"
SectionEnd


SectionGroupEnd

Section "MatLab Component Run-Time" SEC_MCR

        StrCpy $R0 $varTempDirDest  ; pour l'extraction des installateurs de Viewers
        CreateDirectory "$R0"
	SetOutPath "$R0"
        ExecWait '"$R0\MCRInstaller.exe"'
        WriteINIstr "$INSTDIR\Install.log" "INSTALL" "MCR" "Yes"

SectionEnd

Section "Microsoft Studio Visual C++ Run-Time" SEC_MSVCR

        StrCpy $R0 $varTempDirDest  ; pour l'extraction des installateurs de Viewers
        CreateDirectory "$R0"
	SetOutPath "$R0"
        ${If} ${TARGET} != Test
 	    File /r "${SRC_SSC}\${VER_SSC}\Installation\MSVCR\${MSVCRVers}\vcredist_x64.exe"
	    ExecWait '"$R0\vcredist_x64.exe"'
        ${EndIf}
        WriteINIstr "$INSTDIR\Install.log" "INSTALL" "MSVCR ${MSVCRVers}" "Yes"
SectionEnd

Section "GDAL" SEC_GDAL

        StrCpy $varFlagGDALInstall "1"
        StrCpy $R0 $varTempDirDest  ; pour l'extraction des installateurs de Viewers
        CreateDirectory "$R0"
	SetOutPath "$R0"
        ; On d�ploie ces ex�cutables gdal uniquement en 64 bits.
        ; En version 32 bits, gdal est partiellement d�ploy� depuis le SetUp SonarScope.
        ${If} ${TARGET} != Test
           File /r "${SRC_SSC}\${VER_SSC}\Installation\Viewers\gdal\gdal-204-1911-x64-core.msi"
	   ExecWait '"msiexec" /i "$R0\gdal-204-1911-x64-core.msi"'
        ${EndIf}
        WriteINIstr "$INSTDIR\Install.log" "INSTALL" "GDAL" "Yes"


SectionEnd

SectionGroup "GMT - Tools"

Section "GMT Installation"  SEC_GMT

        ${If} ${TARGET} != Test
           
           StrCpy $varFlagGMTInstall "1"

	   ; Copie des sources de GMT
           StrCpy $R0 $varTempDirDest  ; pour l'extraction des installateurs de Viewers
           CreateDirectory "$R0"
	   SetOutPath "$R0"
           ${If} ${TARGET} != Test
               File /r "${SRC_SSC}\${VER_SSC}\Installation\Viewers\gmt-${GMTVers}-win64.exe"
               ExecWait '"$R0\gmt-${GMTVers}-win64.exe"'
           ${EndIf}

           WriteINIstr "$INSTDIR\Install.log" "INSTALL" "GMT" "Yes"
	${EndIf}

SectionEnd

Section "GhostScript" SEC_GhostScript
        StrCpy $R0 $varTempDirDest             ; pour l'extraction des installateurs de Viewers
        CreateDirectory "$R0"
	SetOutPath "$R0"
	${If} ${TARGET} != Test
	    File /r "${SRC_SSC}\${VER_SSC}\Installation\Viewers\gs952w64.exe"
	    ExecWait '"$R0\gs925w32.exe"'
        ${EndIf}
	
        WriteINIstr "$INSTDIR\Install.log" "INSTALL" "GhostScript" "Yes"
SectionEnd

Section "GhostView" SEC_GhostView
	${If} ${TARGET} != Test
           ; Positionnement des variables apr�s effacement pr�alable.
           StrCpy $R0 "$varExternalFolder\Ghostgum\gsview"
           Push $R0
           ; Ajout dans le Path en auto ou semi-auto selon la longueur de la chaine courante du PATH.
           ; Lecture syst�matique du Path windows au cas o� il aurait chang�.
           ReadRegStr $varWindowsPath HKLM "SYSTEM\CurrentControlSet\Control\Session Manager\Environment" "Path"
           ${If} $varWindowsPath != ""
                ${EnvVarUpdate} $0 "PATH" "R" "HKLM" "$R0"
                ;OLD Call RemoveFromPath
                ${EnvVarUpdate} $0 "PATH" "A" "HKLM" "$R0"
                ;OLD Call AddToPath
           ${Else}
                MessageBOX MB_ICONEXCLAMATION  $(warn_pathTooLong)
                /*
                ${WriteToFile} "$TEMP\sscExternal_setPATH.bat" "$\r$\n"
                ${WriteToFile} "$TEMP\sscExternal_setPATH.bat" "call rmpath $R0$\r$\n"
                ${WriteToFile} "$TEMP\sscExternal_setPATH.bat" "$\r$\n"
                ${WriteToFile} "$TEMP\sscExternal_setPATH.bat" "SET path=%path%;$R0$\r$\n"
                */
           ${EndIf}
 
           StrCpy $R0 $varTempDirDest             ; pour l'extraction des installateurs de Viewers
           CreateDirectory "$R0"
	   SetOutPath "$R0"
	   File /r "${SRC_SSC}\${VER_SSC}\Installation\Viewers\gsv50w64.exe"
	   ExecWait '"$R0\gsv50w64.exe"'
        ${EndIf}
        WriteINIstr "$INSTDIR\Install.log" "INSTALL" "GhostView" "Yes"
SectionEnd

SectionGroupEnd


Section -FinishSection

        ; Obtention du flag d'installation du MCR
        SectionGetFlags ${SEC_MCR} $varInstallMCR
        IntOp $varInstallMCR $varInstallMCR & ${SF_SELECTED}
        ${If} $varInstallMCR == ${SF_SELECTED}
              /*
              ; La construction du lien HTTP d�pend de la version. C'est trop compliqu� � construire, on remplace par une fene^tre dialogue
              ; � charge de l'op�rateur de dowloader lui-m�me.
              ; Autre solution : tester le lien HTTP avec https://nsis-dev.github.io/NSIS-Forums/html/t-302297.html
              ; StrCpy $varHTTPLinkMCR "http://ssd.mathworks.com/supportfiles/downloads/${VER_MATLAB}/deployment_files/${VER_MATLAB}/installers/win64/MCR_${VER_MATLAB}_win64_installer.exe"
              ; R2019a : StrCpy $varHTTPLinkMCR "http://ssd.mathworks.com/supportfiles/downloads/${VER_MATLAB}/Release/$varMCRUpdate/deployment_files/installer/complete/win64/MATLAB_Runtime_${VER_MATLAB}_Update_$varMCRUpdate_win64.zip"
              ${OpenURL} $varHTTPLinkMCR
              */

              Strcpy $1 "This setup should open the link https://mathworks.com/products/compiler/matlab-runtime.html$\r$\n\
                        Please select the Windows 64 bits version of ${VER_MATLAB}. Then launch the installation once the download is over."
	      DetailPrint "FormatStr: $1"
              MessageBox MB_YESNO|MB_ICONQUESTION "$1 $\r$\n$\r$\n Do you want to download it now ?" IDNO +2
              ExecShell open "https://fr.mathworks.com/products/compiler/matlab-runtime.html"

        ${EndIf}
    
        ; Obtention du flag d'installation du CloudCompare
        SectionGetFlags ${SEC_CloudCompare} $varInstallCloudCompare
        IntOp $varInstallCloudCompare $varInstallCloudCompare & ${SF_SELECTED}
        ${If} $varInstallCloudCompare == ${SF_SELECTED}

              Strcpy $1 "This setup should open the link https://www.danielgm.net/cc/release/CloudCompare_v2.12.beta_setup_x64.exe$\r$\n\
                        Then launch the installation once the download is over."
	      DetailPrint "FormatStr: $1"
              MessageBox MB_YESNO|MB_ICONQUESTION "$1 $\r$\n$\r$\n Do you want to download it now ?" IDNO +2
              ExecShell open "https://www.danielgm.net/cc/release/CloudCompare_v2.12.beta_setup_x64.exe"

        ${EndIf}
        
        ; Forcage des Flags de section
        SectionSetFlags ${SEC_AdobeReader} 0

        ; Forcage des Flags de section
        SectionSetFlags ${SEC_Panoply} 0

        ; Forcage du chemin de localisation des applications ErViewer et Image Magic
        StrCpy $2 "C:\Program Files"
        ;Push $2

        ; Cr�ation du r�pertoire de ressources.
        StrCpy "$varRepCibleRsc" "C:\SonarScope-Resource"
        
        ; Ecriture des localisations des Viewers dans la base de registres
        ; apr�s recherche dans cette m�me base des logiciels.
        ReadRegStr $0 HKLM "SOFTWARE\Earth Resource Mapping\ERViewer(libversion${ERViewerVers})" "BASE_PATH"
        ${If} $0 != ""
           WriteRegStr HKLM "SOFTWARE\Ifremer\SonarScope-Resource" "VisualisateurImageExterne1" "$0\bin\ERViewer.exe"
        ${Else}
           WriteRegStr HKLM "SOFTWARE\Ifremer\SonarScope-Resource" "VisualisateurImageExterne1" "$2\Earth Resource Mapping\ER Viewer ${ERViewerVers}\bin\ERViewer.exe"
        ${EndIf}
        
        ReadRegStr $0 HKLM "SOFTWARE\ImageMagick\${ImageMagickVers}\Q:16" "BinPath"
        ${If} $0 != ""
           WriteRegStr HKLM "SOFTWARE\Ifremer\SonarScope-Resource" "VisualisateurImageExterne2" "$0\imdisplay.exe"
         ${Else}
           WriteRegStr HKLM "SOFTWARE\Ifremer\SonarScope-Resource" "VisualisateurImageExterne2" "$2\ImageMagick-${ImageMagickVers}-Q16-HDRI\imdisplay.exe"
        ${EndIf}

        ; OLD WriteRegStr HKLM "SOFTWARE\Ifremer\SonarScope-Resource" "Visualisateur3D" "$varExternalFolder\iView3D\iview3d.exe"
        WriteRegStr HKLM "SOFTWARE\Ifremer\SonarScope-Resource" "Visualisateur3D" "$2\IVS7\bin\iview4d.exe"

        ; Ecriture des localisations des Viewers par recherche dans la base de registres.
        ReadRegStr $0 HKCU "SOFTWARE\Google\Google Earth Plus\autoupdate" "AppPath"
        ${If} $0 != ""
           WriteRegStr HKLM "SOFTWARE\Ifremer\SonarScope-Resource" "Google Earth" "$0\GoogleEarth.exe"
        ${Else}
           WriteRegStr HKLM "SOFTWARE\Ifremer\SonarScope-Resource" "Google Earth" "$varExternalFolder\Google\Google Earth\GoogleEarth.exe"
        ${EndIf}

        ; Ecriture des localisations des Viewers par recherhce dans la base de registres.
        ReadRegStr $0 HKCU "SOFTWARE\NASA\World Wind" ""
        ${If} $0 != ""
           WriteRegStr HKLM "SOFTWARE\Ifremer\SonarScope-Resource" "NASA World Wind" "$0"
        ${Else}
           WriteRegStr HKLM "SOFTWARE\Ifremer\SonarScope-Resource" "NASA World Wind" "$varExternalFolder\NASA\World Wind ${NASAWWVers}"
        ${EndIf}

        ${If} $varFlagGDALInstall == "1"
                StrCpy $R0 "$PROGRAMFILES64\GDAL\"
                StrCpy $R1 "$PROGRAMFILES64\GDAL\gdal-data"
                Push $R0

                ; Effacement pr�alable de la variable d'environnement GDAL
                ${EnvVarUpdate} $0 "GDAL_HOME" "R" "HKLM" "$R0"
                ; Ajout
                ${EnvVarUpdate} $0 "GDAL_HOME" "A" "HKLM" "$R0"
                ; Effacement pr�alable de la variable d'environnement GDAL_DATA
                ${EnvVarUpdate} $0 "GDAL_DATA" "R" "HKLM" "$R1"
                ; Ajout
                ${EnvVarUpdate} $0 "GDAL_DATA" "A" "HKLM" "$R1"

                ReadRegStr $varWindowsPath HKLM "SYSTEM\CurrentControlSet\Control\Session Manager\Environment" "Path"
                ${If} $varWindowsPath != ""
                        ; Retouche du Path pour ajout des binaires GDAL
                        ${EnvVarUpdate} $0 "PATH" "A" "HKLM" "$R0"
                ${Else}
                        MessageBOX MB_ICONEXCLAMATION  $(info_manually_GDAL)
                ${EndIf}

                WriteINIstr "$INSTDIR\Install.log" "INSTALL" "GDAL" "Yes"
                
        ${EndIf}
 
	; Appel de l'�criture de la configuration d'installation.
	FlushINI "$INSTDIR\Install.log"

        ; Suppression des executables copi�s sous C:\TEMP
        StrCpy $R0 $varTempDirDest
        ExecWait "cmd /k; del $R0\ERViewer${ERViewerVers}.exe & del $R0\ImageMagick-7.0.7-6-Q8-x86-dll.exe & del $R0\win64_iview4d710-b481.exe & Exit"
	; 10/10/08 FWTOOLS Inutile depuis le tuilage depuis SonarScope
	ExecWait "cmd /k; del $R0\gs952w64.exe & del $R0\gsv50w64.exe & Exit"

        ; Suppression de l'installeur de Google Earth
        ExecWait "cmd /k; del $R0\GoogleEarthWin.exe & Exit"

        /*
        ; Lancement du fichier de changement du path si celui-ci est plus long que 1024 Car.
        ; Lecture syst�matique du Path windows au cas o� il aurait chang�.
        ReadRegStr $varWindowsPath HKLM "SYSTEM\CurrentControlSet\Control\Session Manager\Environment" "Path"
        ${If} $varWindowsPath == ""
              ExecWait "cmd /k;$TEMP\sscExternal_setPATH.bat"
        ${EndIf}
        */

SectionEnd ; fin de FinishSection

;Uninstall section
Section Uninstall

        Call un.Supprime

        ; Ajout dans le Path en auto ou semi-auto selon la longueur de la chaine courante du PATH.
        ; Lecture syst�matique du Path windows au cas o� il aurait chang�.
        ReadRegStr $varWindowsPath HKLM "SYSTEM\CurrentControlSet\Control\Session Manager\Environment" "Path"
        ${If} $varWindowsPath != ""
              StrCpy $R0 "$varExternalFolder\Ghostgum\gsview"
              Push $R0
              ${un.EnvVarUpdate} $0 "PATH" "R" "HKLM" "$R0"
        ${EndIf}
        StrCpy $R0 "$PROGRAMFILES64\GDAL\"
        Push $R0
        ${un.EnvVarUpdate} $0 "PATH" "R" "HKLM" "$R0"

        
SectionEnd

; ---- Positionnement du Flag de la section.
Function .onSelChange

  ; Flag=1, for�age de l'installation
  ;OLD SectionSetFlags ${SEC_AdobeReader} 0

FunctionEnd

; *******************************************************************
; Fonctions
Function un.Supprime

;Var /GLOBAL pathInstall

	; En attendant de customiser la d�sinstallation
	;RMDir /r "C:\GMT"

FunctionEnd

	
; *******************************************************************

; Description des composants
!insertmacro MUI_FUNCTION_DESCRIPTION_BEGIN
	!insertmacro MUI_DESCRIPTION_TEXT ${SEC_MCR} $(info_sec_MCR)
	!insertmacro MUI_DESCRIPTION_TEXT ${SEC_ERviewer} $(info_sec_ERViewer)
	!insertmacro MUI_DESCRIPTION_TEXT ${SEC_ImageMagick} $(info_ImageMagick)
	!insertmacro MUI_DESCRIPTION_TEXT ${SEC_IView4D} $(info_IView4D)
	!insertmacro MUI_DESCRIPTION_TEXT ${SEC_GoogleEarth} $(info_GoogleEarth)
	!insertmacro MUI_DESCRIPTION_TEXT ${SEC_Cortona3D} $(info_Cortona3D)
        !insertmacro MUI_DESCRIPTION_TEXT ${SEC_Kongsberg} $(info_Kongsberg)
	!insertmacro MUI_DESCRIPTION_TEXT ${SEC_AdobeReader} $(info_AdobeReader)
	!insertmacro MUI_DESCRIPTION_TEXT ${SEC_Panoply} $(info_Panoply)
	!insertmacro MUI_DESCRIPTION_TEXT ${SEC_GMT} $(info_GMT)
	!insertmacro MUI_DESCRIPTION_TEXT ${SEC_GDAL} $(info_GDAL)
	!insertmacro MUI_DESCRIPTION_TEXT ${SEC_AsciiDoc_FX} $(info_AsciiDocFx)
	!insertmacro MUI_DESCRIPTION_TEXT ${SEC_GhostScript} $(info_GhostScript)
	!insertmacro MUI_DESCRIPTION_TEXT ${SEC_GhostView} $(info_GhostView)
	!insertmacro MUI_DESCRIPTION_TEXT ${SEC_MSVCR} $(info_MSVCR)
!insertmacro MUI_FUNCTION_DESCRIPTION_END

; --------------------------------
; Visualisation du ReadMe si l'utilisateur le demande
Function ReadMe
    SetOutPath "$INSTDIR\"
    ${If} $LANGUAGE == ${LANG_FRENCH}
        Exec "notepad.exe $INSTDIR\readme_fr.txt"
    ${Else}
        Exec "notepad.exe $INSTDIR\readme_us.txt"
    ${EndIf}
FunctionEnd


; --------------------------------
; Validation des sections de composants
Function Pre_InstallComponents_Page


    ; Identification de la version Windows (Ex : Windows 8 --> $R0 = 8.1)
    ${GetWindowsVersion} $R1

    ; Comparaison case-insensitive string tests (si la version est sup�rieure � Windows Seven)
    ${If} $R1 == "XP"
           ReadRegStr $0 HKLM "SOFTWARE\MATLAB\MCRPathDir\" "MCRPathDir"
           ; Comptage de l'occurence du num de version dans le path
           ; Retourne $0 si il ne trouve pas l'occurrence.
           ${WordFind} "$0" $varMCRVersion "#" $varFlagMCR
    ${Else}
          ReadRegStr $0 HKLM "SOFTWARE\MathWorks\$pathMCRVersion\$varMCRVERSION2" "MATLABROOT"
          ${If} $0 == ""
               ; For�age du Test pour dire que MCR est � installer.
               StrCpy $varFlagMCR ""
          ${EndIf}
    ${EndIf}


    SectionGetFlags ${SEC_MCR} $R0
    IntOp $R0 $R0 & ${SF_SELECTED}
    ${If} $R0 == ${SF_SELECTED}
        ${If} $0 == $varFlagMCR
           ;MessageBox MB_ICONEXCLAMATION  $(warn_NO_MCR)
        ${ElseIf} $0 != ""
           ;MessageBox MB_ICONEXCLAMATION  $(warn_MCR)
           SectionSetFlags ${SEC_MCR} 0
        ${EndIf}
    ${EndIf}

    IntOp $0 0 + 0
    SectionGetFlags ${SEC_MSVCR} $R0
    IntOp $R0 $R0 & ${SF_SELECTED}
    ifFileExists "C:\Windows\System32\Msvcr90.dll" 0 +2
    IntOp $0 0 + 1
    ${If} $R0 == ${SF_SELECTED}
        ${If} $0 == $varFlagMSVCR
           ;MessageBox MB_ICONEXCLAMATION  $(warn_NO_MSVCR)
        ${ElseIf} $0 != ""
           ;MessageBox MB_ICONEXCLAMATION  $(warn_MSVCR)
           SectionSetFlags ${SEC_MSVCR} 0
        ${EndIf}
    ${EndIf}

        ; V�rification de l'existence des Viewers avant une s�lection d'installation.
    SectionGetFlags ${SEC_ERViewer} $R0
    IntOp $R0 $R0 & ${SF_SELECTED}
    ReadRegStr $0 HKLM "SOFTWARE\Wow6432Node\Earth Resource Mapping\ERViewer(libversion${ERViewerVers})" "BASE_PATH"

    ${If} $R0 == ${SF_SELECTED}
    ${AndIf} $0 != ""
        ;MessageBox MB_ICONEXCLAMATION  $(warn_ERViewer)
        SectionSetFlags ${SEC_ERViewer} 0
    ${EndIf}

    SectionGetFlags ${SEC_ImageMagick} $R0
    IntOp $R0 $R0 & ${SF_SELECTED}
    ReadRegStr $0 HKLM "SOFTWARE\Wow6432Node\ImageMagick\${ImageMagickVers}\Q:16" "BinPath"

    ${If} $R0 == ${SF_SELECTED}
    ${AndIf} $0 != ""
        ; MessageBox MB_ICONEXCLAMATION  $(warn_ImageMagick)
        SectionSetFlags ${SEC_ImageMagick} 0
    ${EndIf}
    
    SectionGetFlags ${SEC_IView4D} $R0
    IntOp $R0 $R0 & ${SF_SELECTED}
    ${If} $R0 == ${SF_SELECTED}
        ifFileExists "$varExternalFolder\IVS7\bin\iview4d.exe" 0 +3
        ;MessageBox MB_ICONEXCLAMATION  $(warn_iView4D)
        SectionSetFlags ${SEC_IView4D} 0
    ${EndIf}

    SectionSetFlags ${SEC_AsciiDoc_FX} 0
    
    SectionSetFlags ${SEC_Panoply} 0
  
    
    ; D�coch� par d�faut (demande de JMA le 2015/03/24)
    SectionSetFlags ${SEC_GoogleEarth} 0

    SectionSetFlags ${SEC_Kongsberg} 0

    ; Evolution : for�age du skip de l'installation d'Adobe Reader.
    SectionSetFlags ${SEC_AdobeReader} 0
    ; V�rification de l'existence de Acrobat Reader avant une s�lection d'installation.
    SectionGetFlags ${SEC_AdobeReader} $R0
    IntOp $R0 $R0 & ${SF_SELECTED}
    ; Lecture du chemin de Adobe Reader
    ReadRegStr $0 HKLM "SOFTWARE\Adobe\Acrobat Reader\5.0\InstallPath" ""
    ${If} $0 == ""
          ReadRegStr $0 HKLM "SOFTWARE\Adobe\Acrobat Reader\6.0\InstallPath" ""
          ${If} $0 == ""
                ReadRegStr $0 HKLM "SOFTWARE\Adobe\Acrobat Reader\7.0\InstallPath" ""
                ${If} $0 == ""
                      ReadRegStr $0 HKLM "SOFTWARE\Adobe\Acrobat Reader\8.0\InstallPath" ""
                ${EndIf}
          ${EndIf}
    ${EndIf}

    ${If} $R0 == ${SF_SELECTED}
    ${AndIf} $0 != ""
             ;MessageBox MB_ICONEXCLAMATION  $(warn_AcrobatReader)
             SectionSetFlags ${SEC_AdobeReader} 0
    ${EndIf}

    ; V�rification de l'existence des Viewers avant une s�lection d'installation.
    SectionGetFlags ${SEC_AsciiDoc_FX} $R0
    IntOp $R0 $R0 & ${SF_SELECTED}
    ReadRegStr $0 HKCU "SOFTWARE\ej-technologies\exe4j\pids" ""
    ${If} $R0 == ${SF_SELECTED}
    ${AndIf} $0 != ""
             ;MessageBox MB_ICONEXCLAMATION  $(warn_AsciiDoc_FX)
             SectionSetFlags ${SEC_AsciiDoc_FX} 0
    ${EndIf}

    ; Inhibition de l'installation de GDAL sur d�tection de la variable d'environnement
    ; globale GDAL_HOME
    SectionGetFlags ${SEC_GDAL} $R0
    IntOp $R0 $R0 & ${SF_SELECTED}
    ReadEnvStr $0 GDAL_HOME
    ${If} $R0 == ${SF_SELECTED}
    ${AndIf} $0 != ""
        ;MessageBox MB_ICONEXCLAMATION $(warn_GDAL)
        SectionSetFlags ${SEC_GDAL} 0
    ${EndIf}

    ; Inhibition de l'installation de GMT sur d�tection de la variable d'environnement
    ; globale GMTHOME
    SectionGetFlags ${SEC_GMT} $R0
    IntOp $R0 $R0 & ${SF_SELECTED}
    ReadEnvStr $0 GMT6_SHAREDIR
    ${If} $R0 == ${SF_SELECTED}
    ${AndIf} $0 != ""
        ;MessageBox MB_ICONEXCLAMATION $(warn_GMT)
        SectionSetFlags ${SEC_GMT} 0
    ${EndIf}

    ; Inhibition de l'installation de GhostView avant une s�lection d'installation.
    SectionGetFlags ${SEC_GhostView} $R0
    IntOp $R0 $R0 & ${SF_SELECTED}
    ReadRegStr $0 HKLM "SOFTWARE\GhostGum\GSView" ${GhostViewVers}
 
    ${If} $R0 == ${SF_SELECTED}
    ${AndIf} $0 != ""
         ;MessageBox MB_ICONEXCLAMATION $(warn_GhostView)
         SectionSetFlags ${SEC_GhostView} 0
    ${EndIf}

    ; Inhibition de l'installation de GhostScript avant une s�lection d'installation.
    SectionGetFlags ${SEC_GhostScript} $R0
    IntOp $R0 $R0 & ${SF_SELECTED}
    ReadRegStr $0 HKLM "SOFTWARE\Wow6432Node\AFPL Ghostscript\${GhostScriptVers}" "GS_LIB"
    ${If} $R0 == ${SF_SELECTED}
    ${AndIf} $0 != ""
         ;MessageBox MB_ICONEXCLAMATION $(warn_GhostScript)
         SectionSetFlags ${SEC_GhostScript} 0
    ${EndIf}
    
    ; D�coch� par d�faut (demande de JMA le 2016/09/21)
    SectionSetFlags ${SEC_Cortona3D} 0


FunctionEnd ; fin de Pre_InstallComponents_Page

; --------------------------------
; Positionnement des infos param�tres.
Function InstallComponents_Page

    ; Identification de la version Windows (Ex : Windows 8 --> $R0 = 8.1)
    ${GetWindowsVersion} $R1

    ; Comparaison case-insensitive string tests (si la version est sup�rieure � Windows Seven)
    ${If} $R1 == "XP"
           ReadRegStr $0 HKLM "SOFTWARE\MATLAB\MCRPathDir\" "MCRPathDir"
           ; Comptage de l'occurence du num de version dans le path
           ; Retourne $0 si il ne trouve pas l'occurrence.
           ${WordFind} "$0" $varMCRVERSION "#" $varFlagMCR
    ${Else}
          ReadRegStr $0 HKLM "SOFTWARE\MathWorks\$pathMCRVersion\$varMCRVERSION2" "MATLABROOT"
          ${If} $0 == ""
               ; For�age du Test pour dire que MCR est � installer.
               StrCpy $varFlagMCR ""
          ${EndIf}
    ${EndIf}
    
    ifFileExists "C:\Windows\System32\Msvcr90.dll" 0 +2
    ; For�age du Test pour dire que Microsfot Visual Studio CR est � installer.
    StrCpy $varFlagMSVCR ""

FunctionEnd ; Fin de InstallComponents_Page

Function Pre_SSCExternalPath_Page

   ; Chargement avec le dossier program files semon la machine cible.
   StrCpy $varExternalFolder $PROGRAMFILES64
   WriteINIStr "$PLUGINSDIR\SSCExternalPath.ini" "Field 6" "State" $varExternalFolder
   FlushIni "$PLUGINSDIR\SSCExternalPath.ini"

   InstallOptions::initDialog /NOUNLOAD "$PLUGINSDIR\SSCExternalPath.ini"

   ; In this mode InstallOptions returns the window handle so we can use it
   Pop $varHdlWindow
   ; Now show the dialog and wait for it to finish
   InstallOptions::show
   ; Finally fetch the InstallOptions status value (we don't care what it is though)
   Pop $0

FunctionEnd ; fin de SSCExternalPath_Page

Function SSCExternalPath_Page


  ; Lecture du Handle de Window courante.
  FindWindow $varHdlWindow "#32770" "" $HWNDPARENT

  ; Lecture du .ini pour interagir sur la fen�tre sur les boutons activ� ou non.
  ReadINIStr $1 "$PLUGINSDIR\SSCExternalPath.ini" "Settings" "State"
  ${If} $1 != 0
     ReadINIStr $0 "$PLUGINSDIR\SSCExternalPath.ini" "Field 3" "state"
     ${If} $0 != 0
         GetDlgItem $2 $varHdlWindow 1209 ; 1200 + FieldNumber
         EnableWindow $2 1
     ${Else}
         GetDlgItem $2 $varHdlWindow 1209 ; 1200 + FieldNumber
         EnableWindow $2 0
     ${EndIf}
  ${EndIf}

  ; R�cup�ration de la touche s�lectionn�.
  ${If} $1 != 0
     Abort ; Return to the page
  ${Else}
     ${If} $0 != 0
        ReadINIStr $varExternalFolder "$PLUGINSDIR\SSCExternalPath.ini" "Field 9" "state"
     ${EndIf}
  ${EndIf}

  ; Lecture du chemin temporaire d'installation des ex�cutables.
  ReadINIStr $varTempDirDest "$PLUGINSDIR\SSCExternalPath.ini" "Field 8" "state"
  
FunctionEnd ; Fin de SSCExternalPath_Page


;--------------------------------
;Uninstaller Functions

Function un.onInit
         ; On force la d�sintallation en anglais.
         StrCpy $LANGUAGE ${LANG_ENGLISH}
         ;!insertmacro MUI_UNGETLANGUAGE
FunctionEnd
