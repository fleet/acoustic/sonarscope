INTERACTIVE VISUALIZATION SYSTEMS INC.
LICENSE AND LIMITED WARRANTY AGREEMENT

IMPORTANT: This program is only to be used for viewing files created with the Fledermaus software suite.

NOTICE:   You should carefully read this License and Limited Warranty Agreement ("License Agreement") before  installing  the software. By installing, copying, downloading, accessing or otherwise using the software, you agree to all of the terms and conditions hereof. If you do not agree to these terms and conditions, do not install or use the software. You may return the software together with any accompanying documentation and associated components and a copy of your invoice to the place of purchase. You will be reimbursed or credited in accordance with the current policy of Interactive Visualization Systems Inc., or the representative from whom you purchased the software.

Software License:    

Grant of License: Subject to payment of the license fee, Interactive Visualization Systems Inc. grants to you a non-exclusive, non-transferable license to use the software on a single computer. A separate license agreement and fee is required for each computer on which the software is used.  This License Agreement does not provide the right to distribute, rent, sub-license, lease or otherwise transfer the software or documentation. You may not modify, adapt, translate, or create any derivative works based on,  the software or documentation, and you may not decompile, disassemble, or reverse engineer the software.

Term:  The license granted hereby is effective until terminated. You may terminate it at any time by returning the software (and all copies in any form) and documentation to Interactive Visualization Systems Inc. or by destroying same.  It will also terminate upon conditions set forth elsewhere in this License Agreement or your breach of any of the provisions of this License Agreement. 

Ownership:  The license granted hereby does not transfer to you  any right, title or interest in the software except as specifically set forth in this License Agreement. The software and documentation are protected under copyright laws. Portions of the software may have been developed by an independent third party software supplier that holds copyrights and other proprietary rights to some software routines or modules. You may be held responsible by this supplier for any infringement of such rights by you. 

Duplication Limitation: The software may not be duplicated or copied except as reasonably required for back-up purposes.  All removable copies made must bear the copyright notice contained on the original media. You agree to destroy all copies (in any form) upon termination of  your license. 

Limited Warranty.  Interactive Visualization Systems Inc. warrants that the software will perform substantially in accordance with its accompanying documentation for a period of one year from the date of purchase.   This warranty does not mean that the software will meet your requirements or that the operation of the software will be uninterrupted or error free.

Customer Remedies.  Interactive Visualization Systems Inc.'s entire liability hereunder, and your exclusive remedy, shall be, at Interactive Visualization Systems Inc.'s option, either: (a) termination of the License Agreement and refund of the license fee upon return of defective software and accompanying documentation; or (b) repair or replacement of defective software which is returned. Interactive Visualization Systems Inc. shall  have no liability hereunder if failure of the software has resulted from accident, abuse or misapplication. All replacements will be warranted for the remainder of the original warranty period. 

NO OTHER WARRANTIES. INTERACTIVE VISUALIZATION SYSTEMS INC. DISCLAIMS ALL OTHER WARRANTIES WITH RESPECT TO THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.  

LIMITATION OF LIABILITY. INTERACTIVE VISUALIZATION SYSTEMS INC.'S ENTIRE LIABILITY HEREUNDER SHALL BE LIMITED TO THE AMOUNT OF THE LICENSE FEE PAID BY YOU. IN NO EVENT SHALL INTERACTIVE VISUALIZATION SYSTEMS INC. OR ITS SUPPLIERS BE LIABLE TO YOU FOR ANY  DAMAGES WHATSOEVER (INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF PROFITS, LOSS OF BUSINESS, BUSINESS INTERRUPTION, LOSS OF INFORMATION, OR ANY OTHER INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES) WHETHER BASED ON BREACH OF CONTRACT, TORT (INCLUDING NEGLIGENCE), PRODUCT LIABILITY OR OTHERWISE, EVEN IF INTERACTIVE VISUALIZATION SYSTEMS INC. HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.

This License Agreement is governed by the laws of the province of New Brunswick, Canada.
