---------------------------------------------------------------------
iView3D Information - Readme file.

Copyright 2002-2003, by Interactive Visualization Systems Inc.

- Last Updated - June 14th, 2004 - for Version 6.1b

---------------------------------------------------------------------

INSTALLATION FOR LINUX

Newer releases of Linux require an additional file in order for the
setup and iView3D applications to run.  Simply copy the file
libstdc++-libc6.2-2.so.3 to the /usr/lib directory before installing.
This file is included with the iView3D distribution in the data/
directory.

---------------------------------------------------------------------

iView3D is a program designed to view Fledermaus data files in either
'.sd' or '.scene' format. Fledermaus is a professional visualization
and analysis package capable of viewing large amounts of data in a
variety of formats, including digital terrain maps, images, points,
lines, models, etc. This viewer contains only a small portion of the
features of the full visualization package. For more information on
the Fledermaus package, visit 'http://www.ivs3d.com/'.

---------------------------------------------------------------------

For more information about using iView3D, run the program and select
the 'Help > Navigating the Scene' menu option. For information on
running iView3D from the command line, run 'iview3d -h'.

---------------------------------------------------------------------

If you have downloaded the Linux, Sun, or SGI version of iView3D,
start the installation process by typing in './setup' and 
pressing enter.

---------------------------------------------------------------------
