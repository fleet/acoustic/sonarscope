function Y = identificationLoginExtranet(X)

switch X
    case 'ml1eb76'
        Y = 'Mathieu Levesque - CIDCO';
    case 'md1ec34'
        Y = 'Miles Dunkin - NIWA';
    case 'ag1eb8a'
        Y = 'Andr� Godin - SHC';
    case 'mr1ed18'
        Y = 'Marc Roche - SPFE';
    case 'ai1ec6a'
        Y = 'Alexandre Dano - Geo-Azur';
    case 'ad1eb6e'
        Y = 'Alexandre Dano - Geo-Azur';
    case 'yl1ed12'
        Y = 'Yves Le Gonidec - Geo-Azur';
    case 'ms1ec67'
        Y = 'Sorin Moga - ENST';
    case 'vv1ed38'
        Y = 'V Van Lancker _ Univ Gent';
    case 'js1ed39'
        Y = 'J Siwabessy - CSIRO';
    case 'cs1ec4f'
        Y = 'Christophe Sintes - ENST';
    case 'sb1ed37'
        Y = 'S Bern� - Univ Perpignan';
    case 'vh1f33f'
        Y = 'V Hanquiez - Univ Bordeaux';
    case 'bc1f10e'
        Y = 'UNH - B Calder';
    case 'rg2236d'
        Y = 'R Gallou - Quiet-Oceans';
    case 'cv1ef59'
        Y = 'C Vrignaud - SHOM';
    case 'ja00636'
        Y = 'JM Augustin';
    case 'kg1f610'
        Y = 'Kevin GUSTAFSON - Kongsberg-USA';
    case 'aj1f454'
        Y = 'CREOCEAN';
    case 'cd1eacb'
        Y = 'Christine Deplus - IPGP';
    case 'pp1f10d'
        Y = 'Reson';
    case 'eb01981'
        Y = 'Eric Bernard - Ifremer';
    case 'bb1f110'
        Y = 'Frank-Solberg Wilhelmsen - Kongsberg';
    case 'ag05b23'
        Y = 'Arnaud Gaillot - Ifremer';
    case 'js054fa'
        Y = 'Jean-Marc Sinquin - Ifremer';      

    % Compte apparemment jamais utilis�s
    case 'rc1e94a '
        Y = 'Bob Courtney - BIO';
    case 'ajuif '
        Y = 'Alain Juif - CREOCEAN';
    case 'so1f3ad'
        Y = 'Suzanne OHARA - Lamont';
    case 'jr1ef33'
        Y = 'JC Rosada - SHOM';
        
    otherwise
        Y = [X ' : Non identifi�'];
        my_warndlg(Y, 0)
end
