To visualize an image in Nasa-World-Wind SonarScope generates 2 things which are needed by NWW:
- an .xml file which is paced in C:\Program Files\NASA\World Wind 1.4\Config\Earth (Ex : Prismed125.xml)
- a directory which contains the tiling pyramids. (Ex : C:\TEMP\output_tiles\Prismed125)

SonarScope creates automaticaly these 2 things when one exports an image in NWW (Multi-resolution).
In order to help you in carrying these files on another computer or to send them to a colleague SonarScope
adds 2 other files in the root directory ot the pyramids :
- this readme.txt
- a copy of the .xml file (which is not used by NWW)

If you send the pyramid directory on another computer or if you move it or rename it on your computer you
have to follow these steps :
- edit the .xml file and change the line beginning by "<PermanentDirectory" with the new directory name.
Ex : <PermanentDirectory>C:\TEMP\output_tiles\Prismed125</PermanentDirectory>
by 
<PermanentDirectory>C:\myNewDirectory\Prismed125</PermanentDirectory>
- copy the modified .xml file in the C:\Program Files\NASA\World Wind 1.4\Config\Earth directory
- Restart NWW or click on "File" then "Earth"