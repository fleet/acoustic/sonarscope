ProjCode.m
----------
Le fichier projcode.m est natif à la Mapping Toolbox : il se révèle très lent quand il est appelé de façon répétée. La cause est dans le codage de l'initialisation (à chaque passage) des structures en début de fonction.
Change Log

Les structures sont déclarées comme "persistent".
Soit xx Année MatLab et L, lettre de la release Semestrielle, à copier dans :
C:\Program Files\MATLAB\R20xxL\toolbox\map\mapproj\private

imcontrast.m
------------
In order imcontrast conserves the corect limits wher the Adjust button is pushed

getHistogramData.m
------------------
Correction du calcul d'histogramme lié à l'outil imcontrast : 
- calcul de 256 bins même sur une dynamique faible, 
- calcul des marges autour de la dynamique si elle est faible (1/8). 
Fonctionne dans cette version sous R2013b également.

2 steps :

- Rename the C:\Program Files\MATLAB\R20XXLb\toolbox\images\imuitools\imcontrast.m in 
C:\Program Files\MATLAB\R20XXLb\toolbox\images\imuitools\imcontrast_MathWorks.m 

- Copy the file in this directory :
C:\Program Files\MATLAB\R20XXLb\toolbox\images\imuitools

Same for the 2 other files except they are in the private directory

getAxesScale.m
--------------
A copier sous : 
C:\Program Files\MATLAB\R20xxL\toolbox\images\imuitools\private
