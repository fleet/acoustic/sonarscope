imcontrast.m
------------
In order imcontrast conserves the correct limits wher the Adjust button is pushed

2 steps :

- Rename the C:\Program Files\MATLAB\R20XXLb\toolbox\images\imuitools\imcontrast.m in 
C:\Program Files\MATLAB\R20XXLb\toolbox\images\imuitools\imcontrast_MathWorks.m 

- Copy the file in this directory :
C:\Program Files\MATLAB\R20XXLb\toolbox\images\imuitools

getHistogramData.m
------------------
Correction du calcul d'histogramme lié à l'outil imcontrast : 
- calcul de 256 bins même sur une dynamique faible, 
- calcul des marges autour de la dynamique si elle est faible (1/8). 
Fonctionne dans cette version sous R2013b également.

Même traitement que imcontrast.m