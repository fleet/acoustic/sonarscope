% Script testImContrastDeployement
% for testing deployement of imcontrast (MatLab internal fct)
% 
% Compile instructions :
%     global IfrTbx
%     dirTestImContrast = 'C:\Users\augustin\Desktop\TestImContrast';
%     if ~exist(dirTestImContrast, 'dir')
%         mkdir(dirTestImContrast);
%     end
%     mcc -v -m 'testImContrastDeployed.m' -d dirTestImContrast
% 
% 
% ------------------------------------------------

%%
figure;
hAxe = subplot(1,1,1);
data = (rand(500)-0.5)*1e-6;
imagesc(data); 
colorbar;
colormap(jet(256));

%%
if isdeployed
    
end
fprintf('==>> 1 - %s\n', which('my_imcontrast'))
[hImcontrast, vSSC] = my_imcontrast(hAxe);
