% Compilation :
% mcc -v -m testFillNaN_mean.m
% 
function isOK = testFillNaN_mean

isOK = 0;

global NUMBER_OF_PROCESSORS

NUMBER_OF_PROCESSORS = 8;

s = mod(0:2:1800, 360);
X = repmat(s, length(s), 1);
X(100:250,100:250) = NaN;
N = floor(length(s)^2 / 2);
d = randi(length(s), [N 2]);
for k=1:size(d,1)
    X(d(k,2),d(k,1)) = NaN;
end
figure; imagesc(X)

Xsingle = single(X);
Y = fillNaN_mean(Xsingle, [3 3]);
figure; imagesc(Y)

isOK = 1