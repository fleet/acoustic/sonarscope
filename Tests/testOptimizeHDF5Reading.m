%%
nomFicHdf5 = 'E:\Temp\testSATURN\Measurement\01_DataFromCNR\03_Version20230113\MS1_filtered.hdf5';
% Fichier de 330 Mo
nomFicHdf5 = 'E:\SonarScopeAnnexes\SonarScopeDataArchive\TestsFormatXsf\Globe-2.0.1\0039_20180905_222154_raw.xsf.nc';
% Fichier de 1.3 Go
nomFicHdf5 = 'E:\SonarScopeAnnexes\SonarScopeDataArchive\From Marc Roche\0114_20221212_200247.xsf.nc';

tStart = tic; % pair 2: tic
n = 1; % 10;
T = zeros(1,n);
for k=1:n
    tic
    sInfo = h5info(nomFicHdf5);
    T(k)= toc;
end
tMul = sum(T)

%% Lecture par fcn native H5O.Visit - Recursive
s = struct;
s.Datasets      = [];
s.Groups        = [];

% profile on
fid = H5F.open(nomFicHdf5, 'H5F_ACC_RDONLY', 'H5P_DEFAULT');
tStart = tic; % pair 2: tic
n = 1; % 10;
T = zeros(1,n);
for k=1:n
    tic
    % [status sOut] = H5O.visit(fid, 'H5_INDEX_NAME', 'H5_ITER_NATIVE', @hdf5_getObjInfo, s);
    [status,sOut] = H5O.visit2(fid, 'H5_INDEX_CRT_ORDER', 'H5_ITER_NATIVE', @hdf5_getObjInfo, s, 'H5O_INFO_BASIC');
    T(k)= toc;
end
H5F.close(fid);
tMul = sum(T)
% profile report

%%
tStart = tic; % pair 2: tic
n = 10;
T = zeros(1,n);
idx_type = 'H5_INDEX_NAME';
order   = 'H5_ITER_INC';
lapl_id = 'H5P_DEFAULT';
for k=1:n
    tic
    fid = H5F.open(nomFicHdf5);
    idxGrp = 0;
    while true
        this_name = H5G.get_objname_by_idx(fid,idxGrp);
        if isempty(this_name)
            break
        end
        sSkeHdf5.group_names{idxGrp+1,1} = this_name;
        sSkeHdf5.group_id{idxGrp+1,1} = idxGrp;
        gid = H5G.open(fid,this_name);
        infoGrp     = H5G.get_info(gid);
        % nbObjGrp    = H5G.get_num_objs(gid); % !!! Deprecated
        for iLink=1:infoGrp.nlinks
            subgrp_name = H5G.get_objname_by_idx(gid,iLink-1)
            dset_id     = H5D.open(fid,'/Environment/Sound_speed_profile/lat');
            type_id     = H5D.get_type(dset_id);
            H5T.detect_class(type_id, 'H5I_GROUP')
            H5D.close(dset_id)
            objname = H5I.get_name(gid)
            objtype = H5I.get_type(gid)
            H5T.detect_class(objtype, 'H5T_GROUP')
            % name = H5L.get_name_by_idx(fid,this_name,idx_type,order,iLink-1,lapl_id)
        end
        H5G.close(gid);
        idxGrp = idxGrp+1;

    end
    num_grps = length(group_names);
    H5F.close(fid)
    T(k)= toc; % pair 1: toc
end