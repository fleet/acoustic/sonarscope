%Ce programme redistribue les donn�es provenant des fichiers .mat obtenus
%par EI_BOB . Les donn�es sont structur�es en Secteur X Seuil X Cycle X Layer. Le programme corrige
%les donn�es du secteur 24 cycle 4 o� Bob est rest� 2h sur le secteur 2 et a donc rat� le secteur 24.
%Clean workspace
%GLUGLUGLU clear all;
close all;
%listage = dir (fullfile('G:','\svalbard2011\Hacssphase\Hacparsecteur_sans_phase\OptionDistance\sect*'));
listRep = uigetdir('F:\svalbard2011\BOB\Hacparsecteur_sans_phase\OptionDistanceAngMax','Select the RUN* directory');
listage = dir (fullfile(listRep, '\RUN*'));
%path_hac_survey = 'F:\HAC_Marmara_2011_par_secteurs\'
path_hac_survey = listRep;
%path_config = [path_hac_survey,'/Config/EI_Lay/EIlay-IBTS']
%GLUGLUGLU path_save = [listRep,'\Result\EI_Lay_Svalbard'];
path_save = listRep;
Labels = {'Nom de la campagne'};
Titre = 'NOM de la campagne';
NbLignes = 1;
Defaults = {'Svalbard'};
survey_name = inputdlg(Labels, Titre, NbLignes, Defaults);
nbsect = length(listage);
thresholds = [-100:5:-60];
nbthresh = size(thresholds,2);
nbcycles=16;
nblayers = 55;
vallayer = 2;
[CCUfile,CCUpath] = uigetfile(listRep,'Select the Mat_CCU file');
Mat_CCU = importdata([CCUpath CCUfile]);
% %Initialisation de Mat_EI_Secteurs
% Mat_EI_Secteurs(nbsect,nbthresh).depth_bottom_ER60_db(1:nbcycles,1:nblayers) = [NaN];
% Mat_EI_Secteurs(nbsect,nbthresh).depth_surface_ER60_db(1:nbcycles,1:nblayers) = [NaN];
% Mat_EI_Secteurs(nbsect,nbthresh).lat_surfER60_db(1:nbcycles,1:nblayers)= [NaN];
% Mat_EI_Secteurs(nbsect,nbthresh).lon_surfER60_db(1:nbcycles,1:nblayers) = [NaN];
% Mat_EI_Secteurs(nbsect,nbthresh).lat_botER60_db(1:nbcycles,1:nblayers)= [NaN];
% Mat_EI_Secteurs(nbsect,nbthresh).lon_botER60_db(1:nbcycles,1:nblayers) = [NaN];
% Mat_EI_Secteurs(nbsect,nbthresh).Sa_surfER60_db(1:nbcycles,1:nblayers) = [NaN];
% Mat_EI_Secteurs(nbsect,nbthresh).Sa_botER60_db(1:nbcycles,1:nblayers) = [NaN];
% Mat_EI_Secteurs(nbsect,nbthresh).Sv_surfER60_db(1:nbcycles,1:nblayers) = [NaN];
% Mat_EI_Secteurs(nbsect,nbthresh).Sv_botER60_db(1:nbcycles,1:nblayers) = [NaN];
% Mat_EI_Secteurs(nbsect,nbthresh).time_ER60_db(1,1:nbcycles)= [NaN];
% Mat_EI_Secteurs(nbsect,nbthresh).vol_surfER60_db(1:nbcycles,1:nblayers) = [NaN];
% Mat_EI_Secteurs(nbsect,nbthresh).Angle_secteur_Pan(1,1) = [NaN];
% Mat_EI_Secteurs(nbsect,nbthresh).Mat_EI(1,1).Thresholds(1,1) = [NaN];
%Read CCU.log matrix
for secteur = 1:nbsect;
    for t = 1:size(thresholds,2);
        cheminfic = [path_save,'\Svalbard-',listage(secteur).name,'-TH',num2str(thresholds(t)),'-ER60-EIlay.mat'];
        nomficcomplet = cheminfic;
        Mat_Temp = load(nomficcomplet);
        Mat_Temp.Angle_secteur_Pan = zeros(1,1);
        Mat_Temp.Thresholds = zeros(1,1);
        time_ER60_db_transp = (Mat_Temp.time_ER60_db)';
        %extraction des angles de la matrice CCU par correspondance des
        %dates
        display(secteur);
        display(thresholds(t));
        for i = 1:length(time_ER60_db_transp);
            index = find(abs(time_ER60_db_transp(i)-Mat_CCU.Sdate)<(5/(nbsect*60)));
            Angle_secteur_Pan = Mat_CCU.Pan(index(1),1);
        end
        Mat_Temp.Angle_secteur_Pan = [Angle_secteur_Pan];
        Mat_Temp.Thresholds = [thresholds(t)];
        Mat_EI_Secteurs(secteur,t) = [Mat_Temp];
        clear Mat_Temp;
    end
end
clear Mat_CCU;
%GLUGLUGLU cd (listRep(1,:))
%offset=-82*pi/180;
offset=-85.5*pi/180;
rho = 0:vallayer:vallayer*nblayers;
theta = ((0:nbsect)*7+71).*pi./180 + offset;%71 est la valeur du 1er angle de MatCCU=angle observ� par PAN&TILT
%theta=((0:3)*7+71).*pi./180 + offset;
% Prise en compte de l'absence de donn�es pour le secteur 24 cycle 4
for t=1:size(thresholds,2)
    [mm,nn] = size(Mat_EI_Secteurs(24,t).Sa_botER60_db);
    for s24=mm:-1:5
        Mat_EI_Secteurs(24,t).depth_bottom_ER60_db(s24+1,:)= Mat_EI_Secteurs(24,t).depth_bottom_ER60_db(s24,:);
        Mat_EI_Secteurs(24,t).depth_surface_ER60_db(s24+1,:)= Mat_EI_Secteurs(24,t).depth_surface_ER60_db(s24,:);
        Mat_EI_Secteurs(24,t).lat_surfER60_db(s24+1,:)= Mat_EI_Secteurs(24,t).lat_surfER60_db(s24,:);
        Mat_EI_Secteurs(24,t).lon_surfER60_db(s24+1,:)= Mat_EI_Secteurs(24,t).lon_surfER60_db(s24,:);
        Mat_EI_Secteurs(24,t).lat_botER60_db(s24+1,:)= Mat_EI_Secteurs(24,t).lat_botER60_db(s24,:);
        Mat_EI_Secteurs(24,t).lon_botER60_db(s24+1,:)= Mat_EI_Secteurs(24,t).lon_botER60_db(s24,:);
        Mat_EI_Secteurs(24,t).Sa_surfER60_db(s24+1,:)= Mat_EI_Secteurs(24,t).Sa_surfER60_db(s24,:);
        Mat_EI_Secteurs(24,t).Sa_botER60_db(s24+1,:)= Mat_EI_Secteurs(24,t).Sa_botER60_db(s24,:);
        Mat_EI_Secteurs(24,t).Sv_surfER60_db(s24+1,:)= Mat_EI_Secteurs(24,t).Sv_surfER60_db(s24,:);
        Mat_EI_Secteurs(24,t).Sv_botER60_db(s24+1,:)= Mat_EI_Secteurs(24,t).Sv_botER60_db(s24,:);
        Mat_EI_Secteurs(24,t).time_ER60_db(:,s24+1)= Mat_EI_Secteurs(24,t).time_ER60_db(:,s24) ;
        Mat_EI_Secteurs(24,t).vol_surfER60_db(s24+1,:)= Mat_EI_Secteurs(24,t).vol_surfER60_db(s24,:);
    end
    Mat_EI_Secteurs(24,t).depth_bottom_ER60_db(4,:)= [NaN];
    Mat_EI_Secteurs(24,t).depth_surface_ER60_db(4,:)= [NaN];
    Mat_EI_Secteurs(24,t).lat_surfER60_db(4,:)=  [NaN];
    Mat_EI_Secteurs(24,t).lon_surfER60_db(4,:)=  [NaN];
    Mat_EI_Secteurs(24,t).lat_botER60_db(4,:)=  [NaN];
    Mat_EI_Secteurs(24,t).lon_botER60_db(4,:)=  [NaN];
    Mat_EI_Secteurs(24,t).Sa_surfER60_db(4,:)=  [NaN];
    Mat_EI_Secteurs(24,t).Sa_botER60_db(4,:)= [NaN];
    Mat_EI_Secteurs(24,t).Sv_surfER60_db(4,:)=  [NaN];
    Mat_EI_Secteurs(24,t).Sv_botER60_db(4,:)=  [NaN];
    Mat_EI_Secteurs(24,t).time_ER60_db(:,4)=  [NaN];
    Mat_EI_Secteurs(24,t).vol_surfER60_db(4,:)= [NaN];
end
%GLUGLUGLU save 'F:\svalbard2011\BOB\Hacparsecteur_sans_phase\OptionDistanceAngMax\Result\Mat_EI_Secteurs.mat';
save 'C:\SonarScopeTbxGLU\BOB\Result\Mat_EI_Secteurs_GLU.mat';
%graphique et filtrage pour tous les cycles
for t=1:size(thresholds,2)
    for cycle  = 1:1:length(time_ER60_db_transp) % nbre de cycles
        Namefig=['Threshold ',num2str(thresholds(t)), ' Cycle ', num2str(cycle)];
        h=figure('Name',Namefig(1,:),'NumberTitle','off','color','w');
        c1=[];
        for secteur=1:1:nbsect;
            c0=Mat_EI_Secteurs(secteur,t).Sa_botER60_db(cycle,:);
            c1=vertcat(c1, c0);
            c0=[];
        end
        for j=1:1:nbsect;
            [a,ii]=size(Mat_EI_Secteurs(1).Sa_botER60_db(cycle,:));
            for i=1:1:ii;
                if (c1(j,i)<10000)
                    img1(i,j)=c1(j,i);
                else
                    img1(i,j)=NaN;
                end
            end
        end
        c1=[];
        %         debut polar_image
        polar_image(theta,rho,fliplr(img1));
        title(['Threshold ',num2str(thresholds(t)),'Cycle  ',num2str(cycle)]);
        caxis([0 20]);
        saveas(h,Namefig,'fig');
        img1(:,:)=[NaN];
        close all;
    end
end
%%
%%Ce programme redistribue les donn�es extraites de Mat_EI qui sont une
%%
%structure de la forme Secteur X Seuil X Cycle X Layer en structure Cycle X
%Seuil X Secteur X Layer. Il transforme les donn�es en fichiers .xyz.
%Initialisation de Mat_EI_Cycles
% Mat_EI_Cycles(1:nbcycles,1:nbthresh).Sa_bot(1:nbsect,1:nblayers) = [NaN];
% Mat_EI_Cycles(1:nbcycles,1:nbthresh).lat_bot(1:nbsect,1:nblayers) = [NaN];
% Mat_EI_Cycles(1:nbcycles,1:nbthresh).lon_bot(1:nbsect,1:nblayers) = [NaN];
% Mat_EI_Cycles(1:nbcycles,1:nbthresh).time_bot(1:nbsect,1:nblayers) = [NaN];
for cycle=1:length(Mat_EI_Secteurs(1,1).time_ER60_db);
    for t=1:size(thresholds,2);
        for secteur=1:nbsect;
            Mat_EI_Cycles(cycle,t).Sa_bot(secteur,:) = Mat_EI_Secteurs(secteur,t).Sa_botER60_db(cycle,:);
            Mat_EI_Cycles(cycle,t).lat_bot(secteur,:) = Mat_EI_Secteurs(secteur,t).lat_botER60_db(cycle,:);
            Mat_EI_Cycles(cycle,t).lon_bot(secteur,:) = Mat_EI_Secteurs(secteur,t).lon_botER60_db(cycle,:);
            Mat_EI_Cycles(cycle,t).time_bot(secteur,:) = Mat_EI_Secteurs(secteur,t).time_ER60_db(cycle);
        end;
    end;
end;
% % % % for cycle=1:length(Mat_EI_Secteurs(1,1).time_ER60_db);
% % % %     for t=1:size(thresholds,2);
% % % %         pcolor(Mat_EI_Cycles(cycle,t).Sa_bot');
% % % %         Namefig=['Threshold ',num2str(thresholds(t)), ' Cycle ', num2str(cycle)];
% % % %         %h=figure('Name',Namefig(1,:),'NumberTitle','off','color','w');
% % % %         h=figure(gcf);
% % % %         saveas(h,[path_save '\Mat_EI_Cycles_' num2str(cycle),'-TH' num2str(thresholds(t))]);
% % % %         save 'F:\svalbard2011\BOB\Hacparsecteur_sans_phase\OptionDistanceAngMax\Result\Mat_EI_Cycles.mat';
% % % %     end
% % % % end
% % % % 

for cycle = 1:nbcycles;
    for t = 1:size(thresholds,2);
        for secteur = 1:nbsect;
            Mat_EI_Cycles(cycle,t).depth_bottom_ER60_db(secteur,:) = Mat_EI_Secteurs(secteur,t).depth_bottom_ER60_db(cycle,:);
            Mat_EI_Cycles(cycle,t).depth_surface_ER60_db(secteur,:) = Mat_EI_Secteurs(secteur,t).depth_surface_ER60_db(cycle,:);
            Mat_EI_Cycles(cycle,t).lat_surfER60_db(secteur,:) = Mat_EI_Secteurs(secteur,t).lat_surfER60_db(cycle,:);
            Mat_EI_Cycles(cycle,t).lon_surfER60_db(secteur,:) = Mat_EI_Secteurs(secteur,t).lon_surfER60_db(cycle,:);
            Mat_EI_Cycles(cycle,t).lat_bot(secteur,:) = Mat_EI_Secteurs(secteur,t).lat_botER60_db(cycle,:);
            Mat_EI_Cycles(cycle,t).lon_bot(secteur,:) = Mat_EI_Secteurs(secteur,t).lon_botER60_db(cycle,:);
            Mat_EI_Cycles(cycle,t).Sa_surfER60_db(secteur,:) = Mat_EI_Secteurs(secteur,t).Sa_surfER60_db(cycle,:);
            Mat_EI_Cycles(cycle,t).Sa_bot(secteur,:) = Mat_EI_Secteurs(secteur,t).Sa_botER60_db(cycle,:);
            Mat_EI_Cycles(cycle,t).Sv_surfER60_db(secteur,:) = Mat_EI_Secteurs(secteur,t).Sv_surfER60_db(cycle,:);
            Mat_EI_Cycles(cycle,t).Sv_botER60_db(secteur,:) = Mat_EI_Secteurs(secteur,t).Sv_botER60_db(cycle,:);
            Mat_EI_Cycles(cycle,t).time_bot(secteur,:) = Mat_EI_Secteurs(secteur,t).time_ER60_db(cycle);
            Mat_EI_Cycles(cycle,t).vol_surfER60_db(secteur,:) = Mat_EI_Secteurs(secteur,t).vol_surfER60_db(cycle,:);
            Mat_EI_Cycles(cycle,t).Angle_secteur_Pan(secteur,:) = Mat_EI_Secteurs(secteur,t).Angle_secteur_Pan(1,:);
        end;
        h=figure('Name',Namefig(1,:),'NumberTitle','off','color','w');
        Namefig=['Threshold ',num2str(thresholds(t)), ' Cycle ', num2str(cycle)];
        pcolor(Mat_EI_Cycles(cycle,t).Sa_bot');
        saveas(h,[path_save '\Mat_EI_Cycles_' num2str(cycle),'-TH' num2str(thresholds(t))]);
        for d=1:nblayers;
            dlmwrite([path_save,'\XYZ', '\Mat_EI_Cycles_' num2str(cycle),'-TH' num2str(thresholds(t)),'_BOB.xyz'],[Mat_EI_Cycles(cycle,t).lat_bot(:,d),Mat_EI_Cycles(cycle,t).lon_bot(:,d),Mat_EI_Cycles(cycle,t).Sa_bot(:,d)],'-append','delimiter',' ','precision',17);
        end;
    end;
end;
save ('Mat_EI_Cycles.mat');

%for secteur=1:nbsect
for t=1:size(thresholds,2);
    for cycle=1:length(Mat_EI_Secteurs(secteur,t).time_ER60_db);
        for d=1:nblayers;
            dlmwrite([path_save '\Mat_EI_Cycles_' num2str(cycle),'-TH' num2str(thresholds(t)),'BOB.xyz'],[Mat_EI_Cycles(cycle,t).lat_bot(:,d),Mat_EI_Cycles(cycle,t).lon_bot(:,d),Mat_EI_Cycles(cycle,t).Sa_bot(:,d)],'-append','delimiter',' ','precision',17);
        end;
    end;
end;
%end;
