function flag = create_XMLBin_BOB(I, NomFicXML, cmap, DataIn)

[nbPings, nbRows, nbSlices] = size(I);

%% Cr�ation du r�pertoire

[nomDirXml, nomDirBin] = fileparts(NomFicXML);
nomDirXml = fullfile(nomDirXml, nomDirBin);
nomDirRoot = fileparts(nomDirXml);
if ~exist(nomDirXml, 'dir')
    flag = mkdir(nomDirXml);
    if ~flag
        messageErreurFichier(nomDirXml, 'WriteFailure')
        return
    end
end

%% Ecriture de l'image PNG

for k=1:nbRows
    nomFic1 = sprintf('%05d.png', k); % '00000.png';
    nomFic2 = fullfile(nomDirXml, nomFic1);
    J = squeeze(I(:,k,:));
    imwrite(J', cmap, nomFic2, 'Transparency', 0)
end

%% Definition of the structure

Info.Signature1      = 'SonarScope';
Info.Signature2      = 'ImagesAlongNavigation';
Info.FormatVersion   = 20100624;
Info.Name            = DataIn.name;
Info.ExtractedFrom   = DataIn.from;

Info.Survey         = 'Unknown';
Info.Vessel         = 'Unknown';
Info.Sounder        = 'Unknown';
Info.ChiefScientist = 'Unknown';

Info.Dimensions.nbPings     = nbPings;
Info.Dimensions.nbSlices    = nbSlices;
Info.Dimensions.nbRows      = nbRows;

Data.AcrossDistance         = zeros(nbSlices,1);
Data.SliceExists            = true(nbSlices,1);
Data.Date                   = DataIn.Date;
Data.Hour                   = DataIn.Hour;
Data.PingNumber             = DataIn.PingNumber;
Data.LatitudeBottom         = DataIn.LatitudeBottom;
Data.LongitudeBottom        = DataIn.LongitudeBottom;
Data.LatitudeTop            = DataIn.LatitudeTop;
Data.LongitudeTop           = DataIn.LongitudeTop;
Data.DepthTop               = DataIn.DepthTop;
Data.DepthBottom            = DataIn.DepthBottom;
Data.iSlices                = (1:nbSlices)';


Info.Signals(1).Name          = 'Date';
Info.Signals(end).Dimensions  = '1, nbRows';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'Ifremer';
Info.Signals(end).FileName    = fullfile(nomDirBin,'Date.bin');

Info.Signals(end+1).Name      = 'Hour';
Info.Signals(end).Dimensions  = '1, nbRows';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'nb milliseconds since midnight';
Info.Signals(end).FileName    = fullfile(nomDirBin,'Hour.bin');

Info.Signals(end+1).Name      = 'PingNumber';
Info.Signals(end).Dimensions  = '1, nbRows';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = ' ';
Info.Signals(end).FileName    = fullfile(nomDirBin,'PingNumber.bin');


Info.Signals(end+1).Name      = 'SliceExists';
Info.Signals(end).Dimensions  = 'nbSlices, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile(nomDirBin,'SliceExists.bin');

Info.Signals(end+1).Name      = 'iSlices';
Info.Signals(end).Dimensions  = 'nbSlices, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile(nomDirBin,'iSlices.bin');

Info.Signals(end+1).Name      = 'DepthTop';
Info.Signals(end).Dimensions  = '1, nbSlices';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'm';
Info.Signals(end).FileName    = fullfile(nomDirBin, 'DepthTop.bin');

Info.Signals(end+1).Name      = 'DepthBottom';
Info.Signals(end).Dimensions  = '1, nbSlices';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'm';
Info.Signals(end).FileName    = fullfile(nomDirBin, 'DepthBottom.bin');

Info.Signals(end+1).Name      = 'AcrossDistance';
Info.Signals(end).Dimensions  = 'nbSlices, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'm';
Info.Signals(end).FileName    = fullfile(nomDirBin, 'AcrossDistance.bin');


Info.Images(1).Name           = 'LatitudeTop';
Info.Images(end).Dimensions   = 'nbPings, nbSlices';
Info.Images(end).Storage      = 'double';
Info.Images(end).Unit         = 'deg';
Info.Images(end).FileName     = fullfile(nomDirBin, 'LatitudeTop.bin');

Info.Images(end+1).Name       = 'LongitudeTop';
Info.Images(end).Dimensions   = 'nbPings, nbSlices';
Info.Images(end).Storage      = 'double';
Info.Images(end).Unit         = 'deg';
Info.Images(end).FileName     = fullfile(nomDirBin,'LongitudeTop.bin');

Info.Images(end+1).Name      = 'LatitudeBottom';
Info.Images(end).Dimensions  = 'nbPings, nbSlices';
Info.Images(end).Storage     = 'double';
Info.Images(end).Unit        = 'deg';
Info.Images(end).FileName    = fullfile(nomDirBin, 'LatitudeBottom.bin');

Info.Images(end+1).Name      = 'LongitudeBottom';
Info.Images(end).Dimensions  = 'nbPings, nbSlices';
Info.Images(end).Storage     = 'double';
Info.Images(end).Unit        = 'deg';
Info.Images(end).FileName    = fullfile(nomDirBin, 'LongitudeBottom.bin');
% Info.Images(end+1).Name       = 'Reflectivity';
% Info.Images(end).Dimensions   = 'nbRows, nbPings, nbSlices';
% Info.Images(end).Storage      = 'single';
% Info.Images(end).Unit         = 'dB';
% Info.Images(end).FileName     = fullfile(nomDirBin,'Reflectivity.bin');


%% Cr�ation du fichier XML d�crivant la donn�e

% NomFicXML = fullfile(nomDirRacine, [TypeDataVoxler '.xml']);
xml_write(NomFicXML, Info);
flag = exist(NomFicXML, 'file');
if ~flag
    messageErreur(NomFicXML)
    return
end

%% Cr�ation du r�pertoire Info

nomDirInfo = nomDirXml;
if ~exist(nomDirInfo, 'dir')
    status = mkdir(nomDirInfo);
    if ~status
        messageErreur(nomDirInfo)
        flag = 0;
        return
    end
end

%% Cr�ation des fichiers binaires des signaux

for i=1:length(Info.Signals)
    flag = writeSignal(nomDirRoot, Info.Signals(i), Data.(Info.Signals(i).Name));
    figure; plot(Data.(Info.Signals(i).Name)); grid on; title(Info.Signals(i).Name)
    if ~flag
        return
    end
end

%% Cr�ation des fichiers binaires des signaux

for i=1:length(Info.Images)
    flag = writeImage(nomDirRoot, Info.Images(i), Data.(Info.Images(i).Name));
    figure; imagesc(Data.(Info.Images(i).Name)); colorbar; title(Info.Images(i).Name)
    if ~flag
        return
    end
end

% % % % Comment� pour l'instant flag = zipDirectory(nomDirInfo);
