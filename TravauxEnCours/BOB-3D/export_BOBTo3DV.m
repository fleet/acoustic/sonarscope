% Ecriture d'un fichier VRML de nuage de pints en G�om�trie XY.
%
% SYNTAX :
%   flag = exportVRML_PointSet_GeoYX(this, varargin)
%
% INPUT PARAMETERS :
%   this        : Instance de cl_image
%
% OUTPUT PARAMETERS :
%   flag : 1 si ecriture reussie, 0 sinon
%
% EXAMPLES :
%   nomFicXml = 'F:\SonarScopeData\BOB-3D\Data\EI_Lay_SvalBard.xml';
%   flag = export_BOBTo3DV(nomFicXml);
% SEE ALSO  : cl_image Authors
% AUTHORS   : GLT, GLU
% -------------------------------------------------------------------------
function flag = export_BOBTo3DV(nomFicXml)

global DEBUG;
flag = 0;

if ~exist(nomFicXml, 'file')
    return
end

LayerXml        = xml_read(nomFicXml);
[nomDir, nomFicSeul, ext] = fileparts(nomFicXml);

listage         = dir (fullfile(nomDir, LayerXml.NomDirRun, '\RUN*'));
survey_name     = LayerXml.Survey;
id              = findIndVariable(LayerXml.Signals, 'Thresholds');
thresholds      = LayerXml.Signals(id).Constant;
nbCycles        = LayerXml.Dimensions.nbCycles;
nbThresholds    = LayerXml.Dimensions.nbThresholds;
nbSectors       = LayerXml.Dimensions.nbSectors;
nbCells         = LayerXml.Dimensions.nbCells;
%% Import des donn�es Capteur
Mat_CCU         = importdata(fullfile(nomDir, LayerXml.NomFicSensor));

%% Chargement des donn�es.
for secteur = 1:nbSectors
    for t = 1:nbThresholds
        NomFicMat                   = fullfile(nomDir, LayerXml.NomDirRun,...
                                        [survey_name '-' listage(secteur).name '-TH' num2str(thresholds(t)) '-ER60-EIlay.mat']);
        Mat_Temp                    = load(NomFicMat);
        Mat_Temp.Angle_secteur_Pan  = 0;
        Mat_Temp.Thresholds         = 0;
        time_ER60_db_transp         = (Mat_Temp.time_ER60_db)';
        %extraction des angles de la matrice CCU par correspondance des
        %dates
        % Pour info : timeCCU = cl_time('timeMat', time_ER60_db_transp)
        for i = 1:length(time_ER60_db_transp);
            index = find(abs(time_ER60_db_transp(i)-Mat_CCU.Sdate)<(5/(nbSectors*60)));
            Angle_secteur_Pan = Mat_CCU.Pan(index(1),1);
        end
        Mat_Temp.Angle_secteur_Pan  = Angle_secteur_Pan;
        Mat_Temp.Thresholds         = thresholds(t);
        Mat_EI_Secteurs(secteur,t)  = Mat_Temp;
        clear Mat_Temp
    end
end
%% Traitement des absences de donn�es.
%
% Prise en compte de l'absence de donn�es pour le secteur 24 cycle 4

for t=1:nbThresholds
    mm = size(Mat_EI_Secteurs(24,t).Sa_botER60_db, 1);
    for s24=mm:-1:5
        Mat_EI_Secteurs(24,t).depth_bottom_ER60_db(s24+1,:)     = Mat_EI_Secteurs(24,t).depth_bottom_ER60_db(s24,:);
        Mat_EI_Secteurs(24,t).depth_surface_ER60_db(s24+1,:)    = Mat_EI_Secteurs(24,t).depth_surface_ER60_db(s24,:);
        Mat_EI_Secteurs(24,t).lat_surfER60_db(s24+1,:)          = Mat_EI_Secteurs(24,t).lat_surfER60_db(s24,:);
        Mat_EI_Secteurs(24,t).lon_surfER60_db(s24+1,:)          = Mat_EI_Secteurs(24,t).lon_surfER60_db(s24,:);
        Mat_EI_Secteurs(24,t).lat_botER60_db(s24+1,:)           = Mat_EI_Secteurs(24,t).lat_botER60_db(s24,:);
        Mat_EI_Secteurs(24,t).lon_botER60_db(s24+1,:)           = Mat_EI_Secteurs(24,t).lon_botER60_db(s24,:);
        Mat_EI_Secteurs(24,t).Sa_surfER60_db(s24+1,:)           = Mat_EI_Secteurs(24,t).Sa_surfER60_db(s24,:);
        Mat_EI_Secteurs(24,t).Sa_botER60_db(s24+1,:)            = Mat_EI_Secteurs(24,t).Sa_botER60_db(s24,:);
        Mat_EI_Secteurs(24,t).Sv_surfER60_db(s24+1,:)           = Mat_EI_Secteurs(24,t).Sv_surfER60_db(s24,:);
        Mat_EI_Secteurs(24,t).Sv_botER60_db(s24+1,:)            = Mat_EI_Secteurs(24,t).Sv_botER60_db(s24,:);
        Mat_EI_Secteurs(24,t).time_ER60_db(:,s24+1)             = Mat_EI_Secteurs(24,t).time_ER60_db(:,s24) ;
        Mat_EI_Secteurs(24,t).vol_surfER60_db(s24+1,:)          = Mat_EI_Secteurs(24,t).vol_surfER60_db(s24,:);
    end
    Mat_EI_Secteurs(24,t).depth_bottom_ER60_db(4,:)     = NaN;
    Mat_EI_Secteurs(24,t).depth_surface_ER60_db(4,:)    = NaN;
%     Mat_EI_Secteurs(24,t).lat_surfER60_db(4,:)          = NaN;
%     Mat_EI_Secteurs(24,t).lon_surfER60_db(4,:)          = NaN;
%     Mat_EI_Secteurs(24,t).lat_botER60_db(4,:)           = NaN;
%     Mat_EI_Secteurs(24,t).lon_botER60_db(4,:)           = NaN;
    Mat_EI_Secteurs(24,t).Sa_surfER60_db(4,:)           = NaN;
    Mat_EI_Secteurs(24,t).Sa_botER60_db(4,:)            = NaN;
    Mat_EI_Secteurs(24,t).Sv_surfER60_db(4,:)           = NaN;
    Mat_EI_Secteurs(24,t).Sv_botER60_db(4,:)            = NaN;
    Mat_EI_Secteurs(24,t).time_ER60_db(:,4)             = NaN;
    Mat_EI_Secteurs(24,t).vol_surfER60_db(4,:)          = NaN;
end
%GLUGLUGLU save 'F:\svalbard2011\BOB\Hacparsecteur_sans_phase\OptionDistanceAngMax\Result\Mat_EI_Secteurs.mat';
% save 'C:\SonarScopeTbxGLU\BOB\Result\Mat_EI_Secteurs.mat';

%%
if DEBUG
    % Ce programme redistribue les donn�es extraites de Mat_EI qui sont une
    % structure de la forme Secteur X Seuil X Cycle X Layer en structure Cycle X
    % Seuil X Secteur X Layer. Il transforme les donn�es en fichiers .xyz.
    hdlFig(1)   = figure('Name', 'Geom�trie Tableau');
    hAxe(1)     = gca;
    set(hdlFig(1), 'Position', [284   473   672   504])
    
    hdlFig(2)   = figure('Name', 'Geom�trie Polaire');
    hAxe(2)     = gca;
    CLim = [0 50];
    set(hdlFig(2), 'Position', [983   473   672   504])
end
% Attention, il peut y avoir des cyles manquants.
for cycle=1:nbCycles
    for t=1:nbThresholds
        for secteur=1:nbSectors
            Mat_EI_Cycles(cycle,t).Sa_bot(secteur,:)        = Mat_EI_Secteurs(secteur,t).Sa_botER60_db(cycle,:);
            Mat_EI_Cycles(cycle,t).lat_bot(secteur,:)       = Mat_EI_Secteurs(secteur,t).lat_botER60_db(cycle,:);
            Mat_EI_Cycles(cycle,t).lon_bot(secteur,:)       = Mat_EI_Secteurs(secteur,t).lon_botER60_db(cycle,:);
            Mat_EI_Cycles(cycle,t).time_bot(secteur,:)      = Mat_EI_Secteurs(secteur,t).time_ER60_db(cycle);
            
            Mat_EI_Cycles(cycle,t).depth_bottom_ER60_db(secteur,:)  = Mat_EI_Secteurs(secteur,t).depth_bottom_ER60_db(cycle,:);
            Mat_EI_Cycles(cycle,t).depth_surface_ER60_db(secteur,:) = Mat_EI_Secteurs(secteur,t).depth_surface_ER60_db(cycle,:);
            Mat_EI_Cycles(cycle,t).lat_surfER60_db(secteur,:)       = Mat_EI_Secteurs(secteur,t).lat_surfER60_db(cycle,:);
            Mat_EI_Cycles(cycle,t).lon_surfER60_db(secteur,:)       = Mat_EI_Secteurs(secteur,t).lon_surfER60_db(cycle,:);
            Mat_EI_Cycles(cycle,t).lat_bot(secteur,:)               = Mat_EI_Secteurs(secteur,t).lat_botER60_db(cycle,:);
            Mat_EI_Cycles(cycle,t).lon_bot(secteur,:)               = Mat_EI_Secteurs(secteur,t).lon_botER60_db(cycle,:);
            Mat_EI_Cycles(cycle,t).Sa_surfER60_db(secteur,:)        = Mat_EI_Secteurs(secteur,t).Sa_surfER60_db(cycle,:);
            Mat_EI_Cycles(cycle,t).Sa_bot(secteur,:)                = Mat_EI_Secteurs(secteur,t).Sa_botER60_db(cycle,:);
            Mat_EI_Cycles(cycle,t).Sv_surfER60_db(secteur,:)        = Mat_EI_Secteurs(secteur,t).Sv_surfER60_db(cycle,:);
            Mat_EI_Cycles(cycle,t).Sv_botER60_db(secteur,:)         = Mat_EI_Secteurs(secteur,t).Sv_botER60_db(cycle,:);
            Mat_EI_Cycles(cycle,t).time_bot(secteur,:)              = Mat_EI_Secteurs(secteur,t).time_ER60_db(cycle);
            Mat_EI_Cycles(cycle,t).vol_surfER60_db(secteur,:)       = Mat_EI_Secteurs(secteur,t).vol_surfER60_db(cycle,:);
            Mat_EI_Cycles(cycle,t).Angle_secteur_Pan(secteur,:)     = Mat_EI_Secteurs(secteur,t).Angle_secteur_Pan(1,:);
        end
        if DEBUG
            % Trac� des donn�es.
            Namefig = ['Threshold ',num2str(thresholds(t)), ' - Cycle ', num2str(cycle) ' - Geometry : Cartesian'];
            figure(hdlFig(1));
            pcolor(hAxe(1), Mat_EI_Cycles(cycle,t).Sa_bot');
            title(Namefig, 'FontAngle', 'italic', 'FontSize', 8.0, 'FontWeight', 'bold');
            
            % Trac� des donn�es en G�om�trie Polar_Echogramme
            Namefig = ['Threshold ',num2str(thresholds(t)), ' - Cycle ', num2str(cycle) ' - Geometry : Polar'];
            figure(hdlFig(2));
            subx = 1:size(Mat_EI_Cycles(cycle,t).Sa_bot, 2);
            suby = 1:size(Mat_EI_Cycles(cycle,t).Sa_bot, 1);
            I(suby,subx,secteur) = reflec_Amp2db(Mat_EI_Cycles(cycle,t).Sa_bot);
            pcolor(hAxe(2), Mat_EI_Cycles(cycle,t).lon_bot, Mat_EI_Cycles(cycle,t).lat_bot, I(suby,subx,secteur)); colorbar
            title(Namefig, 'FontAngle', 'italic', 'FontSize', 8.0, 'FontWeight', 'bold');
            set(hAxe, 'CLim', CLim)
            shading flat
            axisGeo
            pause(0.5)
            % saveas(hdfFig,[path_save '\Mat_EI_Cycles_' num2str(cycle),'-TH' num2str(thresholds(t))]);
            % Sauvegarde des Secteurs sous forme Cycle Threshold : lat lon Sa
            % for d=1:nblayers;
            %     dlmwrite([path_save,'\XYZ', '\Mat_EI_Cycles_' num2str(cycle),'-TH' num2str(thresholds(t)),'_BOB.xyz'],[Mat_EI_Cycles(cycle,t).lat_bot(:,d),Mat_EI_Cycles(cycle,t).lon_bot(:,d),Mat_EI_Cycles(cycle,t).Sa_bot(:,d)],'-append','delimiter',' ','precision',17);
            % end;
            % saveas(hdfFig,[path_save '\Mat_EI_Cycles_' num2str(cycle),'-TH' num2str(thresholds(t))]);
            % Sauvegarde des Secteurs sous forme Cycle Threshold : lat lon Sa
         end
    end
end

%% Sauvegarde temporaire (?) sous forme de fichier MatLab.
[rep, flag] = my_questdlg(Lang('Voulez-vous sauver les donn�es en Mat File', 'Do you want to save Mat Files ?'));
if rep == 1 && flag == 1
    nomFicMat = ['Mat_EI_Cycles_' survey_name '.bob'];
    [filename, pathname] = uiputfile('*.mat', 'Give a file name', nomFicMat);
    if isequal(filename,0) || isequal(pathname,0)
        flag = 0;
        return
    end
    save(nomFicMat, 'Mat_EI_Cycles');
end

%% Export et cr�ation d'un layer de type WaterColumn horizontale.
idThreshold = 1;
flag = export_Bob2SSc3DV(nomFicXml, idThreshold, Mat_EI_Cycles);


flag = 1;

