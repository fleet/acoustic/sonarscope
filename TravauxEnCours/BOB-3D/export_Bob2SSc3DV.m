% nomFic = 'C:\SonarScopeTbx\TravauxEnCours\BOB-3D\From JMA\matcycle.mat';
% flag = export_Bob2SSc3DV(nomFic);

function flag = export_Bob2SSc3DV(nomFicBobXml, idThreshold, Mat_EI_Cycles)

flag = 0;
if ~exist(nomFicBobXml, 'file')
    return
end

[~, nomFicSeul]         = fileparts(nomFicBobXml);
nomDir                  = uigetdir('J:\Data\IFREMER\Data_SSC\SSC-3DViewer-Data\3DViewer-CHARLIE\WC\BOB',Lang('Veuillez saisir le r�pertoire de sortie ? ', 'Please, select the output directory ?'));
nomFicXml               = fullfile(nomDir, [nomFicSeul '_BOB3DV.xml']);

% Chargement des donn�es index�es en Cycles.
X           = Mat_EI_Cycles(:,idThreshold);
nbSequences = size(X,1);

figure;
hAxe = gca;
CLim = [0 50];
TransducerDepth = 0;

[nbSectors, nbCells] = size(X(1).Sa_bot);
I = NaN([nbSectors nbCells nbSequences]);
Date            = NaN(nbSequences,1);
Heure           = NaN(nbSequences,1);
LatitudeBob     = NaN(nbSectors,nbCells);
LongitudeBob    = NaN(nbSectors,nbCells);
LatitudeSwath   = NaN(nbSectors,nbCells);
LongitudeSwath  = NaN(nbSectors,nbCells);
for kSequence=1:nbSequences
    subx                = 1:size(X(kSequence).Sa_bot, 2);
    suby                = 1:size(X(kSequence).Sa_bot, 1);
    t                   = cl_time('timeMat', X(kSequence).time_bot(1));
    Date(kSequence)     = t.date;
    Heure(kSequence)    = t.heure;

% % % % %     LatitudeBob(   kSequence,subx) = X(kSequence).lat_bot(1,subx);
% % % % %     LongitudeBob(  kSequence,subx) = X(kSequence).lon_bot(1,subx);
% % % % %     LatitudeSwath( kSequence,subx) = X(kSequence).lat_bot(end,subx);
% % % % %     LongitudeSwath(kSequence,subx) = X(kSequence).lon_bot(end,subx);
    I(suby,subx,kSequence) = reflec_Amp2db(X(kSequence).Sa_bot);
    
    for kSector=1:nbSectors
        LatitudeBob(kSector,subx)      = X(kSequence).lat_bot(floor(nbSectors/2),subx(1));
        LongitudeBob(kSector,subx)     = X(kSequence).lon_bot(floor(nbSectors/2),subx(1));
        LatitudeSwath(kSector,subx)    = X(kSequence).lat_bot(kSector,subx(end));
        LongitudeSwath(kSector,subx)   = X(kSequence).lat_bot(kSector,subx(end));
    end
        
    pcolor(hAxe, X(kSequence).lon_bot, X(kSequence).lat_bot, I(suby,subx,kSequence)); colorbar
    titleFig = ['Index Threshold ',num2str(idThreshold), ' - Cycle ', num2str(kSequence) ' - Geometry : Polar'];
    title(titleFig, 'FontAngle', 'italic', 'FontSize', 8.0, 'FontWeight', 'bold');
    set(hAxe, 'CLim', CLim)
    shading flat
    axisGeo
    pause(0.5)
    
        
end
    
[~, name] = fileparts(nomFicXml);
BOB3DVData.name = name;

Colormap        = jet(256);

BOB3DVData.from = 'BOB';
BOB3DVData.Date = Date;  % 1, nbPings'
BOB3DVData.Hour = Heure; % 1, nbPings'

BOB3DVData.PingNumber       = 1:nbSequences;
BOB3DVData.DepthTop         = TransducerDepth + zeros(1, nbCells);
BOB3DVData.DepthBottom      = zeros(1, nbCells);

BOB3DVData.LatitudeTop      = LatitudeBob;
BOB3DVData.LongitudeTop     = LongitudeBob;

BOB3DVData.LatitudeBottom   = LatitudeSwath;
BOB3DVData.LongitudeBottom  = LongitudeSwath;

% Cr�ation des layers d'images pour 3DViewer.
J       = get_ImageInd(I, Colormap, CLim);
J       = permute(J, [1 3 2]);
flag    = create_XMLBin_BOB(J, nomFicXml, Colormap, BOB3DVData);

%% C'est fini

message(flag, nomFicXml)



function message(flag, nomFicXml)
if flag
    str1 = sprintf('La cr�ation du fichier "%s" est termin�e.', nomFicXml);
    str2 = sprintf('Processing "%s" is over.', nomFicXml);
else
    str1 = sprintf('La cr�ation du fichier "%s" ne s''est pas bien termin�e.', nomFicXml);
    str2 = sprintf('Processing "%s" failed.', nomFicXml);
end
my_warndlg(Lang(str1,str2), 0, 'Tag', 'ProcessingWCIsOver')



function J = get_ImageInd(I, Colormap, CLim)

J = gray2ind(mat2gray(I, double(CLim)), size(Colormap,1));
J(I == 0) = 1;
