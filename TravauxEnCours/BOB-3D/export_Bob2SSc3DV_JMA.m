% nomFic = 'C:\Temp\Andre\matcycle.mat';
% nomFic = 'C:\SonarScopeTbx\TravauxEnCours\BOB-3D\From JMA\matcycle.mat';
% flag = export_Bob2SSc3DV_JMA(nomFic);

function flag = export_Bob2SSc3DV_JMA(nomFic)

flag = 0;
if ~exist(nomFic, 'file')
    return
end

[nomDir, nomFicSeul] = fileparts(nomFic);
nomFicXml = fullfile(nomDir, [nomFicSeul '.xml']);

X = loadmat(nomFic, 'nomVar', 'Mat_cycle');
nbSequences = length(X);

figure;
hAxe = gca;
CLim = [0 50];
TransducerDepth = 0;

[nbSamples, nbPings] = size(X(1).Sa_bot);
I = NaN([nbSamples nbPings nbSequences]);
Date  = NaN(nbSequences,1);
Heure = NaN(nbSequences,1);
for kSequence=1:nbSequences
    nbSectors = length(X(kSequence).time_bot);
    LatitudeBob    = NaN(nbSectors,nbPings);
    LongitudeBob   = NaN(nbSectors,nbPings);
    LatitudeSwath  = NaN(nbSectors,nbPings);
    LongitudeSwath = NaN(nbSectors,nbPings);
    for kSector=1:nbSectors
        k = kSector;
        %     time_bot
        if ~ishandle(hAxe)
            break
        end
        subx = 1:size(X(kSequence).Sa_bot, 2);
        suby = 1:size(X(kSequence).Sa_bot, 1);
        I(suby,subx,k) = reflec_Amp2db(X(k).Sa_bot);
        pcolor(hAxe, X(k).lon_bot, X(k).lat_bot, I(suby,subx,k)); colorbar
        set(hAxe, 'CLim', CLim)
        shading flat
        axisGeo
        pause(0.5)
        
        t = cl_time('timeMat', X(k).time_bot(1));
        Date(k)      = t.date;
        Heure(k)     = t.heure;
        LatitudeBob(   k,subx) = X(k).lat_bot(1,subx);
        LongitudeBob(  k,subx) = X(k).lon_bot(1,subx);
        LatitudeSwath( k,subx) = X(k).lat_bot(end,subx);
        LongitudeSwath(k,subx) = X(k).lon_bot(end,subx);
    end
    
    [~, name] = fileparts(nomFicXml);
    seismic.name = name;
    
    Colormap = jet(256);
    % I = mat2ind(I, Colormap, CLim);
    
    seismic.from = 'Unkown';
    seismic.Date = Date;  % 1, nbPings'
    seismic.Hour = Heure; % 1, nbPings'
    
    seismic.PingNumber       = 1:nbSequences;
    seismic.DepthTop         = TransducerDepth + zeros(1, nbPings);
    seismic.DepthBottom      = zeros(1, nbPings);
    
    seismic.LatitudeTop      = LatitudeBob;
    seismic.LongitudeTop     = LongitudeBob;
    
    seismic.LatitudeBottom   = LatitudeSwath;
    seismic.LongitudeBottom  = LongitudeSwath;
    
    %{
Info.Images(1).Name           = 'LatitudeTop';
Info.Images(end).Dimensions   = 'nbSlices, nbPings';
Info.Images(end).Storage      = 'double';
Info.Images(end).Unit         = 'deg';
Info.Images(end).FileName     = fullfile(nomDirBin, 'LatitudeTop.bin');
    %}
    
    J = get_ImageInd(I, Colormap, CLim);
    flag = create_XMLBin_ImagesAlongNavigationMultiSlices(J, nomFicXml, Colormap, seismic);
end

%% C'est fini

message(flag, nomFicXml)



function message(flag, nomFicXml)
if flag
    str1 = sprintf('La cr�ation du fichier "%s" est termin�e.', nomFicXml);
    str2 = sprintf('Processing "%s" is over.', nomFicXml);
else
    str1 = sprintf('La cr�ation du fichier "%s" ne s''est pas bien termin�e.', nomFicXml);
    str2 = sprintf('Processing "%s" failed.', nomFicXml);
end
my_warndlg(Lang(str1,str2), 0, 'Tag', 'ProcessingWCIsOver')



function J = get_ImageInd(I, Colormap, CLim)

J = gray2ind(mat2gray(I, double(CLim)), size(Colormap,1));
J(I == 0) = 1;
