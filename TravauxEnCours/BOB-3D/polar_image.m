%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%
%%% fonction polar_image(img,theta,rho)
%%%
%%% permet de tracer une image en coordonn�es polaires
%%%
%%% en entr�e theta vecteur contenant les valeurs des secteurs depuis le point d'origine jusqu'au point d'arriv�e
%%%           rho  vecteur contenant les valeurs des distances depuis le point d'origine jusqu'au point d'arriv�e
%%%           img image � tracer
%%%
%%% note : il est important de voir que les vecteurs englobent les points de d�part et d'arriv�e, on a donc :
%%%    size(img,1)=length(rho)-1 (soit length(rho)=size(img,1)+1 !)
%%% et size(img,2)=length(theta)-1 (soit length(theta)=size(img,2)+1 !)
%%%
%%% en sortie image de la matrice en polaire, avec une �chelle de valeurs semblable � imagesc
%%%
%%% exemple : pour une image contenant 22 secteurs de 7� commen�ant � 10�
%%% et un range de 40 valeurs par pas de 2 et commen�ant � 2, on a une matrice de 40x22
%%% => la valeur du 1er secteur est entre 10 et 17� et le dernier entre 157 et 164�
%%%    la valeur du 1er range est entre 2 et 4 et la derni�re entre 80 et 82
%%%
%%% il faut theta=((0:22)*7+10).*pi./180; % longueur de theta 23 commence � 10 et finit � 164
%%%         rho=2:2:82; % longueur de rho 41 commence � 2 et finit � 82
%%%         figure
%%%         polar_image(theta,rho,img)
%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%







function polar_image(theta,rho,img)



[THETA,RHO]=meshgrid(theta,rho);

[XX,YY]=pol2cart(THETA,RHO); % transformation des coordonn�es polaires en cart�siennes pour chaque coin des pixels de l'image

img2=ones(size(img,1)+1,size(img,2)+1); % on cr�e une matrice plus grande d'une case dans chaque dimension, sinon l'image est coup�e d'une valeur
img2(1:end-1,1:end-1)=img;
figure
imagesc(img)
vv=caxis; % permet de r�cup�rer les valeurs min et max pour avoir une image avec un colormap acceptable
close gcf

pcolor(XX,YY,img2)
caxis(vv) % on remet la m�me plage de couleur qu'imagesc










