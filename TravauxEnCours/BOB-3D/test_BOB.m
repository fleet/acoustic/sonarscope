function flag = test_BOB

%%
delete('F:\SonarScopeData\BOB-3D\Data\Svalbard\SonarScope\Mat_EI_Cycles_Svalbard.mat');

nomFicXmlIn = 'F:\SonarScopeData\BOB-3D\Data\Svalbard\EI_Lay_SvalBard.bob';
a                   = cl_Bob('nomFic', nomFicXmlIn);
listCycles          = 1; %[1 5 10 16]; %1:get(a,'NbCycles'); %
% Liste des layers (ex: Reflectivity, RxBeamIndex, RxPathRange)
listLayerType       = [1 2 3];% 1:get(a,'NbCycles'); %
% Liste des index dans les layers 
listIndexType       = [0 1 1]; 
listThresholds      = [1 2]; %1:get(a,'NbThresholds'); %
[flag, b]           = import_BOB(cl_multidata([]), nomFicXmlIn, ...
                            'listNumLayers', listCycles, ...
                            'listLayerType', listLayerType, ...
                            'listIndexType', listIndexType, ...
                            'listThresholds', listThresholds); 
%% Pour debug
a                   = read_DataBob(nomFicXmlIn);

%%
delete('F:\SonarScopeData\BOB-3D\Data\Marmara\SonarScope\Mat_EI_Cycles_Marm.mat');

nomFicXmlIn = 'F:\SonarScopeData\BOB-3D\Data\Marmara\EI_Lay_Marmara.bob';
a                   = cl_Bob('nomFic', nomFicXmlIn);
listLayerType       = [1 5];% 1:get(a,'NbCycles'); %
% Liste des index dans les layers 
listIndexType       = [0 1]; 
listThresholds      = [1 2]; %1:get(a,'NbThresholds'); %
[flag, b]           = import_BOB(cl_multidata([]), nomFicXmlIn, ...
                            'listNumLayers', listCycles, ...
                            'listLayerType', listLayerType, ...
                            'listIndexType', listIndexType, ...
                            'listThresholds', listThresholds); 

%%
pppp = get(b, 'DataLayer');
subx                = 1:get(pppp(1), 'NbColumns');
suby                = 1:get(pppp(1), 'NbRows');
flagImageEntiere    = 1;

% Recherche d'images synchro :
% [indLayers, subx, suby] = gestionMultiImages(b, 1, flagImageEntiere, subx, suby);

%%
% [flag, a, nomFicXML]    = paramsExport3DViewerV3(a);
% remplac� par 
ImageName            = get(a ,'SurveyName');
filtreXml            = fullfile('F:\SonarScopeData\BOB-3D\3DV', [ImageName '.xml']);
[filename, pathname] = uiputfile('*.xml', 'Give a file name', filtreXml);
if isequal(filename,0) || isequal(pathname,0)
    flag = 0; 
    return
end

nomFic                  = fullfile(pathname, filename);
[pathname, nomFic, ext] = fileparts(nomFic);
nomFicXML               = fullfile(pathname, [nomFic ext]);

flag = 1;
%%
I0 = cl_image_I0;
if flag
    GeometryType = get_GeometryType(pppp(1));
    switch GeometryType
       case LatLong(I0)
% % %              flag = exportGeotiffSonarScope3DViewerMultiV3_GLU(b, nomFicXML, ...
% % %                 'sizeTiff', 2500, 'subx', subx, 'suby', suby);
             flag = exportGeotiffSonarScope3DViewerMultiV3(b, nomFicXML, ...
                'sizeTiff', 2500, 'subx', subx, 'suby', suby);
        otherwise
            str1 = 'Faut passer par l''autre version pour cette g�om�trie';
            str2 = 'Use the previous export for this geometry';
            my_warndlg(Lang(str1,str2), 1)
    end
end