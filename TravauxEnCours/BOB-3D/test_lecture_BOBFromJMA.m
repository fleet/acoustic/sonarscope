% Example :
% nomFicXml = 'J:\Data\IFREMER\Data_SSC\SSC-3DViewer-Data\3DViewer-CHARLIE\WC\BOB\From JMA\matcycle_GLU.xml';
% nomFicXml = 'F:\SonarScopeData\BOB-3D\3DV\EI_Lay_SvalBard_BOB3DV.xml';
%
% Data = test_lecture_BOBFromJMA(nomFicXml);
% ----------------------------------------------------------------------
function Data = test_lecture_BOBFromJMA(nomFicXml)


[nomDir, nomFicSeul, ext] = fileparts(nomFicXml);

flag    = exist(nomFicXml, 'file');
if ~flag
    return
end
Data = xml_mat_read(nomFicXml);
flag_Memmapfile = 0;
for i=1:length(Data.Signals)
    [flag, Signal] = readSscSignal(nomDir, Data.Signals(i), Data.Dimensions, ...
        'Memmapfile', flag_Memmapfile);
    if ~flag
        return
    end
    Data.(Data.Signals(i).Name) = Signal;
end


%% Lecture des fichiers binaires des images

for i=1:length(Data.Images)
    [flag, Image] = readSscSignal(nomDir, Data.Images(i), Data.Dimensions, 'Memmapfile', flag_Memmapfile);
    if ~flag
        return
    end
    Data.(Data.Images(i).Name) = Image;
end
