% Calcul de la bathy a partir des temps de parcours et des angles
% avec un modele de celerite par couches horizontales de gradient constant
%
% SYNTAX :
%   [Xbathy, Zbathy, AngleIntersec] = BathyRayon(ZCelerite, Celerite, AngleIncident, TempsSimple)
% 
% INPUT PARAMETERS : 
%   ZCelerite     : Profondeur des couches (m) : Vecteur de valeurs
%                   decroissantes (par ex de 0 a Zmax<0 )
%   Celerite      : Profil(s) de celerite en m/s aux profondeurs ZCelerite
%   AngleIncident : Angle(s) incident(s) a la surface : Vecteur
%   TempsSimple   : Temps aller-simple surface fond : Vecteur
%
% OUTPUT PARAMETERS : 
%   []            : Auto-plot activation
%   XBathy        : Abscisses des intersections des rayons avec le fond.
%   ZBathy        : Ordonnees des intersections des rayons avec le fond.
%   AngleIntersec : Angles des rayons sur le fond.
% 
% EXEMPLES :
%   nomFic = getNomFicDatabase('BATHYCEL_North-East_Atlantic_Winter.csv');
%   [Z, S, T] = lecCsv(nomFic);
%   C = celeriteChen( Z, T, S );
%   teta = -75:75;
%   [X, TH, t] = TrajetRayon(Z, C, teta);
%   ts = t(end, :);
%
%   [Xbathy, Zbathy, AngleIntersec] = BathyRayon(Z, C, teta, ts);
%       figure; plot(Xbathy, Zbathy, '*k')
%
%   C2 = C; C2(1:2) = C2(1:2) + 1;
%   [Xbathy, Zbathy, AngleIntersec] = BathyRayon(Z, C2, teta, ts);
%       hold on; plot(Xbathy, Zbathy, '*r')
%
%   [Xbathy, Zbathy, AngleIntersec] = BathyRayon(Z, C, teta + 0.05 * (rand(size(teta))-0.5), ts);
%       hold on; plot(Xbathy, Zbathy, '*r')
%
%   [Xbathy, Zbathy, AngleIntersec] = BathyRayon(Z, C, teta, ts, 'Immersion', -5);
%       hold on; plot(Xbathy, Zbathy, '*r')
%
% SEE ALSO : cl_car_bat_data/calculer_bathy TrajetRayon Authors
% AUTHORS  : YHDR
% VERSION  : $Id: BathyRayon.m,v 1.2 2002/06/20 09:52:55 augustin Exp $
%-------------------------------------------------------------------------------

function [XBathy, YBathy, ZBathy, AngleIntersec] = BathyRayon_GLT(ZCelerite, Celerite, AngleIncident, TempsSimple,  Pitch, TxAlongAngle, varargin)


%--------------------------------------------------------------------------
% Calcul de la position relative du pied du faisceau (par rapport �
% l'antenne), et de l'Azimuth du pied de faisceau
Ux = sind(AngleIncident);                              % ici pas de correction � faire de roulis (correction d�j� faites dans AngleIncident)
Uy = sind(Pitch + TxAlongAngle)*cosd(AngleIncident);   % m�me chose ici;
cosAz = Uy./sqrt(Ux.^2+Uy.^2);                         % cos de l'Azimuth du pied du faisceau en radian (Az positif sur tribord, -180�<Az<180�)
sinAz = Ux./sqrt(Ux.^2+Uy.^2);                         % sin de l'Azimuth du pied du faisceau en radian (Az positif sur tribord, -180�<Az<180�)

%--------------------------------------------------------------------------
% r�cup�ration du n� de figure pour M�J  graphique.
Fig = getPropertyValue(varargin, 'Fig', []);

%--------------------------------------------------------------------------
% On ne conserve que les valeurs uniques du profil de c�l�rit�.
subEqual = find(diff(Celerite) == 0);
ZCelerite(subEqual) = [];
Celerite(subEqual) = [];

%--------------------------------------------------------------------------
% R�cup�ration de l'immersion des transducteurs
Immersion = getPropertyValue(varargin, 'Immersion', ZCelerite(1));

if Immersion ~= ZCelerite(1)
    indImm = sum(ZCelerite >= Immersion);
    if indImm > 0
        % Interpolation du profil de c�l�rit� � l'immersion des transducteurs
        % et conservation du profil depuis cette immersion jusqu'au fond
        ImmCelerite = interp1(ZCelerite, Celerite, Immersion, 'linear');
        ZCelerite(indImm)   = Immersion ;        
        Celerite(indImm)    = ImmCelerite;
        ZCelerite           = ZCelerite(indImm:end);
        Celerite            = Celerite(indImm:end);
    end
end


ZCelerite     = ZCelerite(:);           % force le stockage en colonne
Celerite      = Celerite(:);            % force le stockage en colonne
nceler        = length(ZCelerite);      % nbre de c�l�rit� diff�rentes
nLayer      = nceler - 1 ;            % nbre de couches
hLayer      = -(diff(ZCelerite));     % �paisseur des couches
AngleIncident = AngleIncident(:)';      % force le stockage en ligne
TempsSimple   = TempsSimple(:)';        % force le stockage en ligne
nRayon        = length(AngleIncident);  % Nbre de rayon

% -----------------------------------------------------------------------
% Afin de ne pas rencontrer de probl�me de limite (changement de formule),
% on modifie les c�l�rit�s conduisant a des gradients nuls,
% le tout a la racine de la pr�cision machine 
% => jamais de propagation en ligne droite

DCelerite   = diff(Celerite);
preci       = sqrt(eps);
celermoy    = mean(Celerite);
realPreci   = preci * celermoy ;           % Pr�cision r�elle
IndDroite   = (abs(DCelerite)< realPreci);
if any(IndDroite)
    for icouche = 1:nLayer
        if IndDroite(icouche)
            Celerite(icouche+1) = Celerite(icouche) + realPreci ; % On ajoute la pr�cision machine
        end
    end
    DCelerite = diff(Celerite);     % M�J de la c�l�rit� avec diff non nulles
end

degrad = pi / 180 ;
raddeg = 1 / degrad ;

%  (meme manip sur les angles que sur
% les gradients, pour eviter les passages a la limite)
AngleIncident   = AngleIncident * degrad ;
realPreci       = 10 * preci ;
AngleIncident(abs(AngleIncident)<realPreci) = realPreci ;

% ------------------
% constante de rayon

Cste    = sin(AngleIncident)/Celerite(1);   % sin(teta0)/c0, constante quelque soit la couche (Snell-descartes)
gradCel = DCelerite ./ hLayer;              % gradient au sein de chaque couche
teta    = asin(Celerite * Cste);            % = teta(i), angle de sortie de couche
t       = log(tan(teta/2));                 % = t(i), temps de sortie de couche


% --------------------------------------------------
% Temps de parcours des nLayer-1 premieres couches

dt     = diff(t) ./ ( gradCel * ones(1,nRayon));   % = t(i+1)-t(i) tps de parcours au sein de chaque couche
tTotal = [zeros(1,nRayon) ; cumsum(dt,1)];         % tps de parcours cumul�

% ------------------------------------------------
% Identification de la couche ou s'arrete le rayon

iLastLayer      = sum( tTotal < (ones(nLayer+1,1)*TempsSimple) ) ;       % indice de l'avant derni�re couche pour chaque rayon
indexLastLayer  = iLastLayer+(0:(nLayer+1):(nRayon-1)*(nLayer+1)) ;    % indexation simple (colonne) plut�t que double (optimisation)

% ---------------------------------------------------
% temps de parcours dans la derniere couche traversee

dt_last = TempsSimple - tTotal(indexLastLayer);

% ------------------------------------------------------------------
% Calcul du gradient dans la derniere couche, extrapole vers -infini

gradCel = [gradCel ; gradCel(end)];

sub = find(iLastLayer == 0);
if ~isempty(sub)
    iLastLayer(sub) = 1;%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
end


gradCel_last = gradCel(iLastLayer)'; % gradient entre l'avant derni�re couche et le fond

% -------------------------
% Angles d'incidence au sol

AngleIntersec = 2*(atan(tan(teta(indexLastLayer)/2).*exp(gradCel_last.*dt_last)));

% ---------------
% Celerite au sol

cele_last = sin(AngleIntersec) ./ Cste; % car c(end)/sin(teta(end)) = cste

% ------------
% Bathy au sol

dz_last = (cele_last - Celerite(iLastLayer)') ./ gradCel_last;      % �paisseur entre avant derni�re couche et fond
Zbathy  = ZCelerite(iLastLayer)' - dz_last;                         % on ajoute cette �paisseur
Zbathy  = Zbathy - Immersion;                                       % correction de l'immersion des transducteurs

% ---------------
% Abscisse au sol

rCourb      = - ((Celerite./gradCel) * ones(1,nRayon)) ./ sin(teta);     % rayon de courbure dans chaque couche pour chaque faisceau
xrelatif    = rCourb(1:end-1,:) .* diff(cos(teta));                      % distance lat�rale en sortie de chaque couche
xtotal      = [zeros(1,nRayon) ; cumsum(xrelatif,1) ];                   % distance lat�rale cumul�e

Xbathy = xtotal(indexLastLayer) - ...
    (cos(AngleIntersec)-cos(teta(indexLastLayer))) .* Celerite(iLastLayer)' ./ gradCel_last ./ sin(teta(indexLastLayer)); % on ajoute la distance lat�rale entre l'avant derni�re couche et le fond

AngleIntersec = AngleIntersec * raddeg ; 


% ----------------------------------------------------------------
% Distances Transversale et Longitudinale parcourues par le fasceau

XBathy = abs(Xbathy).*sinAz;
YBathy = abs(Xbathy).*cosAz;
ZBathy = Zbathy;

% -----------------------------------------
% Sortie des parametre ou traces graphiques

if nargout == 0
    if isempty(Fig)
        figure;
    else
        figure(Fig)
        hold off
    end
    PlotUtils.createSScPlot(XBathy, ZBathy, '*'); grid on; hold on;
    xlabel('X (m)'); ylabel('Z (m)'); title('Trajet rayon');
    axis equal
end

