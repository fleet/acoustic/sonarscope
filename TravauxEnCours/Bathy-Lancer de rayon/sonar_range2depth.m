% sonar_range2depth7111 ente 2 images
%
% SYNTAX :
%   [c, status] = sonar_range2depth(a, b, ...) 
%
% INPUT PARAMETERS :
%   a : Instance de cl_image de type "RayPathSampleNb"
%   b : Instance de cl_image de type "RxBeamAngle"
%
% PROPERTY NAMES / PROPERTY VALUES :
%   subx : Sous-echantillonnage en abscisses
%   suby : Sous-echantillonnage en ordonnees
%
% OUTPUT PARAMETERS :
%   c      : Instances de cl_image contenant "Bathymetry", "MbesRangeAcross"
%            et "MbesRangeAlong"
%   status : 1=tout c'est bien^passe
%
% EXAMPLES : 
%
% SEE ALSO : cl_image Authors
% AUTHORS  : JMA
% VERSION  : $Id: sonar_range2depth.m,v 1.1 2005/09/14 08:23:13 augustin Exp augustin $
% ----------------------------------------------------------------------------

function [c, this, status] = sonar_range2depth(this, b, ~, ProfilCelerite, varargin)

% COrrectif � faire en amont (rencontr� dans le cas de donn�es Reson non
% pass�es par PDS
this.Sonar.Heave(abs(this.Sonar.Heave) > 100) = 0;

subx     = getPropertyValue(varargin, 'subx', 1:this.NbColumns);
suby     = getPropertyValue(varargin, 'suby', 1:this.nbLig);

SystemSerialNumber = get(this.Sonar.Desciption, 'SystemSerialNumber');
switch SystemSerialNumber
    case 7150
        RollCoef = 0;
    case 7111
        RollCoef = 1;
    otherwise
        my_warndlg('Roll effect condidered as already set in the angles. If it is not the case for your sounder please contact Jean-Marie to correct this "otherwise" part of the sofrware or to suppress this message for your sounder.', 1, 'Tag', 'MessageSonar_range2depth')
        RollCoef = 0;
end

% ---------------------
% Controles de validite

identRayTimeLength     = identDataType(cl_image_I0, 'RayPathTravelTime');
identRayPathSampleNb   = identDataType(cl_image_I0, 'RayPathSampleNb');
identRayPathTravelTime = identDataType(cl_image_I0, 'RayPathTravelTime');
identRayPathRange      = identDataType(cl_image_I0, 'RayPathRange');

status = testSignature(this, ...
    'DataType', [identRayTimeLength identRayPathSampleNb identRayPathTravelTime identRayPathRange], ...
    'Message', 'Pour realiser cette operation, il faut etre positionne sur un Layer de DataType="RayPathTravelTime"'); 
if ~status
    c = [];
    return
end


% if b.DataType ~= identDataType(cl_image_I0, 'TxAngle');%  RxBeamAngle'); % CORRIGER CECI dans view
%     str = sprintf('La Layer de "RxBeamAngle" n''est pas bon');
%     msgbox(str, 'IFREMER')
%     return
% end

% Attention : sort(sublLM) peut etre utile pour supprimer les tests de
% flipud (voir cl_image/plus

[XLim, YLim, subx, suby, subxb, subyb] = intersectionImages(this, subx, suby, b);%#ok
if isempty(subx) || isempty(suby)
    str = sprintf('Pas d''intersection commune entre les images');
    my_warndlg(str, 1);
    return
end

c(1) = this;
c(1) = replace_Image(c(1), NaN(length(suby), length(subx), 'single'));

c(2) = this;
c(2) = replace_Image(c(2), NaN(length(suby), length(subx), 'single'));

c(3) = this;
c(3) = replace_Image(c(3), NaN(length(suby), length(subx), 'single'));

Time = this.Sonar.Time;

if isempty(ProfilCelerite)
    BathyCel = this.Sonar.BathyCel;
    if isempty(BathyCel.Z)
        this = init_BathyCel(this);
    end
end


Heave             = this.Sonar.Heave;
TransducerDepth   = this.Sonar.Immersion;
%{
SurfaceSoundSpeed = this.Sonar.SurfaceSoundSpeed;
TxAlongAngle      = this.Sonar.TxAlongAngle;
Pitch             = this.Sonar.Pitch;
figure;
h(1) = subplot(3,1,1); plot(TxAlongAngle, '*'); grid on; title('TxAlongAngle');
h(2) = subplot(3,1,2); plot(Pitch, '*'); grid on; title('Pitch');
h(3) = subplot(3,1,3); plot(TxAlongAngle+Pitch, '*'); grid on; title('TxAlongAngle + Pitch');
linkaxes(h, 'x')
%} 

TideValue       = this.Sonar.Tide;
if isempty(TideValue)
    TideValue = zeros(length(TransducerDepth),1);
end

SamplingRate    = this.Sonar.SampleFrequency;

W = warning;
warning('off') %#ok<WNOFF>
h = create_waitbar('Processing, please wait ...', 'Entete', 'IFREMER - SonarScope');
M = length(suby);
for i=1:M
    if mod(i, floor(M/20)) == 1
        waitbar(i/M, h);
        OnTrace = 1;
    else
        OnTrace = 0;
    end
    
    if ~isempty(ProfilCelerite)
        difTime = ProfilCelerite.TimeStartOfUse - Time(suby(i));
        difTime(difTime > 0) = [];
        k = length(difTime);
        if k == 0
            k = 1; %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        end        
        ZCelerite = ProfilCelerite.Depth(k,:);
        Celerite  = ProfilCelerite.SoundSpeed(k,:);
    else
        ZCelerite = this.Sonar.BathyCel.Z;
        Celerite  = this.Sonar.BathyCel.C;
    end

    switch this.DataType
        case identRayTimeLength
            TempsSimple =  abs(this.Image(suby(i), subx)) / (2*1000);
        case identRayPathSampleNb % SIMRAD
            TempsSimple = this.Image(suby(i), subx) / (2*SamplingRate(suby(i))); % Simrad
        case identRayPathTravelTime
%             TempsSimple = abs(this.Image(suby(i), subx)) / (2*SamplingRate*1e-3/1500);
        case identRayPathRange
            TempsSimple = abs(this.Image(suby(i), subx)) / 1500;
            
        otherwise
            my_warndlg('Message pour JMA : Ici je sais pas faire', 1)
    end

    AngleIncident = b.Image(subyb(i), subxb);
    
    sub = find(~isnan(AngleIncident) & (TempsSimple > eps));
    
    Immersion = TransducerDepth(suby(i));
    if isnan(Immersion)
        Immersion = -1.5;
    end
    Z = ZCelerite;
    C = Celerite;
    
    if (RollCoef == 0) || isempty(this.Sonar.Roll)
        A = AngleIncident(sub);
    else
        A = AngleIncident(sub) - RollCoef * (this.Sonar.Roll(i) + this.Sonar.Ship.MRU.RollCalibration);
    end    

    deltaT = 0;
    timeRx = double(TempsSimple(sub)) - deltaT;
    
    if all(isnan(Z))
        Xbathy =  (1500 * timeRx) .* sind(AngleIncident(sub));
        Zbathy = -(1500 * timeRx) .* cosd(AngleIncident(sub));
    else
        pitch        = this.Sonar.Pitch(i) + this.Sonar.Ship.MRU.PitchCalibration; %corrig�e des donn�es de calibration
        txAlongAngle = this.Sonar.TxAlongAngle(i);
        %heave       = this.Sonar.Heave(i);                
        if OnTrace
            %BathyRayon(Z, C, A, timeRx, 'Immersion', Immersion, 'Fig', 753943);            
            BathyRayon_GLT(Z, C, A, timeRx,  pitch, txAlongAngle, 'Immersion', Immersion, 'Fig', 753943);
        end
        %[Xbathy, Zbathy] = BathyRayon(Z, C, A, timeRx, 'Immersion', Immersion);%#ok
        [Xbathy, Ybathy, Zbathy] = BathyRayon_GLT(Z, C, A, timeRx, pitch, txAlongAngle, 'Immersion', Immersion, 'Fig', 753943);
        
    end
    
    subComplex = (imag(Zbathy) ~= 0);
    Zbathy(subComplex) = NaN;
    Xbathy(subComplex) = NaN;

    % ----------------------------
    % Calcul du pilonnement induit
    
	TimeTx      = this.Sonar.Time.timeMat;
    UTMtimeRx   = TimeTx(suby(i)) + timeRx / (24*3600);
	p           = sonar_pilonnementInduit(this, suby(i), UTMtimeRx);
    Zbathy      = Zbathy + p;
    
%     Zbathy = Zbathy + (TransducerDepth(suby(i)) - Heave(suby(i)) + TideValue(suby(i)));
    Zbathy = Zbathy + (TransducerDepth(suby(i)) + TideValue(suby(i)));

%     angleAlong = angleAlong * (pi/180);
    if all(isnan(Heave))
        c(1).Image(i, sub) = Zbathy;
    else
        c(1).Image(i, sub) = Zbathy + Heave(suby(i));
    end
    %c(1).Image(i, sub) = Zbathy;
    c(2).Image(i, sub) = Xbathy;
    c(3).Image(i, sub) = Ybathy;
end
close(h)
warning(W)

% --------------------------
% Mise a jour de coordonnees

c(1) = majCoordonnees(c(1), subx, suby);
c(2) = majCoordonnees(c(2), subx, suby);
c(3) = majCoordonnees(c(3), subx, suby);

% -----------------------
% Calcul des statistiques

c(1) = calculer_stats(c(1));
c(2) = calculer_stats(c(2));
c(3) = calculer_stats(c(3));

% -------------------------
% Rehaussement de contraste

c(1).TagSynchroContrast = num2str(rand(1));
c(2).TagSynchroContrast = num2str(rand(1));
c(3).TagSynchroContrast = num2str(rand(1));
CLim = [c(1).StatValues.Min c(1).StatValues.Max];
c(1).CLim          = CLim;
CLim = [c(2).StatValues.Min c(2).StatValues.Max];
c(2).CLim          = CLim;
CLim = [c(3).StatValues.Min c(3).StatValues.Max];
c(3).CLim          = CLim;
c(1).ColormapIndex    = 3;
c(2).ColormapIndex    = 3;
c(3).ColormapIndex    = 3;
c(1).DataType    = identDataType(this, 'Bathymetry');
c(2).DataType    = identDataType(this, 'MbesRangeAcross');
c(3).DataType    = identDataType(this, 'MbesRangeAlong');
c(1).Unit          = 'm';
c(2).Unit          = 'm';
c(3).Unit          = 'm';

% -------------------
% Completion du titre

c(1).Titre = [c(1).Titre '_Computed'];
c(1) = set_FullImageName(c(1));
c(2).Titre = [c(2).Titre '_Computed'];
c(2) = set_FullImageName(c(2));
c(3).Titre = [c(3).Titre '_Computed'];
c(3) = set_FullImageName(c(3));

% -----------------
% Par ici la sortie

status = 1;
