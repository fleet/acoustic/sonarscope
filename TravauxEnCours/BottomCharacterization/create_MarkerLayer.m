function flag = create_MarkerLayer

global DEBUG
%% Avec affichage g�ographique.
if DEBUG
    figure(111111);
    load coast
    hAxesm =  worldmap('world');
    setm(hAxesm,'MapLatLimit',[-45.5 -35.5]);
    setm(hAxesm,'MapLonLimit',[167.5 178.5]);
    setm(hAxesm,'ParallelLabel', 'on');
    setm(hAxesm,'MeridianLabel', 'on');
    setm(hAxesm, 'MLineLocation',   5.0);
    setm(hAxesm, 'MLabelLocation',  5.0);
    setm(hAxesm, 'PLineLocation',   2.0);
    setm(hAxesm, 'PLabelLocation',  2.0);
    geoshow(lat, long); 
    hold on;
    tightmap;
end
%% Chargement des donn�es
nomFic                 = 'G:\IFREMER\Doc\CR-Rapports\SonarScope\120417-3DViewer-Evol IHM\Data\SupportingInfo\BarrettReef_FieldData_WGS84Combined.xlsx';
[flag, dataFromFic]    = readBottomCharacterization(nomFic);

if DEBUG
    plotm(dataFromFic.Latitude, dataFromFic.Longitude, 'r*');
end
%% Ecriture en points csv des donn�es des nouveaux types d'objets Markers Surfaciques.
% Relief du sol
[nomDir, nomFicSeul, ext] = fileparts(nomFic);

% Pr�voir plus tard :
% - des valeurs qualitatives non quantitatives (prim et Second Hab)
% - des m�tadata (n� de la vid�o ...)
% - d�cliner les markers : camembert, cha�ne de caract�res (# vid�o), 

% Indicateur de pr�sence des Sans ripples.
layerName   = 'SandWaves';
nomFicSC    = fullfile(my_tempdir, [nomFicSeul '_' layerName '.sc']);

fid = fopen(nomFicSC, 'w');
if ~fid
    my_warndlg(Lang('Impossible de g�n�rer le fichier de sortie de Soil Composition', ...
                    'Writing impossible of soil composition file'),1);
    return
end
fprintf(fid, '"latitude", "longitude", "SandWaves", "Comments"\n');
% Ecriture de l'ent�te.
for k=1:numel(dataFromFic.ID)
    fprintf(fid, '%15.12f, %15.12f, %3d, %s\n', dataFromFic.Latitude(k), dataFromFic.Longitude(k), dataFromFic.SandWaves(k), dataFromFic.SAndWavecomments{k});
end
fclose(fid);

% Composition du sol
% Indicateur de pr�sence des Sans ripples.
layerName   = 'Habitat';
nomFicSC    = fullfile(my_tempdir, [nomFicSeul '_' layerName '.sc']);

fid = fopen(nomFicSC, 'w');
if ~fid
    my_warndlg(Lang('Impossible de g�n�rer le fichier de sortie de Soil Composition', ...
                    'Writing impossible of soil composition file'),1);
    return
end
fprintf(fid, '"latitude", "longitude", "PrimHab", "SecHab", "Comments"\n');
% Type de sol identifi�.
typeSoilPrim    = unique(dataFromFic.PrimHab);
typeSoilSecond  = unique(dataFromFic.SecHab);
% Ecriture de l'ent�te.
for k=1:numel(dataFromFic.ID)
    idPrimHab = strcmp(typeSoilPrim, dataFromFic.PrimHab(k));
    [~, isubPrimHab] = find(idPrimHab == 1);
    idSecHab = strcmp(typeSoilSecond, dataFromFic.SecHab(k));
    [~, isubSecHab] = find(idSecHab == 1);
    fprintf(fid, '%15.12f, %15.12f, %3d, %3d, %s\n', dataFromFic.Latitude(k), dataFromFic.Longitude(k), isubPrimHab, isubSecHab, dataFromFic.Notes{k});
end
fclose(fid);

%% Ecriture en points csv des donn�es des Markers Cartographiques.
[nomDir, nomFicSeul, ext] = fileparts(nomFic);

% Pr�voir plus tard :
% - des valeurs qualitatives non quantitatives (prim et Second Hab)
% - des m�tadata (n� de la vid�o ...)
% - d�cliner les markers : camembert, cha�ne de caract�res (# vid�o), 

% Extraction de marqueurs cartographiques sur le champ Prim Hab.
for i=1:numel(typeSoilPrim)
    typeSoil        = regexprep(typeSoilPrim{i}, ' ','');
    nomDirMarkers   = fullfile(my_tempdir, 'CartoMarkers');
    if ~exist(nomDirMarkers, 'dir')
        mkdir(nomDirMarkers);
    end
    nomFicMarkers   = fullfile(nomDirMarkers, [nomFicSeul '_' typeSoil '.csv'])

    fid = fopen(nomFicMarkers, 'w');
    if ~fid
        my_warndlg(Lang('Impossible de g�n�rer le fichier de sortie de Markers Cartographiques', ...
                        'Writing impossible of Carto Markers file'),1);
        return
    end
    idPoint = 0;
    fprintf(fid, '"id","type","lon(deg)","lat(deg)","immersion","ref","layer","ping","topPoint","line","bottomPoint","pointColor","pointShape","pointSize"\n');
    % Ecriture de l'ent�te.
    for k=1:numel(dataFromFic.ID)
        % k �quivaut au num�ro de pr�l�vement (ou Ping dans notre cas par
        % �quivalence avec le WC).
        if strcmp(dataFromFic.PrimHab(k), typeSoilPrim{i})
            fprintf(fid, '"%2d","LINE","%15.12f","%15.12f","%8.4f","ABSOLUTE","%s","%d","false","true","true","yellow","plain","45"\n', ...
                        idPoint,dataFromFic.Longitude(k),dataFromFic.Latitude(k),-str2double(dataFromFic.Depth{k}), 'Prim Habitat', k);
            fprintf(fid, '"%2d","LINE","%15.12f","%15.12f","%8.4f","CLAMP_TO_GROUND","%s","%d","false","true","true","yellow","plain","45"\n', ...
                        idPoint,dataFromFic.Longitude(k),dataFromFic.Latitude(k),0.0, 'Prim Habitat', k);
            idPoint = idPoint + 1;
        end
    end
    fclose(fid);
end
%% Ecriture en points XML des donn�es des Markers Cartographiques.
[nomDir, nomFicSeul, ext] = fileparts(nomFic);

% Pr�voir plus tard :
% - des valeurs qualitatives non quantitatives (prim et Second Hab)
% - des m�tadata (n� de la vid�o ...)
% - d�cliner les markers : camembert, cha�ne de caract�res (# vid�o), 
%% Chargement des couleurs.
% http://www.commentcamarche.net/contents/html/htmlcouleurs.php3
nomFicColors    = getNomFicDatabase('listColorsCodeHexa.csv');
fid             = fopen(nomFicColors, 'r');
C = textscan(fid,'%s%s', 'delimiter', ';');
fclose(fid);

% Extraction de marqueurs cartographiques sur le champ Prim Hab.
typeSoil        = regexprep(typeSoilPrim{i}, ' ','');
nomDirMarkers   = fullfile(my_tempdir, 'CartoMarkers');
if ~exist(nomDirMarkers, 'dir')
    mkdir(nomDirMarkers);
end
nomFicXml   = fullfile(nomDirMarkers, [nomFicSeul '.xml'])

PieMarkers.Title         = 'Classe Markers';
PieMarkers.FormatVersion = '20130419';
PieMarkers.Comments      = 'Created from original Data';
PieMarkers.Filename      = nomFic;

% Enregistrement des classes
for k=1:numel(typeSoilPrim)
    if k==1
        PieMarkers.DataTypeDescriptor.DataType(1).Id   = k;
    else
        PieMarkers.DataTypeDescriptor.DataType(end+1).Id   = k;
    end
    PieMarkers.DataTypeDescriptor.DataType(end).Name       = typeSoilPrim{k};
    PieMarkers.DataTypeDescriptor.DataType(end).Color      = C{2}{k,1};
    PieMarkers.DataTypeDescriptor.DataType(end).NameColor  = C{1}{k,1};
end

% Enregistrement des points
for k=1:numel(dataFromFic.ID)
    if k==1
        PieMarkers.PieMarker(1).Id       = k;
    else
        PieMarkers.PieMarker(end+1).Id   = k;
    end
    [C, ia, ~] = intersect(typeSoilPrim, dataFromFic.PrimHab(k));
    PieMarkers.PieMarker(end).Lat              = dataFromFic.Latitude(k);
    PieMarkers.PieMarker(end).Lon              = dataFromFic.Longitude(k);
    PieMarkers.PieMarker(end).PieData.DataTypeId  = ia;
    PieMarkers.PieMarker(end).PieData.Percent     = 1; % �quivaut � 100.
end

% Pref.RootOnly       = false;
Pref.StructItem     = false;

xml_write(nomFicXml, PieMarkers, 'PieMarkers', Pref);

