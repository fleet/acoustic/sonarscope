% Lecture d'un fichier de composantes de fond 
%
% SYNTAX :
%   flag        = readBottomCaracterisation(nomFic);
%
% INPUT PARAMETERS :
%   nomFic     : Nom du fichier
%
% OUTPUT PARAMETERS :
%   flag        : indicateur de bon fonctionnement.
%
% EXAMPLES :
%   nomFic          = 'G:\IFREMER\Doc\CR-Rapports\SonarScope\120417-3DViewer-Evol IHM\Data\SupportingInfo\BarrettReef_FieldData_WGS84Combined.xlsx';
%   [flag, Data]    = readBottomCharacterization(nomFic);
%
% SEE ALSO  : /
% AUTHORS   : GLU
%-------------------------------------------------------------------------------
function [flag, data] = readBottomCharacterization(nomFic)

flag = 0;


% C = dlmread(nomFic, '\t', 1, 0);
nomData = {};

[num,txt,raw]   = xlsread(nomFic);
nomColumn       = raw(1,:);

% Remplissage des champs de donn�es.
% idVar       = strcmp(nomColumn, nameField);
% [sub, isub] = find(idVar == 1);
% data(:).ID  = [raw{2:end,isub}];

sz              = size(raw);
nbLinesHeader   = 1;
for k=1:numel(nomColumn)
    nameField   = nomColumn{k};
    expression  = '\(m\)|[# ()/]';
    nameField   = regexprep(nameField, expression,'');
    % idVar       = strcmp(nomColumn, nameField);
    % [sub, isub] = find(idVar == 1);
    if ischar(raw{nbLinesHeader+1:nbLinesHeader+1,k})
        data(:).(nameField) = raw(nbLinesHeader+1:end,k)';
    else
        data(:).(nameField) = [raw{nbLinesHeader+1:end,k}];
    end
end

flag = 1;

