classdef clImage2019 < handle
    % Description
    %   New classe clImage2019 pour testing clCRS
    %
    % Syntax
    %   I = clImage2019(...)
    %
    % class clImage2019 properties :
    %   TODO
    %
    % Examples
    % I = clImage2019('TestCarto'); % Carto par défaut = EPSG4326
    % I.lon = -5.1:0.1:3.2;
    % I.lat = 48.2:0.1:49.5;
    %
    % zone = utmzone(48.2,-5.1);
    % strProj = sprintf('326%s', zone(1:2));
    % a = clCRS('NlleCarto', 'ProjectionId', strProj);
    % I.set('Carto', a);
    % I.latLon2xy([]);    
    %
    % a = clCRS('NlleCarto', 'ProjectionId', '4326');
    % I.set('Carto', a);
    % I.xY2latLon([]);    
    % 
    % codeEPSG = 25832;
    % X = repmat(645002.94332, [1 5]);
    % Y = repmat(6041810.55417, [1 5]);
    % c = clCRS('NlleCarto', 'ProjectionId', codeEPSG);
    % I = clImage2019('TestCartoDom'); % Carto par défaut = EPSG4326
    % I.set('Carto', c);
    % I.x = X;
    % I.y = Y;
    % I.xy2latLon([])

    % Authors : GLU
    % See also clEllipsoid, clProjection
    % ----------------------------------------------------------------------------

    %% Strcuture definition
    properties (Access = public)    
        Titre   = 'Image';       % Rename in "Name"
        Carto   = clCRS('Origine');   % Nlle Carto
        x       = [];   	
        y       = [];   	
        lon     = [];   	
        lat     = [];   	
    end

    methods
        function obj = clImage2019(title)
            obj.Titre = title;
        end
        
        function obj = set(obj, varargin)
            [varargin, targetCarto] = getPropertyValue(varargin, 'Carto', []);
            if ~isempty(targetCarto)
                % Carto assignation
                obj.Carto = targetCarto;
                
                % Conversion des coordonnées
                % TODO
            end
        end
        
        function obj = latLon2xy(obj, varargin)

            % Coordinates formatting
            [Lon, Lat]  = meshgrid(obj.lon, obj.lat);

            % Coordinates conversion
            MStruct     = obj.Carto.projection.mstruct;
            [X, Y]      = mfwdtran(MStruct, Lat, Lon);

            % Calcul des coordonnées géographiques centrées
            deltaX  = sqrt(mean(diff(X(:,1))) ^2 + mean(diff(X(1,:))) ^ 2);
            deltaY  = sqrt(mean(diff(Y(:,1))) ^2 + mean(diff(Y(1,:))) ^ 2);
            deltaX  = (deltaX + deltaY) / 2;
            deltaY  = deltaX;
            xMin    = min(X(:));
            xMax    = max(X(:));
            yMin    = min(Y(:));
            yMax    = max(Y(:));
            xGrid   = xMin:deltaX:xMax;
            yGrid   = yMin:deltaY:yMax;
            [obj.x, deltaX1, xMin, xMax] = centrage_magnetique(xGrid); %#ok
            [obj.y, deltaY1, yMin, yMax] = centrage_magnetique(yGrid); %#ok
        end
        
        function obj = xy2latLon(obj, varargin)

            % Coordinates formatting
            [X, Y]      = meshgrid(obj.x, obj.y);

            % Coordinates conversion
            MStruct     = obj.Carto.projection.mstruct;
            [Lat, Lon]  = minvtran(MStruct, X, Y);

            % Calcul des coordonnées géographiques centrées
            deltaLon  = sqrt(mean(diff(Lon(:,1))) ^2 + mean(diff(Lon(1,:))) ^ 2);
            deltaLat  = sqrt(mean(diff(Lat(:,1))) ^2 + mean(diff(Lat(1,:))) ^ 2);
            deltaLon  = (deltaLon + deltaLat) / 2;
            deltaLat  = deltaLon;
            lonMin    = min(Lon(:));
            lonMax    = max(Lon(:));
            latMin    = min(Lat(:));
            latMax    = max(Lat(:));
            if deltaLon > 0
                Lon = lonMin:deltaLon:lonMax;
                [obj.lon, deltaLon1, lonMin, lonMax] = centrage_magnetique(Lon); %#ok
            else
                obj.lon = repmat(lonMin, [1 numel(obj.x)]); % Pas de variabilité
            end
            if deltaLat > 0
                Lat = latMin:deltaLat:latMax;
                [obj.lat, deltaLat1, latMin, latMax] = centrage_magnetique(Lat); %#ok
            else
                obj.lat = repmat(latMin, [1 numel(obj.y)]); % Pas de variabilité
            end
                
        end
       
    end
end

