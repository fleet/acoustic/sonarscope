classdef (Abstract) AbstractDisplayCRS < handle & matlab.mixin.CustomDisplay
    % AbstractDisplay
    %   The class which implement this Abstract class have to implement
    %       the protected method displayScalarObjectCustom(this)
% %     
% %     methods
% %         
% %     end
% %     
    methods (Access = protected, Sealed = true)
        
        function p2 = editPropertiesChild(this, propertyName, propertyValue)
            
            p2 = com.jidesoft.grid.DefaultProperty();
            p2.setName(propertyName);
            p2.setEditable(false);
            
            structFieldNameList = fieldnames(propertyValue);
            for k3=1:length(structFieldNameList)
                structFieldValue = eval(sprintf('propertyValue.%s', structFieldNameList{k3}));
                if isstruct(structFieldValue) || isa(structFieldValue, 'referenceEllipsoid')
                    p2 = editPropertiesChild(this, structFieldNameList{k3}, structFieldValue);
                else
                    p2 = EditPropertiesUtils.addItemText(p2, ...
                        'Name', structFieldNameList{k3}, ...
                        'Value', structFieldValue);
                end
            end
            
        end        
        function displayNonScalarObject(this)
            % Called when 'this' is a non-scalar object
            
            % Display Name / Size / Class
            dispWhos(this, inputname(1));
            
            % Call the custom display for the non-scalar object
            str = createNonScalarObjectDisplay(this);
            disp(str);
        end
        
        function str = createNonScalarObjectDisplay(this)
            % Summary of the object instance(s) in a character array
            
            N = numel(this);
            str = cell(2*N,1);
            for k=1:N
                str{2*k-1} = sprintf('-----Element (%d/%d) -------', k, N);
                str1 =  createScalarObjectDisplay(this(k));
                str1 = [repmat('   ', size(str1,1), 1) str1]; %#ok<AGROW>
                
                str{2*k} = str1;
            end
            str = cell2str(str);
            
        end
        
        function displayScalarObject(this)
            % Called when 'this' is a saclar object
            
            % Display Name / Size / Class
            dispWhos(this, inputname(1));
            
            % Call the custom display for the scalar object
            str =  createScalarObjectDisplay(this);
            disp(str);
        end
        
        
        function str = createScalarObjectDisplay(this)
            % Display Name / Size / Class
            %                         dispWhos(this, inputname(1));
            
            str = {};
            metaClass = metaclass(this);
            str{end+1} =  sprintf('------- %s -------', metaClass.Name);
            
            N = numel(metaClass.PropertyList);
            for k=1:N
                if strcmp(metaClass.PropertyList(k).GetAccess, 'public') % check if public
                    
                    propertyName  = metaClass.PropertyList(k).Name;
                    propertyValue = eval(sprintf('this.%s', propertyName));
                    
                    %                     	str{end+1} =  sprintf('%s : Array of datetime [%d,1] : %s to %s', propertyName, size(propertyValue,1), propertyValue(1), propertyValue(end));
                    if isempty(propertyValue)
                        str{end+1} = sprintf('%s :', propertyName); %#ok<AGROW>
                        
                    elseif isstring(propertyValue)
                        str{end+1} = sprintf('%s : ''%s''', propertyName, propertyValue); %#ok<AGROW>
                        
                    elseif ischar(propertyValue)
                        str{end+1} =  sprintf('%s : ''%s''', propertyName, propertyValue); %#ok<AGROW>
                        
                    elseif islogical(propertyValue)
                        str{end+1} =  sprintf('%s : %d', propertyName, propertyValue); %#ok<AGROW>
                        
                    elseif isnumeric(propertyValue)
                        strSize = strcat(class(propertyValue), ' - [',  num2str(size(propertyValue)), ']');
                        if isempty(propertyValue)
                            str{end+1} = sprintf('%s : Array of %s', propertyName, strSize); %#ok<AGROW>
                        elseif length(propertyValue) > 10
                            str{end+1} = sprintf('%s : Array of %s : Summary : %s', propertyName, strSize, summaryArrayValues(propertyValue(:)')); %#ok<AGROW>
                        else
                            str{end+1} = sprintf('%s : Array of %s : Values : %s', propertyName, strSize, num2str(propertyValue(:)')); %#ok<AGROW>
                        end
                        
                        % D�but ajout JMA le 07/12/2018
                    elseif isdatetime(propertyValue)
                        if length(propertyValue) > 10
                            str{end+1} = sprintf('%s : Array of datetime - %s : %s to %s', ...
                                propertyName, strcat('[', num2str(size(propertyValue)), ']'), char(propertyValue(1)), char(propertyValue(end))); %#ok<AGROW>
                        else
                            str{end+1} = sprintf('%s : datetime : %s', propertyName, char(propertyValue)); %#ok<AGROW>
                        end
                        % Fin ajout JMA le 07/12/2018
                        
                    elseif isstruct(propertyValue)
                        % Appel r�cursif
                        str{end+1}  = deserializeStruct(this, {propertyName}, []);
                        str{end+1}  = sprintf('-------------------');

                        
% %                         structFieldNameList = fieldnames(propertyName);
% %                         for k2=1:length(propertyValue)
% %                             structFieldValue = eval(sprintf('propertyValue.%s', structFieldNameList{k2}));
% %                             str{end+1} = sprintf('%s : %s', structFieldNameList{k2}, structFieldValue); %#ok<AGROW>
% %                         end
                        
                    elseif iscell(propertyValue)
                        str{end+1} = sprintf('%s : {%s}', propertyName, cell2str(propertyValue)'); %#ok<AGROW>
                        
                    elseif isobject(propertyValue)
                        if (numel(propertyValue) > 1)
                            str{end+1} = sprintf('%s : Array of %s - [%s]', propertyName, class(propertyValue), num2str(size(propertyValue))); %#ok<AGROW>
                        else
                            str{end+1} = sprintf('%s : Object Class : %s', propertyName, class(propertyValue)); %#ok<AGROW>
                        end
                        if isa(propertyValue, 'AbstractDisplay')
                            str1 = propertyValue.createObjectDisplay();
                            % Add indentation for childrens
                            str1 = [repmat('   ', size(str1,1), 1) str1]; %#ok<AGROW>
                            str{end+1}= str1; %#ok<AGROW>
                        end
                        
                    elseif isa(propertyValue, 'function_handle')
                        str{end+1} = sprintf('%s : @%s', propertyName, func2str(propertyValue)'); %#ok<AGROW>
                        
                    else
                        % unknowntype
                        str{end+1} = sprintf('%s : Unknown type', propertyName); %#ok<AGROW>
                    end
                end
            end
            
            str = cell2str(str);
        end
        
        function dispWhos(this, varName) %#ok<INUSL>
            % Display Name / Size / Class
            s  = whos('this');
            sz = s.size;
            fprintf('Name\tSize\tClass\n');
            fprintf('%s\t%s\t%s\n', varName, num2strCode(sz), s.class);
        end
        
        function str = createObjectDisplay(this)
            % Switch between scalar and non-scalar obect display
            
            N = numel(this);
            if (N==1)
                str = createScalarObjectDisplay(this);
            else
                str = createNonScalarObjectDisplay(this);
            end
        end
        
        function str = deserializeStruct(this, listPropertyName, str)
            strTab = {};
            for t=1:numel(listPropertyName)-1
                 str{end+1} = sprintf('\t');
                 strTab{t}  = char(9);
            end
            str{end+1}          = sprintf('%s STRUCT\n', listPropertyName{end});
            str{end+1}          = sprintf('%s----------------\n', [strTab{:}]);
            S       = [];
            that    = this;
            for k=1:numel(listPropertyName)
                that = that.(listPropertyName{k});
            end
            structFieldNameList = fieldnames(that);
            for k2=1:length(structFieldNameList)
                structFieldValue = that.(structFieldNameList{k2});
                if isstruct(structFieldValue)
                    
                    listPropertyName{end+1} = structFieldNameList{k2};
                    str{end+1}              = deserializeStruct(this, listPropertyName, []);
                    % str{end+1}              = deserializeStruct(this, listPropertyName, str);

                elseif ischar(structFieldValue)
                    
                    for t=1:numel(listPropertyName)
                         str{end+1} = sprintf('\t');
                    end
                    % Ajout d'une tabulation si le label d�passe 7 caract�res
                    if numel(structFieldNameList{k2}) <= 4
                        strTab      = repmat(char(9), 1, 2);
                    elseif numel(structFieldNameList{k2}) <= 7
                        strTab      = char(9);
                    else
                        strTab      = [];
                    end
                    str{end+1}  = sprintf('%s\t\t%s<->\t%s\n', structFieldNameList{k2}, strTab, structFieldValue); %#ok<AGROW>
                
                elseif isnumeric(structFieldValue)
                    
                    for t=1:numel(listPropertyName)
                         str{end+1} = sprintf('\t');
                    end
                    % Ajout d'une tabulation selon la taille du label
                    if numel(structFieldNameList{k2}) <= 4
                        strTab      = repmat(char(9), 1, 2);
                    elseif numel(structFieldNameList{k2}) <= 7
                        strTab      = char(9);
                    else
                        strTab      = [];
                    end
                    % Formattage en cas de donn�es vectorielles.
                    if isempty(structFieldValue)
                        dummy = sprintf('%s\t\t%s<->\t', structFieldNameList{k2}, strTab);
                    else
                        strFormat       = sprintf('%%s\t\t%s<->\t%s', strTab, repmat('%.2f\t', [1 numel(structFieldValue)]));
                        strFormat(end)  = [];
                        strFormat(end)  = []; % Suppression derni�re tabulation.
                        dummy           = sprintf(strFormat, structFieldNameList{k2}, structFieldValue);
                        dummy           = strtrim(dummy);
                    end
                    str{end+1}      = sprintf('%s\n', dummy);
                    
                elseif isobject(structFieldValue) % Cas de l'ellipsoide
                    
                    for t=1:numel(listPropertyName)-1
                         str{end+1} = sprintf('\t');
                         strTab{t}  = char(9);
                    end
                    if (numel(structFieldValue) > 1)
                        str{end+1} = sprintf('%s\t\t\t<->\tArray of %s - [%s]', structFieldNameList{k2}, class(structFieldValue), num2str(size(structFieldValue))); %#ok<AGROW>
                    else
                        warning off MATLAB:structOnObject;
                        % R�cup�ration de la cha�ne de description de l'objet
                        strDisp     = matlab.unittest.diagnostics.ConstraintDiagnostic.getDisplayableString(structFieldValue);
                        % str{end+1}  = sprintf('Object Class\t\t\t<->\t%s\n%s\n\n', class(structFieldValue), strDisp); %#ok<AGROW>
                        strDisp     = struct(structFieldValue);
                        strDisp     = matlab.unittest.diagnostics.ConstraintDiagnostic.getDisplayableString(strDisp);
                        str{end+1}  = sprintf('Object Class\t\t\t<->\t%s\n%s\n\n', class(structFieldValue), strDisp);
                        warning on MATLAB:structOnObject;
                    end
                    if isa(structFieldValue, 'AbstractDisplay')
                        str1 = propertyValue.createObjectDisplay();
                        % Add indentation for childrens
                        str1 = [repmat('   ', size(str1,1), 1) str1]; %#ok<AGROW>
                        str{end+1}= str1; %#ok<AGROW>
                    end
                end
            end
            str = [str{:}];
        end

    end
    
    
    methods (Access = protected, Abstract)
        %         str = displayScalarObjectCustom(this)
        % Summary of one object instance in a character array
    end
    
    methods (Sealed = true)
        function parent = editProperties(this, varargin)
            
            [varargin, parent] = getPropertyValue(varargin, 'Parent', []);
            [varargin, WindowStyle] = getPropertyValue(varargin, 'WindowStyle', 'normal');
            
            %% Check if all the arguments have been interpretated
            if checkIfVararginIsEmpty(varargin)
                return
            end
            
            com.mathworks.mwswing.MJUtilities.initJIDE;
            
            if isempty(parent)
                parent = java.util.ArrayList();
                Fig = true;
            else
                Fig = false;
            end
            
            for k1=1:length(this)
                metaClass = metaclass(this(k1));
                p1 = com.jidesoft.grid.DefaultProperty();
                p1.setName([metaClass.Name ' (' num2str(k1) ')']);
                p1.setEditable(false);
                
                if Fig
                    parent.add(p1);
                else
                    parent.addChild(p1);
                end
                
                N = numel(metaClass.PropertyList);
                for k2 = 1:N
                    if (strcmp(metaClass.PropertyList(k2).GetAccess, 'public')) % check if public
                        propertyName = metaClass.PropertyList(k2).Name;
                        propertyValue = eval(sprintf('this(%d).%s',k1, propertyName));
                        
                        if (isobject(propertyValue) || isstruct(propertyValue))
                            if isstruct(propertyValue)  % Struct Property
                                p2 = com.jidesoft.grid.DefaultProperty();
                                p2.setName(propertyName);
                                p2.setEditable(false);
                                
                                structFieldNameList = fieldnames(propertyValue);
                                for k3=1:length(structFieldNameList)
                                    structFieldValue = eval(sprintf('propertyValue.%s', structFieldNameList{k3}));                                   
                                    if isstruct(structFieldValue)
                                        % Manque mstruct
                                        p2 = editPropertiesChild(this, structFieldNameList{k3}, structFieldValue);
                                    else
                                        p2 = EditPropertiesUtils.addItemText(p2, ...
                                        'Name', structFieldNameList{k3}, ...
                                        'Value', structFieldValue);
                                    end
                                end
                                
                                p1.addChild(p2);
                                p2.setExpanded(false);
                                
                            elseif isobject(propertyValue)   % Object Property
                                if isa(propertyValue, 'AbstractDisplay')
                                    p2 = com.jidesoft.grid.DefaultProperty();
                                    p2.setName(propertyName);
                                    p2.setEditable(false);
                                    if (numel(propertyValue) > 1)
                                        strSize = strcat(class(propertyValue), ' - [',  num2str(size(propertyValue)), ']');
                                        propertyValueStr =  sprintf('Array of %s', strSize);
                                    else
                                        propertyValueStr = sprintf('%s', class(propertyValue));
                                    end
                                    p2.setValue(propertyValueStr);
                                    
                                    p2 = editProperties(propertyValue, 'Parent', p2);
                                    p1.addChild(p2);
                                    p2.setExpanded(false);
                                elseif isa(propertyValue, 'referenceEllipsoid')   % Object Property
                                    p2 = editPropertiesChild(this, propertyName, propertyValue);
                                    p1.addChild(p2);
                                    p2.setExpanded(false);
                                end
                            else
                                p1 = EditPropertiesUtils.addItemText(p1, ...
                                    'Name', propertyName, ...
                                    'Value', ['Class : ' class(propertyValue)]);
                            end

                        else
                            
                            if isstring(propertyValue)
                                propertyValueStr =  sprintf('%s',propertyValue);
                            elseif ischar(propertyValue)
                                propertyValueStr =  sprintf('%s',propertyValue);
                            elseif islogical(propertyValue)
                                propertyValueStr =  sprintf('%d',propertyValue);
                            elseif isnumeric(propertyValue)
                                if length(propertyValue) > 10
                                    strSize = strcat(class(propertyValue), ' - [',  num2str(size(propertyValue)), ']');
                                    propertyValueStr =  sprintf('Array of %s', strSize);
                                else
                                    propertyValueStr = sprintf('%s', num2str(propertyValue(:)'));
                                end
                            elseif iscell(propertyValue)
                                propertyValueStr =   sprintf('{%s}', cell2str(propertyValue)');
                            elseif isa(propertyValue, 'function_handle')
                                propertyValueStr=  sprintf('@%s' , func2str(propertyValue));
                            else
                                propertyValueStr=  sprintf('Unknown type');
                            end
                            
                            p1 = EditPropertiesUtils.addItemText(p1, ...
                                'Name', propertyName, ...
                                'Value', propertyValueStr);
                        end
                    end
                end
            end
            
            %% Create figure
            
            EditPropertiesUtils.createObjEditPropertiesFig(parent, Fig, WindowStyle);
        end
    end
  
end
