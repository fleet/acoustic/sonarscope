classdef clCRS < handle & clDatum & AbstractDisplayCRS
    % Desciption
    %   Class for Coordinate Reference System, new classe Carto, based on Mapping Toolbox
    %
    % Syntax
    %   a = clCRS(...)
    %
    % class Carto2019 properties :
    %   TODO
    %
    % Examples
    %   a               = clCRS('NlleCarto');
    %   a.ellipsoid     = '7030';
    %
    %   % NTF (Paris) / Lambert Nord France
    %   a               = clCRS('NlleCarto', 'ProjectionId', '27561')
    %
    %   % UTM 32 N
    %   a               = clCRS('UTM32N', 'ProjectionId', '32632')
    %
    %   % Carto Web Mercator
    %   a               = clCRS('WebMercator', 'ProjectionId', '3857')
    %
    %   % Lambert CC44
    %   a               = clCRS('NlleCarto', 'ProjectionId', 4275, 'DatumId', 7011);
    %
    %   [flag, structEPSG, hasProjection] = getParamsProjFromESRIFile('ProjectionName', 'NTF (Paris) / Lambert Nord France')
    % 
    % Nv test avec projection Custom : 
    %
    % datumTWD97 = clDatum('7019'); % Taiwan Datum 1997 eq. to GRS80
    % datumTWD97.ellipsoid;
    % 
    % % Parameters definition for structure as in MatLab.
    % sGeostruct.projName            = 'mercator'; % 'Mercator';
    % sGeostruct.codeEPSG            = 4326;
    % sGeostruct.strProj4            = '+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs ';
    % sGeostruct.central_meridian    = 0.0;
    % sGeostruct.latitude_of_origin  = 0.0;
    % sGeostruct.scale_factor        = 1;
    % sGeostruct.false_easting       = [];
    % sGeostruct.false_northing      = [];
    % sGeostruct.unit                = 'degrees';
    % sGeostruct.Easting             = 'EAST';
    % sGeostruct.Northing            = 'NORTH';
    % 
    % crsMerc = clCRS('Mercator4326', 'Geostruct', sGeostruct, 'ProjectionId', [], 'DatumId', 7030);
    %
    % sGeostruct.projName            = 'tranmerc'; % 'Transverse Mercator';
    % sGeostruct.codeEPSG            = 3826;
    % sGeostruct.strProj4            = [];
    % sGeostruct.central_meridian    = 121.0;
    % sGeostruct.latitude_of_origin  = 0.0;
    % sGeostruct.scale_factor        = 0.9999;
    % sGeostruct.false_easting       = 250000.0;
    % sGeostruct.false_northing      = 0.0;
    % sGeostruct.unit                = 'm';
    % sGeostruct.Easting             = 'EAST';
    % sGeostruct.Northing            = 'NORTH';
    % 
    % crsTWD97 = clCRS('TWD97-TM2-Zone 121', 'ProjectionId', 3826, 'Geostruct', sGeostruct, 'DatumId', '7019');
    % 
    % ncols         = 3276;
    % nrows         = 3954;
    % xllcorner     = 217185;
    % yllcorner     = 2723500;
    % cellSize      = 5.0;
    % x             = xllcorner:cellSize:xllcorner+(cellSize*(ncols-1));
    % y             = yllcorner:cellSize:yllcorner+(cellSize*(nrows-1));
    % [lat, lon]    = xy2latlon(crsTWD97, x, y);
    % 
	% [w, z]        = latlon2xy(crsTWD97, lat, lon);
    % Check on  : https://epsg.io/transform#s_srs=1026-datum
    %
    % nomFicWkt   = 'E:\Temp\EPSG3826WKT.html';
    % fid         = fopen(nomFicWkt, 'r');
    % C = textscan(fid, '%s');
    % fclose(fid)
    % C = [C{1}{:}];
    % sDefWkt   = WKT2Struct(C);
    % crsTWD97  = clCRS('TWD97-TM2-Zone 121', 'ProjectionId', 3826, 'DefWkt', sDefWkt, 'DatumId', '7019');
    % 
    % Exemple avec chargement de fichier : 
    %   fileName    = 'E:\Temp\EPSG3826WKT.html';
    %   crsTWD97   = import_wkt(crsTWD97, fileName);
    %
    % Authors : GLU
    % See also clEllipsoid, clProjection
    % ----------------------------------------------------------------------------
    
    properties (Access = protected)
    end
    
    properties (Access = public)
        name
        projection
    end
    
    methods (Access = public)
        function this = clCRS(name, varargin)

            [varargin, datumId]     = getPropertyValue(varargin, 'DatumId',         '7030');
            [varargin, projId]      = getPropertyValue(varargin, 'ProjectionId',    '4326');
            % Pour un for�age manuel (pas d'identification de la projection, par ex).
            [varargin, sDefWkt]     = getPropertyValue(varargin, 'DefWKT',          []);
            % Pour un for�age manuel (pas d'identification de la projection, par ex).
            [varargin, sGeoStruct]  = getPropertyValue(varargin, 'Geostruct',       []);
            
            if isnumeric(datumId)
                strDatumId = num2str(datumId);
            else
                strDatumId = datumId;
            end
            if isnumeric(projId)
                strProjId = num2str(projId);
            else
                strProjId = projId;
            end
            
            % Projection attachment
            % First search in MapList, list of projection avaible in Mapping Toolbox
            
            [flag, sEPSG, hasProjection]   = getParamsProjFromESRIFile('CodeEPSG', strProjId);
            if isempty(sEPSG)
                [flag, sEPSG, hasProjection] = getParamsProjFromESRIFile('SearchProjName', strProjId);
            end
            if hasProjection
                [flag, mstruct, deltaWGS84] = createMstruct(sEPSG.projName, sEPSG.paramsProj);
            else
                mstruct.mapprojection       = [];
                deltaWGS84                  = struct('DX',0,'DY',0,'DZ',0,'RX',0,'RY',0,'RZ',0,'M',0);
            end
            
            % Superclass Constructor - appel au Datum de r�f�rence.
            this                            = this@clDatum(strDatumId, 'ToWGS84', deltaWGS84);
            
            if isfield(sEPSG, 'code')
                % Then, search in ESRI file in <matlabroot>\toolbox\map\mapproj\projdata\proj
                this.name                   = name;
                this.projection.nameEPSG    = sEPSG.projName;
                this.projection.codeEPSG    = sEPSG.code;
                this.projection.proj4       = sEPSG.strProj4;
                this.projection.mstruct     = mstruct;
                this.projection.mstruct.geoid = this.ellipsoid;
            else
                mstruct                     = defaultm();
                mstruct.geoid               = this.ellipsoid;
                % Then, search in ESRI file in <matlabroot>\toolbox\map\mapproj\projdata\proj
                this.name                   = name;
                if ~isempty(sDefWkt)
                    if isfield(sDefWkt, 'PROJCS')
                    % Search of Matlab specific name for projection
                    [list,defproj]  = maplist;
                    strList         = arrayfun(@(x) regexprep(x.Name, ' ' ,''), list, 'Un', 0);
                    idxM4b          = find(contains(strList, sDefWkt.PROJCS.PROJECTION.Object{:}));
                    if numel(idxM4b) == 1 % Pas deux identifiants possibles
                    else
                        % Identification si UTM oui non.
                        isUTM       = ismember(sDefWkt.PROJCS.Object, 'UTM');
                        isUTMInList = contains(strList(idxM4b(:)), 'UTM');
                        if isUTM 
                            idxM4b      = idxM4b(isUTMInList);
                        else
                            idxM4b      = idxM4b(~isUTMInList);
                        end
                    end
                        
                    % La structure provient d'un fichier WKT de projection.
                    this.projection.nameEPSG    = list(idxM4b).IdString; % projName;
                    this.projection.codeEPSG    = sDefWkt.PROJCS.AUTHORITY.Object{2};
                    this.projection.proj4       = 'TODO';  % sDefWkt.strProj4;
                    % Construction de mstruct
                    mstruct.mapprojection       = list(idxM4b).IdString;
                    idx                         = contains(sDefWkt.PROJCS.PARAMETER.Object(:,1), 'false_easting');
                    mstruct.falseeasting        = str2double(sDefWkt.PROJCS.PARAMETER.Object{idx,2});
                    idx                         = contains(sDefWkt.PROJCS.PARAMETER.Object(:,1), 'false_northing');
                    mstruct.falsenorthing       = str2double(sDefWkt.PROJCS.PARAMETER.Object{idx,2});
                    idx                         = contains(sDefWkt.PROJCS.PARAMETER.Object(:,1), 'scale_factor');
                    mstruct.scalefactor         = str2double(sDefWkt.PROJCS.PARAMETER.Object{idx,2});
                    idx1                        = contains(sDefWkt.PROJCS.PARAMETER.Object(:,1), 'latitude_of_origin');
                    idx2                        = contains(sDefWkt.PROJCS.PARAMETER.Object(:,1), 'central_meridian');
                    mstruct.origin              = [ str2double(sDefWkt.PROJCS.PARAMETER.Object{idx1,2}), ...
                                                    str2double(sDefWkt.PROJCS.PARAMETER.Object{idx2,2}), ...
                                                    0];
                    this.projection.mstruct     = mstruct;
                    this.projection.mstruct.geoid = this.ellipsoid;

                    end
                elseif ~isempty(sGeoStruct)
                    this.projection.nameEPSG    = sGeoStruct.projName;
                    this.projection.codeEPSG    = sGeoStruct.codeEPSG;
                    this.projection.proj4       = sGeoStruct.strProj4;
                    % Construction de mstruct
                    mstruct                     = defaultm('mercator');
                    mstruct.geoid               = this.ellipsoid;
                    mstruct.mapprojection       = sGeoStruct.projName;
                    mstruct.falseeasting        = sGeoStruct.false_easting;
                    mstruct.falsenorthing       = sGeoStruct.false_northing;
                    mstruct.scalefactor         = sGeoStruct.scale_factor;
                    mstruct.origin              = [sGeoStruct.latitude_of_origin,sGeoStruct.central_meridian,0];
                    this.projection.mstruct     = mstruct;
                else
                    strWrnFR = sprintf('Pas d''identification de la projection du code EPSG : merci de saisir les param�tres WKT.');
                    strWrnUS = sprintf('No identification of the projection of the EPSG code : please input WKT params.');
                    dummy    = dbstack;
                    strWrn  = sprintf('%s(%d) : %s', dummy(1).name, dummy(1).line, ...
                                                  Lang(strWrnFR, strWrnUS));
                    fprintf('==SSC> - %s \n',strWrn);
                    mstruct     = [];
                    this.projection.mstruct     = mstruct;
                end

            end
            
        end
        
        function this = set(this, CRS)
            this.name       = CRS.name;
            this.projection = CRS.projection;    
            this.ellipsoid  = CRS.ellipsoid;
            this.toWGS84    = CRS.toWGS84;
        end
        
        function [x, y] = latlon2xy(this, lat, lon)
            if numel(lat) > 0 && numel(lon) > 0
                % Mise en forme pour concordance des tailles lors de l'appel � minvra,n
                [Lat, Lon]  = meshgrid(lat, lon);

                [x, y]      = mfwdtran(this.projection.mstruct, Lat, Lon);
                x           = x(:,1);
                y           = y(1,:)';
            end
        end

        function [lat, lon] = xy2latlon(this, x, y)
            if numel(x) > 0 && numel(y) > 0
                % Mise en forme pour concordance des tailles lors de l'appel � minvra,n
                [X, Y] = meshgrid(x, y);

                [lat, lon] = minvtran(this.projection.mstruct, X, Y); % projinv(this.projection.mstruct, x, y);
                lat = lat(:,1);
                lon = lon(1,:)';
            end
        end
        
        function this = import_wkt(this, fileName)
            mstruct      = [];
            if exist(fileName, 'file')
                fid         = fopen(fileName, 'r');
                C = textscan(fid, '%s');
                fclose(fid)
                C = [C{1}{:}];
                sDefWkt = WKT2Struct(C);
            else
                strFr = sprintf('Absence de fichier %s.\nMerci de v�rifier sa pr�sence.', fileName);
                strUs = sprintf('File %s absent.\nPlease, check its existence.', fileName);
                my_warndlg(Lang(strFr, strUs), 'User parameters');
                return
            end

            mstruct = defaultm('mercator');

            if isfield(sDefWkt.PROJCS, 'GEOGCS')
                if ~isempty(sDefWkt.PROJCS.GEOGCS.DATUM)
                    idxDatum    = find(arrayfun(@(x) strcmpi(x, 'EPSG'), sDefWkt.PROJCS.GEOGCS.DATUM.SPHEROID.AUTHORITY.Object));
                    strDatumId  = sDefWkt.PROJCS.GEOGCS.DATUM.SPHEROID.AUTHORITY.Object{idxDatum+1};
                    
                    if isfield(sDefWkt.PROJCS.GEOGCS.DATUM, 'TOWGS84')
                        dummy = arrayfun(@(x) str2double(x.Object), sDefWkt.PROJCS.GEOGCS.DATUM.TOWGS84, 'Un', 0);
                        dummy = dummy{1}(1:7);
                        deltaWGS84 = struct('DX',dummy(1),'DY',dummy(2),'DZ',dummy(3),'RX', ...
                                            dummy(4),'RY',dummy(5),'RZ',dummy(6),'M',dummy(7));
                    	datum = clDatum(strDatumId, 'ToWGS84', deltaWGS84);
                        mstruct.geoid   = datum.ellipsoid;
                        this.ellipsoid  = datum.ellipsoid;
                        this.toWGS84    = datum.toWGS84;
                    end
                end
            end
            
            % Search of Matlab specific name for projection
            [list,defproj]  = maplist;
            strList         = arrayfun(@(x) regexprep(x.Name, ' ' ,''), list, 'Un', 0);
            isEmpty         = cellfun(@(x) isempty(x), sDefWkt.PROJCS.PROJECTION.Object);
            sDefWkt.PROJCS.PROJECTION.Object(isEmpty) = []; 
            idxM4b          = find(strcmpi(strList, sDefWkt.PROJCS.PROJECTION.Object{:}));
            if numel(idxM4b) == 1 % Pas deux identifiants possibles
            elseif isempty(idxM4b)
                if strfind(sDefWkt.PROJCS.PROJECTION.Object{:}, 'Mercator')
                    strM4bName  = 'MercatorCylindrical';
                    idxM4b      = find(strcmpi(strList, strM4bName));
                end
            else
                % Identification si UTM oui non.
                isUTM       = ismember(sDefWkt.PROJCS.Object, 'UTM');
                isUTMInList = contains(strList(idxM4b(:)), 'UTM');
                if isUTM 
                    idxM4b      = idxM4b(isUTMInList);
                else
                    idxM4b      = idxM4b(~isUTMInList);
                end
            end

            % La structure provient d'un fichier WKT de projection.
            this.projection.nameEPSG    = list(idxM4b).IdString; % projName;
            this.projection.codeEPSG    = sDefWkt.PROJCS.AUTHORITY.Object{2};
            if isfield(sDefWkt.PROJCS, 'EXTENSION')
                if ~isempty(sDefWkt.PROJCS.EXTENSION.Object)
                    idxProj4    = find(arrayfun(@(x) strcmpi(x, 'PROJ4'), sDefWkt.PROJCS.EXTENSION.Object));
                    strProj4    = sDefWkt.PROJCS.EXTENSION.Object{idxProj4+1}; 
                    this.projection.proj4       = strProj4;
                else
                    this.projection.proj4       = 'TODO';  % sDefWkt.strProj4;
                end
            else    
                this.projection.proj4       = 'TODO';  % sDefWkt.strProj4;
            end
            % Construction de mstruct
            mstruct.mapprojection       = list(idxM4b).IdString;
            idx                         = strcmpi(sDefWkt.PROJCS.PARAMETER.Object(:,1), 'false_easting');
            mstruct.falseeasting        = str2double(sDefWkt.PROJCS.PARAMETER.Object{idx,2});
            idx                         = strcmpi(sDefWkt.PROJCS.PARAMETER.Object(:,1), 'false_northing');
            mstruct.falsenorthing       = str2double(sDefWkt.PROJCS.PARAMETER.Object{idx,2});
            idx                         = strcmpi(sDefWkt.PROJCS.PARAMETER.Object(:,1), 'scale_factor');
            mstruct.scalefactor         = str2double(sDefWkt.PROJCS.PARAMETER.Object{idx,2});
            idx1                        = strcmpi(sDefWkt.PROJCS.PARAMETER.Object(:,1), 'latitude_of_origin');
            idx2                        = strcmpi(sDefWkt.PROJCS.PARAMETER.Object(:,1), 'central_meridian');
            if any(idx1)
                latOrigin = str2double(sDefWkt.PROJCS.PARAMETER.Object{idx1,2});
            else
                latOrigin = 0;
            end
            if isempty(idx2)
                centralMeridian = str2double(sDefWkt.PROJCS.PARAMETER.Object{idx2,2});
            else
                centralMeridian = 0;
            end
            mstruct.origin              = [ latOrigin, ...
                                            centralMeridian, ...
                                            0];

            this.projection.mstruct     = mstruct;
        end % fct import_wkt
    
        % -- Fct export CRS in Xml file into cartographic properties
        function this = export_Xml(this, fileNameXml, varargin)
            
            [varargin, versionFmt] = getPropertyValue(varargin, 'FormatVersion', '1.0');

            % Object PROJCS : 
            % - name (name of CRS)
            % - datum : ellipsoid and deltaToWGS84
            % - projection: matlab Mapping toolbox fields for porjection configure.
            % 
            [rootDir, ~, ~]= fileparts(fileNameXml);
            if ~exist(rootDir, 'dir')
                mkdir(rootDir);
            end
            
            % reimport deltaToWGS84 parameters into datum
            warning off MATLAB:structOnObject;
            PROJCS.name             = this.name;
            PROJCS.history.dateOfCreation   = datestr(now(),31);
            PROJCS.history.formatVersion    = versionFmt;
            PROJCS.history.author           = getenv('username');
            PROJCS.datum.ellipsoid  = obj2struct(this.ellipsoid);
            PROJCS.datum.toWGS84    = this.toWGS84;
            PROJCS.projection       = struct(this.projection);
            PROJCS.projection.mstruct = rmfield(PROJCS.projection.mstruct, 'geoid');
            % Control charaters transforms for export
            if ~isempty(this.projection.proj4) % Ajout� par JMA le 16/01/2017 pour images g�n�r�es par Ridha
                PROJCS.projection.proj4 = regexprep(PROJCS.projection.proj4, '<', '-(');
                PROJCS.projection.proj4 = regexprep(PROJCS.projection.proj4, '>', ')-');
            end
            % Order parameters fields.
            PROJCS  = orderfields(PROJCS, {'name', 'history', 'datum', 'projection'});
           
            xml_write(fileNameXml, PROJCS);
            warning on MATLAB:structOnObject;
        end
            
        
        % -- Fct import CRS in Xml file into cartographic properties
        function this = import_Xml(this, fileNameXml)
            % Object PROJCS content in xml file: 
            % - name (name of CRS)
            % - datum : ellipsoid and deltaToWGS84
            % - projection: matlab Mapping toolbox fields for porjection configure.
            % 
            
            if ~exist(fileNameXml, 'file')
                strFr = sprintf('Absence de fichier %s.\nMerci de v�rifier sa pr�sence.', fileName);
                strUs = sprintf('File %s absent.\nPlease, check its existence.', fileName);
                my_warndlg(Lang(strFr, strUs), 'User parameters');
                return
            end
            
            try
                tree = xml_mat_read(fileNameXml);
                
                % Recreation of object Geoid for Mapping Tbx from XML file.
                objGeoid            = referenceEllipsoid(tree.datum.ellipsoid.Code);
                sCRS.name         = tree.name;
                sCRS.ellipsoid    = objGeoid;
                sCRS.toWGS84      = tree.datum.toWGS84;

                % Control charaters transforms for export
                if ~isempty(tree.projection.proj4)
                    tree.projection.proj4 = regexprep(tree.projection.proj4, '-(', '<');
                    tree.projection.proj4 = regexprep(tree.projection.proj4, ')-', '>');
                end

                % Move xml params for mstruct in structure Carto.
                sCRS.projection.nameEPSG      = tree.projection.nameEPSG;
                sCRS.projection.codeEPSG      = tree.projection.codeEPSG;
                sCRS.projection.proj4         = tree.projection.proj4;
                sCRS.projection.mstruct       = tree.projection.mstruct;
                sCRS.projection.mstruct.geoid = objGeoid;

                % Order parameters fields.
                sCRS  = orderfields(sCRS, {'name','projection', 'ellipsoid', 'toWGS84'});
                   
                % Property attribution
                set(this, sCRS);
                
            catch ME 
                [~, rootFic, ~] = fileparts(ME.stack(2).file);
                strErr = sprintf('==SSC> - %s(%d)/%s : %s', rootFic,  ME.stack(2).line, ME.stack(2).name, ...
                                               ME.message);
                fprintf('%s\n', strErr);
                errordlg(strErr, 'Carto import from xml');
                return
            end
        end
        
    end
end
    