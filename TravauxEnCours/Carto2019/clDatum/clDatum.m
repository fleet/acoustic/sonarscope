classdef clDatum < handle
    % Desciption
    %   Class Datum is refered to an Ellipsoid and shift parameters from WGS84 ellipsoid.
    %
    % Syntax
    %   a = clDatum(...)
    %
    % class Carto2019 properties :
    %   ellipsoid           - identification of ellipsoid 
    %   toWGS84 parameters  - parameters for shifting from WGS84
    %
    % Examples
    %   a           = clDatum('7019'); % GRS80
    %   a.ellipsoid;
    % 
    %   a           = clDatum([], 'ProjectionId', '4167'); % NZGD2000
    %   a           = clDatum('NZGD2000');
    %   a.ellipsoid;
    % 
    %   a           = clDatum([], 'ProjectionId', '4275'); % NTF
    %   a.ellipsoid;
    %
    % Authors : GLU
    % See also clEllipsoid, clProjection
    % ----------------------------------------------------------------------------
    
    properties (Access = public)
        ellipsoid
        toWGS84
    end
    
    methods (Access = public)
        function this = clDatum(nameOrCode, varargin)
            
            deltaWGS84                  = struct('DX',0,'DY',0,'DZ',0,'RX',0,'RY',0,'RZ',0,'M',0);
            [varargin, toWGS84]         = getPropertyValue(varargin, 'ToWGS84',         deltaWGS84);
            [varargin, projectionId]    = getPropertyValue(varargin, 'ProjectionId',    '4326');
            if ischar(nameOrCode)
                nameOrCode = str2double(nameOrCode);
            end
            if ischar(projectionId)% Utilisation la projection
                projectionId = str2double(projectionId);
            end
            
            if isempty(nameOrCode)
                if ~isnan(projectionId)
                    nameOrCode = projectionId;
                end
            end
            try
                % Find Ellipsoid in MatLab default list
                this.ellipsoid  = referenceEllipsoid(nameOrCode);
                this.toWGS84    = toWGS84;
            catch
                % Recherche du datum dans le fichier EPSG contenant projection et datum
                % pour r�cup�rer uniquement du datum.
                if isnumeric(nameOrCode)
                    nameOrCode = num2str(nameOrCode);
                end
                [flag, sEPSG, isOK]     = getParamsProjFromESRIFile('CodeEPSG', nameOrCode);
                
                % Recherche du CS dans le label.
                if ~isOK
                    [flag, sEPSG, isOK] = getParamsProjFromESRIFile('CsLabelName', nameOrCode);
                end               
                if isOK
                    [flag, mstruct, toWGS84] = createMstruct(sEPSG.projName, sEPSG.paramsProj);
                    ellipsoidId              = mstruct.geoid.Code;
                end
                this.ellipsoid  = referenceEllipsoid(ellipsoidId);
                this.toWGS84    = toWGS84;
            end
        end
    end
end
    