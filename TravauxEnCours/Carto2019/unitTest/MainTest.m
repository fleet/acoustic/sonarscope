classdef MainTest < matlab.unittest.TestCase
    % Description
    %   MainTest Unit Test
    %
    % Laucnh test :
    %   res = run(MainTest)
    %
    % Output Arguments
    %   res : Matlab.unittest.TestResult instance
    %
    % Examples
    %   res = run(MainTest);
    %   rt = table(res)
    %
    % Authors  : GLU
    % See also ClSignal DialogsTest
    % ----------------------------------------------------------------------------
    
    properties
    end
    
    methods (Test)
        function testCartoTest(this)
            res = run(CartoTest);
            isAllPassed = all([res.Passed]);
            this.verifyTrue(isAllPassed, 'CartoTest Failed');
        end
        
    end
end
