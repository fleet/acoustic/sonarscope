classdef ClCartoIOTest < matlab.unittest.TestCase
    % Description
    %   ClCartoIOFile Unit Test for ClCRS
    %
    % Laucnh test :
    %   res = run(ClCartoIOFile)
    %
    % Output Arguments
    %   res : Matlab.unittest.TestResult instance
    %
    % Examples
    %   res = run(ClCartoIOFile);
    %   rt = table(res)
    %
    % Authors  : GLU
    % See also ClCRS
    % ----------------------------------------------------------------------------
    
    properties
        CRS
    end
    
    methods(TestMethodSetup)
        function createClCRS(testCase) 
            % CRS Lambert
            testCase.CRS = clCRS('LambertUnitTest', 'ProjectionId', '4275');
        end
                
    end
    
    methods(TestMethodTeardown)
        function closeParametre(this) %#ok<MANU>
            % nothing to do here...
        end
    end
    
    methods (Test)
        
        function testExportCRSFromXml(testCase)
            
            % Create Xml file for export
            fileNameXml = fullfile(my_tempdir, ['defCRS_' testCase.CRS.name '.xml']);
            testCase.CRS.export_Xml(fileNameXml);
            valExpected     = 2;
            valObj          = exist(fileNameXml, 'file');
            testCase.verifyEqual(valExpected,valObj, sprintf('%s : Value expected : %d, but was %d', 'File Export Creation', valExpected, valObj));
        end
        
        function testImportCRSFromXml(testCase)

            % Creation of CRS exotique TAIWAN 
            sGeostruct.projName            = 'tranmerc'; % 'Transverse Mercator';
            sGeostruct.codeEPSG            = 3826;
            sGeostruct.strProj4            = [];
            sGeostruct.central_meridian    = 121.0;
            sGeostruct.latitude_of_origin  = 0.0;
            sGeostruct.scale_factor        = 0.9999;
            sGeostruct.false_easting       = 250000.0;
            sGeostruct.false_northing      = 0.0;
            sGeostruct.unit                = 'm';
            sGeostruct.Easting             = 'EAST';
            sGeostruct.Northing            = 'NORTH';
            crsTWD97        = clCRS('TWD97-TM2-Zone121', 'ProjectionId', 3826, 'Geostruct', sGeostruct, 'DatumId', '7019');
            fileNameXml     = fullfile(my_tempdir, ['defCRS' crsTWD97.name '.xml']);
            crsTWD97.export_Xml(fileNameXml);
            
            % CRS 1 replace by CRS loaded from CRS TWD97
            testCase.CRS.import_Xml(fileNameXml);
            
            % Name
            nameField       = 'name';
            listFields      = regexp(nameField, '\.', 'split');            
            valExpected     = getfield(crsTWD97, listFields{:});
            valObj          = getfield(testCase.CRS, listFields{:});
            testCase.verifyEqual(valExpected,valObj, sprintf('%s : Value expected : %d, but was %d', nameField, valExpected, valObj));
            
            % Ellipsoid code
            nameField       = 'ellipsoid.Code';
            listFields      = regexp(nameField, '\.', 'split');            
            valExpected     = getfield(crsTWD97, listFields{:});
            valObj          = getfield(testCase.CRS, listFields{:});
            testCase.verifyEqual(valExpected,valObj, sprintf('%s : Value expected : %d, but was %d', nameField, valExpected, valObj));
            
            % ToWGS84
            nameField     	= 'toWGS84';
            listFields      = regexp(nameField, '\.', 'split');            
            valExpected     = getfield(crsTWD97, listFields{:});
            valObj          = getfield(testCase.CRS, listFields{:});
            testCase.verifyEqual(valExpected,valObj, sprintf('%s : Value expected : %s, but was %s',nameField, ...
                            matlab.unittest.diagnostics.ConstraintDiagnostic.getDisplayableString(valExpected), ...
                            matlab.unittest.diagnostics.ConstraintDiagnostic.getDisplayableString(valObj)));

            % Name of EPSG projection in mstruct
            nameField       = 'projection.nameEPSG';
            listFields      = regexp(nameField, '\.', 'split');            
            valExpected     = getfield(crsTWD97, listFields{:});
            valObj          = getfield(testCase.CRS, listFields{:});
            testCase.verifyEqual(valExpected,valObj, sprintf('%s : Value expected : %s, but was %s',nameField, valExpected, valObj));

            % Code of EPSG projection in mstruct
            nameField       = 'projection.codeEPSG';
            listFields      = regexp(nameField, '\.', 'split');            
            valExpected     = getfield(crsTWD97, listFields{:});
            valObj          = getfield(testCase.CRS, listFields{:});
            testCase.verifyEqual(valExpected,valObj, sprintf('%s : Value expected : %d, but was %d',nameField, valExpected, valObj));

            % Geoid in mstruct
            nameField       = 'projection.mstruct.geoid';
            listFields      = regexp(nameField, '\.', 'split');            
            valExpected     = getfield(crsTWD97, listFields{:});
            valObj          = getfield(testCase.CRS, listFields{:});
            testCase.verifyEqual(valExpected,valObj, sprintf('%s : Value expected : %s, but was %s',nameField, ...
                            matlab.unittest.diagnostics.ConstraintDiagnostic.getDisplayableString(valExpected), ...
                            matlab.unittest.diagnostics.ConstraintDiagnostic.getDisplayableString(valObj)));
        end
        
        
        function testImportCRSFromWkt(testCase)
            
% TODO
%             nomFicWkt   = 'C:\SonarScope\SScTbx\TravauxEnCours\Carto2019\unitTest\cartoTest\ProjectionTest\EPSG3857WKT.html';
%             testCase.CRS         = clCRS('WebMercator');
%             testCase.CRS         = import_wkt(CRS, nomFicWkt);

        end

    end
end
