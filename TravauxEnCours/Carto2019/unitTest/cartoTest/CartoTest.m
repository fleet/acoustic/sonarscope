classdef CartoTest < matlab.unittest.TestCase
    % Description
    %   CartoTest Unit Test for Science Classes
    %
    % Laucnh test :
    %   res = run(CartoTest)
    %
    % Output Arguments
    %   res : Matlab.unittest.TestResult instance
    %
    % Examples
    %   res = run(CartoTest);
    %   rt = table(res)
    %
    % Authors  : GLU
    % See also ClSignal DialogsTest
    % ----------------------------------------------------------------------------
    
    properties
    end
    
    methods (Test)
        
        function testDatumTest(this)
            res             = run(ClDatumTest);
            isAllPassed     = all([res.Passed]);
            this.verifyTrue(isAllPassed, 'ClDatumTest Failed');
        end
        
        function testProjectionTest(this)
            res             = run(ClProjectionTest);
            isAllPassed     = all([res.Passed]);
            this.verifyTrue(isAllPassed, 'ClProjectionTest Failed');
        end   
        
        function testConversionTest(this)
            res             = run(ClConversionTest);
            isAllPassed     = all([res.Passed]);
            this.verifyTrue(isAllPassed, 'ClConversionTest Failed');
        end

        function testCartoIOFile(this)
            res             = run(ClCartoIOTest);
            isAllPassed     = all([res.Passed]);
            this.verifyTrue(isAllPassed, 'ClCartoIOFile Failed');
            
        end
        
    end
end
