classdef ClConversionTest < matlab.unittest.TestCase
    % Description
    %   ClXY2LatLonTest Unit Test for ClCRS
    %
    % Launch test :
    %   res = run(ClConversionTest)
    %
    % Output Arguments
    %   res : Matlab.unittest.TestResult instance
    %
    % Examples
    %   res = run(ClConversionTest);
    %   rt = table(res)
    %
    % Authors  : GLU
    % See also ClCRS
    % ----------------------------------------------------------------------------
    
    properties
        CRS
    end
    
    methods(TestMethodSetup)
        function createClProjection(testCase)
            % Nothing to od here.
        end
                
    end
    
    methods(TestMethodTeardown)
        function closeParametre(testCase) %#ok<MANU>
            % nothing to do here...
        end
    end
    
    methods (Test)
        
        function testConversionUTM32N(testCase)
            % Op�ration de contr�le op�r�es sur le site : 
            % https://epsg.io/transform
            % https://tool-online.com/conversion-coordonnees.php
            testCase.CRS    = clCRS('UTM30N', 'ProjectionId', '32630');
            
            % Coord. WGS84 (Mercator - 4326) --> UTM 32N
            lat             = 48; 
            lon             = -4; 
            % Pour check : zone            = utmzone(lat,lon);
            [x, y]          = latlon2xy(testCase.CRS, lat, lon);
            
            valExpected     = [425404.89,5316784.01];
            valObj          = [round(x*1e2)/1e2,round(y*1e2)/1e2];
            
            testCase.verifyEqual(valExpected,valObj, 'Conversion WGS84 to UTM 32 Failed');
            
            % Coord. UTM 32N --> WGS84 (Mercator - 4326)
            [lat, lon]      = xy2latlon(testCase.CRS, x, y);

            valExpected     = [48.0,-4.0];
            valObj          = [round(lat*1.e6)/1e6, round(lon*1.e6)/1e6];
            
            testCase.verifyEqual(valExpected,valObj, 'Conversion WGS84 to UTM 32 Failed');
            
        end
        
        function testConversionTWD97(testCase)
            global IfrTbx
            
% %             nomFicWkt       = fullfile(IfrTbx, 'TravauxEnCours\Carto2019\unitTest\cartoTest\ProjectionTest\EPSG3826WKT.html');
% %             testCase.CRS    = clCRS('TWD97-TM2-Zone 121');
% %             testCase.CRS    = import_wkt(testCase.CRS, nomFicWkt);
            
            sGeostruct.projName            = 'tranmerc'; % 'Transverse Mercator';
            sGeostruct.codeEPSG            = 3826;
            sGeostruct.strProj4            = [];
            sGeostruct.central_meridian    = 121.0;
            sGeostruct.latitude_of_origin  = 0.0;
            sGeostruct.scale_factor        = 0.9999;
            sGeostruct.false_easting       = 250000.0;
            sGeostruct.false_northing      = 0.0;
            sGeostruct.unit                = 'm';
            sGeostruct.Easting             = 'EAST';
            sGeostruct.Northing            = 'NORTH';
            
            testCase.CRS = clCRS('TWD97-TM2-Zone 121', 'ProjectionId', 3826, 'Geostruct', sGeostruct, 'DatumId', '7019');
            
            x               = 217185;
            y               = 2723500;
            [lat, lon]      = xy2latlon(testCase.CRS, x, y);
            valExpected     = [24.617943,120.675902];
            valObj          = [round(lat*1e6)/1e6,round(lon*1e6)/1e6];
            
            testCase.verifyEqual(valExpected,valObj, 'Conversion TWD97 to WGS84 Failed');
            
            [x2, y2]        = latlon2xy(testCase.CRS, lat, lon);
            valExpected     = [x,y];
            valObj          = [round(x2*1e2)/1e2,round(y2*1e2)/1e2];
            
            testCase.verifyEqual(valExpected,valObj, 'Conversion WGS84 to TWD97 Failed');
           
        end
    end
end
