classdef ClDatumTest < matlab.unittest.TestCase
    % Description
    %   ClDatumTest Unit Test for ClCRS
    %
    % Laucnh test :
    %   res = run(ClDatumTest)
    %
    % Output Arguments
    %   res : Matlab.unittest.TestResult instance
    %
    % Examples
    %   res = run(ClDatumTest);
    %   rt = table(res)
    %
    % Authors  : GLU
    % See also ClCRS
    % ----------------------------------------------------------------------------
    
    properties
        Datum
    end
    
    methods(TestMethodSetup)
        function createDatum(this)
            this.Datum = clDatum(7011);
        end
        
        function setEllipdoid(this)
            this.Datum = 7030;
        end
    end
    
    methods(TestMethodTeardown)
        function closeParametre(this) %#ok<MANU>
            % nothing to do here...
        end
    end
    
    methods (Test)
        
        function testGetters(this)
% % %             % EPSG 4275 est la r�f�rence d'une projection.
% % %             % Pointe vers l'Datume 7011.
% % %             datum = clDatum([], 'ProjectionId', 4275);            
            % Test des getters de ClParametre
            
            % Name
            nameField       = 'Datum';
            valExpected     = 7030;
            valObj          = this.(nameField);
            this.verifyEqual(valExpected,valObj, sprintf('%s : Value expected : %d, but was %d', nameField, valExpected, valObj));
            
            % EPSG 4275 est la r�f�rence d'une projection.
            % Pointe vers l'Datume 7011.
            datum = clDatum([], 'ProjectionId', 4275);            
            % Test des getters de ClParametre
            
            % Name
            a               = 'datum.ellipsoid.Code';
            valExpected     = 7011;
            valObj          = eval(a);
            this.verifyEqual(valExpected,valObj, sprintf('%s : Value expected : %d, but was %d',a, valExpected, valObj));
        end
        
        function testSetters(testCase)
            % Test des setters de ClDatum
            
            % Value
            valueExpected   = 7011;
            testCase.Datum  = valueExpected;
            valueObj        = testCase.Datum;
            testCase.verifyEqual(valueExpected,valueObj, sprintf('value expected : %f, but was %f',valueExpected, valueObj));
            
        end
    end
end
