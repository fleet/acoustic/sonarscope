classdef ClProjectionTest < matlab.unittest.TestCase
    % Description
    %   ClProjectionTest Unit Test for ClCRS
    %
    % Laucnh test :
    %   res = run(ClProjectionTest)
    %
    % Output Arguments
    %   res : Matlab.unittest.TestResult instance
    %
    % Examples
    %   res = run(ClProjectionTest);
    %   rt = table(res)
    %
    % Authors  : GLU
    % See also ClCRS
    % ----------------------------------------------------------------------------
    
    properties
        CRS
    end
    
    methods(TestMethodSetup)
        function createClProjection(testCase)
            % Nothing to od here.
        end
                
    end
    
    methods(TestMethodTeardown)
        function closeParametre(testCase) %#ok<MANU>
            % nothing to do here...
        end
    end
    
    methods (Test)
        
        function testProjectionMercator(testCase)
            % Carto Mercator - 4326
            sGeostruct.projName            = 'mercator'; % 'Mercator';
            sGeostruct.codeEPSG            = '4326';
            sGeostruct.strProj4            = '+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs';
            sGeostruct.central_meridian    = 0.0;
            sGeostruct.latitude_of_origin  = 0.0;
            sGeostruct.scale_factor        = 1;
            sGeostruct.false_easting       = [];
            sGeostruct.false_northing      = [];
            sGeostruct.unit                = 'degrees';
            sGeostruct.Easting             = 'EAST';
            sGeostruct.Northing            = 'NORTH';
            testCase.CRS = clCRS('MercatorUnitTest', 'Geostruct', sGeostruct, 'ProjectionId', []);
            
            % Name EPSG
            nameField       = 'projection.nameEPSG';
            listFields      = regexp(nameField, '\.', 'split');            
            valExpected     = 'mercator';
            valObj          = getfield(testCase.CRS, listFields{:});
            testCase.verifyEqual(valExpected,valObj, sprintf('%s : Value expected : %s, but was %s', nameField, ...
                                                    valExpected, valObj));
            % Code EPSG
            nameField       = 'projection.codeEPSG';
            listFields      = regexp(nameField, '\.', 'split');            
            valExpected     = '4326';
            valObj          = getfield(testCase.CRS, listFields{:});
            testCase.verifyEqual(valExpected,valObj, sprintf('%s : Value expected : %d, but was %d', nameField, ...
                                                    valExpected, valObj));
            
            % Proj4.
            strProj4        = '+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs';
            nameField       = 'projection.proj4';
            listFields      = regexp(nameField, '\.', 'split');            
            valExpected     = strProj4;
            valObj          = getfield(testCase.CRS, listFields{:});
            testCase.verifyEqual(valExpected,valObj, sprintf('%s : Value expected : %s, but was %s', nameField, ...
                                                    valExpected, valObj));
                                                            
            % Vérification des champs significatifs de la structure
            nameField       = 'projection.mstruct.mapprojection';
            listFields      = regexp(nameField, '\.', 'split');            
            valExpected     = 'mercator';
            valObj          = getfield(testCase.CRS, listFields{:});
            testCase.verifyEqual(valExpected,valObj, sprintf('%s : Value expected : %s, but was %s', nameField, ...
                                                    valExpected, valObj));
                                                
            nameField       = 'projection.mstruct.angleunits';
            listFields      = regexp(nameField, '\.', 'split');            
            valExpected     = 'degrees';
            valObj          = getfield(testCase.CRS, listFields{:});
            testCase.verifyEqual(valExpected,valObj, sprintf('%s : Value expected : %s, but was %s', nameField, ...
                                                    valExpected, valObj));
            
            nameField       = 'projection.mstruct.geoid';
            listFields      = regexp(nameField, '\.', 'split');            
            valExpected     = referenceEllipsoid(7030);
            valObj          = getfield(testCase.CRS, listFields{:});
            testCase.verifyEqual(valExpected,valObj, sprintf('%s : Value expected : %s, but was %s', nameField, ...
                                                     matlab.unittest.diagnostics.ConstraintDiagnostic.getDisplayableString(valExpected),...
                                                     matlab.unittest.diagnostics.ConstraintDiagnostic.getDisplayableString(valObj)));

            nameField       = 'projection.mstruct.scalefactor';
            listFields      = regexp(nameField, '\.', 'split');            
            valExpected     = 1;
            valObj          = getfield(testCase.CRS, listFields{:});
            testCase.verifyEqual(valExpected,valObj, sprintf('%s : Value expected : %s, but was %s', nameField, ...
                                                     valExpected, valObj));
                                                 
            nameField       = 'projection.mstruct.origin';
            listFields      = regexp(nameField, '\.', 'split');            
            valExpected     = [0 0 0];
            valObj          = getfield(testCase.CRS, listFields{:});
            testCase.verifyEqual(valExpected,valObj, sprintf('%s : Value expected : %s, but was %s', nameField, ...
                                                     matlab.unittest.diagnostics.ConstraintDiagnostic.getDisplayableString(valExpected),...
                                                     matlab.unittest.diagnostics.ConstraintDiagnostic.getDisplayableString(valObj)));
                                                 
            nameField       = 'projection.mstruct.falsenorthing';
            listFields      = regexp(nameField, '\.', 'split');            
            valExpected     = [];
            valObj          = getfield(testCase.CRS, listFields{:});
            testCase.verifyEqual(valExpected,valObj, sprintf('%s : Value expected : %s, but was %s', nameField, ...
                                                     valExpected, valObj));

            nameField       = 'projection.mstruct.falseeasting';
            listFields      = regexp(nameField, '\.', 'split');            
            valExpected     = [];
            valObj          = getfield(testCase.CRS, listFields{:});
            testCase.verifyEqual(valExpected,valObj, sprintf('%s : Value expected : %s, but was %s', nameField, ...
                                                     valExpected, valObj));
        end
        
        function testProjectionLambert(testCase)
            % Carto Lambert
            testCase.CRS = clCRS('LambertUnitTest', 'ProjectionId', 4275, 'DatumId', 7011);
            
            % Name EPSG
            nameField       = 'projection.nameEPSG';
            listFields      = regexp(nameField, '\.', 'split');            
            valExpected     = 'NTF';
            valObj          = getfield(testCase.CRS, listFields{:});
            testCase.verifyEqual(valExpected,valObj, sprintf('%s : Value expected : %s, but was %s', nameField, ...
                                                    valExpected, valObj));
            % Code EPSG
            nameField       = 'projection.codeEPSG';
            listFields      = regexp(nameField, '\.', 'split');            
            valExpected     = '4275';
            valObj          = getfield(testCase.CRS, listFields{:});
            testCase.verifyEqual(valExpected,valObj, sprintf('%s : Value expected : %d, but was %d', nameField, ...
                                                    valExpected, valObj));
            
            % Proj4.
            strProj4 = '<4275> +proj=longlat +a=6378249.2 +b=6356515 +towgs84=-168,-60,320,0,0,0,0 +no_defs  no_defs <>';
            nameField       = 'projection.proj4';
            listFields      = regexp(nameField, '\.', 'split');            
            valExpected     = strProj4;
            valObj          = getfield(testCase.CRS, listFields{:});
            testCase.verifyEqual(valExpected,valObj, sprintf('%s : Value expected : %s, but was %s', nameField, ...
                                                    valExpected, valObj));
            
                                                
            % Vérification des champs significatifs de la structure
            nameField       = 'projection.mstruct.mapprojection';
            listFields      = regexp(nameField, '\.', 'split');            
            valExpected     = 'mercator';
            valObj          = getfield(testCase.CRS, listFields{:});
            testCase.verifyEqual(valExpected,valObj, sprintf('%s : Value expected : %s, but was %s', nameField, ...
                                                    valExpected, valObj));
                                                
            nameField       = 'projection.mstruct.angleunits';
            listFields      = regexp(nameField, '\.', 'split');            
            valExpected     = 'degrees';
            valObj          = getfield(testCase.CRS, listFields{:});
            testCase.verifyEqual(valExpected,valObj, sprintf('%s : Value expected : %s, but was %s', nameField, ...
                                                    valExpected, valObj));
            
            nameField       = 'projection.mstruct.geoid';
            listFields      = regexp(nameField, '\.', 'split');            
            valExpected     = referenceEllipsoid(7011);
            valObj          = getfield(testCase.CRS, listFields{:});
            testCase.verifyEqual(valExpected,valObj, sprintf('%s : Value expected : %s, but was %s', nameField, ...
                                                     matlab.unittest.diagnostics.ConstraintDiagnostic.getDisplayableString(valExpected),...
                                                     matlab.unittest.diagnostics.ConstraintDiagnostic.getDisplayableString(valObj)));

            nameField       = 'projection.mstruct.scalefactor';
            listFields      = regexp(nameField, '\.', 'split');            
            valExpected     = 1;
            valObj          = getfield(testCase.CRS, listFields{:});
            testCase.verifyEqual(valExpected,valObj, sprintf('%s : Value expected : %s, but was %s', nameField, ...
                                                     valExpected, valObj));
                                                 
            nameField       = 'projection.mstruct.origin';
            listFields      = regexp(nameField, '\.', 'split');            
            valExpected     = [0 0 0];
            valObj          = getfield(testCase.CRS, listFields{:});
            testCase.verifyEqual(valExpected,valObj, sprintf('%s : Value expected : %s, but was %s', nameField, ...
                                                     matlab.unittest.diagnostics.ConstraintDiagnostic.getDisplayableString(valExpected),...
                                                     matlab.unittest.diagnostics.ConstraintDiagnostic.getDisplayableString(valObj)));
                                                 
            nameField       = 'projection.mstruct.falsenorthing';
            listFields      = regexp(nameField, '\.', 'split');            
            valExpected     = 0;
            valObj          = getfield(testCase.CRS, listFields{:});
            testCase.verifyEqual(valExpected,valObj, sprintf('%s : Value expected : %s, but was %s', nameField, ...
                                                     valExpected, valObj));

            nameField       = 'projection.mstruct.falseeasting';
            listFields      = regexp(nameField, '\.', 'split');            
            valExpected     = 0;
            valObj          = getfield(testCase.CRS, listFields{:});
            testCase.verifyEqual(valExpected,valObj, sprintf('%s : Value expected : %s, but was %s', nameField, ...
                                                     valExpected, valObj));
        end
        
        function testProjectionTDW97(testCase)
            
            % Carto exotique 'TWD97-TM2-Zone121'
            % Dans ce mode de remplissage, la carto doit être vide.
            testCase.CRS = clCRS('TWD97-TM2-Zone 121', 'ProjectionId', 3826);
            testCase.verifyEmpty(testCase.CRS.projection.mstruct, 'Array is not empty.');
            
            % Chargement du fichier test de projection 3826
            pppp        = dbstack;
            pppp        = which(pppp(1).file);
            [rootDir, rootFile, ext] = fileparts(pppp);
            nomFicWkt   = fullfile(rootDir, '..', '..', 'rscTests', 'EPSG3826WKT.html');
            fid         = fopen(nomFicWkt, 'r');
            C           = textscan(fid, '%s');
            fclose(fid);
            C = [C{1}{:}];
            sDefWkt   = WKT2Struct(C);
            testCase.CRS  = clCRS('TWD97-TM2-Zone 121', 'ProjectionId', 3826, 'DefWkt', sDefWkt, 'DatumId', '7019');

            
            % Name EPSG
            nameField       = 'projection.nameEPSG';
            listFields      = regexp(nameField, '\.', 'split');            
            valExpected     = 'tranmerc';
            valObj          = getfield(testCase.CRS, listFields{:});
            testCase.verifyEqual(valExpected,valObj, sprintf('%s : Value expected : %s, but was %s', nameField, ...
                                                    valExpected, valObj));
            % Code EPSG
            nameField       = 'projection.codeEPSG';
            listFields      = regexp(nameField, '\.', 'split');            
            valExpected     = '3826';
            valObj          = getfield(testCase.CRS, listFields{:});
            testCase.verifyEqual(valExpected,valObj, sprintf('%s : Value expected : %d, but was %d', nameField, ...
                                                    valExpected, valObj));
            
            % Proj4.
            strProj4 = 'TODO';
            nameField       = 'projection.proj4';
            listFields      = regexp(nameField, '\.', 'split');            
            valExpected     = strProj4;
            valObj          = getfield(testCase.CRS, listFields{:});
            testCase.verifyEqual(valExpected,valObj, sprintf('%s : Value expected : %s, but was %s', nameField, ...
                                                    valExpected, valObj));
            
            % Vérification des champs significatifs de la structure
            nameField       = 'projection.mstruct.mapprojection';
            listFields      = regexp(nameField, '\.', 'split');            
            valExpected     = 'tranmerc';
            valObj          = getfield(testCase.CRS, listFields{:});
            testCase.verifyEqual(valExpected,valObj, sprintf('%s : Value expected : %s, but was %s', nameField, ...
                                                    valExpected, valObj));
                                                
            nameField       = 'projection.mstruct.angleunits';
            listFields      = regexp(nameField, '\.', 'split');            
            valExpected     = 'degrees';
            valObj          = getfield(testCase.CRS, listFields{:});
            testCase.verifyEqual(valExpected,valObj, sprintf('%s : Value expected : %s, but was %s', nameField, ...
                                                    valExpected, valObj));
            
            nameField       = 'projection.mstruct.geoid';
            listFields      = regexp(nameField, '\.', 'split');            
            valExpected     = referenceEllipsoid(7019);
            valObj          = getfield(testCase.CRS, listFields{:});
            testCase.verifyEqual(valExpected,valObj, sprintf('%s : Value expected : %s, but was %s', nameField, ...
                                                     matlab.unittest.diagnostics.ConstraintDiagnostic.getDisplayableString(valExpected),...
                                                     matlab.unittest.diagnostics.ConstraintDiagnostic.getDisplayableString(valObj)));

            nameField       = 'projection.mstruct.scalefactor';
            listFields      = regexp(nameField, '\.', 'split');            
            valExpected     = 0.99990;
            valObj          = getfield(testCase.CRS, listFields{:});
            testCase.verifyEqual(valExpected,valObj, sprintf('%s : Value expected : %s, but was %s', nameField, ...
                                                     valExpected, valObj));
                                                 
            nameField       = 'projection.mstruct.origin';
            listFields      = regexp(nameField, '\.', 'split');            
            valExpected     = [0 121 0];
            valObj          = getfield(testCase.CRS, listFields{:});
            testCase.verifyEqual(valExpected,valObj, sprintf('%s : Value expected : %s, but was %s', nameField, ...
                                                     matlab.unittest.diagnostics.ConstraintDiagnostic.getDisplayableString(valExpected),...
                                                     matlab.unittest.diagnostics.ConstraintDiagnostic.getDisplayableString(valObj)));
                                                 
            nameField       = 'projection.mstruct.falsenorthing';
            listFields      = regexp(nameField, '\.', 'split');            
            valExpected     = 0;
            valObj          = getfield(testCase.CRS, listFields{:});
            testCase.verifyEqual(valExpected,valObj, sprintf('%s : Value expected : %s, but was %s', nameField, ...
                                                     valExpected, valObj));

            nameField       = 'projection.mstruct.falseeasting';
            listFields      = regexp(nameField, '\.', 'split');            
            valExpected     = 250000;
            valObj          = getfield(testCase.CRS, listFields{:});
            testCase.verifyEqual(valExpected,valObj, sprintf('%s : Value expected : %s, but was %s', nameField, ...
                                                     valExpected, valObj));
        end
    end
end
