classdef cl_plot < handle
    properties (Access = 'public')
        hdlP
        hdlA
        hdlLMForXLim
        hdlLMForYLim
    end

    events
        changeProperty
    end
    
    methods
        function this   = cl_plot(varargin)
            this.hdlP    = plot(varargin{:});
            this.hdlA    = get(this.hdlP, 'Parent');
            hhAxe = handle(this.hdlA);
            hPropForX    = findprop(hhAxe,'XLim');
            hPropForY    = findprop(hhAxe,'YLim');
            this.hdlLMForXLim = handle.listener(hhAxe,hPropForX,'PropertyPostSet',...
                @(src,evnt)clPlotListenerXLim_cb(src,evnt));            
            this.hdlLMForYLim = handle.listener(hhAxe,hPropForY,'PropertyPostSet',...
                @(src,evnt)clPlotListenerYLim_cb(src,evnt));            
        end            
    end
end