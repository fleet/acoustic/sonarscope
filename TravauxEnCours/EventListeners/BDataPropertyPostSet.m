function BDataPropertyPostSet(hProp,eventData)   
   
   hLine        = eventData.AffectedObject;
   hdlAxe       = eventData.AffectedObject.Parent;
   hdlFig       = get(hdlAxe, 'Parent');
   hdlAxes      = getappdata(hdlAxe, 'tabHandlesAxes');
   
   if ~ishghandle(hdlAxe)
       my_warndlg('La fen�tre de trac� n''existe plus !', 1);
       return
   end
   
      %% Initialisation
   % TODO : remplacer gco par la r�cup�ration du handle point� par le
   % EventData.
   if isempty(handle(gco))
       % Le trac� est en cours d'initialisation
       return
   else
        % Recherche du Plot1.
        % TODO : tester avec tag Brushing si lien non-suffisant
        hPlot1          = findobj(hdlFig, 'tag', 'plot1'); 
        % Traitement des donn�es filtr�es.
        bData           = get(hPlot1, 'subBrushData');
        subBrushData    = find(bData(1,:) == 1);
        tmpBData        = getappdata(hdlFig, 'subBrushData');
        subBrushData    = [tmpBData subBrushData];
        setappdata(hdlFig, 'BrushData', subBrushData);
        % Identification du bb correspondant au plot1.
        
        %{
        bb      = findall(hdlFig,'Tag','Brushing');
        % Retrouve les objets qui sont brush�s (= points)
        % hdlObj = findall(gcf,'Tag','Brushing');
        Xbb = get(hLine,'XData');
        sub = (~isnan(Xbb));
        sub = find(sub == 1);
        %}
        
   end

end  % myCallbackFunction