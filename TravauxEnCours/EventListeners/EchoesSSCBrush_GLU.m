function flag = EchoesSSCBrush_GLU(varargin)

    flag = 0;
    NomFicEchoes = getPropertyValue(varargin, 'nomFicEchoes', ...
        'K:\Data\IFREMER\Data_SSC\SSC-3DViewer-Data\3DViewer-GLOBE\WC\Marmesonet\Echoes\0306_Echoes-Clean.xml');
    X   = getPropertyValue(varargin, 'X', []);
    Y   = getPropertyValue(varargin, 'Y', []);
    Z   = getPropertyValue(varargin, 'Z', []);
    Data   = getPropertyValue(varargin, 'Data', []);
    XLabel = getPropertyValue(varargin, 'XLabel', 'X');
    YLabel = getPropertyValue(varargin, 'YLabel', 'Y');
    ZLabel = getPropertyValue(varargin, 'ZLabel', 'Z');
    
    myFig = FigUtils.createSScFigure;
    %% Lecture du fichier Echoes
    if isempty(Data)
        [flag, Data] = read_XMLBin(NomFicEchoes, 'BinInData', 1); 
        X = Data.iPing;
        Y = Data.AcrossDistance;
        Z = Data.Z;
    end
    
    ha(1) = subplot(2,2,1, 'Tag', 'axe1');
    hp(1) = plot(X, Z, 'o', 'XDataSource', 'X','YDataSource','Z', 'Tag', 'plot1');
    % hp(1) = plot(X, Z, 'o');
    grid on; xlabel(XLabel); ylabel(ZLabel); title('Z=f(x)');

    ha(2) = subplot(2,2,2, 'Tag', 'axe2');
    hp(2) = plot(Y, Z,'o', 'XDataSource', 'Y','YDataSource','Z', 'Tag', 'plot2');
    % hp(2) = plot(Y, Z, 'o');
    grid on; xlabel(YLabel); ylabel(ZLabel); title('Z=f(y)');
    linkdata

    ha(3) = subplot(2,2,3, 'Tag', 'axe3');
    hp(3) = plot(X, Y,'o', 'XDataSource', 'X','YDataSource','Y', 'Tag', 'plot3');
    % hp(3) = plot(X, Y, 'o');
    grid on; xlabel(XLabel); ylabel(YLabel); title('Y=f(X)');

    ha(4) = subplot(2,2,4, 'Tag', 'axe4');
    hp(4) = plot3(X, Y, Z, 'o', 'XDataSource', 'X','YDataSource','Y','ZDataSource','Z', 'Tag', 'plot4');
    hp(4) = plot3(X, Y, Z, 'o');
    grid on; xlabel(XLabel); ylabel(YLabel); zlabel(ZLabel); title('Z=f(X,Y)');

    % Peut-�tre remplac� par findobj(gcf, 'Type', 'axes') dans la fonction
    % de callback.
    setappdata(myFig, 'tabHandlesAxes', ha); 
    setappdata(myFig, 'tabHandlesPlots', hp); 
    set(myFig, 'DeleteFcn', {@DeleteFcnEchoesSScBrush, NomFicEchoes, Data})
    
    linkdata on

    brush on
    
    % Affectation des propri�t�s et des gestions d'�v�nements pour chaque
    % trac�.
    for k=1:numel(ha)
        hhAxes(k)               = handle(ha(k));    
        hPropForXLim(k)        = findprop(hhAxes(k),'XLim');        % a schema.prop object
%          hPropForYLim(k)        = findprop(hhAxes(k),'YLim');     % a schema.prop object
        hListenerForXLim(k)    = handle.listener(hhAxes(k), hPropForXLim(k), 'PropertyPostSet', @(src,event)XLimPropertyPostSet(src, event));
%         hListenerForYLim(k)    = handle.listener(hhAxes(k), hPropForYLim(k), 'PropertyPostSet', @(src,event)YLimPropertyPostSet(src, event));
        setappdata(hhAxes(k), 'XLimListener', hListenerForXLim(k));
%         setappdata(hhAxes(k), 'YLimListener', hListenerForYLim(k));
    end
    % Cr�ation du Listener BrushData que pour le PLOT1 (on supppose la synchronisation effective).
    addlistener(hp(1),'YData','PostSet',@(src,evnt)BDataPropertyPostSet(src, evnt));
    
    waitfor(myFig);
    
    flag = 1;

function DeleteFcnEchoesSScBrush(hFig, varargin)

nomFicEchoes    = varargin{2};
Data            = varargin{3};
% hp = findobj(hFig, 'Type', 'Line'); % TODO : indexation des Handle.
subBrushData    = getappdata(hFig, 'subBrushData');
subBrushData    = unique(subBrushData);
% On filtre tous les points qui n'ont pas �t� impact�s.
idxPts          = 1:numel(Data.Latitude);
sub             = setdiff(idxPts,subBrushData);

% Enregistrement des points.
[nomDir, nomFic] = fileparts(nomFicEchoes);
nomFicSSc = fullfile(nomDir, [nomFic '-Clean.xml']);
Points.Latitude       = Data.Latitude(sub);
Points.Longitude      = Data.Longitude(sub);
Points.Z              = Data.Z(sub);
Points.iPing          = Data.iPing(sub);
Points.Angle          = Data.Angle(sub);
Points.Energie        = Data.Energie(sub);
Points.Date           = Data.Date(sub);
Points.Hour           = Data.Hour(sub);
Points.EnergieTotale  = Data.EnergieTotale(sub);
Points.XCoor          = Data.XCoor(sub);
Points.YCoor          = Data.YCoor(sub);
Points.RangeInSamples = Data.RangeInSamples(sub);
Points.AcrossDistance = Data.AcrossDistance(sub);
Points.Celerite       = Data.Celerite(sub);

[rep, flag] = my_questdlg('Voulez-vous sauvegarder ?');
if flag && rep == 1
    flag = WC_SaveEchoes(nomFicSSc, Points); %#ok<NASGU>
end
%% Reprise de l'activit� hors IHM (provoque la fin du waifor).
uiresume(hFig);
