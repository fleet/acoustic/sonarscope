classdef MyEventData < event.EventData
  properties (Access = public)
    % Event data
    Data
  end

  methods
    function this = MyEventData(data)
      % Constructor
      this.Data = data;
    end
  end
end