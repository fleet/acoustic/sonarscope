classdef PushButton < handle
   properties (SetObservable)
      % Enable listener to observe PreSet event
      ResultNumber = 1;
   end
   properties
      AxHandle
   end
   methods
      function buttonObj = PushButton
         myFig = figure;
         buttonObj.AxHandle = axes('Parent',myFig);
         uicontrol('Parent',myFig,...
            'Style','pushbutton',...
            'String','Plot Data',...
            'Callback',@(src,evnt)pressed(buttonObj));
         addlistener(buttonObj,'ResultNumber','PreSet',...
            @PushButton.updateTitle);
      end
   end
   methods
      function pressed(obj)
         % Push button callback
         % Perform plotting operations
         % Specify target axes for output of plot function
         scatter(obj.AxHandle,randn(1,20),randn(1,20),'p')
         % Update observed property
         % which triggers PreSet event
         obj.ResultNumber = obj.ResultNumber + 1;
      end
   end
   methods (Static)
      function updateTitle(~,eventData)
         % Listener callback - updates axes title
         h = eventData.AffectedObject;
         % Get handle to title text object
         % And set String property
         set(get(h.AxHandle,'Title'),'String',['Result Number: ',...
            num2str(h.ResultNumber)])
      end
   end
end