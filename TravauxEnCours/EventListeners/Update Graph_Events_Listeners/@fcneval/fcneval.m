classdef fcneval < handle                 % EvalFcn is a subclass of handle
   properties
      FofXY
   end % properties
   
   properties (SetObservable = true)
      Lm = [-2*pi 2*pi];
   end % properties SetObservable = true
   
   properties (Dependent = true)        % Do not store the Data property
      Data
   end % properties Dependent = true
   
   events
      UpdateGraph % only if FofXY changes
   end
   
   methods
      function obj = fcneval(fcn_handle,limits)  % Constructor returns object
         if nargin > 0
            obj.FofXY = fcn_handle;                 % Assign property values
            obj.Lm = limits;
            
         end
      end % fcneval
      
      function set.FofXY(obj,func)
         me = fcneval.isSuitable(func);
         if ~isempty(me)
            throw(me)
         end
         obj.FofXY = func;
         notify(obj,'UpdateGraph');       % Fire UpdateGraph event
      end % set.FofXY
      
      function set.Lm(obj,lim)     % Lm property set function
         if  ~(lim(1) < lim(2))
            error('Limits must be monotonically increasing')
         else
            obj.Lm = lim;
         end
      end % set.Lm
      
      function data = get.Data(obj)        % get function calculates Data
         [x,y] = fcneval.grid(obj.Lm);     % Use class name to call static method
         matrix = obj.FofXY(x,y);
         data.X = x;
         data.Y = y;
         data.Matrix = matrix;            % Return value of property
         
      end % get.Data
      
   end % methods
   
   methods (Static = true)            % Define static method
      function [x,y] = grid(lim)
         inc = (lim(2)-lim(1))/20;
         [x,y] = meshgrid(lim(1):inc:lim(2));
      end % grid
      
      function isOk = isSuitable(funcH)
         v = [1 1;1 1];
         try
            funcH(v,v);
         catch  %#ok<CTCH>
            me = MException('DocExample:fcneval',...
               ['The function ',func2str(funcH),' Is not a suitable F(x,y)']);
            isOk = me;
            return
         end
         if isscalar(funcH(v,v));
            me = MException('DocExample:fcneval',...
               ['The function ',func2str(funcH),' Returns a scalar when evaluated']);
            isOk = me;
            return
         end
         isOk = [];
      end
      
   end % methods Static = true
   
end % fcneval classdef