
function createViews(fcnevalobj)
   p = pi; deg = 180/p;
   %t = char(fcnevalobj.FofXY);
   hfig = figure('Visible','off',...
      'Toolbar','none');
  
    for k=4:-1:1
      fcnviewobj(k) = fcnview(fcnevalobj);
      axh = subplot(2,2,k);
      fcnviewobj(k).HAxes = axh;
      hcm(k) = uicontextmenu;
      set(axh,'Parent',hfig,...
         'FontSize',8,...
         'UIContextMenu',hcm(k))
      fcnviewobj(k).HEnableCm = uimenu(hcm(k),...
         'Label','Listen',...
         'Checked','on',...
         'Callback',@(src,evnt)enableLisn(fcnviewobj(k),src,evnt));
      fcnviewobj(k).HDisableCm = uimenu(hcm(k),...
         'Label','Don''t Listen',...
         'Checked','off',...
         'Callback',@(src,evnt)disableLisn(fcnviewobj(k),src,evnt));
      az = p/k*deg;
      view(axh,az,30)
      title(axh,['View: ',num2str(az),' 30'])
      fcnviewobj(k).lims;
      surfLight(fcnviewobj(k),axh)
   end
   set(hfig,'Visible','on')

   
      
end % createViews
function surfLight(obj,axh) % Graph function as surface
    obj.HSurface = surface(obj.FcnObject.Data.X,...
        obj.FcnObject.Data.Y,...
        obj.FcnObject.Data.Matrix,...
        'FaceColor',[.8 .8 0],'EdgeColor',[.3 .3 .2],...
        'FaceLighting','phong',...
        'FaceAlpha',.3,...
        'Parent',axh);
	set_HitTest_Off(obj.HSurface)
    lims(obj)
    camlight left; material shiny; grid off
    colormap copper
end % surfLight method

function enableLisn(obj,~,~)
   obj.HLUpdateGraph.Enabled = true;
   obj.HLLm.Enabled = true;
   set(obj.HEnableCm,'Checked','on')
   set(obj.HDisableCm,'Checked','off')
end % enableLisn

function disableLisn(obj,~,~)
   obj.HLUpdateGraph.Enabled = false;
   obj.HLLm.Enabled = false;
   set(obj.HEnableCm,'Checked','off')
   set(obj.HDisableCm,'Checked','on')
end % disableLisn

      
         
