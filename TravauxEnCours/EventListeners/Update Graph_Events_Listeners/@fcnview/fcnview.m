classdef fcnview < handle
   properties
      FcnObject     % fcneval object
      HAxes         % subplot axes handle
      HLUpdateGraph % UpdateGraph listener handle
      HLLm          % Lm property PostSet listener handle
      HEnableCm     % "Listen" context menu handle
      HDisableCm    % "Don't Listen" context menu handle
      HSurface      % Surface object handle
   end
   
   methods
      function obj = fcnview(fcnobj)
         if nargin > 0
            obj.FcnObject = fcnobj;
            obj.createLisn;
         end
      end % fcnview
      
      function createLisn(obj)
         obj.HLUpdateGraph = addlistener(obj.FcnObject,'UpdateGraph',...
            @(src,evnt)listenUpdateGraph(obj,src,evnt));
         obj.HLLm = addlistener(obj.FcnObject,'Lm','PostSet',...
            @(src,evnt)listenLm(obj,src,evnt));
      end % createLisn
      
      function lims(obj)
         lmts = obj.FcnObject.Lm;
         set(obj.HAxes,'XLim',lmts);
         set(obj.HAxes,'Ylim',lmts);
      end % lims
            
      function updateSurfaceData(obj) 
         data = obj.FcnObject.Data;
            set(obj.HSurface,...
               'XData',data.X,...
               'YData',data.Y,...
               'ZData',data.Matrix);
      end % updateSurfaceData
      
      function listenUpdateGraph(obj,~,~)
         if ishandle(obj.HSurface)
           obj.updateSurfaceData
         end         
      end % listenUpdateGraph event handler
      
      function listenLm(obj,~,~)
         if ishandle(obj.HAxes)
            lims(obj);
            if ishandle(obj.HSurface)
               obj.updateSurfaceData
            end
         end
      end % Lm property event handler
 
      function delete(obj)
         if ishandle(obj.HAxes)
            delete(obj.HAxes); % Delete the axes and children
         else
            return
         end
      end % delete   
  
   end % methods 
   methods (Static)
      createViews(a)
   end 
end % FcnView