function YLimPropertyPostSet(hProp,eventData)   
   hAxes        = eventData.AffectedObject;
   
   hdlFig       = eventData.AffectedObject.Parent;
   hdlAxes      = getappdata(hdlFig, 'tabHandlesAxes');
   
   if ~ishghandle(hdlFig)
       my_warndlg('La fen�tre de trac� n''existe plus !', 1);
       return
   end
   %% Initialisation
   % TODO : remplacer gco par la r�cup�ration du handle point� par le
   % EventData.
   if isempty(handle(gco))
       % Le trac� est en cours d'initialisation
       return
   end
   
   [C, ia, ib]  = intersect(hdlAxes, gco); 
   XLim         = get(gco, 'XLim');
   YLim         = get(gco, 'YLim');
   if ~isempty(ia)
       switch ia
           case 1
               % hlink = linkprop(hdlAxes([1 3 4]),{'XLim'});
               % set(hdlAxes(3), 'XLim', XLim);
               % set(hdlAxes(4), 'XLim', XLim);
               set(hdlAxes(2), 'YLim', YLim);
               set(hdlAxes(4), 'ZLim', YLim);
           case 2
               % hlink = linkprop(hdlAxes([2 1]),{'YLim'});
               set(hdlAxes(1), 'YLim', YLim);
               % Attention au croisement de coordonn�es.
%                set(hdlAxes(3), 'YLim', XLim);
%                set(hdlAxes(4), 'YLim', XLim);
               set(hdlAxes(4), 'ZLim', YLim);
           case 3
               % hlink = linkprop(hdlAxes([3 1 4]),{'XLim'});
%                set(hdlAxes(1), 'XLim', XLim);
%                set(hdlAxes(4), 'XLim', XLim);
               % Attention au croisement de coordonn�es.
               set(hdlAxes(2), 'XLim', YLim);
               set(hdlAxes(4), 'ZLim', YLim);
           case 4
               ZLim = get(gco, 'ZLim');
               % hlink = linkprop(hdlAxes([4 1 3]),{'XLim'});
%                set(hdlAxes(1), 'XLim', XLim);
%                set(hdlAxes(3), 'XLim', XLim);
               % Attention au croisement de coordonn�es.
%                set(hdlAxes(1), 'YLim', ZLim);
               set(hdlAxes(2), 'XLim', YLim);
%                set(hdlAxes(2), 'YLim', ZLim);
%                set(hdlAxes(3), 'YLim', ZLim);
               
          otherwise
               my_warndlg(Lang('Probl�me dans la gestion des �v�nements du Trac� - SVP, contactez JMA','Problem in plot event - please contact JMA'),1);
               return
       end
   end
   
   drawnow expose update
end  % myCallbackFunction