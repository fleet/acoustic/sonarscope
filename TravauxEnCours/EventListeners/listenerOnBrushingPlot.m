figure; 
ha(1) = subplot(2,1,1, 'Tag', 'axe1');
X = 1:10;
p1 = plot(X, 10:10:100,'o');

ha(2) = subplot(2,1,2, 'Tag', 'axe2');
p2 = plot(X, 20:20:200,'+');

addlistener(p1,'YData','PostSet',@(s,e)disp('Data changed'));

linkdata on
brush on;

% If you need to identify which data points have been selecting you can use the following code in the callback:


ll = findobj(gcf, 'Type', 'line');
Xll = get(ll(1),'XData');
Yll = get(ll(1),'YData');


%% Brushing actions.

% Find brushing object
bb = findall(gcf,'Tag','Brushing');
% Find selected points
Xbb = get(bb(1),'XData');
sub = (~isnan(Xbb{1}));
sub = find(sub == 1);

% Return coordinates for these points
% X = get(bb,'XData');
X = Xll(sub)

% Y = get(bb,'YData');
Y = Yll(sub)

% % Z = get(bb,'ZData');
% % Z = Z(sub)
