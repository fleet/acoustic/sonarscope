x = -pi:pi/10:pi;
y = tan(sin(x)) - sin(tan(x));
hPlot = plot(x,y,'--rs','LineWidth',2,...
           'MarkerEdgeColor','k',...
           'MarkerFaceColor','g',...
           'MarkerSize',10);
       
hListener = handle.listener(hPlot,'simpleEvent',@displayEventInfo);
setappdata(hPlot, 'listeners', hListener);

evtData = handle.EventData(hListener, 'simpleEvent');

%%
clear MyEventData
clear MyEventData
clear eventData
X = 1:10;
Y = 1:10;
data        = struct('XData', X, 'YData', Y);
eventData   = MyEventData(data);

obj = MyClass;
l = addlistener(obj, 'MyEvent', @(evtSrc,evtData)disp(evtData.Data));
notify(obj, 'MyEvent', eventData)

%%
NomFicEchoes = 'K:\Data\IFREMER\Data_SSC\SSC-3DViewer-Data\3DViewer-GLOBE\WC\Marmesonet\Echoes\0306_Echoes.xml';

myFig = FigUtils.createSScFigure;

% Lecture du fichier Echoes
[flag, Data] = read_XMLBin(NomFicEchoes, 'BinInData', 1);
X = Data.iPing;
Y = Data.AcrossDistance;
Z = Data.Z;

XLabel = 'X';
YLabel = 'Y';
ZLabel = 'Z';

c = cl_plot(X, Z, 'o');
grid on; xlabel(XLabel); ylabel(ZLabel); title('Z=f(X)');