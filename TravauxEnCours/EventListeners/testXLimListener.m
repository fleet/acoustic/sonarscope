function testXLimListener()
global xLimCount;
xLimCount = 0;

handles.fig(1) = figure();
handles.axs(1) = axes('parent',handles.fig(1),'NextPlot','add');

x = -pi:.1:pi;
handles.sta(1) = stairs(handles.axs(1),x,cos(x),'-r');
handles.sta(2) = stairs(handles.axs(1),x,sin(x),'-b');

handles.cb(1) = line([0 0],[-1 1],'Color','black','LineWidth',3); %own cursorbar
setappdata(handles.axs(1),'ismoving',false);

xlimp = findprop(handle(handles.axs(1)),'XLim');
XLimListener = handle.listener(handle(handles.axs(1)),xlimp,...
'PropertyPostSet',@xlimlistener);
setappdata(handles.axs(1), 'XLimListener',XLimListener);
%--------------------------------------------------------------------------
set(handles.fig(1),'WindowButtonDownFcn',{@wbdfcn,handles});
set(handles.fig(1),'WindowButtonMotionFcn',{@wbmfcn,handles});
set(handles.fig(1),'WindowButtonUpFcn',{@wbufcn,handles});
%--------------------------------------------------------------------------
function wbdfcn(~, ~, handles)

if gco(handles.fig(1)) == handles.cb(1), setptr(handles.fig(1),'lrdrag');end
setappdata(handles.axs(1),'ismoving',true);
%--------------------------------------------------------------------------
function wbmfcn(~, ~, handles)

if ~getappdata(handles.axs(1),'ismoving'), return;end
xlim = get(handles.axs(1),'XLim');
cp = getCurrentPoint(handles.axs(1));
if cp(1) <= xlim(1) || cp(1) >= xlim(2), return;end
set(handles.cb(1),'xdata',[cp(1) cp(1)]);
%--------------------------------------------------------------------------
function wbufcn(~, ~, handles)

set(handles.fig(1),'pointer','arrow');
setappdata(handles.axs(1),'ismoving',false);
%--------------------------------------------------------------------------
function cp = getCurrentPoint(aobj)

cp = get(aobj,'CurrentPoint');
cp = cp(1,1:2);
%--------------------------------------------------------------------------
function xlimlistener(~, evt)
global xLimCount;

xLimCount = xLimCount + 1;
disp(xLimCount);
%-------------------------------------------------------------------------- 