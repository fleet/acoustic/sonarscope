% Transcodage des cl�s d'activation du fichier xls des mots de passe de
% SonarScope
%
% SYNTAX :
%   flag = cryptSSC3DV_CSV(nomFic)
%
% INPUT PARAMETERS :
%  nomFic : nom du fichier de cl�s d'activation de SonarScope
%
% OUTPUT PARAMETERS :
%   flag : 1 (OK) ou 0 (erreur)
%
% EXAMPLES :
%   nomFic = 'C:\eclipse_ws\ScriptsSonarscope-3DViewer\Resources\ListePLP_SonarScope.xls'
%   [flag,nomFicOut] = cryptSSC3DV_CSV(nomFic)
%
% Compilation :
%   mcc -m -v cryptSSC3DV_CSV.m -d ...
%   C:\eclipse_ws\ScriptsSonarscope-3DViewer\Resources\Cryptage-Cl�s cryptSSC3DV_CSV
%
% SEE ALSO   : SonarScope Authors
% AUTHORS    : GLU
% VERSION    : $Id: checkGrantCleActivation.m,v 1.0 2005/10/12 12:46:11 GLU Exp $
%-------------------------------------------------------------------------------

function [flag, nomFicOut] = cryptSSC3DV_CSV(nomFic)

try
    % Lecture des 100 premiers clients/destinataires.
    %%%% Provoque un "touch" du fichier : genant pour svn .
    %%%% X = readfromexcel(filename, 'A1:G100');
    [num,txt,X] = xlsread(nomFic, 'A1:G200', 'basic'); %#ok<ASGLU>
        
    % Ecriture du fichier ListePLP_SonarScope.csv
    [pathstr, name, ~, ] = fileparts(nomFic);
    nomFicCSV   = fullfile(pathstr, [name '.csv']);
    nomFicOut   =  fullfile(pathstr, [name '_crypt.csv']);
   
    % Elimination des lignes disposant de NaN.
    cellclass       = 'char';
    ciscellclass    = cellfun('isclass',X,cellclass);
    X               = X(ciscellclass);
    sz              = size(X,1);
    X               = reshape(X, sz/7, 7);
    [nrows,ncols]   = size(X); %#ok<NASGU>
    fid = fopen(nomFicCSV, 'w');
    for row=1:nrows
        fprintf(fid, '%s;%s;%s;%s;%s;%s;%s;\n', X{row,:});
    end
    fclose(fid);

    % Cryptage des chaines.
    X = cryptSSC3DV_PLP(X);
    
    % Sauvegarde dans le fichier PLP de d�ploiement.
    cell2csv(nomFicOut, X, ';')
    
    % save(nomFicPLP, 'X');
    flag = 1;
catch  ME 
    X       = [];
    flag    = 0;
end