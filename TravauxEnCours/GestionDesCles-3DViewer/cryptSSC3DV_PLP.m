function Y = cryptSSC3DV_PLP(X)

for i=1:size(X,1)
    for j=1:size(X,2)
        if ~isnan(X{i,j})
            % Pour chaque caract�re c de X :
            %     y = (c+j)*2 + (-i)*30
            Y{i,j} = cryptSSC3DV_str2num(X{i,j}, i, j); %#ok<AGROW>
            
        end
    end
end
