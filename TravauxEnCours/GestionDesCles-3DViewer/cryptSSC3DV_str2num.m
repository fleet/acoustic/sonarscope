function y = cryptSSC3DV_str2num(str, i, j)
x = double(str);

for k=1:length(x)
   y(k)= (x(k)+j)*2 + (-i)*30; %#ok<AGROW>
end
