function testAnchois

% nomFic = 'CLASS08_004_20080602_050523_1_school.mat';
% nomDirData = 'F:\Data_WC\FromLaurentBerger\school anchois\';
nomFic = '0006_20090605_211643_1_school.mat';
nomDirData = 'F:\Data_WC\FromLaurentBerger\epave douarnenez';
nomDirOut = nomDirData;

[pathstr, name, ext, versn] = fileparts(nomFic);

var = load(fullfile(nomDirData, nomFic));
for i=1:length(var.schools_ME70)
    nomFicKML = fullfile(nomDirOut,[name num2str(i) '_polygon.kml']);
    pppp = var.schools_ME70(i);
    M = zeros(size(pppp.contours,2),3);
    str = {};
    for j=1:size(pppp.contours,2)
        M(j,1) = pppp.contours(j).m_latitude;
        M(j,2) = pppp.contours(j).m_longitude;
        M(j,3) = pppp.contours(j).m_depth;
        str{end+1} = [num2str(M(j,1), '%f') ' ' num2str(M(j,2), '%f') ' ' num2str(M(j,3), '%f') ' 20'];
    end
    % D�finition de la carto.
    Carto.Ellipsoide.Type = 11; % WGS84
    Carto.Ellipsoide.Excentricite = 000.08181919104281098;
    Carto.Ellipsoide.DemiGrandAxe = 006378137.00000000000; % (m)                                                                                                                                                                                                                             
    Carto.Projection.Lambert.Type                 = 1; % Lambert 93'
    Carto.Projection.Lambert.first_paral          =  44.000000;                                                                                                                                                                                                                               
    Carto.Projection.Lambert.second_paral         = 49.000000;                                                                                                                                                                                                                               
    Carto.Projection.Lambert.long_merid_orig      = 3.000000;                                                                                                                                                                                                                               
    Carto.Projection.Lambert.X0                   = 700000.000000;                                                                                                                                                                                                                           
    Carto.Projection.Lambert.Y0                   = 6600000.000000;                                                                                                                                                                                                                          
    Carto.Projection.Lambert.scale_factor         = 1.000000;                                                                                                                                                                                                                                

    % Carto = cl_carto('Ellipsoide.Type', 12, 'Projection.Type', 4);
    [x, y] = latlon2xy(Carto, M(:,1), M(:,2));
    x = x-mean(x);
    y = y-mean(y);
    X = [x y M(:,3)];
    % Calcul d'enveloppe Convexe.
    K = convhulln(X, {'Qt','Qx'});
    % Autre calcul par Delaunay3
    T = delaunay3(x,y,M(:,3));
    % figure; trimesh(K,X(:,1),X(:,2),X(:,3));
    % figure; trisurf(K,X(:,1),X(:,2),X(:,3));
    [V,C]=voronoin(X);
    for k=1:length(C)
        if all(C{k}~=1)
            VertCell = V(C{k},:);
            KVert = convhulln(VertCell);
            patch('Vertices',VertCell,'Faces',KVert,'FaceColor','g','FaceAlpha',0.5)
        end
    end
    banc(i).Points = M;
    banc(i).Facettes = K;
    banc(i).Time = pppp.time;
    
    % Ecriture dans le fichier XML.
    %         nomFicPlume = fullfile(nomDirOut,[name num2str(i) '_pointsPlume.txt']);
    %         fid = fopen(nomFicPlume, 'w+');
    %         for k=1:length(str)
    %             fprintf(fid,'%s\n', str{k});
    %         end
    %         fclose(fid);
    
end
writeKMLBanc(nomFicKML, banc);


% Passage de coordonn�es g�ographiques � coordonn�es cartographiques
%
% Cf. Cl_carto\latlon2xy de la Bilbioth�que SSC.
%
% SYNTAX :
%   [x, y] = latlon2xy(Carto, lat, lon)
% 
%-------------------------------------------------------------------------------

function [x, y, flag] = latlon2xy(Carto, lat, lon)

flag = 1;
E  = double(Carto.Ellipsoide.Excentricite);
E2 = E ^ 2;
A  = double(Carto.Ellipsoide.DemiGrandAxe);
deg2rad = pi / 180;

x = NaN(size(lon), 'double');
y = NaN(size(lon), 'double');

L1   = double(Carto.Projection.Lambert.first_paral)     * deg2rad;
L2   = double(Carto.Projection.Lambert.second_paral)    * deg2rad;
G0   = double(Carto.Projection.Lambert.long_merid_orig) * deg2rad;
X0   = double(Carto.Projection.Lambert.X0);
Y0   = double(Carto.Projection.Lambert.Y0);
RK   = double(Carto.Projection.Lambert.scale_factor);
N1   = A / sqrt(1 - (E2 * sin(L1) ^ 2));
N2   = A / sqrt(1 - (E2 * sin(L2) ^ 2));
ISO1 = tan(L1/2+pi/4)*((1-E*sin(L1))/(1+E*sin(L1)))^(E/2);
ISO2 = tan(L2/2+pi/4)*((1-E*sin(L2))/(1+E*sin(L2)))^(E/2);
L12  = (L1 + L2) / 2;
R0   = A / sqrt(1 - E2*sin(L12)^2) / sin(L12) * cos(L12) * RK;
N    = log((N2*cos(L2)) / (N1 * cos(L1))) / log(ISO1/ISO2);
C    = N1 * cos(L1) / N * ISO1^N;

for i=1:size(lon,1)
    G = lon(i,:) * deg2rad;
    L = lat(i,:) * deg2rad;
    ISO  = tan(pi/4 + L/2) * ((1-E*sin(L)) / (1+E*sin(L))) ^ (E/2);
    
    IsoN   = ISO .^ N;
    NG_G0  = N .* (G - G0);
    x(i,:) =      C .* sin(NG_G0) ./ IsoN + X0;
    y(i,:) = R0 - C .* cos(NG_G0) ./ IsoN + Y0;
end
