%%
% EXAMPLE :
% nomFic = 'C:\bilFilesLocalHost\pointsPlume_Light.txt';
% nomFic = 'C:\bilFilesLocalHost\pointsPlume.txt';
% testPlume('C:\bilFilesLocalHost','pointsPlume.txt',10)
% 
function testPlume(nomDir, nomFic, nbSliceZ, varargin)

%%
%{
% trac� d'un c�ne
clear all
% close all
clc

Xo=0;
Yo=0;
Zo=0;
% theta = -pi:3*pi/180:pi;
% m = 0:100-Yo;
theta = -pi:2*pi/150:pi-2*pi/150;
m = 0:149-Yo;
r=m*tand(70);

%Les matrices x, y et z repr�sentent les coordonn�es des points.
%Ces matrices ont un format de 150x150. La matrice z est repr�sent�e par
%cercles concentriques de rayons diff�rents et d'altitudes diff�rentes.
%Chaque colonne de la matrice z repr�sente un cercle.

y = cos(theta(:))*r+Xo;
z = (ones(length(theta),1)*m+Yo) * 5 ;
x = sin(theta(:))*r+Zo;
 
randBidon =  rand(150,150)*(1*40) ;
randNaN = find(randBidon > (0.8*40));
randBidon(randNaN) = NaN;

%C�ne invers�
% z = z ;%+ randBidon;
%C�ne normal
z = -z -  min(min(-z));% + randBidon;
%%
figure;
surf(x,y,z,...
	'EdgeColor','none',...
	'FaceLighting','phong')
path = 'C:\bilFilesLocalHost';
nameFile = 'Montagne800m_bas0_plein_normale';
name = fullfile(path,[nameFile '.bil']);
         

multibandwrite(z , name ,'bil','precision', 'float32');
%%

% plot3(x,y,z)

% tracer d'une demi-sph�re
% alpha = -pi:3*pi/180:pi;
% phi   = 0:pi/180:pi/2;
% 
% r=85;
% 
% for b=1:length(alpha)
%     for c=1:length(phi)
%         g(b,c)=r*cos(alpha(b))*cos(phi(c))+Xo;
%         h(b,c)=r*sin(phi(c))+Yo;
%         j(b,c)=r*sin(alpha(b))*cos(phi(c))+Zo;
%     end
% end
% 
% figure;
% plot3(g,h,j)

%}
%%
M = dlmread(fullfile(nomDir, nomFic));

%%
lat = M(:,1);
lon = M(:,2);
z = M(:,3);
%%
% [a, b, c] = unique(z, 'first');
ampZ = max(z) - min(z);
pasRoundZ = round(ampZ/nbSliceZ/100)*100;
nLoop = round(ampZ/pasRoundZ) + 1;
ampLat = linspace(max(lat),min(lat),150);
ampLon = linspace(min(lon),max(lon),150);
for i=1:nLoop
    % Extraction des points par tranche de hauteur (plaquage artificiel des
    % echos sur des slices verticales).
    subz = find(z > (min(z)+(i-1)*pasRoundZ) & z < (min(z)+i*pasRoundZ));
    subLat = lat(subz);
    subLon = lon(subz);
    % Remplissage des slices.
    newZ = NaN(150,150,'single');
    for k=1:length(subz);
        indLat = find(ampLat > subLat(k), 1, 'last');
        indLon = find(ampLon > subLon(k), 1, 'first');
        newZ(indLat, indLon) = z(subz(k));
    end
    % Toutes les tuiles sont � positionner sur le point Max(Lat) et Min(Lon) 
    % correspondant au haut � gauche de chaque tuile.
    nomFicBil = fullfile(my_tempdir,['fic_' num2str(floor(i*pasRoundZ), '%02d') '.bil']);
    multibandwrite(newZ , nomFicBil ,'bil','precision', 'float32');
end
