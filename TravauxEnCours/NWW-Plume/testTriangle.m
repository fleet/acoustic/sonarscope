function testTriangle

nomFileName = 'C:\bilFilesLocalHost\pointsPlume.txt';
% nomFileName = 'C:\bilFilesLocalHost\pointsPlume_Light.txt';

M = dlmread(nomFileName);
Carto = cl_carto('Ellipsoide.Type', 12, 'Projection.Type', 4)
[x, y] = latlon2xy(Carto, M(:,1), M(:,2));
x = x-mean(x);
y = y-mean(y);
%%
figure;
X = [x y M(:,3)];
Tes = delaunayn(X);
tetramesh(Tes,X);
view(3);
%% Trac� de l'enveloppe 3D
X = [x y M(:,3)];
K = convhulln(X);
figure;
trisurf(K,X(:,1),X(:,2),X(:,3))
% figure;
% trimesh(K,X(:,1),X(:,2),X(:,3));
%%
nomFicXml = 'C:\Temp\Polygons_GE.kml';
fid = fopen(nomFicXml, 'w+');
if fid == -1
    str = sprintf(Lang('Cr�ation de fichier Example de Polygon sous GE', ...
        'Cr�ation de fichier Example de Polygon sous GE', 1));
    my_warndlg(str, 1);
    return
end

fprintf(fid, '<?xml version="1.0" encoding="UTF-8"?>\n');
fprintf(fid, '<kml xmlns="http://www.opengis.net/kml/2.2">\n');
fprintf(fid, '  <Document>\n');
fprintf(fid, '    <Style id="transBluePoly">\n');
fprintf(fid, '      <LineStyle>\n');
fprintf(fid, '        <width>1.5</width>\n');
fprintf(fid, '      </LineStyle>\n');
fprintf(fid, '      <PolyStyle>\n');
fprintf(fid, '        <color>7dff0000</color>\n');
fprintf(fid, '      </PolyStyle>\n');
fprintf(fid, '    </Style>\n');
fprintf(fid, '    <Placemark>\n');
fprintf(fid, '      <name>Building 41</name>\n');
fprintf(fid, '      <styleUrl>#transBluePoly</styleUrl>\n');
fprintf(fid, '      <Polygon>\n');
fprintf(fid, '        <extrude>1</extrude>\n');
fprintf(fid, '        <altitudeMode>relativeToGround</altitudeMode>\n');
fprintf(fid, '        <outerBoundaryIs>\n');
fprintf(fid, '          <LinearRing>\n');
fprintf(fid, '          <coordinates>\n');
rand(1);
for i=1:length(M)
    fprintf(fid, '%f, %f, %f\n', M(i,2)+0.001*i,M(i,1)+0.001*i,100+10*rand(1));
end
fprintf(fid, '            </coordinates>\n');
fprintf(fid, '          </LinearRing>\n');
fprintf(fid, '        </outerBoundaryIs>\n');
fprintf(fid, '      </Polygon>\n');
fprintf(fid, '    </Placemark>\n');
fprintf(fid, '  </Document>\n');
fprintf(fid, '</kml>\n');

fclose(fid);