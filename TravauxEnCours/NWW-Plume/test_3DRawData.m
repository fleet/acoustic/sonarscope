function test_3DRawData

nomFicXML = 'F:\Data_WC\WC\0009_20090716_064152_EX.xml';
nomFic3D = 'F:\Data_WC\WC\0009_20090716_064152_EX.raw';

nbPings = 11;
nbColumns = 1667;
nbRows = 600;

fid = fopen(nomFic3D, 'r');
if fid == -1
    str = sprintf(Lang('Impossible to lire le fichier de donn�e %s', ...
        'Impossible to read data file %s'), nomFic3D);
    my_warndlg(str, 1)
    return
end

% Initialisation de la matrice Image.
A = zeros(nbRows, nbColumns, 'uint8');

for iPing=1:nbPings
    % Positionnement depuis le d�but de fichier sur le Ping courant.
    fseek(fid, (iPing-1)*nbColumns*nbRows, 'bof');
    % Lecture par paquets de NbColumns de la donn�e.
    for i=1:nbRows
        A(i, :) = fread(fid, nbColumns, 'uint8=>uint8');
    end
    % Trac� de l'image sous MatLab (leur trac� se met � jour dans la figure
    % d�sign�e, �quivalent � un film).
    figure(23456);imagesc(A);axis xy;
end
fclose(fid);

