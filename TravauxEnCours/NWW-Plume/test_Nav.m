function test_Nav
%% Trac� de navigations des Lignes de pppp.xml
nomFicXML = 'F:\Data_WC\FromJMA\Echos_WC\Liv_20100209\Nav_306_307\306_307_20091113_Lesuroit.xml';
nomDirNav = 'F:\Data_WC\FromJMA\Echos_WC\Liv_20100209\Nav_306_307\306_307_20091113_Lesuroit';

[tree, RootName, DOMnode] = xml_read(nomFicXML);


for i=1:tree.Dimensions.nbLines
    % Lecture des donn�es
    % Date
    nbPts       = tree.Line(1).Dimensions.nbPoints;
    label       = tree.Line(i).Signals(1).Name;
    labelType   = tree.Line(i).Signals(1).Storage;
    labelUnite  = tree.Line(i).Signals(1).Unit;
    nomFic      = fullfile(nomDirNav, tree.Line(i).Signals(1).FileName);
    data(i).Date   = navFicRead(nomFic, labelType, nbPts);

    % Heure.
    label       = tree.Line(i).Signals(2).Name;
    labelType   = tree.Line(i).Signals(2).Storage;
    labelUnite  = tree.Line(i).Signals(2).Unit;
    nomFic      = fullfile(nomDirNav, tree.Line(i).Signals(2).FileName);
    data(i).Hour   = navFicRead(nomFic, labelType, nbPts);

    % Lat.
    label       = tree.Line(i).Signals(3).Name;
    labelType   = tree.Line(i).Signals(3).Storage;
    labelUnite  = tree.Line(i).Signals(3).Unit;
    nomFic      = fullfile(nomDirNav, tree.Line(i).Signals(3).FileName);
    data(i).Lon   = navFicRead(nomFic, labelType, nbPts);

    % Lon.
    label       = tree.Line(i).Signals(4).Name;
    labelType   = tree.Line(i).Signals(4).Storage;
    labelUnite  = tree.Line(i).Signals(4).Unit;
    nomFic      = fullfile(nomDirNav, tree.Line(i).Signals(4).FileName);
    data(i).Lat = navFicRead(nomFic, labelType, nbPts);

    % Depth.
    label       = 'Depth.bin';
    labelType   = 'double';
    labelUnite  = 'm';
    nomFic      = fullfile(nomDirNav, ['00' num2str(i)], label);
    data(i).Depth = navFicWrite(nomFic, labelType, nbPts, -100*i);
    
end

figure(111111); 
plot(data(1).Lon, data(1).Lat,'Color','red');
hold on;
plot(data(2).Lon, data(2).Lat,'Color','green');
% 

%% Trac� de navigations des Lignes de PolarEchograms.xml
nomFicXML = 'F:\Data_WC\FromJMA\Echos_WC\Liv_20100209\WC_NWW_0307_20091113_163950_Lesuroit\PolarEchograms.xml';
nomDirData = 'F:\Data_WC\FromJMA\Echos_WC\Liv_20100209\WC_NWW_0307_20091113_163950_Lesuroit';
% nomFicXML = 'F:\Data_WC\FromJMA\Echos_WC\Liv_20100209\WC_NWW_0306_20091113_160950_Lesuroit\PolarEchograms.xml';
% nomDirData = 'F:\Data_WC\FromJMA\Echos_WC\Liv_20100209\WC_NWW_0306_20091113_160950_Lesuroit';

[tree, RootName, DOMnode] = xml_read(nomFicXML);

    i=0
    % Lecture des donn�es
    % Date
    nbPings     = tree.Dimensions(1).nbPings;
    label       = tree.Signals(1).Name;
    labelType   = tree.Signals(1).Storage;
    labelUnite  = tree.Signals(1).Unit;
    nomFic      = fullfile(nomDirData, tree.Signals(1).FileName);
    data(i+1).Date   = navFicRead(nomFic, labelType, nbPings);

    % Heure.
    label       = tree.Signals(2).Name;
    labelType   = tree.Signals(2).Storage;
    labelUnite  = tree.Signals(2).Unit;
    nomFic      = fullfile(nomDirData, tree.Signals(2).FileName);
    data(i+1).Hour   = navFicRead(nomFic, labelType, nbPings);

    % Lon. Babord
    label       = tree.Signals(7).Name;
    labelType   = tree.Signals(7).Storage;
    labelUnite  = tree.Signals(7).Unit;
    nomFic      = fullfile(nomDirData, tree.Signals(7).FileName);
    data(i+1).lonBab   = navFicRead(nomFic, labelType, nbPings);

    % Lat Babord
    label       = tree.Signals(8).Name;
    labelType   = tree.Signals(8).Storage;
    labelUnite  = tree.Signals(8).Unit;
    nomFic      = fullfile(nomDirData, tree.Signals(8).FileName);
    data(i+1).latBab = navFicRead(nomFic, labelType, nbPings);
    

    % Lon. Centre
    label       = tree.Signals(9).Name;
    labelType   = tree.Signals(9).Storage;
    labelUnite  = tree.Signals(9).Unit;
    nomFic      = fullfile(nomDirData, tree.Signals(9).FileName);
    data(i+1).lonCentre   = navFicRead(nomFic, labelType, nbPings);

    % Lat Centre
    label       = tree.Signals(10).Name;
    labelType   = tree.Signals(10).Storage;
    labelUnite  = tree.Signals(10).Unit;
    nomFic      = fullfile(nomDirData, tree.Signals(10).FileName);
    data(i+1).latCentre = navFicRead(nomFic, labelType, nbPings);

    % Lon. Tribord
    label       = tree.Signals(11).Name;
    labelType   = tree.Signals(11).Storage;
    labelUnite  = tree.Signals(11).Unit;
    nomFic      = fullfile(nomDirData, tree.Signals(11).FileName);
    data(i+1).lonTri   = navFicRead(nomFic, labelType, nbPings);

    % Lat Tribord
    label       = tree.Signals(12).Name;
    labelType   = tree.Signals(12).Storage;
    labelUnite  = tree.Signals(12).Unit;
    nomFic      = fullfile(nomDirData, tree.Signals(12).FileName);
    data(i+1).latTri = navFicRead(nomFic, labelType, nbPings);

    figure(222222);
    hold on;
    plot(data(i+1).lonBab, data(i+1).latBab,'Color','r');
    plot(data(i+1).lonTri, data(i+1).latTri,'Color','g');
    plot(data(i+1).lonCentre, data(i+1).latCentre,'Color','b');

end

%%

function data = navFicRead(nomFic, Type, nbPoints)

fid = fopen(nomFic, 'r');
if fid == -1
    str = sprintf(Lang('Impossible to lire le fichier de donn�e %s', ...
        'Impossible to read data file %s'), nomFic);
    my_warndlg(str, 1)
    return
end

% Initialisation de la matrice Image.
data = zeros(nbPoints, Type);

data = fread(fid, nbPoints, [Type '=>' Type]);

fclose(fid);
end

%%
function data = navFicWrite(nomFic, Type, nbPoints, offset)

fid = fopen(nomFic, 'w');
if fid == -1
    str = sprintf(Lang('Impossible to lire le fichier de donn�e %s', ...
        'Impossible to read data file %s'), nomFic);
    my_warndlg(str, 1)
    return
end

% Initialisation de la matrice Image.
data = rand(nbPoints, 1, Type)*3+offset;

fwrite(fid, data, Type);

fclose(fid);
end


