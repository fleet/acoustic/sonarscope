% Fonction writeKMLPolygon
% EXAMPLE:
%   test_writeKML('C:\bilFilesLocalHost', 'pointsPlume.txt')
%   test_writeKML('F:\Data_WC\FromLaurent\extraction\seuil 95', '')
% ----------------------------------------------------------------------------

function test_writeKML(nomDirData, nomFicData)


if ~isempty(nomFicData)
    nomFic = fullfile(nomDirData, nomFicData);
    
    M = dlmread(nomFic);
    Carto = cl_carto('Ellipsoide.Type', 12, 'Projection.Type', 4);
    [x, y] = latlon2xy(Carto, M(:,1), M(:,2));
    x = x-mean(x);
    y = y-mean(y);
    
    %% Trac� de l'enveloppe 3D
    X = [x y M(:,3)];
    K = convhulln(X);
    % Trac� des facettes
    % figure; trisurf(K,X(:,1),X(:,2),X(:,3))
    % Trac� des ar�tes
    % figure; trimesh(K,X(:,1),X(:,2),X(:,3));
    %%
    nomFicKML = fullfile(my_tempdir, nomFicKML);
    test_writeKML(nomFicKML, K, M);
else
    %%
    nomDirData = 'F:\Data_WC\FromLaurentBerger\extraction\seuil 95';
    nivSeuil = nomDirData(end-1:end);
    nomDirOut = fullfile(my_tempdir, 'extraction');
    if ~exist(nomDirOut, 'dir')
        mkdir(nomDirOut);
    end
    nomDirOut = fullfile(nomDirOut, nivSeuil);
    if ~exist(nomDirOut, 'dir')
        mkdir(nomDirOut);
    end
    
    files = dir(nomDirData);
    for f=1:length(files)
        [~, name] = fileparts(files(f).name);
        if ~isfolder(files(f).name)
            var = load(fullfile(nomDirData, files(f).name));
            for i=1:length(var.schools_ME70)
                nomFicKML = fullfile(nomDirOut,[name num2str(i) '_polygon.kml']);
                pppp = var.schools_ME70(i);
                M = zeros(size(pppp.contours,2),3);
                if pppp.contours(1).m_latitude > 40.859 && pppp.contours(1).m_latitude < 40.862
                    str = {};
                    for j=1:size(pppp.contours,2)
                        M(j,1) = pppp.contours(j).m_latitude;
                        M(j,2) = pppp.contours(j).m_longitude;
                        M(j,3) = -pppp.contours(j).m_depth;
                        %str{end+1} = [num2str(M(j,1), '%f') ' ' num2str(M(j,2), '%f') ' ' num2str(M(j,3), '%f') ' 20']; 
                    end
                    Carto = cl_carto('Ellipsoide.Type', 12, 'Projection.Type', 4);
                    [x, y] = latlon2xy(Carto, M(:,1), M(:,2));
                    x = x-mean(x);
                    y = y-mean(y);
                    X = [x y M(:,3)];
                    K = convhulln(M);
                    
                    % Ecriture dans le fichier XML.
                    nomFicPlume = fullfile(nomDirOut,[name num2str(i) '_pointsPlume.txt']);
                    fid = fopen(nomFicPlume, 'w+');
                    for k=1:length(str)
                        fprintf(fid,'%s\n', str{k});
                    end
                    fclose(fid);
                    
                end
            end
            writeKMLBanc(nomFicKML, pppp);
        end
    end
end
