%Test valideLatLon
% Exemple:
% nomDirPts = 'F:\Data_WC\FromJMA\Echos_WC\Liv_29012010\WC_NWW_0307_20091113_163950_Lesuroit\WC_Echos\'
% nomDirPolars = 'F:\Data_WC\FromJMA\Echos_WC\Liv_29012010\WC_NWW_0307_20091113_163950_Lesuroit\PolarEchograms\'
%
% nomDirPts = 'F:\Data_WC\FromJMA\Echos_WC\Liv_29012010\WC_NWW_0306_20091113_160950_Lesuroit\WC_Echos\'
% nomDirPolars = 'F:\Data_WC\FromJMA\Echos_WC\Liv_29012010\WC_NWW_0306_20091113_160950_Lesuroit\PolarEchograms\'
%
% pppp = valideLatLon(nomDirPts, nomDirPolars)
function flagValid = valideLatLon(nomDirPts, nomDirPolars)

flagValid = 0;

nomFicBinPts = fullfile(nomDirPts, 'Latitude.bin');
nomFicBinPolars = fullfile(nomDirPolars, 'latCentre.bin');

% Lecture des latitudes des Points.
fid = fopen(nomFicBinPts, 'r');
latPts = fread(fid, 10, 'double')
fclose(fid);

%%
% Lecture des latitudes des Polar Echograms.
fid = fopen(nomFicBinPolars, 'r');
latPolar = fread(fid, 10, 'double')
fclose(fid);


%%
nomFicBinPts = fullfile(nomDirPts, 'Longitude.bin');
nomFicBinPolars = fullfile(nomDirPolars, 'lonCentre.bin');

% Lecture des latitudes des Points.
fid = fopen(nomFicBinPts, 'r');
latPts = fread(fid, 10, 'double')
fclose(fid);

%%
% Lecture des latitudes des Polar Echograms.
fid = fopen(nomFicBinPolars, 'r');
latPolar = fread(fid, 10, 'double')
fclose(fid);
