% Fonction writeKMLBanc
% EXAMPLE:
% writeKMLBanc('example_Polygon.kml', MPoints, KFacettes, ...)
% ----------------------------------------------------------------------------

function writeKMLBanc(nomFicKml, MultiBanc)

%% Temps
format = 'yyyy-mm-ddTHH:MM:SSZ';

date = now + (1:1:size(MultiBanc,2));
dateStr = cellstr( datestr(date, format) );


fid = fopen(nomFicKml, 'w+', 'native', 'UTF-8');
if fid == -1
    str = sprintf(Lang('Impossible to cr�er Fichier KML %s', ...
        'Impossible to create KML config file %s'), nomFicXml);
    my_warndlg(str, 1)
    return
end

fprintf(fid, '<?xml version="1.0" encoding="utf-8"?>\n');
fprintf(fid, '<!--Fichier cree depuis la fonction writeKMLPolygon -->\n');
fprintf(fid, '<!--Auteur : GLU 29/01/2010-->\n');
fprintf(fid, '<kml xmlns="http://earth.google.com/kml/2.2">\n');
fprintf(fid, '<Document>\n');
fprintf(fid, '\t<Visibility>1</Visibility>\n');
fprintf(fid, '\t<DocumentSource>ADU Blablabla</DocumentSource>\n');
for i=1:size(MultiBanc,2)
fprintf(fid, '\t<Placemark>\n');
% Ajout Time Stamp
fprintf(fid, '\t\t<TimeStamp>\n');
fprintf(fid, '\t\t\t<when>%s</when>\n', dateStr{i});
fprintf(fid, '\t\t</TimeStamp>\n');

fprintf(fid, '\t\t<name>Mesh</name>\n');
fprintf(fid, '\t\t<Style id="examplePolyStyle">\n');
fprintf(fid, '\t\t\t<PolyStyle>\n');
fprintf(fid, '\t\t\t\t<color>775555ee</color>	 <!-- abgr -->\n');
fprintf(fid, '\t\t\t\t<outline>0</outline>\n');
fprintf(fid, '\t\t\t</PolyStyle>\n');
fprintf(fid, '\t\t</Style>\n');
fprintf(fid, '\t\t<MultiGeometry>\n');
for j=1:length(MultiBanc(i).Facettes)
    fprintf(fid, '\t\t\t<Polygon>\n');
    fprintf(fid, '\t\t\t\t<altitudeMode>absolute</altitudeMode>\n');
    fprintf(fid, '\t\t\t\t<outerBoundaryIs>\n');
    fprintf(fid, '\t\t\t\t\t<LinearRing>\n');
    fprintf(fid, '\t\t\t\t\t\t<coordinates>\n');
    fprintf(fid, '%15.12f, %15.12f, %15.12f\n', MultiBanc(i).Points(MultiBanc(i).Facettes(j,1),2),MultiBanc(i).Points(MultiBanc(i).Facettes(j,1),1),MultiBanc(i).Points(MultiBanc(i).Facettes(j,1),3));
    fprintf(fid, '%15.12f, %15.12f, %15.12f\n', MultiBanc(i).Points(MultiBanc(i).Facettes(j,2),2),MultiBanc(i).Points(MultiBanc(i).Facettes(j,2),1),MultiBanc(i).Points(MultiBanc(i).Facettes(j,2),3));
    fprintf(fid, '%15.12f, %15.12f, %15.12f\n', MultiBanc(i).Points(MultiBanc(i).Facettes(j,3),2),MultiBanc(i).Points(MultiBanc(i).Facettes(j,3),1),MultiBanc(i).Points(MultiBanc(i).Facettes(j,3),3));
    fprintf(fid, '\t\t\t\t\t\t</coordinates>\n');
    fprintf(fid, '\t\t\t\t\t</LinearRing>\n');
    fprintf(fid, '\t\t\t\t</outerBoundaryIs>\n');
    fprintf(fid, '\t\t\t</Polygon>\n');
end
fprintf(fid, '\t\t</MultiGeometry>\n');
fprintf(fid, '\t</Placemark>\n');
end
fprintf(fid, '</Document>\n');
fprintf(fid, '</kml>\n');

fclose(fid);