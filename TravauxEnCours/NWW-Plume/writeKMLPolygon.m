% Fonction writeKMLPolygon
% EXAMPLE:
% writeKMLPolygon('example_Polygon.kml', MPoints, KFacettes)
% ----------------------------------------------------------------------------

function writeKMLPolygon(nomFicKml, M, K)


fid = fopen(nomFicKml, 'w+', 'native', 'UTF-8');
if fid == -1
    str = sprintf(Lang('Impossible to cr�er Fichier KML %s', ...
        'Impossible to create KML config file %s'), nomFicXml);
    my_warndlg(str, 1)
    return
end

fprintf(fid, '<?xml version="1.0" encoding="utf-8"?>\n');
fprintf(fid, '<!--Fichier cree depuis la fonction writeKMLPolygon -->\n');
fprintf(fid, '<!--Auteur : GLU 19/02/2010-->\n');
fprintf(fid, '<kml xmlns="http://earth.google.com/kml/2.2">\n');
fprintf(fid, '<Document>\n');
fprintf(fid, '\t<Visibility>1</Visibility>\n');
fprintf(fid, '\t<DocumentSource>ADU Blablabla</DocumentSource>\n');
fprintf(fid, '\t<Placemark>\n');
fprintf(fid, '\t\t<name>Mesh</name>\n');
fprintf(fid, '\t\t<Style id="examplePolyStyle">\n');
fprintf(fid, '\t\t\t<PolyStyle>\n');
fprintf(fid, '\t\t\t\t<color>775555ee</color>	 <!-- abgr -->\n');
fprintf(fid, '\t\t\t\t<outline>0</outline>\n');
fprintf(fid, '\t\t\t</PolyStyle>\n');
fprintf(fid, '\t\t</Style>\n');
fprintf(fid, '\t\t<MultiGeometry>\n');
for i=1:length(K)
    fprintf(fid, '\t\t\t<Polygon>\n');
    fprintf(fid, '\t\t\t\t<altitudeMode>absolute</altitudeMode>\n');
    fprintf(fid, '\t\t\t\t<outerBoundaryIs>\n');
    fprintf(fid, '\t\t\t\t\t<LinearRing>\n');
    fprintf(fid, '\t\t\t\t\t\t<coordinates>\n');
    % Ecriture du 1er point (x, y, z) de la facette
    fprintf(fid, '%f, %f, %f\n', M(K(i, 1),1),M(K(i, 1),2),M(K(i, 1),3));
    % Ecriture du 3�me point(x, y, z) de la facette
    %fprintf(fid, '%f, %f, %f\n', M(K(i, 3),2),M(K(i, 3),1),M(K(i, 3),3));
    % Ecriture du 2�me point (x, y, z) de la facette
    fprintf(fid, '%f, %f, %f\n', M(K(i, 2),1),M(K(i, 2),2),M(K(i, 2),3));
    fprintf(fid, '%f, %f, %f\n', M(K(i, 3),1),M(K(i, 3),2),M(K(i, 3),3));
    fprintf(fid, '\t\t\t\t\t\t</coordinates>\n');
    fprintf(fid, '\t\t\t\t\t</LinearRing>\n');
    fprintf(fid, '\t\t\t\t</outerBoundaryIs>\n');
    fprintf(fid, '\t\t\t</Polygon>\n');
end
fprintf(fid, '\t\t</MultiGeometry>\n');
fprintf(fid, '\t</Placemark>\n');
fprintf(fid, '</Document>\n');
fprintf(fid, '</kml>\n');

fclose(fid);