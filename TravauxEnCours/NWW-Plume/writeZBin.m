function data = writeZBin

nomFic = 'Z:\SonarScope\Donnees WC\Echos\Liv_20100129\WC_NWW_0306_20091113_160950_Lesuroit\Z.txt';
nomFicZ = 'Z:\SonarScope\Donnees WC\Echos\Liv_20100129\WC_NWW_0306_20091113_160950_Lesuroit\Z.bin';
data = dlmread(nomFic);

fid = fopen(nomFicZ, 'w+');
for i=1:length(data)
fwrite(fid, data(i), 'double' );
end
fclose(fid);

%%
nomFic = 'Z:\SonarScope\Donnees WC\Echos\Liv_20100129\WC_NWW_0307_20091113_163950_Lesuroit\Z.txt';
nomFicZ = 'Z:\SonarScope\Donnees WC\Echos\Liv_20100129\WC_NWW_0307_20091113_163950_Lesuroit\Z.bin';
data = dlmread(nomFic);

fid = fopen(nomFicZ, 'w+');
for i=1:length(data)
fwrite(fid, data(i), 'double' );
end
fclose(fid);

