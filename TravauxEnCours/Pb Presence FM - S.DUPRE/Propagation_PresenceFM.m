% Correction du pb sur le Signal de Presence FM pour les traitements d'EI
% r�alis�s par S. DUPRE sur MARMESONET - LEG 1 et 2.
%
% SYNTAX :
%   flag = Propagation_PresenceFM(nomDirALL, varargin);

%
% INPUT PARAMETERS :
%   nomDirEchoIntegration   : nom du r�pertoire d'EI impact�.
%   customDirEI             : chaine particuli�re � concat�ner au chemin
%                               d'EI
%   nomDirALL               : nom du r�pertoire de pr�sence du fichier ALL
%
% OUTPUT PARAMETERS :
%   flag : 1 si ecriture reussie, 0 sinon
%
% EXAMPLES :
%     nomLeg      = 'leg1';
%     nomDirRoot  = '\\meskl2\edf\MARMESONET2'; % 'U:';
%     % nomFicALL   = '0001_20091104_180426_Lesuroit.all';
%     customDirEI = ['Comp' '_' lower(nomLeg) '_com_30_60_0_EI_PingAcrossDist'];
%     nomDirEI    = fullfile(nomDirRoot, 'STEPHANIE-TMP', 'EI', 'EI-vertical-Ecomp-30-60-0');
%     nomDirALL   = fullfile(nomDirRoot, nomLeg, 'ALL_WCD_data');
%     % Attention au nom de signal chez Stephanie.
%     Propagation_PresenceFM(nomDirALL, ...
%                             'nomDirEchoIntegration', nomDirEI, ...
%                             'customDirEI', customDirEI, ...
%                             'nomSignalWaveFormIdentifier', 'SignamWaveformIdentifier.bin');                    
%
% SEE ALSO  : cl_image Authors
% AUTHORS   : GLU
% -------------------------------------------------------------------------
function flag = Propagation_PresenceFM(nomDirALL, varargin)

    % Traitement de tous les fichiers et leur EI respectifs.
    listing         = dir(fullfile(nomDirALL,'*.all'));
    nbFic           = numel(listing);
    maxWaitBar      = 100;
    pasWaitBar      = nbFic/maxWaitBar;
    str             = sprintf('Traitement des signaux "Presence FM" des donn�es en cours, patience ...');
    hw              = create_waitbar(str, 'N', maxWaitBar);
    flag            = 0;
    profile on;
    
    % Traitement de chaque fichier recens�.
    for k=1:numel(listing)
        iWaitBar        = floor(k/pasWaitBar);
        my_waitbar(iWaitBar, maxWaitBar, hw);
        flag = Propagation_PresenceFM_unitaire(nomDirALL, listing(k).name, varargin{:});
        if ~flag
            disp(['Pb de propagation du signal Presence FM sur le fichier : ' fullfile(nomDirALL, listing(k).name)]);
        end
    end
    my_close(hw);
    
    profile report;
end

function flag = Propagation_PresenceFM_unitaire(nomDirALL, nomFicALL, varargin)

    global DEBUG

    flag = 0;
    [ppppdir, nomFicRootALL, ~] = fileparts(nomFicALL); %#ok<ASGLU>
    nomDirEI                = getPropertyValue(varargin, 'nomDirEchoIntegration', []);
    customDirEI             = getPropertyValue(varargin, 'customDirEI', []);
    nomFicWVFIRoot          = getPropertyValue(varargin, 'nomSignalWaveFormIdentifier', 'SignalWaveformIdentifier.bin');

    nomDirDatagram          = 'Ssc_RawRangeBeamAngle';
    nomFicPresenceFM        = 'PresenceFM.bin';
    nomFicPresenceFMSave    = 'PresenceFM_bug.bin';
    nomSignal               = {'AlongDist', 'AveragedPtsNb', 'Reflectivity', 'TxAngle', 'TxBeamIndexSwath'};
    tabColSignal            = {'yellow', 'magenta', 'red', 'green', 'blue'};
    tabMarkerSignal         = ['o', '*', '+', 's', 'd'];
    %%
    if DEBUG
        figure(1111); hold on;
    end
    for k=1:numel(nomSignal)
        fileName        = fullfile(nomDirEI, [nomFicRootALL '_' customDirEI '_' nomSignal{k}], nomFicPresenceFM);
        fileNameSave    = fullfile(nomDirEI, [nomFicRootALL '_' customDirEI '_' nomSignal{k}], nomFicPresenceFMSave);
        % Sauvegarde des fichiers PresenceFM.bin
        if ~exist(fileNameSave, 'file')
            % Recopie si le fichier de sauvegarde n'a pas d�j� �t� cr��e.
            copyfile(fileName, fileNameSave);
        end

        % Ouverture pour d�buggage.
        if DEBUG
            fid         = fopen(fileName, 'r');
            [PresenceFM, NPresenceFM] = fread(fid, 'single');
            PlotUtils.createSScPlot(PresenceFM, tabMarkerSignal(k), 'Color', tabColSignal{k}); title('PresenceFM');
            fclose(fid);
        end
    end


    %% Analyse du signal de cache de niveau 1
    %{
    nomDirDatagram  = {'ALL_RawRangeBeamAngle4eh', 'ALL_RawRangeBeamAngle66h'};

    nomFicWVFI = fullfile(nomDirALL, 'SonarScope', nomFicRootALL, nomDirDatagram{1}, nomFicWVFIRoot);
    if ~exist(nomFicWVFI, 'file')
        nomFicWVFI = fullfile(nomDirALL, 'SonarScope', nomFicRootALL, nomDirDatagram{2}, nomFicWVFIRoot);
        if ~exist(nomFicWVFI, 'file')
            my_warndlg(['Pas de fichier SignalWaveformIdentifier pour le fichier ALL : ' nomFicALL]);
            return
        end
    end

    % Lecture du fichier SignalWaveFormIdentifier
    fid = fopen(nomFicWVFI, 'r');
    [SignalWaveformIdentifier, NSignalWaveformIdentifier] = fread(fid, 'uint8=>uint8');
    SignalWaveformIdentifier = reshape(SignalWaveformIdentifier,8,860);
    if DEBUG
        figure; imagesc(SignalWaveformIdentifier'); title('Cache 1- SignalWaveformIdentifier');
    end
    fclose(fid);
    %}
    %% Analyse du signal de cache de niveau 2
    Datagrams = xml_read(fullfile(nomDirALL, 'SonarScope', nomFicRootALL, [nomDirDatagram '.xml']));
    nomFicWVFI = fullfile(nomDirALL, 'SonarScope', nomFicRootALL, nomDirDatagram, nomFicWVFIRoot);

    if ~exist(nomFicWVFI, 'file')
        my_warndlg(['Pas de fichier SignalWaveformIdentifier pour le fichier ALL : ' nomFicALL], 1);
        return
    end
    fid     = fopen(nomFicWVFI, 'r');

    [SignalWaveformIdentifier, NSignalWaveformIdentifier] = fread(fid, 'single');
    SignalWaveformIdentifier = reshape(SignalWaveformIdentifier,Datagrams.Dimensions.nbTx,Datagrams.Dimensions.nbPings);
    if DEBUG
        figure(4444); imagesc(SignalWaveformIdentifier'); title('Cache 2- SignamWaveformIdentifier');
    end
    fclose(fid);

    %% Analyse des Ping Counter
    nomFicPingCounterSsc    = 'PingCounter.bin';
    nomFicPingCounterEI     = 'PingCounter.bin';

    fileNamePCEI   = fullfile(nomDirEI, [nomFicRootALL '_' customDirEI '_' 'TxAngle'], nomFicPingCounterEI);
    fileNamePCSsc  = fullfile(nomDirALL, 'SonarScope', nomFicRootALL, nomDirDatagram, nomFicPingCounterSsc);

    % Chargement des indices de Ping.
    fid = fopen(fileNamePCEI, 'r');
    [EI_PC, NEI_PC] = fread(fid, 'single=>single');
    % EI_PC = reshape(EI_PC,8,860);
    if DEBUG
        figure(2222); PlotUtils.createSScPlot(EI_PC, '--b+'); title('EI_PC');
    end
    fclose(fid);

    fid = fopen(fileNamePCSsc, 'r');
    [Ssc_PC, NSsc_PC] = fread(fid, 'single=>single');
    % Ssc_PC = reshape(Ssc_PC,8,860);
    if DEBUG
        figure(33333); PlotUtils.createSScPlot(Ssc_PC, '--ro'); title('Ssc_PC');
    end
    fclose(fid);


    %% Intersection des N� de Pings
    [subSsc, subEI] = intersect3(Ssc_PC, EI_PC, EI_PC);

    PresenceFM  = NaN(NEI_PC, 1, 'single');
    pppp        = SignalWaveformIdentifier(:,subSsc);
    % PresenceFM = 1 si au moins un des secteurs est � 1.
    PresenceFM  = max(pppp(:,:));

    % R�percussion des nouveaux PresenceFM dans les layers d'EI.
    for k=1:numel(nomSignal)
        fileName        = fullfile(nomDirEI, [nomFicRootALL '_' customDirEI '_' nomSignal{k}], nomFicPresenceFM);
        %% Test d'acc�s en �criture
        pppp = fileattrib(fileName, 'UserWrite');
        if pppp == 0
            fileattrib(fileName, '+w')
        end
        %% Ouverture pour d�buggage.
        fid         = fopen(fileName, 'w');
        NPresenceFM = fwrite(fid, PresenceFM, 'single');
        fclose(fid);
    end
    
    flag = 1;
end