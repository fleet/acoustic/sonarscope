function correctWaveFormIdentifier

nomDir          = 'F:\SonarScopeData\Level2\Sonar';
nomFicSimrad    = '62mbn08172_raw';
nomDatagram     = 'ALL_RawRangeBeamAngle4eh';
nomDirSsc       = fullfile(nomDir, 'SonarScope', nomFicSimrad);
Datagrams       = xml_read(fullfile(nomDirSsc, [nomDatagram '.xml']));

%% Lecture du nombre de secteurs d'�mission.
nomSignalOrig   = 'NTx';
k = findIndVariable(Datagrams.Variables, nomSignalOrig);
if ~(isfield(Datagrams.Variables(k), 'Constant'))  
    [flag, NTx] = readSignal(fullfile(nomDirSsc, nomDatagram), Datagrams.Variables(k), 1);
else
    NTx = Datagrams.Variables(k).Constant;
end

%% Lecture du nombre de secteurs d'�mission.
nomSignalOrig   = 'SignalWaveformIdentifier';
k = findIndVariable(Datagrams.Tables, nomSignalOrig);
if isfield(Datagrams.Tables(k), 'Constant')
    if isempty(Datagrams.Tables(k).Constant)
        [flag, SWFI] = readSignal(fullfile(nomDirSsc, nomDatagram), Datagrams.Tables(k), ...
                                    NTx, 'NbColumns',Datagrams.NbDatagrams);
    else
        SWFI = Datagrams.Tables(k).Constant;
    end
end
figure; imagesc(SWFI); title('Signal Wave Form Identifier from Cache 1');
%% Lecture du 2�me niveau de cache pour analyse.
% 0 = CW, 1 = FM.
nomDatagram     = 'Ssc_RawRangeBeamAngle';
nomSignalOrig   = 'SignalWaveformIdentifier';
Datagrams       = xml_read(fullfile(nomDirSsc, [nomDatagram '.xml']));
k               = findIndVariable(Datagrams.Images, nomSignalOrig);
if isfield(Datagrams.Images(k), 'Constant')
    if isempty(Datagrams.Images(k).Constant)
        [flag, SWFI] = readSignal(nomDirSsc, Datagrams.Images(k), ...
                                    Datagrams.Dimensions.nbTx, 'NbColumns',Datagrams.Dimensions.nbPings);
    else
        SWFI = Datagrams.Images(k).Constant;
    end
end
figure; imagesc(SWFI); title('Signal Wave Form Identifier from Cache 2');
%% Boucle d'�crasement des fichiers Signal Wave Form Identifier de niveau 1 sur le niveau 2.

