%% Sur meskl2
nomLeg      = 'leg1';
nomDirRoot  = 'U:';  %'\\meskl2\edf\MARMESONET2'; 
% nomFicALL   = '0001_20091104_180426_Lesuroit.all';
customDirEI = ['Comp' '_' lower(nomLeg) '_com_30_60_0_EI_PingAcrossDist'];
nomDirEI    = fullfile(nomDirRoot, 'STEPHANIE-TMP', 'EI-Verticales-legs1-2');
nomDirALL   = fullfile(nomDirRoot, nomLeg, 'ALL_WCD_data');
% Attention au nom de signal chez Stephanie.
Propagation_PresenceFM(nomDirALL, ...
                        'nomDirEchoIntegration', nomDirEI, ...
                        'customDirEI', customDirEI, ...
                        'nomSignalWaveFormIdentifier', 'SignamWaveformIdentifier.bin');
%% En traitement local.
nomLeg      = '';
nomDirRoot  = 'F:\SonarScopeData\From S. DUPRE';
nomFicALL   = '0001_20091104_180426_Lesuroit.all';
nomDirALL   = nomDirRoot;
Propagation_PresenceFM(nomDirALL, nomFicALL, ...
                        'nomDirEchoIntegration', 'F:\SonarScopeData\From S. DUPRE', ...
                        'customDirEI', 'Raw_EI_PingAcrossDist');
