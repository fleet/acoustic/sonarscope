% M�thode de constrcution des fichiers de Texture � partir de traits de
% cot.
%
% SYNTAX :
%   a = cot2Raster(...)
% 
% PROPERTY NAMES / PROPERTY VALUES :
%   'ListeFicData' : nom du fichier de liste de donn�es.
%  
% OUTPUT PARAMETERS : 
%   f : indicateur de r�sultat.
%
% EXAMPLES :
%   nomFicData  = 'C:\SonarScopeTbx\TravauxEnCours\Placa3D\FicData.p3d'
%   flag        = cot2Raster('ListeFicData', nomFicData);
%
% SEE ALSO : PLACA3D Authors
% AUTHORS  : JMA-GLU
% ----------------------------------------------------------------------------
function flag = cot2Raster(varargin)

persistent persistent_RepDefautImage
persistent persistent_RepDefautCot persistent_RepExport

nomFicListeData = getPropertyValue(varargin, 'ListeFicData', []);
ListeFicPole    = {};
flag = 1; %#ok<NASGU>

if my_isdeployed
    loadVariablesEnv;
end

%% Lecture du nom de fichier de l'image de fond

if isempty(persistent_RepDefautImage)
    RepDefaut = my_tempdir;
else
    RepDefaut = persistent_RepDefautImage;
end

if isempty(nomFicListeData)
    flagModeBatch = 0;
else
    flagModeBatch = 1;
    [flag, nomFondCarte, ListeFicCot, ListeFicPole, persistent_RepExport] = read_batchFile(nomFicListeData);
    if isempty(ListeFicCot)
        return
    end
end

% Mode Manuel
if ~flagModeBatch
    strTitre = Lang('Veuillez indiquer votre fond de carte.', 'Please, input your background map.');
    [flag, nomFondCarte, RepDefaut] = uiSelectFiles('ExtensionFiles', {'.ers';'.tif';'.grd'; '.nc'}, 'AllFilters', 1, 'Entete', strTitre, 'RepDefaut', RepDefaut);
    if ~flag
        return
    end
    
    persistent_RepDefautImage = RepDefaut;
    if length(nomFondCarte) > 1
        str1 = 'Un seul fichier SVP.';
        str2 = 'Only one file please';
        my_warndlg(Lang(str1,str2), 1)
        return
    end
    nomFondCarte    = nomFondCarte{1};
end

%% Lecture du fichier Image
[flag, typeFichier] = identifyFileFormat(nomFondCarte, []);
if ~flag
    return
end

%% Lecture du fichier

I0 = cl_image_I0;
switch typeFichier
    case 'ERMAPPER'
        [flag, a] = import_ermapper(I0, nomFondCarte);
    case 'GEOTIFF'
        a = import_geotiff(I0, nomFondCarte);
    case 'GMT_cf'
        [a, flag] = import_gmt_old(I0, nomFondCarte);
    case 'GMT_nf'
        [a, flag] = import_gmt(I0, nomFondCarte);
    case 'GMT_NIWA'
        [a, flag] = import_gmt_niwa(I0, nomFondCarte);
    case 'GMT_LatLong'
        [a, flag] = import_gmt_LatLong(I0, nomFondCarte);
    case 'GMT_GEBCO'
        [a, flag] = import_gmt_Gebco(I0, nomFondCarte);
    case 'ASCII_XYZ'
        [flag, a] = import_xyz(I0, nomFondCarte);
    case 'EsriGridAscii'
        [flag, a] = import_EsriGridAscii(I0, nomFondCarte);
    case 'ASCII_CARIS'
        [typeFichier, ScaleFactor, GeometryType, DataType, IndedexColumns] = identifyFormatAscii(nomFondCarte); %#ok<ASGLU>
        [flag, a] = import_xyz(I0, nomFondCarte, ...
            'NbLigEntete', -1, 'nColumns', -4, 'IndedexColumns', IndedexColumns, ...
            'ScaleFactor', ScaleFactor, 'GeometryType', -GeometryType, ...
            'DataType', -DataType, 'TypeAlgo', 1);
    case 'HDR-FLT'
        [flag, a] = import_hdr(I0, nomFondCarte);
    otherwise
        str1 = sprintf('Ce fichier est de type "%s", ce qui n''est pas pr�vu dans cet import.', typeFichier);
        str2 = sprintf('This file is a "%s" type, this is not set in this import program.', typeFichier);
        my_warndlg(Lang(str1,str2), 1)
end
if ~flag
    my_warndlg(Lang('Le fichier de fond de carte n''est pas exploitable.', 'Background file is not runnable'), 1);
    return
end

%% Lecture des noms de fichiers des traits de cotes

if isempty(persistent_RepDefautCot)
    RepDefaut = my_tempdir;
else
    RepDefaut = persistent_RepDefautCot;
end

% Mode Manuel
if ~flagModeBatch
    strTitre = Lang('Veuillez indiquer votre contours de Plaque.', 'Please, input your cot Files.');
    [flag, ListeFicCot, RepDefaut] = uiSelectFiles('ExtensionFiles', '.cot', 'AllFilters', 1, 'Entete', strTitre, 'RepDefaut', RepDefaut);
    if ~flag
        return
    end
    persistent_RepDefautCot = RepDefaut;
end
%% Lecture du r�pertoire o� vont �tre cr��es les fichiers de sortie

if isempty(persistent_RepExport)
    RepExport = persistent_RepDefautCot;
else
    RepExport = persistent_RepExport;
end
%
% Mode Manuel
if ~flagModeBatch
    str1 = 'Choisissez un r�pertoire pour les fichiers SSc3DV.';
    str2 = 'Pick a Directory for SSc3DV output files.';
    RepExport = uigetdir(RepExport, Lang(str1,str2));
    
    persistent_RepExport = RepExport;
end
%% Traitement des fichiers

for k=1:length(ListeFicCot)
    a = set(a, 'RegionOfInterest', []);
    
    a       = import_coastLinesFile(a, ListeFicCot{k});
    [M, a]  = masque_fromContours(a, 1, 'Mask', []);
    ROI     = get(M, 'RegionOfInterest'); % ROI = Segments tectoniques
    nbROI   = numel(ROI);
    
    [XLim, YLim] = getXLimYLimForCrop_image(M);
    Me = extraction(M, 'XLim', XLim, 'YLim', YLim);
    clear M
    Im = extraction(a, 'XLim', XLim, 'YLim', YLim);
    
    [~, nomContinent] = fileparts(ListeFicCot{k});
    
    for j=1:nbROI
        J = masquage(Im, 'LayerMasque', Me, 'valMasque', j);
        % On n'utilise pas les noms des ROI faute de coh�rence dans les fichiers .cot
        nomSegment = [nomContinent num2str(j)];
        % Export de l'image
        nomFic = fullfile(RepExport, nomContinent);
        [flag, RepExport] = exportGeotiffSonarScope3DViewerMulti(J, 'sizeTiff', 2500000, 'repExport', RepExport, ...
            'fileExport', nomFic, 'FileName3DV', nomSegment);
        if ~flag
            return
        end
   end
    
    
    
    % Export du mask (demande de Pascal)
    nomFic = fullfile(RepExport, [nomContinent '_Mask']);
    nomFicMask = [nomContinent '_Mask'];
    [flag, RepExport] = exportGeotiffSonarScope3DViewerMulti(Me, 'sizeTiff', 2500000, ...
                        'repExport', RepExport, 'fileExport', nomFic, 'FileName3DV', nomFicMask);
    if ~flag
        return
    end
    persistent_RepExport = RepExport;
    
    % Ecriture du fichier Chapeau XML
    nomFicXML       = [nomContinent '_tectonic.xml'];
    if ~isempty(ListeFicPole)
        nomFicPoles     = ListeFicPole{k};
        nomFicPoles     = nomFicPoles(2:end);
    else    
        strTitre = Lang('Veuillez indiquer votre fichier de p�les.', 'Please, input your Poles file.');
        [flag, nomFicPoles, RepDefaut] = uiSelectFiles('ExtensionFiles', {'.PolInter';'.PolTot';'.tot'; '.int';'.total'; '.inter'}, 'AllFilters', 1, 'Entete', strTitre, 'RepDefaut', RepDefaut);
        if ~flag
            return
        end
        nomFicPoles = nomFicPoles{1};
    end
    % Cr�ation du fichier XML
    flag            = writeTectonicXML(RepExport, nomFicXML, nomContinent, nomFicPoles, nbROI);
    dirTectonic     = fullfile(RepExport,[nomContinent '_tectonic']);
    % D�placement (apr�s cr�ation) des diff�rents �l�ments dans le r�pertoire chapeau.
    mkdir(dirTectonic);
    % Masque
    nomFicMask      = [nomContinent '_Mask_texture_v2.xml'];
    movefile(fullfile(RepExport, nomFicMask), dirTectonic);
    nomDirMask      = [nomContinent '_Mask_texture_v2'];
    movefile(fullfile(RepExport, nomDirMask), dirTectonic);
    % Elevation
    for j=1:nbROI
        nomSegment = [nomContinent num2str(j)];
        nomFicTexture   = [nomSegment '_elevation_v2.xml'];
        movefile(fullfile(RepExport, nomFicTexture), dirTectonic);
        nomDirTexture   = [nomSegment '_elevation_v2'];
        movefile(fullfile(RepExport, nomDirTexture), dirTectonic);
    end
    % Simple copie du Fichier Pole
    copyfile(nomFicPoles, dirTectonic);
    if ~flag
        return
    end
    
end