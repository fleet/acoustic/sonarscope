function [flag, nomFondCarte, listeCot, listePol, repExport] = read_batchFile(nomFic)

flag = 1;
fid = fopen(nomFic);
if fid < 0
    flag = 0;
    return
end
listeCot    = [];
listePol    = [];
iLine       = 1;
while 1
    tline = fgetl(fid);
    if ~ischar(tline)
        break
    end
    if iLine == 2
        nomFondCarte = tline;
    elseif iLine == 4
        repExport = tline;
    elseif iLine > 5
        [cotFile, poleFile] = strtok(tline, char(9));
        if ~strcmp(cotFile(1), '#')
            listeCot{end+1} = cotFile; %#ok<AGROW>
            listePol{end+1} = poleFile; %#ok<AGROW>            
        end
    end
    iLine = iLine + 1;
end
fclose(fid);