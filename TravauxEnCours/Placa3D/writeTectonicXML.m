function flag = writeTectonicXML(repExport, nomFicXml, nomContinent, nomFicPoles, nbROI)

flag = 0;

%% Chemin en relatif pris en compte dans SSC-3DViewer

nomFicXml = fullfile(repExport, nomFicXml);
[nomDirXml, racineNomFic] = fileparts(nomFicXml); %#ok<ASGLU>
fid = fopen(nomFicXml, 'w+');
if fid == -1
    str = sprintf(Lang('Placa3D_createXML :  Informations relatives � la cr�ation de Plaques : Impossible to cr�er Fichier XML %s', ...
        'Placa3D_createXML : Impossible to create XML informations Cot and Placa file %s'), nomFicXml);
    my_warndlg(str, 1)
    return
end

[nomDirPoles, racineNomFicPoles, extPoles] = fileparts(nomFicPoles); %#ok<ASGLU>

%% Ecriture du fichier XML

fprintf(fid, '<?xml version="1.0" encoding="UTF-8"?>\n');
fprintf(fid, '<LayerSet Name="%s" ShowOnlyOneLayer="false" ShowAtStartup="false" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="LayerSet.xsd">\n', racineNomFic);
fprintf(fid, '\t\t<PermanentDirectory>%s</PermanentDirectory>\n', [nomContinent '_tectonic']);
fprintf(fid, '\t\t<RotationPole><PathFile>%s</PathFile></RotationPole>\n', [racineNomFicPoles extPoles]);
for k=1:nbROI
    nomFicTexture = [nomContinent num2str(k) '_elevation_v2.xml'];
    fprintf(fid, '\t\t<Texture><PathFile>%s</PathFile></Texture>\n', nomFicTexture);
end
fprintf(fid, '</LayerSet>\n');
fclose(fid);

%%
flag = 1;
