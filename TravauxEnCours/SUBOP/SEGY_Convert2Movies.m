addpath('C:\Documents and Settings\augustin\My Documents\SEGY\S4M\Geophysics_3.0')

filename = 'C:\Temp\Marmesonet\Cube_Marmara\hr3d\Study\Marmara\cube\cube_jma.seg';
[seismic, text_header, binary_header] = read_segy_file(filename);

seismic.header_info

sz = [binary_header(8) binary_header(1) binary_header(2)];
V1 = reshape(seismic.traces, sz);


%% Dim 3

nomFicAvi = 'C:\Temp\Marmesonet\Cube_Marmara\hr3d\Study\Marmara\cube\cube_jmax_3.avi';
[flag, mov, nomFicAvi] = create_movie(nomFicAvi);
if ~flag
    return
end

Fig = FigUtils.createSScFigure('Name', ' - Seismic display - SonarScope - IFREMER');
ScreenSize = get(0,'ScreenSize');
Position = floor([10 40 ScreenSize(3)-20 ScreenSize(4)-140]/4)*4;
set(Fig, 'Position', Position);

for k=1:sz(3)
    I = squeeze(V1(:,:,k));
    I = uint8(quantify(I, 'rangeIn', [-13000 13000]));
    imagesc(I); colormap gray; axis equal; axis tight
    title(sprintf('sz = %s : %d / %d', num2strCode(sz), k, sz(3)))
    
    set(Fig, 'Position', Position);
    I =  getframe(Fig);
    I = I.cdata;
    % OLD mov = addframe(mov, I);
    writeVideo(mov,I); % FORGE 353
end
close(mov);  % FORGE 353


%% Dim 2

nomFicAvi = 'C:\Temp\Marmesonet\Cube_Marmara\hr3d\Study\Marmara\cube\cube_jmax_2.avi';
[flag, mov, nomFicAvi] = create_movie(nomFicAvi);
if ~flag
    return
end

Fig = FigUtils.createSScFigure('Name', ' - Seismic display - SonarScope - IFREMER');
ScreenSize = get(0,'ScreenSize');
Position = floor([10 40 ScreenSize(3)-20 ScreenSize(4)-140]/4)*4;
set(Fig, 'Position', Position);

for k=1:sz(2)
    I = squeeze(V1(:,k,:));
    I = uint8(quantify(I, 'rangeIn', [-13000 13000]));
    imagesc(I); colormap gray; axis equal; axis tight
    title(sprintf('sz = %s : %d / %d', num2strCode(sz), k, sz(2)))
    
    set(Fig, 'Position', Position);
    I =  getframe(Fig);
    I = I.cdata;
    % OLD mov = addframe(mov, I);
    writeVideo(mov,I); % FORGE 353
end
close(mov); 



%% Dim 1
nomFicAvi = 'C:\Temp\Marmesonet\Cube_Marmara\hr3d\Study\Marmara\cube\cube_jmax_1.avi';
[flag, mov, nomFicAvi] = create_movie(nomFicAvi);
if ~flag
    return
end

Fig = FigUtils.createSScFigure('Name', ' - Seismic display - SonarScope - IFREMER');
ScreenSize = get(0,'ScreenSize');
Position = floor([10 40 ScreenSize(3)-20 ScreenSize(4)-140]/4)*4;
set(Fig, 'Position', Position);

for k=1:sz(1)
    I = squeeze(V1(k,:,:));
    I = uint8(quantify(I, 'rangeIn', [-13000 13000]));
    imagesc(I); colormap gray; axis equal; axis tight
    title(sprintf('sz = %s : %d / %d', num2strCode(sz), k, sz(1)))
    
    set(Fig, 'Position', Position);
    I =  getframe(Fig);
    I = I.cdata;
    % OLD mov = addframe(mov, I);
    writeVideo(mov,I); % FORGE 353
end
close(mov);


