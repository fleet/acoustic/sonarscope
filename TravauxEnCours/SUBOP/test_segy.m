function [seismic] = test_segy(fname)
%function [seismic] = test_segy(fname)
%--------------------------------------------------------------------------
% INPUT  : fname, nom du fichier � lire
% OUTPUT : seismic, structure qui contient : 
%   - PingNumber:      num�ro de tir 
%   - LineNumber:      num�ro de profil
%   - RxZOffset:       profondeur du r�cepteur (m)
%   - MRUDepth:        profondeur de la r�f�rence (m)
%   - TxZOffset:       profondeur de la source (m)
%   - WaterDepthAtTx:  profondeur d'eau � l'�mission (m)
%   - WaterDepthAtRx:  profondeur d'eau � la r�ception (m)
%   - TxLon:           longitude � l'�mission (�)
%   - TxLat:           latitude � l'�mission (�)
%   - RxLon:           longitude � la r�ception (�)
%   - RxLat:           latitude � la r�ception (�)
%   - RecordingLag:    d�lai d'enregistrement (ms)
%   - CdpLon:          longitude du point milieu (�)
%   - CdpLat:          latitude du point milieu (�)
%   - WaterVelocity:   vitesse de propagation dans l'eau (m/s)
%   - SampleFreq:      fr�quence d'acquisition (Hz)
%   - LowCutFrq:       filtre passe haut � la r�ception (Hz)
%   - HighCutFrq:      filtre passe bas � la r�ception (Hz)
%   - LowCutSlope:     pente du filtre passe haut (dB/octave)
%   - HighCutSlope:    pente du filtre passe bas (dB/octave)
%   - Year:            ann�e
%   - Day:             jour (jour julian)
%   - Time:            temps depuis minuit (s)
%   - RxGain:          gain en r�ception (gain multiplicatif)
%   - TxLevel:         niveau d'�mission (%)
%   - StartChirpFrq:   fr�quence de d�but de chirp (Hz)
%   - EndChirpFrq:     fr�quence de fin de chirp (Hz)
%   - ChirpLength:     dur�e du chirp (ms)
%   - NbSamples:       nombre d'�chantillons par tir
%   - NbPings:         nombre de tirs
%   - Traces:          donn�es sismiques
%--------------------------------------------------------------------------

[seismic_data,text_header,binary_header] = read_segy_file(fname,...
    {'headers', {'WeatheringVelocity',       91,2,'m/s','WeatheringVelocity (91-92)'},...
                {'SampleInterval',           117,2,'�s','SampleInterval (117-118)'},...
                {'InstrumentGainConstant',   121,2,'dB','InstrumentGainConstant (121-122)'},...
                {'LowCutFrq',                149,2,'Hz','LowCutFrq (149-150)'},...
                {'HighCutFrq',               151,2,'Hz','HighCutFrq (151-152)'},...
                {'LowCutSlope',              153,2,'dB/octave','LowCutSlope (153-154)'},...
                {'HighCutSlope',             155,2,'dB/octave','HighCutSlope (155-156)'},...
                {'YearDataRecorded',         157,2,'y','YearDataRecorded (157-158)'},...
                {'Day',                      159,2,'d','Day (159-160)'},...
                {'Hour',                     161,2,'h','Hour (161-162)'},...
                {'Minute',                   163,2,'m','Minute (163-164)'},...
                {'Second',                   165,2,'s','Second (165-166)'},...
                {'SourceMeasurementMantisse',225,4,'n/a','SourceMeasurementMantisse (225-228)'},... 
                {'SourceMeasurementExponent',229,2,'n/a','SourceMeasurementExponent (229-230)'},... 
                {'Millisecond',              233,2,'ms','Millisecond (233-234)'}});

%--------------------------------------------------------------------------
seismic = finalize(seismic_data,binary_header);

%--------------------------------------------------------------------------
function ddec = convert_dms2ddec(dms)
signe   = sign(dms);
dms     = abs(dms);
d       = floor(dms/1e6);
m       = floor((dms - d*1e6) /1e4);
s       = (dms - d*1e6 - m*1e4) /1e2;
ddec    = signe .*(d + m/60 + s/3600);

%--------------------------------------------------------------------------
function [seismic] = finalize(seismic_data,binary_header)

ind=zeros(size(seismic_data.header_info));
for i=1:size(seismic_data.header_info,1)
    if     strcmp(seismic_data.header_info(i,1),'ds_seqno'),  ind(1)=i;   %Trace sequence number within line (1-4)
    elseif strcmp(seismic_data.header_info(i,1),'ffid'),      ind(2)=i;    %Original Field record number (9-12)
    elseif strcmp(seismic_data.header_info(i,1),'rec_elev'),  ind(3)=i;    %Receiver elevation (41-44);
    elseif strcmp(seismic_data.header_info(i,1),'sou_elev'),  ind(4)=i;    %Surface elevation at source (45-48)
    elseif strcmp(seismic_data.header_info(i,1),'depth'),     ind(5)=i;    %Source depth below surface (49-52)
    elseif strcmp(seismic_data.header_info(i,1),'sou_h2od'),  ind(6)=i;    %Water depth at source (61-64)
    elseif strcmp(seismic_data.header_info(i,1),'rec_h2od'),  ind(7)=i;    %Water depth at receiver group (65-68)
    elseif strcmp(seismic_data.header_info(i,1),'sou_x'),     ind(8)=i;    %X coordinate of source (73-76)
    elseif strcmp(seismic_data.header_info(i,1),'sou_y'),     ind(9)=i;    %Y coordinate of source (77-80)
    elseif strcmp(seismic_data.header_info(i,1),'rec_x'),     ind(10)=i;   %X coordinate of receiver (81-84)
    elseif strcmp(seismic_data.header_info(i,1),'rec_y'),     ind(11)=i;   %Y coordinate of receiver (85-88)
    elseif strcmp(seismic_data.header_info(i,1),'lag'),       ind(12)=i;   %Lag time between shot and recording start in ms (109-110)
    elseif strcmp(seismic_data.header_info(i,1),'cdp_x'),     ind(13)=i;   %X coordinate of CDP (181-184)
    elseif strcmp(seismic_data.header_info(i,1),'cdp_y'),     ind(14)=i;   %Y coordinate of CDP (185-188)
    elseif strcmp(seismic_data.header_info(i,1),'WeatheringVelocity'),        ind(15)=i;    %WeatheringVelocity (91-92)
    elseif strcmp(seismic_data.header_info(i,1),'SampleInterval'),            ind(16)=i;    %SampleInterval (117-118)
    elseif strcmp(seismic_data.header_info(i,1),'InstrumentGainConstant'),    ind(17)=i;    %InstrumentGainConstant (121-122)
    elseif strcmp(seismic_data.header_info(i,1),'LowCutFrq'),                 ind(18)=i;    %LowCutFrq (149-150)
    elseif strcmp(seismic_data.header_info(i,1),'HighCutFrq'),                ind(19)=i;    %HighCutFrq (151-152)
    elseif strcmp(seismic_data.header_info(i,1),'LowCutSlope'),               ind(20)=i;    %LowCutSlope (153-154)
    elseif strcmp(seismic_data.header_info(i,1),'HighCutSlope'),              ind(21)=i;    %HighCutSlope (155-156)
    elseif strcmp(seismic_data.header_info(i,1),'YearDataRecorded'),          ind(22)=i;	%YearDataRecorded (157-158)
    elseif strcmp(seismic_data.header_info(i,1),'Day'),                       ind(23)=i;	%Day (159-160)
    elseif strcmp(seismic_data.header_info(i,1),'Hour'),                      ind(24)=i;	%Hour (161-162)
    elseif strcmp(seismic_data.header_info(i,1),'Minute'),                    ind(25)=i;	%Minute (163-164)
    elseif strcmp(seismic_data.header_info(i,1),'Second'),                    ind(26)=i;	%Second (165-166)
    elseif strcmp(seismic_data.header_info(i,1),'SourceMeasurementMantisse'), ind(27)=i;	%SourceMeasurementMantisse (225-228) 
    elseif strcmp(seismic_data.header_info(i,1),'SourceMeasurementExponent'), ind(28)=i;	%SourceMeasurementExponent (229-230) 
    elseif strcmp(seismic_data.header_info(i,1),'Millisecond'),               ind(29)=i;	%Millisecond (233-234)
    end
end

if ind(1)~=0,  seismic.PingNumber       = seismic_data.headers(ind(1),:); end
if ind(2)~=0,  seismic.LineNumber       = seismic_data.headers(ind(2),1); end
if ind(3)~=0,  seismic.RxZOffset        = seismic_data.headers(ind(3),:)/100; end
if ind(4)~=0,  seismic.MRUDepth         = seismic_data.headers(ind(4),:)/100; end
if ind(5)~=0,  seismic.TxZOffset        = seismic_data.headers(ind(5),:)/100; end
if ind(6)~=0,  seismic.WaterDepthAtTx   = seismic_data.headers(ind(6),:)/100; end
if ind(7)~=0,  seismic.WaterDepthAtRx   = seismic_data.headers(ind(7),:)/100; end
if ind(8)~=0,  seismic.TxLon            = convert_dms2ddec(seismic_data.headers(ind(8),:)); end
if ind(9)~=0,  seismic.TxLat            = convert_dms2ddec(seismic_data.headers(ind(9),:)); end
if ind(10)~=0,  seismic.RxLon           = convert_dms2ddec(seismic_data.headers(ind(10),:)); end
if ind(11)~=0,  seismic.RxLat           = convert_dms2ddec(seismic_data.headers(ind(11),:)); end
if ind(12)~=0,  seismic.RecordingLag    = seismic_data.headers(ind(12),:); end
if ind(13)~=0,  seismic.CdpLon          = convert_dms2ddec(seismic_data.headers(ind(13),:)); end
if ind(14)~=0,  seismic.CdpLat          = convert_dms2ddec(seismic_data.headers(ind(14),:)); end
if ind(15)~=0,  seismic.WaterVelocity   = seismic_data.headers(ind(15),:); end
if ind(16)~=0,  seismic.SampleFreq      = 1e6/seismic_data.headers(ind(16),1); end
if ind(18)~=0,  seismic.LowCutFrq       = seismic_data.headers(ind(18),1); end
if ind(19)~=0,  seismic.HighCutFrq      = seismic_data.headers(ind(19),1); end
if ind(20)~=0,  seismic.LowCutSlope     = seismic_data.headers(ind(20),1); end
if ind(21)~=0,  seismic.HighCutSlope    = seismic_data.headers(ind(21),1); end
if ind(22)~=0,  seismic.Year            = seismic_data.headers(ind(22),:); end
if ind(23)~=0,  seismic.Day             = seismic_data.headers(ind(23),:); end
if ind(24)*ind(25)*ind(26)*ind(27)~=0,  seismic.Time  = seismic_data.headers(ind(24),:)*60*60 + ...
                                                        seismic_data.headers(ind(25),:)*60 + ...
                                                        seismic_data.headers(ind(26),:) + ...
                                                        seismic_data.headers(ind(29),:)/100; 
elseif ind(24)*ind(25)*ind(26)~=0,      seismic.Time  = seismic_data.headers(ind(24),:)*60*60 + ...
                                                        seismic_data.headers(ind(25),:)*60 + ...
                                                        seismic_data.headers(ind(26),:);
end
if ind(17)~=0,  seismic.RxGain = 10^(seismic_data.headers(ind(17),1)/20); end
if ind(27)*ind(28)~=0,  seismic.TxLevel = seismic_data.headers(ind(27),1) * 10^(seismic_data.headers(ind(28),1));end

seismic.StartChirpFrq       = binary_header(14);
seismic.EndChirpFrq         = binary_header(15);
seismic.ChirpLength         = binary_header(16);
seismic.NbSamples           = binary_header(8);
seismic.NbPings             = size(seismic_data.traces,2);
seismic.Traces              = seismic_data.traces;
