%%
nomFic = 'C:\SonarScopeTbxGLU\Test RDI from JMA\Test export Matlab';
S = load(nomFic);

struct(S)
%%
% Affichage de la directivité
sub = (S.SerDir10thDeg == -32768);
pppp = S.SerDir10thDeg;
pppp(sub) = 0;
figure; imagesc(pppp);
title('SerDir10thDeg');

%%
% Affichage de S.SerC1cnt
figure; imagesc(S.SerC1cnt)
title('SerC1cnt');
