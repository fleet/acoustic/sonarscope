% Exploitation de trames RDI pour affichage vertical sous NWW.
% SYNTAXE :
% [flag] = test_RDI2SSC3DV
%
% EXAMPLE sous DOS:
%   test_RDI2SSC3DV
% -----------------------------------------------------------------------

function flag = test_RDI2SSC3DV(varargin)
% [ListeFic,PathName] = uigetfile({'*.odv', '.odv';'*.txt', '.txt';'*.dat', '.dat';'*.xyz', '.xyz'},'Choisissez un fichier ODV, SVP .', 'F:\SonarScopeData\From C.Vrignaud\Gibraltar\Data\ADCP', ...
%                                 'MultiSelect', 'on');
flag = 1;
AskValNaN       = getPropertyValue(varargin, 'AskValNaN',         []);
ValNaN          = getPropertyValue(varargin, 'ValNaN', -32768);
ListeFic{1}          = 'C:\SonarScopeTbxGLU\Test RDI from JMA\Test export Matlab';
[PathName, ~, ~]     = fileparts(ListeFic{1});
if ~isempty(ListeFic)
    if ischar(ListeFic)
        ListeFic = {ListeFic};
    end
else
    return
end


%%
nbFics = length(ListeFic);
if nbFics > 1
    hw = create_waitbar(Lang('Importation de fichier, patience ...', 'Importing files, please wait ...'));
else
    hw = [];
end

%% Traitement des N fichiers s�lectionn�s.
for i=1:nbFics
    my_waitbar(i, nbFics, hw)
    
    % Importation des images
    if isa(ListeFic, 'double')
        return
    else
        if isempty(ListeFic{i})
            continue
        end
    end
    strFR       = sprintf('Importation de "%s"', [ListeFic{i}]);
    strUS       = sprintf('Importing "%s"', [ListeFic{i}]);
    WorkInProgress(Lang(strFR,strUS))
    
    if AskValNaN
        if isempty(ValNaN)
            [ValNaN, flag] = my_inputdlg(Lang({'Sp�cifier la valeur pour "NO-DATA" si besoin'},{'Specific value for "No Data" if any'}), {''});
            if ~flag
                return
            end
            ValNaN = ValNaN{1};
            if ~isempty(ValNaN)
                if strcmpi(ValNaN, 'NaN')
                    ValNaN = [];
                else
                    ValNaN = str2double(ValNaN);
                end
            end
        end
    end
    
    % ---------------------------------------------
    % Extraction des donn�es.
    [nomFicSeul, ~] = strtok(ListeFic{i}, '.');
     
    % Recherche des indices de colonne pour les valeurs � s�lectionner et
    % celles syst�matiquement r�cup�r�es depuis le fichier.

    selected.String   = {...
                'EIBeam1';'EIBeam2';'EIBeam3';'EIBeam4';'EIAve';...
                'CorBeam1';'CorBeam2';'CorBeam3';'CorBeam4';'CorAve';'SpeedE';
                'SpeedN';'SpeedV';'SpeedErr';'Mag';'Dir'};
    selected.SearchString   = {...
                'SerEA1cnt';'SerEA2cnt';'SerEA3cnt';'SerEA4cnt';'SerEAAcnt';...
                'SerC1cnt';'SerC2cnt';'SerC3cnt';'SerC4cnt';'SerCAcnt';'SerEmmpersec';
                'SerNmmpersec';'SerVmmpersec';'SerErmmpersec';'SerMagmmpersec';'SerDir10thDeg'};
    selected.Label          = {'Echo Intensit� Beam 1';'Echo Intensit� Beam 2';'Echo Intensit� Beam 3';...
                               'Echo Intensit� Beam 4';'Echo Intensit� moyenne sur les 4 beams';...
                                'corr�lation beam 1';'corr�lation beam 2';'corr�lation beam 3';...
                                'corr�lation beam 4';'Corr�lation moyenne';'Vitesse courant Est en mm/s';...
                                'Vitesse courant Nord en mm/s';'Vitesse courant verticale en mm/s';...
                                'Erreur de vitesse en mm/s';'Magnitude courant en mm/s';...
                                'Direction courant en 1/10eme de �'};
    selected.Unit          = {'';'';'';...
                               '';'';...
                                '';'';'';...
                                '';'';'mm/s';...
                                'mm/s';'mm/s';...
                                'mm/s';'mm/s';...
                                '0.1�'};
   

    % Indices dans le fichier : dans l'ordre, Dir, Mag et Echo Int.
    DataToDisplay       = my_listdlg(Lang('Donn�e � afficher : ', 'Data to display : '), selected.Label, ...
                        'SelectionMode', 'multiple');
    if isempty(DataToDisplay)
        return
    end
    
    [NomFicXML,~]       = uiputfile(fullfile(PathName, [nomFicSeul '.xml']),Lang('Sauver le fichier XML', 'Save file name'));
    [~, nomFicSeul, ~]  = fileparts(NomFicXML);
    if NomFicXML == 0
        return
    end
    
    DataRDI = load(ListeFic{1});
   
    
    % Cr�ation de l'image depuis les donn�es.
    pppp        = selected.SearchString(DataToDisplay);
    I           = DataRDI.(pppp{:});
    
    
    % Mise en forme des coordonn�es.
    % Immersion en positif.
    ZOffset         = DataRDI.RDIBin1Mid;
    Data.ZBotDepth  = ones(size(DataRDI.AnFLatDeg,1),1)*(ZOffset + size(DataRDI.AnFLatDeg,1)*DataRDI.RDIBinSize);
    Data.ZTopDepth  = ones(size(DataRDI.AnFLatDeg,1),1)*ZOffset;
    Data.LatI       = DataRDI.AnFLatDeg;
    Data.LonI       = DataRDI.AnFLonDeg;

    % Traitement des NaN
    sub     = (I==ValNaN);
    I(sub)  = NaN;
    
    Data.I 	= I';
    clear I
    
    Data.RDIFileName    = DataRDI.RDIFileName;
    if ~flag
        return
    end
    for t=1:numel(DataToDisplay)
        LouisADCP3DViewerV2_unitaire(Data, PathName, nomFicSeul, selected.String{DataToDisplay(t)});        
    end
end
%
if ishandle(hw)
    close(hw)
end

%%
% Fonction unitaire de traitement de donn�e verticale.
function flag = LouisADCP3DViewerV2_unitaire(DataIn, Path, racineFicName, TypeData)


% nbPings  = length(LatI);
nbProfils  = length(DataIn.LatI);

map        = jet(256);

switch TypeData
    case 'Temp'
        CLim = [10 20];
        X = Temp;
    case 'Salt'
        CLim = [35.25 35.70];
        X = Salt;
    case 'Fluo'
        CLim = [0 13];
        X = Fluo;
        
    case {'EIBeam1';'EIBeam2';'EIBeam3';'EIBeam4';'EIAve';...
          'CorBeam1';'CorBeam2';'CorBeam3';'CorBeam4';'CorAve';'SpeedE';
          'SpeedN';'SpeedV';'SpeedErr';'Mag';'Dir'}     
        maxZI   = ceil(max(DataIn.I(:)));
        minZI   = fix(min(DataIn.I(:)));
        CLim    = [minZI maxZI];
        map     = gray(256);
        map     = flipud(map);
        strFR    = sprintf('%s\n%s', 'Voulez-vous redefinir le contraste ?', 'Si oui, une fen�tre SonarScope va s''ouvrir afin que vous r�gleriez le contraste. N''oubliez pas de faire "Save" avant de quitter. Le m�me rehaussement de contraste sera utilis� pour toutes les images.');
        strUS    = sprintf('%s\n%s', 'Do you want to redefine the contrast ?', 'If you answer yes, a SonarScope window is going to be created, please fix the contrast and click on "Save" before "Quit". The save contrast will be used for all the images.');
        [rep, validation] = my_questdlg(Lang(strFR, strUS), 'Init', 1);
        if validation == 1
            if rep == 1
                a       = cl_image('Image', DataIn.I, 'ColormapIndex', 3, 'Video', 1);
                set(a, 'Titre', DataIn.RDIFileName);
                a       = SonarScope(a);
                CLim    = get(a, 'CLim');
                map     = get(a, 'Colormap');
                Video   = get(a, 'Video');
                if Video == 2
                    map = flipud(map);
                end
                X = get(a, 'Image');
            else
                X = DataIn.I;
            end
        else
            X = DataIn.I;
        end

end

%% Cr�ation du r�pertoire
NomFicXML = fullfile(Path, [racineFicName '_' TypeData '.xml']);
[nomDirXml, nomDirBin] = fileparts(NomFicXML);
nomDirXml = fullfile(nomDirXml, nomDirBin);
nomDirRoot = fileparts(nomDirXml);
if ~exist(nomDirXml, 'dir')
    flag = mkdir(nomDirXml);
    if ~flag
        messageErreurFichier(nomDirXml, 'WriteFailure')
        return
    end
end

%% Cr�ation du r�pertoire

nomDirPNG = fullfile(nomDirXml, '000');
if ~exist(nomDirPNG, 'dir')
    flag = mkdir(nomDirPNG);
    if ~flag
        messageErreurFichier(nomDirPNG, 'WriteFailure')
        return
    end
end

%% Cr�ation de l'image PNG

I = gray2ind(mat2gray(X, double(CLim)), size(map,1));
% I = flipud(I); % A confirmer
subNaN = isnan(X);
I(I == 0) = 1;
I(subNaN) = 0;

nomFic1 = '00000.png';
nomFic2 = fullfile(nomDirPNG, nomFic1);
imwrite(I, map, nomFic2, 'Transparency', 0)

%% Definition of the structure

Info.Signature1      = 'SonarScope';
Info.Signature2      = 'ImagesAlongNavigation';
Info.FormatVersion   = 20100624;
Info.Name            = nomDirBin;
Info.ExtractedFrom   = DataIn.RDIFileName;

Info.Survey         = 'Unknown';
Info.Vessel         = 'Unknown';
Info.Sounder        = 'Unknown';
Info.ChiefScientist = 'Unknown';


Info.Dimensions.nbPings  = nbProfils;
Info.Dimensions.nbSlices = 1;
Info.Dimensions.nbRows   = 0;
% Info.MiddleSlice         = 1;

Data.X              = X;
Data.AcrossDistance = 0;
Data.SliceExists    = true;
Data.Date           = NaN(1, nbProfils); % Data.Date !!! 
Data.Hour           = NaN(1, nbProfils);
Data.PingNumber     = 1:nbProfils;
Data.LatitudeNadir  = DataIn.LatI;
Data.LongitudeNadir = DataIn.LonI;
Data.LatitudeTop    = DataIn.LatI;
Data.LongitudeTop   = DataIn.LonI;
Data.LatitudeBottom    = DataIn.LatI;
Data.LongitudeBottom   = DataIn.LonI;
Data.DepthTop           =  -DataIn.ZTopDepth; % zeros(1, nbProfils); %
Data.DepthBottom        = -DataIn.ZBotDepth;


Info.Signals(1).Name          = 'Date';
Info.Signals(end).Dimensions  = '1, nbPings';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'Ifremer';
Info.Signals(end).FileName    = fullfile(nomDirBin,'Date.bin');

Info.Signals(end+1).Name      = 'Hour';
Info.Signals(end).Dimensions  = '1, nbPings';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'nb milliseconds since midnight';
Info.Signals(end).FileName    = fullfile(nomDirBin,'Hour.bin');

Info.Signals(end+1).Name      = 'PingNumber';
Info.Signals(end).Dimensions  = '1, nbPings';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = ' ';
Info.Signals(end).FileName    = fullfile(nomDirBin,'PingNumber.bin');

Info.Signals(end+1).Name      = 'LatitudeNadir';
Info.Signals(end).Dimensions  = '1, nbPings';
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile(nomDirBin, 'LatitudeNadir.bin');

Info.Signals(end+1).Name      = 'LongitudeNadir';
Info.Signals(end).Dimensions  = '1, nbPings';
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile(nomDirBin, 'LongitudeNadir.bin');

Info.Signals(end+1).Name      = 'SliceExists';
Info.Signals(end).Dimensions  = 'nbSlices, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile(nomDirBin,'SliceExists.bin');

Info.Signals(end+1).Name      = 'DepthTop';
Info.Signals(end).Dimensions  = '1, nbPings';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'm';
Info.Signals(end).FileName    = fullfile(nomDirBin, 'DepthTop.bin');

Info.Signals(end+1).Name      = 'DepthBottom';
Info.Signals(end).Dimensions  = '1, nbPings';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'm';
Info.Signals(end).FileName    = fullfile(nomDirBin, 'DepthBottom.bin');

Info.Signals(end+1).Name      = 'AcrossDistance';
Info.Signals(end).Dimensions  = 'nbSlices, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'm';
Info.Signals(end).FileName    = fullfile(nomDirBin, 'AcrossDistance.bin');

Info.Images(1).Name           = 'LatitudeTop';
Info.Images(end).Dimensions   = 'nbSlices, nbPings';
Info.Images(end).Storage      = 'double';
Info.Images(end).Unit         = 'deg';
Info.Images(end).FileName     = fullfile(nomDirBin, 'LatitudeTop.bin');

Info.Images(end+1).Name       = 'LongitudeTop';
Info.Images(end).Dimensions   = 'nbSlices, nbPings';
Info.Images(end).Storage      = 'double';
Info.Images(end).Unit         = 'deg';
Info.Images(end).FileName     = fullfile(nomDirBin,'LongitudeTop.bin');


%{
Nouveau format comme les HAC.


Info.Dimensions.nbPings  = nbProfils;
Info.Dimensions.nbSlices = 1;
Info.Dimensions.nbRows   = 1;
Info.MiddleSlice         = 1;

pppp = Data;
clear Data;
Data.X = pppp;
Data.AcrossDistance =  0;
Data.SliceExists    = true;
Data.Date           = NaN(1, nbProfils);
Data.Hour           = NaN(1, nbProfils);
Data.Tide           = zeros(1, nbProfils);
Data.Heave          = zeros(1, nbProfils);
Data.SliceExists    = 1;
Data.SliceName      = 'ODV';
Data.PingNumber     = 1:nbProfils;
Data.LatitudeNadir  = LatI;
Data.LongitudeNadir = LonI;
Data.LatitudeTop    = LatI;
Data.LongitudeTop   = LonI;
Data.LatitudeBottom    = LatI;
Data.LongitudeBottom   = LonI;
Data.DepthTop       =  -ZTopDepth; % zeros(1, nbProfils); %
% % % % Data.DepthBottom    = ones(1, nbProfils) * -max(ZI);
Data.DepthBottom    = -ZBotDepth;


Info.Signals(1).Name          = 'Date';
Info.Signals(end).Dimensions  = '1, nbPings';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'Ifremer';
Info.Signals(end).FileName    = fullfile(nomDirBin,'Date.bin');

Info.Signals(end+1).Name      = 'Hour';
Info.Signals(end).Dimensions  = '1, nbPings';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'nb milliseconds since midnight';
Info.Signals(end).FileName    = fullfile(nomDirBin,'Hour.bin');

Info.Signals(end+1).Name      = 'PingNumber';
Info.Signals(end).Dimensions  = '1, nbPings';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = ' ';
Info.Signals(end).FileName    = fullfile(nomDirBin,'PingNumber.bin');

Info.Signals(end+1).Name      = 'Tide';
Info.Signals(end).Dimensions  = '1, nbPings';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'm';
Info.Signals(end).FileName    = fullfile(nomDirBin, 'Tide.bin');

Info.Signals(end+1).Name      = 'Heave';
Info.Signals(end).Dimensions  = '1, nbPings';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'm';
Info.Signals(end).FileName    = fullfile(nomDirBin, 'Heave.bin');

Info.Signals(end+1).Name      = 'SliceExists';
Info.Signals(end).Dimensions  = 'nbSlices, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile(nomDirBin,'SliceExists.bin');

Info.Signals(end+1).Name      = 'SliceName';
Info.Signals(end).Dimensions  = 'nbSlices, 1';
Info.Signals(end).Storage     = 'char';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile(nomDirBin,'SliceName.bin');

Info.Signals(end+1).Name      = 'DepthTop';
Info.Signals(end).Dimensions  = '1, nbPings';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'm';
Info.Signals(end).FileName    = fullfile(nomDirBin, 'DepthTop.bin');

Info.Signals(end+1).Name      = 'DepthBottom';
Info.Signals(end).Dimensions  = '1, nbPings';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'm';
Info.Signals(end).FileName    = fullfile(nomDirBin, 'DepthBottom.bin');

Info.Images(1).Name           = 'LatitudeTop';
Info.Images(end).Dimensions   = 'nbSlices, nbPings';
Info.Images(end).Storage      = 'double';
Info.Images(end).Unit         = 'deg';
Info.Images(end).FileName     = fullfile(nomDirBin, 'LatitudeTop.bin');

Info.Images(end+1).Name       = 'LongitudeTop';
Info.Images(end).Dimensions   = 'nbSlices, nbPings';
Info.Images(end).Storage      = 'double';
Info.Images(end).Unit         = 'deg';
Info.Images(end).FileName     = fullfile(nomDirBin,'LongitudeTop.bin');

Info.Images(end+1).Name       = 'LongitudeBottom';
Info.Images(end).Dimensions   = 'nbSlices, nbPings';
Info.Images(end).Storage      = 'double';
Info.Images(end).Unit         = 'deg';
Info.Images(end).FileName     = fullfile(nomDirBin,'LongitudeBottom.bin');

Info.Images(end+1).Name       = 'LatitudeBottom';
Info.Images(end).Dimensions   = 'nbSlices, nbPings';
Info.Images(end).Storage      = 'double';
Info.Images(end).Unit         = 'deg';
Info.Images(end).FileName     = fullfile(nomDirBin,'LatitudeBottom.bin');

% Info.Images(end+1).Name       = 'Reflectivity';
% Info.Images(end).Dimensions   = 'nbRows, nbProfils, nbSlices';
% Info.Images(end).Storage      = 'single';
% Info.Images(end).Unit         = 'dB';
% Info.Images(end).FileName     = fullfile(nomDirBin,'Reflectivity.bin');

%}
%% Cr�ation du fichier XML d�crivant la donn�e

% [FileName,PathName,~, ~] = uiputfile('*.xml','Save file name',NomFicXML);
% NomFicXML = [fullfile(Path, racineFicName, TypeData) '.xml'];
xml_write(NomFicXML, Info);
flag = exist(NomFicXML, 'file');
if ~flag
    messageErreur(NomFicXML)
    return
end

%% Cr�ation du r�pertoire Info

nomDirInfo = nomDirXml;
if ~exist(nomDirInfo, 'dir')
    status = mkdir(nomDirInfo);
    if ~status
        messageErreur(nomDirInfo)
        flag = 0;
        return
    end
end

%% Cr�ation des fichiers binaires des signaux

for i=1:length(Info.Signals)
    flag = writeSignal(nomDirRoot, Info.Signals(i), Data.(Info.Signals(i).Name));
    if ~flag
        return
    end
end

%% Cr�ation des fichiers binaires des signaux

for i=1:length(Info.Images)
    flag = writeImage(nomDirRoot, Info.Images(i), Data.(Info.Images(i).Name));
    if ~flag
        return
    end
end

flag = 1;
        

