% Fonction writeVRMLFaceSet
% EXAMPLE:
% nomFicVrml = 'J:\Data\IFREMER\Data_SSC\SSC-3DViewer-Data\R&D\VRML\epave_FaceSet.vrml';
% writeVRMLFaceSet('epave_FaceSet.vrml', MPoints, KFacettes)
% ----------------------------------------------------------------------------

function writeVRMLFaceSet(nomFicVrml, coordPts, K, tabColor)


fid = fopen(nomFicVrml, 'w+', 'native', 'UTF-8');
if fid == -1
    str = sprintf(Lang('Impossible de cr�er Fichier VRML %s', ...
        'Impossible to create VRML file %s'), nomFicVrml);
    my_warndlg(str, 1)
    return
end

fprintf(fid, '#VRML V2.0 utf8\n');
fprintf(fid, '#IndexedFaceSet example: a pyramid\n');

fprintf(fid, 'Shape {\n');
fprintf(fid, '	appearance Appearance{\n');
fprintf(fid, '		material Material {\n');
fprintf(fid, '			diffuseColor     1 0 0 #simple red\n');
fprintf(fid, '			}\n');
fprintf(fid, '		}\n');
fprintf(fid, '	geometry IndexedFaceSet {\n');
fprintf(fid, '	solid FALSE\n');
fprintf(fid, '		coord Coordinate {\n');
fprintf(fid, '                    point [\n');

for j=1:size(coordPts,1)
    if j<size(coordPts,1)
        fprintf(fid, '				%8.4f %8.4f %8.4f,	#vertex %d\n', coordPts(j,1), coordPts(j,2), coordPts(j,3), j-1);
    else
        fprintf(fid, '				%8.4f %8.4f %8.4f	#vertex %d\n', coordPts(j,1), coordPts(j,2), coordPts(j,3), j-1);
    end
end
fprintf(fid, '                    ]\n');
fprintf(fid, '                }\n'); %Coordinate
fprintf(fid, '	colorPerVertex FALSE #so each face will have one of the colors\n');
fprintf(fid, '	color Color {\n');
fprintf(fid, '                    color [\n');
for j=1:size(tabColor,1)
        fprintf(fid, '				%d %d %d,	#color %d\n', tabColor(j,1), tabColor(j,2), tabColor(j,3), j-1);
end
fprintf(fid, '                    ]\n');
fprintf(fid, '                }\n'); %Color
fprintf(fid, '	colorIndex [ \n');
for j=1:size(tabColor,1)
        fprintf(fid, '				%d\n', j-1);
end
fprintf(fid, '	] \n');
fprintf(fid, '		coordIndex [\n');
for j=1:size(K,1)
    if j<size(K,1)
        fprintf(fid, '				%d, %d, %d, %d,\n', K(j,1)-1, K(j,2)-1, K(j,3)-1, -1);
    else
        fprintf(fid, '				%d, %d, %d, %d\n', K(j,1)-1, K(j,2)-1, K(j,3)-1, -1);
    end
end
fprintf(fid, '			]\n');
			
fprintf(fid, '     }\n'); % Geometry
fprintf(fid, '}\n');    % Shape
fclose(fid);
