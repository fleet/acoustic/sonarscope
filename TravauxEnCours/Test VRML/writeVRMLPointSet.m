% Fonction writeVRMLPointSet
% EXAMPLE:
% nomFicVrml = 'J:\Data\IFREMER\Data_SSC\SSC-3DViewer-Data\R&D\VRML\epave_FaceSet.vrml';
% writeVRMLPointSet('epave_FaceSet.vrml', nomFicVRML, coord, 'tabColor', tabColor)
% ----------------------------------------------------------------------------

function writeVRMLPointSet(nomFicVrml, nomFicCoordVRML, coordPts, varargin)

col    = getPropertyValue(varargin, 'tabColor', []);

fid = fopen(nomFicVrml, 'w+', 'native', 'UTF-8');
if fid == -1
    str = sprintf(Lang('Impossible de cr�er Fichier VRML %s', ...
        'Impossible to create VRML file %s'), nomFicVrml);
    my_warndlg(str, 1)
    return
end

ZMean = round(mean(coordPts(:,3)));
fprintf(fid, '#VRML V2.0 utf8\n');
fprintf(fid, '#Nuage de points de l''�pave\n');

fprintf(fid, 'Inline { url "%s" }\n', nomFicCoordVRML);
fprintf(fid, 'NavigationInfo{\n');
fprintf(fid,  'headlight TRUE\n');
fprintf(fid,  'type "EXAMINE"\n');
fprintf(fid, '}\n');
fprintf(fid, 'DirectionalLight {\n');
fprintf(fid,  'intensity 1\n');
fprintf(fid,  'color 0 0 0\n');
fprintf(fid,  'ambientIntensity 1.0\n');
fprintf(fid,  'direction -0.000000 -0.707107 0.707107\n');
fprintf(fid, '}\n');
fprintf(fid, 'Viewpoint {\n');
fprintf(fid, '#this is the "front" view, \n');
fprintf(fid,          'position    0 0 %d	#the camera positioned to X=0, Y=0 and Z=10\n', abs(ZMean) + 10);
fprintf(fid,          'orientation 0 0 0 0	#"default" orientation\n');
fprintf(fid, 		  'fieldOfView 1.57\n');
fprintf(fid,          'description "Above"\n');	
fprintf(fid, '     }\n');

fprintf(fid, 'Transform {\n');
fprintf(fid, '	translation 0 0 %d # Changement de rep�re\n', abs(ZMean));
fprintf(fid, '		children[\n');
fprintf(fid, '  Shape {\n');
fprintf(fid, '	   geometry PointSet {\n');
fprintf(fid, '		coord Coordinate { # x y z en m�tres\n');
fprintf(fid, '                    point [\n');
for j=1:size(coordPts,1)
    if j<size(coordPts,1)
        fprintf(fid, '				%12.8f %12.8f %12.8f,	#point %d\n', coordPts(j,1), coordPts(j,2), coordPts(j,3), j-1);
    else
        fprintf(fid, '				%12.8f %12.8f %12.8f	#point %d\n', coordPts(j,1), coordPts(j,2), coordPts(j,3), j-1);
    end
end
fprintf(fid, '                    ]\n');

fprintf(fid, '          }\n'); %Coordinate

% Ecriture des couleurs (1 couleur par point)
fprintf(fid, '	color Color {\n');
fprintf(fid, '                    color [\n');
for j=1:size(col,1)
        fprintf(fid, '				%d %d %d,	#color %d\n', col(j,1), col(j,2), col(j,3), j-1);
end
fprintf(fid, '                    ]\n');
fprintf(fid, '                }\n'); % Color
			
fprintf(fid, '     } # Geometry\n'); % Geometry
fprintf(fid, '    } # Shape\n');    % Shape
fprintf(fid, '] # Children\n'); % Children
fprintf(fid, '} # Transform\n');% Transform

fclose(fid);
