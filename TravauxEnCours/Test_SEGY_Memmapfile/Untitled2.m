function Untitled2
% nomFic = 'F:\SonarScopeData\SEGY\SEGY From JMA\LisiblePar_read_segy\AUV0001_D20091123_T050935_process.seg';
% nomFic = 'F:\SonarScopeData\SEGY\SEGY From JMA\GeoLittomer\HA0051_D20120412_T074618.SEG';
% nomFic = 'F:\SonarScopeData\SEGY\SEGY From JMA\Geo-Azur\20101206025510_CH1_35R.seg';
% nomFic = 'F:\SonarScopeData\SEGY\SEGY From JMA\Shallow Survey\LINE001_SINGLEBOOM.seg';
nomFic = 'F:\SonarScopeData\SEGY\SEGY From JMA\LisiblePar_read_segy_file\cube_jma.seg';
% nomFic = 'K:\Data\IFREMER\Data_SSC\SonarScopeData\SEGY\From JMA\Marmesonet\cube_grad_mig2.seg';
%[flag, Data,SegyTraceHeaders,SegyHeader,HeaderInfo]=ReadSegy(nomFic);
%%
% Analyse de l'endianness du fichier.
endian = getEndianness(nomFic);
if exist('endian', 'var')==1
    segyid = fopen(nomFic,'r',endian);
else
    endian ='ieee-be';
    segyid = fopen(nomFic,'r','ieee-be');  % ALL DISK FILES ARE IN BIG
end                                        % ENDIAN FORMAT, ACCORDING TO
SegyHeader          = GetSegyHeader(segyid);
offsetFileHeader    = ftell(segyid);
TraceStart          = offsetFileHeader;

Revision    = SegyHeader.SegyFormatRevisionNumber;
Format      = SegyHeader.Rev(Revision+1).DataSampleFormat(SegyHeader.DataSampleFormat).format;
FormatName  = SegyHeader.Rev(Revision+1).DataSampleFormat(SegyHeader.DataSampleFormat).name;
[flag, SingleSegyTraceHeaders]  = GetSegyTraceHeader(segyid,TraceStart,Format,SegyHeader.ns,[]);
sizeTraceHeader               = ftell(segyid) - offsetFileHeader;
fclose(segyid);

pppp = memmapfile(nomFic,   'Offset', offsetFileHeader, ...
                            'Format', {'uint8', [1 sizeTraceHeader], 'TraceHeader'; ...
                                       'single', [1 SegyHeader.ns], 'Trace'}, ...
                            'Repeat', Inf);

figure; plot(swapbytes(pppp.Data(1).Trace));
n1 = SegyHeader.ns;
n2 = SegyHeader.Job;
n3 = SegyHeader.Line
Image = NaN(n1,n2, 'single');
for k=1:n2
    Image(:,k) = swapbytes(pppp.Data(k).Trace);
end
% Image(Image > 10000) = NaN;
figure; imagesc(Image); colormap(gray)
SonarScope(Image)

