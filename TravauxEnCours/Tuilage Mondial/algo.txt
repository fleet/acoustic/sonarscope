 Calcul de la r�solution en Pixels:
 ==================================
 Hypoth�ses :
    30 niveaux Max de r�solution.
    Taile des tuiles = 150.
	
 ResoPixel = (20/(2^N))/150; 
 On recherche la r�solution la plus proche des r�solutions d'origine de l'image pour d�celer celle qu'on retient.
 
 
Calcul du nombre de niveaux:
============================
On divise l'image par 2 jusqu'� obtenir une image tenant dans 150 Pixels.
Le nombre d'occurrence trouv� est le nombre de niveaux.
 
Calcul du LevelZero:
====================
Lztsd = R�soPxiel * Taille de la tuile * 2^(Nb de niveaux de r�solution-1)

Ex : 
Pour une r�solution de  5.2083e-004, taille de la tuile = 150, LZTSD = 10�.