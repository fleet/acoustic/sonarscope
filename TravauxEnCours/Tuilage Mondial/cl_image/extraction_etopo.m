% Extraction dans une image
%
% SYNTAX :
%   b = extraction_etopo(a, ...)
%
% INPUT PARAMETERS :
%   a : Instance de cl_image
%
% PROPERTY NAMES / PROPERTY VALUES :
%   subx : Sous-echantillonnage en abscisses
%   suby : Sous-echantillonnage en ordonnees
%   x    : Sous-echantillonnage en x
%   y    : Sous-echantillonnage en y
%   XLim : Limites en x
%   YLim : Limites en y
%
% OUTPUT PARAMETERS :
%   b    : Instance de cl_image contenant l'image filtree
%   flag : 1=operation reussie
%
% EXAMPLES :
%   a = extraction_etopo(cl_image);
%   imagesc(a)
%   b = extraction_etopo(a, 'suby', 120:180, 'subx', 130:190);
%   imagesc(a)
%   imagesc(b)
%
%
% SEE ALSO : cl_image Authors
% AUTHORS  : JMA
% VERSION  : $Id: extraction_etopo.m,v 1.1 2005/09/14 08:23:13 augustin Exp augustin $
% ----------------------------------------------------------------------------

function [this, flag] = extraction_etopo(this, varargin)

global SonarScopeDataLevel2

my_warndlg('Etopo values are shifted, do not use this data for scientific purpose. This will be corrected soon I hope.', 0, ...
    'Tag', 'Etopo values are shifted')

XLim = getPropertyValue(varargin, 'XLim', []);
YLim = getPropertyValue(varargin, 'YLim', []);
subx = getPropertyValue(varargin, 'subx', []);
suby = getPropertyValue(varargin, 'suby', []);
x    = getPropertyValue(varargin, 'x',    []);
y    = getPropertyValue(varargin, 'y',    []);

this = this(1);


if ~isempty(XLim) && ~isempty(YLim)
    LatLim = YLim;
    LonLim = XLim;
else
    if isempty(this.x)
        if isempty(XLim) || isempty(YLim)

            % ----------------------------------------------------------------
            % L'image d'entree est vide : appel par extraction_etopo(cl_image)
            % On rend la terre entiere

            LatLim = [-90 90];
            LonLim = [-180 180];

        elseif ~isempty(x) && ~isempty(y)
            [subx, suby] = xy2ij(this, x, y);
            LatLim(1) = this.y(suby(1));
            LatLim(2) = this.y(suby(end));
            LonLim(1) = this.x(subx(1));
            LonLim(2) = this.x(subx(end));
        else

            % ----------------------------------------------------------------
            % L'image d'entree est vide : appel par extraction_etopo(cl_image)
            % mais on donne les limites geographiques de la zone

            LatLim = YLim;
            LonLim = XLim;
        end
    else

        % -----------------------------------------
        % L'image est en coordonnees geographiques

        if this.GeometryType == LatLong(cl_image_I0)
            if isempty(XLim) || isempty(YLim)
                if isempty(x) || isempty(y)
                    if isempty(subx)
                        subx = 1:length(this.x);
                    end
                    if isempty(suby)
                        suby = 1:length(this.y);
                    end
                else
                    [subx, suby] = xy2ij(this, x, y);
                end
            else
                subx = find((this.x >= min(XLim)) & (this.x <= max(XLim)));
                suby = find((this.y >= min(YLim)) & (this.y <= max(YLim)));
            end

            LatLim(1) = this.y(suby(1));
            LatLim(2) = this.y(suby(end));
            LonLim(1) = this.x(subx(1));
            LonLim(2) = this.x(subx(end));
        else

            % ----------------------------------------------
            % L'image n'est pas en coordonn�es geographiques

            flag = 0;
            return

        end
    end
end


% [lon, lat, Z, Comment, flag] = lec_etopo(cl_etopo([]), [min(LatLim) max(LatLim)], LonLim);
% 16/02/2010 : GLU, int�gration du ETOPO1min pour l'ensemble de la terre.
% Rechargement du layer Etopo1.
nomFic    = fullfile(SonarScopeDataLevel2, 'Etopo1.xml');
[dirLayer, layerName, ext] = fileparts(nomFic); %#ok<NASGU>
obj       = cl_tile_ETOPO1.linkLayerEtopo(dirLayer,layerName);
Comment   = layerName;
% Calcul du Z � extraire
[Z, obj, Tiles] = obj('XLim', [XLim(1) XLim(2)], 'YLim', [YLim(1) YLim(2)]); %#ok<NASGU>
lon = linspace(XLim(1),XLim(2), size(Z, 2));
lat = linspace(YLim(2),YLim(1), size(Z, 1));
flag = 1;

if ~flag
    return
end
if (length(lon) < 2) || (length(lat) < 2)
    flag = 0;
    return
end

% --------------------------------------------------------------------------
% Recuperation des parametres cartographiques definis pour l'image d'origine

Carto = this.Carto;
if isempty(Carto)
    Carto = cl_carto('Ellipsoide.Type', 11, 'Projection.Type', 1);
else
    Carto = set(Carto, 'Ellipsoide.Type', 11, 'Projection.Type', 1); % On impose WGS84 car etopo est defini ainsi
end

% -------------------
% Creation de l'image

% sub = find((Z < -13000) | (Z > 9000));
sub = ((Z < -13000) | (Z > 9000));
Z(sub) = NaN;
this = cl_image('Image', Z, 'x', lon, 'y', lat, 'Titre', 'Extraction Etopo (m)', ...
    'ColormapIndex', 3, 'GeometryType', LatLong(cl_image_I0), 'Unit', 'm', 'XUnit', 'deg', 'YUnit', 'deg', ...
    'DataType', identDataType(cl_image_I0, 'Bathymetry'), 'Comments', Comment);
this.Carto = Carto;

% -------------------
% Completion du titre

this = set_FullImageName(this);

% ---------------------------------
% Apparemment tout s'est bien passe

flag = 1;
