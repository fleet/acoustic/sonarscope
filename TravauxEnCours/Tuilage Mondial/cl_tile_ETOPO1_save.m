% Classe cl_tile_ETOPO1 : Tuilage multi-r�solutions de GLOBE avec r�solution 1
% arc-minute
%
% SYNTAX :
%   X = cl_tilebil(...)
%
% PROPERTY NAMES / PROPERTY VALUES :
%   ADU
%
% PROPERTY NAMES ONLY :
%
%
% OUTPUT PARAMETERS :
%   X : Variable
%
%
% EXAMPLES :
%   ADU
%
%
% SEE ALSO   : FormationMemmapfile Authors
% AUTHORS    : JMA, GLU
% VERSION    : $Id: cl_tile_GLOBE.m,v 1.2 2009/12/13 12:46:11 augustin Exp $
%-------------------------------------------------------------------------------
classdef cl_tile_ETOPO1_save < handle
    
    %cl_tile_ElevationAndTexture Summary of obj class goes here
    %   Detailed explanation goes here
    
    
    properties(GetAccess='public', SetAccess='public')
        %   LayerName    : Nom du layer ou sont stock�s les tuiles
        %   Value        : Matrice � mapper
        %   sizeTile     : Taille des tuiles � cr�er (512 par d�faut)
        %   ValNaN       : Valeur repr�sentant les non-valeurs
        %   pasPixel     : Pas d'un pixel � la meilleure r�solution (deg)
        value           = [];
        cmap            = jet(256);
        layerName       = [];
        layerSetName    = fullfile(my_tempdir, 'SonarScope-Tiles');
        valNaN          = NaN;
        sizeTile        = 150;  % On suppose les tuiles carr�es
        bornesData      = [];
        % du point haut � gauche du tuilage.
        pasPixel        = [];
        lztsd           = [];   % Level Zero tile Size Degree
        writeJpg        = 0;
        formatTile      = 'gif';%'jpg';
        scaleLatLon     = [];   % ex : Lat = scaleLatLon.aLat*subL+scaleLatLon.bLat
        scaleLigCol     = [];   % ex : Lig = scaleLigCol.aL*Lat+scaleLigCol.bL
        nbLevels        = 0;    % Nombre de niveaux de d�finition des tuiles.
        nomFicXml    = [];
        flagExportTexture   = 1;
        flagExportElevation = 0;
    end
    
    %################################################################################################
    methods(Access='public')
        
        function obj=cl_tile_ETOPO1(varargin)
            
            
            %% ---------------------
            % Constructeur
            % EXAMPLES :
            %   m = cl_tile_ETOPO1('Value', [1 2 3 4; 5 6 7 8]);
            % ---------------------
            Struct = getPropertyValue(varargin, 'Struct', []);
            %             processType = getPropertyValue(varargin, 'Process', 'Creation');
            %             subx = getPropertyValue(varargin, 'subx', []);
            %             suby = getPropertyValue(varargin, 'suby', []);
            
            if ~isempty(Struct)
                obj = class(Struct, 'cl_tile_ETOPO1');
                return
            end
            
            % On regarde si l'objet a ete cree uniquement pour atteindre une methode de la classe
            if (nargin == 1) && isempty(varargin{1})
                return
            end
            
            % Affectation des propri�t�s de l'objet.
            obj = set(obj, varargin{:});
            
            Value = getPropertyValue(varargin, 'Value', []);
            if ~isempty(Value)
                obj = createAllLevel(obj, Value, varargin{:});
            end
            
            % Initialisation des valeurs transmises lors de l'appel au constructeur
            LogSilence = getPropertyValue(varargin, 'LogSilence', 0);
            
            str = sprintf('Creation matrix tiling mapped on %s', fullfile(obj.layerSetName, obj.layerName));
            
            % Ecriture dans le fichier Log
            if ~LogSilence
                Info.TilingFileLinking.bornesData = obj.bornesData;
                Info.TilingFileLinking.layerName = obj.layerName;
                Info.TilingFileLinking.layerSetName = obj.layerSetName;
                write_LogXml('Info', Info)
            end
            
            if ~LogSilence
                disp('---------------------------------------------------------')
                disp(str)
                if ~isdeployed
                    printStack(obj, dbstack)
                end
            end
            
            
        end % Fin du Constructeur
        
        function printStack(obj, ST)%#ok
            % ---------------------
            % Fonction printStack
            % EXAMPLES :
            %   printStack(m, dbstack)
            % ---------------------
            if ~isdeployed
                for i=1:length(ST)
                    str = sprintf('\tFunction : %s\t - line : %d', ST(i).name, ST(i).line);
                    disp(str)
                end
            end
        end % Fin de printStack
        
        function varargout = set(obj, varargin)
            %%------------------------------------------------------------------
            % Fonction set
            % Ecriture d'une propri�t� d'un objet Tuile.
            %
            % SYNTAX :
            %   a = set(a, PN, PV)
            %
            % INPUT PARAMETERS :
            %   a  : Instance de cl_tile_ETOPO1
            %   PN : Nom de la propri�t�
            %   PV : Valeur de la propri�t�
            %
            % EXAMPLES :
            %   m = cl_tile_ETOPO1('Value', [1 2 3 4; 5 6 7 8]);
            %   m = set(m, 'LayerSetName', 'SSC-Tiles')
            %   m(1) = pi
            %   m(1, 2:3) = [10 11]
            %   m(1:2,2) = [10; 11]
            %   m(1:256,2) = meshgrid(1:256,1:2)
            %------------------------------------------------------------------
            
            % ----------------------------------------------------------
            % Affectation des valeurs ne necessitant pas de verification
            LayerSetName        = getPropertyValue(varargin, 'LayerSetName',obj.layerSetName);
            LayerName           = getPropertyValue(varargin, 'LayerName',   obj.layerName);
            BornesData          = getPropertyValue(varargin, 'BornesData',  obj.bornesData);
            ValNaN              = getPropertyValue(varargin, 'ValNaN',      obj.valNaN);
            WriteJpg            = getPropertyValue(varargin, 'WriteJpg',    obj.writeJpg);
            FormatTile          = getPropertyValue(varargin, 'FormatTile',  obj.formatTile);
            Lztsd               = getPropertyValue(varargin, 'Lztsd',       obj.lztsd);
            PasPixel            = getPropertyValue(varargin, 'PasPixel',    obj.pasPixel);
            nomFicXml           = getPropertyValue(varargin, 'NomFicXML',   obj.nomFicXml);
            SizeTile            = getPropertyValue(varargin, 'SizeTile',    obj.sizeTile);
            flagExportTexture	= getPropertyValue(varargin, 'flagExportTexture',    obj.flagExportTexture);
            flagExportElevation	= getPropertyValue(varargin, 'flagExportElevation',    obj.flagExportElevation);
            subx                = getPropertyValue(varargin, 'subx',   []);
            suby                = getPropertyValue(varargin, 'suby',   []);
            
            Value = getPropertyValue(varargin, 'Value', []);
            % obj.value = Value;
            
            % A placer en 1er.
            if ~isempty(SizeTile)
                obj.sizeTile = SizeTile;
            end
            
            if ~isempty(LayerSetName)
                obj.layerSetName = LayerSetName;
            end
            
            if ~isempty(LayerName)
                obj.layerName = LayerName;
            end
            
            if ~isempty(BornesData)
                % La donn�e d'entr�e n'est pas de classe cl_image.
                obj.bornesData = BornesData;
            end
            
            
            % D�termination des bornes de Tuiles.
            if ~isempty(Value)
                if isa(Value,'cl_image')
                    % Calcul des coordonn�es de l'image dans les diff�rents niveaux
                    xData = get(Value, 'x');
                    yData = get(Value, 'y');
                    if ~isempty(subx)
                        xData = xData(subx);
                    end
                    if ~isempty(suby)
                        yData = yData(suby);
                    end
                    if length(subx) ~= get(Value,'NbColumns') && ...
                            length(suby) ~= get(Value,'nbLig')
                        obj.value = extraction(Value, 'subx', subx, 'suby', suby);
                    else
                        if length(subx) ~= get(Value,'NbColumns')
                            obj.value = extraction(Value, 'subx', subx);
                        else
                            if length(suby) ~= get(Value,'nbLig')
                                obj.value = extraction(Value, 'suby', suby);
                            else
                                obj.value = Value;
                            end
                        end
                    end
                    CLim = get(Value, 'CLim');
                    obj.value= set(obj.value, 'CLim', CLim);
                    obj.bornesData.sizePixels(2) = numel(xData);
                    obj.bornesData.sizePixels(1) = numel(yData);
                    obj.bornesData.lonW = xData(1);
                    obj.bornesData.lonE = xData(end);
                    if yData(1) > yData(end)
                        obj.bornesData.latS = yData(end);
                        obj.bornesData.latN = yData(1);
                    else
                        obj.bornesData.latS = yData(1);
                        obj.bornesData.latN = yData(end);
                    end
                end
                PasPixelLat = double(abs(obj.bornesData.latN - obj.bornesData.latS) / (obj.bornesData.sizePixels(1) - 1));
                PasPixelLon = double(abs(obj.bornesData.lonE - obj.bornesData.lonW) / (obj.bornesData.sizePixels(2) - 1));
                
                % Calcul des facteurs de correspondances (LatLon / xy).
                obj.scaleLatLon.aLat = 1/PasPixelLat;
                obj.scaleLatLon.aLon = 1/PasPixelLon;
                obj.scaleLatLon.bLat = -obj.bornesData.latN / PasPixelLat;
                obj.scaleLatLon.bLon = -obj.bornesData.lonW / PasPixelLon;
                obj.scaleLigCol.aLig   = PasPixelLat;
                obj.scaleLigCol.aCol   = PasPixelLon;
                obj.scaleLigCol.bCol   = obj.bornesData.lonW;
                obj.scaleLigCol.bLig   = obj.bornesData.latN;
                
                % Calcul du nombre de Niveaux.
                latData = cl_tile_ETOPO1.pixDataLat2Lat((1:obj.bornesData.sizePixels(1))-1, -obj.scaleLigCol.aLig, obj.scaleLigCol.bLig);
                lonData = cl_tile_ETOPO1.pixDataLon2Lon((1:obj.bornesData.sizePixels(2))-1, obj.scaleLigCol.aCol, obj.scaleLigCol.bCol);
                % Calcul du nombre de niveaux de r�solution.
                obj.nbLevels = cl_tile_ETOPO1.computeNbLayers(latData, lonData, obj.sizeTile);
            else
                if ~isempty(obj.bornesData)
                    PasPixelLat = double(abs(obj.bornesData.latN - obj.bornesData.latS) / (obj.bornesData.sizePixels(1) - 1));
                    PasPixelLon = double(abs(obj.bornesData.lonE - obj.bornesData.lonW) / (obj.bornesData.sizePixels(2) - 1));
                    % Calcul des facteurs de correspondances (LatLon / xy).
                    obj.scaleLatLon.aLat = 1/PasPixelLat;
                    obj.scaleLatLon.aLon = 1/PasPixelLon;
                    obj.scaleLatLon.bLat = -obj.bornesData.latN / PasPixelLat;
                    obj.scaleLatLon.bLon = -obj.bornesData.lonW / PasPixelLon;
                    obj.scaleLigCol.aLig   = PasPixelLat;
                    obj.scaleLigCol.aCol   = PasPixelLon;
                    obj.scaleLigCol.bCol   = obj.bornesData.lonW;
                    obj.scaleLigCol.bLig   = obj.bornesData.latN;
                end
                
                DirnameLayerSet = fullfile(obj.layerSetName ,obj.layerName);
                obj.nbLevels = size(listeDirOnDir(DirnameLayerSet),1);
                
            end
            
            
            % Approximation du Level Zero choisi par l'op�rateur selon la r�gle du 180/2^N avec N entier.
            if ~isempty(PasPixel)
                obj.pasPixel = PasPixel;
            else
                %NEW ???obj.pasPixel = (20/2^obj.nbLevels)/obj.sizeTile;
                obj.pasPixel = cl_tile_ETOPO1.computePasPixDeg(PasPixelLat, PasPixelLon, obj.sizeTile);
            end;
            
            if ~isempty(Lztsd)
                obj.lztsd = Lztsd;
            else
                % Calcul du Level Zero Tile Size en Deg.
                obj.lztsd = obj.pasPixel * obj.sizeTile * 2^(obj.nbLevels-1);
            end
            
            if ~isempty(ValNaN)
                obj.valNaN = ValNaN;
            end
            
            if ~isempty(nomFicXml)
                obj.nomFicXml = nomFicXml;
            else
                nomFicXml = fullfile(obj.layerSetName, [obj.layerName '.xml']);
                if exist(nomFicXml, 'file')
                    obj.nomFicXml = [obj.layerName '.xml'];
                end
            end
            
            if ~isempty(FormatTile)
                obj.formatTile = FormatTile;
            end
            
            if ~isempty(WriteJpg)
                obj.writeJpg = WriteJpg;
            end
            
            if ~isempty(flagExportTexture)
                obj.flagExportTexture = flagExportTexture;
            end
            
            if ~isempty(flagExportElevation)
                obj.flagExportElevation = flagExportElevation;
            end
            
            % Sortie
            if nargout == 0
                assignin('caller', inputname(1), obj);
            else
                varargout{1} = obj;
            end
            
        end
        
        %{
function display(obj)
            % Representation externe d'une instance sur le terminal
            %
            % SYNTAX :
            %   display(a)
            %
            % INPUT PARAMETERS :
            %   a : Instance de cl_tile_ETOPO1
            %
            % EXAMPLES :
            %   ADU
            %
            % SEE ALSO   : cl_tile_ETOPO1 Authors
            % AUTHORS    : JMA
            % VERSION    : $Id: cl_tile_ETOPO1.m,v 1.2 2003/11/12 12:46:11 augustin Exp $
            %-------------------------------------------------------------------------------
            % Formattage des indices totaux de la matrice.
            subPixLat = 1:obj.bornesData.sizePixels(1);
            subPixLon = 1:obj.bornesData.sizePixels(2);
            linearSub = [];
            LevelMax = obj.nbLevels-1;
            % R�cup�ration de la valeur lue.
            [Value, indReadTiles] = readTile(obj, LevelMax, subPixLat, subPixLon, linearSub); %#ok<NASGU>
            figure; imagesc(Value);

        end % Fonction Display
        %}
        
        function obj = subsasgn(obj, theStruct, ValToWrite)
            %% function subsasgn
            % Acc�s en �criture des �l�ments d'une tuile stock�e.
            %
            % SYNTAX :
            %   a = subsasgn(a, theStruct, ValToWrite)
            %
            % INPUT PARAMETERS :
            %   a         : Instance de cl_tile_ETOPO1
            %   theStruct : (sub) ou (sub1, sub2) ou (sub1, sub2, sub3)
            %   ValToWrite : Valeurs � �crire
            %
            % OUTPUT PARAMETERS :
            %   a : Instance de cl_tile_ETOPO1
            %
            % EXAMPLES :
            %   m = cl_tile_ETOPO1('Value', [1 2 3 4; 5 6 7 8]);
            %   m(1)
            %   m(1,2)
            %   m([2 3 7 3])
            %   m(1,:)
            %   m(:,2)
            %   m(1, 2:3) = [10 11]
            %   m(1:2,2) = [10; 11]
            %
            % SEE ALSO   : cl_tile_ETOPO1 FormationMemmapfile Authors
            % AUTHORS    : JMA
            % VERSION    : $Id: cl_tile_ETOPO1.m,v 1.2 2003/11/12 12:46:11 augustin Exp $
            %-------------------------------------------------------------------------------
            
            % Marquage de la modification de l'objet.
            theSubs = theStruct(1).subs;
            if strcmp(theStruct(1).type, '()')
                if numel(theSubs) > 1
                    subPixLat = cell2mat(theStruct(1).subs(1));
                    subPixLon = cell2mat(theStruct(1).subs(2));
                    linearSub = [];
                elseif ~isempty(theSubs{1})
                    % Recherche des index Lig et Col r�els.
                    s = [obj.bornesData.sizePixels(1),obj.bornesData.sizePixels(2)];
                    [subPixLat, subPixLon] = ind2sub(s, theSubs{:});
                    linearSub = theSubs;
                end
                [obj, indWriteTiles] = writeTile(obj, ValToWrite, subPixLat, subPixLon, linearSub);
                % Impact des modifications sur les tuiles de niveaux sup�rieurs.
                for k=obj.nbLevels-2:-1:0
                    obj = writeTileLevelN(obj, k, indWriteTiles);
                end
            else
                obj = set(obj, theStruct.subs, ValToWrite);
            end
            
        end % Fonction subasgn
        
        function [Val, Img, indReadTiles] = subsref(obj, theStruct)
            %% function subsref
            % Acc�s en lecture des �l�ments d'une tuile stock�e.
            %
            % SYNTAX :
            %   Value = subsref(a, theStruct)
            %
            % INPUT PARAMETERS :
            %   a         : Instance de cl_tile_ETOPO1
            %   theStruct : (sub) ou (sub1, sub2) ou (sub1, sub2, sub3)
            %
            % OUTPUT PARAMETERS :
            %   Value : Valeurs demand�es
            %
            % EXAMPLES :
            %   m = cl_tile_ETOPO1('Value', [1 2 3 4; 5 6 7 8]);
            %   m(1)
            %   m(1,2)
            %   m([2 3 7 3])
            %   m(1,:)
            %   m(:,2)
            %
            % SEE ALSO   : cl_tile_ETOPO1 FormationMemmapfile Authors
            % AUTHORS    : JMA
            % VERSION    : $Id: cl_tile_ETOPO1.m,v 1.2 2003/11/12 12:46:11 augustin Exp $
            %-------------------------------------------------------------------------------
            Val = [];
            theSubs = theStruct(1).subs;
            if strcmp(theStruct(1).type, '()')
                
                if any(strcmp(theSubs, 'XLim') |  strcmp(theSubs, 'YLim'))
                    y = [];
                    x = [];
                    if strcmp(theStruct(1).subs(1),'XLim')
                        x = cell2mat(theStruct(1).subs(2));
                    elseif strcmp(theStruct(1).subs(1),'YLim')
                        y = cell2mat(theStruct(1).subs(2));
                    end
                    if strcmp(theStruct(1).subs(3),'XLim')
                        x = cell2mat(theStruct(1).subs(4));
                    elseif strcmp(theStruct(1).subs(3),'YLim')
                        y = cell2mat(theStruct(1).subs(4));
                    end
                elseif ~isempty(theSubs{1})
                    % TODO :  � terminer 
                    % s = [obj.bornesData.sizePixels(1),obj.bornesData.sizePixels(2)];
                    % [subPixLat, subPixLon] = ind2sub(s, theSubs{:});
                    % linearSub = theSubs{1};
                    
                    x = [obj.bornesData.lonW obj.bornesData.lonE];
                    y = [obj.bornesData.latS obj.bornesData.latN];
                end
                
                % Calcul du level appropri� � la taille de l'�cran et du
                iLevel = cl_tile_ETOPO1.computeLevel(x, y, obj.sizeTile, obj.nbLevels);
                                
                % Lecture des tuiles et composition de l'image finale.
                [Val, Img, indReadTiles] = readTileEtopo(obj, iLevel, x, y);
                
            else
                % Cas de get sur les �l�ments de type Structure (scale
                % LigCol, ...)
                if numel(theStruct) > 1
                    Img = get(obj, theStruct(2).subs);
                else
                    Img = get(obj, theStruct.subs);
                end
                indReadTiles = [];
            end
            
        end % Fonction subsref
              
        function delete(obj)
            %% function delete
            % Destructeur appel� lors de la destruction de l'instance de l'objet
            % par clear obj.
            % EXAMPLES :
            %   clear m;
            % -------------------------------------------------------------
            try
                if isempty(obj)
                    return
                end
                % Suppression des objets (Tuiles en m�moire) qui d�pendent du cl_tile_ETOPO1 courant.
                % GLUGLUGLU : pb � la suppression d'un objet dupliqu�.
                % pppp = evalin('base', 'whos');
                % k = strcmp({pppp.class}, 'cl_memtile');
                % [i, j] = find(k==1);
                % my_warndlg('Finaliser la purges des cl_tile_ETOPO1 et des cl_memtile d�pendantes',0);
                % if ~isempty(j)
                %     for iLoop=1:numel(j)
                %         varMemTile = evalin('base', pppp(j(iLoop)).name);
                %         if strcmp(varMemTile.layerName, obj.layerName) && ...
                %                 strcmp(varMemTile.layerSetName, obj.layerSetName)
                %             eval(['clear ' pppp(j(iLoop)).name]);
                %         end
                %     end
                % end
                
                
            catch ME
                ME.stack
                my_warndlg(Lang('Echec dans la destruction du FileID de l'' object cl_tile_ETOPO1', 'Failure in object cl_tile_ETOPO1 FileID erase'),1);
            end
        end % Fonction Delete
        
        function PV = get(obj, PN)
            %% Function get
            % Lecture d'une propri�t� d'une arbo cl_tile_ETOPO1
            %
            % SYNTAX :
            %   PV = get(a, PN)
            %
            % INPUT PARAMETERS :
            %   a  : Instance de cl_tile_ETOPO1
            %   PN : Nom de la propri�t�
            %
            % OUTPUT PARAMETERS :
            %   PV : Valeur de la propri�t�
            %
            % EXAMPLES :
            %   get(m, 'SizePixels')
            %   get(m, 'Value')
            %   m
            %   m(1)
            %   m(1,2)
            %   m([2 3 7 3])
            %   m(1,2)
            %   m(1,:)
            %   m(:,1)
            %   imagesc(m(1:2,1:4))
            %   imagesc(m(:,:))
            %   V = get(m, 'Value')
            %   SizePixels = get(m, 'SizePixels')
            %
            % SEE ALSO   : cl_tile_ETOPO1 FormationMemmapfile Authors
            % AUTHORS    : JMA
            % VERSION    : $Id: cl_tile_ETOPO1.m,v 1.2 2003/11/12 12:46:11 augustin Exp $
            %--------------------------------------------------------------------------
            switch PN
                case 'layerSetName'
                    PV = obj.layerSetName;
                    
                case 'layerName'
                    PV = obj.layerName;
                    
                case 'formatTile'
                    PV = obj.formatTile;
                    
                case 'sizeTile'
                    PV = obj.sizeTile;
                    
                case 'bornesData'
                    PV = obj.bornesData;
                    
                case 'sizePixels'
                    PV = obj.bornesData.sizePixels;
                    
                case 'nomFicXml'
                    PV = obj.nomFicXml;
                    
                case 'scaleFactor'
                    PV = {obj.scaleLatLon;obj.scaleLigCol};
                    
                case 'scaleLigCol'
                    PV = obj.scaleLigCol;
                    
                case 'scaleLatLon'
                    PV = obj.scaleLatLon;
                    
                case 'Value'
                    PV = obj.value;
                    
                case 'writeJpg'
                    PV = obj.writeJpg;
                    
                case 'lztsd'
                    PV = obj.lztsd;
                    
                case 'nbLevels'
                    PV = obj.nbLevels;
                    
                case 'pasPixel'
                    PV = obj.pasPixel;
                    
                case 'valNaN'
                    PV = obj.valNaN;
                    
                case 'cmap'
                    PV = obj.cmap;
                    
                case'flagExportTexture'
                    PV = obj.flagExportTexture;
                    
                case 'flagExportElevation'
                    PV = obj.flagExportElevation;
                    
                otherwise
                    PV = [];
                    str = sprintf(Lang('cl_tile_ETOPO1/get %s non pris en compte','cl_tile_ETOPO1/get %s not treated'), PN);
                    my_warndlg(['cl_tile_ETOPO1:get ', str], 0);
            end
            
        end % Fonction get
        
        function obj = createAllLevel(obj, varargin)
            % M�thode createAllLevel : Description d'une image sous forme de tuiles
            % multi-r�solutions
            %
            % SYNTAX :
            %   X = cl_tile_ETOPO1(...)
            %
            % PROPERTY NAMES / PROPERTY VALUES :
            %   ADU
            %
            % PROPERTY NAMES ONLY :
            %
            %
            % OUTPUT PARAMETERS :
            %   X : Variable
            %
            %
            % EXAMPLES :
            %   cf .cl_tile_ETOPO1
            %
            % SEE ALSO   : FormationMemmapfile Authors
            % AUTHORS    : JMA, GLU
            % VERSION    : $Id: cl_tile_ETOPO1.m,v 1.2 2003/11/12 12:46:11 augustin Exp $
            %-------------------------------------------------------------------------------
            
            if obj.flagExportTexture
                supprimeFiles(fullfile(obj.layerSetName,obj.layerName),'png');
            end
            
            
            if obj.flagExportElevation
                supprimeFiles(fullfile(obj.layerSetName,obj.layerName),'zip');
                supprimeFiles(fullfile(obj.layerSetName,obj.layerName),'bil');
                
                %{
                A Supprimer, GLUGLUGLU
                [run,matParam] = obj.isPresent(obj); %#ok<NASGU>
                if ~run
                    obj.flagExportElevation = false;
                end
                %}
            end
            
            % Cr�ation du niveau de r�solution le plus fin
            obj  = createLevelMax(obj, varargin{:});
            % Cr�ation des niveaux sup�rieurs (les niveaux d�cr�mentent
            % jusqu'� 0).
            
            for k=obj.nbLevels-2:-1:0
                obj = createLevelN(obj, k, varargin{:});
            end
            
            % Fermeture de la progressBar multiple.
            progressbar('ready');
            progressbar('close');
            
        end  % createAllLevel
        
        function obj = createLevelMax(obj, varargin)
            % M�thode createLevelMax : Description d'une image sous forme de tuiles
            % de r�solution la plus fine
            %
            % SYNTAX :
            %   X = createLevelMax(X, ...)
            %
            % PROPERTY NAMES / PROPERTY VALUES :
            %   ADU
            %
            % OUTPUT PARAMETERS :
            %   X : Variable
            %
            % EXAMPLES :
            %   cf .cl_tile_ETOPO1
            %
            % SEE ALSO   : FormationMemmapfile Authors
            % AUTHORS    : JMA, GLU
            % VERSION    : $Id: cl_tile_ETOPO1.m,v 1.2 2003/11/12 12:46:11 augustin Exp $
            %-------------------------------------------------------------------------------
            global flagDEBUG;
            
            % R�cup�ration des arguments variables.
            LayerName       = getPropertyValue(varargin, 'LayerName',  obj.layerName);
            LayerSetName    = getPropertyValue(varargin, 'LayerSetName',  obj.layerSetName);
            ValNaN          = getPropertyValue(varargin, 'ValNaN', obj.valNaN); %#ok<NASGU>
            % BornesData      = getPropertyValue(varargin, 'BornesData', obj.bornesData);
            BornesData      = obj.bornesData;
            
            Value = obj.value;
            pasPixDegTile = obj.pasPixel;
            
            % Enregistrement  de la table de couleurs de la donn�e � traiter.
            obj.cmap = get(Value, 'Colormap');
            
            % Calcul des coordonn�es de l'image.
            % ScaleLigCol = obj.scaleLigCol;
            % ScaleLigCol.bLig = BornesData.latS;
            % latData = cl_tile_ETOPO1.pixDataLat2Lat((1:BornesData.sizePixels(1))-1, ScaleLigCol.aLig, ScaleLigCol.bLig);
            % lonData = cl_tile_ETOPO1.pixDataLon2Lon((1:BornesData.sizePixels(2))-1, ScaleLigCol.aCol, ScaleLigCol.bCol);
            latData = get(Value, 'y');
            lonData = get(Value, 'x');
            
            % Calcul du nombre de niveaux de r�solution.
            obj.nbLevels = cl_tile_ETOPO1.computeNbLayers(latData, lonData, obj.sizeTile);
            
            % Cr�ation du r�pertoire du sous-ensemble des Layers
            LevelMax = obj.nbLevels-1; % NumLayer (au sens de NWW) d�croit jusqu'� 0
            
            DirnameLayerSet = fullfile(LayerSetName ,LayerName);
            flag = cl_tile_ETOPO1.createDir(DirnameLayerSet, 1); %#ok<NASGU>
            
            % Copie du fichier ReadMe dans le r�pertoire contenant tous les
            % layers si celui-ci n'existe pas.
            flag = cl_tile_ETOPO1.copyReadMe(LayerSetName); %#ok<NASGU>
            
            % Cr�ation du r�pertoire de num�ro du layer N (Echelle de N � 0)
            DirIndexLayer = fullfile(DirnameLayerSet, num2str(LevelMax));
            flag = cl_tile_ETOPO1.createDir(DirIndexLayer, 1); %#ok<NASGU>
            
            % Calcul des tuiles (indice et bornes selon les limites de latitudes et
            % de longitudes).
            Tiles = cl_tile_ETOPO1.computeTiles(BornesData.latS, BornesData.latN, BornesData.lonW, BornesData.lonE, ...
                obj.sizeTile, pasPixDegTile);
            indTileLon = Tiles.xIndiceTuile;
            indTileLat = Tiles.yIndiceTuile;
            latTileDeb = Tiles.YTileDeb;
            lonTileDeb = Tiles.XTileDeb;
            latTileFin = Tiles.YTileFin;
            lonTileFin = Tiles.XTileFin;
            
            % Calcul de la longueur de la barre de progression.
            hldBarTotal = numel(latTileDeb)*numel(lonTileDeb);
            
            % strTitle = ['Creating Tiling Files : ' DirIndexLayer];
            % h = create_waitbar(strTitle, 'Entete', 'IFREMER - SonarScope : Pyramid Image Creation, please wait ...');
            progressbar('start', hldBarTotal, 'IFREMER - SonarScope : Pyramid Image Creation, please wait ...','EstTimeLeft','off');
            
            hdlBarParcourus = 0;
            % Parcours des tuiles � cr�er. R�cup�ration de l'image Extrait
            % par extrait, intercept�e par chaque tuile.
            for YTuile=1:length(latTileDeb)
                
                % Mise � jour de la WaitBar
                progressbar(hdlBarParcourus+YTuile, [Lang('Cr�ation du niveau le plus fin - Niveau', 'Creation of finest resolution - Level ') num2str(LevelMax)]);
                % my_waitbar((hdlBarParcourus+XTuile), hldBarTotal, h);
                
                latTile = linspace(latTileDeb(YTuile),latTileFin(YTuile), obj.sizeTile);
                subLatTile = find((latData >= latTile(1)) & (latData <= latTile(end)));
                if isempty(subLatTile)
                    continue
                end
                subLatTile = [subLatTile(1)-1 subLatTile subLatTile(end)+1]; %#ok<AGROW>
                subLatTile(subLatTile < 1) = [];
                subLatTile(subLatTile > length(latData)) = [];
                DirRaw = fullfile(DirIndexLayer, num2str(indTileLat(YTuile),'%04d'));
                flag = cl_tile_ETOPO1.createDir(DirRaw, 0); %#ok<NASGU>
                
                for XTuile=1:length(lonTileDeb)
                    lonTile = linspace(lonTileDeb(XTuile),lonTileFin(XTuile), obj.sizeTile);
                    subLonTile = find((lonData >= lonTile(1)) & (lonData <= lonTile(end)));
                    if isempty(subLonTile)
                        continue
                    end
                    subLonTile = [subLonTile(1)-1 subLonTile subLonTile(end)+1]; %#ok<AGROW>
                    subLonTile(subLonTile < 1) = [];
                    subLonTile(subLonTile > length(lonData)) = [];
                    if isa(Value, 'cl_image')
                        % Extraction de la donn�e et interpolation dans la
                        % largeur de la tuile.
                        Z = get_pixels_xy(Value, lonTile, latTile);
                        ZTile = my_flipud(Z);
                    else
                        if isscalar(Value)
                            ZTile = ones(length(subLatTile),length(subLonTile))*Value;
                        end
                    end
                    
                    % Ecriture de l'image/matrice de donn�es correspondant � la tuile
                    nomFic = fullfile(DirRaw, [num2str(indTileLat(YTuile),'%04d') '_' num2str(indTileLon(XTuile),'%04d') '.mat']);
                    if ~all(isnan(ZTile(:)))
                        % La substitution de Nan n'a pas encore eu lieu.
                        save(nomFic, 'ZTile');
                        
                        if obj.flagExportTexture && (obj.writeJpg == 1)
                            saveTileImage(obj, nomFic, ZTile);
                        end
                        
                    end
                    
                    % Trac� de d�buggage.
                    if flagDEBUG
                        % Calcul des param�tres de trac�s
                        W = length(lonTileDeb);
                        H = length(latTileDeb);
                        n= length(lonTileDeb)*(length(latTileDeb)- (YTuile - 1));
                        indFig = n - (length(lonTileDeb) - XTuile);
                        fig.handle = 1010;
                        fig.name = 'Cr�ation du niveau max';
                        latSub = cl_tile_ETOPO1.indTileNasaLat2Lat(indTileLat(YTuile), obj.sizeTile, pasPixDegTile) + (0:511) * pasPixDegTile;
                        lonSub = cl_tile_ETOPO1.indTileNasaLon2Lon(indTileLon(XTuile), obj.sizeTile, pasPixDegTile) + (0:511) * pasPixDegTile;
                        
                        % Trac�
                        cl_tile_ETOPO1.traceTile(fig, ZTile, latSub, lonSub, obj.cmap, H, W, indFig, pasPixDegTile, ...
                            indTileLat(YTuile), indTileLon(XTuile));
                    end
                    
                end % Fin de la boucle sur XTuile
                
                
                [stat, mess, id]=rmdir(DirRaw); %#ok<NASGU>
                
                hdlBarParcourus = hdlBarParcourus + length(lonTileDeb); %D�j� parcourus
                
            end % Fin de la boucle sur YTuile
            
            progressbar(hldBarTotal);
            progressbar('ready');
            % close(h)
            
        end % createLevelMax
        
        
        function obj = createLevelN(obj, LevelN, varargin)
            % M�thode createLevelN : Description d'une image sous forme de tuiles
            % de r�solution de niveau N (se basant sur la r�cup�ration de
            % tuiles de niveau N+1.
            %
            % SYNTAX :
            %   X = cl_tile_ETOPO1(...)
            %
            % PROPERTY NAMES / PROPERTY VALUES :
            %   ADU
            %
            % PROPERTY NAMES ONLY :
            %
            %
            % OUTPUT PARAMETERS :
            %   X : Variable
            %
            %
            % EXAMPLES :
            %   cf .cl_tile_ETOPO1
            %
            % SEE ALSO   : FormationMemmapfile Authors
            % AUTHORS    : JMA, GLU
            % VERSION    : $Id: cl_tile_ETOPO1.m,v 1.2 2003/11/12 12:46:11 augustin Exp $
            %-------------------------------------------------------------------------------
            global flagDEBUG;
            
            % R�cup�ration des arguments variables.
            LayerName       = getPropertyValue(varargin, 'LayerName',  obj.layerName);
            LayerSetName    = getPropertyValue(varargin, 'LayerSetName',  obj.layerSetName);
            %ValNaN          = getPropertyValue(varargin, 'ValNaN', obj.valNaN);
            %BornesData      = getPropertyValue(varargin, 'BornesData', obj.bornesData);
            BornesData      = obj.bornesData;
            
            Value = obj.value;
            % Calcul des indices de tuiles pour le niveau de r�solution et la zone g�o concern�e.
            pasPixDegTile = obj.pasPixel*2^(obj.nbLevels-LevelN-1);
            
            % Calcul des tuiles (indice et bornes selon les limites de latitudes et
            % de longitudes).
            Tiles = cl_tile_ETOPO1.computeTiles(BornesData.latS, BornesData.latN, BornesData.lonW, BornesData.lonE, obj.sizeTile, pasPixDegTile);
            indTileLon = Tiles.xIndiceTuile;
            indTileLat = Tiles.yIndiceTuile;
            
            
            hldBarTotal = numel(indTileLat)*numel(indTileLon);
            
            %Cr�ation du r�pertoire de num�ro du layer N (Echelle de N � 0)
            DirnameLayerSet = fullfile(LayerSetName ,LayerName);
            DirIndexLayerInf = fullfile(DirnameLayerSet, num2str(LevelN+1));
            if ~isfolder(DirnameLayerSet) || ~isfolder(DirIndexLayerInf)
                disp(lang('R�pertoire des layers inf�rieurs absent', 'Directory of inferiors levels absent'));
                return
            end
            DirIndexLayer = fullfile(DirnameLayerSet, num2str(LevelN));
            flag = cl_tile_ETOPO1.createDir(DirIndexLayer, 1); %#ok<NASGU>
            
            
            % Barre de progression
            
            % h = create_waitbar(DirIndexLayer, 'Entete', ['IFREMER - SonarScope : Creating Image Tile level ' DirIndexLayer(end)   ', please wait ...']);
            %             set(get(get(h, 'children'), 'Title'), 'Interpreter', 'none', 'String',nstrTitle);
            progressbar('descend', hldBarTotal, ['IFREMER - SonarScope : Creating Image Tile level ' DirIndexLayer(end)   ', please wait ...'],'EstTimeLeft','off');
            
            % Recherche des images � la r�solution sup�rieure pour les fusionner
            % 4 par 4.
            hdlBarParcourus = 0;
            nomLoadFic = {'', '', '', ''};
            for YTuile=1:length(indTileLat)
                % DirRaw = fullfile(DirIndexLayer, num2str(num2str(indTileLat(YTuile),'%04d')));
                DirRaw = fullfile(DirIndexLayer, num2str(num2str(indTileLat(YTuile),'%04d')));
                YIndice = (indTileLat(YTuile)* 2);
                flag = cl_tile_ETOPO1.createDir(DirRaw, 0); %#ok<NASGU>
                
                % MAJ de la barre de progression.
                progressbar(hdlBarParcourus+YTuile, Lang(['Cr�ation du niveau ' num2str(LevelN)], ['Creation of resolution Level ' num2str(LevelN)]));
                % my_waitbar((hdlBarParcourus + XTuile), hldBarTotal, h);
                for XTuile=1:length(indTileLon)
                    XIndice = (indTileLon(XTuile)* 2);
                    nomLoadFic{1} = cellstr(fullfile(DirIndexLayerInf, num2str(YIndice,'%04d'), [num2str(YIndice,'%04d') '_' num2str(XIndice,'%04d') '.mat']));
                    if exist(char(nomLoadFic{1}), 'file')
                        Z1 = loadmat(char(nomLoadFic{1}), 'nomVar', 'ZTile');
                    else
                        if get(Value, 'ImageType') == 2 % image en RGB (3 Tables)
                            Z1 = NaN(obj.sizeTile, obj.sizeTile, 3);
                        else
                            Z1 = NaN(obj.sizeTile, obj.sizeTile);
                        end
                    end
                    
                    nomLoadFic{2} = str2cell(fullfile(DirIndexLayerInf, num2str(YIndice,'%04d'), [num2str(YIndice,'%04d') '_' num2str(XIndice+1,'%04d') '.mat']));
                    if exist(char(nomLoadFic{2}), 'file')
                        Z2 = loadmat(char(nomLoadFic{2}), 'nomVar', 'ZTile');
                    else
                        if get(Value, 'ImageType') == 2
                            Z2 = NaN(obj.sizeTile, obj.sizeTile, 3);
                        else
                            Z2 = NaN(obj.sizeTile, obj.sizeTile);
                        end
                    end
                    
                    nomLoadFic{3} = str2cell(fullfile(DirIndexLayerInf, num2str(YIndice+1,'%04d'), [num2str(YIndice+1,'%04d') '_' num2str(XIndice,'%04d') '.mat']));
                    if exist(char(nomLoadFic{3}), 'file')
                        Z3 = loadmat(char(nomLoadFic{3}), 'nomVar', 'ZTile');
                    else
                        if get(Value, 'ImageType') == 2
                            Z3 = NaN(obj.sizeTile, obj.sizeTile, 3);
                        else
                            Z3 = NaN(obj.sizeTile, obj.sizeTile);
                        end
                    end
                    
                    nomLoadFic{4} = str2cell(fullfile(DirIndexLayerInf, num2str(YIndice+1,'%04d'), [num2str(YIndice+1,'%04d') '_' num2str(XIndice+1,'%04d') '.mat']));
                    if exist(char(nomLoadFic{4}), 'file')
                        Z4 = loadmat(char(nomLoadFic{4}), 'nomVar', 'ZTile');
                    else
                        if get(Value, 'ImageType') == 2
                            Z4 = NaN(obj.sizeTile, obj.sizeTile, 3);
                        else
                            Z4 = NaN(obj.sizeTile, obj.sizeTile);
                        end
                    end
                    
                    % Pour �viter le remplissage par des Nan en remontant
                    % les pyramides.
                    X = [Z3 Z4; Z1 Z2];
                    %                     X = fillNaN(X, [5 5], 'Mute', 1);
                    if ~all(isnan(X(:)))
                        % ZTile = my_mean([Z3 Z4; Z1 Z2]);
                        % ZTile = impyramid(X, 'reduce');
                        ZTile = my_impyramid(X);
                        
                        % Arrondi de ZTile pour conserver dans un format
                        % Int la donn�e (gani de place dans les fichiers
                        % .mat)
                        ZTile = round(ZTile);
                        
                        % nomFic = fullfile(DirRaw, [num2str(indTileLat(YTuile),'%04d') '_' num2str(indTileLon(XTuile),'%04d') '.mat']);
                        nomFic = fullfile(DirRaw, [num2str(indTileLat(YTuile),'%04d') '_' num2str(indTileLon(XTuile),'%04d') '.mat']);
                        % figure(1001); imagesc(x, y, ZTile); axis xy; axis tight; hold on; axis equal;
                        % Ecriture de l'image/matrice de donn�es correspondant � la tuile
                        % La substitution de Nan n'a pas encore eu lieu.
                        save(nomFic, 'ZTile');
                        if obj.flagExportTexture && (obj.writeJpg == 1)
                            saveTileImage(obj, nomFic, ZTile);
                        end
                        
                        if flagDEBUG
                            W = length(indTileLon);
                            H = length(indTileLat);
                            n = W * (H - (YTuile - 1));
                            indFig = n - (W - XTuile);
                            
                            % Trac� de la tuile
                            fig.handle = 1011+LevelN;
                            fig.name = ['Cr�ation du niveau N : ' num2str(LevelN)];
                            latSub = cl_tile_ETOPO1.indTileNasaLat2Lat(indTileLat(YTuile), obj.sizeTile, pasPixDegTile) + (0:511) * pasPixDegTile;
                            lonSub = cl_tile_ETOPO1.indTileNasaLon2Lon(indTileLon(XTuile), obj.sizeTile, pasPixDegTile) + (0:511) * pasPixDegTile;
                            cl_tile_ETOPO1.traceTile(fig, ZTile, latSub, lonSub, obj.cmap, H, W, indFig, pasPixDegTile, indTileLat(YTuile), indTileLon(XTuile));
                            
                        end
                    end
                end
                
                [stat,mess,id] = rmdir (DirRaw); %#ok<NASGU>
                
                hdlBarParcourus = hdlBarParcourus + length(indTileLon);
            end
            progressbar(hldBarTotal);
            progressbar('ready');
            % close(h)
        end % createLevelN
        
        function obj = writeTileLevelN(obj, LevelN, indWriteTilesLevelMax)
            %% Function writeTileLevelN
            % Impact sur les tuiles de niveau sup�rieur des modifications
            % effectu�es sur les tuiles de niveau max.
            % EXAMPLES:
            % [x, y, Z] = lec_etopoNGDC(cl_etopo([]), [41.9667 30.0333], [15.0333 35.9667], 'pasReso', 2);
            % b = cl_image('Image', Z, 'x', x, 'y', y, 'GeometryType', 3, 'DataType', 2);
            % MedEast = cl_tile_ETOPO1('LayerSetName', 'C:\Temp\SSC-Tiles', 'Value', b, 'LayerName', 'MedEast', 'WriteJpg', 1);
            % writeTile(MesEast, 0, []);
            %--------------------------------------------------------------------------
            global flagDEBUG;
            
            % R�cup�ration des arguments variables.
            LayerName       = obj.layerName;
            LayerSetName    = obj.layerSetName;
            % ValNaN          = obj.valNaN;
            Value           = obj.value;
            pasPixDegTile = obj.pasPixel*2^(obj.nbLevels-LevelN-1);
            
            % D�signation des tuiles de Niveau N impact�es par le niveau N
            % +1.
            if isempty(indWriteTilesLevelMax)
                BornesData = obj.bornesData;
                % Calcul des coordonn�es de l'image selon les bornes de
                % celle-ci ainsi que les indices de tuiles selon Nasa WW.
                [indTile, coordData]= cl_tile_ETOPO1.pixToIndTileAndCoord((1:BornesData.sizePixels(1))-1, (1:BornesData.sizePixels(2))-1, ...
                    obj.scaleLigCol, obj.sizeTile, pasPixDegTile); %#ok<NASGU>
                listeIndTileNasaLat = unique(indTile.latData);
                listeIndTileNasaLon = unique(indTile.lonData);
            else
                listeIndTileNasaLat = floor(indWriteTilesLevelMax.NasaLat/2^LevelN);
                listeIndTileNasaLon = floor(indWriteTilesLevelMax.NasaLon/2^LevelN);
            end
            
            
            jTuile = floor(indWriteTilesLevelMax.NasaLat/2^(obj.nbLevels-LevelN-1));
            iTuile = floor(indWriteTilesLevelMax.NasaLon/2^(obj.nbLevels-LevelN-1));
            H = length(unique(jTuile));
            W = length(unique(iTuile));
            hldBarTotal = H*W;
            
            %Cr�ation du r�pertoire de num�ro du layer N (Echelle de N � 0)
            DirnameLayerSet = fullfile(LayerSetName ,LayerName);
            DirIndexLayerInf = fullfile(DirnameLayerSet, num2str(LevelN+1));
            if ~isfolder(DirnameLayerSet) || ~isfolder(DirIndexLayerInf)
                disp(lang('R�pertoire des layers inf�rieurs absent', 'Directory of inferiors levels absent'));
                return
            end
            DirIndexLayer = fullfile(DirnameLayerSet, num2str(LevelN));
            
            
            % Barre de progression
            strTitle = sprintf('%s %s', 'Pyramid Files for Data Layer ',DirIndexLayer);
            h = create_waitbar(strTitle, 'Entete', Lang('IFREMER - SonarScope : Creation de la pyramide, patientez SVP', 'IFREMER - SonarScope : Pyramid Image Creation, please wait ...'));
            set(get(get(h, 'children'), 'Title'), 'Interpreter', 'none', 'String',strTitle);
            
            hdlBarParcourus = 0;
            % On n'impacte que les tuiles d�pendantes des tuiles d�sign�es.
            for j=1:length(listeIndTileNasaLat)
                jTuile = unique(floor(indWriteTilesLevelMax.NasaLat/2^(obj.nbLevels-LevelN-1)));
                DirRaw = fullfile(DirIndexLayer, num2str(num2str(jTuile(j),'%04d')));
                
                for i=1:length(listeIndTileNasaLon)
                    iTuile = unique(floor(indWriteTilesLevelMax.NasaLon/2^(obj.nbLevels-LevelN-1)));
                    nomLoadFic{1} = cellstr(fullfile(DirIndexLayerInf, num2str(listeIndTileNasaLat(j),'%04d'), [num2str(listeIndTileNasaLat(j),'%04d') '_' num2str(listeIndTileNasaLon(i),'%04d') '.mat'])); %#ok<AGROW>
                    if exist(char(nomLoadFic{1}), 'file')
                        Z1 = loadmat(char(nomLoadFic{1}), 'nomVar', 'ZTile');
                    else
                        if get(Value, 'ImageType') == 2
                            Z1 = NaN(obj.sizeTile, obj.sizeTile);
                        else
                            Z1 = NaN(obj.sizeTile, obj.sizeTile);
                        end
                    end
                    
                    nomLoadFic{2} = cellstr(fullfile(DirIndexLayerInf, num2str(listeIndTileNasaLat(j),'%04d'), [num2str(listeIndTileNasaLat(j),'%04d') '_' num2str(listeIndTileNasaLon(i)+1,'%04d') '.mat'])); %#ok<AGROW>
                    if exist(char(nomLoadFic{2}), 'file')
                        Z2 = loadmat(char(nomLoadFic{2}), 'nomVar', 'ZTile');
                    else
                        if get(Value, 'ImageType') == 2
                            Z2 = NaN(obj.sizeTile, obj.sizeTile);
                        else
                            Z2 = NaN(obj.sizeTile, obj.sizeTile);
                        end
                    end
                    
                    nomLoadFic{3} = cellstr(fullfile(DirIndexLayerInf, num2str(listeIndTileNasaLat(j)+1,'%04d'), [num2str(listeIndTileNasaLat(j)+1,'%04d') '_' num2str(listeIndTileNasaLon(i),'%04d') '.mat'])); %#ok<AGROW>
                    if exist(char(nomLoadFic{3}), 'file')
                        Z3 = loadmat(char(nomLoadFic{3}), 'nomVar', 'ZTile');
                    else
                        if get(Value, 'ImageType') == 2
                            Z3 = NaN(obj.sizeTile, obj.sizeTile);
                        else
                            Z3 = NaN(obj.sizeTile, obj.sizeTile);
                        end
                    end
                    
                    nomLoadFic{4} = cellstr(fullfile(DirIndexLayerInf, num2str(listeIndTileNasaLat(j)+1,'%04d'), [num2str(listeIndTileNasaLat(j)+1,'%04d') '_' num2str(listeIndTileNasaLon(i)+1,'%04d') '.mat'])); %#ok<AGROW>
                    if exist(char(nomLoadFic{4}), 'file')
                        Z4 = loadmat(char(nomLoadFic{4}), 'nomVar', 'ZTile');
                    else
                        if get(Value, 'ImageType') == 2
                            Z4 = NaN(obj.sizeTile, obj.sizeTile);
                        else
                            Z4 = NaN(obj.sizeTile, obj.sizeTile);
                        end
                    end
                    
                    if flagDEBUG
                        if all(isnan(Z1(:))) && all(isnan(Z2(:))) && all(isnan(Z3(:))) && all(isnan(Z4(:)))
                            disp(['Probl�me dans la recherche d''un fichier � mettre � jour : ' mfilename('class')]);
                        end
                    end
                    
                    % Pour �viter le remplissage par des Nan en remontant
                    % les pyramides.
                    X = [Z3 Z4; Z1 Z2];
                    %                     X = fillNaN(X, [3 3], 'Mute', 1);
                    % ZTile = my_mean([Z3 Z4; Z1 Z2]);
                    if ~all(isnan(X(:)))
                        %                         ZTile = impyramid(X, 'reduce');
                        ZTile = my_impyramid(X);
                        
                        % Ecriture de l'image/matrice de donn�es correspondant � la tuile
                        nomFic = fullfile(DirRaw, [num2str(jTuile(j),'%04d') '_' num2str(iTuile(i),'%04d') '.mat']);
                        % La substitution de Nan n'a pas encore eu lieu.
                        save(nomFic, 'ZTile');
                        
                        if obj.writeJpg == 1
                            saveTileImage(obj, nomFic, ZTile);
                        end
                    end
                    
                    if flagDEBUG
                        n = W * (H - (iTuile - 1));
                        indFig = n - (W - iTuile);
                        fig.handle = 1031+LevelN;
                        fig.name = ['Ecriture du niveau N : ' num2str(LevelN)];
                        latSub = cl_tile_ETOPO1.indTileNasaLat2Lat(jTuile(j), obj.sizeTile, pasPixDegTile) + (0:511) * pasPixDegTile;
                        lonSub = cl_tile_ETOPO1.indTileNasaLon2Lon(iTuile(i), obj.sizeTile, pasPixDegTile) + (0:511) * pasPixDegTile;
                        cl_tile_ETOPO1.traceTile(fig, ZTile, latSub, lonSub, obj.cmap, H, W, indFig, pasPixDegTile, jTuile, iTuile);
                    end
                    
                    % MAJ de la barre de progression.
                    my_waitbar((hdlBarParcourus + i), hldBarTotal, h);
                end
                hdlBarParcourus = hdlBarParcourus + length(listeIndTileNasaLon);
            end
            close(h);
        end  % writeTileLevelN
        
        function [extractI, extractObj, extractTiles] = readTileEtopo(obj, LevelN, subx, suby)
            %% Function readTileEtopo
            % Lecture d'une arbo cl_tile_ETOPO1
            %-------------------------------------------------------------------------            
            global flagDEBUG
                                
            % R�cup�ration des arguments variables.
            LayerName       = obj.layerName;
            LayerSetName    = obj.layerSetName;
            % ValNaN = obj.valNaN;
            % Calcul des indices de tuiles concern�es par la donn�e.
            pasPixDegTile = obj.pasPixel*2^(obj.nbLevels-LevelN-1);
            
            
            DirnameLayerSet = fullfile(LayerSetName ,LayerName);
            DirIndexLayer = fullfile(DirnameLayerSet, num2str(LevelN));
            if ~isfolder(DirnameLayerSet) || ~isfolder(DirIndexLayer)
                disp(lang('R�pertoire des layers inf�rieurs absent', 'Directory of inferiors levels absent'));
                return
            end
            
            % D�termination des tuiles de la zone concern�e.
            pppp = cl_tile_ETOPO1.computeTiles(suby(1), suby(2), subx(1), subx(2), obj.sizeTile, pasPixDegTile);
            
            % D�termination des tuiles de la zone d'origine.
            OrigTiles = cl_tile_ETOPO1.computeTiles(obj.bornesData.latS, obj.bornesData.latN, obj.bornesData.lonW, obj.bornesData.lonE, obj.sizeTile, pasPixDegTile);
            
            % Intersection des 2.
            Tiles.xIndiceTuile = intersect(pppp.xIndiceTuile, OrigTiles.xIndiceTuile);
            Tiles.yIndiceTuile = intersect(pppp.yIndiceTuile, OrigTiles.yIndiceTuile);
            Tiles.XTileDeb = intersect(pppp.XTileDeb, OrigTiles.XTileDeb);
            Tiles.XTileFin = intersect(pppp.XTileFin, OrigTiles.XTileFin);
            Tiles.YTileDeb = intersect(pppp.YTileDeb, OrigTiles.YTileDeb);
            Tiles.YTileFin = intersect(pppp.YTileFin, OrigTiles.YTileFin);
            
            indTileLat = Tiles.yIndiceTuile;
            indTileLon = Tiles.xIndiceTuile;
            % Calcul des bornes des tuiles de la zone concern�e.
            lonSetTiles = linspace(Tiles.XTileDeb(1), Tiles.XTileFin(end), length(Tiles.XTileDeb)*obj.sizeTile);
            iLonTiles = find((lonSetTiles >= subx(1)) & (lonSetTiles <= subx(end)));
            
            latSetTiles = linspace(Tiles.YTileDeb(1), Tiles.YTileFin(end), length(Tiles.YTileDeb)*obj.sizeTile);
            % Les matrices MatLab d�butent en haut � gauche contrairement � notre rep�re :
            % latitude/longitude: en bas � gauche.
            latSetTiles = fliplr(latSetTiles);
            iLatTiles = find((latSetTiles >= suby(1)) & (latSetTiles <= suby(end)));
            
            % D�termination de la taille de la future zone.
            nbLig = ((indTileLat(end) - indTileLat(1)) + 1)*obj.sizeTile;
            nbCol = ((indTileLon(end) - indTileLon(1)) + 1)*obj.sizeTile;
            
            try
                extractI = NaN(nbLig, nbCol, 'single');
            catch  %#ok<CTCH>
                extractI = cl_memmapfile('Filename', tempname, 'Size', [nbLig nbCol], 'Format', 'single');
            end
            
            
            % Composition de l'image globale constitu�e de toutes les
            % tuiles enti�res.
            for jTuile=length(indTileLat):-1:1
                YIndice = indTileLat(jTuile);
                for iTuile=1:length(indTileLon)
                    XIndice = indTileLon(iTuile);
                    nomLoadFic =fullfile(DirIndexLayer, num2str(YIndice,'%04d'), [num2str(YIndice,'%04d') '_' num2str(XIndice,'%04d') '.mat']);
                    subxTile(1) = (iTuile-1)*obj.sizeTile+1;
                    subxTile(2) = (iTuile)*obj.sizeTile;
                    subyTile(1) = (length(indTileLat)-jTuile)*obj.sizeTile+1;
                    subyTile(2) = (length(indTileLat)-jTuile)*obj.sizeTile + obj.sizeTile;
                    flagExist = exist(nomLoadFic, 'file');
                    if flagExist
                        ZTile = loadmat(nomLoadFic, 'nomVar', 'ZTile');
                    else
                        ZTile = NaN(obj.sizeTile, obj.sizeTile);
                    end
                    
                    extractI(subyTile(1):subyTile(2), subxTile(1):subxTile(2)) = ZTile;
                    
                    % Trac� de DEBUG des tuiles concern�es.
                    if (flagDEBUG)
                        if (iTuile+1) < length(indTileLon)
                            lonCoord = linspace(Tiles.XTileDeb(iTuile), Tiles.XTileDeb(iTuile+1), obj.sizeTile);
                        else
                            lonCoord = linspace(Tiles.XTileDeb(iTuile), Tiles.XTileFin(iTuile), obj.sizeTile);
                        end
                        if jTuile > 1
                            latCoord = linspace(Tiles.YTileDeb(jTuile), Tiles.YTileDeb(jTuile-1), obj.sizeTile);
                        else
                            latCoord = linspace(Tiles.YTileDeb(jTuile), Tiles.YTileFin(jTuile), obj.sizeTile);
                        end
                        figure(123456);imagesc(lonCoord, latCoord, ZTile); colormap(obj.cmap); axis xy;
                    end
                end
            end
            
            % Calcul des Latitudes/Longitudes des tuiles pour les bornes souhait�es.
            Lon = lonSetTiles(iLonTiles); %#ok<FNDSB>
            Lat = fliplr(latSetTiles(iLatTiles)); %#ok<FNDSB>
            
            % Extraction de l'information utile (sans les NaN). Il peut y
            % avoir dans les niveaux sup�rieurs des compl�ments de tuiles
            % en NaN.
            extractI = extractI(iLatTiles, iLonTiles);  
            
            
            % Image complete
            Carto = cl_carto('Ellipsoide.Type', 11, 'Projection.Type', 2, ...
                'Projection.Mercator.long_merid_orig', my_nanmean(Lon), ...
                'Projection.Mercator.lat_ech_cons',    my_nanmean(Lat));
            Comment = Lang('ETOPO1', 'ETOPO1');
            
            %'x', lonSetTiles, 'y', fliplr(latSetTiles), ...
            extractObj = cl_image('Image', extractI, 'Titre', obj.layerName, ...
                'InitialImageName', obj.layerName, ...
                'x', Lon, 'y', Lat, ...
                'DataType', identDataType(cl_image_I0, 'Bathymetry'), ...
                'Unit', 'm', 'XUnit', 'deg', 'YUnit', 'deg', ...
                'ColormapIndex', 3, 'GeometryType', LatLong(cl_image_I0), ...
                'Comments', Comment, 'Carto', Carto);
            % imagesc(c);
            
            % Troncature de l'image globale compos�e
            extractObj = set_XLim(extractObj, [Lon(1) Lon(end)]);
            if Lat(end) < Lat(1)
                extractObj = set_YLim(extractObj, [Lat(end) Lat(1)]);
            else
                extractObj = set_YLim(extractObj, [Lat(1) Lat(end)]);
            end
            
            extractTiles.xTiles = Tiles.xIndiceTuile;
            extractTiles.yTiles = Tiles.yIndiceTuile;
            
            
        end % Fonction readTileEtopo
        
        
    end % methods(Access='public')
    %################################################################################################
    methods(Access='private')
        
        function [Value, subNaN] = subsValNaN(obj, Value)
            %% Function subsValNaN
            % Susbtitution des valeurs de NaN de value par la valeur NaN de
            % l'objet.
            %
            % SYNTAX :
            %   Val = subsValNaN(obj, Val)
            %
            % INPUT PARAMETERS :
            %   obj  : Instance de cl_memtile
            %   Val : Valeur o� susbtituer
            %
            % OUTPUT PARAMETERS :
            %   Val : Valeur apr�s susbtitution
            %
            % EXAMPLES :
            %    subsValNaN(obj, 'NaN')
            %
            % SEE ALSO   : cl_tile_ETOPO1 Authors
            % AUTHORS    : GLU
            % VERSION    : $Id: cl_tile_ETOPO1.m,v 1.2 2008/08/12 12:46:11 rgallou Exp $
            %--------------------------------------------------------------------------
            % Remplissage par Nan des valeurs vues comme Nan.
            
            if isnan(obj.valNaN)
                subNaN = isnan(Value);
            else
                % Cas o� la valeur de codage n'est pas NaN mais qu'il y a
                % quand m�me des NaN dans l'image, c'est arriv�)
                n3 = size(Value,3);
                subNaN = isnan(sum(Value,3)) | (sum(Value == obj.valNaN, 3) == n3);
                subNaN = repmat(subNaN, [1,1,n3]);
                Value(subNaN) = obj.valNaN;
                %                 if any(subNaN)
                %                     switch class(Value)
                %                         case {'double'; 'single'}
                %                         otherwise
                %                             Value = single(Value);
                %                     end
                %                     for i=1:size(Value,3)
                %                         X = Value(:,:,i);
                %                         X(subNaN) = obj.valNaN;
                %                         Value(:,:,i) = X;
                %                     end
                %                 end
            end
            
        end % subsValN
        
        
        function [obj, indTilesToWrite] = writeTile(obj, Value, pixLatToWrite, pixLonToWrite, pixLinearToWrite)
            % Function writeTile
            % Ecriture/modification d'une arbo cl_tile_ETOPO1
            %--------------------------------------------------------------------------
            global flagDEBUG
            
            pasPixDegTile = obj.pasPixel;
            
            % Calcul des coordonn�es de l'image selon les bornes de
            % celle-ci ainsi que les indices de tuiles selon Nasa WW.
            % D�termination des tuiles � lire.
            [indTile, coordData]= cl_tile_ETOPO1.pixToIndTileAndCoord(pixLatToWrite-1, pixLonToWrite-1, ...
                obj.scaleLigCol, obj.sizeTile, pasPixDegTile);
            indTileNasaLatToWrite = indTile.latData;
            indTileNasaLonToWrite = indTile.lonData;
            lonToWrite = coordData.lonData;
            latToWrite = coordData.latData;
            
            % Cr�ation du r�pertoire du sous-ensemble des Layers
            LevelMax = obj.nbLevels-1; % NumLayer (au sens de NWW) d�croit jusqu'� 0
            DirnameLayerSet = fullfile(obj.layerSetName ,obj.layerName);
            
            % Cr�ation du r�pertoire de num�ro du layer N (Echelle de N � 0)
            DirIndexLayer = fullfile(DirnameLayerSet, num2str(LevelMax));
            
            % Utiles l'impact sur les tuiles de plus haut niveau.
            indTilesToWrite.NasaLat = [];
            indTilesToWrite.NasaLon = [];
            
            % Calcul du nombre de tuiles � impacter.
            [listeIndTileNasaLatToWrite, pppp, indjT] = unique(indTileNasaLatToWrite); %#ok<ASGLU>
            hdlBarParcourus = 0;
            hldBarTotal = 0;
            for j=1:length(listeIndTileNasaLatToWrite)
                subj = find(indjT == j);
                [listeIndTileNasaLonToWrite, pppp, indiT] = unique(indTileNasaLonToWrite(subj)); %#ok<FNDSB,NASGU>
                hldBarTotal = hldBarTotal + length(listeIndTileNasaLonToWrite);
            end
            
            
            % Cr�ation des tuiles du niveau le plus bas (haute r�solution)
            strTitle = ['Writing Tiling Files : ' DirIndexLayer];
            h = create_waitbar(strTitle, 'Entete', Lang('IFREMER - SonarScope : Creation de la pyramide, patientez SVP', 'IFREMER - SonarScope : Pyramid Image Creation, please wait ...'));
            subLatValToWrite = size(Value, 1);
            for YTuile=1:length(listeIndTileNasaLatToWrite)
                subj = find(indjT == YTuile);
                
                [listeIndTileNasaLonToWrite, pppp, indiT] = unique(indTileNasaLonToWrite(subj)); %#ok<ASGLU>
                % Cr�ation du r�pertoire de Latitudes.
                DirRaw = fullfile(DirIndexLayer, num2str(listeIndTileNasaLatToWrite(YTuile),'%04d'));
                if ~isDir(DirRaw)
                    my_warndlg(Lang('R�pertoires de tuiles absent en lecture ','Tiling Directory does not exist during Reading'), 1);
                    return
                end
                
                subLonValToWrite = size(Value, 2);
                for XTuile=1:length(listeIndTileNasaLonToWrite)
                    subi = find(indiT == XTuile);
                    
                    nomFic = fullfile(DirRaw, [num2str(listeIndTileNasaLatToWrite(YTuile),'%04d') '_' num2str(listeIndTileNasaLonToWrite(XTuile),'%04d') '.mat']);
                    if exist(nomFic, 'file')
                        ZTile = loadmat(nomFic, 'nomVar', 'ZTile');
                        ZTile = my_flipud(ZTile); % Pour traiter les coordonn�es � partir du point haut � gauche.
                    else
                        if get(Value, 'ImageType') == 2
                            ZTile = NaN(obj.sizeTile, obj.sizeTile);
                        else
                            ZTile = NaN(obj.sizeTile, obj.sizeTile);
                        end
                    end
                    
                    indTile = sub2ind([length(listeIndTileNasaLatToWrite) length(listeIndTileNasaLonToWrite)], YTuile, XTuile);
                    indTilesToWrite.NasaLon(indTile) = listeIndTileNasaLonToWrite(XTuile);
                    indTilesToWrite.NasaLat(indTile) = listeIndTileNasaLatToWrite(YTuile);
                    
                    % Calcul des indices Lat/Long de la tuile � �crire.
                    lonSub = lonToWrite(subj(subi));
                    latSub = latToWrite(subj(subi));
                    pixLonInTile = cl_tile_ETOPO1.lon2pixLonInTile(lonSub, listeIndTileNasaLonToWrite(XTuile), obj.sizeTile, pasPixDegTile);
                    pixLatInTile = cl_tile_ETOPO1.lat2pixLatInTile(latSub, listeIndTileNasaLatToWrite(YTuile), obj.sizeTile, pasPixDegTile);
                    
                    % Extraction de la valeur � �crire.
                    subPixLatInTile = unique(pixLatInTile);
                    subPixLonInTile = unique(pixLonInTile);
                    nbPixLatInTile = length(subPixLatInTile);
                    nbPixLonInTile = length(subPixLonInTile);
                    
                    
                    if ~isempty(pixLinearToWrite)
                        % ! Probl�me car le nombre de lat-lon issus des
                        % calculs n'est pas �gal � la valeur � �crire.
                        while numel(subPixLatInTile) > numel(subPixLonInTile)
                            subPixLatInTile(end) = [];
                        end
                        while numel(subPixLatInTile) < numel(subPixLonInTile)
                            subPixLonInTile(end) = [];
                            nbPixLonInTile = nbPixLonInTile - 1;
                        end
                        try
                            % Extraction de la valeur � partir du bas de
                            % la valeur � �crire.
                            pixLinearFromValue = subLonValToWrite-(nbPixLonInTile-1):subLonValToWrite;
                            pixLinearInTile = sub2ind([obj.sizeTile obj.sizeTile], ...
                                subPixLatInTile,...
                                subPixLonInTile);
                        catch ME
                            my_warndlg(['Probl�me dans la verticalisation des donn�es de writeTile' ME],1);
                        end
                        % Ecriture de la donn�e sous forme de vecteurs.
                        ValToWrite = Value(pixLinearFromValue);
                        % Ecriture de la valeur modif�e.
                        ZTile(pixLinearInTile) = ValToWrite;
                    else
                        ValToWrite = Value(subLatValToWrite-(nbPixLatInTile-1):subLatValToWrite, ...
                            subLonValToWrite-(nbPixLonInTile-1):subLonValToWrite);
                        % Interpolation n�cessaire pour �crire sur l'intervalle
                        % complet de coordonn�es (pixLat et pixLon ne sont pas
                        % continus).
                        interp2PixLatInTile = min(pixLatInTile):max(pixLatInTile);
                        interp2PixLonInTile = min(pixLonInTile):max(pixLonInTile);
                        dimsVal = size(ValToWrite);
                        if all(dimsVal~=1)
                            ValToWrite2 = interp2(subPixLonInTile, fliplr(subPixLatInTile)', double(ValToWrite),interp2PixLonInTile,fliplr(interp2PixLatInTile)');
                        elseif all(dimsVal) == 1
                            ValToWrite2 = ValToWrite;
                        else
                            if numel(subPixLonInTile) == 1
                                ValToWrite2 = interp1(subPixLatInTile', double(ValToWrite),interp2PixLatInTile');
                            else
                                ValToWrite2 = interp1(subPixLonInTile', double(ValToWrite),interp2PixLonInTile');
                            end
                        end
                        % Ecriture de la valeur modif�e.
                        ZTile(interp2PixLatInTile, interp2PixLonInTile) = ValToWrite2;
                    end
                    
                    subLonValToWrite = subLonValToWrite - length(subPixLonInTile);
                    
                    ZTile = fillNaN(ZTile, [5 5], 'Mute', 1);
                    ZTile = my_flipud(ZTile); % Pour compenser le my_flipud pr�c�dent.
                    if ~all(isnan(ZTile(:)))
                        % La substitution de Nan n'a pas encore eu lieu.
                        save(nomFic, 'ZTile');
                        
                        if obj.writeJpg == 1
                            saveTileImage(obj, nomFic, ZTile);
                        end
                    end
                    
                    
                    % Affichage des figures modifi�es.
                    if flagDEBUG
                        H = length(listeIndTileNasaLatToWrite);
                        W = length(listeIndTileNasaLonToWrite);
                        n = length(listeIndTileNasaLonToWrite)*(length(listeIndTileNasaLatToWrite)- (YTuile - 1));
                        indFig = n - (length(listeIndTileNasaLonToWrite) - XTuile);
                        
                        fig.handle = 1030;
                        fig.name = 'Ecriture du niveau max';
                        cl_tile_ETOPO1.traceTile(fig, my_flipud(ZTile), latSub, lonSub, obj.cmap, H, W, indFig, pasPixDegTile, listeIndTileNasaLatToWrite(YTuile), listeIndTileNasaLonToWrite(XTuile));
                    end % Boucle sur les indices de Tuiles en Longitude
                    
                    my_waitbar((hdlBarParcourus+XTuile), hldBarTotal, h);
                    
                end % Boucle sur les indices de Tuiles en Latitude
                subLatValToWrite = subLatValToWrite - length(subPixLatInTile);
                hdlBarParcourus = hdlBarParcourus + length(listeIndTileNasaLonToWrite); %D�j� parcourus
            end
            close(h);
            
        end % Fonction writeTile
        
        function saveTileImage(obj, nomFic, ZTile)
            % Function saveTileImage
            % Sauvegarde dans l'arborescence des fichiers Image.
            %--------------------------------------------------------------------------
            
            FormatTile = obj.formatTile;
            Value = obj.value;
            
            % Ecriture du z�ro � la place du Nan pour la donn�e � �crire.
            nomFic = strrep(nomFic, '.mat', ['.' FormatTile]);
            % Remplissage des valeurs NaN par la valeur NaN sp�cifique de
            % l'objet.
            ZTile2 = ZTile;
            
            [ZTile, subNaN] = subsValNaN(obj, ZTile);
            
            
            if isnan(obj.valNaN)
                if all(isnan(ZTile(:)))
                    return
                end
            else
                if all(ZTile(:) == obj.valNaN)
                    return
                end
            end
            
            switch FormatTile
                case 'bil'
                    fid = fopen(nomFic, 'w');
                    pppp =  int16(ZTile(:));
                    fwrite(fid, pppp, 'int16');
                    fclose(fid);
                otherwise
                    if get(Value, 'ImageType') == 2
                        switch FormatTile
                            case 'jpg'
                                % Test pour tenter d'�liminer les probl�mes
                                % de bords
                                ZTile(~subNaN) = max(3,ZTile(~subNaN));
                                ZTile = min(255,ZTile);
                                imwrite(uint8(floor(ZTile)), nomFic, FormatTile, 'Quality', 100);
                                
                            case 'png'
                                alpha = ZTile;
                                alpha(ZTile==0)=0;
                                alpha(ZTile~=0)=1;
                                ZTile(~subNaN) = max(3,ZTile(~subNaN));
                                ZTile = min(255,ZTile);
                                % imwrite(uint8(floor(ZTile)), nomFic, FormatTile, 'Transparency',[0,0,0]);
                                imwrite(uint8(floor(ZTile)), nomFic, FormatTile, 'Alpha',alpha);
                            otherwise
                                % Test pour tenter d'�liminer les probl�mes de bords
                                ZTile(~subNaN) = max(3,ZTile(~subNaN));
                                ZTile = min(255,ZTile);
                                imwrite(uint8(floor(ZTile)), nomFic, FormatTile);
                        end
                    else
                        CLim = double(get(Value,'CLim'));
                        I = mat2gray(ZTile, CLim); % I entre 0 et 1
                        I = floor(I*256+1);
                        
                        Cmap = obj.cmap;
                        if get(Value, 'Video') == 2
                            Cmap = flipud(Cmap);
                        end
                        
                        switch FormatTile
                            case 'gif'
                                imwrite(uint8(I), Cmap(2:end,:), nomFic, 'TransparentColor', 0);
                                
                            case 'jpg'
                                I(I < 3) = 3;
                                Cmap = [0 0 0; Cmap];
                                I = ind2rgb(I,Cmap);
                                
                                for i=1:3
                                    X = I(:, :, i);
                                    X(subNaN) = 0;
                                    I(:, :, i)= X;
                                end
                                imwrite(uint8(floor(I*255)), nomFic, 'jpg', 'Quality', 100);
                                
                            case 'png'
                                
                                alpha = ZTile2;
                                subNan = isnan(alpha);
                                alpha(subNan)=0;
                                alpha(~subNan)=1;
                                I(I < 3) = 3;
                                Cmap = [0 0 0; Cmap];
                                I = ind2rgb(I,Cmap);
                                
                                for i=1:3
                                    X = I(:, :, i);
                                    X(subNaN) = 0;
                                    I(:, :, i)= X;
                                end
                                imwrite(uint8(floor(I*255)), nomFic, FormatTile, 'Alpha',alpha);
                                
                            otherwise
                                Cmap = [0 0 0; Cmap];
                                I = ind2rgb(I,Cmap);
                                
                                for i=1:3
                                    X = I(:, :, i);
                                    X(subNaN) = 0;
                                    I(:, :, i)= X;
                                end
                                imwrite(uint8(floor(I*255)), nomFic, 'jpg');
                        end
                    end
            end
        end
        
    end % methods(Access='private', Access='private')
    
    %################################################################################################
    methods(Static)
        
         function obj = linkLayerEtopo(dirLayer,layerName)
            % Function linkLayerEtopo
            % Pointage vers le layer � disposition de l'utilisateur.
            % Example :
            %   dirName = 'C:\Temp\SonarScope-Tiles\';
            %   layerName = 'Etopo1';
            %   obj = linkLayerEtopo(dirName, layerName)
            % -------------------------------------------------------------------------
            nameFicXML = fullfile(dirLayer, [layerName '.xml']);
            obj = [];
            if ~exist(nameFicXML, 'file')
                chaineFR = 'Vous ne disposez pas de la structure ETOPO1 ou du fichier demand�. SVP Contacter Jean-Marie AUGUSTIN, jean-marie.augustin@ifremer.fr, IFREMER, NSE/AS, 02 98 22 47 03';
                chaineUS = 'You don''t have ETOPO1 basic data. Please contact, Jean-Marie AUGUSTIN, jean-marie.augustin@ifremer.fr, IFREMER, NSE/AS, 02 98 22 47 03';
                my_warndlg(Lang(chaineFR, chaineUS),1);
                return
            end
            
            % Lecture successive des XML de description des Layers cr�es par
            % l'utilisateur.
            xDoc = xmlread(nameFicXML);
            
            dummy = xDoc.getElementsByTagName('Name');
            obj.layerName = layerName;
            
            dummy = xDoc.getElementsByTagName('North');
            pppp = dummy.item(0);
            obj.bornesData.latN = str2double(pppp.getTextContent);
            
            dummy = xDoc.getElementsByTagName('South');
            pppp = dummy.item(0);
            obj.bornesData.latS = str2double(pppp.getTextContent);
            
            dummy = xDoc.getElementsByTagName('West');
            pppp = dummy.item(0);
            obj.bornesData.lonW = str2double(pppp.getTextContent);
            
            dummy = xDoc.getElementsByTagName('East');
            pppp = dummy.item(0);
            obj.bornesData.lonE = str2double(pppp.getTextContent);
            
            dummy = xDoc.getElementsByTagName('TextureSizePixels');
            pppp = dummy.item(0);
            obj.sizeTile = str2double(pppp.getTextContent);
            
            dummy = xDoc.getElementsByTagName('ImageFileExtension');
            pppp = dummy.item(0);
            obj.formatTile = char(pppp.getTextContent);
            
            dummy = xDoc.getElementsByTagName('LevelZeroTileSizeDegrees');
            pppp = dummy.item(0);
            obj.lztsd = str2double(pppp.getTextContent);
            
            dummy = xDoc.getElementsByTagName('NumberLevels');
            pppp = dummy.item(0);
            obj.nbLevels = str2double(pppp.getTextContent);
            
            dummy = xDoc.getElementsByTagName('Resolution');
            pppp = dummy.item(0);
            obj.pasPixel = str2double(pppp.getTextContent);
            
            dummy = xDoc.getElementsByTagName('SizeY');
            pppp = dummy.item(0);
            obj.sizeY = str2double(pppp.getTextContent);
            
            dummy = xDoc.getElementsByTagName('SizeX');
            pppp = dummy.item(0);
            obj.sizeX = str2double(pppp.getTextContent);
            
            % D�finition de la structure 
            B.sizePixels = [obj.sizeY obj.sizeX];
            B.latS = obj.bornesData.latS;
            B.latN = obj.bornesData.latN;
            B.lonE = obj.bornesData.lonE;
            B.lonW = obj.bornesData.lonW;
            
            %% Pointage vers des tuiles existantes
            obj = cl_tile_ETOPO1('LayerName', layerName, 'BornesData', B, ...
                                'FormatTile','png');
        
        end % Fonction linkLayerEtopo
        
        
        function indTileNasaLon = Lon2indTileNasaLon(Lon, sizeTile, pasPixDegTile)
            % Function Lon2indTileNasaLon
            % Translation des longitudes (deg) en num�ros de tuile
            % -------------------------------------------------------------
            % indTileNasaLon = 1 + floor((Lon + 180) / (sizeTile*pasPixDegTile));
            indTileNasaLon = floor((Lon + 180) / (sizeTile*pasPixDegTile));
        end %Lon2indTileNasaLon
        
        function indTileNasaLat = Lat2indTileNasaLat(Lat, sizeTile, pasPixDegTile)
            % Function Lat2indTileNasaLat
            % Translation des latitudes (deg) en num�ros de tuile
            % -------------------------------------------------------------
            indTileNasaLat = floor((Lat + 90)  / (sizeTile*pasPixDegTile));
        end %Lat2indTileNasaLat
        
        function Lon = indTileNasaLon2Lon(indTileNasaLon, sizeTile, pasPixDegTile)
            % Function indTileNasaLon2Lon
            % Translation des num�ros de tuile en longitudes (deg) (d�but de
            % tuile).
            % -------------------------------------------------------------
            Lon = indTileNasaLon * (sizeTile*pasPixDegTile) - 180;
        end %indTileNasaLon2Lon
        
        function Lat = indTileNasaLat2Lat(indTileNasaLat, sizeTile, pasPixDegTile)
            % Function indTileNasaLat2Lat
            % Translation des num�ros de tuile en latitudes (deg) (d�but de
            % tuile).
            % -------------------------------------------------------------
            Lat = indTileNasaLat * (sizeTile*pasPixDegTile) - 90;
        end %indTileNasaLat2Lat
        
        function lonData = pixDataLon2Lon(indDataLon, a, b)
            % Function pixDataLon2Lon
            % Translation des indices de colonnes de donn�es en longitudes (deg) absolues.
            % -------------------------------------------------------------
            lonData = a * indDataLon + b;
        end %pixDataLon2Lon
        
        function latData = pixDataLat2Lat(indDataLat, a, b)
            % Function pixDataLat2Lat
            % Translation des indices de lignes de donn�es en latitudes (deg) absolues.
            % -------------------------------------------------------------
            latData = a * indDataLat + b;
        end %pixDataLat2Lat
        
        function pixLonInTile = lon2pixLonInTile(lon, indTileNasaLon, sizeTile, pasPixDeg)
            % Function lon2pixLonInTile
            % Translation des longitudes (deg) en num�ro de pixels dans le
            % rep�re de chaque tuile.
            % -------------------------------------------------------------
            pixLonInTile = 1 + floor((lon - (-180+pasPixDeg*sizeTile*(indTileNasaLon)))/pasPixDeg);
        end %lon2pixLonInTile
        
        function pixLatInTile = lat2pixLatInTile(lat, indTileNasaLat, sizeTile, pasPixDeg)
            % Function lat2pixLatInTile
            % Translation des latitudes (deg) en num�ro de pixels dans le
            % rep�re de chaque tuile.
            % -------------------------------------------------------------
            pixLatInTile = 1+ floor((lat - (-90+pasPixDeg*sizeTile*(indTileNasaLat)))/pasPixDeg);
        end %lat2pixLatInTile
        
        function [indTile, absCoordData]= pixToIndTileAndCoord(indPixLat, indPixLon, ScaleFactor, sizeTile, pasPixDeg)
            % Function pixToIndTileAndCoord
            % Transposition des indices de colonnes de la donn�e d'origine (pixels) en num�ros de Tuiles
            % -------------------------------------------------------------
            % D�termination des tuiles � lire.
            LonData = cl_tile_ETOPO1.pixDataLon2Lon(indPixLon, ScaleFactor.aCol, ScaleFactor.bCol);
            % LatData = cl_tile_ETOPO1.pixDataLat2Lat(indPixLat, ScaleFactor.aLig, ScaleFactor.bLig);
            LatData = cl_tile_ETOPO1.pixDataLat2Lat(indPixLat, -ScaleFactor.aLig, ScaleFactor.bLig);
            
            % Pour limiter les tailles de tableaux pour le cas
            % d'indexation volumineuse.
            LonData = unique(LonData);
            LatData = unique(LatData);
            
            [X,Y] = meshgrid(LonData, LatData);
            LonData = X(:);
            LatData = Y(:);
            
            indTileNasaLon = cl_tile_ETOPO1.Lon2indTileNasaLon(LonData, sizeTile, pasPixDeg);
            indTileNasaLat = cl_tile_ETOPO1.Lat2indTileNasaLat(LatData, sizeTile, pasPixDeg);
            
            absCoordData.lonData = LonData;
            absCoordData.latData = LatData;
            indTile.lonData = indTileNasaLon;
            indTile.latData = indTileNasaLat;
        end %pixToIndTileAndCoord
        
        function flag = createDir(layerDirName, flagWarning)
            % Function createDir
            % Cr�ation de r�pertoire de Layer
            % ------------------------------------------------------------
            flag = 0; %#ok<NASGU>
            warning off MATLAB:MKDIR:DirectoryExists
            try
                
                %  if isfolder(layerDirName)
                %      rmdir(layerDirName, 's');
                %  end
            catch %#ok<CTCH>
                if flagWarning
                    disp(Lang(['R�pertoire ' layerDirName ' absent. Pas de suppression possible'], ['Directory ' layerDirName ' absent. No erase available.']));
                end
            end
            
            flag = mkdir(layerDirName);
            warning on MATLAB:MKDIR:DirectoryExists
            if ~flag
                return
            end
        end % createDir
        
        
        
        function traceTile(strFig, ZTile, latCoord, lonCoord, tabColor, H, W, indSubPlot, pasPixDegTile, ...
                indTileNasaLat, indTileNasaLon) %#ok<INUSL>
            % Function traceTile
            % Trac� de la tuile.
            % ------------------------------------------------------------
            
            hdlFigure = strFig.handle;
            nameFig = strFig.name;
            if ~ishandle(hdlFigure)
                hdlFigure = figure(hdlFigure);
            end
            
            latCoord = fliplr(latCoord);
            
            set(0, 'CurrentFigure', hdlFigure);
            subplot(H, W, indSubPlot);
            
            if ndims(ZTile) > 2 && max(ZTile(:)) > 1
                imagesc(lonCoord, latCoord, uint8(floor(ZTile))); colormap(tabColor); axis xy;
            else
                imagesc(lonCoord, latCoord, ZTile); colormap(tabColor); axis xy;
            end
            if H*W > 20
                FontSize = 8;
            else
                if H*W > 12
                    FontSize = 10;
                else
                    FontSize = 14;
                end
            end
            
            text(median(lonCoord), median(latCoord), ...
                ['(' num2str(indTileNasaLat) ',' num2str(indTileNasaLon) ')'], ...
                'FontWeight','bold', 'FontSize', FontSize, 'HorizontalAlignment', 'center', ...
                'Color', [1 1 1]);
            set(hdlFigure, 'Name', nameFig);
            
        end % traceTile
        
        
        function pasPixDegTile = computePasPixDeg(pasPixelLat, pasPixelLon, sizeTile)
            % Function computePasPixDeg
            % Calcul de la r�solution en deg des pixels.
            % Approximation pour le respect de la r�gle de Nasa World Wind
            % concernant le niveau 0.
            % cf : http://www.worldwindcentral.com/wiki/Dstile_howto
            % ------------------------------------------------------------
            
            % On suppose 20 niveaux Max de r�solution.
            pppp = double((20 ./ 2.^(1:30)) / (sizeTile)); % modifier le 29/04/2009 (dominique)
            ILat = find((pppp < pasPixelLat), 1, 'first');
            ILon = find((pppp < pasPixelLon), 1, 'first');
            i = max(ILat, ILon);
            pasPixDegTile = pppp(i);
        end % computePasPixDeg
        
        
        function nbLevels = computeNbLayers(y, x, sizeTile)
            % Function computeNbLayers
            % Determination du nombre de niveaux de r�solutions diff�rentes.
            % Les tuiles dans le tuilage de Nasa WW sont de dimensions 512X512
            % ou 150X150.
            % -------------------------------------------------------------
            
            k = 1;
            [X{k}, xPasImage(k), xMin(k), xMax(k)] = centrage_magnetique(x);
            [Y{k}, yPasImage(k), yMin(k), yMax(k)] = centrage_magnetique(y);
            nbCol = length(X{k});
            nbLig = length(Y{k});
            % D�termination du nombre de niveaux des tuiles.
            while (nbCol> sizeTile) || (nbLig > sizeTile)
                x = X{k};
                y = Y{k};
                % Diminution par 2 des coordonn�es de l'image (moyennage).
                x = (x(1:end-1) + x(2:end)) / 2;
                y = (y(1:end-1) + y(2:end)) / 2;
                x = x(1:2:end);
                y = y(1:2:end);
                
                k = k+1;
                [X{k}, xPasImage(k), xMin(k), xMax(k)] = centrage_magnetique(x);
                [Y{k}, yPasImage(k), yMin(k), yMax(k)] = centrage_magnetique(y);
                nbCol = length(X{k});
                nbLig = length(Y{k});
            end
            nbLevels = k;
            % On force la cr�ation d'un niveau ou l'image (au moins une des dimensions)
            % sera comprise dans une largeur de tuile.
            nbLevels = nbLevels+1;
            
        end % computeNbLayers
        
        function [data, Cmap]= getImageData(Value)
            % Function getImageData
            % Fonction de r�cup�ration de la donn�e � traiter (matrice ou
            % donn�e g�ographique)
            % ------------------------------------------------------------
            
            if isa(Value, 'cl_image')
                valX = get(Value, 'x');
                valY = get(Value, 'y');
                xDir = ((valX(end) - valX(1)) > 0);
                yDir = ((valY(end) - valY(1)) > 0);
                data = get_Image_inXLimYLim(Value);
                Cmap = get(Value, 'Colormap');
                
                if ~xDir
                    data = fliplr(data);
                end
                
                valYDir = get(Value, 'YDir');
                if ~xor(yDir , (valYDir == 1))
                    data = my_flipud(data);
                end
            else
                data = Value;
                Cmap = jet(256)*255;
            end
            
        end % Function getImageData
        
        function Tile = computeTiles(LatMin, LatMax, LonMin, LonMax, sizeTile, pasPixDegTile)
            % Function computeTiles
            % D�termination des tuiles concern�es par la zone.
            % ------------------------------------------------------------
            largTile = pasPixDegTile * sizeTile; % PasPixDegTileDeg d�signe la r�solution de niveau 0.
            Tile.xIndiceTuile = (floor((180+LonMin) / largTile):floor((180+LonMax) / largTile));
            
            % Au cas o� les latitudes seraient entr�es dans le sens Sud-Nord.
            if LatMin > LatMax
                Tile.yIndiceTuile = (floor((90+LatMax) / largTile):floor(( 90+LatMin) / largTile));
            else
                Tile.yIndiceTuile = (floor((90+LatMin) / largTile):floor(( 90+LatMax) / largTile));
            end
            
            % Tile.xIndiceTuile = Tile.xIndiceTuile - 1;
            % Tile.yIndiceTuile = Tile.yIndiceTuile - 1;
            
            Tile.YTileDeb = (Tile.yIndiceTuile * largTile) - 90;
            Tile.XTileDeb = (Tile.xIndiceTuile * largTile) - 180;
            Tile.YTileFin = Tile.YTileDeb + largTile - pasPixDegTile;
            Tile.XTileFin = Tile.XTileDeb + largTile - pasPixDegTile;
            
        end % Function computeTiles
        
        function flag = copyReadMe(LayerSetName)
            % Function copyReadMe
            % Copie du fichier ReadMe dans l'arborescence du layer.
            % ------------------------------------------------------------
            % Fonction de recopie du fichier ReadMe dans le r�pertoire des layers.
            % Chargement des variables d'environnement.
            global IfrTbxResources
            if isempty(IfrTbxResources)
                loadVariablesEnv
            end
            
            % Fichier readMe copi� dans le r�pertoire global des Layers
            fileOrig = fullfile(IfrTbxResources, 'NasaWorldWind_readme.txt');
            fileDest = fullfile(LayerSetName, 'NasaWorldWind_readme.txt');
            status = 1;
            flag = 1;
            if ~exist(fileDest,'file')
                [status,message,messageid] = copyfile(fileOrig,fileDest,'f'); %#ok<ASGLU>
            end
            if status ~= 1
                my_warndlg([messageid ' ' 'Fichier ReadMe :' fileOrig], 1);
                flag = -1;
            end
        end % copyReadMe
        
        function writeNWWXML(obj)
            % Function writeNWWXML
            % Ecriture d'un fichier XML format Nasa World Wind.
            % ------------------------------------------------------------
             West = obj.bornesData.lonW;
            East = obj.bornesData.lonE;
            South = obj.bornesData.latS;
            North = obj.bornesData.latN;
            
            % pathNasaWW = cl_tile_ElevationAndTexture.findPathNWW;
            DirLayer = fullfile(obj.layerSetName, obj.layerName);
            nomFicXml = fullfile(DirLayer, [obj.layerName '.xml']);%
            [nomDirXml, nomFicSimple] = fileparts(nomFicXml); %#ok<ASGLU>
            fid = fopen(nomFicXml, 'w+');
            if fid == -1
                str = sprintf(Lang('clc_image:cl_ETOPO1 : Impossible to cr�er Fichier XML %s', ...
                    'clc_image:cl_ETOPO1 : Impossible to create XML config file %s'), nomFicXml);
                my_warndlg(str, 1)
                return
            end
            
            obj.nomFicXml = nomFicXml;
            
            % Ecriture du fichier XML
            fprintf(fid, '<?xml version="1.0" encoding="UTF-8"?>\n');
            fprintf(fid, '<LayerSet Name="%s" ShowOnlyOneLayer="false" ShowAtStartup="false" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="LayerSet.xsd">\n', nomFicSimple);
            fprintf(fid, '\t<QuadTileSet ShowAtStartup="false">\n');
            fprintf(fid, '\t\t<Name>%s</Name>\n', nomFicSimple);
            fprintf(fid, '\t\t<DistanceAboveSurface>0</DistanceAboveSurface>\n');
            fprintf(fid, '\t\t<BoundingBox>\n');
            fprintf(fid, '\t\t\t<North><Value>%19.14f</Value></North>\n', North);
            fprintf(fid, '\t\t\t<South><Value>%19.14f</Value></South>\n', South);
            fprintf(fid, '\t\t\t<West><Value>%19.14f</Value></West>\n',   West);
            fprintf(fid, '\t\t\t<East><Value>%19.14f</Value></East>\n',   East);
            fprintf(fid, '\t\t</BoundingBox>\n');
            fprintf(fid, '\t\t<RenderStruts>false</RenderStruts>\n');
            fprintf(fid, '\t\t<TerrainMapped>true</TerrainMapped>\n');
            fprintf(fid, '\t\t<Opacity>%d</Opacity>\n', 100);
            fprintf(fid, '\t\t<ImageAccessor>\n');
            fprintf(fid, '\t\t\t<LevelZeroTileSizeDegrees>%19.16f</LevelZeroTileSizeDegrees>\n', obj.lztsd);
            fprintf(fid, '\t\t\t<NumberLevels>%d</NumberLevels>\n', obj.nbLevels);
            fprintf(fid, '\t\t\t<TextureSizePixels>%d</TextureSizePixels>\n', obj.sizeTile);
            fprintf(fid, '\t\t\t<ImageFileExtension>%s</ImageFileExtension>\n', obj.formatTile);
            fprintf(fid, '\t\t\t<PermanentDirectory>%s</PermanentDirectory>\n', DirLayer);
            fprintf(fid, '\t\t</ImageAccessor>\n');
            fprintf(fid, '\t\t<TransparentMinValue>0</TransparentMinValue>\n');
            % Attention : la valeur de transparence Max doit etre diff�rentes
            % de 0 pour �tre efficace.
            fprintf(fid, '\t\t<TransparentMaxValue>1</TransparentMaxValue>\n');
            fprintf(fid, '\t\t<TransparentColor>\n');
            fprintf(fid, '\t\t\t<Red>%3d</Red>\n',      0);
            fprintf(fid, '\t\t\t<Green>%3d</Green>\n',  0);
            fprintf(fid, '\t\t\t<Blue>%3d</Blue>\n',    0);
            fprintf(fid, '\t\t</TransparentColor>\n');
            fprintf(fid, '\t</QuadTileSet>\n');
            fprintf(fid, '</LayerSet>\n');
            
            fclose(fid);
            
            % Fichier XML dupliqu� pour faciliter le d�placement futur de l'ensemble.
            fileOrig = nomFicXml;
            fileDest = fullfile(obj.layerSetName, [obj.layerName '.xml']);
            [status,message,messageid] = copyfile(fileOrig,fileDest,'f'); %#ok<ASGLU>
            if status ~= 1
                my_warndlg([messageid ' ' 'XML file :' fileOrig], 1);
            end
            
        end % writeNWWXML
        
        function writeLayerXML(obj)
            % Function writeLayerXML
            % Ouverture du d�but du fichier xml. Ce fichier reprend qques
            % caract�ristiques du format NWW.
            % ------------------------------------------------------------
            West = obj.bornesData.lonW;
            East = obj.bornesData.lonE;
            South = obj.bornesData.latS;
            North = obj.bornesData.latN;
            
            % pathNasaWW = cl_tile_ElevationAndTexture.findPathNWW;
            DirLayer = fullfile(obj.layerSetName, obj.layerName);
            nomFicXml = fullfile(DirLayer, [obj.layerName '.xml']);
            [nomDirXml, nomFicSimple] = fileparts(nomFicXml); %#ok<ASGLU>
            fid = fopen(nomFicXml, 'w+');
            if fid == -1
                str = sprintf(Lang('clc_image:cl_ETOPO1 : Impossible to cr�er Fichier XML %s', ...
                    'clc_image:cl_ETOPO1 : Impossible to create XML config file %s'), nomFicXml);
                my_warndlg(str, 1)
                return
            end
            
            obj.nomFicXml = nomFicXml;
            
            % ----------------------
            % Ecriture du fichier XML
            
            fprintf(fid, '<?xml version="1.0" encoding="UTF-8"?>\n');
            fprintf(fid, '<LayerSet Name="%s" >\n', nomFicSimple);
            fprintf(fid, '\t<Name>%s</Name>\n', nomFicSimple);
            fprintf(fid, '\t<DataOrigine>\n');
            fprintf(fid, '\t\t<Resolution>%19.16f</Resolution>\n', obj.pasPixel);
            fprintf(fid, '\t\t<SizeY>%19.16f</SizeY>\n', obj.bornesData.sizePixels(1));
            fprintf(fid, '\t\t<SizeX>%19.16f</SizeX>\n', obj.bornesData.sizePixels(2));
            fprintf(fid, '\t\t<ExportElevation>%1d</ExportElevation>\n', obj.flagExportElevation);
            fprintf(fid, '\t\t<ExportTexture>%1d</ExportTexture>\n', obj.flagExportTexture);            
            fprintf(fid, '\t</DataOrigine>\n');
            fprintf(fid, '\t<BoundingBox>\n');
            fprintf(fid, '\t\t<North><Value>%19.14f</Value></North>\n', North);
            fprintf(fid, '\t\t<South><Value>%19.14f</Value></South>\n', South);
            fprintf(fid, '\t\t<West><Value>%19.14f</Value></West>\n',   West);
            fprintf(fid, '\t\t<East><Value>%19.14f</Value></East>\n',   East);
            fprintf(fid, '\t</BoundingBox>\n');
            fprintf(fid, '\t<ImageAccessor>\n');
            fprintf(fid, '\t\t<LevelZeroTileSizeDegrees>%19.16f</LevelZeroTileSizeDegrees>\n', obj.lztsd);
            fprintf(fid, '\t\t<NumberLevels>%d</NumberLevels>\n', obj.nbLevels);
            fprintf(fid, '\t\t<TextureSizePixels>%d</TextureSizePixels>\n', obj.sizeTile);
            fprintf(fid, '\t\t<ImageFileExtension>%s</ImageFileExtension>\n', obj.formatTile);
            fprintf(fid, '\t\t<PermanentDirectory>%s</PermanentDirectory>\n', DirLayer);
            fprintf(fid, '\t</ImageAccessor>\n');
            fprintf(fid, '</LayerSet>\n');
            
            fclose(fid);
            
            % Fichier XML dupliqu� pour faciliter le d�placement futur de l'ensemble.
            fileOrig = nomFicXml;
            fileDest = fullfile(obj.layerSetName, [obj.layerName '.xml']);
            [status,message,messageid] = copyfile(fileOrig,fileDest,'f'); %#ok<ASGLU>
            if status ~= 1
                my_warndlg([messageid ' ' 'XML file :' fileOrig], 1);
            end
            
        end % writeLayerXML
        
        function [iLevel, sizeImage] = computeLevel(subx, suby, sizeTile, nbLevels)
            % Function computeLevel
            % Calcul du level appropri� � la taille de l'�cran et du nveau de
            % zoom.
            % ------------------------------------------------------------
            
            % Calcul du level appropri� � la taille de l'�cran et du
            % zoom.
            sizeScreen = get(0,'Monitor');
            if size(sizeScreen,2) > 1
                % On retient l'�cran principal
                sizeScreen = sizeScreen(:, 3:4);
            end
            sizePixelScreen = prod(sizeScreen);
            
            % On introduit un facteur d'�chelle (arbitraire).
            % sizePixelScreen = sizePixelScreen/1.5;
            
            % Calcul de la meilleure r�solution.
            paxPixScreenLon = (subx(2) - subx(1))/sizeScreen(2);
            pasPixScreenLat = (suby(2) - suby(1))/sizeScreen(1);
            pasPixel = cl_tile_ETOPO1.computePasPixDeg(pasPixScreenLat, paxPixScreenLon, ...
                sizeTile);
            
            % Calcul des tuiles concern�es.
            Tiles = cl_tile_ETOPO1.computeTiles(suby(2), suby(1), subx(1), subx(2), ...
                sizeTile, pasPixel); %#ok<PROP>
            
            % Calcul de la taille de l'image.
            sizeImage= size(Tiles.xIndiceTuile,2)*size(Tiles.yIndiceTuile,2)*sizeTile^2;
            iLevel = nbLevels-1;  % LevelMax � priori
            if sizeImage > sizePixelScreen
                while sizeImage > sizePixelScreen
                    iLevel = iLevel-1;
                    sizeImage = sizeImage/2;
                end
            end
        end %computeLevel
        

    end % methods(Access = 'Static')
end
