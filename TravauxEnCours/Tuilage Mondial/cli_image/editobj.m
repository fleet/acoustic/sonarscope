% Edition d'une instance de cli_image.
%
% SYNTAX :
%   editobj(a)
%   a = editobj(a)
%
% INPUT PARAMETERS :
%   a : Un objet de type cli_image
%
% OUTPUT PARAMETERS :
%   [] : L'interface rend la main
%   a  : Instance de cli_image eventuellement modifiee
%
% REMARKS : L'IHM rend la main si il n'y a pas d'argument de sortie
%
% EXAMPLES :
%   a = cl_image('Image', Lena)
%   editobj(a)
%
% SEE ALSO : cli_image Authors
% AUTHORS  : JMA
% VERSION  : $Id: editobj.m,v 1.1 2003/09/08 15:35:02 augustin Exp augustin $
% ----------------------------------------------------------------------------

function varargout = editobj(this, varargin)



global SonarScopeDataLevel2

ButtonSave = getPropertyValue(varargin, 'ButtonSave', 0);

if isempty(this.Images)
%     a = cl_image('Image', Lena, 'Titre', 'Lena', 'ColormapIndex', 2, 'GeometryType', Unknown(cl_image_I0));
%     a = set(a, 'Comments', 'Whos is Lena ? The answer is "http://www-2.cs.cmu.edu/~chuck/lennapg/"');
%     this = cli_image('Images', a);

    %% Composition du tuilage ETOPO1 depuis l'arborescence existante 
    % pour l'image de base de SonarScope.
       % Gestion statique de cl_ETOPO1 : on recharge l'image � chaque 
       % extraction Etopo1.    
   try 
        % Chargement du layer Etopo1.
        nomFic = fullfile(SonarScopeDataLevel2, 'Etopo1.xml');
        [dirLayer, layerName, ext, versn] = fileparts(nomFic); %#ok<NASGU>
        obj = cl_tile_ETOPO1.linkLayerEtopo(dirLayer,layerName);
        B = get(obj,'bornesData');
        [Z, obj, Tiles] = obj('XLim', [B.lonW B.lonE], 'YLim', [B.latS B.latN]); %#ok<NASGU>
        Carto = cl_carto('Ellipsoide.Type', 11, 'Projection.Type', 1);
        Info.x = linspace(B.lonW, B.lonE, size(Z, 2));
        Info.y = linspace(B.latN, B.latS, size(Z, 1));
        a = cl_image('Image', Z, 'ImageName', 'Earth', ...
            'ColormapIndex', 3, ...
            'GeometryType', LatLong(cl_image_I0), ...
            'X', Info.x, 'Y', Info.y, ...
            'DataType', identDataType(cl_image_I0, 'Bathymetry'), ...
            'XUnit', 'deg', 'YUnit', 'deg', 'Unit', 'm', ...
            'Comments', 'Keep it blue', 'Carto', Carto);
        
        this = cli_image('Images', a);
    catch
        return
    end
        
       % DEB Modif GLU : 21/01/2010, Int�gration de la classe Tuilage Etopo.
%        % Gestion dynamique de cl_ETOPO1 : attachement de cl_ETOPO1 � la
%        % mani�re de cl_memmapfile dans une instance de cl_image    
%      try
%         dirName = 'C:\Temp\SonarScope-Tiles\';
%         layerName = 'Etopo1';
%         obj = cl_tile_ETOPO1.linkLayerEtopo(dirName, layerName);
%         Carto = cl_carto('Ellipsoide.Type', 11, 'Projection.Type', 1);
%         B = get(obj,'bornesData');
%         Info.x = linspace(B.lonW, B.lonE, B.sizePixels(2));
%         Info.y = linspace(B.latN, B.latS, B.sizePixels(1));
%         I = cl_image('Image', obj, ...
%             'ImageName', 'Etopo1', ...
%             'ColormapIndex', 3, ...
%             'X', Info.x, 'Y', Info.y, ...
%             'GeometryType', LatLong(cl_image_I0), ...
%             'DataType', identDataType(cl_image_I0, 'Bathymetry'), ...
%             'XUnit', 'deg', 'YUnit', 'deg', 'Unit', 'm', ...
%             'Comments', 'Keep it blue', 'Carto', Carto);
%         
%         this = cli_image('Images', I);
%         
%     catch
%         return
%     end
    % FIN Modif GLU : 21/01/2010, Int�gration de la classe Tuilage Etopo.
    
% % % % % %     nomFic = getNomFicDatabase('TerreEtopo.mat');
% % % % % %     Z = loadmat(nomFic, 'nomVar', 'Image');
% % % % % %     Info = loadmat(nomFic, 'nomVar', 'Info');
% % % % % %     Carto = cl_carto('Ellipsoide.Type', 11, 'Projection.Type', 1);
% % % % % %     a = cl_image('Image', Z, 'X', Info.x, 'Y', Info.y, 'ImageName', 'Earth', ...
% % % % % %         'ColormapIndex', 3, ...
% % % % % %         'GeometryType', LatLong(cl_image_I0), ...
% % % % % %         'DataType', identDataType(cl_image_I0, 'Bathymetry'), ...
% % % % % %         'XUnit', 'deg', 'YUnit', 'deg', 'Unit', 'm', ...
% % % % % %         'Comments', 'Keep it blue', 'Carto', Carto);
% % % % % %     
% % % % % %     this = cli_image('Images', a);
end

% -------------------------------------
% Creation et remplissage de la fenetre

this = construire_ihm(this);
GCF = this.ihm.figure;

if nargout == 0

    % --------------------------------------------------------------
    % Cas ou l'IHM ne rend pas la main. On ne peut pas recuperer les
    % modifications. On rend le bouton save invisible

    h_Save = findobj(this.ihm.figure, 'Tag', 'ancre cli_image Save');
    set(h_Save, 'Visible', 'off');
    
    h_Quit = findobj(this.ihm.figure, 'Tag', 'ancre cli_image Quit');
    W_Quit = get(h_Quit, 'Position');
    W_Quit(3) = W_Quit(3) * 2;
    set(h_Quit, 'Position', W_Quit);
    
    if isdeployed
        set(findobj(this.ihm.figure, 'Tag', 'ancre cli_image Quit'), 'Visible', 'off');
    end

    disp('Ce mode d''edition ''editobj(a)'' ne permet pas de sauver les modifications.')
    disp('Preferez : ''a = editobj(a);'' si cela est votre intention')
else

    if my_isdeployed && ~ButtonSave
        h_Save = findobj(this.ihm.figure, 'Tag', 'ancre cli_image Save');
        set(h_Save, 'Visible', 'off');

        h_Quit = findobj(this.ihm.figure, 'Tag', 'ancre cli_image Quit');
        W_Quit = get(h_Quit, 'Position');
        W_Quit(3) = W_Quit(3) * 2;
        set(h_Quit, 'Position', W_Quit);
    end


    % ----------------
    % Boucle d'attente

    h0 = GCF;
    UserData = get(h0, 'UserData');
    while ishandle(h0)
        uiwait(h0);
        if ishandle(h0)
            UserData = get(h0, 'UserData');
        end
    end

    % ---------------------------------
    % Recuperation de l'instance sauvee

    InstanceSauvee = getFromUserData('InstanceSauvee', UserData);
    if isempty(InstanceSauvee)
        varargout{1} = this;
    else
        varargout{1} = InstanceSauvee;
    end
end

deleteFileOnListe(cl_memmapfile([]), GCF)
