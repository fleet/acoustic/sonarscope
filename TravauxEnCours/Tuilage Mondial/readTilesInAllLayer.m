% EXAMPLES:
% pppp = readTilesInAllLayer(obj, 'XLim', [-7 +3.75], 'YLim', [45.5 52.74]);
%
% SEE ALSO : cl_tile_ElevationandTexture.m Authors
% AUTHORS  : GLU
% VERSION  : $Id: get.m,v 1.3 2010/01/06 09:59:25 rgallou Exp rgallou $
% ----------------------------------------------------------------------------

function [layer] = readTilesInAllLayer(varargin)

xLim = getPropertyValue(varargin, 'XLim', []);
yLim = getPropertyValue(varargin, 'YLim', []);
% Convention : 1ere valuer de Latitude = Sud.
if yLim(1) > yLim(2)
    yLim = circshift(yLim,[1 -1])';
end
% Recensement des fichiers XML de description des layers cr�es par
% l'utilisateur.
dirName = 'C:\Temp\SonarScope-Tiles\';
listFicXML = dir (fullfile(dirName, '*.xml'));
switch length(listFicXML)
    case 0
        chaineFR = 'Vous ne disposez pas de la structure ETOPO1. SVP Contacter Jean-Marie AUGUSTIN, jean-marie.augustin@ifremer.fr, IFREMER, NSE/AS, 02 98 22 47 03';
        my_warndlg(Lang(chaineFR, 'TODO'),1);
        return 
    case 1
        % Lecture uniquement dans ETOPO1
        % TODO
        return
end

% Lecture successive des XML de description des Layers cr�es par
% l'utilisateur.
layer = [];
for i=1:length(listFicXML)
    filename = fullfile(dirName, listFicXML(i).name);
    xDoc = xmlread(filename);

    dummy = xDoc.getElementsByTagName('Name');
    pppp = dummy.item(0);
    layer(i).name = char(pppp.getTextContent);
    
    dummy = xDoc.getElementsByTagName('North');
    pppp = dummy.item(0);
    layer(i).latN = str2double(pppp.getTextContent);
    
    dummy = xDoc.getElementsByTagName('South');
    pppp = dummy.item(0);
    layer(i).latS = str2double(pppp.getTextContent);
    
    dummy = xDoc.getElementsByTagName('West');
    pppp = dummy.item(0);
    layer(i).lonW = str2double(pppp.getTextContent);
    
    dummy = xDoc.getElementsByTagName('East');
    pppp = dummy.item(0);
    layer(i).lonE = str2double(pppp.getTextContent);
    
    dummy = xDoc.getElementsByTagName('TextureSizePixels');
    pppp = dummy.item(0);
    layer(i).samplesPerTile = str2double(pppp.getTextContent);
    
    dummy = xDoc.getElementsByTagName('ImageFileExtension');
    pppp = dummy.item(0);
    layer(i).imageFileExtension = char(pppp.getTextContent);
    
    dummy = xDoc.getElementsByTagName('LevelZeroTileSizeDegrees');
    pppp = dummy.item(0);
    layer(i).lztsd = str2double(pppp.getTextContent);
    
    dummy = xDoc.getElementsByTagName('NumberLevels');
    pppp = dummy.item(0);
    layer(i).nbLevels = str2double(pppp.getTextContent);
       
    dummy = xDoc.getElementsByTagName('Resolution');
    pppp = dummy.item(0);
    layer(i).pasPixel = str2double(pppp.getTextContent);

    dummy = xDoc.getElementsByTagName('SizeY');
    pppp = dummy.item(0);
    layer(i).sizeY = str2double(pppp.getTextContent);

    dummy = xDoc.getElementsByTagName('SizeX');
    pppp = dummy.item(0);
    layer(i).sizeX = str2double(pppp.getTextContent);
    
    % allLayer = [allLayer layer]; %#ok<AGROW>
end  % End FOR

%% Calcul de l'intersection des limites avec les layers
for i=1:length(layer)
    % A = linspace(allLayer(i).latN, allLayer(i).latS, allLa[layer.flagRetenu]yer(i).samplesPerTile);
    % B = [yLim(1) yLim(2)];
    % Si Latitude Sud demand�e > Latitude Nord du Layer
    % ou Latitude Nord demand�e < Latitude Sud du Layer :
    % le layer n'est pas concern� par la recherche.
    if (yLim(1) > layer(i).latN || yLim(2) < layer(i).latS)
        layer(i).flagRetenu = 0;
    else
        % L'extraction est incluse au moins partiellement dans le layer.
        layer(i).flagRetenu = i;
    end    
end

%%
clear pppp;
disp('Calcul de l''image globale');
disp('==========================');
dummy = [layer(:).flagRetenu];
selectLayer = (dummy ~= 0);
pasPixel = [layer(selectLayer).pasPixel]; 
pasPixel = sort(pasPixel, 'ascend');
for i=1:length(pasPixel)
    NomLayer = layer(i).name;
    B.sizePixels = [layer(i).sizeY layer(i).sizeX];
    B.lonW = layer(i).lonW; 
    B.lonE = layer(i).lonE;
    B.latS = layer(i).latS; 
    B.latN = layer(i).latN;
    % Pointage vers le layer.
    obj = cl_tile_ETOPO1('LayerName', NomLayer, 'BornesData', B, ...
        'Lztsd', layer(i).lztsd, 'PasPixel', layer(i).pasPixel, 'FormatTile', layer(i).imageFileExtension, ...
        'flagExportTexture', 1, 'flagExportElevation', 0);
    % get_Image_inXLimYLim(obj, );
    % get_pixels_xy(this, valX, valY)
    
    % Extraction de l'image utile. On �vite de rechercher des infos autour
    % de la zone utile de l'image.
    xExtract(1,i) = max(xLim(1),layer(i).lonW);
    xExtract(2,i) = min(xLim(2),layer(i).lonE);
    yExtract(1,i) = max(yLim(1),layer(i).latS);
    yExtract(2,i) = min(yLim(2),layer(i).latN);
    pppp(i) = obj('XLim', xExtract(:,i), 'YLim', yExtract(:,i));
    
    yStep(i) = get(pppp(i), 'YStep');
    xStep(i) = get(pppp(i), 'XStep');
    % Calcul des coordonn�es de l'image extraite du layer pr�c�dent plus r�solu.
    if i > 1
         % x = get_subx_suby(pppp(i));
        % Equivalent de la proc�dure get_subx_suby pour suby. 
         y    = get(pppp(i-1), 'y');
         x    = get(pppp(i-1), 'x');
%         YLim = get(pppp(i-1), 'YLim');
%         suby = ((y >= YLim(1)) & (y <= YLim(2)));
%         y = y(suby);
         [~, ix(1), iy(1)] = get_pixels_xy(pppp(i), x(1), y(1));
         [~, ix(2), iy(2)] = get_pixels_xy(pppp(i), x(end), y(end));
         subx = xLim(1):xStep(i-1):xExtract(1, i-1);
         suby = yLim(1):yStep(i-1):yExtract(1, i-1);
         I = get_val(pppp(i),ix(1):1:ix(2),iy(1):1:iy(2));
%         dummy = set_val_xy(pppp(i), this.x(subx), this.y(suby), I);
         
            [x, y] = ij2xy(pppp(i), ix, iy);
%         I = get_Image(pppp(i), layer(i).name);
%          Y    = get(pppp(i), 'y');
%          X    = get(pppp(i), 'x');
%         
%         interpY = yLim(1):yStep(i-1):yLim(2);
%         interpX = xLim(1):xStep(i-1):xLim(2);
%         [X,Y] = meshgrid(X, Y);
%         ZI = interp2(X,Y,I,interpX,interpY');
%         ZI = griddata(X,Y,double(I),interpX,interpY');
       
        x    = get(pppp(i), 'x');
        XLim = get(pppp(i), 'XLim');
%         subx = find((x >= XLim(1)) & (x <= XLim(2)));
        y    = get(pppp(i), 'y');
        y = fliplr(y);
        YLim = get(pppp(i), 'YLim');
%         suby = ((y >= YLim(1)) & (y <= YLim(2)));

        x    = get(pppp(i-1), 'x');
        XLim = get(pppp(i-1), 'XLim');
        subx = find((x >= XLim(1)) & (x <= XLim(2)));
        y    = get(pppp(i-1), 'y');
        y = fliplr(y);
        YLim = get(pppp(i-1), 'YLim');
        suby = ((y >= YLim(1)) & (y <= YLim(2)));
        [XLim, YLim, subx, suby, toto] = intersectionImages(pppp(i-1), subx, suby, 'SameSize', pppp(i-1), pppp(i));
    end
    
end


