% nomFic = 'F:\GLOBE\g10g';
function [flag, obj] = test_ETOPO1(nomFic, varargin)
%
nomFic = 'C:\IfremerToolbox_R2009b\TravauxEnCours\Tuilage Mondial\ETOPO1_Ice_g_gmt4.ers';
%% Pointage vers le fichier Test (Extraction du Etopo Mondial)
nomFic = getNomFicDatabase('z_LatLong_Bathymetry.ers');
SonarIdent = [];
[flag, typeFichier, SonarIdent] = identifyFileFormat(nomFic, SonarIdent);
[flag, a] = import_ermapper(cl_image_I0, nomFic);
% SonarScope(a);
NomLayer = 'Etopo1_extract';
%% Pointage vers le fichier Test (Etopo Mondial)
nomFic = getNomFicDatabase('Earth_ers_LatLong_Bathymetry.ers');
SonarIdent = [];
[flag, typeFichier, SonarIdent] = identifyFileFormat(nomFic, SonarIdent);
[flag, a] = import_ermapper(cl_image_I0, nomFic);
% SonarScope(a);
NomLayer = 'Etopo1';

%% Cr�ation des tuiles
obj = cl_tile_ETOPO1('Value', a, 'LayerName', NomLayer, ...
    'Process', 'Creation', 'FormatTile','png','WriteJpg', 1, ...
    'flagExportTexture', 0, 'flagExportElevation', 0);
obj.bornesData
%% Pointage vers le fichier Test
nomFic = getNomFicDatabase('BRETAGNE.ers');
SonarIdent = [];
[flag, typeFichier, SonarIdent] = identifyFileFormat(nomFic, SonarIdent);
[flag, a] = import_ermapper(cl_image_I0, nomFic);
NomLayer = 'Bretagne';

%% Cr�ation de Tuiles
obj = cl_tile_ETOPO1('Value', a, 'LayerName', NomLayer, ...
    'Process', 'Creation', 'FormatTile','png','WriteJpg', 1, ...
    'flagExportTexture', 1);
%% Pointage vers le fichier Test
nomFic = getNomFicDatabase('EM12D_PRISMED_125m_LatLong_Bathymetry.ers');
SonarIdent = [];
[flag, typeFichier, SonarIdent] = identifyFileFormat(nomFic, SonarIdent);
[flag, a] = import_ermapper(cl_image_I0, nomFic);
NomLayer = 'PRISMED_125m';
%% Cr�ation de Tuiles
obj = cl_tile_ETOPO1('Value', a, 'LayerName', NomLayer, ...
    'Process', 'Creation', 'FormatTile','png','WriteJpg', 1, ...
    'flagExportTexture', 1);
%% Ecriture du fichier XML
cl_tile_ETOPO1.writeNWWXML(obj)
obj.bornesData
%% Ecriture du fichier XML
cl_tile_ETOPO1.writeLayerXML(obj)
obj.bornesData

%% Pointage sur des tuiles existantes
B.sizePixels = [get(a, 'NbRows') get(a, 'NbColumns')];
xData = get(a, 'x');
yData = get(a, 'y');
B.lonW = xData(1); 
B.lonE = xData(end);
% Permutation des Latitudes si le Sud est invers� (???)
if yData(1) > yData(end)
    pppp = circshift([yData(1) yData(end)],[1 -1])';
else
    pppp = [yData(1) yData(end)];
end
B.latS = pppp(1); 
B.latN = pppp(end);
%% Pointage vers des tuiles existantes
obj = cl_tile_ETOPO1('LayerName', NomLayer, 'BornesData', B, ...
    'FormatTile','png');    

flag = 1
%%


%%
tic
pppp = obj('XLim', [-7 +3.75], 'YLim', [45.5 52.74]);
SonarScope(pppp);
toc
%% Extraction de la tuile �largie de Brest
pppp = obj('XLim', [-5 -3.75], 'YLim', [47.5 48.74], 'LayerSetName', 'Bretagne');
imagesc(pppp);
%% Extraction de la tuile de Brest
pppp = obj('XLim', [-4.8 -3.9], 'YLim', [48 48.7]);
figure; imagesc(pppp);
%% Lecture de la totalit� d'un layer.
dummy = obj('XLim', [B.lonW B.lonE], 'YLim', [B.latS B.latN]);
%% Lecture du fichier XML
dirName = 'C:\Temp\SonarScope-Tiles';
layerName = 'Bretagne';
obj = cl_tile_ETOPO1.linkLayerEtopo(dirName, layerName)

%% Lecture du fichier XML
dirName = 'F:\SonarScopeData\Level2';
layerName = 'Etopo1';
obj = cl_tile_ETOPO1.linkLayerEtopo(dirName, layerName)
Carto = cl_carto('Ellipsoide.Type', 11, 'Projection.Type', 1);
B = get(obj, 'bornesData');
Info.x = linspace(B.lonW, B.lonE, B.sizePixels(2));
Info.y = linspace(B.latN, B.latS, B.sizePixels(1));
c = cl_image('Image', obj, ...
             'ImageName', 'Etopo1', ...
            'ColormapIndex', 3, ...
            'GeometryType', LatLong(cl_image_I0), ...
            'X', Info.x, 'Y', Info.y, ...
            'DataType', identDataType(cl_image_I0, 'Bathymetry'), ...
            'XUnit', 'deg', 'YUnit', 'deg', 'Unit', 'm', ...
            'Comments', 'Keep it blue', 'Carto', Carto);
%% Extraction � partir cl_image cabl�e sur cl_tile_ETOPO1.
pppp = c('XLim', [-4.8 -3.9], 'YLim', [48 48.7]);
%% Extraction � partir cl_image cabl�e sur cl_tile_ETOPO1.
[ext_pppp, ext_obj, ext_tiles] = obj('XLim', [-180 180], 'YLim', [-90 90]);

%% Lecture dans l'ensemble des Layers
pppp = readTilesInAllLayer('XLim', [-7 +3.75], 'YLim', [45.5 52.74]);
