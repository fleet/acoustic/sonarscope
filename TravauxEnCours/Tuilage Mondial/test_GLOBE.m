% nomFic = 'F:\GLOBE\g10g';
function flag = test_GLOBE(nomFic)

nomFic = 'F:\GLOBE\g10g';
B.sizePixels = [4800 10800];
B.latN = 50.0;
B.latS = 0.0;
B.lonE = 90.0;
B.lonW = 0.0;
nbPointsLat = 4800;
nbPointsLon = 10800;
lat = linspace(B.latS,B.latN,nbPointsLat);
lon = linspace(B.lonW,B.lonE,nbPointsLon);
%%
latlim = [41 42.5]; lonlim = [-73 -69.9];
globedem(latlim,lonlim)
%%
latlim = [0 50]; lonlim = [0 90];
[Z,refvec] = globedem(nomFic,1,latlim,lonlim);
%%

a = cl_image('Image', nomFic, 'x', lon, 'y', lat, 'GeometryType', 3, 'DataType', 2);
g10g = cl_tile_GLOBE('LayerSetName', 'C:\Temp\GLOBE', 'Value', a, 'BornesData', B, ...
    'LayerName', 'g10g', 'Process', 'Creation', 'FormatTile','png','WriteJpg', 1, ...
    'flagExportTexture', 1, 'flagExportElevation', 0);

flag = 1;
