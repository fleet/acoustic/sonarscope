function [flag, I] = createImagefromODV(Data)
flag = 1;
% Tri des profils.
[b, m, n]   = unique(Data(:,3)); %#ok<NASGU>
pppp        = diff(m);
maxPasZ     = max(pppp);
I = nan(length(b), maxPasZ);
offset = 0;
% Cas du 1er groupe
nbVal = m(1);
sub = 1:nbVal;
I(1,sub) = Data(sub + offset, 6);
offset = offset + nbVal;
% Cas des autres groupes
for i=2:length(b)
    nbVal = m(i) - m(i-1);
    
    sub = 1:nbVal;
    I(i,sub) = Data(sub + offset, 6);
    offset = offset + nbVal;
end

I = I';
% figure; imagesc(I)