% Exportation de l'image dans NasaWorldWind
%
% SYNTAX :
%   value = exportTilingNasaWorldWind(a, ident, ...)
%
% INPUT PARAMETERS :
%   a     : Une instance de la classe cl_image
%
% PROPERTY NAMES / PROPERTY VALUES:
%   subx : Echantillonnage de la premiere dimension
%   suby : Echantillonnage de la deuxieme dimension
%   ...
%
% OUTPUT PARAMETERS :
%   value : Valeur du parametre ramene a l'unite
%
% EXAMPLES :
%   [I, label] = ImageSonar(1);
%   a = cl_image('Image', I, 'Titre', label, 'XLabel', 'Largeur', 'YLabel', 'Hauteur')
%   nomFic = tempname
%   exportTilingNasaWorldWind(a, 'nomFic', nomFic)
%
% SEE ALSO : clc_image clc_image Authors
% AUTHORS  : JMA,GLU
% VERSION  : $Id: get.m,v 1.3 2007/10/19 09:59:25 rgallou Exp rgallou $
% ----------------------------------------------------------------------------

function [flag, repExport] = exportTilingNasaWorldWindBil(this, varargin) % subx et suby sont � inhiber

global IfrTbx
% global IfrTbxResources

subx        = getPropertyValue(varargin, 'subx', []);
suby        = getPropertyValue(varargin, 'suby', []);
sizeTileNWW = getPropertyValue(varargin, 'SizeTileNWW', 150);

repExport   = getPropertyValue(varargin, 'repExport', fullfile(my_tempdir,'SSC-Tiles'));

flag = 0;

bathy = get(this, 'DataType') == 2;

flagRunSSC3DViewer = 0;
if bathy
    % Affichage de l'IHM de dialogue d'export.
    a = cla_dialog_NWW;
    a = set(a, 'state1', 1);
    a = set(a, 'state2', 0);
    strUS = '<html><FONT SIZE=5 FACE="Geneva"><b>Elevation</b> layer export.</FONT><br><i><font color="red">Only bathymetry is exported, they are visible only if you add a texture.</i></html>';
    strFR = '<html><FONT SIZE=5 FACE="Geneva">Export des donn�es d''<b>Elevation</b>.</FONT></b><br><i><font color="red">Seules les donn�es de bathym�trie sont export�es; elles ne sont visibles que si on ajoute une texture.</i></html>';
    strState1 = Lang(strFR, strUS);
    a = set(a, 'labelState1', strState1);
    strUS = '<html><FONT SIZE=5 FACE="Geneva"><b>Texture</b> layer export.</FONT><br><i><font color="red">You can export the texture but it would be better to export it after a shading.</i></html>';
    strFR = '<html><FONT SIZE=5 FACE="Geneva">Export des donn�es de <b>texture</b>.</FONT></b><br><i><font color="red">Vous pouvez exporter la texture mais il serait pr�f�rable de l''exporter apr�s une op�ration d''ombrage.</i></html>';
    strState2 = Lang(strFR, strUS);
    a = set(a, 'labelState2', strState2);
    strUS = '<html><FONT SIZE=5 FACE="Geneva"><b>Run SonarScope3D-Viewer.</b></FONT><br><i><font color="red">You can execute directly SonarScope3D-Viewer but you''ll have to add your new layer.</i></html>';
    strFR = '<html><FONT SIZE=5 FACE="Geneva"><b>Lancement de SonarScope3D-Viewer.</b>.</FONT></b><br><i><font color="red">Vous pouvez lancer directement SonarScope3D-Viewer mais vous aurez � charger votre nouvelle couche de donn�e.</i></html>';
    strState3 = Lang(strFR, strUS);
    a = set(a, 'labelState3', strState3);
    a = editobj(a);     

    
    if (~get(a, 'state1') && ~get(a, 'state2')) || get(a, 'flagOk') == 0
        return
    end

    if get(a, 'state1') == 1
        flagExportElevation = true;
    else
        flagExportElevation = false;
    end
    if get(a, 'state2') == 1
        flagExportTexture = true;
    else
        flagExportTexture = false;
    end
    if get(a, 'state3') == 1
        flagRunSSC3DViewer = true;
    else
        flagRunSSC3DViewer = false;
    end

%     button = questdlg(Lang('Voulez-vous exporter la texture coloris�e et l''�l�vation?','Export colored texture and elevation data ?'), ...
%         Lang('Sortie Elevation', 'Elevation Output'),Lang('Les deux','Both'),Lang('Uniquement l''�levation','Elevation only'), Lang('Annuler', 'Cancel'), Lang('Les deux','Both'));
%     switch button
%         case Lang('Les deux','Both')
%             flagExportTexture = true;
%             flagExportElevation = true;
%         case Lang('Uniquement l''�levation','Elevation only')
%             flagExportTexture = false;
%             flagExportElevation = true;
%         case Lang('Annuler', 'Cancel')
%             return;
%     end
else
    flagExportTexture = true;
    flagExportElevation = false;
end

%% V�rification de la pr�sence de NASA world wind

pathNasaWW = cl_tile_ElevationAndTexture.findPathNWW;

if isempty(pathNasaWW)
    str = sprintf(Lang('NASA World Wind n''est pas pr�sent sur le PC. Veuillez l''installer','NASA World Wind doesn''t exist on the Computer. Please, install it.'));
    my_warndlg(str, 1);
    return
end

if get(this, 'GeometryType') ~= 3
    my_warndlg(Lang('L''image n''est pas en coordonn�es Lat-Long','This image is not in Lat-Lon coordinates '),1);
    return
end

%% Configuration des param�tres de Nasa via l'interface de saisie

y =  get(this, 'Y');
North= max(y);
South= min(y);
x =  get(this, 'X');
West= min(x);
East= max(x);

% Selon le type de donn�es, on ajoute une extension propre au type de la
% donn�e.
if flagExportElevation
    filtre = '*_elevation.xml';
    nameFiltre = '_elevation.xml';
else
    filtre = '*_texture.xml';
    nameFiltre = '_texture.xml';
end

paramNASAWW = cla_repertoire('RepDefaut', repExport, 'FileNameVisible', 1, ...
    'ExtensionFiles', {nameFiltre, filtre}, 'EnableSelection', 0);

[paramNASAWW, flag] = editobj(paramNASAWW);
if ~flag
    return
end
listeFic = get(paramNASAWW, 'listeFic');
repExport = get(paramNASAWW, 'lastDir');
if isempty(listeFic{1})
    % GLU : il faudrait faire comme uiputfile si on clique sur OK alors
    % qu'on a pas donn� de nom.
    my_warndlg(Lang('Aucun nom de fichier n''a �t� donn�, recommencez svp.','No file was given, do it again.'), 1)
    return
end



% [pppp, nomLayer, ext, versn] = fileparts(listeFic{1}); %#ok<ASGLU,NASGU>
nomLayer = [listeFic{1} nameFiltre(1:end-4)];

WorkInProgress

%% Configuration des param�tres NASA

if isequal(pathNasaWW,0) || isequal(nomLayer,0)
    return
else
    if length(this) ~= 1
        my_warndlg('clc_image:exportTilingNasaWorldWind Une image a la fois svp', 0)
        return
    end
    
    % if isempty(this.Images(this.indImage))
    if isempty(this)
        my_warndlg('clc_image:exportTilingNasaWorldWind L''image est vide', 0)
        return
    end
    
    %% Cr�ation de l'arborescence de tuiles.
    % On cr�e des tuiles de 512x512 au format jpg.
    
    objTile = cl_tile_ElevationAndTexture('LayerSetName', repExport, 'LayerName', nomLayer,...
        'Value', this, 'WriteJpg', 1, 'Process', 'Creation','FormatTile',...
        'png', 'SizeTile', sizeTileNWW,'ValNaN', 0, 'subx', subx, 'suby', suby,...
        'flagExportTexture',flagExportTexture,'flagExportElevation',flagExportElevation);
    % Si pb relev� dans la cr�ation.
    if isempty(objTile.layerName)
        return
    end
    
    % Cr�ation du fichier XML du layer.
    cl_tile_ElevationAndTexture.writeNWWXML(objTile);
    
    
    %{ 
    DEB 17/02/2010, GLU, Suppression du lancement vers DotNet
    button = questdlg(lang('Voulez-vous exporter vers NWW DotNet ou Nasa Java ("BathyScope") ?', ...
        'Do you want to export towards NWW DotNet or Nasa Java ("BathyScope")?'), ...
        'Export NWW',Lang('Nasa DotNet','Nasa DotNet'),Lang('BathyScope','BathyScope'), Lang('Annuler', 'Cancel'), Lang('Nasa DotNet','Nasa DotNet'));
    switch button
        case Lang('Nasa DotNet','Nasa DotNet')
            flagExportNWWDotNet = true;
            flagExportNWWJava = false;
        case Lang('BathyScope','BathyScope')
            flagExportNWWDotNet = false;
            flagExportNWWJava = true;
        case Lang('Annuler', 'Cancel')
            return;
    end
    
    % Identification (et nettoyage) des layers d�j� pr�sents.
    [run,matParam] = cl_tile_ElevationAndTexture.isPresent(objTile);
    if flagExportElevation
        % Ecriture du fichier Earth.xml sous
        % C:\Program Files\NASA\World Wind 1.4\Config
        writeEarthXml('matParam',matParam, 'LayerSetName', repExport);
    end
    
    % Si on veut la sortie sur la version NWW DotNET
    if flagExportNWWDotNet
        % Lancement du serveur Web
        [status, result] = system('tasklist /fi "imagename eq httpd.exe"');
        sub = strfind(result, 'httpd.exe');
        if isempty(sub)
            if ispc
                if strcmp(computer, 'PCWIN')
                    dirWebServer = 'C:\Program Files\xampp-win32-1.7.1\xampp';
                else
                    if strcmp(computer, 'PCWIN64')
                        dirWebServer = 'C:\Program Files (x86)\xampp-win32-1.7.1\xampp';
                    end
                end
                if exist(dirWebServer, 'dir')
                    cd(dirWebServer);
                else
                    error_msg = Lang(['Le Serveur XAMPP n''est pas install� sous le r�pertoire ' dirWebServer 'NWW DotNet ne chargera pas les donn�es d''�l�vation.'], ...
                        ['The web Server XAMPP is not installed under ' dirWebServer 'NWW won''t load the Elevation']);
                    my_warndlg(error_msg, 1);
                end
            end
            winopen('setup_xampp.bat');
        end
        
        
        % Traitement du lancement direct de Nasa World Wind
        lat = (North+South)/2;
        lon = (West+East)/2;
        AltitudePointOfView = 180 * get(cl_tile_ElevationAndTexture, 'lztsd');
        if isdeployed
            cmd = sprintf('!winopen "%s%s" "worldwind://goto/world=Earth&lat=%9.4f&lon=%9.4f&alt=%f"\n', pathNasaWW, '\WorldWind.exe', ...
                lat, lon, AltitudePointOfView);
        else
            cmd = sprintf('!%s\\Installation\\Viewers\\winopen.exe "%s%s" "worldwind://goto/world=Earth&lat=%9.4f&lon=%9.4f&alt=%f"\n', ...
                IfrTbx, pathNasaWW, '\WorldWind.exe', lat, lon, AltitudePointOfView);
        end
        eval(cmd);
    end
    
    % Lancement exclusif de la version NWW Java.
    % if flagExportNWWJava
    logoFileName = fullfile(IfrTbxResources, 'IconesLogiciel', 'ATL_AltranOuest.jpg');
    A = imread(logoFileName);
    chaineFR = sprintf('BathyScope a �t� d�velopp� par la Soci�t� ALTRAN Ouest-ATLANTIDE. \nIl est mis � disposition du logiciel SonarScope. \n\nPour plus d''informations, contactez R.GALLOU :\n 02 98 05 43 21, roger.gallou@altran.com');
    chaineUS = sprintf('BathyScope was developped par ALTRAN Ouest-ATALNTIDE Corporated. \nIt is available for sonarScope software. \n\nFor futher informations, please contact R.GALLOU :\n 02 98 05 43 21, roger.gallou@altran.com');
    msg = Lang(chaineFR, chaineUS);
    msgbox(msg,Lang('Lancement de Bathyscope','Running BathyScope'),'custom',A,hot(size(A,2)), 'modal')

    % Lancement exclusif de BathyScope.
    % Pas fiable !!!!!!!!!!!!!!!!
    [status, result] = system('tasklist /fi "imagename eq BTSTAC~1.EXE"');
    sub = strfind(result, 'BTSTAC~1.EXE');
    if isempty(sub)
        if isdeployed
            exeNWWJava = fullfile(IfrTbx, 'ifremer', 'extern', 'java', 'lib', 'BathyScope.jar');
            cmd = sprintf('!winopen "%s" ',exeNWWJava);
        else
            exeNWWJava = fullfile(IfrTbx, 'ifremer', 'extern', 'java', 'lib', 'BathyScope.jar');
            cmd = sprintf('!%s\\Installation\\Viewers\\winopen.exe "%s" ',IfrTbx, exeNWWJava);
        end
        eval(cmd);
    else
        my_warndlg(Lang('BathyScope est d�j� ouvert !', 'BathyScope is already running !'), 1);
    end
    end
    FIN 17/02/2010, GLU, Suppression du lancement vers DotNet
    %} 
    
    % Lancement exclusif de la version NWW Java.
    if flagRunSSC3DViewer
        if isdeployed
            my_warndlg(Lang('Lancement direct de SSC-3DViewer : en cours de travaux ','Running Directly SSC-3DViewer : Work in progress'), 1);
            
%             exeNWWJava = fullfile(IfrTbx, 'ifremer', 'extern', 'java', 'lib', 'SonarScope-3DViewer.jar');
%             cmd = sprintf('!winopen "%s" ',exeNWWJava);
        else
            exeNWWJava = fullfile(IfrTbx, 'ifremer', 'extern', 'java', 'lib', 'SonarScope-3DViewer.jar');
            cmd = sprintf('!%s\\Installation\\Viewers\\winopen.exe "%s" ',IfrTbx, exeNWWJava);
        end
        eval(cmd);
    end    

        
    flag = 1;
    

end
