% Importation d'un fichier ASCII ODV (sondes donnees en colonnes)
%
% SYNTAX :
%   [flag, Data] = read_odv(nomFic)
%
% INPUT PARAMETERS :
%   nomFic : Nom du fichier
%
% OUTPUT PARAMETERS :
%   flag : 1 si lecture reussie, 0 sinon
%   Data : Ensemble de donn�es
%
% EXAMPLES :
%   TODO
% SEE ALSO  : cl_image/import_* Authors
% AUTHORS   : JMA
% VERSION   : $Id: import_xyz.m,v 1.1 2005/09/14 08:23:13 augustin Exp augustin $
%--------------------------------------------------------------------------

function [flag, Data, I] = read_odv(nomFic, varargin)

% % % sub            = getPropertyValue(varargin, 'sub',            []);
% % % Carto          = getPropertyValue(varargin, 'Carto',          []);
Separator      = getPropertyValue(varargin, 'Separator',      []);
NbLigEntete    = getPropertyValue(varargin, 'NbLigEntete',    []);
DataType       = getPropertyValue(varargin, 'DataType',       []);
GeometryType   = getPropertyValue(varargin, 'GeometryType',   []);
ValNaN         = getPropertyValue(varargin, 'ValNaN',         []);
nColumns       = getPropertyValue(varargin, 'nColumns',       []);
IndexColumns   = getPropertyValue(varargin, 'IndexColumns',   []);
% % % ScaleFactor    = getPropertyValue(varargin, 'ScaleFactor',    []);
% % % ControleManuel = getPropertyValue(varargin, 'ControleManuel', 1);
% % % pasxy          = getPropertyValue(varargin, 'pasxy',          []);
Unit           = getPropertyValue(varargin, 'Unit',           []);
DisplayEntete  = getPropertyValue(varargin, 'DisplayEntete',  1);

AskSeparator    = getPropertyValue(varargin, 'AskSeparator',    1);
AskValNaN       = getPropertyValue(varargin, 'AskValNaN',       1);
AskGeometryType = getPropertyValue(varargin, 'AskGeometryType', 1);
% % % TypeAlgo        = getPropertyValue(varargin, 'TypeAlgo',        []);

flagMemmapfile = getPropertyValue(varargin, 'Memmapfile', 0);

% % % this = [];
I0 = cl_image_I0;

%% Create the file

fid = fopen(nomFic, 'r');
if fid == -1
    [nomDir, nomFic] = fileparts(nomFic);
    nomFic = fullfile(nomDir, [nomFic '.xyz']);
    fid = fopen(nomFic, 'r');
    if fid == -1
        flag = 0;
        return
    end
end

for i=1:20
    Entete{i} = fgetl(fid);%#ok
end
fclose(fid);

%% Decodage guid� par l'op�rateur

if DisplayEntete
    str = sprintf(Lang('Debut du fichier %', 'Begin of the file %s'), nomFic);
    LigneTemoin = Entete{end};
    mots = strsplit(LigneTemoin);
    
    Entete{end+1} = ' ';
    Entete{end+1} = Lang('Information sur le contenu des colonnes pour la derni�re ligne', 'Information about columns content for last line');
    for i=1:length(mots)
        Entete{end+1} = sprintf('Column %d : %s', i, mots{i}); %#ok
    end
    my_txtdlg(str, Entete, 'windowstyle', 'normal');
end

%% Nombre de lignes d'ent�te

if isempty(NbLigEntete)
    a = cla_edit_params('Titre', 'xyz importation', ...
        'nom', Lang('Nb de lignes d''entete', 'Nb of Header Lines'), ...
        'value', 0, 'minvalue', 0, 'nbColUnite', 0, 'format', '%d');
    a = editobj(a);
    flag = get(a, 'Validation');
    if ~flag
        return
    end
    NbLigEntete = get(a, 'value');
elseif NbLigEntete < 0
    a = cla_edit_params('Titre', 'xyz importation', ...
        'nom', Lang('Nb de lignes d''entete', 'Nb of Header Lines'), ...
        'value', -NbLigEntete, 'minvalue', 0, 'nbColUnite', 0, 'format', '%d');
    a = editobj(a);
    flag = get(a, 'Validation');
    if ~flag
        return
    end
    NbLigEntete = get(a, 'value');
end

%% Separateur

if AskSeparator
    if isempty(Separator)
        str1 = 'S�parateur sp�cifique autre que "Espace", "Chararct�re blanc" ou "Tabulation"';
        str2 = 'Specific separator if any. (OK if white-space character like space or Tab).';
        [Separator, flag] = my_inputdlg({Lang(str1,str2)}, {''});
        if ~flag
            return
        end
        Separator = Separator{1};
    end
end

if AskValNaN
    if isempty(ValNaN)
        [ValNaN, flag] = my_inputdlg(Lang({'Sp�cifier la valeur pour "NO-DATA" si besoin'},{'Specific value for "No Data" if any'}), {''});
        if ~flag
            return
        end
        ValNaN = ValNaN{1};
        if ~isempty(ValNaN)
            if strcmpi(ValNaN, 'NaN')
                ValNaN = [];
            else
                ValNaN = str2double(ValNaN);
            end
        end
    end
end

%% Nombe de colonnes utiles

if isempty(nColumns)
    a = cla_edit_params('Titre', 'xyz importation', ...
        'nom', Lang('Nb de champs par ligne', 'Nb of fields in columns'), ...
        'value', 3, 'minvalue', 0, 'nbColUnite', 0, 'format', '%d');
    a = editobj(a);
    flag = get(a, 'Validation');
    if ~flag
        return
    end
    nColumns = get(a, 'value');
elseif nColumns < 1
    a = cla_edit_params('Titre', 'xyz importation', ...
        'nom', Lang('Nb de champs par ligne', 'Nb of fields in columns'), ...
        'value', -nColumns, 'minvalue', 0, 'nbColUnite', 0, 'format', '%d');
    a = editobj(a);
    flag = get(a, 'Validation');
    if ~flag
        return
    end
    nColumns = get(a, 'value');
end

%% Type de g�om�trie

if AskGeometryType
    if isempty(GeometryType)
        str = get(I0, 'strGeometryType');
        GeometryType = my_listdlg(Lang('Type de coordonn�es : ', 'Type of coordinates : '), str, ...
            'SelectionMode', 'single');
    elseif GeometryType < 0
        str = get(I0, 'strGeometryType');
        GeometryType = my_listdlg(Lang('Type de coordonn�es : ', 'Type of coordinates : '), str, ...
            'SelectionMode', 'single', 'InitialValue', -GeometryType);
    end
end

if isempty(IndexColumns)
    IndexColumns = [7 6 8 16];
    switch GeometryType
        case LatLong(I0)
            NomVar = {'Longitude', 'Latitude', 'Bot. Depth', 'Mag'}; 
            [flag, IndexColumns] = getColumnOrder(NomVar, IndexColumns);
        otherwise
            NomVar = {'X', 'Y', 'Bot. Depth', 'Mag'}; 
            [flag, IndexColumns] = getColumnOrder(NomVar, IndexColumns);
    end
    if ~flag
        return
    end
end

%% Type de donn�es

if isempty(DataType)
    [flag, DataType] = edit_DataType(I0);
    if ~flag
        return
    end
elseif DataType < 0
    [flag, DataType] = edit_DataType(I0, 'DataType', -DataType);
    if ~flag
        return
    end
end

%% Unit�

if isempty(Unit)
    Unit= expectedUnitOfData(I0, DataType);
    str1 = 'Unit� de la mesure.';
    str2 = 'Unit of the data.';
    [Unit, flag] = my_inputdlg(Lang({str1},{str2}), {Unit});
    if ~flag
        return
    end
    Unit = Unit{1};
end

%% Lecture
[Data]      = lectureIndirecte(nomFic, NbLigEntete, Separator, IndexColumns, nColumns, flagMemmapfile);

[flag, I] 	= createImagefromODV(Data);


%{
if ~isempty(sub)
    Data.X = Data.X(sub);
    Data.Y = Data.Y(sub);
    Data.V = Data.V(sub);
    Data.U = Data.U(sub);
    Data.W = Data.W(sub);
    Data.Mag = Data.Mag(sub);
end

if ~isempty(ValNaN)
    Data.Mag(Data.Mag == ValNaN) = NaN;
end
sub = ~isnan(Data.Mag);
Data.X = Data.X(sub);
Data.Y = Data.Y(sub);
Data.Mag = Data.Mag(sub);
clear sub

unique_x = unique(Data.X);
unique_y = unique(Data.Y);

if isempty(unique_x) || isempty(unique_y)
    my_warndlg('File reading failure.', 1)
    flag = 0;
    return
end

cols = length(unique_x);
rows = length(unique_y);
N = length(Data.Mag);

% LatMean = [];
% LonMean = [];

if isempty(GeometryType)
    unitexy = 'deg';
else
    if GeometryType == LatLong(I0)
        unitexy = 'deg';
    elseif GeometryType == GeoYX(I0)
        unitexy = 'm';
    else
        unitexy = '';
    end
end

x = unique(Data.X);
y = unique(Data.Y);

pas = max(1, floor(N/1e5));

xmin = min(Data.X);
xmax = max(Data.X);
ymin = min(Data.Y);
ymax = max(Data.Y);

if ControleManuel
    str1 = 'Une figure va pr�senter la r�partition des donn�es, vous devrez la d�truire apr�s avoir �ventuellement zoom� sur la zone que vous voulez importer. Le traitement se poursuivra apr�s la destruction de cette fen�tre.';
    str2 = 'A figure will give you a preview of the data, you will have to delete it to continue or zoom on the good part of it and then delete it.';
    my_warndlg(Lang(str1,str2), 1)
    fig = figure;
    axes;
    plot(Data.X(1:pas:end), Data.Y(1:pas:end), '+');
    grid on;
    axis([xmin xmax ymin ymax])
    title('You have to kill me to continue')
    
    nomFicSave = fullfile(my_tempdir,'ffff.mat');
    clbk = sprintf('w = axis; save(''%s'', ''w'')', nomFicSave);
    set(fig, 'DeleteFcn', clbk)
    
    % ----------------
    % Boucle d'attente
    
    while ishandle(fig)
        uiwait(fig);
        w = loadmat(nomFicSave, 'nomVar', 'w');
        delete(nomFicSave)
    end
else
    w = [xmin xmax ymin ymax];
end


%% Carto

if isempty(Carto)
    if GeometryType == LatLong(I0)
        [flag, Carto] = editParams(cl_carto([]), mean([ymin ymax]), mean([xmin xmax]), 'MsgInit');
    else
        [flag, Carto] = editParams(cl_carto([]), [], [], 'MsgInit');
    end
    if ~flag
        return
    end
end


if ~isequal(w, [xmin xmax ymin ymax])
    xmin = w(1);
    xmax = w(2);
    ymin = w(3);
    ymax = w(4);
    sub = find((Data.X >= xmin) & (Data.X <= xmax) & (Data.Y >= ymin) & (Data.Y <= ymax));
    Data.Y = Data.Y(sub);
    Data.X = Data.X(sub);
    Data.Mag = Data.Mag(sub);
    N = length(Data.Y);
    
    xmin = min(Data.X);
    xmax = max(Data.X);
    ymin = min(Data.Y);
    ymax = max(Data.Y);
end

rangex = xmax - xmin;
rangey = ymax - ymin;
surfaceUnitaire = rangex * rangey / N;

if GeometryType == PingBeam(I0)
    pasy = 1;
else
    pasy = sqrt(surfaceUnitaire) * 2;
end

%% Type de l'algorithme

if isempty(TypeAlgo)
    str1 = 'Algorithme de maillage.';
    str2 = 'Gridding algorithm';
    str3 = 'Plus proche voisin';
    str4 = 'Nearest';
    str5 = 'Biin�aire';
    str6 = 'Biinear';
    [TypeAlgo, flag] = my_questdlg(Lang(str1,str2), Lang(str3,str4), Lang(str5,str6));
    
    %{
clear str
str1 = 'Algorithme de maillage.';
str2 = 'Gridding algorithm';
Titre = Lang(str1,str2);
str1 = 'Plus proche voisin';
str2 = 'Nearest';
str{1} = Lang(str1,str2);
str1 = 'Biin�aire';
str2 = 'Biinear';
str{2} = Lang(str1,str2);
% str1 = 'Gauss';
% str2 = 'Gauss';
% str{3} = Lang(str1,str2);
[TypeAlgo, flag] = my_listdlg(Titre, str, 'SelectionMode', 'single');
    %}
    if ~flag
        return
    end
end

%% Cr�ation des axes x et y

if isempty(pasxy)
    flag = 0;
    while ~flag
        if GeometryType == LatLong(I0)
            pasx = pasy / cos(mean(y) * (pi/180));
        else
            pasx = pasy;
        end
        
        x = xmin:pasx:xmax;
        y = ymin:pasy:ymax;
        [x, pasx, xmin, xmax] = centrage_magnetique(x);
        [y, pasy, ymin, ymax] = centrage_magnetique(y);
        
        cols = length(x);
        rows = length(y);
        
        if GeometryType == LatLong(I0)
            str = sprintf('Proposition : \n\nPixel size (m): %f\nRows        : %d\nColumns    : %d\n\nAre you satisfied ?', ...
                pasy * (6378137 * (pi/180)), rows, cols);
        else
            str = sprintf('Proposition : \n\nPixel size : %f\nRows        : %d\nColumns    : %d\n\nAre you satisfied ?', ...
                pasx, rows, cols);
        end
        [rep, flag] = my_questdlg(Lang('TODO', str));
        if ~flag
            return
        end
        if rep == 1
            flag = 1;
        else
            if GeometryType == LatLong(I0)
                pasyEdition = pasy * (6378137 * pi/180);
            else
                pasyEdition = pasy;
            end
            a = cla_edit_params('titre', Lang('TODO', 'The importation will grid the data. Please select the grid step'), ...
                'nom', 'Grid step (m)', 'Unit', unitexy, 'value', pasyEdition);
            a = editobj(a);
            flag = get(a, 'Validation');
            if ~flag
                return
            end
            pasy = get(a, 'value');
            if GeometryType == LatLong(I0)
                pasy = pasy / (6378137 * pi/180);
                pasx = pasy / cos(mean(y) * (pi/180));
            else
                pasx = pasy;
            end
            flag = 0;
        end
    end
else
    pasx = pasxy(1);
    pasy = pasxy(2);
    x = xmin:pasx:xmax;
    y = ymin:pasy:ymax;
    [x, pasx, xmin] = centrage_magnetique(x);
    [y, pasy, ymin] = centrage_magnetique(y);
    cols = length(x);
    rows = length(y);
end

%% Cr�ation de l'image

% [xi,yi] = meshgrid(x, y);
% z2 = griddata(Data.Y, Data.X, Data.V, xi, yi);

z1 = zeros(rows, cols, 'single');
z2 = zeros(rows, cols, 'single');
WorkInProgress(Lang('Maillage en cours, patience', 'Griding data,    please wait ...'));
% Pour info : j'ai d� supprimer le my(waitbar car il prenait 99% du temps

switch TypeAlgo
    case 1 % Plus proche voisin
        tic
        for i=1:N
            ix = 1 + floor((Data.X(i) - xmin) / pasx);
            iy = 1 + floor((Data.Y(i) - ymin) / pasy);
            
            if (iy >= 1) && (iy <= rows) && (ix >= 1) && (ix <= cols)
                z1(iy,ix) = z1(iy,ix) + 1;
                z2(iy,ix) = z2(iy,ix) + Data.Mag(i);
            end
        end
        sub = (z1 ~= 0);
        z2(sub) = z2(sub) ./ z1(sub);
        sub = find(z1 == 0);
        z2(sub) = NaN;
        z1(sub) = NaN;
        clear sub
        toc
        
    case 2 % Lin�aire
        tic
        for i=1:N
            xp = (Data.X(i) - xmin) / pasx;
            yp = (Data.Y(i) - ymin) / pasy;
            ix1 = 1 + floor(xp);
            iy1 = 1 + floor(yp);
            xp = xp +1;
            yp = yp + 1;
            ix2 = ix1 + 1;
            iy2 = iy1 + 1;
            
            if ((iy1 >= 1) && (iy1 <= rows) && (ix1 >= 1) && (ix1 <= cols) && ...
                    (iy2 >= 1) && (iy2 <= rows) && (ix2 >= 1) && (ix2 <= cols))
                xalpha = ix2 - xp;
                yalpha = iy2 - yp;
                
                A1 =     xalpha*yalpha;
                A2 = (1-xalpha)*yalpha;
                A3 = (1-xalpha)*(1-yalpha);
                A4 =     xalpha*(1-yalpha);
                Vali = Data.Mag(i);
                
                z1(iy1,ix1) = z1(iy1,ix1) + A1;
                z2(iy1,ix1) = z2(iy1,ix1) + A1 * Vali;
                
                z1(iy1,ix2) = z1(iy1,ix2) + A2;
                z2(iy1,ix2) = z2(iy1,ix2) + A2 * Vali;
                
                z1(iy2,ix2) = z1(iy2,ix2) + A3;
                z2(iy2,ix2) = z2(iy2,ix2) + A3 * Vali;
                
                z1(iy2,ix1) = z1(iy2,ix1) + A4;
                z2(iy2,ix1) = z2(iy2,ix1) + A4 * Vali;
            end
        end
        sub = (z1 ~= 0);
        z2(sub) = z2(sub) ./ z1(sub);
        sub = find(z1 == 0);
        z2(sub) = NaN;
        z1(sub) = NaN;
        clear sub
        toc
        
    case 3 % Fen�tre de pond�ration gaussi�ne
        % TODO : � poursuivre
        sigma = 0.75;
        n = floor(2 * sigma);
        window = [max(3,2*n+1) max(3,2*n+1)];
        w = window(1);
        %         Data.Y = normpdf(Data.X,mu,sigma)
        h = 100 * fspecial('gaussian', [1 w*100*2], sigma*100);
        h = h(floor(length(h)/2):end);
        %     figure(20081008); plot(h); grid on; title('Gaussian coefficient filter');
        subh = -n:n;
        IX = repmat(subh,  w, 1);
        IY = repmat(subh', 1, w);
        tic
        for i=1:N
            Vali = Data.Mag(i);
            xp = (Data.X(i) - xmin) / pasx;
            yp = (Data.Y(i) - ymin) / pasy;
            ix = 1 + floor(xp);
            iy = 1 + floor(yp);
            
            ix = IX + ix;
            iy = IY + iy;
            
            sub = find((iy >= 1) & (iy <= rows) & (ix >= 1) & (ix <= cols));
            
            for k=1:length(sub)
                kk = sub(k);
                xh = xp+1-ix(kk);
                yh = yp+1-iy(kk);
                %             g = normpdf(sqrt(xh*xh+yh*yh),0,sigma);
                g = h(1+floor(100*sqrt(xh*xh+yh*yh)));
                kx = ix(kk);
                ky = iy(kk);
                z1(ky,kx) = z1(ky,kx) + g;
                z2(ky,kx) = z2(ky,kx) + g * Vali;
            end
        end
        z1(z1 < 0.5) = NaN;
        sub = (z1 ~= 0);
        z2(sub) = z2(sub) ./ z1(sub);
        sub = find(z1 == 0);
        z2(sub) = NaN;
        z1(sub) = NaN;
        clear sub
        toc
end


[nomDir, Titre] = fileparts(nomFic);

if ~isempty(ScaleFactor)
    z2 = z2 * ScaleFactor;
end

flag = 1;


%}

function [flag, ind] = getColumnOrder(nomVar, IndexColumns)


N = length(nomVar);
for i=1:N
    value{i} = num2str(IndexColumns(i)); %#ok<AGROW>
end
[value, flag] = my_inputdlg(nomVar, value);
if ~flag
    ind = [];
    return
end
ind = zeros(N, 1);
for i=1:N
    ind(i) = str2double(value{i});
end



% --------------------------------------------------------------------------
function [Data] = lectureIndirecte(nomFic, NbLigEntete, Separator, ind, nColumns, flagMemmapfile)

% Comptage du nombre de lignes
NLignesTotal   = str2double( perl('countlines.pl', nomFic) );

% Cr�ation d'une zone memmapfile.
if flagMemmapfile==1
    Data        = cl_memmapfile('FileName', tempname, 'Value', 0, 'Size', [NLignesTotal-NbLigEntete 6], 'Format', 'double');
else
    Data        = zeros(NLignesTotal-NbLigEntete, 6, 'double');
end


fid = fopen(nomFic, 'r');
for i=1:NbLigEntete
    Entete{i} = fgetl(fid); %#ok
end


% % % % % % Data.X = {};
% % % % % % Data.Y = {};
% % % % % % Data.V = {};
% % % % % % Data.U = {};
% % % % % % Data.W = {};
% % % % % % Data.Mag = {};
OK = 1;
Format ='';

for i=1:nColumns
    Format = [Format '%s']; %#ok
end

str             = sprintf(Lang('Lecture des Data du fichier %s. Patientez SVP\n', 'Reading data from %s, please wait.'), nomFic);
h               = create_waitbar(str);
pppp            = dir(nomFic);
tailleFic       = pppp.bytes;
NLignesToRead   = 20000;
offset          = 0;
while OK
    tailleLue = ftell(fid);
    waitbar(tailleLue/tailleFic, h);
    if isempty(Separator)
        C = textscan(fid, Format, NLignesToRead);
        % C = textscan(fid, Format, 1e6);
    else
        C = textscan(fid, Format, NLignesToRead, 'delimiter', Separator);
        % C = textscan(fid, Format, 1e6, 'delimiter', Separator);
    end
    if isempty(C{1})
        OK = 0;
    else
        N = length(C{1});
        if N ~= NLignesToRead
            NLignesToRead = N;
        end
        str1 = sprintf('IFREMER - SonarScope : decodage du fichier par paquets de %d lignes.', NLignesToRead);
        str2 = sprintf('IFREMER - SonarScope : decode data by blocks of %d lines.', NLignesToRead);
        w = create_waitbar(Lang(str1,str2));
        for i=1:N
            my_waitbar(i, N, w);
            % Affectation des colonnes � revoir.
            Data(i+offset,1) =  str2double(C{ind(2)}(i));
            Data(i+offset,2) =  str2double(C{ind(3)}(i));
            Data(i+offset,3) =  str2double(C{ind(1)}(i));
            Data(i+offset,4) =  str2double(C{ind(4)}(i));
            Data(i+offset,5) =  str2double(C{ind(5)}(i));
            Data(i+offset,6) =  str2double(C{ind(6)}(i));
% % % % % %             Data.X{i} = str2double(C{ind(2)}(i));
% % % % % %             Data.Y{i} = str2double(C{ind(3)}(i));
% % % % % %             Data.V{i} = str2double(C{ind(1)}{i});
% % % % % %             Data.U{i} = str2double(C{ind(4)}{i});
% % % % % %             Data.W{i} = str2double(C{ind(5)}{i});
% % % % % %             Data.Mag{i} = str2double(C{ind(6)}{i});
        end
        offset = offset + N;

        close(w)
    end
end
fclose(fid);
close(h);
% % % % % % Data.X = [Data.X{:}];
% % % % % % Data.Y = [Data.Y{:}];
% % % % % % Data.V = [Data.V{:}];
% % % % % % Data.U = [Data.U{:}];
% % % % % % Data.W = [Data.W{:}];
% % % % % % Data.Mag = [Data.Mag{:}];
