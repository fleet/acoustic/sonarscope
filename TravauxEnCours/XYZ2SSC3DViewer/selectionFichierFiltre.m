% -------------------------------------------------------------------------
function [nomFic, flag] = selectionFichierFiltre(filtre, varargin)

global IfrTbxResources
global SonarScopeData

Exclude = getPropertyValue(varargin, 'Exclude', []);

if ~iscell(filtre)
    filtre = {filtre};
end

listeFicPerDir = {};
for i=1:length(filtre)
    listeFicPerDir{end+1} = findFicInDir(IfrTbxResources,                       filtre{i}, 'Exclude', Exclude); %#ok
    listeFicPerDir{end+1} = findFicInDir(fullfile(SonarScopeData,    'Public',  filtre{i}), 'Exclude', Exclude); %#ok
    listeFicPerDir{end+1} = findFicInDir(fullfile(SonarScopeData,    'Sonar',   filtre{i}), 'Exclude', Exclude); %#ok
    listeFicPerDir{end+1} = findFicInDir(fullfile(SonarScopeData,    'Etopo',   filtre{i}), 'Exclude', Exclude); %#ok
    listeFicPerDir{end+1} = findFicInDir(fullfile(SonarScopeData,    'Levitus', filtre{i}), 'Exclude', Exclude); %#ok
end
liste = {};
for i=1:length(listeFicPerDir)
    for k=1:length(listeFicPerDir{i})
        liste{end+1} = listeFicPerDir{i}{k}; %#ok
    end
end

if isempty(liste)
    my_warndlg('No file exists. Are you sure you have installed the demonstration data directories ?', 1)
end

[rep, flag] = my_listdlg(Lang('Choisissez un fichier','Select a file'), liste);
if flag
    if length(rep) > 1
        nomFic = liste(rep);
    else
        nomFic = liste{rep};
    end
else
    nomFic = [];
end

