% Extraction d'un fichier ODV sous forme de Tuile Nasa World Wind.
% SYNTAXE :
% [flag] = test_XYZ3SSC3DV
%
% Compilation : sous R2009b
% Options de compilation :
% - placement dans le r�pertoire cit�.
% - pas de commande DOS associ� mais un fichier Log.
%   mcc -v -m test_XYZ2SSC3DV.m -e -R '-logfile,test_XYZ2SSC3DV.log' -d C:\Temp\test_XYZ2SSC3DV test_XYZ2SSC3DV
%
% EXAMPLE sous DOS:
%   test_XYZ2SSC3DV
% -----------------------------------------------------------------------

function flag = test_ODV2SSC3DV
% [ListeFic,PathName] = uigetfile('*.txt','Choisissez un fichier ODV, SVP .', 'F:\SonarScopeData\From C.Vrignaud', ...
%                                 'MultiSelect', 'on');
                            
ListeFic = 'ADCP_20090219_115939_204430_1.txt';
ListeFic = 'ADCP_38khz_Malaga_Brest_007.txt';
PathName = 'F:\SonarScopeData\From C.Vrignaud';

if ~isempty(ListeFic)
    if ischar(ListeFic)
        ListeFic = {ListeFic};
    end
else
    return
end


%%
nbFics = length(ListeFic);
if nbFics > 1
    hw = create_waitbar(Lang('Importation de fichier, patience ...', 'Importing files, please wait ...'));
else
    hw = [];
end

%% Traitement des N fichiers s�lectionn�s.
for i=1:nbFics
    my_waitbar(i, nbFics, hw)
    
    % Importation des images
    if isempty(ListeFic{i})
        continue
    end
    nomFic = fullfile(PathName, ListeFic{i});
    str1 = sprintf('Importation de "%s"', [ListeFic{i}]);
    str2 = sprintf('Importing "%s"', [ListeFic{i}]);
    WorkInProgress(Lang(str1,str2))
    

    % ---------------------------------------------
    % Extraction des donn�es.
    [nomFicSeul, ~] = strtok(ListeFic{i}, '.');
    NomFicXML       = fullfile(PathName, [nomFicSeul '_Mag.xml']);
    [~, filename]   =  fileparts(ListeFic{i});
    c               = cl_carto('Ellipsoide.Type', 11, 'Projection.Type', 3, 'Projection.UTM.Fuseau', 37);
    [flag, Data, I]    = read_odv(nomFic, 'IndexColumns', [2 6 7 8 9 16], 'NbLigEntete', 1, ...
                        'nColumns', 18, 'DisplayEntete', 0, 'GeometryType', 3, ...
                        'DataType', 3, 'Unit', 'cm/s', 'Carto', c, 'AskSeparator', 0, ...
                        'AskValNaN', 0, 'Memmapfile', 0);
    if ~flag
        return
    end
    LouisADCP3DViewerV2_unitaire(Data, I, NomFicXML, 'Mag', 'cm/s');


end
%%
if ishandle(hw)
    close(hw)
end
function flag = LouisADCP3DViewerV2_unitaire(Data, Image, NomFicXML, TypeData, Unit)

% % %     LatI    = unique(Data.Y);
% % %     LonI    = unique(Data.X);
% % %     ZI      = unique(Data.U);
    LatI    = unique(Data(:,2));
    LonI    = unique(Data(:,1));
    ZI      = unique(Data(:,4));

%{
figure; plot(-BathI); grid on; title('BathI')
figure; plot(LonI, LatI); grid on; title('Nav')
figure; plot(ZI); grid on; title('ZI')
figure; imagesc(Salt); colorbar; title('Salt')
figure; imagesc(Temp); colorbar; title('Temp')
%}

nbPings  = length(LatI);

map = jet(256);

switch TypeData
    case 'Temp'
        CLim = [10 20];
        X = Temp;
    case 'Salt'
        CLim = [35.25 35.70];
        X = Salt;
    case 'Fluo'
        CLim = [0 13];
        X = Fluo;
    case 'Mag'
% % % %         maxZI = ceil(max(Data.I(:)));
% % % %         minZI = fix(min(Data.I(:)));
        
        maxZI = ceil(max(Image(:)));
        minZI = fix(min(Image(:)));
        CLim = [minZI maxZI];
        map  = gray(256);
        map = flipud(map);
        str1 = sprintf('%s\n%s', 'Voulez-vous redefinir le contraste ?', 'Si oui, une fen�tre SonarScope va s''ouvrir afin que vous r�gleriez le contraste. N''oubliez pas de faire "Save" avant de quitter. Le m�me rehaussement de contraste sera utilis� pour toutes les images.');
        str2 = sprintf('%s\n%s', 'Do you want to redefine the contrast ?', 'If you answer yes, a SonarScope window is going to be created, please fix the contrast and click on "Save" before "Quit". The save contrast will be used for all the images.');
        [rep, validation] = my_questdlg(Lang(str1, str2), 'Init', 1);
        if validation == 1
            if rep == 1
                a = cl_image('Image', Image, 'ColormapIndex', 2, 'Video', 2);
                a = SonarScope(a);
                CLim = get(a, 'CLim');
                map = get(a, 'Colormap');
                Video = get(a, 'Video');
                if Video == 2
                    map = flipud(map);
                end
                X = get(a, 'Image');
            else
                X = Image;
            end
        else
            X = Image;
        end

end

%% Cr�ation du r�pertoire

[nomDirXml, nomDirBin] = fileparts(NomFicXML);
nomDirXml = fullfile(nomDirXml, nomDirBin);
nomDirRoot = fileparts(nomDirXml);
if ~exist(nomDirXml, 'dir')
    flag = mkdir(nomDirXml);
    if ~flag
        messageErreurFichier(nomDirXml, 'WriteFailure')
        return
    end
end

%% Cr�ation du r�pertoire

nomDirPNG = fullfile(nomDirXml, '000');
if ~exist(nomDirPNG, 'dir')
    flag = mkdir(nomDirPNG);
    if ~flag
        messageErreurFichier(nomDirPNG, 'WriteFailure')
        return
    end
end

%% Cr�ation de l'image PNG

I = gray2ind(mat2gray(X, double(CLim)), size(map,1));
% I = flipud(I); % A confirmer
subNaN = isnan(X);
I(I == 0) = 1;
I(subNaN) = 0;

nomFic1 = '00000.png';
nomFic2 = fullfile(nomDirPNG, nomFic1);
imwrite(I, map, nomFic2, 'Transparency', 0)

%% Definition of the structure

Info.Signature1      = 'SonarScope';
Info.Signature2      = 'ImagesAlongNavigation';
Info.FormatVersion   = 20100624;
Info.Name            = nomDirBin;
% Info.Description.Content    = TypeData;
% Info.Description.Unit       = Unit;
Info.ExtractedFrom   = 'Louis Mat files';

Info.Survey         = 'Unknown';
Info.Vessel         = 'Unknown';
Info.Sounder        = 'Unknown';
Info.ChiefScientist = 'Unknown';


Info.Dimensions.nbPings  = nbPings;
Info.Dimensions.nbSlices = 1;
Info.Dimensions.nbRows   = 0;

Data.AcrossDistance =  0;
Data.SliceExists    = true;
Data.Date           = NaN(1, nbPings);
Data.Hour           = NaN(1, nbPings);
Data.PingNumber     = 1:nbPings;
Data.LatitudeNadir  = LatI;
Data.LongitudeNadir = LonI;
Data.LatitudeTop    = LatI;
Data.LongitudeTop   = LonI;
Data.DepthTop       = zeros(1, nbPings);
Data.DepthBottom    = ones(1, nbPings) * -max(ZI);


Info.Signals(1).Name          = 'Date';
Info.Signals(end).Dimensions  = '1, nbPings';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'Ifremer';
Info.Signals(end).FileName    = fullfile(nomDirBin,'Date.bin');

Info.Signals(end+1).Name      = 'Hour';
Info.Signals(end).Dimensions  = '1, nbPings';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'nb milliseconds since midnight';
Info.Signals(end).FileName    = fullfile(nomDirBin,'Hour.bin');

Info.Signals(end+1).Name      = 'PingNumber';
Info.Signals(end).Dimensions  = '1, nbPings';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = ' ';
Info.Signals(end).FileName    = fullfile(nomDirBin,'PingNumber.bin');

Info.Signals(end+1).Name      = 'LatitudeNadir';
Info.Signals(end).Dimensions  = '1, nbPings';
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile(nomDirBin, 'LatitudeNadir.bin');

Info.Signals(end+1).Name      = 'LongitudeNadir';
Info.Signals(end).Dimensions  = '1, nbPings';
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile(nomDirBin, 'LongitudeNadir.bin');

Info.Signals(end+1).Name      = 'SliceExists';
Info.Signals(end).Dimensions  = 'nbSlices, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile(nomDirBin,'SliceExists.bin');

Info.Signals(end+1).Name      = 'DepthTop';
Info.Signals(end).Dimensions  = '1, nbPings';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'm';
Info.Signals(end).FileName    = fullfile(nomDirBin, 'DepthTop.bin');

Info.Signals(end+1).Name      = 'DepthBottom';
Info.Signals(end).Dimensions  = '1, nbPings';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'm';
Info.Signals(end).FileName    = fullfile(nomDirBin, 'DepthBottom.bin');

Info.Signals(end+1).Name      = 'AcrossDistance';
Info.Signals(end).Dimensions  = 'nbSlices, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'm';
Info.Signals(end).FileName    = fullfile(nomDirBin, 'AcrossDistance.bin');


Info.Images(1).Name           = 'LatitudeTop';
Info.Images(end).Dimensions   = 'nbSlices, nbPings';
Info.Images(end).Storage      = 'double';
Info.Images(end).Unit         = 'deg';
Info.Images(end).FileName     = fullfile(nomDirBin, 'LatitudeTop.bin');

Info.Images(end+1).Name       = 'LongitudeTop';
Info.Images(end).Dimensions   = 'nbSlices, nbPings';
Info.Images(end).Storage      = 'double';
Info.Images(end).Unit         = 'deg';
Info.Images(end).FileName     = fullfile(nomDirBin,'LongitudeTop.bin');

% Info.Images(end+1).Name       = 'Reflectivity';
% Info.Images(end).Dimensions   = 'nbRows, nbPings, nbSlices';
% Info.Images(end).Storage      = 'single';
% Info.Images(end).Unit         = 'dB';
% Info.Images(end).FileName     = fullfile(nomDirBin,'Reflectivity.bin');


%% Cr�ation du fichier XML d�crivant la donn�e

% NomFicXML = fullfile(nomDirRacine, [TypeDataVoxler '.xml']);
xml_write(NomFicXML, Info);
flag = exist(NomFicXML, 'file');
if ~flag
    messageErreur(NomFicXML)
    return
end

%% Cr�ation du r�pertoire Info

nomDirInfo = nomDirXml;
if ~exist(nomDirInfo, 'dir')
    status = mkdir(nomDirInfo);
    if ~status
        messageErreur(nomDirInfo)
        flag = 0;
        return
    end
end

%% Cr�ation des fichiers binaires des signaux

for i=1:length(Info.Signals)
    flag = writeSignal(nomDirRoot, Info.Signals(i), Data.(Info.Signals(i).Name));
    if ~flag
        return
    end
end

%% Cr�ation des fichiers binaires des signaux

for i=1:length(Info.Images)
    flag = writeImage(nomDirRoot, Info.Images(i), Data.(Info.Images(i).Name));
    if ~flag
        return
    end
end

flag = 1;
        

