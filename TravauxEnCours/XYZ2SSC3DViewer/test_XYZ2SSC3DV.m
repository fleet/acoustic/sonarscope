% Extraction d'un fichier XYZ sous forme de Tuile Nasa World Wind.
% SYNTAXE :
% [flag] = test_XYZ3SSC3DV
%
% Compilation : sous R2009b
% Options de compilation :
% - placement dans le r�pertoire cit�.
% - pas de commande DOS associ� mais un fichier Log.
%   mcc -v -m test_XYZ2SSC3DV.m -e -R '-logfile,test_XYZ2SSC3DV.log' -d C:\Temp\test_XYZ2SSC3DV test_XYZ2SSC3DV
%
% EXAMPLE sous DOS:
%   test_XYZ2SSC3DV
% -----------------------------------------------------------------------

function flag = test_XYZ2SSC3DV

persistent  repExport

[ListeFic,PathName] = uigetfile('*.xyz','Choisissez un fichier XYZ, SVP .', 'F:\SonarScopeData\Level2\Sonar', ...
                                'MultiSelect', 'on');
if ListeFic ~= 0
    if ischar(ListeFic)
        ListeFic = {ListeFic};
    end
else
    return
end


%%
nbFics = length(ListeFic);
if nbFics > 1
    hw = create_waitbar(Lang('Importation de fichier, patience ...', 'Importing files, please wait ...'));
else
    hw = [];
end

%% Traitement des N fichiers s�lectionn�s.
for i=1:nbFics
    my_waitbar(i, nbFics, hw)
    
    % Importation des images
    if isempty(ListeFic{i})
        continue
    end
    nomFic = fullfile(PathName, ListeFic{i});
    str1 = sprintf('Importation de "%s"', [ListeFic{i}]);
    str2 = sprintf('Importing "%s"', [ListeFic{i}]);
    WorkInProgress(Lang(str1,str2))
    
    I0          = cl_image([]);
    [flag, a]   = import_xyz(I0, nomFic); %#ok<ASGLU>
    if ~flag
        return
    end

    % ---------------------------------------------
    % Extraction des Min et Max sur la donn�e Bathym�trie extraite.

    x = get(a(1), 'x');
    y = get(a(1), 'y');

    subx = 1:length(x);
    suby = 1:length(y);

    GeometryType = get_GeometryType(a(1));
    if GeometryType == LatLong(I0)
        sizeTileNWW = 150;

        % Tuilage pour Nasa.
        [flag, repExport] = exportTilingNasaWorldWindBil(a(1), 'subx', subx, 'suby', suby, ...
        'SizeTileNWW', sizeTileNWW, 'repExport', 'c:\Temp');
    end
    
    %% Calcul de l'ombrage sur l'image.
    [rep, ~] = my_questdlg('Voulez-vous calculer l''ombrage  ?');
    if rep == 1
        % Saisie des param�tres de l'ombrage.
        Azimuth  = get(a(1), 'Azimuth');
        VertExag = get(a(1), 'VertExag');
        algo     = 1;

        p(1) = cl_parametre('nom', Lang('Azimut', 'Azimuth'), 'unite', 'deg', 'minvalue', 0, 'maxvalue', 360, ...
            'value', Azimuth);
        p(2) = cl_parametre('nom', Lang('Coefficient d''ombrage', 'Shading coefficient'), 'minvalue', 0, 'maxvalue', 100, ...
            'value', VertExag);
        b = cla_parametre('titre', Lang('Param�tres de la fonction d''ombrage', 'Sun illumination parametres'), ...
            'param', p, 'widths', [0 2 0 1 0 1 0]);
        b = editobj(b);

        flag        = get(b, 'Validation');
        val         = get_value(b);
        Azimuth     = val(1);
        VertExag    = val(2);
        algo        = 1;
        % Calcul de l'ombrage.
        if flag
            a(1) = set(a(1), 'Azimuth',  Azimuth);
            a(1) = set(a(1), 'VertExag', VertExag);
            a(2) = ombrage(a(1),'algo', algo, 'subx', subx, 'suby', suby); %#ok<NASGU>
            % Tuilage de l'ombrage pour Nasa.
            [flag, repExport] = exportTilingNasaWorldWindBil(a(2), 'subx', subx, 'suby', suby, ...
            'SizeTileNWW', sizeTileNWW, 'repExport', 'c:\Temp');
        end

        if ~flag
            continue
        end
    end
        

end
%%
if ishandle(hw)
    close(hw)
end

