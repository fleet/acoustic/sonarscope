% Fichier creationMCRIntaller_exe.m
% Recr�ation du moteur Matlab Component RunTime (dif�rent selon les
% versions).
%
% SYNTAX :
%   creationMCRIntaller_exe
%
% INPUT PARAMETERS :
%   N�ant
%
% EXAMPLES :
%
% SEE ALSO : Authors
% AUTHORS  : JMA
% VERSION  : $Id: entete.m,v 1.2 2003/11/12 12:46:11 augustin Exp $
%--------------------------------------------------------------------------
function creationMCRIntaller_exe

global IfrTbx
buildmcr(fullfile(IfrTbx, 'Installation', 'Install_MCR'))