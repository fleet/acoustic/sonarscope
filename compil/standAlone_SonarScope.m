% Compilation de SonarScope
%
% SYNTAX :
%   standAlone_SonarScope(...)
%
% Name-Value Pair Arguments
%   nomFunc : Nom de la fonction
%
% EXAMPLES :
%   standAlone_SonarScope('nomFunc', 'SonarScope')
%
% SEE ALSO : Authors
% AUTHORS  : JMA
%-------------------------------------------------------------------------------

function standAlone_SonarScope(varargin)

global IfrTbx
global IfrDpy

tic

v = ver;
v = v(1).Release;
DirVer = v(2:7);

Path = getenv('path');
PathCleaned = CleanPath(Path);
setenv('path', PathCleaned)

setenv('MCC_USE_DEPFUN', '0')

% Initialisation des objets JIDE pour �viter un message de licence.
% cf : http://undocumentedmatlab.com/blog/date-selection-components
com.mathworks.mwswing.MJUtilities.initJIDE;

try
    [varargin, nomFunc] = getPropertyValue(varargin, 'nomFunc', 'SonarScope'); %#ok<ASGLU>
   
    str{1} = sprintf('mcc -v -m %s ', nomFunc);
    nomDirExe = fullfile(IfrDpy, DirVer);
    if ~exist(nomDirExe, 'dir')
        mkdir(nomDirExe)
    end
    str{end+1} = sprintf('-d %s -o %s', nomDirExe, nomFunc);
    
    % Int�gration des DLL Win32 ou 64 bits pour le traitement optimis� via Mex-OpenMP-CUDA.
    nomDirDll{1} = fullfile(IfrTbx, 'ifremer', 'extern', 'Mex-CUDA-OpenMP', 'dll');
    % Int�gration des DLL Win32 ou 64 bits pour le traitement du Bottom Detect Reson via Mex-OpenMP-CUDA.
    nomDirDll{2} = fullfile(IfrTbx, 'ifremer', 'extern', 'Mex-CUDA-OpenMP', 'reson', 'dll');
    
    % Construction de la liste des fichiers � compiler
    for k1=1:numel(nomDirDll)
        dir1 = dir(fullfile(nomDirDll{k1}, '*.mexw64'));
        dir2 = dir(fullfile(nomDirDll{k1}, '*.dll'));
        FileSources = [dir1; dir2 ];
        for k2=1:numel(FileSources)
            str{end+1} = sprintf('-a %s%s%s ',  nomDirDll{k1}, filesep, FileSources(k2).name); %#ok
        end
    end
    
    % Ajout des ressources de la contribution GUILayoutToolbox (uiExtras)
    % Forge : 458.
    str{end+1} = sprintf('-a %s%s%s ', [IfrTbx '\contributions\GUILayoutToolbox2.3.5\layout\*']);
%     str{end+1} = sprintf('-a %s ', fullfile(userpath, 'Add-Ons\Toolboxes\GUI_Layout_Toolbox\code\layout\*'));

    % Ajout des ressources de la contribution MOVIES pour les traitements BOB
    str{end+1} = sprintf('-a %s ', [IfrTbx '\contributions\MOVIES3D\*.dll']);
    str{end+1} = sprintf('-a %s ', [IfrTbx '\contributions\MOVIES3D\MATLABLINK\*.mexw64']);

    str = cell2str(str);
    str = str';
    str = str(:);
    str = str';
    str = rmblank(str);
    
    eval(str)
catch ME %#ok<CTCH>
    ME.getReport
    disp('Ca s''est pas bien fini.')
    return
end

setenv('path', Path)
disp('C''est fini.')
T = toc;
fprintf('Heure de fin de la compilation : %s  -  Temps de compilation : %4.1f\n', char(datetime), T/60);


function PathCleaned = CleanPath(Path)
mots = strsplit(Path, ';');
flagOK = true(1, length(mots));
for k1=1:length(mots)
    ind = strfind(mots{k1}, 'MATLAB Compiler Runtime');
    if ~isempty(ind)
        flagOK(k1) = false;
        continue
    end
end
PathCleaned = '';
for k1=1:length(mots)
    if flagOK(k1)
        PathCleaned = [PathCleaned ';' mots{k1}]; %#ok<AGROW>
    end
end
PathCleaned = PathCleaned(2:end);
