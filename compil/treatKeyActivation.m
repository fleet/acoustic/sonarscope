% TRanscodage des cl�s d'activation du fichier xls des mots de passe de
% SonarScope
%
% SYNTAX :
%   flag = treatKeyActivation(nomFic)
%
% INPUT PARAMETERS :
%  nomFic : nom du fichier de cl�s d'activation de SonarScope
%
% OUTPUT PARAMETERS :
%   flag : 1 (OK) ou 0 (erreur)
%
% EXAMPLES :
%   nomFic = 'C:\SonarScopeTbx\Installation\Scripts_SonarScope\Resources\ListePLP_SonarScope.xls'
%   [flag,X] = treatKeyActivation(nomFic)
%
% SEE ALSO   : SonarScope Authors
% AUTHORS    : GLU
%-------------------------------------------------------------------------------

function [flag, X] = treatKeyActivation(nomFic)

global IfrTbxResources

try
    % Lecture des 100 premiers clients/destinataires.
    %%%% Provoque un "touch" du fichier : genant pour svn .
    %%%% X = readfromexcel(filename, 'A1:G100');
    [num,txt,X] = xlsread(nomFic, '', 'A1:G100'); %#ok<ASGLU>
    nomFicPLP   = fullfile(IfrTbxResources, 'PLPSonarScope.mat');
        
    % Ecriture du fichier ListePLP_SonarScope.csv
    [pathstr, name, ~, ] = fileparts(nomFic);
    nomFicCSV = fullfile(pathstr, [name '.csv']);
    
    % Elimination des lignes disposant de NaN.
    cellclass      = 'char';
    ciscellclass   = cellfun('isclass',X,cellclass);
    X              = X(ciscellclass);
    sz             = size(X,1);
    X              = reshape(X, sz/7, 7);
    [nrows, ncols] = size(X); %#ok<ASGLU>
    fid = fopen(nomFicCSV, 'w');
    for row=1:nrows
        fprintf(fid, '%s;%s;%s;%s;%s;%s;%s;\n', X{row,:});
    end
    fclose(fid);

    % Sauvegarde apr�s cryptage dans le fichier PLP de d�ploiement.
    X = cryptPLP(X);
    save(nomFicPLP, 'X');
    flag = 1;
catch  %#ok<CTCH>
    X       = [];
    flag    = 0;
end