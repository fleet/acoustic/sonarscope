function abort(ncid)
%my_netcdf.abort Revert recent netCDF file definitions.
%   my_netcdf.abort(ncid) will revert a netCDF file out of any definitions
%   made after my_netcdf.create but before my_netcdf.endDef.  The file will
%   also be closed.
%
%   This function corresponds to the function "nc_abort" in the netCDF 
%   library C API.
%
%   Please read the files netcdfcopyright.txt and mexnccopyright.txt for 
%   more information.
%
%   See also netcdf, my_netcdf.create, my_netcdf.endDef.
%

%   Copyright 2008-2013 The MathWorks, Inc.

try
    netcdflib('abort',ncid);
catch
    matlab.internal.imagesci.netcdflib('abort',ncid);
end
