function close(ncid)
%my_netcdf.close Close netCDF file.
%   my_netcdf.close(ncid) terminates access to the netCDF file identified
%   by ncid.
%
%   This function corresponds to the "nc_close" function in the netCDF 
%   library C API.
%
%   Please read the files netcdfcopyright.txt and mexnccopyright.txt for 
%   more information.
%
%   See also netcdf, my_netcdf.open, my_netcdf.create.

%   Copyright 2008-2013 The MathWorks, Inc.

try
    netcdflib('close', ncid);
catch
    matlab.internal.imagesci.netcdflib('close', ncid);
end
