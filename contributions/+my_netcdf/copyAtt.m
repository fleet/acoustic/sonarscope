function copyAtt(ncid_in,varid_in,attname,ncid_out,varid_out)
%my_netcdf.copyAtt Copy attribute to new location.
%   my_netcdf.copyAtt(ncid_in,varid_in,attname,ncid_out,varid_out) copies 
%   an attribute from one variable to another, possibly across files.
%
%   This function corresponds to the "nc_copy_att" function in the netCDF 
%   library C API.
%
%   Example:
%       srcFile = fullfile(matlabroot,'toolbox','matlab','demos','example.nc');
%       copyfile(srcFile,'myfile.nc');
%       fileattrib('myfile.nc','+w');
%       ncid = my_netcdf.open('myfile.nc','WRITE');
%       varid_in = my_netcdf.inqVarID(ncid,'temperature');
%       varid_out = my_netcdf.inqVarID(ncid,'peaks');
%       my_netcdf.copyAtt(ncid,varid_in,'scale_factor',ncid,varid_out);
%       my_netcdf.close(ncid);
%
%   Please read the files netcdfcopyright.txt and mexnccopyright.txt for 
%   more information.
%
%   See also netcdf, my_netcdf.putAtt.

%   Copyright 2008-2013 The MathWorks, Inc.

if nargin > 2
    attname = convertStringsToChars(attname);
end

try
    netcdflib('copyAtt', ncid_in, varid_in, attname, ncid_out, varid_out);
catch
    matlab.internal.imagesci.netcdflib('copyAtt', ncid_in, varid_in, attname, ncid_out, varid_out);
end
