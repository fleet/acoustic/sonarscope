function dimid = defDim(ncid,dimname,dimlen)
%my_netcdf.defDim Create netCDF dimension.
%   dimid = my_netcdf.defDim(ncid,dimname,dimlen) creates a new dimension 
%   given its name and length.  The return value is the numeric ID
%   corresponding to the new dimension.  dimlen for unlimited dimensions
%   should be specified by the constant value for 'UNLIMITED'.
%
%   This function corresponds to the "nc_def_dim" function in the netCDF 
%   library C API.
%
%   Example:  Create a netCDF file with a fixed-size dimension called 'lat'
%   and an unlimited dimension called 'time'.
%       ncid      = my_netcdf.create('myfile.nc','NOCLOBBER');
%       latDimId  = my_netcdf.defDim(ncid,'latitude',180);
%       timeDimId = my_netcdf.defDim(ncid,'time',my_netcdf.getConstant('UNLIMITED'));
%       my_netcdf.close(ncid);
%
%   Please read the files netcdfcopyright.txt and mexnccopyright.txt for 
%   more information.
%
%   See also netcdf, my_netcdf.defVar, my_netcdf.inqDimIDs.

%   Copyright 2008-2013 The MathWorks, Inc.

if nargin > 1
    dimname = convertStringsToChars(dimname);
end

try
    dimid = netcdflib('defDim', ncid, dimname, dimlen);
catch
    dimid = matlab.internal.imagesci.netcdflib('defDim', ncid, dimname, dimlen);
end
