function childNcid = defGrp(ncid,childGroupName)
%my_netcdf.defGrp Create group.
%   childGrpID = my_netcdf.defGrp(parentGroupId,childGroupName) creates a child 
%   group with name childGroupName given the identifier of the parent 
%   group specified by parentGroupId.
%
%   This function corresponds to the "nc_def_grp" function in the netCDF 
%   library C API.  
%
%   Example:
%       ncid = my_netcdf.create('myfile.nc','netcdf4');
%       childGroupId = my_netcdf.defGrp(ncid,'mygroup');
%       my_netcdf.close(ncid);
%
%   Please read the files netcdfcopyright.txt and mexnccopyright.txt for 
%   more information.
%
%   See also netcdf, my_netcdf.inqGrps.

%   Copyright 2010-2013 The MathWorks, Inc.

if nargin > 1
    childGroupName = convertStringsToChars(childGroupName);
end

try
    childNcid = netcdflib('defGrp',ncid,childGroupName);
catch
    childNcid = matlab.internal.imagesci.netcdflib('defGrp',ncid,childGroupName);
end