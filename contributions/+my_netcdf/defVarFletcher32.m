function defVarFletcher32(ncid,varid,setting)
%my_netcdf.defVarFletcher32 Define checksum parameters for netCDF variable.
%   my_netcdf.defVarFletcher32(ncid,varid,setting) defines the checksum settings 
%   for a netCDF variable specified by varid in the file specified by 
%   ncid.
%
%   The setting can be either 'NOCHECKSUM' or 'FLETCHER32'.  If setting is 
%   'fletcher32', then checksums will be turned on for this variable.
%
%   This function corresponds to the "nc_def_var_fletcher32" function in 
%   the netCDF library C API.  
%
%   Example:
%       ncid = my_netcdf.create('myfile.nc','NETCDF4');
%       latdimid = my_netcdf.defDim(ncid,'lat',1800);
%       londimid = my_netcdf.defDim(ncid,'col',3600);
%       varid = my_netcdf.defVar(ncid,'earthgrid','double',[latdimid londimid]);
%       my_netcdf.defVarFletcher32(ncid,varid,'FLETCHER32');
%       my_netcdf.close(ncid);
%
%   Please read the files netcdfcopyright.txt and mexnccopyright.txt for 
%   more information.
%
%   See also netcdf, my_netcdf.inqVarFletcher32.

%   Copyright 2010-2013 The MathWorks, Inc.

if nargin > 2
    setting = convertStringsToChars(setting);
end

if ischar(setting)
	setting = my_netcdf.getConstant(setting);
end

try
    netcdflib('defVarFletcher32', ncid, varid, setting);
catch
    matlab.internal.imagesci.netcdflib('defVarFletcher32', ncid, varid, setting);
end
