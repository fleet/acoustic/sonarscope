function delAtt(ncid,varid,attname)
%my_netcdf.delAtt Delete netCDF attribute.
%   my_netcdf.delAtt(ncid,varid,attName) deletes the attribute identified
%   by attName from the variable identified by varid.  In order to delete
%   a global attribute, use my_netcdf.getConstant('GLOBAL') for the varid.
%
%   This function corresponds to the "nc_del_att" function in the netCDF 
%   library C API.
%
%   Example:
%       srcFile = fullfile(matlabroot,'toolbox','matlab','demos','example.nc');
%       copyfile(srcFile,'myfile.nc');
%       fileattrib('myfile.nc','+w');
%       ncid = my_netcdf.open('myfile.nc','WRITE');
%       my_netcdf.reDef(ncid);
%       my_netcdf.delAtt(ncid,my_netcdf.getConstant('GLOBAL'),'creation_date');
%       my_netcdf.close(ncid);
%
%   Please read the files netcdfcopyright.txt and mexnccopyright.txt for 
%   more information.
%
%   See also netcdf, my_netcdf.putAtt, my_netcdf.getConstant.

%   Copyright 2008-2013 The MathWorks, Inc.

if nargin > 2
    attname = convertStringsToChars(attname);
end

try
    netcdflib('delAtt', ncid, varid, attname);
catch
    matlab.internal.imagesci.netcdflib('delAtt', ncid, varid, attname);
end
