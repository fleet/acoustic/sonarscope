function endDef(ncid,varargin)
%my_netcdf.endDef End netCDF file define mode.
%   my_netcdf.endDef(ncid) Takes a netCDF file identified by ncid out of
%   define mode.
%
%   my_netcdf.endDef(ncid,h_minfree,v_align,v_minfree,r_align) is the same as
%   my_netcdf.endDef, but with the addition of four performance tuning
%   parameters.  One reason for using the performance parameters is to
%   reserve extra space in the netCDF file header using the h_minfree
%   parameter.
%
%   Example:  Reserve 20000 bytes in the netCDF header which may be used
%   at a later time when adding attributes.  This can be extremely
%   efficient when working with very large netCDF-3 files.
%       ncid = my_netcdf.create('myfile.nc','CLOBBER');
%       % define dimensions, variables
%       my_netcdf.endDef(ncid,20000,4,0,4);
%       my_netcdf.close(ncid);
%
%   This function corresponds to the "nc_enddef" and "nc__enddef" functions 
%   in the netCDF library C API.
%
%   Please read the files netcdfcopyright.txt and mexnccopyright.txt for 
%   more information.
%
%   See also netcdf, my_netcdf.create, my_netcdf.reDef.

%   Copyright 2008-2013 The MathWorks, Inc.


switch nargin
    case 1
        try
            netcdflib('endDef', ncid);
        catch
            matlab.internal.imagesci.netcdflib('endDef', ncid);
        end
    case 5
        try
            netcdflib('pEndDef', ncid, varargin{:});
        catch
            matlab.internal.imagesci.netcdflib('pEndDef', ncid, varargin{:});
        end
    otherwise
        error (message('MATLAB:imagesci:validate:wrongNumberOfInputs'));
end
