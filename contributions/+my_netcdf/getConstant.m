function param = getConstant(param_name)
%my_netcdf.getConstant Return numeric value of named constant.
%   val = my_netcdf.getConstant(param_name) returns the numeric value 
%   corresponding to the name of a constant defined by the netCDF
%   library.  
%
%   The value for param_name can be either upper case or lower case, and
%   does not need to include the leading three characters 'NC_'.
%
%   The list of all names can be retrieved with my_netcdf.getConstantNames
%
%   Example:
%       val = my_netcdf.getConstant('NOCLOBBER');
%
%   Please read the files netcdfcopyright.txt and mexnccopyright.txt for 
%   more information.
%
%   See also netcdf, my_netcdf.getConstantNames
%

%   Copyright 2008-2013 The MathWorks, Inc.

if nargin > 0
    param_name = convertStringsToChars(param_name);
end

switch param_name
    case {'NOWRITE'; 'CHUNKED'; 'NC_NOWRITE'}
        param = 0;
    case {'WRITE'; 'NC_BYTE'}
        param = 1;
    case {'char'; 'NC_CHAR'}
        param = 2;
    case 'int'
        param = 4;
    case {'float'; 'NC_FLOAT'}
        param = 5;
    case {'double'; 'NC_DOUBLE'}
        param = 6;
    case {'ubyte'; 'NC_UBYTE'}
        param = 7;
    case 'NC_USHORT'
        param = 8;
    case {'uint'; 'NC_UINT'}
        param = 9;
    case 'NC_INT64'
        param = 10;
    otherwise
        try
            param = netcdflib('parameter', param_name);
        catch
            param = matlab.internal.imagesci.netcdflib('parameter', param_name);
        end
        my_breakpoint
end
