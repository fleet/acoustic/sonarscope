function names = getConstantNames()
%my_netcdf.getConstantNames Return list of constants known to netCDF library.
%   names = my_netcdf.getConstantNames() returns a list of names of netCDF 
%   library constants, definitions, and enumerations.  When these 
%   strings are supplied as actual parameters to the netCDF package 
%   functions, they will automatically be converted to the appropriate 
%   numeric value.
%
%   Please read the files netcdfcopyright.txt and mexnccopyright.txt for 
%   more information.
%
%   See also netcdf, my_netcdf.create, my_netcdf.defVar, my_netcdf.open, 
%   my_netcdf.setDefaultFormat, my_netcdf.setFill.
%

%   Copyright 2008-2013 The MathWorks, Inc.

try
    names = netcdflib('getConstantNames');
catch
    names = matlab.internal.imagesci.netcdflib('getConstantNames');
end

names = sort(names);
