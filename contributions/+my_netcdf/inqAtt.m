function [xtype,attlen] = inqAtt(ncid,varid,attname)
%my_netcdf.inqAtt Return information about netCDF attribute.
%   [xtype,attlen] = my_netcdf.inqAtt(ncid,varid,attname) returns
%   the datatype and length of an attribute identified by attname.
%
%   This function corresponds to the "nc_inq_att" function in the netCDF 
%   library C API.
%
%   Example:
%       ncid = my_netcdf.open('example.nc','NOWRITE');
%       varid = my_netcdf.inqVarID(ncid,'temperature');
%       [xtype,attlen] = my_netcdf.inqAtt(ncid,varid,'scale_factor');
%       my_netcdf.close(ncid);
%
%   Please read the files netcdfcopyright.txt and mexnccopyright.txt for 
%   more information.
%
%   See also netcdf, my_netcdf.getAtt, my_netcdf.putAtt.

%   Copyright 2008-2013 The MathWorks, Inc.

if nargin > 2
    attname = convertStringsToChars(attname);
end

try
    [xtype, attlen] = netcdflib('inqAtt', ncid, varid, attname);
catch
    [xtype, attlen]  = matlab.internal.imagesci.netcdflib('inqAtt', ncid, varid, attname);
end
