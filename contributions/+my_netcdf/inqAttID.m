function attid = inqAttID(ncid,varid,attname)
%my_netcdf.inqAttID Return ID of netCDF attribute.
%   attnum = my_netcdf.inqAttID(ncid,varid,attname) retrieves the 
%   number of the attribute associated with the attribute name.
%
%   This function  corresponds to the "nc_inq_att_id" function in the 
%   netCDF library C API.
%
%   Example:
%       ncid = my_netcdf.open('example.nc','NOWRITE');
%       varid = my_netcdf.inqVarID(ncid,'temperature');
%       attid = my_netcdf.inqAttID(ncid,varid,'scale_factor');
%       my_netcdf.close(ncid);
%
%   Please read the files netcdfcopyright.txt and mexnccopyright.txt for 
%   more information.
%
%   See also netcdf, my_netcdf.inqAtt.

%   Copyright 2008-2013 The MathWorks, Inc.

if nargin > 2
    attname = convertStringsToChars(attname);
end

try
    attid = netcdflib('inqAttID', ncid, varid, attname);
catch
    attid = matlab.internal.imagesci.netcdflib('inqAttID', ncid, varid, attname);
end