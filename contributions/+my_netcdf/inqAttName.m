function attname = inqAttName(ncid,varid,attnum)
%my_netcdf.inqAttName Return name of netCDF attribute.
%   attname = my_netcdf.inqAttName(ncid,varid,attnum) returns
%   the name of an attribute given the attribute number.
%
%   This function corresponds to the "nc_inq_attname" function in the 
%   netCDF library C API.
%
%   Example:
%       ncid = my_netcdf.open('example.nc','NOWRITE');
%       varid = my_netcdf.inqVarID(ncid,'temperature');
%       attname = my_netcdf.inqAttName(ncid,varid,0);
%       my_netcdf.close(ncid);
%
%   Please read the files netcdfcopyright.txt and mexnccopyright.txt for 
%   more information.
%
%   See also netcdf, my_netcdf.inqAtt, my_netcdf.inqAttID.

%   Copyright 2008-2013 The MathWorks, Inc.

try
    attname = netcdflib('inqAttName', ncid, varid, attnum);
catch
    attname = matlab.internal.imagesci.netcdflib('inqAttName', ncid, varid, attnum);
end