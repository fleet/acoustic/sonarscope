function [dimname,dimlen] = inqDim(ncid,dimid)
%my_netcdf.inqDim Return netCDF dimension name and length.
%   [dimname, dimlen] = my_netcdf.inqDim(ncid,dimid) returns the name and 
%   length of a dimension given the dimension identifier.
%
%   To use this function, you should be familiar with the information about 
%   netCDF contained in the "NetCDF C Interface Guide".  This function 
%   corresponds to the "nc_inq_dim" function in the netCDF library C API.
%
%   Example:
%       ncid = my_netcdf.open('example.nc','NOWRITE');
%       dimid = my_netcdf.inqDimID(ncid,'x');
%       [~,length] = my_netcdf.inqDim(ncid,dimid);
%       my_netcdf.close(ncid);
%
%   Please read the files netcdfcopyright.txt and mexnccopyright.txt for 
%   more information.
%
%   See also netcdf, my_netcdf.inqDimID, my_netcdf.inqVar.
    
%   Copyright 2008-2013 The MathWorks, Inc.

try
    [dimname, dimlen] = netcdflib('inqDim', ncid, dimid);
catch
    [dimname, dimlen] = matlab.internal.imagesci.netcdflib('inqDim', ncid, dimid);
end
