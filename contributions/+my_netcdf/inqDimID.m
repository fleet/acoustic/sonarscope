function dimid = inqDimID(ncid,dimname)
%my_netcdf.inqDimID Return dimension ID.
%   dimid = my_netcdf.inqDimID(ncid,dimname) returns the ID of a dimension
%   given the name.
%
%   To use this function, you should be familiar with the information about 
%   netCDF contained in the "NetCDF C Interface Guide".  This function 
%   corresponds to the "nc_inq_dimid" function in the netCDF library C API.
%
%   Example:
%       ncid = my_netcdf.open('example.nc','NOWRITE');
%       dimid = my_netcdf.inqDimID(ncid,'x');
%       my_netcdf.close(ncid);
%
%   Please read the files netcdfcopyright.txt and mexnccopyright.txt for 
%   more information.
%
%   See also netcdf, my_netcdf.inqDim.

%   Copyright 2008-2013 The MathWorks, Inc.

if nargin > 1
    dimname = convertStringsToChars(dimname);
end

try
    dimid = netcdflib('inqDimID', ncid, dimname);
catch
    dimid = matlab.internal.imagesci.netcdflib('inqDimID', ncid, dimname);
end