function dimids = inqDimIDs(ncid,includeParents)
%my_netcdf.inqDimIDs Return list of dimension identifiers in group.
%   dimIDs = my_netcdf.inqDimIDs(ncid) returns a list of dimension identifiers
%   in the group specified by ncid.  
%
%   dimIDs = my_netcdf.inqDimIDs(ncid,includeParents) includes all dimensions 
%   in all parent groups if includeParents is true.  By default, 
%   includeParents is false.
%
%   This function corresponds to the "nc_inq_dimids" function in the 
%   netCDF library C API.  
%
%   Example:
%       ncid = my_netcdf.open('example.nc','NOWRITE');
%       gid = my_netcdf.inqNcid(ncid,'grid1');
%       dimids = my_netcdf.inqDimIDs(gid);
%       my_netcdf.close(ncid);
%
%   Please read the files netcdfcopyright.txt and mexnccopyright.txt for 
%   more information.
%
%   See also netcdf, my_netcdf.inqVarIDs.

%   Copyright 2010-2013 The MathWorks, Inc.

if nargin < 2
	includeParents = false;
end

try
    dimids = netcdflib('inqDimIDs', ncid, includeParents);
catch
    dimids = matlab.internal.imagesci.netcdflib('inqDimIDs', ncid, includeParents);
end
