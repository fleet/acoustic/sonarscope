function groupName = inqGrpName(ncid)
%my_netcdf.inqGrpName Return relative name of group.
%   groupName = my_netcdf.inqGrpName(ncid) returns the name of a group
%   specified by ncid.  The root group will have name '/'.  
%
%   This function corresponds to the "nc_inq_grpname" function in the
%   netCDF library C API.
%
%   Example:
%       ncid = my_netcdf.open('example.nc','nowrite');
%       name = my_netcdf.inqGrpName(ncid);
%       my_netcdf.close(ncid);
%
%   Please read the files netcdfcopyright.txt and mexnccopyright.txt for 
%   more information.
%
%   See also netcdf, my_netcdf.inqGrpNameFull.

%   Copyright 2010-2013 The MathWorks, Inc.

try
    groupName = netcdflib('inqGrpName', ncid);
catch
    groupName = matlab.internal.imagesci.netcdflib('inqGrpName', ncid);
end