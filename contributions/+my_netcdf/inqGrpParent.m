function parentNcid = inqGrpParent(childNcid)
%my_netcdf.inqGrpParent Return ID of parent group.
%   parentGroupID = my_netcdf.inqGrpParent(ncid) returns the ID of the parent 
%   group given the location ncid of the child group.  
%
%   This function corresponds to the "nc_inq_grp_parent" function in the
%   netCDF library C API.
%
%   Example:
%       ncid = my_netcdf.open('example.nc','NOWRITE');
%       gid = my_netcdf.inqNcid(ncid,'grid2');
%       parentId = my_netcdf.inqGrpParent(gid);
%       fullName = my_netcdf.inqGrpNameFull(parentId);
%       my_netcdf.close(ncid);
%
%   Please read the files netcdfcopyright.txt and mexnccopyright.txt for 
%   more information.
%
%   See also netcdf, my_netcdf.inqGrps.

%   Copyright 2010-2013 The MathWorks, Inc.

try
    parentNcid = netcdflib('inqGrpParent', childNcid);
catch
    parentNcid = matlab.internal.imagesci.netcdflib('inqGrpParent', childNcid);
end
