function childGrps = inqGrps(ncid)
%my_netcdf.inqGrps Return array of child group IDs.
%   childGrps = my_netcdf.inqGrps(ncid) returns all the child group IDs in 
%   a parent group.
%
%   This function corresponds to the "nc_inq_grps" function in the 
%   netCDF library C API.  
%
%   Example:
%       ncid = my_netcdf.open('example.nc','nowrite');
%       childGroups = my_netcdf.inqGrps(ncid);
%       my_netcdf.close(ncid);
%
%   Please read the files netcdfcopyright.txt and mexnccopyright.txt for 
%   more information.
%
%   See also netcdf, my_netcdf.inqNcid.

%   Copyright 2010-2013 The MathWorks, Inc.

try
    childGrps = netcdflib('inqGrps', ncid);
catch
    childGrps = matlab.internal.imagesci.netcdflib('inqGrps', ncid);
end
