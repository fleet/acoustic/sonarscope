function libvers = inqLibVers()
%my_netcdf.inqLibVers Return netCDF library version information.
%   libvers = my_netcdf.inqLibVers returns a string identifying the 
%   version of the netCDF library.
%
%   This function corresponds to the "nc_inq_libvers" function in the 
%   netCDF library C API.
%
%   Example:
%       libvers = my_netcdf.inqLibVers();
%
%   Please read the files netcdfcopyright.txt and mexnccopyright.txt for 
%   more information.
%  
%   See also my_netcdf.

%   Copyright 2008-2015 The MathWorks, Inc.

try
    libvers = netcdflib('inqLibVers');
catch
    libvers = matlab.internal.imagesci.netcdflib('inqLibVers');
end

% The version sring returned from the netCDF library contains the version
% plus extra date information, so we need remove everything after
% the version.  
% With netCDF official release 4.6.1, need to extract two extra
% characters due to the ".1".
libvers = libvers(1:5);
