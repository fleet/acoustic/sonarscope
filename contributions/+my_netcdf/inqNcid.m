function childGrpId = inqNcid(ncid,childGroupName)
%my_netcdf.inqNcid Return ID of named group.
%   childGroupId = my_netcdf.inqNcid(ncid,childGroupName) returns the ID of 
%   the named child group in the group specified by ncid.
%
%   This function corresponds to the "nc_inq_ncid" function in the 
%   netCDF library C API.  
%
%   Example:
%       ncid = my_netcdf.open('example.nc','nowrite');
%       gid = my_netcdf.inqNcid(ncid,'grid1');
%       my_netcdf.close(ncid);
%
%   Please read the files netcdfcopyright.txt and mexnccopyright.txt for 
%   more information.
%
%   See also netcdf, my_netcdf.inqGrpName, netcdf,inqGrpNameFull.

%   Copyright 2010-2013 The MathWorks, Inc.

if nargin > 1
    childGroupName = convertStringsToChars(childGroupName);
end

try
    childGrpId = netcdflib('inqNcid', ncid, childGroupName);
catch
    childGrpId = matlab.internal.imagesci.netcdflib('inqNcid', ncid, childGroupName);
end
