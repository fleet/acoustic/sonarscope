function dimids = inqUnlimDims(ncid)
%my_netcdf.inqUnlimDims Return list of unlimited dimensions visible in group.
%   unlimdimIDs = my_netcdf.inqUnlimDims(ncid) returns the IDs of all 
%   unlimited dimensions in the group specified by ncid.  unlimDimIDs will 
%   be the empty set if no such unlimited dimensions exist.
%
%   This function corresponds to the "nc_inq_unlim_dims" function in the 
%   netCDF library C API.
%
%   Example:
%       ncid = my_netcdf.open('example.nc','NOWRITE');
%       dimids = my_netcdf.inqUnlimDims(ncid);
%       my_netcdf.close(ncid);
%
%   Please read the files netcdfcopyright.txt and mexnccopyright.txt for 
%   more information.
%
%   See also netcdf, my_netcdf.defDim, my_netcdf.inqDim, my_netcdf.inqDimID,
%   my_netcdf.renameDim, my_netcdf.inqDimIDs.
    
%   Copyright 2010-2013 The MathWorks, Inc.

try
    dimids = netcdflib('inqUnlimDims', ncid);
catch
    dimids = matlab.internal.imagesci.netcdflib('inqUnlimDims', ncid);
end
