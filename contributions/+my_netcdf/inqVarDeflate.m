function [shuffle,deflate,deflateLevel] = inqVarDeflate(ncid,varid)
%my_netcdf.inqVarDeflate Return compression settings for netCDF variable.
%   [shuffle,deflate,deflateLevel] = my_netcdf.inqVarDeflate(ncid,varid) 
%   returns the shuffle flag and deflate setting of a variable 
%   identified by varid in the file or group identified by ncid.
%
%   If shuffle is true, then the shuffle filter was turned on.
%
%   If deflate is true, then the deflate filter was turned on and the 
%   deflate level is returned in deflateLevel.
%
%   This function corresponds to the "nc_inq_var_deflate" function in the
%   netCDF library C API.
%
%   Example:
%       ncid = my_netcdf.open('example.nc','NOWRITE');
%       groupid = my_netcdf.inqNcid(ncid,'grid1');
%       varid = my_netcdf.inqVarID(groupid,'temp');
%       [shuffle,deflate,deflateLevel] = my_netcdf.inqVarDeflate(groupid,varid);
%       my_netcdf.close(ncid);
%
%   Please read the files netcdfcopyright.txt and mexnccopyright.txt for 
%   more information.
%
%   See also netcdf, my_netcdf.defVarDeflate.

%   Copyright 2010-2013 The MathWorks, Inc.

try
    [shuffle, deflate, deflateLevel] = netcdflib('inqVarDeflate', ncid, varid);
catch
    [shuffle, deflate, deflateLevel] = matlab.internal.imagesci.netcdflib('inqVarDeflate', ncid, varid);
end
