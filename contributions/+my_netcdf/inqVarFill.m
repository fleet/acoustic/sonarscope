function [noFillMode,fillValue] = inqVarFill(ncid,varid)
%my_netcdf.inqVarFill Return fill parameters for a variable in a netCDF-4 file.
%   [noFillMode,fillValue] = my_netcdf.inqVarFill(ncid,varid) returns the 
%   no-fill mode and the fill value itself for the variable varid.  
%   ncid identifies the file or group.
%
%   This function corresponds to the "nc_inq_var_fill" function in the 
%   netCDF library C API.  
%
%   Example:
%       ncid = my_netcdf.open('example.nc','NOWRITE');
%       varid = my_netcdf.inqVarID(ncid,'temperature');
%       [noFillMode,fillValue] = my_netcdf.inqVarFill(ncid,varid);
%       my_netcdf.close(ncid);
%
%   Please read the files netcdfcopyright.txt and mexnccopyright.txt for 
%   more information.
%
%   See also netcdf, my_netcdf.defVarFill, my_netcdf.setFill.

%   Copyright 2010-2016 The MathWorks, Inc.

try
    [noFillMode, fillValue] = netcdflib('inqVarFill', ncid, varid);
catch
    [noFillMode, fillValue] = matlab.internal.imagesci.netcdflib('inqVarFill', ncid, varid);
end
