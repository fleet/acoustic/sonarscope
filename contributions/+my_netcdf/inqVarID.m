function varid = inqVarID(ncid,varname)
%my_netcdf.inqVarID Return ID associated with variable name.
%   varid = my_netcdf.inqVarID(ncid,varname) returns the ID of a netCDF 
%   variable identified by varname.
%
%   This function corresponds to the "nc_inq_varid" function in the netCDF
%   library C API.
%
%   Example:
%       ncid = my_netcdf.open('example.nc','NOWRITE');
%       varid = my_netcdf.inqVarID(ncid,'temperature');
%       my_netcdf.close(ncid);
%
%   Please read the files netcdfcopyright.txt and mexnccopyright.txt for 
%   more information.
%
%   See also netcdf, my_netcdf.inqVar.

%   Copyright 2010-2013 The MathWorks, Inc.

if nargin > 1
    varname = convertStringsToChars(varname);
end

try
    varid = netcdflib('inqVarID', ncid, varname);
catch
    varid = matlab.internal.imagesci.netcdflib('inqVarID', ncid, varname);
end
