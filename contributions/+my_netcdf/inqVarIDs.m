function varids = inqVarIDs(ncid)
%my_netcdf.inqVarIDs Return list of variables in group.
%   varids = my_netcdf.inqVarIDs(ncid) returns the list of all variable IDs 
%   in the group specified by ncid.
%
%   This function corresponds to the "nc_inq_varids" function in the 
%   netCDF library C API.  
%
%   Example:
%       ncid = my_netcdf.open('example.nc','NOWRITE');
%       gid = my_netcdf.inqNcid(ncid,'grid1');
%       varids = my_netcdf.inqVarIDs(gid);
%       my_netcdf.close(ncid);
%
%   Please read the files netcdfcopyright.txt and mexnccopyright.txt for 
%   more information.
%
%   See also netcdf, my_netcdf.inqDimIDs.

%   Copyright 2010-2013 The MathWorks, Inc.

try
    varids = netcdflib('inqVarIDs', ncid);
catch
    varids = matlab.internal.imagesci.netcdflib('inqVarIDs', ncid);
end
