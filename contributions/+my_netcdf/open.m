function varargout = open(filename, varargin)
%my_netcdf.open Open NetCDF source.
%   ncid = my_netcdf.open(filename) opens an existing file in read-only mode.
%   ncid = my_netcdf.open(opendapURL) opens an OPeNDAP NetCDF data source in
%   read-only mode.
%
%   ncid = my_netcdf.open(filename, mode) opens a NetCDF file and returns a
%   netCDF ID in ncid. The type of access is described by the mode
%   parameter,  which can be 'WRITE' for read-write access, 'SHARE' for
%   synchronous file updates, or 'NOWRITE' for read-only access.  The mode
%   may also be a numeric value that can be retrieved via
%   my_netcdf.getConstant.  The mode may also be a bitwise-or of numeric mode
%   values.
%
%   [chosen_chunksize, ncid] = my_netcdf.open(filename, mode, chunksize)
%   is similar to the above, but makes use of an additional
%   performance tuning parameter, chunksize, which can affect I/O
%   performance.  The actual value chosen by the netCDF library may
%   not correspond to the input value.
%
%   This function corresponds to the "nc_open" and "nc__open" functions in
%   the netCDF library C API.
%
%   Example:
%       ncid = my_netcdf.open('example.nc','NOWRITE');
%       my_netcdf.close(ncid);
%
%   Please read the files netcdfcopyright.txt and mexnccopyright.txt for
%   more information.
%
%   See also netcdf, my_netcdf.close, my_netcdf.getConstant.


%   Copyright 2008-2016 The MathWorks, Inc.

if nargin > 0
    filename = convertStringsToChars(filename);
end

if nargin > 1
    [varargin{:}] = convertStringsToChars(varargin{:});
end

narginchk(1,3);

if contains(filename, '://')
    % OPeNDAP link
else
    % Get the full path name.
    fid = fopen(filename,'r');
    if fid == -1
        error(message('MATLAB:imagesci:validate:fileOpen',filename));
    end
    filename = fopen(fid);
    fclose(fid);
end


if(nargin>=2 && ischar(varargin{1}))
    varargin{1} = my_netcdf.getConstant(varargin{1});
end
varargout = cell(1,nargout);
switch nargin
    case 1
        try
            [varargout{:}] = netcdflib('open', filename, 'NOWRITE');
        catch
            [varargout{:}] = matlab.internal.imagesci.netcdflib('open', filename, 'NOWRITE');
        end
    case 2
        try
            [varargout{:}] = netcdflib('open', filename, varargin{1});
        catch
            [varargout{:}] = matlab.internal.imagesci.netcdflib('open', filename, varargin{1});
        end
    case 3
        try
            [varargout{:}] = netcdflib('pOpen', filename, varargin{1}, varargin{2});
        catch
            [varargout{:}] = matlab.internal.imagesci.netcdflib('pOpen', filename, varargin{1}, varargin{2});
        end
end

