function putAtt(ncid,varid,attname,attvalue)
%my_netcdf.putAtt Write netCDF attribute.
%   my_netcdf.putAtt(ncid,varid,attrname,attrvalue) writes an attribute
%   to a netCDF variable specified by varid.  In order to specify a 
%   global attribute, use my_netcdf.getConstant('GLOBAL') for the varid.  
%
%   Note: You cannot use my_netcdf.putAtt to set the _FillValue attribute of
%   NetCDF4 files. Use the my_netcdf.defVarFill function to set the fill value
%   for a variable. 
%
%   This function corresponds to the "nc_put_att" family of functions in 
%   the netCDF library C API.
%
%   Example:
%       ncid = my_netcdf.create('myfile.nc','CLOBBER');
%       varid = my_netcdf.getConstant('GLOBAL');
%       my_netcdf.putAtt(ncid,varid,'creation_date',datestr(now));
%       my_netcdf.close(ncid);
%
%   Please read the files netcdfcopyright.txt and mexnccopyright.txt for 
%   more information.
%
%   See also netcdf, my_netcdf.getAtt, my_netcdf.defVarFill, my_netcdf.getConstant.
%

%   Copyright 2008-2013 The MathWorks, Inc.

if nargin > 2
    attname = convertStringsToChars(attname);
end

if nargin > 3
    attvalue = convertStringsToChars(attvalue);
end

fmt = my_netcdf.inqFormat(ncid);

validateattributes(attvalue,{'numeric','char'},{},'','ATTVALUE');

persistent nc_classes;
nc_classes = { 'double','single','int64','uint64', 'int32','uint32', ...
            'int16', 'uint16', 'int8', 'uint8','char'};
typeAttValue = class(attvalue);
switch typeAttValue
    case nc_classes
        %% Nothing to do
    otherwise
        validatestring(typeAttValue, nc_classes);
end

% Determine the xtype (datatype) and attribute data parameters.
% Get the datatype from the class of data.
switch ( class(attvalue) )
    case 'double'
        xtype = my_netcdf.getConstant('double');
        funstr = 'putAttDouble';
    case 'single'
        xtype = my_netcdf.getConstant('float');
        funstr = 'putAttFloat';
    case 'int64'
        xtype = my_netcdf.getConstant('int64');
        funstr = 'putAttInt64';
    case 'uint64'
        xtype = my_netcdf.getConstant('uint64');
        funstr = 'putAttUint64';
    case 'int32'
        xtype = my_netcdf.getConstant('int');
        funstr = 'putAttInt';
    case 'uint32'
        xtype = my_netcdf.getConstant('uint');
        funstr = 'putAttUint';
    case 'int16'
        xtype = my_netcdf.getConstant('short');
        funstr = 'putAttShort';
    case 'uint16'
        xtype = my_netcdf.getConstant('ushort');
        funstr = 'putAttUshort';
    case 'int8'
        xtype = my_netcdf.getConstant('byte');
        funstr = 'putAttSchar';
    case 'uint8'
        if strcmp(fmt,'FORMAT_CLASSIC') ...
                || strcmp(fmt,'FORMAT_64BIT') ...
                || strcmp(fmt,'NETCDF4_FORMAT_CLASSIC')
            xtype = my_netcdf.getConstant('byte');
            funstr = 'putAttUchar';
        else
            xtype = my_netcdf.getConstant('ubyte');
            funstr = 'putAttUbyte';
        end
    case 'char'
        xtype = my_netcdf.getConstant('char');
        funstr = 'putAttText';
end

% Invoke the correct netCDF library routine.
if ischar(attvalue)
    try
        netcdflib('putAttText', ncid, varid, attname, attvalue);
    catch
        matlab.internal.imagesci.netcdflib('putAttText', ncid, varid, attname, attvalue);
    end
else
    try
        netcdflib(funstr, ncid, varid, attname, xtype, attvalue);
    catch
        matlab.internal.imagesci.netcdflib(funstr, ncid, varid, attname, xtype, attvalue);
    end
end
