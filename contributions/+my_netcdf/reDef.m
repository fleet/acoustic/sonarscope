function reDef(ncid)
%my_netcdf.reDef Set netCDF file into define mode.
%   my_netcdf.reDef(ncid) Puts an open netCDF dataset into define mode so 
%   that dimensions, variables, and attributes can be added or renamed.  
%   Attributes can also be deleted in define mode.
%
%   For all netCDF-4 files, the root ncid must be used. This is the ncid 
%   returned by my_netcdf.open and my_netcdf.create, and points to the root of 
%   the hierarchy tree for netCDF-4 files. 
%
%   To use this function, you should be familiar with the information about 
%   netCDF contained in the "NetCDF C Interface Guide".  This function 
%   corresponds to the "nc_redef" function in the netCDF library C API.
%
%   Example:
%       srcFile = fullfile(matlabroot,'toolbox','matlab','demos','example.nc');
%       copyfile(srcFile,'myfile.nc');
%       fileattrib('myfile.nc','+w');
%       ncid = my_netcdf.open('myfile.nc','WRITE');
%       my_netcdf.reDef(ncid);
%       my_netcdf.close(ncid);
%
%   Please read the files netcdfcopyright.txt and mexnccopyright.txt for 
%   more information.
%
%   See also netcdf, my_netcdf.endDef.

%   Copyright 2008-2013 The MathWorks, Inc.

try
    netcdflib('redef', ncid);
catch
    matlab.internal.imagesci.netcdflib('redef', ncid);
end
