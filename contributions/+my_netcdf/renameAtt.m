function renameAtt(ncid,varid,oldname,newname)
%my_netcdf.renameAtt Change name of netCDF attribute.
%   my_netcdf.renameAtt(ncid,varid,oldName,newName) renames the attribute 
%   identified by oldName to newName.  The attribute is associated with
%   the variable identified by varid.  A global attribute can be 
%   specified by using my_netcdf.getConstant('GLOBAL') for the varid.
%
%   To use this function, you should be familiar with the information about 
%   netCDF contained in the "NetCDF C Interface Guide".  This function 
%   corresponds to the "nc_rename_att" function in the netCDF library C 
%   API.
% 
%   Example:  Rename the global attribute 'creation_date' to
%   'modification_date'.
%       srcFile = fullfile(matlabroot,'toolbox','matlab','demos','example.nc');
%       copyfile(srcFile,'myfile.nc');
%       fileattrib('myfile.nc','+w');
%       ncid = my_netcdf.open('myfile.nc','WRITE');
%       varid = my_netcdf.getConstant('GLOBAL');
%       my_netcdf.renameAtt(ncid,varid,'creation_date','modification_date');
%       my_netcdf.close(ncid);
% 
%   Please read the files netcdfcopyright.txt and mexnccopyright.txt for 
%   more information.
%
%   See also netcdf, my_netcdf.renameDim, my_netcdf.renameVar.


%   Copyright 2008-2013 The MathWorks, Inc.

if nargin > 2
    oldname = convertStringsToChars(oldname);
end

if nargin > 3
    newname = convertStringsToChars(newname);
end

try
    netcdflib('renameAtt', ncid, varid, oldname, newname);
catch
    matlab.internal.imagesci.netcdflib('renameAtt', ncid, varid, oldname, newname);
end
