function renameDim(ncid,dimid,new_name)
%my_netcdf.renameDim Change name of netCDF dimension.
%   my_netcdf.renameDim(ncid,dimid,newName) renames a dimension identified
%   by dimid to the new name.
%
%   To use this function, you should be familiar with the information about 
%   netCDF contained in the "NetCDF C Interface Guide".  This function 
%   corresponds to the "nc_rename_dim" function in the netCDF library C 
%   API.
%
%   Example:
%       srcFile = fullfile(matlabroot,'toolbox','matlab','demos','example.nc');
%       copyfile(srcFile,'myfile.nc');
%       fileattrib('myfile.nc','+w');
%       ncid = my_netcdf.open('myfile.nc','WRITE');
%       my_netcdf.reDef(ncid);
%       dimid = my_netcdf.inqDimID(ncid,'x');
%       my_netcdf.renameDim(ncid,dimid,'new_x');
%       my_netcdf.close(ncid);
%       
%   Please read the files netcdfcopyright.txt and mexnccopyright.txt for 
%   more information.
%
%   See also netcdf, my_netcdf.reDef, my_netcdf.renameVar.
%

%   Copyright 2008-2013 The MathWorks, Inc.

if nargin > 2
    new_name = convertStringsToChars(new_name);
end

try
    netcdflib('renameDim', ncid, dimid, new_name);
catch
    matlab.internal.imagesci.netcdflib('renameDim', ncid, dimid, new_name);
end
