function renameVar(ncid,varid,new_name)
%my_netcdf.renameVar Change name of netCDF variable.
%   my_netcdf.renameVar(ncid,varid,newName) renames the variable identified 
%   by varid in the netCDF file or group associated with ncid.
%
%   This function corresponds to the "nc_rename_var" function in the netCDF
%   library C API.
%
%   Example:
%       srcFile = fullfile(matlabroot,'toolbox','matlab','demos','example.nc');
%       copyfile(srcFile,'myfile.nc');
%       fileattrib('myfile.nc','+w');
%       ncid = my_netcdf.open('myfile.nc','WRITE');
%       varid = my_netcdf.inqVarID(ncid,'temperature');
%       my_netcdf.renameVar(ncid,varid,'fahrenheight_temperature');
%       my_netcdf.close(ncid);
%
%   Please read the files netcdfcopyright.txt and mexnccopyright.txt for 
%   more information.
%
%   See also netcdf, my_netcdf.renameDim, my_netcdf.renameAtt.

%   Copyright 2008-2013 The MathWorks, Inc.

if nargin > 2
    new_name = convertStringsToChars(new_name);
end

try
    netcdflib('renameVar', ncid, varid, new_name);    
catch
    matlab.internal.imagesci.netcdflib('renameVar', ncid, varid, new_name);    
end
