function oldMode = setFill(ncid,newMode)
%my_netcdf.setFill Set netCDF fill mode.
%   oldMode = my_netcdf.setFill(ncid,newMode) sets the fill mode for a 
%   netCDF file.  newMode can be either 'FILL' or 'NOFILL' or their 
%   numeric equivalents as retrieved by my_netcdf.getConstant.  The default 
%   mode is 'FILL'.  The old fill mode is returned in oldMode.
%
%   To use this function, you should be familiar with the information about 
%   netCDF contained in the "NetCDF C Interface Guide".  This function 
%   corresponds to the "nc_set_fill" function in the netCDF library C API.
%
%   Example:
%       ncid = my_netcdf.create('myfile.nc','CLOBBER');
%       my_netcdf.setFill(ncid,'NOFILL');
%       my_netcdf.close(ncid);
%
%   Please read the files netcdfcopyright.txt and mexnccopyright.txt for 
%   more information.
%
%   See also netcdf, my_netcdf.defVarFill, my_netcdf.inqVarFill, 
%   my_netcdf.getConstant.
%

%   Copyright 2008-2013 The MathWorks, Inc.

if nargin > 1
    newMode = convertStringsToChars(newMode);
end

if ischar(newMode)
    % Convert the character value to a numeric value.
    newMode = my_netcdf.getConstant(newMode);
end

try
    oldMode = netcdflib('setFill', ncid, newMode);
catch
    oldMode = matlab.internal.imagesci.netcdflib('setFill', ncid, newMode);
end