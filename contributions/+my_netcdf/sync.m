function sync(ncid)
%my_netcdf.sync Synchronize netCDF dataset to disk.  
%   
%   my_netcdf.sync(ncid) synchronizes the state of a netCDF dataset to disk.  
%   The netCDF library will normally buffer accesses to the underlying
%   netCDF file unless the NC_SHARE mode is supplied to my_netcdf.open or
%   my_netcdf.create.
%
%   This function corresponds to the "nc_sync" function in the netCDF 
%   library C API.
%
%   Please read the files netcdfcopyright.txt and mexnccopyright.txt for 
%   more information.
%
%   See also netcdf, my_netcdf.open, my_netcdf.create, my_netcdf.close, 
%   my_netcdf.endDef.

%   Copyright 2008-2013 The MathWorks, Inc.

narginchk(1,1);

try
    netcdflib('sync', ncid);
catch
    matlab.internal.imagesci.netcdflib('sync', ncid);
end
