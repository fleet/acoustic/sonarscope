function p = moFilterModule(a)
if nargin == 0
   p.Enable=1;
   p = class(p,'moFilterModule');

   
elseif isa(a,'moFilterModule')
   p = a;
else
   p.Enable=1;
   p = class(p,'moFilterModule');

end
