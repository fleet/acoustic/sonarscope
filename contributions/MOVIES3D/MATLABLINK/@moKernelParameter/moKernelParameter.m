function p = moKernelParameter(a)
if nargin == 0
   p.read=true;
    p = class(p,'moKernelParameter');
elseif isa(a,'moKernelParameter')
   p = a;
else
   p.read=1;
   p = class(p,'moKernelParameter');
end