% moGetESUDefinition gets the ESU definition parameters
%   moGetESUDefinition ()
%       
%   return values : ESU cut type :
%					0 = cut by distance (miles)
%					1 = cut by ping
%					2 = cut by time (s)
%                   
%
% IPSIS OTonck 07/07/2008
% $Revision: 0.1 $
