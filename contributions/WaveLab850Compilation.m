% Compilation des mexFiles de la toolbox WaveLab
% (A faire une fois pour toutes sur chaque type de machine)
% C'est realise pour Solaris et Linux
%
% WaveLab850Compilation(IfremerToolboxContrib)
% 
% INPUT PARAMETERS : 
%   IfremerToolboxContrib : repertoire ou se trouve les contributions
%
% REMARKS : 
% 
% EXAMPLES :
%   IfremerToolboxContrib = IfrTbxCnt
%   WaveLab850Compilation(IfremerToolboxContrib)
%
% SEE ALSO   : matlab_startup acoustics_startup Authors
% AUTHORS    : JMA
% VERSION    : $Id: WaveLab850Compilation.m,v 1.1 2002/07/25 09:46:55 augustin Exp $
%-------------------------------------------------------------------------------

function  WaveLab850Compilation(IfremerToolboxContrib)
InstallMEX