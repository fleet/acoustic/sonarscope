% Initialisation generale des contributions extrieures
%
% ifrStartupContributions(IfrTbxCnt)
% 
% INPUT PARAMETERS : 
%   IfrTbxCnt : repertoire ou se trouve les 
%
% REMARKS : 
% 
% EXAMPLES :
%   IfrTbxCnt = IfrTbxCnt
%   ifrStartupContributions(IfrTbxCnt)
%
% SEE ALSO : matlab_startup acoustics_startup Authors
% AUTHORS  : JMA
%-------------------------------------------------------------------------------

function ifrStartupContributions(IfrTbxCnt)

% keyboard
path(path, fullfile(IfrTbxCnt, 'findjobj'));
path(path, fullfile(IfrTbxCnt, 'gabortb'));
% path(path, fullfile(IfrTbxCnt, 'waterloo')); % Comment� le 22/09/2020 car conflit de nom ('MATLAB') � partir de la R2020B
path(path, fullfile(IfrTbxCnt, 'OpenStreetMap'));
% path(path, fullfile(IfrTbxCnt, 'waterloo', 'Utilities'));
% path(path, fullfile(IfrTbxCnt, 'waterloo', 'Utilities', 'MATLAB'));
% path(path, fullfile(IfrTbxCnt, 'waterloo', 'Utilities', 'MATLAB', 'FileUtilities'));
% path(path, fullfile(IfrTbxCnt, 'waterloo', 'Utilities', 'MATLAB', 'FileUtilities', 'MAT'));
path(path, fullfile(IfrTbxCnt, 'SEGY', 'S4M', 'Geophysics_3.0'));
path(path, fullfile(IfrTbxCnt, 'MOVIES3D', 'MATLABLINK'));

% D�mo de segmentation.
nomDir = fullfile(IfrTbxCnt, 'sfm_chanvese_demo');
if exist(nomDir, 'dir')
    path(path, nomDir);
end

%% OpenCL

nomDir = fullfile(IfrTbxCnt, 'openclToolbox', 'matlab_src');
if exist(nomDir, 'dir')
    path(path, nomDir);
end

%% Contributions GUILayoutToolbox

% Gestion de la toolbox sous forme de r�pertoire car l'install via
% installtoolbox a vari� entre les versions R2017b et R2018b

% Contribution  Ben Tordoff : cr�ation simplifi�e d'IHM
% http://www.mathworks.com/matlabcentral/fileexchange/27758-gui-layout-toolbox
% Version R2014b pour laquelle GUILayoutToolbox est dans une version �volu�e.
contribGUILayout = fullfile(IfrTbxCnt, 'GUILayoutToolbox2.3.5');
addpath(genpath(contribGUILayout));


%% Duplication de la librairie NetCDFLib ad hoc du r�pertoire my_netcdf
% selon la version de MatLab

%{
sVer    = ver('MatLab');
release = regexprep(sVer.Release, '\(|\)', '');
if str2double(release(2:5)) >= 2021
    return
end

nomDirCdfLib = fullfile(IfrTbxCnt, '+my_netcdf', 'private');
nomNetCdfLib = ['netcdf' release '.mexw64'];
nomLibTarget = 'netcdflib.mexw64';
nomFicLib = fullfile(nomDirCdfLib, nomNetCdfLib);
if exist(nomFicLib, 'file') % = 3 for a mex file
    % Overwrite current NetCDFLib
    try
        copyfile(fullfile(nomDirCdfLib, nomNetCdfLib), fullfile(nomDirCdfLib, nomLibTarget));
    catch ME
        ME.getReport
    end

else
    msg = sprintf(['NetCDF Library %s not present. '...
                   'Please, check that you have netcdflib for this MatLab release.'], nomNetCdfLib);
    my_warndlg(msg);
    return   
end
%}
