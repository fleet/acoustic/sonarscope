% %%%%%%%%%%%%%%%% CONFIGURATION %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%opencl_include_dir = '/usr/include';
%opencl_lib_dir = '/usr/lib';

opencl_include_dir = 'C:\Program Files\NVIDIA GPU Computing SDK\common\inc';
opencl_lib_dir = 'C:\Program Files\NVIDIA GPU Computing SDK\common\lib\x64';


opencl_include_dir = 'C:\Program Files\NVIDIA GPU Computing Toolkit\CUDA\v4.0\include';
opencl_lib_dir = 'C:\Program Files\NVIDIA GPU Computing Toolkit\CUDA\v4.0\lib\x64';
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Avec debuggage

% mex('C:\SonarScopeTbx\contributions\openclToolbox/src/openclcmd.cpp', '-outdir', 'C:\SonarScopeTbx\contributions\openclToolbox/matlab_src', '-Iinclude', '-v', '-g', ['-I' opencl_include_dir], ...
%    ['-L' opencl_lib_dir], '-lOpenCL' );
%%
 mex('src/openclcmd.cpp', '-outdir', './matlab_src', '-v', '-Iinclude', ['-I' opencl_include_dir], ...
     ['-L' opencl_lib_dir], '-lOpenCL' );
