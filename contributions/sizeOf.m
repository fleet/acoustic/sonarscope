function numBytes = sizeOf(var) %#ok<INUSD>

    % Use the functional form of whos, and get the number of bytes   
    W = whos('var');
    numBytes = W.bytes;

end