Project Waterloo Utilities

To install:

[1]. As part of an existing Project Waterloo installation, just drag the "Utilities" folder to the existing "waterloo"
folder

[2]. As a new Project Waterloo installation. Inflate and place the "waterloo" folder on your MATLAB path.
Other Project Waterloo components can be added later

In either case, the Utilities will be added to the MATLAB path when you run "waterloo" at the command prompt
or from user code.

[3]. Without Waterloo. Place the Utilities folder and sub-folders on your MATLAB path.

All m-files are fully documented and a PDF in the main folder describes the code.

ML
11/2011

