% Definition des paths de la toolbox "ifremer"
%
% ifrStartup(IfrTbx)
% 
% INPUT PARAMETERS : 
%   IfrTbx : repertoire ou se trouve les developpements "Ifremer"
%
% REMARKS : Nous vous conseillons d'appeler cette fonction dans votre fonction startup.m
%           qui doit se trouver dans LATLABRXX/work dans l'environnement windows ou
%           ~/matlab sous unix (Si ce repertoire n'existe pas, creez-le
%           Creez ensuite le fichier statup.m qui doit contenir
%               TmsiAsToolbox = '/Le/Repertoire/que/nous/Vous/Indiquerons/MatlabToolboxIfremer';
%               path(path, IfrTbx);
%               ifrStartup(IfrTbx)
% 
% EXAMPLES :
%   ifrStartup(IfrTbx)
%
% SEE ALSO : ifrStartupIfremer ifrStartupContributions Authors
% AUTHORS  : JMA
%-------------------------------------------------------------------------------

function ifrStartup(IfrTbx)

% Repertoire ifremer
nomDir = fullfile(IfrTbx, 'ifremer');
if exist(nomDir, 'dir')
    path(path, nomDir);
end

% Initialisation de la toolbox "ifremer"
nomDir = fullfile(IfrTbx, 'ifremer');
if exist(nomDir, 'dir')
    path(path, nomDir);
    ifrStartupIfremer(nomDir)
end

% Repertoire mex\lib
nomDir = fullfile(IfrTbx, 'mex', 'lib');
if exist(nomDir, 'dir')
    path(path, nomDir);
end

nomDir = fullfile(IfrTbx, 'mex', 'lib', 'GLNX86', 'CIB_DOU');
if exist(nomDir, 'dir')
    path(path, nomDir);
end

% Initialisation de la toolbox "contributions"
nomDir = fullfile(IfrTbx, 'contributions');
if exist(nomDir, 'dir')
    path(path, nomDir);
    ifrStartupContributions(nomDir)
end

% Initialisation de la toolbox "contributions"
nomDir = fullfile(IfrTbx, 'Tests');
if exist(nomDir, 'dir')
    path(path, genpath(nomDir));
end

%% Installation des patch Matlab pour corriger certaines erreurs ou imperfections

% if ~isdeployed
%     checkPatchsMatLab;
% end
