% Description
%   ImportNavigation
%
% Syntax
%    [flag, nomFicNav, repImport, ImportImmersion] = SSc.Params.ImportNavigation(extNav, repImport)
%
% Intput Arguments
%   extNav    : Expected navigation file extensions 
%   repImport : The SonarScope default directory for file import
%
% Output Arguments
%   flag            : 0=KO, 1=OK
%   nomFicNav       : Navigation filename
%   repImport       : The SonarScope default directory for file import
%   ImportImmersion : 1=Import the immersion values, 0=do not
%
% Examples
%    [flag, nomFicNav, repImport, ImportImmersion] = SSc.Params.ImportNavigation(extNav, repImport)
%
% Author : JMA
% See also Authors

function [flag, nomFicNav, repImport, ImportImmersion] = ImportNavigation(extNav, repImport)

nomFicNav       = [];
ImportImmersion = 0;

%% Message d'alerte

str1 = 'TODO';
str2 = sprintf('WARNING : It is highly recommended to backup the navigation before importing a new one.\n\nYou can do it as "Survey Processings" / ".xxx files" / "Utilities" / "Manage signals" / "Save Signals as Source ones."\n\nDo you want to continue the navigation import ?');
[rep, flag] = my_questdlg(Lang(str1,str2), 'Init', 2);
if ~flag 
    return
end
if rep == 2
    flag = 0;
    return
end

%% S�lection des fichiers de navigation

strFR = 'SVP, choisissez le fichier de navigation.';
strUS = 'Please, input the navigation file.';
[flag, nomFicNav, repImport] = uiSelectFiles('Entete', Lang(strFR, strUS), 'ExtensionFiles', extNav, 'AllFilters', 1, 'RepDefaut', repImport);
if ~flag
    return
end

%% Import de l'immersion si elle existe

% Comment� car la donn�e n'est pas bonne, compar� aux valeurs d�livr�es par la PHINS

% QL = get_LevelQuestion;
% if QL == 3
    str1 = sprintf('Import de l''immersion si cette donn�e est pr�sente.\n\nAttention : il est recommande de r�pondre non pour les AUV Ifremer');
    str2 = sprintf('Import the immersion in case this data is available in the file ?\n\nWarning : it is often recommanded to answer No for Ifremer AUV');
    [rep, flag] = my_questdlg(Lang(str1,str2), 'Init', 2);
    if ~flag
        return
    end
    ImportImmersion = (rep == 1);
% end
