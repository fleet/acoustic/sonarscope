% Description
%   Import Tide
%
% Syntax
%    [flag, nomFicTide, repImport] = ImportTide(repImport)
%
% Intput Arguments
%   repImport : The SonarScope default directory for file import
%
% Output Arguments
%   flag       : 0=KO, 1=OK
%   nomFicTide : Tide file
%   repImport : The SonarScope default directory for file import
%
% Examples
%    [flag, nomFicTide, repImport] = ImportTide(repImport)
%
% Author : JMA
% See also Authors

function [flag, nomFicTide, repImport] = ImportTide(repImport)

[flag, nomFicTide, repImport] = uiSelectFiles('ExtensionFiles', {'.ttb';'.csv';'.tid';'.tide';'.txt'}, 'AllFilters', 1, ...
    'RepDefaut', repImport, 'SubTitle', 'Tide file selection (.ttb, .csv, .tid, .tide,.txt)');
if ~flag
    return
end
repImport = fileparts(nomFicTide{1});

%% Check if tide file is OK

for k=1:length(nomFicTide)
    [flag, Tide] = read_Tide(nomFicTide{k}, 'CleanData', 1); %#ok<ASGLU>
    if ~flag
        return
    end
end
