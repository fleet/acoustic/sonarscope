% Description
%   Params for SpikeRemovalJMA
%
% Syntax
%    [flag, Threshold, RestoreThreshold, Margin, OtherImages] = SSc.Params.SpikeRemovalJMA(...)
%
% Intput Arguments
%   repImport : 
%   repExport : 
%
% Output Arguments
%   flag             : 
%   Threshold        : 
%   RestoreThreshold : 
%   Margin           : 
%   OtherImages      : 
%
% Examples
%    [flag, Threshold, RestoreThreshold, Margin, OtherImages] = SSc.Params.SpikeRemovalJMA
%
% Author : JMA
% See also Authors


function [flag, Threshold, RestoreThreshold, Margin, OtherImages] = SpikeRemovalJMA(varargin)

[varargin, Unit] = getPropertyValue(varargin, 'Unit', 'm'); %#ok<ASGLU>

Threshold        = [];
RestoreThreshold = 0;
Margin           = [];
OtherImages      = [];

%% Message //Tbx

flag = messageParallelTbx(1);
if ~flag
    return
end

%% Saisie des param�tres

p    = ClParametre('Name', Lang('Seuil de r�jection', 'Spike Threshold'), ...
    'Unit', 'Sigma',  'MinValue', 0.1, 'MaxValue', 50, 'Value', 3);
p(2) = ClParametre('Name', Lang('Seuil de Rehabilitation', 'Restore Threshold'), ...
    'Unit', Unit, 'MinValue', 0,  'MaxValue', Inf, 'Value', 0);
p(3) = ClParametre('Name', Lang('Marge', 'Margin'), ...
    'Unit', 'Pixels', 'MinValue', 0,  'MaxValue', 10, 'Value', 0, 'Format', '%d');
a = StyledSimpleParametreDialog('params', p, 'Title', 'Demo of StyledSimpleParametreDialog');
a.openDialog;
flag = a.okPressedOut;
if ~flag
    return
end
value = a.getParamsValue;

Threshold        = value(1);
RestoreThreshold = value(2);
Margin           = value(3);

%% Images interm�diaires

QL = get_LevelQuestion;
if (nargout >= 5) && (QL >= 3)
    str1 = 'Vouilez-vous cr�er les images interm�diaires qui servent � calculer le masque ?';
    str2 = 'Create intermediate work images ?';
    [rep, flag] = my_questdlg(Lang(str1,str2), 'Init', 2);
    if ~flag
        return
    end
    OtherImages = (rep == 1);
end
