function [flag, nomFicSummary, repExport] = getSummaryFilename(repExport, ext)

str1 = 'IFREMER - SonarScope - Nom du fichier R�sum�';
str2 = 'IFREMER - SonarScope - Summary file name';

% Suppression du point de l'extension.
ext  = regexprep(ext, '\.', '');
[flag, nomFicSummary] = my_uiputfile('*.txt', Lang(str1,str2), fullfile(repExport, ['Summary_' upper(ext) '.csv']));
if ~flag
    return
end
repExport = fileparts(nomFicSummary);
