function PingCounter = computePingCounter(Hdf5Data)

PingCounter = Hdf5Utils.read_value(Hdf5Data.Sonar.Beam_group1.Vendor_specific.ping_raw_count);
if isfield(Hdf5Data.Sonar.Beam_group1.Vendor_specific, 'swath_along_position') % Fichiers XSF qui ne contiennent pas "swath_along_position"
    swath_along_position = Hdf5Utils.read_value(Hdf5Data.Sonar.Beam_group1.Vendor_specific.swath_along_position);
    PingCounter          = double(PingCounter) * 2 + double(swath_along_position(1,:))';
elseif isfield(Hdf5Data.Sonar.Beam_group1.Bathymetry, 'multiping_sequence') % Mail Cyrille du 02/09/2022
    swath_along_position = Hdf5Utils.read_value(Hdf5Data.Sonar.Beam_group1.Bathymetry.multiping_sequence); % TODO : OK pour EM304 mais reste à vérifier pour système dual
    PingCounter          = double(PingCounter) * 2 + double(swath_along_position);
end

PingCounter = unwrapPingCounter(PingCounter);
