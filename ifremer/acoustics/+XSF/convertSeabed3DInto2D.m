function seabedImage = convertSeabed3DInto2D(seabedImage)

% Les données sont en NPings x N Detections (Beams) x N Samples. On
% les convertit en Cell pour un traitement calqué sur le fctt VLen.
tmp = permute(seabedImage, [2 1 3]);
tmp = squeeze(num2cell(tmp,2));
seabedImage   = tmp';
seabedImage   = cellfun(@(x) x', seabedImage, 'UniformOutput', false);

% Suppression du 0 Padding (conservation des valeurs négatives).
%     XDummy = [];
[n1,n2] = size(seabedImage);
XDummy = cell(n1,n2);
for n=1:n1
    for k=1:n2
        XDummy{n,k} = seabedImage{n,k}(seabedImage{n,k} < 0);
    end
end
seabedImage = XDummy;
