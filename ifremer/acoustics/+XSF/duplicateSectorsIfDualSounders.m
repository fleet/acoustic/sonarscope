function [Signals, TransmitSectorNumberRx] = duplicateSectorsIfDualSounders(Signals, NTx, nbPings, nbRx, TransmitSectorNumberRx)

subTri = (nbRx/2+1):nbRx;

for k=1:nbPings
    switch NTx(k)
        case 1 % Single sector : 1 secteur dans EM_2040_short_spec_Feb_2012__v2.pdf, on duplique le secteur à bâbord et tribord
            Signals.TiltAngle(k,2)                = Signals.TiltAngle(k,1);
            Signals.FocusRange(k,2)               = Signals.FocusRange(k,1);
            Signals.SignalLength(k,2)             = Signals.SignalLength(k,1);
            Signals.SectorTransmitDelay(k,2)      = Signals.SectorTransmitDelay(k,1);
            Signals.CentralFrequency(k,2)         = Signals.CentralFrequency(k,1);
            Signals.MeanAbsorptionCoeff(k,2)      = Signals.MeanAbsorptionCoeff(k,1);
            Signals.SignalWaveformIdentifier(k,2) = Signals.SignalWaveformIdentifier(k,1);
            Signals.SignalBandwith(k,2)           = Signals.SignalBandwith(k,1);
            Signals.TransmitSectorNumberTx(k,2)   = Signals.TransmitSectorNumberTx(k,1) + 1;

            TransmitSectorNumberRx(k,subTri) = TransmitSectorNumberRx(k,subTri) + 1;

        case 2 % Normal mode 200 kHz : 2 secteurs dans EM_2040_short_spec_Feb_2012__v2.pdf,
            Signals.TiltAngle(k,1:4)                = Signals.TiltAngle(k,[1 2 1 2]);
            Signals.FocusRange(k,1:4)               = Signals.FocusRange(k,[1 2 1 2]);
            Signals.SignalLength(k,1:4)             = Signals.SignalLength(k,[1 2 1 2]);
            Signals.SectorTransmitDelay(k,1:4)      = Signals.SectorTransmitDelay(k,[1 2 1 2]);
            Signals.CentralFrequency(k,1:4)         = Signals.CentralFrequency(k,[1 2 1 2]);
            Signals.MeanAbsorptionCoeff(k,1:4)      = Signals.MeanAbsorptionCoeff(k,[1 2 1 2]);
            Signals.SignalWaveformIdentifier(k,1:4) = Signals.SignalWaveformIdentifier(k,[1 2 1 2]);
            Signals.SignalBandwith(k,1:4)           = Signals.SignalBandwith(k,[1 2 1 2]);
            Signals.TransmitSectorNumberTx(k,1:4)   = 1:4;

            TransmitSectorNumberRx(k,subTri) = TransmitSectorNumberRx(k,subTri) + 2; % Modif JMA le 29/02/2022 pour fichier EM2040 DualHead 200 kHz

        case 3 % Normal mode : 3 secteurs dans EM_2040_short_spec_Feb_2012__v2.pdf, on duplique le secteur central à bâbord et tribord
            Signals.TiltAngle(k,1:4)                = Signals.TiltAngle(k,[1 2 2 3]);
            Signals.FocusRange(k,1:4)               = Signals.FocusRange(k,[1 2 2 3]);
            Signals.SignalLength(k,1:4)             = Signals.SignalLength(k,[1 2 2 3]);
            Signals.SectorTransmitDelay(k,1:4)      = Signals.SectorTransmitDelay(k,[1 2 2 3]);
            Signals.CentralFrequency(k,1:4)         = Signals.CentralFrequency(k,[1 2 2 3]);
            Signals.MeanAbsorptionCoeff(k,1:4)      = Signals.MeanAbsorptionCoeff(k,[1 2 2 3]);
            if size(Signals.SignalWaveformIdentifier, 2) == 3
                Signals.SignalWaveformIdentifier(k,1:4) = Signals.SignalWaveformIdentifier(k,[1 2 2 3]);
            else % Ajout JMA pour EM2040 provenant de KMALL-XSF
                Signals.SignalWaveformIdentifier(k,1:4) = Signals.SignalWaveformIdentifier(k,[1 1 2 2]);
            end
            Signals.SignalBandwith(k,1:4)           = Signals.SignalBandwith(k,[1 2 2 3]);
            Signals.TransmitSectorNumberTx(k,3:4)   = Signals.TransmitSectorNumberTx(k,2:3) + 1;

            TransmitSectorNumberRx(k,subTri) = TransmitSectorNumberRx(k,subTri) + 1;
        otherwise
            if isnan(NTx(k))% Ajout JMA le 16/11/2023 pour DualSwath provenant des XSF
                Signals.TiltAngle(k,:)                = NaN;
                Signals.FocusRange(k,:)               = NaN;
                Signals.SignalLength(k,:)             = NaN;
                Signals.SectorTransmitDelay(k,:)      = NaN;
                Signals.CentralFrequency(k,:)         = NaN;
                Signals.MeanAbsorptionCoeff(k,:)      = NaN;
                Signals.SignalWaveformIdentifier(k,:) = NaN;
                Signals.SignalBandwith(k,:)           = NaN;
                Signals.TransmitSectorNumberTx(k,:)   = NaN;

                TransmitSectorNumberRx(k,:) = NaN;
            else
                my_breakpoint
            end
    end
end

Signals.SamplingRate = Signals.SamplingRate(:,1); % Ajout JMA pour corriger bug
