function [flag, XML, Data] = extractAttitude(Hdf5Data, varargin)

[varargin, strMRUId]             = getPropertyValue(varargin, 'SensorId',             []); %#ok<ASGLU>

%{
Variables ayant disparu dans les .kmall :
	SensorStatus
    Heave
	
Nouvelles variables :
    heading_rate: [1�1 struct]
    pitch_rate: [1�1 struct]
    roll_rate: [1�1 struct]
    Dim: [1�1 struct]
    vertical_offset: [1�1 struct]
    Vendor_specific: [1�1 struct]
%}
    
XML  = [];
Data = [];

%% Variables

% Time : OK
try
    listSensors = fieldnames(Hdf5Data.Platform.Attitude);
    nSensors = numel(listSensors);
    DataBin.Datetime    = [];
    DataBin.Heading     = [];
    DataBin.Roll        = [];
    DataBin.Pitch       = [];
    DataBin.Heave       = [];
    DataBin.SensorStatus       = [];
    DataBin.SensorSystemDescriptor      = []; 
    for k=1:nSensors
%         strMRUId = sprintf('Sensor%03d', k);
        strMRUId = listSensors{k};
        nSamples = Hdf5Utils.read_value(Hdf5Data.Platform.Attitude.(strMRUId).time);
        DataBin.Datetime    = [DataBin.Datetime;Hdf5Utils.read_value(Hdf5Data.Platform.Attitude.(strMRUId).time)];
        DataBin.Heading     = [DataBin.Heading;Hdf5Utils.read_value(Hdf5Data.Platform.Attitude.(strMRUId).heading)];
        DataBin.Roll        = [DataBin.Roll;Hdf5Utils.read_value(Hdf5Data.Platform.Attitude.(strMRUId).roll)];
        DataBin.Pitch       = [DataBin.Pitch;Hdf5Utils.read_value(Hdf5Data.Platform.Attitude.(strMRUId).pitch)];
        DataBin.Heave       = [DataBin.Heave;Hdf5Utils.read_value(Hdf5Data.Platform.Attitude.(strMRUId).vertical_offset)];
        DataBin.SensorStatus = [DataBin.SensorStatus;Hdf5Utils.read_value(Hdf5Data.Platform.Attitude.(strMRUId).Vendor_specific.kmall_sensor_status)];
        DataBin.SensorSystemDescriptor = [DataBin.SensorSystemDescriptor; ones(numel(nSamples),1)*k];
    end
catch
    flag = 0;
    return
end

% Utile par similitude avec .all
DataBin.IndexSensorSystemDescriptor = DataBin.SensorSystemDescriptor;
FieldUnits.IndexSensorSystemDescriptor = '';


DataBin.Time = cl_time('timeMat', datenum(DataBin.Datetime));
N = length(DataBin.Time);

%% Heading : OK

DataBin.Heading = single(DataBin.Heading);
% Unit = Hdf5Utils.read_unit(Hdf5Data.Platform.Attitude.Sensor000.heading)
FieldUnits.Heading = 'deg'; 
% figure; plot(DataBin.Heading, '.r'); grid on; hold on; title('Heading);

%% Roll : OK

DataBin.Roll = single(DataBin.Roll);
% Unit = Hdf5Utils.read_unit(Hdf5Data.Platform.Attitude.Sensor000.roll)
FieldUnits.Roll = 'deg';
% figure; plot(DataBin.Roll, '.r'); grid on; hold on; title('Roll);

%% Pitch : OK

DataBin.Pitch = single(DataBin.Pitch);
% Unit = Hdf5Utils.read_unit(Hdf5Data.Platform.Attitude.Sensor000.pitch)
FieldUnits.Pitch = 'deg';
% figure; plot(DataBin.Pitch, '.r'); grid on; hold on; title('Pitch);

%% Heave : valeurs diff�rentes

DataBin.Heave = single(DataBin.Heave);
% }
% DataBin.Heave = zeros([N,1], 'single');
% Unit = Hdf5Utils.read_unit(Hdf5Data.Platform.Attitude.Sensor000.vertical_offset)
FieldUnits.Heave = 'm';
% figure; plot(DataBin.Heave, '.r'); grid on; hold on; title('Heave');

%% vertical_offset

% vertical_offset % TODO GLU : Nouvelle variable diff�rente de Heave du .all
%{
DataBin.Vertical_offset = Hdf5Utils.read_value(Hdf5Data.Platform.Attitude.Sensor000.vertical_offset);
DataBin.Vertical_offset = single(DataBin.Vertical_offset);
% Unit = Hdf5Utils.read_unit(Hdf5Data.Platform.Attitude.Sensor000.vertical_offset)
FieldUnits.Vertical_offset = 'm';
% figure; plot(DataBin.Heave, '.r'); grid on; hold on; title('Heave');
%}

%% SensorStatus : Valeurs diff�rentes

DataBin.SensorStatus = uint16(DataBin.SensorStatus);
% Unit = Hdf5Utils.read_unit(Hdf5Data.Platform.Attitude.Sensor000.Vendor_specific.kmall_sensor_status)
FieldUnits.SensorStatus = '';
% figure; plot(DataBin.SensorStatus, '.r'); grid on; hold on; title('SensorStatus');

%% SensorSystemDescriptor

DataBin.SensorSystemDescriptor = uint8(DataBin.SensorSystemDescriptor);
FieldUnits.SensorSystemDescriptor = '';

%% Variable pr�d�finies par SonarScope (non pr�sentes dans les fichiers KM)

DataBin.Tide      = zeros([N,1], 'single');
DataBin.Draught   = zeros([N,1], 'single');
DataBin.TrueHeave = zeros([N,1], 'single');

FieldUnits.Tide      = 'm';
FieldUnits.Draught   = 'm';
FieldUnits.TrueHeave = 'm';

%% Cr�ation du XML

Fields = fields(DataBin);

XML.Dimensions.NbSamples = N;

XML.Title         = 'Attitude';
XML.Constructor   = 'Kongsberg';
XML.EmModel       = XSF.getEmModel(Hdf5Data);
XML.TimeOrigin    = '01/01/-4713';
XML.Comments      = 'Sensor sampling rate'; % ne rien mettre apr�s premoers tests
XML.FormatVersion = 20101118;

XML.SystemSerialNumber = Hdf5Data.Sonar.Att.sonar_serial_number;

Fields = Fields(~contains(Fields, 'Datetime')); 
for k=1:length(Fields)
    XML.Signals(k).Dimensions.Name      = 'NbSamples';
    XML.Signals(k).Dimensions.Length    = N;
    XML.Signals(k).Dimensions.Unlimited = 0;
    
    Values  = DataBin.(Fields{k});
    Storage = class(Values);
    switch Storage
        case 'cl_time'
            Storage = 'double';
            Unit    = 'days since JC';
            Values  = Values.timeMat;
        otherwise
            Unit = FieldUnits.(Fields{k});
    end
    
    XML.Signals(k).Name      = Fields{k};
    XML.Signals(k).Storage   = Storage;
    XML.Signals(k).Unit      = Unit;
    XML.Signals(k).Populated = 1;
    XML.Signals(k).Stats     = stats(Values);
end


%{
XML.Signals(3).Unit = '';
%}

FieldsData = fields(DataBin);
FieldsXML  = fields(XML);
for k=1:length(FieldsXML)
    Data.(FieldsXML{k}) = XML.(FieldsXML{k});
end

for k=1:length(FieldsData)
    Data.(FieldsData{k}) = DataBin.(FieldsData{k});
end

flag = 1;
