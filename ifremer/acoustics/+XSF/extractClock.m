function [flag, XML, Data] = extractClock(Hdf5Data)
    
XML  = [];
Data = [];

%% Variables

%% Time : OK

DataBin.Datetime = Hdf5Utils.read_value(Hdf5Data.Environment.Vendor_specific.clock.datagram_time);
DataBin.Time = cl_time('timeMat', datenum(DataBin.Datetime));
N = length(DataBin.Time);

%% Offset : TODO GLU

Offset = Hdf5Utils.read_value(Hdf5Data.Environment.Vendor_specific.clock.offset);
Offset = duration(0, 0, 0); % TODO GLU : Quelle est l'unit� de Offset ? Faut-il faire un + ou un - ?
DataBin.ExternalDatetime = DataBin.Datetime + Offset;
DataBin.ExternalTime = cl_time('timeMat', datenum(DataBin.ExternalDatetime));

%% PingCounter : Valeurs � 0 dans XSF

DataBin.PingCounter = Hdf5Utils.read_value(Hdf5Data.Environment.Vendor_specific.clock.clock_count);
DataBin.PingCounter = (0:(N-1))';
FieldUnits.PingCounter = '';

%% PPS : OK

DataBin.PPS = Hdf5Utils.read_value(Hdf5Data.Environment.Vendor_specific.clock.sensor_status);
% Unit = Hdf5Utils.read_unit(Hdf5Data.Environment.Vendor_specific.clock.sensor_status)
FieldUnits.PPS = '';

%% Cr�ation du XML

Fields = fields(DataBin);

XML.Dimensions.NbSamples = N;

XML.Title         = 'Clock';
XML.Constructor   = 'Kongsberg';
XML.EmModel       = XSF.getEmModel(Hdf5Data);
XML.TimeOrigin    = '01/01/-4713';
XML.Comments      = 'Sensor sampling rate'; % ne rien mettre apr�s premoers tests
XML.FormatVersion = 20101118;

XML.SystemSerialNumber = Hdf5Data.Sonar.Att.sonar_serial_number;

Fields = Fields(~contains(Fields, 'Datetime')); 
for k=1:length(Fields)
    XML.Signals(k).Dimensions.Name      = 'NbSamples';
    XML.Signals(k).Dimensions.Length    = N;
    XML.Signals(k).Dimensions.Unlimited = 0;
    
    Values  = DataBin.(Fields{k});
    Storage = class(Values);
    switch Storage
        case 'cl_time'
            Storage = 'double';
            Unit    = 'days since JC';
            Values  = Values.timeMat;
        otherwise
            Unit = FieldUnits.(Fields{k});
    end
    
    XML.Signals(k).Name      = Fields{k};
    XML.Signals(k).Storage   = Storage;
    XML.Signals(k).Unit      = Unit;
    XML.Signals(k).Populated = 1;
    XML.Signals(k).Stats     = stats(Values);
end

%{
XML.Signals(3).Unit = '';
%}

FieldsData = fields(DataBin);
FieldsXML  = fields(XML);
for k=1:length(FieldsXML)
    Data.(FieldsXML{k}) = XML.(FieldsXML{k});
end

for k=1:length(FieldsData)
    Data.(FieldsData{k}) = DataBin.(FieldsData{k});
end

flag = 1;
