function [flag, XML, Data] = extractDepth(Hdf5Data, DataAttitude, DataSeabedXSF, DataRawRange, varargin)
   
[varargin, BypassSeabedImage] = getPropertyValue(varargin, 'BypassSeabedImage', 0); %#ok<ASGLU> 

XML  = [];
Data = [];

%% Time et Datetime : OK

% DataBin.Datetime = Hdf5Utils.read_value(Hdf5Data.Environment.Sound_speed_profile.time);
Signals.Datetime = Hdf5Utils.read_value(Hdf5Data.Sonar.Beam_group1.ping_time);
Signals.Time = cl_time('timeMat', datenum(Signals.Datetime));
N = length(Signals.Datetime);

%% PingCounter : OK

Signals.PingCounter = XSF.computePingCounter(Hdf5Data);
% Signals.PingCounter = single(Signals.PingCounter); % ATTENTION : surtout pas de single
% Signals.PingCounter = (1:N)';
% Unit = Hdf5Utils.read_unit(Hdf5Data.Sonar.Beam_group1.Vendor_specific.ping_raw_count) % TODO GLU : pour toutes les unit�s qui sont vides, il vaudrait mieux rendre '' plut�t que []
FieldUnits.PingCounter = '';
% figure; plot(Signals.PingCounter, '.r'); grid on;

%% SystemSerialNumber

Signals.SystemSerialNumber = repmat(single(Hdf5Data.Sonar.Att.sonar_serial_number), N, 1); % TODO : trouver o� r�cup�rer cette info !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
FieldUnits.SystemSerialNumber = '';

%% Heading : OK

Signals.Heading = Hdf5Utils.read_value(Hdf5Data.Sonar.Beam_group1.platform_heading);
Signals.Heading = single(Signals.Heading);
% Unit = Hdf5Utils.read_unit(Hdf5Data.Sonar.Beam_group1.platform_heading)
FieldUnits.Heading = 'deg';
% figure; plot(Signals.Heading, '.r'); grid on;

%% SoundSpeed : OK

Signals.SoundSpeed = Hdf5Utils.read_value(Hdf5Data.Sonar.Beam_group1.sound_speed_at_transducer);
Signals.SoundSpeed = single(Signals.SoundSpeed);
% Unit = Hdf5Utils.read_unit(Hdf5Data.Sonar.Beam_group1.sound_speed_at_transducer)
FieldUnits.SoundSpeed = 'm/s';
% figure; plot(Signals.SoundSpeed, '.r'); grid on;

%% TransducerDepth : OK

Signals.TransducerDepth = Hdf5Utils.read_value(Hdf5Data.Sonar.Beam_group1.tx_transducer_depth);
Signals.TransducerDepth = single(Signals.TransducerDepth);
% Unit = Hdf5Utils.read_unit(Hdf5Data.Sonar.Beam_group1.tx_transducer_depth)
FieldUnits.TransducerDepth = 'm';
% my_figure; my_plot(Signals.TransducerDepth, '.r'); grid on;

%% MaxNbBeamsPossible : OK

Signals.MaxNbBeamsPossible = Hdf5Utils.read_value(Hdf5Data.Sonar.Beam_group1.Vendor_specific.num_sounding_max_main);
Signals.MaxNbBeamsPossible = single(Signals.MaxNbBeamsPossible');
% Unit = Hdf5Utils.read_unit(Hdf5Data.Sonar.Beam_group1.Vendor_specific.num_sounding_max_main)
FieldUnits.MaxNbBeamsPossible = '';
% figure; plot(Signals.MaxNbBeamsPossible, '.r'); grid on;

%% NbBeams : OK

Signals.NbBeams = Hdf5Utils.read_value(Hdf5Data.Sonar.Beam_group1.Vendor_specific.num_soundings_valid_main);
Signals.NbBeams = single(Signals.NbBeams');
Signals.NbBeams = sum(Signals.NbBeams,2);
% Unit = Hdf5Utils.read_unit(Hdf5Data.Sonar.Beam_group1.Vendor_specific.num_soundings_valid_main)
FieldUnits.NbBeams = '';
% figure; plot(Signals.NbBeams, '.r'); grid on;

%% SamplingRate : OK

Signals.SamplingRate = Hdf5Utils.read_value(Hdf5Data.Sonar.Beam_group1.Vendor_specific.seabed_image_sample_rate);
Signals.SamplingRate = single(Signals.SamplingRate');
% Unit = Hdf5Utils.read_unit(Hdf5Data.Sonar.Beam_group1.Vendor_specific.seabed_image_sample_rate) % TODO GLU : rend []
FieldUnits.SamplingRate = 'Hz';
% figure; plot(Signals.SamplingRate, '.r'); grid on;

%% ScanningInfo % TODO GLU

%{
Signals.ScanningInfo = Hdf5Utils.read_value(Hdf5Data.Sonar.Beam_group1.Vendor_specific.tx_xxxxxxx);
%}
Signals.ScanningInfo = zeros(N, 1, 'single');
FieldUnits.ScanningInfo = '';
% figure; plot(Signals.ScanningInfo, '.r'); grid on;

%% MaskPingsBathy

Signals.MaskPingsBathy = zeros(N, 1, 'single');
FieldUnits.MaskPingsBathy = '';
% figure; plot(Signals.MaskPingsBathy, '.r'); grid on;

%% MaskPingsReflectivity

Signals.MaskPingsReflectivity = zeros(N, 1, 'single');
FieldUnits.MaskPingsReflectivity = '';
% figure; plot(Signals.MaskPingsReflectivity, '.r'); grid on;

%% Depth : Diff�rence de 0.904 m � 0.917 m sur fichier D:\XSF\FromKmall\0039_20180905_222154_raw.xsf.nc

Images.Depth = Hdf5Utils.read_value(Hdf5Data.Sonar.Beam_group1.Bathymetry.detection_z);
%Images.Depth = -single(Images.Depth');
Images.Depth = single(Images.Depth');
water_level = 0;% ce champs est int�gr� dans platformVerticalOffset
% Pas de champ Water Level dans la version de Globe 1.20.1
% if isfield(Hdf5Data.Platform, 'water_level')
%     water_level = Hdf5Utils.read_value(Hdf5Data.Platform.water_level);
% end
% tide est la mar�e par ping Ajout ridha 17/11/23
if isfield(Hdf5Data.Sonar.Beam_group1, 'waterline_to_chart_datum')
    tide = Hdf5Utils.read_value(Hdf5Data.Sonar.Beam_group1.waterline_to_chart_datum);
end
% D�but ajout JMA le 03/03/2023 : c'est un bricolage infame. Voir ce qui est fait
% dans cl_simrad_all/read_attitude_bin
ind = (DataAttitude.SensorSystemDescriptor == 1);
Datetime = DataAttitude.Datetime(ind);
Heave    = DataAttitude.Heave(ind);
% Fin ajout JMA le 03/03/2023 
% modif ridha 17/11/23
platformVerticalOffset = Hdf5Utils.read_value(Hdf5Data.Sonar.Beam_group1.platform_vertical_offset);
Images.Depth = Images.Depth - platformVerticalOffset;
%Images.Depth = Images.Depth - tide;% � voir si en recupere la mar�e si elle existe !!! flag tide
Images.Depth = Images.Depth - Signals.TransducerDepth;% remet du depth ref Sonar
Images.Depth = -Images.Depth;

% HeaveTx = interp1(Datetime, Heave, Signals.Datetime); % Ajout JMA le 24/07/2021
% Images.Depth = Images.Depth + Signals.TransducerDepth;                          % Ajout JMA le 24/07/2021
% Images.Depth = Images.Depth - HeaveTx;                                          % Ajout JMA le 24/07/2021

% Images.Depth = Images.Depth + water_level;                                      % Ajout JMA le 24/01/2023
% WARNING : il se peut que la WCD soit d�cal�e de water_level. Dans ce cas,
% pour conserver la coh�rence avec l'ancien, il conviendrait de modifier Signals.TransducerDepth = Signals.TransducerDepth-HeaveTx

% Unit = Hdf5Utils.read_unit(Hdf5Data.Sonar.Beam_group1.Bathymetry.detection_z)
FieldUnits.Depth = 'm';
% figure; imagesc(Images.Depth); colormap(jet(256)); colorbar; title('Images.Depth')

nbBeams = size(Images.Depth, 2);

%% AcrossDist : OK

Images.AcrossDist = Hdf5Utils.read_value(Hdf5Data.Sonar.Beam_group1.Bathymetry.detection_y);
Images.AcrossDist = single(Images.AcrossDist');
% Unit = Hdf5Utils.read_unit(Hdf5Data.Sonar.Beam_group1.Bathymetry.detection_y)
FieldUnits.AcrossDist = 'm';
% figure; imagesc(Images.AcrossDist); colormap(jet(256)); colorbar; title('Images.AcrossDist')

%% AlongDist : OK

Images.AlongDist = Hdf5Utils.read_value(Hdf5Data.Sonar.Beam_group1.Bathymetry.detection_x);
Images.AlongDist = single(Images.AlongDist');
% Unit = Hdf5Utils.read_unit(Hdf5Data.Sonar.Beam_group1.Bathymetry.detection_x)
FieldUnits.AlongDist = 'm';
% figure; imagesc(Images.AlongDist); colormap(jet(256)); colorbar; title('Images.AlongDist')

%% LengthDetection : TODO GLU : je ne retrouve pas cette info alors que Sonar Record Viewer la mentionne comme "Number samples:"

Images.LengthDetection = Hdf5Utils.read_value(Hdf5Data.Sonar.Beam_group1.Bathymetry.seabed_image_center);
Images.LengthDetection = single(Images.LengthDetection');
% Unit = Hdf5Utils.read_unit(Hdf5Data.Sonar.Beam_group1.Bathymetry.seabed_image_center) % TODO GLU : devrait rendre "s" ou "sample"
FieldUnits.LengthDetection = '';
% figure; imagesc(Images.LengthDetection); colormap(jet(256)); colorbar; title('Images.LengthDetection')

%% QualityFactor % TODO GLU

%{
Images.QualityFactor = Hdf5Utils.read_value(Hdf5Data.Sonar.Beam_group1.Bathymetry.xxxx);
Images.QualityFactor = single(Images.LengthDetection');
%}
Images.QualityFactor = zeros(size(Images.Depth), 'single');
% Unit = Hdf5Utils.read_unit(Hdf5Data.Sonar.Beam_group1.Bathymetry.xxxx)
FieldUnits.QualityFactor = '';
% figure; imagesc(Images.QualityFactor); colormap(jet(256)); colorbar; title('Images.QualityFactor')

%% BeamIBA : OK mais valeurs toutes � 0

Images.BeamIBA = Hdf5Utils.read_value(Hdf5Data.Sonar.Beam_group1.Bathymetry.Vendor_specific.beam_incidence_angle_adjustment);
Images.BeamIBA = single(Images.BeamIBA');
% Unit = Hdf5Utils.read_unit(Hdf5Data.Sonar.Beam_group1.Bathymetry.Vendor_specific.beam_incidence_angle_adjustment) % TODO GLU : devrait rendre 'deg'
FieldUnits.BeamIBA = 'deg';
% figure; imagesc(Images.BeamIBA); colormap(jet(256)); colorbar; title('Images.BeamIBA')

%% DetectionInfo : TODO factoriser ce code avec extractRawRangeBeamAngle

% TODO GLU : bug Hdf5Utils.read_value sur donn�es �num�ration
%         X = Hdf5Utils.read_value(Hdf5Data.Sonar.Beam_group1.Bathymetry.detection_type);
%         Images.DetectionInfo = arrayfun(@(x) strcmp(x{:}, 'phase'), X);
Images.DetectionInfo = Hdf5Data.Sonar.Beam_group1.Bathymetry.detection_type.value;
Images.DetectionInfo = single(Images.DetectionInfo(:,:)');
% Unit = Hdf5Utils.read_unit(Hdf5Data.Sonar.Beam_group1.Bathymetry.detection_type)
FieldUnits.DetectionInfo = '';

% sub = (Images.DetectionInfo == 0);
% Images.DetectionInfo(sub)  = 16;
% Images.DetectionInfo(~sub) = 17;

% figure; imagesc(Images.DetectionInfo); colormap(jet(256)); colorbar; title('Images.DetectionInfo')

%% RealTimeCleaningInfo : TODO : quelle est la correspondance ?

Images.RealTimeCleaningInfo = zeros(size(Images.Depth), 'single');
FieldUnits.RealTimeCleaningInfo = '';
% figure; imagesc(Images.RealTimeCleaningInfo); colormap(jet(256)); colorbar; title('Images.RealTimeCleaningInfo')

%% Reflectivity : OK

Images.Reflectivity = Hdf5Utils.read_value(Hdf5Data.Sonar.Beam_group1.Bathymetry.detection_backscatter_r);
Images.Reflectivity = single(Images.Reflectivity');
% Unit = Hdf5Utils.read_unit(Hdf5Data.Sonar.Beam_group1.Bathymetry.detection_backscatter_r) % TODO GLU : devrait rendre 'dB'
FieldUnits.Reflectivity = 'dB';
% figure; imagesc(Images.Reflectivity); colormap(gray(256)); colorbar; title('Images.Reflectivity')

%% Reflectivity_1 : OK

Images.Reflectivity1 = Hdf5Utils.read_value(Hdf5Data.Sonar.Beam_group1.Bathymetry.Vendor_specific.reflectivity1);
Images.Reflectivity1 = single(Images.Reflectivity1');
% Unit = Hdf5Utils.read_unit(Hdf5Data.Sonar.Beam_group1.Bathymetry.Vendor_specific.reflectivity1) % TODO GLU : devrait rendre 'dB'
FieldUnits.Reflectivity1 = 'dB';
% figure; imagesc(Images.Reflectivity1); colormap(gray(256)); colorbar; title('Images.Reflectivity1')

%% Reflectivity_2 : OK

Images.Reflectivity2 = Hdf5Utils.read_value(Hdf5Data.Sonar.Beam_group1.Bathymetry.Vendor_specific.reflectivity2);
Images.Reflectivity2 = single(Images.Reflectivity2');
% Unit = Hdf5Utils.read_unit(Hdf5Data.Sonar.Beam_group1.Bathymetry.Vendor_specific.reflectivity2) % TODO GLU : devrait rendre 'dB'
FieldUnits.Reflectivity2 = 'dB';
% figure; imagesc(Images.Reflectivity_2); colormap(gray(256)); colorbar; title('Images.Reflectivity_2')

%% ReflectivityFromSnippets % TODO JMA OK mais diff�rence due � correction sp�culaire dans SSc

if BypassSeabedImage % On shunte la lecture des matrices 3D
    Images.ReflectivityFromSnippets = Images.Reflectivity1;
else
    if isfield(Hdf5Data.Sonar.Beam_group1.Bathymetry, 'seabed_image_samples_r')
        % Lecture des Seabed a ete faite prealablement de fa�on directe.
        X = XSF.getSeabedImageFromFile(Hdf5Data);
    else
        % Lecture des Seabed par chunk pour eviter de charger au maximum la
        % memoire.
        dsName  = '/Sonar/Beam_group1/Bathymetry/seabed_image_samples_r';
        X  = XSF.getSeabedImageFromChunk(Hdf5Data.fileName, dsName);
    end
    %         Images.ReflectivityFromSnippets  = arrayfun(@(x) mean(x{:}), X);
    Images.ReflectivityFromSnippets = cellfun(@(x) reflec_Amp2dB(mean(reflec_dB2Amp(double(x)))), X);
    %Images.ReflectivityFromSnippets = single(Images.ReflectivityFromSnippets');
    Images.ReflectivityFromSnippets = single(Images.ReflectivityFromSnippets);

    %{
    X = Hdf5Utils.read_value(Hdf5Data.Sonar.Beam_group1.Bathymetry.seabed_image_samples_r);

    swVersion   = Hdf5Data.Provenance.Att.conversion_software_version;
    globeVerRef = '2.3.3';
    if verLessThanRef(swVersion, globeVerRef)
        X = X';
    else
        % Les donn�es sont en NPings x N Detections (Beams) x N Samples. On
        % les convertit en Cell pour un traitement calqu� sur le fctt VLen.
        tmp = permute(X, [2 1 3]);
        tmp = squeeze(num2cell(tmp,2));
        X   = tmp';
        X   = cellfun(@(x) x', X, 'UniformOutput', false);

        % Suppression du 0 Padding (conservation des valeurs n�gatives).
        %     XDummy = [];
        [n1,n2] = size(X);
        XDummy = cell(n1,n2);
        for n=1:n1
            for k=1:n2
                XDummy{n,k} = X{n,k}(X{n,k} < 0);
            end
        end
        X = XDummy;
        %         Images.ReflectivityFromSnippets  = arrayfun(@(x) mean(x{:}), X);
        ImagesOld.ReflectivityFromSnippets = cellfun(@(x) reflec_Amp2dB(mean(reflec_dB2Amp(double(x)))), X);
        %Images.ReflectivityFromSnippets = single(Images.ReflectivityFromSnippets');
        ImagesOld.ReflectivityFromSnippets = single(ImagesOld.ReflectivityFromSnippets);

    end
    %}

end

% DataSeabedXSF.BSN
% DataSeabedXSF.BSO
% DataSeabedXSF.TVGN

deltaBS = DataSeabedXSF.BSN - DataSeabedXSF.BSO;
Rn = DataSeabedXSF.TVGN;
TVGCrossOver = DataSeabedXSF.TVGCrossOver;
Range = DataRawRange.TwoWayTravelTime;

% Ajout JMA le 30/09/2021 pour donn�es HROV : y a-t'il eu changement
% d'unit� dans le convertisseur XSF ?
if size(DataRawRange.SamplingRate,2) == 1
    Range = Range .* DataRawRange.SamplingRate(:);
else
    n = nbBeams / 2;
    subBab = 1:n;
    subTri = n+1:nbBeams;
    Range(:,subBab) = Range(:,subBab) .* DataRawRange.SamplingRate(:,1);
    Range(:,subTri) = Range(:,subTri) .* DataRawRange.SamplingRate(:,2);
end

for k=1:size(Images.ReflectivityFromSnippets, 1)
%     Images.ReflectivityFromSnippets(k,:) = CorSpec(Images.ReflectivityFromSnippets(k,:), deltaBS(k,:), Range(k,:), Rn(k,:), TVGCrossOver(k,:), false);
    Images.ReflectivityFromSnippets(k,:) = CorSpec(Images.ReflectivityFromSnippets(k,:), deltaBS(k,:), Range(k,:), Rn(k,:), TVGCrossOver(k,:), true); % Modif JMA le 25/07/2021 car Rn pas bien estim� dans extractSeabedImage
end

% Unit = Hdf5Utils.read_unit(Hdf5Data.Sonar.Beam_group1.Bathymetry.detection_backscatter_r) % TODO GLU : devrait rendre dB
FieldUnits.ReflectivityFromSnippets = 'dB';
% figure; imagesc(Images.ReflectivityFromSnippets); colormap(gray(256)); colorbar; title('Images.ReflectivityFromSnippets')

%% ReflectivityFromSnippetsWithoutSpecularRestablishment : TODO JMA

Images.ReflectivityFromSnippetsWithoutSpecularRestablishment = Images.Reflectivity;
FieldUnits.ReflectivityFromSnippetsWithoutSpecularRestablishment = 'dB';
% figure; imagesc(Images.ReflectivityFromSnippetsWithoutSpecularRestablishment); colormap(gray(256)); colorbar; title('Images.ReflectivityFromSnippetsWithoutSpecularRestablishment')

%% Mask : OK

Images.Mask = Hdf5Utils.read_value(Hdf5Data.Sonar.Beam_group1.Bathymetry.status);
Images.Mask = single(Images.Mask');
% Unit = Hdf5Utils.read_unit(Hdf5Data.Sonar.Beam_group1.Bathymetry.detection_backscatter_r) % TODO GLU : devrait rendre 'dB'
FieldUnits.Mask = '';
% figure; imagesc(Images.Mask); colormap(jet(256)); colorbar; title('Images.Mask')

%% AbsorptionCoefficientRT % TODO JMA 

% Hdf5Data.Sonar.Beam_group1.Bathymetry.detection_mean_absorption_coefficient
Images.AbsorptionCoefficientRT = Hdf5Utils.read_value(Hdf5Data.Sonar.Beam_group1.Bathymetry.detection_mean_absorption_coefficient);
Images.AbsorptionCoefficientRT = single(Images.AbsorptionCoefficientRT');
% Images.AbsorptionCoefficientRT = zeros(size(Images.Depth), 'single');
FieldUnits.AbsorptionCoefficientRT = 'dB/km';
% figure; imagesc(Images.AbsorptionCoefficientRT); colormap(gray(256)); colorbar; title('Images.AbsorptionCoefficientRT')

%% AbsorptionCoefficientSSc % TODO JMA 

Images.AbsorptionCoefficientSSc = zeros(size(Images.Depth), 'single');
FieldUnits.AbsorptionCoefficientSSc = 'dB/km';
% figure; imagesc(Images.AbsorptionCoefficientSSc); colormap(gray(256)); colorbar; title('Images.AbsorptionCoefficientSSc')

%% AbsorptionCoefficientUser % TODO JMA 

Images.AbsorptionCoefficientUser = NaN(size(Images.Depth), 'single'); % Modif JMA le 14/02/2024 : NaN au lieu de zeros
FieldUnits.AbsorptionCoefficientUser = 'dB/km';
% figure; imagesc(Images.AbsorptionCoefficientUser); colormap(gray(256)); colorbar; title('Images.AbsorptionCoefficientUser')

%% IncidenceAngle % TODO JMA 

Images.IncidenceAngle = zeros(size(Images.Depth), 'single');
FieldUnits.IncidenceAngle = 'deg';
% figure; imagesc(Images.IncidenceAngle); colormap(gray(256)); colorbar; title('Images.IncidenceAngle')

%% ReflectivityBestBS % TODO JMA 

Images.ReflectivityBestBS = zeros(size(Images.Depth), 'single');
FieldUnits.ReflectivityBestBS = 'dB';

%% SlopeAcross % TODO JMA 

Images.SlopeAcross = zeros(size(Images.Depth), 'single');
FieldUnits.SlopeAcross = 'deg';

%% SlopeAlong % TODO JMA 

Images.SlopeAlong = zeros(size(Images.Depth), 'single');
FieldUnits.SlopeAlong = 'deg';

%% BathymetryFromDTM % TODO JMA 

Images.BathymetryFromDTM = zeros(size(Images.Depth), 'single');
FieldUnits.BathymetryFromDTM = 'm';

%% InsonifiedAreaSSc % TODO JMA 

Images.InsonifiedAreaSSc = zeros(size(Images.Depth), 'single');
FieldUnits.InsonifiedAreaSSc = 'db/m2';

%% WCSignalWidth

Images.WCSignalWidth = zeros(size(Images.Depth), 'single');
FieldUnits.WCSignalWidth = 'Hz'; % TODO : Hz ou kHz ?

%% Mask

% Images.Mask = zeros(size(Images.Depth), 'single');
FieldUnits.Mask = '';

%% MaskBackup

Images.MaskBackup = zeros(size(Images.Depth), 'single');
FieldUnits.MaskBackup = '';

%% Cr�ation du XML

XML.Title         = 'Depth';
XML.Constructor   = 'Kongsberg';
XML.EmModel       = XSF.getEmModel(Hdf5Data);
XML.TimeOrigin    = '01/01/-4713';
XML.Comments      = 'Sounder ping rate';
XML.Version       = 'V2';
XML.FormatVersion = 20101118;
XML.IdentAlgoSnippets2OneValuePerBeam = 2;

XML.Dimensions.nbPings    = N;
XML.Dimensions.nbBeams    = nbBeams;
XML.Dimensions.nbSounders = size(Signals.SamplingRate,2);

% Dans datagramme Installation SN=10001 ?
XML.ListeSystemSerialNumber = Hdf5Data.Sonar.Att.sonar_serial_number;

Fields = fields(Signals);
Fields = Fields(~contains(Fields, 'Datetime')); 
for k=1:length(Fields)
    XML.Signals(k).Dimensions.Name      = 'nbPings';
    XML.Signals(k).Dimensions.Length    = N;
    XML.Signals(k).Dimensions.Unlimited = 0;
    
    Values  = Signals.(Fields{k});
    Storage = class(Values);
    switch Storage
        case 'cl_time'
            Storage = 'double';
            Unit    = 'days since JC';
            Values  = Values.timeMat;
        otherwise
            Unit = FieldUnits.(Fields{k});
    end
    
    if size(Values, 2) == XML.Dimensions.nbSounders
        XML.Signals(k).Dimensions(2).Name      = 'nbSounders';
        XML.Signals(k).Dimensions(2).Length    = XML.Dimensions.nbSounders;
        XML.Signals(k).Dimensions(2).Unlimited = 0;
    end
    
    XML.Signals(k).Name      = Fields{k};
    XML.Signals(k).Storage   = Storage;
    XML.Signals(k).Unit      = Unit;
    XML.Signals(k).Populated = 1;
    XML.Signals(k).Stats     = stats(Values);
end


Fields = fields(Images);
for k=1:length(Fields)
    XML.Images(k).Dimensions.Name      = 'nbPings';
    XML.Images(k).Dimensions.Length    = N;
    XML.Images(k).Dimensions.Unlimited = 0;
    
    XML.Images(k).Dimensions(2).Name      = 'nbBeams';
    XML.Images(k).Dimensions(2).Length    = nbBeams;
    XML.Images(k).Dimensions(2).Unlimited = 0;
    
    Values  = Images.(Fields{k});
    Storage = class(Values);
    switch Storage
        case 'cl_time'
            Storage = 'double';
            Unit    = 'days since JC';
            Values  = Values.timeMat;
        case 'datetime'
            continue
        otherwise
            Unit = FieldUnits.(Fields{k});
    end
    
    XML.Images(k).Name      = Fields{k};
    XML.Images(k).Storage   = Storage;
    XML.Images(k).Unit      = Unit;
    XML.Images(k).Populated = 1;
    XML.Images(k).Stats     = stats(Values);
    
    %{
    try
        for k2=1:length(DataPosition.Images)
            if strcmp(XML.Images(k2).Name, DataPosition.Images(k2).Name)
                break
            end
        end
        fprintf('XML.Images(%d).Unit = ''%s'';\n', k2, DataPosition.Images(k2).Unit);
    catch
        XML.Images(k).Name;
    end
    %}
end

FieldsXML  = fields(XML);
for k=1:length(FieldsXML)
    Data.(FieldsXML{k}) = XML.(FieldsXML{k});
end

FieldsData = fields(Signals);
for k=1:length(FieldsData)
    Data.(FieldsData{k}) = Signals.(FieldsData{k});
end

FieldsData = fields(Images);
for k=1:length(FieldsData)
    Data.(FieldsData{k}) = Images.(FieldsData{k});
end

% DataPosition
% Data

flag = 1;
