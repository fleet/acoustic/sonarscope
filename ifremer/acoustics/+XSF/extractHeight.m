function [flag, XML, Data] = extractHeight(Hdf5Data, varargin)

[varargin, DataHeight] = getPropertyValue(varargin, 'DataHeightControle', []); %#ok<ASGLU>

%{
Variables ayant disparu dans les .kmall :
	xxx
	
Nouvelles variables :
    xxxx
%}

%{
DataHeight = 
                 Title: 'Height'
           Constructor: 'Kongsberg'
               EmModel: 304
    SystemSerialNumber: 160
             NbSamples: 528
            TimeOrigin: '01/01/-4713'
              Comments: 'Sensor sampling rate'
         FormatVersion: 20101118
            Dimensions: [1�1 struct]
               Signals: [1�5 struct]
                  Time: [1�1 cl_time]
              Datetime: [528�1 datetime]
           PingCounter: [528�1 uint16]
                Height: [528�1 single]
            HeightType: [528�1 uint8]
                  Tide: [528�1 single]
%}
    
XML  = [];
Data = [];

%% Variables

% Time : OK
% DataHeight.Datetime
%         DataBin.Datetime = Hdf5Data.Platform.Vendor_specific.runtime.time.value;
DataBin.Datetime = Hdf5Utils.read_value(Hdf5Data.Platform.Attitude.Sensor000.time);
DataBin.Time = cl_time('timeMat', datenum(DataBin.Datetime));

%% Heading

% Heading : OK
% DataHeight.Heading
DataBin.Heading = Hdf5Utils.read_value(Hdf5Data.Platform.Attitude.Sensor000.heading);
DataBin.Heading = single(DataBin.Heading);
FieldUnits.Heading = 'deg';

%% Roll

% Roll : OK
% DataHeight.Roll
DataBin.Roll = Hdf5Utils.read_value(Hdf5Data.Platform.Attitude.Sensor000.roll);
DataBin.Roll = single(DataBin.Roll);
FieldUnits.Roll = 'deg';

%% Pitch

% Pitch : OK
% DataHeight.Pitch
DataBin.Pitch = Hdf5Utils.read_value(Hdf5Data.Platform.Attitude.Sensor000.pitch);
DataBin.Pitch = single(DataBin.Pitch);
FieldUnits.Pitch = 'deg';

%% Heave

% Heave % KO TODO GLU : heave est bien pr�sent pourtant dans .kmall (visible dans
% SonarRecord Viewer)

%{
% DataHeight.Heave
DataBin.Heave = Hdf5Utils.read_value(Hdf5Data.Platform.Attitude.Sensor000.Heave);
DataBin.Heave = single(DataBin.Heave);
%}
N = length(DataBin.Roll);
DataBin.Heave = zeros([N,1], 'single');
FieldUnits.Heave = 'm';

%% vertical_offset

% vertical_offset % TODO GLU :Nopuvelle variable diff�rente de Heave du .all
%{
% DataHeight.Heave
DataBin.Vertical_offset = Hdf5Utils.read_value(Hdf5Data.Platform.Attitude.Sensor000.vertical_offset);
DataBin.Vertical_offset = single(DataBin.Vertical_offset);
FieldUnits.Vertical_offset = 'm';
%}

%% SensorStatus

% TODO GLU
DataBin.SensorStatus = zeros([N,1], 'uint16');
FieldUnits.SensorStatus = '';

%% Variable pr�d�finies par SonarScope (non pr�sentes dans les fichiers KM)

DataBin.Tide      = zeros([N,1], 'single');
DataBin.Draught   = zeros([N,1], 'single');
DataBin.TrueHeave = zeros([N,1], 'single');

FieldUnits.Tide      = 'm';
FieldUnits.Draught   = 'm';
FieldUnits.TrueHeave = 'm';

%% Cr�ation du XML

Fields = fields(DataBin);

XML.Dimensions.NbSamples = N;

XML.Title         = 'Attitude';
XML.Constructor   = 'Kongsberg';
XML.EmModel       = XSF.getEmModel(Hdf5Data);
XML.TimeOrigin    = '01/01/-4713';
XML.Comments      = 'Sensor sampling rate'; % ne rien mettre apr�s premoers tests
XML.FormatVersion = 20101118;

XML.SystemSerialNumber = Hdf5Data.Sonar.Att.sonar_serial_number;

Fields = Fields(~contains(Fields, 'Datetime')); 
for k=1:length(Fields)
    XML.Signals(k).Dimensions.Name      = 'NbSamples';
    XML.Signals(k).Dimensions.Length    = N;
    XML.Signals(k).Dimensions.Unlimited = 0;
    
    Values  = DataBin.(Fields{k});
    Storage = class(Values);
    switch Storage
        case 'cl_time'
            Storage = 'double';
            Unit    = 'days since JC';
            Values  = Values.timeMat;
        otherwise
            Unit = FieldUnits.(Fields{k});
    end
    
    XML.Signals(k).Name      = Fields{k};
    XML.Signals(k).Storage   = Storage;
    XML.Signals(k).Unit      = Unit;
    XML.Signals(k).Populated = 1;
    XML.Signals(k).Stats     = stats(Values);
    
    %{
    try
        for k2=1:length(DataHeight.Signals)
            if strcmp(XML.Signals(k2).Name, DataHeight.Signals(k2).Name)
                break
            end
        end
        fprintf('XML.Signals(%d).Unit = ''%s'';\n', k2, DataHeight.Signals(k2).Unit);
    catch
        XML.Signals(k).Name;
    end
    %}
end

%{
XML.Signals(3).Unit = '';
%}

FieldsData = fields(DataBin);
FieldsXML  = fields(XML);
for k=1:length(FieldsXML)
    Data.(FieldsXML{k}) = XML.(FieldsXML{k});
end

for k=1:length(FieldsData)
    Data.(FieldsData{k}) = DataBin.(FieldsData{k});
end

flag = 1;

% DataHeight
% Data
