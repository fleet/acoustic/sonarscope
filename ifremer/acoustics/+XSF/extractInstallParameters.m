function [flag, DataInstallParams] = extractInstallParameters(Hdf5Data)

% flag              = 0;
% DataInstallParams = [];

%% Time et Datetime % TODO GLU : on perd l'heure des datagrammes dans le XSF. A défaut on prend la première heur des datagrammes runtime

DataInstallParams.Datetime = Hdf5Data.Platform.Vendor_specific.runtime.time.value(1);
DataInstallParams.Time     = cl_time('timeMat', datenum(DataInstallParams.Datetime));

%% Section installationparameters

str = Hdf5Data.Platform.Vendor_specific.Att.installation_raw_data_kmall;
mots = strsplit(str, ',');

%% TRAI_TX1

% k = find(contains(mots, 'TRAI_TX1') | contains(mots, 'TRAI_HD1'));
k = find(contains(mots, 'TRAI_TX1'));
if ~isempty(k)
    TRAI_TX1 = mots{k};
    strParams = strsplit(TRAI_TX1, {';'; ':'});
    mots(k) = [];

    ind = contains(strParams, 'N=');
    DataInstallParams.S1N = str2double(strParams{ind}(3:end));

    ind = find(contains(strParams, 'X=')); % Ajout find par JMA le 26/10/2021 pour EM2040 DualRx du Belgica
    DataInstallParams.S1X = str2double(strParams{ind(1)}(3:end)); % Ajout ind(1) par JMA le 26/10/2021 pour EM2040 DualRx du Belgica

    ind = find(contains(strParams, 'Y=')); % Même type de correctif pour tout ce qui suit
    DataInstallParams.S1Y = str2double(strParams{ind(1)}(3:end));

    ind = find(contains(strParams, 'Z='));
    DataInstallParams.S1Z = str2double(strParams{ind(1)}(3:end));

    ind = find(contains(strParams, 'R='));
    DataInstallParams.S1R = str2double(strParams{ind(1)}(3:end));

    ind = find(contains(strParams, 'P='));
    DataInstallParams.S1P = str2double(strParams{ind(1)}(3:end));

    ind = contains(strParams, 'H=');
    DataInstallParams.S1H = str2double(strParams{ind}(3:end));

    ind = contains(strParams, 'S=');
    X = str2double(strParams{ind}(3:end));
    DataInstallParams.S1S = find(X == [0.5 1 2]); % (0=0.5º,1=1º,2=2º)
end

%% TRAI_RX1

k = find(contains(mots, 'TRAI_RX1') | contains(mots, 'TRAI_HD1'));
if ~isempty(k)
    TRAI_RX1 = mots{k};
    strParams = strsplit(TRAI_RX1, {';'; ':'});
    mots(k) = [];
    
    ind = contains(strParams, 'N=');
    DataInstallParams.S2N = str2double(strParams{ind}(3:end));
    
    ind = find(contains(strParams, 'X=')); % Ajout find par JMA le 26/10/2021 pour EM2040 DualRx du Belgica
    DataInstallParams.S2X = str2double(strParams{ind(1)}(3:end)); % Ajout ind(1) par JMA le 26/10/2021 pour EM2040 DualRx du Belgica
    
    ind = find(contains(strParams, 'Y=')); % Même type de correctif pour tout ce qui suit
    DataInstallParams.S2Y = str2double(strParams{ind(1)}(3:end));
    
    ind = find(contains(strParams, 'Z='));
    DataInstallParams.S2Z = str2double(strParams{ind(1)}(3:end));
    
    ind = contains(strParams, 'R=');
    DataInstallParams.S2R = str2double(strParams{ind}(3:end));
    
    ind = contains(strParams, 'P=');
    DataInstallParams.S2P = str2double(strParams{ind}(3:end));
    
    ind = contains(strParams, 'H=');
    DataInstallParams.S2H = str2double(strParams{ind}(3:end));
    
    ind = find(contains(strParams, 'G='));
    if ~isempty(ind)
        DataInstallParams.GO2 = str2double(strParams{ind}(3:end));
    end
    
    ind = find(contains(strParams, 'S='));
    if ~isempty(ind)
        DataInstallParams.S2S = str2double(strParams{ind}(3:end)); % Différent de .all et SonarRecord Viewer
    end
    % X = str2double(strParams{ind}(3:end));
    % DataInstallParams.S2S = find(X == [0.5 1 2]); % (0=0.5º,1=1º,2=2º)
end

%% TRAI_RX2

% k = find(contains(mots, 'TRAI_RX2'));
k = find(contains(mots, 'TRAI_RX2') | contains(mots, 'TRAI_HD2'));
if ~isempty(k)
    TRAI_RX2 = mots{k};
    strParams = strsplit(TRAI_RX2, {';'; ':'});
    mots(k) = [];
    
    ind = contains(strParams, 'N=');
    DataInstallParams.S3N = str2double(strParams{ind}(3:end));
    
    ind = find(contains(strParams, 'X=')); % Ajout find par JMA le 26/10/2021 pour EM2040 DualRx du Belgica
    DataInstallParams.S3X = str2double(strParams{ind(1)}(3:end)); % Ajout ind(1) par JMA le 26/10/2021 pour EM2040 DualRx du Belgica
    
    ind = find(contains(strParams, 'Y=')); % Même type de correctif pour tout ce qui suit
    DataInstallParams.S3Y = str2double(strParams{ind(1)}(3:end));
    
    ind = find(contains(strParams, 'Z='));
    DataInstallParams.S3Z = str2double(strParams{ind(1)}(3:end));
    
    ind = contains(strParams, 'R=');
    DataInstallParams.S3R = str2double(strParams{ind}(3:end));
    
    ind = contains(strParams, 'P=');
    DataInstallParams.S3P = str2double(strParams{ind}(3:end));
    
    ind = contains(strParams, 'H=');
    DataInstallParams.S3H = str2double(strParams{ind}(3:end));
    
    ind = find(contains(strParams, 'G='));
    if ~isempty(ind)
        DataInstallParams.GO3 = str2double(strParams{ind(1)}(3:end));
    end
    
    ind = find(contains(strParams, 'S='));
    if ~isempty(ind)
        DataInstallParams.S2S = str2double(strParams{ind}(3:end)); % Différent de .all et SonarRecord Viewer
    end
    % X = str2double(strParams{ind}(3:end));
    % DataInstallParams.S2S = find(X == [0.5 1 2]); % (0=0.5º,1=1º,2=2º)
end

%% POSI_1

k = contains(mots, 'POSI_1');
POSI_1 = mots{k};
strParams = strsplit(POSI_1, {';'; ':'});
mots(k) = [];

ind = contains(strParams, 'X=');
DataInstallParams.P1X = str2double(strParams{ind}(3:end));

ind = contains(strParams, 'Y=');
DataInstallParams.P1Y = str2double(strParams{ind}(3:end));

ind = contains(strParams, 'Z=');
DataInstallParams.P1Z = str2double(strParams{ind}(3:end));

ind = contains(strParams, 'D=');
DataInstallParams.P1D = str2double(strParams{ind}(3:end));

ind = contains(strParams, 'G=');
DataInstallParams.P1G = strParams{ind}(3:end);

ind = contains(strParams, 'T=');
DataInstallParams.P1T = strParams{ind}(3:end);

ind = contains(strParams, 'C=');
DataInstallParams.P1C = strParams{ind}(3:end);

ind = contains(strParams, 'F=');
DataInstallParams.P1F = strParams{ind}(3:end);

ind = contains(strParams, 'Q=');
DataInstallParams.P1Q = strParams{ind}(3:end);

ind = contains(strParams, 'I=');
DataInstallParams.P1I = strParams{ind}(3:end);

ind = contains(strParams, 'U=');
DataInstallParams.P1U = strParams{ind}(3:end);

%% POSI_2

k = contains(mots, 'POSI_2');
POSI_2 = mots{k};
strParams = strsplit(POSI_2, {';'; ':'});
mots(k) = [];

% ind = contains(strParams, 'N=');
% DataInstallParams.S2N = str2double(strParams{ind}(3:end));

%% POSI_3

k = contains(mots, 'POSI_3');
POSI_3 = mots{k};
strParams = strsplit(POSI_3, {';'; ':'});
mots(k) = [];

% ind = contains(strParams, 'N=');
% DataInstallParams.S2N = str2double(strParams{ind}(3:end));

%% ATTI_1

k = contains(mots, 'ATTI_1');
ATTI_1 = mots{k};
strParams = strsplit(ATTI_1, {';'; ':'});
mots(k) = [];

ind = contains(strParams, 'X=');
DataInstallParams.MSX = str2double(strParams{ind}(3:end));

ind = contains(strParams, 'Y=');
DataInstallParams.MSY = str2double(strParams{ind}(3:end));

ind = contains(strParams, 'Z=');
DataInstallParams.MSZ = str2double(strParams{ind}(3:end));

ind = contains(strParams, 'R=');
DataInstallParams.MSR = str2double(strParams{ind}(3:end));

ind = contains(strParams, 'P=');
DataInstallParams.MSP = str2double(strParams{ind}(3:end));

ind = contains(strParams, 'D=');
DataInstallParams.MSD = str2double(strParams{ind}(3:end)) * 1000;

ind = contains(strParams, 'H=');
DataInstallParams.MSH = str2double(strParams{ind}(3:end));

ind = contains(strParams, 'U=');
DataInstallParams.MSU = strParams{ind}(3:end);

ind = contains(strParams, 'I=');
DataInstallParams.MSI = strParams{ind}(3:end);

ind = contains(strParams, 'F=');
DataInstallParams.MSF = strParams{ind}(3:end);

ind = contains(strParams, 'M=');
DataInstallParams.MSM = strParams{ind}(3:end);

%% ATTI_2

k = contains(mots, 'ATTI_2');
ATTI_2 = mots{k};
strParams = strsplit(ATTI_2, {';'; ':'});
mots(k) = [];

ind = find(contains(strParams, 'U=NOT_SET'), 1);
if isempty(ind)
    ind = contains(strParams, 'X=');
    DataInstallParams.NSX = str2double(strParams{ind}(3:end));
    
    ind = contains(strParams, 'Y=');
    DataInstallParams.NSY = str2double(strParams{ind}(3:end));
    
    ind = contains(strParams, 'Z=');
    DataInstallParams.NSZ = str2double(strParams{ind}(3:end));
    
    ind = contains(strParams, 'R=');
    DataInstallParams.NSR = str2double(strParams{ind}(3:end));
    
    ind = contains(strParams, 'P=');
    DataInstallParams.NSP = str2double(strParams{ind}(3:end));
    
    ind = contains(strParams, 'D=');
    DataInstallParams.NSD = str2double(strParams{ind}(3:end)) * 1000;
    
    ind = contains(strParams, 'H=');
    DataInstallParams.NSH = str2double(strParams{ind}(3:end));
    
    ind = contains(strParams, 'U=');
    DataInstallParams.NSU = strParams{ind}(3:end);
    
    ind = contains(strParams, 'I=');
    DataInstallParams.NSI = strParams{ind}(3:end);
    
    ind = contains(strParams, 'F=');
    DataInstallParams.NSF = strParams{ind}(3:end);
    
    ind = contains(strParams, 'M=');
    DataInstallParams.NSM = strParams{ind}(3:end);
end

%% CLCK

k = contains(mots, 'CLCK');
CLCK = mots{k};
strParams = strsplit(CLCK, {';'; ':'});
mots(k) = [];

% ind = contains(strParams, 'X=');
% DataInstallParams.NSX = str2double(strParams{ind}(3:end));

%{
{'CLCK'}    {'F=ZDA'}    {'S=CLK'}    {'A=On rising edge'}    {'I=Serial port 3'}    {'Q=OK'}
CLCK:F=ZDA
S=CLK
CLS  =  0,
CLO  =  0,
%}

%% EMXI

k = contains(mots, 'EMXI');
EMXI = mots{k};
strParams = strsplit(EMXI, {';'; ':'});
mots(k) = [];

ind = contains(strParams, 'SWLZ=');
DataInstallParams.WLZ = str2double(strParams{ind}(6:end));

%% CPU

k = contains(mots, 'CPU:');
CPU = mots{k};
DataInstallParams.PSV = CPU(5:end);
mots(k) = [];

%% CBMF

k = contains(mots, 'CBMF:');
CBMF = mots{k};
DataInstallParams.BSV = CBMF(6:end);
mots(k) = [];

%% RSV

k = find(contains(mots, 'RX:'));
RX = mots{k(1)};
DataInstallParams.RSV = RX(5:end);

%% Ouverture angulaire en Rx

k = find(contains(mots, 'RX:'));
RX = mots{k(2)};
DataInstallParams.RxAcrossBeamWidthDeg = str2double(RX(7:end-3)); % Creation nouvelle variable
mots(k) = [];

%% Ouverture angulaire en Tx

k = find(contains(mots, 'TX:'));
TX = mots{k(2)};
strParams = strsplit(TX, {';'; ':'});
DataInstallParams.TxAlongBeamWidthDeg = str2double(strParams{end}(1:end-3)); % Creation nouvelle variable
mots(k) = [];

%% EMXV

k = contains(mots, 'EMXV:');
EMXV = mots{k};
if isequal(EMXV(8:end), '2040C')
    DataInstallParams.EmModel = 2045;
else
    DataInstallParams.EmModel = str2double(EMXV(8:end));
end
mots(k) = [];

%% SMH

% k = contains(mots, 'SMH');
% EMXV = mots{k};
% DataInstallParams.EmModel = str2double(EMXV(8:end));

%% DPHI

%% SVPI

%% SN

k = contains(mots, 'SN');
SN = mots{k};
DataInstallParams.SN = str2double(SN(5:end));
mots(k) = []; %#ok<NASGU>

% TODO : SMH et S1S = 320 sur SonarRecord Viewer du .all convertit, 10001 sur XSF !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

%{
mots

mots =

  1×13 cell array

  Columns 1 through 6

    {'OSCV:Empty'}    {'↵PU_0'}    {'↵IP=157.237.14…'}    {'↵UDP=1997'}    {'↵TYPE=CON_TECH…'}    {'↵VERSIONS:'}

  Columns 7 through 11

    {'↵VXW:6.9 SMP J…'}    {'↵FILTER:1.1.0 …'}    {'↵VERSIONS-END'}    {'↵SERIALno-END'}    {'DPHI:U=NOT_SET'}

  Columns 12 through 13

    {'SVPI:U=NOT_SET'}    {0×0 char}    
%}

% DataInstallParams

% Ex de caracterisation d'une tête Dual.
% SYSTEM:EM 2040 - Single Tx dual Rx 
% rmd GLU : voir aussi dans la liste des TX/RX comprise entre les mots-cle SERIALno et SERIALno-End
DataInstallParams.isDual        = double(~isempty(regexp(str, 'Dual', 'ignorecase')));

DataInstallParams.Title         = 'InstallationParameters';
DataInstallParams.Constructor   = 'Kongsberg';
DataInstallParams.TimeOrigin    = '01/01/-4713';
DataInstallParams.Comments      = 'One per file';
DataInstallParams.FormatVersion = 20101118;

DataInstallParams.SystemSerialNumber = Hdf5Data.Sonar.Att.sonar_serial_number;
% DataInstallParams.SystemSerialNumberSecondHead: 0

DataInstallParams.Dimensions.NbDatagrams = 1; % 2 dans .all !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

% DataInstallParams.Time: [1×1 cl_time]
% DataInstallParams.Datetime: [2×1 datetime]
% DataInstallParams.SurveyLineNumber: [2×1 single]

% GLU : comment fait-on pour récupérer l'heure et SurveyLineNumber à partir du XSF !!!!!!!!!!!!!!!!!!!!!!!!!!!!!


% Head1.SystemSerialNumber          
% Head1.SystemSerialNumberSecondHead 
    
flag = 1;
