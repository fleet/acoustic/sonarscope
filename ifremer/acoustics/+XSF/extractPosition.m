function [flag, XML, Data, XMLHeight, DataHeight] = extractPosition(Hdf5Data, varargin)

[varargin, strPositionId]   = getPropertyValue(varargin, 'SensorId', []); %#ok<ASGLU>

%{
Variables ayant disparu dans les .kmall :
                      PingCounter: [528�1 uint16]
                         Quality: [528�1 single]
                     CourseVessel: [528�1 single]
               PositionDescriptor: [528�1 uint8]	

Nouvelles variables :
    []
%}
    
XML        = [];
Data       = [];
XMLHeight  = [];
DataHeight = [];

%% Variables
% Time : OK
% DataAttitudeControle.Datetime
try
    sensorNames = fieldnames(Hdf5Data.Platform.Position);
    nSensors = numel(sensorNames);
    DataBin.Datetime           = [];
    DataBin.Latitude           = [];
    DataBin.Longitude          = [];
    DataBin.Heading            = [];
    DataBin.CourseVessel       = [];
    DataBin.AltitudeOfIMU      = [];
    DataBin.Height             = [];
    DataBin.Speed              = [];
    DataBin.PositionDescriptor = [];
    NMEA = {};
    for k=1:nSensors
%       strPositionId = sprintf('Sensor%03d', k);
        strPositionId = sensorNames{k};
        nSamples      = numel(Hdf5Utils.read_value(Hdf5Data.Platform.Position.(strPositionId).time));
        DataBin.Datetime            = [DataBin.Datetime;Hdf5Utils.read_value(Hdf5Data.Platform.Position.(strPositionId).time)];
        DataBin.Latitude           = [DataBin.Latitude;Hdf5Utils.read_value(Hdf5Data.Platform.Position.(strPositionId).latitude)];
        DataBin.Longitude          = [DataBin.Longitude;Hdf5Utils.read_value(Hdf5Data.Platform.Position.(strPositionId).longitude)];
        DataBin.Heading            = [DataBin.Heading;Hdf5Utils.read_value(Hdf5Data.Platform.Position.(strPositionId).course_over_ground)];
        DataBin.CourseVessel       = [DataBin.CourseVessel;calCapFromLatLon(DataBin.Latitude, DataBin.Longitude)];
%       DataBin.AltitudeOfIMU      = [DataBin.AltitudeOfIMU;Hdf5Utils.read_value(Hdf5Data.Platform.Position.(strPositionId).height_above_reference_ellipsoid)];
        DataBin.Height             = [DataBin.Height; Hdf5Utils.read_value(Hdf5Data.Platform.Position.(strPositionId).height_above_reference_ellipsoid)];
        DataBin.PositionDescriptor = [DataBin.PositionDescriptor; ones(nSamples, 1, 'uint8')*k];
        DataBin.Speed              = [DataBin.Speed; Hdf5Utils.read_value(Hdf5Data.Platform.Position.(strPositionId).speed_over_ground)];
        NMEA = [NMEA;Hdf5Utils.read_value(Hdf5Data.Platform.Position.(strPositionId).Vendor_specific.data_received_from_sensor)]; %#ok<AGROW> 
    end
catch
    flag = 0;
    return
end

%% Time : OK

DataBin.Time = cl_time('timeMat', datenum(DataBin.Datetime));
N = length(DataBin.Time);

%% Latitude : OK

% Unit = Hdf5Utils.read_unit(Hdf5Data.Platform.Position.(strPositionId).latitude)
FieldUnits.Latitude = 'deg';
% figure; plot(DataBin.Latitude, '.r'); grid on; hold on;

%% Longitude : OK

% Unit = Hdf5Utils.read_unit(Hdf5Data.Platform.Position.(strPositionId).longitude)
FieldUnits.Longitude = 'deg';
% figure; plot(DataBin.Longitude, '.r'); grid on; hold on;

%% Heading : Valeurs assez diff�rentes

% TODO GLU : Valeur non correcte
DataBin.Heading = single(DataBin.Heading);
% Unit = Hdf5Utils.read_unit(Hdf5Data.Platform.Position.(strPositionId).course_over_ground)
FieldUnits.Heading = 'deg';
% figure; plot(DataBin.Heading, '.r'); grid on; hold on;

%% Height above reference ellipsoid

DataBin.Height = single(DataBin.Height);
% Unit = Hdf5Utils.read_unit(Hdf5Data.Platform.Position.(strPositionId).height_above_reference_ellipsoid)
FieldUnits.Height = 'm';
% figure; plot(DataBin.Heigth, 'k'); grid on;

%% CourseVessel : Valeurs assez diff�rentes % ATTENTION : sans doute une confusion entre Heading et CourseVessel : � inspecter

% TODO GLU : Valeur non correcte
DataBin.CourseVessel = single(DataBin.Heading);
% Unit = Hdf5Utils.read_unit(Hdf5Data.Platform.Position.(strPositionId).course_over_ground)
FieldUnits.CourseVessel = 'deg';
% figure; plot(DataBin.Heading, '.r'); grid on; hold on;

%% Speed : OK

DataBin.Speed = single(DataBin.Speed);
% Unit = Hdf5Utils.read_unit(Hdf5Data.Platform.Position.(strPositionId).speed_over_ground)
FieldUnits.Speed = 'm/s';
% figure; plot(DataBin.Speed, '.r'); grid on; hold on;

%% AltitudeOfIMU : OK

DataBin.AltitudeOfIMU = single(DataBin.AltitudeOfIMU);
% Unit = Hdf5Utils.read_unit(Hdf5Data.Platform.Position.(strPositionId).height_above_reference_ellipsoid)
FieldUnits.AltitudeOfIMU = 'm';
% figure; plot(DataBin.AltitudeOfIMU, '.r'); grid on; hold on;

%% system_serial_number

%{
% system_serial_number % Nouvelle variable ?
DataBin.system_serial_number = Hdf5Utils.read_value(Hdf5Data.Platform.Position.(strPositionId).Vendor_specific.system_serial_number);
DataBin.system_serial_number = single(DataBin.system_serial_number);
FieldUnits.system_serial_number = '';
% figure; plot(DataBin.system_serial_number, '.r'); grid on; hold on;
%}

%% PositionDescriptor

%{
DataBin.PositionDescriptor = Hdf5Utils.read_value(Hdf5Data.Platform.Position.(strPositionId).Vendor_specific.system_serial_number);
DataBin.PositionDescriptor = single(DataBin.system_serial_number);
%}
FieldUnits.PositionDescriptor = '';
% figure; plot(DataBin.PositionDescriptor, '.r'); grid on; hold on;

%% data_received_from_sensor

% {
% data_received_from_sensor % Nouvelle variable ?
if ~isempty(NMEA)
    [flag, Heure1, Lat1, Lon1, GPS] = read_Navigation_NMEA(NMEA); %#ok<ASGLU>
    if flag
        DataBin.GPSDatetime = dateshift(DataBin.Datetime, 'start', 'day') + duration(0, 0, Heure1/1000);
        DataBin.GPSTime = cl_time('timeMat', datenum(DataBin.GPSDatetime));
        
        DataBin.QualityIndicator              = single([GPS.QualityIndicator]');
        DataBin.NumberSatellitesUsed          = single([GPS.NumberSatellitesUsed]');
        DataBin.HorizontalDilutionOfPrecision = single([GPS.HorizontalDilutionOfPrecision]');
        DataBin.AltitudeOfIMU                 = single([GPS.AltitudeOfIMU]');
        DataBin.GeoidalSeparation             = single([GPS.GeoidalSeparation]');
        DataBin.AgeOfDifferentialCorrections  = single([GPS.AgeOfDifferentialCorrections]');
        DataBin.DGPSRefStationIdentity        = [GPS.DGPSRefStationIdentity]';
        
        FieldUnits.QualityIndicator              = '';
        FieldUnits.NumberSatellitesUsed          = '';
        FieldUnits.HorizontalDilutionOfPrecision = 'm';
        FieldUnits.AltitudeOfIMU                 = 'm';
        FieldUnits.GeoidalSeparation             = '';
        FieldUnits.AgeOfDifferentialCorrections  = '';
        FieldUnits.DGPSRefStationIdentity        = '';

        %{
        figure; plot(DataBin.QualityIndicator, '.r'); grid on; hold on;
        figure; plot(DataBin.NumberSatellitesUsed, '.r')e; grid on; hold on;
        figure; plot(DataBin.HorizontalDilutionOfPrecision, '.r'); grid on; hold on; 
        figure; plot(DataBin.AltitudeOfIMU, '.r'); grid on; hold on; 
        figure; plot(DataBin.GeoidalSeparation, '.r'); grid on; hold on; 
        figure; plot(DataBin.AgeOfDifferentialCorrections, '.r'); grid on; hold on; 
        %}
    end
end

%% sensor_status

%{
% sensor_status % Nouvelle variable ?
DataBin.system_serial_number = Hdf5Utils.read_value(Hdf5Data.Platform.Position.(strPositionId).Vendor_specific.sensor_status);
DataBin.sensor_status = single(DataBin.system_serial_number);
FieldUnits.sensor_status = '';
% figure; plot(DataBin.sensor_status, '.r'); grid on; hold on;
%}

%% Cr�ation arbitraire de PingCounter

DataBin.PingCounter = (1:N)';
FieldUnits.PingCounter = '';

%% Cr�ation du XML

Fields = fields(DataBin);

XML.Dimensions.NbSamples = N;

XML.Title         = 'Position';
XML.Constructor   = 'Kongsberg';
XML.EmModel       = XSF.getEmModel(Hdf5Data);
XML.TimeOrigin    = '01/01/-4713';
XML.Comments      = 'Sensor sampling rate'; % ne rien mettre apr�s premoers tests
XML.FormatVersion = 20101118;

XML.SystemSerialNumber = Hdf5Data.Sonar.Att.sonar_serial_number;

Fields = Fields(~contains(Fields, 'Datetime')); 
for k=1:length(Fields)
    XML.Signals(k).Dimensions.Name      = 'NbSamples';
    XML.Signals(k).Dimensions.Length    = N;
    XML.Signals(k).Dimensions.Unlimited = 0;
    
    Values  = DataBin.(Fields{k});
    Storage = class(Values);
    switch Storage
        case 'cl_time'
            Storage = 'double';
            Unit    = 'days since JC';
            Values  = Values.timeMat;
        otherwise
            Unit = FieldUnits.(Fields{k});
    end
    
    XML.Signals(k).Name      = Fields{k};
    XML.Signals(k).Storage   = Storage;
    XML.Signals(k).Unit      = Unit;
    XML.Signals(k).Populated = 1;
    XML.Signals(k).Stats     = stats(Values);
end

%{
XML.Signals(3).Unit = '';
%}

FieldsData = fields(DataBin);
FieldsXML  = fields(XML);
for k=1:length(FieldsXML)
    Data.(FieldsXML{k}) = XML.(FieldsXML{k});
end

for k=1:length(FieldsData)
    Data.(FieldsData{k}) = DataBin.(FieldsData{k});
end

flag = 1;

% Data

%% Ajout JMA le 06/12/2023 pour donn�es RTK pour Marc Roche

DataHeight.Time          = Data.Time;
DataHeight.PingCounter   = Data.PingCounter;
DataHeight.Height        = Data.Height;
DataHeight.HeightType    = Data.QualityIndicator;
% DataHeight.HeightType  = NaN(size(Data.PingCounter), 'single');
DataHeight.Tide          = NaN(size(Data.PingCounter), 'single');
DataHeight.HeightInitial = Data.Height;


XMLHeight = XML;
XMLHeight.Title = 'Height';
kTime             = find(strcmp({XML.Signals(:).Name}, 'Time'));
kPingCounter      = find(strcmp({XML.Signals(:).Name}, 'PingCounter'));
kHeight           = find(strcmp({XML.Signals(:).Name}, 'Height'));
kQualityIndicator = find(strcmp({XML.Signals(:).Name}, 'QualityIndicator'));
XMLHeight.Signals            = XML.Signals([kTime kPingCounter kHeight kQualityIndicator]);
XMLHeight.Signals(4).Name    = 'HeightType';
XMLHeight.Signals(end+1)     = XML.Signals(kHeight);        
XMLHeight.Signals(end).Name  = 'Tide';
XMLHeight.Signals(end).Stats = stats(DataHeight.Tide);

XMLHeight.Signals(end+1)     = XML.Signals(kHeight);        
XMLHeight.Signals(end).Name  = 'HeightInitial';
