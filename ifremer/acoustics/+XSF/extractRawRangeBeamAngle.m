function [flag, XML, Data] = extractRawRangeBeamAngle(Hdf5Data, SounderDualOneTxAntenna)
    
XML  = [];
Data = [];

%% Time et Datetime : OK

% DataBin.Datetime = Hdf5Utils.read_value(Hdf5Data.Environment.Sound_speed_profile.time);
Signals.Datetime = Hdf5Utils.read_value(Hdf5Data.Sonar.Beam_group1.ping_time);
Signals.Time = cl_time('timeMat', datenum(Signals.Datetime));
N = length(Signals.Datetime);

%% PingCounter : OK

Signals.PingCounter = XSF.computePingCounter(Hdf5Data);
% Signals.PingCounter = single(Signals.PingCounter); % ATTENTION : surtout pas de single
% Signals.PingCounter = (1:N)';
% Unit = Hdf5Utils.read_unit(Hdf5Data.Sonar.Beam_group1.Vendor_specific.ping_raw_count) % TODO GLU : pour toutes les unit�s qui sont vides, il vaudrait mieux rendre '' plut�t que []
FieldUnits.PingCounter = '';
% figure; plot(Signals.PingCounter, '.r'); grid on;

%% SystemSerialNumber % TODO GLU

Signals.SystemSerialNumber = repmat(single(Hdf5Data.Sonar.Att.sonar_serial_number), N, 1); % TODO : trouver o� r�cup�rer cette info !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
FieldUnits.SystemSerialNumber = '';

%% SoundSpeed : OK

Signals.SoundSpeed = Hdf5Utils.read_value(Hdf5Data.Sonar.Beam_group1.sound_speed_at_transducer);
Signals.SoundSpeed = single(Signals.SoundSpeed);
% Unit = Hdf5Utils.read_unit(Hdf5Data.Sonar.Beam_group1.sound_speed_at_transducer)
FieldUnits.SoundSpeed = 'm/s';
% figure; plot(Signals.SoundSpeed, '.r'); grid on;

%% NbBeams : OK

Signals.NbBeams = Hdf5Utils.read_value(Hdf5Data.Sonar.Beam_group1.Vendor_specific.num_soundings_valid_main);
Signals.NbBeams = single(Signals.NbBeams');
Signals.NbBeams = sum(Signals.NbBeams,2);

 % Ajout JMA le 09/02/2024 pour fichier Marc Roche 0039_20240208_100346_Simon_Stevin.kmall
Signals.NbBeams(isnan(Signals.NbBeams)) = 0;

% Unit = Hdf5Utils.read_unit(Hdf5Data.Sonar.Beam_group1.Vendor_specific.num_soundings_valid_main)
FieldUnits.NbBeams = '';
% figure; hold on; plot(Signals.NbBeams, '.r'); grid on;

%% SamplingRate : OK

Signals.SamplingRate = Hdf5Utils.read_value(Hdf5Data.Sonar.Beam_group1.Vendor_specific.seabed_image_sample_rate);
Signals.SamplingRate = single(Signals.SamplingRate');
% Unit = Hdf5Utils.read_unit(Hdf5Data.Sonar.Beam_group1.Vendor_specific.seabed_image_sample_rate) % TODO GLU : devrait rendre Hz
FieldUnits.SamplingRate = 'Hz';
% figure; plot(Signals.SamplingRate, '.r'); grid on;

%% TiltAngle : OK 

% Signals.TiltAngle = Hdf5Utils.read_value(Hdf5Data.Sonar.Beam_group1.tx_beam_rotation_theta);
Signals.TiltAngle = Hdf5Utils.read_value(Hdf5Data.Sonar.Beam_group1.Vendor_specific.raw_tx_beam_tilt_angle);
Signals.TiltAngle = single(Signals.TiltAngle)';
% Unit = Hdf5Utils.read_unit(Hdf5Data.Sonar.Beam_group1.Vendor_specific.raw_tx_beam_tilt_angle)
FieldUnits.TiltAngle = 'deg';
% figure; plot(Signals.TiltAngle, '.r'); grid on;

nbTx = size(Signals.TiltAngle, 2);

%% FocusRange : OK

Signals.FocusRange = Hdf5Utils.read_value(Hdf5Data.Sonar.Beam_group1.Vendor_specific.tx_focus_range);
Signals.FocusRange = single(Signals.FocusRange)';
% Unit = Hdf5Utils.read_unit(Hdf5Data.Sonar.Beam_group1.Vendor_specific.tx_focus_range) % TOFO GLU : devrait rentre 'm'
FieldUnits.FocusRange = 'm';
% figure; plot(Signals.FocusRange, '.r'); grid on;

%% SignalLength : OK

Signals.SignalLength = Hdf5Utils.read_value(Hdf5Data.Sonar.Beam_group1.Vendor_specific.signal_length);
Signals.SignalLength = single(Signals.SignalLength)';
% Unit = Hdf5Utils.read_unit(Hdf5Data.Sonar.Beam_group1.Vendor_specific.signal_length) % TODO GLU : devrait rendre s ou sample
FieldUnits.SignalLength = 's';
% figure; plot(Signals.SignalLength, '.r'); grid on;

%% SectorTransmitDelay : OK

Signals.SectorTransmitDelay = Hdf5Utils.read_value(Hdf5Data.Sonar.Beam_group1.Vendor_specific.transmit_time_delay);
Signals.SectorTransmitDelay = single(Signals.SectorTransmitDelay)' * 1000;
% Unit = Hdf5Utils.read_unit(Hdf5Data.Sonar.Beam_group1.Vendor_specific.transmit_time_delay)
FieldUnits.SectorTransmitDelay = 'ms'; % Hdf5Utils.read_unit rend bien des s mais on multiplie la valeur par 1000 pour obtenir des ms
% figure; plot(Signals.SectorTransmitDelay, '.r'); grid on;

%% CentralFrequency : OK

Signals.CentralFrequency = Hdf5Utils.read_value(Hdf5Data.Sonar.Beam_group1.Vendor_specific.center_frequency);
Signals.CentralFrequency = single(Signals.CentralFrequency)';
% Unit = Hdf5Utils.read_unit(Hdf5Data.Sonar.Beam_group1.Vendor_specific.center_frequency)
FieldUnits.CentralFrequency = 'Hz';
% figure; plot(Signals.CentralFrequency, '.r'); grid on;

%% MeanAbsorptionCoeff : TODO JMA OK sauf dimension Nx800 au lieu de Nx8

%{
Signals.MeanAbsorptionCoeff = Hdf5Utils.read_value(Hdf5Data.Sonar.Beam_group1.Bathymetry.detection_mean_absorption_coefficient);
Signals.MeanAbsorptionCoeff = single(Signals.MeanAbsorptionCoeff)';
%}

Signals.MeanAbsorptionCoeff = zeros(size(Signals.CentralFrequency), 'single');
FieldUnits.MeanAbsorptionCoeff = 'dB/km';
% figure; plot(Signals.MeanAbsorptionCoeff, '.r'); grid on;

%% SignalWaveformIdentifier : OK mais toutes les valeurs sont = 0

Signals.SignalWaveformIdentifier = Hdf5Utils.read_value(Hdf5Data.Sonar.Beam_group1.Vendor_specific.pulse_form);
Signals.SignalWaveformIdentifier = single(Signals.SignalWaveformIdentifier)';
% Unit = Hdf5Utils.read_unit(Hdf5Data.Sonar.Beam_group1.Vendor_specific.pulse_form)
FieldUnits.SignalWaveformIdentifier = '';
% figure; plot(Signals.SignalWaveformIdentifier, '.r'); grid on;

%% TransmitSectorNumberTx : OK

Signals.TransmitSectorNumberTx = Hdf5Utils.read_value(Hdf5Data.Sonar.Beam_group1.Vendor_specific.tx_sector_number);
Signals.TransmitSectorNumberTx = single(Signals.TransmitSectorNumberTx)';
% Unit = Hdf5Utils.read_unit(Hdf5Data.Sonar.Beam_group1.Vendor_specific.tx_sector_number)
FieldUnits.TransmitSectorNumberTx = '';
% figure; plot(Signals.TransmitSectorNumberTx, '.r'); grid on;

%% SignalBandwith : OK

Signals.SignalBandwith = Hdf5Utils.read_value(Hdf5Data.Sonar.Beam_group1.transmit_bandwidth);
Signals.SignalBandwith = single(Signals.SignalBandwith)';
% Unit = Hdf5Utils.read_unit(Hdf5Data.Sonar.Beam_group1.transmit_bandwidth)
FieldUnits.SignalBandwith = 'Hz';
% figure; plot(Signals.SignalBandwith, '.r'); grid on;

%% DualSwath : OK

Signals.DualSwath = Hdf5Utils.read_value(Hdf5Data.Sonar.Beam_group1.Vendor_specific.swath_per_ping);
Signals.DualSwath = single(Signals.DualSwath)';
% Unit = Hdf5Utils.read_unit(Hdf5Data.Sonar.Beam_group1.Vendor_specific.swath_per_ping)
FieldUnits.DualSwath = '';
% figure; plot(Signals.DualSwath, '.r'); hold on;


%% BeamPointingAngle : OK

Images.BeamPointingAngle = Hdf5Utils.read_value(Hdf5Data.Sonar.Beam_group1.Bathymetry.detection_beam_pointing_angle);
Images.BeamPointingAngle = -single(Images.BeamPointingAngle');
% Unit = Hdf5Utils.read_unit(Hdf5Data.Sonar.Beam_group1.Bathymetry.detection_beam_pointing_angle)
FieldUnits.BeamPointingAngle = 'deg';
% figure; imagesc(Images.BeamPointingAngle); colormap(jet(256)); colorbar; title('Images.BeamPointingAngle')

nbRx = size(Images.BeamPointingAngle, 2);

%% TransmitSectorNumberRx : OK

Images.TransmitSectorNumberRx = Hdf5Utils.read_value(Hdf5Data.Sonar.Beam_group1.Bathymetry.detection_tx_beam);
Images.TransmitSectorNumberRx = single(Images.TransmitSectorNumberRx');
Images.TransmitSectorNumberRx = Images.TransmitSectorNumberRx + 1;
% Unit = Hdf5Utils.read_unit(Hdf5Data.Sonar.Beam_group1.Bathymetry.detection_tx_beam)
FieldUnits.TransmitSectorNumberRx = '';
% figure; imagesc(Images.TransmitSectorNumberRx); colormap(jet(256)); colorbar; title('Images.TransmitSectorNumberRx')

%% DetectionInfo % TODO factoriser ce code avec extractDepth

% TODO GLU : bug Hdf5Utils.read_value sur donn�es �num�ration
% X = Hdf5Utils.read_value(Hdf5Data.Sonar.Beam_group1.Bathymetry.detection_type);
% Images.DetectionInfo = arrayfun(@(x) strcmp(x{:}, 'phase'), X);
Images.DetectionInfo = Hdf5Data.Sonar.Beam_group1.Bathymetry.detection_type.value;
Images.DetectionInfo = single(Images.DetectionInfo(:,:)');
% Unit = Hdf5Utils.read_unit(Hdf5Data.Sonar.Beam_group1.Bathymetry.detection_type)
FieldUnits.DetectionInfo = '';

% sub = (Images.DetectionInfo == 0);
% Images.DetectionInfo(sub)  = 16;
% Images.DetectionInfo(~sub) = 17;

% figure; imagesc(Images.DetectionInfo); colormap(jet(256)); colorbar; title('Images.DetectionInfo')

%% LengthDetection % TODO GLU : o� se trouve cette variable ? Pour l'instant, je mets seabed_image_center

Images.LengthDetection = Hdf5Utils.read_value(Hdf5Data.Sonar.Beam_group1.Bathymetry.seabed_image_center);
Images.LengthDetection = single(Images.LengthDetection(:,:)');
% Unit = Hdf5Utils.read_unit(Hdf5Data.Sonar.Beam_group1.Bathymetry.seabed_image_center) % TODO GLU : devrait rentre s ou sample ?
FieldUnits.LengthDetection = 'sample';
% figure; imagesc(Images.LengthDetection); colormap(jet(256)); colorbar; title('Images.LengthDetection')


%% QualityFactor % TODO GLU attention : c'est le m�me code que dans extractDepth : faire une seule fonction

%{
Images.QualityFactor = Hdf5Utils.read_value(Hdf5Data.Sonar.Beam_group1.Bathymetry.seabed_image_center);
Images.QualityFactor = single(Images.LengthDetection');
%}
Images.QualityFactor = zeros(size(Images.BeamPointingAngle), 'single');
FieldUnits.QualityFactor = '';
% figure; imagesc(Images.QualityFactor); colormap(jet(256)); colorbar; title('Images.QualityFactor')

%% TwoWayTravelTime : OK

Images.TwoWayTravelTime = Hdf5Utils.read_value(Hdf5Data.Sonar.Beam_group1.Bathymetry.detection_two_way_travel_time);
Images.TwoWayTravelTime = single(Images.TwoWayTravelTime');
% Unit = Hdf5Utils.read_unit(Hdf5Data.Sonar.Beam_group1.Bathymetry.detection_two_way_travel_time)
FieldUnits.TwoWayTravelTime = 's';
% figure; imagesc(Images.TwoWayTravelTime); colormap(jet(256)); colorbar; title('Images.TwoWayTravelTime')

%% Reflectivity : OK : TODO c'est le m�me code que dans extractDepth : faire un seule fonction

Images.Reflectivity = Hdf5Utils.read_value(Hdf5Data.Sonar.Beam_group1.Bathymetry.detection_backscatter_r);
Images.Reflectivity = single(Images.Reflectivity');
% Unit = Hdf5Utils.read_unit(Hdf5Data.Sonar.Beam_group1.Bathymetry.detection_backscatter_r) % TODO GLU : devrait rendre Hz
FieldUnits.Reflectivity = 'dB';
% figure; imagesc(Images.Reflectivity); colormap(gray(256)); colorbar; title('Images.Reflectivity')

%% RealTimeCleaningInfo

Images.RealTimeCleaningInfo = zeros(size(Images.BeamPointingAngle), 'single');
FieldUnits.RealTimeCleaningInfo = '';

if SounderDualOneTxAntenna
    pi;%JMA
    TxIndex = Hdf5Utils.read_value(Hdf5Data.Sonar.Beam_group1.Vendor_specific.tx_sector_number);
    TxIndex = TxIndex';
    NTx = max(TxIndex, [], 2) + 1; %#ok<UDIM> 
    nbPings = size(Images.TwoWayTravelTime,1);
    [Signals, Images.TransmitSectorNumberRx] = XSF.duplicateSectorsIfDualSounders(Signals, NTx, nbPings, nbRx, Images.TransmitSectorNumberRx);
%     [aaaa, bbbb] = duplicateSectorsIfDualSounders(Signals, NTx, nbPings, nbRx, Images.TransmitSectorNumberRx);
    nbTx = size(Signals.TiltAngle, 2);
end

%% Cr�ation du XML

XML.Title         = 'RawRangeBeamAngle';
XML.Constructor   = 'Kongsberg';
XML.EmModel       = XSF.getEmModel(Hdf5Data);
XML.TimeOrigin    = '01/01/-4713';
XML.Comments      = 'Sounder ping rate';
XML.Version       = 'V2';
XML.FormatVersion = 20101118;
XML.IdentAlgoSnippets2OneValuePerBeam = 2;

XML.Dimensions.nbPings    = N;
XML.Dimensions.nbTx       = nbTx;
XML.Dimensions.nbRx       = nbRx;
XML.Dimensions.nbSounders = size(Signals.SamplingRate,2); % size(Signals.BSN,2);

% Dans datagramme Installation SN=10001 ?
XML.ListeSystemSerialNumber = Hdf5Data.Sonar.Att.sonar_serial_number;

Fields = fields(Signals);
Fields = Fields(~contains(Fields, 'Datetime')); 
for k=1:length(Fields)
    XML.Signals(k).Dimensions.Name      = 'nbPings';
    XML.Signals(k).Dimensions.Length    = N;
    XML.Signals(k).Dimensions.Unlimited = 0;
    
    Values  = Signals.(Fields{k});
    Storage = class(Values);
    switch Storage
        case 'cl_time'
            Storage = 'double';
            Unit    = 'days since JC';
            Values  = Values.timeMat;
        otherwise
            Unit = FieldUnits.(Fields{k});
    end
    
    if size(Values, 2) == nbTx
        XML.Signals(k).Dimensions(2).Name      = 'nbTx';
        XML.Signals(k).Dimensions(2).Length    = nbTx;
        XML.Signals(k).Dimensions(2).Unlimited = 0;
    elseif size(Values, 2) == XML.Dimensions.nbSounders
        XML.Signals(k).Dimensions(2).Name      = 'nbSounders';
        XML.Signals(k).Dimensions(2).Length    = XML.Dimensions.nbSounders;
        XML.Signals(k).Dimensions(2).Unlimited = 0;
    end
    
    XML.Signals(k).Name      = Fields{k};
    XML.Signals(k).Storage   = Storage;
    XML.Signals(k).Unit      = Unit;
    XML.Signals(k).Populated = 1;
    XML.Signals(k).Stats     = stats(Values);
    
    %{
    try
        for k2=1:length(DataPosition.Signals)
            if strcmp(XML.Signals(k2).Name, DataPosition.Signals(k2).Name)
                break
            end
        end
        fprintf('XML.Signals(%d).Unit = ''%s'';\n', k2, DataPosition.Signals(k2).Unit);
    catch
        XML.Signals(k).Name;
    end
    %}
end


Fields = fields(Images);
for k=1:length(Fields)
    XML.Images(k).Dimensions.Name      = 'nbPings';
    XML.Images(k).Dimensions.Length    = N;
    XML.Images(k).Dimensions.Unlimited = 0;
    
    XML.Images(k).Dimensions(2).Name      = 'nbRx';
    XML.Images(k).Dimensions(2).Length    = nbRx;
    XML.Images(k).Dimensions(2).Unlimited = 0;
    
    Values  = Images.(Fields{k});
    Storage = class(Values);
    switch Storage
        case 'cl_time'
            Storage = 'double';
            Unit    = 'days since JC';
            Values  = Values.timeMat;
        case 'datetime'
            continue
        otherwise
            Unit = FieldUnits.(Fields{k});
    end
    
    XML.Images(k).Name      = Fields{k};
    XML.Images(k).Storage   = Storage;
    XML.Images(k).Unit      = Unit;
    XML.Images(k).Populated = 1;
    XML.Images(k).Stats     = stats(Values);
end

FieldsXML  = fields(XML);
for k=1:length(FieldsXML)
    Data.(FieldsXML{k}) = XML.(FieldsXML{k});
end

FieldsData = fields(Signals);
for k=1:length(FieldsData)
    Data.(FieldsData{k}) = Signals.(FieldsData{k});
end

FieldsData = fields(Images);
for k=1:length(FieldsData)
    Data.(FieldsData{k}) = Images.(FieldsData{k});
end

% DataPosition
% Data

flag = 1;
