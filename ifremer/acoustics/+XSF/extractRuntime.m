function [flag, XML, Data] = extractRuntime(Hdf5Data)

%{
Variables ayant disparu dans les .kmall :
	OperatorStationStatus
	ProcessingUnitStatus
	BSPStatus
	SonarHeadStatus
	TransmitPulseLength
	TransmitBeamWidth
	TransmitPowerMax
	ReceiveBeamWidth
	ReceiveBandWidth
	ReceiverFixGain
	
Nouvelles variables :
	ExternalTrigger
	UseLambertsLaw
	TransmitPowerLevel
	SoftStartup
	WaterColumnStatus
	WaterColumnCoef
	AngularCoverageMode
	FMDisable
	EnableSimulation
	WaterPhaseData
	SonarMode
%}

XML  = [];
Data = [];

%% Variables

%% Time et Datetime : OK

DataBin.Datetime = Hdf5Utils.read_value(Hdf5Data.Platform.Vendor_specific.runtime.time);
DataBin.Time = cl_time('timeMat', datenum(DataBin.Datetime));

strRuntime = Hdf5Utils.read_value(Hdf5Data.Platform.Vendor_specific.runtime.runtime_txt);
N = size(strRuntime, 1);
for k=1:N
    if iscell(strRuntime)
        str = strRuntime{k,:};
    else
        str = strRuntime(k,:);
    end
    str = strrep(str, ':', '');
    strEnrRuntime{k} = strsplit(str, '\n'); %#ok<AGROW>
end

%% PingCounter % Different

DataBin.PingCounter = Hdf5Utils.read_value(Hdf5Data.Platform.Vendor_specific.runtime.runtime_count);
DataBin.PingCounter = (1:length(DataBin.PingCounter))'; % TODO : valeurs 0 dans fichier
DataBin.PingCounter = uint16(DataBin.PingCounter);
FieldUnits.PingCounter = '';

%% Sector coverage

%% MaximumPortCoverage : OK

pattern1 = 'Max angle Port';
pattern2 = 'Max angle RX_1 Port';
for k=1:N
    str = strEnrRuntime{k};
    ind = contains(str, pattern1) | contains(str, pattern2);
    str = str{ind};
    ind = strfind(str, 'Port');
    val = str2double(str(ind+4:end));
    DataBin.MaximumPortCoverage(k,1) = val;
end
DataBin.MaximumPortCoverage = single(DataBin.MaximumPortCoverage);
FieldUnits.MaximumPortCoverage = 'deg';

%% MaximumStarboardCoverage : OK

pattern1 = 'Max angle Starboard';
pattern2 = 'Max angle RX_2 Starboard';
for k=1:N
    str = strEnrRuntime{k};
    ind = contains(str, pattern1) | contains(str, pattern2);
    str = str{ind};
    ind = strfind(str, 'Starboard');
    val = str2double(str(ind+9:end));
    DataBin.MaximumStarboardCoverage(k,1) = val;
end
DataBin.MaximumStarboardCoverage = single(DataBin.MaximumStarboardCoverage);
FieldUnits.MaximumStarboardCoverage = 'deg';

%% MaximumPortSwath : OK

pattern = 'Max coverage Port';
for k=1:N
    str = strEnrRuntime{k};
    ind = (contains(str, pattern));
    val = str2double(str{ind}(length(pattern)+1:end));
    DataBin.MaximumPortSwath(k,1) = val;
end
DataBin.MaximumPortSwath = single(DataBin.MaximumPortSwath);
FieldUnits.MaximumPortSwath = 'm';

%% MaximumStarboardSwath : OK

pattern = 'Max coverage Starboard';
for k=1:N
    str = strEnrRuntime{k};
    ind = (contains(str, pattern));
    val = str2double(str{ind}(length(pattern)+1:end));
    DataBin.MaximumStarboardSwath(k,1) = val;
end
DataBin.MaximumStarboardSwath = single(DataBin.MaximumStarboardSwath);
FieldUnits.MaximumStarboardSwath = 'm';

%% Angular coverage Mode : Nouvelle variable

pattern = 'Angular coverage Mode';
for k=1:N
    str = strEnrRuntime{k};
    ind = (contains(str, pattern));
    val = strtrim(str{ind}(length(pattern)+1:end));
    switch val
        case 'Manual'
            val = 1;
        case 'Auto'
            val = 2;
        otherwise
            my_breakpoint
    end
    DataBin.AngularCoverageMode(k,1) = val; % TODO JMA : voir comment c'est utilis� (0/1/2 ???)
end
FieldUnits.AngularCoverageMode = ''; % Peut-�tre donner la liste ?

%% BeamSpacing % Valeurs diff�rentes

pattern = 'Beam spacing';
for k=1:N
    str = strEnrRuntime{k};
    ind = (contains(str, pattern));
    val = strtrim(str{ind}(length(pattern)+1:end));
    switch val
        case {'Equidistant'; 'Equidistance'}
            val = 2;
        case 'Equiangle'
            val = 3;
        case 'High density'
            val = 4;
        case 'Ultra high density' % TODO : ajout JMA le 26/10/2021 pour donn�es EM2040 DualRX
            val = 4;
        otherwise
            my_breakpoint
            val = 1;
    end
    DataBin.BeamSpacing(k,1) = val; % TODO JMA : voir comment c'est utilis� (0/1/2 ???)
end
DataBin.BeamSpacing = uint8(DataBin.BeamSpacing);
% DataBin.BeamSpacing = single(DataBin.BeamSpacing); % Comment� pour l'instant car �a devrait rester un uint8
FieldUnits.BeamSpacing = ''; % Peut-�tre donner la liste ?

%% Depth settings

%% Forced depth : Nouvelle variable ?



%% MinimumDepth : OK

pattern = 'Min. depth';
for k=1:N
    str = strEnrRuntime{k};
    ind = (contains(str, pattern));
    val = str2double(str{ind}(length(pattern)+1:end));
    DataBin.MinimumDepth(k,1) = val;
end
DataBin.MinimumDepth = single(DataBin.MinimumDepth);
FieldUnits.MinimumDepth = 'm';

%% MaximumDepth : OK

pattern = 'Max. depth';
for k=1:N
    str = strEnrRuntime{k};
    ind = (contains(str, pattern));
    val = str2double(str{ind}(length(pattern)+1:end));
    DataBin.MaximumDepth(k,1) = val;
end
DataBin.MaximumDepth = single(DataBin.MaximumDepth);
FieldUnits.MaximumDepth = 'm';

%% DualSwath : OK

DualSwath = Hdf5Utils.read_value(Hdf5Data.Sonar.Beam_group1.Vendor_specific.swath_per_ping);
DatetimeRaw = Hdf5Utils.read_value(Hdf5Data.Sonar.Beam_group1.ping_time);
DataBin.DualSwath = my_interp1_Extrap_PreviousThenNext(DatetimeRaw, DualSwath, DataBin.Datetime);
DataBin.DualSwath = uint8(DataBin.DualSwath);
FieldUnits.DualSwath = '';

%% ReceiverFixGain

DataBin.ReceiverFixGain = zeros([N 1], 'uint8');
FieldUnits.ReceiverFixGain = 'dB';

%% Mode : OK

DataBin.Mode = zeros([N 1], 'uint8'); % Attention : d�fini pour plusieurs infos stock�es dans les 8 bits

pattern = 'Depth mode'; % Ajout JMA et Ridha pour EM2040C HDrix
Bit = NaN;
for k=1:N
    str = strEnrRuntime{k};
    ind = find(contains(str, pattern));

    if ~isempty(find(contains(str, 'Depth settings'), 1)) && ~isempty(find(contains(str, 'Frequency'), 1))
        continue % Ajout JMA le 12/09/2022 pour d�tecter si la version du .kmall est r�cente
    end

    if ~isempty(ind)
        str = str{ind(end)};
        val = strtrim(str(length(pattern)+1:end));
        switch lower(val)
            case 'very shallow'
                Bit = uint8(1);
            case 'shallow'
                Bit = uint8(2);
            case 'medium'
                Bit = uint8(3);
            case 'deep'
                Bit = uint8(4);
            case 'deeper'
                Bit = uint8(5);
            case 'very deep'
                Bit = uint8(6);
            case 'extra deep'
                Bit = uint8(7);
            case 'extreme deep'
                Bit = uint8(8);
            case 'auto'
                Bit = uint8(1); % Ajout � l'arrache le 30/09/2021 avec Ridha
            otherwise
                my_breakpoint
        end
        DataBin.Mode(k,1) = bitor(DataBin.Mode(k,1), Bit, 'uint8');
    end
end
if isnan(Bit)
    pattern = 'Depth setting';
    for k=1:N
        str = strEnrRuntime{k};
        ind = find(contains(str, pattern));
        str = str{ind(end)};
        if strcmp(str, 'Depth setting Auto')
           DepthMode = Hdf5Utils.read_value(Hdf5Data.Sonar.Beam_group1.Vendor_specific.depth_mode);
           DatetimeRaw = Hdf5Utils.read_value(Hdf5Data.Sonar.Beam_group1.ping_time);
           DataBin.Mode(k,1) = my_interp1_Extrap_PreviousThenNext(DatetimeRaw, DepthMode, DataBin.Datetime(k));
           DataBin.Mode(k,1) = uint8(DataBin.Mode(k,1)+1);
           
%             Bit = uint8(1); % Ajout � l'arrache le 11/10/2022 pour donn�es EM304 Marc Roche
%             DataBin.Mode(k,1) = bitor(DataBin.Mode(k,1), Bit, 'uint8');
        else
            if strcmp(str, 'Depth settings')
                str2 = strEnrRuntime{k};
                pattern2 = 'Frequency';
                ind2 = find(contains(str2, pattern2));
                if ~isempty(ind2)
                    str2 = str2{ind2(end)};
                    val2 = strtrim(str2(length(pattern2)+1:end));
                    switch lower(val2)
                        case '200khz'
                            DataBin.Mode(k,1) = 1;
                        case '300khz'
                            DataBin.Mode(k,1) = 2;
                        case '400khz'
                            DataBin.Mode(k,1) = 3;
                        otherwise
                            DataBin.Mode(k,1) = 2; % Ajout � l'arrache le 26/10/2021 pour EM2040 Dual du Belgica
                    end

                    % Ajout JMA le 15/10/2022 pour EM2040 pulse length
                    str = strEnrRuntime{k};
                    pattern2 = 'Depth mode';
                    ind2 = find(contains(str, pattern2));
                    if ~isempty(ind2)
                        str2 = str{ind2(end)};
                        val2 = strtrim(str2(length(pattern2)+1:end));
                        switch lower(val2)
                            case 'shallow'
                                Bit = uint8(0);
                            case 'medium'
                                Bit = uint8(4);
                            case 'deep'
                                Bit = uint8(8);
                            otherwise
                                Bit = uint8(1); % Au cas o�
                        end
                        %                         PulseLengthConfIfEM2040 = mode(1 + decodeBits(uint8(DataRuntime.ReceiverFixGain), [4 3]));
                        DataBin.ReceiverFixGain(k,1) = bitor(DataBin.ReceiverFixGain(k,1), Bit, 'uint8');
                    end
                    % Fin ajout JMA le 15/10/2022 pour EM2040 pulse length
                    continue

                else
                    val = strtrim(str(length(pattern)+1:end));
                    switch lower(val)
                        case 'very shallow'
                            Bit = uint8(1);
                        case 'shallow'
                            Bit = uint8(2);
                        case 'medium'
                            Bit = uint8(3);
                        case 'deep'
                            Bit = uint8(4);
                        case 'deeper'
                            Bit = uint8(5);
                        case 'very deep'
                            Bit = uint8(6);
                        case 'extra deep'
                            Bit = uint8(7);
                        case 'extreme deep'
                            Bit = uint8(8);
                        case 'auto'
                            Bit = uint8(1); % Ajout � l'arrache le 30/09/2021 avec Ridha
                        otherwise
%                             my_breakpoint

% Modif JMA le 17/01/2025 pour fichier NC de Ridha 0010_20200122_102500_712.kmall
                            Bit = uint8(1); % Ajout � l'arrache le 17/0&/20215 avec Ridha
                    end
                end
            else
                val = strtrim(str(length(pattern)+1:end));
                switch lower(val)
                    case 'very shallow'
                        Bit = uint8(1);
                    case 'shallow'
                        Bit = uint8(2);
                    case 'medium'
                        Bit = uint8(3);
                    case 'deep'
                        Bit = uint8(4);
                    case 'deeper'
                        Bit = uint8(5);
                    case 'very deep'
                        Bit = uint8(6);
                    case 'extra deep'
                        Bit = uint8(7);
                    case 'extreme deep'
                        Bit = uint8(8);
                    case 'auto'
                        Bit = uint8(1); % Ajout � l'arrache le 30/09/2021 avec Ridha
                    otherwise
                        my_breakpoint
                end
            end
            DataBin.Mode(k,1) = bitor(DataBin.Mode(k,1), Bit, 'uint8');
        end
    end
end
DataBin.Mode = uint8(DataBin.Mode);
DataBin.ModeDatagram = DataBin.Mode - 1;
FieldUnits.Mode         = '';
FieldUnits.ModeDatagram = '';

%% FM disable : Nouvelle variable

pattern = 'FM disable';
for k=1:N
    str = strEnrRuntime{k};
    ind = contains(str, pattern);
    val = strtrim(str{ind}(length(pattern)+1:end));
    DataBin.FMDisable(k,1) = strcmp(val, 'On'); % TODO JMA : voir comment c'est utilis� (0/1/2 ???)
end
FieldUnits.FMDisable = '';

%% Transmit Control

%% YawStabMode % Diff�rent mais sans doute d� � une erreur du convertKmall2All

DataBin.YawStabMode = zeros([N 1], 'uint8'); % Attention : d�fini pour plusieurs infos stock�es dans les 8 bits
pattern = 'Pitch stabilisation';
for k=1:N
    str = strEnrRuntime{k};
    ind = contains(str, pattern);
    val = strtrim(str{ind}(length(pattern)+1:end));
    switch val
        case 'Off'
            Bit = uint8(0);
        case 'On'
            Bit = uint8(128);
        otherwise
            my_breakpoint
    end
    DataBin.YawStabMode(k,1) = bitor(DataBin.YawStabMode(k,1), Bit, 'uint8');
    DataBin.PitchStabMode(k,1) = strcmp(val, 'On');
end
DataBin.YawStabMode = uint8(DataBin.YawStabMode);
% DataBin.YawStabMode = single(DataBin.YawStabMode); % Comment� pour le moment
DataBin.PitchStabMode = uint8(DataBin.YawStabMode);
% DataBin.PitchStabMode = single(DataBin.PitchStabMode); % Comment� pour le moment
FieldUnits.PitchStabMode = '';

%% Transmit angle Along : OK (� priori car valeurs = 0)

pattern = 'Transmit angle Along';
for k=1:N
    str = strEnrRuntime{k};
    ind = contains(str, pattern);
    val = str2double(str{ind}(length(pattern)+1:end));
    %             DataBin.TransmitAngleAlong(k,1) = val; % TODO JMA : voir comment c'est utilis� (0/1/2 ???)
    DataBin.DurotongSpeed(k,1) = val;
end
DataBin.DurotongSpeed = uint16(DataBin.DurotongSpeed);
FieldUnits.DurotongSpeed = '';

%% External Trigger : Nouvelle variable

pattern = 'External Trigger';
for k=1:N
    str = strEnrRuntime{k};
    ind = contains(str, pattern);
    val = strtrim(str{ind}(length(pattern)+1:end));
    DataBin.ExternalTrigger(k,1) = strcmp(val, 'On'); % TODO JMA : voir comment c'est utilis� (0/1/2 ???)
end
FieldUnits.ExternalTrigger = '';

%% Yaw Stabilisation Mode : Diff�rent mais sans doute d� � une erreur du convertKmall2All

pattern = 'Yaw Stabilisation Mode';
for k=1:N
    str = strEnrRuntime{k};
    ind = contains(str, pattern);
    val = strtrim(str{ind}(length(pattern)+1:end));
    switch val
        case 'Off'
            Bit = uint8(0);
        case 'Survey line heading'
            Bit = uint8(1);
        case {'Mean vessel heading'; 'Rel. mean heading'} % Ajout JMA le 26/10/2021 pour EM2040 Dual Rx du Belgica
            Bit = uint8(2);
        case 'Manually entered heading'
            Bit = uint8(3);
        otherwise
            my_breakpoint
    end
    DataBin.YawStabMode(k,1) = bitor(DataBin.YawStabMode(k,1), Bit, 'uint8');
end

FieldUnits.YawStabMode = '';

%% 3D Scanning Enable : Nouvelle variable

for k=1:N
    str = strEnrRuntime{k};
    ind = contains(str, pattern);
    val = strtrim(str{ind}(length(pattern)+1:end));
    DataBin.D3ScanningEnable(k,1) = strcmp(val, 'On'); % TODO JMA : voir comment c'est utilis� (0/1/2 ???)
end
FieldUnits.D3ScanningEnable = '';

%% Sound Speed at Transducer

%% Sound Velocity source % Different (.all=2,.kmall=3)

DataBin.SourceSoundSpeed = zeros([N 1], 'uint8');
pattern = 'Sound Velocity source';
for k=1:N
    str = strEnrRuntime{k};
    ind = contains(str, pattern);
    val = strtrim(str{ind}(length(pattern)+1:end));
    switch val
        case {'Probe at HWS'; 'Probe'; 'Profile'} % 'Profile' rajout� par JMA le 26/10/2021 pour EM2040 dual Rx du Belgica
            Bit = 3;
        otherwise
            my_breakpoint
    end
    DataBin.SourceSoundSpeed(k,1) = bitor(DataBin.SourceSoundSpeed(k,1), Bit, 'uint8');
end
% DataBin.SourceSoundSpeed = single(DataBin.SourceSoundSpeed); % Comment� pour l'instant car �a devrait rester un uint8
FieldUnits.SourceSoundSpeed = '';

%% Absorption Coefficient

%% AbsorptionCoeff % Diff�rent mais sans doute d� � une erreur du convertKmall2All

pattern = 'Absorption Coefficient';
for k=1:N
    str = strEnrRuntime{k};
    ind = find(contains(str, pattern));
    if isempty(ind)
        DataBin.AbsorptionCoeff(k,1) = NaN;
    else
        str = strsplit(str{ind+1}, ' '); % Attention : ind+1 ici car l'info est sur le champ suivant
        val = str2double(str{3});
        DataBin.AbsorptionCoeff(k,1) = val; % TODO JMA : voir comment c'est utilis� (0/1/2 ???)
    end
end
DataBin.AbsorptionCoeff = single(DataBin.AbsorptionCoeff);
FieldUnits.AbsorptionCoeff = 'dB/km';

%% Filtering

%% Spike filter strength

DataBin.FilterIdentifier = zeros([N 1], 'uint8'); % Attention : d�fini pour plusieurs infos stock�es dans les 8 bits
pattern = 'Spike filter strength';
for k=1:N
    str = strEnrRuntime{k};
    ind = contains(str, pattern);
    val = strtrim(str{ind}(length(pattern)+1:end));
    switch val
        case 'Off'
            Bit = 0;
        case 'Weak'
            Bit = 1;
        case 'Medium'
            Bit = 2;
        case 'Strong'
            Bit = 3;
        otherwise
            my_breakpoint
    end
    DataBin.FilterIdentifier(k,1) = bitor(DataBin.FilterIdentifier(k,1), Bit, 'uint8');
end
FieldUnits.FilterIdentifier = '';
% bitand(uint8(DataBin.FilterIdentifier), uint8(3), 'uint8')

%% Range gate size : OK (a priori car valeurs=0)

pattern = 'Range gate size';
for k=1:N
    str = strEnrRuntime{k};
    ind = contains(str, pattern);
    val = strtrim(str{ind}(length(pattern)+1:end));
    switch val
        case 'Normal'
            Bit = uint8(0);
        case 'Large'
            Bit = uint8(16);
        case 'Small'
            Bit = uint8(128);
        otherwise
            my_breakpoint
    end
    DataBin.FilterIdentifier(k,1) = bitor(DataBin.FilterIdentifier(k,1), Bit, 'uint8');
end
% bitand(uint8(DataBin.FilterIdentifier), uint8(128+16), 'uint8')

%% Phase ramp : OK

DataBin.HiLoFreqAbsCoefRatio = zeros([N 1], 'uint8'); % Attention : d�fini pour plusieurs infos stock�es dans les 8 bits
pattern = 'Phase ramp';
for k=1:N
    str = strEnrRuntime{k};
    ind = contains(str, pattern);
    val = strtrim(str{ind}(length(pattern)+1:end));
    switch val
        case 'Short'
            Bit = uint8(0);
        case 'Normal'
            Bit = uint8(16);
        case 'Long'
            Bit = uint8(32);
        otherwise
            my_breakpoint
    end
    DataBin.HiLoFreqAbsCoefRatio(k,1) = bitor(DataBin.HiLoFreqAbsCoefRatio(k,1), Bit, 'uint8');
end
FieldUnits.HiLoFreqAbsCoefRatio = '';
% bitand(uint8(DataBin.HiLoFreqAbsCoefRatio), uint8(32+16), 'uint8')

%% Penetration Filter Strength : OK (a priory mais valeurs=0)

pattern = 'Penetration Filter Strength';
for k=1:N
    str = strEnrRuntime{k};
    ind = contains(str, pattern);
    val = strtrim(str{ind}(length(pattern)+1:end));
    switch val
        case 'Off'
            Bit = uint8(0);
        case 'Weak'
            Bit = uint8(1);
        case 'Medium'
            Bit = uint8(2);
        case 'Strong'
            Bit = uint8(3);
        otherwise
            my_breakpoint
    end
    DataBin.HiLoFreqAbsCoefRatio(k,1) = bitor(DataBin.HiLoFreqAbsCoefRatio(k,1), Bit, 'uint8');
end
% bitand(uint8(DataBin.HiLoFreqAbsCoefRatio), uint8(3), 'uint8')

%% Slope : OK

pattern = 'Slope';
for k=1:N
    str = strEnrRuntime{k};
    ind = contains(str, pattern);
    val = strtrim(str{ind}(length(pattern)+1:end));
    switch val
        case 'Off'
            Bit = 0;
        case 'On'
            Bit = 4;
        otherwise
            my_breakpoint
    end
    DataBin.FilterIdentifier(k,1) = bitor(DataBin.FilterIdentifier(k,1), Bit, 'uint8');
end
% bitand(uint8(DataBin.FilterIdentifier), uint8(4), 'uint8')

%% Aeration (a priori var valeurs=0)

pattern = 'Aeration';
for k=1:N
    str = strEnrRuntime{k};
    ind = contains(str, pattern);
    val = strtrim(str{ind}(length(pattern)+1:end));
    switch val
        case 'Off'
            Bit = uint8(0);
        case 'On'
            Bit = uint8(32);
        otherwise
            my_breakpoint
    end
    DataBin.FilterIdentifier(k,1) = bitor(DataBin.FilterIdentifier(k,1), Bit, 'uint8');
end
% bitand(uint8(DataBin.FilterIdentifier), uint8(32), 'uint8')

%% Interference (a priori var valeurs=0)

pattern = 'Interference';
for k=1:N
    str = strEnrRuntime{k};
    ind = contains(str, pattern);
    val = strtrim(str{ind}(length(pattern)+1:end));
    switch val
        case 'Off'
            Bit = uint8(0);
        case 'On'
            Bit = uint8(64);
        otherwise
            my_breakpoint
    end
    DataBin.FilterIdentifier(k,1) = bitor(DataBin.FilterIdentifier(k,1), Bit, 'uint8');
end
% bitand(uint8(DataBin.FilterIdentifier), uint8(64), 'uint8')

%% Backscatter Adjustment

%% Normal incidence corr. : OK

pattern = 'Normal incidence corr.'; % Attention, il s'agit bien de ce champ (ind+2 apr�s)
for k=1:N
    str = strEnrRuntime{k};
    ind = (contains(str, pattern));
    val = str2double(str{ind}(length(pattern)+1:end));
    DataBin.TVGCrossAngle(k,1) = val; % TODO JMA : voir comment c'est utilis� (0/1/2 ???)
end
DataBin.TVGCrossAngle = single(DataBin.TVGCrossAngle);
FieldUnits.TVGCrossAngle = 'deg';

%% Use Lambert's law : Nouvelle variable

pattern = 'Use Lambert''s law';
for k=1:N
    str = strEnrRuntime{k};
    ind = contains(str, pattern);
    val = strtrim(str{ind}(length(pattern)+1:end));
    DataBin.UseLambertsLaw(k,1) = strcmp(val, 'On'); % TODO JMA : voir comment c'est utilis� (0/1/2 ???)
end
FieldUnits.UseLambertsLaw = '';

%% TX Power

%% Transmit power level : Nouvelle variable

pattern = 'Transmit power level';
for k=1:N
    str = strEnrRuntime{k};
    ind = contains(str, pattern);
    val = strtrim(str{ind}(length(pattern)+1:end));
    switch val
        case 'Normal'
            val = 1;
        otherwise
            my_breakpoint
    end
    DataBin.TransmitPowerLevel(k,1) = val; % TODO JMA : voir comment c'est utilis� (0/1/2 ???)
end
FieldUnits.TransmitPowerLevel = 'dB';

%% Soft startup : Nouvelle variable

pattern = 'Soft startup';
for k=1:N
    str = strEnrRuntime{k};
    ind = find(contains(str, pattern));
    if ~isempty(ind)
        val = str2double(str{ind}(length(pattern)+1:end));
        DataBin.SoftStartup(k,1) = val; % TODO JMA : voir comment c'est utilis� (0/1/2 ???)
    end
end
FieldUnits.SoftStartup = '';

%% Water Column

%% Water Column (status) : Nouvelle variable

pattern = 'Water Column';
for k=1:N
    str = strEnrRuntime{k};
    ind = contains(str, pattern);
    val = strtrim(str{ind}(length(pattern)+1:end));
    switch val
        case 'Off'
            val = 0;
        case 'On'
            val = 1;
        otherwise
            my_breakpoint
    end
    DataBin.WaterColumnStatus(k,1) = val;
end
FieldUnits.WaterColumnStatus = '';

%% Water Column (Coef) : Nouvelle variable

if DataBin.WaterColumnStatus(k,1) == 1
    pattern = 'Water column'; % Attention : c minuscule
    for k=1:N
        str = strEnrRuntime{k};
        ind = find(contains(str, pattern) & contains(str, 'log R'));
        if ~isempty(ind)
            str = strsplit(str{ind(1)}, ' ');
            val = str2double(str{3});
            DataBin.WaterColumnCoef(k,1) = val; % TODO JMA : 30 log R : d�coder 30
        end
    end
    FieldUnits.WaterColumnCoef = '';

    %% Water Column (Offset)

    pattern = 'Water column'; % Attention : c minuscule
    for k=1:N
        str = strEnrRuntime{k};
        ind = find(contains(str, pattern) & contains(str, 'dB Offset'));
        if ~isempty(ind)
            str = strsplit(str{ind(1)}, ' ');
            val = str2double(str{3});
            DataBin.WaterColumnOffset(k,1) = val; % TODO JMA : 30 log R : d�coder 30
        end
    end
    FieldUnits.WaterColumnOffset = 'dB';

else
    FieldUnits.WaterColumnCoef = '';
    FieldUnits.WaterColumnOffset = 'dB';
    DataBin.WaterColumnCoef(k,1)   = NaN;
    DataBin.WaterColumnOffset(k,1) = NaN;
end

%% Water phase data : Nouvelle variable

pattern = 'Water phase data';
for k=1:N
    str = strEnrRuntime{k};
    ind = find(contains(str, pattern));
    if ~isempty(ind)
        val = strtrim(str{ind}(length(pattern)+1:end));
        switch val
            case 'Off'
                val = 1;
            case 'On' % Ajout JMA le 26/10/2021 pour EM2040 Dual Rx du Belgica
                val = 2;
            case 'Low resolution'
                val = 2;
            case 'High resolution'% Ajout Ridha 08/2023 pour EM304
                val = 3;            

            otherwise
                my_breakpoint
        end
    else
        val = 1;
    end
    DataBin.WaterPhaseData(k,1) = val; % TODO JMA : voir comment c'est utilis� (0/1/2 ???)
end
FieldUnits.WaterPhaseData = '';

%% Special Mode

%% Sonar mode : Nouvelle variable

pattern = 'Sonar mode';
for k=1:N
    str = strEnrRuntime{k};
    ind = contains(str, pattern);
    val = strtrim(str{ind}(length(pattern)+1:end));
    DataBin.SonarMode(k,1) = strcmp(val, 'On');
end
FieldUnits.SonarMode = '';

%% Simulation setup

%% Enable Simulation : Nouvelle variable

pattern = 'Enable Simulation';
for k=1:N
    str = strEnrRuntime{k};
    ind = contains(str, pattern);
    val = strtrim(str{ind}(length(pattern)+1:end));
    DataBin.EnableSimulation(k,1) = strcmp(val, 'On');
end
FieldUnits.EnableSimulation = '';

%% Test � r�aliser sur fichier provenant de vrais .all

%% OperatorStationStatus

DataBin.OperatorStationStatus = zeros([N 1], 'uint8');
FieldUnits.OperatorStationStatus = '';

%% ProcessingUnitStatus

DataBin.ProcessingUnitStatus = zeros([N 1], 'uint8');
FieldUnits.ProcessingUnitStatus = '';

%% BSPStatus

DataBin.BSPStatus = zeros([N 1], 'uint8');
FieldUnits.BSPStatus = '';

% SonarHeadStatus
DataBin.SonarHeadStatus = zeros([N 1], 'uint8');
FieldUnits.SonarHeadStatus = '';

%% TransmitPulseLength

DatetimeRaw = Hdf5Utils.read_value(Hdf5Data.Sonar.Beam_group1.ping_time);
TransmitPulseLengthRaw = Hdf5Utils.read_value(Hdf5Data.Sonar.Beam_group1.Vendor_specific.max_effective_tx_pulse_length);
TransmitPulseLengthRaw = TransmitPulseLengthRaw' * 1000; % Transpos�e ajout�e par JMA le 26/10/2021 pour EM2040 DualRx du Belgica
TransmitPulseLengthRaw = TransmitPulseLengthRaw(:,1); % Ajout JMA le 26/10/2021
DataBin.TransmitPulseLength = my_interp1_Extrap_PreviousThenNext(DatetimeRaw, TransmitPulseLengthRaw, DataBin.Datetime);
DataBin.TransmitPulseLength = single(DataBin.TransmitPulseLength);
FieldUnits.TransmitPulseLength = 'ms';

%% TransmitBeamWidth

TransmitBeamWidth = Hdf5Utils.read_value(Hdf5Data.Sonar.Beam_group1.Vendor_specific.transmit_array_size_used);
TransmitBeamWidth = TransmitBeamWidth'; % Transpos�e ajout�e par JMA le 26/10/2021 pour EM2040 DualRx du Belgica
TransmitBeamWidth = TransmitBeamWidth(:,1); % Ajout JMA le 26/10/2021
DataBin.TransmitBeamWidth = my_interp1_Extrap_PreviousThenNext(DatetimeRaw, TransmitBeamWidth, DataBin.Datetime);
%         DataBin.TransmitBeamWidth = NaN([N 1], 'single'); % 0.6000 valeur affect�e par le convertKmall2All
DataBin.TransmitBeamWidth = single(DataBin.TransmitBeamWidth);
FieldUnits.TransmitBeamWidth = 'deg';

%% TransmitPowerMax

%         DataBin.TransmitPowerMax = zeros([N 1], 'single');
TransmitPowerMax = Hdf5Utils.read_value(Hdf5Data.Sonar.Beam_group1.Vendor_specific.transmit_power);
TransmitPowerMax = TransmitPowerMax'; % Transpos�e ajout�e par JMA le 26/10/2021 pour EM2040 DualRx du Belgica
TransmitPowerMax = TransmitPowerMax(:,1); % Ajout JMA le 26/10/2021
DataBin.TransmitPowerMax = my_interp1_Extrap_PreviousThenNext(DatetimeRaw, TransmitPowerMax, DataBin.Datetime);

DataBin.TransmitPowerMax = single(DataBin.TransmitPowerMax);
FieldUnits.TransmitPowerMax = 'dB';

%% ReceiveBeamWidth : Un peu diff�rent

%         ReceiveBeamWidth = Hdf5Utils.read_value(Hdf5Data.Sonar.Beam_group1.beamwidth_receive_major);
%         ReceiveBeamWidth = Hdf5Utils.read_value(Hdf5Data.Sonar.Beam_group1.vendor_specific.receive_array_size_used);
%         DataBin.ReceiveBeamWidth = my_interp1_Extrap_PreviousThenNext(DatetimeRaw, min(ReceiveBeamWidth), DataBin.Datetime);

% Modif pour Ridha le 18/08/2021
ReceiveBeamWidth = ones(size(DatetimeRaw));
DataBin.ReceiveBeamWidth = my_interp1_Extrap_PreviousThenNext(DatetimeRaw, ReceiveBeamWidth, DataBin.Datetime);
FieldUnits.ReceiveBeamWidth = 'deg';

%% ReceiveBandWidth : Un peu diff�rent

ReceiveBandWidth = Hdf5Utils.read_value(Hdf5Data.Sonar.Beam_group1.Vendor_specific.max_effective_tx_bandwidth);
ReceiveBandWidth = ReceiveBandWidth'; % Transpos�e ajout�e par JMA le 26/10/2021 pour EM2040 DualRx du Belgica
ReceiveBandWidth = ReceiveBandWidth(:,1); % Ajout JMA le 26/10/2021
DataBin.ReceiveBandWidth = my_interp1_Extrap_PreviousThenNext(DatetimeRaw, ReceiveBandWidth, DataBin.Datetime);
DataBin.ReceiveBandWidth = single(DataBin.ReceiveBandWidth);
FieldUnits.ReceiveBandWidth = 'kHz';


%% Cr�ation du XML

Fields = fields(DataBin);

XML.Dimensions.NbSamples = N;

XML.Title              = 'Runtime';
XML.Constructor        = 'Kongsberg';
XML.EmModel            = XSF.getEmModel(Hdf5Data); %str2double(strrep(Hdf5Data.summary.Att.model, 'EM', ''));
XML.TimeOrigin         = '01/01/-4713';
XML.Comments           = 'Sensor sampling rate'; % ne rien mettre apr�s premoers tests
XML.FormatVersion      = 20101118;

XML.SystemSerialNumber = Hdf5Data.Sonar.Att.sonar_serial_number;

Fields = Fields(~contains(Fields, 'Datetime'));
for k=1:length(Fields)
    XML.Signals(k).Dimensions.Name      = 'NbSamples';
    XML.Signals(k).Dimensions.Length    = N;
    XML.Signals(k).Dimensions.Unlimited = 0;

    Values  = DataBin.(Fields{k});
    Storage = class(Values);
    switch Storage
        case 'cl_time'
            Storage = 'double';
            Unit    = 'days since JC';
            Values  = Values.timeMat;
        otherwise
            Unit = FieldUnits.(Fields{k});
    end

    XML.Signals(k).Name      = Fields{k};
    XML.Signals(k).Storage   = Storage;
    XML.Signals(k).Unit      = Unit;
    XML.Signals(k).Populated = 1;
    XML.Signals(k).Stats     = stats(Values);
end

FieldsData = fields(DataBin);
FieldsXML  = fields(XML);
for k=1:length(FieldsXML)
    Data.(FieldsXML{k}) = XML.(FieldsXML{k});
end

for k=1:length(FieldsData)
    Data.(FieldsData{k}) = DataBin.(FieldsData{k});
end

flag = 1;
