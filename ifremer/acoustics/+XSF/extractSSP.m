function [flag, XML, Data] = extractSSP(Hdf5Data)
    
XML  = [];
Data = [];

%% Variables

%% Time : OK

try
    DataBin.DatetimeStartOfUse = Hdf5Utils.read_value(Hdf5Data.Environment.Sound_speed_profile.Vendor_specific.datagram_time);
catch % Voir mail envoy� � Ga�l le 02/11/2021
    DataBin.DatetimeStartOfUse = Hdf5Utils.read_value(Hdf5Data.Environment.Sound_speed_profile.profile_time);
end
DataBin.TimeStartOfUse = cl_time('timeMat', datenum(DataBin.DatetimeStartOfUse));
N = length(DataBin.DatetimeStartOfUse);

% TODO GLU : l'heure est fausse dans XSF mais on la r�cup�re bien dans Vendor_specific
%{
DataBin.Datetime : 1970-01-18 18:43:06.114 (provenant de Hdf5Data.Environment.Sound_speed_profile.time)
DataBin.Datetime : 2018-09-05 22:21:54.434 (provenant de Hdf5Data.Environment.Sound_speed_profile.Vendor_specific.datagram_time)
%}

%% NbEntries

DataBin.NbEntries = Hdf5Utils.read_value(Hdf5Data.Environment.Sound_speed_profile.sample_count);
maxNbEntries = max(DataBin.NbEntries);

%% Depth : OK

try
    Z = Hdf5Utils.read_value(Hdf5Data.Environment.Sound_speed_profile.sample_depth);
catch
    Z = Hdf5Utils.read_value(Hdf5Data.Environment.Sound_speed_profile.depth); % Anciennes versions du XSF
end
DataBin.Depth = NaN(maxNbEntries, N, 'single');
for k=1:N
    if iscell(Z)
        DataBin.Depth(1:DataBin.NbEntries(k),k) = -Z{k};
    else
        DataBin.Depth(1:DataBin.NbEntries(k),1:size(Z,2)) = -Z;
    end
end
% Unit = Hdf5Utils.read_unit(Hdf5Data.Environment.Sound_speed_profile.depth)
% figure; plot(DataBin.Depth, '.k'); grid on;

%% SoundSpeed : OK

C = Hdf5Utils.read_value(Hdf5Data.Environment.Sound_speed_profile.sound_speed);
DataBin.SoundSpeed = NaN(maxNbEntries, N, 'single');
for k=1:N
    if iscell(C)
        DataBin.SoundSpeed(1:DataBin.NbEntries(k),k) = C{k};
    else
        DataBin.SoundSpeed(1:DataBin.NbEntries(k),1:size(C,2)) = C;
    end
end
% Unit = Hdf5Utils.read_unit(Hdf5Data.Environment.Sound_speed_profile.sound_speed)
% figure; plot(DataBin.SoundSpeed, '.r'); grid on;
% figure; plot(DataBin.SoundSpeed, DataBin.Depth, '.r'); grid on;

%% Temperature : OK

T = Hdf5Utils.read_value(Hdf5Data.Environment.Sound_speed_profile.temperature);
DataBin.Temperature = NaN(maxNbEntries, N, 'single');
for k=1:N
    if iscell(T)
        DataBin.Temperature(1:DataBin.NbEntries(k),k) = T{k};
    else
        DataBin.Temperature(1:DataBin.NbEntries(k),1:size(T,2)) = T;
    end
end
% Unit = Hdf5Utils.read_unit(Hdf5Data.Environment.Sound_speed_profile.temperature)
% figure; plot(DataBin.Temperature, '.r'); grid on;

%% Salinity : OK

S = Hdf5Utils.read_value(Hdf5Data.Environment.Sound_speed_profile.salinity);
DataBin.Salinity = NaN(maxNbEntries, N, 'single');
for k=1:N
    if iscell(S)
        DataBin.Salinity(1:DataBin.NbEntries(k),k) = S{k};
    else
        DataBin.Salinity(1:DataBin.NbEntries(k),1:size(S,2)) = S;
    end
end
% Unit = Hdf5Utils.read_unit(Hdf5Data.Environment.Sound_speed_profile.salinity)
% figure; plot(DataBin.salinity, '.r'); grid on;

%% Latitude
%{
% Latitude % TODO GLU : valeur fausse
DataBin.Latitude = Hdf5Utils.read_value(Hdf5Data.Environment.Sound_speed_profile.lat);
% Unit = Hdf5Utils.read_unit(Hdf5Data.Environment.Sound_speed_profile.lat)
% figure; plot(DataBin.Latitude, '.r'); grid on;
%}

%% Longitude

%{
% Longitude % TODO GLU : valeur fausse
DataBin.Longitude = Hdf5Utils.read_value(Hdf5Data.Environment.Sound_speed_profile.lon);
% Unit = Hdf5Utils.read_unit(Hdf5Data.Environment.Sound_speed_profile.lon)
% figure; plot(DataBin.Longitude, '.r'); grid on;
%}

%% PingCounter

DataBin.PingCounter = uint16((1:N)');

%% TimeProfileMeasurement et DatetimeProfileMeasurement

% TODO GLU
DataBin.TimeProfileMeasurement     = DataBin.TimeStartOfUse;
DataBin.DatetimeProfileMeasurement = DataBin.DatetimeStartOfUse;

%% DepthResolution

DataBin.DepthResolution = min(abs(diff(DataBin.Depth)));

%% Ajout JMA le 02/02/2022 pour conformit� avec .all mais c'est bien comme cela est fait ici que �a devrait �tre !

%{
% Mis en commentaire par JMA le 17/04/2024 pour fichier SPFE 0039_20240208_100346_Simon_Stevin.kmall
[n1 ,n2] = size(DataBin.Depth);
if n1 > n2
    DataBin.Depth      = DataBin.Depth';
    DataBin.SoundSpeed = DataBin.SoundSpeed';
    if isfield(DataBin, 'Temperature')
        DataBin.Temperature = DataBin.Temperature';
        DataBin.Salinity    = DataBin.Salinity';
    end
end
%}

%% Cr�ation du XML

XML.Title         = 'SoundSpeedProfile';
XML.Constructor   = 'Kongsberg';
XML.EmModel       = XSF.getEmModel(Hdf5Data);
XML.TimeOrigin    = '01/01/-4713';
XML.Comments      = '';
XML.FormatVersion = 20101118;

XML.Dimensions.nbPoints   = max(DataBin.NbEntries);
XML.Dimensions.nbProfiles = N;

XML.SystemSerialNumber = Hdf5Data.Sonar.Att.sonar_serial_number;

Dimensions(1) = struct('Name', 'nbPoints',   'Length', XML.Dimensions.nbPoints, 'Unlimited', 0);
Dimensions(2) = struct('Name', 'nbProfiles', 'Length', 1,                       'Unlimited', 0);


XML.Signals.Name       = 'TimeStartOfUse';
XML.Signals.Storage    = 'double';
XML.Signals.Unit       = 'days since JC';
XML.Signals.Populated  = 1;
XML.Signals.Stats      = stats(DataBin.TimeStartOfUse.timeMat);
XML.Signals.Dimensions = Dimensions(2);

XML.Signals(end+1).Name     = 'TimeProfileMeasurement';
XML.Signals(end).Storage    = 'double';
XML.Signals(end).Unit       = 'days since JC';
XML.Signals(end).Populated = 1;
XML.Signals(end).Stats      = stats(DataBin.TimeProfileMeasurement.timeMat);
XML.Signals(end).Dimensions = Dimensions(2);

XML.Signals(end+1).Name     = 'PingCounter';
XML.Signals(end).Storage    = 'uint16';
XML.Signals(end).Unit       = '';
XML.Signals(end).Populated  = 1;
XML.Signals(end).Stats      = stats(DataBin.PingCounter);
XML.Signals(end).Dimensions = Dimensions(2);

XML.Signals(end+1).Name     = 'NbEntries';
XML.Signals(end).Storage    = 'int32';
XML.Signals(end).Unit       = '';
XML.Signals(end).Populated  = 1;
XML.Signals(end).Stats      = stats(DataBin.NbEntries);
XML.Signals(end).Dimensions = Dimensions(2);

XML.Signals(end+1).Name     = 'DepthResolution';
XML.Signals(end).Storage    = 'single';
XML.Signals(end).Unit       = 'm';
XML.Signals(end).Populated  = 1;
XML.Signals(end).Stats      = stats(DataBin.DepthResolution);
XML.Signals(end).Dimensions = Dimensions(2);

% Both dimensions for images
XML.Images.Name             = 'Depth';
XML.Images.Storage          = 'single';
XML.Images.Unit             = 'm';
XML.Images.Populated        = 1;
XML.Images.Stats            = stats(DataBin.Depth);
XML.Images(end).Dimensions = Dimensions;

XML.Images(end+1).Name      = 'SoundSpeed';
XML.Images(end).Storage     = 'single';
XML.Images(end).Unit        = 'm/s';
XML.Images(end).Populated   = 1;
XML.Images(end).Stats       = stats(DataBin.SoundSpeed);
XML.Images(end).Dimensions = Dimensions;

XML.Images(end+1).Name      = 'Temperature';
XML.Images(end).Storage     = 'single';
XML.Images(end).Unit        = 'deg';
XML.Images(end).Populated   = 1;
XML.Images(end).Stats       = stats(DataBin.Temperature);
XML.Images(end).Dimensions = Dimensions;

XML.Images(end+1).Name      = 'Salinity';
XML.Images(end).Storage     = 'single';
XML.Images(end).Unit        = '1/1000';
XML.Images(end).Populated   = 1;
XML.Images(end).Stats       = stats(DataBin.Salinity);
XML.Images(end).Dimensions = Dimensions;

FieldsData = fields(DataBin);
FieldsXML  = fields(XML);
for k=1:length(FieldsXML)
    Data.(FieldsXML{k}) = XML.(FieldsXML{k});
end

for k=1:length(FieldsData)
    Data.(FieldsData{k}) = DataBin.(FieldsData{k});
end

flag = 1;
