function [flag, XML, Data] = extractSeabedImage(Hdf5Data, DataRuntimeXSF, varargin)
    
[varargin, BypassSeabedImage] = getPropertyValue(varargin, 'BypassSeabedImage', 0); %#ok<ASGLU> 

XML  = [];
Data = [];

%% Time et Datetime : OK

%         DataBin.Datetime = Hdf5Utils.read_value(Hdf5Data.Environment.Sound_speed_profile.time);
Signals.Datetime = Hdf5Utils.read_value(Hdf5Data.Sonar.Beam_group1.ping_time);
Signals.Time = cl_time('timeMat', datenum(Signals.Datetime));
N = length(Signals.Datetime);

%% PingCounter : OK

Signals.PingCounter = XSF.computePingCounter(Hdf5Data);
% Signals.PingCounter = single(Signals.PingCounter); % ATTENTION : surtout pas de single
% Signals.PingCounter = (1:N)';
% Unit = Hdf5Utils.read_unit(Hdf5Data.Sonar.Beam_group1.Vendor_specific.ping_raw_count) % TODO GLU : pour toutes les unit�s qui sont vides, il vaudrait mieux rendre '' plut�t que []
FieldUnits.PingCounter = '';
% figure; plot(Signals.PingCounter, '.r'); grid on;

%% NbBeams : OK

Signals.NbBeams = Hdf5Utils.read_value(Hdf5Data.Sonar.Beam_group1.Vendor_specific.num_sounding_max_main);

% Ajout JMA le 18/11/2021
Signals.NbBeams(Signals.NbBeams == Hdf5Data.Sonar.Beam_group1.Vendor_specific.num_sounding_max_main.FillValue) = 0;
Signals.NbBeams = single(Signals.NbBeams');
Signals.NbBeams = sum(Signals.NbBeams,2);
% Unit = Hdf5Utils.read_unit(Hdf5Data.Sonar.Beam_group1.Vendor_specific.num_sounding_max_main)
FieldUnits.NbBeams = '';
% figure; plot(Signals.NbBeams, '.r'); grid on;

% Signals.NbBeams(Signals.NbBeams == 65535) = NaN;
nbBeams = sum(max(Signals.NbBeams));

% Recalcul de N ?

%% SystemSerialNumber % TODO GLU

Signals.SystemSerialNumber = repmat(single(Hdf5Data.Sonar.Att.sonar_serial_number), N, 1); % TODO : trouver o� r�cup�rer cette info !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
FieldUnits.SystemSerialNumber = '';

%% SamplingRate : OK

Signals.SamplingRate = Hdf5Utils.read_value(Hdf5Data.Sonar.Beam_group1.Vendor_specific.seabed_image_sample_rate);
Signals.SamplingRate = single(Signals.SamplingRate');
% Unit = Hdf5Utils.read_unit(Hdf5Data.Sonar.Beam_group1.Vendor_specific.seabed_image_sample_rate) % TODO GLU : devrait rendre Hz
FieldUnits.SamplingRate = 'Hz';
% figure; plot(Signals.SamplingRate, '.r'); grid on;

%% TVGN % TODO GLU : je n'ai pas trouv� la correspondance, je recalcule la valeur mais �a ne correspond pas parfaitement, diff�rence de l'ordre de 50 �chantillons

TVGNInSeconds = Hdf5Utils.read_value(Hdf5Data.Sonar.Beam_group1.Bathymetry.detection_two_way_travel_time);
SamplingRate  = Hdf5Utils.read_value(Hdf5Data.Sonar.Beam_group1.Vendor_specific.seabed_image_sample_rate);

TVGNInSeconds = min(TVGNInSeconds);
Signals.TVGN = TVGNInSeconds .* SamplingRate;
Signals.TVGN = single(Signals.TVGN');

% Unit = Hdf5Utils.read_unit(Hdf5Data.Sonar.Beam_group1.Bathymetry.detection_time_varying_gain) 

FieldUnits.TVGN = 'samples';
% my_figure; my_plot(Signals.TVGN, '.r'); grid on;

%% TVGNRecomputed

Signals.TVGNRecomputed = NaN([N, 1], 'single');
FieldUnits.TVGNRecomputed = 'samples';
% figure; plot(Signals.TVGNRecomputed, '.r'); grid on;

%% BSN : OK

Signals.BSN = Hdf5Utils.read_value(Hdf5Data.Sonar.Beam_group1.Bathymetry.Vendor_specific.backscatter_normal_incidence_level);
Signals.BSN = single(Signals.BSN');
% Unit = Hdf5Utils.read_unit(Hdf5Data.Sonar.Beam_group1.Bathymetry.Vendor_specific.backscatter_normal_incidence_level) % TODO GLU : devrait rendre dB
FieldUnits.BSN = 'dB';
% figure; plot(Signals.BSN, '.r'); grid on;

%% BSO : OK

Signals.BSO = Hdf5Utils.read_value(Hdf5Data.Sonar.Beam_group1.Bathymetry.Vendor_specific.backscatter_oblique_incidence_level);
Signals.BSO = single(Signals.BSO');
% Unit = Hdf5Utils.read_unit(Hdf5Data.Sonar.Beam_group1.Bathymetry.Vendor_specific.backscatter_oblique_incidence_level) % TODO GLU : devrait rendre dB
FieldUnits.BSO = 'dB';
% figure; plot(Signals.BSO, '.r'); grid on;

%% TVGCrossOver : OK

Signals.TVGCrossOver = my_interp1_Extrap_PreviousThenNext(DataRuntimeXSF.Datetime, DataRuntimeXSF.TVGCrossAngle, Signals.Datetime);
Signals.TVGCrossOver = single(Signals.TVGCrossOver);

FieldUnits.TVGCrossOver = 'deg';
% figur; plot(Signals.TVGCrossOver, '.r')e; grid on;

%% TxBeamWidth

Signals.TxBeamWidth = NaN([N, 1], 'single');
FieldUnits.TxBeamWidth = 'deg';
% figure; plot(Signals.TxBeamWidth, '.r'); grid on;

%% InfoBeamSortDirection

%{
Images.InfoBeamSortDirection = Hdf5Utils.read_value(Hdf5Data.Sonar.Beam_group1.Bathymetry.detection_z);
Images.InfoBeamSortDirection = single(Images.InfoBeamSortDirection');
%}
Images.InfoBeamSortDirection = ones([N, nbBeams], 'single');
FieldUnits.InfoBeamSortDirection = '';
% figure; imagesc(Images.InfoBeamSortDirection); colormap(jet(256)); colorbar; title('Images.InfoBeamSortDirection')

%% InfoBeamDetectionInfo % TODO GLU

%{
Images.InfoBeamDetectionInfo = Hdf5Utils.read_value(Hdf5Data.Sonar.Beam_group1.Bathymetry.detection_z);
Images.InfoBeamDetectionInfo = single(Images.InfoBeamDetectionInfo');
%}
Images.InfoBeamDetectionInfo = zeros([N, nbBeams], 'single');
FieldUnits.InfoBeamDetectionInfo = '';
% figure; imagesc(Images.InfoBeamDetectionInfo); colormap(jet(256)); colorbar; title('Images.InfoBeamDetectionInfo')

%% InfoBeamCentralSample : OK

Images.InfoBeamCentralSample = Hdf5Utils.read_value(Hdf5Data.Sonar.Beam_group1.Bathymetry.seabed_image_center);
Images.InfoBeamCentralSample = single(Images.InfoBeamCentralSample');
% Unit = Hdf5Utils.read_unit(Hdf5Data.Sonar.Beam_group1.Bathymetry.seabed_image_center) % TODO GLU devrait rendre 'sample'
FieldUnits.InfoBeamCentralSample = 'sample';
% figure; imagesc(Images.InfoBeamCentralSample); colormap(jet(256)); colorbar; title('Images.InfoBeamCentralSample')

%% Entries % TODO JMA : OK mais correction sp�culaire � faire

if BypassSeabedImage
%     seabed_image_center = Hdf5Utils.read_value(Hdf5Data.Sonar.Beam_group1.Bathymetry.seabed_image_start_range);
%     seabed_image_center = seabed_image_center';
%     Images.InfoBeamNbSamples = seabed_image_center - Images.InfoBeamCentralSample;
    Images.InfoBeamNbSamples = 2 * Images.InfoBeamCentralSample + 1;

%     X = Hdf5Utils.read_value(Hdf5Data.Sonar.Beam_group1.sample_count);
%     Images.InfoBeamNbSamples = squeeze(X);
%     Images.InfoBeamNbSamples = Images.InfoBeamNbSamples';
    Images.InfoBeamNbSamples(isnan(Images.InfoBeamNbSamples)) = 0;
    nbSamplesEntries = sum(Images.InfoBeamNbSamples(:));

    Signals.Entries = NaN(nbSamplesEntries, 1, 'single');
    FieldUnits.Entries = 'dB';

    %% InfoBeamNbSamples : OK

    FieldUnits.InfoBeamNbSamples = '';
    Images.InfoBeamNbSamples = uint32(Images.InfoBeamNbSamples);

else
    dsName = '/Sonar/Beam_group1/Bathymetry/seabed_image_samples_r';

    % TODO : if comment� par JMA le 17/06/2024 car bug pour fichiers Marc Roche 0002_20240507_145713.kmall
    % if isfield(Hdf5Data.Sonar.Beam_group1.Bathymetry, 'seabed_image_samples_r')
    %     % Lecture des Seabed a ete faite pr�alablement de fa�on directe et unique via le fichier.
    %     X = XSF.getSeabedImageFromFile(Hdf5Data);
    % else
    % Lecture des Seabed par chunk pour eviter de charger au maximum la m�moire.
    X = XSF.getSeabedImageFromChunk(Hdf5Data.fileName, dsName);
    % end
    % Fin TODO

    %{
    X = Hdf5Utils.read_value(Hdf5Data.Sonar.Beam_group1.Bathymetry.seabed_image_samples_r);
    swVersion   = Hdf5Data.Provenance.Att.conversion_software_version;
    globeVerRef = '2.3.3';
    if verLessThanRef(swVersion, globeVerRef)
        X = X';
    else
        % Les donn�es sont en NPings x N Detections (Beams) x N Samples. On
        % les convertit en Cell pour un traitement calqu� sur le fctt VLen.
        tmp     = permute(X, [2 1 3]);
        tmp     = squeeze(num2cell(tmp,2));
        X       = tmp';
        X       = cellfun(@(x) x', X, 'UniformOutput', false);

        % Suppression du 0 Padding (conservation des valeurs n�gatives).
        XDummy = [];
        for n=1:size(X,1)
            for k=1:size(X,2)
                XDummy{n,k} = X{n,k}(X{n,k} < 0);
            end
        end
        X = XDummy;    
    end
    %}

    Images.InfoBeamNbSamples = cellfun(@(x) numel(x), X);
    nbSamplesEntries = sum(Images.InfoBeamNbSamples(:));
    Signals.Entries = NaN(nbSamplesEntries, 1, 'single');
    kDeb = 1;
    for kPing=1:N
        for kBeam=1:nbBeams
            s = X{kPing, kBeam};
            n = length(s);
            kFin = kDeb + n - 1;
            Signals.Entries(kDeb:kFin) = s;
            kDeb = kDeb + n;
        end
    end
    % Unit = Hdf5Utils.read_unit(Hdf5Data.Sonar.Beam_group1.Bathymetry.seabed_image_samples_r)
    FieldUnits.Entries = 'dB';
    % figure; plot(Signals.Entries); grid on; title('Signals.Entries')
    % my_figure; my_plot(Signals.Entries, '.r'); grid on;

    %% InfoBeamNbSamples : OK

    FieldUnits.InfoBeamNbSamples = '';
    Images.InfoBeamNbSamples = uint32(Images.InfoBeamNbSamples);
    % figure; imagesc(Images.InfoBeamNbSamples); colormap(jet(256)); colorbar; title('Images.InfoBeamNbSamples')
end

%% detection_time_varying_gain : OK

Images.detection_time_varying_gain = Hdf5Utils.read_value(Hdf5Data.Sonar.Beam_group1.Bathymetry.detection_time_varying_gain);
Images.detection_time_varying_gain = single(Images.detection_time_varying_gain');
% Unit = Hdf5Utils.read_unit(Hdf5Data.Sonar.Beam_group1.Bathymetry.detection_time_varying_gain) % TODO GLU devrait rendre 'sample'
FieldUnits.detection_time_varying_gain = 'dB';
% figure; imagesc(Images.detection_time_varying_gain); colormap(jet(256)); colorbar; title('Images.detection_time_varying_gain')

%% Cr�ation du XML

XML.Title         = 'SeabedImage59h';
XML.Constructor   = 'Kongsberg';
XML.EmModel       = XSF.getEmModel(Hdf5Data);
XML.TimeOrigin    = '01/01/-4713';
XML.Comments      = 'Sounder ping rate';
XML.Version       = 'V2';
XML.FormatVersion = 20101118;
XML.IdentAlgoSnippets2OneValuePerBeam = 2;

XML.Dimensions.nbPings          = N;
XML.Dimensions.nbBeams          = nbBeams;
XML.Dimensions.nbSounders       = size(Signals.BSN,2);
XML.Dimensions.nbSamplesEntries = nbSamplesEntries;

% Dans datagramme Installation SN=10001 ?
XML.ListeSystemSerialNumber = Hdf5Data.Sonar.Att.sonar_serial_number;

Fields = fields(Signals);
Fields = Fields(~contains(Fields, 'Datetime')); 
for k=1:(length(Fields)-1)
    Values  = Signals.(Fields{k});
    
    XML.Signals(k).Dimensions.Name      = 'nbPings';
    XML.Signals(k).Dimensions.Length    = N;
    XML.Signals(k).Dimensions.Unlimited = 0;
    
    if size(Values,2) == XML.Dimensions.nbSounders
        XML.Signals(k).Dimensions(2).Name      = 'nbSounders';
        XML.Signals(k).Dimensions(2).Length    = XML.Dimensions.nbSounders;
        XML.Signals(k).Dimensions(2).Unlimited = 0;
    end
    
    Storage = class(Values);
    switch Storage
        case 'cl_time'
            Storage = 'double';
            Unit    = 'days since JC';
            Values  = Values.timeMat;
        otherwise
            Unit = FieldUnits.(Fields{k});
    end

    XML.Signals(k).Name      = Fields{k};
    XML.Signals(k).Storage   = Storage;
    XML.Signals(k).Unit      = Unit;
    XML.Signals(k).Populated = 1;
    XML.Signals(k).Stats     = stats(Values);
end

k = length(Fields);
XML.Signals(k).Dimensions.Name      = 'nbSamplesEntries';
XML.Signals(k).Dimensions.Length    = nbSamplesEntries;
XML.Signals(k).Dimensions.Unlimited = 0;

Values  = Signals.(Fields{k});
Storage = class(Values);
Unit    = FieldUnits.(Fields{k});

XML.Signals(k).Name      = Fields{k};
XML.Signals(k).Storage   = Storage;
XML.Signals(k).Unit      = Unit;
XML.Signals(k).Populated = 1;
XML.Signals(k).Stats     = stats(Values);


Fields = fields(Images);
for k=1:length(Fields)
    XML.Images(k).Dimensions.Name      = 'nbPings';
    XML.Images(k).Dimensions.Length    = N;
    XML.Images(k).Dimensions.Unlimited = 0;
    
    XML.Images(k).Dimensions(2).Name      = 'nbBeams';
    XML.Images(k).Dimensions(2).Length    = nbBeams;
    XML.Images(k).Dimensions(2).Unlimited = 0;
    
    Values  = Images.(Fields{k});
    Storage = class(Values);
    switch Storage
        case 'cl_time'
            Storage = 'double';
            Unit    = 'days since JC';
            Values  = Values.timeMat;
        case 'datetime'
            continue
        otherwise
            Unit = FieldUnits.(Fields{k});
    end
    
    XML.Images(k).Name      = Fields{k};
    XML.Images(k).Storage   = Storage;
    XML.Images(k).Unit      = Unit;
    XML.Images(k).Populated = 1;
    XML.Images(k).Stats     = stats(Values);
    
    %{
    try
        for k2=1:length(DataPosition.Images)
            if strcmp(XML.Images(k2).Name, DataPosition.Images(k2).Name)
                break
            end
        end
        fprintf('XML.Images(%d).Unit = ''%s'';\n', k2, DataPosition.Images(k2).Unit);
    catch
        XML.Images(k).Name;
    end
    %}
end

FieldsXML  = fields(XML);
for k=1:length(FieldsXML)
    Data.(FieldsXML{k}) = XML.(FieldsXML{k});
end

FieldsData = fields(Signals);
for k=1:length(FieldsData)
    Data.(FieldsData{k}) = Signals.(FieldsData{k});
end

FieldsData = fields(Images);
for k=1:length(FieldsData)
    Data.(FieldsData{k}) = Images.(FieldsData{k});
end

flag = 1;
