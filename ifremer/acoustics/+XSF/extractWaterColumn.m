function [flag, XML, Data] = extractWaterColumn(Hdf5Data)
    
XML  = [];
Data = [];

if ~isfield(Hdf5Data.Sonar.Beam_group1.Vendor_specific, 'tvg_function_applied')
    flag = 1;
    return
end

%% Time et Datetime : OK

%         DataBin.Datetime = Hdf5Utils.read_value(Hdf5Data.Environment.Sound_speed_profile.time);
Signals.Datetime = Hdf5Utils.read_value(Hdf5Data.Sonar.Beam_group1.ping_time);
Signals.Time = cl_time('timeMat', datenum(Signals.Datetime));
N = length(Signals.Datetime);

%% PingCounter % TODO GLU : Le nombre de datagrammes de WC peut �tre diff�rent que Depth : il faut absolument r�cup�rer les num�ros de ping WC

% DataDepth.PingCounter
Signals.PingCounter = XSF.computePingCounter(Hdf5Data);
% Signals.PingCounter = single(Signals.PingCounter); % ATTENTION : surtout pas de single
% Signals.PingCounter = (1:N)';
% Unit = Hdf5Utils.read_unit(Hdf5Data.Sonar.Beam_group1.Vendor_specific.ping_raw_count) % TODO GLU : pour toutes les unit�s qui sont vides, il vaudrait mieux rendre '' plut�t que []
FieldUnits.PingCounter = '';
% figure; plot(DataDepth.PingCounter, 'k'); grid on; hold on; plot(Signals.PingCounter, '.r');

%% SystemSerialNumber % TODO GLU

Signals.SystemSerialNumber = repmat(single(Hdf5Data.Sonar.Att.sonar_serial_number), N, 1); % TODO : trouver o� r�cup�rer cette info !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
FieldUnits.SystemSerialNumber = '';

%% NumberOfDatagrams % TODO : est-ce que le convertisseur kmall2Xsf agr�ge les datagrammes multiples si rencontr�s .

Signals.NumberOfDatagrams = ones(N, 1, 'single');
FieldUnits.NumberOfDatagrams = '';

%% DatagramNumbers

Signals.DatagramNumbers = ones(N, 1, 'single');
FieldUnits.DatagramNumbers = '';

%% SoundSpeed : OK

Signals.SoundSpeed = Hdf5Utils.read_value(Hdf5Data.Sonar.Beam_group1.sound_speed_at_transducer);
Signals.SoundSpeed = single(Signals.SoundSpeed);
FieldUnits.SoundSpeed = 'deg';
% figure; plot(Signals.SoundSpeed, '.r'); grid on;

%% SamplingFreq % Ok mais tester si NaN pour savoir s'il y a WC

Signals.SamplingFreq = Hdf5Utils.read_value(Hdf5Data.Sonar.Beam_group1.Vendor_specific.wc_sample_rate);
Signals.SamplingFreq = single(Signals.SamplingFreq');
FieldUnits.SamplingFreq = 'Hz';
% figure; plot(Signals.SamplingFreq, '.r'); grid on;

%% TxTimeHeave % TODO GLU : les valeurs ne correspondent pas

Signals.TxTimeHeave = Hdf5Utils.read_value(Hdf5Data.Sonar.Beam_group1.platform_vertical_offset);
%         Signals.TxTimeHeave = Hdf5Utils.read_value(Hdf5Data.Sonar.Beam_group1.Vendor_specific.tx_heave);
Signals.TxTimeHeave = single(Signals.TxTimeHeave);
FieldUnits.TxTimeHeave = 'm';
% my_figure; my_plot(Signals.TxTimeHeave, '.r'); grid on;

%% TVGFunctionApplied : OK

Signals.TVGFunctionApplied = Hdf5Utils.read_value(Hdf5Data.Sonar.Beam_group1.Vendor_specific.tvg_function_applied);
Signals.TVGFunctionApplied = single(Signals.TVGFunctionApplied)';
FieldUnits.TVGFunctionApplied = '';
% figure; plot(Signals.TVGFunctionApplied, '.r'); grid on;

%% TVGOffset : OK

Signals.TVGOffset = Hdf5Utils.read_value(Hdf5Data.Sonar.Beam_group1.Vendor_specific.tvg_offset);
Signals.TVGOffset = single(Signals.TVGOffset)';
FieldUnits.TVGOffset = '';
% figure; plot(Signals.TVGOffset, '.r'); grid on;

%% FlagPings

Signals.FlagPings = zeros(N, 1, 'single');
FieldUnits.FlagPings = '';

%% TiltAngle : TODO : pas les m�me valeurs 

%         Signals.TiltAngle = Hdf5Utils.read_value(Hdf5Data.Sonar.Beam_group1.tx_beam_rotation_theta);
%         Signals.TiltAngle = Hdf5Utils.read_value(Hdf5Data.Sonar.Beam_group1.rx_beam_rotation_theta);
Signals.TiltAngle = Hdf5Utils.read_value(Hdf5Data.Sonar.Beam_group1.Vendor_specific.raw_tx_beam_tilt_angle);
Signals.TiltAngle = single(Signals.TiltAngle)';
% Unit = Hdf5Utils.read_unit(Hdf5Data.Sonar.Beam_group1.Vendor_specific.raw_tx_beam_tilt_angle)
FieldUnits.TiltAngle = 'deg';
% figure; plot(Signals.TiltAngle, '.r'); grid on;

nbTx = size(Signals.TiltAngle, 2);

%% CentralFrequency

Signals.CentralFrequency = Hdf5Utils.read_value(Hdf5Data.Sonar.Beam_group1.Vendor_specific.center_frequency);
Signals.CentralFrequency = single(Signals.CentralFrequency)';
% Unit = Hdf5Utils.read_unit(Hdf5Data.Sonar.Beam_group1.Vendor_specific.center_frequency)
FieldUnits.CentralFrequency = 'Hz';
% figure; plot(Signals.CentralFrequency, '.r'); grid on;

%%  TransmitSectorNumberTx

Signals.TransmitSectorNumberTx = Hdf5Utils.read_value(Hdf5Data.Sonar.Beam_group1.Vendor_specific.tx_sector_number);
Signals.TransmitSectorNumberTx = single(Signals.TransmitSectorNumberTx)';
% Unit = Hdf5Utils.read_unit(Hdf5Data.Sonar.Beam_group1.Vendor_specific.center_frequency)
FieldUnits.TransmitSectorNumberTx = 'Hz';
% figure; plot(Signals.TransmitSectorNumberTx, '.r'); grid on;

%% Amplitude

X = Hdf5Utils.read_value(Hdf5Data.Sonar.Beam_group1.backscatter_r);
X = squeeze(X)';
%         pppp = arrayfun(@(x) mean(x{:}), X);
%         SonarScope(pppp)

nbBeams = size(X,2);

Images.NumberOfSamples = arrayfun(@(x) length(x{:}), X);
nbSamplesWC = sum(Images.NumberOfSamples(:));
Amplitude = zeros(nbSamplesWC, 1, 'int8');
kDeb = 1;
for kPing=1:N
    for kBeam=1:nbBeams
        s = X{kPing, kBeam};
        n = length(s);
        kFin = kDeb + n - 1;
        Amplitude(kDeb:kFin) = 2 * s;
        kDeb = kDeb + n;
    end
end
FieldUnits.Amplitude = '';
% figure; plot(Amplitude); grid on; title('Amplitude')
clear X

%% TotalOfReceiveBeams

Signals.TotalOfReceiveBeams = repmat(single(nbBeams), N, 1);
FieldUnits.TotalOfReceiveBeams = '';
% figure; plot(Signals.TotalOfReceiveBeams); grid on; title('Signals.TotalOfReceiveBeams')

%% NRx: [84�1 single]

Signals.NRx = repmat(single(nbBeams), N, 1);
FieldUnits.NRx = '';
% figure; plot(Signals.NRx); grid on; title('Signals.NRx')

%% idebAmp

Signals.idebAmp = cumsum(sum(Images.NumberOfSamples,2));
Signals.idebAmp = [0; Signals.idebAmp(1:end-1)] + 1;
FieldUnits.idebAmp = '';
% figure; plot(Signals.idebAmp); grid on; title('Signals.idebAmp')

%% NumberOfSamples : OK

FieldUnits.NumberOfSamples = '';
% figure; imagesc(Images.NumberOfSamples); colormap(jet(256)); colorbar; title('Images.NumberOfSamples')

%% StartRangeNumber : OK mais valeurs toutes � 0, difficile de savoir si c'est vraiment OK

% Amount of time during reception where samples are discarded. The number of discarded sample is given by blanking_interval*sample_interval

sample_interval   = Hdf5Utils.read_value(Hdf5Data.Sonar.Beam_group1.sample_interval);
blanking_interval = Hdf5Utils.read_value(Hdf5Data.Sonar.Beam_group1.blanking_interval);
Images.StartRangeNumber = single(blanking_interval' .* sample_interval);
FieldUnits.StartRangeNumber = 'sample';
% figure; imagesc(Images.StartRangeNumber); colormap(jet(256)); colorbar; title('Images.StartRangeNumber')

%% BeamPointingAngle : OK

Images.BeamPointingAngle = Hdf5Utils.read_value(Hdf5Data.Sonar.Beam_group1.Vendor_specific.raw_rx_beam_pointing_angle);
%         Images.BeamPointingAngle = Hdf5Utils.read_value(Hdf5Data.Sonar.Beam_group1.rx_beam_rotation_phi);
%         Images.BeamPointingAngle = Hdf5Utils.read_value(Hdf5Data.Sonar.Beam_group1.rx_beam_rotation_psi);
%         Images.BeamPointingAngle = Hdf5Utils.read_value(Hdf5Data.Sonar.Beam_group1.rx_beam_rotation_theta);
Images.BeamPointingAngle = -single(Images.BeamPointingAngle');
FieldUnits.BeamPointingAngle = 'deg';
% figure; imagesc(Images.BeamPointingAngle); colormap(jet(256)); colorbar; title('Images.BeamPointingAngle')

nbRx = size(Images.BeamPointingAngle, 2);

%% DetectedRangeInSamples : OK

Images.DetectedRangeInSamples = Hdf5Utils.read_value(Hdf5Data.Sonar.Beam_group1.Vendor_specific.beam_detection_range);
Images.DetectedRangeInSamples = single(Images.DetectedRangeInSamples');
FieldUnits.DetectedRangeInSamples = 'sample';
% figure; imagesc(Images.DetectedRangeInSamples); colormap(jet(256)); colorbar; title('Images.DetectedRangeInSamples')

%% TransmitSectorNumber : OK

Images.TransmitSectorNumber = Hdf5Utils.read_value(Hdf5Data.Sonar.Beam_group1.transmit_beam_index);
Images.TransmitSectorNumber = single(Images.TransmitSectorNumber');
FieldUnits.TransmitSectorNumber = '';
% figure; imagesc(Images.TransmitSectorNumber); colormap(jet(256)); colorbar; title('Images.TransmitSectorNumber')

%% RxBeamNumber

%{
Images.RxBeamNumber = Hdf5Utils.read_value(Hdf5Data.Sonar.Beam_group1.Vendor_specific.raw_receive_transducer_index);
Images.RxBeamNumber = single(Images.RxBeamNumber');
%}
Images.RxBeamNumber = repmat(single(1:nbBeams), N, 1);
FieldUnits.RxBeamNumber = '';
% figure; imagesc(Images.RxBeamNumber); colormap(jet(256)); colorbar; title('Images.RxBeamNumber')

%% DatagramOrder

Images.DatagramOrder = ones(N, nbBeams, 'single');
FieldUnits.DatagramOrder = '';

%% Cr�ation du XML

XML.Title         = 'WaterColumn';
XML.Constructor   = 'Kongsberg';
XML.EmModel       = XSF.getEmModel(Hdf5Data);
XML.TimeOrigin    = '01/01/-4713';
XML.Comments      = 'Sounder ping rate';
XML.Version       = 'V2';
XML.FormatVersion = 20101118;
XML.IdentAlgoSnippets2OneValuePerBeam = 2;

XML.Dimensions.nbPings     = N;
XML.Dimensions.nbTx        = nbTx;
XML.Dimensions.nbRx        = nbRx;
XML.Dimensions.nbSounders  = size(Signals.SamplingFreq,2);
XML.Dimensions.nbSamplesWC = nbSamplesWC;

XML.ListeSystemSerialNumber = Hdf5Data.Sonar.Att.sonar_serial_number;

Signals.Amplitude = Amplitude;
Fields = fields(Signals);
Fields = Fields(~contains(Fields, 'Datetime')); 
for k=1:(length(Fields)-1)
    XML.Signals(k).Dimensions.Name      = 'nbPings';
    XML.Signals(k).Dimensions.Length    = N;
    XML.Signals(k).Dimensions.Unlimited = 0;
    
    Values  = Signals.(Fields{k});
    Storage = class(Values);
    switch Storage
        case 'cl_time'
            Storage = 'double';
            Unit    = 'days since JC';
            Values  = Values.timeMat;
        otherwise
            Unit = FieldUnits.(Fields{k});
    end
        
    if size(Values, 2) == XML.Dimensions.nbSounders
        XML.Signals(k).Dimensions(2).Name      = 'nbSounders';
        XML.Signals(k).Dimensions(2).Length    = XML.Dimensions.nbSounders;
        XML.Signals(k).Dimensions(2).Unlimited = 0;
    elseif size(Values, 2) == nbTx
        XML.Signals(k).Dimensions(2).Name      = 'nbTx';
        XML.Signals(k).Dimensions(2).Length    = nbTx;
        XML.Signals(k).Dimensions(2).Unlimited = 0;
    end
    
    XML.Signals(k).Name      = Fields{k};
    XML.Signals(k).Storage   = Storage;
    XML.Signals(k).Unit      = Unit;
    XML.Signals(k).Populated = 1;
    XML.Signals(k).Stats     = stats(Values);
end

k = length(Fields);
XML.Signals(k).Dimensions.Name      = 'nbSamplesWC';
XML.Signals(k).Dimensions.Length    = nbSamplesWC;
XML.Signals(k).Dimensions.Unlimited = 0;

Values  = Signals.(Fields{k});
Storage = class(Values);
Unit    = FieldUnits.(Fields{k});

XML.Signals(k).Name      = Fields{k};
XML.Signals(k).Storage   = Storage;
XML.Signals(k).Unit      = Unit;
XML.Signals(k).Populated = 1;
NMax = 1e8;
if nbSamplesWC > NMax
    step = ceil(nbSamplesWC / NMax);
    XML.Signals(k).Stats = stats(Values(1:step:nbSamplesWC));
else
    XML.Signals(k).Stats = stats(Values);
end

Fields = fields(Images);
for k=1:length(Fields)
    XML.Images(k).Dimensions.Name      = 'nbPings';
    XML.Images(k).Dimensions.Length    = N;
    XML.Images(k).Dimensions.Unlimited = 0;
    
    XML.Images(k).Dimensions(2).Name      = 'nbRx';
    XML.Images(k).Dimensions(2).Length    = nbRx;
    XML.Images(k).Dimensions(2).Unlimited = 0;
    
    Values  = Images.(Fields{k});
    Storage = class(Values);
    switch Storage
        case 'cl_time'
            Storage = 'double';
            Unit    = 'days since JC';
            Values  = Values.timeMat;
        case 'datetime'
            continue
        otherwise
            Unit = FieldUnits.(Fields{k});
    end
    
    XML.Images(k).Name      = Fields{k};
    XML.Images(k).Storage   = Storage;
    XML.Images(k).Unit      = Unit;
    XML.Images(k).Populated = 1;
    XML.Images(k).Stats     = stats(Values);
    
    %{
    try
        for k2=1:length(DataPosition.Images)
            if strcmp(XML.Images(k2).Name, DataPosition.Images(k2).Name)
                break
            end
        end
        fprintf('XML.Images(%d).Unit = ''%s'';\n', k2, DataPosition.Images(k2).Unit);
    catch
        XML.Images(k).Name;
    end
    %}
end

FieldsXML  = fields(XML);
for k=1:length(FieldsXML)
    Data.(FieldsXML{k}) = XML.(FieldsXML{k});
end

FieldsData = fields(Signals);
for k=1:length(FieldsData)
    Data.(FieldsData{k}) = Signals.(FieldsData{k});
end

FieldsData = fields(Images);
for k=1:length(FieldsData)
    Data.(FieldsData{k}) = Images.(FieldsData{k});
end

flag = 1;
