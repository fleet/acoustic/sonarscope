function EmModel = getEmModel(Hdf5Data)

switch Hdf5Data.summary.Att.model
    case 'EM2040C'
        EmModel = 2045;
    otherwise
        EmModel  = str2double(strrep(Hdf5Data.summary.Att.model, 'EM', ''));
end
