function seabedEntries = getSeabedImageFromChunk(nomFicXsf, dsName)

seabedEntries = [];

fid         = H5F.open(nomFicXsf, 'H5F_ACC_RDONLY', 'H5P_DEFAULT');
dset_id     = H5D.open(fid, dsName);
% plist_id    = H5D.get_create_plist(dset_id);
% [rank, h5_chunk_dims] = H5P.get_chunk(plist_id);

% Définir la sélection pour le sous-ensemble
dspace_id       = H5D.get_space(dset_id);
% Identificatoin des dimensions
[~, h5_dims] = H5S.get_simple_extent_dims(dspace_id);

fillValue   = NaN;
scaleFactor = 1;
addOffset   = 0;
info = H5O.get_info(dset_id);
% Open each attribute, print its name, then close it.
gID = H5G.open(fid,'/Sonar/Beam_group1/Bathymetry/');
for idx = 0:info.num_attrs-1
    attrID = H5A.open_by_idx(gID,'seabed_image_samples_r','H5_INDEX_NAME','H5_ITER_DEC',idx);
    % fprintf("attribute name:  %s\n",H5A.get_name(attrID));
    switch lower(H5A.get_name(attrID))
        case 'scale_factor'
            scaleFactor = double(H5A.read(attrID,'H5ML_DEFAULT'));
        case 'add_offset'
            addOffset   = double(H5A.read(attrID,'H5ML_DEFAULT'));
        case '_fillvalue'
            fillValue   = double(H5A.read(attrID,'H5ML_DEFAULT'));
    end
    H5A.close(attrID);
end
H5G.close(gID);

nbPingTotal     = h5_dims(1);
nbPingToRead    = min([200 fix(nbPingTotal/10)]); % h5_chunk_dims(1); % 
pingStep        = min([nbPingTotal nbPingToRead]);
nbPingStep      = fix(nbPingTotal/pingStep);

varDesc.dset_id     = dset_id;
varDesc.dspace_id   = dspace_id;
varDesc.scaleFactor = scaleFactor;
varDesc.fillValue   = fillValue;
varDesc.addOffset   = addOffset;
varDesc.dims        = h5_dims;

% Lecture par paquet de Ping
str = Lang('Lecture du Seabed Image en paquets de Ping. Attendez, SVP', 'Reading Seabed image via Ping subdatasets, please wait.');
hw = create_waitbar(str, 'N', nbPingStep);
for iPS=1:nbPingStep
    idxPingStart    = pingStep*(iPS-1);
    idxPingEnd      = pingStep*(iPS)-1;

    [flag, dataSeabed] = fcnNested_extractPingFromSeabed(varDesc, nbPingToRead, idxPingStart, idxPingEnd);
    % Convertit l'image des N Ping x N Beams X Samples en series de signals.
    seabedEntries = [seabedEntries; XSF.convertSeabed3DInto2D(dataSeabed)]; %#ok<AGROW>

    % seabedImage(:, :, idxPingStart+1:idxPingEnd+1) = dataSeabed;
    my_waitbar(iPS, nbPingStep, hw);
end
my_close(hw);

% Lecture du dernier paquet de Ping
if (idxPingEnd+1) < nbPingTotal
    % Traitement des N Ping padding hors step.
    idxPingStart    = pingStep*iPS;
    idxPingEnd      = nbPingTotal - 1;
    nbPingToRead    = idxPingEnd - idxPingStart + 1;
    [flag, dataSeabed] = fcnNested_extractPingFromSeabed(varDesc, nbPingToRead, idxPingStart, idxPingEnd);
    % Convertit l'image des N Ping x N Beams X Samples en series de signals.
    seabedEntries = [seabedEntries;XSF.convertSeabed3DInto2D(dataSeabed)];
    % seabedImage(:, :, idxPingStart+1:idxPingEnd+1) = dataSeabed;
end
my_close(hw, 'MsgEnd');

% Fermer les identifiants
H5S.close(dspace_id);
H5D.close(dset_id);
H5F.close(fid);


%% --- Nested function

function [flag, dataSeabed] = fcnNested_extractPingFromSeabed(varDesc, nbPingToRead, idxPingStart, idxPingEnd)

% dataSeabed  = [];
% flag        = -1;
start_indices   = [idxPingStart, 0, 0];  % Indice de début du sous-ensemble
end_indices     = [idxPingEnd, varDesc.dims(2)-1, varDesc.dims(3)-1];  % Indice de fin du sous-ensemble
% ndims           = fliplr(H5S.get_simple_extent_dims(varDesc.dspace_id));
H5S.select_hyperslab(varDesc.dspace_id, 'H5S_SELECT_SET', start_indices, [], end_indices - start_indices + 1, []);

% Créer une dataspace pour les données lues
data_space   = H5S.create_simple(length(start_indices), fliplr(end_indices - start_indices + 1), []);

% Lire les données du sous-ensemble
dataSeabed    = H5D.read(varDesc.dset_id, 'H5ML_DEFAULT', data_space, varDesc.dspace_id, 'H5P_DEFAULT');
% Transformation necessaire avec ce type de lecture, different de la
% lecture d'un seul bloc du dataset.
dataSeabed    = reshape(dataSeabed, [varDesc.dims(3) varDesc.dims(2) nbPingToRead]);
dataSeabed    = double(squeeze(dataSeabed));
dataSeabed(dataSeabed==varDesc.fillValue | dataSeabed==(varDesc.fillValue+1)) = 0; % NaN;
dataSeabed   = dataSeabed * varDesc.scaleFactor + varDesc.addOffset;

flag = 0;

