function seabedImage = getSeabedImageFromFile(Hdf5Data)

% DataDepth.ReflectivityFromSnippets
seabedImage = Hdf5Utils.read_value(Hdf5Data.Sonar.Beam_group1.Bathymetry.seabed_image_samples_r);

swVersion   = Hdf5Data.Provenance.Att.conversion_software_version;
globeVerRef = '2.3.3';
if verLessThanRef(swVersion, globeVerRef)
%     seabedImage = seabedImage';
    seabedImage = my_transpose(seabedImage); % Modif JMA le 17/06/2024
else
    seabedImage = XSF.convertSeabed3DInto2D(seabedImage);
end
