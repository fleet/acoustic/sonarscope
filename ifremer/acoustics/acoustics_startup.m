% Definition des paths de la toolbox "acoustics"
%
% Syntax
%   acoustics_startup(IfrTbxIfr)
% 
% Input Arguments 
%   IfrTbxIfr : 
%
% Examples
%   acoustics_startup(IfrTbxIfr)
%
% Remarks : cette fonction est appel�e dans ifrstartup
%
% See also ifrstartup Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function acoustics_startup(IfrTbxIfr)

% disp('Initialisation de la Toolbox Acoustics');

path(path,fullfile(IfrTbxIfr, 'acoustics', 'backscatter'));
path(path,fullfile(IfrTbxIfr, 'acoustics', 'propagation'));
path(path,fullfile(IfrTbxIfr, 'acoustics', 'bubble'));
path(path,fullfile(IfrTbxIfr, 'acoustics', 'sounder'));
path(path,fullfile(IfrTbxIfr, 'acoustics', 'sounder', 'general'));
% path(path,fullfile(IfrTbxIfr, 'acoustics', 'sounder', 'Cosmos'));
% path(path,fullfile(IfrTbxIfr, 'acoustics', 'sounder', 'Cosmos', 'General'));
% path(path,fullfile(IfrTbxIfr, 'acoustics', 'sounder', 'Cosmos', 'Image'));
% path(path,fullfile(IfrTbxIfr, 'acoustics', 'sounder', 'Cosmos', 'DataProcess', 'AbcProcess'));
% path(path,fullfile(IfrTbxIfr, 'acoustics', 'sounder', 'Cosmos', 'DataProcess', 'MatProcess'));
path(path,fullfile(IfrTbxIfr, 'acoustics', 'sounder', 'Oretech'));
path(path,fullfile(IfrTbxIfr, 'acoustics', 'sounder', 'Sar'));
path(path,fullfile(IfrTbxIfr, 'acoustics', 'sounder', 'Simrad'));
path(path,fullfile(IfrTbxIfr, 'acoustics', 'sounder', 'ExRaw'));
path(path,fullfile(IfrTbxIfr, 'acoustics', 'sounder', 'Reson'));
path(path,fullfile(IfrTbxIfr, 'acoustics', 'sounder', 'Xtf'));
path(path,fullfile(IfrTbxIfr, 'acoustics', 'sounder', 'Rdf'));
path(path,fullfile(IfrTbxIfr, 'acoustics', 'sounder', 'Segy'));
path(path,fullfile(IfrTbxIfr, 'acoustics', 'sounder', 'Segy', 'SegyMAT'));
path(path,fullfile(IfrTbxIfr, 'acoustics', 'sounder', 'Hac'));
path(path,fullfile(IfrTbxIfr, 'acoustics', 'sounder', 'ADCP'));
path(path,fullfile(IfrTbxIfr, 'acoustics', 'sounder', 'Bob'));
path(path,fullfile(IfrTbxIfr, 'acoustics', 'sounder', 'CINNA'));
path(path,fullfile(IfrTbxIfr, 'acoustics', 'sounder', 'Remus'));
path(path,fullfile(IfrTbxIfr, 'acoustics', 'noise'));
path(path,fullfile(IfrTbxIfr, 'acoustics', 'caraibes'));
% R�f�rence aux chemins pour la conversion vers HAC.
path(path,fullfile(IfrTbxIfr, 'acoustics', 'sounder', 'Simrad',  'CONVERT_ssc2hac'));
path(path,fullfile(IfrTbxIfr, 'acoustics', 'sounder', 'general', 'CONVERT_ssc2hac'));
path(path,fullfile(IfrTbxIfr, 'acoustics', 'sounder', 'ExRaw',   'CONVERT_ssc2hac'));
path(path,fullfile(IfrTbxIfr, 'acoustics', 'sounder', 'ExRaw',   'EK80'));
path(path,fullfile(IfrTbxIfr, 'acoustics', 'sounder', 'Reson',   'CONVERT_ssc2hac'));

%% Initialisation de la toolbox "sound_speed"

nomDir = fullfile(IfrTbxIfr, 'acoustics', 'propagation', 'sound_speed');
if exist(nomDir, 'dir')
    path(path, nomDir);
    path(path,fullfile(IfrTbxIfr, 'acoustics', 'propagation', 'sound_speed'));
end

%% Initialisation de la toolbox "pames"

nomDir = fullfile(IfrTbxIfr, 'acoustics', 'pames');
if exist(nomDir, 'dir')
    path(path, nomDir);
    path(path,fullfile(IfrTbxIfr, 'acoustics', 'pames'));
end
