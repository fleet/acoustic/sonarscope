% BS(R, Emission, 'Mask', masque);
function varargout = BS(reflectivite, angle, varargin)

[varargin, masque] = getPropertyValue(varargin, 'Mask', []);
[varargin, anglesCentraux] = getPropertyValue(varargin, 'anglesCentraux', -85:85); %#ok<ASGLU>

%% Contr�le de taille des matrices

sz = size(reflectivite);
if ~isequal(sz, size(angle))
    my_warndlg('backscatter/BS : Les tailles de matrices de reflectivite et d''angle ne correspondent pas', 1);
    return
end

if isempty(masque)
    masque = ones(sz);
end
if ~isequal(sz, size(masque))
    my_warndlg('backscatter/BS : Les tailles de matrices de reflectivite et du masque ne correspondent pas', 1);
    return
end

%% Calcul des limites angulaires

d = diff(anglesCentraux) / 2;
d(end+1) = d(end);
anglesLimites = anglesCentraux - d;
anglesLimites(end+1) = anglesLimites(end) + (anglesLimites(end)-anglesLimites(end-1));

%% Elimination de NaN

sub = ~isnan(reflectivite) & ~isnan(angle) & ~isnan(masque) & (masque ~= 0);
reflectivite = reflectivite(sub);
angle        = angle(sub);
masque       = masque(sub);

labelMin = double(min(masque));
labelMax = double(max(masque));
BSMoyen  = NaN * ones(labelMax, length(anglesCentraux));
NbP      = zeros(labelMax, length(anglesCentraux));

nA = length(anglesLimites)-1;
hw = create_waitbar('Calcul du BS en cours', 'N', nA);
for k=1:nA
    my_waitbar(k, nA, hw)
    
    sub = (angle >= anglesLimites(k)) & (angle < anglesLimites(k+1));
    if isempty(sub)
        continue
    end
    R = reflectivite(sub);
    %     A = angle(sub);
    M = masque(sub);
    for label=labelMin:labelMax
        sub = find(M == label);
        if ~isempty(sub)
            BSMoyen(label, k) = mean(R(sub));
            NbP(label, k) = length(sub);
        end
    end
end
my_close(hw, 'MsgEnd')

%% Par ici la sortie

if nargout == 0
    for label=labelMin:labelMax
        str{label} = num2str(label); %#ok<AGROW>
    end
    figure; plot(anglesCentraux, BSMoyen); grid on;
    legend(str{:})
    figure; plot(abs(anglesCentraux), BSMoyen); grid on;
    legend(str{:})
    figure; plot(anglesCentraux, NbP); grid on;
    legend(str{:})
else
    varargout{1} = BSMoyen;
    varargout{2} = NbP;
    varargout{3} = anglesCentraux;
    varargout{4} = labelMin:labelMax;
end
