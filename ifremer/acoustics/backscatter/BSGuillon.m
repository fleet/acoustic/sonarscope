% Reflectivite angulaire par le modele de Guillon
%
% Syntax
%   [total, ramene, couche] = BSGuillon( angles, Parametres )
%
% Input Arguments
%   angles     : Angles d'incidence (deg) (verticale = 0)
%   Parametres : Tableau contenant les parametres du multicouche
%     F    : Frequence en kHz
%     CEau : Celerite de l'eau (m/s)
%     N    : Nombre de couches
%     N * Parametres de la couche
%       Celerite            : Celerite de la couche (m/s)
%       Densite             : Densite
%       Attenuation         : ie (dB / lambda)
%       Epaisseur           : Epaisseur de la couche (m)
%       Exposant spectral   : ie
%       Puissance spectrale : ie (m-4)
%       Retrodiffusion volumique : ie (dB/m3)
%     Parametres du socle
%       Celerite            : Celerite de la couche (m/s)
%       Densite             : Densite
%       Attenuation         : ie (dB / lambda)
%       Exposant spectral   : ie
%       Puissance spectrale : ie (m-4)
%       Retrodiffusion volumique : ie (dB/m3)
%
% Output Arguments
%   []     : Auto-plot activation
%   total  : Indice total de retrodiffusion.
%   ramene : Indices manenes a la surface des differentes couches.
%   couche : Indices elementaires des differentes couches.
%
% Examples
%   angles = 0:0.1:89;
%   F = 13; CEau = 1500; N = 2;
%   PCouche(1,:) = [1550 1.30 0.20 0.2 3.25 0.0004 -40];
%   PCouche(2,:) = [1650 1.43 0.87 0.1 3.25 0.003  -30];
%   PSocle		 = [1840 2.20 0.90     3.25 0.006  -35];
%   Parametres = [F CEau N PCouche(:)' PSocle];
%
%   BSGuillon(angles, Parametres);
%
%   [total, ramene, couche] = BSGuillon(angles, Parametres);
%     figure
%     plot(angles, total, 'r', 'linewidth', 2); grid on; zoom on; hold on;
%     plot(angles, ramene);
%
% See also BSGuillon1 BSJackson Authors
% Authors : LG + JMA + XL + YHDR
%------------------------------------------------------------------------------

function varargout = BSGuillon(angles, Parametres)

%% Remise en forme des parametres sous forme de tableau bidimensionnel

nbCourbe = size(Parametres,1); % nbre de vecteurs de parametres
nbAngle  = length(angles);	% nbre d'angles

angles = abs(angles);

f(:,1) = Parametres(:,1) * 1000;

n = Parametres(1,3);
if ~isequal(Parametres(:,3),repmat(n, [nbCourbe 1]))
    disp('ATTENTION!!! Fonction BSGuillon: le nombre de couches doit etre identique pour tous les vecteurs de parametres.')
    return
end

c0(:,1)  = Parametres(:,2);
c(:,1:n) = Parametres(:,4:(4+n-1));
c(:,1+n) = Parametres(:,4+7*n);

rho0(:,1)  = ones(nbCourbe,1);
rho(:,1:n) = Parametres(:,4+n:(4+2*n-1));
rho(:,1+n) = Parametres(:,5+7*n);

% beta0 = zeros(nbCourbe,1);
beta(:,1:n) = Parametres(:,4+2*n:(4+3*n-1));
beta(:,1+n) = Parametres(:,6+7*n);

d(:,1:n) = Parametres(:,4+3*n:(4+4*n-1));
d(:,1+n) = zeros(nbCourbe,1);

gam(:,1:n) = Parametres(:,4+4*n:(4+5*n-1));
gam(:,1+n) = Parametres(:,7+7*n);

w2(:,1:n) = Parametres(:,4+5*n:(4+6*n-1));
w2(:,1+n) = Parametres(:,8+7*n);
w2 = w2 .* 1e-8;

sigv(:,1:n) = Parametres(:,4+6*n:(4+7*n-1));
sigv(:,1+n) = Parametres(:,9+7*n);

angles = reshape(angles, 1, 1, nbAngle);

N = n + 1;

%% Indices de surface

a  = c ./ repmat(c0, [1 N]);
ro = rho ./ repmat(rho0, [1 N]);

[kir, rc, ind_surf_nu] = jacksurf(angles, a, ro, beta, gam, w2, f, c0); %#ok<ASGLU>
coef_surf_nu = reflec_dB2Enr( ind_surf_nu );

%% Indices de volume

mv = reflec_dB2Enr( sigv );

coef_vol_nu = retro_vol(c0, rho0, c, rho, beta, mv, 100, angles, f);

%% Indices individuels complets

couche = reflec_Enr2dB( coef_vol_nu + coef_surf_nu );
couche = squeeze(couche);

%% Calcul des coefficients de passage

[Phi, R, T, Cpl] = calcul_Cpl(f, n, c0, c, rho0, rho, beta, d, angles); %#ok<ASGLU>

Cp = Cpl;
[crit, Rsoc, Tsoc] = interface(f, c(:,(N-1):N), rho(:,(N-1):N), beta(:,(N-1):N), real(Phi(:,N,:))); %#ok<ASGLU>
R(:,N+1,:) = Rsoc;


%--------------------------------------
% Modifications des indices individuels
%--------------------------------------

%% Limitation du volume.

h(:,1:N-1) = d(:,1:N-1);
h(:,N) = repmat(100, [nbCourbe 1]);

c_ant = [c0,c(:,1:N-1)];
rho_ant = [rho0,rho(:,1:N-1)];

coef_vol_mod = retro_vol( c_ant, rho_ant, c, rho, beta, ...
    mv, h, real(Phi(:,1:N,:)), f);

%% Modification de l'indice de surface.

a = c ./ c_ant;
ro = rho ./ rho_ant;
[kir, rc, ind_surf_mod] = jacksurf_moe(real(Phi(:,1:N,:)),...
    a, ro, beta, gam, w2, f, c_ant, R(:,1:N,:)); %#ok<ASGLU>
coef_surf_mod = reflec_dB2Enr( ind_surf_mod );

%% Indice de couche.

coef_mod = coef_surf_mod + abs(coef_vol_mod);

%% Calcul de l'index total.

coef_ramene = Cp .* coef_mod;
ramene = reflec_Enr2dB(coef_ramene);
ramene = squeeze(ramene);

coef_total = sum(coef_ramene,2);
coef_total = squeeze(coef_total);

if (nbCourbe == 1)
    coef_total = coef_total.';
end

total  = reflec_Enr2dB(coef_total);
angles = squeeze(angles);
total  = squeeze(total);
ramene = squeeze(ramene);
couche = squeeze(couche);

%% Sortie des param�tres ou traces graphiques

if nargout == 0
    legende{1} = 'Total';
    for k=1:(N-1)
        legende{1+k} = sprintf('Couche%d', k); %#ok<AGROW>
    end
    legende{end+1} = 'socle';
    
    figure
    
    % Indices individuels
    
    subplot(1,2,1)
    plot(angles, total, 'r', 'linewidth', 2); grid on; zoom on; hold on;
    plot(angles, couche); hold off;
    legend(legende);
    xlabel('Angle d''incidence (deg)'); ylabel('BS (dB)');
    title('Indices Individuels');
    YLim1 = get(gca, 'YLim');
    
    % Indices ramenes
    
    subplot(1,2,2)
    plot(angles, total, 'r', 'linewidth', 2); grid on; zoom on; hold on;
    plot(angles, ramene); hold off;
    legend(legende);
    xlabel('Angle d''incidence (deg)'); ylabel('BS (dB)');
    title('Indices ramenes');
    YLim2 = get(gca, 'YLim');
    
    % Meme plage verticale sur les 2 graphiques
    
    YLim = [min(YLim1(1), YLim2(1)) max(YLim1(2), YLim2(2))];
    subplot(1,2,1)
    set(gca, 'YLim', YLim);
    subplot(1,2,2)
    set(gca, 'YLim', YLim);
    
else
    varargout{1} = total;
    varargout{2} = ramene;
    varargout{3} = couche;
end



%----------------------------------------------------------------------------
%				SOUS-FONCTIONS
%----------------------------------------------------------------------------

%--------------------------------------------------------------------------
% Calcule l'indice de retrodiffusion de surface par le modele de Jackson
% suivant l'article de Mourad et Jackson (references dans jackcal.m).
%
% [kirchhoff, rug_comp, surface] = jacksurf(phi, a, rho, beta, gam, w2, freq, c)
%    phi:  angle d'incidence en degre.
%    a:    rapport des celerites.
%    rho:  rapport des masses volumiques.
%    beta: coefficient d'absorption en dB/lambda.
%    gam:  exposant spectral.
%    w2:   "force spectrale".
%    freq: frequence en Hertz.
%    c:    celerite dans le milieu incident en m/s.
%--------------------------------------------------------------------------

function [kirchhoff, rug_comp, surface] = jacksurf(phi, a, rho, beta, gam, w2, freq, c)

nbCourbe = size(rho,1);
N = size(rho,2);
nbAngle = size(phi,3);  %nombre d'elements dans le vecteur phi

phirad = phi .* (pi./180);

%---------------------------------------------
% Definitions des donnees annexes necessaires.

ho = 0.01;  % longueur de reference en cm
alpha = gam./2 - 1;
k = 2*pi.* freq ./ c;
delta = beta.*log(10)./(40*pi);
kappa = (1./a) .* (1+1i.*delta);

x1 = (( 2*pi.* w2 .* ho.^(-gam) ).^(1./alpha)) ./ (2.*(1-alpha));
x2 = (repmat(k.^2, [1 N])./alpha).^((1-alpha)./alpha);
s2=x1.*x2;
s=sqrt(s2);

num = (2*pi) .* w2 .* gamma(2-alpha) .* (2.^(-2.*alpha));
deno = (ho.^gam) .* alpha .*(1-alpha) .* gamma(1+alpha);
Ch = num./deno;


%-----------------------------------
% CALCUL DE L'INDEX DE RETRODIFUSION
% DE L'INTERFACE EAU-SEDIMENT.(mts)
%-----------------------------------

[sig1,C4] = sig_kir(phirad, alpha, Ch, k, rho, kappa);
sig2 = sig_cr(phirad, kappa, rho, k, ho, gam, w2, s);


%------------------------------------
% Raccordement des deux contributions

tetakb=(1./C4+4).^(-0.25);
tetakb=acos(tetakb);
tetakb=pi/2-tetakb;
x=80.*(repmat(sin(phirad), [nbCourbe N 1]) - repmat(sin(tetakb), [1 1 nbAngle]));
fx=1./(1+exp(x));
mts=fx.*sig1+(1-fx).*sig2;


% passage en index de retrodiffusion
kirchhoff=10*log10(sig1);
rug_comp=10*log10(sig2);
surface=10*log10(mts);



%--------------------------------------------------------------------------
% Coefficient de retrodiffusion de surface suivant l'approximation de Kirchhoff
% (article de Mourad et Jackson)
%
% [back_kir, C4] = sig_kir(phi, alpha, Ch, k, rho, kappa)
%    phi   :   angle d'incidence en radian.
%    alpha : parametre alpha du modele
%    Ch    : parametre Ch du modele
%    k     :     nombre d'onde incident.
%    rho   :   rapport des masses volumiques.
%    kappa : rapport du nombre d'onde du sediment sur celui de l'eau.
%--------------------------------------------------------------------------
function [back_kir, C4] = sig_kir(phi, alpha, Ch, k, rho, kappa)


nbAngle = size(phi,3);
nbCourbe = size(alpha, 1);
N = size(alpha,2);

if size(phi,1) == 1
    phi = repmat(phi, [nbCourbe 1 1]);
end

if size(phi,2) == 1
    phi = repmat(phi, [1 N 1]);
end

if size(k,2) == 1
    k = repmat(k, [1 N]);
end

% calcul des parametres intermediaires.
num  = 8.*alpha.^2.*gamma((1+alpha)./(2.*alpha));
deno = gamma(0.5).*gamma(1./alpha).*gamma(1./(2.*alpha));
a    = (num./deno).^(2.*alpha);
b    = a.^((alpha+1)./(2*alpha)).*gamma(1./alpha)./(2.*alpha);
qc   = Ch.*(2.^(1-2.*alpha)).*(k.^(2.*(1-alpha)));

% index de retrodiffusion "de Kirchhoff"
R_normal = (rho-kappa)./(rho+kappa);
haut     = b.*qc.*(abs(R_normal)).^2;
bas      = sin(phi).^(repmat(4*alpha, [1 1 nbAngle])) + repmat(a.*(qc.^2), [1 1 nbAngle]) .* cos(phi).^4;
back_kir = repmat(haut, [1 1 nbAngle]) ./ (8.*pi.*bas.^(repmat((1+alpha)./(2*alpha), [1 1 nbAngle])) );

% parametre C4
C4 = (1000).^(1./(1+alpha)).*(a.*qc.^2).^(1./alpha);


%--------------------------------------------------------------------------
% Coefficient de retrodiffusion de surface suivant le modele de rugosite
% composee (article de Mourad et Jackson)
%
% back_cr = sig_cr(phi, kappa, rho, k, ho, gam, w2, s)
%    phi:   angle d'incidence en radian.
%    kappa: rapport du nombre d'onde du sediment sur celui de l'eau
%    rho:   rapport des masses volumiques.
%    k:     nombre d'onde incident.
%    ho:    parametre ho du modele (longueur de reference)
%    gam:   exposant spectral.
%    w2:    puissance spectrale.
%    s:     ecart-type des pentes de la rugosite grande echelle.
%
%--------------------------------------------------------------------------
function back_cr = sig_cr(phi, kappa, rho, k, ho, gam, w2,s)


nbAngle  = size(phi,3);
nbCourbe = size(kappa, 1);
N = size(kappa,2);


S = zeros(nbCourbe,N,nbAngle);
Q = zeros(nbCourbe,N,nbAngle);

% calcul de S(phi,s)
t = zeros(nbCourbe, N, nbAngle);
ind = find(phi == 0);

if ~isempty(ind)
    t(:, :, ind) = Inf([nbCourbe N 1]);
end

if ind == 1
    t(:, :, 2:nbAngle) = repmat(cot(phi(2:nbAngle)), [nbCourbe N 1]) ./ repmat(s, [1 1 nbAngle-1]);
elseif ind == nbAngle
    t(:, :, 1:nbAngle-1) = repmat(cot(phi(1:nbAngle-1)), [nbCourbe N 1]) ./ repmat(s, [1 1 nbAngle-1]);
else
    t(:, :, [1:ind-1,ind+1:nbAngle]) = repmat(cot(phi([1:ind-1,ind+1:nbAngle])), [nbCourbe N 1]) ./ repmat(s, [1 1 nbAngle-1]);
end

T = t < 2.1;
new_t = T.*t;

new_t = reshape(new_t, [(nbCourbe*N*nbAngle) 1]);
ind   = find(new_t~=0);

Q      = reshape(Q, [(nbCourbe*N*nbAngle) 1]);
Q(ind) = pi .^ (-0.5) .* exp(-new_t(ind).^2) - new_t(ind) .* (1-erf(new_t(ind))) ./ (4.*new_t(ind));
S      = reshape(S, [(nbCourbe*N*nbAngle) 1]);
S(ind) = (1-exp(-2.*Q(ind)))./(2.*Q(ind));

ind = find(new_t==0);
S(ind) = ones(size(ind));

% Q = reshape(Q, [nbCourbe N nbAngle]);
S = reshape(S, [nbCourbe N nbAngle]);

S(:,:,1) = NaN([nbCourbe N 1]);

% calcul de F(phi,sig_pr,s)
phi_neg = 1.224745.*atan(s);
phi_pos = -phi_neg;
a_neg   = 0.295410;
a_pos   = a_neg;
a_nul   = 1.181636;

som1 = a_neg .* sig_pr(repmat(phi, [nbCourbe N 1])+repmat(phi_neg, [1 1 nbAngle]),kappa,rho,k,ho,gam,w2);
som2 = a_nul .* sig_pr(phi,kappa,rho,k,ho,gam,w2);
som3 = a_pos .* sig_pr(repmat(phi, [nbCourbe N 1])+repmat(phi_pos, [1 1 nbAngle]),kappa,rho,k,ho,gam,w2);

som = som1 + som2 + som3;

F = pi .^ (-0.5) .* som;

back_cr = S .* F;



%--------------------------------------------------------------------------
% Coefficient de retrodiffusion d'interface par les petites perturbations
% suivant le modele de Kuo (article de Mourad et Jackson). Utilise comme
% argument dans le calcul de la rugosite composee.
%
% sigma_pr = sig_pr(phi, kappa, rho, k, ho, gam, w2)
%    phi:   angle d'incidence en radian.
%    kappa: rapport du nombre d'onde du sediment sur celui de l'eau
%    rho:   rapport des masses volumiques.
%    k:     nombre d'onde incident.
%    ho:    parametre ho du modele (longueur de reference)
%    gam:   exposant spectral.
%    w2:    puissance spectrale.
%--------------------------------------------------------------------------
function sigma_pr = sig_pr(phi,kappa,rho, k,ho,gam,w2)

nbAngle = length(phi);
nbCourbe = size(kappa, 1);
N = size(kappa,2);

if size(phi,1) == 1
    phi = repmat(phi, [nbCourbe 1 1]);
end
if size(phi,2) == 1
    phi = repmat(phi, [1 N 1]);
end

P = sqrt(repmat(kappa.^2, [1 1 nbAngle])-sin(phi).^2);
Y = (repmat((rho-1.).^2, [1 1 nbAngle]) .* sin(phi).^2+repmat(rho.^2-kappa.^2, [1 1 nbAngle])) ./ ((repmat(rho, [1 1 nbAngle]) .* cos(phi) + P).^2);
K = sqrt(repmat(4.*k.^2, [1 N nbAngle]) .* sin(phi).^2 + repmat((k./10).^2, [1 N nbAngle]) );
W = repmat(w2, [1 1 nbAngle]) .* (1./(ho.*K)).^repmat(gam, [1 1 nbAngle]);
sigma_pr = repmat(4*k.^4, [1 N nbAngle]) .* cos(phi).^4 .* (abs(Y)).^2 .* W;



%------------------------------------------------------------------------------
% Calcul de l'indice de la contribution du volume par le biais de l'indice
% de retrodiffusion volumique equivalent d'interface.
%
% ind_vo = retro_vol(c1,ro1,beta1,c2,ro2,beta2,mv,h,phi,f)
%	c, ro ... : parametres acoustiques des deux milieux
% 	mv : coefficient de retrodiffusion volumique
%	h : epaisseur de la strate
%	phi : angle d'incidence en degre
%	f : frequence en Hz
%------------------------------------------------------------------------------
function ind_vol = retro_vol(c1, rho1, c2, rho2, beta2, mv, h, phi, f)

if beta2==0
    disp('Attention ! l''amortissement dans le sediment est nul.')
    disp('Il y a un probl�me de d�finition pour le coeff. equivalent.')
end

nbCourbe = size(c2,1);
N = size(c2,2);
nbAngle = size(phi,3);

if size(c1,2) == 1
    c1 = repmat(c1, [1 N]);
end

if size(rho1,2) == 1
    rho1 = repmat(rho1, [1 N]);
end

if size(h,1) == 1
    h = repmat(h, [nbCourbe 1]);
end
if size(h,2) == 1
    h = repmat(h, [1 N]);
end

if size(phi,1) == 1
    phi = repmat(phi, [nbCourbe 1 1]);
end
if size(phi,2) == 1
    phi = repmat(phi, [1 N 1]);
end


%------------------
% Variables locales.

ome = (2 * pi) .* f;
k1 = repmat(ome, [1 N]) ./ c1;

k2i = (beta2.*repmat(f,[1 N])) ./ (8.686.*c2);
k2 = (repmat(ome, [1 N]) ./ c2) + 1i*k2i;

phirad = phi * pi ./ 180;
% phi2rad = asin( repmat(k1, [1 1 nbAngle]) .* sin(phirad) ./ repmat(k2, [1 1 nbAngle]) );

% nu = c2 ./ c1;

%--------------------------------------------------
% Nombres d'onde, impedances et coef. de reflexion

kx = repmat(k1, [1 1 nbAngle]) .* sin(phirad);
k1z = sqrt( repmat(k1.^2, [1 1 nbAngle]) - kx.^2);
k2z = sqrt( repmat(k2.^2, [1 1 nbAngle]) - kx.^2);

Z1 = repmat(rho1 .* repmat(ome, [1 N]), [1 1 nbAngle]) ./ k1z;
Z2 = repmat(rho2 .* repmat(ome, [1 N]), [1 1 nbAngle]) ./ k2z;

R = (Z2 - Z1) ./ (Z1 + Z2);

%-------------------------------------------------
% Coefficient de retrodiff vol. equi. d'interface

P = k2z ./ repmat(k1, [1 1 nbAngle]);
limvol = 1 - exp( - 4 .* repmat(h, [1 1 nbAngle]) .* imag(k2z) ); %limitation du volume

num = ( abs(1 - R.^2) ).^2  .* cos(phirad).^2 .* repmat(mv, [1 1 nbAngle]) .* limvol;
deno = repmat(4 .* k1, [1 1 nbAngle]) .* (abs(P)).^2 .* imag(P);
ind_vol = num ./ deno;



%------------------------------------------------------------------------------
% Calcul des coefficients de passage pour l'indice de retrodiffusion ramene.
% Attention, le calcul est fait pour un multicouche numerote a partir de l'eau,
% i.e. le socle est la strate numero N. Les expressions ne sont donc pas formel-
% lement celles de la these. Le programme coefficient_passage.m du repertoire
% /retrodiff/sauve_multi/ presente le calcul de la these.
%
% [Phi, R, T, Cpl] = calcul_Cpl(f,n,c,rho,beta,d,phi)
%    f : frequence en Hz
%    n : nombre de couches (p.e. une strate surmontant un socle, n = 1)
%    c, rho, beta : caracteristiques acoustiques du multicouche
%    d : vecteur des epaisseurs.
%    phi : angle d'incidence en degres
%
%    Phi : Tableau des incidences dans chaque strate.
%    R : Tableau des coefficients de reflexion pour chaque strate.
%    T : Coefficient de transmission eau/socle
%    Cpl : Tableau des coefficients de passage pour chaque couche.
%
%------------------------------------------------------------------------------
function [Phi, R, T, Cpl] = calcul_Cpl(f, n, c0, c, rho0, rho, beta, d, phi)

% Initialisations
N = n+ 1;
nbAngle = size(phi,3);
nbCourbe = size(c,1);
phi = phi .* (pi/180);
ome = (2*pi) .* f;

%***********************************************************************
%			CALCUL DES VECTEURS ET TABLEAUX INITIAUX
%***********************************************************************
ki = (beta.*repmat(f*log(10), [1 N])) ./ (20.*c);
k0 = ome ./ c0;
k = (ome ./ c0);
k(:,2:N+1) = (repmat(ome, [1 N]) ./ c) + 1i.*ki;
kx = repmat(ome./c0, [1 N+1 nbAngle]) .* repmat(sin(phi), [nbCourbe N+1 1]);  %projection sur x du vecteur d'onde.

Phi(:,1:N+1,:) = asin( repmat(repmat(k0, [1 1 nbAngle]) .* repmat(sin(phi), [nbCourbe 1 1]), [1 N+1 1]) ./ repmat(k, [1 1 nbAngle]) );

kz = sqrt(repmat(k.^2, [1 1 nbAngle])-kx.^2);

Z = repmat(rho0 .* ome, [1 1 nbAngle]) ./ kz(:,1,:);
Z(:,2:N+1,:) = repmat((rho .* repmat(ome, [1 N])), [1 1 nbAngle]) ./ kz(:,2:N+1,:);
teta = kz(:,2:N+1,:) .* repmat(d, [1 1 nbAngle]);
s = tan(teta);

% Td = ( 2 .* Z(:,N,:) ) ./ ( Z(:,N+1,:) + Z(:,N,:) );


%***********************************************************************
%  CALCUL DES COEF DE REFLEXION ET DE TRANSMISSION DU MULTICOUCHE ENTIER
%***********************************************************************
Zin = Z(:,N+1,:);
T = exp(1i*teta(:,N,:)) .* 2 .* Z(:,N+1,:) ./ (Z(:,N+1,:)+Z(:,N,:));
R = zeros(nbCourbe,N,nbAngle);

for ind=N:-1:2
    R(:,ind,:) = (Zin - Z(:,ind,:)) ./ (Zin + Z(:,ind,:));
    Zin = Z(:,ind,:).*(Zin-1i*Z(:,ind,:).*s(:,ind-1,:))./(Z(:,ind,:)-1i*Zin.*s(:,ind-1,:));
    T = T .* exp(1i*teta(:,ind-1,:)).*(Zin+Z(:,ind,:))./(Zin+Z(:,ind-1,:));
end

R(:,1,:) = (Zin - Z(:,1,:)) ./ (Zin + Z(:,1,:));


%***********************************************************************
% 	CALCUL DE LA MATRICE DES COEFFICIENTS DE PASSAGE
%***********************************************************************
Cpl = zeros(nbCourbe,N,nbAngle);
Cpl(:,1,:) = ones(nbCourbe,1,nbAngle);
A=T.*(Z(:,N+1,:)+Z(:,N,:))./(2.*Z(:,N+1,:));
B=T.*(Z(:,N+1,:)-Z(:,N,:))./(2.*Z(:,N+1,:));

for l = N:-1:2
    
    %-------------
    % Calcul de Tl et Zin
    
    Zin=Z(:,1,:);
    Tl=2.*Z(:,1,:)./(Z(:,1,:)+Z(:,2,:));
    
    for ind = 2:l
        Zin=Z(:,ind,:).*(Zin-1i*Z(:,ind,:).*s(:,ind-1,:))./(Z(:,ind,:)-1i*Zin.*s(:,ind-1,:));
        Tl=Tl.*exp(1i*teta(:,ind-1,:)).*(Zin+Z(:,ind,:))./(Zin+Z(:,ind+1,:));
    end
    
    %------------
    % Calcul de A
    
    if l<N
        int1=A.*exp(-1i*teta(:,l,:)).*(Z(:,l+1,:)+Z(:,l,:))./(2*Z(:,l+1,:));
        int2=B.*exp(1i*teta(:,l,:)).*(Z(:,l+1,:)-Z(:,l,:))./(2*Z(:,l+1,:));
        A=int1+int2;
        int3=A.*exp(-1i*teta(:,l,:)).*(Z(:,l+1,:)-Z(:,l,:))./(2*Z(:,l+1,:));
        int4=B.*exp(1i*teta(:,l,:)).*(Z(:,l+1,:)+Z(:,l,:))./(2*Z(:,l+1,:));
        B=int3+int4;
    end
    
    Tloc = ( 2 * Z(:,l,:) ) ./ ( Z(:,l+1,:) + Z(:,l,:) );
    
    Cpl(:,l,:) = (abs(A)) .^2 .* ( abs ( Tl./Tloc) ).^2;
    
end

% ---------------------------------
% RECONVERSION DES ANGLES EN DEGRES

Phi=Phi*180./pi;



%------------------------------------------------------------------------------
% Calcul de l'angle critque et des coefficients de reflexion et  transmission
% d'une simple interface.
%
% [crit, R, T] = interface(f,c1,rho1,beta1,c2,rho2,beta2,phi)
%    f : frequence en Hz
%    c, rho, beta : caracteristiques acoustiques (1 : incidence, 2 : refraction)
%    phi: angle d'incidence en degre
%
%------------------------------------------------------------------------------
function [crit, R, T] = interface(f,c,rho,beta,phi)

c1 = c(:,1);
rho1 = rho(:,1);
beta1 = beta(:,1);
c2 = c(:,2);
rho2 = rho(:,2);
beta2 = beta(:,2);

nbAngle = size(phi,3);

ome = (2*pi) .* f;
a=c2./c1;
[ind1,ind2] = find(a>1);
crit(ind1,ind2) = asin(1./a(ind1,ind2)) .* (180/pi);
[ind1,ind2] = find(~(a>1));
nb = length(ind1);
crit(ind1,ind2) = repmat(90, [nb 1]);

k1i = (beta1.*f)./(8.686.*c1); %coefficient d'attenuation pour la pression
k1  = (ome./c1)+1i.*k1i;
k2i = (beta2.*f)./(8.686.*c2);
k2  = (ome./c2)+1i.*k2i;

phirad = phi .* (pi/180);


%--------------------------------------------------------
% Calcul des coefficients de reflexion et de transmission

kx  = repmat(k1, [1 1 nbAngle]) .* sin(phirad);
k1z = sqrt(repmat(k1.^2, [1 1 nbAngle]) - kx.^2);
k2z = sqrt(repmat(k2.^2, [1 1 nbAngle]) - kx.^2);

R = (repmat(rho2, [1 1 nbAngle]) .* k1z - repmat(rho1, [1 1 nbAngle]) .* k2z) ./ (repmat(rho2, [1 1 nbAngle]) .* k1z+repmat(rho1, [1 1 nbAngle]) .* k2z); % coef. de reflexion
T = 1+R;  % coef. de transmission.



%--------------------------------------------------------------------------
% Calcule l'indice de retrodiffusion de surface par le modele de Jackson et Moe
% suivant l'article de Moe et Jackson (First-order perturbation solution for
% rough surface scattering cross section including the effects of gradients,
% JASA, 96 (3), pp 1748-1754, 1994).
%
% [kirchhoff, rug_comp, surface] = jacksurf_moe(phi, a, rho, beta, gam, w2, freq, c, Ri)
%    phi:  angle d'incidence en degre.
%    a:    rapport des celerites.
%    rho:  rapport des masses volumiques.
%    beta: coefficient d'absorption en dB/lambda.
%    gam:  exposant spectral.
%    w2:   "force spectrale".
%    freq: frequence en Hertz.
%    c:    celerite dans le milieu incident en m/s.
%    Ri:   coefficient de reflexion du milieu sous-jacent.
%--------------------------------------------------------------------------
function [kirchhoff, rug_comp, surface] = jacksurf_moe(phi, a, ro, beta, gam, w2, freq, c, Ri)

% NbCourbe = size(c,1);
N = size(c,2);
nbAngle = size(phi,3);

phirad=phi .* (pi/180);

% -------------------------------------------
% Definitions des donnees annexes necessaires

ho=0.01;  % longueur de reference en cm
alpha=gam./2-1;
k=repmat((2*pi) .* freq, [1 N]) ./ c;
delta = beta.*(log(10)/(40*pi));
kappa=(1./a).*(1+1i.*delta);
x1=(((2*pi).*w2.*ho.^(-gam)).^(1./alpha))./(2.*(1-alpha));
x2=(k.^2./alpha).^((1-alpha)./alpha);
s2=x1.*x2;
s=sqrt(s2);
num=(2*pi) .*w2.*gamma(2-alpha).*2.^(-2*alpha);
deno=ho.^gam.*alpha.*(1-alpha).*gamma(1+alpha);
Ch=num./deno;

%-----------------------------------
% CALCUL DE L'INDEX DE RETRODIFUSION
% DE L'INTERFACE EAU-SEDIMENT.(mts)
%-----------------------------------

[sig1,C4] = sig_kir(phirad,alpha,Ch,k,ro,kappa);
sig2 = sig_cr_moe(phirad, kappa, ro, k, ho, gam, w2, s, Ri);

%-----------------------------------
% Raccordement des deux contribution

tetakb=(1./C4+4).^(-0.25);
tetakb=acos(tetakb);
tetakb=pi/2-tetakb;
x = 80.*( sin(phirad)-repmat(sin(tetakb), [1 1 nbAngle]) );
fx=1./(1+exp(x));
mts=fx.*sig1+(1-fx).*sig2;


% passage en index de retrodiffusion
kirchhoff=10.*log10(sig1);
rug_comp=10.*log10(sig2);
surface=10.*log10(mts);



%--------------------------------------------------------------------------
% Coefficient de retrodiffusion de surface suivant le modele de rugosite
% composee modifie par Moe et Jackson
%
% back_cr= sig_cr(phi,kappa,rho,k,ho,gam,w2,s)
%    phi:   angle d'incidence en radian.
%    kappa: rapport du nombre d'onde du sediment sur celui de l'eau
%    rho:   rapport des masses volumiques.
%    k:     nombre d'onde incident.
%    ho:    parametre ho du modele (longueur de reference)
%    gam:   exposant spectral.
%    w2:    puissance spectrale.
%    s:     ecart-type des pentes de la rugosite grande echelle.
%    Ri :   coefficient de reflecion sous-jacent
%
%--------------------------------------------------------------------------
function back_cr = sig_cr_moe(teta, kappa, ro, k, ho, gam, w2, s, Ri)

nbAngle = size(teta,3);
nbCourbe = size(kappa,1);
N = size(kappa,2);

S=zeros(nbCourbe,N,nbAngle);
Q=zeros(nbCourbe,N,nbAngle);

% calcul de S(teta,s)
nbTotal = nbCourbe*N*nbAngle;
teta = reshape(teta, [nbTotal 1]);

ind = find(teta==0);
jnd = find(teta~=0);
nbZero = length(ind);

t(ind) = Inf([nbZero 1]);

sminu = repmat(s, [1 1 nbAngle]);
sminu = reshape(sminu, [nbTotal 1]);
t(jnd) = cot(teta(jnd)) ./ sminu(jnd);

t = reshape(t, [nbCourbe N nbAngle]);
teta = reshape(teta, [nbCourbe N nbAngle]);

T=t<2.1;
new_t=T.*t;

new_t = reshape(new_t, [nbTotal 1]);

ind = find(new_t~=0);
Q = reshape(Q, [nbTotal 1]);
% [ind1,ind2,ind3]=find(new_t~=0);
Q(ind)=(pi^(-0.5)).*exp(-new_t(ind).^2)-new_t(ind).*(1-erf(new_t(ind)))./(4.*new_t(ind));

S = reshape(S, [nbTotal 1]);
S(ind)=(1-exp(-2.*Q(ind)))./(2.*Q(ind));

ind = find(new_t==0);
S(ind) = ones(size(ind));

% Q = reshape(Q, [nbCourbe N nbAngle]);
S = reshape(S, [nbCourbe N nbAngle]);

S(:,:,1) = NaN([nbCourbe N 1]);

% calcul de F(teta,sig_pr,s)
teta_neg=1.224745.*atan(s);
teta_pos=-teta_neg;
a_neg=0.295410;
a_pos=a_neg;
a_nul=1.181636;
som1=a_neg.*sig_pr_moe(teta+repmat(teta_neg, [1 1 nbAngle]),kappa,ro,k,ho,gam,w2,Ri);
som2=a_nul.*sig_pr_moe(teta,kappa,ro,k,ho,gam,w2,Ri);
som3=a_pos.*sig_pr_moe(teta+repmat(teta_pos, [1 1 nbAngle]),kappa,ro,k,ho,gam,w2,Ri);
som=som1+som2+som3;
F=(pi^(-0.5)).*som;

back_cr = S.*F;



%--------------------------------------------------------------------------
% Coefficient de retrodiffusion d'interface par les petites perturbations
% suivant le modele de Moe et Jackson.
%
% sigma_pr = sig_pr(phi,kappa,rho, k,ho,gam,w2)
%    phi:   angle d'incidence en radian.
%    kappa: rapport du nombre d'onde du sediment sur celui de l'eau
%    rho:   rapport des masses volumiques.
%    k:     nombre d'onde incident.
%    ho:    parametre ho du modele (longueur de reference)
%    gam:   exposant spectral.
%    w2:    puissance spectrale.
%    Ri:    coefficient de reflexion du milieu sous-jacent.
%--------------------------------------------------------------------------
function sigma_pr = sig_pr_moe(phi,kappa,rho, k,ho,gam,w2,Ri)

nbAngle = size(phi,3);

K = sqrt( repmat((4*k.^2), [1 1 nbAngle]) .* sin(phi).^2 + repmat((k./10).^2, [1 1 nbAngle]) );
W = repmat(w2, [1 1 nbAngle]) .* (1./(ho.*K)).^repmat(gam, [1 1 nbAngle]);

part1 = (k.^4) ./ 4;
part2 = (abs ( 1+ Ri )) .^ 4;
rap   = ( 1-Ri ) ./ ( 1+Ri );
part3 = ( sin(phi) ).^2 + repmat(rho, [1 1 nbAngle]) .* (cos(phi)).^2 .* rap .^2;
part4 = ( abs( repmat(( 1 - ((kappa.^2)./rho) ), [1 1 nbAngle]) + repmat(( 1 - 1./rho ), [1 1 nbAngle]) .* part3) ).^2;

sigma_pr = repmat(part1, [1 1 nbAngle]) .* part2 .* part4 .* W;


