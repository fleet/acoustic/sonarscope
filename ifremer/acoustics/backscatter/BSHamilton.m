% Description
%   Hamilton Angular Backscatter model
%
% Syntax
%   [reflec, composantes] = BSHamilton(angles, Parametres)
%
% Input Arguments
%   angle       : Incidence angles (deg) (verticale = 0)
%   Parametres  : Tableau contenant les parametres du modele
%		AmplSpecul  : Amplitude de BS en speculaire (en dB/m2)
%		LargSpecul  : Demi-largeur du lobe speculaire deg)
%		AmplLambert : Cte de Lambert (dB/m2)
%		CoefLambert : Coefficient de la puissance du cos(theta)
%   Parametres optionnels
%		AmplTransitoire : Amplitude de BS en speculaire (dB/m2)
%		LargTransitoire : Demi-largeur du lobe speculaire (deg)
%
% Output Arguments
%   []          : Auto-plot activation
%   reflec      : Indice de retrodiffusion total.
%   composantes : [lambert; speculaire; transitoire] ou [lambert; speculaire].
%
% Examples
%   angles = -80:80;
%   BSHamilton(angles, [300 5]);
%
%   reflec = BSHamilton(angles, [300 5]);
%   plot(angles, reflec); grid on; zoom on; hold on;
%
% Authors : JMA
% See also BSJackson Authors
%
% Reference pages in Help browser
%   <a href="matlab:doc BSHamilton">BSHamilton</a>
%------------------------------------------------------------------------------

function [reverb, composantes] = BSHamilton(angles, Parametres)

Frequence  = Parametres(1);
Index      = Parametres(2);
Parametres = refHamilton(Frequence);
[reverb, composantes] = BSJackson(angles, Parametres(Index, :));

%% Sortie des param�tres ou trac�s graphiques

if nargout == 0
    FigUtils.createSScFigure;
    if size(reverb, 1) == 1
        PlotUtils.createSScPlot(angles, composantes); grid on;  hold on;
        PlotUtils.createSScPlot(angles, reverb, 'black', 'LineWidth', 2); grid on;
        legend({'Kirchhoff'; 'Rugosite composee'; 'medium Roughness';'large Roughness'; 'Surface';'Volume'; 'Total'});
        
    else
        PlotUtils.createSScPlot(angles, reverb); grid on;
    end
    xlabel('Incidence angles (deg)', 'FontSize', 13); ylabel('BS (dB)', 'FontSize', 13);
    title(['Jackson''s Model (Frequency : ' num2str(Frequence(1)) ' kHz)'], 'FontSize', 15);
end
