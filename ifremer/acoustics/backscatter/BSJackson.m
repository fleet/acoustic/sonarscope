% Calcul de la reflectivite angulaire par le modele de Jackson
%
% Syntax
%   [total, composantes] = BSJackson( angles, [Frequence, c_eau, nu, ro, gam, w2, sigma2, beta] )
%
% Input Arguments
%   angle       : angle d'incidence (deg) (verticale = 0)
%   Parametres  : Tableau contenant les parametres du modele
%     Frequence : Frequence (kHz)
%     c_eau     : Celerite dans l'eau (m/s)
%     nu        : Rapport des celerites (c_sed/c_eau)
%     ro        : Rapport des densites (rho_sed/rho_eau)
%     gam       : Exposant spectral
%     w2        : Puissance spectrale
%     sigma2    : Indice de volume (dB)
%     beta      : Attenuation dans le sediment (dB/lambda).
%
% Output Arguments
%   []          : Auto-plot activation
%   total       : Indice total de retrodiffusion.
%   composantes : kirchhoff; rug_comp; interfaceRoughness; volume.
%
% Examples
%   angles = 0:89;
%   Frequence=100; c_eau=1520; nu = 0.98; rho = 1.7; gam=3.5; w2=0.003; sigma2=-27; beta=40*pi/log(10)*(0.01);
%   Parametres1 = [Frequence, c_eau, nu, rho, gam, w2, sigma2, beta];
%
%   BSJackson(angles, Parametres1);
%
%   [total, composantes] = BSJackson( angles, Parametres1);
%   figure; plot(angles, composantes); grid on; zoom on; hold on;
%   plot(angles, total, 'black'); grid on; zoom on;
%
%   Parametres2 = [95, c_eau, c_sed/c_eau, rho_sed/rho_eau, gam, w2, -40, beta];
%   BSJackson( angles, [Parametres1; Parametres2]);
%
% See also BSLurton Lambert BSJacksonModel Authors
% Authors : LG + YHDR + GLC + JMA
%------------------------------------------------------------------------------

function [total, composantes] = BSJackson(angles, Parametres)

Frequence = Parametres(:,1);
c_eau     = Parametres(:,2);
nu        = Parametres(:,3);
ro        = Parametres(:,4);
gam       = Parametres(:,5);
w2        = Parametres(:,6);
sigma2    = Parametres(:,7);
beta      = Parametres(:,8);

%% Definitions et calcul des donnees annexes necessaires

% Transformation du vecteur des angles d'incidence en vecteur colonne
% d'angles rasant
subposi = find(angles >=0);
subnega = find(angles < 0);

anglesRasant(subposi) = (90-angles(subposi))' * pi / 180;
anglesRasant(subnega) = (90+angles(subnega))' * pi / 180;
anglesRasant = abs(anglesRasant); % "Redressement" pour les angles negatifs

nbangles = length(anglesRasant);
nbCourbes = size(Parametres, 1) ;

% Preparation du produit tensoriel
% vecteur colonnes (resp: lignes) pour les angles (resp: parametres)
oneAngles = ones(1, nbangles);
oneParam  = ones(nbCourbes, 1);
AnglesRasant = oneParam * anglesRasant;

% Calculs Trigonometriques
sinAngles = oneParam * sin(anglesRasant);
cosAngles = oneParam * cos(anglesRasant);
Ws = warning;
warning off
tanAngles = oneParam * tan(anglesRasant);
warning(Ws)

% Calculs des exposants utils
alpha = (gam / 2) - 1;
if any((alpha <= 0) | (alpha >= 1))
    my_warndlg('Condition sur alpha en defaut.', 0);
end
UnPlusAlpha	 = 1 + alpha;
UnSurAlpha	 = 1./ alpha;
UnMoinsAlpha = 1 - alpha;
DeuxAlpha	 = 2 * alpha;

% Nombre d'onde en cm
kcm = 2 * pi * Frequence * 1e3./ c_eau /100;

% Calcul du Parameter Loss
delta = beta * 0.01832338997199; % 0.01832338997199 = log(10) / (40 * pi);

% Rapport des nombres d'ondes
% kappa = (1 ./ nu) .* (1 + 1i*delta);
delta(delta == 0) = eps;
kappa = complex(1, delta) ./ nu;
Kappa = kappa * oneAngles;

% Transformation des parametres en vecteur colonnes
Ro     = ro     * oneAngles;
Kcm    = kcm    * oneAngles;
Gam    = gam    * oneAngles;
W2     = w2     * oneAngles;
Nu     = nu     * oneAngles;
Sigma2 = sigma2 * oneAngles;
Delta  = delta  * oneAngles;

%----------------------------------------------------------------------------
% CALCUL DU COEF. DE RETRODIFUSION DE L'INTERFACE EAU-SEDIMENT.
% (sig_mr = InterfaceRoughness)
%----------------------------------------------------------------------------

%-----------------------------------------------------------------------------
% Calcul de la contribution de KIRCHHOFF dans le cadre du modele de Jackson.
%-----------------------------------------------------------------------------

%% Calcul des parametres intermediaires.

num_Ch  = (2*pi) .* w2 .* gamma(2-alpha) .* (2 .^ (-DeuxAlpha));
deno_Ch = alpha .* UnMoinsAlpha .* gamma(UnPlusAlpha);
Ch   = num_Ch ./ deno_Ch;

Gam1SurAlpha = gamma(UnSurAlpha);
num_a  = 8 * (alpha .^ 2) .* gamma(UnPlusAlpha ./ DeuxAlpha);
deno_a = gamma(0.5) .* Gam1SurAlpha .* gamma(1 ./ DeuxAlpha);
a  = ( num_a ./ deno_a ) .^ DeuxAlpha;

b = a .^ (UnPlusAlpha ./ DeuxAlpha) .* Gam1SurAlpha ./ DeuxAlpha;

qc = Ch .* (2.^(1 - DeuxAlpha)) .* (kcm .^ (2 .* UnMoinsAlpha));

%% Coeff de r�trodiffusion de Kirchhoff

R_normal = (ro - kappa) ./ (ro + kappa);  % Coef de reflexion en incidence normale
haut = (b .* qc .* (( abs(R_normal) ).^2)) * oneAngles;

%% D�but du calcul tensoriel

bas  = cosAngles .^ (4 .* (alpha * oneAngles)) ...
    + ((a .* (qc .^ 2)) * oneAngles) .* (sinAngles .^ 4);

sig_k = haut ./ ( 8  * pi * bas .^ (( UnPlusAlpha ./ DeuxAlpha ) * oneAngles ) );

sig_k(AnglesRasant < 40*pi/180) = 0;

%% Calcul de la contribution de RUGOSITE COMPOSEE dans le cadre du modele de
% Jackson.

x1 = ((2*pi * w2) .^ UnSurAlpha) ./ (2 * UnMoinsAlpha);%%%%%%%%%%%%%%%%%%%%
x2 = ((kcm .^ 2) ./ alpha) .^ (UnMoinsAlpha ./ alpha);
s2 = x1 .* x2;
largeScaleRmsSlope = sqrt(s2);
LargeScaleRmsSlope = largeScaleRmsSlope * oneAngles;


%% calcul de S(teta,s) (fonction d'ombre).

t = (1 ./ LargeScaleRmsSlope) .* tanAngles;
subt  = find(t < 2.1); % Condition numerique donnee par Jackson himself
t = t(subt);

Qcr = (pi^(-0.5) .* exp(-t .^ 2) - t .* (1 - erf(t))) ./ (4 * t);
Scr = (1 - exp(-2 .* Qcr)) ./ (2 .* Qcr);

%% calcul de F(teta,sig_pr, LargeScaleRmsSlope)

teta_s = largeScaleRmsSlope* oneAngles;  %atan(largeScaleRmsSlope)* oneAngles ;
teta_neg= 1.224745 * teta_s ;
teta_pos= -teta_neg;
a_neg	= 0.295410;
a_pos	= a_neg;
a_nul	= 1.181636;

tetamoinsun = AnglesRasant - teta_neg; % cf Jackson
tetamoinsun(tetamoinsun < 0    ) = 0;
tetamoinsun(tetamoinsun > pi/2 ) = pi/2;

%test_neg=  find(tetamoinsun < 0);      %
%tetamoinsun(test_neg) = 0;             %
%test_pos=  find(tetamoinsun> pi/2);    %
%tetamoinsun(test_pos) = pi/2;          %

tetaplusun = AnglesRasant - teta_pos; %
tetaplusun(tetaplusun < 0    ) = 0;
tetaplusun(tetaplusun > pi/2 ) = pi/2;


%test_neg=  find(tetamoinsun < 0);      %
% tetaplusun(test_neg) = 0;            %
%test_pos=  find(tetamoinsun> pi/2);    %
% tetaplusun(test_pos) = pi/2;         %

som1	= a_neg * Sigma_pr(tetamoinsun , Kappa, Ro, Kcm, Gam, W2);
som2	= a_nul * Sigma_pr(AnglesRasant, Kappa, Ro, Kcm, Gam, W2);
som3	= a_pos * Sigma_pr(tetaplusun  , Kappa, Ro, Kcm, Gam, W2);
som		= som1 + som2 + som3;
Fcr		= (pi ^ (-0.5)) * som;

sig_cr = Fcr;
sig_cr(subt) = Scr .* Fcr(subt);

%% Raccordement des deux contributions.

C4 = 1000 .^ ( 1 ./ UnPlusAlpha ) .* ( a .* (qc.^2) ) .^ UnSurAlpha ;
cosTetaKdB = (1 ./ C4 + 4) .^ (-0.25) * oneAngles;
x = 80 .* (cosAngles - cosTetaKdB);
% sub40_neg = find( x < -40);
% sub40_pos = find( x > 40);
sub40_neg = (x < -40);
sub40_pos = (x > 40);

fx = 1./ ( 1 + exp( x ) );
fx(sub40_neg) = 1;
fx(sub40_pos) = 0;

sig_mr = fx .* sig_k + ( 1 - fx ) .* sig_cr ;  % coefficient interfaceRoughness.

%% Angle Critique

teta_c = (2.5613 / 180 *pi)* oneParam ;
subnu = find(nu >= 1.001);
teta_c(subnu) = acos(1./nu(subnu));
teta_c = teta_c * oneAngles;

m = 0.7263.*(LargeScaleRmsSlope).^(-1/3);

num_sigma1 = 0.04682*(LargeScaleRmsSlope.^1.25).*(Nu.^3.25).*( (1-2./Ro).*(Nu.^-2) +1).^2;
deno_sigma1 = 1+( 3.54*teta_s./teta_c);
sigma1 =  num_sigma1./deno_sigma1;

aa = sin(pi./( 1+( 0.81*(teta_c./AnglesRasant).^2 ) ));
x_sig_lr = sigma1.* (aa).^m;
y_sig_lr = 0.0260*abs(R_normal*oneAngles).^2./(LargeScaleRmsSlope.^2);
y_sig_lr = y_sig_lr ./( 1+( (AnglesRasant-pi/2)./teta_s ).^2 /2.6 ).^1.9 ;
y_sig_lr = y_sig_lr./( 1 + 0.81*(teta_c./AnglesRasant).^2 );
sig_lr = x_sig_lr +  y_sig_lr ;

delta_teta = 0.5*pi/180;
teta_r = 7*pi/180;
y = (atan(LargeScaleRmsSlope) - teta_r)/delta_teta;
fy = 1./ ( 1 + exp( y ) );
sig_r = fy.* sig_mr + (1 - fy).*sig_lr;

%----------------------------------------------------------------------------
%	      CALCUL DU COEF DE RETRODIFFUSION DU VOLUME. (mtv).
%----------------------------------------------------------------------------

%-----------------------------------------------------------------------------
% Calcul de la contribution du volume dans le cadre du modele de Jackson.
%-----------------------------------------------------------------------------


%% Definitions preliminaires.

som1v	= a_neg .* Sigma_pv(tetamoinsun, Kappa, Ro, Nu, Sigma2, Delta);
som2v	= a_nul .* Sigma_pv(AnglesRasant,			Kappa, Ro, Nu, Sigma2, Delta);
som3v	= a_pos .* Sigma_pv(tetaplusun, Kappa, Ro, Nu, Sigma2, Delta);
somv	= som1v + som2v + som3v;
Fv		= (pi ^ (-0.5)) * somv;

mtv = Fv;
mtv(subt) = Scr .* Fv(subt);

%% COEFFICIENT TOTAL DE RETRODIFFUSION

mtot = mtv + sig_r;

%% passage en indice de retrodiffusion

kirchhoff       = reflec_Enr2dB(sig_k);
rug_comp        = reflec_Enr2dB(sig_cr);
mediumRoughness = reflec_Enr2dB(sig_mr);
largeRoughness  = reflec_Enr2dB(sig_lr);
surface         = reflec_Enr2dB(sig_r);
volume          = reflec_Enr2dB(mtv);
total           = reflec_Enr2dB(mtot);

composantes = [kirchhoff; rug_comp; mediumRoughness; largeRoughness; surface; volume];

%% Sortie des param�tres ou trac�s graphiques

if nargout == 0
    FigUtils.createSScFigure;
    if size(total, 1) == 1
        PlotUtils.createSScPlot(angles, composantes); grid on;  hold on;
        PlotUtils.createSScPlot(angles, total, 'black', 'LineWidth', 2); grid on;
        legend({'Kirchhoff'; 'Rugosite composee'; 'medium Roughness';'large Roughness'; 'Surface';'Volume'; 'Total'});
    else
        PlotUtils.createSScPlot(angles, total); grid on;
    end
    xlabel('Incidence angles (deg)', 'FontSize', 13); ylabel('BS (dB)', 'FontSize', 13);
    title(['Jackson''s Model (Frequency : ' num2str(Frequence(1)) ' kHz)'], 'FontSize', 15);
end



%----------------------------------------------------------------------------
%				SOUS-FONCTIONS
%----------------------------------------------------------------------------


function sigma_pr = Sigma_pr(teta, kappa, ro, k, gam, w2)
P = sqrt( kappa .^ 2 - cos(teta).^2 );
Y = ((ro-1) .^2  .* cos(teta).^2 + ro.^2-kappa.^2 ) ...
    ./ ((ro .* sin(teta)+P) .^2 );
K = sqrt((4*k .^2 ) .* cos(teta).^2 + (k./10).^2);
W = w2 .* (1./K) .^ (gam) ;
sigma_pr = 4 * ((k .* sin(teta)) .^ 4) .* (abs(Y)) .^ 2 .* W ;



function sigma_pv = Sigma_pv(teta, kappa, ro, nu, sigma2, delta)
P = sqrt( kappa .^2  - cos(teta).^2 );
y = (ro .* sin(teta)) ./ P;
R = (y-1) ./ (y+1) ;
numerateur = 5*delta .* sin(teta).^2 .* reflec_dB2Enr(sigma2) .* (abs(1-R.^2)).^2 ;
deno = nu .* log(10) .* (abs(P)).^2.*imag(P);
sigma_pv = numerateur ./ deno;
