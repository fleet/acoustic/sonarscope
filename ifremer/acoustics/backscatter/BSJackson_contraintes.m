% Reflectivite angulaire par le modele de Jackson avec contraintes
%
% Syntax
%   [total, composantes] = BSJackson_contraintes( angles, Parametres1);
%
% Input Arguments 
%   angle       : angle d'incidence (deg) (verticale = 0)
%   Parametres  : Tableau contenant les parametres du modele
%     Frequence : Frequence (kHz)
%     C_eau     : Celerite dans l'eau (m/s)
%     Phi       : Bulk Grain Size (Phi)
%     gam       : Exposant spectral (fixe a 3.25)
%     sigma2    : Indice de volume (dB)
%
% Output Arguments
%   []          : Auto-plot activation
%   total       : Indice total de retrodiffusion.
%   composantes : kirchhoff; rug_comp; interfaceRoughnes; volume.
%
% Examples 
%   angles = 0:89;
%   Frequence=100; C_eau=1520; Phi =  -1; sigma2=-30; 
%   Parametres1 = [Frequence, C_eau, Phi, sigma2];
%
%   BSJackson_contraintes( angles, Parametres1);
%
%   [total, composantes] = BSJackson_contraintes( angles, Parametres1);
%   figure; plot(angles, composantes); grid on; zoom on; hold on;
%   plot(angles, total, 'black'); grid on; zoom on;
%
%   Parametres2 = [95, C_eau, Phi, -40];
%   BSJackson( angles, [Parametres1; Parametres2]);
%   
%   Phi = -1:.5:9; clear Parametres
%   for i = 1:length(Phi)
%     Parametres(i, 1) = 100;
%     Parametres(i, 2) = 1520;
%     Parametres(i, 3) = Phi(i);
%     Parametres(i, 4) = sigma2;
%   end
%   BSJackson_contraintes( angles, Parametres)
%
% See also BSLurton CalculParaJackson
% Authors : LG + YHDR + GLC + JMA
%------------------------------------------------------------------------------

function varargout = BSJackson_contraintes(angles, Parametres)

if nargin == 0
	return
end

Frequence = Parametres(:,1);
C_eau	  = Parametres(:,2);
Phi		  = Parametres(:,3);
[rho, nu, beta, delta, w2] = CalculParaJackson(Phi, C_eau); %#ok<ASGLU>
gam	   = 3.25*ones(size(Parametres,1),1);
sigma2 = Parametres(:,4);

%% D�finitions et calcul des donn�es annexes n�cessaires.

anglesRadian = angles(:)' * pi / 180;	% On met le vecteur angles en colonne
anglesRadian = abs(anglesRadian); 		% "Redressement" pour les angles negatifs
nbangles = length(angles);

nbCourbes = size(Parametres, 1) ;

oneAngles = ones(1, nbangles);
oneParam  = ones(nbCourbes, 1);

% AnglesRadian = oneParam * anglesRadian;
sinAngles = oneParam * sin(anglesRadian);
cosAngles = oneParam * cos(anglesRadian);
Ws = warning;
warning off
cotAngles = oneParam * cot(anglesRadian);
warning(Ws)

alpha = (gam / 2) - 1;
kcm = 20 * pi * Frequence ./ C_eau;  % Nombre d'onde en cm
delta = beta * 0.01832338997199;	% 0.01832338997199 = log(10) / (40 * pi);

% kappa = (1 ./ nu) .* (1 + 1i*delta);
delta(delta == 0) = eps;
kappa = complex(1, delta) ./ nu;
Kappa = kappa * oneAngles;
Ro = rho * oneAngles;
Kcm = kcm * oneAngles;
Gam = gam * oneAngles;
W2 = w2 * oneAngles;
Rapcel = nu * oneAngles;
Sigma2 = sigma2 * oneAngles;
Delta = delta * oneAngles;

UnPlusAlpha		= 1+alpha;
UnSurAlpha		= 1./alpha;
UnMoinsAlpha	= 1-alpha;
DeuxAlpha		= 2*alpha;

x1 = ( (2*pi * w2) .^ UnSurAlpha) ./ (2 * UnMoinsAlpha);
x2 = ( (kcm .^ 2) ./ alpha) .^ ( UnMoinsAlpha ./ alpha);
s2 = x1 .* x2;
largeScaleRmsSlope = sqrt(s2);
LargeScaleRmsSlope  = largeScaleRmsSlope * oneAngles;
teta_neg= 1.224745 * atan(largeScaleRmsSlope) * oneAngles;
% teta_pos= -teta_neg;
a_neg	= 0.295410;
a_pos	= a_neg;
a_nul	= 1.181636;

num  = (2*pi) .* w2 .* gamma(2-alpha) .* (2 .^ (-DeuxAlpha));
deno = alpha .* UnMoinsAlpha .* gamma(UnPlusAlpha);
Ch   = num ./ deno;



%----------------------------------------------------------------------------
% CALCUL DU COEF. DE RETRODIFUSION DE L'INTERFACE EAU-SEDIMENT.(mts)
%----------------------------------------------------------------------------

%-----------------------------------------------------------------------------
% Calcul de la contribution de Kirchhoff dans le cadre du modele de Jackson.
%-----------------------------------------------------------------------------


%-------------------------------------
% Calcul des parametres intermediaires.

Gam1SurAlpha = gamma(UnSurAlpha);
num  = 8 * (alpha .^ 2) .* gamma( UnPlusAlpha ./ DeuxAlpha );
deno = gamma(0.5) .* Gam1SurAlpha .* gamma(1 ./ DeuxAlpha);
aaa  = ( num ./ deno ) .^ DeuxAlpha;

b = aaa .^ (UnPlusAlpha ./ DeuxAlpha) .* Gam1SurAlpha ./ DeuxAlpha;

qc = Ch .* ( 2.^(1 - DeuxAlpha) ) .* ( kcm .^ ( 2 .* UnMoinsAlpha ) );
 

% ------------------------------------
% Coeff de retrhodiffusion de Kirchhoff

R_normal = (rho - kappa) ./ (rho + kappa) ; % Coef de reflexion en incidence normale
haut = (b .* qc .* (( abs(R_normal) ).^2)) * oneAngles;


% -------------------------
% Debut du calcul tensoriel

bas  = sinAngles .^ (4 .* (alpha * oneAngles)) ...
   		+ ((aaa .* (qc .^ 2)) * oneAngles) .* (cosAngles .^ 4) ;

sig1 = haut ./ ( 8  * pi * bas .^ (( UnPlusAlpha ./ DeuxAlpha ) * oneAngles ) );




%-----------------------------------------------------------------------------
% Calcul de la contribution de rugosite composee dans le cadre du modele de
% Jackson.
%-----------------------------------------------------------------------------



%----------------------------------------
% calcul de S(teta,s) (fonction d'ombre).

new_t = (1 ./ LargeScaleRmsSlope) .* cotAngles;
subt  = find(new_t < 2.1);
new_t = new_t(subt);

Qcr = (pi^(-0.5) .* exp(-new_t .^ 2) - new_t .* (1 - erf(new_t))) ./ (4 * new_t);
Scr = (1 - exp(-2 .* Qcr)) ./ (2 .* Qcr);


%---------------------------
% calcul de F(teta,sig_pr, LargeScaleRmsSlope)

AnglesArray = repmat(anglesRadian, [nbCourbes 1]);

som1	= a_neg * Sigma_pr(AnglesArray + teta_neg,	Kappa, Ro, Kcm, Gam, W2);
som2	= a_nul * Sigma_pr(AnglesArray, 			Kappa, Ro, Kcm, Gam, W2);
som3	= a_pos * Sigma_pr(AnglesArray - teta_neg,	Kappa, Ro, Kcm, Gam, W2);
som		= som1 + som2 + som3;
Fcr		= (pi ^ (-0.5)) * som;

sig2 = Fcr;
sig2(subt) = Scr .* Fcr(subt);


% Raccordement des deux contributions.
%-------------------------------------

C4 = 1000 .^ ( 1 ./ UnPlusAlpha ) .* ( aaa .* (qc.^2) ) .^ UnSurAlpha ;
sinKdB = (1 ./ C4 + 4) .^ (-0.25) * oneAngles;
x = 80 .* (sinAngles - sinKdB);

fx = 1./ ( 1 + exp( x ) );

mts = fx .* sig1 + ( 1 - fx ) .* sig2 ;  % indice de interfaceRoughnes.



%----------------------------------------------------------------------------
%	      CALCUL DU COEF DE RETRODIFFUSION DU VOLUME. (mtv).
%----------------------------------------------------------------------------

%-----------------------------------------------------------------------------
% Calcul de la contribution du volume dans le cadre du modele de Jackson.
%-----------------------------------------------------------------------------


% Definitions preliminaires.
%---------------------------

som1v	= a_neg .* Sigma_pv(AnglesArray + teta_neg, Kappa, Ro, Rapcel, Sigma2, Delta);
som2v	= a_nul .* Sigma_pv(AnglesArray,			Kappa, Ro, Rapcel, Sigma2, Delta);
som3v	= a_pos .* Sigma_pv(AnglesArray - teta_neg, Kappa, Ro, Rapcel, Sigma2, Delta);
somv	= som1v + som2v + som3v;
Fv		= (pi ^ (-0.5)) * somv;

mtv = Fv;
mtv(subt) = Scr .* Fv(subt);


%----------------------------------------------------------------------------
%			COEFFICIENT TOTAL DE RETRODIFFUSION.
%----------------------------------------------------------------------------

mtot = mtv + mts;

%----------------------------------------------------------------------------
%				  AFFICHAGE
%----------------------------------------------------------------------------

% passage en index de retrodiffusion.
%------------------------------------

kirchhoff           = reflec_Enr2dB( sig1 );
rug_comp            = reflec_Enr2dB( sig2 );
interfaceRoughnes   = reflec_Enr2dB( mts );
volume              = reflec_Enr2dB( mtv );
total               = reflec_Enr2dB( mtot );

composantes = [kirchhoff; rug_comp; interfaceRoughnes; volume];




% -----------------------------------------
% Sortie des parametre ou traces graphiques

if(nargout == 0)
	figure
	if(size(total, 1) == 1)
		plot(angles, composantes); grid on; zoom on; hold on;
		plot(angles, total, 'black', 'LineWidth', 2); grid on; zoom on;
	else
		plot(angles, total); grid on; zoom on;
	end
	if(size(total, 1) == 1)
		legend({'Kirchhoff'; 'Rugosite composite'; 'Surface'; 'Volume'; 'Total'});
	end
	xlabel('angles d''incidence (deg)'); ylabel('BS (dB)');
	title('Modele de Jackson avec contraintes', 'Fontsize', 16);
else
	varargout{1} = total;
	varargout{2} = composantes;
end








%----------------------------------------------------------------------------
%				SOUS-FONCTIONS
%----------------------------------------------------------------------------


function sigma_pr = Sigma_pr(teta, kappa, rho, k, gam, w2)

P = sqrt( kappa .^ 2 - sin(teta).^2 );
Y = ((rho-1) .^2  .* sin(teta).^2 + rho.^2-kappa.^2 ) ...
   		./ ((rho .* cos(teta)+P) .^2 );
K = sqrt((4*k .^2 ) .* sin(teta).^2 + (k./10).^2); % nombre d'onde pour lequel W est evalue
W = w2 .* (1./K) .^ (gam) ;                        % spectre de puissance du relief du fond
sigma_pr = 4 * ((k .* cos(teta)) .^ 4) .* (abs(Y)) .^ 2 .* W ;







function sigma_pv = Sigma_pv(teta, kappa, rho, a1, sigma2, delta)

P = sqrt( kappa .^2  - sin(teta).^2 );
y = (rho .* cos(teta)) ./ P;
R = (y-1) ./ (y+1) ; 
numerateur = (5*delta .* cos(teta).^2) .* reflec_dB2Enr(sigma2) .* (abs(1-R.^2)).^2 ;
deno = a1 .* log(10) .* (abs(P)).^2.*imag(P);
sigma_pv = numerateur ./ deno;
