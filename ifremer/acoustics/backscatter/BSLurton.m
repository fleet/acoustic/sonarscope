% Description
%   Lurton Angular Backscatter model
%
% Syntax
%   [reflec, composantes] = BSLurton(angles, Parametres)
%
% Input Arguments
%   angle       : Incidence angles (deg) (verticale = 0)
%   Parametres  : Tableau contenant les parametres du modele
%		AmplSpecul  : Amplitude de BS en speculaire (en dB/m2)
%		LargSpecul  : Demi-largeur du lobe speculaire deg)
%		AmplLambert : Cte de Lambert (dB/m2)
%		CoefLambert : Coefficient de la puissance du cos(theta)
%   Parametres optionnels
%		AmplTransitoire : Amplitude de BS en speculaire (dB/m2)
%		LargTransitoire : Demi-largeur du lobe speculaire (deg)
%
% Output Arguments
%   []          : Auto-plot activation
%   reflec      : Indice de retrodiffusion total.
%   composantes : [lambert; speculaire; transitoire] ou [lambert; speculaire].
%
% Examples
%   angles = -80:80;
%   BSLurton(angles, [0 5 -50 2]);
%
%   reflec = BSLurton(angles, [-5, 5, -30, 2]);
%   plot(angles, reflec); grid on; zoom on; hold on;
%
%   BSLurton(angles, [-5 3 -30 2 -20 10]);
%   BSLurton(angles, [-5 3 -30 2 -20 10 40]);
%
%   BSLurton(angles, [-5 10 -20 2; -7 7 -30 2.0; -10 4 -40 2]);
%
% Authors : JMA + XL
% See also BSLurtonCritique BSJackson Lambert Authors
%
% Reference pages in Help browser
%   <a href="matlab:doc BSLurton">BSLurton</a>
%------------------------------------------------------------------------------

function [reverb, composantes] = BSLurton(angles, Parametres)

reverb      = [];
composantes = [];

angles(abs(angles) > 89) = NaN;

% Preparation de la liste d'angles si elle est 2 D
[m, n] = size(angles);
if m > 1
    angles = reshape(angles, 1, m*n);
end

subNaN = find(isnan(angles));
angles(subNaN) = 0;

nbAngles = length(angles);
nbCourbes = size(Parametres, 1);
nbParams = size(Parametres, 2);

RevbFondAmplSpecul  = Parametres(:,1);
RevbFondLargSpecul  = Parametres(:,2) / 2;
RevbFondAmplLambert = Parametres(:,3);
RevbFondCoefLambert = Parametres(:,4);

%% Lambert

lambert = repmat(reflec_dB2Enr(RevbFondAmplLambert), [1 nbAngles]) .* (repmat(cos(angles * (pi / 180)), [nbCourbes 1]) .^ repmat(RevbFondCoefLambert, [1 nbAngles]));
reverb_nat = lambert;
lambert = reflec_Enr2dB(lambert);

%% Sp�culaire

speculaire = NaN(size(lambert));
c = sqrt(-1 / log(0.5));

ind_nz = find(RevbFondLargSpecul ~= 0.) ;

if ~isempty(ind_nz)
    RevbFondAmplSpecul = RevbFondAmplSpecul(ind_nz) ;
    RevbFondLargSpecul = RevbFondLargSpecul(ind_nz) ;
    nbCourbe = length(ind_nz) ;
    
    speculaire(ind_nz,:) = repmat(reflec_dB2Enr(RevbFondAmplSpecul), [1 nbAngles]) .* exp(-(repmat(angles, [nbCourbe 1]) ./ (repmat(RevbFondLargSpecul, [1 nbAngles]) .* c)) .^ 2);
    reverb_nat(ind_nz,:) = reverb_nat(ind_nz,:) + speculaire(ind_nz,:) ;
    speculaire(ind_nz,:) = reflec_Enr2dB(speculaire(ind_nz,:));
end

%% R�gime transitoire

transitoire = NaN(size(lambert));
if nbParams >= 6
    RevbFondAmplTrans = Parametres(:, 5);
    RevbFondLargTrans = Parametres(:, 6) / 2;
    
    if nbParams == 7
        CutOffAngleTrans = Parametres(:, 7);
    else
        CutOffAngleTrans = [];
    end
    
    ind_nz = find(RevbFondLargTrans ~= 0.) ;
    
    if ~isempty(ind_nz)
        RevbFondAmplTrans = RevbFondAmplTrans(ind_nz) ;
        RevbFondLargTrans = RevbFondLargTrans(ind_nz) ;
        nbCourbe = length(ind_nz) ;
        
        if isempty(CutOffAngleTrans)
            transitoire(ind_nz,:) = repmat(reflec_dB2Enr(RevbFondAmplTrans), [1 nbAngles]) .* exp(-(repmat(angles, [nbCourbe 1]) ./ (repmat(RevbFondLargTrans, [1 nbAngles]) .* c)) .^ 2);
        else
            newAngles = angles;
            newAngles = abs(newAngles) - CutOffAngleTrans;
            newAngles(abs(angles) < CutOffAngleTrans) = 0;
            transitoire(ind_nz,:) = repmat(reflec_dB2Enr(RevbFondAmplTrans), [1 nbAngles]) .* exp(-(repmat(newAngles, [nbCourbe 1]) ./ (repmat(RevbFondLargTrans, [1 nbAngles]) .* c)) .^ 2);
        end
        
        reverb_nat(ind_nz,:)  = reverb_nat(ind_nz,:) + transitoire(ind_nz,:) ;
        transitoire(ind_nz,:) = reflec_Enr2dB(transitoire(ind_nz,:));
    end
end

reverb_nat(subNaN) = NaN;
reverb = [reverb; reflec_Enr2dB(reverb_nat)];

lambert(subNaN)     = NaN;
speculaire(subNaN)  = NaN;
transitoire(subNaN) = NaN;
composantes = [composantes; lambert; speculaire; transitoire];

if m > 1
    reverb = reshape(reverb, m, n);
    composantes = [];
end

%% Sortie des param�tres ou trac�s graphiques

if nargout == 0
    figure
    if nbCourbes == 1
        plot(angles, reverb, 'k'); grid on; hold on;
        YLim = get(gca, 'YLim');
        plot(angles, composantes); grid on; zoom on; hold off;
        set(gca, 'YLim', YLim');
        legend({'Total'; 'Speculaire'; 'Lambert'; 'Transitoire'});
        xlabel('angles d''incidence (deg)'); ylabel('BS (dB)');
        title('Modele de Lurton');
    else
        plot(angles, reverb); grid on; hold on;
    end
end
