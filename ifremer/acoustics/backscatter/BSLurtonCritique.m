% Description
%   Lurton Angular Backscatter model with a critic angle
%
% Syntax
%   [reflec, composantes] = BSLurtonCritique(angles, Parametres)
%
% Input Arguments
%   angle       : Incidence angles (deg) (verticale = 0)
%   Parametres  : Tableau contenant les parametres du modele
%		FondAmplSpecul       : Amplitude de BS en speculaire (en dB/m2)
%		FondLargSpecul       : Demi-largeur du lobe speculaire (deg)
%		FondAmplLambert      : Cte de Lambert (dB/m2)
%		FondCoefLambert      : Coefficient de la puissance du cos(theta)
%		FondAmplTransitoire  : Amplitude de BS en speculaire (dB/m2)
%		FondLargTransitoire  : Demi-largeur du lobe speculaire (deg)
%       AngleCritique        : Angle de la discontinuite de comportement angulaire (deg)
%       DecroissanceCritique : Decroissance apres angle critique
%
% Output Arguments
%   []          : Auto-plot activation
%   reflec      : Indice de retrodiffusion total.
%   composantes : [lambert; speculaire; transitoire] or [lambert; speculaire].
%
% Examples 
%   angles = -80:80;
%   BSLurtonCritique(angles, [-5, 5, -30, 2, -30, 10, 60, 3]);
%
%   reflec = BSLurtonCritique(angles, [-5, 5, -30, 2, -30, 10, 60, 3]);
%   plot(angles, reflec); grid on; zoom on; hold on;
%
% Authors  : JMA + XL
% See also : BSLurton BSJackson Lambert Authors
%
% Reference pages in Help browser
%   <a href="matlab:doc BSLurtonCritique">BSLurtonCritique</a>
%------------------------------------------------------------------------------

function [reflec, composantes] = BSLurtonCritique(angles, Parametres)

sub1 = find(angles < -Parametres(7));
sub3 = find(angles >  Parametres(7));

sub2 = find(angles >= -Parametres(7) & angles <= Parametres(7));
par2 = Parametres(1:6);

par1 = Parametres(1:6);
par1(4) = Parametres(8);

offset = BSLurton(Parametres(7), par2 ) - BSLurton(Parametres(7), par1);

par1(3) = Parametres(3) + offset;

if isempty(sub1)
    reflec1 = [];
    composantes1 = [];
else
    [reflec1, composantes1] = BSLurton(angles(sub1), par1);
end

if isempty(sub2)
    reflec2 = [];
    composantes2 = [];
else
    [reflec2, composantes2] = BSLurton(angles(sub2), par2);
end

if isempty(sub3)
    reflec3 = [];
    composantes3 = [];
else
    [reflec3, composantes3] = BSLurton(angles(sub3), par1);
end

reflec = [reflec1 reflec2 reflec3];
composantes = [composantes1 composantes2 composantes3];

%% Sortie des param�tres ou trac�s graphiques

if nargout == 0
    figure
    plot(angles, reflec, 'k'); grid on; hold on;
    YLim = get(gca, 'YLim');
    plot(angles, composantes); grid on; zoom on; hold off;
    set(gca, 'YLim', YLim');
    legend({'Total'; 'Speculaire'; 'Lambert'; 'Transitoire'});
    xlabel('angles d''incidence (deg)'); ylabel('BS (dB)');
    title('Modele de Lurton');
end
