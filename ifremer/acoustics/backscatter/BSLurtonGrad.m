% Gradient de la fonction BSLurton
%
% Syntax
%   [reflec, J] = BSLurtonGrad( angles, coefs )
%
% Remarks : Celle fonction est appelee par l'algorithme d'optimisation NewtonRecSim. 
% 
% Input Arguments 
%   angles       : angles d'incidence (deg) (verticale = 0)
%   Parametres  : Tableau contenant les parametres du modele
%		FondAmplSpecul  : Amplitude de BS en speculaire (en dB/m2)
%		FondLargSpecul  : Demi-largeur du lobe speculaire deg)
%		FondAmplLambert : Cte de Lambert (dB/m2)
%		FondCoefLambert : Coefficient de la puissance du cos(theta)
%   Parametres optionnels
%		FondAmplTransitoire  : Amplitude de BS en speculaire (dB/m2)
%		FondLargTransitoire  : Demi-largeur du lobe speculaire (deg)
%
% Output Arguments
%   []     : Auto-plot activation
%   reflec : Indice de retrodiffusion total.
%   J      : Jacobienne.
%
% Examples 
%   angles = -80:80;
%   BSLurtonGrad( angles, [-5, 5, -30, 2.] );
%
%   [reflec, J] = BSLurtonGrad( angles, [-5, 5, -30, 2.] );
%
% See also BSLurton NewtonRecSim Authors
% Authors : YHDR + MS
%------------------------------------------------------------------------------

% TODO : Cette fonction est adapt�e au BSLurlon d'origine c.a.d sans le
% Cutt-Off

function varargout = BSLurtonGrad( angles, coefs )

% Preparation de la liste d'angles si elle est 2 D
[m, n] = size(angles);
if m > 1
    angles = reshape(angles, 1, m*n);
end

nbCourbes = size(coefs, 1);
nbParams  = size(coefs, 2);

if nbCourbes > 1 
    disp('On ne peut pas calculer le gradient pour differents jeux de coeffs');
    return
end

RevbFondAmplSpecul = coefs(:, 1);
alpha = coefs(:, 2) ;
RevbFondAmplLambert = coefs(:, 3);
beta = coefs(:, 4);
reverb_nat = 0;


B = reflec_dB2Enr( RevbFondAmplLambert );
cos1 = cos(angles * pi / 180.);
Arg3 = cos1 .^ beta;
lambert =  B.* Arg3;

c = sqrt(-1./log(0.5));

if alpha ~= 0
    Incoherent = angles ./ (alpha .* c);
    Arg2 = exp(-(Incoherent.^ 2.) );
    A = reflec_dB2Enr(RevbFondAmplSpecul);
    speculaire = A .* Arg2;
else
    speculaire = NaN(size(lambert));
end


if nbParams == 6
    RevbFondAmplTrans = coefs(:, 5);
    F = coefs(:, 6);
    
    if F ~= 0
        Inattendu = angles ./ (F .* c);
        Arg1 = exp(-(Inattendu.^ 2.));
        E = reflec_dB2Enr(RevbFondAmplTrans);
        transitoire = E .* Arg1 ;  
    else
        transitoire = NaN(size(lambert));
    end
    
    reverb_nat = reverb_nat + transitoire;
end

reverb_nat = reverb_nat + speculaire + lambert;

reverb = reflec_Enr2dB(reverb_nat);

%
% calcul du gradient par rapport a RevbFondAmplSpecul

ldb = 10 / log(10) ;

V(1,:) = speculaire ./ reverb_nat ;

V(2,:) = 2 .* ldb .* V(1,:) .* (Incoherent.^2) ./ alpha;

V(3,:)= lambert ./ reverb_nat;

V(4,:)= ldb .* V(3,:) .* log(cos1);

if (nbParams == 6)
    V(5,:)= transitoire ./ reverb_nat;
    V(6,:)= 2 .* ldb .* V(5,:) .* (Inattendu.^2) ./ F;
end

J = V';

%% Sortie des parametre ou traces graphiques

if nargout == 0
    figure;
    subplot(2, 1, 1);
    plot( angles', reverb' ); grid on; zoom on;
    xlabel('Angles (deg)'); ylabel('Gain (dB)');
    title('Modele de Lurton');
    
    subplot(2, 1, 2);
    plot( angles', J ); grid on; zoom on;
    xlabel('Angles (deg)'); title('Jacobien');
else
    varargout{1} = reverb;
    varargout{2} = J;
end
