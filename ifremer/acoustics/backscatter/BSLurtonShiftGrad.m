% Gradient de la fionction BSLurtonShift
%
% Syntax
%   [reflec, J] = BSLurtonShiftGrad( angles, Parametres )
%
% Remarks : Celle fonction est appelee par l'algorithme d'optimisation NewtonRecSim.
%           Il ne peut pas y avoir de tableaux de parametres.
%
% Input Arguments 
%   angle       : angle d'incidence (deg) (verticale = 0)
%   Parametres  : Tableau contenant les parametres du modele
%       Decentrage  : Decentrage angulaire (deg)
%		AmplSpecul  : Amplitude de BS en speculaire (en dB/m2)
%		LargSpecul  : Demi-largeur du lobe speculaire deg)
%		AmplLambert : Cte de Lambert (dB/m2)
%		CoefLambert : Coefficient de la puissance du cos(theta)
%   Parametres optionnels
%		AmplTransitoire  : Amplitude de BS en speculaire (dB/m2)
%		LargTransitoire  : Demi-largeur du lobe speculaire (deg)
%
% Output Arguments
%   reflec      : Indice de retrodiffusion total.
%   J           : Jacobienne.
%
% Examples 
%   angles = -80:80;
%
%   [reflec,J] = BSLurtonShiftGrad( angles, [4 -5, 5, -30, 2.] );
%
%   [reflec,J] = BSLurtonShift( angles, [4 -5, 3, -30, 2., -20, 10] );
%
% See also BSLurton BSLurtonCritique BSJackson Lambert Authors
% Authors : JMA + XL + YHDR
%------------------------------------------------------------------------------

function varargout = BSLurtonShiftGrad( angles, Parametres )

nbCourbes = size(Parametres, 1);
nbParams = size(Parametres, 2);

if nbCourbes > 1 
	disp('On ne peut pas calculer le gradient pour differents jeux de coeffs');
	return
end
%
decentrage = Parametres(1);
[reflec, JJ] = BSLurtonGrad( angles-decentrage, Parametres(2:end));
%
LargSpecul = Parametres(3);
d2g = pi / 180;
bidule = (angles-decentrage)';
theta = bidule * d2g;
cos1 = cos(theta);
Beta = Parametres(5);
%
J0 = JJ(:,4) ./ cos1 ./ log(cos1) .* Beta .* d2g .* sin(theta) + ...
    JJ(:,2) .* LargSpecul ./ bidule;
%
if nbParams == 7
    F = Parametres(:, 7);
    J0 = J0 + JJ(:,6) .* F ./ bidule;
end
%
JJ = [J0 , JJ ];
%
varargout{1} = reflec;
varargout{2} = JJ;
