% Calcul des parametres du modele de Jackson a partir des param du modele BSJackson_contraintes
%
% Syntax
%   [rho, nu, beta, delta, w2] = CalculParaJackson(Phi, C_eau)
%
% Input Arguments 
%   Phi       : Taille des grains
%   C_eau     : Celerite dans l'eau (m/s)
%
% Output Arguments
%   rho       : Rapport des densites (rho_sed/rho_eau)
%   nu        : Rapport des celerites (c_sed/c_eau)
%   beta      : Attenuation dans le sediment (dB/lambda).
%   delta     : Loss Parameter 
%   w2        : Puissance spectrale
%
% Examples 
%   Phi = -1:.5:9; Frequence = 100; C_eau=1528;
%   [rho, nu, beta, delta, w2] = CalculParaJackson(Phi, C_eau);
%
%   angles = -89:89;
%   C_sed=1550; rho_eau=1.2; rho_sed=1.3; gam=3.25; w2=0.0004; sigma2=-30; 
%   Parametres1 = [repmat(Frequence, size(beta)) ...
%                  repmat(C_eau, size(beta)) ...
%                  repmat(C_sed/C_eau, size(beta)) ...
%                  repmat(rho_sed/rho_eau, size(beta)) ...
%                  repmat(gam, size(beta)) ...
%                  w2 ...
%                  repmat(sigma2, size(beta)) ...
%                  beta];
%
%   BSJackson( angles, Parametres1);
%
%
% See also BSLurton Lambert Authors
% Authors : LG + YHDR + GLC + JMA
% VERSION  : $Id: CalculParaJackson.m,v 1.2 2002/06/17 16:21:37 augustin Exp $
%------------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HISTORIQUE DEVELOPPEMENT
%   02/11/00 - JMA - creation
%   22/03/01 - JMA - Mise en ref.
% ----------------------------------------------------------------------------

function varargout = CalculParaJackson(Phi, C_eau)

if nargin == 0
    help CalculParaJackson
    return
end

%--------------------
% Calcul de rho et nu

sub1 = find(Phi >= -1   & Phi <   1  );
rho(sub1) = 0.007797 * Phi(sub1).^2 - 0.17057 * Phi(sub1) + 2.3139;
nu(sub1) = 0.002709 * Phi(sub1).^2 - 0.056452 * Phi(sub1) + 1.2778; 
sub2 = find(Phi >=  1   & Phi <   5.3);
rho(sub2) = -0.0165406 * Phi(sub2).^3 + 0.2290201 * Phi(sub2).^2 - 1.1069031 * Phi(sub2) + 3.0455;
nu(sub2) = -0.0014881 * Phi(sub2).^3 + 0.0213937 * Phi(sub2).^2 - 0.1382798 * Phi(sub2) + 1.3425;
sub3 = find(Phi >   5.3 & Phi <=  9  );
rho(sub3) = -0.0012973 * Phi(sub3) + 1.1565 ;
nu(sub3)  = -0.0024324 * Phi(sub3) + 1.0019 ;
if (isempty(sub1) && isempty(sub2) && isempty(sub3))
    warning('Bulk Grain Size Illegal (rho,nu)');
end      

%---------------------------
% Calcul de beta (dB/lambda) 

sub1 = find(Phi >=  -1 & Phi < 0  );
alpha2surf(sub1) = 0.4556;
sub2 = find(Phi >=   0 & Phi < 2.6);
alpha2surf(sub2) = 0.4556 + 0.0245 * Phi(sub2);
sub3 = find(Phi >= 2.6 & Phi < 4.5);
alpha2surf(sub3) = 0.1978 + 0.1245 * Phi(sub3);
sub4 = find(Phi >= 4.5 & Phi < 6.0);
alpha2surf(sub4) = 8.0399 - 2.5228 * Phi(sub4) + 0.20098 * Phi(sub4).^2;
sub5 = find(Phi >= 6.0 & Phi < 9.5);
alpha2surf(sub5) = 0.9431 - 0.2041 * Phi(sub5) + 0.0117 * Phi(sub5).^2 ;
sub6 = find(Phi >= 9.5            );
alpha2surf(sub6) = 0.0601; 
if (isempty(sub1) && isempty(sub2) && isempty(sub3) && isempty(sub4) && isempty(sub5) && isempty(sub6))
    warning('Bulk Grain Size Illegal (rho,nu)');
end      

beta = alpha2surf .* nu .* C_eau' *1e-3;
delta =  beta *log(10) /40 /pi;

%-------------
% Calcul de w2

sub1 = find(Phi >=  -1 & Phi < 5  );
h(sub1) = (2.03846 - 0.26923 *Phi(sub1)) ./ (1 + 0.076923 *Phi(sub1)) ;
sub2 = find(Phi >=  5 & Phi <= 9  );
h(1,sub2) = 0.5 ;
if (isempty(sub1) && isempty(sub2))
    warning('Bulk Grain Size Illegal (w2)');
end

w2 = 0.00207 * h.^2  ;

%-----------------------
% Sorties des Parametres

varargout{1} = rho'  ;
varargout{2} = nu'  ;
varargout{3} = beta' ;
varargout{4} = delta' ;
varargout{5} = w2'   ;
