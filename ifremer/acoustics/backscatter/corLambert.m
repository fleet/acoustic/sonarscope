% Correction de reflectivite de Lambert
%
% Syntax
%   cor = corLambert(angles)
%
% Input Arguments 
%   angles : angle d'incidence (deg) (verticale = 0)
%
% Output Arguments
%   []  : Auto-plot activation
%   cor : Correction
%
% Examples 
%   corLambert(-80:80)
%   cor = corLambert(-80:80)
%
% See also BSLurton Authors
% Authors : JMA + XL
%------------------------------------------------------------------------------

% TODO : Correction R/Rn si signature=1 ou angles / Verticale sinon

function cor = corLambert(angles)

cor = reflec_Amp2dB(cosd(angles)); 
