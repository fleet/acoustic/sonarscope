% Dimitrios special function
%
% Syntax
%   dimitrios01(...)
%
% Input Arguments
%   curvesFileName : Liste of .mat files containing the curves
%
% Name-Value Pair Arguments
%   nbLoops : Number of times that every optimization is thrown (Default : 50)
%
% Output Arguments
%   [] : Auto-plot activation
%   a  : Description du param�tre (unit�).
%
% Examples
%   repImport = 'Z:\private\ifremer\dimitrios\SHOM\ALL_2';
%   [flag, curvesFileName, repImport] = uiSelectFiles('ExtensionFiles', '.mat', 'RepDefaut', repImport);
%   if flag
%       parameterFileName = 'Z:\private\ifremer\dimitrios\SHOM\ALL_2\Descriptors2.txt';
%       residualsFileName = 'Z:\private\ifremer\dimitrios\SHOM\ALL_2\Residuals2.mat';
%       dimitrios01(curvesFileName,  'Tag', '_Comp2', 'parameterFileName', parameterFileName, 'residualsFileName', residualsFileName)
%  end
%
% See also ClModel Authors
% Authors  : JMA + DE
%-------------------------------------------------------------------------------

function dimitrios01(curvesFileName, varargin)

[varargin, nbLoops]             = getPropertyValue(varargin, 'nbLoops',           50);
[varargin, MaxIter]             = getPropertyValue(varargin, 'MaxIter',           200);
[varargin, Tag]                 = getPropertyValue(varargin, 'Tag',               []);
[varargin, descriptorsFileName] = getPropertyValue(varargin, 'parameterFileName', fullfile(my_tempdir, 'Descriptors.txt'));
[varargin, residualsFileName]   = getPropertyValue(varargin, 'residualsFileName', fullfile(my_tempdir, 'Residuals.mat')); %#ok<ASGLU>

if ~iscell(curvesFileName)
    curvesFileName = {curvesFileName};
end

%% Loop on the set of curve files

fid = fopen(descriptorsFileName, 'w+t');
if fid == -1
    return
end

xGlobal = [];
str = [];
fig = FigUtils.createSScFigure;
nbFiles = length(curvesFileName);
for k=1:nbFiles
    fprintf('%d/%d : %s\n', k, nbFiles, curvesFileName{k});
    
    %% Read the curve for the current file
    
    [CourbeConditionnelle, flag] = load_courbesStats(curvesFileName{k});
    if ~flag
        return
    end
    [strLegend, figCurve] = CourbesStatsPlot(CourbeConditionnelle.bilan, 'fig', fig, 'iCoul', k, 'Stats', 0, 'subTypeCurve', 1);
    str = [str strLegend];  %#ok<AGROW>
    
    %% Initialyse the optimization tool
    
    %     InitModel = getLastDefinedModel(CourbeConditionnelle, {'BSLurton'});
    %     optim = optimBSLurtonWithoutGUI(CourbeConditionnelle.bilan, 'InitModel', InitModel, 'MaxIter', MaxIter);
    optim = optimBSLurtonWithoutGUI(CourbeConditionnelle.bilan, 'MaxIter', MaxIter);
    model = get(optim, 'models');
    %     plot(model)
    %editProperties(optim);
    %editProperties(model);
    
    %{
%% Optimization with GUI
optim_ihm = cl$i_optim('optim', optim);
optim_ihm = editobj(optim_ihm);
optim          = get(optim_ihm, 'optim');
model         = get(optim, 'models');
    %}
    
    %     get(optim, 'Algo')
    %     optim = set(optim, 'Algo', 2); % Newton
    %     get(optim, 'Algo')
    %     optim = set(optim, 'currentModel', 2); % BSLurtonCritique
    %     get(optim, 'currentModel')
    
    %{
% Parameters that can be set and get
Dependences     <-> '1=No' | {'2=Yes'}
Algo            <-> {'1=Simplex'} | '2=Newton'
SimAnneal       <-> {'1=No'} | '2=Yes'
Visu            <-> {'1=No'} | '2=Yes'
currentModel    <-> 1
Err             <-- [0.667344416894955 2.53801720823305]
TolF            <-> []
MaxIter         <-> 100
    %}
    
    %% Repeat nbLoops time the optimization afer having randomized it
    
    Rms = NaN(1, nbLoops, 'single');
    valParams = cell(1, nbLoops);
    for k2=1:nbLoops
        optim = random(optim);
        %         plot(optim);
        optim = optimize(optim);
        %         plot(optim);
        Rms(k2) = get(optim, 'Err');
        valParams{k2} = get_valParams(optim);
    end
    
    %% Search for the minimum RMS for the nbLoops iterations
    
    figure(54656); plot(Rms); grid on; title('Rms')
    [rmsMin, indMin] = min(Rms);
    bestParams = valParams{indMin};
    model = set_valParams(model, bestParams);
    
    if nargout == 0 % Auto plot activation
        [~, Titre, Ext] = fileparts(curvesFileName{k});
        [~, hFigModel] = plot(model, 'Titre', [Titre Ext]);
    end
    my_close(hFigModel, 'TimeDelay', 30);
    
    %     optim = set(optim, 'models', model);
    %     plot(optim)
    
    %% Save parameters in a .txt file
    
    fprintf(fid, '%s %s %f\n', curvesFileName{k}, num2str(bestParams), rmsMin);
    
    %% Compute the residuals
    
    [residuals{k}, x{k}] = compute_residuals(model); %#ok<AGROW>
    xGlobal = unique([xGlobal x{k}]);
end
fclose(fid);
my_close(54656);
figure(figCurve)
legend(str, 'Interpreter', 'none')

%% Compute the mean curve of all the residual curves

FigUtils.createSScFigure;
GCA = gca;
color = [0.8 0.8 0.8];
y = zeros(size(xGlobal), 'single');
for k=1:nbFiles
    y = y + interp1(x{k}, residuals{k}, xGlobal);
    PlotUtils.createSScPlot(GCA, x{k}, residuals{k}, 'Color', color); grid on; hold on;
end
meanResiduals = y / nbFiles;
PlotUtils.createSScPlot(GCA, xGlobal, meanResiduals, 'b', 'LineWidth', 3); grid on; hold on;
xlabel('Angles (deg)');
ylabel('Residuals (dB)');
title('Mean value of residuals')

%% Save residuals as a .mat file

CourbeConditionnelle.bilan{1}.x            = xGlobal;
CourbeConditionnelle.bilan{1}.y            = meanResiduals;
CourbeConditionnelle.bilan{1}.ymedian      = NaN(size(xGlobal));
CourbeConditionnelle.bilan{1}.nom          = 'Residuals from Dimitrios washing machine';
CourbeConditionnelle.bilan{1}.sub          = 1:length(xGlobal);
CourbeConditionnelle.bilan{1}.nx           = ones(size(xGlobal)); % TODO
CourbeConditionnelle.bilan{1}.std          = NaN(size(xGlobal));
CourbeConditionnelle.bilan{1}.titreV       = 'Residuals from Dimitrios washing machine';
CourbeConditionnelle.bilan{1}.NY           = length(xGlobal);
CourbeConditionnelle.bilan{1}.nomX         = 'Residuals_PingBeam_BeamPointingAngle';
CourbeConditionnelle.bilan{1}.nomZoneEtude = 'Residuals from Dimitrios washing machine';
CourbeConditionnelle.bilan{1}.commentaire  = '';
CourbeConditionnelle.bilan{1}.Tag          = 'TxDiag';
CourbeConditionnelle.bilan{1}.model        = [];
CourbeConditionnelle.bilan{1}.residuals    = NaN(size(xGlobal));
save(residualsFileName, 'CourbeConditionnelle');

%% Correct the curves of the residuals averaged curve

str = {};
fig = FigUtils.createSScFigure;
% GCA = gca;
for k=1:nbFiles
    CourbeConditionnelle = load_courbesStats(curvesFileName{k});
    r = interp1(xGlobal, meanResiduals, CourbeConditionnelle.bilan{1}.x);
    CourbeConditionnelle.bilan{1}.y(:) = CourbeConditionnelle.bilan{1}.y(:) - r(:);
    %     PlotUtils.createSScPlot(GCA, CourbeConditionnelle.bilan{1}.x, y); grid on; hold on;
    strLegend = CourbesStatsPlot(CourbeConditionnelle.bilan, 'fig', fig, 'iCoul', k, 'Stats', 0, 'subTypeCurve', 1);
    str = [str strLegend];  %#ok<AGROW>
    
    [nomDir, nomFic, Ext] = fileparts(curvesFileName{k});
    nomFic = fullfile(nomDir, [nomFic Tag Ext]);
    save(nomFic, 'CourbeConditionnelle');
end
legend(str, 'Interpreter', 'none')
