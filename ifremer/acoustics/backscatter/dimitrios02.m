% Dimitrios special function
%
% Syntax
%   dimitrios01(...)
%
% Input Arguments
%   curvesFileName : Liste of .mat files containing the curves
%
% Name-Value Pair Arguments
%   nbLoops : Number of times that every optimization is thrown (Default : 50)
%
% Output Arguments
%   [] : Auto-plot activation
%   a  : Description du param�tre (unit�).
%
% Examples
%   step 1: load bs curves (.mat) (except the residuals)
%     repImport = 'X:\Dimitrios\shallow_survey\Short_Pulse_Length\400'
%     [flag, curvesFileName, repImport] = uiSelectFiles('ExtensionFiles', '.mat', 'RepDefaut', repImport);
%
%   step 2: subtract residual from each one of the bs curves and plot (all in one) and save new
%   bs curves
%     residualsFileNameIn = 'X:\Dimitrios\shallow_survey\Short_Pulse_Length\400\Residuals_Carre_Renard_400.mat';
%     dimitrios02(curvesFileName, 'Tag', '_Comp2', 'residualsFileNameIn', residualsFileNameIn)
%
%   step 3: fit Lurton model to new BS curves and save all models in one plot and the descriptors in a text file
%     [flag, curvesFileName, repImport] = uiSelectFiles('ExtensionFiles', '.mat', 'RepDefaut', repImport);
%     parameterFileName = 'X:\Dimitrios\shallow_survey\Short_Pulse_Length\400\Descriptors.txt';
%     dimitrios03(curvesFileName, 'nbLoops', 50, 'MaxIter', 200, 'parameterFileName', parameterFileName)
%
%   repImport = 'X:\Dimitrios\all_final_CENT_200';
%   [flag, curvesFileName, repImport] = uiSelectFiles('ExtensionFiles', '.mat', 'RepDefaut', repImport);
%   parameterFileName1 = 'X:\Dimitrios\all_final_CENT_200\Descriptors1.txt';
%   parameterFileName4 = 'X:\Dimitrios\all_final_CENT_200\Descriptors4.txt';
%   residualsFileNameOut = 'X:\Dimitrios\all_final_CENT_200\Residuals1.mat';
%   residualsFileNameIn  = 'X:\Dimitrios\all_final_CENT_200\residuals_Carre_Renard.mat';
%   dimitrios01(curvesFileName, 'nbLoops', 5, 'MaxIter', 50, 'Tag', '_Comp3', 'parameterFileName', parameterFileName1, 'residualsFileNameOut', residualsFileNameOut)
%   dimitrios01(curvesFileName, 'nbLoops', 5, 'MaxIter', 50, 'Tag', '_Comp3', 'residualsFileNameIn', residualsFileNameOut)
%   dimitrios01(curvesFileName, 'nbLoops', 5, 'MaxIter', 50, 'Tag', '_Comp4', 'parameterFileName', parameterFileName4)
%
% See also ClModel Authors
% Authors  : JMA + DE
%-------------------------------------------------------------------------------

function dimitrios02(curvesFileName, varargin)

[varargin, Tag]                 = getPropertyValue(varargin, 'Tag',                 []);
[varargin, residualsFileNameIn] = getPropertyValue(varargin, 'residualsFileNameIn', []); %#ok<ASGLU>

if ~iscell(curvesFileName)
    curvesFileName = {curvesFileName};
end

%% Loop on the set of curve files

% Axe = [];
% xGlobal = [];
str = [];
fig = FigUtils.createSScFigure;
nbFiles = length(curvesFileName);
for k=1:nbFiles
    fprintf('%d/%d : %s\n', k, nbFiles, curvesFileName{k});
    
    %% Read the curve for the current file
    
    [CourbeConditionnelle, flag] = load_courbesStats(curvesFileName{k});
    if ~flag
        return
    end
    [strLegend, figCurve] = CourbesStatsPlot(CourbeConditionnelle.bilan, 'fig', fig, 'iCoul', k, 'Stats', 0, 'subTypeCurve', 1);
    str = [str strLegend];  %#ok<AGROW>
end
figure(figCurve)
legend(str, 'Interpreter', 'none')

%% Compute the mean curve of all the residual curves

CourbeResiduals = loadmat(residualsFileNameIn, 'nomVar', 'CourbeConditionnelle');

%% Correct the curves of the residuals averaged curve

str = {};
fig = FigUtils.createSScFigure;
% GCA = gca;
for k=1:nbFiles
    CourbeConditionnelle = load_courbesStats(curvesFileName{k});
    r = interp1(CourbeResiduals.bilan{1}.x, CourbeResiduals.bilan{1}.y, CourbeConditionnelle.bilan{1}.x);
    CourbeConditionnelle.bilan{1}.y(:) = CourbeConditionnelle.bilan{1}.y(:) - r(:);
    %     PlotUtils.createSScPlot(GCA, CourbeConditionnelle.bilan{1}.x, y); grid on; hold on;
    strLegend = CourbesStatsPlot(CourbeConditionnelle.bilan, 'fig', fig, 'iCoul', k, 'Stats', 0, 'subTypeCurve', 1);
    str = [str strLegend];  %#ok<AGROW>
    
    [nomDir, nomFic, Ext] = fileparts(curvesFileName{k});
    nomFic = fullfile(nomDir, [nomFic Tag Ext]);
    save(nomFic, 'CourbeConditionnelle');
end
legend(str, 'Interpreter', 'none')
