% nomDir = 'X:\dimitrios_JMA\AZIMUTH\complete';
% [flag, curvesFileName, lastDir] = uiSelectFiles('ExtensionFiles', '.mat', 'RepDefaut', nomDir)
% dimitrios03(curvesFileName)

function dimitrios03(curvesFileName, varargin)

[varargin, nbLoops] = getPropertyValue(varargin, 'nbLoops', 50);
[varargin, MaxIter] = getPropertyValue(varargin, 'MaxIter', 100);
% [varargin, Tag] = getPropertyValue(varargin, 'Tag', []);
[varargin, descriptorsFileName] = getPropertyValue(varargin, 'parameterFileName', fullfile(my_tempdir, 'Descriptors.txt')); %#ok<ASGLU>

if ~iscell(curvesFileName)
    curvesFileName = {curvesFileName};
end

%% Loop on the set of curve files

fid = fopen(descriptorsFileName, 'w+t');
if fid == -1
    return
end

Axe = [];
% xGlobal = [];
str = [];
fig = FigUtils.createSScFigure;
nbFiles = length(curvesFileName);
for k=1:nbFiles
    fprintf('%d/%d : %s\n', k, nbFiles, curvesFileName{k});
    
    %% Read the curve for the current file
    
    [CourbeConditionnelle, flag] = load_courbesStats(curvesFileName{k});
    if ~flag
        return
    end
    [strLegend, figCurve] = CourbesStatsPlot(CourbeConditionnelle.bilan, 'fig', fig, 'iCoul', k, 'Stats', 0, 'subTypeCurve', 1);
    str = [str strLegend];  %#ok<AGROW>
    
    %% Initialyse the optimization tool
    
    optim = optimBSLurtonWithoutGUI(CourbeConditionnelle.bilan, 'MaxIter', MaxIter, 'NoTransitory');
    model = get(optim, 'models');
    
    %% Repeat nbLoops time the optimization afer having randomized it
    
    Rms = NaN(1, nbLoops, 'single');
    valParams = cell(1, nbLoops);
    for k2=1:nbLoops
        optim = random(optim);
        %         plot(optim);
        optim = optimize(optim);
        %         plot(optim);
        Rms(k2) = get(optim, 'Err');
        valParams{k2} = get_valParams(optim);
    end
    
    %% Search for the minimum RMS for the nbLoops iterations
    
    figure(54656); plot(Rms); grid on; title('Rms')
    [rmsMin, indMin] = min(Rms);
    bestParams = valParams{indMin};
    model = set_valParams(model, bestParams);
    
    Axe = plot(model, 'Axe', Axe, 'NoData', 'visuComposantes', 0, 'Erase', 0, 'Legend', CourbeConditionnelle.bilan{1}.nom);
    
    if nargout == 0 % Auto plot activation
        [~, Titre, Ext] = fileparts(curvesFileName{k});
        [~, hFigModel] = plot(model, 'Titre', [Titre Ext]);
    end
    my_close(hFigModel, 'TimeDelay', 30);
    
    %     optim = set(optim, 'models', model);
    %     plot(optim)
    
    %% Save parameters in a .txt file
    
    fprintf(fid, '%s %s %f\n', curvesFileName{k}, num2str(bestParams), rmsMin);
end
fclose(fid);
my_close(54656);
figure(figCurve)
legend(str, 'Interpreter', 'none')

