% nomDir = '\\tera10\sonarscope\private\ifremer\Ridha\Files.mat';
% nomDir = 'D:\Temp\Ridha\Files.mat';
% [flag, curvesFileName, lastDir] = uiSelectFiles('ExtensionFiles', '.mat', 'RepDefaut', nomDir)
% curvesFileName = listeFicOnDir('\\tera10\sonarscope\private\ifremer\Ridha\Files.mat', '.mat')
% dimitrios04(curvesFileName, 'nbLoops', 100)

function dimitrios04(curvesFileName, varargin)

[varargin, nbLoops]             = getPropertyValue(varargin, 'nbLoops',           50);
[varargin, MaxIter]             = getPropertyValue(varargin, 'MaxIter',           100);
[varargin, descriptorsFileName] = getPropertyValue(varargin, 'parameterFileName', fullfile(my_tempdir, 'Descriptors.txt')); %#ok<ASGLU>

if ~iscell(curvesFileName)
    curvesFileName = {curvesFileName};
end

%% Loop on the set of curve files

fid = fopen(descriptorsFileName, 'w+t');
if fid == -1
    return
end

nbFiles = length(curvesFileName);
for k=1:nbFiles
    fprintf('%d/%d : %s\n', k, nbFiles, curvesFileName{k});
    [~, Titre, Ext] = fileparts(curvesFileName{k});
    
    %% Read the curve for the current file
    
    S = load(curvesFileName{k});
    FN = fieldnames(S);
    x = S.(FN{1}).x;
    y = S.(FN{1}).y;
    if isfield(S.(FN{1}), 'npoints')
        n = S.(FN{1}).npoints;
    else
        n = ones(size(x));
    end
    x = x(:)';
    y = y(:)';
    n = n(:)';
    
    %     [~, strLegend] = fileparts(curvesFileName{k});
    %     FigUtils.createSScFigure; PlotUtils.createSScPlot(x, y); grid on, title(strLegend)
    %     str = [str strLegend];
    strLegend{k} = Titre; %#ok<AGROW>
    
    %% Initialyse the optimization tool
    
    optim = optimBSLurtonWithoutGUI_new(x, y, n, 'MaxIter', MaxIter, 'NoTransitory');
    model = optim.models;
    
    %% Repeat nbLoops time the optimization afer having randomized it
    
    Rms = NaN(1, nbLoops, 'single');
    valParams = cell(1, nbLoops);
    for k2=1:nbLoops
        optim = random(optim);
        % optim.plot;
        optim = optimize(optim);
        % optim.plot;
        model = optim.models;
        valParams{k2} = model.getParamsValue;
        Rms(k2) = model.rms(0);
    end
    
    %% Search for the minimum RMS for the nbLoops iterations
    
    figure(54656); plot(Rms); grid on; title('Rms')
    [rmsMin, indMin] = min(Rms);
    bestParams = valParams{indMin};
    model.setParamsValue(bestParams);
    
    optim.setParamsValue(bestParams);
    a = OptimDialog(optim, 'Title', ['Optim BS ' Titre], 'windowStyle', 'normal');
    a.openDialog; % GUI qui attend la confirmation
    optim = a.optim;
    model = optim.models;
    
    [~, hFigModel] = model.plot('Title', [Titre Ext]); %#ok<ASGLU>
    %   my_close(hFigModel, 'TimeDelay', 30);
    
    FigUtils.createSScFigure(55447788, 'Name', 'Residuals');

    PlotUtils.createSScPlot(model.XData, model.residuals); grid on; hold on; legend(strLegend); drawnow;
    
    %% Save parameters in a .txt file
    
    fprintf(fid, '%s %s %f\n', curvesFileName{k}, num2str(bestParams), rmsMin);
end
fclose(fid);
my_close(54656);
