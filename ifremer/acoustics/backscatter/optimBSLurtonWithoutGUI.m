% TODO : non restest� depuis conversion den ClModel, ClOptim, etc ...
% Appel� uniquement � partir des fonctions dimitrios01, ...
% voir optimBSLurtonWithoutGUI_new si pb

function optim = optimBSLurtonWithoutGUI(bilan, varargin)

[varargin, InitModel] = getPropertyValue(varargin, 'InitModel', []);
[varargin, MaxIter]   = getPropertyValue(varargin, 'MaxIter',   []);

[varargin, NoTransitory] = getFlag(varargin, 'NoTransitory'); %#ok<ASGLU>

XData   = bilan{1}.x;
YData   = bilan{1}.y';
Weights = bilan{1}.nx';

sub = find((XData >= -80) & (XData <= 80));
XData = XData(sub);
YData = YData(sub);
Weights = Weights(sub);

sub = find(~isnan(XData) & ~isnan(YData) & ~isnan(Weights));
XData = XData(sub);
YData = YData(sub);
Weights = Weights(sub);

coef = tan(abs(XData) * (pi / 180));
Weights = Weights ./ (coef + 1);

maxYData  = max(YData);
minYData  = min(YData);
meanYData = mean(YData);

if isempty(InitModel)
    %     if isfield(bilan{1}, 'model') && ~isempty(bilan{1}.model)
    %         valParams    = get_valParams(bilan{1}.model);
    %         valminParams = get_minvalue(bilan{1}.model);
    %         valmaxParams = get_maxvalue(bilan{1}.model);
    %     else
    valParams    = [maxYData    6.0 meanYData 2 minYData 20 0];
    valminParams = [minYData    0.1 minYData  0 minYData 20 0];
    valmaxParams = [maxYData+3 20.0 maxYData  4 maxYData+3 max(abs(XData))-10 max(abs(XData))];
    if NoTransitory
        valParams(5) = -1000;
        valminParams(5) = -1000;
        locked = [0 0 0 0 1 0 1];
    else
        locked = [0 0 0 0 0 0 0];
    end
    %     end
else
    valParams    = InitModel.getParamsValue();
    valminParams = InitModel.getParamsMinValue();
    valmaxParams = InitModel.getParamsMaxValue();
    
    if NoTransitory
        valParams(5) = -1000;
        valminParams(5) = -1000;
        locked = [0 0 0 0 1 0 1];
    else
        locked = [0 0 0 0 0 0 0];
    end
end

%% Cr�ation de mod�les

model_BSLurton = BSLurtonModel('XData', XData, 'YData', YData, ...
    'ValParams', valParams, 'ValMinParams', valminParams, 'ValMaxParams', valmaxParams, ...
    'Enable', ~locked, 'WeightsData', Weights);
% model_BSLurton.plot();
% model_BSLurton.editProperties();

%% D�claration d'une instance d'ajustement

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% On impose le Simplex pour le moment car Newton a l'air fatigu� :
% chercher l'erreur
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

optim = ClOptim('XData', x, 'YData', y, ...
    'xlabel', 'Angle d''indidence', ...
    'ylabel', 'Reflectivite angulaire', ...
    'Name', 'Reflectivite angulaire', ...
    'AlgoType', 1, 'UseSimAnneal', 0, 'models', model_BSLurton, ...
    'MaxIter', MaxIter, 'currentModel', 1, 'UseSimAnneal', 1, ...
    'UseWeights', 1, 'UseDependences', 1);
% optim.editProperties();
% optim.plot  
optim.optimize();
% optim.plot  
