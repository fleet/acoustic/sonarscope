function optim = optimBSLurtonWithoutGUI_new(XData, YData, Weights, varargin)

[varargin, InitModel] = getPropertyValue(varargin, 'InitModel', []);
[varargin, MaxIter]   = getPropertyValue(varargin, 'MaxIter',   []);

[varargin, NoTransitory] = getFlag(varargin, 'NoTransitory'); %#ok<ASGLU>

sub = find((XData >= -80) & (XData <= 80));

XData = XData(sub);
YData = YData(sub);
Weights = Weights(sub);

sub = find(~isnan(XData) & ~isnan(YData) & ~isnan(Weights));
XData = XData(sub);
YData = YData(sub);
Weights = Weights(sub);

coef = tan(abs(XData) * (pi / 180));
Weights = Weights ./ (coef + 1);

maxYData  = max(YData);
minYData  = min(YData);
meanYData = mean(YData);

if isempty(InitModel)
    %     if isfield(bilan{1}, 'model') && ~isempty(bilan{1}.model)
    %         valParams    = get_valParams(bilan{1}.model);
    %         valminParams = get_minvalue(bilan{1}.model);
    %         valmaxParams = get_maxvalue(bilan{1}.model);
    %     else
    valParams    = [maxYData    6.0 meanYData 2 minYData 20 0];
    valminParams = [minYData    0.1 minYData  0 minYData 20 0];
    valmaxParams = [maxYData+3 20.0 maxYData  4 maxYData+3 max(abs(XData))-10 max(abs(XData))];
    if NoTransitory
        valParams(5) = -1000;
        valminParams(5) = -1000;
        valEnable = [1 1 1 1 0 0 0];
    else
        valEnable = [1 1 1 1 1 1 1];
    end
    %     end
else
    valParams    = InitModel.getParamsValue();
    valminParams = InitModel.getParamsMinValue();
    valmaxParams = InitModel.getParamsMaxValue();
    if NoTransitory
        valParams(5) = -1000;
        valminParams(5) = -1000;
        valEnable = [1 1 1 1 0 1 0];
    else
        valEnable = [1 1 1 1 1 1 1];
    end
end

%% Cr�ation de mod�les

model_BSLurton = BSLurtonModel('XData', XData, 'YData', YData, ...
    'WeightsType', 2, 'WeightsData', Weights);

model_BSLurton.setParamsValue(valParams);
model_BSLurton.setParamsMinValue(valminParams);
model_BSLurton.setParamsMaxValue(valmaxParams);
model_BSLurton.setParamsEnable(valEnable);
% model_BSLurton.editProperties;
% model_BSLurton.plot

optim = ClOptim('XData', XData, 'YData', YData, ...
    'xLabel', 'Incidence angles', ...
    'yLabel', 'Gain', ...
    'Name', 'Gain', 'MaxIter', MaxIter);

optim.set('models', model_BSLurton);

% TODO : Gros pb � r�gler ici, Newton ne fonctionne pas bien sur cet exemple !!!!!!!!!!!!!!!!!!
optim.set('AlgoType', 1); % optim = set(optim, 'AlgoType', 1, 'UseSimAnneal', 0);

optim.optimize;

%{
optim.plot;

a = OptimDialog(optim, 'Title', 'Optim ExClOptimBS', 'windowStyle', 'normal');
a.openDialog;
optim = a.optim;
% optim.plot
ParamsValue = optim.getParamsValue;
% a.optim.editProperties;
%}
