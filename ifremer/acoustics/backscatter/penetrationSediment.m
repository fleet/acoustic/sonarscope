% Reflectivite d'une couche sedimentaire
%
% Syntax
%   reflec = penetrationSediment(zEau, alphaEau zSedim, alphaSedim )
%
% Input Arguments 
%   zEau       : Profondeur de l'interface eau/sediment (m)
%   alphaEau   : Attenuation dans l'eau (dB/km)
%   zSedm      : Profondeur du sediment
%   alphaSedim : Attenuation dans le sediment (dB/lambda)
%
% Output Arguments
%   []         : Auto-plot activation
%   reflec     : Indice de retrodiffusion total.
%
% Examples 
%   zEau = 0:100:6000; alphaEau = 0.15; lambda = 0.5;
%   zSedim = 0:lambda:(100*lambda); alphaSedim = 0.5;
%   penetrationSediment( zEau, alphaEau, zSedim, alphaSedim );
%   reflec = penetrationSediment( zEau, alphaEau, zSedim, alphaSedim );
%
% See also cli_pasp Authors
% Authors : JMA + XL
% VERSION  : $Id: penetrationSediment.m,v 1.3 2003/05/07 14:09:41 sleconte Exp $
%------------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HISTORIQUE DEVELOPPEMENT
%   02/11/00 - JMA - creation
%   22/03/01 - JMA - Mise en ref.
% ----------------------------------------------------------------------------

function reflec = penetrationSediment( zEau, alphaEau, zSedim, alphaSedim)

if nargin ~= 4
	help penetrationSediment
	return
end

zEau = abs(zEau(:));
alphaEau = alphaEau(:);
if length(alphaEau) ~= 1
    alphaEau = repmat(alphaEau, 1, length(zSedim));
end

zSedim = abs(zSedim(:)');

he = repmat(zEau, 1, length(zSedim));
hs = repmat(zSedim, length(zEau), 1);
reflec = 20 * log10(2*(he + hs)) + (2 * alphaEau .* he/1000 + 2 * alphaSedim .* hs);

% -----------------------------------------
% Sortie des parametre ou traces graphiques

if nargout == 0
    figure
    imagesc(zSedim, zEau, reflec); colorbar;
    xlabel('Penetration (m)'); ylabel('Profondeur (m)');
    title('Reflectivite des sediments');
end
