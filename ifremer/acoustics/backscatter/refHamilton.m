% Liste des paramètres de la table de Hamilton revisitee par Xavier
%
% Syntax
%   [Parametres, TypeFond] = refHamilton(Frequence, Indices)
%
% Input Arguments
%   Frequence : Frequence du sondeur
%
% Name-Value Pair Arguments
%   Indices   : Indices des type de fonds (1 a 9)
%
% Name-Value Pair Arguments
%   'c_eau'  : Celerite de l'eau (m/s) (1528 par defaut)
%   'Angles' : Angles de trace
%   'Data'   : Donnees
%
% Output Arguments
%   []         : Auto-plot activation
%   Parametres : InParametres de Jackson
%   TypeFond   : Noms des types de fond
%
% Examples
%   Frequence = 300;
%   refHamilton(Frequence)
%   refHamilton(Frequence, 9)
%   refHamilton(Frequence, 7:9)
%   [Parametres, TypeFond] = refHamilton(Frequence)
%
%   Frequence = 100;
%   Angles = 0:80; Parametres = refHamilton(Frequence);
%   reflec = BSJackson(Angles, Parametres(5, :));
%   Bruit = 2 * (rand(size(Angles)) - 0.5);
%   refHamilton(Frequence, 7:9, 'Angles', Angles, 'Data', reflec+Bruit)
%
% See also refJacksonAPL BSJackson Authors
% References : references de l'article XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
% Authors : JMA
%------------------------------------------------------------------------------

function [Parametres, TypeFond] = refHamilton(Frequence, varargin)

[varargin, c_eau] = getPropertyValue(varargin, 'c_eau', 1528);
[varargin, Angles] = getPropertyValue(varargin, 'Angles', 0:90);
[varargin, Data] = getPropertyValue(varargin, 'Data', []);

%SedimentName  					 			Mz(F)   rs/rw   Cs/Cw   d       a(dB/l) s2      g       w2
lig{1}	= 'Clay                 			9		1.2     0.98     0.01374 0.08   0.0004   3.25    0.0005';
lig{2}	= 'SiltyClay       					8		1.3     0.99     0.01374 0.10   0.0004   3.25    0.0005';
lig{3}	= 'ClayeySilt                   	7		1.5     1.01     0.01374 0.15   0.0004   3.25    0.0005';
lig{4}	= 'Sand/Silt/Clay    				6       1.6     1.04     0.01705 0.20   0.0004   3.25    0.0005';
lig{5}	= 'Sand/Silt        		    	5       1.7     1.07     0.01667 1.     0.0004   3.25    0.0005';
lig{6}	= 'SiltySand      		            4       1.8     1.10     0.0163  1.10   0.0004   3.25    0.001' ;
lig{7}	= 'VeryFineSand  	                3       1.9     1.12     0.01638 1.     0.0004   3.25    0.002' ;
lig{8}	= 'FineSand                			2       1.95    1.15     0.01645 0.80   0.0004   3.25    0.003' ;
lig{9}	= 'CoarseSand     					1       2       1.20     0.01624 0.90   0.0004   3.25    0.007' ;
%SedimentName  					 			Mz(F)   rs/rw   Cs/Cw   d       a(dB/l) s2      g       w2


if isempty(varargin)
    sub = 1:length(lig);
else
    sub = varargin{1};
end

NbTypeFond = length(sub);
Parametres = NaN(NbTypeFond, 9);
for k=1:NbTypeFond
    mots = strsplit(lig{sub(k)});
    TypeFond{k} = mots{1}; %#ok
    Parametres(k,1) = Frequence; 		                  % Frequence
    Parametres(k,2) = c_eau; 			                  % c_eau
    Parametres(k,3) = str2double(mots{4});                % nu = c_sed/c_eau
    Parametres(k,4) = str2double(mots{3});                % ro = rho_sed/rho_eau
    Parametres(k,5) = str2double(mots{8});                % gam = g
    Parametres(k,6) = str2double(mots{9});                % w2
    Parametres(k,7) = reflec_Enr2dB(str2double(mots{7})); % s2 en dB
    Parametres(k,8) = str2double(mots{6});                % beta = a(dB/l)
    Parametres(k,9) = str2double(mots{2});
end

if nargout == 0
    
    %% Affichage des courbes
    
    BSJackson(Angles, Parametres);
    if NbTypeFond == 1
        title(TypeFond);
    else
        legend(TypeFond); %, 'Location', 'eastoutside');
    end
    
    if ~isempty(Data)
        
        %% Affichage des donnees
        
        hold on;
        plot(Angles, Data, '+k');
        hold off
    end
end
