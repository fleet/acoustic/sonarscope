% Liste des param�tres de la table de Jackson
%
% Syntax
%   [Parametres, TypeFond] = refJacksonAPL(Frequence, ...)
%
% Input Arguments
%   Frequence : Frequence du sondeur
%
% Name-Value Pair Arguments
%   Indices   : Indices des type de fonds (1 a 23)
%
% Name-Value Pair Arguments
%   'c_eau'  : Celerite de l'eau (m/s) (1528 par defaut)
%   'Angles' : Angles de trace
%   'Data'   : Donnees
%
% Output Arguments
%   []         : Auto-plot activation
%   Parametres : InParametres de Jackson.
%   TypeFond   : Noms des types de fond.
%
% Examples
%   Frequence = 300;
%   refJacksonAPL(Frequence)
%   refJacksonAPL(Frequence, 10)
%   refJacksonAPL(Frequence, 10:12)
%   [Parametres, TypeFond] = refJacksonAPL(Frequence)
%
%   Frequence = 100;
%   Angles = 0:80; Parametres = refJacksonAPL(Frequence);
%   reflec = BSJackson(Angles, Parametres(12, :));
%   Bruit = 2 * (rand(size(Angles)) - 0.5);
%   refJacksonAPL(Frequence, 11:15, 'Angles', Angles, 'Data', reflec+Bruit)
%
% See also refJackson BSJackson Authors
% References : references de l'article XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
% Authors : JMA
%------------------------------------------------------------------------------

function [Parametres, TypeFond] = refJacksonAPL(Frequence, varargin)

[varargin, c_eau] = getPropertyValue(varargin, 'c_eau', 1528);
[varargin, Angles] = getPropertyValue(varargin, 'Angles', 0:90);
[varargin, Data] = getPropertyValue(varargin, 'Data', []);

%SedimentName  					 			Mz(F)   rs/rw   Cs/Cw   d       a(dB/l) s2      g       w2
lig{1}	= 'RoughRock              			NaN		2.5     2.5     0.01374 0.750   0.002   3.25    0.20693';
lig{2}	= 'Rock            					NaN		2.5     2.5     0.01374 0.750   0.002   3.25    0.01862';
lig{3}	= 'Cobble/Gravel/Pebble         	NaN		2.5     1.8     0.01374 0.750   0.002   3.25    0.016';
lig{4}	= 'SandyGravel    					-1      2.492   1.337   0.01705 0.931   0.002   3.25    0.012937';
lig{5}	= 'VeryCoarseSand        			-0.5    2.401   1.3067  0.01667 0.910   0.002   3.25    0.010573';
lig{6}	= 'MuddySandyGravel      			0       2.314   1.2778  0.0163  0.890   0.002   3.25    0.008602';
lig{7}	= 'CoarseSand/GravellySand  	    0.5     2.231   1.2503  0.01638 0.894   0.002   3.25    0.006957';
lig{8}	= 'GravellyMuddySand     			1       2.151   1.2241  0.01645 0.898   0.002   3.25    0.005587';
lig{9}	= 'MediumSand     					1.5     1.845   1.1782  0.01624 0.886   0.002   3.25    0.004446';
lig{10}	= 'MuddyGravel   					2       1.615   1.1396  0.0161  0.879   0.002   3.25    0.003498';
lig{11}	= 'FineSand/SiltySand   			2.5     1.451   1.1073  0.01602 0.874   0.002   3.25    0.002715';
lig{12}	= 'MuddySand      					3       1.339   1.08    0.01728 0.943   0.002   3.25    0.00207';
lig{13}	= 'VeryFineSand  					3.5     1.268   1.0568  0.01875 1.023   0.002   3.25    0.001544';
lig{14}	= 'ClayeySand     					4       1.224   1.0364  0.02019 1.102   0.002   3.25    0.001119';
lig{15}	= 'CoarseSilt     					4.5     1.195   1.0179  0.02158 1.178   0.002   3.25    0.000781';
lig{16}	= 'SandySilt/GravellyMud   			5       1.169   0.9999  0.01261 0.688   0.002   3.25    0.000518';
lig{17}	= 'MediumSilt/Sand-silt-clay  	    5.5     1.149   0.9885  0.00676 0.369   0.001   3.25    0.000518';
lig{18}	= 'SandyMud       					6       1.149   0.9873  0.00386 0.211   0.001   3.25    0.000518';
lig{19}	= 'FineSilt/clayeySilt  			6.5     1.148   0.9861  0.00306 0.167   0.001   3.25    0.000518';
lig{20}	= 'SandyClay      					7       1.147   0.9849  0.00242 0.132   0.001   3.25    0.000518';
lig{21}	= 'VeryFineSilt  					7.5     1.147   0.9837  0.00194 0.106   0.001   3.25    0.000518';
lig{22}	= 'SiltyClay      					8       1.146   0.9824  0.00163 0.089   0.001   3.25    0.000518';
lig{23}	= 'Clay    							9       1.145   0.98    0.00148 0.081   0.001   3.25    0.000518';
%SedimentName  					 			Mz(F)   rs/rw   Cs/Cw   d       a(dB/l) s2      g       w2

if isempty(varargin)
    sub = 1:length(lig);
else
    sub = varargin{1};
end

NbTypeFond = length(sub);
for k=1:NbTypeFond
    mots = strsplit(lig{sub(k)});
    TypeFond{k} = mots{1}; %#ok<AGROW>
    Parametres(k,1) = Frequence;								%#ok<AGROW> % Frequence
    Parametres(k,2) = c_eau;									%#ok<AGROW> % c_eau
    Parametres(k,3) = str2double(mots{4});						%#ok<AGROW> % nu = c_sed/c_eau
    Parametres(k,4) = str2double(mots{3});						%#ok<AGROW> % ro = rho_sed/rho_eau
    Parametres(k,5) = str2double(mots{8});						%#ok<AGROW> % gam = g
    Parametres(k,6) = str2double(mots{9});						%#ok<AGROW> % w2
    Parametres(k,7) = reflec_Enr2dB(str2double(mots{7}));		%#ok<AGROW> % s2 en dB
    Parametres(k,8) = str2double(mots{6});						%#ok<AGROW> % beta = a(dB/l)
    Parametres(k,9) = str2double(mots{2});						%#ok<AGROW> % beta = a(dB/l)
end

if nargout == 0
    
    %% Affichage des courbes
    
    BSJackson( Angles, Parametres);
    if NbTypeFond == 1
        title(TypeFond);
    else
        legend(TypeFond);
    end
    
    if ~isempty(Data)
        
        %% Affichage des donn�es
        
        hold on;
        plot(Angles, Data, '+k');
        hold off
    end
end
