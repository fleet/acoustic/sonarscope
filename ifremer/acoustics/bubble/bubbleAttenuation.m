% Calcul des pertes de propagation aller_retour dues a une population de bulles
%
% Syntax
%   attenuation = bubbleAttenuation(Angle, Frequence, Immersion, Profondeur, Vent )
%
% Input Arguments
%   Angle       :  Angle d'emission du sondeur (deg/vericale)
%   Frequence   :  Frequence d'emission du sondeur (kHz)
%   Immersion   :  Immersion du sondeur (m)
%   Profondeur  :  Profondeur de la cible depuis la surface (m)
%   Vent        :  Vitesse du Vent de surface (m/s)
%
% Output Arguments
%   []          :  Auto-plot activation
%   attenuation :  pertes totales (dB).
%
% Examples
%   IMM  = 3:0.2:5;
%   A    = 50:10:80;
%   Abis = 50:5:80;
%   attenuation = bubbleAttenuation( 30, 10, 4, 20, 15 )
%   attenuation = bubbleAttenuation( 30, 10, IMM, 12, 15 )
%   attenuation = bubbleAttenuation( Abis, 10, 4, 12, 15 )
%   attenuation = bubbleAttenuation( A, 10, IMM, 12, 15 )
%
% Remarks    : resolution de l'equation n^ 30
% See also bubbleRateAttenuation bubbleResonantRadius bubbleComplexCelerity windSpeed plotBubbleAttenuation
% References : Marshall V. Hall ,Wind-generated bubbles ,J.Acoust.Soc.Am.,Vol.86,No.3,September 1989
% Authors : PYT
% VERSION    : $Id: bubbleAttenuation.m,v 1.2 2002/06/20 12:01:23 augustin Exp $
%------------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HISTORIQUE DEVELOPPEMENT
%   14/09/01 - PYT - creation
% ----------------------------------------------------------------------------


function  attenuation = bubbleAttenuation(Angle, Frequence, Immersion, Profondeur, Vent )

if nargin ~= 5
    help bubbleAttenuation
    return
end

if nargout == 0
    plotBubbleAttenuation(Angle, Frequence, Immersion, Profondeur, Vent )
    return
end

nA = length(Angle);
nF = length(Frequence);
nI = length(Immersion);
nP = length(Profondeur);
nV = length(Vent);

if isequal([nA nF nI nP nV], [1 1 1 1 1])
    angleH      = 90 - abs(Angle);
    Immersion  = abs(Immersion);
    Profondeur = abs(Profondeur);
    if Profondeur > 12
        Profondeur = 12;
    end
    depth       = linspace(Immersion, Profondeur, 32); %domaine d'integration
    attenuation = 2 * trapz(depth, integrand(depth, angleH, Frequence, Immersion, Vent)); %eq30
else
    for iA = 1:nA
        for iF = 1:nF
            for iI = 1:nI
                for iP = 1:nP
                    for iV = nV
                        attenuation(iA, iF, iI, iP, iV) = bubbleAttenuation(Angle(iA), Frequence(iF), Immersion(iI), Profondeur(iP), Vent(iV) );
                    end
                end
            end
        end
    end
    attenuation = squeeze(attenuation);
end



% --------------------------------------------------
% Calcul de l'attenuation locale a chaque profondeur

function Integrand = integrand(p1, a1, f1, imm1, v1)
Integrand = zeros(size(p1));
for q=1:length(p1)
    p1q          = p1(q);
    Integrand(q) = bubbleRateAttenuation(f1, p1q, v1) / real( sqrt(1-(anglelocal(a1, f1, imm1, p1q, v1)^2)) );
end


% ----------------------------
% Calcul de l'angle local eq29

function psi = anglelocal(angl, freq, immer, prof, ven)

Ar   = bubbleResonantRadius(freq, prof);
ray  = eps: ((Ar-eps) / 100) :Ar;

ArS  = bubbleResonantRadius(freq, immer);
rayS = eps: ((ArS-eps) / 100) :ArS;
V    = bubbleComplexCelerity(rayS, freq, immer, ven, ArS) / cos(angl * pi / 180); %determination de la constante 'velocity' V eq29

psi  = bubbleComplexCelerity(ray, freq, prof, ven, Ar) / V; %sortie de la loi de cos( phi(z) ), eq29
