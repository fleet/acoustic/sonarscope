% Calcul du complexe celerite au travers des bulles
%
% Syntax
%   vitessebulles = bubbleComplexCelerity(domaine, frequence, profondeur, vent, Ar  )
%
% Input Arguments
%   domaine          :  vecteur d'integration [0,Ar] (m)
%   frequence        :  frequence d'emission du sondeur (kHz)
%   profondeur       :  immersion de la bulle (m)
%   vent             :  vitesse du vent de surface (m/s)
%   Ar               :  rayon de la bulle a la resonnance (m)
%
% Output Arguments
%   []               :  Auto-plot activation
%   vitessebulles     :  celerite complexe dans le milieu (m/s).
% 
% Examples 
%   bubbleComplexCelerity( [eps:1e-6:1e-4], 10, 4, 15, 1e-4 )
%
% Remarks    : determination de la loi de C issue de l'equation n^37
% See also bubbleDepth
% References : Marshall V. Hall ,Wind-generated bubbles ,J.Acoust.Soc.Am.,Vol.86,No.3,September 1989
% Authors : PYT
% VERSION    : $Id: bubbleComplexCelerity.m,v 1.2 2002/06/20 12:01:23 augustin Exp $
%------------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HISTORIQUE DEVELOPPEMENT
%   14/09/01 - PYT - creation
% ----------------------------------------------------------------------------

function vitessebulles = bubbleComplexCelerity(domaine, frequence, profondeur, vent, Ar)

if nargin ~= 5
	help bubbleComplexCelerity
	return
end

Co = 1500; %Celerite en surface (m/s)
tempo = (Co^(-2)) + (1/ (pi*((1000*frequence)^2)) ) * trapz(domaine,operateur(domaine, frequence, profondeur, vent, Ar)); %eq37
vitessebulles = tempo^-0.5;

function Operateur=operateur(r1,f1,p1,v1,Ar)

Operateur = Fa(r1,f1,p1,v1,Ar) + ((Ar./r1).^2) .* Fa((Ar^2)./r1,f1,p1,v1,Ar); %objet de l'integrale eq37

function fa = Fa(r9, f9, p9, v9, Ar)
fa = r9 .* distrib(r9,p9,v9) ./ (((Ar./r9).^2)-1 + 1i*delta(r9, f9, Ar)); %fonction F


% ------------------------------------------
% calcul de la loi de distribution des bulles


function N=distrib(r3,p3,v3)
No = 1.6e10; %m-4
N  = No * g(r3,p3) .* u(v3) .* y(p3,v3); %eq 1

function G=g(r4,p4)
a1 = (34 + 1.24*p4) * 1e-6; %eq5
a2 = 1.6*a1; %eq 6
g1 = (r4<a1) .* ((r4./a1).^2);
g2 = (r4<=a2)&(r4>=a1);
g3 = (r4>a2) .* ((a2./r4).^(4.37+(p4/2.55)^2));
g3(isnan(g3))=0; %mise a zero des NaN
G  = g1 + g2 + g3; %eq 4

function U=u(v5) %eq 9
U = (v5/13)^3;

function Y=y(z6,v6) %eq 3
Y = exp(-z6/l(v6));

function L=l(v7) %eq 10
if v7<=7.5
    L=0.4;
else
    L=0.4+0.115*(v7-7.5);
end


% --------------------------------------------------------------------------------------
%Calcul du coefficient d'amortissement d'une bulle a la resonnance (damping coefficient)


function del=delta(r3,f3,Ar)
Co = 1500; %Celerite en surface (m/s)
del= ((Ar./r3).^2) .* (imag(grandB(r3,f3))./real(grandB(r3,f3))) + (2*pi*1000*f3.*r3./Co) + deltav(r3,f3); %eq27

function delv=deltav(r0,f0)
mu = 1.4e-3; %Coefficient moleculaire de viscosite de l'eau de mer (kg/m/s)
Ro = 1030;   %Masse volumique de l'eau (kg/m^3)
delv = 4*mu ./ (Ro*2*pi*1000*f0.*(r0.^2)); %eqA13

function B=grandB(R1,F1)
Po = 1.008e5; %Pression moyenne de surface (Pa)
crochet = 3 * 1i * 0.4 ./ ( 2 * (bubbleDepth(F1)^2) .* (R1.^2) );
accolade= (1+1i) .* bubbleDepth(F1) .* R1 .* coth((1+1i) .* bubbleDepth(F1).*R1) -1;
B = 1.4*Po ./ (1-(crochet.*accolade)); %eq16
