% Calcul du niveau d'immersion d'une bulle
%
% Syntax
%   phi = bubbleDepth( frequence )
%
% Input Arguments 
%   frequence        :  frequence d'emission du sondeur (kHz)
%
% Output Arguments
%   []               :  Auto-plot activation
%   phi              :  (depth constant of bubble layer)^-1 (m^-1).
% 
% Examples 
%   bubbleDepth( 4 )
%
% Remarks    : resolution de l'equation n^ 17
% See also Authors
% References : Marshall V. Hall ,Wind-generated bubbles ,J.Acoust.Soc.Am.,Vol.86,No.3,September 1989
% Authors : PYT
%------------------------------------------------------------------------------

function phi = bubbleDepth( frequence )

D   = 1.84e-5; %Diffusivite thermique de l'air (m^2/s)
phi = sqrt(pi*1000*frequence/D); %eq 17
