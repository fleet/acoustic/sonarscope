% Calcul du niveau local d'attenuation provoque par les bulles
%
% Syntax
%   attenuation = bubbleRateAttenuation( frequence, profondeur, vent )
%
% Input Arguments 
%   frequence        :  frequence d'emission du sondeur (kHz)
%   profondeur       :  profondeur de la localite (m)
%   vent             :  vitesse du vent de surface (m/s)
%
% Output Arguments
%   []               :  Auto-plot activation
%   attenuation      :  niveau d'attenuation (dB/m).
% 
% Examples 
%   bubbleRateAttenuation( 13, 4, 15 )
%
%   P=0:8;
%   F=[1.25 2.5 5 10 20 40 50 60];
%   bubbleRateAttenuation( 13, P, 15 )
%   bubbleRateAttenuation( F, 4, 15 )
%   bubbleRateAttenuation(F, P, 15 )
%
% Remarks    : resolution de l'equation n^ 28
% See also bubbleResonantRadius, bubbleComplexCelerity
% References : Marshall V. Hall ,Wind-generated bubbles ,J.Acoust.Soc.Am.,Vol.86,No.3,September 1989
% Authors : PYT
% VERSION    : $Id: bubbleRateAttenuation.m,v 1.2 2002/06/20 12:01:23 augustin Exp $
%------------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HISTORIQUE DEVELOPPEMENT
%   11/09/01 - PYT - creation
% ----------------------------------------------------------------------------

function varargout = bubbleRateAttenuation( frequence, profondeur, vent )

if nargin ~= 3
 	help bubbleRateAttenuation
	return
end


profondeur = profondeur(:);
frequence  = frequence(:)';

for k=1:length(frequence)
    for j=1:length(profondeur)
        Ar = bubbleResonantRadius(frequence(k), profondeur(j));
        ray = eps:((Ar-eps)/1000):Ar;
        attenuation(j,k) = -(20/log(10)) * 2 * pi * frequence(k) *1000 ...
            * imag(1 ./ bubbleComplexCelerity(ray, frequence(k), profondeur(j), vent, Ar));  %eq28
    end
end


% ----------------------------------------
% Sortie des parametres ou trace graphique

if(nargout == 0)
    if(length(frequence) == 1)
        if(length(profondeur) == 1)
            varargout{1} = attenuation;	% Pour retourner reverb dans ans
        else
            figure;
            plot(attenuation, profondeur); grid on;
            xlabel('Absorbtion Rate (dB/m)'); ylabel('Depth (m)');
            s=sprintf('Niveau d''attenuation ( alpha(z) ) pour une frequence de %4.1f kHz et un vent de %4.1f m/s ',frequence,vent);
            title(s)
        end
    else
        figure
        if(length(profondeur) == 1)
            plot(frequence, attenuation); grid on;
            ylabel('Absorbtion Rate (dB/m)'); xlabel('Frequency (kHz)');
            s=sprintf('Niveau d''attenuation ( alpha(z) ) a %4.1f m de profondeur pour un vent de %4.1f m/s',profondeur,vent);
            title(s)
        else
            semilogy(profondeur,attenuation);
            grid on;
            ylabel('Absorbtion Rate (dB/m)'); xlabel('Depth (m)');
            s=sprintf('Niveau d''attenuation ( alpha(z) ) pour un vent de %4.1f m/s trace par frequence',vent);
            title(s)
            for w=1:length(frequence)
                s2{w} = sprintf('F=%4.2f kHz',frequence(w));
            end
            legend(s2)
        end
    end
else
    varargout{1} = attenuation;
end

