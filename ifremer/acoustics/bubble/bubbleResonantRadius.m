% Calcul du rayon d'une bulle a la resonnance
%
% Syntax
%   rayonres = bubbleResonantRadius( frequence, profondeur )
%
% Input Arguments 
%   frequence        :  frequence d'emission du sondeur (kHz)
%   profondeur       :  immersion de la bulle (m)
%
% Output Arguments
%   []               :  Auto-plot activation
%   rayonres         :  rayon de la bulle a la resonnance (m).
% 
% Examples 
%   bubbleResonantRadius( 10, 4 )
%
%   P=0:8;
%   bubbleResonantRadius( 10, P )
%
% Remarks    : calcul du rayon a partir des equations n^24 et 22
%
% See also bubbleDepth
% References : Marshall V. Hall ,Wind-generated bubbles ,J.Acoust.Soc.Am.,Vol.86,No.3,September 1989
% Authors : PYT
%------------------------------------------------------------------------------

function rayonres = bubbleResonantRadius( frequence, profondeur )

rayonres = ao( frequence, profondeur ) - (0.3/ bubbleDepth(frequence) ); %eq 24 ,0.3=3(gamma-1)/4,gamma=1.4
% gamma is the ratio of specific heats of the gas in the bubble



function Ao = ao(f7,p7) %eq22

Po = 1.008e5; %Pression moyenne de surface (Pa)
Ro = 1030;    %Masse volumique de l'eau (kg/m^3)
Pression = Po + p7*1.013e4; %loi de pression dans l'eau
Ao=(1/ (2*pi*1000*f7)) * sqrt(4.2*Pression/Ro); %eq 22 ,3*gamma=4.2
