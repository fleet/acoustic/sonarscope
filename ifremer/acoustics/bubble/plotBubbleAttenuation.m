% Trace automatique des courbes de pertes aller_retour dues a une population de bulles
%
% Syntax
%   plotBubbleAttenuation(Angle, Frequence, Immersion, Profondeur, Vent )
%
% Input Arguments 
%   Angle      :  Angle d'emission du sondeur (/vericale) (degres)
%   Frequence  :  Frequence d'emission du sondeur (kHz)
%   Immersion  :  Immersion du sondeur (m)
%   Profondeur :  Profondeur de la cible depuis la surface (m)
%   Vent       :  Vitesse du Vent de surface (m/s)
%
% Examples
%   Angle = [0 20 40 60 70 75];
%   IMM   = 0:2:8;
%   F     = [1.25 2.5];
%   plotBubbleAttenuation( 30, 10, IMM, 20, 15 )    % figure x de l'article
%   plotBubbleAttenuation( 30, F, 4, 20, 15 )
%   plotBubbleAttenuation( 80, F, IMM, 20, 15 )
%   plotBubbleAttenuation( Angle, 10, 2, 20, 15 )
%   plotBubbleAttenuation( Angle, 10, IMM, 20, 15 )
%
% See also bubbleAttenuation windSpeed Authors
% Authors : PYT
% VERSION    : $Id: plotBubbleAttenuation.m,v 1.2 2002/06/20 12:01:23 augustin Exp $
%------------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HISTORIQUE DEVELOPPEMENT
%   14/09/01 - PYT - creation
% ----------------------------------------------------------------------------


function  plotBubbleAttenuation(Angle, Frequence, Immersion, Profondeur, Vent )

if nargin ~= 5 
	help plotBubbleAttenuation
	return
end

attenuation = bubbleAttenuation(Angle, Frequence, Immersion, Profondeur, Vent );

nA = length(Angle);
nF = length(Frequence);
nI = length(Immersion);
nP = length(Profondeur);
nV = length(Vent);

% Tous des scalaires
if isequal([nA nF nI nP nV], [1 1 1 1 1])
    return
end

% Trace en fonction de l'angle
if isequal([nF nI nP nV], [1 1 1 1])
    figure
    plot(Angle, attenuation); grid on;
    ylabel('Integrated Attenuation (dB)'); xlabel('Angle (degre)');
    s = sprintf('Pertes (absorption+diffusion) a F=%4.1f kHz, Imm=%4.1f m, P=%4.1f m, vent=%4.1f m/s', ...
        Frequence, Immersion, Profondeur, Vent);
    title(s)
    return
end

% Trace en fonction de la frequence
if isequal([nA nI nP nV], [1 1 1 1])
    figure;
    loglog(Frequence, attenuation); grid on;
    ylabel('Integrated Attenuation (dB)'); xlabel('Frequence (kHz)');
    s=sprintf('Pertes (attenuation+diffusion) pour a=%4.1f degres, Imm=%4.1f m, P=%4.1f m, Vent=%4.1f m/s', ...
        Angle, Immersion, Profondeur, Vent);
    title(s)
    return
end

% Trace en fonction de l'immersion
if isequal([nA nF nP nV], [1 1 1 1])
    figure;
    plot(attenuation, Immersion); grid on;
    xlabel('Integrated Attenuation (dB)'); ylabel('Source Depth (m)');
    s=sprintf('Pertes (attenuation+diffusion) a %4.1f degre pour F=%4.1f kHz, P=%4.1f m, Vent=%4.1f m/s', ...
        Angle, Frequence, Profondeur, Vent);
    title(s)
    return
end

% Trace en fonction de la profondeur
if isequal([nA nF nI nV], [1 1 1 1])
    figure;
    plot(attenuation, Profondeur); grid on;
    xlabel('Integrated Attenuation (dB)'); ylabel('Depth (m)');
    s=sprintf('Pertes (attenuation+diffusion) a %4.1f degre pour F=%4.1f kHz, Imm=%4.1f m, Vent=%4.1f m/s', ...
        Angle, Frequence, Immersion, Vent);
    title(s)
    return
end

% Trace en fonction du vent
if isequal([nA nF nI nP], [1 1 1 1])
    figure;
    plot(Vent, attenuation); grid on;
    xlabel('Wind Speed (m/s)');ylabel('Integrated Attenuation (dB)');
    s=sprintf('Pertes (attenuation+diffusion) a %4.1f degre pour F=%4.1f kHz, P=%4.1f m, Imm=%4.1f m', ...
        Angle, Frequence, Profondeur, Immersion);
    title(s)
    return
end

% Trace en fonction de la profondeur et du vent
if isequal([nA nF nI], [1 1 1])
    figure
    semilogy(Profondeur, attenuation);
    grid on;
    ylabel('Integrated Attenuation (dB)'); xlabel('Depth (m)');
    s=sprintf('Pertes (attenuation+diffusion) pour a=%4.1f degres, F=%4.1f kHz, Imm=%4.1f m', ...
        Angle, Frequence, Immersion);
    title(s)
    for w=1:length(Vent)
        s2{w}=sprintf('Vent = %4.1f m/s',Vent(w));
    end
    legend(s2)
    return
end

% Trace en fonction de l'immersion et du vent
if isequal([nA nF nP], [1 1 1])
    figure
    semilogy(Immersion, attenuation);
    grid on;
    ylabel('Integrated Attenuation (dB)'); xlabel('Source Depth (m)');
    s=sprintf('Pertes (attenuation+diffusion) pour a=%4.1f degres, F=%4.1f kHz, P=%4.1f m', ...
        Angle, Frequence, Profondeur);
    title(s)
    for w=1:length(Vent)
        s2{w}=sprintf('Vent = %4.1f m/s',Vent(w));
    end
    legend(s2)
    return
end

% Trace en fonction de la frequence et du vent
if isequal([nA nI nP], [1 1 1])
    disp('CAS PAS ENCORE PREVU')
    return
end

% Trace en fonction de l'angle et du vent
if isequal([nF nI nP], [1 1 1])
    disp('CAS PAS ENCORE PREVU')
    return
end

% Trace en fonction de l'immersion et de la profondeur
if isequal([nA nF nV], [1 1 1])
    disp('CAS PAS ENCORE PREVU')
    return
end

% Trace en fonction de la frequence et de la profondeur
if isequal([nA nI nV], [1 1 1])
    figure
    semilogy(Profondeur, attenuation);
    grid on;
    ylabel('Integrated Attenuation (dB)'); xlabel('Depth (m)');
    s=sprintf('Pertes (attenuation+diffusion) pour a=%4.1f degres, Imm=%4.1f m, Vent=%4.1f m/s', ...
        Angle, Immersion, Vent);
    title(s)
    for w=1:length(Frequence)
        s2{w}=sprintf('f = %4.1f kHz',Frequence(w));
    end
    legend(s2)
    return
end

% Trace en fonction de l'angle et de la profondeur
if isequal([nF nI nV], [1 1 1])
    figure
    semilogy(Profondeur, attenuation);
    grid on;
    ylabel('Integrated Attenuation (dB)'); xlabel('Depth (m)');
    s = sprintf('Pertes (absorption+diffusion) a F=%4.1f kHz, Imm=%4.1f m, vent=%4.1f m/s', ...
        Frequence, Immersion, Vent);
    title(s)
    for w=1:length(Angle)
        s2{w}=sprintf('a = %4.1f degres', Angle(w));
    end
    legend(s2)
    return
end

% Trace en fonction de la Frequence et de l'immersion
if isequal([nA nP nV], [1 1 1])
    figure
    semilogy(Immersion, attenuation);
    grid on;
    ylabel('Integrated Attenuation (dB)'); xlabel('Source Depth (m)');
    s=sprintf('Pertes (attenuation+diffusion) pour a=%4.1f degres, P=%4.1f m, Vent=%4.1f m/s', ...
        Angle, Profondeur, Vent);
    title(s)
    for w=1:length(Frequence)
        s2{w}=sprintf('f = %4.1f kHz',Frequence(w));
    end
    legend(s2)
    return
end

% Trace en fonction de l'angle et de l'immersion
if isequal([nF nP nV], [1 1 1])
    figure
    semilogy(Immersion, attenuation);
    grid on;
    ylabel('Integrated Attenuation (dB)'); xlabel('Source Depth (m)');
    s = sprintf('Pertes (absorption+diffusion) a F=%4.1f kHz, P=%4.1f m, vent=%4.1f m/s', ...
        Frequence, Profondeur, Vent);
    title(s)
    for w=1:length(Angle)
        s2{w}=sprintf('a = %4.1f degres', Angle(w));
    end
    legend(s2)
    return
end

% Trace en fonction de l'angle et de la frequence
if isequal([nI nP nV], [1 1 1])
    figure
    semilogy(Frequence, attenuation);
    grid on;
    ylabel('Integrated Attenuation (dB)'); xlabel('Frequency (kHz)');
    s = sprintf('Pertes (absorption+diffusion) pour a=%4.1f degres, P=%4.1f m, Imm=%4.1f m, vent=%4.1f m/s', ...
        Angle, Profondeur, Immersion, Vent);
    title(s)
    for w=1:length(Angle)
        s2{w}=sprintf('a = %4.1f degres', Angle(w));
    end
    legend(s2)
    return
end

disp('CAS PAS ENCORE PREVU')
