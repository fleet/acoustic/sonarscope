% Realisation de la mosaique de donnees des "DepthDatagrams" d'une campagne
%
% Syntax
%   [flag, Mosa, nomDirImport, nomDirExport] = Caraibes_PingBeam2LatLong(...)
%
% Name-Value Pair Arguments
%   nomDir : Nom du r�pertoire des donnees
%   resol  : Pas de la grille en m�tres
%   Backup : P�riode de sauvegarde des fichiers de sortie
%   Layers : Liste des numeros de layers a mosaiquer (voir cl_simrad_all/view_depth)
%            (Un seul layer pour le moment)
%   Rayon  : Rayon utilis� lors de l'interpolation
%
% Output Arguments
%   []           : Auto-plot activation
%   flag         : Description du parametre (unite).
%   a            : Instances de cl_image contenant la mosaique et la mosaique angulaire
%   nomDirImport : Nom du r�pertoire des .all
%   nomDirExport : Nom du r�pertoire des images cr��es
%
% Examples
%   Caraibes_PingBeam2LatLong
%   flag, a] = Caraibes_PingBeam2LatLong('NomDir', '/home1/jackson/FEMME2005/Donnees', 'Resol', 10);
%   SonarScope(a)
%   [flag, a] = Caraibes_PingBeam2LatLong('NomDir', 'D:\NIWA_DATA\TAN0105', 'Resol', 10, 'Backup', 5);
%   SonarScope(a)
%
% See also Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function [flag, Mosa, nomDirImport, nomDirExport] = Caraibes_PingBeam2LatLong(varargin)

Mosa = [];

[varargin, XLim]         = getPropertyValue(varargin, 'XLim',         []);
[varargin, YLim]         = getPropertyValue(varargin, 'YLim',         []);
[varargin, resol]        = getPropertyValue(varargin, 'Resol',        []);
% [varargin, Layers]     = getPropertyValue(varargin, 'Layers',       []);
% [varargin, Backup]     = getPropertyValue(varargin, 'Backup',       []);
[varargin, nomDirImport] = getPropertyValue(varargin, 'nomDirImport', pwd);
[varargin, nomDirExport] = getPropertyValue(varargin, 'nomDirExport', pwd);
[varargin, window]       = getPropertyValue(varargin, 'window',       []); %#ok<ASGLU>
% [varargin, MasqueActif] = getPropertyValue(varargin, 'MasqueActif', []);

I0 = cl_image_I0;
E0 = cl_ermapper([]);

%% Question for Mask

%{
[flag, MasqueActif] = question_UseMask(MasqueActif, 'QL', 3);
if ~flag
return
end
%}

%% Recherche des fichiers .mbg

[flag, liste] = uiSelectFiles('ExtensionFiles', '.mbg', 'RepDefaut', nomDirImport);
if ~flag || isempty(liste)
    return
end

nbProfils = length(liste);
nomDirImport = fileparts(liste{1});

%% S�lection du layer � traiter

in = cl_netcdf('fileName', liste{1});
[dim, nomDim] = get_varDim(in, 'mbDepth'); %#ok<ASGLU>

Carto = get_Caraibes_carto(liste{1});


% mbSounder = get_valAtt(in, 'mbSounder');
% SonarIdent = SonarIdent_caraibes2sonarScope(mbSounder);

% [listVarNum, listeLayers] = find_listVarAssociated2Dim(in, nomDim); %#ok
% if isempty(Layers)
%     str1 = 'Layer � mosa�quer';
%     str2 = 'Layer to mosaic';
%     [choixLayer, flag] = my_listdlg(Lang(str1,str2), listeLayers, 'InitialValue', 3, 'SelectionMode', 'Single');
%     if ~flag
%         return
%     end
%     LayerName = listeLayers{choixLayer};
% end
LayerName = 'mbDepth';

%% Grid size

[flag, resol] = get_gridSize('Value', -resol, 'Initial', 10);
if ~flag
    return
end

%% Backup frequency

[flag, Backup] = saisie_backupFrequency(nbProfils);
if ~flag
    return
end

%% Across distance interpolation method

[flag, AcrossInterpolation] = question_AcrossDistanceInterpolation('QL', 3);
if ~flag
    return
end

%% Mosaic interpolation window

[flag, window] = question_MosaicInterpolationWindow(window);
if ~flag
    return
end

%% Noms des fichiers de sauvegarde des mosa�ques

switch LayerName
    case 'mbDepth'
        NomGenerique = 'DTM';
        DataType = cl_image.indDataType('Bathymetry');
    otherwise
        NomGenerique = '???';
        DataType = cl_image.indDataType('Unknown');
end

[nomFicErMapperLayer, flag] = initNomFicErs(E0, NomGenerique, ...
    cl_image.indGeometryType('LatLong'), DataType, nomDirExport, 'resol', resol);
if ~flag
    return
end
[nomDirExport, TitreDepth] = fileparts(nomFicErMapperLayer);
DataType = ALL.identSonarDataType('BeamDepressionAngle');
[nomFicErMapperAngle, flag] = initNomFicErs(E0, TitreDepth, cl_image.indGeometryType('LatLong'), ...
    DataType, nomDirExport, 'confirm', (get_LevelQuestion >= 3));
if ~flag
    return
end
[~, TitreAngle] = fileparts(nomFicErMapperAngle);

%% R�cuperation des mosa�ques existantes

if exist(nomFicErMapperLayer, 'file') && exist(nomFicErMapperAngle, 'file')
    [flag, rep] = question_CompleteExistingMosaic;
    if ~flag
        return
    end
    if rep == 1
        [flag, Z_Moza_All] = cl_image.import_ermapper(nomFicErMapperLayer);
        if ~flag
            return
        end
        Carto = get(Z_Moza_All, 'Carto');
        
        [flag, A_Moza_All]  = cl_image.import_ermapper(nomFicErMapperAngle);
        if ~flag
            return
        end
    else
        Z_Moza_All = [];
        A_Moza_All = [];
    end
else
    Z_Moza_All = [];
    A_Moza_All = [];
end

%% Si Mosaique de Depth, on propose de faire la correction de mar�e

%{
[flag, flagTideCorrection] = question_TideCorrection;
if ~flag
    return
end

% TODO : utiliser [flag, TideFile] = inputTideFile(isBathy, nomDirImport) � la place de tout ce qui suit
if flagTideCorrection% && any((choixLayer == 1) || (choixLayer == 24))    % Depth ou Depth-Heave
    [flag, TideFile] = my_uigetfile({'*.tid*;*.mar','Tide Files (*.tid*,*.mar)';
        '*.tid*',  'Tide file (*.tid*)'; ...
        '*.mar','SHOM tide file (*.mar)'; ...
        '*.*',  'All Files (*.*)'}, 'Give a tide file (cancel instead)');
    if ~flag
        TideFile = [];
    end
else
    TideFile = [];
end
%}

%% Si Mosaique de r�flectivit�, on propose de faire une compensation

% TODO : remplacer ce qui suit par [flag, CorFile, ModeDependant, UseModel, PreserveMeanValueIfModel] = QuestionCompensationCurve(nomDirImport, varargin)
[rep, flag] = my_questdlg(Lang('Voulez-vous faire une compensation ?', 'Do you want to do a compensation ?'));
if ~flag
    return
end
if rep == 1
    [flag, CorFile] = my_uigetfile('*.mat', 'Give a compensation file (cancel instead)', nomDirExport);
    if flag
        CourbeConditionnelle = load_courbesStats(CorFile);
        bilan = CourbeConditionnelle.bilan;
    else
        CorFile = [];
        bilan = [];
    end
else
    CorFile = [];
    bilan = [];
end

%% Filtre spike

% if Bathy seulement
[repSpikeRemoval, flag] = my_questdlg(Lang('Voulez-vous faire une correction de spike ?', 'Do you want to do a spike removal ?'));
if ~flag
    return
end
if repSpikeRemoval == 1
    p = ClParametre('Name', Lang('Seuil', 'Threshold'), 'Unit', 'm', 'MinValue', 0.1, 'MaxValue', 50, 'Value', 2);
    a = StyledSimpleParametreDialog('params', p, 'Title', 'Spike filter');
    a.openDialog;
    flag = a.okPressedOut;
    if ~flag
        return
    end
    seuilSpike = a.getParamsValue;
end

%% Moustache removal

[repMoustacheRemoval, flag] = my_questdlg(Lang('Voulez-vous faire une correction de "moustache" ?', 'Do you want to do a "moustache" removal ?'));
if ~flag
    return
end
if repMoustacheRemoval == 1
    p = ClParametre('Name', Lang('Angle max', 'Max Angle'), 'Unit', 'deg', 'MinValue', 0, 'MaxValue', 25, 'Value', 12);
    a = StyledSimpleParametreDialog('params', p, 'Title', 'Moustache filter');
    a.openDialog;
    flag = a.okPressedOut;
    if ~flag
        return
    end
    AngleMoustache = a.getParamsValue;
end

%% Recouvrements de profils

if get_LevelQuestion >= 3
    str1 = 'Priorit� donn�e pour le recouvrement de profil : ';
    str2 = 'Priority to be used for recovering profiles : ';
    [CoveringPriority, flag] = my_questdlg(Lang(str1,str2), Lang('Centre', 'Center'), Lang('Extr�mit�s', 'Borders'), 'ColorLevel', 3);
    if ~flag
        return
    end
else
    CoveringPriority = 1;
end

% profile on
% tic
indLayerImport      = [];
SonarIdent          = [];
ListeFlagsInvalides = [];
for k=1:nbProfils
    fprintf('Processing file %d/%d : %s\n', k, nbProfils, liste{k});
    
    a = cl_netcdf('fileName', liste{k});
    [listVarNum, listeLayers] = find_listVarAssociated2Dim(a, nomDim); %#ok
    
    %     listeLayersSupplementaires = []; % Ca pourrait �tre TxBeamIndex
    
    if isempty(ListeFlagsInvalides)
        [flag, ListeFlagsInvalides] = paramsFlagCaraibes;
        if ~flag
            return
        end
    end
    
    [c, Carto, indLayerImport, SonarIdent, ListeFlagsInvalides] = import_CaraibesMbg(I0, liste{k}, 'Carto', Carto, ...
        'Sonar.Ident', SonarIdent, 'indLayerImport', [1 2 3], ...
        'ListeFlagsInvalides', ListeFlagsInvalides, 'indLayerImport', indLayerImport);
    
    DT = cl_image.indDataType('Bathymetry');
    indLayer = findIndLayerSonar(c, 1, 'DataType', DT, 'WithCurrentImage', 1);
    Z = c(indLayer);
    
    nbRows = Z.nbRows;
    if nbRows <= 1
        Message = sprintf('The file %s is discarded because it has %d row.', liste{k}, nbRows);
        my_warndlg(Message, 0);
        continue
    end
    
    DT = cl_image.indDataType('AcrossDist');
    indLayer = findIndLayerSonar(c, 1, 'DataType', DT, 'WithCurrentImage', 1);
    AcrossDistance = c(indLayer);
    
    DT = cl_image.indDataType('AlongDist');
    indLayer = findIndLayerSonar(c, 1, 'DataType', DT, 'WithCurrentImage', 1);
    AlongDistance = c(indLayer);
    
    DT = cl_image.indDataType('TxAngle');
    indLayer = findIndLayerSonar(c, 1, 'DataType', DT, 'WithCurrentImage', 1);
    if isempty(indLayer) % Cas avec version V2 des datagrams
        DT = cl_image.indDataType('RxBeamAngle');
        indLayer = findIndLayerSonar(c, 1, 'DataType', DT, 'WithCurrentImage', 1);
        if isempty(indLayer)
            [flag, A] = sonar_Caraibes_estimationTxAngle(Z, AcrossDistance);
            if ~flag
                return
            end
        else
            A = c(indLayer);
            A = set(A, 'DataType', cl_image.indDataType('TxAngle'));
        end
    else
        A = c(indLayer);
    end
    
    Heading        = get(Z, 'Heading');
    FishLatitude   = get(Z, 'FishLatitude');
    FishLongitude  = get(Z, 'FishLongitude');
    
    FishLatitude(FishLatitude == 0) = NaN;
    FishLongitude(FishLongitude == 0) = NaN;
    
    if ~isempty(CorFile)
        %         c(5) = sonar_secteur_emission(A, []);
        [Z, flag] = compensationCourbesStats(c(1), c(indexLayersCompensation), bilan);
        if ~flag
            return
        end
    end
    
    %% Filtre spike
    
    if repSpikeRemoval == 1
        Z =  filterSpikeBathy(Z, seuilSpike);
        Z = Z(1);
    end
    
    if repMoustacheRemoval == 1
        [flag, ZM] = Sonar_MoustacheRemoval(Z, A, AngleMoustache);
        if ~flag
            return
        end
        Z = ZM(1);
    end
    
    %% Correction de heave si possible
    
    %     if choixLayer == 1
    %         Heave = get(Z, 'Heave');
    %         if isempty(Heave)
    % %              Height = -get(Z, 'Height');
    %             Height = mean_lig(Z);
    %
    %            [ButterB,ButterA] = butter(2, 0.05);
    %             subNaN = find(isnan(Height));
    %             subNonNaN = find(~isnan(Height));
    %             Height(subNaN) = interp1(subNonNaN, Height(subNonNaN), subNaN);
    %             Heightfiltree = filtfilt(ButterB, ButterA, double(Height));
    % %             figure; plot(Height, 'b'); grid on
    % %             hold on; plot(Heightfiltree, 'r'); grid on
    %             Heave = Height - Heightfiltree;
    %         end
    %         Z = Z - Heave;
    %     end
    
    %% Compensation de donnees si possible
    
    %     if choixLayer == 9
    %
    %     end
    
    %% Recherche des pings dans la zone d'�tude
    
    if isempty(XLim) || isempty(YLim)
        suby = 1:Z.nbRows;
    else
        SonarFishLatitude  = get(Z, 'SonarFishLatitude');
        SonarFishLongitude = get(Z, 'SonarFishLongitude');
        suby = find((SonarFishLongitude >= XLim(1)) & ...
            (SonarFishLongitude <= XLim(2)) & ...
            (SonarFishLatitude >= YLim(1)) & ...
            (SonarFishLatitude <= YLim(2)));
        clear SonarFishLatitude SonarFishLongitude
        if isempty(suby)
            clear Z AcrossDistance AlongDistance A c
            continue
        end
    end
    
    %% Calcul des coordonn�es g�ographiques
    
    [flag, LatLon] = sonar_calcul_coordGeo_BathyFais(Z, AcrossDistance, AlongDistance, ...
        Heading, FishLatitude, FishLongitude, 'suby', suby);
    if ~flag
        return
    end
    
    [flag, Z_Mosa, A_Mosa] = sonar_mosaique2(Z, A, LatLon(1), LatLon(2), resol, ...
        'AcrossInterpolation', AcrossInterpolation, 'AlongInterpolation',  1);
    if ~flag
        return
    end
    clear LatLon Z A
    
    %% Interpolation
    
    if ~isequal(window, [1 1])
        Z_Mosa = WinFillNaN(Z_Mosa, 'window', window, 'NoStats');
        A_Mosa = WinFillNaN(A_Mosa, 'window', window, 'NoStats');
    end
    
    [Z_Moza_All, A_Moza_All] = mosaique([Z_Moza_All Z_Mosa], 'Angle', [A_Moza_All A_Mosa], ...
        'FistImageIsCollector', 1, 'CoveringPriority', CoveringPriority);
    Z_Moza_All.Name = TitreDepth;
    A_Moza_All.Name = TitreAngle;
    clear Mosa
    Mosa(1) = Z_Moza_All;
    Mosa(2) = A_Moza_All;
    
    clear c
    
    %% Sauvegarde des mosaiques dans des fichiers ErMapper
    
    if (mod(k,Backup) == 0) || (k == nbProfils)
        Z_Moza_All = majCoordonnees(Z_Moza_All);
        A_Moza_All = majCoordonnees(A_Moza_All);
        export_ermapper(Z_Moza_All, nomFicErMapperLayer);
        export_ermapper(A_Moza_All, nomFicErMapperAngle);
    end
    
    %% Suppression des fichier memmapfile
    
    deleteFileOnListe(cl_memmapfile.empty, gcbf)
end
flag = 1;
% toc
% profile report

% % InitialFileName = sprintf('Not saved for the moment. Internal code for mosaic asembling : %f', rand(1));
% for k=1:length(Mosa)
%     Mosa(k) = majCoordonnees(Mosa(k));
% %     Mosa(k) = set(Mosa(k), 'InitialFileName', InitialFileName);
% end

if nargout == 0
    SonarScope(Mosa)
end
