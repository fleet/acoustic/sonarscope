function flag = NVI_summary(nomFic, nomFicSummary)
 
if ~iscell(nomFic)
    nomFic = {nomFic};
end

fid = fopen(nomFicSummary, 'w+t');
if fid == -1
    messageErreurFichier(nomFicSummary, 'WriteFailure');
    flag = 0;
    return
end

for k=1:length(nomFic)
    flag = summary_unitaire(nomFic{k}, fid);
    if ~flag
        % Message SVP
    end
end
fclose(fid);

try
    winopen(nomFicSummary)
catch %#ok<CTCH>
end


function flag = summary_unitaire(nomFic, fid)

[flag, ~, ~, Time] = lecFicNav(nomFic);
if ~flag
    return
end

fprintf(fid, '%s  :  %s\n', nomFic, summary(Time));
clear Data
flag = 1;
