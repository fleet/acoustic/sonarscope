function [flag, navRaw, nav, Type] = Nvi2ClNavigation(ListeFicNvi, Fig1, Name)

navRaw = ClNavigation.empty();
nav    = ClNavigation.empty();
Type   = [];

NbFic = length(ListeFicNvi);
if NbFic == 0
    flag   = 0;
    return
end

%% Cr�ation de la fen�tre avec les menus et callbacks sp�cialis�s pour la nav

Fig1 = plot_navigation_Figure(Fig1, 'Name', Name);

str1 = 'Lecture de la navigation';
str2 = 'Import navigation.';
hw = create_waitbar(Lang(str1,str2), 'N', NbFic);
for k=1:NbFic
    my_waitbar(k, NbFic, hw);
    fprintf('Decode "%s"\n', ListeFicNvi{k});
    
    [flag, Latitude, Longitude, SonarTime, Heading, ~, Immersion, ~, Type, Pitch, Roll, Heave] = lecFicNav(ListeFicNvi{k});
    if ~flag
        messageErreurFichier(ListeFicNvi{k}, 'ReadFailure');
        continue
    end
    
    subNaN    = find(isnan(Heading));
    subNonNaN = find(~isnan(Heading));
    if length(subNonNaN) > 1
        Heading(subNaN) = interp1(subNonNaN, Heading(subNonNaN), subNaN);
    end
    
    %% Trac� de la navigation avec l'outil usue (utile tant que l'on a pas les heures affich�es dans NavigationDialog)
    
    plot_navigation(ListeFicNvi{k}, Fig1, Longitude, Latitude, SonarTime.timeMat, Heading, 'Immersion', Immersion);
    
    %% R�cup�ration des signaux
    
    T = datetime(SonarTime.timeMat, 'ConvertFrom', 'datenum');
    timeSample = XSample('name', 'time', 'data', T);
    latSample  = YSample('data', Latitude);
    lonSample  = YSample('data', Longitude);
    
    latSignal = ClSignal('xSample', timeSample, 'ySample', latSample);
    lonSignal = ClSignal('xSample', timeSample, 'ySample', lonSample);
    
    if isempty(Immersion) || all(isnan(Immersion))
%         immersionSignal = [];
        Immersion = zeros(size(T));
        immersionSample = YSample('name', 'Immersion', 'unit', 'm', 'data', Immersion);
        immersionSignal = ClSignal('xSample', timeSample, 'ySample', immersionSample);
    else
        immersionSample = YSample('name', 'Immersion', 'unit', 'm', 'data', Immersion);
        immersionSignal = ClSignal('xSample', timeSample, 'ySample', immersionSample);
    end
    
    if isempty(Heading) || all(isnan(Heading))
        headingSignal = [];
    else
        headingSample = YSample('name', 'Heading',   'unit', 'deg', 'data', Heading);
        headingSignal = ClSignal('xSample', timeSample, 'ySample', headingSample);
    end
    
    if isempty(Pitch) || all(isnan(Pitch))
        pitchSignal = [];
        rollSignal  = [];
        HeaveSignal = [];
    else
        pitchSample = YSample('name', 'Pitch', 'unit', 'deg', 'data', Pitch);
        rollSample  = YSample('name', 'Roll',  'unit', 'deg', 'data', Roll);
        HeaveSample = YSample('name', 'Heave', 'unit', 'm',   'data', Heave);
        pitchSignal = ClSignal('xSample', timeSample, 'ySample', pitchSample);
        rollSignal  = ClSignal('xSample', timeSample, 'ySample', rollSample);
        HeaveSignal = ClSignal('xSample', timeSample, 'ySample', HeaveSample);
    end
    
    %% Cr�ation des instances de navigation
    
    [~, nomFicSeul, Ext] = fileparts(ListeFicNvi{k});
    navRaw(end+1) = ClNavigation(latSignal, lonSignal, ...
        'headingVesselSignal', headingSignal, ...
        'immersionSignal',     immersionSignal, ...
        'otherSignal',         [pitchSignal rollSignal HeaveSignal], ...
        'name',                [nomFicSeul Ext]); %#ok<AGROW>
    
    nav(end+1) = ClNavigation(latSignal, lonSignal, ...
        'headingVesselSignal', headingSignal, ...
        'immersionSignal',     immersionSignal, ...
        'otherSignal',         [pitchSignal rollSignal HeaveSignal], ...
        'name',                [nomFicSeul Ext]); %#ok<AGROW>
end
my_close(hw);
