% Creation d'un MNT au format Caraibes(R)
%
% Syntax
%   create_caraibesGrid_3_6(nomFicOut, ...)
%
% Input Arguments
%   nomFicOut : nom du fichier de sortie
%   sk_ref    : squelette de r�f�rence
%   NomLayer  : Nom du Layer
%   Depth     : Layer
%   x         : �chantillonage en x
%   y         : �chantillonage en y
%   Carto     : objet Carto associ�.
%
% Name-Value Pair Arguments
%   Sans objet
%
% Examples
%   nomFic = getNomFicDatabase('EM300_Ex2.mnt');
%   a_ref = cl_netcdf('fileName', nomFic);
%   sk_ref = get(a_ref, 'skeleton');
%   a = cl_car_ima(nomFic);
%   b = view(a);
%   c = get_Image(b, 1);
%
%   nomFicOut = [tempname '.dtm']
%   I = get(c, 'Image');
%   x = get(c, 'x');
%   y = get(c, 'y');
%   Carto = get(c, 'Carto');
%   create_caraibesGrid_3_6(nomFicOut, sk_ref, 'Depth', I, x, y, Carto);
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function create_caraibesGrid_3_6(nomFicOut, sk_ref, NomLayers, Layers, Unite, x, y, ...
    Type, Mosaic_type, Format, Sounder_type, Carto, GeometryType, varargin)

[varargin, ValNaN] = getPropertyValue(varargin, 'ValNaN', []);

[varargin, ValeursDansUnite] = getFlag(varargin, 'ValeursDansUnite');

Xmin_metric = min(x);
Xmax_metric = max(x);
Ymin_metric = min(y);
Ymax_metric = max(y);

Orientation = 0;
Number_layers = numel(Layers);% 1;

Number_lines   = size(Layers{1},1);
Number_columns = size(Layers{1},2);

resol = mean(diff(x));
Element_x_size = resol; % * 1000;
Element_y_size = resol; % * 1000;
clear resol

%% Mise � jour des Dimensions

numDim = cdf_find_numDim(sk_ref, 'LINES');
sk_ref.dim(numDim).value = Number_lines;
numDim = cdf_find_numDim(sk_ref, 'COLUMNS');
sk_ref.dim(numDim).value = Number_columns;
clear numDim

%% Nettoyage du squelette, pour enlever des couches dont on a pas besoin.

mandatoryLayers = {'mbHistDate','mbHistTime','mbHistCode','mbHistAutor','mbHistModule','mbHistComment','Layer_name','COLUMNS','LINES'};
mandatoryLayers = cat(2, mandatoryLayers, NomLayers);
sklayerNames = {sk_ref.var(:).name};
Layer2delete = false( size( sklayerNames ) );
for k=1:length(sklayerNames)
    if ~any(strcmpi(sklayerNames{k}, mandatoryLayers))
        % couche optionnelle
        Layer2delete(k) = true;
    end
end
sk_ref.var(Layer2delete) = []; % on supprime

%% V�rif du squelette

for k=1:length(NomLayers)
    numVar = cdf_find_numVar(sk_ref, NomLayers{k});
    if isempty(numVar)
        my_warndlg(['Layer "' NomLayers{k} '" not found in skeleton.'], 1);
        return
    end
end

%% Cr�ation du fichier r�sultat

b = cl_netcdf('fileName', nomFicOut, 'skeleton', sk_ref);
%clear nomFicOut

%% Remplissage des valeurs des variables

% Variables globales du format SMF g�n�rique.
b = set_valAtt(b, 'Type', Type);
b = set_valAtt(b, 'Format', Format);
b = set_valAtt(b, 'Mosaic_type', Mosaic_type);
clear Format

b = set_valAtt(b, 'Number_layers',  Number_layers);
% GFORGE 334 : pb de conformit� de type avec la norme 1.4
b = set_valAtt(b, 'Number_lines',   int32(Number_lines));
b = set_valAtt(b, 'Number_columns', int32(Number_columns));

% Variables de g�om�trie du format SMF MNT

b = set_valAtt(b, 'Element_x_size', Element_x_size);
b = set_valAtt(b, 'Element_y_size', Element_y_size);
b = set_valAtt(b, 'Orientation',    Orientation);
b = set_valAtt(b, 'Xmin_metric',    Xmin_metric);
b = set_valAtt(b, 'Xmax_metric',    Xmax_metric);
b = set_valAtt(b, 'Ymin_metric',    Ymin_metric);
b = set_valAtt(b, 'Ymax_metric',    Ymax_metric);
% % b = set_valAtt(b, 'Str_Xmin',        Xmin_metric); % Modif GLA
% % b = set_valAtt(b, 'Str_Xmax',        Xmax_metric);

[varargin, DataType] = getPropertyValue(varargin, 'Data_type', 'NONE');

if strcmp(Mosaic_type, 'STR')
    %% Fichier rectiligne "STR"
    
    [varargin, X] = getPropertyValue(varargin, 'Str_pixel_size', []);
    if ~isempty(X)
        b = set_valAtt(b, 'Str_pixel_size', X);
    end
    
    [varargin, X] = getPropertyValue(varargin, 'Str_alt_beg', []);
    if ~isempty(X)
        b = set_valAtt(b, 'Str_alt_beg', X);
    end
    
    [varargin, X] = getPropertyValue(varargin, 'Str_alt_end', []);
    if ~isempty(X)
        b = set_valAtt(b, 'Str_alt_end', X);
    end
    
    [varargin, X] = getPropertyValue(varargin, 'Str_channel_pix_nb', []);
    if ~isempty(X)
        b = set_valAtt(b, 'Str_channel_pix_nb', X);
    end
    
    [varargin, Field] = getPropertyValue(varargin, 'Field', []);
    
    b = set_value(b, 'Field_name',                  Field.Field_name);
    
    [varargin, Date] = getPropertyValue(varargin, 'Date', []);
    if ~isempty(Date)
        if ValeursDansUnite
            [A_Resol, B_Resol, MissingVal] = computeScaledVar(Date);
            b = set_valVarAtt(b, 'Date', 'scale_factor', A_Resol);
            b = set_valVarAtt(b, 'Date', 'add_offset', B_Resol);
            if A_Resol == 0
                b = set_valVarAtt(b, 'Date', 'missing_value', MissingVal);
            end
        end
        b = set_value(b, 'Date', Date(:));
    end
    
    [varargin, Hour] = getPropertyValue(varargin, 'Hour', []);
    if ~isempty(Hour)
        if ValeursDansUnite
            [A_Resol, B_Resol, MissingVal] = computeScaledVar(Hour);
            b = set_valVarAtt(b, 'Hour', 'scale_factor', A_Resol);
            b = set_valVarAtt(b, 'Hour', 'add_offset', B_Resol);
            if A_Resol == 0
                b = set_valVarAtt(b, 'Hour', 'missing_value', MissingVal);
            end
        end
        b = set_value(b, 'Hour', Hour(:));
    end
    
    [varargin, X] = getPropertyValue(varargin, 'Latitude', []);
    if ~isempty(X)
        Latitude_TL = max(X);
        Latitude_TR = Latitude_TL;
        Latitude_BL = min(X);
        Latitude_BR = Latitude_BL;
        if ValeursDansUnite
            [A_Resol, B_Resol, MissingVal] = computeScaledVar(X);
            b = set_valVarAtt(b, 'Latitude', 'scale_factor', A_Resol);
            b = set_valVarAtt(b, 'Latitude', 'add_offset', B_Resol);
            if A_Resol == 0
                b = set_valVarAtt(b, 'Latitude', 'missing_value', MissingVal);
            end
        end
        b = set_value(b, 'Latitude', X(:));
    end
    
    [varargin, X] = getPropertyValue(varargin, 'Longitude', []);
    if ~isempty(X)
        Longitude_TL = max(X);
        Longitude_TR = Longitude_TL;
        Longitude_BL = min(X);
        Longitude_BR = Longitude_BL;
        if ValeursDansUnite
            [A_Resol, B_Resol, MissingVal] = computeScaledVar(X);
            b = set_valVarAtt(b, 'Longitude', 'scale_factor', A_Resol);
            b = set_valVarAtt(b, 'Longitude', 'add_offset', B_Resol);
            if A_Resol == 0
                b = set_valVarAtt(b, 'Longitude', 'missing_value', MissingVal);
            end
        end
        b = set_value(b, 'Longitude', X(:));
    end
    
    [varargin, X] = getPropertyValue(varargin, 'Heading', []);
    if ~isempty(X)
        if ValeursDansUnite
            [A_Resol, B_Resol, MissingVal] = computeScaledVar(X);
            b = set_valVarAtt(b, 'Heading', 'scale_factor', A_Resol);
            b = set_valVarAtt(b, 'Heading', 'add_offset', B_Resol);
            if A_Resol == 0
                b = set_valVarAtt(b, 'Heading', 'missing_value', MissingVal);
            end
        end
        b = set_value(b, 'Heading', X(:));
    end
    
    [varargin, X] = getPropertyValue(varargin, 'Velocity', []);
    if ~isempty(X)
        if ValeursDansUnite
            [A_Resol, B_Resol, MissingVal] = computeScaledVar(X);
            b = set_valVarAtt(b, 'Velocity', 'scale_factor', A_Resol);
            b = set_valVarAtt(b, 'Velocity', 'add_offset', B_Resol);
            if A_Resol == 0
                b = set_valVarAtt(b, 'Velocity', 'missing_value', MissingVal);
            end
        end
        b = set_value(b, 'Velocity', X(:));
    end
    
    [varargin, X] = getPropertyValue(varargin, 'PortMode', []);
    if ~isempty(X)
        if ValeursDansUnite
            [A_Resol, B_Resol, MissingVal] = computeScaledVar(X);
            b = set_valVarAtt(b, 'PortMode', 'scale_factor', A_Resol);
            b = set_valVarAtt(b, 'PortMode', 'add_offset', B_Resol);
            if A_Resol == 0
                b = set_valVarAtt(b, 'PortMode', 'missing_value', MissingVal);
            end
        end
        b = set_value(b, 'PortMode', X(:));
    end
    
    [varargin, X] = getPropertyValue(varargin, 'StarboardMode', []);
    if ~isempty(X)
        if ValeursDansUnite
            [A_Resol, B_Resol, MissingVal] = computeScaledVar(X);
            b = set_valVarAtt(b, 'StarboardMode', 'scale_factor', A_Resol);
            b = set_valVarAtt(b, 'StarboardMode', 'add_offset', B_Resol);
            if A_Resol == 0
                b = set_valVarAtt(b, 'StarboardMode', 'missing_value', MissingVal);
            end
        end
        b = set_value(b, 'StarboardMode', X(:));
    end
    
    [varargin, X] = getPropertyValue(varargin, 'SurfaceVelocity', []);
    if ~isempty(X)
        if ValeursDansUnite
            [A_Resol, B_Resol, MissingVal] = computeScaledVar(X);
            b = set_valVarAtt(b, 'SurfaceVelocity', 'scale_factor', A_Resol);
            b = set_valVarAtt(b, 'SurfaceVelocity', 'add_offset', B_Resol);
            if A_Resol == 0
                b = set_valVarAtt(b, 'SurfaceVelocity', 'missing_value', MissingVal);
            end
        end
        b = set_value(b, 'SurfaceVelocity', X(:));
    end
    
    [varargin, X] = getPropertyValue(varargin, 'VerticalDepth', []);
    if ~isempty(X)
        if ValeursDansUnite
            [A_Resol, B_Resol, MissingVal] = computeScaledVar(X);
            b = set_valVarAtt(b, 'VerticalDepth', 'scale_factor', A_Resol);
            b = set_valVarAtt(b, 'VerticalDepth', 'add_offset', B_Resol);
            if A_Resol == 0
                b = set_valVarAtt(b, 'VerticalDepth', 'missing_value', MissingVal);
            end
        end
        b = set_value(b, 'VerticalDepth', X(:));
        
    end
    
    [varargin, X] = getPropertyValue(varargin, 'SimradResolution', []);
    if ~isempty(X)
        if ValeursDansUnite
            [A_Resol, B_Resol, MissingVal] = computeScaledVar(X);
            b = set_valVarAtt(b, 'SimradResolution', 'scale_factor', A_Resol);
            b = set_valVarAtt(b, 'SimradResolution', 'add_offset', B_Resol);
            if A_Resol == 0
                b = set_valVarAtt(b, 'SimradResolution', 'missing_value', MissingVal);
            end
        end
        b = set_value(b, 'SimradResolution', X(:));
    end
    
    [varargin, X] = getPropertyValue(varargin, 'Interlacing', []);
    if ~isempty(X)
        if ValeursDansUnite
            [A_Resol, B_Resol, MissingVal] = computeScaledVar(X);
            b = set_valVarAtt(b, 'Interlacing', 'scale_factor', A_Resol);
            b = set_valVarAtt(b, 'Interlacing', 'add_offset', B_Resol);
            if A_Resol == 0
                b = set_valVarAtt(b, 'Interlacing', 'missing_value', MissingVal);
            end
        end
        b = set_value(b, 'Interlacing', X(:));
    end
    
    [varargin, X] = getPropertyValue(varargin, 'PortNormalIncidenceObliqueDist', []);
    if ~isempty(X)
        if ValeursDansUnite
            [A_Resol, B_Resol, MissingVal] = computeScaledVar(X);
            b = set_valVarAtt(b, 'PortNormalIncidenceObliqueDist', 'scale_factor', A_Resol);
            b = set_valVarAtt(b, 'PortNormalIncidenceObliqueDist', 'add_offset', B_Resol);
            if A_Resol == 0
                b = set_valVarAtt(b, 'PortNormalIncidenceObliqueDist', 'missing_value', MissingVal);
            end
        end
        b = set_value(b, 'PortNormalIncidenceObliqueDist', X(:));
    end
    
    [varargin, X] = getPropertyValue(varargin, 'PortTvgBsDifference', []);
    if ~isempty(X)
        if ValeursDansUnite
            [A_Resol, B_Resol, MissingVal] = computeScaledVar(X);
            b = set_valVarAtt(b, 'PortTvgBsDifference', 'scale_factor', A_Resol);
            b = set_valVarAtt(b, 'PortTvgBsDifference', 'add_offset', B_Resol);
            if A_Resol == 0
                b = set_valVarAtt(b, 'PortTvgBsDifference', 'missing_value', MissingVal);
            end
        end
        b = set_value(b, 'PortTvgBsDifference', X(:));
    end
    
    [varargin, X] = getPropertyValue(varargin, 'StarboardNormalIncidenceObliqueDist', []);
    if ~isempty(X)
        if ValeursDansUnite
            [A_Resol, B_Resol, MissingVal] = computeScaledVar(X);
            b = set_valVarAtt(b, 'StarboardNormalIncidenceObliqueDist', 'scale_factor', A_Resol);
            b = set_valVarAtt(b, 'StarboardNormalIncidenceObliqueDist', 'add_offset', B_Resol);
            if A_Resol == 0
                b = set_valVarAtt(b, 'StarboardNormalIncidenceObliqueDist', 'missing_value', MissingVal);
            end
        end
        b = set_value(b, 'StarboardNormalIncidenceObliqueDist', X(:));
    end
    
    [varargin, X] = getPropertyValue(varargin, 'StarboardTvgBsDifference', []);
    if ~isempty(X)
        if ValeursDansUnite
            [A_Resol, B_Resol, MissingVal] = computeScaledVar(X);
            b = set_valVarAtt(b, 'StarboardTvgBsDifference', 'scale_factor', A_Resol);
            b = set_valVarAtt(b, 'StarboardTvgBsDifference', 'add_offset', B_Resol);
            if A_Resol == 0
                b = set_valVarAtt(b, 'StarboardTvgBsDifference', 'missing_value', MissingVal);
            end
        end
        b = set_value(b, 'StarboardTvgBsDifference', X(:));
    end
    
    [varargin, X] = getPropertyValue(varargin, 'DistPortPing', []);
    if ~isempty(X)
        if ValeursDansUnite
            [A_Resol, B_Resol, MissingVal] = computeScaledVar(X);
            b = set_valVarAtt(b, 'DistPortPing', 'scale_factor', A_Resol);
            b = set_valVarAtt(b, 'DistPortPing', 'add_offset', B_Resol);
            if A_Resol == 0
                b = set_valVarAtt(b, 'DistPortPing', 'missing_value', MissingVal);
            end
        end
        b = set_value(b, 'DistPortPing', X(:));
    end
    
    [varargin, X] = getPropertyValue(varargin, 'DistStarboardPing', []);
    if ~isempty(X)
        if ValeursDansUnite
            [A_Resol, B_Resol, MissingVal] = computeScaledVar(X);
            b = set_valVarAtt(b, 'DistStarboardPing', 'scale_factor', A_Resol);
            b = set_valVarAtt(b, 'DistStarboardPing', 'add_offset', B_Resol);
            if A_Resol == 0
                b = set_valVarAtt(b, 'DistStarboardPing', 'missing_value', MissingVal);
            end
        end
        b = set_value(b, 'DistStarboardPing', X(:));
    end
    
    [varargin, X] = getPropertyValue(varargin, 'DistMiddlePing', []);
    if ~isempty(X)
        if ValeursDansUnite
            [A_Resol, B_Resol, MissingVal] = computeScaledVar(X);
            b = set_valVarAtt(b, 'DistMiddlePing', 'scale_factor', A_Resol);
            b = set_valVarAtt(b, 'DistMiddlePing', 'add_offset', B_Resol);
            if A_Resol == 0
                b = set_valVarAtt(b, 'DistMiddlePing', 'missing_value', MissingVal);
            end
        end
        b = set_value(b, 'DistMiddlePing', X(:));
    end
    
    [varargin, X] = getPropertyValue(varargin, 'Col', []);
    if ~isempty(X)
        if ValeursDansUnite
            [A_Resol, B_Resol, MissingVal] = computeScaledVar(X);
            b = set_valVarAtt(b, 'Col', 'scale_factor', A_Resol);
            b = set_valVarAtt(b, 'Col', 'add_offset', B_Resol);
            if A_Resol == 0
                b = set_valVarAtt(b, 'Col', 'missing_value', MissingVal);
            end
        end
        b = set_value(b, 'Col', X(:));
    end
    
    [varargin, X] = getPropertyValue(varargin, 'Row', []);
    if ~isempty(X)
        if ValeursDansUnite
            [A_Resol, B_Resol, MissingVal] = computeScaledVar(X);
            b = set_valVarAtt(b, 'Row', 'scale_factor', A_Resol);
            b = set_valVarAtt(b, 'Row', 'add_offset', B_Resol);
            if A_Resol == 0
                b = set_valVarAtt(b, 'Row', 'missing_value', MissingVal);
            end
        end
        b = set_value(b, 'Row', X(:));
    end
    
    [varargin, X] = getPropertyValue(varargin, 'Flag', []);
    if ~isempty(X)
        if ValeursDansUnite
            [A_Resol, B_Resol, MissingVal] = computeScaledVar(X);
            b = set_valVarAtt(b, 'Flag', 'scale_factor', A_Resol);
            b = set_valVarAtt(b, 'Flag', 'add_offset', B_Resol);
            if A_Resol == 0
                b = set_valVarAtt(b, 'Flag', 'missing_value', MissingVal);
            end
        end
        b = set_value(b, 'Flag', X(:));
    end
    
    [varargin, X] = getPropertyValue(varargin, 'PortAlongResolution', []);
    if ~isempty(X)
        if ValeursDansUnite
            [A_Resol, B_Resol, MissingVal] = computeScaledVar(X);
            b = set_valVarAtt(b, 'PortAlongResolution', 'scale_factor', A_Resol);
            b = set_valVarAtt(b, 'PortAlongResolution', 'add_offset', B_Resol);
            if A_Resol == 0
                b = set_valVarAtt(b, 'PortAlongResolution', 'missing_value', MissingVal);
            end
        end
        b = set_value(b, 'PortAlongResolution', X(:));
    end
    
    [varargin, X] = getPropertyValue(varargin, 'MiddleAlongResolution', []);
    if ~isempty(X)
        if ValeursDansUnite
            [A_Resol, B_Resol, MissingVal] = computeScaledVar(X);
            b = set_valVarAtt(b, 'MiddleAlongResolution', 'scale_factor', A_Resol);
            b = set_valVarAtt(b, 'MiddleAlongResolution', 'add_offset', B_Resol);
            if A_Resol == 0
                b = set_valVarAtt(b, 'MiddleAlongResolution', 'missing_value', MissingVal);
            end
        end
        b = set_value(b, 'MiddleAlongResolution', X(:));
    end
    
    [varargin, X] = getPropertyValue(varargin, 'StarboardAlongResolution', []);
    if ~isempty(X)
        if ValeursDansUnite
            [A_Resol, B_Resol, MissingVal] = computeScaledVar(X);
            b = set_valVarAtt(b, 'StarboardAlongResolution', 'scale_factor', A_Resol);
            b = set_valVarAtt(b, 'StarboardAlongResolution', 'add_offset', B_Resol);
            if A_Resol == 0
                b = set_valVarAtt(b, 'StarboardAlongResolution', 'missing_value', MissingVal);
            end
        end
        b = set_value(b, 'StarboardAlongResolution', X(:));
    end
    
    [varargin, X] = getPropertyValue(varargin, 'EmissionAlongAngle', []); %#ok<ASGLU>
    if ~isempty(X)
        if ValeursDansUnite
            [A_Resol, B_Resol, MissingVal] = computeScaledVar(X);
            b = set_valVarAtt(b, 'EmissionAlongAngle', 'scale_factor', A_Resol);
            b = set_valVarAtt(b, 'EmissionAlongAngle', 'add_offset', B_Resol);
            if A_Resol == 0
                b = set_valVarAtt(b, 'EmissionAlongAngle', 'missing_value', MissingVal);
            end
        end
        b = set_value(b, 'EmissionAlongAngle', X(:));
    end
    
    [~, Milieu] = sort(abs(x));
    MiddleColNumber = zeros(1,Number_lines) + Milieu(1);
    %     iField = lookForStringInField(Field.Field_name,'MiddleColNumber');
    %     X = (MiddleColNumber(:) - double(Field.Field_B_resol(iField))) / double(Field.Field_A_resol(iField));
    [A_Resol, B_Resol, MissingVal] = computeScaledVar(MiddleColNumber);
    b = set_valVarAtt(b, 'MiddleColNumber', 'scale_factor', A_Resol);
    b = set_valVarAtt(b, 'MiddleColNumber', 'add_offset', B_Resol);
    if A_Resol == 0
        b = set_valVarAtt(b, 'MiddleColNumber', 'missing_value', MissingVal);
    end
    b = set_value(b, 'MiddleColNumber', X(:));
    
    if ~isempty(Date) && ~isempty(Hour)
        Time = cl_time('timeIfr', Date, Hour);
        ShiftTime = Time - Time(1);
        ShiftTime = (ShiftTime - min(ShiftTime)) / 1000;
        %         iField = lookForStringInField(Field.Field_name,'ShiftTime');
        %         ShiftTime = (ShiftTime(:) - double(Field.Field_B_resol(iField))) / double(Field.Field_A_resol(iField));
        [A_Resol, B_Resol, MissingVal] = computeScaledVar(ShiftTime);
        b = set_valVarAtt(b, 'ShiftTime', 'scale_factor', A_Resol);
        b = set_valVarAtt(b, 'ShiftTime', 'add_offset', B_Resol);
        if A_Resol == 0
            b = set_valVarAtt(b, 'ShiftTime', 'missing_value', MissingVal);
        end
        b = set_value(b, 'ShiftTime', ShiftTime);
    end
    
else % Fichiers g�or�f�renc�s -> Mosaic_type = 'MOS' ou 'GEO'
    if GeometryType == cl_image.indGeometryType('LatLong')
        set(Carto, 'Projection.Type', 6) % GLU pour export en cylindrique �quidistant vers geoseas
    end
    
    [Latitude_TL ,Longitude_TL] = xy2latlon(Carto, Xmin_metric, Ymax_metric);
    [Latitude_TR ,Longitude_TR] = xy2latlon(Carto, Xmax_metric, Ymax_metric);
    [Latitude_BR ,Longitude_BR] = xy2latlon(Carto, Xmax_metric, Ymin_metric);
    [Latitude_BL ,Longitude_BL] = xy2latlon(Carto, Xmin_metric, Ymin_metric);
end

%% Cadre g�ographique

South_latitude = min([Latitude_TL Latitude_TR Latitude_BR Latitude_BL]);
North_latitude = max([Latitude_TL Latitude_TR Latitude_BR Latitude_BL]);
West_longitude = min([Longitude_TL Longitude_TR Longitude_BR Longitude_BL]);
East_longitude = max([Longitude_TL Longitude_TR Longitude_BR Longitude_BL]);

b = set_valAtt(b, 'Latitude_TL',     Latitude_TL);
b = set_valAtt(b, 'Longitude_TL',    Longitude_TL);
b = set_valAtt(b, 'Latitude_TR',     Latitude_TR);
b = set_valAtt(b, 'Longitude_TR',    Longitude_TR);
b = set_valAtt(b, 'Latitude_BR',     Latitude_BR);
b = set_valAtt(b, 'Longitude_BR',    Longitude_BR);
b = set_valAtt(b, 'Latitude_BL',     Latitude_BL);
b = set_valAtt(b, 'Longitude_BL',    Longitude_BL);
b = set_valAtt(b, 'mbSouthLatitude', South_latitude);
b = set_valAtt(b, 'mbNorthLatitude', North_latitude);
b = set_valAtt(b, 'mbWestLongitude', West_longitude);
b = set_valAtt(b, 'mbEastLongitude', East_longitude);
clear South_latitude North_latitude West_longitude East_longitude Ellipsoid_name Ellipsoid E2 DemiGrandAxe

if ~isempty(Sounder_type)
    b = set_valAtt(b, 'Sounder_type', Sounder_type);
end
clear Sounder_type Orientation
clear Number_layers Element_y_size Element_x_size
clear Creation_label Creation_hour Creation_day

%% Variables de cartographie du format SMF de MNT

[~, Ellipsoid_name] = getEllipsoidCaraibes(Carto);
% CARAIBES : dans le set_value, on est oblig� de faire char(Ellipsoid)
% car Ellipsoid est en ncchar mais c'est en r�alit� un ncbyte (valeur num�rique)

DemiGrandAxe    = get(Carto, 'Ellipsoide.DemiGrandAxe');
Aplatissement   = get(Carto, 'Ellipsoide.Aplatissement');
% Excentricite    = get(Carto, 'Ellipsoide.Excentricite');
E2              = get(Carto, 'Ellipsoide.E2');
b = set_valAtt(b, 'mbEllipsoidName', Ellipsoid_name);
b = set_valAtt(b, 'mbEllipsoidA', DemiGrandAxe);
b = set_valAtt(b, 'mbEllipsoidE2', E2);
b = set_valAtt(b, 'mbEllipsoidInvF', 1/Aplatissement);

Projection = getProjectionCaraibes(Carto);
Projection = int16(Projection);
% CARAIBES : dans le set_value, on est oblig� de faire char(Projection)
% car Projection est en ncchar mais c'est en r�alit� un ncbyte (valeur num�rique)

% GFORGE 334 : pb de conformit� de type avec la norme 1.4
b = set_valAtt(b, 'mbProjType', Projection);
b = set_valAtt(b, 'Conventions', 'CF-1.4');

switch Projection
    case 1 % MERCATOR
        %Meridian_180 = 1;
        % b = set_value(b, 'Meridian_180', Meridian_180);
        
        Reference_latitude = get(Carto, 'Projection.Mercator.lat_ech_cons');
        Central_meridian   = get(Carto, 'Projection.Mercator.long_merid_orig'); % * 1e6 ??????????????
        Mercator_Y0        = get(Carto, 'Projection.Mercator.Y0');
        Mercator_X0        = get(Carto, 'Projection.Mercator.X0');
        
        %% Encodage de fichiers CARAIBES.
        clear geoInfo;
        geoInfo.Projection              = 'Mercator';
        geoInfo.Ellipsoid_name          = 'GCS_WGS_1984';
        geoInfo.Datum                   = 'D_WGS_1984';
        geoInfo.Spheroid                = 'WGS_1984';
        geoInfo.Half_great_axis         = DemiGrandAxe;
        geoInfo.InverseFlattening       = Aplatissement;
        geoInfo.Longitude               = 0.0;
        geoInfo.ConversionFactor        = 0.017453292519943;
        geoInfo.ProjectionName          = 'Mercator';
        geoInfo.projParam(1).name       = 'False_Easting';
        geoInfo.projParam(1).value      = 0.0;
        geoInfo.projParam(2).name       = 'False_Northing';
        geoInfo.projParam(2).value      = 0.0;
        geoInfo.projParam(3).name       = 'Central_Meridian';
        geoInfo.projParam(3).value      = Central_meridian;
        geoInfo.projParam(4).name       = 'Standard_Parallel_1';
        geoInfo.projParam(4).value      = Reference_latitude;
        geoInfo.projection.unit.name    = 'Meter';
        geoInfo.projection.unit.value   = 1.0;
        esriProj = encodeESRIProj(geoInfo);
        
        % Positionnement des attributs globaux de projection.
        paramValue(1) = Central_meridian;
        paramValue(2) = Reference_latitude;
        paramValue(3) = Mercator_X0;
        paramValue(4) = Mercator_Y0;
        %b = set_valAtt(b, 'mbMeridian180', Meridian_180); % Modif
        b = set_valAtt(b, 'mbProjParameterCode', 'MB_M0,MB_L0,MB_X0,MB_Y0');
        b = set_valAtt(b, 'mbProjParameterValue', paramValue);
        
        clear Mercator_Central_meridian Minimum_value Mercator_Reference_latitude
        
    case 2 % LAMBERT
        %% Encodage de fichiers CARAIBES.
        Central_meridian     = get(Carto, 'Projection.Lambert.long_merid_orig');
        Reference_latitude_1 = get(Carto, 'Projection.Lambert.first_paral');
        Reference_latitude_2 = get(Carto, 'Projection.Lambert.second_paral');
        Lambert_X_origin     = get(Carto, 'Projection.Lambert.X0');
        Lambert_Y_origin     = get(Carto, 'Projection.Lambert.Y0');
        Lambert_factor       = get(Carto, 'Projection.Lambert.scale_factor');
        Lambert_type         = get(Carto, 'Projection.Lambert.Type');
        
        clear geoInfo;
        geoInfo.Projection            = 'Lambert';
        geoInfo.Ellipsoid_name        = 'GCS_WGS_1984';
        geoInfo.Datum                 = 'D_WGS_1984';
        geoInfo.Spheroid              = 'WGS_1984';
        geoInfo.Half_great_axis       = DemiGrandAxe;
        geoInfo.InverseFlattening     = Aplatissement;
        geoInfo.Longitude             = 0.0;
        geoInfo.ConversionFactor      = 0.017453292519943;
        geoInfo.ProjectionName        = 'Lambert_Conformal_Conic';
        geoInfo.projParam(1).name     = 'False_Easting';
        geoInfo.projParam(1).value    = 0.0;
        geoInfo.projParam(2).name     = 'False_Norting';
        geoInfo.projParam(2).value    = 0.0;
        geoInfo.projParam(3).name     = 'Central_Meridian';
        geoInfo.projParam(3).value    = Central_meridian;
        geoInfo.projParam(4).name     = 'Standard_Parallel_1';
        geoInfo.projParam(4).value    = Reference_latitude_1;
        geoInfo.projParam(5).name     = 'Standard_Parallel_2';
        geoInfo.projParam(5).value    = Reference_latitude_2;
        geoInfo.projParam(6).name     = 'Scale_Factor';
        geoInfo.projParam(6).value    = Lambert_factor;
        geoInfo.projParam(7).name     = 'Latitude_Of_Origin';
        geoInfo.projParam(7).value    = (Reference_latitude_1 + Reference_latitude_2) / 2;
        geoInfo.projection.unit.name  = 'Meters';
        geoInfo.projection.unit.value = 1.0;
        esriProj = encodeESRIProj(geoInfo);
        
        % Positionnement des attributs globaux de projection.
        paramValue(1) = Central_meridian;
        paramValue(2) = Reference_latitude_1;
        paramValue(3) = Reference_latitude_2;
        paramValue(4) = Lambert_factor;
        paramValue(5) = Lambert_X_origin;
        paramValue(6) = Lambert_Y_origin;
        paramValue(7) = Lambert_type - 1; % Type ???
        b = set_valAtt(b, 'mbProjParameterCode', 'MB_M0,MB_LC1,MB_LC2,MB_ECHELLE,MB_X0,MB_Y0,MB_TYPE');
        b = set_valAtt(b, 'mbProjParameterValue', paramValue);
        
    case 3  % UTM
        UTM_Hemisphere      = getUTMHemisphereCaraibes(Carto);
        Central_meridian    = get(Carto, 'Projection.UTM.Central_meridian');
        UTM_X0              = get(Carto, 'Projection.UTM.X0');
        UTM_Y0              = get(Carto, 'Projection.UTM.Y0');
        % CARAIBES : L'attribut "Commentaire" dit 1 pour le Nord, 2 pour le
        % sud et dans les faits c'est 0 et 1
        UTM_Hemisphere = UTM_Hemisphere -1;
        % CARAIBES
        UTM_Fuseau = get(Carto, 'Projection.UTM.Fuseau');
        
        clear geoInfo;
        geoInfo.Projection            = 'UTM';
        geoInfo.Ellipsoid_name        = 'GCS_WGS_1984';
        geoInfo.Datum                 = 'D_WGS_1984';
        geoInfo.Spheroid              = 'WGS_1984';
        geoInfo.Half_great_axis       = DemiGrandAxe;
        geoInfo.InverseFlattening     = Aplatissement;
        geoInfo.Longitude             = 0.0;
        geoInfo.ConversionFactor      = 0.017453292519943;
        geoInfo.ProjectionName        = 'Lambert_Conformal_Conic';
        geoInfo.projParam(1).name     = 'False_Easting';
        geoInfo.projParam(1).value    = UTM_X0;
        geoInfo.projParam(2).name     = 'False_Norting';
        geoInfo.projParam(2).value    = UTM_Y0;
        geoInfo.projParam(3).name     = 'Central_Meridian';
        geoInfo.projParam(3).value    = Central_meridian;
        geoInfo.projParam(4).name     = 'Scale_Factor';
        geoInfo.projParam(4).value    = 1.0; % ????????????
        geoInfo.projParam(5).name     = 'Latitude_Of_Origin';
        geoInfo.projParam(5).value    = UTM_Y0;
        geoInfo.projection.unit.name  = 'Meters';
        geoInfo.projection.unit.value = 1.0;
        esriProj = encodeESRIProj(geoInfo);
        
        % Positionnement des attributs globaux de projection.
        paramValue(1) = Central_meridian;
        paramValue(2) = UTM_Hemisphere;
        paramValue(3) = UTM_Fuseau;
        paramValue(4) = UTM_X0;
        paramValue(5) = UTM_Y0;
        b = set_valAtt(b, 'mbProjParameterCode', 'MB_M0,MB_HEMI,MB_FUSEAU, MB_X0, MB_Y0');
        b = set_valAtt(b, 'mbProjParameterValue', paramValue);
        
    case 4  % St�r�ographique polaire
        % Rmq de GLUGLUGLU
        my_warndlg('Mod�le de projection non obtenu en test', 1);
        return
        
        %         POLAR_Reference_latitude = get(Carto, 'Projection.Stereo.lat_ech_cons') * 1e6;
        %         b = set_value(b, 'POLAR_Reference_latitude', POLAR_Reference_latitude);
        %
        %         POLAR_Central_meridian = get(Carto, 'Projection.Stereo.long_merid_horiz') * 1e6;
        %         b = set_value(b, 'POLAR_Central_meridian', POLAR_Central_meridian);
        %
        %         POLAR_Hemisphere = getPolarHemisphereCaraibes(Carto);
        %         b = set_value(b, 'POLAR_Hemisphere', POLAR_Hemisphere);
        
    case 6 % EquiCylindrique distant
        Reference_latitude = get(Carto, 'Projection.Mercator.lat_ech_cons');
        Central_meridian   = get(Carto, 'Projection.Mercator.long_merid_orig'); % * 1e6 ??????????????
        Mercator_Y0        = get(Carto, 'Projection.Mercator.Y0');
        Mercator_X0        = get(Carto, 'Projection.Mercator.X0');
        
        %% Encodage de fichiers CARAIBES.
        clear geoInfo;
        geoInfo.Projection            = 'Equidistant_Cylindrical';
        geoInfo.Ellipsoid_name        = 'GCS_WGS_1984';
        geoInfo.Datum                 = 'D_WGS_1984';
        geoInfo.Spheroid              = 'WGS_1984';
        geoInfo.Half_great_axis       = DemiGrandAxe;
        geoInfo.InverseFlattening     = Aplatissement;
        geoInfo.Longitude             = 0.0;
        geoInfo.ConversionFactor      = 0.017453292519943;
        geoInfo.ProjectionName        = 'Equidistant_Cylindrical';
        geoInfo.projParam(1).name     = 'False_Easting';
        geoInfo.projParam(1).value    = 0.0;
        geoInfo.projParam(2).name     = 'False_Northing';
        geoInfo.projParam(2).value    = 0.0;
        geoInfo.projParam(3).name     = 'Central_Meridian';
        geoInfo.projParam(3).value    = Central_meridian;
        geoInfo.projParam(4).name     = 'Standard_Parallel_1';
        geoInfo.projParam(4).value    = Reference_latitude;
        geoInfo.projection.unit.name  = 'Meter';
        geoInfo.projection.unit.value = 1.0;
        esriProj = encodeESRIProj(geoInfo);
        
        
        % Positionnement des attributs globaux de projection.
        paramValue(1) = Central_meridian;
        paramValue(2) = Reference_latitude;
        paramValue(3) = Mercator_X0;
        paramValue(4) = Mercator_Y0;
        %b = set_valAtt(b, 'mbMeridian180', Meridian_180); % Modif
        b = set_valAtt(b, 'mbProjParameterCode', 'MB_M0,MB_L0,MB_X0,MB_Y0');
        b = set_valAtt(b, 'mbProjParameterValue', paramValue);
        
        clear Mercator_Central_meridian Minimum_value Mercator_Reference_latitude
        
    otherwise
        my_warndlg(['ATTENTION PROJECTION PAS ENCORE TRAITEE DANS ' mfilename], 1);
end
clear Projection Carto

% Variables d'historique des modifications
% v = get_value(a_ref, 'Modif_name');
% b = set_value(b, 'Modif_name', v);
%
% v = get_value(a_ref, 'Modif_day');
% b = set_value(b, 'Modif_day', v);
%
% v = get_value(a_ref, 'Modif_hour');
% b = set_value(b, 'Modif_hour', v);
%
% v = get_value(a_ref, 'Modif_type');
% b = set_value(b, 'Modif_type', v);

% Variables d'Ent�te de couche

Layer_name = repmat(char(0), 20, 20);
Layer_type = repmat(char(0), 20, 3);
Layer_unit = repmat(char(0), 20, 16);
Data_type  = repmat(char(0), 20, 8);

Sign_value     = zeros(20, 1);
Element_type   = repmat(char(0), 1, 20); %#ok<NASGU>
Element_type   = zeros(1, 20);
Element_length = zeros(1, 20);
Missing_Value  = zeros(1, 20);
Minimum_value  = zeros(20, 1);
Maximum_value  = zeros(20, 1);
A_resol        = zeros(20, 1);
B_resol        = zeros(20, 1);
First_line     = zeros(20, 1);
Last_line      = zeros(20, 1);
First_column   = zeros(20, 1);
Last_column    = zeros(20, 1);

%% Ecriture

for k=1:length(NomLayers)
    disp(['Processing ' NomLayers{k}])
    [numVarDepth, NomLayer] = find_numVar(b, NomLayers{k});
    if isempty(numVarDepth)
        my_warndlg(['Layer "' NomLayers{k} '" not found.'], 1);
        return
    end
    if length(size(Layers{k})) == 2
        X = Layers{k}(:,:);
    else
        X = Layers{k}(:,:,:);
    end
    if ~isempty(ValNaN)
        if isnan(ValNaN(k))
            X(X == ValNaN(k)) = NaN;
        end
    end
    
    %     cdf_disp_list_var(sk_ref, 'Level', 3, 'VarListe', numVarDepth)
    switch sk_ref.var(numVarDepth).type
        case 'NC_LONG'
            Element_type(k)    = 4;
            Sign_value(k)      = 2^31-1;
            Element_length(k)  = 4;
            Missing_Value(k)   = 2^(31-1);
            NbQuantifiedValues = 2^30;
            
        case 'NC_SHORT'
            Element_type(k)   = 3;
            Sign_value(k)     = 2^15-1;
            Element_length(k) = 2;
            Missing_Value(k)   = intmax('int16');
            NbQuantifiedValues = (2^15-2);
            
        case 'NC_BYTE'
            Element_type(k)   = 1;
            Sign_value(k)     = 255;
            Element_length(k) = 1;
            Missing_Value(k)   = intmax('int8');
            NbQuantifiedValues = 254;
            
        case 'NC_FLOAT'
            Element_type(k)   = 5;
            Sign_value(k)     = 4.2950e+009;
            Element_length(k) = 4;
            Missing_Value(k)   = realmax('single');
            NbQuantifiedValues = 2^30;
            
        case 'NC_DOUBLE'
            Element_type(k)   = 6;
            Sign_value(k)     = 1.8446744e+019;
            Element_length(k) = 8;
            Missing_Value(k)   = realmax('double');
            NbQuantifiedValues = 2^63;
            
        case 'NC_CHAR'
            Element_type(k)   = 2;
            Sign_value(k)     = 255;
            Element_length(k) = 1;
            Missing_Value(k)   = intmax('int8');
            NbQuantifiedValues = []; %254;
            
        otherwise
            my_warndlg('create_caraibesGrid_3_6 : Cas pas encore trait� ici', 1);
    end
    
    Layer_name(k,1:length(NomLayer)) = NomLayer;
    Data_type(k,1:length(DataType))  = DataType;
    
    Layer_type(k,1:length(Type)) = Type;
    Layer_unit(k,1:length(Unite{k})) = Unite{k};
    
    FirstColNumber = zeros(1, length(size(X,1)));
    LastColNumber  = zeros(1, length(size(X,1)));
    MinValue = Inf;
    MaxValue = -Inf;
    for iLig=1:size(X,1)
        Ligne = X(iLig,:,:);
        subNonNaN = ~isnan(Ligne);
        if sum(subNonNaN) ~= 0
            Ligne = Ligne(subNonNaN);
            MinValue = min(MinValue, double(min(Ligne)));
            MaxValue = max(MaxValue, double(max(Ligne)));
        end
        if k == 1
            if sum(subNonNaN) == 0
                FirstColNumber(iLig) = length(subNonNaN)+1;
                LastColNumber(iLig)  = length(subNonNaN);
            else
                FirstColNumber(iLig) = find(subNonNaN, 1, 'first');
                LastColNumber(iLig)  = find(subNonNaN, 1, 'last');
            end
        end
    end
    Minimum_value(k) = MinValue;
    Maximum_value(k) = MaxValue;
    clear MinValue MaxValue subNonNaN Ligne iLig
    
    if isempty(NbQuantifiedValues)
        A_resol(k) = 1;
        Minimum_value(k)	= Minimum_value(k);
        Maximum_value(k)    = Maximum_value(k);
        % En version 3.6, le squelette de r�f�rence code la r�flectivit�
        % sur 1 NC_CHAR.
        if strcmp(NomLayers{k}, 'REFLECTIVITY')
            A_resol(k) = 0.5;
            B_resol(k) = -64.0;
        end
    else
        A_resol(k)       = (Maximum_value(k) - Minimum_value(k)) / NbQuantifiedValues;
        B_resol(k)       = Minimum_value(k); % GLUGLUGLU
        Minimum_value(k) = (Minimum_value(k) - B_resol(k) ) / A_resol(k);
        Maximum_value(k) = (Maximum_value(k) - B_resol(k) ) / A_resol(k);
    end
    First_line(k)   = 1;
    Last_line(k)    = size(Layers{k},1);
    First_column(k) = 1;
    Last_column(k)  = size(Layers{k},2);
    
    if isa(Layers{k}, 'cl_memmapfile')
        Layers{k}.Writable = true;
    end
    
    if strcmp(Mosaic_type, 'GEO')
        if strcmp(Type, 'DTM') && (y(end) < y(1))
            X = my_flipud(X);
        end
        if strcmp(Type, 'MOS') && ~(y(end) < y(1))
            X = my_flipud(X);
        end
    else
        if strcmp(Type, 'MOS') && (y(end) < y(1))
            X = my_flipud(X);
        end
        
    end
    if length(size(X)) == 2
        Layers{k}(:,:) = X;
    else
        Layers{k}(:,:,:) = X;
    end
    if isa(Layers{k}, 'cl_memmapfile')
        Layers{k}.Writable = false;
    end
    clear X Ligne iLig subNaN
    
    b = set_valVarAtt(b, numVarDepth, NomLayer, NomLayer);
end
clear k Unite NomLayer

numVar = find_numVar(b, 'Layer_name');
b = set_valVarAtt(b, numVar, 'type', 'string');
b = set_valVarAtt(b, numVar, 'long_name', 'Nom de la couche');
b = set_valVarAtt(b, numVar, 'name_code', 'LAYER_NAME');
b = set_value(b, 'Layer_name', Layer_name);

if strcmp(Mosaic_type, 'GEO')
    COLUMNS = linspace(Xmin_metric, Xmax_metric, Number_columns);
    LINES   = linspace(Ymin_metric, Ymax_metric, Number_lines);
    b = set_value(b, 'COLUMNS', COLUMNS);
    b = set_value(b, 'LINES', LINES);
end

for k=1:length(NomLayers)
    [numVarDepth, ~] = find_numVar(b, NomLayers{k});
    % % % % % % % sz = size(Data_type);
    % % % % % % % Data_type = my_transpose(Data_type);
    % % % % % % % Data_type = reshape(Data_type, [sz(1) sz(2)]);
    b = set_valVarAtt(b, numVarDepth, deblank(Layer_name(k,:)), deblank(Layer_name(k,:)));
    % % % % % % % % sz = size(Layer_type);
    % % % % % % % % Layer_type = my_transpose(Layer_type);
    % % % % % % % % Layer_type = reshape(Layer_type, [sz(1) sz(2)]);
    b = set_valVarAtt(b, numVarDepth,  'long_name',         deblank(Layer_name(k,:)));
    b = set_valVarAtt(b, numVarDepth,  'layer_type',        Layer_type(k,:));
    %b = set_valVarAtt(b, numVarDepth,  'element_type',    num2str(Element_type(k), '%d'));
    b = set_valVarAtt(b, numVarDepth,  'element_type',      char(3)); % Ca marche comme �a :-|
    b = set_valVarAtt(b, numVarDepth,  'units',             deblank(Layer_unit(k,:)));
    b = set_valVarAtt(b, numVarDepth,  'scale_factor',      A_resol(k));
    b = set_valVarAtt(b, numVarDepth,  'add_offset',        B_resol(k));
    b = set_valVarAtt(b, numVarDepth,  'valid_minimum',     Minimum_value(k) );
    b = set_valVarAtt(b, numVarDepth,  'valid_maximum',     Maximum_value(k) );
    b = set_valVarAtt(b, numVarDepth,  'first_line',        int16(First_line(k)));
    b = set_valVarAtt(b, numVarDepth,  'last_line',         int16(Last_line(k)));
    b = set_valVarAtt(b, numVarDepth,  'first_column',      int16(First_column(k)));
    b = set_valVarAtt(b, numVarDepth,  'last_column',       int16(Last_column(k)));
    b = set_valVarAtt(b, numVarDepth,  'ext_missing_value', 9.969209968386869e+036);
    b = set_valVarAtt(b, numVarDepth,  'layer_min_value',   Minimum_value(k) );
    b = set_valVarAtt(b, numVarDepth,  'layer_max_value',   Maximum_value(k) );
    b = set_valVarAtt(b, numVarDepth,  'data_type',         deblank(Data_type(k,:)));
    b = set_valVarAtt(b, numVarDepth,  'processing_method', '');
    %b = set_valVarAtt(b, numVarDepth,  'element_length', num2str(Element_length(k), '%d'));
    b = set_valVarAtt(b, numVarDepth,  'element_length', 	char(3));
    b = set_valVarAtt(b, numVarDepth,  'missing_value',     Missing_Value(k));
    b = set_valVarAtt(b, numVarDepth,  'esri_pe_string',    esriProj);
end

clear Minimum_value Maximum_value Layer_unit Layer_type Layer_name Last_line Last_column First_line First_column
clear Element_type Element_length B_resol A_resol
for k=1:length(NomLayers)
    disp(['Writing ' NomLayers{k}])
    if length(size(Layers{k})) == 2
        b = set_value(b, NomLayers{k}, Layers{k}(:,:));
    else
        b = set_value(b, NomLayers{k}, Layers{k}(:,:,:));
    end
end

str1 = sprintf('%s\n\nExport r�ussi !', nomFicOut);
str2 = sprintf('%s\n\nExport sucessful !', nomFicOut);
my_warndlg(Lang(str1,str2), 1);

%{
% JMA : Attibuts pas trait�s : quelle est leur signification ????????
v = get_value(a_ref, 'Decod_sign_value');
b = set_value(b, 'Decod_sign_value', v);

v = get_value(a_ref, 'Layer_min_value');
b = set_value(b, 'Layer_min_value', v);

v = get_value(a_ref, 'Layer_max_value');
b = set_value(b, 'Layer_max_value', v);

v = get_value(a_ref, 'Processing_method');
b = set_value(b, 'Processing_method', v);
%}


function [ScaleFactor, Offset, MissingValue] = computeScaledVar(Data)
MissingValue    = [];
Offset          = min(Data);
ScaleFactor     = (max(Data) - min(Data))/numel(Data);
if ScaleFactor == 0
    MissingValue = 9999;
end

