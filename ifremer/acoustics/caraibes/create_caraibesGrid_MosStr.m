% Creation d'un MNT au format Caraibes(R)
%
% Syntax
%   create_caraibesGrid_3_6(nomFicOut, ...)
%
% Input Arguments
%   nomFicOut   : nom du fichier de sortie
%   sk_ref      : squelette de r�f�rence
%   NomLayer    : Nom du Layer
%   Depth       : Layer
%   x           : �chantillonnage en x
%   y           : �chantillonnage en y
%   Carto       : objet Carto associ�.
%
% Name-Value Pair Arguments
%   Sans objet
%
% Examples
%   nomFic = getNomFicDatabase('EM300_Ex2.mnt');
%   a_ref = cl_netcdf('fileName', nomFic);
%   sk_ref = get(a_ref, 'skeleton');
%   a = cl_car_ima(nomFic);
%   b = view(a);
%   c = get_Image(b, 1);
%
%   nomFicOut = [tempname '.dtm']
%   I = get(c, 'Image');
%   x = get(c, 'x');
%   y = get(c, 'y');
%   Carto = get(c, 'Carto');
%   create_caraibesGrid_3_6(nomFicOut, sk_ref, 'Depth', I, x, y, Carto);
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function create_caraibesGrid_MosStr(nomFicOut, sk_ref, NomLayers, Layers,  varargin)

[varargin, suby] = getPropertyValue(varargin, 'suby', 1:numel(size(Layers,2)));

Number_lines   = size(Layers{1},1);
Number_columns = size(Layers{1},2);

%% Mise � jour des Dimensions
% Pas d'�criture de CIB_BLOCKS_DIM
numDim = cdf_find_numDim(sk_ref, 'mbHistoryRecNbr');
sk_ref.dim(numDim).value = 20;
numDim = cdf_find_numDim(sk_ref, 'mbNameLength');
sk_ref.dim(numDim).value = 20;
numDim = cdf_find_numDim(sk_ref, 'mbCommentLength');
sk_ref.dim(numDim).value = 256;
numDim = cdf_find_numDim(sk_ref, 'grRawImage_NbLin');
sk_ref.dim(numDim).value = Number_lines;
numDim = cdf_find_numDim(sk_ref, 'grRawImage_NbCol');
sk_ref.dim(numDim).value = Number_columns;
numDim = cdf_find_numDim(sk_ref, 'grWrkImage_NbLin');
sk_ref.dim(numDim).value = Number_lines;
numDim = cdf_find_numDim(sk_ref, 'grWrkImage_NbCol');
sk_ref.dim(numDim).value = Number_columns;
clear numDim

%% Cr�ation du fichier r�sultat

% profile on
b = cl_netcdf('fileName', nomFicOut, 'skeleton', sk_ref);
% profile report
%clear nomFicOut

%% Remplissage des valeurs des variables
% Pas d'�criture de CIB_BLOCKS_FOR_UNLIMITED
b = set_valAtt(b, 'mbVersion', 130);
b = set_valAtt(b, 'mbName', 'Sacr�e CHARLINE');
b = set_valAtt(b, 'mbClasse', 'GR_SONAR_STR_IMAGE');
b = set_valAtt(b, 'mbLevel', int16(0));
b = set_valAtt(b, 'mbNbrHistoryRec', 1);
% b = set_valAtt(b, 'mbTimeReference', datestr(now));

[varargin, Date] = getPropertyValue(varargin, 'Date', []);
[varargin, Heure] = getPropertyValue(varargin, 'Hour', []);
b = set_valAtt(b, 'mbStartDate', Date(1));
b = set_valAtt(b, 'mbEndDate', Date(end));
b = set_valAtt(b, 'mbStartTime', Heure(1));
b = set_valAtt(b, 'mbEndTime', Heure(end));

[varargin, Sonar] = getPropertyValue(varargin, 'Sonar', []);
b = set_valAtt(b, 'grStrSensor', get(Sonar.Desciption,'SonarName'));
b = set_valAtt(b, 'grStrSamplingFrequency', get(Sonar.Desciption,'Proc.SampFreq')/1000);
b = set_valAtt(b, 'grStrCelerity', 1500); %TODO


%% Fichier rectiligne "STR"

[varargin, X] = getPropertyValue(varargin, 'Str_pixel_size', []);
if ~isempty(X)
    b = set_valAtt(b, 'grStrRawPixelSize', X(:));
end

[varargin, X] = getPropertyValue(varargin, 'Str_alt_beg', []);
if ~isempty(X)
    b = set_valAtt(b, 'grStrAltBeg', X(:));
end
[varargin, X] = getPropertyValue(varargin, 'Str_alt_beg', []);
if ~isempty(X)
    b = set_valAtt(b, 'grStrAltEnd', X(:));
end

[varargin, X] = getPropertyValue(varargin, 'Str_channel_pix_nb', floor(Number_columns/2)); %#ok<ASGLU>
if ~isempty(X)
    b = set_valAtt(b, 'grStrChannelPixNb', X(:));
end

Number_layers = numel(NomLayers);
b = set_valAtt(b, 'grLayerNb', Number_layers);

clear Number_layers Element_y_size Element_x_size


%% Les FLAGs
% Variables d'Ent�te de couche
% Attributs Flag � 2 : TODO, comprendre la signification de ces flags
% (pas d�crit dans le .cfg CARAIBES).
X = 2 * ones(size(Layers{1}, 1), 1, 'uint8');
b = set_value(b, 'grRawImage_grSyncFlag', X(:));
b = set_value(b, 'grRawImage_grSyncJulianDay_Flag', X(:));
b = set_value(b, 'grRawImage_grSyncJulianMs_Flag', X(:));
b = set_value(b, 'grRawImage_grSyncPingNumber_Flag', X(:));
b = set_value(b, 'grRawImage_grSyncImmersion_Flag', X(:));
b = set_value(b, 'grRawImage_grSyncPrtRTHeight_Flag', X(:));
b = set_value(b, 'grRawImage_grSyncStbRTHeight_Flag', X(:));
b = set_value(b, 'grRawImage_grSyncPrtUsrHeight_Flag', X(:));
b = set_value(b, 'grRawImage_grSyncStbUsrHeight_Flag', X(:));
b = set_value(b, 'grRawImage_grSyncCableOut_Flag', X(:));
b = set_value(b, 'grRawImage_grSyncSpeed_Flag', X(:));
b = set_value(b, 'grRawImage_grSyncHeading_Flag', X(:));
b = set_value(b, 'grRawImage_grSyncRoll_Flag', X(:));
b = set_value(b, 'grRawImage_grSyncCableOut_Flag', X(:));
b = set_value(b, 'grRawImage_grSyncPitch_Flag', X(:));
% b = set_value(b, 'grRawImage_grSyncYaw_Flag', X(:));
b = set_value(b, 'grRawImage_grSyncPrtMaxRange_Flag', X(:));
b = set_value(b, 'grRawImage_grSyncStbMaxRange_Flag', X(:));
b = set_value(b, 'grRawImage_grSyncPrtUsrRange_Flag', X(:));
b = set_value(b, 'grRawImage_grSyncStbUsrRange_Flag', X(:));
b = set_value(b, 'grRawImage_grSyncSensorDrift_Flag', X(:));
b = set_value(b, 'grRawImage_grSyncPrtPixNb_Flag', X(:));
b = set_value(b, 'grRawImage_grSyncStbPixNb_Flag', X(:));
b = set_value(b, 'grRawImage_grSyncLatitude_Flag', X(:));
b = set_value(b, 'grRawImage_grSyncLongitude_Flag', X(:));

% WrkImage
b = set_value(b, 'grWrkImage_grSyncFlag', X(:));
b = set_value(b, 'grWrkImage_grSyncJulianDay_Flag', X(:));
b = set_value(b, 'grWrkImage_grSyncJulianMs_Flag', X(:));
b = set_value(b, 'grWrkImage_grSyncPingNumber_Flag', X(:));
b = set_value(b, 'grWrkImage_grSyncImmersion_Flag', X(:));
b = set_value(b, 'grWrkImage_grSyncPrtRTHeight_Flag', X(:));
b = set_value(b, 'grWrkImage_grSyncStbRTHeight_Flag', X(:));
b = set_value(b, 'grWrkImage_grSyncPrtUsrHeight_Flag', X(:));
b = set_value(b, 'grWrkImage_grSyncStbUsrHeight_Flag', X(:));
b = set_value(b, 'grWrkImage_grSyncCableOut_Flag', X(:));
b = set_value(b, 'grWrkImage_grSyncSpeed_Flag', X(:));
b = set_value(b, 'grWrkImage_grSyncHeading_Flag', X(:));
b = set_value(b, 'grWrkImage_grSyncRoll_Flag', X(:));
b = set_value(b, 'grWrkImage_grSyncCableOut_Flag', X(:));
b = set_value(b, 'grWrkImage_grSyncPitch_Flag', X(:));
% b = set_value(b, 'grWrkImage_grSyncYaw_Flag', X(:));
b = set_value(b, 'grWrkImage_grSyncPrtMaxRange_Flag', X(:));
b = set_value(b, 'grWrkImage_grSyncStbMaxRange_Flag', X(:));
b = set_value(b, 'grWrkImage_grSyncPrtUsrRange_Flag', X(:));
b = set_value(b, 'grWrkImage_grSyncStbUsrRange_Flag', X(:));
b = set_value(b, 'grWrkImage_grSyncSensorDrift_Flag', X(:));
b = set_value(b, 'grWrkImage_grSyncPrtPixNb_Flag', X(:));
b = set_value(b, 'grWrkImage_grSyncStbPixNb_Flag', X(:));
b = set_value(b, 'grWrkImage_grSyncLatitude_Flag', X(:));
b = set_value(b, 'grWrkImage_grSyncLongitude_Flag', X(:));

%% Le reste

%% Sauvegarde de l'heure

b = set_value(b, 'grRawImage_grSyncJulianDay', Date(:));
b = set_value(b, 'grRawImage_grSyncJulianMs', Heure(:));
b = set_value(b, 'grWrkImage_grSyncJulianDay', Date(:));
b = set_value(b, 'grWrkImage_grSyncJulianMs', Heure(:));

%% Sauvegarde de PingNumber

nomFieldSsc = 'PingCounter';
nomFieldCIB = 'grSyncPingNumber';
write_autoscaleVar(b, Sonar, nomFieldSsc, nomFieldCIB, 'suby', suby)

%% Sauvegarde de l'immersion

nomFieldSsc = 'Immersion';
nomFieldCIB = 'grRawImage_grSyncImmersion';
if isfield(Sonar, nomFieldSsc) && ~isempty(Sonar.(nomFieldSsc))
    try
        X = Sonar.(nomFieldSsc)(suby);
        X(isnan(X)) = 0;
        [ScaleFactor, Offset, MissingValue] = computeScaledVar(-abs(X));
        %         b = set_value(b, nomFieldCIB, -abs(X(:)));
        b = set_valVarAtt(b, nomFieldCIB,  'scale_factor',    ScaleFactor);
        b = set_valVarAtt(b, nomFieldCIB,  'add_offset',      Offset);
        b = set_valVarAtt(b, nomFieldCIB,  'missing_value',   MissingValue);
        b = set_value(b, nomFieldCIB, -abs(X(:)));
    catch ME %#ok<NASGU>
        str1 = ['Erreur � l''�criture du fichier Imo dans la variable : ' nomFieldSsc];
        str2 = ['Error in writing file Imo in variable : ' nomFieldSsc];
        my_warndlg(Lang(str1,str2), 1);
        return
    end
end

%% Sauvegarde de la hauteur

nomFieldSsc = 'Height';
nomFieldCIB = {'grSyncPrtRTHeight', 'grSyncStbRTHeight', 'grSyncPrtUsrHeight','grSyncStbUsrHeight'};
write_autoscaleVar(b, Sonar, nomFieldSsc, nomFieldCIB, 'suby', suby)

%% Sauvegarde de CableOut

nomFieldSsc = 'CableOut';
nomFieldCIB = 'grSyncCableOut';
write_autoscaleVar(b, Sonar, nomFieldSsc, nomFieldCIB, 'suby', suby)

%% Sauvegarde de la Speed

nomFieldSsc = 'Speed';
nomFieldCIB = 'grSyncSpeed';
write_autoscaleVar(b, Sonar, nomFieldSsc, nomFieldCIB, 'suby', suby)

%% Sauvegarde du cap

nomFieldSsc = 'Heading';
nomFieldCIB = 'grSyncHeading';
write_autoscaleVar(b, Sonar, nomFieldSsc, nomFieldCIB, 'suby', suby)

%% Sauvegarde de la Roll

nomFieldSsc = 'Roll';
nomFieldCIB = 'grSyncRoll';
write_autoscaleVar(b, Sonar, nomFieldSsc, nomFieldCIB, 'suby', suby)

%% Sauvegarde de la Pitch

nomFieldSsc = 'Pitch';
nomFieldCIB = 'grSyncPitch';
write_autoscaleVar(b, Sonar, nomFieldSsc, nomFieldCIB, 'suby', suby)

%% Sauvegarde de Yaw

%{
nomFieldSsc = 'Yaw';
nomFieldCIB = 'grSyncYaw';
write_autoscaleVar(b, Sonar, nomFieldSsc, nomFieldCIB, 'suby', suby)
%}

%% Sauvegarde de grSyncPrtPixNb

nomFieldSsc = 'PrtPixNb';
nomFieldCIB = 'grSyncPrtPixNb';
write_autoscaleVar(b, Sonar, nomFieldSsc, nomFieldCIB, 'suby', suby)

%% Sauvegarde de grSyncStbPixNb

nomFieldSsc = 'StbPixNb';
nomFieldCIB = 'grSyncStbPixNb';
write_autoscaleVar(b, Sonar, nomFieldSsc, nomFieldCIB, 'suby', suby)

%% Sauvegarde de PortAcrossDist

nomFieldSsc = 'PortAcrossDist';
nomFieldCIB = {'grSyncPrtMaxRange', 'grSyncPrtUsrRange'};
write_autoscaleVar(b, Sonar, nomFieldSsc, nomFieldCIB, 'suby', suby)

%% Sauvegarde de StbAcrossDist

nomFieldSsc = 'StbAcrossDist';
nomFieldCIB = {'grSyncStbMaxRange', 'grSyncStbUsrRange'};
write_autoscaleVar(b, Sonar, nomFieldSsc, nomFieldCIB, 'suby', suby)


% Variables non-remplies
% grSyncSensorDrift
% grSyncPrtLatitude
% grSyncPrtLongitude
% grSyncStbLatitude
% grSyncStbLongitude

%% Sauvegarde des images

sz = size(Layers{1});
b = set_valVarAtt(b, NomLayers{1},  'grFirstRow',       1);
b = set_valVarAtt(b, NomLayers{1},  'grLastRow',        sz(1));
b = set_valVarAtt(b, NomLayers{1},  'grFirstColumn',	1);
b = set_valVarAtt(b, NomLayers{1},  'grLastColumn',     sz(2));

% Sauvegarde de la r�solution en Y
nomFieldSsc = 'ResolutionD';
nomFieldCIB = 'grXPixelSize';
if isfield(Sonar, nomFieldSsc) && ~isempty(Sonar.(nomFieldSsc))
    valVarAtt   = Sonar.(nomFieldSsc);
    b           = set_valVarAtt(b, NomLayers{1}, nomFieldCIB, valVarAtt);
    %     if ~(flagRaw && flagWrk)
    %         return
    %     end
end

% Ajout� par JMA le 24/05/2013 : les voies b�bord et tribord sont invers�es
% dans Caraibes, on fait la m�me inversion � la lecture dans import_CaraibesSideScan
Layers{1} = my_fliplr(Layers{1});

% Ecriture des valeurs avec typage en NC_SHORT mais for�age du Scale_factor
% � 1 pour disposer de valeurs cod�es entre [0,255], vue avec GLT le
% 15/02/2014.

write_autoscale(b, NomLayers{1}, Layers{1});
numVar = find_numVar(b, NomLayers{1});
b = set_valVarAtt(b, numVar, 'scale_factor', 1);
b = set_valVarAtt(b, numVar, 'add_offset', 0);


write_autoscale(b, NomLayers{2}, Layers{1});
numVar = find_numVar(b, NomLayers{2});
b = set_valVarAtt(b, numVar, 'scale_factor', 1);
b = set_valVarAtt(b, numVar, 'add_offset', 0); %#ok<NASGU>

% Petit message !
str1 = sprintf('%s\nExport r�ussi !', nomFicOut);
str2 = sprintf('%s\nExport sucessful !', nomFicOut);
my_warndlg(Lang(str1,str2), 0, 'Tag', 'ExportCaraibesTermine', 'TimeDelay', 300);


%__________________________________________________________________________
%% --------------------- SOUS-FONCTIONS -----------------------------------

% --- Ecriture des variables
function write_autoscaleVar(netCDFid, Sonar, nomFieldSsc, nomFieldCIB, varargin)
% exemple :
% nomFieldSsc = 'ResolutionD';
% nomFieldCIB = 'grXPixelSize';
% write_autoscaleVar(b, Sonar, nomFieldSsc, nomFieldCIB, 'suby', suby);
if ~iscellstr(nomFieldCIB)
    nomFieldCIB = cellstr(nomFieldCIB);
end

if isfield(Sonar, nomFieldSsc) && ~isempty(Sonar.(nomFieldSsc))
    [varargin, suby] = getPropertyValue(varargin, 'suby', 1:length(Sonar.(nomFieldSsc))); %#ok<ASGLU>
    
    valVar  = Sonar.(nomFieldSsc)(suby);
    for k=1:length(nomFieldCIB)
        flagRaw = write_autoscale(netCDFid, ['grRawImage_' nomFieldCIB{k}], valVar', 'subl', suby);
        flagWrk = write_autoscale(netCDFid, ['grWrkImage_' nomFieldCIB{k}], valVar', 'subl', suby);
        if ~(flagRaw && flagWrk)
            str1 = ['Erreur � l''�criture du fichier Imo dans la variable : ' nomFieldCIB{k}];
            str2 = ['Error in writing file Imo in variable : ' nomFieldCIB{k}];
            my_warndlg(Lang(str1,str2), 1);
            return
        end
    end
else
    str1 = ['Variable inexistante ou vide d''origine : "' nomFieldSsc '" pour la variable netCDF "' [nomFieldCIB{:}]];
    str2 = ['Variable does not exist or is empty : "' nomFieldSsc '" for netCDF variable "' [nomFieldCIB{:}]];
    my_warndlg(Lang(str1,str2), 0);
end

% --- Fonction de calcul des attributs Facteur Echelle et Offset.
function [ScaleFactor, Offset, MissingValue] = computeScaledVar(Data)
MissingValue    = [];
Offset          = min(Data);
ScaleFactor     = (max(Data) - min(Data))/numel(Data);
if ScaleFactor == 0
    MissingValue = 9999;
end

