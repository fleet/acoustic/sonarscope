% Decodage des valeurs de projection d'une chaine au format ESRI (format
% WKT) dans une structure.
%
% Syntax
%   esri = decodeESRIProj(str)
%
% Input Arguments
%   a : objet de type cl_car_ima
%
% Output Argumentsget_Caraibes_carto
%   x : Abscisses (m)
%   y : Ordonnees (m)
%
% Examples
%   % Lambert
%   nomFic  = 'F:\SonarScopeData\From JMS\EM300-Lambert93.mnt';
%   a       = cl_netcdf('fileName', nomFic);
%   numVar  = find_numVar(a, 'DEPTH');
%   str     = get_valVarAtt(a, numVar, 'esri_pe_string');
%   esri    = decodeESRIProj(str);
%
%   % Mercator
%   nomFic  = 'F:\SonarScopeData\From JMS\EM300-MercatorN46.mnt';
%   a       = cl_netcdf('fileName', nomFic);
%   numVar  = find_numVar(a, 'DEPTH');
%   str     = get_valVarAtt(a, numVar, 'esri_pe_string');
%   esri    = decodeESRIProj(str);
%
%   % UTM
%   nomFic  = 'F:\SonarScopeData\From JMS\EM300-Utm2.mnt';
%   a       = cl_netcdf('fileName', nomFic);
%   numVar  = find_numVar(a, 'DEPTH');
%   str     = get_valVarAtt(a, numVar, 'esri_pe_string');
%   esri    = decodeESRIProj(str);
%
% See also http://geoapi.sourceforge.net/2.0/javadoc/org/opengis/referencing/doc-files/WKT.html#SPHEROID
% Authors : JMA

%-------------------------------------------------------------------------------
function esri = decodeESRIProj(str)

esri = [];

% Chaines � remplacer.
strToReplace = {'["', '"', ']]', ',', ']'};
strOfReplace = '';
[pppp, remain]        = strtok(str, '["');
esri.projgcs.label = regexprep(pppp, strToReplace, strOfReplace, 'preservecase');
[pppp, remain]  = strtok(remain, '["');
esri.projgcs.coordsystem = regexprep(pppp, strToReplace, strOfReplace, 'preservecase');
[pppp, remain] = strtok(remain, '["');
esri.projgcs.geogcs.label = regexprep(pppp, strToReplace, strOfReplace, 'preservecase');
[pppp, remain] = strtok(remain, '["');
esri.projgcs.geogcs.geoid = regexprep(pppp, strToReplace, strOfReplace, 'preservecase');

%% Datum
[~, remain]                     = strtok(remain, '["');
[pppp, remain] = strtok(remain, '["');
esri.projgcs.geogcs.datum = regexprep(pppp, strToReplace, strOfReplace, 'preservecase');
[~, remain]                     = strtok(remain, '["');

%% Spheroid
[pppp, remain]                 = strtok(remain, ']],');
esri.projgcs.geogcs.spheroid.name = regexprep(pppp, strToReplace, strOfReplace, 'preservecase');
[pppp, remain]      = strtok(remain, ',');
esri.projgcs.geogcs.spheroid.Half_great_axis = regexprep(pppp, strToReplace, strOfReplace, 'preservecase');
% esri.projgcs.geogcs.spheroid.Half_great_axis              = deblank(esri.projgcs.geogcs.spheroid.Half_great_axis);
esri.projgcs.geogcs.spheroid.Half_great_axis                = []; % ???? Comment r�cup�rer cette valeur ?
[pppp, remain]    = strtok(remain, ']],');
esri.projgcs.geogcs.spheroid.InverseFlattening = regexprep(pppp, strToReplace, strOfReplace, 'preservecase');
% esri.projgcs.geogcs.spheroid.InverseFattening             = deblank(esri.projgcs.geogcs.spheroid.InverseFlattening);
esri.projgcs.geogcs.spheroid.InverseFlattening              = []; % ???? Comment r�cup�rer cette valeur ?

%% Prime meridian.
[pppp, remain]  = strtok(remain, '["');
esri.projgcs.geogcs.primem.label = regexprep(pppp, strToReplace, strOfReplace, 'preservecase');
[pppp, remain]   = strtok(remain, ',');
esri.projgcs.geogcs.primem.name = regexprep(pppp, strToReplace, strOfReplace, 'preservecase');
[pppp, remain] = strtok(remain, '"]');
esri.projgcs.geogcs.primem.offset = regexprep(pppp, strToReplace, strOfReplace, 'preservecase');
%% Unit�
[pppp, remain]    = strtok(remain, '["');
esri.projgcs.geogcs.unit.label = regexprep(pppp, strToReplace, strOfReplace, 'preservecase');
[pppp, remain]     = strtok(remain, ',');
esri.projgcs.geogcs.unit.name = regexprep(pppp, strToReplace, strOfReplace, 'preservecase');
[pppp, remain]      = strtok(remain, '],');
esri.projgcs.geogcs.unit.pas = regexprep(pppp, strToReplace, strOfReplace, 'preservecase');
%% Projection
[pppp, remain] = strtok(remain, '["');
esri.projgcs.projection.label = regexprep(pppp, strToReplace, strOfReplace, 'preservecase');
[pppp, remain]  = strtok(remain, '["');
esri.projgcs.projection.name = regexprep(pppp, strToReplace, strOfReplace, 'preservecase');
pppp2 = strfind(remain, 'UNIT');
strParameter = remain(4:pppp2-2);
pppp = strfind(strParameter, 'PARAMETER');
% esri.projgcs.projection.parameter.name{1:size(pppp)} = [];
% esri.projgcs.projection.parameter.value{1:size(pppp)} = [];
for k=1:size(pppp,2)
    % Lecture de PARAMETER
    [~, strParameter] = strtok(strParameter, '["');
%     % Lecture du nom du param�tre
%     [esri.projgcs.projection.parameter.name{k}, strParameter] = strtok(strParameter, '["');
%     strParameter = strParameter(3:end);
%     % Lecture de la valeur
%     [esri.projgcs.projection.parameter.value{k}, strParameter] = strtok(strParameter, ']');
     [nameParameter, strParameter] = strtok(strParameter, '["');
     strParameter                  = strParameter(3:end);
%     % Lecture de la valeur
     [pppp, strParameter]                       = strtok(strParameter, ']');
     valParameter = regexprep(pppp, strToReplace, strOfReplace, 'preservecase');
     esri.projgcs.projection.parameter.(nameParameter) = valParameter;
end
remain = remain(pppp2:end);
% Lecture de PARAMETER
[pppp, remain]    = strtok(remain, '["');
esri.projgcs.projection.unit.label = regexprep(pppp, strToReplace, strOfReplace, 'preservecase');
[pppp, remain]    = strtok(remain, '["');
esri.projgcs.projection.unit.name  = regexprep(pppp, strToReplace, strOfReplace, 'preservecase');
pppp              = strtok(remain, ']]');
esri.projgcs.projection.unit.value = regexprep(pppp, strToReplace, strOfReplace, 'preservecase');
