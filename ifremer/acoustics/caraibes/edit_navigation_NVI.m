%   ListeFicNvi = {'C:\SonarScopeData\Sonar\EM300_Ex1.nvi'};
%   flag = edit_navigation_NVI(ListeFicNvi);
%   ListeFicNvi = 'C:\SonarScopeData\Sonar\EM300_Ex1.nvi';
%   flag = edit_navigation_NVI(ListeFicNvi);

function flag = edit_navigation_NVI(ListeFicNvi, varargin)

[varargin, Fig]  = getPropertyValue(varargin, 'Fig',  []);
[varargin, Name] = getPropertyValue(varargin, 'Name', 'Navigation'); %#ok<ASGLU>

if ischar(ListeFicNvi)
    ListeFicNvi = {ListeFicNvi};
end

%% Lecture des fichiers

% TODO MHO : Je sors deux fois la m�me info car je veux comparer le signal
% obtenu apr�s {NavigationDialog, a.openDialog()} au signal original. Cela
% n'est pas tr�s �l�gant, il y a certainement un moyen plus appropri� genre
% un attribut de NavigationDialog qui pourrait indiquer si un signal a �t�
% modifi�
[flag, navRaw, nav, Type] = Nvi2ClNavigation(ListeFicNvi, Fig, Name);
if ~flag
    return
end

%% Create the GUI

a = NavigationDialog(nav, 'Title', 'Navigation', 'CleanInterpolation', true);
a.openDialog();
if ~a.okPressedOut
    flag = 0;
    return
end

%% Check if the navigation has been modified

NbFic = length(ListeFicNvi);
for k1=1:NbFic
    signalContainerFic = a.signalContainerList(k1);
    signalFic = signalContainerFic.signalList;
    %     subNonNaN = find(isfinite(signalFic.ySample(1).data));
    if length(navRaw(k1).signalList(1).ySample.data) == length(nav(k1).signalList(1).ySample.data)
        for k2=1:length(signalFic)
            if isequal(signalFic(k2).ySample.data, navRaw(k1).signalList(k2).ySample.data)
                %                 NavModified(k1) = false; %#ok<AGROW>
                NavModified(k1) = true; %#ok<AGROW>  TODO on force � true car rien ne fonctionne (clonable, nawRaw, nav, navNew, ...)
                break
            else
                NavModified(k1) = true; %#ok<AGROW>
            end
        end
    else
        NavModified(k1) = true;
    end
end
if all(~NavModified)
    return % Aucune navigation n'a �t� modifi�e
end

%% Export des navigations

str1 = 'Voulez-vous sauvegarder cette nouvelle navigation ?';
str2 = 'Do you want to export this new navigation  ?';
[rep, flag] = my_questdlg(Lang(str1,str2));
if ~flag || (rep == 2)
    return
end

%% Sauvegarde des signaux corrig�s dans le r�pertoire cache de SSc

str1 = 'Sauvegarde de la navigation.';
str2 = 'Export of the navigation';
hw = create_waitbar(Lang(str1,str2), 'N', NbFic);
for k=1:NbFic
    signalContainerFic = a.signalContainerList(k);
    signalFic = signalContainerFic.signalList;
    my_waitbar(k, NbFic, hw)
    if NavModified(k)
        T         = signalFic(1).xSample.data;
        Latitude  = signalContainerFic.getLatitudeSignalSample().data;
        Longitude = signalContainerFic.getLongitudeSignalSample().data;
        if isempty(signalContainerFic.getImmersionSignalSample())
            Immersion = [];
        else
            Immersion = signalContainerFic.getImmersionSignalSample().data;
        end
    else
        str1 = sprintf('"%s" n''a pas �t� modifi�, donc pas d''export.', signalContainerFic.name);
        str2 = sprintf('"%s" was not changed, no export is done for this navigation.', signalContainerFic.name);
        my_warndlg(Lang(str1,str2), 0);
        continue
    end
    
    %% Export result in a file
    
    [pathname, filename, Ext] = fileparts(ListeFicNvi{k});
    nomFicOut = fullfile(pathname, [filename '-new' Ext]);
    
    switch Type
        case 'DelphINS'
            Heading = signalContainerFic.signalList(4).ySample.data;
            Roll    = signalContainerFic.signalList(5).ySample.data;
            Pitch   = signalContainerFic.signalList(6).ySample.data;
            Heave   = signalContainerFic.signalList(7).ySample.data;
            flag = export_DELPHINS(nomFicOut, T,  Latitude, Longitude, ...
                Immersion, Heading, Pitch, Roll, Heave);
            
        case 'Genavir_AUV'
            flag = export_Genavir_AUV(nomFicOut, T,  Latitude, Longitude, Immersion);
            
        case 'VLIZ'
            flag = export_VLIZ(nomFicOut, T,  Latitude, Longitude);
            
        case 'Caraibes'
            Height     = zeros(1,length(Latitude));
            Heading    = zeros(1,length(Latitude));
            SonarSpeed = zeros(1,length(Latitude));
            export_nav_Caraibes(nomFicOut, T, Latitude, Longitude, SonarSpeed, Heading, Height, Immersion);
            
        otherwise
            str1 = sprintf('La sauvegarde de la navigation au format "%s" n''est pas encore cod�', Type);
            str2 = sprintf('The export of navigation in "%s" format is not written yet', Type);
            my_warndlg(Lang(str1,str2), 1);
    end
end



%% Check result

%{
timeSample      = XSample('name', 'time', 'data', T(subNonNaN));
latSample       = YSample('data', Latitude(subNonNaN));
lonSample       = YSample('data', Longitude(subNonNaN));
immersionSample = YSample('name', 'Immersion', 'unit', 'm',   'data', -Immersion(subNonNaN));
headingSample   = YSample('name', 'Heading',   'unit', 'deg', 'data', Heading(subNonNaN));

if isempty(Pitch)
pitch = [];
roll  = [];
heave = [];
else
pitch = YSample('name', 'Pitch',     'unit', 'deg', 'data', Pitch(subNonNaN));
roll  = YSample('name', 'Roll',      'unit', 'deg', 'data', Roll(subNonNaN));
heave = YSample('name', 'Heave  ',   'unit', 'm',   'data', Heave(subNonNaN));
end

nav = ClNavigation(latSample, lonSample, 'headingVesselSample', headingSample, 'immersionSample', immersionSample, ...
'xSample', timeSample, ...
'ySample', [roll pitch heave], ...
'name',  'Check');
b = NavigationDialog(nav, 'Title', 'Navigation', 'windowStyle', 'normal');
b.openDialog();
%}
