% Encodage des valeurs de projection dans une chaine au format ESRI (format
% WKT).
%
% Syntax
%   esri = encodeESRIProj(str)
%
% Input Arguments
%   a : objet de type cl_car_ima
%
% Output Argumentsget_Caraibes_carto
%   x : Abscisses (m)
%   y : Ordonnees (m)
%
% Examples
%   % Lambert
%	geoInfo = ADU
%   esri = encodeESRIProj(geoInfo);
%
%   % UTM
%     clear geoInfo;
%     geoInfo.Projection              = 'Mercator';
%     geoInfo.Ellipsoid_name          = 'GCS_WGS_1984';
%     geoInfo.Datum                   = 'D_WGS_1984';
%     geoInfo.Spheroid                = 'WGS_1984';
%     geoInfo.Half_great_axis         = 6378137.000000;
%     geoInfo.InverseFlattening       = 298.257224;
%     geoInfo.Longitude               = 0.0;
%     geoInfo.ConversionFactor        = 0.017453292519943;
%     geoInfo.ProjectionName          = 'Mercator';
%
%     geoInfo.projParam(1).name       = 'False_Easting';
%     geoInfo.projParam(1).value      = 0.0;
%     geoInfo.projParam(2).name       = 'False_Norting';
%     geoInfo.projParam(2).value      = 0.0;
%     geoInfo.projParam(3).name       = 'Central_Meridian';
%     geoInfo.projParam(3).value      = Meridian_180;
%     geoInfo.projParam(4).name       = 'Standard_Parallel_1';
%     geoInfo.projParam(4).value      = Mercator_Reference_latitude;
%     geoInfo.projection.unit.name    = 'Meters';
%     geoInfo.projection.unit.value   = 1.0;
%     esriProj = encodeESRIProj(geoInfo);
%   esri = encodeESRIProj(geoInfo);
%
% See also
% http://www.geoapi.org/2.0/javadoc/org/opengis/referencing/doc-files/WKT.html
% Authors : JMA
%-------------------------------------------------------------------------------

function strESRIProj = encodeESRIProj(geoInfo)

%% Syst�me de coordonn�es

esri.projgcs.label        = 'PROJCS';
esri.projgcs.coordsystem  = geoInfo.Projection;
esri.projgcs.geogcs.label = 'GEOGCS';
esri.projgcs.geogcs.geoid = geoInfo.Ellipsoid_name;

%% Datum

esri.projgcs.geogcs.datum.label = 'DATUM';
esri.projgcs.geogcs.datum.name  = geoInfo.Datum;

%% Spheroid

esri.projgcs.geogcs.spheroid.label             = 'SPHEROID';
esri.projgcs.geogcs.spheroid.name              = geoInfo.Spheroid;
esri.projgcs.geogcs.spheroid.Half_great_axis   = geoInfo.Half_great_axis;
esri.projgcs.geogcs.spheroid.InverseFlattening = geoInfo.InverseFlattening;

%% Prime meridian

esri.projgcs.geogcs.primem.label = 'PRIMEM';
esri.projgcs.geogcs.primem.name  = 'Greenwich';
esri.projgcs.geogcs.primem.value = geoInfo.Longitude;

% Unit�s du Prime M�ridian
esri.projgcs.geogcs.primem.unit.label = 'UNIT';
esri.projgcs.geogcs.primem.unit.name  = 'Degrees';
esri.projgcs.geogcs.primem.unit.value = geoInfo.ConversionFactor;

%% Projection
esri.projgcs.projection.label = 'PROJECTION';
esri.projgcs.projection.name  = geoInfo.ProjectionName;

% Param�tres de la projection
esri.projgcs.projection.parametre.label = 'PARAMETER';
esri.projgcs.projection.parametre.name  = {geoInfo.projParam.name};
esri.projgcs.projection.parametre.value = [geoInfo.projParam.value];

% Unit� de la projection
esri.projgcs.projection.unit.label = 'UNIT';
esri.projgcs.projection.unit.name  = geoInfo.projection.unit.name;
esri.projgcs.projection.unit.value = geoInfo.projection.unit.value;

%% Encodage de la cha�ne Well-Known Text de projection ESRI

str = {};
str{end+1} =  [esri.projgcs.label '["'];
str{end+1} =  [esri.projgcs.coordsystem '",'];
str{end+1} =  [esri.projgcs.geogcs.label '["'];
str{end+1} =  [esri.projgcs.geogcs.geoid '",'];
str{end+1} =  [esri.projgcs.geogcs.datum.label '["'];
str{end+1} =  [esri.projgcs.geogcs.datum.name '",'];
str{end+1} =  [esri.projgcs.geogcs.spheroid.label '["'];
str{end+1} =  [esri.projgcs.geogcs.spheroid.name '",'];
str{end+1} =  [num2str(esri.projgcs.geogcs.spheroid.Half_great_axis, '%f') ','];
str{end+1} =  [num2str(esri.projgcs.geogcs.spheroid.InverseFlattening, '%f') ']],'];
str{end+1} =  [esri.projgcs.geogcs.primem.label '["'];
str{end+1} =  [esri.projgcs.geogcs.primem.name '",'];
str{end+1} =  [num2str(esri.projgcs.geogcs.primem.value, '%f') '],'];
str{end+1} =  [esri.projgcs.geogcs.primem.unit.label '["'];
str{end+1} =  [esri.projgcs.geogcs.primem.unit.name '",'];
str{end+1} =  [num2str(esri.projgcs.geogcs.primem.unit.value, '%15.12f') ']],'];
str{end+1} =  [esri.projgcs.projection.label '["'];
str{end+1} =  [esri.projgcs.projection.name '"],'];

n2 = size(geoInfo.projParam,2);
if n2 > 0
    for k=1:n2
        str{end+1} = [esri.projgcs.projection.parametre.label, '["']; %#ok<AGROW>
        str{end+1} = [esri.projgcs.projection.parametre.name{k}, '",']; %#ok<AGROW>
        str{end+1} = [num2str(esri.projgcs.projection.parametre.value(k), '%f') '],']; %#ok<AGROW>
    end
end
str{end+1} = [esri.projgcs.projection.unit.label '["'];
str{end+1} = [esri.projgcs.projection.unit.name '",'];
str{end+1} = [num2str(esri.projgcs.projection.unit.value, '%f') ']]'];

% Concat�nation de la cha�ne ESRI.
strESRIProj = [str{:}];
