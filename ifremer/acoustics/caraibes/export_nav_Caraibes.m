function flag = export_nav_Caraibes(nomFicNav, T, Latitude, Longitude, SonarSpeed, Heading, ...
    Height, Immersion, varargin)

[varargin, mbShip]      = getPropertyValue(varargin, 'mbShip',      'BlackPearl');
[varargin, mbSurvey]    = getPropertyValue(varargin, 'mbSurvey',    'Caraibes');
[varargin, mbHistAutor] = getPropertyValue(varargin, 'mbHistAutor', 'SonarScope');
[varargin, mbName]      = getPropertyValue(varargin, 'mbName',      'Johnny');
[varargin, Ellipsoid]   = getPropertyValue(varargin, 'Ellipsoid',   'wgs84'); %#ok<ASGLU>

flag = 0;

%% Lecture du skelette Modele

nomFicRef = getNomFicDatabase('RefCIB36_EM300.nvi');

a = cl_netcdf('fileName', nomFicRef);
sk_ref = get(a, 'skeleton');

% Attributs globaux du format SMF g�or�f�renc�

[Y,M,D] = ymd(T);
date = juliandate(datetime(Y,M,D)) + 1; % TODO : pourquoi + 1 ? Si on ne fait pas �a, �a donne je jour pr�c�dent !!! juliandate serait-il bugu� ?
if isempty(date)
    messageErreurFichier(nomFicRef, 'ReadFailure');
    return
end

[h,m,s] = hms(T);
heure = ((h* 60 + m) * 60 + s) * 1000; % heure(end)

mbStartDate = date(1);
mbEndDate   = date(end);
mbStartTime = heure(1);
mbEndTime   = heure(end);

% Variables du format SMF de navigation
mbNorthLatitude = max(Latitude);
mbSouthLatitude = min(Latitude);

mbEastLongitude = max(Longitude);
mbWestLongitude = min(Longitude);

% Dimensions du format SMF de navigation
mbPositionNbr = length(Longitude);

%% Modification du skelette

sk = sk_ref;

% Attributs globaux du format SMF g�n�rique
numAtt = cdf_find_numAtt(sk, 'mbName');
sk.att(numAtt).value  = mbName;

% Attributs globaux du format SMF g�or�f�renc�
numAtt = cdf_find_numAtt(sk, 'mbStartDate');
sk.att(numAtt).value  = mbStartDate;

numAtt = cdf_find_numAtt(sk, 'mbStartTime');
sk.att(numAtt).value  = mbStartTime;

numAtt = cdf_find_numAtt(sk, 'mbEndDate');
sk.att(numAtt).value  = mbEndDate;

numAtt = cdf_find_numAtt(sk, 'mbEndTime');
sk.att(numAtt).value = mbEndTime;

numAtt = cdf_find_numAtt(sk, 'mbNorthLatitude');
sk.att(numAtt).value = mbNorthLatitude;

numAtt = cdf_find_numAtt(sk, 'mbSouthLatitude');
sk.att(numAtt).value = mbSouthLatitude;

numAtt = cdf_find_numAtt(sk, 'mbEastLongitude');
sk.att(numAtt).value = mbEastLongitude;

numAtt = cdf_find_numAtt(sk, 'mbWestLongitude');
sk.att(numAtt).value = mbWestLongitude;

% Attributs globaux du format SMF de navigation
numAtt = cdf_find_numAtt(sk, 'mbShip');
sk.att(numAtt).value = mbShip;

numAtt = cdf_find_numAtt(sk, 'mbSurvey');
sk.att(numAtt).value = mbSurvey;

numAtt = cdf_find_numAtt(sk, 'mbPointCounter');
sk.att(numAtt).value = mbPositionNbr;

numAtt = cdf_find_numAtt(sk, 'mbReference');
sk.att(numAtt).value = Ellipsoid;

% Dimensions du format SMF de navigation
numDim = cdf_find_numDim(sk, 'mbPositionNbr');
sk.dim(numDim).value = mbPositionNbr;    % 'mbPositionNbr'

%% Cr�ation du fichier r�sultat

b = cl_netcdf('fileName', nomFicNav, 'skeleton', sk);

%% Remplissage des valeurs des variables

% Dimensions du format SMF g�n�rique
numDim = cdf_find_numDim(sk, 'mbNameLength');
mbNameLength = sk.dim(numDim).value;

% Variables du format SMF g�n�rique.
v = get_value(a, 'mbHistAutor');
N = length(mbHistAutor);
v(1,1:N) = mbHistAutor;
v(1,N+1:mbNameLength) = repmat(' ', 1, mbNameLength-N);
b = set_value(b, 'mbHistAutor', v);

v = get_value(a, 'mbHistModule');
mbHistModule = 'SonarScope';
N = length(mbHistModule);
v(1,1:N) = mbHistModule;
v(1,N+1:mbNameLength) = repmat(' ', 1, mbNameLength-N);
b = set_value(b, 'mbHistModule', v);

% Variables du format SMF de navigation.
b = set_value(b, 'mbDate', date);

b = set_value(b, 'mbTime', heure);

b = set_value(b, 'mbOrdinate', Latitude);

b = set_value(b, 'mbAbscissa', Longitude);

b = set_value(b, 'mbPFlag', 2+zeros(size(Longitude)));

if ~isempty(Height)
    b = set_value(b, 'mbAltitude', Height);
end

if ~isempty(Immersion)
    b = set_value(b, 'mbImmersion', Immersion);
end

if ~isempty(Heading)
    b = set_value(b, 'mbHeading', Heading);
end

if ~isempty(SonarSpeed)
    b = set_value(b, 'mbSpeed', SonarSpeed); %#ok
end

flag = 1;
