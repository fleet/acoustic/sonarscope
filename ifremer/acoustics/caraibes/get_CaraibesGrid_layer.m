% Lecture d'un layer contenu dans un fichier grille de Caribes (.dtm, .mos, ...)
%
% Syntax
%   value = get_CaraibesGrid_layer(nomFic, NomLayer, ...)
%
% Input Arguments
%   nomFic   : Nom du fichier MNT ou MOS
%   NomLayer : Nom de la variable
%
% Name-Value Pair Arguments
%   subx         : subsampling in X
%   suby         : subsampling in Y
%
% Output Arguments
%   value : Valeur du parametre ramene a l'unite
%
% Examples
%   nomFic = getNomFicDatabase('EM12D_PRISMED_500m.mnt')
%   [x, y, GeometryType] = get_CaraibesGrid_xy(nomFic)
%   I = get_CaraibesGrid_layer(nomFic, 'Depth');
%   figure; imagesc(x, y, I); axis xy; colorbar;
%
% See also cl_netcdf cl_netcdf/get_value Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function value = get_CaraibesGrid_layer(nomFic, NomLayer, varargin)

[varargin, suby] = getPropertyValue(varargin, 'suby', []);
[varargin, subx] = getPropertyValue(varargin, 'subx', []); %#ok<ASGLU>

in = cl_netcdf('fileName', nomFic);

Layer_name = get_value(in, 'Layer_name');
for k=1:size(Layer_name, 1)
    if strcmpi(NomLayer, deblank(Layer_name(k,:)))
        ident = k;
        break
    end
end

%% Bidouille pour savoir comment interpréter le NC_BYTE -uint8 ou int8) ?

Minimum_value = get_value(in, 'Minimum_value', 'noMessage');
Maximum_value = get_value(in, 'Maximum_value', 'noMessage');
if ~isempty(Minimum_value) && ~isempty(Maximum_value)
    Minimum_value = Minimum_value(ident);
    Maximum_value = Maximum_value(ident);
else
    Minimum_value = [];
    Maximum_value = [];
end

if strcmpi(NomLayer, 'LATITUDE') || strcmpi(NomLayer, 'LONGITUDE')
    value = get_value(in, NomLayer, 'sub1', subx, 'sub2', suby, 'Convert', 'double', 'MinMaxvalue', [Minimum_value Maximum_value]);
else
    value = get_value(in, NomLayer, 'sub1', subx, 'sub2', suby, 'MinMaxvalue', [Minimum_value Maximum_value]); % , 'CharIsByte');
end

if isempty(value)
    return
end

if ischar(value)
    value = uint8(value);
end

% ---------------------------------------------------------------------
% Test si le fichier est au nouveau format (scale_factor et add_offset
% contenus dans la variable, dans ce cas on n'utilise plus les A_resol,
% ...

scale_factor = get_valVarAtt(in, NomLayer, 'scale_factor', 'noMessage');
if isempty(scale_factor)
    Sign_value = get_value(in, 'Sign_value', 'Convert', 'double');
    A_resol = get_value(in, 'A_resol', 'Convert', 'double');
    B_resol = get_value(in, 'B_resol', 'Convert', 'double');
    
    % --------------------------
    % Traitement des non valeurs
    
    % subNaN = find(val == Sign_value);
    subNaN = (value == Sign_value(ident));
    value(subNaN) = NaN;
    
    % --------------------------------------------------------------
    % Traitement des valeurs codees en dehrors des bornes specifiees
    
    if  A_resol(ident) ~= 1
        value = double(A_resol(ident)) * value;
    end
    if  B_resol(ident) ~= 0
        value = value +  double(B_resol(ident));
    end
end
