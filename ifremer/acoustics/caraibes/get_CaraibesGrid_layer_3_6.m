% Lecture d'un layer contenu dans un fichier grille de Caraibes (.dtm, .mos, ...)
%
% Syntax
%   value = get_CaraibesGrid_layer_3_6(nomFic, NomLayer, ...)
%
% Input Arguments
%   nomFic   : Nom du fichier MNT ou MOS
%   NomLayer : Nom de la variable
%
% Name-Value Pair Arguments
%   subx         : subsampling in X
%   suby         : subsampling in Y
%
% Output Arguments
%   value : Valeur du parametre ramene a l'unite
%
% Examples
%   nomFic = getNomFicDatabase('EM12D_PRISMED_500m.mnt')
%   [x, y, GeometryType] = get_CaraibesGrid_xy(nomFic)
%   I = get_CaraibesGrid_layer_3_6(nomFic, 'Depth');
%   figure; imagesc(x, y, I); axis xy; colorbar;
%
% See also cl_netcdf cl_netcdf/get_value Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function value = get_CaraibesGrid_layer_3_6(nomFic, NomLayer, varargin)

[varargin, suby] = getPropertyValue(varargin, 'suby', []);
[varargin, subx] = getPropertyValue(varargin, 'subx', []); %#ok<ASGLU>

in = cl_netcdf('fileName', nomFic);

Minimum_value = get_valVarAtt(in, NomLayer, 'valid_minimum');
Maximum_value = get_valVarAtt(in, NomLayer, 'valid_maximum');
% if ~isempty(Minimum_value) && ~isempty(Maximum_value)
%     Minimum_value = Minimum_value(ident);
%     Maximum_value = Maximum_value(ident);
% else
%     Minimum_value = [];
%     Maximum_value = [];
% end

if strcmpi(NomLayer, 'LATITUDE') || strcmpi(NomLayer, 'LONGITUDE')
    value = get_value(in, NomLayer, 'sub1', subx, 'sub2', suby, 'Convert', 'double', 'MinMaxvalue', [Minimum_value Maximum_value]);
else
    value = get_value(in, NomLayer, 'sub1', subx, 'sub2', suby, 'MinMaxvalue', [Minimum_value Maximum_value]); % , 'CharIsByte');
end

if isempty(value)
    return
end

if ischar(value)
    value = uint8(value);
end
