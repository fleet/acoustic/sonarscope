% Lecture des coordonnees des layers de l'image
%
% Syntax
%   [x, y, GeometryType] = get_CaraibesGrid_xy_3_6(nomFic)
%
% Input Arguments
%   a : objet de type cl_car_ima
% 
% Output Arguments
%   x : Abscisses (m)
%   y : Ordonnees (m)
% 
% Examples
%   nomFic = 'F:\SonarScopeData\From JMS\EM300-Lambert93.mnt';
%   Carto = get_Caraibes_carto_3_6(nomFic);
%   [x, y, GeometryType] = get_CaraibesGrid_xy_3_6(nomFic, Carto);
%   I = get_CaraibesGrid_layer(nomFic, 'Depth');
%   figure; imagesc(x, y, I); axis xy; colorbar;
%
% See also Authors
% Authors : JMA
%-------------------------------------------------------------------------------
 
function [x, y, GeometryType] = get_CaraibesGrid_xy_3_6(nomFic, Carto)

in = cl_netcdf('fileName', nomFic);

if get(Carto, 'Projection.Type') == 1
    GeometryType = cl_image.indGeometryType('LatLong');
else
    GeometryType = cl_image.indGeometryType('GeoYX');
end

%     resolutionCaraibes      = get(this, 'Element_x_size') / 1000;
Number_lines   = get_valAtt(in, 'Number_lines');
Number_columns = get_valAtt(in, 'Number_columns');

LINES   = get_valDim(in, 'LINES');
COLUMNS = get_valDim(in, 'COLUMNS');

if (Number_lines ~= LINES) || (Number_columns ~= COLUMNS)
    % TODO : str1 en fran�ais
    str1 = 'Warning, there is an inconsistency between Number_lines and Number_lines or/and Number_columns and COLUMNS in this file, geographic coordinates willl certainly be false.';
    str2 = 'Warning, there is an inconsistency between Number_lines and Number_lines or/and Number_columns and COLUMNS in this file, geographic coordinates willl certainly be false.';
    my_warndlg(Lang(str1,str2), 0);
end
 
Xmin_metric = get_valAtt(in, 'Xmin_metric');
Xmax_metric = get_valAtt(in, 'Xmax_metric');
Ymin_metric = get_valAtt(in, 'Ymin_metric');
Ymax_metric = get_valAtt(in, 'Ymax_metric');

Orientation = get_valAtt(in, 'Orientation');
if Orientation ~= 0
    str = sprintf('Warning : Orientation=%f degres, LatLong image conversion will not be possible', Orientation);
    GeometryType = cl_image.indGeometryType('OrthoYX');
    my_warndlg(str, 1);
    
    
% % Impossible de comprendre la logique de Caraibes : j'abandonne
    
%     Carto = get_Caraibes_carto(nomFic);
    Latitude_BL  = get_valAtt(in, 'Latitude_BL') * 1e-6;
%     Latitude_BR  = get_value(in, 'Latitude_BR') * 1e-6;
    Longitude_BL = get_valAtt(in, 'Longitude_BL') * 1e-6;
    Longitude_BR = get_valAtt(in, 'Longitude_BR') * 1e-6;
    Latitude_TL  = get_valAtt(in, 'Latitude_TL') * 1e-6;
%     Latitude_TR  = get_value(in, 'Latitude_TR') * 1e-6;
%     Longitude_TL = get_value(in, 'Longitude_TL') * 1e-6;
%     Longitude_TR = get_value(in, 'Longitude_TR') * 1e-6;
    [x1, y1] = latlon2xy_approx(Latitude_BL, Longitude_BL);
    [x2, y2] = latlon2xy_approx(Latitude_BL, Longitude_BR);
    [x3, y3] = latlon2xy_approx(Latitude_TL, Longitude_BR);
    [x4, y4] = latlon2xy_approx(Latitude_TL, Longitude_BL);
    
    c = cosd(Orientation);
    s = sind(Orientation);
    x1p = Xmin_metric + c * (x1) - s * (y1);
	y1p = Ymin_metric + s * (x1) + c * (y1);
    x2p = Xmin_metric + c * (x2) - s * (y2);
	y2p = Ymin_metric + s * (x2) + c * (y2);
    x3p = Xmin_metric + c * (x3) - s * (y3);
	y3p = Ymin_metric + s * (x3) + c * (y3);
    x4p = Xmin_metric + c * (x4) - s * (y4);
	y4p = Ymin_metric + s * (x4) + c * (y4);

    figure; plot([x1, x2, x3, x4], [y1, y2, y3, y4], 'ob'); grid on; axis equal;
    figure; plot([x1p, x2p, x3p, x4p], [y1p, y2p, y3p, y4p], 'or'); grid on; axis equal;
    
%     Xmin_metric = min([x1 x2 x3 x4]);
%     Xmax_metric = max([x1 x2 x3 x4]);
%     Ymin_metric = min([y1 y2 y3 y4]);
%     Ymax_metric = max([y1 y2 y3 y4]);
% 
%     c = cosd(Orientation);
%     s = sind(Orientation);
%     NewXmax_metric = Xmin_metric + c * (Xmax_metric - Xmin_metric) - s * (Ymax_metric - Ymin_metric);
%     NewYmax_metric = Ymin_metric + s * (Xmax_metric - Xmin_metric) + c * (Ymax_metric - Ymin_metric);
%     Xmax_metric = NewXmax_metric;
%     Ymax_metric = NewYmax_metric;
end

% Merci Caraibes : pb constate sur : /home2/brekh/augustin/Results_MarcRoche/ThorntonBank/Th0028/etude_chevauch/EM1002_MARCROCHE_Th0028_3profilmil1.mnt
X1 = Ymin_metric;
X2 = Ymax_metric;
Ymin_metric = min(X1, X2);
Ymax_metric = max(X1, X2);

if ((Xmin_metric == 0) && (Ymin_metric == 0)) || (Ymin_metric == 0)
    str1 = 'Les descripteurs de positionnement dans le fichier Caraibes sont incomplets, j''utilise une m�thode non garantie pour positionner l''image.';
    % TODO : str2 en anglais
    str2 = 'Les descripteurs de positionnement dans le fichier Caraibes sont incomplets, j''utilise une m�thode non garantie pour positionner l''image.';
    my_warndlg(Lang(str1,str2), 1);
%     Carto = get_carto(this);
%     Latitude_BL  = get(this, 'Latitude_BL') * 1e-6;
%     Latitude_BR  = get(this, 'Latitude_BR') * 1e-6;
%     Longitude_BL = get(this, 'Longitude_BL') * 1e-6;
%     Longitude_BR = get(this, 'Longitude_BR') * 1e-6;
%     Latitude_TL  = get(this, 'Latitude_TL') * 1e-6;
%     Latitude_TR  = get(this, 'Latitude_TR') * 1e-6;
%     Longitude_TL = get(this, 'Longitude_TL') * 1e-6;
%     Longitude_TR = get(this, 'Longitude_TR') * 1e-6;
%     [x1, y1] = latlon2xy(Latitude_BL, Longitude_BL)
%     [x2, y2] = latlon2xy(Latitude_BL, Longitude_BR)
%     [x3, y3] = latlon2xy(Latitude_TL, Longitude_BR)
%     [x4, y4] = latlon2xy(Latitude_TL, Longitude_BL)
%     Xmin_metric = min([x1 x2 x3 x4]);
%     Xmax_metric = max([x1 x2 x3 x4]);
%     Ymin_metric = min([y1 y2 y3 y4]);
%     Ymax_metric = max([y1 y2 y3 y4]);
    GeometryType = cl_image.indGeometryType('LatLong');
    South_latitude  = get_value(in, 'mbSouthLatitude');
    North_latitude  = get_value(in, 'mbNorthLatitude');
    West_longitude  = get_value(in, 'mbWestLongitude');
    East_longitude  = get_value(in, 'mbEastLongitude');

    Xmin_metric = West_longitude;
    Xmax_metric = East_longitude;
    Ymin_metric = South_latitude;
    Ymax_metric = North_latitude;
end

%     Mercator_Reference_latitude = get(this, 'Mercator_Reference_latitude') * 1e-6;
%     Mercator_Central_meridian   = get(this, 'Mercator_Central_meridian') * 1e-6;

% resolutionCalculee = (Xmax_metric - Xmin_metric) / (Number_columns - 1);

xmin = min(Xmin_metric, Xmax_metric); % Eh oui, c'est arrive
xmax = max(Xmin_metric, Xmax_metric);

x = linspace(xmin, xmax, Number_columns);
y = linspace(Ymin_metric, Ymax_metric, Number_lines);

x = centrage_magnetique(x);
y = centrage_magnetique(y);

