% Lecture des coordonnees des layers de l'image
%
% Syntax
%   b = get_Caraibes_carto(nomFic)
%
% Input Arguments
%   a : objet de type cl_car_ima
%
% Output Arguments
%   x : Abscisses (m)
%   y : Ordonnees (m)
%
% Examples
%   nomFic = getNomFicDatabase('EM12D_PRISMED_500m.mnt');
%   Carto = get_Caraibes_carto(nomFic)
%
% See also Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function b = get_Caraibes_carto(nomFic)

in = cl_netcdf('fileName', nomFic);

b = cl_carto;

Half_great_axis = get_value(in, 'Half_great_axis', 'Convert', 'double') * 0.01;
if isempty(Half_great_axis)
    Half_great_axis = get_valAtt(in, 'mbEllipsoidA');
end
Square_eccentricity = get_value(in, 'Square_eccentricity', 'Convert', 'double');
E = sqrt(Square_eccentricity) * 1e-5;

if isempty(Square_eccentricity)
    Square_eccentricity = get_valAtt(in, 'mbEllipsoidE2');
    E = sqrt(Square_eccentricity);
end

numEllipsoide = ident_ellipsoide(b, Half_great_axis, E);

if numEllipsoide == 1
    b(1) = [];
    return
end

b = set(b, 'Ellipsoide.Type', numEllipsoide);


identProjectionCaraibes = get_value(in, 'Projection', 'CharIsByte');
if isempty(identProjectionCaraibes) % Modif JMA  le 22/04/2011 : Nouvelle version de Caraibes
    identProjectionCaraibes = get_valAtt(in, 'mbProjType');
end

Orientation = get_value(in, 'Orientation');
if isempty(Orientation) % Modif JMA  le 22/04/2011 : Nouvelle version de Caraibes
    Orientation = get_valAtt(in, 'Orientation'); %#ok<NASGU>
end

if isempty(identProjectionCaraibes)
    b = set(b, 'Projection.Type', 1);
    return
end

NordSud = 'NS';
switch identProjectionCaraibes
    case 1  % Mercator
        b = set(b, 'Projection.Type', 2);

        Mercator_Reference_latitude = double(get_value(in, 'Mercator_Reference_latitude')) * 1e-6;
        Mercator_Central_meridian   = double(get_value(in, 'Mercator_Central_meridian'))   * 1e-6;
        
        if isempty(Mercator_Reference_latitude) % Modif JMA  le 22/04/2011 : Nouvelle version de Caraibes
            val = get_valAtt(in, 'mbProjParameterValue');
            Mercator_Reference_latitude = val(2);
            Mercator_Central_meridian   = val(1);
            X0 = val(3);
            Y0 = val(4);
        else
            X0 = 0;
            Y0 = 0;
        end

        b = set(b, 'Projection.Mercator.X0',              X0);
        b = set(b, 'Projection.Mercator.Y0',              Y0);
        b = set(b, 'Projection.Mercator.lat_ech_cons',    Mercator_Reference_latitude);
        b = set(b, 'Projection.Mercator.long_merid_orig', Mercator_Central_meridian);

    case 2  % Lambert
% TODO : en cas de format Caraibes 3.6 : lire les bonnes valeurs en attributs globaux
        b = set(b, 'Projection.Type', 4);
        
        Lambert_type = get_value(in, 'Lambert_type', 'CharIsByte');
        Lambert_type = Lambert_type + 1; % TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO 
        
        b = set(b, 'Projection.Lambert.Type', Lambert_type); 
        
        Lambert_Central_meridian = get_value(in, 'Lambert_Central_meridian') * 1e-6;
        b = set(b, 'Projection.Lambert.long_merid_orig', Lambert_Central_meridian); 
        
        Lambert_factor = get_value(in, 'Lambert_factor');
        b = set(b, 'Projection.Lambert.scale_factor', Lambert_factor); 
        
        Lambert_X_origin = get_value(in, 'Lambert_X_origin');
        b = set(b, 'Projection.Lambert.X0', Lambert_X_origin); 
        
        Lambert_Y_origin = get_value(in, 'Lambert_Y_origin');
        b = set(b, 'Projection.Lambert.Y0', Lambert_Y_origin);         

    case 3  % UTM
        b = set(b, 'Projection.Type', 3);
        
        UTM_Central_meridian = get_value(in, 'UTM_Central_meridian') * 1e-6;
        UTM_Zone = floor(UTM_Central_meridian/6 + 31);
        
        UTM_Hemisphere = get_value(in, 'UTM_Hemisphere', 'CharIsByte');
        UTM_Hemisphere = UTM_Hemisphere + 1;

        UTM_X_origin   = get_value(in, 'UTM_X_origin');
        UTM_Y_origin   = get_value(in, 'UTM_Y_origin');
        

        if UTM_Hemisphere == 3 % Non normalisť
            b = set(b, 'Projection.UTM.Fuseau',     UTM_Zone);
            b = set(b, 'Projection.UTM.Hemisphere', 'N');
        else
            b = set(b, 'Projection.UTM.Fuseau',     UTM_Zone);
            b = set(b, 'Projection.UTM.Hemisphere', NordSud(UTM_Hemisphere));
        end
        b = set(b, 'Projection.UTM.X0', UTM_X_origin);
        b = set(b, 'Projection.UTM.Y0', UTM_Y_origin);

    case 4 % Stereographique polaire
% TODO : en cas de format Caraibes 3.6 : lire les bonnes valeurs en attributs globaux
        b = set(b, 'Projection.Type', 5);
        POLAR_Hemisphere = get_value(in, 'POLAR_Hemisphere', 'CharIsByte');

        POLAR_Central_meridian   = get_value(in, 'POLAR_Central_meridian');
        POLAR_Reference_latitude = get_value(in, 'POLAR_Hemisphere');

        b = set(b, 'Projection.Stereo.hemisphere',      POLAR_Hemisphere);
        b = set(b, 'Projection.Stereo.long_merid_horiz',POLAR_Central_meridian);
        b = set(b, 'Projection.Stereo.lat_ech_cons',    POLAR_Reference_latitude);

    case 5 % Cylindrique equidistante
        b = set(b, 'Projection.Type', 6);
        my_warndlg('TODO : Travail pas termine dans get_Caraibes_carto', 1);
end
