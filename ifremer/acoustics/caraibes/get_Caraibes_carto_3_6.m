% Lecture des coordonnees des layers de l'image
%
% Syntax
%   b = (nomFic)
%
% Input Arguments
%   a : objet de type cl_car_ima
%
% Output Argumentsget_Caraibes_carto
%   x : Abscisses (m)
%   y : Ordonnees (m)
%
% Examples
%   nomFic = getNomFicDatabase('EM12D_PRISMED_500m.mnt');
%   Carto = get_Caraibes_carto_3_6(nomFic)
%
% See also Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function b = get_Caraibes_carto_3_6(nomFic)

in = cl_netcdf('fileName', nomFic);

b = cl_carto;

identProjectionCaraibes = get_valAtt(in, 'mbProjType');

Half_great_axis = get_valAtt(in, 'mbEllipsoidA');
% E               = get_valAtt(in, 'mbEllipsoidInvF');
E2              = get_valAtt(in, 'mbEllipsoidE2');

numEllipsoide = ident_ellipsoide(b, Half_great_axis, sqrt(E2));
if numEllipsoide == 1
    b(1) = [];
    return
end

b = set(b, 'Ellipsoide.Type', numEllipsoide);

Orientation = get_valAtt(in, 'Orientation');
if Orientation ~= 0 % Ajouté par JMA le 02/11/2015 fichier Y:\private\ifremer\Arnaud\VOLT\VOLT-5m-tout-tideRTK_v2.mnt
    b = set(b, 'Referentiel.Azimut', Orientation); % TODO : bug ici cl_carto:set  :  "Referentiel.Azimut" is not a property of the class, please report to sonarscope@ifremer.fr.
end
    
if isempty(identProjectionCaraibes)
    b = set(b, 'Projection.Type', 1);
    return
end

NordSud = 'NS';
switch identProjectionCaraibes
    case 1  % Mercator
        b = set(b, 'Projection.Type', 2);

        % Récupération des paramètres de projection.
        pppp = get_valAtt(in, 'mbProjParameterValue');
        Mercator_Central_meridian   = pppp(1);
        Mercator_Reference_latitude = pppp(2);  
        Mercator_X0                 = pppp(3);  
        Mercator_Y0                 = pppp(4);  

        b = set(b, 'Projection.Mercator.X0',              Mercator_X0);
        b = set(b, 'Projection.Mercator.Y0',              Mercator_Y0);
        b = set(b, 'Projection.Mercator.lat_ech_cons',    Mercator_Reference_latitude);
        b = set(b, 'Projection.Mercator.long_merid_orig', Mercator_Central_meridian);

    case 2  % Lambert
        b = set(b, 'Projection.Type', 4);
        
        % Récupération des paramètres de projection.
        pppp = get_valAtt(in, 'mbProjParameterValue');
        Lambert_Central_meridian = pppp(1);
        Lambert_first_paral      = pppp(2);
        Lambert_second_paral     = pppp(3);
        Lambert_factor           = pppp(4);
        Lambert_X_origin         = pppp(5);
        Lambert_Y_origin         = pppp(6);
        Lambert_type             = pppp(7) + 1;
        
        b = set(b, 'Projection.Lambert.Type',     Lambert_type, ...
            'Projection.Lambert.long_merid_orig', Lambert_Central_meridian, ...
            'Projection.Lambert.lat_ech_cons',    Lambert_first_paral, ...
            'Projection.Lambert.first_paral',     Lambert_first_paral, ...
            'Projection.Lambert.second_paral',    Lambert_second_paral, ...
            'Projection.Lambert.scale_factor',    Lambert_factor, ...
            'Projection.Lambert.X0',              Lambert_X_origin, ...
            'Projection.Lambert.Y0',              Lambert_Y_origin); % lat_ech_cons : rajout JMA le 19/06/2017 
        
    case 3 % UTM
        b = set(b, 'Projection.Type', 3);

         % Récupération des paramètres de projection.
        pppp = get_valAtt(in, 'mbProjParameterValue');
%         UTM_Central_meridian = pppp(1);
        % Ancien calcul : UTM_Zone = floor(UTM_Central_meridian/6 + 31);
        
        UTM_Hemisphere  = pppp(2);
        UTM_Hemisphere  = UTM_Hemisphere + 1;
        UTM_Zone        = pppp(3);

        UTM_X_origin    = pppp(4);
        UTM_Y_origin    = pppp(5);

        if UTM_Hemisphere == 3 % Non normalisé
            b = set(b, 'Projection.UTM.Fuseau',     UTM_Zone);
            b = set(b, 'Projection.UTM.Hemisphere', 'N');
        else
            b = set(b, 'Projection.UTM.Fuseau',     UTM_Zone);
            b = set(b, 'Projection.UTM.Hemisphere', NordSud(UTM_Hemisphere));
        end
        b = set(b, 'Projection.UTM.X0', UTM_X_origin);
        b = set(b, 'Projection.UTM.Y0', UTM_Y_origin);

    case 4 % Stereographique polaire
        b = set(b, 'Projection.Type', 5);
        
        pppp = get_valAtt(in, 'mbProjParameterValue');
        MB_M0 = pppp(1);
        MB_L0 = pppp(2);
        % Hemi  = pppp(3);
        b = set(b, 'Projection.Stereo.long_merid_horiz', MB_M0);
        b = set(b, 'Projection.Stereo.lat_ech_cons', MB_L0);
        b = set(b, 'Projection.Stereo.hemisphere', 'N');

    case 5 % Cylindrique equidistante
        b = set(b, 'Projection.Type', 6);
        pppp = get_valAtt(in, 'mbProjParameterValue');
        MB_M0 = pppp(1);
        MB_L0 = pppp(2);
        b = set(b, 'Projection.CylEquiDist.long_merid_orig', MB_M0);
        b = set(b, 'Projection.CylEquiDist.lat_ech_cons', MB_L0);
end
