function [flag, ListeFlagsInvalides] = paramsFlagCaraibes

ListeFlags = [-4 -3 -2 -1 0 1 2 3 4];
strListeFlags = {'-4 : sondes invalides fix�es'
    '-3 : Sondes douteuses invalid�es'
    '-2 : Sondes valides invalid�es'
    '-1 : Sondes invalides'
    ' 0 : Sondes inexistantes'
    ' 1 : Sondes douteuses'
    ' 2 : Sondes valides'
    ' 3 : Sondes douteuses valid�es'
    ' 4 : Sondes invalides valid�es'};
[choix, flag] = my_listdlg(Lang('Flags � prendre en compte pour Depth','Flags to use for Depth'), ...
    strListeFlags, 'InitialValue', [7 8 9]);
if ~flag
    ListeFlagsInvalides = [];
    return
end
ListeFlagsInvalides = setdiff(ListeFlags, ListeFlags(choix));
