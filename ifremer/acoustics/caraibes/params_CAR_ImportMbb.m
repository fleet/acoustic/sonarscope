function [flag, NomFicMbb, NomFicNav, SonarIdent, listeFic, repImport] = params_CAR_ImportMbb(repImport)

listeFic   = [];
NomFicMbb  = [];
SonarIdent = [];

[flag, NomFicNav, repImport] = uiSelectFiles('ExtensionFiles', '.nvi', ...
    'RepDefaut', repImport, 'Entete', 'IFREMER - SonarScope - Select a navigation file (.nvi)');
if ~flag
    return
end
if length(NomFicNav) > 1
    my_warndlg('Only one file for the moment', 1);
end

% G:\REBENT\bathy_em1000_concarneau\200307003055.mbb
[flag, NomFicMbb, repImport] = uiSelectFiles('ExtensionFiles', '.mbb', ...
    'RepDefaut', repImport, 'Entete', 'IFREMER - SonarScope - Select a bathymetry file (.mbb)');
if ~flag
    return
end
if length(NomFicMbb) > 1
    my_warndlg('Only one file for the moment', 1);
end

[flag, SonarDescription] = selection(cl_sounder);
if ~flag
    return
end
SonarIdent = get(SonarDescription, 'Sonar.Ident');

[flag, listeFic, repImport] = uiSelectFiles('ExtensionFiles', '.ers', ...
    'RepDefaut', repImport, 'Entete', 'IFREMER - SonarScope - Select the sidescan sonar .ers files for which you want to create corresponing files.');
if ~flag
    return
end
