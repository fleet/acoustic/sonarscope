function [flag, ListeFicNvi, nomSignal, repImport] = params_CAR_PlotNviSignal(repImport)

nomSignal = [];

[flag, ListeFicNvi, lastDir] = uiSelectFiles('ExtensionFiles', '.nvi', 'RepDefaut', repImport);
if ~flag
    return
end
repImport = lastDir;

str1 = 'Liste des signaux disponibles dans les fichiers .nvi';
str2 = 'List of signals available in the .nvi files';
listeSignaux = {'Heading'; 'Speed'; 'Altitude'; 'Immersion'; 'Type'; 'Quality'; 'PFlag'; 'Nav Heading'; 'Nav Speed'};
[rep, flag] = my_listdlg(Lang(str1,str2), listeSignaux, 'InitialValue', 1, 'SelectionMode', 'Single');
if ~flag
    return
end
nomSignal = listeSignaux{rep};
