function plot_navigation_MBG(listFileNames)

I0 = cl_image;
Carto = cl_carto([]);
Fig = figure;
Fig = plot_navigation_Figure(Fig);
SonarIdent = 1;
ListeFlagsInvalides = [-4 -3 -2 -1 1];
N = length(listFileNames);
for k=1:length(listFileNames)
    fprintf('%3d/%3d - %s\n', k, N, listFileNames{k});
    try
        [a, Carto] = import_CaraibesMbg(I0, listFileNames{k}, 'Sonar.Ident', SonarIdent, ...
            'ListeFlagsInvalides', ListeFlagsInvalides, 'Carto', Carto, 'SonarIdent', 1, 'indLayerImport', 3);
        Fig = sonar_plot_nav(a, 'Fig', Fig);
        fprintf('%3d/%3d - %s : OK\n', k, N, listFileNames{k});
    catch
        fprintf('%3d/%3d - %s : Error\n', k, N, listFileNames{k});
    end
end
