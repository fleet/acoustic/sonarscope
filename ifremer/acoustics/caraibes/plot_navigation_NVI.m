function Fig = plot_navigation_NVI(ListeFicNvi, varargin)

[varargin, Fig]  = getPropertyValue(varargin, 'Fig',  []);
[varargin, Name] = getPropertyValue(varargin, 'Name', 'Navigation'); %#ok<ASGLU>

if ischar(ListeFicNvi)
    ListeFicNvi = {ListeFicNvi};
end

%% Lecture des fichiers

[flag, navRaw, nav, Type] = Nvi2ClNavigation(ListeFicNvi, Fig, Name); %#ok<ASGLU>
if ~flag
    return
end
