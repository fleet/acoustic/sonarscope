function flag = process_CAR_ImportMbb(listeFic, NomFicMbb, NomFicNav, SonarIdent)

%% Ouverture du fichier de bathymetrie

if iscell(NomFicMbb)
    NomFicMbb = NomFicMbb{1};
end
b = cl_car_bat_data(NomFicMbb);
mbCycleCounter = get(b, 'mbCycleCounter');
mbDate = get(b, 'mbDate');
mbTime = get(b, 'mbTime');
tMbb = cl_time('timeIfr', mbDate, mbTime);
clear mbDate mbTime

%% Ouverture du fichier de navigation

if iscell(NomFicNav)
    NomFicNav = NomFicNav{1};
end

%% Traitement des fichiers ErMapper

indLayerImport = [];
for i=1:length(listeFic)
    [flag, a] = cl_image.import_ermapper(listeFic{i});
    if ~flag
        continue
    end
    tErs = get(a, 'SonarTime');
    Carto = get(a, 'Carto');
 
% my_warndlg('ATTENTION ICI : Import G:\REBENT\PingRange\Concarneau.def', 1);
% Carto = import(cl_carto([]), 'G:\REBENT\PingRange\Concarneau.def')

    % -----------------------
    % Importation de la bathy

    [subErs, subMbb] = intersect(tErs, tMbb);
    if isempty(subErs)
        continue
    end
    
    sublDeb = max((subMbb(1)-100), 1);
    sublFin = min((subMbb(end)+100), mbCycleCounter);
    
    [c, CartoOut, indLayerImport] = view(b, 'subl', sublDeb:sublFin, 'Carto', Carto, ...
        'Sonar.Ident', SonarIdent, 'indLayerImport', indLayerImport); %#ok<ASGLU>
    if isempty(c)
        return
    end
    
    % ----------------------------
    % Importation de la navigation
    
    c = get(c, 'Images');
    c = sonar_import_nav(c, NomFicNav);
%     SonarScope([a c])

    % ---------------------------------------------------------------------------------
    % Extraction du nom de l'image en supprimant les attributs GeometryType et DataType

    ImageName = extract_ImageName(a);
    [~, ImageName] = fileparts(ImageName);
    nomDir = fileparts(listeFic{i});
    for k=1:length(c)
        c(k) = update_Name(c(k), 'Name', ImageName);
        nomFicErs = c(k).Name;
        nomFicOut = fullfile(nomDir, [nomFicErs '.ers']);
        flag = export_ermapper(c(k), nomFicOut);
        if ~flag
            return
        end
    end
end
