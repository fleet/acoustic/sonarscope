% Bruit propre
%
% Syntax
%   sN = selfNoise( Frequency, SelfNoise1kHz )
%
% Input Arguments
%   Frequency     : Frequence du signal (kHz)
%   SelfNoise1kHz : (dB)
%
% Output Arguments
%  sN         : Bruit de surface (dB)
%
% Examples 
%   sN = selfNoise( 13, 64 )
%
%   f = 1:100; sN = selfNoise( f, 64 );
%   plot(f, sN); grid on; xlabel('Frequence (kHz)'); ylabel('Self noise (dB)');
%
%   SelfNoise1kHz = 50:100; sN = selfNoise( 13, SelfNoise1kHz );
%   plot(SelfNoise1kHz, sN); grid on; xlabel('SelfNoise1kHz (dB)'); ylabel('Self noise (dB)');
%
% See also surfaceNoise Authors
% Authors : JMA + XL
% VERSION  : $Id: selfNoise.m,v 1.2 2002/06/20 12:09:18 augustin Exp $
%----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HISTORIQUE DEVELOPPEMENT
%   26/03/01 - JMA - creation
%   26/03/01 - JMA - Mise en ref.
% ----------------------------------------------------------------------------

function Noise = selfNoise( Frequency, SelfNoise1kHz )

if nargin == 0
    help selfNoise
    return
end

Noise = SelfNoise1kHz - 20 * log10(Frequency);