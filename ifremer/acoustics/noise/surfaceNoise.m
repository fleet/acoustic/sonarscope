% Bruit de surface
%
% Syntax
%   sN = surfaceNoise( Frequency, vitesseVent, ... )
%
% Input Arguments
%   Frequency   : Frequence du signal (kHz)
%   vitesseVent : Vitesse du vent (nds)
%
% Optional Input Arguments
%   H      : Profondeur du sonar (m)
%   alphaH : Attenuation inegree a la surface (dB/km)
%
% Output Arguments
%   sN          : Bruit de surface (dB)
%
% Examples 
%   sN = surfaceNoise( 13, 20 )
%
%   f = 1:100; sN = surfaceNoise( f, 20 );
%   plot(f, sN); grid on; xlabel('Frequence (kHz)'); ylabel('Surface noise (dB)');
%
%   seaState = 0:12; sN = surfaceNoise( 13, windSpeed(seaState) );
%   plot(seaState, sN); grid on; xlabel('Etat de mer (Beaubort)'); ylabel('Surface noise (dB)');
%
%   H = 0:0.1:6; sN = surfaceNoise( 13, 20, H, 1.2 );
%   plot(H, sN); grid on; xlabel('Profondeur des antennes (m)'); ylabel('Surface noise (dB)');
%
% See also windSpeed thermalNoise Authors
% Authors : JMA + XL
% VERSION    : $Id: surfaceNoise.m,v 1.3 2002/06/20 12:09:18 augustin Exp $
%----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HISTORIQUE DEVELOPPEMENT
%   26/03/01 - JMA - creation
%   26/03/01 - JMA - Mise en ref.
%   01/10/01 - JMA - Introduction de la profondeur des antennes.
% ----------------------------------------------------------------------------

function sN = surfaceNoise( Frequency, vitesseVent, varargin )

if nargin == 0
    help surfaceNoise
    return
end

sN = 95 + sqrt(21 * vitesseVent) - 17 * log10(Frequency * 1000);

if nargin == 4
    H = varargin{1};
    alpha = varargin{2};
    Hkm = H/ 1000;
    termeCorrectif = alpha * Hkm + 10 * log10(1 + (alpha / 8.686) * Hkm);
    sN = sN - termeCorrectif;
end

