% Bruit thermique
%
% Syntax
%   tN = thermalNoise( Frequency )
%
% Input Arguments
%   Frequency : Frequence du signal (kHz)
%
% Output Arguments
%  tN         : Bruit de surface (dB)
%
% Examples 
%   tN = thermalNoise( 13 )
%
%   f = 1:100; tN = thermalNoise( f );
%   plot(f, tN); grid on; xlabel('Frequence (kHz)'); ylabel('Thermal noise (dB)');
%
% See also surfaceNoise Authors
% Authors : JMA + XL
% VERSION  : $Id: thermalNoise.m,v 1.2 2002/06/20 12:09:18 augustin Exp $
%----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HISTORIQUE DEVELOPPEMENT
%   26/03/01 - JMA - creation
%   26/03/01 - JMA - Mise en ref.
% ----------------------------------------------------------------------------

function Noise = thermalNoise( Frequency )

if nargin == 0
    help thermalNoise
    return
end

Noise = -15 + 20 *log10(Frequency);
