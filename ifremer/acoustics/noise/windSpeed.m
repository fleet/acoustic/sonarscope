% Vitesse du vent en fonction de l'etat de mer
%
% Syntax
%   v = windSpeed( SeaState )
%
% Input Arguments
%   SeaState  : Etat de mer (Beaufort)
%
% Output Arguments
%   v : Vitesse du vent (nds)
%
% Examples 
%   v = windSpeed( 0:12 )
%   plot(0:12, v); grid on;
%
% See also SurfaceNoise Authors
% Authors : JMA + XL
% References : http://www.univ-lemans.fr/~hainry/articles/beaufort.html
% VERSION    : $Id: windSpeed.m,v 1.3 2002/06/20 12:09:18 augustin Exp $
%----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HISTORIQUE DEVELOPPEMENT
%   02/11/00 - JMA - creation
%   26/03/01 - JMA - Mise en ref.
% ----------------------------------------------------------------------------

function v = windSpeed( SeaState )

if nargin == 0
    help windSpeed
    return
end

v = 3 * SeaState .^ 1.5 / 1.852;