% Coefficient d'attenuation en fonction de f, Z, T, S
% Z,T et S constituent le profil de bathy-celerimetrie
%
% Cette fonction calcule l'attenuation locale ainsi que l'attenuation
% moyenne integree a partir de la source acoustique
%
% Cette fonction calcule egalement la droite de regression lineaire de
% l'attenuation integree adaptee a la bathymetrie locale
%
% Syntax
%   [Alpha, AlphaSum] = AttenuationGarrison(f, Z, T, S, ...)
%
% Input Arguments
%   f : frequence(s) du sondeur en kHz
%   Z : Profondeur (m)
%   T : Temperature (degres celsius)
%   S : Salinite (0/00)
%
% Name-Value Pair Arguments
%   Immersion      : Immersion de la source (m) (Z(1) par defaut)
%   LimBathyLocale : Limites de la bathymetie locale afin de calculer la droire de
%                    regression lineaire de l'attenuation integr�e
%
% Output Arguments
%   []            : Auto-plot activation
%   Alpha         : Coefficient d'attenuation pour chaque valeur de Z en db/km
%   AlphaSum      : Valeur moyenne du coefficient d'attenuation sur la tranche d'eau [Immersion,Z] en db/km
%   DroiteOrigine : Valeur a l'origine de la droite de regression lineaire
%   DroitePente   : Pente de la droite de regression lineaire
%
% Examples
%   [Alpha, AlphaSum] = AttenuationGarrison(13, 0:500:6000, 4, 35)
%
%     nomFic = getNomFicDatabase('BATHYCEL_North-East_Atlantic_Winter.csv');
%     [Z, S, T] = lecCsv(nomFic);
%
%   [Alpha, AlphaSum] = AttenuationGarrison(95, Z, T, S);
%   AttenuationGarrison(95, Z, T, S );
%   AttenuationGarrison(95, Z, T, S, 'LimBathyLocale', [-4000 -3000])
%   AttenuationGarrison(95, Z, T, S ,'Immersion', -3000)
%   AttenuationGarrison(95, Z, T, S, 'LimBathyLocale', [-4000 -3000], 'Immersion', -3000)
%
%   AttenuationGarrison([10:10:100], Z, T, S );
%
%   [Alpha, AlphaSum] = AttenuationGarrison([10:10:100], Z, T, S);
%   plot(Alpha,Z); grid on; zoom on;
%   hold on; plot(AlphaSum,Z, 'r');
%
% See also lecCsv FMgSO4 FBOH3 moyx0 Authors
% Authors : JMA + XL
%---------------------------------------------------------------------------------------

function [Alpha, AlphaSum, DroiteOrigine, DroitePente] = AttenuationGarrison(f, Z, T, S, varargin)

[varargin, Immersion]      = getPropertyValue(varargin, 'Immersion',      []);
[varargin, LimBathyLocale] = getPropertyValue(varargin, 'LimBathyLocale', [min(Z) 0]); %#ok<ASGLU>

if isempty(Immersion)
    Immersion = 0;
end

Z = -abs(Z(:));
T = T(:);
S = S(:);
sz = size(f);

f = double(f);
Z = double(Z);
T = double(T);
S = double(S);
Immersion = double(Immersion);
LimBathyLocale = double(LimBathyLocale);


%% Frequence de relaxation des molecules de sulfate de magnesium MgSO4 (kHz)

F2 = FMgSO4(T, S);

%% Frequence de relaxation des molecules d'acide borique B(OH)3 (kHz)

F1 = FBOH3(T, S);

%% C�l�rit� du son dans l'eau (m/s)

Celerite = 1412 + (3.21 * T) + (1.19 * S) - (1.67e-2 * Z);

%%

T_carre = T .* T;

Index = T <= 20;
A31 = (4.937e-4 - 2.59e-5 * T + 9.11e-7 * T_carre - 1.5e-8  * T_carre .* T) .* Index;
Index = T > 20;
A32 = (3.964e-4 - 1.146e-5 * T + 1.45e-7  * T_carre - 6.5e-10  * T_carre .* T) .* Index;
A3 = A31 + A32;

Z2 = Z .* Z;
A1 = (154 ./ Celerite);
P1 = 1;
A2 = 21.44 * S ./ Celerite .* (1. + 0.025 * T);
P2 = (1. - 1.37e-4 * (-Z) + 6.2e-9 * Z2);
P3 = 1. - 3.83e-5 .* (-Z) + 4.9e-10 .* Z2;

% Avant

Alpha = [];
AlphaSum = [];
nbp = length(Z);
for k=1:sz(2)
    fCarre = f(k) .* f(k);
    a = (A1 .* P1 .* (F1 .* fCarre) ./ (F1 .* F1 + fCarre)) + ...
        A2 .* P2 .* (F2 .* fCarre) ./ (fCarre + F2 .* F2) + ...
        A3 .* P3 .* fCarre;
    Alpha = [Alpha, a]; %#ok<AGROW>
    
    if nbp == 1
        b = a;
    else
        b = moyx0(Z, a, Immersion);
    end
    AlphaSum = [AlphaSum, b]; %#ok<AGROW>
end


% Modif 30/01/2011
% nbp = length(Z);
% for k=1:sz(2)
%     fCarre = f(k) .* f(k);
%     a = (A1 .* P1 .* (F1 .* fCarre) ./ (F1 .* F1 + fCarre)) + ...
%         A2 .* P2 .* (F2 .* fCarre) ./ (fCarre + F2 .* F2) + ...
%         A3 .* P3 .* fCarre;
%     Alpha = a;
%
%     if nbp == 1
%         b = a;
%     else
%         b = moyx0(Z, a, Immersion);
%     end
%     AlphaSum = b;
% end

N = size(AlphaSum,2);
DroiteOrigine = NaN(1, N, 'single');
DroitePente   = NaN(1, N, 'single');
% TODO : actuellement c'est faux il faut calculer polyfit � partir de l'immersion : sub = find(ZBathy > Immersion)
if (nargout > 2) || (nargout == 0)
    for k=1:N
        if size(AlphaSum,1) == 1
            DroiteOrigine(k) = AlphaSum(1,k);
            DroitePente(k) = 0;
        else
            ZBathy = linspace(LimBathyLocale(1), LimBathyLocale(2), 100);
            AlphaBathy = interp1(Z, AlphaSum(:,k), ZBathy, 'linear', 'extrap');
            p = polyfit(ZBathy, AlphaBathy, 1);
            DroiteOrigine(k) = p(2);
            DroitePente(k) = p(1);
        end
    end
end

%% Sortie des param�tres ou trac�s graphiques

if nargout == 0
    FigUtils.createSScFigure;
    if size(Alpha, 2) == 1
        PlotUtils.createSScPlot(Alpha, Z); grid on; zoom on; hold on;
        PlotUtils.createSScPlot(AlphaSum, Z, 'r'); grid on;
        xlabel('Attenuation (db/km)'); ylabel('Z (m)'); title('Alpha et AlphaSum');
        
        h = PlotUtils.createSScPlot(polyval(p, Z), Z, 'k--');
        str = sprintf('disp(''Origine : %f (dB/km) - Pente : %f (dB/km/m)'')', DroiteOrigine, DroitePente);
        set(h, 'ButtonDownFcn', str)
        legend({'Alpha'; 'AlphaSum'; 'Approximation'});
    else
        subplot(1, 2, 1);
        PlotUtils.createSScPlot(Alpha, Z); grid on; zoom on;
        xlabel('Attenuation (db/km)'); ylabel('Z (m)'); title('Attenuation');
        subplot(1, 2, 2);
        PlotUtils.createSScPlot(AlphaSum, Z); grid on;
        xlabel('Alpha (db/km)'); ylabel('Z (m)'); title('Attenuation integree de 0 a z');
    end
    %         disp(sprintf('Valeur a l''origine (Z=0) de la droite de regression lineaire : %s', mat2str(DroiteOrigine)))
    %         disp(sprintf('Pente de la droite : %s', mat2str(DroitePente)))
end
