% Frequence de relaxation des molecules d'acide borique (BOH3)
%
% Syntax
%   F = FBOH3(T, S)
%
% Input Arguments
%   T : Temperature (deg)
%   S : Salinite (0/000)
% 
% Output Arguments
%   F : Frequenve (kHz)
%
% Examples 
%   FBOH3(4, 38)
%   FBOH3([4:8], [36:0.5:38])
%
%   nomFic = getNomFicDatabase('BATHYCEL_North-East_Atlantic_Winter.csv')
%   [Z, S, T] = lecCsv(nomFic);
%   F = FBOH3(T, S);
%   plot(F, Z); grid on; 
%
% See also AttenuationGarrison FMgSO4 Authors
% Authors : JMA + XL
%--------------------------------------------------------------------------------

function F = FBOH3(T, S)

F = 2.8 * sqrt(S / 35) .* 10 .^ (4. - (1245 ./ (T + 273)));
