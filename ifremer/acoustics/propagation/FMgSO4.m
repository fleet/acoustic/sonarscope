% Frequence de relaxation des molecules de sulfate de magnesium (MgSO4)
%
% Syntax
%   F = FMgSO4(T, S)
%
% Input Arguments 
%   T : Temperature (deg)
%   S : Salinite (0/000)
% 
% Output Arguments
%   F : Frequenve (kHz)
%
% Examples 
%   FMgSO4( 4, 38 )
%   FMgSO4( [4:8], [36:0.5:38] )
%
%   nomFic = getNomFicDatabase('BATHYCEL_North-East_Atlantic_Winter.csv')
%   [Z, S, T] = lecCsv(nomFic);
%   F = FMgSO4(T, S);
%   plot(F, Z); grid on; 
%
% See also AttenuationGarrison FBOH3 Authors
% Authors : JMA + XL
%--------------------------------------------------------------------------------

function F = FMgSO4( T, S )

F = 8.17 * 10 .^ (8 - (1990 ./ (273 + T))) ./ (1 + 1.8e-3 * (S - 35));
