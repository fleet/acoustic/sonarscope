% Calcul de la perte de transmission Aller/Retour a differentes profondeurs
%
% Syntax
%   pertePropaAR = PerteTransmission(Depth, angleRayon, attenuationIntegree)
% 
% Input Arguments 
%   Depth               : Profondeurs (m)
%   angleRayon          : Angle de depart du rayon / verticale (deg)
%   attenuationIntegree : Attenuation integree (dB/km)
%
% Output Arguments
%   []           : Auto-plot activation
%   pertePropaAR : Perte de propagation
% 
% Examples
%   nomFic = getNomFicDatabase('BATHYCEL_North-East_Atlantic_Winter.csv');
%   [Z, S, T] = lecCsv(nomFic);
%   [Alpha, AlphaSum] = AttenuationGarrison(13, Z, T, S );
%   PerteTransmission(Z, 75, AlphaSum)
%   pertePropaAR = PerteTransmission(Z, 75, AlphaSum)
% 
%
% See also AttenuationGarrison cli_sound_speed Authors
% Authors : JMA
% VERSION  : $Id: PerteTransmission.m,v 1.5 2002/06/20 09:52:55 augustin Exp $
%-------------------------------------------------------------------------------

function varargout = PerteTransmission(Depth, angleRayon, attenuationIntegree)

Depth = abs(Depth(:));
attenuationIntegree = attenuationIntegree(:);
angleRayon = (angleRayon(:))';

if length(attenuationIntegree) == 1
        attenuationIntegree = ones(size(Depth)) * attenuationIntegree;
end

distanceOblique     = repmat(Depth, 1, length(angleRayon)) ./ repmat(cos(angleRayon * pi/180), length(Depth), 1);
pertePropaAR = -40. * log10(distanceOblique) - 2 * repmat(attenuationIntegree, 1, length(angleRayon)) .* (distanceOblique / 1000);

if nargout == 0
    figure
    subplot(1,2,1); plot(attenuationIntegree, -Depth); grid on; title('Attenuation integree');
    subplot(1,2,2); plot(pertePropaAR, -Depth); grid on; title('Perte de propagation AR');
else
	varargout{1} = pertePropaAR;
end
