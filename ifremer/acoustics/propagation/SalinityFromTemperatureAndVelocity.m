% Temperature from Salinity and Velocity
%
% Syntax
%   S = SalinityFromTemperatureAndVelocity(C, T, Z)
%
% Input Arguments
%   C : Velocity (m/s)
%   T : Temperature (deg).
%   Z : Depth (m)
%
% Output Arguments
%   S : Salinity (1/1000)
%
% Examples
%   S = 0:40;
%   Z = -5;
%   T = 13;
%   C = VelocityMedwinXL(T, S, Z);
%   figure; plot(S, C); grid on
%
%   S1 = SalinityFromTemperatureAndVelocity(C, T, Z)
%   figure; plot(S-S1); grid on; title('S-S1')
%   C1 = VelocityMedwinXL(T, S1, Z);
%   figure; plot(S, C); grid on; hold on; plot(S1, C1, 'xr'); title('C=f(S)')
%
% See also celeriteLovett TemperatureFromSalinityAndVelocity Authors
% References : Document Xavier
% Authors    : JMA
%-------------------------------------------------------------------------------

function S = SalinityFromTemperatureAndVelocity(C, T, Z)

S = 35 + (C - 1449.8 - 4.3406.*T +0.0387.*(T.^2) - 0.01682.*Z) ./ (1.3106 - 0.0088.*T);
