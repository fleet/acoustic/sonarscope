% Temperature from Salinity and Velocity
%
% Syntax
%   T = TemperatureFromSalinityAndVelocity(C, S, Z)
%
% Input Arguments
%   C : Velocity (m/s)
%   S : Salinity (1/1000)
%   Z : Depth (m)
%
% Output Arguments
%   T : Temperature (deg).
%
% Examples
%   S = 35;
%   Z = -5;
%   T = -10:0.1:56;
%   C = VelocityMedwinXL(T, S, Z);
%   figure; plot(T, C); grid on
%
%   T2 =  TemperatureFromSalinityAndVelocity(C, S, Z)
%   figure; plot(T-T2); grid on; title('T-T2')
%   C2 = VelocityMedwinXL(T2, S, Z);
%   figure; plot(T, C); grid on; hold on; plot(T2, C2, 'xr'); title('f(x) et f(f-1(f(x)))')
%
% See also celeriteLovett SalinityFromTemperatureAndVelocity Authors
% References : Document Xavier
% Authors    : JMA
%-------------------------------------------------------------------------------

function T = TemperatureFromSalinityAndVelocity(C, S, Z)

T = 23243/387 - (5000*((121*S^2)/1562500 + (1513319*S)/12500000 - (387*C)/2500 + (325467*abs(Z))/125000000 + 5973442279/25000000).^(1/2))/387 - (44*S)/387;
