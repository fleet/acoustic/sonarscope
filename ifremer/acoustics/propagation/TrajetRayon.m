% Calcul de trajets de rayons acoustiques
% avec un modele de celerite par couches horizontales de gradient constant
%
% Syntax
%   [AngleIntersec, Xintersec, TempsIntersec] = ...
%              TrajetRayon(ZCelerite, Celerite, AngleIncident)
%
% Input Arguments
%   ZCelerite     : Profondeur des couches (m) : Vecteur de valeurs
%                   decroissantes (par ex de 0 a Zmax<0 )
%   Celerite      : Profil(s) de celerite en m/s aux profondeurs ZCelerite
%   AngleIncident : Angle(s) incident(s) a la surface : Vecteur
%
% Name-Value Pair Arguments
%   'Immersion' : Immersion de l'antenne (m) (<0)
%   'Z'         : Profondeurs pour lesquelles on veut calculer X, TH et t
%
% Output Arguments
%   []            : Auto-plot activation
%   Xintersec     : Abscisses des intersections des rayons avec les couches.
%   AngleIntersec : Angles des rayons a la traversee des couches.
%   TempsIntersec : Temps de parcours jusqu'aux intersections.
%
% EXEMPLES :
%   nomFic = getNomFicDatabase('BATHYCEL_North-East_Atlantic_Winter.csv');
%   [Z, S, T] = lecCsv(nomFic);
%   C = celeriteChen( Z, T, S );
%   [TH, X, t] = TrajetRayon(Z, C, 75);
%       figure; plot(X, Z); grid on; hold on; zoom on;
%       plot(-tan(75 * pi / 180) * Z, Z, '--');
%
%   [TH, X, t, Zi] = TrajetRayon(Z, C, 75, 'Immersion', -200, 'Z', 0:-100:-5000);
%       figure; plot(X, Zi); grid on; hold on; zoom on;
%       plot(-tan(75 * pi / 180) * Z, Z, '--');
%
%   TrajetRayon(Z, C, 75, 'Immersion', -200, 'Z', 0:-100:-5000);
%   TrajetRayon(Z, C, 10:10:80, 'Immersion', -200, 'Z', 0:-100:-5000);
%
% See also rayTracing celeriteChen cli_sound_speed Authors
% Authors : XL + YHDR + JMA
%-------------------------------------------------------------------------------

function [AngleIntersec, Xintersec, TempsIntersec, Zintersec] = ...
    TrajetRayon(ZCelerite, Celerite, AngleIncident, varargin)

AngleIntersec = [];
Xintersec     = [];
TempsIntersec = [];
Zintersec     = [];

%% Test des valeurs d'entree. Mise en vecteur colonne ou ligne pour la suite.

if min(size(ZCelerite)) ~= 1
    fprintf('Pb ZCelerite doit �tre un vecteur');
    return
end
nCelerite = length(ZCelerite);
ZCelerite = reshape(ZCelerite,nCelerite,1);

if min(size(Celerite)) ~= 1
    fprintf('Pb Celerite doit �tre un vecteur');
    return
end
ntest = length(Celerite);
if ntest~=nCelerite
    fprintf('Pb ZCelerite et Celerite doivent avoir m�me taille');
    return
end
Celerite = reshape(Celerite,nCelerite,1);

if min(size(AngleIncident)) ~= 1
    fprintf('Pb AngleIncident doit �tre un vecteur');
    %return
end
nrayons = length(AngleIncident);
AngleIncident = reshape(AngleIncident,1,nrayons);

[varargin, Zintersec] = getPropertyValue(varargin, 'Z', ZCelerite);
if min(size(Zintersec)) ~= 1
    fprintf('Pb Z doit �tre un vecteur');
    return
end
nintersec = length(Zintersec);
Zintersec = reshape(Zintersec,nintersec,1);

[varargin, Immersion] = getPropertyValue(varargin, 'Immersion', Zintersec(1)); %#ok<ASGLU>

if Immersion > Zintersec(1)
    fprintf('Pb Zintersec ne couvre pas l''immersion');
    return
end

if Immersion ~= Zintersec(1)
    indImm = sum(Zintersec >= Immersion);
    Zintersec(indImm) = Immersion;
    Zintersec = Zintersec(indImm:end);
end
nintersec = length(Zintersec);

if (ZCelerite(1) < Zintersec(1)) || (ZCelerite(end) > Zintersec(end))
    fprintf('Pb ZCelerite n''englobe pas Zintersec');
    return
end

Celerite = interp1(ZCelerite, Celerite, Zintersec, 'linear');

Hcouche = diff(Zintersec);
if any(Hcouche >= 0)
    fprintf('Pb de monotonie avec ZCelerite ou Zintersec');
    return
end

%% Afin de ne pas rencontrer de probl�me de limite (changement de formule),
% on modifie les c�l�rit�s conduisant a des gradients nuls,
% le tout a la racine de la pr�cision machine.

DCelerite = diff(Celerite);
prec = sqrt(eps);
celermoy = mean(Celerite);
relprec = prec * celermoy;
IndDroite = (abs(DCelerite)< relprec);
if any(IndDroite)
    for icouche = 1:(nintersec-1)
        if IndDroite(icouche)
            Celerite(icouche+1) = Celerite(icouche) + relprec ;
        end
    end
    DCelerite = diff(Celerite);
end

degrad = pi / 180 ;
raddeg = 1 / degrad ;

%  (meme manip sur les angles que sur
% les gradients, pour eviter les passages a la limite)

AngleIncident = AngleIncident * degrad ;
relprec = 10 * prec ;
AngleIncident(abs(AngleIncident)<relprec) = relprec ;

%% Constante de rayon

Qcst = sin(AngleIncident)./Celerite(1);

% et d�j� un resultat

CeleriteXQcst = Celerite * Qcst;
sub = (CeleriteXQcst > 1);
CeleriteXQcst(sub) = NaN;
AngleIntersec = asin(CeleriteXQcst);

%% Temps de trajet dans les couches

Beta = log(-tan((AngleIntersec+pi)/2));
Insurge = (Hcouche./DCelerite) * ones(1,nrayons);
TempsIntersec = [zeros(1,nrayons); cumsum(Insurge.*diff(Beta))];

%% Abscisses relatives des rayons

Rcourb = (Celerite(1:nintersec-1) * ones(1,nrayons)) .* Insurge ./ sin(AngleIntersec(1:nintersec-1,:));
Deltax = Rcourb .* diff(cos(AngleIntersec));
Xintersec = [zeros(1,nrayons); cumsum(Deltax)];

AngleIntersec = AngleIntersec * raddeg;

%% Sortie des param�tre ou trac�s graphiques

if nargout == 0
    fig = figure;
    set(fig, 'Name', '{x,z} from {R,Angle}')
    subplot(1, 2, 1);
    plot(Celerite, Zintersec); grid on;
    xlabel('Celerite (m/s)'); ylabel('Z (m)'); title('Celerite');
    subplot(1, 2, 2);
    plot(Xintersec, Zintersec); grid on; hold on;
    
    ColorOrder = get(gca, 'ColorOrder');
    for k=1:length(AngleIncident)
        imod = 1 + mod(k-1, size(ColorOrder, 1));
        plot(-tan(AngleIncident(k)) * (Zintersec - Immersion), Zintersec, '--', 'Color', ColorOrder(imod, :));
    end
    xlabel('X (m)'); ylabel('Z (m)'); title('Trajet rayon');
    axis equal
end


