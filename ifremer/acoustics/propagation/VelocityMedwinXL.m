% Temperature from Salinity and Velocity
%
% Syntax
%   C = VelocityMedwinXL(T, S, Z)
%
% Input Arguments
%   T : Temperature (deg).
%   S : Salinity (1/1000)
%   Z : Depth (m)
%
% Output Arguments
%   C : Velocity (m/s)
%
% Examples
%   S = 35;
%   Z = -5;
%   T = -10:0.1:56;
%   C = VelocityMedwinXL(T, S, Z);
%   figure; plot(T, C); grid on
%
% See also celeriteLovett SalinityFromTemperatureAndVelocity TemperatureFromSalinityAndVelocity Authors
% References : Document Xavier
% Authors    : JMA
%-------------------------------------------------------------------------------

function C = VelocityMedwinXL(T, S, Z)

C = 1449.8 + 4.3406 .*T - 0.0387 .* (T.^2) + (1.3106 - 0.0088 .* T) .* (S - 35) + 0.01682 .* abs(Z);
