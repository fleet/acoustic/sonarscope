% All angles in rd
function [TransX, TransY, TransZ, EulerRoll] = XyzTranslation(X, Y, Z, Roll, Pitch, Yaw, zoff)

EulerRoll = asin(sin(Roll) ./ cos(Pitch));
cosEulerRoll = cos(EulerRoll);
sinEulerRoll = sin(EulerRoll);
cosPitch = cos(Pitch);
sinPitch = sin(Pitch);
cosYaw   = cos(Yaw);
sinYaw   = sin(Yaw);

Xx = X .* cosEulerRoll + zoff .* sinEulerRoll;
Yy = Y .* cosPitch  - X .* sinEulerRoll .* sinPitch + ...
     Z .* cosEulerRoll .* sinPitch;
TransX = cosYaw .* Xx + sinYaw .* Yy;
TransY = cosYaw .* Yy - sinYaw .* Xx;
TransZ = -Y .* sinPitch - X .* sin(Roll) + ...
          Z .* cosEulerRoll .* cosPitch;
