% Celerite par le modele de Lovett
%
% Syntax
%   C = celeriteLovett( Z, T, S )
%
% Input Arguments
%   Z  : Profondeur (m)
%   T  : Temperature (deg)
%   S  : salinite (0/000)
%
% Output Arguments
%   [] : Auto-plot activation
%   C  : Celerite (m/s)
%
% Examples 
%   C  = celeriteLovett( (0:500:6000), 4, 35 )
%
%   nomFic = getNomFicDatabase('BATHYCEL_North-East_Atlantic_Winter.csv');
%   [Z, S, T] = lecCsv(nomFic);
%   celeriteLovett( Z, T, S );
%
%   C = celeriteLovett( Z, T, S );
%     subplot(1,3,1); plot(T,Z); grid on; title('T')
%     subplot(1,3,2); plot(S,Z); grid on; title('S')
%     subplot(1,3,3); plot(C,Z); grid on; title('C')
%   CChen = celeriteChen( Z, T, S );
%     hold on; plot(CChen,Z, 'r'); hold off; legend({'Lovett';'Chen'})
%
% REMAKS : Il est vivement recommandé d'utiliser la formule de Chen&Millero : celeriteChen
%
% See also celeriteChen lecCsv lecLevitus cli_sound_speed Authors
% Authors : JMA + XL
%----------------------------------------------------------------------------

function varargout = celeriteLovett(Z, T, S)

P = abs(Z);
c0 = 1402.384;
ct1 = 5.01132;
ct2 = -5.513036e-2;
ct3 = 2.221008e-4;
cs1 = 1.332947;
cp1 = 1.605336e-2;
cp2 = 2.12448e-7;
ctsp1 = -1.266383e-2;
ctsp2 = 9.543664e-5;
ctsp3 = -1.052396e-8;
ctsp4 = 2.183988e-13;
ctsp5 = -2.253828e-13;
ctsp6 = 2.062107e-8;

c = c0 * ones(size(T));

ct = ct1 * T + ct2 * T.^2 + ct3 * T.^3;
cs = cs1 * S;
cp = cp1 * P + cp2 * P.^2;

ctsp =  ctsp1 * (T.*S)    + ctsp2 * (T.^2.*S) + ctsp3 * (T.*P.^2) + ...
        ctsp4 * (T.*P.^3) + ctsp5 * (S.*P.^3) + ctsp6 * (T.*S.^2.*P);

cel = c + ct + cs + cp + ctsp;

% -----------------------------------------
% Sortie des parametres ou traces graphiques

if nargout == 0
	figure;
	subplot(1,3,1); plot(T,Z); grid on; title('T (deg)')
	subplot(1,3,2); plot(S,Z); grid on; title('S (0/000)')
	subplot(1,3,3); plot(cel,Z, 'r'); grid on; title('C (m/s)')
else
        varargout{1} = cel;
end
