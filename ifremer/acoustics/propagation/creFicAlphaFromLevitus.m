% Creation d'un fichier d'attenuation pour Imagem
%
% Syntax
%   creFicAlphaFromLevitus(Latitude, Longitude, Mois, Frequency, nomFic)
%  
% Input Arguments
%   Latitude  : Latitude du point
%   Longitude : Longitude du point
%   Mois      : Mois de l'annee
%   Frequency : Frequence du sondeur
%   nomFic    : Nom du fichier
%
% Examples
%   nomFic = fullfile(my_tempdir, 'toto.dat')
%   creFicAlphaFromLevitus(-6, 7, 11, 13, nomFic)
%
% See also AttenuationGarrison cli_sound_speed Authors
% Authors : JMA
% VERSION  : $Id: creFicAlphaFromLevitus.m,v 1.2 2002/06/20 09:52:55 augustin Exp $
%---------------------------------------------------------------------------------------

function creFicAlphaFromLevitus(Latitude, Longitude, Mois, Frequency, nomFic)

a = cli_sound_speed('Incoming', 2, ...
    'LevitusAveragingType', 3, ...
    'LevitusMonth', Mois, ...
    'LevitusGridSize', 1, ...
    'Latitude', Latitude, 'Longitude', Longitude, ...
    'Frequency', Frequency);

Z = get(a, 'Depth');
T = get(a, 'Temperature');
S = get(a, 'Salinity');
C = get(a, 'SoundSpeed');
Al = get(a, 'LocalAbsorption');
Am = get(a, 'AveragedAbsorption');

% Z = 0:max(-Z);
ZOut = 0:7000;
AmOut = interp1(-Z, Am, ZOut,'linear', 'extrap');

% figure;
% plot(Am, Z, '*'); grid on;
% hold on; plot(AmOut, -ZOut, 'r*'); grid on;

alpha = [ZOut' AmOut'];
save(nomFic, 'alpha', '-ascii')