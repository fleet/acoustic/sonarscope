function flag = createDefaultSalinityFile(repImport)

p = ClParametre('Name', 'Salinity', 'Unit', '0/00', 'MinValue', 0,  'MaxValue', 45, 'Value', 0);
a = StyledParametreDialog('params', p, 'Title', 'Set the salinity value');
a.openDialog;
flag = a.okPressedOut;
if ~flag
    return
end
Salinity = a.getParamsValue;

nomFic = fullfile(repImport, 'DefaultSalinity.txt');
[flag, nomFic] = my_uiputfile('*.txt', 'Save a "DefaultSalinity.txt" where the .all or .s7k files are', nomFic);
if ~flag
    return
end

fid = fopen(nomFic, 'w+t');
if fid == -1
    return
end
fprintf(fid, 'Default salinity (0/000) : %f\n', Salinity);
fclose(fid);

flag = 1;
