function [TimeStartOfUse, TimeProfileMeasurement] = get_TimeProfilCelerite(ProfilCelerite)

if isfield(ProfilCelerite, 'TimeStartOfUse') % Anciens formats
    TimeStartOfUse = ProfilCelerite.TimeStartOfUse;
    TimeProfileMeasurement = ProfilCelerite.TimeProfileMeasurement;    
else % Nouveaux formats
    N = length(ProfilCelerite);
    TimeStartOfUsetimeMat  = NaN(1,N);
    TimeProfileMeasurement = NaN(1,N);
    for k=1:N
        TimeStartOfUsetimeMat(k)  = ProfilCelerite(k).TimeStartOfUse.timeMat;
        TimeProfileMeasurement(k) = ProfilCelerite(k).TimeProfileMeasurement.timeMat;
    end
    TimeStartOfUse         = cl_time('timeMat', TimeStartOfUsetimeMat);
    TimeProfileMeasurement = cl_time('timeMat', TimeProfileMeasurement);
end
