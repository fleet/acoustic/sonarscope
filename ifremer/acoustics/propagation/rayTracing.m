% Calcul de la bathy a partir des temps de parcours et des angles
% avec un mod�le de c�l�rite par couches horizontales de gradient constant
%
% Syntax
%   [Xbathy, Zbathy, AngleIntersec] = rayTracing(ZCelerite, Celerite, BeamAngles, TempsSimple)
%
% Input Arguments
%   ZCelerite     : Profondeur des couches (m) : Vecteur de valeurs
%                   decroissantes (par ex de 0 a Zmax<0 )
%   Celerite      : Profil(s) de celerite en m/s aux profondeurs ZCelerite
%   BeamAngles : Angle(s) incident(s) a la surface : Vecteur
%   TempsSimple   : Temps aller-simple surface fond : Vecteur
%
% Output Arguments
%   []            : Auto-plot activation
%   XBathy        : Abscisses des intersections des rayons avec le fond.
%   ZBathy        : Ordonnees des intersections des rayons avec le fond.
%   AngleIntersec : Angles des rayons sur le fond.
%
% EXEMPLES :
%   nomFic = getNomFicDatabase('BATHYCEL_North-East_Atlantic_Winter.csv');
%   [Z, S, T] = lecCsv(nomFic);
%   C = celeriteChen( Z, T, S );
%   teta = -75:75;
%   [X, TH, t] = TrajetRayon(Z, C, teta);
%   ts = t(end, :);
%
%   [Xbathy, Zbathy, AngleIntersec] = rayTracing(Z, C, teta, ts);
%       figure; plot(Xbathy, Zbathy, '*k')
%
%   C2 = C; C2(1:2) = C2(1:2) + 1;
%   [Xbathy, Zbathy, AngleIntersec] = rayTracing(Z, C2, teta, ts);
%       hold on; plot(Xbathy, Zbathy, '*r')
%
%   [Xbathy, Zbathy, AngleIntersec] = rayTracing(Z, C, teta + 0.05 * (rand(size(teta))-0.5), ts);
%       hold on; plot(Xbathy, Zbathy, '*r')
%
%   [Xbathy, Zbathy, AngleIntersec] = rayTracing(Z, C, teta, ts, 'Immersion', -5);
%       hold on; plot(Xbathy, Zbathy, '*r')
%
% See also cl_car_bat_data/calculer_bathy TrajetRayon Authors
% Authors : YHDR
%-------------------------------------------------------------------------------

function [Xbathy, Zbathy, AngleIntersec] = rayTracing(ZCelerite, Celerite, BeamAngles, TempsSimple, varargin)
 
% Valid� le 06/02/2016
% Attention : BeamAngles en radian

[varargin, Immersion] = getPropertyValue(varargin, 'Immersion', ZCelerite(1));
[varargin, Fig] = getPropertyValue(varargin, 'Fig', []); %#ok<ASGLU>

if Immersion ~= ZCelerite(1)
    indImm = sum(ZCelerite >= Immersion);
    if indImm > 0
        ImmCelerite = interp1(ZCelerite, Celerite, Immersion, 'linear');
        ZCelerite(indImm) = Immersion ;
        ZCelerite = ZCelerite(indImm:end);
        Celerite(indImm) = ImmCelerite;
        Celerite = Celerite(indImm:end);
    end
end

ZCelerite   = ZCelerite(:);
Celerite    = Celerite(:);
nceler      = length(ZCelerite);
ncouches    = nceler - 1 ;
hcouches    = -(diff(ZCelerite));
BeamAngles  = BeamAngles(:)';
TempsSimple = TempsSimple(:)';
nangles     = length(BeamAngles);

% -----------------------------------------------------------------------
% Afin de ne pas rencontrer de probl�me de limite (changement de formule),
% on modifie les c�l�rit�s conduisant a des gradients nuls,
% le tout a la racine de la pr�cision machine.

DCelerite = diff(Celerite);
prec      = sqrt(eps);
celermoy  = mean(Celerite);
relprec   = prec * celermoy ;
IndDroite = (abs(DCelerite)< relprec);
if any(IndDroite)
    for icouche = 1:ncouches
        if IndDroite(icouche)
            Celerite(icouche+1) = Celerite(icouche) + relprec ;
        end
    end
    DCelerite = diff(Celerite);
end

% (meme manip sur les angles que sur les gradients, pour �viter les passages � la limite)

relprec = 10 * prec ;
BeamAngles(abs(BeamAngles) < relprec) = relprec;

%% constante de rayon

qcst = sin(BeamAngles) / Celerite(1);
teta = asin(Celerite * qcst);

betat = log(tan(teta/2));
gradcel = DCelerite ./ hcouches;

%% Temps de parcours des ncouches-1 premi�res couches

tau = diff(betat) ./ ( gradcel * ones(1,nangles));
Ttau = [zeros(1,nangles) ; cumsum(tau,1)];

%% Identification de la couche ou s'arr�te le rayon

iclast = sum((ones(ncouches+1,1) * TempsSimple) > Ttau);
indexlast = iclast + (0:(ncouches+1):(nangles-1)*(ncouches+1));
indexlast(indexlast == 0) = 1; % TODO  : rajout� le 17/02/2016 mais sans savoir si �a peut avoir des cons�quences f�cheuses

%% Temps de parcours dans la derni�re couche travers�e

deltat = TempsSimple - Ttau(indexlast);

%% Calcul du gradient dans la derni�re couche, extrapol� vers -infini

gradcel = [gradcel ; gradcel(end)];

sub = find(iclast == 0);
if ~isempty(sub)
    iclast(sub) = 1;
end

gn = gradcel(iclast)';

%% Angles d'incidence au sol

AngleIntersec = 2*(atan(tan(teta(indexlast)/2) .* exp(gn .* deltat)));

%% C�l�rit� au sol

cf = sin(AngleIntersec) ./ qcst;

%% Bathy au sol

dzpied = (cf - Celerite(iclast)') ./ gn;
Zbathy = ZCelerite(iclast)' - dzpied;
Zbathy = Zbathy + Immersion;

%% Abscisse au sol

rcourb = - ((Celerite./gradcel) * ones(1,nangles)) ./ sin(teta);
xrelatif =  rcourb(1:end-1,:).*diff(cos(teta));
xtotal = [zeros(1,nangles) ; cumsum(xrelatif,1) ];

Xbathy = xtotal(indexlast) - ...
    (cos(AngleIntersec)-cos(teta(indexlast))) .* Celerite(iclast)' ./ gn ./ sin(teta(indexlast));

AngleIntersec = AngleIntersec * (180/pi) ;

%% Sortie des param�tres ou trac�s graphiques

if nargout == 0
    if isempty(Fig)
        figure;
    else
        figure(Fig)
        hold off
    end
    PlotUtils.createSScPlot(Xbathy, Zbathy, '*'); grid on; hold on;
    
    %     ColorOrder = get(gca, 'ColorOrder');
    %     for iang=1:nangles
    %         imod = 1 + mod(iang-1, size(ColorOrder, 1));
    %         xray = xtotal(1:iclast(iang),iang);
    %         zray = ZCelerite(1:iclast(iang));
    %         subDepassement = find(zray > Zbathy(iang));
    %         xray = [xray(subDepassement) ; Xbathy(iang)];
    %         zray = [zray(subDepassement) ; Zbathy(iang)];
    %         plot(xray, zray, '--', 'Color', ColorOrder(imod, :));
    %     end
    xlabel('X (m)'); ylabel('Z (m)'); title('Trajet rayon');
    axis equal
end
