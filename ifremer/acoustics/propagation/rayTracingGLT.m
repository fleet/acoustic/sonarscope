% Calcul de la bathy a partir des temps de parcours et des angles
% avec un modele de celerite par couches horizontales de gradient constant
%
% Syntax
%   [Xbathy, Zbathy, AngleIntersec] = rayTracingGLT(ZCelerite, Celerite, AngleIncident, TempsSimple)
%
% Input Arguments
%   ZCelerite     : Profondeur des couches (m) : Vecteur de valeurs
%                   decroissantes (par ex de 0 a Zmax<0 )
%   Celerite      : Profil(s) de celerite en m/s aux profondeurs ZCelerite
%   AngleIncident : Angle(s) incident(s) a la surface : Vecteur
%   TempsSimple   : Temps aller-simple surface fond : Vecteur
%
% Output Arguments
%   []            : Auto-plot activation
%   XBathy        : Abscisses des intersections des rayons avec le fond.
%   ZBathy        : Ordonnees des intersections des rayons avec le fond.
%   AngleIntersec : Angles des rayons sur le fond.
%
% EXEMPLES :
%   nomFic = getNomFicDatabase('BATHYCEL_North-East_Atlantic_Winter.csv');
%   [Z, S, T] = lecCsv(nomFic);
%   C = celeriteChen( Z, T, S );
%   teta = -75:75;
%   [X, TH, t] = TrajetRayon(Z, C, teta);
%   ts = t(end, :);
%
%   [Xbathy, Zbathy, AngleIntersec] = rayTracingGLT(Z, C, teta, ts);
%       figure; plot(Xbathy, Zbathy, '*k')
%
%   C2 = C; C2(1:2) = C2(1:2) + 1;
%   [Xbathy, Zbathy, AngleIntersec] = rayTracingGLT(Z, C2, teta, ts);
%       hold on; plot(Xbathy, Zbathy, '*r')
%
%   [Xbathy, Zbathy, AngleIntersec] = rayTracingGLT(Z, C, teta + 0.05 * (rand(size(teta))-0.5), ts);
%       hold on; plot(Xbathy, Zbathy, '*r')
%
%   [Xbathy, Zbathy, AngleIntersec] = rayTracingGLT(Z, C, teta, ts, 'Immersion', -5);
%       hold on; plot(Xbathy, Zbathy, '*r')
%
% See also cl_car_bat_data/calculer_bathy TrajetRayon Authors
% Authors : YHDR
%-------------------------------------------------------------------------------

function [XBathy, YBathy, ZBathy, AngleIntersec] = rayTracingGLT(ZCelerite, Celerite, AngleIncident, TempsSimple,  PitchTx, PitchRx, RollRx, shipSettings, varargin)

[varargin, Fig] = getPropertyValue(varargin, 'Fig', []);
[varargin, dPitch] = getPropertyValue(varargin, 'Fig', 0);
[varargin, Immersion] = getPropertyValue(varargin, 'Immersion', []); %#ok<ASGLU>

%%

degrad = pi/180 ;
AngleIncident = AngleIncident * degrad;

Celerite      = double(Celerite(:));       % pour la pr�cisiton des calculs
ZCelerite     = double(ZCelerite(:));      % idem
AngleIncident = double(AngleIncident);  % idem
TempsSimple   = double(TempsSimple);    % idem
RollRx        = double(RollRx);
PitchRx       = double(PitchRx);
PitchTx       = double(PitchTx);

%% Tests

ZCelerite     = ZCelerite(:);           % force le stockage en colonne
Celerite      = Celerite(:);            % force le stockage en colonne

if ZCelerite(end) > ZCelerite(1)
    ZCelerite = flipud(ZCelerite);
    Celerite = flipud(Celerite);
end

%% Suppression des couches inutiles dans le profil de c�l�rit�

% ZMAx = -max(Celerite) * max(TempsSimple .* cos(AngleIncident));
ZMAx    = -max(Celerite) * max(TempsSimple);
if isempty(Immersion)
    sub = find(ZCelerite < ZMAx);
    sub = sub(2:end);
else
    sub = find(ZCelerite < (ZMAx+Immersion));
end

if length(sub) == (length(ZCelerite) - 1)
    sub = sub(2:end);
end

ZCelerite(sub) = [];
Celerite(sub)  = [];

% if length(ZCelerite) == 1
%     D = Celerite * TempsSimple;
%     XBathy =  sin(AngleIncident) .* D;
%     ZBathy = -cos(AngleIncident) .* D;
%     YBathy = zeros(size(XBathy));
%     return
% end

%% Calcul de la position relative du pied du faisceau (par rapport �
% l'antenne), et de l'Azimuth du pied de faisceau

switch dPitch
    case 0
        %% cas 1 : sondeur non compens� en tangage
        Ux = sin(AngleIncident);                   % + tribord, ici pas de correction � faire de roulis (correction d�j� faites dans AngleIncident)
        Uy = sind(PitchTx) .* cos(AngleIncident);  % + avant, m�me chose ici;
        Uz = cosd(PitchTx) .* cos(AngleIncident);  % + vers le haut
        %Angle d'incidence par rapport � la verticale
        teta = acos(Uz);
        % Azimuth du pied du faisceau
        cosAz = Uy./sqrt(Ux.^2+Uy.^2);
        sinAz = Ux./sqrt(Ux.^2+Uy.^2);
        %         Az = asind(sinAz);                             % Azimuth du pied
        
        
    otherwise
        %% cas 2 : sondeur compens� en tangage
        ay = sind(PitchRx).*sind(RollRx).*sind(dPitch).*cosd(PitchTx) - ...
            cosd(PitchRx).*sind(RollRx).*sind(dPitch).*sind(PitchTx) + sin(AngleIncident); % n�gatif bab�rd
        ax = -cosd(RollRx).*cosd(dPitch);
        az = -(sind(PitchRx).*sind(RollRx).*cosd(dPitch).*sind(PitchTx) + ...
            cosd(PitchRx).*sind(RollRx).*cosd(dPitch).*cosd(PitchTx));
        
        %         sinb = (az.*sqrt(ax.^2 + az.^2 - ay.^2) - ax.*ay)  ./ (ax.^2 + az.^2); % avant -
        cosb = (-ax.*sqrt(ax.^2 + az.^2 - ay.^2) - ay.*az) ./ (ax.^2 + az.^2);
        
        %         Uy = sind(dPitch).*cosd(PitchTx) + cosd(dPitch).*sind(PitchTx).*cosb;
        %         Ux = -cosd(dPitch).*sinb;
        %Uz = -sind(dPitch).*cosd(PitchTx) + cosd(dPitch).*sind(PitchTx).*sinb;
        Uz = -sind(dPitch).*sind(PitchTx) + cosd(dPitch).*cosd(PitchTx).*cosb;
        
        % Angle d'incidence par rapport � la verticale
        teta = acos(Uz);
        
        % Azimuth du pied du faisceau
        %         Az = atan2(Ux,Uy)*(180/pi);
end

%% R�cup�ration de l'immersion des transducteurs

if isempty(Immersion)
    Immersion = ZCelerite(1);
end

if Immersion ~= ZCelerite(1)
    indImm = sum(ZCelerite >= Immersion);
    if indImm > 0
        % Interpolation du profil de c�l�rit� � l'immersion des transducteurs
        % et conservation du profil depuis cette immersion jusqu'au fond
        if length(ZCelerite) == 1
            ImmCelerite =  Celerite;
        else
            ImmCelerite = interp1(ZCelerite, Celerite, Immersion, 'linear');
        end
        ZCelerite(indImm)   = Immersion ;
        Celerite(indImm)    = ImmCelerite;
        ZCelerite           = ZCelerite(indImm:end);
        Celerite            = Celerite(indImm:end);
    end
end

nceler        = length(ZCelerite);      % nbre de c�l�rit� diff�rentes
nLayer        = nceler - 1 ;            % nbre de couches
hLayer        = -(diff(ZCelerite));     % �paisseur des couches
%AngleIncident = AngleIncident(:)';      % force le stockage en ligne
AngleIncident = teta(:)'; %AngleIncident(:)';      % c'est ce qu'on
%devrait faire, mais ne marche pas
TempsSimple   = TempsSimple(:)';        % force le stockage en ligne
nRayon        = length(AngleIncident);  % Nbre de rayon

%% Afin de ne pas rencontrer de probl�me de limite (changement de formule),
% on modifie les c�l�rit�s conduisant a des gradients nuls,
% le tout a la racine de la pr�cision machine
% => jamais de propagation en ligne droite

DCelerite   = diff(Celerite);
preci       = sqrt(eps);
celermoy    = mean(Celerite);
realPreci   = preci * celermoy ;           % Pr�cision r�elle
IndDroite   = (abs(DCelerite)< realPreci);
if any(IndDroite)
    for icouche = 1:nLayer
        if IndDroite(icouche)
            Celerite(icouche+1) = Celerite(icouche) + realPreci ; % On ajoute la pr�cision machine
        end
    end
    DCelerite = diff(Celerite);     % MAJ de la c�l�rit� avec diff non nulles
end

raddeg = 1 / degrad ;

%  (meme manip sur les angles que sur
% les gradients, pour �viter les passages � la limite)

realPreci = 10 * preci ;
AngleIncident(abs(AngleIncident) < realPreci) = realPreci ;

%% Constante de rayon

Cste    = sin(AngleIncident)/Celerite(1);   % sin(teta0)/c0, constante quelque soit la couche (Snell-descartes)
gradCel = DCelerite ./ hLayer;              % gradient au sein de chaque couche
teta    = asin(Celerite * Cste);            % = teta(i), angle de sortie de couche
t       = log(tan(teta/2));                 % = t(i), temps de sortie de couche

%% Temps de parcours des nLayer-1 premi�res couches

dt     = diff(t) ./ ( gradCel * ones(1,nRayon));   % = t(i+1)-t(i) tps de parcours au sein de chaque couche
tTotal = [zeros(1,nRayon) ; cumsum(dt,1)];         % tps de parcours cumul�

%% Identification de la couche o� s'arr�te le rayon

iLastLayer      = sum( tTotal < (ones(nLayer+1,1)*TempsSimple) ) ;       % indice de l'avant derni�re couche pour chaque rayon
indexLastLayer  = iLastLayer+(0:(nLayer+1):(nRayon-1)*(nLayer+1)) ;    % indexation simple (colonne) plut�t que double (optimisation)

%% Temps de parcours dans la derni�re couche travers�e

dt_last = TempsSimple - tTotal(indexLastLayer);

%% Calcul du gradient dans la derni�re couche, extrapole vers -infini

gradCel = [gradCel ; gradCel(end)];
sub = find(iLastLayer == 0);
if ~isempty(sub)
    iLastLayer(sub) = 1;
end
gradCel_last = gradCel(iLastLayer)'; % gradient entre l'avant derni�re couche et le fond

%% Angles d'incidence au sol

AngleIntersec = 2*(atan(tan(teta(indexLastLayer)/2).*exp(gradCel_last.*dt_last)));

%% C�l�rite au sol

cele_last = sin(AngleIntersec) ./ Cste; % car c(end)/sin(teta(end)) = cste

%% Bathy au sol

dz_last = (cele_last - Celerite(iLastLayer)') ./ gradCel_last;      % �paisseur entre avant derni�re couche et fond
Zbathy  = ZCelerite(iLastLayer)' - dz_last;                         % on ajoute cette �paisseur
% Zbathy  = Zbathy - Immersion;                                       % correction de l'immersion des transducteurs
Zbathy  = Zbathy + Immersion;                                       % correction de l'immersion des transducteurs

%% Abscisses au sol

rCourb   = - ((Celerite./gradCel) * ones(1,nRayon)) ./ sin(teta);     % rayon de courbure dans chaque couche pour chaque faisceau
xrelatif = rCourb(1:end-1,:) .* diff(cos(teta));                      % distance lat�rale en sortie de chaque couche
xtotal   = [zeros(1,nRayon) ; cumsum(xrelatif,1) ];                   % distance lat�rale cumul�e

Xr = xtotal(indexLastLayer) - ...
    (cos(AngleIntersec)-cos(teta(indexLastLayer))) .* Celerite(iLastLayer)' ./ gradCel_last ./ sin(teta(indexLastLayer)); % on ajoute la distance lat�rale entre l'avant derni�re couche et le fond

X = abs(Xr);

AngleIntersec = AngleIntersec * raddeg ;

%% Distances Transversale et Longitudinale parcourues par le fasceau

ZBathy = Zbathy;
XBathy = X.*sinAz;
YBathy = X.*cosAz;
% XBathy = -X.*sind(Az);
% YBathy = X.*cosd(Az);

%{
%Az du pied du faisceau (� l'essai)
%� l'essai, correction de d�pointage en pitch (correction = -pitch) + bras de levier
dy = shipSettings.Arrays.Transmit.Y;
% Ux = ZBathy .* tan(AngleIncident); % hypoth�se de correction du Pitch
% Uy = dy.*cosd(Pitch) + ...         % correction bras de levier
%      sind(Pitch) .* (ZBathy - sqrt( X.^2 + (ZBathy./cosd(Pitch)).^2 ) ) ; % correction du Pitch
%
% Az = atan2(Ux,Uy)*(180/pi);    % Azimuth du pied du faisceau en degr�e

% XBathy = Ux;
if  ~isempty(dy)
%     YBathy = YBathy + dy.*cosd(PitchTx);
YBathy = YBathy + dy(1) .* cosd(PitchTx); % TODO Sondeur Dual
end
%}

%% Sortie des parametres ou trac�s graphiques

if nargout == 0
    if isempty(Fig)
        figure;
    else
        figure(Fig)
        hold off
    end
    
    %% Vue de derri�re
    %     subplot(121)
    PlotUtils.createSScPlot(XBathy, ZBathy, '*'); grid on; hold on;
    xlabel('Across distance (m)'); ylabel('Z (m)'); title(Lang('Trajet rayon','Ray path'));
    
    % bateau
    [~, imin] = min(XBathy);
    [~, imax] = max(XBathy);
    %     PlotUtils.createSScPlot([0 min(XBathy)], [0 ZBathy(imin)], 'r');
    %     PlotUtils.createSScPlot([0 max(XBathy)], [0 ZBathy(imax)], 'g');
    % TODO : Ici faire la distinction entre Immersion et distance s�parant
    % l'antenne du point de r�f�rence (pb rencontra sur AUV�
    if Immersion < -10 % Si ecart de plus de 10m alors on suppose que c'est l'AUV. Je sais c'est abominablement mal fait TOD TODO TODO
        Immersion = 0;
    end
    PlotUtils.createSScPlot([0 min(XBathy)], [Immersion ZBathy(imin)], 'r');
    PlotUtils.createSScPlot([0 max(XBathy)], [Immersion ZBathy(imax)], 'g');
    
    scale = abs(min(ZBathy) / 3); % 30% de la hauteur d'eau
    pos = [  0 cosd(30) 0.5      0.5          -0.5         -0.5     -cosd(30) 0;...  %X
        -1 sind(30) sind(30) sind(30)+0.5 sind(30)+0.5 sind(30) sind(30) -1];     %Y
    roll = mean(RollRx);
    matRot = [cosd(roll)  sind(roll); ...
        -sind(roll) cosd(roll)];
    newPos = matRot*pos*scale;
    patch(newPos(1,:), newPos(2,:),0, 'FaceColor','k')
    axis equal; axis tight
    %     Ylim = get(gca, 'Ylim');
    
    %     %% Vue de tribord
    %     subplot(122)
    %     PlotUtils.createSScPlot(YBathy, ZBathy, '*'); grid on; hold on;
    %     xlabel('Along distance (m)'); ylabel('Z (m)');
    %
    %     % bateau
    %     [~, imin] = min(YBathy);
    %     [~, imax] = max(YBathy);
    %
    %     PlotUtils.createSScPlot([0 min(YBathy)], [Immersion ZBathy(imin)], 'r');
    %     PlotUtils.createSScPlot([0 max(YBathy)], [Immersion ZBathy(imax)], 'g');
    %
    %     scale = abs(max(YBathy)); % 30% de la hauteur d'eau
    %     pos = [ -1 -1 1 1.5 0.2 0.2 -0.2 -0.2 ;...  %X
    %             0.7 -0.3 -0.3 1 0.7 1.5 1.5 0.7 ];     %Y
    %
    %     pitch = mean(PitchTx);
    %     matRot = [cosd(pitch)  sind(pitch); ...
    %               -sind(pitch) cosd(pitch)];
    %     newPos = matRot * pos * scale;
    %     patch(newPos(1,:), newPos(2,:),0, 'FaceColor','k')
    %     set(gca, 'Ylim', Ylim);
    %     axis equal
end
