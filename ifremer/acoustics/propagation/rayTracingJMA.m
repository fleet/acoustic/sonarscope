% TODO : not used : keep it just in case until a unique function is validated for all sounders
function [XBathy, YBathy, ZBathy, AngleIntersec, Az] = rayTracingJMA(ZCelerite, Celerite, BeamAngles, TempsSimple, PitchTx, RollTx, RollRx, PitchRx, TiltAngles, varargin)

% Attention : BeamAngles en radian

[varargin, Fig]       = getPropertyValue(varargin, 'Fig',       []);
[varargin, Immersion] = getPropertyValue(varargin, 'Immersion', []); %#ok<ASGLU>

%% For�age des calculs en double pr�cision

ZCelerite   = double(ZCelerite(:));  % idem
Celerite    = double(Celerite(:));   % pour la pr�cisiton des calculs
BeamAngles  = double(BeamAngles); % idem
TempsSimple = double(TempsSimple);   % idem
PitchTx     = double(PitchTx);
RollTx      = double(RollTx);

%% Tests

ZCelerite = ZCelerite(:); % force le stockage en colonne
Celerite  = Celerite(:);  % force le stockage en colonne

if ZCelerite(end) > ZCelerite(1)
    ZCelerite = flipud(ZCelerite);
    Celerite  = flipud(Celerite);
end

%% Calcul de la position relative du pied du faisceau (par rapport � l'antenne), et de l'Azimuth du pied de faisceau

%% cas 1 : sondeur non compens� en tangage

pppp = 2;
if pppp == 1
    Ux = sin(BeamAngles);                   % + tribord, ici pas de correction � faire de roulis (correction d�j� faites dans BeamAngles)
    Uy = sind(PitchTx) .* cos(BeamAngles);  % + avant, m�me chose ici;
    
    Uz = cosd(PitchTx) .* cos(BeamAngles);  % + vers le haut
    teta = acos(Uz);                        % Angle d'incidence par rapport � la verticale
    teta = abs(teta) .* sign(BeamAngles);
    
    % Azimuth du pied du faisceau
    cosAz = Uy ./ sqrt(Ux.^2 + Uy.^2);
    sinAz = Ux ./ sqrt(Ux.^2 + Uy.^2);
    Az    = asind(sinAz); % Azimuth du pied
else
    %% cas 2 : sondeur compens� en tangage
    ay = sind(PitchRx) .* sind(RollRx) .* sind(TiltAngles) .* cosd(PitchTx) - ...
        cosd(PitchRx) .* sind(RollRx) .* sind(TiltAngles) .* sind(PitchTx) + sin(BeamAngles); % n�gatif bab�rd
    ax = -cosd(RollRx) .* cosd(TiltAngles);
    az = -(sind(PitchRx) .* sind(RollRx) .* cosd(TiltAngles) .* sind(PitchTx) + ...
        cosd(PitchRx) .* sind(RollRx) .* cosd(TiltAngles) .* cosd(PitchTx));
    
    sinb = (az.*sqrt(ax.^2 + az.^2 - ay.^2) - ax.*ay)  ./ (ax.^2 + az.^2); % avant -
    cosb = (-ax .* sqrt(ax .^ 2 + az .^ 2 - ay .^ 2) - ay .* az) ./ (ax .^ 2 + az .^ 2);
    
    Uy = sind(TiltAngles) .* cosd(PitchTx) + cosd(TiltAngles) .* sind(PitchTx) .* cosb;
    Ux = -cosd(TiltAngles) .* sinb;
    %Uz = -sind(TiltAngles).*cosd(PitchTx) + cosd(TiltAngles).*sind(PitchTx).*sinb;
    Uz = -sind(TiltAngles) .* sind(PitchTx) + cosd(TiltAngles) .* cosd(PitchTx).*cosb;
    
    % Angle d'incidence par rapport � la verticale
    teta = acos(Uz);
    
    % Azimuth du pied du faisceau
    Az = atan2(Ux,Uy);
    sinAz = sin(Az);
    cosAz = cos(Az);
    Az = Az * (180/pi);
end

%% Trac� de rayon

[X, ZBathy, AngleIntersec] = rayTracing(ZCelerite, Celerite, teta, TempsSimple, 'Immersion', Immersion);

%% Distances Transversale et Longitudinale parcourues par le fasceau

% ZBathy = ZBathy - Immersion; % correction de l'immersion des transducteurs
XBathy = X .* abs(sinAz);
YBathy = X .* cosAz;

%% Sortie des param�tres ou trac�s graphiques

if nargout == 0
    if isempty(Fig)
        figure;
    else
        figure(Fig)
        hold off
    end
    
    %% Vue de derri�re
    %     subplot(121)
    PlotUtils.createSScPlot(XBathy, ZBathy, '*'); grid on; hold on;
    xlabel('Across distance (m)'); ylabel('Z (m)'); title(Lang('Trajet rayon','Ray path'));
    
    % bateau
    [~, imin] = min(XBathy);
    [~, imax] = max(XBathy);
    %     PlotUtils.createSScPlot([0 min(XBathy)], [0 ZBathy(imin)], 'r');
    %     PlotUtils.createSScPlot([0 max(XBathy)], [0 ZBathy(imax)], 'g');
    % TODO : Ici faire la distinction entre Immersion et distance s�parant
    
    % l'antenne du point de r�f�rence (pb rencontra sur AUV�
    if Immersion < -10 % Si �cart de plus de 10m alors on suppose que c'est l'AUV. Je sais c'est abominablement mal fait TOD TODO TODO
        Immersion = 0;
    end
    PlotUtils.createSScPlot([0 min(XBathy)], [Immersion ZBathy(imin)], 'r');
    PlotUtils.createSScPlot([0 max(XBathy)], [Immersion ZBathy(imax)], 'g');
    
    scale = abs(min(ZBathy) / 3); % 30% de la hauteur d'eau
    pos = [  0 cosd(30) 0.5      0.5          -0.5         -0.5     -cosd(30) 0;...  %X
        -1 sind(30) sind(30) sind(30)+0.5 sind(30)+0.5 sind(30) sind(30) -1];     %Y
    matRot = [cosd(RollTx)  sind(RollTx); ...
        -sind(RollTx) cosd(RollTx)];
    newPos = matRot*pos*scale;
    patch(newPos(1,:), newPos(2,:),0, 'FaceColor','k')
    axis equal; axis tight
    
    %     figure(9853); plot(XBathy, YBathy, '*'); grid on
    
    %     Ylim = get(gca, 'Ylim');
    
    %     %% Vue de tribord
    %     subplot(122)
    %     PlotUtils.createSScPlot(YBathy, ZBathy, '*'); grid on; hold on;
    %     xlabel('Along distance (m)'); ylabel('Z (m)');
    %
    %     % bateau
    %     [~, imin] = min(YBathy);
    %     [~, imax] = max(YBathy);
    %
    %     PlotUtils.createSScPlot([0 min(YBathy)], [Immersion ZBathy(imin)], 'r');
    %     PlotUtils.createSScPlot([0 max(YBathy)], [Immersion ZBathy(imax)], 'g');
    %
    %     scale = abs(max(YBathy)); % 30% de la hauteur d'eau
    %     pos = [ -1 -1 1 1.5 0.2 0.2 -0.2 -0.2 ;...  %X
    %             0.7 -0.3 -0.3 1 0.7 1.5 1.5 0.7 ];     %Y
    %
    %     pitch = mean(PitchTx);
    %     matRot = [cosd(pitch)  sind(pitch); ...
    %               -sind(pitch) cosd(pitch)];
    %     newPos = matRot * pos * scale;
    %     patch(newPos(1,:), newPos(2,:),0, 'FaceColor','k')
    %     set(gca, 'Ylim', Ylim);
    %     axis equal
end
