% TODO : not used : keep it just in case until a unique function is validated for all sounders
function [RetracedRelWorldX, RetracedRelWorldY, RetracedRelWorldZ, AngleIntersec, RetracedAzimuth] ...
    = rayTracingPitchCompensationEM710(...
    ZCelerite, Celerite, BeamAngles, OneWayTravelTime, ProjectorAngle, ...
    RollTx, PitchTx, RollRx, PitchRx, ...
    RollMounting, PitchMounting, YawMounting, varargin)

% Attention : Tous les angles en radian

[varargin, Fig]             = getPropertyValue(varargin, 'Fig',       []);
[varargin, TrancducerDepth] = getPropertyValue(varargin, 'Immersion', 0);
[varargin, isXYZ]           = getPropertyValue(varargin, 'isXYZ',     0); %#ok<ASGLU>

%% For�age des calculs en double pr�cision

ZCelerite   = double(ZCelerite(:));  % idem
Celerite    = double(Celerite(:));   % pour la pr�cisiton des calculs
BeamAngles  = double(BeamAngles); % idem
PitchTx     = double(PitchTx);
RollTx      = double(RollTx);
RollRx      = double(RollRx);
PitchRx     = double(PitchRx);
ProjectorAngle = double(ProjectorAngle);
OneWayTravelTime = double(OneWayTravelTime);   % idem

%% Tests

%{
ZCelerite = ZCelerite(:); % force le stockage en colonne
Celerite  = Celerite(:);  % force le stockage en colonne

if ZCelerite(end) > ZCelerite(1)
ZCelerite = flipud(ZCelerite);
Celerite  = flipud(Celerite);
end
%}

z = OneWayTravelTime .* 1500 .* cos(BeamAngles);
sub = find(abs(ZCelerite) > (max(z) + abs(TrancducerDepth)));
if length(sub) >= 1
    ZCelerite(sub(2:end)) = [];
    Celerite(sub(2:end))  = [];
end
% figure; plot(Celerite, ZCelerite); grid on;

% ProjectorAngle = PitchRx + 0; % TODO
% ProjectorAngle = 0; %PitchTx + 0; % TODO
SpeedOfSound = mean(Celerite);

%% Compute SonarXyz

% cosProjectorAngle = cos(-ProjectorAngle - PitchRx);
% sinProjectorAngle = sin(-ProjectorAngle - PitchRx);

cosProjectorAngle =  cos(ProjectorAngle);
sinProjectorAngle = -sin(ProjectorAngle);

Range = OneWayTravelTime .* SpeedOfSound;
SonarX = Range .* cosProjectorAngle .* sin(BeamAngles);

if isXYZ % TODO : bizarre !!!
    %     SonarX = SonarX - SonarX .* abs(sind(1.52)); % Test JMA le 22/02/2016 pour EM122 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
end

SonarY = Range .* sinProjectorAngle;
SonarZ = Range .* cosProjectorAngle .* cos(BeamAngles);

%% Compute VesselXyz

[VesselX, VesselY, VesselZ] = XyzTranslation(SonarX, SonarY, SonarZ, RollMounting, PitchMounting, YawMounting);

%% Compute RelWorldXyz

HeadingTx = 0;
[RelWorldX, RelWorldY, RelWorldZ] = XyzTranslation(VesselX, VesselY, VesselZ, RollRx, PitchTx, HeadingTx);

%% Compute IncidentAngle and OneWayTravelTime

Range = sqrt(RelWorldX .* RelWorldX + RelWorldY .* RelWorldY + RelWorldZ .* RelWorldZ);
IncidentAngle = acos(-RelWorldZ ./ Range); % WorldZ !!!!!!!!!!!!!!!
OneWayTravelTime = Range ./ SpeedOfSound;

%% Trac� de rayon

[HorizontalDistance, Depth, AngleIntersec] = rayTracing(ZCelerite, Celerite, IncidentAngle, OneWayTravelTime, 'Immersion', TrancducerDepth);

%% Distances Transversales et Longitudinales parcourues par le fasceau

R = sqrt(HorizontalDistance .* HorizontalDistance);
sin_hor = RelWorldX ./ R;
cos_hor = RelWorldY ./ R;
RetracedRelWorldX = HorizontalDistance .* sin_hor;
RetracedRelWorldY = HorizontalDistance .* cos_hor;
RetracedRelWorldZ = Depth;
RetracedAzimuth = 90 - atan2d(RetracedRelWorldY, RetracedRelWorldX);

%% Sortie des param�tres ou trac�s graphiques

if nargout == 0
    if isempty(Fig)
        figure;
    else
        figure(Fig)
        hold off
    end
    
    %% Vue de derri�re
    %     subplot(121)
    PlotUtils.createSScPlot(RetracedRelWorldX, RetracedRelWorldZ, '*'); grid on; hold on;
    xlabel('Across distance (m)'); ylabel('Z (m)'); title(Lang('Trajet rayon','Ray path'));
    
    %% Plot of an arrow to represent the ship with roll
    
    [~, imin] = min(RetracedRelWorldX);
    [~, imax] = max(RetracedRelWorldX);
    %     PlotUtils.createSScPlot([0 min(RetracedRelWorldX)], [0 RetracedRelWorldZ(imin)], 'r');
    %     PlotUtils.createSScPlot([0 max(RetracedRelWorldX)], [0 RetracedRelWorldZ(imax)], 'g');
    % TODO : Ici faire la distinction entre TrancducerDepth et distance s�parant
    % l'antenne du point de r�f�rence (pb rencontr� sur AUV�
    if TrancducerDepth < -10 % Si �cart de plus de 10m alors on suppose que c'est l'AUV. Je sais c'est abominablement mal fait TOD TODO TODO
        TrancducerDepth = 0;
    end
    PlotUtils.createSScPlot([0 min(RetracedRelWorldX)], [TrancducerDepth RetracedRelWorldZ(imin)], 'r');
    PlotUtils.createSScPlot([0 max(RetracedRelWorldX)], [TrancducerDepth RetracedRelWorldZ(imax)], 'g');
    
    scale = abs(min(RetracedRelWorldZ) / 3); % 30% de la hauteur d'eau
    pos = [  0 cosd(30) 0.5      0.5          -0.5         -0.5     -cosd(30) 0;...  %HorizontalDistance
        -1 sind(30) sind(30) sind(30)+0.5 sind(30)+0.5 sind(30) sind(30) -1];     %Y
    matRot = [cosd(RollTx)  sind(RollTx); ...
        -sind(RollTx) cosd(RollTx)];
    newPos = matRot*pos*scale;
    patch(newPos(1,:), newPos(2,:),0, 'FaceColor','k')
    axis equal; axis tight
end
