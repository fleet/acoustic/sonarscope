% TODO : not used : keep it just in case until a unique function is validated for all sounders
function [RetracedRelWorldX, RetracedRelWorldY, RetracedRelWorldZ, AngleIntersec, Az] = rayTracingPitchCompensationOff(...
    ZCelerite, Celerite, BeamAngles, OneWayTravelTime, ...
    RollTx, PitchTx, RollRx, PitchRx, RollMounting, ...
    PitchMounting, YawMounting, varargin)

% Attention : BeamAngles en radian

[varargin, Fig]             = getPropertyValue(varargin, 'Fig',       []);
[varargin, TrancducerDepth] = getPropertyValue(varargin, 'Immersion', 0); %#ok<ASGLU>

%% For�age des calculs en double pr�cision

ZCelerite   = double(ZCelerite(:));  % idem
Celerite    = double(Celerite(:));   % pour la pr�cisiton des calculs
BeamAngles  = double(BeamAngles); % idem
PitchTx     = double(PitchTx) * (pi/180);
RollTx      = double(RollTx)  * (pi/180);
RollRx      = double(RollRx)  * (pi/180);
OneWayTravelTime = double(OneWayTravelTime);   % idem

%% Tests

ZCelerite = ZCelerite(:); % force le stockage en colonne
Celerite  = Celerite(:);  % force le stockage en colonne

if ZCelerite(end) > ZCelerite(1)
    ZCelerite = flipud(ZCelerite);
    Celerite  = flipud(Celerite);
end

z = OneWayTravelTime .* 1500 .* cos(BeamAngles);
sub = find(abs(ZCelerite) > (max(z) + abs(TrancducerDepth)));
if length(sub) >= 1
    ZCelerite(sub(2:end)) = [];
    Celerite(sub(2:end))  = [];
end
% figure; plot(Celerite, ZCelerite); grid on;

%{
%% Calcul de la position relative du pied du faisceau (par rapport � l'antenne), et de l'Azimuth du pied de faisceau

x = sin(BeamAngles);                   % + tribord, ici pas de correction � faire de roulis (correction d�j� faites dans BeamAngles)
y = sind(PitchTx) .* cos(BeamAngles);  % + avant, m�me chose ici;

Uz = cosd(PitchTx) .* cos(BeamAngles);  % + vers le haut
IncidentAngle = acos(Uz);                        % Angle d'incidence par rapport � la verticale
% IncidentAngle = abs(IncidentAngle) .* sign(BeamAngles);

R = sqrt(x .* x + y .* y);
WorldX = x; % TODO !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
WorldY = y; % TODO !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
sin_hor = WorldX ./ R;
cos_hor = WorldY ./ R;

% Azimuth du pied du faisceau ?
Az = asind(sin_hor); % Azimuth du pied
%{
FigUtils.createSScFigure; plot(x, y, '*'); grid on;
FigUtils.createSScFigure;
subplot(3,1,1); PlotUtils.createSScPlot(cos_hor, '*'); grid on; %axis equal
subplot(3,1,2); PlotUtils.createSScPlot(sin_hor, '*'); grid on; %axis equal
subplot(3,1,3); PlotUtils.createSScPlot(Az, '*'); grid on; %axis equal
%}
Az = mod(360 + asind(sin_hor), 360); % Pour revenir � la convention de KM
%}


% ProjectorAngle = PitchRx + 0; % TODO
ProjectorAngle = 0; %PitchTx + 0; % TODO
SpeedOfSound   = mean(Celerite);

% BeamAngles = BeamAngles;        % Si DataType = cl_image.indDataType('RxAngleEarth'); demand� comme image d'angle
BeamAngles = BeamAngles - RollTx; % Si DataType = cl_image.indDataType('TxAngle');      demand� comme image d'angle

%% Compute SonarXyz

cosProjectorAngle = cos(ProjectorAngle);
sinProjectorAngle = sin(ProjectorAngle);
Range = OneWayTravelTime .* SpeedOfSound;
SonarX = Range .* cosProjectorAngle .* sin(BeamAngles);
SonarY = Range .* sinProjectorAngle;
SonarZ = Range .* cosProjectorAngle .* cos(BeamAngles);

%% Compute VesselXyz

[VesselX, VesselY, VesselZ] = XyzTranslation(SonarX, SonarY, SonarZ, RollMounting, PitchMounting, YawMounting);

%% Compute RelWorldXyz

HeadingTx = 0;
[RelWorldX, RelWorldY, RelWorldZ] = XyzTranslation(VesselX, VesselY, VesselZ, RollRx, PitchTx, HeadingTx);

%% Compute IncidentAngle and OneWayTravelTime

Range = sqrt(RelWorldX .* RelWorldX + RelWorldY .* RelWorldY + RelWorldZ .* RelWorldZ);
IncidentAngle = acos(-RelWorldZ ./ Range); % WorldZ !!!!!!!!!!!!!!!
OneWayTravelTime = Range ./ SpeedOfSound;

%% Trac� de rayon

[HorizontalDistance, Depth, AngleIntersec] = rayTracing(ZCelerite, Celerite, IncidentAngle, OneWayTravelTime, 'Immersion', TrancducerDepth);

%% Distances Transversales et Longitudinales parcourues par le faisceau

sin_hor = RelWorldX ./ HorizontalDistance; % WorldX !!!!!!!!!!!!!!!!!
cos_hor = RelWorldY ./ HorizontalDistance; % WorldY !!!!!!!!!!!!!!!!!
RetracedRelWorldX = HorizontalDistance .* sin_hor;
RetracedRelWorldY = HorizontalDistance .* cos_hor;
RetracedRelWorldZ = Depth;

% Modif JMA
% AzRelWorld = atan2(RelWorldY, RelWorldX);
% RetracedRelWorldX = HorizontalDistance .* cos(AzRelWorld);
% RetracedRelWorldY = HorizontalDistance .* sin(AzRelWorld);

%% Sortie des param�tres ou trac�s graphiques

if nargout == 0
    if isempty(Fig)
        figure;
    else
        figure(Fig)
        hold off
    end
    
    %% Vue de derri�re
    %     subplot(121)
    PlotUtils.createSScPlot(RetracedRelWorldX, RetracedRelWorldZ, '*'); grid on; hold on;
    xlabel('Across distance (m)'); ylabel('Z (m)'); title(Lang('Trajet rayon','Ray path'));
    
    %% Plot of an arrow to represent the ship with roll
    
    [~, imin] = min(RetracedRelWorldX);
    [~, imax] = max(RetracedRelWorldX);
    %     PlotUtils.createSScPlot([0 min(RetracedRelWorldX)], [0 RetracedRelWorldZ(imin)], 'r');
    %     PlotUtils.createSScPlot([0 max(RetracedRelWorldX)], [0 RetracedRelWorldZ(imax)], 'g');
    % TODO : Ici faire la distinction entre TrancducerDepth et distance s�parant
    % l'antenne du point de r�f�rence (pb rencontr� sur AUV�
    if TrancducerDepth < -10 % Si �cart de plus de 10m alors on suppose que c'est l'AUV. Je sais c'est abominablement mal fait TOD TODO TODO
        TrancducerDepth = 0;
    end
    PlotUtils.createSScPlot([0 min(RetracedRelWorldX)], [TrancducerDepth RetracedRelWorldZ(imin)], 'r');
    PlotUtils.createSScPlot([0 max(RetracedRelWorldX)], [TrancducerDepth RetracedRelWorldZ(imax)], 'g');
    
    scale = abs(min(RetracedRelWorldZ) / 3); % 30% de la hauteur d'eau
    pos = [  0 cosd(30) 0.5      0.5          -0.5         -0.5     -cosd(30) 0;...  %HorizontalDistance
        -1 sind(30) sind(30) sind(30)+0.5 sind(30)+0.5 sind(30) sind(30) -1];     %Y
    matRot = [cosd(RollTx)  sind(RollTx); ...
        -sind(RollTx) cosd(RollTx)];
    newPos = matRot*pos*scale;
    patch(newPos(1,:), newPos(2,:),0, 'FaceColor','k')
    axis equal; axis tight
end
