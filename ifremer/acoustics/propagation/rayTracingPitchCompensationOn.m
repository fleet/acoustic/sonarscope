% TODO : not used : keep it just in case until a unique function is validated for all sounders
function [RetracedRelWorldX, RetracedRelWorldY, RetracedRelWorldZ, AngleIntersec, Az] = rayTracingPitchCompensationOn(ZCelerite, Celerite, BeamAngles, TempsSimple, RollTx, PitchTx, RollRx, PitchRx, TiltAngles, varargin)

% Attention : BeamAngles en radian

[varargin, Fig]       = getPropertyValue(varargin, 'Fig',       []);
[varargin, Immersion] = getPropertyValue(varargin, 'Immersion', []); %#ok<ASGLU>

%% For�age des calculs en double pr�cision

ZCelerite   = double(ZCelerite(:));  % idem
Celerite    = double(Celerite(:));   % pour la pr�cisiton des calculs
BeamAngles  = double(BeamAngles);    % idem
TempsSimple = double(TempsSimple);   % idem
PitchTx     = double(PitchTx);
RollTx      = double(RollTx);
TiltAngles  = double(TiltAngles);
Immersion   = double(Immersion);

%% Tests

ZCelerite = ZCelerite(:); % force le stockage en colonne
Celerite  = Celerite(:);  % force le stockage en colonne

if ZCelerite(end) > ZCelerite(1)
    ZCelerite = flipud(ZCelerite);
    Celerite  = flipud(Celerite);
end

%% Calcul de la position relative du pied du faisceau (par rapport � l'antenne), et de l'Azimuth du pied de faisceau

%% cas 1 : sondeur non compens� en tangage

isPitchCompensated = 2;
if isPitchCompensated == 1
    ax = sin(BeamAngles);                   % + tribord, ici pas de correction � faire de roulis (correction d�j� faites dans BeamAngles)
    ay = sind(PitchTx) .* cos(BeamAngles);  % + avant, m�me chose ici;
    
    Uz = cosd(PitchTx) .* cos(BeamAngles);  % + vers le haut
    teta = acos(Uz);                        % Angle d'incidence par rapport � la verticale
    teta = abs(teta) .* sign(BeamAngles);
    
    % Azimuth du pied du faisceau
    r = sqrt(ax.^2 + ay.^2);
    sin_hor = ax ./ r;
    cos_hor = ay ./ r;
    Az      = asind(sin_hor); % Azimuth du pied
    
else
    
    %% cas 2 : sondeur compens� en tangage
    % TODO : tout m'a l'air embrouill� lorsque j'essaye de retrouver les equations du cas 1 en prenant
    % les sin=0 et cos=1 pour PitchTx, TiltAngles et RollRx
    
    ax = -cosd(RollRx) .* cosd(TiltAngles);
    ay = sind(PitchRx) .* sind(RollRx) .* sind(TiltAngles) .* cosd(PitchTx) - ...
        cosd(PitchRx) .* sind(RollRx) .* sind(TiltAngles) .* sind(PitchTx) + sin(BeamAngles); % n�gatif b�bord
    az = -(sind(PitchRx) .* sind(RollRx) .* cosd(TiltAngles) .* sind(PitchTx) + ...
        cosd(PitchRx) .* sind(RollRx) .* cosd(TiltAngles) .* cosd(PitchTx));
    
    sinb = ( az .* sqrt(ax .^ 2 + az .^ 2 - ay .^ 2) - ax .* ay) ./ (ax .^ 2 + az .^ 2); % avant -
    cosb = (-ax .* sqrt(ax .^ 2 + az .^ 2 - ay .^ 2) - ay .* az) ./ (ax .^ 2 + az .^ 2);
    
    RelWorldY =  sind(TiltAngles) .* cosd(PitchTx) + cosd(TiltAngles) .* sind(PitchTx) .* cosb;
    RelWorldX = -cosd(TiltAngles) .* sinb;
    
    % Angle d'incidence par rapport � la verticale
    Uz = -sind(TiltAngles) .* sind(PitchTx) + cosd(TiltAngles) .* cosd(PitchTx) .* cosb;
    teta = acos(Uz);
    
    % Azimuth du pied du faisceau
    Az = atan2(RelWorldX, RelWorldY);
    sin_hor = sin(Az);
    cos_hor = cos(Az);
    Az      = Az * (180/pi);
end

%% Trac� de rayon

[HorizontalDistance, Depth, AngleIntersec] = rayTracing(ZCelerite, Celerite, abs(BeamAngles), TempsSimple, 'Immersion', Immersion);

%% Distances Transversale et Longitudinale parcourues par le fasceau

RetracedRelWorldX = HorizontalDistance .* abs(sin_hor) .* sign(BeamAngles);
RetracedRelWorldY = HorizontalDistance .* cos_hor;
RetracedRelWorldZ = Depth;

%% Sortie des param�tres ou trac�s graphiques

if nargout == 0
    if isempty(Fig)
        figure;
    else
        figure(Fig)
        hold off
    end
    
    %% Vue de derri�re
    %     subplot(121)
    PlotUtils.createSScPlot(RetracedRelWorldX, RetracedRelWorldZ, '*'); grid on; hold on;
    xlabel('Across distance (m)'); ylabel('Z (m)'); title(Lang('Trajet rayon','Ray path'));
    
    % bateau
    [~, imin] = min(RetracedRelWorldX);
    [~, imax] = max(RetracedRelWorldX);
    %     PlotUtils.createSScPlot([0 min(RetracedRelWorldX)], [0 RetracedRelWorldZ(imin)], 'r');
    %     PlotUtils.createSScPlot([0 max(RetracedRelWorldX)], [0 RetracedRelWorldZ(imax)], 'g');
    % TODO : Ici faire la distinction entre Immersion et distance s�parant
    
    % l'antenne du point de r�f�rence (pb rencontra sur AUV�
    if Immersion < -10 % Si �cart de plus de 10m alors on suppose que c'est l'AUV. Je sais c'est abominablement mal fait TOD TODO TODO
        Immersion = 0;
    end
    PlotUtils.createSScPlot([0 min(RetracedRelWorldX)], [Immersion RetracedRelWorldZ(imin)], 'r');
    PlotUtils.createSScPlot([0 max(RetracedRelWorldX)], [Immersion RetracedRelWorldZ(imax)], 'g');
    
    scale = abs(min(RetracedRelWorldZ) / 3); % 30% de la hauteur d'eau
    pos = [  0 cosd(30) 0.5      0.5          -0.5         -0.5     -cosd(30) 0;...  %X
        -1 sind(30) sind(30) sind(30)+0.5 sind(30)+0.5 sind(30) sind(30) -1];     %Y
    matRot = [cosd(RollTx)  sind(RollTx); ...
        -sind(RollTx) cosd(RollTx)];
    newPos = matRot*pos*scale;
    patch(newPos(1,:), newPos(2,:),0, 'FaceColor','k')
    axis equal; axis tight
    
    %     figure(9853); plot(RetracedRelWorldX, RetracedRelWorldY, '*'); grid on
    
    %     Ylim = get(gca, 'Ylim');
    
    %     %% Vue de tribord
    %     subplot(122)
    %     PlotUtils.createSScPlot(RetracedRelWorldY, RetracedRelWorldZ, '*'); grid on; hold on;
    %     xlabel('Along distance (m)'); ylabel('Z (m)');
    %
    %     % bateau
    %     [~, imin] = min(RetracedRelWorldY);
    %     [~, imax] = max(RetracedRelWorldY);
    %
    %     PlotUtils.createSScPlot([0 min(RetracedRelWorldY)], [Immersion RetracedRelWorldZ(imin)], 'r');
    %     PlotUtils.createSScPlot([0 max(RetracedRelWorldY)], [Immersion RetracedRelWorldZ(imax)], 'g');
    %
    %     scale = abs(max(RetracedRelWorldY)); % 30% de la hauteur d'eau
    %     pos = [ -1 -1 1 1.5 0.2 0.2 -0.2 -0.2 ;...  %X
    %             0.7 -0.3 -0.3 1 0.7 1.5 1.5 0.7 ];     %Y
    %
    %     pitch = mean(PitchTx);
    %     matRot = [cosd(pitch)  sind(pitch); ...
    %               -sind(pitch) cosd(pitch)];
    %     newPos = matRot * pos * scale;
    %     patch(newPos(1,:), newPos(2,:),0, 'FaceColor','k')
    %     set(gca, 'Ylim', Ylim);
    %     axis equal
end
