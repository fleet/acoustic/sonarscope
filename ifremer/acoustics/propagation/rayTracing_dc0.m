% Calcul de la d�riv�e de la bathy en fonction de la �lerite de surface.
% La bathy est obtenue dans rayTracing � partir des temps de parcours et des angles
% avec un mod�le de c�l�rite par couches horizontales de gradient constant
%
% Syntax
%   [Xbathy, Zbathy, AngleIntersec, dXbathy, dZbathy, dAngleIntersec] = ...
%   rayTracing_dc0(ZCelerite, Celerite, AngleIncident, TempsSimple)
%
% Input Arguments
%   ZCelerite     : Profondeur des couches (m) : Vecteur de valeurs
%                   decroissantes (par ex de 0 a Zmax<0 )
%   Celerite      : Profil(s) de celerite en m/s aux profondeurs ZCelerite
%   AngleIncident : Angle(s) incident(s) a la surface : Vecteur
%   TempsSimple   : Temps aller-simple surface fond : Vecteur
%
% Output Arguments
%   []            : Auto-plot activation
%   XBathy        : Abscisses des intersections des rayons avec le fond.
%   ZBathy        : Ordonnees des intersections des rayons avec le fond.
%   AngleIntersec : Angles des rayons sur le fond.
%   dXBathy       : derivees par rapport  c0 des abscisses des intersections des rayons avec le fond.
%   dZBathy       : derivees par rapport  c0 des ordonnees des intersections des rayons avec le fond.
%   dAngleIntersec: derivees par rapport  c0 des angles des rayons sur le fond.
%
% EXEMPLES :
%   nomFic = getNomFicDatabase('BATHYCEL_North-East_Atlantic_Winter.csv');
%   [Z, S, T] = lecCsv(nomFic);
%   C = celeriteChen( Z, T, S );
%   teta = -75:75;
%   [X, TH, t] = TrajetRayon(Z, C, teta);
%   ts = t(end, :);
%
%   [Xbathy, Zbathy, AngleIntersec] = rayTracing(Z, C, teta, ts);
%       figure; plot(Xbathy, Zbathy, '*k')
%
%   dC = 2 ;
%   C2 = C; C2(1) = C2(1) + dC ;
%   [Xbathy, Zbathy, AngleIntersec] = rayTracing(Z, C2, teta, ts);
%       hold on; plot(Xbathy, Zbathy, '*r')
%
%   [Xbathy, Zbathy, AngleIntersec, dXbathy, dZbathy, dAngleIntersec] = rayTracing_dc0(Z, C, teta, ts);
%       hold on; plot(Xbathy+dC*dXbathy, Zbathy+dC*dZbathy, '*g')
%   rayTracing_dc0(Z, C, teta, ts);
%
%
% See also rayTracing TrajetRayon cl_car_bat_data/calculer_bathy Authors
% Authors : YHDR
%-------------------------------------------------------------------------------

% TODO : not used : keep it just in case until a unique function is validated for all sounders

function [Xbathy, Zbathy, AngleIntersec, dXbathy, dZbathy, dAngleIntersec] = ...
    rayTracing_dc0(ZCelerite, Celerite, AngleIncident, TempsSimple, varargin)

[varargin, Immersion] = getPropertyValue(varargin, 'Immersion', ZCelerite(1)); %#ok<ASGLU>

if Immersion ~= ZCelerite(1)
    indImm = sum(ZCelerite >= Immersion);
    ImmCelerite = interp1(ZCelerite, Celerite, Immersion, 'linear');
    ZCelerite(indImm) = Immersion ;
    ZCelerite = ZCelerite(indImm:end);
    Celerite(indImm) = ImmCelerite;
    Celerite = Celerite(indImm:end);
end

ZCelerite     = ZCelerite(:);
Celerite      = Celerite(:);
nceler        = length(ZCelerite);
ncouches      = nceler - 1 ;
hcouches      = -(diff(ZCelerite));
AngleIncident = AngleIncident(:)';
TempsSimple   = TempsSimple(:)';
nangles       = length(AngleIncident);

%% Afin de ne pas rencontrer de probl�me de limite (changement de formule),
% on modifie les c�l�rit�s conduisant a des gradients nuls,
% le tout a la racine de la pr�cision machine.

DCelerite = diff(Celerite);
prec      = sqrt(eps);
celermoy  = mean(Celerite);
relprec   = prec * celermoy ;
IndDroite = (abs(DCelerite)< relprec);
if any(IndDroite)
    for icouche = 1:ncouches
        if IndDroite(icouche)
            Celerite(icouche+1) = Celerite(icouche) + relprec ;
        end
    end
    %    Celerite
    DCelerite = diff(Celerite);
end

degrad = pi / 180 ;
raddeg = 1 / degrad ;

%  (meme manip sur les angles que sur
% les gradients, pour eviter les passages a la limite)

AngleIncident = AngleIncident * degrad ;
relprec = 10 * prec ;
AngleIncident(abs(AngleIncident)<relprec) = relprec;

%% Constante de rayon

qcst = sin(AngleIncident)/Celerite(1);
qcst_dc0 = - qcst / Celerite(1);
%
% teta = asin(Celerite * qcst);
xtmp = Celerite * qcst ;
teta = asin(xtmp);
teta_dc0 = (Celerite * qcst_dc0) ./ ((1-xtmp.^2).^(1/2));
teta_dc0(1,:) = 0.;
%
% betat = log(tan(teta/2));
xtmp = tan(teta/2);
betat = log(xtmp);
betat_dt = 0.5 * (1 + xtmp.^2) ./ xtmp ;
betat_dc0 = betat_dt .* teta_dc0 ;
%
gradcel = DCelerite./hcouches ;
gradcel_dc0 = -ones(1,nangles)/hcouches(1);

%% Temps de parcours des ncouches-1 premieres couches

% tau = diff(betat) ./ ( gradcel * ones(1,nangles) );
xtmp = 1 ./ gradcel * ones(1,nangles);
tau = diff(betat) .* xtmp ;
tau_dc0 = diff(betat_dc0) .* xtmp ;
tau_dc0(1,:) = tau_dc0(1,:) - gradcel_dc0 .* tau(1,:) .* xtmp(1,:);
%
Ttau = [zeros(1,nangles); cumsum(tau,1)];
Ttau_dc0 = [zeros(1,nangles); cumsum(tau_dc0,1)];

%% Identification de la couche ou s'arrete le rayon

iclast = sum((ones(ncouches+1,1) * TempsSimple) > Ttau); %
indexlast = iclast + (0:(ncouches+1):(nangles-1)*(ncouches+1));

%% Temps de parcours dans la derniere couche traversee

deltat = TempsSimple - Ttau(indexlast);
deltat_dc0 = - Ttau_dc0(indexlast);

%% Calcul du gradient dans la derniere couche, extrapole vers -infini

gradcel = [gradcel ; gradcel(end)];
gn = gradcel(iclast)';

%% Angles d'incidence au sol

% AngleIntersec = 2*(atan(tan(teta(indexlast)/2).*exp(gn.*deltat)));
vtmp = exp(gn.*deltat);
vtmp_dc0 = gn.* vtmp .* deltat_dc0 ;
wtmp = tan(teta(indexlast)/2);
wtmp_dc0 = 0.5 * (1 + wtmp.^2) .* teta_dc0(indexlast);
ztmp = vtmp .* wtmp ;
AngleIntersec = 2*atan(ztmp);
AngleIntersec_dc0 = 2 ./ (1 + ztmp.^2) .*( vtmp .* wtmp_dc0 + vtmp_dc0 .* wtmp );

%% Celerite au sol

cf = sin(AngleIntersec)./qcst ;
cf_dc0 = cos(AngleIntersec) ./ qcst .* AngleIntersec_dc0 - cf ./ qcst .* qcst_dc0;

%% Bathy au sol

dzpied = (cf-Celerite(iclast)')./gn ;
dzpied_dc0 = cf_dc0./gn ;
Zbathy = ZCelerite(iclast)'-dzpied ;
dZbathy = -dzpied_dc0 ;
Zbathy = Zbathy - Immersion;

%% Abscisse au sol

% rcourb = - ((Celerite./gradcel)*ones(1,nangles))./sin(teta);
xtmp = - (Celerite./gradcel)*ones(1,nangles);
rcourb = xtmp ./ sin(teta);
rcourb_dc0 = - rcourb .* cot(teta) .* teta_dc0 ;
rcourb_dc0(1,:) = rcourb_dc0(1,:) - rcourb(1,:) .* ( - 1/Celerite(1)+ gradcel_dc0 / gradcel(1) );
%
xrelatif =  rcourb(1:end-1,:).*diff(cos(teta));
xrelatif_dc0 =  rcourb_dc0(1:end-1,:).*diff(cos(teta)) - rcourb(1:end-1,:).*diff(sin(teta).*teta_dc0);
%
xtotal = [zeros(1,nangles); cumsum(xrelatif,1)];
xtotal_dc0 = [zeros(1,nangles); cumsum(xrelatif_dc0,1)];
%
Xbathy = xtotal(indexlast) - ...
    (cos(AngleIntersec)-cos(teta(indexlast))) .* Celerite(iclast)' ./ gn ./ sin(teta(indexlast));
dXbathy = xtotal_dc0(indexlast) - Celerite(iclast)' ./ gn .*...
    (-sin(AngleIntersec).*AngleIntersec_dc0./ sin(teta(indexlast))+ ...
    teta_dc0(indexlast).*(1-cos(AngleIntersec).*cos(teta(indexlast)))./(sin(teta(indexlast)).^2));

AngleIntersec = AngleIntersec * raddeg ;
dAngleIntersec = AngleIntersec_dc0 * raddeg ;

%% Sortie des parametre ou traces graphiques

if nargout == 0
    figure;
    plot(Xbathy, Zbathy, '*'); grid on; hold on;
    
    ColorOrder = get(gca, 'ColorOrder');
    for iang=1:nangles
        imod = 1 + mod(iang-1, size(ColorOrder, 1));
        xray = [xtotal(1:iclast(iang),iang); Xbathy(iang)];
        zray = [ZCelerite(1:iclast(iang)); Zbathy(iang)];
        plot(xray,zray, '--', 'Color', ColorOrder(imod, :));
    end
    xlabel('X (m)'); ylabel('Z (m)'); title('Trajet rayon');
end
