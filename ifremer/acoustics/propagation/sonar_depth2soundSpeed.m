function SoundSpeed = sonar_depth2soundSpeed(Depth, T, DataSoundSpeedProfile)

if isfield(DataSoundSpeedProfile, 'Z')
    Z = DataSoundSpeedProfile.Z(:,:);
    C = DataSoundSpeedProfile.C(:,:);
%     timeZC = DataSoundSpeedProfile.TimeProfileMeasurement; % Non valid�
    timeZC = DataSoundSpeedProfile.DatetimeStartOfUse; % Non valid�
elseif isfield(DataSoundSpeedProfile, 'SoundSpeed')
    Z = DataSoundSpeedProfile.Depth(:,:);
    C = DataSoundSpeedProfile.SoundSpeed(:,:);
%     timeZC = DataSoundSpeedProfile.TimeProfileMeasurement; % Valid�
    timeZC = DataSoundSpeedProfile.DatetimeStartOfUse; % Modif JMA le 17/07/2019 car DatetimeProfileMeasurement: 1992-06-06 00:00:51.120
%     timeZC = timeZC.timeMat;
else
    str1 = 'Message pour JMA : sonar_depth2soundSpeed profil de bathyc�l�rim�trie non d�cod�.';
    str2 = 'Message for JMA : sonar_depth2soundSpeed : sound speed profile not decoded.';
    my_warndlg(Lang(str1,str2), 1);
end

% TODO : prendre en compte la profondeur des antennes
% Immersion = 0;

SoundSpeed = NaN(size(Depth), 'single');
timeZC = [timeZC(:); datetime(Inf, 'ConvertFrom', 'datenum')];
for k1=1:(length(timeZC)-1)
    sub1 = find((T >= timeZC(k1)) & (T < timeZC(k1+1)));
    if ~isempty(sub1)
        if isvector(Z) % Ajout JMA le 25/03/2024
            z = Z;
            c = C;
            subNaN = isnan(z) | isnan(c);
            z(subNaN) = [];
            c(subNaN) = [];
        else
            if size(Z, 2) == (length(timeZC) - 1)
                z = Z(:,k1);
                c = C(:,k1);
            else
                z = Z(k1,:);
                c = C(k1,:);
            end
            subNaN = isnan(z) | isnan(c);
            z(subNaN) = [];
            c(subNaN) = [];
        end
        
%         FigUtils.createSScFigure; PlotUtils.createSScPlot(c, z); grid on;
        
        SoundSpeed(sub1,:) = interp1(z, c, Depth(sub1,:));
    end
end
