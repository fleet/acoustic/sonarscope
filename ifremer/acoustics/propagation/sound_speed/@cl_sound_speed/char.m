% Transformation en chaine de caracteres d'un objet de classe cl_sound_speed.
%
% Syntax
%   str = char(this) 
%
% Input Arguments
%   this : instance de cl_sound_speed
%
% Output Arguments
%   str : une chaine de caracteres representative d'un objet de classe cl_sound_speed
%
% Examples
%   sp  = cl_sound_speed ;
%   str = char (sp)      ;
%   disp (str)           ; 
%
% See also 
%   help cl_sound_speed Authors
%
% See also cl_sound_speed Authors
% Authors : JMA + XL
% VERSION  : $Id: char.m,v 1.5 2002/06/25 07:29:59 augustin Exp $
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HISTORIQUE DEVELOPPEMENT
%    XX/XX/XX   - JMA - creation
%    01/12/00   - DCF - normalisation et isolement de la fonctionnalite
%    08/03/01   - JMA - Normalisation
%    23/04/2001 - DCF - mise en reference
%    31/08/2001 - JMA - ajout propertyName 'SonarDepth'
% ----------------------------------------------------------------------------

function str = char (this)

% ----------------------------------------------------------------------------
% Initialisation de la variable retournee

str = [] ;

% ----------------------------------------------------------------------------
% Taille de la matrice donnee en argument

sz = size (this) ;

% ----------------------------------------------------------------------------
% Matrice non reduite a un element : traitement pour chaque element

if prod(sz) > 1
    % boucle sur les lignes
    for i=1:sz(1)
        % boucle sur les colonnes
        for j=1:sz(2)
            % appel recursif de cette methode sur l element courant, resultat
            % sauve dans une liste de chaine
            str{end+1} = sprintf('-------------\nElement (%d,%d)', i, j);
            str1 = char(this(i, j));
            str{end+1} = sprintf('%s\n', str1);
        end
    end
    % concatenation de toutes les chaines de la liste de chaines
    str = sprintf('%s\n', str{:});
    
    % ----------------------------------------------------------------------------
    % Matrice reduite a un element : construction de la chaine pour cet element
    
else
    str{end+1} = sprintf('                  Incoming <-> %s', SelectionDansListe2str(this.Incoming, getStrIncoming(this), 'Num'));
    str{end+1} = ' ';
    str{end+1} = sprintf('                     Ocean <-> %s', SelectionDansListe2str(this.Ocean, getStrOceansList(this), 'Num'));
    str{end+1} = sprintf('               CSVFileName <-- ''%s''', this.CSVFileName );
    str{end+1} = ' ';
    str{end+1} = sprintf('      LevitusAveragingType <-> %s', SelectionDansListe2str(this.LevitusAveragingType, getStrLevitusAveragingType(this), 'Num'));
    str{end+1} = sprintf('           LevitusFileName <-- ''%s''', this.LevitusFileName );
    str{end+1} = sprintf('                    Season <-> %s', SelectionDansListe2str(this.Season, SeasonsList, 'Num'));
    str{end+1} = sprintf('             LevitusMonth  <-> %s', SelectionDansListe2str(this.LevitusMonth, MonthsList, 'Num'));
    str{end+1} = sprintf('           LevitusGridSize <-> %s', SelectionDansListe2str(this.LevitusGridSize, getStrLevitusGridSize(this), 'Num'));
    str{end+1} = ' ';
    str{end+1} = sprintf('                  Latitude <-> %s', lat2str(this.Latitude));
    str{end+1} = sprintf('                 Longitude <-> %s', lon2str(this.Longitude));
    str{end+1} = ' ';
    str{end+1} = sprintf('          CaraibesFileName <-> ''%s''', this.CaraibesFileName );
    str{end+1} = sprintf('        CaraibesNbProfiles <-- %d', this.CaraibesNbProfiles );
    str{end+1} = sprintf('    CaraibesCurrentProfile <-> %d', this.CaraibesCurrentProfile );
    str{end+1} = sprintf('             AnneeCaraibes <-- %d', this.AnneeCaraibes );
    str{end+1} = sprintf('        CaraibesSoundSpeed <-- %s', num2strCode(this.CaraibesSoundSpeed) );

    if ~isempty(this.Date)
        Date = dayIfr2str(this.Date);
    else
        Date = ' ';
    end
    str{end+1} = sprintf('                      Date <-- %s', Date );
    
    if ~isempty(this.CaraibesTime)
        CaraibesTime = hourIfr2str(this.Date);
    else
        CaraibesTime = ' ';
    end
    str{end+1} = sprintf('              CaraibesTime <-- %s', CaraibesTime );

    str{end+1} = ' ';
    str{end+1} = sprintf('            IsModifiedData <-- %s', SelectionDansListe2str(this.IsModifiedData, {'Original';'Modified'}, 'Num'));
    str{end+1} = sprintf('                 Depth (m) <-- %s', num2strCode(this.Depth));
    str{end+1} = sprintf('         Temperature (deg) <-- %s', num2strCode(this.Temperature));
    str{end+1} = sprintf('           Salinity (0/00) <-- %s', num2strCode(this.Salinity));
    str{end+1} = sprintf('          SoundSpeed (m/s) <-- %s', num2strCode(this.SoundSpeed));
    str{end+1} = sprintf('           Frequency (kHz) <-- %s', num2strCode(this.Frequency));
    str{end+1} = sprintf('   LocalAbsorption (db/km) <-- %s', num2strCode(this.LocalAbsorption));
    str{end+1} = sprintf('AveragedAbsorption (db/km) <-- %s', num2strCode(this.AveragedAbsorption));
    str{end+1} = sprintf('            SonarDepth (m) <-> %s', num2str(this.SonarDepth));
    str{end+1} = sprintf('            RayAngles(deg) <-> %s', num2strCode(this.RayAngles));
    
    str = sprintf('%s\n', str{:});
end