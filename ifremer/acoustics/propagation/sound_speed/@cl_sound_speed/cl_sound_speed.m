% Constructeur d'une instance de cl_sound_speed.
%
% Syntax
%   a = cl_sound_speed (...)
% 
% Name-Value Pair Arguments 
%   [] : Ensemble vide pour signifier au constructeur de creer un objet vide
%        cad pas de set donc par d'init. Ceci est utile pour l'appel des fonctions
%        editobj utilisees dans les callbacks.
%
% Name-Value Pair Arguments
%    ProfilTypeSelect'     : {'Vase'} | 'Sable' | 'Roche' | 'Autre'
%    Frequency'            :  
%    Date'                 : 
%    CaraibesTime'         : 
%    Latitude'             : 
%    Longitude'            : 
%    Depth'                : 
%    Salinity'             : 
%    Temperature'          : 
%    SoundSpeed'           : 
%    LocalAbsorption'      : 
%    AveragedAbsorption'   : 
%
% Output Arguments
%    a : une instance de cl_sound_speed
% 
% Examples
%    a = cl_sound_speed
%    editobj(a)
%
% Remarks : 
%   l'argument [] utilis� seul construit l'instance sans initialisation 
%   � utiliser pour typer les m�thodes dans les callbacks
%
% See also cl_sound_speed Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function a = cl_sound_speed(varargin)

%% Construction de la structure de l instance
%    Incoming : 
%       1=Set of profiles, 2=Levitus, 3=Caraibes, 4=DataArray
%    LevitusAveragingType
%       1=Annual, 2=LevitusSeasonnier, 3=Monthly
%    IsModifiedData
%       1=Donnees d'origine, 2=Donnees modifiees
%    LevitusMonth
%       valeur a 1 dans le cas  Monthly
%    LevitusGridSize
%       1='1x1', 2='5x5';

ObjStruct.Incoming                 = 1;
ObjStruct.LevitusAveragingType     = 1;
ObjStruct.CSVFileName   	       = [];
ObjStruct.LevitusFileName          = [];
ObjStruct.CaraibesFileName         = [];
ObjStruct.CaraibesOldFileName      = [];
ObjStruct.CaraibesNbProfiles       = 0;
ObjStruct.CaraibesCurrentProfile   = [];
ObjStruct.CaraibesData	           = []; %{[]};
ObjStruct.AnneeCaraibes            = [];
ObjStruct.CaraibesSoundSpeed	   = [];
ObjStruct.IsModifiedData           = 1;
ObjStruct.Season        	       = 1;
ObjStruct.LevitusMonth        	   = 1;	
ObjStruct.LevitusGridSize          = 1;	
ObjStruct.Ocean		               = 1;	 
ObjStruct.Date                     = [];
ObjStruct.CaraibesTime             = [];
ObjStruct.Latitude                 = 0;
ObjStruct.Longitude                = 0;

ObjStruct.Frequency                = 100;
ObjStruct.Temperature              = [];
ObjStruct.Salinity                 = [];
ObjStruct.SoundSpeed               = [];
ObjStruct.Depth                    = [];
ObjStruct.LocalAbsorption          = [];
ObjStruct.AveragedAbsorption       = [];
ObjStruct.SonarDepth               = 0;

ObjStruct.OldLat                   = [];
ObjStruct.OldLon                   = [];
ObjStruct.RayAngles                = 0:10:70;

%% Creation de l'instance

a = class(ObjStruct, 'cl_sound_speed');

%% On regarde si l'objet a ete cree uniquement pour atteindre une methode de la classe

if (nargin == 1) && isempty(varargin{1})
    return
end

%% Initialisation des valeurs transmises lors de l'appel au constructeur

a = set(a, varargin {:});
