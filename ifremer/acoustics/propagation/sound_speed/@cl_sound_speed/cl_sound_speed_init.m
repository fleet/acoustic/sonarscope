% Initialisation d'un objet cl_sound_speed
%
% Syntax
%   b = cl_sound_speed_init(a)
%
% Input Arguments 
%   a : Objet dont on a modifie les commandes 
%
% Name-Value Pair Arguments
%   b : Objet dont les donnees ont ete adaptees aux commandes.
% 
% Examples
%   a = cl_sound_speed
%   a = cl_sound_speed_init(a)
%
% See also celeriteChen lecCsv lecLevitus cli_sound_speed Authors
% Authors : JMA + XL
% ----------------------------------------------------------------------------

function this = cl_sound_speed_init(this)

%% Initialisation en fonction du type de donnees en entree

switch this.Incoming
case 1
    init_set_of_profiles(this);
case 2
    init_levitus(this);
case 3
    init_caraibes(this);
case 4  
    % ne fait rien de plus
otherwise
    msg = sprintf('Incoming=%s non reconnu dans cl_sound_speed/cl_sound_speed_init', this.Incoming);
    my_warndlg (msg, 1);
end 

%% Traitement du retour

if nargout == 0
    assignin('caller', inputname(1), this);
end

