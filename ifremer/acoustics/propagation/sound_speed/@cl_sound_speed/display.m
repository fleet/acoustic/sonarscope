% Informe sur le contenu de l instance de cl_sound_speed
%
% Syntax
%   display (a)
% 
% Input Arguments 
%  a : instance de cl_sound_speed
%
% Examples
%  a = cl_sound_speed;
%  a
%
% See also cl_sound_speed Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function display(a) %#ok<DISPLAY>

fprintf('\n%s =\n\n', inputname(1));
fprintf('%s\n', char(a));
