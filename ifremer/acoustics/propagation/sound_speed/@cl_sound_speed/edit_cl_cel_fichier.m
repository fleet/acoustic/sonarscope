% Edition d'un fichier de bathycelerimetrie de Caraibes.
%
% Syntax
%   edit_cl_cel_fichier(a, ...)
%
% Input Arguments
%   a : instance de cl_sound_speed
%
% Name-Value Pair Arguments
%   RefreshData :
%
% Name-only Arguments
%   Save :
%
% See also cl_sound_speed Authors
% Authors : NS
% ----------------------------------------------------------------------------

function edit_cl_cel_fichier(a, varargin)

%% Choix de l'action en fonction des valeurs des arguments

[varargin, RefreshData] = getPropertyValue(varargin, 'RefreshData', []);
if ~isempty(RefreshData)
    editrefreshData(RefreshData);
    return
end

[varargin, Save] = getFlag(varargin, 'Save');
if Save
    saveObjInCaller;
    return
end

[varargin, RefreshPosition] = getFlag(varargin, 'RefreshPosition');
if RefreshPosition
    editrefreshPosition;
    return
end

[varargin, SelectProfil] = getFlag(varargin, 'SelectProfil');
if SelectProfil
    editSelectProfil;
    return
end

[varargin, Zoom] = getFlag(varargin, 'Zoom'); %#ok<ASGLU>
if Zoom
    activeZoom;
    return
end

%% Cr�ation de la figure

h0 = figure (...
    'Color',[0.8 0.8 0.8], ...
    'PaperOrientation', 'landscape', ...
    'PaperPosition', [18 180 576 432], ...
    'PaperType', 'a4letter', ...
    'PaperUnits', 'points', ...
    'Position', [200 100 800 580], ...
    'Tag', 'Fig', ...
    'ToolBar', 'none');

%% Cr�ation des frames et boutons

ib = 0;

%% Frame Entete

ib = ib+1;
ihm{ib}.Lig          = 1;
ihm{ib}.Col          = 1:10;
ihm{ib}.Style        = 'frame';
ihm{ib}.Tag          = 'frameEntete';

%% Frame a.CaraibesData

ib = ib+1;
ihm{ib}.Lig          = 2:13;
ihm{ib}.Col          = 1:5;
ihm{ib}.Style        = 'frame';
ihm{ib}.Tag          = 'frameData';

%% Frame infos visu

ib = ib+1;
ihm{ib}.Lig          = 2;
ihm{ib}.Col          = 6:10;
ihm{ib}.Style        = 'frame';
ihm{ib}.Tag          = 'frameInfoVisu';

%% Frame graph

ib = ib+1;
ihm{ib}.Lig          = 3:13;
ihm{ib}.Col          = 6:10;
ihm{ib}.Style        = 'frame';
ihm{ib}.Tag          = 'axes';

%% Frame frameSaveQuitHelp

ib = ib+1;
ihm{ib}.Lig          = 15;
ihm{ib}.Col          = 1:5;
ihm{ib}.Style        = 'frame';
ihm{ib}.Tag          = 'frameSaveQuitHelp';

%% Construction de la frame

fillFrame([], ihm);

%% Remplissage de la frame Entete

frameEntete = setFrameEntete(h0, 'CARAIBES SOUND SPEED PROFILES');

%% Remplissage de la frame Data

frameData = setFrameData(h0);

%% Remplissage de la frame InfoVisu

frameInfoVisu = setFrameInfoVisu(a);

%% Remplissage de la frame setFrameSaveQuitHelp

frameSaveQuitHelp = setFrameSaveQuitHelp(h0, 'edit_cl_cel_fichier', 'cl_sound_speed');

%% Trace des axes

HandleAxes = findobj( h0, 'Tag', 'axes');
set (HandleAxes, 'Visible', 'off');
position = get( HandleAxes, 'Position');

%% D�finition d'une marge

marge = position(3)/15;
VecteurMarge = [marge 0.5*marge -1.5*marge -0.6*marge];

%% Position avec marge

position = position + VecteurMarge;

%% Cr�ation de l'axe

h1 = axes ( ...
    'Parent', h0 , ...
    'Units', 'pixels', ...
    'CameraUpVector', [0 1 0], ...
    'Color', [1 1 1], ...
    'Units', 'Normalized', ...
    'Position', position , ...
    'Tag', 'Axes1', ...
    'XColor', [0 0 0], ...
    'XGrid', 'on', ...
    'YColor', [0 0 0], ...
    'YGrid', 'on', ...
    'ZColor', [0 0 0]);

%% Nombre de pages a afficher

DimMatriceData = size (a.CaraibesData, 1);
NombrePage = ceil( DimMatriceData/10 );

%% Propriet�s du Slider ChangePlage

HandleSlider = findobj( h0, 'Tag', 'ChangePlage');
if NombrePage==1
    set( HandleSlider,'Value', NombrePage);
else
    minstep = 1/( NombrePage-1);
    maxstep = 2*minstep;
    SliderStep = [minstep maxstep];
    set( HandleSlider,'Value', NombrePage, 'Min', 1, 'Max', NombrePage,...
        'SliderStep',SliderStep);
end

%% Sauvegarde des informations utiles dans le Usera.CaraibesData

UserData = [];
putInUserData('HandleObjet', a, UserData );
putInUserData('inputname', inputname(1), UserData );
putInUserData('SliderValue', NombrePage, UserData );
set(h0, 'UserData', UserData);
uiresume(h0);

%% Remplissage des Boutons Date, CaraibesTime, Lat et longitude

editrefreshPosition;

%% Trac� courbe

refreshCourbe;

%% Boucle d'attente

UserData = get(h0, 'UserData');
while ishandle(h0)
    uiwait(h0);
    if ishandle(h0)
        UserData = get(h0, 'UserData');
    end
end
a = getFromUserData( 'HandleObjetSauvegarde', UserData);
if ~isempty(a)
    assignin('caller', inputname(1), a);
end

%% C'est fini


%% PARTIE CORPS DES FONCTIONS

function editrefreshPosition

%% Mise en place du curseur et de son indice

h0 = gcf;
UserData = get(h0, 'UserData');
a = getFromUserData('HandleObjet', UserData);
DimMatriceData = size( a.CaraibesData, 1);
NombrePage = ceil( DimMatriceData/10 );
if NombrePage==1
    for i= 1:size( a.CaraibesData, 1)
        Numero = num2str(i);
        set( findobj(h0, 'Tag',  ['Date',         Numero]), 'Style', 'edit', 'String', dayIfr2str( a.CaraibesData{i,1} )  );
        set( findobj(h0, 'Tag',  ['CaraibesTime', Numero]), 'Style', 'edit', 'String', hourIfr2str(a.CaraibesData{i,2} )   );
        set( findobj(h0, 'Tag',  ['Latitude',     Numero]), 'Style', 'edit', 'String', lat2str(a.CaraibesData{i,3} )   );
        set( findobj(h0, 'Tag',  ['Longitude',    Numero]), 'Style', 'edit', 'String', lon2str(a.CaraibesData{i,4} )   );
        set( findobj(h0, 'Tag',  ['NumeroProfil', Numero]), 'Style', 'pushbutton', 'String', Numero  );
    end
    for i= size( a.CaraibesData, 1)+1:10
        Numero = num2str(i);
        set( findobj(h0, 'Tag',  ['Date',         Numero]), 'Style', 'text', 'String', 'X');
        set( findobj(h0, 'Tag',  ['CaraibesTime', Numero]), 'Style', 'text', 'String', 'X');
        set( findobj(h0, 'Tag',  ['Latitude',     Numero]), 'Style', 'text', 'String', 'X');
        set( findobj(h0, 'Tag',  ['Longitude',    Numero]), 'Style', 'text', 'String', 'X');
        set( findobj(h0, 'Tag',  ['NumeroProfil', Numero]), 'Style', 'text', 'String', '');
    end
    NumeroData = num2str(a.CaraibesCurrentProfile);
    set( findobj(h0, 'Tag', ['Date', NumeroData]), 'BackgroundColor', [1 1 0.8]);
    set( findobj(h0, 'Tag', ['CaraibesTime', NumeroData]), 'BackgroundColor',[1 1 0.8]);
    set( findobj(h0, 'Tag', ['Latitude', NumeroData]), 'BackgroundColor',[1 1 0.8]);
    set( findobj(h0,'Tag',['Longitude', NumeroData]),'BackgroundColor',[1 1 0.8]);
    return
end

OldSliderValue = getFromUserData( 'SliderValue', UserData);
HandleSlider = findobj( h0, 'Tag', 'ChangePlage');
NewSliderValue = get( HandleSlider, 'Value');

if ceil(NewSliderValue) ~= NewSliderValue
    if NewSliderValue < OldSliderValue
        NewSliderValue = floor(NewSliderValue);
    else
        NewSliderValue = ceil(NewSliderValue);
    end
end

set( HandleSlider, 'Value', NewSliderValue);
putInUserData( 'SliderValue', NewSliderValue , UserData );

set(h0, 'UserData', UserData);
uiresume(h0);


%% Remplissage des Boutons Date, CaraibesTime, Latitude et longitude

a = getFromUserData('HandleObjet', UserData);
DebutBoucle =  ( get( HandleSlider, 'Max') - NewSliderValue )*10 + 1;
FinBoucle = DebutBoucle + 9;

if NewSliderValue~=1
    for i= DebutBoucle:FinBoucle
        Numero = num2str(i - DebutBoucle + 1);
        set( findobj(h0, 'Tag',  ['Date', Numero]), 'Style', 'edit',...
            'String', dayIfr2str( a.CaraibesData{i,1} )  );
        set( findobj(h0, 'Tag',  ['CaraibesTime', Numero]), 'Style', 'edit',...
            'String', hourIfr2str(a.CaraibesData{i,2} )   );
        set( findobj(h0, 'Tag',  ['Latitude', Numero]), 'Style', 'edit',...
            'String', lat2str(a.CaraibesData{i,3} )   );
        set( findobj(h0, 'Tag',  ['Longitude', Numero]), 'Style', 'edit',...
            'String', lat2str(a.CaraibesData{i,4} )   );
        set( findobj(h0, 'Tag',  ['NumeroProfil', Numero]), 'Style',...
            'pushbutton','String', Numero  );
    end
else
    for i= DebutBoucle:size( a.CaraibesData, 1)
        Numero = num2str(i - DebutBoucle + 1);
        set( findobj(h0, 'Tag',  ['Date', Numero]), 'Style', 'edit',...
            'String', dayIfr2str( a.CaraibesData{i,1} )  );
        set( findobj(h0, 'Tag',  ['CaraibesTime', Numero]), 'Style', 'edit',...
            'String', hourIfr2str(a.CaraibesData{i,2} )   );
        set( findobj(h0, 'Tag',  ['Latitude', Numero]), 'Style', 'edit',...
            'String', lat2str(a.CaraibesData{i,3} )   );
        set( findobj(h0, 'Tag',  ['Longitude', Numero]), 'Style', 'edit',...
            'String', lat2str(a.CaraibesData{i,4} )   );
        set( findobj(h0, 'Tag',  ['NumeroProfil', Numero]), 'Style',...
            'pushbutton','String', Numero  );
    end
    for i= size( a.CaraibesData, 1)+1:FinBoucle
        Numero = num2str(i - DebutBoucle + 1);
        set( findobj(h0, 'Tag',  ['Date', Numero]),'Style', 'text', ...
            'String', 'X'  );
        set( findobj(h0, 'Tag',  ['CaraibesTime', Numero]), 'Style', 'text',...
            'String', 'X'  );
        set( findobj(h0, 'Tag',  ['Latitude', Numero]), 'Style', 'text',...
            'String', 'X'   );
        set( findobj(h0, 'Tag',  ['Longitude', Numero]), 'Style', 'text',...
            'String', 'X'  );
        set( findobj(h0, 'Tag',  ['NumeroProfil', Numero]), 'Style', 'text',...
            'String', '' );
    end
end

str = ['de   ', num2str(DebutBoucle)]; str = [str, '  a  '];

if FinBoucle>size(a.CaraibesData,1)
    FinBoucle = size(a.CaraibesData,1);
end
str = [str, num2str( FinBoucle ) ];
set( findobj(h0, 'Tag', 'PlageAngulaire'), 'String', str);

for i=1:10
    set( findobj(h0, 'Tag', ['Date',         num2str(i)]), 'BackgroundColor', [0.9 0.9 0.9]);
    set( findobj(h0, 'Tag', ['CaraibesTime', num2str(i)]), 'BackgroundColor', [0.9 0.9 0.9]);
    set( findobj(h0, 'Tag', ['Latitude',     num2str(i)]), 'BackgroundColor', [0.9 0.9 0.9]);
    set( findobj(h0, 'Tag', ['Longitude',    num2str(i)]), 'BackgroundColor', [0.9 0.9 0.9]);
end

if( (a.CaraibesCurrentProfile >= DebutBoucle) && (a.CaraibesCurrentProfile <= FinBoucle))
    HandleSlider = findobj( h0, 'Tag', 'ChangePlage');
    pageData = get( HandleSlider, 'Value');
    ligneData =  a.CaraibesCurrentProfile - ( get( HandleSlider, 'Max') - pageData )*10;
    NumeroData = num2str(ligneData);
    set( findobj(h0, 'Tag', ['Date',         NumeroData]), 'BackgroundColor', [1 1 0.8]);
    set( findobj(h0, 'Tag', ['CaraibesTime', NumeroData]), 'BackgroundColor', [1 1 0.8]);
    set( findobj(h0, 'Tag', ['Latitude',     NumeroData]), 'BackgroundColor', [1 1 0.8]);
    set( findobj(h0, 'Tag', ['Longitude',    NumeroData]), 'BackgroundColor', [1 1 0.8]);
end



function editrefreshData(ligneData)

h0 = gcf;
UserData = get(h0, 'UserData');
a = getFromUserData('HandleObjet', UserData);
DimMatriceData = size( a.CaraibesData, 1);
NombrePage = ceil( DimMatriceData/10 );


if NombrePage==1
    ligneProfil = ligneData;
else
    HandleSlider = findobj( h0, 'Tag', 'ChangePlage');
    pageData = get( HandleSlider, 'Value');
    ligneProfil =  ( get( HandleSlider, 'Max') - pageData )*10 + ligneData;
end
% NumeroProfil = num2str(ligneProfil);
NumeroData = num2str(ligneData);

Date = get( findobj(h0, 'Tag', ['Date', NumeroData]), 'String');
CaraibesTime = get( findobj(h0, 'Tag', ['CaraibesTime', NumeroData]), 'String');
Latitude = get( findobj(h0, 'Tag', ['Latitude', NumeroData]), 'String');
Longitude = get( findobj(h0, 'Tag', ['Longitude', NumeroData]), 'String');

if strcmp( Date, 'X')
    return
end

if ~strcmp(Date, dayIfr2str( a.CaraibesData{ligneProfil, 1}))
    set( findobj(h0, 'Tag', ['Date', NumeroData]), 'String', ...
        dayIfr2str( a.CaraibesData{ligneProfil, 1}));
end
if ~strcmp(CaraibesTime, dayIfr2str( a.CaraibesData{ligneProfil, 2}))
    set( findobj(h0, 'Tag', ['CaraibesTime', NumeroData]), 'String', ...
        hourIfr2str( a.CaraibesData{ligneProfil, 2}));
end
if ~strcmp(Latitude, dayIfr2str( a.CaraibesData{ligneProfil, 3}))
    set( findobj(h0, 'Tag', ['Latitude', NumeroData]), 'String', ...
        lat2str( a.CaraibesData{ligneProfil, 3}));
end
if ~strcmp(Longitude, dayIfr2str( a.CaraibesData{ligneProfil, 4}))
    set( findobj(h0, 'Tag', ['Longitude', NumeroData]), 'String', ...
        lon2str( a.CaraibesData{ligneProfil, 4}));
end

for i=1:10
    set( findobj(h0, 'Tag', ['Date',         num2str(i)]), 'BackgroundColor', [0.9 0.9 0.9]);
    set( findobj(h0, 'Tag', ['CaraibesTime', num2str(i)]), 'BackgroundColor',[0.9 0.9 0.9]);
    set( findobj(h0, 'Tag', ['Latitude',     num2str(i)]), 'BackgroundColor',[0.9 0.9 0.9]);
    set( findobj(h0, 'Tag', ['Longitude',    num2str(i)]), 'BackgroundColor',[0.9 0.9 0.9]);
end
set( findobj(h0, 'Tag', ['Date',         NumeroData]), 'BackgroundColor', [1 1 0.8]);
set( findobj(h0, 'Tag', ['CaraibesTime', NumeroData]), 'BackgroundColor', [1 1 0.8]);
set( findobj(h0, 'Tag', ['Latitude',     NumeroData]), 'BackgroundColor', [1 1 0.8]);
set( findobj(h0,'Tag',  ['Longitude',    NumeroData]),'BackgroundColor',  [1 1 0.8]);

h = getFromUserData( 'HandleCourbe', UserData);
set( h, 'Linewidth', 0.3);
set( h( ligneProfil), 'Linewidth', 4);

a.CaraibesCurrentProfile = ligneProfil;
putInUserData( 'HandleObjet', a, UserData );
set(h0, 'UserData', UserData);
uiresume(h0);


function refreshCourbe

h0 = gcf;
UserData = get(h0, 'UserData');
a = getFromUserData('HandleObjet', UserData);
DimMatriceData = size( a.CaraibesData, 1);
NombrePage = ceil( DimMatriceData/10 );
NewSliderValue = getFromUserData( 'SliderValue', UserData);

if NombrePage==1
    DebutBoucle = 1;FinBoucle = DebutBoucle + 9;
    if FinBoucle> size(a.CaraibesData, 1)
        FinBoucle = size(a.CaraibesData, 1);
    end
else
    HandleSlider = findobj( h0, 'Tag', 'ChangePlage');
    DebutBoucle =  (get(HandleSlider, 'Max') - NewSliderValue )*10 + 1;
    FinBoucle = DebutBoucle + 9;
    if FinBoucle> size(a.CaraibesData, 1)
        FinBoucle = size(a.CaraibesData, 1);
    end
end

hold on
for i=DebutBoucle:FinBoucle
    Z = a.CaraibesData{i, 5};
    C = a.CaraibesData{i, 6};
    h(i) = plot( C, Z); %#ok<AGROW>
end
grid on;
%zoom on;
hold off;

UserData = putInUserData('HandleCourbe', h, UserData);
set(h0, 'UserData', UserData);
uiresume(h0);

set( h( a.CaraibesCurrentProfile), 'Linewidth', 4);
set(gca, 'ButtonDownFcn',  'edit_cl_cel_fichier(cl_sound_speed, ''SelectProfil'');');


function saveObjInCaller
h0 = gcf;
UserData = get(h0, 'UserData');
a = getFromUserData( 'HandleObjet', UserData);
putInUserData( 'HandleObjetSauvegarde', a, UserData );
set(h0, 'UserData', UserData);
uiresume(h0);


function  editSelectProfil

h0 = gcf;
graphe = gca;

UserData = get(h0, 'UserData');
a = getFromUserData('HandleObjet', UserData);

% on prend la position cliquee
positionCliquee = get(graphe, 'CurrentPoint');

x = positionCliquee(1,1);
y = positionCliquee(1,2);

for i=1:size(a.CaraibesData,1)
    % test avec une seule courbe  (profondeur premier profil)
    xVecteur = x * ones(size(a.CaraibesData{i,6}, 1),1);
    yVecteur = y * ones(size(a.CaraibesData{i,5}, 1),1);
    
    % IsModifiedData des distances entre le point clique et les points
    % de la courbe
    IsModifiedDataDistances = (xVecteur - a.CaraibesData{i,6}).^2  +   (yVecteur - a.CaraibesData{i,5}).^2;
    
    % element dont la distance est minimale
    DistanceMin = min(IsModifiedDataDistances);
    
    % affectation de cette element dans un tableau
    DistanceMinTab(i) = DistanceMin; %#ok<AGROW>
end

%% Recherche de la courbe la plus proche

[~, IndiceCourbeMin] = min(DistanceMinTab);

a.CaraibesCurrentProfile = IndiceCourbeMin;

HandleSlider = findobj(h0, 'Tag', 'ChangePlage');

% positionnement a la bonne page
borneInf = (1 - IndiceCourbeMin)/10 + get( HandleSlider, 'Max');

% borneSup = 1 + get( HandleSlider, 'Max') - (IndiceCourbeMin)/10
set(HandleSlider, 'Value', ceil(borneInf));

pageData = get( HandleSlider, 'Value');
ligneData =  IndiceCourbeMin - ( get( HandleSlider, 'Max') - pageData )*10;

for i=1:10
    set( findobj(h0, 'Tag', ['Date',         num2str(i)]), 'BackgroundColor', [0.9 0.9 0.9]);
    set( findobj(h0, 'Tag', ['CaraibesTime', num2str(i)]), 'BackgroundColor', [0.9 0.9 0.9]);
    set( findobj(h0, 'Tag', ['Latitude',     num2str(i)]), 'BackgroundColor', [0.9 0.9 0.9]);
    set( findobj(h0, 'Tag', ['Longitude',    num2str(i)]), 'BackgroundColor', [0.9 0.9 0.9]);
end

NumeroData = num2str(ligneData);
set( findobj(h0, 'Tag', ['Date', NumeroData]), 'BackgroundColor', [1 1 0.8]);
set( findobj(h0, 'Tag', ['CaraibesTime', NumeroData]), 'BackgroundColor',[1 1 0.8]);
set( findobj(h0, 'Tag', ['Latitude', NumeroData]), 'BackgroundColor',[1 1 0.8]);
set( findobj(h0, 'Tag',['Longitude', NumeroData]),'BackgroundColor',[1 1 0.8]);

h = getFromUserData( 'HandleCourbe', UserData);
set( h, 'Linewidth', 0.3);
set( h( IndiceCourbeMin), 'Linewidth', 4);

a.CaraibesCurrentProfile = IndiceCourbeMin;
putInUserData( 'HandleObjet', a, UserData );
set(h0, 'UserData', UserData);
uiresume(h0);


function activeZoom

h0 = gcf;
etat = get( findobj(h0, 'Tag', 'ZoomSelectionProfil'), 'Value');
if etat == 1
    zoom on;
else
    zoom off;
end

%% Remplissage du frame Data

function frameData = setFrameData(h0)

ib = 0;

%% Bouton DateTitle

ib = ib+1;
ihm{ib}.Lig          = 1;
ihm{ib}.Col          = 3:6;
ihm{ib}.Style        = 'text';
ihm{ib}.String       = 'Date';
ihm{ib}.Tag          = 'DateText';

%% Bouton CaraibesTimeTitle

ib = ib+1;
ihm{ib}.Lig          = 1;
ihm{ib}.Col          = 7:10;
ihm{ib}.Style        = 'text';
ihm{ib}.String       = 'Time';
ihm{ib}.Tag          = 'CaraibesTimeText';


%% Bouton LatitudeTitle

ib = ib+1;
ihm{ib}.Lig          = 1;
ihm{ib}.Col          = 11:14;
ihm{ib}.Style        = 'text';
ihm{ib}.String       = 'Latitude';
ihm{ib}.Tag          = 'LatitudeText';

%% Bouton LongitudeTitle

ib = ib+1;
ihm{ib}.Lig          = 1;
ihm{ib}.Col          = 15:18;
ihm{ib}.Style        = 'text';
ihm{ib}.String       = 'Longitude';
ihm{ib}.Tag          = 'LongitudeText';

%% Boutons NumeroProfil

for tempo=2:11
    ib = ib+1;
    ihm{ib}.Lig          = tempo;
    ihm{ib}.Col          = 1:2;
    ihm{ib}.Style        = 'pushbutton';
    
    NumeroProfil = num2str(tempo-1);
    ihm{ib}.Tag = [ 'NumeroProfil', NumeroProfil];
    commande = sprintf ( ...
        'edit_cl_cel_fichier(cl_sound_speed, ''RefreshData'', %d);', tempo-1);
    ihm{ib}.Callback = commande;
end

%% Boutons date

for tempo=12:21
    ib = ib+1;
    ihm{ib}.Lig          = tempo-10;
    ihm{ib}.Col          = 3:6;
    ihm{ib}.Style        = 'text';
    
    NumeroDate = num2str(tempo-11);
    ihm{ib}.Tag          = [ 'Date', NumeroDate];
end

%% Boutons CaraibesTime

for tempo=22:31
    ib=ib+1;
    ihm{ib}.Lig          = tempo-20;
    ihm{ib}.Col          = 7:10;
    ihm{ib}.Style        = 'edit';
    
    NumeroCaraibesTime = num2str(tempo-21);
    ihm{ib}.Tag        = [ 'CaraibesTime', NumeroCaraibesTime];
    commande = sprintf('edit_cl_cel_fichier(cl_sound_speed ([]), ''RefreshData'', %d);', tempo-21);
    ihm{ib}.Callback   = commande;
end

%% Boutons Latitude

for tempo=32:41
    ib=ib+1;
    ihm{ib}.Lig          = tempo-30;
    ihm{ib}.Col          = 11:14;
    ihm{ib}.Style        = 'edit';
    
    NumeroLatitude = num2str(tempo-31);
    ihm{ib}.Tag   = [ 'Latitude', NumeroLatitude];
    commande = sprintf('edit_cl_cel_fichier(cl_sound_speed ([]), ''RefreshData'', %d);', tempo-31);
    ihm{ib}.Callback   = commande;
end

%% Boutons Longitude

for tempo=42:51
    ib=ib+1;
    ihm{ib}.Lig          = tempo-40;
    ihm{ib}.Col          = 15:18;
    ihm{ib}.Style        = 'edit';
    
    NumeroLongitude = num2str(tempo-41);
    ihm{ib}.Tag     = ['Longitude', NumeroLongitude];
    commande = sprintf('edit_cl_cel_fichier(cl_sound_speed ([]), ''RefreshData'', %d);', tempo-41);
    ihm{ib}.Callback  = commande;
end

%% Bouton ChangePlage

ib = ib+1;
ihm{ib}.Lig          = 2:11;
ihm{ib}.Col          = 19:20;
ihm{ib}.Style        = 'Slider';
ihm{ib}.Tag          = 'ChangePlage';
ihm{ib}.Callback     = 'edit_cl_cel_fichier(cl_sound_speed ([]), ''RefreshPosition'');';

%% Cr�ation de la frame

frameData = fillFrame('frameData', ihm);

%% Positionnement automatique des boutons

handleFrame = findobj(h0, 'Tag', 'frameData');
% Position = get(handleFrame , 'Position');
% theMargin = 0.005;

% Fin du remplissage du frame Data

%% Remplissage du frame InfoVisu

function frameInfoVisu = setFrameInfoVisu(a)

ib = 0;

%% Plage de donnee visualisee

ib = ib+1;
ihm{ib}.Lig     = 1;
ihm{ib}.Col     = 1:2;
ihm{ib}.Style   = 'Text';
ihm{ib}.String  = 'Plage de donnee visualisee';
ihm{ib}.Tag     = 'PlageDonneetext';

%% Plage de donnee visualis�e

ib = ib+1;
ihm{ib}.Lig     = 1;
ihm{ib}.Col     = 3:4;
ihm{ib}.Style   = 'Text';

str = ['de   ', num2str(1)]; str = [str, '  a  '];
N = size(a.CaraibesData, 1);
if(N <= 10)
    fin =  N;
else
    fin = 10;
end
str = [str, num2str(fin)];

ihm{ib}.String = str;
ihm{ib}.Tag    = 'PlageDonnee';

%% Cr�ation de la frame

frameInfoVisu = fillFrame('frameInfoVisu', ihm);

% Fin du remplissage du frame InfoVisu
