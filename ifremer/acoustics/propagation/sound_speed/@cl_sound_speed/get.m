% Recuperation des elements d'un objet cl_sound_speed.
%
% Syntax
%   [...] = get(a, ...)  
%
% Input Arguments 
%   a : instance de cl_sound_speed
%
% Name-Value Pair Arguments
%   Frequency            :  
%   Date                 : 
%   CaraibesTime         : 
%   Latitude             : 
%   Longitude            : 
%   Depth                : 
%   Salinity             : 
%   Temperature          : 
%   SoundSpeed           : 
%   LocalAbsorption      : 
%   AveragedAbsorption   : 
%   SonarDeprh           :
%
%   Autres informations disponibles
%    IsModifiedData         :
%    Season                 :
%    LevitusGridSize        :
%    LevitusMonth           :
%    Ocean                  :
%    RayAngles
%
%   Informations specifiques de l affichage
%    OceansList             :
%    CaraibesFileName       :
%    CaraibesCurrentProfile :
%    CaraibesSoundSpeed     :
%    Incoming               :
%    CaraibesNbProfiles     :
%    CaraibesData           :
%    LevitusAveragingType   :
%
% Output Arguments
%   Les PropertyValues correspondany aux PropertyNames.
%
% Examples
%   a = cl_sound_speed
%   S = get(a, 'Salinity')
%
% See also cl_sound_speed cli_sound_speed Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function varargout = get(this, varargin)

% ---------------------------------------
% Initialisation de la variable de retour

varargout = {};

% --------------------------------------------------
% Traitement de chaque propriete donnee en arguments

for i=1:length(varargin)
    
    % Verification du type de l argument
    
    if ~ischar(varargin{i})
        w = sprintf('cl_sound_speed : Get %s non pris en compte', varargin{i});
        my_warndlg(w, 0);   
        
    else
        
        % ------------------------------
        % Selection du type de propriete
        
        switch varargin {i}
            
        case 'IsModifiedData'
            varargout{end+1} = this.IsModifiedData; %#ok<AGROW>
        case 'Temperature'
            varargout{end+1} = this.Temperature; %#ok<AGROW>
        case 'Salinity'
            varargout{end+1} = this.Salinity; %#ok<AGROW>
        case 'SoundSpeed'
            varargout{end+1} = this.SoundSpeed; %#ok<AGROW>
        case 'Depth'
            varargout{end+1} = this.Depth; %#ok<AGROW>
        case 'Date'
            varargout{end+1} = this.Date; %#ok<AGROW>
        case 'Season'
            varargout{end+1} = this.Season; %#ok<AGROW>
        case 'LevitusGridSize'
            varargout{end+1} = this.LevitusGridSize; %#ok<AGROW>
        case 'LevitusMonth'
            varargout{end+1} = this.LevitusMonth; %#ok<AGROW>
        case 'Ocean'
            varargout{end+1} = this.Ocean; %#ok<AGROW>
        case 'CaraibesTime'
            varargout{end+1} = this.CaraibesTime; %#ok<AGROW>
        case 'LocalAbsorption'
            varargout{end+1} = this.LocalAbsorption; %#ok<AGROW>
        case 'AveragedAbsorption'
            varargout{end+1} = this.AveragedAbsorption; %#ok<AGROW>
        case 'Latitude'
            varargout{end+1} = this.Latitude; %#ok<AGROW>
        case 'Longitude'
            varargout{end+1} = this.Longitude; %#ok<AGROW>
        case 'Frequency'
            varargout{end+1} = this.Frequency; %#ok<AGROW>
        case 'CaraibesFileName'
            varargout{end+1} = this.CaraibesFileName; %#ok<AGROW>
        case 'CaraibesCurrentProfile'
            varargout{end+1} = this.CaraibesCurrentProfile; %#ok<AGROW>
        case 'CaraibesSoundSpeed'
            varargout{end+1} = this.CaraibesSoundSpeed; %#ok<AGROW>
        case 'Incoming'
            varargout{end+1} = this.Incoming; %#ok<AGROW>
        case 'CaraibesNbProfiles'
            varargout{end+1} = this.CaraibesNbProfiles; %#ok<AGROW>
        case 'CaraibesData'
            varargout{end+1} = this.CaraibesData; %#ok<AGROW>
        case 'LevitusAveragingType'
            varargout{end+1} = this.LevitusAveragingType; %#ok<AGROW>
        case 'SonarDepth'
            varargout{end+1} = this.SonarDepth; %#ok<AGROW>
        case 'RayAngles'
            varargout{end+1} = this.RayAngles; %#ok<AGROW>
            
        otherwise
            w = sprintf('cl_sound_speed : Get %s non pris en compte', varargin{i});
            my_warndlg(w, 0);   
        end
    end
end
