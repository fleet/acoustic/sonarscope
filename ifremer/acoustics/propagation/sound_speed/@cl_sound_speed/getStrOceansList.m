% Retourne une liste de noms d oceans
%
% Syntax
%   str = getStrOceansList(this)
%
% Input Arguments
%   a : instance de cl_sound_speed
%
% Output Arguments
%  str : une liste de noms d'oceans
%
% See also cl_sound_speed cli_sound_speed Authors
% Authors : JMA
% VERSION  : $Id: getStrOceansList.m,v 1.2 2002/06/25 07:29:59 augustin Exp $
% ----------------------------------------------------------------------------

function str = getStrOceansList(this)

str = { 'BATHYCEL_North-East_Atlantic'
        'BATHYCEL_Equatorial_Atlantic'
        'BATHYCEL_Norwegian_Sea'
        'BATHYCEL_Mediterranean'};
       