% Initialisation d'une instance de cl_sound_speed pour la propriete 'incoming=3'
%
% Syntax
%   b = init_caraibes(a)
% 
% Input Arguments 
%    a : instance de cl_sound_speed
% 
% Output Arguments 
%    b : instance de cl_sound_speed 
%
% Examples
%   a = cl_sound_speed
%   a = init_caraibes(a)
%
% See also celeriteChen lecCsv lecLevitus cli_sound_speed Authors
% Authors : JMA + XL
% ----------------------------------------------------------------------------

function this = init_caraibes(this)

%% Evite un traitement pour un fichier vide

if isempty(this.CaraibesFileName) || strcmp (this.CaraibesFileName, '')
    return 
end

%% Choix des donnees

if ~strcmp(this.CaraibesFileName, this.CaraibesOldFileName)
    Car = cl_car_cel_data(this.CaraibesFileName);
    mbTime = get(Car,'mbTime');
    mbDate = get(Car,'mbDate');
    Z = get(Car,'mbDepth');
    C = get(Car,'mbSoundVelocity');
    S = get(Car,'mbSalinity');
    T = get(Car,'mbTemperature');
    
    tempoDate = mbDate;		
    tempoCaraibesTime = mbTime;
    
    this.CaraibesNbProfiles = length(mbTime);
    
    for i=1:this.CaraibesNbProfiles
        this.CaraibesData{i,1} = tempoDate(i);        %Date
        this.CaraibesData{i,2} = tempoCaraibesTime(i); %heure
        this.CaraibesData{i,3} = 0;                    %latitude
        this.CaraibesData{i,4} = 0;                    %longitude
        this.CaraibesData{i,5} = Z(:,i);               %profondeur
        this.CaraibesData{i,6} = C(:,i);               %celerite
        this.CaraibesData{i,7} = S(:,i);               %salinite
        this.CaraibesData{i,8} = T(:,i);               %temperature
    end 
    
end

if isempty(this.CaraibesCurrentProfile)
    this.CaraibesCurrentProfile = 1;
end

if (this.CaraibesCurrentProfile < 1) || (this.CaraibesCurrentProfile > this.CaraibesNbProfiles)
    my_warndlg('Numero de profil incompatible', 1);
    this.CaraibesCurrentProfile = 1;
end

%% Affectation des champs 

C = this.CaraibesData{this.CaraibesCurrentProfile,6};
Z = this.CaraibesData{this.CaraibesCurrentProfile,5};
S = this.CaraibesData{this.CaraibesCurrentProfile,7};
T = this.CaraibesData{this.CaraibesCurrentProfile,8};
sub = find(~isnan(Z));
Z = Z(sub);
T = T(sub);
S = S(sub);
C = C(sub);

if any(S)  % Cas d'un profil de celerite contenant des info de salinite et de temperature
    % Bidouille a mettre plutot dans la procedure de lecture
    sub = find(Z == 0 & S == 0 & T == 0);
    Z(sub) = [];
    T(sub) = [];
    S(sub) = [];
    C(sub) = [];
end

this.Depth       = Z;
this.Salinity    = S;
this.Temperature = T;
this.CaraibesSoundSpeed = C;
% this.Latitude    = 0;
% this.Longitude   = 0;

%% Calcul de la celerite et de l'atteuation

this.SoundSpeed         = celeriteChen( Z, T, S );
[Alpha, AlphaSum]       = AttenuationGarrison(this.Frequency, Z', T', S', 'Immersion', this.SonarDepth );
this.LocalAbsorption    = Alpha;
this.AveragedAbsorption = AlphaSum;
this.IsModifiedData     = 1;

%% Traitement du retour

if nargout == 0
    assignin('caller', inputname(1), this);
end
