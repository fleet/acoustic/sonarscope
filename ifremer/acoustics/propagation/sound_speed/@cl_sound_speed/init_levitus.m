% Initialisation d'une instance de cl_sound_speed pour la propriete 'incoming=2'
%
% Syntax
%   b = initCaraibes(a)
% 
% Input Arguments 
%   a : instance de cl_sound_speed
% 
% Output Arguments 
%   b : instance de cl_sound_speed
%
% Examples
%   a = cl_sound_speed
%   a = init_levitus(a)
%
% See also celeriteChen lecCsv lecLevitus cli_sound_speed Authors
% Authors : JMA + XL
% -------------------------------------------------------------------------

function this = init_levitus(this)

switch this.LevitusAveragingType 
case 1	% 'Annual'
    Epoque = 1;
case 2	% 'LevitusSeasonnier'
    Epoque = this.Season;
case 3	% 'Monthly'
    Epoque = this.LevitusMonth;
end

[flag, Z, T, S, ~, ~, ~, ~, INFO] = lecLevitusSyntheseTemporelle(this.Latitude, this.Longitude, ...
    this.LevitusGridSize, this.LevitusAveragingType, Epoque);

if ~flag
    my_warndlg('Message for sonarscope@ifremer.fr : No Levitus data for this point, This bug must be fixed by searching the first neighboor which has data.', 1);
    return
end

if ~isempty(Z)
    this.LevitusFileName = INFO{1}.FileName;
    this.Depth       = Z;
    this.Salinity    = S;
    this.Temperature = T;

    % ----------------------------------------
    % Calcul de la c�l�rit� et de l'att�nuation

    this.SoundSpeed         = celeriteChen(Z, T, S);
    [Alpha, AlphaSum]       = AttenuationGarrison(this.Frequency, Z', T', S', 'Immersion', this.SonarDepth);
    this.LocalAbsorption    = Alpha;
    this.AveragedAbsorption = AlphaSum;
    this.IsModifiedData     = 1;
else
    this.Longitude = this.OldLon;
    this.Latitude  = this.OldLat;
    this.IsModifiedData = 0;
end

%% Traitement du retour

if nargout == 0
    assignin('caller', inputname(1), this);
end
