% Initialisation d'une instance de cl_sound_speed pour la propriete 'incoming=1'
%
% Syntax
%   b = init_set_of_profiles(this)
%
% Input Arguments 
%    this : instance de cl_sound_speed a initialisee
%
% Output Arguments 
%    b : instance de cl_sound_speed a initialisee
%
% Examples
%   a = cl_sound_speed
%   a = init_set_of_profiles(a)
%
% See also celeriteChen lecCsv lecLevitus cli_sound_speed Authors
% Authors : JMA + XL
% ----------------------------------------------------------------------------

function this = init_set_of_profiles(this)

%% Gestion de l'acc�s aux donn�es

OceansList = getStrOceansList(this);
nomZTS     = sprintf('%s_%s.csv', OceansList{this.Ocean}, SeasonsList(this.Season));
nomFic = getNomFicDatabase('BATHYCEL_North-East_Atlantic_Winter.csv');
if isempty(nomFic)
    return
end
dirZTS        = fileparts(nomFic);
this.CSVFileName = fullfile(dirZTS, nomZTS);

%% Acc�s aux donn�es

[Z, S, T, lat, lon, Annee, ~, LevitusGridSize] = lecCsv(this.CSVFileName);

%% Affectation des champs

this.Depth	         = Z;
this.Salinity        = S;
this.Temperature     = T;
this.Latitude        = lat;
this.Longitude       = lon;
this.AnneeCaraibes   = str2double(Annee);
this.LevitusGridSize = LevitusGridSize;

%% Calcul de la c�l�rit� et de l'att�uation

this.SoundSpeed         = celeriteChen( Z, T, S );
% Il faut changer la convention
[Alpha, AlphaSum]       = AttenuationGarrison(this.Frequency, Z', T', S', 'Immersion', this.SonarDepth ); 
% dans AttenuationGarrison ----> prevenir Xavier
this.LocalAbsorption    = Alpha;
this.AveragedAbsorption = AlphaSum;
this.IsModifiedData     = 1;

%% Traitement du retour

if nargout == 0
    assignin('caller', inputname(1), this);
end
