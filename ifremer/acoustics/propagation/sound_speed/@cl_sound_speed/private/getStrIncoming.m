% Retourne la liste des choix
%
% Syntax
%   str = getStrIncoming (this)
%
% Output Arguments
%   str : une liste des choix
%
% See also Authors
% Authors : NS
% ----------------------------------------------------------------------------

function str = getStrIncoming(~)

str = {'Set of profiles'; 'Levitus'; 'Caraibes'; 'IsModifiedData'};