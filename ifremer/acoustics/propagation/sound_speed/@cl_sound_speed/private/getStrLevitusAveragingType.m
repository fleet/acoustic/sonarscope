% Retourne une liste de type de periodes
%
% Syntax
%   str = getStrLevitusAveragingType (this)
%
% Output Arguments
%  str : une liste de type de periodes
%
% See also Authors
% Authors : NS
% ----------------------------------------------------------------------------

function str = getStrLevitusAveragingType(~)

str = {'Annual'; 'Seasonnal'; 'Monthly'};
