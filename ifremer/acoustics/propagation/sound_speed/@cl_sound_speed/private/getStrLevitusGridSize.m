% Retourne une liste de type de maillage
%
% Syntax
%   str = getStrLevitusGridSize (this)
%
% Output Arguments
%   str : liste de type de maillage
%
% See also Authors
% Authors : NS
% ----------------------------------------------------------------------------

function str = getStrLevitusGridSize(~)

str = {'1x1'; '5x5'};
