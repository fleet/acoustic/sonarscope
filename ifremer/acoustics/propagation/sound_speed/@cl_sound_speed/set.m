% Modification des champs de la classe cl_sound_speed
%
% Syntax
%   [...] = set (a, ...)
%
% Input Arguments
%   a : instance de cl_sound_speed
%
% Name-Value Pair Arguments
%   Incoming                : {'1=Set of profiles'} | '2=Levitus' | '3=Caraibes' | '4=IsModifiedData'
%
%   Si Incoming=1
%     Ocean                   : {'1=North-East_Atlantic'} | '2=Equatorial_Atlantic' | '3=Norwegian_Sea' | '4=Mediterranean'
%
%   Si Incoming=2
%     LevitusAveragingType    : {'1=Annual'} | '2=Seasonnal' | '3=Monthly'
%     Season                  : {'1=Winter'} | '2=Spring' | '3=Summer' | '4=Autumn'
%     LevitusMonth            : {'1=January'} | '2=February' | '3=March' | '4=April' | '5=May' | '6=June' | '7=July' | '8=August' | '9=September' | '10=October' | '11=November' | '12=December'
%     LevitusGridSize         : {'1=1x1'} | '2=5x5'
%     Latitude                :
%     Longitude               :
%
%   Si Incoming=3
%     CaraibesFileName        :
%     CaraibesCurrentProfile  :
%     SonarDepth              :
%     RayAngles               :
%
% Examples
%   a = cl_sound_speed
%
% Remarks :
%    Si aucune variable de retour n est precisee, initialisation de
%    l'instance donnee en arguments
%
% See also celeriteChen lecCsv lecLevitus cli_sound_speed Authors
% Authors : JMA + XL
% VERSION  : $Id: set.m,v 1.5 2002/06/25 08:57:31 augustin Exp augustin $
% ----------------------------------------------------------------------------

function varargout = set(this, varargin)

%% Lecture des proprietes donnees en arguments

[varargin, this.Incoming]             = getPropertyValue(varargin, 'Incoming',             this.Incoming);
[varargin, this.LevitusAveragingType] = getPropertyValue(varargin, 'LevitusAveragingType', this.LevitusAveragingType);
[varargin, this.CSVFileName]          = getPropertyValue(varargin, 'CSVFileName',          this.CSVFileName);
[varargin, this.LevitusFileName]      = getPropertyValue(varargin, 'NomFichier',           this.LevitusFileName);
[varargin, this.CaraibesFileName]     = getPropertyValue(varargin, 'NomFichier',           this.CaraibesFileName);
[varargin, this.IsModifiedData]       = getPropertyValue(varargin, 'IsModifiedData',       this.IsModifiedData);
[varargin, this.AnneeCaraibes]        = getPropertyValue(varargin, 'AnneeCaraibes',        this.AnneeCaraibes);
[varargin, this.Season]               = getPropertyValue(varargin, 'Season',               this.Season);
[varargin, this.LevitusMonth]         = getPropertyValue(varargin, 'LevitusMonth',         this.LevitusMonth);
[varargin, this.LevitusGridSize]      = getPropertyValue(varargin, 'LevitusGridSize',      this.LevitusGridSize);
[varargin, this.Ocean]                = getPropertyValue(varargin, 'Ocean',                this.Ocean);
[varargin, this.Date]                 = getPropertyValue(varargin, 'Date',                 this.Date);
[varargin, this.CaraibesTime]         = getPropertyValue(varargin, 'CaraibesTime',         this.CaraibesTime);
[varargin, this.Frequency]            = getPropertyValue(varargin, 'Frequency',            this.Frequency);
[varargin, this.Temperature]          = getPropertyValue(varargin, 'Temperature',          this.Temperature);
[varargin, this.Salinity]             = getPropertyValue(varargin, 'Salinity',             this.Salinity);
[varargin, this.SoundSpeed]           = getPropertyValue(varargin, 'SoundSpeed',           this.SoundSpeed);
[varargin, this.Depth]                = getPropertyValue(varargin, 'Depth',                this.Depth);
[varargin, this.LocalAbsorption]      = getPropertyValue(varargin, 'LocalAbsorption',      this.LocalAbsorption);
[varargin, this.AveragedAbsorption]   = getPropertyValue(varargin, 'AveragedAbsorption',   this.AveragedAbsorption);
[varargin, this.SonarDepth]           = getPropertyValue(varargin, 'SonarDepth',           this.SonarDepth);
[varargin, this.RayAngles]           = getPropertyValue(varargin, 'RayAngles',             this.RayAngles);

[varargin, newLat] = getPropertyValue(varargin, 'Latitude', this.Latitude);
if ~isempty(newLat)
    this.OldLat   =  this.Latitude ;
    this.Latitude = newLat ;
end

[varargin, newLon] = getPropertyValue(varargin, 'Longitude', this.Longitude);
if ~isempty(newLon)
    this.OldLon    = this.Longitude ;
    this.Longitude = newLon ;
end

[varargin, this.CaraibesFileName]       = getPropertyValue(varargin, 'CaraibesFileName',       this.CaraibesFileName);
[varargin, this.CaraibesCurrentProfile] = getPropertyValue(varargin, 'CaraibesCurrentProfile', this.CaraibesCurrentProfile); %#ok<ASGLU>

%% Initialisation de l'objet

this = cl_sound_speed_init(this);

%% Traitement du retour

if nargout == 0
    assignin('caller', inputname(1), this);
else
    varargout{1} = this;
end
