% Constructeur d'une instance de cli_sound_speed.
%
% this = cli_sound_speed(varargin)
% 
% Name-Value Pair Arguments 
%   [] : Ensemble vide pour signifier au constructeur de creer un objet vide
%        cad pas de set donc par d'init. Ceci est utile pour l'appel des 
%        fonctions editobj utilisees dans les callbacks.
%
% Name-Value Pair Arguments
%    'ProfilTypeSelect'     : {'Vase'} | 'Sable' | 'Roche' | 'Autre'
%    'Frequency'            :  
%    'Date'                 : 
%    'CaraibesTime'         : 
%    'Latitude'             : 
%    'Longitude'            : 
%    'Depth'                : 
%    'Salinity'             : 
%    'Temperature'          : 
%    'SoundSpeed'           : 
%    'LocalAbsorption'      : 
%    'AveragedAbsorption'   : 
%
% Output Arguments
%    this : une instance de cli_sound_speed
% 
% Examples
%    this = cli_sound_speed
%    editobj(this)
%
% Remarks : 
%   l'argument [] utilis� seul construit l'instance sans initialisation 
%   � utiliser pour typer les m�thodes dans les callbacks
%
% See also cli_sound_speed/set cli_sound_speed/get Authors
% Authors : DCF
% ----------------------------------------------------------------------------

function this = cli_sound_speed(varargin)

% --------------------------------------------------------------------------
% Definition de la structure et initialisation
%
% hFigure        : handle de la fenetre
% globalFrame    : frame de decoupage general
% c_entete       : composant, barre ifremer service appli copyright
% c_quit         : composant, barre quit save help
% c_axev         : composant, vecteur des axes de representation
% f_dataIncoming : frame presentant le type de donnees en entree
% f_setOfProfils : frame donnees 'set of profils'
% f_levitusFile  : frame donnees 'levitus file'
% f_caraibesFile : frame donnees 'caraibes file'
% f_modifiedData : frame donnees modifiees
% f_frequency    : frame de presentation de la frequence
% f_choice       : frame de presentation du choix d affichage
% zoom           : instance de gestion du zoom

this.hFigure        = [];
this.globalFrame    = [];
this.c_entete       = [];
this.c_quit         = [];
this.c_axev         = [];
this.f_dataIncoming = [];
this.f_setOfProfils = [];
this.f_levitusFile  = [];
this.f_caraibesFile = [];
this.f_modifiedData = [];
this.f_frequency    = [];
this.f_choice       = [];
this.zoom           = [];

%% Super-classe

vitesseDuSon = cl_sound_speed([]);

%% Instanciation

this = class(this, 'cli_sound_speed', vitesseDuSon);

%% Pre-nitialisation des champs

if ((nargin == 1) && isempty(varargin {1}))
    return
end
this = set(this, varargin {:});
