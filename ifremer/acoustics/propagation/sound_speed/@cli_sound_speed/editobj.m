% Representation de l this
%
% this = editobj (this) 
%
% Input Arguments
%   this : this de cli_sound_speed
%
% OUPUT PARAMETERS :
%   this : this de cli_sound_speed, gestionnaire de la figure
%
% Examples
%   editobj(cli_sound_speed)
%
% See also 
%   cli_sound_speed
%
% Authors : DCF
% -------------------------------------------------------------------------

function varargout = editobj(this, varargin)

%% Cr�ation et remplissage de la fen�tre

this = construire_ihm(this);

if nargout == 0

    %% Cas ou l'IHM ne rend pas la main. On ne peut pas recuperer les
    % modifications. On rend le bouton save invisible
    
    if ~isdeployed
        disp('Ce mode d''edition ''editobj(a)'' ne permet pas de sauver les modifications.')
        disp('Preferez : ''a = editobj(a);'' si cela est votre intention')
    end
    
else

    %% Boucle d'attente

    h0 = gcf;
    UserData = get(h0, 'UserData');
    while ishandle(h0)
        uiwait(h0);
        if ishandle(h0)
            UserData = get(h0, 'UserData');
        end
    end

    %% R�cuperation de l'instance sauv�e

    InstanceSauvee = getFromUserData('InstanceSauvee', UserData);
    if isempty(InstanceSauvee)
        varargout{1} = this;
    else
        varargout{1} = InstanceSauvee;
    end
end
