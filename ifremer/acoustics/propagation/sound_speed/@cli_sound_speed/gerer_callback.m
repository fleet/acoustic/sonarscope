% Gestion des callbacks
% 
% this = gerer_callback(this, varargin)
%
% Input Arguments
%    this : instance de cli_sound_speed
%    varargin : identifiant de call back, information specifiques, ...
%
% Output Arguments
%    this : instance de cli_sound_speed initialisee
%
% See also cli_sound_speed
% Authors : DCF
% ----------------------------------------------------------------------------

function this = gerer_callback(this, varargin)

if nargin == 0
    return
end

msg = varargin{1};
if isempty(msg)
    return
end

%% On recup�re l'instance dans le UserData de la figure

this = handle2obj(gcbf);
if ~isa(this, 'cli_sound_speed')
    return
end

%% Message provenant du bouton Save

if strcmp(msg, 'clc_quit__Save')
    savedObj2handle(this, this.hFigure);
    uiresume(this.hFigure);
    return
end

%% Selection du traitement approprie :
% - nargin == 2 : callback sans transmition d'info (gestion IHM)
% - nargin == 3 : callback avec transmition d'info (info � afficher sur IHM)

switch nargin
    case 2
        switch msg
            case 'Frequency'
                this = traiter_frequency(this);

            case 'DataIncoming'
                this = traiter_data_incoming(this);

            case 'Salinity'
                this = traiter_salinity(this);

            case 'Temperature'
                this = traiter_temperature(this);

            case 'SoundSpeed'
                this = traiter_sound_speed(this);

            case 'Absorption'
                this = traiter_absorption(this);

            case 'Ray'
                this = traiter_ray(this);

            case 'OceanChoice'
                this = traiter_ocean_choice(this);

            case 'SaisonChoice'
                this = traiter_season_choice(this);

            case 'DataEdition'
                this = traiter_data_edition(this);

            case 'AveragingChoice'
                this = traiter_averaging_choice(this);

            case 'GridSizeChoice'
                this = traiter_grid_size_choice(this);

            case 'LongitudeLevitus'
                this = traiter_longitude_levitus(this);

            case 'LatitudeLevitus'
                this = traiter_latitude_levitus(this);

            case 'Apply'
                this = traiter_apply(this);

            case 'EarthEdition'
                this = traiter_earth_edition(this);

            case 'SeasonChoice'
                this = traiter_season_choice_levitus(this, msg);

            case 'MonthChoice'
                this = traiter_month_choice_levitus(this, msg);

            case 'DataEditionLevitus'
                this = traiter_data_edition(this);%, msg);

            case 'BrowserCaraibes'
                this = traiter_browser_caraibes(this);

            case 'caraibesFileName'
                this = traiter_caraibes_file_name(this, msg);

            case 'CurrentProfile'
                this = traiter_current_profile(this, msg);

            case 'ProfileSelection'
                this = traiter_profile_selection(this);

            case 'DataEditionCaraibes'
                this = traiter_data_edition(this);

            case 'DataEditionModifiedData'
                this = traiter_data_edition(this);

%             case get(this.c_quit, 'msgSave')
%                 this = traiter_save(this);

            case {get(this.c_entete, 'msgIfremer'), ...
                    get(this.c_entete, 'msgService'), ...
                    get(this.c_entete, 'msgTitre'), ...
                    get(this.c_entete, 'msgCopyright')}
                this.c_entete = gerer_callback(this.c_entete, msg);
                return

            case {get(this.c_quit, 'msgQuit') , ...
                    get(this.c_quit, 'msgHelp')}
                this.c_quit = gerer_callback(this.c_quit, msg);
                return

            otherwise
                my_warndlg('cli_sound_speed/gerer_callback : invalid argument', 1);
        end

    case 3
        switch msg
            case 'zoom'
                this = traiter_activation_zoom(this, varargin{2});
                
            otherwise
                my_warndlg('cli_sound_speed/gerer_callback : invalid argument', 1);
        end

    case 4
        switch msg
            case 'zoom'
                this = traiter_zoom(this, varargin{2}, varargin{3});

            otherwise
                my_warndlg('cli_sound_speed/gerer_callback : invalid argument', 1);
        end

    otherwise
%         my_warndlg('cli_sound_speed/gerer_callback : too many arguments', 1);
end

%% Sauve l'instance dans le UserData

obj2handle(this, this.hFigure);

%% Message provenant du bouton Quit

if strcmp(msg, 'Quit')
    [rep, flag] = my_questdlg(Lang('TODO', 'Exit cli_sound_speed ?'));
    if flag && (rep == 1)
        delete(this.hFigure)
    end
end
