% Trace les variables de l instance
%
% plot (instance) 
%
% Input Arguments
%   instance : instance de cli_sound_speed
%
% Examples
%   plot (cli_sound_speed)
%
% See also 
%    help cli_sound_speed
%
% Authors : DCF
% VERSION  : $Id
% ----------------------------------------------------------------------------

function toto = plot(instance, varargin)

ib  = 0   ;
ihm = []  ;

% ---------------------
% Creation de la figure

instance.hFigure = figure ;

% ---------------------------------------
% Creation de la frame de support des axes

ib = ib + 1 ;
ihm{ib}.Lig     = 1              ;
ihm{ib}.Col     = 1              ;
ihm{ib}.Style   = 'frame'        ;
ihm{ib}.Tag     = 'ancre_c_axev' ;
ihm{ib}.Visible = 'off'           ;
frame           = cl_frame (...
    'InsetX'     , 10  , ...
    'InsetY'     , 10  , ...
    'MaxLig'     , 1   , ...
    'MaxCol'     , 1   ) ;
frame = set (frame, 'ListeUicontrols', ihm) ;

% -----------------
% Creation des axes

instance = creer_c_axev   (instance) ;
instance = init_courbes   (instance) ;
instance = tracer_courbes (instance) ;

% --------------------------------------------------------------------------
% Collapse le trace de rayon

instance.c_axev = collapse (instance.c_axev, 5, 'on') ;

% --------------------------------------------------------------------------
% Traite les labels

for i = 1:1:5

    a = get_c (instance.c_axev, i) ;
    h = get (a, 'handle')          ;

    if (i == 1)
        set (h, 'YTickLabelMode', 'auto') ;
        visibilite = 'on'                 ;
    else
        set (h, 'YTickLabel', [])         ;
        visibilite = 'off'                ;
    end
    h = get (h, 'YLabel')             ;
    set (h, 'Visible', visibilite)    ;

end



toto = instance.c_axev;

