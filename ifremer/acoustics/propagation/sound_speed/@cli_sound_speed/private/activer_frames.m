% Activation des frames de la fenetre
% 
% this = activer_frames(this)
%
% Input Arguments
%    this : instance de cli_sound_speed
%
% Output Arguments
%    this : instance de cli_sound_speed
%
% See also cli_sound_speed
% Authors : DCF
% ----------------------------------------------------------------------------

function this = activer_frames(this)

% ----------------------------
% Desactivation des composants

this.c_entete = set(this.c_entete, 'componentEnable', 'on');
this.c_quit   = set(this.c_quit, 'componentEnable', 'on');
this.c_axev   = set(this.c_axev, 'componentEnable', 'on');

% ------------------------
% Desactivation des frames

this.f_dataIncoming = set(this.f_dataIncoming, 'enable', 'on');
this.f_setOfProfils = set(this.f_setOfProfils, 'enable', 'on');
this.f_levitusFile  = set(this.f_levitusFile, 'enable', 'on');
this.f_caraibesFile = set(this.f_caraibesFile, 'enable', 'on');
this.f_frequency    = set(this.f_frequency, 'enable', 'on');
this.f_choice       = set(this.f_choice, 'enable', 'on');
