% Construction de base de l IHM
% 
% this = construire_ihm(this)
%
% Input Arguments
%    this : this de cli_sound_speed
%
% Output Arguments
%    this : this de cli_sound_speed
%
% See also cli_sound_speed
% Authors : DCF
% ----------------------------------------------------------------------------

function this = construire_ihm(this)

%% Cr�ation d'une figure et initialisation de ses propri�t�s

this.hFigure = figure;

pause(0.1)
set(this.hFigure, ...
    'Color',            [0.8 0.8 0.8], ...
    'PaperOrientation', 'landscape', ...
    'PaperPosition',    [18 180 576 432], ...
    'PaperType',        'a4letter', ...
    'PaperUnits',       'points', ...
    'Position',         [25 40 800 540], ...
    'Tag',              'Fig', ...
    'ToolBar',          'figure');

%% Cr�ation des frames generales

this = creer_frames(this);

%% Assemblage des elements graphiques avec les frames

titre = 'SOUND SPEED PROFILE' ;
this = creer_c_en_tete(this, titre);
this = creer_c_quit(this);
this = creer_c_axev(this);
this = creer_f_data_incoming(this);
this = creer_f_set_of_profils(this);
this = creer_f_levitus_file(this);
this = creer_f_caraibes_file(this);
this = creer_f_modified_data(this);
this = creer_f_frequency(this);
this = creer_f_choice(this);

%% Initialisation des axes

this = init_courbes(this);
this = tracer_courbes(this);
this = placer_courbes(this);

%% Mise � jour des frames

this = maj_frames(this);

%% Mise en place du zoom

this = creer_zoom(this);

%% Sauvegarde de l'objet dans le UserData de la figure

obj2handle(this, this.hFigure);

