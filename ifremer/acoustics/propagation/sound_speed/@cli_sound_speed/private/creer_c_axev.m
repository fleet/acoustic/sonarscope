% Cr�ation du composant de repr�sentation graphique (axe)
% 
% this = creer_c_axev (this)
%
% Input Arguments
%    this : instance de cli_sound_speed
%
% Output Arguments
%    this : instance de cli_sound_speed initialisee
%
% See also cli_sound_speed
% Authors : DCF
% ----------------------------------------------------------------------------

function this = creer_c_axev(this)

%% Cr�ation du composant

% this.c_axev = clc_axev ; % Comment� le 05/10/2015
this.c_axev = clc_axev( ...
    'nbElements',        5, ...
    'tag',               'clc_axev tag', ...
    'orientation',       'lig', ...
    'componentName',     'ancre_c_axev', ...
    'componentUserName', 'cli_sound_speed', ...
    'componentUserCb',   'gerer_callback') ;

%% Cr�ation graphique du composant

this.c_axev = editobj(this.c_axev);
