% Creation du composant de presentation de l application
% 
% instance = creer_c_en_tete (instance, titre)
%
% Input Arguments
%    instance : instance de cli_sound_speed
%
% Output Arguments
%    instance : instance de cli_sound_speed initialisee
%
% See also help cli_sound_speed
% Authors : DCF
% VERSION  : $Id
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    01/02/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function instance = creer_c_en_tete (instance, titre)

% ----------------------------------------------------------------------------
% ----------------------------------------------------------------------------
% ----------------------------------------------------------------------------
% Bloc try

% Maintenance Auto : try

  % --------------------------------------------------------------------------
  % Creation de l instance de frame d en tete
  
  instance.c_entete = clc_entete ;
  instance.c_entete = set ( instance.c_entete  , ...
      'titre'             , titre              , ...
      'componentName'     , 'ancre_c_entete'   , ...
      'componentUserName' , 'cli_sound_speed'      , ...
      'componentUserCb'   , 'gerer_callback'   ) ;
  
  % --------------------------------------------------------------------------
  % Creation graphique du composant
  
  instance.c_entete = editobj (instance.c_entete) ;

% ----------------------------------------------------------------------------
% ----------------------------------------------------------------------------
% ----------------------------------------------------------------------------
% Bloc catch

% Maintenance Auto : catch
% Maintenance Auto :   err ('cli_sound_speed', 'creer_c_en_tete', '') ;
% Maintenance Auto : end
