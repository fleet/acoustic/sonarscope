% Creation du composant de sortie de l application
% 
% this = creer_c_quit (this)
%
% Input Arguments
%    this : instance de cli_sound_speed
%
% Output Arguments
%    this : instance de cli_sound_speed initialisee
%
% See also cli_sound_speed
% Authors : DCF
% ----------------------------------------------------------------------------

function this = creer_c_quit(this)
  
this.c_quit = clc_quit;

this.c_quit = set (this.c_quit, ...
    'fQuit'             , 1, ...
    'fSave'             , 1, ...
    'fHelp'             , 1, ...
    'msgQuit'           , 'Quit', ...
    'componentName'     , 'ancre_c_quit', ...
    'componentUserName' , 'cli_sound_speed', ...
    'componentUserCb'   , 'gerer_callback') ;

this.c_quit = editobj(this.c_quit) ;
