% Cr�ation de la frame pour les fichiers caraibes
% 
% this = creer_f_caraibes_file (this)
%
% Input Arguments
%    this : instance de cli_sound_speed
%
% Output Arguments
%    this : instance de cli_sound_speed initialisee
%
% See also cli_sound_speed
% Authors : DCF
% ----------------------------------------------------------------------------

function this = creer_f_caraibes_file (this)

% selection du fichier caraibes  : texte de presentation et bouton
% nom du fichier caraibes selectionne (afficheur)
% nombre de profils              : texte de presentation et valeur
% profil courant                 : texte de presentation et valeur
% selection du profil courant (bouton)
% date                           : texte de presentation et valeur
% temps                          : texte de presentation et valeur
% browser                        : texte de presentation et bouton
% selection                      : texte de presentation et bouton
% edition des donnees            : texte de presentation et bouton


%% Initialisation des locales

ib  = 0;
ihm = [];

%% Texte de pr�sentation du nom du fichier caraibes

ib = ib + 1;
ihm{ib}.Lig    = 2;
ihm{ib}.Col    = 1;
ihm{ib}.Style  = 'text';
ihm{ib}.String = 'File Name';

%% Bouton de selection d un fichier caraibes

ib = ib + 1;
ihm{ib}.Lig    = 1:3;
ihm{ib}.Col    = 2;
ihm{ib}.Style  = 'pushbutton';
ihm{ib}.String = 'Browser';
ihm{ib}.Tag    = 'BrowserCaraibes';
ihm{ib}.Callback = ...
    'gerer_callback (cli_sound_speed ([]), ''BrowserCaraibes'');';

%% Nom du fichier caraibes selectionne

ib = ib + 1;
ihm{ib}.Lig    = 4:5;
ihm{ib}.Col    = 1:2;
ihm{ib}.Style  = 'edit';
ihm{ib}.Tag    = 'caraibesFileName';
ihm{ib}.String = get (this, 'CaraibesFileName');
ihm{ib}.Callback = 'gerer_callback (cli_sound_speed ([]), ''caraibesFileName'');';

%% Texte de presentation du nombre de profils

ib = ib + 1;
ihm{ib}.Lig    = 7;
ihm{ib}.Col    = 1;
ihm{ib}.Style  = 'text';
ihm{ib}.String = 'Number of profiles';

%% Valeur du nombre de profils

ib = ib + 1;
ihm{ib}.Lig    = 7:8;
ihm{ib}.Col    = 2;
ihm{ib}.Style  = 'text';
ihm{ib}.Tag    = 'NumberOfProfiles';
ihm{ib}.BackgroundColor = GrisClair;

%% Texte de presentation du profil courant

ib = ib + 1;
ihm{ib}.Lig    = 9;
ihm{ib}.Col    = 1;
ihm{ib}.Style  = 'text';
ihm{ib}.String = 'Current profile';

%% Valeur du profil courant

ib = ib + 1;
ihm{ib}.Lig    = 9:10    ;
ihm{ib}.Col    = 2       ;
ihm{ib}.Style  = 'edit'  ;
ihm{ib}.Tag    = 'CurrentProfile';
ihm{ib}.String = num2str (get (this, 'CaraibesCurrentProfile'));
ihm{ib}.Callback = 'gerer_callback (cli_sound_speed ([]), ''CurrentProfile'');';

%% Bouton de selection du profil courant

ib = ib + 1;
ihm{ib}.Lig    = 11:12;
ihm{ib}.Col    = 1:2;
ihm{ib}.Style  = 'pushbutton';
ihm{ib}.String = 'Profile selection';
ihm{ib}.Tag    = 'ProfileSelection';
ihm{ib}.Callback =  'gerer_callback (cli_sound_speed ([]), ''ProfileSelection'');';
ihm{ib}.Visible  = 'off';

%% Texte de presentation de la date

ib = ib + 1;
ihm{ib}.Lig    = 14;
ihm{ib}.Col    = 1;
ihm{ib}.Style  = 'text';
ihm{ib}.String = 'Date';

%% Valeur de la date

ib = ib + 1;
ihm{ib}.Lig    = 14:15;
ihm{ib}.Col    = 2;
ihm{ib}.Style  = 'text';
ihm{ib}.Tag    = 'Date';
ihm{ib}.BackgroundColor = GrisClair;
ihm{ib}.Callback = 'gerer_callback (cli_sound_speed ([]), ''Date'' );';

%% Texte de pr�sentation de l'heure

ib = ib + 1;
ihm{ib}.Lig    = 16;
ihm{ib}.Col    = 1;
ihm{ib}.Style  = 'text';
ihm{ib}.String = 'Time';

%% Valeur de l heure

ib = ib + 1;
ihm{ib}.Lig    = 16:17;
ihm{ib}.Col    = 2;
ihm{ib}.Style  = 'text';
ihm{ib}.Tag    = 'Time';
ihm{ib}.BackgroundColor = GrisClair;
ihm{ib}.Callback = 'gerer_callback (cli_sound_speed ([]), ''Time'' );';

%% Bouton d'�dition

ib = ib + 1;
ihm{ib}.Lig      = 19:20;
ihm{ib}.Col      = 1:2;
ihm{ib}.Style    = 'pushbutton';
ihm{ib}.String   = 'Data Edition';
ihm{ib}.Tag      = 'DataEditionCaraibes';
ihm{ib}.Visible  = 'off';
ihm{ib}.Callback = 'gerer_callback (cli_sound_speed ([]), ''DataEditionCaraibes'');';

%% Cr�ation des frames

this.f_caraibesFile = cl_frame(...
    'NomFrame', 'frameCaraibesFile', ...
    'InsetX', 5, ...
    'InsetY', 5, ...
    'MaxLig', 20, ...
    'MaxCol', 2);
this.f_caraibesFile = set(this.f_caraibesFile, 'ListeUicontrols', ihm);
