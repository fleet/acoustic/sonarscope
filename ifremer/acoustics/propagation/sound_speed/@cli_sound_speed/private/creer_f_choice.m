% Creation de la frame
% 
% this = creer_f_choice(this)
%
% Input Arguments
%    this : this de cli_sound_speed
%
% Output Arguments
%    this : this de cli_sound_speed initialisee
%
% See also cli_sound_speed
% Authors : DCF
% ----------------------------------------------------------------------------

function this = creer_f_choice(this)

% toggle affichage salinite
% toggle affichage temperature
% toggle affichage vitesse du son
% toggle affichage absorption

%% Initialisation des locales

ib  = 0;
ihm = [];

%% Choix affichage de la salinit�

ib = ib + 1;
ihm{ib}.Lig           = 1;
ihm{ib}.Col           = 1;
ihm{ib}.Style         = 'checkbox';
ihm{ib}.String        = 'Salinity';
ihm{ib}.Tag           = 'Salinity';
ihm{ib}.Value         = 1;
ihm{ib}.TooltipString = 'Plot Salinity';
ihm{ib}.Callback      = 'gerer_callback(cli_sound_speed([]), ''Salinity'');';

%% Choix affichage de la temp�rature

ib = ib + 1;
ihm{ib}.Lig           = 1;
ihm{ib}.Col           = 2;
ihm{ib}.Style         = 'checkbox';
ihm{ib}.String        = 'Temperature';
ihm{ib}.Tag           = 'Temperature';
ihm{ib}.Value         = 1;
ihm{ib}.TooltipString = 'Plot Temperature';
ihm{ib}.Callback      = 'gerer_callback(cli_sound_speed([]), ''Temperature'');';

%% Choix affichage de la vitesse du son

ib = ib + 1;
ihm{ib}.Lig           = 1;
ihm{ib}.Col           = 3;
ihm{ib}.Style         = 'checkbox';
ihm{ib}.String        = 'SoundSpeed';
ihm{ib}.Tag           = 'SoundSpeed';
ihm{ib}.Value         = 1;
ihm{ib}.TooltipString = 'Plot SoundSpeed';
ihm{ib}.Callback      = 'gerer_callback(cli_sound_speed([]), ''SoundSpeed'');';

%% Choix affichage de la Absorption

ib = ib + 1;
ihm{ib}.Lig           = 1;
ihm{ib}.Col           = 4;
ihm{ib}.Style         = 'checkbox';
ihm{ib}.String        = 'Absorption';
ihm{ib}.Tag           = 'Absorption';
ihm{ib}.Value         = 1;
ihm{ib}.TooltipString = 'Plot Absorption';
ihm{ib}.Callback      = 'gerer_callback(cli_sound_speed([]), ''Absorption'');';

%% Choix affichage Rayons

ib = ib + 1;
ihm{ib}.Lig           = 1;
ihm{ib}.Col           = 5;
ihm{ib}.Style         = 'checkbox';
ihm{ib}.String        = 'Ray';
ihm{ib}.Tag           = 'Ray';
ihm{ib}.Value         = 0;
ihm{ib}.TooltipString = 'Plot ray';
ihm{ib}.Callback      = 'gerer_callback(cli_sound_speed([]), ''Ray'');';

%% Cr�ation des frames

this.f_choice = cl_frame(...
    'NomFrame', 'frameChoice', ...
    'InsetX', 5, ...
    'InsetY', 5, ...
    'MaxLig', 1, ...
    'MaxCol', 1);
this.f_choice = set(this.f_choice, 'ListeUicontrols', ihm);
