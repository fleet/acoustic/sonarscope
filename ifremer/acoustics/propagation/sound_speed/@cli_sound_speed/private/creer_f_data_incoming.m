% Cr�ation de la frame indiquant la provenance des donn�es (data incoming)
% 
% this = creer_f_data_incoming (this)
%
% Input Arguments
%    this : instance de cli_sound_speed
%
% Output Arguments
%    this : instance de cli_sound_speed initialisee
%
% See also cli_sound_speed
% Authors : DCF
% ----------------------------------------------------------------------------

function this = creer_f_data_incoming(this)

%% Bouton menu de choix de la provenance des donn�es

%% Initialisation des locales

ib  = 0;
ihm = [];

%% Choix de la source des donn�es

ib = ib + 1;
ihm{ib}.Lig           = 1;
ihm{ib}.Col           = 1;
ihm{ib}.Style         = 'popupmenu';
ihm{ib}.String        = {'Set of Profiles';'Levitus'; 'Caraibes';'Modified'};
ihm{ib}.Tag           = 'DataIncoming';
ihm{ib}.Callback      = 'gerer_callback (cli_sound_speed ([]), ''DataIncoming'');';
ihm{ib}.TooltipString = 'Data Incoming';
ihm{ib}.Value         = get(this, 'Incoming');

%% Cr�ation des frames

this.f_dataIncoming = cl_frame(...
    'NomFrame', 'frameDataIncoming', ...
    'InsetX', 5, ...
    'InsetY', 5, ...
    'MaxLig', 1, ...
    'MaxCol', 1);
this.f_dataIncoming = set(this.f_dataIncoming, 'ListeUicontrols', ihm);
