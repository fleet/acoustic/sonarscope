% Creation de la frame indiquant la frequence
% 
% this = creer_f_frequency (this)
%
% Input Arguments
%    this : instance de cli_sound_speed
%
% Output Arguments
%    this : instance de cli_sound_speed initialisee
%
% See also cli_sound_speed
% Authors : DCF
% ----------------------------------------------------------------------------

function this = creer_f_frequency (this)

% texte indiquant la nature de la variable
% valeur de la variable

%% Initialisation des locales

ib  = 0;
ihm = [];

%% Texte indiquant le choix possible

ib = ib + 1;
ihm{ib}.Lig    = 1;
ihm{ib}.Col    = 1;
ihm{ib}.Style  = 'text';
ihm{ib}.String = 'Frequency (kHz)';

%% Valeur de la fr�quence

ib = ib + 1;
ihm{ib}.Lig      = 1;
ihm{ib}.Col      = 2;
ihm{ib}.Style    = 'edit';
ihm{ib}.String   = num2str (get (this, 'Frequency'));
ihm{ib}.Tag      = 'Frequency';
ihm{ib}.TooltipString = 'n''intervient que dans le calcul de l''attenuation';
ihm{ib}.BackgroundColor = [1 1 1];
ihm{ib}.Callback = 'gerer_callback (cli_sound_speed ([]), ''Frequency'');';

%% Cr�ation des frames

this.f_frequency = cl_frame(...
    'NomFrame', 'frameFrequency', ...
    'InsetX', 5, ...
    'InsetY', 5, ...
    'MaxLig', 1, ...
    'MaxCol', 2);
this.f_frequency = set(this.f_frequency, 'ListeUicontrols', ihm);
