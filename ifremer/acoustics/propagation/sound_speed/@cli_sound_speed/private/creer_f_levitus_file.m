% Creation de la frame de presentation des donnees levitus
% 
% this = creer_f_levitus_file(this)
%
% Input Arguments
%    this : this de cli_sound_speed
%
% Output Arguments
%    this : this de cli_sound_speed initialisee
%
% See also cli_sound_speed
% Authors : DCF
% ----------------------------------------------------------------------------

function this = creer_f_levitus_file(this)

% texte de presentation de la moyenne
% choix de la moyenne
% texte de presentation du maillage
% choix du maillage
% Latitude  (texte et valeur)
% Longitude (texte et valeur)
% Acces a la carte de selection geographique
% Saison (texte et valeurs)
% Mois (texte et valeurs)
% Application des choix

%% Initialisation des locales

ib  = 0;
ihm = [];

%% Texte de presentation de la moyenne

ib = ib + 1;
ihm{ib}.Lig    = 1;
ihm{ib}.Col    = 1:2;
ihm{ib}.Style  = 'text';
ihm{ib}.String = 'Averaging Type';

%% Choix de la moyenne

ib = ib + 1;
ihm{ib}.Lig           = 1:2;
ihm{ib}.Col           = 3:4;
ihm{ib}.Style         = 'popupmenu';
ihm{ib}.String        = {'Annual'; 'Seasonnal'; 'Monthly'};
ihm{ib}.Tag           = 'AveragingChoice';
ihm{ib}.TooltipString = 'Averaging type choice';
ihm{ib}.Callback      = 'gerer_callback(cli_sound_speed([]), ''AveragingChoice'');';

%% Saison, texte

ib = ib + 1;
ihm{ib}.Lig      = 3;
ihm{ib}.Col      = 1:2;
ihm{ib}.Style    = 'text';
ihm{ib}.String   = 'Season';
ihm{ib}.Tag      = 'Season';
ihm{ib}.Visible  = 'off';

%% Saison, liste

ib = ib + 1;
ihm{ib}.Lig      = 3:4;
ihm{ib}.Col      = 3:4;
ihm{ib}.Style    = 'popupmenu';
ihm{ib}.String   = SeasonsList;
ihm{ib}.Tag      = 'SeasonChoice';
ihm{ib}.Visible  = 'off';
ihm{ib}.Callback = 'gerer_callback(cli_sound_speed([]), ''SeasonChoice'');';

%% Mois, texte

ib = ib + 1;
ihm{ib}.Lig      = 3;
ihm{ib}.Col      = 1:2;
ihm{ib}.Style    = 'text';
ihm{ib}.String   = 'Month';
ihm{ib}.Tag      = 'Month';
ihm{ib}.Visible  = 'off';

%% Saison, liste

ib = ib + 1;
ihm{ib}.Lig      = 3:4;
ihm{ib}.Col      = 3:4;
ihm{ib}.Style    = 'popupmenu';
ihm{ib}.String   = MonthsList;
ihm{ib}.Tag      = 'MonthChoice';
ihm{ib}.Visible  = 'off';
ihm{ib}.Callback = 'gerer_callback(cli_sound_speed([]), ''MonthChoice'');';

%% Texte de presentation du maillage

ib = ib + 1;
ihm{ib}.Lig    = 5;
ihm{ib}.Col    = 1:2;
ihm{ib}.Style  = 'text';
ihm{ib}.String = 'Grid size';

%% Choix du maillage

ib = ib + 1;
ihm{ib}.Lig           = 5:6;
ihm{ib}.Col           = 3:4;
ihm{ib}.Style         = 'popupmenu';
ihm{ib}.String        = {'1 X 1 deg'; '5 X 5 deg'};
ihm{ib}.Tag           = 'GridSizeChoice';
ihm{ib}.TooltipString = 'Grid size choice';
ihm{ib}.Callback      = 'gerer_callback(cli_sound_speed([]), ''GridSizeChoice'');';

%% Latitude, texte

ib = ib + 1;
ihm{ib}.Lig      = 8;
ihm{ib}.Col      = 2;
ihm{ib}.Style    = 'text';
ihm{ib}.String   = 'Latitude';

%% Latitude, valeur

ib = ib + 1;
ihm{ib}.Lig      = 8:9;
ihm{ib}.Col      = 3:4;
ihm{ib}.Style    = 'edit';
ihm{ib}.String   = 'N 00 00.00000';
ihm{ib}.Tag      = 'LatitudeLevitus';
ihm{ib}.Callback = 'gerer_callback(cli_sound_speed([]), ''LatitudeLevitus'');';

%% Longitude, texte

ib = ib + 1;
ihm{ib}.Lig      = 10;
ihm{ib}.Col      = 2;
ihm{ib}.Style    = 'text';
ihm{ib}.String   = 'Longitude';

%% Longitude, valeur

ib = ib + 1;
ihm{ib}.Lig      = 10:11;
ihm{ib}.Col      = 3:4;
ihm{ib}.Style    = 'edit';
ihm{ib}.String   = 'E 00 00.00000';
ihm{ib}.Tag      = 'LongitudeLevitus';
ihm{ib}.Callback = 'gerer_callback(cli_sound_speed([]), ''LongitudeLevitus'');';

%% Acces � la carte de s�lection g�ographique

ib = ib + 1;
ihm{ib}.Lig           = 7:11;
ihm{ib}.Col           = 1;
ihm{ib}.Style         = 'pushbutton';
ihm{ib}.CData         = imread(getNomFicDatabase('Earth.bmp'));
ihm{ib}.Tag           = 'EarthEdition';
ihm{ib}.TooltipString = 'Earth edition';
ihm{ib}.Callback      = 'gerer_callback(cli_sound_speed([]), ''EarthEdition'');';

%% Bouton d'�dition

ib = ib + 1;
ihm{ib}.Lig      = 17:18;
ihm{ib}.Col      = 1:4;
ihm{ib}.Style    = 'pushbutton';
ihm{ib}.String   = 'Data Edition';
ihm{ib}.Tag      = 'DataEditionLevitus';
ihm{ib}.Callback = ...
    'gerer_callback(cli_sound_speed([]), ''DataEditionLevitus'');';

%% Application des choix

ib = ib + 1;
ihm{ib}.Lig           = 19:20;
ihm{ib}.Col           = 1:4;
ihm{ib}.Style         = 'pushbutton';
ihm{ib}.Tag           = 'Apply';
ihm{ib}.String        = 'Apply';
ihm{ib}.Callback      = 'gerer_callback(cli_sound_speed([]), ''Apply'');';
ihm{ib}.BackgroundColor = [1 0 0]; % red
ihm{ib}.Visible       = 'off';

%% Cr�ation des frames

this.f_levitusFile = cl_frame(...
    'NomFrame'   , 'frameLevitusFile', ...
    'InsetX'     , 5                 , ...
    'InsetY'     , 5                 , ...
    'MaxLig'     , 20                , ...
    'MaxCol'     , 4                 );
this.f_levitusFile = set( this.f_levitusFile, 'ListeUicontrols', ihm);
