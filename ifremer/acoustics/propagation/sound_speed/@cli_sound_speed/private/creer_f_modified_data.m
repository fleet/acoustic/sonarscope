% Cr�ation de la frame des donnees modifiees
% 
% this = creer_f_modified_data (this)
%
% Input Arguments
%    this : instance de cli_sound_speed
%
% Output Arguments
%    this : instance de cli_sound_speed initialis�e
%
% See also cli_sound_speed
% Authors : DCF
% ----------------------------------------------------------------------------

function this = creer_f_modified_data (this)

% selection du fichier caraibes  : texte de presentation et bouton
% nom du fichier caraibes selectionne (afficheur)
% nombre de profils              : texte de presentation et valeur
% profil courant                 : texte de presentation et valeur
% selection du profil courant (bouton)
% date                           : texte de presentation et valeur
% temps                          : texte de presentation et valeur
% browser                        : texte de presentation et bouton
% selection                      : texte de presentation et bouton
% edition des donnees            : texte de presentation et bouton

%% Initialisation des locales

ib  = 0;
ihm = [];

%% Bouton d edition

ib = ib + 1;
ihm{ib}.Lig      = 19:20;
ihm{ib}.Col      = 1:2;
ihm{ib}.Style    = 'pushbutton';
ihm{ib}.String   = 'Data Edition';
ihm{ib}.Tag      = 'DataEditionModifiedData';
ihm{ib}.Visible  = 'on';
ihm{ib}.Callback = 'gerer_callback (cli_sound_speed ([]), ''DataEditionModifiedData'');';

%% Cr�ation des frames

this.f_modifiedData = cl_frame(...
    'NomFrame', 'frameModifiedData', ...
    'InsetX', 5, ...
    'InsetY', 5, ...
    'MaxLig', 20, ...
    'MaxCol', 2);
this.f_modifiedData = set ( this.f_modifiedData, 'ListeUicontrols', ihm);
