% Cr�ation de la frame permettant le choix parmi un jeu de profils
% 
% this = creer_f_set_of_profils (this)
%
% Input Arguments
%    this : instance de cli_sound_speed
%
% Output Arguments
%    this : instance de cli_sound_speed initialisee
%
% See also cli_sound_speed
% Authors : DCF
% ----------------------------------------------------------------------------

function this = creer_f_set_of_profils(this)

% Pr�sentation (ocean, saison)
% Choix de l ocean
% Choix de la saison
% Latitude  (texte et valeur)
% Longitude (texte et valeur)
% Edition des donnees

%% Initialisation des locales

ib  = 0;
ihm = [];

%% Texte de presentation de la partie gauche : oc�an

ib = ib + 1;
ihm{ib}.Lig    = 2:3;
ihm{ib}.Col    = 1;
ihm{ib}.Style  = 'text';
ihm{ib}.String = 'Ocean';

%% Texte de presentation de la partie droite : saison

ib = ib + 1;
ihm{ib}.Lig    = 2:3;
ihm{ib}.Col    = 2;
ihm{ib}.Style  = 'text';
ihm{ib}.String = 'Saison';

%% Choix de l'Oc�an

ib = ib + 1;
ihm{ib}.Lig      = 4:6;
ihm{ib}.Col      = 1;
ihm{ib}.Style    = 'popupmenu';
ihm{ib}.String   = getStrOceansList(cl_sound_speed([]));
ihm{ib}.Tag      = 'OceanChoice';
ihm{ib}.Callback = 'gerer_callback(cli_sound_speed ([]), ''OceanChoice'');';
ihm{ib}.TooltipString = 'Ocean Choice';

%% Choix de la saison

ib = ib + 1;
ihm{ib}.Lig      = 4:6;
ihm{ib}.Col      = 2;
ihm{ib}.Style    = 'popupmenu';
ihm{ib}.String   = {'Winter', 'Spring', 'Summer', 'Autumn'};
ihm{ib}.Tag      = 'SaisonChoice';
ihm{ib}.Callback = 'gerer_callback(cli_sound_speed ([]), ''SaisonChoice'');';
ihm{ib}.TooltipString = 'Saison Choice';

%% Longitude, texte

ib = ib + 1;
ihm{ib}.Lig      = 14:15;
ihm{ib}.Col      = 1;
ihm{ib}.Style    = 'text';
ihm{ib}.String   = 'Longitude';

%% Longitude, valeur

ib = ib + 1;
ihm{ib}.Lig      = 14:15;
ihm{ib}.Col      = 2;
ihm{ib}.Style    = 'text';
ihm{ib}.String   = lon2str(get(this, 'Longitude'));
ihm{ib}.Tag      = 'Longitude';

%% Latitude, texte

ib = ib + 1;
ihm{ib}.Lig      = 12:13;
ihm{ib}.Col      = 1;
ihm{ib}.Style    = 'text';
ihm{ib}.String   = 'Latitude';

%% Latitude, valeur

ib = ib + 1;
ihm{ib}.Lig      = 12:13;
ihm{ib}.Col      = 2;
ihm{ib}.Style    = 'text';
ihm{ib}.String   = lat2str(get(this, 'Latitude'));
ihm{ib}.Tag      = 'Latitude';

%% Edition des donn�es

ib = ib + 1;
ihm{ib}.Lig      = 17:19;
ihm{ib}.Col      = 1:2;
ihm{ib}.Style    = 'pushbutton';
ihm{ib}.String   = 'Data edition';
ihm{ib}.Tag      = 'DataEdition';
ihm{ib}.Callback = 'gerer_callback (cli_sound_speed ([]), ''DataEdition'');';

%% Cr�ation des frames

this.f_setOfProfils = cl_frame (    ...
    'NomFrame', 'frameSetOfProfils', ...
    'InsetX', 5, ...
    'InsetY', 5, ...
    'MaxLig', 20, ...
    'MaxCol', 2                  );
this.f_setOfProfils = set(this.f_setOfProfils, 'ListeUicontrols', ihm);
