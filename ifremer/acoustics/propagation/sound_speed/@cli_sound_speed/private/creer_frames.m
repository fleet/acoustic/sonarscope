% Creation des frames de decoupage generale de la fenetre
% 
% instance = creer_frames (instance)
%
% Input Arguments
%    instance : instance de cli_sound_speed
%
% Output Arguments
%    instance : instance de cli_sound_speed initialisee
%
% See also cli_sound_speed
% Authors : DCF
% ----------------------------------------------------------------------------

function instance = creer_frames (instance)

% 8 frames sont utilisees :
% - En tete                                    = ancre_c_entete
% - Choix de l origine des donnees             = frameDataIncoming
% - Set of profils                             = frameSetOfProfils
% - fichier levitus                            = frameLevitusFile
% - fichier caraibes                           = frameCaraibesFile
% - donnees modifiees                          = frameModifiedData
% - frequence                                  = frameFrequency
% - choix                                      = frameChoice
% - axes                                       = ancre_c_axev
% - Sauve, Aide et Quit                        = ancre_c_quit


%% Initialisation des locales

ib  = 0;
ihm = [];

%% frameEnTete

ib = ib + 1;
ihm{ib}.Lig   = 1:3;
ihm{ib}.Col   = 1:40;
ihm{ib}.Style = 'frame';
ihm{ib}.Tag   = 'ancre_c_entete';

%% frameQuit

ib = ib + 1;
ihm{ib}.Lig     = 37:40;
ihm{ib}.Col     = 1:15;
ihm{ib}.Style   = 'frame';
ihm{ib}.Tag     = 'ancre_c_quit';
ihm{ib}.Visible = 'on';

%% frameAxes

ib = ib + 1;
ihm{ib}.Lig     = 5:36;
ihm{ib}.Col     = 17:40;
ihm{ib}.Style   = 'frame';
ihm{ib}.Tag     = 'ancre_c_axev';
ihm{ib}.Visible = 'off';

%% frameDataIncoming

ib = ib + 1;
ihm{ib}.Lig   = 5:8;
ihm{ib}.Col   = 1:15;
ihm{ib}.Style = 'frame';
ihm{ib}.Tag   = 'frameDataIncoming';

%% frameSetOfProfils

ib = ib + 1;
ihm{ib}.Lig   = 9:32;
ihm{ib}.Col   = 1:15;
ihm{ib}.Style = 'frame';
ihm{ib}.Tag   = 'frameSetOfProfils';

%% frameLevitusFile

ib = ib + 1;
ihm{ib}.Lig     = 9:32;
ihm{ib}.Col     = 1:15;
ihm{ib}.Style   = 'frame';
ihm{ib}.Tag     = 'frameLevitusFile';

%% frameCaraibesFile

ib = ib + 1;
ihm{ib}.Lig   = 9:32;
ihm{ib}.Col   = 1:15;
ihm{ib}.Style = 'frame';
ihm{ib}.Tag   = 'frameCaraibesFile';

%% frameModifiedData

ib = ib + 1;
ihm{ib}.Lig   = 9:32;
ihm{ib}.Col   = 1:15;
ihm{ib}.Style = 'frame';
ihm{ib}.Tag   = 'frameModifiedData';

%% frameFrequency

ib = ib + 1;
ihm{ib}.Lig   = 33:36;
ihm{ib}.Col   = 1:15;
ihm{ib}.Style = 'frame';
ihm{ib}.Tag   = 'frameFrequency';

%% frameChoice

ib = ib + 1;
ihm{ib}.Lig   = 37:40;
ihm{ib}.Col   = 16:40;
ihm{ib}.Style = 'frame';
ihm{ib}.Tag   = 'frameChoice';

%% Cr�ation des frames

instance.globalFrame = cl_frame(...
    'InsetX', 10, ...
    'InsetY', 10, ...
    'MaxLig', 40, ...
    'MaxCol', 40);
instance.globalFrame = set(instance.globalFrame, 'ListeUicontrols', ihm);
