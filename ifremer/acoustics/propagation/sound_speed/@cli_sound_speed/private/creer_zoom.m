% Creation de l'instance de gestion du zoom
% 
% this = creer_zoom(this)
%
% Input Arguments
%    this : instance de cli_sound_speed
%
% Output Arguments
%    this : instance de cli_sound_speed initialisee
%
% See also cli_sound_speed
% Authors : DCF
% VERSION  : $Id
% ----------------------------------------------------------------------------

function this = creer_zoom(this)

hg = [];

% -------------------------------
% Creation d une instance de zoom

this.zoom = cl_zoom;
cbZoomIn  = 'eval(''gerer_callback(cli_sound_speed, ''''zoom'''', ''''In'''');'');';
cbZoomOut = 'eval(''gerer_callback(cli_sound_speed, ''''zoom'''', ''''Out'''');'');';

% -------------------------------------------------------
% Determination des handles des graphes utilisant le zoom

for i = 1:1:5
    a  = get_c(this.c_axev, i);
    hg(i) = get(a, 'handle');
end

% ------------------------------------------------
% Determination de l'asservissement entre les axes

asserv = [3 2 2 2 2 ; 2 3 2 2 2 ; 2 2 3 2 2 ; 2 2 2 3 2 ; 2 2 2 2 3];

% ----------------------
% Initialisation du zoom

this.zoom = set(this.zoom, 'cbName', 'gerer_callback', 'cbObj', 'cli_sound_speed');
this.zoom = set(this.zoom, ...
    'hFig'   , this.hFigure, ...
    'hAxes'  , hg              , ...
    'asserv' , asserv          , ...
    'action' , 'In'            );

% -----------------------------------------------
% Definition des call back sur les icones du menu

maj_icone_cb(this.zoom, cbZoomIn, cbZoomOut);
