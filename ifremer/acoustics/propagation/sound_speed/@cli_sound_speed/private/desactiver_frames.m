% Desactivation des frames de la fenetre
% 
% this = desactiver_frames(this)
%
% Input Arguments
%    this : instance de cli_sound_speed
%
% Output Arguments
%    this : instance de cli_sound_speed
%
% See also cli_sound_speed
% Authors : DCF
% ----------------------------------------------------------------------------

function this = desactiver_frames(this)

% ----------------------------
% Desactivation des composants

this.c_entete = set(this.c_entete,  'componentEnable', 'off');
this.c_quit   = set(this.c_quit,    'componentEnable', 'off');
this.c_axev   = set(this.c_axev,    'componentEnable', 'off');

% ------------------------
% Desactivation des frames

this.f_dataIncoming = set(this.f_dataIncoming,  'enable', 'off');
this.f_setOfProfils = set(this.f_setOfProfils,  'enable', 'off');
this.f_levitusFile  = set(this.f_levitusFile,   'enable', 'off');
this.f_caraibesFile = set(this.f_caraibesFile,  'enable', 'off');
this.f_frequency    = set(this.f_frequency,     'enable', 'off');
this.f_choice       = set(this.f_choice,        'enable', 'off');
