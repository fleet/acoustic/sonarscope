% Efface l ensemble des courbes
% 
% instance = effacer_courbes (instance)
%
% Input Arguments
%    instance : instance de cli_sound_speed
%
% Output Arguments
%    instance : instance de cli_sound_speed
%
% See also help cli_sound_speed
% Authors : DCF
% VERSION  : $Id
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    05/02/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function instance = effacer_courbes (instance)

% ----------------------------------------------------------------------------
% ----------------------------------------------------------------------------
% ----------------------------------------------------------------------------
% Bloc try

% Maintenance Auto : try

  % --------------------------------------------------------------------------
  % Boucle de parcours des axes
  
  for i = 1:1:5
    
    % ------------------------------------------------------------------------
    % Acces au handle de l axe courant
    
    a = get_c (instance.c_axev, i) ;
    h = get (a, 'handle')          ;
    
    % ------------------------------------------------------------------------
    % Acces a l ensemble des courbes de l axe courant
    
    hh = get (h, 'Children') ;
    
    % ------------------------------------------------------------------------
    % Suppression des courbes
    
    delete (hh) ;
    
  end
  
% ----------------------------------------------------------------------------
% ----------------------------------------------------------------------------
% ----------------------------------------------------------------------------
% Bloc catch

% Maintenance Auto : catch
% Maintenance Auto :   err ('cli_sound_speed', 'effacer_courbes', '') ;
% Maintenance Auto : end
