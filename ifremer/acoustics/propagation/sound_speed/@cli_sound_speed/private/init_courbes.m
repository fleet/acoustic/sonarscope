% Initialisation des courbes(titre, label, ...)
% 
% this = init_courbes(this)
%
% Input Arguments
%    this : this de cli_sound_speed
%
% Output Arguments
%    this : this de cli_sound_speed
%
% See also cli_sound_speed
% Authors : DCF
% ----------------------------------------------------------------------------

function this = init_courbes(this)

% --------------------------------------------------------------------------
% Lecture de l'axe courant
% Attention : si aucun axe n existe, gca cree un axe !

currentAxe = get(this.hFigure, 'CurrentAxes');

%% Cr�ation de tous les axes

for i = 1:1:5
    this.c_axev = add(this.c_axev, i, clc_axe);
end

%% Repositionne l'axe courant

if ishandle(currentAxe)
    axes(currentAxe);
end
