% Mise a jour graphique de la frame caraibesFile
%
% this = maj_f_caraibes_file(this)
%
% Input Arguments
%    this : instance de cli_sound_speed
%
% Output Arguments
%    this : instance de cli_sound_speed
%
% See also cli_sound_speed
% Authors : DCF
% ----------------------------------------------------------------------------

function this = maj_f_caraibes_file(this)

hCaraibesFileName   = findobj(this.hFigure, 'Tag', 'caraibesFileName');
hNumeroProfil       = findobj(this.hFigure, 'Tag', 'CurrentProfile');
hBrowserCaraibes    = findobj(this.hFigure, 'Tag', 'BrowserCaraibes');
hProfileSelection   = findobj(this.hFigure, 'Tag', 'ProfileSelection');
hDate               = findobj(this.hFigure, 'Tag', 'Date');
hTime               = findobj(this.hFigure, 'Tag', 'Time');
hNumberOfProfiles   = findobj(this.hFigure, 'Tag', 'NumberOfProfiles');
hCurrentProfile     = findobj(this.hFigure, 'Tag', 'CurrentProfile');
hDataEditionCaraibes= findobj(this.hFigure, 'Tag', 'DataEditionCaraibes');

%% Traitement de la visibilité des frames

this.f_setOfProfils = set(this.f_setOfProfils, 'visible', 'off');
this.f_levitusFile  = set(this.f_levitusFile,  'visible', 'off');
this.f_caraibesFile = set(this.f_caraibesFile, 'visible', 'on');
this.f_modifiedData = set(this.f_modifiedData, 'visible', 'off');

%% Initialisation

% Gestion du fichier
% - doit etre different de vide
% - doit avoir l extension .cel

caraibesFileName = get(hCaraibesFileName, 'String');
if isempty(caraibesFileName)
    return
else
    if ~strcmp(caraibesFileName(end-3:end), '.cel')
         my_warndlg('Fichier Caraibes  = *.cel', 1);
        return
    end
end

% Intialisation de l'instance

this = set(this, 'CaraibesFileName', caraibesFileName);
this = set(this, 'CaraibesCurrentProfile', str2num(get(hNumeroProfil, 'String')));

% Intialisation et mise a jour de l IHM

if ~isempty(caraibesFileName)
    this = init(this);
    set(hBrowserCaraibes, 'Visible', 'on');
    set(hDataEditionCaraibes, 'Visible', 'on');
    if get(this, 'CaraibesNbProfiles') > 1
        set(hProfileSelection, 'Visible', 'on');
    else
        set(hProfileSelection, 'Visible', 'off');
    end
else
    set(hBrowserCaraibes, 'Visible', 'off');
    set(hProfileSelection, 'Visible', 'off');
    set(hDataEditionCaraibes, 'Visible', 'off');
end

caraibesData = get(this, 'CaraibesData');
caraibesCurrentProfile = get(this, 'CaraibesCurrentProfile');
Date         = caraibesData {caraibesCurrentProfile,1};
CaraibesTime = caraibesData {caraibesCurrentProfile,2};
set(hDate, 'String', dayIfr2str(Date))           ;
set(hTime, 'String', hourIfr2str(CaraibesTime))  ;
set(hNumberOfProfiles, 'String', get(this, 'CaraibesNbProfiles'));
set(hCurrentProfile, 'String', get(this, 'CaraibesCurrentProfile'));
