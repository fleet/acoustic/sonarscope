% Mise a jour graphique de la frame levitusFile
% 
% this = maj_f_levitus_file(this)
%
% Input Arguments
%    this : this de cli_sound_speed
%
% Output Arguments
%    this : this de cli_sound_speed
%
% See also cli_sound_speed
% Authors : DCF
% ----------------------------------------------------------------------------

function this = maj_f_levitus_file(this)

%% Locales

hMonth           = findobj(this.hFigure, 'Tag', 'Month');
hSeason          = findobj(this.hFigure, 'Tag', 'Season');
hMonthChoice     = findobj(this.hFigure, 'Tag', 'MonthChoice');
hSeasonChoice    = findobj(this.hFigure, 'Tag', 'SeasonChoice');
hGridSize        = findobj(this.hFigure, 'Tag', 'GridSizeChoice');
hLatitude        = findobj(this.hFigure, 'Tag', 'LatitudeLevitus');
hLongitude       = findobj(this.hFigure, 'Tag', 'LongitudeLevitus');
hAveragingChoice = findobj(this.hFigure, 'Tag', 'AveragingChoice');

%% Traitement de la visibilite des frames

this.f_setOfProfils = set(this.f_setOfProfils, 'visible', 'off');
this.f_levitusFile  = set(this.f_levitusFile,  'visible', 'on');
this.f_caraibesFile = set(this.f_caraibesFile, 'visible', 'off');
this.f_modifiedData = set(this.f_modifiedData, 'visible', 'off');

%% Initialisation

set(hAveragingChoice, 'Value', get(this, 'LevitusAveragingType'));

switch get(this, 'LevitusAveragingType')
    case 1 % Annuel
        set([hSeason, hMonth, hSeasonChoice, hMonthChoice], 'Visible', 'off');

    case 2 % Saisonnier
        set([hSeason, hSeasonChoice], 'Visible', 'on');
        set([hMonth, hMonthChoice], 'Visible', 'off');
        set(hSeasonChoice, 'Value', get(this, 'Season'));

    case 3 % Monthly
        set([hSeason, hSeasonChoice], 'Visible', 'off');
        set([hMonth, hMonthChoice], 'Visible', 'on');
        set(hMonthChoice, 'Value', get(this, 'LevitusMonth'));

    otherwise
        msg = 'cli_sound_speed::editobj, initLevitusFile, invalid value';
        my_warndlg(msg, 1);
end

set(hGridSize, 'Value', get(this, 'LevitusGridSize'));

set(hLatitude, 'String', lat2str(get(this, 'Latitude')));
set(hLongitude, 'String', lon2str(get(this, 'Longitude')));
this = cl_sound_speed_init(this);
