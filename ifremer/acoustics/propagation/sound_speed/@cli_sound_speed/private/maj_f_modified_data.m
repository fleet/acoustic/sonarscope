% Mise a jour graphique de la frame modifiedData
% 
% instance = maj_f_modified_data (instance)
%
% Input Arguments
%    instance : instance de cli_sound_speed
%
% Output Arguments
%    instance : instance de cli_sound_speed
%
% See also help cli_sound_speed
% Authors : DCF
% VERSION  : $Id
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    06/02/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function instance = maj_f_modified_data (instance)

% ----------------------------------------------------------------------------
% ----------------------------------------------------------------------------
% ----------------------------------------------------------------------------
% Bloc try

% Maintenance Auto : try

  % --------------------------------------------------------------------------
  % Locales
  
  % --------------------------------------------------------------------------
  % Traitement de la visibilite des frames
  
  instance.f_setOfProfils = set (instance.f_setOfProfils, 'visible', 'off') ;
  instance.f_levitusFile  = set (instance.f_levitusFile,  'visible', 'off') ;
  instance.f_caraibesFile = set (instance.f_caraibesFile, 'visible', 'off') ;
  instance.f_modifiedData = set (instance.f_modifiedData, 'visible', 'on')  ;
  
% ----------------------------------------------------------------------------
% ----------------------------------------------------------------------------
% ----------------------------------------------------------------------------
% Bloc catch

% Maintenance Auto : catch
% Maintenance Auto :   err ('cli_sound_speed', 'maj_f_caraibes_file', '') ;
% Maintenance Auto : end
