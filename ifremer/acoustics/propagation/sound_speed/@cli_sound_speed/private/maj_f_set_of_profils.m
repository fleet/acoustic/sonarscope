% Mise a jour graphique de la frame setOfProfils
% 
% this = maj_f_set_of_profils(this)
%
% Input Arguments
%    this : this de cli_sound_speed
%
% Output Arguments
%    this : this de cli_sound_speed
%
% See also cli_sound_speed
% Authors : DCF
% ----------------------------------------------------------------------------

function this = maj_f_set_of_profils(this)

%% Locales

hOceanChoice = findobj(this.hFigure, 'Tag', 'OceanChoice');
hMonthChoice = findobj(this.hFigure, 'Tag', 'MonthChoice');

%% Initialisation

set(hOceanChoice, 'Value', get(this, 'Ocean'));
set(hMonthChoice, 'Value', get(this, 'LevitusMonth'));
% this = init(this);
this = cl_sound_speed_init(this);

%% Traitement de la visibilité des frames

this.f_setOfProfils = set(this.f_setOfProfils, 'visible', 'on');
this.f_levitusFile  = set(this.f_levitusFile,  'visible', 'off');
this.f_caraibesFile = set(this.f_caraibesFile, 'visible', 'off');
this.f_modifiedData = set(this.f_modifiedData, 'visible', 'off');
