% Mise a jour graphique des frames
% 
% this = maj_frames(this)
%
% Input Arguments
%    this : this de cli_sound_speed
%
% Output Arguments
%    this : this de cli_sound_speed
%
% See also cli_sound_speed
% Authors : DCF
% ----------------------------------------------------------------------------

function this = maj_frames(this)

%% Locales

h = findobj(this.hFigure, 'Tag', 'DataIncoming');

%% Initialisation de la source de donnees

this = set(this, 'Incoming', get(h, 'Value'));

%% Choix selon la source des donnees

switch get(h, 'Value')
    case 1  % set of profiles
        this = maj_f_set_of_profils(this);
        this = tracer_courbes(this);
        this = placer_courbes(this);

    case 2  % levitus file
        this = maj_f_levitus_file(this);
        this = tracer_courbes(this);
        this = placer_courbes(this);

    case 3  % caraibes file
        this = maj_f_caraibes_file(this);
        caraibesFileName = get(this, 'CaraibesFileName');
        if isempty(caraibesFileName) || strcmp(caraibesFileName, '')
            this = effacer_courbes(this);
        else
            this = tracer_courbes(this);
            this = placer_courbes(this);
        end

    case 4  % modified data
        this = maj_f_modified_data(this);
        this = tracer_courbes(this);
        this = placer_courbes(this);

    otherwise
        my_warndlg('cli_sound_speed/maj_frames : Invalid incoming', 0);
end
