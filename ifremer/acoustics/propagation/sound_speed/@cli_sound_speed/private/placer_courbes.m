% Positionnement des courbes
% 
% this = placer_courbes (this)
%
% Input Arguments
%    this : instance de cli_sound_speed
%
% Output Arguments
%    this : instance de cli_sound_speed
%
% See also cli_sound_speed
% Authors : DCF
% ----------------------------------------------------------------------------

function this = placer_courbes (this)

%% Locales

indice = 0;
hh    = findobj(this.hFigure, 'Tag', 'Salinity');
hh(2) = findobj(this.hFigure, 'Tag', 'Temperature');
hh(3) = findobj(this.hFigure, 'Tag', 'SoundSpeed');
hh(4) = findobj(this.hFigure, 'Tag', 'Absorption');
hh(5) = findobj(this.hFigure, 'Tag', 'Ray');

%% Boucle de parcours des axes

for k=1:5
    
    %% Doit on supprimer le k eme axe (suppression virtuelle)
    
    if get(hh(k), 'Value')
        indice = indice + 1;
        
        if is_collapse(this.c_axev, k)
            this.c_axev = collapse(this.c_axev, k, 'off');
        end
        
        a = get_c(this.c_axev, k);
        h = get(a, 'handle');
        if indice == 1
            set(h, 'YTickLabelMode', 'auto');
            visibilite = 'on';
        else
            set(h, 'YTickLabel', []);
            visibilite = 'off';
        end
        h = get(h, 'YLabel');
        set(h, 'Visible', visibilite);
        
    else
        if ~is_collapse(this.c_axev, k)
            this.c_axev = collapse(this.c_axev, k, 'on');
        end
    end
end
