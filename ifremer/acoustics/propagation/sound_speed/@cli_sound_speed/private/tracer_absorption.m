% Trace les courbes d absorption
%
% this = tracer_absorption(this)
%
% Input Arguments
%    this : instance de cli_sound_speed
%
% Output Arguments
%    this : instance de cli_sound_speed
%
% See also cli_sound_speed
% Authors : DCF
% ----------------------------------------------------------------------------

function this = tracer_absorption(this)

%% Lecture de l axe courant
% Attention : si aucun axe n existe, gca cr�e un axe !

currentAxe = get(this.hFigure, 'CurrentAxes');

%% Acces a l axe de trace de la salinite

a = get_c(this.c_axev, 4);
h = get(a, 'handle');
if ishandle(h)

    %% Efface le contenu pr�cedent

    hh = get(h, 'Children');
    delete(hh);

    %% Lecture des donnees a afficher

    depth             = get(this, 'Depth');
    localAbsorption   = get(this, 'LocalAbsorption');
    averageAbsorption = get(this, 'AveragedAbsorption');
    
    if isempty(localAbsorption)
        return
    end

    %% Trac� de la courbe

    axes(h);
    hPlot = PlotUtils.createSScPlot(localAbsorption, depth);
    hold on;
    hPlot(2) = PlotUtils.createSScPlot(averageAbsorption, depth, 'r');
    hold off;
    grid on;

    %% Traitement de la visibilite

    if is_collapse(this.c_axev, 4)
        visibilite = 'off';
    else
        visibilite = 'on';
    end
    set(h, 'Visible', visibilite);
    set(hPlot, 'Visible', visibilite);

else
    my_warndlg('cli_sound_speed/tracer_absorption : Invalid handle', 1);
end

%% Repositionne l axe courant

if ishandle(currentAxe)
    axes(currentAxe);
end
