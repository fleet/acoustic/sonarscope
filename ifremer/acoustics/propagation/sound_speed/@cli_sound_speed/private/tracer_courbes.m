% Trace les differentes courbes
% 
% this = tracer_courbes(this)
%
% Input Arguments
%    this : instance de cli_sound_speed
%
% Output Arguments
%    this : instance de cli_sound_speed
%
% See also cli_sound_speed
% Authors : DCF
% ----------------------------------------------------------------------------

function this = tracer_courbes(this)

titre = [];
titre {end+1} = 'Salinity';
titre {end+1} = 'Temperature';
titre {end+1} = 'Sound Speed';
titre {end+1} = 'Absorption';
titre {end+1} = 'Ray';

x_label = []  ;
x_label {end+1} = '(p.p.t.)';
x_label {end+1} = '(�C)';
x_label {end+1} = '(m/s)';
x_label {end+1} = '(dB/km)';
x_label {end+1} = '';

y_label = [];
y_label {end+1} = 'Depth (m)';
y_label {end+1} = 'Depth (m)';
y_label {end+1} = 'Depth (m)';
y_label {end+1} = 'Depth (m)';
y_label {end+1} = '';

%% Acces � chaque axe, trace l� ou les courbes correspondantes

this = tracer_salinity(this);
this = tracer_temperature(this);
this = tracer_sound_speed(this);
this = tracer_absorption(this);
this = tracer_ray(this);

%% Lecture de l axe courant
% Attention : si aucun axe n existe, gca cree un axe !

currentAxe = get(this.hFigure, 'CurrentAxes');

%% Mise � jour des labels et titres

for k=1:5
    a = get_c(this.c_axev, k);
    h = get(a, 'handle');
    if ishandle(h)
        axes(h);
        title(titre {k});
        xlabel(x_label {k});
        ylabel(y_label {k});
    else
        my_warndlg('cli_sound_speed/init_courbes : Invalid handle', 0);
    end
end

%% Repositionne l axe courant

if ishandle(currentAxe)
    axes(currentAxe)
end
