% Trace les courbes des rayons
% 
% this = tracer_ray (this)
%
% Input Arguments
%    this : instance de cli_sound_speed
%
% Output Arguments
%    this : instance de cli_sound_speed
%
% See also cli_sound_speed
% Authors : DCF
% ----------------------------------------------------------------------------

function this = tracer_ray(this)

%% Lecture de l axe courant
% Attention : si aucun axe n existe, gca cree un axe !

currentAxe = get(this.hFigure, 'CurrentAxes');

%% Acces � l'axe de trac� de la salinit�

a = get_c(this.c_axev, 5);
h = get(a, 'handle');
if ishandle(h)
    
    %% Efface le contenu precedent
    
    hh = get(h, 'Children');
    delete(hh);
    
    %% Lecture des donnees a afficher
    
    depth       = get(this, 'Depth');
    soundSpeed  = get(this, 'SoundSpeed');
    RayAngles   = get(this, 'RayAngles');
    
    %% Ajout JMA 26/2/02 pour traiter un bug prevenant d'un profil Caraibes
    sub = find(~isnan(depth));
    depth = depth(sub);
    soundSpeed = soundSpeed(sub);
    % --------------------------------------------------------------------
    
    %     [X, TH, t] = TrajetRayon(depth, soundSpeed, RayAngles);
    Z = linspace(0, min(depth), 1000);
    [X, TH, t] = TrajetRayon(depth, soundSpeed, RayAngles, 'Z', Z); %#ok<ASGLU>
    X = squeeze(X);
    
    %% Trac� de la courbe
    
    axes(h);
    hPlot = PlotUtils.createSScPlot(X, Z);
    grid on
    
    %% Traitement de la visibilite
    
    if is_collapse(this.c_axev, 5)
        visibilite = 'off';
    else
        visibilite = 'on';
    end
    set(h, 'Visible', visibilite);
    set(hPlot, 'Visible', visibilite);
    
else
    my_warndlg('cli_sound_speed/tracer_ray : Invalid handle', 0);
end

%% Repositionne l axe courant

if ishandle(currentAxe)
    axes(currentAxe);
end
