% Trace la courbe de salinite
% 
% this = tracer_salinity(this)
%
% Input Arguments
%    this : instance de cli_sound_speed
%
% Output Arguments
%    this : instance de cli_sound_speed
%
% See also cli_sound_speed
% Authors : DCF
% ----------------------------------------------------------------------------

function this = tracer_salinity(this)

%% Lecture de l'axe courant
% Attention : si aucun axe n'existe, gca cr�e un axe !

currentAxe = get(this.hFigure, 'CurrentAxes');

%% Acces � l'axe de trac� de la salinit�

a = get_c(this.c_axev, 1);
h = get(a, 'handle');
if ishandle(h)
    
    %% Efface le contenu pr�cedent
    
    hh = get(h, 'Children');
    delete(hh);
    
    %% Lecture des donnees a afficher
    
    depth    = get(this, 'Depth');
    salinity = get(this, 'Salinity');
    
    %% Trac� de la courbe
    
    axes(h);
    hPlot = PlotUtils.createSScPlot(salinity, depth);
    grid on
    
    %% Traitement de la visibilite
    
    if is_collapse(this.c_axev, 1)
        visibilite = 'off';
    else
        visibilite = 'on';
    end
    set(h, 'Visible', visibilite);
    set(hPlot, 'Visible', visibilite);
    
else
    my_warndlg('cli_sound_speed/tracer_salinity : Invalid handle', 0);
end

%% Repositionne l'axe courant

if ishandle(currentAxe)
    % axes(currentAxe); ligne suivante pr�conis�e car plus rapide.
    set(this.hFigure, 'CurrentAxes', currentAxe);
end
