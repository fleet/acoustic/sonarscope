% Trace la courbe de vitesse du son
% 
% this = tracer_sound_speed(this)
%
% Input Arguments
%    this : instance de cli_sound_speed
%
% Output Arguments
%    this : instance de cli_sound_speed
%
% See also cli_sound_speed
% Authors : DCF
% ----------------------------------------------------------------------------

function this = tracer_sound_speed(this)

hPlot = [];

%% Lecture de l'axe courant
% Attention : si aucun axe n'existe, gca cr'e un axe !

currentAxe = get(this.hFigure, 'CurrentAxes');

%% Acces � l'axe de trac� de la salinit�

a = get_c(this.c_axev, 3);
h = get(a, 'handle');
if ishandle(h)
    
    %% Efface le contenu precedent
    
    hh = get(h, 'Children');
    delete(hh);
    
    %% Lecture des donn�es � afficher
    
    depth       = get(this, 'Depth');
    soundSpeed  = get(this, 'SoundSpeed');
    cSoundSpeed = get(this, 'CaraibesSoundSpeed');
    
    %% Trac� de la courbe
    
    axes(h);
    set(h, 'Visible', 'On') % Rajout� par JMA le 02/10/2015
    if ~isempty(cSoundSpeed)
        if ~isequal(cSoundSpeed, soundSpeed)
            hPlot = PlotUtils.createSScPlot(cSoundSpeed, depth, 'Color', [0.7 0.7 0.7]);
        end
    end
    hold on
    hPlot = [hPlot, PlotUtils.createSScPlot(soundSpeed, depth)];
    hold off
    grid on
    
    %% Traitement de la visibilit�
    
    if is_collapse(this.c_axev, 3)
        visibilite = 'off';
    else
        visibilite = 'on';
    end
    set(h, 'Visible', visibilite);
    set(hPlot, 'Visible', visibilite);
    
else
    my_warndlg('cli_sound_speed/tracer_sound_speed : Invalid handle', 0);
end

%% Repositionne l axe courant

if ishandle(currentAxe)
    axes(currentAxe);
end
