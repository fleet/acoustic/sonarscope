% Trace la courbe de temperature
%
% this = tracer_temperature(this)
%
% Input Arguments
%    this : instance de cli_sound_speed
%
% Output Arguments
%    this : instance de cli_sound_speed
%
% See also cli_sound_speed
% Authors : DCF
% ----------------------------------------------------------------------------

function this = tracer_temperature(this)

%% Lecture de l axe courant
% Attention : si aucun axe n'existe, gca cr�e un axe !

currentAxe = get(this.hFigure, 'CurrentAxes');

%% Acces � l'axe de trac� de la salinit�

a = get_c(this.c_axev, 2);
h = get(a, 'handle');
if ishandle(h)

    %% Efface le contenu prec�dent

    hh = get(h, 'Children');
    delete(hh);

    %% Lecture des donn�es � afficher

    depth       = get(this, 'Depth');
    temperature = get(this, 'Temperature');

    %% Trac� de la courbe

    axes(h); %ligne suivante pr�conis�e car plus rapide.
%     set(this.hFigure, 'CurrentAxes', h); % R2011b
    hPlot = PlotUtils.createSScPlot(temperature, depth);
    grid on

    %% Traitement de la visibilit�

    if is_collapse(this.c_axev, 2)
        visibilite = 'off';
    else
        visibilite = 'on';
    end
    set(h, 'Visible', visibilite);
    set(hPlot, 'Visible', visibilite);

else
    my_warndlg('cli_sound_speed/tracer_temperature : Invalid handle', 0);
end

%% Repositionne l'axe courant

if ishandle(currentAxe)
    axes(currentAxe); % ligne suivante pr�conis�e car plus rapide.
%     set(this.hFigure, 'CurrentAxes', currentAxe);
end
