% Reaction a un changement d affichage de l'absorption
% 
% this = traiter_absorption(this)
%
% Input Arguments
%    this : instance de cli_sound_speed
%
% Output Arguments
%    this : instance de cli_sound_speed
%
% See also cli_sound_speed
% Authors : DCF
% ----------------------------------------------------------------------------


function this = traiter_absorption(this)

this = placer_courbes(this);
