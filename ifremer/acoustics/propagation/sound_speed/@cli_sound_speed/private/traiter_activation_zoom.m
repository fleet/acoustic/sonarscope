% Reaction a une activation du zoom
% 
% instance = traiter_activation_zoom (instance, msg)
%
% Input Arguments
%    instance : instance de cli_sound_speed
%    msg      : message associe
%
% Output Arguments
%    instance : instance de cli_sound_speed
%
% See also help cli_sound_speed
% Authors : DCF
% VERSION  : $Id
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    05/02/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function instance = traiter_activation_zoom (instance, msg)

% ----------------------------------------------------------------------------
% ----------------------------------------------------------------------------
% ----------------------------------------------------------------------------
% Bloc try

% Maintenance Auto : try

  % --------------------------------------------------------------------------
  % Traitement
  
  if (strcmp (msg, 'In'))
    instance.zoom = set (instance.zoom, 'action', 'In') ;
  else
    instance.zoom = set (instance.zoom, 'action', 'Out') ;
  end
  if (get (instance.zoom, 'isOn'))
    instance.zoom = desactiver (instance.zoom) ;
  else
    instance.zoom = activer (instance.zoom) ;
  end

% ----------------------------------------------------------------------------
% ----------------------------------------------------------------------------
% ----------------------------------------------------------------------------
% Bloc catch

% Maintenance Auto : catch
% Maintenance Auto :   err ('cli_sound_speed', 'traiter_activation_zoom', '') ;
% Maintenance Auto : end
