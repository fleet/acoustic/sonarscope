% Reaction a la demande de prise en compte des choix
% 
% this = traiter_apply(this)
%
% Input Arguments
%    this : instance de cli_sound_speed
%
% Output Arguments
%    this : instance de cli_sound_speed
%
% See also cli_sound_speed
% Authors : DCF
% VERSION  : $Id
% -------------------------------------------------------------------------

function this = traiter_apply(this)

% -----------------------------------------------
% Suppression du moyen de valider la modification

set(gcbo, 'Visible', 'off');

% ---------------------------------------------------------
% Traitement de la latitude et de la longitude pour levitus

if get(this, 'Incoming') == 2

    hLon = findobj(this.hFigure, 'Tag', 'LongitudeLevitus');
    hLat = findobj(this.hFigure, 'Tag', 'LatitudeLevitus');
    hEarth = findobj(this.hFigure, 'Tag', 'EarthEdition');

    lon = str2lon(get(hLon, 'String'));
    lat = str2lat(get(hLat, 'String'));

    if ~isnan(lon) && ~isnan(lat)
        set(this, 'Latitude', lat, 'Longitude', lon);
    end

    set(hLon, 'String', lon2str(get(this, 'Longitude')));
    set(hLat, 'String', lat2str(get(this, 'Latitude')));
    set(hEarth, 'Visible', 'on');

else

    % -------------------------
    % Mise a jour de l'instance

    this = init(this);

end

% -----------------------
% Mise a jour des courbes

this = tracer_courbes(this);
this = placer_courbes(this);
