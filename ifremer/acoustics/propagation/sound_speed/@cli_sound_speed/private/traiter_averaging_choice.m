% Reaction a un changement du choix de la saison
% 
% this = traiter_averaging_choice(this)
%
% Input Arguments
%    this : instance de cli_sound_speed
%
% Output Arguments
%    this : instance de cli_sound_speed
%
% See also help cli_sound_speed
% Authors : DCF
% -------------------------------------------------------------------------

function this = traiter_averaging_choice(this)

h             = gcbo;
hMonth        = findobj(this.hFigure, 'Tag', 'Month');
hSeason       = findobj(this.hFigure, 'Tag', 'Season');
hMonthChoice  = findobj(this.hFigure, 'Tag', 'MonthChoice');
hSeasonChoice = findobj(this.hFigure, 'Tag', 'SeasonChoice');
hApply        = findobj(this.hFigure, 'Tag', 'Apply');

%% Choix de l'utilisateur

valeur = get(h, 'Value');
this = set(this, 'LevitusAveragingType', valeur);

switch valeur
    case 1 % Annuel
        set([hSeason, hMonth, hSeasonChoice, hMonthChoice], 'Visible', 'off');

    case 2 % Saisonnier
        set([hSeason, hSeasonChoice], 'Visible', 'on');
        set([hMonth, hMonthChoice], 'Visible', 'off');
        valeur = get(hSeasonChoice, 'value');
        this = set(this, 'Season', valeur);

    case 3 % Monthly
        set([hSeason, hSeasonChoice], 'Visible', 'off');
        set([hMonth, hMonthChoice], 'Visible', 'on');
        valeur = get(hMonthChoice, 'value');
        this = set(this, 'LevitusMonth', valeur);

    otherwise
        my_warndlg('cli_sound_speed/traiter_averaging_choice : Invalid value', 1);
end

%% Efface les courbes obsoletes

this = effacer_courbes(this);

%% Mise en place du moyen de valider la modification

set(hApply, 'Visible', 'on');
