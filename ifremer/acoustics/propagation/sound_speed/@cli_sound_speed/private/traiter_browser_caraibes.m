% Reaction a une demande de choix de fichier caraibes
% 
% this = traiter_browser_caraibes(this)
%
% Input Arguments
%    this : instance de cli_sound_speed
%
% Output Arguments
%    this : instance de cli_sound_speed
%
% See also cli_sound_speed
% Authors : DCF
% ----------------------------------------------------------------------------

function this = traiter_browser_caraibes(this)

%% Locales

hCaraibesFileName = findobj(this.hFigure, 'Tag', 'caraibesFileName');

%% Lance le browser et renvoie le nom de fichier selectionne

CaraibesFileName = fileparts(getNomFicDatabase('BATHYCEL_ZTS.cel'));
this = desactiver_frames(this);
CaraibesFileName = SelectionFichier(fullfile(CaraibesFileName, '*.cel'));
this = activer_frames(this);

%% Analyse du resultat

if ~isempty(CaraibesFileName)
    set(hCaraibesFileName, 'String', CaraibesFileName);
    this = maj_f_caraibes_file(this);
    this = tracer_courbes(this);
    this = placer_courbes(this);
else
    my_warndlg('cli_sound_speed/traiter_browser_caraibes : Aucun fichier selectionne', 1);
end
