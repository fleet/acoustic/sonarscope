% Reaction a un changement du nom du fichier caraibes
% 
% this = traiter_caraibes_file_name(this, msg)
%
% Input Arguments
%    this : instance de cli_sound_speed
%    msg  : message associe
%
% Output Arguments
%    this : instance de cli_sound_speed
%
% See also help cli_sound_speed
% Authors : DCF
% VERSION  : $Id
% ----------------------------------------------------------------------------

function this = traiter_caraibes_file_name(this, msg)

% -------
% Locales

hCaraibesFileName = findobj(this.hFigure, 'Tag', msg);

% -------------------------
% Lecture du nom du fichier

CaraibesFileName = get(hCaraibesFileName, 'String');

% -------------------
% Analyse du resultat

if ~isempty(CaraibesFileName)
    this = maj_f_caraibes_file(this);
    this = tracer_courbes(this);
    this = placer_courbes(this);
end
