% Reaction a un changement de profile courant
% 
% this = traiter_current_profile(this, msg)
%
% Input Arguments
%    this : instance de cli_sound_speed
%    msg      : message associe
%
% Output Arguments
%    this : instance de cli_sound_speed
%
% See also cli_sound_speed
% Authors : DCF
% VERSION  : $Id
% ----------------------------------------------------------------------------

function this = traiter_current_profile(this, msg)

% --------
% Locales

hCurrentProfile = findobj(this.hFigure, 'Tag', msg);

% -------------------------
% Mise a jour de l'instance

currentProfil = str2double(get(hCurrentProfile, 'String'));
this = set(this, 'CaraibesCurrentProfile', currentProfil);

% -------------------
% Analyse du resultat

if ~isempty(get(this, 'CaraibesFileName'))
    this = maj_f_caraibes_file(this);
    this = tracer_courbes(this);
    this = placer_courbes(this);
end
