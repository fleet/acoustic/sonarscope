% Reaction a une demande d'edition des donnees
% 
% this = traiter_data_edition(this)
%
% Input Arguments
%    this : instance de cli_sound_speed
%
% Output Arguments
%    this : instance de cli_sound_speed
%
% See also cli_sound_speed
% Authors : DCF
% VERSION  : $Id
% ----------------------------------------------------------------------------

function this = traiter_data_edition(this)

my_warndlg('This function is not available anymore.', 1);

return

% --------
% Locales

h = findobj(this.hFigure, 'Tag', 'DataIncoming');

% --------------------------
% Inactivation de la fenetre

this = desactiver_frames(this);

% ------------------------------------
% Acces aux donnees avant modification

[oSalinity, oTemperature] = get(this, 'Salinity', 'Temperature');

% -------------------
% Edition des donnees

edit_data(this);

% ------------------------
% Activation de la fenetre

this = activer_frames(this);

% ------------------------------------
% Acces aux donnees apres modification

[nSalinity, nTemperature] = get(this, 'Salinity', 'Temperature');

% -----------------------------------------------------
% Si les donnees sont modifiees, changement de Incoming

if ~isequal(oSalinity, nSalinity) || ~isequal(oTemperature, nTemperature)
    set(h, 'Value', 4);
    this = maj_frames(this);
end
