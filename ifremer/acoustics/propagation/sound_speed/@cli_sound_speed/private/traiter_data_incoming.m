% Reaction a un changement de donnees en entree
% 
% instance = traiter_data_incoming(this)
%
% Input Arguments
%    this : instance de cli_sound_speed
%
% Output Arguments
%    this : instance de cli_sound_speed
%
% See also cli_sound_speed
% Authors : DCF
% ----------------------------------------------------------------------------


function this = traiter_data_incoming(this)

this = maj_frames(this);
