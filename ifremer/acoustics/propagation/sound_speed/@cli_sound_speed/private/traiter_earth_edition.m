% Reaction a la demande d edition de la carte terrestre
% 
% this = traiter_earth_edition(this)
%
% Input Arguments
%    this : instance de cli_sound_speed
%
% Output Arguments
%    this : instance de cli_sound_speed
%
% See also cli_sound_speed
% Authors : DCF
% ----------------------------------------------------------------------------

function this = traiter_earth_edition(this)

hApply = findobj(this.hFigure, 'Tag', 'Apply');
hLon   = findobj(this.hFigure, 'Tag', 'LongitudeLevitus');
hLat   = findobj(this.hFigure, 'Tag', 'LatitudeLevitus');
Lon = get(this, 'Longitude');
Lat = get(this, 'Latitude');

if isnan(Lon)
    Lon = 0;
end
if isnan(Lat)
    Lat = 0;
end

%% Edition

this = desactiver_frames(this);

nomFic = getNomFicDatabase('TerreEtopo.mat');
Z = loadmat(nomFic, 'nomVar', 'Image');
Info = loadmat(nomFic, 'nomVar', 'Info');
Carto = cl_carto('Ellipsoide.Type', 11, 'Projection.Type', 2, ...
    'Projection.Mercator.long_merid_orig', my_nanmean(Info.x), ...
    'Projection.Mercator.lat_ech_cons',    my_nanmean(Info.y));
a = cl_image('Image', Z, 'X', Info.x, 'Y', Info.y, 'Name', 'Earth', ...
    'ColormapIndex', 19, 'CLim', [-6000 6000], ...
    'GeometryType', cl_image.indGeometryType('LatLong'), ...
    'DataType',     cl_image.indDataType('Bathymetry'), ...
    'XUnit', 'deg', 'YUnit', 'deg', 'Unit', 'm', ...
    'Comments', 'Keep it blue', 'Carto', Carto);
b = cli_image('Images', a, 'visuProfilX', 1, 'visuProfilY', 1, 'profilY.Type', 1, ...
    'Point', [Lon Lat]);
b = SonarScope(b);

Point = get(b, 'Point');
indImage = get(b, 'indImage');
Lon = Point(indImage,1);
Lat = Point(indImage,2);

this = activer_frames(this);

%% Mise a jour de l'instance

this = set(this, 'Longitude', Lon, 'Latitude', Lat);
this = set(this);

%% Mise a jour IHM

set(hLon, 'String', lon2str(get(this, 'Longitude')));
set(hLat, 'String', lat2str(get(this, 'Latitude')));

%% Efface les courbes obsoletes

this = effacer_courbes(this);

%% Mise en place du moyen de valider la modification

set(hApply, 'Visible', 'on');
