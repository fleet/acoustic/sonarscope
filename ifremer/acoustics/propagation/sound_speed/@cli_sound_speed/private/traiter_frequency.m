% Reaction a un changement d affichage de l absorption
% 
% this = traiter_frequency(this)
%
% Input Arguments
%    this : this de cli_sound_speed
%
% Output Arguments
%    this : this de cli_sound_speed
%
% See also cli_sound_speed
% Authors : DCF
% ----------------------------------------------------------------------------

function this = traiter_frequency(this)

h = gcbo;

% ----------------------
% Changement de l'ocean

this = set(this, 'Frequency', str2num(get(h, 'String')));

% ----------------------------
% Initialisation de l'instance

this = cl_sound_speed_init(this);

% ----------------------
% Mise a jour des traces

this = tracer_courbes(this);
