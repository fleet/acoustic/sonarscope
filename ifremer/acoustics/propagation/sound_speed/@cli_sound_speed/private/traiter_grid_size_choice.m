% Reaction a un changement du maillage geographique
% 
% this = traiter_grid_size_choice(this)
%
% Input Arguments
%    this : instance de cli_sound_speed
%
% Output Arguments
%    this : instance de cli_sound_speed
%
% See also cli_sound_speed
% Authors : DCF
% ----------------------------------------------------------------------------

function this = traiter_grid_size_choice(this)

h      = gcbo;
valeur = get(h, 'Value');
hApply = findobj(this.hFigure, 'Tag', 'Apply');

% -------------------------
% Mise a jour de l'instance

this = set(this, 'LevitusGridSize', valeur);

% ------------------
% Efface les courbes

this = effacer_courbes(this);

% -------------------------------------------------
% Mise en place du moyen de valider la modification

set(hApply, 'Visible', 'on');
