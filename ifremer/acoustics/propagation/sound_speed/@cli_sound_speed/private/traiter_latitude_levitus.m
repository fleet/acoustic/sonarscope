% Reaction a un changement de latitude pour levitus
% 
% this = traiter_latitude_levitus(this)
%
% Input Arguments
%    this : instance de cli_sound_speed
%
% Output Arguments
%    this : instance de cli_sound_speed
%
% See also cli_sound_speed
% Authors : DCF
% ----------------------------------------------------------------------------

function this = traiter_latitude_levitus(this)

hLat   = gcbo;
hApply = findobj(this.hFigure, 'Tag', 'Apply');
hEarth = findobj(this.hFigure, 'Tag', 'EarthEdition');

% --------------------
% Reformatte la chaine

set(hLat, 'String', lat2str(str2lat(get(hLat, 'String'))));

% --------------------------
% Rend la terre inaccessible

set(hEarth, 'Visible', 'off');

% ------------------
% Efface les courbes

this = effacer_courbes(this);

% -------------------------------------------------
% Mise en place du moyen de valider la modification

set(hApply, 'Visible', 'on');
