% Reaction a un changement de longitude pour levitus
% 
% this = traiter_longitude_levitus(this)
%
% Input Arguments
%    this : instance de cli_sound_speed
%    msg  : message associe
%
% Output Arguments
%    this : instance de cli_sound_speed
%
% See also cli_sound_speed
% Authors : DCF
% ----------------------------------------------------------------------------

function this = traiter_longitude_levitus(this)

hLon   = gcbo;
hApply = findobj(this.hFigure, 'Tag', 'Apply');
hEarth = findobj(this.hFigure, 'Tag', 'EarthEdition');

% --------------------
% Reformatte la chaine

set(hLon, 'String', lon2str(str2lon(get(hLon, 'String'))));

% --------------------------
% Rend la terre inaccessible

set(hEarth, 'Visible', 'off');

% ------------------
% Efface les courbes

this = effacer_courbes(this);

% -------------------------------------------------
% Mise en place du moyen de valider la modification

set(hApply, 'Visible', 'on');
