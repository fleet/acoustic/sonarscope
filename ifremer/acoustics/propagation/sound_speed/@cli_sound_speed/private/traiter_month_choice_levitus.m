% Reaction au changement de mois pour levitus
% 
% this = traiter_month_choice_levitus(this, msg)
%
% Input Arguments
%    this : instance de cli_sound_speed
%    msg  : message associe
%
% Output Arguments
%    this : instance de cli_sound_speed
%
% See also help cli_sound_speed
% Authors : DCF
% ----------------------------------------------------------------------------

function this = traiter_month_choice_levitus(this, msg)

% -------
% Locales

hMonth  = findobj(this.hFigure, 'Tag', msg);
hApply  = findobj(this.hFigure, 'Tag', 'Apply');

% -------------------------
% Mise a jour de l'instance

this = set(this, 'LevitusMonth', get(hMonth, 'Value'));

% ----------------------------
% Efface les courbes obsoletes

this = effacer_courbes(this);

% -------------------------------------------------
% Mise en place du moyen de valider la modification

set(hApply, 'Visible', 'on');
