% Reaction a un changement d'ocean
% 
% this = traiter_ocean_choice(this)
%
% Input Arguments
%    this : instance de cli_sound_speed
%    msg      : message associe
%
% Output Arguments
%    this : instance de cli_sound_speed
%
% See also cli_sound_speed
% Authors : DCF
% ----------------------------------------------------------------------------

function this = traiter_ocean_choice(this)

h = gcbo;

% ---------------------
% Changement de l'ocean

this = set(this, 'Ocean', get(h, 'Value'));

% ----------------------------
% Initialisation de l'instance

% this = init(this); % Comment� le 16/03/2010 cat m�thone init pas trouv�e

% ----------------------
% Mise a jour des traces

this = tracer_courbes(this);
this = placer_courbes(this);
