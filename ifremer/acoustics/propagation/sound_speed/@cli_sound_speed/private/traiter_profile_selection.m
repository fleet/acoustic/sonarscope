% Reaction a une demande d edition des profils
% 
% this = traiter_profile_selection(this)
%
% Input Arguments
%    this : instance de cli_sound_speed
%
% Output Arguments
%    this : instance de cli_sound_speed
%
% See also cli_sound_speed
% Authors : DCF
% ----------------------------------------------------------------------------

function this = traiter_profile_selection(this)

%% Locales

hCurrentProfile = findobj(this.hFigure, 'Tag', 'CurrentProfile');

%% Test existance du nom de fichier

if isempty(get(this, 'CaraibesFileName'))
    return
end

%% Edition

this = desactiver_frames(this);
edit_cl_cel_fichier(this);
this = activer_frames(this);

%% Mise a jour de l IHM

set(hCurrentProfile, 'String', get(this, 'CaraibesCurrentProfile'));

%% Analyse du resultat

if ~isempty(get(this, 'CaraibesFileName'))
    this = maj_f_caraibes_file(this);
    this = tracer_courbes(this);
    this = placer_courbes(this);
end
