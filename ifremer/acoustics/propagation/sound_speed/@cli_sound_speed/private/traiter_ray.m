% Reaction a un changement d affichage du trace des rayons
% 
% this = traiter_ray(this)
%
% Input Arguments
%    this : instance de cli_sound_speed
%
% Output Arguments
%    this : instance de cli_sound_speed
%
% See also help cli_sound_speed
% Authors : DCF
% ----------------------------------------------------------------------------

function this = traiter_ray(this)

this = placer_courbes(this);
