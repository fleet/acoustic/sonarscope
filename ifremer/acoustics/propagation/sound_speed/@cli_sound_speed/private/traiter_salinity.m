% Reaction a un changement d affichage de la salinite
% 
% this = traiter_salinity(this)
%
% Input Arguments
%    this : instance de cli_sound_speed
%
% Output Arguments
%    this : instance de cli_sound_speed
%
% See also cli_sound_speed
% Authors : DCF
% ----------------------------------------------------------------------------

function this = traiter_salinity(this)
this = placer_courbes(this);
