% Reaction a la demande de sauvegarde des modifications
% 
% instance = traiter_save (instance)
%
% Input Arguments
%    instance : instance de cli_sound_speed
%
% Output Arguments
%    instance : instance de cli_sound_speed
%
% See also help cli_sound_speed
% Authors : DCF
% VERSION  : $Id
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    05/02/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function instance = traiter_save (instance)

% ----------------------------------------------------------------------------
% ----------------------------------------------------------------------------
% ----------------------------------------------------------------------------
% Bloc try

% Maintenance Auto : try

  % --------------------------------------------------------------------------
  % Sauvegarde de l instance
  
  savedObj2handle (instance, instance.hFigure) ;
  
  % --------------------------------------------------------------------------
  % Casse la boucle d arret
  
  uiresume (instance.hFigure) ;
  
% ----------------------------------------------------------------------------
% ----------------------------------------------------------------------------
% ----------------------------------------------------------------------------
% Bloc catch

% Maintenance Auto : catch
% Maintenance Auto :   err ('cli_sound_speed', 'traiter_save', '') ;
% Maintenance Auto : end
