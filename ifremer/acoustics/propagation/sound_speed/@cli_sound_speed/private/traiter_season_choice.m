% Reaction a un changement de saison
% 
% this = traiter_season_choice(this)
%
% Input Arguments
%    this : this de cli_sound_speed
%
% Output Arguments
%    this : this de cli_sound_speed
%
% See also cli_sound_speed
% Authors : DCF
% ----------------------------------------------------------------------------

function this = traiter_season_choice(this)

h = gcbo;

% ---------------------
% Changement de l'ocean

this = set(this, 'Season', get(h, 'Value'));

% ----------------------------
% Initialisation de l'this

this = cl_sound_speed_init(this);

% ----------------------
% Mise a jour des traces

this = tracer_courbes(this);
this = placer_courbes(this);
