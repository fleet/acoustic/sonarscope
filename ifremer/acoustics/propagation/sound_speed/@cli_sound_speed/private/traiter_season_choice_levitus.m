% Reaction au changement de saison pour levitus
% 
% instance = traiter_season_choice_levitus(instance, msg)
%
% Input Arguments
%    instance : instance de cli_sound_speed
%    msg      : message associe
%
% Output Arguments
%    instance : instance de cli_sound_speed
%
% See also cli_sound_speed
% Authors : DCF
% ----------------------------------------------------------------------------

function instance = traiter_season_choice_levitus(instance, msg)

% -------
% Locales

hSeason = findobj(instance.hFigure, 'Tag', msg);
hApply  = findobj(instance.hFigure, 'Tag', 'Apply');

% --------------------------------------------------------------------------
% Mise a jour de l instance

instance = set(instance, 'Season', get(hSeason, 'Value'));

% --------------------------------------------------------------------------
% Efface les courbes obsoletes

instance = effacer_courbes(instance);

% --------------------------------------------------------------------------
% Mise en place du moyen de valider la modification

set(hApply, 'Visible', 'on');
