% Reaction a un changement d affichage de la vitesse du son
% 
% this = traiter_sound_speed(this, msg)
%
% Input Arguments
%    this : instance de cli_sound_speed
%
% Output Arguments
%    this : instance de cli_sound_speed
%
% See also cli_sound_speed
% Authors : DCF
% ----------------------------------------------------------------------------

function this = traiter_sound_speed(this)

% ----------------------
% Mise a jour des frames

this = placer_courbes(this);
