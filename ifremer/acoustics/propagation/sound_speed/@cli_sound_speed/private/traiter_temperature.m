% Reaction a un changement d affichage de la temperature
% 
% this = traiter_temperature(this, msg)
%
% Input Arguments
%    this : instance de cli_sound_speed
%    msg      : message associe
%
% Output Arguments
%    this : instance de cli_sound_speed
%
% See also cli_sound_speed
% Authors : DCF
% ----------------------------------------------------------------------------

function this = traiter_temperature(this)
this = placer_courbes(this);
