% Reaction a une action de zoom
% 
% instance = traiter_zoom (instance, hAxe, bbox)
%
% Input Arguments
%    instance : instance de cli_sound_speed
%    hAxe     : handle de l axe de realisation du zoom
%    bbox     : bounding box determinee par le zoom
%
% Output Arguments
%    instance : instance de cli_sound_speed
%
% See also help cli_sound_speed
% Authors : DCF
% VERSION  : $Id
% ----------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HST
%    05/02/2001 - DCF - creation
%    23/04/2001 - DCF - mise en reference
% ----------------------------------------------------------------------------

function instance = traiter_zoom (instance, hAxe, bbox)

% ----------------------------------------------------------------------------
% ----------------------------------------------------------------------------
% ----------------------------------------------------------------------------
% Bloc try

% Maintenance Auto : try

  % --------------------------------------------------------------------------
  % Traitement
  
  appliquer (instance.zoom, hAxe, bbox) ;

% ----------------------------------------------------------------------------
% ----------------------------------------------------------------------------
% ----------------------------------------------------------------------------
% Bloc catch

% Maintenance Auto : catch
% Maintenance Auto :   err ('cli_sound_speed', 'traiter_zoom', '') ;
% Maintenance Auto : end
