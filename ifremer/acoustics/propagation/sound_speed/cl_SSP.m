% Class for Sound Speed Profiles
%
% Examples
%   a1 = set_tZTS(cl_SSP, now, (-1:-10:-500)', 10, 35)
%   plot(a1)
%   a2 = set_tZTS(cl_SSP, now, (-1:-10:-500)', 4, 35)
%   plot(a2)
%   plot([a1 a2])
%
%   a1 = set_tZTS(cl_SSP, [now now+1], (-1:-10:-500)', [4 14], 35)
%   plot(a1)
%
%   a1 = set_tZTS(cl_SSP, [now now+1], (-1:-10:-500)', 10, [30 35])
%   plot(a1)
%
%   a1 = set_tZTS(cl_SSP, [now now+1], (-1:-10:-500)', [4 14], [30 35])
%   plot(a1)
%
%   Alpha = absorption(a1, 300, 'Immersion', -100)
%   plot(a1, 'Freq', 300)
%   plot(a1, 'Freq', 300, 'Immersion', -100)
%
%   a1 = set_tZTS(cl_SSP, now, (-1:-10:-500)', 10, 35)
%   C = a1.C
%   a2 = set_tZC(cl_SSP, now, (-1:-10:-500)', C, 35)
%   plot([a1 a2])
%
%   a1 = set_tLevitus(cl_SSP, now,     25, -34);
%   a2 = set_tLevitus(cl_SSP, now+180, 25, -34);
%   plot([a1 a2])
%
%   a1 = set_tLevitus(cl_SSP, [now now+180], 25, -34);
%   plot(a1)
%
%   a1 = set_tLevitus(cl_SSP, [now now+180], [25 35], [-34 -40]);
%   plot(a1)
%
%   a1 = set_tZTS(cl_SSP, [now now+180], (-1:-10:-500)', 10, [30 35])
%   plot(a1)
%   filename = fullfile(my_tempdir, 'SSP_a1.mat')
%   flag = export(a1, filename)
%   [flag, a3] = import(cl_SSP, filename)
%   [a1 a3]
%
%   nomFic = 'Y:\private\SPFE\TEST ABS COEFFICIENT\copco1516.tab';
%   [flag, Odas] = read_ODAS(nomFic, 'CleanData', 1);
%   t = Odas.Time.timeMat;
%   a1 = set_tZTS(cl_SSP, t(1:100)', [0 -1000]', Odas.Temp(1:100)', Odas.Sal(1:100)')
%   plot(a1)
%   a1 = set_tZTS(cl_SSP, t', [0 -1000]', Odas.Temp', Odas.Sal')
%   plot(a1, 'sub2', 1:1000:length(t))
%
%   a1 = set_tLevitus(cl_SSP, now, Odas.Lat', Odas.Lon');
%   plot(a1, 'sub2', 1:1000:length(Odas.Lat))

classdef cl_SSP < handle
    
    %% Properties
    
    properties(GetAccess='public', SetAccess='private')
        Time = []; % Matlab
        Z    = []; % m
        Temp = []; % Deg
        Sal  = []; % 0/000
        C    = []; % m/s
    end
    
    %% Methods
    
    methods(Access='public')
        
        function this = set_tZTS(this, t1, Z1, T1, S1)
            % Set a cl_SPP
            %
            % Syntax
            %   a = set_tZTS(a, t, Z, T, S)
            %
            % Input Arguments
            %   a : Instance of cl_SSP
            %   t : Time of the SSP (Matlab) - size [1, nt]
            %   S : Depths (m > 0)           - size [nz, 1]
            %   T : Temperature (�c)         - size [nz, nt]
            %   S : Salinity (0/000)         - size [nz, nt]
            %
            % Output Arguments
            %   a : Instance of cl_SSP
            %
            % Examples
            %   a = cl_SSP
            %   a = set_tZTS(a, now, -1:-1:-10, 10, 35)
            %   plot(a)
            %
            % See also celeriteLovett Authors
            % Authors  : JMA
            %-------------------------------------------------------------------------------
            
            %% Contr�les
            
            flag = checkSizes_tZTS(this, t1, Z1, T1, S1);
            if ~flag
                return
            end
            
            %% Mise � jour des propri�t�s
            
            this.Time = t1;
            this.Z    = Z1;
            this.Temp = T1;
            this.Sal  = S1;
            
            %% Calcul de la c�l�rit�
            
            nt = length(this.Time);
            for k=nt:-1:1
                if size(this.Temp, 2) == 1
                    T = this.Temp(:,1);
                else
                    T = this.Temp(:,k);
                end
                if size(this.Sal, 2) == 1
                    S = this.Sal(:,1);
                else
                    S = this.Sal(:,k);
                end
                this.C(:,k) = celeriteChen(this.Z, T, S);
                %             this.C = VelocityMedwinXL(this.Temp(:,k), this.Sal(:,k), this.Z); % TODO : voir avec Xavier quelle formule utiliser
            end
            
        end
        
        function plot(this, varargin)
            [varargin, Freq]      = getPropertyValue(varargin, 'Freq',      []);
            [varargin, Immersion] = getPropertyValue(varargin, 'Immersion', 0);
            [varargin, sub2]      = getPropertyValue(varargin, 'sub2',      []); %#ok<ASGLU>
            
            if isempty(Freq)
                N = 3;
            else
                N = 5;
            end
            
            Color = 'brmkg';
            FigUtils.createSScFigure;
            for k=1:length(this)
                Z1 = this(k).Z;
                if isempty(sub2)
                    t1 = this(k).Time;
                    T1 = this(k).Temp;
                    S1 = this(k).Sal;
                    C1 = this(k).C;
                    sub2 = 1:length(t1);
                else
                    t1 = this(k).Time(sub2);
                    T1 = this(k).Temp(sub2);
                    S1 = this(k).Sal(sub2);
                    C1 = this(k).C(:,sub2);
                end
                
                if numel(Z1) <= 1
                    return
                end
                
                if size(T1,1) == 1
                    T1 = repmat(T1, size(Z1));
                end
                if size(S1,1) == 1
                    S1 = repmat(S1, size(Z1));
                end
                
                kMod = 1 + mod(k-1, numel(Color));
                Legend{k} = datestr(t1); %#ok<AGROW>
                
                h(1) = subplot(1,N,1);
                PlotUtils.createSScPlot(S1, Z1, Color(kMod));
                grid on; title('Sal (0/000)'); hold on;
                
                h(2) = subplot(1,N,2);
                PlotUtils.createSScPlot(T1, Z1, Color(kMod));
                grid on; title('Temp (deg cel)'); hold on;
                
                h(3) = subplot(1,N,3);
                PlotUtils.createSScPlot(C1, Z1, Color(kMod));
                grid on; title('Cel (m/s)'); hold on;
                
                if ~isempty(Freq)
                    h(4) = subplot(1,N,4);
                    [Alpha, AlphaSum] = absorption(this, Freq, 'sub2', sub2, 'Immersion', Immersion);
                    for k3=1:size(Alpha,3)
                        PlotUtils.createSScPlot(Alpha(:,:,k3), Z1, Color(kMod));
                        grid on; title('Alpha (dB/km)'); hold on;
                    end
                    
                    h(5) = subplot(1,N,5);
                    for k3=1:size(Alpha,3)
                        PlotUtils.createSScPlot(AlphaSum(:,:,k3), Z1, Color(kMod));
                        grid on; title('AlphaSum (dB/km)'); hold on;
                    end
                end
                
            end
            str = str2cell(cell2str(Legend));
            if length(str) < 10
                legend(Legend)
            end
            linkaxes(h, 'y')
        end
        
        function flag = export(this, filename)
            save(filename, 'this')
            flag = exist(filename, 'file');
        end
        
        function [flag, this] = import(this, filename)
            flag = exist(filename, 'file');
            if ~flag
                return
            end
            load(filename, 'this')
        end
        
        function display(this) %#ok<DISPLAY>
            disp(char(this))
        end
        
        function  str = char(this)
            N = numel(this);
            str = cell(2*N,1);
            for k=1:N
                if N > 1
                    str{2*k-1} = sprintf('-----Element (%d/%d) -------', k, N);
                end
                str1 = char_instance(this(k));
                if N > 1
                    str1 = [repmat(' ', size(str1,1), 2) str1]; %#ok<AGROW>
                end
                str{2*k} = str1;
            end
            str = cell2str(str);
        end
        
        function [Alpha, AlphaSum] = absorption(this, freq, varargin)
            [varargin, sub1]      = getPropertyValue(varargin, 'sub1',      1:size(this.Z,1));
            [varargin, sub2]      = getPropertyValue(varargin, 'sub2',      1:size(this.Temp,2));
            [varargin, Immersion] = getPropertyValue(varargin, 'Immersion', 0); %#ok<ASGLU>
            
            %             [Alpha, AlphaSum] = AttenuationGarrison(freq, this.Z, this.Temp(:,sub2), this.Sal(:,sub2), varargin{:});
            
            z = -abs(this.Z(sub1));
            T = this.Temp(:,sub2);
            
            if all(isnan(T))
                Alpha    = [];
                AlphaSum = [];
                return
            end
            
            S = this.Sal(:, sub2);
            nbFreq = length(freq);
            
            f = freq;
            f = double(f);
            z = double(z);
            T = double(T);
            S = double(S);
            
            nt = size(S,2);
            nz = size(z,1);
            if size(T,1) == 1
                T = repmat(T, nz, 1);
            end
            if size(S,1) == 1
                S = repmat(S, nz, 1);
            end
            if size(z,2) == 1
                z = repmat(z, 1, nt);
            end
            
            Immersion = double(Immersion);
            
            %% Frequence de relaxation des molecules de sulfate de magnesium MgSO4 (kHz)
            
            F2 = FMgSO4(T, S);
            
            %% Frequence de relaxation des molecules d'acide borique B(OH)3 (kHz)
            
            F1 = FBOH3(T, S);
            
            %% C�l�rit� du son dans l'eau (m/s)
            
            Celerite = 1412 + (3.21 * T) + (1.19 * S) - (1.67e-2 * z);
            
            %%
            
            T_carre = T .* T;
            
            Index = T <= 20;
            A31 = (4.937e-4 - 2.59e-5 * T + 9.11e-7 * T_carre - 1.5e-8  * T_carre .* T) .* Index;
            Index = T > 20;
            A32 = (3.964e-4 - 1.146e-5 * T + 1.45e-7  * T_carre - 6.5e-10  * T_carre .* T) .* Index;
            A3 = A31 + A32;
            
            Z2 = z .* z;
            A1 = (154 ./ Celerite);
            P1 = 1;
            A2 = 21.44 * S ./ Celerite .* (1. + 0.025 * T);
            P2 = (1 - 1.37e-4 * (-z) + 6.2e-9 * Z2);
            P3 = 1 - 3.83e-5 .* (-z) + 4.9e-10 .* Z2;
            
            % Avant
            
            Alpha    = NaN([nz, nt, nbFreq]);
            AlphaSum = NaN([nz, nt, nbFreq]);
            for k=1:nbFreq
                fCarre = f(k) .* f(k);
                Alpha(:,:,k) = (A1 .* P1 .* (F1 .* fCarre) ./ (F1 .* F1 + fCarre)) + ...
                    A2 .* P2 .* (F2 .* fCarre) ./ (fCarre + F2 .* F2) + ...
                    A3 .* P3 .* fCarre;
                
                if nz == 1
                    AlphaSum(:,:,k) =  Alpha(:,:,k);
                else
                    for k2=1:size(z,2)
                        AlphaSum(:,k2,k) = moyx0(z(:,k2), Alpha(:,k2,k), Immersion);
                    end
                end
            end
            Alpha    = single(Alpha);
            AlphaSum = single(AlphaSum);
        end
        
        
        function this = set_tZC(this, t1, Z1, C1, S1)
            
            %% Contr�les
            
            if numel(t1) ~= 1
                str1 = '"t" doit �tre de dimension 1';
                str2 = 'Dimension of "t" must be 1';
                my_warndlg(Lang(str1,str2), 1);
                return
            end
            
            switch numel(Z1)
                case 0 % Z1 is empty
                    str1 = '"Z" doit �tre au moins de dimension 1';
                    str2 = 'Dimension of "Z" must be at least 1';
                    my_warndlg(Lang(str1,str2), 1);
                    return
                case 1 % Only one value is given for Z
                    if numel(C1) ~= 1
                        str1 = '"T" doit �tre au moins de dimension 1';
                        str2 = 'Dimension of "T" must be at least 1';
                        my_warndlg(Lang(str1,str2), 1);
                        return
                    end
                    if numel(S1) ~= 1
                        str1 = '"S" doit �tre au moins de dimension 1';
                        str2 = 'Dimension of "S" must be at least 1';
                        my_warndlg(Lang(str1,str2), 1);
                        return
                    end
                    if Z1 > 0
                        str1 = 'Les profondeurs doivent �tre n�gatives.';
                        str2 = 'Depth values must be negative.';
                        my_warndlg(Lang(str1,str2), 1);
                        return
                    end
                otherwise % A series of value is given for Z
                    if (numel(C1) ~= 1) && ~isequal(size(C1), size(Z1))
                        str1 = 'C doit �tre soit un scalaire soit �tre un vecteur de m�me dimension que Z.';
                        str2 = 'C must be either a scalar or a vector of same dimension than Z';
                        my_warndlg(Lang(str1,str2), 1);
                        return
                    end
                    if (numel(S1) ~= 1) && ~isequal(size(S1), size(Z1))
                        str1 = 'S doit �tre soit un scalaire soit �tre un vecteur de m�me dimension que Z.';
                        str2 = 'S must be either a scalar or a vector of same dimension than Z';
                        my_warndlg(Lang(str1,str2), 1);
                        return
                    end
            end
            
            %% Nettoyage
            
            sub = (C1 == 0);
            C1(sub) = [];
            Z1(sub) = [];
            
            ind = find(diff(Z1) > 0);
            if ~isempty(ind)
                C1(ind+1) = [];
                Z1(ind+1) = [];
            end
            
            %% Calcul de T1
            
            T1 = TemperatureFromSalinityAndVelocity(C1, S1, Z1);
            
            %% Mise � jour des propri�t�s
            
            this.Time = t1;
            this.Z    = Z1;
            this.Temp = T1;
            this.Sal  = S1;
            this.C    = C1;
            % Faut-il recalculer C ? ou comparer ?
        end
        
        function this = set_tLevitus(this, t1, Lat, Lon)
            
            %% Contr�les
            
            flag = checkSizes_tLatLon(this, t1, Lat, Lon);
            if ~flag
                return
            end
            
            %% Extraction Levitus
            
            Lat_Previous   = NaN;
            Lon_Previous   = NaN;
            Month_Previous = NaN;
            nt = length(t1);
            nL = length(Lat);
            nk = [];
            for k=max(nt,nL):-1:1
                if nt == 1
                    dIfr = timeMat2Ifr(t1(1));
                else
                    dIfr = timeMat2Ifr(t1(k));
                end
                if nL == 1
                    Latitude  = Lat(1);
                    Longitude = Lon(1);
                else
                    Latitude  = Lat(k);
                    Longitude = Lon(k);
                end
                
                if isnan(Latitude) || isnan(Longitude)
                    continue
                end
                
                [~, Month] = dayIfr2Jma(dIfr);
                
                if (Lat_Previous ~= round(Latitude)) || (Lon_Previous ~= round(Longitude)) || (Month_Previous ~= Month)
                    [flag, Z1, T1, S1] = lecLevitusSyntheseTemporelle(Latitude, Longitude, 1, 3, Month);
                    if ~flag
                        str1 = 'Il semble que le r�pertoire SonarScopeData ne soit pas bien install� sur votre ordi, les fichiers "SonarScopeData\Levitus\*.mat" n''ont pas �t� trouv�s. Ces fichiers sont utiles pour retrouver la salinit�.';
                        str2 = 'It seems that the directory SonarScopeData is not correctly installed. I need the "SonarScopeData\Levitus\*.mat" files to extract the salinity.';
                        my_warndlg(Lang(str1,str2), 1);
                        return % TODO : peut-�tre demander la salinit� ?
                    end
                end
                
                if isempty(Z1)
                    continue
                end
                
                Lat_Previous   = round(Latitude);
                Lon_Previous   = round(Longitude);
                Month_Previous = Month;
                
                %% Mise � jour des propri�t�s
                
                this.Time(:,k) = t1;
                nk(k) = length(Z1);
                sub = 1:nk(k);
                this.Z(sub,1)    = Z1; % TODO : v�rifier que Z ne change pas
                this.Temp(sub,k) = T1;
                this.Sal(sub,k)  = S1;
                
                %% Calcul de la c�l�rit�
                
                this.C(sub,k) = celeriteChen(Z1(:), T1, S1);
                %             this.C = VelocityMedwinXL(this.Temp, this.Sal, this.Z); % TODO : voir avec Xavier quelle formule utiliser
            end
            for k=1:length(nk)
                sub = (nk(k)+1):max(nk);
                this.Temp(sub,k) = NaN;
                this.Sal( sub,k) = NaN;
                this.C(   sub,k) = NaN;
            end
        end
        
    end
    
    methods(Access='private')
        function  str = char_instance(this)
            str = [];
            T = cl_time('timeMat', this.Time);
            %             str{end+1} = sprintf('  Time <-> %s', datestr(this.Time)');
            str{end+1} = sprintf('  Time <-> %s', char(T));
            str{end+1} = sprintf('  Z    <-> %s', num2strCode(this.Z));
            str{end+1} = sprintf('  Temp <-> %s', num2strCode(this.Temp));
            str{end+1} = sprintf('  Sal  <-> %s', num2strCode(this.Sal));
            str{end+1} = sprintf('  C    <-> %s', num2strCode(this.C));
            
            str = cell2str(str);
        end
        
        function flag = checkSizes_tZTS(~, t, Z, T, S)
            %   t : Time of the SSP (Matlab) - size [1,nt]
            %   Z : Depths (m > 0)           - size [nz,1]
            %   T : Temperature (�c)         - size [nz,nt]
            %   S : Salinity (0/000)         - size [nz,nt]
            
            flag = 0;
            
            nt = size(t,2);
            nz = size(Z,1);
            
            if size(t,1) ~= 1
                str1 = sprintf('"t" doit �tre de dimension [1 nt]=[1,%d]', size(t,1));
                str2 = sprintf('Dimension of "t" must be [1 nt]=[1,%d]', size(t,1));
                my_warndlg(Lang(str1,str2), 1);
                return
            end
            
            if size(Z,2) ~= 1
                str1 = sprintf('"Z" doit �tre de dimension [nz 1]=[%d,1]', nz);
                str2 = sprintf('Dimension of "Z" must be [nz 1]=[%d,1]', nz);
                my_warndlg(Lang(str1,str2), 1);
                return
            end
            
            if length(T) == 1
                % OK, on consid�re que la valeur est la m�me pour toutes
                % les couches de profondeur
            else
                if ~isequal(size(T), [nz,nt]) && ~isequal(size(T), [1,nt])
                    str1 = sprintf('"T" doit �tre de dimension [nz nt] = [%d,%d]', nz, nt);
                    str2 = sprintf('Dimension of "T" must be [nz nt] = [%d,%d]', nz, nt);
                    my_warndlg(Lang(str1,str2), 1);
                    return
                end
            end
            
            if length(S) == 1
                % OK, on consid�re que la valeur est la m�me pour toutes
                % les couches de profondeur
            else
                if ~isequal(size(S), [nz,nt]) && ~isequal(size(S), [1,nt])
                    str1 = sprintf('"S" doit �tre de dimension [nz nt] = [%d,%d]', nz, nt);
                    str2 = sprintf('Dimension of "S" must be [nz nt] = [%d,%d]', nz, nt);
                    my_warndlg(Lang(str1,str2), 1);
                    return
                end
            end
            
            flag = 1;
        end
        
        function flag = checkSizes_tLatLon(~, t, Lat, Lon)
            
            flag = 0;
            
            nt = size(t,2);
            nL = size(Lat,2);
            
            if size(t,1) ~= 1
                str1 = sprintf('"t" doit �tre de dimension [1 nt]=[1,%d]', nt);
                str2 = sprintf('Dimension of "t" must be [1 nt]=[1,%d]', nt);
                my_warndlg(Lang(str1,str2), 1);
                return
            end
            
            if size(Lat,1) ~= 1
                str1 = sprintf('"Lat" doit �tre de dimension [1 nL]=[1,%d]', nL);
                str2 = sprintf('Dimension of "Lat" must be [1 nL]=[1,%d]', nL);
                my_warndlg(Lang(str1,str2), 1);
                return
            end
            
            if ~isequal(size(Lon,1), size(Lat,1))
                str1 = sprintf('"Lon" doit �tre de dimension [1 nL]=[1,%d]', nL);
                str2 = sprintf('Dimension of "Lon" must be [1 nL]=[1,%d]', nL);
                my_warndlg(Lang(str1,str2), 1);
                return
            end
            
            if (nt ~= 1) && (nL ~= 1) && (nt ~= nL)
                str1 = 'Si "t" ou "lat" ne sont pas des scalaires alors il faut qu''ils aient la m�me dimension.';
                str2 = 'If "t" or "lat" are not scalars then they must have the same dimensions.';
                my_warndlg(Lang(str1,str2), 1);
                return
            end
            
            flag = 1;
        end
    end
end
