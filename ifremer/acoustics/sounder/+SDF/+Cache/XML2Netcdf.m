% [flag, dataFileName] = FtpUtils.getSScDemoFile('Syp_001_180507104600.sdf', 'OutputDir', 'D:\Temp\ExData')
%
% SDF.Cache.XML2Netcdf(dataFileName)

function flag = XML2Netcdf(dataFileName, varargin)

[varargin, deleteXMLBin] = getPropertyValue(varargin, 'deleteXMLBin', 0);
[varargin, deflateLevel] = getPropertyValue(varargin, 'deflateLevel', 1); %#ok<ASGLU>

%% Cr�ation du fichier Netcdf

[flag, ncFileName] = SScCacheNetcdfUtils.createSScNetcdfFile(dataFileName);
if ~flag
    return
end

%% Transfert des donn�es Index

flag = SScCacheNetcdfUtils.Cache2Netcdf(ncFileName, dataFileName, 'FileIndex', 'deflateLevel', deflateLevel);
if ~flag
    return
end

%% Transfert des donn�es V3001

flag = SScCacheNetcdfUtils.Cache2Netcdf(ncFileName, dataFileName, 'V3001', 'deflateLevel', deflateLevel);
if ~flag
    return
end

% S0 = cl_sdf.empty;
% [flag, Data, XML] = read_FileIndex_bin(S0, 'nomFic', dataFileName);
% if flag
%     flag = NetcdfUtils.XMLBin2Netcdf(XML, Data, ncFileName, package, 'deflateLevel', deflateLevel);
%     if ~flag
%         return
%     end
% end

if deleteXMLBin
    SScCacheXMLBinUtils.deleteCachDirectory(dataFileName);
end

%% Renommage de l'extension .NC en .nc pour indiquer � l'utilisateur que le fichier est termin�

my_rename(ncFileName, strrep(ncFileName, '.NC', '.nc'));

%% The End

flag = 1;
