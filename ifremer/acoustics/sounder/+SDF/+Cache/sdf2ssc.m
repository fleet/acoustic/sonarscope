function [flag, PageVersion] = sdf2ssc(nomFic, varargin)

global IfrTbxResources %#ok<GVMIS>
global IfrTbx %#ok<GVMIS>
global useCacheNetcdf %#ok<GVMIS>
global isUsingParfor %#ok<GVMIS>

if isempty(isUsingParfor)
    isUsingParfor = 0;
elseif isUsingParfor == 1
    loadVariablesEnv
    %     fprintf('IfrTbx = "%s"\n', IfrTbx);
    %     fprintf('IfrTbxResources = "%s"\n', IfrTbxResources);
end

% [varargin, logFileId]     = getPropertyValue(varargin, 'logFileId',       logFileId);
% [varargin, ShowProgressBar] = getPropertyValue(varargin, 'ShowProgressBar', 1);
[varargin, isUsingParfor]   = getPropertyValue(varargin, 'isUsingParfor',   isUsingParfor);
[varargin, useCacheNetcdf]  = getPropertyValue(varargin, 'useCacheNetcdf',  useCacheNetcdf); %#ok<ASGLU>

% Barre de progression uniquement visible en version de Dev car semble �tre
% � l'origine de probl�me lors du traitement successif de donn�es (27/09/2012).
% if isUsingParfor
%     ShowProgressBar = 0; % Modif JMA le 08/02/2021  En attendant que Roger ait recomplil� sdf2ssc 
% end

% TODO GLU
ShowProgressBar = 0; % Modif JMA le 09/02/2021  car message SonarScope:Qt: Untested Windows version 6.2 detected! 
% TODO GLU

%% G�n�ration des XML et des binaires

[nomDir, nomFicSeul] = fileparts(nomFic);
nomDirRacine = fullfile(nomDir, 'SonarScope', nomFicSeul);

unzipSonarScopeCache(nomDirRacine)

% Si le fichier SSC_<format>.xml existe, alors on sort car d�codage d�j� effectu�.
[PageVersion, flagExistSDF, flagExistSSC] = SDF.Process.getPageVersion(nomDirRacine); %#ok<ASGLU>
% if all(flag) && (strcmp(getFormatVersion(nomDirRacine), PageVersionV3000) == 0 || strcmp(getFormatVersion(nomDirRacine), FormatVersionV3001) == 0 || strcmp(getFormatVersion(nomDirRacine), FormatVersionV5000) == 0 || strcmp(getFormatVersion(nomDirRacine), FormatVersionV5001) == 0 || strcmp(getFormatVersion(nomDirRacine), FormatVersionV7000) == 0  || strcmp(getFormatVersion(nomDirRacine), FormatVersionV7001) == 0)
if flagExistSSC
    PageVersion = SDF.Process.getPageVersion(nomDirRacine);
    flag = 1;
    return
end

%% Lancement du converteur

nomExe = 'CONVERT_SDF.exe';
if isdeployed
    nomDir = fileparts(IfrTbxResources);
    nomExe = fullfile(nomDir, 'extern', 'C', nomExe);
else
    nomExe = [IfrTbx '\ifremer\extern\C\CONVERT_exe\Release\' nomExe];
end
cmd = sprintf('!"%s" "%d" "%s"', nomExe, ShowProgressBar, nomFic);

nomFicTmp = [nomDirRacine '_tmp'];
if exist(nomFicTmp, 'dir')
    message_erreur_convert(nomFicTmp, nomFic, nomDirRacine)
    flag = 0;
    return
end

fprintf('%s\n', cmd)
try
    [~, msg] = dos(cmd(2:end));
catch %#ok<CTCH>
    str1 = sprintf('CONVERT_ALL.exe semble avoir rencontr� un probl�me sur le fichier %s', nomFic);
    str2 = sprintf('CONVERT_ALL.exe does not seem working for file %s', nomFic);
    my_warndlg(Lang(str1,str2), 0);
end

%  Bidouille pour bypasser bug fichier Charline, survey RESIT (mail Roger du 09/02/2021
nomDirRacineTmp = [nomDirRacine '_tmp'];
if exist(nomDirRacineTmp, 'dir')
    my_rename(nomDirRacineTmp, nomDirRacine);
end

% test si le r�pertoire a bien �t� cr��
if ~exist(nomDirRacine, 'dir')
    if isempty(msg)
        str1 = sprintf('CONVERT_SDF.exe semble avoir rencontr� un probl�me sur le fichier %s', nomFic);
        str2 = sprintf('CONVERT_SDF.exe does not seem working for file %s', nomFic);
        msg = Lang(str1,str2);
    end
    my_warndlg(msg, 0);
    flag = 0;
    return
end

PageVersion = SDF.Process.getPageVersion(nomDirRacine);

%% Conversion en sch�ma SonarScope

switch PageVersion
    case 'V3000'
        flag = SDF.Cache.sdf2ssc_V3000(nomDirRacine);
        if ~flag
        %     return
        end
        
    case 'V3001'
        flag = SDF.Cache.sdf2ssc_V3001(nomDirRacine);
        if ~flag
        %     return
        end
        
    case 'V5000'
        flag = SDF.Cache.sdf2ssc_V5000(nomDirRacine);
        if ~flag
        %     return
        end
        
    case 'V5001'
        flag = SDF.Cache.sdf2ssc_V5001(nomDirRacine);
        if ~flag
        %     return
        end
        
    case 'V7000'
        flag = SDF.Cache.sdf2ssc_V7000(nomDirRacine);
        if ~flag
        %     return
        end
        
    case 'V7001'
        flag = SDF.Cache.sdf2ssc_V7001(nomDirRacine);
        if ~flag
        %     return
        end
        
    otherwise
        msg = Lang('Datagramme non identifi�','Datagram non identified');
        my_warndlg(msg, 1);
        flag = 0;
        return
end

flag = SDF.Cache.sdf2ssc_FileIndex(nomFic);
if ~flag
    %     return
end

%% Gestion du cache

switch useCacheNetcdf
    case 1 % 'Create XML-Bin only'
    case 2 % 'Create Netcdf and keep XML-Bin'
        SDF.Cache.XML2Netcdf(nomFic, 'deleteXMLBin', 0);
    case 3 % 'Create Netcdf and delete XML-Bin'
        SDF.Cache.XML2Netcdf(nomFic, 'deleteXMLBin', 1);
end

%% Cr�ation du layer de r�flectivit� � partir des snippets

flag = 1;
