function flag = sdf2ssc_FileIndex(nomFic, varargin)

[pathname, filename] = fileparts(nomFic);
nomDirRacine = fullfile(pathname, 'SonarScope', filename);

%% Lecture du fichier XML d�crivant la donn�e 

nomFicXml = fullfile(nomDirRacine, 'SDF_FileIndex.xml');
flag = exist(nomFicXml, 'file');
if ~flag
    special_message('FichierNonExistant', nomFicXml)
    return
end
% edit(nomFicXml)
XML = xml_read(nomFicXml);

[nomDirSignal, nomFicSignal] = fileparts(nomFicXml);
nomDirSignal = fullfile(nomDirSignal, nomFicSignal);

%% Lecture des variables

[flag, Data.NbOfBytesInPage] = Read_BinVariable(XML, nomDirSignal, 'NbOfBytesInPage', 'uint32');
if ~flag
    return
end

[flag, Data.TypeOfDatagram] = Read_BinVariable(XML, nomDirSignal, 'TypeOfPage', 'uint32');
if ~flag
    return
end

[flag, year] = Read_BinVariable(XML, nomDirSignal, 'year', 'uint32');
if ~flag
    return
end

[flag, month] = Read_BinVariable(XML, nomDirSignal, 'month', 'uint32');
if ~flag
    return
end

[flag, day] = Read_BinVariable(XML, nomDirSignal, 'day', 'uint32');
if ~flag
    return
end

[flag, hour] = Read_BinVariable(XML, nomDirSignal, 'hour', 'uint32');
if ~flag
    return
end

[flag, minute] = Read_BinVariable(XML, nomDirSignal, 'minute', 'uint32');
if ~flag
    return
end

[flag, second] = Read_BinVariable(XML, nomDirSignal, 'second', 'uint32');
if ~flag
    return
end

[flag, hSecond] = Read_BinVariable(XML, nomDirSignal, 'hSecond', 'uint32');
if ~flag
    return
end

[flag, Data.FileIndexCounter] = Read_BinVariable(XML, nomDirSignal, 'FileIndexCounter', 'uint16');
if ~flag
    return
end

[flag, Data.DatagramPosition] = Read_BinVariable(XML, nomDirSignal, 'DatagramPosition', 'uint64');
if ~flag
    return
end

[flag, Data.DatagramLength] = Read_BinVariable(XML, nomDirSignal, 'DatagramLength', 'int32');
if ~flag
    return
end

%%

dIfr = dayJma2Ifr(day(:), month(:), year(:));
hIfr = hourHms2Ifr(hour(:), minute(:), second(:)+hSecond(:)/100);
Data.Time  = cl_time('timeIfr', dIfr, hIfr);

%% Lecture des tables

nbPings = XML.NbDatagrams;
Data.PingCounter = 1:nbPings;

%% G�n�ration du XML SonarScope

Info.Title                  = 'FileIndex'; % Motion Reference Unit
Info.Constructor            = 'Klein';
Info.PageVersion            = 'FileIndex';
% Info.SystemSerialNumber     = XML.SystemSerialNumber;
Info.TimeOrigin             = '01/01/-4713';
Info.Comments               = 'Sensor sampling rate';

Info.Dimensions.nbPings     = nbPings;

%info des variables
Info.Signals(1).Name          = 'TypeOfDatagram';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'int32';
Info.Signals(end).Unit        = 'm';
Info.Signals(end).FileName    = fullfile('Ssc_FileIndex','TypeOfDatagram.bin');

Info.Signals(end+1).Name      = 'PingCounter';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_FileIndex','PingCounter.bin');

Info.Signals(end+1).Name      = 'DatagramLength';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'int32';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_FileIndex','DatagramLength.bin');

Info.Signals(end+1).Name      = 'DatagramPosition';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'uint64';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_FileIndex','DatagramPosition.bin');

Info.Signals(end+1).Name      = 'Time';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = 'days since JC';
Info.Signals(end).FileName    = fullfile('Ssc_FileIndex','Time.bin');

%% Cr�ation du fichier XML d�crivant la donn�e 

nomFicXml = fullfile(nomDirRacine, 'Ssc_FileIndex.xml');
xml_write(nomFicXml, Info);
flag = exist(nomFicXml, 'file');
if ~flag
    messageErreur(nomFicXml)
    return
end

%       TypeOfDatagram: [1101�1 single]
%          PingCounter: [1101�1 single]
%       DatagramLength: [1101�1 single]
%     DatagramPosition: [1101�1 double]
%                 Time: [1�1 cl_time]

%% Cr�ation du r�pertoire Ssc_FileIndex

nomDirFileIndex = fullfile(nomDirRacine, 'Ssc_FileIndex');
if ~exist(nomDirFileIndex, 'dir')
    status = mkdir(nomDirFileIndex);
    if ~status
        messageErreur(nomDirFileIndex)
        flag = 0;
        return
    end
end

%% Cr�ation des fichiers binaires des signaux

for k=1:length(Info.Signals)
    flag = writeSignal(nomDirRacine, Info.Signals(k), Data.(Info.Signals(k).Name));
    if ~flag
        return
    end
end

flag = 1;
