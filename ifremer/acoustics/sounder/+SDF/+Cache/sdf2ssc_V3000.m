function flag = sdf2ssc_V3000(nomDirRacine, varargin)

%% Lecture du fichier XML d�crivant la donn�e 

nomFicXml = fullfile(nomDirRacine, 'SDF_V3000.xml');
flag = exist(nomFicXml, 'file');
if ~flag
    special_message('FichierNonExistant', nomFicXml)
    return
end
% edit(nomFicXml)
XML = xml_read(nomFicXml);

[nomDirSignal, nomFicSignal] = fileparts(nomFicXml);
nomDirSignal = fullfile(nomDirSignal, nomFicSignal);

%% Lecture des variables

% Lecture du nombre de bytes
[flag, Data.numberBytes] = Read_BinVariable(XML, nomDirSignal, 'numberBytes', 'uint32');
if ~flag
    return
end

% Lecture du pingNumber
[flag, Data.pingNumber] = Read_BinVariable(XML, nomDirSignal, 'pingNumber', 'uint32');
if ~flag
    return
end

% Lecture du nombre de samples
[flag, numSamples] = Read_BinVariable(XML, nomDirSignal, 'numSamples', 'uint32');
if ~flag
    return
end
nbSamples = sum(numSamples);

% Lecture du beamsToDisplay
[flag, Data.beamsToDisplay] = Read_BinVariable(XML, nomDirSignal, 'beamsToDisplay', 'uint32');
if ~flag
    return
end

% Lecture du errorFlags
[flag, Data.errorFlags] = Read_BinVariable(XML, nomDirSignal, 'errorFlags', 'uint32');
if ~flag
    return
end

% Lecture du range
[flag, Data.range] = Read_BinVariable(XML, nomDirSignal, 'range', 'uint32');
if ~flag
    return
end

% Lecture du speedFish
[flag, Data.speedFish] = Read_BinVariable(XML, nomDirSignal, 'speedFish', 'uint32');
if ~flag
    return
end

% Lecture du speedSound
[flag, Data.speedSound] = Read_BinVariable(XML, nomDirSignal, 'speedSound', 'uint32');
if ~flag
    return
end

% Lecture du resMode
[flag, Data.resMode] = Read_BinVariable(XML, nomDirSignal, 'resMode', 'uint32');
if ~flag
    return
end

% Lecture du txWaveform
[flag, Data.txWaveform] = Read_BinVariable(XML, nomDirSignal, 'txWaveform', 'uint32');
if ~flag
    return
end

% Lecture du respDiv
[flag, Data.respDiv] = Read_BinVariable(XML, nomDirSignal, 'respDiv', 'uint32');
if ~flag
    return
end

% Lecture du respFreq
[flag, Data.respFreq] = Read_BinVariable(XML, nomDirSignal, 'respFreq', 'uint32');
if ~flag
    return
end

% Lecture du manualSpeedSwitch
[flag, Data.manualSpeedSwitch] = Read_BinVariable(XML, nomDirSignal, 'manualSpeedSwitch', 'uint32');
if ~flag
    return
end

% Lecture du despeckleSwitch
[flag, Data.despeckleSwitch] = Read_BinVariable(XML, nomDirSignal, 'despeckleSwitch', 'uint32');
if ~flag
    return
end

% Lecture du speedFilterSwitch
[flag, Data.speedFilterSwitch] = Read_BinVariable(XML, nomDirSignal, 'speedFilterSwitch', 'uint32');
if ~flag
    return
end

% Lecture du year
[flag, Year] = Read_BinVariable(XML, nomDirSignal, 'year', 'uint32');
if ~flag
    return
end

% Lecture du month
[flag, Month] = Read_BinVariable(XML, nomDirSignal, 'month', 'uint32');
if ~flag
    return
end

% Lecture du day
[flag, Day] = Read_BinVariable(XML, nomDirSignal, 'day', 'uint32');
if ~flag
    return
end

% Lecture du hour
[flag, Hour] = Read_BinVariable(XML, nomDirSignal, 'hour', 'uint32');
if ~flag
    return
end

% Lecture du minute
[flag, Minute] = Read_BinVariable(XML, nomDirSignal, 'minute', 'uint32');
if ~flag
    return
end

% Lecture du second
[flag, Second] = Read_BinVariable(XML, nomDirSignal, 'second', 'uint32');
if ~flag
    return
end

% Lecture du hSecond
[flag, CentSecond] = Read_BinVariable(XML, nomDirSignal, 'hSecond', 'uint32');
if ~flag
    return
end
dIfr = dayJma2Ifr(Day(:), Month(:), Year(:));
hIfr = hourHms2Ifr(Hour(:), Minute(:), Second(:)+CentSecond(:)/100);
Data.Time  = cl_time('timeIfr', dIfr, hIfr);


% Lecture du fixTimeHour
[flag, Data.fixTimeHour] = Read_BinVariable(XML, nomDirSignal, 'fixTimeHour', 'uint32');
if ~flag
    return
end

% Lecture du fixTimeMinute
[flag, Data.fixTimeMinute] = Read_BinVariable(XML, nomDirSignal, 'fixTimeMinute', 'uint32');
if ~flag
    return
end

% Lecture du fixTimeSecond
[flag, Data.fixTimeSecond] = Read_BinVariable(XML, nomDirSignal, 'fixTimeSecond', 'single');
if ~flag
    return
end
hIfr = hourHms2Ifr(Hour(:), Minute(:), Second(:));
Data.fixTime  = cl_time('timeIfr', dIfr, hIfr);

% Lecture du heading
[flag, Data.heading] = Read_BinVariable(XML, nomDirSignal, 'heading', 'single');
if ~flag
    return
end

% Lecture du pitch
[flag, Data.pitch] = Read_BinVariable(XML, nomDirSignal, 'pitch', 'single');
if ~flag
    return
end

% Lecture du roll
[flag, Data.roll] = Read_BinVariable(XML, nomDirSignal, 'roll', 'single');
if ~flag
    return
end

% Lecture des Voltage Min/Max et Max Pressure.
[flag, pressureMax] = Read_BinVariable(XML, nomDirSignal, 'pressureSensorMax', 'single');
if ~flag
%     return % TODO GLU 
end

[flag, pVoltageMax] = Read_BinVariable(XML, nomDirSignal, 'pressureSensorVoltageMax', 'single');
if ~flag
%     return % TODO GLU 
end

[flag, pVoltageMin] = Read_BinVariable(XML, nomDirSignal, 'pressureSensorVoltageMin', 'single');
if ~flag
%     return % TODO GLU 
end
% Lecture du depth
[flag, depthVolts] = Read_BinVariable(XML, nomDirSignal, 'depth', 'single');
if ~flag
    return
end
% Cf SDF/SDFX Specification - 15300018 - Rev 3.06 - 28-October-2010
% Note 5, pg 15
if isempty(pVoltageMax) % TOFO GLU, ajout JMA le 05/02/2020
    Data.depth = depthVolts;
else
    Data.depth = -((((depthVolts - pVoltageMin) / (pVoltageMax - pVoltageMin)) * pressureMax) - 14.696) / 1.487;
end

% Lecture du altitude
[flag, Data.altitudeMeters] = Read_BinVariable(XML, nomDirSignal, 'altitude', 'single');
if ~flag
    return
end

% Lecture du temperature
[flag, Data.temperature] = Read_BinVariable(XML, nomDirSignal, 'temperature', 'single');
if ~flag
    return
end

% Lecture du speed
[flag, Data.speed] = Read_BinVariable(XML, nomDirSignal, 'speed', 'single');
if ~flag
    return
end

% Lecture du shipHeading
[flag, Data.shipHeading] = Read_BinVariable(XML, nomDirSignal, 'shipHeading', 'single');
if ~flag
    return
end

% Lecture du magneticVariation
[flag, Data.magneticVariation] = Read_BinVariable(XML, nomDirSignal, 'magneticVariation', 'single');
if ~flag
    return
end

% Lecture du shipLat
[flag, Data.shipLat] = Read_BinVariable(XML, nomDirSignal, 'shipLat', 'double');
if ~flag
    return
end
Data.shipLat =  Data.shipLat * (180/pi);

% Lecture du shipLon
[flag, Data.shipLon] = Read_BinVariable(XML, nomDirSignal, 'shipLon', 'double');
if ~flag
    return
end
Data.shipLon = Data.shipLon * (180/pi);

% Lecture du shipLat
[flag, Data.fishLat] = Read_BinVariable(XML, nomDirSignal, 'fishLat', 'double');
if ~flag
    return
end
Data.fishLat = Data.fishLat * (180/pi);

% Lecture du fishLon
[flag, Data.fishLon] = Read_BinVariable(XML, nomDirSignal, 'fishLon', 'double');
if ~flag
    return
end
Data.fishLon = Data.fishLon * (180/pi);

% Lecture du tvgPage
[flag, Data.tvgPage] = Read_BinVariable(XML, nomDirSignal, 'tvgPage', 'uint32');
if ~flag
    return
end

% Lecture du headerSize
[flag, Data.headerSize] = Read_BinVariable(XML, nomDirSignal, 'headerSize', 'uint32');
if ~flag
    return
end

% Lecture du fixTimeYear
[flag, Data.fixTimeYear] = Read_BinVariable(XML, nomDirSignal, 'fixTimeYear', 'uint32');
if ~flag
    return
end

% Lecture du fixTimeMonth
[flag, Data.fixTimeMonth] = Read_BinVariable(XML, nomDirSignal, 'fixTimeMonth', 'uint32');
if ~flag
    return
end

% Lecture du fixTimeDay
[flag, Data.fixTimeDay] = Read_BinVariable(XML, nomDirSignal, 'fixTimeDay', 'uint32');
if ~flag
    return
end

% Lecture du auxPitch
[flag, Data.auxPitch] = Read_BinVariable(XML, nomDirSignal, 'auxPitch', 'single');
if ~flag
    return
end

% Lecture du auxRoll
[flag, Data.auxRoll] = Read_BinVariable(XML, nomDirSignal, 'auxRoll', 'single');
if ~flag
    return
end

% Lecture du auxDepth
[flag, Data.auxDepth] = Read_BinVariable(XML, nomDirSignal, 'auxDepth', 'single');
if ~flag
    return
end

% Lecture du auxAlt
[flag, Data.auxAlt] = Read_BinVariable(XML, nomDirSignal, 'auxAlt', 'single');
if ~flag
    return
end

% Lecture du cableOut
[flag, Data.cableOut] = Read_BinVariable(XML, nomDirSignal, 'cableOut', 'single');
if ~flag
    return
end

% Lecture du fseconds
[flag, Data.fseconds] = Read_BinVariable(XML, nomDirSignal, 'fseconds', 'single');
if ~flag
    return
end

% Lecture du altimeter
[flag, Data.altimeter] = Read_BinVariable(XML, nomDirSignal, 'altimeter', 'uint32');
if ~flag
    return
end

% Lecture du sampleFreq
[flag, Data.sampleFreq] = Read_BinVariable(XML, nomDirSignal, 'sampleFreq', 'uint32');
if ~flag
    return
end

% Lecture du depressorType
[flag, Data.depressorType] = Read_BinVariable(XML, nomDirSignal, 'depressorType', 'uint32');
if ~flag
    return
end

% Lecture du cableType
[flag, Data.cableType] = Read_BinVariable(XML, nomDirSignal, 'cableType', 'uint32');
if ~flag
    return
end

% Lecture du shieveXoff
[flag, Data.shieveXoff] = Read_BinVariable(XML, nomDirSignal, 'shieveXoff', 'single');
if ~flag
    return
end

% Lecture du shieveYoff
[flag, Data.shieveYoff] = Read_BinVariable(XML, nomDirSignal, 'shieveYoff', 'single');
if ~flag
    return
end

% Lecture du shieveZoff
[flag, Data.shieveZoff] = Read_BinVariable(XML, nomDirSignal, 'shieveZoff', 'single');
if ~flag
    return
end

% Lecture du GPSheight
[flag, Data.GPSheight] = Read_BinVariable(XML, nomDirSignal, 'GPSheight', 'single');
if ~flag
    return
end

% Lecture du rawDataConfig
[flag, Data.rawDataConfig] = Read_BinVariable(XML, nomDirSignal, 'rawDataConfig', 'uint32');
if ~flag
    return
end

%% Lecture des tables

nbPings = XML.NbDatagrams;
nbCol   = max(numSamples);

% Lecture du portlf
[flagportlf, X] = Read_BinTable(XML, nomDirSignal, 'portlf', 'single', nbSamples);
if flagportlf
    % TODO : rechercher le facteur d'�chelle
    if all(numSamples == numSamples(1))
        portlf = reshape(X, [nbCol nbPings]);
        clear X
        portlf = portlf';
    else
        portlf = NaN(nbPings, nbCol, 'single');
        iDeb = 0;
        for k=1:nbPings
            sub = 1:numSamples(k);
            portlf(k,sub) = X(iDeb+sub);
            iDeb = iDeb + numSamples(k);
        end
        clear X
    end
    portlf = reflec_Enr2dB(portlf);
end

% Lecture du stbdlf
[flagstbdlf, X] = Read_BinTable(XML, nomDirSignal, 'stbdlf', 'single', nbSamples);
if flagstbdlf
    if all(numSamples == numSamples(1))
        stbdlf = reshape(X, [nbCol nbPings]);
        clear X
        stbdlf = stbdlf';
    else
        stbdlf = NaN(nbPings, nbCol, 'single');
        iDeb = 0;
        for k=1:nbPings
            sub = 1:numSamples(k);
            stbdlf(k,sub) = X(iDeb+sub);
            iDeb = iDeb + numSamples(k);
        end
        clear X
    end
    stbdlf = reflec_Enr2dB(stbdlf);
end

if flagportlf && flagstbdlf
    Data.SonarLF = assemblagePortStarboard(portlf,stbdlf);
else
    Data.SonarLF = [];
end
clear portlf stbdlf

% Lecture du porthf
[flagporthf, X] = Read_BinTable(XML, nomDirSignal, 'porthf', 'single', nbSamples);
if flagporthf
    if all(numSamples == numSamples(1))
        porthf = reshape(X, [nbCol nbPings]);
        clear X
        porthf = porthf';
    else
        porthf = NaN(nbPings, nbCol, 'single');
        iDeb = 0;
        for k=1:nbPings
            sub = 1:numSamples(k);
            porthf(k,sub) = X(iDeb+sub);
            iDeb = iDeb + numSamples(k);
        end
        clear X
    end
    porthf = reflec_Enr2dB(porthf);
end

% Lecture du stbdhf
[flagstbdhf, X] = Read_BinTable(XML, nomDirSignal, 'stbdhf', 'single', nbSamples);
if flagstbdhf
    if all(numSamples == numSamples(1))
        stbdhf = reshape(X, [nbCol nbPings]);
        clear X
        stbdhf = stbdhf';
    else
        stbdhf = NaN(nbPings, nbCol, 'single');
        iDeb = 0;
        for k=1:nbPings
            sub = 1:numSamples(k);
            stbdhf(k,sub) = X(iDeb+sub);
            iDeb = iDeb + numSamples(k);
        end
        clear X
    end
    stbdhf = reflec_Enr2dB(stbdhf);
end

if flagstbdhf && flagporthf
    Data.SonarHF = assemblagePortStarboard(porthf,stbdhf);
else
    Data.SonarHF = [];
end
clear porthf stbdhf


% Lecture du sbp
nbSamplesSbp = nbSamples; % TODO : o� peut on trouver le nombre d'�chantillons ?
[flagsbp, Data.sbp] = Read_BinTable(XML, nomDirSignal, 'sbp', 'single', nbSamplesSbp);
if flagsbp
    Data.sbp = reflec_Enr2dB(Data.sbp);
    nbColSbp = size(Data.sbp, 2);
else
    Data.sbp = [];
    nbColSbp = 0;
end

%% 

nbPings  = XML.NbDatagrams;
nbCol    = 1 + 2 * nbCol;

%% G�n�ration du XML SonarScope

Info.Title                  = 'V3000'; % Motion Reference Unit
Info.Constructor            = 'Klein';
% Info.EmModel                = EmModel;
% Info.SystemSerialNumber     = Info.SystemSerialNumber;
Info.TimeOrigin             = '01/01/-4713';
Info.Comments               = 'Sensor sampling rate';

% Info.Dimensions.NbSamples     = nbSamples;
Info.Dimensions.nbPings     = nbPings;
Info.Dimensions.nbCol       = nbCol;
if nbSamplesSbp ~= 0
    Info.Dimensions.nbColSbp = nbColSbp;
end


%info des variables
Info.Signals(1).Name          = 'range';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'm';
Info.Signals(end).FileName    = fullfile('Ssc_V3000','range.bin');

Info.Signals(end+1).Name      = 'speedFish';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'cm/s';
Info.Signals(end).FileName    = fullfile('Ssc_V3000','speedFish.bin');

Info.Signals(end+1).Name      = 'speedSound';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V3000','speedSound.bin');

Info.Signals(end+1).Name      = 'resMode';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V3000','resMode.bin');

Info.Signals(end+1).Name      = 'txWaveform';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V3000','txWaveform.bin');

Info.Signals(end+1).Name      = 'respDiv';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V3000','respDiv.bin');

Info.Signals(end+1).Name      = 'respFreq';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V3000','respFreq.bin');

Info.Signals(end+1).Name      = 'manualSpeedSwitch';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V3000','manualSpeedSwitch.bin');

Info.Signals(end+1).Name      = 'despeckleSwitch';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V3000','despeckleSwitch.bin');

Info.Signals(end+1).Name      = 'speedFilterSwitch';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V3000','speedFilterSwitch.bin');

%{
Info.Signals(end+1).Name      = 'year';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V3000','year.bin');

Info.Signals(end+1).Name      = 'month';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V3000','month.bin');

Info.Signals(end+1).Name      = 'day';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V3000','day.bin');

Info.Signals(end+1).Name      = 'hour';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V3000','hour.bin');

Info.Signals(end+1).Name      = 'minute';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V3000','minute.bin');

Info.Signals(end+1).Name      = 'second';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V3000','second.bin');

Info.Signals(end+1).Name      = 'hSecond';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V3000','hSecond.bin');
%}

Info.Signals(end+1).Name      = 'fixTimeHour';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V3000','fixTimeHour.bin');

Info.Signals(end+1).Name      = 'fixTimeMinute';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V3000','fixTimeMinute.bin');

Info.Signals(end+1).Name      = 'fixTimeSecond';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V3000','fixTimeSecond.bin');

Info.Signals(end+1).Name      = 'Time';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = 'days since JC';
Info.Signals(end).FileName    = fullfile('Ssc_V3000','Time.bin');

Info.Signals(end+1).Name      = 'fixTime';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = 'days since JC';
Info.Signals(end).FileName    = fullfile('Ssc_V3000','fixTime.bin');

Info.Signals(end+1).Name      = 'heading';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V3000','heading.bin');

Info.Signals(end+1).Name      = 'pitch';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V3000','pitch.bin');

Info.Signals(end+1).Name      = 'roll';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V3000','roll.bin');

Info.Signals(end+1).Name      = 'depth';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V3000','depth.bin');

Info.Signals(end+1).Name      = 'altitudeMeters';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V3000','altitudeMeters.bin');

Info.Signals(end+1).Name      = 'temperature';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V3000','temperature.bin');

Info.Signals(end+1).Name      = 'speed';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V3000','speed.bin');

Info.Signals(end+1).Name      = 'shipHeading';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V3000','shipHeading.bin');

Info.Signals(end+1).Name      = 'magneticVariation';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V3000','magneticVariation.bin');

Info.Signals(end+1).Name      = 'shipLat';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V3000','shipLat.bin');

Info.Signals(end+1).Name      = 'shipLon';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V3000','shipLon.bin');

Info.Signals(end+1).Name      = 'fishLat';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V3000','fishLat.bin');

Info.Signals(end+1).Name      = 'fishLon';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V3000','fishLon.bin');

Info.Signals(end+1).Name      = 'tvgPage';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V3000','tvgPage.bin');

Info.Signals(end+1).Name      = 'fixTimeYear';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V3000','fixTimeYear.bin');

Info.Signals(end+1).Name      = 'fixTimeMonth';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V3000','fixTimeMonth.bin');

Info.Signals(end+1).Name      = 'fixTimeDay';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V3000','fixTimeDay.bin');

Info.Signals(end+1).Name      = 'auxPitch';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V3000','auxPitch.bin');

Info.Signals(end+1).Name      = 'auxRoll';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V3000','auxRoll.bin');

Info.Signals(end+1).Name      = 'auxDepth';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V3000','auxDepth.bin');

Info.Signals(end+1).Name      = 'auxAlt';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V3000','auxAlt.bin');

Info.Signals(end+1).Name      = 'cableOut';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V3000','cableOut.bin');

Info.Signals(end+1).Name      = 'fseconds';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V3000','fseconds.bin');

Info.Signals(end+1).Name      = 'altimeter';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V3000','altimeter.bin');

Info.Signals(end+1).Name      = 'sampleFreq';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V3000','sampleFreq.bin');

% Info.Signals(end+1).Name      = 'depressorType';
% Info.Signals(end).Dimensions  = 'nbPings, 1';
% Info.Signals(end).Storage     = 'uint32';
% Info.Signals(end).Unit        = '';
% Info.Signals(end).FileName    = fullfile('Ssc_V3000','depressorType.bin');
% 
% Info.Signals(end+1).Name      = 'cableType';
% Info.Signals(end).Dimensions  = 'nbPings, 1';
% Info.Signals(end).Storage     = 'uint32';
% Info.Signals(end).Unit        = '';
% Info.Signals(end).FileName    = fullfile('Ssc_V3000','cableType.bin');

Info.Signals(end+1).Name      = 'shieveXoff';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V3000','shieveXoff.bin');

Info.Signals(end+1).Name      = 'shieveYoff';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V3000','shieveYoff.bin');

Info.Signals(end+1).Name      = 'shieveZoff';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V3000','shieveZoff.bin');

Info.Signals(end+1).Name      = 'GPSheight';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V3000','GPSheight.bin');

Info.Signals(end+1).Name      = 'rawDataConfig';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V3000','rawDataConfig.bin');

%info des tables
Info.Images = struct([]);
if ~isempty(Data.SonarLF)
    Info.Images(end+1).Name      = 'SonarLF';
    Info.Images(end).Dimensions  = 'nbPings, nbCol';
    Info.Images(end).Storage     = 'single';
    Info.Images(end).Unit        = 'dB';
    Info.Images(end).FileName    = fullfile('Ssc_V3000','SonarLF.bin');
end

if ~isempty(Data.SonarHF)
    Info.Images(end+1).Name      = 'SonarHF';
    Info.Images(end).Dimensions  = 'nbPings, nbCol';
    Info.Images(end).Storage     = 'single';
    Info.Images(end).Unit        = 'dB';
    Info.Images(end).FileName    = fullfile('Ssc_V3000','SonarHF.bin');
end


if ~isempty(Data.sbp)
    Info.Images(end+1).Name      = 'sbp';
    Info.Images(end).Dimensions  = 'nbPings, nbColSbp';
    Info.Images(end).Storage     = 'single'; %'int64';
    Info.Images(end).Unit        = 'dB';
    Info.Images(end).FileName    = fullfile('Ssc_V3000','sbp.bin');
end

%% Cr�ation du fichier XML d�crivant la donn�e 

nomFicXml = fullfile(nomDirRacine, 'Ssc_V3000.xml');
xml_write(nomFicXml, Info);
flag = exist(nomFicXml, 'file');
if ~flag
    messageErreur(nomFicXml)
    return
end

%% Cr�ation du r�pertoire V3000

nomDirV3000 = fullfile(nomDirRacine, 'Ssc_V3000');
if ~exist(nomDirV3000, 'dir')
    status = mkdir(nomDirV3000);
    if ~status
        messageErreur(nomDirV3000)
        flag = 0;
        return
    end
end

%% Cr�ation des fichiers binaires des signaux

for k=1:length(Info.Signals)
    flag = writeSignal(nomDirRacine, Info.Signals(k), Data.(Info.Signals(k).Name));
    if ~flag
        return
    end
end

%% Cr�ation des fichiers binaires des signaux

for k=1:length(Info.Images)
    Image = Data.(Info.Images(k).Name);
    flag = writeSignal(nomDirRacine, Info.Images(k), Image);
    if ~flag
        return
    end
end

flag = 1;


function X = assemblagePortStarboard(P,S)

nbPings = size(P,1);
try
    X = [fliplr(P(:,:)) NaN(nbPings, 1, 'single') S(:,:)];
    X = cl_memmapfile('Value', X);
catch %#ok<CTCH>
    nbCol = 1 + 2 * size(P,2);
    str1 = 'Cr�ation image Klein ...';
    str2 = 'Creta matrix for Klein sonar image.';
    hw = create_waitbar(Lang(str1,str2), 'N', nbPings);
    X = cl_memmapfile('Value', NaN, 'Size', [nbPings nbCol], 'Format', 'single');
    for k=1:nbPings
        my_waitbar(hw, k, nbPings);
        X(k,:) = [fliplr(P(k,:)) NaN S(k,:)];
    end
    my_close(hw)
end
