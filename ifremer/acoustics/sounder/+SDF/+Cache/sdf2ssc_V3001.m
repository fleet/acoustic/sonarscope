function flag = sdf2ssc_V3001(nomDirRacine, varargin)

%% Lecture du fichier XML d�crivant la donn�e 

nomFicXml = fullfile(nomDirRacine, 'SDF_V3001.xml');
flag = exist(nomFicXml, 'file');
if ~flag
    special_message('FichierNonExistant', nomFicXml)
    return
end
% edit(nomFicXml)
XML = xml_read(nomFicXml);

[nomDirSignal, nomFicSignal] = fileparts(nomFicXml);
nomDirSignal = fullfile(nomDirSignal, nomFicSignal);

%% Lecture des variables

% Lecture du nombre de bytes
[flag, Data.numberBytes] = Read_BinVariable(XML, nomDirSignal, 'numberBytes', 'uint32');
if ~flag
    return
end

% Lecture du pingNumber
[flag, Data.pingNumber] = Read_BinVariable(XML, nomDirSignal, 'pingNumber', 'uint32');
if ~flag
    return
end

% Lecture du nombre de samples
[flag, numSamples] = Read_BinVariable(XML, nomDirSignal, 'numSamples', 'uint32');
if ~flag
    return
end
nbSamples = sum(numSamples);

% Lecture du beamsToDisplay
[flag, Data.beamsToDisplay] = Read_BinVariable(XML, nomDirSignal, 'beamsToDisplay', 'uint32');
if ~flag
    return
end

% Lecture du errorFlags
[flag, Data.errorFlags] = Read_BinVariable(XML, nomDirSignal, 'errorFlags', 'uint32');
if ~flag
    return
end

% Lecture du range
[flag, Data.range] = Read_BinVariable(XML, nomDirSignal, 'range', 'single');
if ~flag
    return
end

% Lecture du speedFish
[flag, Data.speedFish] = Read_BinVariable(XML, nomDirSignal, 'speedFish', 'single');
if ~flag
    return
end

% Lecture du speedSound
[flag, Data.speedSound] = Read_BinVariable(XML, nomDirSignal, 'speedSound', 'single');
if ~flag
    return
end
Data.speedSound = Data.speedSound / 100;

% Lecture du resMode
[flag, Data.resMode] = Read_BinVariable(XML, nomDirSignal, 'resMode', 'single');
if ~flag
    return
end

% Lecture du txWaveform
[flag, Data.txWaveform] = Read_BinVariable(XML, nomDirSignal, 'txWaveform', 'single');
if ~flag
    return
end

% Lecture du respDiv
[flag, Data.respDiv] = Read_BinVariable(XML, nomDirSignal, 'respDiv', 'single');
if ~flag
    return
end

% Lecture du respFreq
[flag, Data.respFreq] = Read_BinVariable(XML, nomDirSignal, 'respFreq', 'single');
if ~flag
    return
end

% Lecture du manualSpeedSwitch
[flag, Data.manualSpeedSwitch] = Read_BinVariable(XML, nomDirSignal, 'manualSpeedSwitch', 'single');
if ~flag
    return
end

% Lecture du despeckleSwitch
[flag, Data.despeckleSwitch] = Read_BinVariable(XML, nomDirSignal, 'despeckleSwitch', 'single');
if ~flag
    return
end

% Lecture du speedFilterSwitch
[flag, Data.speedFilterSwitch] = Read_BinVariable(XML, nomDirSignal, 'speedFilterSwitch', 'single');
if ~flag
    return
end

% Lecture du year
[flag, Year] = Read_BinVariable(XML, nomDirSignal, 'year', 'double');
if ~flag
    return
end

% Lecture du month
[flag, Month] = Read_BinVariable(XML, nomDirSignal, 'month', 'double');
if ~flag
    return
end

% Lecture du day
[flag, Day] = Read_BinVariable(XML, nomDirSignal, 'day', 'double');
if ~flag
    return
end

% Lecture du hour
[flag, Hour] = Read_BinVariable(XML, nomDirSignal, 'hour', 'double');
if ~flag
    return
end

% Lecture du minute
[flag, Minute] = Read_BinVariable(XML, nomDirSignal, 'minute', 'double');
if ~flag
    return
end

% Lecture du second
[flag, Second] = Read_BinVariable(XML, nomDirSignal, 'second', 'double');
if ~flag
    return
end

% Lecture du hSecond
[flag, CentSecond] = Read_BinVariable(XML, nomDirSignal, 'hSecond', 'double');
if ~flag
    return
end

dIfr = dayJma2Ifr(Day(:), Month(:), Year(:));
hIfr = hourHms2Ifr(Hour(:), Minute(:), Second(:)+CentSecond(:)/100);
Data.Time  = cl_time('timeIfr', dIfr, hIfr);

% Lecture du fixTimeHour
[flag, Hour] = Read_BinVariable(XML, nomDirSignal, 'fixTimeHour', 'double');
if ~flag
    return
end

% Lecture du fixTimeMinute
[flag, Minute] = Read_BinVariable(XML, nomDirSignal, 'fixTimeMinute', 'double');
if ~flag
    return
end

% Lecture du fixTimeSecond
[flag, Second] = Read_BinVariable(XML, nomDirSignal, 'fixTimeSecond', 'double');
if ~flag
    return
end

hIfr = hourHms2Ifr(Hour(:), Minute(:), Second(:));
Data.fixTime  = cl_time('timeIfr', dIfr, hIfr);

clear Year Month Day Hour Minute Second CentSecond dIfr hIfr

% Lecture du heading
[flag, Data.heading] = Read_BinVariable(XML, nomDirSignal, 'heading', 'single');
if ~flag
    return
end

% Lecture du pitch
[flag, Data.pitch] = Read_BinVariable(XML, nomDirSignal, 'pitch', 'single');
if ~flag
    return
end

% Lecture du roll
[flag, Data.roll] = Read_BinVariable(XML, nomDirSignal, 'roll', 'single');
if ~flag
    return
end

% Lecture des Voltage Min/Max et Max Pressure.
[flag, pressureMax] = Read_BinVariable(XML, nomDirSignal, 'pressureSensorMax', 'single');
if ~flag
    return
end

[flag, pVoltageMax] = Read_BinVariable(XML, nomDirSignal, 'pressureSensorVoltageMax', 'single');
if ~flag
    return
end

[flag, pVoltageMin] = Read_BinVariable(XML, nomDirSignal, 'pressureSensorVoltageMin', 'single');
if ~flag
    return
end
% Lecture du depth
[flag, depthVolts] = Read_BinVariable(XML, nomDirSignal, 'depth', 'single');
if ~flag
    return
end
% Cf SDF/SDFX Specification - 15300018 - Rev 3.06 - 28-October-2010
% Note 5, pg 15
Data.depth = -((((depthVolts - pVoltageMin) ./ (pVoltageMax - pVoltageMin)) .* pressureMax) - 14.696) / 1.487;

% depth = ((((volts - voltageMin) / (voltageMax - voltageMin)) * maxPressure) - 14.696) / 1.487


% Lecture du altitude
[flag, Data.altitudeMeters] = Read_BinVariable(XML, nomDirSignal, 'altitude', 'single');
if ~flag
    return
end

% Lecture du temperature
[flag, Data.temperature] = Read_BinVariable(XML, nomDirSignal, 'temperature', 'single');
if ~flag
    return
end

% Lecture du speed
[flag, Data.speed] = Read_BinVariable(XML, nomDirSignal, 'speed', 'single');
if ~flag
    return
end

% Lecture du shipHeading
[flag, Data.shipHeading] = Read_BinVariable(XML, nomDirSignal, 'shipHeading', 'single');
if ~flag
    return
end

% Lecture du magneticVariation
[flag, Data.magneticVariation] = Read_BinVariable(XML, nomDirSignal, 'magneticVariation', 'single');
if ~flag
    return
end

% Lecture du shipLat
[flag, Data.shipLat] = Read_BinVariable(XML, nomDirSignal, 'shipLat', 'double');
if ~flag
    return
end
Data.shipLat =  Data.shipLat * (180/pi);

% Lecture du shipLon
[flag, Data.shipLon] = Read_BinVariable(XML, nomDirSignal, 'shipLon', 'double');
if ~flag
    return
end
Data.shipLon = Data.shipLon * (180/pi);

% Lecture du fishLat
[flag, Data.fishLat] = Read_BinVariable(XML, nomDirSignal, 'fishLat', 'double');
if ~flag
    return
end
Data.fishLat(Data.fishLat == 0) = NaN;
Data.fishLat = Data.fishLat * (180/pi);

% Lecture du fishLon
[flag, Data.fishLon] = Read_BinVariable(XML, nomDirSignal, 'fishLon', 'double');
if ~flag
    return
end
Data.fishLon(Data.fishLon == 0) = NaN;
Data.fishLon = Data.fishLon * (180/pi);

% Lecture du tvgPage
[flag, Data.tvgPage] = Read_BinVariable(XML, nomDirSignal, 'tvgPage', 'single');
if ~flag
    return
end

% Lecture du headerSize
[flag, Data.headerSize] = Read_BinVariable(XML, nomDirSignal, 'headerSize', 'uint32');
if ~flag
    return
end

% Lecture du fixTimeYear
[flag, Data.fixTimeYear] = Read_BinVariable(XML, nomDirSignal, 'fixTimeYear', 'uint32');
if ~flag
    return
end

% Lecture du fixTimeMonth
[flag, Data.fixTimeMonth] = Read_BinVariable(XML, nomDirSignal, 'fixTimeMonth', 'uint32');
if ~flag
    return
end

% Lecture du fixTimeDay
[flag, Data.fixTimeDay] = Read_BinVariable(XML, nomDirSignal, 'fixTimeDay', 'uint32');
if ~flag
    return
end

% Lecture du auxPitch
[flag, Data.auxPitch] = Read_BinVariable(XML, nomDirSignal, 'auxPitch', 'single');
if ~flag
    return
end

% Lecture du auxRoll
[flag, Data.auxRoll] = Read_BinVariable(XML, nomDirSignal, 'auxRoll', 'single');
if ~flag
    return
end

% Lecture du auxDepth
[flag, Data.auxDepth] = Read_BinVariable(XML, nomDirSignal, 'auxDepth', 'single');
if ~flag
    return
end

% Lecture du auxAlt
[flag, Data.auxAlt] = Read_BinVariable(XML, nomDirSignal, 'auxAlt', 'single');
if ~flag
    return
end

% Lecture du cableOut
[flag, Data.cableOut] = Read_BinVariable(XML, nomDirSignal, 'cableOut', 'single');
if ~flag
    return
end

% Lecture du fseconds
[flag, Data.fseconds] = Read_BinVariable(XML, nomDirSignal, 'fseconds', 'single');
if ~flag
    return
end

% Lecture du altimeter
[flag, Data.altimeter] = Read_BinVariable(XML, nomDirSignal, 'altimeter', 'single');
if ~flag
    return
end

% Lecture du sampleFreq
[flag, Data.sampleFreq] = Read_BinVariable(XML, nomDirSignal, 'sampleFreq', 'single');
if ~flag
    return
end

% Lecture du depressorType
[flag, Data.depressorType] = Read_BinVariable(XML, nomDirSignal, 'depressorType', 'uint32');
if ~flag
    return
end

% Lecture du cableType
[flag, Data.cableType] = Read_BinVariable(XML, nomDirSignal, 'cableType', 'uint32');
if ~flag
    return
end

% Lecture du shieveXoff
[flag, Data.shieveXoff] = Read_BinVariable(XML, nomDirSignal, 'shieveXoff', 'single');
if ~flag
    return
end

% Lecture du shieveYoff
[flag, Data.shieveYoff] = Read_BinVariable(XML, nomDirSignal, 'shieveYoff', 'single');
if ~flag
    return
end

% Lecture du shieveZoff
[flag, Data.shieveZoff] = Read_BinVariable(XML, nomDirSignal, 'shieveZoff', 'single');
if ~flag
    return
end

% Lecture du GPSheight
[flag, Data.GPSheight] = Read_BinVariable(XML, nomDirSignal, 'GPSheight', 'single');
if ~flag
    return
end

% Lecture du rawDataConfig
[flag, Data.rawDataConfig] = Read_BinVariable(XML, nomDirSignal, 'rawDataConfig', 'uint32');
if ~flag
    return
end

%% Reinterpolation de la navigation Scut (fishNav)

nbPings = XML.NbDatagrams;
nbCol   = max(numSamples);

% Voir mail d'Herv� du 07/02/2021
ind = find(diff(Data.fishLat) ~= 0);
indFirst = [1; ind+1];
if length(indFirst) > 1
    x = interp1(indFirst, Data.fishLon(indFirst), 1:nbPings, 'linear', 'extrap');
    y = interp1(indFirst, Data.fishLat(indFirst), 1:nbPings, 'linear', 'extrap');
else
    x = NaN(nbPings, 1);
    y = NaN(nbPings, 1);
end
%{
figure;
plot(Data.fishLon, Data.fishLat, '+b'); grid on; hold on;
plot(x, y, 'xr');
figure;
plot(Data.fishLon, '+b'); grid on; hold on; plot(x, 'xr');
figure;
plot(Data.heading, '+b'); grid on; hold on; plot(x, 'xr');
figure;
plot(Data.cableOut, '+b'); grid on; hold on; plot(x, 'xr');
%}
Data.fishLon = x;
Data.fishLat = y;

Data.fishNavHeading = calCapFromLatLon(Data.fishLat, Data.fishLon, 'Time', Data.fixTime);

%% Lecture des tables

% Lecture du portlf
[flagportlf, X] = Read_BinTable(XML, nomDirSignal, 'portlf', 'single', nbSamples);
if flagportlf
    % TODO : rechercher le facteur d'�chelle
    if all(numSamples == numSamples(1))
        portlf = reshape(X, [nbCol nbPings]);
        clear X
        portlf = portlf';
    else
        portlf = NaN(nbPings, nbCol, 'single');
        iDeb = 0;
        for k=1:nbPings
            sub = 1:numSamples(k);
            portlf(k,sub) = X(iDeb+sub);
            iDeb = iDeb + numSamples(k);
        end
        clear X
    end
    
    % Mise � NaN des derniers samples qui sont souvent d�connants
    portlf(:,end-15:end) = NaN;
    
    % Filtrage de quelques �chantillons incoh�rents (Fichiers RECOSOM2 :
    % Charline).
    [flag, portlf] 	= filterIncoherentSamples(portlf);
    if ~flag
        return
    end
    portlf = reflec_Enr2dB(portlf);
end

% Lecture du stbdlf
[flagstbdlf, X] = Read_BinTable(XML, nomDirSignal, 'stbdlf', 'single', nbSamples);
if flagstbdlf
    if all(numSamples == numSamples(1))
        stbdlf = reshape(X, [nbCol nbPings]);
        clear X
        stbdlf = stbdlf';
    else
        stbdlf = NaN(nbPings, nbCol, 'single');
        iDeb = 0;
        for k=1:nbPings
            sub = 1:numSamples(k);
            stbdlf(k,sub) = X(iDeb+sub);
            iDeb = iDeb + numSamples(k);
        end
        clear X
    end
    
    % Mise � NaN des derniers samples qui sont souvent d�connants
    stbdlf(:,end-15:end) = NaN;
    
    % Filtrage de quelques �chantillons incoh�rents (Fichiers RECOSOM2 :
    % Charline).
    [flag, stbdlf] 	= filterIncoherentSamples(stbdlf);
    if ~flag
        return
    end
    stbdlf = reflec_Enr2dB(stbdlf);
end

if flagportlf && flagstbdlf
    Data.SonarLF = assemblagePortStarboard(portlf,stbdlf);
else
    Data.SonarLF = [];
end
clear portlf stbdlf

% Lecture du porthf
[flagporthf, X] = Read_BinTable(XML, nomDirSignal, 'porthf', 'single', nbSamples);
if flagporthf
    if all(numSamples == numSamples(1))
        porthf = reshape(X, [nbCol nbPings]);
        clear X
        porthf = porthf';
    else
        porthf = NaN(nbPings, nbCol, 'single');
        iDeb = 0;
        for k=1:nbPings
            sub = 1:numSamples(k);
            porthf(k,sub) = X(iDeb+sub);
            iDeb = iDeb + numSamples(k);
        end
        clear X
    end
    
    % Mise � NaN des derniers samples qui sont souvent d�connants
    porthf(:,end-15:end) = NaN;
    
    % Filtrage de quelques �chantillons incoh�rents (Fichiers RECOSOM2 :
    % Charline).
    [flag, porthf] 	= filterIncoherentSamples(porthf);
    if ~flag
        return
    end
    porthf = reflec_Enr2dB(porthf);
end

% Lecture du stbdhf
[flagstbdhf, X] = Read_BinTable(XML, nomDirSignal, 'stbdhf', 'single', nbSamples);
if flagstbdhf
    if all(numSamples == numSamples(1))
        stbdhf = reshape(X, [nbCol nbPings]);
        clear X
        stbdhf = stbdhf';
    else
        stbdhf = NaN(nbPings, nbCol, 'single');
        iDeb = 0;
        for k=1:nbPings
            sub = 1:numSamples(k);
            stbdhf(k,sub) = X(iDeb+sub);
            iDeb = iDeb + numSamples(k);
        end
        clear X
    end
    
    % Mise � NaN des derniers samples qui sont souvent d�connants
    stbdhf(:,end-15:end) = NaN;
    
    % Filtrage de quelques �chantillons incoh�rents (Fichiers RECOSOM2 :
    % Charline).
    [flag, stbdhf] 	= filterIncoherentSamples(stbdhf);
    if ~flag
        return
    end
    stbdhf = reflec_Enr2dB(stbdhf);
end

if flagstbdhf && flagporthf
    Data.SonarHF = assemblagePortStarboard(porthf,stbdhf);
else
    Data.SonarHF = [];
end
clear porthf stbdhf


% Lecture du sbp
nbSamplesSbp = nbSamples; % TODO : o� peut on trouver le nombre d'�chantillons ?
[flagsbp, Data.sbp] = Read_BinTable(XML, nomDirSignal, 'sbp', 'single', nbSamplesSbp);
if flagsbp
    Data.sbp = reflec_Enr2dB(Data.sbp);
    nbColSbp = size(Data.sbp, 2);
else
    Data.sbp = [];
    nbColSbp = 0;
end

%% 

nbPings = XML.NbDatagrams;
nbCol   = 1 + 2 * nbCol;

%% G�n�ration du XML SonarScope

Info.Title                  = 'V3001'; % Motion Reference Unit
Info.Constructor            = 'Klein';
Info.PageVersion            = 'V3001';
% Info.SystemSerialNumber     = XML.SystemSerialNumber;
Info.TimeOrigin             = '01/01/-4713';
Info.Comments               = 'Sensor sampling rate';

% Info.Dimensions.NbSamples   = nbSamples;
Info.Dimensions.nbPings     = nbPings;
Info.Dimensions.nbCol       = nbCol;
if nbSamplesSbp ~= 0
    Info.Dimensions.nbColSbp = nbColSbp;
end

%info des variables
Info.Signals(1).Name          = 'range';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'm';
Info.Signals(end).FileName    = fullfile('Ssc_V3001', 'range.bin');

Info.Signals(end+1).Name      = 'speedFish';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'cm/s';
Info.Signals(end).FileName    = fullfile('Ssc_V3001', 'speedFish.bin');

Info.Signals(end+1).Name      = 'speedSound';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'm/s';
Info.Signals(end).FileName    = fullfile('Ssc_V3001', 'speedSound.bin');

Info.Signals(end+1).Name      = 'resMode';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V3001', 'resMode.bin');

Info.Signals(end+1).Name      = 'txWaveform';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V3001', 'txWaveform.bin');

Info.Signals(end+1).Name      = 'respDiv';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V3001', 'respDiv.bin');

Info.Signals(end+1).Name      = 'respFreq';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V3001', 'respFreq.bin');

Info.Signals(end+1).Name      = 'manualSpeedSwitch';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V3001', 'manualSpeedSwitch.bin');

Info.Signals(end+1).Name      = 'despeckleSwitch';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V3001', 'despeckleSwitch.bin');

Info.Signals(end+1).Name      = 'speedFilterSwitch';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V3001', 'speedFilterSwitch.bin');

Info.Signals(end+1).Name      = 'Time';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = 'days since JC';
Info.Signals(end).FileName    = fullfile('Ssc_V3001', 'Time.bin');

Info.Signals(end+1).Name      = 'fixTime';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = 'days since JC';
Info.Signals(end).FileName    = fullfile('Ssc_V3001', 'fixTime.bin');

Info.Signals(end+1).Name      = 'heading';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile('Ssc_V3001', 'heading.bin');

Info.Signals(end+1).Name      = 'fishNavHeading';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile('Ssc_V3001', 'fishNavHeading.bin');

Info.Signals(end+1).Name      = 'pitch';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile('Ssc_V3001', 'pitch.bin');

Info.Signals(end+1).Name      = 'roll';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile('Ssc_V3001', 'roll.bin');

Info.Signals(end+1).Name      = 'depth';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'm';
Info.Signals(end).FileName    = fullfile('Ssc_V3001', 'depth.bin');

Info.Signals(end+1).Name      = 'altitudeMeters';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'm';
Info.Signals(end).FileName    = fullfile('Ssc_V3001', 'altitudeMeters.bin');

Info.Signals(end+1).Name      = 'temperature';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile('Ssc_V3001', 'temperature.bin');

Info.Signals(end+1).Name      = 'speed';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'm/s';
Info.Signals(end).FileName    = fullfile('Ssc_V3001', 'speed.bin');

Info.Signals(end+1).Name      = 'shipHeading';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile('Ssc_V3001', 'shipHeading.bin');

Info.Signals(end+1).Name      = 'magneticVariation';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V3001', 'magneticVariation.bin');

Info.Signals(end+1).Name      = 'shipLat';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile('Ssc_V3001', 'shipLat.bin');

Info.Signals(end+1).Name      = 'shipLon';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile('Ssc_V3001', 'shipLon.bin');

Info.Signals(end+1).Name      = 'fishLat';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile('Ssc_V3001', 'fishLat.bin');

Info.Signals(end+1).Name      = 'fishLon';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile('Ssc_V3001', 'fishLon.bin');

Info.Signals(end+1).Name      = 'tvgPage';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V3001', 'tvgPage.bin');

Info.Signals(end+1).Name      = 'fixTimeYear';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V3001', 'fixTimeYear.bin');

Info.Signals(end+1).Name      = 'fixTimeMonth';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V3001', 'fixTimeMonth.bin');

Info.Signals(end+1).Name      = 'fixTimeDay';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V3001', 'fixTimeDay.bin');

Info.Signals(end+1).Name      = 'auxPitch';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile('Ssc_V3001', 'auxPitch.bin');

Info.Signals(end+1).Name      = 'auxRoll';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile('Ssc_V3001', 'auxRoll.bin');

Info.Signals(end+1).Name      = 'auxDepth';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'm';
Info.Signals(end).FileName    = fullfile('Ssc_V3001', 'auxDepth.bin');

Info.Signals(end+1).Name      = 'auxAlt';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'm';
Info.Signals(end).FileName    = fullfile('Ssc_V3001', 'auxAlt.bin');

Info.Signals(end+1).Name      = 'cableOut';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'm';
Info.Signals(end).FileName    = fullfile('Ssc_V3001', 'cableOut.bin');

Info.Signals(end+1).Name      = 'fseconds';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V3001', 'fseconds.bin');

Info.Signals(end+1).Name      = 'altimeter';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'm';
Info.Signals(end).FileName    = fullfile('Ssc_V3001', 'altimeter.bin');

Info.Signals(end+1).Name      = 'sampleFreq';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'Hz';
Info.Signals(end).FileName    = fullfile('Ssc_V3001', 'sampleFreq.bin');

% Info.Signals(end+1).Name      = 'depressorType';
% Info.Signals(end).Dimensions  = 'nbPings, 1';
% Info.Signals(end).Storage     = 'uint32';
% Info.Signals(end).Unit        = '';
% Info.Signals(end).FileName    = fullfile('Ssc_V3001', 'depressorType.bin');
% 
% Info.Signals(end+1).Name      = 'cableType';
% Info.Signals(end).Dimensions  = 'nbPings, 1';
% Info.Signals(end).Storage     = 'uint32';
% Info.Signals(end).Unit        = '';
% Info.Signals(end).FileName    = fullfile('Ssc_V3001', 'cableType.bin');

Info.Signals(end+1).Name      = 'shieveXoff';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V3001', 'shieveXoff.bin');

Info.Signals(end+1).Name      = 'shieveYoff';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V3001', 'shieveYoff.bin');

Info.Signals(end+1).Name      = 'shieveZoff';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V3001', 'shieveZoff.bin');

Info.Signals(end+1).Name      = 'GPSheight';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'm';
Info.Signals(end).FileName    = fullfile('Ssc_V3001', 'GPSheight.bin');

Info.Signals(end+1).Name      = 'rawDataConfig';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V3001', 'rawDataConfig.bin');

%info des tables
Info.Images = struct([]);
if ~isempty(Data.SonarLF)
    Info.Images(end+1).Name      = 'SonarLF';
    Info.Images(end).Dimensions  = 'nbPings, nbCol';
    Info.Images(end).Storage     = 'single';
    Info.Images(end).Unit        = 'dB';
    Info.Images(end).FileName    = fullfile('Ssc_V3001', 'SonarLF.bin');
end

if ~isempty(Data.SonarHF)
    Info.Images(end+1).Name      = 'SonarHF';
    Info.Images(end).Dimensions  = 'nbPings, nbCol';
    Info.Images(end).Storage     = 'single';
    Info.Images(end).Unit        = 'dB';
    Info.Images(end).FileName    = fullfile('Ssc_V3001', 'SonarHF.bin');
end

if ~isempty(Data.sbp)
    Info.Images(end+1).Name      = 'sbp';
    Info.Images(end).Dimensions  = 'nbPings, nbColSbp';
    Info.Images(end).Storage     = 'single'; %'int64';
    Info.Images(end).Unit        = 'dB';
    Info.Images(end).FileName    = fullfile('Ssc_V3001', 'sbp.bin');
end

%% Cr�ation du fichier XML d�crivant la donn�e 

nomFicXml = fullfile(nomDirRacine, 'Ssc_V3001.xml');
xml_write(nomFicXml, Info);
flag = exist(nomFicXml, 'file');
if ~flag
    messageErreur(nomFicXml)
    return
end

%% Cr�ation du r�pertoire V3001

nomDirV3001 = fullfile(nomDirRacine, 'Ssc_V3001');
if ~exist(nomDirV3001, 'dir')
    status = mkdir(nomDirV3001);
    if ~status
        messageErreur(nomDirV3001)
        flag = 0;
        return
    end
end

%% Cr�ation des fichiers binaires des signaux

for k=1:length(Info.Signals)
    flag = writeSignal(nomDirRacine, Info.Signals(k), Data.(Info.Signals(k).Name));
    if ~flag
        return
    end
end

%% Cr�ation des fichiers binaires des signaux

for k=1:length(Info.Images)
    Image = Data.(Info.Images(k).Name);
    flag = writeSignal(nomDirRacine, Info.Images(k), Image);
    if ~flag
        return
    end
end

flag = 1;


function X = assemblagePortStarboard(P,S)

nbPings = size(P,1);
try
    X = [fliplr(P(:,:)) NaN(nbPings, 1, 'single') S(:,:)];
    X = cl_memmapfile('Value', X);
catch %#ok<CTCH>
    nbCol = 1 + 2 * size(P,2);
    str1 = 'Cr�ation image Klein ...';
    str2 = 'Creta matrix for Klein sonar image.';
    hw = create_waitbar(Lang(str1,str2), 'N', nbPings);
    X = cl_memmapfile('Value', NaN, 'Size', [nbPings nbCol], 'Format', 'single');
    for k=1:nbPings
        my_waitbar(hw, k, nbPings);
        X(k,:) = [fliplr(P(k,:)) NaN S(k,:)];
    end
    my_close(hw)
end
