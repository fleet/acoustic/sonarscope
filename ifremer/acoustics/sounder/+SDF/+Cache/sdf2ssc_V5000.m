function flag = sdf2ssc_V5000(nomDirRacine, varargin)

%% Lecture du fichier XML d�crivant la donn�e 

nomFicXml = fullfile(nomDirRacine, 'SDF_V5000.xml');
flag = exist(nomFicXml, 'file');
if ~flag
    special_message('FichierNonExistant', nomFicXml)
    return
end
% edit(nomFicXml)
XML = xml_read(nomFicXml);

[nomDirSignal, nomFicSignal] = fileparts(nomFicXml);
nomDirSignal = fullfile(nomDirSignal, nomFicSignal);

%% Lecture des variables

% Lecture du nombre de bytes
[flag, Data.numberBytes] = Read_BinVariable(XML, nomDirSignal, 'numberBytes', 'uint32');
if ~flag
    return
end

% Lecture du pingNumber
[flag, Data.pingNumber] = Read_BinVariable(XML, nomDirSignal, 'pingNumber', 'uint32');
if ~flag
    return
end

% Lecture du nombre de samples
[flag, numSamples] = Read_BinVariable(XML, nomDirSignal, 'numSamples', 'uint32');
if ~flag
    return
end
nbSamples = sum(numSamples);

% Lecture du beamsToDisplay
[flag, Data.beamsToDisplay] = Read_BinVariable(XML, nomDirSignal, 'beamsToDisplay', 'uint32');
if ~flag
    return
end

% Lecture du errorFlags
[flag, Data.errorFlags] = Read_BinVariable(XML, nomDirSignal, 'errorFlags', 'uint32');
if ~flag
    return
end

% Lecture du range
[flag, Data.range] = Read_BinVariable(XML, nomDirSignal, 'range', 'uint32');
if ~flag
    return
end

% Lecture du speedFish
[flag, Data.speedFish] = Read_BinVariable(XML, nomDirSignal, 'speedFish', 'uint32');
if ~flag
    return
end

% Lecture du speedSound
[flag, Data.speedSound] = Read_BinVariable(XML, nomDirSignal, 'speedSound', 'uint32');
if ~flag
    return
end

% Lecture du resMode
[flag, Data.resMode] = Read_BinVariable(XML, nomDirSignal, 'resMode', 'uint32');
if ~flag
    return
end

% Lecture du txWaveform
[flag, Data.txWaveform] = Read_BinVariable(XML, nomDirSignal, 'txWaveform', 'uint32');
if ~flag
    return
end

% Lecture du respDiv
[flag, Data.respDiv] = Read_BinVariable(XML, nomDirSignal, 'respDiv', 'uint32');
if ~flag
    return
end

% Lecture du respFreq
[flag, Data.respFreq] = Read_BinVariable(XML, nomDirSignal, 'respFreq', 'uint32');
if ~flag
    return
end

% Lecture du manualSpeedSwitch
[flag, Data.manualSpeedSwitch] = Read_BinVariable(XML, nomDirSignal, 'manualSpeedSwitch', 'uint32');
if ~flag
    return
end

% Lecture du despeckleSwitch
[flag, Data.despeckleSwitch] = Read_BinVariable(XML, nomDirSignal, 'despeckleSwitch', 'uint32');
if ~flag
    return
end

% Lecture du speedFilterSwitch
[flag, Data.speedFilterSwitch] = Read_BinVariable(XML, nomDirSignal, 'speedFilterSwitch', 'uint32');
if ~flag
    return
end

% Lecture du year
[flag, Data.year] = Read_BinVariable(XML, nomDirSignal, 'year', 'uint32');
if ~flag
    return
end

% Lecture du month
[flag, Data.month] = Read_BinVariable(XML, nomDirSignal, 'month', 'uint32');
if ~flag
    return
end

% Lecture du day
[flag, Data.day] = Read_BinVariable(XML, nomDirSignal, 'day', 'uint32');
if ~flag
    return
end

% Lecture du hour
[flag, Data.hour] = Read_BinVariable(XML, nomDirSignal, 'hour', 'uint32');
if ~flag
    return
end

% Lecture du minute
[flag, Data.minute] = Read_BinVariable(XML, nomDirSignal, 'minute', 'uint32');
if ~flag
    return
end

% Lecture du second
[flag, Data.second] = Read_BinVariable(XML, nomDirSignal, 'second', 'uint32');
if ~flag
    return
end

% Lecture du hSecond
[flag, Data.hSecond] = Read_BinVariable(XML, nomDirSignal, 'hSecond', 'uint32');
if ~flag
    return
end


% Lecture du fixTimeHour
[flag, Data.fixTimeHour] = Read_BinVariable(XML, nomDirSignal, 'fixTimeHour', 'uint32');
if ~flag
    return
end

% Lecture du fixTimeMinute
[flag, Data.fixTimeMinute] = Read_BinVariable(XML, nomDirSignal, 'fixTimeMinute', 'uint32');
if ~flag
    return
end

% Lecture du fixTimeSecond
[flag, Data.fixTimeSecond] = Read_BinVariable(XML, nomDirSignal, 'fixTimeSecond', 'single');
if ~flag
    return
end

% Lecture du heading
[flag, Data.heading] = Read_BinVariable(XML, nomDirSignal, 'heading', 'single');
if ~flag
    return
end

% Lecture du pitch
[flag, Data.pitch] = Read_BinVariable(XML, nomDirSignal, 'pitch', 'single');
if ~flag
    return
end

% Lecture du roll
[flag, Data.roll] = Read_BinVariable(XML, nomDirSignal, 'roll', 'single');
if ~flag
    return
end

% Lecture des Voltage Min/Max et Max Pressure.
[flag, pressureMax] = Read_BinVariable(XML, nomDirSignal, 'pressureSensorMax', 'single');
if ~flag
    return
end

[flag, pVoltageMax] = Read_BinVariable(XML, nomDirSignal, 'pressureSensorVoltageMax', 'single');
if ~flag
    return
end

[flag, pVoltageMin] = Read_BinVariable(XML, nomDirSignal, 'pressureSensorVoltageMin', 'single');
if ~flag
    return
end
% Lecture du depth
[flag, depthVolts] = Read_BinVariable(XML, nomDirSignal, 'depth', 'single');
if ~flag
    return
end
% Cf SDF/SDFX Specification - 15300018 - Rev 3.06 - 28-October-2010
% Note 5, pg 15
Data.depth = -((((depthVolts - pVoltageMin) / (pVoltageMax - pVoltageMin)) * pressureMax) - 14.696) / 1.487;


% Lecture du altitude
[flag, Data.altitude] = Read_BinVariable(XML, nomDirSignal, 'altitude', 'single');
if ~flag
    return
end

% Lecture du temperature
[flag, Data.temperature] = Read_BinVariable(XML, nomDirSignal, 'temperature', 'single');
if ~flag
    return
end

% Lecture du speed
[flag, Data.speed] = Read_BinVariable(XML, nomDirSignal, 'speed', 'single');
if ~flag
    return
end

% Lecture du shipHeading
[flag, Data.shipHeading] = Read_BinVariable(XML, nomDirSignal, 'shipHeading', 'single');
if ~flag
    return
end

% Lecture du magneticVariation
[flag, Data.magneticVariation] = Read_BinVariable(XML, nomDirSignal, 'magneticVariation', 'single');
if ~flag
    return
end

% Lecture du shipLat
[flag, Data.shipLat] = Read_BinVariable(XML, nomDirSignal, 'shipLat', 'double');
if ~flag
    return
end

% Lecture du shipLon
[flag, Data.shipLon] = Read_BinVariable(XML, nomDirSignal, 'shipLon', 'double');
if ~flag
    return
end

% Lecture du shipLat
[flag, Data.fishLat] = Read_BinVariable(XML, nomDirSignal, 'fishLat', 'double');
if ~flag
    return
end

% Lecture du fishLon
[flag, Data.fishLon] = Read_BinVariable(XML, nomDirSignal, 'fishLon', 'double');
if ~flag
    return
end

% Lecture du tvgPage
[flag, Data.tvgPage] = Read_BinVariable(XML, nomDirSignal, 'tvgPage', 'uint32');
if ~flag
    return
end

% Lecture du headerSize
[flag, Data.headerSize] = Read_BinVariable(XML, nomDirSignal, 'headerSize', 'uint32');
if ~flag
    return
end

% Lecture du fixTimeYear
[flag, Data.fixTimeYear] = Read_BinVariable(XML, nomDirSignal, 'fixTimeYear', 'uint32');
if ~flag
    return
end

% Lecture du fixTimeMonth
[flag, Data.fixTimeMonth] = Read_BinVariable(XML, nomDirSignal, 'fixTimeMonth', 'uint32');
if ~flag
    return
end

% Lecture du fixTimeDay
[flag, Data.fixTimeDay] = Read_BinVariable(XML, nomDirSignal, 'fixTimeDay', 'uint32');
if ~flag
    return
end

% Lecture du auxPitch
[flag, Data.auxPitch] = Read_BinVariable(XML, nomDirSignal, 'auxPitch', 'single');
if ~flag
    return
end

% Lecture du auxRoll
[flag, Data.auxRoll] = Read_BinVariable(XML, nomDirSignal, 'auxRoll', 'single');
if ~flag
    return
end

% Lecture du auxDepth
[flag, Data.auxDepth] = Read_BinVariable(XML, nomDirSignal, 'auxDepth', 'single');
if ~flag
    return
end

% Lecture du auxAlt
[flag, Data.auxAlt] = Read_BinVariable(XML, nomDirSignal, 'auxAlt', 'single');
if ~flag
    return
end

% Lecture du cableOut
[flag, Data.cableOut] = Read_BinVariable(XML, nomDirSignal, 'cableOut', 'single');
if ~flag
    return
end

% Lecture du fseconds
[flag, Data.fseconds] = Read_BinVariable(XML, nomDirSignal, 'fseconds', 'single');
if ~flag
    return
end

% Lecture du altimeter
[flag, Data.altimeter] = Read_BinVariable(XML, nomDirSignal, 'altimeter', 'uint32');
if ~flag
    return
end

% Lecture du sampleFreq
[flag, Data.sampleFreq] = Read_BinVariable(XML, nomDirSignal, 'sampleFreq', 'uint32');
if ~flag
    return
end

% Lecture du depressorType
[flag, Data.depressorType] = Read_BinVariable(XML, nomDirSignal, 'depressorType', 'uint32');
if ~flag
    return
end

% Lecture du cableType
[flag, Data.cableType] = Read_BinVariable(XML, nomDirSignal, 'cableType', 'uint32');
if ~flag
    return
end

% Lecture du shieveXoff
[flag, Data.shieveXoff] = Read_BinVariable(XML, nomDirSignal, 'shieveXoff', 'single');
if ~flag
    return
end

% Lecture du shieveYoff
[flag, Data.shieveYoff] = Read_BinVariable(XML, nomDirSignal, 'shieveYoff', 'single');
if ~flag
    return
end

% Lecture du shieveZoff
[flag, Data.shieveZoff] = Read_BinVariable(XML, nomDirSignal, 'shieveYoff', 'single');
if ~flag
    return
end

% Lecture du GPSheight
[flag, Data.GPSheight] = Read_BinVariable(XML, nomDirSignal, 'GPSheight', 'single');
if ~flag
    return
end

% Lecture du rawDataConfig
[flag, Data.rawDataConfig] = Read_BinVariable(XML, nomDirSignal, 'rawDataConfig', 'uint32');
if ~flag
    return
end

% -----------------
%% Lecture des tables
% -----------------
% Lecture du chan1Data
[flag, Data.chan1Data] = Read_BinTable(XML, nomDirSignal, 'chan1Data', 'uint8', nbSamples);
if ~flag
    %return
end

% Lecture du chan2Data
[flag, Data.chan2Data] = Read_BinTable(XML, nomDirSignal, 'chan2Data', 'uint8', nbSamples);
if ~flag
    %return
end

% Lecture du chan3Data
[flag, Data.chan3Data] = Read_BinTable(XML, nomDirSignal, 'chan3Data', 'uint8', nbSamples);
if ~flag
    %return
end

% Lecture du chan4Data
[flag, Data.chan4Data] = Read_BinTable(XML, nomDirSignal, 'chan4Data', 'uint8', nbSamples);
if ~flag
    %return
end

% Lecture du chan5Data
[flag, Data.chan5Data] = Read_BinTable(XML, nomDirSignal, 'chan5Data', 'uint8', nbSamples);
if ~flag
    %return
end

% Lecture du chan6Data
[flag, Data.chan6Data] = Read_BinTable(XML, nomDirSignal, 'chan6Data', 'uint8', nbSamples);
if ~flag
    %return
end

% Lecture du chan7Data
[flag, Data.chan7Data] = Read_BinTable(XML, nomDirSignal, 'chan7Data', 'uint8', nbSamples);
if ~flag
    %return
end

% Lecture du chan8Data
[flag, Data.chan8Data] = Read_BinTable(XML, nomDirSignal, 'chan8Data', 'uint8', nbSamples);
if ~flag
    %return
end

% Lecture du chan9Data
[flag, Data.chan9Data] = Read_BinTable(XML, nomDirSignal, 'chan9Data', 'uint8', nbSamples);
if ~flag
    %return
end

% Lecture du chan10Data
[flag, Data.chan10Data] = Read_BinTable(XML, nomDirSignal, 'chan10Data', 'uint8', nbSamples);
if ~flag
    %return
end

% Lecture du bathyPort1i
[flag, Data.bathyPort1i] = Read_BinTable(XML, nomDirSignal, 'bathyPort1i', 'int8', nbSamples);
if ~flag
    %return
end

% Lecture du bathyPort1q
[flag, Data.bathyPort1q] = Read_BinTable(XML, nomDirSignal, 'bathyPort1q', 'int8', nbSamples);
if ~flag
    %return
end

% Lecture du bathyPort2i
[flag, Data.bathyPort2i] = Read_BinTable(XML, nomDirSignal, 'bathyPort2i', 'int8', nbSamples);
if ~flag
    %return
end

% Lecture du bathyPort2q
[flag, Data.bathyPort2q] = Read_BinTable(XML, nomDirSignal, 'bathyPort2q', 'int8', nbSamples);
if ~flag
    %return
end

% Lecture du bathyPort3i
[flag, Data.bathyPort3i] = Read_BinTable(XML, nomDirSignal, 'bathyPort3i', 'int8', nbSamples);
if ~flag
    %return
end

% Lecture du bathyPort3q
[flag, Data.bathyPort3q] = Read_BinTable(XML, nomDirSignal, 'bathyPort3q', 'int8', nbSamples);
if ~flag
    %return
end

% Lecture du bathyStbd1i
[flag, Data.bathyStbd1i] = Read_BinTable(XML, nomDirSignal, 'bathyStbd1i', 'int8', nbSamples);
if ~flag
    %return
end

% Lecture du bathyStbd1q
[flag, Data.bathyStbd1q] = Read_BinTable(XML, nomDirSignal, 'bathyStbd1q', 'int8', nbSamples);
if ~flag
    %return
end

% Lecture du bathyStbd2i
[flag, Data.bathyStbd2i] = Read_BinTable(XML, nomDirSignal, 'bathyStbd2i', 'int8', nbSamples);
if ~flag
    %return
end

% Lecture du bathyStbd2q
[flag, Data.bathyStbd2q] = Read_BinTable(XML, nomDirSignal, 'bathyStbd2q', 'int8', nbSamples);
if ~flag
    %return
end

% Lecture du bathyStbd3i
[flag, Data.bathyStbd3i] = Read_BinTable(XML, nomDirSignal, 'bathyStbd3i', 'int8', nbSamples);
if ~flag
    %return
end

% Lecture du bathyStbd3q
[flag, Data.bathyStbd3q] = Read_BinTable(XML, nomDirSignal, 'bathyStbd3q', 'int8', nbSamples);
if ~flag
    %return
end

% Lecture du echo1
[flag, Data.echo1] = Read_BinTable(XML, nomDirSignal, 'echo1', 'int8', nbSamples);
if ~flag
    %return
end

% Lecture du echo2
[flag, Data.echo2] = Read_BinTable(XML, nomDirSignal, 'echo2', 'int8', nbSamples);
if ~flag
    %return
end

% Lecture du subBottom1
[flag, Data.subBottom1] = Read_BinTable(XML, nomDirSignal, 'subBottom1', 'int8', nbSamples);
if ~flag
    %return
end

% Lecture du subBottom2
[flag, Data.subBottom2] = Read_BinTable(XML, nomDirSignal, 'subBottom2', 'int8', nbSamples);
if ~flag
    %return
end

% Lecture du rollSensor
[flag, Data.rollSensor] = Read_BinTable(XML, nomDirSignal, 'rollSensor', 'int8', nbSamples);
if ~flag
    %return
end

% Lecture du yawRate
[flag, Data.yawRate] = Read_BinTable(XML, nomDirSignal, 'yawRate', 'int8', nbSamples);
if ~flag
    %return
end

% Lecture du rawdataPort1i
[flag, Data.rawdataPort1i] = Read_BinTable(XML, nomDirSignal, 'rawdataPort1i', 'int8', nbSamples);
if ~flag
    %return
end

% Lecture du rawdataPort1q
[flag, Data.rawdataPort1q] = Read_BinTable(XML, nomDirSignal, 'rawdataPort1q', 'int8', nbSamples);
if ~flag
    %return
end

% Lecture du rawdataPort2i
[flag, Data.rawdataPort2i] = Read_BinTable(XML, nomDirSignal, 'rawdataPort2i', 'int8', nbSamples);
if ~flag
    %return
end

% Lecture du rawdataPort2q
[flag, Data.rawdataPort2q] = Read_BinTable(XML, nomDirSignal, 'rawdataPort2q', 'int8', nbSamples);
if ~flag
    %return
end

% Lecture du rawdataPort3i
[flag, Data.rawdataPort3i] = Read_BinTable(XML, nomDirSignal, 'rawdataPort3i', 'int8', nbSamples);
if ~flag
    %return
end

% Lecture du rawdataPort3q
[flag, Data.rawdataPort3q] = Read_BinTable(XML, nomDirSignal, 'rawdataPort3q', 'int8', nbSamples);
if ~flag
    %return
end

% Lecture du rawdataPort4i
[flag, Data.rawdataPort4i] = Read_BinTable(XML, nomDirSignal, 'rawdataPort4i', 'int8', nbSamples);
if ~flag
    %return
end

% Lecture du rawdataPort4q
[flag, Data.rawdataPort4q] = Read_BinTable(XML, nomDirSignal, 'rawdataPort4q', 'int8', nbSamples);
if ~flag
    %return
end

% Lecture du rawdataPort5i
[flag, Data.rawdataPort5i] = Read_BinTable(XML, nomDirSignal, 'rawdataPort5i', 'int8', nbSamples);
if ~flag
    %return
end

% Lecture du rawdataPort5q
[flag, Data.rawdataPort5q] = Read_BinTable(XML, nomDirSignal, 'rawdataPort5q', 'int8', nbSamples);
if ~flag
    %return
end

% Lecture du rawdataPort6i
[flag, Data.rawdataPort6i] = Read_BinTable(XML, nomDirSignal, 'rawdataPort6i', 'int8', nbSamples);
if ~flag
    %return
end

% Lecture du rawdataPort6q
[flag, Data.rawdataPort6q] = Read_BinTable(XML, nomDirSignal, 'rawdataPort6q', 'int8', nbSamples);
if ~flag
    %return
end

% Lecture du rawdataPort7i
[flag, Data.rawdataPort7i] = Read_BinTable(XML, nomDirSignal, 'rawdataPort7i', 'int8', nbSamples);
if ~flag
    %return
end

% Lecture du rawdataPort7q
[flag, Data.rawdataPort7q] = Read_BinTable(XML, nomDirSignal, 'rawdataPort7q', 'int8', nbSamples);
if ~flag
    %return
end

% Lecture du rawdataPort8i
[flag, Data.rawdataPort8i] = Read_BinTable(XML, nomDirSignal, 'rawdataPort8i', 'int8', nbSamples);
if ~flag
    %return
end

% Lecture du rawdataPort8q
[flag, Data.rawdataPort8q] = Read_BinTable(XML, nomDirSignal, 'rawdataPort8q', 'int8', nbSamples);
if ~flag
    %return
end

% Lecture du rawdataPort9i
[flag, Data.rawdataPort9i] = Read_BinTable(XML, nomDirSignal, 'rawdataPort9i', 'int8', nbSamples);
if ~flag
    %return
end

% Lecture du rawdataPort9q
[flag, Data.rawdataPort9q] = Read_BinTable(XML, nomDirSignal, 'rawdataPort9q', 'int8', nbSamples);
if ~flag
    %return
end

% Lecture du rawdataPort10i
[flag, Data.rawdataPort10i] = Read_BinTable(XML, nomDirSignal, 'rawdataPort10i', 'int8', nbSamples);
if ~flag
    %return
end

% Lecture du rawdataPort10q
[flag, Data.rawdataPort10q] = Read_BinTable(XML, nomDirSignal, 'rawdataPort10q', 'int8', nbSamples);
if ~flag
    %return
end

% Lecture du rawdataPort11i
[flag, Data.rawdataPort11i] = Read_BinTable(XML, nomDirSignal, 'rawdataPort11i', 'int8', nbSamples);
if ~flag
    %return
end

% Lecture du rawdataPort11q
[flag, Data.rawdataPort11q] = Read_BinTable(XML, nomDirSignal, 'rawdataPort11q', 'int8', nbSamples);
if ~flag
    %return
end

% Lecture du rawdataPort12i
[flag, Data.rawdataPort12i] = Read_BinTable(XML, nomDirSignal, 'rawdataPort12i', 'int8', nbSamples);
if ~flag
    %return
end

% Lecture du rawdataPort12q
[flag, Data.rawdataPort12q] = Read_BinTable(XML, nomDirSignal, 'rawdataPort12q', 'int8', nbSamples);
if ~flag
    %return
end

% Lecture du rawdataPort13i
[flag, Data.rawdataPort13i] = Read_BinTable(XML, nomDirSignal, 'rawdataPort13i', 'int8', nbSamples);
if ~flag
    %return
end

% Lecture du rawdataPort13q
[flag, Data.rawdataPort13q] = Read_BinTable(XML, nomDirSignal, 'rawdataPort13q', 'int8', nbSamples);
if ~flag
    %return
end

% Lecture du rawdataPort14i
[flag, Data.rawdataPort14i] = Read_BinTable(XML, nomDirSignal, 'rawdataPort14i', 'int8', nbSamples);
if ~flag
    %return
end

% Lecture du rawdataPort14q
[flag, Data.rawdataPort14q] = Read_BinTable(XML, nomDirSignal, 'rawdataPort14q', 'int8', nbSamples);
if ~flag
    %return
end

% Lecture du rawdataStbd1i
[flag, Data.rawdataStbd1i] = Read_BinTable(XML, nomDirSignal, 'rawdataStbd1i', 'int8', nbSamples);
if ~flag
    %return
end

% Lecture du rawdataStbd1q
[flag, Data.rawdataStbd1q] = Read_BinTable(XML, nomDirSignal, 'rawdataStbd1q', 'int8', nbSamples);
if ~flag
    %return
end

% Lecture du rawdataStbd2i
[flag, Data.rawdataStbd2i] = Read_BinTable(XML, nomDirSignal, 'rawdataStbd2i', 'int8', nbSamples);
if ~flag
    %return
end

% Lecture du rawdataStbd2q
[flag, Data.rawdataStbd2q] = Read_BinTable(XML, nomDirSignal, 'rawdataStbd2q', 'int8', nbSamples);
if ~flag
    %return
end

% Lecture du rawdataStbd3i
[flag, Data.rawdataStbd3i] = Read_BinTable(XML, nomDirSignal, 'rawdataStbd3i', 'int8', nbSamples);
if ~flag
    %return
end

% Lecture du rawdataStbd3q
[flag, Data.rawdataStbd3q] = Read_BinTable(XML, nomDirSignal, 'rawdataStbd3q', 'int8', nbSamples);
if ~flag
    %return
end

% Lecture du rawdataStbd4i
[flag, Data.rawdataStbd4i] = Read_BinTable(XML, nomDirSignal, 'rawdataStbd4i', 'int8', nbSamples);
if ~flag
    %return
end

% Lecture du rawdataStbd4q
[flag, Data.rawdataStbd4q] = Read_BinTable(XML, nomDirSignal, 'rawdataStbd4q', 'int8', nbSamples);
if ~flag
    %return
end

% Lecture du rawdataStbd5i
[flag, Data.rawdataStbd5i] = Read_BinTable(XML, nomDirSignal, 'rawdataStbd5i', 'int8', nbSamples);
if ~flag
    %return
end

% Lecture du rawdataStbd5q
[flag, Data.rawdataStbd5q] = Read_BinTable(XML, nomDirSignal, 'rawdataStbd5q', 'int8', nbSamples);
if ~flag
    %return
end

% Lecture du rawdataStbd6i
[flag, Data.rawdataStbd6i] = Read_BinTable(XML, nomDirSignal, 'rawdataStbd6i', 'int8', nbSamples);
if ~flag
    %return
end

% Lecture du rawdataStbd6q
[flag, Data.rawdataStbd6q] = Read_BinTable(XML, nomDirSignal, 'rawdataStbd6q', 'int8', nbSamples);
if ~flag
    %return
end

% Lecture du rawdataStbd7i
[flag, Data.rawdataStbd7i] = Read_BinTable(XML, nomDirSignal, 'rawdataStbd7i', 'int8', nbSamples);
if ~flag
    %return
end

% Lecture du rawdataStbd7q
[flag, Data.rawdataStbd7q] = Read_BinTable(XML, nomDirSignal, 'rawdataStbd7q', 'int8', nbSamples);
if ~flag
    %return
end

% Lecture du rawdataStbd8i
[flag, Data.rawdataStbd8i] = Read_BinTable(XML, nomDirSignal, 'rawdataStbd8i', 'int8', nbSamples);
if ~flag
    %return
end

% Lecture du rawdataStbd8q
[flag, Data.rawdataStbd8q] = Read_BinTable(XML, nomDirSignal, 'rawdataStbd8q', 'int8', nbSamples);
if ~flag
    %return
end

% Lecture du rawdataStbd9i
[flag, Data.rawdataStbd9i] = Read_BinTable(XML, nomDirSignal, 'rawdataStbd9i', 'int8', nbSamples);
if ~flag
    %return
end

% Lecture du rawdataStbd9q
[flag, Data.rawdataStbd9q] = Read_BinTable(XML, nomDirSignal, 'rawdataStbd9q', 'int8', nbSamples);
if ~flag
    %return
end

% Lecture du rawdataStbd10i
[flag, Data.rawdataStbd10i] = Read_BinTable(XML, nomDirSignal, 'rawdataStbd10i', 'int8', nbSamples);
if ~flag
    %return
end

% Lecture du rawdataStbd10q
[flag, Data.rawdataStbd10q] = Read_BinTable(XML, nomDirSignal, 'rawdataStbd10q', 'int8', nbSamples);
if ~flag
    %return
end

% Lecture du rawdataStbd11i
[flag, Data.rawdataStbd11i] = Read_BinTable(XML, nomDirSignal, 'rawdataStbd11i', 'int8', nbSamples);
if ~flag
    %return
end

% Lecture du rawdataStbd11q
[flag, Data.rawdataStbd11q] = Read_BinTable(XML, nomDirSignal, 'rawdataStbd11q', 'int8', nbSamples);
if ~flag
    %return
end

% Lecture du rawdataStbd12i
[flag, Data.rawdataStbd12i] = Read_BinTable(XML, nomDirSignal, 'rawdataStbd12i', 'int8', nbSamples);
if ~flag
    %return
end

% Lecture du rawdataStbd12q
[flag, Data.rawdataStbd12q] = Read_BinTable(XML, nomDirSignal, 'rawdataStbd12q', 'int8', nbSamples);
if ~flag
    %return
end

% Lecture du rawdataStbd13i
[flag, Data.rawdataStbd13i] = Read_BinTable(XML, nomDirSignal, 'rawdataStbd13i', 'int8', nbSamples);
if ~flag
    %return
end

% Lecture du rawdataStbd13q
[flag, Data.rawdataStbd13q] = Read_BinTable(XML, nomDirSignal, 'rawdataStbd13q', 'int8', nbSamples);
if ~flag
    %return
end

% Lecture du rawdataStbd14i
[flag, Data.rawdataStbd14i] = Read_BinTable(XML, nomDirSignal, 'rawdataStbd14i', 'int8', nbSamples);
if ~flag
    %return
end

% Lecture du rawdataStbd14q
[flag, Data.rawdataStbd14q] = Read_BinTable(XML, nomDirSignal, 'rawdataStbd14q', 'int8', nbSamples);
if ~flag
    %return
end


% ----------------------------
%% G�n�ration du XML SonarScope

Info.Title                  = 'V5000'; % Motion Reference Unit
Info.Constructor            = 'Klein';
Info.EmModel                = XML.Model;
Info.SystemSerialNumber     = XML.SystemSerialNumber;
Info.TimeOrigin             = '01/01/-4713';
Info.Comments               = 'Sensor sampling rate';

Info.Dimensions.NbSamples     = nbSamples;

%info des variables
Info.Signals(1).Name          = 'range';
Info.Signals(end).Dimensions  = 'NbXML, 1';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','range.bin');

Info.Signals(end+1).Name      = 'speedFish';
Info.Signals(end).Dimensions  = 'NbXML, 1';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = 'cm/s';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','speedFish.bin');

Info.Signals(end+1).Name      = 'speedSound';
Info.Signals(end).Dimensions  = 'NbXML, 1';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','speedSound.bin');

Info.Signals(end+1).Name      = 'resMode';
Info.Signals(end).Dimensions  = 'NbXML, 1';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','resMode.bin');

Info.Signals(end+1).Name      = 'txWaveform';
Info.Signals(end).Dimensions  = 'NbXML, 1';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','txWaveform.bin');

Info.Signals(end+1).Name      = 'respDiv';
Info.Signals(end).Dimensions  = 'NbXML, 1';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','respDiv.bin');

Info.Signals(end+1).Name      = 'respFreq';
Info.Signals(end).Dimensions  = 'NbXML, 1';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','respFreq.bin');

Info.Signals(end+1).Name      = 'manualSpeedSwitch';
Info.Signals(end).Dimensions  = 'NbXML, 1';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','manualSpeedSwitch.bin');

Info.Signals(end+1).Name      = 'despeckleSwitch';
Info.Signals(end).Dimensions  = 'NbXML, 1';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','despeckleSwitch.bin');

Info.Signals(end+1).Name      = 'speedFilterSwitch';
Info.Signals(end).Dimensions  = 'NbXML, 1';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','speedFilterSwitch.bin');

Info.Signals(end+1).Name      = 'year';
Info.Signals(end).Dimensions  = 'NbXML, 1';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','year.bin');

Info.Signals(end+1).Name      = 'month';
Info.Signals(end).Dimensions  = 'NbXML, 1';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','month.bin');

Info.Signals(end+1).Name      = 'day';
Info.Signals(end).Dimensions  = 'NbXML, 1';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','day.bin');

Info.Signals(end+1).Name      = 'hour';
Info.Signals(end).Dimensions  = 'NbXML, 1';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','hour.bin');

Info.Signals(end+1).Name      = 'minute';
Info.Signals(end).Dimensions  = 'NbXML, 1';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','minute.bin');

Info.Signals(end+1).Name      = 'second';
Info.Signals(end).Dimensions  = 'NbXML, 1';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','second.bin');

Info.Signals(end+1).Name      = 'hSecond';
Info.Signals(end).Dimensions  = 'NbXML, 1';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','hSecond.bin');

Info.Signals(end+1).Name      = 'fixTimeHour';
Info.Signals(end).Dimensions  = 'NbXML, 1';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','fixTimeHour.bin');

Info.Signals(end+1).Name      = 'fixTimeMinute';
Info.Signals(end).Dimensions  = 'NbXML, 1';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','fixTimeMinute.bin');

Info.Signals(end+1).Name      = 'fixTimeSecond';
Info.Signals(end).Dimensions  = 'NbXML, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','fixTimeSecond.bin');

Info.Signals(end+1).Name      = 'heading';
Info.Signals(end).Dimensions  = 'NbXML, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','heading.bin');

Info.Signals(end+1).Name      = 'pitch';
Info.Signals(end).Dimensions  = 'NbXML, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','pitch.bin');

Info.Signals(end+1).Name      = 'roll';
Info.Signals(end).Dimensions  = 'NbXML, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','roll.bin');

Info.Signals(end+1).Name      = 'depth';
Info.Signals(end).Dimensions  = 'NbXML, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','depth.bin');

Info.Signals(end+1).Name      = 'altitude';
Info.Signals(end).Dimensions  = 'NbXML, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','altitude.bin');

Info.Signals(end+1).Name      = 'temperature';
Info.Signals(end).Dimensions  = 'NbXML, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','temperature.bin');

Info.Signals(end+1).Name      = 'speed';
Info.Signals(end).Dimensions  = 'NbXML, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','speed.bin');

Info.Signals(end+1).Name      = 'shipHeading';
Info.Signals(end).Dimensions  = 'NbXML, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','shipHeading.bin');

Info.Signals(end+1).Name      = 'magneticVariation';
Info.Signals(end).Dimensions  = 'NbXML, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','magneticVariation.bin');

Info.Signals(end+1).Name      = 'shipLat';
Info.Signals(end).Dimensions  = 'NbXML, 1';
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','shipLat.bin');

Info.Signals(end+1).Name      = 'shipLon';
Info.Signals(end).Dimensions  = 'NbXML, 1';
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','shipLon.bin');

Info.Signals(end+1).Name      = 'fishLat';
Info.Signals(end).Dimensions  = 'NbXML, 1';
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','fishLat.bin');

Info.Signals(end+1).Name      = 'fishLon';
Info.Signals(end).Dimensions  = 'NbXML, 1';
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','fishLon.bin');

Info.Signals(end+1).Name      = 'tvgPage';
Info.Signals(end).Dimensions  = 'NbXML, 1';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','tvgPage.bin');

Info.Signals(end+1).Name      = 'fixTimeYear';
Info.Signals(end).Dimensions  = 'NbXML, 1';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','fixTimeYear.bin');

Info.Signals(end+1).Name      = 'fixTimeMonth';
Info.Signals(end).Dimensions  = 'NbXML, 1';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','fixTimeMonth.bin');

Info.Signals(end+1).Name      = 'fixTimeDay';
Info.Signals(end).Dimensions  = 'NbXML, 1';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','fixTimeDay.bin');

Info.Signals(end+1).Name      = 'auxPitch';
Info.Signals(end).Dimensions  = 'NbXML, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','auxPitch.bin');

Info.Signals(end+1).Name      = 'auxRoll';
Info.Signals(end).Dimensions  = 'NbXML, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','auxRoll.bin');

Info.Signals(end+1).Name      = 'auxDepth';
Info.Signals(end).Dimensions  = 'NbXML, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','auxDepth.bin');

Info.Signals(end+1).Name      = 'auxAlt';
Info.Signals(end).Dimensions  = 'NbXML, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','auxAlt.bin');

Info.Signals(end+1).Name      = 'cableOut';
Info.Signals(end).Dimensions  = 'NbXML, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','cableOut.bin');

Info.Signals(end+1).Name      = 'fseconds';
Info.Signals(end).Dimensions  = 'NbXML, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','fseconds.bin');

Info.Signals(end+1).Name      = 'altimeter';
Info.Signals(end).Dimensions  = 'NbXML, 1';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','altimeter.bin');

Info.Signals(end+1).Name      = 'sampleFreq';
Info.Signals(end).Dimensions  = 'NbXML, 1';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','sampleFreq.bin');

% Info.Signals(end+1).Name      = 'depressorType';
% Info.Signals(end).Dimensions  = 'NbXML, 1';
% Info.Signals(end).Storage     = 'uint32';
% Info.Signals(end).Unit        = '';
% Info.Signals(end).FileName    = fullfile('Ssc_V5000','depressorType.bin');
% 
% Info.Signals(end+1).Name      = 'cableType';
% Info.Signals(end).Dimensions  = 'NbXML, 1';
% Info.Signals(end).Storage     = 'uint32';
% Info.Signals(end).Unit        = '';
% Info.Signals(end).FileName    = fullfile('Ssc_V5000','cableType.bin');

Info.Signals(end+1).Name      = 'shieveXoff';
Info.Signals(end).Dimensions  = 'NbXML, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','shieveXoff.bin');

Info.Signals(end+1).Name      = 'shieveYoff';
Info.Signals(end).Dimensions  = 'NbXML, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','shieveYoff.bin');

Info.Signals(end+1).Name      = 'shieveZoff';
Info.Signals(end).Dimensions  = 'NbXML, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','shieveZoff.bin');

Info.Signals(end+1).Name      = 'GPSheight';
Info.Signals(end).Dimensions  = 'NbXML, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','GPSheight.bin');

Info.Signals(end+1).Name      = 'rawDataConfig';
Info.Signals(end).Dimensions  = 'NbXML, 1';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','rawDataConfig.bin');

%info des tables
Info.Signals(end+1).Name      = 'chan1Data';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','V5000.bin');

Info.Signals(end+1).Name      = 'chan2Data';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','V5000.bin');

Info.Signals(end+1).Name      = 'chan3Data';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','V5000.bin');

Info.Signals(end+1).Name      = 'chan4Data';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','V5000.bin');

Info.Signals(end+1).Name      = 'chan5Data';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','V5000.bin');

Info.Signals(end+1).Name      = 'chan6Data';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','V5000.bin');

Info.Signals(end+1).Name      = 'chan7Data';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','V5000.bin');

Info.Signals(end+1).Name      = 'chan8Data';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','V5000.bin');

Info.Signals(end+1).Name      = 'chan9Data';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','V5000.bin');

Info.Signals(end+1).Name      = 'chan10Data';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','V5000.bin');

Info.Signals(end+1).Name      = 'bathyPort1i';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'int16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','V5000.bin');

Info.Signals(end+1).Name      = 'bathyPort1q';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'int16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','V5000.bin');

Info.Signals(end+1).Name      = 'bathyPort2i';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'int16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','V5000.bin');

Info.Signals(end+1).Name      = 'bathyPort2q';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'int16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','V5000.bin');

Info.Signals(end+1).Name      = 'bathyPort3i';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'int16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','V5000.bin');

Info.Signals(end+1).Name      = 'bathyPort3q';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'int16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','V5000.bin');

Info.Signals(end+1).Name      = 'bathyStb1i';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'int16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','V5000.bin');

Info.Signals(end+1).Name      = 'bathyStb1q';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'int16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','V5000.bin');

Info.Signals(end+1).Name      = 'bathyStb2i';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'int16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','V5000.bin');

Info.Signals(end+1).Name      = 'bathyStb2q';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'int16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','V5000.bin');

Info.Signals(end+1).Name      = 'bathyStb3i';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'int16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','V5000.bin');

Info.Signals(end+1).Name      = 'bathyStb3q';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'int16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','V5000.bin');

Info.Signals(end+1).Name      = 'echo1';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'int16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','V5000.bin');

Info.Signals(end+1).Name      = 'echo2';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'int16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','V5000.bin');

Info.Signals(end+1).Name      = 'subBottom1';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'int16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','V5000.bin');

Info.Signals(end+1).Name      = 'subBottom2';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'int16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','V5000.bin');

Info.Signals(end+1).Name      = 'rollSensor';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'int16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','V5000.bin');

Info.Signals(end+1).Name      = 'yawRate';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'int16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','V5000.bin');

Info.Signals(end+1).Name      = 'rawDataPort1i';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'int16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','V5000.bin');

Info.Signals(end+1).Name      = 'rawDataPort1q';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'int16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','V5000.bin');

Info.Signals(end+1).Name      = 'rawDataPort2i';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'int16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','V5000.bin');

Info.Signals(end+1).Name      = 'rawDataPort2q';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'int16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','V5000.bin');

Info.Signals(end+1).Name      = 'rawDataPort3i';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'int16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','V5000.bin');

Info.Signals(end+1).Name      = 'rawDataPort3q';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'int16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','V5000.bin');

Info.Signals(end+1).Name      = 'rawDataPort4i';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'int16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','V5000.bin');

Info.Signals(end+1).Name      = 'rawDataPort4q';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'int16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','V5000.bin');

Info.Signals(end+1).Name      = 'rawDataPort5i';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'int16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','V5000.bin');

Info.Signals(end+1).Name      = 'rawDataPort5q';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'int16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','V5000.bin');

Info.Signals(end+1).Name      = 'rawDataPort6i';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'int16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','V5000.bin');

Info.Signals(end+1).Name      = 'rawDataPort6q';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'int16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','V5000.bin');

Info.Signals(end+1).Name      = 'rawDataPort7i';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'int16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','V5000.bin');

Info.Signals(end+1).Name      = 'rawDataPort7q';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'int16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','V5000.bin');

Info.Signals(end+1).Name      = 'rawDataPort8i';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'int16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','V5000.bin');

Info.Signals(end+1).Name      = 'rawDataPort8q';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'int16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','V5000.bin');

Info.Signals(end+1).Name      = 'rawDataPort9i';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'int16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','V5000.bin');

Info.Signals(end+1).Name      = 'rawDataPort9q';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'int16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','V5000.bin');

Info.Signals(end+1).Name      = 'rawDataPort10i';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'int16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','V5000.bin');

Info.Signals(end+1).Name      = 'rawDataPort10q';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'int16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','V5000.bin');

Info.Signals(end+1).Name      = 'rawDataPort11i';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'int16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','V5000.bin');

Info.Signals(end+1).Name      = 'rawDataPort11q';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'int16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','V5000.bin');

Info.Signals(end+1).Name      = 'rawDataPort12i';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'int16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','V5000.bin');

Info.Signals(end+1).Name      = 'rawDataPort12q';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'int16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','V5000.bin');

Info.Signals(end+1).Name      = 'rawDataPort13i';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'int16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','V5000.bin');

Info.Signals(end+1).Name      = 'rawDataPort13q';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'int16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','V5000.bin');

Info.Signals(end+1).Name      = 'rawDataPort14i';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'int16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','V5000.bin');

Info.Signals(end+1).Name      = 'rawDataPort14q';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'int16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','V5000.bin');

Info.Signals(end+1).Name      = 'rawDataStbd1i';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'int16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','V5000.bin');

Info.Signals(end+1).Name      = 'rawDataStbd1q';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'int16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','V5000.bin');

Info.Signals(end+1).Name      = 'rawDataStbd2i';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'int16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','V5000.bin');

Info.Signals(end+1).Name      = 'rawDataStbd2q';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'int16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','V5000.bin');

Info.Signals(end+1).Name      = 'rawDataStbd3i';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'int16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','V5000.bin');

Info.Signals(end+1).Name      = 'rawDataStbd3q';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'int16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','V5000.bin');

Info.Signals(end+1).Name      = 'rawDataStbd4i';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'int16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','V5000.bin');

Info.Signals(end+1).Name      = 'rawDataStbd4q';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'int16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','V5000.bin');

Info.Signals(end+1).Name      = 'rawDataStbd5i';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'int16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','V5000.bin');

Info.Signals(end+1).Name      = 'rawDataStbd5q';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'int16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','V5000.bin');

Info.Signals(end+1).Name      = 'rawDataStbd6i';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'int16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','V5000.bin');

Info.Signals(end+1).Name      = 'rawDataStbd6q';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'int16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','V5000.bin');

Info.Signals(end+1).Name      = 'rawDataStbd7i';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'int16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','V5000.bin');

Info.Signals(end+1).Name      = 'rawDataStbd7q';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'int16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','V5000.bin');

Info.Signals(end+1).Name      = 'rawDataStbd8i';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'int16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','V5000.bin');

Info.Signals(end+1).Name      = 'rawDataStbd8q';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'int16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','V5000.bin');

Info.Signals(end+1).Name      = 'rawDataStbd9i';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'int16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','V5000.bin');

Info.Signals(end+1).Name      = 'rawDataStbd9q';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'int16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','V5000.bin');

Info.Signals(end+1).Name      = 'rawDataStbd10i';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'int16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','V5000.bin');

Info.Signals(end+1).Name      = 'rawDataStbd10q';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'int16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','V5000.bin');

Info.Signals(end+1).Name      = 'rawDataStbd11i';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'int16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','V5000.bin');

Info.Signals(end+1).Name      = 'rawDataStbd11q';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'int16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','V5000.bin');

Info.Signals(end+1).Name      = 'rawDataStbd12i';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'int16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','V5000.bin');

Info.Signals(end+1).Name      = 'rawDataStbd12q';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'int16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','V5000.bin');

Info.Signals(end+1).Name      = 'rawDataStbd13i';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'int16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','V5000.bin');

Info.Signals(end+1).Name      = 'rawDataStbd13q';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'int16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','V5000.bin');

Info.Signals(end+1).Name      = 'rawDataStbd14i';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'int16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','V5000.bin');

Info.Signals(end+1).Name      = 'rawDataStbd14q';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'int16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V5000','V5000.bin');

%% Cr�ation du fichier XML d�crivant la donn�e 

nomFicXml = fullfile(nomDirRacine, 'Ssc_V5000.xml');
xml_write(nomFicXml, Info);
flag = exist(nomFicXml, 'file');
if ~flag
    messageErreur(nomFicXml)
    return
end

%% Cr�ation du r�pertoire V5000

nomDirV5000 = fullfile(nomDirRacine, 'Ssc_V5000');
if ~exist(nomDirV5000, 'dir')
    status = mkdir(nomDirV5000);
    if ~status
        messageErreur(nomDirV5000)
        flag = 0;
        return
    end
end

%% Cr�ation des fichiers binaires des signaux

for i=1:length(Info.Signals)
    flag = writeSignal(nomDirRacine, Info.Signals(i), Data.(Info.Signals(i).Name));
    if ~flag
        return
    end
end

flag = 1;
