function flag = sdf2ssc_V7000(nomDirRacine, varargin)

%% Lecture du fichier XML d�crivant la donn�e 

nomFicXml = fullfile(nomDirRacine, 'SDF_V7000.xml');
flag = exist(nomFicXml, 'file');
if ~flag
    special_message('FichierNonExistant', nomFicXml)
    return
end
% edit(nomFicXml)
XML = xml_read(nomFicXml);

[nomDirSignal, nomFicSignal] = fileparts(nomFicXml);
nomDirSignal = fullfile(nomDirSignal, nomFicSignal);

%% Lecture des variables

% Lecture du nombre de bytes
[flag, Data.numberBytes] = Read_BinVariable(XML, nomDirSignal, 'numberBytes', 'uint32');
if ~flag
    return
end

% Lecture du pingNumber
[flag, Data.pingNumber] = Read_BinVariable(XML, nomDirSignal, 'pingNumber', 'uint32');
if ~flag
    return
end

% Lecture du nombre de samples
[flag, numSamples] = Read_BinVariable(XML, nomDirSignal, 'numSamples', 'uint32');
if ~flag
    return
end
nbSamples = sum(numSamples);

% Lecture du beamsToDisplay
[flag, Data.beamsToDisplay] = Read_BinVariable(XML, nomDirSignal, 'beamsToDisplay', 'uint32');
if ~flag
    return
end

% Lecture du errorFlags
[flag, Data.errorFlags] = Read_BinVariable(XML, nomDirSignal, 'errorFlags', 'uint32');
if ~flag
    return
end

% Lecture du range
[flag, Data.range] = Read_BinVariable(XML, nomDirSignal, 'range', 'uint32');
if ~flag
    return
end

% Lecture du speedFish
[flag, Data.speedFish] = Read_BinVariable(XML, nomDirSignal, 'speedFish', 'uint32');
if ~flag
    return
end

% Lecture du speedSound
[flag, Data.speedSound] = Read_BinVariable(XML, nomDirSignal, 'speedSound', 'uint32');
if ~flag
    return
end

% Lecture du resMode
[flag, Data.resMode] = Read_BinVariable(XML, nomDirSignal, 'resMode', 'uint32');
if ~flag
    return
end

% Lecture du txWaveform
[flag, Data.txWaveform] = Read_BinVariable(XML, nomDirSignal, 'txWaveform', 'uint32');
if ~flag
    return
end

% Lecture du respDiv
[flag, Data.respDiv] = Read_BinVariable(XML, nomDirSignal, 'respDiv', 'uint32');
if ~flag
    return
end

% Lecture du respFreq
[flag, Data.respFreq] = Read_BinVariable(XML, nomDirSignal, 'respFreq', 'uint32');
if ~flag
    return
end

% Lecture du manualSpeedSwitch
[flag, Data.manualSpeedSwitch] = Read_BinVariable(XML, nomDirSignal, 'manualSpeedSwitch', 'uint32');
if ~flag
    return
end

% Lecture du despeckleSwitch
[flag, Data.despeckleSwitch] = Read_BinVariable(XML, nomDirSignal, 'despeckleSwitch', 'uint32');
if ~flag
    return
end

% Lecture du speedFilterSwitch
[flag, Data.speedFilterSwitch] = Read_BinVariable(XML, nomDirSignal, 'speedFilterSwitch', 'uint32');
if ~flag
    return
end

% Lecture du year
[flag, Data.year] = Read_BinVariable(XML, nomDirSignal, 'year', 'uint32');
if ~flag
    return
end

% Lecture du month
[flag, Data.month] = Read_BinVariable(XML, nomDirSignal, 'month', 'uint32');
if ~flag
    return
end

% Lecture du day
[flag, Data.day] = Read_BinVariable(XML, nomDirSignal, 'day', 'uint32');
if ~flag
    return
end

% Lecture du hour
[flag, Data.hour] = Read_BinVariable(XML, nomDirSignal, 'hour', 'uint32');
if ~flag
    return
end

% Lecture du minute
[flag, Data.minute] = Read_BinVariable(XML, nomDirSignal, 'minute', 'uint32');
if ~flag
    return
end

% Lecture du second
[flag, Data.second] = Read_BinVariable(XML, nomDirSignal, 'second', 'uint32');
if ~flag
    return
end

% Lecture du hSecond
[flag, Data.hSecond] = Read_BinVariable(XML, nomDirSignal, 'hSecond', 'uint32');
if ~flag
    return
end


% Lecture du fixTimeHour
[flag, Data.fixTimeHour] = Read_BinVariable(XML, nomDirSignal, 'fixTimeHour', 'uint32');
if ~flag
    return
end

% Lecture du fixTimeMinute
[flag, Data.fixTimeMinute] = Read_BinVariable(XML, nomDirSignal, 'fixTimeMinute', 'uint32');
if ~flag
    return
end

% Lecture du fixTimeSecond
[flag, Data.fixTimeSecond] = Read_BinVariable(XML, nomDirSignal, 'fixTimeSecond', 'single');
if ~flag
    return
end

% Lecture du heading
[flag, Data.heading] = Read_BinVariable(XML, nomDirSignal, 'heading', 'single');
if ~flag
    return
end

% Lecture du pitch
[flag, Data.pitch] = Read_BinVariable(XML, nomDirSignal, 'pitch', 'single');
if ~flag
    return
end

% Lecture du roll
[flag, Data.roll] = Read_BinVariable(XML, nomDirSignal, 'roll', 'single');
if ~flag
    return
end

% Lecture des Voltage Min/Max et Max Pressure.
[flag, pressureMax] = Read_BinVariable(XML, nomDirSignal, 'pressureSensorMax', 'single');
if ~flag
    return
end

[flag, pVoltageMax] = Read_BinVariable(XML, nomDirSignal, 'pressureSensorVoltageMax', 'single');
if ~flag
    return
end

[flag, pVoltageMin] = Read_BinVariable(XML, nomDirSignal, 'pressureSensorVoltageMin', 'single');
if ~flag
    return
end
% Lecture du depth
[flag, depthVolts] = Read_BinVariable(XML, nomDirSignal, 'depth', 'single');
if ~flag
    return
end
% Cf SDF/SDFX Specification - 15300018 - Rev 3.06 - 28-October-2010
% Note 5, pg 15
Data.depth = -((((depthVolts - pVoltageMin) / (pVoltageMax - pVoltageMin)) * pressureMax) - 14.696) / 1.487;


% Lecture du altitude
[flag, Data.altitude] = Read_BinVariable(XML, nomDirSignal, 'altitude', 'single');
if ~flag
    return
end

% Lecture du temperature
[flag, Data.temperature] = Read_BinVariable(XML, nomDirSignal, 'temperature', 'single');
if ~flag
    return
end

% Lecture du speed
[flag, Data.speed] = Read_BinVariable(XML, nomDirSignal, 'speed', 'single');
if ~flag
    return
end

% Lecture du shipHeading
[flag, Data.shipHeading] = Read_BinVariable(XML, nomDirSignal, 'shipHeading', 'single');
if ~flag
    return
end

% Lecture du magneticVariation
[flag, Data.magneticVariation] = Read_BinVariable(XML, nomDirSignal, 'magneticVariation', 'single');
if ~flag
    return
end

% Lecture du shipLat
[flag, Data.shipLat] = Read_BinVariable(XML, nomDirSignal, 'shipLat', 'double');
if ~flag
    return
end

% Lecture du shipLon
[flag, Data.shipLon] = Read_BinVariable(XML, nomDirSignal, 'shipLon', 'double');
if ~flag
    return
end

% Lecture du shipLat
[flag, Data.fishLat] = Read_BinVariable(XML, nomDirSignal, 'fishLat', 'double');
if ~flag
    return
end

% Lecture du fishLon
[flag, Data.fishLon] = Read_BinVariable(XML, nomDirSignal, 'fishLon', 'double');
if ~flag
    return
end

% Lecture du tvgPage
[flag, Data.tvgPage] = Read_BinVariable(XML, nomDirSignal, 'tvgPage', 'uint32');
if ~flag
    return
end

% Lecture du headerSize
[flag, Data.headerSize] = Read_BinVariable(XML, nomDirSignal, 'headerSize', 'uint32');
if ~flag
    return
end

% Lecture du fixTimeYear
[flag, Data.fixTimeYear] = Read_BinVariable(XML, nomDirSignal, 'fixTimeYear', 'uint32');
if ~flag
    return
end

% Lecture du fixTimeMonth
[flag, Data.fixTimeMonth] = Read_BinVariable(XML, nomDirSignal, 'fixTimeMonth', 'uint32');
if ~flag
    return
end

% Lecture du fixTimeDay
[flag, Data.fixTimeDay] = Read_BinVariable(XML, nomDirSignal, 'fixTimeDay', 'uint32');
if ~flag
    return
end

% Lecture du auxPitch
[flag, Data.auxPitch] = Read_BinVariable(XML, nomDirSignal, 'auxPitch', 'single');
if ~flag
    return
end

% Lecture du auxRoll
[flag, Data.auxRoll] = Read_BinVariable(XML, nomDirSignal, 'auxRoll', 'single');
if ~flag
    return
end

% Lecture du auxDepth
[flag, Data.auxDepth] = Read_BinVariable(XML, nomDirSignal, 'auxDepth', 'single');
if ~flag
    return
end

% Lecture du auxAlt
[flag, Data.auxAlt] = Read_BinVariable(XML, nomDirSignal, 'auxAlt', 'single');
if ~flag
    return
end

% Lecture du cableOut
[flag, Data.cableOut] = Read_BinVariable(XML, nomDirSignal, 'cableOut', 'single');
if ~flag
    return
end

% Lecture du fseconds
[flag, Data.fseconds] = Read_BinVariable(XML, nomDirSignal, 'fseconds', 'single');
if ~flag
    return
end

% Lecture du altimeter
[flag, Data.altimeter] = Read_BinVariable(XML, nomDirSignal, 'altimeter', 'uint32');
if ~flag
    return
end

% Lecture du sampleFreq
[flag, Data.sampleFreq] = Read_BinVariable(XML, nomDirSignal, 'sampleFreq', 'uint32');
if ~flag
    return
end

% Lecture du depressorType
[flag, Data.depressorType] = Read_BinVariable(XML, nomDirSignal, 'depressorType', 'uint32');
if ~flag
    return
end

% Lecture du cableType
[flag, Data.cableType] = Read_BinVariable(XML, nomDirSignal, 'cableType', 'uint32');
if ~flag
    return
end

% Lecture du shieveXoff
[flag, Data.shieveXoff] = Read_BinVariable(XML, nomDirSignal, 'shieveXoff', 'single');
if ~flag
    return
end

% Lecture du shieveYoff
[flag, Data.shieveYoff] = Read_BinVariable(XML, nomDirSignal, 'shieveYoff', 'single');
if ~flag
    return
end

% Lecture du shieveZoff
[flag, Data.shieveZoff] = Read_BinVariable(XML, nomDirSignal, 'shieveYoff', 'single');
if ~flag
    return
end

% Lecture du GPSheight
[flag, Data.GPSheight] = Read_BinVariable(XML, nomDirSignal, 'GPSheight', 'single');
if ~flag
    return
end

% Lecture du rawDataConfig
[flag, Data.rawDataConfig] = Read_BinVariable(XML, nomDirSignal, 'rawDataConfig', 'uint32');
if ~flag
    return
end

% -----------------
%% Lecture des tables
% -----------------

% Lecture du chan1Data
[flag, Data.chan1Data] = Read_BinTable(XML, nomDirSignal, 'chan1Data', 'uint8', nbSamples);
if ~flag
    %return
end

% Lecture du chan2Data
[flag, Data.chan2Data] = Read_BinTable(XML, nomDirSignal, 'chan2Data', 'uint8', nbSamples);
if ~flag
    %return
end

% Lecture du chan3Data
[flag, Data.chan3Data] = Read_BinTable(XML, nomDirSignal, 'chan3Data', 'uint8', nbSamples);
if ~flag
    %return
end

% Lecture du chan4Data
[flag, Data.chan4Data] = Read_BinTable(XML, nomDirSignal, 'chan4Data', 'uint8', nbSamples);
if ~flag
    %return
end

% Lecture du chan5Data
[flag, Data.chan5Data] = Read_BinTable(XML, nomDirSignal, 'chan5Data', 'uint8', nbSamples);
if ~flag
    %return
end

% Lecture du chan6Data
[flag, Data.chan6Data] = Read_BinTable(XML, nomDirSignal, 'chan6Data', 'uint8', nbSamples);
if ~flag
    %return
end

% Lecture du chan7Data
[flag, Data.chan7Data] = Read_BinTable(XML, nomDirSignal, 'chan7Data', 'uint8', nbSamples);
if ~flag
    %return
end

% Lecture du chan8Data
[flag, Data.chan8Data] = Read_BinTable(XML, nomDirSignal, 'chan8Data', 'uint8', nbSamples);
if ~flag
    %return
end

% Lecture du chan9Data
[flag, Data.chan9Data] = Read_BinTable(XML, nomDirSignal, 'chan9Data', 'uint8', nbSamples);
if ~flag
    %return
end

% Lecture du chan10Data
[flag, Data.chan10Data] = Read_BinTable(XML, nomDirSignal, 'chan10Data', 'uint8', nbSamples);
if ~flag
    %return
end

% Lecture du chan11Data
[flag, Data.chan11Data] = Read_BinTable(XML, nomDirSignal, 'chan11Data', 'uint8', nbSamples);
if ~flag
    %return
end

% Lecture du chan12Data
[flag, Data.chan12Data] = Read_BinTable(XML, nomDirSignal, 'chan12Data', 'uint8', nbSamples);
if ~flag
    %return
end

% Lecture du chan13Data
[flag, Data.chan13Data] = Read_BinTable(XML, nomDirSignal, 'chan13Data', 'uint8', nbSamples);
if ~flag
    %return
end

% Lecture du chan14Data
[flag, Data.chan14Data] = Read_BinTable(XML, nomDirSignal, 'chan14Data', 'uint8', nbSamples);
if ~flag
    %return
end

% Lecture du chan15Data
[flag, Data.chan15Data] = Read_BinTable(XML, nomDirSignal, 'chan15Data', 'uint8', nbSamples);
if ~flag
    %return
end

% Lecture du chan16Data
[flag, Data.chan16Data] = Read_BinTable(XML, nomDirSignal, 'chan16Data', 'uint8', nbSamples);
if ~flag
    %return
end

% Lecture du chan17Data
[flag, Data.chan17Data] = Read_BinTable(XML, nomDirSignal, 'chan17Data', 'uint8', nbSamples);
if ~flag
    %return
end

% Lecture du chan18Data
[flag, Data.chan18Data] = Read_BinTable(XML, nomDirSignal, 'chan18Data', 'uint8', nbSamples);
if ~flag
    %return
end

% Lecture du chan19Data
[flag, Data.chan19Data] = Read_BinTable(XML, nomDirSignal, 'chan19Data', 'uint8', nbSamples);
if ~flag
    %return
end

% Lecture du chan20Data
[flag, Data.chan20Data] = Read_BinTable(XML, nomDirSignal, 'chan20Data', 'uint8', nbSamples);
if ~flag
    %return
end

% Lecture du sector1Data
[flag, Data.sector1Data] = Read_BinTable(XML, nomDirSignal, 'sector1Data', 'uint8', nbSamples);
if ~flag
    %return
end

% Lecture du sector2Data
[flag, Data.sector2Data] = Read_BinTable(XML, nomDirSignal, 'sector2Data', 'uint8', nbSamples);
if ~flag
    %return
end

% Lecture du sector3Data
[flag, Data.sector3Data] = Read_BinTable(XML, nomDirSignal, 'sector3Data', 'uint8', nbSamples);
if ~flag
    %return
end

% Lecture du sector4Data
[flag, Data.sector4Data] = Read_BinTable(XML, nomDirSignal, 'sector4Data', 'uint8', nbSamples);
if ~flag
    %return
end

% Lecture du sector5Data
[flag, Data.sector5Data] = Read_BinTable(XML, nomDirSignal, 'sector5Data', 'uint8', nbSamples);
if ~flag
    %return
end

% Lecture du sector6Data
[flag, Data.sector6Data] = Read_BinTable(XML, nomDirSignal, 'sector6Data', 'uint8', nbSamples);
if ~flag
    %return
end

% Lecture du sector7Data
[flag, Data.sector7Data] = Read_BinTable(XML, nomDirSignal, 'sector7Data', 'uint8', nbSamples);
if ~flag
    %return
end

% Lecture du sector8Data
[flag, Data.sector8Data] = Read_BinTable(XML, nomDirSignal, 'sector8Data', 'uint8', nbSamples);
if ~flag
    %return
end

% Lecture du bathy1
[flag, Data.bathy1] = Read_BinTable(XML, nomDirSignal, 'bathy1', 'int8', nbSamples);
if ~flag
    %return
end

% Lecture du bathy2
[flag, Data.bathy2] = Read_BinTable(XML, nomDirSignal, 'bathy2', 'int8', nbSamples);
if ~flag
    %return
end

% Lecture du bathy3
[flag, Data.bathy3] = Read_BinTable(XML, nomDirSignal, 'bathy3', 'int8', nbSamples);
if ~flag
    %return
end

% Lecture du bathy4
[flag, Data.bathy4] = Read_BinTable(XML, nomDirSignal, 'bathy4', 'int8', nbSamples);
if ~flag
    %return
end

% Lecture du bathy5
[flag, Data.bathy5] = Read_BinTable(XML, nomDirSignal, 'bathy5', 'int8', nbSamples);
if ~flag
    %return
end

% Lecture du bathy6
[flag, Data.bathy6] = Read_BinTable(XML, nomDirSignal, 'bathy6', 'int8', nbSamples);
if ~flag
    %return
end

% Lecture du bathy7
[flag, Data.bathy7] = Read_BinTable(XML, nomDirSignal, 'bathy7', 'int8', nbSamples);
if ~flag
    %return
end

% Lecture du bathy8
[flag, Data.bathy8] = Read_BinTable(XML, nomDirSignal, 'bathy8', 'int8', nbSamples);
if ~flag
    %return
end

% Lecture du bathy9
[flag, Data.bathy9] = Read_BinTable(XML, nomDirSignal, 'bathy9', 'int8', nbSamples);
if ~flag
    %return
end

% Lecture du bathy10
[flag, Data.bathy10] = Read_BinTable(XML, nomDirSignal, 'bathy10', 'int8', nbSamples);
if ~flag
    %return
end

% Lecture du bathy11
[flag, Data.bathy11] = Read_BinTable(XML, nomDirSignal, 'bathy11', 'int8', nbSamples);
if ~flag
    %return
end

% Lecture du bathy12
[flag, Data.bathy12] = Read_BinTable(XML, nomDirSignal, 'bathy12', 'int8', nbSamples);
if ~flag
    %return
end

% Lecture du bathy13
[flag, Data.bathy13] = Read_BinTable(XML, nomDirSignal, 'bathy13', 'int8', nbSamples);
if ~flag
    %return
end

% Lecture du bathy14
[flag, Data.bathy14] = Read_BinTable(XML, nomDirSignal, 'bathy14', 'int8', nbSamples);
if ~flag
    %return
end

% Lecture du bathy15
[flag, Data.bathy15] = Read_BinTable(XML, nomDirSignal, 'bathy15', 'int8', nbSamples);
if ~flag
    %return
end

% Lecture du bathy16
[flag, Data.bathy16] = Read_BinTable(XML, nomDirSignal, 'bathy16', 'int8', nbSamples);
if ~flag
    %return
end

% Lecture du bathy17
[flag, Data.bathy17] = Read_BinTable(XML, nomDirSignal, 'bathy17', 'int8', nbSamples);
if ~flag
    %return
end

% Lecture du bathy18
[flag, Data.bathy18] = Read_BinTable(XML, nomDirSignal, 'bathy18', 'int8', nbSamples);
if ~flag
    %return
end

% Lecture du bathy19
[flag, Data.bathy19] = Read_BinTable(XML, nomDirSignal, 'bathy19', 'int8', nbSamples);
if ~flag
    %return
end

% Lecture du bathy20
[flag, Data.bathy20] = Read_BinTable(XML, nomDirSignal, 'bathy20', 'int8', nbSamples);
if ~flag
    %return
end

% Lecture du bathy21
[flag, Data.bathy21] = Read_BinTable(XML, nomDirSignal, 'bathy21', 'int8', nbSamples);
if ~flag
    %return
end

% Lecture du bathy22
[flag, Data.bathy22] = Read_BinTable(XML, nomDirSignal, 'bathy22', 'int8', nbSamples);
if ~flag
    %return
end

% Lecture du bathy23
[flag, Data.bathy23] = Read_BinTable(XML, nomDirSignal, 'bathy23', 'int8', nbSamples);
if ~flag
    %return
end

% Lecture du bathy24
[flag, Data.bathy24] = Read_BinTable(XML, nomDirSignal, 'bathy24', 'int8', nbSamples);
if ~flag
    %return
end

% Lecture du echo1
[flag, Data.echo1] = Read_BinTable(XML, nomDirSignal, 'echo1', 'uint8', nbSamples);
if ~flag
    %return
end

% Lecture du echo2
[flag, Data.echo2] = Read_BinTable(XML, nomDirSignal, 'echo2', 'uint8', nbSamples);
if ~flag
    %return
end

% Lecture du echo3
[flag, Data.echo3] = Read_BinTable(XML, nomDirSignal, 'echo3', 'uint8', nbSamples);
if ~flag
    %return
end

% Lecture du echo4
[flag, Data.echo4] = Read_BinTable(XML, nomDirSignal, 'echo4', 'uint8', nbSamples);
if ~flag
    %return
end

% Lecture du echo5
[flag, Data.echo5] = Read_BinTable(XML, nomDirSignal, 'echo5', 'uint8', nbSamples);
if ~flag
    %return
end

% Lecture du echo6
[flag, Data.echo6] = Read_BinTable(XML, nomDirSignal, 'echo6', 'uint8', nbSamples);
if ~flag
    %return
end

% Lecture du echo7
[flag, Data.echo7] = Read_BinTable(XML, nomDirSignal, 'echo7', 'uint8', nbSamples);
if ~flag
    %return
end

% Lecture du echo8
[flag, Data.echo8] = Read_BinTable(XML, nomDirSignal, 'echo8', 'uint8', nbSamples);
if ~flag
    %return
end

% Lecture du sensor1
[flag, Data.sensor1] = Read_BinTable(XML, nomDirSignal, 'sensor1', 'int8', nbSamples);
if ~flag
    %return
end

% Lecture du sensor2
[flag, Data.sensor2] = Read_BinTable(XML, nomDirSignal, 'sensor2', 'int8', nbSamples);
if ~flag
    %return
end

% Lecture du sensor3
[flag, Data.sensor3] = Read_BinTable(XML, nomDirSignal, 'sensor3', 'int8', nbSamples);
if ~flag
    %return
end

% Lecture du sensor4
[flag, Data.sensor4] = Read_BinTable(XML, nomDirSignal, 'sensor4', 'int8', nbSamples);
if ~flag
    %return
end

% ----------------------------
%% G�n�ration du XML SonarScope

Info.Title                  = 'V7000'; % Motion Reference Unit
Info.Constructor            = 'Klein';
Info.EmModel                = XML.Model;
Info.SystemSerialNumber     = XML.SystemSerialNumber;
Info.TimeOrigin             = '01/01/-4713';
Info.Comments               = 'Sensor sampling rate';

Info.Dimensions.NbSamples     = nbSamples;

%info des variables
Info.Signals(1).Name          = 'range';
Info.Signals(end).Dimensions  = 'NbXML, 1';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V7001','range.bin');

Info.Signals(end+1).Name      = 'speedFish';
Info.Signals(end).Dimensions  = 'NbXML, 1';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = 'cm/s';
Info.Signals(end).FileName    = fullfile('Ssc_V7001','speedFish.bin');

Info.Signals(end+1).Name      = 'speedSound';
Info.Signals(end).Dimensions  = 'NbXML, 1';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V7001','speedSound.bin');

Info.Signals(end+1).Name      = 'resMode';
Info.Signals(end).Dimensions  = 'NbXML, 1';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V7001','resMode.bin');

Info.Signals(end+1).Name      = 'txWaveform';
Info.Signals(end).Dimensions  = 'NbXML, 1';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V7001','txWaveform.bin');

Info.Signals(end+1).Name      = 'respDiv';
Info.Signals(end).Dimensions  = 'NbXML, 1';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V7001','respDiv.bin');

Info.Signals(end+1).Name      = 'respFreq';
Info.Signals(end).Dimensions  = 'NbXML, 1';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V7001','respFreq.bin');

Info.Signals(end+1).Name      = 'manualSpeedSwitch';
Info.Signals(end).Dimensions  = 'NbXML, 1';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V7001','manualSpeedSwitch.bin');

Info.Signals(end+1).Name      = 'despeckleSwitch';
Info.Signals(end).Dimensions  = 'NbXML, 1';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V7001','despeckleSwitch.bin');

Info.Signals(end+1).Name      = 'speedFilterSwitch';
Info.Signals(end).Dimensions  = 'NbXML, 1';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V7001','speedFilterSwitch.bin');

Info.Signals(end+1).Name      = 'year';
Info.Signals(end).Dimensions  = 'NbXML, 1';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V7001','year.bin');

Info.Signals(end+1).Name      = 'month';
Info.Signals(end).Dimensions  = 'NbXML, 1';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V7001','month.bin');

Info.Signals(end+1).Name      = 'day';
Info.Signals(end).Dimensions  = 'NbXML, 1';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V7001','day.bin');

Info.Signals(end+1).Name      = 'hour';
Info.Signals(end).Dimensions  = 'NbXML, 1';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V7001','hour.bin');

Info.Signals(end+1).Name      = 'minute';
Info.Signals(end).Dimensions  = 'NbXML, 1';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V7001','minute.bin');

Info.Signals(end+1).Name      = 'second';
Info.Signals(end).Dimensions  = 'NbXML, 1';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V7001','second.bin');

Info.Signals(end+1).Name      = 'hSecond';
Info.Signals(end).Dimensions  = 'NbXML, 1';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V7001','hSecond.bin');

Info.Signals(end+1).Name      = 'fixTimeHour';
Info.Signals(end).Dimensions  = 'NbXML, 1';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V7001','fixTimeHour.bin');

Info.Signals(end+1).Name      = 'fixTimeMinute';
Info.Signals(end).Dimensions  = 'NbXML, 1';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V7001','fixTimeMinute.bin');

Info.Signals(end+1).Name      = 'fixTimeSecond';
Info.Signals(end).Dimensions  = 'NbXML, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V7001','fixTimeSecond.bin');

Info.Signals(end+1).Name      = 'heading';
Info.Signals(end).Dimensions  = 'NbXML, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V7001','heading.bin');

Info.Signals(end+1).Name      = 'pitch';
Info.Signals(end).Dimensions  = 'NbXML, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V7001','pitch.bin');

Info.Signals(end+1).Name      = 'roll';
Info.Signals(end).Dimensions  = 'NbXML, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V7001','roll.bin');

Info.Signals(end+1).Name      = 'depth';
Info.Signals(end).Dimensions  = 'NbXML, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V7001','depth.bin');

Info.Signals(end+1).Name      = 'altitude';
Info.Signals(end).Dimensions  = 'NbXML, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V7001','altitude.bin');

Info.Signals(end+1).Name      = 'temperature';
Info.Signals(end).Dimensions  = 'NbXML, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V7001','temperature.bin');

Info.Signals(end+1).Name      = 'speed';
Info.Signals(end).Dimensions  = 'NbXML, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V7001','speed.bin');

Info.Signals(end+1).Name      = 'shipHeading';
Info.Signals(end).Dimensions  = 'NbXML, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V7001','shipHeading.bin');

Info.Signals(end+1).Name      = 'magneticVariation';
Info.Signals(end).Dimensions  = 'NbXML, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V7001','magneticVariation.bin');

Info.Signals(end+1).Name      = 'shipLat';
Info.Signals(end).Dimensions  = 'NbXML, 1';
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V7001','shipLat.bin');

Info.Signals(end+1).Name      = 'shipLon';
Info.Signals(end).Dimensions  = 'NbXML, 1';
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V7001','shipLon.bin');

Info.Signals(end+1).Name      = 'fishLat';
Info.Signals(end).Dimensions  = 'NbXML, 1';
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V7001','fishLat.bin');

Info.Signals(end+1).Name      = 'fishLon';
Info.Signals(end).Dimensions  = 'NbXML, 1';
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V7001','fishLon.bin');

Info.Signals(end+1).Name      = 'tvgPage';
Info.Signals(end).Dimensions  = 'NbXML, 1';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V7001','tvgPage.bin');

Info.Signals(end+1).Name      = 'fixTimeYear';
Info.Signals(end).Dimensions  = 'NbXML, 1';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V7001','fixTimeYear.bin');

Info.Signals(end+1).Name      = 'fixTimeMonth';
Info.Signals(end).Dimensions  = 'NbXML, 1';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V7001','fixTimeMonth.bin');

Info.Signals(end+1).Name      = 'fixTimeDay';
Info.Signals(end).Dimensions  = 'NbXML, 1';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V7001','fixTimeDay.bin');

Info.Signals(end+1).Name      = 'auxPitch';
Info.Signals(end).Dimensions  = 'NbXML, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V7001','auxPitch.bin');

Info.Signals(end+1).Name      = 'auxRoll';
Info.Signals(end).Dimensions  = 'NbXML, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V7001','auxRoll.bin');

Info.Signals(end+1).Name      = 'auxDepth';
Info.Signals(end).Dimensions  = 'NbXML, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V7001','auxDepth.bin');

Info.Signals(end+1).Name      = 'auxAlt';
Info.Signals(end).Dimensions  = 'NbXML, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V7001','auxAlt.bin');

Info.Signals(end+1).Name      = 'cableOut';
Info.Signals(end).Dimensions  = 'NbXML, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V7001','cableOut.bin');

Info.Signals(end+1).Name      = 'fseconds';
Info.Signals(end).Dimensions  = 'NbXML, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V7001','fseconds.bin');

Info.Signals(end+1).Name      = 'altimeter';
Info.Signals(end).Dimensions  = 'NbXML, 1';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V7001','altimeter.bin');

Info.Signals(end+1).Name      = 'sampleFreq';
Info.Signals(end).Dimensions  = 'NbXML, 1';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V7001','sampleFreq.bin');

% Info.Signals(end+1).Name      = 'depressorType';
% Info.Signals(end).Dimensions  = 'NbXML, 1';
% Info.Signals(end).Storage     = 'uint32';
% Info.Signals(end).Unit        = '';
% Info.Signals(end).FileName    = fullfile('Ssc_V7001','depressorType.bin');
% 
% Info.Signals(end+1).Name      = 'cableType';
% Info.Signals(end).Dimensions  = 'NbXML, 1';
% Info.Signals(end).Storage     = 'uint32';
% Info.Signals(end).Unit        = '';
% Info.Signals(end).FileName    = fullfile('Ssc_V7001','cableType.bin');

Info.Signals(end+1).Name      = 'shieveXoff';
Info.Signals(end).Dimensions  = 'NbXML, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V7001','shieveXoff.bin');

Info.Signals(end+1).Name      = 'shieveYoff';
Info.Signals(end).Dimensions  = 'NbXML, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V7001','shieveYoff.bin');

Info.Signals(end+1).Name      = 'shieveZoff';
Info.Signals(end).Dimensions  = 'NbXML, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V7001','shieveZoff.bin');

Info.Signals(end+1).Name      = 'GPSheight';
Info.Signals(end).Dimensions  = 'NbXML, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V7001','GPSheight.bin');

Info.Signals(end+1).Name      = 'rawDataConfig';
Info.Signals(end).Dimensions  = 'NbXML, 1';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V7001','rawDataConfig.bin');

%info des tables
Info.Signals(end+1).Name      = 'chan1Data';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V7000','V7000.bin');

Info.Signals(end+1).Name      = 'chan2Data';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V7000','V7000.bin');

Info.Signals(end+1).Name      = 'chan3Data';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V7000','V7000.bin');

Info.Signals(end+1).Name      = 'chan4Data';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V7000','V7000.bin');

Info.Signals(end+1).Name      = 'chan5Data';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V7000','V7000.bin');

Info.Signals(end+1).Name      = 'chan6Data';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V7000','V7000.bin');

Info.Signals(end+1).Name      = 'chan7Data';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V7000','V7000.bin');

Info.Signals(end+1).Name      = 'chan8Data';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V7000','V7000.bin');

Info.Signals(end+1).Name      = 'chan9Data';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V7000','V7000.bin');

Info.Signals(end+1).Name      = 'chan10Data';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V7000','V7000.bin');

Info.Signals(end+1).Name      = 'chan11Data';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V7000','V7000.bin');

Info.Signals(end+1).Name      = 'chan12Data';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V7000','V7000.bin');

Info.Signals(end+1).Name      = 'chan13Data';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V7000','V7000.bin');

Info.Signals(end+1).Name      = 'chan14Data';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V7000','V7000.bin');

Info.Signals(end+1).Name      = 'chan15Data';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V7000','V7000.bin');

Info.Signals(end+1).Name      = 'chan16Data';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V7000','V7000.bin');

Info.Signals(end+1).Name      = 'chan17Data';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V7000','V7000.bin');

Info.Signals(end+1).Name      = 'chan18Data';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V7000','V7000.bin');

Info.Signals(end+1).Name      = 'chan19Data';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V7000','V7000.bin');

Info.Signals(end+1).Name      = 'chan20Data';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V7000','V7000.bin');

Info.Signals(end+1).Name      = 'sector1Data';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V7000','V7000.bin');

Info.Signals(end+1).Name      = 'sector2Data';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V7000','V7000.bin');

Info.Signals(end+1).Name      = 'sector3Data';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V7000','V7000.bin');

Info.Signals(end+1).Name      = 'sector4Data';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V7000','V7000.bin');

Info.Signals(end+1).Name      = 'sector5Data';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V7000','V7000.bin');

Info.Signals(end+1).Name      = 'sector6Data';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V7000','V7000.bin');

Info.Signals(end+1).Name      = 'sector7Data';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V7000','V7000.bin');

Info.Signals(end+1).Name      = 'sector8Data';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V7000','V7000.bin');

Info.Signals(end+1).Name      = 'bathy1';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'int16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V7000','V7000.bin');

Info.Signals(end+1).Name      = 'bathy2';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'int16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V7000','V7000.bin');

Info.Signals(end+1).Name      = 'bathy3';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'int16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V7000','V7000.bin');

Info.Signals(end+1).Name      = 'bathy4';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'int16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V7000','V7000.bin');

Info.Signals(end+1).Name      = 'bathy5';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'int16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V7000','V7000.bin');

Info.Signals(end+1).Name      = 'bathy6';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'int16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V7000','V7000.bin');

Info.Signals(end+1).Name      = 'bathy7';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'int16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V7000','V7000.bin');

Info.Signals(end+1).Name      = 'bathy8';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'int16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V7000','V7000.bin');

Info.Signals(end+1).Name      = 'bathy9';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'int16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V7000','V7000.bin');

Info.Signals(end+1).Name      = 'bathy10';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'int16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V7000','V7000.bin');

Info.Signals(end+1).Name      = 'bathy11';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'int16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V7000','V7000.bin');

Info.Signals(end+1).Name      = 'bathy12';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'int16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V7000','V7000.bin');

Info.Signals(end+1).Name      = 'bathy13';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'int16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V7000','V7000.bin');

Info.Signals(end+1).Name      = 'bathy14';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'int16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V7000','V7000.bin');

Info.Signals(end+1).Name      = 'bathy15';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'int16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V7000','V7000.bin');

Info.Signals(end+1).Name      = 'bathy16';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'int16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V7000','V7000.bin');

Info.Signals(end+1).Name      = 'bathy17';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'int16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V7000','V7000.bin');

Info.Signals(end+1).Name      = 'bathy18';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'int16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V7000','V7000.bin');

Info.Signals(end+1).Name      = 'bathy19';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'int16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V7000','V7000.bin');

Info.Signals(end+1).Name      = 'bathy20';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'int16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V7000','V7000.bin');

Info.Signals(end+1).Name      = 'bathy21';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'int16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V7000','V7000.bin');

Info.Signals(end+1).Name      = 'bathy22';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'int16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V7000','V7000.bin');

Info.Signals(end+1).Name      = 'bathy23';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'int16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V7000','V7000.bin');

Info.Signals(end+1).Name      = 'bathy24';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'int16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V7000','V7000.bin');

Info.Signals(end+1).Name      = 'echo1';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V7000','V7000.bin');

Info.Signals(end+1).Name      = 'echo2';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V7000','V7000.bin');

Info.Signals(end+1).Name      = 'echo3';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V7000','V7000.bin');

Info.Signals(end+1).Name      = 'echo4';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V7000','V7000.bin');

Info.Signals(end+1).Name      = 'echo5';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V7000','V7000.bin');

Info.Signals(end+1).Name      = 'echo6';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V7000','V7000.bin');

Info.Signals(end+1).Name      = 'echo7';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V7000','V7000.bin');

Info.Signals(end+1).Name      = 'echo8';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V7000','V7000.bin');

Info.Signals(end+1).Name      = 'sensor1';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'int16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V7000','V7000.bin');

Info.Signals(end+1).Name      = 'sensor1';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'int16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V7000','V7000.bin');

Info.Signals(end+1).Name      = 'sensor2';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'int16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V7000','V7000.bin');

Info.Signals(end+1).Name      = 'sensor3';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'int16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V7000','V7000.bin');

Info.Signals(end+1).Name      = 'sensor4';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'int16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_V7000','V7000.bin');

%% Cr�ation du fichier XML d�crivant la donn�e 

nomFicXml = fullfile(nomDirRacine, 'Ssc_V7000.xml');
xml_write(nomFicXml, Info);
flag = exist(nomFicXml, 'file');
if ~flag
    messageErreur(nomFicXml)
    return
end

%% Cr�ation du r�pertoire V7000

nomDirV7000 = fullfile(nomDirRacine, 'Ssc_V7000');
if ~exist(nomDirV7000, 'dir')
    status = mkdir(nomDirV7000);
    if ~status
        messageErreur(nomDirV7000)
        flag = 0;
        return
    end
end

%% Cr�ation des fichiers binaires des signaux

for i=1:length(Info.Signals)
    flag = writeSignal(nomDirRacine, Info.Signals(i), Data.(Info.Signals(i).Name));
    if ~flag
        return
    end
end

flag = 1;
