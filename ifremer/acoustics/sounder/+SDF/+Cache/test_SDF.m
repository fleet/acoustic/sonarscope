% SDF.Cache.test_SDF

function test_SDF

nomFicSDF = 'F:\SonarScopeData\From Charline\SoReco2_010_111012160800.sdf';
[flag, PageVersion] = SDF.Cache.sdf2ssc(nomFic, varargin); %#ok<ASGLU>
a = cl_sdf('nomFic', nomFicSDF); %#ok<NASGU>

%% Summary

nomFicSummary = 'F:\SonarScopeData\From Charline\Summary_SDF.txt';
if flag
    Summary(S0, nomFicSDF, nomFicSummary);
end

%% Split

nomFicSDF = 'F:\SonarScopeData\From Charline\SoReco2_010_111012160800.sdf';
a  = cl_sdf('nomFic', nomFicSDF);
selectDatagram  = {a.nbPings, 3001, 'V3001', 1};
SplitTime = [];
NbPings   = 2800;
nomFicOut = saucissonne_bin(a, NbPings, SplitTime, selectDatagram); %#ok<NASGU>
