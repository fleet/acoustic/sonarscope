function [flag, AlongInterpolation] = alongDistanceInterpolation

flag =  1;
AlongInterpolation = 2;

if get_LevelQuestion >= 3
    str1 = 'Interpolation longitudinale';
    str2 = 'Along distance Interpolation';
    [AlongInterpolation, flag] = my_questdlg(Lang(str1,str2), 'Init', 1, 'ColorLevel', 3);
    if ~flag
        return
    end
    AlongInterpolation = 3 - AlongInterpolation;
end
