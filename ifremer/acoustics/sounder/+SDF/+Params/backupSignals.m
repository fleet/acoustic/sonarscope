function [flag, NomSignals, backupFileName] = backupSignals(repImport, InOut) 

NomSignals     = [];
backupFileName = [];

%% Saisie de la liste des signaux � sauvegarder

listSignaNames = {'altitudeMeters'
    'heading'
    'fishNavHeading'
    'shipHeading'
    'fishLat'
    'fishLon'
    'cableOut'};
%     'depth' % TODO

[flag, sub] = question_SelectASignal(listSignaNames, 'InitialValue', []);
if ~flag || isempty(sub)
    return
end
NomSignals = listSignaNames(sub);

%% Nom du fichier de backup

backupFileName = fullfile(repImport, 'SignalsBackup.nc');
if strcmp(InOut, 'Output')
    [flag, backupFileName] = my_uiputfile('.nc', 'Save as', backupFileName);
    if ~flag
        return
    end
else
     [flag, backupFileName] = my_uigetfile('.nc', 'Pick a file', repImport, 'MultiSelect', 'off');
end
