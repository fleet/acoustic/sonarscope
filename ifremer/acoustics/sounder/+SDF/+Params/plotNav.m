function [flag, Origin] = plotNav

str = {'Ship'; 'Fish'; 'Auto (fish if the data exists, ship otherwise)'};
[Origin, flag] = my_listdlg('Source of the navigation', str, 'SelectionMode', 'Single', 'InitialValue', 3);
if ~flag
    return
end
