function [flag, NomSignal] = plotNavAndSignal(listFileNames)

NomSignal = [];

ToBeSureTheCacheIsDone = cl_sdf('nomFic', listFileNames{1}); %#ok<NASGU>

%% Lecture du répertoire SonarScope

[nomDir, nomFicSeul] = fileparts(listFileNames{1});
nomDirRacine = fullfile(nomDir, 'SonarScope', nomFicSeul);
PageVersion = SDF.Process.getPageVersion(nomDirRacine);

[flag, Data] = SSc_ReadDatagrams(listFileNames{1}, ['Ssc_' PageVersion], 'Memmapfile', -1);
if ~exist(nomDir, 'dir')
    return
end

%% Select a signal

Option = {'Time'; 'Height'; 'Bathy'; 'Inter ping distance'; 'PitchMean'; 'PitchStd'; 'RollMean'; 'RollStd'; 'HeadingMean'; 'HeadingStd'};
for k=1:length(Data.Signals)
    switch Data.Signals(k).Name
        case {'day'; 'month'; 'year'; 'hour'; 'minute'; 'second'; 'hSecond'}
%         case {'fixTimeDay'; 'fixTimeMonth'; 'fixTimeYear'; 'fixTimeHour'; 'fixTimeMinute'; 'fixTimeSecond'}
        case {'fixTimeMonth'; 'fixTimeYear'; 'fixTimeHour'; 'fixTimeMinute'; 'fixTimeSecond'}
        case {'shipLat'; 'shipLon'; 'fishLat'; 'fishLon'}
        otherwise
            Option{end+1} = Data.Signals(k).Name; %#ok<AGROW>
    end
end
[flag, sub] = question_SelectASignal(Option);
if ~flag
    return
end
NomSignal = Option{sub};
