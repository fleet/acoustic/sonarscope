function [flag, listeSignals, typeAxeX] = plotSignals(listFileNames)

typeAxeX = [];

a = cl_sdf('nomFic', listFileNames{1});
listeSignals = get_listeSignals(a);

listeSup = {'Time'; 'Height'; 'Bathy'; 'Inter ping distance'; 'PitchMean'; 'PitchStd'; 'RollMean'; 'RollStd'; 'HeadingMean'; 'HeadingStd'};
listeSignals = [listeSup ; listeSignals'];

indSignalTime = strcmp(listeSignals, 'Time');
listeSignals(indSignalTime) = [];
[flag, listeSignals] = uiSelectItems('ItemsToSelect', listeSignals);
if ~flag
    return
end

str{1} = Lang('Heure', 'Time');
str{2} = Lang('Num�ro de ping', 'Ping number');
str1 = 'Choix de l''axe des X';
str2 = 'Select X axis';
% str3 = 'IFREMER - SonarScope : Plot Klein signals';
% str4 = 'TODO';
[typeAxeX, flag] = my_listdlg(Lang(str1,str2), str, 'SelectionMode', 'Single'); %, 'Entete', Lang(str3,str4));
