function [flag, SourceHeading] = selectSourceHeading

persistent persistent_SourceHeading

if isempty(persistent_SourceHeading)
    InitialValue = 1;
else
    InitialValue = persistent_SourceHeading;
end
str = {'Fish sensor'; 'Deduced from fish navigation'; 'Ship sensor'};
[SourceHeading, flag] = my_listdlg('Source of the heading values', str, 'SelectionMode', 'Single', 'InitialValue', InitialValue);
if ~flag
    return
end
persistent_SourceHeading = SourceHeading;

if SourceHeading == 2
    str = 'Warning : As you selected the second option, in the NavigationDialog window to come, the heading values will be computed when the plot is created. It is unnessasary to clean or filter this signal because it will be recomputed from the cleaned navigation when the window will close. You could eventually redo the Edit Navigation a second time to check this heading signal.';
    my_warndlg(str,  1);
end
