% R�alisation de la mosa�que de donn�es en g�om�trie PingSamples d'une campagne
%
% Syntax
%   [flag, Mosa, repExport] = SDF.Process.PingSamples2LatLong(listFileNames, repExport, ...)
%
% Name-Value Pair Arguments
%   listFileNames      : Liste des fichiers .sdf
%   repExport          : Nom du r�pertoire des .ers
%   AlongInterpolation : 1=Yes, 2=No
%
% Output Arguments
%   flag      : 1=OK, 0=KO
%   Mosa      : Instances de cl_image contenant la mosaique et la mosaique angulaire
%   repExport : Nom du r�pertoire des .ers
%
% Examples
%   flag, a] = SDF.Process.PingSamples2LatLong('NomDir', '/home1/jackson/FEMME2005/Donnees', 'Resol', 10);
%   SonarScope(a)
%   [flag, a] = SDF.Process.PingSamples2LatLong('NomDir', 'D:\NIWA_DATA\TAN0105', 'Resol', 10, 'Backup', 5);
%   SonarScope(a)
%
% See also Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function [flag, Mosa, repExport] = PingSamples2LatLong(listFileNames, repExport, varargin)

[varargin, resol] = getPropertyValue(varargin, 'resol', []); %#ok<ASGLU>

Mosa = [];
C0 = cl_ermapper([]);

if ischar(listFileNames)
    listFileNames = {listFileNames};
end

nbProfils = length(listFileNames);
repImport = fileparts(listFileNames{1});

%% Choix de la fr�quence

ListeLayers = {'132kHz'; '445kHz'};
[indLayer, flag] = my_listdlg('Layers :', ListeLayers, 'InitialValue', 1, 'SelectionMode', 'Single');
if ~flag || isempty(indLayer)
    flag = 0;
    return
end

%% Saisie de la r�solution

[flag, resol] = get_gridSize('Value', resol);
if ~flag
    return
end

%% Origine du cap

[flag, SourceHeading] = SDF.Params.selectSourceHeading;
if ~flag
    return
end

%% Interpolation longitudinale

[flag, AlongInterpolation] = SDF.Params.alongDistanceInterpolation;
if ~flag
    return
end

%% Backup frequency

[flag, Backup] = saisie_backupFrequency(nbProfils);
if ~flag
    return
end

%% Noms du fichier de sauvegarde de la mosaique de r�flectivit�

DataType = cl_image.indDataType('Reflectivity');
[nomFicErMapperLayer, flag] = initNomFicErs(C0, ['Mosaic_' ListeLayers{indLayer}], cl_image.indGeometryType('LatLong'), DataType, repExport, 'resol', resol);
if ~flag
    return
end
[repExport, TitreDepth] = fileparts(nomFicErMapperLayer);

%% Noms du fichier de sauvegarde de la mosaique des angles d'�mission

DataType = cl_image.indDataType('TxAngle');
[nomFicErMapperAngle, flag] = initNomFicErs(C0, TitreDepth, cl_image.indGeometryType('LatLong'), DataType, repExport, 'confirm', (get_LevelQuestion >= 3));
if ~flag
    return
end

%% R�cup�ration des mosa�ques existantes

Carto = [];
if exist(nomFicErMapperLayer, 'file') && exist(nomFicErMapperAngle, 'file')
    [flag, rep] = question_CompleteExistingMosaic;
    if ~flag
        return
    end
    if rep == 1
        [flag, Z_Mosa_All] = cl_image.import_ermapper(nomFicErMapperLayer);
        if ~flag
            return
        end
        Carto = get(Z_Mosa_All, 'Carto');
        
        [flag, A_Mosa_All]  = cl_image.import_ermapper(nomFicErMapperAngle);
        if ~flag
            return
        end
    else
        Z_Mosa_All = [];
        A_Mosa_All = [];
    end
else
    Z_Mosa_All = [];
    A_Mosa_All = [];
end

%% Si Mosaique de R�flectivite, on propose de faire une compensation statistique ou une calibration

% [flag, TypeCalibration, CorFile, bilan, DataTypeConditions, alpha, ModelTxDiag, CourbeBS] = params_SonarCompensation(repImport);
[flag, TypeCalibration, CorFile, bilan] = params_SonarCompensation(repImport);
if ~flag
    return
end

%% Traitement des fichiers

str = 'SDF : PingSamples -> LatLong';
hw = create_waitbar(str, 'N', nbProfils);
for kProfil=1:nbProfils
    my_waitbar(kProfil, nbProfils, hw)
    
    fprintf('Processing file %d/%d : %s\n', kProfil, nbProfils, listFileNames{kProfil});
    
    %% Lecture des layers "Range", "AcrossDistance", "AlongDistance" et "BeamDepressionAngle"
    
    a = cl_sdf('nomFic', listFileNames{kProfil});
    if isempty(a)
        continue
    end
    
    if ~isNavPreprocessed(a)
        continue
    end
    
    [flag, c, Carto] = read_Image(a, 'Carto', Carto, 'SourceHeading', SourceHeading);
    if ~flag
        return
    end
    
    c = c(indLayer);
    
    clear a
    nbRows = c.nbRows;
    if nbRows <= 1
        Message = sprintf('The file %s is discarded because it has %d row.', listFileNames{kProfil}, nbRows);
        my_warndlg(Message, 0);
        continue
    end
    
    Heading       = get(c, 'Heading');
    FishLatitude  = get(c, 'FishLatitude');
    FishLongitude = get(c, 'FishLongitude');
    if isempty(FishLatitude) || isempty(FishLongitude)
        continue
    end
    
    GT_Image = c.GeometryType;
    if TypeCalibration == 1
        GT_Bilan = cl_image.indGeometryType(bilan{1}(1).GeometryType);
        if ~isempty(CorFile) && (GT_Image == GT_Bilan)
            TxAngle_PingSamples = sonar_lateral_emission(c, 'useRoll', 0);
            c = compensationCourbesStats(c, TxAngle_PingSamples, bilan);
            clear TxAngle_PingSamples
        end
    end
    
    Reflectivity_PingAcrossDist = sonarLateral_PingRange2PingAcrossDist(c, 'resolutionX', resol, 'createNbp', 0, 'NoWaitbar', 1);
    
    [flag, LatLon] = sonar_calcul_coordGeo_SonarX(Reflectivity_PingAcrossDist, Heading, ...
        FishLatitude, FishLongitude, 'NoWaitbar', 1);
    if ~flag
        continue
    end
    clear c
    
    %% Compensation de l'image si la courbe a �t� calcul�e dans la g�om�trie
    % PingSamples
    
    TxAngle_PingAcrossDist = sonar_lateral_emission(Reflectivity_PingAcrossDist, 'useRoll', 0);
    GT_Image = Reflectivity_PingAcrossDist.GeometryType;
    if TypeCalibration == 1
        GT_Bilan = cl_image.indGeometryType(bilan{1}(1).GeometryType);
        if ~isempty(CorFile) && (GT_Image == GT_Bilan)
            Reflectivity_PingAcrossDist = compensationCourbesStats(Reflectivity_PingAcrossDist, TxAngle_PingAcrossDist, bilan);
        end
    end
    
    nbRows = Reflectivity_PingAcrossDist.nbRows;
    nbCol = Reflectivity_PingAcrossDist.nbColumns;
    pas = floor(nbRows / ((nbRows * nbCol * 4) / 1e7)); % SizePhysTotalMemory
    
    NBlocks = ceil(nbRows/pas);
    pas = ceil(nbRows/NBlocks);
    
    KO = 0;
    for k=1:pas:nbRows
        subl = max(1,k-5):min(k-1+5+pas, nbRows);
        fprintf('Processing pings [%d:%d] Fin=%d\n', subl(1), subl(end), nbRows);
        
        [flag, Z_Mosa, A_Mosa] = sonar_mosaique2(Reflectivity_PingAcrossDist, TxAngle_PingAcrossDist, LatLon(1), LatLon(2), resol, ...
            'AcrossInterpolation', 2, 'AlongInterpolation',  AlongInterpolation, ...
            'suby', subl, 'NoWaitbar', 1);
        if ~flag
            return
        end
        if isempty(A_Mosa)
            KO = 1;
            break
        end
        
        Z_Mosa.Writable = false;
        A_Mosa.Writable = false;
        
        if isempty(Z_Mosa_All)
            Z_Mosa_All = Z_Mosa;
            A_Mosa_All = A_Mosa;
        else
            [Z_Mosa_All, A_Mosa_All] = mosaique([Z_Mosa_All Z_Mosa], 'Angle', [A_Mosa_All A_Mosa], 'FistImageIsCollector', 1);
            clear Z_Mosa A_Mosa
        end
        Z_Mosa_All.Name = 'Reflectivity mosaic';
        A_Mosa_All.Name = 'Emission angle mosaic';
        Z_Mosa_All.Writable = false;
        A_Mosa_All.Writable = false;
    end
    clear Reflectivity_PingAcrossDist TxAngle_PingAcrossDist LatLon
    if KO
        continue
    end
    
    %% Sauvegarde des mosaiques dans des fichiers ErMapper
    
    if (mod(kProfil,Backup) == 0) || (kProfil == nbProfils)
        Z_Mosa_All = majCoordonnees(Z_Mosa_All);
        A_Mosa_All = majCoordonnees(A_Mosa_All);
        
        [~, ImageName] = fileparts(nomFicErMapperLayer);
        Z_Mosa_All.Name = ImageName;
        
        [~, ImageName] = fileparts(nomFicErMapperAngle);
        A_Mosa_All.Name = ImageName;
        
        export_ermapper(Z_Mosa_All, nomFicErMapperLayer);
        export_ermapper(A_Mosa_All, nomFicErMapperAngle);
        
        deleteFileOnListe(cl_memmapfile.empty, gcbf)
    end
end
my_close(hw, 'MsgEnd');

if isempty(Z_Mosa_All)
    Mosa = [];
else
    clear Mosa
    Mosa(1) = Z_Mosa_All;
    Mosa(2) = A_Mosa_All;
end

flag = 1;
