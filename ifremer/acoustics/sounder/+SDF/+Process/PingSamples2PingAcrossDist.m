function flag = PingSamples2PingAcrossDist(listFileNames, varargin)

persistent persistent_nomDirErs

[varargin, resol]  = getPropertyValue(varargin, 'Resol',  []); %#ok<ASGLU>

if ischar(listFileNames)
    listFileNames = {listFileNames};
end

nbProfils = length(listFileNames);
nomDir = fileparts(listFileNames{1});

if isempty(persistent_nomDirErs)
    nomDirErs = nomDir;
else
    nomDirErs = persistent_nomDirErs;
end

%% Choix de la fr�quence

ListeLayers = {'132kHz'; '445kHz'};
[indLayer, flag] = my_listdlg('Layers :', ListeLayers, 'InitialValue', 1, 'SelectionMode', 'Single');
if ~flag || isempty(indLayer)
    flag = 0;
    return
end

%% Saisie de la r�solution

[flag, resol] = get_gridSize('Value', -resol, 'Initial', 0.2);
if ~flag
    return
end

%% Origine du cap

[flag, SourceHeading] = SDF.Params.selectSourceHeading;
if ~flag
    return
end

%% Nom du r�pertoire de stockage des images

str1 = 'S�lectionner un r�pertoire o� stocker les images en PingAcrossDist';
str2 = 'Please select a folder for the PingAcrossDist images';
[flag, nomDirErs] = my_uigetdir(nomDirErs, Lang(str1,str2));
if ~flag
    return
end
persistent_nomDirErs = nomDirErs;

%% Si Mosaique de Reflectivite, on propose de faire une compensation statistique ou une calibration

% [flag, TypeCalibration, CorFile, bilan, DataTypeConditions, alpha, ModelTxDiag, CourbeBS] = params_SonarCompensation(nomDir);
[flag, TypeCalibration, CorFile, bilan] = params_SonarCompensation(nomDir);
if ~flag
    return
end

%% Traitement des fichiers

% profile on
% tic

Carto = [];
for k=1:nbProfils
    fprintf('Processing file %d/%d : %s\n', k, nbProfils, listFileNames{k});
    
    %% Lecture des layers "Range", "AcrossDistance", "AlongDistance" et "BeamDepressionAngle"
    
    a = cl_sdf('nomFic', listFileNames{k});
    if isempty(a)
        continue
    end
    
    if ~isNavPreprocessed(a)
        continue
    end
    
    [flag, c, Carto] = read_Image(a, 'Carto', Carto, 'SourceHeading', SourceHeading);
    if ~flag
        return
    end
    
    c = c(indLayer);
    
    clear a
    nbRows = c.nbRows;
    if nbRows <= 1
        Message = sprintf('The file %s is discarded because it has %d row.', listFileNames{k}, nbRows);
        my_warndlg(Message, 0);
        continue
    end
    
    GT_Image = c.GeometryType;
    if TypeCalibration == 1
        GT_Bilan = cl_image.indGeometryType(bilan{1}(1).GeometryType);
        if ~isempty(CorFile) && (GT_Image == GT_Bilan)
            TxAngle_PingSamples = sonar_lateral_emission(c, 'useRoll', 0);
            c = compensationCourbesStats(c, TxAngle_PingSamples, bilan);
            clear TxAngle_PingSamples
        end
    end
    
    Reflectivity_PingAcrossDist = sonarLateral_PingRange2PingAcrossDist(c, 'resolutionX', resol, 'createNbp', 0);
    clear c
    
    %% Compensation de l'image si la courbe a �t� calcul�e dans la g�om�trie
    % PingSamples
    
    TxAngle_PingAcrossDist = sonar_lateral_emission(Reflectivity_PingAcrossDist, 'useRoll', 0);
    GT_Image = Reflectivity_PingAcrossDist.GeometryType;
    if TypeCalibration == 1
        GT_Bilan = cl_image.indGeometryType(bilan{1}(1).GeometryType);
        if ~isempty(CorFile) && (GT_Image == GT_Bilan)
            Reflectivity_PingAcrossDist = compensationCourbesStats(Reflectivity_PingAcrossDist, TxAngle_PingAcrossDist, bilan);
        end
    end
    
    %% Sauvegarde des images
    
    nomImage = Reflectivity_PingAcrossDist.Name;
    nomFicErs = fullfile(nomDirErs, [nomImage '.ers']);

    flag = export_ermapper(Reflectivity_PingAcrossDist, nomFicErs);
    if ~flag
        % Message SVP
    end
    
end
flag = 1;

% toc
% profile report
