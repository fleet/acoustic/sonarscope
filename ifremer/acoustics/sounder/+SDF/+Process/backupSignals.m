function flag = backupSignals(nomFic, nomSignals, backupFileName)

if ~iscell(nomFic)
    nomFic = {nomFic};
end

if ~iscell(nomSignals)
    nomSignals = {nomSignals};
end

N = length(nomFic);
str1 = 'Sauvegarde des signaux SDF';
str2 = 'Backuping SDF signals';
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    flag = BackupSignals_unitaire(nomFic{k}, nomSignals, backupFileName);
    if ~flag
        continue
    end
end
my_close(hw, 'MsgEnd')


function flag = BackupSignals_unitaire(nomFic, nomSignals, backupFileName)

flag = 0;

this = cl_sdf('nomFic', nomFic);
if isempty(this)
    return
end

% PageVersion = get(this, 'PageVersion');
PageVersion = this.PageVersion;
for k=1:length(nomSignals)
    [~, X] = SScCacheNetcdfUtils.getSignalUnit(nomFic, PageVersion, nomSignals{k});
    Data.(nomSignals{k}) = X;
end

[~, grpName] = fileparts(nomFic);
Data.Dimensions.nbPings = length(X);
NetcdfUtils.Struct2Netcdf(Data, backupFileName, grpName)

flag = 1;
