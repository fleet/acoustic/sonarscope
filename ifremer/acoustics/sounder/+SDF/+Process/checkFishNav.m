function flag = checkFishNav(nomFic, varargin)

[varargin, OriginNav] = getPropertyValue(varargin, 'OriginNav', 'Fish'); %#ok<ASGLU> % Fish | Ship

flag = 0;

if ~iscell(nomFic)
    nomFic = {nomFic};
end

%% Lecture des signaux

N = length(nomFic);
str1 = sprintf('Contr�le de la navigation %s des fichiers SDF (USBL ou recalcul�e)', OriginNav);
str2 = sprintf('Check %s navigation (USBL or recomputed)', OriginNav);
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=N:-1:1
    my_waitbar(N-k+1, N, hw);
    
    a = cl_sdf('nomFic', nomFic{k});
    if isempty(a)
        return
    end
    
    X = get(a, 'Time');
    X = X.timeMat;
    Tdeb(k) = X(1);
    Tfin(k) = X(end);
    SonarTime{k} = X';
    
    if strcmp(OriginNav, 'Fish')
        X = get(a, 'fishLon');
        Longitude{k} = X';
        
        X = get(a, 'fishLat');
        Latitude{k} = X';
    else
        X = get(a, 'shipLon');
        Longitude{k} = X';
        
        X = get(a, 'shipLat');
        Latitude{k} = X';
    end
    
    X = get(a, 'heading');
    Heading{k} = X';
    
    X = get(a, 'pitch');
    Pitch{k} = X';
    
    X = get(a, 'roll');
    Roll{k} = X';
    
    X = get(a, 'depth');
    Immersion{k} = X';
    
    X = get(a, 'altitudeMeters');
    AltitudeMeters{k} = X';
    
    X = get(a, 'speed');
    Speed{k} = X';
    
    X = get(a, 'shipHeading');
    shipHeading{k} = X';
    
    X = get(a, 'cableOut');
    CableOut{k} = X';
    
    X = get(a, 'fishNavHeading');
    fishNavHeading{k} = X';
    
    nbPings(k) = length(X);
end
my_close(hw)

%% Ordre chronologique

[~, sub] = sort(Tdeb);
Tdeb           = Tdeb(sub);
Tfin           = Tfin(sub);
nbPings        = nbPings(sub);
nomFic         = nomFic(sub);
SonarTime      = SonarTime(sub);
Longitude      = Longitude(sub);
Latitude       = Latitude(sub);
Heading        = Heading(sub);
Pitch          = Pitch(sub);
Roll           = Roll(sub);
Immersion      = Immersion(sub);
AltitudeMeters = AltitudeMeters(sub);
Speed          = Speed(sub);
shipHeading    = shipHeading(sub);
CableOut       = CableOut(sub);
fishNavHeading = fishNavHeading(sub);

% FigUtils.createSScFigure; plot(Longitude, Latitude, '-*'); grid on;

%% Essai de segmentation des lignes

EcartEntreFichiers = (Tdeb(2:end) - Tfin(1:end-1)) * (3600*24);

kProfil = 1;
subProcess{1} = 1;
for k=1:length(EcartEntreFichiers)
    if EcartEntreFichiers(k) < 5 % Ecarts inf�rieur � 5 s
        subProcess{kProfil} = [subProcess{kProfil} k+1]; %#ok<AGROW>
    else
        kProfil = kProfil + 1;
        subProcess{kProfil} = k+1; %#ok<AGROW>
    end
end

% FigUtils.createSScFigure;
% Color = 'rgbcmyk';

N = length(subProcess);
str1 = 'Sauvegarde des signaux nettoy�s dans le cache de SSc';
str2 = 'Backup cleaned signals in SSc cache';
hw = create_waitbar(Lang(str1,str2), 'N', N);
for ksub=1:N
    my_waitbar(ksub, N, hw)
    sub = subProcess{ksub};
    Data.Tdeb           = Tdeb(sub);
    Data.Tfin           = Tfin(sub);
    Data.nbPings        = nbPings(sub);
    Data.nomFic         = nomFic(sub);
    Data.SonarTime      = [SonarTime{sub}];
    Data.Longitude      = [Longitude{sub}];
    Data.Latitude       = [Latitude{sub}];
    Data.Heading        = [Heading{sub}];
    Data.Pitch          = [Pitch{sub}];
    Data.Roll           = [Roll{sub}];
    Data.Immersion      = [Immersion{sub}];
    Data.AltitudeMeters = [AltitudeMeters{sub}];
    Data.Speed          = [Speed{sub}];
    Data.shipHeading    = [shipHeading{sub}];
    Data.CableOut       = [CableOut{sub}];
    Data.fishNavHeading = [fishNavHeading{sub}];
    
    %% V�rification de la navigation du poisson
    
    [flag, Data] = EditFishNav(Data);
    if flag
        
        %% Sauvegarde des r�sultats
        
        sub = 0;
        for k=1:length(Data.nomFic)
            sub = sub(end) + (1:Data.nbPings(k));
            %         PlotUtils.createSScPlot(Data.Longitude(sub), Data.Latitude(sub), ['+' Color(1+mod(k,length(Color)))]); grid on; hold on; %title(Data.nomFic{k})
            
            a = cl_sdf('nomFic', Data.nomFic{k});
            
            if strcmp(OriginNav, 'Fish')
                flag = save_signal(a, 'fishLon', Data.Longitude(sub));
                if ~flag
                    return
                end
                
                flag = save_signal(a, 'fishLat', Data.Latitude(sub));
                if ~flag
                    return
                end
            else
                flag = save_signal(a, 'shipLon', Data.Longitude(sub));
                if ~flag
                    return
                end
                
                flag = save_signal(a, 'shipLat', Data.Latitude(sub));
                if ~flag
                    return
                end
            end
            
            flag = save_signal(a, 'heading', Data.Heading(sub));
            if ~flag
                return
            end
            
            flag = save_signal(a, 'shipHeading', Data.shipHeading(sub));
            if ~flag
                return
            end
            
            flag = save_signal(a, 'depth', Data.Immersion(sub));
            if ~flag
                return
            end
            
            flag = save_signal(a, 'altitudeMeters', Data.AltitudeMeters(sub));
            if ~flag
                return
            end
            
            flag = save_signal(a, 'cableOut', Data.CableOut(sub));
            if ~flag
                return
            end
            
            flag = save_signal(a, 'fishNavHeading', Data.fishNavHeading(sub));
            if ~flag
                return
            end
        end
    end
    
    %     if ksub ~= length(subProcess)
    %         if (repCheckLatLon ~= 3) || (repCheckHeading ~= 3)
    %             str1 = 'On continue avec le prochain profil ?';
    %             str2 = 'Do we continue with the next line ?';
    %             [rep, flag] = my_questdlg(Lang(str1,str2));
    %             if ~flag || (rep == 2)
    %                 my_close(hw)
    %                 return
    %             end
    %         end
    %     end
end
my_close(hw, 'MsgEnd');
