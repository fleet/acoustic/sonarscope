function flag = checkHeights(nomFic, varargin)

if ~iscell(nomFic)
    nomFic = {nomFic};
end

%% Traitement des fichiers

firstTime = 1;
Carto = [];
N = length(nomFic);
str1 = 'Contr�le qualit� des signaux des fichiers SDF';
str2 = 'SDF Quality Control processing';
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
    my_waitbar(k, N, hw);
    
    a = cl_sdf('nomFic', nomFic{k});
    if isempty(a)
        %         str1 = sprintf('ATTENTION : "%s" n''a pas pu �tre d�cod�.', nomFic{k});
        %         str2 = sprintf('WARNING : "%s" could not be loaded.', nomFic{k});
        %         my_warndlg(Lang(str1,str2), 0);
        continue
    end
    
    %% Affichage de l'image dans SonarScope pour v�rifier la hauteur
    
    if firstTime
        messageCheckHeight();
        firstTime = 0;
    end
    
    [flag, b, Carto] = read_Image(a, 'Carto', Carto);
    if ~flag
        break
    end
    
    c = SonarScope(b(1), 'visuProfilY', 1, 'profilY.Type', 'Height', 'visuProfilX', 1, 'ButtonSave', 1);
    SonarHeight = get(c(1), 'SonarHeight');
    sampleFreq  = get(c(1), 'SonarSampleFrequency');
    speedSound  = get(c(1), 'SonarSurfaceSoundSpeed');
    altitude = -speedSound(:) .* SonarHeight ./ (2 * sampleFreq(:));
    %         altitude = getHeightInMeters(a, SonarHeight); % Aquivalent � ce
    %         qui est fait au-dessus
    
    flag = save_signal(a, 'altitudeMeters', altitude);
    if ~flag
        return
    end
    
    %% On demande si on veut traiter les autres fichiers
    
    if k ~= N
        flag = question_ContinueNextFile();
        if ~flag
            break
        end
    end
end
my_close(hw, 'MsgEnd')
