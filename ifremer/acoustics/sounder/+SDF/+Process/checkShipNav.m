function flag = checkShipNav(nomFic, varargin)

if ~iscell(nomFic)
    nomFic = {nomFic};
end

DistanceAntenneGPSPoulieCompteuse = 12.5;

Format1 = 'V�rification de la %s.\n\nUne fen�tre SignalScope va s''ouvrir, le signal de %s est en noir et une version filtr�e est en rouge. Zoomez sur le signal pour voir si la version filtr�e est meilleure (elle rem�die souvent � des marches d''escalier dues � une quantification de la mesure de positionnemen).\n\nSi vous conservez le filtre et que vous clickez sur Save et Quit c''est lui qui sera sauvegard�.';
Format2 = 'Checking %s.\n\nA SignalScope window is going to be open, %s signal is the black one, the red one is a filtered version. Zoom on the signal to check if the filter is efficient (it often solves problems of stair steps due to signal quantification).\n\nIf you conserve the filter AND you click on Save and Quit il will be the red curve which will be saved.';
Format3 = 'V�rification de la %s.\n\nUne fen�tre SignalScope va s''ouvrir. Zoomez sur le signal pour voir si un filtrage s''impose.';
Format4 = 'Checking %s.\n\nA SignalScope window is going to be opened. Zoom on the signal to check if a filter is necessary.';

%% Traitement des fichiers

FiltreDelay.Type     = 6;
FiltreLonLat.Type    = 4;
FiltreImmersion.Type = 5;
FiltreCableOut.Type  = 5;
FiltreHeading.Type   = 4;
   
%% Lecture des signaux

N = length(nomFic);
str1 = 'Contr�le qualit� des signaux des fichiers SDF';
str2 = 'SDF Quality Control processing';
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=N:-1:1
    my_waitbar(N-k+1, N, hw);
    
    a = cl_sdf('nomFic', nomFic{k});
    if isempty(a)
        return
    end
    
    X = get(a, 'Time');
    X = X.timeMat;
    Tdeb(k) = X(1);
    Tfin(k) = X(end);
    SonarTime{k} = X';
    
    X = get(a, 'shipLon');
    ShipLongitude{k} = X';
    
    X = get(a, 'shipLat');
    ShipLatitude{k} = X';
    
    X = get(a, 'shipHeading');
    ShipHeading{k} = X';
    
    X = get(a, 'depth');
    Immersion{k} = X';
    
    X = get(a, 'cableOut');
    CableOut{k} = X';
    
    X = get(a, 'speed');
    Speed{k} = X';
    
    X = get(a, 'heading');
    Heading{k} = X';
    
    X = get(a, 'altitudeMeters');
    AltitudeMeters{k} = X';
    
    X = get(a, 'pitch');
    Pitch{k} = X';
    
    X = get(a, 'roll');
    Roll{k} = X';
    
    nbPings(k) = length(X);
end
my_close(hw)

%% Ordre chronologique

[~, sub] = sort(Tdeb);
Tdeb = Tdeb(sub);
Tfin = Tfin(sub);
nbPings        = nbPings(sub);
nomFic         = nomFic(sub);
SonarTime     = SonarTime(sub);
ShipLongitude  = ShipLongitude(sub);
ShipLatitude   = ShipLatitude(sub);
ShipHeading    = ShipHeading(sub);
Immersion      = Immersion(sub);
CableOut       = CableOut(sub);
Speed          = Speed(sub);
Heading        = Heading(sub);
Pitch          = Pitch(sub);
Roll           = Roll(sub);
AltitudeMeters = AltitudeMeters(sub);

% FigUtils.createSScFigure; plot(ShipLongitude, ShipLatitude, '-*'); grid on;

%% Essai de segmentation des lignes

EcartEntreFichiers = (Tdeb(2:end) - Tfin(1:end-1)) * (3600*24);

kProfil = 1;
subProcess{1} = 1;
for k=1:length(EcartEntreFichiers)
    if EcartEntreFichiers(k) < 5 % Ecarts inf�rieur � 5 s
        subProcess{kProfil} = [subProcess{kProfil} k+1]; %#ok<AGROW>
    else
        kProfil = kProfil + 1;
        subProcess{kProfil} = k+1; %#ok<AGROW>
    end
end

FigUtils.createSScFigure; 
Color = 'rgbcmyk';
repCheckLatLon    = 1;
repCheckImmersion = 1;
repCheckCableOut  = 1;
repCheckHeading   = 1;
repCheckDelay     = 1;

N = length(subProcess);
str1 = 'Calcul de la position du poisson en fonction de la longueur de cable';
str2 = 'Processing Fish position from layback';
hw = create_waitbar(Lang(str1,str2), 'N', N);
for ksub=1:N
    my_waitbar(ksub, N, hw)
    sub = subProcess{ksub};
    Data.Tdeb           = Tdeb(sub);
    Data.Tfin           = Tfin(sub);
    Data.nbPings        = nbPings(sub);
    Data.nomFic         = nomFic(sub);
    Data.SonarTime      = [SonarTime{sub}];
    Data.Longitude      = [ShipLongitude{sub}];
    Data.Latitude       = [ShipLatitude{sub}];
    Data.shipHeading    = [ShipHeading{sub}];
    Data.Immersion      = [Immersion{sub}];
    Data.CableOut       = [CableOut{sub}];
    Data.Speed          = [Speed{sub}];
    Data.Heading        = [Heading{sub}];
    Data.AltitudeMeters = [AltitudeMeters{sub}];
    Data.Pitch          = [Pitch{sub}];
    Data.Roll           = [Roll{sub}];
    
    %% Calcul de la navigation du poisson
    
    [flag, Data, FiltreLonLat, FiltreImmersion, FiltreCableOut, FiltreDelay, FiltreHeading, ...
        repCheckLatLon, repCheckImmersion, repCheckCableOut, repCheckHeading, repCheckDelay] = ...
        CheckAndComputeNavFishFromCabeout(Data, DistanceAntenneGPSPoulieCompteuse, ...
        Format1, Format2, Format3, Format4, ksub, ...
        FiltreLonLat, FiltreImmersion, FiltreCableOut, FiltreDelay, FiltreHeading, ...
        repCheckLatLon, repCheckImmersion, repCheckCableOut, repCheckHeading, repCheckDelay);
    if ~flag
        return
    end
    
    %% Sauvegarde des r�sultats
    
    sub = 0;
    for k=1:length(Data.nomFic)
        sub = sub(end) + (1:Data.nbPings(k));
        PlotUtils.createSScPlot(Data.Longitude(sub), Data.Latitude(sub), ['+' Color(1+mod(k,length(Color)))]); grid on; hold on; %title(Data.nomFic{k})
        
        a = cl_sdf('nomFic', Data.nomFic{k});
        
        flag = save_signal(a, 'shipLon', Data.Longitude(sub));
        if ~flag
            return
        end
        flag = save_signal(a, 'shipLat', Data.Latitude(sub));
        if ~flag
            return
        end
        flag = save_signal(a, 'depth', Data.Immersion(sub));
        if ~flag
            return
        end
        flag = save_signal(a, 'cableOut', Data.CableOut(sub));
        if ~flag
            return
        end
        flag = save_signal(a, 'heading', Data.Heading(sub));
        if ~flag
            return
        end
        flag = save_signal(a, 'shipHeading', Data.shipHeading(sub));
        if ~flag
            return
        end
        flag = save_signal(a, 'altitudeMeters', Data.AltitudeMeters(sub));
        if ~flag
            return
        end
        
        % [fishNavHeading, speed, distance, acceleration] = calCapFromLatLon(Data.Latitude, Data.Longitude, 'Time', T);
        Data.fishNavHeading = calCapFromLatLon(Data.Latitude, Data.Longitude);

        flag = save_signal(a, 'fishNavHeading', Data.fishNavHeading(sub));
        if ~flag
            return
        end
    end
    
    if ksub ~= length(subProcess)
        if (repCheckLatLon ~= 3) || (repCheckImmersion ~= 3) || (repCheckCableOut ~= 3) || (repCheckHeading ~= 3) || (repCheckDelay ~= 3)
            str1 = 'On continue avec le prochain profil ?';
            str2 = 'Do we continue with the next line ?';
            [rep, flag] = my_questdlg(Lang(str1,str2));
            if ~flag || (rep == 2)
                my_close(hw)
                return
            end
        end
    end
end
my_close(hw, 'MsgEnd');


function [flag, Data, FiltreLonLat, FiltreImmersion, FiltreCableOut, FiltreDelay, FiltreHeading, ...
    repCheckLatLon, repCheckImmersion, repCheckCableOut, repCheckHeading, repCheckDelay] ...
    = CheckAndComputeNavFishFromCabeout(Data, DistanceAntenneGPSPoulieCompteuse, Format1, Format2, Format3, Format4, kFile, ...
    FiltreLonLat, FiltreImmersion, FiltreCableOut, FiltreDelay, FiltreHeading, ...
    repCheckLatLon, repCheckImmersion, repCheckCableOut, repCheckHeading, repCheckDelay) %#ok<INUSL> 

strListe{1} = Lang('Oui', 'Yes');
strListe{2} = Lang('Non pour cette-ci', 'Not this one');
strListe{3} = Lang('Non pour toutes', 'Not for all');

%% V�rification de la navigation du poisson

[flag, Data] = EditFishNav(Data);
if ~flag
    return
end

%% Calcul de la distance horizontale bateau-poisson

H = Data.Immersion; % + 3;
distanceFish = Data.CableOut .^ 2 - H .^2;
distanceFish(distanceFish < 0) = 0;
distanceFish = sqrt(distanceFish);
if all(distanceFish == 0)
    str = 'It seems the immersion is higher than the cableout length : it is impossible, the delay cannot be computed.';
    my_warndlg(str, 0, 'Tag', 'DonneesIncoherentes', 'displayItEvenIfSSc', 1);
    return
end

%% Calcul du d�calage temporel de la navigation

distanceFish = distanceFish + DistanceAntenneGPSPoulieCompteuse;
Delay = double(distanceFish) ./ double(Data.Speed);

if repCheckDelay ~= 3
    str1 = 'Voulez-vous v�rifier le d�calage temporel ?';
    str2 = 'Check time delay ?';
    [repCheckDelay, flag] = my_listdlg(Lang(str1,str2), strListe, 'SelectionMode', 'Single');
    if ~flag
        return
    end
    if repCheckDelay == 1
        if kFile == 1
            str1 = sprintf(Format3, 'Delay', 'Delay');
            str2 = sprintf(Format4, 'Delay', 'Delay');
            my_warndlg(Lang(str1,str2), 1);
        end
        [Delay, FiltreDelay] = controleSignalNEW('Delay', Delay, 'Filtre', FiltreDelay);
    end
end

%% D�calage temporel de la navigation

% figure;
% subplot(3,1,1); plot(Data.SonarTime); grid on;
% subplot(3,1,2); plot(Delay/(24*3600)); grid on;
% subplot(3,1,3); plot(Data.SonarTime - (Delay/(24*3600))); grid on;
Time = Data.SonarTime - (Delay/(24*3600));
Data.Longitude = my_interp1_longitude(Data.SonarTime, Data.Longitude(:), Time);
Data.Latitude  = my_interp1(Data.SonarTime, Data.Latitude(:), Time);
% figure; plot(Data.Longitude, Data.Latitude); grid on

%% Calcul du cap

%{
Heading  = calCapFromLatLon(Latitude, Longitude, 'Time', Time);
flag = save_signal(this, 'heading', Heading);
if ~flag
    return
end

%% Contr�le du cap calcul�

if repCheckHeading ~= 3
    str1 = 'Voulez-vous v�rifier le cap du poisson ?';
    str2 = 'Check fish heading ?';
    [repCheckHeading, flag] = my_listdlg(Lang(str1,str2), strListe, 'SelectionMode', 'Single');
    if ~flag
        return
    end
    if repCheckHeading == 1
        if kFile == 1
            str1 = sprintf(Format3, 'Heading', 'Heading');
            str2 = sprintf(Format4, 'Heading', 'Heading');
            my_warndlg(Lang(str1,str2), 1);
        end
        [Data.Heading, FiltreHeading] = controleSignalNEW('Heading', Data.Heading, 'Filtre', FiltreHeading);
    end
end
%}

% figure; plot(Longitude, Latitude); grid on
% figure; plot(Heading); grid on; hold on; plot(Signal, '+r');

flag = 1;


%{
function [Signal, Filtre] = controleCableOut(NomSignal, Signal, varargin)

subKO = find(Signal <= 0);
if ~isempty(subKO)
    subOK = find(Signal > 0);
    if ~isempty(subOK)
        Signal(subKO) = interp1(subOK, Signal(subOK), subKO, 'linear', 'extrap');
    end
end
[Signal, Filtre] = controleSignalNEW(NomSignal, Signal, varargin{:});
%}
