function flag = computeFishNavFromShipNav(nomFic, varargin)

persistent persistent_DistanceAntenneGPSPoulieCompteuse

if ~iscell(nomFic)
    nomFic = {nomFic};
end

%% Warning

str = 'Warning : this processing must be done ONLY if you do not have a real time Fish navigation (Ex : USBL). Do you want to continue ?';
[rep, flag] = my_questdlg(str, 'Init', 2);
if ~flag || (rep == 2)
    return
end

%% Distance entre la poulie compteuse et l'antenne GPS

if isempty(persistent_DistanceAntenneGPSPoulieCompteuse)
    DistanceAntenneGPSPoulieCompteuse = 12.5;
else
    DistanceAntenneGPSPoulieCompteuse = persistent_DistanceAntenneGPSPoulieCompteuse;
end

str1 = 'Distance entre la poulie et l''antenne GPS';
str2 = 'Distance between the pulley and the GPS antena';
[flag, DistanceAntenneGPSPoulieCompteuse] = inputOneParametre(Lang(str1,str2), 'Value', ...
    'Value', DistanceAntenneGPSPoulieCompteuse, 'Unit', 'm', 'MinValue', 0, 'MaxValue', 500);
if ~flag
    return
end
persistent_DistanceAntenneGPSPoulieCompteuse = DistanceAntenneGPSPoulieCompteuse;
   
%% Utilisation de l'immersion

strImmer = {'Use immersion sensor'};
strImmer{2} = 'Estimate the immersion value from the cableout';
[identImmersion, flag] = my_listdlg('For Ifremer Klein sonar, the immersion sensor is sometimes out of order. It it is the case please use the second option.', strImmer, 'SelectionMode', 'Single');
if ~flag
    return
end

if identImmersion == 2
    [flag, immerCoef] = inputOneParametre('Immersion value will be "Cableout / N"', 'N', 'Value', 5);
end

%% Lecture des signaux

N = length(nomFic);
str1 = 'Contr�le qualit� des signaux des fichiers SDF';
str2 = 'SDF Quality Control processing';
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=N:-1:1
    my_waitbar(N-k+1, N, hw);
    
    a = cl_sdf('nomFic', nomFic{k});
    if isempty(a)
        return
    end
    
    X = get(a, 'Time');
    X = X.timeMat;
    Tdeb(k) = X(1);
    Tfin(k) = X(end);
    SonarTime{k} = X';
    
    X = get(a, 'shipLon');
    ShipLongitude{k} = X';
    
    X = get(a, 'shipLat');
    ShipLatitude{k} = X';
    
    X = get(a, 'cableOut');
    CableOut{k} = X';
    
    if identImmersion == 1
        X = get(a, 'depth');
        Immersion{k} = X';
    else
        Immersion{k} = CableOut{k} / immerCoef;
    end
    
    X = get(a, 'speed');
    Speed{k} = X';
    
    X = get(a, 'heading');
    Heading{k} = X';
    
    nbPings(k) = length(X);
end
my_close(hw)

%% Ordre chronologique

[~, sub] = sort(Tdeb);
Tdeb = Tdeb(sub);
Tfin = Tfin(sub);
nbPings       = nbPings(sub);
nomFic        = nomFic(sub);
SonarTime     = SonarTime(sub);
ShipLongitude = ShipLongitude(sub);
ShipLatitude  = ShipLatitude(sub);
Immersion     = Immersion(sub);
CableOut      = CableOut(sub);
Speed         = Speed(sub);
Heading       = Heading(sub);

% FigUtils.createSScFigure; plot(ShipLongitude, ShipLatitude, '-*'); grid on;

%% Essai de segmentation des lignes

EcartEntreFichiers = (Tdeb(2:end) - Tfin(1:end-1)) * (3600*24);

kProfil = 1;
subProcess{1} = 1;
for k=1:length(EcartEntreFichiers)
    if EcartEntreFichiers(k) < 5 % Ecarts inf�rieur � 5 s
        subProcess{kProfil} = [subProcess{kProfil} k+1]; %#ok<AGROW>
    else
        kProfil = kProfil + 1;
        subProcess{kProfil} = k+1; %#ok<AGROW>
    end
end

% FigUtils.createSScFigure; 
% Color = 'rgbcmyk';

N = length(subProcess);
str1 = 'Calcul de la position du poisson en fonction de la longueur de cable';
str2 = 'Processing Fish position from layback';
hw = create_waitbar(Lang(str1,str2), 'N', N);
for ksub=1:N
    my_waitbar(ksub, N, hw)
    sub = subProcess{ksub};
    Data.Tdeb          = Tdeb(sub);
    Data.Tfin          = Tfin(sub);
    Data.nbPings       = nbPings(sub);
    Data.nomFic        = nomFic(sub);
    Data.SonarTime     = [SonarTime{sub}];
    Data.ShipLongitude = [ShipLongitude{sub}];
    Data.ShipLatitude  = [ShipLatitude{sub}];
    Data.Immersion     = [Immersion{sub}];
    Data.CableOut      = [CableOut{sub}];
    Data.Speed         = [Speed{sub}];
    Data.Heading       = [Heading{sub}];
    
    %% Calcul de la navigation du poisson
    
    [flag, Data] = ComputeNavFishFromCabeout(Data, DistanceAntenneGPSPoulieCompteuse);
    if ~flag
        return
    end
    %{
    Fig1 = plot_navigation_Figure([], 'Klein');
    plot_navigation('Ship', Fig1, Data.ShipLongitude, Data.ShipLatitude, Data.SonarTime, Data.Heading, 'xb');
    plot_navigation('Ship', Fig1, Data.FishLongitude, Data.FishLatitude, Data.SonarTime, Data.Heading, '+r');
    %}
    
    %% Sauvegarde des r�sultats
    
    sub = 0;
    for k=1:length(Data.nomFic)
        sub = sub(end) + (1:Data.nbPings(k));
%         PlotUtils.createSScPlot(Data.FishLongitude(sub), Data.FishLatitude(sub), ['+' Color(1+mod(k,length(Color)))]); grid on; hold on; %title(Data.nomFic{k})
        
        a = cl_sdf('nomFic', Data.nomFic{k});
        
        flag = save_signal(a, 'fishLon', Data.FishLongitude(sub));
        if ~flag
            return
        end
        
        flag = save_signal(a, 'fishLat', Data.FishLatitude(sub));
        if ~flag
            return
        end
    end
end
my_close(hw, 'MsgEnd');




function [flag, Data] = ComputeNavFishFromCabeout(Data, DistanceAntenneGPSPoulieCompteuse)


%% Calcul de la distance horizontale bateau-poisson

H = Data.Immersion; % + 3;
distanceFish = Data.CableOut .^ 2 - H .^2;
distanceFish(distanceFish < 0) = 0;
distanceFish = sqrt(distanceFish);

%% Calcul du d�calage temporel de la navigation

distanceFish = distanceFish + DistanceAntenneGPSPoulieCompteuse;
Delay = double(distanceFish) ./ double(Data.Speed);

%% D�calage temporel de la navigation

% figure;
% subplot(3,1,1); plot(Data.SonarTime); grid on;
% subplot(3,1,2); plot(Delay/(24*3600)); grid on;
% subplot(3,1,3); plot(Data.SonarTime - (Delay/(24*3600))); grid on;
Time = Data.SonarTime - (Delay/(24*3600));
Data.FishLongitude = my_interp1_longitude(Data.SonarTime, Data.ShipLongitude(:), Time);
Data.FishLatitude  = my_interp1(Data.SonarTime, Data.ShipLatitude(:), Time);
% figure; plot(Data.FishLongitude, Data.FishLatitude); grid on

%% Calcul du cap

%{
Heading = calCapFromLatLon(Data.FishLatitude, Data.FishLongitude, 'Time', Time);
flag = save_signal(this, 'heading', Heading);
if ~flag
    return
end
%}

% figure; plot(Data.ShipLongitude, '.-'); grid on
% figure; plot(Data.FishLongitude, '.-'); grid on
% figure; plot(Data.FishLatitude, '.-'); grid on
% figure; plot(Data.FishLongitude, Data.FishLatitude, '.-'); grid on
% figure; plot(Heading); grid on; hold on; plot(Signal, '+r');

flag = 1;
