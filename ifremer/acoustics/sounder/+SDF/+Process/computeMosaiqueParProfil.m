% TODO : s�parer saisie des param�tres et code de calcul !!!
function  [flag, nomDirImport, nomDirExport] = computeMosaiqueParProfil(varargin)

persistent persistent_indLayer persistent_skipExistingMosaic
persistent persistent_nomDirImport persistent_nomDirExport

[varargin, resol]              = getPropertyValue(varargin, 'Resol',              []);
[varargin, nomDirImport]       = getPropertyValue(varargin, 'nomDirImport',       []);
[varargin, nomDirExport]       = getPropertyValue(varargin, 'nomDirExport',       []);
[varargin, listeFicSDF]        = getPropertyValue(varargin, 'listeFic',           []);
[varargin, skipExistingMosaic] = getPropertyValue(varargin, 'skipExistingMosaic', []);
[varargin, indLayer]           = getPropertyValue(varargin, 'indLayer',           []); %#ok<ASGLU>
% [varargin, window] = getPropertyValue(varargin, 'window', []);

%% Recherche des fichiers .sdf

if isempty(listeFicSDF)
    if isempty(nomDirImport)
        if isempty(persistent_nomDirImport)
            nomDirImport = pwd;
        else
            nomDirImport = persistent_nomDirImport;
        end
    end
    [flag, listeFicSDF] = uiSelectFiles('ExtensionFiles', '.sdf', 'RepDefaut', nomDirImport);
    if ~flag || isempty(listeFicSDF)
        flag = 0;
        return
    end
end
nomDirImport = fileparts(listeFicSDF{1});
persistent_nomDirImport = nomDirImport;

%% Choix de la fr�quence

ListeLayers = {'132kHz'; '445kHz'};
if isempty(indLayer)
    if isempty(persistent_indLayer)
        indLayer = 1;
    else
        indLayer = persistent_indLayer;
    end
    [indLayer, flag] = my_listdlg('Layers :', ListeLayers, 'InitialValue', indLayer, 'SelectionMode', 'Single');
    if ~flag || isempty(indLayer)
        flag = 0;
        return
    end
end
persistent_indLayer = indLayer;

%% Saisie de la r�solution

[flag, resol] = get_gridSize('Value', -resol, 'Initial', 0.6);
if ~flag
    return
end

%% Origine du cap

[flag, SourceHeading] = SDF.Params.selectSourceHeading;
if ~flag
    return
end

%% Nom du r�pertoire des mosaiques individuelles

if isempty(persistent_nomDirExport)
    nomDirExport = pwd;
else
    nomDirExport = persistent_nomDirExport;
end

[flag, nomDirExport] = question_NomDirIndividualMosaics(nomDirExport);
if ~flag
    return
end
persistent_nomDirExport = nomDirExport;

%% skipExistingMosaic

if isempty(skipExistingMosaic)
    str1 = 'Ne pas refaire les mosaiques existantes ?';
    str2 = 'Skip existing mosaics ?';
    if isempty(persistent_skipExistingMosaic)
        skipExistingMosaic = 2;
    else
        skipExistingMosaic = persistent_skipExistingMosaic;
    end
    [rep, flag] = my_questdlg(Lang(str1,str2), 'Init', skipExistingMosaic);
    if ~flag
        return
    end
    skipExistingMosaic = (rep == 1);
end
persistent_skipExistingMosaic = skipExistingMosaic;

%% Si Mosaique de Reflectivit�, on propose de faire une compensation statistique ou une calibration

% [flag, TypeCalibration, CorFile, bilan, DataTypeConditions, alpha, ModelTxDiag, CourbeBS] = params_SonarCompensation(nomDir);
[flag, TypeCalibration, CorFile, bilan] = params_SonarCompensation(nomDirImport);
if ~flag
    return
end

%% Constitution de la mosaique / MNT

% profile on
% tic

strResol = num2str(resol);
strResol = strrep(strResol, '.', ',');

nbProfils = length(listeFicSDF);
Carto = [];
str1 = 'Mosaique des fichiers Klein .sdf';
str2 = 'Mosaic of Klein files (.sdf)';
hw = create_waitbar(Lang(str1,str2), 'N', nbProfils);
for k=1:nbProfils
    my_waitbar(k, nbProfils, hw)
    
    fprintf('\n--------------------------------------------------------------------------------------\n');
    fprintf('Processing file %d/%d : %s\n', k, nbProfils, listeFicSDF{k});
    
    [~, nomFic] = fileparts(listeFicSDF{k});
    nomLayer = [nomFic '_' ListeLayers{indLayer} '_' strResol 'm'];
    if skipExistingMosaic
        nomFicErs = fullfile(nomDirExport, [nomLayer '_Reflectivity_LatLong.ers']);
        if exist(nomFicErs, 'file')
            continue
        end
    end
    
    %% Lecture des layers "Range", "AcrossDistance", "AlongDistance" et "BeamDepressionAngle"
    
    a = cl_sdf('nomFic', listeFicSDF{k});
    if isempty(a)
        continue
    end
    
    if ~isNavPreprocessed(a)
        continue
    end
    
    [flag, c, Carto] = read_Image(a, 'Carto', Carto, 'SourceHeading', SourceHeading);
    if ~flag
        return
    end
    
    c = c(indLayer);
    
    clear a
    nbRows = c.nbRows;
    if nbRows <= 1
        Message = sprintf('The file %s is discarded because it has %d row.', listeFicSDF{k}, nbRows);
        my_warndlg(Message, 0);
        continue
    end
    
    Heading       = get(c, 'Heading');
    FishLatitude  = get(c, 'FishLatitude');
    FishLongitude = get(c, 'FishLongitude');
    if isempty(FishLatitude) || isempty(FishLongitude)
        continue
    end
    
    GT_Image = c.GeometryType;
    if TypeCalibration == 1
        GT_Bilan = cl_image.indGeometryType(bilan{1}(1).GeometryType);
        if ~isempty(CorFile) && (GT_Image == GT_Bilan)
            TxAngle_PingSamples = sonar_lateral_emission(c, 'useRoll', 0);
            c = compensationCourbesStats(c, TxAngle_PingSamples, bilan);
            clear TxAngle_PingSamples
        end
    end
    
    Reflectivity_PingAcrossDist = sonarLateral_PingRange2PingAcrossDist(c, 'resolutionX', resol, 'createNbp', 0);
    
    [flag, LatLon] = sonar_calcul_coordGeo_SonarX(Reflectivity_PingAcrossDist, Heading, ...
        FishLatitude, FishLongitude);
    if ~flag
        continue
    end
    clear c
    
    %% Compensation de l'image si la courbe a �t� calcul�e dans la g�om�trie
    % PingSamples
    
    TxAngle_PingAcrossDist = sonar_lateral_emission(Reflectivity_PingAcrossDist, 'useRoll', 0);
    GT_Image = Reflectivity_PingAcrossDist.GeometryType;
    if TypeCalibration == 1
        GT_Bilan = cl_image.indGeometryType(bilan{1}(1).GeometryType);
        if ~isempty(CorFile) && (GT_Image == GT_Bilan)
            Reflectivity_PingAcrossDist = compensationCourbesStats(Reflectivity_PingAcrossDist, TxAngle_PingAcrossDist, bilan);
        end
    end
    
    %% Mosaique de la ligne
    
    [flag, Z_Mosa, A_Mosa] = sonar_mosaique2(Reflectivity_PingAcrossDist, TxAngle_PingAcrossDist, LatLon(1), LatLon(2), resol, ...
        'AcrossInterpolation', 2, 'AlongInterpolation', 2);
    if ~flag
        return
    end
    clear LatLon
    
    %% Interpolation
    
    %     if ~isequal(Window, [1 1])
    %         Z_Mosa = WinFillNaN(Z_Mosa, 'window', Window, 'NoStats');
    %         A_Mosa = WinFillNaN(A_Mosa, 'window', Window, 'NoStats');
    %     end
    
    %% Sauvegarde de la mosa�que du layer principal
    
    nomFicErs = fullfile(nomDirExport, [nomLayer '_Reflectivity_LatLong.ers']);
    flag = export_ermapper(Z_Mosa, nomFicErs);
    if ~flag
        return
    end
    
    %% Sauvegarde de la mosa�que des angles
    
    nomFicErs = fullfile(nomDirExport, [nomLayer '_TxAngle_LatLong.ers']);
    flag = export_ermapper(A_Mosa, nomFicErs);
    if ~flag
        return
    end
    
    %% Suppression des fichiers memmapfile
    
    deleteFileOnListe(cl_memmapfile.empty, gcbf)
end
my_close(hw, 'MsgEnd')
flag = 1;
