% [flag, dataFileName] = FtpUtils.getSScDemoFile('Syp_001_180507104600.sdf', 'OutputDir', 'D:\Temp\ExData')
% a = cl_sdf('nomFic', dataFileName);
%
% [flag, Data] = SSc_ReadDatagrams(dataFileName, 'Ssc_V3001', 'Memmapfile', -1)
% nomFicSummary = my_tempname('.csv');
% flag = SDF.Process.computeSummary(dataFileName, nomFicSummary)

function flag = computeSummary(dataFileName, nomFicSummary)

if ~iscell(dataFileName)
    dataFileName = {dataFileName};
end

%% Test d'�critude du fichier

flag = testEcritureFichier(nomFicSummary);
if ~flag
    return
end
 
%% R�um� de tous les fichiers

NbFic = length(dataFileName);
str1 = 'Cr�ation du r�sum� des fichiers SDF';
str2 = 'Summary of SDF files';
hw = create_waitbar(Lang(str1,str2), 'N', NbFic);
for k=1:NbFic
    my_waitbar(k, NbFic, hw);
    this = cl_sdf('nomFic', dataFileName{k});
    [N, C(k,:)] = Summary_unitaire(this); %#ok<AGROW>
end
my_close(hw, 'MsgEnd');

%% Ecriture du fichier

flag = write_Summary(nomFicSummary, N, C);

%% Affichage des r�sultat dans une table

figure;
t = uitable;
set(t, 'ColumnName', N, 'Data', C)
set(t, 'RearrangeableColumns', 'On')
set(t, 'Unit', 'normalized', 'Position', [0 0 1 1])

%% Affichage des r�sultats dans un �diteur

if flag
    winopen(nomFicSummary)
end


function flag = write_Summary(nomFicSummary, N, C)

%% Comma or not comma ?

sep = getCSVseparator;

%% Cr�ation du fichier r�sum�

flag = 0;
fid = fopen(nomFicSummary, 'w+t');
if fid == -1
    messageErreurFichier(nomFicSummary, 'WriteFailure');
    return
end

%% Ecriture du header

N2 = length(N);
for k2=1:N2
    fprintf(fid, '%s', N{k2});
    if k2 ~= N2
        fprintf(fid, '%s', sep);
    end
end
fprintf(fid, '\n');

%% Ecriture du r�sum� par ligne

N2 = size(C,2);
for k1=1:size(C,1)
    for k2=1:N2
        fprintf(fid, '%s', C{k1,k2});
        if k2 ~= N2
            fprintf(fid, '%s', sep);
        end
    end
    fprintf(fid, '\n');
end

%% Fermeture du fichier

fclose(fid);
flag = 1;


function [N, C] = Summary_unitaire(this)

nomFic = get(this, 'nomFic');
N{1} = 'FileName';
C{1} = nomFic;

[flag, Signal] = get_signal(this, 'heading');
N{end+1} = 'Nb Pings';
N{end+1} = 'Heading (deg - median)';
if flag
    C{end+1} = num2str(length(Signal));
    C{end+1} = num2str(median(Signal), '%5.1f');
else
    C{end+1} = ' ';
    C{end+1} = ' ';
end

[flag, Signal] = get_signal(this, 'altitudeMeters');
N{end+1} = 'Height (m - mean)';
if flag
    C{end+1} = num2str(mean(-Signal, 'omitnan'));
else
    C{end+1} = ' ';
end

[flag, Signal] = get_signal(this, 'roll');
N{end+1} = 'Roll (deg - mean)';
N{end+1} = 'Roll (deg - std)';
if flag
    C{end+1} = num2str(mean(Signal, 'omitnan'));
    C{end+1} = num2str(std( Signal, 'omitnan'));
else
    C{end+1} = ' ';
    C{end+1} = ' ';
end

[flag, Signal] = get_signal(this, 'pitch');
N{end+1} = 'Pitch (deg - mean)';
N{end+1} = 'Pitch (deg - std)';
if flag
    C{end+1} = num2str(mean(Signal, 'omitnan'));
    C{end+1} = num2str(std( Signal, 'omitnan'));
else
    C{end+1} = ' ';
    C{end+1} = ' ';
end

[flag, Signal] = get_signal(this, 'Inter ping distance');
N{end+1} = 'Inter ping distance (m - mean)';
if flag
    C{end+1} = num2str(mean(Signal, 'omitnan'));
else
    C{end+1} = ' ';
end

[flag, Signal] = get_signal(this, 'Datetime');
N{end+1} = 'Beginning';
N{end+1} = 'End';
if flag
    C{end+1} = char(Signal(1));
    C{end+1} = char(Signal(end));
else
    C{end+1} = ' ';
    C{end+1} = ' ';
end

kb = sizeFic(nomFic);
N{end+1} = 'File size (bytes)';
C{end+1} = num2strMoney(kb);

listeSignals = get_listeSignals(this);
for k=1:length(listeSignals)
    switch lower(listeSignals{k})
        case {'pitch'; 'roll'; 'heading'}
%             listeSignals{k}

        otherwise
            [flag, Signal, Unit] = get_signal(this, listeSignals{k});
            if ~flag
                continue
            end
            if isa(Signal, 'cl_time')
%                 Signal
            else
                if isempty(Unit)
                    N{end+1} = listeSignals{k}; %#ok<AGROW>
                else
                    N{end+1} = [listeSignals{k} '(' Unit ')']; %#ok<AGROW>
                end
                C{end+1} = num2str(mean(Signal, 'omitnan')); %#ok<AGROW>
            end
    end
end
