function [Fig, Carto] = editNav(nomFic, varargin)

[varargin, OriginNav]     = getPropertyValue(varargin, 'OriginNav',     2);
[varargin, SourceHeading] = getPropertyValue(varargin, 'SourceHeading', 2);
[varargin, Carto]         = getPropertyValue(varargin, 'Carto',         []);
[varargin, Fig]           = getPropertyValue(varargin, 'Fig',           []); %#ok<ASGLU>

%% Cr�ation de la fen�tre avec les menu et callbacks sp�cialis�s pour la nav

Fig = plot_navigation_Figure(Fig);

%% Traitement des fichiers

NbFic = length(nomFic);
str1 = 'Trac� de nav des fichiers SDF, en cours';
str2 = 'Plot nav from SDF files';
hw = create_waitbar(Lang(str1,str2), 'N', NbFic);
for k=1:NbFic
    my_waitbar(k, NbFic, hw);
    
    SDF = cl_sdf('nomFic', nomFic{k});
    if isempty(SDF)
        %         str1 = sprintf('%s n''a pas pu �tre d�cod�.', nomFic{k});
        %         str2 = sprintf('%s could not be loaded.', nomFic{k});
        %         my_warndlg(Lang(str1,str2), 0);
        continue
    end

    Immersion = get(SDF, 'depth');
    
    [flag, OrigineNavigation, SonarTime, Latitude, Longitude, ~, Heading] ...
        = read_navigation(SDF, 'OriginNav', OriginNav, 'SourceHeading', SourceHeading);
    if ~flag
        continue
    end
    Title = ['Position - ' OrigineNavigation];
    plot_navigation(nomFic{k}, Fig, Longitude, Latitude, SonarTime.timeMat, Heading, 'Title', Title);
    
    %% Cr�ation des instances ClNavigation

    SonarTime = datetime(SonarTime.timeMat, 'ConvertFrom', 'datenum');
    timeSample = XSample('name', 'time', 'data', SonarTime);
    
    latSample = YSample('data', Latitude, 'marker', '.');
    latSignal = ClSignal('ySample', latSample, 'xSample', timeSample);
   
    lonSample = YSample('data', Longitude, 'marker', '.');
	lonSignal = ClSignal('ySample', lonSample, 'xSample', timeSample);
   
    shipHeadingSample   = YSample('unit', 'deg', 'data', Heading, 'marker', '.');
    headingVesselSignal = ClSignal('ySample', shipHeadingSample, 'xSample', timeSample);
    
    identSignalCap = 3; %#ok<NASGU> 
    % R�introduit par JMA le 08/03/2023 car bug sur standalone

    immersionSample = YSample('unit', 'm', 'data', Immersion, 'single', 'marker', '.'); % R�introduit par JMA le 08/03/2023 car bug sur standalone
    immersionSignal = ClSignal('ySample', immersionSample,     'xSample', timeSample); % R�introduit par JMA le 08/03/2023 car bug sur standalone
    identSignalCap = 4;
    % Fin r�introduction par JMA le 08/03/2023 car bug sur standalone
    
    [~, name] = fileparts(nomFic{k});
    nav(k) = ClNavigation(latSignal, lonSignal, ...
        'headingVesselSignal', headingVesselSignal, ...
        'immersionSignal',     immersionSignal, ...
        'name', name); %#ok<AGROW>
%         'immersionSignal',     immersionSignal, ... % R�introduit par JMA le 08/03/2023 car bug sur standalone
end
my_close(hw, 'MsgEnd');

%% Create the GUI

a = NavigationDialog(nav, 'Title', 'Navigation', 'CleanInterpolation', true);
a.displayedSignalList = [1 1 0 1];
% a.editProperties();
pause(0.5)
a.openDialog();
if ~a.okPressedOut
    return
end

%% Trac� des nav corrig�es

for k=1:NbFic
    Title = ['Position - ' OrigineNavigation];
    SonarTime = nav(k).signalList(1).xSample.data;
    plot_navigation(nomFic{k}, Fig, ...
        nav(k).signalList(2).ySample.data, ...
        nav(k).signalList(1).ySample.data, ...
        SonarTime, ...
        nav(k).signalList(identSignalCap).ySample.data, 'Title', Title, 'ColorCurve', [0.8 0.8 0.8]);
end

%% Export des navigations

str1 = 'Voulez-vous sauvegarder cette nouvelle navigation dans le cache SSc des .all ?';
str2 = 'Do you want to save this navigation in the SSc cach directories of the .all ?';
[rep, flag] = my_questdlg(Lang(str1,str2));
if ~flag || (rep == 2)
    return
end

%% Sauvegarde des signaux corrig�s dans le r�pertoire cache de SSc

for k=1:length(nomFic)
    
    SDF = cl_sdf('nomFic', nomFic{k});
    
    switch OrigineNavigation
        case 'Ship'
            flag = save_signal(SDF, 'shipLon', nav(k).signalList(2).ySample.data);
            if ~flag
                return
            end
            
            flag = save_signal(SDF, 'shipLat', nav(k).signalList(1).ySample.data);
            if ~flag
                return
            end
            
        case 'Fish'
            flag = save_signal(SDF, 'fishLon', nav(k).signalList(2).ySample.data);
            if ~flag
                return
            end
            
            flag = save_signal(SDF, 'fishLat', nav(k).signalList(1).ySample.data);
            if ~flag
                return
            end
    end
    
    switch SourceHeading
        case 1 % Fish
            flag = save_signal(SDF, 'heading', nav(k).signalList(identSignalCap).ySample.data); % Modif JM1 le 25/11/2021
            if ~flag
                return
            end
        case 2 % Computed from Fish navigation
             %Avant modif JMA le 15/03/2023
%             flag = save_signal(SDF, 'fishNavHeading', nav(k).signalList(identSignalCap).ySample.data);

            % Modif JMA le 15/03/2023
            Latitude  = nav(k).signalList(1).ySample.data;
            Longitude = nav(k).signalList(2).ySample.data;
            Heading = calCapFromLatLon(Latitude, Longitude, 'Time', SonarTime);
            flag = save_signal(SDF, 'fishNavHeading', Heading);
            if ~flag
                return
            end
        case 3 % % Ship
            flag = save_signal(SDF, 'shipHeading', nav(k).signalList(identSignalCap).ySample.data);
            if ~flag
                return
            end
    end
end
