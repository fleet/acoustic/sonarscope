function [FormatIdVersion, flagExistSDF, flagExistSSC] = getPageVersion(nomDirRacine)

pageVersion = ['V3000'; 'V3001'; 'V5000'; 'V5001'; 'V7000'; 'V7001'];
for k=1:size(pageVersion,1)
    nomFicSDFNetcdf = [nomDirRacine, '.nc'];
    flag = exist(nomFicSDFNetcdf, 'file');
    if flag
        flag = NetcdfUtils.existGrp(nomFicSDFNetcdf, pageVersion(k,:));
        if flag
            tabFlagExistSDF(k) = 1; %#ok<AGROW>
            tabFlagExistSSC(k) = 1; %#ok<AGROW>
        else
            tabFlagExistSDF(k) = 0; %#ok<AGROW>
            tabFlagExistSSC(k) = 0; %#ok<AGROW>
        end
    else
        nomFicSDFxml = fullfile(nomDirRacine, ['SDF_' pageVersion(k,:) '.xml']);
        tabFlagExistSDF(k) = exist(char(nomFicSDFxml), 'file');   %#ok<AGROW>
        nomFicSSCxml = fullfile(nomDirRacine, ['SSC_' pageVersion(k,:) '.xml']);
        tabFlagExistSSC(k) = exist(char(nomFicSSCxml), 'file');   %#ok<AGROW>
    end
end

FormatIdVersion = '';
flagExistSSC = 1;
flagExistSDF = 1;

if ~any(tabFlagExistSSC)&& ~any(tabFlagExistSDF) 
    % Aucun SSC ni SDF n'existe.
    flagExistSSC = 0;
    flagExistSDF = 0;
else
%     % Si on a plus de un format !!!!!!
%     if sum(any(tabFlagExistSSC)) ~= 0
%         FormatIdVersion = '';
%         return
%     end

    if sum(any(tabFlagExistSDF)) ~= 0
        flagExistSDF = 1;
        ind = (tabFlagExistSDF ~= 0);
        FormatIdVersion = pageVersion(ind,:);
    end
end    
