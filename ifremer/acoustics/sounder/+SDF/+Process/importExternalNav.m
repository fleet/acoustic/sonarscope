function importExternalNav(nomFicSDF, nomFicNav)

if ~iscell(nomFicSDF)
    nomFicSDF = {nomFicSDF};
end

Carto = [];
N = length(nomFicSDF);
str1 = 'Import de la nav dans les fichiers SDF';
str2 = 'SDF navigation import';
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
    my_waitbar(k, N, hw);
    [flag, Carto] = import_external_nav_unitaire(nomFicSDF{k}, nomFicNav, Carto);
    if ~flag
        my_close(hw)
        return
    end
end
my_close(hw, 'MsgEnd')


function [flag, Carto] = import_external_nav_unitaire(nomFicSDF, nomFicNav, Carto)

%% Lecture de l'image

this = cl_sdf('nomFic', nomFicSDF);

[flag, ~, SonarTime, Latitude, Longitude] = read_navigation(this);
if ~flag
    return
end

% nbRows = length(Latitude);
T = SonarTime.timeMat;

LatitudeImage  = Latitude;
LongitudeImage = Longitude;

for k=1:length(nomFicNav)
    [flag, Latitude, Longitude, tNavBab] = lecFicNav(nomFicNav{k});
    tNavBab = tNavBab.timeMat;
    
    % TODO : on pourrait tr�s bien faire l'interpolation en fonction de
    % cl_time
    LongitudeSDF = my_interp1_longitude(tNavBab, Longitude, T);
    LatitudeSDF  = my_interp1(tNavBab, Latitude, T);
    
    subNonNaN = find(~isnan(LongitudeSDF));
    if ~isempty(subNonNaN)
        LatitudeImage(subNonNaN)  = LatitudeSDF(subNonNaN);
        LongitudeImage(subNonNaN) = LongitudeSDF(subNonNaN);
    end
end

flag = save_signal(this, 'fishLat',  LatitudeImage);
if ~flag
    return
end
    
flag = save_signal(this, 'fishLon', LongitudeImage);
if ~flag
    return
end
