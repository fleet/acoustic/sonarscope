% Trace de la navigation et de la couverture d'un fichier .all
%
% Syntax
%   plotCoverage(nomFic)
%
% Input Arguments
%   nomFic : Liste de fichiers .all
%
% Examples
%   nomDir = 'D:\NIWA_DATA\TAN0105'
%   liste = listeFicOnDir2(nomDir, '.sdf')
%   SDF.Process.plotCoverage(liste)
%
% See also cl_sdf plot_index Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function plotCoverage(nomFic, varargin)

if ~iscell(nomFic)
    nomFic = {nomFic};
end

this = cl_sdf.empty;
for k=1:length(nomFic)
    a = cl_sdf('nomFic', nomFic{k});
    if isempty(a)
        %         str1 = sprintf('%s n''a pas pu �tre d�cod�.', nomFic{k});
        %         str2 = sprintf('%s could not be loaded.', nomFic{k});
        %         my_warndlg(Lang(str1,str2), 0);
        continue
    end
    this(end+1) = a; %#ok<AGROW>
end

Ident = rand(1);

[varargin, Fig] = getPropertyValue(varargin, 'Fig', []); %#ok<ASGLU>

if isempty(Fig)
    Fig = figure('name', 'Position datagrams');
else
    figure(Fig);
end
set(Fig, 'UserData', Ident)
hold on;

nbCourbes = length(this);
ColorOrder = get(gca, 'ColorOrder');
nbCoul = size(ColorOrder,1);

hw = create_waitbar('Plot Coverage .sdf navigation', 'N', nbCourbes);
for k=1:nbCourbes
    my_waitbar(k, nbCourbes, hw);
    iCoul = mod(k-1, nbCoul) + 1;
    plot_coverage_unitaire(this(k), Fig, ColorOrder(iCoul,:), nomFic{k});
end
my_close(hw, 'MsgEnd');

% figure(Fig);
% set(Fig, 'name', FigName)
% drawnow


function plot_coverage_unitaire(this, Fig, Color, nomFic)

[flag, OrigineNavigation, tNav, Latitude, Longitude] = read_navigation(this); %#ok<ASGLU>
if ~flag
    return
end

NbPings = length(Latitude);
step = min(max(1, floor(NbPings/100)), 100);
tDepth = tNav;
T = tDepth.timeMat;

NbPingsDessines = NbPings;
% TDepth = zeros(NbPingsDessines, 1);
D_Bab  = zeros(NbPingsDessines, 1);
D_Tri  = zeros(NbPingsDessines, 1);

range           = get(this, 'range');
altitudeMeters  = get(this, 'altitudeMeters');
heading         = get(this, 'heading');

Range = sqrt(range .^ 2 - altitudeMeters(:) .^ 2);

Data_DepthAcrossDist = [-Range Range];

k = 1;
i = 1;
D = Data_DepthAcrossDist(i,:);
D_Bab(k) = D(1);
D_Tri(k) = D(2);
% TDepth(k) = T(i);
k = k+1;

for i=1:NbPings
    D = Data_DepthAcrossDist(i,:);
    iDeb = find(~isnan(D), 1, 'first');
    iFin = find(~isnan(D), 1, 'last');
    if isempty(iDeb) || isempty(iFin) || isnan(T(i))
        continue
    end
    D_Bab(k) = D(iDeb);
    D_Tri(k) = D(iFin);
    %     TDepth(k) = T(i);
    k = k+1;
end

i = NbPings;
D = Data_DepthAcrossDist(i,:);
if ~isempty(iDeb) && ~isempty(iFin)
    D_Bab(k) = D(1);
    D_Tri(k) = D(2);
    %     TDepth(k) = T(i);
end

% TDepth(k+1:end) = [];
% D_Bab(k+1:end)  = [];
% D_Tri(k+1:end)  = [];
D_Bab(NbPings+1:end)  = [];
D_Tri(NbPings+1:end)  = [];

% tDepth = cl_time('timeMat', TDepth);

% Longitude = my_interp1_longitude(tNav, Longitude, tDepth);
% Latitude  = my_interp1(tNav, Latitude,  tDepth);
Heading   = unwrap(heading(:) * (pi/180)) * (180/pi);
% Heading   = my_interp1_headingDeg(tNav, Heading, tDepth);

[Lat, Lon] = get_faucheeInLatLong(Latitude, Longitude, Heading, D_Bab, D_Tri, zeros(size(D_Bab)), zeros(size(D_Bab)));

subNan = isnan(sum(Lon, 2)) | isnan(sum(Lat,2));
Lon(subNan, :) = [];
Lat(subNan, :) = [];

if isempty(Lon)
    return
end

X1 = Lon(2:end-1,1);
Y1 = Lat(2:end-1,1);
X2 = flipud(Lon(2:end-1,2));
Y2 = flipud(Lat(2:end-1,2));

X1 = reduceByStep(X1, step);
X2 = reduceByStep(X2, step);
Y1 = reduceByStep(Y1, step);
Y2 = reduceByStep(Y2, step);

X = [Lon(1,1); X1; Lon(end,1); Lon(end,2); X2; Lon(1,2); Lon(1,1)];
Y = [Lat(1,1); Y1; Lat(end,1); Lat(end,2); Y2; Lat(1,2); Lat(1,1)];

figure(Fig)
h = patch(X, Y, Color, 'FaceAlpha', 0.5, 'Tag', nomFic);
set_HitTest_Off(h)

%% Affichage de la navigation

axisGeo;
drawnow


function y = reduceByStep(x, step)
k = 0;
y = NaN(floor(length(x) / step), 1);
for i=1:step:length(x)
    k = k + 1;
    sub = i:min(i+step-1, length(x));
    %     y(k) = median(x(sub));
    y(k) = mean(x(sub));
end

