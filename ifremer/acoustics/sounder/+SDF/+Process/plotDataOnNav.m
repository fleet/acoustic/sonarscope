% Trace de la navigation et des capteurs de vitesse et de cap d'un fichier .all
%
% Syntax
%   SDF.Process.plotDataOnNav(nomFic, Invite)
%
% Input Arguments
%   nomFic : Liste de fichiers .all
%   Invite : Inter ping distance | ...
%
% Examples
%   nomDir = 'D:\NIWA_DATA\TAN0105'
%   liste = listeFicOnDir2(nomDir, '.sdf')
%   SDF.Process.plotDataOnNav(liste, 'Inter ping distance')
%
% See also cl_sdf Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function plotDataOnNav(Fig1, nomFic, Invite, varargin)

% [varargin, Carto] = getPropertyValue(varargin, 'Carto', []);

if ~iscell(nomFic)
    nomFic = {nomFic};
end

NbFic = length(nomFic);

Ident = rand(1);

figure(Fig1);
set(Fig1, 'UserData', Ident)
hold on;

%% Boucle sur les fichiers

XLim = [Inf -Inf];
YLim = [Inf -Inf];
ZLim = [Inf -Inf];
this = cl_sdf.empty;

str1 = sprintf('Trac� du signal "%s" sur la navigation des fichiers SDF', Invite);
str2 = sprintf('Plot "%s" on navigation of SDF files', Invite);
my_warndlg(Lang(str1,str2), 0, 'Tag', 'SDF_ProcessingIsOver', 'TimeDelay', 0);
hw = create_waitbar(Lang(str1,str2), 'N', NbFic);
for k=1:NbFic
    my_waitbar(k, NbFic, hw);
    
    a = cl_sdf('nomFic', nomFic{k});
    if isempty(a)
        %         str1 = sprintf('%s n''a pas pu �tre d�cod�.', nomFic{k});
        %         str2 = sprintf('%s could not be loaded.', nomFic{k});
        %         my_warndlg(Lang(str1,str2), 0);
        continue
    end
    this(end+1) = a; %#ok<AGROW>
    
    [flag, OrigineNavigation, SonarTime, Latitude, Longitude] = read_navigation(a); %#ok<ASGLU>
    if ~flag
        continue
    end
    
    [flag, Signal, Unit, Colors] = get_signal(a, Invite);
    if ~flag
        continue
    end
    
    switch lower(Invite)
        case {'heading'; 'shipheading'; 'headingmean'}
            ZLim = [0 360];
        case 'fixtime'
            ZLim(1) = min(ZLim(1), double(min(Signal.timeMat, [], 'omitnan')));
            ZLim(2) = max(ZLim(2), double(max(Signal.timeMat, [], 'omitnan')));
        otherwise
            ZLim(1) = min(ZLim(1), double(min(Signal(:), [], 'omitnan')));
            ZLim(2) = max(ZLim(2), double(max(Signal(:), [], 'omitnan')));
    end
    XLim(1) = min(XLim(1), min(Longitude(:), [], 'omitnan'));
    XLim(2) = max(XLim(2), max(Longitude(:), [], 'omitnan'));
    YLim(1) = min(YLim(1), min(Latitude(:),  [], 'omitnan'));
    YLim(2) = max(YLim(2), max(Latitude(:),  [], 'omitnan'));
end
my_close(hw)

figure(Fig1);
if isempty(Unit)
    title(Invite)
else
    title([Invite ' (' Unit ')']);
end

%% Saisie des bornes de la table de couleurs

[flag, ZLim] = saisie_CLim(min(ZLim), max(ZLim), Unit, 'SignalName', Invite, 'hFigHisto', 87698);
if ~flag
    return
end

%%

nbColors = size(Colors,1);

figure(Fig1);
W = axis;
x = linspace(XLim(1), XLim(1)+100*nbColors*eps(XLim(1)), nbColors);
y = linspace(YLim(1), YLim(2), nbColors);
h = imagesc(x, y, linspace(ZLim(1), ZLim(2), nbColors)); colormap(Colors), colorbar %#ok<NASGU>
% delete(h)
hold on;
axis(W)

N = length(this);
hw = create_waitbar('SDF files : plotting signal', 'N', N);
for k=1:N
    my_waitbar(k, N, hw);
    plotDataOnNav_unitaire(this(k), Invite, ZLim, Fig1, nbColors);
end
my_close(hw, 'MsgEnd')
figure(Fig1);


function plotDataOnNav_unitaire(this, Invite, ZLim, Fig1, nbColors)

nomFic = get(this, 'nomFic');
figure(Fig1)

%% Lecture de la navigation et du signal

[flag, OrigineNavigation, SonarTime, Latitude, Longitude] = read_navigation(this); %#ok<ASGLU>
if ~flag
    return
end

[flag, Signal, Unit, Colors] = get_signal(this, Invite); %#ok<ASGLU>
if ~flag
    return
end

%% Trac�

% nbSounders = size(Signal,2);
% if nbSounders == 2
%     str1 = 'Ce soundeur est dual, les donn�es peuvent �tre diff�rentes � b�bord et � tribord, pour l''instant je ne trace que les donn�es du sondeur tribord.';
%     str2 = 'This sounder is this dual one, for the moment I plot only data from the port one.';
%     my_warndlg(Lang(str1,str2), 0, 'Tag', 'PlotPositionDataNbSounders=2');
nbSounders = 1;
% end
for iCote = 1:nbSounders
    figure(Fig1(iCote))
    ZData = Signal(:,iCote);
    if isa(ZData, 'cl_time')
        ZData = ZData.timeMat;
    end
    subNonNaN = find(~isnan(ZData));
    if length(subNonNaN) <= 1
        return
    end
    subNaN = find(isnan(ZData));
    if ~isempty(subNaN)
        ZData(subNaN) = interp1(subNonNaN, ZData(subNonNaN), subNaN, 'linear', 'extrap');
    end
    %     Value = interp1(Time, ZData, Time);
    Value = ZData;
    if ZLim(2) == ZLim(1)
        ZLim(1) = ZLim(1) - 0.5;
        ZLim(2) = ZLim(2) + 0.5;
    end
    coef = (nbColors-1) / (ZLim(2) - ZLim(1));
    Value = ceil((Value - ZLim(1)) * coef);
    
    % --------------------------
    % Affichage de la navigation
    
    for k=1:nbColors
        sub = find(Value == (k-1));
        if ~isempty(sub)
            h = plot(Longitude(sub), Latitude(sub), '+');
            set(h, 'Color', Colors(k,:), 'Tag', nomFic)
            set_HitTest_Off(h)
        end
    end
    
    sub = find(Value < 0);
    if ~isempty(sub)
        h = plot(Longitude(sub), Latitude(sub), '+');
        set(h, 'Color', Colors(1,:)*0.8, 'Tag', nomFic)
        set_HitTest_Off(h)
    end
    
    sub = find(Value > (nbColors-1));
    if ~isempty(sub)
        h = plot(Longitude(sub), Latitude(sub), '+');
        set(h, 'Color', Colors(nbColors,:)*0.8, 'Tag', nomFic)
    end
end

axisGeo;
grid on;
% colorbar % Supprim� le 7/11/2006 car ca remet les infos de colorbar de 0
% � 64
drawnow
