function [Fig, Carto] = plotNav(nomFic, varargin)

[varargin, OriginNav] = getPropertyValue(varargin, 'OriginNav', 3);
[varargin, Carto]     = getPropertyValue(varargin, 'Carto',     []);
[varargin, Fig]       = getPropertyValue(varargin, 'Fig',       []); %#ok<ASGLU>

%% Cr�ation de la fen�tre avec les menu et callbacks sp�cialis�s pour la nav

Fig = plot_navigation_Figure(Fig);

%% Traitement des fichiers

% strOrigineNavigation = {'Ship'; 'Fish'};
NbFic = length(nomFic);
str1 = 'Trac� de nav des fichiers SDF, en cours';
str2 = 'Plot nav from SDF files';
hw = create_waitbar(Lang(str1,str2), 'N', NbFic);
for k=1:NbFic
    my_waitbar(k, NbFic, hw);
    
    a = cl_sdf('nomFic', nomFic{k});
    if isempty(a)
        %         str1 = sprintf('%s n''a pas pu �tre d�cod�.', nomFic{k});
        %         str2 = sprintf('%s could not be loaded.', nomFic{k});
        %         my_warndlg(Lang(str1,str2), 0);
        continue
    end
    
    [flag, OrigineNavigation, SonarTime, Latitude, Longitude, ~, Heading] = read_navigation(a, 'OriginNav', OriginNav);
    if ~flag
        continue
    end
    Title = ['Position - ' OrigineNavigation];
    plot_navigation(nomFic{k}, Fig, Longitude, Latitude, SonarTime.timeMat, Heading, 'Title', Title);
end
my_close(hw, 'MsgEnd');

