function listeSignals = plotSignals(nomFicSDF, listeSignals, typeAxeX)

color = 'krbgm';
nbColors = length(color);

N = length(listeSignals);
Fig = FigUtils.createSScFigure;
set(Fig, 'Name', ' Klein signals')
X = 0;

NbFic = length(nomFicSDF);
str1 = 'Trac� des signaux des fichiers SDF, en cours';
str2 = 'Plot signals from SDF files';
hw = create_waitbar(Lang(str1,str2), 'N', NbFic);
for iFic=1:NbFic
    my_waitbar(iFic, NbFic, hw);

    a = cl_sdf('nomFic', nomFicSDF{iFic});
    if isempty(a)
%         str1 = sprintf('%s n''a pas pu �tre d�cod�.', nomFicSDF{iFic});
%         str2 = sprintf('%s could not be loaded.', nomFicSDF{iFic});
%         my_warndlg(Lang(str1,str2), 0);
        continue
    end
    
%     XML = [];
%     PageVersion = a.PageVersion;
    for k=1:N
        [flag, Y, Unit] = get_signal(a, listeSignals{k});
        if ~flag
            continue
        end

        if isempty(Unit)
            Titre = listeSignals{k};
        else
            Titre = [listeSignals{k} ' (' Unit ')'];
        end
        if isa(Y, 'cl_time')
            continue
        end
        
        figure(Fig);
        h(k) = subplot(N, 1, k); %#ok<AGROW>
        
        switch typeAxeX
            case 1
%                 [flag, X, ~, XML] = SScCacheXMLBinUtils.getSignalUnit(nomFicSDF{iFic}, ['Ssc_' PageVersion], 'Time', 'XML', XML);
                [flag, X] = get_signal(a, 'Time'); % Modif JMA le 09/12/2020
                if ~flag
                    return
                end
                X = datetime(X, 'ConvertFrom', 'datenum');
            case 2
                if k==1
                    X = X(end) + (1:length(Y)); 
                end
        end
        iColor = mod(iFic, nbColors);
%         [~, Name, Ext] = fileparts(nomFicSDF{iFic});
        PlotUtils.createSScPlot(X, Y, color(iColor + 1)); grid on; title(Titre); hold on; % 'Name', [Name Ext]
    end
end
linkaxes(h, 'x')
my_close(hw, 'MsgEnd');
