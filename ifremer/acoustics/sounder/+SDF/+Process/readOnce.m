function readOnce(nomFic)

global useCacheNetcdf %#ok<GVMIS>

useParallel = get_UseParallelTbx;
useCacheNetcdfHere = useCacheNetcdf;

if ~iscell(nomFic)
    nomFic = {nomFic};
end

NbFic = length(nomFic);
str1 = 'Prétraitement des fichiers SDF';
str2 = 'Preprocessings SDF files';
if useParallel && (NbFic > 1)
    [hw, DQ] = create_parforWaitbar(NbFic, Lang(str1,str2));
    parfor (iFic=1:NbFic, useParallel)
        fprintf('%d/%d : %s\n', iFic, NbFic, nomFic{iFic});
        cl_sdf('nomFic', nomFic{iFic}, 'isUsingParfor', true, 'useCacheNetcdf', useCacheNetcdfHere);
        send(DQ, iFic);
    end
    my_close(hw, 'MsgEnd')
else
    hw = create_waitbar(Lang(str1,str2), 'N', NbFic);
    for iFic=1:NbFic
        my_waitbar(iFic, NbFic, hw);
        fprintf('%d/%d : %s\n', iFic, NbFic, nomFic{iFic});
        cl_sdf('nomFic', nomFic{iFic});
    end
    my_close(hw, 'MsgEnd')
end
