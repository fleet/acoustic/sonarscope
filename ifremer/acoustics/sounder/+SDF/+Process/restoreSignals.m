function flag = restoreSignals(nomFic, nomSignals, backupFileName)

if ~iscell(nomFic)
    nomFic = {nomFic};
end

if ~iscell(nomSignals)
    nomSignals = {nomSignals};
end

N = length(nomFic);
str1 = 'Restauration des signaux SDF';
str2 = 'Resore SDF signals';
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    flag = RestoreSignals_unitaire(nomFic{k}, nomSignals, backupFileName);
    if ~flag
        continue
    end
end
my_close(hw, 'MsgEnd')


function flag = RestoreSignals_unitaire(nomFic, nomSignals, backupFileName)

flag = 0;

this = cl_sdf('nomFic', nomFic);
if isempty(this)
    return
end

% PageVersion = this.PageVersion;

[~, grpName] = fileparts(nomFic);
Data = NetcdfUtils.Netcdf2Struct(backupFileName, grpName);

for k=1:length(nomSignals)
    nomSignal = nomSignals{k};
%     [~, pppp] = SScCacheNetcdfUtils.getSignalUnit(nomFic, PageVersion, nomSignal);
    X = Data.(nomSignal);
    %     switch nomSignal
    %         case 'altitudeMeters'
    %             altitudeMeters = getHeightInMeters(this, SonarHeightInAmples);
    %         otherwise
    flag = save_signal(this, nomSignal, X);
    if ~flag
        pi;%JMA
    end
    %     end
end

% Signal = getHeightInSamples(this)
% altitudeMeters = getHeightInMeters(this, SonarHeightInAmples)

flag = 1;
