function [repImport, repExport] = splitSdfFile(nomFicSDF, NomFicPoints, repImport, repExport)

[flag, NbPings, SplitTime] = get_sonarTimeFromLogFile(NomFicPoints, repImport, repExport);
if ~flag
    return
end

nomFicListe = fullfile(repExport, 'ListSplitFiles.txt');
str1 = 'Nom du fichier donnant la liste des nouveaux fichiers .SDF sectionnés';
str2 = 'Give the name to the file that will collect the full names of the splitted .SDF files';
[flag, nomFicListe] = my_uiputfile('*.txt', Lang(str1,str2), nomFicListe);
if ~flag
    return
end
repExport = fileparts(nomFicListe);

if ~iscell(nomFicSDF)
    nomFicSDF = {nomFicSDF};
end

fid = fopen(nomFicListe, 'w+');
str1 = 'Saucissonnage des fichiers .SDF';
str2 = 'Splitting .SDF files';
N = length(nomFicSDF);
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k1=1:N
    a = cl_sdf('nomFic', nomFicSDF{k1});
    % Pour l'instant, on ne traite que les formats 3001
    PageVersion = a.PageVersion;
    if strcmp(PageVersion, 'V3001')
        selectDatagram = {a.nbPings, 3001, 'V3001', 1};
    else
        str1 = 'On ne peut splitter pour l''instant que les SDF de Page Version = 3001';
        str2 = 'Only files with Page Version = 3001 could be spliited';
        my_warndlg(Lang(str1,str2), 1);
        return
    end
    % selectDatagram  = selectDatagramFromFile(a);
    nomFicOut = saucissonne_bin(a, NbPings, SplitTime, selectDatagram);
    for k2=1:length(nomFicOut)
        fprintf(fid, '%s\n', nomFicOut{k2});
    end
    my_waitbar(k1, N, hw);
end
my_close(hw, 'MsgEnd')
fclose(fid);
