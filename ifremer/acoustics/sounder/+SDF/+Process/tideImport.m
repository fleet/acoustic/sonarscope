function tideImport(ListFic, NomFicTide)

if ~iscell(ListFic)
    ListFic = {ListFic};
end

%% Lecture de la mar�e

[flag, Tide] = read_Tide(NomFicTide); %#ok<ASGLU>
if ~flag
    return
end

%% Import de la mar�e dans les fichiers .all

nbFic = length(ListFic);
str1 = 'Importation de la mar�e dans les .sdf.';
str2 = 'Tide Import in the .sdf files.';
hw = create_waitbar(Lang(str1,str2), 'N', nbFic);
for k=1:nbFic
    my_waitbar(k, nbFic, hw);
    a = cl_sdf('nomFic', ListFic{k});
    
     % TODO : � terminer
    
     %{
    [flag, Data, nomFicXml] = read_position_bin(a, 'Memmapfile', 0);
    if ~flag
        break
    end
    
    X = my_interp1(Tide.Time, Tide.Value, Data.Time);

    flag = save_signal(a.nomFic, 'Ssc_Position', 'Tide', X);
    if ~flag
        break
    end
     %}
end
my_close(hw, 'MsgEnd')
