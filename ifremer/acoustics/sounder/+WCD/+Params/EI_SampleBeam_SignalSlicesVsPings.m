function [flag, listFileNames, DataName, TypeUnit, CorrectionTVG, Quantile1, Quantile2, ALim, BLim, params_ZoneEI, flagComp, ...
    nomDirCSVOut, Tag, maskOverlapingBeams, CLim, repImport] ...
    = EI_SampleBeam_SignalSlicesVsPings(repImport, varargin)

persistent persistent_BLim

flagComp      = 0;
BLim          = [];
params_ZoneEI = [];

%% M�me question que WC_PlotNav_EI_SSc

[flag, listFileNames, DataName, TypeUnit, CorrectionTVG, Quantile1, Quantile2, CLim, ALim, nomDirCSVOut, Tag, ...
    maskOverlapingBeams, repImport] = params_SimradAll_WC_PlotNav_EI_SSc(repImport, 'AskQuestion', 0, varargin{:});
if ~flag
    return
end

%% Masquage par les angles ?

str1 = 'Voulez-vous limiter le calcul du signal entre 2 num�ros de faisceau ?';
str2 = 'Do you want to limitate the signals computation between 2 Beam Numbers ?';
[rep, flag] = my_questdlg(Lang(str1,str2), 'Init', 2);
if ~flag
    return
end
if rep == 1
    if isempty(persistent_BLim)
        BLim = [1 Inf];
    else
        BLim = persistent_BLim;
    end
    [flag, BLim] = saisie_CLim(BLim(1), BLim(2), '#', 'SignalName', 'Beam numbers');
    if ~flag
        return
    end
    persistent_BLim = BLim;
end

%% Saisie des seuils haut et bas

[flag, params_ZoneEI] = WCD.Params.EchoIntegrationSlices('OverSpecular', 1, 'QuestionDisplay', 0);
if ~flag
    return
end

%% Traitement de �chogrammes compens�s

 [ind, flag] = my_questdlg('Process compensated echograms ?', 'Init', 2);
 if ~flag
     return
 end
flagComp = (ind == 1);
