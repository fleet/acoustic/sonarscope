function [flag, params_ZoneEI, Display] = EchoIntegration(varargin)

persistent persistent_SeuilHaut persistent_SeuilBas persistent_DistanceSecutity persistent_DistHaut persistent_DistBas

[varargin, QuestionDisplay] = getPropertyValue(varargin, 'QuestionDisplay', 1);
[varargin, OverSpecular]    = getPropertyValue(varargin, 'OverSpecular',    1); %#ok<ASGLU>

params_ZoneEI = [];
Display       = [];

%% Visualisation intermédiaire de l'échogramme

if QuestionDisplay
    str1 = 'Visualisation de l''écho-intégration';
    str2 = 'Echo Integration display (setting 0 will give access to parallel computing)';
    [flag, Display] = WCD.Params.question_VisuEchograms('Title', Lang(str1,str2));
    if ~flag
        return
    end
end

%% Saisie des paramètres d'intégration

% TODO : décomposer le truc en deux parties, utiliser
% WCD.Params.question_VisuEchograms qui donne l'info pour la // Tbx

if isempty(persistent_SeuilHaut)
    SeuilHaut = 85;
    SeuilBas  = 96;
    DistHaut  = 1;
    DistBas   = 0;
else
    SeuilHaut = persistent_SeuilHaut;
    SeuilBas  = persistent_SeuilBas;
    DistHaut  = persistent_DistHaut;
    DistBas   = persistent_DistBas;
end

if isempty(persistent_DistanceSecutity)
    DistanceSecutity = 0;
else
    DistanceSecutity = persistent_DistanceSecutity;
end

if OverSpecular
    strMin = Lang('Rayon du cercle supérieur / spéculaire', 'Radius of the upper circle / specular');
    strMax = Lang('Rayon du cercle inférieur / spéculaire', 'Radius of the lower circle / specular');
    Unit = '%';
    params_ZoneEI.Where = 'ASC';
else
    str    = {'Percentages of height over the seafloor'};
    str{2} = 'Distances over the seafloor';
    [rep, flag] = my_listdlg('Type of window over the seafloor', str, 'SelectionMode', 'Single');
    if ~flag
        return
    end
    if rep == 1
        strMin = Lang('Pourcentage de la limite supérieure', 'Percentatge of height of the upper limit');
        strMax = Lang('Pourcentage de la limite inférieure', 'Percentatge of height of the lower limit');
        Unit = '%';
        params_ZoneEI.Where = 'ASF-Height';
    else
        strMin = Lang('Distance de la limite supérieure', 'Distance of the upper limit over the seabed');
        strMax = Lang('Distance de la limite inférieure', 'Distance of the lower limit over the seabed');
        Unit = 'm';
        params_ZoneEI.Where = 'ASF-Distance';
        SeuilHaut = DistHaut;
        SeuilBas  = DistBas;
    end
end

p    = ClParametre('Name', strMin, 'Unit', Unit, 'MinValue', 0, 'MaxValue', 6000, 'Value', SeuilHaut);
p(2) = ClParametre('Name', strMax, 'Unit', Unit, 'MinValue', 0, 'MaxValue', 6000, 'Value', SeuilBas);
if nargout == 5
    p(3) = ClParametre('Name', Lang('Distance de sécurité','Safety margin'), ...
    'Unit', 'm',  'MinValue', 0, 'MaxValue', 100, 'Value', DistanceSecutity);
end
str1 = 'Paramètres d''intégration.';
str2 = 'Select the parameters to define the echo integration layer.';
a = StyledParametreDialog('params', p, 'Title', Lang(str1,str2));
% a.Help = 'http://flotte.ifremer.fr/fleet/Presentation-of-the-fleet/On-board-software/SonarScope';
a.sizes = [0 -3 0 -2 0 -1 -1 0];
a.openDialog;
flag = a.okPressedOut;
if ~flag
    return
end
val = a.getParamsValue;

if strcmp(params_ZoneEI.Where, 'ASF-Distance')
    DistHaut = max(val(1:2));
    DistBas  = min(val(1:2));
    persistent_DistHaut = DistHaut;
    persistent_DistBas  = DistBas;
else
    SeuilHaut = min(val(1:2)) / 100;
    SeuilBas  = max(val(1:2)) / 100;
    persistent_SeuilHaut = SeuilHaut * 100;
    persistent_SeuilBas  = SeuilBas * 100;
    
    if nargout == 5
        DistanceSecutity = val(3);
        persistent_DistanceSecutity = DistanceSecutity;
    end
end

params_ZoneEI.SeuilHaut        = SeuilHaut;
params_ZoneEI.SeuilBas         = SeuilBas;
params_ZoneEI.DistanceSecutity = DistanceSecutity;
params_ZoneEI.DistHaut         = DistHaut;
params_ZoneEI.DistBas          = DistBas;
