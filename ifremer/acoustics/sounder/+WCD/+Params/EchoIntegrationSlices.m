function [flag, params_ZoneEI, Display] = EchoIntegrationSlices(varargin)

persistent persistent_Seuils persistent_DistanceSecutity persistent_Dists

[varargin, QuestionDisplay] = getPropertyValue(varargin, 'QuestionDisplay', 1);
[varargin, OverSpecular]    = getPropertyValue(varargin, 'OverSpecular',    1); %#ok<ASGLU>

params_ZoneEI = [];
Display       = [];

%% Visualisation intermédiaire de l'échogramme

if QuestionDisplay
    str1 = 'Visualisation de l''écho-intégration';
    str2 = 'Echo Integration display (setting 0 will give access to parallel computing)';
    [flag, Display] = WCD.Params.question_VisuEchograms('Title', Lang(str1,str2), 'StepDisplay', 0);
    if ~flag
        return
    end
end

%% Saisie des paramètres d'intégration

% TODO : décomposer le truc en deux parties, utiliser
% WCD.Params.question_VisuEchograms qui donne l'info pour la // Tbx

if isempty(persistent_Seuils)
    Seuils  = [96 85];
    Dists   = [0 0];
else
    Seuils = persistent_Seuils;
    Dists  = persistent_Dists;
end

if isempty(persistent_DistanceSecutity)
    DistanceSecutity = 0;
else
    DistanceSecutity = persistent_DistanceSecutity;
end

if OverSpecular
    params_ZoneEI.Where = 'ASC';
else
    str    = {'Percentages of height over the seafloor'};
    str{2} = 'Distances over the seafloor';
    [rep, flag] = my_listdlg('Type of window over the seafloor', str, 'SelectionMode', 'Single');
    if ~flag
        return
    end
    if rep == 1
        params_ZoneEI.Where = 'ASF-Height';
    else
        params_ZoneEI.Where = 'ASF-Distance';
        Seuils  = Dists;
    end
end

%% Saisie des limites de slices

flag = 0;
while ~flag
    nomVar = {'Select the parameters to define the echo integration layers (Ex [30 60 80 95] or 25:10:95'};
    val = {'25:10:95'};
    [val, flag] = my_inputdlg(nomVar, val);
    if ~flag
        return
    end
    val = evalin('caller', val{1});
    str = '';
    for k=1:length(val)
        str = sprintf('%s %d', str, val(k));
    end
    str = sprintf('Matlab interpretated your string as "%s", is it correct ? ', str);
    [ind, flag] = my_questdlg(str);
    if ind == 2
        flag = 0;
    end
end

if nargout == 5
    p = ClParametre('Name', Lang('Distance de sécurité','Safety margin'), ...
        'Unit', 'm',  'MinValue', 0, 'MaxValue', 100, 'Value', DistanceSecutity);
    str1 = 'Paramètres d''intégration.';
    str2 = 'Select the parameters to define the echo integration layer.';
    a = StyledParametreDialog('params', p, 'Title', Lang(str1,str2));
    % a.Help = 'http://flotte.ifremer.fr/fleet/Presentation-of-the-fleet/On-board-software/SonarScope';
    a.sizes = [0 -3 0 -2 0 -1 -1 0];
    a.openDialog;
    flag = a.okPressedOut;
    if ~flag
        return
    end
    DistanceSecutity = a.getParamsValue;
    persistent_DistanceSecutity = DistanceSecutity;
end

if strcmp(params_ZoneEI.Where, 'ASF-Distance')
    Dists  = val;
    persistent_Dists  = Dists;
else
    Seuils = val / 100;
    persistent_Seuils  = Seuils * 100;
end

params_ZoneEI.Seuils           = Seuils;
params_ZoneEI.DistanceSecutity = DistanceSecutity;
params_ZoneEI.Dists            = Dists;
