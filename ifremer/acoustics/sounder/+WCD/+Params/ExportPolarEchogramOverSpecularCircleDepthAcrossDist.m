function [flag, params_ZoneEI, Display, ALim, configExport] = ExportPolarEchogramOverSpecularCircleDepthAcrossDist(varargin)

persistent persistent_ALim

[varargin, QuestionDisplay] = getPropertyValue(varargin, 'QuestionDisplay', 1); %#ok<ASGLU>

ALim         = [];
configExport = [];

%% Anneau

[flag, params_ZoneEI, Display] = WCD.Params.EchoIntegration('OverSpecular', 1, 'QuestionDisplay', QuestionDisplay);
if ~flag
    return
end

%% Secteur angulaire

if isempty(persistent_ALim)
    ALim = [-40 40];
else
    ALim = persistent_ALim;
end
[flag, ALim] = saisie_CLim(ALim(1), ALim(2), 'deg', 'SignalName', 'Angles');
if ~flag
    return
end
persistent_ALim = ALim;

%% Choix du type d'export

str   = {'Ping Number; Time; Across Distance (m); Depth (m); Value (dB); Lat (deg); Lon (deg)'};
str{2} = 'Ping Number; Time; Across Distance (m); Depth (m); Value (dB); x (m); y (m)';
str{3} = 'Depth (m); Value (dB); Lat (deg); Lon (deg)';
str{4} = 'Depth (m); Value (dB); x (m); y (m)';
[configExport, flag] = my_listdlg('Output parameters', str, 'SelectionMode', 'Single');
if ~flag
    return
end
 