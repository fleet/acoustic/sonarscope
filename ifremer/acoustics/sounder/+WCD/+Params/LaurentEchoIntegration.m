function [flag, repImport, repExport, FileList, Resol, Window, Slice, Backup, nomFicErMapperLayer, nomFicErMapperAngle] = LaurentEchoIntegration(repImport, repExport)

persistent persistentResol  persistentBackup persistentSlice
% persistent persistentWindow 

E0 = cl_ermapper([]);

Resol  = persistentResol;
Window = [];
Slice  = persistentSlice;
% Window = persistentWindow;
Backup = persistentBackup;
nomFicErMapperLayer = [];
nomFicErMapperAngle = [];

%% Liste des fichiers .mat

[flag, FileList, repImport] = uiSelectFiles('ExtensionFiles', '.mat', ...
    'RepDefaut', repImport);
if ~flag
    return
end

nbProfils = length(FileList);
if isempty(persistentSlice)
    Slice = 1;
else
    Slice = persistentSlice;
end
if isempty(persistentResol)
    Resol = 50;
else
    Resol = persistentResol;
end
if isempty(persistentBackup)
    Backup = nbProfils;
else
    Backup = persistentBackup;
end

str1 = 'Paramètres d''échointégration de la WC';
str2 = 'WE echointegration parameters';
p    = ClParametre('Name', Lang('Numéro de la couche', 'Slice #'), ...
    'Value', Slice, 'MinValue', 1, 'MaxValue', 12);
p(2) = ClParametre('Name', Lang('Pas de grille', 'Grid size'), ...
    'Value', Resol, 'MinValue', 0.1, 'MaxValue', nbProfils, 'Unit', 'm');
p(3) = ClParametre('Name', Lang('Fréquence de sauvegarde', 'Backup frequency'), ...
    'Value', Backup, 'MinValue', 1, 'MaxValue', 12, 'Unit', 'Files');
a = StyledSimpleParametreDialog('params', p, 'Title', Lang(str1,str2));
a.openDialog;
flag = a.okPressedOut;
if ~flag
    return
end
val = a.getParamsValue;
Slice  = val(1);
Resol  = val(2);
Backup = val(3);

persistentSlice  = Slice;
persistentResol  = Resol;
persistentBackup = Backup;

% }

%% Interpolation Window

Window = [5 5];
% if isempty(Window)
%     Window = [5 5];
% end
% [flag, Window] = saisie_window(Window);
% if ~flag
%     return
% end
% persistentWindow = Window;

%% Nom des fichiers de sauvegarde

DataType = cl_image.indDataType('Reflectivity');
NomGenerique = sprintf('Mosaic_%02d', Slice);
nomDirExport = repExport;
[nomFicErMapperLayer, flag] = initNomFicErs(E0, NomGenerique, cl_image.indGeometryType('LatLong'), ...
    DataType, nomDirExport, 'resol', Resol);
if ~flag
    return
end
[nomDirExport, TitreDepth] = fileparts(nomFicErMapperLayer);
DataType = cl_image.indDataType('TxAngle');
[nomFicErMapperAngle, flag] = initNomFicErs(E0, TitreDepth, cl_image.indGeometryType('LatLong'), ...
    DataType, nomDirExport, 'confirm', (get_LevelQuestion >= 3));
if ~flag
    return
end
repExport = fileparts(nomFicErMapperAngle);

if exist(nomFicErMapperLayer, 'file') && exist(nomFicErMapperAngle, 'file')
    [flag, rep] = question_CompleteExistingMosaic;
    if ~flag
        return
    end
    if rep == 2
        try
            delete(nomFicErMapperLayer)
        catch %#ok<CTCH>
        end
        try
            delete(nomFicErMapperAngle)
        catch %#ok<CTCH>
        end
    end
end
