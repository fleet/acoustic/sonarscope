function [flag, listFileNames, DataName, TypeUnit, CorrectionTVG, Quantile1, Quantile2, CLim, ALim, BLim, params_ZoneEI, Display, ...
    nomDirCSVOut, Tag, maskOverlapingBeams, thresholdValueRaw, thresholdValueComp, repImport] ...
    = PlotNav_EI_SScSampleBeam(repImport, varargin) 

persistent persistent_BLim

Display            = [];
BLim               = [];
thresholdValueRaw  = [];
thresholdValueComp = [];
params_ZoneEI      = [];

%% M�me question que WC_PlotNav_EI_SSc

[flag, listFileNames, DataName, TypeUnit, CorrectionTVG, Quantile1, Quantile2, CLim, ALim, nomDirCSVOut, Tag, ...
    maskOverlapingBeams, repImport] = params_SimradAll_WC_PlotNav_EI_SSc(repImport, 'AskQuestion', 0, varargin{:});
if ~flag
    return
end

%% Masquage par les angles ?

str1 = 'Voulez-vous limiter le calcul du signal entre 2 num�ros de faisceau ?';
str2 = 'Do you want to limitate the signals computation between 2 Beam Numbers ?';
[rep, flag] = my_questdlg(Lang(str1,str2), 'Init', 2);
if ~flag
    return
end
if rep == 1
    if isempty(persistent_BLim)
        BLim = [1 Inf];
    else
        BLim = persistent_BLim;
    end
    [flag, BLim] = saisie_CLim(BLim(1), BLim(2), '#', 'SignalName', 'Beam numbers');
    if ~flag
        return
    end
    persistent_BLim = BLim;
end

%% Saisie des seuils haut et bas

[flag, params_ZoneEI, Display] = WCD.Params.EchoIntegration('OverSpecular', 1);
if ~flag
    return
end

%% Saisie des seuils au del� desquels on cr�e une figure illustrant le ping de WC dont la valeur est la plus �lev�e

[flag, thresholdValueRaw, thresholdValueComp] = saisie_tresholdDisplaWCEchogramEI('thresholdValueRaw', -Inf, 'thresholdValueComp', -Inf);
if ~flag
    return
end
