function [flag, StepDisplay] = question_VisuEchograms(varargin)

persistent persistent_StepDisplay

if isempty(persistent_StepDisplay)
    persistent_StepDisplay = 20;
end

[varargin, Title]       = getPropertyValue(varargin, 'Title',       []);
[varargin, StepDisplay] = getPropertyValue(varargin, 'StepDisplay', persistent_StepDisplay); %#ok<ASGLU>

if isempty(Title)
    str1 = 'Visualisation des échogrammes polaires';
    str2 = 'Polar echograms display (setting 0 will give access to parallel computing)';
    Title = Lang(str1,str2);
end

str1 = 'Récurrence (0 = pas de visu)';
str2 = 'Recurrence (0 = no plot)';
p = ClParametre('Name', Lang(str1,str2), 'Unit', 'Pings',  'MinValue', 0, 'MaxValue', 1000, 'Value', StepDisplay, 'Format', '%d');
a = StyledParametreDialog('params', p, 'Title', Title);
% a.Help = 'http://flotte.ifremer.fr/fleet/Presentation-of-the-fleet/On-board-software/SonarScope';
a.sizes = [0 -2 0 -2 0 -1 -1 0];
a.openDialog;
flag = a.okPressedOut;
if ~flag
    return
end
StepDisplay = a.getParamsValue;

persistent_StepDisplay = StepDisplay;
 