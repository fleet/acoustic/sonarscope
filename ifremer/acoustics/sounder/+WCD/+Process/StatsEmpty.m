function s = StatsEmpty(Latitude, Longitude)

s.PingCounter    = NaN;
s.NbSamples      = 0;
s.Max            = NaN;
s.Min            = NaN;
s.Range          = NaN;
s.Mean           = NaN;
s.Median         = NaN;
s.Std            = NaN;
s.Quantile1      = NaN;
s.Quantile2      = NaN;
s.QuantilesRange = NaN;
s.Speed          = NaN;
s.Acceleration   = NaN;
s.Heading        = NaN;

s.Depth          = NaN;
s.Bathy          = NaN;

s.Latitude       = Latitude;
s.Longitude      = Longitude;
