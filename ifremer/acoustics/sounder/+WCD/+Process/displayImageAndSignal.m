function [flag, Fig, DisplayImage] = displayImageAndSignal(A, Value, k, DataName, nomFic, Tag, params_ZoneEI, ALim, BLim, CorrectionTVG, ...
    TypeUnit, maskOverlapingBeams, DataDepth, DataRaw, DataWC, DataSeabed, DataInstalParam, DataRuntime, DataSSP, DataAttitude, subDepth)

persistent persistent_Fig

DisplayImage = [];

SeuilBas  = params_ZoneEI.SeuilBas;
SeuilHaut = params_ZoneEI.SeuilHaut;

[~, ~, PatchDepthAcrossDist] = getPatchEchoIntegrationOverSpecular(A, params_ZoneEI, ALim, BLim);
% SonarDescription = get(A, 'SonarDescription');
% Duration = SonarDescription.Signal.Duration;

%% Plot de l'image SampleBeam moyennée

[~, ~, Ext] = fileparts(nomFic);
switch Ext
    case {'.all'; '.kmall'}
        aKM = cl_simrad_all('nomFic', nomFic);
        [flag, C, subDepth] = import_SummedWaterColumn_All(aKM, Tag, 'AngleLim', ALim, 'CorrectionTVG', CorrectionTVG, 'TypeMeanUnit', TypeUnit, ...
            'maskOverlapingBeams', maskOverlapingBeams, ...
            'DataDepth', DataDepth, 'DataRaw', DataRaw, 'DataSeabed', DataSeabed, 'DataWC', DataWC, 'DataInstalParam', DataInstalParam, ...
            'DataRuntime', DataRuntime, 'DataSSP', DataSSP, 'DataAttitude', DataAttitude, 'subDepth', subDepth);
        if ~flag
            return
        end
    case '.s7k'
        a = cl_reson_s7k('nomFic', nomFic);
        [flag, C] = import_SummedWaterColumn_S7K(a);
        subDepth = 1:C.nbRows;
    otherwise
        return
end
% [varargin, MaskFlaggedPings]    = getPropertyValue(varargin, 'MaskFlaggedPings',    1);
% [varargin, maskOverlapingBeams] = getPropertyValue(varargin, 'maskOverlapingBeams', 0); %#ok<ASGLU>

[~, filename, ext] = fileparts(nomFic);
FigureName = ['Echo integration per ping - ' Tag ' - ' filename ext];
if isempty(persistent_Fig) || ~ishandle(persistent_Fig)
    Fig = FigUtils.createSScFigure('name', FigureName, 'Position', centrageFig(1000, 900));
    persistent_Fig = Fig;
else
    Fig = FigUtils.createSScFigure('name', FigureName, 'Position', persistent_Fig.Position);
end
setappdata(Fig, 'FigureNameForSurveyReport', FigureName);

if flag
    ImageName = C.Name;
    Height = abs(get(C, 'Height'));
    Fe = get(C, 'SampleFrequency');
    y = C.x;
    stats = C.StatValues;
    D = get_Image(C);
    coef = max(Fe) ./ Fe;
    D = dilateImage(D, coef);
    D = D';
    CLim = stats.Quant_01_99;
    if (CLim(1) == CLim(2)) || isnan(CLim(1))
        flag = 0;
        return
    end
    x = 1:size(D,2);

    if ~isempty(ALim)
        ImageName = sprintf('%s - Angular sector [%d %d] deg', ImageName, ALim(1), ALim(2));
    end
    
    subFlag = ~isfinite(Value);
    D(:,subFlag) = NaN;
    DisplayImage.CData      = D;
    DisplayImage.CLim       = CLim;
    DisplayImage.x          = x;
    DisplayImage.y          = y;
    DisplayImage.HeightBas  = (Height .* coef) * SeuilBas;
    DisplayImage.HeightHaut = (Height .* coef) * SeuilHaut;
    DisplayImage.Name       = ImageName;
    
    h(1) = subplot(3,1,1);
    imagesc(DisplayImage.x, DisplayImage.y, DisplayImage.CData, DisplayImage.CLim); colormap(jet(256)); colorbar;
    hold on; xlabel('Pings #'); ylabel('Samples #');
    plot(DisplayImage.HeightBas, 'r');
    plot(DisplayImage.HeightHaut, 'r');
    title(DisplayImage.Name, 'Interpreter', 'None')
end

%% Plot du signal

h(2) = subplot(3,1,2);
PlotUtils.createSScPlot(x, Value(subDepth), '.-'); grid on;
hold on;
PlotUtils.createSScPlot([x(k) x(k)], [min(Value) max(Value)], 'r');
hold off; axis tight; hc = colorbar; hc.Visible = 'Off';
[~, Title, ext] = fileparts(nomFic);
xlabel('Ping #'); ylabel(DataName);
Title = sprintf('Echo Integration %s-%s : %d', Tag, [Title, ext], k);
title(Title, 'Interpreter', 'None')

linkaxes(h, 'x');

%% Display image transformed in DepthAcrossDist geometry

% [~, B] = sonar_polarEchogramPlus(A, ProfilCelerite, 1, 'DisplayDetection', 0, 'typeAlgoWC', 2);

%% Display image maintenant

if ~isempty(PatchDepthAcrossDist)
    h(3) = subplot(3,1,3);
    pcolor(A, h(3));
    hold on; plot(PatchDepthAcrossDist.XPatch, PatchDepthAcrossDist.YPatch, 'r'); hold off;
    CLim = A.CLim;
    set(h(3), 'CLim', CLim);
    xlabel('Across Dist (m)'); ylabel('Depth (m)');
    title(A.Name, 'Interpreter', 'None')
end
