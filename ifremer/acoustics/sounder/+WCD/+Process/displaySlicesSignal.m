function [flag, Fig, DisplayImage] = displaySlicesSignal(Value, DataName, nomFic, Tag, params_ZoneEI, ALim, CorrectionTVG, ...
    TypeUnit, maskOverlapingBeams, DataDepth, DataRaw, DataWC, DataSeabed, DataInstalParam, DataRuntime, DataSSP, DataAttitude, ...
    subDepth, varargin)

[varargin, CLim] = getPropertyValue(varargin, 'CLim', []); %#ok<ASGLU>

persistent persistent_Fig

DisplayImage = [];

%% Plot de l'image SampleBeam moyennée

[~, ~, Ext] = fileparts(nomFic);
switch Ext
    case {'.all'; '.kmall'}
        aKM = cl_simrad_all('nomFic', nomFic);
        [flag, C, sublDepth] = import_SummedWaterColumn_All(aKM, Tag, 'AngleLim', ALim, 'CorrectionTVG', CorrectionTVG, 'TypeMeanUnit', TypeUnit, ...
            'maskOverlapingBeams', maskOverlapingBeams, ...
            'DataDepth', DataDepth, 'DataRaw', DataRaw, 'DataSeabed', DataSeabed, 'DataWC', DataWC, 'DataInstalParam', DataInstalParam, ...
            'DataRuntime', DataRuntime, 'DataSSP', DataSSP, 'DataAttitude', DataAttitude, 'subDepth', subDepth);
        if ~flag
            return
        end
    case '.s7k'
        a = cl_reson_s7k('nomFic', nomFic);
        [flag, C] = import_SummedWaterColumn_S7K(a);
        sublDepth = 1:get_nbRows(C);
    otherwise
        return
end

[~, filename, ext] = fileparts(nomFic);
FigureName = ['Echo integration per ping - ' Tag ' - ' filename ext];
if isempty(persistent_Fig) || ~ishandle(persistent_Fig)
    Fig = FigUtils.createSScFigure('name', FigureName, 'Position', centrageFig(1000, 900));
    persistent_Fig = Fig;
else
    Fig = FigUtils.createSScFigure('name', FigureName, 'Position', persistent_Fig.Position);
end
setappdata(Fig, 'FigureNameForSurveyReport', FigureName);

if flag
    ImageName = C.Name;
    Height = abs(get(C, 'Height'));
    Fe = get(C, 'SampleFrequency');
    y = C.x;
    stats = C.StatValues;
    D = get_Image(C);
    coef = max(Fe) ./ Fe;
    D = dilateImage(D, coef);
    D = D';
    if isempty(CLim)
        CLim = stats.Quant_01_99;
    end
    if (CLim(1) == CLim(2)) || isnan(CLim(1))
        flag = 0;
        return
    end
    x = 1:size(D,2);

    if ~isempty(ALim)
        ImageName = sprintf('%s - Angular sector [%d %d] deg', ImageName, ALim(1), ALim(2));
    end
    
    subFlag = ~isfinite(sum(Value,2));
    D(:,subFlag) = NaN;
    DisplayImage.CData = D;
    DisplayImage.CLim  = CLim;
    DisplayImage.x     = x;
    DisplayImage.y     = y;
%     DisplayImage.HeightBas  = (Height .* coef) * SeuilBas;
%     DisplayImage.HeightHaut = (Height .* coef) * SeuilHaut;
%     DisplayImage.Heights = (Height .* coef) * params_ZoneEI.Seuils;
    DisplayImage.Name  = ImageName;
    
    h(1) = subplot(2,1,1);
    imagesc(DisplayImage.x, DisplayImage.y, DisplayImage.CData, DisplayImage.CLim); colormap(jet(256)); colorbar;
    hold on; xlabel('Pings #'); ylabel('Samples #');
    nbSlices = length(params_ZoneEI.Seuils) - 1;
    Colors = jet(nbSlices+1);
    for k=1:(nbSlices+1)
        hc = plot((Height .* coef) * params_ZoneEI.Seuils(k));
        set(hc, 'Color', Colors(k,:))
    end
    title(DisplayImage.Name, 'Interpreter', 'None')
end

%% Plot du signal

h(2) = subplot(2,1,2);
for k=1:nbSlices
    hc = PlotUtils.createSScPlot(x, Value(sublDepth,k), '.-'); grid on; hold on;
    set(hc, 'Color', Colors(k,:))
end
axis tight; hc = colorbar; hc.Visible = 'Off';
[~, Title, ext] = fileparts(nomFic);
xlabel('Ping #'); ylabel(DataName);
Title = sprintf('Echo Integration %s-%s', Tag, [Title, ext]);
title(Title, 'Interpreter', 'None')

linkaxes(h, 'x');
