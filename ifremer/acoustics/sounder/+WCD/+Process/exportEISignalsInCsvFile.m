function flag = exportEISignalsInCsvFile(nomFicCsv, Datetime, varargin)

[varargin, VarNames]       = getPropertyValue(varargin, 'VarNames',       []);
[varargin, OriginFileName] = getPropertyValue(varargin, 'OriginFileName', []);

Value = varargin;

%% Réarangement des données pour pouvoir utiliser exportSignalsInCsvFile

OK = true;
while OK
    OK = false;
    for k=1:length(Value)
        if size(Value{k},1) ~= 1
            X = Value{k}(2,:);
            Value{end+1} = X; %#ok<AGROW>
            Value{k}(2,:) = [];
            OK = true;
        end
    end
end

%% Export

flag = exportSignalInCsvFile(nomFicCsv, Datetime, Value{:}, 'VarNames', VarNames, 'OriginFileName', OriginFileName);
