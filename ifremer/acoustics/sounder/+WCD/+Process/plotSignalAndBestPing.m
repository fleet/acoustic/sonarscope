function [InfoImageRaw, InfoImageComp, Carto] = plotSignalAndBestPing(ARaw, BComp, nomFic, DataName, MetriqueRaw, MetriqueComp, ...
    Carto, subDepth, params_ZoneEI, ALim, BLim, CorrectionTVG, TypeUnit, maskOverlapingBeams, ...
    DataDepth, DataRaw, DataWC, DataSeabed, DataInstalParam, DataRuntime, DataSSP, DataAttitude)

InfoImageRaw  = [];
InfoImageComp = [];

%% Recherche du ping le plus intéressant

[MaxValue, kRaw]      = max(MetriqueRaw);
[MaxValueComp, kComp] = max(MetriqueComp);

%% Affichage détection sur Raw data

[flag, FigRaw, DisplayImageRaw] = WCD.Process.displayImageAndSignal(ARaw, MetriqueRaw, kRaw, DataName, nomFic, 'Raw', params_ZoneEI, ALim, BLim, CorrectionTVG, ...
    TypeUnit, maskOverlapingBeams, DataDepth, DataRaw, DataWC, DataSeabed, DataInstalParam, DataRuntime, DataSSP, DataAttitude, subDepth);
if ~flag
    return
end

%% Affichage détection sur Comp data

CLimBComp = BComp.CLim;
CLimBComp(1) = -10;
BComp.CLim = CLimBComp;
[flag, FigComp, DisplayImageComp] = WCD.Process.displayImageAndSignal(BComp, MetriqueComp, kComp, DataName, nomFic, 'Comp', params_ZoneEI, ALim, BLim, CorrectionTVG, ...
    TypeUnit, maskOverlapingBeams, DataDepth, DataRaw, DataWC, DataSeabed, DataInstalParam, DataRuntime, DataSSP, DataAttitude, subDepth);
if ~flag
    return
end

%% Sauvegarde des métadonnées

SampleBeamData = get_SonarSampleBeamData(ARaw);
InfoImageRaw.AllFilename = nomFic;
InfoImageRaw.PingNumber  = subDepth(kRaw);
InfoImageRaw.Datetime    = SampleBeamData.Datetime;
InfoImageRaw.Latitude    = SampleBeamData.Latitude;
InfoImageRaw.Longitude   = SampleBeamData.Longitude;
InfoImageRaw.Heading     = SampleBeamData.Heading;
InfoImageRaw.DataName    = DataName;
InfoImageRaw.MaxValue    = MaxValue;
InfoImageRaw.Tag         = 'Raw';
InfoImageRaw.FigRaw      = FigRaw;
InfoImageRaw.DisplayImageRaw = DisplayImageRaw;
setappdata(FigRaw, 'InfoImage', InfoImageRaw)

SampleBeamData = get_SonarSampleBeamData(BComp);
InfoImageComp.AllFilename = nomFic;
InfoImageComp.PingNumber  = subDepth(kComp);
InfoImageComp.Datetime    = SampleBeamData.Datetime;
InfoImageComp.Latitude    = SampleBeamData.Latitude;
InfoImageComp.Longitude   = SampleBeamData.Longitude;
InfoImageComp.Heading     = SampleBeamData.Heading;
InfoImageComp.DataName    = DataName;
InfoImageComp.MaxValue    = MaxValueComp;
InfoImageComp.Tag         = 'Comp';
InfoImageComp.FigComp     = FigComp;
InfoImageRaw.DisplayImageComp = DisplayImageComp;
setappdata(FigComp, 'InfoImage', InfoImageComp)
