function plotSlicesSignal(nomFic, DataName, MetriqueRaw, MetriqueComp, ...
    subDepth, params_ZoneEI, ALim, CorrectionTVG, TypeUnit, maskOverlapingBeams, ...
    DataDepth, DataRaw, DataWC, DataSeabed, DataInstalParam, DataRuntime, DataSSP, DataAttitude)

%% Affichage détection sur Raw data

[flag, FigRaw, DisplayImageRaw] = WCD.displaySlicesSignal(MetriqueRaw, DataName, nomFic, 'Raw', params_ZoneEI, ALim, CorrectionTVG, ...
    TypeUnit, maskOverlapingBeams, DataDepth, DataRaw, DataWC, DataSeabed, DataInstalParam, DataRuntime, DataSSP, DataAttitude, subDepth); %#ok<ASGLU>
if ~flag
    return
end

%% Affichage détection sur Comp data

[flag, FigComp, DisplayImageComp] = WCD.displaySlicesSignal(MetriqueComp, DataName, nomFic, 'Comp', params_ZoneEI, ALim, CorrectionTVG, ...
    TypeUnit, maskOverlapingBeams, DataDepth, DataRaw, DataWC, DataSeabed, DataInstalParam, DataRuntime, DataSSP, DataAttitude, subDepth); %#ok<ASGLU>
if ~flag
    return
end
