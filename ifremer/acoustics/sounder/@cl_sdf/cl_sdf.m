% Constructeur de cl_sdf (Imagerie contenue dans une .sdf)
%
% Syntax
%   a = cl_sdf(...)
% 
% Name-Value Pair Arguments
%   nomFic : Nom(s) du fichier .sdf
%  
% Output Arguments 
%   a : Instance de cl_sdf
%
% Examples
%   nomFic = getNomFicDatabase('SSS_091009092600.sdf');
%   a = cl_sdf('nomFic', nomFic)
%
% See also Authors
% Authors : JMA
% ----------------------------------------------------------------------------

classdef cl_sdf < handle
    
    properties(GetAccess = 'public', SetAccess = 'public')
        nomFic = '';
    end
    
    properties(GetAccess = 'public', SetAccess = 'private')
        PageVersion = '';
    end
    
    methods (Access = 'public')
        function this = cl_sdf(varargin)
            this = set(this, varargin{:});
        end

        function x = nbPings(this)
            [flag, XML] = SSc_ReadDatagrams(this.nomFic, 'Ssc_V3001', 'XMLOnly', 1);
            if flag
                x = XML.Dimensions.nbPings;
            else
                x = 0;
            end
        end

        function x = nbCol(this)
            [flag, XML] = SSc_ReadDatagrams(this.nomFic, 'Ssc_V3001', 'XMLOnly', 1);
            if flag
                x = XML.Dimensions.nbCol;
            else
                x = 0;
            end
        end
    end
end
