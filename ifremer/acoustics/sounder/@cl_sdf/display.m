% Representation externe d'une instance sur le terminal
%
% Syntax
%   display(this)
%
% Input Arguments 
%   this : une instance de cl_sdf
%
% Examples
%   nomFic = getNomFicDatabase('EW150.RDF')
%   a = cl_sdf('nomFic', nomFic);
%   display(a);
%
% See also cl_sdf cl_sdf/char Authors
% Authors : GLU
% ----------------------------------------------------------------------------

function display(this)

s = whos('this');
sz = s.size;
fprintf('Name\tSize\tClass\n');
fprintf('%s\t%s\t%s\n', inputname(1), num2strCode(sz), s.class);
disp(char(this));

