% Acces en lecture des proprietes d'une instance
%
% Syntax
%   [...] = get(this, ...)
%
% Input Arguments
%   this : Une instance de la classe cl_sdf
%
% Name-Value Pair Arguments
%   nomFic : Nom du fichier .sdf
%   Memmapfile : 0=Variable en RAM, 1=variable en m�moire virtuelle, 0 par d�faut)
%   Liste des propri�t�s list�es par display(this)
%
% Output Arguments
%   PropertyValues associees aux PropertyNames passes en argument
%
% Examples
%   nomFic = getNomFicDatabase('SSS_091009092600.sdf');
%   a = cl_sdf('nomFic', nomFic);
%   a
%   a.nomFic
%   a.PageVersion
%   a.nbPings
%   a.nbCol
%   get(a, 'heading')
%   SonarLF = get(a, 'SonarLF');
%   SonarLF = get(a, 'SonarLF', 'Memmapfile', 1);
%
% See also cl_sdf cl_sdf/set Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function varargout = get(this, varargin)

[varargin, Memmapfile] = getPropertyValue(varargin, 'Memmapfile', 0);

%% Boucle de parcours des arguments

varargout = {};

PageVersion = this.PageVersion;
for k=1:length(varargin)
    PN = varargin{k};
    flag = existCacheNetcdf(this.nomFic);
    if flag
        [~, X] = SScCacheNetcdfUtils.getSignalUnit(this.nomFic, PageVersion, PN, 'Memmapfile', Memmapfile);
    else
        [~, X] = SScCacheXMLBinUtils.getSignalUnit(this.nomFic, ['Ssc_' PageVersion], PN, 'Memmapfile', Memmapfile);
    end
    varargout{end+1} = X; %#ok<AGROW>
end
