% [flag, dataFileName] = FtpUtils.getSScDemoFile('Syp_001_180507104600.sdf', 'OutputDir', 'D:\Temp\ExData')
% a = cl_sdf('nomFic', dataFileName);
%
% listeSignals = get_listeSignals(a)

function listeSignals = get_listeSignals(this)

listeSignals = {};
[flag, XML] = SSc_ReadDatagrams(this.nomFic, 'Ssc_V3001', 'XMLOnly', 1);
if flag
%     Fields = fieldnames(XML.Dimensions);
    for k=1:length(XML.Signals)
        listeSignals{end+1} = XML.Signals(k).Name; %#ok<AGROW>
    end
end
