function [flag, Signal, Unit, Colors] = get_signal(a, nomSignal, varargin)

[varargin, Memmapfile] = getPropertyValue(varargin, 'Memmapfile', 0); %#ok<ASGLU>

Signal = [];
Unit   = '?';
nbColors = 64;
Colors = jet(nbColors);

switch nomSignal
    case {'Time'; 'Datetime'; 'PitchMean'; 'PitchStd'; 'RollMean'; 'RollStd'; 'HeadingMean'; 'HeadingStd'; 'Inter ping distance'}
        [flag, OrigineNavigation, SonarTime, Latitude, Longitude] = read_navigation(a); %#ok<ASGLU>
        if ~flag
            return
        end
end

ConstanteDeTemps = 1*60 ; % 5 mn
switch nomSignal
    case 'Time'
        Signal = SonarTime.timeMat;
        Unit = 'ms';
        
    case 'Datetime'
        Signal = datetime(SonarTime.timeMat, 'ConvertFrom', 'datenum');
        Unit = 'proleptic ISO calendar';
        
    case 'Height'
        Signal = -get(a, 'altitudeMeters');
        Unit = 'm';
        
    case 'Bathy' % TODO : sans doute utiliser Data.depth
        Signal = -get(a, 'altitudeMeters');
        Unit = 'm';
        
    case 'PitchMean'
        Signal = get(a, 'pitch');
        Signal = filtreAttitude(SonarTime, Signal, ConstanteDeTemps);
        Unit = 'deg';
        
    case 'PitchStd'
        Signal = get(a, 'pitch');
        [~, Signal] = filtreAttitude(SonarTime, Signal, ConstanteDeTemps);
        Unit = 'deg';
    case 'RollMean'
        Signal = get(a, 'roll');
        Signal = filtreAttitude(SonarTime, Signal, ConstanteDeTemps);
        Unit = 'deg';
        
    case 'RollStd'
        Signal = get(a, 'roll');
        [~, Signal] = filtreAttitude(SonarTime, Signal, ConstanteDeTemps);
        
    case 'HeadingMean'
        Signal = get(a, 'heading');
        Signal = filtreAttitude(SonarTime, Signal, ConstanteDeTemps);
        Colors = hsv(nbColors);
        Unit = 'deg';
        
    case 'HeadingStd'
        Signal = get(a, 'heading');
        [~, Signal] = filtreAttitude(SonarTime, Signal, ConstanteDeTemps);
        Unit = 'deg';
        
    case 'Inter ping distance'
        [~, ~, Signal] = calCapFromLatLon(Latitude, Longitude);
        Unit = 'm';
        
    case 'heading'
        Signal = get(a, nomSignal);
        Colors = hsv(nbColors);
        Unit = 'deg';
        
    case 'temperature'
        Signal = get(a, nomSignal) * 5;
        Unit = 'deg';
        
    case 'Salinity'
        T = get(a, 'temperature') * 5;
        C = get(a, 'speedSound');
        
    otherwise
        flag = existCacheNetcdf(a.nomFic);
        PageVersion = a.PageVersion;
        if flag
            [~, Signal, Unit] = SScCacheNetcdfUtils.getSignalUnit(a.nomFic, PageVersion, nomSignal, 'Memmapfile', Memmapfile);
        else
            [~, Signal, Unit] = SScCacheXMLBinUtils.getSignalUnit(a.nomFic, ['Ssc_' PageVersion], nomSignal, 'Memmapfile', Memmapfile);
        end
end

flag = 1;

    %{
     'range'    'speedFish'    'speedSound'  'altitude'    'temperature'    'speed'    'shipHeading'

  Columns 12 through 21

    'magneticVariation'    'tvgPage'    'auxPitch'    'auxRoll'    'auxDepth'    'auxAlt'    'cableOut'    'fseconds'    'altimeter'    'sampleFreq'

  Columns 22 through 26

    'shieveXoff'    'shieveYoff'    'shieveZoff'    'GPSheight'    'rawDataConfig'

    %}

