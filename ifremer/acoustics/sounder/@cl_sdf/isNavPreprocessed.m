function flag = isNavPreprocessed(this)

%% Test si il y a une nav "Poisson"

fishLat  = get(this, 'fishLat');
fishLon  = get(this, 'fishLon');

flag = ~(all(fishLat == 0) || all(fishLon == 0));
   
if ~flag
    str1 = sprintf('Le fichier "%s" ne contient pas de navigation au niveau "Poisson".', this.nomFic);
    str2 = sprintf('File "%s" does not have any "Fish" navigation.', this.nomFic);
    my_warndlg(Lang(str1,str2), 0, 'Tag', 'isNavPreprocessed', 'TimeDelay', 60);
end

%% Test si il y a une nav "Navire"

shipLat  = get(this, 'shipLat');
shipLon  = get(this, 'shipLon');

flag = ~(all(shipLat == 0) || all(shipLon == 0));
if ~flag
    str1 = sprintf('Le fichier "%s" ne contient pas non plus de navigation "Navire".', this.nomFic);
    str2 = sprintf('File "%s" does not have any "Ship" navigation either.', this.nomFic);
    my_warndlg(Lang(str1,str2), 0, 'Tag', 'isNavPreprocessed');
end
