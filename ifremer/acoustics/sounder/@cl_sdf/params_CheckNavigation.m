function [flag, nomFic, TypeCheck, repImport] = params_CheckNavigation(~, repImport)

TypeCheck = [];

[flag, nomFic, repImport] = uiSelectFiles('ExtensionFiles', '.sdf', 'RepDefaut', repImport);
if ~flag
    return
end

str = {'Check fish nav (USBL data)'; 'Compute Fish nav from Ship nav & Layback in case there is no Fich nav'};
[TypeCheck, flag] = my_listdlg('Liste of items', str, 'SelectionMode', 'Single', 'InitialValue', 1);
if ~flag
    return
end
