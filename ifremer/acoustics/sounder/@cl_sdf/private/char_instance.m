% Transformation en chaine de caracteres d'une instance
%
% Syntax
%   str = char_instance(a)
%
% Input Arguments
%   a : instance de cl_sdf
%
% Name-Value Pair Arguments
%   InfoSonar  : 1=Information de l'image uniquement
%                2=Info de l'information sonar uniquement
%                3=Display de toute l'information disponible (image+sonar) : (3 par defaut)
%
% Output Arguments
%   str : une chaine de caracteres representative de l instance
%
% Examples
%   [flag, dataFileName] = FtpUtils.getSScDemoFile('Syp_001_180507104600.sdf', 'OutputDir', 'D:\Temp\ExData')
%   a = cl_sdf('nomFic', dataFileName);
%   a
%
% See also cl_sdf Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function str = char_instance(this, varargin)

str = {};
str{end+1} = sprintf('nomFic        <-> %s', this.nomFic);
str{end+1} = sprintf('PageVersion   <-- %s', this.PageVersion);

[flag, XML] = SSc_ReadDatagrams(this.nomFic, 'Ssc_V3001', 'XMLOnly', 1);
if flag
    Fields = fieldnames(XML.Dimensions);
    str{end+1} = 'Dimensions';
    for k=1:length(Fields)
        str{end+1} = sprintf('\t%s\t\t\t<-- %d', Fields{k}, XML.Dimensions.(Fields{k})); %#ok<AGROW>
    end
    
    str{end+1} = '-- Signals --';
    for k=1:length(XML.Signals)
        str{end+1} = sprintf('\t%s\t\t\t<-> %s(%s) (%s)', XML.Signals(k).Name, XML.Signals(k).Storage, ...
            num2str(XML.Signals(k).Dimensions.Length), XML.Signals(k).Unit); %#ok<AGROW>
    end
    
    str{end+1} = '-- Images --';
    for k=1:length(XML.Images)
        str{end+1} = sprintf('\t%s\t\t\t<-> %s(%s) (%s)', XML.Images(k).Name, XML.Images(k).Storage, ...
            num2strCode([XML.Images(k).Dimensions.Length]), XML.Images(k).Unit); %#ok<AGROW>
    end
end

%% Concatenation en une chaine

str = cell2str(str);
