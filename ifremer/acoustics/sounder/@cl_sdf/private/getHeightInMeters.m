function altitudeMeters = getHeightInMeters(this, SonarHeightInAmples)

speedSound     = get(this, 'speedSound');
% altitudeMeters = get(this, 'altitudeMeters');
sampleFreq     = get(this, 'sampleFreq');

altitudeMeters = -speedSound(:) .* SonarHeightInAmples ./ (2 * sampleFreq(:));
