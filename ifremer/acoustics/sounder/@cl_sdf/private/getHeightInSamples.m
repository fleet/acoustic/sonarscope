function Signal = getHeightInSamples(this)

speedSound     = get(this, 'speedSound');
altitudeMeters = get(this, 'altitudeMeters');
sampleFreq     = get(this, 'sampleFreq');

Signal = altitudeMeters .* (2 * sampleFreq) ./ speedSound;
