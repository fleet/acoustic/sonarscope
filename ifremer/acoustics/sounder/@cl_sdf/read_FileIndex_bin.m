function [flag, Data, XML] = read_FileIndex_bin(this, varargin)

Data = [];
XML  = [];
 
[varargin, fileName] = getPropertyValue(varargin, 'nomFic', get(this, 'nomFic')); %#ok<ASGLU>

if isempty(fileName)
    flag = 0;
    return
end

%% Lecture du fichier XML d�crivant la donn�e

[nomDirRacine, nomFicSeul, ~] = fileparts(fileName);
nomFicXml = fullfile(nomDirRacine, 'SonarScope', nomFicSeul, 'SDF_FileIndex.xml');

flag = exist(nomFicXml, 'file');
if ~flag
    special_message('FichierCacheAbsent', nomFicXml)
    return
end
% edit(nomFicXml)
XML = xml_read(nomFicXml);

%{
for i=1:length(XML.Variables)
fprintf(1,'%d  %s\n', i, XML.Variables(i).Name)
end
%}

%% Lecture des signaux

[nomDirSignal, nomFicSignal] = fileparts(nomFicXml);
nomDirSignal = fullfile(nomDirSignal, nomFicSignal);

[flag, Year] = Read_BinVariable(XML, nomDirSignal, 'year', 'uint32');
if ~flag
    return
end
% figure; plot(Year); grid on; title('Year');

[flag, Month] = Read_BinVariable(XML, nomDirSignal, 'month', 'uint32');
if ~flag
    return
end
% figure; plot(Month); grid on; title('Month');

[flag, Day] = Read_BinVariable(XML, nomDirSignal, 'day', 'uint32');
if ~flag
    return
end
% figure; plot(Day); grid on; title('Day');

[flag, Hour] = Read_BinVariable(XML, nomDirSignal, 'hour', 'uint32');
if ~flag
    return
end
% figure; plot(Hour); grid on; title('Hour');

[flag, Minute] = Read_BinVariable(XML, nomDirSignal, 'minute', 'uint32');
if ~flag
    return
end
% figure; plot(Minute); grid on; title('Minute');

[flag, Second] = Read_BinVariable(XML, nomDirSignal, 'second', 'uint32');
if ~flag
    return
end
% figure; plot(Second); grid on; title('Second');

[flag, HSecond] = Read_BinVariable(XML, nomDirSignal, 'hSecond', 'uint32');
if ~flag
    return
end
% figure; plot(HSecond); grid on; title('HSecond');

%% TypeOfPage

[flag, Data.TypeOfDatagram] = Read_BinVariable(XML, nomDirSignal, 'TypeOfPage', 'single');
if ~flag
    return
end
% figure; plot(Data.TypeOfPage, '*'); grid on; title('TypeOfPage')

%% Lecture de FileIndexCounter

[flag, Data.PingCounter] = Read_BinVariable(XML, nomDirSignal, 'FileIndexCounter', 'single');
if ~flag
    return
end
% figure; plot(Data.FileIndexCounter); grid on; title('FileIndexCounter')

%% Lecture du DatagramLength

[flag, Data.DatagramLength] = Read_BinVariable(XML, nomDirSignal, 'DatagramLength', 'single');
if ~flag
    return
end
% figure; plot(Data.DatagramLength); grid on; title('DatagramLength')


%% Lecture du DatagramPosition

[flag, Data.DatagramPosition] = Read_BinVariable(XML, nomDirSignal, 'DatagramPosition', 'double');
if ~flag
    return
end
% figure; plot(Data.DatagramPosition); grid on; title('DatagramPosition')

% Data.DatagramPosition = [0; Data.DatagramPosition];
% DatagramPosition = [Data.DatagramPosition; sizeFic(this.nomFic)];
% Data.DatagramLength = diff(DatagramPosition);

%% Calcul des dates et heures

NbMilliSecs = (Hour*3600 + Minute*60 + Second)*1e3 + HSecond;
Date = dayJma2Ifr(Day, Month, Year);
Data.Time = cl_time('timeIfr', Date, NbMilliSecs);
