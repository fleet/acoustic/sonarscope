% Lecture des donn�es d'un fichier .RDF
%
% Syntax
%   [Data, b, Carto] = read_Image(a, ...)
%
% Input Arguments
%   a : Instance de cl_rdf
%
% Name-Value Pair Arguments
%   Carto : Instance de cl_carto
%
% Output Arguments
%   Data  : Structure contenant :
%   b     : Instances de cl_image contenant la reflectivite et le numero de faisceau
%   Carto : Instance de cl_carto
%
% Examples
%   nomFic = getNomFicDatabase('EW150.RDF');
%   a = cl_rdf('nomFic', nomFic);
%   [Data, b] = read_Image(a);
%   SonarScope(b)
%
%   figure; plot(Data.Time, Data.PingCounter);     grid on;  title('PingCounter')
%
% See also cl_rdf cl_rdf/plot_nav Authors
% Authors : JMA
% -------------------------------------------------------------------------

function [flag, b, Carto, OrigineNavigation] = read_Image(this, varargin)

[varargin, Carto] = getPropertyValue(varargin, 'Carto', []);

nbImages = length(this);
for k=1:nbImages
    [flag, b, Carto, OrigineNavigation] = unitaire_read_Image(this(k), Carto, varargin{:});
    if ~flag
        b = [];
        Carto = [];
        return
    end
end


function [flag, b, Carto, OrigineNavigation] = unitaire_read_Image(this, Carto, varargin)

[varargin, SourceHeading] = getPropertyValue(varargin, 'SourceHeading', 1);
[varargin, SelecImage]    = getPropertyValue(varargin, 'SelecImage',    [1 2]);
[varargin, subx]          = getPropertyValue(varargin, 'subx',          []); %#ok<ASGLU>

b    = cl_image;
b(:) = [];

nomDir = fileparts(this.nomFic);

%% Lecture du fichier

[flag, OrigineNavigation, SonarTime, Latitude, Longitude, ~, Heading] = read_navigation(this, 'SourceHeading', SourceHeading);
if ~flag
    return
end

%% Cr�ation des images

GeometryType = cl_image.indGeometryType('PingSamples');
InitialFileFormat = 'KleinSDF';

range = get(this, 'range');
rangeMedian = median(range);

% BF
Mode_1 = 1;
switch rangeMedian
    case 50
        Mode_2 = 1;
    case 75
        Mode_2 = 2;
    case 100
        Mode_2 = 3;
    case 150
        Mode_2 = 4;
    case 200
        Mode_2 = 5;
    case 600
        Mode_2 = 6;
    otherwise
        str1 = sprintf('rangeMedian=%f : valeur non reconnue.', rangeMedian);
        str2 = sprintf('rangeMedian=%f : this value is not recognized.', rangeMedian);
        my_warndlg(Lang(str1,str2), 1);
        Mode_2 = 1; % TODO
end
SonarDescription(1) = cl_sounder('Sonar.Ident', 27, 'Sonar.Mode_1', Mode_1, 'Sonar.Mode_2', Mode_2);

% HF
Mode_1 = 2;
switch rangeMedian
    case 25
        Mode_2 = 1;
    case 50
        Mode_2 = 2;
    case 75
        Mode_2 = 3;
    case 100
        Mode_2 = 4;
    case 150
        Mode_2 = 5;
    case 200
        Mode_2 = 5; % TODO : Ajout JMA le 10/08/2015 pour donn�es C�line o� rangeMedian=200
    otherwise
        str1 = sprintf('rangeMedian=%f : valeur non reconnue.', rangeMedian);
        str2 = sprintf('rangeMedian=%f : this value is not recognized.', rangeMedian);
        my_warndlg(Lang(str1,str2), 1);
        Mode_2 = 1; % TODO
end
SonarDescription(2) = cl_sounder('Sonar.Ident', 27, 'Sonar.Mode_1', Mode_1, 'Sonar.Mode_2', Mode_2);

% Subbottom
Mode_1 = 1; % TODO : regarder ce qui est fait pour le SAR
SonarDescription(3) = cl_sounder('Sonar.Ident', 26, 'Sonar.Mode_1', Mode_1, 'Sonar.Mode_2', Mode_2);

% SamplingRate = Data.sampleFreq(:);
SamplingRate = get(this, 'sampleFreq');

tvgPage     = get(this, 'tvgPage');
temperature = get(this, 'temperature');
speedSound  = get(this, 'speedSound');

N = floor(this.nbCol / 2);
x = double(-N:N);
y = double(1:this.nbPings);

if isempty(subx)
    subx = 1:length(x);
end
x = x(subx);

[~, nomFicSeul] = fileparts(this.nomFic);

%% Cr�ation des objets image

k = 0;
if any(SelecImage == 1) % BF
    k = k + 1;
    ImageName{k} = [nomFicSeul '_BF'];
    I = get(this, 'SonarLF', 'Memmapfile', 1);
    b(k) = cl_image('Image', I, 'Name', ImageName{k}, 'Unit', 'dB', ...
        'DataType', cl_image.indDataType('Reflectivity'), 'ColormapIndex', 2, ...
        'TagSynchroX', [nomFicSeul ' - PingSamples'], 'TagSynchroY', nomFicSeul,...
        'x', x, 'XUnit', 'samples', 'y', y, 'YUnit', 'Pings', ...
        'InitialFileName', this.nomFic, ...
        'InitialFileFormat', InitialFileFormat, ...
        'GeometryType', GeometryType,...
        'SonarDescription', SonarDescription(1));
    resol(k) = get(SonarDescription(1), 'Proc.RangeResol');
end

if any(SelecImage == 2) % HF
    k = k + 1;
    ImageName{k} = [nomFicSeul '_HF'];
    I = get(this, 'SonarHF', 'Memmapfile', 1);
    b(k) = cl_image('Image',  I, 'Name', ImageName{k}, 'Unit', 'dB', ...
        'DataType', cl_image.indDataType('Reflectivity'), 'ColormapIndex', 2, ...
        'TagSynchroX', [nomFicSeul ' - PingSamples'], 'TagSynchroY', nomFicSeul,...
        'x', x, 'XUnit', 'samples', 'y', y, 'YUnit', 'Pings', ...
        'InitialFileName', this.nomFic, ...
        'InitialFileFormat', InitialFileFormat, ...
        'GeometryType', GeometryType,...
        'SonarDescription', SonarDescription(2));
    resol(k) = get(SonarDescription(2), 'Proc.RangeResol');
end

% TODO : trouver un fichier qui contient du subbottom et d�velopper
if any(SelecImage == 3) % SBP
    SBP = get(this, 'sbp', 'Memmapfile', 1);
    if ~isempty(SBP)
        my_warndlg('Please send this file to sonarscope@ifremer.fr in order he can develop this part of the software.', 1);
        %{
k = k + 1;
ImageName{k} = [nomFicSeul '_SPB'];
b(k) = cl_image('Image', SBP, 'Name', ImageName{k}, 'Unit', '', ...
'DataType', cl_image.indDataType('Reflectivity'), 'ColormapIndex', 2, ...
'TagSynchroX', [nomFicSeul ' - PingSamples'], 'TagSynchroY', nomFicSeul,...
'x', x, 'XUnit', 'samples', 'y', y, 'YUnit', 'Pings', ...
'InitialFileName', this.nomFic, ...
'InitialFileFormat', InitialFileFormat, ...
'GeometryType', GeometryType,...
'SonarDescription', SonarDescription);
        %}
    else
        str1 = sprintf('Il n''y a pas de donn�es de sondeur p�n�trateur de s�diments dans le fichier "%s".', nomFicSeul);
        str2 = sprintf('There is no subbottom data in file "%s".', nomFicSeul);
        my_warndlg(Lang(str1,str2), 0);
    end
end
clear X

SonarHeight = getHeightInSamples(this);
for k=1:length(b)
    b(k) = update_Name(b(k));
    b(k) = set(b(k), 'SonarTime', SonarTime);
    b(k) = set(b(k), 'SonarRawDataResol', resol(k), 'SonarResolutionD', resol(k), 'SonarResolutionX', resol(k));
    b(k) = set_SonarRoll(b(k),              get(this, 'roll'));
    b(k) = set_SonarPitch(b(k),             get(this, 'pitch'));
    %     b(k) = set_SonarHeave(b(k),             Heave);
    b(k) = set_SonarPingCounter(b(k),       1:length(y));
    b(k) = set_SonarFishLongitude(b(k),     Longitude);
    b(k) = set_SonarFishLatitude(b(k),      Latitude);
    b(k) = set_SonarHeading(b(k),           Heading);
    b(k) = set_SonarSampleFrequency(b(k),   SamplingRate);
    %     b(k) = set_SonarSurfaceSoundSpeed(b(k), Data.speedSound(:));
    b(k) = set_SonarSurfaceSoundSpeed(b(k), get(this, 'speedSound'));
    b(k) = set_SonarHeight(b(k),            SonarHeight);
    
    b(k) = set_SonarCableOut(b(k),          get(this, 'cableOut'));
    b(k) = set_SonarSpeed(b(k),             get(this, 'speed'));
    
    %% Definition d'une carto
    
    if isempty(Carto)
        [Carto, flag] = getDefinitionCarto(b(k), 'nomDirSave', nomDir, 'NoGeodetic', 0);
        if ~flag
            return
        end
    end
    b(k) = set(b(k), 'Carto', Carto);
    
    %% Definition du nom de l'image
    
    b(k) = set(b(k), 'Name', ImageName{k});
    b(k) = update_Name(b(k));
end
flag = 1;
