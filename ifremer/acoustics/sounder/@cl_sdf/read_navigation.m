function [flag, OrigineNavigation, SonarTime, Latitude, Longitude, SonarSpeed, Heading] = read_navigation(this, varargin)

[varargin, SourceHeading] = getPropertyValue(varargin, 'SourceHeading', 3);
[varargin, OriginNav]     = getPropertyValue(varargin, 'OriginNav',     3); %#ok<ASGLU>

SonarTime = get(this, 'Time');

if isempty(SonarTime)
    flag              = 0;
    OrigineNavigation = [];
    SonarTime         = [];
    Latitude          = [];
    Longitude         = [];
    SonarSpeed        = [];
    Heading           = [];
    return
end

switch OriginNav
    case 1 % Ship
        Latitude  = get(this, 'shipLat');
        Longitude = get(this, 'shipLon');
        OrigineNavigation = 'Ship';
    case 2 % Fish
        Latitude  = get(this, 'fishLat');
        Longitude = get(this, 'fishLon');
        OrigineNavigation = 'Fish';
    case 3 % Auto : on prend Fish si elle est d�finie
        Latitude  = get(this, 'fishLat');
        Longitude = get(this, 'fishLon');
        OrigineNavigation = 'Fish';
        if isempty(Latitude) || all(isnan(Latitude))
            Latitude  = get(this, 'shipLat');
            Longitude = get(this, 'shipLon');
            OrigineNavigation = 'Ship';
        end
end

SonarSpeed = get(this, 'speed');
switch SourceHeading
    case 1 % Fish
        Heading = get(this, 'heading');
    case 2 % Computed from Fish navigation
%         Heading = get(this, 'fishNavHeading'); % Modif JMA le 09/02/2021
        Heading = calCapFromLatLon(Latitude, Longitude, 'Time', SonarTime); % Modif JMA le 15/03/2023
        if isempty(Heading)
            str = sprintf('The "fishNavHeading" for %s does not seem to be computed. Select another heading please.', this.nomFic);
            my_warndlg(str, 1)
            flag = 0;
            return
        end
    case 3 % Ship
        Heading = get(this, 'shipHeading'); % Modif JMA le 08/02/2021
    otherwise
        my_breakpoint
end
% figure; plot(Heading, 'k'); grid on; hold on; plot(shipHeading, 'b');

if (OriginNav == 3) && all(Latitude == 0)
    str1 = sprintf('La navigation du poisson n''a pas �t� calcul�e pour "%s".\nC''est la navigation du bateau qui est retourn�e.', this.nomFic);
    str2 = sprintf('The fish navigation has not been computed yet for "%s".\nThe ship navigation is returned.', this.nomFic);
    my_warndlg(Lang(str1,str2), 0, 'Tag', 'isNavPreprocessed');
    Latitude  = get(this, 'shipLat');
    Longitude = get(this, 'shipLon');
    Heading   = get(this, 'shipHeading');
    OrigineNavigation = 'Ship';
end

Longitude = Longitude(:,1);
Latitude  = Latitude(:,1);

sub = (Longitude > 180);
Longitude(sub) = Longitude(sub) - 360;

flag = 1;
