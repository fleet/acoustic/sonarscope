% Saucissonnage d'un fichier .SDF
%
% Syntax
%   Data = saucissonne_bin(a, ...)
%
% Input Arguments
%   a : Instance de cl_sdf
%
% Name-only Arguments
%   NbPings  : [] ou Nombre max de pings
%   tCoupure : [] ou heures des coupures
%
% Output Arguments
%   nomFicOut : liste de fichiers de sortie.
%
% Examples
%   nomFic = 'F:\SonarScopeData\From Charline\SoReco2_010_111012160800.sdf';
%   a = cl_sdf('nomFic', nomFic);
%   [~, Data] = SSc_ReadDatagrams(nomFic, 'Ssc_V3001', 'Memmapfile', -1);
%   iPing   = floor(Data.Dimensions.nbPings/3/2);
%
%   t1 = Data.fixTime(iPing*1);
%   t2 = Data.fixTime(iPing*2);
%   t3 = Data.fixTime(iPing*3);
%   saucissonne_bin(a, Data.Dimensions.nbPings, [t1.timeMat t2.timeMat t3.timeMat], []);
%
% See also cl_SDF Authors
% Authors : GLU
% ----------------------------------------------------------------------------

function nomFicOut = saucissonne_bin(this, NbPings, tCoupure, subDatagram)

nomFicOut = {};

%% Check the number of elements

flag = checkIfOneElement(length(this));
if ~flag
    return
end

%%

% Cr�ation des fichiers unitaire de type SDF.
nomFicSDFOut = saucissonne_bin_unitaire(this, NbPings, tCoupure, subDatagram);

[nomDir, nomFic] = fileparts(get(this, 'nomFic'));

%% Lecture des nouveaux fichiers SDF (et WCD en automatique)

for k=1:length(nomFicSDFOut)
    a(k) = cl_sdf('nomFic', nomFicSDFOut{k}); %#ok<NASGU,AGROW>
end

% plot_position(a)

%% Renommage du fichier .SDF et .wcd

if isempty(NbPings) && isempty(tCoupure)
    nomFicNew = fullfile(nomDir, [nomFic '.SDFReduced']);
else
    nomFicNew = fullfile(nomDir, [nomFic '.SDFSplitted']);
end
[status, message] = movefile(get(this, 'nomFic'), nomFicNew, 'f');

if status
    strFR = sprintf('Le fichier "%s" a �t� renomm� en "%s"', this.nomFic, nomFicNew);
    strUS = sprintf('File "%s" was renamed as "%s"', this.nomFic, nomFicNew);
    my_warndlg(Lang(strFR,strUS), 0, 'Tag', 'ImpossibleDeRenommerCeFichier');
else
    strFR = sprintf('Echec de renommage de "%s" en "%s", faites le � la main : \nMessage matlab : %s', this.nomFic, nomFicNew, message);
    strUS = sprintf('It was impossible to rename "%s" in "%s", do it manualy : \nMatlab message : %s', this.nomFic, nomFicNew, message);
    my_warndlg(Lang(strFR,strUS), 0, 'Tag', 'ImpossibleDeRenommerCeFichier');
end


if status
    strFR = sprintf('Le fichier "%s" a �t� renomm� en "%s"', this.nomFic, nomFicNew);
    strUS = sprintf('File "%s" was renamed as "%s"', this.nomFic, nomFicNew);
    my_warndlg(Lang(strFR,strUS), 0, 'Tag', 'ImpossibleDeRenommerCeFichier');
else
    strFR = sprintf('Echec de renommage de "%s" en "%s", faites le � la main : \nMessage matlab : %s', this.nomFic, nomFicNew, message);
    strUS = sprintf('It was impossible to rename "%s" in "%s", do it manualy : \nMatlab message : %s', this.nomFic, nomFicNew, message);
    my_warndlg(Lang(strFR,strUS), 0, 'Tag', 'ImpossibleDeRenommerCeFichier');
end
end


function nomFicOut = saucissonne_bin_unitaire(this, NbPings, tCoupure, subDatagram)

nomFicOut = {};

%% Check the number of elements

flag = checkIfOneElement(length(this));
if ~flag
    return
end

%%

if isa(tCoupure, 'cl_time')
    tCoupure = tCoupure.timeMat;
end

[nomDir, nomFic, ext] = fileparts(this.nomFic);
typeFile = 'SDF';

%% Lecture du fichier d'index

[flag, Data] = SSc_ReadDatagrams(nomFic, 'SSc_FileIndex');
if ~flag
    return
end

Position       = Data.DatagramPosition;
Taille         = double(Data.DatagramLength);
% PingCounter    = Data.PingCounter;
TypeOfDatagram = Data.TypeOfDatagram;
t              = Data.Time.timeMat;

% On d�coupe sur la base des datagrammes de Bathy ou de WCD.
if ~isempty(NbPings)
    T = unique(t);
    tCoupure = T(NbPings:NbPings:end);
end

% if isempty(NbPings) % Cas d'un fichier de d�coupe par heure
% On supprime les points de d�coupe qui sont en dehors du fichier
sub = (tCoupure < min(t(:,1)));
tCoupure(sub) = [];
sub = (tCoupure >= max(t(:,1)));
tCoupure(sub) = [];


sub          = [subDatagram{:,4}];
selectedCode = [subDatagram{sub,2}];
if isempty(selectedCode)
    strFR = sprintf('Aucun datagramme de s�lectionner : pas de cr�ation de fichiers splitt�s.');
    strUS = sprintf('None datagram selected : no split files creation.');
    my_warndlg(Lang(strFR,strUS), 0);
    return;
end

nbCoupures = length(tCoupure);
tCoupure   = sort(tCoupure);
index      = ones(size(t,1),1) * (nbCoupures+1);

for k=length(tCoupure):-1:1
    sub = (t(:,1) <= tCoupure(k));
    index(sub) = k;
end

% %% V�rification pour que des pings d'un sondeur dual ne se retrouvent pas s�par�s dans 2 fichiers s�par�s
% 
% listeDatagramsMBES = [3000 3001 5000 5001 7000 7001];
% 
% for k=1:length(listeDatagramsMBES)
%     sub             = (TypeOfDatagram == listeDatagramsMBES(k));
%     PC              = PingCounter(sub);
%     PCIndex         = index(sub);
%     subChangement   = find(diff(PCIndex) == 1);
%     for k=1:length(subChangement)
%         if length(unique(PC(subChangement:subChangement+1))) == 1
%             PCIndex(subChangement:subChangement+1) = PCIndex(subChangement+1);
%         end
%     end
%     index(sub) = PCIndex;
% end
% 
% plot_position(this)

%% D�codage du fichier de donn�es

% % % % typeIndian = getIndianSDF(this.nomFic);
typeIndian = 'l';
[fidIn, message] = fopen(this.nomFic, 'r', typeIndian);
if fidIn < 0
    my_warndlg(message, 1);
    return
end

for k=1:max(index)
    if isempty(tCoupure)
        % Simple filtrage des datagrammes.
        nomFicOut{k} = fullfile(nomDir, [nomFic '_reduced' ext]); %#ok<AGROW>
    else
        % Splittage des fichiers.
        nomFicOut{k} = fullfile(nomDir, [nomFic '_' num2str(k,'%02d') ext]); %#ok<AGROW>
    end
    fidOut(k) = fopen(nomFicOut{k}, 'w+'); %#ok<AGROW>
    [nomDir2, nomFic2] = fileparts(nomFicOut{k});
    nomsSsc = fullfile(nomDir2, 'SonarScope', nomFic2);
    if exist(nomsSsc, 'dir')
        try
            if ~strcmp(typeFile, 'WCD')
                rmdir(nomsSsc, 's')
                if exist(nomsSsc, 'dir')
                    strFR = sprintf('"%s" n''a pas pu �tre supprim�.', nomsSsc);
                    strUS = sprintf('"%s" could not be suppressed.', nomsSsc);
                    my_warndlg(Lang(strFR,strUS), 0);
                    flag = 0;
                end
            end
        catch %#ok<CTCH>
            strFR = sprintf('Le r�pertoire"%s" n''a pas pu �tre supprim�, on arr�te tout.', nomsSsc);
            strUS = sprintf('Folder "%s" could not be suppressed. Nothing will be done before, please solve the problem before.', nomsSsc);
            my_warndlg(Lang(strFR,strUS), 0);
            flag = 0;
        end
    end
end
if ~flag
    return
end

nomFicAuDela = fullfile(nomDir, [nomFic '_' num2str(k+1,'%02d') ext]);
if exist(nomFicAuDela, 'file')
    strFR = sprintf('ATTENTION : des fichiers existent sur le r�pertoire avec des num�ros supp�rieurs � ceux qui vont �tre g�n�r�s. Cela risque de semer la confusion lors des traitements ult�rieurs. Je vous conseille de les supprimer � partir du fichier "%s".', nomFicAuDela);
    strUS = sprintf('WARNING : some files exist with numbers supeperior to the ones you are going to create. I recommand that you supress them beginning from "%s"', nomFicAuDela);
    my_warndlg(Lang(strFR,strUS), 0);
end

%% Traitement du fichier

% my_warndlg('Beware : for speed reason, I do not copy "Clock datagrams" and "Central beams echogram" in splited files', 0, ...
%     'Tag', 'BewaredDoNotCopyClock')

str = sprintf('Loading file %s', nomFic);
N   = length(Position);
hw  = create_waitbar(str, 'N', N);
for k1=1:N
    my_waitbar(k1, N, hw);
    % On ne sauvegarde que les datagrammes s�lectionn�s.
    pppp = find(selectedCode == TypeOfDatagram(k1),1);
    if ~isempty(pppp)
        switch TypeOfDatagram(k1)
            
            % Depth, Raw range and beam angle, SeabedImage, Central beams
            % echogram, quality factor,
            case {3001}
                fseek(fidIn, Position(k1), 'bof');
                X = fread(fidIn, Taille(k1), 'uchar');
                fwrite(fidOut(index(k1)), X, 'uchar');
                
                %
            otherwise
                strFR = sprintf('TypeOfDatagram "%d" non identifi� dans "saucissonne_bin", signalez ce message � sonarscope@ifremer.fr', TypeOfDatagram(k1));
                strUS =  sprintf('TypeOfDatagram "%d" not identified in "saucissonne_bin", please report to sonarscope@ifremer.fr', TypeOfDatagram(k1));
                my_warndlg(Lang(strFR,strUS), 0, 'Tag', 'DatagrammeNonIdentifie');
                fseek(fidIn, Position(k1), 'bof');
                X = fread(fidIn, Taille(k1), 'uchar');
                for k2=1:max(index)
                    fwrite(fidOut(index(k2)), X, 'uchar');
                end
        end
    end
end
my_close(hw)
fclose(fidIn);
for k=1:max(index)
    fclose(fidOut(k));
end
end
