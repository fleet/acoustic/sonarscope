function flag = save_signal(this, NomLayer, X)

flag = existCacheNetcdf(this.nomFic);
if flag
    flag = save_signalNetcdf(this, NomLayer, X);
else
    flag = save_signalXMLBin(this, NomLayer, X);
end


function flag = save_signalXMLBin(this, NomLayer, X)

[nomDir, nom] = fileparts(this.nomFic);
nomFicXml = fullfile(nomDir, 'SonarScope', nom, ['Ssc_' this.PageVersion '.xml']);
if ~exist(nomFicXml, 'file')
    flag = 0;
    return
end

XML = xml_read(nomFicXml);
nomDirRacine = fullfile(nomDir, 'SonarScope', nom);

k = find(strcmp({XML.Signals.Name}, NomLayer));
if isempty(k)
    str1 = sprintf('Signal "%s" non trouv� dans "%s', NomLayer, nomFicXml);
    str2 = 'TODO';
    my_warndlg(Lang(str1,str2), 1);
    return
end

SaveOriginalSignalIfNotAlreadySaved(nomDirRacine, XML.Signals(k));

flag = writeSignal(nomDirRacine, XML.Signals(k), X);


function flag = save_signalNetcdf(this, NomLayer, Val)

[nomDir, nom] = fileparts(this.nomFic);
nomFicNc = fullfile(nomDir, 'SonarScope', [nom, '.nc']);
if ~exist(nomFicNc, 'file')
    flag = 0;
    return
end

grpName = this.PageVersion;

ncID  = netcdf.open(nomFicNc, 'WRITE');
grpID = netcdf.inqNcid(ncID, grpName);
varID = netcdf.inqVarID(grpID, NomLayer);
NetcdfUtils.putVarVal(grpID, varID, Val, 1);
netcdf.close(ncID);

flag = 1;
