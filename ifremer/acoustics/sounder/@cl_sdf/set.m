% Acces en ecriture des proprietes d'une instance
%
% Syntax
%   b = set(a, ...)
%
% Input Arguments
%   a : Une instance de la classe cl_simrad_all
%
% Name-Value Pair Arguments
%   nomFic : Nom(s) du fichier .sdf
%
% Output Arguments
%   [] : Sauve l'instance dans a
%   b  : Instance de la classe
%
% Examples
%   nomFic = getNomFicDatabase('SSS_091009092600.sdf');
%   a = cl_sdf('nomFic', nomFic);
%   a
%   a.nomFic
%   a.PageVersion
%   a.nbPings
%   a.nbCol
%   H1 = get(a, 'heading');
%   a = set(a, 'heading', H1-360);
%   H2 = get(a, 'heading');
%   figure; plot(H1, 'b'); grid on; hold on; plot(H2, 'r')
%
% See also cl_sdf cl_sdf/get Authors
% Authors : MVN
% ----------------------------------------------------------------------------

function varargout = set(this, varargin)

global useCacheNetcdf %#ok<GVMIS>

[varargin, nomFic]             = getPropertyValue(varargin, 'nomFic',          []);
[varargin, isUsingParfor]      = getPropertyValue(varargin, 'isUsingParfor',   0);
[varargin, useCacheNetcdfHere] = getPropertyValue(varargin, 'useCacheNetcdf',  useCacheNetcdf);

if ~isempty(nomFic)
    [flag, PageVersion] = SDF.Cache.sdf2ssc(nomFic, 'Display', 0, 'isUsingParfor', isUsingParfor, 'useCacheNetcdf', useCacheNetcdfHere);
    
    nomFic = windows_replaceVolumeLetter(nomFic);
    this.nomFic = nomFic;
    this.PageVersion = PageVersion;
    
    if ~flag
        varargout{1} = [];
        return
    end
end

N = length(varargin);
if N > 1
    [flag, XML] = SSc_ReadDatagrams(this.nomFic, 'Ssc_V3001', 'XMLOnly', 1);
    if flag
        Fields = fieldnames(XML.Dimensions);
        while N > 1
            
            % Test si par hazard on voulait modifier une dimension
            
            for k=1:length(Fields)
                [varargin, X] = getPropertyValue(varargin, Fields{k}, []);
                N = length(varargin);
                if ~isempty(X)
                    str1 = 'On ne peut pas modifier une dimension.';
                    str2 = 'One cannot modify a dimension.';
                    my_warndlg(Lang(str1,str2), 1);
                    break
                end
            end
            
            % Test si c'est un signal que l'on veut mettre � jour
            
            for k=1:length(XML.Signals)
                [varargin, X] = getPropertyValue(varargin, XML.Signals(k).Name, []);
                N = length(varargin);
                if ~isempty(X)
                    flag = save_signal(this, XML.Signals(k).Name, X); %#ok<NASGU>
                    break
                end
            end
            
            % Test si c'est une image que l'on veut mettre � jour
            
            for k=1:length(XML.Images)
                [varargin, X] = getPropertyValue(varargin, XML.Images(k).Name, []);
                N = length(varargin);
                if ~isempty(X)
                    flag = save_signal(this, XML.Images(k).Name, X); %#ok<NASGU>
                    break
                end
            end
        end
    end
end

%% Sortie

if nargout == 0
    assignin('caller', inputname(1), this);
else
    varargout{1} = this;
end
