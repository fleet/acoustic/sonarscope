function setHeightInSamples(this, Signal)

speedSound     = get(this, 'speedSound');
sampleFreq     = get(this, 'sampleFreq');

altitudeMeters = Signal .* speedSound ./ (2 * sampleFreq);
set(this, 'altitudeMeters', altitudeMeters);
