% Visualisation du contenu des datagrams "Seabed Image" contenus dans un fichier .sdf
%
% Syntax
%   [b,S] = view_Image(a, ...);
%
% Input Arguments
%   a : Instance de cl_sdf
%
% Name-Value Pair Arguments
%   Carto        :
%   memeReponses :
%   ListeLayers  :
%
% Output Arguments
%   b            : Instance de cli_image
%   Carto        :
%   memeReponses :
%
% Examples
%   nomFic = getNomFicDatabase('getNomFicDatabase('???');
%   a = cl_sdf('nomFic', nomFic);
%   b = view_Image(a);
%   SonarScope(b);
%
%   b = view_Image(a, 'subl', 1:100);
%   SonarScope(b);
%
% See also cl_sdf cl_sdf/plot_position cl_sdf/plot_nav Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [flag, b, Carto, memeReponses] = view_Image(this, varargin)

persistent persistent_ListeLayers

[varargin, SourceHeading] = getPropertyValue(varargin, 'SourceHeading', 1);
[varargin, Carto]         = getPropertyValue(varargin, 'Carto',         []);
[varargin, memeReponses]  = getPropertyValue(varargin, 'memeReponses',  false);
[varargin, ListeLayers]   = getPropertyValue(varargin, 'ListeLayers',   []); %#ok<ASGLU>

b = [];

%% Check the number of elements

flag = checkIfOneElement(length(this));
if ~flag
    return
end

%% Lecture de la donn�e

if isempty(ListeLayers)
    if ~memeReponses
        str = {'132kHz'; '445kHz'; 'SBD'};
        ListeLayers = my_listdlg('Layers :', str, 'InitialValue', 1);
        if isempty(ListeLayers)
            flag = 0;
            return
        end
        persistent_ListeLayers = ListeLayers;
    else
        ListeLayers = persistent_ListeLayers;
    end
end

[flag, b, Carto, OrigineNavigation] = read_Image(this, 'SelecImage', ListeLayers, 'Carto', Carto, 'SourceHeading', SourceHeading); %#ok<ASGLU>
if ~flag
    return
end

%% Edition de l'objet cli_image

if nargout == 0
    SonarScope(b);
end

flag = 1;
