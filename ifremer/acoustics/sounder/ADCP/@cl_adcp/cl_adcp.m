% Constructeur de cl_adcp (Fichier de donn�es ADCP, sondes Courant-Doppler contenue dans une .odv)
%
% Syntax
%   a = cl_odv(...)
% 
% Name-Value Pair Arguments
%   dataFileName : Nom(s) du fichier .odv
%  
% Output Arguments 
%   a : Instance de cl_adcp
%
% Examples
%   [flag, dataFileName] = FtpUtils.getSScDemoFile('ADCP_38khz_Malaga_Brest__006_allPofil.odv', 'OutputDir', 'D:\Temp\ExData')
%   a = cl_adcp('nomFic', dataFileName);
%
% See also cl_hac Authors
% Authors : GLU
% ----------------------------------------------------------------------------

function this = cl_adcp(varargin)

%% Definition de la structure

this.nomFic = '';

%% Creation de l'instance

this = class(this, 'cl_adcp');

%% On regarde si l'objet a ete cree uniquement pour atteindre une methode de la classe

if (nargin == 1) && isempty(varargin{1})
    return
end

%% Initialisation des valeurs transmises lors de l'appel au constructeur

this = set(this, varargin{:});
