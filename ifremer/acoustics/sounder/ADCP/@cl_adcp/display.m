% Representation externe d'une instance sur le terminal
%
% Syntax
%   display(this)
%
% Input Arguments 
%   this : une instance de cl_adcp
%
% Examples
%   nomFic = fullfile(  'F:\SonarScopeData\From C.Vrignaud\Gibraltar\Data\ADCP', ...
%                       'ADCP_38khz_Malaga_Brest__005.odv');
%   a = cl_adcp('nomFic', nomFic)
%   display(a)
%   a
%
% See also cl_adcp cl_adcp/char Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function display(this)

s = whos('this');
sz = s.size;

fprintf('Name\tSize\tClass\n');
fprintf('%s\t%s\t%s\n', inputname(1), num2strCode(sz), s.class);

disp(char(this));
