% Acces en lecture des proprietes d'une instance
%
% Syntax
%   [...] = get(this, ...)
% 
% Input Arguments 
%   this : Une instance de la classe cl_adcp
%
% Name-Value Pair Arguments
%   nomFic : Nom du fichier .adcp
%
% Output Arguments
%   PropertyValues associees aux PropertyNames passes en argument
% 
% Examples
%   nomFic = fullfile(  'F:\SonarScopeData\adcp From JMA\Marmesonet\SDS-auv_Marmesonet1', 
%                       'AUV001_PR04_proc.seg');
%   a = cl_adcp('nomFic', nomFic)
%   nomFic = get(a, 'nomFic')
%
% See also cl_adcp cl_adcp/char Authors
% Authors : GLU
% ----------------------------------------------------------------------------

function varargout = get(this, varargin)

%% Boucle de parcours des arguments

varargout = {};
for i=1:length(varargin)
    switch varargin{i}
        case 'nomFic'
            varargout{end+1} = this.nomFic; %#ok<AGROW>

% %         case 'DataName'
% %             varargout{end+1} = this.dataName;
            
        otherwise
            w = sprintf('cl_adcp/get %s non pris en compte', varargin{i});
            my_warndlg(w, 0);
    end
end
