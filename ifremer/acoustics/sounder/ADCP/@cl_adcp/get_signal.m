%   [flag, dataFileName] = FtpUtils.getSScDemoFile('ADCP_38khz_Malaga_Brest__006_allPofil.odv', 'OutputDir', 'D:\Temp\ExData')
%   a = cl_adcp('nomFic', dataFileName);
%
%  [flag, Signal] = get_signal(a, 'Time')

function [flag, Signal] = get_signal(this, nomSignal, varargin)

[varargin, DataODV] = getPropertyValue(varargin, 'Data', []); %#ok<ASGLU>

Signal = [];

if isempty(DataODV)
    [flag, DataODV] = SSc_ReadDatagrams(this.nomFic, 'Ssc_Data', 'Memmapfile', -1);
    if ~flag
        return
    end
end

switch nomSignal
    case 'Time'
        [flag, Time] = get_time(this, 'Data', DataODV);
        if ~flag
            return
        end
        Signal.Time  = Time;
        Signal.Value = Time.timeMat';
        Signal.Unit  = 'Time';
        
    case 'Date'
        [flag, Time] = get_time(this, 'Data', DataODV);
        if ~flag
            return
        end
        Signal.Time  = Time;
        Signal.Value = floor(Time.timeMat');
        Signal.Unit  = 'Date';
        
    case 'Hour'
        [flag, Time] = get_time(this, 'Data', DataODV);
        if ~flag
            return
        end
        Signal.Time  = Time;
        Signal.Value = (Time.timeMat' - floor(Time.timeMat')) * 24;
        Signal.Unit  = 'Hour';
        
    case 'Resolution'
        [flag, Time] = get_time(this, 'Data', DataODV);
        if ~flag
            return
        end
        resol = (DataODV.ZBotDepth -  DataODV.ZTopDepth) / DataODV.Dimensions.ns;
        
        Signal.Time  = Time;
        Signal.Value = resol;
        Signal.Unit  = 'm';
        
    case 'ProfileNumber'
        [flag, Time] = get_time(this, 'Data', DataODV);
        if ~flag
            return
        end
        Signal.Time  = Time;
        Signal.Value = 1:DataODV.Dimensions.NbProfiles;
        Signal.Unit  = '';
        
    case 'Immersion'
        [flag, Time] = get_time(this, 'Data', DataODV);
        if ~flag
            return
        end
        Signal.Time  = Time;
        Signal.Value = DataODV.ZTopDepth;
        Signal.Unit  = '';
        
    case 'Depth'
        [flag, Time] = get_time(this, 'Data', DataODV);
        if ~flag
            return
        end
        Signal.Time  = Time;
        Signal.Value = DataODV.ZBotDepth;
        Signal.Unit  = '';
        
        
    otherwise
        str1 = sprintf('%s pas encore preogrtammé dans cl_adcpy/get_signal', nomSignal);
        str2 = 'TODO';
        my_warndlg(Lang(str1,str2), 1);
        flag = 0;
end
