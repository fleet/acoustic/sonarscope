%   [flag, dataFileName] = FtpUtils.getSScDemoFile('ADCP_38khz_Malaga_Brest__006_allPofil.odv', 'OutputDir', 'D:\Temp\ExData')
%   a = cl_adcp('nomFic', dataFileName);
%
%  [flag, Signal] = get_time(a')

function [flag, Time] = get_time(this, varargin)

[varargin, DataODV] = getPropertyValue(varargin, 'Data', []); %#ok<ASGLU>

if isempty(DataODV)
    [flag, DataODV] = SSc_ReadDatagrams(this.nomFic, 'Ssc_Data', 'Memmapfile', -1);
    if ~flag
        Time = [];
        return
    end
end

%% Traitement des Dates

pppp = DataODV.strDate;
for k=1:numel(pppp)
    x = sscanf(pppp{k}, '%d/%d/%d');
    Jour 	= x(2);
    Mois 	= x(1);
    Annee	= x(3);
    date(k) = dayJma2Ifr(Jour, Mois, Annee); %#ok<AGROW>
end

pppp = DataODV.strHour;
for k=1:numel(pppp)
    x = sscanf(pppp{k}, '%d:%d:%d');
    Heure 	 = x(1)*3600;
    Min 	 = x(2)*60;
    Sec      = x(3);
    heure(k) = (Heure + Min + Sec) * 1000; %#ok<AGROW> % en millisecs depuis Minuit.
end
Time = cl_time('timeIfr', date, heure);

flag = 1;
