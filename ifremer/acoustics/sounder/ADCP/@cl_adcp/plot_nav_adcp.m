%   [flag, dataFileName] = FtpUtils.getSScDemoFile('ADCP_38khz_Malaga_Brest__006_allPofil.odv', 'OutputDir', 'D:\Temp\ExData')
%   a = cl_adcp('nomFic', dataFileName);
%
%  [Fig, Carto] = plot_nav_adcp(cl_adcp([]), dataFileName)

function [Fig, Carto] = plot_nav_adcp(~, dataFileName, varargin)

[varargin, Carto] = getPropertyValue(varargin, 'Carto', []);
[varargin, Fig]   = getPropertyValue(varargin, 'Fig',   []); %#ok<ASGLU>
% [varargin, EtapeUnique] = getPropertyValue(varargin, 'EtapeUnique', 1);

%% Cr�ation de la fen�tre avec les menu et callbacks sp�cialis�s pour la nav

Fig = plot_navigation_Figure(Fig);

%% Traitement des fichiers

if ~iscell(dataFileName)
    dataFileName = {dataFileName};
end

NbFic = length(dataFileName);
str1 = 'Trac� de la navigation des fichiers adcp';
str2 = 'Plot adcp navigation';
hw = create_waitbar(Lang(str1,str2), 'N', NbFic);
for k=1:NbFic
    my_waitbar(k, NbFic, hw);
    
    % Lecture des datagrams de navigation
    
    a = cl_adcp('nomFic', dataFileName{k});
    if isempty(a)
        continue
    end
    
    [flag, DataODV] = SSc_ReadDatagrams(a.nomFic, 'Ssc_Data', 'Memmapfile', -1);
    if ~flag
        str1 = sprintf('La navigation n''a pas pu �tre d�cod�e dans le fichier "%s".', dataFileName{k});
        str2 = sprintf('The navigation could not be read in file "%s".', dataFileName{k});
        my_warndlg(Lang(str1,str2), 0, 'Tag', 'ErreurLectureNav_adcp', 'TimeDelay', 60);
        continue
    end
    
    [flag, TimeNav] = get_time(a, 'Data', DataODV);
    if ~flag || isempty(DataODV.Latitude)
        continue
    end
    
    Longitude  = DataODV.Longitude(:,1);
    Latitude   = DataODV.Latitude(:,1);
    SonarTime  = TimeNav;
    Heading    = [];
    plot_navigation(dataFileName{k}, Fig, Longitude, Latitude, SonarTime.timeMat, Heading);
end
my_close(hw, 'MsgEnd');
