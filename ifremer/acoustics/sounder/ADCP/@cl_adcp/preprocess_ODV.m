function preprocess_ODV(~, nomFic)

if ~iscell(nomFic)
    nomFic = {nomFic};
end

% profile on
tic
str1 = 'Traitement des fichiers ODV en cours';
str2 = 'Processing ODV files';
N = length(nomFic);
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    fprintf('%d/%d - %s', k, N, nomFic{k});
    try
        a = cl_adcp('nomFic', nomFic{k}); %#ok<NASGU>
        fprintf(' : OK\n');
    catch ME
        ErrorReport = getReport(ME) %#ok<NOPRT>
        SendEmailSupport(ErrorReport)
        fprintf(' : ERROR\n');
    end
end
my_close(hw, 'MsgEnd')

% profile report
toc
