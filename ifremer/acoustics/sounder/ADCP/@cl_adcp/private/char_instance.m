% Transformation en chaine de caracteres d'une instance
%
% Syntax
%   str = char_instance(a)
%
% Input Arguments
%   a : instance de cl_image
%
% Name-Value Pair Arguments
%   InfoSonar  : 1=Information de l'image uniquement
%                2=Info de l'information sonar uniquement
%                3=Display de toute l'information disponible (image+sonar) : (3 par defaut)
%
% Output Arguments
%   str : une chaine de caracteres representative de l instance
%
% Examples
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function str = char_instance(this, varargin)

if ~isempty(this.nomFic)
    str = [];
    str{end+1} = sprintf('NomFic                <-> %s', this.nomFic);
    str{end+1} = sprintf('DataFormat            <-> %s', 'Open Data File format');
    
    [flag, DataODV] = SSc_ReadDatagrams(this.nomFic, 'Ssc_Data', 'Memmapfile', -1);
    if flag
        listeFields = fieldnames(DataODV);
        N = length(listeFields);
        for k=1:N
            X = DataODV.(listeFields{k});
            switch class(X)
                case 'struct'
                case 'cell'
                case 'char'
                    str{end+1} = sprintf('\t\tData.%s\t: %s', listeFields{k}, X); %#ok<AGROW>
                case {'single'; 'double'}
                    str{end+1} = sprintf('\t\tData.%s\t: %s', listeFields{k}, num2strCode(X)); %#ok<AGROW>
                case {'cl_memmapfile'}
                    str{end+1} = sprintf('\t\tData.%s\t: %s', listeFields{k}, num2strCode(size(X))); %#ok<AGROW>
                otherwise
                    class(X)
            end
        end
    end

    % ---------------------------
    % Concatenation en une chaine

    str = cell2str(str);
else
    str = [];
end
