%   [flag, dataFileName] = FtpUtils.getSScDemoFile('ADCP_38khz_Malaga_Brest__006_allPofil.odv', 'OutputDir', 'D:\Temp\ExData')
%   a = cl_adcp('nomFic', dataFileName);
%
%  [flag, Data] = read_position(a)

function [flag, Data] = read_position(this, varargin)

[varargin, DataODV] = getPropertyValue(varargin, 'DataODV', []); %#ok<ASGLU>

if isempty(DataODV)
    [flag, DataODV] = SSc_ReadDatagrams(this.nomFic, 'Ssc_Data', 'Memmapfile', -1);
    if ~flag
        Data = [];
        return
    end
end

Data.Latitude  = DataODV.Latitude;
Data.Longitude = DataODV.Longitude;

[flag, Time] = get_time(this, 'Data', DataODV);
if ~flag
    return
end
Data.Time = Time;
