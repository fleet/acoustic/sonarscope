% TODO : il faudrait trouver un moyen de mutualiser cette fonction entre
% classes diff�rentes (m�ta classes ???)
function flag = save_ProcessingParameters(this, nomFicXml, nomVar, Var)

%% Nom du fichier

[nomDir, nom] = fileparts(this.nomFic);
nomFicXml = fullfile(nomDir, 'SonarScope', nom, nomFicXml);

%% Lecture si il existe d�j�

if exist(nomFicXml, 'file')
    XML = xml_read(nomFicXml);
else
    XML.Signature1 = 'IFREMER';
    XML.Signature2 = 'SonarScope';
    XML.Signature3 = 'ODV';
    XML.Signature4 = 'ProcessingParameters';
end

%% Attribution de la variable

XML.(nomVar) = Var;

%% Ecriture du fichier

xml_write(nomFicXml, XML);
flag = exist(nomFicXml, 'file');
if ~flag
    messageErreur(nomFicXml)
    return
end
% edit(nomFicXml)
