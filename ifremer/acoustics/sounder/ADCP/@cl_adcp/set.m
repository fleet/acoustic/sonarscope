% Acces en ecriture des proprietes d'une instance
%
% Syntax
%   b = set(a, ...)
%
% Input Arguments
%   a : Une instance de la classe cl_adcp
%
% Name-Value Pair Arguments
%   nomFic : Nom(s) du fichier .odv
%
% Output Arguments
%   [] : Sauve l'instance dans a
%   b  : Instance de la classe
%
% Examples
%   nomFic = fullfile(  'F:\SonarScopeData\From C.Vrignaud\Gibraltar\Data\ADCP', ...
%               'ADCP_38khz_Malaga_Brest__005.odv');
%
%   a = cl_adcp
%   a = set(a, 'nomFic', nomFic)
%
% See also cl_adcp cl_adcp/get Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function varargout = set(this, varargin)

[varargin, nomFic]         = getPropertyValue(varargin, 'nomFic',     []);
[varargin, flagMemmapfile] = getPropertyValue(varargin, 'Memmapfile', 1); %#ok<ASGLU>

if ~isempty(nomFic)
    this.nomFic = nomFic;
    flag = odv2ssc(nomFic);
    if ~flag
        varargout{1} = [];
        return
    end
end

%% Sortie

if nargout == 0
    assignin('caller', inputname(1), this);
else
    varargout{1} = this;
end
