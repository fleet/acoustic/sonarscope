function flag = set_time(this, Time, varargin)

[varargin, DataODV] = getPropertyValue(varargin, 'DataODV', []); %#ok<ASGLU>

if isempty(DataODV)
    [flag, DataODV] = SSc_ReadDatagrams(this.nomFic, 'Ssc_Data', 'Memmapfile', -1);
    if ~flag
        return
    end
end

n = length(DataODV.Date);
flag = (length(Time.timeMat) == n);
if ~flag
    str1 = 'Mise � jour de l''heure impossible dans cl_adcp/set_time car les dimensions sont diff�rentes';
    str2 = 'TODO';
    my_warndlg(Lang(str1,str2), 1);
    return
end

[Jour, Mois, Annee] = dayIfr2Jma(Time.date);
[Heure, Minute, Seconde] = hourIfr2Hms(Time.heure);


pppp = DataODV.Hour;
for i=1:numel(pppp)
    Hour{i} = [num2str(Heure(i)) ':' num2str(Minute(i)) ':' num2str(Seconde(i))]; %#ok<AGROW>
    Date{i} = [num2str(Mois(i)) '/' num2str(Jour(i)) '/' num2str(Annee(i))]; %#ok<AGROW>
end

flag = save_string(this, 'Date', Date);
if ~flag
    return
end
flag = save_string(this, 'Hour', Hour);
if ~flag
    return
end

