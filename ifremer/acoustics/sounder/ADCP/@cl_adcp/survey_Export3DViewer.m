function flag = survey_Export3DViewer(~, listeFicODV, nomDirOut, OrigineCLim, CLim, ColormapIndex, Tag, ...
    IdentPartData, TailleMax, Video)

Carto = [];

N = length(listeFicODV);
str1 = 'Export des fichiers ODV dans GLOBE';
str2 = 'Exporting ODV files to GLOBE';
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
     my_waitbar(k, N, hw)
    [flag, Carto] = survey_Export3DViewer_unitaire(listeFicODV{k}, nomDirOut, OrigineCLim, CLim, ColormapIndex, Tag, Carto, ...
        IdentPartData, TailleMax, Video);
end
my_close(hw, 'MsgEnd')



function [flag, Carto] = survey_Export3DViewer_unitaire(nomFicODV, nomDirOut, OrigineCLim, CLim, ColormapIndex, Tag, Carto, ...
    IdentPartData, TailleMax, Video)

%% Cr�ation du r�pertoire

[~, nomDirBin] = fileparts(nomFicODV);
strIdentPartData = {'Dir'; 'Mag'; 'EchoInt'};
if isempty(Tag)
    nomDirXml = fullfile(nomDirOut, [nomDirBin '_' strIdentPartData{IdentPartData}]);
else
    nomDirXml = fullfile(nomDirOut, [nomDirBin '_' strIdentPartData{IdentPartData} '_' Tag]);
end


% nomDirRoot = fileparts(nomDirXml);
if ~exist(nomDirXml, 'dir')
    flag = mkdir(nomDirXml);
    if ~flag
        messageErreurFichier(nomDirXml, 'WriteFailure');
        return
    end
end
NomFicXML = [nomDirXml '.xml'];

%% Instance cl_ODV

a = cl_adcp('nomFic', nomFicODV);

%% Lecture de l''image

[flag, b, Carto] = import_ODV(nomFicODV, 'Carto', Carto, 'DataToDisplay', IdentPartData);
if ~flag
    return
end

if OrigineCLim == 2
    [flag, CLimXML] = read_ProcessingParameters(a, 'Ssc_ProcessingParameters.xml', 'CLim');
    if flag
        if isempty(CLimXML)
            str1 = sprintf('Le fichier "Ssc_ProcessingParameters.xml" associ� � "%s" ne contient pas de balise "CLim". On utilise le rehaussement de contraste par d�faut de l''image.', nomFicODV);
            str2 = sprintf('"Ssc_ProcessingParameters.xml" associated to "%s" does not contain the marker "CLim". I will use the default contrast enhancement of the image.', nomFicODV);
            my_warndlg(Lang(str1,str2), 0, 'TimeDelay', 60);
            CLim = b.CLim;
        else
            CLim = CLimXML;
        end
    else
        str1 = sprintf('Le fichier "Ssc_ProcessingParameters.xml" n''a pas encore �t� g�n�r� pour "%s". On utilise le rehaussement de contraste par d�faut de l''image.', nomFicODV);
        str2 = sprintf('"Ssc_ProcessingParameters.xml" has not been generated for "%s". I will use the default contrast enhancement of the image.', nomFicODV);
        my_warndlg(Lang(str1,str2), 0, 'TimeDelay', 60);
        CLim = b.CLim;
    end
end

% R�cup�ration de l'image.
nbCol     = b.nbColumns;
subx      = 1:nbCol;
[a, flag] = extraction(b, 'subx', subx);
if ~flag
    return
end

a.CLim          = CLim;
a.ColormapIndex = ColormapIndex;
a.Video         = Video;
% figure; imagesc(get(a, 'Image'));
flag = adcp_Export3DV_ImagesAlongNavigation_ByBlocks(a, NomFicXML, TailleMax, 'subx', subx, 'Mute');
