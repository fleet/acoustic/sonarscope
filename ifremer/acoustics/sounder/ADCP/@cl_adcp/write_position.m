function flag = write_position(this, Data, varargin)

[varargin, DataODV] = getPropertyValue(varargin, 'DataODV', []); %#ok<ASGLU>

if isempty(DataODV)
    [flag, DataODV] = SSc_ReadDatagrams(this.nomFic, 'Ssc_Data', 'Memmapfile', -1);
    if ~flag
        return
    end
end

n = length(DataODV.Latitude);
flag = (length(Data.Latitude) == n);
if ~flag
    str1 = 'Mise � jour de l''heure impossible dans cl_odv/set_time car les dimensions sont diff�rentes';
    str2 = 'TODO';
    my_warndlg(Lang(str1,str2), 1);
    return
end


Longitude = Data.Longitude;
Latitude  = Data.Latitude;

%% Ecriture des signaux

flag = save_signal(this, 'Longitude', Longitude);
if ~flag
    return
end

flag = save_signal(this, 'Latitude', Latitude);
if ~flag
    return
end
