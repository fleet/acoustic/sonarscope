% [flag, dataFileName] = FtpUtils.getSScDemoFile('ADCP_38khz_Malaga_Brest__006_allPofil.odv', 'OutputDir', 'D:\Temp\ExData')
%
% ODV_Cache2Netcdf(dataFileName)

function flag = ODV_Cache2Netcdf(dataFileName)

%% Cr�ation du fichier Netcdf

[flag, nomFicNc] = SScCacheNetcdfUtils.createSScNetcdfFile(dataFileName);
if ~flag
    return
end

%% Transfert des donn�es Ssc_Data

flag = SScCacheNetcdfUtils.Cache2Netcdf(nomFicNc, dataFileName, 'Data');
if ~flag
    return
end

%% The End

flag = 1;
