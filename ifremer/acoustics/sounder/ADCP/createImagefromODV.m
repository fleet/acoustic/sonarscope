function [flag, I] = createImagefromODV(Data, IndexData, ValToDisplay)
flag = 1;
% Tri des profils.
[b, m, ~] = unique(Data(:,IndexData.Station));
pppp      = diff(m);
maxPasZ   = max(pppp);
I         = nan(length(b), maxPasZ);
% Z       = nan(length(b), maxPasZ);

offset      = 0;
% Traitements des profils.
% Hypo : tous les profils commencent � ZMin.
for i=1:length(b)
    if i>1
        nbVal = m(i) - m(i-1);
    else
        nbVal = m(1);
    end
    sub = 1:nbVal;
    I(i,sub) = Data(sub + offset, IndexData.(ValToDisplay));
    % Z(i,sub) = Data(sub + offset, IndexData.Longitude);
    offset = offset + nbVal;
end

% A tester pour le cas o� les donn�es de profil sont �tal�s le long d'un
% profil vertical (ne commencent pas syst�matiquement � Zmin par exemple).

% % % % minZ        = min(Data(:,5));
% % % % maxZ        = max(Data(:,5));
% % % % ZReSample   = linspace(minZ, maxZ, maxPasZ);
% % % % offset      = 0;
% % % % I = [];
% % % % % Traitements des profils.
% % % % for i=1:length(b)
% % % %     if i>1
% % % %         nbVal = m(i) - m(i-1);
% % % %     else
% % % %         nbVal = m(1);
% % % %     end
% % % %     sub = 1:nbVal;
% % % %     % Cas du 1er profil
% % % %     pppp            = ZReSample;
% % % %     ZProfil         = Data(sub + offset, 5);
% % % %     ValProfil       = Data(sub + offset, 6);
% % % %     ValProfil2      = NaN(size(pppp,2), 1);
% % % %     for k=1:size(ZProfil,1)
% % % %         % Recherche du positionnement en hauteur du point de valeur.
% % % %         ind                 = find(pppp<=ZProfil(k), 1, 'last');
% % % %         % Allocation dans la nouvelle colonne.
% % % %         ValProfil2(ind)     = ValProfil(k);
% % % %     end
% % % %     offset = offset + nbVal;
% % % %     I = [I ValProfil2];
% % % % end
I = I';
% figure; imagesc(I)