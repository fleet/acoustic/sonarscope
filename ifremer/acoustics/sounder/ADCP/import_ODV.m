% Extraction d'un fichier ODV sous forme de Tuile Nasa World Wind.
% SYNTAXE :
%
% EXAMPLE sous DOS:
%   importation_XYZ2SSC3DV
%
%   [flag, dataFileName] = FtpUtils.getSScDemoFile('ADCP_38khz_Malaga_Brest__006_allPofil.odv', 'OutputDir', 'D:\Temp\ExData')
%   a = cl_adcp('nomFic', dataFileName);
%
%  [flag, b, Carto] = import_ODV(dataFileName)
%  SonarScope(b)
% -----------------------------------------------------------------------

function [flag, this, Carto] = import_ODV(dataFileName, varargin)

[varargin, Carto]         = getPropertyValue(varargin, 'Carto',         []);
[varargin, DataToDisplay] = getPropertyValue(varargin, 'DataToDisplay', []); %#ok<ASGLU>

flag = 0; %#ok<NASGU>
this = [];

%% Lecture du fichier

a = cl_adcp('nomFic', dataFileName, 'Memmapfile', 0);
if isempty(a)
    flag = 0;
    return
end

%% Traitement des N fichiers s�lectionn�s

% Importation des images
[~, ImageName, ~] = fileparts(dataFileName);

%% Lecture des donn�es g�n�rales

[flag, DataODV] = SSc_ReadDatagrams(dataFileName, 'Ssc_Data', 'Memmapfile', -1);
if ~flag
    return
end

%% Description du Sonar

SonarDescription = cl_sounder('Sonar.Ident', 29);

%% R�cup�ration du temps

[flag, Time] = get_time(a, 'Data', DataODV);
if ~flag
    return
end

%% R�cup�ration des num�ros de profile

[flag, ProfileNumber] = get_signal(a, 'ProfileNumber', 'Data', DataODV);
if ~flag
    return
end

%% R�cup�ration de l'immersion du 1er �chantillon

[flag, Immersion] = get_signal(a, 'Immersion', 'Data', DataODV);
if ~flag
    return
end

%% R�cup�ration de la profondeur du dernier �chantillon

[flag, Depth] = get_signal(a, 'Depth', 'Data', DataODV);
if ~flag
    return
end

%% R�cup�ration de la r�solution

[flag, Resolution] = get_signal(a, 'Resolution', 'Data', DataODV);
if ~flag
    return
end

[resol, m] = unique(Resolution.Value);
[~, im] = max(m);
resol = resol(im);
if length(resol) > 1
    str1 = 'Plusieurs valeurs de r�solution sont pr�sentes dans ce fichier. Je prends la valeur majoritaire : Ceci doit �tre am�lior�.';
    str2 = 'Several resolution are read in this file, I take the most represented, this must be improves..';
    my_warndlg(Lang(str1,str2), 0, 'Tag', 'PlusieursResolsDansFichier');
end


% Indices dans le fichier : dans l'ordre, Dir, Mag et Echo Int.
if isempty(DataToDisplay)
    str1 = 'Donn�e � afficher : ';
    str2 = 'Data to display : ';
    DataToDisplay = my_listdlg(Lang(str1,str2), {DataODV.Images(:).Name}, 'SelectionMode', 'Multiple');
end

tabImage = {DataODV.Images(:).Name};
this     = cl_image;
for k=1:numel(DataToDisplay)
    % R�cup�ration de l'image depuis les donn�es.
    Image = DataODV.(tabImage{DataToDisplay(k)})';
    Image = fliplr(Image);
    x = 1:size(Image,2);
    y = 1:size(Image,1);
    
    if ~flag
        return
    end
    
    this(k) = cl_image('Image', Image, ...
        'Name',           [ImageName '_' tabImage{k}], ...
        'Unit',                 DataODV.Images(DataToDisplay(k)).Unit, ...
        'x',                    x, ...
        'y',                    y, ...
        'XUnit',                '"', ...
        'YUnit',                'Ping', ...
        'ColormapIndex',        3, ...
        'DataType',             cl_image.indDataType('Unknown'), ...
        'TagSynchroX',          [ImageName '_' tabImage{k}], ...
        'TagSynchroY',          [ImageName '_' tabImage{k}], ...
        'GeometryType',         cl_image.indGeometryType('PingSamples'), ...
        'InitialFileName',      dataFileName, ...
        'InitialFileFormat',    'ODV', ...
        'SonarDescription',     SonarDescription);
    
    this(k) = set(this(k), 'SonarPingCounter',        ProfileNumber.Value');
    this(k) = set(this(k), 'SonarImmersion',          -Immersion.Value);
    this(k) = set(this(k), 'SonarFishLatitude',       DataODV.Latitude(:));
    this(k) = set(this(k), 'SonarFishLongitude',      DataODV.Longitude(:));
    this(k) = set(this(k), 'SonarHeight',             Depth.Value - Immersion.Value);
    
    this(k) = set(this(k), 'SonarTime', Time);
    if isempty(Carto)
        Carto = getDefinitionCarto(this(k));
        this(k) = set(this,  'Carto', Carto);
    end
    this(k) = update_Name(this(k));
    
    flag = 1;
    
end

flag = 1;

