%  [flag, dataFileName] = FtpUtils.getSScDemoFile('ADCP_38khz_Malaga_Brest__006_allPofil.odv', 'OutputDir', 'D:\Temp\ExData')
%  flag = odv2ssc(dataFileName)

function flag = odv2ssc(dataFileName)

FormatVersion  = 20120312;

nbOc = sizeFic(dataFileName);
if nbOc == 0
    flag = 0;
    return
end

%% G�n�ration des XML et des binaires

[nomDir, nomFicSeul] = fileparts(dataFileName);
nomDirRacine = fullfile(nomDir, 'SonarScope', nomFicSeul);

unzipSonarScopeCache(nomDirRacine);

if ~exist(nomDirRacine, 'dir')
    mkdir(nomDirRacine);
else
    flag = [];
    
    flag(end+1) = exist(fullfile(nomDirRacine, 'Ssc_Data.xml'), 'file');
    flag(end+1) = exist(fullfile(nomDirRacine, 'Ssc_Data'), 'dir');
    
    if all(flag) && (getFormatVersion(nomDirRacine) == FormatVersion)
        flag = 1;
        
        return
    end
end

msg = 'OK';
% Certains param�tres sont inspir�s de la lecture en ASCII et conserv�s tel que pour l'instant.
[~, ODVData] = read_odv(dataFileName, ...
    'NbLigEntete',  1, 'DisplayEntete', 0, 'GeometryType', 3, ...
    'AskSeparator', 0, 'AskValNaN',     0, 'Memmapfile',   0);
if isempty(ODVData)
    msg = [];
end

if isempty(msg)
    str1 = sprintf('Le traitement de fichiers ODV a rencontr� un probl�me sur le fichier %s', dataFileName);
    str2 = sprintf('ODV processing library does not seem working for file %s', dataFileName);
    my_warndlg(Lang(str1,str2), 0, 'Tag', dataFileName);
end

if ~exist(nomDirRacine, 'dir')
    if isempty(msg)
        str1 = sprintf('Le traitement de fichiers ODV a rencontr� un probl�me sur le fichier %s', dataFileName);
        str2 = sprintf('ODV processing library does not seem working for file %s', dataFileName);
        msg = Lang(str1,str2);
    end
    my_warndlg(msg, 0, 'Tag', dataFileName);
    flag = 0;
    return
end

%% Datagrammes Data

flag = odv2ssc_Data(nomDirRacine, FormatVersion, 'ODVData', ODVData);
if ~flag
    return
end
flag = 1;


function FormatVersion = getFormatVersion(nomDirRacine)

nomFicXml = fullfile(nomDirRacine, 'Ssc_Data.xml');
flag = exist(nomFicXml, 'file');
if ~flag
    special_message('FichierNonExistant', nomFicXml)
    return
end
% edit(nomFicXml)
Datagrams = xml_read(nomFicXml);
if ~isfield(Datagrams, 'FormatVersion')
    FormatVersion = 0;
    return
end
FormatVersion = Datagrams.FormatVersion;

