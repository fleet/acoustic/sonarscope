function flag = odv2ssc_Data(nomDirRacine, FormatVersion, varargin)

% Il n'y pas dans le cas du ODV de fichier XML g�n�r� pr�alablement par
% l'Etape 1 du SEGY 2 SSC. On se retrouve directement en �tape 2.

[varargin, ODVData] = getPropertyValue(varargin, 'ODVData', []); %#ok<ASGLU>

% Nommage en Data.Data pour �tre conforme aux autres conversion de formats.
Data = ODVData;

clear ODVData;

%% G�n�ration du XML SonarScope

Info.Title         = Data.Cruise{1};
Info.Constructor   = 'TODO';
Info.TimeOrigin    = '01/01/-4713';
Info.Comments      = 'Binary Data';
Info.FormatVersion = FormatVersion;

f = fieldnames(Data);
nbSignaux = numel(f)-1; % Nb de champs de Data moins le champ Image.

pppp = fieldnames(Data.Image);
nbImages = numel(pppp);

Info.Dimensions.NbProfiles = size(Data.Image.(pppp{1}),2);
Info.Dimensions.ns         = size(Data.Image.(pppp{1}),1);

% nbSignaux   = numel(NbSig)-1; % Nb de champs de Data moins le champ Image.
%
% for k=1:nbSignaux
%     Info.Signals(k).Name          = NbSig{k};
%     Info.Signals(end).Dimensions  = 'NbProfiles, 1';
%     Info.Signals(end).Storage     = 'single';
%     Info.Signals(end).Unit        = '';
%     Info.Signals(end).FileName    = fullfile('Ssc_Data',[NbSig{k} '.bin']);
%     Info.Signals(end).Tag         = verifKeyWord('TODO');
% end

tabUnit    = {'';     '';       'mon/day/yr'; 'hh:mm'; 'm';      'm';      'deg';    'deg'}; %;      'deg';    'cm/s';   ''};
tabStorage = {'char'; 'single'; 'char';       'char';  'single'; 'single'; 'double'; 'double'}; %; 'single'; 'single'; 'single'};

Dim    = {'NbProfiles, 1'};
tabDim = repmat(Dim{1}, [size(f,1),1]);
NbStr  = 1;
NbSig  = 1;
for k=1:nbSignaux
    if strcmp(tabStorage{k}, 'char')
        % TODO GLU
        % Pas net pour ODV_Cache2Netcdf, on commente
        %{
        Info.Strings(NbStr).Name     = f{k};
        Info.Strings(end).Dimensions = 'NbProfiles, 1';
        Info.Strings(end).Storage    = 'string';
        Info.Strings(end).Unit       = '';
        Info.Strings(end).FileName   = fullfile('Ssc_Data',[f{k} '.bin']);
        Info.Strings(end).Tag        = f{k};
        NbStr = NbStr + 1;
        %}
    else
        Info.Signals(NbSig).Name     = f{k};
        Info.Signals(end).Dimensions = tabDim(k,:); %tabDim{k};
        Info.Signals(end).Storage    = tabStorage{k};
        Info.Signals(end).Unit       = tabUnit{k};
        Info.Signals(end).FileName   = fullfile('Ssc_Data',[f{k} '.bin']);
        Info.Signals(end).Tag        = f{k};
        NbSig = NbSig+1;
    end
end

for k=1:nbImages
    Info.Images(k).Name       = pppp{k};
    Info.Images(k).Dimensions = 'ns, NbProfiles';
    Info.Images(k).Storage    = 'double';
    Info.Images(k).Unit       = '';
    Info.Images(k).FileName   = fullfile('Ssc_Data',[pppp{k} '.bin']);
    Info.Images(k).Tag        = verifKeyWordODV(pppp{k});
end

%% Cr�ation du fichier XML d�crivant la donn�e

nomFicXml = fullfile(nomDirRacine, 'Ssc_Data.xml');
xml_write(nomFicXml, Info);
flag = exist(nomFicXml, 'file');
if ~flag
    messageErreur(nomFicXml)
    return
end

%% Cr�ation du r�pertoire SegyData

nomDirODVData = fullfile(nomDirRacine, 'Ssc_Data');
if ~exist(nomDirODVData, 'dir')
    status = mkdir(nomDirODVData);
    if ~status
        messageErreur(nomDirODVData)
        flag = 0;
        return
    end
end

%% Cr�ation des fichiers binaires des signaux

for k=1:length(Info.Signals)
    flag = writeSignal(nomDirRacine, Info.Signals(k), Data.(Info.Signals(k).Name));
    if ~flag
        return
    end
end

%% Cr�ation des fichiers binaires des Strings

if isfield(Info, 'Strings')
    for k=1:length(Info.Strings)
        flag = writeStrings(nomDirRacine, Info.Strings(k),Data.(Info.Strings(k).Name));
        if ~flag
            return
        end
    end
end

%% Cr�ation des fichiers binaires des images

for k=1:length(Info.Images)
    flag = writeImage(nomDirRacine, Info.Images(k), Data.Image.(Info.Images(k).Name));
    if ~flag
        return
    end
end

flag = 1;
