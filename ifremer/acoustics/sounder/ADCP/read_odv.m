% Importation d'un fichier ASCII ODV (sondes donnees en colonnes)
%
% Syntax
%   [flag, Data] = read_odv(nomFic)
%
% Input Arguments
%   nomFic : Nom du fichier
%
% Output Arguments
%   flag : 1 si lecture reussie, 0 sinon
%   Data : Ensemble de donn�es
%
% Examples
%   TODO
% See also cl_image/import_* Authors
% Authors : JMA
%--------------------------------------------------------------------------

% TODO : utiliser read_ASCII pour remplacer toutes les questions

function [flag, ODVData] = read_odv(nomFic, varargin)

ODVData = [];

[varargin, Separator]       = getPropertyValue(varargin, 'Separator',       []);
[varargin, NbLigEntete]     = getPropertyValue(varargin, 'NbLigEntete',     []);
% [varargin, GeometryType]  = getPropertyValue(varargin, 'GeometryType',    []);
% [varargin, ValNaN]        = getPropertyValue(varargin, 'ValNaN',          []);
% [varargin, Unit]          = getPropertyValue(varargin, 'Unit',            []);
[varargin, DisplayEntete]   = getPropertyValue(varargin, 'DisplayEntete',   1);
[varargin, AskSeparator]    = getPropertyValue(varargin, 'AskSeparator',    1);
% [varargin, AskValNaN]     = getPropertyValue(varargin, 'AskValNaN',       1);
% [varargin, AskGeometryType] = getPropertyValue(varargin, 'AskGeometryType', 1);
[varargin, flagMemmapfile]  = getPropertyValue(varargin, 'Memmapfile',      0); %#ok<ASGLU>

TitreDialogBox = 'ASCII ODV file import';

%% Extraction des donn�es.

% [nomFicSeul, ~] = strtok(ListeFic{k}, '.');

% Recherche des indices de colonne pour les valeurs � s�lectionner et
% celles syst�matiquement r�cup�r�es depuis le fichier.
selected.String       = {'Dir'; 'Mag'; 'EchoInt'};
selected.SearchString = {'Dir'; 'Mag'; 'EchoInt'};
selected.Label        = {'Current Dir (deg)'; 'Current Magnitude (cm/s)'; 'Echo Integration (count)'};
selected.Unit         = {'[deg]'; '[cm/s]'; '[count]'};
default.String        = {'Cruise'; 'Station'; 'Date'; 'Heure'; 'Longitude'; 'Latitude'; 'BotDepth'; 'Depth'};
default.SearchString  = {'Cruise';'Station'; 'mon/day/yr'; 'hh:mm'; 'Longitude'; 'Latitude'; 'Bot. Depth'; 'Depth'};
default.Label         = {'Cruise';'Num Station'; 'Date mon/day/yr'; 'Heure hh:mm'; 'Longitude (deg)'; 'Latitude (deg)'; 'Profile Bottom Depth (m)'; 'Instant Depth (m)'};
default.Unit          = {'';''; ''; ''; '[degrees_east]'; '[degrees_north]'; '[m]'; '[m]'};

% Identification des n� de colonnes des variables.
fid = fopen(nomFic, 'r');
if fid == -1
    fid = fopen(nomFic, 'r');
    if fid == -1
        flag = 0;
        return
    end
end
% Lecture de la ligne d'ent�te pour recherche des indices de colonnes.
% Attention : Le nombre de colonnes peut varier dans les fichiers.!!!.
idxFile = [];
idxData = [];

pppp = fgetl(fid);
for k=1:20 % Nb Max de colonnes.
    [token, pppp] = strtok(pppp, char(9)); %#ok<*STTOK>
    for u=1:numel(default.String)
        if u == 4 && strcmp(token, 'hh:mm:ss')
            % Verrue temporaire le temps de figer les ent�tes de colonne.
            token = 'hh:mm';
        end
        if strcmp(token, strtrim([default.SearchString{u} ' ' default.Unit{u}]))
            idxFile.(default.String{u}) = k;
            names = fieldnames(idxFile);
            dummy = numel(names);
            idxData.(default.String{u}) = dummy;
            break
        end
    end
    for u=1:numel(selected.String)
        if strcmp(token, strtrim([selected.SearchString{u} ' ' selected.Unit{u}]))
            idxFile.(selected.String{u}) = k;
            names = fieldnames(idxFile);
            dummy = numel(names);
            idxData.(selected.String{u}) = dummy;
            break
        end
    end
    if isempty(token)
        nColumns = k-1;
        break
    end
end
fclose(fid);

if isempty(idxFile)
    % Si le fichier n'est pas au bon format : traitement du prochain
    % fichier.
    my_warndlg([Lang('Fichier non adapt� au traitement ODV :', 'File does not occur to be process as ODV File :') nomFic] , 1);
end


%% Lecture du fichier

% TODO TODO TODO
% TODO : faut g�n�raliser read_ASCII pour pouvoir lire en un seul coup
% les diff�rentes variables
% [flag, X, Y, V, NbLigEntete, Separator, ValNaN, nColumns, GeometryType, IndexColumns, ...
%        DataType, Unit] = read_ASCII(FileName, varargin{:})

%% Analyse de l'ent�te du fichier

fid = fopen(nomFic, 'r');
if fid == -1
    [nomDir, nomFic] = fileparts(nomFic);
    nomFic = fullfile(nomDir, [nomFic '.xyz']);
    fid = fopen(nomFic, 'r');
    if fid == -1
        flag = 0;
        return
    end
end

for k=1:20
    Entete{k} = fgetl(fid);%#ok
end
fclose(fid);

%% Decodage guid� par l'op�rateur

if DisplayEntete
    str = sprintf(Lang('Debut du fichier %', 'Begin of the file %s'), nomFic);
    LigneTemoin = Entete{end};
    mots = strsplit(LigneTemoin);
    
    Entete{end+1} = ' ';
    Entete{end+1} = Lang('Information sur le contenu des colonnes pour la derni�re ligne', 'Information about columns content for last line');
    for k=1:length(mots)
        Entete{end+1} = sprintf('Column %d : %s', k, mots{k}); %#ok
    end
    my_txtdlg(str, Entete, 'windowstyle', 'normal');
end

%% Nombre de lignes d'ent�te

if isempty(NbLigEntete)
    NbLigEntete = 0;
    str1 = 'Nb de champs par ligne';
    str2 = 'Nb of fields in columns';
    [flag, NbLigEntete] = inputOneParametre(TitreDialogBox, Lang(str1,str2), 'Value', NbLigEntete, 'Format', '%d');
    if ~flag
        return
    end
elseif NbLigEntete < 0
    NbLigEntete = -NbLigEntete;
end

%% Separateur

if AskSeparator
    if isempty(Separator)
        str1 = 'S�parateur sp�cifique autre que "Espace", "Chararct�re blanc" ou "Tabulation"';
        str2 = 'Specific separator if any. (OK if white-space character like space or Tab).';
        [Separator, flag] = my_inputdlg({Lang(str1,str2)}, {''});
        if ~flag
            return
        end
        Separator = Separator{1};
    end
end

% if AskValNaN
%     if isempty(ValNaN)
%         [ValNaN, flag] = my_inputdlg(Lang({'Sp�cifier la valeur pour "NO-DATA" si besoin'},{'Specific value for "No Data" if any'}), {''});
%         if ~flag
%             return
%         end
%         ValNaN = ValNaN{1};
%         if ~isempty(ValNaN)
%             if strcmpi(ValNaN, 'NaN')
%                 ValNaN = [];
%             else
%                 ValNaN = str2double(ValNaN);
%             end
%         end
%     end
% end

%% Nombre de colonnes utiles

if isempty(nColumns)
    nColumns = 3;
    str1 = 'Nb de champs';
    str2 = 'Nb of fields';
    [flag, nColumns] = inputOneParametre(TitreDialogBox, Lang(str1,str2), 'Value', nColumns, 'Format', '%d');
    if ~flag
        return
    end
end

%% Lecture

[flag, DataRead] = lectureIndirecte(nomFic, NbLigEntete, Separator, idxFile, nColumns);
if ~flag
    strFR = sprintf(Lang('Probl�me dans la lecture du fichier : %s\n', nomFic));
    strUS = sprintf(Lang('Problem in reading data File : %s\n', nomFic));
    my_warndlg(Lang(strFR, strUS), 1);
    return;
end

%% Formattage des donn�es

[flag, ODVData] = formatODVData(DataRead);
if ~flag
    strFR = sprintf(Lang('Probl�me dans l''exploitation des profils du fichier : %s\n', nomFic));
    strUS = sprintf(Lang('Problem in processing profiles data from File : %s\n', nomFic));
    my_warndlg(Lang(strFR, strUS), 1);
    return;
end

flag = 1;


function [flag, Data] = lectureIndirecte(nomFic, NbLigEntete, Separator, indexFile, nColumns)

flag = 0; %#ok<NASGU>
% Comptage du nombre de lignes
% NLignesTotal   = str2double( perl('countlines.pl', nomFic) );

% Cr�ation d'une zone memmapfile.
% % % % if flagMemmapfile==1
% % % %     Data        = cl_memmapfile('FileName', tempname, 'Value', 0, 'Size', [NLignesTotal-NbLigEntete 6], 'Format', 'double');
% % % % else
% % % %     Data        = zeros(NLignesTotal-NbLigEntete, 6, 'double');
% % % % end


fid = fopen(nomFic, 'r');
for k=1:NbLigEntete
    Entete{k} = fgetl(fid); %#ok
end

OK = 1;
Format ='';

for k=1:nColumns
    Format = [Format '%s']; %#ok
end

str           = sprintf('Reading data from %s', nomFic);
h             = create_waitbar(str);
pppp          = dir(nomFic);
tailleFic     = pppp.bytes;
NLignesToRead = 20000;
offset        = 0;
while OK
    tailleLue = ftell(fid);
    waitbar(tailleLue/tailleFic, h);
    if isempty(Separator)
        C = textscan(fid, Format, NLignesToRead);
        % C = textscan(fid, Format, 1e6);
    else
        C = textscan(fid, Format, NLignesToRead, 'delimiter', Separator);
        % C = textscan(fid, Format, 1e6, 'delimiter', Separator);
    end
    if isempty(C{1})
        OK = 0;
    else
        N = length(C{1});
        if N ~= NLignesToRead
            NLignesToRead = N;
        end

        FieldsODV = fieldnames(indexFile);
        NbFields  = numel(FieldsODV);
        str1 = sprintf('IFREMER - SonarScope : decodage du fichier par paquets de %d lignes.', NLignesToRead);
        str2 = sprintf('IFREMER - SonarScope : decode data by blocks of %d lines.', NLignesToRead);
        hw = create_waitbar(Lang(str1,str2), 'N', N);
        for k=1:N
            my_waitbar(k, N, hw);
            % Affectation des colonnes � �tudier.
            for j=1:NbFields
                % if isa(C{indexFile.(FieldsODV{j})}(k), 'double')
                % En dur pour l'instant, r�cup�ration des chaines de char.
                if (j==1 || j==3 || j==4)
                    Data.(FieldsODV{j}){k} = C{indexFile.(FieldsODV{j})}{k};
                    % Data(k+offset,j) =  str2double(C{indexFile.(FieldsODV{j})}(k));
                else
                    Data.(FieldsODV{j})(k) = str2double(C{indexFile.(FieldsODV{j})}(k));
                    % Data(k+offset,j) =  C{indexFile.(FieldsODV{j})}(k);
                end
            end
        end
        my_close(hw, 'MsgEnd');
        
        offset = offset + N;
    end
end
fclose(fid);
my_close(h);

flag = 1;


function [flag, ODVData] = formatODVData(Data)

flag = 0; %#ok<NASGU>

% Analyse des unicit�s des id de profiles.
% Mise en forme des coordonn�es � cr�er dans l'ordre d'�criture des XML +
% Binaires.
[pppp, m, ~]      = unique(Data.Station(:));
ODVData.Cruise    = {Data.Cruise{m}};
ODVData.Station   = pppp;
ODVData.strDate   = {Data.Date{m}};
ODVData.strHour   = {Data.Heure{m}};
idx0              = 1;
ODVData.ZBotDepth = [];
ODVData.ZTopDepth = [];
ODVData.Latitude  = [];
ODVData.Longitude = [];

for ii=1:size(m, 1)
    idx = idx0:m(ii);
    % Analyse des bornes Min-Max de chaque profil.
    maxZProfil = max(Data.Depth(idx));
    minZProfil = min(Data.Depth(idx));
    LatIProfil = unique(Data.Latitude(idx));
    LonIProfil = unique(Data.Longitude(idx));
    LatIProfil = LatIProfil(1);
    LonIProfil = LonIProfil(1);
    
    ODVData.ZBotDepth = [ODVData.ZBotDepth maxZProfil];
    ODVData.ZTopDepth = [ODVData.ZTopDepth minZProfil];
    ODVData.Latitude  = [ODVData.Latitude  LatIProfil];
    ODVData.Longitude = [ODVData.Longitude LonIProfil];
    
    idx0 = m(ii)+1;
end

% Cr�ation de l'image selon les colonnes de donn�es.
nameField = {'Dir', 'Mag', 'EchoInt'};
for s=1:numel(nameField)
    [~, Image] = formatImagefromODV(Data, nameField{s});
    Image = flipud(Image);
    ODVData.Image.(nameField{s}) = Image;
end

flag = 1;
% figure; PlotUtils.createSScPlot(flipud(ODVData.ZBotDepth), '-b'); title('Z-Bottom Depth');
