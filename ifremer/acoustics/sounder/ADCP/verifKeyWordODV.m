function word = verifKeyWordODV(word)

listeODV = {'Cruise'
    'Date'
    'Hour'
    'Station'
    'ZTopDepth'
    'ZBotDepth'
    'Longitude'
    'Latitude'
    'Dir'
    'Mag'
    'EchoInt'
    'U'
    'V'};

listeOther = {};

listeTotale = [listeODV;listeOther];
rep = strcmp(listeTotale, word);
if all(rep == 0)
    str = sprintf('"%s" ne fait pas partie du dictionnaire.', word);
    my_warndlg(str, 0, 'Tag', 'VerifMotDansDictionnaire');
end
