function flag = BOB_Export3DViewer(~, listeFicBOB, nomDirOut, nomFicOut, ColormapIndex, Video, Compress)

Carto = [];

N = length(listeFicBOB);
str1 = 'Export des fichiers BOB dans GLOBE';
str2 = 'Exporting BOB files to GLOBE';
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
    my_waitbar(k, N, hw);
    [flag, listLayers, listThresholds, listFlagDataType, listIndexType] = selectionLayers(cl_Bob([]), listeFicBOB{k});
    if ~flag
        return
    end
    [flag, Carto] = BOB_Export3DViewer_unitaire(listeFicBOB{k}, nomDirOut, nomFicOut, ...
                                                ColormapIndex, Video, Carto, Compress, ...
                                                listLayers, listThresholds, listFlagDataType, listIndexType);
    if ~flag
        return
    end
end
my_close(hw, 'MsgEnd')



function [flag, Carto] = BOB_Export3DViewer_unitaire(nomFicBob, nomDirOut, nomFicOut, ...
                                                    ColormapIndex,  Video, Carto, Compress, ...
                                                     listCycles, listThresholds, listFlagLayerType, listIndexType)

%% Cr�ation du r�pertoire

nomFicXml = fullfile(nomDirOut, nomFicOut);

%% Lecture de l''image

[flag, a] = import_BOB(cl_multidata([]), nomFicBob, ...
                            'listNumLayers', listCycles, ...
                            'listFlagLayerType', listFlagLayerType, ...
                            'listIndexType', listIndexType, ...
                            'listThresholds', listThresholds, ...
                            'ColorMapIndex', ColormapIndex, ...
                            'Video', Video); 
                                
if ~flag
    return
end

% Pour BOB, les layers de m�me type sont g�n�r�s par cycle de temps.
flagMultiTime = 1;

% R�organisation par type et nom des layers.
a = reorganization(a, flagMultiTime);

flag = exportGeotiffSonarScope3DViewerMultiV3(a, nomFicXml, 'sizeTiff', 2500, 'flagCompress', Compress);

