function BOB_ReadOnce(~, nomFic)

if ~iscell(nomFic)
    nomFic = {nomFic};
end

% profile on
% tic
N = length(nomFic);
hw = create_waitbar('Processing', 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    fprintf('%d/%d - %s', k, N, nomFic{k});
    try
        a = cl_Bob('nomFic', nomFic{k}); %#ok<NASGU>
        fprintf(' : OK\n');
    catch ME
        ErrorReport = getReport(ME) %#ok<NOPRT>
        SendEmailSupport(ErrorReport)
        fprintf(' : ERROR\n');
    end
end
my_close(hw, 'MsgEnd')

% profile report
% toc
