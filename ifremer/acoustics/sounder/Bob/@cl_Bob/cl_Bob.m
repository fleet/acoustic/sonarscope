% Constructeur de cl_Bob (Issu d'un traitement d'�cho int�gration sur les
% �quipements BOB).
%
% Syntax
%   a = cl_Bob(...)
% 
% Name-Value Pair Arguments
%   nomFic : Nom(s) du fichier .bob
%  
% Output Arguments 
%   a : Instance de cl_Bob
%
% Examples
%   nomFic = 'F:\SonarScopeData\BOB-3D\Data\EI_Lay_SvalBard.xml';
%   a = cl_Bob('nomFic', nomFic)
%
% See also cl_hac Authors
% Authors : GLU
% ----------------------------------------------------------------------------

function this = cl_Bob(varargin)

%% Definition de la structure

this.nomFic          = '';

this.Survey          = '';
this.NbCells         = 0;
this.NbCycles        = 0;
this.NbSectors       = 0;
this.Thresholds.Nb   = 0;
this.Thresholds.Step = 0;
this.Thresholds.Min  = 0;
this.Thresholds.Max  = 0;

this.Heading         = 0;
this.SizeCellEI      = 0;
this.OffsetBeam1     = 0;
this.BeamWidth       = 0;

this.Mat_CCU_File    = '';
this.Mat_Data_File   = '';

%% Creation de l'instance

this = class(this, 'cl_Bob');

%% On regarde si l'objet a ete cree uniquement pour atteindre une methode de la classe

if (nargin == 1) && isempty(varargin{1})
    return
end

%% Initialisation des valeurs transmises lors de l'appel au constructeur

this = set(this, varargin{:});
