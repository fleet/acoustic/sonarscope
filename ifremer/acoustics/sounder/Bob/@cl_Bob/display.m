% Representation externe d'une instance sur le terminal
%
% Syntax
%   display(this)
%
% Input Arguments 
%   this : une instance de cl_segy
%
% Examples
%   nomFic = 'F:\SonarScopeData\BOB-3D\Data\EI_Lay_SvalBard.xml';
%   a = cl_bob('nomFic', nomFic, 'Memmapfile', 1)
%
%   display(a)
%   a
%
% See also cl_segy cl_segy/char Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function display(this)

s = whos('this');
sz = s.size;

fprintf('Name\tSize\tClass\n');
fprintf('%s\t%s\t%s\n', inputname(1), num2strCode(sz), s.class);

disp(char(this));
