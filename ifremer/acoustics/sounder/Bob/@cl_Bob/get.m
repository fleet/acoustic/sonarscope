% Acces en lecture des proprietes d'une instance
%
% Syntax
%   [...] = get(this, ...)
% 
% Input Arguments 
%   this : Une instance de la classe cl_bob
%
% Name-Value Pair Arguments
%   nomFic : Nom du fichier .bob
%
% Output Arguments
%   PropertyValues associees aux PropertyNames passes en argument
% 
% Examples
%   nomFic = 'F:\SonarScopeData\BOB-3D\Data\EI_Lay_SvalBard.xml';
%   a = cl_Bob('nomFic', nomFic)
%   nomFic = get(a, 'nomFic')
%
% See also cl_bob cl_bob/char Authors
% Authors : GLU
% ----------------------------------------------------------------------------

function varargout = get(this, varargin)

%% Boucle de parcours des arguments

varargout = {};
for k=1:length(varargin)
    switch varargin{k}
        case 'nomFic'
            varargout{end+1} = this.nomFic; %#ok<AGROW>
                      
        case 'SurveyName'
            varargout{end+1} = this.Survey; %#ok<AGROW>

        case 'NbCells'
            varargout{end+1} = this.NbCells; %#ok<AGROW>

        case 'NbSectors'
            varargout{end+1} = this.NbSectors; %#ok<AGROW>

        case 'NbCycles'
            varargout{end+1} = this.NbCycles; %#ok<AGROW>

        case 'NbThresholds'
            varargout{end+1} = this.Thresholds.Nb; %#ok<AGROW>

        case 'StepThresholds'
            varargout{end+1} = this.Thresholds.Step; %#ok<AGROW>

        case 'MinThresholds'
            varargout{end+1} = this.Thresholds.Min; %#ok<AGROW>

        case 'MaxThresholds'
            varargout{end+1} = this.Thresholds.Max; %#ok<AGROW>

        case {'Thresholds', 'Th'}
            varargout{end+1} = this.Thresholds.Min:this.Thresholds.Step:this.Thresholds.Max; %#ok<AGROW>

        case 'OffsetBeam1'
            varargout{end+1} = this.OffsetBeam1; %#ok<AGROW>

        case 'SizeCellEI'
            varargout{end+1} = this.SizeCellEI; %#ok<AGROW>

        case 'Heading'
            varargout{end+1} = this.Heading; %#ok<AGROW>

        case 'BeamWidth'
            varargout{end+1} = this.BeamWidth; %#ok<AGROW>

        case 'Mat_CCU_File'
            varargout{end+1} = this.Mat_CCU_File; %#ok<AGROW>

        case 'Mat_Data_File'
            varargout{end+1} = this.Mat_Data_File; %#ok<AGROW>

        otherwise
            w = sprintf('cl_Bob/get %s non pris en compte', varargin{k});
            my_warndlg(w, 0);
            varargout{end+1} = []; %#ok<AGROW>
    end
end
