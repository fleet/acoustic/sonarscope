% Transformation en chaine de caracteres d'une instance
%
% Syntax
%   str = char_instance(a)
%
% Input Arguments
%   a : instance de cl_image
%
% Name-Value Pair Arguments
%   InfoSonar  : 1=Information de l'image uniquement
%                2=Info de l'information sonar uniquement
%                3=Display de toute l'information disponible (image+sonar) : (3 par defaut)
%
% Output Arguments
%   str : une chaine de caracteres representative de l instance
%
% Examples
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function str = char_instance(this, varargin)

if ~isempty(this.nomFic)
    str = [];
    str{end+1} = sprintf('NomFic               <-> %s', this.nomFic);
    str{end+1} = sprintf('SurveyName           <-- %s', this.Survey);
    str{end+1} = sprintf('NbCells              <-- %d', this.NbCells);
    str{end+1} = sprintf('NbCycles             <-- %d', this.NbCycles);
    str{end+1} = sprintf('NbSectors            <-- %d', this.NbSectors);
    
    % Affichage du positionnement:
    str{end+1} = sprintf('-------------------------------------------');
    str{end+1} = sprintf('OffsetBeam1 (deg)             <-- %8.3f', this.OffsetBeam1);
    str{end+1} = sprintf('Size Cell EchoIntegration (m) <-- %8.3f', this.SizeCellEI);
    str{end+1} = sprintf('Heading (deg)                 <-- %8.3f', this.Heading);
    str{end+1} = sprintf('BeamWidth (deg)               <-- %8.3f', this.BeamWidth);
    str{end+1} = sprintf('------ Threshold (dB) --------------------- ');
    str{end+1} = sprintf('  NbThresholds                   <-- %4d',  this.Thresholds.Nb);
    str{end+1} = sprintf('  Min Threshold (dB)             <-- %8.3f', this.Thresholds.Min);
    str{end+1} = sprintf('  Max Threshold (dB)             <-- %8.3f', this.Thresholds.Max);
    str{end+1} = sprintf('  Step Threshold (dB)            <-- %8.3f', this.Thresholds.Step);
  
    % Donn�es Capteurs:
    str{end+1} = sprintf('------ Mat Data File --------------------- ');
    str{end+1} = sprintf('Mat_Data_File                 <-- %s', this.Mat_Data_File);

    % Donn�es Capteurs:
    str{end+1} = sprintf('------ Sensor Data ----------------------- ');
    str{end+1} = sprintf('CCU Sensors File              <-- %s', this.Mat_CCU_File);

    str = cell2str(str);
else
    str = [];
end
