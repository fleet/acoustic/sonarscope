function [flag, listLayers, listTh, listDataType, listIndexType] = selectionLayers(~, nomFic)

flag = 0;

listLayers          = [];
listTh              = [];
listDataType        = [];
listIndexType       = [];
%% Cr�ation de l'instance

this = cl_Bob('nomFic', nomFic); %

if isempty(this)
    my_warndlg(Lang('Le fichier pose probl�me. Pas de solution pour l''instant', ...
                    'Problem in load file: no solution for the moment.'), 1);
    flag = 0;
    return
end

%% Lecture de la liste des fr�quences

listCycles      = 1:get(this, 'NbCycles');
listThresholds  = get(this, 'Thresholds');

%% S�lection des fr�quences

for k=1:length(listCycles)
    str{k} = num2str(listCycles(k)); %#ok<AGROW>
end
str1 = 'S�lectionnez la /les cycles � importer.';
str2 = 'Select the cycles you want to import.';
[listLayers, flag] = my_listdlg(Lang(str1,str2), str, 'InitialValue', 1);
if ~flag
    return
end

%% S�lection des Seuils
clear str
for k=1:length(listThresholds)
    str{k} = num2str(listThresholds(k)); %#ok<AGROW>
end
str1 = 'S�lectionnez la /les valeurs de seuils � importer.';
str2 = 'Select the thresholds values you want to import.';
[listTh, flag] = my_listdlg(Lang(str1,str2), str, 'InitialValue', 1);
if ~flag
    return
end

%% S�lection des types de data

clear str
str{1}  = 'Reflectivity value';
str{2}  = 'RxBeamIndex - Sectors angles';
str{3}  = 'RayPathRange - Range cells';
str1    = 'S�lectionnez les donn�es � importer.';
str2    = 'Select the data type you want to import.';
listDataType = zeros(1,3);
[pppp, flag] = my_listdlg(Lang(str1,str2), str, 'InitialValue', 1);
if ~flag
    return
end
listDataType(pppp) = 1;

% For�age des index de donn�es pour le BOB.
listIndexType = [0 1 1];

flag = 1;
