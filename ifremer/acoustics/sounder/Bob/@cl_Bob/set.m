% Acces en ecriture des proprietes d'une instance
%
% Syntax
%   b = set(a, ...)
%
% Input Arguments
%   a : Une instance de la classe cl_bob
%
% Name-Value Pair Arguments
%   nomFic : Nom(s) du fichier .bob
%
% Output Arguments
%   [] : Sauve l'instance dans a
%   b  : Instance de la classe
%
% Examples
%   nomFic = 'F:\SonarScopeData\BOB-3D\Data\EI_Lay_SvalBard.xml';
%   a = cl_Bob('nomFic', nomFic)
%   a = set(a, 'nomFic', nomFic)
%
%
% See also cl_bob cl_bob/get Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function varargout = set(this, varargin)

% persistent nomFicMat
[varargin, nomFic] = getPropertyValue(varargin, 'nomFic', []); %#ok<ASGLU>

if ~isempty(nomFic)
    this.nomFic = nomFic;
    [nomDir, nomFicSeul, ext]   = fileparts(this.nomFic); %#ok<ASGLU>
    pppp                        = xml_read(nomFic);
    if isempty(pppp)
        varargout{1} = [];
        return
    end
    this.Survey            = pppp.Survey;
    this.NbCells           = pppp.Dimensions.nbCells;
    this.NbSectors         = pppp.Dimensions.nbSectors;
    this.NbCycles          = pppp.Dimensions.nbCycles;
    this.Thresholds.Nb     = pppp.Dimensions.nbThresholds;
    
    % R�cup�ration du positionnement du BOB
    identVariable = findIndVariable(pppp.Variables, 'OffsetBeam1');
    if isVariableConstante(pppp.Variables(identVariable))
        this.OffsetBeam1 = pppp.Variables(identVariable).Constant;
    else
        my_warndlg(Lang('Le positionnement BOB doit �tre scalaire.', 'BOB position must be scalar.'), 1);
        return
    end
    
    identVariable = findIndVariable(pppp.Variables, 'SizeCellEI');
    if isVariableConstante(pppp.Variables(identVariable))
        this.SizeCellEI = pppp.Variables(identVariable).Constant;
    else
        my_warndlg(Lang('Le positionnement BOB doit �tre scalaire.', 'BOB position must be scalar.'), 1);
        return
    end
    
    identVariable = findIndVariable(pppp.Variables, 'Heading');
    if isVariableConstante(pppp.Variables(identVariable))
        this.Heading = pppp.Variables(identVariable).Constant;
    else
        my_warndlg(Lang('Le positionnement BOB doit �tre scalaire.', 'BOB position must be scalar.'), 1);
        return
    end
    
    identVariable = findIndVariable(pppp.Variables, 'BeamWidth');
    if isVariableConstante(pppp.Variables(identVariable))
        this.BeamWidth = pppp.Variables(identVariable).Constant;
    else
        my_warndlg(Lang('L''ouverture angulaire doit �tre scalaire.', 'BOB beamwidth must be scalar.'), 1);
        return
    end
    
    identVariable = findIndVariable(pppp.Variables, 'StepThreshold');
    if isVariableConstante(pppp.Variables(identVariable))
        this.Thresholds.Step= pppp.Variables(identVariable).Constant;
    else
        my_warndlg(Lang('Le pas de seuillage doit �tre scalaire.', 'Threshold Step must be scalar.'), 1);
        return
    end
    
    identVariable = findIndVariable(pppp.Variables, 'MinThreshold');
    if isVariableConstante(pppp.Variables(identVariable))
        this.Thresholds.Min = pppp.Variables(identVariable).Constant;
    else
        my_warndlg(Lang('Le Minimum de seuillage doit �tre scalaire. ', 'Threshold min must be scalar.'), 1);
        return
    end
    
    identVariable = findIndVariable(pppp.Variables, 'MaxThreshold');
    if isVariableConstante(pppp.Variables(identVariable))
        this.Thresholds.Max = pppp.Variables(identVariable).Constant;
    else
        my_warndlg(Lang('Le Maximum de seuillage doit �tre scalaire. ', 'Threshold max must be scalar..'), 1);
        return
    end
    
    %% Import des donn�es Capteur
    % this.Mat_CCU_File   = fullfile(nomDir, pppp.NomFicSensor);
    
    % En entr�e de ExRaw : nomFic et nomFicNav si il n'est pas vide.
    if isempty(this.Mat_Data_File) || ~exist(this.Mat_Data_File, 'file')
        [flag, ~, nomFicMat] = read_DataBob(this.nomFic);
        if ~flag
            varargout{1} = [];
            return
        end
    end
    this.Mat_Data_File = nomFicMat;
end

%% Sortie

if nargout == 0
    assignin('caller', inputname(1), this);
else
    varargout{1} = this;
end
