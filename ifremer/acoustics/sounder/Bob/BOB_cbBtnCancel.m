% = Callback ===============
function BOB_cbBtnCancel(hObject, event)

hdlFig         = ancestor(hObject, 'figure','toplevel');
setappdata(hdlFig,'flagOKCancel', 0);

% Stimulation de la Callback de fermeture
ListItems      = [];
BOB_cbClose(hdlFig, event, ListItems);

% -- BOB_cbBtnCancel