% = Callback ===============
function BOB_cbBtnComputeEstimate(hObject, event)

    hdlFig      = ancestor(hObject, 'figure', 'toplevel');
    
    valCompas   = getappdata(hdlFig, 'ValueEstimateCompass');
    
    nomVar = {Lang('Saisie la valeur estim�e de Compas (Deg)', 'Input estimate Compass value(deg)')};
    if ~isnan(valCompas)
        value = {num2str(valCompas)};
    else
        value = {''};
    end
        
    [valCompas, flagValid] = my_inputdlg(nomVar, value, ...
                                    'Entete', Lang( 'Veuillez saisir le compas manuellement', ...
                                                    'Please input the compass Value'), 1);
    if flagValid == 1
        valCompas = str2double(valCompas{:});
        setappdata(hdlFig, 'ValueEstimateCompass', valCompas);
    else
        return
    end
    
    %% Estimation de la configuration nominale.
    [flag, sConfigBOB]  = BOB_fcnSaveConfig(hdlFig); 
    if ~flag
        return
    end
	[flag, StructEstimateSector] = BOB_fcnEstimateAngles(valCompas, sConfigBOB);
    if ~flag
        return
    end
    
    %% Affichage des angles Estim�s.
    flag = BOB_uiSummaryConfig(StructEstimateSector, ...
                                    'Resizable', 'on', ...
                                    'Entete', 'IFREMER - SonarScope - BOB Processing - Estimation Angle');

%-- BOB_cbBtnComputeEstimate
