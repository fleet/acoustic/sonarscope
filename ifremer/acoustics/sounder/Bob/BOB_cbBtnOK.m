% = Callback ===============
function BOB_cbBtnOK(hObject, event, msg)

hdlFig = ancestor(hObject, 'figure', 'toplevel');
if ~isempty(msg)
    [rep, flag] = my_questdlg(msg, 'Init', 1, 'ThirdButton', 0);
    if ~flag || (rep == 2)
        return
    end
end
% Stimulation de la Callback de fermeture
setappdata(hdlFig,'flagOKCancel',   1);
BOB_cbClose(hdlFig, event,           []);

%-- BOB_cbBtnOK
