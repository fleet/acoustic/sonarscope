% Lancement de la chaine d'exploitation du CCU log.
function BOB_cbBtnProcessCCU(hObject, ~, nbSectors)

    hdlFig      = ancestor(hObject, 'figure', 'toplevel');

    
    hControl    = findobj(hdlFig,'Tag','tagEditFileCCU');
    nomFic      = get(hControl, 'String');
    
    hControl    = findobj(hdlFig,'Tag','tagEditDateBegin');
    dateBegin   = get(hControl, 'String');

    hControl    = findobj(hdlFig,'Tag','tagEditDateEnd');
    dateEnd      = get(hControl, 'String');

    % Interpr�tation et traitement du fichier CCU.
    [flag, subCCUData, subPAROSData] = BOB_fcnExtractionCCU(...
                                                'DateBegin', dateBegin, ...
                                                'DateEnd', dateEnd, ...
                                                'NomFicCCULog', nomFic);
    if ~flag
        return
    end
    
    % Sauvegarde de la structure d'extraction des donn�es CCU.
    setappdata(hdlFig, 'subCCUData',    subCCUData);                                        
    setappdata(hdlFig, 'subPAROSData',  subPAROSData);                                        
    
    if isempty(nbSectors)
        [nbSectors, validation] = my_inputdlg(Lang('Saisissez le nombre de secteurs � traiter','Please, input the number of sectors'), {nbSectors});
        if ~validation
            return
        elseif isempty(nbSectors{:})
            return
        else
           nbSectors = nbSectors{:};
        end
    end
    
    % Structuration de la configuration nominale mesur�e
    [flag, sMeasuredData] = BOB_fcnExtractMeasuredAngles(nbSectors, subCCUData);
    if ~flag
        return
    end
    %% Affichage de la valeur de compas observ� dans le CCU.
    hControl    = findobj(hdlFig,'Tag','tagEditCCUCompass');
    set(hControl, 'String', num2str(mean(sMeasuredData.Compas.Cap)));
                                            
    %% Affichage des donn�es de recording et d'angles.
    oneFigure   = 1;
    flag        = BOB_fcnDisplayGraphsCCU(subCCUData, 'FlagOneFigure', oneFigure);  %#ok<NASGU>
    
end % BOB_cbBtnProcessCCU