function BOB_cbMenuLoadDataFromCsv(hObject, ~, idCol, colWidth)
    hdlFig = ancestor(hObject, 'figure', 'toplevel');

    % Menu de sauvegarde de l'image sous forme JPG ou BMP.
    [flag, nomFic] = my_uigetfile(...
        {   '*.csv', 'CSV file (*.csv)'}, ...
        'Load as', my_tempdir);
    if ~flag
        return
    end
    
    % Pr�paration de la chaine de format de lecture.
    nbLines     = countLines(nomFic);
    [num,txt,X] = xlsread(nomFic, '', ['A1:A' num2str(nbLines)]); %#ok<ASGLU>
    
    % On passe la ligne d'ent�te.
    for k=2:nbLines
        tabData(k-1,:) = regexp(X{k}, '\t', 'split'); %#ok<AGROW>
    end
    
    setappdata(hdlFig, 'TabData', tabData);
    
    % Mise � jour de la table sous forme HTML
    [flag, htmlTabData] = BOB_fcnCustomDataWithHtml(idCol, colWidth, tabData);
    if ~flag
        return
    end
    hdlTable = findobj(hdlFig, 'tag', 'tagTabData');
    set(hdlTable, 'data', htmlTabData);
