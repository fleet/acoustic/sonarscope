function BOB_cbMenuSaveAs(hObject, ~)

hdlFig = ancestor(hObject, 'figure', 'toplevel');

% Menu de sauvegarde de l'image sous forme JPG ou BMP.
[flag, nomFic] = my_uiputfile(...
    {   '*.png', 'PNG file (*.png)'}, ...
    'Save as', my_tempdir);
if ~flag
    return
end
[path, nomFic, ext] = fileparts(nomFic); %#ok<ASGLU>
%     ext = regexprep(ext, '\.', '');


% Fonction MatLab
% printpreview(hdlFig);
%             set(gcf,'PaperPositionMode', 'manual', ...
%         'PaperUnits','centimeters', ...
%         'Paperposition',[1 1 35 15])
% saveas(hdlFig, fullfile(pathname, 'toto'), 'tif');
rez         = 900; %resolution (dpi) of final graphic
figpos      = getpixelposition(hdlFig); %dont need to change anything here
resolution  = get(0,'ScreenPixelsPerInch'); %dont need to change anything here
set(hdlFig,'paperunits','inches','papersize',figpos(3:4)/resolution,'paperposition',[0 0 figpos(3:4)/resolution]); %dont need to change anything here
path        = 'C:\Temp\'; %the folder where you want to put the file
nomFic      = [nomFic '.png']; %what you want the file to be called
% print(hdlFig, fullfile(path, nomFic), '-dpng', ['-r', num2str(rez)], '-opengl') %save file
print(hdlFig, fullfile(path, nomFic), '-dpng', ['-r', num2str(rez)], '-image') %save file
