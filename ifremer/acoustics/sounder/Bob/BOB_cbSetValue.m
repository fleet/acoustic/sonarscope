function BOB_cbSetValue(hObject, eventData, tag) %#ok<INUSL>
    hdlFig = ancestor(hObject, 'figure', 'toplevel');

    set(gcbo,'UserData',1);
    step = sign(get(gcbo,'value'));
    set(gcbo,'value',0)

    %
    hControl = findobj(hdlFig,'Tag',tag);
    val = sscanf(get(hControl,'String'),'%d');
    val = val+step;

    val = sprintf('%d',val);
    set(hControl,'String',val)   