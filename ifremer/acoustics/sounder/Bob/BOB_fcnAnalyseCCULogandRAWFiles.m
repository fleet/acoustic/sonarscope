% Analyse et comparaison entre un fichier CCU et une arborescence de fichiers RAW.
%
% Syntax
%   flag = BOB_fcnAnalyseCCULogandRAWFiles(...)
%
% Input Arguments
%   TODO
%
% Name-Value Pair Arguments
%   Entete          :   libell� de l'entete de la fen�tre.
%   Resizable       :   option de redimensionnement de la fen�tre.
%	fileNameConfig  :   fichier de configuration nominale d'une mission BOB
%                       (XML)
%	fileNameCCU     :   d�signation d'une fichier CCU.
%	dirNameRAW      :   d�signation du r�pertoire de fichiers RAW.
%	DateBegin       :   date de d�but d'intervalle d'analyse.
%   DateEnd         :   date de fin d'intervalle d'analyse..
%
% Output Arguments
%   flag            :   flag de r�ponse OK ou Annuler.
%   newTabData      :   les donn�es peuvent avoir �t� corrig�es par
%                       l'op�rateur.
%
% Examples
%   fileNameConfig    = './ESSBOB.xml';
%   fileNameCCU       = 'F:\SonarScopeData\BOB\Data\ESSBOB_DataEtConf\20130922_ccu.log'
%   dirNameRAW        =   'F:\SonarScopeData\BOB\Data\ESSBOB2';
%   dateBegin         =  '21-Sep-2013 15:00:00', ...
%   dateEnd           = '22-Sep-2013 15:00:00');
%
%   flagOKCancel = BOB_fcnAnalyseCCULogandRAWFiles('FileNameConfig', [], ...
%                                                 'FileNameCCU', fileNameCCU, ...
%                                                 'DirNameRAW', dirNameRAW, ...
%                                                 'DateBegin', dateBegin, ...
%                                                 'DateEnd', dateEnd).
%
% See also BOB_uiSelectConfig Authors
% Authors : GLU, A. OGOR
% ----------------------------------------------------------------------------

function flag = BOB_fcnAnalyseCCULogandRAWFiles(varargin)

[varargin, fileNameConfig] = getPropertyValue(varargin, 'FileNameConfig', []);
[varargin, fileNameCCU]    = getPropertyValue(varargin, 'FileNameCCU',    []);
[varargin, dirNameRAW]     = getPropertyValue(varargin, 'DirNameRAW',     []);
[varargin, dateBegin]      = getPropertyValue(varargin, 'DateBegin',      []);
[varargin, dateEnd]        = getPropertyValue(varargin, 'DateEnd',        []); %#ok<ASGLU>

flag = 0;

%% Configuration nominale de BOB

[flagOKCancel, valueCompass, sConfigBOB] = BOB_uiSelectConfig('fileNameConfig', fileNameConfig); % './ESSBOB.xml');
if ~flagOKCancel
    return
end

%% Configuration r�elle par interpr�tation du CCU et du Compass
nbSectors = str2double(sConfigBOB.Parameters.NbSectors.Value);
[flagOKCancel, sCCUandCompassExtract, subCCUData] = BOB_uiSelectCCUandCompass( 'nomFicCCULog', ...
    fileNameCCU, ...
    'nomDirRawData', ...
    dirNameRAW, ...
    'Resizable', 'on', ...
    'NbSectors', nbSectors, ...
    'DateBegin', dateBegin, ...
    'DateEnd', dateEnd);

if ~flagOKCancel
    return
end

% Test d'existences des fichiers RAW � la racine.
listFic = dir(fullfile(sCCUandCompassExtract.Parameters.RawDir.Value,'*.raw'));
if isempty(listFic)
    str1 = 'Le r�pertoire de fichiers RAW est vide. V�rifier son contenu';
    str2 = 'Please, check Raw directory content';
    my_warndlg(Lang(str1,str2), 1);
    return
end
%% Estimation de la configuration nominale.

[flag, structEstimatedSector] = BOB_fcnEstimateAngles(valueCompass, sConfigBOB);
nbSectors                     = numel(structEstimatedSector.SectorNumber);
if ~flag
    return
end

%% Structuration de la configuration nominale mesur�e

[flag, structMeasuredSector] = BOB_fcnExtractMeasuredAngles(nbSectors, subCCUData);
if ~flag
    return
end


%% Lancement de la synth�se

[flag, nbCycles, tabData]    = BOB_fcnCreateDataAnalyser(sConfigBOB, sCCUandCompassExtract, structEstimatedSector, structMeasuredSector); %#ok<ASGLU>
if ~flag
    str1 = 'Probl�me dans la comparaison des donn�es CCU et des fichiers RAW. V�rifier le contenu du r�pertoire et/ou le fichier CCU.';
    str2 = 'Problem in comparaison CCU Log File and RAW file. Check the content directory or/and CCU Log File.';
    my_warndlg(Lang(str1,str2), 1);
    return
end

% Affichage graphique de la synth�se
[flag, ~]          = BOB_uiDataAnalyser(tabData, 'Entete', 'Synthesis for BOB Survey', 'Resizable', 'on');
if ~flag
    str1 = 'Probl�me dans l''affichage de la comparaison des donn�es CCU et des fichiers RAW. V�rifier le contenu du r�pertoire et/ou le fichier CCU.';
    str2 = 'Problem in displaying comparaison CCU Log File and RAW file. Check the content directory or/and CCU Log File.';
    my_warndlg(Lang(str1,str2), 1);
    return
end

flag = 1;

