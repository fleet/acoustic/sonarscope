% This program computes echo-integration for raw HAC files using the MOVIES3D libraries
% The HAC files are stored in folders named RUNXXX for each day of the
% survey as stored by MOVIES+ or HERMES
% The configuration for reading HAC files, filtering, and defining the cell
% size for integration and threshold are located in a dedicated folder with
% one sub folder for each threshold.
%
%
% Syntax
%   flag = BOB_fcnBatchEImultiThreshold(...)
%
% Input Arguments
%   path_hac_survey : path for HAC files 
%   path_config     : path for xml configuration files created with MOVIES3D
%   path_result     : path for .mat result files for echo-integration
%   thresholds      : list of thresolds for echo-integration,
%                   corresponding configuration have been created before
%   runs            : list of runs for echo-integration
%
% Name-Value Pair Arguments
%
% Output Arguments
%   flag        : flag de comportement de la fonction
%
% Examples
%   path_hac_survey = 'K:\Data\IFREMER\Data_SSC\SonarScopeData\BOB\TestEI';
%   path_config     = '\config\EI_Lay_Svalbard'
%   path_result     = 'K:\Data\IFREMER\Data_SSC\SonarScopeData\BOB\TestEI\Result\EI_Lay_TestEI'
%   thresholds      = -100:5:-60
%   runs            = 1:4
%   flag            = BOB_fcnBatchEImultiThreshold(path_hac_survey,path_config,path_result,thresholds,runs)
%
% See also BOB_uiSelectConfig Authors
% Authors : A. OGOR, GLU
% ----------------------------------------------------------------------------

function flag = BOB_fcnBatchEImultiThreshold(path_hac_survey,path_conf,path_result,thresholds,runs, varargin)

strFR   = 'Calcul de l''EI sur chaque répertoire RUN*';
strUS   = 'EI computing on each RUN* directory';
N       = length(runs);
hw      = create_waitbar(Lang(strFR,strUS), 'N', N);
for ir=1:N
     my_waitbar(ir, N, hw);
     for t=1:size(thresholds,2)
        str_run = sprintf('%03d', runs(ir));
        
        % Observation de la présence des fichiers HAC.
        path_hac= fullfile(path_hac_survey, ['RUN' str_run]);
        if ~exist(path_hac, 'dir')
            error([path_hac,' : Invalid path for HAC localisation'])
            return
        else
            filelist = dir([path_hac,'/*.hac']);     % ensemble des fichiers hac
            if isempty(filelist)
                 error([path_hac,' does not contain HAC files'])
                 return
            end
        end
        
        % Observation de la présence des fichiers XML.
        path_config = fullfile(path_hac, path_conf, num2str(thresholds(:,t)));
        if ~exist(path_config, 'dir')
           error([path_config,' : Invalid path for MOVIES3D configuration'])
        else
            filelistxml = dir([path_config,'/*.xml']);  % ensemble des fichiers hac
            if isempty(filelistxml)
                 error([path_config,' does not contain XML config files'])
            end
        end
     
        % Chemin de sauvegarde des fichiers EI
        path_save  = fullfile(path_result, ['RUN' str_run],num2str(thresholds(:,t)));
        
        % Remplacement des '\' pour compatibilité de fonctionnement avec
        % les scripts MOVIES.
        path_config = strrep(path_config, '\', '/');
        %% Appels des Scripts MOVIES.
        % Chargement de la configuration
        try
            % warning off; %#ok<WNOFF>
            moLoadConfig(path_config);
            % Activate 'EchoIntegrModule'
            EchoIntegrModule = moEchoIntegration();
            moSetEchoIntegrationEnabled(EchoIntegrModule);
            % Run EI
            SampleEchoIntegration(path_hac,path_save,EchoIntegrModule);
            % Disable 'EchoIntegrModule' : déverrouillage de la config
            % pour une relance éventuelle.
            moSetEchoIntegrationDisabled(EchoIntegrModule);
            clear EchoIntegrModule;
            % warning on; %#ok<WNOFF>
        catch
            % Disable 'EchoIntegrModule' : déverrouillage de la config
            % pour une relance éventuelle.
            moSetEchoIntegrationDisabled(EchoIntegrModule);
            clear EchoIntegrModule;
        end
    end
end
my_close(hw, 'MsgEnd')

flag = 1;
    
    