% Avant d'uiliser ce programme, pr�parer les fichiers de config de Movies3D
% par seuil et  par RUN s'il y en a plusieurs Clean workspace
% Calcul de l'�choint�gration sur les r�pertoires RUN contenant les
% fichiers HAC.
%
% Syntax
%   flag = BOB_fcnComputeEIByMovies(...)
%
% Input Arguments
%   nbSectors           :   nb de secteurs identifi�s pour la
%                           configuration.
%
%   structCCUandCompass :   structure contenant les param�tres d'acquisition
%                           et le r�pertoire de localisation des fichirs RAW.
%
%   structSurvey    : structure dcomportant les informations de la campagne
%                     (nom, niveaux de seuils).
%
% Name-Value Pair Arguments
%   NbSectors       : nombre de sectos identifi�s au pr�lable (� d�faut, on d�tecte le nombre de r�peroitees RUN00X).
%   NomDirRawData   : nom du r�pertoire contenant les fichiers RAW (ou HAC par�s conversion)
%
% Output Arguments
%   flag            : flag de comportement de la fonction
%
% Examples
%   flag  = BOB_fcnComputeEIByMovies(  structConfigSurvey, ...
%                                   'NbSectors',nbSectors, ...
%                                   'NomDirRawData', sCCUandCompassExtract.Parameters.RawDir.Value);
%
% See also BOB_uiSelectConfig Authors
% Authors : A. OGOR, GLU
% ----------------------------------------------------------------------------

function flag = BOB_fcnComputeEIByMovies(sConfigSurvey, varargin)

[varargin, pathRUNToProcess] = getPropertyValue(varargin, 'NomDirRawData', '');
[varargin, nbSectors]        = getPropertyValue(varargin, 'NbSectors',     []); %#ok<ASGLU>

flag = 0;

if isempty(pathRUNToProcess)
    [flag, pathRUNToProcess] = my_uigetdir(my_tempdir, Lang('Saisissez le r�pertoire racine des secteurs � traiter','Please, input Root directory of RUN*'));
    if ~flag
        return
    end
end

if isempty(nbSectors)
    listRunDir  = dir(fullfile(pathRUNToProcess, 'RUN*'));
    nbSectors   = numel(listRunDir);
end

if ~nbSectors
    str1 = 'V�rifier le r�pertoire qui contient les fichiers HAC.';
    str2 = 'Please, check the directory of Hac Data.';
    my_warndlg(Lang(str1,str2), 1);
    return
end
%% Choix du nom de la campagne du seuil de d�part.
surveyName         = sConfigSurvey.name;
firstThreshold      = sConfigSurvey.firstThreshold;
stepThreshold       = sConfigSurvey.stepThreshold;
lastThreshold       = sConfigSurvey.lastThreshold;

resultRep           = ['\Result\EI_Lay_', surveyName];
path_config         = ['\config\EI_Lay_', surveyName];
path_save           = [pathRUNToProcess,resultRep];

if ~exist(path_save,'dir')
    mkdir(path_save)
end

%% Echo-integration
thresholds      = firstThreshold:stepThreshold:lastThreshold;
%required RUNS or SECTORS (for BOB usually 5, 24, ...)
runs            = 1:nbSectors;

%% Echo-integration
warning('off')
flag = BOB_fcnBatchEImultiThreshold(pathRUNToProcess,path_config,path_save,thresholds,runs);
if ~flag
    str1 = ['Probl�me dans le calcul de l''�choint�gration via MOVIES.' ...
        'SVP, v�rifiez l''int�grit� de vos donn�es et r�pertoires ou contactez C. Scalabrin'];
    str2 = 'TODO';
    my_warndlg(Lang(str1,str2), 1);
    return
end
warning('on')

%% Bind EIlay results matrices
for ir=1:length(runs)
    for t=1:size(thresholds,2)
        str_run = sprintf('%03d', runs(ir));
        filename_ME70   = fullfile(path_save,[surveyName '-RUN' str_run '-TH' num2str(thresholds(t)) '-ME70-EIlay.mat']);
        filename_ER60   = fullfile(path_save,[surveyName '-RUN' str_run '-TH' num2str(thresholds(t)) '-ER60-EIlay.mat']);
        filename_ER60h  = fullfile(path_save,[surveyName '-RUN' str_run '-TH' num2str(thresholds(t)) '-ER60h-EIlay.mat']);
        path_results    = [fullfile(path_save,['RUN' str_run], num2str(thresholds(:,t))) '/'];
        BOB_fcnBindIntoArraysEILayerResults(path_results, filename_ME70, filename_ER60,filename_ER60h);
    end
end

flag = 1;
