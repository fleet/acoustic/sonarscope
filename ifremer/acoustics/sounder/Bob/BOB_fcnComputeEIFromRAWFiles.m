% Traitemenr complet du calcul de l'EI depuis une arborescence de fichiers
% RAW.
%
% Syntax
%   flag = BOB_fcnComputeEIFromRAWFiles(...)
%
% Input Arguments
%   TODO
%
% Name-Value Pair Arguments
%   Entete          :   libell� de l'entete de la fen�tre.
%   Resizable       :   option de redimensionnement de la fen�tre.
%	fileNameConfig  :   fichier de configuration nominale d'une mission BOB
%                       (XML)
%	fileNameCCU     :   d�signation d'une fichier CCU.
%	dirNameRAW      :   d�signation du r�pertoire de fichiers RAW.
%	DateBegin       :   date de d�but d'intervalle d'analyse.
%   DateEnd         :   date de fin d'intervalle d'analyse..
%
% Output Arguments
%   flag            :   flag de r�ponse OK ou Annuler.
%   nomFicBob       :   nom du fichier BOB de sortie vers 3DViewer.
%
% Examples
%   fileNameConfig    = './ESSBOB.xml';
%   fileNameCCU       = 'F:\SonarScopeData\BOB\Data\ESSBOB_DataEtConf\20130922_ccu.log'
%   dirNameRAW        =   'F:\SonarScopeData\BOB\Data\ESSBOB2';
%   dateBegin         =  '21-Sep-2013 15:00:00', ...
%   dateEnd           = '22-Sep-2013 15:00:00');
%
%   [flagOKCancel, nomFicBob] = BOB_fcnComputeEIFromRAWFiles('FileNameConfig', [], ...
%                                                 'FileNameCCU', fileNameCCU, ...
%                                                 'DirNameRAW', dirNameRAW, ...
%                                                 'DateBegin', dateBegin, ...
%                                                 'DateEnd', dateEnd).
%
% See also BOB_uiSelectConfig Authors
% Authors : GLU, A. OGOR
% ----------------------------------------------------------------------------

function [flag, nomFicBob] = BOB_fcnComputeEIFromRAWFiles(varargin)

[varargin, fileNameConfig] = getPropertyValue(varargin, 'FileNameConfig', []);
[varargin, fileNameCCU]    = getPropertyValue(varargin, 'FileNameCCU',    []);
[varargin, dirNameRAW]     = getPropertyValue(varargin, 'DirNameRAW',     []);
[varargin, dateBegin]      = getPropertyValue(varargin, 'DateBegin',      []);
[varargin, dateEnd]        = getPropertyValue(varargin, 'DateEnd',        []); %#ok<ASGLU>

nomFicBob = [];

%% Configuration nominale de BOB
% On calcule les variables Estim�es des Angles.

[flagOKCancel, valueCompass, sConfigBOB, ~] = BOB_uiSelectConfig('fileNameConfig', fileNameConfig); % './ESSBOB.xml');
if ~flagOKCancel
    return
end

%% Configuration r�elle par interpr�tation du CCU et du Compass

nbSectors   = str2double(sConfigBOB.Parameters.NbSectors.Value);
[flag, sCCUandCompassExtract, subCCUData, subPAROSData] = BOB_uiSelectCCUandCompass(...
    'nomFicCCULog',  fileNameCCU, ...
    'nomDirRawData', dirNameRAW, ...
    'Resizable',     'on', ...
    'NbSectors',     nbSectors, ...
    'DateBegin',     dateBegin, ...
    'DateEnd',       dateEnd); %#ok<ASGLU>
if ~flag || isempty(subCCUData)
    return
end

%% Estimation de la configuration nominale.

[flag, sEstimatedSector] = BOB_fcnEstimateAngles(valueCompass, sConfigBOB);
nbSectors                = numel(sEstimatedSector.SectorNumber);
if ~flag
    return
end

%% Structuration de la configuration nominale mesur�e

[flag, structMeasuredSector] = BOB_fcnExtractMeasuredAngles(nbSectors, subCCUData);
if ~flag
    return
end

%% Lancement de la synth�se

[flag, nbCycles, tabData] = BOB_fcnCreateDataAnalyser(sConfigBOB, sCCUandCompassExtract, sEstimatedSector, structMeasuredSector);
if ~flag
    str1 = 'Probl�me dans la comparaison des donn�es CCU et des fichiers RAW. V�rifier le contenu du r�pertoire et/ou le fichier CCU.';
    str2 = 'Problem in comparaison CCU Log File and RAW file. Check the content directory or/and CCU Log File.';
    my_warndlg(Lang(str1,str2), 1);
    return
end

% Affichage graphique de la synth�se
flag = BOB_uiDataAnalyser(tabData, 'Entete', 'Synthesis for BOB Survey', 'Resizable', 'on');
if ~flag
    return
end

%% Lancement de la correction de la table par ajout/duplication/suppression des lignes ad hoc.

str1 = 'Voulez-vous continuer le traitement par la correction semi-auto des erreurs ?';
str2 = 'Do you want to go on processing  by semi-automatic correction ?';
[rep, flag] = my_questdlg(Lang(str1,str2), 'Init', 1, ...
    Lang('Oui', 'Yes'), Lang('Annuler', 'Cancel'), 'ThirdButton', 0);
if ~flag || (rep == 2)
    flag = 0;
    return
end
newTabDataCorrect = []; %#ok<NASGU>
[flag, tabDataCorrect] = BOB_fcnCorrectData(tabData, sConfigBOB, sCCUandCompassExtract);
if ~flag
    return
end

[flag, newTabDataCorrect] = BOB_uiDataAnalyser(tabDataCorrect, ...
    'Entete', 'Synthesis after correction for Survey', ...
    'Resizable', 'on', ...
    'Editable', 1);
if ~flag
    return
end

%% R�partition des RAW files

[rep, flag] = my_questdlg(Lang(   'Voulez-vous proc�der � la r�partition des fichiers par secteurs ?', ...
    'Do you want to do dispatching Raw files by Sector ?'), ...
    'Init', 1, ...
    Lang('Oui', 'Yes'), Lang('Annuler', 'Cancel'), 'ThirdButton', 0);
if ~flag || (rep == 2)
    flag = 0;
    return
end
if isempty(newTabDataCorrect)
    newTabDataCorrect = tabData;
end
flag = BOB_fcnDispatchRawFiles(nbSectors, newTabDataCorrect, sCCUandCompassExtract);
if ~flag
    str1 = 'Probl�me dans la r�partition des fichiers RAW. V�rifier le contenu du r�pertoire et/ou le fichier CCU.';
    str2 = 'Problem in RAW files dispatching. Check the content directory or/and CCU Log File.';
    my_warndlg(Lang(str1,str2), 1);
    return
end

%% Conversion de toute l'arborescence en fichier HAC

[rep, flag] = my_questdlg(Lang(   'Voulez-vous continuer par la conversion des fichiers RAW en HAC?', ...
    'Do you want to go on conversion RAW 2 HAC Files ?'), ...
    'Init', 1, ...
    Lang('Oui', 'Yes'), Lang('Annuler', 'Cancel'), 'ThirdButton', 0);
if ~flag || (rep == 2)
    flag = 0;
    return
end
flag  = BOB_fcnMultiConvertRaw2Hac( 'NbSectors', str2double(sConfigBOB.Parameters.NbSectors.Value), ...
    'NomDirRawData', sCCUandCompassExtract.Parameters.RawDir.Value);
if ~flag
    str1 = 'Probl�me dans la conversion des RAW vers HAC. V�rifier le contenu du r�pertoire.';
    str2 = 'Problem in conversion RAW 2 HAC. Check the content directory.';
    my_warndlg(Lang(str1,str2), 1);
    return
end

%% Saisie des param�tres de la campagne

str1 = 'Voulez-vous continuer par l''op�ration de duplication?';
str2 = 'Do you want to go on Configuration duplication ?';
[rep, flag] = my_questdlg(Lang(str1,str2), 'Init', 1, ...
    Lang('Oui', 'Yes'), Lang('Annuler', 'Cancel'), 'ThirdButton', 0);
if ~flag || (rep == 2)
    flag = 0;
    return
end
[flag, structConfigSurvey] = BOB_fcnInputSurveyParameters;
if ~flag
    return
end

%% Duplication des r�pertoires de config M3D

nbSectors = str2double(sConfigBOB.Parameters.NbSectors.Value);
stepAngle = str2double(sConfigBOB.Parameters.Step.Value);
flag      = BOB_fcnDuplicateConfig(   'ConfigSurvey', structConfigSurvey, ...
    'NbSectors',     {nbSectors}, ...
    'StepAngle',     {stepAngle}, ...
    'XRotRef',       {sEstimatedSector.Transducer180_180Angle(1)}, ...
    'NomDirRawData', sCCUandCompassExtract.Parameters.RawDir.Value);

if ~flag
    str1 = 'Probl�me dans la duplication de la configuration. V�rifier le contenu du r�pertoire de r�f�rence.';
    str2 = 'Problem in configuration duplication. Check the reference directory or XML Files.';
    my_warndlg(Lang(str1,str2), 1);
    return
end

%% Purge de l'arborecence de Cache SonarScope arp�s conversion

[rep, flag] = my_questdlg(Lang(   'Voulez-vous proc�der � l''effacement des r�pertoires CACHE SonarScope. ?', ...
    'Do you want process CACHE SonarScope directories erasing ?'), 'Init', 1, ...
    Lang('Oui', 'Yes'), Lang('Annuler', 'Cancel'), 'ThirdButton', 0);
if ~flag || (rep == 2)
    flag = 0;
    return
end
flag  = BOB_fcnEraseDirCacheSSC('NbSectors', nbSectors, ...
    'NomDirRawData', sCCUandCompassExtract.Parameters.RawDir.Value);
if ~flag
    str1 = 'Probl�me dans l''effacement des r�pertoires CACHE SonarScope.';
    str2 = 'Problem in Erase SonarScope cache directories.';
    my_warndlg(Lang(str1,str2), 1);
    return
end

%% Lancement du calcul d'EI.

str1 = 'Voulez-vous continuer par l''op�ration de calcul de l''EI?';
str2 = 'Do you want to do go by EI computation ?';
[rep, flag] = my_questdlg(Lang(str1,str2), 'Init', 1, ...
    Lang('Oui', 'Yes'), Lang('Annuler', 'Cancel'), 'ThirdButton', 0);
if ~flag || (rep == 2)
    flag = 0;
    return
end
flag  = BOB_fcnComputeEIByMovies(  structConfigSurvey, ...
    'NbSectors', nbSectors, ...
    'NomDirRawData', sCCUandCompassExtract.Parameters.RawDir.Value);
if ~flag
    str1 = 'Probl�me dans le calcul de l''EI. SVP, contacter JMA ou Andr� OGOR.';
    str2 = 'Problem in Echo Integration computation. Please, contact JMA or ANDRE OGOR';
    my_warndlg(Lang(str1,str2), 1);
    return
end

%% Ecriture du fichier CCU Mat et fichier BOB d'entr�e de la cha�ne d'export vers GLOBE

[flag, nomFicBob] = BOB_fcnWriteFileConfigBOBInSSCFormat(...
    nbCycles, ...
    sConfigBOB, ...
    sCCUandCompassExtract, ...
    sEstimatedSector, ...
    structConfigSurvey);
if ~flag
    return
end
