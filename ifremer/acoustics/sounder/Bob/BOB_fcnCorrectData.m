% Affichage du r�sum� des infos d'une s�rie d'images
%
% Syntax
%   liste = displaySummary(liste, ...)
%
% Input Arguments
%   TODO.
%
% Name-Value Pair Arguments
%   TODO :
%
% Output Arguments
%   TODO 
%
% Examples
%     dummy = BOB_fcnCorrectData((tabData, structConfigBOB, structCCUandCompass)
%
% See also summary Authors
% Authors : GLU
% ----------------------------------------------------------------------------
function [flag, tabDataOut] = BOB_fcnCorrectData(tabData, structConfigBOB, structCCUandCompass)

flag = 0;

tabDataOut          = {};
sz                  = size(tabData);
nbMeasure           = sz(1);
OneMin              = 1/(24*60);
OneSec              = 1/(24*60*60);
margeHauteObsTime   = OneMin;
margeBasseObsTime   = 5*OneSec;
obsTime             = str2double(structConfigBOB.Parameters.ObservationTime.Value)/(24*60); % En min lors de la saisie de config.

% L'instruction Dir ressort syst�matiquement les noms tri�s par ordre
% alpha-num�riques (ce qui est obligatoire dans les �tapes suivantes).
listFic = dir(fullfile(structCCUandCompass.Parameters.RawDir.Value,'*.raw'));

% Cr�ation d'un tableau de dates de fichiers utilis�s pour r�partir ces
% fichiers par cr�neaux horaires.
for k=1:numel(listFic)
    idxStr              = regexp(listFic(k).name, '-D\d{8}') + 2;
    dateFic             = listFic(k).name(idxStr:idxStr+7);
    idxStr              = regexp(listFic(k).name, '-T\d{6}') + 2;
    heureFic            = listFic(k).name(idxStr:idxStr+5);
    tabDHFichierNum(k)  = datenum([dateFic ' ' heureFic], 'yyyymmdd HHMMSS');
end


idColError      = 18;
for iM=1:nbMeasure
    switch str2double(tabData{iM, idColError})
        
        case 0 % Cas nominal
            tabDataOut(end+1,:)        = tabData(iM,:);
            
        case -1 % Ecart entre panTiltPanMesure et panTiltPanSimule
            tabDataOut(end+1,:)        = tabData(iM,:);

        case -2 % Angle de Pan d�tect� comme constant.
            tabDataOut(end+1,:)        = tabData(iM,:);
            
            % Remplissage au cas par cas des champs de cette mesure.
            tabDataOut(end,1)          = tabData(iM-1,1);       % R�f�rence Cycle
            tabDataOut(end,2)          = tabData(iM-1,2);       % R�f�rence Secteur.
            tabDataOut(end,3)          = tabData(iM,3);         % Pan mesur�
            tabDataOut(end,4)          = tabData(iM-1,4);       % Pan mesur�
            tabDataOut(end,5)          = tabData(iM,5);         % Flip mesur�
            tabDataOut(end,6)          = tabData(iM-1,6);       % Flip estim�
            tabDataOut(end,7)          = tabData(iM,7);         % Geo Pan mesur�
            tabDataOut(end,8)          = tabData(iM-1,8);       % Geo Pan estim�
            tabDataOut(end,9)          = tabData(iM,9);       	% Date mesur�e
            tabDataOut(end,10)         = tabData(iM-1,10);      % Date estim�e
            tabDataOut(end,11:end)     = tabData(iM,11:end);    % Date, taille, nom du fichier
            
        case -3 % Condition sur �carts de date > 1 min.
            tabDataOut(end+1,:)        = tabData(iM,:);
            
        case -4
            % Cas d'un fichier suppl�mentaire dans l'intervalle d'observation.
            dateHeureMesure = tabData{iM, 10};
            
            % Recherche des fichiers trouv�s dans l'intervalle.
            iFicInObsTime   = find(tabDHFichierNum >= (datenum(dateHeureMesure) - margeBasseObsTime) & tabDHFichierNum <= (datenum(dateHeureMesure) + obsTime - margeHauteObsTime));
            
           for k=1:numel(iFicInObsTime)
                tabDataOut(end+1,1:11)   = tabData(iM,1:11);
                % Date du fichier
                tabDataOut{end,12}       = datestr(tabDHFichierNum(iFicInObsTime(k)), 'yyyy-mm-dd HH:MM:SS');
                % Taille du fichier
                tabDataOut{end,13}       = num2str(floor(listFic(iFicInObsTime(k)).bytes /1024));            
                % Nom du fichier
                [dirName, rootFicname, ext] = fileparts(listFic(iFicInObsTime(k)).name); %#ok<ASGLU>
                tabDataOut{end,14}       = rootFicname;            
                tabDataOut(end,15:end)   = tabData(iM,15:end);            
           end
                       
        case -5 % Calibration
            tabDataOut(end+1,:)        = tabData(iM,:);

        otherwise
            strFR = sprintf('Code d''erreur %d non reconnu pour la donn�e analys�e. SVP, Contactez JMA', tabData{iM, end-1});
            strUS = sprintf('Error Code %d from Data Analyse not recognised. Please, contact JMA.', tabData{iM, end-1});
            my_warndlg(Lang(strFR, strUS), 1);
            return

    end

end

flag = 1;




