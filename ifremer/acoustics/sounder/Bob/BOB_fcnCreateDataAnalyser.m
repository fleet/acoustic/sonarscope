% Fonction de cr�ation du tableau d'analyse et de comparaison de la
% configuration nominale avec la configuation mesur�e dans le CCU.
%
% Syntax
%   [flag, N, Data] = BOB_fcnCreateDataAnalyser(sConfigBOB, sCCUandCompass)
%
% Input Arguments
%   sConfigBOB      : structure comportant tous les param�tres de
%                     configuration.
%   sCCUandCompass  : structure comportant les param�tres de
%                     configuration de CCU et de Compass.
%
% Name-Value Pair Arguments
%
% Output Arguments
%   flag        :   flag de comportement de la fonction
%   nbCycles    :   nb de cycles de l'acquisition
%   Data        :   tableau de synth�se avec comparaison des angles Mesur�s et
%                   Simul�s
%
% Examples
%     % Configuration nominale de BOB
%     [flagOKCancel, valueCompass, sConfigBOB] = BOB_uiSelectConfig('fileNameConfig', './ESSBOB.xml');
%     if ~flagOKCancel
%         return
%     end
%     % Estimation de la configuration nominale.
%     [flag, sEstimatedSector] = BOB_fcnEstimateAngles(valueCompass, sConfigBOB);
%
%     % Configuration r�elle par interpr�tation du CCU et du Compass
%     [flagOKCancel, sCCUandCompass] = BOB_uiSelectCCUandCompass( 'nomFicCCULog', ...
%                                                             'F:\SonarScopeData\BOB\Data\ESSBOB\20130922_ccu.log', ...
%                                                             'Resizable', 'on', ...
%                                                             'NbSectors', nbSectors, ...
%                                                             'DateBegin', '21-Sep-2013 15:05:00', ...
%                                                             'DateEnd', '22-Sep-2013 15:00:00');
%     nSector         = numel(structEstimatedSector.SectorNumber);
%     stepInit        = str2double(sConfigBOB.Parameters.Step.Value);
%     durationInit    = str2double(sConfigBOB.Parameters.ObservationTime.Value);
%     dateBegin       = sCCUandCompassExtract.Parameters.DateBegin.Value;
%     dateEnd         = sCCUandCompassExtract.Parameters.DateEnd.Value;
%     % profile on
%     [flag, structFilterMeasuredSector, sMeasuredSector] = BOB_fcnExtractionCCU( 'NbSector', nSector, ...
%                                             'StepInit', stepInit, ...
%                                             'DurationInit', durationInit,  ...
%                                             'DateBegin', dateBegin, ...
%                                             'DateEnd', dateEnd, ...
%                                             'NomFicCCULog', 'F:\SonarScopeData\BOB\Data\ESSBOB\20130922_ccu.log');
% 
%     % Estimation de la configuration nominale.
%     [flag, nbCycles, tabData] = BOB_fcnCreateDataAnalyser(sConfigBOB, sCCUandCompass, sEstimatedSector, sMeasuredSector)
%
% See also uiSelectCCUAndCompass Authors
% Authors : GLU
% ----------------------------------------------------------------------------
function [flag, nbCycles, data] = BOB_fcnCreateDataAnalyser(sConfigBOB, sCCUandCompass, sEstimatedSector, sMeasuredSector)

flag = 0;

data            = [];
nbCycles        = 0;

nbSectors   = str2double(sConfigBOB.Parameters.NbSectors.Value);
obsTime     = str2double(sConfigBOB.Parameters.ObservationTime.Value)/(24*60);  % en min ramen�es en portion de jour
calibTime   = str2double(sConfigBOB.Parameters.CalibTime.Value)/(24*60);        % en min ramen�es en portion de jour
OneMin      = 1/(24*60);
OneSec      = 1/(24*60*60);
nbMeas      = size(sMeasuredSector.TemporaryPanAngle, 1);
dateBeginNum    =  datenum(sCCUandCompass.Parameters.DateBegin.Value);
dateEndNum      =  datenum(sCCUandCompass.Parameters.DateEnd.Value);

% L'instruction Dir ressort syst�matiquement les noms tri�s par ordre
% alpha-num�riques (ce qui est obligatoire dans les �tapes suivantes).
ppppFic = dir(fullfile(sCCUandCompass.Parameters.RawDir.Value,'*.raw'));
if isempty(ppppFic)
    my_warndlg(Lang('Le r�pertoire de fichiers RAW est vide. V�rifier son contenu','Please, check Raw directory content'), 1);
    return
end
% Cr�ation d'un tableau de dates de fichiers contenus dans l'intevalle de temps pour r�partir ces
% fichiers par cr�neaux horaires.
nbRawFiles      = numel(ppppFic);
listFic         = [];
tabDHFichierNum = [];
for k=1:nbRawFiles
    idxStr   = regexp(ppppFic(k).name, '-D\d{8}') + 2;
    dateFic  = ppppFic(k).name(idxStr:idxStr+7);
    idxStr   = regexp(ppppFic(k).name, '-T\d{6}') + 2;
    heureFic = ppppFic(k).name(idxStr:idxStr+5);
    dateNumFic = datenum([dateFic ' ' heureFic], 'yyyymmdd HHMMSS');
    if dateNumFic > dateBeginNum && dateNumFic < dateEndNum
        tabDHFichierNum(end+1) = datenum([dateFic ' ' heureFic], 'yyyymmdd HHMMSS'); %#ok<AGROW>
        listFic(end+1).name    = ppppFic(k).name; %#ok<AGROW>
        listFic(end).date      = ppppFic(k).date;
        listFic(end).bytes     = ppppFic(k).bytes;
        listFic(end).isdir     = ppppFic(k).isdir;
        listFic(end).datenum   = ppppFic(k).datenum;
    end
end


% Format de la matrice d'analyse : S = simul�, M = Mesur�
% | NumCycle | Num Secteur | Compas | Pan   | Flip  |  Pan Geo | Heure | Heure Fic | Nom Fic | Volume Fic | Operation
% |          |             |        | M/S   | M/S   |    M/S   |  M/S  |
% Data = zeros(nbCycles*nbSectors, 16);

% Classement des op�rations d'�talonnage si elles existent.
idxCalib = [];
if ~isempty(sMeasuredSector.Calib.DateHeure)
    for k=1:numel(sMeasuredSector.Calib.DateHeure)
        [idxCalib(k), ~] = find(sMeasuredSector.Calib.DateHeure <= sMeasuredSector.PanTilt.DateHeure, 1, 'first'); %#ok<AGROW>
    end
else
    str1 = 'Aucune mesure d''�talonnage trouv�e. Veuillez v�rifier l''intervalle de temps d''exploitation de la CCU';
    str2 = 'None calibration step found. Please, check your time interval for CCU processing';
    my_warndlg(Lang(str1,str2), 1);
    return
end

idxCrtCalib     = 1;    % Index de calibration en cours.
nbCalib         = 0;    % Nb d'�tapes de calibrations pr�alables.
iCrtCompas      = 1;
% On fige la 1�re simul� sur la 1�re mesure effectu�e : � cause du d�marrage de BOB, il y a un
% d�calage entre la consigne de Start et le d�marrage effectif (entre 2 et
% 3 min).

dateNumHeureSimule  = sMeasuredSector.PanTilt.DateHeure(1); % dateBegin;
margeHauteObsTime   = OneMin;
margeBasseObsTime   = 2*OneSec;
nbExtraFiles   = 0;
iC = 1;

%% Boucle sur les N fichiers RAW trouv�s dans l'arborescence.        
for iMeas=1:nbMeas
        iS = (iMeas - (iC-1)*nbSectors);
        if iS > nbSectors
            iC = iC + 1;
            iS = mod(iS, nbSectors);
        end
        % Index d'�tape Cycle/Sector en comptant les �tapes de
        % calibration �ventuelles.
        iMeasWithCalib  = iMeas - nbCalib;
        flagDetectCalib = 0;
       
        % Enregistrement d'une ligne d'�talonnage si l'index courant
        % y correspond. Il peut y avoir plusieurs lignes de calibration.
        if ~isempty(idxCalib) && nbCalib <= numel(idxCalib)
            for iCal=(nbCalib+1):numel(idxCalib)
                if iMeas == idxCalib(iCal)
                    strCycle            = ['Calibration' num2str(iCal)];
                    strSector           = 'NaN';
                    strCompas           = 'NaN';
                    panTiltPanMesure    = sMeasuredSector.FinalPanAngle(iMeas);
                    panTiltPanSimule    = 'NaN';
                    panTiltFlipMesure   = sMeasuredSector.Flip(iMeas);
                    panTiltFlipSimule   = 'NaN';
                    panTiltGeoMesure    = sMeasuredSector.Transducer0_360PanAngle(iMeas);
                    panTiltGeoSimule    = 'NaN';
                    dateHeureMesure     = datestr(sMeasuredSector.PanTilt.DateHeure(iMeas), 'yyyy-mm-dd HH:MM:SS');
                    
                    volumeFichier       = floor(listFic(iMeas).bytes/1024);
                    
                    % Nom du fichier
                    [dirName, rootFicname, ext] = fileparts(listFic(iMeas).name); %#ok<ASGLU>
                    rawFilename         = rootFicname;
                    
                    operation          = 'Traiter le fichier';
                    
                    % Cons�quence de la d�tection de calibration
                    idxCrtCalib        = iMeas;
                    nbCalib            = nbCalib + 1;
                    iSectorSimule      = iS;
                    flagDetectCalib    = 1;
                    break;
                end
            end
        end
        
        % Enregistrement d'une ligne de mesure "standard".
        if flagDetectCalib == 0
            strSector           = ['S' num2str(iSectorSimule)];
            % Num�ro de Cycle en tenant des d�claages dues � la Calibration.
            idx                 = floor((iMeasWithCalib-1)/nbSectors) +1;
            strCycle            = ['C' num2str(idx)];
            % Incr�ment de l'indice de compas par comparaison avec celui du
            % Pan & Tilt extrait du fichier.
            strCompas  = 'NaN';
            if (iCrtCompas < numel(sMeasuredSector.Compas.IndexPT) && ...
                sMeasuredSector.Compas.IndexPT(iCrtCompas) == sMeasuredSector.PanTilt.IndexPT(iMeas))
                    strCompas  = sprintf('%5.2f', sMeasuredSector.Compas.Cap(iCrtCompas));
                    iCrtCompas = iCrtCompas + 1;
            end
            
            panTiltPanMesure    = sMeasuredSector.FinalPanAngle(iMeas);
            panTiltPanSimule    = sEstimatedSector.FinalPanAngle(iSectorSimule);
            panTiltFlipMesure   = sMeasuredSector.Flip(iMeas);
            panTiltFlipSimule   = sEstimatedSector.Flip(iS);
            panTiltGeoMesure    = sMeasuredSector.Transducer0_360PanAngle(iMeas);
            panTiltGeoSimule    = sEstimatedSector.Transducer0_360PanAngle(iSectorSimule);
            dateHeureMesure     = datestr(sMeasuredSector.PanTilt.DateHeure(iMeas), 'yyyy-mm-dd HH:MM:SS');

            % Volume du fichier
            volumeFichier       = floor(listFic(iMeas + nbExtraFiles).bytes/1024); % En kO    

            % Nom du fichier
            [dirName, rootFicname, ext] = fileparts(listFic(iMeas + nbExtraFiles).name); %#ok<ASGLU>
            rawFilename         = rootFicname;

            operation           = Lang('Fichier � traiter', 'File to Process');
            
            iSectorSimule       = (iSectorSimule+1) - floor(iSectorSimule/nbSectors)*nbSectors;
        end

        %% Calcul des heures simul�es d'acquisition
        if iMeas == idxCrtCalib
            % Sur une �tape de calibration
            recalDate           = 0;
            margeHauteObsTime   = 5*OneSec; % 0
            
        elseif iMeas-1 == idxCrtCalib
            % Temps de calibration.
            recalDate = calibTime;
            
        elseif iMeas-2 == idxCrtCalib
            % Recalage en temps sur le secteur courant du temps
            % d'observation moins le temps de calibration.            
            recalDate = sMeasuredSector.PanTilt.DateHeure(iMeas)-sMeasuredSector.PanTilt.DateHeure(iMeas-1) - ...
                        (dateNumHeureSimule - sMeasuredSector.PanTilt.DateHeure(iMeas-1));
        else
            recalDate = obsTime;            
        end
        dateNumHeureSimule = dateNumHeureSimule + recalDate;
                
        dateHeureSimule    = datestr(dateNumHeureSimule, 'yyyy-mm-dd HH:MM:SS');
            
        %% Remplissage des colonnes.
        iErrorCodeData      = 0;
        
        % Condition sur �carts de Pan Mesur�/Simul�.
        if panTiltPanMesure ~= panTiltPanSimule
            % Ecriture rouge sur fond jaune, centr�.
            iErrorCodeData      = -1;
            operation           = Lang('Angles de Pan Mes/Sim diff�rents', 'Meas/Simu angles different.');

            % Condition sur la constance de l'angle de PAN lors du changement de
            % secteur (cas de Svalbard, secteur 4).
            if iMeas > 1 && iMeas ~= idxCrtCalib
                panTiltPanMesurePrevious = sMeasuredSector.FinalPanAngle(iMeas-1);
            else
                panTiltPanMesurePrevious = panTiltPanMesure;
            end
            if (panTiltPanMesure == panTiltPanMesurePrevious) && iMeas ~= idxCrtCalib
                % Ecriture rouge sur fond jaune-gris, centr�.
                iErrorCodeData    = -2;
                operation         = Lang('Constance dans l''angle de Pan.', 'Pan angle did''nt change.');
            end
        end
        
        % Condition sur �carts de date > 1 min.
        if (abs(datenum(dateHeureMesure) - datenum(dateHeureSimule)) > 1/(24*60))
            % Ecriture rouge sur fond orange, centr�.
            iErrorCodeData    = -3;
            operation         = Lang('Ecarts de date > 1 min.', 'Date differences are too big.');
        end
        
        % Condition sur la pr�sence de plusieurs fichiers dans l'intervalle
        % d'observation.
        % Marge haute contrainte : 5 Sec
        iFicInObsTime     = find(tabDHFichierNum >= (datenum(dateHeureSimule)) & tabDHFichierNum <= (datenum(dateHeureSimule) + recalDate - 5*OneSec));
        % Pas de marge dans 1er temps.
        if numel(iFicInObsTime) > 1
            % Le fichier de r�f�rence pour cet interval est le 1er index.
            dateHeureFichier    = datestr(tabDHFichierNum(iFicInObsTime(1)), 'yyyy-mm-dd HH:MM:SS');
            
            % Ecriture rouge sur fond rose, centr�.
            iErrorCodeData      = -4;
            operation           = Lang('Plusieurs fichiers dans l''intervalle d''observation.', 'Many files in observation time..');
            
            % Nb de fichiers en extra par secteur : sert � ne pas nommer ces fichiers dans les lignes de tableau.
            nbExtraFiles        = nbExtraFiles + (numel(iFicInObsTime) - 1);
        else
            iFicInObsTime       = find(tabDHFichierNum >= (datenum(dateHeureSimule) - margeBasseObsTime) & tabDHFichierNum <= (datenum(dateHeureSimule) + recalDate + margeHauteObsTime));
            if isempty(iFicInObsTime)
                iFicInObsTime       = find(tabDHFichierNum >= (datenum(dateHeureMesure) - margeBasseObsTime) & tabDHFichierNum <= (datenum(dateHeureMesure) + recalDate + margeHauteObsTime));
            end
            dateHeureFichier    = datestr(tabDHFichierNum(iFicInObsTime(1)), 'yyyy-mm-dd HH:MM:SS');          
        end
        
        % Condition sur Etape de type Calibration
        if contains(strCycle, 'Calibration')
            % Ecriture rouge sur fond Bleu ciel, centr�.
            iErrorCodeData    = -5;
        end
                
        iCol    = 1;
        % Num du Cycle 
        data{iMeas, iCol} = strCycle; %#ok<AGROW>
        iCol = iCol + 1;
        
        % Num du Secteur
        data{iMeas, iCol} = strSector; %#ok<AGROW>
        iCol = iCol + 1;
        
        % Compas
        data{iMeas, iCol} = strCompas; %#ok<AGROW>
        iCol = iCol + 1;
        
        % PanTilt Pan Mesur�
        data{iMeas, iCol} = num2str(panTiltPanMesure); %#ok<AGROW>
        iCol = iCol + 1;
        
        % PanTilt Pan Simul�
        data{iMeas, iCol} = num2str(panTiltPanSimule); %#ok<AGROW>     
        iCol = iCol + 1;
        
        % Flip Mesur�
        data{iMeas, iCol} = num2str(panTiltFlipMesure); %#ok<AGROW>
        iCol = iCol + 1;
        
        % Flip Simul�
        data{iMeas, iCol} = num2str(panTiltFlipSimule); %#ok<AGROW>      
        iCol = iCol + 1;
        
        % PanTilt Pan M3D Mesur�
        data{iMeas, iCol} = num2str(panTiltGeoMesure); %#ok<AGROW>
        iCol = iCol + 1;
        
        % PanTilt Pan M3D Simul�
        data{iMeas, iCol} = num2str(panTiltGeoSimule); %#ok<AGROW>     
        iCol = iCol + 1;
        
        % Date/Heure mesur�e
        data{iMeas, iCol} = dateHeureMesure; %#ok<AGROW>
        iCol = iCol + 1;

        % Date/Heure suppos�e
        data{iMeas, iCol} = dateHeureSimule; %#ok<AGROW>
        iCol = iCol + 1;
        
        % DateHeure du fichier
        data{iMeas, iCol} = dateHeureFichier; %#ok<AGROW>      
        iCol = iCol + 1;
        
        % Volume du fichier en kO  
        data{iMeas, iCol} = num2str(volumeFichier); %#ok<AGROW>
        iCol = iCol + 1;
        
        % Nom du fichier
        data{iMeas, iCol} = rawFilename; %#ok<AGROW>
        iCol = iCol + 1;
                
        % Op�ration
        data{iMeas, iCol} = operation; %#ok<AGROW>
        iCol = iCol + 1;
        
        % Champ de s�lection ou non par l'op�rateur.
        data{iMeas, iCol} = num2str(true); %#ok<AGROW>
        iCol = iCol + 1;

        % Pr�paration du champ Commentaire
        data{iMeas, iCol} = ' '; %#ok<AGROW>
        iCol = iCol + 1;
        
        % Enregistrement d'erreur de la ligne courante.
        data{iMeas, iCol} = num2str(iErrorCodeData); %#ok<AGROW>

end % Fin de la boucle sur les fichiers.

% Nb de cycles.
nbCycles = iC - 1;
flag = 1;

