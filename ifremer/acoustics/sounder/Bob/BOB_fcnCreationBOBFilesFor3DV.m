%% Creation des fichiers CCU et BOB pour import sous SSC et export dans GLOBE.
%   
% Syntax
%   flag = BOB_fcnCreationBOBFilesFor3DV(...)
%
% Input Arguments
%
% Name-Value Pair Arguments
%   
% Output Arguments
%   flag                : flag de comportement de la fonction
%
%   nomFicMatCCUSensos  : nom du fichier Mat CCU Sensors.
%
%   nomFicBOB           : nom du fichier BOB pointant vers le r�pertoire de
%                         r�sultat d'EI de la config.
%
% Examples
%     
%     [flag, nomFicCCUMat, nomFicBob] = BOB_fcnCreationBOBFilesFor3DV;
%
% See also BOB_uiSelectConfig Authors
% Authors : GLU

function [flag, nomFicCCUMat, nomFicBob] = BOB_fcnCreationBOBFilesFor3DV(varargin)

nomFicCCUMat    = [];
nomFicBob       = [];
flag            = 0;

%% Configuration nominale de BOB
% On calcule les variables Estim�es des Angles.

[flagOKCancel, valueCompass, sConfigBOB, sEstimatedSector] = BOB_uiSelectConfig; %#ok<ASGLU> % './ESSBOB.xml');
if ~flagOKCancel
    return
end

%% Configuration r�elle par interpr�tation du CCU et du Compass
nbSectors   = str2double(sConfigBOB.Parameters.NbSectors.Value);
[flagOKCancel, sCCUandCompassExtract, subCCUData, subPAROSData] = BOB_uiSelectCCUandCompass( 'Resizable', 'on', ...
                                                        'NbSectors', nbSectors); %#ok<ASGLU>

if ~flagOKCancel || isempty(subCCUData)
    return
end

nbCycles = {''};
[nbCycles, validation] = my_inputdlg(Lang('Saisissez le nombre de cycles � traiter','Please, input the number of cycles'), nbCycles);
if ~validation
    return
elseif isempty(nbCycles{:})
    return
else
   nbCycles = str2double(nbCycles);
end

%% Choix du nom de la campagne et des param�tres de seuils.
[flag, sConfigSurvey] = BOB_fcnInputSurveyParameters('InputPosition', 0);
if ~flag
    return
end


%% Ecriture du fichier CCU Mat et fichier BOB d'entr�e de la cha�ne d'export vers 3DV.

[flag, nomFicBob]       = BOB_fcnWriteFileConfigBOBInSSCFormat(...
                                        nbCycles, ...
                                        sConfigBOB, ...
                                        sCCUandCompassExtract, ...
                                        sEstimatedSector, ...
                                        sConfigSurvey); 
if ~flag
    return
end

strFR = 'Fichier .bob cr�� avec Succ�s. Vous pouvez exporter dans GLOBE.';
strUS = 'File .bob exported with success. You can export to GLOBE';
my_warndlg(Lang(strFR, strUS), 0, 'Tag', 'CreationBobFile', 'TimeDelay', 10);



flag = 1;

