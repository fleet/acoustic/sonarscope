function [flag, htmlTabData] =  BOB_fcnCustomDataWithHtml(idCol, tabColWidth, tabData)

flag        = 0;
htmlTabData = [];

% Customisation des colonnes et lignes si certaines conditions ne
% sont pas remplies.
strColor        = [];
strAlign        = '<DIV ALIGN="CENTER">';

for k=1:size(tabData,1)
    switch str2double(tabData{k,idCol.Error})
        case -1     % Ecarts de Pan Mesur�/Simul�.      
            strColor = '<body bgcolor="#FFFF00" text="#FF0000">'; % Jaune
        case -2     % Constance de l'angle de PAN lors du changement de secteur 
            strColor = '<body bgcolor="#ACAC00" text="#FF0000">'; % Jaune-gris
        case -3     % Ecarts de date > 1 min.
            strColor = '<body bgcolor="#FFC90E" text="#FF0000">'; % Orange
        case -4     % Plusieurs fichiers dans l'intervalle d'observation.
            strColor = '<body bgcolor="#FF8080" text="#FFFFFF">'; % Rose
        case -5     % Code Calibration
            strColor = '<body bgcolor="#71BFFF" text="#FF0000">'; % Bleu ciel           
        case 0
            strColor = '<body bgcolor="#FFFFFF" text="#000000">';
    end
    
    for c=1:size(tabData,2)
        strSizeCol          = ['<body width="' num2str(tabColWidth{c}) 'px">'];
        
        if c==idCol.Comment            
            htmlTabData{k, c}   = tabData{k,c};
            
        elseif c==idCol.Processing            
            htmlTabData{k, c}   = true;
            
        else
            htmlTabData{k, c}   = ['<html>' strColor strSizeCol strAlign tabData{k,c}];
        end
    end
end

flag = 1;
