% Fonction de r�partition des fichiers dans une arborescence de traitement
% EI par Movies.
%
% Syntax
%   flag = BOB_fcnDispatchRawFiles(...)
%
% Input Arguments
%   nSectors            :    nb dde secteurs � analyser.
%   tabData             :   table de donn�es cycle/secteur/fichier/statut
%   structCCUandCompass :   structure contenant les param�tres d'acquisition
%                           et le r�pertoire de localisation des fichirs RAW.
%   
% Name-Value Pair Arguments
%   TODO
%
% Output Arguments
%   flag            :   flag de r�ponse OK ou Annuler.
%   dirOut          :   r�pertoire de classement des fichiers RAW
%
% Examples
%   % Cf Test.m in BOB Processing
%   [flag, dirOut] = BOB_fcnDispatchRawFiles(nbSectors, tabData, sCCUandCompassExtract);
%
% See also BOB_uiSelectConfig Authors
% Authors : GLU, A. OGOR
% ----------------------------------------------------------------------------
function [flag, dirOut] = BOB_fcnDispatchRawFiles(nbSectors, tabData, structCCUandCompass, varargin)

flag = 0;

rawDir      = structCCUandCompass.Parameters.RawDir.Value;
sz          = size(tabData);
nbRawFiles  = sz(1);


dirOut      = uigetdir(rawDir, Lang('Entrer le r�pertoire de classement des fichiers', 'Input Directory for sort Raw Files'));
if ~dirOut
    return
end

% Cr�ation du r�pertoire Calibration.
calibDir    = fullfile(dirOut, 'Calibration');
if ~exist(calibDir, 'dir')
    [status,~,~] =  mkdir(calibDir);
    if ~status
        str1 = sprintf('Le r�pertoire %s n''a pas pu �tre cr��. V�rifier le chemin d''acc�s', calibDir);
        str2 = sprintf('Directory %s could not be created. Check the path.', calibDir);
        my_warndlg(Lang(str1,str2) , 1);
        return
    end
end

% Cr�ation du r�pertoire Erreur.
errorRawDir    = fullfile(dirOut, 'DotNotProcess');
if ~exist(errorRawDir, 'dir')
    [status,~,~] =  mkdir(errorRawDir);
    if ~status
        str1 = sprintf('Le r�pertoire %s n''a pas pu �tre cr��. V�rifier le chemin d''acc�s', errorRawDir);
        str2 = sprintf('Directory %s could not be created. Check the path.', errorRawDir);
        my_warndlg(Lang(str1,str2), 1);
        return
    end
end


for iS=1:nbSectors
    % Cr�ation des r�pertoires RUN.
    runDir{iS} = fullfile(dirOut, ['RUN' sprintf('%03d', iS)]);
    if ~exist(runDir{iS}, 'dir')
        [status,~,~] =  mkdir(runDir{iS});
        if ~status
            str1 = sprintf('Le r�pertoire %s n''a pas pu �tre cr��. V�rifier le chemin d''acc�s', runDir{iS});
            str2 = sprintf('Directory %s could not be created. Check the path.', runDir{iS});
            my_warndlg(Lang(str1,str2), 1);
            return
        end
    end
end

% Identification des colonnes dans le tableau tabData.
idColRawFile    = 14;
idColCycle      = 1;
idColSector     = 2;
idColStatut     = 18;
idColComment    = 17;
idColProcessing = 16;

hw = create_waitbar('Dispatching Raw Files in RUN*', 'N', nbRawFiles);
for k=1:nbRawFiles
    rawFile     = tabData{k, idColRawFile};
    strSector   = tabData{k, idColSector};
    statutFile  = str2double(tabData{k, idColStatut});
    flagProcess = str2double(tabData{k, idColProcessing});
    % Suppression des codes HTML.
    rawFile     = [rawFile '.raw'];
    
    % Recherche des �tapes calibration.
    if statutFile == -5 % ~isempty(strfind(strCycle, 'Calibration'))
        runDirTarget = fullfile(rawDir, 'Calibration');
    else
        iSector         = str2double(regexprep(strSector, 'S', ''));
        runDirTarget    = runDir{iSector};
    end
    
    % Si le fichier a un statut d'erreur et n�cessite d'�tre mise � l'�cart.
    if statutFile == -4 || ~flagProcess   % Code d'erreur choisi pour l'instant.
        runDirTarget = fullfile(rawDir, 'DotNotProcess');
    end
        
    % Movefile apr�s validation du processus.
    [status,msg, msgid] = movefile(fullfile(rawDir, rawFile), fullfile(runDirTarget, rawFile)); %#ok<ASGLU>
    if ~status
        str1 = sprintf('Le fichier Raw  %s n''a pas pu �tre cr��. V�rifier le chemin d''acc�s',rawFile);
        str2 = sprintf('Raw file %s could not be created. Check the path.', rawFile);
        my_warndlg(Lang(str1,str2), 1);
        my_close(hw);
        return
    end
    
    % Indication de la progression.
    my_waitbar(k, nbRawFiles, hw);
end
my_close(hw, 'MsgEnd');

flag = 1;


