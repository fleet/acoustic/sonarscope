function flag = BOB_fcnDisplayGraphsCCU(subDataExtractCCU, varargin)

flag = 0; %#ok<NASGU>

[varargin, flagOneFigure] = getPropertyValue(varargin, 'FlagOneFigure', 1); %#ok<ASGLU>

%% Affichage
if flagOneFigure == 1
    % Cr�ation d'un support aux figures.
    desktop     = com.mathworks.mde.desk.MLDesktop.getInstance;
    myGroup     = desktop.addGroup('GraphicsCCU'); %#ok<NASGU>
    desktop.setGroupDocked('GraphicsCCU', 0);
    nbColFig    = 2;
    nbRowFig    = 2;
    myDim       = java.awt.Dimension(nbRowFig, nbColFig);
    desktop.setDocumentArrangement('GraphicsCCU', 2, myDim);
    
    figH = preallocateGraphics(1, nbColFig*nbRowFig);
    for iFig=1:nbColFig*nbRowFig
        figH(iFig) = figure('WindowStyle', 'docked', ...
            'Name', sprintf('Figure %d', iFig), 'NumberTitle', 'off');
        set(get(handle(figH(iFig)), 'JavaFrame'), 'GroupName', 'GraphicsCCU');
    end
end

clear hAxes;
if flagOneFigure
    h = figH(1);
    figure(h);
else
    h = figure; %#ok<NASGU>
end

%% Affichage des �tats d'enregistrements.
subplot(2,1,1);
PlotUtils.createSScPlot(subDataExtractCCU.Fonctionnement.date_heure,subDataExtractCCU.Fonctionnement.EtatRecord(:,:),'b-*');
hAxesEtat(1)    = gca;
titre           = ['Record On/Record Off : ',subDataExtractCCU.DateDebutMission,' to ',subDataExtractCCU.DateFinMission];
title(strrep(titre,'_','\_'),'FontWeight','bold','FontSize',8);
datetick(gca, 'x','dd/mm/yy HH:MM','keepticks')
set(hAxesEtat(end),'xticklabel',{[]})
ylabel('0=Off/1=On Recording','FontWeight','bold','FontSize',8);
ylim([-1 2]);
% % rotateTicksLabel(gca,30);
grid on
hold on

subplot(2,1,2);
PlotUtils.createSScPlot(subDataExtractCCU.Fonctionnement.date_heure,subDataExtractCCU.Fonctionnement.EtatEmission(:,:),'r-o');
hAxesEtat(end+1)    = gca;
titre               = ['Transmit On/Transmit Off : ',subDataExtractCCU.DateDebutMission,' to ',subDataExtractCCU.DateFinMission];
title(strrep(titre,'_','\_'),'FontWeight','bold','FontSize',8);
xlabel('Date','FontWeight','bold','FontSize',8);
ylabel('0=Off/1=On Transmission','FontWeight','bold','FontSize',8);
ylim([-1 2]);
datetick(gca, 'x','dd/mm/yy HH:MM','keepticks')
rotateTicksLabel(hAxesEtat(end),'AngleRotation', 30);
set(hAxesEtat(end),'FontSize',6)

linkaxes(hAxesEtat, 'xy'); axis tight; drawnow;
grid on
hold on;

%% Affichage des angles de Pan&Tilt.
if flagOneFigure
    h = figH(2);
    figure(h);
else
    figure;
end
subplot(3,1,1);
titre = {'P&T Pan : ',[subDataExtractCCU.DateDebutMission,' to ',subDataExtractCCU.DateFinMission]};
plot(subDataExtractCCU.PanTilt.date_heure,subDataExtractCCU.PanTilt.pan,'b-*');
% datetick('x','dd/mm/yy HH:MM','keepticks')
hAxes(1) = gca;
datetick(gca, 'x','dd/mm/yy HH:MM','keepticks')
set(hAxes(end),'xticklabel',{[]})
xlabel('Date','FontWeight','bold','FontSize',8);
ylabel('Pan (deg)','FontWeight','bold','FontSize',8);
title(strrep(titre,'_','\_'),'FontWeight','bold','FontSize',8);
grid on
hold on;

% h = figure;
subplot(3,1,2);
titre = {'P&T Tilt : ',[subDataExtractCCU.DateDebutMission,' to ',subDataExtractCCU.DateFinMission]};
plot(subDataExtractCCU.PanTilt.date_heure,subDataExtractCCU.PanTilt.tilt,'r-+');
% datetick('x','dd/mm/yy HH:MM','keepticks')
hAxes(end+1) = gca;
datetick(gca, 'x','dd/mm/yy HH:MM','keepticks')
set(hAxes(end),'xticklabel',{[]})
xlabel('Date','FontWeight','bold','FontSize',8);
ylabel('Tilt (deg)','FontWeight','bold','FontSize',8);
title(strrep(titre,'_','\_'),'FontWeight','bold','FontSize',8);
grid on
hold on;

% h = figure;
subplot(3,1,3);
titre={'P&T Flip : ',[subDataExtractCCU.DateDebutMission,' to ',subDataExtractCCU.DateFinMission]};
plot(subDataExtractCCU.PanTilt.date_heure,subDataExtractCCU.PanTilt.retournement,'b-*');
hAxes(end+1) = gca;
datetick(gca, 'x','dd/mm/yy HH:MM','keepticks')

xlabel('Date','FontWeight','bold','FontSize',8);
ylabel('Flip value','FontWeight','bold','FontSize',8);
title(strrep(titre,'_','\_'),'FontWeight','bold','FontSize',8);
rotateTicksLabel(hAxes(end),'AngleRotation', 30);
set(hAxes,'FontSize',6)

grid on
hold on;

%% Affichage des angles de Transducteur.
if flagOneFigure
    h = figH(3);
    figure(h);
else
    figure;
end
subplot(2,1,1);
titre={'TransducerPan : ',[subDataExtractCCU.DateDebutMission,' to ',subDataExtractCCU.DateFinMission]};
plot(subDataExtractCCU.Transducteur.date_heure,subDataExtractCCU.Transducteur.pan,'r-+');
hAxes(end+1) = gca;
datetick(gca, 'x','dd/mm/yy HH:MM','keepticks')
set(hAxes(end),'xticklabel',{[]})
xlabel('Date','FontWeight','bold','FontSize',8);
ylabel('Pan (deg)','FontWeight','bold','FontSize',8);
title(strrep(titre,'_','\_'),'FontWeight','bold','FontSize',8);
grid on
hold on;

% h = figure;
subplot(2,1,2);
titre={'TransducerTilt : ',[subDataExtractCCU.DateDebutMission,' to ',subDataExtractCCU.DateFinMission]};
plot(subDataExtractCCU.Transducteur.date_heure,subDataExtractCCU.Transducteur.tilt,'g-o');
hAxes(end+1) = gca;
datetick(gca, 'x','dd/mm/yy HH:MM','keepticks')
xlabel('Date','FontWeight','bold','FontSize',8);
ylabel('Tilt (deg)','FontWeight','bold','FontSize',8);
title(strrep(titre,'_','\_'),'FontWeight','bold','FontSize',8);
rotateTicksLabel(hAxes(end),'AngleRotation', 30);
set(hAxes,'FontSize',6)

grid on
hold on;

%% Affichage des angles de Compas.
% clear hAxes
if flagOneFigure
    h = figH(4);
    figure(h);
else
    figure;
end
subplot(3,1,1);
titre={'Compass Heading : ',[subDataExtractCCU.DateDebutMission,' to ',subDataExtractCCU.DateFinMission]};
plot(subDataExtractCCU.Compass.date_heure,subDataExtractCCU.Compass.cap,'*k');
datetick(gca, 'x','dd/mm/yy HH:MM','keepticks')
hAxes(end+1) = gca;
datetick(gca,'x','dd/mm/yy HH:MM','keepticks')
set(hAxes(end),'xticklabel',{[]})
xlabel('Date','FontWeight','bold','FontSize',8);
ylabel('Heading (deg)','FontWeight','bold','FontSize',8);
title(strrep(titre,'_','\_'),'FontWeight','bold','FontSize',8);
grid on
hold on;

% h = figure;
subplot(3,1,2);
titre={'Compass Pitch : ',[subDataExtractCCU.DateDebutMission,' to ',subDataExtractCCU.DateFinMission]};
plot(subDataExtractCCU.Compass.date_heure,subDataExtractCCU.Compass.pitch,'*k');
hAxes(end+1) = gca;
datetick(gca,'x','dd/mm/yy HH:MM','keepticks')
set(hAxes(end),'xticklabel',{[]})
xlabel('Date','FontWeight','bold','FontSize',8);
ylabel('Pitch (deg)','FontWeight','bold','FontSize',8);
title(strrep(titre,'_','\_'),'FontWeight','bold','FontSize',8);
grid on
hold on;

% h = figure;
subplot(3,1,3);
titre={'Compass Roll : ', [subDataExtractCCU.DateDebutMission,' to ',subDataExtractCCU.DateFinMission]};
plot(subDataExtractCCU.Compass.date_heure,subDataExtractCCU.Compass.roll,'*k');
hAxes(end+1) = gca;
datetick(gca, 'x','dd/mm/yy HH:MM','keepticks')
set(hAxes(end),'xticklabel',{[]})
xlabel('Date','FontWeight','bold','FontSize',8);
ylabel('Roll (deg)','FontWeight','bold','FontSize',8);
title(strrep(titre,'_','\_'),'FontWeight','bold','FontSize',8);

datetick(gca, 'x','dd/mm/yy HH:MM','keepticks')
rotateTicksLabel(hAxes(end),'AngleRotation', 30);
set(hAxes,'FontSize',6)
grid on
hold on;

linkaxes(hAxes, 'x'); axis tight; drawnow;

flag = 1;

