function flag = BOB_fcnDisplayGraphsCCUParosData(subDataExtractPAROS, varargin)

[varargin, flagOneFigure] = getPropertyValue(varargin, 'FlagOneFigure', 1); %#ok<ASGLU>

if flagOneFigure
    % Cr�ation d'un support aux figures.
    h           = figure;
    desktop     = com.mathworks.mde.desk.MLDesktop.getInstance;
    % myGroup     = desktop.addGroup('myGroup');
    desktop.setGroupDocked('CCUMeasuredValues_Group', 0);
    nbColFig    = 1;
    nbRowFig    = 2;
    myDim       = java.awt.Dimension(nbRowFig, nbColFig);
    desktop.setDocumentArrangement('CCUMeasuredValues_Group', 2, myDim);
    figParos = preallocateGraphics(1, nbColFig*nbRowFig);
    for iFig = 1:nbColFig*nbRowFig
        figParos(iFig) = figure(h, 'WindowStyle', 'docked', ...
            'Name', sprintf('Figure %d', iFig), 'NumberTitle', 'off');
        set(get(handle(figParos(iFig)), 'JavaFrame'), 'GroupName', 'CCUMeasuredValues_Group');
    end
    % added condition in case PAROS is not active
    if flagOneFigure
        h = figParos(31);
        figure(h);
    else
        figure;
    end
    titre={'Pressure : ',[subDataExtractPAROS.DateDebutMission,' to ',subDataExtractPAROS.DateFinMission]};
    plot(subDataExtractPAROS.date_heure,subDataExtractPAROS.pression,'*k');
    datetick('x','dd/mm/yy HH:MM','keepticks')
    xlabel('Date','FontWeight','bold','FontSize',8);
    ylabel('Valeur pression','FontWeight','bold','FontSize',10);
    title(strrep(titre,'_','\_'),'FontWeight','bold','FontSize',8);
    grid on
    hold on;
    
    if flagOneFigure
        h = figParos(31);
        figure(h);
    else
        figure;
    end
    titre = {'Temperature : ',[subDataExtractPAROS.DateDebutMission,' to ',subDataExtractPAROS.DateFinMission]};
    plot(subDataExtractPAROS.date_heure,subDataExtractPAROS.temperature,'*k');
    datetick('x','dd/mm/yy HH:MM','keepticks')
    xlabel('Date','FontWeight','bold','FontSize',8);
    ylabel('Valeur temperature','FontWeight','bold','FontSize',10);
    title(strrep(titre,'_','\_'),'FontWeight','bold','FontSize',8);
    grid on
    hold on;
    set(gcf, 'PaperPositionMode', 'manual');
    set(gcf, 'PaperUnits', 'centimeters');
    set(gcf, 'PaperPosition', [0.63 0.63 28.41 19.72]);
    set(gcf, 'PaperOrientation','landscape');
    
end

flag = 1;

