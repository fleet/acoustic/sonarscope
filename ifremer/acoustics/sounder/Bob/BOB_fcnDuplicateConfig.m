%% Copie d'une liste de fichiers de config vers une liste de r�pertoire et cr�ation des r�pertoires le cas �ch�ant
%(par ex. copie de "*.xml"sauf CompensationParameter et EchoIntegrationParameter de -100 vers -95,-90,-85,-80,-75,-70,-65,-60).
% au pr�alable une config de r�f�rence doit �tre cr��e par Movies3D dans le r�pertoire RUN001\configDist\EI_Lay_Nom de la campagne\-100
%
% Syntax
%   flag = BOB_fcnDuplicateConfig(...)
%
% Input Arguments
%   sConfigBOB          : structure de configuration nominale de BOB
%   structCCUandCompass : structure contenant les param�tres d'acquisition
%                         et le r�pertoire de localisation des fichirs RAW.
%   sConfigSurvey       : structure des param�tres de la campagne
%
% Name-Value Pair Arguments
%   NbSectors       : nombre de sectos identifi�s au pr�lable (� d�faut, on d�tecte le nombre de r�peroitees RUN00X).
%   NomDirRawData   : nom du r�pertoire contenant les fichiers RAW (ou HAC par�s conversion)
%   ConfigSurvey    : param�tre de la configuration nominale de seuil.
%
% Output Arguments
%   flag            : flag de comportement de la fonction
%
% Examples
%     % Configuration nominale de BOB
%     [flagOKCancel, valueCompass, sConfigBOB] = BOB_uiSelectConfig('fileNameConfig', './ESSBOB.xml');
%     if ~flagOKCancel
%         return
%     end
%     % Configuration r�elle par interpr�tation du CCU et du Compass
%     nbSectors = str2double(sConfigBOB.Parameters.NbSectors.Value);
%     [flagOKCancel, sCCUandCompassExtract] = BOB_uiSelectCCUandCompass( 'nomFicCCULog', ...
%                                                             'F:\SonarScopeData\BOB\Data\ESSBOB\20130922_ccu.log', ...
%                                                             'nomDirRawData', ...
%                                                             'F:\SonarScopeData\BOB\Data\ESSBOB2', ...
%                                                             'Resizable', 'on', ...
%                                                             'NbSectors', nbSectors, ...
%                                                             'DateBegin', '21-Sep-2013 15:00:00', ...
%                                                             'DateEnd', '22-Sep-2013 15:00:00');
%
%     pathRUNToProcess      = sCCUandCompassExtract.Parameters.RawDir.Value;
%     nbSectors             = str2double(sConfigBOB.Parameters.NbSectors.Value);
%     stepAngle             = str2double(sConfigBOB.Parameters.Step.Value);
%     xRotRef               = sEstimSectors.Transducer180_180Angle(1)*pi/180;
%     Labels      =  {Lang('Nom de la campagne:', 'SurveyName:'), ...
%                     Lang('Nom du r�pertoire contenant les donn�es de r�f�rence (dB):', 'Reference threshold directory (dB):'),...
%                     Lang('1er niveau de seuil (dB):', 'First threshold value (dB):'),...
%                     Lang('Pas du seuillage (dB):', 'Step threshold value (dB):'),...
%                     Lang('Dernier niveau de seuil (dB):', 'Last threshold value (dB):')};
%     Titre       = Lang('Saisissez les param��tres de la mission', 'Choose the survey parameters');
%     NbLignes    = 1;
%     Defauts     = {'ESSBOB2','C:\SScTbxResources\BOBConfigXML','-100','5','-60'};
%     Resultats   = inputdlg(Labels, Titre, NbLignes, Defauts);
%     structConfigSurvey.name              = Resultats{1};
%     structConfigSurvey.configXMLM3D      = str2double(Resultats{2});
%     structConfigSurvey.firstThreshold    = str2double(Resultats{3});
%     structConfigSurvey.stepThreshold     = str2double(Resultats{4});
%     structConfigSurvey.lastThreshold     = str2double(Resultats{5});
%     configSurvey          = structConfigSurvey;
%
%     flag = BOB_fcnDuplicateConfig('ConfigSurvey', sConfigSurvey, ...
%                                   'NbSectors', {nbSectors}, ...
%                                   'StepAngle', {stepAngle}, ...
%                                   'XRotRef', {xRotRef})
%
% See also BOB_uiSelectConfig Authors
% Authors : A. OGOR, GLU

function flag = BOB_fcnDuplicateConfig(varargin)

global DEBUG %#ok<GVMIS> 

[varargin, pathRUNToProcess] = getPropertyValue(varargin, 'NomDirRawData', '');
[varargin, nbSectors]        = getPropertyValue(varargin, 'NbSectors',     {''});
[varargin, pasAngle]         = getPropertyValue(varargin, 'StepAngle',     {''});
[varargin, xRotRef]          = getPropertyValue(varargin, 'XRotRef',       {''});
[varargin, sConfigSurvey]    = getPropertyValue(varargin, 'ConfigSurvey',  []); %#ok<ASGLU>

flag = 0;

if isempty(pathRUNToProcess)
    [flag, pathRUNToProcess] = my_uigetdir(my_tempdir, Lang('Saisissez le r�pertoire racine des secteurs � traiter','Please, input Root directory of RUN*'));
    if ~flag
        return
    end
end

if isempty(nbSectors{:})
    [nbSectors, validation] = my_inputdlg(Lang('Saisissez le nombre de secteurs � traiter','Please, input the number of sectors'), nbSectors);
    if ~validation
        return
    elseif isempty(nbSectors{:})
        return
    else
        nbSectors = str2double(nbSectors);
    end
else
    nbSectors = nbSectors{:};
end

% Valeur du pas d'angle de chaque secteur en radian
if isempty(pasAngle{:})
    [pasAngle, validation] = my_inputdlg(Lang('Saisissez le pas angulaire de balayage (deg).','Please, input the angle Pan step (deg).'), pasAngle);
    if ~validation
        return
    elseif isempty(pasAngle)
        return
    else
        pasAngle = str2double(pasAngle{:})*pi/180;
    end
else
    pasAngle    = pasAngle{:}*pi/180;
end

% Valeur de l'angle de r�f�rence de d�marrage.
if isempty(xRotRef{:})
    [xRotRef, validation] = my_inputdlg(Lang('Saisissez le 1er angle d''orientation M3D du Transducteur (-180/+180 deg).','Please, input the first transducer M3D orientation angle (-180/+180 deg).'), xRotRef);
    if ~validation
        return
    elseif isempty(xRotRef)
        return
    else
        xRotRef = str2double(xRotRef{:})*pi/180;
    end
else
    xRotRef    = xRotRef{:}*pi/180;
end

%% Choix du nom de la campagne du seuil de d�part.
if isempty(sConfigSurvey)
    [flag, sConfigSurvey] = BOB_fcnInputSurveyParameters;
    if ~flag
        return
    end
end

surveyName          = sConfigSurvey.name;
sounderName         = sConfigSurvey.sounderName;
configXMLM3D        = sConfigSurvey.configXMLM3D;
firstThreshold      = sConfigSurvey.firstThreshold;
stepThreshold       = sConfigSurvey.stepThreshold;
lastThreshold       = sConfigSurvey.lastThreshold;
layerWidth          = sConfigSurvey.layerWidth;
firstLayerOffset    = sConfigSurvey.firstLayerOffset;
layerNumber         = sConfigSurvey.layerNumber;
posLat              = sConfigSurvey.latPosition;
posLon              = sConfigSurvey.lonPosition;
posDepth            = sConfigSurvey.depthPosition;

thresholds          = firstThreshold:stepThreshold:lastThreshold;

%% Nom du r�pertoire dans lequel seront stock�es les fichier .xml de config
path_config = ['\config\EI_Lay_' surveyName];%modifi� configDist en config

if isempty(pathRUNToProcess)
    % Liste des r�pertoires RUN
    listRep     = uigetdir(pwd,Lang('Localiser le r�pertoire des RUN*', 'Select the RUN* directory'));
else
    listRep     = pathRUNToProcess;
end

% Listage des fichiers sources .xml de r�f�rence.
% Impose d'avoir la config initiale dans le r�pertoire SScTbxResources\BOBConfigXML.
sourceXml   = configXMLM3D;
listageXml  = dir(fullfile(sourceXml, '*.xml'));


%% Dans un premier temps on cr�e les r�pertoires et on copie les fichiers .xml
% (sauf EchoIntegrationParameter.xml et CompensationParameter.xml)
% dans ces r�pertoires cr��s ou pr�-existants
% Suppression des fichiers EchoIntegrationParameter.xml et
% CompensationParameter.xml de la liste des fichiers � recopier
for k=size(listageXml,1):-1:1
    if strcmp(listageXml(k).name(1,:),'EchoIntegrationParameter.xml')==true || ...
            strcmp(listageXml(k).name(1,:),'CompensationParameter.xml')==true || ...
            strcmp(listageXml(k).name(1,:),'KernelParameter.xml')==true
        listageXml(k)=[];
    end
end

% Recopiage de la liste de fichiers restants dans chaque secteur et pour
% chaque seuil. Cr�ation du r�pertoire s'il n'existe pas.
for secteur=1:nbSectors
    for t=1:size(thresholds,2)
        for n=1:size(listageXml,1)
            nomfic = listageXml(n).name;
            chemin = fullfile(listRep,sprintf('RUN%03d', secteur),path_config ,num2str(thresholds(t)));%idxTh)));
            if ~exist (chemin,'dir')
                mkdir(chemin)
            end
            sourceCPXml =  fullfile(sourceXml, nomfic);   %fullfile(listRep,'RUN001', path_config, num2str(referenceThreshold),nomfic);
            cible       =  fullfile(listRep,sprintf('RUN%03d', secteur), path_config, num2str(thresholds(t)), nomfic); %idxTh)),nomfic);
            copyfile (sourceCPXml,cible);
            if DEBUG
                str     =  ['Fichier ',sourceCPXml,' copi� � la place de ',cible];
                disp(str);
            end
        end
    end
end
%% Modification des fichiers EchoIntegrationParameter.xml de configuration des seuils d'�choint�gration.
% On choisit les valeurs hautes et basses des seuils ainsi que le pas, puis on �crit les
% fichiers dans chaque r�pertoire.
nomfic     = 'EchoIntegrationParameter.xml';
fileEIXml  = fullfile(sourceXml, nomfic); %fullfile(listRep,'RUN001',path_config ,num2str(referenceThreshold), nomfic);

% Lecture du fichier EchoIntegrationParameter.xml pour assignation des valeurs de layers
% en distance, nombre et offset.
EchoIntegration = xml_read(fileEIXml);

% Customisation des pr�f�rences pour ne pas ajouter 'item' dans un
% champ de 2�me niveau.
Pref.ItemName   = 'item'; % name of a special tag used to itemize cell arrays
Pref.StructItem = false;  % allow arrays of structs to use 'item' notation
Pref.CellItem   = false;  % allow cell arrays to use 'item' notation
Pref.XmlEngine  = 'Matlab';  % use matlab provided XMLUtils
Pref.RootOnly   = true;  % use matlab provided XMLUtils

for secteur=1:nbSectors
    for t=1:size(thresholds,2)
        chemin                                  = fullfile(listRep,sprintf('RUN%03d', secteur),path_config ,num2str(thresholds(t)));
        EchoIntegration.LowThreshold            = thresholds(t);
        EchoIntegration.DistanceLayerNumber     = layerNumber;
        cible2                                  = fullfile(chemin,nomfic);
        
        % Traitement des cellules d'EI
        EchoIntegration = rmfield(EchoIntegration, 'LayerDefDistance');
        for L=1:layerNumber
            EchoIntegration.LayerDefDistance(L).MinDistance = (L-1)*layerWidth + firstLayerOffset;
            EchoIntegration.LayerDefDistance(L).MaxDistance = L*layerWidth + firstLayerOffset;
        end
        % Reclassement en dernier du champ BroadcastAndRecord (permutation
        % par orderfields d�licate).
        pppp = EchoIntegration.BroadcastAndRecord;
        EchoIntegration = rmfield(EchoIntegration, 'BroadcastAndRecord');
        EchoIntegration.BroadcastAndRecord = pppp;
        
        xml_write (cible2, EchoIntegration, [], Pref);
        if DEBUG
            str = ['Fichier ' cible2, ' cr��'];
            disp(str);
        end
    end
end

%% Modification des fichiers CompensationParameter.xml de configuration des valeurs d'angles.
%Le fichier du secteur 001 seuil 100 est cr�� via Movies3D. Les autres config pour le seuil -100
%des autres secteurs sont modifiees en rajoutant un pas d'angle (ici 7� en radians soit 7*pi/180)� l'angle Heading, Pitch � 90� et Roll � 0�
%puis les fichiers du
%seuil -100 seront recopi�s dans les r�p des autres seuils.

nomfic            = 'CompensationParameter.xml';
sourceCPXml       = fullfile(sourceXml,nomfic);%fullfile(listRep,sprintf('RUN%03d', secteur-1),path_config ,num2str(referenceThreshold),nomfic);
CompensationParameter   = xml_read(sourceCPXml);
pppp              = strcmp({CompensationParameter.TransducerPositionSet.TransducerPosition(:).TransducerName}, sounderName);
idxES1207D        = find(pppp == 1);

% Nouvelle saisie du nom du sondeur si il n'est pas trouv�.
while isempty(idxES1207D)
    Titre       = Lang('Veuillez resaisir le nom du sondeur :', 'Please re-input the sounder name :');
    Labels      =  { Lang('Nom du sondeur :', 'Sounder name :')};
    Defauts     = {'ES120-7D'};
    Resultats   = inputdlg(Labels, Titre, [1 40], Defauts);
    if isempty(Resultats)
        return
    end
    dummy        = Resultats{1};
    pppp         = strcmp({CompensationParameter.TransducerPositionSet.TransducerPosition(:).TransducerName}, dummy);
    idxES1207D   = find(pppp == 1);
end

for secteur=1:nbSectors
    XRot100     = xRotRef + (secteur-1)*pasAngle;
    CompensationParameter.TransducerPositionSet.TransducerPosition(idxES1207D).HeadingRot = XRot100;
    CompensationParameter.TransducerPositionSet.TransducerPosition(idxES1207D).PitchRot   = 1.570796326795;
    CompensationParameter.TransducerPositionSet.TransducerPosition(idxES1207D).RollRot    = 0;
    for t=1:size(thresholds,2)
        cible2      = fullfile(listRep,sprintf('RUN%03d', secteur),path_config,num2str(thresholds(t)),nomfic);
        
        xml_write (cible2, CompensationParameter, [], Pref);
        if DEBUG
            str1 = fullfile('Fichier ',cible2(1,:),' modifi�');
            disp(str1);
        end
    end
end

%% Modification des fichiers KernelParameter.xml et BottomDetection de configuration des positionnement.
nomFicKernel                        = 'KernelParameter.xml';
sourceCPXml                         = fullfile(sourceXml,nomFicKernel);
KernelParameter                     = xml_read(sourceCPXml);
KernelParameter.MaxRange            = layerWidth*layerNumber;
KernelParameter.UserPositionLat     = posLat;
KernelParameter.UserPositionLong    = posLon;
KernelParameter.UserPositionDepth   = posDepth;

for secteur=1:nbSectors
    for t=1:size(thresholds,2)
        cible2 = fullfile(listRep,sprintf('RUN%03d', secteur),path_config,num2str(thresholds(t)),nomFicKernel);
        xml_write (cible2, KernelParameter, [], Pref);
        if DEBUG
            str1 = fullfile('Fichier ',cible2(1,:),' modifi�');
            disp(str1);
        end
    end
end

% % my_close(hW);

flag = 1;

