% Purge de l'arborecence de Cache SonarScope arp�s conversion.
%
% Syntax
%   flag = BOB_fcnEraseDirCacheSSC(...)
%
% Input Arguments
%   nSectors            :    nb dde secteurs � analyser.
%   structCCUandCompass :   structure contenant les param�tres d'acquisition
%                           et le r�pertoire de localisation des fichirs RAW.
%
% Name-Value Pair Arguments
%   TODO
%
% Output Arguments
%   flag    :  indicateur de comportement de la fonction.
%
% Examples
%   nbSectors = 5;
%   % For�age pour test.
%   structCCUandCompass.Parameters.RawDir.Value = 'F:\SonarScopeData\BOB\Data\ESSBOB2\';
%   flag = BOB_fcnEraseDirCacheSSC('NbSectors', nbSectors, 'NomDirRawData', 'F:\SonarScopeData\BOB\Data\ESSBOB2\')
%
% See also BOB_uiSelectConfig Authors
% Authors : GLU, A. OGOR
% ----------------------------------------------------------------------------

function flag = BOB_fcnEraseDirCacheSSC(varargin)

[varargin, nbSectors]        = getPropertyValue(varargin, 'NbSectors',     []);
[varargin, pathRUNToProcess] = getPropertyValue(varargin, 'NomDirRawData', []); %#ok<ASGLU>

if isempty(pathRUNToProcess)
    [flag, pathRUNToProcess ]= my_uigetdir(my_tempdir, Lang('Saisissez le r�pertoire des donn�es RAW','Please, input RAW Directory'));
    if ~flag
        return
    end
end

if isempty(nbSectors)
    listRunDir  = dir(fullfile(RAW, 'RUN*'));
    nbSectors   = numel(listRunDir);
end

% Parcours de l'arborescence. Identification des fichiers RAW � convertir
% en fichiers HAC
hw = create_waitbar('Erase SonarScope CACHE in RUN* directories', 'N', nbSectors);
for k=1:nbSectors
    sscCacheDir   = fullfile(pathRUNToProcess, sprintf('RUN%03d', k), 'SonarScope');
    if exist(sscCacheDir, 'dir')
        rmdir(sscCacheDir, 's');
    end
    
    % Indication de la progression.
    my_waitbar(k, nbSectors, hw);
    
end
my_close(hw, 'MsgEnd');

flag = 1;
