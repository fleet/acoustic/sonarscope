% Fontion d'estimation des angles selon la configuration nominale saisie
% dans les IHM BOB_uiSelectConfig et BOB_uiSelectCCUandCompass
%
% Syntax
%   [flag, StructSector] = BOB_fcnEstimateAngles(sConfigBOB, sCCUandCompass)
%
% Input Arguments
%   sConfigBOB      : structure comportant tous les param�tres de
%                     configuration.
%   sCCUandCompass  : structure comportant les param�tres de
%                     configuration de CCU et de Compass.
%
% Name-Value Pair Arguments
%
% Output Arguments
%   flag            : flag de comportement de la fonction
%   StructSector    : calcul des angles de balayage estim�s.
%
% Examples
%     % Configuration nominale de BOB
%     [flagOKCancel, valueCompass, sConfigBOB] = BOB_uiSelectConfig('fileNameConfig', './ESSBOB.xml');
%     if ~flagOKCancel
%       return
%     end
% 
%     % Estimation de la configuration nominale.
%     [flag, StructSector] = BOB_fcnEstimateAngles(valueCompass, sConfigBOB);
%
% See also uiSelectCCUAndCompass Authors
% Authors : GLU
% ----------------------------------------------------------------------------
function [flag, StructSector] = BOB_fcnEstimateAngles(valCompass, sConfigBOB)

flag = 0; %#ok<NASGU>
%% Configuration parameters mechanically designed

%Mechanica minimum angle of the Pan&Tilt
AngleStartMovePT    = 15;

%Mechanical maximum angle of the Pan&Tilt
AngleStopMovePT     = 345;

%difference between the Pan&Tilt 0 value and the Compass 0 value. This value is mechanically fixed 
OffsetInit = str2double(sConfigBOB.Parameters.Offset.Value);

%% Configuration parameters user defined
% Heading geographical value (corresponding to the heading of the number of sectors/2 and the number of sectors/2+1)
HeadingInit     = str2double(sConfigBOB.Parameters.Cap.Value); % 355 %15;
% Aperture in degrees of the whole session transducer insonification (between 0� and 360�) 
ApertureInit    = str2double(sConfigBOB.Parameters.Aperture.Value); % 162; % 35;

% Time spent on a sector (in hour)
DurationInit    = str2double(sConfigBOB.Parameters.ObservationTime.Value); % 1;
%  Eventually, correction to take into account the difference between geographic and magnetic north
CorrectionInit  = str2double(sConfigBOB.Parameters.MNBias.Value); %0 TODO, prendre en compte la cardinal

% Angle apperture in degrees of the transducer for each sector
StepInit        = str2double(sConfigBOB.Parameters.Step.Value); 

%% Measured parameters

% Compass value after deployment in geographic degrees
CompassInit     = valCompass; % 30;


% Theoretical nb of sectors
if mod(ApertureInit(1),StepInit(1)) ~= 0 
    NbsectPT_Init= floor(ApertureInit(1)/StepInit(1)) + 1;
else
    NbsectPT_Init= floor(ApertureInit(1)/StepInit(1));
end
% PanDefaultInit=central angle of the Pan&Tilt default value according to
% configuration parameters and Compass value
if(OffsetInit-CompassInit+HeadingInit+CorrectionInit) < 360
    PanDefaultInit=OffsetInit-CompassInit+HeadingInit+CorrectionInit;
else
    PanDefaultInit=OffsetInit-CompassInit+HeadingInit+CorrectionInit-360;
end
%MinAnglePan=minimum value of the Pan&Tilt default value
if (PanDefaultInit-ApertureInit/2) < 0
    MinAnglePan=PanDefaultInit-floor(ApertureInit/2)+360;
else
    MinAnglePan=PanDefaultInit-floor(ApertureInit/2);
end
% Determination of the Temporary Pan angle
if MinAnglePan(1,1)<360
    TheorMinAnglePan(1,1) = MinAnglePan(1,1); 
else
    TheorMinAnglePan(1,1)= MinAnglePan(1,1)+360;
end
for k=2:NbsectPT_Init
    if (TheorMinAnglePan(1,k-1)+StepInit) < 360
        TheorMinAnglePan(1,k)   = TheorMinAnglePan(1,k-1)+StepInit; 
    else
        TheorMinAnglePan(1,k)   = TheorMinAnglePan(1,k-1)+StepInit-360;
    end
end

% Determination of the flip values
flip = zeros(NbsectPT_Init,1);
sub1 = (TheorMinAnglePan > AngleStopMovePT(1));
flip(sub1) = 1;
sub2 = (TheorMinAnglePan < AngleStartMovePT(1));
flip(sub2) = 1;


%Determination of the Final Pan Angle
finalPanAngle           = TheorMinAnglePan;
subFlip                 = (flip == 1);
finalPanAngle(subFlip)  = TheorMinAnglePan(subFlip)+180;
sub                     = (finalPanAngle > 360);
finalPanAngle(sub)      = finalPanAngle(sub)-360;


%Determination of the Final Tilt Angle
finalTiltAngle             = repmat(str2double(sConfigBOB.Parameters.Tilt.Value), NbsectPT_Init,1);
finalTiltAngle(~subFlip)   = 90+finalTiltAngle(~subFlip);
finalTiltAngle(subFlip)    = 270-finalTiltAngle(subFlip);


transducer0_360PanAngle     = finalPanAngle - OffsetInit(1) + CompassInit(1);
pppp                        = finalPanAngle - OffsetInit(1) + CompassInit(1);


pppp(subFlip)                       = pppp(subFlip) + 180;
transducer0_360PanAngle(subFlip)    = transducer0_360PanAngle(subFlip) + 180;
sub = (pppp > 360);
transducer0_360PanAngle(sub)        = pppp(sub)-360;
sub = (pppp < 0);
transducer0_360PanAngle(sub)        = pppp(sub)+360;


transducer0_360TiltAngle = repmat(str2double(sConfigBOB.Parameters.Tilt.Value), 1, NbsectPT_Init); 

transducer180_180Angle = zeros(NbsectPT_Init,1);
sub = (transducer0_360PanAngle < 180);
transducer180_180Angle(sub) = transducer0_360PanAngle(sub);
transducer180_180Angle(~sub) = transducer0_360PanAngle(~sub)-360;

StructSector = struct(  'Compass', CompassInit, ...
                        'SectorNumber',{1:NbsectPT_Init}, ...
                        'TemporaryPanAngle',{TheorMinAnglePan}, ...
                        'Flip',{flip}, ...
                        'FinalPanAngle',{finalPanAngle}, ...
                        'FinalTiltAngle',{finalTiltAngle}, ...
                        'Transducer0_360PanAngle',{transducer0_360PanAngle}, ...
                        'Transducer0_360TiltAngle',{transducer0_360TiltAngle}, ...
                        'Transducer180_180Angle',{transducer180_180Angle});

flag = 1;
