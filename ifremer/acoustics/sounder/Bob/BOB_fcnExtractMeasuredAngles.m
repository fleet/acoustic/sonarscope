% Fontion de mise en forme des donn�es pour pouvoir �tre visualis�s
%
% Syntax
%   [flag, structFilterMeasuredSector, structMeasuredSector] = BOB_fcnExtractMeasuredAngles(subCCUExtract)
%
% Input Arguments
%   subCCUExtract   : extraction de donn�es CCU selon un intervalle de temps.
%
% Name-Value Pair Arguments
%
% Output Arguments
%   flag                        : flag de comportement de la fonction
%   structFilterMeasuredSector  : donn�es structur�es rendues filtr�es
%   (unique , Attention � la signification !!!!)
%
% Examples
%     nSector         = numel(structEstimatedSector.SectorNumber);
%     stepInit        = str2double(sConfigBOB.Parameters.Step.Value);
%     durationInit    = str2double(sConfigBOB.Parameters.ObservationTime.Value);
%     dateBegin       = sCCUandCompassExtract.Parameters.DateBegin.Value;
%     dateEnd         = sCCUandCompassExtract.Parameters.DateEnd.Value;
%     [flag, subCCUExtract, subPAROSExtract] = BOB_fcnExtractionCCU( 'NbSector', nSector, ...
%                                             'StepInit',             stepInit, ...
%                                             'DurationInit',         durationInit,  ...
%                                             'DateBegin',            dateBegin, ...
%                                             'DateEnd',              dateEnd, ...
%                                             'NomFicCCULog', 'F:\SonarScopeData\BOB\Data\ESSBOB\20130922_ccu.log');
%
%     % Estimation de la configuration nominale.
%     [flag, structMeasuredSector] = BOB_fcnExtractMeasuredAngles(subCCUExtract);
%
% See also uiSelectCCUAndCompass Authors
% Authors : GLU
% ----------------------------------------------------------------------------
function [flag, structMeasuredSector] = BOB_fcnExtractMeasuredAngles(nbSectors, subCCUExtract)

flag = 0;  %#ok<NASGU>
%% Sauvegarde dans la structure.
% Calcul de Angle subCCUExtract.Transducteur -180� / +180�
PanMoins180Plus180      = subCCUExtract.Transducteur.pan;
sub                     = (PanMoins180Plus180 > 180);
PanMoins180Plus180(sub) = PanMoins180Plus180(sub) - 360;

%% Calcul des valeurs de compas.
[valCompas, ia, ib] = unique(subCCUExtract.Compass.cap); %#ok<ASGLU>
if std(valCompas)/mean(valCompas) > 0.1 % Valeur de fiabilit� de 10 %
    str1 = 'Valeur de compas instable. Veuillez saisir le compas manuellement';
    str2 = 'Compass value is unstable. Please input the compass Value';
    my_warndlg(Lang(str1,str2), 1);
    nomVar = {Lang('Saisie la valeur de Compas (Deg)', 'Input Compass Value(deg)')};
    value = {num2str(mean(valCompas))};
    [valCompas, flagValid] = my_inputdlg(nomVar, value, ...
        'Entete', Lang(   'Veuillez saisir le compas manuellement', ...
        'Please input the compass Value'), 1);
    if flagValid == 1
        valCompas = str2double(valCompas{:});
    end
else
    valCompas = mean(valCompas);
end

[ppppPan, ia, ~]   = unique(subCCUExtract.PanTilt.pan); %#ok<ASGLU> 

%% Mise en forme de la structure de sortie.

structMeasuredSector.PanTilt.DateHeure          = subCCUExtract.PanTilt.date_heure;
structMeasuredSector.PanTilt.IndexPT            = subCCUExtract.PanTilt.indexPanTilt;
structMeasuredSector.Calib.DateHeure            = subCCUExtract.Calib.date_heure;
structMeasuredSector.Compas.Cap                 = subCCUExtract.Compass.cap;
structMeasuredSector.Compas.IndexPT             = subCCUExtract.Compass.indexPanTilt;
structMeasuredSector.SectorNumber               = {1:nbSectors};
structMeasuredSector.TemporaryPanAngle          = subCCUExtract.PanTilt.pan;
structMeasuredSector.Flip                       = subCCUExtract.PanTilt.retournement;
structMeasuredSector.FinalPanAngle              = subCCUExtract.PanTilt.pan;
structMeasuredSector.FinalTiltAngle             = subCCUExtract.PanTilt.tilt;
structMeasuredSector.Transducer0_360PanAngle    = subCCUExtract.Transducteur.pan;
structMeasuredSector.Transducer0_360TiltAngle   = subCCUExtract.Transducteur.tilt;
structMeasuredSector.Transducer180_180Angle     = PanMoins180Plus180;


flag = 1;