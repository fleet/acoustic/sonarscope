%% Import data from text file.
% Script for importing data from the following text file:
%
%    Q:\CCUDepiaut\ccunew.log
%
% To extend the code to different selected data or a different text file,
% generate a function instead of a script.
%
%
% Valeurs et significations des param�tres qui se trouvent dans le CCU
% % 'PAROS:       07/12/11,12:24:05,0015.778,+0017.774' from link
% % 'PAROS:        date,    heure,    pression, temperature'
% %
% %
% % 'PAN&TILT:    07/12/11,12:24:19,258,110,0' from link 'CommandLink';
% % 'PAN&TILT:    date    ,heure,    pan,tilt,retournement'
% %
% % 'TRANSDUCTEUR:07/12/11,12:24:19,211,020' from link 'CommandLink'; red.
% % 'TRANSDUCTEUR:    date    ,heure    ,pan,    tilt'
% %
% % 'COMPAS:      07/12/11,12:27:09,209.0,000.1,000.8' from link
% % 'COMPAS:     date,    heure    ,cap, pitch ,roll'
% %
% % 'SERVITUDE:07/12/11,14:01:01,132,247,169,002,998,999,GGG,0' from link
% % 'SERVITUDE:    date,heure,  tens� 12V, tens� 24V, courant instantan�e 12V, courant instantan� 24V, energie restant 12V, energie restant 24V, pression interne, voie d'eau
% -------------------------------------------------------------
function [flag, subCCUExtract, subPAROSExtract] = BOB_fcnExtractionCCU(varargin)

[varargin, nomFicCCULog] = getPropertyValue(varargin, 'NomFicCCULog', '');
[varargin, dateBegin]    = getPropertyValue(varargin, 'DateBegin',    []);
[varargin, dateEnd]      = getPropertyValue(varargin, 'DateEnd',      []); %#ok<ASGLU>

subPAROSExtract = [];
subCCUExtract   = [];

%% Initialize variables.
if isempty(nomFicCCULog)
    [flag, nomFicCCULog] = my_uigetfile({'*.log'},'Choose the .log file', '20130917_expurge_ccu.log');
    if ~flag
        return
    end
end

%% D�codage du fichier CCU.
[flag, dataArray] = BOB_fcnReadCCULog(nomFicCCULog);
if ~flag
    return
end
%% Post processing for unimportable data.
% No unimportable data rules were applied during the import, so no post
% processing code is included. To generate code which works for
% unimportable data, select unimportable cells in a file and regenerate the
% script.
%% Convert the contents of columns containing numeric strings to numbers.
% Replace non-numeric strings with NaN.

raw             = dataArray(:,1);
raw(:,2)        = dataArray(:,2);
% Convert the contents of column with dates to serial date numbers using
% date format string (datenum).
dateNumeric     = datenum(dataArray(:,1), 'HH:MM:SS');
for k=1:length(dateNumeric)
    raw{k, 1}    = dateNumeric(k);
end


%% Split data into numeric and cell columns.
rawNumericColumns   = raw(:, 1);
rawCellColumns      = raw(:, 2);


%% Replace non-numeric cells with NaN

R = cellfun(@(x) ~isnumeric(x) && ~islogical(x),rawNumericColumns); % Find non-numeric cells
rawNumericColumns(R)    = []; % Delete non-numeric cells
rawCellColumns(R)       = [];

%% Allocate imported array to column variable names

DonneesCCU1         = cell2mat(rawNumericColumns(:, 1));
DonneesCCU2         = rawCellColumns(:, 1);
StringDonneesCCU    = datestr(DonneesCCU1(:,:));

%% Clear temporary variables
clearvars filen filename dataArray raw col rawNumericColumns rawCellColumns R;

%% Extraction of data from CCU file

%Initialisation des variables
lignesPAROS 		= 1;
lignesPANTILT 		= 1;
lignesCOMPAS 		= 1;
lignesTRANSDUCTEUR 	= 1;
lignesSERVITUDE 	= 1;
lignesDEM 			= 1;
lignesCALIB         = 1;
% % % lignesEMISS 		= 1;
% % % lignesSTOP 			= 1;
% % % lignesDATEHEURE 	= 1;
% % % lignesHeureCCU 		= 1;
% % % lignesStartCCU 		= 1;
% % % lignesHeureCCU 		= 1;
% % % lignesPrevPANTILT 	= 1;
% % % formatOut = 'dd/mm/yy,HH:MM:SS';

%% Using the CCU values
%
pppp    = regexp(DonneesCCU2(:,1),'Starting the CCU on','match');
sub     = cellfun(@isempty, pppp);
% R�cup�ration des indices de ligne qui comportent des dates-heures.
[IndexStartCCU, v]              = find(sub == 0); 
% lignesStartCCU                  = 1:(size(IndexStartCCU,1)+1);
%Date et heure de d�marrage de la CCU
TempStartCCU                    = regexp(DonneesCCU2(~sub),'(?<!=Starting the CCU on)\w*\s\d{02}\s\d{4}\s\d{02}:\d{02}:\d{02}','match');
TempStartCCU                    = [TempStartCCU{:}]';
CCUgeneral.StartCCUDateHeure    = datestr(TempStartCCU,'dd mm yyyy HH:MM:SS');

% Initialisation des fonctionnements Record et Emission.
Fonctionnement.RecordOn=0;
Fonctionnement.EmissionOn=0;


%% Calculate the date for each line

for session = 1:size(TempStartCCU,1)
    %session = correction de l'effet de bord � la fin de la session pour prendre en compte les
    %valeurs finales
    if session<size(TempStartCCU,1)
        % t = compteur de lignes interm�diaires entre le nieme
        % d�marrage et le (n+1)eme d�marrage (par ex aud�part entre 1
        % et 119,puis quand session change t va de 119 � ...
        intervalSession = IndexStartCCU(session):IndexStartCCU(session+1);
    else
        intervalSession = IndexStartCCU(session):size(DonneesCCU1,1);
    end
    pppp = repmat(datestr(TempStartCCU{session},'dd-mmm-yyyy HH:MM:SS'), size(intervalSession, 2), 1);
    CCUgeneral.DateHeureCCU(intervalSession,:)  = [pppp(:,1:12),StringDonneesCCU(intervalSession,13:end)];
end

DateHeureCCUNum = datenum(CCUgeneral.DateHeureCCU);
%Attention il faut int�grer le changement de jour � partir de minuit!!!!
for a = 2:size(CCUgeneral.DateHeureCCU,1)
    % TODO : perfo !!!
    if DateHeureCCUNum(a) > DateHeureCCUNum(a-1)
        CCUgeneral.DateHeureCCU(a,:) = datestr(addtodate(DateHeureCCUNum(a),1,'day'));
    end
end


%% Extracting time of start/stop of emission/recording

pppp    = regexp(DonneesCCU2(:,1),'\d{2}/\d{2}/\d{2},\d{2}:\d{2}:\d{2}','match');
sub     = cellfun(@isempty, pppp);
% R�cup�ration des indices de ligne qui comportent des dates-heures.
% % % [IndexStartCCU, v]  = find(sub == 0); %#ok<NASGU>
Tempheure               = [pppp{~sub}];
CCUgeneral.date_heure   = datenum(Tempheure,'dd/mm/yy,HH:MM:SS');


Fonctionnement.EtatEmission = repmat(Fonctionnement.EmissionOn(1,:), size(DonneesCCU2,1), 1);
Fonctionnement.EtatRecord   = repmat(Fonctionnement.RecordOn(1,:), size(DonneesCCU2,1), 1);
pppp    = regexp(DonneesCCU2(:,1),'to\sstart|to\sstop','match');
sub     = cellfun(@isempty, pppp);
% R�cup�ration des indices de ligne qui comportent des ordres Start ou
% Stop.
[IndexStartCCU, ~]  = find(sub == 0);
for k=1:size(IndexStartCCU,1)
    % Recherche de la date dans DonneesCCU quand il y en a une
    n=1;
    while isempty (regexp(DonneesCCU2{IndexStartCCU(k)-n},'\d{2}/\d{2}/\d{2},\d{2}:\d{2}:\d{2}','match'))
        n=n+1;
    end
    % D�termination de la date de d�but et de fin de la session
    Fonctionnement.date_heure_start(lignesDEM,:) = datenum(regexp(DonneesCCU2{IndexStartCCU(k)-n},'\d{2}/\d{2}/\d{2},\d{2}:\d{2}:\d{2}','match'), 'dd/mm/yy,HH:MM:SS');
    Fonctionnement.heure_fonct(lignesDEM,:)      = regexp(DonneesCCU2{IndexStartCCU(k),1},'(to\sstart|to\sstop)(\srecordings|\semissions)','match');
    if ~isempty(regexp(DonneesCCU2{IndexStartCCU(k),1},'to\sstart|to\sstop','match'))
        % D�termination de l'heure de d�marrage et d'arret du transducteur en mode Record
        % et Emission
        switch Fonctionnement.heure_fonct{lignesDEM,1}
            case 'to start recordings'
                Fonctionnement.RecordOn             = 1;
                Fonctionnement.EtatRecord(IndexStartCCU(k),1)   = Fonctionnement.RecordOn(1,:);
                Fonctionnement.EmissionOn           = 1;
                Fonctionnement.EtatEmission(IndexStartCCU(k),1) = Fonctionnement.EmissionOn(1,:);
            case 'to stop recordings'
                Fonctionnement.RecordOn             = 0;
                Fonctionnement.EtatRecord(IndexStartCCU(k),1)   = Fonctionnement.RecordOn(1,:);
                Fonctionnement.EmissionOn           = 0;
                Fonctionnement.EtatEmission(IndexStartCCU(k),1) = Fonctionnement.EmissionOn(1,:);
            otherwise
                Fonctionnement.EtatRecord(IndexStartCCU(k),1)   = Fonctionnement.RecordOn(1,:);
                Fonctionnement.EtatEmission(IndexStartCCU(k),1) = Fonctionnement.EmissionOn(1,:);
                
        end
        lignesDEM=lignesDEM+1;
    end
end

%% Processing PAROS Data

% Extracting Paros data(temperature, pressure), then Pan&Tilt data (pan, tilt, flip), then Compass data(heading, pitch,
% Roll) and finally Transducer data(pan, tilt) and Service data(energy)

for k = 1:size(DonneesCCU2,1)
    flagParosData           = ~isempty(regexp(DonneesCCU2{k,1},'PAROS:(?=\s+\d+)','match'));
    flagPanTiltData         = ~isempty(regexp(DonneesCCU2{k,1},'PAN&TILT:(?=\s+\d+)','match'));
    flagCompasData          = ~isempty(regexp(DonneesCCU2{k,1},'COMPAS:(?=\s+\d+)','match'));
    flagTransducteurData    = ~isempty(regexp(DonneesCCU2{k,1},'TRANSDUCTEUR:(?=\d+)','match'));
    flagServitudeData       = ~isempty(regexp(DonneesCCU2{k,1},'SERVITUDE:(?=\d+)','match'));
    
    if flagParosData
        Paros.date_heure(lignesPAROS,:)         = datenum(regexp(DonneesCCU2{k,1},'(?<!=PAROS:)\d{2}/\d{2}/\d{2},\d{2}:\d{2}:\d{2}','match'),'dd/mm/yy,HH:MM:SS');
        Paros.pression(lignesPAROS,:)           = str2double(regexp(DonneesCCU2{k,1},'(?<!=PAROS:)\d{4}.\d{3}','match','once'));
        Paros.temperature(lignesPAROS,:)        = str2double(regexp(DonneesCCU2{k,1},'(?<!=PAROS:)\+\d{4}.\d{3}','match','once'));
        lignesPAROS                             = lignesPAROS+1;
    elseif flagPanTiltData
        PanTilt.date_heure(lignesPANTILT,:)     = datenum(regexp(DonneesCCU2{k,1},'(?<!=PAN&TILT:)\d{2}\/\d{2}\/\d{2},\d{2}:\d{2}:\d{2}','match'),'dd/mm/yy,HH:MM:SS');
        pppp                                    = regexp(DonneesCCU2{k,1},'(?!=PAN&TILT:)\d{3}','match');
        PanTilt.pan(lignesPANTILT,1)            = str2double(cell2mat(pppp(1)));
        PanTilt.tilt(lignesPANTILT,1)           = str2double(cell2mat(pppp(2)));
        PanTilt.retournement(lignesPANTILT,:)   = str2double(cell2mat(regexp(DonneesCCU2{k,1},'(?<!=PAN&TILT:)\d{1}(?='')','match')));
        PanTilt.indexPanTilt(lignesPANTILT,1)   = lignesPANTILT;
        lignesPANTILT                           = lignesPANTILT+1;
        
        % D�tection d'une s�quence d'�talonnage �ventuel sur cette �tape.
        % Abstraction des erreurs �ventuelles de feedback.
        EtalonnageFeedbackError = ~isempty(regexp(DonneesCCU2{k-2,1},'A:Pan erreur commande feedback','match'));
        if EtalonnageFeedbackError
            idxEtalonnage = k-3;
        else
            idxEtalonnage = k-1;
        end
        flagCalib = ~isempty(regexp(DonneesCCU2{idxEtalonnage,1},'PAN&TILT:(?=\s+Etalonnage)','match'));
        if flagCalib
            Calib.date_heure(lignesCALIB)       = datenum(regexp(DonneesCCU2{k,1},'(?<!=PAN&TILT:)\d{2}\/\d{2}\/\d{2},\d{2}:\d{2}:\d{2}','match'),'dd/mm/yy,HH:MM:SS');
            Calib.refPANTILT(lignesCALIB)       = lignesPANTILT-1;
            lignesCALIB                         = lignesCALIB+1;
            flagCalib                           = 0; %#ok<NASGU>
        end
        
        
    elseif flagCompasData
        % Dans le fichier 20130922_ccu.log, il y a une valeur de Compas
        % toutes les heures : il nous faut remplir les valeurs pour
        % tous les cycles observ�s.
        % IndexCompass(lignesCOMPAS,1)             = k;
        Compass.date_heure(lignesCOMPAS,:)       = datenum(regexp(DonneesCCU2{k,1},'(?<!=Compass:)\d{2}\/\d{2}\/\d{2},\d{2}:\d{2}:\d{2}','match'),'dd/mm/yy,HH:MM:SS');
        pppp                                     = regexp(DonneesCCU2{k,1},'(?!=Compass:)\d{3}\.\d{1}','match');
        Compass.cap(lignesCOMPAS,1)              = str2double(cell2mat(pppp(1)));
        Compass.pitch(lignesCOMPAS,1)            = str2double(cell2mat(pppp(2)));
        Compass.roll(lignesCOMPAS,1)             = str2double(cell2mat(pppp(3)));
        Compass.indexPanTilt(lignesCOMPAS,1)     = lignesPANTILT;
        lignesCOMPAS                             = lignesCOMPAS+1;
        
    elseif flagTransducteurData
        % IndexTransducteur(lignesTRANSDUCTEUR,1) = k;
        Transducteur.date_heure(lignesTRANSDUCTEUR,:)   = datenum(regexp(DonneesCCU2{k,1},'(?<!=TRANSDUCTEUR:)\d{2}\/\d{2}\/\d{2},\d{2}:\d{2}:\d{2}','match'),'dd/mm/yy,HH:MM:SS');
        pppp                                            = regexp(DonneesCCU2{k,1},'(?!=TRANSDUCTEUR:)\d{3}','match');
        Transducteur.pan(lignesTRANSDUCTEUR,1)          = str2double(cell2mat(pppp(1)));
        Transducteur.tilt(lignesTRANSDUCTEUR,1)         = str2double(cell2mat(pppp(2)));
        lignesTRANSDUCTEUR                              = lignesTRANSDUCTEUR+1;
        
    elseif flagServitudeData
        % IndexServitude(lignesSERVITUDE,1)               = k;
        Servitude.date_heure(lignesSERVITUDE,:)         = datenum(regexp(DonneesCCU2{k,1},'(?<!=SERVITUDE:)\d{2}\/\d{2}\/\d{2},\d{2}:\d{2}:\d{2}','match'),'dd/mm/yy,HH:MM:SS');
        pppp                                            =  regexp(DonneesCCU2{k,1},'(?!=SERVITUDE:)\d{3}','match');
        pppp2                                           = regexp(DonneesCCU2{k,1},'(?!=SERVITUDE:\d{2}\/\d{2}\/\d{2},\d{2}:\d{2}:\d{2}\,)\d*(?='')','match');
        Servitude.tens12V(lignesSERVITUDE,1)            = str2double(cell2mat(pppp(1)));
        Servitude.tens24V(lignesSERVITUDE,1)            = str2double(cell2mat(pppp(2)));
        Servitude.instant12V(lignesSERVITUDE,1)         = str2double(cell2mat(pppp(3)));
        Servitude.instant24V(lignesSERVITUDE,1)         = str2double(cell2mat(pppp(4)));
        Servitude.restant12V(lignesSERVITUDE,1)         = str2double(cell2mat(pppp(5)));
        Servitude.restant24V(lignesSERVITUDE,1)         = str2double(cell2mat(pppp(6)));
        Servitude.intpression(lignesSERVITUDE,1)        = regexp(DonneesCCU2{k,1},'(?!=SERVITUDE:)([A-Z]{3})(?=,)','match');%str2double(cell2mat(Servitude.valeurs(lignesSERVITUDE,6)));
        Servitude.voieeau(lignesSERVITUDE,1)            = str2double(cell2mat(pppp2(1)));
        lignesSERVITUDE                                 = lignesSERVITUDE+1;
    end
end


%% Config des graphiques
% % % % DateDebutMission        = CCUgeneral.DateHeureCCU(1,:);
% % % % DateFinMission          = CCUgeneral.DateHeureCCU(end,:);
DateDebutSession        = CCUgeneral.DateHeureCCU(IndexStartCCU,:);
DateFinSession          = CCUgeneral.DateHeureCCU(IndexStartCCU(2:end,:)-1,:);
DateFinSession(end+1,:) = CCUgeneral.DateHeureCCU(end,:);
% % % DureeSession(1:size(IndexStartCCU,1),1) = datenum(DateFinSession(1:end,:)) - datenum(DateDebutSession(1:end,:));
% % % % days                    = num2str(floor(DureeSession));
% % % % hrs                     = datestr(DureeSession, 'HH');
% % % % mins                    = datestr(DureeSession, 'MM');
% % % % secs                    = datestr(DureeSession, 'SS');
% % %


%% Filtrage selon les dates et heures d'intervalle saisies.
subCCUExtract.DateDebutMission    = dateBegin;
subCCUExtract.DateFinMission      = dateEnd;
dateBegin           = datenum(dateBegin);
dateEnd             = datenum(dateEnd);
subTransducteur     = (Transducteur.date_heure >= dateBegin & Transducteur.date_heure <= dateEnd);
subCompas           = (Compass.date_heure >= dateBegin & Compass.date_heure <= dateEnd);
subFonctionnement   = (DateHeureCCUNum >= dateBegin & DateHeureCCUNum <= dateEnd);
subPanTilt          = (PanTilt.date_heure >= dateBegin & PanTilt.date_heure <= dateEnd);
subCalib            = (Calib.date_heure >= dateBegin & Calib.date_heure <= dateEnd);

%% Extraction des valeurs des Structures : Transducteur, Compas, Fonctionnement et PanTilt.
% Tranducteur
f   = fieldnames(Transducteur);
for k=1:numel(f)
    subCCUExtract.Transducteur.(f{k}) = Transducteur.(f{k})(subTransducteur);
end

% PanTilt
f   = fieldnames(PanTilt);
for k=1:numel(f)
    subCCUExtract.PanTilt.(f{k}) = PanTilt.(f{k})(subPanTilt);
end

% Calibration/Etalonnage
f   = fieldnames(Calib);
for k=1:numel(f)
    subCCUExtract.Calib.(f{k}) = Calib.(f{k})(subCalib);
end

% Compass
f   = fieldnames(Compass);
for k=1:numel(f)
    subCCUExtract.Compass.(f{k})    = Compass.(f{k})(subCompas);
end

% La structure Fonctionnement n'est pas homog�ne. Les r�f�rences de temps
% sont DateHeureCCUNum
f = {'EtatEmission','EtatRecord'};
for k=1:numel(f)
    subCCUExtract.Fonctionnement.(f{k}) = Fonctionnement.(f{k})(subFonctionnement);
end
subCCUExtract.Fonctionnement.date_heure = DateHeureCCUNum(subFonctionnement);

%% Traitement et affichage des donn�es de capteurs PAROS
if exist('Paros', 'var')
    %% Filtrage selon les dates et heures d'intervalle saisies.
    subPAROSExtract.DateDebutMission    = subCCUExtract.DateDebutMission;
    subPAROSExtract.DateFinMission      = subCCUExtract.DateFinMission;
    subParos     = (Paros.date_heure >= dateBegin & Paros.date_heure <= dateEnd);
    % Extraction des valeurs des Structures : Transducteur, Compas, Fonctionnement et PanTilt.
    f = fieldnames(Paros);
    for k=1:numel(f)
        subPAROSExtract.Paros.(f{k}) = Paros.(f{k})(subParos);
    end
    
end


flag = 1;
return
