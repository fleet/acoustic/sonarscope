% Création des boutons des boutons de validation ou annulation.
function flag = BOB_fcnInitPnlOKSaveRestore(hdlPnl)

flag = 0;  %#ok<NASGU>
                
%% Affichage des boutons Save/Restore
%% Affichage des boutons Save/Restore
% hLabelFileConfig  = uicontrol('Style', 'text', ...
%                     'Parent',   hdlPnl,...
%                     'Units',    'normalized',...
%                     'HorizontalAlignment','left',...
%                     'Position', [0.1 0.9 0.7 0.1],...
%                     'Tag',      'tagLabelFileConfig',...
%                     'String',   Lang('Fichier de configuration', 'Configuration File')); %#ok<NASGU>

hEditFileConfig  = uicontrol('Style', 'edit', ...
                    'Parent',   hdlPnl,...
                    'String',   '', ...
                    'Enable',   'on', ...
                    'Units',    'normalized',...
                    'HorizontalAlignment','center',...
                    'Position', [0.05 0.60 0.85 0.3],...
                    'Tag',      'tagEditFileConfig', ...
                    'BackgroundColor', 'w'); %#ok<NASGU>

hBtnInputFileConfig  =     uicontrol(...
                    'Parent',   hdlPnl,...
                    'Units',    'normalized',...
                    'Position', [0.9 0.60 0.05 0.3],...
                    'Tag',      'tagBtnBegin',...
                    'String',   '...', ...
                    'Tooltip',  Lang('Entrer le fichier XML contenant les informations de configuration','Input Configuration File in XML format'), ...
                    'CallBack', {@cbBtnSetFileConfig, 'tagEditFileConfig'}); %#ok<NASGU>

hBtnSave  =     uicontrol(...
                    'Parent',   hdlPnl,...
                    'Units',    'normalized',...
                    'Position', [0.1 0.15 0.3 0.4],...
                    'Tag',      'tagBtnOk',...
                    'String',   Lang('Sauvegarde','Save'), ...
                    'Tooltip',  Lang('Sauvegarde XML du fichier','Save XML file Config'), ...
                    'CallBack', @cbBtnSave); %#ok<NASGU>
    
hBtnRestore =    uicontrol(...
                    'Parent',   hdlPnl,...
                    'Units',    'normalized',...
                    'Position', [0.6 0.15 0.3 0.4],...
                    'Tag',      'tagBtnRestore',...
                    'String',   Lang('Restauration','Restore'), ...
                    'Tooltip',  Lang('Restrautration d''une configuration','Restore'), ...
                    'CallBack', @cbBtnRestore); %#ok<NASGU>
flag = 1;

% = Callback ===============
function cbBtnSave(hObject, ~)

    hdlFig  = ancestor(hObject, 'figure', 'toplevel');
    
    % Sauvegarde de la structure
    ConfigBOB.Comments  = 'Fichier de configuration des données de mesure de BOB';
    ConfigBOB.Version   = '20131219';
    ConfigBOB.Parameters.MeasurementBegin.Value = get(findobj(hdlFig, 'Tag', 'tagEditDateBegin'), 'String');
    ConfigBOB.Parameters.MeasurementBegin.Unit = 'dd-mmm-yyyy hh:mm:ss:FFF';
    ConfigBOB.Parameters.MeasurementEnd.Value  = get(findobj(hdlFig, 'Tag', 'tagEditDateEnd'), 'String');
    ConfigBOB.Parameters.MeasurementEnd.Unit = 'dd-mmm-yyyy hh:mm:ss:FFF';

    ConfigBOB.Parameters.Cap.Value  = get(findobj(hdlFig, 'Tag', 'tagEditCap'), 'String');
    ConfigBOB.Parameters.Cap.Unit   = 'deg';
    
    ConfigBOB.Parameters.MNBias.Value  = get(findobj(hdlFig, 'Tag', 'tagEditNordMagnetic'), 'String');
    ConfigBOB.Parameters.MNBias.Unit   = 'deg';

    h       = findobj(hdlFig, 'Tag', 'tagEditNordMagneticCard');
    pppp    = get(h, 'String');
    ConfigBOB.Parameters.MNBiasCard.String  = pppp{get(h, 'Value')};
    ConfigBOB.Parameters.MNBiasCard.Value   = get(h, 'Value');
    ConfigBOB.Parameters.MNBiasCard.Unit    = '';

    ConfigBOB.Parameters.Aperture.Value  = get(findobj(hdlFig, 'Tag', 'tagEditAperture'), 'String');
    ConfigBOB.Parameters.Aperture.Unit   = 'deg';

    ConfigBOB.Parameters.ObservationTime.Value  = get(findobj(hdlFig, 'Tag', 'tagEditDuree'), 'String');
    ConfigBOB.Parameters.ObservationTime.Unit   = 'min';

    ConfigBOB.Parameters.CalibInterval.Value  = get(findobj(hdlFig, 'Tag', 'tagEditPeriode'), 'String');
    ConfigBOB.Parameters.CalibInterval.Unit   = 'Nb Cycle';

    ConfigBOB.Parameters.CalibTime.Value  = get(findobj(hdlFig, 'Tag', 'tagEditDureeCalib'), 'String');
    ConfigBOB.Parameters.CalibTime.Unit   = 'min';

    ConfigBOB.Parameters.NbCycleON.Value  = get(findobj(hdlFig, 'Tag', 'tagEditNbCycleOn'), 'String');
    ConfigBOB.Parameters.NbCycleON.Unit   = '';

    ConfigBOB.Parameters.TimeOFF.Value  = get(findobj(hdlFig, 'Tag', 'tagEditDureeOff'), 'String');
    ConfigBOB.Parameters.TimeOFF.Unit   = '';

    % Détermination du fichier de sauvegarde de la configuration.
    hControl    = findobj(hdlFig,'Tag','tagEditFileConfig');
    % Détermination du fichier
    if ~isempty(get(hControl, 'String'))
    else
        [FileName,PathName,~] = uiputfile({'*.xml'});
        if ~FileName
            return
        else
            nomFicSave = fullfile(PathName, FileName);
            xml_write(nomFicSave, ConfigBOB);
            h = findojb(hdlFig, 'Tag', 'tagEditFileConfig');
            set(h, 'String', nomFicSave);
        end
    end

% = Callback ===============
function cbBtnSetFileConfig(hObject, ~, tag)

    hdlFig  = ancestor(hObject, 'figure', 'toplevel');

    hControl = findobj(hdlFig,'Tag',tag);    
    [flag, nomFic] = my_uigetfile({'*.xml'}, 'MultiSelect', 'off');
    if ~flag
        return
    end
% BOB_cbClose
