% Création des boutons des boutons de validation ou annulation.
function flag = BOB_fcnInitPnlSaveRestore(hdlPnl, nomFic)

flag = 0;  %#ok<NASGU>
                
%% Affichage des boutons Save/Restore
%% Affichage des boutons Save/Restore
% hLabelFileConfig  = uicontrol('Style', 'text', ...
%                     'Parent',   hdlPnl,...
%                     'Units',    'normalized',...
%                     'HorizontalAlignment','left',...
%                     'Position', [0.1 0.9 0.7 0.1],...
%                     'Tag',      'tagLabelFileConfig',...
%                     'String',   Lang('Fichier de configuration', 'Configuration File')); %#ok<NASGU>

hEditFileConfig  = uicontrol('Style', 'edit', ...
                    'Parent',   hdlPnl,...
                    'String',   nomFic, ...
                    'Enable',   'on', ...
                    'Units',    'normalized',...
                    'HorizontalAlignment','center',...
                    'Position', [0.05 0.55 0.85 0.4],...
                    'Tag',      'tagEditFileConfig', ...
                    'BackgroundColor', 'w'); %#ok<NASGU>

hBtnInputFileConfig  =     uicontrol(...
                    'Parent',   hdlPnl,...
                    'Units',    'normalized',...
                    'Position', [0.9 0.55 0.05 0.4],...
                    'Tag',      'tagBtnBegin',...
                    'String',   '...', ...
                    'Tooltip',  Lang('Entrer le fichier XML contenant les informations de configuration','Input Configuration File in XML format'), ...
                    'CallBack', {@cbBtnSetFileConfig, 'tagEditFileConfig'}); %#ok<NASGU>

hBtnSave  =     uicontrol(...
                    'Parent',   hdlPnl,...
                    'Units',    'normalized',...
                    'Position', [0.1 0.15 0.3 0.4],...
                    'Tag',      'tagBtnOk',...
                    'String',   Lang('Sauvegarde','Save'), ...
                    'Tooltip',  Lang('Sauvegarde XML du fichier','Save XML file Config'), ...
                    'CallBack', @cbBtnSave); %#ok<NASGU>
    
hBtnRestore =    uicontrol(...
                    'Parent',   hdlPnl,...
                    'Units',    'normalized',...
                    'Position', [0.6 0.15 0.3 0.4],...
                    'Tag',      'tagBtnRestore',...
                    'String',   Lang('Restauration','Restore'), ...
                    'Tooltip',  Lang('Restauration d''une configuration','Restore'), ...
                    'CallBack', @cbBtnRestore); %#ok<NASGU>

                
if ~isempty(nomFic)
    hdlFig = ancestor(hdlPnl, 'figure', 'toplevel');
    % Appel de la fonction de restauration des données de configurations.
    flag = BOB_fcnRestoreConfig(hdlFig, nomFic); %#ok<NASGU>
end

flag = 1;

% = Callback ===============
function cbBtnSave(hObject, ~)

    hdlFig    = ancestor(hObject, 'figure', 'toplevel');
    
    % Mise en forme de la structure BOB.
    [flag, ConfigBOB] = BOB_fcnSaveConfig(hdlFig);
    if ~flag
        return
    end
    
    % Détermination du fichier de sauvegarde de la configuration.
    hControl    = findobj(hdlFig,'Tag','tagEditFileConfig');
    % Détermination du fichier
    if isempty(get(hControl, 'String'))
        [FileName,PathName,~] = uiputfile({'*.xml'});
        if ~FileName
            return
        else
            nomFicSave = fullfile(PathName, FileName);
            h = findobj(hdlFig, 'Tag', 'tagEditFileConfig');
            set(h, 'String', nomFicSave);
        end
    else
        nomFicSave = get(hControl, 'String');
    end
    xml_write(nomFicSave, ConfigBOB);

% -- cbBtnSave

% = Callback ===============
function cbBtnRestore(hObject, ~)

    persistent oldDir
    
    if isempty(oldDir)
        oldDir = my_tempdir;
    end
    
    hdlFig      = ancestor(hObject, 'figure', 'toplevel');
    % Détermination du fichier de sauvegarde de la configuration.
    hControl    = findobj(hdlFig,'Tag','tagEditFileConfig');
    % Détermination du fichier
    if isempty(get(hControl, 'String'))
        [flag, nomFic] = my_uigetfile({'*.xml'}, 'Config XML to restore', oldDir);
        if ~flag
            return
        end
        h = findobj(hdlFig, 'Tag', 'tagEditFileConfig');
        set(h, 'String', nomFic);
        oldDir = fileparts(nomFic);
    else
        nomFic = get(hControl, 'String');
    end
    
    % Appel de la fonction de restauration des données de configurations.
    flag = BOB_fcnRestoreConfig(hdlFig, nomFic); %#ok<NASGU>



% -- cbBtnRestore

% = Callback ===============
function cbBtnSetFileConfig(hObject, ~, tag)

    hdlFig = ancestor(hObject, 'figure', 'toplevel');

    hControl    = findobj(hdlFig,'Tag',tag);    
    [flag, nomFic] = my_uigetfile({'*.xml'}, 'MultiSelect', 'off');
    if ~flag
        return
    end
    set(hControl, 'String', nomFic);
% BOB_cbClose
