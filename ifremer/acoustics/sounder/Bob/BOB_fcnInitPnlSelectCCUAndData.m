% Cr�ation des boutons de choix d'un fichier CCU et du r�pertoire de
% donn�es RAW.
function flag = BOB_fcnInitPnlSelectCCUAndData(hdlPnl, nomFic, nomDir, flagEnableProcess)


hLabelFileCCU = uicontrol('Style', 'text', ...
                    'Parent',   hdlPnl,...
                    'String',   'Input CCU Log File', ...
                    'Units',    'normalized',...
                    'HorizontalAlignment','left',...
                    'Position', [0.1 0.88 0.7 0.12],...
                    'Tag',      'tagLabelFileCCU'); %#ok<NASGU>
                
hEditFileCCU = uicontrol('Style', 'edit', ...
                    'Parent',   hdlPnl,...
                    'String',   nomFic, ...
                    'Enable',   flagEnableProcess, ...
                    'Units',    'normalized',...
                    'HorizontalAlignment','center',...
                    'Position', [0.1 0.55 0.7 0.3],...
                    'Tag',      'tagEditFileCCU', ...
                    'BackgroundColor', 'w'); %#ok<NASGU>

hBtnInputFileCCU  =     uicontrol(...
                    'Parent',   hdlPnl,...
                    'Units',    'normalized',...
                    'Position', [0.8 0.55 0.1 0.3],...
                    'Tag',      'tagBtnInputFileCCU',...
                    'String',   '...', ...
                    'Enable',   flagEnableProcess, ...
                    'Tooltip',  Lang('Entrer le fichier CCU ','Input CCU File'), ...
                    'CallBack', {@cbBtnSetFileCCU, 'tagEditFileCCU'}); %#ok<NASGU>
                
hLabelRawDir = uicontrol('Style', 'text', ...
                    'Parent',   hdlPnl,...
                    'String',   'Input RawData Directory', ...
                    'Units',    'normalized',...
                    'HorizontalAlignment','left',...
                    'Position', [0.1 0.38 0.7 0.12],...
                    'Tag',      'tagLabelRawDir'); %#ok<NASGU>

hEditRawDir = uicontrol('Style', 'edit', ...
                    'Parent',   hdlPnl,...
                    'String',   nomDir, ...
                    'Enable',   flagEnableProcess, ...
                    'Units',    'normalized',...
                    'HorizontalAlignment','center',...
                    'Position', [0.1 0.05 0.7 0.3],...
                    'Tag',      'tagEditRawDir', ...
                    'BackgroundColor', 'w'); %#ok<NASGU>

hBtnInputRawDir  =     uicontrol(...
                    'Parent',   hdlPnl,...
                    'Units',    'normalized',...
                    'Position', [0.8 0.05 0.1 0.3],...
                    'Tag',      'tagBtnInputRawDir',...
                    'String',   '...', ...
                    'Enable',   flagEnableProcess, ...
                    'Tooltip',  Lang('Entrer le r�pertoire de donn�es RAW','Input Raw file Directory'), ...
                    'CallBack', {@cbBtnSetRawDir, 'tagEditRawDir'}); %#ok<NASGU>


flag = 1;

end %BOB_fcnInitPnlSelectCCUAndData

function cbBtnSetFileCCU(hObject, ~, tag)

    hdlFig          = ancestor(hObject, 'figure', 'toplevel');
    
    hControl        = findobj(hdlFig,'Tag',tag);        
    [flag, nomFic] = my_uigetfile({'*.log'}, 'MultiSelect', 'off');
    if ~flag
        return
    end
    set(hControl, 'String', nomFic);
end % cbBtnSetFileCCU

function cbBtnSetRawDir(hObject, ~, tag)

    hdlFig      = ancestor(hObject, 'figure', 'toplevel');
    
    hControl    = findobj(hdlFig,'Tag',tag);
    str         = get(hControl, 'String');
    if isempty(str)
        str = my_tempdir;
    end
    pathName    = uigetdir(str, Lang(   'SVP, d�signer le chemin de localisation des RAW data', ...
                                        'Please, input RAW Data directory'));
    if ~pathName
        return
    end
    set(hControl, 'String', pathName);
end % cbBtnSetRawDir
