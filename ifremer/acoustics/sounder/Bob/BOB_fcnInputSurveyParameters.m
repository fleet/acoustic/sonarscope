% IHM de saisie des paramètres de la mission.
%
%
% Syntax
%   [flag, S] = BOB_fcnInputSurveyParameters(...)
%
% Input Arguments
%   /
%
% Name-Value Pair Arguments
%  	InputPosition       : flag (0/1) de saisie nécessaires des positions.
%
% Output Arguments
%   flag        : flag résultant de l'action OK/Cancel
%   S           : structure comportant les différents paramètres saisis
%
% Examples
%     [flag, structConfigSurvey] = BOB_fcnInputSurveyParameters('InputPosition', 1);
%
%     [flag, structConfigSurvey] = BOB_fcnInputSurveyParameters('InputPosition', 0);
%
%     [flag, sCCUandCompass, subCCUData, subPAROSData] = BOB_fcnInputSurveyParameters('nomFicCCULog', ...
%                  'K:\Data\IFREMER\Data_SSC\SonarScopeData\BOB\ESSBOB\20130922_ccu.log', ...
%                  'Resizable', 'on');
%
% See also BOB_uiSelectConfig Authors
% Authors : GLU, A. OGOR

function [flag, structConfigSurvey] = BOB_fcnInputSurveyParameters(varargin)

global IfrTbxResources %#ok<GVMIS>

[varargin, flagInputPosition] = getPropertyValue(varargin, 'InputPosition', 1); %#ok<ASGLU> 

flag                = 0;
structConfigSurvey  = [];

%% Saisie des paramètres de la campagne
Labels      =  {    Lang('Nom de la campagne :', 'SurveyName :'), ...
    Lang('Nom du sondeur :', 'Sounder name :'), ...
    Lang('Nom du répertoire contenant la configuration de référence MOVIES :', 'Reference MOVIES configuration directory :'),...
    Lang('1er niveau de seuil (dB) :', 'First threshold value (dB) :'),...
    Lang('Pas du seuillage (dB) :', 'Step threshold value (dB) :'),...
    Lang('Dernier niveau de seuil (dB) :', 'Last threshold value (dB) :'), ...
    Lang('Largeur du layer (m) :','Layer width (m) :'), ...
    Lang('Offset du 1er layer (m) :','First layer offset (m) :'), ...
    Lang('Nombre de couches :','Layers number :')};

sz      = [1 50; 1 50; 1 50; 1 30; 1 30; 1 30; 1 30; 1 30; 1 30];
Defauts = {'ESSBOB2', 'ES120-7D', fullfile(IfrTbxResources,'XML_BOBConfigM3D'), '-100', '5', '-60', '2', '0', '55'};

if flagInputPosition
    Labels{end+1} =  Lang('Position en latitude (x.y deg) :','Latitude position (x.y deg):');
    Labels{end+1} =  Lang('Position en longitude (x.y deg) :','Longitude position (x.y deg):');
    Labels{end+1} =  Lang('Position en profondeur (m) :','Depth position (m) :');
    
    sz(end+1,:) = [1 40];
    sz(end+1,:) = [1 40];
    sz(end+1,:) = [1 40];
    
    Defauts{end+1} = 'NaN';
    Defauts{end+1} = 'NaN';
    Defauts{end+1} = 'NaN';
end

Titre       = Lang('Saisissez les paramètres de la mission', 'Choose the survey parameters');
% NbLignes    = 1;
Resultats   = inputdlg(Labels, Titre, sz, Defauts);
if isempty(Resultats)
    return
end
k = 1;
structConfigSurvey.name              = Resultats{k}; k = k+1;
structConfigSurvey.sounderName       = Resultats{k}; k = k+1;
structConfigSurvey.configXMLM3D      = Resultats{k}; k = k+1;
structConfigSurvey.firstThreshold    = str2double(Resultats{k}); k = k+1;
structConfigSurvey.stepThreshold     = str2double(Resultats{k}); k = k+1;
structConfigSurvey.lastThreshold     = str2double(Resultats{k}); k = k+1;
structConfigSurvey.layerWidth        = str2double(Resultats{k}); k = k+1;
structConfigSurvey.firstLayerOffset  = str2double(Resultats{k}); k = k+1;
structConfigSurvey.layerNumber       = str2double(Resultats{k}); k = k+1;

if flagInputPosition
    structConfigSurvey.latPosition       = str2double(Resultats{k}); k = k+1;
    structConfigSurvey.lonPosition       = str2double(Resultats{k}); k = k+1;
    structConfigSurvey.depthPosition     = str2double(Resultats{k});
end

flag = 1;
