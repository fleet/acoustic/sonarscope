% Fonction de parcours de l'arborescence RAW et conversion des fichiers RAW
% en fichier HAC.
%
% Syntax
%   flag = BOB_fcnMultiConvertRaw2Hac(...)
%
% Input Arguments
%   TODO
%
% Name-Value Pair Arguments
%   TODO
%
% Output Arguments
%   % Cf Test.m in BOB Processing
%   [flag, dirOut] = BOB_fcnMultiConvertRaw2Hac;
%
% Examples
%   flag = BOB_fcnMultiConvertRaw2Hac(tabData, TODO)
%
% See also BOB_uiSelectConfig Authors
% Authors : GLU, A. OGOR
% ----------------------------------------------------------------------------

function flag = BOB_fcnMultiConvertRaw2Hac(varargin)

[varargin, nbSectors]        = getPropertyValue(varargin, 'NbSectors',     []);
[varargin, pathRUNToProcess] = getPropertyValue(varargin, 'NomDirRawData', []); %#ok<ASGLU>

if isempty(pathRUNToProcess)
    [flag, pathRUNToProcess] = my_uigetdir(my_tempdir, Lang('Saisissez le r�pertoire des donn�es RAW','Please, input RAW Directory'));
    if ~flag
        return
    end
end

if isempty(nbSectors)
    listRunDir  = dir(fullfile(pathRUNToProcess, 'RUN*'));
    nbSectors   = numel(listRunDir);
end

% Parcours de l'arborescence. Identification des fichiers RAW � convertir
% en fichiers HAC
hw = create_waitbar('Processing RUN* Directories', 'N', nbSectors);
for k=1:nbSectors
    runDir{k}   = fullfile(pathRUNToProcess, sprintf('RUN%03d', k)); %#ok<AGROW>
    rawFiles  	= dir([runDir{k} '\*.raw']);
    flag        = BOB_fcnMultiConvertRaw2Hac_unitaire(runDir{k}, rawFiles); %#ok<NASGU>
    
    % Indication de la progression.
    my_waitbar(k, nbSectors, hw);
    
end
my_close(hw, 'MsgEnd');

% Traitement des fichiers de calibration
calibDir    = fullfile(pathRUNToProcess, 'Calibration');
rawFiles  	= dir([calibDir '\*.raw']);
flag        = BOB_fcnMultiConvertRaw2Hac_unitaire(calibDir, rawFiles);
if ~flag
    return
end

flag = 1;


%% Fonction unitaire de conversion des fichiers RAW d'un r�pertoire
function flag = BOB_fcnMultiConvertRaw2Hac_unitaire(dirFiles, listFilesToConvert)

flag = 0;  %#ok<NASGU>

for f=1:numel(listFilesToConvert)
    nomFic  = fullfile(dirFiles, listFilesToConvert(f).name);
    % Appel de la conversion unitaire de la fonction RAW2HAC
    a       = cl_ExRaw('NomFic', nomFic); %#ok<NASGU>
    [pathName, rootName, ext] = fileparts(nomFic); %#ok<ASGLU>
    targetFile = fullfile(dirFiles, [rootName '_raw2hac' '.hac']);
    
    % On v�rifie si le fichier n'aurai pas d�j� cr�� dans une
    % pr�c�dente ex�cution qui aurait �t� stopp�e.
    if ~exist(targetFile, 'file')
        flag    = ExRaw_ssc2hac('NomFicRaw', nomFic, 'RepDefaut', dirFiles);
        if ~flag
            str1 = sprintf('Probl�me dans la conversion en HAC du fichier : %s', nomFic);
            str2 = sprintf('Problem in conversion in HAC format : %s', nomFic);
            my_warndlg(Lang(str1,str2), 1);
            my_close(hw);
            % On poursuit la conversion m�me si un des fichiers pose probl�me.
        end
    end
    
end

flag = 1;
