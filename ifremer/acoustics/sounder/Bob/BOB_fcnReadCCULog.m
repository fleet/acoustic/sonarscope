% Lecture du fichier CCU pour sortie les donn�es sous forme de data / commande.
%
% Syntax
%   [flag, data] = BOB_fcnReadCCULog(nomFicCCULog);
%
% Input Arguments
%   nomFicCCULog        : nom du fichier CCU
%
% Name-Value Pair Arguments
%
% Output Arguments
%   flag        : flag de comportement de la fonction
%   data        : matrice de donn�es (2X N sous forme date/texte).
%
% Examples
%   nomFicCCULog = 'F:\SonarScopeData\BOB\Data\ESSBOB\20130922_ccu.log'
%   [flag, data] = BOB_fcnReadCCULog(nomFicCCULog)
%
% See also BOB_uiSelectConfig Authors
% Authors : A. OGOR, GLU
% ----------------------------------------------------------------------------
function [flag, data] = BOB_fcnReadCCULog(nomFicCCULog)

flag = 0;
data = {};
%% Open the text file.
fileID = fopen(nomFicCCULog,'r');
if fileID == -1
    my_warndlg(Lang('Le fichier de CCU n''est pas trouv�','CCU file is not found.'), 1);
    return
end

% Les lignes ne sont pas prises en compte, il faut n�anmoins filter les
% '****'.
dataArray   = textscan(fileID, '%s', 'delimiter', '\n', 'ReturnOnError', false);

% Close the text file.
fclose(fileID);

try
    
    dataArray = dataArray{1};
    pppp    = regexp(dataArray,'*******', 'match');
    sub     = cellfun(@isempty, pppp);
    pppp    = dataArray(sub);
    sub     = cellfun(@isempty, pppp);
    pppp    = pppp(~sub);
    
    % Eclatement de chaque ligne en deux tron�ons : date] et reste de la ligne.
    splitStr    = regexp(pppp,']','split');
    dummy       = [splitStr{:,1}];
    dummy       = reshape(dummy, 2, size(splitStr,1))';
    % Suppression du "[" de d�but.
    dummy       = dummy(:,1);
    dummy       = regexprep(dummy,'[', '');
    
    % Mise en forme de la donn�e de sortie,par exemple:
    % "12:55:04" / "CCU             -> INFO    : Starting the CCU on Sep 17
    % 2013 12:55:04."
    data(:,1)   = dummy;
    % Boucle suivante � refaire (traitement sur les cellules).
    for k=1:size(splitStr,1)
        dummy       = splitStr{k,1};
        data(k,2)   = dummy(1,2);
    end
catch
    my_warndlg(Lang('Le fichier de CCU n''est pas d�cod�','CCU file is not interpreted.'), 1);
    return
    
end

flag = 1;

%% Ancien d�codage du fichier CCU.
% L'appel � textscan est optimis� � partir de la version R2013a par
% l'entr�e de plusieurs d�limiters sous forme de caract�res. Cela n'est pas
% possible dans les versions pr�c�dentes de MatLab.

% %delimiter = {'[',']',',','','"',};
% %delimiter = {'['};
% delimiter = ']'; % {'\t','[',']'}; % '\t'; % 
% 
% %% Format string for each line of text:
% %   column1: text (%s)
% % For more information, see the TEXTSCAN documentation.
% %formatSpec = '%s%[^\n\r]';
% %formatSpec = '%s%[^*\n\r]';
% formatSpec = '%*s%s%s%[^\n\r]';
% %% Open the text file.
% fileID = fopen(nomFicCCULog,'r');
% if fileID == -1
%     my_warndlg(Lang('Le fichier de CCU n''est pas trouv�','CCU file is not found.'), 1);
%     return
% end
% %% Read columns of data according to format string.
% % This call is based on the structure of the file used to generate this
% % code. If an error occurs for a different file, try regenerating the code
% % from the Import Tool.
% %dataArray = textscan(fileID, formatSpec, 'Delimiter', delimiter,'TreatAsEmpty',{'*********************************************************************************'}, 'ReturnOnError', false);
% %dataArray = textscan(fileID, formatSpec, 'Delimiter', delimiter,'CommentStyle',{'*'},'ReturnOnError', true);
% data = textscan(fileID, formatSpec, 'Delimiter', delimiter,  'ReturnOnError', false);
% 
% %% Close the text file.
% fclose(fileID);

