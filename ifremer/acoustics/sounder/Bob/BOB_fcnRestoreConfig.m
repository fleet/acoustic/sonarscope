% Restauration de la configuration BOB saisie dans l'IHM BOB_uiSelectConfig
%
% Syntax
%   ConfigBOB = BOB_fcnRestoreConfigBOB(hFig)
%
% Input Arguments
%   h : handle de figure
%
% Name-Value Pair Arguments
%
% Output Arguments
%   ConfigBOB : Structure de la config nominale BOB
%
% Examples
%
% See also BOB_uiSelectConfig Authors
% Authors : GLU
% ----------------------------------------------------------------------------
function flag = BOB_fcnRestoreConfig(hFig, nomFic)

flag = 0; %#ok<NASGU>
S = xml_read(nomFic);

f = fieldnames(S.Parameters);
for k=1:numel(f)
    tagControl  = S.Parameters.(f{k}).Tag;
    hControl    = findobj(hFig, 'Tag', tagControl);
    % Pour le cas o� le champ n'est pas un controle (NbSectors)
    if ~isempty(hControl)
        if strcmp(tagControl, 'tagEditNordMagneticCard')
            valControl  = S.Parameters.(f{k}).Value;
            set(hControl, 'Value', valControl);
        else
            valControl  = num2str(S.Parameters.(f{k}).Value);
            set(hControl, 'String', valControl);            
        end
    end
end
   
flag = 1;