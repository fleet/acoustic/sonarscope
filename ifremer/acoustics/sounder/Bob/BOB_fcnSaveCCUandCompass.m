% Sauvegarde de la configuration de CCU and Compass saisie dans l'IHM
% uiSelectCCUAndCompass
%
% Syntax
%   [flag, CCUandCompass = BOB_fcnSaveCCUandCompass(hFig)
%
% Input Arguments
%   h : handle de figure
%
% Name-Value Pair Arguments
%
% Output Arguments
%   flag            : flag de comportement de la fonction
%   CCUandCompass   : Structure de sauvegarde des paramètres CCU et
%   Compass.
%
% Examples
%
% See also uiSelectCCUAndCompass Authors
% Authors : GLU
% ----------------------------------------------------------------------------
function [flag, CCUandCompass] = BOB_fcnSaveCCUandCompass(hdlFig)

flag = 0; %#ok<NASGU>

% Sauvegarde de la structure
CCUandCompass.Comments                  = Lang('Fichier de paramétrage du CCU Log et du Compass', 'TODO');
CCUandCompass.Version                   = '20131219';

CCUandCompass.Parameters.LogFile.Value       = get(findobj(hdlFig, 'Tag', 'tagEditFileCCU'), 'String');
CCUandCompass.Parameters.LogFile.Tag         = 'tagEditFileCCU';
CCUandCompass.Parameters.LogFile.Unit        = '';

CCUandCompass.Parameters.Compass.Value       = get(findobj(hdlFig, 'Tag', 'tagEditCCUCompass'), 'String');
CCUandCompass.Parameters.Compass.Tag         = 'tagEditCCUCompass';
CCUandCompass.Parameters.Compass.Unit        = 'deg';

CCUandCompass.Parameters.RawDir.Value        = get(findobj(hdlFig, 'Tag', 'tagEditRawDir'), 'String');
CCUandCompass.Parameters.RawDir.Tag          = 'tagEditRawDir';
CCUandCompass.Parameters.RawDir.Unit         = '';

CCUandCompass.Parameters.DateBegin.Value     = get(findobj(hdlFig, 'Tag', 'tagEditDateBegin'), 'String');
CCUandCompass.Parameters.DateBegin.Tag       = 'tagEditDateBegin';
CCUandCompass.Parameters.DateBegin.Unit      = '';

CCUandCompass.Parameters.DateEnd.Value       = get(findobj(hdlFig, 'Tag', 'tagEditDateEnd'), 'String');
CCUandCompass.Parameters.DateEnd.Tag         = 'tagEditDateEnd';
CCUandCompass.Parameters.DateEnd.Unit        = '';

flag = 1;