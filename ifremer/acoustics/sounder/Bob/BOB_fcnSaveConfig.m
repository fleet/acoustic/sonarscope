% Sauvegarde de la configuration BOB saisie dans l'IHM BOB_uiSelectConfig
%
% Syntax
%   ConfigBOB = BOB_fcnSaveConfig(hFig)
%
% Input Arguments
%   h : handle de figure
%
% Name-Value Pair Arguments
%
% Output Arguments
%   flag        : flag de comportement de la fonction
%   ConfigBOB   : Structure de la config nominale BOB
%
% Examples
%
% See also BOB_uiSelectConfig Authors
% Authors : GLU
% ----------------------------------------------------------------------------
function [flag, sConfigBOB] = BOB_fcnSaveConfig(hdlFig)

flag = 0; %#ok<NASGU>

% Sauvegarde de la structure
sConfigBOB.Comments              = 'Fichier de configuration des donn�es de mesure de BOB';
sConfigBOB.Version               = '20131219';

sConfigBOB.Parameters.Cap.Value  = get(findobj(hdlFig, 'Tag', 'tagEditCap'), 'String');
sConfigBOB.Parameters.Cap.Tag    = 'tagEditCap';
sConfigBOB.Parameters.Cap.Unit   = 'deg';
sConfigBOB.Parameters.Cap.Label   = 'Cap';

sConfigBOB.Parameters.Tilt.Value  = get(findobj(hdlFig, 'Tag', 'tagEditTilt'), 'String');
sConfigBOB.Parameters.Tilt.Tag    = 'tagEditTilt';
sConfigBOB.Parameters.Tilt.Unit   = 'deg';
sConfigBOB.Parameters.Tilt.Label  = Lang('Tilt du transducteur', 'Tilt Transducer');

sConfigBOB.Parameters.MNBias.Value   = get(findobj(hdlFig, 'Tag', 'tagEditNordMagnetic'), 'String');
sConfigBOB.Parameters.MNBias.Tag     = 'tagEditNordMagnetic';
sConfigBOB.Parameters.MNBias.Unit    = 'deg';
sConfigBOB.Parameters.MNBias.Label   = 'Magnetic North Bias';

h       = findobj(hdlFig, 'Tag', 'tagEditNordMagneticCard');
pppp    = get(h, 'String');
sConfigBOB.Parameters.MNBiasCard.String      = pppp{get(h, 'Value')};
sConfigBOB.Parameters.MNBiasCard.Tag         = 'tagEditNordMagneticCard';
sConfigBOB.Parameters.MNBiasCard.Value       = get(h, 'Value');
sConfigBOB.Parameters.MNBiasCard.Unit        = '';
sConfigBOB.Parameters.MNBiasCard.Label       = 'Magnetic North Bias Cardinal';

apertureValue = get(findobj(hdlFig, 'Tag', 'tagEditAperture'), 'String');
sConfigBOB.Parameters.Aperture.Value         = apertureValue;
sConfigBOB.Parameters.Aperture.Tag           = 'tagEditAperture';
sConfigBOB.Parameters.Aperture.Unit          = 'deg';
sConfigBOB.Parameters.Aperture.Label         = Lang('Ouverture globale','Aperture');

sConfigBOB.Parameters.ObservationTime.Value  = get(findobj(hdlFig, 'Tag', 'tagEditDuree'), 'String');
sConfigBOB.Parameters.ObservationTime.Tag    = 'tagEditDuree';
sConfigBOB.Parameters.ObservationTime.Unit   = 'min';
sConfigBOB.Parameters.ObservationTime.Label  = Lang('Dur�e d''observation', 'Observation Time');

sConfigBOB.Parameters.Offset.Value           = get(findobj(hdlFig, 'Tag', 'tagEditOffset'), 'String');
sConfigBOB.Parameters.Offset.Tag             = 'tagEditOffset';
sConfigBOB.Parameters.Offset.Unit            = 'deg';
sConfigBOB.Parameters.Offset.Label           = 'TODO : Offset ...';

stepValue = get(findobj(hdlFig, 'Tag', 'tagEditStep'), 'String');
sConfigBOB.Parameters.Step.Value             = stepValue;
sConfigBOB.Parameters.Step.Tag               = 'tagEditStep';
sConfigBOB.Parameters.Step.Unit              = 'deg';
sConfigBOB.Parameters.Step.Label             = 'TODO : Step ...';

if mod(str2double(apertureValue),str2double(stepValue)) ~= 0
    nbSectors = floor(str2double(apertureValue)/str2double(stepValue)) + 1;
else
    nbSectors= floor(str2double(apertureValue)/str2double(stepValue));
end
sConfigBOB.Parameters.NbSectors.Value        = num2str(nbSectors);
sConfigBOB.Parameters.NbSectors.Tag          = 'tagNbSectors';
sConfigBOB.Parameters.NbSectors.Unit         = '';
sConfigBOB.Parameters.NbSectors.Label        = 'NbSectors';

sConfigBOB.Parameters.CalibInterval.Value    = get(findobj(hdlFig, 'Tag', 'tagEditPeriode'), 'String');
sConfigBOB.Parameters.CalibInterval.Tag      = 'tagEditPeriode';
sConfigBOB.Parameters.CalibInterval.Unit     = 'Nb Cycle';
sConfigBOB.Parameters.CalibInterval.Label    = 'Calibration Interval';

sConfigBOB.Parameters.CalibTime.Value        = get(findobj(hdlFig, 'Tag', 'tagEditDureeCalib'), 'String');
sConfigBOB.Parameters.CalibTime.Tag          = 'tagEditDureeCalib';
sConfigBOB.Parameters.CalibTime.Unit         = 'min';
sConfigBOB.Parameters.CalibTime.Label        = 'TODO : Calibration Duration';

sConfigBOB.Parameters.NbCycleON.Value        = get(findobj(hdlFig, 'Tag', 'tagEditNbCycleOn'), 'String');
sConfigBOB.Parameters.NbCycleON.Tag          = 'tagEditNbCycleOn';
sConfigBOB.Parameters.NbCycleON.Unit         = '';
sConfigBOB.Parameters.NbCycleON.Label        = 'TODO : Nb Cycle ON';

sConfigBOB.Parameters.TimeOFF.Value          = get(findobj(hdlFig, 'Tag', 'tagEditDureeOff'), 'String');
sConfigBOB.Parameters.TimeOFF.Tag            = 'tagEditDureeOff';
sConfigBOB.Parameters.TimeOFF.Unit           = '';
sConfigBOB.Parameters.TimeOFF.Label          = 'TODO : Tps Duree Off';

flag = 1;

