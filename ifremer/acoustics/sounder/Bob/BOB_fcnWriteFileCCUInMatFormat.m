% Fonction d'�criture des donn�es CCU n�cessaires au formattage 3DV depuis
% SonarScope.
%
%
% Syntax
%   [flagOKCancel, sConfigBOB] = BOB_fcnWriteFileCCUInMatFormat(...)
%
% Input Arguments
%   surveyName          : nom de la mission
%   
%   sCCUandCompass      : structure comportant tous les param�tres CCU et
%                         de Compass
%   subCCUData          : extraction des donn�es CCU dans l'intervalle de
%                         temps.
%   subPAROSData        : extraction des donn�es PAROS dans l'intervalle de
%                         temps.
%
% Name-Value Pair Arguments
%
% Output Arguments
%   flag        : flag de bon fonctionnement
%   nomFicCCU   : nom du fichier CCU au format MAT.
%
% Examples
%   [flag, nomFicCCUMat]  = BOB_fcnWriteFileCCUInMatFormat(...
%                                             'ESSBOB', ...
%                                             sCCUandCompassExtract, ...
%                                             subCCUData, ...
%                                             subPAROSData);
%
% See also BOB_fcnWriteFileCCUInMatFormat Authors
% Authors : GLU
% ----------------------------------------------------------------------------
function [flag, nomFicMatCCU] = BOB_fcnWriteFileCCUInMatFormat(surveyName, sCCUandCompassExtract, subCCUData, subPAROSData)

flag = 0; %#ok<NASGU>

if ~isempty(subPAROSData)
    MAT_CCU_Capteurs.Pressure_psi   = subPAROSData.Paros.pression;         % Pression en Pound per Inch
    MAT_CCU_Capteurs.Pressure_bar   = subPAROSData.Paros.pression*14.51;   % Pression en bar
    MAT_CCU_Capteurs.Temperature    = subPAROSData.Paros.temperature;
    MAT_CCU_Capteurs.Pan            = subCCUData.Transducer.pan;
    MAT_CCU_Capteurs.Tilt           = subCCUData.Transducer.tilt;
    MAT_CCU_Capteurs.Heading        = subCCUData.Compass.cap;
    MAT_CCU_Capteurs.Roll           = subCCUData.Compass.roll;
    MAT_CCU_Capteurs.Pitch          = subCCUData.Compass.pitch;
    MAT_CCU_Capteurs.SDate          = subCCUData.PanTilt.date_heure;
end

% Remplissage des dates.
MAT_CCU_Capteurs.StartHeading   = NaN;
MAT_CCU_Capteurs.StartPitch     = NaN;
MAT_CCU_Capteurs.StartRoll      = NaN;
MAT_CCU_Capteurs.StartTilt      = NaN;
MAT_CCU_Capteurs.StartPan       = NaN;

% Enregistrement des donn�es CCU.
nomFicMat = fullfile(sCCUandCompassExtract.Parameters.RawDir.Value, 'Result', [surveyName{:} '_CCU_Sensors.mat']);
save(nomFicMat, 'MAT_CCU_Capteurs');

% Pour sauvegarde dans le fichier de pointage des resultats d'EI
nomFicMatCCU = [surveyName{:} '_CCU_Sensors.mat'];

flag = 1;
