% Fonction de r�alisation du fichier BOB servant � pointer les r�sultats EI
% Mat issus du calcul d'EI.
%
%
% Syntax
%   [flagOKCancel, sConfigBOB] = BOB_fcnWriteFileConfigBOBInSSCFormat(...)
%
% Input Arguments
%   N               : nb de cycles.
%   sEstimateSector : structure comportant les valeurs des angles secteur par secteur.
%   sConfigBOB      : structure comportant tous les param�tres de
%                     configuration.
%
% Name-Value Pair Arguments
%
% Output Arguments
%   flag        : flag de bon fonctionnement
%   nomFic      : structure comportant tous les param�tres de
%                 configuration.
%
% Examples
%   [flag, nomFicBob]  = BOB_fcnWriteFileConfigBOBInSSCFormat(...
%                                             5, ...
%                                             sConfigBOB, ...
%                                             sCCUandCompassExtract, ...
%                                             sEstimateSectors, ...
%                                             structConfigSurvey);
%
% See also BOB_fcnWriteFileConfigBOBInSSCFormat Authors
% Authors : GLU, A. OGOR
% ----------------------------------------------------------------------------

function [flag, nomFic] = BOB_fcnWriteFileConfigBOBInSSCFormat(nbCycles, S1, S2, S3, S4)

flag = 0; 

%% Saisie de certains param�tres de mission :
Labels =  { Lang('Equipement :', 'Equipment :'), ...
            Lang('V�hicule:', 'Vessel:'), ...
            Lang('Sondeur :', 'Sounder :'), ...
            Lang('Chief Scientifique :', 'Chef Scientist :')};

Titre       = Lang('Saisissez les param�tres de la mission', 'Choose the survey parameters');
NbLignes    = 1;
Defauts     = {'BOB', 'EUROPE','ER60','Carla SCALABRIN'};
Resultats   = inputdlg(Labels, Titre, NbLignes, Defauts);
if isempty(Resultats)
    return
end
Equipment       = Resultats{1};
Vessel          = Resultats{2};
Sounder         = Resultats{3};
ChiefScientist  = Resultats{4};

%% Pr�paration des MetaDonn�es de la structure.
EI_Mat_Data.Signature1      = 'MOVIES';
EI_Mat_Data.Signature2      = 'BOB';
EI_Mat_Data.Software        = 'MOVIESBySSC';
EI_Mat_Data.DataTag         = 'PolarHorizontalesImages';
EI_Mat_Data.Name            = S4.name;
EI_Mat_Data.ExtractedFrom   = S2.Parameters.RawDir.Value;
EI_Mat_Data.NomDirRun       = ['EI_Lay_' S4.name];
EI_Mat_Data.NomFicSensor    = [];
EI_Mat_Data.FormatVersion   = '20121025';
EI_Mat_Data.Survey          = S4.name;
EI_Mat_Data.Equipment       = Equipment;
EI_Mat_Data.Vessel          = Vessel;
EI_Mat_Data.Sounder         = Sounder;
EI_Mat_Data.ChiefScientist  = ChiefScientist;

th = S4.firstThreshold:S4.stepThreshold:S4.lastThreshold;
EI_Mat_Data.Dimensions.nbCycles     = nbCycles;
EI_Mat_Data.Dimensions.nbSectors    = str2double(S1.Parameters.NbSectors.Value);
EI_Mat_Data.Dimensions.nbCells      = S4.layerNumber;
EI_Mat_Data.Dimensions.nbThresholds = numel(th);

tabVariables(1).Name            = 'StepThreshold';
tabVariables(end).Dimensions    = '1, 1';
tabVariables(end).Storage       = 'single';
tabVariables(end).Unit          = 'dB';
tabVariables(end).Constant      = S4.stepThreshold;
tabVariables(end).Tag           = tabVariables(end).Name;

tabVariables(end+1).Name        = 'MinThreshold';
tabVariables(end).Dimensions    = '1, 1';
tabVariables(end).Storage       = 'single';
tabVariables(end).Unit          = 'dB';
tabVariables(end).Constant      = S4.firstThreshold;
tabVariables(end).Tag           = tabVariables(end).Name;

tabVariables(end+1).Name        = 'MaxThreshold';
tabVariables(end).Dimensions    = '1, 1';
tabVariables(end).Storage       = 'single';
tabVariables(end).Unit          = 'dB';
tabVariables(end).Constant      = S4.lastThreshold;
tabVariables(end).Tag           = tabVariables(end).Name;

tabVariables(end+1).Name        = 'OffsetBeam1';
tabVariables(end).Dimensions    = '1, 1';
tabVariables(end).Storage       = 'single';
tabVariables(end).Unit          = 'deg';
tabVariables(end).Constant      = S3.Transducer180_180Angle(1);
tabVariables(end).Tag           = tabVariables(end).Name;

tabVariables(end+1).Name        = 'SizeCellEI';
tabVariables(end).Dimensions    = '1, 1';
tabVariables(end).Storage       = 'single';
tabVariables(end).Unit          = 'm';
tabVariables(end).Constant      = S4.layerWidth;
tabVariables(end).Tag           = tabVariables(end).Name;

tabVariables(end+1).Name        = 'Heading';
tabVariables(end).Dimensions    = '1, 1';
tabVariables(end).Storage       = 'double';
tabVariables(end).Unit          = 'deg';
tabVariables(end).Constant      = S3.Compass;
tabVariables(end).Tag           = tabVariables(end).Name;

tabVariables(end+1).Name        = 'BeamWidth';
tabVariables(end).Dimensions    = '1, 1';
tabVariables(end).Storage       = 'single';
tabVariables(end).Unit          = 'dB';
tabVariables(end).Constant      = str2double(S1.Parameters.Step.Value);
tabVariables(end).Tag           = tabVariables(end).Name;

tabVariables(end+1).Name        = 'SectorObservationTime';
tabVariables(end).Dimensions    = '1, 1';
tabVariables(end).Storage       = 'single';
tabVariables(end).Unit          = 'min';
tabVariables(end).Constant      = str2double(S1.Parameters.ObservationTime.Value);
tabVariables(end).Tag           = tabVariables(end).Name;

for k=1:numel(tabVariables)
    EI_Mat_Data.Variables(k) = tabVariables(k);
end

%% Ecriture dans le fichier BOB (XML) de pointage des fichiers mat d'EI.
nomFic = S4.name;
nomFic = fullfile(S2.Parameters.RawDir.Value, 'Result', [nomFic '.bob']);

% Ecriture du fichier BOB pour pointage en entr�e de la proc�dure de
% 3DViewer.
xml_write(nomFic, EI_Mat_Data);

flag    = 1;

