% IHM d'analyse des donn�es Mesur�s et Simul�s.
%
% Syntax
%   flag = BOB_uiDataAnalyser(...)
%
% Input Arguments
%   tabData         : tableau r�sum� des �carts observ�s.
%
% Name-Value Pair Arguments
%   Entete          : libell� de l'entete de la fen�tre.
%   Resizable       : option de redimensionnement de la fen�tre.
%   Editable        : flag 0/1 pour savoir si certaines cellules sont
%                     �ditables (Cycle, Secteur, Commentaire).
%
% Output Arguments
%   flag            :   flag de r�ponse OK ou Annuler.
%   newTabData      :   les donn�es peuvent avoir �t� corrig�es par
%                       l'op�rateur.
%
% Examples
%   flagOKCancel = BOB_uiDataAnalyser([], 'Editable', 1)
%
% See also BOB_uiSelectConfig Authors
% Authors : GLU, A. OGOR
% ----------------------------------------------------------------------------

function [flagOKCancel, newTabData] = BOB_uiDataAnalyser(tabData, varargin)


[varargin, Entete]       = getPropertyValue(varargin, 'Entete', 'IFREMER-SonarScope');
[varargin, Resizable]    = getPropertyValue(varargin, 'Resizable', 'off');
[varargin, flagEditable] = getPropertyValue(varargin, 'Editable', 0); %#ok<ASGLU>

newTabData          = [];
flagOKCancel        = 0;


%% Affichage de la table de synth�se
% R�sum� des angles sous forme de tableau.
% | NumCycle | Num Secteur | Compas | Pan   | Flip  |  Pan Geo | Heure Deb | Heure Fic | Volume Fic | Nom Fic | Statut | Operation
% |          |             |        | M/S   | M/S   |    M/S   |
colNames  = {   sprintf(...
    'Cycle | number'), ...
    'Sector | Number', ...
    'Compas |(deg)', ...
    'Measured | Pan (deg)',...
    'Estimated | Pan (deg)',...
    'Measured | Flip',...
    'Estimated | Flip',...
    'Measured | Geo Pan (deg)',...
    'Estimated | Geo Pan (deg)',...
    'Measured | Begin Date', ...
    'Estimated | Begin Date', ...
    'File | Date', ...
    'File | size (kO)', ...
    'Filename', ...
    'Message', ...
    'Process | Or Not', ...
    'User Comment' ...
    'Error Code'};


% Identification des colonnes dans le tableau tabData.
nbCol               = size(tabData,2);
idColProcessing     = 16;
idColComment        = 17;
idColError          = 18;
idCol.Comment       = idColComment;
idCol.Processing    = idColProcessing;
idCol.Error         = idColError;

% En cours de debug.
colFormat           = repmat({'char'}, 1, nbCol);
colWidth            = {60, 50, 50, 60, 60, 50, 50, 70, 70, 110, 110, 110, 40, 150, 150, 50, 50, 50};
colEditable         = false(1, nbCol);


%% Cr�ation de la figure.
hdlFig   = figure(  'MenuBar','None', ...
    'Units', 'pixels', ...
    'Position',[100 10 1600 600], ...
    'Name', Lang('','Select datagrams you want to save'), ...
    'Color', [229/255 229/255 229/255], ...
    'Name', Entete, ...
    'Visible', 'on', ...
    'Tag', 'tagUISummaryConfigBOB', ...
    'Resize', Resizable);

% D�finition des menus de sauvegarde.
hdlMenu = uimenu(hdlFig, 'Text', 'Menu figure');
uimenu(hdlMenu, 'Text', 	Lang('Sauver la figure', 'Save Figure As'), ...
    'Callback', @BOB_cbMenuSaveAs);

uimenu(hdlMenu, 'Text',    Lang('Sauver data (format CSV)', 'Save Data in CSV'), ...
    'Callback', @BOB_cbMenuSaveDataInCsv, 'Separator','on');
uimenu(hdlMenu, 'Text',    Lang('Recharger data (format CSV)', 'Load Data from CSV'),...
    'Callback', {@BOB_cbMenuLoadDataFromCsv, idCol, colWidth});

strFR = 'Avez-vous d�j� pens� � sauvegarder le tableau dans un fichier CSV ?';
strUS = 'Did you already think to save table in a Csv file ?';
msgExit = Lang(strFR, strUS);
uimenu(hdlMenu,'Text',     Lang('Fermer Figure', 'Close Figure'), ...
    'Callback', {@BOB_cbBtnOK, msgExit}, ...
    'Separator','on');

% Cr�ation du container vertical
bgColorMainPnl  = [160/255 160/255 160/255];
bgColorAnxPnl   = [180/255 180/255 180/255];

hMainBox        = uiextras.VBox('Parent', hdlFig, 'Spacing', 5, ...
    'Tag', 'tagMainBox', ...
    'BackgroundColor', bgColorMainPnl);

hDataTableBox   = uiextras.HBox('Parent', hMainBox, 'Padding', 5, 'Spacing', 5 , 'Tag', 'tagTableBox', 'BackgroundColor', bgColorAnxPnl);
hBtnBox         = uiextras.HBox('Parent', hMainBox, 'Tag', 'tagPlotBox', 'BackgroundColor', bgColorAnxPnl);

% Appel de la fonction de chargement si la table.
if isempty(tabData)
    my_warndlg(Lang('SVP, charger un fichier de donn�es de Survey BOB.', 'Please, input data from a Survey File.'), 1);
    BOB_cbMenuLoadDataFromCsv(hdlMenu, [],idCol, colWidth);
    if isappdata(hdlFig, 'TabData')
        tabData = getappdata(hdlFig, 'TabData');
    else
        delete(hdlFig);
        return
    end
end


%% Affichage des panneaux
% Panel et container Date Debut/Fin
hPnlBoxDataTable           = uiextras.BoxPanel('Parent', hDataTableBox, ...
    'Tag', 'tagPnlBoxDataTable', ...
    'FontWeight', 'bold', ...
    'FontAngle', 'italic', ...
    'Title',  Lang('Analyse des angles mesur�s et estim�s.', 'Analyse of measured and estimated angles.'));

hPnlDataTable              = uipanel('Parent', hPnlBoxDataTable, ...
    'Tag', 'tagPnlDataTable', ...
    'Title','');

% Panel et container de saisie de valeur du Compass.
hPnlBoxBtnOKCancel      = uiextras.BoxPanel('Parent', hBtnBox, ...
    'Tag', 'tagPnlBoxBtnOKCancel', ...
    'FontWeight', 'bold', ...
    'FontAngle', 'italic', ...
    'Title',  'Choose operation to follow');

hPnlBtnOKCancel         = uipanel('Parent', hPnlBoxBtnOKCancel, ...
    'Tag', 'tagPnlCompass', ...
    'Title','');

% Ajustement de la taille des panels d'une cellule
set(hMainBox,       'Sizes', [-80 -20]);    % Taille en pourcentage


%% Remplissage des colonnes.
% Customisation des colonnes et lignes si certaines conditions ne
% sont pas remplies.

%% Remplissage des colonnes.
% Customisation des colonnes et lignes si certaines conditions ne
% sont pas remplies.
[flag, htmlTabData] = BOB_fcnCustomDataWithHtml(idCol, colWidth, tabData); %#ok<ASGLU>


%% Cr�ation de la table
% Obligatoire pour disposer d'une case � cocher.
colEditable(idColProcessing)    = true;
colFormat{idColProcessing}      = 'logical';

if flagEditable
    colEditable(1:2)                = true;
    colEditable(idColComment)       = true;
    colEditable(idColProcessing)    = true;
    hdlDataTable = uitable(... 
        'Parent',           hPnlDataTable, ...
        'Data',             htmlTabData, ...
        'Tag',              'tagTabData', ...
        'ColumnName',       colNames, ...
        'ColumnEditable',   colEditable, ...
        'ColumnFormat',     colFormat, ...
        'Position',         [10 20 1550 400], ...
        'ColumnWidth',      colWidth, ...
        'CellSelectionCallback', {@cbSelectCellFromTable, idCol});
else
    hdlDataTable = uitable(...
        'Parent',           hPnlDataTable, ...
        'Data',             htmlTabData, ...
        'Tag',              'tagTabData', ...
        'ColumnName',       colNames, ...
        'ColumnEditable',   colEditable, ...
        'ColumnFormat',     colFormat, ...
        'Position',         [10 20 1550 400], ...
        'ColumnWidth',      colWidth);
end

%% Customisation de la table.
% Redimensionnement automatique des colonnes
jScroll = findjobj(hdlDataTable);
jTable  = jScroll.getViewport.getView;

% % % % % Alignement central du texte.
% % % % renderer    = javax.swing.table.DefaultTableCellRenderer;
% % % % renderer.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
% % % % renderer.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
% % % % for k=1:nbCol
% % % %     jTable.getColumnModel.getColumn(k-1).setCellRenderer(renderer);
% % % % end

% Enable multiple row selection, auto-column resize, and auto-scrollbars
jScroll.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
jScroll.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
% jTable.setSelectionMode(javax.swing.ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
jTable.setAutoResizeMode(jTable.AUTO_RESIZE_SUBSEQUENT_COLUMNS);

%% Cr�ation des boutons de validation ou annulation

setappdata(hdlFig,    'flagOKCancel', 1);
setappdata(hdlFig,    'TabData', tabData);

strFR = 'Avez-vous d�j� pens� � sauvegarder le tableau dans un fichier CSV ?';
strUS = 'Did you already think to save table in a Csv file ?';
flag = BOB_fcnInitPnlBtnOKCancel(hPnlBtnOKCancel, 'FlagOtherButtons', 0, 'MsgExit', Lang(strFR, strUS)); %#ok<NASGU>

%% Centrage de la figure.

posFig    = get(hdlFig, 'Position');
heightFig = posFig(4);
widthFig  = posFig(3);
centrageFig(widthFig, heightFig, 'Fig', hdlFig)

% Attente de commande sur la fen�tre.
uiwait(hdlFig);

pause(0.5);

if ishandle(hdlFig)
    flagOKCancel = getappdata(hdlFig,'flagOKCancel');
    newTabData = getappdata(hdlFig, 'TabData');
    % Fermeture de l'IHM
    delete(hdlFig);
end


%% = Callback =====================
% Callback de gestion de l'�dition des cellules.
function cbSelectCellFromTable(hObject, event, idCol)

hdlFig  = ancestor(hObject, 'figure', 'toplevel');
data    = get(hObject,'Data');

if isempty(event.Indices)
    return
end

tabData = getappdata(hdlFig, 'TabData');

% Bo�te de dialogue pour modifier le Cycle.
if event.Indices(2) == 1
    % Saisie du n� de cycle
    pppp = cell2str(data(:,1));
    strCycle = {};
    for k=1:size(pppp,1)
        if ~contains(pppp(k,:), 'Calibration')
            strCycle{end+1} = my_deleteTagHTML(pppp(k,:)); %#ok<AGROW>
            % Suppression des blancs de fin de cellules.
            strCycle{end} = deblank(strCycle{end});
            strCycle{end} = regexprep(strCycle{end}, 'C', '');
        end
    end
    [C, ~, ~] = unique(strCycle);
    % Tri alphanum�rique
    C = sort(str2double(C));
    str = {};
    for k=1:numel(C)
        str{k} = num2str(C(k)); %#ok<AGROW>
    end
    % R�cup�ration de la cha�ne HTML pour la remise en forme apr�s
    % changement.
    str1 = 'Veuillez saisir le N� de cycle';
    str2 = 'Please, input Cycle Number';
    strHtml = fcnRecupTagHTML(cell2str(data(event.Indices(1),1)));
    [rep, validation] = my_listdlg(Lang(str1,str2), str, 1, 'SelectionMode', 'Single');
    if validation
        strCycle                        = ['C' num2str(rep)];
        data{event.Indices(1), 1}       = [strHtml strCycle];
        % Mise � jour du secteur concern�.
        tabData{event.Indices(1), 1}    = strCycle;
    else
        return
    end
    
    
elseif  event.Indices(2) == 2
    % Saisie du n� de secteur
    pppp = cell2str(data(:,2));
    strSector = {};
    for k=1:size(pppp,1)
        if ~contains(pppp(k,:), 'NaN')
            strSector{end+1} = my_deleteTagHTML(pppp(k,:)); %#ok<AGROW>
            % Suppression des blancs de fin de cellules.
            strSector{end} = deblank(strSector{end});
            strSector{end} = regexprep(strSector{end}, 'S', '');
        end
    end
    [S, ~, ~] = unique(strSector);
    % Tri alphanum�rique
    S = sort(str2double(S));
    str = {};
    for k=1:numel(S)
        str{k} = num2str(S(k)); %#ok<AGROW>
    end
    % R�cup�ration de la cha�ne HTML pour la remise en forme apr�s
    % changement.
    strHtml = fcnRecupTagHTML(cell2str(data(event.Indices(1),2)));
    str1 = 'Veuillez saisir le N� de sector';
    str2 = 'Please, input Sector Number';
    [rep, validation] = my_listdlg( Lang(str1,str2), str, 1, 'SelectionMode', 'Single');
    if validation
        strSector                   = ['S' num2str(rep)];
        data{event.Indices(1), 2}   = [strHtml strSector];
        % Mise � jour du secteur concern�.
        tabData{event.Indices(1), 2} = strSector;
    else
        return
    end
    
elseif  event.Indices(2) == idCol.Comment
    content = data(event.Indices(1), event.Indices(2));
    rep = inputdlg('Measure Comment', Lang('Ajouter un commentaire', 'Please, input comment'), 10, content);
    if ~isempty(rep)
        data{event.Indices(1), idCol.Comment}       = rep{:};
        % Mise � jour du commentaire concern�.
        tabData{event.Indices(1), idCol.Comment}    = rep{:};
    end
end

% Mise � jour des containers de data.
set(hObject, 'data', data);
setappdata(hdlFig, 'TabData', tabData);


%% = Callback =====================
% Callback de r�cup�ration des Tags HTML  en d�but de cellules.
function htmlStr = fcnRecupTagHTML(str)

[startIndex,endIndex]   = regexp(str, '<[^>]*>');
htmlStr                 = str(startIndex(1):endIndex(end));
