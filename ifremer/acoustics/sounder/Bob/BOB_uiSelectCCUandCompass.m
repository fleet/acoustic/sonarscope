% IHM de saisie de la configuration de BOB : fichier et intervall de CCU et
% valeurs de Compass.
%
%
% Syntax
%   [flagOKCancel, sCCUandCompass] = BOB_uiSelectCCUandCompass(...)
%
% Input Arguments
%
% Name-Value Pair Arguments
%  	NomFicCCULog 	: nom du fichier CCU.
%   NomDirRawData   : nom du r�pertoire contenant les fichiers RAW (ou HAC par�s conversion)
%   Entete          : libell� de l'entete de la fen�tre.
%   Resizable       : option de redimensionnement de la fen�tre.
%   DateBegin       : date de d�but de l'intervalle.
%   DateEnd         : date de fin de l'intervalle.
%
% Output Arguments
%   flagOKCancel        : flag r�sultant de l'action OK/Cancel
%   sCCUandCompass      : structure comportant tous les param�tres CCU et
%                         de Compass
%   subCCUData          : extraction des donn�es CCU dans l'intervalle de
%                         temps.
%   subPAROSData        : extraction des donn�es PAROS dans l'intervalle de
%                         temps.
%
% Examples
%     [flagOKCancel, sCCUandCompass] = BOB_uiSelectCCUandCompass('nomFicCCULog', './20130917_expurge_ccu.log', ...
%                                                        'Resizable', 'on');
%     [flagOKCancel, sCCUandCompass, subCCUData, subPAROSData] = BOB_uiSelectCCUandCompass('nomFicCCULog', ...
%                  'F:\SonarScopeData\BOB\Data\ESSBOB2\20130922_ccu.log', ...
%                  'NomDirRawData', ...
%                  'F:\SonarScopeData\BOB\Data\ESSBOB2', ...
%                  'Resizable', 'on');
%
% See also BOB_uiSelectConfig Authors
% Authors : GLU, A. OGOR
% ----------------------------------------------------------------------------
function [flagOKCancel, sCCUandCompass, subCCUData, subPAROSData] = BOB_uiSelectCCUandCompass(varargin)

persistent persistent_nomFicCCULog
persistent persistent_nomDirRawData
persistent persistent_dateBegin
persistent persistent_dateEnd

flagOKCancel   = 0; %#ok<NASGU>
sCCUandCompass = [];
subCCUData     = [];
subPAROSData   = [];

[varargin, nomFicCCULog]  = getPropertyValue(varargin, 'NomFicCCULog',  '');
[varargin, nomDirRawData] = getPropertyValue(varargin, 'NomDirRawData', '');
[varargin, Entete]        = getPropertyValue(varargin, 'Entete',        'IFREMER-SonarScope');
[varargin, nbSectors]     = getPropertyValue(varargin, 'NbSectors',     []);
[varargin, Resizable]     = getPropertyValue(varargin, 'Resizable',     'off');
[varargin, dateBegin]     = getPropertyValue(varargin, 'DateBegin',     []);
[varargin, dateEnd]       = getPropertyValue(varargin, 'DateEnd',       []); %#ok<ASGLU>
flag = 0;  %#ok<NASGU>

if isempty(nomFicCCULog)
    nomFicCCULog = persistent_nomFicCCULog;
end

if isempty(nomDirRawData)
    nomDirRawData = persistent_nomDirRawData;
end

if isempty(dateBegin)
    if isempty(persistent_dateBegin)
        persistent_dateBegin = datestr(now, 'dd-mmm-yyyy HH:MM:SS.FFF');
    end
    dateBegin = persistent_dateBegin;
end

if isempty(dateEnd)
    if isempty(persistent_dateEnd)
        persistent_dateEnd = datestr(now+1, 'dd-mmm-yyyy HH:MM:SS.FFF');
    end
    dateEnd = persistent_dateEnd;
end
%% Cr�ation de la figure.
hdlFig   = figure(  'Menubar', 'none',...
    'Units', 'pixels', ...
    'Position',[400 10 600 540], ...
    'Name', Lang('','Select datagrams you want to save'), ...
    'Color', [229/255 229/255 229/255], ...
    'CloseRequestFcn', {@BOB_cbClose, []}, ...
    'Name', Entete, ...
    'Visible', 'on', ...
    'Tag', 'tagUISelectConfigBOB', ...
    'Resize', Resizable);

% Cr�ation du container vertical
bgColorMainPnl  = [160/255 160/255 160/255];
bgColorAnxPnl   = [180/255 180/255 180/255];

hMainBox    = uiextras.VBox('Parent', hdlFig, 'Spacing', 5, ...
    'Tag', 'tagMainBox', ...
    'BackgroundColor', bgColorMainPnl);

hCCUFileBtnBox      = uiextras.VBox('Parent', hMainBox, 'Tag', 'tagCCUFileBtnBox', 'BackgroundColor', bgColorAnxPnl);
hDateEditBox        = uiextras.HBox('Parent', hMainBox, 'Padding', 5, 'Spacing', 5 , 'Tag', 'tagh1DateEditBox', 'BackgroundColor', bgColorAnxPnl);
hCompassBtnBox      = uiextras.VBox('Parent', hMainBox, 'Tag', 'tagCCUFileBtnBox', 'BackgroundColor', bgColorAnxPnl);
hOkCancelBtnBox     = uiextras.VBox('Parent', hMainBox, 'Tag', 'tagOkCancelBtnBox', 'BackgroundColor', bgColorAnxPnl);


% Panel des boutons de sorties.
hPnlOkCancelBtn     = uipanel('Parent', hOkCancelBtnBox, 'Title','', 'Tag', 'hPnlBtn');

%% Affichage des panneaux
% Panel et container Date Debut/Fin
hPnlBoxDate           = uiextras.BoxPanel('Parent', hDateEditBox, ...
    'Tag', 'tagPnlBoxDate', ...
    'FontWeight', 'bold', ...
    'FontAngle', 'italic', ...
    'Title',  'Date Measurement');

hPnlDate              = uipanel('Parent', hPnlBoxDate, ...
    'Tag', 'tagPnlDate', ...
    'Title','');

% Panel et container de sl�ection d'un fichier de CCU
hPnlBoxCCU           = uiextras.BoxPanel('Parent', hCCUFileBtnBox, ...
    'Tag', 'tagPnlBoxCCU', ...
    'FontWeight', 'bold', ...
    'FontAngle', 'italic', ...
    'Title',  'CCU file and Raw Data Selection');

hPnlCCU             = uipanel('Parent', hPnlBoxCCU, ...
    'Tag', 'tagPnlCCU', ...
    'Title','');

% Panel et container de saisie de valeur du Compass.
hPnlBoxCompass      = uiextras.BoxPanel('Parent', hCompassBtnBox, ...
    'Tag', 'tagPnlBoxCompass', ...
    'FontWeight', 'bold', ...
    'FontAngle', 'italic', ...
    'Title',  'Compass Input');

hPnlCompass             = uipanel('Parent', hPnlBoxCompass, ...
    'Tag', 'tagPnlCompass', ...
    'Title','');


% Ajustement de la taille des panels d'une cellule
set(hMainBox,       'Sizes', [-30 -40 -20 -10]);    % Taille en %



flagEnableProcess = 'on';
% Cr�ations des champs de s�lection d'un fichier CCU.
flag = BOB_fcnInitPnlSelectCCUAndData(hPnlCCU, nomFicCCULog, nomDirRawData, flagEnableProcess); %#ok<NASGU>

% Cr�ation des champs de configuration, de commandes de la plate-forme.
flag = BOB_fcnInitPnlDateMeasurement(hPnlDate, flagEnableProcess, dateBegin, dateEnd, nbSectors); %#ok<NASGU>

% Cr�ations des champs de s�lection d'une valeur de Compass.
flag = BOB_fcnInitPnlCompass(hPnlCompass); %#ok<NASGU>

% Cr�ation des boutons de validation ou annulation.
setappdata(hdlFig,'flagOKCancel', 1);
flag = BOB_fcnInitPnlBtnOKCancel(hPnlOkCancelBtn); %#ok<NASGU>


%% Centrage de la figure.
posFig          = get(hdlFig, 'Position');
heightFig       = posFig(4);
widthFig        = posFig(3);
centrageFig(widthFig, heightFig, 'Fig', hdlFig)

% Make the GUI blocking
uiwait(hdlFig);

% Conservation des infos de dates.
hControl = findobj(hdlFig, 'tag', 'tagEditFileCCU');
persistent_nomFicCCULog = get(hControl, 'String');
hControl = findobj(hdlFig, 'tag', 'tagEditRawDir');
persistent_nomDirRawData = get(hControl, 'String');
hControl = findobj(hdlFig, 'tag', 'tagEditDateBegin');
persistent_dateBegin = get(hControl, 'String');
hControl = findobj(hdlFig, 'tag', 'tagEditDateEnd');
persistent_dateEnd = get(hControl, 'String');

flagOKCancel            = getappdata(hdlFig,'flagOKCancel');
if flagOKCancel
    [flag, sCCUandCompass]  = BOB_fcnSaveCCUandCompass(hdlFig);
    if ~flag
        my_warndlg('Probl�me dans la sauvegarde des param�tres CCU et Compass', 1);
    end
end

if isappdata(hdlFig,'subCCUData')
    subCCUData      = getappdata(hdlFig,'subCCUData');
    subPAROSData    = getappdata(hdlFig,'subPAROSData');
else
    if flagOKCancel
        % For�age de l'extraction pour le cas o� l'op�rateur ne l'a pas d�j�
        % fait.
        hControl        = findobj(hdlFig, 'tag', 'tagBtnProcessCCU');
        BOB_cbBtnProcessCCU(hControl, [], nbSectors);
        subCCUData      = getappdata(hdlFig,'subCCUData');
        subPAROSData    = getappdata(hdlFig,'subPAROSData');
    end
end

% Fermeture de l'IHM
delete(hdlFig);
drawnow;

