% IHM de saisie de la configuration de BOB.
%
%
% Syntax
%   [flagOKCancel, sConfigBOB] = BOB_uiSelectConfig(...)
%
% Input Arguments
%
% Name-Value Pair Arguments
%   fileNameConfig 	: nom du fichier XML de configuration de BOB.
%   Entete          : libell� de l'entete de la fen�tre.
%   Resizable       : option de redimensionnement de la fen�tre.
%
% Output Arguments
%   flagOKCancel    : flag r�sultant de l'action OK/Cancel
%   sConfigBOB      : structure comportant tous les param�tres de
%                     configuration.
%   sEstimateSector : structure comportant les valeurs des angles secteur par secteur.
%
% Examples
%   [flagOKCancel, valueEstimateCompass, sConfigBob, sEstimSectors]  = BOB_uiSelectConfig;
%
% See also BOB_uiSelectConfig Authors
% Authors : GLU, A. OGOR
% ----------------------------------------------------------------------------

function [flagOKCancel, valueEstimateCompass, sConfigBOB, sEstimateSector] = BOB_uiSelectConfig(varargin)

flagOKCancel    = 0; %#ok<NASGU>

[varargin, fileNameConfig] = getPropertyValue(varargin, 'fileNameConfig', '');
[varargin, Entete]         = getPropertyValue(varargin, 'Entete',         'IFREMER-SonarScope');
[varargin, Resizable]      = getPropertyValue(varargin, 'Resizable',      'off'); %#ok<ASGLU>

% La valeur de Compass est demand�e pour trac�s des Estimations
valueEstimateCompass        = NaN;
sConfigBOB          = [];
sEstimateSector     = [];
%% Cr�ation de la figure.
hdlFig   = figure(  'Menubar', 'none',...
    'Units', 'pixels', ...
    'Position',[400 10 600 800], ...
    'Name', Lang('Saisie de la configuration','Configuration selection'), ...
    'Color', [229/255 229/255 229/255], ...
    'CloseRequestFcn', {@BOB_cbClose, []}, ...
    'Name', Entete, ...
    'Visible', 'on', ...
    'Tag', 'tagUISelectConfigBOB', ...
    'Resize', Resizable);

% Cr�ation du container vertical
bgColorMainPnl  = [160/255 160/255 160/255];
bgColorAnxPnl   = [180/255 180/255 180/255];

hMainBox    = uiextras.VBox('Parent', hdlFig, 'Spacing', 5, ...
    'Tag', 'tagMainBox', ...
    'BackgroundColor', bgColorMainPnl);

hH2Box              = uiextras.HBox('Parent', hMainBox, 'Padding', 5, 'Spacing', 5 , 'Tag', 'tagh1VBox', 'BackgroundColor', bgColorAnxPnl);

hH2V1Box            = uiextras.HBox('Parent', hH2Box, 'Padding', 5, 'Spacing', 5 , 'Tag', 'tagh11HBox', 'BackgroundColor', bgColorAnxPnl);
hH1V2VBox           = uiextras.VBox('Parent', hH2Box, 'Padding', 5, 'Spacing', 5 , 'Tag', 'tagh12HBox', 'BackgroundColor', bgColorAnxPnl);
hH1V2V1VBox         = uiextras.VBox('Parent', hH1V2VBox, 'Padding', 5, 'Spacing', 5 , 'Tag', 'tagh121VBox', 'BackgroundColor', bgColorAnxPnl);
hH1V2V2VBox         = uiextras.VBox('Parent', hH1V2VBox, 'Padding', 5, 'Spacing', 5 , 'Tag', 'tagh122VBox', 'BackgroundColor', bgColorAnxPnl);
hBoxSaveRestore     = uiextras.VBox('Parent', hMainBox, 'Tag', 'tagSaveRestoreBtnBox', 'BackgroundColor', bgColorAnxPnl);
hBoxOkCancel        = uiextras.VBox('Parent', hMainBox, 'Tag', 'tagOkCancelBtnBox', 'BackgroundColor', bgColorAnxPnl);


% Panel des boutons de sorties.
hBoxPnlSaveRestore  = uiextras.BoxPanel('Parent', hBoxSaveRestore, ...
    'Tag', 'tagPnlSaveRestore', ...
    'FontWeight', 'bold', ...
    'FontAngle', 'italic', ...
    'Title',  Lang('Sauvegarde/restauration de configuration (fichier Xml)', 'Save/Restore Config  (Xml file)'));
hPnlBtnSaveRestore  = uipanel('Parent', hBoxPnlSaveRestore, 'Title','', 'Tag', 'hPnlBtnSaveRestore');
hPnlBtnOkCancel     = uipanel('Parent', hBoxOkCancel, 'Title','', 'Tag', 'hPnlBtnOkCancel');

%% Affichage des panneaux

% Ajustement de la taille des panels d'une cellule
set(hMainBox,       'Sizes', [-1 80 90]);

% Panel et container AutoCommand
hBoxPnlAC           = uiextras.BoxPanel('Parent', hH2V1Box, ...
    'Tag', 'tagPnlAC', ...
    'FontWeight', 'bold', ...
    'FontAngle', 'italic', ...
    'Title',  'Auto Commands');

hPnlAC              = uipanel('Parent', hBoxPnlAC, ...
    'Tag', 'tagPnlAC', ...
    'FontWeight', 'bold', ...
    'FontAngle', 'italic');
% Panel et container Etalonnage
hBoxPnlEtalonnage   = uiextras.BoxPanel('Parent', hH1V2V1VBox, ...
    'Tag', 'tagPnlEtalonnage', ...
    'FontWeight', 'bold', ...
    'FontAngle', 'italic', ...
    'Title',  'Calibration');
hPnlEtalonnage = uipanel('Parent', hBoxPnlEtalonnage, ...
    'Tag', 'tagPnlAC', ...
    'FontWeight', 'bold', ...
    'FontAngle', 'italic');

% Panel et container Mode Fractionne
hBoxPnlModeFraction = uiextras.BoxPanel('Parent', hH1V2V2VBox, ...
    'Tag', 'tagPnlModeFraction', ...
    'FontWeight', 'bold', ...
    'FontAngle', 'italic', ...
    'Title',  'Split Mode');
hPnlModeFraction = uipanel('Parent', hBoxPnlModeFraction, ...
    'Tag', 'tagPnlAC', ...
    'FontWeight', 'bold', ...
    'FontAngle', 'italic');

% Cr�ation des champs de configuration, de commandes de la plate-forme.
flag = BOB_fcnInitPnlCommands(hPnlAC, hPnlEtalonnage, hPnlModeFraction); %#ok<NASGU>

% Cr�ation des boutons de validation ou annulation.
flag = BOB_fcnInitPnlSaveRestore(hPnlBtnSaveRestore, fileNameConfig); %#ok<NASGU>

% Attachement de la valeur Estim�e de compass
setappdata(hdlFig, 'ValueEstimateCompass', valueEstimateCompass);

% Cr�ation des boutons de validation ou annulation.
setappdata(hdlFig,'flagOKCancel', 1);
flag = BOB_fcnInitPnlButtons(hPnlBtnOkCancel); %#ok<NASGU>


%% Centrage de la figure.
posFig          = get(hdlFig, 'Position');
heightFig       = posFig(4);
widthFig        = posFig(3);
centrageFig(widthFig, heightFig, 'Fig', hdlFig)

% Attente de commande sur la fen�tre.
uiwait(hdlFig);

flagOKCancel  = getappdata(hdlFig,'flagOKCancel');
if flagOKCancel
    [flag, sConfigBOB]              = BOB_fcnSaveConfig(hdlFig); %#ok<ASGLU>
    valueEstimateCompass            = getappdata(hdlFig,'ValueEstimateCompass');
    if isnan(valueEstimateCompass)
        if ~isnan(valueEstimateCompass)
            value = {num2str(valueEstimateCompass)};
        else
            value = {''};
        end
        
        nomVar = {Lang('Saisie la valeur estim�e de Compas (Deg)', 'Input estimate Compass value(deg)')};
        [valueEstimateCompass, flagValid] = my_inputdlg(nomVar, value, ...
            'Entete', Lang( 'Veuillez saisir le compas manuellement', ...
            'Please input the compass Value'), 1);
        if flagValid == 1
            valueEstimateCompass = str2double(valueEstimateCompass{:});
            setappdata(hdlFig, 'ValueEstimateCompass', valueEstimateCompass);
        else
            return
        end
        
    end
    [flag, sEstimateSector] = BOB_fcnEstimateAngles(valueEstimateCompass, sConfigBOB);
    if ~flag
        return
    end
end

% Fermeture de l'IHM
delete(hdlFig);
drawnow;

