% IHM de r�sum� de la configuration de BOB.
%
%
% Syntax
%   flag = BOB_uiSummaryConfig(...)
%
% Input Arguments
%   StructSector 	: structure comportant le comportage du balayage.
%
% Name-Value Pair Arguments
%   Entete          : libell� de l'entete de la fen�tre.
%   Resizable       : option de redimensionnement de la fen�tre.
%
% Output Arguments
%   flag            : flag de comportement de la fonction
%
% Examples
%   [flagOKCancel, valueCompass, sConfigBOB, structEstimatedSector] = BOB_uiSelectConfig;
%   flag  = BOB_uiSummaryConfig(structEstimatedSector);
%
% See also BOB_uiSelectConfig Authors
% Authors : GLU, A. OGOR
% ----------------------------------------------------------------------------
function flag = BOB_uiSummaryConfig(StructSector, varargin)

flag = 0;

[varargin, Entete]     = getPropertyValue(varargin, 'Entete',     'IFREMER-SonarScope');
[varargin, StatusData] = getPropertyValue(varargin, 'StatusData', 'Simulated');
[varargin, Resizable]  = getPropertyValue(varargin, 'Resizable',  'off'); %#ok<ASGLU>

if isnan(StructSector.Compass)
    str1 = sprintf('La valeur de compas n''est pas renseign�e. Veuillez la saisie dans l''IHM %s', StatusData);
    str2 = sprintf('Compass value is not filled. Please fill it in MMI %s', StatusData);
    my_warndlg(Lang(str1,str2), 1);
    return
end

%% Cr�ation de la figure.
hdlFig   = figure(  'MenuBar','None', ...
    'Units', 'pixels', ...
    'Position',[100 10 1500 600], ...
    'Name', Lang('Saisie de la configuration','Configuration selection'), ...
    'Color', [229/255 229/255 229/255], ...
    'Name', Entete, ...
    'Visible', 'on', ...
    'Tag', 'tagUISummaryConfigBOB', ...
    'Resize', Resizable);

% D�finition des menus de suavegarde.
hdlMenu = uimenu(hdlFig, 'Text', 'Menu figure');
uimenu(hdlMenu, 'Text', 'Save As','Callback', @BOB_cbMenuSaveAs);
hClose = @close;
uimenu(hdlMenu, 'Text', 'Close Figure', 'Callback', hClose);

% Cr�ation du container vertical
bgColorMainPnl = [160/255 160/255 160/255];
bgColorAnxPnl  = [180/255 180/255 180/255];

hMainBox        = uiextras.VBox('Parent', hdlFig, 'Spacing', 5, ...
    'Tag', 'tagMainBox', ...
    'BackgroundColor', bgColorMainPnl);

hSummTableBox   = uiextras.HBox('Parent', hMainBox, 'Padding', 5, 'Spacing', 5 , 'Tag', 'tagTableBox', 'BackgroundColor', bgColorAnxPnl);
hPlotBox        = uiextras.HBox('Parent', hMainBox, 'Tag', 'tagPlotBox', 'BackgroundColor', bgColorAnxPnl);



%% Affichage des panneaux
% Panel et container Date Debut/Fin
hPnlBoxSummTable           = uiextras.BoxPanel('Parent', hSummTableBox, ...
    'Tag', 'tagPnlBoxSummTable', ...
    'FontWeight', 'bold', ...
    'FontAngle', 'italic', ...
    'Title',  sprintf('Summary of %s angles and Observation sectors', StatusData));

hPnlSummTable              = uipanel('Parent', hPnlBoxSummTable, ...
    'Tag', 'tagPnlSummTable', ...
    'Title','');

% Panel et container de saisie de valeur du Compass.
hPnlBoxPlot      = uiextras.BoxPanel('Parent', hPlotBox, ...
    'Tag', 'tagPnlBoxCompass', ...
    'FontWeight', 'bold', ...
    'FontAngle', 'italic', ...
    'Title',  sprintf('%s Angles (Compass value =  %d deg)', StatusData, StructSector.Compass));

hPnlPlot             = uipanel('Parent', hPnlBoxPlot, ...
    'Tag', 'tagPnlCompass', ...
    'Title','');
%% Affichage des contr�les
% R�sum� des angles sous forme de tableau.
rnames  = { 'Temporary Pan angles', ...
    'Transducer Flip', ...
    'Final Pan Angles', ...
    'Final Tilt Angles',...
    'Transducteur Geographic Heading (transducer pan angle 0/360�)', ...
    'Transducteur Tilt Angle (-90/90�)', ...
    'Transducteur M3D orientation (transducer pan angle -180/+180�)'};

if isnan(StructSector.SectorNumber)
    % Cas de l'analyse de CCU.
    StructSector.SectorNumber  =  1:numel(StructSector.TemporaryPanAngle);
end
cnames       = num2cell(StructSector.SectorNumber);
data(1,:)    = StructSector.TemporaryPanAngle;
data(2,:)    = StructSector.Flip';
data(3,:)    = StructSector.FinalPanAngle;
data(4,:)    = StructSector.FinalTiltAngle';
data(5,:)    = StructSector.Transducer0_360PanAngle;
data(6,:)    = StructSector.Transducer0_360TiltAngle;
data(7,:)    = StructSector.Transducer180_180Angle';

hdlSummTable = uitable( 'Parent',hPnlSummTable, ...
    'Data',data, ...
    'ColumnName',cnames,...
    'RowName',rnames',...
    'Position',[20 20 1400 230], ...
    'ColumnWidth',{'auto'});

% Dimensionnement de la 1�re colonne.
% get the row header
jScroll           = findjobj(hdlSummTable);
rowHeaderViewport = jScroll.getComponent(4);
rowHeader         = rowHeaderViewport.getComponent(0);
% height              = rowHeader.getSize
% rowHeader.setSize(80,360)

%resize the row header
newWidth    = 450; %100 pixels.
rowHeaderViewport.setPreferredSize(java.awt.Dimension(newWidth,0));
height      = rowHeader.getHeight;
rowHeader.setPreferredSize(java.awt.Dimension(newWidth,height));
rowHeader.setSize(newWidth,height);
% Redimensionnement automatique des colonnes
jTable      = jScroll.getViewport.getView;
jTable.setAutoResizeMode(jTable.AUTO_RESIZE_SUBSEQUENT_COLUMNS);

% Alignement central du texte.
renderer    = javax.swing.table.DefaultTableCellRenderer;
renderer.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
for k=1:numel(StructSector.SectorNumber)
    jTable.getColumnModel.getColumn(k-1).setCellRenderer(renderer);
end

%% R�sum� des angles sous formes de graphes.
% Visualisation du positionnement en Pan.
hdlAxes(1) = axes('parent', hPnlPlot, 'Position', [0.1 0.05 0.4 0.75]) ;


% Make sure limits are correct.
hdlPolar(1) = polarplot(0, -1, 'parent', hdlAxes(1));
hLabelAnglesPan  = uicontrol(   'Parent',               hPnlPlot,...
    'Style',                'text', ...
    'Units',                'normalized',...
    'HorizontalAlignment',  'left',...
    'Position',             [0.01 0.90 0.5 0.1],...
    'Tag',                  'tagLabelAnglesPan',...
    'FontWeight',           'bold',...
    'String',               Lang('Angles de Transducteur Pan', 'Transducer Pan Angles')); %#ok<NASGU>

% Rotation de 90� de l'origine des axes.
view(-90,90);

% Get the handles of interest (using volatile info above).
hands       = findall(hdlFig,'parent', hdlAxes(1), 'Type', 'text');
hands       = hands(strncmp('  ', get(hands,'String'), 2));
hands       = sort(hands);
% R�affectation des labels des radius � vide
set(hands,'String',[])

% R�affectation des Ticks sym�trie verticale pour avoir l'Est � 90�.
ph              = findall(hdlAxes(1),'type','text');
ps              = get(ph,'string');
pppp            = str2double(ps);
subNaN          = isnan(pppp);
bidon           = find(subNaN == 1);
pppp(~subNaN)   = 360 - pppp(~subNaN);
sub             = (pppp == 360);
pppp(sub)       = pppp(sub)-360;
polarTicks      = cellstr(num2str(pppp, '%d'));
polarTicks(subNaN)    = repmat({''}, numel(bidon), 1);
set(ph,{'string'},polarTicks);

% Pour �tre conforme au trac� en sens horaire.
data(7,:) = 360 - data(7,:);

for k=1:size(data,2)
    if k < size(data,2)
        ppppTheta   = [data(7,k) data(7,k) data(7,k+1) data(7,k+1)];
        ppppRho     = [0 1 1 0];
    end
    hold on
    h = polarplot(ppppTheta*pi/180,ppppRho,'*r', 'parent', hdlAxes(1)); %#ok<NASGU>
    
    % Trac� polaire pour superposition des secteurs � cheval sur les axes.
    ppppTheta2 = [data(7,k)-3.5 data(7,k)-3.5 data(7,k)+3.5 data(7,k)+3.5];
    h2 = polarplot(ppppTheta2*pi/180,ppppRho,'-', 'parent', hdlAxes(1));
    patch( get(h2,'XData'), get(h2,'YData'), 0.2*k)
end


%% Visualisation du positionnement en Tilt.
hdlAxes(2) = axes('parent', hPnlPlot, 'Position', [0.5 0.01 0.4 0.85]) ;

% Make sure limits are correct.
hdlPolar(2) = polarplot(0, -1, 'parent', hdlAxes(2));
hLabelAnglesPan  = uicontrol(   'Parent',               hPnlPlot,...
    'Style',                'text', ...
    'Units',                'normalized',...
    'HorizontalAlignment',  'left',...
    'Position',             [0.51 0.9 0.7 0.1],...
    'Tag',                  'tagLabelAnglesTilt',...
    'FontWeight',           'bold',...
    'String',               Lang('Angles de Transducteur Tilt', 'Transducer Tilt')); %#ok<NASGU>

% Get the handles of interest (using volatile info above).
hands       = findall(hdlFig,'parent', hdlAxes(2), 'Type', 'text');
hands       = hands(strncmp('  ', get(hands,'String'), 2));
hands       = sort(hands);
% R�affectation des labels des radius � vide
set(hands,'String',[])

for k=1:size(data,2)
    if k < size(data,2)
        ppppTheta   = [data(6,k) data(6,k) data(6,k+1) data(6,k+1)];
        ppppRho     = [0 1 1 0];
    end
    hold on
    h = polarplot(ppppTheta*pi/180,ppppRho,'-+b', ...
        'parent', hdlAxes(2));
    set(h,'LineWidth', 2);
    
end

% R�affectation des Ticks sym�trie verticale pour avoir l'Est � 90�.
ph              = findall(hdlAxes(2),'type','text');
ps              = get(ph,'string');
pppp            = str2double(ps);
sub             = (pppp > 90 & pppp < 270);
pppp(sub)       = NaN;
subNaN          = isnan(pppp);
bidon           = find(subNaN == 1);
sub             = (pppp >= 270);
pppp(sub)       = pppp(sub) - 360;
polarTicks      = cellstr(num2str(pppp, '%d'));
polarTicks(subNaN)    = repmat({''}, numel(bidon), 1);
set(ph,{'string'},polarTicks);

%% Sortie
drawnow
flag = 1;
