% Lecture d'une arborescence Bob pour chargement dans la classe.
%
% Syntax
%   flag = readDataBOB(this, varargin)
%
% Input Arguments
%   this        : Instance de cl_image
%
% Output Arguments
%   flag : 1 si ecriture reussie, 0 sinon
%
% Examples
%   nomFicXml       = 'F:\SonarScopeData\BOB-3D\Data\Svalbard\EI_Lay_SvalBard.xml';
%   a               = cl_Bob(nomFicXml);
%   [flag, Data]    = readDataBob(nomFicXml)
%
% See also cl_image Authors
% Authors : GLU
% -------------------------------------------------------------------------

function [flag, Mat_EI_Cycles, nomFicMat] = read_DataBob(nomFic)

global DEBUG %#ok<GVMIS> 

flag          = 0;
Mat_EI_Cycles = [];
nomFicMat     = [];

if ~exist(nomFic, 'file')
    return
end

if isempty(DEBUG)
    DEBUG = false;
end

LayerXml                    = xml_read(nomFic);
[nomDir, nomFicSeul, ext]   = fileparts(nomFic); %#ok<ASGLU>

survey_name                 = LayerXml.Survey;
nbCycles                    = LayerXml.Dimensions.nbCycles;
nbThresholds                = LayerXml.Dimensions.nbThresholds;
nbCells                     = LayerXml.Dimensions.nbCells;
nbSectors                   = LayerXml.Dimensions.nbSectors;
             

identVariable = findIndVariable(LayerXml.Variables, 'MinThreshold');
if isVariableConstante(LayerXml.Variables(identVariable))
    this.Thresholds.Min = LayerXml.Variables(identVariable).Constant;
else
    my_warndlg(Lang('Le Minimum de seuillage doit �tre scalaire. ', 'Threshold min must be scalar.'), 1);
    return
end

identVariable = findIndVariable(LayerXml.Variables, 'MaxThreshold');
if isVariableConstante(LayerXml.Variables(identVariable))
    this.Thresholds.Max = LayerXml.Variables(identVariable).Constant;
else
    my_warndlg(Lang('Le Maximum de seuillage doit �tre scalaire. ', 'Threshold max must be scalar..'), 1);
    return
end

identVariable = findIndVariable(LayerXml.Variables, 'StepThreshold');
if isVariableConstante(LayerXml.Variables(identVariable))
    this.Thresholds.Step = LayerXml.Variables(identVariable).Constant;
else
    my_warndlg(Lang('Le pas de seuillage doit �tre scalaire. ', 'Threshold step max must be scalar..'), 1);
    return
end
thresholds = this.Thresholds.Min:this.Thresholds.Step:this.Thresholds.Max;

identVariable = findIndVariable(LayerXml.Variables, 'BeamWidth');
if isVariableConstante(LayerXml.Variables(identVariable))
    this.BeamWidth = LayerXml.Variables(identVariable).Constant;
else
    my_warndlg(Lang('Le pas de seuillage doit �tre scalaire. ', 'Threshold step max must be scalar..'), 1);
    return
end

identVariable = findIndVariable(LayerXml.Variables, 'OffsetBeam1');
if isVariableConstante(LayerXml.Variables(identVariable))
    this.OffsetBeam1 = LayerXml.Variables(identVariable).Constant;
else
    my_warndlg(Lang('Le pas de seuillage doit �tre scalaire. ', 'Threshold step max must be scalar..'), 1);
    return
end

identVariable = findIndVariable(LayerXml.Variables, 'SectorObservationTime');
if isVariableConstante(LayerXml.Variables(identVariable)) > 0
    this.SectorObservationTime = LayerXml.Variables(identVariable).Constant;
else
    if isVariableConstante(LayerXml.Variables(identVariable)) == -1
        % On initialise le temps d'observation � 60 min pour �viter
        % d'avoir � modifier les fichiers Svalbard, Marmara apr�s les
        % travaux de traitement de BOB.
        this.SectorObservationTime = 60;
    else
        my_warndlg(Lang('Temps d''observation par secteur doit �tre scalaire. ', 'Observation time by sector must be scalar.'), 1);
        return
    end
end
%% Import des donn�es Capteur, devenus inutiles

% Mat_CCU = importdata(fullfile(nomDir, LayerXml.NomFicSensor));
% figure; PlotUtils.createSScPlot(Mat_CCU.Pan, '+')


%% Chargement des donn�es.
timePPPP = NaN(nbSectors, 7);
for iSecteur = 1:nbSectors
    for iTh = 1:nbThresholds
        NomFicMat                   = fullfile(nomDir, LayerXml.NomDirRun,...
            [survey_name '-RUN' num2str(iSecteur, '%03d') '-TH' num2str(thresholds(iTh)) '-ER60-EIlay.mat']);
        Mat_Temp                    = load(NomFicMat);
        Mat_Temp.Angle_secteur_Pan  = 0;
        Mat_Temp.Thresholds         = 0;
        time_ER60_db_transp         = (Mat_Temp.time_ER60_db)';
        timePPPP(iSecteur, 1: size(time_ER60_db_transp,1))     = time_ER60_db_transp;

        
        %extraction des angles de la matrice CCU par correspondance des
        %dates
        % Recalcul de l'angle Pan suite aux observations sur le ayer de
        % Marmara o� celui-ci appara�t constant.
        Angle_secteur_Pan               = this.OffsetBeam1  + this.BeamWidth*(iSecteur-1);
        Mat_Temp.Angle_secteur_Pan      = Angle_secteur_Pan;
        Mat_Temp.Thresholds             = thresholds(iTh);
        Mat_EI_Secteurs(iSecteur,iTh)   = Mat_Temp;

        clear Mat_Temp
    end
end

% Cr�ation d'un tableau de r�f�rence temporelle par cycle et secteur.
cycleTime = this.SectorObservationTime * nbSectors;
minTime             = min([Mat_EI_Secteurs(:,:).time_ER60_db]);
validTimeByCycle    = (0:nbCycles-1) * (cycleTime/(24*60)) + minTime;
for iS=1:nbSectors
    validTime(iS, :) =  validTimeByCycle + (iS-1)*(this.SectorObservationTime/(24*60));
end


%% Traitement des absences de donn�es.
% Prise en compte de l'absence de donn�es pour le secteur 24 cycle 4
% TODO : traiter les cas d'erreurs.

if strcmpi(survey_name, 'Svalbard')
    % Le Secteur 1 est d�boubl� sur le Cycle 4 car le Pan est y rest�
    % bloqu�. Le secteur 1 commence le 9/08/2011 � 22h00 et se termine le
    % 10/08/2011 � 00:04:19, les secteurs 2,3,...23 sont d�cal�s d'1h et le
    % secteur 24 de ce cycle n'existe pas.
    for iTh=1:nbThresholds
        mm = size(Mat_EI_Secteurs(24,iTh).Sa_botER60_db, 1);
        for s24=mm:-1:4
            Mat_EI_Secteurs(24,iTh).depth_bottom_ER60_db(s24+1,:)     = Mat_EI_Secteurs(24,iTh).depth_bottom_ER60_db(s24,:);
            Mat_EI_Secteurs(24,iTh).depth_surface_ER60_db(s24+1,:)    = Mat_EI_Secteurs(24,iTh).depth_surface_ER60_db(s24,:);
            Mat_EI_Secteurs(24,iTh).lat_surfER60_db(s24+1,:)          = Mat_EI_Secteurs(24,iTh).lat_surfER60_db(s24,:);
            Mat_EI_Secteurs(24,iTh).lon_surfER60_db(s24+1,:)          = Mat_EI_Secteurs(24,iTh).lon_surfER60_db(s24,:);
            Mat_EI_Secteurs(24,iTh).lat_botER60_db(s24+1,:)           = Mat_EI_Secteurs(24,iTh).lat_botER60_db(s24,:);
            Mat_EI_Secteurs(24,iTh).lon_botER60_db(s24+1,:)           = Mat_EI_Secteurs(24,iTh).lon_botER60_db(s24,:);
            Mat_EI_Secteurs(24,iTh).Sa_surfER60_db(s24+1,:)           = Mat_EI_Secteurs(24,iTh).Sa_surfER60_db(s24,:);
            Mat_EI_Secteurs(24,iTh).Sa_botER60_db(s24+1,:)            = Mat_EI_Secteurs(24,iTh).Sa_botER60_db(s24,:);
            Mat_EI_Secteurs(24,iTh).Sv_surfER60_db(s24+1,:)           = Mat_EI_Secteurs(24,iTh).Sv_surfER60_db(s24,:);
            Mat_EI_Secteurs(24,iTh).Sv_botER60_db(s24+1,:)            = Mat_EI_Secteurs(24,iTh).Sv_botER60_db(s24,:);
            Mat_EI_Secteurs(24,iTh).time_ER60_db(:,s24+1)             = Mat_EI_Secteurs(24,iTh).time_ER60_db(:,s24) ;
            Mat_EI_Secteurs(24,iTh).vol_surfER60_db(s24+1,:)          = Mat_EI_Secteurs(24,iTh).vol_surfER60_db(s24,:);
        end
        Mat_EI_Secteurs(24,iTh).depth_bottom_ER60_db(4,:)     = NaN;
        Mat_EI_Secteurs(24,iTh).depth_surface_ER60_db(4,:)    = NaN;
        %     Mat_EI_Secteurs(24,t).lat_surfER60_db(4,:)          = NaN;
        %     Mat_EI_Secteurs(24,t).lon_surfER60_db(4,:)          = NaN;
        %     Mat_EI_Secteurs(24,t).lat_botER60_db(4,:)           = NaN;
        %     Mat_EI_Secteurs(24,t).lon_botER60_db(4,:)           = NaN;
        Mat_EI_Secteurs(24,iTh).Sa_surfER60_db(4,:)           = NaN;
        Mat_EI_Secteurs(24,iTh).Sa_botER60_db(4,:)            = NaN;
        Mat_EI_Secteurs(24,iTh).Sv_surfER60_db(4,:)           = NaN;
        Mat_EI_Secteurs(24,iTh).Sv_botER60_db(4,:)            = NaN;
        Mat_EI_Secteurs(24,iTh).time_ER60_db(:,4)             = NaN;
        Mat_EI_Secteurs(24,iTh).vol_surfER60_db(4,:)          = NaN;
    end
end

%%
if DEBUG
    % Ce programme redistribue les donn�es extraites de Mat_EI qui sont une
    % structure de la forme Secteur X Seuil X Cycle X Layer en structure Cycle X
    % Seuil X Secteur X Layer. Il transforme les donn�es en fichiers .xyz.
    
    hdlFig(1)   = figure(66666);
    set(hdlFig(1), 'Name', 'Geom�trie Tableau');
    hAxe(1)     = gca;
    set(hdlFig(1), 'Position', [284   473   672   504])
    
    hdlFig(2)   = figure(77777);
    set(hdlFig(2), 'Name', 'Geom�trie Polaire');
    hAxe(2)     = gca;
    CLim = [0 50];
    set(hdlFig(2), 'Position', [983   473   672   504])
    
    hControl = uicontrol('Position',[20 20 200 40],'String','Continue',...
              'Callback','uiresume(gcbf)');
end
% Attention, il peut y avoir des cycles manquants.

% Initialisation � NaN des matrices par Cycles.
clear Mat_EI_Cycles
Mat_Temp               = initMat_EI_Cycles(nbSectors, nbCells);
Mat_EI_Cycles(1:nbCycles,1:nbThresholds) = repmat(Mat_Temp,nbCycles,nbThresholds); 


flagSaEmpty = 0;
for iCycle=1:nbCycles
    for iTh=1:nbThresholds
        for iSecteur=1:nbSectors
            % Recherche de la proximit� de la date enregistr�e avec celle suppos�e pour r�cup�rer les donn�es.
            [diffTime, idxTime] = min(abs((Mat_EI_Secteurs(iSecteur,iTh).time_ER60_db-validTime(iSecteur,iCycle))));
            if round(diffTime) == 0
                Mat_EI_Cycles(iCycle,iTh).Sa_bot(iSecteur,:)        = Mat_EI_Secteurs(iSecteur,iTh).Sa_botER60_db(idxTime,:);
                Mat_EI_Cycles(iCycle,iTh).Sv_bot(iSecteur,:)        = Mat_EI_Secteurs(iSecteur,iTh).Sv_botER60_db(idxTime,:);
                Mat_EI_Cycles(iCycle,iTh).lat_bot(iSecteur,:)       = Mat_EI_Secteurs(iSecteur,iTh).lat_botER60_db(idxTime,:);
                Mat_EI_Cycles(iCycle,iTh).lon_bot(iSecteur,:)       = Mat_EI_Secteurs(iSecteur,iTh).lon_botER60_db(idxTime,:);
                Mat_EI_Cycles(iCycle,iTh).time_bot(iSecteur,:)      = Mat_EI_Secteurs(iSecteur,iTh).time_ER60_db(idxTime);
                
                Mat_EI_Cycles(iCycle,iTh).depth_bottom_ER60_db(iSecteur,:)  = Mat_EI_Secteurs(iSecteur,iTh).depth_bottom_ER60_db(idxTime,:);
                Mat_EI_Cycles(iCycle,iTh).time_bot(iSecteur,:)              = Mat_EI_Secteurs(iSecteur,iTh).time_ER60_db(idxTime);
                Mat_EI_Cycles(iCycle,iTh).Sa_bot(iSecteur,:)                = Mat_EI_Secteurs(iSecteur,iTh).Sa_botER60_db(idxTime,:);
                Mat_EI_Cycles(iCycle,iTh).lat_bot(iSecteur,:)               = Mat_EI_Secteurs(iSecteur,iTh).lat_botER60_db(idxTime,:);
                Mat_EI_Cycles(iCycle,iTh).lon_bot(iSecteur,:)               = Mat_EI_Secteurs(iSecteur,iTh).lon_botER60_db(idxTime,:);
                if ~isempty(Mat_EI_Secteurs(iSecteur,iTh).depth_surface_ER60_db)
                    Mat_EI_Cycles(iCycle,iTh).depth_surface_ER60_db(iSecteur,:) = Mat_EI_Secteurs(iSecteur,iTh).depth_surface_ER60_db(idxTime,:);
                    Mat_EI_Cycles(iCycle,iTh).lat_surfER60_db(iSecteur,:)       = Mat_EI_Secteurs(iSecteur,iTh).lat_surfER60_db(idxTime,:);
                    Mat_EI_Cycles(iCycle,iTh).lon_surfER60_db(iSecteur,:)       = Mat_EI_Secteurs(iSecteur,iTh).lon_surfER60_db(idxTime,:);
                    Mat_EI_Cycles(iCycle,iTh).Sa_surfER60_db(iSecteur,:)        = Mat_EI_Secteurs(iSecteur,iTh).Sa_surfER60_db(idxTime,:);
                    Mat_EI_Cycles(iCycle,iTh).Sv_surfER60_db(iSecteur,:)        = Mat_EI_Secteurs(iSecteur,iTh).Sv_surfER60_db(idxTime,:);
                    Mat_EI_Cycles(iCycle,iTh).vol_surfER60_db(iSecteur,:)       = Mat_EI_Secteurs(iSecteur,iTh).vol_surfER60_db(idxTime,:);
                end
            end
            % Assignation du Pan quelque soit le verdict de correspodance
            % de date.
            Mat_EI_Cycles(iCycle,iTh).Angle_secteur_Pan(iSecteur,:)      = Mat_EI_Secteurs(iSecteur,iTh).Angle_secteur_Pan;
        end
        
        % if DEBUG && (iTh == 1 && iCycle == 1)
        if DEBUG && iTh == 1
            % Trac� des donn�es.
            Namefig = ['Threshold ',num2str(thresholds(iTh)), ' - Cycle ', num2str(iCycle) ' - Geometry : Cartesian'];
            pppp                = flipud(Mat_EI_Cycles(iCycle,iTh).Sa_bot');
            figure(hdlFig(1));
            maxPPPP             = max(pppp(:));
            pppp(isnan(pppp))   = maxPPPP + maxPPPP/10;
            colordata           = colormap; colordata(end,:) = [1 1 1]; colormap(colordata);
            imagesc(pppp); colorbar;

            % pcolor(hAxe(1), Mat_EI_Cycles(iCycle,iTh).Sa_bot');
            title(Namefig, 'FontAngle', 'italic', 'FontSize', 8.0, 'FontWeight', 'bold');
            
            % Trac� des donn�es en G�om�trie Polar_Echogramme
            Namefig = ['Threshold ',num2str(thresholds(iTh)), ' - Cycle ', num2str(iCycle) ' - Geometry : Polar with PCOLOR'];
            figure(hdlFig(2));
            subx = 1:size(Mat_EI_Cycles(iCycle,iTh).Sa_bot, 2);
            suby = 1:size(Mat_EI_Cycles(iCycle,iTh).Sa_bot, 1);
            I(suby,subx,iSecteur) = reflec_Amp2dB(Mat_EI_Cycles(iCycle,iTh).Sa_bot);
            pcolor(hAxe(2), Mat_EI_Cycles(iCycle,iTh).lon_bot, Mat_EI_Cycles(iCycle,iTh).lat_bot, I(suby,subx,iSecteur)); colorbar
            title(Namefig, 'FontAngle', 'italic', 'FontSize', 8.0, 'FontWeight', 'bold');
            set(hAxe, 'CLim', CLim)
            shading flat
            axisGeo
            pause(0.5)
            
            uiwait(gcf);
            % saveas(hdfFig,[path_save '\Mat_EI_Cycles_' num2str(cycle),'-TH' num2str(thresholds(t))]);
            % Sauvegarde des Secteurs sous forme Cycle Threshold : lat lon Sa
            % for d=1:nblayers;
            %     dlmwrite([path_save,'\XYZ', '\Mat_EI_Cycles_' num2str(cycle),'-TH' num2str(thresholds(t)),'_BOB.xyz'],[Mat_EI_Cycles(cycle,t).lat_bot(:,d),Mat_EI_Cycles(cycle,t).lon_bot(:,d),Mat_EI_Cycles(cycle,t).Sa_bot(:,d)],'-append','delimiter',' ','precision',17);
            % end;
            % saveas(hdfFig,[path_save '\Mat_EI_Cycles_' num2str(cycle),'-TH' num2str(thresholds(t))]);
            % Sauvegarde des Secteurs sous forme Cycle Threshold : lat lon Sa
        end
    end
    % Information et sortie si une des matrices de Sa est compl�tement
    % vide.
    if all(isnan(Mat_EI_Cycles(iCycle,iTh).Sa_bot(:)))
        my_warndlg(['Le cycle ' num2str(iCycle) ' au seuil de ' num2str(thresholds(iTh)) ' est compl�tement vide: merci de v�rifier la donn�e d''origine '], 1);
        % On marque le probl�me le pb mais on laisse charger pour avoir
        % l'ensemble des messages d'erreurs.
        flagSaEmpty = 1;
    end
end

if flagSaEmpty == 1
    return
end

%% Sauvegarde temporaire (?) sous forme de fichier MatLab.

nomFicMat = ['Mat_EI_Cycles_' LayerXml.NomDirRun '.mat'];
nomFicMat = fullfile(nomDir, 'SonarScope', nomFicMat);
if ~exist(nomFicMat, 'file')
    nomDirSsc = fullfile(nomDir, 'SonarScope');
    if ~exist(nomDirSsc, 'dir')
        status = mkdir(nomDirSsc);
        if ~status
            messageErreur(nomDirSsc)
            flag = 0;
            return
        end
    end
    %     [rep, flag] = my_questdlg(Lang('Voulez-vous sauver les donn�es de Cycles en Mat File', 'Do you want to save Data Cycles in Mat Files ?'));
%     if ~isempty(rep) % R�ponse Oui ou Non effectu�e.
%     if rep == 1 && flag == 1
%         nomFicMat = fullfile(nomDir, nomFicMat);
%         [flag, nomFicMat] = my_uiputfile('*.mat', 'Give a file name', nomFicMat);
%         if ~flag
%             return
%         end
%         save(nomFicMat, 'Mat_EI_Cycles');
%     end
    save(nomFicMat, 'Mat_EI_Cycles');
end
flag = 1;


function Mat_Temp = initMat_EI_Cycles(nbSectors, nbCells)

Mat_Temp.Sa_bot                = NaN(nbSectors, nbCells);
Mat_Temp.Sv_bot                = NaN(nbSectors, nbCells);
Mat_Temp.lat_bot               = NaN(nbSectors, nbCells);
Mat_Temp.lon_bot               = NaN(nbSectors, nbCells);
Mat_Temp.time_bot              = NaN(nbSectors, nbCells);

Mat_Temp.depth_bottom_ER60_db  = NaN(nbSectors, nbCells);
% Deux niveaux de surface.
Mat_Temp.depth_surface_ER60_db = NaN(nbSectors, 2);
Mat_Temp.lat_surfER60_db       = NaN(nbSectors, 2);
Mat_Temp.lon_surfER60_db       = NaN(nbSectors, 2);
Mat_Temp.lat_bot               = NaN(nbSectors, nbCells);
Mat_Temp.lon_bot               = NaN(nbSectors, nbCells);
Mat_Temp.Sa_surfER60_db        = NaN(nbSectors, 2);
Mat_Temp.Sa_bot                = NaN(nbSectors, nbCells);
Mat_Temp.Sv_surfER60_db        = NaN(nbSectors, 2);
Mat_Temp.time_bot              = NaN(1, 1);
Mat_Temp.vol_surfER60_db       = NaN(nbSectors, 2);
Mat_Temp.Angle_secteur_Pan     = NaN(1, 1);
