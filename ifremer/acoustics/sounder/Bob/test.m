global DEBUG %#ok<GVMIS> 

%% Configuration nominale de BOB

[flagOKCancel, valueCompass, sConfigBOB, structEstimatedSector] = BOB_uiSelectConfig;
if ~flagOKCancel
    return
end
%% Configuration r�elle par interpr�tation du CCU et du Compass
nbSectors = str2double(sConfigBOB.Parameters.NbSectors.Value);
[flagOKCancel, sCCUandCompassExtract, subCCUData, subPAROSData] = BOB_uiSelectCCUandCompass( 'nomFicCCULog', ...
                                                        'E:\SonarScopeAnnexes\SonarScopeDataArchive\BOB\Data\ESSBOB2\20130922_ccuParos.log', ...
                                                        'nomDirRawData', ...
                                                        'E:\SonarScopeAnnexes\SonarScopeDataArchive\BOB\Data\ESSBOB2', ...
                                                        'Resizable', 'on', ...
                                                        'NbSectors', nbSectors, ...
                                                        'DateBegin', '21-Sep-2013 15:00:00', ...
                                                        'DateEnd', '22-Sep-2013 15:00:00');

if ~flagOKCancel
    return
end

%% Estimation de la configuration nominale.

% % % [flag, structEstimatedSector]   = BOB_fcnEstimateAngles(valueCompass, sConfigBOB);
% % % nbSectors                       = numel(structEstimatedSector.SectorNumber);

%% Appel de la fonction d'affichage des angles Estim�s.

flag = BOB_uiSummaryConfig(structEstimatedSector, 'Resizable', 'on', 'Entete', 'IFREMER - SonarScope - BOB Processing - Estimation Angle');


%% Intr�pr�tation du fichier CCU.

% % nbSectors        = numel(structEstimatedSector.SectorNumber);
% % stepInit        = str2double(sConfigBOB.Parameters.Step.Value);
% % durationInit    = str2double(sConfigBOB.Parameters.ObservationTime.Value);
% % dateBegin       = sCCUandCompassExtract.Parameters.DateBegin.Value;
% % dateEnd         = sCCUandCompassExtract.Parameters.DateEnd.Value;
% % % profile on
% % [flag, subCCUData, subPAROSData] = BOB_fcnExtractionCCU( 'NbSector', nbSectors, ...
% %                                             'StepInit',             stepInit, ...
% %                                             'DurationInit',         durationInit,  ...
% %                                             'DateBegin',            dateBegin, ...
% %                                             'DateEnd',              dateEnd, ...
% %                                             'NomFicCCULog', 'F:\SonarScopeData\BOB\Data\ESSBOB_DataEtConf\20130922_ccu.log');
% profile report
%% Affichage des donn�es de recording et d'angles.

oneFigure   = 1;
flag = BOB_fcnDisplayGraphsCCU(subCCUData, 'FlagOneFigure', oneFigure);  %#ok<NASGU>
flag = BOB_fcnDisplayGraphsCCUParosData(subPAROSData, 'FlagOneFigure', oneFigure);  %#ok<NASGU>

%% Structuration de la configuration nominale mesur�e

[flag, structMeasuredSector] = BOB_fcnExtractMeasuredAngles(nbSectors, subCCUData);

%% Lancement de la synth�se

[flag, nbCycles, tabData]   = BOB_fcnCreateDataAnalyser(sConfigBOB, sCCUandCompassExtract, structEstimatedSector, structMeasuredSector);
[flag, newTabData]          = BOB_uiDataAnalyser(tabData, 'Entete', 'Synthesis for Survey ESSBOB_DataEtConf', 'Resizable', 'on');

%% Lancement de la correction de la table par ajout/duplication/suppression des lignes ad hoc.

[flag, tabDataCorrect]      = BOB_fcnCorrectData(tabData, sConfigBOB, sCCUandCompassExtract);
[flag, newtabDataCorrect]   = BOB_uiDataAnalyser(tabDataCorrect, ...
                                                'Entete', 'Synthesis after correction for Survey ESSBOB_DataEtConf', ...
                                                'Resizable', 'on', ...
                                                'Editable', 1);

%% Simulation d'un cas d'erreur. o� l'angle de Pan est rest� fig�.

clear newSMeasuredSector  newTabData;
sz = size(structMeasuredSector.Flip);
newSMeasuredSector = structMeasuredSector;
iNumInError = 8;
for k=iNumInError+3:-1:iNumInError
    newSMeasuredSector.FinalPanAngle(k)            = newSMeasuredSector.FinalPanAngle(k-1) ;
    newSMeasuredSector.TemporaryPanAngle(k)        = newSMeasuredSector.TemporaryPanAngle(k-1);
    newSMeasuredSector.Transducer0_360PanAngle(k)  = newSMeasuredSector.Transducer0_360PanAngle(k-1);
    newSMeasuredSector.Transducer180_180Angle(k)   = newSMeasuredSector.Transducer180_180Angle(k-1);
end
% Demo de l'instroduction du pn en Cycle = 2 et Sector = 4.
if DEBUG
    figure; 
    PlotUtils.createSScPlot(newSMeasuredSector.FinalPanAngle(2:end), '-*b'); hold on;
    figure; 
    PlotUtils.createSScPlot(structMeasuredSector.FinalPanAngle(2:end), '-+r');
end
[flag, nbCycles, newTabData]    = BOB_fcnCreateDataAnalyser(sConfigBOB, sCCUandCompassExtract, structEstimatedSector, newSMeasuredSector);
[flag, newTabData]    = BOB_uiDataAnalyser(newTabData, 'Entete', 'Synthesis for Survey ESSBOB_DataEtConf', 'Resizable', 'on');
%% Lancement de la correction de la table par ajout/duplication/suppression des lignes ad hoc.
[flag, newTabDataCorrect] = BOB_fcnCorrectData(newTabData, sConfigBOB, sCCUandCompassExtract);
[flag, newTabDataCorrect] = BOB_uiDataAnalyser(newTabDataCorrect,    'Entete', 'Synthesis after correction for Survey ESSBOB_DataEtConf', ...
                                            'Resizable', 'on', ...
                                            'Editable', 1);
                                        
%% R�partition des RAW files
[flag, dirOut] = BOB_fcnDispatchRawFiles(nbSectors, newTabData, sCCUandCompassExtract);

[flag, dirOut] = BOB_fcnDispatchRawFiles(nbSectors, newTabDataCorrect, sCCUandCompassExtract);

%% Saisie des param�tres de la campagne
[flag, structConfigSurvey] = BOB_fcnInputSurveyParameters;
if ~flag
    return
end

%% Duplication des r�pertoires de config M3D.
nbSectors  = str2double(sConfigBOB.Parameters.NbSectors.Value);
stepAngle  = str2double(sConfigBOB.Parameters.Step.Value);
xRotRef    = sEstimSectors.Transducer180_180Angle(1)*pi/180;
flag = BOB_fcnDuplicateConfig(  'ConfigSurvey', sConfigSurvey, ...
                                'NbSectors', {nbSectors}, ...
                                'StepAngle', {stepAngle}, ...
                                'XRotRef', {xRotRef});
%% Conversion de toute l'arborescence en fichier HAC.

flag  = BOB_fcnMultiConvertRaw2Hac(nbSectors, sCCUandCompassExtract);

%% Purge de l'arborecence de Cache SonarScope arp�s conversion.
flag  = BOB_fcnEraseDirCacheSSC(nbSectors, 'nomDirRawData', sCCUandCompassExtract.Parameters.RawDir.Value);

%% Lancement du calcul d'EI.

flag  = BOB_fcnComputeEIByMovies(sConfigBOB, sCCUandCompassExtract, structConfigSurvey);

%% Cr�tion des fichiers BOB pour 3DV.
[flag, nomFicMatCCU, nomFicBOB] = BOB_fcnCreationBOBFilesFor3DV; 
if ~flag
    return
end


