function flag = test_BOB

global DEBUG %#ok<GVMIS> 

DEBUG = 1;

%%
nomFicXmlIn = 'F:\SonarScopeData\BOB-3D\Data\Svalbard\EI_Lay_SvalBard.bob';
a                   = cl_Bob('nomFic', nomFicXmlIn);
listLayers          = 1;% 1:get(a,'NbCycles'); %
listDataType        = [1 2 3];% Reflectivity, RxBeamIndex, RayPathRange
listThresholds      = [1 2];% 1:get(a,'NbCycles'); %
I0 = cl_image_I0;
[flag, b]           = import_BOB(I0, nomFicXmlIn, 'listLayers', listLayers, ...
                                'listDataType', listDataType, 'listThresholds', listThresholds);

%% Pour debug
a                   = read_DataBob(nomFicXmlIn);

%%
nomFicXmlIn = 'F:\SonarScopeData\BOB-3D\Data\Marmara\Marmara_2011\EI_Lay_Marmara.bob';
nomFicXmlIn = 'F:\SonarScopeData\BOB-3D\Data\Marmara\Marmara_2011\EI_Lay_Marmara_Gaye.bob';
a                   = cl_Bob('nomFic', nomFicXmlIn);
listLayers          = 1:get(a,'NbCycles'); %
listDataType        = [1 2 3];% Reflectivity, RxBeamIndex, RayPathRange
listThresholds      = 1:get(a,'NbThresholds'); %
[flag, b]           = import_BOB(cl_multidata([]), nomFicXmlIn, 'listLayers', listLayers, ...
                            'listDataType', listDataType, 'listThresholds', listThresholds);

%%
subx             = []; % 1:b(1).nbRows;
suby             = []; % 1:b(1).nbColumns;
flagImageEntiere = 1;

[flag, indLayers, subx, suby] = gestionMultiImages(b, 1, flagImageEntiere, subx, suby);

%%

ImageName = get(a ,'SurveyName');
filtreXml = fullfile('C:\Temp\BOB_MultiData', [ImageName '.xml']);
[flag, nomFicXML] = my_uiputfile('*.xml', 'Give a file name', filtreXml);
if ~flag
    return
end

%%

if flag
    GeometryType = b(1).GeometryType;
    switch GeometryType
        case cl_image.indGeometryType('LatLong')
            flag = exportGeotiffSonarScope3DViewerMultiV3(b, nomFicXML, ...
                'sizeTiff', 2500, 'subx', subx, 'suby', suby);
        otherwise
            str1 = 'Faut passer par l''autre version pour cette g�om�trie';
            str2 = 'Use the previous export for this geometry';
            my_warndlg(Lang(str1,str2), 1);
    end
end