function CINNA_RTK_Clean(ListFic)

if ~iscell(ListFic)
    ListFic = {ListFic};
end

%% Nettoyage des donn�es d'Altitude RTK dans les fichiers .all

% FiltreHeight = [];
nbFic = length(ListFic);
str1 = 'V�rification de la donn�e Height RTK des fichiers .NA';
str2 = 'Check RTK Height of .NA files';
hw = create_waitbar(Lang(str1,str2), 'N', nbFic);
for k=1:nbFic
    my_waitbar(k, nbFic, hw);
    
    %% Lecture de la hauteur RTK
    
    [flag, NANAV] = lecFicNav_CINNA(ListFic{k});
    if ~flag
        str1 = sprintf('Fichier vide : %s\n', ListFic{k});
        str2 = sprintf('Empty file : %s\n', ListFic{k});
        my_warndlg(Lang(str1,str2), 0);
        continue
    end
%     Lat      = NANAV.Lat;
%     Lon      = NANAV.Lon;
%     Time     = NANAV.Time;
%     Heading  = NANAV.Heading;
    if isfield(NANAV, 'FlagCleaned')
        FlagRTK  = NANAV.FlagCleaned;
    else
        FlagRTK  = NANAV.Flag;
    end
    if isfield(NANAV, 'AltitudeCleaned')
        Altitude = NANAV.AltitudeCleaned;
    else
        Altitude = NANAV.Altitude;
%         Altitude = medfilt1(NANAV.Altitude, 120); % filtre median de 120s pour filtrer le pilonnement
    end
%     Roll     = NANAV.Roll;
%     Pitch    = NANAV.Pitch;
%     Heave    = NANAV.Heave;
    
%     [fig1, fig2] = plot_DatagramHeight(a);
         
    %% Recherche des occurrences RTK
    
    subRTK = find(FlagRTK);
    if isempty(subRTK)
        str1 = sprintf('Le facteur de qualit� RTK ne donne aucune mesure valid�e pour "%s"', ListFic{k});
        str2 = sprintf('RTK QualityFactor does not give any validated data for "%s"', ListFic{k});
        my_warndlg(Lang(str1,str2), 0, 'Tag', 'QualityIndicatorNoRTK');
%         FigUtils.createSScFigure(54221);
%         subplot(2,1,1); PlotUtils.createSScPlot(QualityIndicator, '*'); grid on; title(Lang(str1,str2), 'Interpreter', 'none')
%         subplot(2,1,2); PlotUtils.createSScPlot(DataHeight.Height(:)); grid on; title(['Height for ' ListFic{k}], 'Interpreter', 'none')
        continue
    end
        
        %% Division en sections
    [~, nomFic2, Ext] = fileparts(ListFic{k});
    nomFic2 = [nomFic2 Ext]; %#ok<AGROW>
    val = Altitude;
    kFinSections = find(diff(subRTK) ~= 1);
    kFinSections = kFinSections(:)';
    kDebSections = [1 kFinSections+1];
    kFinSections = [kFinSections length(subRTK)]; %#ok<AGROW>
    for k2=1:length(kFinSections)
        subSection = subRTK(kDebSections(k2):kFinSections(k2));
        Title = sprintf('Edit RTK %s - Section %d/%d', nomFic2, k2, length(kFinSections));
        T = NANAV.Datetime(subSection);
        xSample = XSample('name', 'Samples',  'data', T);
        ySample = YSample('Name', 'Height', 'data', Altitude(subSection));
        signal = ClSignal('Name', Title, 'xSample', xSample, 'ySample', ySample);
        
        d = diff(T);
        subd = (d > 0);
        Heave = interp1(T(subd), NANAV.Heave(subd), T);
        ySample = YSample('Name', 'MRU-Heave', 'data', Heave);
        signal(2) = ClSignal('Name', Title, 'xSample', xSample, 'ySample', ySample);
        
        s = SignalDialog(signal, 'Title', 'Edit signal', 'cleanInterpolation', 1);
        s.openDialog();
        if s.okPressedOut
            valSection = signal(1).ySample.data;
            val(subSection) = valSection;
            figure; plot(T, valSection, 'r'); grid on;
        end
    end
    if ~isequal(Altitude, val)
        Altitude = val;
        
        % Save Height dand le fichiers cache
        
        NANAV.AltitudeCleaned = Altitude;
        NANAV.FlagCleaned     = FlagRTK;
        [nomDir, nomFic] = fileparts(ListFic{k});
        nomFicMat = fullfile(nomDir, [nomFic '.mat']);
        FormatVersion = loadmat(nomFicMat, 'nomVar', 'FormatVersion');
        save(nomFicMat, 'NANAV', 'FormatVersion')
%         my_close(fig1);
    end
end
my_close(hw, 'MsgEnd')
