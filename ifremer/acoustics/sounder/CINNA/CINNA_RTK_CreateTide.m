function CINNA_RTK_CreateTide(ListFic, nomFicTide, varargin)

persistent persistent_offsetWL

[varargin, nomDirSIS] = getPropertyValue(varargin, 'nomDirSIS', []); %#ok<ASGLU>

if ~iscell(ListFic)
    ListFic = {ListFic};
end

%% Import de la mar�e dans les fichiers .all

RTKGeoid    = [];
UseRTK      = 1;
TDebNav     = [];
TimeNav     = {};
LatNav      = {};
LonNav      = {};
AltitudeNav = {};
FlagNav     = {};
nbFic = length(ListFic);
str1 = 'Cr�ation de la mar�e � partir de la donn�e Height RTK des .NA';
str2 = 'Create Tide signal from RTK Height of .NA files';
hw = create_waitbar(Lang(str1,str2), 'N', nbFic);
for k=1:nbFic
    my_waitbar(k, nbFic, hw);
    
    [flag, NANAV] = lecFicNav_CINNA(ListFic{k});
    if ~flag
        str1 = sprintf('Fichier vide : %s\n', ListFic{k});
        str2 = sprintf('Empty file : %s\n', ListFic{k});
        my_warndlg(Lang(str1,str2), 0);
        return
    end
    % TODO : prendre  Cleaned et si �a n'existe pas envoyer un message
    
    Lat  = NANAV.Lat;
    Lon  = NANAV.Lon;
    Time = NANAV.Time;
    %     Heading  = NANAV.Heading;
    %     FlagRTK  = NANAV.Flag;
    %     Altitude = NANAV.Altitude;
    if isfield(NANAV, 'AltitudeCleaned')
        Altitude = NANAV.AltitudeCleaned;
        FlagRTK  = NANAV.FlagCleaned;
    else
        continue
    end
    %     Roll     = NANAV.Roll;
    %     Pitch    = NANAV.Pitch;
    %     Heave    = NANAV.Heave;
    
    TimeNav{end+1}     = Time.timeMat; %#ok<AGROW>
    LatNav{end+1}      = Lat; %#ok<AGROW>
    LonNav{end+1}      = Lon; %#ok<AGROW>
    AltitudeNav{end+1} = Altitude; %#ok<AGROW>
    FlagNav{end+1}     = FlagRTK; %#ok<AGROW>
    TDebNav(end+1)     = TimeNav{end}(1); %#ok<AGROW>
end
my_close(hw, 'MsgEnd')

if isempty(AltitudeNav)
    str1 = 'Il semble que vous n''avez pas valid� les donn�es RTK. Veuillez le faire avant de vouloir exporter la mar�e.';
    str2 = 'It seems that you have not checked the RTK data, please proceed before tide export.';
    my_warndlg(Lang(str1,str2), 1);
    return
end

[~, ordre] = sort(TDebNav);
TimeNav     = TimeNav(ordre);
AltitudeNav = AltitudeNav(ordre);
FlagNav     = FlagNav(ordre);

TimeNav     = horzcat(TimeNav{:});
LatNav      = horzcat(LatNav{:});
LonNav      = horzcat(LonNav{:});
AltitudeNav = horzcat(AltitudeNav{:});
FlagNav     = horzcat(FlagNav{:});

% TimeNav     = TimeNav(FlagNav);
% AltitudeNav = AltitudeNav(FlagNav);
% LonNav      = LonNav(FlagNav);
% LatNav      = LatNav(FlagNav);

[flag, HeightRTK, flagRTK, RTKGeoid, UseRTK] = processRTK(AltitudeNav, nomDirSIS, LonNav, LatNav, ...
    'RTKGeoid', RTKGeoid, 'UseRTK', UseRTK); %#ok<ASGLU>
if ~flag
    return
end

% flag = export_tide_SIMRAD2(nomFicTide, TimeNav, HeightRTK);

% fid = fopen(nomFicTide, 'w+t');
% if fid == -1
%     messageErreurFichier(nomFicTide, 'WriteFailure');
%     return
% end
% N = length(TimeNav);
% str1 = 'Export du fichier de mar�e';
% str2 = 'Export tide file';
% hw = create_waitbar(Lang(str1,str2), 'N', N);
% for k=1:N
%     my_waitbar(k, N, hw)
%     if FlagNav(k) == 1
%         fprintf(fid, '%s %f\n', timeMat2str(TimeNav(k)), HeightRTK(k));
%     else
%         fprintf(fid, '%s %f\n', timeMat2str(TimeNav(k)), NaN);
%     end
% end
% fclose(fid);
% my_close(hw, 'MsgEnd')
% fprintf(fid, '%s %f\n', datestr(TimeNav, 'dd/mm/yyyy  HH:MM:SS.FFF')', HeightRTK');

%% Offset de ligne de flottaison
% Mar�e_GPS = ZGPS - HGPS � Bathyelli

if isempty(persistent_offsetWL)
    offsetWL = 0;
else
    offsetWL = persistent_offsetWL;
end

str1 = 'Hauteur du point de r�f�rence de Nav au dessus de la ligne de flottaion';
str2 = 'Navigation reference above water line';
p = ClParametre('Name', 'Height', 'Unit', 'deg',  'MinValue', 0, 'MaxValue', 10, 'Value', offsetWL);
a = SimpleParametreDialog('params', p, 'Title', Lang(str1,str2));
a.openDialog;
flag = a.okPressedOut;
if ~flag
    return
end
offsetWL = a.getParamsValue; 
persistent_offsetWL = offsetWL;

HeightRTK = HeightRTK - offsetWL;

T = datetime(TimeNav, 'ConvertFrom', 'datenum');
figure; plot(T, HeightRTK); grid on; title('Tide (m)')

%% Ecriture du fichier de mar�e

fid = fopen(nomFicTide, 'w+t');
if fid == -1
    messageErreurFichier(nomFicTide, 'WriteFailure');
    return
end
dateMat = datevec(TimeNav);
year    = dateMat(:,1);
ms      = dateMat(:,end)-floor(dateMat(:,end));
dateMat(:,1)     = dateMat(:,3);
dateMat(:,3)     = year;
dateMat(:,end)   = floor(dateMat(:,end));
dateMat(:,end+1) = floor(ms*1000);
dateMat          = cat(2, dateMat, HeightRTK');
dataOk = ~isnan(HeightRTK);
dateMat = dateMat(dataOk,:);
fprintf(fid, '%02d/%02d/%4d  %02d:%02d:%02d.%03d  %f  2\n', dateMat');
fclose(fid);

str1 = 'TODO';
str2 = sprintf('"%s" is over', nomFicTide);
my_warndlg(Lang(str1,str2), 1)
