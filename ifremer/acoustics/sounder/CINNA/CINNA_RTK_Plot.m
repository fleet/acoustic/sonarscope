function CINNA_RTK_Plot(FileNameList)

if ~iscell(FileNameList)
    FileNameList = {FileNameList};
end

%% Import de la mar�e dans les fichiers .all

fig = [];
Color = 'bgkm';
nbFic = length(FileNameList);
str1 = 'Visualisation des donn�es RTK des .all';
str2 = 'Plot RTK data of .all files';
hw = create_waitbar(Lang(str1,str2), 'N', nbFic);
for k=1:nbFic
    my_waitbar(k, nbFic, hw);
    fig = plot_DatagramHeight(FileNameList{k}, 'fig', fig, 'Color', Color(1 + mod(k-1,length(Color))));
end
my_close(hw, 'MsgEnd')


%{
nomFic = 'D:\Temp\Herve\EM2040-Thalia_05032013\0029_20130305_074519_ShipName.all';
aKM = cl_simrad_all('nomFic', nomFic);
fig = plot_DatagramHeight(aKM)
%}

function fig = plot_DatagramHeight(nomFic, varargin)

[varargin, fig]   = getPropertyValue(varargin, 'fig',   []);
[varargin, Color] = getPropertyValue(varargin, 'Color', []); %#ok<ASGLU>

[flag, NANAV] = lecFicNav_CINNA(nomFic); %, 'nomVar', nomVar
if ~flag
    str1 = sprintf('Fichier vide : %s\n', nomFic);
    str2 = sprintf('Empty file : %s\n', nomFic);
    my_warndlg(Lang(str1,str2), 0);
    return
end

Lat      = NANAV.Lat;
Lon      = NANAV.Lon;
Time     = NANAV.Time;
Heading  = NANAV.Heading;
FlagRTK  = NANAV.Flag;
Altitude = NANAV.Altitude;
Roll     = NANAV.Roll;
Pitch    = NANAV.Pitch;
% Heave    = NANAV.Heave;

[~, dist] = legs(Lat, Lon, 'gc');
Deltat = diff(Time.timeMat) * (24*3600);
dist = distdim(dist,'nm','m');
Speed = dist ./ Deltat';
Speed = [Speed(1); Speed];

FigUtils.createSScFigure('Name', nomFic);
N = 5;
h1(1) = subplot(N,2,1); PlotUtils.createSScPlot(Altitude, '-b'); grid on; title('Altitude (m)')
h1(2) = subplot(N,2,3); PlotUtils.createSScPlot(FlagRTK,  '*b'); grid on; title('FlagRTK')
h1(3) = subplot(N,2,5); PlotUtils.createSScPlot(Heading,  '-b'); grid on; title('Heading (deg)')
h1(4) = subplot(N,2,7); PlotUtils.createSScPlot(Roll,     '-b'); grid on; title('Roll (deg)')
h1(5) = subplot(N,2,9); PlotUtils.createSScPlot(Pitch,    '-b'); grid on; title('Pitch (deg)')
% h1(6) = subplot(N,2,2); PlotUtils.createSScPlot(Heave,    '-b'); grid on; title('Heave (m)')
h1(6) = subplot(N,2,2); PlotUtils.createSScPlot(Lat,      '-b'); grid on; title('Lat (deg)')
h1(7) = subplot(N,2,4); PlotUtils.createSScPlot(Lon,      '-b'); grid on; title('Lon (deg)')
h1(8) = subplot(N,2,6); PlotUtils.createSScPlot(Speed,    '-b'); grid on; title('Speed (m/s)')
if isfield(NANAV, 'FlagCleaned')
    subplot(N,2,1); hold on; PlotUtils.createSScPlot(NANAV.AltitudeCleaned, 'r');
    sub = find(NANAV.FlagCleaned ~= NANAV.Flag);
    subplot(N,2,3); hold on; PlotUtils.createSScPlot(sub, NANAV.FlagCleaned(sub), 'or');
end
linkaxes(h1, 'x'); axis tight; drawnow;
subplot(N,2,[8 10]); PlotUtils.createSScPlot(Lon, Lat, 'b'); grid on; axisGeo; title('Navigation');
if isfield(NANAV, 'FlagCleaned')
    sub = (NANAV.FlagCleaned == 1);
else
    sub = (NANAV.Flag == 1);
end
hold on; PlotUtils.createSScPlot(Lon(sub), Lat(sub), '.r');

if isempty(fig)
    fig = FigUtils.createSScFigure;
else
    FigUtils.createSScFigure(fig);
end
h2(1) = subplot(N,2,1); PlotUtils.createSScPlot(Time, Altitude,       ['-' Color]); grid on; hold on; title('Altitude/Time (deg)')
h2(2) = subplot(N,2,3); PlotUtils.createSScPlot(Time, uint8(FlagRTK), ['*' Color]); grid on; hold on; title('FlagRTK/Time')
h2(3) = subplot(N,2,5); PlotUtils.createSScPlot(Time, Heading,        ['-' Color]); grid on; hold on; title('FlagRTHeadingK/Time (deg)')
h2(4) = subplot(N,2,7); PlotUtils.createSScPlot(Time, Roll,           ['-' Color]); grid on; hold on; title('Roll/Time (deg)')
h2(5) = subplot(N,2,9); PlotUtils.createSScPlot(Time, Pitch,          ['-' Color]); grid on; hold on; title('Pitch/Time (deg)')
% h2(6) = subplot(N,2,2); PlotUtils.createSScPlot(Time, Heave,          ['-' Color]); grid on; hold on; title('Heave/Time (m)')
h2(6) = subplot(N,2,2); PlotUtils.createSScPlot(Time, Lat,            ['-' Color]); grid on; hold on; title('Lat/Time (deg)')
h2(7) = subplot(N,2,4); PlotUtils.createSScPlot(Time, Lon,            ['-' Color]); grid on; hold on; title('Lon/Time (deg)')
h2(8) = subplot(N,2,6); PlotUtils.createSScPlot(Time, Speed,          ['-' Color]); grid on; hold on; title('Speed/Time (m/s)')
if isfield(NANAV, 'FlagCleaned')
    subplot(N,2,1); hold on; PlotUtils.createSScPlot(Time, NANAV.AltitudeCleaned, 'r');
    subplot(N,2,3); hold on; PlotUtils.createSScPlot(Time, uint8(NANAV.FlagCleaned), 'or');
end
linkaxes(h2, 'x'); axis tight; drawnow;
subplot(N,2,[8 10]); PlotUtils.createSScPlot(Lon, Lat, ['-' Color]); grid on; axisGeo; title('Navigation');
if isfield(NANAV, 'FlagCleaned')
    sub = (NANAV.FlagCleaned == 1);
else
    sub = (NANAV.Flag == 1);
end
hold on; PlotUtils.createSScPlot(Lon(sub), Lat(sub), '.r');

% TODO : ajouter un addlisener pour pouvoir tracer la navigation de la zone
% zoom�e (supprimer Heave et tracer la nav sur [8 10]
