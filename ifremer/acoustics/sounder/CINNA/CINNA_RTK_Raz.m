function CINNA_RTK_Raz(ListFic)

if ~iscell(ListFic)
    ListFic = {ListFic};
end

%% Nettoyage des donn�es d'Altitude RTK dans les fichiers .all

nbFic = length(ListFic);
str1 = 'RAZ des fichiers .NA';
str2 = 'Reset .NA files';
hw = create_waitbar(Lang(str1,str2), 'N', nbFic);
for k=1:nbFic
    my_waitbar(k, nbFic, hw);
    
    %% Lecture de la hauteur RTK
    
    [flag, NANAV] = lecFicNav_CINNA(ListFic{k});
    if ~flag
        str1 = sprintf('Fichier vide : %s\n', ListFic{k});
        str2 = sprintf('Empty file : %s\n', ListFic{k});
        my_warndlg(Lang(str1,str2), 0);
        continue
    end
%     Lat      = NANAV.Lat;
%     Lon      = NANAV.Lon;
%     Time     = NANAV.Time;
%     Heading  = NANAV.Heading;
    flagOK = 0;
    if isfield(NANAV, 'FlagCleaned')
        NANAV = rmfield(NANAV, 'FlagCleaned');
        flagOK = 1;
    end
    if isfield(NANAV, 'AltitudeCleaned')
        NANAV = rmfield(NANAV, 'AltitudeCleaned'); 
        flagOK = 1;
    end
%     Roll     = NANAV.Roll;
%     Pitch    = NANAV.Pitch;
%     Heave    = NANAV.Heave;
           
    if flagOK
        [nomDir, nomFic] = fileparts(ListFic{k});
        nomFicMat = fullfile(nomDir, [nomFic '.mat']);
        FormatVersion = loadmat(nomFicMat, 'nomVar', 'FormatVersion'); 
        save(nomFicMat, 'NANAV', 'FormatVersion')
    end
end
my_close(hw, 'MsgEnd')
