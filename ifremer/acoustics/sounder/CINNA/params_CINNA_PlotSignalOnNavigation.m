function [flag, SignalName] = params_CINNA_PlotSignalOnNavigation()

SignalName = {'Nav Heading'; 'Nav Speed'; 'Heading'; 'Altitude'; 'AltitudeCleaned'; 'FlagRTK'; 'FlagRTKCleaned'; 'MeanRoll'; 'StdRoll'; 'MeanPitch'; 'StdPitch'; 'MeanHeave'; 'StdHeave'};
[flag, sub] = question_SelectASignal(SignalName);
if ~flag
    return
end
SignalName = SignalName{sub};
