function [flag, listFileNames, nomFicTide, repImport, repExport] = params_CINNA_RTK_CreateTide(repImport, repExport)

nomFicTide = [];

%% List of .NA files

% [flag, listFileNames, repImport] = survey_uiSelectFiles(this, 'ExtensionFiles', {'.NA'; '.VLIZ'}, 'RepDefaut', repImport);
[flag, listFileNames, repImport] = uiSelectFiles('ExtensionFiles', {'.NA'; '.VLIZ'}, 'AllFilters', 1, 'RepDefaut', repImport);
if ~flag
    return
end

%% Name of the tide file

str1 = 'Nom du fichier de mar�e';
str2 = 'Tide file';
[flag, nomFicTide] = my_uiputfile({'*.txt', 'List file (*.txt)'; '*.*', 'All Files (*.*)'}, ...
    Lang(str1,str2), fullfile(repExport, 'TideFile.txt'));
if ~flag
    return
end

repExport = fileparts(nomFicTide);
