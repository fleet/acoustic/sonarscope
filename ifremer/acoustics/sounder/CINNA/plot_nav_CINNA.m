function Fig = plot_nav_CINNA(nomFic, varargin)

[varargin, Fig]       = getPropertyValue(varargin, 'Fig',    []);
[varargin, Name]      = getPropertyValue(varargin, 'Name',   []);
[varargin, nomSignal] = getPropertyValue(varargin, 'Signal', []); %#ok<ASGLU>

if isempty(nomFic)
    return
end

if ~iscell(nomFic)
    nomFic = {nomFic};
end

if isempty(Name)
    Name = nomFic{1};
end

Fig = plot_navigation_Figure(Fig, 'Name', Name);
XLim = [Inf -Inf];
YLim = [Inf -Inf];
ZLim = [Inf -Inf];
ConstanteDeTemps = 1*60 ; % 1 mn

%% Traitement des fichiers

NbFic = length(nomFic);
str1 = 'Trac� de la navigation des .NA en cours.';
str2 = 'Plot navigation from the .NA files';
hw = create_waitbar(Lang(str1,str2), 'N', NbFic);
for k=1:NbFic
    my_waitbar(k, NbFic, hw);
    
    [flag, NASY2] = lecFicNav_CINNA(nomFic{k});
    if ~flag
        str1 = sprintf('Fichier vide : %s\n', nomFic{k});
        str2 = sprintf('Empty file : %s\n', nomFic{k});
        my_warndlg(Lang(str1,str2), 0);
        continue
    end
    Lat      = NASY2.Lat;
    Lon      = NASY2.Lon;
    Time     = NASY2.Time;
    Heading  = NASY2.Heading;
    FlagRTK  = NASY2.Flag;
    Altitude = NASY2.Altitude;
    Roll     = NASY2.Roll;
    Pitch    = NASY2.Pitch;
    Heave    = NASY2.Heave;
    
    plot_navigation(nomFic{k}, Fig, Lon, Lat, NASY2.Time.timeMat, []);
    %     FigUtils.createSScFigure; PlotUtils.createSScPlot(NASY2.Altitude); grid
    %     sub1 = find(NASY2.Flag);
    %     sub2 = find(~NASY2.Flag);
    %     FigUtils.createSScFigure; PlotUtils.createSScPlot(sub1, NASY2.Altitude(sub1), '.b'); grid on; hold on;  PlotUtils.createSScPlot(sub2, NASY2.Altitude(sub2), '.r');
    
    if ~isempty(nomSignal)
        switch nomSignal
            case 'Nav Heading' % OK
                [Heading, ~] = legs(Lat, Lon, 'gc'); % Attention : laisser le ~ici car sinon Heading est une matrice
                Signal = Heading;
                Unit = 'deg';
                
            case 'Nav Speed' % OK
                [~, dist] = legs(Lat, Lon, 'gc');
                Deltat = diff(Time.timeMat) * (24*3600);
                dist = distdim(dist,'nm','m');
                Speed = dist ./ Deltat';
                %                 FigUtils.createSScFigure; PlotUtils.createSScPlot(Speed); grid;
                %                 nomSignal = 'Speed';
                Signal = Speed;
                Unit = 'm/s';
                
            case 'Heading' % OK
                Signal = Heading;
                Unit = 'deg';
                
            case 'Altitude' % OK
                Signal = Altitude;
                Unit = 'm';
                
            case 'AltitudeCleaned' % OK
                if isfield(NASY2, 'AltitudeCleaned')
                    Signal = NASY2.AltitudeCleaned;
                    Unit = 'm';
                else
                    continue
                end
                
            case 'FlagRTK' % OK
                Signal = FlagRTK;
                Unit = ' ';
                
            case 'FlagRTKCleaned' % OK
                if isfield(NASY2, 'FlagCleaned')
                    Signal = NASY2.FlagCleaned;
                    Unit = ' ';
                else
                    continue
                end
                
            case 'MeanRoll' % OK
                Signal = filtreAttitude(Time, Roll, ConstanteDeTemps);
                Unit = 'deg';
                
            case 'StdRoll' % OK
                [~, Signal] = filtreAttitude(Time, Roll, ConstanteDeTemps);
                Unit = 'deg';
                
            case 'MeanPitch' % OK
                Signal = filtreAttitude(Time, Pitch, ConstanteDeTemps);
                Unit = 'deg';
                
            case 'StdPitch' % OK
                [~, Signal] = filtreAttitude(Time, Pitch, ConstanteDeTemps);
                Unit = 'deg';
                
            case 'MeanHeave' % OK
                Signal = filtreAttitude(Time, Heave, ConstanteDeTemps);
                Unit = 'm';
                
            case 'StdHeave' % OK
                [~, Signal] = filtreAttitude(Time, Heave, ConstanteDeTemps);
                Unit = 'm';
                
            otherwise
                messageForDebugInspection
        end
        
        switch nomSignal
            case {'Heading'; 'Nav Heading'}
                ZLim = [0 360];
            case 'Type' % 'PFlag'
                % ZLim = [0 2];
                ZLim = [0 2]; % Quelle valeur pour Type ???
            otherwise
                ZLim(1) = min(ZLim(1), double(min(Signal, [], 'omitnan')));
                ZLim(2) = max(ZLim(2), double(max(Signal, [], 'omitnan')));
        end
        XLim(1) = min(XLim(1), min(Lon, [], 'omitnan'));
        XLim(2) = max(XLim(2), max(Lon, [], 'omitnan'));
        YLim(1) = min(YLim(1), min(Lat, [], 'omitnan'));
        YLim(2) = max(YLim(2), max(Lat, [], 'omitnan'));
    end
end
my_close(hw, 'MsgEnd');


if isempty(nomSignal)
    return
end

%% Saisie des bornes de la table de couleurs

[flag, ZLim] = saisie_CLim(min(ZLim), max(ZLim), Unit, 'SignalName', nomSignal);
if ~flag
    return
end

%% Trac� du signal

if isinf(ZLim(1))
    return
end

nbColors = 64;
Colors = jet(nbColors);

figure(Fig);
W = axis;
x = linspace(XLim(1), XLim(1)+100*nbColors*eps(XLim(1)), nbColors);
y = linspace(YLim(1), YLim(2), nbColors);
h = imagesc(x, y, linspace(ZLim(1), ZLim(2), nbColors)); colormap(Colors), colorbar %#ok<NASGU>
% delete(h)
hold on;
axis(W)

str1 = 'Trac� du signal sur la navigation des .NA';
str2 = 'Plot signal onto the navigation of the .NA files';
hw = create_waitbar(Lang(str1,str2), 'N', NbFic);
for k=1:NbFic
    my_waitbar(k, NbFic, hw);
    
    [flag, NASY2] = lecFicNav_CINNA(nomFic{k});
    if ~flag
        continue
    end
    Lat      = NASY2.Lat;
    Lon      = NASY2.Lon;
    Time     = NASY2.Time;
    Heading  = NASY2.Heading;
    FlagRTK  = NASY2.Flag;
    Altitude = NASY2.Altitude;
    Roll     = NASY2.Roll;
    Pitch    = NASY2.Pitch;
    Heave    = NASY2.Heave;
    switch nomSignal
        case 'Nav Heading' % OK
            [Heading, ~] = legs(Lat, Lon, 'gc'); % Attention : laisser le ~ici car sinon Heading est une matrice
            nomSignal = 'Heading';
            Signal = [Heading(1); Heading];
            Unit = 'deg';
            
        case 'Nav Speed' % OK
            [~, dist] = legs(Lat, Lon, 'gc');
            Deltat = diff(Time.timeMat) * (24*3600);
            dist = distdim(dist,'nm','m');
            Speed = dist ./ Deltat';
            %                 FigUtils.createSScFigure; PlotUtils.createSScPlot(Speed); grid;
            nomSignal = 'Speed';
            Signal = [Speed(1); Speed];
            Unit = 'm/s';
            
        case 'Heading' % OK
            Signal = Heading;
            Unit = 'deg';
            
        case 'Altitude' % OK
            Signal = Altitude;
            Unit = 'm';
            
        case 'AltitudeCleaned' % OK
            if isfield(NASY2, 'AltitudeCleaned')
                Signal = NASY2.AltitudeCleaned;
                Unit = 'm';
            else
                continue
            end
            
        case 'FlagRTK' % OK
            Signal = FlagRTK;
            Unit = ' ';
            
        case 'FlagRTKCleaned' % OK
            if isfield(NASY2, 'FlagCleaned')
                Signal = NASY2.FlagCleaned;
                Unit = ' ';
            else
                continue
            end
            
        case 'MeanRoll' % OK
            Signal = filtreAttitude(Time, Roll, ConstanteDeTemps);
            Unit = 'deg';
            
        case 'StdRoll' % OK
            [~, Signal] = filtreAttitude(Time, Roll, ConstanteDeTemps);
            Unit = 'deg';
            
        case 'MeanPitch' % OK
            Signal = filtreAttitude(Time, Pitch, ConstanteDeTemps);
            Unit = 'deg';
            
        case 'StdPitch' % OK
            [~, Signal] = filtreAttitude(Time, Pitch, ConstanteDeTemps);
            Unit = 'deg';
            
        case 'MeanHeave' % OK
            Signal = filtreAttitude(Time, Heave, ConstanteDeTemps);
            Unit = 'm';
            
        case 'StdHeave' % OK
            [~, Signal] = filtreAttitude(Time, Heave, ConstanteDeTemps);
            Unit = 'm';
            
        otherwise
            pi
    end
    
    if ZLim(2) == ZLim(1)
        ZLim(1) = ZLim(1) - 0.5;
        ZLim(2) = ZLim(2) + 0.5;
    end
    coef = (nbColors-1) / (ZLim(2) - ZLim(1));
    Value = ceil((Signal - ZLim(1)) * coef);
    
    figure(Fig)
    hold on;
    
    [~, nomFichier] = fileparts(nomFic{k});
    for k2=1:nbColors
        sub = find(Value == (k2-1));
        if ~isempty(sub)
            h = plot(Lon(sub), Lat(sub), '+');
            set(h, 'Color', Colors(k2,:), 'Tag', nomFichier)
            set_HitTest_Off(h)
        end
    end
    
    sub = find(Value < 0);
    if ~isempty(sub)
        h = plot(Lon(sub), Lat(sub), '+');
        set(h, 'Color', Colors(1,:)*0.8, 'Tag', nomFichier)
        set_HitTest_Off(h)
    end
    
    sub = find(Value > (nbColors-1));
    if ~isempty(sub)
        h = plot(Lon(sub), Lat(sub), '+');
        set(h, 'Color', Colors(nbColors,:)*0.8, 'Tag', nomFichier)
    end
end
my_close(hw, 'MsgEnd')

if isempty(Unit)
    FigName = sprintf('Navigation - %s', nomSignal);
else
    FigName = sprintf('Navigation - %s (%s)', nomSignal, Unit);
end
title(FigName)

if isempty(Unit)
    FigName = sprintf('Caraibes .nvi plot - %s', nomSignal);
else
    FigName = sprintf('Caraibes .nvi plot - %s (%s)', nomSignal, Unit);
end
figure(Fig);
set(Fig, 'name', FigName)

