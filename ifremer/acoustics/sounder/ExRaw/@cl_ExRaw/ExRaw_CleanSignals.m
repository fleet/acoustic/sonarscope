function ExRaw_CleanSignals(this, listFileNames, varargin) %#ok<INUSL>

[varargin, identFreq] = getPropertyValue(varargin, 'identFreq', 1);
[varargin, identData] = getPropertyValue(varargin, 'identData', 1); %#ok<ASGLU>

if ~iscell(listFileNames)
    listFileNames = {listFileNames};
end

%% Traitement des fichiers

Carto = [];
Video = 1;
I0    = cl_image_I0;
N = length(listFileNames);
hw = create_waitbar('ExRaw Heiht detection', 'N', N);
for k=1:N
    my_waitbar(k, N, hw);
    
    str1 = sprintf('Traitement du fichier %d/%d : "%s" en cours.', k, N, listFileNames{k});
    str2 = sprintf('Processing file %d/%d : "%s".', k, N, listFileNames{k});
    fprintf(1,'%s\n', Lang(str1,str2));
    
    %% V�rification de l'existence du fichier Status
    
    [nomDir, listFileNamesSeul, ext] = fileparts(listFileNames{k});
    listFileNamesXmlStatus = fullfile(nomDir, [listFileNamesSeul '_Status.xml']);
    if exist(listFileNamesXmlStatus, 'file')
        str1 = sprintf('Il semble que le fichier "%s" a d�j� �t� trait�. Que voulez-vous faire ?', [listFileNamesSeul ext]);
        str2 = sprintf('It seems file "%s" has been already processed. What do you want to do ?',  [listFileNamesSeul ext]);
        str3 = 'Passer au fichier suivant';
        str4 = 'Skip it';
        str5 = 'Le retraiter';
        str6 = 'Reprocess it';
        [rep, flag] = my_questdlg(Lang(str1,str2), Lang(str3,str4), Lang(str5,str6));
        if ~flag
            my_close(hw)
            return
        end
        if rep == 1
            continue
        end
        Info = xml_read(listFileNamesXmlStatus);
        CLim  = Info.Clim_Apres;
        Video = Info.Video;
    else
        CLim = [];
    end
    
    %% Lecture du fichier
    
    a = cl_ExRaw('listFileNames', listFileNames{k});
    
    %% Importation de l'image
    
    [flag, b, Carto] = import_ExRaw(I0, listFileNames{k}, 'Carto', Carto, 'listFrequencies', identFreq, 'listDataType', identData);
    if ~flag
        my_close(hw)
        return
    end
    
    b.Video = Video;
    if ~isempty(CLim)
        b.CLim = CLim;
    end
    
    %% Pr�-d�tection de la hauteur
    
    Height = get(b, 'SonarHeight');
    if isempty(Height) || all(Height == 0)
        str1 = 'Aucune hauteur n''a �t� trouv�e, je d�clenche d''autorit� un calcul de la hauteur.';
        str2 = 'No height was found, I am lauching the height detection prior to the image edit.';
        my_warndlg(Lang(str1,str2), 0, 'Tag', 'ControleSignal', 'TimeDelay', 60);
        b = sonar_lateral_hauteur(b);
%         [flag, b] = subbottom_compute_height(b, 'TypeSignal', 1, 'ComputingUnit', 'Amp');
    end
    
    %% Affichage de l'image dans SonarScope pour v�rifier la hauteur
    
    if k == 1
    	messageCheckHeight();
    end
    Clim_Avant = b.CLim;
    b = SonarScope(b, 'visuProfilY', 1, 'profilY.Type', 'Height', 'visuProfilX', 1, 'ButtonSave', 1);
    Clim_Apres = b.CLim;
    
    %% V�rification si le contraste a bien �t� r�gl�
    
    if isequal(Clim_Avant, Clim_Apres)
        str1 = 'Il semblerait que vous ayez oubli� de r�gler le contraste, voulez-vous sonarscopier l''image de nouveau ?';
        str2 = 'It seems you have forgotten to adjust the contrast, do you want to edit the image again ?';
        [rep, flag] = my_questdlg(Lang(str1,str2));
        if ~flag
            my_close(hw)
            return
        end
        if rep == 1
            b = SonarScope(b, 'visuProfilY', 1, 'profilY.Type', 'Height', 'visuProfilX', 1, 'ButtonSave', 1);
            Clim_Apres = b.CLim;
            if isequal(Clim_Avant, Clim_Apres)
                str1 = 'Y''a de la fatigue dans l''air on dirait, tant pis pour le contraste.';
                str2 = 'It seems you are tired, let us drop the contrast for the moment.';
                my_warndlg(Lang(str1,str2), 0, 'Tag', 'ControleSignal', 'TimeDelay', 60);
            end
        end
    end
    Video = b.Video;
    
    %% Sauvegarde de la hauteur
    
    Height = get(b(end), 'SonarHeight');
%     fe_Hz = get(b, 'SonarSampleFrequency');
%     Height_seconds = abs(Height) ./ fe_Hz;
    Height_seconds = abs(Height);
    flag = save_signal(a, 'Height', Height_seconds(:,1), 'nomDirBin', 'Ssc_Height');
    if ~flag
        flag = save_Height(a, Height_seconds(:,1));
        if ~flag
            my_close(hw)
            return
        end
    end
    
    %% Sauvegarde de CLim
    
    CLim = b(end).CLim;
    flag = save_ProcessingParameters(a, 'Ssc_ProcessingParameters.xml', 'CLim', CLim);
    if ~flag
        my_close(hw)
        return
    end
    
    %% Cr�ation du fichier de status du fichier
    
    Info.Clim_Apres = Clim_Apres;
    Info.Video      = Video;
    Info.Height     = 1;
    xml_write(listFileNamesXmlStatus, Info);
    flag = exist(listFileNamesXmlStatus, 'file');
    if ~flag
        messageErreur(listFileNamesXmlStatus)
    end
    
    %% On demande si on veut traiter les autres fichiers
    
    if k ~= N
        flag = question_ContinueNextFile();
        if ~flag
            break
        end
    end
    
end
my_close(hw, 'MsgEnd');
