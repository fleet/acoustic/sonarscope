function ExRaw_ExtractSeabedBS(~, nomFicRaw, nomFicSvp)

if ~iscell(nomFicRaw)
    nomFicRaw = {nomFicRaw};
end

%% Lecture des profils de c�l�rite

nbFic = length(nomFicSvp);
for k=1:nbFic
    fid = fopen(nomFicSvp{k}, 'r');
    line = fgetl(fid);
    mots = strsplit(line);
    x = sscanf(mots{5}, '%4d%2d%2d%2d%2d');
    fclose(fid);
    
    date(k) = dayJma2Ifr(x(3), x(2), x(1)); %#ok<AGROW>
    heure(k) = hourHms2Ifr(x(4), x(5), 0); %#ok<AGROW>
end
tSvp = cl_time('timeIfr', date, heure);
tSvp = tSvp.timeMat;

%% Traitement des fichiers .all

nbFic = length(nomFicRaw);
str1 = 'Extraction du BS de la surface des .raw.';
str2 = 'Extract the seabed Backscatter from .raw files.';
hw = create_waitbar(Lang(str1,str2), 'N', nbFic);
for k=1:nbFic
    my_waitbar(k, nbFic, hw);
    [flag, tRaw] = SScCacheXMLBinUtils.getSignalUnit(nomFicRaw{k}, 'Ssc_Position', 'Time');
    if ~flag
        continue
    end
    tRaw = tRaw.timeMat;
	[~, ind] = min(abs(tSvp - tRaw(1)));
    
    figure;
    plot(tSvp, '*-'); grid on; hold on;
    plot([1 length(tSvp)], [tRaw(1) tRaw(1)]);
    plot([1 length(tSvp)], [tRaw(end) tRaw(end)]);
    plot(ind , tRaw(ind), 'ok'); title('Time SVP files & Time ') 
    
    [PathToFile, Filename, ExtRaw]    = fileparts(nomFicRaw{k});
    [PathToSVP, Filename_SVP, ExtSVP] = fileparts(nomFicSvp{ind});
%     [PathToAtt, Filename_Att, ExtATT] = fileparts(nomFicSvp{ind});
    
    PathToATT    = '';
    Filename_Att = '';
    ExtATT       = '';
    
    Frequencies = 38000;
    
    % {
    bs_analysis_EK60_standalone(PathToFile, [Filename ExtRaw], ...
        'PathToSVP', PathToSVP, 'Filename_SVP', [Filename_SVP ExtSVP], ...
        'PathToAtt', PathToATT, 'Filename_Att', [Filename_Att ExtATT], ...
        'PathToMemmap', my_tempdir, ...
        'Frequencies', Frequencies);
    % }
end
my_close(hw, 'MsgEnd')


%{
addParameter(p, 'Calibration',[]); %Calibration is a struct containing 3 fields:F,G0,SACORRECT.
%}
