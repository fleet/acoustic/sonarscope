function ExRaw_TideImport(~, ListFic, NomFicTide)

if ~iscell(ListFic)
    ListFic = {ListFic};
end

%% Lecture de la mar�e

[flag, Tide] = read_Tide(NomFicTide);
if ~flag
    return
end

%% Import de la mar�e dans les fichiers .all

nbFic = length(ListFic);
str1 = 'Importation de la mar�e dans les .raw.';
str2 = 'Tide Import in the .raw files.';
hw = create_waitbar(Lang(str1,str2), 'N', nbFic);
for k=1:nbFic
    my_waitbar(k, nbFic, hw);
    a = cl_ExRaw('nomFic', ListFic{k});
    
    [flag, Data] = SSc_ReadDatagrams(ListFic{k}, 'Ssc_Position');
    if ~flag
        break
    end
    
    X = my_interp1(Tide.Time, Tide.Value, Data.Time);
    X(isnan(X)) = 0;
    
    flag = save_signal(a.nomFic, 'Ssc_Position', 'Tide', X);
    if ~flag
        break
    end
end
my_close(hw, 'MsgEnd')
