function flag = RAW_EditNav(~, listFileNames)

if ~iscell(listFileNames)
    listFileNames = {listFileNames};
end

N = length(listFileNames);
if N == 0
    flag = 0;
    return
end

str1 = 'Edition de la navigation.';
str2 = 'Edit the navigation';
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    
    [flag, DataPosition] = SSc_ReadDatagrams(listFileNames{k}, 'Ssc_Position');
    if ~flag
        % TODO : message
        continue
    end
    
    Longitude = DataPosition.Longitude;
    Latitude  = DataPosition.Latitude;
    Heading   = DataPosition.Heading;
    if all(Heading == 0)
        Heading = calCapFromLatLon(Latitude, Longitude);
    end
    Immersion = DataPosition.Immersion;
    
    T = datetime(DataPosition.Time.timeMat, 'ConvertFrom', 'datenum');
    timeSample = XSample('name', 'time', 'data', T);
    
    latSample = YSample('data', Latitude, 'marker', '.');
    latSignal = ClSignal('ySample', latSample, 'xSample', timeSample);
    
    lonSample = YSample('data', Longitude, 'marker', '.');
    lonSignal = ClSignal('ySample', lonSample, 'xSample', timeSample);
    
    headingVesselSample = YSample('name', 'Heading', 'unit', 'deg', 'data', Heading, 'marker', '.');
    headingVesselSignal = ClSignal('ySample', headingVesselSample, 'xSample', timeSample);
    
    immersionSample = YSample('name', 'Immersion', 'unit', 'm', 'data', Immersion, 'marker', '.');
    immersionSignal     = ClSignal('ySample', immersionSample,     'xSample', timeSample);
    
    [~, name] = fileparts(listFileNames{k});
    nav(k) = ClNavigation(latSignal, lonSignal, ...
        'immersionSignal',     immersionSignal, ...
        'headingVesselSignal', headingVesselSignal, ...
        'name', name); %#ok<AGROW>
end

%% Create the GUI

a = NavigationDialog(nav, 'Title', 'Navigation', 'CleanInterpolation', true);
% a.editProperties();
pause(0.5)
a.openDialog();
if ~a.okPressedOut
    return
end

%% Save the results

firstTime = 1;
for k=1:N
    a = cl_ExRaw('nomFic', listFileNames{k});
    [flag, DataPosition] = SSc_ReadDatagrams(listFileNames{k}, 'Ssc_Position');
    if ~flag
        % TODO : message
        continue
    end
    
    Latitude  = DataPosition.Latitude;
    Longitude = DataPosition.Longitude;
    Heading   = DataPosition.Heading;
    if all(Heading == 0)
        Heading = calCapFromLatLon(Latitude, Longitude);
    end
    Immersion = DataPosition.Immersion;
%     T = datetime(DataPosition.Time.timeMat, 'ConvertFrom', 'datenum');
    
    LatitudeNew  = nav(k).getLatitudeSignalSample().data;
    LongitudeNew = nav(k).getLongitudeSignalSample().data;
    ImmersionNew = nav(k).getImmersionSignalSample().data;
    HeadingNew   = nav(k).getHeadingVesselSignalSample().data;
%     TNew         = nav(k).getTimeSignalSample().data;
    
    flagModified = 0;
    if ~isequal(LatitudeNew, Latitude)
        DataPosition.Latitude = LatitudeNew;
        flagModified = 1;
    end
    if ~isequal(LongitudeNew, Longitude)
        DataPosition.Longitude = LongitudeNew;
        flagModified = 1;
    end
    if ~isequal(ImmersionNew, Immersion)
        DataPosition.Immersion = ImmersionNew;
        flagModified = 1;
    end
    if ~isequal(HeadingNew, Heading)
        DataPosition.Heading = HeadingNew;
        flagModified = 1;
    end
    %         if ~isequal(TNew, T)
    %             DataPosition.xxx = TNew;
    %         end
    
    %% Sauvegarde des signaux dans le r�pertoire cache
    
    if flagModified
        if firstTime
            str1 = 'Voulez-vous sauvegarder les modifications dans le cache de SonarScope ?';
            str2 = 'Do you want to save the modifications in the SonarScope cache directory ?';
            [rep, flag] = my_questdlg(Lang(str1,str2));
            if ~flag || (rep == 2)
                return
            end
            firstTime = 0;
        end
        
        [~, listFileNamesSeul, ext] = fileparts(listFileNames{k});
        
        flag = save_signal(a, 'Latitude', DataPosition.Latitude);
        if ~flag
            str1 = sprintf('Il est impossible de sauver "Latitude" dans le r�pertoire cache associ� � "%s".', [listFileNamesSeul ext]);
            str2 = sprintf('It is impossible to save "Latitude" in the cache associated to "%s".', [listFileNamesSeul ext]);
            my_warndlg(Lang(str1,str2), 1);
        end
        
        flag = save_signal(a, 'Longitude', DataPosition.Longitude);
        if ~flag
            str1 = sprintf('Il est impossible de sauver "Longitude" dans le r�pertoire cache associ� � "%s."', [listFileNamesSeul ext]);
            str2 = sprintf('It is impossible to save "Longitude" in the cache associated to "%s".', [listFileNamesSeul ext]);
            my_warndlg(Lang(str1,str2), 1);
        end
        
        flag = save_signal(a, 'Heading', DataPosition.Heading);
        if ~flag
            str1 = sprintf('Il est impossible de sauver "Heading" dans le r�pertoire cache associ� � "%s."', [listFileNamesSeul ext]);
            str2 = sprintf('It is impossible to save "Heading" in the cache associated to "%s".', [listFileNamesSeul ext]);
            my_warndlg(Lang(str1,str2), 1);
        end
        
        flag = save_signal(a, 'Immersion', DataPosition.Immersion);
        if ~flag
            str1 = sprintf('Il est impossible de sauver "Immersion" dans le r�pertoire cache associ� � "%s."', [listFileNamesSeul ext]);
            str2 = sprintf('It is impossible to save "Immersion" in the cache associated to "%s".', [listFileNamesSeul ext]);
            my_warndlg(Lang(str1,str2), 1);
        end
    end
end
