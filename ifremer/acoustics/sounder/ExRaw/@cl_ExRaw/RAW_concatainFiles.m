% listFilenames = {'E:\MAYOBS15\RAW_EK80\MD228_MAYOBS15-D20201006-T082431.raw'};
% listFilenames{end+1} = 'E:\MAYOBS15\RAW_EK80\MD228_MAYOBS15-D20201006-T085316.raw';
% R0 = cl_ExRaw([]);
% RAW_concatainFiles(R0, listFilenames)

function RAW_concatainFiles(~, listFilenames)

if ~iscell(listFilenames)
    listFilenames = {listFilenames};
end

N = length(listFilenames);
if N < 2
    fprintf('There is less than 2 listFilenames !!!');
    return
end

str1 = 'Lecture des fichiers .raw';
str2 = 'Reading .raw files';
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:length(N)
    my_waitbar(k, N, hw)
    
    % Cr�ation de l'instance juste pour �tre s�r que le d�codage aura �t�
    % fait
    
    cl_ExRaw('nomFic', listFilenames{k});
    
    [pathname, filename] = fileparts(listFilenames{k});
    nomDir = fullfile(pathname, 'SonarScope', filename);
    if ~exist(nomDir, 'dir')
        fprintf('The directory "%s" does not exist.', nomDir);
        continue
    end
    
    %% Ssc_ConfigHeader
    
    xmlFileName = fullfile(nomDir, 'Ssc_ConfigHeader.xml');
    [flag, XMLConfigHeader] = XMLBinUtils.readGrpData(xmlFileName, 'XMLOnly', 1);
    if ~flag
        continue
    end
    [flag, DataConfigHeader, DataBinConfigHeader] = XMLBinUtils.readGrpData(xmlFileName);
    if ~flag
        continue
    end
 
    %% Ssc_ConfigTransducer
    
    xmlFileName = fullfile(nomDir, 'Ssc_ConfigTransducer.xml');
    [flag, XMLConfigTransducer] = XMLBinUtils.readGrpData(xmlFileName, 'XMLOnly', 1);
    if ~flag
        continue
    end
    [flag, DataConfigTransducer, DataBinConfigTransducer] = XMLBinUtils.readGrpData(xmlFileName);
    if ~flag
        continue
    end
 
    %% Ssc_Sample
    
    xmlFileName = fullfile(nomDir, 'Ssc_Sample.xml');
    [flag, XMLSample] = XMLBinUtils.readGrpData(xmlFileName, 'XMLOnly', 1);
    if ~flag
        continue
    end
    [flag, DataSample, DataBinSample] = XMLBinUtils.readGrpData(xmlFileName);
     if ~flag
        continue
     end
    
     if k == 1
         DataBinSampleNew = DataBinSample;
     else
         DataBinSampleNew = concatainStructureData(DataBinSampleNew, DataBinSample);
     end
   
    %% Ssc_Position
    
    xmlFileName = fullfile(nomDir, 'Ssc_Position.xml');
    [flag, XMLPosition] = XMLBinUtils.readGrpData(xmlFileName, 'XMLOnly', 1);
    if ~flag
        continue
    end
    [flag, DataPosition, DataBinPosition] = XMLBinUtils.readGrpData(xmlFileName);
    if ~flag
        continue
    end
    
     if k == 1
         DataBinPositionNew = DataBinPosition;
     else
         DataBinPositionNew = concatainStructureData(DataBinPositionNew, DataBinPosition);
     end
     
    %% Ssc_Height
    
    xmlFileName = fullfile(nomDir, 'Ssc_Height.xml');
    [flag, XMLHeight] = XMLBinUtils.readGrpData(xmlFileName, 'XMLOnly', 1);
    if ~flag
        continue
    end
    [flag, DataHeight, DataBinHeight] = XMLBinUtils.readGrpData(xmlFileName);
    if ~flag
        continue
    end
    
     if k == 1
         DataBinHeightNew = DataBinHeight;
     else
         DataBinHeightNew = concatainStructureData(DataBinHeightNew, DataBinHeight);
     end
end
my_close(hw, 'MsgEnd')

%% Cr�ation du fichier de sortie

% XMLSample.Dimensions.nbPings = size(DataBinSampleNew.NbSamplesByChannel, 1)
% 
% ans = 
% 
%   struct with fields:
% 
%        nbPings: 227
%     nbChannels: 5
%      nbSamples: 20502

     
% XMLSample
% DataSample
% DataBinSampleNew


function DataNew = concatainStructureData(DataNew, Data)

FieldNames = fieldnames(DataNew);
for k=1:length(FieldNames)
    FN = FieldNames{k};
    DataField = Data.(FN);
    if size(DataField,2) ~= 1
        DataNew.(FN) = [DataNew.(FN); DataField];
    end
end
