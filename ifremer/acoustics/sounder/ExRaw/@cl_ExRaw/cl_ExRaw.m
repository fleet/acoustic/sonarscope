% Constructeur de cl_ExRaw (Imagerie contenue dans une .raw)
%
% Syntax
%   a = cl_ExRaw(...)
% 
% Name-Value Pair Arguments
%   nomFic : Nom(s) du fichier .raw
%  
% Output Arguments 
%   a : Instance de cl_ExRaw
%
% Examples
%   nomFic = getNomFicDatabase('FK-D20120510-T202356.raw');
%   a = cl_ExRaw('nomFic', nomFic)
%
% See also CONVERT_HAC.exe Authors
% Authors : GLU
% ----------------------------------------------------------------------------

function this = cl_ExRaw(varargin)

%% D�finition de la structure

this.nomFic         = '';
this.strIdentData   = {'Amp'; 'PhaseAcross'; 'PhaseAlong'};
this.NavigationFile = ''; % Nom du fichier Annexe de donn�e de navigation.


%% Creation de l'instance

this = class(this, 'cl_ExRaw');

%% On regarde si l'objet a �t� cr�e uniquement pour atteindre une m�thode de la classe

if (nargin == 1) && isempty(varargin{1})
    return
end

%% Initialisation des valeurs transmises lors de l'appel au constructeur

this = set(this, varargin{:});
