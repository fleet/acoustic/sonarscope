function cleanDirectories(~, nomFic)

[nomDir, nomFic] = fileparts(nomFic);
nomDirRacine    = fullfile(nomDir, 'SonarScope', nomFic);

pppp = fullfile(nomDirRacine, 'ExRaw_*.xml');
delete(pppp)

try
    pppp = fullfile(nomDirRacine, 'ExRaw_*');
    rmdir(pppp, 's')
catch ME
    ME;
end
