% Representation externe d'une instance sur le terminal
%
% Syntax
%   display(this)
%
% Input Arguments 
%   this : une instance de cl_ExRaw
%
% Examples
%   nomFic = getNomFicDatabase('PELGAS10_003_20100428_183857.raw')
%   a = cl_ExRaw('nomFic', nomFic);
%   display(a)
%   a
%
% See also cl_ExRaw cl_ExRaw/char Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function display(this)

s = whos('this');
sz = s.size;

fprintf('Name\tSize\tClass\n');
fprintf('%s\t%s\t%s\n', inputname(1), num2strCode(sz), s.class);

disp(char(this));
