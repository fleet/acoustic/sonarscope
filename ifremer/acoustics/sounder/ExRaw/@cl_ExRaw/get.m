function varargout = get(this, varargin)

%% Boucle de parcours des arguments

varargout = {};
for i=1:length(varargin)
    switch varargin{i}
        case 'nomFic'
            varargout{end+1} = this.nomFic; %#ok<AGROW>
            
        otherwise
            w = sprintf('cl_ExRaw/get %s non pris en compte', varargin{i});
            my_warndlg(w, 0);
    end
end
