function  [flag, identFreq] = params_ExRaw_CleanSignals(~, nomFic)

identFreq = [];

%% Recherche des fr�quences disponibles dans le premier fichier

[flag, Frequency] = SScCacheXMLBinUtils.getSignalUnit(nomFic, 'Ssc_Sample', 'AcousticFrequency');
if ~flag
    return
end

%% S�lection des fr�quences

for k=1:length(Frequency)
    str{k} = num2str(Frequency(k)); %#ok<AGROW>
end
str1 = 'S�lectionnez la fr�quence � utiliser pour faire la d�tection de hauteur.';
str2 = 'Select the frequency to use for the heiht detection.';
[identFreq, flag] = my_listdlg(Lang(str1,str2), str, 'InitialValue', 1, 'SelectionMode', 'Single');
if ~flag
    return
end
