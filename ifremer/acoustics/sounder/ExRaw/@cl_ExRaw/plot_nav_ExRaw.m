function [flag, Fig, Carto] = plot_nav_ExRaw(~, nomFic, varargin)

[varargin, Carto] = getPropertyValue(varargin, 'Carto', []);
[varargin, Fig]  = getPropertyValue(varargin, 'Fig',  []); %#ok<ASGLU>

%% Cr�ation de la fen�tre avec les menu et callbacks sp�cialis�s pour la nav

Fig = plot_navigation_Figure(Fig);

%% Traitement des fichiers

NbFic = length(nomFic);
str1 = 'Trac� de la navigation des fichiers .raw.';
str2 = 'Plot .raw navigation';
hw = create_waitbar(Lang(str1,str2), 'N', NbFic);
for k=1:NbFic
    my_waitbar(k, NbFic, hw);
    
    a = cl_ExRaw('nomFic', nomFic{k});
    if isempty(a)
        continue
    end
    
    [flag, DataPosition] = SSc_ReadDatagrams(nomFic{k}, 'Ssc_Position');
    if ~flag
        continue
    end
    % Si pas de navigation (le Cache a �t� g�n�r� � vide.), on propose
    % de charger a posteriori la nav depuis un fichier Annexe.
    if all(isnan(DataPosition.Latitude(:)))
        strFR = 'La navigation est vide. Veuillez tout d''abord importer une navigation annexe.';
        strUS = 'Navigation Data is empty. Please, import first a navigation file.';
        my_warndlg(Lang(strFR, strUS), 1);
        my_close(Fig);
        if ~isempty(Fig2)
            my_close(Fig);
        end
        return
    end
    
    Latitude  = DataPosition.Latitude(:);
    Longitude = DataPosition.Longitude(:);
    TimeNav   = DataPosition.Time;
    
    if ~flag || isempty(Latitude)
        continue
    end
    
    Longitude  = Longitude(:,1);
    Latitude   = Latitude(:,1);
    SonarTime  = TimeNav;
    Heading    = [];
    
    plot_navigation(nomFic{k}, Fig, Longitude, Latitude, SonarTime.timeMat, Heading);
end
my_close(hw, 'MsgEnd');

flag = 1;
