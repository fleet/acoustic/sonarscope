% Trace de la navigation et des capteurs de vitesse et de cap d'un fichier .all
%
% Syntax
%   plot_position_data(nomFic, nomSignal)
% 
% Input Arguments 
%   nomFic : Liste de fichiers .all
%   nomSignal : Height | Heading | Speed | CourseVessel | SoundSpeed 
%
% Examples
%   nomDir = '/home3/jackson/calimero/EM1002/ZoneB/B20040916_1'
%   nomDir = '/home1/jackson/FEMME2005'
%   nomDir = 'D:\NIWA_DATA\TAN0105'
%   liste = listeFicOnDir2(nomDir, '.all')
%   plot_position_data(liste, 'Heading')
%   plot_position_data(liste, 'Speed')
%   plot_position_data(liste, 'CourseVessel')
%   plot_position_data(liste, 'SoundSpeed')
%   plot_position_data(liste, 'Height')
%   plot_position_data(liste, 'Mode')
%   plot_position_data(liste, 'Time')
%   plot_position_data(liste, 'Drift')
%
% See also cl_simrad_all plot_index Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function plot_position_data(~, Fig1, nomFic, nomSignal, varargin)

if ~iscell(nomFic)
    nomFic = {nomFic};
end

NbFic = length(nomFic);
nbColors = 64;
Colors = jet(nbColors);

Ident = rand(1);

figure(Fig1);
set(Fig1, 'UserData', Ident)
hold on;

%% Boucle sur les fichiers

XLim = [Inf -Inf];
YLim = [Inf -Inf];
ZLim = [Inf -Inf];
hw = create_waitbar('RAW files : reading signal', 'N', NbFic);
for k=1:NbFic
    my_waitbar(k, NbFic, hw);
    this = cl_ExRaw('nomFic', nomFic{k});
    
    [flag, Data] = SSc_ReadDatagrams(nomFic{k}, 'Ssc_Position');
    if ~flag
        continue
    end
    Longitude = Data.Longitude(:);
    Latitude  = Data.Latitude(:);
    Time      = Data.Time;

    [flag, Signal] = extractionSignal(this, nomSignal, Latitude, Longitude, Time);
    if ~flag
        continue
    end
    
    if strcmp(nomSignal, 'Heading')
        Colors = hsv(nbColors);
    end
    
    if strcmp(nomSignal, 'Heading')
        ZLim = [0 360];
    else
        ZLim(1) = min(ZLim(1), double(min(Signal.Value(:), [], 'omitnan')));
        ZLim(2) = max(ZLim(2), double(max(Signal.Value(:), [], 'omitnan')));
    end
    XLim(1) = min(XLim(1), min(Longitude(:), [], 'omitnan'));
    XLim(2) = max(XLim(2), max(Longitude(:), [], 'omitnan'));
    YLim(1) = min(YLim(1), min(Latitude(:),  [], 'omitnan'));
    YLim(2) = max(YLim(2), max(Latitude(:),  [], 'omitnan'));
end
my_close(hw)

figure(Fig1);
if isempty(Signal.Unit)
    title(nomSignal)
else
    title([nomSignal ' (' Signal.Unit ')']);
end

%% Saisie des bornes de la table de couleurs

[flag, ZLim] = saisie_CLim(min(ZLim), max(ZLim), Signal.Unit, 'SignalName', nomSignal, 'hFigHisto', 87698);
if ~flag
    return
end

%%

figure(Fig1);
W = axis;
x = linspace(XLim(1), XLim(1)+100*nbColors*eps(XLim(1)), nbColors);
y = linspace(YLim(1), YLim(2), nbColors);
h = imagesc(x, y, linspace(ZLim(1), ZLim(2), nbColors)); colormap(Colors), colorbar %#ok<NASGU>
% delete(h)
hold on;
axis(W)

hw = create_waitbar('RAW files : plting signal', 'N', NbFic);
for k=1:NbFic
    my_waitbar(k, NbFic, hw);
    this = cl_ExRaw('nomFic', nomFic{k});
    plot_position_data_unitaire(this, nomSignal, ZLim, Fig1, nbColors, Colors);
end
my_close(hw, 'MsgEnd')

% if isempty(Z.Unit)
%     FigName = sprintf('Sondeur %d Serial Number  %d - %s', EmModel, SystemSerialNumber, nomSignal);
% else
%     FigName = sprintf('Sondeur %d Serial Number  %d - %s (%s)', EmModel, SystemSerialNumber, nomSignal, Z.Unit);
% end
figure(Fig1);



function plot_position_data_unitaire(this, nomSignal, ZLim, Fig1, nbColors, Colors)

figure(Fig1)
nomFic = this.nomFic;

%% Lecture de la navigation et du signal

[flag, Data] = SSc_ReadDatagrams(nomFic, 'Ssc_Position');
if ~flag
    return
end
Longitude = Data.Longitude(:);
Latitude  = Data.Latitude(:);
TimeNav   = Data.Time;

%% Lecture du signal

[flag, Signal] = extractionSignal(this, nomSignal, Latitude, Longitude, TimeNav);
if ~flag
    return
end

%% Trac�

ZData = Signal.Value(:,1);
subNonNaN = find(~isnan(ZData));
if length(subNonNaN) <= 1
    return
end
subNaN = find(isnan(ZData));
ZData(subNaN) = interp1(subNonNaN, ZData(subNonNaN), subNaN, 'linear', 'extrap');

Value = interp1(Signal.Time, ZData, TimeNav);

% Value = ZData;
if ZLim(2) == ZLim(1)
    ZLim(1) = ZLim(1) - 0.5;
    ZLim(2) = ZLim(2) + 0.5;
end
coef = (nbColors-1) / (ZLim(2) - ZLim(1));
Value = ceil((Value - ZLim(1)) * coef);

%% Affichage de la navigation

figure(Fig1)
for k=1:nbColors
    sub = find(Value == (k-1));
    if ~isempty(sub)
        h = plot(Longitude(sub), Latitude(sub), '+');
        set(h, 'Color', Colors(k,:), 'Tag', nomFic)
        set_HitTest_Off(h)
    end
end

sub = find(Value < 0);
if ~isempty(sub)
    h = plot(Longitude(sub), Latitude(sub), '+');
    set(h, 'Color', Colors(1,:)*0.8, 'Tag', nomFic)
    set_HitTest_Off(h)
end

sub = find(Value > (nbColors-1));
if ~isempty(sub)
    h = plot(Longitude(sub), Latitude(sub), '+');
    set(h, 'Color', Colors(nbColors,:)*0.8, 'Tag', nomFic)
end

axisGeo;
grid on;
% colorbar % Supprim� le 7/11/2006 car ca remet les infos de colorbar de 0
% � 64
drawnow


function [flag, Signal] = extractionSignal(this, nomSignal, Latitude, Longitude, Time)

[flag, Signal] = SScCacheXMLBinUtils.getTimeSignalUnit(this.nomFic, 'Ssc_Position', 'Heading');
if ~flag
    return
end

if strcmp(nomSignal, 'Time')
    Signal.Value = Signal.Value.timeMat;
    Signal.Value = (Signal.Value - Signal.Value(1)) * (24 * 3600);
    Signal.Unit = 's';
    return
    
elseif strcmp(nomSignal, 'Heading') && (all(Signal.Value(:) == 0) || all(isnan(Signal.Value(:))))
    cap = calCapFromLatLon(Latitude, Longitude, 'Time', Time);
    Signal.Value = cap;
    Signal.Unit = 'deg';
    return
    
elseif strcmp(nomSignal, 'Speed')
    [~, speed] = calCapFromLatLon(Latitude, Longitude, 'Time', Time);
    Signal.Value = speed;
    Signal.Unit = 'm/s';
    return
    
elseif strcmp(nomSignal, 'Ping Inter Dist')
    [~, ~, distance] = calCapFromLatLon(Latitude, Longitude, 'Time', Time);
    Signal.Value = distance;
    Signal.Unit = 'm';
    Signal.Time = Time;
    return
    
elseif strcmp(nomSignal, 'SampleFrequency')
    [flag, DataSample] = SSc_ReadDatagrams(this.nomFic, 'Ssc_Sample', 'Memmapfile', -1);
    if ~flag
        return
    end
    Signal.Value = repmat(DataSample.SampleFrequency(1), length(Time), 1);
    Signal.Unit = 'Hz';
    Signal.Time = Time;
    return
    
elseif strcmp(nomSignal, 'SoundVelocity')
    [flag, DataSample] = SSc_ReadDatagrams(this.nomFic, 'Ssc_Sample', 'Memmapfile', -1);
    if ~flag
        return
    end
    Signal.Value = repmat(DataSample.SoundVelocity(1), length(Time), 1);
    Signal.Unit = 'm/s';
    Signal.Time = Time;
    return
    
elseif strcmp(nomSignal, 'Height')
%    Height_seconds = abs(Height) ./ fe_Hz;
    [flag, DataHeight] = SSc_ReadDatagrams(this.nomFic, 'Ssc_Height');
    if ~flag
        return
    end
    [flag, DataSample] = SSc_ReadDatagrams(this.nomFic, 'Ssc_Sample', 'Memmapfile', -1);
    if ~flag
        return
    end
    Height_seconds = DataHeight.Height;
    fe_Hz          = DataSample.SampleFrequency(1);
    Signal.Value = -(Height_seconds / 200) * fe_Hz;
    Signal.Unit = 'm';
    return
end

if isempty(Signal)
    flag = 0;
end
