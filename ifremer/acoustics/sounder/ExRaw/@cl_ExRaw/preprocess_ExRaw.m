function preprocess_ExRaw(~, listFileNames)

if ~iscell(listFileNames)
    listFileNames = {listFileNames};
end

useParallel = get_UseParallelTbx;

str1 = 'Prétraitement des fichiers EK';
str2 = 'Preprocessing EK files';
N = length(listFileNames);
if useParallel && (N > 1)
    [hw, DQ] = create_parforWaitbar(N, Lang(str1,str2));
    parfor kFic=1:N
        fprintf('%d/%d - %s', kFic, N, listFileNames{kFic});
        try
            a = cl_ExRaw('nomFic', listFileNames{kFic}) %#ok<NASGU>
            fprintf(' : OK\n');
        catch ME
            ErrorReport = getReport(ME)
            SendEmailSupport(ErrorReport)
            fprintf(' : ERROR\n');
        end
        send(DQ, kFic);
    end
    my_close(hw, 'MsgEnd')
else
    hw = create_waitbar(Lang(str1,str2), 'N', N);
    for kFic=1:N
        my_waitbar(kFic, N, hw)
        fprintf('%d/%d - %s', kFic, N, listFileNames{kFic});
        try
            a = cl_ExRaw('nomFic', listFileNames{kFic}); %#ok<NASGU>
            fprintf(' : OK\n');
        catch ME
            ErrorReport = getReport(ME) %#ok<NOPRT>
            SendEmailSupport(ErrorReport)
            fprintf(' : ERROR\n');
        end
    end
    my_close(hw, 'MsgEnd')
end
