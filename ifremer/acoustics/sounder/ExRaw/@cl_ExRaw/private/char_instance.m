% Transformation en chaine de caracteres d'une instance
%
% Syntax
%   str = char_instance(a)
%
% Input Arguments
%   a : instance de cl_sdf
%
% Name-Value Pair Arguments
%   InfoSonar  : 1=Information de l'image uniquement
%                2=Info de l'information sonar uniquement
%                3=Display de toute l'information disponible (image+sonar) : (3 par defaut)
%
% Output Arguments
%   str : une chaine de caracteres representative de l instance
%
% Examples
%   nomFic = '???\GeoLittomer\BOURGNEUF-D20120412-T071623_raw.raw';
%   a = cl_ExRaw('nomFic', nomFic);
%   a
%
% See also cl_sdf Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function str = char_instance(this, varargin)

str = {};
str{end+1} = sprintf('nomFic            <-> %s', this.nomFic);
str{end+1} = sprintf('NavigationFile    <-> %s', this.NavigationFile);

%% Lecture du r�pertoire SonarScope

[nomDir, nom] = fileparts(this.nomFic);
nomDir = fullfile(nomDir, 'SonarScope', nom);
if ~exist(nomDir, 'dir')
    return
end

%% Lecture du fichier XML d�crivant les donn�es d'�chantillon

nomFicXml = fullfile(nomDir, 'Ssc_Sample.xml');
flag = exist(nomFicXml, 'file');
if ~flag
    messageErreurFichier(nomFicXml, 'ReadFailure');
    return
end
XML = xml_mat_read(nomFicXml);

Fields = fieldnames(XML.Dimensions);
str{end+1} = 'Dimensions';
for k=1:length(Fields)
    str{end+1} = sprintf('\t%s\t\t\t<-- %d', Fields{k}, XML.Dimensions.(Fields{k})); %#ok<AGROW>
end

str{end+1} = '-- Signals Data Sample --';
for k=1:length(XML.Signals)
    str{end+1} = sprintf('\t%s\t\t\t<-> %s(%s) (%s)', XML.Signals(k).Name, XML.Signals(k).Storage, XML.Signals(k).Dimensions, XML.Signals(k).Unit); %#ok<AGROW>
end

if isfield(XML, 'Images')
    str{end+1} = '-- Images Data Sample --';
    for k=1:length(XML.Images)
        str{end+1} = sprintf('\t%s\t\t\t<-> %s(%s) (%s)', XML.Images(k).Name, XML.Images(k).Storage, XML.Images(k).Dimensions, XML.Images(k).Unit); %#ok<AGROW>
    end
end

%% Lecture du fichier XML d�crivant les donn�es de position

nomFicXml = fullfile(nomDir, 'Ssc_Position.xml');
flag = exist(nomFicXml, 'file');
if ~flag
    messageErreurFichier(nomFicXml, 'ReadFailure');
    return
end
XML = xml_mat_read(nomFicXml);

str{end+1} = '-- Signals Position--';
for k=1:length(XML.Signals)
    str{end+1} = sprintf('\t%s\t\t\t<-> %s(%s) (%s)', XML.Signals(k).Name, XML.Signals(k).Storage, XML.Signals(k).Dimensions, XML.Signals(k).Unit); %#ok<AGROW>
end

if isfield(XML, 'Images')
    str{end+1} = '-- Images Position --';
    for k=1:length(XML.Images)
        str{end+1} = sprintf('\t%s\t\t\t<-> %s(%s) (%s)', XML.Images(k).Name, XML.Images(k).Storage, XML.Images(k).Dimensions, XML.Images(k).Unit); %#ok<AGROW>
    end
end
%% Concatenation en une chaine

str = cell2str(str);
