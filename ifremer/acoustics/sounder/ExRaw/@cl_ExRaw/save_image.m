function flag = save_image(this, numSounder, NomLayer, X)

% TODO : tester appel � fonction save_signal flag =
% save_signal(this.nomFic, NomLayer, X, )

%% Lecture du r�pertoire SonarScope

[nomDir, nom] = fileparts(this.nomFic);
nomDir        = fullfile(nomDir, 'SonarScope', nom);
if ~exist(nomDir, 'dir')
    return
end

%% Lecture du fichier XML d�crivant la donn�e

nomFicXml = fullfile(nomDir, 'Ssc_Sample.xml');
flag = exist(nomFicXml, 'file');
if ~flag
    return
end
% edit(nomFicXml)
XML = xml_mat_read(nomFicXml);

%% Recherche du signal dans le XML

k = find(strcmp({XML.Images.Name}, NomLayer));
if isempty(k)
    str1 = sprintf('Image "%s" non trouv� dans "%s', NomLayer, nomFicXml);
    str2 = 'TODO';
    my_warndlg(Lang(str1,str2), 1);
    return
end

%% Ecriture du signal

Image = X;
flag = writeSignal(nomDir, XML.Images(k), Image);
if ~flag
    % Ici �a bugue car le fichier est ouvert dans SonarScope. Il faut
    % trouver une solution. Peut-�tre en ramenant l'image courante en RAM
    return
end

