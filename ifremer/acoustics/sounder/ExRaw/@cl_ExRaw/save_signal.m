function flag = save_signal(this, NomLayer, X, varargin)

% TODO : tester appel � fonction save_signal flag =
% save_signal(this.nomFic, NomLayer, X, )

[varargin, nomDirBin] = getPropertyValue(varargin, 'nomDirBin', 'Ssc_Position'); %#ok<ASGLU>

[nomDir, nom] = fileparts(this.nomFic);
nomFicXml = fullfile(nomDir, 'SonarScope', nom, [nomDirBin '.xml']);
if ~exist(nomFicXml, 'file')
    flag = 0;
    return
end

XML = xml_read(nomFicXml);
nomDirRacine = fullfile(nomDir, 'SonarScope', nom);

k = find(strcmp({XML.Signals.Name}, NomLayer));
if isempty(k)
    str1 = sprintf('Signal "%s" non trouv� dans "%s', NomLayer, nomFicXml);
    str2 = 'TODO';
    my_warndlg(Lang(str1,str2), 1);
    return
end
flag = writeSignal(nomDirRacine, XML.Signals(k), X);
