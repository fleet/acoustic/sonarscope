function [flag, listFrequencies, listDataType, nomLayers] = selectionLayers(~, nomFic, varargin)

[varargin, InitValueDataType]     = getPropertyValue(varargin, 'InitValueDataType',     3);
[varargin, InitValueFreq]         = getPropertyValue(varargin, 'InitValueFreq',         1);
[varargin, SelectionModeFreq]     = getPropertyValue(varargin, 'SelectionModeFreq',     'Multiple');
[varargin, SelectionModeDataType] = getPropertyValue(varargin, 'SelectionModeDataType', 'Multiple'); %#ok<ASGLU>

listFrequencies = [];
listDataType    = [];
nomLayers       = {};

%% Cr�ation de l'instance

this = cl_ExRaw('nomFic', nomFic); % Juste pour forcer le passage par xxx2ssc et en attendant que
% ce qui suit puisse �tre fait par des get(this, 'PN')
% Ex : \COM25-EK60-18kHz-DBS-RAW_20120702-093141.raw
if isempty(this)
%     str1 = 'Le fichier pose probl�me. Pas de solution pour l''instant';
%     str2 = 'Problem in load file: no solution for the moment.';
%     my_warndlg(Lang(str1,str2), 1);
    flag = 0;
    return
end

%% S�lection des types de data

nomLayers{1}     = 'Power';
nomLayers{end+1} = 'Sp';
nomLayers{end+1} = 'Sv';
nomLayers{end+1} = 'Phase Along';
nomLayers{end+1} = 'Phase Athwart';
str1 = 'S�lectionnez les donn�es � importer.';
str2 = 'Select the data type you want to import.';
[listDataType, flag] = my_listdlg(Lang(str1,str2), nomLayers, 'InitialValue', InitValueDataType, 'SelectionMode', SelectionModeDataType);
if ~flag
    return
end
nomLayers = nomLayers(listDataType);

%% S�lection des fr�quences

[flag, Frequency] = SScCacheXMLBinUtils.getSignalUnit(nomFic, 'Ssc_Sample', 'AcousticFrequency');
if ~flag
    return
end
if length(Frequency) == 1
    listFrequencies = 1;
else
    for k=1:length(Frequency)
        str{k} = num2str(Frequency(k)); %#ok<AGROW>
    end
    str1 = 'S�lectionnez la /les fr�quences � importer.';
    str2 = 'Select the frequencies you want to import.';
    [listFrequencies, flag] = my_listdlg(Lang(str1,str2), str, 'InitialValue', InitValueFreq, 'SelectionMode', SelectionModeFreq);
    if ~flag
        return
    end
end
