% Acces en ecriture des proprietes d'une instance
%
% Syntax
%   b = set(a, ...)
%
% Input Arguments
%   a : Une instance de la classe cl_ExRaw
%
% Name-Value Pair Arguments
%   nomFic : Nom(s) du fichier .all
%
% Output Arguments
%   [] : Sauve l'instance dans a
%   b  : Instance de la classe
%
% Examples
%   nomFic = getNomFicDatabase('xxxx.raw')
%   a = cl_ExRaw
%   a = set(a, 'nomFic', nomFic)
%
% See also cl_ExRaw cl_ExRaw/get Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function varargout = set(this, varargin)

[varargin, nomFic]    = getPropertyValue(varargin, 'nomFic',    []);
[varargin, nomFicNav] = getPropertyValue(varargin, 'nomFicNav', []); %#ok<ASGLU>

if ~isempty(nomFic)
    nomFic = windows_replaceVolumeLetter(nomFic);
    this.nomFic = nomFic;
    
    % En entr�e de ExRaw : nomFic et nomFicNav si il n'est pas vide.
    isEK80 = ExRaw_identifySounder(nomFic);
    if isEK80
        flag = EK80Raw2ssc(nomFic);
    else
        flag = ExRaw2ssc(nomFic);
    end

    this.nomFic = nomFic;
    if ~isempty(nomFicNav)
        this.NavigationFile = nomFicNav;
    end
    
    if ~flag
        varargout{1} = [];
        return
    end
    
    [flag, Data] = SSc_ReadDatagrams(nomFic, 'Ssc_Position');
    if ~flag
        varargout{1} = [];
        return
    end
    
    % R�cup�ration du fichier de Navigation.
    if iscell(Data.NavigationFile) % Ajour JMA le 05/03/3030 % TODO GLU
        this.NavigationFile = Data.NavigationFile{1};
    end
end

%% Sortie

if nargout == 0
    assignin('caller', inputname(1), this);
else
    varargout{1} = this;
end
