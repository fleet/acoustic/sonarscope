function flag = survey_Export3DViewer(this, listeFicRAW, nomDirOut, CLim, CLimNaN, ColormapIndex, Tag, ...
    HauteurMinInMeters, SuppressionOverHeight, identLayer, TailleMax, MeanTide, varargin)
    
Carto = [];

N = length(listeFicRAW);
str1 = 'Export des fichiers EK-raw dans GLOBE';
str2 = 'Exporting EK-raw files to GLOBE';
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    [flag, Carto] = survey_Export3DViewer_unitaire(this, listeFicRAW{k}, nomDirOut, CLim, CLimNaN, ColormapIndex, Tag, Carto, ...
        HauteurMinInMeters, SuppressionOverHeight, identLayer, TailleMax, MeanTide, varargin{:});
end
my_close(hw, 'MsgEnd')


function [flag, Carto] = survey_Export3DViewer_unitaire(~, nomFicExRaw, nomDirOut, CLim, CLimNaN, ColormapIndex, Tag, Carto, ...
    HauteurMinInMeters, SuppressionOverHeight, identLayer, TailleMax, MeanTide, varargin)

[varargin, listFreq] = getPropertyValue(varargin, 'listFreq', []); %#ok<ASGLU>

I0 = cl_image_I0;

%% Instance cl_ExRaw

a = cl_ExRaw('nomFic', nomFicExRaw); %#ok<NASGU> % Juste pour provoquer une premi�re lecture si pas encore r�alis��

%% S�lection des fr�quences

if isempty(listFreq) % TODO : faudrait
    [flag, Frequency] = SScCacheXMLBinUtils.getSignalUnit(nomFicExRaw, 'Ssc_Sample', 'AcousticFrequency');
    if ~flag
        return
    end
    for k=1:length(Frequency)
        str{k} = num2str(Frequency(k)); %#ok<AGROW>
    end
    str1 = 'S�lectionnez la /les fr�quences � importer.';
    str2 = 'Select the frequencies you want to import.';
    [listFreq, flag] = my_listdlg(Lang(str1,str2), str, 'InitialValue', 1);
    if ~flag
        return
    end
end

%% Lecture de l'image

[flag, b, Carto, identLayer] = import_ExRaw(I0, nomFicExRaw, 'Carto', Carto, ...
    'listFrequencies', listFreq, 'listDataType', identLayer); %#ok<ASGLU>
if ~flag || isempty(b)
    return
end

nx = b(1).nbColumns;
ny = b(1).nbRows;

% Le calcul de d�coupage ne se fait que le long du profil (pour l'instant).
TailleMax = min(TailleMax, nx*ny);
nBBlocks  = ceil(ny / TailleMax);
n2        = floor(ny / nBBlocks);

[flag, ~] = SSc_ReadDatagrams(nomFicExRaw, 'Ssc_Sample', 'Memmapfile', -1);
if ~flag
    % TODO : message d'erreur SVP
    return
end

str1 = 'Export par blocks.';
str2 = 'Export by blocks.';
hw = create_waitbar(Lang(str1,str2), 'N', nBBlocks);
for kBlock=1:nBBlocks
    my_waitbar(kBlock, nBBlocks, hw)
    
    %% Cr�ation du r�pertoire
    
    [~, nomDirBin] = fileparts(nomFicExRaw);
%     for k=1:length(nomLayers)
        if isempty(Tag)
            nomDirXml = fullfile(nomDirOut, [nomDirBin '_' num2str(kBlock, '%02d')]);
        else
            nomDirXml = fullfile(nomDirOut, [nomDirBin '_' Tag '_' num2str(kBlock, '%02d')]);
        end
        if ~exist(nomDirXml, 'dir')
            flag = mkdir(nomDirXml);
            if ~flag
                messageErreurFichier(nomDirXml, 'WriteFailure');
                return
            end
        end
        
        suby = (0:(n2+1)) + (kBlock-1)*n2;
        suby(suby < 1)  = [];
        suby(suby > ny) = [];
%         NomFicXMLLayers{k} = [nomDirXml{k} '.xml']; %#ok<AGROW>
%     end
    NomFicXML = [nomDirXml '.xml'];
    flag = survey_Export3DViewer_Block(b, suby, nomDirXml, NomFicXML, nomFicExRaw, Tag, ...
        CLim, CLimNaN, ColormapIndex, HauteurMinInMeters, SuppressionOverHeight, MeanTide);
    if ~flag
        break
    end
end
my_close(hw)


function flag = survey_Export3DViewer_Block(b, suby, nomDirXml, NomFicXML, nomFicExRaw, Tag, ...
    CLim, CLimNaN, ColormapIndex, HauteurMinInMeters, SuppressionOverHeight, MeanTide)

%% Traitements des diff�rentes fr�quences

% b = [b(1);  b(:)]; % TODO  : en attendant que le pb soit r�solu dans 3DViewer
nbSlices = length(b);

% R�cup�ration des nbs d'�chantillons utiles des images.
% Cas de Colmeia ou les images des diff�rentes Freq ne sont pas de la m�me taille utile.
[flag, DataSample] = SSc_ReadDatagrams(nomFicExRaw, 'Ssc_Sample', 'Memmapfile', -1);
if ~flag
    % TODO : message d'erreur SVP
    return
end

for k=1:nbSlices
    try
        NbSamples = DataSample.Dimensions.nbSamples(k);
    catch
        NbSamples = DataSample.Dimensions.nbSamples(1);
    end
    SliceName         = b(k).Comments;
    SurfaceSoundSpeed = get(b(k), 'SurfaceSoundSpeed');
    SampleFrequency   = get(b(k), 'SampleFrequency');
    deltay            = mean(SurfaceSoundSpeed ./ (2*SampleFrequency), 'omitnan');
    Layer             = b(k);
%     layerNames{k} = extract_ImageName(Layer); %#ok<AGROW>
%     layerUnits{k} = Layer.Unit; %#ok<AGROW>
    
    %% Gestion de la supression de la partie haute de l'image
    
    % subxLimites = {[1 100]; [800 890]};
    %     xLimites = {};
    subxLimites = {};
    if HauteurMinInMeters ~= 0
        HauteurMinInSamples = floor(HauteurMinInMeters / deltay);
        subxLimites{end+1} = [1 HauteurMinInSamples]; %#ok<AGROW>
    end
    nbBande = length(subxLimites);
    
    nbPings = Layer.nbRows;
    if SuppressionOverHeight || ~isempty(subxLimites) % || ~isempty(xLimites)
        nbCol = Layer.nbColumns;
        
        % TODO : oups, y'a un truc qui va pas quelque part. parfois on a H en samples, parfois en s
        H1 = get(Layer, 'SonarHeight');
        fe_Hz = get(b(k), 'SonarSampleFrequency');
        H2 = abs(H1 ./ fe_Hz);
        H = min(H1,H2);
        
        if all(H == 0)
            % TODO : Message d'erreur toutes les hauteurs � 0
        else
            subxLimites{nbBande+1}(:,1) = max(1,floor(abs(H(:)))); %#ok<AGROW>
            subxLimites{nbBande+1}(:,2) = nbCol; %#ok<AGROW>
            
            [flag, Layer] = masquage_abscisses(Layer, 'subxLimites', subxLimites);
            if ~flag
                return
            end
        end
    end
    
    %% Suppression des colonnes totalement vides
    
    subNoNaN = (sum(isnan(Layer)) ~= nbPings);
    indXLastNoNaN =  find(subNoNaN, 1, 'last');
    if ~isempty(indXLastNoNaN)
        subX = 1:indXLastNoNaN;
        Layer = extraction(Layer, 'subx', subX);
    end
    
    %% Seuillage �ventuel
    
    if any(~isinf(CLimNaN))
        I = get(Layer, 'Image');
        
        if ~isinf(CLimNaN(k,1))
            I(I < CLimNaN(k,1)) = NaN;
        end
        if ~isinf(CLimNaN(k,2))
            I(I > CLimNaN(k,2)) = NaN;
        end
        Layer = set_Image(Layer, I);
    end
    Layer = extraction(Layer, 'suby', suby, 'subx', 1:Layer.nbColumns, 'ForceExtraction');
% 	I = get(Layer, 'Image');
    
    %% Transformation de l'image en indexed
    
%     mots = strsplit(SliceName, ' ');
% %     kFrequency = mots{3}(12:end)
%     sliceName = mots{6};
%     switch sliceName % Pas trop mais on va abandonner les exports en XML-Bin
%         case 'Amplitude'
%             kSlice = 1;
%         case 'Sp'
%             kSlice = 2;
%         case 'Sv'
%             kSlice = 3;
%         case 'AlongShip'
%             kSlice = 4;
%         case 'pppp'
%             kSlice = 5;
%     end
%{
ans =
  5�1 cell array
    {'SounderName=EK60  NumSounder=28  NumChannel=1  Frequency=18 kHz Amplitude'}
    {'SounderName=EK60  NumSounder=28  NumChannel=1  Frequency=18 kHz Sp'       }
    {'SounderName=EK60  NumSounder=28  NumChannel=1  Frequency=18 kHz Sv'       }
    {'SounderName=EK60  NumSounder=28  NumChannel=1  Frequency=18 kHz AlongShip'}
    {'SounderName=EK60  NumSounder=28  NumChannel=2  Frequency=38 kHz Amplitude'}
%}
    
    
    Layer.ColormapIndex = ColormapIndex;
    if size(CLim,1 >= k)
        CLimHere = CLim(k,:);
    else
        CLimHere = CLim(1,:);
    end
%     [c, flag] = Intensity2Indexed(Layer, 'CLim', CLim(k,:));
    [c, flag] = Intensity2Indexed(Layer, 'CLim', CLimHere); % Modif JMA le 18/09/2021 pour bug Carla mail du 09/09/2021
    if ~flag
        return
    end
    map = Layer.Colormap;
    
    %% Cr�ation de l'image
    
    I = get(c, 'Image');
    %     if NbSamples/DataSample.Dimensions.nbSamples < 1
    if (NbSamples / Layer.nbColumns) < 1
        % Redimensionnement n�cessaire de l'image : la Range est identique
        % pour chacune mais pas le nb Samples (cas de Colmeia par ex).
        pppp = I(:,1:NbSamples);
        I = imresize(pppp, [size(I,1) size(I,2)]);
    end
    I(I == 0) = 1;
    %     I(subNaN) = 0;
    
    %% Cr�ation si n�cessaire du r�pertoire d'archivage de l'image
    
    kRep = floor(k/100);
    nomDirPng = fullfile(nomDirXml, num2str(kRep, '%03d'));
    if ~exist(nomDirPng, 'dir')
        flag = mkdir(nomDirPng);
        if ~flag
            messageErreurFichier(nomDirPng, 'WriteFailure');
            return
        end
    end
    
    nbRows = Layer.nbRows;
    %     nbColumns = c.nbColumns;
    x = get(Layer, 'x');
    sumX = sum(I == 0);
    subX = find(sumX ~= nbRows);
    if isempty(subX)
        %% TODO message d'erreur
        flag = 0;
        return
    end
    
    %% Ecriture de l'image PNG
    
    x = x(subX);
    nbColumns = length(x);
    nomFic1 = [num2str(k, '%05d') '.png'];
    nomFic2 = fullfile(nomDirPng, nomFic1);
    I(I == 0) = 1;
    I(I == 255) = 0;
    imwrite(I(:,subX)', map, nomFic2, 'Transparency', 0)
    
    %% Export .tif
    
    nomFic1 = sprintf('%05d.tif', k);
    nomFic2 = fullfile(nomDirPng, nomFic1);
    X = get_Image(Layer);
    export_ImageTif32bits(X(:,subX)', nomFic2, 'Mute', 1);
    
    %% Cr�ation du fichier XML-Bin
    
    Immersion     = get(Layer, 'Immersion');
    FishLatitude  = get(Layer, 'FishLatitude');
    FishLongitude = get(Layer, 'FishLongitude');
    %     Heave         = get(Layer, 'SonarHeave');
    Tide          = get(Layer, 'Tide');
    %     SonarFrequency     = get(Layer, 'SonarFrequency');
    
    %     Tide  = zeros(size(FishLongitude), 'single');
    Heave = zeros(size(FishLongitude), 'single'); % TODO : constat� qu'il ne faut pas prendre en compte le Heave sur donn�es Axel Concarneau
    
    %     y = get(Layer, 'y');
    T = get(Layer, 'Time');
    %     ImageName = Layer.Name;
    [~, ImageName] = fileparts(nomFicExRaw);
    
    MinHeight = -x(1)   * deltay;
    MaxHeight = -x(end) * deltay;
    MinHeight = ones(nbRows, 1, 'single') * MinHeight;
    MaxHeight = ones(nbRows, 1, 'single') * MaxHeight;
    if ~isempty(Immersion)
        MinHeight = MinHeight + Immersion;
        MaxHeight = MaxHeight + Immersion;
    end
    % if ~isempty(this.Sonar.Height)
    %     MinHeight = MinHeight + min(this.Sonar.Height(suby));
    %     MaxHeight = MaxHeight + min(this.Sonar.Height(suby));
    % end
    if any(isnan(FishLatitude))
        %         fprintf('FishLatitude NaN\n');
        subNaN = isnan(FishLatitude);
        subNonNaN = find(~subNaN);
        subNaN = find(subNaN);
        FishLatitude(subNaN) = interp1(subNonNaN, FishLatitude(subNonNaN), subNaN, 'linear', 'extrap');
    end
    if any(isnan(FishLongitude))
        %         fprintf('LongitudeNadir NaN\n');
        subNaN = isnan(FishLongitude);
        subNonNaN = find(~subNaN);
        subNaN = find(subNaN);
        FishLongitude(subNaN) = interp1(subNonNaN, FishLongitude(subNonNaN), subNaN, 'linear', 'extrap');
    end
    
    if isempty(Tag)
        DataSlices.name = ImageName;
    else
        DataSlices.name = [ImageName '_' Tag];
    end
    DataSlices.from = nomFicExRaw; %'Unkown';
    DataSlices.Date = (T.date)';
    DataSlices.Hour = (T.heure)';
    DataSlices.PingNumber = (1:nbRows)';
    if isempty(Tide) || all(Tide == 0)
        DataSlices.Tide = repmat(MeanTide, size(DataSlices.PingNumber));
    else
        DataSlices.Tide  = Tide;
    end
    
    DataSlices.Heave = Heave;
    
    DataSlices.SliceName{k,1} = SliceName;
    
    % Comment� pour test
    %     DataSlices.LatitudeNadir  = FishLatitude';
    %     DataSlices.LongitudeNadir = FishLongitude';
    
	SliceLatitude  = FishLatitude';
    SliceLongitude = FishLongitude';
    %{
    
    % TODO
    % get(this, 'EchoSounderAcrossAngle')
    
    across = (k-2) * 0;
    along = 0;
    Heading = get(b(k), 'SonarHeading');
    [SliceLatitude, SliceLongitude] = latlonAcrossAlong2latlon(Carto, FishLatitude', FishLongitude', Heading, across, along);
    %}
    
    DataSlices.LatitudeTop(k,:)  = SliceLatitude;
    DataSlices.LongitudeTop(k,:) = SliceLongitude;
    
    % Donn�es optionnelles
    % TODO , corriger les valeurs en fonction du roulis et du tangage
    DataSlices.LatitudeBottom(k,:)  = SliceLatitude;
    DataSlices.LongitudeBottom(k,:) = SliceLongitude;
    
    HeaveInSamples = Heave / deltay; % TODO POURQUOI 0.25 ? Parce que c'est la valeur qui me donnait le meilleur rendu ! TODO TODO TODO !
    DataSlices.DepthTop    = MinHeight - 0.25*HeaveInSamples; % TODO  : en attendant que ce soit dfait dans le 3DViewer
    DataSlices.DepthBottom = MaxHeight - 0.25*HeaveInSamples;
        
    if any(isnan(DataSlices.Date))
        fprintf('Date NaN\n');
    end
    if any(isnan(DataSlices.Hour))
        fprintf('Hour NaN\n');
    end
    if any(isnan(DataSlices.DepthTop))
        fprintf('DepthTop NaN\n');
    end
    if any(isnan(DataSlices.DepthBottom))
        fprintf('DepthBottom NaN\n');
    end
end

% flag = EKcreate_XMLBin_ImagesAlongNavigation(NomFicXML, DataSlices, nbRows, nbColumns);
flag = write_XML_ImagesAlongNavigation(NomFicXML, DataSlices, nbRows, nbColumns);

%{
FigUtils.createSScFigure;
subplot(4,1,1); PlotUtils.createSScPlot(T, DataSlices.LatitudeTop(:)); grid on;
subplot(4,1,2); PlotUtils.createSScPlot(T, DataSlices.LongitudeTop(:)); grid on;
subplot(4,1,3); PlotUtils.createSScPlot(T, DataSlices.DepthTop(:)); grid on;
subplot(4,1,4); PlotUtils.createSScPlot(T, DataSlices.DepthBottom(:)); grid on;
find(isnan(DataSlices.LatitudeTop(:)))
find(isnan(DataSlices.LongitudeTop(:)))
find(isnan(DataSlices.DepthTop(:)))
find(isnan(DataSlices.DepthBottom(:)))
%}

