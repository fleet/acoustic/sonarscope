function flag = survey_ExportGlobeFlyTexture(this, listeFicRAW, nomDirOut, Tag, ...
    HauteurMinInMeters, SuppressionOverHeight, identLayer, MeanTide, varargin)
    
Carto = [];

N = length(listeFicRAW);
str1 = 'Export des fichiers EK-raw dans GLOBE';
str2 = 'Exporting EK-raw files to GLOBE';
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    [flag, Carto] = survey_Export3DViewer_unitaire(this, listeFicRAW{k}, nomDirOut, Tag, Carto, ...
        HauteurMinInMeters, SuppressionOverHeight, identLayer, MeanTide, varargin{:});
end
my_close(hw, 'MsgEnd')


function [flag, Carto] = survey_Export3DViewer_unitaire(~, nomFicExRaw, nomDirOut, Tag, Carto, ...
    HauteurMinInMeters, SuppressionOverHeight, identLayer, MeanTide, varargin)

[varargin, suby]     = getPropertyValue(varargin, 'suby',     []);
[varargin, listFreq] = getPropertyValue(varargin, 'listFreq', []); %#ok<ASGLU>

I0 = cl_image_I0;

%% Instance cl_ExRaw

a = cl_ExRaw('nomFic', nomFicExRaw); %#ok<NASGU> % Juste pour provoquer une premi�re lecture si pas encore r�alis��

%% S�lection des fr�quences

if isempty(listFreq) % TODO : faudrait
    [flag, Frequency] = SScCacheXMLBinUtils.getSignalUnit(nomFicExRaw, 'Ssc_Sample', 'AcousticFrequency');
    if ~flag
        return
    end
    for k=1:length(Frequency)
        str{k} = num2str(Frequency(k)); %#ok<AGROW>
    end
    str1 = 'S�lectionnez la /les fr�quences � importer.';
    str2 = 'Select the frequencies you want to import.';
    [listFreq, flag] = my_listdlg(Lang(str1,str2), str, 'InitialValue', 1);
    if ~flag
        return
    end
end

%% Lecture de l'image

[flag, b, Carto, identLayer, channelName, channelFreq] = import_ExRaw(I0, nomFicExRaw, 'Carto', Carto, 'listFrequencies', listFreq, 'listDataType', identLayer);
if ~flag || isempty(b)
    return
end

% nx = b(1).nbColumns;
ny = b(1).nbRows;
if isempty(suby)
    suby = 1:ny;
end

[flag, ~] = SSc_ReadDatagrams(nomFicExRaw, 'Ssc_Sample', 'Memmapfile', -1);
if ~flag
    % TODO : message d'erreur SVP
    return
end

[~, nomDirBin] = fileparts(nomFicExRaw);
if isempty(Tag)
    nomDirXml = fullfile(nomDirOut, nomDirBin);
else
    nomDirXml = fullfile(nomDirOut, [nomDirBin '_' Tag]);
end
NomFicXML = [nomDirXml '.xml'];

%% Traitements des diff�rentes fr�quences

% b = [b(1);  b(:)]; % TODO  : en attendant que le pb soit r�solu dans 3DViewer
nbLayers = length(b);

% % R�cup�ration des nbs d'�chantillons utiles des images.
% % Cas de Colmeia ou les images des diff�rentes Freq ne sont pas de la m�me taille utile.
% [flag, DataSample] = SSc_ReadDatagrams(nomFicExRaw, 'Ssc_Sample', 'Memmapfile', -1);
% if ~flag
%     % TODO : message d'erreur SVP
%     return
% end
[channelName, ~, subChannelName] = unique(channelName);
[channelFreq, ~, subChannelFreq] = unique(channelFreq);

for k=1:nbLayers
%     try
%         NbSamples = DataSample.Dimensions.nbSamples(k);
%     catch
%         NbSamples = DataSample.Dimensions.nbSamples(1);
%     end
    SliceName         = b(k).Comments;
    SurfaceSoundSpeed = get(b(k), 'SurfaceSoundSpeed');
    SampleFrequency   = get(b(k), 'SampleFrequency');
    deltay            = mean(SurfaceSoundSpeed ./ (2*SampleFrequency), 'omitnan');
    Layer             = b(k);
%     layerNames{subChannelFreq(k),subChannelName(k)} = extract_ImageName(Layer); %#ok<AGROW>
    sliceNames{subChannelName(k)} = channelName{subChannelName(k)}; %#ok<AGROW>
    groupNames{subChannelFreq(k)} = sprintf('%d_kHz', channelFreq(subChannelFreq(k))); %#ok<AGROW>
    sliceUnits{subChannelName(k)} = Layer.Unit; %#ok<AGROW>
    
    %% Gestion de la supression de la partie haute de l'image
    
    % subxLimites = {[1 100]; [800 890]};
    %     xLimites = {};
    subxLimites = {};
    if HauteurMinInMeters ~= 0
        HauteurMinInSamples = floor(HauteurMinInMeters / deltay);
        subxLimites{end+1} = [1 HauteurMinInSamples]; %#ok<AGROW>
    end
    nbBande = length(subxLimites);
    
    nbPings = Layer.nbRows;
    if SuppressionOverHeight || ~isempty(subxLimites) % || ~isempty(xLimites)
        nbCol = Layer.nbColumns;
        
        % TODO : oups, y'a un truc qui va pas quelque part. parfois on a H en samples, parfois en s
        H1 = get(Layer, 'SonarHeight');
        fe_Hz = get(b(k), 'SonarSampleFrequency');
        H2 = abs(H1 ./ fe_Hz);
        H = min(H1,H2);
        
        if all(H == 0)
            % TODO : Message d'erreur toutes les hauteurs � 0
        else
            subxLimites{nbBande+1}(:,1) = max(1,floor(abs(H(:)))); %#ok<AGROW>
            subxLimites{nbBande+1}(:,2) = nbCol; %#ok<AGROW>
            
            [flag, Layer] = masquage_abscisses(Layer, 'subxLimites', subxLimites);
            if ~flag
                return
            end
        end
    end
    
    %% Suppression des colonnes totalement vides
    
    subNoNaN = (sum(isnan(Layer)) ~= nbPings);
    indXLastNoNaN =  find(subNoNaN, 1, 'last');
    if ~isempty(indXLastNoNaN)
        subX = 1:indXLastNoNaN;
        Layer = extraction(Layer, 'subx', subX);
    end
    
    %% Seuillage �ventuel
    
    Layer = extraction(Layer, 'suby', suby, 'subx', 1:Layer.nbColumns, 'ForceExtraction');
	I = get(Layer, 'Image');
    n2 = size(I,2);
%     Reflectivity(1:n2,:,subChannelFreq(k),subChannelName(k)) = flipud(I'); %#ok<AGROW>
    Reflectivity(1:n2,:,subChannelFreq(k),subChannelName(k)) = I'; %#ok<AGROW> % Modif JMA le 20/02/2022 pour images EK80 multifr�quences
    
    nbRows = Layer.nbRows;
    %     nbColumns = c.nbColumns;
    x = get(Layer, 'x');
    sumX = sum(I == 0);
    subX = find(sumX ~= nbRows, 1);
    if isempty(subX)
        %% TODO message d'erreur
        flag = 0;
        return
    end
    
    %% Ecriture de l'image PNG

%     nbColumns = Layer.nbColumns;
    
    %% Cr�ation du fichier XML-Bin
    
    Immersion     = get(Layer, 'Immersion');
    FishLatitude  = get(Layer, 'FishLatitude');
    FishLongitude = get(Layer, 'FishLongitude');
    %     Heave         = get(Layer, 'SonarHeave');
    Tide          = get(Layer, 'Tide');
    %     SonarFrequency     = get(Layer, 'SonarFrequency');
    
    %     Tide  = zeros(size(FishLongitude), 'single');
    Heave = zeros(size(FishLongitude), 'single'); % TODO : constat� qu'il ne faut pas prendre en compte le Heave sur donn�es Axel Concarneau
    
    %     y = get(Layer, 'y');
    T = get(Layer, 'Time');
    %     ImageName = Layer.Name;
    [~, ImageName] = fileparts(nomFicExRaw);
    
    MinHeight = -x(1)   * deltay;
    MaxHeight = -x(end) * deltay;
    MinHeight = ones(nbRows, 1, 'single') * MinHeight;
    MaxHeight = ones(nbRows, 1, 'single') * MaxHeight;
    if ~isempty(Immersion)
        MinHeight = MinHeight + Immersion;
        MaxHeight = MaxHeight + Immersion;
    end
    % if ~isempty(this.Sonar.Height)
    %     MinHeight = MinHeight + min(this.Sonar.Height(suby));
    %     MaxHeight = MaxHeight + min(this.Sonar.Height(suby));
    % end
    if any(isnan(FishLatitude))
        %         fprintf('FishLatitude NaN\n');
        subNaN = isnan(FishLatitude);
        subNonNaN = find(~subNaN);
        subNaN = find(subNaN);
        FishLatitude(subNaN) = interp1(subNonNaN, FishLatitude(subNonNaN), subNaN, 'linear', 'extrap');
    end
    if any(isnan(FishLongitude))
        %         fprintf('LongitudeNadir NaN\n');
        subNaN = isnan(FishLongitude);
        subNonNaN = find(~subNaN);
        subNaN = find(subNaN);
        FishLongitude(subNaN) = interp1(subNonNaN, FishLongitude(subNonNaN), subNaN, 'linear', 'extrap');
    end
    
    if isempty(Tag)
        DataSlices.name = ImageName;
    else
        DataSlices.name = [ImageName '_' Tag];
    end
    DataSlices.from = nomFicExRaw; %'Unkown';
    DataSlices.Date = (T.date)';
    DataSlices.Hour = (T.heure)';
    DataSlices.PingNumber = (1:nbRows)';
    if isempty(Tide) || all(Tide == 0)
        DataSlices.Tide = repmat(MeanTide, size(DataSlices.PingNumber));
    else
        DataSlices.Tide  = Tide;
    end
    
    DataSlices.Heave = Heave;
    
    DataSlices.SliceName{k,1} = SliceName;
    
    % Comment� pour test
    %     DataSlices.LatitudeNadir  = FishLatitude';
    %     DataSlices.LongitudeNadir = FishLongitude';
    
	SliceLatitude  = FishLatitude';
	SliceLongitude = FishLongitude';
    %{
    
    % TODO
    % get(this, 'EchoSounderAcrossAngle')
    
    across = (k-2) * 0;
    along = 0;
    Heading = get(b(k), 'SonarHeading');
    [SliceLatitude, SliceLongitude] = latlonAcrossAlong2latlon(Carto, FishLatitude', FishLongitude', Heading, across, along);
    %}
    
    DataSlices.LatitudeTop(k,:)  = SliceLatitude;
    DataSlices.LongitudeTop(k,:) = SliceLongitude;
    
    % Donn�es optionnelles
    % TODO , corriger les valeurs en fonction du roulis et du tangage
    DataSlices.LatitudeBottom(k,:)  = SliceLatitude;
    DataSlices.LongitudeBottom(k,:) = SliceLongitude;
    
    HeaveInSamples = Heave / deltay; % TODO POURQUOI 0.25 ? Parce que c'est la valeur qui me donnait le meilleur rendu ! TODO TODO TODO !
    DataSlices.DepthTop = MinHeight - 0.25*HeaveInSamples; % TODO  : en attendant que ce soit fait dans le 3DViewer
    DepthBottom         = MaxHeight - 0.25*HeaveInSamples;
    if k == 1
        DataSlices.DepthBottom = DepthBottom;
    else
        DataSlices.DepthBottom = min(DataSlices.DepthBottom, DepthBottom);
    end
    
    if any(isnan(DataSlices.Date))
        fprintf('Date NaN\n');
    end
    if any(isnan(DataSlices.Hour))
        fprintf('Hour NaN\n');
    end
    if any(isnan(DataSlices.DepthTop))
        fprintf('DepthTop NaN\n');
    end
    if any(isnan(DataSlices.DepthBottom))
        fprintf('DepthBottom NaN\n');
    end
end

Reflectivity = my_flipud(Reflectivity); % Modif JMA le 20/02/2022 pour images EK80 multifr�quences

ncFileName = strrep(NomFicXML, '.xml', '.g3d.nc');
DataSlices.Dimensions.nbPings  = nbRows;
DataSlices.Dimensions.nbGroups = size(Reflectivity, 3);
DataSlices.Dimensions.nbSlices = size(Reflectivity, 4);
DataSlices.Dimensions.nbRows   = size(Reflectivity, 1);

DataSlices.DepthTop    = DataSlices.DepthTop    + DataSlices.Tide;
DataSlices.DepthBottom = DataSlices.DepthBottom + DataSlices.Tide;

DataSlices.Reflectivity = Reflectivity;

flag = NetcdfGlobe3DUtils.createGlobeFlyTexture(ncFileName, DataSlices, groupNames, sliceNames, sliceUnits);

%{
FigUtils.createSScFigure;
subplot(4,1,1); PlotUtils.createSScPlot(T, DataSlices.LatitudeTop(:)); grid on;
subplot(4,1,2); PlotUtils.createSScPlot(T, DataSlices.LongitudeTop(:)); grid on;
subplot(4,1,3); PlotUtils.createSScPlot(T, DataSlices.DepthTop(:)); grid on;
subplot(4,1,4); PlotUtils.createSScPlot(T, DataSlices.DepthBottom(:)); grid on;
find(isnan(DataSlices.LatitudeTop(:)))
find(isnan(DataSlices.LongitudeTop(:)))
find(isnan(DataSlices.DepthTop(:)))
find(isnan(DataSlices.DepthBottom(:)))
%}
