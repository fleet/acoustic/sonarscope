function flag = write_NavigationFile(this, nomFicNav)


%% Ecriture des signaux : nom du fichier de navigation
flag = save_string(this, 'Position', 'NavigationFile', nomFicNav);
if ~flag
    return
end

