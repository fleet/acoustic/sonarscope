function flag = write_position(this, DataPosition, varargin)


%% Ecriture des signaux : seuls Longitude; Latitude, Time sont pour l'instant relus depuis le fichier 
% GPS.

DataPosition.Latitude = DataPosition.Latitude(:); % Pour �viter lockage fichier en memmapfile
flag = save_signal(this, 'Latitude', DataPosition.Latitude);
if ~flag
    return
end

DataPosition.Longitude = DataPosition.Longitude(:); % Pour �viter lockage fichier en memmapfile
flag = save_signal(this, 'Longitude', DataPosition.Longitude);
if ~flag
    return
end

DataPosition.Time = DataPosition.Time(:); % Pour �viter lockage fichier en memmapfile
flag = save_signal(this, 'Time', DataPosition.Time);
if ~flag
    return
end

