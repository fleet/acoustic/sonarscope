function [flag, NbSLusOut] = ExRaw_Em2hac_10011(file, ~, iTuple, NbFaisceaux, Data, SeuildB, ~)

flag = 0;

% Tuple 10011 Ping C-32-16 compressed 16 bytes format pour chaque
% faisceau
TupleType           = 10011;
TimeFraction        = Data.DataSample.time_fraction(iTuple);    %0.0000;
TimeCPU             = Data.DataSample.time_cpu(iTuple);         %InfoPing.startdatatime + InfoPing.datatime;
TransceiverMode     = 0;
PingNumber          = Data.DataSample.Ping(iTuple);


for k = 1:NbFaisceaux   %InfoPing.faisceaux
    
    
    % Traitement du paquets d'�chantillons (Ping, Faisceau), traitement de
    % la compression.
%     angles_athwart          = squeeze(Data.DataSample.angles_athwart(k, iTuple, :));
    dataSamples             = squeeze(Data.DataSample.angles_athwart(k, iTuple, :))';  
    detectedRangeBottom     = numel(dataSamples);
    % M�canisme de compression des �chantillons d'angles.
    [flag, N, CompressSamples] = compression10011(dataSamples, TODO);
    
    sub = (CompressSamples > 32767);
    pppp = CompressSamples(sub);
    pppp = -32768+(pppp-32768)-1;
    CompressSamples(sub==1) = pppp;
  
    %% Ecriture de l'ent�te et des �chantillons pour calcul de taille.
    
    fwrite(file.fid, zeros(14,1), 'int16');
    fwrite(file.fid, int16(CompressSamples),'*int16');
    TupleSize       = 26 + 2*N;
    SoftChannelId   = k;
    
    fseek(fid, -28-2*N, 'cof');
    
    %% R��criture de l'ent�te et des �chantillons.
    
    fwrite(file.fid, TupleSize,'uint32');
    fwrite(file.fid, TupleType,'uint16');
    fwrite(file.fid, TimeFraction,'uint16');
    fwrite(file.fid, TimeCPU,'uint32');
    fwrite(file.fid, SoftChannelId,'uint16');
    fwrite(file.fid, TransceiverMode,'uint16');
    fwrite(file.fid, PingNumber,'uint32');
    if detectedRangeBottom==0
        fwrite(file.fid, 2147483647,'int32');
        %  fwrite(file.fid, ((24/cos(WC.angles_athwart(iTuple,k)*pi/180))*1000),'int32');
    else
        pppp = int32(detectedRangeBottom*Data.DataSample.SoundVelocity(k)/(2*Data.DataSample.SampleFrequency(k)))*1000;
        fwrite(file.fid, pppp,'int32');
        %  fwrite(file.fid, ((24/cos(WC.angles_athwart(iTuple,k)*pi/180))*1000),'int32');
        
    end
    
    % Ecriture du nombre d'�chantillons � venir.
    fwrite(file.fid, N,'uint32');
    
    % Avance pour recalage sur la fin du paquet.
    fseek(file.fid, 2*N, 'cof');
    
    % Ecriture de la fin du tuple
    TupleAttribute  = 0;
    Backlink        = TupleSize+10;
    fwrite(file.fid, TupleAttribute,'int32');
    fwrite(file.fid, Backlink,'uint32');
end

NbSLusOut = 0;   


