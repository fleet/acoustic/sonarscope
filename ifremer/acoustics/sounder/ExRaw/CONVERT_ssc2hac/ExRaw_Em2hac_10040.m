function [flag, NbSLusOut] = ExRaw_Em2hac_10040(file, ~, iTuple, NbFaisceaux, Data, SeuildB, ~)

flag = 0;

% Tuple 10040 Ping C-16 compressed 16 bytes format pour chaque
% faisceau
TupleType           = 10040;
TimeFraction        = Data.DataSample.time_fraction(iTuple);    %0.0000;
TimeCPU             = Data.DataSample.time_cpu(iTuple);         %InfoPing.startdatatime + InfoPing.datatime;
TransceiverMode     = 0;
PingNumber          = Data.DataSample.Ping(iTuple);


for k = 1:NbFaisceaux   %InfoPing.faisceaux
    
    PulseLength         = Data.DataSample.PulseDuration(k);
    BeamWidth.along     = Data.Config.BeamwidthAlongShip(k);
    BeamWidth.athwart   = Data.Config.BeamwidthAthwartShip(k);
    
    % Traitement du paquets d'�chantillons (Ping, Faisceau), traitement de
    % la compression.
    angles_athwart          = squeeze(Data.DataSample.angles_athwart(k, iTuple, :));
    dataSamples             = squeeze(Data.DataSample.Samples(k, iTuple, :))';  
    detectedRangeBottom     = numel(dataSamples);
    % Pour garder l'homog�n�it� du calcul de compression, on traite le signal brut
    % par analogie avec le traitement ALL.
    [flag, N, CompressSamples] = ExRaw_compression10040(dataSamples, ...
                                Data.DataSample.SampleFrequency(k), Data.DataSample.SoundVelocity(k), ...
                                Data.DataSample.TVGFunctionApplied(k), BeamWidth, angles_athwart, ...
                                PulseLength, SeuildB);
    
    % Calcul de compl�ment � deux pour les valeurs d�passant 32767
    % for s=1:numel(CompressSamples)
    %     if CompressSamples(s) > 32767
    %         CompressSamples(s) = twos_comp(double(CompressSamples(s)), 16);
    %         CompressSamples(s) = CompressSamples(s) - 1;
    %     end
    % 
    % end

    sub = (CompressSamples > 32767);
    pppp = CompressSamples(sub);
    pppp = -32768+(pppp-32768)-1;
    CompressSamples(sub==1) = pppp;
  
    
    %% Ecriture de l'ent�te et des �chantillons pour calcul de taille.
% % % %     fwrite(file.fid, zeros(14,1), 'int16');
    % Ecriture dans le fichier HAC.
% % % %     fwrite(file.fid, int16(CompressSamples),'*int16');
    TupleSize       = 26 + 2*N;
    SoftChannelId   = k;
    
% % % %     fseek(file.fid, -28-2*N, 'cof');
    
    %% R��criture de l'ent�te et des �chantillons.
    
    fwrite(file.fid, TupleSize,         'uint32');
    fwrite(file.fid, TupleType,         'uint16');
    fwrite(file.fid, TimeFraction,      'uint16');
    fwrite(file.fid, TimeCPU,           'uint32');
    fwrite(file.fid, SoftChannelId,     'uint16');
    fwrite(file.fid, TransceiverMode,   'uint16');
    fwrite(file.fid, PingNumber,        'uint32');
    if detectedRangeBottom==0
        fwrite(file.fid, 2147483647,'int32');
        %  fwrite(file.fid, ((24/cos(WC.angles_athwart(iTuple,k)*pi/180))*1000),'int32');
    else
        pppp = int32(detectedRangeBottom*Data.DataSample.SoundVelocity(k)/(2*Data.DataSample.SampleFrequency(k)))*1000;
        fwrite(file.fid, pppp,'int32');
        %  fwrite(file.fid, ((24/cos(WC.angles_athwart(iTuple,k)*pi/180))*1000),'int32');
        
    end
    
    % Ecriture du nombre d'�chantillons � venir.
    fwrite(file.fid, N,'uint32');
    
    fwrite(file.fid, int16(CompressSamples),'*int16');
% % % %     % Avance pour recalage sur la fin du paquet.
% % % %     fseek(file.fid, 2*N, 'cof');
    
    % Ecriture de la fin du tuple
    TupleAttribute  = 0;
    Backlink        = TupleSize+10;
    fwrite(file.fid, TupleAttribute,'int32');
    fwrite(file.fid, Backlink,'uint32');
end

NbSLusOut = 0;   


