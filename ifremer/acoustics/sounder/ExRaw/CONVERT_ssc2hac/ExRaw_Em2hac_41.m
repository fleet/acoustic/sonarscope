function flag = ExRaw_Em2hac_41(file, iTuple, NbFaisceaux, Data)

persistent TuplePrev;
if (iTuple == 1)
    TuplePrev = [];
end


flag = 0;


% Allocation de m�moire.
TupleCrt(NbFaisceaux) = struct( 'AttitudeSensorId', [], ...
    'PlatformType', [], ...
    'AlongOffset', [], ...
    'AthwartOffset', [], ...
    'ElevationOffset', [], ...
    'Remarks', [], ...
    'Space', [], ...
    'TupleAttribute', [], ...
    'Backlink', [] );

for k = 1:NbFaisceaux
    % Tuple 41 attitude sondeur pour chaque faisceau
    TupleSize                   = 54;
    TupleType                   = 41;
    TimeFraction                = Data.Position.time_fraction(iTuple); %0.0000;
    TimeCPU                     = Data.Position.time_cpu(iTuple);      %InfoPing.startdatatime + InfoPing.datatime;
    % Enregistrement des informations utiles du Tuple pour comparaison
    % avec le Ping pr�c�dent.
    TupleCrt(k).AttitudeSensorId    = 1;
    TupleCrt(k).PlatformType        = 0;
    TupleCrt(k).AlongOffset         = Data.Config.AlongshipOffsetRelToAttSensor(k)*100; % Motion sensor 1 along location in m
    TupleCrt(k).AthwartOffset       = Data.Config.AthwartshipOffsetRelToAttSensor(k)*100; % Motion sensor 1 athwart location in m
    TupleCrt(k).ElevationOffset     = Data.Config.VerticalOffsetRelToAttSensor(k)*100; % Motion sensor 1 vertical location in m
% %     TupleCrt(k).Remarks             = blanks(30);
    TupleCrt(k).Remarks             = '123456789012345678901234567890';
    TupleCrt(k).Space               = 0;
    TupleCrt(k).TupleAttribute      = 0;
    TupleCrt(k).Backlink            = 64;
    
    % Ecriture des tuples que si les donn�es diff�rent du datagramme
    % pr�c�dent.
    if ~isempty(TuplePrev)
        if ~isequal(TuplePrev(k), TupleCrt(k))
            TupleCrt(k).TransceiverChannelNumber = k;
            fwrite(file.fid, TupleSize,'uint32');
            fwrite(file.fid, TupleType,'uint16');
            fwrite(file.fid, TimeFraction,'uint16');
            fwrite(file.fid, TimeCPU,'uint32');
            fwrite(file.fid, TupleCrt(k).AttitudeSensorId,'uint16');
            fwrite(file.fid, TupleCrt(k).TransceiverChannelNumber,'uint16');
            fwrite(file.fid, TupleCrt(k).PlatformType,'uint16');
            fwrite(file.fid, TupleCrt(k).AlongOffset,'int16');
            fwrite(file.fid, TupleCrt(k).AthwartOffset,'int16');
            fwrite(file.fid, TupleCrt(k).ElevationOffset,'int16');
            fwrite(file.fid, TupleCrt(k).Remarks,'char');
            fwrite(file.fid, TupleCrt(k).Space,'uint16');
            fwrite(file.fid, TupleCrt(k).TupleAttribute,'int32');
            fwrite(file.fid, TupleCrt(k).Backlink,'uint32');
        end
    else
        TupleCrt(k).TransceiverChannelNumber = k;
        fwrite(file.fid, TupleSize,'uint32');
        fwrite(file.fid, TupleType,'uint16');
        fwrite(file.fid, TimeFraction,'uint16');
        fwrite(file.fid, TimeCPU,'uint32');
        fwrite(file.fid, TupleCrt(k).AttitudeSensorId,'uint16');
        fwrite(file.fid, TupleCrt(k).TransceiverChannelNumber,'uint16');
        fwrite(file.fid, TupleCrt(k).PlatformType,'uint16');
        fwrite(file.fid, TupleCrt(k).AlongOffset,'int16');
        fwrite(file.fid, TupleCrt(k).AthwartOffset,'int16');
        fwrite(file.fid, TupleCrt(k).ElevationOffset,'int16');
        fwrite(file.fid, TupleCrt(k).Remarks,'char');
        fwrite(file.fid, TupleCrt(k).Space,'uint16');
        fwrite(file.fid, TupleCrt(k).TupleAttribute,'int32');
        fwrite(file.fid, TupleCrt(k).Backlink,'uint32');
        
    end
end
% Conservation des donn�es dans une structure
TuplePrev = TupleCrt;