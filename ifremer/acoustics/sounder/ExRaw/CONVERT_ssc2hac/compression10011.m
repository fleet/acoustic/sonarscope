% Compression d'un paquet d'�chantillons de phase d'un faisceau d'un ping.
%
% Syntax
%   compression10011(nomFic, ADU)
% 
% Input Arguments
%   ADU
% 
% Name-Value Pair Arguments
%  ADU
%
% Remarks : Ce fichier calcule les paquets d'�chantillons li�s � un Ping
% avec compression inh�rente au paquet 10011.
%
% Examples
%   TODO
%
% See also S�rie Em2hac Authors
% Authors : GLU
% -------------------------------------------------------------------------

function [flag, NbSamples, Samples] = compression10011(DataSamples, seuilDeg)

flag            = 0;
flagComputeTVG  = 0;
% Facteur d'�chelle = 1/2 dB
% DataSamples = DataSamples;

% Traitement des samples.
NbSampThres = NaN(numel(DataSamples), 1);
nbBlanc     = 0;
NbSamples   = 0;

for j = 1:numel(DataSamples)
    if (DataSamples(j) <= seuilDeg)%|| SampleValue<(max(WC.amplitudes(index,:,j))-LS))
        nbBlanc = nbBlanc+1;
    else
        if nbBlanc > 0
            NbSampThres(j-1) = nbBlanc;
            NbSamples        = NbSamples+1;
            nbBlanc          = 0;
        end
        NbSamples = NbSamples+1;
    end
end
% Gestion de la sortie de boucle - cas des NaN en fin de paquets.
if nbBlanc > 0
    NbSampThres(j+1) = nbBlanc;
    NbSamples        = NbSamples+1;
end

%% Calcul de la formule des �chos sous forme vectorielle.

sub     = (DataSamples <= seuilDeg); 
X       = DataSamples;
X(sub)  = NaN;


SampSupThres = X ;

% Trancodage en hexa, y compris pour une valeur n�gative.
pppp                = SampSupThres(~sub);
%  Voir http://www.mathworks.com/matlabcentral/newsreader/view_thread/138885
% pppp                = int16(typecast(bitand(uint16(typecast(int16(pppp),'uint16')),32767),'int16'));
pppp                = typecast(bitand(typecast(int16(pppp), 'uint16'), 32767), 'int16');
SampSupThres(~sub)  = pppp;

%% Calcul de masque pour la compression.

% Trancodage en hexa, y compris pour une valeur n�gative.
sub  = ~isnan(NbSampThres);
if ~all(sub == 0)
    pppp                = NbSampThres(sub);
    numSamplesLastNaN   = pppp(end);
    pppp                = typecast(int16(pppp),'uint16');
    NbThresMask         = bitor(pppp, 32768);
end

%% Merge des deux matrices en supprimant les NaN devenus inutiles.

% R�cup�ration des indices o� la valeur (> Seuil) est rest�e � NaN
sub3    = find(isnan(SampSupThres));
if ~isempty(sub3)
    % Calcul des paquets de NaN (Ecart entre samples > 1). 
    % Attention : la diff�rence supprime le dernier intervalle.
    sub4    = diff(sub3)~=1;
    % R�cup�ration des indices avec ajout du dernier intervalle.
    sub5    = [sub3(sub4) find(isnan(SampSupThres), 1, 'last')-(numSamplesLastNaN-1)];
    % Merge des deux matrices (masquage pr�calcul�).
    SampSupThres(sub5) = NbThresMask;
end

% On ne retient que les valeurs signficatives.
sub     = ~isnan(SampSupThres);
Samples = SampSupThres(sub);

% Ecriture d'une valeur de Padding (en l'occurence = valeur de d�passement du seuil).
if (NbSamples/2 - round(NbSamples/2)) ~= 0
    pppp      = 32768;
    Samples   = [Samples pppp];
    NbSamples = NbSamples+1;
end

