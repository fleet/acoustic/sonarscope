% Recopie par correspondance vers structure de type Config  depuis les datagrammes
% RAW.
%
% Syntax
%   ConfigData = loadConfigDataFromBinFile(ConfigH, ConfigT)
%
% Input Arguments
%   ConfigData = loadConfigDataFromBinFile(ConfigH, ConfigT);
%
% OUTPUT_PARAMETERS : 
%   TODO
%   
% Examples
%   ADU
%
% See also ssc2hac Authors
% Authors : GLU
%-----------------------------------------------------------------------
function [flag, Config] = loadConfigDataFromBinFile(ConfigHeader, ConfigTransducer)

flag = 0;

nbChannels = ConfigTransducer.Dimensions.nbChannels;
Config.TransceiverSoftwareVersion       = ConfigHeader.TransceiverSoftwareVersion;

Config.AlongshipOffsetRelToAttSensor    = ConfigTransducer.PosX;
Config.AthwartshipOffsetRelToAttSensor  = ConfigTransducer.PosY;
Config.VerticalOffsetRelToAttSensor     = ConfigTransducer.PosZ;
Config.AngleOffsetAlongship             = ConfigTransducer.AngleOffsetAlongShip*1e4;           % en 0.0001deg
Config.AngleOffsetAthwartship           = ConfigTransducer.AngleOffsetAthwartShip*1e4;         % en 0.0001deg
Config.AngleSensitivityAlongship        = ConfigTransducer.AngleSensitivityAlongShip*1e4;      % en 0.0001El./mec.deg
Config.AngleSensitivityAthwartship      = ConfigTransducer.AngleSensitivityAthwartShip*1e4;    % en 0.0001El./mec.deg

Config.EquivalentBeamAngle              = ConfigTransducer.EquivalentBeamAngle*1e4;            % en 0.0001deg
Config.BeamwidthAlongShip               = ConfigTransducer.BeamwidthAlongShip*1e4;             % en 0.0001deg
Config.BeamwidthAthwartShip             = ConfigTransducer.BeamwidthAthwartShip*1e4;           % en 0.0001deg

Config.FrequencyChannelName             = ConfigTransducer.ChannelIdentification;

for k=1:nbChannels
    % Traitement pour récupération du nom du sondeur alors que
    % ChannelIdentification est de la forme : 
    % GPT 120 kHz 009072055b9a 1 ES120-7D (complété de N caractères Null pour
    % faire 128 char).
    pppp                    = regexp(Config.FrequencyChannelName{k}, ' ', 'split');
    sub                     = pppp{end} == 0;
    pppp{end}(sub)          = [];
    Config.TransducerName{k}   = pppp{end};
end
flag = 1;
