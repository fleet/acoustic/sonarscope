% Recopie par correspondance vers structure pour l'export HAC depuis les datagrammes
% ExRaw.
%
% Syntax
%   PosNavAtt = loadPosNavAttDataFromBinFile(PosBinData)
%
% Input Arguments
%   PosBinData : donn�es de datagramms de positionnement, d'attitude et de navigation4
%                depuis les fichiers RAW.
%
% Output Arguments
%   PosNavAtt :  structure de sortie des �chantillons pr�t � former un(des)
%                tuple(s).
%
% Examples
%   ADU
%
% See also ssc2hac Authors
% Authors : GLU
%-----------------------------------------------------------------------

function [flag, Position, Attitude] = loadPosNavAttDataFromBinFile(PosBinData)

% flag = 0;
Position.Ping  = 1:size(PosBinData.Time,1);
Time = PosBinData.Time.timeMat;

% D�composition du time.
Position.time_cpu      = floor((Time-datenum('19700101','yyyymmdd'))*86400);
Position.time_fraction = ((Time-datenum('19700101','yyyymmdd'))*86400-Position.time_cpu)*10000;

Position.latitude      = PosBinData.Latitude*1e6;      % en 1e-6 deg
Position.longitude     = PosBinData.Longitude*1e6;     % en 1e-6 deg
Position.speed         = PosBinData.Speed*10;          % en 0.1 m/s
Position.heading       = PosBinData.Heading;           % en 1e-6 deg

Attitude.roll          = PosBinData.Roll*10;           % en 1/10 deg
Attitude.pitch         = PosBinData.Pitch*10;          % en 1/10 deg
Attitude.heave         = PosBinData.Heave*10;          % en 1/10 m
Attitude.heading       = PosBinData.Heading*10;        % en 1/10 deg

flag = 1;
