% Recopie par correspondance vers structure de type HAC Tuple depuis les datagrammes
% ExRaw.
%
% Syntax
%   SampleRaw = loadSampleTupleDataFromBinFile(SampleRawBinData)
%
% Input Arguments
%   SampleRawBinData : donn�es de datagramms de Water Column depuis les fichiers ExRaw.
%
% Output Arguments
%   SampleTuple : structure de sortie des �chantillons pr�t � former un
%   tuple.
%
% Examples
%   ADU
%
% See also ssc2hac Authors
%-----------------------------------------------------------------------

function [flag, SampleTuple] = loadSampleDataFromBinFile(Time, Config, DataSample)

flag = 0;

% NbPings             = DataSample.Dimensions.nbPings;
NbFaisceaux         = DataSample.Dimensions.nbChannels;
nbChannels          = NbFaisceaux;

SampleTuple.Ping    = 1:DataSample.Dimensions.nbPings;
Time                = Time.timeMat;   

% D�composition du time.
SampleTuple.time_cpu=floor((Time-datenum('19700101','yyyymmdd'))*86400);
SampleTuple.time_fraction=((Time-datenum('19700101','yyyymmdd'))*86400-SampleTuple.time_cpu)*10000;

for iChannel=1:nbChannels
    freq        = DataSample.AcousticFrequency(iChannel);
    
    % Rechargement des signaux (=vecteurs).
    % -------------------------
    SampleTuple.AcousticFrequency(iChannel)     = DataSample.AcousticFrequency(iChannel);
    SampleTuple.AbsorptionCoefficient(iChannel) = DataSample.AbsorptionCoefficient(iChannel)*1e7;   %0.0001 dB/km
    SampleTuple.PulseDuration(iChannel)         = DataSample.PulseDuration(iChannel)*1e6;           % en us
    SampleTuple.SoundVelocity(iChannel)         = DataSample.SoundVelocity(iChannel);
    SampleTuple.Bandwidth(iChannel)             = DataSample.Bandwidth(iChannel);
    SampleTuple.TransmitPower(iChannel)         = DataSample.TransmitPower(iChannel);
    SampleTuple.TimeSampleInterval(iChannel)    = DataSample.SampleInterval(iChannel)*1e6;          % en us
    SampleTuple.SampleFrequency(iChannel)       = DataSample.SampleFrequency(iChannel);
    SampleTuple.Gain(iChannel)                  = DataSample.Gain(iChannel)*1e4;                    % en 10e-4 dB  
    SampleTuple.SaCorrection(iChannel)          = DataSample.SaCorrection(iChannel)*1e4;            % en 10e-4 dB
    SampleTuple.TransducerDepth(iChannel)       = DataSample.TransducerDepth(iChannel)*1e4;         % en 10e-4 dB
    

    % Rechargement des images par channel.
    % ------------------------------------
    
    SampleTuple.angles_along(iChannel,:, :)     = DataSample.(['AlongShip_' num2str(freq/1000) 'kHz']) * Config.AngleSensitivityAlongShip(iChannel) * 10;       % en 0.1 deg
    SampleTuple.angles_athwart(iChannel,:,:)    = DataSample.(['AthwartShip_' num2str(freq/1000) 'kHz']) * Config.AngleSensitivityAthwartShip(iChannel) * 10;  % en 0.1 deg
    SampleTuple.Samples(iChannel,:,:)           = DataSample.(['Sv_' num2str(freq/1000) 'kHz']) * 100; % en 0.01dB

    % Calcul de TVG
    % -------------
    
    nbSamples                               =  DataSample.NbSamplesByChannel(:,iChannel);   
    samplespace                             = DataSample.SoundVelocity(iChannel).*DataSample.SampleInterval(iChannel)/2;
    
    svcenterrange                           = DataSample.SoundVelocity(iChannel).*SampleTuple.PulseDuration(iChannel)/4;
    svrange                                 = nbSamples(1)-1'*samplespace - svcenterrange;
    svtvgrange                              = svrange;
    svtvgrange(svtvgrange<samplespace)      = samplespace;
    SampleTuple.TVGFunctionApplied(iChannel, :) = 20*log10(svtvgrange) + 2*DataSample.AbsorptionCoefficient(iChannel)*svtvgrange;
    
end


