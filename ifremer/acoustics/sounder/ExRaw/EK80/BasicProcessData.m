function [BasicProcessedData] = BasicProcessData(RawData,varargin)
%BASICPROCESSDATA Perform basic processing of EK80 raw data.
%
% CALL: [BasicProcessedData] = BasicProcessData(RawData)
%
% Inputs:
%   RawData            = 
%   varagin            = index of channel to process (all of them are processed if omitted)
%   
% Outputs:
%   BasicProcessedData = 
%
% Description:
%
%
%
% Examples(s):
%   [BasicProcessedData] = BasicProcessData(RawData)
%

% References:
%
%
% Created by Lars Nonboe Andersen
%
%
%
% Copyright (c) 2015 Kongsberg Maritime
%
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
% IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
% FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
% AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
% LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
% OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
% THE SOFTWARE.
% ---------------------------------------------------------------------------

narginchk(1,2);
nargoutchk(0,1);

nFiles = length(RawData);

for fileNumber = 1:nFiles
    FileData    = RawData(fileNumber).Data;
    PingData    = FileData.PingData;
    nChannels   = size(PingData,1);
    nPings      = size(PingData,2);
    
    if nargin>1
        if varargin{1}> nChannels
            disp('ichannel > nChannels in BasicProcessData');
            break;
        else
            ichannel0=varargin{1};
            ichannel1=varargin{1};
        end
    else
        ichannel0=1;
        ichannel1=nChannels;
    end
    
    for channelNumber = ichannel0:ichannel1
        ChannelListPing=arrayfun(@(x) length(x.time),PingData(channelNumber,:));
        channelId=PingData(channelNumber,min(find(ChannelListPing))).SampleData.channelId;
        %ChannelData = GetChannelData(FileData.ConfigurationData,channelNumber); % 02/16
        ChannelData = GetChannelData(FileData.ConfigurationData,channelId); 
        %ChannelData = GetChannelDataToBeChanged(FileData.ConfigurationData,channelNumber); % 02/16 
        ChannelData.FilterData = FileData.FilterData(channelNumber,:);
        
        ProcessedSampleDataVector = struct('range',[],'power',[],'alongship',[],'athwartship',[],'spRange',[],'sp',[],'svRange',[],'sv',[]);
        %ProcessedSampleDataVector = struct('range',[],'power',[],'alongship',[],'athwartship',[],'spRange',[],'sp',[],'svRange',[],'sv',[],'ampCpx',[]);
        
        if strcmp(FileData.ConfigurationData.Transceivers(ChannelData.TransceiverNumber).TransceiverType,'GPT')
            wbtImpedanceRx=nan;
        else
            if isfield(FileData.ConfigurationData.Transceivers(ChannelData.TransceiverNumber),'Impedance')
                wbtImpedanceRx=FileData.ConfigurationData.Transceivers(ChannelData.TransceiverNumber).Impedance;
            else
                wbtImpedanceRx=5400;
            end
        end
        
        for pingNumber = 1:nPings
            pingNumber;
                [ProcessedSampleData,effectivePulselLength] = EstimateProcessedSampleData(ChannelData,PingData(channelNumber,pingNumber),wbtImpedanceRx); %ajout effectivePulseLength & Impedance nlb
                if (~isnan(effectivePulselLength)) % cas ping sequence
                    ChannelData.Transducer.effectivePulselLength=effectivePulselLength; 
                end
                ProcessedSampleDataVector(pingNumber) = ProcessedSampleData;
        end
        
        BasicProcessedData(fileNumber,channelNumber-ichannel0+1).ChannelData            = ChannelData;
        BasicProcessedData(fileNumber,channelNumber-ichannel0+1).ProcessedSampleData    = ProcessedSampleDataVector;

    end
    disp(['Finished processing file ' int2str(fileNumber) ' of ' int2str(nFiles)]);
end

end


%function ChannelData = GetChannelData(ConfigurationData,channelNumber)% 01/2021
function ChannelData = GetChannelData(ConfigurationData,channelId)

Transceivers = ConfigurationData.Transceivers;
nTransceivers = length(Transceivers);
for transceiverNumber = 1:nTransceivers
    CurrentTransceiver = Transceivers(transceiverNumber);
    ChannelsInTransceiver = CurrentTransceiver.Channels;
    nChannelsInTransceiver = length(ChannelsInTransceiver);
    for channelInTransceiverNumber = 1:nChannelsInTransceiver
        CurrentChannelInTransceiver = ChannelsInTransceiver(channelInTransceiverNumber);
        %if (str2num(CurrentChannelInTransceiver.ChannelNumber) == channelNumber) % 02/16 no field ChannelNumber in CurrentChannelInTransceiver, see GetChannelDataToBeChanged
        if strcmp(deblank(CurrentChannelInTransceiver.ChannelID),deblank(channelId)) % 01/2021
            ChannelData = CurrentChannelInTransceiver;
            ChannelData.TransceiverType = CurrentTransceiver.TransceiverType;
            ChannelData.TransceiverNumber = transceiverNumber; % 01/2021
        end
    end
end

end
    
function ChannelData = GetChannelDataToBeChanged(ConfigurationData,channelNumber)
%probably wrong routine (channelNumber = iTransceiver and just one channel per transceiver), but ok on 02/2016 data
Transceivers = ConfigurationData.Transceivers;
nTransceivers = length(Transceivers);
CurrentTransceiver = Transceivers(channelNumber);
ChannelsInTransceiver = CurrentTransceiver.Channels;
ChannelData = ChannelsInTransceiver(1);
ChannelData.TransceiverType = CurrentTransceiver.TransceiverType;

end
    

    
    
    
    