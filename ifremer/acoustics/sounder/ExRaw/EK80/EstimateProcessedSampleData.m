function [ProcessedSampleData,effectivePulselLength] = EstimateProcessedSampleData(ChannelData,PingData,wbtImpedanceRx)  %ajout effectivePulselLength & impedance nlb
%ESTIMATEPROCESSEDSAMPLEDATA
%
% CALL: [ProcessedSampleData] = EstimateProcessedSampleData(ChannelData,PingData)
%
% Inputs:
%   ChannelData     =
%   PingData        =
%
% Outputs:
%   ProcessedSampleData =
%
% Description:
%
%
%
% Examples(s):
%   [ProcessedSampleData] = EstimateProcessedSampleData(ChannelData,PingData)
%

% References:
%
%
% Created by Lars Nonboe Andersen
%
%
%
% Copyright (c) 2015 Kongsberg Maritime
%
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
% IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
% FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
% AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
% LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
% OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
% THE SOFTWARE.
% ---------------------------------------------------------------------------

narginchk(3,3);
%nargoutchk(0,1);
nargoutchk(0,2);%ajout effectivePulselLength nlb

ProcessedSampleData = struct('range',[],'power',[],'alongship',[],'athwartship',[],'spRange',[],'sp',[],'svRange',[],'sv',[]);

EnvironmentData = PingData.EnvironmentData;
ParameterData   = PingData.ParameterData;
SampleData      = PingData.SampleData;

effectivePulselLength = nan;
if (~isempty(SampleData))
    
    % Power and Angle
    if strcmp(ChannelData.TransceiverType,'GPT') | isfield(SampleData,'power')
        % GPT
        
        % Estimate power and angle
        %PowerAngleData.power        = SampleData.power;
        PowerAngleData.power        = 10.^(SampleData.power/10); %change nlb for WBT consistency
        if isfield(SampleData, 'angleAlongship')
            PowerAngleData.alongship    = SampleData.angleAlongship*180/128; %change nlb PowerAngleData.alongship    = SampleData.alongship
            PowerAngleData.athwartship  = SampleData.angleAthwartship*180/128;%change nlb PowerAngleData.alongship    = SampleData.athwartship
        end
    else
        % WBT
        
        nominalTransducerImpedance  = 75;
        %wbtImpedanceRx              = 5e3; %nlb / lu dans le raw
        
        % Perform pulse compression if FM
        if (~ParameterData.pulseForm)
            % CW
            complexSamples = SampleData.complexSamples;
            PingData.ParameterData.frequencyStart=PingData.ParameterData.frequencyUsed; %nlb
            PingData.ParameterData.frequencyStop=PingData.ParameterData.frequencyUsed; %nlb
            transmitSignal = CreateTransmitSignal(PingData);
        else
            % FM
            transmitSignal = CreateTransmitSignal(PingData);
            pulseCompressedData = conv2(flipud(conj(transmitSignal)),1,PingData.SampleData.complexSamples)/norm(transmitSignal)^2;
            pulseCompressedData = pulseCompressedData(length(transmitSignal):end,:);
            
            complexSamples  = pulseCompressedData;
        end
        
        % Estimate power and angle
        nSectors = size(SampleData.complexSamples,2);
        PowerAngleData.power = nSectors*(abs(sum(complexSamples,2)/nSectors)/(2*sqrt(2))).^2 * ((wbtImpedanceRx+nominalTransducerImpedance)/wbtImpedanceRx)^2 * 1/nominalTransducerImpedance;
        
                   
        if (nSectors==4)
            
            if strcmp(ChannelData.Transducer.TransducerName,'ES38-7')
            SEC1=complexSamples(:,1)+complexSamples(:,4);
            SEC2=complexSamples(:,2)+complexSamples(:,4);
            SEC3=complexSamples(:,3)+complexSamples(:,4);
            
            PowerAngleData.alongship    = angle( SEC3.*conj(SEC1)) *180/pi;
            PowerAngleData.athwartship  = angle( SEC3.*conj(SEC2)) *180/pi;
            else
            complexFore         = sum(complexSamples(:,3:4),2)/2;
            complexAft          = sum(complexSamples(:,1:2),2)/2;
            complexStarboard    = (complexSamples(:,1) + complexSamples(:,4))/2;
            complexPort         = sum(complexSamples(:,2:3),2)/2;
            
            PowerAngleData.alongship    = angle( complexFore.*conj(complexAft)) *180/pi;
            PowerAngleData.athwartship  = angle( complexStarboard.*conj(complexPort)) *180/pi;
            end
        elseif (nSectors==3)
            PowerAngleData.alongship    = angle( complexSamples(:,3).*conj(complexSamples(:,1))) *180/pi;
            PowerAngleData.athwartship  = angle( complexSamples(:,3).*conj(complexSamples(:,2))) *180/pi;
            
            
        elseif (nSectors==1)
            % Single beam
            PowerAngleData.alongship    = 0*complexSamples;
            PowerAngleData.athwartship  = 0*complexSamples;
        else
            error('Sector configuration not supported')
        end
        
    end
    
    % Sp and Sv
    
    % Estimate effective pulse duration
    if strcmp(ChannelData.TransceiverType,'GPT')| isfield(SampleData,'power')
        % GPT
        
        effectivePulselLength = ParameterData.pulseLength; % nlb: SaCorr added after
    else
        %WBT
        
        if (~PingData.ParameterData.pulseForm)
            % CW
            transmitSignalPower = abs(transmitSignal).^2;
            effectivePulselLength  = ParameterData.sampleInterval * sum(transmitSignalPower) / max(transmitSignalPower); % nlb: SaCorr added after
        else
            autoCorrelationTransmitSignal = conv(transmitSignal,flipud(conj(transmitSignal)))/norm(transmitSignal)^2;
            autoCorrelationTransmitSignalPower = (abs(autoCorrelationTransmitSignal).^2);
            effectivePulselLength  = ParameterData.sampleInterval * sum(autoCorrelationTransmitSignalPower) / max(autoCorrelationTransmitSignalPower);
        end
    end
    
    nSamples = length(PowerAngleData.power);
    rangeVector = (0:nSamples-1)'*ParameterData.sampleInterval*EnvironmentData.soundSpeed/2;
    
    absorptionCoefficients = EstimateAbsorptionCoefficients(EnvironmentData,PingData.ParameterData.frequencyUsed);
    
    % Do not apply TVG before transmit pulse has finished and range > 1 m
    tvgDelay = ParameterData.pulseLength * EnvironmentData.soundSpeed/2;
    startTvg = max(tvgDelay, 1);
    tvg20 = zeros(length(rangeVector), 1);
    tvg40 = zeros(length(rangeVector), 1);
    if (~PingData.ParameterData.pulseForm)
        % CW
        tvg20RangeVector = rangeVector - EnvironmentData.soundSpeed*ParameterData.pulseLength/4;
    else
        % FM
        tvg20RangeVector = rangeVector;
    end
    
    for index = 1:length(rangeVector)
        if(rangeVector(index) > startTvg)
            tvg20(index) = 20*log10(tvg20RangeVector(index)) + 2*absorptionCoefficients*tvg20RangeVector(index);
            tvg40(index) = 40*log10(rangeVector(index)) + 2*absorptionCoefficients*rangeVector(index);
        else
            tvg20(index) = 0;
            tvg40(index) = 0;
        end
    end
    
    if (ParameterData.fileFormatVersion<1.2)
        pulseLengthTable = str2num(ChannelData.PulseLength);
    else
        if ( strcmp(ChannelData.TransceiverType,'GPT'))| isfield(SampleData,'power')
            pulseLengthTable = str2num(ChannelData.PulseDuration);
        elseif ( strcmp(ChannelData.TransceiverType,'WBT')||strcmp(ChannelData.TransceiverType,'WBT Mini'))
            if (~PingData.ParameterData.pulseForm)
                % CW
                pulseLengthTable = str2num(ChannelData.PulseDuration);
            else
                %FM
                pulseLengthTable = str2num(ChannelData.PulseDurationFM);
            end
        end
    end
    
    gainTable       = str2num(ChannelData.Transducer.Gain);
    SaCorrTable       = str2num(ChannelData.Transducer.SaCorrection);
    if (~PingData.ParameterData.pulseForm)
        % CW
        pulseLengthIndex = dsearchn(pulseLengthTable,ParameterData.pulseLength);
        gain = gainTable(pulseLengthIndex);
        % ajout nlb : correction du gain pour un CW d�centr�, qui est
        % d�-corrig� ensuite pour sv/sp (supposition: le gain lu est bien celui du CW �
        % appliquer, ou celui tsd si FM)
        if PingData.ParameterData.frequencyUsed~=str2num(ChannelData.Transducer.Frequency)
            gain = gain - 20*log10(PingData.ParameterData.frequencyUsed/str2num(ChannelData.Transducer.Frequency));
        end
        % ajout nlb : calibrated effective pulse length
        effectivePulselLength=10.^((10*log10(effectivePulselLength)+2*SaCorrTable(pulseLengthIndex))/10);
        
    else
        % FM
        gain = gainTable(end);
    end
    
    % Sp
    sp = 10*log10(PowerAngleData.power)  + tvg40 - 10*log10(ParameterData.transmitPower*(EnvironmentData.soundSpeed/PingData.ParameterData.frequencyUsed)^2/(16*pi^2)) - 2*(gain + 20*log10(PingData.ParameterData.frequencyUsed/str2num(ChannelData.Transducer.Frequency)));    
    %sp = 10*log10(PowerAngleData.power)  + tvg40 - 10*log10(ParameterData.transmitPower*(EnvironmentData.soundSpeed/PingData.ParameterData.frequencyUsed)^2/(16*pi^2)) - 2*(gain + 0);
    
    % Sv
    sv = 10*log10(PowerAngleData.power)  + tvg20 - 10*log10(ParameterData.transmitPower*(EnvironmentData.soundSpeed/PingData.ParameterData.frequencyUsed)^2*EnvironmentData.soundSpeed/(32*pi^2)) - 2*(gain + 20*log10(PingData.ParameterData.frequencyUsed/str2num(ChannelData.Transducer.Frequency))) - 10*log10(effectivePulselLength) - (str2num(ChannelData.Transducer.EquivalentBeamAngle) + 20*log10(str2num(ChannelData.Transducer.Frequency)/PingData.ParameterData.frequencyUsed));
    %sv = 10*log10(PowerAngleData.power)  + tvg20 - 10*log10(ParameterData.transmitPower*(EnvironmentData.soundSpeed/PingData.ParameterData.frequencyUsed)^2*EnvironmentData.soundSpeed/(32*pi^2)) - 2*(gain + 0) - 10*log10(effectivePulselLength) - (str2num(ChannelData.Transducer.EquivalentBeamAngle) + 0);
    
    
    % Create ProcessedSampleData structure
    ProcessedSampleData.range           = rangeVector;
    ProcessedSampleData.power           = PowerAngleData.power;
    %if isfield(SampleData,'alongship')
        ProcessedSampleData.alongship       = PowerAngleData.alongship  / (str2num(ChannelData.Transducer.AngleSensitivityAlongship) * PingData.ParameterData.frequencyUsed/str2num(ChannelData.Transducer.Frequency));
        ProcessedSampleData.athwartship     = PowerAngleData.athwartship  / (str2num(ChannelData.Transducer.AngleSensitivityAthwartship) * PingData.ParameterData.frequencyUsed/str2num(ChannelData.Transducer.Frequency));
    %end
    if ~isfield(SampleData,'power')
        if (nSectors==3)
            ProcessedSampleData.alongship       = (PowerAngleData.alongship+PowerAngleData.athwartship/sqrt(3))  / (str2num(ChannelData.Transducer.AngleSensitivityAlongship) * PingData.ParameterData.frequencyUsed/str2num(ChannelData.Transducer.Frequency));
            ProcessedSampleData.athwartship     = (PowerAngleData.athwartship-PowerAngleData.alongship)  / (str2num(ChannelData.Transducer.AngleSensitivityAthwartship) * PingData.ParameterData.frequencyUsed/str2num(ChannelData.Transducer.Frequency));
        end
    end
    ProcessedSampleData.spRange         = rangeVector;
    ProcessedSampleData.sp              = sp;
    ProcessedSampleData.svRange         = tvg20RangeVector;
    ProcessedSampleData.sv              = sv;
end