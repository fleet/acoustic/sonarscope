%PARSECONFIGURATIONXMLDATA Parse EK80 raw Configuration XML0 datagram
%
% CALL: [ConfigurationData] = ParseConfigurationXmlData(XmlData)
%
% Inputs:
%   XmlData    = 
%
% Outputs:
%   ConfigurationData = 
%
% Description:
%
%
%
% Examples(s):
%   [ConfigurationData] = ParseConfigurationXmlData(XmlData)
%

% References:
%
%
% Created by Lars Nonboe Andersen
%
%
%
% Copyright (c) 2015 Kongsberg Maritime
%
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
% IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
% FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
% AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
% LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
% OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
% THE SOFTWARE.
% ---------------------------------------------------------------------------

function ConfigurationData = ParseConfigurationXmlData(XmlData)

narginchk(1,1);
nargoutchk(0,1);

% Header

headerIndex = find(strcmp({XmlData.Children.Name},'Header'));
HeaderXml = XmlData.Children(headerIndex);

nAttributes = length(HeaderXml.Attributes);

for kAtt=1:nAttributes
    [value, status] = str2num(HeaderXml.Attributes(kAtt).Value);
    if status
        Header.(HeaderXml.Attributes(kAtt).Name) = value;
    else
        Header.(HeaderXml.Attributes(kAtt).Name) = HeaderXml.Attributes(kAtt).Value;
    end
end

transceiversIndex   = strcmp({XmlData.Children.Name}, 'Transceivers');
TransceiversXml     = XmlData.Children(transceiversIndex);
% Ajout des donn�es de positionnement
transducerIndex     = strcmp({XmlData.Children.Name}, 'Transducers');
PosTransducersXml   = XmlData.Children(transducerIndex);

nTransceivers = length(TransceiversXml.Children);

for kTrans=1:nTransceivers
   TransceiverXml = TransceiversXml.Children(kTrans);
   % Concat�nation des attributes Transceivers/Transducers pour r�cup�rer les pos. du transducer
   Transceivers(kTrans).id =  TransceiverXml.Name; %#ok<AGROW>
   nAttributes = length(TransceiverXml.Attributes);
   for kAtt=1:nAttributes
       [value, status] = str2num(TransceiverXml.Attributes(kAtt).Value);
       if status
           Transceivers(kTrans).(TransceiverXml.Attributes(kAtt).Name) = value;
       else
           Transceivers(kTrans).(TransceiverXml.Attributes(kAtt).Name) = TransceiverXml.Attributes(kAtt).Value;
       end
   end
   
   ChannelsXml = TransceiverXml.Children;
   nChannels = length(ChannelsXml.Children);
   Channels  = [];
   for kChan=1:nChannels
       ChannelXml = ChannelsXml.Children(kChan);
       Channels(kChan).Name = ChannelXml.Name; %#ok<AGROW>
       nAttributes = length(ChannelXml.Attributes);
       for k=1:nAttributes
           Channels(kChan).(ChannelXml.Attributes(k).Name) = ChannelXml.Attributes(k).Value;
       end
       
       Transducer    = [];
       TransducerXml = ChannelXml.Children;
       
       % GLU-QO : ajout des bras de levier du transducer.
       if kChan==1
           % Pb � r�cup�rer la structure PosTransducersXml dans certains
           % fichiers ESSTECH21-D20210424-T143657.raw
           TransducerXml.Attributes = [TransducerXml.Attributes PosTransducersXml.Children(kChan).Attributes];
       end

       nAttributes = length(TransducerXml.Attributes);
       for m=1:nAttributes
           Transducer.(TransducerXml.Attributes(m).Name) = TransducerXml.Attributes(m).Value;
       end
       Channels(kChan).Transducer = Transducer; %#ok<AGROW>
   end
   Transceivers(kTrans).Channels = Channels; %#ok<AGROW>
end  

ConfigurationData.Header = Header;
ConfigurationData.Transceivers = Transceivers;

%{
return

% Header
config.header.TimeBias = str2num(ConfigurationData.header.TimeBias);

% Transceivers
for kTrans=1:nTransceivers
    config.transceivers(kTrans).SerialNumber = str2num(ConfigurationData.transceivers(kTrans).SerialNumber);
    nChannels = length(ConfigurationData.transceivers(kTrans).channels);
    Channels = [];
    for kChan=1:nChannels
        Channels(kChan).ChannelId          = ConfigurationData.transceivers(kTrans).channels(kChan).ChannelID;
        Channels(kChan).TransceiverType    = ConfigurationData.transceivers(kTrans).TransceiverType;
%        channels(kChan).ChannelId = configxml.transceivers(kTrans).channels(kChan).ChannelIdLong;
        Transducer = [];
        Transducer.AngleSensitivityAlongship    = str2num(ConfigurationData.transceivers(kTrans).channels(kChan).transducer.AngleSensitivityAlongship);
        Transducer.AngleSensitivityAthwartship  = str2num(ConfigurationData.transceivers(kTrans).channels(kChan).transducer.AngleSensitivityAthwartship);
        Transducer.BeamWidthAlongship           = str2num(ConfigurationData.transceivers(kTrans).channels(kChan).transducer.BeamWidthAlongship);
        Transducer.BeamWidthAthwartship         = str2num(ConfigurationData.transceivers(kTrans).channels(kChan).transducer.BeamWidthAthwartship);
        Transducer.EquivalentBeamAngle          = str2num(ConfigurationData.transceivers(kTrans).channels(kChan).transducer.EquivalentBeamAngle);
        Transducer.Frequency                    = str2num(ConfigurationData.transceivers(kTrans).channels(kChan).transducer.Frequency);
        Transducer.Gain                         = str2num(ConfigurationData.transceivers(kTrans).channels(kChan).transducer.Gain);
        Transducer.TransducerName               = ConfigurationData.transceivers(kTrans).channels(kChan).transducer.TransducerName;
        Channels(kChan).transducer = Transducer;
    end
    config.transceivers(kTrans).channels = Channels;
end
%}