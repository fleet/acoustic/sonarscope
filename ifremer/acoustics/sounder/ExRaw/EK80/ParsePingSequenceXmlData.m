function [PingSequenceData] = ParsePingSequenceXmlData(XmlData,PingSequenceData)
%PARSEPingSequenceXMLDATA Parse EK80 raw PingSequence XML0 datagram
%
% CALL: [PingSequenceData] = ParsePingSequenceXmlData(XmlData,EnvironmentData)
%
% Inputs:
%   XmlData         =
%   PingSequenceData = Previous PingSequence Data
% Outputs:
%   PingSequenceData = Updated PingSequence Data
%
% Description:
%
%
%
% Examples(s):
%   [PingSequenceData] = ParsePingSequenceXmlData(XmlData,PingSequenceData)
%

% References:
%
% Created by GLU-QO 2021/05/06
% Inspired by Lars Nonboe Andersen
%
%
%
% ---------------------------------------------------------------------------

narginchk(2,2);
nargoutchk(0,1);

nAttributes = length(XmlData.Attributes);
if (nAttributes~=0)
    for i = 1:nAttributes
        PingSequenceXmlData.(XmlData.Attributes(i).Name) = XmlData.Attributes(i).Value;
    end
    
    if isfield(PingSequenceXmlData,'ChannelID')
        PingSequenceData.ChannelID     = PingSequenceXmlData.ChannelID;
    end
end