function [SensorData] = ParseSensorXmlData(XmlData,SensorData)
%PARSESENSORXMLDATA Parse EK80 raw Sensor XML0 datagram
%
% CALL: [SensorData] = ParseSensorXmlData(XmlData,EnvironmentData)
%
% Inputs:
%   XmlData         =
%   SensorData = Previous Sensor Data
% Outputs:
%   SensorData = Updated Sensor Data
%
% Description:
%
%
%
% Examples(s):
%   [SensorData] = ParseSensorXmlData(XmlData,SensorData)
%

% References:
%
% Created by GLU-QO 2021/05/006
% Inspired by Lars Nonboe Andersen
%
%
%
% ---------------------------------------------------------------------------

narginchk(2,2);
nargoutchk(0,1);

nAttributes = length(XmlData.Attributes);
if (nAttributes~=0)
    for i = 1:nAttributes
        SensorXmlData.(XmlData.Attributes(i).Name) = XmlData.Attributes(i).Value;
    end
    
    if isfield(SensorXmlData,'IsManual')
        SensorData.IsManual     = str2double(SensorXmlData.IsManual);
    end
    if isfield(SensorXmlData,'IsManual')
        SensorData.Type         = SensorXmlData.Type;
    end
    if isfield(SensorXmlData,'ManualValue')
        SensorData.ManualValue  = str2double(SensorXmlData.ManualValue);
    end
end