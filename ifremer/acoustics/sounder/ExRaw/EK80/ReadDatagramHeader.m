% READDATAGRAMHEADER Read EK80 Raw Datagram Header
%
% DatagramHeader = ReadDatagramHeader(fileId)
%
% Inputs:
%   fileId :  
%
% Outputs:
%   DatagramHeader = 
%
% Description:
%
% Examples(s):
%   [DatagramHeader] = ReadDatagramHeader(fileId)
%
% References:
% Created by Lars Nonboe Andersen
%
% Copyright (c) 2015 Kongsberg Maritime
%
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
% IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
% FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
% AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
% LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
% OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
% THE SOFTWARE.
% ---------------------------------------------------------------------------

function [DatagramHeader] = ReadDatagramHeader(fileId)

DatagramHeader.type = char(fread(fileId, 4, 'char')');

DatagramHeader.lowDateTime  = fread(fileId, 1, 'uint32');
DatagramHeader.highDateTime = fread(fileId, 1, 'uint32');

DatagramHeader.dateTime = NTTime2Mlab(DatagramHeader.highDateTime*2^32 + DatagramHeader.lowDateTime);
