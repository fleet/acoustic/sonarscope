function [RawData, InfoDatagrams] = ReadEK80Data(varargin)
%READEK80RAWFILES Read EK80 Data
%
% CALL: [RawData] = ReadEK80Data(filename(optional))
%
% Outputs:  
%   RawData = 
%
% Description:
%
%
%x
% Examples(s):
%   [RawData] = ReadEK80Data
%

% References:
%
%
% Created by Lars Nonboe Andersen
%
%
%
% Copyright (c) 2015 Kongsberg Maritime
%
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
% IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
% FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
% AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
% LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
% OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
% THE SOFTWARE.
% ---------------------------------------------------------------------------
%nargoutchk(0,1); %nlb

defaultPath = 'C:\Installations\EK80 Installation\Example Data';
%defaultPath = 'C:\Users\Public\Documents\Simrad\EK80\Data';

if nargin == 0
    % Select raw data files
    [fileNameList, filePath, ~] = uigetfile('*.raw', 'Select raw files', 'MultiSelect','on');
else %nlb
    fileNameList = varargin{1};
    filePath = '';
end
if ~iscell(fileNameList)
    fileNameList = {fileNameList};
end
nFiles = length(fileNameList);

% Read raw data files
for fileNumber = 1:nFiles
    fileName = fullfile(filePath,fileNameList{fileNumber});
    
    %%%%%%%%%%%%%%%%%%%
    % Read "npingsmax" pings from file "filename"
    
    [RawData(fileNumber).Data, listDgmTypes] = ReadEK80RawFile(fileName);
    fprintf('%s : Finished reading file %d of %d\n', mfilename, fileNumber, nFiles);
end

% Comptage des occurrences des types de datagrammes.
a = unique(listDgmTypes,'stable');
b = cellfun(@(x) sum(ismember(listDgmTypes,x)),a,'un',0);
InfoDatagrams = [];
for k=1:numel(a)
    nameField = sprintf('nb%s',a{k});
    InfoDatagrams.(nameField) = b{k};
end
