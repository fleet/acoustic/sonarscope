function [hh, mm, ss] = read_day_time_EK80(datetime)

% date = datestr(datetime, 'yyyy-mm-dd HH:MM:SS:FFF');

time_day = (datetime-floor(datetime))*24*3600; %secondes

hh = floor(time_day / 3600);
mm = floor((time_day / 3600 - hh) * 60);
ss = ((time_day / 3600-hh) * 60 - mm) * 60;
