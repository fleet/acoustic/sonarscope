function flag = EK80Raw2ssc(nomFic, varargin)

global useCacheNetcdf %#ok<GVMIS>
global useParallel %#ok<GVMIS>

% [varargin, useCacheNetcdf]  = getPropertyValue(varargin, 'useCacheNetcdf',  useCacheNetcdf); %#ok<ASGLU>

previous_useCacheNetcdf = useCacheNetcdf; %#ok<NASGU> 
% useCacheNetcdf = 1;

FormatVersion = 20190710;

%% Test d'existence du fichier

flag = exist(nomFic, 'file');
if ~flag
    str1 = sprintf('Le fichier "%s" n''existe pas.', nomFic);
    str2 = sprintf('File "%s" does not exist.', nomFic);
    my_warndlg(Lang(str1,str2), 1);
    return
end

[nomDirRacine, nomFicSeul] = fileparts(nomFic);

% Test d'existence du Cache complet..
flag = [];
flag(end+1) = exist(nomDirRacine, 'dir');

nomDirRacine = fullfile(nomDirRacine, 'SonarScope', nomFicSeul);
unzipSonarScopeCache(nomDirRacine);

flag(end+1) = exist(fullfile(nomDirRacine, 'Ssc_Sample.xml'), 'file');
flag(end+1) = exist(fullfile(nomDirRacine, 'Ssc_Sample'), 'dir');
  
if all(flag) && (getFormatVersion(nomDirRacine) >= FormatVersion)
    flag = 1;
    return
end

%% Cr�ation du r�pertoire cache

[nomDirRacine, nomFicSeul] = fileparts(nomFic);
nomDirRacine = fullfile(nomDirRacine, 'SonarScope');
if ~exist(nomDirRacine, 'dir')
    [flag, message] = mkdir(nomDirRacine);
    if ~flag
        messageErreurFichier(nomDirRacine, 'WriteFailure', 'Message', message);
        return
    end
end

nomDirRacine = fullfile(nomDirRacine, nomFicSeul);
if ~exist(nomDirRacine, 'dir')
     [flag, message] = mkdir(nomDirRacine);
    if ~flag
        messageErreurFichier(nomDirRacine, 'WriteFailure', 'Message', message);
        return
    end
end

%% Lecture en Matlab pour l'instant du fichier .raw

% Read EK80 Raw data files
[RawData, InfoDatagrams] = ReadEK80Data(nomFic);

% Perform basic processing of the raw complex sample data
BasicProcessedData = BasicProcessData(RawData);

% Flag de pr�sence de la navigation, qui peut �tre recharg�e ult�rieurement.

flag = exist(nomFic, 'file');
if ~flag
    str1 = sprintf('Le fichier "%s" n''existe pas.', nomFic);
    str2 = sprintf('File "%s" does not exist.', nomFic);
    my_warndlg(Lang(str1,str2), 1);
    return
end

nbPings    = numel(BasicProcessedData(1).ProcessedSampleData); % size(RawData.PingData, 2);
nbChannels = numel(BasicProcessedData); % numel(RawData.Data.ConfigurationData.Transceivers);
% Extraction du nbSamples sur le 1er ping.
nbSamples    = max(unique(arrayfun(@(x) numel(x.ProcessedSampleData(1).range), BasicProcessedData))); % max(unique(arrayfun(@(x) max(numel(x.SampleData.complexSamples)), RawData.PingData)));
nameChannels = arrayfun(@(x) (x.ChannelData.ChannelID), BasicProcessedData, 'Un', 0); % arrayfun(@(x) (x.Channels.ChannelID), RawData.ConfigurationData.Transceivers, 'Un', 0);

%% DataHeader

DataHeader.Config.surveyname      = 'TBF'; % 'To be Find'
DataHeader.Config.transectname    = 'TBF'; % 'To be Find'
DataHeader.Config.version         = RawData.Data.ConfigurationData.Transceivers(1).TransceiverSoftwareVersion; % transceiverSoftwareVersion
DataHeader.Config.transducercount = numel(RawData.Data.ConfigurationData.Transceivers);
DataHeader.Config.nrefbeams       = 1; % 
DataHeader.Config.soundername     = RawData.Data.ConfigurationData.Header.ApplicationName;

%% DataSensor : latitude, navigation, ...
% On consid�re les donn�es de nav identiques entre les diff�rents channels.
if isfield(RawData.Data.PingData(1).NMEAData, 'lat')
%     DataSensor.Nav.Latitude              = arrayfun(@(x) (x.NMEAData.lat),       x)';
%     DataSensor.Nav.Longitude             = arrayfun(@(x) (x.NMEAData.long),      x)';
%     DataSensor.Nav.Time                  = arrayfun(@(x) (x.time),               x)';
%     DataSensor.Course.HeadingVessel      = arrayfun(@(x) (x.NMEAData.heading),   x)';
%     DataSensor.Course.SpeedInKnots       = arrayfun(@(x) (x.NMEAData.speed_knt), x)';
%     DataSensor.Course.Time               = arrayfun(@(x) (x.time),               x)';
%     DataSensor.Course.HeadingVesselIndex = [];
    
    % Modif JMA le 12/02/2021
    DataSensor.Nav.Latitude              = [];
    DataSensor.Nav.Longitude             = [];
    DataSensor.Nav.Time                  = [];
    DataSensor.Course.HeadingVessel      = [];
    DataSensor.Course.SpeedInKnots       = [];
    DataSensor.Course.Time               = [];
    DataSensor.Course.HeadingVesselIndex = [];
    
    x = RawData.Data.PingData(1, :);
    for k1=1:length(x)
        if ~isempty(x(k1).NMEAData)
            y = x(k1).NMEAData;
            DataSensor.Nav.Latitude(end+1,1)              = y.lat;
            DataSensor.Nav.Longitude(end+1,1)             = y.long;
            DataSensor.Nav.Time(end+1,1)                  = x(k1).time;
            DataSensor.Course.HeadingVessel(end+1,1)      = y.heading;
            DataSensor.Course.SpeedInKnots(end+1,1)       = y.speed_knt;
            DataSensor.Course.Time(end+1,1)               = x(k1).time;
        end
    end
else
    DataSensor.Nav.Latitude              = [];
    DataSensor.Nav.Longitude             = [];
    DataSensor.Nav.Time                  = [];
    DataSensor.Course.HeadingVessel      = [];
    DataSensor.Course.SpeedInKnots       = [];
    DataSensor.Course.Time               = [];
    DataSensor.Course.HeadingVesselIndex = [];
end

%% DataChannel

sRawConfig = RawData.Data.ConfigurationData.Transceivers; % (iChannel).Channels(1).Transducer

for iChannel=1:nbChannels
    try 
        % Cas nominal (ex : D20190625-T100713.raw)
        DataConfig.Transducer(iChannel).channelid                   = sRawConfig(iChannel).TransceiverNumber;
    catch 
        % Cas du fichier ESSTECH21-D20210424-T143657.raw
        DataConfig.Transducer(iChannel).channelid                   = sRawConfig(1).TransceiverNumber;
    end
    DataConfig.Transducer(iChannel).beamtype                    = str2double(BasicProcessedData(iChannel).ChannelData.Transducer.BeamType);
    DataConfig.Transducer(iChannel).equivalentbeamangle         = str2double(BasicProcessedData(iChannel).ChannelData.Transducer.EquivalentBeamAngle);
    DataConfig.Transducer(iChannel).beamwidthalongship          = str2double(BasicProcessedData(iChannel).ChannelData.Transducer.BeamWidthAlongship);
    DataConfig.Transducer(iChannel).beamwidthathwartship        = str2double(BasicProcessedData(iChannel).ChannelData.Transducer.BeamWidthAthwartship);
    DataConfig.Transducer(iChannel).anglesensitivityalongship   = str2double(BasicProcessedData(iChannel).ChannelData.Transducer.AngleSensitivityAlongship);
    DataConfig.Transducer(iChannel).anglesensitivityathwartship = str2double(BasicProcessedData(iChannel).ChannelData.Transducer.AngleSensitivityAthwartship);
    DataConfig.Transducer(iChannel).angleoffsetalongship        = str2double(BasicProcessedData(iChannel).ChannelData.Transducer.AngleOffsetAlongship);
    DataConfig.Transducer(iChannel).angleoffsetathwartship      = str2double(BasicProcessedData(iChannel).ChannelData.Transducer.AngleOffsetAthwartship);
    if numel(sRawConfig) > 1
        % Cas du fichier D20190625-T100713.raw.raw
        DataConfig.Transducer(iChannel).posx                        = str2double(sRawConfig(iChannel).Channels.Transducer.TransducerOffsetX); % TBC
        DataConfig.Transducer(iChannel).posy                        = str2double(sRawConfig(iChannel).Channels.Transducer.TransducerOffsetY); % TBC
        DataConfig.Transducer(iChannel).posz                        = str2double(sRawConfig(iChannel).Channels.Transducer.TransducerOffsetZ); % TBC
        DataConfig.Transducer(iChannel).dirx                        = str2double(sRawConfig(iChannel).Channels.Transducer.TransducerAlphaX);	% TBC
        DataConfig.Transducer(iChannel).diry                        = str2double(sRawConfig(iChannel).Channels.Transducer.TransducerAlphaY);	% TBC
        DataConfig.Transducer(iChannel).dirz                        = str2double(sRawConfig(iChannel).Channels.Transducer.TransducerAlphaZ);	% TBC
    else
        % Cas du fichier ESSTECH21-D20210424-T143657.raw
        DataConfig.Transducer(iChannel).posx                        = 0;% str2double(sRawConfig.Channels(iChannel).Transducer.TransducerOffsetX); % TBC
        DataConfig.Transducer(iChannel).posy                        = 0;% str2double(sRawConfig.Channels(iChannel).Transducer.TransducerOffsetY); % TBC
        DataConfig.Transducer(iChannel).posz                        = 0;% str2double(sRawConfig.Channels(iChannel).Transducer.TransducerOffsetZ); % TBC
        DataConfig.Transducer(iChannel).dirx                        = 0;% str2double(sRawConfig.Channels(iChannel).Transducer.TransducerAlphaX);	% TBC
        DataConfig.Transducer(iChannel).diry                        = 0;% str2double(sRawConfig.Channels(iChannel).Transducer.TransducerAlphaY);	% TBC
        DataConfig.Transducer(iChannel).dirz                        = 0;% str2double(sRawConfig.Channels(iChannel).Transducer.TransducerAlphaZ);	% TBC
    end
end

%% Data par Channel

for iChannel=1:nbChannels
    DataExRaw(iChannel).mode                  = NaN(nbPings, 1, 'single'); %#ok<AGROW>
    DataExRaw(iChannel).transducerdepth       = NaN(nbPings, 1, 'single'); %#ok<AGROW>
    DataExRaw(iChannel).frequency             = NaN(nbPings, 1, 'single'); %#ok<AGROW>
    DataExRaw(iChannel).transmitpower         = NaN(nbPings, 1, 'single'); %#ok<AGROW>
    DataExRaw(iChannel).pulselength           = NaN(nbPings, 1, 'single'); %#ok<AGROW>
    DataExRaw(iChannel).bandwidth             = NaN(nbPings, 1, 'single'); %#ok<AGROW>
    DataExRaw(iChannel).sampleinterval        = NaN(nbPings, 1, 'single'); %#ok<AGROW>
    DataExRaw(iChannel).soundvelocity         = NaN(nbPings, 1, 'single'); %#ok<AGROW>
    DataExRaw(iChannel).absorptioncoefficient = NaN(nbPings, 1, 'single'); %#ok<AGROW>
    DataExRaw(iChannel).heave                 = NaN(nbPings, 1, 'single'); %#ok<AGROW>
    DataExRaw(iChannel).roll                  = NaN(nbPings, 1, 'single'); %#ok<AGROW>
    DataExRaw(iChannel).pitch                 = NaN(nbPings, 1, 'single'); %#ok<AGROW>
    DataExRaw(iChannel).temperature           = NaN(nbPings, 1, 'single'); %#ok<AGROW>
    DataExRaw(iChannel).trawlupperdepthvalid  = NaN(nbPings, 1, 'single'); %#ok<AGROW>
    DataExRaw(iChannel).trawlopeningvalid     = NaN(nbPings, 1, 'single'); %#ok<AGROW>
    DataExRaw(iChannel).trawlupperdepth       = NaN(nbPings, 1, 'single'); %#ok<AGROW>
    DataExRaw(iChannel).trawlopening          = NaN(nbPings, 1, 'single'); %#ok<AGROW>
    DataExRaw(iChannel).offset                = NaN(nbPings, 1, 'single'); %#ok<AGROW>
    DataExRaw(iChannel).count                 = NaN(nbPings, 1, 'single'); %#ok<AGROW>
    DataExRaw(iChannel).power                 = NaN(nbPings, nbSamples, 'single'); %#ok<AGROW>
    DataExRaw(iChannel).sv                    = NaN(nbPings, nbSamples, 'single'); %#ok<AGROW>
    DataExRaw(iChannel).sp                    = NaN(nbPings, nbSamples, 'single'); %#ok<AGROW>
% % % %     DataExRaw(iChannel).angle                   = NaN(nbPings, nbSamples, 'single'); %#ok<AGROW>
    DataExRaw(iChannel).alongship             = NaN(nbPings, nbSamples, 'single'); %#ok<AGROW>
    DataExRaw(iChannel).athwartship           = NaN(nbPings, nbSamples, 'single'); %#ok<AGROW>
end

%% R�partition des SampleData issus de la lecture des diff�rents paquets

for iChannel=1:nbChannels
    x = RawData.Data.PingData(iChannel, :);
    for k1=1:length(x)
        if ~isempty(RawData.Data.PingData(iChannel, k1).EnvironmentData)
            envData(iChannel) = RawData.Data.PingData(iChannel, k1).EnvironmentData; %#ok<AGROW>
        end
    end
end
for iChannel=1:nbChannels
%     DataExRaw(iChannel).mode            = arrayfun(@(x) (x.SampleData.mode),             RawData.Data.PingData(iChannel, :))';
%     DataExRaw(iChannel).transducerdepth = arrayfun(@(x) (x.EnvironmentData.depth),       RawData.Data.PingData(iChannel, :))';
%     DataExRaw(iChannel).frequency       = arrayfun(@(x) (x.ParameterData.frequencyUsed), RawData.Data.PingData(iChannel, :))';
%     DataExRaw(iChannel).transmitpower   = arrayfun(@(x) (x.ParameterData.transmitPower), RawData.Data.PingData(iChannel, :))';
%     DataExRaw(iChannel).pulselength     = arrayfun(@(x) (x.ParameterData.pulseLength),   RawData.Data.PingData)';
    
    % Modif JMA le 12/02/2021
    DataExRaw(iChannel).mode            = [];
    DataExRaw(iChannel).transducerdepth = [];
    DataExRaw(iChannel).frequency       = [];
    DataExRaw(iChannel).transmitpower   = [];
    DataExRaw(iChannel).pulselength     = [];
    
    x = RawData.Data.PingData(iChannel, :);
    idxPingValid = [];
    for k1=1:length(x)
        if ~isempty(x(k1).time)
            y = x(k1);
            DataExRaw(iChannel).mode(end+1,1)            = y.SampleData.mode;
            DataExRaw(iChannel).transducerdepth(end+1,1) = y.EnvironmentData.depth;
            DataExRaw(iChannel).frequency(end+1,1)       = y.ParameterData.frequencyUsed;
            DataExRaw(iChannel).transmitpower(end+1,1)   = y.ParameterData.transmitPower;
            DataExRaw(iChannel).pulselength              = y.ParameterData.pulseLength;
            idxPingValid(end+1) = k1; %#ok<AGROW>
        end
    end
    
	fMin = str2double(BasicProcessedData(iChannel).ChannelData.Transducer.FrequencyMinimum);
	fMax = str2double(BasicProcessedData(iChannel).ChannelData.Transducer.FrequencyMaximum);
    
    DataExRaw(iChannel).bandwidth      = fMax - fMin;
%     DataExRaw(iChannel).sampleinterval = arrayfun(@(x) (x.ParameterData.sampleInterval), RawData.Data.PingData(iChannel, :))';
%     DataExRaw(iChannel).soundvelocity  = arrayfun(@(x) (x.EnvironmentData.soundSpeed),   RawData.Data.PingData(iChannel, :))';
%     fUsed   = arrayfun(@(x) (x.ParameterData.frequencyUsed), RawData.Data.PingData(iChannel, :));
    
     % TODO GLU, Modif JMA le 12/02/2021
    DataExRaw(iChannel).sampleinterval = [];
    DataExRaw(iChannel).soundvelocity  = [];
    fUsed                              = [];
    
    x = RawData.Data.PingData(iChannel, :);
    for k1=1:length(x)
        if ~isempty(x(k1).time)
            y = x(k1);
            DataExRaw(iChannel).sampleinterval(end+1,1) = y.ParameterData.sampleInterval;
            DataExRaw(iChannel).soundvelocity(end+1,1)  = y.EnvironmentData.soundSpeed;
            fUsed(end+1,1)                              = y.ParameterData.frequencyUsed; %#ok<AGROW>
        end
    end   
    
    
    % Calcul du coef d'absoption selon la formule Kongsberg appliqu�e dans le parsing du EK80
%     envData = RawData.Data.PingData(iChannel, :).EnvironmentData;
    
    absorptionCoefficients                    = EstimateAbsorptionCoefficients(envData(iChannel), fUsed');
    DataExRaw(iChannel).absorptioncoefficient = absorptionCoefficients;
    
    pppp                                       = regexp(BasicProcessedData(iChannel).ChannelData.Transducer.Gain, ';', 'split');
    DataExRaw(iChannel).gain                   = arrayfun(@(x) str2double(x), pppp)';
    
    % TODO GLU DataExRaw(iChannel).sacorrection = [DataSample.Data(iChannel,1:nbPings).SaCorrection]';
    pppp                             = regexp(BasicProcessedData(iChannel).ChannelData.Transducer.SaCorrection, ';', 'split');
    DataExRaw(iChannel).sacorrection = arrayfun(@(x) str2double(x), pppp)';
    
%     DataExRaw(iChannel).heave        = arrayfun(@(x) (x.MotionData.heave), RawData.Data.PingData(iChannel, :))';
%     DataExRaw(iChannel).roll         = arrayfun(@(x) (x.MotionData.roll),  RawData.Data.PingData(iChannel, :))';
%     DataExRaw(iChannel).pitch        = arrayfun(@(x) (x.MotionData.pitch), RawData.Data.PingData(iChannel, :))';
%     DataExRaw(iChannel).temperature  = arrayfun(@(x) (x.EnvironmentData.temperature), RawData.Data.PingData(iChannel, :))';
%     % TODO GLU DataExRaw(iChannel).trawlupperdepthvalid    = [DataSample.Data(iChannel,1:nbPings).trawlupperdepthvalid]';
%     % TODO GLU DataExRaw(iChannel).trawlopeningvalid       = [DataSample.Data(iChannel,1:nbPings).trawlopeningvalid]';
%     % TODO GLU DataExRaw(iChannel).trawlupperdepth         = [DataSample.Data(iChannel,1:nbPings).trawlupperdepth]';
%     % TODO GLU DataExRaw(iChannel).trawlopening            = [DataSample.Data(iChannel,1:nbPings).trawlopening]';
%     DataExRaw(iChannel).offset   = arrayfun(@(x) (x.SampleData.offset), RawData.Data.PingData(iChannel, :))';
%     DataExRaw(iChannel).count    = arrayfun(@(x) (x.SampleData.count),  RawData.Data.PingData(iChannel, :))';
%     DataExRaw(iChannel).pingtime = arrayfun(@(x) (x.time),              RawData.Data.PingData(iChannel, :))';
%     pppp = arrayfun(@(x) strtrim(x.SampleData.channelId), RawData.Data.PingData(iChannel,:), 'Un', 0);
%     pppp = cellfun(@(x) regexprep(x, char(0), ''), pppp, 'Un',0);
    
    % Modif JMA le 12/02/2021
    DataExRaw(iChannel).heave       = [];
    DataExRaw(iChannel).roll        = [];
    DataExRaw(iChannel).pitch       = [];
    DataExRaw(iChannel).temperature = [];
    DataExRaw(iChannel).offset      = [];
    DataExRaw(iChannel).count       = [];
    DataExRaw(iChannel).pingtime    = [];
    pppp  = {};
    
    x = RawData.Data.PingData(iChannel, :);
    for k1=1:length(x)
        if ~isempty(x(k1).time)
            y = x(k1);
            DataExRaw(iChannel).heave(end+1,1)       = y.MotionData.heave;
            DataExRaw(iChannel).roll(end+1,1)        = y.MotionData.roll;
            DataExRaw(iChannel).pitch(end+1,1)       = y.MotionData.pitch;
            DataExRaw(iChannel).temperature(end+1,1) = y.EnvironmentData.temperature;
            DataExRaw(iChannel).offset(end+1,1)      = y.SampleData.offset;
            DataExRaw(iChannel).count(end+1,1)       = y.SampleData.count;
            DataExRaw(iChannel).pingtime(end+1,1)    = y.time;
            channelId = y.SampleData.channelId;
            channelId = strtrim(channelId);
            pppp{end+1,1} = regexprep(channelId, char(0), ''); %#ok<AGROW>
        end
    end   
    
    % Suppression des codes 0 (???) mis en bout de noms du channels.
	[~, Locb] = ismember(pppp, nameChannels);
    DataExRaw(iChannel).channelIndex = Locb;
    
    nbPingsValid = numel(DataExRaw(iChannel).pingtime);
    DataExRaw(iChannel).power       = NaN(nbPingsValid, nbSamples, 'single'); 
    DataExRaw(iChannel).sv          = NaN(nbPingsValid, nbSamples, 'single'); 
    DataExRaw(iChannel).sp          = NaN(nbPingsValid, nbSamples, 'single'); 
    DataExRaw(iChannel).alongship   = NaN(nbPingsValid, nbSamples, 'single'); 
    DataExRaw(iChannel).athwartship = NaN(nbPingsValid, nbSamples, 'single'); 
    
    for kk=1:nbPingsValid
        % On identifie l'index des informations utiles.
        idx = idxPingValid(kk);
        if ~isempty(RawData.Data.PingData(iChannel,idx).time)
            sub = 1:RawData.Data.PingData(iChannel,idx).SampleData.count;
            DataExRaw(iChannel).power(kk,sub) = BasicProcessedData(iChannel).ProcessedSampleData(idx).power;
            DataExRaw(iChannel).sp(kk,sub)    = BasicProcessedData(iChannel).ProcessedSampleData(idx).sp;
            DataExRaw(iChannel).sv(kk,sub)    = BasicProcessedData(iChannel).ProcessedSampleData(idx).sv;
            if isfield(BasicProcessedData(iChannel).ProcessedSampleData(kk), 'alongship') && ~isempty(BasicProcessedData(iChannel).ProcessedSampleData(idx).alongship)
                try 
                    % Cas nominal (ex : D20190625-T100713.raw)
                    DataExRaw(iChannel).alongship(kk,sub)   = BasicProcessedData(iChannel).ProcessedSampleData(idx).alongship /str2double(sRawConfig(iChannel).Channels.Transducer.AngleSensitivityAlongship);
                    DataExRaw(iChannel).athwartship(kk,sub) = BasicProcessedData(iChannel).ProcessedSampleData(idx).athwartship/str2double(sRawConfig(iChannel).Channels.Transducer.AngleSensitivityAthwartship);
                catch 
                    try
                        % Cas du fichier ESSTECH21-D20210424-T143657.raw
                        DataExRaw(iChannel).alongship(kk,sub)   = BasicProcessedData(iChannel).ProcessedSampleData(idx).alongship /str2double(sRawConfig(1).Channels(iChannel).Transducer.AngleSensitivityAlongship);
                        DataExRaw(iChannel).athwartship(kk,sub) = BasicProcessedData(iChannel).ProcessedSampleData(idx).athwartship/str2double(sRawConfig(1).Channels(iChannel).Transducer.AngleSensitivityAthwartship);
                    catch
                        DataExRaw(iChannel).alongship(kk,sub)   = BasicProcessedData(iChannel).ProcessedSampleData(idx).alongship /str2double(sRawConfig(iChannel).Channels(1).Transducer.AngleSensitivityAlongship);
                        DataExRaw(iChannel).athwartship(kk,sub) = BasicProcessedData(iChannel).ProcessedSampleData(idx).athwartship/str2double(sRawConfig(iChannel).Channels(1).Transducer.AngleSensitivityAthwartship);
                    end
                end
            end
        end
    end  
end
%{
figure; plot(Longitude, Latitude); grid on
figure; PlotUtils.createSScPlot(DataExRaw(1).soundvelocity);  grid on; title('soundvelocity');
figure; PlotUtils.createSScPlot(DataExRaw(1).SpeedInKnots);  grid on; title('SpeedInKnots');
figure; PlotUtils.createSScPlot(DataExRaw(1).HeadingVessel); grid on; title('HeadingVessel');
figure; PlotUtils.createSScPlot(DataExRaw(1).HeureNav);      grid on; title('HeureNav');
figure; PlotUtils.createSScPlot(DataExRaw(1).Time);          grid on; title('Time');
%}

% clear RawData BasicProcessedData sRawConfig

%% Ecriture des paquets.
% Datagrammes Position

nomSondeur = 'EK80';

% Prise en charge (sans le 1er niveau ce cache) des donn�es issues de la lecture.
flag = EK80Raw2ssc_ConfigHeader(nomDirRacine, InfoDatagrams, DataHeader.Config, FormatVersion);
if ~flag
    return
end

% flag = EK80Raw2ssc_ConfigTransducer(nomDirRacine, InfoDatagrams, DataExRaw, DataConfig.Transducer, FormatVersion, nomSondeur);
% Modif JMA le 12/02/2021
flag = EK80Raw2ssc_ConfigTransducer(nomDirRacine, InfoDatagrams, DataConfig.Transducer, FormatVersion, nomSondeur);
if ~flag
    return
end

flag = EK80Raw2ssc_Position(nomDirRacine, InfoDatagrams, DataExRaw, DataSensor, FormatVersion);
if ~flag
    return
end

flag = EK80Raw2ssc_DataSample(nomDirRacine, InfoDatagrams, DataExRaw, FormatVersion);
if ~flag
    return
end

%% Gestion du cache

%{
% Comment� pour le moment : Pour quelle raison ?

useCacheNetcdf = previous_useCacheNetcdf;
switch useCacheNetcdf
    case 1 % 'Create XML-Bin only'
    case 2 % 'Create Netcdf and keep XML-Bin'
        RAW_Cache2Netcdf(nomFic, 'deleteXMLBin', 0, 'useParallel', useParallel);

    case 3 % 'Create Netcdf and delete XML-Bin'
        RAW_Cache2Netcdf(nomFic, 'deleteXMLBin', 1, 'useParallel', useParallel);
end
%}

flag = 1;


function FormatVersion = getFormatVersion(nomDirRacine)

nomFicXml = fullfile(nomDirRacine, 'Ssc_Sample.xml');
flag = exist(nomFicXml, 'file');        
if ~flag
    special_message('FichierNonExistant', nomFicXml)
    return
end
% edit(nomFicXml)
Datagrams = xml_read(nomFicXml);
if ~isfield(Datagrams, 'FormatVersion')
    FormatVersion = 0;
    return
end
FormatVersion = Datagrams.FormatVersion;
