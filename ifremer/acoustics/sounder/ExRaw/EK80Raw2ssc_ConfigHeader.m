function flag = EK80Raw2ssc_ConfigHeader(nomDirRacine, InfoDatagrams, ConfigHeader, FormatVersion)

%% Pr�paratifs

Data.SurveyName                  = ConfigHeader.surveyname;
Data.TransectName                = ConfigHeader.transectname;
Data.TransceiverSoftwareVersion  = ConfigHeader.version;
Data.TransducerCount             = ConfigHeader.transducercount;
Data.NbRefBeams                  = ConfigHeader.nrefbeams;


%% G�n�ration du XML SonarScope

Info.Title                  = 'ConfigurationHeader'; % Entete du fichier
Info.Constructor            = 'Simrad';
Info.Sounder                = ConfigHeader.soundername;
Info.TimeOrigin             = 'Matlab';
Info.Comments               = 'Position and Attitude';
Info.FormatVersion          = FormatVersion;
Info.InfoDatagrams          = InfoDatagrams;
Info.Dimensions.nbPings     = 1;

Info.Signals(1).Name          = 'TransducerCount';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_ConfigHeader','TransducerCount.bin');
Info.Signals(end).Tag         = verifKeyWord('TransducerCount');

Info.Signals(end+1).Name      = 'NbRefBeams';
Info.Signals(end).Dimensions  = 'nbPings,1';
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_ConfigHeader','NbRefBeams.bin');
Info.Signals(end).Tag         = verifKeyWord('NbReferenceBeams');

%
Info.Strings(1).Name          = 'SurveyName';
Info.Strings(end).Dimensions  = '1, 1';
Info.Strings(end).Storage     = 'char';
Info.Strings(end).Unit        = '';
Info.Strings(end).FileName    = fullfile('Ssc_ConfigHeader','SurveyName.bin');
Info.Strings(end).Tag         = verifKeyWord('SurveyName');

Info.Strings(end+1).Name      = 'TransectName';
Info.Strings(end).Dimensions  = '1, 1';
Info.Strings(end).Storage     = 'char';
Info.Strings(end).Unit        = '';
Info.Strings(end).FileName    = fullfile('Ssc_ConfigHeader','TransectName.bin');
Info.Strings(end).Tag         = verifKeyWord('TransectName');

Info.Strings(end+1).Name      = 'TransceiverSoftwareVersion';
Info.Strings(end).Dimensions  = '1, 1';
Info.Strings(end).Storage     = 'char';
Info.Strings(end).Unit        = '';
Info.Strings(end).FileName    = fullfile('Ssc_ConfigHeader','TransceiverSoftwareVersion.bin');
Info.Strings(end).Tag         = verifKeyWord('TransceiverSoftwareVersion');


%% Cr�ation du fichier XML d�crivant la donn�e 

nomFicXml = fullfile(nomDirRacine, 'Ssc_ConfigHeader.xml');
xml_write(nomFicXml, Info);
flag = exist(nomFicXml, 'file');
if ~flag
    messageErreurFichier(nomFicXml);
    return
end

%% Cr�ation du r�pertoire File Heading

nomDirSscData = fullfile(nomDirRacine, 'Ssc_ConfigHeader');
if ~exist(nomDirSscData, 'dir')
    status = mkdir(nomDirSscData);
    if ~status
        messageErreur(nomDirSscData)
        flag = 0;
        return
    end
end

%% Cr�ation des fichiers binaires des signaux

for k=1:numel(Info.Signals)
    flag = writeSignal(nomDirRacine, Info.Signals(k), Data.(Info.Signals(k).Name));
    if ~flag
        return
    end
end

%% Cr�ation des fichiers binaires des chaines.

for k=1:numel(Info.Strings)
    flag = writeString(nomDirRacine, Info.Strings(k), Data.(Info.Strings(k).Name));
    if ~flag
        return
    end
end

flag = 1;
