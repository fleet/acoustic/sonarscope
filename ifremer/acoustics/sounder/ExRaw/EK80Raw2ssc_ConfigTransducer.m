function [flag] = EK80Raw2ssc_ConfigTransducer(nomDirRacine, InfoDatagrams, ConfigTransducer, FormatVersion, nomSondeur)

%% Pr�paratifs

nbChannels = size(ConfigTransducer, 2);

for iChannel=1:nbChannels
    Data.ChannelIdentification{iChannel}      = ConfigTransducer(iChannel).channelid; 
    Data.BeamType(iChannel)                   = ConfigTransducer(iChannel).beamtype; 
    Data.EquivalentBeamAngle(iChannel)        = ConfigTransducer(iChannel).equivalentbeamangle; 
    Data.BeamwidthAlongShip(iChannel)         = ConfigTransducer(iChannel).beamwidthalongship; 
    Data.BeamwidthAthwartShip(iChannel)       = ConfigTransducer(iChannel).beamwidthathwartship; 
    Data.AngleSensitivityAlongShip(iChannel)  = ConfigTransducer(iChannel).anglesensitivityalongship; 
    Data.AngleSensitivityAthwartShip(iChannel)= ConfigTransducer(iChannel).anglesensitivityathwartship; 
    Data.AngleOffsetAlongShip(iChannel)       = ConfigTransducer(iChannel).angleoffsetalongship; 
    Data.AngleOffsetAthwartShip(iChannel)     = ConfigTransducer(iChannel).angleoffsetathwartship; 
    Data.PosX(iChannel)                       = ConfigTransducer(iChannel).posx; 
    Data.PosY(iChannel)                       = ConfigTransducer(iChannel).posy; 
    Data.PosZ(iChannel)                       = ConfigTransducer(iChannel).posz; 
    Data.DirX(iChannel)                       = ConfigTransducer(iChannel).dirx; 
    Data.DirY(iChannel)                       = ConfigTransducer(iChannel).diry; 
    Data.DirZ(iChannel)                       = ConfigTransducer(iChannel).dirz; 
end

%% G�n�ration du XML SonarScope

Info.Title                    = 'ConfigurationTransducer'; % Entete du fichier
Info.Constructor              = 'Simrad';
Info.Sounder                  = nomSondeur;
Info.TimeOrigin               = 'Matlab';
Info.Comments                 = 'Position and Attitude';
Info.FormatVersion            = FormatVersion;
Info.InfoDatagrams            = InfoDatagrams;
Info.Dimensions.nbChannels    = nbChannels;

Info.Signals(1).Name          = 'EquivalentBeamAngle';
Info.Signals(end).Dimensions  = 'nbChannels,1';
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile('Ssc_ConfigTransducer','EquivalentBeamAngle.bin');
Info.Signals(end).Tag         = verifKeyWord('BeamEquivalentTwoWayBeamAngle');

Info.Signals(end+1).Name      = 'BeamwidthAlongShip';
Info.Signals(end).Dimensions  = 'nbChannels,1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile('Ssc_ConfigTransducer','BeamwidthAlongShip.bin');
Info.Signals(end).Tag         = verifKeyWord('BeamAlongship3dBBeamWidth');

Info.Signals(end+1).Name      = 'BeamwidthAthwartShip';
Info.Signals(end).Dimensions  = 'nbChannels,1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile('Ssc_ConfigTransducer','BeamwidthAthwartShip.bin');
Info.Signals(end).Tag         = verifKeyWord('BeamAthwartship3dBBeamWidth');

Info.Signals(end+1).Name      = 'AngleSensitivityAlongShip';
Info.Signals(end).Dimensions  = 'nbChannels,1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_ConfigTransducer','AngleSensitivityAlongShip.bin');
Info.Signals(end).Tag         = verifKeyWord('BeamAlongshipAngleSensitivity');

Info.Signals(end+1).Name      = 'AngleSensitivityAthwartShip';
Info.Signals(end).Dimensions  = 'nbChannels,1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_ConfigTransducer','AngleSensitivityAthwartShip.bin');
Info.Signals(end).Tag         = verifKeyWord('BeamAthwartshipAngleSensitivity');

Info.Signals(end+1).Name      = 'AngleOffsetAlongShip';
Info.Signals(end).Dimensions  = 'nbChannels,1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_ConfigTransducer','AngleOffsetAlongShip.bin');
Info.Signals(end).Tag         = verifKeyWord('AlongOffset');

Info.Signals(end+1).Name      = 'AngleOffsetAthwartShip';
Info.Signals(end).Dimensions  = 'nbChannels,1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_ConfigTransducer','AngleOffsetAthwartShip.bin');
Info.Signals(end).Tag         = verifKeyWord('AthwartOffset');

Info.Signals(end+1).Name      = 'PosX';
Info.Signals(end).Dimensions  = 'nbChannels,1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_ConfigTransducer','PosX.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'PosY';
Info.Signals(end).Dimensions  = 'nbChannels,1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_ConfigTransducer','PosY.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'PosZ';
Info.Signals(end).Dimensions  = 'nbChannels,1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_ConfigTransducer','PosZ.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'DirX';
Info.Signals(end).Dimensions  = 'nbChannels,1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_ConfigTransducer','DirX.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'DirY';
Info.Signals(end).Dimensions  = 'nbChannels,1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_ConfigTransducer','DirY.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'DirZ';
Info.Signals(end).Dimensions  = 'nbChannels,1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_ConfigTransducer','DirZ.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

%
Info.Strings(1).Name          = 'ChannelIdentification';
Info.Strings(end).Dimensions  = 'nbChannels, 1';
Info.Strings(end).Storage     = 'char';
Info.Strings(end).Unit        = '';
Info.Strings(end).FileName    = fullfile('Ssc_ConfigTransducer','ChannelIdentification.bin');
Info.Strings(end).Tag         = verifKeyWord('EchosounderDocIdentifier');

%% Cr�ation du fichier XML d�crivant la donn�e 

nomFicXml = fullfile(nomDirRacine, 'Ssc_ConfigTransducer.xml');
xml_write(nomFicXml, Info);
flag = exist(nomFicXml, 'file');
if ~flag
    messageErreurFichier(nomFicXml);
    return
end

%% Cr�ation du r�pertoire File Heading

nomDirSscData = fullfile(nomDirRacine, 'Ssc_ConfigTransducer');
if ~exist(nomDirSscData, 'dir')
    status = mkdir(nomDirSscData);
    if ~status
        messageErreur(nomDirSscData)
        flag = 0;
        return
    end
end

%% Cr�ation des fichiers binaires des signaux

for k=1:numel(Info.Signals)
    flag = writeSignal(nomDirRacine, Info.Signals(k), Data.(Info.Signals(k).Name));
    if ~flag
        return
    end
end


%% Cr�ation des fichiers binaires des chaines.

for k=1:numel(Info.Strings)
    flag = writeString(nomDirRacine, Info.Strings(k), Data.(Info.Strings(k).Name));
    if ~flag
        return
    end
end

flag = 1;

