function flag = EK80Raw2ssc_DataSample(nomDirRacine, InfoDatagrams, DataExRaw, FormatVersion)

% On suppose les m�mes dimensions pour les N Channels.
% nbSamples  = DataExRaw(1).Dim.nbSamples;
% nbPings    = DataExRaw(1).Dim.nbPings;
% nbChannels = DataExRaw(1).Dim.nbChannels;

% Modif JMA le 12/02/2021
nomSondeur = 'EK80';
[nbPings, nbSamples] = size(DataExRaw(1).power);
nbChannels = length(DataExRaw);

%% Pr�paratifs

for iChannel=1:nbChannels
    Data.AcousticFrequency(iChannel)     = my_unique(DataExRaw(iChannel).frequency, 'NameVariable', 'DataExRaw.frequency');
    FreqkHz                              = floor(Data.AcousticFrequency(iChannel)/1000);
        
    Data.AbsorptionCoefficient(iChannel) = my_unique(DataExRaw(iChannel).absorptioncoefficient, 'NameVariable', 'DataExRaw.absorptioncoefficient');
    Data.PulseDuration(iChannel)         = my_unique(DataExRaw(iChannel).pulselength,           'NameVariable', 'DataExRaw.frequency');
    Data.TransmitPower(iChannel)         = my_unique(DataExRaw(iChannel).transmitpower,         'NameVariable', 'DataExRaw.pulselength');
    Data.Bandwidth(iChannel)             = my_unique(DataExRaw(iChannel).bandwidth,             'NameVariable', 'DataExRaw.bandwidth');
    Data.TransducerDepth(iChannel)       = my_unique(DataExRaw(iChannel).transducerdepth,       'NameVariable', 'DataExRaw.transducerdepth');
    Data.Mode(iChannel)                  = my_unique(DataExRaw(iChannel).mode,                  'NameVariable', 'DataExRaw.mode');
    Data.Immersion(iChannel)             = -(my_unique(DataExRaw(iChannel).transducerdepth,     'NameVariable', 'DataExRaw.transducerdepth'));
    Data.SampleFrequency(iChannel)       = 1 ./ my_unique(DataExRaw(iChannel).sampleinterval,   'NameVariable', 'DataExRaw.sampleinterval');
    Data.SoundVelocity(iChannel)         = my_unique(DataExRaw(iChannel).soundvelocity,         'NameVariable', 'DataExRaw.soundvelocity');
    Data.SampleInterval(iChannel)        = my_unique(DataExRaw(iChannel).sampleinterval,        'NameVariable', 'DataExRaw.sampleinterval');
    
    Data.Gain(iChannel)                  = my_unique(DataExRaw(iChannel).gain,                  'NameVariable', 'DataExRaw.gain');
    Data.SaCorrection(iChannel)          = my_unique(DataExRaw(iChannel).sacorrection,          'NameVariable', 'DataExRaw.sacorrection');

    % Le nb de Samples est variable d'une fr�quence � l'autre. Seule la
    % Range est fix�e par l'op�rateur. Le NBSamples d�pend de Fe (ou
    % PulseDuration 4/).
    % Cas du fichier ESSTECH21-D20210424-T143657.raw pour le nb de Samples
    % n'est pas identiques sur toutes les voies.
    n = min(arrayfun(@(x) numel(x.count), DataExRaw));
    Data.NbSamplesByChannel(:,iChannel) = DataExRaw(iChannel).count(1:n);

    Data.(sprintf('Amplitude_%dkHz',   FreqkHz)) = DataExRaw(iChannel).power;
    Data.(sprintf('Sv_%dkHz',          FreqkHz)) = DataExRaw(iChannel).sv;
    Data.(sprintf('Sp_%dkHz',          FreqkHz)) = DataExRaw(iChannel).sp;
    Data.(sprintf('AlongShip_%dkHz',   FreqkHz)) = DataExRaw(iChannel).alongship;
    Data.(sprintf('AthwartShip_%dkHz', FreqkHz)) = DataExRaw(iChannel).athwartship;
end

%% G�n�ration du XML SonarScope

Info.Title                  = 'Data Sample'; % Entete du fichier
Info.Constructor            = 'Simrad';
Info.Sounder                = nomSondeur;
Info.TimeOrigin             = 'Matlab';
Info.Comments               = 'NoComment';
Info.FormatVersion          = FormatVersion;
Info.InfoDatagrams          = InfoDatagrams;

Info.Dimensions.nbPings     = nbPings;
Info.Dimensions.nbChannels  = nbChannels;
Info.Dimensions.nbSamples   = nbSamples;

Info.Signals(1).Name          = 'SampleFrequency';
Info.Signals(end).Dimensions  = 'nbChannels,1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'Hz';
Info.Signals(end).FileName    = fullfile('Ssc_Sample','SampleFrequency.bin');
Info.Signals(end).Tag         = verifKeyWord('SampleFrequency');

Info.Signals(end+1).Name      = 'SoundVelocity';
Info.Signals(end).Dimensions  = 'nbChannels,1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'm/s';
Info.Signals(end).FileName    = fullfile('Ssc_Sample','SoundVelocity.bin');
Info.Signals(end).Tag         = verifKeyWord('SounderSoundSpeed');

Info.Signals(end+1).Name      = 'Mode';
Info.Signals(end).Dimensions  = 'nbChannels,1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = ' ';
Info.Signals(end).FileName    = fullfile('Ssc_Sample','Mode.bin');
Info.Signals(end).Tag         = verifKeyWord('SounderMode');

Info.Signals(end+1).Name      = 'TransducerDepth';
Info.Signals(end).Dimensions  = 'nbChannels,1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'm';
Info.Signals(end).FileName    = fullfile('Ssc_Sample','TransducerDepth.bin');
Info.Signals(end).Tag         = verifKeyWord('TransducerInstallationDepth');

Info.Signals(end+1).Name      = 'AcousticFrequency';
Info.Signals(end).Dimensions  = 'nbChannels,1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'Hz';
Info.Signals(end).FileName    = fullfile('Ssc_Sample','AcousticFrequency.bin');
Info.Signals(end).Tag         = verifKeyWord('AcousticFrequency');

Info.Signals(end+1).Name      = 'AbsorptionCoefficient';
Info.Signals(end).Dimensions  = 'nbChannels,1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'dB/km';
Info.Signals(end).FileName    = fullfile('Ssc_Sample','AbsorptionCoefficient.bin');
Info.Signals(end).Tag         = verifKeyWord('AbsorptionOfSound');

Info.Signals(end+1).Name      = 'PulseDuration';
Info.Signals(end).Dimensions  = 'nbChannels,1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'us';
Info.Signals(end).FileName    = fullfile('Ssc_Sample','PulseDuration.bin');
Info.Signals(end).Tag         = verifKeyWord('PulseDuration');

Info.Signals(end+1).Name      = 'TransmitPower';
Info.Signals(end).Dimensions  = 'nbChannels,1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'dB';
Info.Signals(end).FileName    = fullfile('Ssc_Sample','TransmitPower.bin');
Info.Signals(end).Tag         = verifKeyWord('TransceiverPower');

Info.Signals(end+1).Name      = 'Bandwidth';
Info.Signals(end).Dimensions  = 'nbChannels,1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'Hz';
Info.Signals(end).FileName    = fullfile('Ssc_Sample','Bandwidth.bin');
Info.Signals(end).Tag         = verifKeyWord('Bandwidth');

Info.Signals(end+1).Name      = 'SampleInterval';
Info.Signals(end).Dimensions  = 'nbChannels,1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 's';
Info.Signals(end).FileName    = fullfile('Ssc_Sample','SampleInterval.bin');
Info.Signals(end).Tag         = verifKeyWord('TimeSampleInterval');

Info.Signals(end+1).Name      = 'Gain';
Info.Signals(end).Dimensions  = 'nbChannels,1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'dB';
Info.Signals(end).FileName    = fullfile('Ssc_Sample','Gain.bin');
Info.Signals(end).Tag         = verifKeyWord('BeamGain');

Info.Signals(end+1).Name      = 'SaCorrection';
Info.Signals(end).Dimensions  = 'nbChannels,1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'dB';
Info.Signals(end).FileName    = fullfile('Ssc_Sample','SaCorrection.bin');
Info.Signals(end).Tag         = verifKeyWord('BeamSaCorrection');

Info.Images = [];
Info.Images(1).Name          = 'NbSamplesByChannel';
Info.Images(end).Dimensions  = 'nbPings,nbChannels';
Info.Images(end).Storage     = 'single';
Info.Images(end).Unit        = '';
Info.Images(end).FileName    = fullfile('Ssc_Sample', 'NbSamplesByChannel.bin');
Info.Images(end).Tag         = verifKeyWord('SounderDetectionNbSamples');

for iChannel=1:nbChannels
    FreqkHz = floor(Data.AcousticFrequency(iChannel)/1000);
    
    nomVar = sprintf('Amplitude_%dkHz', FreqkHz);
    if isfield(Data, nomVar)
        Info.Images(end+1).Name      = nomVar;
        Info.Images(end).Dimensions  = 'nbPings,nbSamples';
        Info.Images(end).Storage     = 'single';
        Info.Images(end).Unit        = 'Amp';
        Info.Images(end).FileName    = fullfile('Ssc_Sample', [sprintf('Amplitude_%dkHz', FreqkHz) '.bin']);
        Info.Images(end).Tag         = verifKeyWord('TODO');
    end
       
    nomVar = sprintf('AlongShip_%dkHz', FreqkHz);
    if isfield(Data, nomVar)
        Info.Images(end+1).Name      = nomVar;
        Info.Images(end).Dimensions  = 'nbPings,nbSamples';
        Info.Images(end).Storage     = 'single';
        Info.Images(end).Unit        = 'deg';
        Info.Images(end).FileName    = fullfile('Ssc_Sample', [sprintf('AlongShip_%dkHz', FreqkHz) '.bin']);
        Info.Images(end).Tag         = verifKeyWord('AlongShipPhase');
    end
    
    nomVar = sprintf('AthwartShip_%dkHz', FreqkHz);
    if isfield(Data, nomVar)
        Info.Images(end+1).Name      = nomVar;
        Info.Images(end).Dimensions  = 'nbPings,nbSamples';
        Info.Images(end).Storage     = 'single';
        Info.Images(end).Unit        = 'deg';
        Info.Images(end).FileName    = fullfile('Ssc_Sample', [sprintf('AthwartShip_%dkHz', FreqkHz) '.bin']);
        Info.Images(end).Tag         = verifKeyWord('AthwartShipPhase');
    end

    nomVar = sprintf('Sv_%dkHz', FreqkHz);
    if isfield(Data, nomVar)
        Info.Images(end+1).Name      = nomVar;
        Info.Images(end).Dimensions  = 'nbPings,nbSamples';
        Info.Images(end).Storage     = 'single';
        Info.Images(end).Unit        = 'dB';
        Info.Images(end).FileName    = fullfile('Ssc_Sample', [sprintf('Sv_%dkHz', FreqkHz) '.bin']);
        Info.Images(end).Tag         = verifKeyWord('SvSample');
    end
    
    nomVar = sprintf('Sp_%dkHz', FreqkHz);
    if isfield(Data, nomVar)
        Info.Images(end+1).Name      = nomVar;
        Info.Images(end).Dimensions  = 'nbPings,nbSamples';
        Info.Images(end).Storage     = 'single';
        Info.Images(end).Unit        = 'dB';
        Info.Images(end).FileName    = fullfile('Ssc_Sample', [sprintf('Sp_%dkHz', FreqkHz) '.bin']);
        Info.Images(end).Tag         = verifKeyWord('SvSample');
    end
end

%% Cr�ation du fichier XML d�crivant la donn�e 

nomFicXml = fullfile(nomDirRacine, 'Ssc_Sample.xml');
xml_write(nomFicXml, Info);
flag = exist(nomFicXml, 'file');
if ~flag
    messageErreurFichier(nomFicXml);
    return
end

%% Cr�ation du r�pertoire File Heading

nomDirSscData = fullfile(nomDirRacine, 'Ssc_Sample');
if ~exist(nomDirSscData, 'dir')
    status = mkdir(nomDirSscData);
    if ~status
        messageErreur(nomDirSscData)
        flag = 0;
        return
    end
end

%% Cr�ation des fichiers binaires des signaux

for k=1:numel(Info.Signals)
    flag = writeSignal(nomDirRacine, Info.Signals(k), Data.(Info.Signals(k).Name));
    if ~flag
        return
    end
end

%% Cr�ation des fichiers binaires des images

for k=1:numel(Info.Images)
    Image = Data.(Info.Images(k).Name);
    flag = writeSignal(nomDirRacine, Info.Images(k), Image);
    if ~flag
        return
    end
end

flag = 1;


function myVar = my_unique(var, varargin)

[varargin, nameVariable] = getPropertyValue(varargin, 'NameVariable', []); %#ok<ASGLU>

if isempty(nameVariable)
    nameVariable = inputname(1);
end
[myVar, ia, ~] = unique(var);
if ~isscalar(ia)
    str1 = ['Variation de la variable au cours des acquistions. Pr�voir le traitement sous forme d''image de la variable : ' nameVariable];
    str2 = ['Variation in data variable during Acquisition. Please, modify SSC for processing those variables as Images for variable : ' nameVariable];
    %  my_warndlg(Lang(str1,str2), 0); % Modif JMA le 16/10/2020 pour //Tbx
    fprintf('%s\n', Lang(str1,str2));
    myVar = var(1);
end
