function [flag] = EK80Raw2ssc_Position(nomDirRacine, InfoDatagrams, DataExRaw, DataSensor,  FormatVersion)

% On suppose les m�mes dimensions pour les N Channels.
% nbSamples   = DataExRaw(1).Dim.nbSamples;
% nbPings     = DataExRaw(1).Dim.nbPings;
% nbChannels  = DataExRaw(1).Dim.nbChannels;
% nomSondeur  = DataExRaw(1).nomSondeur;

% Modif JMA le 12/02/2021
nbChannels  = length(DataExRaw);

% Cas du fichier ESSTECH21-D20210424-T143657.raw
% 4 channels, 3 de longueur 56 et le quatri�me de longueur 57
nbPings = min(arrayfun(@(x) numel((x.pingtime)), DataExRaw));

nomSondeur  = 'EK80';
 
for iChannel=1:nbChannels
% %     resol   =  1500 * median(DataExRaw(iChannel).sampleinterval, 'omitnan') / 2;
    AcousticFrequency(iChannel) = median(DataExRaw(iChannel).frequency, 'omitnan'); %#ok<AGROW>
    
    Immersion = -DataExRaw(iChannel).transducerdepth;
    Roll      =  DataExRaw(iChannel).roll;
    Pitch     =  DataExRaw(iChannel).pitch;
    Heave     =  DataExRaw(iChannel).heave;
    
    if all(Immersion == 0) % Ajout JMA le 30/01/2018 pour donn�es Carla SEEK-LEAK AUV !!!
        Immersion = -Heave;
        Heave(:) = 0; % Ajout JMA le 31/01/2018 : � confirmer
    end
        
    PingTime = DataExRaw(iChannel).pingtime(:); 
    % T0     = datenum('01-jan-1601', 'dd-mmm-yyyy');
    % Si pingtime en fractions de jours depuis le 01/01/0000
    % PingTime = T0 + PingTime*1.e-9*100/(3600*24);
  
    % Si la navigation n'est pas lue.
    if isempty(DataSensor)
        PingLatitude  = NaN(1, size(PingTime, 1));
        PingLongitude = NaN(1, size(PingTime, 1));
        PingHeading   = NaN(1, size(PingTime, 1));    
        PingSpeed     = NaN(size(PingLatitude), 'single');
    else
        if ~isfield(DataSensor.Nav, 'NavIndex')
            DataSensor.Nav.NavIndex = (1:numel(DataSensor.Nav.Latitude))';
        end
        if ~isfield(DataSensor.Course, 'HeadingVesselIndex')
            DataSensor.Course.HeadingVesselIndex = (1:numel(DataSensor.Course.Heading))';
        end
        if isfield(DataSensor.Course, 'Time') && isfield(DataSensor.Nav, 'Time')
            PingHeading   = my_interp1_headingDeg(DataSensor.Course.Time, DataSensor.Course.HeadingVessel, PingTime, 'linear', 'extrap');
            PingLongitude = my_interp1_longitude( DataSensor.Nav.Time,    DataSensor.Nav.Longitude,        PingTime, 'linear', 'extrap');
            PingLatitude  = my_interp1(DataSensor.Nav.Time,               DataSensor.Nav.Latitude,         PingTime, 'linear', 'extrap');
        else
            if isempty(DataSensor.Course.HeadingVesselIndex)
                N = length(DataExRaw(iChannel).channelIndex);
                PingHeading = zeros(1,N);
            else
                PingHeading = my_interp1_headingDeg(DataSensor.Course.HeadingVesselIndex, DataSensor.Course.HeadingVessel, DataExRaw(iChannel).channelIndex, 'linear', 'extrap');
            end
            PingLongitude = my_interp1_longitude(DataSensor.Nav.NavIndex, DataSensor.Nav.Longitude, DataExRaw(iChannel).channelIndex, 'linear', 'extrap');
            PingLatitude  = my_interp1(DataSensor.Nav.NavIndex, DataSensor.Nav.Latitude,  DataExRaw(iChannel).channelIndex, 'linear', 'extrap');
        end
        if ~isfield(DataSensor.Course, 'SpeedIndex') || isempty(DataSensor.Course.SpeedIndex)
            PingSpeed = NaN(size(PingLatitude), 'single');
        else
            PingSpeed = my_interp1(DataSensor.Course.SpeedIndex, DataSensor.Course.SpeedInKnots, DataExRaw(iChannel).channelIndex, 'linear', 'extrap');
            PingSpeed = PingSpeed * (1852 / 3600);
        end
    end
end

%% Pr�paratifs

% On ne traite que les Pings communs
subPing         = 1:nbPings;
Data.Time      = PingTime(subPing)';
Data.Tide      = zeros(nbPings, 1, 'single');
Data.Height    = zeros(nbPings, 1, 'single');
Data.Latitude  = PingLatitude(subPing)';
Data.Longitude = PingLongitude(subPing)';
Data.Heading   = PingHeading(subPing)';
Data.Speed     = PingSpeed(subPing)';

Data.Roll      = Roll(subPing);
Data.Pitch     = Pitch(subPing);
Data.Heave     = Heave(subPing);
Data.Immersion = Immersion(subPing);

Data.Frequency = AcousticFrequency;

for k=1:nbChannels
    FreqkHz = floor(AcousticFrequency(k)/1000);
    Data.(sprintf('Amplitude_%dkHz',   FreqkHz)) = DataExRaw(k).power(subPing,:); % Ajout (subPings,:) par JMA le 30/04/2021
% % % %     Data.(sprintf('Angle_%dkHz',       FreqkHz)) = DataExRaw(k).angle;
    Data.(sprintf('AlongShip_%dkHz',   FreqkHz)) = DataExRaw(k).alongship(subPing,:); % Ajout (subPings,:) par JMA le 30/04/2021
    Data.(sprintf('AthwartShip_%dkHz', FreqkHz)) = DataExRaw(k).athwartship(subPing,:); % Ajout (subPings,:) par JMA le 30/04/2021
end

%% Ecriture du fichier de r�f�rence de Nav si les positions Lat/Lon sont correctes

if ~all(isnan(PingLatitude))
    Data.NavigationFile = 'internal';    
else
    Data.NavigationFile = '';
end

%% G�n�ration du XML SonarScope

Info.Title                  = 'Position'; % Entete du fichier
Info.Constructor            = 'Simrad';
Info.Sounder                = nomSondeur;
Info.TimeOrigin             = 'Matlab';
Info.Comments               = 'Position and Attitude';
Info.FormatVersion          = FormatVersion;
Info.InfoDatagrams          = InfoDatagrams;

Info.Dimensions.nbPings     = nbPings;
% Info.Dimensions.nbChannels  = nbChannels;
% Info.Dimensions.nbSamples   = nbSamples;

Info.Signals(1).Name          = 'Time';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = 'days since JC';
Info.Signals(end).FileName    = fullfile('Ssc_Position','Time.bin');
Info.Signals(end).Tag         = verifKeyWord('SounderTime');

Info.Signals(end+1).Name      = 'Tide';
Info.Signals(end).Dimensions  = 'nbPings,1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'sample';
Info.Signals(end).FileName    = fullfile('Ssc_Position','Tide.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'Height';
Info.Signals(end).Dimensions  = 'nbPings,1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'sample';
Info.Signals(end).FileName    = fullfile('Ssc_Position','Height.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'Latitude';
Info.Signals(end).Dimensions  = 'nbPings,1';
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile('Ssc_Position','Latitude.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'Longitude';
Info.Signals(end).Dimensions  = 'nbPings,1';
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile('Ssc_Position','Longitude.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'Heading';
Info.Signals(end).Dimensions  = 'nbPings,1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile('Ssc_Position','Heading.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'Speed';
Info.Signals(end).Dimensions  = 'nbPings,1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'm/s';
Info.Signals(end).FileName    = fullfile('Ssc_Position','Speed.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'Roll';
Info.Signals(end).Dimensions  = 'nbPings,1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile('Ssc_Position','Roll.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'Pitch';
Info.Signals(end).Dimensions  = 'nbPings,1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile('Ssc_Position','Pitch.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'Heave';
Info.Signals(end).Dimensions  = 'nbPings,1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'm';
Info.Signals(end).FileName    = fullfile('Ssc_Position','Heave.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'Immersion';
Info.Signals(end).Dimensions  = 'nbPings,1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'm';
Info.Signals(end).FileName    = fullfile('Ssc_Position','Immersion.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

%
Info.Strings(1).Name          = 'NavigationFile';
Info.Strings(end).Dimensions  = '1, 1';
Info.Strings(end).Storage     = 'char';
Info.Strings(end).Unit        = '';
Info.Strings(end).FileName    = fullfile('Ssc_Position','NavigationFile.bin');
Info.Strings(end).Tag         = verifKeyWord('SounderTime');

%% Cr�ation du fichier XML d�crivant la donn�e 

nomFicXml = fullfile(nomDirRacine, 'Ssc_Position.xml');
xml_write(nomFicXml, Info);
flag = exist(nomFicXml, 'file');
if ~flag
    messageErreurFichier(nomFicXml);
    return
end

%% Cr�ation du r�pertoire File Heading

nomDirSscData = fullfile(nomDirRacine, 'Ssc_Position');
if ~exist(nomDirSscData, 'dir')
    status = mkdir(nomDirSscData);
    if ~status
        messageErreur(nomDirSscData)
        flag = 0;
        return
    end
end

%% Cr�ation des fichiers binaires des signaux

for k=1:numel(Info.Signals)
    flag = writeSignal(nomDirRacine, Info.Signals(k), Data.(Info.Signals(k).Name));
    if ~flag
        return
    end
end

%% Cr�ation des fichiers binaires des signaux

for k=1:numel(Info.Strings)
    flag = writeString(nomDirRacine, Info.Strings(k), Data.(Info.Strings(k).Name));
    if ~flag
        return
    end
end

flag = 1;
