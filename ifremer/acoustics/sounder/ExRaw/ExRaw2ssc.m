function flag = ExRaw2ssc(nomFic, varargin)

FormatVersion = 20131105;

%% Test d'existence du fichier

flag = exist(nomFic, 'file');
if ~flag
    str1 = sprintf('Le fichier "%s" n''existe pas.', nomFic);
    str2 = sprintf('File "%s" does not exist.', nomFic);
    my_warndlg(Lang(str1,str2), 1);
    return
end

[nomDirRacine, nomFicSeul] = fileparts(nomFic);

% Test d'existence du Cache complet..
flag = [];
flag(end+1) = exist(nomDirRacine, 'dir');

nomDirRacine = fullfile(nomDirRacine, 'SonarScope', nomFicSeul);
unzipSonarScopeCache(nomDirRacine);

flag(end+1) = exist(fullfile(nomDirRacine, 'Ssc_Sample.xml'), 'file');
flag(end+1) = exist(fullfile(nomDirRacine, 'Ssc_Sample'), 'dir');
  
if all(flag) && (getFormatVersion(nomDirRacine) >= FormatVersion)
    flag = 1;
    return
end

%% Cr�ation du r�pertoire cache

[nomDirRacine, nomFicSeul] = fileparts(nomFic);
nomDirRacine = fullfile(nomDirRacine, 'SonarScope');
if ~exist(nomDirRacine, 'dir')
    [flag, message] = mkdir(nomDirRacine);
    if ~flag
        messageErreurFichier(nomDirRacine, 'WriteFailure', 'Message', message);
        return
    end
end

nomDirRacine = fullfile(nomDirRacine, nomFicSeul);
if ~exist(nomDirRacine, 'dir')
     [flag, message] = mkdir(nomDirRacine);
    if ~flag
        messageErreurFichier(nomDirRacine, 'WriteFailure', 'Message', message);
        return
    end
end

%% Lecture en Matlab pour l'instant du fichier .raw

[flag, InfoDatagrams, DataHeader, DataSensor, DataSample] = read_DataExRaw(nomFic);

if ~flag
    %{
    % nomFic =
    '\\meskl3\mission\Stage_ENSTA_huitres_Plates\20190625_roz_EK80\CW_50_170\D20190625-T100713.raw';
    RawData = ReadEK80Data(nomFic);
    BasicProcessedData = BasicProcessData(RawData);
    %}
    return
end

% Flag de pr�sence de la navigation, qui peut �tre recharger ult�rieurement.

flag = exist(nomFic, 'file');
if ~flag
    str1 = sprintf('Le fichier "%s" n''existe pas.', nomFic);
    str2 = sprintf('File "%s" does not exist.', nomFic);
    my_warndlg(Lang(str1,str2), 1);
    return
end


nbSamples  = max(DataHeader.nbSamplesMax);
% GLU le 23/05/20114 : cas du fichier D20130713-T225801.raw o� le
% nombre de samples n'est pas identiques par channel.
nbPings    = min(DataSample.k_iChannel); % size(DataSample.Data,2);
nbChannels = DataHeader.nbChannels;

for iChannel=1:nbChannels
    DataExRaw(iChannel).mode                    = NaN(nbPings, 1, 'single'); %#ok<AGROW>
    DataExRaw(iChannel).transducerdepth         = NaN(nbPings, 1, 'single'); %#ok<AGROW>
    DataExRaw(iChannel).frequency               = NaN(nbPings, 1, 'single'); %#ok<AGROW>
    DataExRaw(iChannel).transmitpower           = NaN(nbPings, 1, 'single'); %#ok<AGROW>
    DataExRaw(iChannel).pulselength             = NaN(nbPings, 1, 'single'); %#ok<AGROW>
    DataExRaw(iChannel).bandwidth               = NaN(nbPings, 1, 'single'); %#ok<AGROW>
    DataExRaw(iChannel).sampleinterval          = NaN(nbPings, 1, 'single'); %#ok<AGROW>
    DataExRaw(iChannel).soundvelocity           = NaN(nbPings, 1, 'single'); %#ok<AGROW>
    DataExRaw(iChannel).absorptioncoefficient   = NaN(nbPings, 1, 'single'); %#ok<AGROW>
    DataExRaw(iChannel).heave                   = NaN(nbPings, 1, 'single'); %#ok<AGROW>
    DataExRaw(iChannel).roll                    = NaN(nbPings, 1, 'single'); %#ok<AGROW>
    DataExRaw(iChannel).pitch                   = NaN(nbPings, 1, 'single'); %#ok<AGROW>
    DataExRaw(iChannel).temperature             = NaN(nbPings, 1, 'single'); %#ok<AGROW>
    DataExRaw(iChannel).trawlupperdepthvalid    = NaN(nbPings, 1, 'single'); %#ok<AGROW>
    DataExRaw(iChannel).trawlopeningvalid       = NaN(nbPings, 1, 'single'); %#ok<AGROW>
    DataExRaw(iChannel).trawlupperdepth         = NaN(nbPings, 1, 'single'); %#ok<AGROW>
    DataExRaw(iChannel).trawlopening            = NaN(nbPings, 1, 'single'); %#ok<AGROW>
    DataExRaw(iChannel).offset                  = NaN(nbPings, 1, 'single'); %#ok<AGROW>
    DataExRaw(iChannel).count                   = NaN(nbPings, 1, 'single'); %#ok<AGROW>
    DataExRaw(iChannel).power                   = NaN(nbPings, nbSamples, 'single'); %#ok<AGROW>
    DataExRaw(iChannel).sv                      = NaN(nbPings, nbSamples, 'single'); %#ok<AGROW>
% % % %     DataExRaw(iChannel).angle                   = NaN(nbPings, nbSamples, 'single'); %#ok<AGROW>
    DataExRaw(iChannel).alongship               = NaN(nbPings, nbSamples, 'single'); %#ok<AGROW>
    DataExRaw(iChannel).athwartship             = NaN(nbPings, nbSamples, 'single'); %#ok<AGROW>
end


%% R�partition des SampleData issus de la lecture des diff�rents paquets.

for iChannel=1:nbChannels
    DataExRaw(iChannel).Dim.nbPings             = nbPings;
    DataExRaw(iChannel).Dim.nbSamples           = nbSamples;
    DataExRaw(iChannel).Dim.nbChannels          = nbChannels;
    DataExRaw(iChannel).nomSondeur              = DataHeader.Config.header.soundername;    
    DataExRaw(iChannel).TransceiverSWVersion    = DataHeader.Config.header.version;        
    DataExRaw(iChannel).mode                    = [DataSample.Data(iChannel,1:nbPings).mode]';
    DataExRaw(iChannel).transducerdepth         = [DataSample.Data(iChannel,1:nbPings).transducerdepth]';
    DataExRaw(iChannel).frequency               = [DataSample.Data(iChannel,1:nbPings).frequency]';
    DataExRaw(iChannel).transmitpower           = [DataSample.Data(iChannel,1:nbPings).transmitpower]';
    DataExRaw(iChannel).pulselength             = [DataSample.Data(iChannel,1:nbPings).pulselength]';
    DataExRaw(iChannel).bandwidth               = [DataSample.Data(iChannel,1:nbPings).bandwidth]';
    DataExRaw(iChannel).sampleinterval          = [DataSample.Data(iChannel,1:nbPings).sampleinterval]';
    DataExRaw(iChannel).soundvelocity           = [DataSample.Data(iChannel,1:nbPings).soundvelocity]';
    DataExRaw(iChannel).absorptioncoefficient   = [DataSample.Data(iChannel,1:nbPings).absorptioncoefficient]';
    DataExRaw(iChannel).gain                    = [DataSample.Data(iChannel,1:nbPings).Gain]';
    DataExRaw(iChannel).sacorrection            = [DataSample.Data(iChannel,1:nbPings).SaCorrection]';
    DataExRaw(iChannel).heave                   = [DataSample.Data(iChannel,1:nbPings).heave]';
    DataExRaw(iChannel).roll                    = [DataSample.Data(iChannel,1:nbPings).roll]';
    DataExRaw(iChannel).pitch                   = [DataSample.Data(iChannel,1:nbPings).pitch]';
    DataExRaw(iChannel).temperature             = [DataSample.Data(iChannel,1:nbPings).temperature]';
    DataExRaw(iChannel).trawlupperdepthvalid    = [DataSample.Data(iChannel,1:nbPings).trawlupperdepthvalid]';
    DataExRaw(iChannel).trawlopeningvalid       = [DataSample.Data(iChannel,1:nbPings).trawlopeningvalid]';
    DataExRaw(iChannel).trawlupperdepth         = [DataSample.Data(iChannel,1:nbPings).trawlupperdepth]';
    DataExRaw(iChannel).trawlopening            = [DataSample.Data(iChannel,1:nbPings).trawlopening]';
    DataExRaw(iChannel).offset                  = [DataSample.Data(iChannel,1:nbPings).offset]';
    DataExRaw(iChannel).count                   = [DataSample.Data(iChannel,1:nbPings).count]';
    DataExRaw(iChannel).pingtime                = [DataSample.Data(iChannel,1:nbPings).pingtime]';
    DataExRaw(iChannel).channelIndex            = DataSample.ChannelIndex(iChannel,1:nbPings);

    for kk=1:nbPings    
        sub = 1:DataSample.Data(iChannel,kk).count;
        DataExRaw(iChannel).power(kk,sub) = DataSample.Data(iChannel,kk).power(:)';
        DataExRaw(iChannel).sv(kk,sub)    = DataSample.Data(iChannel,kk).Sv(:)';
        if isfield(DataSample.Data, 'alongship') && ~isempty(DataSample.Data(iChannel,kk).alongship(:))
% % % %             DataExRaw(iChannel).angle(kk,sub)               = DataSample.Data(iChannel,kk).angle(:)'; %#ok<AGROW>
            DataExRaw(iChannel).alongship(kk,sub)   = DataSample.Data(iChannel,kk).alongship(:)'/DataHeader.Config.transducer(iChannel).anglesensitivityalongship;
            DataExRaw(iChannel).athwartship(kk,sub) = DataSample.Data(iChannel,kk).athwartship(:)'/DataHeader.Config.transducer(iChannel).anglesensitivityathwartship;
        else
            %                 'On passe par ici'
        end
    end
end
%{
figure; plot(Longitude, Latitude); grid on
figure; PlotUtils.createSScPlot(DataExRaw(1).HeadingTrack); grid on; title('HeadingTrack');
figure; PlotUtils.createSScPlot(DataExRaw(1).SpeedInKnots); grid on; title('SpeedInKnots');
figure; PlotUtils.createSScPlot(DataExRaw(1).HeadingVessel); grid on; title('HeadingVessel');
figure; PlotUtils.createSScPlot(DataExRaw(1).HeureNav); grid on; title('HeureNav');
figure; PlotUtils.createSScPlot(DataExRaw(1).Time); grid on; title('Time');
%}

clear DataSample
%% Ecriture des paquets.
% Datagrammes Position

% Prise en charge (sans le 1er niveau ce cache) des donn�es issues de la
% lecture.
flag = ExRaw2ssc_ConfigHeader(nomDirRacine, InfoDatagrams, DataHeader.Config.header, FormatVersion);
if ~flag
    return
end

flag = ExRaw2ssc_ConfigTransducer(nomDirRacine, InfoDatagrams, DataExRaw, DataHeader.Config.transducer,  FormatVersion);
if ~flag
    return
end

flag = ExRaw2ssc_Position(nomDirRacine, InfoDatagrams, DataExRaw, DataSensor, FormatVersion);
if ~flag
    return
end

flag = ExRaw2ssc_DataSample(nomDirRacine, InfoDatagrams, DataExRaw, FormatVersion);
if ~flag
    return
end

flag = 1;


function FormatVersion = getFormatVersion(nomDirRacine)

nomFicXml = fullfile(nomDirRacine, 'Ssc_Sample.xml');
flag = exist(nomFicXml, 'file');        
if ~flag
    special_message('FichierNonExistant', nomFicXml)
    return
end
% edit(nomFicXml)
Datagrams = xml_read(nomFicXml);
if ~isfield(Datagrams, 'FormatVersion')
    FormatVersion = 0;
    return
end
FormatVersion = Datagrams.FormatVersion;
