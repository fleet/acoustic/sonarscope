function ExRaw_ImportNavigation(~, nomFicExRaw, nomFicNav)

if ~iscell(nomFicExRaw)
    nomFicExRaw = {nomFicExRaw};
end

Carto = [];
N = length(nomFicExRaw);
hw = create_waitbar('ExRaw navigation import', 'N', N);
for k=1:N
    my_waitbar(k, N, hw);
    [flag, Carto] = ExRaw_ImportNavigation_unitaire(nomFicExRaw{k}, nomFicNav, Carto);
    if ~flag
        my_close(hw)
        return
    end
end
my_close(hw, 'MsgEnd')



function [flag, Carto] = ExRaw_ImportNavigation_unitaire(nomFicExRaw, nomFicNav, Carto)

this = cl_ExRaw('nomFic', nomFicExRaw);
if isempty(this)
    flag = 0;
    return
end

[flag, DataPosition] = SSc_ReadDatagrams(nomFicExRaw, 'Ssc_Position');
if ~flag
    return
end

T              = DataPosition.Time.timeMat;
LatitudeImage  = DataPosition.Latitude(:);
LongitudeImage = DataPosition.Longitude(:);           

DonneesModifiees = 0;
for k=1:length(nomFicNav)
    [flag, Data]        = import_nav(nomFicNav{k});
    TExternalNavigation = Data.Time;
    Longitude   = Data.Nav.lon;
    Latitude    = Data.Nav.lat;
    % Test si le fichier est dans la nav

    %         if any(T) > TExternalNavigation(end)
    if T(1) > TExternalNavigation(end)
        str1 = sprintf('Le fichier ExRaw "%s" est post�rieur � la navigation "%s".', nomFicExRaw, nomFicNav{k});
        str2 = sprintf('"%s" is posterior to "%s".', nomFicExRaw, nomFicNav{k});
        my_warndlg(Lang(str1,str2), 0, 'Tag', nomFicExRaw);
        continue
    end

    %         if any(T) < TExternalNavigation(1)
    if T(end) < TExternalNavigation(1)
        str1 = sprintf('Le fichier ExRaw "%s" est ant�rieur � la navigation "%s".', nomFicExRaw, nomFicNav{k});
        str2 = sprintf('"%s" is anterior to "%s".', nomFicExRaw, nomFicNav{k});
        my_warndlg(Lang(str1,str2), 0, 'Tag', nomFicExRaw);
        continue
    end

    LongitudeExRaw = my_interp1_longitude(TExternalNavigation, Longitude, T);
    LatitudeExRaw  = my_interp1(TExternalNavigation, Latitude,  T);

    subNonNaN = find(~isnan(LongitudeExRaw(:)));
    if ~isempty(subNonNaN)
        LatitudeImage(subNonNaN, 1)  = LatitudeExRaw(subNonNaN);
        LongitudeImage(subNonNaN, 1) = LongitudeExRaw(subNonNaN);
        DonneesModifiees = 1;
    end
end
%% Ecriture de la nav et de leur r�f�rence dans les fichiers cache

if DonneesModifiees
    % Ecriture des vecteurs
    DataPosition.Latitude  = LatitudeImage;
    DataPosition.Longitude = LongitudeImage;
    flag = write_position(this, DataPosition);
    if ~flag
        return
    end
 
    % Ecriture du fichier de r�f�rence de Nav.
    flag = write_NavigationFile(this, nomFicNav{k});
    if ~flag
        return
    end
end
