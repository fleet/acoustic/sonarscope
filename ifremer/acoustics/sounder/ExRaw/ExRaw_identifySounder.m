% ExRaw_identifySounder : fonction d'identification d'un sondeur de EK80.
%
% Syntax
%   isEK80 = ExRaw_identifySounder(nomFic)
%
% Input Arguments 
%   nomFic : nom du fichier de EK60 ou mieux EK80
%
% Name-Value Pair Arguments
%   nomFic : Nom(s) du fichier .raw
%  
% Output Arguments 
%   isEK80
%
% Examples
%
%   % EK60
%   nomFic      = 'E:\SonarScopeAnnexes\SonarScopeDataArchive\From Anne Le Vourch - IRD\D20130714-T192934.raw';
%
%   % EK80
%   nomFic      = 'E:\SonarScopeAnnexes\SonarScopeDataArchive\From Rida\D20190625-T100713.raw';
%   a           = cl_ExRaw('nomFic', nomFic)
%
% See also CONVERT_HAC.exe Authors
% Authors : GLU
% ----------------------------------------------------------------------------

function isEK80 = ExRaw_identifySounder(nomFic)

isEK80 = 0;

headerLength = 12; % Bytes in datagram header
fileId       = fopen(nomFic,'r');

datagramLength = fread(fileId,1,'int32');

% Lecture obligatoire du DatagramHeader.
DatagramHeader = ReadDatagramHeader(fileId);

[XmlDataUnparsed, textData] = ReadXmlData(fileId,datagramLength-headerLength); %#ok<ASGLU>
if isempty(XmlDataUnparsed)
    try
        % Tentative de d�codage du header de fichier
        fseek(fileId, 0, 'bof');
        lengthRec       = fread(fileId, 1, 'int32');
        datagramheader  = readdgheader(fileId); %#ok<*NASGU>
        configheader    = readconfigheader(fileId);
        nbChannels      = configheader.transducercount;
        isEK80          = ~contains(configheader.soundername, {'EK60', 'ER60', 'ME70'});
    catch ME
        strFr = sprintf('Format de fichier RAW issu d''un sondeur non identifi�. Merci de contacter JM Augustin-IFR\n');
        strUs = sprintf('Raw format from unidentified sounder. Please contact JM Augustin-IFR.\n');
        my_warndlg(Lang(strFr, strUs), 0);
    end
else
    XmlData             = ParseXmlData(XmlDataUnparsed);
    ConfigurationData   = ParseConfigurationXmlData(XmlData);
    if strcmpi(ConfigurationData.Header.ApplicationName, 'EK80')
        isEK80 = 1;
    end
end
fclose (fileId);
