% Cr�ation d'un fichier HAC � partir d'une arborescence SSC_ (extraite
% d'un fichier RAW)
%
% Syntax
%   ExRaw_ssc2hac(...)
%
% Name-Value Pair Arguments
%   nomFic      : Nom d'un fichier ou liste de fichiers .raw.
%   nomDir      : Nom du r�pertoire
%   SeuilSample : seuil de compression (-100 dB par d�faut)
%
% Examples
%   nomFic = getNomFicDatabase('FK-D20120510-T202356.raw')%
%   flag    = ExRaw_ssc2hac('NomFicRaw', nomFic, 'RepDefaut', 'C:\Temp\Test_HAC_Creation');
%
% See also ssc2hac pour des fichiers ALL Authors
%-----------------------------------------------------------------------

function flag = ExRaw_ssc2hac(varargin)

[varargin, nomDir]      = getPropertyValue(varargin, 'NomDir',      []);
[varargin, nomFic]      = getPropertyValue(varargin, 'NomFicRaw',   []);
[varargin, SeuilSample] = getPropertyValue(varargin, 'SeuilSample', -100);
[varargin, RepDefaut]   = getPropertyValue(varargin, 'RepDefaut',   []); %#ok<ASGLU>

flag = 0;
if isempty(nomDir)
    if isempty(nomFic)
        [flag, nomFic] = uiSelectFiles('ExtensionFiles', '.raw', 'RepDefaut', RepDefaut);
    else
        if ischar(nomFic)
            nomFic = {nomFic};
        end
    end
else
    nomFic = listeFicOnDir('C:\IfremerToolboxDataPublic\Level1', '.raw');
end

[path, ~, ~] = fileparts(nomFic{1});

if isempty(RepDefaut)
    [flag, pathOut] = my_uigetdir(path);
    if ~flag
        return
    end
else
    pathOut = RepDefaut;
end

N = length(nomFic);
str1 = 'Conversion des fichiers .raw en .hac';
str2 = 'Converting .raw files into .hac';
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    
    flag = ExRaw_ssc2hac_unitaire(nomFic{k}, SeuilSample, pathOut);
    if ~flag
        % Que fait-on ?
    end
end
my_close(hw, 'MsgEnd')


function flag = ExRaw_ssc2hac_unitaire(nomFic, SeuilSample, DirOut)

global DEBUG %#ok<GVMIS> 

a = cl_ExRaw('nomFic', nomFic);

%%
% Lecture des descriptions de datagrammes pour indexation de ceux-ci.
[pathstr, name] = fileparts(nomFic);

nomDir = fullfile(pathstr, 'SonarScope', name);
if ~exist(nomDir, 'dir')
    str1 = 'Le r�pertoire de donn�e au format SonarScope n''existe pas. Il n''est pas possible d''exporter au format Simrad.';
    str2 = 'The data directory with SonarScope format does not exist. It is not possible to export in Hac Format.';
    my_warndlg(Lang(str1,str2), 1);
end

nomFicHac = fullfile(DirOut, [name '_raw2hac.hac']);
if exist(nomFicHac, 'file')
    flag = 1;
    return
end

nameDatagramsSSC = {'ConfigHeader', 'ConfigTransducer', 'Position', 'Sample'};


minLp                   = 1;
maxLp                   = numel(nameDatagramsSSC);

for lp=minLp:maxLp
    
    nomFicXml = fullfile(nomDir, ['Ssc_' nameDatagramsSSC{lp} '.xml']);
    
    [flag, Data] = XMLBinUtils.readGrpData(nomFicXml); %#ok<ASGLU>
    DataSSC.(nameDatagramsSSC{lp}) = Data;
    %% Test si c'est le bon format
    
    if ~isfield(Data, 'FormatVersion') || (Data.FormatVersion < 20131105)
        str1 = sprintf('D�sol�, le r�pertoire SonarScope associ� au fichier "%s" est trop ancien, il vous faut le reconstruire pour pouvoir convertir les donn�es de "Water Column" en HAC.', nomFic);
        str2 = sprintf('Sorry, the SonarScope folder corresponding to "%s" is too old, you have to redo it in order to convert "Water Column" data into HAC.', nomFic);
        my_warndlg(Lang(str1,str2), 1);
        flag = 0;
        Datagrams.FormatVersion
        return
    end
    
    
end

%% test si il y a bien du WC dans ce fichier

if ~isfield(DataSSC, 'Sample')
    str1 = sprintf('D�sol�, le fichier "%s" ne contient pas de Water Column.', nomFic);
    str2 = sprintf('Sorry, "%s" do not contain any Water Column data.', nomFic);
    my_warndlg(Lang(str1,str2), 0, 'Tag', 'NoWCInThisFile');
    flag = 1;
    return
end



%% Indexation des paquets
% Tri des paquets par Date.
Time                = DataSSC.Position.Time.timeMat;
SamplePingCounter   = 1:size(DataSSC.Sample.Dimensions.nbPings,1);
PositionPingCounter = 1:size(DataSSC.Position.Roll,1);

Time                = num2cell(Time);
SamplePingCounter   = num2cell(SamplePingCounter);
PositionPingCounter = num2cell(PositionPingCounter);
IndexPosTuple       = num2cell((1:size(Time,1))');      % Index des ping
IndexWCTuple        = num2cell((1:size(Time,1))');        % Index des ping trait�s.

Data1 = struct( 'Date', Time, 'PingCounter', PositionPingCounter', ...
    'Type', 20, 'Index', IndexPosTuple);
Data2   = struct( 'Date', Time, 'PingCounter', SamplePingCounter', ...
    'Type',10000, 'Index', IndexWCTuple);

Ping    = struct( 'Date', {Data1.Date Data2.Date}, ...
    'PingCounter',  {Data1.PingCounter Data2.PingCounter}, ...
    'Type', {Data1.Type Data2.Type}, ...
    'Index', {Data1.Index Data2.Index});

[~, order]  = sort([Ping.Date]);
Ping        = Ping(order);

% -----------------------------------------------
% Calcul de l'histogramme des types de datagramme
% Historisation des types de Tuples �crits (cf. le s�quencement de processTuples)
tmpType = [ Ping.Type ones(1,size(Time,1))*30 ones(1,size(Time,1))*220 ...
    ones(1,size(Time,1))*2200 ...
    ones(1,size(Time,1))*41];
bins                = min(tmpType):max(tmpType);
histoTypeDatagram   = my_hist(tmpType,bins);
list                = unique(tmpType);
histoTypeDatagram   = histoTypeDatagram(histoTypeDatagram~=0);

sub = find(histoTypeDatagram ~= 0);

texteTypeDatagram   = codesTuplesHac(list, histoTypeDatagram);

if DEBUG
    hdlFig = figure; %#ok<NASGU>
    hBar    = bar(list(sub), histoTypeDatagram(sub)); %#ok<NASGU>
    set(gca, 'Position', [0.08  0.11 0.50 0.815]);
    hText   = text(13000,mean(histoTypeDatagram(sub)),max(histoTypeDatagram(sub)), texteTypeDatagram, ...
        'FontSize', 8, 'VerticalAlignment', 'top'); %#ok<NASGU>
end
NMax = numel(Ping);
nbTuples = histoTypeDatagram(list == Ping(1).Type);
fprintf('Packet 1 - %s  %s\n', Ping(1).Date(1), codesTuplesHac(Ping(1).Type, nbTuples));
nbTuples = histoTypeDatagram(list == Ping(end).Type);
fprintf('Packet %d - %s  %s\n', NMax, Ping(end).Date(1), codesTuplesHac(Ping(end).Type, nbTuples));



%% Traitement des paquets.

% Chargement des donn�es utiles au HAC depuis les caches de SonarScope.
[~, DataTuple.Position, DataTuple.Attitude]  = loadPosNavAttDataFromBinFile(DataSSC.Position);
[~, DataTuple.Config]                        = loadConfigDataFromBinFile(DataSSC.ConfigHeader, DataSSC.ConfigTransducer);
[~, DataTuple.DataSample]                    = loadSampleDataFromBinFile(DataSSC.Position.Time, DataSSC.ConfigTransducer, DataSSC.Sample);

nomFicOut   = fullfile(pathstr, [name '_raw2hac.hac_tmp']);
% Boucle sur l'ensemble des paquets recens�s dans les formats Kongsberg.
if exist(nomFicOut, 'file')
    delete(nomFicOut);
end
% Chargement des donn�es pour les datagrammes identifi�s.
NbChannels = DataSSC.ConfigTransducer.Dimensions.nbChannels;
SounderName = DataSSC.ConfigHeader.Sounder;

% On se contente de la mise en forme induite par le chargement pr�c�dent.
clear DataSSC;

% Ouverture du fichier pour enrichissement � chaque paquet.
fid = fopen(nomFicOut, 'w+');

% Traitement des tuples selon le type de fichier en jeu.
file.Class = class(a);
file.fid   = fid;
% flag       = processTuples(file, nomDir, SeuilSample, Ping, NbChannels, SounderName, DataTuple);
flag       = processTuples(file, nomDir, Ping, NbChannels, SounderName, DataTuple, SeuilSample);
if ~flag
    return
end

fclose(fid);

try
    movefile(nomFicOut, nomFicHac, 'f')
catch %#ok<CTCH>
    str1 = sprintf('Le fichier "%s" n''a pas pu �tre renomm� en "%s", tentez de le faire manuellement apr�s �tre sorti de SonarScope.', nomFicOut, nomFicHac);
    str2 = sprintf('"%s" could not be moved in "%s", try to do it by hand after having closed SonarScope.', nomFicOut, nomFicHac);
    my_warndlg(Lang(str1,str2), 1);
end

