% Extract backscatter of EK60 data

function [RangeDetection, BS, H1Gate, H2Gate] = ExtractBackscatterMagPhaseEK60(AmpImage, PhaseImage, SampleRate)

%% D�tection of beam footprint on amplitude signal

[nbSamples, nbPings] = size(AmpImage);
TypeSignal = 2;
FiltreSonarA = [];
FiltreSonarB = [];
H1 = subbottom_compute_height_image(AmpImage', TypeSignal, FiltreSonarA, FiltreSonarB);
H2 = subbottom_compute_height_image(fliplr(AmpImage'), TypeSignal, FiltreSonarA, FiltreSonarB);
H2 = size(AmpImage, 1) - H2;
RangeDetection = (H1 + H2) / 2;
% FigUtils.createSScFigure; PlotUtils.createSScPlot(H1, 'k.');  grid on; hold on; PlotUtils.createSScPlot(H2, 'k.');  PlotUtils.createSScPlot(RangeDetection, 'b');

W = (H2 - H1);
H1Gate = H1 - W;
H2Gate = H2 + W;
H1Gate = max(floor(H1Gate), 1);
H2Gate = max(floor(H2Gate), 1);
H1Gate = min(H1Gate, nbSamples);
H2Gate = min(H2Gate, nbSamples);

BeamAngles         = repmat(45, 1, nbPings); % TODO
Frequency          = 300; % TODO
TxPulseWidth       = 0.150; % TODO
SystemSerialNumber = 7150;
DisplayLevel       = 2;
iPing              = 1;
BeamsToDisplay     = [];
nbSamplesMoyennes = 1;
parameters_BDAgetPlatform
R0 = min(H1);

for k=1:length(H1)
    AmpImage(1:H1Gate(k),k)   = NaN;
    AmpImage(H2Gate(k):end,k) = NaN;
end

%% D�tection de l'amplitude

[AmpPingRange, AmpPingQF, AmpPingSamplesNb, AmpPingQF2] = ...
    ResonDetectionAmplitudeImage(AmpImage, R0, BeamAngles, ...
    SystemSerialNumber, SampleRate, TxPulseWidth, DisplayLevel, BeamsToDisplay, iPing); %, 'IdentAlgo', IdentAlgo);
subNonNan = ~isnan(AmpPingRange);
RangeDetection(subNonNan) = AmpPingRange(subNonNan);

%% D�tection � partir de la phase

try
    H1Gate = movmean(H1, 5);
    H2Gate = movmean(H2, 5);
catch
    H1Gate = smooth(H1, 5);
    H2Gate = smooth(H2, 5);
end
H1Gate = max(floor(H1Gate), 1);
H2Gate = max(floor(H2Gate), 1);
H1Gate = min(H1Gate, nbSamples);
H2Gate = min(H2Gate, nbSamples);
for k=1:length(H1)
    AmpImage(1:H1Gate(k),k)   = NaN;
    AmpImage(H2Gate(k):end,k) = NaN;
end

[PhasePingRange, PhasePingNbSamples, PhasePingPente, PhasePingEqm, PhasePingQF] = ...
    ResonDetectionPhaseImage_3steps(AmpImage, PhaseImage, BeamAngles, R0, SampleRate, ...
    Frequency, nbSamplesMoyennes, SystemSerialNumber, iPing, TxPulseWidth, ...
    DisplayLevel, BeamsToDisplay);

%{
FigUtils.createSScFigure; PlotUtils.createSScPlot(AmpPingRange, 'b'); grid on; hold on; PlotUtils.createSScPlot(PhasePingRange, 'r');
PlotUtils.createSScPlot(H1, 'k.');  PlotUtils.createSScPlot(H2, 'k.')
PlotUtils.createSScPlot(H1Gate, 'k');  PlotUtils.createSScPlot(H2Gate, 'k')
%}

subNonNan = ~isnan(PhasePingRange);
RangeDetection(subNonNan) = PhasePingRange(subNonNan);

%% Extraction of backscatter

W = H2Gate - H1Gate;
H1GateBS = floor(RangeDetection - W/4);
H2GateBS = ceil( RangeDetection + W/4);
H1GateBS = max(floor(H1GateBS), 1);
H2GateBS = max(floor(H2GateBS), 1);
H1GateBS = min(H1GateBS, nbSamples);
H2GateBS = min(H2GateBS, nbSamples);
% subNaN = find(isnan(H1GateBS))
% subNaN = find(isnan(H2GateBS))
for k=nbPings:-1:1
    BS(k) = mean(AmpImage(H1GateBS(k):H2GateBS(k),k), 'omitnan');
end
% FigUtils.createSScFigure; PlotUtils.createSScPlot(BS, 'b'); grid on;

