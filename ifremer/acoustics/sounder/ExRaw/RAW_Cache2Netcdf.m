% [flag, dataFileName] = FtpUtils.getSScDemoFile('tan1505-D20150428-T013331.raw', 'OutputDir', 'D:\Temp\ExData')
% RAW_Cache2Netcdf(dataFileName)
%
% [flag, dataFileName] = FtpUtils.getSScDemoFile('D20190625-T100713.raw', 'OutputDir', 'D:\Temp\ExData')
% RAW_Cache2Netcdf(dataFileName)

function flag = RAW_Cache2Netcdf(dataFileName, varargin)

global useCacheNetcdf %#ok<GVMIS>

[varargin, useCacheNetcdf] = getPropertyValue(varargin, 'useCacheNetcdf', useCacheNetcdf);
[varargin, deleteXMLBin]   = getPropertyValue(varargin, 'deleteXMLBin',   0); %#ok<ASGLU>

%% Cr�ation du fichier Netcdf

[flag, nomFicNc] = SScCacheNetcdfUtils.createSScNetcdfFile(dataFileName);
if ~flag
    return
end

%% Transfert des donn�es Ssc_Sample

flag = SScCacheNetcdfUtils.Cache2Netcdf(nomFicNc, dataFileName, 'Sample');
if ~flag
    return
end

%% Transfert des donn�es Ssc_Height

flag = SScCacheNetcdfUtils.Cache2Netcdf(nomFicNc, dataFileName, 'Height');
if ~flag
    return
end

%% Transfert des donn�es Ssc_Position

flag = SScCacheNetcdfUtils.Cache2Netcdf(nomFicNc, dataFileName, 'Position');
if ~flag
    return
end

%% Delete WML cache

if deleteXMLBin
    SScCacheXMLBinUtils.deleteCachDirectory(dataFileName);
end

%% The End

flag = 1;
