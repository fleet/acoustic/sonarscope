function [flag, nomSignal] = params_RAW_PlotNavigationAndSignal

nomSignal = [];

listeSignaux = {'Time'
'Height'
'Heading'
'Speed'
'Roll'
'Pitch'
'Heave'
'SampleFrequency'
'SoundVelocity'
'Tide'
'Immersion'
'Ping Inter Dist'};

str1 = 'Nom du signal � visualiser';
str2 = 'Signal to plot';
[rep, flag] = my_listdlg(Lang(str1,str2), listeSignaux, 'SelectionMode', 'Single', 'InitialValue', 2);
if ~flag
    return
end
nomSignal = listeSignaux{rep};

% TODO : rajouter quelques signaux :
%         AcousticFrequency: [1�1 cl_memmapfile]
%     AbsorptionCoefficient: [1�1 cl_memmapfile]
%             PulseDuration: [1�1 cl_memmapfile]
%             TransmitPower: [1�1 cl_memmapfile]
%                 Bandwidth: [1�1 cl_memmapfile]
%            SampleInterval: [1�1 cl_memmapfile]
%                      Gain: [1�1 cl_memmapfile]
%              SaCorrection: [1�1 cl_memmapfile]
%        NbSamplesByChannel: [7505�1 cl_memmapfile]
%          Amplitude_120kHz: [7505�8311 cl_memmapfile]
%          AlongShip_120kHz: [7505�8311 cl_memmapfile]
%        AthwartShip_120kHz: [7505�8311 cl_memmapfile]
%                 Sv_120kHz: [7505�8311 cl_memmapfile]
