% Lecture d'un fichier RAW Data.
%
% Syntax
%   [flag, Info, DataHeader, DataSensor, Info, DataSample] = read_DataExRaw(nomFic)
% 
% Name-Value Pair Arguments
%   nomFic : Nom(s) du fichier .raw
%  
% Output Arguments 
%   flag            : indicateur de bon fontionnement
%   DataHeader      : donn�es d'ent�te
%   DataSensor      : donn�es capteur
%   Info            : info sur les datagrammes lus
%   DAtaSample      : donn�es d'�chantillons
%
% Examples
%   nomFicRaw = getNomFicDatabase('FK-D20120510-T202356.raw')
%   [flag, DataHeader, DataSensor, Info, DataSample] = read_DataExRaw(nomFicRaw);
%
%   nomFicRaw       = 'F:\SonarScopeData\From CSIRO\Raw\EK60_12Khz-D20120730-T045457.raw';
%   [flag, DataHeader, DataSensor, Info, DataSample] = read_DataExRaw(nomFicRaw);
%
% See also cl_hac Authors
% Authors : GLU
% ----------------------------------------------------------------------------

function [flag, Info, DataHeader, DataSensor, DataSample] = read_DataExRaw(nomFic, varargin)

flag = 0;

DataHeader = [];
DataSensor = [];
DataSample = [];
Info       = [];

Info.nbDatagrams  = 0;
Info.nbCON1       = 0;
Info.nbTAG0       = 0;
Info.nbRAW0       = 0;
Info.nbOther      = 0;
Info.nbINZDA      = 0;
Info.nbINGGA      = 0;
Info.nbINVTG      = 0;
Info.nbINHDT      = 0;
Info.nbSDVLW      = 0;
Info.nbAGHDT      = 0;
Info.nbAGVHW      = 0;
Info.nbGPGGA      = 0;
Info.nbGPRMC      = 0;
Info.nbGPGLL      = 0;
Info.nbNMEO_Other = 0;

nbChannels   = [];
nbPings      = [];
nbSamplesMax = [];

flag = exist(nomFic, 'file');
if ~flag
    str1 = sprintf('Le fichier "%s" n''existe pas.', nomFic);
    str2 = sprintf('File "%s" does not exist.', nomFic);
    my_warndlg(Lang(str1,str2), 1);
    return
end

headerlength = 12; % Bytes in datagram header
pingno       = 0;
nrefbeams    = 0;
echo         = [];
pingtime     = 0;
lastpingtime = 0;
pingrate     = 0;
kTime        = 0;
kNav         = 0;
kSpeed       = 0;
kINHDT       = 0;
kAGHDT       = 0;
kAGVHW       = 0;
KGPGGA       = 0;

Time          = [];
TimeIndex     = [];
NavIndex      = [];
Latitude      = [];
Longitude     = [];
HeureNav      = [];
HeadingTrack  = [];
SpeedInKnots  = [];
SpeedIndex    = [];
HeadingVessel = [];

%{
nomFic = 'G:\FSK\Course\RAW\20140527112635.raw';
fclose(fid);
fid = fopen(nomFic, 'r', 'b');
if fid == -1
    messageErreurFichier(nomFic, 'ReadFailure');
    flag = 0;
    return
end

X = fread(fid, 9, 'int16');
TraceInFile = X(1)
Format = X(2)
Year   = X(3)
Month  = X(4)
Day    = X(5)
Hour   = X(6)
Minute = X(7)
Sec    = X(8)
Msec   = X(9)

clear configheader
configheader.surveyname   = char(fread(fid, 16, 'char')');
configheader.transectname = char(fread(fid, 18, 'char')');
configheader.soundername  = char(fread(fid, 26, 'char')');
configheader
X = fread(fid, 9, 'int16');
X'

fclose(fid); 
%}        
 
fid = fopen(nomFic, 'r');
if fid == -1
    messageErreurFichier(nomFic, 'ReadFailure');
    flag = 0;
    return
end

lengthRec      = fread(fid, 1, 'int32');
datagramheader = readdgheader(fid); %#ok<*NASGU>
configheader   = readconfigheader(fid);
nbChannels     = configheader.transducercount;

% D�tection de probl�me sur le format de fichiers.
% Tentative de relecture en format Machaine diff�rent.
% Ex : \COM25-EK60-18kHz-DBS-RAW_20120702-093141.raw
if (nbChannels > 500) || (nbChannels < 0) 
    fclose(fid);
    % R�cup�ration de l'enndianness natif pour tenter l'inverse � la lecture.
    [~, ~, endian] = computer;
    if strcmp(endian, 'L')
        typeEndian = 'b';
    else
        typeEndian = 'l';
    end
    fid = fopen(nomFic, 'r', typeEndian);
    if fid == -1
        messageErreurFichier(nomFic, 'ReadFailure');
        flag = 0;
        return
    end
    lengthRec      = fread(fid, 1, 'int32');
    datagramheader = readdgheader(fid); %#ok<*NASGU>
    configheader   = readconfigheader(fid);
    nbChannels     = configheader.transducercount;
    if (nbChannels > 500) || (nbChannels < 0) 
        fclose(fid);
        % Fichier d�cid�ment pas bon.
        
        str1 = sprintf('Le fichier "%s" n''a pas pu �tre lu.', nomFic);
        str2 = sprintf('"%s" could not be read.', nomFic);
        my_warndlg(Lang(str1,str2), 1);
        flag = 0;
        return
    end
end

%%

for k=1:nbChannels
    configtransducer(k) = readconfigtransducer(fid); %#ok<AGROW>
    if ~isempty(strfind(configtransducer(k).channelid,'Reference'))
        nrefbeams = nrefbeams+1;
    end
end
% Structure Config de sauvegarde des �l�ments de configuration
% d'acquisition.
config                  = struct('header', configheader, 'transducer', configtransducer);
config.header.nrefbeams = nrefbeams;
lengthRec               = fread(fid,1,'int32');

nbSamplesMax = zeros(1, nbChannels, 'single');
pingno       = zeros(1, nbChannels, 'single');

% Pr�allocation limit�e des tableaux
channelIndex = NaN(nbChannels, 1);
k_iChannel   = zeros(1, nbChannels);

%% Initialisations

Time               = [];
TimeIndex          = [];
HeureNav           = [];
Latitude           = [];
Longitude          = [];
NavIndex           = [];
HeadingTrack       = [];
SpeedInKnots       = [];
SpeedIndex         = [];
HeadingVessel      = [];
HeadingVesselIndex = [];


% Read NMEA, Annotation, or Sample datagram
[~, nomFicSeul] = fileparts(nomFic);
str1            = sprintf('Lecture de "%s"', nomFicSeul);
str2            = sprintf('Reading "%s"', nomFicSeul);
pppp            = dir(nomFic);
nbOctetsTotal   = pppp.bytes;
NbIter          = 1000;
pasWaitBar      = nbOctetsTotal/NbIter;
hw              = create_waitbar(Lang(str1,str2), 'N', NbIter);
% Initialisation de la Waitbar pour forcer les �carts lors du calcul de temporation.
my_waitbar(1, NbIter, hw);

while 1
    nbOctetsLus = ftell(fid);
    iWaitBar    = floor(nbOctetsLus/pasWaitBar);
    my_waitbar(iWaitBar, NbIter, hw);
    
    Info.nbDatagrams = Info.nbDatagrams + 1;
    lengthRec = fread(fid,1,'int32');
    if feof(fid)
        break
    end
    datagramheader = readdgheader(fid);
    switch datagramheader.datagramtype
        case 'CON1' % SMS extra configuration datagram
            text = readtextdata(fid, lengthRec-headerlength); 
            Info.nbCON1 = Info.nbCON1 + 1;
            %             disp('CON1');
        case 'NME0' % NMEA datagram
            text = readtextdata(fid, lengthRec-headerlength);
            switch text(1:6)
                case '$INZDA'
                    Info.nbINZDA = Info.nbINZDA + 1;
                    kTime = kTime + 1;
                    if flag
                        % Comment� par JMA le 14/06/2012
                        %                         Time(kTime)      = X; %#ok<AGROW>
                        %                         TimeIndex(kTime) = k; %#ok<AGROW>
                        
                        % TODO : rajout� par JMA le 14/06/2012
                        [flag, T] = read_NMEA_INZDA(text);
                        if flag
                            Time(kTime)      = T; %#ok<AGROW>
                            TimeIndex(kTime) = Info.nbDatagrams; %#ok<AGROW>
                        end
                    end
                    
                case '$INGGA'
                    Info.nbINGGA = Info.nbINGGA + 1;
                    [flag, H, Lat, Lon, GPS] = read_NMEA_INGGA(text);
%                     kNav = kNav + 1; % Comment� par JMA le 28/01/2015
                    if flag
                        kNav = kNav + 1; % Rajout� par JMA le 28/01/2015
                        HeureNav(kNav)  = H; %#ok<AGROW>
                        Latitude(kNav)  = Lat;%#ok<AGROW>
                        Longitude(kNav) = Lon;%#ok<AGROW>
                        NavIndex(kNav)  = Info.nbDatagrams;%#ok<AGROW>
                    end
                    
                case '$INVTG'
                    Info.nbINVTG = Info.nbINVTG + 1;
                    [flag, H, S] = read_NMEA_INVTG(text);
                    kSpeed = kSpeed + 1;
                    if flag
                        HeadingTrack(kSpeed) = H;%#ok<AGROW>
                        SpeedInKnots(kSpeed) = S;%#ok<AGROW>
                        SpeedIndex(kSpeed)   = Info.nbDatagrams;%#ok<AGROW>
                        % Cas du fichier de S.Bermell : iguanes-D20130507-T204603.raw
                        if ~isempty(HeureNav)
                            HeureTrack(kSpeed)   = HeureNav(end); %#ok<AGROW>
                        end
                    end
                    
                case '$INHDT'
                    Info.nbINHDT = Info.nbINHDT + 1;
                    [flag, H] = read_NMEA_INHDT(text);
                    kINHDT = kINHDT + 1;
                    if flag
                        HeadingVessel(kINHDT)      = H; %#ok<AGROW>
                        HeadingVesselIndex(kINHDT) = Info.nbDatagrams;%#ok<AGROW>
                    end
                    
                case '$SDVLW'
                    Info.nbSDVLW = Info.nbSDVLW + 1;
                    flag = read_NMEA_SDVLW(text);
                    
                case '$AGHDT' % Rajout JMA le 28/01/2014 pour fichier ES60 du NIWA
                    Info.nbAGHDT = Info.nbAGHDT + 1;
                    [flag, H] = read_NMEA_AGHDT(text);
                    kAGHDT = k + 1;
                    if flag
                        HeadingVessel(k)      = H; %#ok<AGROW>
                        HeadingVesselIndex(k) = Info.nbDatagrams;%#ok<AGROW>
                    end
                    
                case '$GPGLL'   % $GPGLL,3740.120,S,17622.035,E % Rajout JMA le 28/01/2014 pour fichier ES60 du NIWA
                    Info.nbGPGLL = Info.nbGPGLL + 1;
                    [flag, Heure, Lat, Lon] = read_NMEA_GPGLL(text);
                    if flag
                        kNav = kNav + 1;
                        HeureNav(kNav)  = Heure; %#ok<AGROW>
                        Latitude(kNav)  = Lat;%#ok<AGROW>
                        Longitude(kNav) = Lon;%#ok<AGROW>
                        NavIndex(kNav)  = Info.nbDatagrams;%#ok<AGROW>
                   end
                    
                case '$AGVHW' % $AGVHW,111.7,T,,,,,,   % Rajout JMA le 28/01/2014 pour fichier ES60 du NIWA
                    Info.nbAGVHW = Info.nbAGVHW + 1;
                    [flag, H] = read_NMEA_AGHDT(text); % Same uncoder than for AGHDT
                    kAGVHW = k + 1;
                    if flag
                        HeadingVessel(k)      = H; %#ok<AGROW>
                        HeadingVesselIndex(k) = Info.nbDatagrams;%#ok<AGROW>
                    end
                    
                case '$GPGGA' % $GPGGA,032016,3740.120,S,17622.035,E,1,8,2.4,23,M,29,M % Rajout JMA le 28/01/2014 pour fichier ES60 du NIWA
                    Info.nbGPGGA = Info.nbGPGGA + 1;
                    [flag, Heure, Lat, Lon, PosData] = read_NMEA_GPGGA(text);
%                     kNav = kNav + 1; % Comment� par JMA le 28/01/2015
                    if flag
                        kNav = kNav + 1; % Ajout� par JMA le 28/01/2015
                        HeureNav(kNav)  = Heure; %#ok<AGROW>
                        Latitude(kNav)  = Lat;%#ok<AGROW>
                        Longitude(kNav) = Lon;%#ok<AGROW>
                        NavIndex(kNav)  = Info.nbDatagrams;%#ok<AGROW>
                    end
                    
                case '$GPRMC' % $GPRMC,133059,A,3750.0263,S,17701.5251,E,007.3,206.5,270310,020.0,E*63  
                    Info.nbGPRMC = Info.nbGPRMC + 1;
                    [flag, Heure, Lat, Lon, Speed, Heading] = read_NMEA_GPRMC(text);
%                     kNav = kNav + 1; % Comment� par JMA le 28/01/2015
                    if flag
                        kNav = kNav + 1; % Ajout� par JMA le 28/01/2015
                        HeureNav(kNav)  = Heure; %#ok<AGROW>
                        Latitude(kNav)  = Lat;%#ok<AGROW>
                        Longitude(kNav) = Lon;%#ok<AGROW>
                        NavIndex(kNav)  = Info.nbDatagrams;%#ok<AGROW>
                    end

                    
                case '$GPVTG' % $GPVTG,115,T,95,M,10.1,N,18.7,K % Rajout JMA le 28/01/2014 pour fichier ES60 du NIWA
                    
                case '$INDBT'
                    text; %#ok<VUNUS> %$INDBT,,,000012.5,M,,
                    
                otherwise
                    Info.nbNMEO_Other = Info.nbNMEO_Other + 1;
            end
            
            %disp('NME0');
        case 'TAG0' % Annotation datagram
            Info.nbTAG0 = Info.nbTAG0 + 1;
            text = readtextdata(fid,lengthRec-headerlength); 
            %disp('TAG0');
        case 'RAW0' % Sample datagram
            pppp                   = readsampledata(fid);
            Info.nbRAW0            = Info.nbRAW0 + 1;
            iChannel               = pppp.channel;
            nbSamplesMax(iChannel) = max(nbSamplesMax(iChannel), pppp.count);
            
            % Enregistrement des num�ros de Ping pour le channel courant.
            kk                        = k_iChannel(iChannel) + 1;
            k_iChannel(iChannel)      = kk;
            channelIndex(iChannel,kk) = Info.nbDatagrams;
            
            % disp('RAW0');
            
            % WRITE YOUR OWN CODE HERE TO PROCESS AND/OR DISPLAY SAMPLE DATA
            % % % %             % channel = sampledata.channel;
            %             if ~contains(configtransducer(channel).channelid, 'Reference'))
            %                 tvgsampledata = applytvg(config,sampledata);
            %                 tvgdata(channel) = tvgsampledata;
            %                 if (channel==((config.header.transducercount-config.header.nrefbeams-1)/2+1))
            %                     beam(pingno+1).data = tvgsampledata;
            %                     echo(:,pingno+1) = tvgsampledata.sv(:,1);
            %                     heave(pingno+1) = sampledata.heave;
            %                     roll(pingno+1) = sampledata.roll;
            %                     pitch(pingno+1) = sampledata.pitch;
            %                 end
            %             end
            
            if iChannel == nbChannels
                pingno(iChannel) = pingno(iChannel) + 1;
                lastpingtime     = pingtime;
                pingtime         = datagramheader.datetime;
                if pingno(iChannel) > 1
                    pingrate = (pingtime - lastpingtime) * 1e-7;
                end
            end
            
            % Ajout du temps comme vecteur du donn�es du Ping.
            pppp.pingtime = datagramheader.datetime;
            pppp.pingno   = pingno(iChannel);
            pppp.pingrate = pingrate;
            
            % Calcul des �chantillons avec TVG et r�cup�ration des "Gain" et "SaCorrection".
            % Les �chantillons sont juste calcul�es en Sv dans cette
            % version.
            try
                tvgsampledata       = applytvg(config, pppp);
                pppp.Gain           = tvgsampledata.gain;
                pppp.SaCorrection   = tvgsampledata.sacorrection;
                pppp.Sv             = tvgsampledata.sv;
                sampledata(iChannel, k_iChannel(iChannel)) = pppp; %#ok<AGROW>
            catch
                pppp.Gain           = NaN;
                pppp.SaCorrection   = NaN;
                pppp.Sv             = [];
                pppp.power          = [];
                sampledata(iChannel, k_iChannel(iChannel)) = sampledata(iChannel, k_iChannel(iChannel) - 1); %#ok<AGROW>
            end
            
        otherwise
            Info.nbOther = Info.nbOther + 1;
            %error(strcat('Unknown datagram ''',dgheader.datagramtype,''' in file'));
    end
    lengthRec = fread(fid, 1, 'int32');
    
end
my_close(hw);
fclose(fid);

nbPings = numel(sampledata);

%% Structuration des sorties de la lecture.

if ~isempty(configheader)
    % Enregistrement de configheader + nbRefBeams
    DataHeader.Config = config;
end


if ~isempty(datagramheader)
    DataHeader.Datagram = datagramheader;
end

% filtrage des Pings dont le nb d'�chantillons est plus grande que la m�dianne
% pour �viter de dimensionner trop largement les images de sorties de channels.
% Cas du fichier de Marc ROCHE : L0030-D20150203-T223600-EA400.raw
% L'id�al serait de dimensionner les images selon les tailles r�elles des celles-ci.
for kk=1:nbChannels
    meanNbSamples = median([sampledata(kk,:).count]);
    sub = find([sampledata(kk,:).count] > 10 * meanNbSamples);
    
    
    if ~isempty(sub)
        [sampledata(kk,sub).count]  = deal(meanNbSamples);
        [sampledata(kk,sub).power]  = deal(NaN(meanNbSamples, 1));
        [sampledata(kk,sub).Sv]     = deal(NaN(meanNbSamples, 1));
        
        % Nv calcul des Max de Samples pour dimensionner les images de sortie.
        nbSamplesMax(kk) = max([sampledata(kk,:).count]);
    end
end    
    
DataHeader.nbChannels   = nbChannels;
DataHeader.nbPings      = nbPings;
DataHeader.nbSamplesMax = nbSamplesMax;

if ~isempty(NavIndex) % NavIndex TimeIndex
    DataSensor.Time.Time                 = Time;
    DataSensor.Time.TimeIndex            = TimeIndex;
    
    DataSensor.Nav.Longitude             = Longitude;
    DataSensor.Nav.Latitude              = Latitude;
    DataSensor.Nav.NavIndex              = NavIndex;
    
    DataSensor.Course.HeadingTrack       = HeadingTrack;
    DataSensor.Course.SpeedInKnots       = SpeedInKnots;
    DataSensor.Course.SpeedIndex         = SpeedIndex;
    DataSensor.Course.HeadingVessel      = HeadingVessel;
    DataSensor.Course.HeadingVesselIndex = HeadingVesselIndex;
    % Remplissage des trames de temps si trames GGA rencontr�es.
    % Cas du fichier : ???\GeoLittomer\BOURGNEUF-D20120412-T071623_raw
    if ~isempty(Time)
        % Traitement en jour/heure de l'heure re�ue dans le paquet GGA.
        [Date, Heure] = timeMat2Ifr(Time);
        Date2 = timeMat2Ifr(Time(Time ~= 0));
        
        if length(Date) == length(HeureNav)
            t = cl_time('timeIfr', Date, HeureNav); % Modifi� par JMA le 03/12/2015 pour fichier D:\Temp\NIWA\tan1507_for_JM\MBES\raw\EM302\Yoann\D20150523-T085128.raw
        elseif length(unique(Date2)) == 1 % 1721059 = an 0
            t = cl_time('timeIfr', Date2(1), HeureNav);
        else
            messageForDebugInspection
            % TODO 
        end
        DataSensor.Nav.Time = t.timeMat;
        
        % TODO : affiner l'algo de recherche des occurrences de Temps les plus fines.
        if exist('HeureTrack', 'var') % Modifi� par JMA le 03/12/2015 pour fichier D:\Temp\NIWA\tan1507_for_JM\MBES\raw\EM302\Yoann\D20150523-T085128.raw
            t = cl_time('timeIfr', Date(1:size(HeureTrack,2)), HeureTrack);
            DataSensor.Course.Time = t.timeMat;
        end
    end
else
    [nomDir, nomFicSeul, ext] = fileparts(nomFic);
    nomFicNav = [fullfile(nomDir,nomFicSeul) ext '.gps.csv'];
    if exist(nomFicNav,'file')
        % Chargement des donn�es de Navigation par fichier Annexe.
        [flag, dataAscii] = import_nav(nomFicNav);
        if ~flag
            my_warndlg(Lang('Le fichier de navigation associ� n''est pas correct', 'File Navigation is not in the good format'), 1);
            return
        end
        sz = size([dataAscii.Nav.lon]);
        t = cl_time('timeMat', dataAscii.Time);
        pppp = (1:sz(1));
        DataSensor.Nav.Latitude              = double([dataAscii.Nav.lat]');
        DataSensor.Nav.Longitude             = double([dataAscii.Nav.lon]');
        DataSensor.Nav.NavIndex              = pppp;
        DataSensor.Nav.Time                  = t.timeMat;

        DataSensor.Time.Time                 = t.timeMat;
        DataSensor.Time.TimeIndex            = pppp;

        DataSensor.Course.SpeedInKnots       = [dataAscii.Nav.speed]';
        DataSensor.Course.SpeedIndex         = pppp;
        DataSensor.Course.Time               = t.timeMat;
        DataSensor.Course.HeadingTrack       = [dataAscii.Nav.cap]';
        DataSensor.Course.HeadingVessel      = [dataAscii.Nav.cap]';
        DataSensor.Course.HeadingVesselIndex = pppp;
    end
end

DataSample.Data         = sampledata;
DataSample.ChannelIndex = channelIndex;
DataSample.k_iChannel   = k_iChannel;

%% Check sizes

n1 = length(DataSensor.Course.HeadingTrack);
n2 = length(DataSensor.Course.SpeedInKnots);
n3 = length(DataSensor.Course.SpeedIndex);
n4 = length(DataSensor.Course.HeadingVessel);
n5 = length(DataSensor.Course.HeadingVesselIndex);
if ~isfield(DataSensor.Course, 'Time')
    DataSensor.Course.Time = [];
end
n6 = length(DataSensor.Course.Time);
n7 = length(DataSensor.Course.SpeedInKnots);
N = min([n1 n2 n3 n4 n5 n6 n7]);

DataSensor.Course.HeadingTrack(N+1:end)       = [];
DataSensor.Course.SpeedInKnots(N+1:end)       = [];
DataSensor.Course.SpeedIndex(N+1:end)         = [];
DataSensor.Course.HeadingVessel(N+1:end)      = [];
DataSensor.Course.HeadingVesselIndex(N+1:end) = [];
DataSensor.Course.Time(N+1:end)               = [];

flag = 1;
