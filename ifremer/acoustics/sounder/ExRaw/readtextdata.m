% Reading EK60 raw data file text data
% Simrad, Lars Nonboe Andersen, 21/12-01

function textdata = readtextdata(fid,length)
% length in bytes

textdata = char(fread(fid,length,'char')');
