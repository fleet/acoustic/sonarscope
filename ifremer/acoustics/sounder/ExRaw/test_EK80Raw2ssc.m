% EK60
nomFic      = 'E:\SonarScopeAnnexes\SonarScopeDataArchive\From Anne Le Vourch - IRD\D20130714-T192934.raw'; %#ok<NASGU>

% EK80
nomFic      = 'E:\SonarScopeAnnexes\SonarScopeDataArchive\From Rida\D20190625-T100713.raw';

try
    isEK80 = ExRaw_identifySounder(nomFic); %#ok<NASGU>
catch
    isEK80 = 0 ; %#ok<NASGU>
end

% Tests unitaires

%%  
% nomFic      = 'E:\SonarScopeAnnexes\SonarScopeDataArchive\From Rida\D20190625-T100713.raw';
% nomFic      = 'E:\SonarScopeAnnexes\SonarScopeDataArchive\From Rida\D20190625-T100926.raw';
% nomFic      = 'E:\SonarScopeAnnexes\SonarScopeDataArchive\From Rida\D20190625-T101139.raw';
% nomFic      = 'E:\SonarScopeAnnexes\SonarScopeDataArchive\From Rida\D20190625-T101351.raw';
% nomFic      = 'E:\SonarScopeAnnexes\SonarScopeDataArchive\From Rida\D20190625-T101351.raw';
nomFic      = 'E:\SonarScopeAnnexes\SonarScopeDataArchive\From Rida\MD221_MAYOBS2-D20190611-T134426.raw';
a           = cl_ExRaw('NomFic', nomFic);

isEK80 = ExRaw_identifySounder(nomFic) %#ok<NOPTS>