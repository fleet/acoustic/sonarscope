%% Fichier de Falkor - Mai 2012
nomDir = '???\Sonar\SonarScope\FK-D20120510-T202356';
nomFic = getNomFicDatabase('FK-D20120510-T202356.raw');
%% Fichier de GeoLittomer
cd 'C:\SonarScopeTbx\ifremer\acoustics\sounder\ExRaw'
nomDir = '???\Sonar\GeoLittomer\SonarScope';
%%
if exist(nomDir, 'dir')
    rmdir(nomDir, 's');
end
nomFic = '???\Sonar\GeoLittomer\BOURGNEUF-D20120412-T071623_raw.raw';
[flag, DataHeader, DataSensor, InfoDatagrams, DataSample] = readraw(nomFic); %#ok<*ASGLU>
flag = ExRaw2ssc(nomFic);

%% OLD
cd 'C:\SonarScopeTbx\ifremer\acoustics\sounder\ER60-Old JMA'
nomDir = '???\Sonar\GeoLittomer\SonarScope';
if exist(nomDir, 'dir')
    rmdir(nomDir, 's');
end
nomFic = '???\Sonar\GeoLittomer\BOURGNEUF-D20120412-T071623_raw.raw';
flag = ER602ssc(nomFic);
%%
nomFic = getNomFicDatabase('COM25-EK60-18kHz-DBS-RAW_20120702-093141.raw');
% [flag, DataHeader, DataSensor, InfoDatagrams, DataSample] = readraw(nomFic);
flag = ExRaw2ssc(nomFic);
%%
nomFicNav   = 'F:\SonarScopeData\From CSIRO\Raw\EK60_12Khz-D20120730-T045457.raw.gps.csv';
nomFic      = 'F:\SonarScopeData\From CSIRO\Raw\EK60_12Khz-D20120730-T045457.raw';
a = cl_ExRaw('NomFic', nomFic);
flag = ExRaw2ssc(nomFic); %#ok<*NASGU>
flag = ExRaw2ssc_Save(nomFic);
ExRaw_ImportNavigation(cl_ExRaw([]), nomFic, nomFicNav);
%%
nomFic  = 'F:\SonarScopeData\From Carla\Colmeia\colmeia-D20130211-T120602.raw';
a       = cl_ExRaw('NomFic', nomFic);
%%
nomFic  = '???\Sonar\RAW_PG_IN\ESSBASE-D20130725-T104541.raw';
a       = cl_ExRaw('NomFic', nomFic);
%%
nomFic  = getNomFicDatabase('FK-D20120510-T202356.raw');
flag    = ExRaw_ssc2hac('NomFicRaw', nomFic, 'RepDefaut', 'C:\Temp\Test_HAC_Creation');
%%
nomFic = 'F:\SonarScopeData\From Carla\RawData\BOB_MARMESONET-D20091107-T150129.raw';
a       = cl_ExRaw('NomFic', nomFic);
flag    = ExRaw_ssc2hac('NomFicRaw', nomFic, 'RepDefaut', 'C:\Temp\Test_HAC_Creation');
nomFic = 'C:\Temp\Test_HAC_Creation\BOB_MARMESONET-D20091107-T150129_raw2hac.hac';
a       = cl_hac('nomFic', nomFic);
%%
nomFic  = 'F:\SonarScopeData\From Carla\RAW2HAC\BOB_MARMESONET-D20091107-T150129.raw';
a       = cl_ExRaw('NomFic', nomFic);
flag    = ExRaw_ssc2hac('NomFicRaw', nomFic, 'RepDefaut', 'F:\SonarScopeData\From Carla\RAW2HAC');
nomFic = 'F:\SonarScopeData\From Carla\RAW2HAC\BOB_MARMESONET-D20091107-T150129_raw2hac.hac';
a       = cl_hac('nomFic', nomFic);
%%
nomFic  = 'F:\SonarScopeData\From Carla\RAW2HAC\L0001-D20070513-T080745-EK60.raw';
a       = cl_ExRaw('NomFic', nomFic);
flag    = ExRaw_ssc2hac('NomFicRaw', nomFic, 'RepDefaut', 'F:\SonarScopeData\From Carla\RAW2HAC');
nomFic = 'F:\SonarScopeData\From Carla\RAW2HAC\L0001-D20070513-T080745-EK60_raw2hac.hac';
a       = cl_hac('nomFic', nomFic);
%%
nomFic  = 'F:\SonarScopeData\From Carla\RAW2HAC\colmeia-D20130128-T103915.raw';
a       = cl_ExRaw('NomFic', nomFic);
flag    = ExRaw_ssc2hac('NomFicRaw', nomFic, 'RepDefaut', 'F:\SonarScopeData\From Carla\RAW2HAC');
nomFic = 'F:\SonarScopeData\From Carla\RAW2HAC\colmeia-D20130128-T103915_raw2hac.hac';
a       = cl_hac('nomFic', nomFic);
%%
nomFic  = 'F:\SonarScopeData\BOB\Data\ESSBOB2\ESSBOB-D20130921-T150236.raw';
a       = cl_ExRaw('NomFic', nomFic);
flag    = ExRaw_ssc2hac('NomFicRaw', nomFic, 'RepDefaut', 'F:\SonarScopeData\BOB\Data\ESSBOB2');
nomFic = 'F:\SonarScopeData\BOB\Data\ESSBOB2\RAW2HAC\ESSBOB-D20130921-T150236.raw';
a       = cl_hac('nomFic', nomFic);

%% 
nomFicRaw   = 'E:\SonarScopeAnnexes\SonarScopeDataArchive\Level2\Sonar\FK-D20120510-T202356.raw';
nomFic      = 'E:\SonarScopeAnnexes\SonarScopeDataArchive\From Anne Le Vourch - IRD\D20130714-T054556.raw';
flag        = ExRaw2ssc(nomFic);
% a           = cl_ExRaw('nomFic', nomFic);

%% 
nomFicRaw = 'E:\SonarScopeAnnexes\SonarScopeDataArchive\From Carla\FLUID3D-D20181102-T073944.raw';
flag        = ExRaw2ssc(nomFicRaw);

%% Identification 

% ME70 au format Raw
nomFic   = 'E:\SonarScopeAnnexes\SonarScopeDataArchive\From Carla\ME70\FLUID3D18-D20181102-T073738.raw';
isEK80   = ExRaw_identifySounder(nomFic);
a       = cl_ExRaw('nomFic', nomFic);

% EK80
nomFic   = 'E:\SonarScopeAnnexes\SonarScopeDataArchive\From Rida\MD221_MAYOBS2-D20190611-T134426.raw';
nomFic   = 'E:\SonarScopeAnnexes\SonarScopeDataArchive\From Rida\D20190625-T100713.raw';
% isEK80   = ExRaw_identifySounder(nomFic);
a       = cl_ExRaw('nomFic', nomFic);

% EK60
nomFic  = 'E:\SonarScopeAnnexes\SonarScopeDataArchive\From Carla\RAW2HAC\L0001-D20070513-T080745-EK60.raw';
nomFic  = 'E:\SonarScopeAnnexes\SonarScopeDataArchive\From CSIRO\Raw\EK60_12Khz-D20120730-T045457.raw';
isEK80   = ExRaw_identifySounder(nomFic);
a       = cl_ExRaw('nomFic', nomFic);
