function PosAbs= ComputeWorldPositionER60(Delay, transFaceAlongAngleOffsetRad,transFaceAthwarAngleOffsetRad, PingFan, Transducer, ...
    SoftChannel , Range, AlongTSAngle, AthwartTSAngle)
	
m_MatrixNav=CreateRotationMatrix(PingFan.navigationAttribute.m_headingRad,PingFan.navigationAttitude.pitchRad,PingFan.navigationAttitude.rollRad );
m_TranslationNav=([0,0,PingFan.navigationAttitude.sensorHeaveMeter] )';

m_transducerTranslation=[Transducer.m_pPlatform.along_ship_offset;Transducer.m_pPlatform.athwart_ship_offset;Transducer.m_pPlatform.depth_offset  ];

m_transducerRotation=CreateRotationMatrix(Transducer.m_transRotationAngleRad,transFaceAlongAngleOffsetRad,transFaceAthwarAngleOffsetRad);

temp = 	Range+Delay;
temp=temp*(cos(AlongTSAngle)*cos(AthwartTSAngle));
	
CoordSoftChannel=[temp*tan(AlongTSAngle);temp*tan(AthwartTSAngle);temp];

SoftRotation=CreateRotationMatrix(0,0,0);

PosRel= m_MatrixNav*(m_transducerRotation*SoftRotation*CoordSoftChannel+m_transducerTranslation)+m_TranslationNav;

PosGeo = [PingFan.navigationPosition.m_lattitudeDeg;PingFan.navigationPosition.m_longitudeDeg;0];
PosAbs(1) = PosGeo(1)+PosRel(1)/60.0/1852.0;
PosAbs(2) = PosGeo(2)+PosRel(2)/60.0/1852.0/cos(PosGeo(1)*3.14159/180.0);
PosAbs(3) = PosRel(3);

PosAbs=PosRel;
