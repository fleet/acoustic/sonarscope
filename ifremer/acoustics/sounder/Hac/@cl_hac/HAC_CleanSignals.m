function HAC_CleanSignals(this, nomFic, varargin) %#ok<INUSL>

[varargin, listFrequencies] = getPropertyValue(varargin, 'identFreq', 1); %#ok<ASGLU>

if ~iscell(nomFic)
    nomFic = {nomFic};
end

%% Initialisations

% FiltreTime.Type  = 5;
% FiltreTime.Ordre = 2;
% FiltreTime.Wc    = 0.02;

FiltreNav.Type  = 5;
FiltreNav.Ordre = 2;
FiltreNav.Wc    = 0.02;

%% Traitement des fichiers

Format1 = 'V�rification de la %s.\n\nUne fen�tre SignalScope va s''ouvrir, le signal de %s est en noir et une version filtr�e est en rouge. Zoomez sur le signal pour voir si la version filtr�e est meilleure (elle rem�die souvent � des marches d''escalier dues � une quantification de la mesure de positionnemen).\n\nSi vous conservez le filtre et que vous clickez sur Save et Quit c''est lui qui sera sauvegard�.';
Format2 = 'Checking %s.\n\nA SignalScope window is going to be open, %s signal is the black one, the red one is a filtered version. Zoom on the signal to check if the filter is efficient (it often solves problems of stair steps due to signal quantification).\n\nIf you conserve the filter AND you click on Save and Quit il will be the red curve which will be saved.';
% Format3 = 'V�rification de la %s.\n\nUne fen�tre SignalScope va s''ouvrir. Zoomez sur le signal pour voir si un filtrage s''impose.';
% Format4 = 'Checking %s.\n\nA SignalScope window is going to be opened. Zoom on the signal to check if a filter is necessary.';

Carto = [];
Video = 1;
I0    = cl_image_I0;
N = length(nomFic);
hw = create_waitbar('ExRaw Quality Control processing', 'N', N);
for k=1:N
    my_waitbar(k, N, hw);
    
    str1 = sprintf('Traitement du fichier %d/%d : "%s" en cours.', k, N, nomFic{k});
    str2 = sprintf('Processing file %d/%d : "%s".', k, N, nomFic{k});
    fprintf(1,'%s\n', Lang(str1,str2));
    
    %% V�rification de l'existence du fichier Status
    
    [nomDir, nomFicSeul] = fileparts(nomFic{k});
    nomFicXmlStatus = fullfile(nomDir, [nomFicSeul '_Status.xml']);
    if exist(nomFicXmlStatus, 'file')
        str1 = sprintf('Il semble que le fichier "%s" a d�j� �t� trait�. Que voulez-vous faire ?', nomFicSeul);
        str2 = sprintf('It seems file "%s" has been already processed. What do you want to do ?', nomFicSeul);
        str3 = 'Passer au fichier suivant';
        str4 = 'Skip it';
        str5 = 'Le retraiter';
        str6 = 'Reprocess it';
        [rep, flag] = my_questdlg(Lang(str1,str2), Lang(str3,str4), Lang(str5,str6));
        if ~flag
            my_close(hw)
            return
        end
        if rep == 1
            continue
        end
        Info = xml_read(nomFicXmlStatus);
        %         FiltreTime  = Info.FiltreTime;
        %         FiltreNav   = Info.FiltreNav;
        CLim        = Info.Clim_Apres;
        Video       = Info.Video;
    else
        CLim = [];
    end
    
    %% Lecture du fichier
    
    a = cl_ExRaw('nomFic', nomFic{k});
    
    %% Contr�le de l'heure
    
    %{
str1 = sprintf(Format1, 'Temps', 'Temps');
str2 = sprintf(Format2, 'Time', 'Time');
if k == 1
my_warndlg(Lang(str1,str2), 1);
else
my_warndlg(Lang(str1,str2), 0, 'Tag', 'ControleSignal', 'TimeDelay', 60);
end
T = Time.timeMat;
TDeb = T(1);
T = T - TDeb;
Before = T;
[T, FiltreTime] = controleSignalNEW('Time', T, 'Filtre', FiltreTime);
if ~isequal(T, Before)
T = T + TDeb;
T = cl_time('timeMat', T);
flag = set_time(a, T, 'DataTrace', DataTrace);
if ~flag
my_close(hw)
return
end
end
    %}
    
    %% Nettoyage de la navigation
    
    str1 = 'Voulez-vous nettoyer la navigation ?';
    str2 = 'Clean the navigation ?';
    [rep, flag] = my_questdlg(Lang(str1,str2));
    if ~flag
        return
    end
    if rep == 1
        
        %% Contr�le de la longitude du bateau
        
        str1 = sprintf(Format1, 'Longitude', 'Longitude');
        str2 = sprintf(Format2, 'Longitude', 'Longitude');
        if k == 1
            my_warndlg(Lang(str1,str2), 1);
        else
            my_warndlg(Lang(str1,str2), 0, 'Tag', 'ControleSignal', 'TimeDelay', 60);
        end
        
        [flag, DataPosition] = read_position_bin(a);
        if ~flag
            % TODO : message
            break
        end
        
        ShipLongitude = DataPosition.Longitude;
        Before = ShipLongitude;
        [ShipLongitude, FiltreNav] = controleShipLatitude('ShipLongitude', ShipLongitude, 'Filtre', FiltreNav);
        PosModif = ~isequal(ShipLongitude, Before);
        DataPosition.Longitude = ShipLongitude;
        
        %% Contr�le de la latitude du bateau
        
        str1 = sprintf(Format1, 'Latitude', 'Latitude');
        str2 = sprintf(Format2, 'Latitude', 'Latitude');
        if k == 1
            my_warndlg(Lang(str1,str2), 1);
        else
            my_warndlg(Lang(str1,str2), 0, 'Tag', 'ControleSignal', 'TimeDelay', 60);
        end
        ShipLatitude = DataPosition.Latitude;
        %     ShipLatitude = controleShipLatitude('ShipLatitude', ShipLatitude, 'Filtre', FiltreNav);
        Before = ShipLatitude;
        [ShipLatitude, FiltreNav] = controleSignalNEW('ShipLatitude', ShipLatitude, 'Filtre', FiltreNav);
        PosModif = PosModif | ~isequal(ShipLatitude, Before);
        DataPosition.Latitude = ShipLatitude;
        
        %% Sauvegarde des signaux dans le r�pertoire cache
        
        if PosModif
            flag = save_signal(a, 'Latitude', DataPosition.Latitude);
            if ~flag
                str1 = sprintf('Il est impossible de sauver "Latitude" dans le r�pertoire cache associ� � "%s".', nomFicSeul);
                str2 = sprintf('It is impossible to save "Latitude" in the cache associated to "%s".', nomFicSeul);
                my_warndlg(Lang(str1,str2), 1);
            end
            flag = save_signal(a, 'Longitude', DataPosition.Longitude);
            if ~flag
                str1 = sprintf('Il est impossible de sauver "Longitude" dans le r�pertoire cache associ� � "%s."', nomFicSeul);
                str2 = sprintf('It is impossible to save "Longitude" in the cache associated to "%s".', nomFicSeul);
                my_warndlg(Lang(str1,str2), 1);
            end
        end
    end
    
    %% Importation de l'image
    
    [flag, b, Carto] = import_ExRaw(I0, nomFic{k}, 'Carto', Carto, 'listFrequencies', listFrequencies, 'listDataType', 1);
    if ~flag
        my_close(hw)
        return
    end
    
    b.Video = Video;
    if ~isempty(CLim)
        b.CLim = CLim;
    end
    
    %% Pr�-d�tection de la hauteur
    
    Height = get(b, 'SonarHeight');
    if isempty(Height) || all(Height == 0)
        str1 = 'Aucune hauteur n''a �t� trouv�e, je d�clenche d''autorit� un calcul de la hauteur.';
        str2 = 'No height was found, I am lauching the height detection prior to the image edit.';
        my_warndlg(Lang(str1,str2), 0, 'Tag', 'ControleSignal', 'TimeDelay', 60);
        b = sonar_lateral_hauteur(b);
    end
    
    %% Affichage de l'image dans SonarScope pour v�rifier la hauteur
    
    if k == 1
    	messageCheckHeight();
    end
    Clim_Avant = b.CLim;
    b = SonarScope(b, 'visuProfilY', 1, 'profilY.Type', 'Height', 'visuProfilX', 1, 'ButtonSave', 1);
    Clim_Apres = b.CLim;
    
    %% V�rification si le contraste a bien �t� r�gl�
    
    if isequal(Clim_Avant, Clim_Apres)
        str1 = 'Il semblerait que vous ayez oubli� de r�gler le contraste, voulez-vous sonarscopier l''image de nouveau ?';
        str2 = 'It seems you have forgotten to adjust the contrast, do you want to edit the image again ?';
        [rep, flag] = my_questdlg(Lang(str1,str2));
        if ~flag
            my_close(hw)
            return
        end
        if rep == 1
            b = SonarScope(b, 'visuProfilY', 1, 'profilY.Type', 'Height', 'visuProfilX', 1, 'ButtonSave', 1);
            Clim_Apres = b.CLim;
            if isequal(Clim_Avant, Clim_Apres)
                str1 = 'Y''a de la fatigue dans l''air on dirait, tant pis pour le contraste.';
                str2 = 'It seems you are tired, let us drop the contrast for the moment.';
                my_warndlg(Lang(str1,str2), 0, 'Tag', 'ControleSignal', 'TimeDelay', 60);
            end
        end
    end
    Video = b.Video;
    
    %% Sauvegarde de la hauteur
    
    Height = get(b(end), 'SonarHeight');
    flag = save_signal(a, 'Height', Height(:,1));
    if ~flag
        my_close(hw)
        return
    end
    
    %% Sauvegarde de CLim
    
    CLim = b(end).CLim;
    flag = save_ProcessingParameters(a, 'Ssc_ProcessingParameters.xml', 'CLim', CLim);
    if ~flag
        my_close(hw)
        return
    end
    
    %% Cr�ation du fichier de status du fichier
    
    %     Info.FiltreTime = FiltreTime;
    %     Info.FiltreNav  = FiltreNav;
    Info.Clim_Apres = Clim_Apres;
    Info.Video      = Video;
    Info.Height     = 1;
    xml_write(nomFicXmlStatus, Info);
    flag = exist(nomFicXmlStatus, 'file');
    if ~flag
        messageErreur(nomFicXmlStatus)
    end
    
    %% On demande si on veut traiter les autres fichiers
    
    if k ~= N
        flag = question_ContinueNextFile();
        if ~flag
            break
        end
    end
    
end
my_close(hw, 'MsgEnd')

