% Constructeur de cl_hac (Imagerie contenue dans une .hac)
%
% Syntax
%   a = cl_hac(...)
% 
% Name-Value Pair Arguments
%   nomFic : Nom(s) du fichier .hac
%  
% Output Arguments 
%   a : Instance de cl_hac
%
% Examples
%   nomFic = getNomFicDatabase('PELGAS10_003_20100428_183857.hac');
%   a = cl_hac('nomFic', nomFic)
%
% See also CONVERT_HAC.exe Authors
% Authors : GLU
% ----------------------------------------------------------------------------

function this = cl_hac(varargin)

%% Definition de la structure

this.nomFic        = '';
this.typeTuples    = '';
this.listSounders  = [];
this.strIdentData  = {'Amplitude'; 'PhaseAcross'; 'PhaseAlong'};
this.flagIdentData = [];


%% Creation de l'instance

this = class(this, 'cl_hac');

%% On regarde si l'objet a ete cree uniquement pour atteindre une methode de la classe

if (nargin == 1) && isempty(varargin{1})
    return
end

%% Initialisation des valeurs transmises lors de l'appel au constructeur

this = set(this, varargin{:});
