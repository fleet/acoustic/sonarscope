% Acces en lecture des proprietes d'une instance
%
% Syntax
%   [...] = get(this, ...)
% 
% Input Arguments 
%   this : Une instance de la classe cl_hac
%
% Name-Value Pair Arguments
%   nomFic : Nom du fichier .xtf
%
% Output Arguments
%   PropertyValues associees aux PropertyNames passes en argument
% 
% Examples
%   nomFic = getNomFicDatabase('TODO.hac')
%   a = cl_hac('nomFic', nomFic)
%   nomFic = get(a, 'nomFic')
%
% See also cl_hac cl_hac/char Authors
% Authors : GLU
% ----------------------------------------------------------------------------

function varargout = get(this, varargin)

%% Boucle de parcours des arguments

varargout = {};
for k=1:length(varargin)
    switch varargin{k}
        case 'nomFic'
            varargout{end+1} = this.nomFic; %#ok<AGROW>

        case 'nbSounders'
            [~, varargout{end+1}] = get_nbSounders(this); %#ok<AGROW>

        case 'nameSounders'
            varargout{end+1}      = get_nameSounders(this); %#ok<AGROW>

        case 'listSounders'
            [~, varargout{end+1}] = get_listSounders(this); %#ok<AGROW>
            
        case 'listGeometry'
            [~, varargout{end+1}] = get_listGeometry(this); %#ok<AGROW>
            
        case 'typeSounders'
            [~, varargout{end+1}] = get_typeSounders(this); %#ok<AGROW>
            
        case 'strIdentData'
            varargout{end+1}      = this.strIdentData; %#ok<AGROW>

        case 'flagIdentData'
            varargout{end+1}      = this.flagIdentData; %#ok<AGROW>
            
        otherwise
            w = sprintf('cl_hac/get %s non pris en compte', varargin{k});
            my_warndlg(w, 0);
    end
end
