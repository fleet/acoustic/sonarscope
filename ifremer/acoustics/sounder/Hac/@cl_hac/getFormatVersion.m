function FormatVersion = getFormatVersion(~, nomDirRacine)

nomFicXml = fullfile(nomDirRacine, 'Ssc_SignatureBeginOfFile.xml');
flag = exist(nomFicXml, 'file');        
if ~flag
    nomFicXml = fullfile(nomDirRacine, 'Ssc_SignatureEndOfFile.xml');
    flag = exist(nomFicXml, 'file');        
    if ~flag
        special_message('FichierNonExistant', nomFicXml)
        return
    end
end
% edit(nomFicXml)
Datagrams = xml_read(nomFicXml);
if ~isfield(Datagrams, 'FormatVersion')
    FormatVersion = 0;
    return
end
FormatVersion = Datagrams.FormatVersion;
