function [flag, flagIdentData] = get_flagIdentLayers(this, varargin)


% Fonction d'énumération des layers présents.
[varargin, NbSounder] = getPropertyValue(varargin, 'NbSounder', []); %#ok<ASGLU>

flagIdentData   = get(this, 'flagIdentData');
[nomDirRacine, file, ~] = fileparts(get(this, 'nomFic'));
nomDirRacine            = fullfile(nomDirRacine, 'SonarScope', file);

for k=1:NbSounder
    % Test de présence des layers Amplitudes.
    nomDir          = fullfile(nomDirRacine, ['Ssc_Sounder' num2str(k)]);
    nomFicXmlC16Amp = fullfile(nomDir, 'Ssc_PingC16Amplitude.xml');
    nomFicXmlU16Amp = fullfile(nomDir, 'Ssc_PingU16Amplitude.xml');
    flag = exist(nomFicXmlC16Amp, 'file');
    if ~flag
        if ~exist(nomFicXmlU16Amp, 'file')
            flagIdentData(k,1) = 0;
        else
            flagIdentData(k,1) = 1;
        end
    else
        flagIdentData(k,1) = 1;
    end
    
    % Test de présence des layers de phases.
    flag                    = 0;
    nomFicXmlC1632Angle     = fullfile(nomDir, 'Ssc_PingC1632Angle.xml');
    nomFicXmlU16Angle       = fullfile(nomDir, 'Ssc_PingU16Angle.xml');
    if ~exist(nomFicXmlC1632Angle, 'file')
        if ~exist(nomFicXmlU16Angle, 'file')
            flagIdentData(k,2:3) = [0 0];
        else
            flagIdentData(k,2:3) = [1 1];
        end
    else
        flagIdentData(k,2:3) = [1 1];
    end
end
flag = 1;
