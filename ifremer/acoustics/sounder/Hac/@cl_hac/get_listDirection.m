function [flag, listDirection] = get_listDirection(this)

listDirection = [];

[flag, listSounders] = get_listSounders(this);
if ~flag
    return
end

nbSounders = length(listSounders);
listDirection = cell(1,nbSounders);
for k=1:nbSounders
    switch listSounders(k).Name
        case 'EK60'
            [flag, Data] = read_Channel_bin(this, 'Memmapfile', 0, 'TypeSounder', 1, 'NumSounder', k);
            if ~flag % Rajout� par JMA
                return
            end
            TransducerFaceAthwartshipAngleOffset = mean(unique(Data.TransducerFaceAthwartshipAngleOffset));
            % Orientation du plan du sondeur. 
            if TransducerFaceAthwartshipAngleOffset == -90
                listDirection{k} = 'Horizontal';
            else
                listDirection{k} = 'Vertical';
            end    
        case 'ME70'
            listDirection{k} = 'Multibeam';
        case 'Generic'
            listDirection{k} = 'Multibeam';
        otherwise
            str1 = sprintf('%s pas encore pr�vu dans cl_hac/get_listDirection : transmettez le fichier HAC � sonarscope@ifremer.fr SVP', listSounders(k).Name);
            str2 = sprintf('%s not yet available in cl_hac/get_listDirection : please send the HAC file to sonarscope@ifremer.fr', listSounders(k).Name);
            my_warndlg(Lang(str1,str2), 1);
    end
end

flag = 1;
