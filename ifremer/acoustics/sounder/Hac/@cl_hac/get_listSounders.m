function [flag, listSounders] = get_listSounders(this, varargin)
% Fonction d'�num�ration des sondeurs par type et par num�ro de r�pertoire.

[varargin, SounderName] = getPropertyValue(varargin, 'SounderName', {'EK60', 'ME70', 'Generic'}); %#ok<ASGLU>

[nomDirRacine, file, ~] = fileparts(get(this, 'nomFic'));
% flag = 0;
nomDirRacine   = fullfile(nomDirRacine, 'SonarScope', file);
listDirSounder = dir(fullfile(nomDirRacine, 'Ssc_Sounder*'));

% Initialisation de la structure des noms de r�pertoires de sondeurs.
listSounders = [];
NbTotSounder = size(listDirSounder,1);
for iSounder=1:NbTotSounder
    xmldata = xml_read(fullfile(nomDirRacine,listDirSounder(iSounder).name, 'Ssc_Channel.xml'));
    for j=1:size(SounderName,2)
        % Recherche de l'occurrence SounderName dans le titre du sondeur.
        pppp = regexp(xmldata.Title, SounderName{j}, 'ONCE');
        if ~isempty(pppp)
            if ~isempty(listSounders)
                listSounders(end+1).id   = iSounder; %#ok<AGROW>
                listSounders(end).Name   =  SounderName{j};
            else
                listSounders(1).id       = iSounder;
                listSounders(end).Name   =  SounderName{j};
            end
        end
    end
end

flag = 1;
