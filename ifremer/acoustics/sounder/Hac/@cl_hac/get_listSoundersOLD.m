function [flag, listSounders] = get_listSoundersOLD(this)

flag = 0;

%% Lecture du répertoire SonarScope

[nomDir, nom] = fileparts(this.nomFic);
nomDir1 = fullfile(nomDir, 'SonarScope', nom);
if ~exist(nomDir1, 'dir')
    return
end

%% Recherche des répertoires de sondeurs

nomDirSounder = {};
listeDir = listeDirOnDir(nomDir1);
for k=1:length(listeDir)
    if ~isempty(strfind(listeDir(k).name, 'Ssc_Sounder'))
        nomDirSounder{end+1} = listeDir(k).name; %#ok<AGROW>
    end
end

%% Recherche des noms

listSounders = {};
for k=1:length(nomDirSounder)
    nomDir2 = fullfile(nomDir, 'SonarScope', nom, nomDirSounder{k});
    nom2 = fullfile(nomDir2, 'Ssc_EK60Channel.xml');
    if exist(nom2, 'file')
        [flag, nom3] = getSounderName(nom2);
        if ~flag
            return
        end
        listSounders{end+1} = nom3; %#ok<AGROW>
    end
    nom2 = fullfile(nomDir2, 'Ssc_ME70Channel.xml');
    if exist(nom2, 'file')
        [flag, nom3] = getSounderName(nom2);
        if ~flag
            return
        end
        listSounders{end+1} = nom3; %#ok<AGROW>
    end
end
flag = 1;


function [flag, nom3] = getSounderName(nomFicXml)

flag = exist(nomFicXml, 'file');
if ~flag
    return
end
Data = xml_mat_read(nomFicXml);

if isfield(Data, 'Title') && ~isempty(Data.Title)
    nom3 = Data.Title(1:4);
end
