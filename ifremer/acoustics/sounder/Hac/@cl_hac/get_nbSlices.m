function [flag, nbSlices] = get_nbSlices(this, NumSounder, TypeSounder)

[flag, Data] = read_data(this, 'NumSounder', NumSounder, 'Memmapfile', 1);
if ~flag
    nbSlices = [];
    return
end


%% lecture des images

if TypeSounder == 1
    nbSlices = Data.Dimensions.nbFreq;
else
    nbSlices = Data.Dimensions.nbBeams;
end
