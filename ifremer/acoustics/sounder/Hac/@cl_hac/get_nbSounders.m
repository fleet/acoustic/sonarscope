function [flag, nbSounders] = get_nbSounders(this)

nbSounders = [];

[flag, listSounders] = get_listSounders(this);
if ~flag
    return
end

nbSounders = length(listSounders);
