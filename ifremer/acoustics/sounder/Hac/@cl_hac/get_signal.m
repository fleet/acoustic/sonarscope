function [flag, Signal] = get_signal(this, nomSignal, varargin)

[varargin, NumSounder] = getPropertyValue(varargin, 'NumSounder', 1);
[varargin, NumChannel] = getPropertyValue(varargin, 'NumChannel', 1); %#ok<ASGLU>

Signal = [];
switch nomSignal
    case 'Time'
        [flag, Data] = read_SpeedHeading_bin(this, 'Memmapfile', 1);
        if ~flag
            return
        end
        Signal.Time  = Data.TimeCPU;
        Signal.Value = Data.TimeCPU.timeMat;
        Signal.Unit  = 'Time';
        
    case 'Date'
        [flag, Data] = read_SpeedHeading_bin(this, 'Memmapfile', 1);
        if ~flag
            return
        end
        Signal.Time  = Data.TimeCPU;
        Signal.Value = floor(Data.TimeCPU.timeMat);
        Signal.Unit  = 'Date';
        
    case 'Hour'
        [flag, Data] = read_SpeedHeading_bin(this, 'Memmapfile', 1);
        if ~flag
            return
        end
        Signal.Time  = Data.TimeCPU;
        Signal.Value = (Data.TimeCPU.timeMat - floor(Data.TimeCPU.timeMat)) * 24;
        Signal.Unit  = 'Hour';
        
    case 'Heading'
        [flag, Data] = read_SpeedHeading_bin(this, 'Memmapfile', 1);
        if ~flag
            return
        end
        Signal.Time  = Data.TimeCPU;
        Signal.Value = Data.Heading;
        Signal.Unit  = 'deg';
        
    case 'PingNumber'
        [flag, Data] = read_data(this, 'Memmapfile', 1, 'NumSounder', NumSounder);
        if ~flag
            return
        end
        Signal.Time  = Data.TimeCPU;
        Signal.Value = double(Data.PingNumber(:,NumChannel));
        Signal.Unit  = '#';
        
    case 'Height'
        [flag, Data] = read_data(this, 'Memmapfile', 1, 'NumSounder', NumSounder);
        if ~flag
            return
        end
        
        [flag, DataSPD] = read_STDProfile_bin(this, 'memmapfile', 1);
        if flag
            SoundVelocity = DataSPD.SoundVelocity(:);
            %     figure; plot(DataSPD.SoundVelocity(:), DataSPD.Depth(:)); grid on
            SoundVelocity = mean(SoundVelocity);
        else
            SoundVelocity = 1500;
        end
        SoundVelocity = repmat(SoundVelocity, Data.Dimensions.nbPings, 1);
        
        [flag, DataChannel] = read_Channel_bin(this, 'NumSounder', NumSounder, 'TypeSounder', 1);
        if ~flag
            return
        end
        
        SampleFrequency  = repmat(1 / DataChannel.TimeSampleInterval(NumChannel), Data.Dimensions.nbPings, 1);
        
        Signal.Time = Data.TimeCPU;
        Signal.Value = -single(Data.DetectedBottomRange(:,NumChannel)) * 2 .* SampleFrequency ./ SoundVelocity;
        Signal.Unit  = 'm';
        
    otherwise
        'Pas encore pr�t'
end
Signal = get_TimeNumChannel(Signal, NumChannel);

function Signal = get_TimeNumChannel(Signal, NumChannel)

T = Signal.Time.timeMat;
T  = T(:,NumChannel);
Signal.Time = cl_time('timeMat', T);
