function [flag, typeSounders] = get_typeSounders(this)

typeSounders = [];

[flag, listSounders] = get_listSounders(this);
if ~flag
    return
end

nbSounders = length(listSounders);
typeSounders = zeros(1,nbSounders);
for k=1:nbSounders
    switch listSounders(k).Name
        case 'EK60'
            typeSounders(k) = 1;
        case 'ME70'
            typeSounders(k) = 2;
        case 'Generic'
            typeSounders(k) = 2;
        otherwise
            str1 = sprintf('%s pas encore pr�vu dans cl_hac/get_typeSounders : transmettez le fichier HAC � sonarscope@ifremer.fr SVP', listSounders(k).Name);
            str2 = sprintf('%s not yet available in cl_hac/get_typeSounders : please send the HAC file to sonarscope@ifremer.fr', listSounders(k).Name);
            my_warndlg(Lang(str1,str2), 1);
    end
end
