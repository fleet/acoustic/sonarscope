% Histogramme des type de datagrams contenus dans un fichier .hac
%
% Syntax
%   histo_index(a)
%
% Input Arguments
%   a : Instance de cl_simrad_all
%
% Name-Value Pair Arguments
%   fullListe : 1=affichage que des Tuples presents, 0=tous les Tuples
%
% Output Arguments
%   []                : Auto-plot activation
%   texteTupleType : Resume textuel de l'histogramme
%   Code              : Abscisses
%   histoTupleType : Nb de Tuplemes (ordonnees)
%
% Examples
%   nomFic = getNomFicDatabase('PELGAS10_003_20100428_183857.hac')
%   a = cl_hac('nomFic', nomFic);
%   histo_index(a)
%   [texteTupleType] = histo_index(a)
%   [texteTupleType, Code, histoTupleType] = histo_index(a)
%   figure; bar(Code, histoTupleType)
%
% See also cl_hac Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [texteTupleType, Code, histoTupleType] = histo_index(this, varargin)

flagPlot = (nargout == 0);

NbFic = length(this);
hw = create_waitbar('Plot .hac Histo Index', 'N', NbFic);
for k=1:NbFic
    my_waitbar(k, NbFic, hw)
    [flag, texteTupleType, Code(k,:), histoTupleType(k,:)] = histo_index_unitaire(this(k), flagPlot, varargin{:}); %#ok<AGROW>
end
my_close(hw, 'MsgEnd')


function [flag, texteTupleType, Code, histoTupleType, tabNbBytes] = histo_index_unitaire(this, flagPlot, varargin)

[varargin, Fig] = getPropertyValue(varargin, 'Fig', []);

[varargin, fullListe] = getPropertyValue(varargin, 'fullListe', 0); %#ok<ASGLU>

[flag, Data] = read_FileIndex_bin(this, 'Memmapfile', 1);
if ~flag
    texteTupleType = [];
    Code = [];
    histoTupleType = [];
    tabNbBytes = [];
    return
end
TupleType = Data.TupleType(:);

listeTupleType = unique(TupleType);
nbTupleTypes   = length(listeTupleType);
tabNbBytes     = zeros(1,nbTupleTypes );

TupleSize = Data.TupleSize(:);
for i=1:nbTupleTypes
    sub = (TupleType == listeTupleType(i));
    tabNbBytes(i) = sum(TupleSize(sub));
    %     fprintf('Tuplem %3d    Nb of Bytes %10d\n', listeTupleType(i), nbBytes(i));
end

%% Lecture du fichier d'index

Code = unique(TupleType);
histoTupleType = my_hist(TupleType, Code);
texteTupleType = codesTuplesHac(Code, histoTupleType, 'fullListe', fullListe);

% tabNbBytes = zeros(size(histoTupleType));
% tabNbBytes(histoTupleType ~= 0) = nbBytes;
if flagPlot
    plot_histo_index_tab(Code, histoTupleType, texteTupleType, tabNbBytes, this.nomFic, 'Fig', Fig);
end
