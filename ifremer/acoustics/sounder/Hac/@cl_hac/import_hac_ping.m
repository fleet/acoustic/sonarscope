function [flag, b, Carto, Data, DataPosition, DataSpeedHeading, DataSPD, DataEchoSounder, SonarDescription] = import_hac_ping(this, NumSounder, Carto, varargin)

[varargin, Memmapfile]       = getPropertyValue(varargin, 'Memmapfile',        1);
[varargin, TypeSounder]      = getPropertyValue(varargin, 'TypeSounder',      []);
[varargin, listeLayers]      = getPropertyValue(varargin, 'listeLayers',       1);
[varargin, PingNumbers]      = getPropertyValue(varargin, 'PingNumbers',       1);
[varargin, DataPosition]     = getPropertyValue(varargin, 'DataPosition',     []);
[varargin, DataSpeedHeading] = getPropertyValue(varargin, 'DataSpeedHeading', []);
[varargin, DataSPD]          = getPropertyValue(varargin, 'DataSPD',          []);
[varargin, DataChannel]      = getPropertyValue(varargin, 'DataChannel',      []);
[varargin, DataEchoSounder]  = getPropertyValue(varargin, 'DataEchoSounder',  []);
[varargin, SonarDescription] = getPropertyValue(varargin, 'SonarDescription', []); %#ok<ASGLU>

Data  = [];
Phase = [];

b = cl_image.empty;
c = cl_image.empty;

% Non utilis� pour
LoadAmp         = any(listeLayers == 1);
LoadPhaseAcross = any(listeLayers == 2);
LoadPhaseAlong  = any(listeLayers == 3);

if isempty(TypeSounder)
    [flag, typeSounders] = get_typeSounders(this);
    TypeSounder = typeSounders(NumSounder);
end

% En attendant de visionner les �chogrammes de phases.
if LoadAmp && isempty(Data)
    [flag, Data] = read_data(this, 'Memmapfile', Memmapfile, 'NumSounder', NumSounder); % , 'NumSounder', []);
    if ~flag
        return
    end
end
if isinf(PingNumbers)
    PingNumbers = 1:Data.Dimensions.nbPings;
end

if LoadPhaseAcross ||  LoadPhaseAlong
    [flag, Phase] = read_phase(this, 'Memmapfile', Memmapfile, 'NumSounder', NumSounder); % , 'NumSounder', []);
    if ~flag
        LoadPhaseAcross = 0;
        LoadPhaseAlong  = 0;
        % Sortie imm�diate l'op�rateur n'a pas demand� pas le chargement des datas
        % en plus des phases absentes.
        if ~LoadAmp
            return
        end
    end
end

%% Lecture de la navigation

if isempty(DataPosition)
    [flag, DataPosition] = read_position_bin(this, 'memmapfile', 1);
    if ~flag
        return
    end
end

if ~isempty(Data)
    TimeSounder = Data.TimeCPU;
    TimeSounder = TimeSounder.timeMat;
    dimSignals  = Data.Dimensions;
end

Latitude  = DataPosition.Latitude(:);
Longitude = DataPosition.Longitude(:);
TimeNav   = DataPosition.TimeCPU;

if isempty(DataSpeedHeading)
    [flag, DataSpeedHeading] = read_SpeedHeading_bin(this, 'memmapfile', 1);
    if ~flag
        return
    end
end
TimeSpeedHeading = DataSpeedHeading.TimeCPU;
Heading          = DataSpeedHeading.Heading(:);
Speed            = DataSpeedHeading.Speed(:);

%% Lecture de la bathyc�l�rim�trie

if isempty(DataSPD)
    [~, DataSPD] = read_STDProfile_bin(this, 'memmapfile', 1);
end
if isempty(DataSPD)
    SoundVelocity = 1500;
else
    SoundVelocity = DataSPD.SoundVelocity(:);
    SoundVelocity = mean(SoundVelocity);
end
SoundVelocity = repmat(SoundVelocity, Data.Dimensions.nbPings, 1);

%% Lecture des configs. d'acquisition

if isempty(DataChannel)
    [flag, DataChannel] = read_Channel_bin(this, 'memmapfile', 1, 'NumSounder', NumSounder, 'TypeSounder', 2);
    if ~flag
        return
    end
end
AcousticFrequency = DataChannel.AcousticFrequency;

%% Lecture de EchoSounder

if isempty(DataEchoSounder)
    [flag, DataEchoSounder] = read_EchoSounder_bin(this, 'NumSounder', NumSounder, 'TypeSounder', TypeSounder);
    if ~flag
        return
    end
end

%% lecture des images

if ~strcmp(DataChannel.Title, 'GenericChannel')
    nbFreq = Data.Dimensions.nbFreq;
    nomSondeur = strrep(DataChannel.Title, 'Channel', '');
else
    nomSondeur = strrep(DataChannel.Title, 'GenericChannel', 'HAC_Generic');
    nbFreq = 1;
end

%% Lecture du fichier

if isempty(SonarDescription)
    SonarDescription = cl_sounder('Sonar.Ident', 17);
end

for kPing=1:length(PingNumbers)
    iPing = PingNumbers(kPing);
    HeightInMeters = Data.DetectedBottomRange(iPing,:);
    
    DataPing.PingCounter        = iPing;
    DataPing.SystemSerialNumber = [];
    % DataPing.SampleRate         = 1 /  unique(DataChannel.SamplingRate);
    DataPing.EmModel            = DataChannel.Title;
    DataPing.SoundVelocity      = DataEchoSounder.SoundSpeed;
    DataPing.SamplingRateMinMax = []; % TODO : Utile ? 1 / DataEchoSounder.TimeSampleInterval;
    DataPing.Latitude           = [];
    DataPing.Longitude          = [];
    DataPing.Heading            = [];
    % DataPing.TransducerDepth    = unique(DataChannel.TransducerInstallationDepth);
    DataPing.Time               = []; % [1x1 cl_time]
    
    DataPing.TxAngle              = NaN(1, nbFreq);
    DataPing.BeamPointingAngle    = NaN(1, nbFreq);
    DataPing.TransmitSectorNumber = NaN(1, nbFreq);
    DataPing.AcrossDist           = NaN(1, nbFreq);
    DataPing.AlongDist            = NaN(1, nbFreq);
    DataPing.BeamPointingAngle    = NaN(1, nbFreq);
    DataPing.R1SamplesFiltre      = NaN(1, nbFreq);
    
    iFreq = 1;
    % for iFreq=1:nbFreq
    
    %% Interpr�tation du fichier
    
    if LoadAmp
        if ~strcmp(DataChannel.Title, 'GenericChannel')
            Reflec = squeeze(Data.Samples(:,:,iPing,iFreq));
        else
            Reflec = squeeze(Data.Samples(iPing,:,:));
            Reflec = flipud(Reflec);
        end
        % TODO : GLU : r�cup�rer les info permettant de calculer x : sampleFrequency
        x = 1:size(Reflec,2);
        y = 1:size(Reflec,1);
    end
    
    if LoadPhaseAcross
        if ~strcmp(DataChannel.Title, 'GenericChannel')
            PhaseAcrossAngle = squeeze(Phase.AthwartShip(:,:,iPing));
            PhaseAcrossAngle = flipud(PhaseAcrossAngle);  % ??? GLU: Etudier le parall�le avec les Amplitudes plus haut.
        else
            PhaseAcrossAngle = squeeze(Phase.AthwartShip(:,:,iPing,iFreq));
            
        end
        % TODO : GLU : r�cup�rer les info permettant de calculer x : sampleFrequency
        xAcross = 1:size(PhaseAcrossAngle,2);
        yAcross = 1:size(PhaseAcrossAngle,1);
    end
    if LoadPhaseAlong
        if ~strcmp(DataChannel.Title, 'GenericChannel')
            PhaseAlongAngle  = squeeze(Phase.AlongShip(:,:,iPing));
            PhaseAlongAngle  = flipud(PhaseAlongAngle);
        else
            PhaseAlongAngle  = squeeze(Phase.AlongShip(:,:,iPing,iFreq));
        end
        % TODO : GLU : r�cup�rer les info permettant de calculer x : sampleFrequency
        xAlong = 1:size(PhaseAlongAngle,2);
        yAlong = 1:size(PhaseAlongAngle,1);
    end
    
    % Reflec(Reflec == 0) = NaN;
    
    
    %{
% TODO : GLU : quelle diff�rence entre Time et GPSTime ?
DataPosition =
Dimensions: [1x1 struct]
Time: [1x1 cl_time]
GPSTime: [601x1 cl_memmapfile]
PositSystem: [601x1 cl_memmapfile]
    %}
    % Bizarrerie pourME70
    if TypeSounder == 2
        if ~strcmp(nomSondeur, 'HAC_Generic')
            SampleFrequency  = repmat(1 / DataEchoSounder.TimeSampleInterval(:), Data.Dimensions.nbPings, 1); % TODO LB: voir avec LB
        else % Cas du sondeur Generic
            SampleFrequency  = repmat(1 / DataChannel.SamplingInterval(:), Data.Dimensions.nbPings, 1); % TODO LB: voir avec LB
        end
        if isfield(DataChannel,'AthwartshipSteeringAngle')
            AthwartshipSteeringAngle = DataChannel.AthwartshipSteeringAngle;
        else
            AthwartshipSteeringAngle = DataChannel.AthwartshipAngleOffsetOfTransducer;
        end
        if isfield(DataChannel,'AlongshipSteeringAngle')
            AlongshipSteeringAngle = DataChannel.AlongshipSteeringAngle;
        else
            disp(['A faire confirmer par JMA/BRG : ' mfilename]);
            AlongshipSteeringAngle = DataChannel.AlongshipSteeringAngle;
        end
    else
        SampleFrequency  = repmat(1 / DataChannel.SamplingInterval(:), Data.Dimensions.nbPings, 1);
        %         DataChannel.TransducerFaceAthwartshipAngleOffset
        AthwartshipSteeringAngle = DataChannel.AthwartshipAngleOffsetOfTransducer';
        AlongshipSteeringAngle   = DataChannel.AlongshipAngleOffsetOfTransducer';
    end
    
    HeightInSamples = single(HeightInMeters) * 2 .* SampleFrequency(iPing) ./ SoundVelocity(iPing);
    
    %% Cr�ation de l'Reflec
    
    %     DataPing                    = Extraction(DataPing, iPing, SampleFrequency(iPing));
    %     DataPing.TxPulseWidth       = DataEchoSounder.PulseDuration;
    %     DataPing.DataPing           = SoundVelocity;
    %     DataPing.SystemSeriaNumber  = DataChannel.SystemSerialNumber;
    %     DataPing.R0                 = R0;
    %     DataPing.RMax1              = RMax1;
    %     DataPing.RMax2              = RMax2;
    %     DataPing.iBeamMax0          = iBeamMax0;
    %     DataPing.AmpPingNormHorzMax = AmpPingNormHorzMax;
    %     DataPing.MaskWidth          = MaskWidth;
    %     DataPing.DataTVG            = [];
    %     DataPing.iBeamBeg           = 1;
    %     DataPing.iBeamEnd           = DataEchoSounder.NumberSWChannels;
    
    Comments = sprintf('SounderName=%s  NumSounder=%d  Ping=%d', nomSondeur, NumSounder, iPing);
    nomFic          = get(this, 'nomFic');
    [~, ImageName]  = fileparts(nomFic);
    TagSynchroX     = [ImageName 'SampleBeam'];
    TagSynchroY     = [ImageName 'SampleBeam'];
    ImageName       = sprintf('%s_%d', ImageName, iPing);
    if LoadAmp
        c(end+1) = cl_image('Image', Reflec, ...
            'Name',           ImageName, ...
            'Unit',                'dB', ...
            'x',                    x, ...
            'y',                    y, ...
            'XUnit',               '"', ...
            'YUnit',               'Sample', ...
            'YDir',                 2, ...
            'ColormapIndex',        3, ...
            'DataType',             cl_image.indDataType('Reflectivity'), ...
            'TagSynchroX',          TagSynchroX, ...
            'TagSynchroY',          TagSynchroY, ...
            'GeometryType',         cl_image.indGeometryType('SampleBeam'), ...
            'InitialFileName',      nomFic, 'InitialFileFormat', 'HAC', ...
            'SonarDescription',     SonarDescription, ...
            'Comments',             Comments);
    end
    
    if LoadPhaseAcross
        c(end+1) = cl_image('Image', PhaseAcrossAngle, ...
            'Name',           ImageName, ...
            'Unit',                'deg', ...
            'x',                    xAcross, ...
            'y',                    yAcross, ...
            'XUnit',               '"', ...
            'YUnit',               'Ping', ...
            'ColormapIndex',        17, ...
            'Video',                2, ...
            'DataType',             cl_image.indDataType('SonarAcrossBeamAngle'), ...
            'TagSynchroX',          TagSynchroX, ...
            'TagSynchroY',          TagSynchroY, ...
            'GeometryType',         cl_image.indGeometryType('SampleBeam'), ...
            'InitialFileName',      nomFic, 'InitialFileFormat', 'HAC', ...
            'SonarDescription',      SonarDescription, ...
            'Comments',             Comments); %#ok<AGROW>
    end
    
    if LoadPhaseAlong
        c(end+1) = cl_image('Image', PhaseAlongAngle, ...
            'Name',           ImageName, ...
            'Unit',                'deg', ...
            'x',                    xAlong, ...
            'y',                    yAlong, ...
            'XUnit',               '"', ...
            'YUnit',               'Ping', ...
            'ColormapIndex',        3, ...
            'DataType',             cl_image.indDataType('SonarAlongBeamAngle'), ...
            'TagSynchroX',          TagSynchroX, ...
            'TagSynchroY',          TagSynchroY, ...
            'GeometryType',         cl_image.indGeometryType('SampleBeam'), ...
            'InitialFileName',      nomFic, 'InitialFileFormat', 'HAC', ...
            'SonarDescription',     SonarDescription, ...
            'Comments',             Comments); %#ok<AGROW>
    end
    
    
    TimeFreq      = cl_time('timeMat', TimeSounder(iPing,iFreq));
    LatitudeFreq  = my_interp1(TimeNav, Latitude,  TimeFreq);
    LongitudeFreq = my_interp1_longitude(TimeNav, Longitude, TimeFreq);
    
    HeadingFreq = my_interp1_headingDeg(TimeSpeedHeading, Heading,  TimeFreq);
    SpeedFreq   = my_interp1(TimeSpeedHeading, Speed,  TimeFreq);
    
    % SonarScope(b);
    % end
    % Gestion des donn�es pour caract�riser un Ping.
    DataPing.Latitude  = LatitudeFreq;
    DataPing.Longitude = LongitudeFreq;
    DataPing.Heading   = HeadingFreq;
    DataPing.Time      = TimeFreq;
    DataPing.TxAngle   = AthwartshipSteeringAngle(kPing,:)';
    DataPing.BeamPointingAngle    = AthwartshipSteeringAngle(kPing,:)'; % TODO : rechercher pourquoi on a besoin des 2 info dans le cas des EM
    nbBeams                       = length(DataPing.TxAngle);
    DataPing.TransmitSectorNumber = ones(nbBeams,1);
    DataPing.R1SamplesFiltre      = HeightInSamples;
    D = SoundVelocity(iPing) * HeightInSamples ./ (2 * SampleFrequency(iPing));
    DataPing.AcrossDist           = (D' .* sind(DataPing.TxAngle));
    DataPing.AlongDist            = zeros(nbBeams,1);
    
    
    for q=1:length(c)
        c(q) = set(c(q), 'SonarHeading',                HeadingFreq);
        c(q) = set(c(q), 'SonarSpeed',                  SpeedFreq);
        c(q) = set(c(q), 'SonarFishLatitude',           LatitudeFreq);
        c(q) = set(c(q), 'SonarFishLongitude',          LongitudeFreq);
        c(q) = set(c(q), 'SampleFrequency',             SampleFrequency(iPing));
        c(q) = set(c(q), 'SonarSurfaceSoundSpeed',      SoundVelocity(iPing));
        c(q) = set(c(q), 'SonarTime',                   TimeFreq);
        c(q) = set(c(q), 'SonarTxAcrossAngle',          AthwartshipSteeringAngle);
        c(q) = set(c(q), 'SonarTxAlongAngle',           AlongshipSteeringAngle);
        
        if isempty(Carto)
            Carto = getDefinitionCarto(c(q));
        end
        c(q) = set(c(q),  'Carto', Carto);
        
        % TODO JMA : il faudrait obtenir la fr�quence : cr�er un nouveau champ "Frequence" dans Data
        % Voir � r�cup�rer les donn�es Data.Channel.
        c(q) = update_Name(c(q));
        
        c(q) = set_SampleBeamData(c(q), DataPing);
    end
    
    %     if length(PingNumbers) == 1
    %         FigUtils.createSScFigure; PlotUtils.createSScPlot(HeightInSamples); grid on; title(ImageName, 'Interpreter', 'None');
    %         set(gca, 'YDir', 'Reverse');
    %     end
    
end
b = [b ; c(:)];
flag = 1;

%{
function DataPing = Extraction(DataPing, i, SampleFrequency)

str1 = 'ATTENTION : ce format de donn�e ne donne pas acc�s aux param�tres du sondeur ni � la c�l�rit�.';
str2 = 'BEWARE : this format does not deliver sonar parameters ans sound velocity.';
my_warndlg(Lang(str1,str2), 0, 'Tag', 'DonnesBrutesHacIndeterminees');

% TODO JMA
DataPing.SampleRate         = SampleFrequency;    % Hz
DataPing.TxPulseWidth       = 150e-6;   % s
DataPing.ReceiveBeamWidth   = 1.5;      % Deg
DataPing.PingCounter        = i;
DataPing.RangeMax           = 100; % (m)
DataPing.IdentSonar         = 17;
DataPing.SystemSerialNumber = 0;
DataPing.SoundVelocity      = 1477.5;
DataPing.Frequency          = 307;
DataPing.Time               = now;

% Pour compatibility� avec syst�mes Reson
DataPing.MultiPingSequence = 1;
%}

