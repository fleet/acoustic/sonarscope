function [flag, b, Carto] = import_hac_profil(this, NumSounder, Carto, varargin)

[varargin, Memmapfile]  = getPropertyValue(varargin, 'Memmapfile',  1);
[varargin, listeLayers] = getPropertyValue(varargin, 'listeLayers', 1);
[varargin, TypeSounder] = getPropertyValue(varargin, 'TypeSounder', 1); %#ok<ASGLU>
% [varargin, typeImport] = getPropertyValue(varargin, 'typeImport', 1);

b               = cl_image.empty;
LoadAmp         = any(listeLayers == 1);
LoadPhaseAcross = any(listeLayers == 2);
LoadPhaseAlong  = any(listeLayers == 3);
Data            = [];
Phase           = [];
if LoadAmp
    [flag, Data] = read_data(this, 'Memmapfile', Memmapfile, 'NumSounder', NumSounder); % , 'NumSounder', []);
    if ~flag
        return
    end
end
% TimeSounder = Data.TimeCPU;
% pppp        = TimeSounder.timeMat;
% TimeSounder = reshape(pppp, [Data.Dimensions.nbPings Data.Dimensions.nbFreq]);
% figure; plot(Data.DetectedBottomRange(:,:)); grid on;

if LoadPhaseAcross ||  LoadPhaseAlong
    [flag, Phase] = read_phase(this, 'Memmapfile', Memmapfile, 'NumSounder', NumSounder); % , 'NumSounder', []);
    if ~flag
        LoadPhaseAcross = 0;
        LoadPhaseAlong  = 0;
        % Sortie imm�diate l'op�rateur n'a pas demand� pas le chargement des datas
        % en plus des phases absentes.
        if ~LoadAmp
            return
        end
    end
end

if ~isempty(Data)
    TimeSounder = Data.TimeCPU;
    TimeSounder = TimeSounder.timeMat;
    nbPings     = Data.Dimensions.nbPings;
    dimSignals  = Data.Dimensions;
    detectedBottomRange = Data.DetectedBottomRange;
    pingNumber  = Data.PingNumber(:,1);
elseif ~isempty(Phase)
    TimeSounder = Phase.TimeCPU;
    TimeSounder = TimeSounder.timeMat;
    nbPings     = Phase.Dimensions.nbPings;
    dimSignals  = Phase.Dimensions;
    detectedBottomRange = Phase.DetectedBottomRange;
    pingNumber  = Phase.PingNumber;
end
% 'TODO GLU Correction bug heure'
% sz = size(TimeSounder);
% TimeSounder = linspace(min(TimeSounder(:)), max(TimeSounder(:)), sz(1));
% TimeSounder = repmat(TimeSounder(:), 1, 5);

%% lecture de l'attitude

[flag, DataAttitude] = read_attitude_bin(this,  'NumSounder', NumSounder, 'Memmapfile', 1);
if ~flag
    nomFic = get(this, 'nomFic');
    [~, nomFic, ~] = fileparts(nomFic);
    strFR = sprintf('Abscence de donn�es d''attitude dans l''exploitation du fichier HAC : % s\n', nomFic);
    strUS = sprintf('Not any attitude data in HAC file : % s\n', nomFic);
    my_warndlg(Lang(strFR, strUS), 1);
    return
end
TimeAttitude = DataAttitude.TimeCPU;
Pitch        = DataAttitude.Pitch(:);
Roll         = DataAttitude.Roll(:);
Heave        = DataAttitude.Heave(:);
Yaw          = DataAttitude.Yaw(:);

%% Lecture de la navigation

[flag, DataPosition] = read_position_bin(this, 'memmapfile', 1);
if ~flag
    return
end

Latitude  = DataPosition.Latitude(:);
Longitude = DataPosition.Longitude(:);
TimeNav   = DataPosition.TimeCPU;

%% Lecture de la vitesse et du cap

[flag, DataSpeedHeading] = read_SpeedHeading_bin(this, 'memmapfile', 1);
if ~flag
    return
end
TimeSpeedHeading    = DataSpeedHeading.TimeCPU;
Heading             = DataSpeedHeading.Heading(:);
Speed               = DataSpeedHeading.Speed(:);

%% Lecture de la bathyc�l�rim�trie

[flag, DataSPD] = read_STDProfile_bin(this, 'memmapfile', 1);
if flag
    SoundVelocity = DataSPD.SoundVelocity(:);
    %     figure; plot(DataSPD.SoundVelocity(:), DataSPD.Depth(:)); grid on
    SoundVelocity = mean(SoundVelocity);
else
    SoundVelocity = 1500;
end
SoundVelocity = repmat(SoundVelocity, nbPings, 1);

%% Lecture de Channel

% Lecture des configs. d'acquisition
% TODO / GLU : Attention, ici on suppose que c'est de l'EK60 typeImport=1
% dans read_Channel_bin. Il faut r�cup�rer les nom du sondeur et le passer
% � read_Channel_bin genre get_nomSounder(this, NumSounder)
%%%%%%%[flag, DataChannel] = read_Channel_bin(this, 'NumSounder', NumSounder, 'TypeSounder', TypeSounder, 'Dimensions', dimSignals); %, 'typeImport', typeImport);
[flag, DataChannel] = read_Channel_bin(this, 'NumSounder', NumSounder, 'TypeSounder', TypeSounder); %, 'typeImport', typeImport);
if ~flag
    return
end
AcousticFrequency = DataChannel.AcousticFrequency;
if strfind(DataChannel.Title, 'GenericChannel')
    nomSondeur = strrep(DataChannel.Title, 'GenericChannel', 'HAC_Generic');
else
    nomSondeur = strrep(DataChannel.Title, 'Channel', '');
end
if isfield(DataChannel, 'TransducerFaceAthwartshipAngleOffset')
    if DataChannel.TransducerFaceAthwartshipAngleOffset == -90
        Orientation = 'Horizontal';
    else
        Orientation = 'Vertical';
    end
else
    Orientation = 'Vertical';
end

%% Lecture de EchoSounder

[flag, DataEchoSounder] = read_EchoSounder_bin(this, 'NumSounder', NumSounder, 'TypeSounder', TypeSounder); %, 'typeImport', typeImport);
if ~flag
    return
end

%% lecture des images

if TypeSounder == 1
    nbSlices = dimSignals.nbFreq;
else
    if strfind(DataChannel.Title, 'GenericChannel') % Cas du sondeur anecdotique Generic.
        nbSlices = dimSignals.nbFreq;
    else
        nbSlices = dimSignals.nbBeams;
    end
end

b = cl_image.empty;
str1 = 'Lecture des canaux du fichier HAC';
str2 = 'Reading the channels from HAC file';
hw = create_waitbar(Lang(str1,str2), 'N', nbSlices);
for k=1:nbSlices
    my_waitbar(k, nbSlices, hw)
    
    Comments = sprintf('SounderName=%s  NumSounder=%d  NumChannel=%d  Frequency=%s kHz Orientation=%s', ...
        nomSondeur, NumSounder, k, num2str(AcousticFrequency(k)/1000), Orientation);
    
    c =  cl_image.empty;
    %% Interpr�tation du fichier
    
    if LoadAmp
        if TypeSounder == 2
            Reflec = squeeze(Data.Samples(:,k,:));
            Reflec = Reflec';
        else
            Reflec = squeeze(Data.Samples(:,:,k));
        end
        % TODO : GLU : r�cup�rer les info permettant de calculer x : sampleFrequency
        x = 1:size(Reflec,2);
        y = 1:size(Reflec,1);
    end
    
    if LoadPhaseAcross
        if TypeSounder == 2
            PhaseAcrossAngle = squeeze(Phase.AthwartShip(:,k,:));
            PhaseAcrossAngle = PhaseAcrossAngle';
        else
            PhaseAcrossAngle = squeeze(Phase.AthwartShip(:,:,k));
        end
        % TODO : GLU : r�cup�rer les info permettant de calculer x : sampleFrequency
        xAcross = 1:size(PhaseAcrossAngle,2);
        yAcross = 1:size(PhaseAcrossAngle,1);
    end
    if LoadPhaseAlong
        if TypeSounder == 2
            PhaseAlongAngle  = squeeze(Phase.AlongShip(:,k,:));
            PhaseAlongAngle = PhaseAlongAngle';
        else
            PhaseAlongAngle  = squeeze(Phase.AlongShip(:,:,k));
        end
        % TODO : GLU : r�cup�rer les info permettant de calculer x : sampleFrequency
        xAlong = 1:size(PhaseAlongAngle,2);
        yAlong = 1:size(PhaseAlongAngle,1);
    end
    
    
    %{
% TODO : GLU : quelle diff�rence entre Time et GPSTime ?
DataPosition =
Dimensions: [1x1 struct]
Time: [1x1 cl_time]
GPSTime: [601x1 cl_memmapfile]
PositSystem: [601x1 cl_memmapfile]
    %}
    
    
    %% Cr�ation de l'image
    
    % TODO : compl�ter cl_sounder pour tous les sondeurs de p�che
    
    %     strMode_1 = [num2str(floor(AcousticFrequency(k)/1000)) ' kHz'];
    Sonar = identSonar(cl_sounder, 'SonarName', nomSondeur); % , 'SonarStrMode_1', strMode_1);
    SonarDescription = cl_sounder('Sonar.Ident', Sonar.Ident, 'Sonar.Mode_1',1); % Sonar.Mode_1);
    
    nomFic = get(this, 'nomFic');
    [~, ImageName] = fileparts(nomFic);
    TagSynchroX = ImageName;
    TagSynchroY = ImageName;
    
    if LoadAmp
        c(end+1) = cl_image('Image', Reflec, ...
            'Name',           ImageName, ...
            'Unit',                'dB', ...
            'x',                    x, ...
            'y',                    y, ...
            'XUnit',               '"', ...
            'YUnit',               'Ping', ...
            'ColormapIndex',        3, ...
            'DataType',             cl_image.indDataType('Reflectivity'), ...
            'TagSynchroX',          TagSynchroX, ...
            'TagSynchroY',          TagSynchroY, ...
            'GeometryType',         cl_image.indGeometryType('PingSamples'), ...
            'InitialFileName',      nomFic, 'InitialFileFormat', 'HAC', ...
            'SonarDescription',     SonarDescription, ...
            'Comments',             Comments); %#ok<AGROW>
    end
    
    if LoadPhaseAcross
        %         if ~isequal(size(Reflec), size(PhaseAcrossAngle))
        %             str1 = sprintf('Taille de PhaseAcrossAngle diff�rente de Reflec pour "%s"', ImageName);
        %             str2 = 'TODO';
        %             my_warndlg(Lang(str1,str2), 0);
        %         end
        c(end+1) = cl_image('Image', PhaseAcrossAngle, ...
            'Name',           ImageName, ...
            'Unit',                'deg', ...
            'x',                    xAcross, ...
            'y',                    yAcross, ...
            'XUnit',               '"', ...
            'YUnit',               'Ping', ...
            'ColormapIndex',        17, ...
            'Video',                2, ...
            'DataType',             cl_image.indDataType('SonarAcrossBeamAngle'), ...
            'TagSynchroX',          TagSynchroX, ...
            'TagSynchroY',          TagSynchroY, ...
            'GeometryType',         cl_image.indGeometryType('PingSamples'), ...
            'InitialFileName',      nomFic, 'InitialFileFormat', 'HAC', ...
            'SonarDescription',      SonarDescription, ...
            'Comments',             Comments); %#ok<AGROW>
    end
    
    if LoadPhaseAlong
        %         if ~isequal(size(Reflec), size(PhaseAlongAngle))
        %             str1 = sprintf('Taille de PhaseAlongAngle diff�rente de Reflec pour "%s"', ImageName);
        %             str2 = 'TODO';
        %             my_warndlg(Lang(str1,str2), 0);
        %         end
        c(end+1) = cl_image('Image', PhaseAlongAngle, ...
            'Name',           ImageName, ...
            'Unit',                'deg', ...
            'x',                    xAlong, ...
            'y',                    yAlong, ...
            'XUnit',               '"', ...
            'YUnit',               'Ping', ...
            'ColormapIndex',        3, ...
            'DataType',             cl_image.indDataType('SonarAlongBeamAngle'), ...
            'TagSynchroX',          TagSynchroX, ...
            'TagSynchroY',          TagSynchroY, ...
            'GeometryType',         cl_image.indGeometryType('PingSamples'), ...
            'InitialFileName',      nomFic, 'InitialFileFormat', 'HAC', ...
            'SonarDescription',     SonarDescription, ...
            'Comments',             Comments); %#ok<AGROW>
    end
    
    TimeFreq = cl_time('timeMat', TimeSounder(:,k));
    LatitudeFreq  = my_interp1(TimeNav, Latitude,  TimeFreq, 'linear', 'extrap');
    LongitudeFreq = my_interp1_longitude(TimeNav, Longitude, TimeFreq, 'linear', 'extrap');
    
    HeadingFreq = my_interp1_headingDeg(TimeSpeedHeading, Heading,  TimeFreq, 'linear', 'extrap');
    SpeedFreq   = my_interp1(TimeSpeedHeading, Speed,  TimeFreq, 'linear', 'extrap');
    
    % Bizarrerie pourME70
    if TypeSounder == 2
        if ~strcmp(nomSondeur, 'HAC_Generic')
            SampleFrequency  = repmat(1 / DataEchoSounder.TimeSampleInterval, nbPings, 1); % TODO LB: voir avec LB
        else % Cas du sondeur Generic
            SampleFrequency  = DataChannel.SamplingInterval;
        end
        if isfield(DataChannel,'AthwartshipSteeringAngle')
            AthwartshipSteeringAngle = DataChannel.AthwartshipSteeringAngle(k);
        else
            AthwartshipSteeringAngle = DataChannel.AthwartshipAngleOffsetOfTransducer(k);
        end
        if isfield(DataChannel,'AlongshipSteeringAngle')
            AlongshipSteeringAngle = DataChannel.AlongshipSteeringAngle(k);
        else
            AlongshipSteeringAngle = DataChannel.AlongshipAngleOffsetOfTransducer(k);
        end
    else
        SampleFrequency  = repmat(1 / DataChannel.TimeSampleInterval(k), nbPings, 1);
        %         DataChannel.TransducerFaceAthwartshipAngleOffset
        AthwartshipSteeringAngle = 0;
        AlongshipSteeringAngle   = 0;
    end
    AthwartshipSteeringAngle = repmat(AthwartshipSteeringAngle, nbPings, 1);
    AlongshipSteeringAngle   = repmat(AlongshipSteeringAngle, nbPings, 1);
    
    PitchFreq = my_interp1(TimeAttitude, Pitch, TimeFreq, 'linear', 'extrap');
    RollFreq  = my_interp1(TimeAttitude, Roll,  TimeFreq, 'linear', 'extrap');
    HeaveFreq = my_interp1(TimeAttitude, Heave, TimeFreq, 'linear', 'extrap');
    YawFreq   = my_interp1(TimeAttitude, Yaw,   TimeFreq, 'linear', 'extrap');
    %     figure(6744); PlotUtils.createSScPlot(Pitch, 'k'); grid on; title('Pitch')
    %     figure(6745); PlotUtils.createSScPlot(TimeAttitude, Pitch, 'k'); grid on; hold on; PlotUtils.createSScPlot(TimeFreq, PitchFreq, 'r'); hold off; title('Pitch')
    %     figure; PlotUtils.createSScPlot(TimeAttitude, Roll, 'k');  grid on; hold on; PlotUtils.createSScPlot(TimeFreq, RollFreq, 'r');  title('Roll')
    %     figure; PlotUtils.createSScPlot(TimeAttitude, Heave, 'k'); grid on; hold on; PlotUtils.createSScPlot(TimeFreq, HeaveFreq, 'r'); title('Heave')
    %     figure; PlotUtils.createSScPlot(TimeAttitude, Yaw, 'k');   grid on; hold on; PlotUtils.createSScPlot(TimeFreq, YawFreq, 'r');   title('Yaw')
    
    if TypeSounder == 2 % MultiBeam
        % Il nous faut identifier l'origine des tuples 901 ou 220 (ME70)
        if strcmp(nomSondeur, 'ME70')
            Immersion = zeros(nbPings, 1) - DataEchoSounder.TransducerInstallationDepth;
        else
            Immersion = zeros(nbPings, 1) - DataChannel.TransducerInstallationDepth(k);
        end
    else
        Immersion = -DataChannel.TransducerInstallationDepth(k);
        Immersion = repmat(Immersion, nbPings, 1);
    end
    
    if isvector(detectedBottomRange)
        % Cas de traitement exclusif des phases.
        Height = single(detectedBottomRange(:)) * 2 .* SampleFrequency(:) / SoundVelocity;
    else
        Height = single(detectedBottomRange(:,k)) * 2 * SampleFrequency(k) / SoundVelocity(k);
    end
    for q=1:length(c)
        c(q) = set(c(q), 'SonarPingCounter',   pingNumber(:));
        c(q) = set(c(q), 'SonarImmersion',     Immersion(:));
        c(q) = set(c(q), 'SonarHeight',        Height(:));
        c(q) = set(c(q), 'SonarHeading',       HeadingFreq(:));
        c(q) = set(c(q), 'SonarSpeed',         SpeedFreq(:));
        c(q) = set(c(q), 'SonarFishLatitude',  LatitudeFreq(:));
        c(q) = set(c(q), 'SonarFishLongitude', LongitudeFreq(:));
        c(q) = set(c(q), 'SampleFrequency',    SampleFrequency(:));
        c(q) = set(c(q), 'SonarSurfaceSoundSpeed',      SoundVelocity);
        c(q) = set(c(q), 'SonarTime',                   TimeFreq);
        
        c(q) = set(c(q), 'SonarRoll',           RollFreq);
        c(q) = set(c(q), 'SonarPitch',          PitchFreq);
        c(q) = set(c(q), 'SonarHeave',          HeaveFreq);
        c(q) = set(c(q), 'SonarYaw',            YawFreq);
        
        
        c(q) = set(c(q), 'SonarTxAcrossAngle',  AthwartshipSteeringAngle);
        c(q) = set(c(q), 'SonarTxAlongAngle',   AlongshipSteeringAngle);
        
        if isempty(Carto)
            Carto = getDefinitionCarto(c(q));
        end
        c(q) = set(c(q),  'Carto', Carto);
        
        if isfield(dimSignals, 'nbBeams')
            c(q) = update_Name(c(q), 'Append', ['B' num2str(k)]);
        end
        c(q) = update_Name(c(q), 'Append', [num2str(floor(AcousticFrequency(k)/1000)) ' kHz']);
    end
    % SonarScope(c);
    b = [b ; c(:)]; %#ok<AGROW>
end
my_close(hw, 'MsgEnd')

flag = 1;
