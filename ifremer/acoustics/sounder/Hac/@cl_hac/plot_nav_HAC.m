function [Fig, Carto] = plot_nav_HAC(~, nomFic, varargin)

[varargin, Carto] = getPropertyValue(varargin, 'Carto', []);
[varargin, Fig]   = getPropertyValue(varargin, 'Fig',   []); %#ok<ASGLU>

%% Cr�ation de la fen�tre avec les menu et callbacks sp�cialis�s pour la nav

Fig = plot_navigation_Figure(Fig);

%% Traitement des fichiers

NbFic = length(nomFic);
hw = create_waitbar('Plot .hac navigation', 'N', NbFic);
for k=1:NbFic
    my_waitbar(k, NbFic, hw);
    
    % Lecture des datagrams de navigation
    
    a = cl_hac('nomFic', nomFic{k});
    if isempty(a)
        continue
    end
    
    [flag, DataPosition] = read_position_bin(a, 'memmapfile', 1);
    if ~flag
        continue
    end
    
    Latitude  = DataPosition.Latitude(:);
    Longitude = DataPosition.Longitude(:);
    TimeNav   = DataPosition.TimeCPU;
    
    if ~flag || isempty(Latitude)
        continue
    end
    
    Longitude  = Longitude(:,1);
    Latitude   = Latitude(:,1);
    SonarTime  = TimeNav;
    Heading    = [];
    
    plot_navigation(nomFic{k}, Fig, Longitude, Latitude, SonarTime.timeMat, Heading);
end
my_close(hw, 'MsgEnd');

