function preprocess_HAC(~, nomFic)

if ~iscell(nomFic)
    nomFic = {nomFic};
end

% if DisplayHisto
%     Fig = figure;
% end

% profile on
tic
% H0 = cl_hac([]);
N = length(nomFic);
hw = create_waitbar('Preprocessing HAC', 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    fprintf('%d/%d - %s', k, N, nomFic{k});
    try
        a = cl_hac('nomFic', nomFic{k}); %#ok<NASGU>
%         if CheckHeight
%             histo_index(a, 'Fig', Fig);
%             cleanDirectories(H0, nomFic{k}) % N'est plus n�cessaire maintenant (c'�tait pour r�cup�rer des r�pertoires trait�s dans les premiers temps)
%         end
        fprintf(' : OK\n');
    catch ME
        ErrorReport = getReport(ME) %#ok<NOPRT>
        SendEmailSupport(ErrorReport)
        fprintf(' : ERROR\n');
    end
end
my_close(hw, 'MsgEnd')
% profile report
toc
