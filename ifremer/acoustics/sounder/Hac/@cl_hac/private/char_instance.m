% Transformation en chaine de caracteres d'une instance
%
% Syntax
%   str = char_instance(a)
%
% Input Arguments
%   a : instance de cl_hac
%
% Name-Value Pair Arguments
%   InfoSonar  : 1=Information de l'image uniquement
%                2=Info de l'information sonar uniquement
%                3=Display de toute l'information disponible (image+sonar) : (3 par defaut)
%
% Output Arguments
%   str : une chaine de caracteres representative de l instance
%
% Examples
%
% See also cl_hac Authors
% Authors : GLU
% ----------------------------------------------------------------------------

function str = char_instance(this, varargin)

str = [];

str{end+1} = sprintf('nomFic\t\t<-> %s', this.nomFic);
str{end+1} = sprintf('typeTuples\t<-- %s', this.typeTuples);
if ~isempty(this.nomFic)
    strHisto = histo_index(this, 'minimal', 1);
    strHisto = strsplit(strHisto, newline);
    for i=1: length(strHisto)
        str{end+1} = sprintf('\t\t\t\t\t%s', strHisto{i}); %#ok<AGROW>
    end
end

n               = get(this, 'nbSounders');
listSounders    = get(this, 'listSounders');
str{end+1}      = sprintf('Sounders\t\t<--');
strIdentLayers     = get(this, 'strIdentData');
flagIdentLayers    = get(this, 'flagIdentData');
for i=1:n
    %GLU str{end+1} = sprintf('\t\t\t\t\t%d\t''%s''\t''%s''\t''%s''', listSounders(i).id, listSounders(i).Name, listSounders(i).StrType, listSounders(i).StrMode); %#ok<AGROW>
    str{end+1} = sprintf('\t\t\t\t\t%d\t''%s', listSounders(i).id, listSounders(i).Name); %#ok<AGROW>
    str{end+1}         = sprintf('Content layers\t\t<--');
    for k=1:3
        str{end+1} = sprintf('\t\t\t\t%s\t : \t%d', strIdentLayers{k}, flagIdentLayers(i,k)); %#ok<AGROW>
    end
end


%% Concatenation en une chaine

str = cell2str(str);
