function [flag, Data, nomFicXml] = read_Channel_bin(this, varargin)

% ----------------------------------------------------------------
% Impose un beakpoint ici pour rep�rer les appels � cette fonction
my_breakpoint('FctName', 'cl_hac/read_Channel_bin');
% ----------------------------------------------------------------

% TODO : GLU : remplacer par XMLBinUtils.readGrpData ?

[varargin, Memmapfile] = getPropertyValue(varargin, 'Memmapfile',  0);
[varargin, numSounder] = getPropertyValue(varargin, 'NumSounder',  1);
[varargin, Dimensions] = getPropertyValue(varargin, 'Dimensions', []); %#ok<ASGLU>
% [varargin, TypeSounder] = getPropertyValue(varargin, 'TypeSounder', 1);

flag        = 0;
Data        = [];
nomFicXml   = [];

%% Lecture du r�pertoire SonarScope

[nomDir, nom] = fileparts(this.nomFic);

% nomDir = fullfile(nomDir, 'SonarScope', nom, ['Ssc_Sounder' num2str(numSounder)]);
nomDir = fullfile(nomDir, 'SonarScope', nom); % Modif JMA le 06/04/2012
if ~exist(nomDir, 'dir')
    return
end

%% Lecture du fichier XML d�crivant la donn�e

% TODO / GLU :
% nameListSounders    = {'EK60', 'ME70', 'HAC_Generic'};
% pppp                = nameListSounders{TypeSounder};
% nomFicXml = fullfile(nomDir, 'Ssc_Channel.xml');
% nomFicXml = fullfile(nomDir, ['Ssc_' pppp 'Channel_' num2str(numSounder) '.xml']); % Modif JMA le 06/04/2012
nomDir = fullfile(nomDir, ['Ssc_Sounder' num2str(numSounder)]);
nomFicXml = fullfile(nomDir, 'Ssc_Channel.xml');
flag = exist(nomFicXml, 'file');
if ~flag
    return
end
Data = xml_mat_read(nomFicXml);

%% Lecture des fichiers binaires des signaux

for k=1:length(Data.Signals)
    % GLUGLUGLU : cas d'erreur sur les donn�es de type Char :
    % FrequencychannelName : coder en Info.Strings les donn�es de type
    % Char (cf Info.Parameters des .ALL).
    if (~strcmp(Data.Signals(k).Storage, 'char'))
        [flag, Signal] = XMLBinUtils.readSignal(nomDir, Data.Signals(k), Data.Dimensions, ...
            'Memmapfile', Memmapfile, 'LogSilence', 1);
        if ~flag
            return
        end
        
        % Tranformation en matrice si les informations de dimensions sont bien identifi�es.
        if isempty(Dimensions)
            Data.(Data.Signals(k).Name) = Signal;
        else
            Data.(Data.Signals(k).Name) = reshape(Signal(:), Dimensions.nbFreq, Dimensions.nbPings);
        end
    end
end
