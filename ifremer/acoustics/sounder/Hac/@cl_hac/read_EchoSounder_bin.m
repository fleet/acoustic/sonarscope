% nomFic = 'D:\Temp\ExData\ExHac\PELGAS17_030_20170521_181546.hac';
% a = cl_hac('nomFic', nomFic);
%
% [flag, Data, nomFicXml] = read_EchoSounder_bin(a);
% [flag, Data] = SSc_ReadDatagrams(nomFic, '????'); % Jamais test�

function [flag, Data, nomFicXml] = read_EchoSounder_bin(this, varargin)

% ----------------------------------------------------------------
% Impose un beakpoint ici pour rep�rer les appels � cette fonction
my_breakpoint('FctName', 'cl_hac/read_EchoSounder_bin');
% ----------------------------------------------------------------

[varargin, Memmapfile]  = getPropertyValue(varargin, 'Memmapfile',  0);
[varargin, numSounder]  = getPropertyValue(varargin, 'NumSounder',  1);
[varargin, TypeSounder] = getPropertyValue(varargin, 'TypeSounder', 1); %#ok<ASGLU>

flag = 0;
Data = [];
nomFicXml = [];

%% Lecture du r�pertoire SonarScope

[nomDir, nom] = fileparts(this.nomFic);

% nomDir = fullfile(nomDir, 'SonarScope', nom, ['Ssc_Sounder' num2str(numSounder)]);
nomDir = fullfile(nomDir, 'SonarScope', nom); % Modif JMA le 06/04/2012
if ~exist(nomDir, 'dir')
    return
end

%% Lecture du fichier XML d�crivant la donn�e

% nomFicXml = fullfile(nomDir, 'Ssc_EchoSounder.xml');
% nameListSounders    = {'EK60', 'ME70', 'HAC_Generic'};
% pppp                = nameListSounders{TypeSounder};
% nomFicXml = fullfile(nomDir, ['Ssc_' pppp 'EchoSounder_' num2str(numSounder) '.xml']); % Modif JMA le 06/04/2012
nomDir = fullfile(nomDir, ['Ssc_Sounder' num2str(numSounder)]);
nomFicXml = fullfile(nomDir, 'Ssc_EchoSounder.xml');
flag = exist(nomFicXml, 'file');
if ~flag
    return
end
Data = xml_mat_read(nomFicXml);


%% Lecture des fichiers binaires des signaux

for k=1:length(Data.Signals)
    % GLUGLUGLU : cas d'erreur sur les donn�es de type Char :
    % FrequencychannelName
    if (~strcmp(Data.Signals(k).Storage, 'char'))
        [flag, Signal] = XMLBinUtils.readSignal(nomDir, Data.Signals(k), Data.Dimensions, ...
            'Memmapfile', Memmapfile, 'LogSilence', 0);
        if ~flag
            return
        end
        Data.(Data.Signals(k).Name) = Signal;
    end
end
