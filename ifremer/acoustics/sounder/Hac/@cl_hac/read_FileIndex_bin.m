% nomFic = 'D:\Temp\ExData\ExHac\PELGAS17_030_20170521_181546.hac';
% a = cl_hac('nomFic', nomFic);
%
% [flag, Data] = read_FileIndex_bin(a)
% [flag, Data] = SSc_ReadDatagrams(nomFic, 'Ssc_FileIndex') % Jamais test�

function [flag, Data] = read_FileIndex_bin(this, varargin)

% ----------------------------------------------------------------
% Impose un beakpoint ici pour rep�rer les appels � cette fonction
my_breakpoint('FctName', 'cl_hac/read_FileIndex_bin');
% ----------------------------------------------------------------

[varargin, this.nomFic] = getPropertyValue(varargin, 'nomFic',     this.nomFic);
[varargin, Memmapfile]  = getPropertyValue(varargin, 'Memmapfile', 0); %#ok<ASGLU>

flag = 0;
Data = [];

%% Lecture du r�pertoire SonarScope

[nomDir, nom] = fileparts(this.nomFic);
nomDir = fullfile(nomDir, 'SonarScope', nom);
if ~exist(nomDir, 'dir')
    return
end

%% Lecture du fichier XML d�crivant la donn�e

nomFicXml = fullfile(nomDir, 'Ssc_FileIndex.xml');
flag = exist(nomFicXml, 'file');
if ~flag
    messageErreurFichier(nomFicXml, 'ReadFailure');
    return
end
Data = xml_mat_read(nomFicXml);

%% Lecture du r�pertoire Position

nomDirPosition = fullfile(nomDir, 'Ssc_FileIndex');
if ~exist(nomDirPosition, 'dir')
    flag = 0;
    return
end

%% Lecture des fichiers binaires des signaux

for k=1:length(Data.Signals)
    [flag, Signal] = XMLBinUtils.readSignal(nomDir, Data.Signals(k), Data.Dimensions, ...
        'Memmapfile', Memmapfile);
    if ~flag
        return
    end
    Data.(Data.Signals(k).Name) = Signal;
end

