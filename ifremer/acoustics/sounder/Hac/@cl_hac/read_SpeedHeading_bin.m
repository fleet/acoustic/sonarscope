% nomFic = 'D:\Temp\ExData\ExHac\PELGAS17_030_20170521_181546.hac';
% a = cl_hac('nomFic', nomFic);
%
% [flag, Data, nomFicXml] = read_SpeedHeading_bin(a);
% [flag, Data] = SSc_ReadDatagrams(nomFic, 'Ssc_SpeedHeading'); % Jamais test�

function [flag, Data, nomFicXml] = read_SpeedHeading_bin(this, varargin)

% ----------------------------------------------------------------
% Impose un beakpoint ici pour rep�rer les appels � cette fonction
my_breakpoint('FctName', 'cl_hac/read_SpeedHeading_bin');
% ----------------------------------------------------------------

[varargin, Memmapfile] = getPropertyValue(varargin, 'Memmapfile', 0); %#ok<ASGLU>

flag      = 0;
Data      = [];
nomFicXml = [];

%% Lecture du r�pertoire SonarScope

[nomDir, nom] = fileparts(this.nomFic);

nomDir = fullfile(nomDir, 'SonarScope',nom);
if ~exist(nomDir, 'dir')
    return
end

%% Lecture du fichier XML d�crivant la donn�e

% TODO GLU : pourquoi appeler ce fichier navigation ? Il ne contient pas de coordonn�es g�ographiques !
nomFicXml = fullfile(nomDir, 'Ssc_SpeedHeading.xml');
flag = exist(nomFicXml, 'file');
if ~flag
    return
end
Data = xml_mat_read(nomFicXml);

%% Lecture du r�pertoire Position

nomDirPosition = fullfile(nomDir, 'Ssc_Position');
if ~exist(nomDirPosition, 'dir')
    flag = 0;
    return
end

%% Lecture des fichiers binaires des signaux

for k=1:length(Data.Signals)
    [flag, Signal] = XMLBinUtils.readSignal(nomDir, Data.Signals(k), Data.Dimensions, ...
        'Memmapfile', Memmapfile);
    if ~flag
        return
    end
    Data.(Data.Signals(k).Name) = Signal;
end
