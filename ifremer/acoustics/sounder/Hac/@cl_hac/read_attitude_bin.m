% [flag, dataFileName] = FtpUtils.getSScDemoFile('Mobydick_025_20180315_002209.hac', 'OutputDir', 'D:\Temp\ExData')
% a = cl_hac('nomFic', dataFileName)
%
% [flag, Data1, nomFicXml] = read_attitude_bin(this, 'Memmapfile', 1)
% [flag, Data2] = SSc_ReadDatagrams(dataFileName, ['Ssc_Sounder' num2str(numSounder)], 'Memmapfile', -1);

function [flag, Data, nomFicXml] = read_attitude_bin(this, varargin)

% ----------------------------------------------------------------
% Impose un beakpoint ici pour rep�rer les appels � cette fonction
my_breakpoint('FctName', 'cl_hac/read_attitude_bin');
% ----------------------------------------------------------------

[varargin, Memmapfile] = getPropertyValue(varargin, 'Memmapfile',  0);
[varargin, numSounder] = getPropertyValue(varargin, 'NumSounder', []); %#ok<ASGLU>

flag = 0;
Data = [];
nomFicXml = [];

%% Lecture du r�pertoire SonarScope

[nomDir, nom] = fileparts(this.nomFic);

nomDirPosition = fullfile(nomDir, 'SonarScope', nom, 'Ssc_Position');
% nomDir          = fullfile(nomDir, 'SonarScope', nom, ['Ssc_Sounder' num2str(numSounder)]);
nomDir          = fullfile(nomDir, 'SonarScope', nom); % Modif JMA le 06/04/2012
if ~exist(nomDir, 'dir')
    return
end

%% Lecture du fichier XML d�crivant la donn�e

% nomFicXml = fullfile(nomDir, 'Ssc_AttitudeSensor.xml');
% nomFicXml = fullfile(nomDir, ['Ssc_AttitudeSensor_' num2str(numSounder) '.xml']); % Modif JMA le 06/04/2012
nomDir = fullfile(nomDir, ['Ssc_Sounder' num2str(numSounder)]);
nomFicXml = fullfile(nomDir, 'Ssc_AttitudeSensor.xml');
flag = exist(nomFicXml, 'file');
if ~flag
    return
end
Data = xml_mat_read(nomFicXml);

%% Lecture du r�pertoire Position

%nomDirPosition = fullfile(nomDir, 'Ssc_Position');
if ~exist(nomDirPosition, 'dir')
    flag = 0;
    return
end

%% Lecture des fichiers binaires des signaux

for i=1:length(Data.Signals)
    [flag, Signal] = XMLBinUtils.readSignal(nomDir, Data.Signals(i), Data.Dimensions, ...
        'Memmapfile', Memmapfile);
    if ~flag
        return
    end
    Data.(Data.Signals(i).Name) = Signal;
end
% Data = rmfield(Data, {'Signals'; 'Comments'});


