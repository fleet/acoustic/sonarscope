% TODO : Data.Dimensions.nbBeams --> Data.Dimensions.nbFreq si sondedur monofaisceau ?
%
% Constructeur de cl_hac (Imagerie contenue dans une .hac)
%
% Syntax
%   [f, D] = read_data(a, ...)
%
% Name-Value Pair Arguments
%   nomFic : Nom(s) du fichier .hac
%
% Output Arguments
%   flag : indicateur de traitement
%   Data : donn�es
%
% Examples
%   nomFic = getNomFicDatabase('PELGAS10_003_20100428_183857.hac')
%   [flag, Data] = read_data('NomFic', nomFic, 'Memmapfile', 1,
%   'NumSounder', 2);
%
% See also CONVERT_HAC.exe Authors
% Authors : GLU
% ----------------------------------------------------------------------------

function [flag, Data] = read_data(this, varargin)

% ----------------------------------------------------------------
% Impose un beakpoint ici pour rep�rer les appels � cette fonction
my_breakpoint('FctName', 'cl_hac/read_data');
% ----------------------------------------------------------------

[varargin, Memmapfile] = getPropertyValue(varargin, 'Memmapfile', 0);
[varargin, numSounder] = getPropertyValue(varargin, 'NumSounder', []); %#ok<ASGLU>

Data = [];

%% Check the number of elements

flag = checkIfOneElement(length(this));
if ~flag
    return
end

%% Lecture du r�pertoire SonarScope

[nomDir, nom] = fileparts(this.nomFic);
% nomDir          = fullfile(nomDir, 'SonarScope', nom, ['Ssc_Sounder' num2str(numSounder)]);
nomDir = fullfile(nomDir, 'SonarScope', nom); % Modif JMA le 06/04/2012
if ~exist(nomDir, 'dir')
    flag = hac2ssc(this.nomFic);
    %     return % Modif JMA le 06/04/2012
end

%% Lecture du fichier XML d�crivant la donn�e

% nomFicXml = fullfile(nomDir, 'Ssc_PingC1632Angle.xml');
% nomFicXml = fullfile(nomDir, ['Ssc_PingC16Amplitude_' num2str(numSounder) '.xml']); % Modif JMA le 06/04/2012
nomDir          = fullfile(nomDir, ['Ssc_Sounder' num2str(numSounder)]);
nomFicXmlC16Amp = fullfile(nomDir, 'Ssc_PingC16Amplitude.xml');
nomFicXmlU16Amp = fullfile(nomDir, 'Ssc_PingU16Amplitude.xml');
flag = exist(nomFicXmlC16Amp, 'file');
if ~flag
    if ~exist(nomFicXmlU16Amp, 'file')
        str1 = 'Pas d''information d''amplitude : v�rifier la pr�sence de tuples 10030/10040 dans le fichier d''origine';
        str2 = 'No amplitude information : control the Tuples 10030/10040 in original file';
        my_warndlg(Lang(str1,str2), 1);
        return
    else
        nomFicXml = nomFicXmlU16Amp;
    end
else
    nomFicXml = nomFicXmlC16Amp;
end
% edit(nomFicXml)
Data = xml_mat_read(nomFicXml);
% gen_object_display(Data)


%% Lecture des fichiers binaires des signaux

for k=1:length(Data.Signals)
    [flag, Signal] = XMLBinUtils.readSignal(nomDir, Data.Signals(k), Data.Dimensions, 'Memmapfile', Memmapfile);
    if ~flag
        return
    end
    Data.(Data.Signals(k).Name) = Signal;
end

%% Lecture des fichiers binaires des images

for k=1:length(Data.Images)
    [flag, Image] = XMLBinUtils.readSignal(nomDir, Data.Images(k), Data.Dimensions, 'Memmapfile', Memmapfile);
    if ~flag
        return
    end
    Data.(Data.Images(k).Name) = Image;
end

% Data

%{
Data =
Title: 'PingC16Amplitude'
Constructor: 'Kongsberg'
TimeOrigin: '01/01/-4713'
Comments: 'PingC16Amplitude Tuple'
FormatVersion: 20110531
Dimensions: [1x1 struct]
Signals: [1x8 struct]
Images: [1x1 struct]
Time: [1x1 cl_time]
SoftwareChannelIdentifier: [13944x1 cl_memmapfile]
TransceiverMode: [13944x1 cl_memmapfile]
PingNumber: [664x1 cl_memmapfile]
DetectedBottomRange: [13944x1 cl_memmapfile]
NbSamples: [664x21 cl_memmapfile]
IndexTupleRoot: [13944x1 cl_memmapfile]
IndexTupleParent: [13944x1 cl_memmapfile]
Samples: [664x21x2388 cl_memmapfile]
%}

%{
for p=1:size(Data.PingNumber,1)
% Affichage de la donn�e du Ping.
pppp = squeeze(Data.Samples(p, :,:))*0.01;
figure(11111); imagesc(pppp'); title(['UnCompressSamples - Ping' num2str(p)]); axis xy; colorbar;
end

for p=1:size(Data.Samples,2)
% Affichage de la donn�e du Ping.
pppp = squeeze(Data.Samples(:,p,:))*0.01;
figure(2222+p); imagesc(pppp'); title(['UnCompressSamples - Freq' num2str(p)]); axis xy; colorbar;
end
%}
