% nomFic = 'D:\Temp\ExData\ExHac\PELGAS17_030_20170521_181546.hac';
% a = cl_hac('nomFic', nomFic);
%
% [flag, Data] = read_phase(a);
% [flag, Data] = SSc_ReadDatagrams(nomFic, '????'); % Jamais test�

% TODO : Data.Dimensions.nbBeams --> Data.Dimensions.nbFreq si sondedur monofaisceau ?

function [flag, Data] = read_phase(this, varargin)

% ----------------------------------------------------------------
% Impose un beakpoint ici pour rep�rer les appels � cette fonction
my_breakpoint('FctName', 'cl_hac/read_phase');
% ----------------------------------------------------------------

[varargin, Memmapfile] = getPropertyValue(varargin, 'Memmapfile',  0);
[varargin, numSounder] = getPropertyValue(varargin, 'NumSounder', []); %#ok<ASGLU>

Data = [];

%% Check the number of elements

flag = checkIfOneElement(length(this));
if ~flag
    return
end

%% Lecture du r�pertoire SonarScope

[nomDir, nom] = fileparts(this.nomFic);
nomDir        = fullfile(nomDir, 'SonarScope', nom); % Modif JMA le 06/04/2012
if ~exist(nomDir, 'dir')
    flag = hac2ssc(this.nomFic);
    %     return % Modif JMA le 06/04/2012
end

%% Lecture du fichier XML d�crivant la donn�e

nomDir              = fullfile(nomDir, ['Ssc_Sounder' num2str(numSounder)]);
nomFicXmlC1632Angle = fullfile(nomDir, 'Ssc_PingC1632Angle.xml');
nomFicXmlU16Angle   = fullfile(nomDir, 'Ssc_PingU16Angle.xml');
if ~exist(nomFicXmlC1632Angle, 'file')
    if ~exist(nomFicXmlU16Angle, 'file')
        str1 = 'Pas de donn�es de phase : v�rifier la pr�sence de tuples 10011/10031 dans le fichier d''origine';
        str2 = 'No angle data : control the Tuples 10011/10031 in original file';
        my_warndlg(Lang(str1,str2), 1);
        return
    else
        nomFicXml = nomFicXmlU16Angle;
    end
else
    nomFicXml = nomFicXmlC1632Angle;
end
Data = xml_mat_read(nomFicXml);
% gen_object_display(Data)

%% Lecture des fichiers binaires des signaux

for k=1:length(Data.Signals)
    [flag, Signal] = XMLBinUtils.readSignal(nomDir, Data.Signals(k), Data.Dimensions, 'Memmapfile', Memmapfile);
    if ~flag
        return
    end
    Data.(Data.Signals(k).Name) = Signal;
end

%% Lecture des fichiers binaires des images

for k=1:length(Data.Images)
    [flag, Image] = XMLBinUtils.readSignal(nomDir, Data.Images(k), Data.Dimensions, 'Memmapfile', Memmapfile);
    if ~flag
        return
    end
    Data.(Data.Images(k).Name) = Image;
end

% Data

%{
Data =
Title: 'PingC16Amplitude'
Constructor: 'Kongsberg'
TimeOrigin: '01/01/-4713'
Comments: 'PingC16Amplitude Tuple'
FormatVersion: 20110531
Dimensions: [1x1 struct]
Signals: [1x8 struct]
Images: [1x1 struct]
Time: [1x1 cl_time]
SoftwareChannelIdentifier: [13944x1 cl_memmapfile]
TransceiverMode: [13944x1 cl_memmapfile]
PingNumber: [664x1 cl_memmapfile]
DetectedBottomRange: [13944x1 cl_memmapfile]
NbSamples: [664x21 cl_memmapfile]
IndexTupleRoot: [13944x1 cl_memmapfile]
IndexTupleParent: [13944x1 cl_memmapfile]
Samples: [664x21x2388 cl_memmapfile]
%}

%{
for p=1:size(Data.PingNumber,1)
% Affichage de la donn�e du Ping.
pppp = squeeze(Data.Samples(p, :,:))*0.01;
figure(11111); imagesc(pppp'); title(['UnCompressSamples - Ping' num2str(p)]); axis xy; colorbar;
end

for p=1:size(Data.Samples,2)
% Affichage de la donn�e du Ping.
pppp = squeeze(Data.Samples(:,p,:))*0.01;
figure(2222+p); imagesc(pppp'); title(['UnCompressSamples - Freq' num2str(p)]); axis xy; colorbar;
end
%}
