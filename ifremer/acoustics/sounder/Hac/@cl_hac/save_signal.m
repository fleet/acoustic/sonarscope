function flag = save_signal(this, numSounder, numChannel, NomLayer, X)

% ----------------------------------------------------------------
% Impose un beakpoint ici pour rep�rer les appels � cette fonction
my_breakpoint('FctName', 'cl_hac/save_signal');
% ----------------------------------------------------------------

% TODO : tester appel � fonction save_signal flag =
% save_signal(this.nomFic, NomLayer, X, )

%% Lecture du r�pertoire SonarScope

[nomDir, nom] = fileparts(this.nomFic);
nomDir        = fullfile(nomDir, 'SonarScope', nom);
if ~exist(nomDir, 'dir')
    return
end

%% Lecture du fichier XML d�crivant la donn�e

nomFicXml = fullfile(nomDir, ['Ssc_PingC16Amplitude_' num2str(numSounder) '.xml']);
flag = exist(nomFicXml, 'file');
if ~flag
    return
end
% edit(nomFicXml)
XML = xml_mat_read(nomFicXml);


k = find(strcmp({XML.Signals.Name}, NomLayer));
if isempty(k)
    str1 = sprintf('Signal "%s" non trouv� dans "%s', NomLayer, nomFicXml);
    str2 = 'TODO';
    my_warndlg(Lang(str1,str2), 1);
    return
end

[flag, Signal] = XMLBinUtils.readSignal(nomDir, XML.Signals(k), XML.Dimensions, 'Memmapfile', 0);
if ~flag
    return
end

Signal(:,numChannel) = X;

flag = writeSignal(nomDir, XML.Signals(k), Signal);
