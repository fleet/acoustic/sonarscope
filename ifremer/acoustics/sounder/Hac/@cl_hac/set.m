% Acces en ecriture des proprietes d'une instance
%
% Syntax
%   b = set(a, ...)
%
% Input Arguments
%   a : Une instance de la classe cl_hac
%
% Name-Value Pair Arguments
%   nomFic : Nom(s) du fichier .all
%
% Output Arguments
%   [] : Sauve l'instance dans a
%   b  : Instance de la classe
%
% Examples
%   nomFic = getNomFicDatabase('PELGAS10_003_20100428_183857.hac')
%   a = cl_hac
%   a = set(a, 'nomFic', nomFic)
%
%
% See also cl_hac cl_hac/get Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function varargout = set(this, varargin)

[varargin, nomFic] = getPropertyValue(varargin, 'nomFic', []); %#ok<ASGLU>

if ~isempty(nomFic)
    nomFic = windows_replaceVolumeLetter(nomFic);
    this.nomFic = nomFic;
    [flag, listSounders] = hac2ssc(nomFic, 'Display', 0);
    this.nomFic         = nomFic;
    this.listSounders   = listSounders;
    [flag, flagIdentLayers] = get_flagIdentLayers(this, 'NbSounder',numel(listSounders));
    this.flagIdentData  = flagIdentLayers;
    if ~flag
        varargout{1} = [];
        return
    end
end

%% Sortie

if nargout == 0
    assignin('caller', inputname(1), this);
else
    varargout{1} = this;
end
