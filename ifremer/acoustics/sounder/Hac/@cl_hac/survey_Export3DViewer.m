function flag = survey_Export3DViewer(this, listeFicHac, nomDirOut, CLim, CLimNaN, ColormapIndex, Tag, ...
    HauteurMinInMeters, SuppressionOverHeight, identLayer, TailleMax, MeanTide, varargin)

[varargin, Coef] = getPropertyValue(varargin, 'Zoom', 10); %#ok<ASGLU>

Carto        = [];
idSounder    = [];
typeGeometry = [];

N = length(listeFicHac);
str1 = 'Export des fichiers HAC dans GLOBE';
str2 = 'Exporting HAC files to GLOBE';
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    [flag, Carto, idSounder, typeGeometry] = survey_Export3DViewer_unitaire(this, listeFicHac{k}, nomDirOut, CLim, CLimNaN, ColormapIndex, Tag, Carto, ...
        HauteurMinInMeters, SuppressionOverHeight, identLayer, TailleMax, MeanTide, idSounder, typeGeometry, Coef);
end
my_close(hw, 'MsgEnd')



function [flag, Carto, idSounder, typeGeometry] = survey_Export3DViewer_unitaire(this, nomFicHac, nomDirOut, CLim, CLimNaN, ColormapIndex, Tag, Carto, ...
    HauteurMinInMeters, SuppressionOverHeight, identLayer, TailleMax, MeanTide, idSounder, typeGeometry, Coef)


%% Instance cl_hac

a = cl_hac('nomFic', nomFicHac);

%% Type de soundeur

[flag, listSounders] = get_listSounders(a);
if ~flag
    return
end
[flag, listDirection] = get_listDirection(a);
if ~flag
    return
end
[flag, typeSounders] = get_typeSounders(a);
if ~flag
    return
end

if isempty(idSounder)
    if length(listSounders) > 1
        str1 = 'S�lectionnez le/les sondeurs � importer';
        str2 = 'Please, select sounders to import';
        pppp = {};
        for k=1:numel(listSounders)
            pppp{k} = sprintf('%s | %s', listSounders(k).Name, listDirection{k}); %#ok<AGROW>
        end
        [idSounder, flag] = my_listdlg(Lang(str1,str2), pppp, 'SelectionMode', 'Single');
        if strcmp(listDirection(idSounder), 'Horizontal')
            str1 = 'Traitement Mono-faisceau : g�om�trie non exploit�e pour l''instant';
            str2 = 'Simple Beam processing : geometry not yet implemented';
            my_warndlg(Lang(str1,str2), 1);
            flag = 0;
            return
        end
        
        if ~flag
            return
        end
    else
        idSounder = 1;
    end
end
TypeSounder = typeSounders(idSounder);


%% D�termination de la g�om�trie de sortie

strTypeGeometry = {'PingSamples'; 'SampleBeam'};
if TypeSounder == 2 % Cas du ME70
    if isempty(typeGeometry)
        str1 = 'Visualisation sous forme de : ';
        str2 = 'Display as :';
        [typeGeometry, flag] = my_listdlg(Lang(str1,str2), strTypeGeometry, 'SelectionMode', 'Single');
        if ~flag
            return
        end
    end
else
    typeGeometry = 1; % 'PingSamples'
end

%% Cr�ation du r�pertoire

[~, nomDirBin] = fileparts(nomFicHac);
nomDirXml = fullfile(nomDirOut, [nomDirBin '_' this.strIdentData{identLayer} '_' listSounders(TypeSounder).Name '_' strTypeGeometry{typeGeometry}]);
if ~isempty(Tag)
    nomDirXml = [nomDirXml '_' Tag];
end
[~, NomLayer] = fileparts(nomDirXml);

% nomDirRoot = fileparts(nomDirXml);
if ~exist(nomDirXml, 'dir')
    flag = mkdir(nomDirXml);
    if ~flag
        messageErreurFichier(nomDirXml, 'WriteFailure');
        return
    end
end

%% Lecture de l''image

if typeGeometry == 1
    % Exploitation en g�om�trie PingSamples
    [flag, b, Carto] = import_hac_profil(a, idSounder, Carto, 'TypeSounder', TypeSounder, 'listeLayers', identLayer, ...
        'Memmapfile', 1);
    if ~flag
        return
    end
    
    %     LayerName = a.strIdentData{identLayer};
    flag = sonar_HACWriteWC_profil(b, nomFicHac, nomDirXml, CLim, CLimNaN, ColormapIndex, ...
        HauteurMinInMeters, SuppressionOverHeight, TailleMax, MeanTide);
    if ~flag
        return
    end
    
else
    %% TODO : cr�er une fonction sonar_HACWriteWC_Echograms
    
    Data             = [];
    DataPosition     = [];
    DataSpeedHeading = [];
    DataSPD          = [];
    DataEchoSounder  = [];
    SonarDescription = [];
    
    [flag, pppp] = read_data(a, 'Memmapfile', 1, 'NumSounder', idSounder); % , 'NumSounder', []);
    if ~flag
        return
    end
    nbPings = pppp.Dimensions.nbPings;
    clear pppp
    
    PingNumbers = 1:nbPings;
    nbBeams = 21; % TODO : R�cup�rer la valeur dansw cl_hac
    
    DataWCRaw.from            = 'Unknown';
    DataWCRaw.name            = NomLayer;
    DataWCRaw.PingNumber      = PingNumbers;
    DataWCRaw.Date            = NaN(nbPings,nbBeams);
    DataWCRaw.Hour            = NaN(nbPings,nbBeams);
    DataWCRaw.LatitudeTop     = NaN(nbPings,nbBeams);
    DataWCRaw.LongitudeTop    = NaN(nbPings,nbBeams);
    DataWCRaw.LatitudeBottom  = NaN(nbPings,nbBeams);
    DataWCRaw.LongitudeBottom = NaN(nbPings,nbBeams);
    DataWCRaw.DepthTop        = NaN(nbPings,nbBeams);
    DataWCRaw.DepthBottom     = NaN(nbPings,nbBeams);
    DataWCRaw.ImageSegment    = NaN(nbPings,nbBeams);
    date  = NaN(nbPings, 1);
    heure = NaN(nbPings, 1);
    
    NomFicXML = [nomDirXml '.xml'];
    
    str1 = 'Traitement des pings Water Column du ME70';
    str2 = 'EM70 Water Column processing';
    hw = create_waitbar(Lang(str1,str2), 'N', nbPings);
    for k=1:length(PingNumbers)
        my_waitbar(k, nbPings, hw)
        
        [flag, b, Carto, Data, DataPosition, DataSpeedHeading, DataSPD, DataEchoSounder, SonarDescription] = import_hac_ping(a, idSounder, Carto, 'TypeSounder', TypeSounder, ...
            'listeLayers', identLayer, 'PingNumbers', PingNumbers(k), ...
            'Data', Data, 'DataPosition', DataPosition, 'DataSpeedHeading', DataSpeedHeading, ...
            'DataSPD', DataSPD, 'DataEchoSounder', DataEchoSounder, 'SonarDescription', SonarDescription, 'Memmapfile', 0);
        if ~flag
            return
        end
        
        %         ProfilCelerite      = get(b, 'SonarBathyCel_C');
        %         TxAcrossAngle       = get(b, 'TxAcrossAngle');
        %         Depth               = get(b, 'Height');
        SampleBeamData = get(b, 'SampleBeamData');
        if isempty(SampleBeamData)
            flag = 0;
            return
        end
        DetectedRangeInSamples = SampleBeamData.R1SamplesFiltre;
        %         TxAcrossAngle = TxAcrossAngle(:);
        
        %         [flag, c, cMasqued, Detection, Mask, WCSlide] = sonar_polarEchogram(b,  ProfilCelerite, ...
        %             'otherImage', b, ...
        %             'RangeDetectionInSamples', DetectedRangeInSamples, ...
        %             'across', TxAcrossAngle, 'depth', Depth, 'MaxImageSize', 800);
        %
        %         imagesc(cMasqued(2))
        
        
        subNaN = find(DetectedRangeInSamples < 5);
        if ~isempty(subNaN)
            subNonNaN = find(DetectedRangeInSamples >= 5);
            DetectedRangeInSamples(subNaN) = floor(interp1(subNonNaN, DetectedRangeInSamples(subNonNaN), subNaN, 'linear', 'extrap'));
        end
        
        if SuppressionOverHeight
            % On fait la compensation avant d'avoir fait le masquage sinon
            % l'op�ration est moins bonne
            [flag, X] = MaskVert(b, 'kY', DetectedRangeInSamples+1);
            if flag
                b = X;
            end
        end
        
%         Roll = 0;
        [flag, lat0, lon0, LatitudeBottom, LongitudeBottom, DepthBottom] = calcul_positionRawDataWC(b);
        if ~flag
            return
        end
        
        TransducerDepth = SampleBeamData.TransducerDepth;
        TransmitSectorNumber = ones(1,nbBeams);
        
        DataWCRaw.LatitudeTop(k,:)     = lat0 + zeros(1, nbBeams);
        DataWCRaw.LongitudeTop(k,:)    = lon0 + zeros(1, nbBeams);
        DataWCRaw.LatitudeBottom(k,:)  = LatitudeBottom;
        DataWCRaw.LongitudeBottom(k,:) = LongitudeBottom;
        DataWCRaw.DepthTop(k,:)        = -TransducerDepth + zeros(1, nbBeams);
        DataWCRaw.DepthBottom(k,:)     = -TransducerDepth + DepthBottom;
        DataWCRaw.ImageSegment(k,:)    = TransmitSectorNumber;
        DataWCRaw.EndData(k,:)         = DetectedRangeInSamples;
        
        date(k)  = SampleBeamData.Time.date;
        heure(k) = SampleBeamData.Time.heure;
        
        subNaN = findNaN(b);
        
        
        %% Transformation de l'image en indexe
        
        [bInd, flag] = Intensity2Indexed(b, 'CLim', CLim);
        if ~flag
            return
        end
        map = b.Colormap;
        
        nbRows = bInd.nbRows;
        %     nbColumns = bInd.nbColumns;
        x = get(bInd, 'x');
        
        %         kRep = floor((k-1)/100);
        kRep = floor(k/100);
        
        [nomDir, nomFic2] = fileparts(NomFicXML);
        nomDir = fullfile(nomDir, nomFic2);
        if ~exist(nomDir, 'dir')
            flag = mkdir(nomDir);
            if ~flag
                messageErreurFichier(nomDirPng, 'WriteFailure');
                return
            end
        end
        nomDirPngRaw = fullfile(nomDir, num2str(kRep, '%03d'));
        if ~exist(nomDirPngRaw, 'dir')
            flag = mkdir(nomDirPngRaw);
            if ~flag
                messageErreurFichier(nomDirPngRaw, 'WriteFailure');
                return
            end
        end
        
        
        %% Cr�ation de l'image
        
        I = get(bInd, 'Image');
        I(I == 0) = 1;
        % I(I<5) = 5;
        I(subNaN) = 0;
        
        sumX = sum(I == 0);
        subX = find(sumX ~= nbRows);
        if isempty(subX)
            %% TODO message d'erreur
            flag = 0;
            return
        end
        x = x(subX);
        nbColumns = length(x);
        
        
        %         nomFicPng = [num2str(k-1, '%05d') '.png'];
        nomFicPng = [num2str(k, '%05d') '.png'];
        nomFicPng = fullfile(nomDirPngRaw, nomFicPng);
        DataWCRaw.SliceName{k}  = nomFicPng;
        %         nomFicBin = [num2str(k-1, '%05d') '.bin'];
        nomFicBin = [num2str(k, '%05d') '.bin'];
        nomFicBin = fullfile(nomDirPngRaw, nomFicBin);
        
        FormatImage = 1;
        if any(FormatImage == 2)
            % A mettre � jour si Coeff �= 1
            Coef = 1;
            export_bin(nomFicBin, b.Image)
            DataWCRaw.SliceName{k} = nomFicBin;
        end
        
        if any(FormatImage == 1)
            I = I(:,subX);
            sz = size(I);
            I = imresize(I, [sz(1) sz(2)*Coef]);
            imwrite(I, map, nomFicPng, 'Transparency', 0)
            DataWCRaw.SliceName{k} = nomFicPng;
        end
        
    end
    my_close(hw, 'MsgEnd');
    
    DataWCRaw.Date  = date;
    DataWCRaw.Hour  = heure;
    DataWCRaw.Tide  = zeros(nbPings,1);
    DataWCRaw.Heave = zeros(nbPings,1);
    
    DataWCRaw = truandageResolution(DataWCRaw, Coef);
    nbColumns = nbColumns * Coef;
    flag = write_XML_ImagesAlongNavigation(NomFicXML, DataWCRaw, nbColumns, nbRows);
    
    %{
FigUtils.createSScFigure; PlotUtils.createSScPlot(DataWCRaw.LongitudeTop, DataWCRaw.LatitudeTop, '*k'); grid on;
hold on; PlotUtils.createSScPlot(DataWCRaw.LongitudeBottom, DataWCRaw.LatitudeBottom, '*r');
    %}
end

str1 = sprintf('Le traitement du fichier "%s" est termin�.', nomFicHac);
str2 = sprintf('Processing of "%s" is over.', nomFicHac);
my_warndlg(Lang(str1,str2), 0, 'Tag', 'ProcessingRawDataWCIsOver', 'TimeDelay', 600);


function DataWCRaw = truandageResolution(DataWCRaw, Coef)

[nbPings, nbBeams] = size(DataWCRaw.LatitudeTop);
nbBeamsNew = nbBeams * Coef;
sub = 1:nbBeamsNew;
for k=1:nbPings
    DataWCRaw.LatitudeTop(k,sub)     = imresize(DataWCRaw.LatitudeTop(      k,1:nbBeams), [1 nbBeamsNew]);
    DataWCRaw.LongitudeTop(k,sub)    = imresize(DataWCRaw.LongitudeTop(     k,1:nbBeams), [1 nbBeamsNew]);
    DataWCRaw.LatitudeBottom(k,sub)  = imresize(DataWCRaw.LatitudeBottom(   k,1:nbBeams), [1 nbBeamsNew]);
    DataWCRaw.LongitudeBottom(k,sub) = imresize(DataWCRaw.LongitudeBottom(  k,1:nbBeams), [1 nbBeamsNew]);
    DataWCRaw.DepthTop(k,sub)        = imresize(DataWCRaw.DepthTop(         k,1:nbBeams), [1 nbBeamsNew]);
    DataWCRaw.DepthBottom(k,sub)     = imresize(DataWCRaw.DepthBottom(      k,1:nbBeams), [1 nbBeamsNew]);
    DataWCRaw.ImageSegment(k,sub)    = imresize(DataWCRaw.ImageSegment(     k,1:nbBeams), [1 nbBeamsNew]);
    DataWCRaw.EndData(k,sub)         = imresize(DataWCRaw.EndData(          k,1:nbBeams), [1 nbBeamsNew]);
end
DataWCRaw.ImageSegment = round(DataWCRaw.ImageSegment);
DataWCRaw.EndData      = round(DataWCRaw.EndData);
