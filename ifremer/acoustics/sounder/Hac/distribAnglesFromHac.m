function [MaxNbSamples, distribChannelByPing, Angles] = distribAnglesFromHac(Type, Mode, MaxNbChannels, TimeTuple,  ...
                                                          TmpTimeTuple, NbSamples, AnglesAlong, AnglesAthwart, ...
                                                          SWChannelId)
    
% Calcul du nombre max d'�chantillons par Ping/Beam.
MaxNbSamples    = max(max(NbSamples));
NbPings         = size(TimeTuple,1);

% R�partition du nb d'�chantillons par Ping/Beam.
OffsetPing      = 0;
% Mise en forme matricielle de la donn�e du Ping.
if Type == 1 && Mode == 2 % MonoBeam et MultiFreq
    Angles.Along    = NaN(NbPings, MaxNbSamples, MaxNbChannels);
    Angles.Athwart  = NaN(NbPings, MaxNbSamples, MaxNbChannels);
elseif Type == 2 && Mode == 1 % MultiBeam et MonoFreq 
    Angles.Along    = NaN(MaxNbSamples, MaxNbChannels, NbPings);
    Angles.Athwart  = NaN(MaxNbSamples, MaxNbChannels, NbPings);
end
distribChannelByPing = NaN(NbPings, MaxNbChannels);
% TmpPingNumber permettrait de g�rer les absences de Ping pour certaines
% fr�quences/channels.
for p=1:size(TimeTuple,1)
    sub = find(TmpTimeTuple == TimeTuple(p));
    for c=1:MaxNbChannels
        if c <= length(sub)
            distribChannelByPing(p,c)   = SWChannelId(sub(c));
            % Cast en uint64 compte tenu de l'ordre de grandeur des valeurs
            % trait�es (peut y avoir bcp d'�chantillons : cas de PELGAS13 -
            % ME70).
            nbSamples                           = uint64(NbSamples(sub(c)));
            if Type == 1 && Mode == 2 % MonoBeam et MultiFreq
                Angles.Along(p, 1:nbSamples, c)     = AnglesAlong((OffsetPing + 1):(OffsetPing+nbSamples));
                Angles.Athwart(p, 1:nbSamples, c)   = AnglesAthwart((OffsetPing + 1):(OffsetPing+nbSamples));
            else
                Angles.Along(1:nbSamples, c, p)     = AnglesAlong((OffsetPing + 1):(OffsetPing+nbSamples));
                Angles.Athwart(1:nbSamples, c, p)   = AnglesAthwart((OffsetPing + 1):(OffsetPing+nbSamples));
            end
            OffsetPing = OffsetPing + nbSamples;
        end
    end
end



