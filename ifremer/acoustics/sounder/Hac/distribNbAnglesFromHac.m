function [MaxNbSamples, nbSamples, distribChannelByPing] = distribNbAnglesFromHac(MaxNbChannels, ...
                TimeTuple, TmpTimeTuple, NbSamplesCompress, SoftwareChannelIdentifier)
    
% Répartition du nb d'échantillons par Ping/Beam.
nbSamples               = NaN(size(TimeTuple,1), MaxNbChannels);
distribChannelByPing    = NaN(size(TimeTuple,1), MaxNbChannels);
for p=1:size(TimeTuple,1)
    sub                 = (TmpTimeTuple == TimeTuple(p));
    listChannel                         = SoftwareChannelIdentifier(sub) - min(SoftwareChannelIdentifier(sub)) + 1;
    nbSamples(p,listChannel)            = NbSamplesCompress(sub);
    distribChannelByPing(p,listChannel) = SoftwareChannelIdentifier(sub);
end

% Calcul du nombre max d'échantillons par Ping/Beam.
MaxNbSamples = max(max(nbSamples));