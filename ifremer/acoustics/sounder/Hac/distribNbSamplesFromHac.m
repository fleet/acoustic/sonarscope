function [maxNbS, nbS, distribChannelByPing] = distribNbSamplesFromHac(MaxNbChannels, ...
                TimeTuple, TmpTimeTuple, NbSamplesCompress, X, ...
                SoftwareChannelIdentifier)
    
% Répartition du nb d'échantillons par Ping/Beam.
OffsetPing        = 0;
nbS               = NaN(size(TimeTuple,1), MaxNbChannels);
distribChannelByPing    = NaN(size(TimeTuple,1), MaxNbChannels);
for p=1:size(TimeTuple,1)
    sub = find(TmpTimeTuple == TimeTuple(p));
    % Calcul de l'Offset de l'échantillon correspondant au Ping en cours de
    % traitement.
    sumSubNbSamples = sum(NbSamplesCompress(sub), 'double');
    try
        XSamples        = X((OffsetPing + 1):(OffsetPing+sumSubNbSamples));
    catch ME %#ok<NASGU>
        % Correction d'un pb sur les derniers Pings pour le fichier PELGAS11_002_20110427_072328.HAC
        sumSubNbSamples = size(X,1)- OffsetPing;
        XSamples        = X((OffsetPing + 1):(OffsetPing+sumSubNbSamples)) ;
    end
    OffsetPing = OffsetPing + sumSubNbSamples;
    
    
    % Décompression pour comptage des échantillons du paquet d'échantillons sélectionnés.
    [flag, ucNbSamples] = unCompressNbSamples(NbSamplesCompress(sub), XSamples); %#ok<ASGLU>
    
    
    % Traitement des Ping incomplet qui n'ont pas le nb de voies attendues 
    % (cas du : PELGAS11_011_20110506_105740.hac, Sounder 3 = ME 70, Ping 5379); 
    % On recale l'échelle pour détecter les channels manquants
    listChannel  = SoftwareChannelIdentifier(sub) - min(SoftwareChannelIdentifier(sub)) + 1;
    distribChannelByPing(p,listChannel) = SoftwareChannelIdentifier(sub);
    if length(sub) < MaxNbChannels
        % [C, ia]         = setdiff(1:MaxNbChannels, listChannel);
        newUcNbSamples  = NaN(MaxNbChannels, 1);
        newUcNbSamples(listChannel) = ucNbSamples;
        % On remplace la ventilation des échantillons par voie par le
        % nouveau calcul prenant en compte les absences de certaines.
        ucNbSamples     = newUcNbSamples;
    end
    nbS(p,:)  = ucNbSamples;
end

% Calcul du nombre max d'échantillons par Ping/Beam.
maxNbS = max(max(nbS));
