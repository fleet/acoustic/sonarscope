function [MaxNbSamples, distribChannelByPing, Samples] = distribSamplesFromHac(Type, Mode, ...
                                                    MaxNbChannels, TimeTuple, TmpTimeTuple, ...
                                                    NbSamples, X, ...
                                                    SWChannelId)
    
% Calcul du nombre max d'�chantillons par Ping/Beam.
MaxNbSamples    = max(max(NbSamples));
NbPings         = size(TimeTuple,1);

% R�partition du nb d'�chantillons par Ping/Beam.
OffsetPing              = 0;
% Mise en forme matricielle de la donn�e du Ping.
if Type == 1 && Mode == 2 % MonoBeam et MultiFreq
    Samples         = NaN(NbPings, MaxNbSamples, MaxNbChannels);
elseif Type == 2 && Mode == 1 % MultiBeam et MonoFreq 
    Samples         = NaN(MaxNbSamples, MaxNbChannels, NbPings);
end
distribChannelByPing    = NaN(NbPings, MaxNbChannels);
% TmpPingNumber permettrait de g�rer les absences de Ping pour certaines
% fr�quences/channels.
for p=1:size(TimeTuple,1)
    sub = find(TmpTimeTuple == TimeTuple(p));
    for c=1:MaxNbChannels
        if c <= length(sub)
            distribChannelByPing(p,c) = SWChannelId(sub(c));
            nbSamples = NbSamples(sub(c));
            if Type == 1 && Mode == 2 % MonoBeam et MultiFreq
                Samples(p, 1:nbSamples, c) = X((OffsetPing + 1):(OffsetPing+nbSamples));
            else
                Samples(1:nbSamples, c, p) = X((OffsetPing + 1):(OffsetPing+nbSamples));
            end
            OffsetPing = OffsetPing + nbSamples;
        end
    end
end
