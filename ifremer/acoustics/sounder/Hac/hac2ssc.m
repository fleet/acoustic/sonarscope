% TODO : FormatVersion � �crire dans tous les .xml
%
%   nomFic = getNomFicDatabase('PELGAS10_003_20100428_183857.hac')
%   nomFic = 'F:\SonarScopeData\From AndreOgor\0812_20091213_205617_Lesuroit_Orig_220et2200.hac';
%   nomFic = 'F:\SonarScopeData\From AndreOgor\0812_20091213_205617_Lesuroit_Orig_901et9001.hac';
%   flag = hac2ssc(nomFic)
%
%   a = cl_hac('nomFic', nomFic);
%
%   [flag, DataBin] = read_Heading_bin(a)
% -------------------------------------------------------------------------

function [flag, identSounders] = hac2ssc(nomFic, varargin)

global IfrTbxResources %#ok<GVMIS>
global IfrTbx %#ok<GVMIS>

FormatVersion = 20110531;
identSounders = [];
C0 = cl_hac([]);

progressBar = 1;

% Barre de progression uniquement visible en version de Dev car semble �tre
% � l'origine de probl�me lors du traitement successif de donn�es (27/09/2012).
%{
if my_isdeployed
    progressBar = 1;
else
    progressBar = 1;
end
%}

nbOc = sizeFic(nomFic);
if nbOc == 0
    flag = 0;
    return
end

nomExe = 'CONVERT_hac.exe';

if isdeployed
    nomDir = fileparts(IfrTbxResources);
    nomExe = fullfile(nomDir, 'extern', 'C', nomExe);
else
    nomExe = [IfrTbx '\ifremer\extern\C\CONVERT_exe\Release\' nomExe];
end

%% G�n�ration des XML et des binaires

[nomDir, nomFicSeul] = fileparts(nomFic);
nomDirRacine = fullfile(nomDir, 'SonarScope', nomFicSeul);
unzipSonarScopeCache(nomDirRacine);

flag = [];
flag(end+1) = exist(nomDirRacine, 'dir');
flag(end+1) = exist(fullfile(nomDirRacine, 'SSc_FileIndex.xml'), 'file');
flag(end+1) = exist(fullfile(nomDirRacine, 'SSc_FileIndex'), 'dir');

% Comment� car un fichier de poss�dait pas ces tuples mais tout le reste
% �tait bon (fichier IRENE PELGAS09).
% % flag(end+1) = exist(fullfile(nomDirRacine, 'Ssc_SignatureBeginOfFile.xml'), 'file');
% % flag(end+1) = exist(fullfile(nomDirRacine, 'Ssc_SignatureBeginOfFile'), 'dir');

% Comment� car un fichier de poss�dait pas ces tuples mais tout le reste
% �tait bon
% flag(end+1) = exist(fullfile(nomDirRacine, 'Ssc_SignatureEndOfFile.xml'), 'file');
% flag(end+1) = exist(fullfile(nomDirRacine, 'Ssc_SignatureEndOfFile'), 'dir');

if all(flag) && (getFormatVersion(C0, nomDirRacine) == FormatVersion)
    flag = 1;
    
    % Identification des sondeurs.
    identSounders = getIdentSounders(nomDirRacine, identSounders);
    
    return
end


nomFicTmp = [nomDirRacine '_tmp'];
if exist(nomFicTmp, 'dir')
    message_erreur_convert(nomFicTmp, nomFic, nomDirRacine)
    flag = 0;
    return
end

%% Test si on diminue les plantages lorsque les donn�es sont sur r�seau

[flagDisqueReseau, nomFic, nomDirReseau] = SonarScopeCacheLocal(nomFic);

%% G�n�ration des XML et des binaires

[nomDir, nomFicSeul] = fileparts(nomFic);
nomDirRacine = fullfile(nomDir, 'SonarScope', nomFicSeul);
cmd = sprintf('!"%s" "%d" "%s"', nomExe, progressBar, nomFic);
unzipSonarScopeCache(nomDirRacine);

fprintf(1,'%s\n', cmd);

try
    flag = checkSpaceBeforeProcessing(nomFic, 3);
    while ~flag
        str1 = 'Le pr�traitement du fichier HAC risque de manquer de place sur disque. Que voulez-vous faire ?';
        str2 = 'The HAC preprocessing could fail beacause ther is not a lot of space left on the disk. What do you want to do ?';
        rep1 = Lang('Attendre le temps de faire du nettoyage', 'TODO');
        rep2 = Lang('Continuer', 'TODO');
        [rep, flag] = my_questdlg(Lang(str1,str2), rep1, rep2);
        if ~flag
            return
        end
        if rep == 1
            flag = checkSpaceBeforeProcessing(nomFic, 3);
        end
    end
    
    
    msg = 'DEBUG';
    try
        [~, msg] = dos(cmd(2:end));
    catch ME
        ErrorReport = getReport(ME) %#ok<NOPRT>
        %             SendEmailSupport(ErrorReport)
        renameBadFile(this.nomFic, 'Message', ErrorReport)
    end
catch %#ok<CTCH>
    str1 = sprintf('CONVERT_hac.exe semble avoir rencontr� un probl�me sur le fichier %s', nomFic);
    str2 = sprintf('CONVERT_hac.exe does not seem working for file %s', nomFic);
    my_warndlg(Lang(str1,str2), 0);
    ErrorReport = getReport(ME) %#ok<NOPRT>
    %     SendEmailSupport(ErrorReport)
    renameBadFile(this.nomFic, 'Message', ErrorReport)
end

if isempty(msg)
    str1 = sprintf('CONVERT_hac.exe semble avoir rencontr� un probl�me sur le fichier %s', nomFic);
    str2 = sprintf('CONVERT_hac.exe does not seem working for file %s', nomFic);
    my_warndlg(Lang(str1,str2), 0, 'Tag', nomFic);
end

if ~exist(nomDirRacine, 'dir')
    if isempty(msg)
        str1 = sprintf('CONVERT_hac.exe semble avoir rencontr� un probl�me sur le fichier %s', nomFic);
        str2 = sprintf('CONVERT_hac.exe does not seem working for file %s', nomFic);
        msg = Lang(str1,str2);
    end
    my_warndlg(msg, 0, 'Tag', nomFic);
    
    if length(nomFic) > 128
        str1 = sprintf('Le nom du fichier est peut �tre trop long pour �tre interpr�t� par la commande DOS, essayer de raccourcsa longueur : %s (%d caract�res).', nomFic, length(nomFic));
        str2 = sprintf('The file name is perhaps too long to be interpreted by the DOS command, try to shorten it : %s (%d characters)', nomFic, length(nomFic));
        msg = Lang(str1,str2);
        my_warndlg(msg, 0, 'Tag', 'CONVERT_hac_Bug');
    end
    
    flag = 0;
    return
end

%% Datagrammes SignatureBeginOfFile (65535)

flag = hac2ssc_SignatureBeginOfFile(nomDirRacine, FormatVersion); %#ok<NASGU>
% if ~flag % TODO : � r�tablir quand Roger aura corrig� le pb dans hac2ssc
%     return
% end


%% Datagrammes Position (20)

if exist(fullfile(nomDirRacine, 'HAC_Position.xml'), 'file')
    flag = hac2ssc_Position(nomDirRacine, FormatVersion);
    if ~flag
        return
    end
end

%% Datagrammes Navigation (30)

if exist(fullfile(nomDirRacine, 'HAC_SpeedHeading.xml'), 'file')
    flag = hac2ssc_SpeedHeading(nomDirRacine, FormatVersion);
    if ~flag
        return
    end
end

%% Datagrammes STD profile (11000)

if exist(fullfile(nomDirRacine, 'HAC_STDProfile.xml'), 'file')
    flag = hac2ssc_STDProfile(nomDirRacine, FormatVersion);
    if ~flag
        return
    end
end

%% D�termination des sondeurs par num�ro de r�pertoire.

identSounders = getIdentSounders(nomDirRacine, identSounders);
if isempty(identSounders)
    str1 = 'Aucun sondeur identifi� par le processus d''import. Import interrompu !';
    str2 = 'No identified sounders by import processus. Import is stopped.';
    my_warndlg(Lang(str1,str2), 1);
    flag = 0;
    return
end
%EK60
% pppp                    = strmatch('EK60', {identSounders(:).Name});
pppp                    = strcmp('EK60', {identSounders(:).Name});
NbSounderEK60           = sum(pppp);
idEchoSounderEK60       = identSounders(pppp);

%ME70
pppp                    = strcmp('ME70', {identSounders(:).Name});
NbSounderME70           = sum(pppp);
idEchoSounderME70       = identSounders(pppp);

%Generic
pppp                    = strcmp('HAC_Generic', {identSounders(:).Name});
NbSounderGeneric        = sum(pppp);
idEchoSounderGeneric    = identSounders(pppp);

NbTotalSounder = size(identSounders,2);

%% Datagrammes Simrad EK 60 EchoSounder (210)

for iSounder=1:NbSounderEK60
    [flag, SounderDesc(idEchoSounderEK60(iSounder).id).DataSounder]= hac2ssc_EK60EchoSounder(nomDirRacine, idEchoSounderEK60(iSounder).id, FormatVersion); %#ok<AGROW>
    if ~flag
        return
    end
end

%% Datagrammes Simrad ME70 EchoSounder (220)

for iSounder=1:NbSounderME70
    [flag, SounderDesc(idEchoSounderME70(iSounder).id).DataSounder]= hac2ssc_ME70EchoSounder(nomDirRacine, idEchoSounderME70(iSounder).id, FormatVersion);
    if ~flag
        return
    end
end

%% Datagrammes GenericEchoSounder (901)

for iSounder=1:NbSounderGeneric
    [flag, SounderDesc(idEchoSounderGeneric(iSounder).id).DataSounder]= hac2ssc_GenericEchoSounder(nomDirRacine, idEchoSounderGeneric(iSounder).id, FormatVersion);
    if ~flag
        return
    end
end

%% Datagrammes Simrad EK60 Channel (2100)

for iSounder=1:NbSounderEK60
    NomFicXML = 'HAC_Channel.xml';
    if exist(fullfile(nomDirRacine, ['HAC_Sounder' num2str(idEchoSounderEK60(iSounder).id)], NomFicXML), 'file')
        [flag, SounderDesc(idEchoSounderEK60(iSounder).id).DataChannel] = hac2ssc_EK60Channel(nomDirRacine, idEchoSounderEK60(iSounder).id, FormatVersion);
        if ~flag
            return
        end
    end
end

%% Datagrammes Simrad ME70 Channel (2200)

for iSounder=1:NbSounderME70
    NomFicXML = 'HAC_Channel.xml';
    if exist(fullfile(nomDirRacine, ['HAC_Sounder' num2str(idEchoSounderME70(iSounder).id)], NomFicXML), 'file')
        [flag, SounderDesc(idEchoSounderME70(iSounder).id).DataChannel] = hac2ssc_ME70Channel(nomDirRacine, idEchoSounderME70(iSounder).id, FormatVersion);
        if ~flag
            return
        end
    end
end

%% Datagrammes Generic Channel (9001)

for iSounder=1:NbSounderGeneric
    NomFicXML = 'HAC_Channel.xml';
    if exist(fullfile(nomDirRacine, ['HAC_Sounder' num2str(idEchoSounderGeneric(iSounder).id)], NomFicXML), 'file')
        [flag, SounderDesc(idEchoSounderGeneric(iSounder).id).DataChannel] = hac2ssc_GenericChannel(nomDirRacine, idEchoSounderGeneric(iSounder).id, FormatVersion);
        if ~flag
            return
        end
    end
end

%% Datagrammes AttitudeSensor (10140)

for iSounder=1:NbTotalSounder
    NomFicXML = 'HAC_AttitudeSensor.xml';
    if exist(fullfile(nomDirRacine, ['HAC_Sounder' num2str(identSounders(iSounder).id)], NomFicXML), 'file')
        flag = hac2ssc_AttitudeSensor(nomDirRacine, identSounders(iSounder).id, FormatVersion);
        if ~flag
            return
        end
    end
end

%% Datagrammes PlatformAttitudeParameters (41)

for iSounder=1:NbTotalSounder
    NomFicXML = 'HAC_PlatformAttitudeParameters.xml';
    if exist(fullfile(nomDirRacine, ['HAC_Sounder' num2str(identSounders(iSounder).id)], NomFicXML), 'file')
        flag = hac2ssc_PlatformAttitudeParameters(nomDirRacine, identSounders(iSounder).id, FormatVersion);
        if ~flag
            return
        end
    end
end

%% Datagrammes Ping tuple C-16 (10040)

for iSounder=1:NbTotalSounder
    NomFicXML = 'HAC_PingC16Amplitude.xml';
    if exist(fullfile(nomDirRacine, ['HAC_Sounder' num2str(identSounders(iSounder).id)], NomFicXML), 'file')
        try
            flag = hac2ssc_PingC16Amplitude(nomDirRacine, identSounders(iSounder).id, SounderDesc(identSounders(iSounder).id), FormatVersion);
            if ~flag
                return
            end
        catch ME
            ErrorReport = getReport(ME) %#ok<NOPRT>
            %             SendEmailSupport(ErrorReport)
            renameBadFile(nomFic, 'Message', ErrorReport)
            [stat, mess, id] = rmdir(nomDirRacine, 's'); %#ok<ASGLU>
        end
    else
        NomFicXML = 'HAC_PingU16Amplitude.xml';
        if ~exist(fullfile(nomDirRacine, ['HAC_Sounder' num2str(identSounders(iSounder).id)], NomFicXML), 'file')
            str1 = sprintf('Le fichier "%s", ne contient pas de tuples Amplitude, il n''y a pas donc pas d''amplitude.', nomFic);
            str2 = sprintf('"%s" do not contain any Amplitude tuple, there is no amplitude.', nomFic);
            my_warndlg(Lang(str1,str2), 0, 'Tag', 'NoHAC_PingC16OrU16Amplitude');
        end
    end
end


%% Datagrammes Ping tuple U16 (10030) - Samples non compress�s.

for iSounder=1:NbTotalSounder
    NomFicXML = 'HAC_PingU16Amplitude.xml';
    if exist(fullfile(nomDirRacine, ['HAC_Sounder' num2str(identSounders(iSounder).id)], NomFicXML), 'file')
        try
            flag = hac2ssc_PingU16Amplitude(nomDirRacine, identSounders(iSounder).id, SounderDesc(identSounders(iSounder).id), FormatVersion);
            if ~flag
                return
            end
        catch ME
            ErrorReport = getReport(ME) %#ok<NOPRT>
            %             SendEmailSupport(ErrorReport)
            renameBadFile(nomFic, 'Message', ErrorReport)
        end
    end
end

%% Datagrammes Ping tuple C-16-32 (10011) - Angles compress�s.

for iSounder=1:NbTotalSounder
    NomFicXML = 'HAC_PingC1632Angle.xml';
    if exist(fullfile(nomDirRacine, ['HAC_Sounder' num2str(identSounders(iSounder).id)], NomFicXML), 'file')
        try
            flag = hac2ssc_PingC1632Angle(nomDirRacine, identSounders(iSounder).id, SounderDesc(identSounders(iSounder).id), FormatVersion);
            if ~flag
                return
            end
        catch ME
            ErrorReport = getReport(ME) %#ok<NOPRT>
            %             SendEmailSupport(ErrorReport)
            renameBadFile(nomFic, 'Message', ErrorReport)
        end
    end
end


%% Datagrammes Ping tuple U16 (10031) - Angles non compress�s.

for iSounder=1:NbTotalSounder
    NomFicXML = 'HAC_PingU16Angle.xml';
    if exist(fullfile(nomDirRacine, ['HAC_Sounder' num2str(identSounders(iSounder).id)], NomFicXML), 'file')
        flag = hac2ssc_PingU16Angle(nomDirRacine, identSounders(iSounder).id, SounderDesc(identSounders(iSounder).id), FormatVersion);
        if ~flag
            return
        end
    end
end


%% Datagrammes GeneralThreshold (10100)

for iSounder=1:NbTotalSounder
    NomFicXML = 'HAC_GeneralThreshold.xml';
    if exist(fullfile(nomDirRacine, ['HAC_Sounder' num2str(identSounders(iSounder).id)], NomFicXML), 'file')
        flag = hac2ssc_GeneralThreshold(nomDirRacine, identSounders(iSounder).id, FormatVersion);
        if ~flag
            return
        end
    end
end

%% Datagrammes FileIndex

flag = hac2ssc_FileIndex(nomDirRacine, FormatVersion);
if ~flag
    return
end

%% Datagrammes SignatureEndOfFile (65534)

flag = hac2ssc_SignatureEndOfFile(nomDirRacine, FormatVersion);
if ~flag
    flag = 1; % TODO : forc� � 1 pour l'instant
    return
end

%% Nettoyage des fichiers cache estempill�s HAC_

cleanDirectories(C0, nomFic)

%% Recopie de l'arborescence au cas o� le fichier d'origine est sur un disque r�seau

SonarScopeCacheRemote(flagDisqueReseau, nomDirReseau, nomFicSeul);

%% C'est fini

flag = 1;


function identSounders = getIdentSounders(nomDirRacine, identSounders)
% Fonction de description des sondeurs selon leur mode, leur type, leur id.

% de type EK60
[flag, ~, iNbSounderEK60, iEchoSounderEK60] = getListSounders(nomDirRacine, 'EK60');
for iSounder=1:iNbSounderEK60
    if isempty(identSounders)
        identSounders(iSounder).id   = iEchoSounderEK60(iSounder);
    else
        identSounders(end+1).id      = iEchoSounderEK60(iSounder); %#ok<AGROW>
    end
    identSounders(end).Name      = 'EK60';
    identSounders(end).Type      = 1;
    identSounders(end).StrType   = 'MonoBeam';
    identSounders(end).Mode      = 1;
    identSounders(end).StrMode   = 'MultiFreq';
    if ~flag
        return
    end
end

% de type ME70
[flag, ~, iNbSounderME70, iEchoSounderME70] = getListSounders(nomDirRacine, 'ME70');
for iSounder=1:iNbSounderME70
    if isempty(identSounders)
        identSounders(iSounder).id   = iEchoSounderME70(iSounder);
    else
        identSounders(end+1).id      = iEchoSounderME70(iSounder); %#ok<AGROW>
    end
    identSounders(end).Name      = 'ME70';
    identSounders(end).Type      = 2;
    identSounders(end).StrType   = 'MultiBeam';
    identSounders(end).Mode      = 2;
    identSounders(end).StrMode   = 'MonoFreq';
    if ~flag
        return
    end
end

% de type Generic
[flag, ~, iNbSounderGeneric, iEchoSounderGeneric] = getListSounders(nomDirRacine, 'Generic');
for iSounder=1:iNbSounderGeneric
    if isempty(identSounders)
        identSounders(iSounder).id   = iEchoSounderGeneric(iSounder);
    else
        identSounders(end+1).id      = iEchoSounderGeneric(iSounder); %#ok<AGROW>
    end
    identSounders(end).Name = 'HAC_Generic';
    identSounders(end).Type      = 0;
    identSounders(end).StrType   = 'FIXME';
    identSounders(end).Mode      = 0;
    identSounders(end).StrMode   = 'FIXME';
    if ~flag
        return
    end
end


function [flag, NbTotSounder, NbSounder, EchoSounder] = getListSounders(nomDirRacine, SounderName)
% Fonction d'�num�ration des sondeurs par type et par num�ro de r�pertoire.

flag = 0; %#ok<NASGU>
listDirSounder      = dir(fullfile(nomDirRacine, 'HAC_Sounder*'));
nomFicChannel       = 'HAC_Channel.xml';

if isempty(listDirSounder)
    listDirSounder      = dir(fullfile(nomDirRacine, 'Ssc_Sounder*'));
    nomFicChannel       = 'Ssc_Channel.xml';
end
NbTotSounder        = size(listDirSounder,1);
% Initialisation de la structure des noms de r�pertoires de sondeurs.
EchoSounder         = [];
NbSounder           = 0;
for iSounder=1:NbTotSounder
    xmldata = xml_read(fullfile(nomDirRacine,listDirSounder(iSounder).name, nomFicChannel));
    pppp    = regexp(xmldata.Title, SounderName, 'ONCE');
    if ~isempty(pppp)
        if ~isempty(EchoSounder)
            % listing(end+1)     =  pppp; %#ok<AGROW>
            EchoSounder(end+1) = iSounder; %#ok<AGROW>
        else
            % listing(1)      =  pppp;
            EchoSounder(1) = iSounder;
        end
    end
end

if ~isempty(EchoSounder)
    NbSounder =  size(EchoSounder,2);
end

flag = 1;
