function flag = hac2ssc_AttitudeSensor(nomDirRacine, iEchoSounder, FormatVersion)

nomFicXml = fullfile(nomDirRacine, ['HAC_Sounder' num2str(iEchoSounder)], 'hac_AttitudeSensor.xml');
SonarFmt2ssc_message(nomFicXml, '')

%% Lecture du fichier XML d�crivant la donn�e 

flag = exist(nomFicXml, 'file');
if ~flag
    special_message('FichierNonExistant', nomFicXml)
    return
end

%% Cr�ation du r�pertoire SounderX
nomDirSondeur = fullfile(nomDirRacine, ['Ssc_Sounder' num2str(iEchoSounder)]);
if ~exist(nomDirSondeur, 'dir')
    status = mkdir(nomDirSondeur);
    if ~status
        messageErreur(nomDirSondeur)
        flag = 0;
        return
    end
end


% edit(nomFicXml)
Datagrams = xml_read(nomFicXml);
[nomDirSignal, nomFicSignal] = fileparts(nomFicXml);
nomDirSignal = fullfile(nomDirSignal, nomFicSignal);


%{
for i=1:length(Datagrams.Variables)
    fprintf(1,'%d  %s\n', i, Datagrams.Variables(i).Name)
end
%}


NbTuples = Datagrams.NbDatagrams;

[flag, TupleSize] = Read_BinVariable(Datagrams, nomDirSignal, 'TupleSize', 'uint32');
if ~flag
    return
end
% figure; plot(TupleSize); grid on; title('TupleSize');

[flag, TupleType] = Read_BinVariable(Datagrams, nomDirSignal, 'TupleType', 'uint16');
if ~flag
    return
end
% figure; plot(TupleType); grid on; title('TupleType');

[flag, Data.TimeFraction] = Read_BinVariable(Datagrams, nomDirSignal,  'TimeFraction', 'double');
if ~flag
    return
end
% figure; plot(Data.TimeFraction); grid on; title('TimeFraction');

[flag, Data.TimeCPU] = Read_BinVariable(Datagrams, nomDirSignal, 'TimeCPU', 'double');
if ~flag
    return
end
% figure; plot(Data.TimeCPU); grid on; title('TimeCPU');

[flag, Data.AttitudeSensorId] = Read_BinVariable(Datagrams, nomDirSignal, 'AttitudeSensorId', 'uint16');
if ~flag
    return
end
% figure; plot(Data.AttitudeSensorId); grid on; title('AttitudeSensorId');

[flag, Data.Pitch] = Read_BinVariable(Datagrams, nomDirSignal, 'Pitch', 'single');
if ~flag
    return
end
% figure; plot(Data.Pitch); grid on; title('Pitch');

[flag, Data.Roll] = Read_BinVariable(Datagrams, nomDirSignal, 'Roll', 'single');
if ~flag
    return
end
% figure; plot(Data.Roll); grid on; title('Roll');

[flag, Data.Heave] = Read_BinVariable(Datagrams, nomDirSignal, 'Heave', 'single');
if ~flag
    return
end
% figure; plot(Data.Heave); grid on; title('Heave');

[flag, Data.Yaw] = Read_BinVariable(Datagrams, nomDirSignal, 'Yaw', 'single');
if ~flag
    return
end
% figure; plot(Data.Yaw); grid on; title('Yaw');

[flag, TupleAttribute] = Read_BinVariable(Datagrams, nomDirSignal, 'TupleAttribute', 'int32');
if ~flag
    return
end
% figure; plot(TupleAttribute); grid on; title('TupleAttribute');

[flag, BackLink] = Read_BinVariable(Datagrams, nomDirSignal, 'BackLink', 'uint16');
if ~flag
    return
end
% figure; plot(BackLink); grid on; title('BackLink');

% Lecture pr�alable des d�pendances avec les tuples Parents.
[flag, Data.IndexTupleParent] = Read_BinVariable(Datagrams, nomDirSignal, 'IndexTupleParent', 'uint32');
if ~flag
    return
end
% figure; plot(Data.IndexTupleParent); grid on; title('Data.IndexTupleParent');

[flag, Data.IndexTupleRoot] = Read_BinVariable(Datagrams, nomDirSignal, 'IndexTupleRoot', 'uint32');
if ~flag
    return
end
% figure; plot(Data.IndexTupleRoot); grid on; title('Data.IndexTupleRoot');

%% Calcul des dates et heures :

T            = double(Data.TimeCPU);
ansi_offset  = datenum(1970,1,1,0,0,0);
matlab_time  = T./86400 + ansi_offset + Data.TimeFraction/86400; 
Data.TimeCPU = cl_time('timeMat', matlab_time);

%% G�n�ration du XML SonarScope
nomDirSsc = fullfile('Ssc_AttitudeSensor');

Info.Title                  = 'AttitudeSensor';
Info.Constructor            = 'Kongsberg';
Info.TimeOrigin             = '01/01/-4713';
Info.Comments               = '"Attitude Sensor Tuple';
Info.FormatVersion          = FormatVersion;

Info.Dimensions.NbTuples       = NbTuples  ;

Info.Signals(1).Name          = 'TimeCPU';
Info.Signals(end).Dimensions  = 'NbTuples, 1';
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = 'days since JC';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'TimeCPU.bin');
Info.Signals(end).Tag         = verifKeyWord('SounderTime');
  
Info.Signals(end+1).Name      = 'AttitudeSensorId';
Info.Signals(end).Dimensions  = 'NbTuples, 1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'AttitudeSensorId.bin');
Info.Signals(end).Tag         = verifKeyWord('AttitudeSensorId');

Info.Signals(end+1).Name      = 'Pitch';
Info.Signals(end).Dimensions  = 'NbTuples, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'Pitch.bin');
Info.Signals(end).Tag         = verifKeyWord('SounderPitch');

Info.Signals(end+1).Name      = 'Roll';
Info.Signals(end).Dimensions  = 'NbTuples, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'Roll.bin');
Info.Signals(end).Tag         = verifKeyWord('SounderRoll');

Info.Signals(end+1).Name      = 'Heave';
Info.Signals(end).Dimensions  = 'NbTuples, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'm';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'Heave.bin');
Info.Signals(end).Tag         = verifKeyWord('SounderHeave');

Info.Signals(end+1).Name      = 'Yaw';
Info.Signals(end).Dimensions  = 'NbTuples, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'Yaw.bin');
Info.Signals(end).Tag         = verifKeyWord('Yaw');

Info.Signals(end+1).Name      = 'IndexTupleRoot';
Info.Signals(end).Dimensions  = 'NbTuples, 1';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'IndexTupleRoot.bin');
Info.Signals(end).Tag         = verifKeyWord('IndexTupleRoot');

Info.Signals(end+1).Name      = 'IndexTupleParent';
Info.Signals(end).Dimensions  = 'NbTuples, 1';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'IndexTupleParent.bin');
Info.Signals(end).Tag         = verifKeyWord('IndexTupleParent');

               
%% Cr�ation du fichier XML d�crivant la donn�e 

nomFicXml = fullfile(nomDirSondeur, 'Ssc_AttitudeSensor.xml');
xml_write(nomFicXml, Info);
flag = exist(nomFicXml, 'file');
if ~flag
    messageErreur(nomFicXml)
    return
end

%% Cr�ation du r�pertoire AttitudeSensor

nomDirSsc = fullfile(nomDirSondeur, 'Ssc_AttitudeSensor');
if ~exist(nomDirSsc, 'dir')
    status = mkdir(nomDirSsc);
    if ~status
        messageErreur(nomDirSsc)
        flag = 0;
        return
    end
end

%% Cr�ation des fichiers binaires des signaux

for i=1:length(Info.Signals)
    flag = writeSignal(nomDirSondeur, Info.Signals(i), Data.(Info.Signals(i).Name));
    if ~flag
        return
    end
end

flag = 1;

