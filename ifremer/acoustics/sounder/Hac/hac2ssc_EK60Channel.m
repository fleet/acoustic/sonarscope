function [flag, DataOut] = hac2ssc_EK60Channel(nomDirRacine, iEchoSounder, FormatVersion)

nomFicXml = fullfile(nomDirRacine, ['HAC_Sounder' num2str(iEchoSounder)], 'hac_Channel.xml');
SonarFmt2ssc_message(nomFicXml, '')

%% Lecture du fichier XML d�crivant la donn�e 

flag = exist(nomFicXml, 'file');
if ~flag
    special_message('FichierNonExistant', nomFicXml)
    return
end


%% Cr�ation du r�pertoire SounderX
nomDirSondeur = fullfile(nomDirRacine, ['Ssc_Sounder' num2str(iEchoSounder)]);
if ~exist(nomDirSondeur, 'dir')
    status = mkdir(nomDirSondeur);
    if ~status
        messageErreur(nomDirSondeur)
        flag = 0;
        return
    end
end

% edit(nomFicXml)
Datagrams = xml_read(nomFicXml);
[nomDirSignal, nomFicSignal] = fileparts(nomFicXml);
nomDirSignal = fullfile(nomDirSignal, nomFicSignal);


%{
for i=1:length(Datagrams.Variables)
    fprintf(1,'%d  %s\n', i, Datagrams.Variables(i).Name)
end
%}


nbTuples = Datagrams.NbDatagrams;

[flag, TupleSize] = Read_BinVariable(Datagrams, nomDirSignal, 'TupleSize', 'uint32'); %#ok<ASGLU>
if ~flag
    return
end
% figure; plot(TupleSize); grid on; title('TupleSize');

[flag, TupleType] = Read_BinVariable(Datagrams, nomDirSignal, 'TupleType', 'uint16'); %#ok<ASGLU>
if ~flag
    return
end
% figure; plot(TupleType); grid on; title('TupleType');

[flag, Data.SoftwareChannelIdentifier] = Read_BinVariable(Datagrams, nomDirSignal, 'SoftwareChannelIdentifier', 'uint16');
if ~flag
    return
end
% figure; plot(Data.SoftwareChannelIdentifier); grid on; title('SoftwareChannelIdentifier');


[flag, Data.EchosounderDocIdentifier] = Read_BinVariable(Datagrams, nomDirSignal, 'EchosounderDocIdentifier', 'uint32');
if ~flag
    return
end
% figure; plot(Data.EchosounderDocIdentifier); grid on; title('EchosounderDocIdentifier');

[flag, Data.FrequencyChannelName] = Read_BinVariable(Datagrams, nomDirSignal, 'FrequencyChannelName', 'char');
if ~flag
    return
end
% figure; plot(Data.FrequencyChannelName); grid on; title('FrequencyChannelName');

[flag, Data.TransducerName] = Read_BinVariable(Datagrams, nomDirSignal, 'TransducerName', 'char');
if ~flag
    return
end

[flag, Data.TransceiverSoftwareVersion] = Read_BinVariable(Datagrams, nomDirSignal, 'TransceiverSoftwareVersion', 'char');
if ~flag
    return
end

[flag, Data.TimeSampleInterval] = Read_BinVariable(Datagrams, nomDirSignal, 'TimeSampleInterval', 'single');
if ~flag
    return
end
% figure; plot(Data.TimeSampleInterval); grid on; title('TimeSampleInterval');

[flag, Data.DataType] = Read_BinVariable(Datagrams, nomDirSignal, 'DataType', 'single');
if ~flag
    return
end
% figure; plot(Data.DataType); grid on; title('DataType');

[flag, Data.BeamType] = Read_BinVariable(Datagrams, nomDirSignal, 'BeamType', 'single');
if ~flag
    return
end
% figure; plot(Data.BeamType); grid on; title('BeamType');

[flag, Data.AcousticFrequency] = Read_BinVariable(Datagrams, nomDirSignal, 'AcousticFrequency', 'single');
if ~flag
    return
end
% figure; plot(Data.AcousticFrequency); grid on; title('AcousticFrequency');

[flag, Data.TransducerInstallationDepth] = Read_BinVariable(Datagrams, nomDirSignal, 'TransducerInstallationDepth', 'single');
if ~flag
    return
end
% figure; plot(Data.TransducerInstallationDepth); grid on; title('TransducerInstallationDepth');

[flag, Data.StartSample] = Read_BinVariable(Datagrams, nomDirSignal, 'StartSample', 'single');
if ~flag
    return
end
% figure; plot(Data.StartSample); grid on; title('StartSample');

[flag, Data.PlatformIdentifier] = Read_BinVariable(Datagrams, nomDirSignal, 'PlatformIdentifier', 'single');
if ~flag
    return
end
% figure; plot(Data.PlatformIdentifier); grid on; title('PlatformIdentifier');

[flag, Data.TransducerShape] = Read_BinVariable(Datagrams, nomDirSignal, 'PlatformIdentifier', 'single');
if ~flag
    return
end
% figure; plot(Data.TransducerShape); grid on; title('TransducerShape');

[flag, Data.TransducerFaceAlongshipAngleOffset] = Read_BinVariable(Datagrams, nomDirSignal, 'TransducerFaceAlongshipAngleOffset', 'single');
if ~flag
    return
end
% figure; plot(Data.TransducerFaceAlongshipAngleOffset); grid on; title('TransducerFaceAlongshipAngleOffset');

[flag, Data.TransducerFaceAthwartshipAngleOffset] = Read_BinVariable(Datagrams, nomDirSignal, 'TransducerFaceAthwartshipAngleOffset', 'single');
if ~flag
    return
end
% figure; plot(Data.TransducerFaceAthwartshipAngleOffset); grid on; title('TransducerFaceAthwartshipAngleOffset');

[flag, Data.TransducerRotationAngle] = Read_BinVariable(Datagrams, nomDirSignal, 'TransducerRotationAngle', 'single');
if ~flag
    return
end
% figure; plot(Data.TransducerRotationAngle); grid on; title('TransducerRotationAngle');

[flag, Data.AlongshipAngleOffsetToMainAxisOfAcBeam] = Read_BinVariable(Datagrams, nomDirSignal, 'AlongshipAngleOffsetToMainAxisOfAcBeam', 'single');
if ~flag
    return
end
% figure; plot(Data.AlongshipAngleOffsetToMainAxisOfAcBeam); grid on; title('AlongshipAngleOffsetToMainAxisOfAcBeam');

[flag, Data.AthwartshipAngleOffsetToMainAxisOfAcBeam] = Read_BinVariable(Datagrams, nomDirSignal, 'AthwartshipAngleOffsetToMainAxisOfAcBeam', 'single');
if ~flag
    return
end
% figure; plot(Data.AthwartshipAngleOffsetToMainAxisOfAcBeam); grid on; title('AthwartshipAngleOffsetToMainAxisOfAcBeam');

[flag, Data.AbsorptionCoefficient] = Read_BinVariable(Datagrams, nomDirSignal, 'AbsorptionCoefficient', 'single');
if ~flag
    return
end
% figure; plot(Data.AbsorptionCoefficient); grid on; title('AbsorptionCoefficient');

[flag, Data.PulseDuration] = Read_BinVariable(Datagrams, nomDirSignal, 'PulseDuration', 'single');
if ~flag
    return
end
% figure; plot(Data.PulseDuration); grid on; title('PulseDuration');

[flag, Data.Bandwidth] = Read_BinVariable(Datagrams, nomDirSignal, 'Bandwidth', 'single');
if ~flag
    return
end
% figure; plot(Data.Bandwidth); grid on; title('Bandwidth');

[flag, Data.TransmissionPower] = Read_BinVariable(Datagrams, nomDirSignal, 'TransmissionPower', 'single');
if ~flag
    return
end
% figure; plot(Data.TransmissionPower); grid on; title('TransmissionPower');

[flag, Data.BeamAlongshipAngleSensitivity] = Read_BinVariable(Datagrams, nomDirSignal, 'BeamAlongshipAngleSensitivity', 'single');
if ~flag
    return
end
% figure; plot(Data.BeamAlongshipAngleSensitivity); grid on; title('BeamAlongshipAngleSensitivity');

[flag, Data.BeamAthwartshipAngleSensitivity] = Read_BinVariable(Datagrams, nomDirSignal, 'BeamAthwartshipAngleSensitivity', 'single');
if ~flag
    return
end
% figure; plot(Data.BeamAthwartshipAngleSensitivity); grid on; title('BeamAthwartshipAngleSensitivity');

[flag, Data.BeamAlongship3dBBeamWidth] = Read_BinVariable(Datagrams, nomDirSignal, 'BeamAlongship3dBBeamWidth', 'single');
if ~flag
    return
end
% figure; plot(Data.BeamAlongship3dBBeamWidth); grid on; title('BeamAlongship3dBBeamWidth');

[flag, Data.BeamAthwartship3dBBeamWidth] = Read_BinVariable(Datagrams, nomDirSignal, 'BeamAthwartship3dBBeamWidth', 'single');
if ~flag
    return
end
% figure; plot(Data.BeamAthwartship3dBBeamWidth); grid on; title('BeamAthwartship3dBBeamWidth');

[flag, Data.BeamEquivalentTwoWayBeamAngle] = Read_BinVariable(Datagrams, nomDirSignal, 'BeamEquivalentTwoWayBeamAngle', 'single');
if ~flag
    return
end
% figure; plot(Data.BeamEquivalentTwoWayBeamAngle); grid on; title('BeamEquivalentTwoWayBeamAngle');

[flag, Data.BeamGain] = Read_BinVariable(Datagrams, nomDirSignal, 'BeamGain', 'single');
if ~flag
    return
end
% figure; plot(Data.BeamGain); grid on; title('BeamGain');

[flag, Data.BeamSACorrection] = Read_BinVariable(Datagrams, nomDirSignal, 'BeamSACorrection', 'single');
if ~flag
    return
end
% figure; plot(Data.BeamSACorrection); grid on; title('BeamSACorrection');

[flag, Data.BottomDetectionMinimumDepth] = Read_BinVariable(Datagrams, nomDirSignal, 'BottomDetectionMinimumDepth', 'single');
if ~flag
    return
end
% figure; plot(Data.BottomDetectionMinimumDepth); grid on; title('BottomDetectionMinimumDepth');

[flag, Data.BottomDetectionMaximumDepth] = Read_BinVariable(Datagrams, nomDirSignal, 'BottomDetectionMaximumDepth', 'single');
if ~flag
    return
end
% figure; plot(Data.BottomDetectionMaximumDepth); grid on; title('BottomDetectionMaximumDepth');

[flag, Data.BottomDetectionMinimumLevel] = Read_BinVariable(Datagrams, nomDirSignal, 'BottomDetectionMinimumLevel', 'single');
if ~flag
    return
end
% figure; plot(Data.BottomDetectionMinimumLevel); grid on; title('BottomDetectionMinimumLevel');


[flag, Remarks] = Read_BinVariable(Datagrams, nomDirSignal, 'Remarks', 'char'); 
if ~flag
    return
end
Remarks = Remarks(1,:);

[flag, TupleAttribute] = Read_BinVariable(Datagrams, nomDirSignal, 'TupleAttribute', 'single'); %#ok<ASGLU>
if ~flag
    return
end
% figure; plot(TupleAttribute); grid on; title('TupleAttribute');

[flag, BackLink] = Read_BinVariable(Datagrams, nomDirSignal, 'BackLink', 'single'); %#ok<ASGLU>
if ~flag
    return
end
% figure; plot(BackLink); grid on; title('BackLink');

%% G�n�ration du XML SonarScope
nomDirSsc = fullfile('Ssc_Channel');

Info.Title                  = 'EK60Channel';
Info.Constructor            = 'Kongsberg';
Info.TimeOrigin             = '01/01/-4713';
Info.Comments               = 'Simrad EK60 Channel Tuple';
Info.FormatVersion          = FormatVersion;
Info.SystemSerialNumber     = Remarks;

Info.Dimensions.NbFreq      = nbTuples;

Info.Signals(1).Name          = 'SoftwareChannelIdentifier';
Info.Signals(end).Dimensions  = '1, NbFreq';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'SoftwareChannelIdentifier.bin');
Info.Signals(end).Tag         = verifKeyWord('SoftwareChannelIdentifier');
  
Info.Signals(end+1).Name      = 'EchosounderDocIdentifier';
Info.Signals(end).Dimensions  = '1, NbFreq';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'EchosounderDocIdentifier.bin');
Info.Signals(end).Tag         = verifKeyWord('EchosounderDocIdentifier');

Info.Signals(end+1).Name      = 'FrequencyChannelName';
Info.Signals(end).Dimensions  = '1, NbFreq';
Info.Signals(end).Storage     = 'char';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'FrequencyChannelName.bin');
Info.Signals(end).Tag         = verifKeyWord('FrequencyChannelName');

Info.Signals(end+1).Name      = 'TransceiverSoftwareVersion';
Info.Signals(end).Dimensions  = '1, NbFreq';
Info.Signals(end).Storage     = 'char';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'TransceiverSoftwareVersion.bin');
Info.Signals(end).Tag         = verifKeyWord('TransceiverSoftwareVersion');

Info.Signals(end+1).Name      = 'TransducerName';
Info.Signals(end).Dimensions  = '1, NbFreq';
Info.Signals(end).Storage     = 'char';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'TransducerName.bin');
Info.Signals(end).Tag         = verifKeyWord('TransducerName');

Info.Signals(end+1).Name      = 'TimeSampleInterval';
Info.Signals(end).Dimensions  = '1, NbFreq';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 's';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'TimeSampleInterval.bin');
Info.Signals(end).Tag         = verifKeyWord('TimeSampleInterval');

Info.Signals(end+1).Name      = 'DataType';
Info.Signals(end).Dimensions  = '1, NbFreq';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'DataType.bin');
Info.Signals(end).Tag         = verifKeyWord('TypeOfData');

Info.Signals(end+1).Name      = 'BeamType';
Info.Signals(end).Dimensions  = '1, NbFreq';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'BeamType.bin');
Info.Signals(end).Tag         = verifKeyWord('BeamType');

Info.Signals(end+1).Name      = 'AcousticFrequency';
Info.Signals(end).Dimensions  = '1, NbFreq';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'Hz';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'AcousticFrequency.bin');
Info.Signals(end).Tag         = verifKeyWord('AcousticFrequency');

Info.Signals(end+1).Name      = 'TransducerInstallationDepth';
Info.Signals(end).Dimensions  = '1, NbFreq';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'm';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'TransducerInstallationDepth.bin');
Info.Signals(end).Tag         = verifKeyWord('TransducerInstallationDepth');

Info.Signals(end+1).Name      = 'StartSample';
Info.Signals(end).Dimensions  = '1, NbFreq';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'StartSample.bin');
Info.Signals(end).Tag         = verifKeyWord('StartSample');

Info.Signals(end+1).Name      = 'PlatformIdentifier';
Info.Signals(end).Dimensions  = '1, NbFreq';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'PlatformIdentifier.bin');
Info.Signals(end).Tag         = verifKeyWord('PlatformIdentifier');

Info.Signals(end+1).Name      = 'TransducerShape';
Info.Signals(end).Dimensions  = '1, NbFreq';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'TransducerShape.bin');
Info.Signals(end).Tag         = verifKeyWord('TransducerShape');

Info.Signals(end+1).Name      = 'TransducerFaceAlongshipAngleOffset';
Info.Signals(end).Dimensions  = '1, NbFreq';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'TransducerFaceAlongshipAngleOffset.bin');
Info.Signals(end).Tag         = verifKeyWord('TransducerFaceAlongshipAngleOffset');

Info.Signals(end+1).Name      = 'TransducerFaceAthwartshipAngleOffset';
Info.Signals(end).Dimensions  = '1, NbFreq';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'TransducerFaceAthwartshipAngleOffset.bin');
Info.Signals(end).Tag         = verifKeyWord('TransducerFaceAthwartshipAngleOffset');

Info.Signals(end+1).Name      = 'TransducerRotationAngle';
Info.Signals(end).Dimensions  = '1, NbFreq';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'TransducerRotationAngle.bin');
Info.Signals(end).Tag         = verifKeyWord('TransducerRotationAngle');

Info.Signals(end+1).Name      = 'AlongshipAngleOffsetToMainAxisOfAcBeam';
Info.Signals(end).Dimensions  = '1, NbFreq';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'AlongshipAngleOffsetToMainAxisOfAcBeam.bin');
Info.Signals(end).Tag         = verifKeyWord('AlongshipAngleOffsetToMainAxisOfAcBeam');

Info.Signals(end+1).Name      = 'AthwartshipAngleOffsetToMainAxisOfAcBeam';
Info.Signals(end).Dimensions  = '1, NbFreq';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'AthwartshipAngleOffsetToMainAxisOfAcBeam.bin');
Info.Signals(end).Tag         = verifKeyWord('AthwartshipAngleOffsetToMainAxisOfAcBeam');

Info.Signals(end+1).Name      = 'AbsorptionCoefficient';
Info.Signals(end).Dimensions  = '1, NbFreq';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'dB/km';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'AbsorptionCoefficient.bin');
Info.Signals(end).Tag         = verifKeyWord('AbsorptionOfSound');

Info.Signals(end+1).Name      = 'PulseDuration';
Info.Signals(end).Dimensions  = '1, NbFreq';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 's';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'PulseDuration.bin');
Info.Signals(end).Tag         = verifKeyWord('PulseDuration');

Info.Signals(end+1).Name      = 'Bandwidth';
Info.Signals(end).Dimensions  = '1, NbFreq';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 's';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'Bandwidth.bin');
Info.Signals(end).Tag         = verifKeyWord('Bandwidth');

Info.Signals(end+1).Name      = 'TransmissionPower';
Info.Signals(end).Dimensions  = '1, NbFreq';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'W';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'TransmissionPower.bin');
Info.Signals(end).Tag         = verifKeyWord('TransmissionPower');

Info.Signals(end+1).Name      = 'BeamAlongshipAngleSensitivity';
Info.Signals(end).Dimensions  = '1, NbFreq';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'El./mec.deg';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'BeamAlongshipAngleSensitivity.bin');
Info.Signals(end).Tag         = verifKeyWord('BeamAlongshipAngleSensitivity');

Info.Signals(end+1).Name      = 'BeamAthwartshipAngleSensitivity';
Info.Signals(end).Dimensions  = '1, NbFreq';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'El./mec.deg';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'BeamAthwartshipAngleSensitivity.bin');
Info.Signals(end).Tag         = verifKeyWord('BeamAthwartshipAngleSensitivity');

Info.Signals(end+1).Name      = 'BeamAlongship3dBBeamWidth';
Info.Signals(end).Dimensions  = '1, NbFreq';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'BeamAlongship3dBBeamWidth.bin');
Info.Signals(end).Tag         = verifKeyWord('BeamAlongship3dBBeamWidth');

Info.Signals(end+1).Name      = 'BeamAthwartship3dBBeamWidth';
Info.Signals(end).Dimensions  = '1, NbFreq';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'BeamAthwartship3dBBeamWidth.bin');
Info.Signals(end).Tag         = verifKeyWord('BeamAthwartship3dBBeamWidth');

Info.Signals(end+1).Name      = 'BeamEquivalentTwoWayBeamAngle';
Info.Signals(end).Dimensions  = '1, NbFreq';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'dB';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'BeamEquivalentTwoWayBeamAngle.bin');
Info.Signals(end).Tag         = verifKeyWord('BeamEquivalentTwoWayBeamAngle');

Info.Signals(end+1).Name      = 'BeamGain';
Info.Signals(end).Dimensions  = '1, NbFreq';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'dB';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'BeamGain.bin');
Info.Signals(end).Tag         = verifKeyWord('BeamGain');

Info.Signals(end+1).Name      = 'BeamSACorrection';
Info.Signals(end).Dimensions  = '1, NbFreq';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'dB';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'BeamSACorrection.bin');
Info.Signals(end).Tag         = verifKeyWord('BeamSACorrection');

Info.Signals(end+1).Name      = 'BottomDetectionMinimumDepth';
Info.Signals(end).Dimensions  = '1, NbFreq';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'm';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'BottomDetectionMinimumDepth.bin');
Info.Signals(end).Tag         = verifKeyWord('BottomDetectionMinimumDepth');

Info.Signals(end+1).Name      = 'BottomDetectionMaximumDepth';
Info.Signals(end).Dimensions  = '1, NbFreq';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'm';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'BottomDetectionMaximumDepth.bin');
Info.Signals(end).Tag         = verifKeyWord('BottomDetectionMaximumDepth');

Info.Signals(end+1).Name      = 'BottomDetectionMinimumLevel';
Info.Signals(end).Dimensions  = '1, NbFreq';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'dB';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'BottomDetectionMinimumLevel.bin');
Info.Signals(end).Tag         = verifKeyWord('BottomDetectionMinimumLevel');

% Conservation pour identification des tuples d�pendants.
DataOut.SoftwareChannelIdentifier   = Data.SoftwareChannelIdentifier;
DataOut.EchosounderDocIdentifier    = Data.EchosounderDocIdentifier;

%% Cr�ation du fichier XML d�crivant la donn�e 
nomFicXml = fullfile(nomDirSondeur, 'Ssc_Channel.xml');
xml_write(nomFicXml, Info);
flag = exist(nomFicXml, 'file');
if ~flag
    messageErreur(nomFicXml)
    return
end

%% Cr�ation du r�pertoire EK60Channel

nomDirSsc = fullfile(nomDirSondeur, 'Ssc_Channel');
if ~exist(nomDirSsc, 'dir')
    status = mkdir(nomDirSsc);
    if ~status
        messageErreur(nomDirSsc)
        flag = 0;
        return
    end
end

%% Cr�ation des fichiers binaires des signaux

for i=1:length(Info.Signals)
    flag = writeSignal(nomDirSondeur, Info.Signals(i), Data.(Info.Signals(i).Name));
    if ~flag
        return
    end
end


flag = 1;

