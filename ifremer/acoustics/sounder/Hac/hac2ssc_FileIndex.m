function flag = hac2ssc_FileIndex(nomDirRacine, FormatVersion)

nomFicXml = fullfile(nomDirRacine, 'hac_FileIndex.xml');
SonarFmt2ssc_message(nomFicXml, '.hac')

%% Lecture du fichier XML d�crivant la donn�e 

flag = exist(nomFicXml, 'file');
if ~flag
    special_message('FichierNonExistant', nomFicXml)
    return
end

% edit(nomFicXml)
Datagrams = xml_read(nomFicXml);
[nomDirSignal, nomFicSignal] = fileparts(nomFicXml);
nomDirSignal = fullfile(nomDirSignal, nomFicSignal);


%{
for i=1:length(Datagrams.Variables)
    fprintf(1,'%d  %s\n', i, Datagrams.Variables(i).Name)
end
%}

[flag, Data.TupleSize] = Read_BinVariable(Datagrams, nomDirSignal, 'TupleSize', 'single');
if ~flag
    return
end
% figure; plot(Data.TupleSize); grid on; title('TupleSize');

[flag, Data.TupleType] = Read_BinVariable(Datagrams, nomDirSignal, 'TupleType', 'single');
if ~flag
    return
end
% figure; plot(Data.TupleType); grid on; title('TupleType');

[flag, Data.DatagramPosition] = Read_BinVariable(Datagrams, nomDirSignal, 'DatagramPosition', 'double');
if ~flag
    return
end
% figure; plot(Data.DatagramPosition); grid on; title('DatagramPosition');


%% G�n�ration du XML SonarScope

Info.Title                  = 'FileIndex';
Info.Constructor            = 'Kongsberg';
Info.TimeOrigin             = '01/01/-4713';
Info.Comments               = 'Signature Begin of File Tuple';
Info.FormatVersion          = FormatVersion;

Info.Dimensions.NbTuples    = Datagrams.NbDatagrams;

Info.Signals(1).Name          = 'TupleSize';
Info.Signals(end).Dimensions  = 'NbTuples, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_FileIndex','TupleSize.bin');
Info.Signals(end).Tag         = verifKeyWord('HACIdentifier');

Info.Signals(end+1).Name      = 'TupleType';
Info.Signals(end).Dimensions  = 'NbTuples, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_FileIndex','TupleType.bin');
Info.Signals(end).Tag         = verifKeyWord('TupleType');

Info.Signals(end+1).Name      = 'DatagramPosition';
Info.Signals(end).Dimensions  = 'NbTuples, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_FileIndex','DatagramPosition.bin');
Info.Signals(end).Tag         = verifKeyWord('DatagramPosition');

               
%% Cr�ation du fichier XML d�crivant la donn�e 

nomFicXml = fullfile(nomDirRacine, 'Ssc_FileIndex.xml');
xml_write(nomFicXml, Info);
flag = exist(nomFicXml, 'file');
if ~flag
    messageErreur(nomFicXml)
    return
end

%% Cr�ation du r�pertoire FileIndex

nomDirSsc = fullfile(nomDirRacine, 'Ssc_FileIndex');
if ~exist(nomDirSsc, 'dir')
    status = mkdir(nomDirSsc);
    if ~status
        messageErreur(nomDirSsc)
        flag = 0;
        return
    end
end

%% Cr�ation des fichiers binaires des signaux

for i=1:length(Info.Signals)
    flag = writeSignal(nomDirRacine, Info.Signals(i), Data.(Info.Signals(i).Name));
    if ~flag
        return
    end
end

flag = 1;

