function flag = hac2ssc_GeneralThreshold(nomDirRacine, iEchoSounder, FormatVersion)

nomFicXml = fullfile(nomDirRacine, ['HAC_Sounder' num2str(iEchoSounder)], 'hac_GeneralThreshold.xml');
SonarFmt2ssc_message(nomFicXml, '')

%% Lecture du fichier XML d�crivant la donn�e 

flag = exist(nomFicXml, 'file');
if ~flag
    special_message('FichierNonExistant', nomFicXml)
    return
end

%% Cr�ation du r�pertoire SounderX
nomDirSondeur = fullfile(nomDirRacine, ['Ssc_Sounder' num2str(iEchoSounder)]);
if ~exist(nomDirSondeur, 'dir')
    status = mkdir(nomDirSondeur);
    if ~status
        messageErreur(nomDirSondeur)
        flag = 0;
        return
    end
end

% edit(nomFicXml)
Datagrams = xml_read(nomFicXml);
[nomDirSignal, nomFicSignal] = fileparts(nomFicXml);
nomDirSignal = fullfile(nomDirSignal, nomFicSignal);


%{
for i=1:length(Datagrams.Variables)
    fprintf(1,'%d  %s\n', i, Datagrams.Variables(i).Name)
end
%}


nbSamples = Datagrams.NbDatagrams;

[flag, TupleSize] = Read_BinVariable(Datagrams, nomDirSignal, 'TupleSize', 'uint32'); %#ok<ASGLU>
if ~flag
    return
end
% figure; plot(TupleSize); grid on; title('TupleSize');

[flag, TupleType] = Read_BinVariable(Datagrams, nomDirSignal, 'TupleType', 'uint16'); %#ok<ASGLU>
if ~flag
    return
end
% figure; plot(TupleType); grid on; title('TupleType');

[flag, Data.TimeFraction] = Read_BinVariable(Datagrams, nomDirSignal,  'TimeFraction', 'double');
if ~flag
    return
end
% figure; plot(Data.TimeFraction); grid on; title('TimeFraction');

[flag, Data.TimeCPU] = Read_BinVariable(Datagrams, nomDirSignal, 'TimeCPU', 'double');
if ~flag
    return
end
% figure; plot(Data.TimeCPU); grid on; title('TimeCPU');

[flag, Data.SoftwareChannelIdentifier] = Read_BinVariable(Datagrams, nomDirSignal, 'SoftwareChannelIdentifier', 'uint16');
if ~flag
    return
end
% figure; plot(Data.SoftwareChannelIdentifier); grid on; title('SoftwareChannelIdentifier');

[flag, Data.TVGMaxRange] = Read_BinVariable(Datagrams, nomDirSignal, 'TVGMaxRange', 'single');
if ~flag
    return
end
% figure; plot(Data.TVGMaxRange); grid on; title('TVGMaxRange');

[flag, Data.TVGMinRange] = Read_BinVariable(Datagrams, nomDirSignal, 'TVGMinRange', 'single');
if ~flag
    return
end
% figure; plot(Data.TVGMinRange); grid on; title('TVGMinRange');

[flag, Data.TVTEvaluationMode] = Read_BinVariable(Datagrams, nomDirSignal, 'TVTEvaluationMode', 'uint16');
if ~flag
    return
end
% figure; plot(Data.TVTEvaluationMode); grid on; title('TVTEvaluationMode');

[flag, Data.TVTEvaluationInterval] = Read_BinVariable(Datagrams, nomDirSignal, 'TVTEvaluationInterval', 'single');
if ~flag
    return
end
% figure; plot(Data.TVTEvaluationInterval); grid on; title('TVTEvaluationInterval');

[flag, Data.TVTEvaluationNoPings] = Read_BinVariable(Datagrams, nomDirSignal, 'TVTEvaluationNoPings', 'uint16');
if ~flag
    return
end
% figure; plot(Data.TVTEvaluationNoPings); grid on; title('TVTEvaluationNoPings');

[flag, Data.TVTEvaluationStartingTVTPingNum] = Read_BinVariable(Datagrams, nomDirSignal, 'TVTEvaluationStartingTVTPingNum', 'uint32');
if ~flag
    return
end
% figure; plot(Data.TVTEvaluationStartingTVTPingNum); grid on; title('TVTEvaluationStartingTVTPingNum');

[flag, Data.TVToffsetparameter] = Read_BinVariable(Datagrams, nomDirSignal, 'TVToffsetparameter', 'single');
if ~flag
    return
end
% figure; plot(Data.TVToffsetparameter); grid on; title('TVToffsetparameter');

[flag, Data.TVTAmplificationParameter] = Read_BinVariable(Datagrams, nomDirSignal, 'TVTAmplificationParameter', 'single');
if ~flag
    return
end
% figure; plot(Data.TVTAmplificationParameter); grid on; title('TVTAmplificationParameter');

[flag, TupleAttribute] = Read_BinVariable(Datagrams, nomDirSignal, 'TupleAttribute', 'int32'); %#ok<ASGLU>
if ~flag
    return
end
% figure; plot(TupleAttribute); grid on; title('TupleAttribute');

[flag, BackLink] = Read_BinVariable(Datagrams, nomDirSignal, 'BackLink', 'uint16'); %#ok<ASGLU>
if ~flag
    return
end
% figure; plot(BackLink); grid on; title('BackLink');

[flag, Data.IndexTupleRoot] = Read_BinVariable(Datagrams, nomDirSignal, 'IndexTupleRoot', 'uint32'); 
if ~flag
    return
end
% figure; plot(IndexTupleRoot); grid on; title('IndexTupleRoot');

[flag, Data.IndexTupleParent] = Read_BinVariable(Datagrams, nomDirSignal, 'IndexTupleParent', 'uint32'); 
if ~flag
    return
end
% figure; plot(IndexTupleParent); grid on; title('IndexTupleParent');

%% Calcul des dates et heures :
T = double(Data.TimeCPU);
ansi_offset = datenum(1970,1,1,0,0,0);
matlab_time = T./86400 + ansi_offset + Data.TimeFraction/86400; 
Data.TimeCPU = cl_time('timeMat', matlab_time);

%% G�n�ration du XML SonarScope
nomDirSsc = fullfile('Ssc_GeneralThreshold');

Info.Title                  = 'GeneralThreshold';
Info.Constructor            = 'Kongsberg';
Info.TimeOrigin             = '01/01/-4713';
Info.Comments               = 'General Threshold Tuple';
Info.FormatVersion          = FormatVersion;

Info.Dimensions.NbSamples     = nbSamples;

Info.Signals(1).Name          = 'TimeCPU';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = 'days since JC';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'TimeCPU.bin');
Info.Signals(end).Tag         = verifKeyWord('SounderTime');
  
Info.Signals(end+1).Name      = 'SoftwareChannelIdentifier';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'SoftwareChannelIdentifier.bin');
Info.Signals(end).Tag         = verifKeyWord('SoftwareChannelIdentifier');

Info.Signals(end+1).Name      = 'TVGMaxRange';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'm';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'TVGMaxRange.bin');
Info.Signals(end).Tag         = verifKeyWord('TVGMaxRange');

Info.Signals(end+1).Name      = 'TVGMinRange';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'm';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'TVGMinRange.bin');
Info.Signals(end).Tag         = verifKeyWord('TVGMinRange');

Info.Signals(end+1).Name      = 'TVTEvaluationMode';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'TVTEvaluationMode.bin');
Info.Signals(end).Tag         = verifKeyWord('TVTEvaluationMode');

Info.Signals(end+1).Name      = 'TVTEvaluationInterval';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 's';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'TVTEvaluationInterval.bin');
Info.Signals(end).Tag         = verifKeyWord('TVTEvaluationInterval');

Info.Signals(end+1).Name      = 'TVTEvaluationNoPings';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'TVTEvaluationNoPings.bin');
Info.Signals(end).Tag         = verifKeyWord('TVTEvaluationNoPings');

Info.Signals(end+1).Name      = 'TVTEvaluationStartingTVTPingNum';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'TVTEvaluationStartingTVTPingNum.bin');
Info.Signals(end).Tag         = verifKeyWord('TVTEvaluationStartingTVTPingNum');

Info.Signals(end+1).Name      = 'TVToffsetparameter';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'TVToffsetparameter.bin');
Info.Signals(end).Tag         = verifKeyWord('TVToffsetparameter');

Info.Signals(end+1).Name      = 'TVTAmplificationParameter';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'TVTAmplificationParameter.bin');
Info.Signals(end).Tag         = verifKeyWord('TVTAmplificationParameter');

Info.Signals(end+1).Name      = 'IndexTupleRoot';
Info.Signals(end).Dimensions  = 'nbTuples, 1';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'IndexTupleRoot.bin');
Info.Signals(end).Tag         = verifKeyWord('IndexTupleRoot');

Info.Signals(end+1).Name      = 'IndexTupleParent';
Info.Signals(end).Dimensions  = 'nbTuples, 1';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'IndexTupleParent.bin');
Info.Signals(end).Tag         = verifKeyWord('IndexTupleParent');


%% Cr�ation du fichier XML d�crivant la donn�e 

nomFicXml = fullfile(nomDirRacine, 'Ssc_GeneralThreshold.xml');
xml_write(nomFicXml, Info);
flag = exist(nomFicXml, 'file');
if ~flag
    messageErreur(nomFicXml)
    return
end

%% Cr�ation du r�pertoire GeneralThreshold

nomDirSsc = fullfile(nomDirSondeur, 'Ssc_GeneralThreshold');
if ~exist(nomDirSsc, 'dir')
    status = mkdir(nomDirSsc);
    if ~status
        messageErreur(nomDirSsc)
        flag = 0;
        return
    end
end

%% Cr�ation des fichiers binaires des signaux

for i=1:length(Info.Signals)
    flag = writeSignal(nomDirSondeur, Info.Signals(i), Data.(Info.Signals(i).Name));
    if ~flag
        return
    end
end

flag = 1;

