function [flag, DataOut] = hac2ssc_GenericChannel(nomDirRacine, iEchoSounder, FormatVersion)

nomFicXml = fullfile(nomDirRacine, ['HAC_Sounder' num2str(iEchoSounder)], 'hac_Channel.xml');
SonarFmt2ssc_message(nomFicXml, '')

%% Lecture du fichier XML d�crivant la donn�e 
nomDirSondeur = fullfile(nomDirRacine, ['Ssc_Sounder' num2str(iEchoSounder)]);
if ~exist(nomDirSondeur, 'dir')
    status = mkdir(nomDirSondeur);
    if ~status
        messageErreur(nomDirSondeur)
        flag = 0;
        return
    end
end

flag = exist(nomFicXml, 'file');
if ~flag
    special_message('FichierNonExistant', nomFicXml)
    return
end

% edit(nomFicXml)
Datagrams = xml_read(nomFicXml);
[nomDirSignal, nomFicSignal] = fileparts(nomFicXml);
nomDirSignal = fullfile(nomDirSignal, nomFicSignal);


%{
for i=1:length(Datagrams.Variables)
    fprintf(1,'%d  %s\n', i, Datagrams.Variables(i).Name)
end
%}


nbSamples = Datagrams.NbDatagrams;

[flag, TupleSize] = Read_BinVariable(Datagrams, nomDirSignal, 'TupleSize', 'uint32'); %#ok<ASGLU>
if ~flag
    return
end
% figure; plot(TupleSize); grid on; title('TupleSize');

[flag, TupleType] = Read_BinVariable(Datagrams, nomDirSignal, 'TupleType', 'uint16'); %#ok<ASGLU>
if ~flag
    return
end
% figure; plot(TupleType); grid on; title('TupleType');

[flag, Data.SoftwareChannelIdentifier] = Read_BinVariable(Datagrams, nomDirSignal, 'SoftwareChannelIdentifier', 'uint16'); 
if ~flag
    return
end
% figure; plot(SoftwareChannelIdentifier); grid on; title('SoftwareChannelIdentifier');
% figure; plot(Data.SoftwareChannelIdentifier); grid on; title('SoftwareChannelIdentifier');

[flag, Data.EchosounderDocIdentifier] = Read_BinVariable(Datagrams, nomDirSignal, 'EchosounderDocIdentifier', 'uint32');
if ~flag
    return
end
% figure; plot(Data.EchosounderDocIdentifier); grid on; title('EchosounderDocIdentifier');

[flag, Data.SamplingRate] = Read_BinVariable(Datagrams, nomDirSignal, 'SamplingRate', 'single');
if ~flag
    return
end
% figure; plot(Data.SamplingRate); grid on; title('SamplingRate');

[flag, Data.SamplingInterval] = Read_BinVariable(Datagrams, nomDirSignal, 'SamplingInterval', 'uint32');
if ~flag
    return
end
% figure; plot(Data.SamplingInterval); grid on; title('SamplingInterval');

[flag, Data.AcousticFrequency] = Read_BinVariable(Datagrams, nomDirSignal, 'AcousticFrequency', 'single');
if ~flag
    return
end
% figure; plot(Data.AcousticFrequency); grid on; title('AcousticFrequency');

[flag, Data.TransceiverChannelNumber] = Read_BinVariable(Datagrams, nomDirSignal, 'TransceiverChannelNumber', 'uint16');
if ~flag
    return
end
% figure; plot(Data.TransceiverChannelNumber); grid on; title('TransceiverChannelNumber');

[flag, Data.TypeOfData] = Read_BinVariable(Datagrams, nomDirSignal, 'TypeOfData', 'uint16');
if ~flag
    return
end
% figure; plot(Data.TypeOfData); grid on; title('TypeOfData');

[flag, Data.TransceiverChannelNumber] = Read_BinVariable(Datagrams, nomDirSignal, 'TransceiverChannelNumber', 'uint16');
if ~flag
    return
end
% figure; plot(Data.TransceiverChannelNumber); grid on; title('TransceiverChannelNumber');

[flag, Data.TimeVariedGainMultiplier] = Read_BinVariable(Datagrams, nomDirSignal, 'TimeVariedGainMultiplier', 'uint16');
if ~flag
    return
end
% figure; plot(Data.TimeVariedGainMultiplier); grid on; title('TimeVariedGainMultiplier');

[flag, Data.TVGBlankingMode] = Read_BinVariable(Datagrams, nomDirSignal, 'TVGBlankingMode', 'uint16');
if ~flag
    return
end
% figure; plot(Data.TVGBlankingMode); grid on; title('TVGBlankingMode');

[flag, Data.TVGMaximumRange] = Read_BinVariable(Datagrams, nomDirSignal, 'TVGMaximumRange', 'single');
if ~flag
    return
end
% figure; plot(Data.TVGMaximumRange); grid on; title('TVGMaximumRange');

[flag, Data.TVGMinimumRange] = Read_BinVariable(Datagrams, nomDirSignal, 'TVGMinimumRange', 'single');
if ~flag
    return
end
% figure; plot(Data.TVGMinimumRange); grid on; title('TVGMinimumRange');

[flag, Data.BlankingUpToRange] = Read_BinVariable(Datagrams, nomDirSignal, 'BlankingUpToRange', 'single');
if ~flag
    return
end
% figure; plot(Data.BlankingUpToRange); grid on; title('BlankingUpToRange');

[flag, Data.SampleRange] = Read_BinVariable(Datagrams, nomDirSignal, 'SampleRange', 'single');
if ~flag
    return
end
% figure; plot(Data.SampleRange); grid on; title('SampleRange');

[flag, Data.TransducerInstallationDepth] = Read_BinVariable(Datagrams, nomDirSignal, 'TransducerInstallationDepth', 'single');
if ~flag
    return
end
% figure; plot(Data.TransducerInstallationDepth); grid on; title('TransducerInstallationDepth');

[flag, Data.PlatformIdentifier] = Read_BinVariable(Datagrams, nomDirSignal, 'PlatformIdentifier', 'uint16');
if ~flag
    return
end
% figure; plot(Data.PlatformIdentifier); grid on; title('PlatformIdentifier');

[flag, Data.AlongshipOffsetRelToAttSensor] = Read_BinVariable(Datagrams, nomDirSignal, 'AlongshipOffsetRelToAttSensor', 'single');
if ~flag
    return
end
% figure; plot(Data.AlongshipOffsetRelToAttSensor); grid on; title('AlongshipOffsetRelToAttSensor');

[flag, Data.AthwartshipOffsetRelToAttSensor] = Read_BinVariable(Datagrams, nomDirSignal, 'AthwartshipOffsetRelToAttSensor', 'single');
if ~flag
    return
end
% figure; plot(Data.AthwartshipOffsetRelToAttSensor); grid on; title('AthwartshipOffsetRelToAttSensor');

[flag, Data.VerticalOffsetRelToAttSensor] = Read_BinVariable(Datagrams, nomDirSignal, 'VerticalOffsetRelToAttSensor', 'single');
if ~flag
    return
end
% figure; plot(Data.VerticalOffsetRelToAttSensor); grid on; title('VerticalOffsetRelToAttSensor');

[flag, Data.AlongshipAngleOffsetOfTransducer] = Read_BinVariable(Datagrams, nomDirSignal, 'AlongshipAngleOffsetOfTransducer', 'single');
if ~flag
    return
end
% figure; plot(Data.AlongshipAngleOffsetOfTransducer); grid on; title('AlongshipAngleOffsetOfTransducer');

[flag, Data.AthwartshipAngleOffsetOfTransducer] = Read_BinVariable(Datagrams, nomDirSignal, 'AthwartshipAngleOffsetOfTransducer', 'single');
if ~flag
    return
end
% figure; plot(Data.AthwartshipAngleOffsetOfTransducer); grid on; title('AthwartshipAngleOffsetOfTransducer');

[flag, Data.RotationAngleOffsetOfTransducer] = Read_BinVariable(Datagrams, nomDirSignal, 'RotationAngleOffsetOfTransducer', 'single');
if ~flag
    return
end
% figure; plot(Data.RotationAngleOffsetOfTransducer); grid on; title('RotationAngleOffsetOfTransducer');

[flag, Data.AlongshipAngleOffsetToMainAxisOfAcBeam] = Read_BinVariable(Datagrams, nomDirSignal, 'AlongshipAngleOffsetToMainAxisOfAcBeam', 'single');
if ~flag
    return
end
% figure; plot(Data.AlongshipAngleOffsetToMainAxisOfAcBeam); grid on; title('AlongshipAngleOffsetToMainAxisOfAcBeam');

[flag, Data.AthwartshipAngleOffsetToMainAxisOfAcBeam] = Read_BinVariable(Datagrams, nomDirSignal, 'AthwartshipAngleOffsetToMainAxisOfAcBeam', 'single');
if ~flag
    return
end
% figure; plot(Data.AthwartshipAngleOffsetToMainAxisOfAcBeam); grid on; title('AthwartshipAngleOffsetToMainAxisOfAcBeam');

[flag, Data.AbsorptionOfSound] = Read_BinVariable(Datagrams, nomDirSignal, 'AbsorptionOfSound', 'single');
if ~flag
    return
end
% figure; plot(Data.AbsorptionOfSound); grid on; title('AbsorptionOfSound');

[flag, Data.PulseDuration] = Read_BinVariable(Datagrams, nomDirSignal, 'PulseDuration', 'single');
if ~flag
    return
end
% figure; plot(Data.PulseDuration); grid on; title('PulseDuration');

[flag, Data.PulseShapeMode] = Read_BinVariable(Datagrams, nomDirSignal, 'PulseShapeMode', 'uint16');
if ~flag
    return
end
% figure; plot(Data.PulseShapeMode); grid on; title('PulseShapeMode');

[flag, Data.BandWidth] = Read_BinVariable(Datagrams, nomDirSignal, 'BandWidth', 'single');
if ~flag
    return
end
% figure; plot(Data.BandWidth); grid on; title('BandWidth');

[flag, Data.TransducerShapeMode] = Read_BinVariable(Datagrams, nomDirSignal, 'TransducerShapeMode', 'uint16');
if ~flag
    return
end
% figure; plot(Data.TransducerShapeMode); grid on; title('TransducerShapeMode');

[flag, Data.BeamAlongship3dBBeamWidth] = Read_BinVariable(Datagrams, nomDirSignal, 'BeamAlongship3dBBeamWidth', 'single');
if ~flag
    return
end
% figure; plot(Data.BeamAlongship3dBBeamWidth); grid on; title('BeamAlongship3dBBeamWidth');

[flag, Data.BeamAthwartship3dBBeamWidth] = Read_BinVariable(Datagrams, nomDirSignal, 'BeamAthwartship3dBBeamWidth', 'single');
if ~flag
    return
end
% figure; plot(Data.BeamAthwartship3dBBeamWidth); grid on; title('BeamAthwartship3dBBeamWidth');

[flag, Data.PulseShapeMode] = Read_BinVariable(Datagrams, nomDirSignal, 'PulseShapeMode', 'uint16');
if ~flag
    return
end
% figure; plot(Data.PulseShapeMode); grid on; title('PulseShapeMode');

[flag, Data.BeamEquivalentTwoWayBeamAngle] = Read_BinVariable(Datagrams, nomDirSignal, 'BeamEquivalentTwoWayBeamAngle', 'single');
if ~flag
    return
end
% figure; plot(Data.BeamEquivalentTwoWayBeamAngle); grid on; title('BeamEquivalentTwoWayBeamAngle');

[flag, Data.CalibrationSourceLevel] = Read_BinVariable(Datagrams, nomDirSignal, 'CalibrationSourceLevel', 'single');
if ~flag
    return
end
% figure; plot(Data.CalibrationSourceLevel); grid on; title('CalibrationSourceLevel');

[flag, Data.CalibrationReceivingSensitivity] = Read_BinVariable(Datagrams, nomDirSignal, 'CalibrationReceivingSensitivity', 'single');
if ~flag
    return
end
% figure; plot(Data.CalibrationReceivingSensitivity); grid on; title('CalibrationReceivingSensitivity');

[flag, Data.SLPlusVR] = Read_BinVariable(Datagrams, nomDirSignal, 'SLPlusVR', 'single');
if ~flag
    return
end
% figure; plot(Data.SLPlusVR); grid on; title('SLPlusVR');


[flag, Data.BottomDetectionMinimumLevel] = Read_BinVariable(Datagrams, nomDirSignal, 'BottomDetectionMinimumLevel', 'single');
if ~flag
    return
end
% figure; plot(Data.BottomDetectionMinimumLevel); grid on; title('BottomDetectionMinimumLevel');

[flag, Data.BottomDetectionMinimumDepth] = Read_BinVariable(Datagrams, nomDirSignal, 'BottomDetectionMinimumDepth', 'single');
if ~flag
    return
end
% figure; plot(Data.BottomDetectionMinimumDepth); grid on; title('BottomDetectionMinimumDepth');

[flag, Data.BottomDetectionMaximumDepth] = Read_BinVariable(Datagrams, nomDirSignal, 'BottomDetectionMaximumDepth', 'single');
if ~flag
    return
end
% figure; plot(Data.BottomDetectionMaximumDepth); grid on; title('BottomDetectionMaximumDepth');

[flag, Remarks] = Read_BinVariable(Datagrams, nomDirSignal, 'Remarks', 'char');
if ~flag
    return
end
Remarks = Remarks(1,:);

[flag, TupleAttribute] = Read_BinVariable(Datagrams, nomDirSignal, 'TupleAttribute', 'int32'); %#ok<ASGLU>
if ~flag
    return
end
% figure; plot(TupleAttribute); grid on; title('TupleAttribute');

[flag, BackLink] = Read_BinVariable(Datagrams, nomDirSignal, 'BackLink', 'uint16'); %#ok<ASGLU>
if ~flag
    return
end
% figure; plot(BackLink); grid on; title('BackLink');

[flag, Data.IndexTupleParent] = Read_BinVariable(Datagrams, nomDirSignal, 'IndexTupleParent', 'uint32'); 
if ~flag
    return
end
% figure; plot(IndexTupleParent); grid on; title('IndexTupleParent');

%% G�n�ration du XML SonarScope
nomDirSsc = fullfile('Ssc_Channel');

Info.Title                  = 'GenericChannel';
Info.Constructor            = 'Kongsberg';
Info.TimeOrigin             = '01/01/-4713';
Info.Comments               = 'Generic Channel Tuple';
Info.FormatVersion          = FormatVersion;
Info.SystemSerialNumber     = Remarks;

Info.Dimensions.NbSamples     = nbSamples;

Info.Signals(1).Name          = 'SoftwareChannelIdentifier';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'SoftwareChannelIdentifier.bin');
Info.Signals(end).Tag         = verifKeyWord('SoftwareChannelIdentifier');
  
Info.Signals(end+1).Name      = 'EchosounderDocIdentifier';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'EchosounderDocIdentifier.bin');
Info.Signals(end).Tag         = verifKeyWord('EchosounderDocIdentifier');

Info.Signals(end+1).Name      = 'SamplingRate';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'sample/s';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'SamplingRate.bin');
Info.Signals(end).Tag         = verifKeyWord('SounderSamplingRate');

Info.Signals(end+1).Name      = 'SamplingInterval';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'm';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'SamplingInterval.bin');
Info.Signals(end).Tag         = verifKeyWord('SamplingInterval');

Info.Signals(end+1).Name      = 'AcousticFrequency';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'Hz';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'AcousticFrequency.bin');
Info.Signals(end).Tag         = verifKeyWord('AcousticFrequency');

Info.Signals(end+1).Name      = 'TransceiverChannelNumber';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'single';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'TransceiverChannelNumber.bin');
Info.Signals(end).Tag         = verifKeyWord('TransceiverChannelNumber');

Info.Signals(end+1).Name      = 'TypeOfData';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'TypeOfData.bin');
Info.Signals(end).Tag         = verifKeyWord('TypeOfData');

Info.Signals(end+1).Name      = 'TVGBlankingMode';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'TVGBlankingMode.bin');
Info.Signals(end).Tag         = verifKeyWord('TVGBlankingMode');

Info.Signals(end+1).Name      = 'TVGMinimumRange';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'm';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'TVGMinimumRange.bin');
Info.Signals(end).Tag         = verifKeyWord('TVGMinimumRange');

Info.Signals(end+1).Name      = 'TVGMaximumRange';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'm';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'TVGMaximumRange.bin');
Info.Signals(end).Tag         = verifKeyWord('TVGMaximumRange');

Info.Signals(end+1).Name      = 'TimeVariedGainMultiplier';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'TimeVariedGainMultiplier.bin');
Info.Signals(end).Tag         = verifKeyWord('TimeVariedGainMultiplier');

Info.Signals(end+1).Name      = 'BlankingUpToRange';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'm';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'BlankingUpToRange.bin');
Info.Signals(end).Tag         = verifKeyWord('BlankingUpToRange');

Info.Signals(end+1).Name      = 'SampleRange';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'm';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'SampleRange.bin');
Info.Signals(end).Tag         = verifKeyWord('SampleRange');

Info.Signals(end+1).Name      = 'TransducerInstallationDepth';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'm';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'TransducerInstallationDepth.bin');
Info.Signals(end).Tag         = verifKeyWord('TransducerInstallationDepth');

Info.Signals(end+1).Name      = 'PlatformIdentifier';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'PlatformIdentifier.bin');
Info.Signals(end).Tag         = verifKeyWord('PlatformIdentifier');

Info.Signals(end+1).Name      = 'AlongshipOffsetRelToAttSensor';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'm';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'AlongshipOffsetRelToAttSensor.bin');
Info.Signals(end).Tag         = verifKeyWord('AlongshipOffsetRelToAttSensor');

Info.Signals(end+1).Name      = 'AthwartshipOffsetRelToAttSensor';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'm';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'AthwartshipOffsetRelToAttSensor.bin');
Info.Signals(end).Tag         = verifKeyWord('AthwartshipOffsetRelToAttSensor');

Info.Signals(end+1).Name      = 'VerticalOffsetRelToAttSensor';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'm';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'VerticalOffsetRelToAttSensor.bin');
Info.Signals(end).Tag         = verifKeyWord('VerticalOffsetRelToAttSensor');

Info.Signals(end+1).Name      = 'AlongshipAngleOffsetOfTransducer';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'AlongshipAngleOffsetOfTransducer.bin');
Info.Signals(end).Tag         = verifKeyWord('AlongshipAngleOffsetOfTransducer');

Info.Signals(end+1).Name      = 'AthwartshipAngleOffsetOfTransducer';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'AthwartshipAngleOffsetOfTransducer.bin');
Info.Signals(end).Tag         = verifKeyWord('AthwartshipAngleOffsetOfTransducer');

Info.Signals(end+1).Name      = 'RotationAngleOffsetOfTransducer';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'RotationAngleOffsetOfTransducer.bin');
Info.Signals(end).Tag         = verifKeyWord('RotationAngleOffsetOfTransducer');

Info.Signals(end+1).Name      = 'AlongshipAngleOffsetToMainAxisOfAcBeam';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'AlongshipAngleOffsetToMainAxisOfAcBeam.bin');
Info.Signals(end).Tag         = verifKeyWord('AlongshipAngleOffsetToMainAxisOfAcBeam');

Info.Signals(end+1).Name      = 'AthwartshipAngleOffsetToMainAxisOfAcBeam';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'AthwartshipAngleOffsetToMainAxisOfAcBeam.bin');
Info.Signals(end).Tag         = verifKeyWord('AthwartshipAngleOffsetToMainAxisOfAcBeam');

Info.Signals(end+1).Name      = 'AbsorptionOfSound';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'dB/km';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'AbsorptionOfSound.bin');
Info.Signals(end).Tag         = verifKeyWord('AbsorptionOfSound');

Info.Signals(end+1).Name      = 'PulseDuration';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'ms';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'PulseDuration.bin');
Info.Signals(end).Tag         = verifKeyWord('PulseDuration');

Info.Signals(end+1).Name      = 'PulseShapeMode';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'PulseShapeMode.bin');
Info.Signals(end).Tag         = verifKeyWord('PulseShapeMode');

Info.Signals(end+1).Name      = 'BandWidth';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'kHz';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'BandWidth.bin');
Info.Signals(end).Tag         = verifKeyWord('Bandwidth');

Info.Signals(end+1).Name      = 'TransducerShapeMode';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'TransducerShapeMode.bin');
Info.Signals(end).Tag         = verifKeyWord('TransducerShapeMode');

Info.Signals(end+1).Name      = 'BeamAlongship3dBBeamWidth';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'BeamAlongship3dBBeamWidth.bin');
Info.Signals(end).Tag         = verifKeyWord('BeamAlongship3dBBeamWidth');

Info.Signals(end+1).Name      = 'BeamAthwartship3dBBeamWidth';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'BeamAthwartship3dBBeamWidth.bin');
Info.Signals(end).Tag         = verifKeyWord('BeamAthwartship3dBBeamWidth');

Info.Signals(end+1).Name      = 'BeamEquivalentTwoWayBeamAngle';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'dB';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'BeamEquivalentTwoWayBeamAngle.bin');
Info.Signals(end).Tag         = verifKeyWord('BeamEquivalentTwoWayBeamAngle');

Info.Signals(end+1).Name      = 'CalibrationSourceLevel';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '0.01 dBv/?Pa @ 1m';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'CalibrationSourceLevel.bin');
Info.Signals(end).Tag         = verifKeyWord('CalibrationSourceLevel');

Info.Signals(end+1).Name      = 'CalibrationReceivingSensitivity';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '0.01 dBv/?Pa @ 1m';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'CalibrationReceivingSensitivity.bin');
Info.Signals(end).Tag         = verifKeyWord('CalibrationReceivingSensitivity');

Info.Signals(end+1).Name      = 'SLPlusVR';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '0.01 dBv/?Pa @ 1m';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'SLPlusVR.bin');
Info.Signals(end).Tag         = verifKeyWord('SLPlusVR');

Info.Signals(end+1).Name      = 'BottomDetectionMinimumLevel';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'single';
switch Data.TypeOfData(1) 
    case {0;10}
        Info.Signals(end).Unit = 'Volts';
    case {5;15}
        Info.Signals(end).Unit = 'Volts�';
    case {4;14}
        Info.Signals(end).Unit = 'dB re 1 watt';
    case {1;2;11;12}
        Info.Signals(end).Unit = 'dB';
end
Info.Signals(end).FileName    = fullfile(nomDirSsc,'BottomDetectionMinimumLevel.bin');
Info.Signals(end).Tag         = verifKeyWord('BottomDetectionMinimumLevel');

Info.Signals(end+1).Name      = 'BottomDetectionMinimumDepth';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'm';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'BottomDetectionMinimumDepth.bin');
Info.Signals(end).Tag         = verifKeyWord('BottomDetectionMinimumDepth');

Info.Signals(end+1).Name      = 'BottomDetectionMaximumDepth';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'm';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'BottomDetectionMaximumDepth.bin');
Info.Signals(end).Tag         = verifKeyWord('BottomDetectionMaximumDepth');

Info.Signals(end+1).Name      = 'IndexTupleParent';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'IndexTupleParent.bin');
Info.Signals(end).Tag         = verifKeyWord('IndexTupleParent');

% Conservation pour identification des tuples d�pendants.
DataOut.SoftwareChannelIdentifier   = Data.SoftwareChannelIdentifier;
DataOut.EchosounderDocIdentifier    = Data.EchosounderDocIdentifier;

%% Cr�ation du fichier XML d�crivant la donn�e 

nomFicXml = fullfile(nomDirSondeur, 'Ssc_Channel.xml');
xml_write(nomFicXml, Info);
flag = exist(nomFicXml, 'file');
if ~flag
    messageErreur(nomFicXml)
    return
end

%% Cr�ation du r�pertoire GenericChannel

nomDirSsc = fullfile(nomDirSondeur, 'Ssc_Channel');
if ~exist(nomDirSsc, 'dir')
    status = mkdir(nomDirSsc);
    if ~status
        messageErreur(nomDirSsc)
        flag = 0;
        return
    end
end

%% Cr�ation des fichiers binaires des signaux

for i=1:length(Info.Signals)
    flag = writeSignal(nomDirSondeur, Info.Signals(i), Data.(Info.Signals(i).Name));
    if ~flag
        return
    end
end


flag = 1;

