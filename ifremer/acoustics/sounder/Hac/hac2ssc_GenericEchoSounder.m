function [flag, DescSounder] = hac2ssc_GenericEchoSounder(nomDirRacine, iEchoSounder, FormatVersion)

nomFicXml = fullfile(nomDirRacine, ['HAC_Sounder' num2str(iEchoSounder)], 'hac_EchoSounder.xml');
SonarFmt2ssc_message(nomFicXml, '')

%% Lecture du fichier XML d�crivant la donn�e 

flag = exist(nomFicXml, 'file');
if ~flag
    special_message('FichierNonExistant', nomFicXml)
    return
end

%% Cr�ation du r�pertoire SounderX
nomDirSondeur = fullfile(nomDirRacine, ['Ssc_Sounder' num2str(iEchoSounder)]);
if ~exist(nomDirSondeur, 'dir')
    status = mkdir(nomDirSondeur);
    if ~status
        messageErreur(nomDirSondeur)
        flag = 0;
        return
    end
end

% edit(nomFicXml)
Datagrams = xml_read(nomFicXml);
[nomDirSignal, nomFicSignal] = fileparts(nomFicXml);
nomDirSignal = fullfile(nomDirSignal, nomFicSignal);


%{
for i=1:length(Datagrams.Variables)
    fprintf(1,'%d  %s\n', i, Datagrams.Variables(i).Name)
end
%}


nbPings = Datagrams.NbDatagrams;

[flag, TupleSize] = Read_BinVariable(Datagrams, nomDirSignal, 'TupleSize', 'uint32'); %#ok<ASGLU>
if ~flag
    return
end
% figure; plot(TupleSize); grid on; title('TupleSize');

[flag, TupleType] = Read_BinVariable(Datagrams, nomDirSignal, 'TupleType', 'uint16'); %#ok<ASGLU>
if ~flag
    return
end
% figure; plot(TupleType); grid on; title('TupleType');

[flag, Data.NumberSWChannels] = Read_BinVariable(Datagrams, nomDirSignal, 'NumberSWChannels', 'uint16'); 
if ~flag
    return
end
% figure; plot(NumberSWChannels); grid on; title('NumberSWChannels');

[flag, Data.EchosounderDocIdentifier] = Read_BinVariable(Datagrams, nomDirSignal, 'EchosounderDocIdentifier', 'uint32');
if ~flag
    return
end
% figure; plot(Data.EchosounderDocIdentifier); grid on; title('EchosounderDocIdentifier');

[flag, Data.SoundSpeed] = Read_BinVariable(Datagrams, nomDirSignal, 'SoundSpeed', 'single');
if ~flag
    return
end
% figure; plot(Data.SoundSpeed); grid on; title('SoundSpeed');

[flag, Data.TriggerMode] = Read_BinVariable(Datagrams, nomDirSignal, 'TriggerMode', 'uint16');
if ~flag
    return
end
% figure; plot(Data.TriggerMode); grid on; title('TriggerMode');

[flag, Data.PingInterval] = Read_BinVariable(Datagrams, nomDirSignal, 'PingInterval', 'single');
if ~flag
    return
end
% figure; plot(Data.PingInterval); grid on; title('PingInterval');

[flag, Data.Remarks] = Read_BinVariable(Datagrams, nomDirSignal, 'Remarks', 'char');
if ~flag
    return
end

[flag, TupleAttribute] = Read_BinVariable(Datagrams, nomDirSignal, 'TupleAttribute', 'int32'); %#ok<ASGLU>
if ~flag
    return
end
% figure; plot(TupleAttribute); grid on; title('TupleAttribute');

[flag, BackLink] = Read_BinVariable(Datagrams, nomDirSignal, 'BackLink', 'uint16'); %#ok<ASGLU>
if ~flag
    return
end
% figure; plot(BackLink); grid on; title('BackLink');

%% G�n�ration du XML SonarScope
nomDirSsc = fullfile('Ssc_EchoSounder');

Info.Title                  = 'GenericEchoSounder';
Info.Constructor            = 'Kongsberg';
Info.TimeOrigin             = '01/01/-4713';
Info.Comments               = 'Generic EchoSounder Tuple';
Info.FormatVersion          = FormatVersion;

Info.Dimensions.NbPings     = nbPings;

Info.Signals(1).Name          = 'NumberSWChannels';
Info.Signals(end).Dimensions  = 'NbPings, 1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'NumberSWChannels.bin');
Info.Signals(end).Tag         = verifKeyWord('NumberSWChannels');
  
Info.Signals(end+1).Name      = 'EchosounderDocIdentifier';
Info.Signals(end).Dimensions  = 'NbPings, 1';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'EchosounderDocIdentifier.bin');
Info.Signals(end).Tag         = verifKeyWord('EchosounderDocIdentifier');

Info.Signals(end+1).Name      = 'SoundSpeed';
Info.Signals(end).Dimensions  = 'NbPings, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'm/s';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'SoundSpeed.bin');
Info.Signals(end).Tag         = verifKeyWord('SounderSoundSpeed');

Info.Signals(end+1).Name      = 'PingInterval';
Info.Signals(end).Dimensions  = 'NbPings, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 's';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'PingInterval.bin');
Info.Signals(end).Tag         = verifKeyWord('PingInterval');

Info.Signals(end+1).Name      = 'TriggerMode';
Info.Signals(end).Dimensions  = 'NbPings, 1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'TriggerMode.bin');
Info.Signals(end).Tag         = verifKeyWord('TriggerMode');

% Conservation pour identification des tuples d�pendants.
DescSounder.NumberSWChannels            = Data.NumberSWChannels;
DescSounder.EchosounderDocIdentifier    = Data.EchosounderDocIdentifier;
DescSounder.TypeSounder                 = 1; % 1 : Mono-Faisceau, 2 : Multi-Faisceau
DescSounder.ModeSounder                 = 2; % 1 : Mono-Fr�quence, 2 : Multi-Fr�quence



%% Cr�ation du fichier XML d�crivant la donn�e 

nomFicXml = fullfile(nomDirSondeur, 'Ssc_EchoSounder.xml');
xml_write(nomFicXml, Info);
flag = exist(nomFicXml, 'file');
if ~flag
    messageErreur(nomFicXml)
    return
end

%% Cr�ation du r�pertoire GenericEchoSounder

nomDirSsc = fullfile(nomDirSondeur, 'Ssc_EchoSounder');
if ~exist(nomDirSsc, 'dir')
    status = mkdir(nomDirSsc);
    if ~status
        messageErreur(nomDirSsc)
        flag = 0;
        return
    end
end

%% Cr�ation des fichiers binaires des signaux

for i=1:length(Info.Signals)
    flag = writeSignal(nomDirSondeur, Info.Signals(i), Data.(Info.Signals(i).Name));
    if ~flag
        return
    end
end

flag = 1;

