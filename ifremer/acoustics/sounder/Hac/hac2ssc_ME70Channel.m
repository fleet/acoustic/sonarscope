function [flag, DataOut] = hac2ssc_ME70Channel(nomDirRacine, iEchoSounder, FormatVersion)

nomFicXml = fullfile(nomDirRacine, ['HAC_Sounder' num2str(iEchoSounder)], 'hac_Channel.xml');
SonarFmt2ssc_message(nomFicXml, '')

%% Lecture du fichier XML d�crivant la donn�e 
nomDirSondeur = fullfile(nomDirRacine, ['Ssc_Sounder' num2str(iEchoSounder)]);
if ~exist(nomDirSondeur, 'dir')
    status = mkdir(nomDirSondeur);
    if ~status
        messageErreur(nomDirSondeur)
        flag = 0;
        return
    end
end

flag = exist(nomFicXml, 'file');
if ~flag
    special_message('FichierNonExistant', nomFicXml)
    return
end

% edit(nomFicXml)
Datagrams = xml_read(nomFicXml);
[nomDirSignal, nomFicSignal] = fileparts(nomFicXml);
nomDirSignal = fullfile(nomDirSignal, nomFicSignal);


%{
for i=1:length(Datagrams.Variables)
    fprintf(1,'%d  %s\n', i, Datagrams.Variables(i).Name)
end
%}


nbSamples = Datagrams.NbDatagrams;

[flag, TupleSize] = Read_BinVariable(Datagrams, nomDirSignal, 'TupleSize', 'uint32'); %#ok<ASGLU>
if ~flag
    return
end
% figure; plot(TupleSize); grid on; title('TupleSize');

[flag, TupleType] = Read_BinVariable(Datagrams, nomDirSignal, 'TupleType', 'uint16'); %#ok<ASGLU>
if ~flag
    return
end
% figure; plot(TupleType); grid on; title('TupleType');

[flag, Data.SoftwareChannelIdentifier] = Read_BinVariable(Datagrams, nomDirSignal, 'SoftwareChannelIdentifier', 'uint16');
if ~flag
    return
end
% figure; plot(Data.SoftwareChannelIdentifier); grid on; title('SoftwareChannelIdentifier');


[flag, Data.EchosounderDocIdentifier] = Read_BinVariable(Datagrams, nomDirSignal, 'EchosounderDocIdentifier', 'uint32');
if ~flag
    return
end
% figure; plot(Data.EchosounderDocIdentifier); grid on; title('EchosounderDocIdentifier');

[flag, Data.FrequencyChannelName] = Read_BinVariable(Datagrams, nomDirSignal, 'FrequencyChannelName', 'char');
if ~flag
    return
end
% figure; plot(Data.FrequencyChannelName); grid on; title('FrequencyChannelName');

[flag, Data.DataType] = Read_BinVariable(Datagrams, nomDirSignal, 'DataType', 'uint16');
if ~flag
    return
end
% figure; plot(Data.DataType); grid on; title('DataType');

[flag, Data.BeamType] = Read_BinVariable(Datagrams, nomDirSignal, 'BeamType', 'uint16');
if ~flag
    return
end
% figure; plot(Data.BeamType); grid on; title('BeamType');

[flag, Data.AcousticFrequency] = Read_BinVariable(Datagrams, nomDirSignal, 'AcousticFrequency', 'single');
if ~flag
    return
end
% figure; plot(Data.AcousticFrequency); grid on; title('AcousticFrequency');

[flag, Data.StartSample] = Read_BinVariable(Datagrams, nomDirSignal, 'StartSample', 'uint16');
if ~flag
    return
end
% figure; plot(Data.StartSample); grid on; title('StartSample');

[flag, Data.AlongshipSteeringAngle] = Read_BinVariable(Datagrams, nomDirSignal, 'AlongshipSteeringAngle', 'single');
if ~flag
    return
end
% figure; plot(Data.AlongshipSteeringAngle); grid on; title('AlongshipSteeringAngle');

[flag, Data.AthwartshipSteeringAngle] = Read_BinVariable(Datagrams, nomDirSignal, 'AthwartshipSteeringAngle', 'single');
if ~flag
    return
end
% figure; plot(Data.AthwartshipSteeringAngle); grid on; title('AthwartshipSteeringAngle');

[flag, Data.AbsorptionCoefficient] = Read_BinVariable(Datagrams, nomDirSignal, 'AbsorptionCoefficient', 'single');
if ~flag
    return
end
% figure; plot(Data.AbsorptionCoefficient); grid on; title('AbsorptionCoefficient');

[flag, Data.Bandwidth] = Read_BinVariable(Datagrams, nomDirSignal, 'Bandwidth', 'single');
if ~flag
    return
end
% figure; plot(Data.Bandwidth); grid on; title('Bandwidth');

[flag, Data.TransmissionPower] = Read_BinVariable(Datagrams, nomDirSignal, 'TransmissionPower', 'single');
if ~flag
    return
end
% figure; plot(Data.TransmissionPower); grid on; title('TransmissionPower');

[flag, Data.BeamAlongshipAngleSensitivity] = Read_BinVariable(Datagrams, nomDirSignal, 'BeamAlongshipAngleSensitivity', 'single');
if ~flag
    return
end
% figure; plot(Data.BeamAlongshipAngleSensitivity); grid on; title('BeamAlongshipAngleSensitivity');

[flag, Data.BeamAthwartshipAngleSensitivity] = Read_BinVariable(Datagrams, nomDirSignal, 'BeamAthwartshipAngleSensitivity', 'single');
if ~flag
    return
end
% figure; plot(Data.BeamAthwartshipAngleSensitivity); grid on; title('BeamAthwartshipAngleSensitivity');

[flag, Data.BeamAlongship3dBBeamWidth] = Read_BinVariable(Datagrams, nomDirSignal, 'BeamAlongship3dBBeamWidth', 'single');
if ~flag
    return
end
% figure; plot(Data.BeamAlongship3dBBeamWidth); grid on; title('BeamAlongship3dBBeamWidth');

[flag, Data.BeamAthwartship3dBBeamWidth] = Read_BinVariable(Datagrams, nomDirSignal, 'BeamAthwartship3dBBeamWidth', 'single');
if ~flag
    return
end
% figure; plot(Data.BeamAthwartship3dBBeamWidth); grid on; title('BeamAthwartship3dBBeamWidth');

[flag, Data.BeamEquivalentTwoWayBeamAngle] = Read_BinVariable(Datagrams, nomDirSignal, 'BeamEquivalentTwoWayBeamAngle', 'single');
if ~flag
    return
end
% figure; plot(Data.BeamEquivalentTwoWayBeamAngle); grid on; title('BeamEquivalentTwoWayBeamAngle');

[flag, Data.BeamGain] = Read_BinVariable(Datagrams, nomDirSignal, 'BeamGain', 'single');
if ~flag
    return
end
% figure; plot(Data.BeamGain); grid on; title('BeamGain');

[flag, Data.BeamSACorrection] = Read_BinVariable(Datagrams, nomDirSignal, 'BeamSACorrection', 'single');
if ~flag
    return
end
% figure; plot(Data.BeamSACorrection); grid on; title('BeamSACorrection');


[flag, Data.BottomDetectionMinimumLevel] = Read_BinVariable(Datagrams, nomDirSignal, 'BottomDetectionMinimumLevel', 'single');
if ~flag
    return
end
% figure; plot(Data.BottomDetectionMinimumLevel); grid on; title('BottomDetectionMinimumLevel');

[flag, Data.BottomDetectionMinimumDepth] = Read_BinVariable(Datagrams, nomDirSignal, 'BottomDetectionMinimumDepth', 'single');
if ~flag
    return
end
% figure; plot(Data.BottomDetectionMinimumDepth); grid on; title('BottomDetectionMinimumDepth');

[flag, Data.BottomDetectionMaximumDepth] = Read_BinVariable(Datagrams, nomDirSignal, 'BottomDetectionMaximumDepth', 'single');
if ~flag
    return
end
% figure; plot(Data.BottomDetectionMaximumDepth); grid on; title('BottomDetectionMaximumDepth');

[flag, Data.AlongshipTXRXWeightingIdentifier] = Read_BinVariable(Datagrams, nomDirSignal, 'AlongshipTXRXWeightingIdentifier', 'single');
if ~flag
    return
end
% figure; plot(Data.AlongshipTXRXWeightingIdentifier); grid on; title('AlongshipTXRXWeightingIdentifier');

[flag, Data.AthwartshipTXRXWeightingIdentifier] = Read_BinVariable(Datagrams, nomDirSignal, 'AthwartshipTXRXWeightingIdentifier', 'single');
if ~flag
    return
end
% figure; plot(Data.AthwartshipTXRXWeightingIdentifier); grid on; title('AthwartshipTXRXWeightingIdentifier');

[flag, Data.SplitbeamAlongshipRXWeightingIdentifier] = Read_BinVariable(Datagrams, nomDirSignal, 'SplitbeamAlongshipRXWeightingIdentifier', 'single');
if ~flag
    return
end
% figure; plot(Data.SplitbeamAlongshipRXWeightingIdentifier); grid on; title('SplitbeamAlongshipRXWeightingIdentifier');

[flag, Data.SplitbeamAthwartshipRXWeightingIdentifier] = Read_BinVariable(Datagrams, nomDirSignal, 'SplitbeamAthwartshipRXWeightingIdentifier', 'single');
if ~flag
    return
end
% figure; plot(Data.SplitbeamAthwartshipRXWeightingIdentifier); grid on; title('SplitbeamAthwartshipRXWeightingIdentifier');

[flag, Remarks] = Read_BinVariable(Datagrams, nomDirSignal, 'Remarks', 'char');
if ~flag
    return
end
Remarks = Remarks(1,:);

[flag, TupleAttribute] = Read_BinVariable(Datagrams, nomDirSignal, 'TupleAttribute', 'int32'); %#ok<ASGLU>
if ~flag
    return
end
% figure; plot(TupleAttribute); grid on; title('TupleAttribute');

[flag, BackLink] = Read_BinVariable(Datagrams, nomDirSignal, 'BackLink', 'uint16'); %#ok<ASGLU>
if ~flag
    return
end
% figure; plot(BackLink); grid on; title('BackLink');


%% G�n�ration du XML SonarScope
nomDirSsc = fullfile('Ssc_Channel');

Info.Title                  = 'ME70Channel';
Info.Constructor            = 'Kongsberg';
Info.TimeOrigin             = '01/01/-4713';
Info.Comments               = 'ME70 Channel Tuple';
Info.FormatVersion          = FormatVersion;
Info.SystemSerialNumber     = Remarks;

Info.Dimensions.NbSamples     = nbSamples;

Info.Signals(1).Name          = 'SoftwareChannelIdentifier';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'SoftwareChannelIdentifier.bin');
Info.Signals(end).Tag         = verifKeyWord('SoftwareChannelIdentifier');
  
Info.Signals(end+1).Name      = 'EchosounderDocIdentifier';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'EchosounderDocIdentifier.bin');
Info.Signals(end).Tag         = verifKeyWord('EchosounderDocIdentifier');

Info.Signals(end+1).Name      = 'FrequencyChannelName';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'char';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'FrequencyChannelName.bin');
Info.Signals(end).Tag         = verifKeyWord('FrequencyChannelName');

Info.Signals(end+1).Name      = 'DataType';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'DataType.bin');
Info.Signals(end).Tag         = verifKeyWord('TypeOfData');

Info.Signals(end+1).Name      = 'BeamType';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'BeamType.bin');
Info.Signals(end).Tag         = verifKeyWord('BeamType');

Info.Signals(end+1).Name      = 'AcousticFrequency';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'Hz';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'AcousticFrequency.bin');
Info.Signals(end).Tag         = verifKeyWord('AcousticFrequency');

Info.Signals(end+1).Name      = 'StartSample';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'StartSample.bin');
Info.Signals(end).Tag         = verifKeyWord('StartSample');

Info.Signals(end+1).Name      = 'AlongshipSteeringAngle';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'AlongshipSteeringAngle.bin');
Info.Signals(end).Tag         = verifKeyWord('AlongshipSteeringAngle');

Info.Signals(end+1).Name      = 'AthwartshipSteeringAngle';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'AthwartshipSteeringAngle.bin');
Info.Signals(end).Tag         = verifKeyWord('AthwartshipSteeringAngle');

Info.Signals(end+1).Name      = 'AbsorptionCoefficient';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'dB/km';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'AbsorptionCoefficient.bin');
Info.Signals(end).Tag         = verifKeyWord('AbsorptionOfSound');

Info.Signals(end+1).Name      = 'Bandwidth';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'Hz';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'Bandwidth.bin');
Info.Signals(end).Tag         = verifKeyWord('Bandwidth');

Info.Signals(end+1).Name      = 'TransmissionPower';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'W';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'TransmissionPower.bin');
Info.Signals(end).Tag         = verifKeyWord('TransmissionPower');

Info.Signals(end+1).Name      = 'BeamAlongshipAngleSensitivity';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'El./mec.deg';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'BeamAlongshipAngleSensitivity.bin');
Info.Signals(end).Tag         = verifKeyWord('BeamAlongshipAngleSensitivity');

Info.Signals(end+1).Name      = 'BeamAthwartshipAngleSensitivity';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'El./mec.deg';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'BeamAthwartshipAngleSensitivity.bin');
Info.Signals(end).Tag         = verifKeyWord('BeamAthwartshipAngleSensitivity');

Info.Signals(end+1).Name      = 'BeamAlongship3dBBeamWidth';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'BeamAlongship3dBBeamWidth.bin');
Info.Signals(end).Tag         = verifKeyWord('BeamAlongship3dBBeamWidth');

Info.Signals(end+1).Name      = 'BeamAthwartship3dBBeamWidth';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'BeamAthwartship3dBBeamWidth.bin');
Info.Signals(end).Tag         = verifKeyWord('BeamAthwartship3dBBeamWidth');

Info.Signals(end+1).Name      = 'BeamEquivalentTwoWayBeamAngle';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'dB';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'BeamEquivalentTwoWayBeamAngle.bin');
Info.Signals(end).Tag         = verifKeyWord('BeamEquivalentTwoWayBeamAngle');

Info.Signals(end+1).Name      = 'BeamGain';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'dB';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'BeamGain.bin');
Info.Signals(end).Tag         = verifKeyWord('BeamGain');

Info.Signals(end+1).Name      = 'BeamSACorrection';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'dB';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'BeamSACorrection.bin');
Info.Signals(end).Tag         = verifKeyWord('BeamSACorrection');

Info.Signals(end+1).Name      = 'BottomDetectionMinimumDepth';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'm';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'BottomDetectionMinimumDepth.bin');
Info.Signals(end).Tag         = verifKeyWord('BottomDetectionMinimumDepth');

Info.Signals(end+1).Name      = 'BottomDetectionMaximumDepth';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'm';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'BottomDetectionMaximumDepth.bin');
Info.Signals(end).Tag         = verifKeyWord('BottomDetectionMaximumDepth');

Info.Signals(end+1).Name      = 'BottomDetectionMinimumLevel';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'dB';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'BottomDetectionMinimumLevel.bin');
Info.Signals(end).Tag         = verifKeyWord('BottomDetectionMinimumLevel');

Info.Signals(end+1).Name      = 'AlongshipTXRXWeightingIdentifier';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'm';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'AlongshipTXRXWeightingIdentifier.bin');
Info.Signals(end).Tag         = verifKeyWord('AlongshipTXRXWeightingIdentifier');

Info.Signals(end+1).Name      = 'AthwartshipTXRXWeightingIdentifier';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'm';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'AthwartshipTXRXWeightingIdentifier.bin');
Info.Signals(end).Tag         = verifKeyWord('AthwartshipTXRXWeightingIdentifier');

Info.Signals(end+1).Name      = 'SplitbeamAlongshipRXWeightingIdentifier';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'm';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'SplitbeamAlongshipRXWeightingIdentifier.bin');
Info.Signals(end).Tag         = verifKeyWord('SplitbeamAlongshipRXWeightingIdentifier');

Info.Signals(end+1).Name      = 'SplitbeamAthwartshipRXWeightingIdentifier';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'm';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'SplitbeamAthwartshipRXWeightingIdentifier.bin');
Info.Signals(end).Tag         = verifKeyWord('SplitbeamAthwartshipRXWeightingIdentifier');

% Conservation pour identification des tuples d�pendants.
DataOut.SoftwareChannelIdentifier   = Data.SoftwareChannelIdentifier;
DataOut.EchosounderDocIdentifier    = Data.EchosounderDocIdentifier;

%% Cr�ation du fichier XML d�crivant la donn�e 

nomFicXml = fullfile(nomDirSondeur, 'Ssc_Channel.xml');
xml_write(nomFicXml, Info);
flag = exist(nomFicXml, 'file');
if ~flag
    messageErreur(nomFicXml)
    return
end

%% Cr�ation du r�pertoire ME70Channel

nomDirSsc = fullfile(nomDirSondeur, 'Ssc_Channel');
if ~exist(nomDirSsc, 'dir')
    status = mkdir(nomDirSsc);
    if ~status
        messageErreur(nomDirSsc)
        flag = 0;
        return
    end
end

%% Cr�ation des fichiers binaires des signaux

for i=1:length(Info.Signals)
    flag = writeSignal(nomDirSondeur, Info.Signals(i), Data.(Info.Signals(i).Name));
    if ~flag
        return
    end
end


flag = 1;

