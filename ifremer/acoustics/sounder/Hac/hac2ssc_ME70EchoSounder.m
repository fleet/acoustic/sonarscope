function [flag, DescSounder] = hac2ssc_ME70EchoSounder(nomDirRacine, iEchoSounder, FormatVersion)

nomFicXml = fullfile(nomDirRacine, ['HAC_Sounder' num2str(iEchoSounder)], 'hac_EchoSounder.xml');
SonarFmt2ssc_message(nomFicXml, '')

%% Lecture du fichier XML d�crivant la donn�e 

flag = exist(nomFicXml, 'file');
if ~flag
    special_message('FichierNonExistant', nomFicXml)
    return
end

%% Cr�ation du r�pertoire SounderX
nomDirSondeur = fullfile(nomDirRacine, ['Ssc_Sounder' num2str(iEchoSounder)]);
if ~exist(nomDirSondeur, 'dir')
    status = mkdir(nomDirSondeur);
    if ~status
        messageErreur(nomDirSondeur)
        flag = 0;
        return
    end
end


% edit(nomFicXml)
Datagrams = xml_read(nomFicXml);
[nomDirSignal, nomFicSignal] = fileparts(nomFicXml);
nomDirSignal = fullfile(nomDirSignal, nomFicSignal);


%{
for i=1:length(Datagrams.Variables)
    fprintf(1,'%d  %s\n', i, Datagrams.Variables(i).Name)
end
%}


nbPings = Datagrams.NbDatagrams;

[flag, TupleSize] = Read_BinVariable(Datagrams, nomDirSignal, 'TupleSize', 'uint32'); %#ok<ASGLU>
if ~flag
    return
end
% figure; plot(TupleSize); grid on; title('TupleSize');

[flag, TupleType] = Read_BinVariable(Datagrams, nomDirSignal, 'TupleType', 'uint16'); %#ok<ASGLU>
if ~flag
    return
end
% figure; plot(TupleType); grid on; title('TupleType');

[flag, Data.NumberSWChannels] = Read_BinVariable(Datagrams, nomDirSignal, 'NumberSWChannels', 'uint16');
if ~flag
    return
end
% figure; plot(NumberSWChannels); grid on; title('NumberSWChannels');

[flag, Data.EchosounderDocIdentifier] = Read_BinVariable(Datagrams, nomDirSignal, 'EchosounderDocIdentifier', 'uint32');
if ~flag
    return
end
% figure; plot(Data.EchosounderDocIdentifier); grid on; title('EchosounderDocIdentifier');

[flag, Data.TransducerName] = Read_BinVariable(Datagrams, nomDirSignal, 'TransducerName', 'char');
if ~flag
    return
end

[flag, Data.TransceiverSoftwareVersion] = Read_BinVariable(Datagrams, nomDirSignal, 'TransceiverSoftwareVersion', 'char');
if ~flag
    return
end

[flag, Data.SoundSpeed] = Read_BinVariable(Datagrams, nomDirSignal, 'SoundSpeed', 'single');
if ~flag
    return
end
% figure; plot(Data.SoundSpeed); grid on; title('SoundSpeed');

[flag, Data.TriggerMode] = Read_BinVariable(Datagrams, nomDirSignal, 'TriggerMode', 'uint16');
if ~flag
    return
end
% figure; plot(Data.TriggerMode); grid on; title('TriggerMode');

[flag, Data.PingInterval] = Read_BinVariable(Datagrams, nomDirSignal, 'PingInterval', 'single');
if ~flag
    return
end
% figure; plot(Data.PingInterval); grid on; title('PingInterval');

[flag, Data.PulseMode] = Read_BinVariable(Datagrams, nomDirSignal, 'PulseMode', 'single');
if ~flag
    return
end
% figure; plot(Data.PulseMode); grid on; title('PulseMode');

[flag, Data.PulseDuration] = Read_BinVariable(Datagrams, nomDirSignal, 'PulseDuration', 'single');
if ~flag
    return
end
% figure; plot(Data.PulseDuration); grid on; title('PulseDuration');

[flag, Data.TimeSampleInterval] = Read_BinVariable(Datagrams, nomDirSignal, 'TimeSampleInterval', 'single');
if ~flag
    return
end
% figure; plot(Data.TimeSampleInterval); grid on; title('TimeSampleInterval');

[flag, Data.FrequencyBeamSpacing] = Read_BinVariable(Datagrams, nomDirSignal, 'FrequencyBeamSpacing', 'uint16');
if ~flag
    return
end
% figure; plot(Data.FrequencyBeamSpacing); grid on; title('FrequencyBeamSpacing');

[flag, Data.FrequencySpaceShape] = Read_BinVariable(Datagrams, nomDirSignal, 'FrequencySpaceShape', 'uint16');
if ~flag
    return
end
% figure; plot(Data.FrequencySpaceShape); grid on; title('FrequencySpaceShape');

[flag, Data.TransceiverPower] = Read_BinVariable(Datagrams, nomDirSignal, 'TransceiverPower', 'single');
if ~flag
    return
end
% figure; plot(Data.TransceiverPower); grid on; title('TransceiverPower');

[flag, Data.TransducerInstallationDepth] = Read_BinVariable(Datagrams, nomDirSignal, 'TransducerInstallationDepth', 'single');
if ~flag
    return
end
% figure; plot(Data.TransducerInstallationDepth); grid on; title('TransducerInstallationDepth');

[flag, Data.PlatformIdentifier] = Read_BinVariable(Datagrams, nomDirSignal, 'PlatformIdentifier', 'uint16');
if ~flag
    return
end
% figure; plot(Data.PlatformIdentifier); grid on; title('PlatformIdentifier');

[flag, Data.TransducerShape] = Read_BinVariable(Datagrams, nomDirSignal, 'TransducerShape', 'uint16');
if ~flag
    return
end
% figure; plot(Data.TransducerShape); grid on; title('TransducerShape');

[flag, Data.TransducerFaceAlongshipAngleOffset] = Read_BinVariable(Datagrams, nomDirSignal, 'TransducerFaceAlongshipAngleOffset', 'single');
if ~flag
    return
end
% figure; plot(Data.TransducerFaceAlongshipAngleOffset); grid on; title('TransducerFaceAlongshipAngleOffset');

[flag, Data.TransducerFaceAthwartshipAngleOffset] = Read_BinVariable(Datagrams, nomDirSignal, 'TransducerFaceAthwartshipAngleOffset', 'single');
if ~flag
    return
end
% figure; plot(Data.TransducerFaceAthwartshipAngleOffset); grid on; title('TransducerFaceAthwartshipAngleOffset');

[flag, Data.TransducerRotationAngle] = Read_BinVariable(Datagrams, nomDirSignal, 'TransducerRotationAngle', 'single');
if ~flag
    return
end
% figure; plot(Data.TransducerRotationAngle); grid on; title('TransducerRotationAngle');

[flag, Data.Remarks] = Read_BinVariable(Datagrams, nomDirSignal, 'Remarks', 'char');
if ~flag
    return
end

[flag, TupleAttribute] = Read_BinVariable(Datagrams, nomDirSignal, 'TupleAttribute', 'int32'); %#ok<ASGLU>
if ~flag
    return
end
% figure; plot(TupleAttribute); grid on; title('TupleAttribute');

[flag, BackLink] = Read_BinVariable(Datagrams, nomDirSignal, 'BackLink', 'uint16'); %#ok<ASGLU>
if ~flag
    return
end
% figure; plot(BackLink); grid on; title('BackLink');

%% G�n�ration du XML SonarScope
nomDirSsc = fullfile('Ssc_EchoSounder');

Info.Title                  = 'ME70EchoSounder';
Info.Constructor            = 'Kongsberg';
Info.TimeOrigin             = '01/01/-4713';
Info.Comments               = 'Simrad ME 70 EchoSounder Tuple';
Info.FormatVersion          = FormatVersion;

Info.Dimensions.NbPings     = nbPings;

Info.Signals(1).Name          = 'NumberSWChannels';
Info.Signals(end).Dimensions  = 'NbPings, 1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'NumberSWChannels.bin');
Info.Signals(end).Tag         = verifKeyWord('NumberSWChannels');
  
Info.Signals(end+1).Name      = 'EchosounderDocIdentifier';
Info.Signals(end).Dimensions  = 'NbPings, 1';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'EchosounderDocIdentifier.bin');
Info.Signals(end).Tag         = verifKeyWord('EchosounderDocIdentifier');

Info.Signals(end+1).Name      = 'TransducerName';
Info.Signals(end).Dimensions  = 'NbPings, 1';
Info.Signals(end).Storage     = 'char';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'TransducerName.bin');
Info.Signals(end).Tag         = verifKeyWord('TransducerName');

Info.Signals(end+1).Name      = 'TransceiverSoftwareVersion';
Info.Signals(end).Dimensions  = 'NbPings, 1';
Info.Signals(end).Storage     = 'char';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'TransceiverSoftwareVersion.bin');
Info.Signals(end).Tag         = verifKeyWord('TransceiverSoftwareVersion');

Info.Signals(end+1).Name      = 'SoundSpeed';
Info.Signals(end).Dimensions  = 'NbPings, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'm/s';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'SoundSpeed.bin');
Info.Signals(end).Tag         = verifKeyWord('SounderSoundSpeed');

Info.Signals(end+1).Name      = 'TriggerMode';
Info.Signals(end).Dimensions  = 'NbPings, 1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'TriggerMode.bin');
Info.Signals(end).Tag         = verifKeyWord('TriggerMode');

Info.Signals(end+1).Name      = 'PingInterval';
Info.Signals(end).Dimensions  = 'NbPings, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 's';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'PingInterval.bin');
Info.Signals(end).Tag         = verifKeyWord('PingInterval');

Info.Signals(end+1).Name      = 'PulseMode';
Info.Signals(end).Dimensions  = 'NbPings, 1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'PulseMode.bin');
Info.Signals(end).Tag         = verifKeyWord('PulseMode');

Info.Signals(end+1).Name      = 'PulseDuration';
Info.Signals(end).Dimensions  = 'NbPings, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 's';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'PulseDuration.bin');
Info.Signals(end).Tag         = verifKeyWord('PulseDuration');

Info.Signals(end+1).Name      = 'TimeSampleInterval';
Info.Signals(end).Dimensions  = 'NbPings, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 's';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'TimeSampleInterval.bin');
Info.Signals(end).Tag         = verifKeyWord('TimeSampleInterval');

Info.Signals(end+1).Name      = 'FrequencyBeamSpacing';
Info.Signals(end).Dimensions  = 'NbPings, 1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'FrequencyBeamSpacing.bin');
Info.Signals(end).Tag         = verifKeyWord('FrequencyBeamSpacing');

Info.Signals(end+1).Name      = 'FrequencySpaceShape';
Info.Signals(end).Dimensions  = 'NbPings, 1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = 's';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'FrequencySpaceShape.bin');
Info.Signals(end).Tag         = verifKeyWord('FrequencySpaceShape');

Info.Signals(end+1).Name      = 'TransceiverPower';
Info.Signals(end).Dimensions  = 'NbPings, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'TransceiverPower.bin');
Info.Signals(end).Tag         = verifKeyWord('TransceiverPower');

Info.Signals(end+1).Name      = 'TransducerInstallationDepth';
Info.Signals(end).Dimensions  = 'NbPings, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'm';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'TransducerInstallationDepth.bin');
Info.Signals(end).Tag         = verifKeyWord('TransducerInstallationDepth');

Info.Signals(end+1).Name      = 'PlatformIdentifier';
Info.Signals(end).Dimensions  = 'NbPings, 1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = 's';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'PlatformIdentifier.bin');
Info.Signals(end).Tag         = verifKeyWord('PlatformIdentifier');

Info.Signals(end+1).Name      = 'TransducerShape';
Info.Signals(end).Dimensions  = 'NbPings, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 's';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'TransducerShape.bin');
Info.Signals(end).Tag         = verifKeyWord('TransducerShape');

Info.Signals(end+1).Name      = 'TransducerFaceAlongshipAngleOffset';
Info.Signals(end).Dimensions  = 'NbPings, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'TransducerFaceAlongshipAngleOffset.bin');
Info.Signals(end).Tag         = verifKeyWord('TransducerFaceAlongshipAngleOffset');

Info.Signals(end+1).Name      = 'TransducerFaceAthwartshipAngleOffset';
Info.Signals(end).Dimensions  = 'NbPings, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'TransducerFaceAthwartshipAngleOffset.bin');
Info.Signals(end).Tag         = verifKeyWord('TransducerFaceAthwartshipAngleOffset');

Info.Signals(end+1).Name      = 'TransducerRotationAngle';
Info.Signals(end).Dimensions  = 'NbPings, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'TransducerRotationAngle.bin');
Info.Signals(end).Tag         = verifKeyWord('TransducerRotationAngle');

% Conservation pour identification des tuples d�pendants.
DescSounder.NumberSWChannels            = Data.NumberSWChannels;
DescSounder.EchosounderDocIdentifier    = Data.EchosounderDocIdentifier;
DescSounder.TypeSounder                 = 2; % 1 : Mono-Faisceau, 2 : Multi-Faisceau
DescSounder.ModeSounder                 = 1; % 1 : Mono-Fr�quence, 2 : Multi-Fr�quence


%% Cr�ation du fichier XML d�crivant la donn�e 

nomFicXml = fullfile(nomDirSondeur, 'Ssc_EchoSounder.xml');
xml_write(nomFicXml, Info);
flag = exist(nomFicXml, 'file');
if ~flag
    messageErreur(nomFicXml)
    return
end

%% Cr�ation du r�pertoire ME70EchoSounder

nomDirSsc = fullfile(nomDirSondeur, 'Ssc_EchoSounder');
if ~exist(nomDirSsc, 'dir')
    status = mkdir(nomDirSsc);
    if ~status
        messageErreur(nomDirSsc)
        flag = 0;
        return
    end
end

%% Cr�ation des fichiers binaires des signaux

for i=1:length(Info.Signals)
    flag = writeSignal(nomDirSondeur, Info.Signals(i), Data.(Info.Signals(i).Name));
    if ~flag
        return
    end
end

flag = 1;

