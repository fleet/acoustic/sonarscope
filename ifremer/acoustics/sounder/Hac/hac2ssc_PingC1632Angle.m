function flag = hac2ssc_PingC1632Angle(nomDirRacine, iEchoSounder, SounderDesc, FormatVersion)

global DEBUG %#ok<GVMIS> 

nomFicXml = fullfile(nomDirRacine, ['HAC_Sounder' num2str(iEchoSounder)], 'hac_PingC1632Angle.xml');
SonarFmt2ssc_message(nomFicXml, '')

%% Lecture du fichier XML d�crivant la donn�e

flag = exist(nomFicXml, 'file');
if ~flag
    special_message('FichierNonExistant', nomFicXml)
    return
end

%% Cr�ation du r�pertoire SounderX

nomDirSondeur = fullfile(nomDirRacine, ['Ssc_Sounder' num2str(iEchoSounder)]);
if ~exist(nomDirSondeur, 'dir')
    status = mkdir(nomDirSondeur);
    if ~status
        messageErreur(nomDirSondeur)
        flag = 0;
        return
    end
end

% edit(nomFicXml)
Datagrams = xml_read(nomFicXml);
[nomDirSignal, nomFicSignal] = fileparts(nomFicXml);
nomDirSignal = fullfile(nomDirSignal, nomFicSignal);


%{
for i=1:length(Datagrams.Variables)
    fprintf(1,'%d  %s\n', i, Datagrams.Variables(i).Name)
end
%}

nbTuples = Datagrams.NbDatagrams;

% Lecture pr�alable des d�pendances avec les tuples Parents.
[flag, IndexTupleParent] = Read_BinVariable(Datagrams, nomDirSignal, 'IndexTupleParent', 'uint32');
if ~flag
    return
end
% figure; plot(IndexTupleParent); grid on; title('Data.IndexTupleParent');

[flag, IndexTupleRoot] = Read_BinVariable(Datagrams, nomDirSignal, 'IndexTupleRoot', 'uint32');
if ~flag
    return
end
% figure; plot(IndexTupleRoot); grid on; title('Data.IndexTupleRoot');



[flag, TupleSize] = Read_BinVariable(Datagrams, nomDirSignal, 'TupleSize', 'uint32'); %#ok<ASGLU>
if ~flag
    return
end
% figure; plot(TupleSize); grid on; title('TupleSize');

[flag, TupleType] = Read_BinVariable(Datagrams, nomDirSignal, 'TupleType', 'uint16'); %#ok<ASGLU>
if ~flag
    return
end
% figure; plot(TupleType); grid on; title('TupleType');

[flag, TimeFraction] = Read_BinVariable(Datagrams, nomDirSignal,  'TimeFraction', 'double');
if ~flag
    return
end
% figure; plot(TimeFraction); grid on; title('TimeFraction');

[flag, TimeCPU] = Read_BinVariable(Datagrams, nomDirSignal, 'TimeCPU', 'double');
if ~flag
    return
end
% figure; plot(TimeCPU); grid on; title('TimeCPU');
% Gestion des unicit�s des Temps. Les temps sont les cl�s d'association des
% tuples. Les num�ros de Pings par Sondeurs ne sont pas forc�ment corr�l�s
% entre eux.
TmpTimeTuple        = TimeCPU + TimeFraction;
[TimeTuple, ~, ~]   = unique(TmpTimeTuple);

[flag, SoftwareChannelIdentifier] = Read_BinVariable(Datagrams, nomDirSignal, 'SoftwareChannelIdentifier', 'uint16');
if ~flag
    return
end
% figure; plot(SoftwareChannelIdentifier); grid on; title('SoftwareChannelIdentifier');
flagDisorderChannel = 0;
X = diff(SoftwareChannelIdentifier);
sub = find(X > 1, 1);
if ~isempty(sub)
    % Cas du fichier PELGAS11_021_20110516_045049.hac notamment.
    flagDisorderChannel = 1;
end


[flag, TransceiverMode] = Read_BinVariable(Datagrams, nomDirSignal, 'TransceiverMode', 'uint16');
if ~flag
    return
end
% figure; plot(TransceiverMode); grid on; title('TransceiverMode');

[flag, TmpPingNumber] = Read_BinVariable(Datagrams, nomDirSignal, 'PingNumber', 'uint32');
if ~flag
    return
end
% figure; plot(TmpPingNumber, '-o'); grid on; title('TmpPingNumber');
flagDisorderPing = 0;
X = diff(int32(TmpPingNumber));
sub = find(X == -1, 1);
if ~isempty(sub)
    % Cas du fichier PELGAS11_021_20110516_045049.hac.
    flagDisorderPing = 1;
end



NbChannels(1:size(TmpPingNumber, 1), 1) = SounderDesc.DataSounder.NumberSWChannels(IndexTupleRoot);
Type    = SounderDesc.DataSounder.TypeSounder;
Mode    = SounderDesc.DataSounder.ModeSounder;
maxNbChannels   = max(NbChannels);
if SounderDesc.DataSounder.TypeSounder == 1
    nbBeams = 1;
else
    nbBeams  = maxNbChannels;
end
if SounderDesc.DataSounder.ModeSounder == 1
    nbFreq  = 1;
else
    nbFreq  = maxNbChannels;
end
% TODO : Sondeur Multi-Faisceau et Multi-Freq.



[flag, DetectedBottomRange] = Read_BinVariable(Datagrams, nomDirSignal, 'DetectedBottomRange', 'single');
if ~flag
    return
end
% figure; plot(DetectedBottomRange); grid on; title('DetectedBottomRange');

[flag, NbSamplesCompress] = Read_BinVariable(Datagrams, nomDirSignal, 'NbSamples', 'uint32');
if ~flag
    return
end
% figure; plot(Data.nbTuples); grid on; title('nbTuples');
% Boucle � r�aliser pour le cas o� le nb de channels change.



[PingNumber, ~]  = unique(TmpPingNumber);
% figure; plot(TmpPingNumber); grid on; title('TmpPingNumber');


[flag, TupleAttribute] = Read_BinVariable(Datagrams, nomDirSignal, 'TupleAttribute', 'int32'); %#ok<ASGLU>
if ~flag
    return
end
% figure; plot(TupleAttribute); grid on; title('TupleAttribute');

[flag, BackLink] = Read_BinVariable(Datagrams, nomDirSignal, 'BackLink', 'uint16'); %#ok<ASGLU>
if ~flag
    return
end
% figure; plot(BackLink); grid on; title('BackLink');

nbAngles       = sum(NbSamplesCompress, 'double');
identTable      = findIndVariable(Datagrams.Tables, 'CompressAngles');
% % % % [~, X]          = readSignal(nomDirSignal, Datagrams.Tables(identTable), nbAngles);
TableInfo       = Datagrams.Tables(identTable);
                            
% R�partition des �chantillons dans deux matrices Along et Athwart Angles.
MaxNbChannels              = max(NbChannels);
% Distribution du nombre �chantillons par Ping/Beam.
[MaxNbSamples, Data.NbSamples, DistribChannelByPing]  = distribNbAnglesFromHac(MaxNbChannels, TimeTuple, ...
                                        TmpTimeTuple, NbSamplesCompress, SoftwareChannelIdentifier);

% Ajout des NaN pour les Tuples manquants d'un Ping.
DistribChannelByPing                    = DistribChannelByPing';
DistribChannelByPing                    = DistribChannelByPing(:);
DistribTimeTuple                        = NaN(size(TimeTuple,1)*MaxNbChannels, 1, 'double');
DistribNbSamplesCompress                = NaN(size(PingNumber,1)*MaxNbChannels, 1);
subNotNaN                               = ~isnan(DistribChannelByPing);
DistribNbSamplesCompress(subNotNaN)     = NbSamplesCompress;
DistribTimeTuple(subNotNaN)             = TmpTimeTuple;


% R�partition des Samples sur une matrice d�pendantes sur type de sondeurs.
% On travaille en vecteur tant que les �chantillons ne sont pas d�compress�s.
[flag, Angles] = readCompressAnglesFromHac(Type, Mode, TableInfo, ...
                                nomDirSignal, MaxNbChannels,...
                                MaxNbSamples, nbAngles, DistribNbSamplesCompress, ...
                                DistribTimeTuple, DistribChannelByPing);
if ~flag
    return
end
Data.AlongShip      = Angles.AlongShip;
Data.AthwartShip    = Angles.AthwartShip;

%% Remise en ordre des Channels par double indexation Ping-Channels pour le
% cas o� les tuples ne sont pas enregistr�s sans l'ordre.

if flagDisorderChannel || flagDisorderPing
    % Calcul du tri nominal.
    % TmpPingNumber               = double(TmpPingNumber);    
    [~, subOrder]               = sort(DistribTimeTuple(subNotNaN));
    
    % figure; imagesc(reshape(SoftwareChannelIdentifier, maxNbChannels, size(PingNumber,1)));
    TimeCPU                     = TimeCPU(subOrder);
    TimeFraction                = TimeFraction(subOrder);
    DetectedBottomRange         = DetectedBottomRange(subOrder);
    TransceiverMode             = TransceiverMode(subOrder);
    IndexTupleParent            = IndexTupleParent(subOrder);
    IndexTupleRoot              = IndexTupleRoot(subOrder);
    TmpPingNumber               = TmpPingNumber(subOrder);
    % NbSamplesCompress           = NbSamplesCompress(subOrder);
end


%% Trac�s graphiques de debuggage.

if DEBUG
    if Type == 1 && Mode == 2 % MonoBeam et MultiFreq
        if MaxNbChannels == 1
            pppp = squeeze(Data.AlongShip(:, :));
            figure(10000); imagesc(pppp); title('Along Ship Angles'); axis xy; colorbar;
            pppp = squeeze(Data.AthwartShip(:, :));
            figure(15000); imagesc(pppp); title('Along Ship Angles'); axis xy; colorbar;
        else
            for b=1:MaxNbChannels
                pppp = squeeze(Data.AlongShip(:, :,b));
                figure(20000+b); imagesc(pppp); title(['Along Ship Angles - Freq' num2str(b)]); axis xy; colorbar;
%                 pppp = squeeze(Data.AthwartShip(:, :,b));
%                 figure(25000+b); imagesc(pppp); title(['Athwart Ship Angles - Freq' num2str(b)]); axis xy; colorbar;
                pause(0.1);
            end
        end
    elseif Type == 2 && Mode == 1 % MultiBeam et MonoFreq
        for p=1:50
            pppp = squeeze(Data.AlongShip(:, :,p));
            figure(30000); imagesc(pppp); title(['Along Ship Angles - Ping' num2str(p)]); axis xy; colorbar;
            pppp = squeeze(Data.AthwartShip(:, :,p));
            figure(35000); imagesc(pppp); title(['Athwart Ship Angles - Ping' num2str(p)]); axis xy; colorbar;
            pause(0.1);
        end
    end
end


%% Recopie dans la structure Data.
%% Reformattage des data pour le cas o� le nb de Samples n'est pas correct.
% En sortie, le nb de voies peut avoir �t� compl�t� par des NaN pour les Pings incomplets.

SoftwareChannelIdentifier = DistribChannelByPing;
% Cas des Pings incomplets : PELGAS11_011_20110506_105740.hac
nbPings = size(PingNumber,1);

% MaxNbChannels est g�n�rique (Nb Freq ou NbBeams)
if length(TimeCPU) < nbPings * MaxNbChannels
    subNotNaN                       = ~isnan(SoftwareChannelIdentifier);
    Data.TimeCPU                    = NaN(nbPings * MaxNbChannels, 1, 'double');
    Data.TimeFraction               = NaN(nbPings * MaxNbChannels, 1, 'double');             
    Data.DetectedBottomRange        = NaN(nbPings * MaxNbChannels, 1, 'double');        
    Data.SoftwareChannelIdentifier  = NaN(nbPings * MaxNbChannels, 1, 'double');   
    Data.TransceiverMode            = NaN(nbPings * MaxNbChannels, 1, 'double');            
    Data.IndexTupleParent           = NaN(nbPings * MaxNbChannels, 1, 'double');            
    Data.IndexTupleRoot             = NaN(nbPings * MaxNbChannels, 1, 'double');              
    Data.PingNumber                 = NaN(nbPings * MaxNbChannels, 1, 'double');
    Data.TimeCPU(subNotNaN)                     = TimeCPU;
    Data.TimeFraction(subNotNaN)                = TimeFraction;
    Data.DetectedBottomRange(subNotNaN)         = DetectedBottomRange;
    Data.TransceiverMode(subNotNaN)             = TransceiverMode;
    Data.IndexTupleParent(subNotNaN)            = IndexTupleParent;
    Data.IndexTupleRoot(subNotNaN)              = IndexTupleRoot;
    Data.PingNumber(subNotNaN)                  = TmpPingNumber;
    % Cas particulier
    Data.SoftwareChannelIdentifier              = SoftwareChannelIdentifier;
else
    %% Recopie dans la structure Data.

    Data.TimeCPU                    = TimeCPU;
    Data.TimeFraction               = TimeFraction;
    Data.DetectedBottomRange        = DetectedBottomRange;
    Data.SoftwareChannelIdentifier  = SoftwareChannelIdentifier;
    Data.TransceiverMode            = TransceiverMode;
    Data.IndexTupleParent           = IndexTupleParent;
    Data.IndexTupleRoot             = IndexTupleRoot;
    Data.PingNumber                 = TmpPingNumber;
end

%% Calcul des dates et heures :

T = Data.TimeCPU;
ansi_offset = datenum(1970,1,1,0,0,0);
matlab_time = T./86400 + ansi_offset + Data.TimeFraction/86400;
Data.TimeCPU = cl_time('timeMat', matlab_time);

%% G�n�ration du XML SonarScope

nomDirSsc = fullfile('Ssc_PingC1632Angle');

Info.Title                  = 'PingC1632Angle';
Info.Constructor            = 'Kongsberg';
Info.TimeOrigin             = '01/01/-4713';
Info.Comments               = 'PingC1632Angle Tuple';
Info.FormatVersion          = FormatVersion;

Info.Dimensions.nbTuples     = nbTuples;
Info.Dimensions.nbPings      = nbPings;
Info.Dimensions.nbBeams      = nbBeams;
Info.Dimensions.nbFreq       = nbFreq;
Info.Dimensions.nbSamples    = MaxNbSamples;

if Type == 1 && Mode == 2 % MonoBeam et MultiFreq
    DimsSignal1 = 'nbPings, nbFreq';
    DimsSignal2 = 'nbPings, nbFreq';
    DimsImages = 'nbPings, nbSamples, nbFreq';
elseif Type == 2 && Mode == 1 % MultiBeam et MonoFreq
    DimsSignal1 = 'nbPings, nbFreq';
    DimsSignal2 = 'nbPings, nbBeams';
    DimsImages = 'nbSamples, nbBeams, nbPings, nbFreq';
end

Info.Signals(1).Name          = 'TimeCPU';
Info.Signals(end).Dimensions  = DimsSignal2;
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = 'days since JC';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'TimeCPU.bin');
Info.Signals(end).Tag         = verifKeyWord('SounderTime');

Info.Signals(end+1).Name      = 'SoftwareChannelIdentifier';
Info.Signals(end).Dimensions  = DimsSignal1;
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'SoftwareChannelIdentifier.bin');
Info.Signals(end).Tag         = verifKeyWord('SoftwareChannelIdentifier');

Info.Signals(end+1).Name      = 'TransceiverMode';
Info.Signals(end).Dimensions  = DimsSignal1;
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'TransceiverMode.bin');
Info.Signals(end).Tag         = verifKeyWord('TransceiverMode');

Info.Signals(end+1).Name      = 'PingNumber';
Info.Signals(end).Dimensions  = DimsSignal1;
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'PingNumber.bin');
Info.Signals(end).Tag         = verifKeyWord('SounderPingCounter');

Info.Signals(end+1).Name      = 'DetectedBottomRange';
Info.Signals(end).Dimensions  = DimsSignal1;
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'm';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'DetectedBottomRange.bin');
Info.Signals(end).Tag         = verifKeyWord('DetectedBottomRange');

Info.Signals(end+1).Name      = 'NbSamples';
Info.Signals(end).Dimensions  = DimsSignal1;
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'NbSamples.bin');
Info.Signals(end).Tag         = verifKeyWord('SounderDetectionNbSamples');

Info.Signals(end+1).Name      = 'IndexTupleRoot';
Info.Signals(end).Dimensions  = DimsSignal1;
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'IndexTupleRoot.bin');
Info.Signals(end).Tag         = verifKeyWord('IndexTupleRoot');

Info.Signals(end+1).Name      = 'IndexTupleParent';
Info.Signals(end).Dimensions  = DimsSignal1;
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'IndexTupleParent.bin');
Info.Signals(end).Tag         = verifKeyWord('IndexTupleParent');

Info.Images(1).Name           = 'AthwartShip';
Info.Images(end).Dimensions   = DimsImages;
Info.Images(end).Storage      = 'double';
Info.Images(end).Unit         = 'deg';
Info.Images(end).FileName     = fullfile(nomDirSsc,'AthwartShip.bin');
Info.Images(end).Tag          = verifKeyWord('Sample');

Info.Images(end+1).Name       = 'AlongShip';
Info.Images(end).Dimensions   = DimsImages;
Info.Images(end).Storage      = 'double';
Info.Images(end).Unit         = 'deg';
Info.Images(end).FileName     = fullfile(nomDirSsc,'AlongShip.bin');
Info.Images(end).Tag          = verifKeyWord('Sample');

%% Cr�ation du fichier XML d�crivant la donn�e

nomFicXml = fullfile(nomDirSondeur, 'Ssc_PingC1632Angle.xml');
xml_write(nomFicXml, Info);
flag = exist(nomFicXml, 'file');
if ~flag
    messageErreur(nomFicXml)
    return
end

%% Cr�ation du r�pertoire PingC1632Angle

nomDirSsc = fullfile(nomDirSondeur, 'Ssc_PingC1632Angle');
if ~exist(nomDirSsc, 'dir')
    status = mkdir(nomDirSsc);
    if ~status
        messageErreur(nomDirSsc)
        flag = 0;
        return
    end
end

%% Cr�ation des fichiers binaires des signaux

for i=1:length(Info.Signals)
    flag = writeSignal(nomDirSondeur, Info.Signals(i), Data.(Info.Signals(i).Name));
    if ~flag
        return
    end
end

%% Cr�ation des fichiers binaires des images

for i=1:length(Info.Images)
    flag = writeImage(nomDirSondeur, Info.Images(i), Data.(Info.Images(i).Name));
    if ~flag
        return
    end
end

flag = 1;

