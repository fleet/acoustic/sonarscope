function flag = hac2ssc_PingC1632Angle_OLD(nomDirRacine, iEchoSounder, SounderDesc, FormatVersion)

nomFicXml = fullfile(nomDirRacine, ['hac_PingC1632Angle_' num2str(iEchoSounder) '.xml']);
SonarFmt2ssc_message(nomFicXml, '.hac')

%% Lecture du fichier XML décrivant la donnée

flag = exist(nomFicXml, 'file');
if ~flag
    special_message('FichierNonExistant', nomFicXml)
    return
end

% edit(nomFicXml)
Datagrams = xml_read(nomFicXml);
[nomDirSignal, nomFicSignal] = fileparts(nomFicXml);
nomDirSignal = fullfile(nomDirSignal, nomFicSignal);


%{
for i=1:length(Datagrams.Variables)
    fprintf(1,'%d  %s\n', i, Datagrams.Variables(i).Name)
end
%}

nbTuples = Datagrams.NbDatagrams;

[flag, PingNumber] = Read_BinVariable(Datagrams, nomDirSignal, 'PingNumber', 'uint32');
% figure; plot(Data.PingNumber); grid on; title('PingNumber');
if ~flag
    return
end
[Data.PingNumber, v] = unique(PingNumber);
AllPingNumber       = unique(PingNumber);


% Lecture préalable au tri des données de parenté du tuple.
[flag, Data.IndexTupleRoot] = Read_BinVariable(Datagrams, nomDirSignal, 'IndexTupleRoot', 'uint32');
if ~flag
    return
end
% figure; plot(Data.IndexTupleRoot); grid on; title('IndexTupleRoot');

[flag, Data.IndexTupleParent] = Read_BinVariable(Datagrams, nomDirSignal, 'IndexTupleParent', 'uint32');
if ~flag
    return
end
% figure; plot(Data.IndexTupleParent); grid on; title('IndexTupleParent');

% Lecture des Nb Channels par Tuple Courant.
if isfield(SounderDesc, 'Data901') && ~isempty(SounderDesc.Data901)
    NbChannels(1:size(PingNumber, 1), 1) = SounderDesc.Data901.NumberSWChannels(Data.IndexTupleRoot);
elseif isfield(SounderDesc, 'Data210') && ~isempty(SounderDesc.Data210)
    NbChannels(1:size(PingNumber, 1), 1) = SounderDesc.Data210.NumberSWChannels(Data.IndexTupleRoot);
elseif isfield(SounderDesc, 'Data220') && ~isempty(SounderDesc.Data220)
    NbChannels(1:size(PingNumber, 1), 1) = SounderDesc.Data220.NumberSWChannels(Data.IndexTupleRoot);
end

NbChannels = NbChannels(v);
% figure; plot(Data.PingNumber); grid on; title('PingNumber');
[flag, Data.TupleSize] = Read_BinVariable(Datagrams, nomDirSignal, 'TupleSize', 'uint32'); 
if ~flag
    return
end
% figure; plot(TupleSize); grid on; title('TupleSize');

[flag, Data.TupleType] = Read_BinVariable(Datagrams, nomDirSignal, 'TupleType', 'uint16'); 
if ~flag
    return
end
% figure; plot(TupleType); grid on; title('TupleType');

[flag, Data.TimeFraction] = Read_BinVariable(Datagrams, nomDirSignal,  'TimeFraction', 'double');
if ~flag
    return
end
% figure; plot(Data.TimeFraction); grid on; title('TimeFraction');

[flag, Data.TimeCPU] = Read_BinVariable(Datagrams, nomDirSignal, 'TimeCPU', 'double');
if ~flag
    return
end
% figure; plot(Data.TimeCPU); grid on; title('TimeCPU');

[flag, Data.SoftwareChannelIdentifier] = Read_BinVariable(Datagrams, nomDirSignal, 'SoftwareChannelIdentifier', 'uint16');
if ~flag
    return
end
% figure; plot(Data.SoftwareChannelIdentifier); grid on; title('SoftwareChannelIdentifier');

[flag, Data.TransceiverMode] = Read_BinVariable(Datagrams, nomDirSignal, 'TransceiverMode', 'uint16');
if ~flag
    return
end
% figure; plot(Data.TransceiverMode); grid on; title('TransceiverMode');


[flag, Data.DetectedBottomRange] = Read_BinVariable(Datagrams, nomDirSignal, 'DetectedBottomRange', 'int32');
if ~flag
    return
end
% figure; plot(Data.DetectedBottomRange); grid on; title('DetectedBottomRange');

[flag, NbSamplesCompress] = Read_BinVariable(Datagrams, nomDirSignal, 'NbSamples', 'uint32');
if ~flag
    return
end

[flag, TupleAttribute] = Read_BinVariable(Datagrams, nomDirSignal, 'TupleAttribute', 'int32'); %#ok<ASGLU>
if ~flag
    return
end
% figure; plot(TupleAttribute); grid on; title('TupleAttribute');

[flag, BackLink] = Read_BinVariable(Datagrams, nomDirSignal, 'BackLink', 'uint16'); %#ok<ASGLU>
if ~flag
    return
end
% figure; plot(BackLink); grid on; title('BackLink');


% Filtrage des Pings avec exclusion des Ping pour lesquels il manque un
% Tuple (cas du fichier PELGAS10_003_20100428_183857 - Tuple 10011 - Ping
% 2729).
dummy               = diff(v);
PingKO              = find(dummy~=NbChannels(1));
if ~isempty(PingKO)
    for i=1:size(PingKO,1)
        sub                     = (PingNumber == Data.PingNumber(PingKO+1));
        PingNumber              = PingNumber(~sub);
        pppp                    = NbSamplesCompress(sub);
        NbSamplesKO{PingKO(i)}  = pppp;
    end
    Data.PingNumber     = unique(PingNumber);
end

% Filtre sur les Pings qui ne sont corrects.
if ~isempty(PingKO)
    NbSamplesCompress                   = NbSamplesCompress(~sub);
    Data.TupleSize                      = Data.TupleSize(~sub);
    Data.TupleType                      = Data.TupleType(~sub);
    Data.TimeCPU                        = Data.TimeCPU(~sub);
    Data.TimeFraction                   = Data.TimeFraction(~sub);
    Data.SoftwareChannelIdentifier      = Data.SoftwareChannelIdentifier(~sub);
    Data.TransceiverMode                = Data.TransceiverMode(~sub);
    Data.DetectedBottomRange            = Data.DetectedBottomRange(~sub);
end
% Boucle à réaliser pour le cas où le nb de channels change.
NbSamplesCompress = reshape(NbSamplesCompress, [NbChannels(1) size(Data.PingNumber,1)]);

%% CompressSamples
% On lit les échantillons tel quel avant de les décompressser ici : on ne
% peut appliquer la lecture des Images car elle y traite le Scale Factor
% sur des valeurs qui ne sont pas encore décompressés.
nbSamples   = sum(sum(NbSamplesCompress));
identTable  = findIndVariable(Datagrams.Tables, 'CompressSamples');
[~, X]      = readSignal(nomDirSignal, Datagrams.Tables(identTable), nbSamples);


% Pour le Calcul du nombre max d'échantillons par Ping/Beam.
OffsetPing      = 0;
% Boucle sur les Ping identifiés y compris les KO pour pointer les
% échantillons respectifs dans CompressSamples.

for p=1:size(AllPingNumber,1)
    % Si le numéro de Ping est dans l'ensemble de Ping valides.
    if find(Data.PingNumber == AllPingNumber(p))
        % Calcul de l'Offset de l'échantillon correspondant au Ping en cours de
        % traitement.
        if p > 1
            OffsetPing = OffsetPing + sum(NbSamplesCompress(:,p-1));
        end
        NbXSamples                  = sum(NbSamplesCompress(:,p));
        XSamples                    = X((OffsetPing + 1): (OffsetPing+NbXSamples));

        % Décompression du paquet d'échantillons sélectionnés.
        [flag, ucNbSamples]         = unCompressNbSamples(NbSamplesCompress(:,p), XSamples); %#ok<ASGLU>
        Data.NbSamples(:,p)         = ucNbSamples;
    else
        % Calcul de l'Offset de l'échantillon correspondant au Ping
        % défaillant (p est supérieur au Ping défaillant car absent de
        % Data.PingNumber).
        if p > 1
            OffsetPing = OffsetPing + sum(NbSamplesKO{p-1});
        end        
    end
end


% Mise en forme des échantillons par Beam/Samples
OffsetPing      = 0;
% Mise en forme matricielle de la donnée du Ping.
MaxNbSamples    = max(max(Data.NbSamples));
Data.AlongShip  = NaN(size(Data.PingNumber,1), NbChannels(p), MaxNbSamples);
Data.AthwartShip  = NaN(size(Data.PingNumber,1), NbChannels(p), MaxNbSamples);
% Boucle sur les Ping identifiés y compris les KO pour pointer les
% échantillons respectifs dans CompressSamples.
for p=1:size(AllPingNumber,1)
    % Si le numéro de Ping est dans l'ensemble de Ping valides.
    if find(Data.PingNumber == AllPingNumber(p))
        % Calcul de l'Offset de l'échantillon correspondant au Ping en cours de
        % traitement.
        if p > 1
            OffsetPing = OffsetPing + sum(NbSamplesCompress(:,p-1));
        end
        NbXSamples   = sum(NbSamplesCompress(:,p));
        XSamples     = X((OffsetPing + 1): (OffsetPing+NbXSamples));

        % Décompression du paquet d'échantillons sélectionnés.
        [flag, ucX]  = unCompressSamples10011(XSamples); %#ok<ASGLU>

        OffsetSample = 0;

        for b=1:NbChannels(p)
            Data.AlongShip(p, b, 1:Data.NbSamples(b,p)) = ucX.AlongShip((OffsetSample+1):(Data.NbSamples(b,p)+OffsetSample));
            Data.AthwartShip(p, b, 1:Data.NbSamples(b,p)) = ucX.AthwartShip((OffsetSample+1):(Data.NbSamples(b,p)+OffsetSample));
            OffsetSample = OffsetSample + Data.NbSamples(b,p);
        end
        % Affichage de la donnée du Ping.
        pppp = squeeze(Data.AlongShip(p, :,:))*0.1;
        figure(11111); imagesc(pppp'); title(['Alongship Angles - Ping' num2str(p)]); axis xy; colorbar;
        pppp = squeeze(Data.AthwartShip(p, :,:))*0.1;
        figure(22222); imagesc(pppp'); title(['Athwartship Angles - Ping' num2str(p)]); axis xy; colorbar;
    end
end


%% Calcul des dates et heures :
T = double(Data.TimeCPU);
ansi_offset = datenum(1970,1,1,0,0,0);
matlab_time = T./86400 + ansi_offset + Data.TimeFraction/86400;
Data.TimeCPU = cl_time('timeMat', matlab_time);

%% Génération du XML SonarScope
nomDirSsc = ['Ssc_PingC1632Angle_' num2str(iEchoSounder)];

Info.Title                  = 'PingC1632Angle';
Info.Constructor            = 'Kongsberg';
Info.TimeOrigin             = '01/01/-4713';
Info.Comments               = 'PingC1632Angle Tuple';
Info.FormatVersion          = FormatVersion;

Info.Dimensions.nbTuples     = nbTuples;
Info.Dimensions.nbPing       = size(Data.PingNumber,2);
Info.Dimensions.nbBeams      = NbChannels;
% Info.Dimensions.nbSamples    = size(Data.NbSamples);

Info.Signals(1).Name          = 'TimeCPU';
Info.Signals(end).Dimensions  = 'nbTuples, 1';
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = 'days since JC';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'TimeCPU.bin');
Info.Signals(end).Tag         = verifKeyWord('SounderTime');

Info.Signals(end+1).Name      = 'SoftwareChannelIdentifier';
Info.Signals(end).Dimensions  = 'nbTuples, 1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'SoftwareChannelIdentifier.bin');
Info.Signals(end).Tag         = verifKeyWord('SoftwareChannelIdentifier');

Info.Signals(end+1).Name      = 'TransceiverMode';
Info.Signals(end).Dimensions  = 'nbTuples, 1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'TransceiverMode.bin');
Info.Signals(end).Tag         = verifKeyWord('TransceiverMode');

Info.Signals(end+1).Name      = 'PingNumber';
Info.Signals(end).Dimensions  = 'nbTuples, 1';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'PingNumber.bin');
Info.Signals(end).Tag         = verifKeyWord('SounderPingCounter');

Info.Signals(end+1).Name      = 'DetectedBottomRange';
Info.Signals(end).Dimensions  = 'nbTuples, 1';
Info.Signals(end).Storage     = 'int32';
Info.Signals(end).Unit        = 'm';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'DetectedBottomRange.bin');
Info.Signals(end).Tag         = verifKeyWord('DetectedBottomRange');

Info.Signals(end+1).Name      = 'NbSamples';
Info.Signals(end).Dimensions  = 'nbPings, nbBeams';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'NbSamples.bin');
Info.Signals(end).Tag         = verifKeyWord('SounderDetectionNbSamples');

Info.Signals(end+1).Name      = 'IndexTupleRoot';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'IndexTupleRoot.bin');
Info.Signals(end).Tag         = verifKeyWord('IndexTupleRoot');

Info.Signals(end+1).Name      = 'IndexTupleParent';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'IndexTupleParent.bin');
Info.Signals(end).Tag         = verifKeyWord('IndexTupleParent');

Info.Images(1).Name           = 'AthwartShip';
Info.Images(end).Dimensions   = 'nbPing, nbBeams, nbSamples';
Info.Images(end).Storage      = 'double';
Info.Images(end).Unit         = 'deg';
Info.Images(end).FileName     = fullfile(nomDirSsc,'AthwartShip.bin');
Info.Images(end).Tag          = verifKeyWord('Sample');

Info.Images(end+1).Name       = 'AlongShip';
Info.Images(end).Dimensions   = 'nbPing, nbBeams, nbSamples';
Info.Images(end).Storage      = 'double';
Info.Images(end).Unit         = 'deg';
Info.Images(end).FileName     = fullfile(nomDirSsc,'AlongShip.bin');
Info.Images(end).Tag          = verifKeyWord('Sample');

%% Création du fichier XML décrivant la donnée

nomFicXml = fullfile(nomDirRacine, ['Ssc_PingC1632Angle_' num2str(iEchoSounder) '.xml']);
xml_write(nomFicXml, Info);
flag = exist(nomFicXml, 'file');
if ~flag
    messageErreur(nomFicXml)
    return
end

%% Création du répertoire PingC1632Angle

nomDirSsc = fullfile(nomDirRacine, ['Ssc_PingC1632Angle_' num2str(iEchoSounder)]);
if ~exist(nomDirSsc, 'dir')
    status = mkdir(nomDirSsc);
    if ~status
        messageErreur(nomDirSsc)
        flag = 0;
        return
    end
end

%% Création des fichiers binaires des signaux

for i=1:length(Info.Signals)
    flag = writeSignal(nomDirRacine, Info.Signals(i), Data.(Info.Signals(i).Name));
    if ~flag
        return
    end
end

%% Création des fichiers binaires des images
%
% for i=1:length(Info.Images)
%     flag = writeImage(nomDirRacine, Info.Images(i), Data.(Info.Images(i).Name));
%     if ~flag
%         return
%     end
% end

flag = 1;

