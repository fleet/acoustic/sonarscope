function flag = hac2ssc_PingC16Amplitude_OLD(nomDirRacine, iEchoSounder, SounderDesc, FormatVersion)

nomFicXml = fullfile(nomDirRacine, ['hac_PingC16Amplitude_' num2str(iEchoSounder) '.xml']);
SonarFmt2ssc_message(nomFicXml, '.hac')

%% Lecture du fichier XML décrivant la donnée

flag = exist(nomFicXml, 'file');
if ~flag
    special_message('FichierNonExistant', nomFicXml)
    return
end

% edit(nomFicXml)
Datagrams = xml_read(nomFicXml);
[nomDirSignal, nomFicSignal] = fileparts(nomFicXml);
nomDirSignal = fullfile(nomDirSignal, nomFicSignal);


%{
for i=1:length(Datagrams.Variables)
    fprintf(1,'%d  %s\n', i, Datagrams.Variables(i).Name)
end
%}

nbTuples = Datagrams.NbDatagrams;

% Lecture préalable des dépendances avec les tuples Parents.
[flag, IndexTupleParent] = Read_BinVariable(Datagrams, nomDirSignal, 'IndexTupleParent', 'uint32');
if ~flag
    return
end
% figure; plot(IndexTupleParent); grid on; title('Data.IndexTupleParent');

[flag, IndexTupleRoot] = Read_BinVariable(Datagrams, nomDirSignal, 'IndexTupleRoot', 'uint32');
if ~flag
    return
end
% figure; plot(IndexTupleRoot); grid on; title('Data.IndexTupleRoot');



[flag, TupleSize] = Read_BinVariable(Datagrams, nomDirSignal, 'TupleSize', 'uint32'); %#ok<ASGLU>
if ~flag
    return
end
% figure; plot(TupleSize); grid on; title('TupleSize');

[flag, TupleType] = Read_BinVariable(Datagrams, nomDirSignal, 'TupleType', 'uint16'); %#ok<ASGLU>
if ~flag
    return
end
% figure; plot(TupleType); grid on; title('TupleType');

[flag, TimeFraction] = Read_BinVariable(Datagrams, nomDirSignal,  'TimeFraction', 'double');
if ~flag
    return
end
% figure; plot(TimeFraction); grid on; title('TimeFraction');

[flag, TimeCPU] = Read_BinVariable(Datagrams, nomDirSignal, 'TimeCPU', 'double');
if ~flag
    return
end
% figure; plot(TimeCPU); grid on; title('TimeCPU');

[flag, SoftwareChannelIdentifier] = Read_BinVariable(Datagrams, nomDirSignal, 'SoftwareChannelIdentifier', 'uint16');
if ~flag
    return
end
% figure; plot(SoftwareChannelIdentifier); grid on; title('SoftwareChannelIdentifier');
flagDisorderChannel = 0;
X = diff(SoftwareChannelIdentifier);
sub = find(X > 1, 1);
if ~isempty(sub)
%     % Cas du fichier PELGAS11_021_20110516_045049.hac.
%     [~, file, ~] = fileparts(nomDirRacine);
%     str = ['TODO JMA - Attention : Fichier ' file '.hac repéré avec non-succession dans l''enregistrement des voies. ' ...
%            'Observer la fréquence de ces fichiers et mettre en place un traitement sur l''index des Channels si besoin'];
%     my_warndlg(str, 0);
    flagDisorderChannel = 1;
end


[flag, TransceiverMode] = Read_BinVariable(Datagrams, nomDirSignal, 'TransceiverMode', 'uint16');
if ~flag
    return
end
% figure; plot(TransceiverMode); grid on; title('TransceiverMode');

[flag, TmpPingNumber] = Read_BinVariable(Datagrams, nomDirSignal, 'PingNumber', 'uint32');
if ~flag
    return
end
% figure; plot(TmpPingNumber); grid on; title('TmpPingNumber');


NbChannels(1:size(TmpPingNumber, 1), 1) = SounderDesc.DataSounder.NumberSWChannels(IndexTupleRoot);
Type    = SounderDesc.DataSounder.TypeSounder;
Mode    = SounderDesc.DataSounder.ModeSounder;
maxNbChannels   = max(NbChannels);
if SounderDesc.DataSounder.TypeSounder == 1
    NbBeams = 1;
else
    NbBeams  = maxNbChannels;
end
if SounderDesc.DataSounder.ModeSounder == 1
    NbFreq  = 1;
else
    NbFreq  = maxNbChannels;
end
% TODO : Sondeur Multi-Faisceau et Multi-Freq.
    
  
    
[flag, DetectedBottomRange] = Read_BinVariable(Datagrams, nomDirSignal, 'DetectedBottomRange', 'single');
if ~flag
    return
end
% figure; plot(DetectedBottomRange); grid on; title('DetectedBottomRange');

[flag, NbSamplesCompress] = Read_BinVariable(Datagrams, nomDirSignal, 'NbSamples', 'uint32');
if ~flag
    return
end
% figure; plot(Data.nbTuples); grid on; title('nbTuples');
% Boucle à réaliser pour le cas où le nb de channels change.



[PingNumber, ~]  = unique(TmpPingNumber);
% figure; plot(TmpPingNumber); grid on; title('TmpPingNumber');


NTheorique = maxNbChannels * size(PingNumber,1);
if length(NbSamplesCompress) ~= NTheorique
    % Identification du(des) paquet(s) de tuple ne disposant pas du nombre complet
    % de channels (faisceaux).
    pppp    = my_hist(single(TmpPingNumber), min(single(TmpPingNumber)): max(single(TmpPingNumber)));
    sub     = find(pppp ~= maxNbChannels);
    % figure; plot(pppp)
    for i=1:size(sub,2)        
        x           = find(TmpPingNumber ==  min(single(TmpPingNumber))-1+sub(i));
        % Un Ping peut être complètement absent.
        % Cas du fichier PELGAS11_016_20110511_202704.hac
        if ~isempty(x)         
            % Récupération des Index de Tuple Parent pour le Ping identifié comme incomplet.
            % OLD le 10/10/11 : idTupleParent = single(IndexTupleParent((min(x):max(x))));
                        
            idTupleParent = NaN(1, maxNbChannels);
            for k=1:size(x,1)
                idTupleParent(x(k)-min(x)+1) = IndexTupleParent(x(k));
            end
            % Complément des Index Tuples au cas où il manque les derniers tuples d'une série. 
            % OLD le 10/10/11 : idTupleParent(end+1:maxNbChannels)   = NaN;
              % Utiliser histcounts
            pppp        = my_hist(idTupleParent, 1: single(maxNbChannels));
            subParent   = find(pppp == 0);
            if (subParent(1) ~=1)
                lastTupleOK = subParent(1)-2+min(x);
            else
                lastTupleOK = (min(x)-1)+ 1;
            end
            for p=1:size(subParent, 2)
                % L'échantillon à compléter si situe en : min(x) + idTupleParent
                % Manquant -1.
                TimeCPU                     = [TimeCPU(1:min(x)+subParent(p)-2);TimeCPU(lastTupleOK);TimeCPU(min(x)+subParent(p)-1:end)];  
                TimeFraction                = [TimeFraction(1:min(x)+subParent(p)-2);TimeFraction(lastTupleOK);TimeFraction(min(x)+subParent(p)-1:end)];  
                DetectedBottomRange         = [DetectedBottomRange(1:min(x)+subParent(p)-2);DetectedBottomRange(lastTupleOK);DetectedBottomRange(min(x)+subParent(p)-1:end)];  
                SoftwareChannelIdentifier  	= [SoftwareChannelIdentifier(1:min(x)+subParent(p)-2);SoftwareChannelIdentifier(lastTupleOK);SoftwareChannelIdentifier(min(x)+subParent(p)-1:end)];  
                TransceiverMode             = [TransceiverMode(1:min(x)+subParent(p)-2);TransceiverMode(lastTupleOK);TransceiverMode(min(x)+subParent(p)-1:end)];  
                IndexTupleParent            = [IndexTupleParent(1:min(x)+subParent(p)-2);IndexTupleParent(lastTupleOK);IndexTupleParent(min(x)+subParent(p)-1:end)];
                IndexTupleRoot              = [IndexTupleRoot(1:min(x)+subParent(p)-2);IndexTupleRoot(lastTupleOK);IndexTupleRoot(min(x)+subParent(p)-1:end)];
                TmpPingNumber               = [TmpPingNumber(1:min(x)+subParent(p)-2);TmpPingNumber(lastTupleOK);TmpPingNumber(min(x)+subParent(p)-1:end)];

                NbSamplesCompress           = [NbSamplesCompress(1:min(x)+subParent(p)-2);NaN;NbSamplesCompress(min(x)+subParent(p)-1:end)];
            end
        end
    end
end



[flag, TupleAttribute] = Read_BinVariable(Datagrams, nomDirSignal, 'TupleAttribute', 'int32'); %#ok<ASGLU>
if ~flag
    return
end
% figure; plot(TupleAttribute); grid on; title('TupleAttribute');

[flag, BackLink] = Read_BinVariable(Datagrams, nomDirSignal, 'BackLink', 'uint16'); %#ok<ASGLU>
if ~flag
    return
end
% figure; plot(BackLink); grid on; title('BackLink');


NbSamplesCompress = reshape(NbSamplesCompress, [maxNbChannels size(PingNumber,1)]);

%% CompressSamples
% On lit les échantillons tel quel avant de les décompressser ici : on ne
% peut appliquer la lecture des Images car elle y traite le Scale Factor
% sur des valeurs qui ne sont pas encore décompressés.

nbSamples   = sum(sum(NbSamplesCompress));
identTable  = findIndVariable(Datagrams.Tables, 'CompressSamples');
% Pas de lecture possible via Read_Bintable car maintien en int16
% nécessaire de la valeur (valeur -32768 à traiter).
[~, X]      = readSignal(nomDirSignal, Datagrams.Tables(identTable), nbSamples);


% Calcul du nombre max d'échantillons par Ping/Beam.
OffsetPing = 0;
for p=1:size(PingNumber,1)
    % Calcul de l'Offset de l'échantillon correspondant au Ping en cours de
    % traitement.
    if p > 1
        OffsetPing = OffsetPing + sum(NbSamplesCompress(:,p-1));
    end
    NbXSamples = sum(NbSamplesCompress(:,p));
    XSamples   = X((OffsetPing + 1): (OffsetPing+NbXSamples));
    
    % Décompression du paquet d'échantillons sélectionnés.
    [flag, ucNbSamples] = unCompressNbSamples(NbSamplesCompress(:,p), XSamples); %#ok<ASGLU>
    Data.NbSamples(p,:) = ucNbSamples;
    
end

% Mise en forme des échantillons par Beam/Samples
OffsetPing      = 0;
% Mise en forme matricielle de la donnée du Ping.
MaxNbSamples    = max(max(Data.NbSamples));
%NEW Data.Samples    = NaN(size(PingNumber,1), NbChannels(p), MaxNbSamples);
if Type == 1 && Mode == 2 % MonoBeam et MultiFreq
    Data.Samples    = NaN(size(PingNumber,1), MaxNbSamples,  NbChannels(p));
elseif Type == 2 && Mode == 1 % MultiBeam et MonoFreq
    Data.Samples    = NaN(MaxNbSamples, NbChannels(p), size(PingNumber,1));
end

for p=1:size(PingNumber,1)
    % Calcul de l'Offset de l'échantillon correspondant au Ping en cours de
    % traitement.
    if p > 1
        OffsetPing = OffsetPing + sum(NbSamplesCompress(:,p-1));
    end
    NbXSamples = sum(NbSamplesCompress(:,p));
    XSamples   = X((OffsetPing + 1): (OffsetPing+NbXSamples));
    
    % Décompression du paquet d'échantillons sélectionnés.
    [flag, ucX]  = unCompressSamples10040(XSamples); %#ok<ASGLU>
    ucX = code2val(ucX, Datagrams.Tables(identTable), 'single');
    
    OffsetSample = 0;
    for b=1:NbChannels(p)
        %OLD Data.Samples(p, b, 1:Data.NbSamples(b,p)) = ucX((OffsetSample+1):(Data.NbSamples(b,p)+OffsetSample));
        if Type == 1 && Mode == 2 % MonoBeam et MultiFreq
            Data.Samples(p, 1:Data.NbSamples(p,b), b) = ucX((OffsetSample+1):(Data.NbSamples(p,b)+OffsetSample));
        elseif Type == 2 && Mode == 1 % MultiBeam et MonoFreq
            Data.Samples(1:Data.NbSamples(p,b), b, p) = ucX((OffsetSample+1):(Data.NbSamples(p,b)+OffsetSample));
        end
        OffsetSample = OffsetSample + Data.NbSamples(p,b);
    end
    % Affichage de la donnée du Ping.
    %OLD pppp = squeeze(Data.Samples(p, :,:))*0.01;
    % pppp = squeeze(Data.Samples(p, :,:))*0.01;
    %OLD figure(11111); imagesc(pppp'); title(['UnCompressSamples - Ping' num2str(p)]); axis xy; colorbar;
    % figure(11111); imagesc(pppp); title(['UnCompressSamples - Ping' num2str(p)]); axis xy; colorbar;
end


if flagDisorderChannel
    % Remise en ordre des Channels par double indexation Ping-Channels pour le
    % cas où les tuples ne sont pas enregistrés n° de channels
    TmpPingNumber               = double(TmpPingNumber);
    SoftwareChannelIdentifier   = double(SoftwareChannelIdentifier);
    NbChannels                  = double(NbChannels);
    minTmpPingNumber            = min(TmpPingNumber);
    IndexPingChannel            = (TmpPingNumber - minTmpPingNumber) * max(NbChannels) + double(SoftwareChannelIdentifier - min(SoftwareChannelIdentifier));
    
    [~, subOrder] = sort(IndexPingChannel);
    
    SoftwareChannelIdentifier   = SoftwareChannelIdentifier(subOrder);
    % figure; imagesc(reshape(SoftwareChannelIdentifier, maxNbChannels, size(PingNumber,1)));
    TimeCPU                     = TimeCPU(subOrder);
    TimeFraction                = TimeFraction(subOrder);
    DetectedBottomRange         = DetectedBottomRange(subOrder);
    TransceiverMode             = TransceiverMode(subOrder);
    IndexTupleParent            = IndexTupleParent(subOrder);
    IndexTupleRoot              = IndexTupleRoot(subOrder);
    TmpPingNumber               = TmpPingNumber(subOrder);
    % NbSamplesCompress           = NbSamplesCompress(subOrder);
    
    
    %% Mise en forme des images selon le tri opéré précédemment.
    % Cas du Monofaisceau et Multi-Fréquence.
    if SounderDesc.DataSounder.TypeSounder == 1 && SounderDesc.DataSounder.ModeSounder == 2
        X = Data.Samples;
        X = permute(X, [3 1 2]);
        sz = size(X);
        X = reshape(X, [sz(1)*sz(2) sz(3)]);
        X = X(subOrder, :);
        X = reshape(X, sz);
        X = permute(X, [2 3 1]);
        Data.Samples = X;
%     for n=1:maxNbChannels
%         figure; imagesc(squeeze(Data.Samples(:,:,n)));
%     end
    elseif SounderDesc.DataSounder.TypeSounder == 2 && SounderDesc.DataSounder.ModeSounder == 1
        X = Data.Samples;
        X = permute(X, [3 2 1]);
        sz = size(X);
        X = reshape(X, [sz(1)*sz(2) sz(3)]);
        X = X(subOrder, :);
        X = reshape(X, sz);
        X = permute(X, [3 2 1]);
        Data.Samples = X;
    end
end
%% Recopie dans la structure Data.

Data.TimeCPU                    = TimeCPU;
Data.TimeFraction               = TimeFraction;
Data.DetectedBottomRange        = DetectedBottomRange;
Data.SoftwareChannelIdentifier  = SoftwareChannelIdentifier;
Data.TransceiverMode            = TransceiverMode;
Data.IndexTupleParent           = IndexTupleParent;
Data.IndexTupleRoot             = IndexTupleRoot;
Data.PingNumber                 = TmpPingNumber;



%% Calcul des dates et heures :

T = Data.TimeCPU;
ansi_offset = datenum(1970,1,1,0,0,0);
matlab_time = T./86400 + ansi_offset + Data.TimeFraction/86400;
Data.TimeCPU = cl_time('timeMat', matlab_time);

%% Génération du XML SonarScope
nomDirSsc = ['Ssc_PingC16Amplitude_' num2str(iEchoSounder)];

Info.Title                  = 'PingC16Amplitude';
Info.Constructor            = 'Kongsberg';
Info.TimeOrigin             = '01/01/-4713';
Info.Comments               = 'PingC16Amplitude Tuple';
Info.FormatVersion          = FormatVersion;

Info.Dimensions.nbTuples     = nbTuples;
Info.Dimensions.nbPings      = size(PingNumber,1);
Info.Dimensions.nbBeams      = NbBeams;
Info.Dimensions.nbFreq       = NbFreq;
Info.Dimensions.nbSamples    = MaxNbSamples;

if Type == 1 && Mode == 2 % MonoBeam et MultiFreq
    DimsSignal1 = 'nbPings, nbFreq';
    DimsSignal2 = 'nbPings, nbFreq';
    DimsImages = 'nbPings, nbSamples, nbFreq';
elseif Type == 2 && Mode == 1 % MultiBeam et MonoFreq
    DimsSignal1 = 'nbPings, nbFreq';
    DimsSignal2 = 'nbPings, nbBeams';
    DimsImages = 'nbSamples, nbBeams, nbPings, nbFreq';
end

Info.Signals(1).Name          = 'TimeCPU';
Info.Signals(end).Dimensions  = DimsSignal2;
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = 'days since JC';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'TimeCPU.bin');
Info.Signals(end).Tag         = verifKeyWord('SounderTime');

Info.Signals(end+1).Name      = 'SoftwareChannelIdentifier';
Info.Signals(end).Dimensions  = DimsSignal1;
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'SoftwareChannelIdentifier.bin');
Info.Signals(end).Tag         = verifKeyWord('SoftwareChannelIdentifier');

Info.Signals(end+1).Name      = 'TransceiverMode';
Info.Signals(end).Dimensions  = DimsSignal1;
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'TransceiverMode.bin');
Info.Signals(end).Tag         = verifKeyWord('TransceiverMode');

Info.Signals(end+1).Name      = 'PingNumber';
Info.Signals(end).Dimensions  = DimsSignal1;
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'PingNumber.bin');
Info.Signals(end).Tag         = verifKeyWord('SounderPingCounter');

Info.Signals(end+1).Name      = 'DetectedBottomRange';
Info.Signals(end).Dimensions  = DimsSignal1;
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'm';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'DetectedBottomRange.bin');
Info.Signals(end).Tag         = verifKeyWord('DetectedBottomRange');

Info.Signals(end+1).Name      = 'NbSamples';
Info.Signals(end).Dimensions  = DimsSignal1;
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'NbSamples.bin');
Info.Signals(end).Tag         = verifKeyWord('SounderDetectionNbSamples');

Info.Signals(end+1).Name      = 'IndexTupleRoot';
Info.Signals(end).Dimensions  = DimsSignal1;
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'IndexTupleRoot.bin');
Info.Signals(end).Tag         = verifKeyWord('IndexTupleRoot');

Info.Signals(end+1).Name      = 'IndexTupleParent';
Info.Signals(end).Dimensions  = DimsSignal1;
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'IndexTupleParent.bin');
Info.Signals(end).Tag         = verifKeyWord('IndexTupleParent');

Info.Images(1).Name           = 'Samples';
Info.Images(end).Dimensions   = DimsImages;
Info.Images(end).Storage      = 'single';
Info.Images(end).Unit         = 'dB';
Info.Images(end).FileName     = fullfile(nomDirSsc,'Samples.bin');
Info.Images(end).Tag          = verifKeyWord('Sample');
% 
%% Création du fichier XML décrivant la donnée

nomFicXml = fullfile(nomDirRacine, ['Ssc_PingC16Amplitude_' num2str(iEchoSounder) '.xml']);
xml_write(nomFicXml, Info);
flag = exist(nomFicXml, 'file');
if ~flag
    messageErreur(nomFicXml)
    return
end

%% Création du répertoire PingC16Amplitude

nomDirSsc = fullfile(nomDirRacine, ['Ssc_PingC16Amplitude_' num2str(iEchoSounder)]);
if ~exist(nomDirSsc, 'dir')
    status = mkdir(nomDirSsc);
    if ~status
        messageErreur(nomDirSsc)
        flag = 0;
        return
    end
end

%% Création des fichiers binaires des signaux

for i=1:length(Info.Signals)
    flag = writeSignal(nomDirRacine, Info.Signals(i), Data.(Info.Signals(i).Name));
    if ~flag
        return
    end
end

%% Création des fichiers binaires des images

for i=1:length(Info.Images)
    flag = writeImage(nomDirRacine, Info.Images(i), Data.(Info.Images(i).Name));
    if ~flag
        return
    end
end

flag = 1;

