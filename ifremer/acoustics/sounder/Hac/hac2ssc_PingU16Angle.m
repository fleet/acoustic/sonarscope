function flag = hac2ssc_PingU16Angle(nomDirRacine, iEchoSounder, SounderDesc, FormatVersion)

global DEBUG %#ok<GVMIS> 

nomFicXml = fullfile(nomDirRacine, ['HAC_Sounder' num2str(iEchoSounder)], 'hac_PingU16Angle.xml');
SonarFmt2ssc_message(nomFicXml, '')

%% Lecture du fichier XML d�crivant la donn�e

flag = exist(nomFicXml, 'file');
if ~flag
    special_message('FichierNonExistant', nomFicXml)
    return
end

%% Cr�ation du r�pertoire SounderX

nomDirSondeur = fullfile(nomDirRacine, ['Ssc_Sounder' num2str(iEchoSounder)]);
if ~exist(nomDirSondeur, 'dir')
    status = mkdir(nomDirSondeur);
    if ~status
        messageErreur(nomDirSondeur)
        flag = 0;
        return
    end
end

% edit(nomFicXml)
Datagrams = xml_read(nomFicXml);
[nomDirSignal, nomFicSignal] = fileparts(nomFicXml);
nomDirSignal = fullfile(nomDirSignal, nomFicSignal);


%{
for i=1:length(Datagrams.Variables)
    fprintf(1,'%d  %s\n', i, Datagrams.Variables(i).Name)
end
%}

nbTuples = Datagrams.NbDatagrams;

% Lecture pr�alable des d�pendances avec les tuples Parents.
[flag, IndexTupleParent] = Read_BinVariable(Datagrams, nomDirSignal, 'IndexTupleParent', 'uint32');
if ~flag
    return
end
% figure; plot(IndexTupleParent); grid on; title('Data.IndexTupleParent');

[flag, IndexTupleRoot] = Read_BinVariable(Datagrams, nomDirSignal, 'IndexTupleRoot', 'uint32');
if ~flag
    return
end
% figure; plot(IndexTupleRoot); grid on; title('Data.IndexTupleRoot');



[flag, TupleSize] = Read_BinVariable(Datagrams, nomDirSignal, 'TupleSize', 'uint32'); %#ok<ASGLU>
if ~flag
    return
end
% figure; plot(TupleSize); grid on; title('TupleSize');

[flag, TupleType] = Read_BinVariable(Datagrams, nomDirSignal, 'TupleType', 'uint16'); %#ok<ASGLU>
if ~flag
    return
end
% figure; plot(TupleType); grid on; title('TupleType');

[flag, TimeCPU] = Read_BinVariable(Datagrams, nomDirSignal, 'TimeCPU', 'double');
if ~flag
    return
end
% figure; plot(TimeCPU); grid on; title('TimeCPU');

[flag, TimeFraction] = Read_BinVariable(Datagrams, nomDirSignal,  'TimeFraction', 'double');
if ~flag
    return
end
% figure; plot(TimeFraction); grid on; title('TimeFraction');



% Gestion des unicit�s des Temps. Les temps sont les cl�s d'association des
% tuples. Les num�ros de Pings par Sondeurs ne sont pas forc�ment corr�l�s
% entre eux.
TmpTimeTuple        = TimeCPU + TimeFraction;
[TimeTuple, ~, ~]   = unique(TmpTimeTuple);

[flag, SoftwareChannelIdentifier] = Read_BinVariable(Datagrams, nomDirSignal, 'SoftwareChannelIdentifier', 'uint16');
if ~flag
    return
end
% figure; plot(SoftwareChannelIdentifier); grid on; title('SoftwareChannelIdentifier');
flagDisorderChannel = 0;
X = diff(SoftwareChannelIdentifier);
sub = find(abs(X) > 1, 1);
if ~isempty(sub)
    % Cas du fichier PELGAS11_021_20110516_045049.hac notamment.
    flagDisorderChannel = 1;
end


[flag, TransceiverMode] = Read_BinVariable(Datagrams, nomDirSignal, 'TransceiverMode', 'uint16');
if ~flag
    return
end
% figure; plot(TransceiverMode); grid on; title('TransceiverMode');

[flag, TmpPingNumber] = Read_BinVariable(Datagrams, nomDirSignal, 'PingNumber', 'uint32');
if ~flag
    return
end


% figure; plot(TmpPingNumber, '-o'); grid on; title('TmpPingNumber');
flagDisorderPing = 0;
X = diff(int32(TmpPingNumber));
sub = find(abs(X) > 1, 1);
if ~isempty(sub)
    % Cas du fichier PELGAS11_021_20110516_045049.hac.
    flagDisorderPing = 1;
end



NbChannels(1:size(TmpPingNumber, 1), 1) = SounderDesc.DataSounder.NumberSWChannels(IndexTupleRoot);
Type    = SounderDesc.DataSounder.TypeSounder;
Mode    = SounderDesc.DataSounder.ModeSounder;
maxNbChannels   = max(NbChannels);
if SounderDesc.DataSounder.TypeSounder == 1
    nbBeams = 1;
else
    nbBeams  = maxNbChannels;
end
if SounderDesc.DataSounder.ModeSounder == 1
    nbFreq  = 1;
else
    nbFreq  = maxNbChannels;
end
% TODO : Sondeur Multi-Faisceau et Multi-Freq.



[flag, DetectedBottomRange] = Read_BinVariable(Datagrams, nomDirSignal, 'DetectedBottomRange', 'single');
if ~flag
    return
end
% figure; plot(DetectedBottomRange); grid on; title('DetectedBottomRange');

[PingNumber, ~]  = unique(TmpPingNumber);
% figure; plot(TmpPingNumber); grid on; title('TmpPingNumber');

[flag, TupleAttribute] = Read_BinVariable(Datagrams, nomDirSignal, 'TupleAttribute', 'int32'); %#ok<ASGLU>
if ~flag
    return
end
% figure; plot(TupleAttribute); grid on; title('TupleAttribute');

[flag, BackLink] = Read_BinVariable(Datagrams, nomDirSignal, 'BackLink', 'uint16');
if ~flag
    return
end
% figure; plot(BackLink); grid on; title('BackLink');

  
[flag, nbSamples] = Read_BinVariable(Datagrams, nomDirSignal, 'NbSamples', 'single');
if ~flag
    return
end
% figure; plot(DetectedBottomRange); grid on; title('DetectedBottomRange');
Type    = SounderDesc.DataSounder.TypeSounder;
Mode    = SounderDesc.DataSounder.ModeSounder;
MaxnbSamples = max(max(nbSamples));
SumNbSamples = sum(nbSamples, 'double');

% [flag, SequenceNumberAngles] = Read_BinTable(Datagrams, nomDirSignal, 'SequenceNumberAngles', 'single', SumNbSamples);
% if ~flag
%     return
% end
% figure; plot(SequenceNumberAngles); grid on; title('SequenceNumberAngles');

nbPings = size(PingNumber,1);
[flag, AlongShip] = Read_BinTable(Datagrams, nomDirSignal, 'AlongShipPhaseAngle', 'single', SumNbSamples);
if ~flag
    return
end
% figure; plot(DetectedBottomRange); grid on; title('DetectedBottomRange');
[flag, AthwartShip] = Read_BinTable(Datagrams, nomDirSignal, 'AthwartShipPhaseAngle', 'single', SumNbSamples);
if ~flag
    return
end
% figure; plot(DetectedBottomRange); grid on; title('DetectedBottomRange');



if isscalar(unique(nbSamples))
    % Cas des HAC g�n�r�s via SSC depuis les RAW (o� toutes les Pings font
    % la m�me taille)
    Data.AlongShip = reshape(AlongShip, MaxnbSamples, maxNbChannels, nbPings);
    Data.AlongShip = permute(Data.AlongShip, [3 1 2]);
    % Cas des HAC g�n�r�s via SSC depuis les RAW (o� toutes les Pings font
    % la m�me taille)
    Data.AthwartShip = reshape(AthwartShip, MaxnbSamples, maxNbChannels, nbPings);
    Data.AthwartShip = permute(Data.AthwartShip, [3 1 2]);
    
    DistribChannelByPing = reshape(SoftwareChannelIdentifier,nbPings,maxNbChannels);
else
    % Distribution du nombre �chantillons par Ping/Beam.
    [MaxnbSamples, DistribChannelByPing, Angles]  = distribAnglesFromHac(Type,Mode, ...
                                                        maxNbChannels, TimeTuple, ...
                                                        TmpTimeTuple, nbSamples, ...
                                                        AlongShip, AthwartShip, ...
                                                        SoftwareChannelIdentifier);
    Data.AlongShip      = Angles.Along;
    Data.AthwartShip    = Angles.Athwart;
    
end

% Ajout des NaN pour les Tuples manquants d'un Ping.
DistribTimeTuple                        = NaN(size(TimeTuple,1)*maxNbChannels, 1, 'double');
subNotNaN                               = ~isnan(DistribChannelByPing(:));
DistribTimeTuple(subNotNaN)             = TmpTimeTuple;

%% Remise en ordre des Channels par double indexation Ping-Channels pour le
% cas o� les tuples ne sont pas enregistr�s sans l'ordre.

if flagDisorderChannel || flagDisorderPing
    % Calcul du tri nominal.
    % TmpPingNumber               = double(TmpPingNumber);    
    subNotNaN                   = ~isnan(DistribTimeTuple);
    [~, subOrder]               = sort(DistribTimeTuple(subNotNaN));
    
    % figure; imagesc(reshape(SoftwareChannelIdentifier, maxNbChannels, size(PingNumber,1)));
    TimeCPU                     = TimeCPU(subOrder);
    TimeFraction                = TimeFraction(subOrder);
    DetectedBottomRange         = DetectedBottomRange(subOrder);
    TransceiverMode             = TransceiverMode(subOrder);
    IndexTupleParent            = IndexTupleParent(subOrder);
    IndexTupleRoot              = IndexTupleRoot(subOrder);
    TmpPingNumber               = TmpPingNumber(subOrder);
    % NbSamplesCompress           = NbSamplesCompress(subOrder);
end


%% Trac�s graphiques de debuggage.

if DEBUG
    if Type == 1 && Mode == 2 % MonoBeam et MultiFreq
        if maxNbChannels == 1
            pppp = squeeze(Data.AlongShip(:, :));
            figure(10000); imagesc(pppp); title('Along Ship Angles'); axis xy; colorbar;
            pppp = squeeze(Data.AthwartShip(:, :));
            figure(15000); imagesc(pppp); title('Along Ship Angles'); axis xy; colorbar;
        else
            for b=1:maxNbChannels
                pppp = squeeze(Data.AlongShip(:, :,b));
                figure(20000+b); imagesc(pppp); title(['Along Ship Angles - Freq' num2str(b)]); axis xy; colorbar;
%                 pppp = squeeze(Data.AthwartShip(:, :,b));
%                 figure(25000+b); imagesc(pppp); title(['Athwart Ship Angles - Freq' num2str(b)]); axis xy; colorbar;
                pause(0.1);
            end
        end
    elseif Type == 2 && Mode == 1 % MultiBeam et MonoFreq
        for p=1:50
            pppp = squeeze(Data.AlongShip(:, :,p));
            figure(30000); imagesc(pppp); title(['Along Ship Angles - Ping' num2str(p)]); axis xy; colorbar;
            pppp = squeeze(Data.AthwartShip(:, :,p));
            figure(35000); imagesc(pppp); title(['Athwart Ship Angles - Ping' num2str(p)]); axis xy; colorbar;
            pause(0.1);
        end
    end
end


%% Recopie dans la structure Data.
%% Reformattage des data pour le cas o� le nb de Samples n'est pas correct.
% En sortie, le nb de voies peut avoir �t� compl�t� par des NaN pour les Pings incomplets.


% MaxNbChannels est g�n�rique (Nb Freq ou NbBeams)
if length(TimeCPU) < nbPings * maxNbChannels
    subNotNaN                       = ~isnan(SoftwareChannelIdentifier);
    Data.TimeCPU                    = NaN(nbPings * maxNbChannels, 1, 'double');
    Data.TimeFraction               = NaN(nbPings * maxNbChannels, 1, 'double');             
    Data.DetectedBottomRange        = NaN(nbPings * maxNbChannels, 1, 'double');        
    Data.SoftwareChannelIdentifier  = NaN(nbPings * maxNbChannels, 1, 'double');   
    Data.TransceiverMode            = NaN(nbPings * maxNbChannels, 1, 'double');            
    Data.IndexTupleParent           = NaN(nbPings * maxNbChannels, 1, 'double');            
    Data.IndexTupleRoot             = NaN(nbPings * maxNbChannels, 1, 'double');              
    Data.PingNumber                 = NaN(nbPings * maxNbChannels, 1, 'double');
    Data.TimeCPU(subNotNaN)                     = TimeCPU;
    Data.TimeFraction(subNotNaN)                = TimeFraction;
    Data.DetectedBottomRange(subNotNaN)         = DetectedBottomRange;
    Data.TransceiverMode(subNotNaN)             = TransceiverMode;
    Data.IndexTupleParent(subNotNaN)            = IndexTupleParent;
    Data.IndexTupleRoot(subNotNaN)              = IndexTupleRoot;
    Data.PingNumber(subNotNaN)                  = TmpPingNumber;
    % Cas particulier
    Data.SoftwareChannelIdentifier              = SoftwareChannelIdentifier;
else
    %% Recopie dans la structure Data.

    Data.TimeCPU                    = TimeCPU;
    Data.TimeFraction               = TimeFraction;
    Data.DetectedBottomRange        = DetectedBottomRange;
    Data.SoftwareChannelIdentifier  = SoftwareChannelIdentifier;
    Data.TransceiverMode            = TransceiverMode;
    Data.IndexTupleParent           = IndexTupleParent;
    Data.IndexTupleRoot             = IndexTupleRoot;
    Data.PingNumber                 = TmpPingNumber;
end

%% Calcul des dates et heures :

T = Data.TimeCPU;
ansi_offset = datenum(1970,1,1,0,0,0);
matlab_time = T./86400 + ansi_offset + Data.TimeFraction/86400;
Data.TimeCPU = cl_time('timeMat', matlab_time);

%% G�n�ration du XML SonarScope

nomDirSsc = fullfile('Ssc_PingU16Angle');

Info.Title                  = 'PingU16Angle';
Info.Constructor            = 'Kongsberg';
Info.TimeOrigin             = '01/01/-4713';
Info.Comments               = 'PingU16Angle Tuple';
Info.FormatVersion          = FormatVersion;

Info.Dimensions.nbTuples     = nbTuples;
Info.Dimensions.nbPings      = nbPings;
Info.Dimensions.nbBeams      = nbBeams;
Info.Dimensions.nbFreq       = nbFreq;
Info.Dimensions.nbSamples    = MaxnbSamples;

if Type == 1 && Mode == 2 % MonoBeam et MultiFreq
    DimsSignal1 = 'nbPings, nbFreq';
    DimsSignal2 = 'nbPings, nbFreq';
    DimsImages = 'nbPings, nbSamples, nbFreq';
elseif Type == 2 && Mode == 1 % MultiBeam et MonoFreq
    DimsSignal1 = 'nbPings, nbFreq';
    DimsSignal2 = 'nbPings, nbBeams';
    DimsImages = 'nbSamples, nbBeams, nbPings, nbFreq';
end

Info.Signals(1).Name          = 'TimeCPU';
Info.Signals(end).Dimensions  = DimsSignal2;
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = 'days since JC';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'TimeCPU.bin');
Info.Signals(end).Tag         = verifKeyWord('SounderTime');

Info.Signals(end+1).Name      = 'SoftwareChannelIdentifier';
Info.Signals(end).Dimensions  = DimsSignal1;
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'SoftwareChannelIdentifier.bin');
Info.Signals(end).Tag         = verifKeyWord('SoftwareChannelIdentifier');

Info.Signals(end+1).Name      = 'TransceiverMode';
Info.Signals(end).Dimensions  = DimsSignal1;
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'TransceiverMode.bin');
Info.Signals(end).Tag         = verifKeyWord('TransceiverMode');

Info.Signals(end+1).Name      = 'PingNumber';
Info.Signals(end).Dimensions  = DimsSignal1;
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'PingNumber.bin');
Info.Signals(end).Tag         = verifKeyWord('SounderPingCounter');

Info.Signals(end+1).Name      = 'DetectedBottomRange';
Info.Signals(end).Dimensions  = DimsSignal1;
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'm';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'DetectedBottomRange.bin');
Info.Signals(end).Tag         = verifKeyWord('DetectedBottomRange');

Info.Signals(end+1).Name      = 'IndexTupleRoot';
Info.Signals(end).Dimensions  = DimsSignal1;
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'IndexTupleRoot.bin');
Info.Signals(end).Tag         = verifKeyWord('IndexTupleRoot');

Info.Signals(end+1).Name      = 'IndexTupleParent';
Info.Signals(end).Dimensions  = DimsSignal1;
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'IndexTupleParent.bin');
Info.Signals(end).Tag         = verifKeyWord('IndexTupleParent');

Info.Images(1).Name           = 'AlongShip';
Info.Images(end).Dimensions   = DimsImages;
Info.Images(end).Storage      = 'double';
Info.Images(end).Unit         = 'deg';
Info.Images(end).FileName     = fullfile(nomDirSsc,'AlongShip.bin');
Info.Images(end).Tag          = verifKeyWord('AlongShipPhase');

Info.Images(end+1).Name       = 'AthwartShip';
Info.Images(end).Dimensions   = DimsImages;
Info.Images(end).Storage      = 'double';
Info.Images(end).Unit         = 'deg';
Info.Images(end).FileName     = fullfile(nomDirSsc,'AthwartShip.bin');
Info.Images(end).Tag          = verifKeyWord('AthwartShipPhase');

%% Cr�ation du fichier XML d�crivant la donn�e

nomFicXml = fullfile(nomDirSondeur, 'Ssc_PingU16Angle.xml');
xml_write(nomFicXml, Info);
flag = exist(nomFicXml, 'file');
if ~flag
    messageErreur(nomFicXml)
    return
end

%% Cr�ation du r�pertoire PingU16Angle

nomDirSsc = fullfile(nomDirSondeur, 'Ssc_PingU16Angle');
if ~exist(nomDirSsc, 'dir')
    status = mkdir(nomDirSsc);
    if ~status
        messageErreur(nomDirSsc)
        flag = 0;
        return
    end
end

%% Cr�ation des fichiers binaires des signaux

for i=1:length(Info.Signals)
    flag = writeSignal(nomDirSondeur, Info.Signals(i), Data.(Info.Signals(i).Name));
    if ~flag
        return
    end
end

%% Cr�ation des fichiers binaires des images

for i=1:length(Info.Images)
    flag = writeImage(nomDirSondeur, Info.Images(i), Data.(Info.Images(i).Name));
    if ~flag
        return
    end
end

flag = 1;

