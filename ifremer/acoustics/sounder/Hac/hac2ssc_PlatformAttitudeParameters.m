function flag = hac2ssc_PlatformAttitudeParameters(nomDirRacine, iEchoSounder, FormatVersion)

nomFicXml = fullfile(nomDirRacine, ['HAC_Sounder' num2str(iEchoSounder)], 'hac_PlatformAttitudeParameters.xml');
SonarFmt2ssc_message(nomFicXml, '')

%% Lecture du fichier XML d�crivant la donn�e 

flag = exist(nomFicXml, 'file');
if ~flag
    special_message('FichierNonExistant', nomFicXml)
    return
end

%% Cr�ation du r�pertoire SounderX
nomDirSondeur = fullfile(nomDirRacine, ['Ssc_Sounder' num2str(iEchoSounder)]);
if ~exist(nomDirSondeur, 'dir')
    status = mkdir(nomDirSondeur);
    if ~status
        messageErreur(nomDirSondeur)
        flag = 0;
        return
    end
end


% edit(nomFicXml)
Datagrams = xml_read(nomFicXml);
[nomDirSignal, nomFicSignal] = fileparts(nomFicXml);
nomDirSignal = fullfile(nomDirSignal, nomFicSignal);


%{
for i=1:length(Datagrams.Variables)
    fprintf(1,'%d  %s\n', i, Datagrams.Variables(i).Name)
end
%}


nbSamples = Datagrams.NbDatagrams;

[flag, TupleSize] = Read_BinVariable(Datagrams, nomDirSignal, 'TupleSize', 'uint32'); %#ok<ASGLU>
if ~flag
    return
end
% figure; plot(TupleSize); grid on; title('TupleSize');

[flag, TupleType] = Read_BinVariable(Datagrams, nomDirSignal, 'TupleType', 'uint16'); %#ok<ASGLU>
if ~flag
    return
end
% figure; plot(TupleType); grid on; title('TupleType');

[flag, Data.TimeFraction] = Read_BinVariable(Datagrams, nomDirSignal,  'TimeFraction', 'double');
if ~flag
    return
end
% figure; plot(Data.TimeFraction); grid on; title('TimeFraction');

[flag, Data.TimeCPU] = Read_BinVariable(Datagrams, nomDirSignal, 'TimeCPU', 'double');
if ~flag
    return
end
% figure; plot(Data.TimeCPU); grid on; title('TimeCPU');

[flag, Data.AttitudeSensorId] = Read_BinVariable(Datagrams, nomDirSignal, 'AttitudeSensorId', 'uint16');
if ~flag
    return
end
% figure; plot(Data.AttitudeSensorId); grid on; title('AttitudeSensorId');

[flag, Data.TransceiverChannelNumber] = Read_BinVariable(Datagrams, nomDirSignal, 'TransceiverChannelNumber', 'uint16');
if ~flag
    return
end
% figure; plot(Data.TransceiverChannelNumber); grid on; title('TransceiverChannelNumber');

[flag, Data.PlatformType] = Read_BinVariable(Datagrams, nomDirSignal, 'PlatformType', 'uint16');
if ~flag
    return
end
% figure; plot(Data.PlatformType); grid on; title('PlatformType');

[flag, Data.AlongOffset] = Read_BinVariable(Datagrams, nomDirSignal, 'AlongOffset', 'single');
if ~flag
    return
end
% figure; plot(Data.AlongOffset); grid on; title('AlongOffset');

[flag, Data.AthwartOffset] = Read_BinVariable(Datagrams, nomDirSignal, 'AthwartOffset', 'single');
if ~flag
    return
end
% figure; plot(Data.AthwartOffset); grid on; title('AthwartOffset');

[flag, Data.ElevationOffset] = Read_BinVariable(Datagrams, nomDirSignal, 'ElevationOffset', 'single');
if ~flag
    return
end
% figure; plot(Data.ElevationOffset); grid on; title('ElevationOffset');

[flag, Data.Remarks] = Read_BinVariable(Datagrams, nomDirSignal, 'Remarks', 'char');
if ~flag
    return
end

[flag, TupleAttribute] = Read_BinVariable(Datagrams, nomDirSignal, 'TupleAttribute', 'int32'); %#ok<ASGLU>
if ~flag
    return
end
% figure; plot(TupleAttribute); grid on; title('TupleAttribute');

[flag, BackLink] = Read_BinVariable(Datagrams, nomDirSignal, 'BackLink', 'uint16');
if ~flag
    return
end
% figure; plot(BackLink); grid on; title('BackLink');

% Lecture pr�alable des d�pendances avec les tuples Parents.
[flag, Data.IndexTupleParent] = Read_BinVariable(Datagrams, nomDirSignal, 'IndexTupleParent', 'uint32');
if ~flag
    return
end
% figure; plot(Data.IndexTupleParent); grid on; title('Data.IndexTupleParent');

[flag, Data.IndexTupleRoot] = Read_BinVariable(Datagrams, nomDirSignal, 'IndexTupleRoot', 'uint32');
if ~flag
    return
end
% figure; plot(Data.IndexTupleRoot); grid on; title('Data.IndexTupleRoot');

%% Calcul des dates et heures :
T = double(Data.TimeCPU);
ansi_offset = datenum(1970,1,1,0,0,0);
matlab_time = T./86400 + ansi_offset + Data.TimeFraction/86400; 
Data.Time = cl_time('timeMat', matlab_time);

%% G�n�ration du XML SonarScope
nomDirSsc = fullfile('Ssc_PlatformAttitudeParameters');

Info.Title                  = 'PlatformAttitudeParameters';
Info.Constructor            = 'Kongsberg';
Info.TimeOrigin             = '01/01/-4713';
Info.Comments               = 'Platform Attitude Parameters Tuple';
Info.FormatVersion          = FormatVersion;

Info.Dimensions.NbSamples     = nbSamples;

Info.Signals(1).Name          = 'TimeCPU';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = 'days since JC';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'TimeCPU.bin');
Info.Signals(end).Tag         = verifKeyWord('SounderTime');
  
Info.Signals(end+1).Name      = 'AttitudeSensorId';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'AttitudeSensorId.bin');
Info.Signals(end).Tag         = verifKeyWord('AttitudeSensorId');

Info.Signals(end+1).Name      = 'TransceiverChannelNumber';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'TransceiverChannelNumber.bin');
Info.Signals(end).Tag         = verifKeyWord('TransceiverChannelNumber');

Info.Signals(end+1).Name      = 'PlatformType';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'PlatformType.bin');
Info.Signals(end).Tag         = verifKeyWord('PlatformType');

Info.Signals(end+1).Name      = 'AlongOffset';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'm';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'AlongOffset.bin');
Info.Signals(end).Tag         = verifKeyWord('AlongOffset');

Info.Signals(end+1).Name      = 'AthwartOffset';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'm';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'AthwartOffset.bin');
Info.Signals(end).Tag         = verifKeyWord('AthwartOffset');

Info.Signals(end+1).Name      = 'ElevationOffset';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'm';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'ElevationOffset.bin');
Info.Signals(end).Tag         = verifKeyWord('ElevationOffset');

Info.Signals(end+1).Name      = 'IndexTupleRoot';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'IndexTupleRoot.bin');
Info.Signals(end).Tag         = verifKeyWord('IndexTupleRoot');

Info.Signals(end+1).Name      = 'IndexTupleParent';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile(nomDirSsc,'IndexTupleParent.bin');
Info.Signals(end).Tag         = verifKeyWord('IndexTupleParent');
               
%% Cr�ation du fichier XML d�crivant la donn�e 

nomFicXml = fullfile(nomDirSondeur, 'Ssc_PlatformAttitudeParameters.xml');
xml_write(nomFicXml, Info);
flag = exist(nomFicXml, 'file');
if ~flag
    messageErreur(nomFicXml)
    return
end

%% Cr�ation du r�pertoire PlatformAttitudeParameters

nomDirSsc = fullfile(nomDirSondeur, 'Ssc_PlatformAttitudeParameters');
if ~exist(nomDirSsc, 'dir')
    status = mkdir(nomDirSsc);
    if ~status
        messageErreur(nomDirSsc)
        flag = 0;
        return
    end
end

%% Cr�ation des fichiers binaires des signaux

for i=1:length(Info.Signals)
    flag = writeSignal(nomDirSondeur, Info.Signals(i), Data.(Info.Signals(i).Name));
    if ~flag
        return
    end
end

flag = 1;

