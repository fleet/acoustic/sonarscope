function flag = hac2ssc_Position(nomDirRacine, FormatVersion)

nomFicXml = fullfile(nomDirRacine, 'hac_Position.xml');
SonarFmt2ssc_message(nomFicXml, '.hac')

%% Lecture du fichier XML d�crivant la donn�e 

flag = exist(nomFicXml, 'file');
if ~flag
    special_message('FichierNonExistant', nomFicXml)
    return
end

% edit(nomFicXml)
Datagrams = xml_read(nomFicXml);
[nomDirSignal, nomFicSignal] = fileparts(nomFicXml);
nomDirSignal = fullfile(nomDirSignal, nomFicSignal);


%{
for i=1:length(Datagrams.Variables)
    fprintf(1,'%d  %s\n', i, Datagrams.Variables(i).Name)
end
%}


nbSamples = Datagrams.NbDatagrams;

[flag, TupleSize] = Read_BinVariable(Datagrams, nomDirSignal, 'TupleSize', 'uint32'); %#ok<ASGLU>
if ~flag
    return
end
% figure; plot(TupleSize); grid on; title('TupleSize');

[flag, TupleType] = Read_BinVariable(Datagrams, nomDirSignal, 'TupleType', 'uint16'); %#ok<ASGLU>
if ~flag
    return
end
% figure; plot(TupleType); grid on; title('TupleType');

[flag, Data.TimeFraction] = Read_BinVariable(Datagrams, nomDirSignal,  'TimeFraction', 'double');
if ~flag
    return
end
% figure; plot(Data.TimeFraction); grid on; title('TimeFraction');

[flag, Data.TimeCPU] = Read_BinVariable(Datagrams, nomDirSignal, 'TimeCPU', 'double');
if ~flag
    return
end
% figure; plot(Data.TimeCPU); grid on; title('TimeCPU');

[flag, Data.TimeGPS] = Read_BinVariable(Datagrams, nomDirSignal, 'TimeGPS', 'double');
if ~flag
    return
end
% figure; plot(Data.TimeGPS); grid on; title('TimeGPS');

[flag, Data.PositSystem] = Read_BinVariable(Datagrams, nomDirSignal, 'PositSystem', 'uint16');
if ~flag
    return
end
% figure; plot(Data.PositSystem); grid on; title('PositSystem');

[flag, Data.Latitude] = Read_BinVariable(Datagrams, nomDirSignal, 'Latitude', 'double');
if ~flag
    return
end
Data.Latitude(Data.Latitude == 0) = NaN;
% figure; plot(Data.Latitude); grid on; title('Latitude');

[flag, Data.Longitude] = Read_BinVariable(Datagrams, nomDirSignal, 'Longitude', 'double');
if ~flag
    return
end
Data.Longitude(Data.Longitude == 0) = NaN; % C'est arriv� sur fichier \PELGAS11_002_20110427_072328
% figure; plot(Data.Longitude); grid on; title('Longitude');

[flag, TupleAttribute] = Read_BinVariable(Datagrams, nomDirSignal, 'TupleAttribute', 'int32'); %#ok<ASGLU>
if ~flag
    return
end
% figure; plot(TupleAttribute); grid on; title('TupleAttribute');

[flag, BackLink] = Read_BinVariable(Datagrams, nomDirSignal, 'BackLink', 'uint16');
if ~flag
    return
end
% figure; plot(BackLink); grid on; title('BackLink');

%% Calcul des dates et heures :
T = double(Data.TimeCPU);
ansi_offset = datenum(1970,1,1,0,0,0);
matlab_time = T./86400 + ansi_offset + Data.TimeFraction/86400; 
Data.TimeCPU = cl_time('timeMat', matlab_time);

%% G�n�ration du XML SonarScope

Info.Title                  = 'Position';
Info.Constructor            = 'Kongsberg';
% % % % Info.EmModel                = Datagrams.Model;
% % % % Info.SystemSerialNumber     = Datagrams.SystemSerialNumber;
Info.TimeOrigin             = '01/01/-4713';
Info.Comments               = 'Position Tuple';
Info.FormatVersion          = FormatVersion;

Info.Dimensions.NbSamples     = nbSamples;

Info.Signals(1).Name          = 'TimeCPU';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = 'days since JC';
Info.Signals(end).FileName    = fullfile('Ssc_Position','TimeCPU.bin');
Info.Signals(end).Tag         = verifKeyWord('SounderTime');
  
Info.Signals(end+1).Name      = 'TimeGPS';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 's';
Info.Signals(end).FileName    = fullfile('Ssc_Position','TimeGPS.bin');
Info.Signals(end).Tag         = verifKeyWord('GPSTime');

Info.Signals(end+1).Name      = 'PositSystem';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_Position','PositSystem.bin');
Info.Signals(end).Tag         = verifKeyWord('PositSystem');

Info.Signals(end+1).Name      = 'Latitude';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = 'nd';
Info.Signals(end).FileName    = fullfile('Ssc_Position','Latitude.bin');
Info.Signals(end).Tag         = verifKeyWord('SounderLatitude');

Info.Signals(end+1).Name      = 'Longitude';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = 'nd';
Info.Signals(end).FileName    = fullfile('Ssc_Position','Longitude.bin');
Info.Signals(end).Tag         = verifKeyWord('SounderLongitude');

               
%% Cr�ation du fichier XML d�crivant la donn�e 

nomFicXml = fullfile(nomDirRacine, 'Ssc_Position.xml');
xml_write(nomFicXml, Info);
flag = exist(nomFicXml, 'file');
if ~flag
    messageErreur(nomFicXml)
    return
end

%% Cr�ation du r�pertoire Position

nomDirSsc = fullfile(nomDirRacine, 'Ssc_Position');
if ~exist(nomDirSsc, 'dir')
    status = mkdir(nomDirSsc);
    if ~status
        messageErreur(nomDirSsc)
        flag = 0;
        return
    end
end

%% Cr�ation des fichiers binaires des signaux

for i=1:length(Info.Signals)
    flag = writeSignal(nomDirRacine, Info.Signals(i), Data.(Info.Signals(i).Name));
    if ~flag
        return
    end
end

flag = 1;

