function flag = hac2ssc_STDProfile(nomDirRacine, FormatVersion)

nomFicXml = fullfile(nomDirRacine, 'hac_STDProfile.xml');
SonarFmt2ssc_message(nomFicXml, '.hac')

%% Lecture du fichier XML d�crivant la donn�e 

flag = exist(nomFicXml, 'file');
if ~flag
    special_message('FichierNonExistant', nomFicXml)
    return
end

% edit(nomFicXml)
Datagrams = xml_read(nomFicXml);
[nomDirSignal, nomFicSignal] = fileparts(nomFicXml);
nomDirSignal = fullfile(nomDirSignal, nomFicSignal);


%{
for i=1:length(Datagrams.Variables)
    fprintf(1,'%d  %s\n', i, Datagrams.Variables(i).Name)
end
%}


nbProfiles = Datagrams.NbDatagrams;

[flag, TupleSize] = Read_BinVariable(Datagrams, nomDirSignal, 'TupleSize', 'uint32'); %#ok<ASGLU>
if ~flag
    return
end
% figure; plot(TupleSize); grid on; title('TupleSize');

[flag, TupleType] = Read_BinVariable(Datagrams, nomDirSignal, 'TupleType', 'uint16'); %#ok<ASGLU>
if ~flag
    return
end
% figure; plot(TupleType); grid on; title('TupleType');

[flag, Data.TimeFraction] = Read_BinVariable(Datagrams, nomDirSignal,  'TimeFraction', 'double');
if ~flag
    return
end
% figure; plot(Data.TimeFraction); grid on; title('TimeFraction');

[flag, Data.TimeCPU] = Read_BinVariable(Datagrams, nomDirSignal, 'TimeCPU', 'double');
if ~flag
    return
end
% figure; plot(Data.TimeCPU); grid on; title('TimeCPU');

[flag, Data.SensorType] = Read_BinVariable(Datagrams, nomDirSignal, 'SensorType', 'uint16');
if ~flag
    return
end
% figure; plot(Data.SensorType); grid on; title('SensorType');

[flag, Data.NumberOfMeasurements] = Read_BinVariable(Datagrams, nomDirSignal, 'NumberOfMeasurements', 'single');
if ~flag
    return
end
% figure; plot(Data.NumberOfMeasurements); grid on; title('NumberOfMeasurements');
nbPoints = max(Data.NumberOfMeasurements);

[flag, TupleAttribute] = Read_BinVariable(Datagrams, nomDirSignal, 'TupleAttribute', 'int32'); %#ok<ASGLU>
if ~flag
    return
end
% figure; plot(TupleAttribute); grid on; title('TupleAttribute');

[flag, BackLink] = Read_BinVariable(Datagrams, nomDirSignal, 'BackLink', 'uint16');
if ~flag
    return
end
% figure; plot(BackLink); grid on; title('BackLink');

%% Pressure
[flag, X] = Read_BinTable(Datagrams, nomDirSignal, 'Pressure', 'single', nbPoints);
if ~flag
    return
end
Data.Pressure = X;
% figure; imagesc(Data.Pressure); title('Pressure'); axis xy; colorbar;

%% Temperature
[flag, X] = Read_BinTable(Datagrams, nomDirSignal, 'Temperature', 'single', nbPoints);
if ~flag
    return
end
Data.Temperature = X;
% figure; imagesc(Data.Temperature); title('Temperature'); axis xy; colorbar;

%% Conductivity
[flag, X] = Read_BinTable(Datagrams, nomDirSignal, 'Conductivity', 'single', nbPoints);
if ~flag
    return
end
Data.Conductivity = X;
% figure; imagesc(Data.Conductivity); title('Conductivity'); axis xy; colorbar;

%% SoundVelocity
[flag, X] = Read_BinTable(Datagrams, nomDirSignal, 'SoundVelocity', 'single', nbPoints);
if ~flag
    return
end
Data.SoundVelocity = X;
% figure; imagesc(Data.SoundVelocity); title('SoundVelocity'); axis xy; colorbar;

%% Depth
[flag, X] = Read_BinTable(Datagrams, nomDirSignal, 'Depth', 'single', nbPoints);
if ~flag
    return
end
Data.Depth = X;
% figure; imagesc(Data.Depth); title('Depth'); axis xy; colorbar;

%% Salinity
[flag, X] = Read_BinTable(Datagrams, nomDirSignal, 'Salinity', 'single', nbPoints);
if ~flag
    return
end
Data.Salinity = X;
% figure; imagesc(Data.Salinity); title('Salinity'); axis xy; colorbar;

%% Absorption
[flag, X] = Read_BinTable(Datagrams, nomDirSignal, 'Absorption', 'single', nbPoints);
if ~flag
    return
end
Data.Absorption = X;
% figure; imagesc(Data.Absorption); title('Absorption'); axis xy; colorbar;

%% Calcul des dates et heures :
T = double(Data.TimeCPU);
ansi_offset = datenum(1970,1,1,0,0,0);
matlab_time = T./86400 + ansi_offset + Data.TimeFraction/86400; 
Data.TimeCPU = cl_time('timeMat', matlab_time);

%% G�n�ration du XML SonarScope

Info.Title                  = 'STDProfile';
Info.Constructor            = 'Kongsberg';
Info.TimeOrigin             = '01/01/-4713';
Info.Comments               = 'STDProfile Tuple';
Info.FormatVersion          = FormatVersion;

Info.Dimensions.NbSamples     = nbPoints;
Info.Dimensions.NbProfiles    = nbProfiles;

Info.Signals(1).Name          = 'TimeCPU';
Info.Signals(end).Dimensions  = 'NbProfiles, 1';
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = 'days since JC';
Info.Signals(end).FileName    = fullfile('Ssc_STDProfile','TimeCPU.bin');
Info.Signals(end).Tag         = verifKeyWord('SounderTime');
  
Info.Signals(end+1).Name      = 'SensorType';
Info.Signals(end).Dimensions  = 'NbProfiles, 1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_STDProfile','SensorType.bin');
Info.Signals(end).Tag         = verifKeyWord('SensorType');

Info.Signals(end+1).Name      = 'NumberOfMeasurements';
Info.Signals(end).Dimensions  = 'NbProfiles, 1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile('Ssc_STDProfile','NumberOfMeasurements.bin');
Info.Signals(end).Tag         = verifKeyWord('SounderNumberOfMeasurements');

Info.Images(1).Name           = 'Pressure';
Info.Images(end).Dimensions   = 'NbProfiles, NbSamples';
Info.Images(end).Storage      = 'single';
Info.Images(end).Unit         = 'Pa';
Info.Images(end).FileName     = fullfile('Ssc_STDProfile','Pressure.bin');
Info.Images(end).Tag          = verifKeyWord('SoundProfilePressure');

Info.Images(end+1).Name       = 'Temperature';
Info.Images(end).Dimensions   = 'NbProfiles, NbSamples';
Info.Images(end).Storage      = 'single';
Info.Images(end).Unit         = 'degC';
Info.Images(end).FileName     = fullfile('Ssc_STDProfile','Temperature.bin');
Info.Images(end).Tag          = verifKeyWord('SoundProfileTemperature');

Info.Images(end+1).Name       = 'Conductivity';
Info.Images(end).Dimensions   = 'NbProfiles, NbSamples';
Info.Images(end).Storage      = 'single';
Info.Images(end).Unit         = 'S/m';
Info.Images(end).FileName     = fullfile('Ssc_STDProfile','Conductivity.bin');
Info.Images(end).Tag          = verifKeyWord('SoundProfileConductivity');

Info.Images(end+1).Name       = 'SoundVelocity';
Info.Images(end).Dimensions   = 'NbProfiles, NbSamples';
Info.Images(end).Storage      = 'single';
Info.Images(end).Unit         = 'm/s';
Info.Images(end).FileName     = fullfile('Ssc_STDProfile','SoundVelocity.bin');
Info.Images(end).Tag          = verifKeyWord('SoundProfileSoundSpeed');

Info.Images(end+1).Name       = 'Depth';
Info.Images(end).Dimensions   = 'NbProfiles, NbSamples';
Info.Images(end).Storage      = 'single';
Info.Images(end).Unit         = 'm';
Info.Images(end).FileName     = fullfile('Ssc_STDProfile','Depth.bin');
Info.Images(end).Tag          = verifKeyWord('SoundProfileDepth');

Info.Images(end+1).Name       = 'Salinity';
Info.Images(end).Dimensions   = 'NbProfiles, NbSamples';
Info.Images(end).Storage      = 'single';
Info.Images(end).Unit         = 'psu';
Info.Images(end).FileName     = fullfile('Ssc_STDProfile','Salinity.bin');
Info.Images(end).Tag          = verifKeyWord('SoundProfileSalinity');

Info.Images(end+1).Name       = 'Absorption';
Info.Images(end).Dimensions   = 'NbProfiles, NbSamples';
Info.Images(end).Storage      = 'single';
Info.Images(end).Unit         = 'dB/km';
Info.Images(end).FileName     = fullfile('Ssc_STDProfile','Absorption.bin');
Info.Images(end).Tag          = verifKeyWord('SoundProfileAbsorption');


%% Cr�ation du fichier XML d�crivant la donn�e 

nomFicXml = fullfile(nomDirRacine, 'Ssc_STDProfile.xml');
xml_write(nomFicXml, Info);
flag = exist(nomFicXml, 'file');
if ~flag
    messageErreur(nomFicXml)
    return
end

%% Cr�ation du r�pertoire STDProfile

nomDirSsc = fullfile(nomDirRacine, 'Ssc_STDProfile');
if ~exist(nomDirSsc, 'dir')
    status = mkdir(nomDirSsc);
    if ~status
        messageErreur(nomDirSsc)
        flag = 0;
        return
    end
end

%% Cr�ation des fichiers binaires des signaux

for i=1:length(Info.Signals)
    flag = writeSignal(nomDirRacine, Info.Signals(i), Data.(Info.Signals(i).Name));
    if ~flag
        return
    end
end

%% Cr�ation des fichiers binaires des images

for i=1:length(Info.Images)
    % TODO : GLUGLUGLU, �viter la r�plication, cf. JMA :
    Data.(Info.Images(i).Name) = repmat(Data.(Info.Images(i).Name), nbProfiles, nbPoints);
    flag = writeImage(nomDirRacine, Info.Images(i), Data.(Info.Images(i).Name));
    if ~flag
        return
    end
end

flag = 1;

