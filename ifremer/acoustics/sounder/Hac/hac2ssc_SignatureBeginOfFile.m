function flag = hac2ssc_SignatureBeginOfFile(nomDirRacine, FormatVersion)

nomFicXml = fullfile(nomDirRacine, 'hac_SignatureBeginOfFile.xml');
SonarFmt2ssc_message(nomFicXml, '.hac')

%% Lecture du fichier XML d�crivant la donn�e 

flag = exist(nomFicXml, 'file');
if ~flag
    special_message('FichierNonExistant', nomFicXml)
    return
end

% edit(nomFicXml)
Datagrams = xml_read(nomFicXml);
[nomDirSignal, nomFicSignal] = fileparts(nomFicXml);
nomDirSignal = fullfile(nomDirSignal, nomFicSignal);


%{
for i=1:length(Datagrams.Variables)
    fprintf(1,'%d  %s\n', i, Datagrams.Variables(i).Name)
end
%}

[flag, TupleSize] = Read_BinVariable(Datagrams, nomDirSignal, 'TupleSize', 'uint32'); %#ok<ASGLU>
if ~flag
    return
end
% figure; plot(TupleSize); grid on; title('TupleSize');

[flag, TupleType] = Read_BinVariable(Datagrams, nomDirSignal, 'TupleType', 'uint16'); %#ok<ASGLU>
if ~flag
    return
end
% figure; plot(TupleType); grid on; title('TupleType');

[flag, Data.HACIdentifier] = Read_BinVariable(Datagrams, nomDirSignal, 'HACIdentifier', 'uint16');
if ~flag
    return
end
% figure; plot(Data.HACIdentifier); grid on; title('HACIdentifier');

[flag, Data.HACVersion] = Read_BinVariable(Datagrams, nomDirSignal, 'HACVersion', 'uint16');
if ~flag
    return
end
% figure; plot(Data.HACVersion); grid on; title('HACVersion');

[flag, Data.AcqSoftVersion] = Read_BinVariable(Datagrams, nomDirSignal, 'AcqSoftVersion', 'uint16');
if ~flag
    return
end
% figure; plot(Data.AcqSoftVersion); grid on; title('AcqSoftVersion');

[flag, Data.AcqSoftIdentifier] = Read_BinVariable(Datagrams, nomDirSignal, 'AcqSoftIdentifier', 'uint32');
if ~flag
    return
end
% figure; plot(Data.AcqSoftIdentifier); grid on; title('AcqSoftIdentifier');


[flag, ~] = Read_BinVariable(Datagrams, nomDirSignal, 'TupleAttribute', 'int32');
if ~flag
    return
end
% figure; plot(TupleAttribute); grid on; title('TupleAttribute');

[flag, ~] = Read_BinVariable(Datagrams, nomDirSignal, 'BackLink', 'uint16');
if ~flag
    return
end
% figure; plot(BackLink); grid on; title('BackLink');


%% G�n�ration du XML SonarScope

Info.Title                  = 'SignatureBeginOfFile';
Info.Constructor            = 'Kongsberg';
Info.TimeOrigin             = '01/01/-4713';
Info.Comments               = 'Signature Begin of File Tuple';
Info.FormatVersion          = FormatVersion;

Info.Dimensions.NbSamples     = 1;

Info.Signals(1).Name          = 'HACIdentifier';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_SignatureBeginOfFile','HACIdentifier.bin');
Info.Signals(end).Tag         = verifKeyWord('HACIdentifier');

Info.Signals(end+1).Name      = 'HACVersion';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = 's';
Info.Signals(end).FileName    = fullfile('Ssc_SignatureBeginOfFile','HACVersion.bin');
Info.Signals(end).Tag         = verifKeyWord('HACFormatVersion');
  
Info.Signals(end+1).Name      = 'AcqSoftVersion';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = 's';
Info.Signals(end).FileName    = fullfile('Ssc_SignatureBeginOfFile','AcqSoftVersion.bin');
Info.Signals(end).Tag         = verifKeyWord('AcqSoftVersion');

Info.Signals(end+1).Name      = 'AcqSoftIdentifier';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = 's';
Info.Signals(end).FileName    = fullfile('Ssc_SignatureBeginOfFile','AcqSoftIdentifier.bin');
Info.Signals(end).Tag         = verifKeyWord('AcqSoftIdentifier');

               
%% Cr�ation du fichier XML d�crivant la donn�e 

nomFicXml = fullfile(nomDirRacine, 'Ssc_SignatureBeginOfFile.xml');
xml_write(nomFicXml, Info);
flag = exist(nomFicXml, 'file');
if ~flag
    messageErreur(nomFicXml)
    return
end

%% Cr�ation du r�pertoire SignatureBeginOfFile

nomDirSsc = fullfile(nomDirRacine, 'Ssc_SignatureBeginOfFile');
if ~exist(nomDirSsc, 'dir')
    status = mkdir(nomDirSsc);
    if ~status
        messageErreur(nomDirSsc)
        flag = 0;
        return
    end
end

%% Cr�ation des fichiers binaires des signaux

for i=1:length(Info.Signals)
    flag = writeSignal(nomDirRacine, Info.Signals(i), Data.(Info.Signals(i).Name));
    if ~flag
        return
    end
end

flag = 1;

