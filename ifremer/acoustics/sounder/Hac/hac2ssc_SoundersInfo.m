%{
FormatVersion = 20110531;
nomDirRacine  = my_tempdir
flag = hac2ssc_SoundersInfo(nomDirRacine, FormatVersion)
%}

function flag = hac2ssc_SoundersInfo(nomDirRacine, FormatVersion)

nomFicXml = fullfile(nomDirRacine, 'hac_EK60Channel.xml');
SonarFmt2ssc_message(nomFicXml, '.hac')

%% Lecture du fichier XML d�crivant la donn�e 

Info.Title                  = 'HAC_Sounders';
Info.Constructor            = 'Kongsberg';
Info.TimeOrigin             = '01/01/-4713';
Info.Comments               = 'Description of sounders and channels contained in the HAC file';
Info.FormatVersion          = FormatVersion;

Info.Sounder(1).Name        = 'EK60';
Info.Sounder(1).Geometry    = 'Vertical';
Info.Sounder(1).Frequency   = [18 20 22 24 26];
Info.Sounder(1).TypeSounder = 1;

Info.Sounder(2).Name        = 'ME70';
Info.Sounder(2).Geometry    = 'Multibeam';
Info.Sounder(2).Frequency   = 1:24;
Info.Sounder(1).TypeSounder = 2;

Info.Sounder(3).Name        = 'EK60';
Info.Sounder(3).Geometry    = 'Horizontal';
Info.Sounder(3).Frequency   = 18;
Info.Sounder(1).TypeSounder = 1;

%{
Info.SounderSpec = {'Vertical'; 'Le precieux';'Horizontal'};

Info.Frequencies{1} = [18 20 22 24 26];
Info.Frequencies{2} = 1:24;;
Info.Frequencies{3} = 21;
%}

%% Cr�ation du fichier XML d�crivant la donn�e 

nomFicXml = fullfile(nomDirRacine, 'Ssc_SoundersInfo.xml');
xml_write(nomFicXml, Info);
flag = exist(nomFicXml, 'file');
if ~flag
    messageErreur(nomFicXml)
    return
end

flag = 1;

