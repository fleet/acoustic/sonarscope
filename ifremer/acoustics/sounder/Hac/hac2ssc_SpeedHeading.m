function flag = hac2ssc_SpeedHeading(nomDirRacine, FormatVersion)

nomFicXml = fullfile(nomDirRacine, 'hac_SpeedHeading.xml');
SonarFmt2ssc_message(nomFicXml, '.hac')

%% Lecture du fichier XML d�crivant la donn�e 

flag = exist(nomFicXml, 'file');
if ~flag
    special_message('FichierNonExistant', nomFicXml)
    return
end

% edit(nomFicXml)
Datagrams = xml_read(nomFicXml);
[nomDirSignal, nomFicSignal] = fileparts(nomFicXml);
nomDirSignal = fullfile(nomDirSignal, nomFicSignal);


%{
for i=1:length(Datagrams.Variables)
    fprintf(1,'%d  %s\n', i, Datagrams.Variables(i).Name)
end
%}


nbSamples = Datagrams.NbDatagrams;

[flag, TupleSize] = Read_BinVariable(Datagrams, nomDirSignal, 'TupleSize', 'uint32'); %#ok<ASGLU>
if ~flag
    return
end
% figure; plot(TupleSize); grid on; title('TupleSize');

[flag, TupleType] = Read_BinVariable(Datagrams, nomDirSignal, 'TupleType', 'uint16'); %#ok<ASGLU>
if ~flag
    return
end
% figure; plot(TupleType); grid on; title('TupleType');

[flag, Data.TimeFraction] = Read_BinVariable(Datagrams, nomDirSignal,  'TimeFraction', 'double');
if ~flag
    return
end
% figure; plot(Data.TimeFraction); grid on; title('TimeFraction');

[flag, Data.TimeCPU] = Read_BinVariable(Datagrams, nomDirSignal, 'TimeCPU', 'double');
if ~flag
    return
end
% figure; plot(Data.TimeCPU); grid on; title('TimeCPU');

[flag, Data.NavigationSystem] = Read_BinVariable(Datagrams, nomDirSignal, 'NavigationSystem', 'uint16');
if ~flag
    return
end
% figure; plot(Data.NavSystem); grid on; title('NavSystem');

[flag, Data.Heading] = Read_BinVariable(Datagrams, nomDirSignal, 'Heading', 'single');
if ~flag
    return
end
% figure; plot(Data.Heading); grid on; title('Heading');

[flag, Data.Speed] = Read_BinVariable(Datagrams, nomDirSignal, 'Speed', 'single');
if ~flag
    return
end
% figure; plot(Data.SpeedHeadingSpeed); grid on; title('SpeedHeadingSpeed');

[flag, TupleAttribute] = Read_BinVariable(Datagrams, nomDirSignal, 'TupleAttribute', 'int32'); %#ok<ASGLU>
if ~flag
    return
end
% figure; plot(TupleAttribute); grid on; title('TupleAttribute');

[flag, BackLink] = Read_BinVariable(Datagrams, nomDirSignal, 'BackLink', 'uint16');
if ~flag
    return
end
% figure; plot(BackLink); grid on; title('BackLink');

%% Calcul des dates et heures :

T = double(Data.TimeCPU);
ansi_offset  = datenum(1970,1,1,0,0,0);
matlab_time  = T./86400 + ansi_offset + Data.TimeFraction/86400; 
Data.TimeCPU = cl_time('timeMat', matlab_time);

%% G�n�ration du XML SonarScope

Info.Title                  = 'SpeedHeading';
Info.Constructor            = 'Kongsberg';
Info.TimeOrigin             = '01/01/-4713';
Info.Comments               = 'SpeedHeading Tuple';
Info.FormatVersion          = FormatVersion;

Info.Dimensions.NbSamples     = nbSamples;

Info.Signals(1).Name          = 'TimeCPU';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = 'days since JC';
Info.Signals(end).FileName    = fullfile('Ssc_SpeedHeading','TimeCPU.bin');
Info.Signals(end).Tag         = verifKeyWord('SounderTime');
  
Info.Signals(end+1).Name      = 'NavigationSystem';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_SpeedHeading','NavigationSystem.bin');
Info.Signals(end).Tag         = verifKeyWord('NavigationSystem');

Info.Signals(end+1).Name      = 'Heading';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile('Ssc_SpeedHeading','Heading.bin');
Info.Signals(end).Tag         = verifKeyWord('SounderHeading');

Info.Signals(end+1).Name      = 'Speed';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'nd';
Info.Signals(end).FileName    = fullfile('Ssc_SpeedHeading','Speed.bin');
Info.Signals(end).Tag         = verifKeyWord('Speed');

               
%% Cr�ation du fichier XML d�crivant la donn�e 

nomFicXml = fullfile(nomDirRacine, 'Ssc_SpeedHeading.xml');
xml_write(nomFicXml, Info);
flag = exist(nomFicXml, 'file');
if ~flag
    messageErreur(nomFicXml)
    return
end

%% Cr�ation du r�pertoire SpeedHeading

nomDirSsc = fullfile(nomDirRacine, 'Ssc_SpeedHeading');
if ~exist(nomDirSsc, 'dir')
    status = mkdir(nomDirSsc);
    if ~status
        messageErreur(nomDirSsc)
        flag = 0;
        return
    end
end

%% Cr�ation des fichiers binaires des signaux

for i=1:length(Info.Signals)
    flag = writeSignal(nomDirRacine, Info.Signals(i), Data.(Info.Signals(i).Name));
    if ~flag
        return
    end
end

flag = 1;

