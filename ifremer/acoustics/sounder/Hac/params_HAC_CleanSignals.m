function  [flag, listeFic, identFreq, repImport] = params_HAC_CleanSignals(repImport)

identFreq = [];

[flag, listeFic, repImport] = uiSelectFiles('ExtensionFiles', '.hac', 'RepDefaut', repImport);
if ~flag
    return
end

%% Recherche des fr�quences disponibles dans le premier fichier

a = cl_hac('nomFic', listeFic{1});

% TODO : A finir
Frequency = get_Frequency(a);

%% S�lection des fr�quences

for k=1:length(Frequency)
    str{k} = num2str(Frequency(k)); %#ok<AGROW>
end
str1 = 'S�lectionnez la fr�quence � utiliser pour faire la d�tection de hauteur.';
str2 = 'Select the frequency to use for the heiht detection.';
[identFreq, flag] = my_listdlg(Lang(str1,str2), str, 'InitialValue', 1, 'SelectionMode', 'Single');
if ~flag
    return
end
