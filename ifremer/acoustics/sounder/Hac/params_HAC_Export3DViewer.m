function [flag, nomDirOut, CLim, CLimNaN, ColormapIndex, Tag, HauteurMin, SuppressionOverHeight, ...
    identLayer, TailleMax, MeanTide, repExport] = params_HAC_Export3DViewer(this, repExport)

persistent persistent_CLimAmp persistent_CLimPhase persistent_ColormapIndex persistent_HauteurMin persistent_MeanTide

nomDirOut               = [];
CLim                    = [];
CLimNaN                 = [];
Tag                     = [];
ColormapIndex           = [];
HauteurMin              = 0;
TailleMax               = 0;
SuppressionOverHeight   = true;
MeanTide                = 0;

%% Type de donn�es � traiter

str1 = 'Type de donn�es';
str2 = 'Data type';
str{1} = 'Amplitude';
str{2} = 'Phase Across';
str{3} = 'Phase Along';
[identLayer, flag] = my_listdlg(Lang(str1,str2), str, 'SelectionMode', 'Single');
if ~flag
    return
end

%% Select directory

str1 = 'Choisissez ou cr�ez le r�pertoire o� seront stock�s les fichiers ';
str2 = 'Select or create the directory in which the files will be created.';
[flag, nomDirOut] = my_uigetdir(repExport, Lang(str1,str2));
if ~flag
    return
end
repExport = nomDirOut;

%% Tag suppl�mentaire dans le mom des images

[flag, Tag] = question_AddTagInOutputFile;
if ~flag
    return
end

%% Bornes de rehaussement de contraste

if identLayer == 1
    if isempty(persistent_CLimAmp)
        minVal   = -100;
        maxVal   = -40;
        SeuilInf = -Inf;
        SeuilSup = Inf;
    else
        minVal   = persistent_CLimAmp(1);
        maxVal   = persistent_CLimAmp(2);
        SeuilInf = persistent_CLimAmp(3);
        SeuilSup = persistent_CLimAmp(4);
    end
    Unit = 'dB';
else
    if isempty(persistent_CLimPhase)
        minVal   = -10;
        maxVal   = 10;
        SeuilInf = -Inf;
        SeuilSup = Inf;
    else
        minVal   = persistent_CLimPhase(1);
        maxVal   = persistent_CLimPhase(2);
        SeuilInf = persistent_CLimPhase(3);
        SeuilSup = persistent_CLimPhase(4);
    end
    Unit = 'deg';
end

if isempty(persistent_HauteurMin)
    HauteurMin = 0;
else
    HauteurMin = persistent_HauteurMin;
end

str1 = 'Valeur Max';
str2 = 'Max value';
str3 = 'Valeur Min';
str4 = 'Min value';
str5 = 'Transparence au dessus de';
str6 = 'Set to NaN over';
str7 = 'Transparence en dessous de ';
str8 = 'Set to NaN under';
str9 = 'Suppression des premiers �chos sur';
str10 = 'Suppress first echos on';
p    = ClParametre('Name', Lang(str1,str2), 'Unit',  Unit, 'Value', maxVal);
p(2) = ClParametre('Name', Lang(str3,str4), 'Unit',  Unit, 'Value', minVal);
p(3) = ClParametre('Name', Lang(str7,str8), 'Unit',  Unit, 'Value', SeuilSup);
p(4) = ClParametre('Name', Lang(str5,str6), 'Unit',  Unit, 'Value', SeuilInf);
p(5) = ClParametre('Name', Lang(str9,str10), 'Unit', 'm', 'Value', HauteurMin);
str1 = 'Bornes de rehaussement de contraste';
str2 = 'Limits of the contrast enhancement';
a = StyledSimpleParametreDialog('params', p, 'Title', Lang(str1,str2));
a.openDialog;
flag = a.okPressedOut;
if ~flag
    return
end
Value = a.getParamsValue;
CLim       = Value([2 1]);
CLimNaN    = Value([4 3]);
HauteurMin = Value(5);

if identLayer == 1
    persistent_CLimAmp   = [CLim CLimNaN];
else
    persistent_CLimPhase = [CLim CLimNaN];
end
persistent_HauteurMin = HauteurMin;

%% S�lection de la table de couleurs

if isempty(persistent_ColormapIndex)
    ColormapIndex = 3;
else
    ColormapIndex = persistent_ColormapIndex;
end
[flag, ColormapIndex] = edit_ColormapIndex(this, 'ColormapIndex', ColormapIndex);
if ~flag
    return
end
persistent_ColormapIndex = ColormapIndex;

%% Supression des donn�es au dela de la d�tection de hauteur

str1 = 'Suppression des donn�es au dela de la hauteur d�tect�e ?';
str2 = 'Suppress data beyond the detected height ?';
[rep, flag] = my_questdlg(Lang(str1,str2));
if ~flag
    return
end
SuppressionOverHeight = (rep == 1);

%% Taille max des images support� par le GLOBE 

pppp = opengl('data');
TailleMax = pppp.MaxTextureSize;

%% Saisie d'une mar�e moyenne si elle n'est pas trouv�e dans les fichiers

if isempty(persistent_MeanTide)
    MeanTide = 0;
else
    MeanTide = persistent_MeanTide;
end

str1 = 'Correction unique de mar�e au cas o� cette information ne serait pas trouv�e dans les fichiers.';
str2 = 'Unique tide correction if ever this data was not found in the files.';
[flag, MeanTide] = inputOneParametre(Lang(str1,str2), Lang('Valeur Min', 'Min value'), ...
    'Unit', 'm', 'Value', MeanTide, 'MinValue', 0, 'MaxValue', 16);
if ~flag
    return
end

persistent_MeanTide = MeanTide;
