function [flag, Data] = readCompressAnglesFromHac(  Type, Mode, TableInfo, ...
                                                    nomDirSignal, MaxNbChannels, ...
                                                    MaxNbAngles, NTotalSamples, NbSamplesCompress, ...
                                                    DistribTimeTuple, SoftwareChannelIdentifier) 


flag = 0; %#ok<NASGU>

%% Lecture des �chantillons d'angles extraits des fichiers HAC.
% Pas de lecture possible via Read_Bintable car donn�e est doublement significative.

nomFic = fullfile(nomDirSignal, 'CompressAngles.bin');
fid = fopen(nomFic, 'r');
if fid == -1
    messageErreurFichier(nomFic);
    flag = 0;
    return
end

[Signal, n] = fread(fid, [2 NTotalSamples], 'int16');
if isempty(NTotalSamples)
    fclose(fid);
    flag = 1;
    return
end

if n < (NTotalSamples)
    messageErreurFichier(nomFic, 'ReadFailure');
    flag = 0;
    fclose(fid);
    return
end

fclose(fid);

%% Traitement du poids fort (Along) et faible (Athwart) de la donn�e par 
% distribution sur deux tableaux.

[TimeTuple, dummy, idxTime]      = unique(DistribTimeTuple);

AthwartShip = Signal(1, :);
% Si le 16 �me bit est � 1, (valeur < 0)
sub = find(AthwartShip > 2^15);
if ~isempty(sub)
    AthwartShip(sub) = AthwartShip(sub) - 2^16;
end
%
AlongShip = Signal(2, :);
% Si le 15 �me bit est � 1, (valeur < 0)
sub = find(AlongShip > 2^14);
if ~isempty(sub)
    AlongShip(sub) = AlongShip(sub) - 2^15;
end

% R�partition des �chantillons angulaires
% Mise en forme matricielle de la donn�e du Ping.
if Type == 1 && Mode == 2 % MonoBeam et MultiFreq
    Data.AlongShip       = NaN(size(TimeTuple,1), MaxNbAngles, MaxNbChannels);
    Data.AthwartShip     = NaN(size(TimeTuple,1), MaxNbAngles, MaxNbChannels);
elseif Type == 2 && Mode == 1 % MultiBeam et MonoFreq
    Data.AlongShip       = NaN(MaxNbAngles, MaxNbChannels, size(TimeTuple,1));
    Data.AthwartShip     = NaN(MaxNbAngles, MaxNbChannels, size(TimeTuple,1));
end

%% R�partition des angles sur les matrices Ping, NSamples et NChannels.

minChannelNumber = min(SoftwareChannelIdentifier);
OffsetLus        = 1;

for p=1:size(DistribTimeTuple,1)
    if ~isnan(DistribTimeTuple(p))
        idxPing    = idxTime(p);
        numChannel = SoftwareChannelIdentifier(p)-minChannelNumber+1;
        nbS        = NbSamplesCompress(p);
        if nbS > 0
            tmpAlongShip          = AlongShip(OffsetLus:OffsetLus+nbS-1);
            tmpAthwartShip        = AthwartShip(OffsetLus:OffsetLus+nbS-1);
            tmpAlongShip          = code2val(tmpAlongShip, TableInfo, 'single');
            tmpAthwartShip        = code2val(tmpAthwartShip, TableInfo, 'single');
            if Type == 1 && Mode == 2 % MonoBeam et MultiFreq
                Data.AthwartShip(idxPing, 1:nbS , numChannel)  = tmpAthwartShip;
                Data.AlongShip(idxPing, 1:nbS , numChannel)    = tmpAlongShip;
            else
                Data.AthwartShip(1:nbS , numChannel, idxPing)  = tmpAthwartShip;
                Data.AlongShip(1:nbS , numChannel, idxPing)    = tmpAlongShip;
            end
            OffsetLus = OffsetLus+nbS;
        end
    else
        fprintf('Message for JMA : DistribTimeTuple(%d)=NaN in "readCompressAnglesFromHac"\n', p);
    end
end

flag = 1;

