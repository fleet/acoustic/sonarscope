function [flag, ucSamples] = readCompressSamplesFromHac(Type, Mode, TableInfo, ...
                                                        MaxNbChannels, MaxNbSamples, DistribTimeTuple, ...
                                                        SWChannelID, NbSamplesCompress, X) 

[TimeTuple, dummy, idxTime]      = unique(DistribTimeTuple);
subNotNaN        = ~isnan(TimeTuple);
TimeTuple        = TimeTuple(subNotNaN);
% minPingNumber    = min(DistribTimeTuple);
minChannelNumber = min(SWChannelID);
% % % % 
% Mise en forme matricielle de la donn�e du Ping.
if Type == 1 && Mode == 2 % MonoBeam et MultiFreq
    ucSamples = NaN(size(TimeTuple,1), MaxNbSamples, MaxNbChannels);
elseif Type == 2 && Mode == 1 % MultiBeam et MonoFreq 
    ucSamples = NaN(MaxNbSamples, MaxNbChannels, size(TimeTuple,1));
end

OffsetLus = 1;

for p=1:size(DistribTimeTuple,1)
    if ~isnan(DistribTimeTuple(p))
        numChannel   = SWChannelID(p)-minChannelNumber+1;
        idxPing      = idxTime(p);
        nbCS         = NbSamplesCompress(p);
        if nbCS > 0
            CS       = X(OffsetLus:(OffsetLus+nbCS-1));
            [~, ucX] = unCompressSamples10040(CS); 
            ucX      = code2val(ucX, TableInfo, 'single');
            nbuCS    = size(ucX,2);
            if Type == 1 && Mode == 2 % MonoBeam et MultiFreq
                ucSamples(idxPing, 1:nbuCS , numChannel) = ucX;
            else
                ucSamples(1:nbuCS, numChannel, idxPing) = ucX;
            end
            OffsetLus = OffsetLus+nbCS;
        end
    end
end

flag = 1;

