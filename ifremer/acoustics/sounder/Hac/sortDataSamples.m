function Samples = sortDataSamples(subOrder, SounderDesc, Samples)

%% Mise en forme des images selon le tri opéré précédemment.
% Cas du Monofaisceau et MultiFréquence.
if SounderDesc.DataSounder.TypeSounder == 1 && SounderDesc.DataSounder.ModeSounder == 2
    X = Samples;
    X = permute(X, [3 1 2]);
    sz = size(X);
    X = reshape(X, [sz(1)*sz(2) sz(3)]);
    X = X(subOrder, :);
    X = reshape(X, sz);
    X = permute(X, [2 3 1]);
    Samples = X;
else
    % Cas du Multifaisceau et MonoFréquence.
    if SounderDesc.DataSounder.TypeSounder == 2 && SounderDesc.DataSounder.ModeSounder == 1
        X = Samples;
        X = permute(X, [3 2 1]);
        sz = size(X);
        X = reshape(X, [sz(1)*sz(2) sz(3)]);
        X = X(subOrder, :);
        X = reshape(X, sz);
        X = permute(X, [3 2 1]);
        Samples = X;
    end
end

