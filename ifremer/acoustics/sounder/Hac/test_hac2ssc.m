%%
nomFic = getNomFicDatabase('PELGAS10_003_20100428_183857.hac');
flag = hac2ssc(nomFic);
[flag, Data] = read_data('NomFic', nomFic, 'Memmapfile', 1, 'NumSounder', 2); 
%%
nomFic = 'F:\SonarScopeData\From AndreOgor\0812_20091213_205617_Lesuroit_Orig_220et2200.hac';
flag = hac2ssc(nomFic);
%%
nomFic = 'F:\SonarScopeData\From AndreOgor\0812_20091213_205617_Lesuroit_Orig_901et9001.hac';
flag = hac2ssc(nomFic);
%%
Head1.SystemSerialNumber =  106;
Head1.S1R = 0.0200;
Head1.SystemSerialNumberSecondHead = 0;
%%    
nomFic  = 'F:\SonarScopeData\From Andre Ogor\0812_20091213_205617_Lesuroit.hac';
a       = cl_hac('nomFic', nomFic);
flag = hac2ssc(nomFic);
[flag, Data] = read_data('NomFic', nomFic, 'Memmapfile', 1); 
%%
nomFic = 'F:\SonarScopeData\From AndreOgor\0812_20091213_205617_Lesuroit_Orig_901et9001.hac';
flag = hac2ssc(nomFic);
%%
nomFic = getNomFicDatabase('0306_20091113_160950_Lesuroit.all');
flag = all2ssc(nomFic);

%% Resultat final de la compression
CompressSamples = [31145  29794  28918  28643 -32767  28350  28442 -32720  27032  27274 -32768 26960 -32768 26596 -32768 26882 26475 26519];
NbSamples(1) = 10; % size(CompressSamples,2);
%% Réallocation des Nb Samples selon les paquets d'échantillons décompressés. 
for n=1:size(NbSamples,2)
    pppp = CompressSamples(1:NbSamples(n));
    [uInf, vInf]         = find(pppp < 0);
    NbSamplesCompress   = pppp(vInf);
    NbSamplesCompress   = (NbSamplesCompress+32768)+1;
    N = sum(NbSamplesCompress) + NbSamples(n) - size(NbSamplesCompress, 2);
end
    
[uInf, vInf]     = find(CompressSamples < 0);
[uSup, vSup]     = find(CompressSamples > 0);
CompressSamples(vSup)     = bitor(uint16(typecast(single(CompressSamples(vSup)),'single')),32768);
CompressSamples(vSup)     = single(CompressSamples(vSup))-32768*2;

decompressSeuil = -6400;

NbSamplesInfThres = CompressSamples(vInf);
NbSamplesInfThres = (NbSamplesInfThres+32768)+1;
SamplesSupThres = CompressSamples(vSup);
UncompressSamples = NaN(1, sum(NbSamplesInfThres)+size(SamplesSupThres,2));

%%

for i=1:size(vInf,2)
    if i==1
        insertInPrev    = 1;
        insertFromPrev  = 1;
        insertFrom      = vInf(i);
        insertIn        = vInf(i);
        insertUntil     = vInf(i) + NbSamplesInfThres(i);
    else
        insertInPrev    = vInf(i-1) + sum(NbSamplesInfThres(1:i-1));
        insertIn        = vInf(i) -1 + sum(NbSamplesInfThres(1:i-1));
        insertFromPrev  = vInf(i-1)+1;
        insertFrom      = vInf(i);
        insertUntil     = vInf(i) + sum(NbSamplesInfThres(1:i-1))-1 + NbSamplesInfThres(i);
    end
    UncompressSamples(insertInPrev:insertIn-1)  = CompressSamples(insertFromPrev:insertFrom-1);
    % UncompressSamples(insertIn:insertUntil-1)   = ones(NbSamplesInfThres(i),1)*decompressSeuil;
end
sub = isnan(UncompressSamples);
UncompressSamples(sub) = decompressSeuil;
% UncompressSamples*0.01
%%
nomFic = 'F:\SonarScopeData\From LaurentBerger\Svalbard_JCR253-ER60\ER60_OUTPUT2_D20110815-T141253.hac';
flag = hac2ssc(nomFic);
%%
nomFic = 'F:\SonarScopeData\Test HAC\PELGAS11_035_20110530_082154.hac';
flag = hac2ssc(nomFic);
%%
nomFic = 'F:\SonarScopeData\Test HAC\PELGAS11_035_20110530_083154.hac';
flag = hac2ssc(nomFic);
% [flag, this, Carto] = import_hac(cl_image, nomFic);
%%
nomFic = 'F:\SonarScopeData\Test HAC\PELGAS11_035_20110530_062154.hac';
flag = hac2ssc(nomFic);
% [flag, this, Carto] = import_hac(cl_image, nomFic);
%%
nomFic = 'F:\SonarScopeData\Test HAC\PELGAS11_016_20110511_192704.HAC';
flag = hac2ssc(nomFic);
% [flag, this, Carto] = import_hac(cl_image, nomFic);

%%
nomFic = 'F:\SonarScopeData\Test HAC\PELGAS11_002_20110427_160339.hac';
flag = hac2ssc(nomFic);
%%
nomFic = 'F:\SonarScopeData\Test HAC\PELGAS11_002_20110427_072328.HAC';
flag = hac2ssc(nomFic);

% [flag, this, Carto] = import_hac(cl_image, nomFic);
%%
nomFic = 'F:\SonarScopeData\Test HAC\PELGAS11_013_20110508_003249.hac';
flag = hac2ssc(nomFic);
%%
nomFic = 'F:\SonarScopeData\Test HAC\PELGAS11_013_20110508_235659.hac';
flag = hac2ssc(nomFic);
%%
nomFic = 'F:\SonarScopeData\Test HAC\PELGAS11_021_20110516_045049.hac';
flag = hac2ssc(nomFic);
%%
nomFic = 'F:\SonarScopeData\Test HAC\PELGAS11_021_20110516_073051.hac';
flag = hac2ssc(nomFic);
%%
nomFic = 'F:\SonarScopeData\Test HAC\PELGAS11_021_20110516_085051.hac';
flag = hac2ssc(nomFic);
%%
nomFic = 'F:\SonarScopeData\Test HAC\PELGAS11_021_20110516_112053.hac';
flag = hac2ssc(nomFic);
%%
nomFic = 'F:\SonarScopeData\Test HAC\PELGAS11_021_20110516_083051.hac';
flag = hac2ssc(nomFic);
%%
nomFic = 'F:\SonarScopeData\Test HAC\PELGAS11_021_20110516_122053.hac';
flag = hac2ssc(nomFic);
%%
nomFic = 'F:\SonarScopeData\Test HAC\PELGAS11_021_20110516_154306.hac';
flag = hac2ssc(nomFic);
%%
nomFic = getNomFicDatabase('pelgas10_002_20100427_050359.hac');
flag = hac2ssc(nomFic);

%%
nomFic = 'F:\SonarScopeData\Test HAC\PELGAS11_021_20110516_045049.hac';
nomDirRacine = 'F:\SonarScopeData\Test HAC\SonarScope\PELGAS11_021_20110516_045049'; 
FormatVersion   = 20110531;
iTotalEchoSounder   = [1 3 2];
iSounder            = 3;

SounderDesc(1).DataSounder.EchosounderDocIdentifier = 1;
SounderDesc(1).DataSounder.NumberSWChannels = 5;
SounderDesc(1).DataSounder.TypeSounder = 1;
SounderDesc(1).DataSounder.ModeSounder = 2;
SounderDesc(1).DataChannel.SoftwareChannelIdentifier = (46:50)';
SounderDesc(1).DataChannel.EchosounderDocIdentifier = ones(5,1)*1;

SounderDesc(3).DataSounder.EchosounderDocIdentifier = 3;
SounderDesc(3).DataSounder.NumberSWChannels = 1;
SounderDesc(3).DataSounder.TypeSounder = 1;
SounderDesc(3).DataSounder.ModeSounder = 2;
SounderDesc(3).DataChannel.SoftwareChannelIdentifier = 136;
SounderDesc(3).DataChannel.EchosounderDocIdentifier = 3;

SounderDesc(2).DataSounder.EchosounderDocIdentifier = 2;
SounderDesc(2).DataSounder.TypeSounder = 2;
SounderDesc(2).DataSounder.ModeSounder = 1;
SounderDesc(2).DataSounder.NumberSWChannels = 21;
SounderDesc(2).DataChannel.SoftwareChannelIdentifier = (91:111)';
SounderDesc(2).DataChannel.EchosounderDocIdentifier = ones(21,1)*2;


NomFicXML           = ['HAC_PingC16Amplitude_' num2str(iTotalEchoSounder(iSounder)) '.xml'];

if exist(fullfile(nomDirRacine, NomFicXML), 'file')
    try
        flag = hac2ssc_PingC16Amplitude(nomDirRacine, iTotalEchoSounder(iSounder), SounderDesc(iTotalEchoSounder(iSounder)), FormatVersion);
        if ~flag
            return
        end
    catch ME
        ErrorReport = getReport(ME);
%         SendEmailSupport(ErrorReport)
        renameBadFile(nomFic, 'Message', ErrorReport)
    end
end

%%
nomFic = 'F:\SonarScopeData\From Carla\FUTUNA 3\D20120518-T062213.hac';
flag = hac2ssc(nomFic);
%%
nomFic = getNomFicDatabase('IBTS10XX_014_20100127_160455.HAC');
% flag = hac2ssc(nomFic);
str = '!"C:\SonarScopeTbx\ifremer\extern\C\CONVERT_exe\Release\CONVERT_hac.exe"';
cmd = [str ' ' nomFic];
evalc(cmd);
%%
nomFic = getNomFicDatabase('pelgas10_002_20100427_050359.HAC');
% flag = hac2ssc(nomFic);
str = '!"C:\SonarScopeTbx\ifremer\extern\C\CONVERT_exe\Release\CONVERT_hac.exe"';
cmd = [str ' ' nomFic] %#ok<*NOPTS>
evalc(cmd);

%%
%%
nomDirRacine        = 'F:\SonarScopeData\Test HAC\SonarScope\PELGAS11_035_20110530_061154'; 
FormatVersion       = 20110531;
iTotalEchoSounder   = [1 3 2];
iSounder            = 3;

SounderDesc(1).DataSounder.EchosounderDocIdentifier = 1;
SounderDesc(1).DataSounder.NumberSWChannels = 5;
SounderDesc(1).DataSounder.TypeSounder = 1;
SounderDesc(1).DataSounder.ModeSounder = 2;
SounderDesc(1).DataChannel.SoftwareChannelIdentifier = (46:50)';
SounderDesc(1).DataChannel.EchosounderDocIdentifier = ones(5,1)*1;

SounderDesc(3).DataSounder.EchosounderDocIdentifier = 3;
SounderDesc(3).DataSounder.NumberSWChannels = 1;
SounderDesc(3).DataSounder.TypeSounder = 1;
SounderDesc(3).DataSounder.ModeSounder = 2;
SounderDesc(3).DataChannel.SoftwareChannelIdentifier = 136;
SounderDesc(3).DataChannel.EchosounderDocIdentifier = 3;

SounderDesc(2).DataSounder.EchosounderDocIdentifier = 2;
SounderDesc(2).DataSounder.TypeSounder = 2;
SounderDesc(2).DataSounder.ModeSounder = 1;
SounderDesc(2).DataSounder.NumberSWChannels = 21;
SounderDesc(2).DataChannel.SoftwareChannelIdentifier = (91:111)';
SounderDesc(2).DataChannel.EchosounderDocIdentifier = ones(21,1)*2;


NomFicXML           = ['HAC_PingC16Amplitude_' num2str(iTotalEchoSounder(iSounder)) '.xml'];

flag = hac2ssc_PingC16Amplitude(nomDirRacine, iTotalEchoSounder(iSounder), SounderDesc(iTotalEchoSounder(iSounder)), FormatVersion);


%%
nomFic = 'F:\SonarScopeData\From LaurentBerger\20091106_HAC\0047_20091106_001539_Lesuroit_2.hac';
flag = hac2ssc(nomFic);
%%
nomFic = 'F:\SonarScopeData\From LaurentBerger\20091106_HAC\0054_20091106_034537_Lesuroit.hac';
flag = hac2ssc(nomFic);
%%
nomFic = 'F:\SonarScopeData\Test HAC\PELGAS11_011_20110506_105740.hac';
flag = hac2ssc(nomFic);
%%
nomFic = 'F:\SonarScopeData\Test HAC\PELGAS11_035_20110530_061154.hac';
flag = hac2ssc(nomFic);
% [flag, this, Carto] = import_hac(cl_image, nomFic);
%%
nomFic = 'C:\temp\Test_HAC_Creation\FK-D20120510-T202356_raw2hac.hac';
flag = hac2ssc(nomFic);
%%
nomFic = 'F:\SonarScopeData\From Carla\FUTUNA 3\D20120518-T062213.hac';
flag = hac2ssc(nomFic);
%%
nomFicHac = 'C:\Temp\Test_HAC_Creation\0024_20100605_114501_all2hac.hac';
a = cl_hac('nomFic', nomFicHac)
%%
nomFic = 'F:\SonarScopeData\Test HAC\PELGAS13_015_20130510_032407.hac';
flag = hac2ssc(nomFic);
%%
nomFic = 'F:\SonarScopeData\Test HAC\PELGAS13_015_20130510_035407.hac';
flag = hac2ssc(nomFic);
%%
nomFic = 'F:\SonarScopeData\Test HAC\PELGAS12_020_20120515_190302.hac';
flag = hac2ssc(nomFic);
%%
nomFic = 'F:\SonarScopeData\Test HAC\ME70_ONLY_PELGAS13_002_20130427_060809.hac';
flag = hac2ssc(nomFic);
