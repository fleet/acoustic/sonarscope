
%%
nomFic = 'F:\SonarScopeData\Test HAC\PELGAS11_021_20110516_045049.hac';
flag = hac2ssc(nomFic);

%%
nomFic              = 'F:\SonarScopeData\Test HAC\PELGAS11_021_20110516_045049.hac';
nomDirRacine        = 'F:\SonarScopeData\Test HAC\SonarScope\PELGAS11_021_20110516_045049'; 
FormatVersion       = 20110531;
iTotalEchoSounder   = [1 3 2];
iSounder            = 1;

SounderDesc(1).DataSounder.EchosounderDocIdentifier = 1;
SounderDesc(1).DataSounder.NumberSWChannels = 5;
SounderDesc(1).DataSounder.TypeSounder = 1;
SounderDesc(1).DataSounder.ModeSounder = 2;
SounderDesc(1).DataChannel.SoftwareChannelIdentifier = (46:50)';
SounderDesc(1).DataChannel.EchosounderDocIdentifier = ones(5,1)*1;

SounderDesc(3).DataSounder.EchosounderDocIdentifier = 3;
SounderDesc(3).DataSounder.NumberSWChannels = 1;
SounderDesc(3).DataSounder.TypeSounder = 1;
SounderDesc(3).DataSounder.ModeSounder = 2;
SounderDesc(3).DataChannel.SoftwareChannelIdentifier = 136;
SounderDesc(3).DataChannel.EchosounderDocIdentifier = 3;

SounderDesc(2).DataSounder.EchosounderDocIdentifier = 2;
SounderDesc(2).DataSounder.TypeSounder = 2;
SounderDesc(2).DataSounder.ModeSounder = 1;
SounderDesc(2).DataSounder.NumberSWChannels = 21;
SounderDesc(2).DataChannel.SoftwareChannelIdentifier = (91:111)';
SounderDesc(2).DataChannel.EchosounderDocIdentifier = ones(21,1)*2;


NomFicXML           = ['HAC_PingC1632Angle_' num2str(iTotalEchoSounder(iSounder)) '.xml'];

if exist(fullfile(nomDirRacine, NomFicXML), 'file')
    try
        flag = hac2ssc_PingC1632Angle(nomDirRacine, iTotalEchoSounder(iSounder), SounderDesc(iTotalEchoSounder(iSounder)), FormatVersion);
        if ~flag
            return
        end
    catch ME
        ErrorReport = getReport(ME);
%         SendEmailSupport(ErrorReport);
        renameBadFile(nomFic, 'Message', ErrorReport);
    end
end

%% Tuple 10031 - U16
nomFic = 'F:\SonarScopeData\Test HAC\PELGAS13_002_20130427_060809.hac';
nomFic = 'F:\SonarScopeData\Test HAC\ER60_ONLY_PELGAS13_002_20130427_060809.hac';
flag = hac2ssc(nomFic);

