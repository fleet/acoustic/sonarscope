function [flag, uCAngles] = unCompressAngles10011(CompressAngles)

flag = 0; %#ok<NASGU>

try

    %% La compression des échantillons d'angles n'est pas mise en oeuvre.
    % A optimiser.
    for i=1:size(CompressAngles, 1)
        pppp = bitget(uint32(CompressAngles(i)), 16:-1:1);
        uCAngles.AthwartShip(i) = bin2dec(sprintf('%-1d',pppp));
        if bitget(uCAngles.AthwartShip(i),16) == 1
            uCAngles.AthwartShip(i) = uCAngles.AthwartShip(i) - 2^16;
        end

        pppp = bitget(uint32(CompressAngles(i)), 31:-1:17);
        uCAngles.AlongShip(i)   = bin2dec(sprintf('%-1d',pppp));
        if bitget(uCAngles.AlongShip(i),15) == 1
            uCAngles.AlongShip(i) = uCAngles.AlongShip(i) - 2^15;
        end
    end

    
    flag = 1;
catch ME %#ok<NASGU>
    flag = -1;
    uCAngles = [];
end