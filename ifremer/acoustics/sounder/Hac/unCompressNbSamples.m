function [flag, ucNbSamples] = unCompressNbSamples(NbSamples, CompressSamples)

% flag        = -1;
ucNbSamples = 0;
try

    % Ajout temporaire d'une valeur négative en fin de paquet pour borner les échantillons à
    % dupliquer.
    if CompressSamples(end) ~=-32768
        CompressSamples(end+1)  = -32768;
    end

    %% Réallocation des Nb Samples selon les paquets d'échantillons décompressés.
    ucNbSamples = zeros(size(NbSamples,1),1);
    Offset  = 0;
    for s=1:size(NbSamples,1)
        pppp    = CompressSamples(uint64(Offset+1):uint64(NbSamples(s)+Offset));
        Offset  = Offset + NbSamples(s);
        [uInf , ~, z]       = find(pppp < 0); %#ok<ASGLU>
        NbSamplesCompress   = pppp(uInf);
        NbSamplesCompress   = (NbSamplesCompress+32768)+1;
        % Somme des échantillons compressés + (nb d'échantillons
        % non-compressés + nb de paquets compressés)
        ucNbSamples(s)      = sum(NbSamplesCompress) + (NbSamples(s) - size(NbSamplesCompress, 1));
    end


    flag = 0;
catch ME
    flag = -1;
end
