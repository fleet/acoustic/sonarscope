function [flag, uCSamples] = unCompressSamples10011(CompressSamples)

flag = -1; %#ok<NASGU>

try

    %% La compression des échantillons d'angles n'est pas mise en oeuvre.
    % A optimiser.
    for i=1:size(CompressSamples, 1)
        pppp = bitget(uint32(CompressSamples(i)), 16:-1:1);
        uCSamples.AthwartShip(i) = bin2dec(sprintf('%-1d',pppp));
        if bitget(uCSamples.AthwartShip(i),16) == 1
            uCSamples.AthwartShip(i) = uCSamples.AthwartShip(i) - 2^16;
        end

        pppp = bitget(uint32(CompressSamples(i)), 31:-1:17);
        uCSamples.AlongShip(i)   = bin2dec(sprintf('%-1d',pppp));
        if bitget(uCSamples.AlongShip(i),15) == 1
            uCSamples.AlongShip(i) = uCSamples.AlongShip(i) - 2^15;
        end
    end

    
    flag = 0;
catch ME %#ok<NASGU>
    flag = -1;
    uCSamples = [];
end