function [flag, uCSamples] = unCompressSamples10040(CompressSamples)

flag = -1; %#ok<NASGU>

try

    % Ajout temporaire d'une valeur n�gative en fin de paquet pour borner les �chantillons �
    % dupliquer.
    flagAjoutNaN = 0;
    if CompressSamples(end) ~=-32768
        flagAjoutNaN            = 1;
        CompressSamples(end+1)  = -32768;
    end
    

    %% D�compression des samples
    % CompressSamples = double(CompressSamples);
    uInf     = find(CompressSamples < 0);
    uSup     = find(CompressSamples >= 0); %
    
    % Conversion des champs en valeurs naturelles (au facteur d'�chelle
    % pr�s). On ne traite que les valeurs n�gatives, les valeurs positives
    % sont conserv�es tel quel dans le fichier.
    % CompressSamples(uSup)     = bitor(uint16(typecast(single(CompressSamples(uSup)),'single')),32768);
    % CompressSamples(uSup)     = single(CompressSamples(uSup))-32768*2;
    uSup10000       = find(CompressSamples >= 10000); %Valeur arbitraire correspondant � +100dB
    pppp            = CompressSamples(uSup10000);
    pppp            = bitor(uint16(pppp),32768);
    pppp            = single(pppp)-32768*2;
    CompressSamples(uSup10000) = pppp;
    
    % Allocation d'une matrice de la taille des �chantillons
    % non-compress�s.
    NbSamplesInfThres   = CompressSamples(uInf);
    NbSamplesInfThres   = int32((NbSamplesInfThres+32768)+1);
    SamplesSupThres     = CompressSamples(uSup); %#ok<FNDSB>
    
    % 'single' rajout� par JMA le 13/10/2010
    uCSamples = NaN(1, sum(NbSamplesInfThres)+size(SamplesSupThres,1), 'single');
    
    insertIn = uInf(1);
    sub = 1:(uInf(1)-1);
    uCSamples(sub) = CompressSamples(sub);
    for i=2:size(uInf,1)
        im1 = i-1;
        insertFromPrev = uInf(im1)+1;
        insertFrom     = uInf(i);
        insertInPrev   = insertIn + NbSamplesInfThres(im1);
        insertIn       = insertInPrev + (uInf(i)-uInf(im1)-1);
        uCSamples(insertInPrev:(insertIn-1)) = CompressSamples(insertFromPrev:(insertFrom-1));
    end
    
    
    % Suppression de l'�chantillon transitoire.
    if flagAjoutNaN == 1
        uCSamples(end) = [];
    end
    
    % Remplissage des valeurs rest�es � NaN par les valeurs de seuil
    % d'origine.
    sub = isnan(uCSamples);
    uCSamples(sub) = NaN;

    
    flag = 0;
catch ME %#ok<NASGU>
    flag = -1;
    uCSamples = [];
end