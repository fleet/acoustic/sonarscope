% Lecture et traitement d'un fichier OREtech brut
%
% Syntax
%   [...] = LecFicOre( nomFic, options )
%
% Input Arguments 
%   nomFic  : nom du fichier
%
% Name-Value Pair Arguments 
%   'B'	   : lecture de voie babord
%   'T'	   : lecture de voie tribord
%   'BT'   : lecture des voies babord et tribord
%   'S'	   : lecture du sondeur de sediment
%   'H'	   : Hauteur calculee a babord (m)
%   'F'	   : Frequence du sonar (kHz ?)
%   subLig : index des lignes a extraire
%
% Output Arguments
%   Varaiables au nombre et dans l'ordre des parametres optionnels demandes
%
% Examples
%   nomFic = '08171959.ore';
%   B = LecFicOre( nomFic, 'B', 1:500 );
%     fig = figure; imagesc(-B); colorbar; colormap(gray(256));
%
%   [T, H, F] = LecFicOre( nomFic, 'T', 'H', 'F', 1:500 );
%     figure; imagesc(-T); colorbar; axis xy; colormap(gray(256)); coef = 2 * F / 1500;
%     hold on; plot(H*coef, 1:size(H, 2),'r'); plot(H*coef, 1:size(H, 2),'g');
%
%   [BT, H, F] = LecFicOre( nomFic, 'BT', 'H', 'F', 1:500 ); coef = 2 * F / 1500;
%     fig = figure; imagesc(-BT); colorbar; axis xy; colormap(gray(256));
%     hold on; plot(2660-H*coef, 1:size(H, 2),'r'); plot(2660+H*coef, 1:size(H, 2),'g');
%
%   S = LecFicOre( nomFic, 'S', 1:500 );
%     fig = figure; imagesc(-S); colorbar; axis xy; colormap(gray(256));
%     hold on; plot(H*coef, 1:size(H, 2),'r');
%
% See also Ore2Sar navOre2ascii LecFicSar CorFicSar cl_car_nav OretechChantier Authors
% Authors : JMA
%------------------------------------------------------------------------------

function varargout = LecFicOre( nomFic, varargin )

%% Lecture du nombre d'octets du fichier

nbOcFic = sizeFic( nomFic );
if( nbOcFic == -1 )
	return;
end

%% Initialisations

nb_oc_header = 11944;
IndiceB = 0;
IndiceT = 0;
IndiceBT = 0;
IndiceS = 0;
IndiceH = 0;
IndiceF = 0;
nbEnr = nbOcFic / nb_oc_header;
stats = 0;

fprintf('Nombre d''headeregistrements : %d\n', nbEnr);

subLig = 1:nbEnr;

indiceOut = 1;
for i = 1:length(varargin)
	if( strcmp(varargin{i}, 'B'))
		IndiceB = indiceOut;
		indiceOut = indiceOut + 1;
	elseif( strcmp(varargin{i}, 'T'))
		IndiceT = indiceOut;
		indiceOut = indiceOut + 1;
	elseif( strcmp(varargin{i}, 'BT'))
		IndiceBT = indiceOut;
		indiceOut = indiceOut + 1;
	elseif( strcmp(varargin{i}, 'S'))
		IndiceS = indiceOut;
		indiceOut = indiceOut + 1;
	elseif( strcmp(varargin{i}, 'H'))
		IndiceH = indiceOut;
		indiceOut = indiceOut + 1;
	elseif( strcmp(varargin{i}, 'F'))
		IndiceF = indiceOut;
		indiceOut = indiceOut + 1;
	elseif isnumeric(varargin{i})	% C'est l'echantillonage des headeregistrements
		subLig = varargin{i};
	end
end

%% Ouverture du fichier

fidIn = fopen(nomFic, 'r');

sub = find(subLig <= nbEnr);
subLig = subLig(sub);

nLig = size(subLig, 2);

if( IndiceB ~= 0)
	B = zeros(nLig, 2660);
end
if( IndiceT ~= 0)
	T = zeros(nLig, 2660);
end
if( IndiceBT ~= 0)
	BT = zeros(nLig, 2660*2);
end
if( IndiceS ~= 0)
	S = zeros(nLig, 640);
end

H = zeros(1, nLig);

%% Lecture des infos

for i=1:nLig

	% Calcul de la position du debut de l'headeregistrement
	% --------------------------------------------------

	position = (subLig(i) - 1) * nb_oc_header;

	% Lecture de l'headeregistrement
	% ---------------------------

	status = fseek( fidIn, position, 'bof');
	header = (fread( fidIn, 25, 'char'))';

	% Recuperation des donnees
	% ------------------------

	x1 = header(1);
	x2 = header(2);
	year = x2*256 + x1;
	mon = header(3);
	day = header(4);
	hour = header(5);
	min = header(6);
	sec = header(7);
	hund = header(8);
	x1 = header(9);
	x2 = header(10);
	alt = x2*256 + x1;
	x1 = header(11);
	x2 = header(12);
	freq = x2*256 + x1;
	x1 = header(13);
	x2 = header(14);
	bort = x2*256 + x1;
	x1 = header(15);
	x2 = header(16);
	leng = x2*256 + x1;
	sss_gain = header(17);
	ssp_gain = header(18);
	x1 = header(19);
	x2 = header(20);
	bottom = x2*256 + x1;

	date      = dayJma2Ifr( day, mon, year );
	charDate  = dayIfr2str(date);
	heure     = hourHms2Ifr(hour, min, sec+hund/100);
	charHeure = hourIfr2str( heure );

	H(i) = (alt * 1500 / 2) / freq;

	reflecB = (fread( fidIn, 2660, 'uint16'))';
	reflecT = (fread( fidIn, 2660, 'uint16'))';
	Sondeur = (fread( fidIn, 640, 'uint16'))';

	if( IndiceB ~= 0)
		B(i,1:2660) = reflecB;
		BT(i,(2660+1):(2*2660)) = reflecT;
	end

	if( IndiceT ~= 0)
		T(i,(2660+1):(2*2660)) = reflecT;
	end

	% Lecture des 2 voies
	% -------------------

	if( IndiceBT ~= 0)
		BT(i,1:2660) = fliplr(reflecB);
		BT(i,(2660+1):(2*2660)) = reflecT;
	end

	% Lecture de la voie sondeur
	% --------------------------

	if( IndiceS ~= 0)
		S(i,:) = Sondeur;
	end

	% Affichage
	% ---------

	if(mod(i, 10) == 1 || i == nbEnr)
		 fprintf('%d/%d %d %s %s H=%f m\n', ...
		i, nLig, subLig(i), charDate, charHeure, H(i) );
	end
end

% Fermeture du fichier
% --------------------

fclose(fidIn);


% Sortie des parametres
% ---------------------

if( IndiceB ~= 0) 
	varargout{IndiceB} = B;
end

if( IndiceT ~= 0) 
	varargout{IndiceT} = T;
end

if( IndiceBT ~= 0) 
	varargout{IndiceBT} = BT;
end

if( IndiceS ~= 0) 
	varargout{IndiceS} = S;
end

if( IndiceH ~= 0) 
	varargout{IndiceH} = H;
end

if( IndiceF ~= 0) 
	varargout{IndiceF} = freq;
end
