% Camouflage d'un fichier OREtech brut en fichier SAR brut
%
% Syntax
%   Ore2Sar( nomFicOre, nomFicSar )
%
% Input Arguments 
%   nomFicOre : nom du fichier ORETech
%	nomFicSar : nom du fichier SAR
%
% Examples
%   nomFicOre = '08171959.ore';
%   nomFicSar = '0001.01.im';
%   Ore2Sar( nomFicOre, nomFicSar );
%
%     [BT, HB, HT] = LecFicSar( nomFicSar, 'BT', 'HB', 'HT' );
%     fig = figure; imagesc(-BT); colorbar; axis xy; colormap(gray(256)); SourisButtonClbk('Figure', fig)
%     hold on; plot(3000-HB/0.25, 1:size(HB, 2),'r'); plot(3000+HT/0.25, 1:size(HB, 2),'g');
%
%     [BT, H, F] = LecFicOre( nomFic, 'BT', 'H', 'F' ); coef = 2 * F / 1500;
%     fig = figure; imagesc(-BT); colorbar; axis xy; colormap(gray(256)); SourisButtonClbk('Figure', fig)
%     hold on; plot(2660-H*coef, 1:size(H, 2),'r'); plot(2660+H*coef, 1:size(H, 2),'g');
%
% See also LecFicOre cl_car_nav OretechChantier navOre2ascii  LecFicSar Authors
% Authors : JMA
%------------------------------------------------------------------------------


function Ore2Sar( nomFicOre, nomFicSar )

% Lecture du nombre d'octets du fichier

nbOcFic = sizeFic( nomFicOre );
if nbOcFic == -1
	return;
end

% Initialisations

nb_oc_header = 11944;
nb_oc_enr = 7644;
nbEnr = nbOcFic / nb_oc_header;

fprintf('Nombre d''enregistrements : %d\n', nbEnr);

%% Ouverture du fichier OREtech

fidOre = fopen(nomFicOre, 'r');

%% Creation du fichier SAR

fidSar = fopen(nomFicSar, 'w+');

% Lecture des infos

for i=1:nbEnr
	enr = uint8(zeros(1, nb_oc_enr));

	% Calcul de la position du debut de l'enregistrement

	position = (i - 1) * nb_oc_header;

	% Lecture de l'enregistrement

	status = fseek( fidOre, position, 'bof'); %#ok<NASGU>
	header = (fread( fidOre, 25, 'char'))';

	% R�cup�ration des donn�es

	x1 = header(1);
	x2 = header(2);
	year = x2*256 + x1;
	mon = header(3);
	day = header(4);
	hour = header(5);
	min = header(6);
	sec = header(7);
	hund = header(8);
	x1 = header(9);
	x2 = header(10);
	alt = x2*256 + x1;
	x1 = header(11);
	x2 = header(12);
	freq = x2*256 + x1;
	x1 = header(13);
	x2 = header(14);
	bort = x2*256 + x1; %#ok<NASGU>
	x1 = header(15);
	x2 = header(16);
	leng = x2*256 + x1; %#ok<NASGU>
	sss_gain = header(17); %#ok<NASGU>
	ssp_gain = header(18); %#ok<NASGU>
	x1 = header(19);
	x2 = header(20);
	bottom = x2*256 + x1;

	date      = dayJma2Ifr( day, mon, year );
	charDate  = dayIfr2str(date);
	heure     = hourHms2Ifr(hour, min, sec+hund/100);
	charHeure = hourIfr2str( heure );

	H(i) = (alt * 1500 / 2) / freq; %#ok<AGROW>

	% Lecture des donn�es sonar et sondeur

	reflecT = (fread( fidOre, 2660, 'uint16'))';
	reflecB = (fread( fidOre, 2660, 'uint16'))';
	Sondeur = (fread( fidOre, 640, 'uint16'))';

	% Filtrage des donn�es

	reflecB = reflecB / 4;
	reflecB(reflecB >=256) = 255;
	reflecT = reflecT / 4;
	reflecT(reflecT >=256) = 255;
	Sondeur = Sondeur / 4;
	Sondeur(Sondeur >=256) = 255;

	% Chargement des voies sonar et du sondeur

	enr(792:3791)  = [reflecB zeros(1,340)];
	enr(3832:6831) = [reflecT zeros(1,340)];
	enr(6871:6871+760-1) = [Sondeur zeros(1,120)];

	% Formatage des donn�es

	enr(1:7) = sprintf('$PRSUN,');
	k = 8;
	enr(k:k+8) = sprintf('%02d/%02d/%02d,',day, mon, year-1900);
	enr(k+9:k+21) = sprintf('%02d:%02d:%06.3f,',hour, min, sec+hund/100);

	enr(73:79) = sprintf('$PRVME,');
	k = 80;
	enr(k:k+8) = sprintf('%02d/%02d/%02d,',day, mon, year-1900);
	enr(k+9:k+21) = sprintf('%02d:%02d:%06.3f,',hour, min, sec+hund/100);
	enr(102:125) = sprintf('VTETHE, 0,  0,VTATTI, 0,');
	enr(126:153) = sprintf('%06.2f,%06.2f,%06.2f,%06.2f,', H(i), H(i), H(i), H(i));
	enr(177:189) = sprintf(',VTNAVS, 0, ,');
	k = 190;
	enr(k:k+8) = sprintf('%02d/%02d/%02d,',day, mon, year-1900);
	enr(k+9:k+21) = sprintf('%02d:%02d:%06.3f,',hour, min, sec+hund/100);

	enr(340:350) = sprintf(',VTNAVB, 0,');
	k = 351;
	enr(k:k+8) = sprintf('%02d/%02d/%02d,',day, mon, year-1900);
	enr(k+9:k+21) = sprintf('%02d:%02d:%06.3f.',hour, min, sec+hund/100);
	
	enr(456:463) = sprintf(',VTBORD,');
	enr(498:505) = sprintf(',VTSTAT,');
	enr(524:531) = sprintf(',VTSERV,');
	enr(599:606) = sprintf(',VTOFFS,');
	enr(648:655) = sprintf(',VTSUPP,');
	enr(728:735) = sprintf(',VTTELE,');
	enr(757:763) = sprintf('$SSSAB,');
	k = 764;
	enr(k:k+8) = sprintf('%02d/%02d/%02d,',day, mon, year-1900);
	enr(k+9:k+21) = sprintf('%02d:%02d:%06.3f.',hour, min, sec+hund/100);
	enr(3797:3803) = sprintf('$SSSAT,');
	k = 3804;
	enr(k:k+8) = sprintf('%02d/%02d/%02d,',day, mon, year-1900);
	enr(k+9:k+21) = sprintf('%02d:%02d:%06.3f.',hour, min, sec+hund/100);
	enr(6837:6843) = sprintf('$SDSND,');
	k = 6844;
	enr(k:k+8) = sprintf('%02d/%02d/%02d,',day, mon, year-1900);
	enr(k+9:k+21) = sprintf('%02d:%02d:%06.3f.',hour, min, sec+hund/100);

	enr(33:38) = sprintf('%06.2f', H(i));
	enr(40:45) = sprintf('%06.2f', H(i));
	
	enr(212:224) = sprintf('n,00,00.00000');	% Latitude
	enr(226:239) = sprintf('n,000,00.00000');	% Longitude
	enr(241:249) = sprintf('+%07.2f,', bottom);	% Immersion
	enr(281:340) = sprintf('000.0,00.0,000.00,000.00, 000.00, 00000.00, 00000.00,000,00,');	% Cap

	% Ecriture de l'enregistrement sur le fichier

	fwrite(fidSar, enr);	

	% Affichage

	if (mod(i, 10) == 1) || (i == nbEnr)
		 fprintf('%d/%d %s %s H=%f m\n',  i, nbEnr, charDate, charHeure, H(i) );
	end
end

%% Fermeture des fichiers

fclose(fidOre);
fclose(fidSar);
