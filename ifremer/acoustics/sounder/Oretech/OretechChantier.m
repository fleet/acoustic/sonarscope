% Mise en oeuvre de la conversion des donnees du sonar Oretech au format Sar pour la missin Medineth
%
% See also navOre2ascii Ore2Sar LecFicOre cl_car_nav  LecFicSar Authors
% Authors : JMA
%------------------------------------------------------------------------------

function OretechChantier(varargin)

help OretechChantier
return

% ---------------------------
% Extraction de la navigation

navOre2ascii( '/home1/doppler/augustin/Campagnes/Medineth/ORE/mnldt01.dat', '/home1/doppler/augustin/Campagnes/Medineth/donnees/mnldt01.ascii' );
navOre2ascii( '/home1/doppler/augustin/Campagnes/Medineth/ORE/mnldt02.dat', '/home1/doppler/augustin/Campagnes/Medineth/donnees/mnldt02.ascii' );
navOre2ascii( '/home1/doppler/augustin/Campagnes/Medineth/ORE/mnldt03.dat', '/home1/doppler/augustin/Campagnes/Medineth/donnees/mnldt03.ascii' );
navOre2ascii( '/home1/doppler/augustin/Campagnes/Medineth/ORE/mnldt04.dat', '/home1/doppler/augustin/Campagnes/Medineth/donnees/mnldt04.ascii' );
navOre2ascii( '/home1/doppler/augustin/Campagnes/Medineth/ORE/mnldt05.dat', '/home1/doppler/augustin/Campagnes/Medineth/donnees/mnldt05.ascii' );
navOre2ascii( '/home1/doppler/augustin/Campagnes/Medineth/ORE/mnldt06.dat', '/home1/doppler/augustin/Campagnes/Medineth/donnees/mnldt06.ascii' );
navOre2ascii( '/home1/doppler/augustin/Campagnes/Medineth/ORE/mnldt07.dat', '/home1/doppler/augustin/Campagnes/Medineth/donnees/mnldt07.ascii' );

% -----------------------------------------------------------------
% Concatenation de tous les fichiers de nav en 1 seul fichier ascii

!cat /home1/doppler/augustin/Campagnes/Medineth/donnees/mnldt*.ascii >  /home1/doppler/augustin/Campagnes/Medineth/donnees/Medineth.ascii

% ----------------------------------------------------------------------
% Conversion du fichier en ascii en fichier binaire par Trias ou Trismus

% La balle est dans ton camp

% -----------------------------------
% Navigation au format Trismus/Imagem

n1 = cl_trismus_nav('/home1/doppler/augustin/Campagnes/Medineth/donnees/Medineth.nav');
plot(n1, '+'); grid on;

% ----------------------------------
% Navigation au format Caraibes V2.0

a = cl_car_nav('/home1/doppler/augustin/Campagnes/Medineth/donnees/Medineth.nvi');
plot(a)


% ------------------------------------------
% Conversion du format Oretech au format Sar

Ore2Sar( '/home1/doppler/augustin/Campagnes/Medineth/ORE/08052348.ore', '/home1/doppler/augustin/Campagnes/Medineth/donnees/0001.01.im' );
Ore2Sar( '/home1/doppler/augustin/Campagnes/Medineth/ORE/08062140.ore', '/home1/doppler/augustin/Campagnes/Medineth/donnees/0002.01.im' );
Ore2Sar( '/home1/doppler/augustin/Campagnes/Medineth/ORE/08092055.ore', '/home1/doppler/augustin/Campagnes/Medineth/donnees/0003.01.im' );
Ore2Sar( '/home1/doppler/augustin/Campagnes/Medineth/ORE/08122256.ore', '/home1/doppler/augustin/Campagnes/Medineth/donnees/0004.01.im' );
Ore2Sar( '/home1/doppler/augustin/Campagnes/Medineth/ORE/08132220.ore', '/home1/doppler/augustin/Campagnes/Medineth/donnees/0005.01.im' );
Ore2Sar( '/home1/doppler/augustin/Campagnes/Medineth/ORE/08152137.ore', '/home1/doppler/augustin/Campagnes/Medineth/donnees/0006.01.im' );
Ore2Sar( '/home1/doppler/augustin/Campagnes/Medineth/ORE/08060235.ore', '/home1/doppler/augustin/Campagnes/Medineth/donnees/0007.01.im' );
Ore2Sar( '/home1/doppler/augustin/Campagnes/Medineth/ORE/08070036.ore', '/home1/doppler/augustin/Campagnes/Medineth/donnees/0008.01.im' );
Ore2Sar( '/home1/doppler/augustin/Campagnes/Medineth/ORE/08092351.ore', '/home1/doppler/augustin/Campagnes/Medineth/donnees/0009.01.im' );
Ore2Sar( '/home1/doppler/augustin/Campagnes/Medineth/ORE/08130152.ore', '/home1/doppler/augustin/Campagnes/Medineth/donnees/0010.01.im' );
Ore2Sar( '/home1/doppler/augustin/Campagnes/Medineth/ORE/08140117.ore', '/home1/doppler/augustin/Campagnes/Medineth/donnees/0011.01.im' );
Ore2Sar( '/home1/doppler/augustin/Campagnes/Medineth/ORE/08160331.ore', '/home1/doppler/augustin/Campagnes/Medineth/donnees/0012.01.im' );
Ore2Sar( '/home1/doppler/augustin/Campagnes/Medineth/ORE/08060540.ore', '/home1/doppler/augustin/Campagnes/Medineth/donnees/0013.01.im' );
Ore2Sar( '/home1/doppler/augustin/Campagnes/Medineth/ORE/08070629.ore', '/home1/doppler/augustin/Campagnes/Medineth/donnees/0014.01.im' );
Ore2Sar( '/home1/doppler/augustin/Campagnes/Medineth/ORE/08100248.ore', '/home1/doppler/augustin/Campagnes/Medineth/donnees/0015.01.im' );
Ore2Sar( '/home1/doppler/augustin/Campagnes/Medineth/ORE/08130449.ore', '/home1/doppler/augustin/Campagnes/Medineth/donnees/0016.01.im' );
Ore2Sar( '/home1/doppler/augustin/Campagnes/Medineth/ORE/08140414.ore', '/home1/doppler/augustin/Campagnes/Medineth/donnees/0017.01.im' );
Ore2Sar( '/home1/doppler/augustin/Campagnes/Medineth/ORE/08171959.ore', '/home1/doppler/augustin/Campagnes/Medineth/donnees/0018.01.im' );
Ore2Sar( '/home1/doppler/augustin/Campagnes/Medineth/ORE/08060844.ore', '/home1/doppler/augustin/Campagnes/Medineth/donnees/0019.01.im' );
Ore2Sar( '/home1/doppler/augustin/Campagnes/Medineth/ORE/08091801.ore', '/home1/doppler/augustin/Campagnes/Medineth/donnees/0020.01.im' );
Ore2Sar( '/home1/doppler/augustin/Campagnes/Medineth/ORE/08122002.ore', '/home1/doppler/augustin/Campagnes/Medineth/donnees/0021.01.im' );
Ore2Sar( '/home1/doppler/augustin/Campagnes/Medineth/ORE/08131939.ore', '/home1/doppler/augustin/Campagnes/Medineth/donnees/0022.01.im' );
Ore2Sar( '/home1/doppler/augustin/Campagnes/Medineth/ORE/08151848.ore', '/home1/doppler/augustin/Campagnes/Medineth/donnees/0023.01.im' );
Ore2Sar( '/home1/doppler/augustin/Campagnes/Medineth/ORE/08172243.ore', '/home1/doppler/augustin/Campagnes/Medineth/donnees/0024.01.im' );


% --------------------------------
% Traitement des fichiers Caraibes

% La balle est dans ton camp

% ---------------------------------------------
% Visualisation de l'image traitee par Caraibes

nomFic = '/home1/doppler/augustin/Campagnes/Medineth/caraibes/Medith0201.imo';
a = cl_car_ima(nomFic)
edit(a);
