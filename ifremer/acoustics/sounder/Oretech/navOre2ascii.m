% Extraction de la navigation d'un fichier sonar Oretech
%
% Syntax
%   navOre2ascii( nomFicOre, nomFicAscii )
% 
% Input Arguments 
%   nomFicOre   : nom du fichier ORETech
%   nomFicAscii : nom du fichier de navigation 
%
% Examples
%   nomFicOre   = 'mnldt02.dat';
%   nomFicAscii = '/tmp/pppp.dat';
%   navOre2ascii( nomFicOre, nomFicAscii );
%
% See also Ore2Sar LecFicOre cl_car_nav OretechChantier Authors
% Authors : JMA
% VERSION  : $Id: navOre2ascii.m,v 1.5 2002/06/20 11:32:31 augustin Exp $
%------------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HISTORIQUE DEVELOPPEMENT
%   02/11/00 - JMA - Creation
%   02/04/01 - JMA - Mise en ref.
% ----------------------------------------------------------------------------

function navOre2ascii( nomFicOre, nomFicAscii )

%% Ouverture du fichier OREtech

fidOre = fopen(nomFicOre, 'r');

%% Cr�ation du fichier ASCII

fidAscii = fopen(nomFicAscii, 'w+t');

%% Lecture de l'ent�te

header = fgetl(fidOre);
year = sscanf(header, '%d');
header = fgetl(fidOre);
truc = sscanf(header, '%*s %*s %*s %s');
month = sscanf(truc(3:4), '%d');
day = sscanf(truc(5:6), '%d');
header = fgetl(fidOre);
header = fgetl(fidOre);
header = fgetl(fidOre);
header = fgetl(fidOre);

i = 1;
heurePre = 0;
while 1
	line = fgetl(fidOre);
        if ~ischar(line) || isempty(line)
            break
        end

	% Lecture du fichier

	heure   = sscanf(line(1:2), '%d');
	if heure < heurePre
		day = day + 1;
	end
	heurePre = heure;

	minute  = sscanf(line(3:4), '%d');
	seconde = sscanf(line(5:6), '%d');
	degreLatiEngin = sscanf(line(8:9), '%d');
	minuteLatiEngin = sscanf(line(10:16), '%f');
	degreLongEngin = sscanf(line(18:19), '%d');
	minuteLongEngin = sscanf(line(20:26), '%f');

	degreLatiShip = sscanf(line(28:29), '%d');
	minuteLatiShip = sscanf(line(30:36), '%f');
	degreLongShip = sscanf(line(38:39), '%d');
	minuteLongShip = sscanf(line(40:46), '%f');

    % Ecriture du fichier
    
    fprintf(fidAscii, '         %02d/%02d/%04d %02d:%02d:%02d N%02d %06.4f E%03d  %06.4f\n', day, month, year, heure, minute, seconde, degreLatiEngin, minuteLatiEngin, degreLongEngin, minuteLongEngin);
    
    xEngin(i) = degreLongEngin + minuteLongEngin / 60;
    yEngin(i) = degreLatiEngin + minuteLatiEngin / 60;
    xShip(i)  = degreLongShip  + minuteLongShip  / 60;
    yShip(i)  = degreLatiShip  + minuteLatiShip  / 60;
    i = i+1;
    
    %fprintf(fidAscii, '%s %s   %s   %s\n', dayIfr2str(dayJma2Ifr(day, month, year)), hourIfr2str(hourHms2Ifr(heure, minute, seconde)), lat2str(degreLatiEngin+minuteLatiEngin/60), long2str(degreLongEngin+minuteLongEngin/60));
end

%% Fermeture des fichiers

fclose(fidOre);
fclose(fidAscii);

figure
plot(xEngin, yEngin,'r+'); grid on; hold on;
plot(xShip, yShip,'g+'); grid on; hold on;
title(nomFicOre);
