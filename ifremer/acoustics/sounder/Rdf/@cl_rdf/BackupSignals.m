function flag = BackupSignals(~, nomFic, nomSignals, varargin) % TODO varargin en principe pas utile

if ~iscell(nomFic)
    nomFic = {nomFic};
end

if ~iscell(nomSignals)
    nomSignals = {nomSignals};
end

N = length(nomFic);
str1 = 'Sauvegarde des signaux en cours.';
str2 = 'Backuping signals.';
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    a = cl_rdf('nomFic', nomFic{k});
    flag = BackupSignals_Attitude(a, nomSignals);
    if ~flag
        % Message SVP
    end
    flag = BackupSignals_Heading(a, nomSignals);
    if ~flag
        % Message SVP
    end
    
    % TODO : Navigation mais complexe �car x, y, ... avec projection ...
end
my_close(hw, 'MsgEnd')


function flag = BackupSignals_Attitude(this, nomSignals)

[nomDir, nom] = fileparts(this.nomFic);
nomFicXml = fullfile(nomDir, 'SonarScope', nom, 'Ssc_Attitude.xml');
if ~exist(nomFicXml, 'file')
    flag = 0;
    return
end

XML = xml_read(nomFicXml);
nomDirRacine = fullfile(nomDir, 'SonarScope', nom);

for k=1:length(nomSignals)
    % TODO strcmp_i_ ci apr�s car heave et Heave qui trainent
    if strcmpi(nomSignals{k}, 'heave') || strcmpi(nomSignals{k}, 'roll') || strcmpi(nomSignals{k}, 'pitch')
        kSignal = find(strcmpi({XML.Signals.Name}, nomSignals{k}));
        if isempty(kSignal)
            str1 = sprintf('Signal "%s" non trouv� dans "%s', nomSignals, nomFicXml);
            str2 = 'TODO';
            my_warndlg(Lang(str1,str2), 1);
            return
        end
        flag = BackupOriginalSignal(nomDirRacine, XML.Signals(kSignal), 'Ssc_Attitude');
        if ~flag
            str1 = sprintf('Le signal "%s" n''a pas pu �tre sauv� pour "%s".',  nomSignals{k}, nom);
            str2 = sprintf('Signal "%s" could not be saved for "%s".',  nomSignals{k}, nom);
            my_warndlg(Lang(str1,str2), 1);
        end
    end
end
flag = 1;


function flag = BackupSignals_Heading(this, nomSignals)

[nomDir, nom] = fileparts(this.nomFic);
nomFicXml = fullfile(nomDir, 'SonarScope', nom, 'Ssc_Heading.xml');
if ~exist(nomFicXml, 'file')
    flag = 0;
    return
end

XML = xml_read(nomFicXml);
nomDirRacine = fullfile(nomDir, 'SonarScope', nom);

for k=1:length(nomSignals)
    if strcmpi(nomSignals{k}, 'Heading')
        kSignal = find(strcmp({XML.Signals.Name}, nomSignals{k}));
        if isempty(kSignal)
            str1 = sprintf('Signal "%s" non trouv� dans "%s', nomSignals, nomFicXml);
            str2 = 'TODO';
            my_warndlg(Lang(str1,str2), 1);
            return
        end
        flag = BackupOriginalSignal(nomDirRacine, XML.Signals(kSignal), 'Ssc_Heading');
        if ~flag
            str1 = sprintf('Le signal "%s" n''a pas pu �tre sauv� pour "%s".',  nomSignals{k}, nom);
            str2 = sprintf('Signal "%s" could not be saved for "%s".',  nomSignals{k}, nom);
            my_warndlg(Lang(str1,str2), 1);
        end
    end
end
flag = 1;
