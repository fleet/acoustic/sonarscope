function flag = RDF_CheckHeading(this, nomFicSeul)

persistent FiltreHeading_persistent

Format1 = 'V�rification de la %s.\n\nUne fen�tre SignalScope va s''ouvrir, le signal de %s est en noir et une version filtr�e est en rouge. Zoomez sur le signal pour voir si la version filtr�e est meilleure (elle rem�die souvent � des marches d''escalier dues � une quantification de la mesure de positionnemen).\n\nSi vous conservez le filtre et que vous clickez sur Save et Quit c''est lui qui sera sauvegard�.';
Format2 = 'Checking %s.\n\nA SignalScope window is going to be open, %s signal is the black one, the red one is a filtered version. Zoom on the signal to check if the filter is efficient (it often solves problems of stair steps due to signal quantification).\n\nIf you conserve the filter AND you click on Save and Quit il will be the red curve which will be saved.';

% Format3 = 'V�rification de la %s.\n\nUne fen�tre SignalScope va s''ouvrir. Zoomez sur le signal pour voir si un filtrage s''impose.';
% Format4 = 'Checking %s.\n\nA SignalScope window is going to be opened. Zoom on the signal to check if a filter is necessary.';

%% Contr�le de la latitude du bateau

if isempty(FiltreHeading_persistent)
    Filtre = [];
else
    Filtre = FiltreHeading_persistent;
end
if isempty(Filtre)
    filterDataParametre = [];
    filtreOK = 0;
else
    filterDataParametre = FilterDataParametre('FilterType', FilterDataParametre.FilterNameList(1), ...
        'ButterworthWc', num2str(Filtre.Wc), 'ButterworthOrder', Filtre.Ordre);
    filtreOK = 1;
end

str1 = sprintf(Format1, 'Cap', 'Cap');
str2 = sprintf(Format2, 'Heading', 'Heading');
my_warndlg(Lang(str1,str2), 1);

[flag, DataHeading] = SSc_ReadDatagrams(this.nomFic, 'Ssc_Heading');
if ~flag
    return
end
T = datetime(DataHeading.timestamp, 'ConvertFrom', 'posixtime');

xSample = XSample('name', 'Samples',  'data', T);
ySample = YSample('Name', 'Heading', 'data', DataHeading.Heading, 'unit', 'deg');
signal = ClSignal('Name', nomFicSeul, 'xSample', xSample, 'ySample', ySample);
s = SignalDialog(signal, 'Title', 'Edit signal', 'cleanInterpolation', 1, 'filterParametreValue', Filtre);
s.openDialog();
if s.okPressedOut
    DataHeading.Heading = ySample.data;
end

FiltreHeading_persistent = s.filterDataParametre;
if filtreOK
    % Recup�ration du parametres de filtrage
    filterDataParametre = a.filterDataParametre;
    % Conversion FilterDataParametre en 'Filtre'
    type = 1;
    order = filterDataParametre.butterworthOrderParam.Value;
    wc    = filterDataParametre.butterworthWcParam.Value;
    Filtre = struct('Type', type, 'Ordre', order, 'Wc', wc);
    FiltreHeading_persistent = Filtre;
end

% Reflec = set(Reflec, 'SonarHeading', DataHeading.Heading);
flag = save_signal(this, 'Ssc_Heading', 'Heading', DataHeading.Heading);
