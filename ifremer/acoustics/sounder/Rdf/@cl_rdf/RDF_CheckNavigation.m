function flag = RDF_CheckNavigation(this, nomFicSeul, Carto)

%% Lecture de la navigation

[flag, Time, Latitude, Longitude] = read_navigation(this, Carto);
if ~flag
    return
end

%% Edition de la navigation

[flag, Lat, Lon] = CheckNavigation(Time, Latitude(:,1), Longitude(:,1), nomFicSeul);
if ~flag
    return
end

%% R�cup�ration de la navigation

Latitude(:,1)  = Lat;
Longitude(:,1) = Lon;

%% Sauvegarde de la navigation

% TODO : impossible � faire pour le moment car on ne sait pas d'o� vient la position et en plus il faut la reinterpoler dans la base de temps des enregistrements de nav
flag = save_signal(this, 'Ssc_Sample', 'Latitude',  Latitude);
if ~flag
    return
end
flag = save_signal(this, 'Ssc_Sample', 'Longitude', Longitude);
if ~flag
    return
end

%% Calcul de la distance interpings

%     distanceEntrePings = dist(Latitude(:,1), Longitude(:,1));