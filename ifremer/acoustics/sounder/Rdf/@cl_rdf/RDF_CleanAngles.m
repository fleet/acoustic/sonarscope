function flag = RDF_CleanAngles(this, InstallationParameters, Carto, firstTime)

%% Message d'avertissement

if firstTime
    str1 = 'Une fen�tre SonarScope va s''ouvrir, elle permet de nettoyer les angles (click droite sur la colormap) puis cliquez sur le bouton Save et ensuite le bouton Quit.';
    str2 = 'A SonarScope window is going to open, it will display the angles. You can click right on the colormap and clean up the angles on the "Save" button then the "Quit" button.';
    my_warndlg(Lang(str1,str2), 1);
end

%% Lecture du layer "Angles processed"

[flag, TetaProc] = read_Image(this, 'SelecImage', 3, ...
    'Carto', Carto, 'InstallationParameters', InstallationParameters);
if ~flag
    return
end

%% Appel de SonarScope bien conditionn� au niveau graphique

Temp = SonarScope(TetaProc, 'FigureName', 'Raw angles', 'ButtonSave', 1);
Mask2 = ~isnan(Temp(end));
clear Temp

%% Sauvegarde du masque dans le cache

flag = save_layer(this, 'Mask', Mask2);
if ~flag
    return
end
