function flag = RDF_CleanAnglesDiff(this, InstallationParameters, Carto, firstTime)

%% Lecture du layer "Angles processed"

[flag, TetaProc] = read_Image(this, 'SelecImage', 3, ...
    'Carto', Carto, 'InstallationParameters', InstallationParameters);
if ~flag
    return
end

%% Calcul de l'image des angles comme si c'�tait un sonar lat�ral

AngleFondPlat = sonar_lateral_emission(TetaProc);

%% Calcul de la diff�rence des angles

Diff = AngleFondPlat - TetaProc;
clear AngleFondPlat

%% Message d'avertissement

if firstTime
    str1 = 'Une fen�tre SonarScope va s''ouvrir, elle permet de nettoyer les r�sidus (angles-AnglesFondPlat) (click droite sur la colormap) puis cliquez sur le bouton Save et ensuite le bouton Quit.';
    str2 = 'A SonarScope window is going to open, it will display the residuals angles (angles-AnglesFlatBottom). You can click right on the colormap and clean up the angles on the "Save" button then the "Quit" button.';
    my_warndlg(Lang(str1,str2), 1);
end

%% Appel de SonarScope bien conditionn� au niveau graphique

Temp = SonarScope(Diff, 'FigureName', 'Residuals angles (angles-AnglesFlatBottom)', 'ButtonSave', 1);
Mask2 = ~isnan(Temp(end));
clear Temp

%% Sauvegarde du masque dans le cache

flag = save_layer(this, 'Mask', Mask2);
if ~flag
    return
end
