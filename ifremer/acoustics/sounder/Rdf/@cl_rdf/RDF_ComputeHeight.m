function flag = RDF_ComputeHeight(this, Reflec, Mask, firstTime)

% D�tection de la hauteur sur b�bord
if firstTime
    str1 = 'Une fen�tre SonarScope va s''ouvrir, elle permet de contr�ler la d�tection de hauteur � b�bord. Utilisez le module de d�tection de la hauteur dans le menu "Sonar Interf�rom�trique"  (consultez l''aide) puis cliquez sur le bouton Save et ensuite le bouton Quit.';
    str2 = 'A SonarScope window is going to open, please check the height on port side and correct it if necessary with the "Interferometric Sonar" menu (see help for it) then click on the "Save" button then the "Quit" button.';
    my_warndlg(Lang(str1,str2), 1);
end
XLim = Reflec.XLim;
Reflec2 = set_XLim(Reflec, [XLim(1) 0]);

% Appel de SonarScope bien conditionn� au niveau graphique
str1Height = 'SonarScope - Fen�tre cr��e pour la d�tection de hauteur';
str2Height = 'SonarScope - Height detection window';
Temp = SonarScope(Reflec2, 'visuProfilY', 1, 'profilY.Type', 'Height', 'visuProfilX', 1, ...
    'FigureName', Lang(str1Height,str2Height), 'ButtonSave', 1);

% R�cup�ration de la hauteur dans l'image
Height = get(Temp(end), 'SonarHeight');
clear Temp
% On recopie la hauteur b�bord dans tribord

% TODO : bidouille vec height bab et tri en attendant que �a soit bien
% g�r� dans SSc

HeightBab = Height(:,1);
HeightTri = Height(:,2);
if all(isnan(HeightTri))
    HeightTri = HeightBab;
end
Height(:,1) = HeightTri;

% Sauvegarde des 2 hauteur dans Reflec2
Reflec2 = set(Reflec2, 'SonarHeight', Height);

% D�tection de hauteur sur tribord
if firstTime
    str1 = 'Une fen�tre SonarScope va s''ouvrir, elle permet de contr�ler la d�tection de hauteur � tribord. Utilisez le module de d�tection de la hauteur dans le menu "Sonar Interf�rom�trique"  (consultez l''aide) puis cliquez sur le bouton Save et ensuite le bouton Quit.';
    str2 = 'A SonarScope window is going to open, please check the height on starboard side and correct it if necessary with the "Interferometric Sonar" menu (see help for it) then click on the "Save" button then the "Quit" button.';
    my_warndlg(Lang(str1,str2), 1);
end
Reflec2 = set_XLim(Reflec2, [0 XLim(2)]);

% Appel de SonarScope bien conditionn� au niveau graphique
Temp = SonarScope(Reflec2, 'visuProfilY', 1, 'profilY.Type', 'Height', 'visuProfilX', 1, ...
    'FigureName', Lang(str1Height,str2Height), 'ButtonSave', 1);

% R�cup�ration de la hauteur dans l'image
Height = get(Temp(end), 'SonarHeight');
clear Temp
HeightTri = Height(:,1);
Height(:,2) = HeightTri;
Height(:,1) = HeightBab;

% Sauvegarde de la hauteur dans toutes les instances
Mask = set_SonarHeight(Mask, Height);
Mask = set_XLim(Mask, XLim);

% Sauver le signal
flag = save_signal(this, 'Ssc_Sample', 'Height', [HeightBab HeightTri]);
if ~flag
    return
end

%     figure; plot(HeightBab, 'r'); grid on; hold on; plot(HeightTri, 'g');
%     title('Height detected on image'); legend({'Port', 'Starboard'})

M = get(Mask, 'Image');
x = get(Mask, 'x');
for iPing=1:size(M,1)
    %         sub = (x > HeightBab(iPing)) & (x < -HeightTri(iPing));
    sub = (x > HeightBab(iPing)) & (x < -HeightTri(iPing));
    M(iPing,sub) = false;
end

% Sauver le masque
flag = save_layer(this, 'Mask', M);
if ~flag
    return
end