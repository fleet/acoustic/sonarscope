function [flag, Carto, InstallationParameters] = RDF_Preprocessing_Step1(this, Carto, InstallationParameters, firstTime)

[~, nomFicSeul, Ext] = fileparts(this.nomFic);
nomFicSeul = [nomFicSeul Ext];

%% Lecture de l'image

[flag, a, Carto, InstallationParameters] = read_Image(this, 'SelecImage', [1 2 4], ...
    'Carto', Carto, 'InstallationParameters', InstallationParameters);
if ~flag
    return
end
if isempty(a)
    return
end
% a = optimiseMemory(a, 'SizeMax', 0);

Mask    = a(1);
TetaRaw = a(2);
Reflec  = a(3);
clear a

%% On recopie le layer TetaRaw dans TetaProc

flag = save_layer(this, 'TetaProc', get(TetaRaw, 'Image'));
if ~flag
    return
end

%% Affichage de l'image de r�flectivit� dans SonarScope pour v�rifier la hauteur

str1 = 'Voulez-vous masquer les donn�es dans la colonne d''eau ?';
str2 = 'Do you want to mask the the water column data ?';
[rep, flag] = my_questdlg(Lang(str1,str2));
if ~flag
    return
end
if rep == 1
	flag = RDF_ComputeHeight(this, Reflec, Mask, firstTime);
    if ~flag
        return
    end 
end

%% Nettoyage des angles

str1 = 'Voulez-vous nettoyer les angles ?';
str2 = 'Do you want to clean up the angles ?';
[rep, flag] = my_questdlg(Lang(str1,str2));
if ~flag
    return
end
if rep == 1
    flag = RDF_CleanAngles(this, InstallationParameters, Carto, firstTime);
    if ~flag
        return
    end 
end

%% Nettoyage sur la difference AngleSonar - Angle de synth�se

str1 = sprintf('Voulez-vous faire un nettoyage sur la diff�rence "Angle Data  moins  Angle hypoth�se fond plat) ?');
str2 = sprintf('Do you want to clean up the data using "Data Angles  minus  Flat bottom Angles" ?');
[rep, flag] = my_questdlg(Lang(str1,str2));
if ~flag
    return
end
if rep == 1
    flag = RDF_CleanAnglesDiff(this, InstallationParameters, Carto, firstTime);
    if ~flag
        return
    end 
end

%% Nettoyage de la navigation

str1 = 'Voulez-vous nettoyer la navigation ?';
str2 = 'Clean up the navigation ?';
[rep, flag] = my_questdlg(Lang(str1,str2));
if ~flag
    return
end
if rep == 1
    flag = RDF_CheckNavigation(this, nomFicSeul, Carto);
    if ~flag
        return
    end
end

%% Nettoyage du cap

str1 = 'Voulez-vous nettoyer le cap ?';
str2 = 'Clean the heading ?';
[rep, flag] = my_questdlg(Lang(str1,str2));
if ~flag
    return
end
if rep == 1
    flag = RDF_CheckHeading(this, nomFicSeul);
    if ~flag
        return
    end
end
