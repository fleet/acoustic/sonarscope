function [flag, Carto, InstallationParameters] = RDF_Preprocessing_Step2(this, Carto, InstallationParameters, CoefSigma)

%% Lecture de l'image

[flag, a, Carto, InstallationParameters] = read_Image(this, 'SelecImage', [1 2 3 4], ...
    'Carto', Carto, 'InstallationParameters', InstallationParameters);
if ~flag
    return
end
if isempty(a)
    return
end
% a = optimiseMemory(a, 'SizeMax', 0); % Comment� par JMA le 14/09/2016 (en test)

%% Tentative de comblage des angles : supprim� le 06/04/2011 car �a
%% g�n�rait plus d'inconv�nients que d'avantages. Il conviendrait cependant
%% de faire une sorte d'interpolation des distances transversales au moment
%% o� on fait la mosaique de r�flectivit�

%{
[~, Reflec, Carto, InstallationParameters] = read_Image(this, 'SelecImage', 4, 'UseMask', 0, ...
'Carto', Carto, 'InstallationParameters', InstallationParameters);
Angle = sonar_lateral_emission(Reflec);
layerMasque = ROI_auto(Angle, 1, 'MaskReflec', Angle, [], [-25 25], 1);
Mx = logical(get(layerMasque, 'Image'));
clear layerMasque

% X = get(Reflec, 'Image');
% clear Reflec
% Y = get(a(4), 'Image');
% Y(Mx) = X(Mx);
% clear X
% a(4) = set(a(4), 'Image', Y);
% clear Y

X = get(Angle, 'Image');
clear Angle
a(2) = masquage(a(2), 'LayerMask', a(1), 'valMask', 0);
Y = get(a(2), 'Image');
My = isnan(Y);
Y(Mx&My) = X(Mx&My);
clear X Mx

a(2) = set(a(2), 'Image', Y);

flag = save_layer(this, 'TetaProc', Y);
if ~flag
return
end
% clear Y
%}

%% Calcul de la bathy

ProfilCelerite = [];
% [b, ~, flag] = sonar_range2depth_Geoswath(a(end), a(3), ProfilCelerite, InstallationParameters);
[flag, b] = sonar_range2depth_GeoswathSep2016(a(3), ProfilCelerite, InstallationParameters, 'Mute', 1);
if ~flag
    return
end
% b = optimiseMemory(b, 'SizeMax', 0); % Comment� par JMA le 14/09/2016 (en test)

%% Filtrage des sondes

[flag, b] = sonar_RDF_FiltreDDP(b, 'Mute', 1);
if ~flag
    return
end

X = get(b(2), 'Image');
flag = save_layer(this, 'AcrossDist', X);
if ~flag
    return
end

X = get(b(3), 'Image');
flag = save_layer(this, 'AlongDist', X);
if ~flag
    return
end

X = get(b(1), 'Image');
flag = save_layer(this, 'Depth', X);
if ~flag
    return
end

%% Nettoyage de la bathy et mise � jour de la bathy

x = get(a(1), 'x');
Height = get(b(1), 'SonarHeight');
HeightBab = floor(-Height(:,1));
HeightTri = floor(-Height(:,2));
Milieu = find(x == 0);
clear x
for iPing=1:size(X,1)
    x0 = Milieu - HeightBab(iPing);
    subIn  = x0 - (1:100);
    subOut = x0 + (1:100);
    subOK = (subIn > 0);
    X(iPing,subOut(subOK)) = X(iPing,subIn(subOK));
    
    x0 = Milieu + HeightTri(iPing);
    subIn  = x0 + (1:100);
    subOut = x0 - (1:100);
    subOK = (subIn <= size(X,2));
    X(iPing,subOut(subOK)) = X(iPing,subIn(subOK));
end

%% Filtre antispike

b(1)= set(b(1), 'Image', X);
clear X

c = filterSpikeBathy(b(1), CoefSigma, 'OtherImages', 0);
clear b

%% Nettoyage suspect

M = get(c(1), 'Image');
clear c
for iPing=1:size(M,1)
    if ~isnan(HeightBab(iPing))
        x0 = Milieu - HeightBab(iPing);
        subOut = x0 + (1:100);
        M(iPing,subOut) = NaN;
    end
    
    if ~isnan(HeightTri(iPing))
        x0 = Milieu + HeightTri(iPing);
        subOut = x0 - (1:100);
        M(iPing,subOut) = NaN;
    end
end
M = isnan(M);
flag = save_layer(this, 'Mask', ~M);
if ~flag
    return
end
% clear M subIn subOut x0 Height HeightBab HeightTri Milieu


%% Test de filtrage des  angles

%{
Y(M) = NaN;
Ordre = 2;
Wc    = 0.1;
for iPing=1:size(Y,1)
yF = Y(iPing,:);
sub1 = find(~isnan(yF));
yF = filtrageButterSignal(yF(sub1), Wc, Ordre, 0);
Y(iPing,sub1) = yF;
end

flag = save_layer(this, 'TetaProc', Y);
if ~flag
return
end

% Il faut recalculer la bathy maintenant : faire une sous-fonction pour ne pas r�p�ter le code
%}


%{
%% Test correction angles

% a(2) = set(a(2), 'Image', Y);
n = size(Y,2);
subBab = 1:floor(n/2);
subTri = floor(n/2):n;
aBab = 0.95374;
bBab = -1.2415;
aTri = 0.94694;
bTri = 6.1911;
for iPing=1:size(Y,1)
yF = Y(iPing,:);
yF(subBab) = (yF(subBab) - bBab) / aBab;
yF(subTri) = (yF(subTri) - bTri) / aTri;
Y(iPing,:) = yF;
end

flag = save_layer(this, 'TetaProc', Y);
if ~flag
return
end
%}

% TODO : A remettre en service et tester sur donn�es Ifremer
%{

% TODO : ne pas prendre a(2) mais TetaProc
a(2) = set(a(2), 'Image', Y);

%% ReCalcul de la bathy

ProfilCelerite = [];
[b, ~, flag] = sonar_range2depth_Geoswath(a(2), ProfilCelerite, InstallationParameters);
if ~flag
return
end
% b = optimiseMemory(b, 'SizeMax', 0);


X = get(b(2), 'Image');
flag = save_layer(this, 'AcrossDist', X);
if ~flag
return
end

X = get(b(3), 'Image');
flag = save_layer(this, 'AlongDist', X);
if ~flag
return
end

X = get(b(1), 'Image');
flag = save_layer(this, 'Depth', X);
if ~flag
return
end
%}

%% Interpolation

% [flag, a, Carto, InstallationParameters] = read_Image(this, 'SelecImage', 2, ...
%     'Carto', Carto, 'InstallationParameters', InstallationParameters);
% if ~flag
%     return
% end
% % a = optimiseMemory(a, 'SizeMax', 0);
