function [flag, Carto, InstallationParameters] = RDF_Preprocessing_Step3(this, Carto, InstallationParameters)

%% Lecture de l'image

[flag, b, Carto, InstallationParameters] = read_Image(this, 'SelecImage', 1:7, ...
    'Carto', Carto, 'InstallationParameters', InstallationParameters);
if ~flag
    return
end
if isempty(b)
    return
end

% Z = get(a(3), 'Image');
A = get(b(3), 'Image');

r = get(b(3), 'x');
for iPing=1:size(A,1)
    Teta = A(iPing,:);
%     x = r .* sind(abs(Teta));
    z = abs(r .* cosd(Teta));
    % FigUtils.createSScFigure; PlotUtils.createSScPlot(r, Teta); grid on;
    % FigUtils.createSScFigure; PlotUtils.createSScPlot(r, tand(Teta)); grid on;
    % FigUtils.createSScFigure; PlotUtils.createSScPlot(r,sind(Teta)); grid on;
    % FigUtils.createSScFigure; PlotUtils.createSScPlot(r .* sind(abs(Teta)), z); grid on;
    
    subNaN    = find((r < 0) &  isnan(Teta));
    subNonNaN = find((r < 0) & ~isnan(Teta));
    if length(subNonNaN) < 2
        continue
    end
    z(subNaN) = interp1(subNonNaN, z(subNonNaN), subNaN);
    % FigUtils.createSScFigure; PlotUtils.createSScPlot(r .* sind(abs(Teta)), z); grid on;
    
    subNaN    = find((r > 0) &  isnan(Teta));
    subNonNaN = find((r > 0) & ~isnan(Teta));
    if length(subNonNaN) < 2
        continue
    end
    z(subNaN) = interp1(subNonNaN, z(subNonNaN), subNaN);
    % FigUtils.createSScFigure; PlotUtils.createSScPlot(r .* sind(abs(Teta)), z); grid on;
    
    Teta = acosd(z ./ r);
    sub = (Teta > 90);
    Teta(sub) = Teta(sub) - 180;
    
    %{
    % B�bord
    
    subNaN    = find((r < 0) &  isnan(Teta));
    subNonNaN = find((r < 0) & ~isnan(Teta));
    if length(subNonNaN) < 2
        continue
    end
    
    sub1 = subNonNaN(end-300):subNonNaN(end);
%   FigUtils.createSScFigure; PlotUtils.createSScPlot(sub1, Teta(sub1)); grid on;

    p = polyfit(sub1, Teta(sub1), 3);
    sub1 = (subNonNaN(end)+1):subNaN(end);
    Teta(sub1) = polyval(p, sub1);
    sub2 = (Teta(sub1) > 0);
    Teta(sub1(sub2)) = NaN;
%   FigUtils.createSScFigure; PlotUtils.createSScPlot(Teta); grid on;
    
     % Tribord
    
    subNaN    = find((r > 0) &  isnan(Teta));
    subNonNaN = find((r > 0) & ~isnan(Teta));
    
    sub1 = subNonNaN(1):subNonNaN(300);
%   FigUtils.createSScFigure; PlotUtils.createSScPlot(sub1, Teta(sub1)); grid on;

    p = polyfit(sub1, Teta(sub1), 3);
    sub1 = (subNaN(1):subNonNaN(1));
    Teta(sub1) = polyval(p, sub1);
    sub2 = (Teta(sub1) < 0);
    Teta(sub1(sub2)) = NaN;
%     FigUtils.createSScFigure; PlotUtils.createSScPlot(Teta); grid on; 
%}

    A(iPing,:) = Teta;
end

flag = save_layer(this, 'TetaProc', A);
if ~flag
    return
end

%{
M = get(b(1), 'Image');
M((M == 0) & ~isnan(A)) = 1;
flag = save_layer(this, 'Mask', M);
if ~flag
    return
end
%}

ProfilCelerite = [];
b(3) = set_Image(b(3), A);
[flag, c] = sonar_range2depth_GeoswathSep2016(b(3), ProfilCelerite, InstallationParameters, 'Mute', 1);
if ~flag
    return
end

X = get(c(2), 'Image');
flag = save_layer(this, 'AcrossDist', X);
if ~flag
    return
end

X = get(c(3), 'Image');
flag = save_layer(this, 'AlongDist', X);
if ~flag
    return
end

X = get(c(1), 'Image');
flag = save_layer(this, 'Depth', X);
if ~flag
    return
end
