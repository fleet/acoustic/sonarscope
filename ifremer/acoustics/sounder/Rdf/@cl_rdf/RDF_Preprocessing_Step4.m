function [flag, Carto, InstallationParameters] = RDF_Preprocessing_Step4(this, Carto, InstallationParameters, firstTime)

%% Lecture de l'image

[flag, a, Carto, InstallationParameters] = read_Image(this, 'SelecImage', 5, ...
    'Carto', Carto, 'InstallationParameters', InstallationParameters);
if ~flag
    return
end
if isempty(a)
    return
end

if firstTime
    str1 = 'Une fen�tre SonarScope va s''ouvrir, elle vous permet de contr�ler la donn�e de bathym�trie. Vous pouvez par exemple faire un nettoyage histogrammique de l''image   (consultez l''aide) puis cliquez sur le bouton Save et ensuite le bouton Quit.';
    str2 = 'A SonarScope window is going to open, You can use it to clean the data. You can for example do a histogram cleaning (see help for it) then click on the "Save" button then the "Quit" button.';
    my_warndlg(Lang(str1,str2), 1);
end

str1 = 'SonarScope - Fen�tre cr��e pour le contr�le de la bathym�trie';
str2 = 'SonarScope - Bathymetry detection control window';
pppp = SonarScope(a, 'FigureName', Lang(str1,str2), 'ButtonSave', 1);

[flag, indLayers] = listeLayersSynchronises(pppp, 1, 'memeDataType', 'IncludeCurrentImage', 'OnlyOneLayer', 1);
if flag
    M = get(pppp(indLayers), 'Image');
    M = isnan(M);
    flag = save_layer(this, 'Mask', ~M);
end
