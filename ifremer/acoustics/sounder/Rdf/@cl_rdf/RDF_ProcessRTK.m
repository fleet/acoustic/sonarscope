function [flag, Carto, RTKGeoid] = RDF_ProcessRTK(this, Carto, varargin)

flag = existCacheNetcdf(this.nomFic);
if flag
    [flag, Carto, RTKGeoid] = RDF_ProcessRTKNetcdf(this, Carto, varargin{:});
else
    [flag, Carto, RTKGeoid] = RDF_ProcessRTKXMLBin(this, Carto, varargin{:});
end


function [flag, Carto, RTKGeoid] = RDF_ProcessRTKNetcdf(this, Carto, varargin)

[varargin, RTKGeoid] = getPropertyValue(varargin, 'RTKGeoid', []); %#ok<ASGLU>

[flag, TNav, Latitude, Longitude, Carto, ~, ~, ~, ~, HeightRTK, HeightType] = read_navigation(this, Carto);
if ~flag
    return
end

subRTK = find((HeightType(:,1) == 4) | (HeightType(:,1) == 5)); % TODO : on fait simple : on oublie tribord !!!
if isempty(subRTK)
    str1 = sprintf('Il n''y a pas de donn�es RTK dans le fichiers %s', this.nomFic);
    str2 = sprintf('There is no RTK data in file %s', this.nomFic);
    my_warndlg(Lang(str1,str2), 0, 'Tag','NoRTKInThisFile', 'displayItEvenIfSSc', 1, 'TimeDelay', 60);
    flag = 0;
    return
end

HeightRTK = HeightRTK(:,1);
% RTK_FixPosition = zeros(size(HeightRTK));
% RTK_FixPosition(subRTK) = HeightType(subRTK,1);
TideRTK = NaN(size(HeightRTK), 'single');

nomDir = fileparts(this.nomFic);
[flag, TideRTK(subRTK), flagRTK, RTKGeoid, UseRTK] = processRTK(HeightRTK(subRTK), nomDir, ...
    Longitude(subRTK,1), Latitude(subRTK,1), 'RTKGeoid', RTKGeoid, 'UseRTK', 1);
if ~flag
    return
end
% HeightRTK = [HeightRTK HeightRTK];
% RTK_FixPosition = [RTK_FixPosition RTK_FixPosition]; % Commente par JMA le 11/03/2020

%% 

[nomDir, nom] = fileparts(this.nomFic);
nomFicNc = fullfile(nomDir, 'SonarScope', [nom, '.nc']);
if ~exist(nomFicNc, 'file')
    flag = 0;
    return
end

if ~NetcdfUtils.existGrp(nomFicNc, 'Height')
    ncID = netcdf.open(nomFicNc, 'WRITE');
    grpID = netcdf.defGrp(ncID, 'Height');
    dim = netcdf.defDim(grpID, 'NbSamples', length(TNav));
    
    deflateLevel = 1;
    
    NC_Storage = NetcdfUtils.getNetcdfClassname('double');
    varID = netcdf.defVar(grpID, 'Time', NC_Storage, dim);
    netcdf.defVarDeflate(grpID, varID, false, true, deflateLevel);
    
    NC_Storage = NetcdfUtils.getNetcdfClassname('single');
    varID = netcdf.defVar(grpID, 'Height', NC_Storage, dim);
    netcdf.defVarDeflate(grpID, varID, false, true, deflateLevel);
    
    NC_Storage = NetcdfUtils.getNetcdfClassname('single');
    varID = netcdf.defVar(grpID, 'Tide', NC_Storage, dim);
    netcdf.defVarDeflate(grpID, varID, false, true, deflateLevel);
    
    NC_Storage = NetcdfUtils.getNetcdfClassname('uint8');
    varID = netcdf.defVar(grpID, 'HeightType', NC_Storage, dim);
    netcdf.defVarDeflate(grpID, varID, false, true, deflateLevel);
    
    varID = netcdf.inqVarID(grpID, 'Time');
    NetcdfUtils.putVarVal(grpID, varID, datenum(TNav), 1, 'Unit', 'days since JC');
    
    varID = netcdf.inqVarID(grpID, 'Height');
    NetcdfUtils.putVarVal(grpID, varID, HeightRTK, 1, 'Unit', 'm');
    
    varID = netcdf.inqVarID(grpID, 'Tide');
    NetcdfUtils.putVarVal(grpID, varID, TideRTK, 1, 'Unit', 'm');
    
    varID = netcdf.inqVarID(grpID, 'HeightType');
    NetcdfUtils.putVarVal(grpID, varID, HeightType(:,1), 1, 'Unit', ' ');
    
    netcdf.close(ncID);
end

flag = 1;


function [flag, Carto, RTKGeoid] = RDF_ProcessRTKXMLBin(this, Carto, varargin)

[varargin, RTKGeoid] = getPropertyValue(varargin, 'RTKGeoid', []); %#ok<ASGLU>

[nomDir, nomFicSeul] = fileparts(this.nomFic);
nomDirRacine = fullfile(nomDir, 'SonarScope', nomFicSeul);

[flag, TNav, Latitude, Longitude, Carto, ~, ~, ~, ~, HeightRTK, HeightType] = read_navigation(this, Carto);
if ~flag
    return
end

subRTK = find((HeightType(:,1) == 4) | (HeightType(:,1) == 5)); % TODO : on fait simple : on oublie tribord !!!
if isempty(subRTK)
    str1 = sprintf('Il n''y a pas de donn�es RTK dans le fichiers %s', this.nomFic);
    str2 = sprintf('There is no RTK data in file %s', this.nomFic);
    my_warndlg(Lang(str1,str2), 0, 'Tag','NoRTKInThisFile', 'displayItEvenIfSSc', 1, 'TimeDelay', 60);
    flag = 0;
    return
end

HeightRTK = HeightRTK(:,1);
% RTK_FixPosition = zeros(size(HeightRTK));
% RTK_FixPosition(subRTK) = HeightType(subRTK,1);
TideRTK = NaN(size(HeightRTK), 'single');

[flag, TideRTK(subRTK), flagRTK, RTKGeoid, UseRTK] = processRTK(HeightRTK(subRTK), nomDir, ...
    Longitude(subRTK,1), Latitude(subRTK,1), 'RTKGeoid', RTKGeoid, 'UseRTK', 1);
if ~flag
    return
end
HeightRTK = [HeightRTK HeightRTK];
% RTK_FixPosition = [RTK_FixPosition RTK_FixPosition]; % Commente par JMA le 11/03/2020

%% Signal Tide

Data.Height     = HeightRTK(:,1); % TODO : traiter tribord aussi
Data.Tide       = NaN(size(Data.Height), 'single');
Data.Time       = datenum(TNav);
Data.HeightType = HeightType(:,1); % TODO : traiter tribord aussi

nbSamples = size(HeightRTK,1);

%% G�n�ration du XML SonarScope

Info.Title              = 'Height';
Info.Constructor        = 'Kongsberg';
Info.EmModel            = 'Geoswath';
% Info.SystemSerialNumber = Datagrams.SystemSerialNumber;
Info.NbSamples          = nbSamples;
Info.TimeOrigin         = '01/01/-4713';
Info.Comments           = 'Sensor sampling rate';
% Info.FormatVersion      = FormatVersion;

Info.Dimensions.NbSamples = nbSamples;

Info.Signals(1).Name          = 'Time';
Info.Signals(end).Dimensions  = 'NbSamples, 1'; % TODO : traiter tribord aussi
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = 'days since JC';
Info.Signals(end).FileName    = fullfile('Ssc_Height','Time.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'Height';
Info.Signals(end).Dimensions  = 'NbSamples, 1'; % TODO : traiter tribord aussi
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile('Ssc_Height','Height.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'HeightType';
Info.Signals(end).Dimensions  = 'NbSamples, 1'; % TODO : traiter tribord aussi
Info.Signals(end).Storage     = 'uint8';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_Height','HeightType.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'Tide';
Info.Signals(end).Dimensions  = 'NbSamples, 1'; % TODO : traiter tribord aussi
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_Height','Tide.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

%% Cr�ation du fichier XML d�crivant la donn�e

nomFicXml = fullfile(nomDirRacine, 'Ssc_Height.xml');
xml_write(nomFicXml, Info);
flag = exist(nomFicXml, 'file');
if ~flag
    messageErreur(nomFicXml)
    return
end

%% Cr�ation du r�pertoire Height

nomDirHeight = fullfile(nomDirRacine, 'Ssc_Height');
if ~exist(nomDirHeight, 'dir')
    status = mkdir(nomDirHeight);
    if ~status
        messageErreur(nomDirHeight)
        flag = 0;
        return
    end
end

%% Cr�ation des fichiers binaires des signaux

for k=1:length(Info.Signals)
    flag = writeSignal(nomDirRacine, Info.Signals(k), Data.(Info.Signals(k).Name));
    if ~flag
        return
    end
end

flag = 1;
