function RDF_RTK_Clean(~, ListFic)

if ~iscell(ListFic)
    ListFic = {ListFic};
end

Carto = [];

%% Import de la mar�e dans les fichiers .rdf

fig2 = [];
nbFic = length(ListFic);
str1 = 'V�rification de la donn�e Height RTK des .rdf';
str2 = 'Check RTK Height of .rdf files';
hw = create_waitbar(Lang(str1,str2), 'N', nbFic);
for k=1:nbFic
    my_waitbar(k, nbFic, hw);
    a = cl_rdf('nomFic', ListFic{k});
    
    [flag, Carto] = createRTKHeightIfDoesNotExist(a, Carto);
    if ~flag
        continue
    end
    
    %% Lecture de la hauteur RTK
    
    [flag, DataHeight] = SSc_ReadDatagrams(ListFic{k}, 'Ssc_Height');
    if ~flag
        str1 = sprintf('Le fichier "%s" n''a pas de datagrammes de "Height".', ListFic{k});
        str2 = sprintf('"%s" has no "Height" datagrams.', ListFic{k});
        my_warndlg(Lang(str1,str2), 0, 'Tag', 'DatagramHeightIsNoGood');
        continue
    end
            
    %% Lecture de la navigation
    
    [flag, T, ~, ~, Carto, ~, ~, ~, ~, HeightRTK, QualityRTK] = read_navigation(a, Carto);
    if ~flag
        str1 = sprintf('Le fichier "%s" n''a pas de datagrammes de "Position".', ListFic{k});
        str2 = sprintf('"%s" has no "Position" datagrams.', ListFic{k});
        my_warndlg(Lang(str1,str2), 0, 'Tag', 'DatagramPositionIsNoGood');
        continue
    end
    
    [~, nomFic2, Ext] = fileparts(ListFic{k});
    nomFic2 = [nomFic2 Ext]; %#ok<AGROW> % Engendrait un pb dans les modes de SignalDialog !!!
    fig2 = FigUtils.createSScFigure('Name', 'Ifremer SonarScope : RTK data cleaning');
    h1 = subplot(3,1,1); plot(T, QualityRTK(:,1), 'b*'); axis tight; grid on; ylabel('GPS IQ'); title(nomFic2)
    h2 = subplot(3,1,2); plot(T, HeightRTK(:,1),  'b');  axis tight; grid on; ylabel('GPS Height'); hold on;
    linkaxes([h1 h2], 'x'); axis tight; drawnow;
    
    %% Lecture de l'attitude
    
    [flag, DataAttitude] = SSc_ReadDatagrams( ListFic{k}, 'Ssc_Attitude');
    if flag
        T3 = DataAttitude.timestamp(:);
        T3 = datetime(T3, 'ConvertFrom', 'posixtime');
        h3 = subplot(3,1,3); plot(T3, -DataAttitude.heave(:),  'b');  axis tight; grid on; ylabel('MRU -Heave'); hold on;
        linkaxes([h1 h2 h3], 'x'); axis tight; drawnow;
    end
   
    %% Recopie du facteur de qualit� de Position dans Height
    
    QualityIndicator = QualityRTK(:,1); % TODO : traiter tribord aussi
    TimePosition     = T; % TODO : traiter tribord aussi
    TimeHeight       = TimePosition;
        
    HeightType = QualityIndicator;
    
    flag = save_signal(a, 'Ssc_Height', 'Time', datenum(TimePosition));
    if ~flag
        break
    end
    
    flag = save_signal(a, 'Ssc_Height', 'HeightType', uint8(HeightType));
    if ~flag
        break
    end
        
    %% Recherche des occurrences RTK
    
    subRTK = find((QualityIndicator == 4) | (QualityIndicator == 5));
    [subPosition45, subHeight45] = intersect3(TimePosition, TimeHeight(subRTK), TimeHeight(subRTK)); %#ok<ASGLU>
    
    if isempty(subPosition45)
        str1 = sprintf('Le facteur de qualit� RTK ne donne aucune mesure valid�e pour "%s"', ListFic{k});
        str2 = sprintf('RTK QualityFactor does not give any validated data for "%s"', ListFic{k});
        my_warndlg(Lang(str1,str2), 0, 'Tag', 'QualityIndicatorNoRTK');
        FigUtils.createSScFigure(54221);
        subplot(2,1,1); plot(QualityIndicator, '*'); grid on; title(Lang(str1,str2), 'Interpreter', 'none')
        subplot(2,1,2); plot(DataHeight.Height(:));  grid on; title(['Height for ' ListFic{k}], 'Interpreter', 'none')
        continue
    end
%   FigUtils.createSScFigure; plot(subPosition, DataPosition.AltitudeOfIMU(subPosition), 'b'); grid on; hold on; plot(subHeight, DataHeight.Height(subHeight), 'r')
    
    %% Division en sections
    
    DataHeight.Height = HeightRTK(:,1);
    val = DataHeight.Height;
    kFinSections = find(diff(subPosition45) ~= 1);
    kFinSections = kFinSections(:)';
    kDebSections = [1 kFinSections+1];
    kFinSections = [kFinSections length(subPosition45)]; %#ok<AGROW>
    for k2=1:length(kFinSections)
        subSection = subPosition45(kDebSections(k2):kFinSections(k2));
        Title = sprintf('Edit RTK %s - Section %d/%d', nomFic2, k2, length(kFinSections));
        T = DataHeight.Datetime(subSection);
        xSample = XSample('name', 'Pings',  'data', T);
        ySample = YSample('Name', 'Height', 'data', DataHeight.Height(subSection));
        signal = ClSignal('Name', Title, 'xSample', xSample, 'ySample', ySample);
        
        d = diff(T3);
        subd = (d > 0);
        Heave = interp1(T3(subd), DataAttitude.heave(subd), T);
        ySample = YSample('Name', 'MRU-Heave', 'data', -Heave);
        signal(2) = ClSignal('Name', Title, 'xSample', xSample, 'ySample', ySample);
        s = SignalDialog(signal, 'Title', 'Edit signal', 'cleanInterpolation', 1);
        s.openDialog();
        if s.okPressedOut
            valSection = signal(1).ySample.data;
            val(subSection) = valSection;
            plot(h2, T, valSection, 'r');
        end
    end
    
    %% Sauvegarde du signal si il a �t� modifi�
    
    if ~isequal(val, DataHeight.Height)
        DataHeight.Height(:) = NaN;
        DataHeight.Height = val;
    
        HeightType = zeros(1,DataHeight.Dimensions.NbSamples);
        HeightType(subPosition45) = QualityIndicator(subPosition45);
        
        % TODO : sauver Height dand le fichiers cache
        
        flag = save_signal(a, 'Ssc_Height', 'HeightType', HeightType);
        if ~flag
            break
        end
        
        flag = save_signal(a, 'Ssc_Height', 'Height',  DataHeight.Height);
        if ~flag
            break
        end
    end
    
    %% Fichier suivant ?
    
    flag = question_ContinueNextFile('k', k, 'N', nbFic);
    if ~flag
        break
    end
    my_close(fig2);
end
my_close(hw, 'MsgEnd');
if ishandle(fig2)
    my_close(fig2);
end
