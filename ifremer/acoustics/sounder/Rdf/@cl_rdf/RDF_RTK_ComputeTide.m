function RDF_RTK_ComputeTide(~, ListFicRdf)

if ~iscell(ListFicRdf)
    ListFicRdf = {ListFicRdf};
end

Carto = [];

nomDirRDF = fileparts(ListFicRdf{1});
[flag, InstallationParameters] = RDF_installationParameters('repImport', nomDirRDF);
if ~flag
    msg = sprintf('No installation parameter was found in directory "%s"', nomDirRDF);
    my_warndlg(msg, 1);
    return
end
Draught = InstallationParameters.Draught;

%% Import de la mar�e dans les fichiers .rdf

RTKGeoid   = [];
nbFic = length(ListFicRdf);
str1 = 'Calcul de la mar�e � partir de la donn�e Height RTK des .rdf';
str2 = 'Compute Tide signal from RTK Height of .rdf files';
hw = create_waitbar(Lang(str1,str2), 'N', nbFic);
for k=1:nbFic
    my_waitbar(k, nbFic, hw);
    a = cl_rdf('nomFic', ListFicRdf{k});
    
    [flag, Carto] = createRTKHeightIfDoesNotExist(a, Carto);
    if ~flag
        continue
    end
   
    nomDir = fileparts(ListFicRdf{k});
    
    %% Lecture de la hauteur RTK
    
    [flag, DataHeight] = SSc_ReadDatagrams(ListFicRdf{k}, 'Ssc_Height');
    if ~flag
        str1 = sprintf('Le fichier "%s" n''a pas de datagrammes de "Height".', ListFicRdf{k});
        str2 = sprintf('"%s" has no "Height" datagrams.', ListFicRdf{k});
        my_warndlg(Lang(str1,str2), 0, 'Tag', 'DatagramHeightIsNoGood');
        continue
    end
    
    %% Lecture et interpolation de la donn�e "Position"
    
    [flag, T, Latitude, Longitude, Carto, HeureGPS, HeightGPS, LatGPS, LonGPS, HeightRTK, QualityRTK] = read_navigation(a, Carto); %#ok<ASGLU>
    if ~flag
        str1 = sprintf('Le fichier "%s" n''a pas de datagrammes de "Position".', ListFicRdf{k});
        str2 = sprintf('"%s" has no "Position" datagrams.', ListFicRdf{k});
        my_warndlg(Lang(str1,str2), 0, 'Tag', 'DatagramPositionIsNoGood');
        continue
    end
    Latitude   = Latitude(:,1); % TODO : traiter tribord aussi
    Longitude  = Longitude(:,1);
%   HeightRTK  = HeightRTK(:,1);
%   QualityRTK = QualityRTK(:,1);

    %% Calcul de la mar�e
    
    if ~isempty(DataHeight) && ~isempty(DataHeight.Height)
        HeightType = DataHeight.HeightType(:);
        subRTK = (HeightType == 4) | (HeightType == 5);
        if ~isempty(find(subRTK, 1))
            % Avant le 05/03/2019
            %{
            RTK_HeightGGA = interp0(DataHeightRTKTime(subRTK), DataHeight.Height(subRTK), DataHeight.Time.time Mat);
            [flag, TideRTK, flagRTK, RTKGeoid] = processRTK(RTK_HeightGGA, nomDir, Longitude, Latitude, ...
                'RTKGeoid', RTKGeoid, 'UseRTK', 1); %#ok<ASGLU>
            %}
            
             % Apr�s le 05/03/2019
            [flag, TideRTK, flagRTK, RTKGeoid] = processRTK(DataHeight.Height, nomDir, Longitude, Latitude, ...
                'RTKGeoid', RTKGeoid, 'UseRTK', 1); %#ok<ASGLU>
            if ~flag
                continue
            end
            TideRTK(~subRTK) = NaN;
            TideRTK = TideRTK - Draught;
            
            %% Save Height dans le fichier cache
            
            flag = save_signal(a, 'Ssc_Height', 'Tide', TideRTK);
            if ~flag
                continue
            end
        end
    end
end
my_close(hw, 'MsgEnd')
