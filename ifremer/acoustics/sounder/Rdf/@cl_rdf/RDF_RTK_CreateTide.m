function RDF_RTK_CreateTide(~, ListFicRdf, nonFicTideIn, nomFicTideOut)

if ~iscell(ListFicRdf)
    ListFicRdf = {ListFicRdf};
end

Carto = [];
RTKGeoid = [];

%% Import de la mar�e dans les fichiers .rdf

TDeb       = [];
Time       = {};
Height     = {};
Tide       = {};
HeightType = {};
nbFic = length(ListFicRdf);
str1 = 'V�rification de la donn�e Height RTK des .rdf';
str2 = 'Check RTK Height of .rdf files';
hw = create_waitbar(Lang(str1,str2), 'N', nbFic);
for k=1:nbFic
    my_waitbar(k, nbFic, hw);
    a = cl_rdf('nomFic', ListFicRdf{k});
    
	[~, Carto, RTKGeoid] = createRTKHeightIfDoesNotExist(a, Carto, 'RTKGeoid', RTKGeoid);
    
    %% Lecture de la hauteur RTK
    
    [flag, DataHeight] = SSc_ReadDatagrams(ListFicRdf{k}, 'Ssc_Height');
    if ~flag
        str1 = sprintf('Le fichier "%s" n''a pas de datagrammes de "Height".', ListFicRdf{k});
        str2 = sprintf('"%s" has no "Height" datagrams.', ListFicRdf{k});
        my_warndlg(Lang(str1,str2), 0, 'Tag', 'DatagramHeightIsNoGood');
        continue
    end
    
    Time{end+1}       = DataHeight.Time.timeMat;  %#ok<AGROW> % TODO : DataHeight.Datetime tester quand il y aura plusieurs fichiers de mar�e
    Height{end+1}     = DataHeight.Height(:);     %#ok<AGROW>
    Tide{end+1}       = DataHeight.Tide(:);       %#ok<AGROW>
    HeightType{end+1} = DataHeight.HeightType(:); %#ok<AGROW>
    TDeb(end+1)       = Time{end}(1);             %#ok<AGROW>
end
my_close(hw, 'MsgEnd')

[~, ordre] = sort(TDeb);
Time       = Time(ordre); % TODO : Tester quand Datetime sera branch�
% Height     = Height(ordre);
Tide       = Tide(ordre);
HeightType = HeightType(ordre);

TimeRTK    = vertcat(Time{:});
% Height     = vertcat(Height{:});
TideRTK    = vertcat(Tide{:});
HeightType = vertcat(HeightType{:});

Flags = (HeightType == 4) - (HeightType == 5);
subNaN = find(~Flags);

% FigUtils.createSScFigure;
% PlotUtils.createSScPlot(TimeRTK, TideRTK); grid on; hold on;
% PlotUtils.createSScPlot(TimeRTK(subRetour), TideRTKRetour(subRetour), 'r')

%% Lecture de la mar�e d'entr�e

if ~isempty(subNaN) && ~isempty(nonFicTideIn)
    [flag, TideIn] = read_Tide(nonFicTideIn);
    if ~flag
        return
    end
    
    %% On compl�te la mar�e RTK par la mar�e classique
    
    TideRTK(subNaN) = interp1(TideIn.Time.timeMat, TideIn.Value, TimeRTK(subNaN));
%     TideRTK(subNaN) = interp1(TideIn.Datetime, TideIn.Value, TimeRTK(subNaN));
end

%% Ecriture du fichier de mar�e

fid = fopen(nomFicTideOut, 'w+t');
if fid == -1
    messageErreurFichier(nomFicTideOut, 'WriteFailure');
    return
end
dateMat = datevec(TimeRTK);
year    = dateMat(:,1);
ms      = dateMat(:,end)-floor(dateMat(:,end));
dateMat(:,1)     = dateMat(:,3);
dateMat(:,3)     = year;
dateMat(:,end)   = floor(dateMat(:,end));
dateMat(:,end+1) = floor(ms*1000);
dateMat          = cat(2, dateMat, TideRTK);
dataOk = ~isnan(TideRTK);
dateMat = dateMat(dataOk,:);
fprintf(fid, '%02d/%02d/%4d  %02d:%02d:%02d.%03d  %f\n', dateMat');
fclose(fid);
