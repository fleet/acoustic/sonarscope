function RDF_RTK_EditTide(~, ListFicRdf)

if ~iscell(ListFicRdf)
    ListFicRdf = {ListFicRdf};
end

%% Import de la mar�e dans les fichiers .rdf

nbFic = length(ListFicRdf);
str1 = 'Edition de la mar�e des .rdf';
str2 = 'Edit Tide signal of .rdf files';
hw = create_waitbar(Lang(str1,str2), 'N', nbFic);
for k=1:nbFic
    my_waitbar(k, nbFic, hw);
    a = cl_rdf('nomFic', ListFicRdf{k}); %#ok<NASGU> 
    
    [flag, DataHeight] = SSc_ReadDatagrams(ListFicRdf{k}, 'Ssc_Height');
    if ~flag
        return
    end
    
    %{
    h3(1) = subplot(3,1,1); PlotUtils.createSScPlot(Time, DataHeight.HeightType, ['*' Color]); grid on; hold on; ylabel('GPS QI')
    h3(2) = subplot(3,1,2); PlotUtils.createSScPlot(Time, DataHeight.Height,     ['-' Color]); grid on; hold on; ylabel('GPS Height')
    h3(3) = subplot(3,1,3); PlotUtils.createSScPlot(Time, DataHeight.Tide,       ['-' Color]); grid on; hold on; ylabel('Tide')
    linkaxes(h3, 'x'); axis tight; drawnow;
    %}
    
    [~, nomFic] = fileparts(ListFicRdf{k});

    % Comment� car le trac� graphique n'est pas possible si les abscisses
    % sont de type "datetime" dans SignalDialog
    xSample(k) = XSample('name', 'time', 'data', DataHeight.Datetime); %#ok<AGROW>

%     xSample(k) = XSample('name', 'time', 'data', datenum(DataHeight.Datetime)); %#ok<AGROW>

    ySample(k) = YSample('Name', nomFic, 'data', DataHeight.Tide, 'unit', 'm'); %#ok<AGROW>
end
my_close(hw, 'MsgEnd')

signal = ClSignal('Name', nomFic, 'xSample', xSample, 'ySample', ySample);

s = SignalDialog(signal, 'Title', 'Edit signal', 'cleanInterpolation', 1);
s.openDialog();
if s.okPressedOut
    s = s.signalContainerList.signalList;
    for k=1:length(s)
        a = cl_rdf('nomFic', ListFicRdf{k});
        [flag, DataHeight] = SSc_ReadDatagrams(ListFicRdf{k}, 'Ssc_Height');
        if ~flag
            return
        end
        DataHeight.Value = s(k).ySample.data;
        flag = save_signal(a, 'Ssc_Height', 'Tide', DataHeight.Value);
        if ~flag
            return
        end
    end
end

