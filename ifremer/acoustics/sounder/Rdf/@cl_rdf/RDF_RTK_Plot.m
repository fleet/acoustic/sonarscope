function RDF_RTK_Plot(~, ListFic)

if ~iscell(ListFic)
    ListFic = {ListFic};
end

Carto    = [];
RTKGeoid = [];

%% Trac� de la mar�e RTK des fichiers RDF

Fig = [];
Color = 'brgkm';
nbFic = length(ListFic);
str1 = 'Visualisation de la mar�e RTK des .rdf';
str2 = 'Plot RTK tide of .rdf files';
hw = create_waitbar(Lang(str1,str2), 'N', nbFic);
for k=1:nbFic
    my_waitbar(k, nbFic, hw);
    a = cl_rdf('nomFic', ListFic{k});
	[flag, Carto, RTKGeoid] = createRTKHeightIfDoesNotExist(a, Carto, 'RTKGeoid', RTKGeoid);
    if flag
        Fig = plot_RTKHeight(a, 'Fig', Fig, 'Color', Color(1 + mod(k-1,length(Color))));
    end
end
my_close(hw, 'MsgEnd');
