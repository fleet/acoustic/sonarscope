function RDF_RTK_Raz(~, ListFicRdf)

if ~iscell(ListFicRdf)
    ListFicRdf = {ListFicRdf};
end

Carto = [];
RTKGeoid = [];

%% Import de la mar�e dans les fichiers .rdf

nbFic = length(ListFicRdf);
str1 = 'RAZ des donn�es RTK des .rdf';
str2 = 'Reset RTK data of .rdf files.';
hw = create_waitbar(Lang(str1,str2), 'N', nbFic);
for k=1:nbFic
    my_waitbar(k, nbFic, hw);
    a = cl_rdf('nomFic', ListFicRdf{k});
    [~, Carto, RTKGeoid] = createRTKHeightIfDoesNotExist(a, Carto, 'RTKGeoid', RTKGeoid, 'flagRAZ', 1);
end
my_close(hw, 'MsgEnd')
