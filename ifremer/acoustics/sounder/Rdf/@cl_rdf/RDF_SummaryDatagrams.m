function RDF_SummaryDatagrams(this, nomFic, nomFicSummary) %#ok<INUSL>
 
if ~iscell(nomFic)
    nomFic = {nomFic};
end

fid = fopen(nomFicSummary, 'w+t');
if fid == 1
    messageErreurFichier(nomFicSummary, 'WriteFailure');
end

N = length(nomFic);
str1 = 'Traitement en cours';
str2 = 'Processing';
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    a = cl_rdf('nomFic', nomFic{k});
    summary(a, fid)
end
fclose(fid);
my_close(hw, 'MsgEnd')

try
    winopen(nomFicSummary)
catch %#ok<CTCH>
end
