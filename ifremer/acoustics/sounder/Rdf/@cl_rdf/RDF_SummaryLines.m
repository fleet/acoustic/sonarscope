function RDF_SummaryLines(this, nomFic, nomFicSummary, listeItems)
 
if ~iscell(nomFic)
    nomFic = {nomFic};
end

%% Comma or not comma ?

sep = getCSVseparator;

%% Cr�ation du r�sum�

strSummary = RDF_getListeSummaryLines(this);
strSummary = strSummary(listeItems);

fid = fopen(nomFicSummary, 'w+t');
if fid == 1
    messageErreurFichier(nomFicSummary, 'WriteFailure');
end
for k=1:length(strSummary)
    fprintf(fid, '%s%s', strSummary{k}, sep);
end
fprintf(fid, '\n');

N = length(nomFic);
str1 = 'Traitement en cours';
str2 = 'Processing';
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k1=1:N
    my_waitbar(k1, N, hw)
    a = cl_rdf('nomFic', nomFic{k1});
    [flag, strFile] = summary_Lines(a);
    strFile = strFile(listeItems);
    if flag
        for k2=1:length(strFile)
            fprintf(fid, '%s%s', strFile{k2}, sep);
        end
        fprintf(fid, '\n');
        strSummary(end+1,:) = strFile; %#ok<AGROW>
    end
end
fclose(fid);
my_close(hw, 'MsgEnd')

%% Affichage des r�sultat dans une table

figure;
t = uitable;
set(t, 'ColumnName', strSummary(1,:), 'Data', strSummary(2:end,:))
set(t, 'RearrangeableColumns', 'On')
set(t, 'Unit', 'normalized', 'Position', [0 0 1 1])

%% Affichage des r�sultats dans un �diteur

try
    winopen(nomFicSummary)
catch %#ok<CTCH>
end
