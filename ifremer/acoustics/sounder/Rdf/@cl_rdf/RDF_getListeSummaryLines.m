function strSummary = RDF_getListeSummaryLines(~)

strSummary = {};
strSummary{end+1} = 'File name';
strSummary{end+1} = 'Time beginning';
strSummary{end+1} = 'Time end';
strSummary{end+1} = 'Nb of pings';
strSummary{end+1} = 'Nb of samples';
strSummary{end+1} = 'PulseLength (median)';
strSummary{end+1} = 'Power (median)';
strSummary{end+1} = 'SideScanGain (median)';
strSummary{end+1} = 'Sound velocity (mean)';
strSummary{end+1} = 'Heading (median)';
% strSummary{end+1} = 'Depth (mean)';
strSummary{end+1} = 'Nb SoundSpeedProfiles';
strSummary{end+1} = 'Roll (mean)';
strSummary{end+1} = 'Roll (std)';
strSummary{end+1} = 'Pitch (mean)';
strSummary{end+1} = 'Pitch (std)';
strSummary{end+1} = 'Heave (mean)';
strSummary{end+1} = 'Heave (std)';
strSummary{end+1} = 'File size';

