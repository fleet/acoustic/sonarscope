function flag = RestoreSignals(~, nomFic, nomSignals)

if ~iscell(nomFic)
    nomFic = {nomFic};
end

if ~iscell(nomSignals)
    nomSignals = {nomSignals};
end

if isempty(nomSignals{1})
    return
end

N = length(nomFic);
str1 = 'Restauration des signaux en cours';
str2 = 'Restore signals';
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    a = cl_rdf('nomFic', nomFic{k});
    flag = RestoreSignals_Attitude(a, nomSignals);
    if ~flag
        % Message SVP
    end
    flag = RestoreSignals_Heading(a, nomSignals);
    if ~flag
        % Message SVP
    end
end
my_close(hw, 'MsgEnd')


function flag = RestoreSignals_Attitude(this, nomSignals)

[nomDir, nom] = fileparts(this.nomFic);
nomFicXml = fullfile(nomDir, 'SonarScope', nom, 'Ssc_Attitude.xml');
if ~exist(nomFicXml, 'file')
    flag = 0;
    return
end

XML = xml_read(nomFicXml);
nomDirRacine = fullfile(nomDir, 'SonarScope', nom);

for k=1:length(nomSignals)
    % TODO strcmpi
    if strcmpi(nomSignals{k}, 'heave') || strcmpi(nomSignals{k}, 'roll') || strcmpi(nomSignals{k}, 'pitch')
        kSignal = find(strcmpi({XML.Signals.Name}, nomSignals{k}));
        if isempty(kSignal)
            str1 = sprintf('Signal "%s" non trouv� dans "%s', nomSignals, nomFicXml);
            str2 = sprintf('Signal "%s" not found in "%s', nomSignals, nomFicXml);
            my_warndlg(Lang(str1,str2), 1);
            continue
        end
        
        flag = RestoreOriginalSignal(nomDirRacine, XML.Signals(kSignal));
        if ~flag
            str1 = sprintf('Le signal "%s" n''a pas pu �tre sauv� pour "%s". Il n''a sans doute pas �t� sauvegard� auparavent.',  nomSignals{k}, nom);
            str2 = sprintf('Signal "%s" could not be restored for "%s". It had not been probably saved previously.',  nomSignals{k}, nom);
            my_warndlg(Lang(str1,str2), 0);
        end

    end
end
flag = 1;

function flag = RestoreSignals_Heading(this, nomSignals)

[nomDir, nom] = fileparts(this.nomFic);
nomFicXml = fullfile(nomDir, 'SonarScope', nom, 'Ssc_Heading.xml');
if ~exist(nomFicXml, 'file')
    flag = 0;
    return
end

XML = xml_read(nomFicXml);
nomDirRacine = fullfile(nomDir, 'SonarScope', nom);

for k=1:length(nomSignals)
    if strcmp(nomSignals{k}, 'Heading')
        kSignal = find(strcmpi({XML.Signals.Name}, nomSignals{k}));
        if isempty(kSignal)
            str1 = sprintf('Signal "%s" non trouv� dans "%s', nomSignals, nomFicXml);
            str2 = sprintf('Signal "%s" not found in "%s', nomSignals, nomFicXml);
            my_warndlg(Lang(str1,str2), 1);
            continue
        end
        
        flag = RestoreOriginalSignal(nomDirRacine, XML.Signals(kSignal));
        if ~flag
            str1 = sprintf('Le signal "%s" n''a pas pu �tre sauv� pour "%s". Il n''a sans doute pas �t� sauvegard� auparavent.',  nomSignals{k}, nom);
            str2 = sprintf('Signal "%s" could not be restored for "%s". It had not been probably saved previously.',  nomSignals{k}, nom);
            my_warndlg(Lang(str1,str2), 0);
        end
    end
end
flag = 1;
