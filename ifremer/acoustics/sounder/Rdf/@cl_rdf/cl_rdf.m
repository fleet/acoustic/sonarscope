% Constructeur de cl_rdf (Container de donn�es contenue dans une .rdf)
%
% Syntax
%   a = cl_rdf(...)
% 
% Name-Value Pair Arguments
%   nomFic : Nom(s) du fichier .rdf
%  
% Output Arguments 
%   a : Instance de cl_rdf
%
% Examples
%   nomFic = getNomFicDatabase('EW150.rdf')
%   a = cl_rdf('nomFic', nomFic)
%
% See also plot_index histo_index Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = cl_rdf(varargin)

%% Definition de la structure

this.nomFic = '';

%% Creation de l'instance

this = class(this, 'cl_rdf');

%% On regarde si l'objet a ete cree uniquement pour atteindre une methode de la classe

if (nargin == 1) && isempty(varargin{1})
    return
end

%% Initialisation des valeurs transmises lors de l'appel au constructeur

this = set(this, varargin{:});
