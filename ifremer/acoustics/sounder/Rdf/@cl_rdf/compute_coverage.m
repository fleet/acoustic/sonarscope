function [flag, Y] = compute_coverage(this)

%% Lecture des Angles

[flag, DataSample] = SSc_ReadDatagrams(this.nomFic, 'Ssc_Sample', 'Memmapfile', -1);
if ~flag
    return
end
Y = NaN(DataSample.Dimensions.nbPings, 2, 'single');
for k=1:DataSample.Dimensions.nbPings
    X = DataSample.AcrossDist(k,:);
    ideb = find(~isnan(X), 1, 'first');
    ifin = find(~isnan(X), 1, 'last');
    if ~isempty(ideb)
        Y(k,:) = X([ideb ifin]);
    end
end

% Rajout� par JMA le 04/07/2013 pour donn�es Geoswatrh
subNaN = find(isnan(Y(:,1)));
if ~isempty(subNaN)
    subNonNaN = find(~isnan(Y(:,1)));
    if isempty(subNonNaN)
        str1 = sprintf('L''�tape 2 du preprocessing n''a pas encore �t� r�alis�e pour le fichier %s\nLa fauch�e va �tre remplac�e par la distance oblique maximale.', this.nomFic);
        str2 = sprintf('Preprocessing step 2 has not been done for file %s yet.\nThe swath width is replaced by the maximum range.', this.nomFic);
        my_warndlg(Lang(str1,str2), 0);
        
        [flag, Y] = compute_coverageSidescan(this, DataSample);
    else
        Y(subNaN,1) = interp1(subNonNaN, Y(subNonNaN,1), subNaN, 'linear', 'extrap');
        Y(subNaN,2) = interp1(subNonNaN, Y(subNonNaN,2), subNaN, 'linear', 'extrap');
    end
end


function [flag, Y] = compute_coverageSidescan(this, DataSample)

Y = [];

[flag, RDFFileHeader] = SSc_ReadDatagrams(this.nomFic, 'Ssc_FileHeader', 'XMLOnly', 1);
if ~flag
    return
end

Frequency = RDFFileHeader.Frequency / 1000; % en kHz
switch Frequency
    case 125
        Mode_1 = 1;
    case 250
        Mode_1 = 2;
    case 500
        Mode_1 = 3;
end

SonarDescription = cl_sounder('Sonar.Ident', 16, 'Sonar.Mode_1', Mode_1); %, 'Time', DataHeader.DateHeure(1));
resol = get(SonarDescription, 'Proc.RangeResol');

n = floor(DataSample.Dimensions.nbCol / 2);
AcrossDistance = (-n:n) * resol;
Y = NaN(DataSample.Dimensions.nbPings, 2, 'single');
for k=1:DataSample.Dimensions.nbPings
    X = DataSample.Reflectivity(k,:);
    ideb = find(~isnan(X), 1, 'first');
    ifin = find(~isnan(X), 1, 'last');
    if ~isempty(ideb)
        Y(k,:) = AcrossDistance([ideb ifin]);
    end
end
