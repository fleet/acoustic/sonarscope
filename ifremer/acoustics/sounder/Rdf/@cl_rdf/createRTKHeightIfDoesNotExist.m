function [flag, Carto, RTKGeoid] = createRTKHeightIfDoesNotExist(this, Carto, varargin)

[varargin, flagRAZ]  = getPropertyValue(varargin, 'flagRAZ',  false);
[varargin, RTKGeoid] = getPropertyValue(varargin, 'RTKGeoid', []); %#ok<ASGLU>

%% Gestion particuli�re des donn�es RTK (on se rapproche le plus possible du traitement de la donn�e RTK des sondeurs Kongsberg)

[nomDir, nomFicSeul] = fileparts(this.nomFic);
nomDirRacine = fullfile(nomDir, 'SonarScope', nomFicSeul);

%% Test existance du fichier

nomFicXml = fullfile(nomDirRacine, 'Ssc_Height.xml');
fileExists = exist(nomFicXml, 'file');

%% Cr�ation du fichier

if flagRAZ || ~fileExists
    [flag, Carto, RTKGeoid] = RDF_ProcessRTK(this, Carto, 'RTKGeoid', RTKGeoid);
    if ~flag
        return
    end
end

flag = 1;
