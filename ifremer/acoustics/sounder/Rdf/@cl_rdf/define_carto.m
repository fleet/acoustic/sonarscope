function Carto = define_carto(this, Carto)

C0 = cl_carto([]);

if isempty(Carto)
    [nomDirInitial, nomFicInitial] = fileparts(this.nomFic);
    nomFicCartoDefault = fullfile(nomDirInitial, ['Carto_' nomFicInitial '.xml']);
    if exist(nomFicCartoDefault, 'file')
        str1 = sprintf('Le fichier "%s" est interprété directement', nomFicCartoDefault);
        str2 = sprintf('File "%s" is used by default.', nomFicCartoDefault);
        my_warndlg(Lang(str1,str2), 0, 'Tag', 'FolderXmlTrouve', 'TimeDelay', 60);
        Carto = import_xml(C0, nomFicCartoDefault);
    else
        liste = listeFicOnDir(nomDirInitial, '.xml', 'Filtre', {'_Folder'; '_Directory'}, 'FiltreExclude', 'GeoswathParameters');
        switch length(liste)
            case 0
                nomFicCartoDefault = '';
            case 1
                nomFicCartoDefault = liste{1};
            otherwise
                str1 = 'Plusieurs fichiers implicites existent, sélectionnez en un.';
                str2 = 'Select one default file please.';
                [rep, flag] = my_listdlg(Lang(str1,str2), liste, 'SelectionMode', 'Single');
                if ~flag
                    return
                end
                nomFicCartoDefault = liste{rep};
        end
        
        if exist(nomFicCartoDefault, 'file')
            str1 = sprintf('Le fichier "%s" est interprété directement', nomFicCartoDefault);
            str2 = sprintf('File "%s" is used by default.', nomFicCartoDefault);
            my_warndlg(Lang(str1,str2), 0, 'Tag', 'FolderXmlTrouve', 'TimeDelay', 60);
            Carto = import_xml(C0, nomFicCartoDefault);
        else
            Carto = C0;
            Carto(:) = [];
       end
    end
end
