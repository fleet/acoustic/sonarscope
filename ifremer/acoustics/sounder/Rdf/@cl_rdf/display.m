% Representation externe d'une instance sur le terminal
%
% Syntax
%   display(this)
%
% Input Arguments 
%   this : une instance de cl_rdf
%
% Examples
%   nomFic = getNomFicDatabase('EW150.RDF')
%   a = cl_rdf('nomFic', nomFic);
%   display(a);
%
% See also cl_rdf cl_rdf/char Authors
% Authors : GLU
% ----------------------------------------------------------------------------

function display(this)

s = whos('this');
sz = s.size;
fprintf('Name\tSize\tClass\n');
fprintf('%s\t%s\t%s\n', inputname(1), num2strCode(sz), s.class);
disp(char(this));

