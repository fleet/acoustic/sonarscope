% Acces en lecture des proprietes d'une instance
%
% Syntax
%   [...] = get(this, ...)
% 
% Input Arguments 
%   this : Une instance de la classe cl_rdf
%
% Name-Value Pair Arguments
%   nomFic : Nom du fichier .rdf
%
% Output Arguments
%   PropertyValues associees aux PropertyNames passes en argument
% 
% Examples
%   nomFic = getNomFicDatabase('EW150.RDF')
%   a = cl_rdf('nomFic', nomFic)
%   nomFic    = get(a, 'nomFic')
%   nbPings   = get(a, 'nbPings')
%   nbColumns = get(a, 'nbColumns')
%
% See also cl_rdf cl_rdf/char Authors
% Authors : GLU
% ----------------------------------------------------------------------------

function varargout = get(this, varargin)

%% Boucle de parcours des arguments

varargout = {};
for k=1:length(varargin)
    switch varargin{k}
        case 'nomFic'
            varargout{end+1} = this.nomFic; %#ok<AGROW>
            
        case 'nbPings'
            [flag, DataSample] = SSc_ReadDatagrams(this.nomFic, 'Ssc_Sample', 'XMLOnly', 1);
            if flag
                varargout{end+1} = DataSample.Dimensions.nbPings; %#ok<AGROW>
            else
                varargout{end+1} = '0'; %#ok<AGROW>
            end
            
        case 'nbColumns'
            [flag, DataSample] = SSc_ReadDatagrams(this.nomFic, 'Ssc_Sample', 'XMLOnly', 1);
            if flag
                varargout{end+1} = DataSample.Dimensions.nbCol; %#ok<AGROW>
            else
                varargout{end+1} = '0'; %#ok<AGROW>
            end
            
        otherwise
            w = sprintf('cl_rdf/get %s non pris en compte', varargin{k});
            my_warndlg(w, 0);
    end
end
