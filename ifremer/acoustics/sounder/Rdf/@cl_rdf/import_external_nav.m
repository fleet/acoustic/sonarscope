function import_external_nav(~, nomFicRDF, nomFicNav, InstallationParameters)

if ~iscell(nomFicRDF)
    nomFicRDF = {nomFicRDF};
end

Carto = [];

N = length(nomFicRDF);
hw = create_waitbar('RDF navigation import.', 'N', N);
for k=1:N
    my_waitbar(k, N, hw);
    [flag, Carto, InstallationParameters] = import_external_nav_unitaire(nomFicRDF{k}, nomFicNav, Carto, InstallationParameters);
    if ~flag
        my_close(hw)
        return
    end
end
my_close(hw, 'MsgEnd')


function [flag, Carto, InstallationParameters] = import_external_nav_unitaire(nomFicRDF, nomFicNav, Carto, InstallationParameters)

%% Lecture de l'image

this = cl_rdf('nomFic', nomFicRDF);
subx = floor(linspace(1, get(this, 'nbColumns'), 20));
[flag, a, Carto, InstallationParameters] = read_Image(this, 'SelecImage', 1, 'subx', subx, ...
    'Carto', Carto, 'InstallationParameters', InstallationParameters);
if ~flag
    return
end
if isempty(a)
    return
end

nbRows = a.nbRows;
T = get(a, 'SonarTime');
T = datetime(T.timeMat, 'ConvertFrom', 'datenum');
T(end+1:(2*nbRows)) = NaT;
T = reshape(T, nbRows, 2);

LatitudeImage  = get(a, 'FishLatitude');
LongitudeImage = get(a, 'FishLongitude');

for k=1:length(nomFicNav)
    isCaraibes = is_CDF(nomFicNav);
    if isCaraibes
        
        %{
        % TODO : il faudrait tester cette lecture en lieu et place de ce
        % qui suit
        [flag, LatitudeNav, LongitudeNav, tNav] = lecFicNav(nomFicNav{k})
        if ~flag
            messageErreurFichier(nomFicNav{k}, 'ReadFailure');
            return
        end
        %}
        
        b = cl_netcdf('fileName', nomFicNav{k});
        % sk = get(a, 'skeleton');
        
        CIB_BLOCKS_FOR_UNLIMITED = get_valAtt(b, 'CIB_BLOCKS_FOR_UNLIMITED');
        if isempty(CIB_BLOCKS_FOR_UNLIMITED) || ...
            (~isempty(CIB_BLOCKS_FOR_UNLIMITED) && (CIB_BLOCKS_FOR_UNLIMITED == 1))
            mbPointCounter = get_valAtt(b, 'mbPointCounter');
            if mod(mbPointCounter, 2) == 1
                mbPointCounter = mbPointCounter - 1;
            end
            
            Date = get_value(b, 'mbDate', 'CheckCIB_BLOCK_DIM', 0);
            Time = get_value(b, 'mbTime', 'CheckCIB_BLOCK_DIM', 0);
            Latitude  = get_value(b, 'mbOrdinate', 'Convert', 'double', 'CheckCIB_BLOCK_DIM', 0);
            Longitude = get_value(b, 'mbAbscissa', 'Convert', 'double', 'CheckCIB_BLOCK_DIM', 0);
            
            Time = Time';
            Time = Time(1:mbPointCounter);
            ns2  = floor(mbPointCounter / 2);
            Time  = reshape(Time, 2, ns2);
            Time  = Time';
            Time = double(Time);
            
            Date = Date';
            Date = Date(1:mbPointCounter);
            Date  = reshape(Date, 2, ns2);
            Date  = Date';
            Date = double(Date);
            
            Latitude = Latitude';
            Latitude = Latitude(1:mbPointCounter);
            Latitude  = reshape(Latitude, 2, ns2);
            Latitude  = Latitude';
           
            Longitude = Longitude';
            Longitude = Longitude(1:mbPointCounter);
            Longitude = reshape(Longitude, 2, ns2);
            Longitude = Longitude';
        else
            Date = get_value(b, 'mbDate');
            Time = get_value(b, 'mbTime');
            Latitude  = get_value(b, 'mbOrdinate', 'Convert', 'double');
            Longitude = get_value(b, 'mbAbscissa', 'Convert', 'double');
        end
                
        tNavBab = datetimeTransition('timeIfr', Date(:,1), Time(:,1));
        tNavTri = datetimeTransition('timeIfr', Date(:,2), Time(:,2));
        
        clear Date Time
                
        LongitudeRDF(:,1) = my_interp1_longitude(tNavBab, Longitude(:,1), T(:,1));
        LatitudeRDF(:,1)  = my_interp1(          tNavBab, Latitude(:,1),  T(:,1));
        LongitudeRDF(:,2) = my_interp1_longitude(tNavTri, Longitude(:,2), T(:,2));
        LatitudeRDF(:,2)  = my_interp1(          tNavTri, Latitude(:,2),  T(:,2));
        
        subNonNaN = find(~isnan(LongitudeRDF(:,1)));
        if ~isempty(subNonNaN)
            LatitudeImage(subNonNaN, 1)  = LatitudeRDF(subNonNaN, 1);
            LongitudeImage(subNonNaN, 1) = LongitudeRDF(subNonNaN, 1);
        end
        subNonNaN = find(~isnan(LongitudeRDF(:,2)));
        if ~isempty(subNonNaN)
            LatitudeImage(subNonNaN, 2)  = LatitudeRDF(subNonNaN, 2);
            LongitudeImage(subNonNaN, 2) = LongitudeRDF(subNonNaN, 2);
            
            % figure; plot(LongitudeImage, LatitudeImage, '*'); grid on
        end
    else
    end
end

flag = save_signal(this, 'Ssc_Sample', 'Latitude',  LatitudeImage);
if ~flag
    return
end
    
flag = save_signal(this, 'Ssc_Sample', 'Longitude', LongitudeImage);
if ~flag
    return
end
