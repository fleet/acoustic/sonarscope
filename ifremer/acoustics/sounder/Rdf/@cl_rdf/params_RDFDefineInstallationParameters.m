function [flag, nomFicXml, repImport] = params_RDFDefineInstallationParameters(~, repImport)

nomFicXml = [];

str1 = 'S�lectionner le r�pertoire o� se trouvent les .rdf';
str2 = 'Please select the folder of the .rdf files';
[flag, repImport] = my_uigetdir(repImport, Lang(str1,str2));
if ~flag
    return
end
        
% nomFicListe = fullfile(repExport, 'GeoswathParameters_Directory.xml');
nomFicListe = fullfile(repImport, 'GeoswathParameters_Directory.xml');
str1 = 'Nom du fichier des param�tres d''installation du Geoswath';
str2 = 'Name of the Installation parameters file';
[flag, nomFicXml] = my_uiputfile('*.xml', Lang(str1,str2), nomFicListe);
if ~flag
    return
end
% repImport = fileparts(nomFicXml);
