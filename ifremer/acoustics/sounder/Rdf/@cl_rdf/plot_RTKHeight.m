%{
nomFic = 'xxx';
a = cl_rdf('nomFic', nomFic);
Fig = plot_RTKHeight(a)
%}

function Fig = plot_RTKHeight(this, varargin)

[varargin, Fig]   = getPropertyValue(varargin, 'Fig',   []);
[varargin, Color] = getPropertyValue(varargin, 'Color', []); %#ok<ASGLU>

[flag, DataHeight] = SSc_ReadDatagrams(this.nomFic, 'Ssc_Height');
if ~flag
    return
end

if isempty(Fig)
    Fig = FigUtils.createSScFigure;
else
    FigUtils.createSScFigure(Fig);
end

Time = DataHeight.Datetime;

h3(1) = subplot(3,1,1); PlotUtils.createSScPlot(Time, DataHeight.HeightType, ['*' Color]); grid on; hold on; ylabel('GPS QI')
h3(2) = subplot(3,1,2); PlotUtils.createSScPlot(Time, DataHeight.Height,     ['-' Color]); grid on; hold on; ylabel('GPS Height')
h3(3) = subplot(3,1,3); PlotUtils.createSScPlot(Time, DataHeight.Tide,       ['-' Color]); grid on; hold on; ylabel('Tide')
linkaxes(h3, 'x'); axis tight; drawnow;
