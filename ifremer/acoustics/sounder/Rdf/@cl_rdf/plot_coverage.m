% Trace de la navigation et de la couverture d'un fichier .all
%
% Syntax
%   plot_coverage(nomFic)
%
% Input Arguments
%   nomFic : Liste de fichiers .all
%
% Examples
%   nomDir = '???'
%   liste = listeFicOnDir2(nomDir, '.rdf')
%   plot_coverage(liste)
%
% See also cl_rdf plot_index Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function plot_coverage(~, nomFic, varargin)

[varargin, Carto] = getPropertyValue(varargin, 'Carto', []);
[varargin, Fig]   = getPropertyValue(varargin, 'Fig',   []); %#ok<ASGLU>

if ~iscell(nomFic)
    nomFic = {nomFic};
end

this = cl_rdf([]);
this(:) = [];
for k=1:length(nomFic)
    a = cl_rdf('nomFic', nomFic{k});
    if isempty(a)
        %         str1 = sprintf('%s n''a pas pu �tre d�cod�.', nomFic{k});
        %         str2 = sprintf('%s could not be loaded.', nomFic{k});
        %         my_warndlg(Lang(str1,str2), 0);
        continue
    end
    this(end+1) = a; %#ok<AGROW>
end

Ident = rand(1);

if isempty(Fig)
    Fig = figure('name', 'Position datagrams');
else
    figure(Fig);
end
set(Fig, 'UserData', Ident)
hold on;

nbCourbes = length(this);
ColorOrder = get(gca, 'ColorOrder');
nbCoul = size(ColorOrder,1);

hw = create_waitbar('Plot Coverage .rdf navigation', 'N', nbCourbes);
for k=1:nbCourbes
    my_waitbar(k, nbCourbes, hw);
    iCoul = mod(k-1, nbCoul) + 1;
    Carto = plot_coverage_unitaire(this(k), Carto, Fig, ColorOrder(iCoul,:), nomFic{k});
end
my_close(hw, 'MsgEnd');

% figure(Fig);
% set(Fig, 'name', FigName)
% drawnow


function Carto = plot_coverage_unitaire(this, Carto, Fig, Color, nomFic)

%% Lecture des datagrams de navigation

[flag, tNav, Latitude, Longitude, C] = read_navigation(this, Carto);
if ~flag || isempty(Latitude)
    return
end
Carto = C;

Longitude = Longitude(:,1);
Latitude  = Latitude(:,1);

NbPings = length(Latitude);
step = min(max(1, floor(NbPings/100)), 100);

NbPingsDessines = NbPings;
% TDepth = zeros(NbPingsDessines, 1);
D_Bab = zeros(NbPingsDessines, 1);
D_Tri = zeros(NbPingsDessines, 1);

[flag, DataHeading] = SSc_ReadDatagrams(nomFic, 'Ssc_Heading');
if ~flag
    return
end

TP = datetime(DataHeading.timestamp(:), 'ConvertFrom', 'posixtime');
heading(:,1) = my_interp1_headingDeg(TP, DataHeading.Heading(:), tNav, 'linear', 'extrap');
heading(:,2) = my_interp1_headingDeg(TP, DataHeading.Heading(:), tNav, 'linear', 'extrap');
% Heading(Heading == 0) = NaN; % pas top mais tant pis

[flag, Data_DepthAcrossDist] = compute_coverage(this);
if ~flag
    return
end

k2 = 1;
k1 = 1;
D = Data_DepthAcrossDist(k1,:);
D_Bab(k2) = D(1);
D_Tri(k2) = D(2);
% TDepth(k2) = tNav(k1);
k2 = k2+1;

for k1=1:NbPings
    D = Data_DepthAcrossDist(k1,:);
    iDeb = find(~isnan(D), 1, 'first');
    iFin = find(~isnan(D), 1, 'last');
    if isempty(iDeb) || isempty(iFin) || isnat(tNav(k1))
        continue
    end
    D_Bab(k2) = D(iDeb);
    D_Tri(k2) = D(iFin);
    %     TDepth(k2) = tNav(k1);
    k2 = k2+1;
end

k1 = NbPings;
D = Data_DepthAcrossDist(k1,:);
if ~isempty(iDeb) && ~isempty(iFin)
    D_Bab(k2) = D(1);
    D_Tri(k2) = D(2);
    %     TDepth(k2) = tNav(k1);
end

% TDepth(k2+1:end) = [];
D_Bab(k2+1:end)  = [];
D_Tri(k2+1:end)  = [];

% Longitude = my_interp1_longitude(tNav, Longitude, tDepth);
% Latitude  = my_interp1(tNav, Latitude,  tDepth);
Heading = unwrap(heading(:) * (pi/180)) * (180/pi);
% Heading   = my_interp1_headingDeg(tNav, Heading, tDepth);

[Lat, Lon] = get_faucheeInLatLong(Latitude, Longitude, Heading, D_Bab, D_Tri, zeros(size(D_Bab)), zeros(size(D_Bab)));

subNan = isnan(sum(Lon, 2)) | isnan(sum(Lat,2));
Lon(subNan, :) = [];
Lat(subNan, :) = [];

if isempty(Lon)
    return
end

X1 = Lon(2:end-1,1);
Y1 = Lat(2:end-1,1);
X2 = flipud(Lon(2:end-1,2));
Y2 = flipud(Lat(2:end-1,2));

X1 = reduceByStep(X1, step);
X2 = reduceByStep(X2, step);
Y1 = reduceByStep(Y1, step);
Y2 = reduceByStep(Y2, step);

X = [Lon(1,1); X1; Lon(end,1); Lon(end,2); X2; Lon(1,2); Lon(1,1)];
Y = [Lat(1,1); Y1; Lat(end,1); Lat(end,2); Y2; Lat(1,2); Lat(1,1)];

figure(Fig)
h = patch(X, Y, Color, 'FaceAlpha', 0.5, 'Tag', nomFic);
set_HitTest_Off(h)

%% Affichage de la navigation

axisGeo;
drawnow


function y = reduceByStep(x, step)
ky = 0;
y = NaN(floor(length(x) / step), 1);
for k=1:step:length(x)
    ky = ky + 1;
    sub = k:min(k+step-1, length(x));
    %     y(ky) = median(x(sub));
    y(ky) = mean(x(sub));
end
