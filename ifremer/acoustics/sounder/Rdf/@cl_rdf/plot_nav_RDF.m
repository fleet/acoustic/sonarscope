function [Fig, Carto] = plot_nav_RDF(~, nomFic, varargin)

[varargin, Carto] = getPropertyValue(varargin, 'Carto', []);
[varargin, Fig]   = getPropertyValue(varargin, 'Fig',   []); %#ok<ASGLU>

%% Cr�ation de la fen�tre avec les menu et callbacks sp�cialis�s pour la nav

Fig = plot_navigation_Figure(Fig);

%% Traitement des fichiers

if ~iscell(nomFic)
    nomFic = {nomFic};
end
NbFic = length(nomFic);
hw = create_waitbar('Plot .rdf navigation', 'N', NbFic);
for k=1:NbFic
    my_waitbar(k, NbFic, hw);
    
    a = cl_rdf('nomFic', nomFic{k});
    if isempty(a)
        fprintf('fichier vide : %s\n', nomFic{k});
        continue
    end
    
    [flag, TP, Latitude, Longitude, Carto] = read_navigation(a, Carto);
    if ~flag || isempty(Latitude)
        fprintf('navigation vide : %s\n', nomFic{k});
        continue
    end
    
    Longitude = Longitude(:,1);
    Latitude  = Latitude(:,1);
    SonarTime = TP;
    Heading   = [];
    
    plot_navigation(nomFic{k}, Fig, Longitude, Latitude, SonarTime, Heading);
end
my_close(hw, 'MsgEnd');
