function [Fig1, TypeDatagram, a, selection, SelectionMode] = plot_nav_RDF0OD(this, nomFic, varargin) %#ok<INUSL>

[varargin, selection]     = getPropertyValue(varargin, 'selection',     []);
[varargin, SelectionMode] = getPropertyValue(varargin, 'SelectionMode', 'Multiple'); %#ok<ASGLU>

if ~iscell(nomFic)
    nomFic = {nomFic};
end

Carto = [];
for k=1:length(nomFic)
    a = cl_rdf('nomFic', nomFic{k});
    [flag, b, Carto] = read_Image(a, 'Carto', Carto);
    c(k) = b(1); %#ok<AGROW>
end

Fig1 = plot_nav(c);
