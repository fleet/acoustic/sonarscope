% Trace de la navigation et des capteurs de vitesse et de cap d'un fichier .all
%
% Syntax
%   plot_position_data(nomFic, Invite)
%
% Input Arguments
%   nomFic : Liste de fichiers .all
%   Invite : Height | Heading | Speed | CourseVessel | SoundSpeed
%
% Examples
%   nomDir = 'D:\NIWA_DATA\TAN0105'
%   liste = listeFicOnDir2(nomDir, '.all')
%   plot_position_data(liste, 'Heading')
%   plot_position_data(liste, 'Speed')
%   plot_position_data(liste, 'CourseVessel')
%   plot_position_data(liste, 'SoundSpeed')
%   plot_position_data(liste, 'Height')
%   plot_position_data(liste, 'Mode')
%   plot_position_data(liste, 'Time')
%   plot_position_data(liste, 'Drift')
%
% See also cl_simrad_all plot_index Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function plot_position_data(this, Fig1, nomFic, Invite, varargin) %#ok<INUSL>

[varargin, InstallationParameters] = getPropertyValue(varargin, 'InstallationParameters', []);
[varargin, Carto]                  = getPropertyValue(varargin, 'Carto',                  []); %#ok<ASGLU>

if ~iscell(nomFic)
    nomFic = {nomFic};
end

NbFic    = length(nomFic);
nbColors = 64;
Colors   = jet(nbColors);
Ident    = rand(1);

figure(Fig1);
set(Fig1, 'UserData', Ident)
hold on;

%% Boucle sur les fichiers

XLim = [Inf -Inf];
YLim = [Inf -Inf];
ZLim = [Inf -Inf];
hw = create_waitbar('RDF files : reading signal', 'N', NbFic);
for k=1:NbFic
    my_waitbar(k, NbFic, hw);
    
    this = cl_rdf('nomFic', nomFic{k});
    subx = floor(linspace(1, get(this, 'nbColumns'), 20));
    [flag, a(k), Carto, InstallationParameters] = read_Image(this, 'SelecImage', 3, 'subx', subx, ...
        'Carto', Carto, 'InstallationParameters', InstallationParameters); %#ok<AGROW>
    if ~flag
        continue
    end
    
    Longitude = get(a(k), 'FishLongitude');
    Latitude  = get(a(k), 'FishLatitude');
        
    Mots = strsplit(Invite);
    if length(Mots) == 1
        [flag, Signal, Unit, C] = compute_signal(a(k), nomFic{k}, Mots{1});
    else
        [flag, Signal, Unit, C] = compute_signal(a(k), nomFic{k}, Mots{1}, 'Type', Mots{2});
    end
    if ~flag
        continue
    end
    Colors = C;
    
    switch Invite
        case {'Heading'; 'Heading Mean'}
            ZLim = [0 360];
        otherwise
            ZLim(1) = min(ZLim(1), double(min(Signal(:), [], 'omitnan')));
            ZLim(2) = max(ZLim(2), double(max(Signal(:), [], 'omitnan')));
    end
    XLim(1) = min(XLim(1), min(Longitude(:), [], 'omitnan'));
    XLim(2) = max(XLim(2), max(Longitude(:), [], 'omitnan'));
    YLim(1) = min(YLim(1), min(Latitude(:),  [], 'omitnan'));
    YLim(2) = max(YLim(2), max(Latitude(:),  [], 'omitnan'));
end
my_close(hw)

figure(Fig1);
if isempty(Unit)
    title(Invite)
else
    title([Invite ' (' Unit ')']);
end

if ~isfinite(ZLim(1))
    str1 = sprintf('Le signal "%s" n''est pas pr�sent dans les fichiers.', Invite);
    str2 = sprintf('Signal "%s" is not available in these files.', Invite);
    my_warndlg(Lang(str1,str2), 1);
    return
end

%% Saisie des bornes de la table de couleurs

[flag, ZLim] = saisie_CLim(min(ZLim), max(ZLim), Unit, 'SignalName', Invite, 'hFigHisto', 87698);
if ~flag
    return
end

%%

figure(Fig1);
W = axis;
x = linspace(XLim(1), XLim(1)+100*nbColors*eps(XLim(1)), nbColors);
y = linspace(YLim(1), YLim(2), nbColors);
h = imagesc(x, y, linspace(ZLim(1), ZLim(2), nbColors)); colormap(Colors), colorbar %#ok<NASGU>
% delete(h)
hold on;
axis(W)

hw = create_waitbar('RDF files : plotting signal', 'N', NbFic);
for k=1:NbFic
    my_waitbar(k, NbFic, hw);
    plot_position_data_unitaire(a(k), Invite, ZLim, Fig1, nbColors, Colors);
end
my_close(hw, 'MsgEnd')

% if isempty(Z.Unit)
%     FigName = sprintf('Sondeur %d Serial Number  %d - %s', EmModel, SystemSerialNumber, Invite);
% else
%     FigName = sprintf('Sondeur %d Serial Number  %d - %s (%s)', EmModel, SystemSerialNumber, Invite, Z.Unit);
% end
figure(Fig1);


function plot_position_data_unitaire(a, Invite, ZLim, Fig1, nbColors, Colors)

nomFic = a.InitialFileName;
figure(Fig1)

%% Lecture de la navigation et du signal

Longitude = get(a, 'FishLongitude');
Latitude  = get(a, 'FishLatitude');

Mots = strsplit(Invite);
if length(Mots) == 1
    % TODO : on pourrait passer "Longitude" et "Latitude" en PN/PV pour
    % �viter une relecture : � voir
    [flag, Signal, ~, ~, InterpMethod] = compute_signal(a, nomFic, Mots{1});
else
    [flag, Signal, ~, ~, InterpMethod] = compute_signal(a, nomFic, Mots{1}, 'Type', Mots{2});
end
if ~flag
    return
end

%% Trac�

% nbSounders = size(Signal,2);
% if nbSounders == 2
%     str1 = 'Ce soundeur est dual, les donn�es peuvent �tre diff�rentes � b�bord et � tribord, pour l''instant je ne trace que les donn�es du sondeur tribord.';
%     str2 = 'This sounder is a dual one, for the moment I plot only data from the port one.';
%     my_warndlg(Lang(str1,str2), 0, 'Tag', 'PlotPositionDataNbSounders=2');
nbSounders = 1;
% end
for iCote=1:nbSounders
    figure(Fig1(iCote))
    ZData = Signal(:,iCote);
    subNonNaN = find(~isnan(ZData));
    if length(subNonNaN) <= 1
        return
    end
    if ~isempty(InterpMethod)
        subNaN = find(isnan(ZData));
        if ~isempty(subNaN)
            ZData(subNaN) = interp1(subNonNaN, ZData(subNonNaN), subNaN, InterpMethod, 'extrap');
        end
    end
    %     Value = interp1(Time, ZData, Time);
    Value = ZData;
    if ZLim(2) == ZLim(1)
        ZLim(1) = ZLim(1) - 0.5;
        ZLim(2) = ZLim(2) + 0.5;
    end
    coef = (nbColors-1) / (ZLim(2) - ZLim(1));
    Value = ceil((Value - ZLim(1)) * coef);
    
    %% Affichage de la navigation
    
    for k=1:nbColors
        sub = find(Value == (k-1));
        if ~isempty(sub)
            h = plot(Longitude(sub), Latitude(sub), '+');
            set(h, 'Color', Colors(k,:), 'Tag', nomFic)
            set_HitTest_Off(h)
        end
    end
    
    sub = find(Value < 0);
    if ~isempty(sub)
        h = plot(Longitude(sub), Latitude(sub), '+');
        set(h, 'Color', Colors(1,:)*0.8, 'Tag', nomFic)
        set_HitTest_Off(h)
    end
    
    sub = find(Value > (nbColors-1));
    if ~isempty(sub)
        h = plot(Longitude(sub), Latitude(sub), '+');
        set(h, 'Color', Colors(nbColors,:)*0.8, 'Tag', nomFic)
    end
end

axisGeo;
grid on;
% colorbar % Supprim� le 7/11/2006 car ca remet les infos de colorbar de 0
% � 64
drawnow
