% TODO : il serait judicieux de faire un mask par bits plut�t qu'un masque
% par valeurs. Le premier bit concernerait le masquage qui a �t� fait dans
% l'�tape1, le deuxi�me dans l'�tape 2, ... Les 5 derniers bits pourraient
% �tre utilis�s pour mettre des masques utilisateur (Masquage manuel,
% masquage hauto, etc ...)


function  [flag, repImport] = preprocess_RDF(~, nomFic, Step, varargin)

[varargin, repImport] = getPropertyValue(varargin, 'repImport', []); %#ok<ASGLU>

if ~iscell(nomFic)
    nomFic = {nomFic};
end

%% Si Step 2 on demande le seuil de rejection de l'algorithme anti-spike (N*Sigma)

if strcmp(Step, 'Step2')
    str1 = 'Param�trage de l''algorithme de suppression des valeurs erratiques.';
    str2 = 'Set parameter for outliers rejection.';
    p = ClParametre('Name', Lang('Seuil','Threshold'), ...
        'Unit', 'std',  'MinValue', 1.5, 'MaxValue', 5, 'Value', 2.5);
    a = StyledParametreDialog('params', p, 'Title', Lang(str1,str2));
    % a.Help = 'http://flotte.ifremer.fr/fleet/Presentation-of-the-fleet/On-board-software/SonarScope';
    a.sizes = [0 -3 -1 -2 -1 -1 -1 0];
    a.openDialog;
    flag = a.okPressedOut;
    if ~flag
        return
    end
    CoefSigma = a.getParamsValue;
end

%% Traitement sur l'ensemble des fichiers

Carto = [];
nomDirRDF = fileparts(nomFic{1});
[flag, InstallationParameters, repImport] = RDF_installationParameters('repImport', nomDirRDF);
if ~flag
    return
end
N = length(nomFic);
str1 = ['Pr�traitement "' Step '" des donn�es du Geoswath'];
str2 = ['Geoswath preprocessing "' Step '"'];
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
    my_waitbar(k, N, hw);
    fprintf('%s - %d/%d : %s\n', Step, k, N, nomFic{k});
    
    this = cl_rdf('nomFic', nomFic{k});

    switch Step
        case 'Step1'
            [flag, Carto, InstallationParameters] = RDF_Preprocessing_Step1(this, Carto, InstallationParameters, k==1);
            
        case 'Step2'
            [flag, Carto, InstallationParameters] = RDF_Preprocessing_Step2(this, Carto, InstallationParameters, CoefSigma);
            
        case 'Step3'
            [flag, Carto, InstallationParameters] = RDF_Preprocessing_Step3(this, Carto, InstallationParameters);
            
        case 'Step4'
            [flag, Carto, InstallationParameters] = RDF_Preprocessing_Step4(this, Carto, InstallationParameters, k==1);
    end
    if ~flag
        my_close(hw, 'MsgEnd')
        return
    end
    
    %% On demande si on veut traiter les autres fichiers
    
    if (k ~= N) && ~(strcmp(Step, 'Step2') || strcmp(Step, 'Step3'))
        flag = question_ContinueNextFile();
        if ~flag
            break
        end
    end
end
my_close(hw, 'MsgEnd')
