% Transformation en chaine de caracteres d'une instance
%
% Syntax
%   str = char_instance(a)
%
% Input Arguments
%   a : instance de cl_rdf
%
% Name-Value Pair Arguments
%   InfoSonar  : 1=Information de l'image uniquement
%                2=Info de l'information sonar uniquement
%                3=Display de toute l'information disponible (image+sonar) : (3 par defaut)
%
% Output Arguments
%   str : une chaine de caracteres representative de l instance
%
% Examples
%
% See also cl_rdf Authors
% Authors : GLU
% ----------------------------------------------------------------------------

function str = char_instance(this, varargin)

str = [];

str{end+1} = sprintf('nomFic      <-> %s', this.nomFic);

[flag, DataSample] = SSc_ReadDatagrams(this.nomFic, 'Ssc_Sample', 'XMLOnly', 1);
if flag
    str{end+1} = sprintf('nbPings     <-- %d', DataSample.Dimensions.nbPings);
    str{end+1} = sprintf('nbColumns   <-- %d', DataSample.Dimensions.nbCol);
else
    str{end+1} = sprintf('nbPings     <-- 0');
    str{end+1} = sprintf('nbColumns   <-- 0');
end

%% Concatenation en une chaine

str = cell2str(str);
