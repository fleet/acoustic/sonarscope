function readOnce_RDF(this, nomFic, varargin) %#ok<INUSL>

global useCacheNetcdf %#ok<GVMIS>

useParallel = get_UseParallelTbx;

if ~iscell(nomFic)
    nomFic = {nomFic};
end

N = length(nomFic);
str1 = 'Lecture des fichiers RDF';
str2 = 'Read once the RDF files';
if useParallel && (N > 1)
    isUsingParfor = 1;
	[hw, DQ] = create_parforWaitbar(N, Lang(str1,str2));
    useCacheNetcdf_Here = useCacheNetcdf;
    parfor (k=1:N, useParallel)
        fprintf('\n%d / %d : %s', k, N,  nomFic{k});
        a = cl_rdf('nomFic', nomFic{k}, 'isUsingParfor', isUsingParfor, 1, 'useCacheNetcdf', useCacheNetcdf_Here);
        display(a)
        send(DQ, k);
    end
    my_close(hw, 'MsgEnd')
else
    hw = create_waitbar(Lang(str1,str2), 'N', N);
    for k=1:N
        my_waitbar(k, N, hw)
        fprintf('\n%d / %d : %s', k, N,  nomFic{k});
        a = cl_rdf('nomFic', nomFic{k});
        display(a)
    end
    my_close(hw, 'MsgEnd')
end
