% Lecture des donn�es d'un fichier .RDF
%
% Syntax
%   [Data, b, Carto] = read_Image(a, ...)
%
% Input Arguments
%   a : Instance de cl_rdf
%
% Name-Value Pair Arguments
%   Carto : Instance de cl_carto
%
% Output Arguments
%   Data  : Structure contenant :
%   b     : Instances de cl_image contenant la reflectivite et le numero de faisceau
%   Carto : Instance de cl_carto
%
% Examples
%   nomFic = getNomFicDatabase('EW150.RDF');
%   a = cl_rdf('nomFic', nomFic);
%   [Data, b] = read_Image(a);
%   SonarScope(b)
%
%   figure; plot(Data.Time, Data.PingCounter);     grid on;  title('PingCounter')
%
% See also cl_rdf cl_rdf/plot_nav Authors
% Authors : JMA
% -------------------------------------------------------------------------

function [flag, b, Carto, InstallationParameters] = read_Image(this, varargin)

[varargin, Carto]                  = getPropertyValue(varargin, 'Carto',                  []);
[varargin, InstallationParameters] = getPropertyValue(varargin, 'InstallationParameters', []);
[varargin, TideFile]               = getPropertyValue(varargin, 'TideFile',               []);

if isempty(TideFile)
    Tide = [];
else
    [flag, Tide] = read_Tide(TideFile, 'CleanData', 1);
    if ~flag
        return
    end

end

if isempty(InstallationParameters)
    repImport = fileparts(this.nomFic);
    [flag, InstallationParameters] = RDF_installationParameters('repImport', repImport);
    if ~flag
        b = [];
        return
    end
end

nbImages = length(this);
for k=1:nbImages
    [flag, b, Carto, InstallationParameters] = unitaire_read_Image(this(k), Carto, InstallationParameters, Tide, varargin{:});
    if ~flag
        b = [];
        Carto = [];
        return
    end
end


function [flag, b, Carto, InstallationParameters] = unitaire_read_Image(this, Carto, InstallationParameters, Tide, varargin)

[varargin, UseMask]    = getPropertyValue(varargin, 'UseMask',    1);
[varargin, SelecImage] = getPropertyValue(varargin, 'SelecImage', [2 4]);
[varargin, subx]       = getPropertyValue(varargin, 'subx',       []); %#ok<ASGLU>

b    = cl_image;
b(:) = [];

[nomDir, nomFicSeul] = fileparts(this.nomFic);

%% Lecture du fichier d'index

[flag, DataFileIndex] = SSc_ReadDatagrams(this.nomFic, 'Ssc_FileIndex');
if ~flag
    return
end

%% Lecture de l'entete du fichier

[flag, DataHeader] = SSc_ReadDatagrams(this.nomFic, 'Ssc_Header');
if ~flag
    return
end

%% Lecture de l'entete de fichier

[flag, RDFFileHeader] = SSc_ReadDatagrams(this.nomFic, 'Ssc_FileHeader', 'XMLOnly', 1);
if ~flag
    return
end

%% Attitude

[flag, DataAttitude] = SSc_ReadDatagrams(this.nomFic, 'Ssc_Attitude');
if ~flag
    %     return % Donn�es UNH Val
end

%% Heading

[flag, DataHeading] = SSc_ReadDatagrams(this.nomFic, 'Ssc_Heading');
if ~flag
    %     return % Donn�es UNH Val
end

%% Images

[flag, DataSample] = SSc_ReadDatagrams(this.nomFic, 'Ssc_Sample', 'Memmapfile', -3);
if ~flag
    return
end

%% MiniSVS

[~, DataMiniSVS] = SSc_ReadDatagrams(this.nomFic, 'Ssc_MiniSVS');

%% Ssc_EchoSounderString

% [flag, DataEchoSounderString] = SSc_ReadDatagrams(nomFic, 'Ssc_EchoSounderString'); % Non test�

%% Ssc_EchoSounder

% [flag, Data2] = SSc_ReadDatagrams(nomFic, 'Ssc_EchoSounder'); % Non test�

%% Cr�ation des images

GeometryType = cl_image.indGeometryType('PingSamples');
InitialFileFormat = 'GeoSwathRDF';
Frequency = RDFFileHeader.Frequency / 1000; % en kHz
switch Frequency
    case 125
        Mode_1 = 1;
    case 250
        Mode_1 = 2;
    case 500
        Mode_1 = 3;
end
SonarDescription = cl_sounder('Sonar.Ident', 16, 'Sonar.Mode_1', Mode_1, 'Time', DataHeader.DateHeure(1));
resol = get(SonarDescription, 'Proc.RangeResol');

sublBab = find(DataHeader.Side(:) == 0);
sublTri = find(DataHeader.Side(:) == 1);
if isempty(sublTri) % Cas du fichier Shallow Survey 2015 H:\Common_Data_Set\Geoacoustics\RDF\Day4 - Target 2 Repeat (LNN003rep).rdf
    flag = 0;
    return
end

T2 = DataHeader.DateHeure(:);
TBab = cl_time('timeMat', T2(sublBab)); % TODO
TTri = cl_time('timeMat', T2(sublTri)); % TODO
DatetimeBab = datetime(T2(sublBab), 'ConvertFrom', 'datenum');
DatetimeTri = datetime(T2(sublTri), 'ConvertFrom', 'datenum');

SamplingRate = get(SonarDescription, 'Proc.SampFreq');
SamplingRate = ones(DataSample.Dimensions.nbPings,2) * SamplingRate;

[flag, ~, Latitude, Longitude, Carto] = read_navigation(this, Carto);
if ~flag
    % TODO : message ?
    return
end

Milieu = floor(DataSample.Dimensions.nbCol/2);
x = (1:DataSample.Dimensions.nbCol) - (Milieu+1);
y = 1:DataSample.Dimensions.nbPings;

if isempty(subx)
    subx = 1:DataSample.Dimensions.nbCol;
end
x = x(subx);
NPings = max(length(DatetimeBab), length(TTri));

%% Cr�ation des objets image

M = DataSample.Mask(:,subx);
M = (M ~= 0);

k = 0;
if any(SelecImage == 1) % Mask
    k = k + 1;
    ImageName{k} = nomFicSeul;
    b(k) = cl_image('Image', M, 'Name', ImageName{k}, 'Unit', '', ...
        'DataType', cl_image.indDataType('Mask'), 'ColormapIndex', 3, ...
        'TagSynchroX', [nomFicSeul ' - PingSamples'], 'TagSynchroY', nomFicSeul,...
        'x', x, 'XUnit', 'samples', 'y', y, 'YUnit', 'Pings', ...
        'InitialFileName', this.nomFic, ...
        'InitialFileFormat', InitialFileFormat, ...
        'GeometryType', GeometryType,...
        'SonarDescription', SonarDescription);
end


if any(SelecImage == 2) % BeamPointingAngle Raw
    k = k + 1;
    X = DataSample.TetaRaw(:,subx);
    subBab = find(x < 0);
    subTri = find(x > 0);
    X(:,subBab) = X(:,subBab) - InstallationParameters.Port.RollCorrection;
    X(:,subTri) = X(:,subTri) - InstallationParameters.Starboard.RollCorrection;
    %     if UseMask
    %         X(M) = NaN;
    %     end
    ImageName{k} = [nomFicSeul '_Raw'];
    b(k) = cl_image('Image', X, 'Name', ImageName{k}, 'Unit', 'deg', ...
        'DataType', cl_image.indDataType('BeamPointingAngle'), 'ColormapIndex', 3, ...
        'TagSynchroX', [nomFicSeul ' - PingSamples'], 'TagSynchroY', nomFicSeul,...
        'x', x, 'XUnit', 'samples', 'y', y, 'YUnit', 'Pings', ...
        'InitialFileName', this.nomFic, ...
        'InitialFileFormat', InitialFileFormat, ...
        'GeometryType', GeometryType,...
        'SonarDescription', SonarDescription);
end

if any(SelecImage == 3) % BeamPointingAngle Proc
    k = k + 1;
    X = DataSample.TetaProc(:,subx);
    if UseMask
        X(~M) = NaN;
    end
    ImageName{k} = [nomFicSeul '_Proc'];
    b(k) = cl_image('Image', X, 'Name', ImageName{k}, 'Unit', 'deg', ...
        'DataType', cl_image.indDataType('BeamPointingAngle'), 'ColormapIndex', 3, ...
        'TagSynchroX', [nomFicSeul ' - PingSamples'], 'TagSynchroY', nomFicSeul,...
        'x', x, 'XUnit', 'samples', 'y', y, 'YUnit', 'Pings', ...
        'InitialFileName', this.nomFic, ...
        'InitialFileFormat', InitialFileFormat, ...
        'GeometryType', GeometryType,...
        'SonarDescription', SonarDescription);
end

if any(SelecImage == 4) % Reflectivity
    k = k + 1;
    X = DataSample.Reflectivity(:,subx);
    ImageName{k} = nomFicSeul;
    b(k) = cl_image('Image', X, 'Name', ImageName{k}, 'Unit', 'dB', ...
        'DataType', cl_image.indDataType('Reflectivity'), 'ColormapIndex', 2, ...
        'TagSynchroX', [nomFicSeul ' - PingSamples'], 'TagSynchroY', nomFicSeul,...
        'x', x, 'XUnit', 'samples', 'y', y, 'YUnit', 'Pings', ...
        'InitialFileName', this.nomFic, ...
        'InitialFileFormat', InitialFileFormat, ...
        'GeometryType', GeometryType,...
        'SonarDescription', SonarDescription);
    
    %% Recherche du profil Levitus
    
    %     SignalFreq = get(SonarDescription, 'Signal.Freq'); % Avec R2011b
    SignalFreq = get(SonarDescription, 'BeamForm.Tx.Freq'); % Avec R2013a
    
    Mois = DatetimeBab.Month;
    
    LatitudeMoyenne  = mean(Latitude(:), 'omitnan');
    LongitudeMoyenne = mean(Longitude(:), 'omitnan');
    z = cl_sound_speed('Incoming', 2, 'LevitusAveragingType', 3, ...
        'Latitude', LatitudeMoyenne, 'Longitude', LongitudeMoyenne, ...
        'Frequency', SignalFreq(1), 'LevitusMonth', Mois(1));
    Salinity = get(z, 'Salinity');
    Salinity = mean(Salinity, 'omitnan');
    
    %% Recherche de la temp�rature � partir de la c�l�rit� et de la salinit�
    
    %{
syms x C T S Z
C = 1449.8 + 4.3406*x -0.0387*(x.^2)+ (1.3106 - 0.0088*x).*(S-35) + 0.01682*Z
y = 1449.8 + 4.3406*x -0.0387*(x.^2)+ (1.3106 - 0.0088*x).*(S-35) + 0.01682*Z - C
solve(y, 'x')
pppp1 = (5000*((121*S^2)/1562500 + (1513319*S)/12500000 - (387*C)/2500 + (325467*Z)/125000000 + 5973442279/25000000)^(1/2))/387 - (44*S)/387 + 23243/387
pppp2 =  23243/387 - (5000*((121*S^2)/1562500 + (1513319*S)/12500000 - (387*C)/2500 + (325467*Z)/125000000 + 5973442279/25000000)^(1/2))/387 - (44*S)/387
S = 35;
Z = -5;
T = -10:0.1:56;
C = 1449.8 + 4.3406*T -0.0387* T.^2 + (1.3106 - 0.0088*T).*(S-35) + 0.01682*Z
figure; plot(T, C); grid on
C = [1490 1520]
T =  23243/387 - (5000*((121*S^2)/1562500 + (1513319*S)/12500000 - (387*C)/2500 + (325467*Z)/125000000 + 5973442279/25000000).^(1/2))/387 - (44*S)/387
C2 = celeriteChen(Z, T, S)
    %}
    
    S = Salinity;
    Z = -5;
    C = mean(DataMiniSVS.velocity(:), 'omitnan');
    Temperatue =  23243/387 - (5000*((121*S^2)/1562500 + (1513319*S)/12500000 - (387*C)/2500 + (325467*Z)/125000000 + 5973442279/25000000).^(1/2))/387 - (44*S)/387;
    
    %% Calcul du coefficient d'absorption
    
    Immersion = -0.5; % TODO : voir o� on peut r�cup�rer cette valeur
    SonarTVG_IfremerAlpha = AttenuationGarrison(SignalFreq, Immersion, Temperatue, Salinity);
    %     SonarTVG_IfremerAlpha = 0;
    
    BSStatus.SonarNE_etat                       = 2; % '1=Compensated' | '2=Not compensated'
    BSStatus.SonarGT_etat                       = 2; % '1=Compensated' | '2=Not compensated'
    BSStatus.SonarSH_etat                       = 2; % '1=Compensated' | '2=Not compensated'
    
    BSStatus.SonarTVG_etat                      = 2; % '1=Compensated' | '2=Not compensated'
    BSStatus.SonarTVG_origine                   = 1; % '1=Constructeur' | '2=Ifremer'
    BSStatus.SonarTVG_ConstructTypeCompens      = 1; % '1=Analytique' | '2=Table'
    
    BSStatus.SonarTVG_ConstructAlpha            = 0;
    BSStatus.SonarTVG_ConstructCoefDiverg       = 20;
    BSStatus.SonarTVG_ConstructConstante        = 0;
    BSStatus.SonarTVG_ConstructTable            = [];
    BSStatus.SonarTVG_IfremerAlpha              = SonarTVG_IfremerAlpha;
    
    BSStatus.SonarDiagEmi_etat                  = 2; % '1=Compensated' | '2=Not compensated'
    BSStatus.SonarDiagEmi_origine               = 1; % '1=Constructeur' | '2=Ifremer'
    BSStatus.SonarDiagEmi_ConstructTypeCompens  = 2; % '1=Analytique' | '2=Table'
    BSStatus.SonarDiagEmi_IfremerTypeCompens    = 2; % '1=Analytique' | '2=Table'
    
    BSStatus.SonarDiagRec_etat                  = 2; % '1=Compensated' | '2=Not compensated'
    BSStatus.SonarDiagRec_origine               = 1; % '1=Constructeur' | '2=Ifremer'
    BSStatus.SonarDiagRec_ConstructTypeCompens  = 1; % '1=Analytique' | '2=Table'
    BSStatus.SonarDiagRec_IfremerTypeCompens    = 1; % '1=Analytique' | '2=Table'
    
    BSStatus.SonarAireInso_etat                 = 2; % '1=Compensated' | '2=Not compensated'
    BSStatus.SonarAireInso_origine              = 2; % '1=Constructeur' | '2=Ifremer'
    
    BSStatus.SonarBS_etat                       = 2; % '1=Compensated' | '2=Not compensated'
    BSStatus.SonarBS_origine                    = 1; % {'1=Belle Image'} | '2=Full compensation'
    BSStatus.SonarBS_origineBelleImage          = 1; % {'1=Lambert'} | '2=Lurton'
    BSStatus.SonarBS_origineFullCompens         = 1; % {'1=Analytique'} | '2=Table'
    BSStatus.SonarBS_LambertCorrection          = 2; % '1=Compensated' | '2=Not compensated'
    BSStatus.SonarBS_IfremerCompensTable        = [];
    
    b(k) = set_BSStatus(b(k), BSStatus);
end

if any(SelecImage == 5) % Depth
    k = k + 1;
    X = DataSample.Depth(:,subx);
%     if UseMask
%         X(~M) = NaN;
%     end
    
    % Tide correction
    if ~isempty(Tide)
        TimePings = NaT(NPings,1); % TODO Traiter tribord aussi
        TimePings(1:length(DatetimeBab)) = DatetimeBab; % TODO Traiter tribord aussi
        TidePings = interp1(Tide.Datetime, Tide.Value, TimePings);
        X = bsxfun(@plus, X, TidePings);
    end
    
    ImageName{k} = nomFicSeul;
    b(k) = cl_image('Image', X, 'Name', ImageName{k}, 'Unit', 'm', ...
        'DataType', cl_image.indDataType('Bathymetry'), 'ColormapIndex', 3, ...
        'TagSynchroX', [nomFicSeul ' - PingSamples'], 'TagSynchroY', nomFicSeul,...
        'x', x, 'XUnit', 'samples', 'y', y, 'YUnit', 'Pings', ...
        'InitialFileName', this.nomFic, ...
        'InitialFileFormat', InitialFileFormat, ...
        'GeometryType', GeometryType,...
        'SonarDescription', SonarDescription);
end

if any(SelecImage == 6) % AcrossDist
    k = k + 1;
    X = DataSample.AcrossDist(:,subx);
%     if UseMask
%         X(~M) = NaN;
%     end
    ImageName{k} = nomFicSeul;
    b(k) = cl_image('Image', X, 'Name', ImageName{k}, 'Unit', 'm', ...
        'DataType', cl_image.indDataType('AcrossDist'), 'ColormapIndex', 3, ...
        'TagSynchroX', [nomFicSeul ' - PingSamples'], 'TagSynchroY', nomFicSeul,...
        'x', x, 'XUnit', 'samples', 'y', y, 'YUnit', 'Pings', ...
        'InitialFileName', this.nomFic, ...
        'InitialFileFormat', InitialFileFormat, ...
        'GeometryType', GeometryType,...
        'SonarDescription', SonarDescription);
end

if any(SelecImage == 7) % AlongDist
    k = k + 1;
    X = DataSample.AlongDist(:,subx);
%     if UseMask
%         X(~M) = NaN;
%     end
    ImageName{k} = nomFicSeul;
    b(k) = cl_image('Image', X, 'Name', ImageName{k}, 'Unit', 'm', ...
        'DataType', cl_image.indDataType('AlongDist'), 'ColormapIndex', 3, ...
        'TagSynchroX', [nomFicSeul ' - PingSamples'], 'TagSynchroY', nomFicSeul,...
        'x', x, 'XUnit', 'samples', 'y', y, 'YUnit', 'Pings', ...
        'InitialFileName', this.nomFic, ...
        'InitialFileFormat', InitialFileFormat, ...
        'GeometryType', GeometryType,...
        'SonarDescription', SonarDescription);
end

clear X M

PC = DataFileIndex.PingCounter(:);
PC = unwrapPingCounter(PC);
PingCounter(:,1) = PC(sublBab);
PingCounter(1:length(DatetimeTri),2) = PC(sublTri);
PingCounter(PingCounter==0) = NaN;

%% Interpolation de la donnee "Attitude"

if isempty(DataAttitude)
    Roll  = NaN(NPings,2);
    Pitch = NaN(NPings,2);
    Heave = NaN(NPings,2);
else
    DatetimeMRU = datetime(DataAttitude.timestamp(:), 'ConvertFrom', 'posixtime');
    DatetimeMRU = DatetimeMRU - duration(0, 0, 0, InstallationParameters.MRUDelay); % Modif JMA le 05/09/2019 : + avant,  - maintenant (pour faire comme ce qui est fait pour Roll, ...)
    Roll (1:length(DatetimeBab),1) = my_interp1(DatetimeMRU, DataAttitude.roll(:),  DatetimeBab, 'linear', 'extrap');
    Pitch(1:length(DatetimeBab),1) = my_interp1(DatetimeMRU, DataAttitude.pitch(:), DatetimeBab, 'linear', 'extrap'); % + InstallationParameters.Port.PitchCorrection;
    Heave(1:length(DatetimeBab),1) = my_interp1(DatetimeMRU, DataAttitude.heave(:), DatetimeBab, 'linear', 'extrap');
    Roll (1:length(DatetimeTri),2) = my_interp1(DatetimeMRU, DataAttitude.roll(:),  DatetimeTri, 'linear', 'extrap');
    Pitch(1:length(DatetimeTri),2) = my_interp1(DatetimeMRU, DataAttitude.pitch(:), DatetimeTri, 'linear', 'extrap'); % + InstallationParameters.Starboard.PitchCorrection;
    Heave(1:length(DatetimeTri),2) = my_interp1(DatetimeMRU, DataAttitude.heave(:), DatetimeTri, 'linear', 'extrap');
end

SonarHeight = DataSample.Height(:,:);
if all(isnan(SonarHeight))
    % On peut essayer de la r�cup�rer de EchoSounder
end

if all(isnan(DataSample.Heading(:)))
    if isempty(DataHeading)
        Heading =  NaN(NPings,2);
    else
        DatetimeHeading = datetime(DataHeading.timestamp(:), 'ConvertFrom', 'posixtime');
        DatetimeHeading = DatetimeHeading - duration(0, 0, 0, InstallationParameters.MRUDelay);
        Heading(1:length(DatetimeBab),1) = my_interp1_headingDeg(DatetimeHeading, DataHeading.Heading(:), DatetimeBab, 'linear', 'extrap');
        Heading(1:length(DatetimeTri),2) = my_interp1_headingDeg(DatetimeHeading, DataHeading.Heading(:), DatetimeTri, 'linear', 'extrap');
        Heading(Heading == 0) = NaN; % pas top mais tant pis
    end
else
    Heading = DataSample.Heading;
end

TP = datetime(DataMiniSVS.timestamp(:), 'ConvertFrom', 'posixtime');
SurfaceSoundSpeed(1:length(DatetimeBab),1) = my_interp1(TP, DataMiniSVS.velocity(:), DatetimeBab, 'linear', 'extrap');
SurfaceSoundSpeed(1:length(DatetimeTri),2) = my_interp1(TP, DataMiniSVS.velocity(:), DatetimeTri, 'linear', 'extrap');
SurfaceSoundSpeed(SurfaceSoundSpeed == 0) = NaN;

for k=1:length(b)
    b(k) = update_Name(b(k));
    b(k) = set(b(k), 'SonarRawDataResol', resol, 'SonarResolutionD', resol, 'SonarResolutionX', resol);
    b(k) = set_SonarTime(b(k),              [TBab, TTri]); % [DatetimeBab, DatetimeTri] % TODO
    b(k) = set_SonarRoll(b(k),              Roll);
    b(k) = set_SonarPitch(b(k),             Pitch);
    b(k) = set_SonarHeave(b(k),             Heave);
    b(k) = set_SonarPingCounter(b(k),       PingCounter);
    b(k) = set_SonarFishLongitude(b(k),     Longitude);
    b(k) = set_SonarFishLatitude(b(k),      Latitude);
    b(k) = set_SonarHeading(b(k),           Heading);
    b(k) = set_SonarSampleFrequency(b(k),   SamplingRate);
    b(k) = set_SonarSurfaceSoundSpeed(b(k), SurfaceSoundSpeed);
    b(k) = set_SonarHeight(b(k),            -abs(SonarHeight)); % Ce n'est pas l'info de profondeur sous les antennes
    b(k) = set_SonarPortMode_1(b(k),        repmat(Mode_1, size(Roll, 1), 1));
    
    %% Definition d'une carto
    
    if isempty(Carto)
        [Carto, flag] = getDefinitionCarto(b(k), 'nomDirSave', nomDir, 'NoGeodetic', 0);
        if ~flag
            return
        end
    end
    b(k) = set(b(k), 'Carto', Carto);
    
    %% Definition du nom de l'image
    
    b(k) = set(b(k), 'Name', ImageName{k});
    b(k) = update_Name(b(k));
end
flag = 1;
