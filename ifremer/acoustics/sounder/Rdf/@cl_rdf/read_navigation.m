function [flag, T, Latitude, Longitude, Carto, HeureGPS, HeightGPS, LatGPS, LonGPS, HeightRTK, QualityRTK] = read_navigation(this, Carto, varargin)

HeureGPS   = [];
HeightGPS  = [];
LatGPS     = [];
LonGPS     = [];
Latitude   = [];
Longitude  = [];
T          = [];
HeightRTK  = [];
QualityRTK = [];

% [varargin, RedoNav] = getPropertyValue(varargin, 'RedoNav', 0);

if isempty(Carto)
    Carto = define_carto(this, Carto);
    %     Carto = cl_carto([]);
    %     Carto(:) = [];
    %     nomFicCartoDefault = fullfile(nomDir, 'Carto_Directory.xml');
    %     if exist(nomFicCartoDefault, 'file')
    %         Carto = import_xml(cl_carto([]), nomFicCartoDefault);
    %     end
end
[nomDir, nomFic] = fileparts(this.nomFic);

%% Test si fichiers Pospac pr�sents

nomFicPospas = fullfile(nomDir, 'SonarScope', nomFic, 'PospacSBET.xml');
if exist(nomFicPospas, 'file')
    messageForDebugInspection
end

%% Lecture de l'ent�te du fichier

[flag, DataHeader] = SSc_ReadDatagrams(this.nomFic, 'Ssc_Header');
if ~flag
    return
end

sublBab = (DataHeader.Side(:) == 0);
sublTri = (DataHeader.Side(:) == 1);
T2 = DataHeader.DateHeure(:);
DatetimeBab = datetime(T2(sublBab), 'ConvertFrom', 'datenum');
DatetimeTri = datetime(T2(sublTri), 'ConvertFrom', 'datenum');
NPings = max(length(DatetimeBab), length(DatetimeTri));

T = NaT(NPings,1);
sub = ~isnat(DatetimeTri);
T(sub) = DatetimeTri(sub);
sub = ~isnat(DatetimeBab);
T(sub) = DatetimeBab(sub);

%% Position

[flag, DataPosition] = SSc_ReadDatagrams(this.nomFic, 'Ssc_Navigation');
if ~flag
    % return % Donn�es UNH Val
end

%% NavString

[flag, DataNavString] = SSc_ReadDatagrams(this.nomFic, 'Ssc_NavigationString');
if ~flag
    %     return % Donn�es UNH Val
end
if ~isempty(DataNavString)
    [~, ~, LatGPS, LonGPS, HeightGPS] = read_Navigation_NMEA(DataNavString.NavigationString);
end

% ATTENTION : Pour l'instant, on reste comme avant car il faut revoir tout
% le processus de correction de la nav (pr�processing) pour aller remettre
% � jour les bonnes variables

% TP = DataPosition.time(:);
% TP = cl _ time('timeUnix', TP);
% pppp = TP.heure
% JourIfr = TP.date
HeureGPS = [];

% if 1
if isempty(HeureGPS)
    yGeo = NaN(NPings,2);
    xGeo = NaN(NPings,2);
    TP = datetime(DataPosition.time(:), 'ConvertFrom', 'posixtime');
    yGeo(1:length(DatetimeBab),1) = my_interp1(TP, DataPosition.y(:), DatetimeBab, 'linear', 'extrap'); % timestamp
    xGeo(1:length(DatetimeBab),1) = my_interp1(TP, DataPosition.x(:), DatetimeBab, 'linear', 'extrap');
    yGeo(1:length(DatetimeTri),2) = my_interp1(TP, DataPosition.y(:), DatetimeTri, 'linear', 'extrap'); % timestamp
    xGeo(1:length(DatetimeTri),2) = my_interp1(TP, DataPosition.x(:), DatetimeTri, 'linear', 'extrap');
    if isempty(Carto)
        LatMean = median(LatGPS, 'omitnan');
        LonMean = median(LonGPS, 'omitnan');
        [~, Carto] = define(Carto, nomDir, LatMean, LonMean, 'NoGeodetic', 1);
    end
    if isempty(Carto)
        flag = 0;
        return
    end
    
    [Latitude, Longitude] = xy2latlon(Carto, xGeo, yGeo);
    flag = 1; % Ajout GLT
    % figure; plot(xGeo(:,1), yGeo(:,1)); grid on
    % figure; plot(Longitude(:,1), Latitude(:,1)); grid on
    
    HeightRTK(1:length(DatetimeBab),1)  = my_interp1(TP, DataPosition.z(:), DatetimeBab, 'linear', 'extrap');
    HeightRTK(1:length(DatetimeTri),2)  = my_interp1(TP, DataPosition.z(:), DatetimeTri, 'linear', 'extrap');
    QualityRTK(1:length(DatetimeBab),1) = interp0(TP, single(DataPosition.quality(:)), DatetimeBab, 'previous', 'extrap');
    QualityRTK(1:length(DatetimeTri),2) = interp0(TP, single(DataPosition.quality(:)), DatetimeTri, 'previous', 'extrap');
else
    if all(HeureGPS == 0)
        TP = datetime(DataPosition.time(:), 'ConvertFrom', 'posixtime');
        yGeo = NaN(NPings,2);
        xGeo = NaN(NPings,2);
        yGeo(1:length(DatetimeBab),1) = my_interp1(TP, DataPosition.y(:), DatetimeBab, 'linear', 'extrap'); % timestamp
        xGeo(1:length(DatetimeBab),1) = my_interp1(TP, DataPosition.x(:), DatetimeBab, 'linear', 'extrap');
        yGeo(1:length(DatetimeTri),2) = my_interp1(TP, DataPosition.y(:), DatetimeTri, 'linear', 'extrap'); % timestamp
        xGeo(1:length(DatetimeTri),2) = my_interp1(TP, DataPosition.x(:), DatetimeTri, 'linear', 'extrap');
        if isempty(Carto)
            LatMean = 0;
            LonMean = 0;
            [~, Carto] = define(Carto, nomDir, LatMean, LonMean, 'NoGeodetic', 1);
        end
        if isempty(Carto)
            flag = 0;
            return
        end
        [Latitude, Longitude] = xy2latlon(Carto, xGeo, yGeo);
        flag = 1; % Ajout GLT
        % figure; plot(Longitude, Latitude); grid on
    else
%         % TODO JMAMAMAMAMAMAM : JourIfr � r�cup�rer
%         TP = cl _ time('timeIfr', JourIfr, HeureGPS);
% %         TP = datetime(DataPosition.time(:), 'ConvertFrom', 'posixtime');
%         Latitude (1:length(DatetimeBab),1) = my_interp1(TP, LatGPS, DatetimeBab, 'linear', 'extrap'); % timestamp
%         Latitude (1:length(DatetimeTri),2) = my_interp1(TP, LatGPS, DatetimeTri, 'linear', 'extrap'); % timestamp
%         Longitude(1:length(DatetimeBab),1) = my_interp1_longitude(TP, LonGPS, DatetimeBab, 'linear', 'extrap');
%         Longitude(1:length(DatetimeTri),2) = my_interp1_longitude(TP, LonGPS, DatetimeTri, 'linear', 'extrap');
%         flag = 1; % Ajout GLT
    end
end

sub = (Latitude == 0) | (Longitude == 0);
Latitude(sub)  = NaN;
Longitude(sub) = NaN;
