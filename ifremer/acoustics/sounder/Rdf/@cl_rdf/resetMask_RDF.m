function resetMask_RDF(~, nomFic, varargin)

if ~iscell(nomFic)
    nomFic = {nomFic};
end

N = length(nomFic);
hw = create_waitbar('RDF Quality Control processing.', 'N', N);
Carto = [];
for k=1:N
    my_waitbar(k, N, hw);
    [flag, Carto] = resetMask_RDF_unitaire(nomFic{k}, Carto);
    if ~flag
        my_close(hw)
        return
    end
end
my_close(hw, 'MsgEnd')


function [flag, Carto] = resetMask_RDF_unitaire(nomFic, Carto)

%% Lecture de l'image

this = cl_rdf('nomFic', nomFic);

%% Nettoyage de la réflectivité

[flag, a, Carto] = read_Image(this, 'Carto', Carto, 'UseMask', 0, 'SelecImage', 3);
if ~flag
    return
end
if isempty(a)
    return
end

M = get(a, 'Image');
M = ~isnan(M);
M(:) = 1;

% SonarScope(M)

[nomDir, nom] = fileparts(nomFic);
nomFicXml = fullfile(nomDir, 'SonarScope', nom, 'Ssc_Sample.xml');
Info = xml_read(nomFicXml);

clear a

nomDirRacine = fullfile(nomDir, 'SonarScope', nom);
flag = writeSignal(nomDirRacine, Info.Images(1), M);
if ~flag
    return
end

    
    
% fid = fopen(nomFicBin, 'w+');
% if fid == -1
%     return
% end
% count = fwrite(fid, M', Info.Images(3).Storage);
% fclose(fid);
% if count ~= (Info.Dimensions.nbCol * Info.Dimensions.nbPings)
%     count
% end

   
% nomFicBin = fullfile(nomDir, 'SonarScope', nom, Info.Images(1).FileName);
% fid = fopen(nomFicBin);
% Y = fread(fid, [Info.Dimensions.nbCol Info.Dimensions.nbPings], Info.Images(1).Storage);
% Y = Y';
% SonarScope(Y)
% fclose(fid);
