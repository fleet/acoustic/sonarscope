function flag = save_layer(this, NomLayer, X)

flag = existCacheNetcdf(this.nomFic);
if flag
    flag = save_layerNetcdf(this, NomLayer, X);
else
    flag = save_layerXMLBin(this, NomLayer, X);
end


function flag = save_layerXMLBin(this, NomLayer, X)

[nomDir, nom] = fileparts(this.nomFic);
nomFicXml = fullfile(nomDir, 'SonarScope', nom, 'Ssc_Sample.xml');
if ~exist(nomFicXml, 'file')
    flag = 0;
    return
end

Info = xml_read(nomFicXml);
nomDirRacine = fullfile(nomDir, 'SonarScope', nom);

k = strcmp({Info.Images.Name}, NomLayer);

flag = writeSignal(nomDirRacine, Info.Images(k), X);


function flag = save_layerNetcdf(this, NomLayer, Val)

[nomDir, nom] = fileparts(this.nomFic);
nomFicNc = fullfile(nomDir, 'SonarScope', [nom, '.nc']);
if ~exist(nomFicNc, 'file')
    flag = 0;
    return
end

ncID = netcdf.open(nomFicNc, 'WRITE');
grpID = netcdf.inqNcid(ncID, 'Sample');
varID = netcdf.inqVarID(grpID, NomLayer);
NetcdfUtils.putVarVal(grpID, varID, Val, 1);
netcdf.close(ncID);

flag = 1;
