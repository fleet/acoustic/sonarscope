function flag = save_signal(this, NomRep, NomLayer, X)

flag = existCacheNetcdf(this.nomFic);
if flag
    flag = save_signalNetcdf(this, NomRep, NomLayer, X);
else
    flag = save_signalXMLBin(this, NomRep, NomLayer, X);
end


function flag = save_signalXMLBin(this, NomRep, NomLayer, X)

[nomDir, nom] = fileparts(this.nomFic);
nomFicXml = fullfile(nomDir, 'SonarScope', nom, [NomRep '.xml']);
if ~exist(nomFicXml, 'file')
    flag = 0;
    return
end

Info = xml_read(nomFicXml);
nomDirRacine = fullfile(nomDir, 'SonarScope', nom);

k = strcmp({Info.Signals.Name}, NomLayer);
if isempty(k)
    str1 = sprintf('Signal "%s" non trouv� dans "%s', NomLayer, nomFicXml);
    str2 = 'TODO';
    my_warndlg(Lang(str1,str2), 1);
    return
end
flag = writeSignal(nomDirRacine, Info.Signals(k), X);



function flag = save_signalNetcdf(this, NomRep, NomLayer, Val)

[nomDir, nom] = fileparts(this.nomFic);
nomFicNc = fullfile(nomDir, 'SonarScope', [nom, '.nc']);
if ~exist(nomFicNc, 'file')
    flag = 0;
    return
end

NomRep = strrep(NomRep, 'Ssc_', '');

ncID = netcdf.open(nomFicNc, 'WRITE');
grpID = netcdf.inqNcid(ncID, NomRep);
varID = netcdf.inqVarID(grpID, NomLayer);
NetcdfUtils.putVarVal(grpID, varID, Val, 1);
netcdf.close(ncID);

flag = 1;
