function flag = save_signalHeight(this, HSamples)

flag = save_signal(this, 'Ssc_Sample', 'Height', HSamples);
