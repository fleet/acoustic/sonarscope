% Acces en ecriture des proprietes d'une instance
%
% Syntax
%   b = set(a, ...)
%
% Input Arguments
%   a : Une instance de la classe cl_rdf
%
% Name-Value Pair Arguments
%   nomFic : Nom(s) du fichier .rdf
%
% Output Arguments
%   [] : Sauve l'instance dans a
%   b  : Instance de la classe
%
% Examples
%   nomFic = getNomFicDatabase('EW150.rdf')
%   a = cl_rdf
%   a = set(a, 'nomFic', nomFic)
%
% See also cl_rdf cl_rdf/char Authors
% Authors : GLU
% ----------------------------------------------------------------------------

function varargout = set(this, varargin)

global isUsingParfor %#ok<GVMIS> 
global useCacheNetcdf %#ok<GVMIS> 

[varargin, nomFic]         = getPropertyValue(varargin, 'nomFic',         []);
[varargin, isUsingParfor]  = getPropertyValue(varargin, 'isUsingParfor',  isUsingParfor);
[varargin, useCacheNetcdf] = getPropertyValue(varargin, 'useCacheNetcdf', useCacheNetcdf); %#ok<ASGLU>

if ~isempty(nomFic)
    nomFic = windows_replaceVolumeLetter(nomFic);
    this.nomFic = nomFic;
    flag = rdf2ssc(nomFic);
    if ~flag
        varargout{1} = [];
        return
    end
end

%% Sortie

if nargout == 0
    assignin('caller', inputname(1), this);
else
    varargout{1} = this;
end
