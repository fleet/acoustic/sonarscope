function summary(this, fid)

fprintf(fid, '\n-------------------------------------------------------------------------------\n');
fprintf(fid, 'File\t\t\t : %s\n', get(this, 'nomFic'));

summary_FileHeader(this, fid)
summary_DataHeader(this, fid)
summary_Attitude(this, fid)
summary_Heading(this, fid)
summary_DataSample(this, fid)
summary_MiniSVS(this, fid)
summary_Navigation(this, fid)
summary_NavString(this, fid)



function summary_NavString(this, fid)
[flag, x] = SSc_ReadDatagrams(this.nomFic, 'Ssc_NavigationString');
if ~flag
    return % TODO / ecrire qu'il n'y a pas de datagrammes
end
[flag, HeureGPS, LatGPS, LonGPS, HeightGPS] = read_Navigation_NMEA(x.NavigationString); %#ok<ASGLU>
if ~flag
    return
end
fprintf(fid, '\n--- NavString datagrams ---\n');
fprintf(fid, '%s\n', formatVar('Nb Samples', x.Dimensions.NbSamples));
fprintf(fid, '%s\n', formatVar('NavigationStringLength', x.Dimensions.NavigationStringLength));
fprintf(fid, '%s\n', formatVar('Head of first datagram', x.NavigationString(1,1:min(100, size(x.NavigationString,2)))));
fprintf(fid, '%s\n', strEntete');
valStats = stats(HeureGPS);
fprintf(fid, '%s\n', formatStatsFull('HeureGPS', valStats));
valStats = stats(LatGPS);
fprintf(fid, '%s\n', formatStatsFull('LatGPS', valStats));
valStats = stats(LonGPS);
fprintf(fid, '%s\n', formatStatsFull('LonGPS', valStats));
% valStats = stats(HeightGPS);
% fprintf(fid, '%s\n', formatStatsFull('HeightGPS', valStats));






function summary_Navigation(this, fid)
[flag, x] = SSc_ReadDatagrams(this.nomFic, 'Ssc_Navigation');
if ~flag
    return % TODO / ecrire qu'il n'y a pas de datagrammes
end
fprintf(fid, '\n--- Navigation datagrams ---\n');
fprintf(fid, '%s\n', formatVar('Nb Samples', x.Dimensions.NbSamples));

T = datetime(x.timestamp(:), 'ConvertFrom', 'posixtime');
strTime = sprintf('%s  -  %s', char(T(1)), char(T(end)));
fprintf(fid, '%s\n', formatVar('Time', strTime));

T = datetime(x.time(:), 'ConvertFrom', 'posixtime');
strTime = sprintf('%s  -  %s', char(T(1)), char(T(end)));
fprintf(fid, '%s\n', formatVar('Time 2', strTime));

fprintf(fid, '%s\n', strEntete');
valStats = stats(x.x(:));
fprintf(fid, '%s\n', formatStatsFull('x', valStats));
valStats = stats(x.y(:));
fprintf(fid, '%s\n', formatStatsFull('y', valStats));
valStats = stats(x.z(:));
fprintf(fid, '%s\n', formatStatsFull('z', valStats));
valStats = stats(x.quality(:));
fprintf(fid, '%s\n', formatStatsFull('quality', valStats));


function summary_MiniSVS(this, fid)
[flag, x] = SSc_ReadDatagrams(this.nomFic, 'Ssc_MiniSVS');
if ~flag
    return % TODO / ecrire qu'il n'y a pas de datagrammes
end
fprintf(fid, '\n--- MiniSVS datagrams ---\n');
fprintf(fid, '%s\n', formatVar('Nb Samples', x.Dimensions.NbSamples));

T = datetime(x.timestamp(:), 'ConvertFrom', 'posixtime');
strTime = sprintf('%s  -  %s', char(T(1)), char(T(end)));

fprintf(fid, '%s\n', formatVar('Time', strTime));
fprintf(fid, '%s\n', strEntete');
valStats = stats(x.velocity(:));
fprintf(fid, '%s\n', formatStatsFull('velocity', valStats));


function summary_DataSample(this, fid)
[flag, x] = SSc_ReadDatagrams(this.nomFic, 'Ssc_Sample', 'Memmapfile', -1);
if ~flag
    return % TODO / ecrire qu'il n'y a pas de datagrammes
end
fprintf(fid, '\n--- DataSample datagrams ---\n');
fprintf(fid, '%s\n', formatVar('Nb Pings', x.Dimensions.nbPings));
fprintf(fid, '%s\n', formatVar('Nb Columns', x.Dimensions.nbCol));
fprintf(fid, '%s\n', strEntete');
valStats = stats(x.Mask(:));
fprintf(fid, '%s\n', formatStatsFull('Mask', valStats));
if isfield(x, 'Teta')
    valStats = stats(x.Teta(:));
    fprintf(fid, '%s\n', formatStatsFull('Teta', valStats));
end
valStats = stats(x.Reflectivity(:));
fprintf(fid, '%s\n', formatStatsFull('Reflectivity', valStats));


function summary_Heading(this, fid)
[flag, x] = SSc_ReadDatagrams(this.nomFic, 'Ssc_Heading');
if ~flag
    return % TODO / ecrire qu'il n'y a pas de datagrammes
end
fprintf(fid, '\n--- Heading datagrams ---\n');
fprintf(fid, '%s\n', formatVar('Nb Pings', x.Dimensions.NbSamples));

T = datetime(x.timestamp(:), 'ConvertFrom', 'posixtime');
strTime = sprintf('%s  -  %s', char(T(1)), char(T(end)));

fprintf(fid, '%s\n', formatVar('Time', strTime));
fprintf(fid, '%s\n', strEntete');
valStats = stats(x.Heading(:));
fprintf(fid, '%s\n', formatStatsFull('Heading', valStats));



function summary_Attitude(this, fid)
[flag, x] = SSc_ReadDatagrams(this.nomFic, 'Ssc_Attitude');
if ~flag
    return % TODO / ecrire qu'il n'y a pas de datagrammes
end
fprintf(fid, '\n--- Attitude datagrams ---\n');
fprintf(fid, '%s\n', formatVar('Nb Pings', x.Dimensions.NbSamples));

T = datetime(x.timestamp(:), 'ConvertFrom', 'posixtime');
strTime = sprintf('%s  -  %s', char(T(1)), char(T(end)));

fprintf(fid, '%s\n', formatVar('Time', strTime));
fprintf(fid, '%s\n', strEntete');
valStats = stats(x.roll(:));
fprintf(fid, '%s\n', formatStatsFull('roll', valStats));
valStats = stats(x.pitch(:));
fprintf(fid, '%s\n', formatStatsFull('pitch', valStats));
valStats = stats(x.heave(:));
fprintf(fid, '%s\n', formatStatsFull('heave', valStats));



function summary_FileHeader(this, fid)
[flag, x] = SSc_ReadDatagrams(this.nomFic, 'Ssc_FileHeader', 'XMLOnly', 1);
if ~flag
    return
end
fprintf(fid, '\n--- File Header datagrams ---\n');
fprintf(fid, '%s\n', formatVar('RawHeaderSize', x.RawHeaderSize));
fprintf(fid, '%s\n', formatVar('RawPingHeaderSize', x.RawPingHeaderSize));
fprintf(fid, '%s\n', formatVar('Frequency', x.Frequency));
fprintf(fid, '%s\n', formatVar('EchoType', x.EchoType));


function summary_DataHeader(this, fid)
[flag, x] = SSc_ReadDatagrams(this.nomFic, 'Ssc_Header');
if ~flag
    return
end
fprintf(fid, '\n--- Data Header datagrams ---\n');
fprintf(fid, '%s\n', formatVar('Nb Pings', x.Dimensions.NbSamples));
T = x.DateHeure(:);
[date, heure] = timeMat2Ifr(T);
strTime = sprintf('%s %s  -  %s % s', ...
    dayIfr2str(date(1)), hourIfr2str(heure(1)), ...
    dayIfr2str(date(end)), hourIfr2str(heure(end)));
fprintf(fid, '%s\n', formatVar('Time', strTime));
fprintf(fid, '%s\n', strEntete');
valStats = stats(x.PingNumber(:));
fprintf(fid, '%s\n', formatStatsLight('PingNumber', valStats));
valStats = stats(x.PingSize(:));
fprintf(fid, '%s\n', formatStatsLight('PingSize', valStats));
valStats = stats(x.NavigationNumber(:));
fprintf(fid, '%s\n', formatStatsLight('NavigationNumber', valStats));
valStats = stats(x.AttitudeNumber(:));
fprintf(fid, '%s\n', formatStatsLight('AttitudeNumber', valStats));
valStats = stats(x.HeadingNumber(:));
fprintf(fid, '%s\n', formatStatsLight('HeadingNumber', valStats));
valStats = stats(x.EchoSounderNumber(:));
fprintf(fid, '%s\n', formatStatsLight('EchoSounderNumber', valStats));
valStats = stats(x.MiniSVSNumber(:));
fprintf(fid, '%s\n', formatStatsLight('MiniSVSNumber', valStats));
valStats = stats(x.Aux1Number(:));
fprintf(fid, '%s\n', formatStatsLight('Aux1Number', valStats));
valStats = stats(x.Aux2Number(:));
fprintf(fid, '%s\n', formatStatsLight('Aux2Number', valStats));
valStats = stats(x.PingLength(:));
fprintf(fid, '%s\n', formatStatsFull('PingLength', valStats));
valStats = stats(x.PulseLength(:));
fprintf(fid, '%s\n', formatStatsFull('PulseLength', valStats));
valStats = stats(x.Power(:));
fprintf(fid, '%s\n', formatStatsFull('Power', valStats));
valStats = stats(x.SideScanGain(:));
fprintf(fid, '%s\n', formatStatsFull('SideScanGain', valStats));
valStats = stats(x.SampleNumber(:));
fprintf(fid, '%s\n', formatStatsLight('SampleNumber', valStats));
valStats = stats(x.Side(:));
fprintf(fid, '%s\n', formatStatsLight('Side', valStats));
valStats = stats(x.NavigationStringSize(:));
fprintf(fid, '%s\n', formatStatsLight('NavigationStringSize', valStats));
valStats = stats(x.AttitudeStringSize(:));
fprintf(fid, '%s\n', formatStatsLight('AttitudeStringSize', valStats));
valStats = stats(x.HeadingStringSize(:));
fprintf(fid, '%s\n', formatStatsLight('HeadingStringSize', valStats));
valStats = stats(x.EchoSounderStringSize(:));
fprintf(fid, '%s\n', formatStatsLight('EchoSounderStringSize', valStats));
valStats = stats(x.MiniSVSStringSize(:));
fprintf(fid, '%s\n', formatStatsLight('MiniSVSStringSize', valStats));
valStats = stats(x.Aux1StringSize(:));
fprintf(fid, '%s\n', formatStatsLight('Aux1StringSize', valStats));
valStats = stats(x.Aux2StringSize(:));
fprintf(fid, '%s\n', formatStatsLight('Aux2StringSize', valStats));



function strStats = formatStatsFull(Nom, valStats)

strStats = repmat(' ', 1, 110);
strStats(1:length(Nom)) = Nom;
strStats(24) = ':';
str = num2str(valStats.Moyenne);
strStats(26:26+length(str)-1) = str;
str = num2str(valStats.Sigma);
strStats(40:40+length(str)-1) = str;
str = num2str(valStats.Mediane);
strStats(54:54+length(str)-1) = str;
str = num2str(valStats.Min);
strStats(68:68+length(str)-1) = str;
str = num2str(valStats.Max);
strStats(82:82+length(str)-1) = str;
str = num2str(valStats.Range);
strStats(96:96+length(str)-1) = str;

function strStats = formatStatsLight(Nom, valStats)

strStats = repmat(' ', 1, 110);
strStats(1:length(Nom)) = Nom;
strStats(24) = ':';
str = '---------';
strStats(26:26+length(str)-1) = str;
str = '---------';
strStats(40:40+length(str)-1) = str;
str = num2str(valStats.Mediane);
strStats(54:54+length(str)-1) = str;
str = num2str(valStats.Min);
strStats(68:68+length(str)-1) = str;
str = num2str(valStats.Max);
strStats(82:82+length(str)-1) = str;
str = '---------';
strStats(96:96+length(str)-1) = str;

function str = strEntete

str = repmat(' ', 1, 100);
str(1:10) = 'Statistics';
str(24) = ':';
str(26:29) = 'Mean';
str(40:42) = 'Std';
str(54:59) = 'Median';
str(68:70) = 'Min';
str(82:84) = 'Max';
str(96:100) = 'Range';

function str = formatVar(Nom, val)
str = repmat(' ', 1, 110);
str(1:length(Nom)) = Nom;
str(24) = ':';
if ischar(val)
    strVal = val;
else
    val = val(:);
    strVal = num2str(val');
end
str(26:26+length(strVal)-1) = strVal;
