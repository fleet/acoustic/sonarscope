function [flag, str] = summary_Lines(this)

str = {};

%% Lecture de l'entete du fichier

[flag, DataHeader] = SSc_ReadDatagrams(this.nomFic, 'Ssc_Header');
if ~flag
    return
end

%% Attitude

[flag, DataAttitude] = SSc_ReadDatagrams(this.nomFic, 'Ssc_Attitude');
if ~flag
%     return % Donn�es UNH Val
end

%% Heading

[flag, DataHeading] = SSc_ReadDatagrams(this.nomFic, 'Ssc_Heading');
if ~flag
%     return % Donn�es UNH Val
end

%% Images

[flag, DataSample] = SSc_ReadDatagrams(this.nomFic, 'Ssc_Sample', 'Memmapfile', -1);
if ~flag
    return
end

%% MiniSVS

[~, DataMiniSVS] = SSc_ReadDatagrams(this.nomFic, 'Ssc_MiniSVS');

%% Ssc_EchoSounderString

% [flag, DataEchoSounderString] = SSc_ReadDatagrams(this.nomFic, 'Ssc_EchoSounderString'); % Non test�

%% Ssc_EchoSounder

% [flag, DataEchoSounder] = SSc_ReadDatagrams(nomFic, 'Ssc_EchoSounder'); % Non test�

%% R�cup�ration des donn�es

T2            = DataHeader.DateHeure(:);
PulseLength   = DataHeader.PulseLength(:);
Power         = DataHeader.Power(:);
SideScanGain  = DataHeader.SideScanGain(:);                       

roll  = DataAttitude.roll(:);
pitch = DataAttitude.pitch(:);
heave = DataAttitude.heave(:);

heading = DataHeading.Heading(:);

velocity = DataMiniSVS.velocity(:);

str{end+1} = this.nomFic;
str{end+1} = char(datetime(T2(1),   'ConvertFrom', 'datenum'));
str{end+1} = char(datetime(T2(end), 'ConvertFrom', 'datenum'));

str{end+1} = num2str(DataSample.Dimensions.nbPings);
str{end+1} = num2str(DataSample.Dimensions.nbCol);

str{end+1} = num2str(median(PulseLength, 'omitnan'));
str{end+1} = num2str(median(Power, 'omitnan'));
str{end+1} = num2str(median(SideScanGain, 'omitnan'));

str{end+1} = num2str(mean(velocity, 'omitnan'));

str{end+1} = num2str(median(heading, 'omitnan'));
% str{end+1} = 'TODO'; % 'Depth (mean)'
str{end+1} = 'TODO'; % 'Nb SoundSpeedProfiles'
str{end+1} = num2str(mean(roll,  'omitnan'));
str{end+1} = num2str(std( roll,  'omitnan'));
str{end+1} = num2str(mean(pitch, 'omitnan'));
str{end+1} = num2str(std( pitch, 'omitnan'));
str{end+1} = num2str(mean(heave, 'omitnan'));
str{end+1} = num2str(std( heave, 'omitnan'));
str{end+1} = num2strMoney(sizeFic(this.nomFic));

flag = 1;
