% Visualisation du contenu des datagrams "Seabed Image" contenus dans un fichier .rdf
%
% Syntax
%   [b,S] = view_Image(a, ...);
%
% Input Arguments
%   a : Instance de cl_rdf
%
% Name-Value Pair Arguments
%   Carto        :
%   memeReponses :
%   ListeLayers  :
%
% Output Arguments
%   b            : Instance de cli_image
%   Carto        :
%   memeReponses :
%
% Examples
%   nomFic = getNomFicDatabase('getNomFicDatabase('EW150.RDF');
%   a = cl_rdf('nomFic', nomFic);
%   b = view_Image(a);
%   SonarScope(b);
%
%   b = view_Image(a, 'subl', 1:100);
%   SonarScope(b);
%
% See also cl_rdf cl_rdf/plot_position cl_rdf/plot_nav Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [flag, b, Carto, memeReponses] = view_Image(this, varargin)

persistent persistent_ListeLayers

[varargin, Carto]                  = getPropertyValue(varargin, 'Carto',                  []);
[varargin, memeReponses]           = getPropertyValue(varargin, 'memeReponses',           false);
[varargin, ListeLayers]            = getPropertyValue(varargin, 'ListeLayers',            []);
[varargin, InstallationParameters] = getPropertyValue(varargin, 'InstallationParameters', []); %#ok<ASGLU>

b = [];

%% Check the number of elements

flag = checkIfOneElement(length(this));
if ~flag
    return
end

%% Lecture de la donnee

if isempty(ListeLayers)
    if ~memeReponses
        clear str
        str{1} = 'Mask';
        str{end+1} = 'Angle Raw';
        str{end+1} = 'Angle Processed';
        str{end+1} = 'Reflectivity';
        str{end+1} = 'Depth';
        str{end+1} = 'AcrossDist';
        str{end+1} = 'AlongDist';
        
        ListeLayers = my_listdlg('Layers :', str, 'InitialValue', [2 4]);
        if isempty(ListeLayers)
            flag = 0;
            return
        end
        persistent_ListeLayers = ListeLayers;
    else
        ListeLayers = persistent_ListeLayers;
    end
end

[flag, b, Carto, InstallationParameters] = read_Image(this, 'SelecImage', ListeLayers, ...
    'Carto', Carto, 'InstallationParameters', InstallationParameters);
if ~flag
    return
end

%% Edition de l'objet cli_image

if nargout == 0
    SonarScope(b);
end

flag = 1;
