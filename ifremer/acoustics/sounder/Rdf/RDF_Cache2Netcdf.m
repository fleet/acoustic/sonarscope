% [flag, dataFileName] = FtpUtils.getSScDemoFile('ESSTECH19020.rdf', 'OutputDir', 'D:\Temp\ExData'); % Geoswath
%
% RDF_Cache2Netcdf(dataFileName)

function flag = RDF_Cache2Netcdf(listFileNames, varargin)

[varargin, deleteXMLBin] = getPropertyValue(varargin, 'deleteXMLBin', 0);
[varargin, deflateLevel] = getPropertyValue(varargin, 'deflateLevel', 1); %#ok<ASGLU>

if ischar(listFileNames)
    listFileNames = {listFileNames};
end

N = length(listFileNames);
hw = create_waitbar('Cache directory : Conversion from XML-Bin to Netcdf', 'N', N);
for k=1:N
    flag(k) = RDF_Cache2Netcdf_unitaire(listFileNames{k}, deflateLevel); %#ok<AGROW>
    if flag(k)
        if deleteXMLBin
            SScCacheXMLBinUtils.deleteCachDirectory(listFileNames{k});
        end
    end
    my_waitbar(k, N, hw)
end
my_close(hw, 'MsgEnd')


function flag = RDF_Cache2Netcdf_unitaire(dataFileName, deflateLevel)

fprintf('\nProcessing RDF_Cache2Netcdf file %s, deflateLevel=%d\n', dataFileName, deflateLevel);

%% Pour �tre s�r que le XMLBin est cr��

cl_rdf('nomFic', dataFileName);

%% Cr�ation du fichier Netcdf

[flag, ncFileName] = SScCacheNetcdfUtils.createSScNetcdfFile(dataFileName);
if ~flag
    return
end

%% Transfert des donn�es Navigation

flag = SScCacheNetcdfUtils.Cache2Netcdf(ncFileName, dataFileName, 'Navigation', 'deflateLevel', deflateLevel);
if ~flag
    return
end

%% Transfert des donn�es Height

flag = SScCacheNetcdfUtils.Cache2Netcdf(ncFileName, dataFileName, 'Height', 'deflateLevel', deflateLevel);
if ~flag
    return
end

%%  Transfert des donn�es Ssc_Heading

flag = SScCacheNetcdfUtils.Cache2Netcdf(ncFileName, dataFileName, 'Heading', 'deflateLevel', deflateLevel);
if ~flag
    return
end

%%  Transfert des donn�es Ssc_Attitude

flag = SScCacheNetcdfUtils.Cache2Netcdf(ncFileName, dataFileName, 'Attitude', 'deflateLevel', deflateLevel);
if ~flag
    return
end

%%  Transfert des donn�es Ssc_Aux1String

flag = SScCacheNetcdfUtils.Cache2Netcdf(ncFileName, dataFileName, 'Aux1String', 'deflateLevel', deflateLevel);
if ~flag
    return
end

%%  Transfert des donn�es Ssc_FileIndex

flag = SScCacheNetcdfUtils.Cache2Netcdf(ncFileName, dataFileName, 'FileIndex', 'deflateLevel', deflateLevel);
if ~flag
    return
end

%%  Transfert des donn�es Ssc_Header

flag = SScCacheNetcdfUtils.Cache2Netcdf(ncFileName, dataFileName, 'Header', 'deflateLevel', deflateLevel);
if ~flag
    return
end

%%  Transfert des donn�es Ssc_HeadingString

flag = SScCacheNetcdfUtils.Cache2Netcdf(ncFileName, dataFileName, 'HeadingString', 'deflateLevel', deflateLevel);
if ~flag
    return
end

%%  Transfert des donn�es Ssc_MiniSVSString

flag = SScCacheNetcdfUtils.Cache2Netcdf(ncFileName, dataFileName, 'MiniSVSString', 'deflateLevel', deflateLevel);
if ~flag
    return
end

%%  Transfert des donn�es Ssc_MiniSVS

flag = SScCacheNetcdfUtils.Cache2Netcdf(ncFileName, dataFileName, 'MiniSVS', 'deflateLevel', deflateLevel);
if ~flag
    return
end

%%  Transfert des donn�es Ssc_NavigationString

flag = SScCacheNetcdfUtils.Cache2Netcdf(ncFileName, dataFileName, 'NavigationString', 'deflateLevel', deflateLevel);
if ~flag
    return
end

%%  Transfert des donn�es Ssc_Aux2String

flag = SScCacheNetcdfUtils.Cache2Netcdf(ncFileName, dataFileName, 'Aux2String', 'deflateLevel', deflateLevel);
if ~flag
    return
end

%%  Transfert des donn�es Ssc_FileHeader

flag = SScCacheNetcdfUtils.Cache2Netcdf(ncFileName, dataFileName, 'FileHeader', 'deflateLevel', deflateLevel);
if ~flag
    return
end

%%  Transfert des donn�es Ssc_Sample

flag = SScCacheNetcdfUtils.Cache2Netcdf(ncFileName, dataFileName, 'Sample', 'deflateLevel', deflateLevel);
if ~flag
    return
end

%%  Transfert des donn�es Ssc_EchoSounderString

flag = SScCacheNetcdfUtils.Cache2Netcdf(ncFileName, dataFileName, 'EchoSounderString', 'deflateLevel', deflateLevel);
if ~flag
    return
end

%%  Transfert des donn�es Ssc_EchoSounder

flag = SScCacheNetcdfUtils.Cache2Netcdf(ncFileName, dataFileName, 'EchoSounder', 'deflateLevel', deflateLevel);
if ~flag
    return
end

%% Renommage de l'extension .NC en .nc pour indiquer � l'utilisateur que le fichier est termin�

my_rename(ncFileName, strrep(ncFileName, '.NC', '.nc'));

%% The End

flag = 1;
