function [flag, Geoswath] = RDF_DefineInstallationParameters(repImport, nomFicXml, varargin)

[varargin, FileNameAlreadyDefined] = getPropertyValue(varargin, 'FileNameAlreadyDefined', 0); %#ok<ASGLU>

str{1} = 'IFREMER - Haliotis';
str{2} = 'UNH ship';
str{3} = 'Shallow Survey 2012';
str{4} = 'Shallow Survey 2015 - After Pospac data integration';
str1 = 'Pr�initialisation des valeurs pour :';
str2 = 'Initialize values for';
[rep, flag] = my_listdlg(Lang(str1,str2), str, 'SelectionMode', 'Single');
if ~flag
    Geoswath = [];
    return
end

str1 = 'ATTENTION : la navigation des donn�es du Geoswatrh est d�finie dans une projection particuli�re (Lambet93 pour l''Haliotis, UTM ailleurs ?) N''oubliez pas de d�ninir la projection ad�quate au moment o� vous lisez pour la premi�re fois un fichier RDF. Cela ne vous emp�che pas de d�finir une projection cartographique diff�rente par la suite.';
str2 = 'WARNING : the navigation of the Geoswath is defined in a particular projection (Lambert93 for Haliotis, UTM elsewere ?). Do not forget to define the appropriate projection when reading the RDF files. You can then define another cartographic parameters if needed.';
switch rep
    case 1 % Haliotis
        pVal(1) = -0.09;  % SbabY (m)
        pVal(2) = -0.12;  % SbabX (m)
        pVal(3) = -0.37;  % SbabZ (m)
        pVal(4) = -0.09;  % StriY (m)
        pVal(5) =  0.11;  % StriX (m)
        pVal(6) = -0.37;  % StriZ (m)
        pVal(7) = -0.23;  % Draught (m)
        pVal(8)  = 0;     % SBabHeading (deg)
        pVal(9)  = 0;     % STriHeading (deg)
        pVal(10) = 0;     % SBabRoll (deg)
        pVal(11) = 1.35;  % STriRoll (deg)
        pVal(12) = 1;     % SBabPitch (deg)
        pVal(13) = 1.5;   % STriPitch (deg)
        pVal(14) = 0;     % DeltaTPos (ms)
        pVal(15) = 12.5;  % DeltaTMRU (ms)
        
        str1 = 'ATTENTION : la navigation des donn�es du Geoswatrh de l''Haliotis est quasiment toujours d�finie en Lambert93. N''oubliez pas de d�ninir cette projection l� au moment o� vous lisez pour la premi�re fois un fichier RDF. Cela ne vous emp�che pas de d�finir une projection cartographique diff�rente par la suite.';
        str2 = 'WARNING : the navigation of the Geoswath for Haliotis is most of time defined in Lambert93. Do not forget to define this projection when reading the RDF files. You can then define another cartographic parameters if needed.';
        my_warndlg(Lang(str1,str2), 1);
        
    case 2 % UNH
        pVal = zeros(15,1);
        pVal(7) = 0.953;
        my_warndlg(Lang(str1,str2), 1);
        
    case 3 % Shallow Survey 2012
        pVal = zeros(15,1);
        my_warndlg(Lang(str1,str2), 1);
        
    case 4 % Shallow Survey 2015
        pVal(1)  =  0;     % SbabY (m) : 'Port antenna longitudinal offset (-rear, +ehead)'
        pVal(2)  = -0.04;  % SbabX (m) : 'Port antenna transversal offset (-port, +starboard)'
        pVal(3)  =  0;     % SbabZ (m) : 'Port antenna vertical offset (-deep, +surface)'
        pVal(4)  =  0;     % StriY (m) : 'Starboard antenna longitudinal offset (-rear, +ehead)'
        pVal(5)  =  0.04;  % StriX (m) : 'Starboard antenna transversal offset (-port, +starboard)'
        pVal(6)  =  0;     % StriZ (m) : 'Starboard antenna vertical offset (-deep, +surface)'
        pVal(7)  = -0.83;  % Draught (m) : 'Draught (-deep, +surface)'
        pVal(8)  =  1.2;   % SBabHeading (deg) : 'Heading correction for port antenna (-port,+starboard)'
        pVal(9)  =  1.2;   % STriHeading (deg) : 'Heading correction for starboard antenna (-port,+starboard)'
        pVal(10) =  0.15;  % SBabRoll (deg)    : 'Roll correction for port antenna (-port,+starboard)',
        pVal(11) = -0.06;  % STriRoll (deg)    : 'Roll correction for starboard antenna (-port,+starboard)'
        pVal(12) =  0.8;   % SBabPitch (deg)   : 'Pitch correction for port antenna (-bow down,+bow up)'
        pVal(13) =  0.8;   % STriPitch (deg)   : 'Pitch correction for starboard antenna (-bow down,+bow up)'
        pVal(14) =  0;     % DeltaTPos (ms)    : 'GPS offset'
        pVal(15) =  0;     % DeltaTMRU (ms)    : 'MRU offset'
end

str1 = 'Param�tres d''installation du sonar Geoswath.';
str2 = 'Geoswath Installation Parameters.';
p(1) = ClParametre('Name', 'Port antenna longitudinal offset (-rear, +ehead)', ...
    'Unit', 'm',  'MinValue', -1, 'MaxValue', 1, 'Value', pVal(1));
p(2) = ClParametre('Name', 'Port antenna transversal offset (-port, +starboard)', ...
    'Unit', 'm',  'MinValue', -1, 'MaxValue', 1, 'Value', pVal(2));
p(3) = ClParametre('Name', 'Port antenna vertical offset (-deep, +surface)', ...
    'Unit', 'm',  'MinValue', -1, 'MaxValue', 1, 'Value', pVal(3));
p(4) = ClParametre('Name', 'Starboard antenna longitudinal offset (-rear, +ehead)', ...
    'Unit', 'm',  'MinValue', -1, 'MaxValue', 1, 'Value', pVal(4));
p(5) = ClParametre('Name', 'Starboard antenna transversal offset (-port, +starboard)', ...
    'Unit', 'm',  'MinValue', -1, 'MaxValue', 1, 'Value', pVal(5));
p(6) = ClParametre('Name', 'Starboard antenna vertical offset (-deep, +surface)', ...
    'Unit', 'm',  'MinValue', -1, 'MaxValue', 1, 'Value', pVal(6));
p(7) = ClParametre('Name', 'Draught (-deep, +surface)', ...
    'Unit', 'm',  'MinValue', -1, 'MaxValue', 1, 'Value', pVal(7));
p(8) = ClParametre('Name', 'Heading correction for port antenna (-port,+starboard)', ...
    'Unit', 'deg',  'MinValue', -1, 'MaxValue', 2, 'Value', pVal(8));
p(9) = ClParametre('Name', 'Heading correction for starboard antenna (-port,+starboard)', ...
    'Unit', 'deg',  'MinValue', -1, 'MaxValue', 2, 'Value', pVal(9));
p(10) = ClParametre('Name', 'Roll correction for port antenna (-port,+starboard)', ...
    'Unit', 'deg',  'MinValue', -5, 'MaxValue', 5, 'Value', pVal(10));
p(11) = ClParametre('Name', 'Roll correction for starboard antenna (-port,+starboard)', ...
    'Unit', 'deg',  'MinValue', -5, 'MaxValue', 5, 'Value', pVal(11));
p(12) = ClParametre('Name', 'Pitch correction for port antenna (-bow down,+bow up)', ...
    'Unit', 'deg',  'MinValue', -5, 'MaxValue', 5, 'Value', pVal(12));
p(13) = ClParametre('Name', 'Pitch correction for starboard antenna (-bow down,+bow up)', ...
    'Unit', 'deg',  'MinValue', -5, 'MaxValue', 5, 'Value', pVal(13));
p(14) = ClParametre('Name', 'GPS offset', ...
    'Unit', 'ms',  'MinValue', 0, 'MaxValue', 2000, 'Value', pVal(14));
p(15) = ClParametre('Name', 'MRU offset', ...
    'Unit', 'ms',  'MinValue', 0, 'MaxValue', 2000, 'Value', pVal(15));
a = StyledParametreDialog('params', p, 'Title', Lang(str1,str2));
% a.Help = 'http://flotte.ifremer.fr/fleet/Presentation-of-the-fleet/On-board-software/SonarScope';
a.sizes = [0 -4 0 0 0 -1 -1 0];
a.openDialog;
flag = a.okPressedOut;
if ~flag
    Geoswath = [];
    return
end
pVal = a.getParamsValue;

Geoswath.Port.AntennaOffsetY         = pVal(1);
Geoswath.Port.AntennaOffsetX         = pVal(2);
Geoswath.Port.AntennaOffsetZ         = pVal(3);
Geoswath.Port.HeadingCorrection      = pVal(8);
Geoswath.Port.RollCorrection         = pVal(10);
Geoswath.Port.PitchCorrection        = pVal(12);

Geoswath.Starboard.AntennaOffsetY    = pVal(4);
Geoswath.Starboard.AntennaOffsetX    = pVal(5);
Geoswath.Starboard.AntennaOffsetZ    = pVal(6);
Geoswath.Starboard.HeadingCorrection = pVal(9);
Geoswath.Starboard.RollCorrection    = pVal(11);
Geoswath.Starboard.PitchCorrection   = pVal(13);

Geoswath.Draught                     = pVal(7);
Geoswath.GPSDelay                    = pVal(14);
Geoswath.MRUDelay                    = pVal(15);

if FileNameAlreadyDefined
    xml_write(nomFicXml, Geoswath);
    flag = exist(nomFicXml, 'file');
    if ~flag
        messageErreurFichier(nomFicXml, 'WriteFailure');
        return
    end
else
    if ~isempty(repImport)
        str1 = 'Nom �ventuel du fichier de sauvegarde des param�tres d''acquisition.';
        str2 = 'Eventual name of the output file to save installation parameters.';
        [flag, nomFicXml] = my_uiputfile('*.xml', Lang(str1,str2), nomFicXml);
        if ~flag
            return
        end
        
        xml_write(nomFicXml, Geoswath);
        flag = exist(nomFicXml, 'file');
        if ~flag
            messageErreurFichier(nomFicXml, 'WriteFailure');
            return
        end
    end
end
