function  RDF_MosaiqueParProfil(nomDirExport, listeFicRDF, gridSize, CorFile, ModeDependant, selectionData, Window, MosHeading, skipExistingMosaic)

% [varargin, XLim] = getPropertyValue(varargin, 'XLim', []);
% [varargin, YLim] = getPropertyValue(varargin, 'YLim', []);
% [varargin, gridSize] = getPropertyValue(varargin, 'Resol', []);
% [varargin, Layers] = getPropertyValue(varargin, 'Layers', []);
% [varargin, Backup] = getPropertyValue(varargin, 'Backup', []);
% [varargin, nomDirImport] = getPropertyValue(varargin, 'nomDirImport', []);
% [varargin, nomDirExport] = getPropertyValue(varargin, 'nomDirExport', []);
% [varargin, window] = getPropertyValue(varargin, 'window', []);

%% Type de layer � mosa�quer

listeLayers = {'Reflectivity'; 'Depth'};
LayerName = listeLayers{selectionData};

nbProfils = length(listeFicRDF);

%% Noms des fichiers de sauvegarde des mosaiques

if selectionData == 1
    nomDataType = 'Reflectivity';
else
    nomDataType = 'Bathymetry';
end

%% Si Mosaique de Depth, on propose de faire la correction de mar�e
% TODO : d�porter ce qui suit dans la fonction de d�finition des param�tres

%{

%% Compensation statistique

for kf=1:length(CorFile)
CourbeConditionnelle = load_courbesStats(CorFile{kf});
bilan = CourbeConditionnelle.bilan;
if isfield(bilan{1}(1), 'Mode') && ~isempty(bilan{1}(1).Mode) && (bilan{1}(1).Mode ~= 0)
break
end
end
%}

%% Constitution de la mosaique / MNT

% profile on
% tic
Carto = [];
InstallationParameters = [];
str1 = 'Mosaique des fichiers Geoacoustics .rdf';
str2 = 'Mosaic of Geoswath files (.rdf)';
hw = create_waitbar(Lang(str1,str2), 'N', nbProfils);
for k=1:nbProfils
    my_waitbar(k, nbProfils, hw)
    
    fprintf('\n--------------------------------------------------------------------------------------\n');
    fprintf('Processing file %d/%d : %s\n', k, nbProfils, listeFicRDF{k});
    
    if skipExistingMosaic
        [~, nomFic] = fileparts(listeFicRDF{k});
        nomFicErs = fullfile(nomDirExport, [nomFic '_' nomDataType '_LatLong.ers']);
        if exist(nomFicErs, 'file')
            continue
        end
    end
    
    a = cl_rdf('nomFic', listeFicRDF{k});
    
    [flag, c, Carto, InstallationParameters] = read_Image(a, 'SelecImage', 3:7, ...
        'Carto', Carto, 'InstallationParameters', InstallationParameters);
    if ~flag || isempty(c)
        continue
    end
    
    switch selectionData
        case 1
            Z = c(2);
        case 2
            Z = c(3);
    end
    
    nbRows = Z.nbRows;
    if nbRows <= 1
        Message = sprintf('The file %s is discarded because it has %d row.', listeFicRDF{k}, nbRows);
        my_warndlg(Message, 0);
        continue
    end
    
    DT = cl_image.indDataType('AcrossDist');
    indLayer = findIndLayerSonar(c, 1, 'DataType', DT, 'WithCurrentImage', 1);
    AcrossDistance = c(indLayer);
    
    DT = cl_image.indDataType('AlongDist');
    indLayer = findIndLayerSonar(c, 1, 'DataType', DT, 'WithCurrentImage', 1);
    AlongDistance = c(indLayer);
    
    %DT = cl_image.indDataType('TxAngle');
    DT = cl_image.indDataType('BeamPointingAngle');
    indLayer = findIndLayerSonar(c, 1, 'DataType', DT, 'WithCurrentImage', 1);
    A = c(indLayer);
    
    Heading       = get(Z, 'Heading');
    FishLatitude  = get(Z, 'FishLatitude');
    FishLongitude = get(Z, 'FishLongitude');
    
    for kf=1:length(CorFile)
        CourbeConditionnelle = load_courbesStats(CorFile{kf});
        bilan = CourbeConditionnelle.bilan;
        indexLayersCompensation = [];
        for k2=1:length(bilan{1})
            strDataType = bilan{1}(k2).DataTypeConditions;
            DataType = cl_image.indDataType(strDataType);
            indexLayersCompensation(end+1) = findIndLayerSonar(c, 1, 'DataType', DataType, 'WithCurrentImage', 1); %#ok<AGROW>
        end
        indexLayersCompensation = unique(indexLayersCompensation);
        
        [Z, flag] = compensationCourbesStats(Z, c(indexLayersCompensation), bilan, 'ModeDependant', ModeDependant);
        if ~flag
            return
        end
        Z = optimiseMemory(Z);
    end
    
    %% Calcul des coordonn�es g�ographiques
    
    [flag, LatLon] = sonar_calcul_coordGeo_BathyFais(Z, AcrossDistance(1), AlongDistance(1), ...
        Heading, FishLatitude, FishLongitude);
    if ~flag
        return
    end
    LatLon = optimiseMemory(LatLon);
    
    %% Interpolation
    
    %     WRDF = [5 21];
    %     Z = WinFillNaN(Z, 'window', WRDF, 'NoStats');
    %     A = WinFillNaN(A, 'window', WRDF, 'NoStats');
    %     LatLon(1) = WinFillNaN(LatLon(1), 'window', WRDF, 'NoStats');
    %     LatLon(2) = WinFillNaN(LatLon(2), 'window', WRDF, 'NoStats');
    
    %% Tentative d'interpolation si mosaique de la r�flectivit�
    
    if strcmp(LayerName, 'Reflectivity')
        [flag, A, LatLon] = sonar_GeoswathInterpAngles(A, LatLon);
        if ~flag
            return
        end
        [flag, Z] = sonar_GeoswathFilterReflec(Z);
        if ~flag
            return
        end
    end
    
    %% Mosaique de la ligne
    
    [flag, Z_Mosa, A_Mosa] = sonar_mosaique2(Z, A, LatLon(1), LatLon(2), gridSize, ...
        'AcrossInterpolation', 2, 'AlongInterpolation',  1);
    if ~flag
        return
    end
    clear LatLon
    
    %% Interpolation
    
    if ~isequal(Window, [1 1])
        Z_Mosa = WinFillNaN(Z_Mosa, 'window', Window, 'NoStats');
        A_Mosa = WinFillNaN(A_Mosa, 'window', Window, 'NoStats');
    end
    
    clear c
    
    %% Sauvegarde de la mosa�que du layer principal
    
    [~, nomFic] = fileparts(listeFicRDF{k});
    nomFicErs = fullfile(nomDirExport, [nomFic '_' nomDataType '_LatLong.ers']);
    flag = export_ermapper(Z_Mosa, nomFicErs);
    if ~flag
        return
    end
    %     InitialImageName = Z_Mosa.InitialImageName
    %
    %     % TODO : � l'essai
    %     InitialImageName = strrep(InitialImageName, '_Compensation_Compensation', '_Compensation');
    %
    %     InitialImageName = [InitialImageName ' - ' num2str(rand(1))]; %#ok<AGROW>
    % %     Layer_LatLong = WinFillNaN(Layer_LatLong, 'window', Window);
    %     Z_Mosa.InitialImageName = InitialImageName);
    
    [~, nomFic] = fileparts(listeFicRDF{k});
    nomFicErs = fullfile(nomDirExport, [nomFic '_TxAngle_LatLong.ers']);
    flag = export_ermapper(A_Mosa, nomFicErs);
    if ~flag
        return
    end
    
    %% Sauvegarde de la mosa�que du cap
    
    if MosHeading
        [~, nomFic] = fileparts(listeFicRDF{k});
        nomFicErs = fullfile(nomDirExport, [nomFic '_Heading_LatLong.ers']);
        flag = export_ermapper(Heading_LatLong, nomFicErs);
        if ~flag
            return
        end
    end
    
    %% Suppression des fichier memmapfile
    
    deleteFileOnListe(cl_memmapfile.empty, gcbf)
end
my_close(hw, 'MsgEnd')
