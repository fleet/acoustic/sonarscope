% TODO : correction de mar�e pas branch�e

function [flag, Mosa, nomDirImport, nomDirExport] = RDF_PingSamples2LatLong(varargin)

[varargin, XLim]                  = getPropertyValue(varargin, 'XLim',                  []);
[varargin, YLim]                  = getPropertyValue(varargin, 'YLim',                  []);
[varargin, resol]                 = getPropertyValue(varargin, 'Resol',                 []);
[varargin, Layers]                = getPropertyValue(varargin, 'Layers',                []);
% [varargin, Backup]              = getPropertyValue(varargin, 'Backup',                []);
[varargin, nomDirImport]          = getPropertyValue(varargin, 'nomDirImport',          []);
[varargin, nomDirExport]          = getPropertyValue(varargin, 'nomDirExport',          []);
[varargin, window]                = getPropertyValue(varargin, 'window',                []);
[varargin, SpecularInterpolation] = getPropertyValue(varargin, 'SpecularInterpolation', 0); %#ok<ASGLU>

Mosa = [];

I0 = cl_image.empty;

% TODO : transf�rer la saisie des param�tres � l'ext�rieur

%% Type de layer � mosa�quer

if isempty(Layers)
    str1 = 'Layer � mosa�quer';
    str2 = 'Layer to mosaic';
    listeLayers = {'Reflectivity'; 'Depth'; 'Sidescan'};
    [choixLayer, flag] = my_listdlg(Lang(str1,str2), listeLayers, 'InitialValue', 1, 'SelectionMode', 'Single');
    if ~flag
        return
    end
    LayerName = listeLayers{choixLayer};
end

%% Recherche des fichiers .rdf

if isempty(nomDirImport)
    nomDirImport = my_tempdir;
end
[flag, liste] = uiSelectFiles('ExtensionFiles', '.rdf', 'RepDefaut', nomDirImport);
if ~flag || isempty(liste)
    return
end

nbProfils = length(liste);
nomDirImport = fileparts(liste{1});

%% Taille de la grille

[flag, resol] = get_gridSize('Value', -resol, 'Initial', 0.3);
if ~flag
    return
end

%% Backup frequency

[flag, Backup] = saisie_backupFrequency(nbProfils);
if ~flag
    return
end

%% Mosaic interpolation window

[flag, window] = question_MosaicInterpolationWindow(window);
if ~flag
    return
end

%% SpecularInterpolation

[flag, SpecularInterpolation] = question_SpecularInterpolation(I0);
if ~flag
    return
end

%% Initialisations

E0 = cl_ermapper([]);
switch choixLayer
    case 1 % Reflectivity
        NomGenerique = 'Mosaic';
        DataType = ALL.identSonarDataType('Reflectivity'); % TODO : c'est pas top �a !
    case 2 % Bathymetry
        NomGenerique = 'DTM';
        DataType = ALL.identSonarDataType('Depth'); % TODO : c'est pas top �a !
    case 3 % Sidescan
        NomGenerique = 'Sidescan';
        DataType = ALL.identSonarDataType('Reflectivity'); % TODO : c'est pas top �a !
end


%% Si Mosaique de Depth, on propose de faire la correction de mar�e

isBathy = strcmp(NomGenerique, 'DTM');
[flag, TideFile] = inputTideFile(isBathy, nomDirImport);
if ~flag
    return
end

%% Compensation statistique

[flag, CorFile, ModeDependant, UseModel, PreserveMeanValueIfModel] = QuestionCompensationCurve(nomDirImport);
if ~flag
    return
end

%% Noms des fichiers de sauvegarde des mosaiques

if isempty(nomDirExport)
    nomDirExport = my_tempdir;
end
[nomFicErMapperLayer, flag] = initNomFicErs(E0, NomGenerique, cl_image.indGeometryType('LatLong'), DataType, nomDirExport, 'resol', resol);
if ~flag
    return
end
[nomDirExport, TitreDepth] = fileparts(nomFicErMapperLayer);

DataType = ALL.identSonarDataType('BeamDepressionAngle');
[nomFicErMapperAngle, flag] = initNomFicErs(E0, TitreDepth, cl_image.indGeometryType('LatLong'), ...
    DataType, nomDirExport, 'confirm', (get_LevelQuestion >= 3));
if ~flag
    return
end
[~, TitreAngle] = fileparts(nomFicErMapperAngle);

%% R�cup�ration des mosa�ques existantes

Carto = [];
if exist(nomFicErMapperLayer, 'file') && exist(nomFicErMapperAngle, 'file')
    [flag, rep] = question_CompleteExistingMosaic;
    if ~flag
        return
    end
    if rep == 1
        [flag, Z_Moza_All] = cl_image.import_ermapper(nomFicErMapperLayer);
        if ~flag
            return
        end
        Carto = get(Z_Moza_All, 'Carto');
        
        [flag, A_Moza_All]  = cl_image.import_ermapper(nomFicErMapperAngle);
        if ~flag
            return
        end
    else
        Z_Moza_All = [];
        A_Moza_All = [];
    end
else
    Z_Moza_All = [];
    A_Moza_All = [];
end

%% Correction de TVG, Aire Insonifi�e, Diagrammes de directivit�

if get_LevelQuestion == 3
    switch choixLayer
        case {1; 3}
            str1 = sprintf('Voulez-vous appliquer les corrections suivantes ? :\n- TVG\n- Aire insonifi�e\n- Diagrammes de directivit�');
            str2 = sprintf('Do you want to do apply the here under corrections ? :\n- TVG\n- Insonified area\n- Beam diagrams');
            [rep, flag] = my_questdlg(Lang(str1,str2));
            if ~flag
                return
            end
            NormalisationReflectivity = (rep == 1);
        case 2
            NormalisationReflectivity = 0;
    end
else
    NormalisationReflectivity = 0;
end

%% Gestion du recouvrement entre profils

[flag, CoveringPriority, TetaLimite] = question_LinesOverlapping('QL', 3);
if ~flag
    return
end

%% Constitution de la mosaique / MNT

switch choixLayer
    case 1 % 'Reflectivity'
        listeLayers = 3:7;
        AlongInterpolation = 1;
    case 2 % {'Depth'; 'Bathymetry'}
        listeLayers = 3:7;
        AlongInterpolation = 1;
    case 3 % 'Sidescan'
        listeLayers = 4;
        AlongInterpolation = 2;
end
% profile on
% tic
InstallationParameters = [];
SonarTVG_IfremerAlpha = [];
str1 = 'Mosaique des fichiers Geoacoustics .rdf';
str2 = 'Mosaic of Geoswath files (.rdf)';
hw = create_waitbar(Lang(str1,str2), 'N', nbProfils);
for k=1:nbProfils
    my_waitbar(k, nbProfils, hw)
    
    fprintf('\n--------------------------------------------------------------------------------------\n');
    fprintf('Processing file %d/%d : %s\n', k, nbProfils, liste{k});
    
    a = cl_rdf('nomFic', liste{k});
    
    [flag, c, Carto, InstallationParameters] = read_Image(a, 'SelecImage', listeLayers, ...
        'Carto', Carto, 'InstallationParameters', InstallationParameters, 'TideFile', TideFile);
    if ~flag || isempty(c)
        continue
    end
    
    switch choixLayer
        case 1 % Reflectivity
            Z = c(2);
        case 2
            Z = c(3); % Bathymetry
        case 3 % Sidescan
            Z = c(1);
            % Calcul du layer angle
            c(2) = sonar_lateral_emission(Z); % , 'useRoll', 1);
    end
    
    StatValues = Z.StatValues;
    if isnan(StatValues.Moyenne)
        str1 = sprintf('Le layer "%s" du fichier %s est vide.', LayerName, liste{k});
        str2 = sprintf('The layer "%s" of file %s is empty.',   LayerName, liste{k});
        my_warndlg(Lang(str1,str2), 0);
        continue
    end
    
    nbRows = Z.nbRows;
    if nbRows <= 1
        Message = sprintf('The file %s is discarded because it has %d row.', liste{k}, nbRows);
        my_warndlg(Message, 0);
        continue
    end
    
    DT = cl_image.indDataType('AcrossDist');
    indLayer = findIndLayerSonar(c, 1, 'DataType', DT, 'WithCurrentImage', 1);
    AcrossDistance = c(indLayer);
    if ~isempty(AcrossDistance)
        StatValues = AcrossDistance.StatValues;
        if isnan(StatValues.Moyenne)
            str1 = sprintf('Le layer "AcrossDist" du fichier %s est vide.', liste{k});
            str2 = sprintf('The layer "AcrossDist" of file %s is empty.',   liste{k});
            my_warndlg(Lang(str1,str2), 0);
            continue
        end
    end
    
    DT = cl_image.indDataType('AlongDist');
    indLayer = findIndLayerSonar(c, 1, 'DataType', DT, 'WithCurrentImage', 1);
    AlongDistance = c(indLayer);
    
    DT = cl_image.indDataType('TxAngle');
    indLayer = findIndLayerSonar(c, 1, 'DataType', DT, 'WithCurrentImage', 1);
    if isempty(indLayer)
        DT = cl_image.indDataType('BeamPointingAngle');
        indLayer = findIndLayerSonar(c, 1, 'DataType', DT, 'WithCurrentImage', 1);
    end
    A = c(indLayer);
    
    Heading       = get(Z, 'Heading');
    FishLatitude  = get(Z, 'FishLatitude');
    FishLongitude = get(Z, 'FishLongitude');
    
    %% Correction de TVG,  diagrammes de directivity et aire insonifi�e
    
    if NormalisationReflectivity
        %         DT = cl_image.indDataType('Reflectivity');
        %         indLayerReflectivity = findIndLayerSonar(c, 1, 'DataType', DT, 'WithCurrentImage', 1);
        %         indLayerReflectivity = 2; % C'est toujours 2 mais j'ai pr�par� ce qu'il faut au cas o� ...
        %         [flag, ReflecComp] = SonarBestBSIfremer(c, indLayerReflectivity);
        % TODO : attention, traitement TVG comment� dans SonarBestBSIfremer
        [flag, ReflecComp, SonarTVG_IfremerAlpha] = SonarBestBSIfremer([Z c], 1, 'SonarTVG_IfremerAlpha', SonarTVG_IfremerAlpha);
        if flag
            Z = ReflecComp;
            clear ReflecComp
        end
    end
    
    for kf=1:length(CorFile)
        CourbeConditionnelle = load_courbesStats(CorFile{kf});
        bilan = CourbeConditionnelle.bilan;
        indexLayersCompensation = [];
        for k2=1:length(bilan{1})
            strDataType = bilan{1}(k2).DataTypeConditions;
            DataType = cl_image.indDataType(strDataType);
            ind = findIndLayerSonar(c, 1, 'DataType', DataType, 'WithCurrentImage', 1);
            if isempty(ind)
                ind = findIndLayerSonar(c, 1, 'strDataType', 'TxAngle', 'WithCurrentImage', 1, 'AtLeastOneLayer');
                if isempty(ind)
                    continue
                end
                bilan{1}(k2).DataTypeConditions = {'TxAngle'};
            end
            indexLayersCompensation(end+1) = ind; %#ok<AGROW>
        end
        indexLayersCompensation = unique(indexLayersCompensation);
        
        [Z, flag] = compensationCourbesStats(Z, c(indexLayersCompensation), bilan, 'ModeDependant', ModeDependant);
        if ~flag
            return
        end
%         Z = optimiseMemory(Z);
        c(1) = Z;
    end
    
    %% Recherche des pings dans la zone d'�tude
    
    if isempty(XLim) || isempty(YLim)
        suby = 1:Z.nbRows;
    else
        SonarFishLatitude  = get(Z, 'SonarFishLatitude');
        SonarFishLongitude = get(Z, 'SonarFishLongitude');
        suby = find((SonarFishLongitude(:,1) >= XLim(1)) & ...
            (SonarFishLongitude(:,1) <= XLim(2)) & ...
            (SonarFishLatitude(:,1) >= YLim(1)) & ...
            (SonarFishLatitude(:,1) <= YLim(2)));
        clear SonarFishLatitude SonarFishLongitude
        if isempty(suby)
            clear Z AcrossDistance AlongDistance A c
            continue
        end
    end
    
    %% Calcul des coordonn�es g�ographiques
    
    if choixLayer == 3
        c_AcrossDist = sonarLateral_PingRange2PingAcrossDist(c, 'resolutionX', resol/2, 'createNbp', 0);
        [flag, LatLon] = sonarCalculCoordGeo(c_AcrossDist, 1);
        Z = c_AcrossDist(1);
        A = c_AcrossDist(2);
    else
        [flag, LatLon] = sonar_calcul_coordGeo_BathyFais(Z, AcrossDistance(1), AlongDistance(1), ...
            Heading, FishLatitude, FishLongitude, 'suby', suby);
    end
    if ~flag
        return
    end
%     LatLon = optimiseMemory(LatLon);
    
    %% Interpolation
    
    %     WRDF = [5 21];
    %     Z = WinFillNaN(Z, 'window', WRDF, 'NoStats');
    %     A = WinFillNaN(A, 'window', WRDF, 'NoStats');
    %     LatLon(1) = WinFillNaN(LatLon(1), 'window', WRDF, 'NoStats');
    %     LatLon(2) = WinFillNaN(LatLon(2), 'window', WRDF, 'NoStats');
    
    %% Tentative d'interpolation si mosa�que de la r�flectivit�
    
    if strcmp(LayerName, 'Reflectivity')
        [flag, A, LatLon] = sonar_GeoswathInterpAngles(A, LatLon);
        if ~flag
            return
        end
        [flag, Z] = sonar_GeoswathFilterReflec(Z);
        if ~flag
            return
        end
    end
    
    %% Mosaique de la ligne
    
    [flag, Z_Mosa, A_Mosa] = sonar_mosaique2(Z, A, LatLon(1), LatLon(2), resol, ...
        'AcrossInterpolation', 2, 'AlongInterpolation',  AlongInterpolation, ...
        'SpecularInterpolation', SpecularInterpolation);
    if ~flag
        return
    end
    clear LatLon Z A
    
    %% Interpolation
    
    if ~isequal(window, [1 1])
        Z_Mosa = WinFillNaN(Z_Mosa, 'window', window, 'NoStats');
        A_Mosa = WinFillNaN(A_Mosa, 'window', window, 'NoStats');
    end
    
    %% Assemblage de la mosaique de la ligne avec les pr�c�dentes
    
    [Z_Moza_All, A_Moza_All] = mosaique([Z_Moza_All Z_Mosa], 'Angle', [A_Moza_All A_Mosa], ...
        'FistImageIsCollector', 1, 'CoveringPriority', CoveringPriority, 'TetaLimite', TetaLimite);
    Z_Moza_All.Name = TitreDepth;
    A_Moza_All.Name = TitreAngle;
    clear Mosa
    Mosa(1) = Z_Moza_All;
    Mosa(2) = A_Moza_All;
    
    clear c
    
    %% Sauvegarde des mosaiques dans des fichiers ErMapper
    
    if (mod(k,Backup) == 0) || (k == nbProfils)
        Z_Moza_All = majCoordonnees(Z_Moza_All);
        A_Moza_All = majCoordonnees(A_Moza_All);
        export_ermapper(Z_Moza_All, nomFicErMapperLayer);
        export_ermapper(A_Moza_All, nomFicErMapperAngle);
    end
    
    Z_Moza_All = optimiseMemory(Z_Moza_All);
    A_Moza_All = optimiseMemory(A_Moza_All);
    
    %% Suppression des fichier memmapfile
    
    deleteFileOnListe(cl_memmapfile.empty, gcbf)
end
my_close(hw, 'MsgEnd')
flag = 1;
% toc
% profile report

% % InitialFileName = sprintf('Not saved for the moment. Internal code for mosaic asembling : %f', rand(1));
% for k=1:length(Mosa)
%     Mosa(k) = majCoordonnees(Mosa(k));
% %     Mosa(k) = set(Mosa(k), 'InitialFileName', InitialFileName);
% end

if nargout == 0
    SonarScope(Mosa)
end
