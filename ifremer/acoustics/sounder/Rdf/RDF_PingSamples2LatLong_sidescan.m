% Realisation de la mosaique de donnees en g�om�trie PingSamples d'une campagne
%
% Syntax
%   [flag, Mosa, nomDirImport, nomDirExport] = RDF_PingSamples2LatLong_sidescan(...)
%
% Name-Value Pair Arguments
%   nomDirImport : Nom du r�pertoire des donnees
%   nomDirExport : Nom du r�pertoire de la mosa�que
%   resol        : Pas de la grille en m�tres
%   Backup       : P�riode de sauvegarde des fichiers de sortie
%
% Output Arguments
%   []     : Auto-plot activation
%   flag   : Description du parametre (unite).
%   a      : Instances de I0 contenant la mosaique et la mosaique angulaire
%   nomDirImport : Nom du r�pertoire des donnees
%   nomDirExport : Nom du r�pertoire de la mosa�que
%
% Examples
%   RDF_PingSamples2LatLong_sidescan
%   flag, a] = RDF_PingSamples2LatLong_sidescan('NomDir', '/home1/jackson/FEMME2005/Donnees', 'Resol', 10);
%   SonarScope(a)
%   [flag, a] = RDF_PingSamples2LatLong_sidescan('NomDir', 'D:\NIWA_DATA\TAN0105', 'Resol', 10, 'Backup', 5);
%   SonarScope(a)
%
% See also Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function [flag, Mosa, nomDirImport, nomDirExport] = RDF_PingSamples2LatLong_sidescan(varargin)

% [varargin, XLim]        = getPropertyValue(varargin, 'XLim',         []);
% [varargin, YLim]        = getPropertyValue(varargin, 'YLim',         []);
[varargin, resol]         = getPropertyValue(varargin, 'Resol',        []);
% [varargin, Backup]      = getPropertyValue(varargin, 'Backup',       []);
[varargin, nomDirImport]  = getPropertyValue(varargin, 'nomDirImport', []);
[varargin, nomDirExport]  = getPropertyValue(varargin, 'nomDirExport', []); %#ok<ASGLU>
% [varargin, window]      = getPropertyValue(varargin, 'window',       []);
% [varargin, MasqueActif] = getPropertyValue(varargin, 'MasqueActif',  []);

Mosa = [];
I0 = cl_image_I0;
E0 = cl_ermapper([]);

QL = get_LevelQuestion;

if isempty(nomDirImport)
    nomDirImport = pwd;
end

%% Recherche des fichiers .rdf

[flag, liste] = uiSelectFiles('ExtensionFiles', '.rdf', 'RepDefaut', nomDirImport);
if ~flag || isempty(liste)
    return
end
nbProfils = length(liste);
nomDirImport = fileparts(liste{1});

%% Saisie de la r�solution

[flag, resol] = get_gridSize('Value', -resol, 'Initial', 10);
if ~flag
    return
end

%% Backup frequency

[flag, Backup] = saisie_backupFrequency(nbProfils);
if ~flag
    return
end

%% Across distance interpolation method

[flag, AcrossInterpolation] = question_AcrossDistanceInterpolation('QL', 3);
if ~flag
    return
end

%% Mosaic interpolation window

%{
[flag, window] = question_MosaicInterpolationWindow(window);
if ~flag
return
end
%}

%% Noms du fichier de sauvegarde de la mosaique de r�flectivit�

DataType = cl_image.indDataType('Reflectivity');
[nomFicErMapperLayer, flag] = initNomFicErs(E0, 'Mosaic', cl_image.indGeometryType('LatLong'), DataType, nomDirExport, 'resol', resol);
if ~flag
    return
end
[nomDirExport, TitreDepth] = fileparts(nomFicErMapperLayer);

%% Noms du fichier de sauvegarde de la mosaique des angles d'�mission

DataType = cl_image.indDataType('TxAngle');
[nomFicErMapperAngle, flag] = initNomFicErs(E0, TitreDepth, cl_image.indGeometryType('LatLong'), DataType, nomDirExport, 'confirm', (QL >= 3));
if ~flag
    %return
    angleLayer = false;
else
    [pppp, TitreAngle] = fileparts(nomFicErMapperAngle);%#ok
    angleLayer = true;
end

%% Gestion des recouvrements

if ~angleLayer
    str1 = 'Priorit� donn�e pour le recouvrement de profil : ';
    str2 = 'Priority to be used for recovering profiles : ';
    [CoveringPriority, flag] = my_listdlg(Lang(str1,str2), ...
        {Lang('Remplacement', 'Overwrite'); ...
        Lang('Valeur Max', 'Max');  ...
        Lang('Valeur Min', 'Min');...
        Lang('Valeur moyenne', 'Mean');...
        Lang('Valeur mediane', 'Median');...
        Lang('Somme', 'Sum');...
        Lang('Mixage', 'Blend')});
    if ~flag
        return
    end
else
    [flag, CoveringPriority, TetaLimite] = question_LinesOverlapping('QL', 3);
    if ~flag
        return
    end
end

%% R�cup�ration des mosaiques existantes

% Carto = [];
if exist(nomFicErMapperLayer, 'file') && exist(nomFicErMapperAngle, 'file')
    [flag, rep] = question_CompleteExistingMosaic;
    if ~flag
        return
    end
    if rep == 1
        [flag, Z_Mosa_All] = cl_image.import_ermapper(nomFicErMapperLayer);
        if ~flag
            return
        end
%         Carto = get(Z_Mosa_All, 'Carto');
        
        [flag, A_Mosa_All]  = cl_image.import_ermapper(nomFicErMapperAngle);
        if ~flag
            return
        end
    else
        Z_Mosa_All = [];
        A_Mosa_All = [];
    end
else
    Z_Mosa_All = [];
    A_Mosa_All = [];
end

%% Si Mosaique de Reflectivite, on propose de faire une compensation statistique ou une calibration

% [flag, TypeCalibration, CorFile, bilan, DataTypeConditions, alpha, ModelTxDiag, CourbeBS] = params_SonarCompensation(nomDirExport);
[flag, TypeCalibration, CorFile, bilan] = params_SonarCompensation(nomDirExport);
if ~flag
    return
end

%% Traitement des fichiers

% profile on
% tic

% Selection     = [];
% SelectionMode = 'Single';
% subSidescan = [];

for k1=1:nbProfils
    fprintf('Processing file %d/%d : %s\n', k1, nbProfils, liste{k1});
    
    % --------------------------------------------------------------------------------------
    % Lecture des layers "Range", "AcrossDistance", "AlongDistance" et "BeamDepressionAngle"
    
    a = cl_rdf('nomFic', liste{k1});
    
    [flag, c, Carto, InstallationParameters] = read_Image(a, 'SelecImage', 4);
    if ~flag || isempty(c)
        continue
    end
    
    nbRows = c(1).nbRows;
    if nbRows <= 1
        Message = sprintf('The file %s is discarded because it has %d row.', liste{k1}, nbRows);
        my_warndlg(Message, 0);
        continue
    end
    
    FishLatitude   = get(c(1), 'FishLatitude');
    FishLongitude  = get(c(1), 'FishLongitude');
    Heading        = get(c(1), 'Heading');
    
    resolutionX = get(c(1), 'SonarResolutionX');
    
    Reflectivity_PingAcrossDist = sonarLateral_PingRange2PingAcrossDist(c(1), 'resolutionX', resolutionX, 'createNbp', 0);
    
    [flag, LatLon] = sonar_calcul_coordGeo_SonarX(Reflectivity_PingAcrossDist, Heading, ...
        FishLatitude, FishLongitude);
    if ~flag
        continue
    end
    clear c
    
    % ---------------------------------------------------------------------
    % Compensation de l'image si la courbe a �t� calcul�e dans la g�om�trie
    % PingSamples
    
    TxAngle_PingAcrossDist = sonar_lateral_emission(Reflectivity_PingAcrossDist, 'useRoll', 0);
    
    GT_Image = Reflectivity_PingAcrossDist.GeometryType;
    
    if TypeCalibration == 1
        GT_Bilan = cl_image.indGeometryType(bilan{1}(1).GeometryType);
        if ~isempty(CorFile) && (GT_Bilan == GT_Image)
            Reflectivity_PingAcrossDist = compensationCourbesStats(Reflectivity_PingAcrossDist, TxAngle_PingAcrossDist, bilan);
        end
    end
    
    nbRows = Reflectivity_PingAcrossDist.nbRows;
    nbCol = Reflectivity_PingAcrossDist.nbColumns;
    pas = floor(nbRows / ((nbRows * nbCol * 4) / 1e7)); % SizePhysTotalMemory
    
    NBlocks = ceil(nbRows/pas);
    pas = ceil(nbRows/NBlocks);
    
    for k2=1:pas:nbRows
        subl = max(1,k2-5):min(k2-1+5+pas, nbRows);
        fprintf('Processing pings [%d:%d] Fin=%d\n', subl(1), subl(end), nbRows);
        
        [flag, Z_Mosa, A_Mosa] = sonar_mosaique2(Reflectivity_PingAcrossDist, TxAngle_PingAcrossDist, LatLon(1), LatLon(2), resol, ...
            'AcrossInterpolation', AcrossInterpolation, 'AlongInterpolation',  2, ...
            'suby', subl);
        if ~flag
            return
        end
        
        Z_Mosa.Writable = false;
        A_Mosa.Writable = false;
        
        if isempty(Z_Mosa_All)
            Z_Mosa_All = Z_Mosa;
            A_Mosa_All = A_Mosa;
        else
            if ~angleLayer
                Z_Mosa_All = mosaique([Z_Mosa_All Z_Mosa], 'FistImageIsCollector', 1, ...
                    'CoveringPriority', CoveringPriority, 'TetaLimite', TetaLimite);
            else
                [Z_Mosa_All, A_Mosa_All] = mosaique([Z_Mosa_All Z_Mosa], 'Angle', [A_Mosa_All A_Mosa], 'FistImageIsCollector', 1, ...
                    'CoveringPriority', CoveringPriority, 'TetaLimite', TetaLimite);
            end
            clear Z_Mosa A_Mosa
        end
        Z_Mosa_All.Name = extract_ImageName(Z_Mosa_All, 'Name', TitreDepth);
        A_Mosa_All.Name = extract_ImageName(A_Mosa_All, 'Name', TitreDepth);
        Z_Mosa_All = update_Name(Z_Mosa_All);
        A_Mosa_All = update_Name(A_Mosa_All);

        Z_Mosa_All.Writable = false;
        A_Mosa_All.Writable = false;
    end
    clear Reflectivity_PingAcrossDist TxAngle_PingAcrossDist LatLon
    
    %% Sauvegarde des mosaiques dans des fichiers ErMapper
    
    if (mod(k1,Backup) == 0) || (k1 == nbProfils)
        Z_Mosa_All = majCoordonnees(Z_Mosa_All);
        export_ermapper(Z_Mosa_All, nomFicErMapperLayer);
        if angleLayer
            A_Mosa_All = majCoordonnees(A_Mosa_All);
            export_ermapper(A_Mosa_All, nomFicErMapperAngle);
        end
        
        % D�placement Mikael
        deleteFileOnListe(cl_memmapfile.empty, gcbf)
        % D�placement Mikael
    end
end
flag = 1;

clear Mosa
Mosa(1) = Z_Mosa_All;
Mosa(2) = A_Mosa_All;

% toc
% profile report
