function varargout = RDF_PingSpecularInterpolation(varargin)

for k=1:nargin
    Ligne_x = varargin{k};
    x1 = 1:length(Ligne_x);
    x2 = 1:0.1:length(Ligne_x);
    Ligne_x2  = interp1(x1, Ligne_x, x2); %, 'nearest');
    
    if k ~= 1
        Wc = 0.05;
        Ordre = 2;
        Ligne_x2 = filtrageButterSignal(Ligne_x2,  Wc, Ordre);
    end
    varargout{k} = Ligne_x2; %#ok<AGROW>
end
