function Image = RDF_formatImage(X, SampleNumber)

SampleNumber = double(SampleNumber);
nbPings = length(SampleNumber);
nbCol = max(SampleNumber);
Image = NaN(nbPings, nbCol, 'single');
k = 0;
for iPing=1:nbPings
    n = SampleNumber(iPing);
    Image(iPing,1:n) = X(k+1:k+n);
    k = k + n;
end
% SonarScope(Image(1:2:end,:))