function [flag, Geoswath, repImport] = RDF_installationParameters(varargin)

[varargin, repImport] = getPropertyValue(varargin, 'repImport', []); %#ok<ASGLU>

if ~isempty(repImport)
    nomFicXml = fullfile(repImport, 'GeoswathParameters_Directory.xml');
    flag = exist(nomFicXml, 'file');
    if flag
        try
            Geoswath = xml_read(nomFicXml);
            % TODO : mettre message disant qu'on lit automatiquement le fichier
            return
        catch %#ok<CTCH>
        end
    end
end

[flag, Geoswath] = RDF_DefineInstallationParameters(repImport, nomFicXml);
