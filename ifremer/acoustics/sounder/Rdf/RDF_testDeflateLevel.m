% nomFic = 'G:\Haliotis\STEANNE2019\GEOSWATH\SonarScope\ESSTECH19017.nc';
% RDF_testDeflateLevel(nomFic)

function RDF_testDeflateLevel(listFileNames)

if ischar(listFileNames)
    listFileNames = {listFileNames};
end

for k=1:length(listFileNames)
    RDF_testDeflateLevel_unitaire(listFileNames{k})
end

figure('Name', 'Synthesis');
h(1) = subplot(3,2,1); grid on; hold on;
xlabel('deflateLevel'); ylabel('Time(s)'); title('Time : Write (s)', 'Interpreter', 'none')
h(2) = subplot(3,2,3); grid on; hold on;
xlabel('deflateLevel'); ylabel('Time(s)'); title('Time : Read (s)', 'Interpreter', 'none')
h(3) = subplot(3,2,5); grid on; hold on;
xlabel('deflateLevel'); ylabel('File size (Mo)'); title('NetcdfSize (Mb)', 'Interpreter', 'none')
hLegend = subplot(3,2,2:2:6); grid on; hold on;
set(hLegend, 'Visible', 'Off');

for k=1:length(listFileNames)
    [~, nomFicSeul] = fileparts(listFileNames{k});
    nomFicTable = fullfile(my_tempdir, ['Compression-' nomFicSeul '.txt']);
    T = readtable(nomFicTable);
    
    fprintf('\n------------------------------------------\n%s\n', nomFicSeul)
    T %#ok<NOPRT>
    
    plot(h(1), 0:9, T.TimeWrite);  drawnow;
    plot(h(2), 0:9, T.TimeRead);   drawnow;
    plot(h(3), 0:9, T.NetcdfSize); drawnow;
    
    plot(hLegend, 0:9, NaN*T.TimeWrite, '*');
    Legend{k} = nomFicSeul; %#ok<AGROW>
end
linkaxes(h, 'x'); axis tight; drawnow;
legend(hLegend, Legend, 'Interpreter', 'None'); drawnow

 
figure('Name', 'Synthesis');
h(1) = subplot(3,2,1); grid on; hold on;
xlabel('deflateLevel'); ylabel('TimeWriteRatio'); title('Time : Write ratio', 'Interpreter', 'none')
h(2) = subplot(3,2,3); grid on; hold on;
xlabel('deflateLevel'); ylabel('TimeReadRatio'); title('Time : Read ratio', 'Interpreter', 'none')
h(3) = subplot(3,2,5); grid on; hold on;
xlabel('deflateLevel'); ylabel('NetcdfSizeRatio'); title('NetcdfSize ratio', 'Interpreter', 'none')
hLegend = subplot(3,2,2:2:6); grid on; hold on;
set(hLegend, 'Visible', 'Off');

for k=1:length(listFileNames)
    [~, nomFicSeul] = fileparts(listFileNames{k});
    nomFicTable = fullfile(my_tempdir, ['Compression-' nomFicSeul '.txt']);
    T = readtable(nomFicTable);
    
    fprintf('\n------------------------------------------\n%s\n', nomFicSeul)
    T %#ok<NOPRT>
    
    plot(h(1), 0:9, T.TimeWriteRatio);  drawnow;
    plot(h(2), 0:9, T.TimeReadRatio);   drawnow;
    plot(h(3), 0:9, T.NetcdfSizeRatio); drawnow;
    
    plot(hLegend, 0:9, NaN*T.TimeWrite, '*');
    Legend{k} = nomFicSeul;
end
linkaxes(h, 'x'); axis tight; drawnow;
legend(hLegend, Legend, 'Interpreter', 'None'); drawnow


function RDF_testDeflateLevel_unitaire(nomFic)

[nomDir, nomFicSeul] = fileparts(nomFic);
nomFicNetcdf = fullfile(nomDir, 'SonarScope', [nomFicSeul '.nc']);

RDFSize = sizeFic(nomFic) * 1e-6;

figure('Name', nomFicSeul);
h(1) = subplot(2,1,1); grid on; hold on;
xlabel('deflateLevel'); ylabel('Time(s)'); title('Time (s) : Write (red), Read (blue)', 'Interpreter', 'none')
h(2) = subplot(2,1,2); grid on; hold on;
xlabel('deflateLevel'); ylabel('File size (Mo)'); title('NetcdfSize (Mb)', 'Interpreter', 'none')

for k=1:10
    cl_rdf('nomFic', nomFic); % Pour s'assurer que le cache XML-Bin est d�j� fait
    
    deflateLevel = k-1;
    
    tic
    RDF_Cache2Netcdf(nomFic, 'deflateLevel', deflateLevel);
    TWrite(k,1) = toc; %#ok<AGROW>
    plot(h(1), deflateLevel, TWrite(k), '*r'); drawnow;
    
    NetcdfSize(k,1) = sizeFic(nomFicNetcdf) * 1e-6; %#ok<AGROW>
    plot(h(2), deflateLevel, NetcdfSize(k), '*b'); drawnow;
    
    tic
    testLectureCacheNetcdf(nomFic)
    TRead(k,1) = toc; %#ok<AGROW>
    plot(h(1), deflateLevel, TRead(k), '*b'); drawnow;
    
    saveTable(nomFicSeul, TWrite, TRead, NetcdfSize, RDFSize)
end


function testLectureCacheNetcdf(nomFic)

[flag, Data2] = SScCacheNetcdfUtils.readWholeData(nomFic, 'Ssc_Attitude');          %#ok<ASGLU>
[flag, Data2] = SScCacheNetcdfUtils.readWholeData(nomFic, 'Ssc_Navigation');        %#ok<ASGLU>
[flag, Data2] = SScCacheNetcdfUtils.readWholeData(nomFic, 'Ssc_Heading');           %#ok<ASGLU>
[flag, Data2] = SScCacheNetcdfUtils.readWholeData(nomFic, 'Ssc_Aux1String');        %#ok<ASGLU>
[flag, Data2] = SScCacheNetcdfUtils.readWholeData(nomFic, 'Ssc_FileIndex');         %#ok<ASGLU>
[flag, Data2] = SScCacheNetcdfUtils.readWholeData(nomFic, 'Ssc_Header');            %#ok<ASGLU>
[flag, Data2] = SScCacheNetcdfUtils.readWholeData(nomFic, 'Ssc_HeadingString');     %#ok<ASGLU>
[flag, Data2] = SScCacheNetcdfUtils.readWholeData(nomFic, 'Ssc_MiniSVSString');     %#ok<ASGLU>
[flag, Data2] = SScCacheNetcdfUtils.readWholeData(nomFic, 'Ssc_MiniSVS');           %#ok<ASGLU>
[flag, Data2] = SScCacheNetcdfUtils.readWholeData(nomFic, 'Ssc_NavigationString');  %#ok<ASGLU>
[flag, Data2] = SScCacheNetcdfUtils.readWholeData(nomFic, 'Ssc_Height');            %#ok<ASGLU>
[flag, Data2] = SScCacheNetcdfUtils.readWholeData(nomFic, 'Ssc_Aux2String');        %#ok<ASGLU>
[flag, Data2] = SScCacheNetcdfUtils.readWholeData(nomFic, 'Ssc_FileHeader');        %#ok<ASGLU>
[flag, Data2] = SScCacheNetcdfUtils.readWholeData(nomFic, 'Ssc_EchoSounderString'); %#ok<ASGLU>
[flag, Data2] = SScCacheNetcdfUtils.readWholeData(nomFic, 'Ssc_EchoSounder');       %#ok<ASGLU>
[flag, Data2] = SScCacheNetcdfUtils.readWholeData(nomFic, 'Ssc_Sample', 'Memmapfile', -1); %#ok<ASGLU>


function saveTable(nomFicSeul, TimeWrite, TimeRead, NetcdfSize, RDFSize)

nomFicTable = fullfile(my_tempdir, ['Compression-' nomFicSeul '.txt']);

TimeWriteRatio  = round(TimeWrite / TimeWrite(1),   2);
TimeReadRatio   = round(TimeRead  / TimeRead(1),    2);
NetcdfSizeRatio = round(NetcdfSize / NetcdfSize(1), 2);
RDFSizeRatio    = round(NetcdfSize / RDFSize,   2);

Compression = (1:length(TimeWrite))' - 1;
TimeWrite   = round(TimeWrite, 2);
TimeRead    = round(TimeRead, 2);
NetcdfSize  = round(NetcdfSize, 2);

T = table(Compression, TimeWrite, TimeRead, NetcdfSize, TimeWriteRatio, TimeReadRatio, NetcdfSizeRatio, RDFSizeRatio);
T.Properties.VariableUnits = {'','s','s','Mb', '', '', '', ''};
T %#ok<NOPRT>
writetable(T, nomFicTable)
