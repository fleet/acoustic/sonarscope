function HorizontalDilutionOfPrecision = decodeHDOP_OLD(GPSQualityIndicator, HorizontalDilutionOfPrecision)

% 1 - (SPS or standard GPS) => 1000
sub = (GPSQualityIndicator == 1);
GPSQualityIndicator(sub) = 1000;
% 2 - (differential GPS) => 100
sub = (GPSQualityIndicator == 2);
GPSQualityIndicator(sub) = 100;
% 3 - (PPS or precise GPS) => 200, but 10 if GGA is treated as RTK. (See Note 2)
sub = (GPSQualityIndicator == 3);
GPSQualityIndicator(sub) = 200;
% 4 - (kinematic GPS with fixed integers) => 10
sub = (GPSQualityIndicator == 4);
GPSQualityIndicator(sub) = 10;
% 5 - (kinematic GPS with floating integers) => 50
sub = (GPSQualityIndicator == 5);
GPSQualityIndicator(sub) = 50;
% 6 - (estimated or dead reckoning mode) => 1000
sub = (GPSQualityIndicator == 6);
GPSQualityIndicator(sub) = 1000;
% 7 - (manual input mode) => 1000
sub = (GPSQualityIndicator == 7);
GPSQualityIndicator(sub) = 1000;
% 8 - (test mode) => 1000, but 10 if GGA is treated as RTK. (See Note 2)
sub = (GPSQualityIndicator == 8);
GPSQualityIndicator(sub) = 1000;
% The �Measure of position fix quality� field will be set to 65534 (largest valid
% number) if the indicator is zero (non-valid position).
sub = (GPSQualityIndicator == 0);
GPSQualityIndicator(sub) = 65534;

% TODO : reste � traite la note 2 pg 13

HorizontalDilutionOfPrecision = GPSQualityIndicator .* HorizontalDilutionOfPrecision;