% Simulateur interferometre
%{
Phrbr = [];
[XMes, ZMes] = filtre_interfero(Phrbr)
figure; plot(XMes, -ZMes);
%}

function [XMes, ZMes] = filtre_interfero(Phrbr)
% Phrbr = simul_phase;
% figure; plot(Phrbr); grid on;

fk = 200;               % freq en kHz
fr = fk*1000;           % freq en Hz
c = 1500;               % celerite
lamda = c/fr;           % longueur d'onde
k = 2*pi/lamda;         % nombre d'onde
alamda = 5;             % espacement / longueur d'onde
a = alamda*lamda;       %  espacement physique
ka = k*a;               % produit ka;

% Tms = 0.2;              % duree signal en ms
% T = Tms/1000;           % duree signal en s
Fechk = 10;             % freq echantillonnage
Fech = Fechk*1000;      % freq ech en Hz
Tech = 1/Fech;          % Periode echantillonnage

H = 20;                 % altitude interfero

Tetamaxd = 80;          % angle incidence max en deg
Tetamax = Tetamaxd*pi/180;  % angle incidence max en rad
Rmax = H/cos(Tetamax);  % Dist oblique max
Tmax = 2*Rmax/c;        % Temps max reception
Tmin = 2*H/c;           % Temps min reception
Tr = Tmin:Tech:Tmax;    % instants reception
NbT = length(Tr);       % Nbre d'echantillons temporels
Rr = c*Tr/2;            % distances obliques �chantillons

%{
H2 = H * H;
Xr = sqrt(Rr.^2-H2);    % distances horizontales �chantillons
Thetar = atan(Xr./H);   % angles incidence arriv�e signaux �chantillons
%}

Tiltd = 10;             % inclinaison interfero / verticale en deg
Tilt = Tiltd*pi/180;    % le m�me en rad

%{
Angr = pi/2-Tilt-Thetar;     % angle interfero
Phr = ka*sin(Angr);      % Phase en r�ception �chantillons
sinPhr = sin(Phr);      % Sinus de la phase physique
Phrmes = mod(Phr, 2*pi);   % Phase ambigue
figure;
subplot(2,1,1); plot(Tr, Phr*180/pi); xlabel('Time'); title('Exact interfero phase');grid on;
subplot(2,1,2); plot(Tr, Phrmes*180/pi); xlabel('Time'); title('Ambiguous interfero phase');grid on;
%}

%{
EtPhd = 15;                         % Ecart-type phase en deg
EtPh = EtPhd*pi/180;                % Ecart-type phase en rad
BrPh = randn(1, NbT)*EtPh;          % vecteur bruit de phase en rad
BrPh = BrPh.*linspace(1, 2, NbT);   % Variation d'un facteur 2 du bruit de tmin a tmax
Phrbr = Phr+BrPh;                   % Phase bruitee
%}

Phrbrmes = mod(Phrbr, 2*pi);        % Phase bruit�e ambigue

%{
figure;
subplot(2,1,1); plot(Tr, Phrbr*180/pi); xlabel('Time'); title('Exact interfero phase + noise');grid on;
subplot(2,1,2); plot(Tr, Phrbrmes*180/pi); xlabel('Time'); title('Ambiguous interfero phase + noise');
hold on; plot(Tr, Phrmes*180/pi, 'r');grid on;
%}

% Dewrappage de la phase bruitee non filtr�e

%{
PhrbrUW = unwrap(Phrbrmes);         % Dewrappage de la phase bruit�e mesur�e
figure;
subplot(2,1,1); plot(Tr, Phrbr*180/pi); xlabel('Time'); ylabel('Exact interfero phase');
% subplot(2,1,2);
hold on;
plot(Tr, PhrbrUW*180/pi,'r'); xlabel('Time'); ylabel('Unwrapped interfero phase'); grid on
%}

% Resolution de l'ambiguite de determination de phase UW

%{
MoyPhaseFin = mean(PhrbrUW(end-100:end));
Nambig = round(MoyPhaseFin/2/pi);
PhrbrUWAmbRem = PhrbrUW - Nambig*2*pi;
subplot(2,1,2); plot(Tr, Phrbr*180/pi); xlabel('Time'); title('Exact interfero phase');
hold on;
plot(Tr, PhrbrUWAmbRem*180/pi,'r'); xlabel('Time'); title('Unwrapped interfero phase ambiguity removed'); grid on;
%}

% Traitement de la phase bruit�e par filtrage du sin et du cos

% AngEst = asin(PhrbrUW/ka);          % Angle arrivee estim� sur phase dewrapp�e
AngEst = asin(Phrbrmes/ka);          % Angle arrivee estim� sur phase bruit�e

SinAngEst = sin(AngEst);            % Sinus de l'angle estim�
sinDdPh = sin(SinAngEst*ka);        % Sinus de diff de phase estim�e
cosDdPh = cos(SinAngEst*ka);        % Sinus de diff de phase estim�e

[B1, A1] = butter(2, 0.2);            % Filtre - Freq 0.2
sinDdPhFilt1 = filtfilt(B1, A1, sinDdPh);
sinDdPhFilt1(sinDdPhFilt1>=1) = 1;     % ecretage � 1
sinDdPhFilt1(sinDdPhFilt1<=-1) = -1;     % ecretage � -1
cosDdPhFilt1 = filtfilt(B1, A1, cosDdPh);
cosDdPhFilt1(cosDdPhFilt1>=1) = 1;     % ecretage � 1
cosDdPhFilt1(cosDdPhFilt1<=-1) = -1;     % ecretage � -1

[B2, A2] = butter(2, 0.01);            % Filtre - Freq 0.01
sinDdPhFilt2 = filtfilt(B2, A2, sinDdPh);
sinDdPhFilt2(sinDdPhFilt2>=1) = 1;     % ecretage � 1
sinDdPhFilt2(sinDdPhFilt2<=-1) = -1;     % ecretage � -1
cosDdPhFilt2 = filtfilt(B2, A2, cosDdPh);
cosDdPhFilt2(cosDdPhFilt2>=1) = 1;     % ecretage � 1
cosDdPhFilt2(cosDdPhFilt2<=-1) = -1;     % ecretage � -1

alfa = linspace(0, 1, NbT);
sinDdPhFilt3 = (1-alfa).*sinDdPhFilt1 + alfa.*sinDdPhFilt2;
cosDdPhFilt3 = (1-alfa).*cosDdPhFilt1 + alfa.*cosDdPhFilt2;

%{
figure;
% subplot(3,1,1); plot(Tr, sinPhr); xlabel('Time'); title('Exact phase sine');
subplot(4,1,1); plot(Tr, sinDdPh); xlabel('Time'); title('Estimated phase sine');
hold on; plot(Tr, cosDdPh,'r');
subplot(4,1,2); plot(Tr, sinDdPhFilt1); xlabel('Time'); title('Filtered est. phase sine');
hold on; plot(Tr, cosDdPhFilt1,'r');
subplot(4,1,3); plot(Tr, sinDdPhFilt2); xlabel('Time'); title('Filtered est. phase sine');
hold on; plot(Tr, cosDdPhFilt2,'r');
subplot(4,1,4); plot(Tr, sinDdPhFilt3); xlabel('Time'); title('Filtered est. phase sine');
hold on; plot(Tr, cosDdPhFilt3,'r');
%}

DdPhFilt3 = atan2(sinDdPhFilt3, cosDdPhFilt3);
DdPhFiltUW = unwrap(DdPhFilt3);

% Resolution de l'ambiguite de determination de phase UW
MoyPhaseFin = mean(DdPhFiltUW(end-100:end));
Nambig = round(MoyPhaseFin/2/pi);
DdPhFiltUWAmbRem = DdPhFiltUW - Nambig*2*pi;

%{
figure;
plot(Tr, Phrbr*180/pi); xlabel('Time'); title('Exact interfero phase'); grid on
hold on; plot(Tr, DdPhFiltUWAmbRem*180/pi, 'r'); xlabel('Time'); title('Unwrapped filtered interfero phase'); hold on;
%}

DdPhRes = DdPhFiltUWAmbRem;
SinAngMes = DdPhRes/ka;
AngMes = asin(SinAngMes);
TetaMes = pi/2-Tilt-AngMes;
XMes = Rr.* sin(TetaMes);
ZMes = Rr.* cos(TetaMes);

%{
figure; plot(XMes, -ZMes);
ErrBathy = std(ZMes - H)/H
%}



function Phrbr = simul_phase
fk = 200;               % freq en kHz
fr = fk*1000;           % freq en Hz
c = 1500;               % celerite
lamda = c/fr;           % longueur d'onde
k = 2*pi/lamda;         % nombre d'onde
alamda = 5;             % espacement / longueur d'onde
a = alamda*lamda;       %  espacement physique
ka = k*a;               % produit ka;

Fechk = 10;             % freq echantillonnage
Fech = Fechk*1000;      % freq ech en Hz
Tech = 1/Fech;          % Periode echantillonnage

H = 20;                 % altitude interfero
H2 = H*H;

Tetamaxd = 80;          % angle incidence max en deg
Tetamax = Tetamaxd*pi/180;  % angle incidence max en rad
Rmax = H/cos(Tetamax);  % Dist oblique max
Tmax = 2*Rmax/c;        % Temps max reception
Tmin = 2*H/c;           % Temps min reception
Tr = Tmin:Tech:Tmax;    % instants reception
NbT = length(Tr);       % Nbre d'echantillons temporels
Rr = c*Tr/2;            % distances obliques �chantillons

Xr = sqrt(Rr.^2-H2);    % distances horizontales �chantillons
Thetar = atan(Xr./H);   % angles incidence arriv�e signaux �chantillons

Tiltd = 10;             % inclinaison interfero / verticale en deg
Tilt = Tiltd*pi/180;    % le m�me en rad

Angr = pi/2-Tilt-Thetar;     % angle interfero
Phr = ka*sin(Angr);      % Phase en r�ception �chantillons

EtPhd = 15;                         % Ecart-type phase en deg
EtPh = EtPhd*pi/180;                % Ecart-type phase en rad
BrPh = randn(1, NbT)*EtPh;          % vecteur bruit de phase en rad
BrPh = BrPh.*linspace(1, 2, NbT);   % Variation d'un facteur 2 du bruit de tmin a tmax
Phrbr = Phr+BrPh;                   % Phase bruitee
