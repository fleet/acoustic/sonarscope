function [flag, NomSignals] = params_RDF_BackupSignals

NomSignals = [];

%% Select a signal

Option = {};
Option{end+1} = 'Heave';
Option{end+1} = 'Roll';
Option{end+1} = 'Pitch';
Option{end+1} = 'Heading';

% TODO 
% Option{end+1} = 'Latitude';
% Option{end+1} = 'Longitude';

[flag, sub] = question_SelectASignal(Option, 'InitialValue', []);
if ~flag || isempty(sub)
    return
end
NomSignals = Option(sub);
