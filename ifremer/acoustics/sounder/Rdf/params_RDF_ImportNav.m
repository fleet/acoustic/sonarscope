function [flag, nomFicNav, InstallationParameters] = params_RDF_ImportNav(repImport)

InstallationParameters = [];

[flag, nomFicNav] = uiSelectFiles('ExtensionFiles', '.nvi', 'RepDefaut', repImport);
if ~flag
    return
end

[flag, InstallationParameters] = RDF_installationParameters('repImport', repImport);
