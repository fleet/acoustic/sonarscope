function [flag, gridSize, CorFile, ModeDependant, selectionData, Window, MosHeading, skipExistingMosaic, repImport, repExport] ...
    = params_RDF_IndividualMosaics(repImport, repExport)

CorFile       = [];
ModeDependant = [];
selectionData = [];
Window        = [5 5];
MosHeading    = 0;
skipExistingMosaic = [];

%% Pas de la grille

[flag, gridSize] = get_gridSize('value', 0.3);
if ~flag
    return
end

%% Nom du répertoire des mosaiques individuelles

[flag, nomDir] = question_NomDirIndividualMosaics(repExport);
if ~flag
    return
end
repExport = nomDir;

%% Choix du layer

listeData{1} = 'Reflectivity';
listeData{2} = 'Bathymetry';
str1 = 'Choix du layer :';
str2 = 'Select the layer :';
[selectionData, flag] = my_listdlg(Lang(str1,str2), listeData, 'SelectionMode', 'Single');
if ~flag
    return
end

%% Compensation statistique

[flag, CorFile, ModeDependant, UseModel, PreserveMeanValueIfModel] = QuestionCompensationCurve(repImport); %#ok<ASGLU>
if ~flag
    return
end

%% skipExistingMosaic

str1 = 'Ne pas refaire les mosaiques existantes ?';
str2 = 'Skip existing mosaics ?';
[rep, flag] = my_questdlg(Lang(str1,str2), 'Init', 2);
if ~flag
    return
end
skipExistingMosaic = (rep == 1);

