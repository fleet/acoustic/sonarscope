function [flag, NomSignal, InstallationParameters] = params_RDF_NavAndSignal(repImport)

NomSignal              = [];
InstallationParameters = [];

%% Select a signal

Option = {'Time'; 'Height'; 'Heading'; 'Drift'; 'Roll'; 'Pitch'; 'SurfaceSoundSpeed'; 'Heave'; 'PingCounter'; 'SampleFrequency';
    'InterPingDistance'; 'Pitch Mean'; 'Pitch Std'; 'Roll Mean'; 'Roll Std'; 'Heading Mean'; 'Drift Mean'; 'Drift Std';
    'RTK-Type'; 'RTK-Height'; 'RTK-Tide'};
[flag, sub] = question_SelectASignal(Option);
if ~flag
    return
end
NomSignal = Option{sub};

%% Installation parameters

[flag, InstallationParameters] = RDF_installationParameters('repImport', repImport);
