function [flag, nomFicSummary, repExport] = params_RDF_SummaryDatagrams(repExport)

str1 = 'IFREMER - SonarScope - Nom du fichier R�sum�';
str2 = 'IFREMER - SonarScope - Summary file name';
Filtre = fullfile(repExport, 'RDF_SummaryDatagrams.txt');
[flag, nomFicSummary] = my_uiputfile('*.txt', Lang(str1,str2), Filtre);
if ~flag
    return
end
repExport = fileparts(nomFicSummary);
