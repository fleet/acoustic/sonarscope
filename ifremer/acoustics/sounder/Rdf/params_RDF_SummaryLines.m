function [flag, nomFicSummary, listeItems, repExport] = params_RDF_SummaryLines(repExport)

nomFicSummary = [];

R0 = cl_rdf([]);

%% S�lection des items du r�sum�

strSummary = RDF_getListeSummaryLines(R0);
str1 = 'Liste des items du r�sum�.';
str2 = 'List of the items of the summary.';
[listeItems, flag] = my_listdlgMultiple(Lang(str1,str2), strSummary, 'InitialValue', 1:length(strSummary));
if ~flag
    return
end
% strSummary = strSummary(listeItems);
    
%% Nom du fichier de sortie 

str1 = 'IFREMER - SonarScope - Nom du fichier R�sum�';
str2 = 'IFREMER - SonarScope - Summary file name';
Filtre = fullfile(repExport, 'RDF_SummaryLines.csv');
[flag, nomFicSummary] = my_uiputfile('*.txt', Lang(str1,str2), Filtre);
if ~flag
    return
end
repExport = fileparts(nomFicSummary);
