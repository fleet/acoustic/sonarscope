%   nomFic = '???\Sonar\CAM021.rdf';
%   nomFic = '???\Sonar\Cormo001.rdf';
%   nomFic = '???\Sonar\EW150.rdf';
%   flag = rdf2ssc(nomFic)
%
%   a = cl_rdf('nomFic', nomFic);
%-------------------------------------------------------------------------------

function flag = rdf2ssc(nomFic)

global IfrTbxResources %#ok<GVMIS>
global IfrTbx %#ok<GVMIS>
global useCacheNetcdf %#ok<GVMIS>

FormatVersion = 20091109;

% Barre de progression uniquement visible en version de Dev car semble �tre
% � l'origine de probl�me lors du traitement successif de donn�es (27/09/2012).
% if my_isdeployed
    progressBar = 0;
% else
%     progressBar = 1;
% end
% progressBar = 0; % Qt: Untested Windows version 6.2 detected! sur mon PC

flag = checkFileName(nomFic);
if ~flag
    return
end

% SystemName = system_dependent('getos'); %Version de windows
nomExe = 'CONVERT_RDF.exe';

if isdeployed
    if isempty(IfrTbxResources)
        loadVariablesEnv
    end
    nomDir = fileparts(IfrTbxResources);
    nomExe = fullfile(nomDir, 'extern', 'C', nomExe);
else
    nomExe = [IfrTbx '\ifremer\extern\C\CONVERT_exe\Release\CONVERT_RDF.exe'];
end

%% G�n�ration des XML et des binaires

[nomDir, nomFicSeul] = fileparts(nomFic);
nomDirRacine = fullfile(nomDir, 'SonarScope', nomFicSeul);

nomFicNc = [nomDirRacine '.nc'];
if exist(nomFicNc, 'file') % Si la conversion a d�j� �t� faite alors on passe
    nomFicXml = strrep(nomFicNc, '.nc', '');
    if exist(nomFicXml, 'dir') % Suppression automatique du r�pertoire XML-Bin
        SScCacheXMLBinUtils.deleteCachDirectory(nomFic);
    end
    return
end

cmd = sprintf('!"%s" "%d" "%s"', nomExe, progressBar, nomFic);

flag = [];
flag(end+1) = exist(nomDirRacine, 'dir');

flag(end+1) = exist(fullfile(nomDirRacine, 'RDF_FileIndex.xml'), 'file');
flag(end+1) = exist(fullfile(nomDirRacine, 'RDF_FileIndex'), 'dir');

flag(end+1) = exist(fullfile(nomDirRacine, 'Ssc_FileHeader.xml'), 'file');

flag(end+1) = exist(fullfile(nomDirRacine, 'Ssc_Header.xml'), 'file');

flag(end+1) = exist(fullfile(nomDirRacine, 'Ssc_Attitude.xml'), 'file');
flag(end+1) = exist(fullfile(nomDirRacine, 'Ssc_Attitude'), 'dir');

% flag(end+1) = exist(fullfile(nomDirRacine, 'Ssc_EchoSounder.xml'), 'file');
% flag(end+1) = exist(fullfile(nomDirRacine, 'Ssc_EchoSounder'), 'dir');

flag(end+1) = exist(fullfile(nomDirRacine, 'Ssc_MiniSVS.xml'), 'file');
flag(end+1) = exist(fullfile(nomDirRacine, 'Ssc_MiniSVS'), 'dir');

flag(end+1) = exist(fullfile(nomDirRacine, 'Ssc_Navigation.xml'), 'file');
flag(end+1) = exist(fullfile(nomDirRacine, 'Ssc_Navigation'), 'dir');

% flag(end+1) = exist(fullfile(nomDirRacine, 'Ssc_Aux1String.xml'), 'file');
% flag(end+1) = exist(fullfile(nomDirRacine, 'Ssc_Aux1String'), 'dir');

% flag(end+1) = exist(fullfile(nomDirRacine, 'Ssc_Aux2String.xml'), 'file');
% flag(end+1) = exist(fullfile(nomDirRacine, 'Ssc_Aux2String'), 'dir');

% Pas de filtrage sur la date pour l'instant. 
% if all(flag) && checkFileCreation(nomDirRacine, 2009, 11, 01, 00, 30, 0)
%GLUGLUGLU
% flag = 0
if all(flag) && (getFormatVersion(nomDirRacine) == FormatVersion)
    flag = 1;
    return
end

nomFicTmp = [nomDirRacine '_tmp'];
if exist(nomFicTmp, 'dir')
    message_erreur_convert(nomFicTmp, nomFic, nomDirRacine)
    flag = 0;
    return
end

fprintf(1,'%s\n', cmd);
if isdeployed
	[~, msg] = dos(cmd(2:end));
else
	[status, msg] = dos(cmd(2:end)); %#ok<ASGLU>
end

if ~exist(nomDirRacine, 'dir')
    if isempty(msg)
        str1 = sprintf('CONVERT_RDF.exe semble avoir rencontr� un probl�me sur le fichier %s', nomFic);
        str2 = sprintf('CONVERT_RDF.exe failed for file %s', nomFic);
        msg = Lang(str1,str2);
    end
    my_warndlg(msg, 0);
    flag = 0;
    return
end

%% Datagrammes FileIndex

nomFicXml = fullfile(nomDirRacine, 'RDF_FileIndex.xml');
if exist(nomFicXml, 'file')
    flag = rdf2ssc_FileIndex(nomDirRacine);
    if ~flag
        return
    end
else
    str1 = sprintf('CONVERT_RDF.exe semble avoir rencontr� un probl�me sur le fichier %s', nomFic);
    str2 = sprintf('CONVERT_RDF.exe does not seem working for file %s', nomFic);
    msg = Lang(str1,str2);
    my_warndlg(msg, 0);
    flag = 0;
    return
end

%% Datagrammes FileHeader

nomFicXml = fullfile(nomDirRacine, 'RDF_FileHeader.xml');
if exist(nomFicXml, 'file')
    flag = rdf2ssc_FileHeader(nomDirRacine, FormatVersion);
    if ~flag
        return
    end
end

%% Datagrammes Header

nomFicXml = fullfile(nomDirRacine, 'RDF_Header.xml');
if exist(nomFicXml, 'file')
    [flag, DataHeader] = rdf2ssc_Header(nomDirRacine);
    if ~flag
        return
    end
end

%% Datagrammes Attitude

nomFicXml = fullfile(nomDirRacine, 'RDF_Attitude.xml');
if exist(nomFicXml, 'file')
    flag = rdf2ssc_Attitude(nomDirRacine, DataHeader);
    if ~flag
        return
    end
end

%% Datagram AttString

nomFicXml = fullfile(nomDirRacine, 'RDF_AttString.xml');
if exist(nomFicXml, 'file')
    flag = rdf2ssc_AttitudeString(nomDirRacine, DataHeader);
    if ~flag
        return
    end
end

%% Datagrammes EchoSounder

nomFicXml = fullfile(nomDirRacine, 'RDF_EchoSounder.xml');
if exist(nomFicXml, 'file')
    flag = rdf2ssc_EchoSounder(nomDirRacine, DataHeader);
    if ~flag
        return
    end
end

%% DatagrammesEchoSounderString

nomFicXml = fullfile(nomDirRacine, 'RDF_EchoSounderString.xml');
if exist(nomFicXml, 'file')
    flag = rdf2ssc_EchoSounderString(nomDirRacine, DataHeader);
    if ~flag
        return
    end
end

%% Datagrammes Heading

nomFicXml = fullfile(nomDirRacine, 'RDF_Heading.xml');
if exist(nomFicXml, 'file')
    flag = rdf2ssc_Heading(nomDirRacine, DataHeader);
    if ~flag
        return
    end
end

%% Datagrammes HeadingString

nomFicXml = fullfile(nomDirRacine, 'RDF_HeadingString.xml');
if exist(nomFicXml, 'file')
    flag = rdf2ssc_HeadingString(nomDirRacine, DataHeader);
    if ~flag
        return
    end
end

%% Datagrammes MiniSVS

nomFicXml = fullfile(nomDirRacine, 'RDF_MiniSVS.xml');
if exist(nomFicXml, 'file')
    flag = rdf2ssc_MiniSVS(nomDirRacine, DataHeader);
    if ~flag
        return
    end
end

%% Datagrammes MiniSVSString

nomFicXml = fullfile(nomDirRacine, 'RDF_MiniSVSString.xml');
if exist(nomFicXml, 'file')
    flag = rdf2ssc_MiniSVSString(nomDirRacine, DataHeader);
    if ~flag
        return
    end
end

%% Datagrammes Data_Navigation

nomFicXml = fullfile(nomDirRacine, 'RDF_Navigation.xml');
if exist(nomFicXml, 'file')
    flag = rdf2ssc_Navigation(nomDirRacine, DataHeader);
    if ~flag
        return
    end
end

%% Cr�ation pseudo datagrammes Height

flag = rdf2ssc_Height(nomFic);
if ~flag
    return
end

%% Datagrammes RDF_NavString

nomFicXml = fullfile(nomDirRacine, 'RDF_NavString.xml');
if exist(nomFicXml, 'file')
    flag = rdf2ssc_NavigationString(nomDirRacine, DataHeader);
    if ~flag
%         return % Comment� le 05/03/2014 pour donn�e Zostera fichier
                 % ZOSTERA_254.rdf erreur dans  read_NMEA_INGGA : 
                 % '$INGGA'    '092418.00'    '4734.88109'    'N'    '00252.28414'    'W'    '5'    '0777777777777777777777777777777777777'
    end
end

%% Datagrammes Aux1_String

nomFicXml = fullfile(nomDirRacine, 'RDF_Aux1String.xml');
if exist(nomFicXml, 'file')
    flag = rdf2ssc_Aux1String(nomDirRacine, DataHeader);
    if ~flag
        return
    end
end

%% Datagrammes Aux2String

nomFicXml = fullfile(nomDirRacine, 'RDF_Aux2String.xml');
if exist(nomFicXml, 'file')
    flag = rdf2ssc_Aux2String(nomDirRacine, DataHeader);
    if ~flag
        return
    end
end

%% Datagrammes Samples

nomFicXml = fullfile(nomDirRacine, 'RDF_Sample.xml');
if exist(nomFicXml, 'file')
    [flag, DataMiniSVS] = SSc_ReadDatagrams(nomFic, 'Ssc_MiniSVS'); %, 'Memmapfile', 1);
    if ~flag
        DataMiniSVS = [];
    end

    flag = rdf2ssc_Sample(nomDirRacine, DataHeader, DataMiniSVS);
    if ~flag
        return
    end
end

%% Gestion du cache

switch useCacheNetcdf
    case 1 % 'Create XML-Bin only'
    case 2 % 'Create Netcdf and keep XML-Bin'
        RDF_Cache2Netcdf(nomFic, 'deleteXMLBin', 0);
    case 3 % 'Create Netcdf and delete XML-Bin'
        RDF_Cache2Netcdf(nomFic, 'deleteXMLBin', 1);
end

%% The End

flag = 1;


function FormatVersion = getFormatVersion(nomDirRacine)

nomFicXml = fullfile(nomDirRacine, 'Ssc_FileHeader.xml');
flag = exist(nomFicXml, 'file');        
if ~flag
    special_message('FichierNonExistant', nomFicXml)
    return
end
% edit(nomFicXml)
Datagrams = xml_read(nomFicXml);
if ~isfield(Datagrams, 'FormatVersion')
    FormatVersion = 0;
    return
end
FormatVersion = Datagrams.FormatVersion;
