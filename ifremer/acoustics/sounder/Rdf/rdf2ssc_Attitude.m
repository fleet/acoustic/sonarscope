function flag = rdf2ssc_Attitude(nomDirRacine, Header)

%% Lecture du fichier XML d�crivant la donn�e 

nomFicXml = fullfile(nomDirRacine, 'RDF_Attitude.xml');
flag = exist(nomFicXml, 'file');        
if ~flag
    special_message('FichierNonExistant', nomFicXml)
    return
end
% edit(nomFicXml)
Datagrams = xml_read(nomFicXml);

[nomDirSignal, nomFicSignal] = fileparts(nomFicXml);
nomDirSignal = fullfile(nomDirSignal, nomFicSignal);

NbSamples = sum(Header.AttitudeNumber);
       
%% Lecture de roll

[flag, Data.roll] = Read_BinTable(Datagrams, nomDirSignal, 'roll', 'single', NbSamples);
if ~flag
    return
end
% figure; plot(Data.roll); grid on; title('roll');

%% Lecture de pitch

[flag, Data.pitch] = Read_BinTable(Datagrams, nomDirSignal, 'pitch', 'single', NbSamples);
if ~flag
    return
end
% figure; plot(Data.pitch); grid on; title('pitch');

%% Lecture du heave

[flag, Data.heave] = Read_BinTable(Datagrams, nomDirSignal, 'heave', 'single', NbSamples);
if ~flag
    return
end
% figure; plot(Data.heave); grid on; title('heave');

%% Lecture du timestamp

[flag, Data.timestamp] = Read_BinTable(Datagrams, nomDirSignal, 'timestamp', 'double', NbSamples); 
if ~flag
    return
end
% figure; plot(Data.timestamp); grid on; title('timestamp');

%% G�n�ration du XML SonarScope

Info.Title       = Datagrams.Title; % Entete du fichier
Info.Constructor = Datagrams.Constructor;
Info.TimeOrigin  = Datagrams.TimeOrigin;
Info.Comments    = Datagrams.Comments;

Info.Dimensions.NbSamples = NbSamples;

Info.Signals(1).Name          = 'roll';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_Attitude', 'roll.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'pitch';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_Attitude', 'pitch.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'heave';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_Attitude', 'heave.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'timestamp';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_Attitude', 'timestamp.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

%% Cr�ation du fichier XML d�crivant la donn�e 

nomFicXml = fullfile(nomDirRacine, 'Ssc_Attitude.xml');
xml_write(nomFicXml, Info);
flag = exist(nomFicXml, 'file');
if ~flag
    messageErreur(nomFicXml)
    return
end

%% Cr�ation du r�pertoire File Attitude

nomDirSscData = fullfile(nomDirRacine, 'Ssc_Attitude');
if ~exist(nomDirSscData, 'dir')
    status = mkdir(nomDirSscData);
    if ~status
        messageErreur(nomDirSscData)
        flag = 0;
        return
    end
end

%% Cr�ation des fichiers binaires des signaux

for k=1:length(Info.Signals)
    flag = writeSignal(nomDirRacine, Info.Signals(k), Data.(Info.Signals(k).Name));
    if ~flag
        return
    end
end

flag = 1;
