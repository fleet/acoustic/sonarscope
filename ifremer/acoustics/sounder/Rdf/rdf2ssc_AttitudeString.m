function flag = rdf2ssc_AttitudeString(nomDirRacine, Header)

%% Lecture du fichier XML d�crivant la donn�e 

nomFicXml = fullfile(nomDirRacine, 'RDF_AttString.xml');
flag = exist(nomFicXml, 'file');        
if ~flag
    special_message('FichierNonExistant', nomFicXml)
    return
end
% edit(nomFicXml)
Datagrams = xml_read(nomFicXml);

[nomDirSignal, nomFicSignal] = fileparts(nomFicXml);
nomDirSignal = fullfile(nomDirSignal, nomFicSignal);          
       
%% Lecture de Attitude String

[flag, Data.AttitudeString] = Read_BinTable(Datagrams, nomDirSignal, 'AttitudeString', 'uint8', sum(Header.AttitudeStringSize));
if ~flag
    return
end

% Pr�paration de la cha�ne � copier dans le ssc.bin
maxSize = max(Header.AttitudeStringSize);
Data.AttitudeString = zeros(Datagrams.NbDatagrams, maxSize, 'uint8');
sub = 0;
for k=1:Datagrams.NbDatagrams
    if Header.AttitudeStringSize(k) ~= 0
        sub = sub(end) + (1:Header.AttitudeStringSize(k));
        Data.AttitudeString(k,1:Header.AttitudeStringSize(k)) = Data.AttitudeString(1:Header.AttitudeStringSize(k));
    end
end

%% G�n�ration du XML SonarScope

Info.Title       = Datagrams.Title; % Entete du fichier
Info.Constructor = Datagrams.Constructor;
Info.TimeOrigin  = Datagrams.TimeOrigin;
Info.Comments    = Datagrams.Comments;

Info.Dimensions.NbDatagrams          = Datagrams.NbDatagrams;
Info.Dimensions.AttitudeStringLength = maxSize;

Info.Images(1).Name          = 'AttitudeString';
Info.Images(end).Dimensions  = 'NbDatagrams, AttitudeStringLength';
Info.Images(end).Storage     = 'uint8';
Info.Images(end).Unit        = [];
Info.Images(end).FileName    = fullfile('Ssc_AttString','AttitudeString.bin');
Info.Images(end).Tag         = verifKeyWord('TODO');

%% Cr�ation du fichier XML d�crivant la donn�e 

nomFicXml = fullfile(nomDirRacine, 'Ssc_AttString.xml');
xml_write(nomFicXml, Info);
flag = exist(nomFicXml, 'file');
if ~flag
    messageErreur(nomFicXml)
    return
end

%% Cr�ation du r�pertoire File AttString

nomDirSscData = fullfile(nomDirRacine, 'Ssc_AttString');
if ~exist(nomDirSscData, 'dir')
    status = mkdir(nomDirSscData);
    if ~status
        messageErreur(nomDirSscData)
        flag = 0;
        return
    end
end

%% Cr�ation des fichiers binaires des tables

for k=1:length(Info.Images)
    flag = writeImage(nomDirRacine, Info.Images(k), Data.(Info.Images(k).Name));
    if ~flag
        return
    end
end

flag = 1;
