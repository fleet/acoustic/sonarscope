function flag = rdf2ssc_Aux1(nomDirRacine, Header, varargin)

[varargin, flagDisplay] = getPropertyValue(varargin, 'Display', 0); %#ok<ASGLU>

% ------------------------------------------
% Lecture du fichier XML d�crivant la donn�e

nomFicXml = fullfile(nomDirRacine, 'RDF_Aux1.xml');
flag = exist(nomFicXml, 'file');
if ~flag
    special_message('FichierNonExistant', nomFicXml)
    return
end
% edit(nomFicXml)
Datagrams = xml_read(nomFicXml);

[nomDirSignal, nomFicSignal] = fileparts(nomFicXml);
nomDirSignal = fullfile(nomDirSignal, nomFicSignal);

% ------------------------
% Lecture du timestamp

[flag, Data.timestamp] = Read_BinTable(Datagrams, nomDirSignal, 'timestamp', 'double', sum(Header.Aux1Number));
if ~flag
    return
end
if flagDisplay
    figure; plot(Data.timestamp); grid on; title('timestamp');
end


% ----------------------------
% G�n�ration du XML SonarScope

Info.Title                  = Datagrams.Title; % Entete du fichier
Info.Constructor            = Datagrams.Constructor;
% Info.EmModel                = Datagrams.Model;
% Info.SystemSerialNumber     = Datagrams.SystemSerialNumber;
Info.TimeOrigin             = Datagrams.TimeOrigin;
Info.Comments               = Datagrams.Comments;

Info.Dimensions.NbDatagrams     = Datagrams.NbDatagrams;
Info.Dimensions.NbAux1   = sum(Header.Aux1Number);


Info.Signals(1).Name          = 'timestamp';
Info.Signals(end).Dimensions  = 'NbDatagrams, NbAux1';
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_Aux1','timestamp.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');


%% Cr�ation du fichier XML d�crivant la donn�e

nomFicXml = fullfile(nomDirRacine, 'Ssc_Aux1.xml');
xml_write(nomFicXml, Info);
flag = exist(nomFicXml, 'file');
if ~flag
    messageErreur(nomFicXml)
    return
end

%% Cr�ation du r�pertoire File Aux1

nomDirSscData = fullfile(nomDirRacine, 'Ssc_Aux1');
if ~exist(nomDirSscData, 'dir')
    status = mkdir(nomDirSscData);
    if ~status
        messageErreur(nomDirSscData)
        flag = 0;
        return
    end
end


%% Cr�ation des fichiers binaires des signaux

for i=1:length(Info.Signals)
    flag = writeSignal(nomDirRacine, Info.Signals(i), Data.(Info.Signals(i).Name));
    if ~flag
        return
    end
end

flag = 1;
