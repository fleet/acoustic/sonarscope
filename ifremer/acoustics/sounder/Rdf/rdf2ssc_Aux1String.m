function flag = rdf2ssc_Aux1String(nomDirRacine, Header)

%% Lecture du fichier XML d�crivant la donn�e 

nomFicXml = fullfile(nomDirRacine, 'RDF_Aux1String.xml');
flag = exist(nomFicXml, 'file');        
if ~flag
    special_message('FichierNonExistant', nomFicXml)
    return
end
% edit(nomFicXml)
Datagrams = xml_read(nomFicXml);

[nomDirSignal, nomFicSignal] = fileparts(nomFicXml);
nomDirSignal = fullfile(nomDirSignal, nomFicSignal);
            
%% Lecture de Aux1 String

[flag, Data.Aux1String] = Read_BinTable(Datagrams, nomDirSignal, 'Aux1String', 'uint8', sum(Header.Aux1StringSize));
if ~flag
    return
end

% Pr�paration de la cha�ne � copier dans le ssc.bin
maxSize = max(Header.Aux1StringSize);
Data.Aux1String = zeros(Datagrams.NbDatagrams, maxSize, 'uint8');
sub = 0;
for k=1:Datagrams.NbDatagrams
    if Header.Aux1StringSize(k) ~= 0
        sub = sub(end) + (1:Header.Aux1StringSize(k));
        Data.Aux1String(k,1:Header.Aux1StringSize(k)) = Data.Aux1String(sub);
    end
end

%% G�n�ration du XML SonarScope

Info.Title       = Datagrams.Title; % Entete du fichier
Info.Constructor = Datagrams.Constructor;
Info.TimeOrigin  = Datagrams.TimeOrigin;
Info.Comments    = Datagrams.Comments;

Info.Dimensions.NbSamples        = Datagrams.NbDatagrams;
Info.Dimensions.Aux1StringLength = maxSize;

Info.Images(1).Name         = 'Aux1String';
Info.Images(end).Dimensions = 'NbSamples, Aux1StringLength';
Info.Images(end).Storage    = 'uint8';
Info.Images(end).Unit       = [];
Info.Images(end).FileName   = fullfile('Ssc_Aux1String','Aux1String.bin');
Info.Images(end).Tag        = verifKeyWord('TODO');

%% Cr�ation du fichier XML d�crivant la donn�e 

nomFicXml = fullfile(nomDirRacine, 'Ssc_Aux1String.xml');
xml_write(nomFicXml, Info);
flag = exist(nomFicXml, 'file');
if ~flag
    messageErreur(nomFicXml)
    return
end

%% Cr�ation du r�pertoire File Aux1String

nomDirSscData = fullfile(nomDirRacine, 'Ssc_Aux1String');
if ~exist(nomDirSscData, 'dir')
    status = mkdir(nomDirSscData);
    if ~status
        messageErreur(nomDirSscData)
        flag = 0;
        return
    end
end

%% Cr�ation des fichiers binaires des images

for k=1:length(Info.Images)
    flag = writeImage(nomDirRacine, Info.Images(k), Data.(Info.Images(k).Name));
    if ~flag
        return
    end
end

flag = 1;
