function flag = rdf2ssc_Aux2(nomDirRacine, Header, varargin)

[varargin, flagDisplay] = getPropertyValue(varargin, 'Display', 0); %#ok<ASGLU>

% ------------------------------------------
% Lecture du fichier XML d�crivant la donn�e

nomFicXml = fullfile(nomDirRacine, 'RDF_Aux2.xml');
flag = exist(nomFicXml, 'file');
if ~flag
    special_message('FichierNonExistant', nomFicXml)
    return
end
% edit(nomFicXml)
Datagrams = xml_read(nomFicXml);

[nomDirSignal, nomFicSignal] = fileparts(nomFicXml);
nomDirSignal = fullfile(nomDirSignal, nomFicSignal);

% ------------------------
% Lecture du timestamp

[flag, Data.timestamp] = Read_BinTable(Datagrams, nomDirSignal, 'timestamp', 'double', sum(Header.Aux2Number));
if ~flag
    return
end
if flagDisplay
    figure; plot(Data.timestamp); grid on; title('timestamp');
end


% ----------------------------
% G�n�ration du XML SonarScope

Info.Title                  = Datagrams.Title; % Entete du fichier
Info.Constructor            = Datagrams.Constructor;
% Info.EmModel                = Datagrams.Model;
% Info.SystemSerialNumber     = Datagrams.SystemSerialNumber;
Info.TimeOrigin             = Datagrams.TimeOrigin;
Info.Comments               = Datagrams.Comments;

Info.Dimensions.NbDatagrams     = Datagrams.NbDatagrams;
Info.Dimensions.NbAux2   = sum(Header.Aux2Number);


Info.Images(1).Name          = 'timestamp';
Info.Images(end).Dimensions  = 'NbDatagrams, NbAux2';
Info.Images(end).Storage     = 'double';
Info.Images(end).Unit        = [];
Info.Images(end).FileName    = fullfile('Ssc_Aux2','timestamp.bin');
Info.Images(end).Tag         = verifKeyWord('TODO');


%% Cr�ation du fichier XML d�crivant la donn�e

nomFicXml = fullfile(nomDirRacine, 'Ssc_Aux2.xml');
xml_write(nomFicXml, Info);
flag = exist(nomFicXml, 'file');
if ~flag
    messageErreur(nomFicXml)
    return
end

%% Cr�ation du r�pertoire File Aux2

nomDirSscData = fullfile(nomDirRacine, 'Ssc_Aux2');
if ~exist(nomDirSscData, 'dir')
    status = mkdir(nomDirSscData);
    if ~status
        messageErreur(nomDirSscData)
        flag = 0;
        return
    end
end


%% Cr�ation des fichiers binaires des signaux

for i=1:length(Info.Images)
    flag = writeSignal(nomDirRacine, Info.Images(i), Data.(Info.Images(i).Name));
    if ~flag
        return
    end
end

flag = 1;
