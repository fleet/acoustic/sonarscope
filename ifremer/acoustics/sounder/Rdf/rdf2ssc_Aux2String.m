function flag = rdf2ssc_Aux2String(nomDirRacine, Header)

%% Lecture du fichier XML d�crivant la donn�e 

nomFicXml = fullfile(nomDirRacine, 'RDF_Aux2String.xml');
flag = exist(nomFicXml, 'file');        
if ~flag
    special_message('FichierNonExistant', nomFicXml)
    return
end
% edit(nomFicXml)
Datagrams = xml_read(nomFicXml);

[nomDirSignal, nomFicSignal] = fileparts(nomFicXml);
nomDirSignal = fullfile(nomDirSignal, nomFicSignal);
            
%% Lecture de Aux2 String

[flag, Data.Aux2String] = Read_BinTable(Datagrams, nomDirSignal, 'Aux2String', 'uint8', sum(Header.Aux2StringSize));
if ~flag
    return
end

% Pr�paration de la cha�ne � copier dans le ssc.bin
maxSize = max(Header.Aux2StringSize);
Data.Aux2String = zeros(Datagrams.NbDatagrams, maxSize, 'uint8');
sub = 0;
for k=1:Datagrams.NbDatagrams
    if Header.Aux2StringSize(k) ~= 0
        sub = sub(end) + (1:Header.Aux2StringSize(k));
        Data.Aux2String(k,1:Header.Aux2StringSize(k)) = Data.Aux2String(sub);
    end
end

%% G�n�ration du XML SonarScope

Info.Title       = Datagrams.Title; % Entete du fichier
Info.Constructor = Datagrams.Constructor;
Info.TimeOrigin  = Datagrams.TimeOrigin;
Info.Comments    = Datagrams.Comments;

Info.Dimensions.NbSamples        = Datagrams.NbDatagrams;
Info.Dimensions.Aux2StringLength = maxSize;

Info.Images(1).Name         = 'Aux2String';
Info.Images(end).Dimensions = 'NbSamples, Aux2StringLength';
Info.Images(end).Storage    = 'uint8';
Info.Images(end).Unit       = [];
Info.Images(end).FileName   = fullfile('Ssc_Aux2String','Aux2String.bin');
Info.Images(end).Tag        = verifKeyWord('TODO');

%% Cr�ation du fichier XML d�crivant la donn�e 

nomFicXml = fullfile(nomDirRacine, 'Ssc_Aux2String.xml');
xml_write(nomFicXml, Info);
flag = exist(nomFicXml, 'file');
if ~flag
    messageErreur(nomFicXml)
    return
end

%% Cr�ation du r�pertoire File Aux2String

nomDirSscData = fullfile(nomDirRacine, 'Ssc_Aux2String');
if ~exist(nomDirSscData, 'dir')
    status = mkdir(nomDirSscData);
    if ~status
        messageErreur(nomDirSscData)
        flag = 0;
        return
    end
end

%% Cr�ation des fichiers binaires des images

for k=1:length(Info.Images)
    flag = writeImage(nomDirRacine, Info.Images(k), Data.(Info.Images(k).Name));
    if ~flag
        return
    end
end

flag = 1;
