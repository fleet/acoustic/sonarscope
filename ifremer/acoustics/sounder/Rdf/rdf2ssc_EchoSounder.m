function flag = rdf2ssc_EchoSounder(nomDirRacine, Header)

%% Lecture du fichier XML d�crivant la donn�e 

nomFicXml = fullfile(nomDirRacine, 'RDF_EchoSounder.xml');
flag = exist(nomFicXml, 'file');        
if ~flag
    special_message('FichierNonExistant', nomFicXml)
    return
end
% edit(nomFicXml)
Datagrams = xml_read(nomFicXml);

[nomDirSignal, nomFicSignal] = fileparts(nomFicXml);
nomDirSignal = fullfile(nomDirSignal, nomFicSignal);
                   
%% Lecture de depth1

[flag, Data.depth1] = Read_BinTable(Datagrams, nomDirSignal, 'depth1', 'single', sum(Header.EchoSounderNumber));
if ~flag
    return
end
% figure; plot(Data.depth1); grid on; title('depth1');

%% Lecture de depth2

[flag, Data.depth2] = Read_BinTable(Datagrams, nomDirSignal, 'depth2', 'single', sum(Header.EchoSounderNumber));
if ~flag
    return
end
% figure; plot(Data.depth2); grid on; title('depth2');

%% Lecture du timestamp

[flag, Data.timestamp] = Read_BinTable(Datagrams, nomDirSignal, 'timestamp', 'double', sum(Header.EchoSounderNumber)); 
if ~flag
    return
end
% figure; plot(Data.timestamp); grid on; title('timestamp');

%% G�n�ration du XML SonarScope

Info.Title                  = Datagrams.Title; % Entete du fichier
Info.Constructor            = Datagrams.Constructor;
% Info.EmModel                = Datagrams.Model;
% Info.SystemSerialNumber     = Datagrams.SystemSerialNumber;
Info.TimeOrigin             = Datagrams.TimeOrigin;
Info.Comments               = Datagrams.Comments;

Info.Dimensions.NbDatagrams     = Datagrams.NbDatagrams;

Info.Signals(1).Name          = 'depth1';
Info.Signals(end).Dimensions  = 'NbDatagrams, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_EchoSounder','depth1.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'depth2';
Info.Signals(end).Dimensions  = 'NbDatagrams, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_EchoSounder','depth2.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'timestamp';
Info.Signals(end).Dimensions  = 'NbDatagrams, 1';
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_EchoSounder','timestamp.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

%% Cr�ation du fichier XML d�crivant la donn�e 

nomFicXml = fullfile(nomDirRacine, 'Ssc_EchoSounder.xml');
xml_write(nomFicXml, Info);
flag = exist(nomFicXml, 'file');
if ~flag
    messageErreur(nomFicXml)
    return
end

%% Cr�ation du r�pertoire File EchoSounder

nomDirSscData = fullfile(nomDirRacine, 'Ssc_EchoSounder');
if ~exist(nomDirSscData, 'dir')
    status = mkdir(nomDirSscData);
    if ~status
        messageErreur(nomDirSscData)
        flag = 0;
        return
    end
end

%% Cr�ation des fichiers binaires des signaux

for i=1:length(Info.Signals)
    flag = writeSignal(nomDirRacine, Info.Signals(i), Data.(Info.Signals(i).Name));
    if ~flag
        return
    end
end

flag = 1;
