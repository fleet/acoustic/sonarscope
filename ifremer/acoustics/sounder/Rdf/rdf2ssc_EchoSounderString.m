function flag = rdf2ssc_EchoSounderString(nomDirRacine, Header)

%% Lecture du fichier XML d�crivant la donn�e 

nomFicXml = fullfile(nomDirRacine, 'RDF_EchoSounderString.xml');
flag = exist(nomFicXml, 'file');        
if ~flag
    special_message('FichierNonExistant', nomFicXml)
    return
end
% edit(nomFicXml)
Datagrams = xml_read(nomFicXml);

[nomDirSignal, nomFicSignal] = fileparts(nomFicXml);
nomDirSignal = fullfile(nomDirSignal, nomFicSignal);
            
       
%% Lecture de EchoSounder String

[flag, Data.EchoSounderString] = Read_BinTable(Datagrams, nomDirSignal, 'EchoSounderString', 'uint8', sum(Header.EchoSounderStringSize));
if ~flag
    return
end

% Pr�paration de la cha�ne � copier dans le ssc.bin
maxSize = max(Header.EchoSounderStringSize);
Data.EchoSounderString = zeros(Datagrams.NbDatagrams, maxSize, 'uint8');
sub = 0;
for k=1:Datagrams.NbDatagrams
    if Header.EchoSounderStringSize(k) ~= 0
        sub = sub(end) + (1:Header.EchoSounderStringSize(k));
        Data.EchoSounderString(k,1:Header.EchoSounderStringSize(k)) = Data.EchoSounderString(1:Header.EchoSounderStringSize(k));
    end
end

%% G�n�ration du XML SonarScope

Info.Title       = Datagrams.Title; % Entete du fichier
Info.Constructor = Datagrams.Constructor;
Info.TimeOrigin  = Datagrams.TimeOrigin;
Info.Comments    = Datagrams.Comments;

Info.Dimensions.NbDatagrams             = Datagrams.NbDatagrams;
Info.Dimensions.EchoSounderStringLength = maxSize;

Info.Images(1).Name          = 'EchoSounderString';
Info.Images(end).Dimensions  = 'NbDatagrams, EchoSounderStringLength';
Info.Images(end).Storage     = 'char';
Info.Images(end).Unit        = [];
Info.Images(end).FileName    = fullfile('Ssc_EchoSounderString','EchoSounderString.bin');
Info.Images(end).Tag         = verifKeyWord('TODO');

%% Cr�ation du fichier XML d�crivant la donn�e 

nomFicXml = fullfile(nomDirRacine, 'Ssc_EchoSounderString.xml');
xml_write(nomFicXml, Info);
flag = exist(nomFicXml, 'file');
if ~flag
    messageErreur(nomFicXml)
    return
end

%% Cr�ation du r�pertoire File EchoSounderString

nomDirSscData = fullfile(nomDirRacine, 'Ssc_EchoSounderString');
if ~exist(nomDirSscData, 'dir')
    status = mkdir(nomDirSscData);
    if ~status
        messageErreur(nomDirSscData)
        flag = 0;
        return
    end
end

%% Cr�ation des fichiers binaires des images

for k=1:length(Info.Images)
    flag = writeImage(nomDirRacine, Info.Images(k), Data.(Info.Images(k).Name));
    if ~flag
        return
    end
end

flag = 1;
