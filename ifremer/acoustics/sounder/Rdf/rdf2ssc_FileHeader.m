function flag = rdf2ssc_FileHeader(nomDirRacine, FormatVersion)

%% Lecture du fichier XML d�crivant la donn�e 

nomFicXml = fullfile(nomDirRacine, 'RDF_FileHeader.xml');
flag = exist(nomFicXml, 'file');        
if ~flag
    special_message('FichierNonExistant', nomFicXml)
    return
end
% edit(nomFicXml)
Datagrams = xml_read(nomFicXml);

%% G�n�ration du XML SonarScope

Info.Title         = Datagrams.Title; % Entete du fichier
Info.Constructor   = Datagrams.Constructor;
Info.TimeOrigin    = Datagrams.TimeOrigin;
Info.Comments      = Datagrams.Comments;
Info.FormatVersion = FormatVersion;

for k=1:length(Datagrams.Variables)
    Info.(Datagrams.Variables(k).Name) = Datagrams.Variables(k).Constant;
end

%% Cr�ation du fichier XML d�crivant la donn�e 

nomFicXml = fullfile(nomDirRacine, 'Ssc_FileHeader.xml');
xml_write(nomFicXml, Info);
flag = exist(nomFicXml, 'file');
if ~flag
    messageErreur(nomFicXml)
    return
end
