function flag = rdf2ssc_FileIndex(nomDirRacine)

%% Lecture du fichier XML d�crivant la donn�e 

nomFicXml = fullfile(nomDirRacine, 'RDF_FileIndex.xml');
flag = exist(nomFicXml, 'file');        
if ~flag
    special_message('FichierNonExistant', nomFicXml)
    return
end
% edit(nomFicXml)
Datagrams = xml_read(nomFicXml);
% gen_object_display(Datagrams)

Datagrams.Tables = Datagrams.Variables;

nbPings = Datagrams.NbDatagrams;

[nomDirSignal, nomFicSignal] = fileparts(nomFicXml);
nomDirSignal = fullfile(nomDirSignal, nomFicSignal);   
       
%% Lecture de Date

[flag, Data.Date] = Read_BinTable(Datagrams, nomDirSignal, 'Date', 'single', nbPings);
if ~flag
    return
end
% figure; plot(Data.Date); grid on; title('Date');

%% Lecture de Position

[flag, Data.Position] = Read_BinTable(Datagrams, nomDirSignal, 'Position', 'single', nbPings);
if ~flag
    return
end
% figure; plot(Data.Position); grid on; title('Position');

%% Lecture du NbOfBytesInDatagram

[flag, Data.NbOfBytesInDatagram] = Read_BinTable(Datagrams, nomDirSignal, 'NbOfBytesInDatagram', 'single', nbPings);
if ~flag
    return
end
% figure; plot(Data.NbOfBytesInDatagram); grid on; title('NbOfBytesInDatagram');

%% Lecture du Side

[flag, Data.Side] = Read_BinTable(Datagrams, nomDirSignal, 'Side', 'double', nbPings); 
if ~flag
    return
end
% figure; plot(Data.Side); grid on; title('Side');

%% Lecture du PingCounter

[flag, Data.PingCounter] = Read_BinTable(Datagrams, nomDirSignal, 'PingCounter', 'double', nbPings); 
if ~flag
    return
end
% figure; plot(Data.PingCounter); grid on; title('PingCounter');

%% G�n�ration du XML SonarScope

Info.Title                  = Datagrams.Title; % Entete du fichier
Info.Constructor            = Datagrams.Constructor;
% Info.EmModel                = Datagrams.Model;
% Info.SystemSerialNumber     = Datagrams.SystemSerialNumber;
Info.TimeOrigin             = Datagrams.TimeOrigin;
Info.Comments               = Datagrams.Comments;

Info.Dimensions.nbPings     = nbPings;

Info.Signals(1).Name          = 'Date';
Info.Signals(end).Dimensions  = 'nbPings,1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_FileIndex','Date.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'Position';
Info.Signals(end).Dimensions  = 'nbPings,1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_FileIndex','Position.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'NbOfBytesInDatagram';
Info.Signals(end).Dimensions  = 'nbPings,1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_FileIndex','NbOfBytesInDatagram.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'Side';
Info.Signals(end).Dimensions  = 'nbPings,1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_FileIndex','Side.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'PingCounter';
Info.Signals(end).Dimensions  = 'nbPings,1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_FileIndex','PingCounter.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');


%% Cr�ation du fichier XML d�crivant la donn�e 

nomFicXml = fullfile(nomDirRacine, 'Ssc_FileIndex.xml');
xml_write(nomFicXml, Info);
flag = exist(nomFicXml, 'file');
if ~flag
    messageErreur(nomFicXml)
    return
end

%% Cr�ation du r�pertoire File FileIndex

nomDirSscData = fullfile(nomDirRacine, 'Ssc_FileIndex');
if ~exist(nomDirSscData, 'dir')
    status = mkdir(nomDirSscData);
    if ~status
        messageErreur(nomDirSscData)
        flag = 0;
        return
    end
end

%% Cr�ation des fichiers binaires des signaux

for i=1:length(Info.Signals)
    flag = writeSignal(nomDirRacine, Info.Signals(i), Data.(Info.Signals(i).Name));
    if ~flag
        return
    end
end

flag = 1;
