function [flag, Data] = rdf2ssc_Header(nomDirRacine)

%% Lecture du fichier XML d�crivant la donn�e 

nomFicXml = fullfile(nomDirRacine, 'RDF_Header.xml');
flag = exist(nomFicXml, 'file');        
if ~flag
    special_message('FichierNonExistant', nomFicXml)
    return
end
% edit(nomFicXml)
Datagrams = xml_read(nomFicXml);

[nomDirSignal, nomFicSignal] = fileparts(nomFicXml);
nomDirSignal = fullfile(nomDirSignal, nomFicSignal);
            
%% Lecture de PingNumber

[flag, Data.PingNumber] = Read_BinVariable(Datagrams, nomDirSignal, 'PingNumber', 'int32');
if ~flag
    return
end
% figure; plot(Data.PingNumber); grid on; title('DateHeure');

%% Lecture de DateHeure

[flag, Data.DateHeure] = Read_BinVariable(Datagrams, nomDirSignal, 'DateHeure', 'double');
if ~flag
    return
end
% figure; plot(Data.DateHeure); grid on; title('DateHeure');

%% Lecture du PreviousPingPosition

[flag, Data.PreviousPingPosition] = Read_BinVariable(Datagrams, nomDirSignal, 'PreviousPingPosition', 'int32');
if ~flag
    return
end
% figure; plot(PreviousPingPosition); grid on; title('PreviousPingPosition');

%% Lecture du PingSize

[flag, Data.PingSize] = Read_BinVariable(Datagrams, nomDirSignal, 'PingSize', 'int32'); 
if ~flag
    return
end
% figure; plot(Data.PingSize); grid on; title('PingSize');

%% Lecture du NavigationNumber

[flag, Data.NavigationNumber] = Read_BinVariable(Datagrams, nomDirSignal, 'NavigationNumber', 'uint8');
if ~flag
    return
end
% figure; plot(Data.NavigationNumber); grid on; title('NavigationNumber')

%% Lecture du AttitudeNumber

[flag, Data.AttitudeNumber] = Read_BinVariable(Datagrams, nomDirSignal, 'AttitudeNumber', 'uint8');
if ~flag
    return
end
% figure; plot(Data.AttitudeNumber); grid on; title('AttitudeNumber')

%% Lecture du HeadingNumber

[flag, Data.HeadingNumber] = Read_BinVariable(Datagrams, nomDirSignal, 'HeadingNumber', 'uint8');
if ~flag
    return
end
% figure; plot(Data.HeadingNumber); grid on; title('HeadingNumber')

%% Lecture du EchoSounderNumber

[flag, Data.EchoSounderNumber] = Read_BinVariable(Datagrams, nomDirSignal, 'EchoSounderNumber', 'uint8');
if ~flag
    return
end
% figure; plot(Data.EchoSounderNumber); grid on; title('EchoSounderNumber')

%% Lecture du MiniSVSNumber

[flag, Data.MiniSVSNumber] = Read_BinVariable(Datagrams, nomDirSignal, 'MiniSVSNumber', 'uint8');
if ~flag
    return
end
% figure; plot(Data.MiniSVSNumber); grid on; title('MiniSVSNumber')

%% Lecture du Aux1Number

[flag, Data.Aux1Number] = Read_BinVariable(Datagrams, nomDirSignal, 'Aux1Number', 'uint8');
if ~flag
    return
end
% figure; plot(Data.Aux1Number); grid on; title('Aux1Number')

%% Lecture du Aux2Number

[flag, Data.Aux2Number] = Read_BinVariable(Datagrams, nomDirSignal, 'Aux2Number', 'uint8');
if ~flag
    return
end
% figure; plot(Data.Aux2Number); grid on; title('Aux2Number')

%% Lecture du PingLength

[flag, Data.PingLength] = Read_BinVariable(Datagrams, nomDirSignal, 'PingLength', 'uint16');
if ~flag
    return
end
% figure; plot(Data.PingLength); grid on; title('PingLength')

%% Lecture du PulseLength

[flag, Data.PulseLength] = Read_BinVariable(Datagrams, nomDirSignal, 'PulseLength', 'uint8');
if ~flag
    return
end
% figure; plot(Data.PulseLength); grid on; title('PulseLength')

%% Lecture du Power

[flag, Data.Power] = Read_BinVariable(Datagrams, nomDirSignal, 'Power', 'uint8');
if ~flag
    return
end
% figure; plot(Data.Power); grid on; title('Power')

%% Lecture du SideScanGain

[flag, Data.SideScanGain] = Read_BinVariable(Datagrams, nomDirSignal, 'SideScanGain', 'uint8');
if ~flag
    return
end
% figure; plot(Data.SideScanGain); grid on; title('SideScanGain')

%% Lecture du SampleNumber

[flag, Data.SampleNumber] = Read_BinVariable(Datagrams, nomDirSignal, 'SampleNumber', 'uint16');
if ~flag
    return
end
% figure; plot(Data.SampleNumber); grid on; title('SampleNumber')

%% Lecture du Side

[flag, Data.Side] = Read_BinVariable(Datagrams, nomDirSignal, 'Side', 'uint8');
if ~flag
    return
end
% figure; plot(Data.Side); grid on; title('Side')

%% Lecture du NavigationStringSize

[flag, Data.NavigationStringSize] = Read_BinVariable(Datagrams, nomDirSignal, 'NavigationStringSize', 'uint16');
if ~flag
    return
end
% figure; plot(Data.NavigationStringSize); grid on; title('NavigationStringSize')

%% Lecture du AttitudeStringSize

[flag, Data.AttitudeStringSize] = Read_BinVariable(Datagrams, nomDirSignal, 'AttitudeStringSize', 'uint16');
if ~flag
    return
end
% figure; plot(Data.AttitudeStringSize); grid on; title('AttitudeStringSize')

%% Lecture du HeadingStringSize

[flag, Data.HeadingStringSize] = Read_BinVariable(Datagrams, nomDirSignal, 'HeadingStringSize', 'uint16');
if ~flag
    return
end
% figure; plot(Data.HeadingStringSize); grid on; title('HeadingStringSize')

%% Lecture du EchoSounderStringSize

[flag, Data.EchoSounderStringSize] = Read_BinVariable(Datagrams, nomDirSignal, 'EchoSounderStringSize', 'uint16');
if ~flag
    return
end
% figure; plot(Data.EchoSounderStringSize); grid on; title('EchoSounderStringSize')

%% Lecture du MiniSVSStringSize

[flag, Data.MiniSVSStringSize] = Read_BinVariable(Datagrams, nomDirSignal, 'MiniSVSStringSize', 'uint16');
if ~flag
    return
end
% figure; plot(Data.MiniSVSStringSize); grid on; title('MiniSVSStringSize')

%% Lecture du Aux1StringSize

[flag, Data.Aux1StringSize] = Read_BinVariable(Datagrams, nomDirSignal, 'Aux1StringSize', 'uint16');
if ~flag
    return
end
% figure; plot(Data.Aux1StringSize); grid on; title('Aux1StringSize')

%% Lecture du Aux2StringSize

[flag, Data.Aux2StringSize] = Read_BinVariable(Datagrams, nomDirSignal, 'Aux2StringSize', 'uint16');
if ~flag
    return
end
% figure; plot(Data.Aux2StringSize); grid on; title('Aux2StringSize')

%% Calcul des dates et heures

Data.DateHeure = datenum(datetime(Data.DateHeure, 'ConvertFrom', 'posixtime'));

%% G�n�ration du XML SonarScope

Info.Title       = Datagrams.Title; % Entete du fichier
Info.Constructor = Datagrams.Constructor;
Info.TimeOrigin  = Datagrams.TimeOrigin;
Info.Comments    = Datagrams.Comments;

Info.Dimensions.NbSamples     = length(Data.PingNumber);

Info.Signals(1).Name          = 'PingNumber';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'int32';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_Header', 'PingNumber.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'DateHeure';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_Header', 'DateHeure.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'PreviousPingPosition';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'int32';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_Header', 'PreviousPingPosition.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'PingSize';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'int32';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_Header', 'PingSize.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'NavigationNumber';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint8';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_Header', 'NavigationNumber.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'AttitudeNumber';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint8';
Info.Signals(end).Unit        = 'Hz';
Info.Signals(end).FileName    = fullfile('Ssc_Header', 'AttitudeNumber.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'HeadingNumber';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint8';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_Header', 'HeadingNumber.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'EchoSounderNumber';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint8';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_Header', 'EchoSounderNumber.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'MiniSVSNumber';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint8';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_Header', 'MiniSVSNumber.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'Aux1Number';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint8';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_Header', 'Aux1Number.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'Aux2Number';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint8';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_Header', 'Aux2Number.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'PingLength';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_Header', 'PingLength.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'PulseLength';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint8';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_Header', 'PulseLength.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'Power';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint8';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_Header', 'Power.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'SideScanGain';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint8';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_Header', 'SideScanGain.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'SampleNumber';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'int32';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_Header', 'SampleNumber.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'Side';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint8';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_Header', 'Side.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'NavigationStringSize';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_Header', 'NavigationStringSize.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'AttitudeStringSize';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_Header', 'AttitudeStringSize.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'HeadingStringSize';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_Header', 'HeadingStringSize.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'EchoSounderStringSize';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_Header', 'EchoSounderStringSize.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'MiniSVSStringSize';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_Header', 'MiniSVSStringSize.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'Aux1StringSize';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_Header', 'Aux1StringSize.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'Aux2StringSize';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_Header', 'Aux2StringSize.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

%% Cr�ation du fichier XML d�crivant la donn�e 

nomFicXml = fullfile(nomDirRacine, 'Ssc_Header.xml');
xml_write(nomFicXml, Info);
flag = exist(nomFicXml, 'file');
if ~flag
    messageErreur(nomFicXml)
    return
end

%% Cr�ation du r�pertoire File Header

nomDirSscData = fullfile(nomDirRacine, 'Ssc_Header');
if ~exist(nomDirSscData, 'dir')
    status = mkdir(nomDirSscData);
    if ~status
        messageErreur(nomDirSscData)
        flag = 0;
        return
    end
end

%% Cr�ation des fichiers binaires des signaux

for k=1:length(Info.Signals)
    flag = writeSignal(nomDirRacine, Info.Signals(k), Data.(Info.Signals(k).Name));
    if ~flag
        return
    end
end

flag = 1;
