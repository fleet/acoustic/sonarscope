function flag = rdf2ssc_Heading(nomDirRacine, Header)

%% Lecture du fichier XML d�crivant la donn�e 

nomFicXml = fullfile(nomDirRacine, 'RDF_Heading.xml');
flag = exist(nomFicXml, 'file');        
if ~flag
    special_message('FichierNonExistant', nomFicXml)
    return
end
% edit(nomFicXml)
Datagrams = xml_read(nomFicXml);

[nomDirSignal, nomFicSignal] = fileparts(nomFicXml);
nomDirSignal = fullfile(nomDirSignal, nomFicSignal);

NbSamples = sum(Header.HeadingNumber);
                   
%% Lecture de Heading

[flag, Data.Heading] = Read_BinTable(Datagrams, nomDirSignal, 'heading', 'single', NbSamples);
if ~flag
    return
end
% figure; plot(Data.Heading); grid on; title('Heading');

%% Lecture du timestamp

[flag, Data.timestamp] = Read_BinTable(Datagrams, nomDirSignal, 'timestamp', 'double', NbSamples); 
if ~flag
    return
end
% figure; plot(Data.timestamp); grid on; title('timestamp');

%% G�n�ration du XML SonarScope

Info.Title       = Datagrams.Title; % Entete du fichier
Info.Constructor = Datagrams.Constructor;
Info.TimeOrigin  = Datagrams.TimeOrigin;
Info.Comments    = Datagrams.Comments;

Info.Dimensions.NbSamples = NbSamples;

Info.Signals(1).Name         = 'Heading';
Info.Signals(end).Dimensions = 'NbSamples,1';
Info.Signals(end).Storage    = 'single';
Info.Signals(end).Unit       = [];
Info.Signals(end).FileName   = fullfile('Ssc_Heading','Heading.bin');
Info.Signals(end).Tag        = verifKeyWord('TODO');

Info.Signals(end+1).Name     = 'timestamp';
Info.Signals(end).Dimensions = 'NbSamples,1';
Info.Signals(end).Storage    = 'double';
Info.Signals(end).Unit       = [];
Info.Signals(end).FileName   = fullfile('Ssc_Heading','timestamp.bin');
Info.Signals(end).Tag        = verifKeyWord('TODO');

%% Cr�ation du fichier XML d�crivant la donn�e 

nomFicXml = fullfile(nomDirRacine, 'Ssc_Heading.xml');
xml_write(nomFicXml, Info);
flag = exist(nomFicXml, 'file');
if ~flag
    messageErreur(nomFicXml)
    return
end

%% Cr�ation du r�pertoire File Heading

nomDirSscData = fullfile(nomDirRacine, 'Ssc_Heading');
if ~exist(nomDirSscData, 'dir')
    status = mkdir(nomDirSscData);
    if ~status
        messageErreur(nomDirSscData)
        flag = 0;
        return
    end
end

%% Cr�ation des fichiers binaires des signaux

for k=1:length(Info.Signals)
    flag = writeSignal(nomDirRacine, Info.Signals(k), Data.(Info.Signals(k).Name));
    if ~flag
        return
    end
end

flag = 1;
