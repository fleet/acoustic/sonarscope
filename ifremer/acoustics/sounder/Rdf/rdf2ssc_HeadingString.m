function flag = rdf2ssc_HeadingString(nomDirRacine, Header)

%% Lecture du fichier XML d�crivant la donn�e 

nomFicXml = fullfile(nomDirRacine, 'RDF_HeadingString.xml');
flag = exist(nomFicXml, 'file');        
if ~flag
    special_message('FichierNonExistant', nomFicXml)
    return
end
% edit(nomFicXml)
Datagrams = xml_read(nomFicXml);

[nomDirSignal, nomFicSignal] = fileparts(nomFicXml);
nomDirSignal = fullfile(nomDirSignal, nomFicSignal);
            
       
%% Lecture de Attitude String

[flag, pppp] = Read_BinTable(Datagrams, nomDirSignal, 'HeadingString', 'uint8', sum(Header.HeadingStringSize));
if ~flag
    return
end

% Pr�paration de la cha�ne � copier dans le ssc.bin
maxSize = max(Header.HeadingStringSize);
Data.HeadingString = zeros(Datagrams.NbDatagrams, maxSize, 'uint8');
for k=1:Datagrams.NbDatagrams
    Data.HeadingString(k,1:Header.HeadingStringSize(k)) = pppp(1:Header.HeadingStringSize(k));
end

%% G�n�ration du XML SonarScope

Info.Title       = Datagrams.Title; % Entete du fichier
Info.Constructor = Datagrams.Constructor;
Info.TimeOrigin  = Datagrams.TimeOrigin;
Info.Comments    = Datagrams.Comments;

Info.Dimensions.NbSamples           = Datagrams.NbDatagrams;
Info.Dimensions.HeadingStringLength = maxSize;

Info.Images(1).Name         = 'HeadingString';
Info.Images(end).Dimensions = 'NbSamples,HeadingStringLength';
Info.Images(end).Storage    = 'uint8';
Info.Images(end).Unit       = [];
Info.Images(end).FileName   = fullfile('Ssc_HeadingString','HeadingString.bin');
Info.Images(end).Tag        = verifKeyWord('TODO');

%% Cr�ation du fichier XML d�crivant la donn�e 

nomFicXml = fullfile(nomDirRacine, 'Ssc_HeadingString.xml');
xml_write(nomFicXml, Info);
flag = exist(nomFicXml, 'file');
if ~flag
    messageErreur(nomFicXml)
    return
end

%% Cr�ation du r�pertoire File HeadingString

nomDirSscData = fullfile(nomDirRacine, 'Ssc_HeadingString');
if ~exist(nomDirSscData, 'dir')
    status = mkdir(nomDirSscData);
    if ~status
        messageErreur(nomDirSscData)
        flag = 0;
        return
    end
end

%% Cr�ation des fichiers binaires des images

for k=1:length(Info.Images)
    flag = writeImage(nomDirRacine, Info.Images(k), Data.(Info.Images(k).Name));
    if ~flag
        return
    end
end

flag = 1;
