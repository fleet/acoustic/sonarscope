function flag = rdf2ssc_Height(nomFic)

[nomDir, nomFicSeul] = fileparts(nomFic);
nomDirRacine = fullfile(nomDir, 'SonarScope', nomFicSeul);

[flag, Data] = SScCacheXMLBinUtils.readWholeData(nomFic, 'Ssc_Height');
if ~flag
    flag = 1;
    return
end

%% Lecture de l'ent�te du fichier

% [flag, DataHeader] = SSc_ReadDatagrams(nomFic, 'Ssc_Header');
% if ~flag
%     return
% end
% 
% sublBab = (DataHeader.Side(:) == 0);
% sublTri = (DataHeader.Side(:) == 1);
% T2 = DataHeader.DateHeure(:);
% DatetimeBab = datetime(T2(sublBab), 'ConvertFrom', 'datenum');
% DatetimeTri = datetime(T2(sublTri), 'ConvertFrom', 'datenum');
% NPings = max(length(DatetimeBab), length(DatetimeTri));
% Time(1:sum(sublTri),1) = T2(sublTri);
% Time(1:sum(sublBab),1) = T2(sublBab);

%% Signal Tide

% Data.Time       = Time;
% Data.Height     = NaN(NPings, 1, 'single');
% Data.Tide       = NaN(NPings, 1, 'single');
% Data.HeightType = zeros(NPings, 1, 'uint8'); % TODO : traiter tribord aussi

%% G�n�ration du XML SonarScope

Info.Title              = 'Height';
Info.Constructor        = 'Kongsberg';
Info.EmModel            = 'Geoswath';
% Info.SystemSerialNumber = Datagrams.SystemSerialNumber;
Info.NbSamples          = NPings;
Info.TimeOrigin         = '01/01/-4713';
Info.Comments           = 'Sensor sampling rate';
% Info.FormatVersion      = FormatVersion;

Info.Dimensions.NbSamples = NPings;

Info.Signals(1).Name          = 'Time';
Info.Signals(end).Dimensions  = 'NbSamples, 1'; % TODO : traiter tribord aussi
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = 'days since JC';
Info.Signals(end).FileName    = fullfile('Ssc_Height','Time.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'Height';
Info.Signals(end).Dimensions  = 'NbSamples, 1'; % TODO : traiter tribord aussi
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile('Ssc_Height','Height.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'HeightType';
Info.Signals(end).Dimensions  = 'NbSamples, 1'; % TODO : traiter tribord aussi
Info.Signals(end).Storage     = 'uint8';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_Height','HeightType.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'Tide';
Info.Signals(end).Dimensions  = 'NbSamples, 1'; % TODO : traiter tribord aussi
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_Height','Tide.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

%% Cr�ation du fichier XML d�crivant la donn�e

nomFicXml = fullfile(nomDirRacine, 'Ssc_Height.xml');
xml_write(nomFicXml, Info);
flag = exist(nomFicXml, 'file');
if ~flag
    messageErreur(nomFicXml)
    return
end

%% Cr�ation du r�pertoire Height

nomDirHeight = fullfile(nomDirRacine, 'Ssc_Height');
if ~exist(nomDirHeight, 'dir')
    status = mkdir(nomDirHeight);
    if ~status
        messageErreur(nomDirHeight)
        flag = 0;
        return
    end
end

%% Cr�ation des fichiers binaires des signaux

for k=1:length(Info.Signals)
    flag = writeSignal(nomDirRacine, Info.Signals(k), Data.(Info.Signals(k).Name));
    if ~flag
        return
    end
end

flag = 1;
