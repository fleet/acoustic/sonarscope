function flag = rdf2ssc_MiniSVSString(nomDirRacine, Header)

%% Lecture du fichier XML d�crivant la donn�e 

nomFicXml = fullfile(nomDirRacine, 'RDF_MiniSVSString.xml');
flag = exist(nomFicXml, 'file');        
if ~flag
    special_message('FichierNonExistant', nomFicXml)
    return
end
% edit(nomFicXml)
Datagrams = xml_read(nomFicXml);

[nomDirSignal, nomFicSignal] = fileparts(nomFicXml);
nomDirSignal = fullfile(nomDirSignal, nomFicSignal);
            
%% Lecture de MiniSVS String

[flag, Data.MiniSVSString] = Read_BinTable(Datagrams, nomDirSignal, 'MiniSVSString', 'uint8', sum(Header.MiniSVSStringSize));
if ~flag
    return
end

% Pr�paration de la cha�ne � copier dans le ssc.bin
maxSize = max(Header.MiniSVSStringSize);
Data.MiniSVSString = zeros(Datagrams.NbDatagrams, maxSize, 'uint8');
sub = 0;
for k=1:Datagrams.NbDatagrams
    if Header.MiniSVSStringSize(k)~= 0
        sub = sub(end) + (1:Header.MiniSVSStringSize(k));
        Data.MiniSVSString(k,1:Header.MiniSVSStringSize(k)) = Data.MiniSVSString(sub);
    end
end

%% G�n�ration du XML SonarScope

Info.Title       = Datagrams.Title; % Entete du fichier
Info.Constructor = Datagrams.Constructor;
Info.TimeOrigin  = Datagrams.TimeOrigin;
Info.Comments    = Datagrams.Comments;

Info.Dimensions.NbSamples           = Datagrams.NbDatagrams;
Info.Dimensions.MiniSVSStringLength = maxSize;

Info.Images(1).Name         = 'MiniSVSString';
Info.Images(end).Dimensions = 'NbSamples, MiniSVSStringLength';
Info.Images(end).Storage    = 'uint8';
Info.Images(end).Unit       = [];
Info.Images(end).FileName   = fullfile('Ssc_MiniSVSString','MiniSVSString.bin');
Info.Images(end).Tag        = verifKeyWord('TODO');

%% Cr�ation du fichier XML d�crivant la donn�e 

nomFicXml = fullfile(nomDirRacine, 'Ssc_MiniSVSString.xml');
xml_write(nomFicXml, Info);
flag = exist(nomFicXml, 'file');
if ~flag
    messageErreur(nomFicXml)
    return
end

%% Cr�ation du r�pertoire File MiniSVSString

nomDirSscData = fullfile(nomDirRacine, 'Ssc_MiniSVSString');
if ~exist(nomDirSscData, 'dir')
    status = mkdir(nomDirSscData);
    if ~status
        messageErreur(nomDirSscData)
        flag = 0;
        return
    end
end

%% Cr�ation des fichiers binaires des images

for k=1:length(Info.Images)
    flag = writeImage(nomDirRacine, Info.Images(k), Data.(Info.Images(k).Name));
    if ~flag
        return
    end
end

flag = 1;
