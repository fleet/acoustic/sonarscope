function flag = rdf2ssc_Navigation(nomDirRacine, Header)

%% Lecture du fichier XML d�crivant la donn�e 

nomFicXml = fullfile(nomDirRacine, 'RDF_Navigation.xml');
flag = exist(nomFicXml, 'file');        
if ~flag
    special_message('FichierNonExistant', nomFicXml)
    return
end
% edit(nomFicXml)
Datagrams = xml_read(nomFicXml);

[nomDirSignal, nomFicSignal] = fileparts(nomFicXml);
nomDirSignal = fullfile(nomDirSignal, nomFicSignal);

NbSamples = sum(Header.NavigationNumber);
            
%% Lecture de x

[flag, Data.x] = Read_BinTable(Datagrams, nomDirSignal, 'x', 'single', NbSamples);
if ~flag
    return
end
% figure; plot(Data.x); grid on; title('x');

%% Lecture de y

[flag, Data.y] = Read_BinTable(Datagrams, nomDirSignal, 'y', 'single', NbSamples);
if ~flag
    return
end
% figure; plot(Data.y); grid on; title('y');
% figure; plot(Data.x, Data.y); grid on; title('y');
% figure; plot(Data.y, Data.x); grid on; title('y');
% TODO : pb rencontr� avec donn�es Haliotis : abscisses attendues de l'ordre de 1.16*1e5 mais on lit des donn�es de l'ordre de 1.5*1.e5 dans les fichiers

%% Lecture de z

[flag, Data.z] = Read_BinTable(Datagrams, nomDirSignal, 'z', 'single', NbSamples);
if ~flag
    return
end
% figure; plot(Data.z); grid on; title('z');

%% Lecture du time

[flag, Data.time] = Read_BinTable(Datagrams, nomDirSignal, 'time', 'double', NbSamples); 
if ~flag
    return
end
% figure; plot(Data.time); grid on; title('time');

%% Lecture du timestamp

[flag, Data.timestamp] = Read_BinTable(Datagrams, nomDirSignal, 'timestamp', 'double', NbSamples); 
if ~flag
    return
end
% figure; plot(Data.timestamp); grid on; title('timestamp');

%% Lecture du quality

[flag, Data.quality] = Read_BinTable(Datagrams, nomDirSignal, 'quality', 'double', NbSamples); 
if ~flag
    return
end
% figure; plot(Data.quality); grid on; title('quality');

%% G�n�ration du XML SonarScope

Info.Title       = Datagrams.Title; % Entete du fichier
Info.Constructor = Datagrams.Constructor;
Info.TimeOrigin  = Datagrams.TimeOrigin;
Info.Comments    = Datagrams.Comments;

Info.Dimensions.NbSamples = NbSamples;

Info.Signals(1).Name          = 'x';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_Navigation','x.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'y';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_Navigation','y.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'z';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_Navigation','z.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'time';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_Navigation','time.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'timestamp';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_Navigation','timestamp.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'quality';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint8';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_Navigation','quality.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

%% Cr�ation du fichier XML d�crivant la donn�e 

nomFicXml = fullfile(nomDirRacine, 'Ssc_Navigation.xml');
xml_write(nomFicXml, Info);
flag = exist(nomFicXml, 'file');
if ~flag
    messageErreur(nomFicXml)
    return
end

%% Cr�ation du r�pertoire File Navigation

nomDirSscData = fullfile(nomDirRacine, 'Ssc_Navigation');
if ~exist(nomDirSscData, 'dir')
    status = mkdir(nomDirSscData);
    if ~status
        messageErreur(nomDirSscData)
        flag = 0;
        return
    end
end

%% Cr�ation des fichiers binaires des signaux

for k=1:length(Info.Signals)
    flag = writeSignal(nomDirRacine, Info.Signals(k), Data.(Info.Signals(k).Name));
    if ~flag
        return
    end
end

flag = 1;
