function flag = rdf2ssc_NavigationString(nomDirRacine, Header)

%% Lecture du fichier XML d�crivant la donn�e 

nomFicXml = fullfile(nomDirRacine, 'RDF_NavString.xml');
flag = exist(nomFicXml, 'file');        
if ~flag
    special_message('FichierNonExistant', nomFicXml)
    return
end
% edit(nomFicXml)
Datagrams = xml_read(nomFicXml);

[nomDirSignal, nomFicSignal] = fileparts(nomFicXml);
nomDirSignal = fullfile(nomDirSignal, nomFicSignal);
            
%% Lecture de Navigation String

[flag, NavigationString] = Read_BinTable(Datagrams, nomDirSignal, 'NavigationString', 'char', sum(Header.NavigationStringSize));
if ~flag
    return
end

% Pr�paration de la cha�ne � copier dans le ssc.bin
maxSize = max(Header.NavigationStringSize);
subEnr = find(Header.NavigationStringSize ~= 0);
nbEnrNavigationString = length(subEnr);
Data.NavigationString = repmat(char(0), nbEnrNavigationString, maxSize);
% zeros(Datagrams.NbDatagrams, maxSize, 'char');
sub = 0;
for k=1:nbEnrNavigationString
    k2 = subEnr(k);
    sub = sub(end) + (1:Header.NavigationStringSize(k2));
    Data.NavigationString(k,1:Header.NavigationStringSize(k2)) = NavigationString(sub);
end

if nbEnrNavigationString    
    [flag, Heure, Lat, Lon, GPS, ~] = read_Navigation_NMEA(Data.NavigationString);
    if ~flag
        return
    end

    Data.Lat    = Lat;
    Data.Lon    = Lon;
    Data.H      = Heure;
    Data.QualityIndicator              = [GPS(:).QualityIndicator]';
    Data.NumberSatellitesUsed          = [GPS(:).NumberSatellitesUsed]';
    Data.HorizontalDilutionOfPrecision = [GPS(:).HorizontalDilutionOfPrecision]';
    Data.AltitudeOfIMU                 = [GPS(:).AltitudeOfIMU]';
    Data.GeoidalSeparation             = [GPS(:).GeoidalSeparation]';
    Data.AgeOfDifferentialCorrections  = [GPS(:).AgeOfDifferentialCorrections]';
    Data.DGPSRefStationIdentity        = [GPS(:).DGPSRefStationIdentity]';
end

%% G�n�ration du XML SonarScope

Info.Title       = Datagrams.Title; % Entete du fichier
Info.Constructor = Datagrams.Constructor;
Info.TimeOrigin  = Datagrams.TimeOrigin;
Info.Comments    = Datagrams.Comments;

Info.Dimensions.NbSamples              = Datagrams.NbDatagrams;
Info.Dimensions.NavigationStringLength = maxSize;

if isfield(Data, 'NumberSatellitesUsed')
    Info.Signals(1).Name      = 'QualityIndicator';
    Info.Signals(end).Dimensions  = 'NbSamples, 1';
    Info.Signals(end).Storage     = 'single';
    Info.Signals(end).Unit        = '';
    Info.Signals(end).FileName    = fullfile('Ssc_NavigationString','QualityIndicator.bin');
    Info.Signals(end).Tag         = verifKeyWord('GPSQualityIndicator');

    Info.Signals(end+1).Name      = 'NumberSatellitesUsed';
    Info.Signals(end).Dimensions  = 'NbSamples, 1';
    Info.Signals(end).Storage     = 'single';
    Info.Signals(end).Unit        = '';
    Info.Signals(end).FileName    = fullfile('Ssc_NavigationString','NumberSatellitesUsed.bin');
    Info.Signals(end).Tag         = verifKeyWord('GPSNumberSatellitesUsed');

    Info.Signals(end+1).Name      = 'HorizontalDilutionOfPrecision';
    Info.Signals(end).Dimensions  = 'NbSamples, 1';
    Info.Signals(end).Storage     = 'single';
    Info.Signals(end).Unit        = '';
    Info.Signals(end).FileName    = fullfile('Ssc_NavigationString','HorizontalDilutionOfPrecision.bin');
    Info.Signals(end).Tag         = verifKeyWord('GPSHDOP');

    Info.Signals(end+1).Name      = 'AltitudeOfIMU';
    Info.Signals(end).Dimensions  = 'NbSamples, 1';
    Info.Signals(end).Storage     = 'single';
    Info.Signals(end).Unit        = 'm';
    Info.Signals(end).FileName    = fullfile('Ssc_NavigationString','AltitudeOfIMU.bin');
    Info.Signals(end).Tag         = verifKeyWord('GPSAltitudeOfMIU');

    Info.Signals(end+1).Name      = 'GeoidalSeparation';
    Info.Signals(end).Dimensions  = 'NbSamples, 1';
    Info.Signals(end).Storage     = 'single';
    Info.Signals(end).Unit        = 'm';
    Info.Signals(end).FileName    = fullfile('Ssc_NavigationString','GeoidalSeparation.bin');
    Info.Signals(end).Tag         = verifKeyWord('GPSGeoidalSeparation');

    Info.Signals(end+1).Name      = 'AgeOfDifferentialCorrections';
    Info.Signals(end).Dimensions  = 'NbSamples, 1';
    Info.Signals(end).Storage     = 'single';
    Info.Signals(end).Unit        = 's';
    Info.Signals(end).FileName    = fullfile('Ssc_NavigationString','AgeOfDifferentialCorrections.bin');
    Info.Signals(end).Tag         = verifKeyWord('GPSAgeOfDifferentialCorrections');
end

Info.Images(1).Name          = 'NavigationString';
Info.Images(end).Dimensions  = 'NbSamples,NavigationStringLength';
Info.Images(end).Storage     = 'char';
Info.Images(end).Unit        = [];
Info.Images(end).FileName    = fullfile('Ssc_NavigationString','NavigationString.bin');
Info.Images(end).Tag         = verifKeyWord('TODO');

if isfield(Data, 'NumberSatellitesUsed')
    Info.Strings(1).Name          = 'DGPSRefStationIdentity';
    Info.Strings(end).Dimensions  = 'NbSamples, 1';
    Info.Strings(end).Storage     = 'char';
    Info.Strings(end).Unit        = 'deg';
    Info.Strings(end).FileName    = fullfile('Ssc_NavigationString','DGPSRefStationIdentity.bin');
    Info.Strings(end).Tag         = verifKeyWord('GPSDGPSRefStationIdentity');
end

%% Cr�ation du fichier XML d�crivant la donn�e 

nomFicXml = fullfile(nomDirRacine, 'Ssc_NavigationString.xml');
xml_write(nomFicXml, Info);
flag = exist(nomFicXml, 'file');
if ~flag
    messageErreur(nomFicXml)
    return
end

%% Cr�ation du r�pertoire File NavigationString

nomDirSscData = fullfile(nomDirRacine, 'Ssc_NavigationString');
if ~exist(nomDirSscData, 'dir')
    status = mkdir(nomDirSscData);
    if ~status
        messageErreur(nomDirSscData)
        flag = 0;
        return
    end
end

%% Cr�ation des fichiers binaires des signaux

for k=1:length(Info.Signals)
    flag = writeSignal(nomDirRacine, Info.Signals(k), Data.(Info.Signals(k).Name));
    if ~flag
        return
    end
end

%% Cr�ation des fichiers binaires des images

for k=1:length(Info.Images)
    flag = writeImage(nomDirRacine, Info.Images(k), Data.(Info.Images(k).Name));
    if ~flag
        return
    end
end
flag = 1;

%% Cr�ation des fichiers binaires des strings

if isfield(Info, 'Strings')
    for k=1:length(Info.Strings)
        flag = writeString(nomDirRacine, Info.Strings(k), Data.(Info.Strings(k).Name));
        if ~flag
            return
        end
    end
end

%{
    nGPS = RDFPingHeader.NavigationNumber;
    if nGPS ~= 0
        LonGPS = zeros(1, nGPS, 'double');
        LatGPS = zeros(1, nGPS, 'double');
        zGPS = zeros(1, nGPS, 'single');
        timeGPS = zeros(1, nGPS, 'double');
        
        for k2=1:nGPS
            str = RDFPingData.NavigationStrings(k2,:);
            [flag, HeureGPS, Lat, Lon, Height] = read_Navigation_NMEA(str);
            if isempty(Lat)
                break
            end
            LonGPS(k2)  = Lon;
            LatGPS(k2)  = Lat;
            zGPS(k2)    = Height;
            timeGPS(k2) = HeureGPS;
        end
        DataGPS.Time = [DataGPS.Time timeGPS];
        DataGPS.Lon = [DataGPS.Lon LonGPS];
        DataGPS.Lat = [DataGPS.Lat LatGPS];
        DataGPS.z = [DataGPS.z zGPS];
    end
%}