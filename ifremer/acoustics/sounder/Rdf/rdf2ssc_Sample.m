function flag = rdf2ssc_Sample(nomDirRacine, Header, DataMiniSVS)

nomFicXml = fullfile(nomDirRacine, 'RDF_Sample.xml');
% SonarFmt2ssc_message(nomFicXml, '.rdf')

nbTotalSamples = sum(Header.SampleNumber);

%% Lecture du fichier XML d�crivant la donn�e 

flag = exist(nomFicXml, 'file');        
if ~flag
    special_message('FichierNonExistant', nomFicXml)
    return
end
% edit(nomFicXml)
Datagrams = xml_read(nomFicXml);

[nomDirSignal, nomFicSignal] = fileparts(nomFicXml);
nomDirSignal = fullfile(nomDirSignal, nomFicSignal);
                   
%% Lecture de Time

[flag, Time] = Read_BinTable(Datagrams, nomDirSignal, 'Time', 'uint16', nbTotalSamples);
if ~flag
    return
end
% figure; plot(Data.Time); grid on; title('Heading');

%% Lecture du Sine

[flag, Sine] = Read_BinTable(Datagrams, nomDirSignal, 'Sine', 'single', nbTotalSamples); 
if ~flag
    return
end
% figure; plot(Data.Sine); grid on; title('timestamp');

%% Lecture du Amplitude

[flag, Amplitude] = Read_BinTable(Datagrams, nomDirSignal, 'Amplitude', 'single', nbTotalSamples); 
if ~flag
    return
end
% figure; plot(Data.Amplitude); grid on; title('timestamp');


%% Pr�paratifs

Time      = RDF_formatImage(Time,      Header.SampleNumber);
Sine      = RDF_formatImage(Sine,      Header.SampleNumber);
Amplitude = RDF_formatImage(Amplitude, Header.SampleNumber);

% Masquage des 4 bits de poids fort car ce sont des bits de masquage
Amplitude = mod(Amplitude, 2^12);

sublBab = find(Header.Side(:) == 0);
sublTri = find(Header.Side(:) == 1);
nbPings = max(length(sublBab), length(sublTri));

% Factor = 5;
Factor = median(diff(Time(1,:)), 'omitnan');
Milieu = 2 + ceil(double(max(Time(:)) / Factor));
nbCol = 2*Milieu-1;

Data.Mask         = zeros(nbPings, nbCol, 'uint8');
Data.TetaRaw      = NaN(nbPings, nbCol, 'single');
Data.TetaProc     = NaN(nbPings, nbCol, 'single');
Data.Reflectivity = NaN(nbPings, nbCol, 'single');
Data.Depth        = NaN(nbPings, nbCol, 'single');
Data.AcrossDist   = NaN(nbPings, nbCol, 'single');
Data.AlongDist    = NaN(nbPings, nbCol, 'single');

sublBabOut = 1:length(sublBab);
sublTriOut = 1:length(sublTri);

Data.Height    = NaN(nbPings, 2, 'single');
Data.Heading   = NaN(nbPings, 2, 'single');
Data.Latitude  = NaN(nbPings, 2);
Data.Longitude = NaN(nbPings, 2);

TMiniSVS = datetime(DataMiniSVS.timestamp(:), 'ConvertFrom', 'posixtime');
% figure; plot(TMiniSVS, DataMiniSVS.velocity(:)); grid;

TImage = datetime(Header.DateHeure, 'ConvertFrom', 'datenum');
CeleriteSurface = my_interp1(TMiniSVS, DataMiniSVS.velocity(:), TImage, 'nearest', 'extrap');
% figure; plot(TImage, CeleriteSurface); grid;

% str1 = 'Mise en forme des �chantillons b�bord du Geoswath.';
% str2 = 'Creating port side for Geoswath data.';
N = length(sublBab);
% hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
%     my_waitbar(k, N, hw)
    kData = sublBabOut(k);
    index = 1 + floor(Time(sublBab(k),:) / Factor);
    subNaN = isnan(index);
    index(subNaN) = [];
    if ~isempty(index)
        subc = Milieu - index;
        Data.Mask(kData, subc) = 1;
        SinTeta = Sine(sublBab(k), ~subNaN) * (CeleriteSurface(k) / (1500 * 32768));
        SinTeta(SinTeta > 1) = NaN;
        
        % TODO : Il a fallu renvoyer tout appel � asind apr�s les boucles
        % for car �a ralentissait �norm�ment le temps d'�x�cution.
        % Apparemment "asind" casse le Just In Time Accelerator !!!
        
%         Teta = asind(SinTeta);
% %         %     figure; plot(asind(Sine(sublBab(k), ~subNaN) / 32768), 'b'); grid on; hold on; plot(Teta, 'r')
%         Data.TetaRaw(     kData, subc) = (Teta - 60);
        Data.TetaRaw(     kData, subc) = SinTeta;
        Data.Reflectivity(kData, subc) = reflec_Amp2dB(Amplitude(sublBab(k), ~subNaN));
    end
end
% my_close(hw);
 
% str1 = 'Mise en forme des �chantillons tribord du Geoswath.';
% str2 = 'Creating starboard side for Geoswath data.';
N = length(sublTri);
% hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
%     my_waitbar(k, N, hw)
    kData = sublTri(k);
    index = floor(Time(kData,:) / Factor);
    subNaN = isnan(index);
    index(subNaN) = [];
    if ~isempty(index)
        subc = Milieu + index;
        Data.Mask(sublTriOut(k), subc) = 1;
%         SinTeta = Sine(kData,~subNaN) / 32768;
%         SinTeta = SinTeta * CeleriteSurface(k) / 1500;
        SinTeta = Sine(kData,~subNaN) * (CeleriteSurface(k) / (1500 * 32768));
        SinTeta = SinTeta * CeleriteSurface(k) / 1500;
        SinTeta(SinTeta > 1) = NaN;
%         Teta = asind(SinTeta);
%         Data.TetaRaw(     sublTriOut(k), subc) = -(Teta - 60);
        Data.TetaRaw(     sublTriOut(k), subc) = SinTeta;
        Data.Reflectivity(sublTriOut(k), subc) = reflec_Amp2dB(Amplitude(kData,~subNaN));
    end
end
% my_close(hw);


% TODO : Il a fallu renvoyer tout appel � asind apr�s les boucles
% for car �a ralentissait �norm�ment le temps d'�x�cution.
% Apparemment "asind" casse le Just In Time Accelerator !!!

subBab = 1:Milieu;
subTri = (Milieu+1:size(Data.TetaRaw,2));
Data.TetaRaw(:, subBab) =  asind(Data.TetaRaw(:, subBab)) - 60;
Data.TetaRaw(:, subTri) = -asind(Data.TetaRaw(:, subTri)) + 60;

% Data.Reflectivity = fillNaN_mean(Data.Reflectivity, [3 3]);

%% G�n�ration du XML SonarScope

Info.Title            = Datagrams.Title; % Entete du fichier
Info.Constructor      = Datagrams.Constructor;
Info.TimeOrigin       = Datagrams.TimeOrigin;
Info.Comments         = Datagrams.Comments;
Info.NbSamplesPerSide = Milieu;

Info.Dimensions.nbPings = nbPings;
Info.Dimensions.nbCol   = nbCol;

Info.Signals(1).Name          = 'Height';
Info.Signals(end).Dimensions  = 'nbPings,2';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'm';
Info.Signals(end).FileName    = fullfile('Ssc_Sample','Height.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'Latitude';
Info.Signals(end).Dimensions  = 'nbPings,2';
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile('Ssc_Sample','Latitude.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'Longitude';
Info.Signals(end).Dimensions  = 'nbPings,2';
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile('Ssc_Sample','Longitude.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'Heading';
Info.Signals(end).Dimensions  = 'nbPings,2';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile('Ssc_Sample','Heading.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Images(1).Name          = 'Mask';
Info.Images(end).Dimensions  = 'nbPings,nbCol';
Info.Images(end).Storage     = 'uint8';
Info.Images(end).Unit        = [];
Info.Images(end).FileName    = fullfile('Ssc_Sample','Mask.bin');
Info.Images(end).Tag         = verifKeyWord('TODO');

Info.Images(end+1).Name      = 'TetaRaw';
Info.Images(end).Dimensions  = 'nbPings,nbCol';
Info.Images(end).Storage     = 'single';
Info.Images(end).Unit        = 'deg';
Info.Images(end).FileName    = fullfile('Ssc_Sample','Sine.bin');
Info.Images(end).Tag         = verifKeyWord('TODO');

Info.Images(end+1).Name      = 'TetaProc';
Info.Images(end).Dimensions  = 'nbPings,nbCol';
Info.Images(end).Storage     = 'single';
Info.Images(end).Unit        = 'deg';
Info.Images(end).FileName    = fullfile('Ssc_Sample','SineModified.bin');
Info.Images(end).Tag         = verifKeyWord('TODO');

Info.Images(end+1).Name      = 'Reflectivity';
Info.Images(end).Dimensions  = 'nbPings,nbCol';
Info.Images(end).Storage     = 'single';
Info.Images(end).Unit        = 'dB';
Info.Images(end).FileName    = fullfile('Ssc_Sample','Amplitude.bin');
Info.Images(end).Tag         = verifKeyWord('TODO');

Info.Images(end+1).Name      = 'Depth';
Info.Images(end).Dimensions  = 'nbPings,nbCol';
Info.Images(end).Storage     = 'single';
Info.Images(end).Unit        = 'm';
Info.Images(end).FileName    = fullfile('Ssc_Sample','Depth.bin');
Info.Images(end).Tag         = verifKeyWord('TODO');

Info.Images(end+1).Name      = 'AcrossDist';
Info.Images(end).Dimensions  = 'nbPings,nbCol';
Info.Images(end).Storage     = 'single';
Info.Images(end).Unit        = 'm';
Info.Images(end).FileName    = fullfile('Ssc_Sample','AcrossDist.bin');
Info.Images(end).Tag         = verifKeyWord('TODO');

Info.Images(end+1).Name      = 'AlongDist';
Info.Images(end).Dimensions  = 'nbPings,nbCol';
Info.Images(end).Storage     = 'single';
Info.Images(end).Unit        = 'm';
Info.Images(end).FileName    = fullfile('Ssc_Sample','AlongDist.bin');
Info.Images(end).Tag         = verifKeyWord('TODO');

%% Cr�ation du fichier XML d�crivant la donn�e 

nomFicXml = fullfile(nomDirRacine, 'Ssc_Sample.xml');
xml_write(nomFicXml, Info);
flag = exist(nomFicXml, 'file');
if ~flag
    messageErreurFichier(nomFicXml);
    return
end

%% Cr�ation du r�pertoire File Heading

nomDirSscData = fullfile(nomDirRacine, 'Ssc_Sample');
if ~exist(nomDirSscData, 'dir')
    status = mkdir(nomDirSscData);
    if ~status
        messageErreur(nomDirSscData)
        flag = 0;
        return
    end
end

%% Cr�ation des fichiers binaires des signaux

for k=1:length(Info.Signals)
    flag = writeSignal(nomDirRacine, Info.Signals(k), Data.(Info.Signals(k).Name));
    if ~flag
        return
    end
end

%% Cr�ation des fichiers binaires des images

for k=1:length(Info.Images)
    Image = Data.(Info.Images(k).Name);
    flag = writeSignal(nomDirRacine, Info.Images(k), Image);
    if ~flag
        return
    end
end

flag = 1;
