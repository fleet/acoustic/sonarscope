function flag = rdf2ssc_Sample_OLD(nomDirRacine, Header)

nbTotalSamples = sum(Header.SampleNumber);

%% Lecture du fichier XML d�crivant la donn�e 

nomFicXml = fullfile(nomDirRacine, 'RDF_Sample.xml');
flag = exist(nomFicXml, 'file');        
if ~flag
    special_message('FichierNonExistant', nomFicXml)
    return
end
% edit(nomFicXml)
Datagrams = xml_read(nomFicXml);

[nomDirSignal, nomFicSignal] = fileparts(nomFicXml);
nomDirSignal = fullfile(nomDirSignal, nomFicSignal);
                   
%% Lecture de Time

[flag, Data.Time] = Read_BinTable(Datagrams, nomDirSignal, 'Time', 'single', nbTotalSamples);
if ~flag
    return
end
% figure; plot(Data.Time); grid on; title('Heading');

%% Lecture du Sine

[flag, Data.Sine] = Read_BinTable(Datagrams, nomDirSignal, 'Sine', 'double', nbTotalSamples); 
if ~flag
    return
end
% figure; plot(Data.Sine); grid on; title('timestamp');

%% Lecture du Amplitude

[flag, Data.Amplitude] = Read_BinTable(Datagrams, nomDirSignal, 'Amplitude', 'double', nbTotalSamples); 
if ~flag
    return
end
% figure; plot(Data.Amplitude); grid on; title('timestamp');

%% G�n�ration du XML SonarScope

Info.Title                  = Datagrams.Title; % Entete du fichier
Info.Constructor            = Datagrams.Constructor;
% Info.EmModel                = Datagrams.Model;
% Info.SystemSerialNumber     = Datagrams.SystemSerialNumber;
Info.TimeOrigin             = Datagrams.TimeOrigin;
Info.Comments               = Datagrams.Comments;

Info.Dimensions.nbPings     = Datagrams.NbDatagrams;
Info.Dimensions.nbSamples   = max(Header.SampleNumber);

Info.Images(1).Name          = 'Time';
Info.Images(end).Dimensions  = 'nbPings,nbSamples';
Info.Images(end).Storage     = 'single';
Info.Images(end).Unit        = [];
Info.Images(end).FileName    = fullfile('Ssc_Sample','Time.bin');
Info.Images(end).Tag         = verifKeyWord('TODO');

Info.Images(end+1).Name      = 'Sine';
Info.Images(end).Dimensions  = 'nbPings,nbSamples';
Info.Images(end).Storage     = 'double';
Info.Images(end).Unit        = [];
Info.Images(end).FileName    = fullfile('Ssc_Sample','Sine.bin');
Info.Images(end).Tag         = verifKeyWord('TODO');

Info.Images(end+1).Name      = 'Amplitude';
Info.Images(end).Dimensions  = 'nbPings,nbSamples';
Info.Images(end).Storage     = 'double';
Info.Images(end).Unit        = [];
Info.Images(end).FileName    = fullfile('Ssc_Sample','Amplitude.bin');
Info.Images(end).Tag         = verifKeyWord('TODO');


%% Cr�ation du fichier XML d�crivant la donn�e 

nomFicXml = fullfile(nomDirRacine, 'Ssc_Sample.xml');
xml_write(nomFicXml, Info);
flag = exist(nomFicXml, 'file');
if ~flag
    messageErreur(nomFicXml)
    return
end

%% Cr�ation du r�pertoire File Heading

nomDirSscData = fullfile(nomDirRacine, 'Ssc_Sample');
if ~exist(nomDirSscData, 'dir')
    status = mkdir(nomDirSscData);
    if ~status
        messageErreur(nomDirSscData)
        flag = 0;
        return
    end
end

%% Cr�ation des fichiers binaires des signaux

for i=1:length(Info.Images)
    Image = Data.(Info.Images(i).Name);
    Image = RDF_formatImage(Image, Header.SampleNumber);
    % SonarScope(Image(1:2:end,:))
    % SonarScope(Image(2:2:end,:))
    flag = writeSignal(nomDirRacine, Info.Images(i), Image);
    if ~flag
        return
    end
end

flag = 1;
