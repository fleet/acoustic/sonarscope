function flag = Remus2Ers(ListeFic)

%% Conversion des fichiers

Carto = [];
nbFiles = length(ListeFic);
str1 = 'Inspection des fichiers';
str2 = 'Checking the files';
hw = create_waitbar(Lang(str1,str2), 'N', nbFiles);
for k=1:nbFiles
    my_waitbar(k, nbFiles, hw);
    
    [nomDir, nomFic] = fileparts(ListeFic{k});
    nomFicTif = fullfile(nomDir, [nomFic '.tif']);
    if ~exist(nomFicTif, 'file')
        str1 = sprintf('Pas de fichier ".tif" trouv� pour %s', ListeFic{k});
        str2 = sprintf('No ".tif" found for %s', ListeFic{k});
        my_warndlg(Lang(str1,str2), 0, 'Tag', 'TifFileNotFound');
        continue
    end

    [flag, Carto] = sonar_convertRemus_unitaire(ListeFic{k}, nomFicTif, Carto);
end
my_close(hw, 'MsgEnd');


function [flag, Carto] = sonar_convertRemus_unitaire(nomFicCsv, nomFicTif, Carto)

flag = 0;

fid = fopen(nomFicCsv);
if fid == -1
    messageErreurFichier(nomFicCsv, 'ReadFailure');
    return
end

header = fgetl(fid); %#ok<NASGU>

Lat     = zeros(1000,1);
Lon     = zeros(1000,1);
date    = zeros(1000,1);
heure   = zeros(1000,1);
Height  = zeros(1000,1);
Heading = zeros(1000,1);
Speed   = zeros(1000,1);
Roll    = zeros(1000,1);
Pitch   = zeros(1000,1);
Yaw     = zeros(1000,1);

kPing = 1;
while 1
    tline = fgetl(fid);
    if ~ischar(tline)
        break
    end
    
    mots = regexp(tline, ',', 'split');

    % mots{1}  : PingID
    % mots{2}  : Time[yyyy/MM/dd HH:mm:ss.hs]
    % mots{3}  : Latitude[dec deg]
    % mots{4}  : Longitude[dec deg]
    % mots{5}  : Smoothed Latitude[dec deg]
    % mots{6}  : Smoothed Longitude[dec deg]
    % mots{7}  : Northing [m]
    % mots{8}  : Easting [m]
    % mots{9}  : Heading[deg]
    % mots{10} : Sin Heading
    % mots{11} : Cos Heading
    % mots{12} : Speed[kts]
    % mots{13} : Altitude[m]
    % mots{14} : Depth[m]
    % mots{15} : SlantRange[m]
    % mots{16} : Roll[deg]
    % mots{17} : Pitch[deg]
    % mots{18} : Yaw[deg]
    
    Lat(kPing) = str2double(mots{3});
    Lon(kPing) = str2double(mots{4});
    
    %2014/11/13 11:19:15.23
    
    mots{2} = rmblank(mots{2});
    Annee = str2double(mots{2}(1:4));
    Mois  = str2double(mots{2}(6:7));
    Jour  = str2double(mots{2}(9:10));
    date(kPing)  = dayJma2Ifr(Jour, Mois, Annee);
    
    HH = str2double(mots{2}(12:13));
    MM = str2double(mots{2}(15:16));
    SS = str2double(mots{2}(18:end));
	heure(kPing) = hourHms2Ifr(HH, MM, SS);
    
    Heading(kPing) = str2double(mots{9});
    Speed(kPing)   = str2double(mots{12}); % kts TODO : convertir en m/s ?
    Height(kPing)  = str2double(mots{14}); % Depth (m)
    Roll(kPing)    = str2double(mots{16});
    Pitch(kPing)   = str2double(mots{17});
    Yaw(kPing)     = str2double(mots{18});
    
    kPing = kPing + 1;
end

fclose(fid);
Lat(kPing:end)     = [];
Lon(kPing:end)     = [];
date(kPing:end)    = [];
heure(kPing:end)   = [];
Height(kPing:end)  = [];
Heading(kPing:end) = [];
Speed(kPing:end)   = [];
Roll(kPing:end)    = [];
Pitch(kPing:end)   = [];
Yaw(kPing:end)     = [];

Time = cl_time('timeIfr', date, heure);

[nomDir, nomFic] = fileparts(nomFicCsv);

Sidescan = imread(nomFicTif);
[nbRows, n] = size(Sidescan);
n2 = n/2;
Sidescan = [Sidescan(:,1:n2) zeros(nbRows,1,class(Sidescan)) Sidescan(:, (n2+1):end)];
Sidescan = flipud(Sidescan);
Sidescan = reflec_Amp2dB(single(Sidescan));
x = -n2:n2;
y = 1:nbRows;
resol = 0.1; % TODO
SonarDescription = cl_sounder('Sonar.Ident', 5);
Immersion = zeros(nbRows, 1);

a = cl_image('Image',      Sidescan, ...
    'Name',           nomFic, ...
    'Unit',                'dB', ...
    'x',                    x, ...
    'y',                    y, ...
    'XUnit',               'Sample', ...
    'YUnit',               'Ping', ...
    'ColormapIndex',        2, ...
    'DataType',             cl_image.indDataType('Reflectivity'), ...
    'TagSynchroX',          [nomFic ' - D'], ...
    'TagSynchroY',          [nomFic ' - Y'], ...
    'GeometryType',         cl_image.indGeometryType('PingSamples'), ...
    'SonarDescription',     SonarDescription, ...
    'SonarRawDataResol',    resol, ...
    'SonarResolutionD',     resol, ...
    'SonarResolutionX',     resol, ...
    'InitialFileFormat',   'SonarSAR');

% a = set(a, 'SonarPingNumber',    y);
a = set(a, 'SonarPingCounter',   y(:));
a = set(a, 'SonarImmersion',     Immersion);
a = set(a, 'SonarHeight',        Height);
a = set(a, 'SonarHeading',       Heading);
a = set(a, 'SonarSpeed',         Speed);
a = set(a, 'SonarFishLatitude',  Lat);
a = set(a, 'SonarFishLongitude', Lon);
a = set(a, 'SonarPortMode_1',    ones(nbRows,1));
a = set(a, 'SonarPortMode_2',    ones(nbRows,1));

% TODO : bidouille en attendant de savoir ce qu'est la fr�quence
% d'�chantillonnage du Remus

SampFreq = get(SonarDescription, 'Proc.SampFreq');
a = set(a, 'SampleFrequency', ones(nbRows,1) * SampFreq * 5);

a = set(a, 'SonarTime', Time);
% a = set(a, 'SonarCableOut', X);
a = set(a, 'SonarRoll',  Roll);
a = set(a, 'SonarPitch', Pitch);
a = set(a, 'SonarYaw',   Yaw);

a = set(a, 'SonarTime', Time);
a = update_Name(a);

if isempty(Carto)
    Carto = getDefinitionCarto(a, 'BypassCartoFolder', 1);
end
a = set_Carto(a, Carto);

% nomFicErs = fullfile(nomDir, [a.Name '.ers']);
nomFicXML = fullfile(nomDir, [a.Name '.xml']);

a.InitialFileName = nomFicXML;

%% R�up�ration des signaux trait�s : temporaire  

if exist(nomFicXML, 'file')
    [flag, b] = cl_image.import_xml(nomFicXML, 'Memmapfile', 0);
    if flag
        Height = get(b, 'SonarHeight');
        a = set(a, 'SonarHeight', Height);
        Lat = get(b, 'SonarFishLatitude');
        a = set(a, 'SonarFishLatitude',  Lat);
        Lon = get(b, 'SonarFishLongitude');
        a = set(a, 'SonarFishLongitude', Lon);
    end
else
    a = sonar_lateral_hauteur(a, 'subx', (n2+15):(n2+140), 'FiltreSonar',  [2 0.1], 'FiltreProfile', [2 0]);  
%     Height = get(a, 'SonarHeight');
%     Height(:) = 0;
%     a = set(a, 'SonarHeight', Height);
end
% export_ermapper(a, nomFicErs);
a = Virtualmemory2Ram(a);
flag = export_xml(a, nomFicXML);
if ~flag
    messageErreurFichier(nomFicXML, 'WriteFailure');
end

% SonarScope(a)

%{
SonarTVG_etat                 = 1;
SonarTVG_origine              = 2;
SonarTVG_ConstructTypeCompens = 2;
SonarTVG_ConstructTable       = []; % Definition a repporter dans cl_sounder
SonarTVG_ConstructAlpha       = 0;
SonarTVG_ConstructConstante   = 0;
SonarTVG_ConstructCoefDiverg  = 0;

SonarDiagEmi_etat                   = 2;
SonarDiagEmi_origine                = 1;
SonarDiagEmi_ConstructTypeCompens   = 1;

SonarDiagRec_etat                   = 2;
SonarDiagRec_origine                = 1;
SonarDiagRec_ConstructTypeCompens   = 1;

Sonar_NE_etat = 2;
Sonar_SH_etat = 2;
Sonar_GT_etat = 2;

SonarBS_etat = 2;  % Image Not compensatede

a = set(a,  'Sonar_DefinitionENCours', ...
    'SonarTVG_etat',                        SonarTVG_etat, ...
    'SonarTVG_origine',                     SonarTVG_origine, ...
    'SonarTVG_ConstructTypeCompens',        SonarTVG_ConstructTypeCompens, ...
    'SonarTVG_ConstructTable',              SonarTVG_ConstructTable, ...
    'SonarTVG_ConstructAlpha',              SonarTVG_ConstructAlpha, ...
    'SonarTVG_ConstructConstante',          SonarTVG_ConstructConstante, ...
    'SonarTVG_ConstructCoefDiverg',         SonarTVG_ConstructCoefDiverg, ...
    'SonarDiagEmi_etat',                    SonarDiagEmi_etat, ...
    'SonarDiagEmi_origine',                 SonarDiagEmi_origine, ...
    'SonarDiagEmi_ConstructTypeCompens',    SonarDiagEmi_ConstructTypeCompens, ...
    'SonarDiagRec_etat',                    SonarDiagRec_etat, ...
    'SonarDiagRec_origine',                 SonarDiagRec_origine, ...
    'SonarDiagRec_ConstructTypeCompens',    SonarDiagRec_ConstructTypeCompens, ...
    'Sonar_NE_etat',                        Sonar_NE_etat, ...
    'Sonar_SH_etat',                        Sonar_SH_etat, ...
    'Sonar_GT_etat',                        Sonar_GT_etat, ...
    'SonarBS_etat',                         SonarBS_etat);
%}

flag = 1;
