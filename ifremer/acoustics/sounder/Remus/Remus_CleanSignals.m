function flag = Remus_CleanSignals(nomFic, varargin)

if ~iscell(nomFic)
    nomFic = {nomFic};
end

Format1 = 'V�rification de la %s.\n\nUne fen�tre SignalScope va s''ouvrir, le signal de %s est en noir et une version filtr�e est en rouge. Zoomez sur le signal pour voir si la version filtr�e est meilleure (elle rem�die souvent � des marches d''escalier dues � une quantification de la mesure de positionnemen).\n\nSi vous conservez le filtre et que vous clickez sur Save et Quit c''est lui qui sera sauvegard�.';
Format2 = 'Checking %s.\n\nA SignalScope window is going to be open, %s signal is the black one, the red one is a filtered version. Zoom on the signal to check if the filter is efficient (it often solves problems of stair steps due to signal quantification).\n\nIf you conserve the filter AND you click on Save and Quit il will be the red curve which will be saved.';
Format3 = 'V�rification de la %s.\n\nUne fen�tre SignalScope va s''ouvrir. Zoomez sur le signal pour voir si un filtrage s''impose.';
Format4 = 'Checking %s.\n\nA SignalScope window is going to be opened. Zoom on the signal to check if a filter is necessary.';

repComputeHeight = 1;
repCheckNav      = 1;
repCheckHeading  = 1;
repCheckTime     = 1;
fistTimeFilterHeight  = 1;
fistTimeFilterNav     = 1;
fistTimeFilterHeading = 1;
fistTimeFilterTime    = 1;

%% Traitement des fichiers

N = length(nomFic);
str1 = 'Contr�le qualit� des signaux des fichiers PingSample Ers';
str2 = 'PingSample Ers Quality Control processing.';
strList1 = {'Oui'; 'Non'; 'Oui et ne plus poser la question'; 'Non et ne plus poser la question'};
strList2 = {'Yes'; 'No'; 'Yes and never ask me the question anymore'; 'No and never ask me the question anymore'};

hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
    my_waitbar(k, N, hw);
    
    fprintf('%d/%d : %s\n', k, N, nomFic{k});
    
    flagModif = 0;
    
    [~, ~, Ext] = fileparts(nomFic{k});
    switch Ext
        case '.ers'
            [flag, a] = cl_image.import_ermapper(nomFic{k}, 'Memmapfile', 0);
        case '.xml'
            [flag, a] = cl_image.import_xml(nomFic{k}, 'Memmapfile', 0);
    end
    if ~flag
        str = sprintf('ErMapper importation failure for %s', nomFic{k});
        my_warndlg(str, 1);
        continue
    end
    
    Longitude  = get(a, 'SonarFishLongitude');
    Latitude   = get(a, 'SonarFishLatitude');
    SonarTime  = get(a, 'SonarTime');
%     Immersion  = get(a, 'SonarImmersion');
    Heading    = get(a, 'SonarHeading');
    
    %% Affichage de l'image dans SonarScope pour v�rifier la hauteur
    
    if repComputeHeight < 3
        str1 = sprintf('Voulez-vous recalculer ou nettoyer le signal de hauteur du fichier "%s" ?', nomFic{k});
        str2 = sprintf('Do you want to reprocess or modify the "Height" for file "%s" ?', nomFic{k});
        %[rep, flag] = my_questdlg(Lang(str1,str2));
        [repComputeHeight, flag] = my_listdlg(Lang(str1,str2), Lang(strList1,strList2), 'InitialValue', repComputeHeight);
        if ~flag
            return
        end
    end
    if (repComputeHeight == 1) || (repComputeHeight == 3)
        if fistTimeFilterHeight
            messageCheckHeight();
            fistTimeFilterHeight = 0;
        end
        
        c = SonarScope(a, 'visuProfilY', 1, 'profilY.Type', 'Height', 'visuProfilX', 1, 'ButtonSave', 1);
        Height = get(c(1), 'SonarHeight');
        
        %{
        flag = save_signal(nomFic{k}, 'Ssc_Sar', 'Height', SonarHeight);
        if ~flag
            return
        end
        %}
        
        a = set(a, 'SonarHeight', Height);
        flagModif = 1;
        %export_ermapper(a, nomFic{k});
    end
    
    %% Contr�le de la latitude et de la longitude du bateau
    
    [~, nomFicSeul] = fileparts(nomFic{k});
    
    if repCheckNav < 3
        str1 = sprintf('Voulez-vous v�rifier la latitude du poisson pour "%s" ?', nomFicSeul);
        str2 = sprintf('Check latitude and longitude of the fish for "%s" ?', nomFicSeul);
%         [repCheckNav, flag] = my_questdlg(Lang(str1,str2));
        [repCheckNav, flag] = my_listdlg(Lang(str1,str2), Lang(strList1,strList2), 'InitialValue', repCheckNav);
        if ~flag
            return
        end
    end
    if (repCheckNav == 1) || (repCheckNav == 3)
        FiltreLonLat.Type = 4;
        
        if fistTimeFilterNav
            str1 = sprintf(Format1, 'Longitude', 'Longitude');
            str2 = sprintf(Format2, 'Longitude', 'Longitude');
            my_warndlg(Lang(str1,str2), 1);
        end
        [Longitude, FiltreLonLat] = controleSignalNEW('FishLongitude', Longitude, 'Filtre', FiltreLonLat);
        
        if fistTimeFilterNav
            str1 = sprintf(Format1, 'Latitude', 'Latitude');
            str2 = sprintf(Format2, 'Latitude', 'Latitude');
            my_warndlg(Lang(str1,str2), 1);
            fistTimeFilterNav = 0;
        end
        Latitude = controleSignalNEW('FishLatitude', Latitude, 'Filtre', FiltreLonLat);
        
        a = set(a, 'SonarFishLongitude', Longitude);
        a = set(a, 'SonarFishLatitude',  Latitude);
        flagModif = 1;
    end
    
    %% Contr�le du cap calcul�
    
    if repCheckHeading < 3
        str1 = sprintf('Voulez-vous v�rifier le cap du poisson pour "%s" ?', nomFicSeul);
        str2 = sprintf('Check the fish heading for "%s" ?', nomFicSeul);
%         [repCheckHeading, flag] = my_questdlg(Lang(str1,str2));
        [repCheckHeading, flag] = my_listdlg(Lang(str1,str2), Lang(strList1,strList2), 'InitialValue', repCheckHeading);
        if ~flag
            return
        end
    end
    if (repCheckHeading == 1) || (repCheckHeading == 3)
        FiltreHeading.Type  = 4;
        if fistTimeFilterHeading
            str1 = sprintf(Format3, 'Heading', 'Heading');
            str2 = sprintf(Format4, 'Heading', 'Heading');
            my_warndlg(Lang(str1,str2), 1);
            fistTimeFilterHeading = 0;
        end
        [Heading, FiltreHeading] = controleSignalNEW('Heading', Heading, 'Filtre', FiltreHeading);
        
        str1 = 'Que voulez-vous faire de ce cap ?';
        str2 = 'What do you want to do with this heading ?';
        str3 = 'L''tiliser tel que';
        str4 = 'Use it as is';
        str5 = 'Le recalculer � partir de la navigation';
        str6 = 'Compute from the navigation';
        [rep, flag] = my_questdlg(Lang(str1,str2), Lang(str3,str4), Lang(str5,str6));
        if ~flag
            return
        end
        if rep == 2
            Heading = calCapFromLatLon(FishLatitude, FishLongitude, 'Time', SonarTime);
            Heading = controleSignalNEW('Heading', Heading, 'Filtre', FiltreHeading);
        end
        
        a = set(a, 'SonarHeading', Heading);
        flagModif = 1;
    end
    
    %% Contr�le de l'heure
    
    if repCheckTime < 3
        str1 = sprintf('Voulez-vous v�rifier l''heure pour "%s" ?', nomFicSeul);
        str2 = sprintf('Check the time for "%s" ?', nomFicSeul);
%         [repCheckTime, flag] = my_questdlg(Lang(str1,str2));
        [repCheckTime, flag] = my_listdlg(Lang(str1,str2), Lang(strList1,strList2), 'InitialValue', repCheckTime);
        if ~flag
            return
        end
    end
    if (repCheckTime == 1) || (repCheckTime == 3)
        if fistTimeFilterTime
            str1 = sprintf(Format3, 'Time', 'Time');
            str2 = sprintf(Format4, 'Time', 'Time');
            my_warndlg(Lang(str1,str2), 1);
            fistTimeFilterTime = 0;
        end
        timeMat = controleSignalNEW('SonarTime', SonarTime.timeMat);
        
        a = set(a, 'SonarTime', cl_time('timeMat', timeMat));
        flagModif = 1;
    end
    
    %% Sauvegarde des modifications
    
    if flagModif
        switch Ext
            case '.ers'
                export_ermapper(a, nomFic{k});
            case '.xml'
                export_xml(a, nomFic{k});
        end
    end
    
    %% On demande si on veut traiter les autres fichiers
    
    if k ~= N
        flag = question_ContinueNextFile();
        if ~flag
            break
        end
    end
end
my_close(hw, 'MsgEnd')
