%  % Phase_A : OK
%  nomDirPDS      = 'R:\USAN-General\PDS_LEG1';
%  nomFicCarto    = 'R:\USAN-General\Carto.def';
%  nomFicIn       = 'M:\USAN\Phase_A\Bulles_A\Phase_A_pings_beams.txt';
%  nomDir7KCenter = 'M:\USAN\Phase_A\7KCenter_A';
%  nomFicOut      = 'M:\USAN\Phase_A\Bulles_A\Phase_A_pings_beams_Complete.csv';
%  flag = CalculCoordPourTotal(cl_reson_s7k.empty(), nomDir7KCenter, nomDirPDS, nomFicIn, nomFicOut, nomFicCarto)
%
%  % Phase_B : OK
%  nomDirPDS      = 'R:\USAN-General\PDS_LEG2';
%  nomFicCarto    = 'R:\USAN-General\Carto.def';
%  nomFicIn       = 'P:\USAN\Phase_B\Bulles_B\Phase_B_pings_beams.txt';
%  nomDir7KCenter = 'P:\USAN\Phase_B\7KCenter_B';
%  nomFicOut      = 'P:\USAN\Phase_B\Bulles_B\Phase_B_pings_beams_Complete.csv';
%  flag = CalculCoordPourTotal(cl_reson_s7k.empty(), nomDir7KCenter, nomDirPDS, nomFicIn, nomFicOut, nomFicCarto)
%
%  % Phase_C : 12 points non apair�s : voir ErreurApairagePings.m
%  nomDirPDS      = 'R:\USAN-General\PDS_LEG2';
%  nomFicCarto    = 'R:\USAN-General\Carto.def';
%  nomFicIn       = 'P:\USAN\Phase_C\Bulles_C\Phase_C_pings_beams.txt';
%  nomDir7KCenter = 'P:\USAN\Phase_C\7KCenter_C';
%  nomFicOut      = 'P:\USAN\Phase_C\Bulles_C\Phase_C_pings_beams_Complete.csv';
%  flag = CalculCoordPourTotal(cl_reson_s7k.empty(), nomDir7KCenter, nomDirPDS, nomFicIn, nomFicOut, nomFicCarto)
%
%  % Phase_D1 : 
%  nomDirPDS      = 'R:\USAN-General\PDS_LEG2';
%  nomFicCarto    = 'R:\USAN-General\Carto.def';
%  nomFicIn       = 'P:\USAN\Phase_D1\Bulles_D1\Phase_D1_pings_beams.txt';
%  nomDir7KCenter = 'P:\USAN\Phase_D1\7KCenter_D1';
%  nomFicOut      = 'P:\USAN\Phase_D1\Bulles_D1\Phase_D1_pings_beams_Complete.csv';
%  flag = CalculCoordPourTotal(cl_reson_s7k.empty(), nomDir7KCenter, nomDirPDS, nomFicIn, nomFicOut, nomFicCarto)
%
%  % Phase_D2 : 
%  nomDirPDS      = 'R:\USAN-General\PDS_LEG2';
%  nomFicCarto    = 'R:\USAN-General\Carto.def';
%  nomFicIn       = 'P:\USAN\Phase_D2\Bulles_D2\Phase_D2_pings_beamsJMA.txt';
%  nomDir7KCenter = 'P:\USAN\Phase_D2\7KCenter_D2';
%  nomFicOut      = 'P:\USAN\Phase_D2\Bulles_D2\Phase_D2_pings_beams_Complete.csv';
%  flag = CalculCoordPourTotal(cl_reson_s7k.empty(), nomDir7KCenter, nomDirPDS, nomFicIn, nomFicOut, nomFicCarto)
%
%  % Phase_D3 : 
%  nomDirPDS      = 'R:\USAN-General\PDS_LEG2';
%  nomFicCarto    = 'R:\USAN-General\Carto.def';
%  nomFicIn       = 'R:\USAN\Phase_D3\Bulles_D3\Phase_D3_pings_beams.txt';
%  nomDir7KCenter = 'R:\USAN\Phase_D3\7KCenter_D3';
%  nomFicOut      = 'R:\USAN\Phase_D3\Bulles_D3\Phase_D3_pings_beams_Complete.csv';
%  flag = CalculCoordPourTotal(cl_reson_s7k.empty(), nomDir7KCenter, nomDirPDS, nomFicIn, nomFicOut, nomFicCarto)
%
%
% % Phase_E : 9 points non apair�s : voir ErreurApairagePings.m
%  nomDirPDS      = 'R:\USAN-General\PDS_LEG2';
%  nomFicCarto    = 'R:\USAN-General\Carto.def';
%  nomFicIn       = 'M:\USAN\Phase_E\Bulles_E\Phase_E_pings_beams.txt';
%  nomDir7KCenter = 'M:\USAN\Phase_E\7KCenter_E';
%  nomFicOut      = 'M:\USAN\Phase_E\Bulles_E\Phase_E_pings_beams_Complete.csv';
%  flag = CalculCoordPourTotal(cl_reson_s7k.empty(), nomDir7KCenter, nomDirPDS, nomFicIn, nomFicOut, nomFicCarto)
%
% % Phase_F : 1 point pas trouv� : voir ErreurApairagePings.m
%  nomDirPDS      = 'R:\USAN-General\PDS_LEG2';
%  nomFicCarto    = 'R:\USAN-General\Carto.def';
%  nomFicIn       = 'R:\USAN\Phase_F\Bulles_F\Phase_F_pings_beams.txt';
%  nomDir7KCenter = 'R:\USAN\Phase_F\7KCenter_F';
%  nomFicOut      = 'R:\USAN\Phase_F\Bulles_F\Phase_F_pings_beams_Complete.csv';
%  flag = CalculCoordPourTotal(cl_reson_s7k.empty(), nomDir7KCenter, nomDirPDS, nomFicIn, nomFicOut, nomFicCarto)
%


function flag = CalculCoordPourTotal(~, nomDir7KCenter, nomDirPDS, nomFicIn, nomFicOut, nomFicCarto)

flag = 0;

% Test existence du fichier

if ~exist(nomFicIn, 'file')
    str1 = sprintf('Le fichier "%s" n''existe pas.', nomFicIn);
    str2 = sprintf('File "%s" does not exist.', nomFicIn);
    my_warndlg(Lang(str1,str2), 1);
    return
end

if ~exist(nomDir7KCenter, 'dir')
    str1 = sprintf('Le r�pertoire "%s" n''existe pas.', nomDir7KCenter);
    str2 = sprintf('Directory "%s" does not exist.', nomDir7KCenter);
    my_warndlg(Lang(str1,str2), 1);
    return
end

%% Fichier de carto

Carto = import_xml(cl_carto([]), nomFicCarto);
if isempty(Carto)
    Carto = import(cl_carto([]), nomFicCarto);
    if isempty(Carto)
         messageErreurFichier(nomFicCarto, 'ReadFailure');
        return
    end
    flag = export_xml(Carto, nomFicCarto);
    if flag
        return
    end
end

%% Liste des fichiers PDS

% listeFicPDS = listeFicOnDir(nomDirPDS, '.s7k');
[flag, listeFicPDS] = uiSelectFiles('ExtensionFiles', '.s7k', 'RepDefaut', nomDirPDS);
if ~flag
    return
end
if isempty(listeFicPDS)
    str1 = sprintf('Pas de fichier PDS trouv� sur %s ', nomDirPDS);
    str2 = sprintf('No PDS file found on %s ', nomDirPDS);
    my_warndlg(Lang(str1,str2), 1);
    return
end
for k=1:length(listeFicPDS)
    b(k) = cl_reson_s7k('nomFic', listeFicPDS{k}); %#ok<AGROW>
end

%% Lecture du fichier

nomFic7KCenterPrecedent = '';
S7kCenter = [];
fid = fopen(nomFicIn);
if fid == -1
    messageErreurFichier(nomFicIn, 'WriteFailure');
    return
end
while 1
    tline = fgetl(fid);
    if ~ischar(tline)
        break
    end
    fprintf('%s\n', tline);
    tline = rmblank(tline);
    if isempty(tline) || strcmp(tline(1), '%')
        continue
    end
    mots = strsplit(tline, ';');
    if length(mots) == 1
        mots = strsplit(tline);
    end
    if length(mots) < 5
        str = sprintf('La ligne "%s" est incompl�te, je l''ignore.', tline);
        fprintf(1,'%s\n', str);
        my_warndlg(str, 0, 'Tag', 'LigneIncomplete');
        continue
    end
    nomDir = mots{1};
    nomFic = mots{2};
    iPing  = str2double(mots{3});
    iBeam  = str2double(mots{4});
    iQual  = str2double(mots{5});
    
    nomFic7KCenter = fullfile(nomDir7KCenter, nomDir, [nomFic '.s7k']);
    
    if ~exist(nomFic7KCenter, 'file')
        str = sprintf('%s n''existe pas', nomFic7KCenter);
        my_warndlg(str, 1);
        fclose(fid);
        return
    end
    
    
    if ~strcmp(nomFic7KCenter, nomFic7KCenterPrecedent)
        a = cl_reson_s7k('nomFic', nomFic7KCenter);
        if isempty(a)
            str = sprintf('%s n''existe pas', nomFic7KCenter);
            my_warndlg(str, 1);
            fclose(fid);
            return
        end
        [flag, DataDepth] = read_depth_s7k(a);
        nomFic7KCenterPrecedent = nomFic7KCenter;
    end
    
    T = DataDepth.Time.timeMat;
    S7kCenter(end+1).Time           = T(iPing); %#ok<AGROW>
    S7kCenter(end).nomProfil        = mots{1};
    S7kCenter(end).nomFic7KCenter	= mots{2};
    S7kCenter(end).PingCounter      = DataDepth.PingCounter(iPing);
    S7kCenter(end).PingSequence     = DataDepth.MultiPingSequence(iPing);
    S7kCenter(end).iPing            = iPing;
    S7kCenter(end).iBeam            = iBeam;
    t = cl_time('timeMat', T(iPing));
    S7kCenter(end).strTime          = t2str(t);
    %         pppp = datestr(T(iPing), FormatHeure)
    S7kCenter(end).iQual            = iQual;
    S7kCenter(end).Range            = DataDepth.Range(iPing, :);
    %         FigUtils.createSScFigure; PlotUtils.createSScPlot(S7kCenter.Range, 'r'); grid on
end
fclose(fid);

%% Calcul des sondes

rechercheDansFichiersPDS(b, S7kCenter, nomFicOut, Carto)

flag = 1;

str1 = 'Le calcul des positions des pieds de panache est termin�';
str2 = 'The processing of gas plume position is over.';
my_warndlg(Lang(str1,str2), 0);

