function [flag, Data, Carto] = ComputeNorbitBathymetry(this, Carto, varargin)

global useCacheNetcdf %#ok<GVMIS>

[varargin, DataIndex]    = getPropertyValue(varargin, 'DataIndex',    []);
[varargin, identSondeur] = getPropertyValue(varargin, 'identSondeur', []); %#ok<ASGLU>

[c, Carto] = view_depth(this, 'ListeLayers', [4 6], 'Carto', Carto, 'ComputeBathymetryIfNeeded', false, ...
    'identSondeur', identSondeur, 'DataIndex', DataIndex);

Data = [];

[flag, indLayerRange] = findAnyLayerRange(c, 1, 'WithCurrentImage', 'AtLeastOneLayer');
if ~flag
    return
end

indLayerAngle = findIndLayerSonar(c, 1, 'strDataType', 'TxAngle', 'WithCurrentImage', 1, 'OnlyOneLayer');
if isempty(indLayerAngle)
    flag = 0;
    return
end

[flag, b] = sonar_range2depth_EM(c(indLayerRange), c(indLayerAngle), [], [], []);
if ~flag
    return
end

indLayerBathymetry = findIndLayerSonar(b, 1, 'strDataType', 'Bathymetry', 'WithCurrentImage', 1, 'OnlyOneLayer');
indLayerAcrossDist = findIndLayerSonar(b, 1, 'strDataType', 'AcrossDist', 'WithCurrentImage', 1, 'OnlyOneLayer');
indLayerAlongDist  = findIndLayerSonar(b, 1, 'strDataType', 'AlongDist',  'WithCurrentImage', 1, 'OnlyOneLayer');

if ~isempty(identSondeur)
    identSondeur = selectionSondeur(this);
end
[flag, Data] = read_depth_s7k(this, 'identSondeur', identSondeur, 'DataIndex', DataIndex);
if ~flag
    return
end

Data.AcrossDist = get_Image(b(indLayerAcrossDist));
Data.AlongDist  = get_Image(b(indLayerAlongDist));
Data.Depth      = get_Image(b(indLayerBathymetry));

[nomDir, nomFic] = fileparts(this.nomFic);
nomFicMat  = fullfile(nomDir, 'SonarScope', [num2str(identSondeur) '_' nomFic '_depth.mat']);
ncFileName = fullfile(nomDir, 'SonarScope', [nomFic '.nc']);

if useCacheNetcdf == 1 % 1=Mat, 2 et 3=Netcdf
    save(nomFicMat, 'Data')
else
    [Data.Dimensions.nbPings, Data.Dimensions.nbBeams] = size(Data.Range);
    NetcdfUtils.Struct2Netcdf(Data, ncFileName, 'Depth');
end
