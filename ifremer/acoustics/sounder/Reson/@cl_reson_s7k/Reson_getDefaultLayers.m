% ListeLayers = Reson_getDefaultLayers
% ListeLayers = Reson_getDefaultLayers('RxBeamAngle')

function ListeLayers = Reson_getDefaultLayers(this, varargin)

[varargin, nomLayer] = getPropertyValue(varargin, 'nomLayer', []); %#ok<ASGLU>

if ischar(nomLayer)
    nomLayer = {nomLayer};
end

[str, Unit, Commentaire] = getListeLayers(this);

ListeLayers = [];
for k1=1:length(nomLayer)
    for k2=1:length(str)
        flag = strcmp(str{k2}, nomLayer{k1});
        if flag
            ListeLayers(end+1) = k2; %#ok<AGROW>
        end
    end
end