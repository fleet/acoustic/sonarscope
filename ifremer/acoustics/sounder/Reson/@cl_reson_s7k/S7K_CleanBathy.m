function S7K_CleanBathy(~, listeFic)

global useCacheNetcdf %#ok<GVMIS>

Bits = [1 2]; % Brihtness et Colinearity seulement

if ~iscell(listeFic)
    listeFic = {listeFic};
end

%% Traitement des fichiers

N = length(listeFic);
str1 = 'Nettoyage des donn�es S7K bas� sur la bathym�trie.';
str2 = 'S7K Data cleaning based on Bathy.';
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
    my_waitbar(k, N, hw);
    
    str1 = sprintf('Traitement du fichier %d/%d : "%s" en cours.', k, N, listeFic{k});
    str2 = sprintf('Processing file %d/%d : "%s".', k, N, listeFic{k});
    fprintf(1,'%s\n', Lang(str1,str2));
    
    [nomDir, nomFic] = fileparts(listeFic{k});

    %% Pr�caution car soft mal branl�. A supprimer apr�s int�gration travaux Roger
    
	flag = prepare_lectures7k_step1(listeFic{k});
    if ~flag
        continue
    end

    %% Lecture de la structure
    
    a = cl_reson_s7k('nomFic', listeFic{k}, 'KeepNcID', true);
    if isempty(a)
        continue
    end
    
    %% Lecture des layers
    
    [c, ~, ~, Data, identSondeur]  = view_depth(a, 'ListeLayers', 12); % Modif JMA le 18/02/2021
    if isempty(c)
        continue
    end
    
    %% Lancement de SonarScope
    
    c = SonarScope(c, 'ButtonSave', 1); %, 'TypeInteractionSouris', 9);
    
    %% R�cup�ration du masquage
    
    Mask = isnan(c(end));
    
    %% Rapport dans le masque
    
    Data.Mask(Mask) = 0; 
%     Data.Depth(Mask) = NaN; % TODO : Corriger le masque plut�t que la bathy !!!
    
    %% Ecriture du masque
    
    nomFicMat  = fullfile(nomDir, 'SonarScope', [num2str(identSondeur) '_' nomFic '_depth.mat']);
    ncFileName = fullfile(nomDir, 'SonarScope', [nomFic '.nc']);
    try
        if useCacheNetcdf == 1 % 1=Mat, 2 et 3=Netcdf
            save(nomFicMat, 'Data')
        else
            [Data.Dimensions.nbPings, Data.Dimensions.nbBeams] = size(Data.BeamDepressionAngle);
            NetcdfUtils.Struct2Netcdf(Data, ncFileName, 'Depth');
        end
    catch
        str = sprintf('Erreur �criture fichier %s', nomFicMat);
        my_warndlg(str, 1);
    end
    
    %% On demande si on veut traiter les autres fichiers
    
    if k ~= N
        flag = question_ContinueNextFile();
        if ~flag
            break
        end
    end
end
my_close(hw, 'MsgEnd')


function flag = save_signalSidescan(this, nomFic, NomLayer, X)

identSondeur = selectionSondeur(this);
[nomFicSidescan, flag] = ResonSidescanPDSNames(nomFic, identSondeur);
if ~flag
    str1 = sprintf('Les signaux n''ont pas pu �tre sauv�s pour "%s"', nomFic);
    str2 = sprintf('The signals could not be saved for "%s"', nomFic);
    my_warndlg(Lang(str1,str2), 1);
    return
end

[nomDirErs, nomFicErs] = fileparts(nomFicSidescan.ReflectivityErs);
nomFicXml = fullfile(nomDirErs, [nomFicErs '.xml']);
if ~exist(nomFicXml, 'file')
    messageErreurFichier(nomFicXml, 'ReadFailure');
    str1 = sprintf('Les signaux n''ont pas pu �tre sauv�s dans "%s"', nomFicXml);
    str2 = sprintf('The signals could not be saved in "%s"', nomFicXml);
    my_warndlg(Lang(str1,str2), 1);
    flag = 0;
    return
end
Info = xml_mat_read(nomFicXml);


kSignal = find(strcmp({Info.Signals.Name}, NomLayer));
if length(kSignal) ~= 1
    str1 = sprintf('Variable "%s" non trouv�e dans "%s"', NomLayer, nomFicXml);
    str2 = sprintf('Variable "%s" not found in "%s"', NomLayer, nomFicXml);
    my_warndlg(Lang(str1,str2), 1);
    flag = 0;
    return
end

flag = writeSignal(nomDirErs, Info.Signals(kSignal), X);
