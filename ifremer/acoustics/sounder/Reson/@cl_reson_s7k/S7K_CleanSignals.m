function S7K_CleanSignals(~, nomFic)

FiltreHeight.Type  = 2;
FiltreHeight.Ordre = 2;
FiltreHeight.Wc    = 0.2;

FiltreImmersion.Type  = 2;
FiltreImmersion.Ordre = 2;
FiltreImmersion.Wc    = 0.2;

if ~iscell(nomFic)
    nomFic = {nomFic};
end

%% Traitement des fichiers

N = length(nomFic);
hw = create_waitbar('S7K Quality Control processing.', 'N', N);
for k=1:N
    my_waitbar(k, N, hw);
    
    str1 = sprintf('Traitement du fichier %d/%d : "%s" en cours.', k, N, nomFic{k});
    str2 = sprintf('Processing file %d/%d : "%s".', k, N, nomFic{k});
    fprintf(1,'%s\n', Lang(str1,str2));
    
    %% Pr�caution car soft mal branl�? a supprimer apr�s int�gration travaux Roger
    
    flag = prepare_lectures7k_step1(nomFic{k});
    if ~flag
        return
    end
    
    %% V�rification de l'existence du fichier Status
    
    a = cl_reson_s7k('nomFic', nomFic{k}, 'KeepNcID', true);
    if isempty(a)
        continue
    end
    
    %% V�rification de la hauteur et de l'immersion
    
    b = view_SidescanImagePDS(a);
    if isempty(b)
        continue
    end
    
    Height = get(b, 'Height');
%     Height = flipud(Height(:)); % Ne pas supprimer car cl_memmapfile
    [Height, FiltreHeight] = controleSignalNEW('Height', Height, 'Filtre', FiltreHeight);
    flag = save_signalSidescan(a, nomFic{k}, 'Height', flipud(Height));
    if ~flag
        str1 = sprintf('Le signal "Height" n''a pas pu �tre sauv� pour "%s"', nomFic{k});
        str2 = sprintf('"Height" coud not be save for "%s"',  nomFic{k});
        my_warndlg(Lang(str1,str2), 1);
    end 
        
    Immersion = get(b, 'Immersion');
%     Immersion = flipud(Immersion(:)); % Ne pas supprimer car cl_memmapfile
    [Immersion, FiltreImmersion] = controleSignalNEW('Immersion', Immersion, 'Filtre', FiltreImmersion);
    flag = save_signalSidescan(a, nomFic{k}, 'Immersion', flipud(Immersion));
    if ~flag
        str1 = sprintf('Le signal "Immersion" n''a pas pu �tre sauv� pour "%s"', nomFic{k});
        str2 = sprintf('"Immersion" coud not be save for "%s"',  nomFic{k});
        my_warndlg(Lang(str1,str2), 1);
    end 
    
    %{
    if k == 1
        messageCheckHeight();
    end
    b(1) = SonarScope(b, 'visuProfilY', 1, 'profilY.Type', 'Height', 'visuProfilX', 1, 'ButtonSave', 1);
    
    Height    = get(b, 'Height');
    Immersion = get(b, 'Immersion');

    %% Save the signals
    
    % Attention bug si l'image Sidescan est ouverte dans SSSc
    flag = save_signalSidescan(a, nomFic{k}, 'Height',    Height);
    if ~flag
        str1 = sprintf('Le signal "Height" n''a pas pu �tre sauv� pour "%s"',  nomFic{k});
        str2  =sprintf('"Height" coud not be save for "%s"',  nomFic{k});
        my_warndlg(Lang(str1,str2), 1);
    end
    flag = save_signalSidescan(a, nomFic{k}, 'Immersion', Immersion);
    if ~flag
        str1 = sprintf('Le signal "Immersion" n''a pas pu �tre sauv� pour "%s"',  nomFic{k});
        str2  =sprintf('"Immersion" coud not be save for "%s"',  nomFic{k});
        my_warndlg(Lang(str1,str2), 1);
    end
    %}
    
    %% Cr�ation du fichier de status du fichier
    
%     Info.FiltreTime      = FiltreTime;
%     Info.FiltreNav       = FiltreNav;
%     Info.FiltreImmersion = FiltreImmersion;
%     Info.FiltreCableOut  = FiltreCableOut;
%     Info.FiltreSpeed     = FiltreSpeed;
%     Info.FiltreDelay     = FiltreDelay;
%     Info.FiltreHeading   = FiltreHeading;
%     Info.FiltreHeave     = FiltreHeave;
%     xml_write(nomFicXmlStatus, Info);
%     flag = exist(nomFicXmlStatus, 'file');
%     if ~flag
%         messageErreur(nomFicXmlStatus)
%     end
    
    %% On demande si on veut traiter les autres fichiers
    
    if k ~= N
        flag = question_ContinueNextFile();
        if ~flag
            break
        end
    end
end
my_close(hw, 'MsgEnd')



function flag = save_signalSidescan(this, nomFic, NomLayer, X)

identSondeur = selectionSondeur(this);
[nomFicSidescan, flag] = ResonSidescanPDSNames(nomFic, identSondeur);
if ~flag
    str1 = sprintf('Les signaux n''ont pas pu �tre sauv�s pour "%s"', nomFic);
    str2 = sprintf('The signals could not be saved for "%s"', nomFic);
    my_warndlg(Lang(str1,str2), 1);
    return
end

[nomDirErs, nomFicErs] = fileparts(nomFicSidescan.ReflectivityErs);
nomFicXml = fullfile(nomDirErs, [nomFicErs '.xml']);
if ~exist(nomFicXml, 'file')
    messageErreurFichier(nomFicXml, 'ReadFailure');
    str1 = sprintf('Les signaux n''ont pas pu �tre sauv�s dans "%s"', nomFicXml);
    str2 = sprintf('The signals could not be saved in "%s"', nomFicXml);
    my_warndlg(Lang(str1,str2), 1);
    flag = 0;
    return
end
Info = xml_mat_read(nomFicXml);


kSignal = find(strcmp({Info.Signals.Name}, NomLayer));
if length(kSignal) ~= 1
    str1 = sprintf('Variable "%s" non trouv�e dans "%s"', NomLayer, nomFicXml);
    str2 = sprintf('Variable "%s" not found in "%s"', NomLayer, nomFicXml);
    my_warndlg(Lang(str1,str2), 1);
    flag = 0;
    return
end

flag = writeSignal(nomDirErs, Info.Signals(kSignal), X);
