function flag  = S7K_EditNav(~, listeFic)

global useCacheNetcdf %#ok<GVMIS>

if ischar(listeFic)
    listeFic = {listeFic};
end

S0 = cl_reson_s7k.empty();

nav = ClNavigation.empty();
N = length(listeFic);
str1 = 'Edition de la navigation.';
str2 = 'Edit navigation';
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    
    %% Instantiation de cl_resen_s7k
    
    s7k(k) = cl_reson_s7k('nomFic', listeFic{k}, 'KeepNcID', true); %#ok<AGROW>
    if isempty(s7k(k))
        continue
    end
    
    [flag, DataDepth, identSondeur] = read_depth_s7k(s7k(k));
    if ~flag
        continue
    end
    
    %% Lecture de la navigation
    
    Data = read_navigation(s7k(k), identSondeur);
    if isempty(Data)
        continue
    end
    
	[SonarTime, Latitude, Longitude, VesselHeading, CourseVessel, VesselHeight, provenance] = getNavFromVariousDatagrams(S0, Data, DataDepth);

%     Latitude      = Data.Latitude(:,:);
%     Longitude     = Data.Longitude(:,:);
%     VesselHeight  = Data.VesselHeight(:);
%     VesselHeading = Data.Heading(:);
%     Time          = Data.Time.timeMat;
    Time  = SonarTime.timeMat;
    Speed = Data.Speed(:);
%     CourseVessel               = Data.CourseVessel(:);
    if isfield(Data, 'HeightAccuracy')
        HeightAccuracy  = Data.HeightAccuracy(:);
    else
        HeightAccuracy = [];
    end
    if isfield(Data, 'HorizontalPositionAccuracy')
        HorizontalPositionAccuracy = Data.HorizontalPositionAccuracy(:);
    else
        HorizontalPositionAccuracy = [];
    end
    
    VesselHeading(VesselHeading > 360) = NaN;
        
    %% Cr�ation des instances ClNavigation
    
    T = datetime(Time, 'ConvertFrom', 'datenum');
    timeSample = XSample('name', 'time', 'data', T);

    latSample = YSample('data', Latitude, 'marker', '.');
    latSignal = ClSignal('ySample', latSample, 'xSample', timeSample);
    
    lonSample = YSample('data', Longitude, 'marker', '.');
    lonSignal = ClSignal('ySample', lonSample, 'xSample', timeSample);

    headingVesselSample = YSample('name', 'Heading', 'data', VesselHeading, 'marker', '.');
    headingVesselSignal = ClSignal('ySample', headingVesselSample, 'xSample', timeSample);
    
    immersionSample = YSample('name', 'Immersion', 'data', VesselHeight,  'marker', '.', 'unit', 'm');
    immersionSignal = ClSignal('ySample', immersionSample, 'xSample', timeSample);
        
	nav(end+1) = ClNavigation(latSignal, lonSignal, ...
        'immersionSignal', immersionSignal, ...
        'headingVesselSignal', headingVesselSignal, ...
        'name', listeFic{k}); %#ok<AGROW>

end
my_close(hw)

%% Create the GUI

a = NavigationDialog(nav, 'Title', 'Navigation', 'CleanInterpolation', true);
% a.displayedSampleList = [1 1 1 1 0 0 0 0];
pause(0.5)
a.openDialog();
if ~a.okPressedOut
    flag = 0;
    return
end
    
%% Export des navigations 

str1 = 'Voulez-vous sauvegarder cette nouvelle navigation dans le cache SSc des .all ?';
str2 = 'Do you want to save this navigation in the SSc cach directories of the .all ?';
[rep, flag] = my_questdlg(Lang(str1,str2));
if ~flag || (rep == 2)
    return
end

%% Sauvegarde des signaux corrig�s dans le r�pertoire cache de SSc
    
nav = a.signalContainerList;
str1 = 'Sauvegarde de la navigation.';
str2 = 'Export of the navigation';
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
        
    Latitude  = nav(k).getLatitudeSignalSample().data;
    Longitude = nav(k).getLongitudeSignalSample().data;
    Immersion = nav(k).getImmersionSignalSample().data;
    Heading   = nav(k).getHeadingVesselSignalSample().data;
    Time      = nav(k).getTimeSignalSample().data;
    
    Time = datenum(Time);
    
    %{
    % TODO MHA
    Speed                      = nav(k).ySample(1).data;
    CourseVessel               = nav(k).ySample(2).data;
    HeightAccuracy             = nav(k).ySample(3).data;
    HorizontalPositionAccuracy = nav(k).ySample(4).data;
    %}
    
    %% Save Heading
    
    switch provenance
        case 'DataDepth'
            [flag, DataDepth, identSondeur] = read_depth_s7k(s7k(k));
            if ~flag
                continue
            end
            
            figure(86543); plot(DataDepth.Longitude, DataDepth.Latitude, 'b'); grid on; hold on; plot(Longitude, Latitude, 'r');
%             figure(86544); plot(DataDepth.Heading, 'b'); grid on; hold on; plot(Heading, 'r'); hold off;
    
            % TODO ! c'est la &~#"{} !
            DataDepth.Latitude  = Latitude;
            DataDepth.Longitude = Longitude;
            DataDepth.Immersion = Immersion;
            DataDepth.Heading   = Heading;
            Data = DataDepth;
            
            [CourseVessel, Speed] = calCapFromLatLon(DataDepth.Latitude, DataDepth.Longitude, 'Time', DataDepth.Datetime);
            
            [nomDir, nomFic] = fileparts(s7k(k).nomFic);
            nomFicMat  = fullfile(nomDir, 'SonarScope', [num2str(identSondeur) '_' nomFic '_depth.mat']);
            ncFileName = fullfile(nomDir, 'SonarScope', [nomFic '.nc']);
            try
                if useCacheNetcdf == 1 % 1=Mat, 2 et 3=Netcdf
                    save(nomFicMat, 'Data')
                else
                    [Data.Dimensions.nbPings, Data.Dimensions.nbBeams] = size(Data.BeamDepressionAngle);
                    NetcdfUtils.Struct2Netcdf(Data, ncFileName, 'Depth');
                    
                    try % Rajout� par JMA le 22/11/2021
                        write_navigation_old(s7k(k), identSondeur, Time, Latitude, Longitude, Heading, Immersion, ...
                            Speed, CourseVessel, HeightAccuracy, HorizontalPositionAccuracy);
                    catch ME
                        my_beakpoint
                    end
                end
            catch %#ok<CTCH>
                messageErreurFichier(nomFicMat, 'WriteFailure');
            end
            
        case 'DataPosition'
            write_navigation_old(s7k(k), identSondeur, Time, Latitude, Longitude, Heading, Immersion, ...
                Speed, CourseVessel, HeightAccuracy, HorizontalPositionAccuracy);
    end
end
my_close(hw, 'MsgEnd');

flag = 1;
