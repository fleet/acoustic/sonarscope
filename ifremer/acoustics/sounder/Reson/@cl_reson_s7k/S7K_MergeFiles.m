function S7K_MergeFiles(this, nomFic) %#ok<INUSL>

if ~iscell(nomFic)
    nomFic = {nomFic};
end

nbFics = length(nomFic);
for k=1:nbFics
    this = cl_reson_s7k('nomFic', nomFic{k});
    identSondeur = selectionSondeur(this);
    
    %% Lecture du fichier d'index
    
    [flag, DataIndex] = readFicIndex(this, identSondeur);
    if ~flag
        return
    end
    
    Date  = DataIndex(1,:);
    Heure = DataIndex(2,:);
    
    Datetime = timeIfr2Datetime(Date, Heure);
    DatetimeDeb(k,1) = Datetime(1); %#ok<AGROW>
    
%     Position         = DataIndex(3,:);
%     Taille           = DataIndex(4,:);
%     TypeDatagram     = DataIndex(5,:);
%     PingCounter      = DataIndex(6,:);
%     PingSequence     = DataIndex(7,:);
%     DeviceIdentifier = DataIndex(8,:);
end

[~, ia] = sort(DatetimeDeb);
nomFic = nomFic(ia);
nomFicFusion = strrep(nomFic{1}, '.s7k', '_Fusion.s7k');
fidOut = fopen(nomFicFusion, 'w+');
if fidOut == -1
    str = [Lang('Impossible de cr�er le fichier', 'Impossible to create file') nomFicOut];
    my_warndlg(str, 1);
    return
end
    
str = Lang('Fusion des fichiers .s7k en cours', 'Merging .s7k files');
hw = create_waitbar(str, 'N', nbFics);
for k1=1:length(nomFic)
    my_waitbar(k1, nbFics, hw);
    
    this = cl_reson_s7k('nomFic', nomFic{k1});
    identSondeur = selectionSondeur(this);
        
    [flag, DataIndex] = readFicIndex(this, identSondeur);
    if ~flag
        continue
    end
    
    % Position = DataIndex(3,:);
    Taille = DataIndex(4,:);
    fidIn = fopen(nomFic{k1}, 'r');
    for k2=1:length(Taille)
        X = fread(fidIn, Taille(k2), 'uint8');
        fwrite(fidOut, X, 'uint8');
    end
    fclose(fidIn);
end
my_close(hw, 'MsgEnd');
fclose(fidOut);
