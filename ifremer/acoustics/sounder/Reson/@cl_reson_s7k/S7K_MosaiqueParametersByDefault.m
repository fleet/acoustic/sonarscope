function [flag, c] = S7K_MosaiqueParametersByDefault(~, listeFicS7K, SurveyName, NomLayer, gridSize, NomDir, varargin)

[varargin, InfoCompensationCurve] = getPropertyValue(varargin, 'InfoCompensationCurve', []); %#ok<ASGLU>

switch NomLayer
    case 'ReflectivityFromSnippets 100%' % 'ListeLayers', 30
        if isempty(InfoCompensationCurve)
            Prefix = 'Mos';
        else
            Prefix = 'Mos-Comp';
        end
        strDataType = 'Reflectivity';
    case 'Depth / SeaLevel' % 'ListeLayers', 12
        Prefix = 'DTM';
        strDataType = 'Bathymetry';
    otherwise
        Prefix = 'TODO';
        strDataType = 'TODO';
end

SameReflectivityStatus = 0;

if isempty(InfoCompensationCurve)
    InfoCompensationCurve.FileName                 = {};
    InfoCompensationCurve.ModeDependant            = 0;
    InfoCompensationCurve.UseModel                 = [];
    InfoCompensationCurve.PreserveMeanValueIfModel = [];
end

InfoMos.TypeMos          = 1;
InfoMos.TypeGrid         = 1;
InfoMos.flagMasquageDual = 1;
InfoMos.LimitAngles      = [];
InfoMos.Backup           = 10;
InfoMos.gridSize         = gridSize;

InfoMos.Import.MasqueActif  = 1;
InfoMos.Import.SlotFilename = [];
InfoMos.Import.SonarTime    = [];

InfoMos.Covering.Priority           = 1;
InfoMos.Covering.SpecularLimitAngle = [];

strGridSize = num2str(gridSize);
strGridSize = strrep(strGridSize, '.', ',');
str = sprintf('m_LatLong_%s.ers%s', strDataType);
InfoMos.Unique.nomFicErMapperLayer    = fullfile(NomDir, [SurveyName '-' Prefix '_' strGridSize str]);
InfoMos.Unique.nomFicErMapperAngle    = strrep(InfoMos.Unique.nomFicErMapperLayer, str, 'm_LatLong_RxAngleEarth.ers');
InfoMos.Unique.CompleteExistingMosaic = 0;

flagTideCorrection = 1;
ModeDependant      = 0;
Window             = [5 5];

I0 = cl_image;

[flag, c] = S7K_MosaiqueParProfil(I0, 1, listeFicS7K, ...
                ModeDependant, NomLayer, Window, ...
                SameReflectivityStatus, InfoCompensationCurve, InfoMos, ...
                'TideCorrection', flagTideCorrection);            
