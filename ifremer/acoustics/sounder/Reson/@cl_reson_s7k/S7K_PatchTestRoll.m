function S7K_PatchTestRoll(~, nomFic, resol)

if ~iscell(nomFic) || (length(nomFic) ~= 2)
    str1 = 'Il faut 2 fichiers acquis sur la ligne mais en sens oppos�s pour faire cette calibration en roulis.';
    str2 = 'You need 2 lines in opposite directions to perform this roll biais identification.';
    my_warndlg(Lang(str1,str2), 1);
    return
end

%% Cr�ation des mosa�ques

Carto = [];
for k=1:2
    a = cl_reson_s7k('nomFic', nomFic{k});
    if isempty(a)
        return
    end
    
    if k == 1
        identSondeur = selectionSondeur(a);
        Invite = 'Distance Inter Pings';
        nbColors = 64;
        [flag, Signal] = read_Invite(a, identSondeur, Invite, nbColors);
        if ~flag || isempty(Signal.Data)
            return
        end
        resol = 2 * mean(Signal.Data, 'omitnan');
        resol = nearestExpectedGridSize(resol);
        
        str1 = 'Taille de la grille';
        str2 = 'Grid size';
        [flag, resol] = inputOneParametre(Lang(str1,str2), 'Value', ...
            'Value', resol, 'Unit', 'm', 'MinValue', resol/2, 'MaxValue', resol*2);
        if ~flag
            return
        end
    end
    
   
    % Lecture des layers depth, across distance, along distance et angles
    [b, Carto] = view_depth(a, 'ListeLayers', [12 2 3 4], 'Carto', Carto, 'Bits', [1 2 6]);
    
    Z(k)              = b(1); %#ok<AGROW>
    AcrossDistance(k) = b(2); %#ok<AGROW>
    AlongDistance(k)  = b(3); %#ok<AGROW>
    A(k)              = b(4); %#ok<AGROW>
end

%% D�termination du biais en roulis

[flag, RollBias] = sonar_PatchtestRoll(Z, AcrossDistance, AlongDistance, A, resol);
