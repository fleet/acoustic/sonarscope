function S7K_SplitFrequencies(this, nomFic) %#ok<INUSL>

% useParallel = get_UseParallelTbx;

if ~iscell(nomFic)
    nomFic = {nomFic};
end

N = length(nomFic);
str1 = 'S�paration des fr�quences des fichiers S7K';
str2 = 'Spliting frequencies for S7K files';

% % Abandon de la // car moins performant sur disque m&gn�tique
% if useParallel && (N > 1)
%     [hw, DQ] = create_parforWaitbar(N, Lang(str1,str2));
%     parfor k=1:N
%         S7K_SplitFrequencies_unitaire(nomFic{k})
%         send(DQ, k);
%     end
%     my_close(hw, 'MsgEnd')
% else
    hw = create_waitbar(Lang(str1,str2), 'N', N);
    for k=1:N
        my_waitbar(k, N, hw)
        S7K_SplitFrequencies_unitaire(nomFic{k})
    end
    my_close(hw, 'MsgEnd')
% end


function S7K_SplitFrequencies_unitaire(nomFic)

this = cl_reson_s7k('nomFic', nomFic);
identSondeur = selectionSondeur(this);

%% Lecture du fichier d'index

[flag, DataIndex] = readFicIndex(this, identSondeur);
if ~flag
    return
end

%% Lecture de la fr�quence

DataSettings = read_sonarSettings(this, identSondeur); % Datagrammes 7000
if isempty(DataSettings) || isempty(DataSettings.Frequency)
    return
end
Frequencies = unique(DataSettings.Frequency);

%% D�coupage par fr�quence

for k=1:length(Frequencies)
    S7K_SplitFrequency(Frequencies(k), DataIndex, DataSettings, nomFic);
end


function S7K_SplitFrequency(Frequency, DataIndex, DataSettings, nomFic)

subPings = (DataSettings.Frequency == Frequency);

Position    = DataIndex(3,:);
Taille      = DataIndex(4,:);
PingCounter = DataIndex(6,:);

fid = fopen(nomFic, 'r', 'l');
if fid == -1
    messageErreurFichier(nomFic, 'ReadFailure');
    return
end

nomFicOut = strrep(nomFic, '.s7k', ['-' num2str(Frequency/1000) 'kHz.s7k']);
fidOut = fopen(nomFicOut, 'w+', 'l');

% [~, nomFicTmp] = fileparts(nomFicOut);
% nomFicTmp = fullfile(my_tempdir, nomFicTmp);
% fidOut = fopen(nomFicTmp, 'w+', 'l');

if fidOut == -1
    str = [Lang('Impossible de cr�er le fichier', 'Impossible to create the file') nomFicOut];
    my_warndlg(str, 1);
    return
end

strLoop = sprintf('S7K splitting frequencies : file %s', nomFicOut);
my_warndlg([strLoop ' : Begin'], 0);
N = sizeFic(nomFic);
for k=1:length(Position)
    infoLoop(strLoop, Position(k), N, 'bytes')
    
    fseek(fid, Position(k), 'bof');
    X = fread(fid, Taille(k), 'uint8');
    
    PC = PingCounter(k);
    if PC == 0
        fwrite(fidOut, X, 'uint8');
    else
        if ismember(PC, DataSettings.PingCounter(subPings))
            fwrite(fidOut, X, 'uint8');
        end
    end
end
fclose(fidOut);
fclose(fid);
my_warndlg([strLoop ' : end'], 0);

% [flag, message, messageId] = movefile(nomFicTmp, nomFicOut, 'f');
