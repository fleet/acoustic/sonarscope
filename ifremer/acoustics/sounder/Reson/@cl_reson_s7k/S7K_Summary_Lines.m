function [FigTable, nomFicConf, ShortSummary] = S7K_Summary_Lines(this, nomFicSummary, varargin)

[varargin, listeItems] = getPropertyValue(varargin, 'listeItems', []);
[varargin, Carto]      = getPropertyValue(varargin, 'Carto',      []); %#ok<ASGLU>

FigTable     = [];
nomFicConf   = {};
ShortSummary = [];

NbFic = numel(this);
for k=NbFic:-1:1
    listeNomFic{k} = this(k).nomFic;
end

useParallel = get_UseParallelTbx;

%% Comma or not comma ?

sep = getCSVseparator;

%% Cr�ation de la liste des items

S0 = cl_reson_s7k.empty();
strHeader = createListAllItems(S0);

indFreq         = find(strcmp(strHeader, 'Main frequency (kHz)'));
% indStrMode    = find(strcmp(strHeader, 'strMode'));
% indNbSwaths   = find(strcmp(strHeader, 'NbSwaths'));
indSwath0       = find(strcmp(strHeader, 'Nb pings Sequence 0'));
indSwath1       = find(strcmp(strHeader, 'Nb pings Sequence 1'));
indSwath2       = find(strcmp(strHeader, 'Nb pings Sequence 2'));
indSwath3       = find(strcmp(strHeader, 'Nb pings Sequence 3'));
indSwath4       = find(strcmp(strHeader, 'Nb pings Sequence 4'));
% indFM         = find(strcmp(strHeader, 'FM'));
indNbPings      = find(strcmp(strHeader, 'Nb pings'));
indDepthMin     = find(strcmp(strHeader, 'Min Depth (m)')); % TODO
indDepthMax     = find(strcmp(strHeader, 'Max Depth (m)')); % TODO
indTimeBegin    = find(strcmp(strHeader, 'Time beginning')); % TODO
indTimeEnd      = find(strcmp(strHeader, 'Time end')); % TODO
indTimeDuration = find(strcmp(strHeader, 'Duration')); % TODO
indSounderName  = find(strcmp(strHeader, 'Sounder name'));
indLineLength   = find(strcmp(strHeader, 'Line length (m)'));
indLineCoverage = find(strcmp(strHeader, 'Line coverage (km2)'));

%% S�lection des items

if isempty(listeItems)
    listeItems = 1:length(strHeader);
end
strHeader = strHeader(listeItems);

%% Cr�ation du fichier r�sum�

fid = fopen(nomFicSummary, 'w+t');
if fid == -1
    messageErreurFichier(nomFicSummary, 'WriteFailure');
    str1 = sprintf('"%s" semble �tre ouvert par un autre programme, fermez cette application et reessayer ce traitement..', nomFicSummary);
    str2 = sprintf('"%s" seems to be locked by another program, please close it and retry this operation.', nomFicSummary);
    my_warndlg(Lang(str1,str2), 1);
    return
end

%% Ecriture de l'ent�te

for k=1:length(strHeader)
    fprintf(fid, '%s%s', strHeader{k}, sep);
end
fprintf(fid, '\n');

%% Boucle de calcul sur les fichiers

nbItems = length(listeItems);

Frequency = NaN(1, NbFic);

NbSwaths0 = zeros(1, NbFic);
NbSwaths1 = zeros(1, NbFic);
NbSwaths2 = zeros(1, NbFic);
NbSwaths3 = zeros(1, NbFic);
NbSwaths4 = zeros(1, NbFic);

strSummary  = cell(NbFic, nbItems);

SurveyDepthMin     =  Inf(1, NbFic);
SurveyDepthMax     = -Inf(1, NbFic);
SurveyTimeBegin    = datetime(0, 0, 0, 0, 0,  Inf(1, NbFic));
SurveyTimeEnd      = datetime(0, 0, 0, 0, 0, -Inf(1, NbFic));
SurveyNbPings      = zeros(1, NbFic);
SurveyDuration     = zeros(1, NbFic);
SurveyLineCoverage = zeros(1, NbFic);
% SurveyEmModel      = cell(1, NbFic);
SurveySounderName  = cell(1, NbFic);
SurveyLineLength   = zeros(1, NbFic);
     
% tic
% TODO : tout �a est mal boutiqu�. Il faut sortir des tableaux de booleen de dimension [nbPings,1] pour chacun des modes, single/double swath, ... et faire un et logique pour savoir si il y a v�ritablement des pings � cette configuration
str1 = 'Cr�ation du r�sum� des fichiers .s7k';
str2 = 'Creating .s7k lines summary';

UN = 0; % TODO : La boucle // ne rend pas la main, pourquoi ?

if UN && useParallel && (NbFic > 1)
    [hw, DQ] = create_parforWaitbar(NbFic, Lang(str1,str2));
    parfor (kFic=1:NbFic, useParallel)
        fprintf('File %d/%d : %s\n', kFic, NbFic, this(kFic).nomFic);
        [flag, Values, strValues] = summary_Lines(this(kFic), 'isUsingParfor', true);
        if flag
            strValues = strValues(listeItems);
            strSummary(kFic,:) = strValues;
            
            Frequency(kFic) = Values{indFreq};
           
            NbSwaths0(kFic) = Values{indSwath0};
            NbSwaths1(kFic) = Values{indSwath1};
            NbSwaths2(kFic) = Values{indSwath2};
            NbSwaths3(kFic) = Values{indSwath3};
            NbSwaths4(kFic) = Values{indSwath4};
            
            SurveyNbPings(kFic)      = Values{indNbPings};
            SurveyTimeBegin(kFic)    = Values{indTimeBegin};
            SurveyTimeEnd(kFic)      = Values{indTimeEnd};
            SurveyDepthMin(kFic)     = Values{indDepthMin};
            SurveyDepthMax(kFic)     = Values{indDepthMax};
            SurveyDuration(kFic)     = Values{indTimeDuration};
            SurveySounderName{kFic}  = Values{indSounderName};
            SurveyLineLength(kFic)   = Values{indLineLength};
            SurveyLineCoverage(kFic) = Values{indLineCoverage};
        end
        send(DQ, kFic);
    end
    my_close(hw, 'MsgEnd')
else
    hw = create_waitbar(Lang(str1,str2), 'N', NbFic);
    for kFic=1:NbFic
        my_waitbar(kFic, NbFic, hw);
        fprintf('File %d/%d : %s\n', kFic, NbFic, this(kFic).nomFic);
        [flag, Values, strValues, Carto] = summary_Lines(this(kFic), strHeader, Carto);
        if flag
            strValues = strValues(listeItems);
            strSummary(kFic,:) = strValues;

            Frequency(kFic) = Values{indFreq};
            
            NbSwaths0(kFic) = Values{indSwath0};
            NbSwaths1(kFic) = Values{indSwath1};
            NbSwaths2(kFic) = Values{indSwath2};
            NbSwaths3(kFic) = Values{indSwath3};
            NbSwaths4(kFic) = Values{indSwath4};
           
            SurveyNbPings(kFic)      = Values{indNbPings};
            SurveyTimeBegin(kFic)    = Values{indTimeBegin};
            SurveyTimeEnd(kFic)      = Values{indTimeEnd};
            SurveyDepthMin(kFic)     = Values{indDepthMin};
            SurveyDepthMax(kFic)     = Values{indDepthMax};
            SurveyDuration(kFic)     = Values{indTimeDuration};
            SurveySounderName{kFic}  = Values{indSounderName};
            SurveyLineLength(kFic)   = Values{indLineLength};
            SurveyLineCoverage(kFic) = Values{indLineCoverage};
       end
    end
    my_close(hw);
end
% toc

%% Ecriture du fichier

for kFic=1:NbFic
    for k=1:nbItems
        fprintf(fid, '%s%s', strSummary{kFic,k}, sep);
    end
    fprintf(fid, '\n');
end
fclose(fid);

%% Affichage des r�sultats dans une table

FigTable = figure;
t = uitable;
set(t, 'ColumnName', strHeader, 'Data', strSummary)
set(t, 'RearrangeableColumns', 'On')
set(t, 'Unit', 'normalized', 'Position', [0 0 1 1])

%% Affichage des r�sultats dans un �diteur

try
    winopen(nomFicSummary)
catch %#ok<CTCH>
end

%% Recherche des configurations

NbSwathsUnique = [];
if any(NbSwaths0)
    NbSwathsUnique = [NbSwathsUnique 1];
end
if any(NbSwaths1 | NbSwaths2 | NbSwaths3 | NbSwaths4)
    NbSwathsUnique = [NbSwathsUnique 2];
end

minPingsPerConfiguration = 10;
[nomDir, nomFicCSV] = fileparts(nomFicSummary);

nomFicConf = createFileListsPerConfig(listeNomFic, nomDir, ...
    Frequency, ...
    NbSwathsUnique, NbSwaths0, NbSwaths1, NbSwaths2, NbSwaths3, NbSwaths4, ...
    nomFicCSV,  minPingsPerConfiguration);

%% Calcul de certains indicateurs

ShortSummary.SurveyNbPings      = sum(SurveyNbPings);
ShortSummary.SurveyTimeBegin    = min(SurveyTimeBegin);
ShortSummary.SurveyTimeEnd      = max(SurveyTimeEnd);
ShortSummary.SurveyDepthMin     = min(SurveyDepthMin);
ShortSummary.SurveyDepthMax     = max(SurveyDepthMax);
ShortSummary.SurveyDuration     = sum(SurveyDuration);
ShortSummary.SurveyLineLength   = sum(SurveyLineLength, 'omitnan');
ShortSummary.SurveyLineCoverage = sum(SurveyLineCoverage, 'omitnan');
ShortSummary.SurveySounderName  = SurveySounderName{1};
ShortSummary.SurveySounderCie   = 'Reson Seabat ';


function nomFicConf = createFileListsPerConfig(listeNomFic, nomDir, ...
    Frequency, ...
    NbSwathsUnique, NbSwaths0, NbSwaths1, NbSwaths2, NbSwaths3, NbSwaths4, ...
        nomFicCSV,  minPingsPerConfiguration)

nomFicConf = {};

listFrequencies = unique(Frequency);
NbFreqUnique = length(listFrequencies);

for kFreq=1:NbFreqUnique
    subFilesFreqOK = (Frequency == listFrequencies(kFreq));
    
    for kSwath=1:length(NbSwathsUnique)
        switch NbSwathsUnique(kSwath)
            case 1 % Double swath
                subFilesSwathOK = (NbSwaths0 > minPingsPerConfiguration);
            case 2 % Single swath
                subFilesSwathOK = ((NbSwaths1 + NbSwaths2 + NbSwaths3 + NbSwaths4) > minPingsPerConfiguration);
        end
        
        subFiles = find(subFilesSwathOK & subFilesFreqOK);
        if ~isempty(subFiles)
            if NbSwathsUnique(kSwath) == 1
                CodeSwath = 'SingleSwath';
            else
                CodeSwath = 'MultiSwath';
            end
            nomFicConf{end+1} = sprintf('%s_%s_%skHz.txt', nomFicCSV, CodeSwath, num2str(listFrequencies(kFreq))); %#ok<AGROW>
            nomFicConf{end} = fullfile(nomDir, nomFicConf{end});
            fid = fopen(nomFicConf{end}, 'w+t');
            if fid == -1
                messageErreurFichier(nomFicConf{end}, 'WriteFailure');
                return
            end
            for k4=1:length(subFiles)
                fprintf(fid, '%s\n', listeNomFic{subFiles(k4)});
            end
            fclose(fid);
        end
    end
end
