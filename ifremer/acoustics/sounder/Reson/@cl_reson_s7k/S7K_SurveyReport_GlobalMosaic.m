function S7K_SurveyReport_GlobalMosaic(S0, nomFicAdoc, AreaName,listeNomFic, gridSize, InfoCompensationCurveReflectivity)

%% Create directory for files

[nomDirSummary, SurveyName] = fileparts(nomFicAdoc);
nomDirFiles = fullfile(nomDirSummary, SurveyName, 'ERS');

%% Nom du fichier ADOC

[nomDirSummary, SurveyName] = fileparts(nomFicAdoc);
Tag = num2str(randi(100000));

if isempty(AreaName)
    nomImage = SurveyName;
else
    AE = str2Filename(AreaName);
    AE = strrep(AE, ' ', '');
    AE = rmblank(AE);
    AE = strrep(AE, '-', '_');
    AE = strrep(AE, '+', '_');
    AE = strrep(AE, '[', '');
    AE = strrep(AE, ']', '');
    nomImage = [SurveyName '-' AE];
end

%% MNT

NomLayer = 'Depth / SeaLevel';
[flag, a] = S7K_MosaiqueParametersByDefault(S0, listeNomFic, SurveyName, NomLayer, gridSize, nomDirFiles);
if flag
    a(1).CLim = '0.5%';
    a(1).ColormapIndex = 20;
    [nomFicFig, nomFicPng, nomFicKmz, nomFicGeotif, TypeGeotif, ImageName] = exportMosaicsFig2PngAndFigFiles(a(1), nomDirSummary, SurveyName, Tag);
    AdocUtils.addMosaic(nomFicAdoc, nomFicFig, nomFicPng, nomFicKmz, nomFicGeotif, TypeGeotif, ImageName, Tag, ...
        'Title1', 'Mosaics', 'AreaName', AreaName, 'listDataFiles', listeNomFic);
        
    b = sunShadingGreatestSlope(a(1), 1);
    [nomFicFig, nomFicPng, nomFicKmz, nomFicGeotif, TypeGeotif, ImageName] = exportMosaicsFig2PngAndFigFiles(b, nomDirSummary, SurveyName, Tag);
    AdocUtils.addMosaic(nomFicAdoc, nomFicFig, nomFicPng, nomFicKmz, nomFicGeotif, TypeGeotif, ImageName, Tag);
end

%% Carte des pentes

[a, flag] = slopeMax(a(1));
if flag
    a.CLim = '0.5%';
    a.ColormapIndex = 20;
    [nomFicFig, nomFicPng, nomFicKmz, nomFicGeotif, TypeGeotif, ImageName] = exportMosaicsFig2PngAndFigFiles(a(1), nomDirSummary, SurveyName, Tag);
    AdocUtils.addMosaic(nomFicAdoc, nomFicFig, nomFicPng, nomFicKmz, nomFicGeotif, TypeGeotif, ImageName, Tag);
end

%% Mosaique de réflectivité

NomLayer = 'ReflectivityFromSnippets 100%';

[flag, a] = S7K_MosaiqueParametersByDefault(S0, listeNomFic, nomImage, NomLayer, gridSize, nomDirFiles);
if flag
    a(1).CLim = '1%';
    
    [nomFicFig, nomFicPng, nomFicKmz, nomFicGeotif, TypeGeotif, ImageName, nomFicPngInverseVideo, nomFicKmzInverseVideo] ...
        = exportMosaicsFig2PngAndFigFiles(a(1), nomDirSummary, SurveyName, Tag);
    
    AdocUtils.addMosaic(nomFicAdoc, nomFicFig, nomFicPng, nomFicKmz, nomFicGeotif, TypeGeotif, ImageName, Tag, ...
        'nomFicPngInverseVideo', nomFicPngInverseVideo, 'nomFicKmzInverseVideo', nomFicKmzInverseVideo);
    
    %% Mosaique de réflectivité compensée
    
    if ~isempty(InfoCompensationCurveReflectivity)
        NomLayer = 'ReflectivityFromSnippets 100%';
        nomImage = [nomImage '-Comp'];
        
        [flag, a] = S7K_MosaiqueParametersByDefault(S0, listeNomFic, nomImage, NomLayer, gridSize, nomDirFiles, ...
            'InfoCompensationCurve', InfoCompensationCurveReflectivity);
        if ~flag
            return
        end
        
        a(1).CLim = '3%';
        
        [nomFicFig, nomFicPng, nomFicKmz, nomFicGeotif, TypeGeotif, ImageName, nomFicPngInverseVideo, nomFicKmzInverseVideo] ...
            = exportMosaicsFig2PngAndFigFiles(a(1), nomDirSummary, SurveyName, Tag);
        
        InfoCompensationCurveReflectivity = AdocUtils.exportCompensationCurve(InfoCompensationCurveReflectivity, nomDirSummary, SurveyName, Tag);
        
        AdocUtils.addMosaic(nomFicAdoc, nomFicFig, nomFicPng, nomFicKmz, nomFicGeotif, TypeGeotif, ImageName, Tag, ...
            'nomFicPngInverseVideo', nomFicPngInverseVideo, 'nomFicKmzInverseVideo', nomFicKmzInverseVideo, ...
            'InfoCompensationCurve', InfoCompensationCurveReflectivity);
    end
end

%% Réouverture du fichier Adoc

AdocUtils.openAdoc(nomFicAdoc);
