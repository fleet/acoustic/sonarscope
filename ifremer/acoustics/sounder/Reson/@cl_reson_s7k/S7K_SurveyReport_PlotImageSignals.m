function S7K_SurveyReport_PlotImageSignals(~, listeNomFic, nomFicAdoc, indexConfigs)

Title1 = 'Image and Signals';
N = numel(listeNomFic);
str1 = 'Rapport Affichage d''Images et de Signaux : Fichiers';
str2 = 'Plot Image and Signals report : Files';
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
    my_waitbar(k, N, hw);
    S7K_SurveyReport_PlotImageSignals_unitaire(listeNomFic{k}, nomFicAdoc, indexConfigs, Title1)
    Title1 = [];
end
my_close(hw, 'MsgEnd')

%% R�ouverture du fichier Adoc

AdocUtils.openAdoc(nomFicAdoc);


function S7K_SurveyReport_PlotImageSignals_unitaire(nomFicS7K, nomFicAdoc, indexConfigs, Title1)

this = cl_reson_s7k('nomFic', nomFicS7K, 'KeepNcID', true);

%% Nom du fichier ADOC

[nomDirSummary, SurveyName] = fileparts(nomFicAdoc);
    
%% Loop on files

[~, nomFicSeul, Ext] = fileparts(this.nomFic);
Title2 = ['File ' nomFicSeul Ext];
N = numel(indexConfigs);
% str1 = 'Rapport Affichage d''Images et de Signaux : Configurations';
% str2 = 'Plot Image and Signals report : Configurations';
% hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
%     my_waitbar(k, N, hw)
    
    %% Lecture de tous les layers qui vont �tre utiles
    
    %% Bathymetry

    switch indexConfigs(k)
        case 1 % 'Bathy and Attitude'
            Layer = get_Layer(this, 'Bathymetry', 'CartoAuto', 1);
            
        case 2 % 'Reflectivity and Sonar Settings'
            Layer = get_Layer(this, 'ReflectivityFromSnippets', 'CartoAuto', 1);
            
        otherwise
            continue
    end
    if isempty(Layer)
        continue
    end
   
    LineStyle = '-';
    Marker    = 'none';
    switch indexConfigs(k)
        case 1 % 'Bathy and Attitude'
            NameOfThePlot = 'BathyAndAttitude'; % TODO : am�liorer cela
            Title3 = 'Bathy and Attitude';
            listSignals = {'Roll'; 'Pitch'; 'Heave'; 'Heading'};
            
        case 2 % 'Reflectivity and Sonar Settings'
            NameOfThePlot = 'ReflectivityAndModes'; % TODO : am�liorer cela
            Title3 = 'Reflectivity and Modes';
            listSignals = {'GainSelection'; 'PowerSelection'; 'TxPulseWidth'; 'RangeSelection'};
        otherwise
            continue
    end
    
    [nomFicFig, nomFicPng] = AdocFile_PlotImageSignalsExportFiles(Layer, listSignals, ...
        nomDirSummary, SurveyName, NameOfThePlot, nomFicSeul, LineStyle, Marker);
    
    AdocUtils.addImageAndSignals(nomFicAdoc, nomFicFig, nomFicPng, ...
        'Title1', Title1, 'Title2', Title2, 'Title3', Title3); %, 'Intro', Intro);
    Title1 = [];
    Title2 = [];
end
% my_close(hw, 'MsgEnd')



function [nomFicFig, nomFicPng] = AdocFile_PlotImageSignalsExportFiles(this, listSignals, nomDirSummary, SurveyName, ...
    NameOfThePlot, nomFicSeul, LineStyle, Marker, varargin)

[varargin, CLim] = getPropertyValue(varargin, 'CLim', []); %#ok<ASGLU>

if isempty(CLim)
    CLim = '0.5%';
end
this.CLim = CLim;

Fig = FigUtils.createSScFigure('Position', centrageFig(1600, 800));

n = length(listSignals);
ha = subplot(n+1, 1, 1);
imagesc(this, 'Axe', ha, 'Rot90', 1, 'Location', 'East'); % 'LOCATION', 'EastOutside'

[~, Time] = getValSignalIfAny(this, 'Time');

for k=1:n
    ha(k+1) = subplot(n+1, 1, k+1);
    
    [flag, Signal, Unit, strLegend] = getValSignalIfAny(this, listSignals{k});
    if ~flag
        continue
    end
    
    if isempty(Signal)
        title(ha(k+1), [listSignals{k} ' - Empty']);
    else
        if strcmp(listSignals{k}, 'Time')
            Signal = datetime(Signal, 'ConvertFrom', 'datenum');
        end
        hc = PlotUtils.createSScPlot(ha(k+1), Signal, 'LineStyle', LineStyle, 'Marker', Marker); grid(ha(k+1), 'on');
        set(hc, 'UserData', Time)
        if isempty(Unit)
            Title = listSignals{k};
        else
            Title = sprintf('%s (%s)', listSignals{k}, Unit);
        end
        
        if ~isempty(strLegend)
            str = sprintf('%d=%s', 1, strLegend{1});
            for k2=2:length(strLegend)
                str = sprintf('%s, %d=%s', str, k2, strLegend{k2});
            end
            Title = sprintf('%s (%s)', Title, str);
        end
        
        title(ha(k+1), Title, 'Interpreter', 'none');
    end
end
linkaxes(ha, 'x'); axis tight; drawnow;

% ImageName = this.Name;
FileName = NameOfThePlot;

nomDirPlotImageSignals = fullfile(nomDirSummary, SurveyName, 'PlotImageSignals');
if ~exist(nomDirPlotImageSignals, 'dir')
    mkdir(nomDirPlotImageSignals)
end

nomDirPlotImageSignals = fullfile(nomDirPlotImageSignals, nomFicSeul);
if ~exist(nomDirPlotImageSignals, 'dir')
    mkdir(nomDirPlotImageSignals)
end

nomFicFig = fullfile(nomDirPlotImageSignals, [FileName '.fig']);
savefig(Fig, nomFicFig)

nomFicPng = fullfile(nomDirPlotImageSignals, [FileName '.png']);
fig2fic(Fig, nomFicPng)
close(Fig);
