function S7K_SurveyReport_PlotSignalsVsTime(~, listeNomFic, nomFicAdoc, indexConfigs)

for k=length(listeNomFic):-1:1
    s7k(k) = cl_reson_s7k('nomFic', listeNomFic{k}, 'KeepNcID', true);
end

%% Plot des signaux en fonction du temps

Title =  'Signals Vs Time';
listeNomFicTitle = listeNomFic;
Tag = num2str(randi(100000));

%% SonarSettings

if any(indexConfigs == 1)
    S7K_SurveyReport_PlotSonarSettings(s7k, nomFicAdoc, Title, listeNomFicTitle, Tag)
    Title = [];
    listeNomFicTitle = [];
end

%% Attitude

if any(indexConfigs == 2)
    S7K_SurveyReport_PlotAttitude(s7k, nomFicAdoc, Title, listeNomFicTitle, Tag)
    Title = []; %#ok<NASGU>
    listeNomFicTitle = []; %#ok<NASGU>
end

%% R�ouverture du fichier Adoc

AdocUtils.openAdoc(nomFicAdoc);


function S7K_SurveyReport_PlotSonarSettings(this, nomFicAdoc, Title, listeNomFicTitle, Tag)

SelectedItems = {'GainSelection'; 'PowerSelection'; 'TxPulseWidth'; 'RangeSelection'};
Units         = {'dB'; 'dB'; 'ms'; 'm'};

identSondeur = selectionSondeur(this(1));
[flag, Fig] = plot_sonarSettings(this, identSondeur, 'SelectedItems', SelectedItems, 'Units', Units);
if flag
    [nomDirSummary, SurveyName] = fileparts(nomFicAdoc);
    [nomFicFig, nomFicPng] = AdocUtils.exportFigAndPng(Fig, nomDirSummary, SurveyName, Tag, 'SonarSettings');
    
    savefig(Fig, nomFicFig)
    fig2fic(Fig, nomFicPng)
    close(Fig);
    
    AdocUtils.addFigures(nomFicAdoc, nomFicFig, nomFicPng, [], Tag, 'Title', Title, 'listDataFiles', listeNomFicTitle); % TODO : [] pas bien pens�
end


function S7K_SurveyReport_PlotAttitude(this, nomFicAdoc, Title, listeNomFicTitle, Tag)

identSondeur = selectionSondeur(this(1));
[flag, Fig] = plot_attitude(this, identSondeur);
if flag
    [nomDirSummary, SurveyName] = fileparts(nomFicAdoc);
    [nomFicFig, nomFicPng] = AdocUtils.exportFigAndPng(Fig, nomDirSummary, SurveyName, Tag, 'Attitude');
    
    savefig(Fig, nomFicFig)
    fig2fic(Fig, nomFicPng)
    close(Fig);
    
    AdocUtils.addFigures(nomFicAdoc, nomFicFig, nomFicPng, [], Tag, 'Title', Title, 'listDataFiles', listeNomFicTitle);
end
