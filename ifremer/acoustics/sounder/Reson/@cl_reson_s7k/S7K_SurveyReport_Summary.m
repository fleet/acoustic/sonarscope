function S7K_SurveyReport_Summary(this, nomFicAdoc, varargin)

[varargin, OpenAdoc] = getPropertyValue(varargin, 'OpenAdoc', 1); %#ok<ASGLU>

dosNetUse('Init')

%% Create histogram of datagrams

for k=1:length(this)
    listFileNames{k} = this(k).nomFic; %#ok<AGROW>
end
Fig = preprocessS7kFiles(listFileNames, 'Tasks', 1, 'flagPlotIndex', 0, 'flagPlotHisto', 0, 'Mute', 1);
if isempty(Fig)
    return
end
Tag = num2str(randi(100000));
[nomDirSummary, SurveyName] = fileparts(nomFicAdoc);
[nomFicFigHistoDatagrams, nomFicPngHistoDatagrams] = AdocUtils.exportFigAndPng(Fig, nomDirSummary, SurveyName, Tag, 'Datagrams');
my_close(Fig);

%% Create summary

[nomDirSummary, SurveyName] = fileparts(nomFicAdoc);
nomFicSummary = fullfile(nomDirSummary, SurveyName, [SurveyName '-SummaryLines.csv']);
[Fig, nomFicConf, ShortSummary] = S7K_Summary_Lines(this, nomFicSummary, 'Mute', 1);
if isempty(Fig)
    return
end
nomFicFig = fullfile(nomDirSummary, SurveyName, 'FIG', [SurveyName '-Navigation.fig']);
savefig(Fig, nomFicFig)
my_close(Fig);
% AdocUtils.addSummary(nomFicAdoc, nomFicSummary, nomFicFig, nomFicConf, ShortSummary, [], []); 
AdocUtils.addSummary(nomFicAdoc, nomFicSummary, nomFicFig, nomFicConf, ShortSummary, nomFicFigHistoDatagrams, nomFicPngHistoDatagrams); 

%% Ouverture du fichier Adoc

if OpenAdoc
    AdocUtils.openAdoc(nomFicAdoc);
end
dosNetUse('Clear')
