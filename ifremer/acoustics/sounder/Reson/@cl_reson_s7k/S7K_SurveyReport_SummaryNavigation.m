function S7K_SurveyReport_SummaryNavigation(this, nomFicAdoc, ListSignals)

dosNetUse('Init')
   
Tag = num2str(randi(100000));

% for k=length(this):-1:1
%     listeNomFic{k} = this(k).nomFic;
% end

%% Plot navigation

[nomDirSummary, SurveyName] = fileparts(nomFicAdoc);
[Fig, flag] =  plot_nav_S7K(this);
if ~flag
    my_warndlg('There is no navigation in these files.', 0);
    return
end

[nomFicFig, nomFicPng] = AdocUtils.exportFigAndPng(Fig, nomDirSummary, SurveyName, Tag, 'Navigation');
nomFicKmz = fullfile(nomDirSummary, SurveyName, 'KMZ', [SurveyName '-Navigation.kmz']);
plot_navigation_ExportGoogleEarth('NomFicKmz', nomFicKmz);
my_close(Fig);
AdocUtils.addNavigation(nomFicAdoc, nomFicFig, nomFicPng, 'Navigation', 'SectionName', 'Navigation plots', ...
    'GoogleEarthFilename', nomFicKmz);

%% Plot navigation & coverage

Fig = plot_nav_S7K(this, 'Title', 'Position and coverage');
plot_coverage(this, 'Fig', Fig);

[nomFicFig, nomFicPng] = AdocUtils.exportFigAndPng(Fig, nomDirSummary, SurveyName, Tag, 'NavigationCoverage');
nomFicKml = fullfile(nomDirSummary, SurveyName, 'KMZ', [SurveyName '-Coverage-' Tag '.kml']);
% SounderName = get(this(1), 'EmModel');
SounderName = get(this(1), 'SounderName');
Comment = plot_navigation_RasterMask('Fig', Fig, 'nomFicKml', nomFicKml, 'SurveyName', SurveyName, 'SounderName', SounderName, 'SounderCie', 'Reson');
my_close(Fig);
AdocUtils.addNavigation(nomFicAdoc, nomFicFig, nomFicPng, 'NavigationCoverage', 'GoogleEarthFilename', nomFicKml, 'Comment', Comment);

%% Plot des signaux

nbSignals = length(ListSignals);
str1 = 'Trac� de la navigation et des signaux';
str2 = 'Plot Navigation and signals.';
hw = create_waitbar(Lang(str1,str2), 'N', nbSignals);
for k=1:nbSignals
    my_waitbar(k, nbSignals, hw)
    SignalName = ListSignals{k};
    
    if k == 1
        Fig = plot_nav_S7K(this);
        if nbSignals > 1
            nomFicNavigationFig = my_tempname('.fig');
            savefig(Fig, nomFicNavigationFig);
        end
    else
        Fig = openfig(nomFicNavigationFig);
    end
    
    InfoSignal = plot_position_data(this, SignalName, 'Fig', Fig, 'Mute', 1);
    
    suffix = strrep(SignalName, '/', 'On'); % pour SwathWith/Depth
    [nomFicFig, nomFicPng] = AdocUtils.exportFigAndPng(Fig, nomDirSummary, SurveyName, Tag, ['Navigation-' suffix]);
    my_close(Fig);
    AdocUtils.addNavigation(nomFicAdoc, nomFicFig, nomFicPng, ['Navigation and ' SignalName], 'InfoSignal', InfoSignal);
end
my_close(hw, 'MsgEnd')
if nbSignals > 1
    delete(nomFicNavigationFig);
end

%% Ouverture du fichier Adoc

AdocUtils.openAdoc(nomFicAdoc);
dosNetUse('Clear')
