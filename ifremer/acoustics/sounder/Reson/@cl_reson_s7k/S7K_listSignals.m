function Option = S7K_listSignals(~)

Option = {'Index'
    'Navigation'
    'Attitude'
    'SoundSpeedProfile'
    'SurfaceSoundSpeed'
    'VerticalDepth'
    'SonarSettings'
    '7k Configuration'
    'Depth'};
