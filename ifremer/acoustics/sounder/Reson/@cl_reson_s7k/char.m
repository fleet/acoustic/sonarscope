% Representation externe d'une instance dans une chaine de caracteres
%
% Syntax 
%   s = char(a)
%
% Input Arguments
%   a : instance ou tableau d'instances de cl_reson_s7k
%
% Output Arguments
%   s : chaine de caracteres
%
% Examples
%   nomFic = getNomFicDatabase('RESON7125_20051116_221245.s7k')
%   a = cl_reson_s7k('nomFic', nomFic)
%   str = char(a)
%
% See also cl_reson_s7k Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function str = char(this)

str = [];
[m, n] = size(this);

if (m*n) > 1

    % -------------------
    % Tableau d'instances

    for i=1:m
        for j=1:n
            str{end+1} = sprintf('-----Element (%d,%d) -------', i, j); %#ok<AGROW>
            str1 = char(this(i, j));
            str{end+1} = str1; %#ok<AGROW>
        end
    end
    str = cell2str(str);

else

    % ---------------
    % Instance unique

    str = char_instance(this);
    str = [repmat(' ', size(str,1), 2) str];
end
