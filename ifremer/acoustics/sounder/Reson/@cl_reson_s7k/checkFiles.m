function [flag, this] = checkFiles(~, listFileNames, varargin)

[varargin, KeepNcID] = getPropertyValue(varargin, 'KeepNcID', false);
[varargin, Carto]    = getPropertyValue(varargin, 'Carto',    []); %#ok<ASGLU>

flag = 0;
this = [];
% isUsingParfor = 0;

% useParallel = get_UseParallelTbx;

if ~iscell(listFileNames)
    listFileNames = {listFileNames};
end
N = length(listFileNames);

if N == 0
    return
end

%% D�codage 

% [~, Carto] = prepare_lectures7k_step1(listFileNames{1},     'Carto', Carto); % Traitement premier fichier au cas o� il y aurait une demande interactive
%              prepare_lectures7k_step1(listFileNames(2:end), 'Carto', Carto); % prepare_lectures7k_step1 utilise la //Tbx si useParallel sur On
prepare_lectures7k_step1(listFileNames, 'Carto', Carto); % Traitement premier fichier au cas o� il y aurait une demande interactive

%% Cr�ation des instances et r�cup�ration de quelques infos

S0 = cl_reson_s7k.empty();
listS7k = repmat(S0, N, 1);
flagS7k = false(N, 1);
str1 = 'Consultation pr�liminaire des fichiers .s7k';
str2 = 'Checking the .s7k files';
hw = create_waitbar(Lang(str1,str2), 'N', N);
for kFic=1:N
    my_waitbar(kFic, N, hw)
    X = cl_reson_s7k('nomFic', listFileNames{kFic}, 'KeepNcID', KeepNcID);
    if ~isempty(X)
        listS7k(kFic) = X;
        flagS7k(kFic) = true;
    end
end
my_close(hw, 'MsgEnd', 'TimeDelay', 60*5)
listS7k = listS7k(flagS7k);
if isempty(listS7k)
    flag = 0;
    return
end
this = listS7k;

%% Suppression des fichier memmapfile

deleteFileOnListe(cl_memmapfile.empty, gcbf); % Ajout JMA le 23/10/2022 pour tester si �a r�soud les pbs de Carla (arr�t robot si beaucoup de fichiers � traiter)

flag = 1;
