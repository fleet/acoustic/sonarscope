function checkInstallationParameters(~, listeFic)

if ischar(listeFic)
    listeFic = {listeFic};
end

N = length(listeFic);
for k=1:N
    s7k = cl_reson_s7k('nomFic', listeFic{k}, 'KeepNcID', true);
    identSondeur = selectionSondeur(s7k);
    DataSonarInstallationParameters = read_sonarInstallationParameters(s7k, identSondeur);
    if isfield(DataSonarInstallationParameters, 'WaterLineVerticalOffset')
        flag = write_installationParameters_bin(s7k, identSondeur); %#ok<NASGU>
    end
end
