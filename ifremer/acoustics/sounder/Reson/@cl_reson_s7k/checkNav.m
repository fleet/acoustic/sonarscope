function checkNav(~, nomFic)

global useCacheNetcdf %#ok<GVMIS>

if ischar(nomFic)
    nomFic = {nomFic};
end

for k=1:length(nomFic)
    a = cl_reson_s7k('nomFic', nomFic{k});

    if k == 1
        identSondeur = selectionSondeur(a);
    end

    Data = read_navigation(a, identSondeur);
    if isempty(Data) || isempty(Data.Latitude)
        str1 = sprintf('Le fichier %s ne contient pas de navigation, il doit s''agir d''un fichier provenant du 7KCenter.', nomFic{k});
        str2 = sprintf('%s file does not contain any navigation data, it seems to be a file coming from the 7KCenter.', nomFic{k});
        my_warndlg(Lang(str1,str2), 0, 'Tag', 's7kNavigationEmpty');
        continue
    end
    
    [flag, Data] = checkFishLatLon(a, Data);
    if flag
        [nomDir, nomFicSeul] = fileparts(nomFic{k});
        nomFicMat  = fullfile(nomDir, 'SonarScope', [nomFicSeul '_navigation.mat']);
        ncFileName = fullfile(nomDir, 'SonarScope', [nomFicSeul '.nc']);
        
        %  Data = loadmat(nomFicMat, 'nomVar', 'Data');
        if useCacheNetcdf == 1 % 1=Mat, 2 et 3=Netcdf
            save(nomFicMat, 'Data')
        else
            Data.Dimensions.nbPoints = length(Data.Latitude);
            NetcdfUtils.Struct2Netcdf(Data, ncFileName, 'Navigation');
        end 
    end
%{     
    figure; PlotUtils.createSScPlot(Data.Longitude, Data.Latitude); grid on; hold on; title('Nav'); axisGeo
    figure;
    subplot(2,1,1)
    PlotUtils.createSScPlot(Data.Time, Data.VesselHeight); grid on; title('VesselHeight'); hold on
    subplot(2,1,2)
    PlotUtils.createSScPlot(Data.Time, Data.Heading); grid on; title('Heading'); hold on; 
%}
    
%     Data.Latitude  = filtrageButterSignal(Data.Latitude,  Wc, Ordre, 'Angle');
%     Data.Longitude = filtrageButterSignal(Data.Longitude, Wc, Ordre, 'Angle');
end


function [flag, Data] = checkFishLatLon(this, Data)

[~, nomFicSeul] = fileparts(this.nomFic);

Format1 = 'V�rification de la %s.\n\nUne fen�tre SignalScope va s''ouvrir, le signal de %s est en noir et une version filtr�e est en rouge. Zoomez sur le signal pour voir si la version filtr�e est meilleure (elle rem�die souvent � des marches d''escalier dues � une quantification de la mesure de positionnemen).\n\nSi vous conservez le filtre et que vous clickez sur Save et Quit c''est lui qui sera sauvegard�.';
Format2 = 'Checking %s.\n\nA SignalScope window is going to be open, %s signal is the black one, the red one is a filtered version. Zoom on the signal to check if the filter is efficient (it often solves problems of stair steps due to signal quantification).\n\nIf you conserve the filter AND you click on Save and Quit il will be the red curve which will be saved.';
Format3 = 'V�rification de la %s.\n\nUne fen�tre SignalScope va s''ouvrir. Zoomez sur le signal pour voir si un filtrage s''impose.';
Format4 = 'Checking %s.\n\nA SignalScope window is going to be opened. Zoom on the signal to check if a filter is necessary.';

%% Trac� de la navigation

% FigUtils.createSScFigure; 
% h(1) = subplot(); 

%% Contr�le de la latitude et de la longitude du bateau

str1 = sprintf('Voulez-vous v�rifier la latitude du poisson pour "%s" ?', nomFicSeul);
str2 = sprintf('Check latitude and longitude of the fish for "%s" ?', nomFicSeul);
[rep, flag] = my_questdlg(Lang(str1,str2));
if ~flag
    return
end
if rep == 1
    FiltreLonLat.Type  = 4;
    
    str1 = sprintf(Format1, 'Longitude', 'Longitude');
    str2 = sprintf(Format2, 'Longitude', 'Longitude');
    my_warndlg(Lang(str1,str2), 1);
    FishLongitude = Data.Longitude;
    FishLongitude = controleSignalNEW('FishLongitude', FishLongitude, 'Filtre', FiltreLonLat);
    Data.Longitude = FishLongitude;
%     flag = save_signal(this, 'fishLon',  FishLongitude);
%     if ~flag
%         return
%     end
        
    str1 = sprintf(Format1, 'Latitude', 'Latitude');
    str2 = sprintf(Format2, 'Latitude', 'Latitude');
    my_warndlg(Lang(str1,str2), 1);
    FishLatitude = Data.Latitude;
    FishLatitude = controleSignalNEW('FishLatitude', FishLatitude, 'Filtre', FiltreLonLat);
    Data.Latitude = FishLatitude;
%     flag = save_signal(this, 'fishLat',  FishLatitude);
%     if ~flag
%         return
%     end
end

%% Contr�le du cap calcul�

str1 = sprintf('Voulez-vous v�rifier le cap du poisson pour "%s" ?', nomFicSeul);
str2 = sprintf('Check the fish heading for "%s" ?', nomFicSeul);
[rep, flag] = my_questdlg(Lang(str1,str2));
if ~flag
    return
end
if rep == 1
    str1 = sprintf(Format3, 'Heading', 'Heading');
    str2 = sprintf(Format4, 'Heading', 'Heading');
    my_warndlg(Lang(str1,str2), 1);
    Heading = Data.Heading;
    Heading = controleSignalNEW('Heading', Heading);
    Data.Heading = Heading;
%     flag = save_signal(this, 'heading',  Heading);
%     if ~flag
%         return
%     end
end

