% Constructeur de cl_reson_s7k (Imagerie contenue dans une .s7k)
%
% Syntax
%   a = cl_reson_s7k(...)
% 
% Name-Value Pair Arguments
%   nomFic   : Nom(s) du fichier .all
%   KeepNcID : Let the Netcdf file open (default : false)
%  
% Output Arguments 
%   a : Instance de cl_reson_s7k
%
% Examples
%   nomFic = getNomFicDatabase('RESON7125_20051116_221245.s7k')
%   nomFic = 'G:\SHOM\20210430\20210222_060230_PP_7150_24kHz-Copie4.s7k';
%   a = cl_reson_s7k('nomFic', nomFic)
%
% See also plot_index histo_index Authors
% Authors : JMA
% ----------------------------------------------------------------------------

classdef cl_reson_s7k < handle
    
    properties(GetAccess = 'public', SetAccess = 'public')
        nomFic      = '';
        nomFicIndex = '';
        SounderName = '';
        
        ncID                           = [];
        ExistGrpIndex                  = false;
        ExistGrpAttitude               = false;
        ExistGrpVerticalDepth          = false;
        ExistGrpNavigation             = false;
        ExistGrpSoundSpeedProfile      = false;
        ExistGrpDepth                  = false;
        ExistGrpWaterColumnDataIFRData = false;
        ExistGrpWCD                    = false;
        ExistGrpBeamRawData            = false;
        ExistGrpMagPhase               = false;
    end
    
    methods (Access = 'public')
        function this = cl_reson_s7k(varargin)
            this = set(this, varargin{:});
        end
    end
end
