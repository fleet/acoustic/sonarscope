%   filename = 'G:\Norbit\Janvier2021\WithSnippets\20201112_223120-190kHz.s7k';
%   s7k = cl_reson_s7k('nomFic', filename); %, 'KeepNcID', true);

function this = cre_ficIndex(this)

[nomDir, nomFic] = fileparts(this.nomFic);

% typeIndian = getIndians7k(this.nomFic);
typeIndian = 'l';

fid = fopen(this.nomFic, 'r', typeIndian);
if fid == -1
    str = ['Impossible d''ouvrir le fichier ' this.nomFic];
    my_warndlg(str, 1);
    return
end

%% On cr�e au besoin le r�pertoire SonarScope

nomDirMat = fullfile(nomDir, 'SonarScope');
if ~exist(nomDirMat, 'dir')
    [status, msg] = mkdir(nomDirMat); %#ok
    if ~status
        fprintf('Impossible de c�er le r�pertoire %s.\nJe vous conseille de rendre cela possible, ca permet de formater la donnee en fichiers matlab,\nce qui permettra d''acc�der beaucoup plus vite � la donn�e\n', nomDirMat);
    end
end

%% Lecture de la taille du fichier .all

% fseek(fid, 0, 'eof');
% nbOcFic = ftell(fid);
fseek(fid, 0, 'bof');

%% Traitement du fichier

ienr = 1;

% Pour eviter des reallocations de memoire a chaque packet, on donne une taille
% importante aux buffers qui vont etre remplis sequentiellement
z = NaN(1, 5000);
Date             = z;
Heure            = z;
Position         = z;
Taille           = z;
TypeDatagram     = z;
PingCounter      = z;
PingSequence     = z;
DeviceIdentifier = z;

nbFaisceaux = [];

strLoop = sprintf('S7K cache makeup : %s : Group index', nomFic);
my_warndlg([strLoop ' : Begin'], 0);
N = sizeFic(this.nomFic);

% ihOld = 0;
while 1
    PingNumber = 0;
    Sequence   = 0;

    PositionCourante = ftell(fid);
    infoLoop(strLoop, PositionCourante, N, 'bytes')

    [flag, DRF] = fread_DRF(fid);
    if ~flag
        break
    end
    
    if DRF.Size == 0 % Cas du 101i�me datagramme du fichier "F:\Ifremer\Delphine\7150\20210417_225447_PP_7150_12kHz.s7k"
    %{
    	fseek(fid, PreviousPositionCourante, 'bof');
        % TODO : recherche it�rative du datagramme suivant
        PositionCourante = searchNextDatagramme(fid); % Fonction � �crire
    %}
    end

    try
        switch DRF.RecordTypeIdentifier
            case 1000 % Reference Point
                flag = fread_RecordType_1000(fid);
                if ~flag
                    break
                end
                
            case 1001 % Sensor Offset Position
                flag = fread_RecordType_1001(fid);
                if ~flag
                    break
                end
                
            case 1002 % Calibrated Sensor Offset Position
                flag = fread_RecordType_1002(fid);
                if ~flag
                    break
                end
            
            case 1003 % Position
                flag = fread_RecordType_1003(fid);
                if ~flag
                    break
                end
                
            case 1004 % Custom Attitude Information
                flag = fread_RecordType_1004(fid);
                if ~flag
                    break
                end
                
            case 1005 % Tide 
                flag = fread_RecordType_1005(fid);
                if ~flag
                    break
                end

            case 1006 % Altitude
                flag = fread_RecordType_1006(fid);
                if ~flag
                    break
                end
                
            case 1007 % Motion Over Ground
                flag = fread_RecordType_1007(fid);
                if ~flag
                    break
                end

            case 1008 % Depth
                flag = fread_RecordType_1008(fid);
                if ~flag
                    break
                end

            case 1009 % Sound Velocity Profile
                flag = fread_RecordType_1009(fid);
                if ~flag
                    break
                end

            case 1010 % CTD
                [flag, Data] = fread_RecordType_1010(fid);
                if ~flag
                    break
                end
                
            case 1011 % Geodesy 
                [flag, Data] = fread_RecordType_1011(fid);
                if ~flag
                    break
                end

            case 1012 % Roll Pitch Heave
                [flag, Data] = fread_RecordType_1012(fid);
                if ~flag
                    break
                end

            case 1013 % Heading
                [flag, Data] = fread_RecordType_1013(fid);
                if ~flag
                    break
                end
                
            case 1014 % Survey Line
                 [flag, Data] = fread_RecordType_1014(fid);
                if ~flag
                    break
                end

            case 1015 % Navigation
                flag = fread_RecordType_1015(fid);
                if ~flag
                    break
                end

            case 1016 % Attitude
                flag = fread_RecordType_1016(fid);
                if ~flag
                    break
                end
                
            case 1050 % Single Record Request
                flag = fread_RecordType_1050(fid);
                if ~flag
                    break
                end
                
            case 1200 % Start Recording
                flag = fread_RecordType_1200(fid);
                if ~flag
                    break
                end
                
            case 2000 % XYZ Data
                flag = fread_RecordType_2000(fid);
                if ~flag
                    break
                end

            case 7000 % 7k Sonar Settings
                [flag, Data] = fread_RecordType_7000(fid);
                if ~flag
                    break
                end
                PingNumber = Data.RTH.PingCounter;
                Sequence   = Data.RTH.MultiPingSequence;

            case 7001 % 7k Configuration
                flag = fread_RecordType_7001(fid);
                if ~flag
                    break
                end

            case 7002 % 7k Pulse Compression
                [flag, Data] = fread_RecordType_7002(fid);
                if ~flag
                    break
                end

            case 7004 % 7k Beam Geometry
                flag = fread_RecordType_7004(fid);
                if ~flag
                    break
                end
                
            case 7005 % 7k Calibration Data
                flag = fread_RecordType_7005(fid);
                if ~flag
                    break
                end

            case 7006 % 7k Bathymetric Data
                [flag, PingNumber, Sequence, nbFaisceaux] = fread_RecordType_7006(fid, DRF);
                if ~flag
                    break
                end

            case 7007 % 7k Backscatter Imagery Data
                [flag, PingNumber, Sequence] = fread_RecordType_7007(fid);
                if ~flag
                    break
                end

            case 7008 % 7K Generic Data (Mag & Phase)
                [flag, PingNumber, Sequence] = fread_RecordType_7008(fid, DRF);
                if ~flag
                    break
                end

            case 7009 % Vertical Depth
                [flag, PingNumber, Sequence] = fread_RecordType_7009(fid);
                if ~flag
                    break
                end

            case 7010 % TVG Gain Values
                flag = fread_RecordType_7010(fid);
                if ~flag
                    break
                end

            case 7011 % 7k Image Data
                [flag, Data] = fread_RecordType_7011(fid, DRF);
                if flag == -1
                    continue
                end
                if ~flag
                    break
                end

            case 7017 % (7k Beamformed Data) � FP1 Release 7125/7101
                flag = fread_RecordType_7017(fid);
                if flag == -1
                    continue
                end
                if ~flag
                    break
                end

            case 7018 % (7k Beamformed Data) � FP1 Release 7125/7101
                flag = fread_RecordType_7018(fid);
                if flag == -1
                    continue
                end
                if ~flag
                    break
                end

            case 7021 % TODO Quel est son nom ?
                [flag, Data] = fread_RecordType_7021(fid, DRF);
                if flag == -1
                    continue
                end
                if ~flag
                    break
                end

            case 7022 % S7k Center Version
                [flag, Data] = fread_RecordType_7022(fid);
                if flag == -1
                    continue
                end
                if ~flag
                    break
                end

            case 7027 % 7k RAW Detection Data
                [flag, PingNumber, Sequence, nbFaisceaux] = fread_RecordType_7027(fid, DRF); 
                if flag == -1
                    continue
                end
                if ~flag
                    break
                end

            case 7028 % Snippet Data
                [flag, PingNumber, Sequence] = fread_RecordType_7028(fid, nbFaisceaux); 
                if flag == -1
                    continue
                end
                if ~flag
                    break
                end

            case 7030 % SonarInstallation Parameters
                [flag, Data] = fread_RecordType_7030(fid);
                if ~flag
                    break
                end

            case 7038 % Raw I&Q Data
                [flag, Data] = fread_RecordType_7038(fid, DRF);
                if flag == -1
                    continue
                end
                if ~flag
                    break
                end

            case 7041 % Compressed Beamformed Magnitude Data
                [flag, Data] = fread_RecordType_7041(fid, DRF, 'noRawData');
                if flag == -1
                    continue
                end
                if ~flag
                    break
                end
                PingNumber = Data.RTH.PingCounter;
                Sequence   = Data.RTH.MultiPingSequence;

           case 7042 % Compressed Watercolumn Data
                [flag, Data] = fread_RecordType_7042(fid, DRF, 'noRawData');
                if flag == -1
                    continue
                end
                if ~flag
                    break
                end
                PingNumber = Data.RTH.PingCounter;
                Sequence   = Data.RTH.MultiPingSequence;

            case 7048 % 7k Calibrated Beam Data
                flag = fread_RecordType_7048(fid);
                if flag == -1
                    continue
                end
                if ~flag
                    break
                end
                
            case 7050 % 7k System Events
                flag = fread_RecordType_7050(fid);
                if flag == -1
                    continue
                end
                
            case 7051 % 7k System Event Message
                flag = fread_RecordType_7051(fid);
                if flag == -1
                    continue
                end

            case 7057 % Calibrated Backscatter Imagery Data
                [flag, PingNumber, Sequence] = fread_RecordType_7057(fid);
                if ~flag
                    break
                end

            case 7058 % Calibrated Snippet Data
                [flag, PingNumber, Sequence] = fread_RecordType_7058(fid, DRF, nbFaisceaux);
                if ~flag
                    break
                end

            case 7200 % 7k File Header (First record of the file)
                flag = fread_RecordType_7200(fid);
                if ~flag
                    break
                end

                %         case 7021   % ??????????????????????????????????????
                %             [flag, Data] = fread_RecordType_7021(fid, DRF); %#ok
                %             if ~flag
                %                 break
                %             end

                %         case 7300   % ??????????????????????????????????????
                %             [flag, Data] = fread_RecordType_7021(fid, DRF); %#ok
                %             if ~flag
                %                 break
                %             end

            case 7300 % Last record of the file
                [flag, Data] = fread_RecordType_7300(fid, DRF);
                if flag == -1
                    continue
                end
                if ~flag
                    break
                end
                
            case 7400 % Time Message
                [flag, Data] = fread_RecordType_7400(fid);
                if flag == -1
                    continue
                end
                
            case 7504 % ????
                [flag, Data] = fread_RecordType_7504(fid, DRF);
                if flag == -1
                    continue
                end
                
            case 7500 % 7k Remote Control
                [flag, Data] = fread_RecordType_7500(fid);
                if ~flag
                    break
                end
                
            case 7501 % 7k Remote Control Acknowledge
                [flag, Data] = fread_RecordType_7501(fid);
                if ~flag
                    break
                end
                
            case 7502 % 7k Remote Control Not Acknowledge
                [flag, Data] = fread_RecordType_7502(fid);
                if ~flag
                    break
                end
                
            case 7503 % Remote Control Sonar Settings
                [flag, Data] = fread_RecordType_7503(fid);
                if ~flag
                    break
                end
                
            case 7610 % 7k Sound Velocity
                [flag, Data] = fread_RecordType_7610(fid);
                if ~flag
                    break
                end
                
            case 7611 % 7k Absorption Loss
                [flag, Data] = fread_RecordType_7611(fid);
                if ~flag
                    break
                end
                
            case 7612 % 7k Spreading Loss
                [flag, Data] = fread_RecordType_7612(fid);
                if ~flag
                    break
                end
                
            % case 7060 7600 7601 8100 : fonctions de lecture pr�sentes
                % mais pas retrouv� de documentation
                
            otherwise
                if DRF.RecordTypeIdentifier == 0
                    str = sprintf('RecordTypeIdentifier=%f This is a bug', DRF.RecordTypeIdentifier);
                    my_warndlg(str, 0, 'Tag', 'RecordTypeIdentifier not ready');
                    break
                elseif DRF.RecordTypeIdentifier <= 8100
%                     str = sprintf('RecordTypeIdentifier=%d not ready', DRF.RecordTypeIdentifier);
%                     my_warndlg(str, 0, 'Tag', 'RecordTypeIdentifier not ready');
                else
                    str = sprintf('RecordTypeIdentifier=%f This is a bug', DRF.RecordTypeIdentifier);
                    my_warndlg(str, 0, 'Tag', 'RecordTypeIdentifier not ready');
                    break
                end
        end
        
        if DRF.Size == 0 % Ajout� par JMA le 27/07/2016 pour fichiers Jean-Guy 20160310_083050_VWFS_Deneb.s7k
            % TODO : rechercher un nouveau DRF
            break
        end

        Date(ienr)             = DRF.Date;
        Heure(ienr)            = DRF.Time;
        Taille(ienr)           = DRF.Size;
        Position(ienr)         = PositionCourante;
        TypeDatagram(ienr)     = DRF.RecordTypeIdentifier;
        PingCounter(ienr)      = PingNumber;%DRF.RecordCount;
        PingSequence(ienr)     = Sequence;
        DeviceIdentifier(ienr) = DRF.DeviceIdentifier;

        %disp(sprintf('Packet %d - %s  %s - Type %d', ...
        %ienr, dayIfr2str(Date(ienr)), hourIfr2str(Heure(ienr)), TypeOfDatagram));
        ienr = ienr + 1;

        PreviousPositionCourante = PositionCourante;
        PositionCourante = PositionCourante + DRF.Size;
        
        % fprintf('Rec=%d  Ident=%d  Size=%d  Posi=%d / %d\n', ienr, DRF.RecordTypeIdentifier, DRF.Size, PositionCourante, N);
        
        fseek(fid, PositionCourante, 'bof');

        if ienr > length(Date)
            Date             = [Date              z]; %#ok<AGROW>
            Heure            = [Heure             z]; %#ok<AGROW>
            Position         = [Position          z]; %#ok<AGROW>
            Taille           = [Taille            z]; %#ok<AGROW>
            TypeDatagram     = [TypeDatagram      z]; %#ok<AGROW>
            PingCounter      = [PingCounter       z]; %#ok<AGROW>
            PingSequence     = [PingSequence      z]; %#ok<AGROW>
            DeviceIdentifier = [DeviceIdentifier  z]; %#ok<AGROW>
        end
    catch %#ok<CTCH>
        my_warndlg([strLoop ' : Failed'], 0);
        my_warndlg(str, 0);
        break
    end
end
fclose(fid);
my_warndlg([strLoop ' : End'], 0);

%% Ecriture du fichier d'index

n = ienr-1;
Date             = Date(1:n);
Heure            = Heure(1:n);
Position         = Position(1:n);
Taille           = Taille(1:n);
TypeDatagram     = TypeDatagram(1:n);
PingCounter      = PingCounter(1:n);
PingSequence     = PingSequence(1:n);
DeviceIdentifier = DeviceIdentifier(1:n);

% Bidouille pour contourner pb double enregistrement Jack pour mission
% essai f�vrier 2008
DeviceIdentifier(TypeDatagram == 1010) = 7000;
% Fin bidouille


listeDevice = unique(DeviceIdentifier(~isnan(DeviceIdentifier) & (DeviceIdentifier >= 7111)));
if isempty(listeDevice)
    listeDevice = unique(DeviceIdentifier(~isnan(DeviceIdentifier) & (DeviceIdentifier == 1002)));
end

if isempty(listeDevice)
    % #GFORGE 367 : trac� de navigation lors de la mission MAGIC.
    % DEB GLU : correctif pour la version PDS 3.0.7.30 qui enregistre le
    % deive ID 7003 dans tous les datagrammes (Mission MAGIC : fichier
    % 20120814_094840_PP-7150-12kHz-.s7k)
    dummy =  {'7125';'7150';'Ocean Stalwart';'Owen'};
    Index =  regexp(nomFic, dummy);
    if Index{1}
        listeDevice = str2double(dummy{1});
    elseif Index{2}
        listeDevice = str2double(dummy{2});
    elseif Index{3}
        listeDevice = 7150;
    elseif Index{4}
        listeDevice = 20;
    else
        nomFicEmpty = [this.nomFic '_EMPTY'];
        str1 = sprintf('Instrument non identifi� : SVP v�rifiez le contenu du fichier de donn�es (avec DataView.exe par exemple). "%s" est renomm� en "%s".', this.nomFic, nomFicEmpty);
        str2 = sprintf('Device not identified : please check the Content of the Data File (DataView.exe for example). "%s" is renamed in "%s"', this.nomFic, nomFicEmpty);
        my_warndlg(Lang(str1,str2), 0);
%         my_warndlg(Lang(str1,str2), 1, 'Tag', 'NoSounderData');
        % str = sprintf('File "%s." do not contain any sounder data, you should remove it from your directory.', this.nomFic);
        % my_warndlg(str, 0, 'Tag', 'NoSounderData');
        my_rename(this.nomFic, nomFicEmpty)
    end
       
end

for k=1:length(listeDevice)
    nomFicIndex = setNomFicIndex(this, listeDevice(k));

    %% Cr�ation du fichier d'index

    [~, ~, Ext] = fileparts(nomFicIndex);
    if strcmp(Ext, '.index')
        fidIndex = fopen(nomFicIndex, 'w+', 'l');
        if fidIndex == -1
            messageErreurFichier(nomFicIndex, 'WriteFailure');
            return
        end
        fclose(fidIndex);
    end

    sub = find((DeviceIdentifier < 7111) | (DeviceIdentifier == listeDevice(k)));

    Index = zeros(8, length(sub));
    Index(1,:) = Date(sub);
    Index(2,:) = Heure(sub);
    Index(3,:) = Position(sub);
    Index(4,:) = Taille(sub);
    Index(5,:) = TypeDatagram(sub);
    Index(6,:) = PingCounter(sub);
    Index(7,:) = PingSequence(sub);
    Index(8,:) = DeviceIdentifier(sub);

    [~, ~, Ext] = fileparts(nomFicIndex);
    if strcmp(Ext, '.index')
        fidIndex = fopen(nomFicIndex, 'w+', 'l');
        fwrite(fidIndex, Index, 'uint32');
        fclose(fidIndex);
    else
        clear Data
        Data.SounderName = listeDevice(k);
        Data.Index = Index;
        [Data.Dimensions.nbColumns, Data.Dimensions.nbDatagrams] = size(Index);
        NetcdfUtils.Struct2Netcdf(Data, nomFicIndex, 'Index', 'ncID', this.ncID)
%         DataVerif = NetcdfUtils.Netcdf2Struct(ncFileName, 'Index', 'ncID', this.ncID);
    end
    
    this.SounderName = listeDevice(k);
    
    %% Infos � l'�cran

%     fprintf(' - Nb datagrams : %6d - %s  %s - %s  %s\n', n, dayIfr2str(Date(1)), hourIfr2str(Heure(1)), dayIfr2str(Date(n)), hourIfr2str(Heure(n)));
end
