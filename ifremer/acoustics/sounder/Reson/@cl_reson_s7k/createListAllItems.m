function strSummary = createListAllItems(~)
 
strSummary = {};
strSummary{end+1} = 'Directory';
strSummary{end+1} = 'File name';
strSummary{end+1} = 'Sounder name';
strSummary{end+1} = 'Nb pings';
strSummary{end+1} = 'Nb beams';
strSummary{end+1} = 'Main frequency (kHz)';
strSummary{end+1} = 'NbSwaths';
strSummary{end+1} = 'Depth (mean)';
strSummary{end+1} = 'Min Depth (m)';
strSummary{end+1} = 'Max Depth (m)';
strSummary{end+1} = 'swath width (m)';
strSummary{end+1} = 'Angular range (deg)';
strSummary{end+1} = 'Swath / Depth';
strSummary{end+1} = 'Inter ping distance (m)';
strSummary{end+1} = 'Recurrence inter ping (s)';
strSummary{end+1} = 'Line length (m)';
strSummary{end+1} = 'Line coverage (km2)';
strSummary{end+1} = 'QF-mean';
strSummary{end+1} = 'BS-mean (dB)';
strSummary{end+1} = 'WC';
strSummary{end+1} = 'Mag&Phase';
strSummary{end+1} = 'Heading (deg)';
strSummary{end+1} = 'Direction (deg)';
strSummary{end+1} = 'Type Line';
strSummary{end+1} = 'Speed (nd)';
strSummary{end+1} = 'Nb Sound Speed Profiles';
strSummary{end+1} = 'Surface sound speed (m/s)';
strSummary{end+1} = 'Absorption (dB/km)';
strSummary{end+1} = 'Latitude';
strSummary{end+1} = 'Longitude';
strSummary{end+1} = 'Roll-mean (deg)';
strSummary{end+1} = 'Roll-std (deg)';
strSummary{end+1} = 'Pitch-mean (deg)';
strSummary{end+1} = 'Pitch-std (deg)';
strSummary{end+1} = 'Heave-mean (m)';
strSummary{end+1} = 'Heave-std (m)';
strSummary{end+1} = 'Signal length (ms)';
strSummary{end+1} = 'Nb Pulse lengths';
strSummary{end+1} = 'Resolution (m)';
strSummary{end+1} = 'Snippets sampling rate - med (kHz)';
strSummary{end+1} = 'Nb snippet sampling rates';

%{
% Comment� pour l'instant car il faut aller lire la valeur dans chaque
datagramme de WC. On attend la refonte pour avoir un acc�s direct � cette
information
strSummary{end+1} = 'WC sampling rate - med (Hz)';
strSummary{end+1} = 'Nb WC sampling rates';
%}

strSummary{end+1} = 'RangeSelection - med (m)';
strSummary{end+1} = 'Nb Range Selection';
strSummary{end+1} = 'PowerSelection - med (dB)';
strSummary{end+1} = 'Nb Power Selection';
strSummary{end+1} = 'GainSelection - med (dB)';
strSummary{end+1} = 'Nb Gain Selection';

%{
strSummary{end+1} = 'Nb pings monoping';
strSummary{end+1} = 'Nb pings multiping';
%}
strSummary{end+1} = 'Nb pings with WC';
strSummary{end+1} = 'Nb pings without WC';
strSummary{end+1} = 'Nb pings with Mag&Phase';
strSummary{end+1} = 'Nb pings without Mag&Phase';
strSummary{end+1} = 'BottomDetectDepthFilterFlag';

%{
- c�l�rit� du profil  en surface (1010)
- le ping p�riod (valeur moyenne?)
%}

strSummary{end+1} = 'Nb pings Sequence 0';
strSummary{end+1} = 'Nb pings Sequence 1';
strSummary{end+1} = 'Nb pings Sequence 2';
strSummary{end+1} = 'Nb pings Sequence 3';
strSummary{end+1} = 'Nb pings Sequence 4';

strSummary{end+1} = 'File size';

strSummary{end+1} = 'Time beginning';
strSummary{end+1} = 'Time end';
strSummary{end+1} = 'Duration';