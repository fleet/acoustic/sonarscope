%% TODO : quand Pierre aura r�alis� les am�liorations Signal/SignalIndex avec matlab.mixin.Heterogeneous alors sortir des signaux du genre signalsBDA, signalsXXX, ...

function [flag, signalContainerList] = create_additionalSignals(this, DataDepth, identSondeur, varargin)

[varargin, DataIndex]    = getPropertyValue(varargin, 'DataIndex',    []);
[varargin, DataAttitude] = getPropertyValue(varargin, 'DataAttitude', []);
[varargin, DataPosition] = getPropertyValue(varargin, 'DataPosition', []); %#ok<ASGLU>

signalContainerList = SignalContainer.empty();

S0 = cl_reson_s7k.empty();

if isempty(DataPosition)
    if isempty(DataIndex)
        [flag, DataIndex] = readFicIndex(this, identSondeur);
        if ~flag
            return
        end
    end
    DataPosition = read_navigation(this, identSondeur, 'DataIndex', DataIndex);
end

if isempty(DataAttitude)
    DataAttitude = read_attitude(this, identSondeur, 'DataIndex', DataIndex);
    if isempty(DataAttitude) || ~isfield(DataAttitude, nomSignal) || isempty(DataAttitude.(nomSignal))
        flag = 0;
        return
    end
end

%% Cr�ation de l'instance XSample

TsubDepth = DataDepth.Datetime;
timeSample = XSample('name', 'time', 'data', TsubDepth);

[SonarTime, Latitude, Longitude, Heading, CourseVessel, Immersion, provenance] = getNavFromVariousDatagrams(S0, DataPosition, DataDepth); %#ok<ASGLU>

%% Speed

tagName = 'Speed';
X = my_interp1(DataPosition.Datetime(:), DataPosition.Speed(:), TsubDepth(:));
ySampleList = YSample('data', X, 'unit', 'm/s');
Signals = ClSignal('name', tagName, 'xSample', timeSample, 'ySample', ySampleList, 'tag', tagName);

%% CourseVessel

tagName = 'CourseVessel';
X = CourseVessel(:);
ySampleList = YSample('data', X, 'unit', 'deg');
Signals(end+1) = ClSignal('name', tagName, 'xSample', timeSample, 'ySample', ySampleList, 'tag', tagName);

%% CourseVessel

tagName = 'Drift';
X = CourseVessel(:) - Heading(:);
ySampleList = YSample('data', X, 'unit', 'deg');
Signals(end+1) = ClSignal('name', tagName, 'xSample', timeSample, 'ySample', ySampleList, 'tag', tagName);

%% RollMean

[flag, Z] = create_signal_Attitude(this, identSondeur, 'Roll', 'Mean', 'DataAttitude', DataAttitude);
if flag
    tagName = 'RollMean';
    X = my_interp1(Z.Datetime, Z.Data, TsubDepth(:));
    ySampleList = YSample('data', X, 'unit', 'deg');
    Signals(end+1) = ClSignal('name', tagName, 'xSample', timeSample, 'ySample', ySampleList, 'tag', tagName);
end

%% RollStd

[flag, Z] = create_signal_Attitude(this, identSondeur, 'Roll', 'Std', 'DataAttitude', DataAttitude);
if flag
    tagName = 'RollStd';
    X = my_interp1(Z.Datetime, Z.Data, TsubDepth(:));
    ySampleList = YSample('data', X, 'unit', 'deg');
    Signals(end+1) = ClSignal('name', tagName, 'xSample', timeSample, 'ySample', ySampleList, 'tag', tagName);
end

%% PitchMean

[flag, Z] = create_signal_Attitude(this, identSondeur, 'Pitch', 'Mean', 'DataAttitude', DataAttitude);
if flag
    tagName = 'PitchMean';
    X = my_interp1(Z.Datetime, Z.Data, TsubDepth(:));
    ySampleList = YSample('data', X, 'unit', 'deg');
    Signals(end+1) = ClSignal('name', tagName, 'xSample', timeSample, 'ySample', ySampleList, 'tag', tagName);
end

%% PitchStd

[flag, Z] = create_signal_Attitude(this, identSondeur, 'Pitch', 'Std', 'DataAttitude', DataAttitude);
if flag
    tagName = 'PitchStd';
    X = my_interp1(Z.Datetime, Z.Data, TsubDepth(:));
    ySampleList = YSample('data', X, 'unit', 'deg');
    Signals(end+1) = ClSignal('name', tagName, 'xSample', timeSample, 'ySample', ySampleList, 'tag', tagName);
end

%% HeaveMean

[flag, Z] = create_signal_Attitude(this, identSondeur, 'Heave', 'Mean', 'DataAttitude', DataAttitude);
if flag
    tagName = 'HeaveMean';
    X = my_interp1(Z.Datetime, Z.Data, TsubDepth(:));
    ySampleList = YSample('data', X, 'unit', 'deg');
    Signals(end+1) = ClSignal('name', tagName, 'xSample', timeSample, 'ySample', ySampleList, 'tag', tagName);
end

%% HeaveStd

[flag, Z] = create_signal_Attitude(this, identSondeur, 'Heave', 'Std', 'DataAttitude', DataAttitude);
if flag
    tagName = 'HeaveStd';
    X = my_interp1(Z.Datetime, Z.Data, TsubDepth(:));
    ySampleList = YSample('data', X, 'unit', 'm');
    Signals(end+1) = ClSignal('name', tagName, 'xSample', timeSample, 'ySample', ySampleList, 'tag', tagName);
end

%% MeanQF

dataFromPP = (identSondeur == 7150) || (identSondeur == 7111);
if dataFromPP % Donn�es Ifremer du PP?
    I = DataDepth.Reflectivity(:,:);
else
%     my_breakpoint % Pas test�
    I = DataDepth.QualityFactor(:,:);
%     I = -log10(I)-1;
end
X = mean(I, 2, 'omitnan');
tagName = 'MeanQF';
ySampleList = YSample('data', X, 'unit', '');
Signals(end+1) = ClSignal('name', tagName, 'xSample', timeSample, 'ySample', ySampleList, 'tag', tagName);

%% NbOfBeams

MeanVal = sum(~isnan(DataDepth.Depth(:,:)), 2);
tagName = 'NbOfBeams';
ySampleList = YSample('data', MeanVal, 'unit', '');
Signals(end+1) = ClSignal('name', tagName, 'xSample', timeSample, 'ySample', ySampleList, 'tag', tagName);

%% NbOfBeamsStd

ConstanteDeTemps = 1*60 ; % 1 mn
[~, StdVal] = filtreAttitude(DataDepth.Time, MeanVal, ConstanteDeTemps); 
tagName = 'NbOfBeamsStd';
ySampleList = YSample('data', StdVal, 'unit', '');
Signals(end+1) = ClSignal('name', tagName, 'xSample', timeSample, 'ySample', ySampleList, 'tag', tagName);

%% DepthMean

MeanVal = mean(DataDepth.Depth(:,:), 2, 'omitnan');
tagName = 'DepthMean';
ySampleList = YSample('data', MeanVal, 'unit', 'm');
Signals(end+1) = ClSignal('name', tagName, 'xSample', timeSample, 'ySample', ySampleList, 'tag', tagName);

%% DepthStd

subNonNan = find(~isnan(MeanVal));
if isempty(subNonNan)
    StdVal = MeanVal;
else
    [~, StdVal] = filtreAttitude(DataDepth.Time(subNonNan), MeanVal(subNonNan), ConstanteDeTemps);
    StdVal =  my_interp1(DataDepth.Datetime(subNonNan), StdVal, TsubDepth(:));
end
tagName = 'DepthStd';
ySampleList = YSample('data', StdVal, 'unit', 'm');
Signals(end+1) = ClSignal('name', tagName, 'xSample', timeSample, 'ySample', ySampleList, 'tag', tagName);

%% Create signalContainers

% Aperture SignalContainer
signalContainerList = SignalContainer('signalList', Signals, 'name', 'OtherSignals', 'tag', 'Others');
signalContainerList.originFilename = this.nomFic;

%% The end

flag = 1;
