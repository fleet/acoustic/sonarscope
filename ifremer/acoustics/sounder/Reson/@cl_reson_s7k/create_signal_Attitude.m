function [flag, Z] = create_signal_Attitude(this, identSondeur, nomSignal, TypeVar, varargin)

[varargin, DataAttitude] = getPropertyValue(varargin, 'DataAttitude', []); %#ok<ASGLU>

flag = 1;

if isempty(DataAttitude)
    DataAttitude = read_attitude(this, identSondeur);
    if isempty(DataAttitude) || ~isfield(DataAttitude, nomSignal) || isempty(DataAttitude.(nomSignal))
        Z.Data = [];
        Z.Unit = '';
        flag = 0;
        return
    end
else
    flag = isfield(DataAttitude, nomSignal);
    if ~flag
        Z.Data = [];
        Z.Unit = '';
        flag = 0;
        return
    end
end

if isempty(DataAttitude.Datetime)
    Z.Data = [];
    Z.Unit = '';
    flag = 0;
    return
end

ConstanteDeTemps = 1*60 ; % 1 mn
[MeanVal, StdVal] = filtreAttitude(DataAttitude.Time, DataAttitude.(nomSignal)(:), ConstanteDeTemps);

switch nomSignal
    case {'Roll'; 'Pitch'}
        Unit = 'deg';
    case {'Heave'; 'TrueHeave'; 'Draught'; 'Tide'}
        Unit = 'm';
    otherwise
        Unit = 'Undefined';
end

Z.Time     = DataAttitude.Time;
Z.Datetime = DataAttitude.Datetime;
switch TypeVar
    case 'Mean'
        Z.Data = MeanVal;
    case 'Std'
        Z.Data = StdVal;
    case 'Value'
        Z.Data = DataAttitude.(nomSignal)(:);
    otherwise
        'Pas encore pr�vu au programme.'
end
Z.Unit = Unit;
