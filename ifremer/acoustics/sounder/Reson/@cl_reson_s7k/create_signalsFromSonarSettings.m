%% TODO : quand Pierre aura r�alis� les am�liorations Signal/SignalIndex avec matlab.mixin.Heterogeneous alors sortir des signaux du genre signalsBDA, signalsXXX, ...

function [flag, signalContainerList] = create_signalsFromSonarSettings(this, TsubDepth, identSondeur, varargin)

[varargin, DataSonarSetting] = getPropertyValue(varargin, 'DataSonarSetting', []); %#ok<ASGLU>

%init signal arrays
Signals = ClSignal.empty();

%% Lecture des datagrammes si pas pr�sents

if isempty(DataSonarSetting)
    DataSonarSetting = read_sonarSettings(this, identSondeur);
    if isempty(DataSonarSetting)
        flag = -1;
        return
    end
end

TSonarSetting = DataSonarSetting.Time.Datetime;
%{
TSonarSetting.Format = 'yyyy-MM-dd HH:mm:ss.SSSSS';
TsubDepth.Format = 'yyyy-MM-dd HH:mm:ss.SSSSS';
[TSonarSetting(1:20)' TsubDepth(1:20)']
%}
DataSonarSetting = rmfield(DataSonarSetting, 'Time');
DataSonarSetting = rmfield(DataSonarSetting, 'SystemSerialNumber');

%% Cr�ation de l'instance XSample

timeSample = XSample('name', 'time', 'data', TsubDepth);

%% Boucle sur les signaux

fields = fieldnames(DataSonarSetting);
for k=length(fields):-1:1
    X = DataSonarSetting.(fields{k});
    X = my_interp1(TSonarSetting(:), X(:), TsubDepth(:), 'nearest');
%     X = my_interp1_Extrap_PreviousThenNext(TSonarSetting(:), X(:), TsubDepth(:)); % Modif JMA le 08/01/2021 pour cache S7K en Netcdf
    tagName = fields{k};
    ySampleList = YSample('data', X, 'unit', '?');
    Signals(k) = ClSignal('name', tagName, 'xSample', timeSample, 'ySample', ySampleList, 'tag', tagName);
end

%% Create signalContainers

% Aperture SignalContainer
signalContainerList = SignalContainer('signalList', Signals, 'name', 'SonarSettings', 'tag', 'Aperture');
signalContainerList.originFilename = this.nomFic;

%% The end

flag = 1;
