function delete(this)

if ~isempty(this.ncID)
    try
        fprintf('Closing file "%s"', this.nomFic);
        netcdf.close(this.ncID);
        fprintf(' : Done\n');
    catch
        my_breakpoint
    end
end
