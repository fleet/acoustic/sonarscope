% Representation externe d'une instance sur le terminal
%
% Syntax
%   display(this)
%
% Input Arguments 
%   this : une instance de cl_reson_s7k
%
% Examples
%   nomFic = getNomFicDatabase('RESON7125_20051116_221245.s7k')
%   a = cl_reson_s7k('nomFic', nomFic);
%   display(a);
%
% See also cl_reson_s7k cl_reson_s7k/char Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function display(this)

s = whos('this');
sz = s.size;
fprintf('Name\tSize\tClass\n');
fprintf('%s\t%s\t%s\n', inputname(1), num2strCode(sz), s.class);
disp(char(this));

