% Export des valeurs d'attitude d'un fichier .s7k
%
% Syntax
%   export_Attitude_S7k(a)
%
% Input Arguments
%   a           : Instance de cl_reson_s7k
%   nomFicAtt   : Nom dui fichier de sortie de l'attitude.
%
% Name-Value Pair Arguments
%   fullListe : 1=affichage que des datagrams presents, 0=tous les datagrams
%
% Output Arguments
%
% Examples
%   nomFic          = getNomFicDatabase('RESON7125_20051116_221245.s7k')
%   a               = cl_reson_s7k('nomFic', nomFic);
%   identSondeur    = selectionSondeur(a(1));
%   export_Attitude_s7k(a, identSondeur, fullfile('E:\Temp', 'RESON7125_20051116_221245_Att.txt'));
%
%   nomDir = '/home1/jackson/7K'
%   liste = listeFicOnDir2(nomDir, '.s7k')
%   for k=1:3%length(liste)
%       a(k) = cl_reson_s7k('nomFic', liste{k});
%   end
%   export_AttitudeS7k(a)
%
% See also cl_reson_s7k plot_index Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function flag = export_Attitude_s7k(this, nomFicAtt, identSondeur, varargin)

flag = 0;
fig1 = figure;
Color = 'bkrmc';
k2 = 0;
t = [];
for k=1:length(this)

    %% Lecture des datagrams d'attitude

    X = read_attitude(this(k), identSondeur);
    if isempty(X) || isempty(X.Pitch)
        str1 = sprintf('Le fichier %s ne contient pas de donn�es d''attitude, il doit s''agir d''un fichier provenant du 7KCenter.', this(k).nomFic);
        str2 = sprintf('%s file does not contain any attitude data, it seems to be a file coming from the 7KCenter.', this(k).nomFic);
        my_warndlg(Lang(str1,str2), 0, 'Tag', 's7kAttitudeEmpty');
        continue
    end
    
    ik = mod(k2,length(Color)) + 1;
    k2 = k2 + 1;
    
    DataAtt(k2) = X; %#ok<AGROW>
    
    figure(fig1);
    subplot(4,1,1)
    PlotUtils.createSScPlot(DataAtt(k2).Time, DataAtt(k2).Roll, Color(ik)); grid on; title('Roll'); hold on
    subplot(4,1,2)
    PlotUtils.createSScPlot(DataAtt(k2).Time, DataAtt(k2).Pitch, Color(ik)); grid on; title('Pitch'); hold on; 
    subplot(4,1,3)
    PlotUtils.createSScPlot(DataAtt(k2).Time, DataAtt(k2).Heave, Color(ik)); grid on; title('Heave'); hold on; 
    subplot(4,1,4)
    PlotUtils.createSScPlot(DataAtt(k2).Time, DataAtt(k2).Heading, Color(ik)); grid on; title('Heading'); hold on;
    drawnow
    
    tAtt = DataAtt(k2).Time.timeMat;
    t(k2) = tAtt(k2); %#ok<AGROW>
end
if isempty(t)
    my_close(fig1)
    return
end
[~, ordre] = sort(t);

tAtt      = [];
Roll      = [];
Pitch     = [];
Heave     = [];
Heading   = [];
% CourseVessel = [];
for k=1:length(t)
    k2 = ordre(k);
    T = DataAtt(k2).Time;
    tAtt = [tAtt T.timeMat]; %#ok<AGROW>
    Roll            = [Roll DataAtt(k2).Roll]; %#ok<AGROW>
    Pitch           = [Pitch DataAtt(k2).Pitch]; %#ok<AGROW>
    Heading         = [Heading DataAtt(k2).Heading]; %#ok<AGROW>
    Heave           = [Heave DataAtt(k2).Heave]; %#ok<AGROW>
end
tAtt = cl_time('timeMat', tAtt);

%% Export dans un fichier Ascii tabul�.

flag = export_att_ascii(nomFicAtt, tAtt, Roll, Pitch, Heave, Heading);
if ~flag
    return
end

flag = 1;