% Trace de la navigation et des capteurs de vitesse et de cap d'un fichier .all
%
% Syntax
%   export_CaraibesNvi(a)
%
% Input Arguments
%   a : Instance de cl_reson_s7k
%
% Name-Value Pair Arguments
%   fullListe : 1=affichage que des datagrams presents, 0=tous les datagrams
%
% Output Arguments
%
% Examples
%   nomFic = getNomFicDatabase('RESON7125_20051116_221245.s7k')
%   a = cl_reson_s7k('nomFic', nomFic);
%   export_CaraibesNvi(a)
%
%   nomDir = '/home1/jackson/7K'
%   liste = listeFicOnDir2(nomDir, '.s7k')
%   for iFic=1:3%length(liste)
%       a(iFic) = cl_reson_s7k('nomFic', liste{iFic});
%   end
%   export_CaraibesNvi(a)
%
% See also cl_reson_s7k plot_index Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function flag = export_CaraibesNvi(this, nomFicNav, identSondeur, varargin)

flag = 0;
fig1 = figure;
fig2 = figure;
Color = 'bkrmc';
k = 0;
t = [];
for iFic=1:length(this)

    %% Lecture des datagrams de navigation

    X = read_navigation(this(iFic), identSondeur);
    if isempty(X) || isempty(X.Latitude)
        str1 = sprintf('Le fichier %s ne contient pas de navigation, il doit s''agir d''un fichier provenant du 7KCenter.', this(iFic).nomFic);
        str2 = sprintf('%s file does not contain any navigation data, it seems to be a file coming from the 7KCenter.', this(iFic).nomFic);
        my_warndlg(Lang(str1,str2), 0, 'Tag', 's7kNavigationEmpty');
        continue
    end
    
    ik = mod(k,length(Color)) + 1;
    k = k + 1;
    
    DataNav(k) = X; %#ok<AGROW>
    
    %% Plots
    
    FigUtils.createSScFigure(fig1); PlotUtils.createSScPlot(DataNav(k).Longitude, DataNav(k).Latitude, Color(ik)); grid on; hold on; title('Nav')
    
    FigUtils.createSScFigure(fig2);
    subplot(2,1,1)
    PlotUtils.createSScPlot(DataNav(k).Time, DataNav(k).VesselHeight, Color(ik)); grid on; title('VesselHeight'); hold on
    subplot(2,1,2)
    PlotUtils.createSScPlot(DataNav(k).Time, DataNav(k).Heading, Color(ik)); grid on; title('Heading'); hold on; 
    
    tNav = DataNav(k).Time.timeMat;
    t(k) = tNav(1); %#ok<AGROW>
    drawnow
       
%     [flag, DataDepth, identSondeur] = read_depth_s7k(this(iFic));
%     if ~flag
%         continue
%     end
%     if isempty(DataDepth.PingCounter)
%         Longitude    = DataNav.Longitude;
%         Latitude     = DataNav.Latitude;
%         SonarTime    = DataNav.Time;
%         SonarSpeed   = DataNav.Speed;
%         Heading      = DataNav.Heading;
%         CourseVessel = DataNav.CourseVessel;
%     else
%         Longitude    = DataDepth.Longitude;
%         Latitude     = DataDepth.Latitude;
%         SonarTime    = DataDepth.Time;
%         
%         SonarSpeed   = interp1(DataNav.Time, DataNav.Speed, SonarTime);
%         Heading      = interp1(DataNav.Time, DataNav.Heading, SonarTime);
%         CourseVessel = interp1(DataNav.Time, DataNav.CourseVessel, SonarTime);
%     end
end
if isempty(t)
    my_close(fig1)
    my_close(fig2)
    return
end

%% Tri des navigations en fonction de l'heure

[~, ordre] = sort(t);

%% ConcatÚnation des navigations

tNvi         = [];
Latitude     = [];
Longitude    = [];
Heading      = [];
Speed        = [];
VesselHeight = [];
% CourseVessel = [];
for iFic=1:length(t)
    k = ordre(iFic);
    T = DataNav(k).Datetime;
    sub = find(abs(DataNav(k).Latitude) > eps);
    tNvi = [tNvi T(sub)]; %#ok<AGROW>
    Latitude        = [Latitude DataNav(k).Latitude(sub)]; %#ok<AGROW>
    Longitude       = [Longitude DataNav(k).Longitude(sub)]; %#ok<AGROW>
    Heading         = [Heading DataNav(k).Heading(sub)]; %#ok<AGROW>
    Speed           = [Speed DataNav(k).Speed(sub)]; %#ok<AGROW>
%     CourseVessel	= [CourseVessel DataNav(k).CourseVessel]; %#ok<AGROW>
    VesselHeight    = [VesselHeight DataNav(k).VesselHeight(sub)]; %#ok<AGROW>
end
% FigUtils.createSScFigure; PlotUtils.createSScPlot(Longitude, Latitude, '*'); grid on

%% Export Caraibes

Immersion = VesselHeight;
export_nav_Caraibes(nomFicNav, tNvi, Latitude, Longitude, Speed, Heading, ...
    VesselHeight, Immersion)

flag = 1;
