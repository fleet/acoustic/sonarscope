% Acces en lecture des proprietes d'une instance
%
% Syntax
%   [...] = get(this, ...)
% 
% Input Arguments 
%   this : Une instance de la classe cl_reson_s7k
%
% Name-Value Pair Arguments
%   nomFic : Nom du fichier .s7k
%
% Output Arguments
%   PropertyValues associees aux PropertyNames passes en argument
% 
% Examples
%   nomFic = getNomFicDatabase('RESON7125_20051116_221245.s7k')
%   a = cl_reson_s7k('nomFic', nomFic)
%   nomFic = get(a, 'nomFic')
%
% See also cl_reson_s7k cl_reson_s7k/set Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function varargout = get(this, varargin)

varargout = {};
for k=1:length(varargin)
    switch varargin{k}
        case 'nomFic'
            varargout{end+1} = this.nomFic; %#ok<AGROW>
            
        case {'SounderName'; 'EmModel'}
            varargout{end+1} = this.SounderName; %#ok<AGROW>
            
        case 'ExistGrpIndex'
            varargout{end+1} = this.ExistGrpIndex; %#ok<AGROW>
            
        case 'ExistGrpAttitude'
            varargout{end+1} = this.ExistGrpAttitude; %#ok<AGROW>
            
        case 'ExistGrpVerticalDepth'
            varargout{end+1} = this.ExistGrpVerticalDepth; %#ok<AGROW>
            
        case 'ExistGrpNavigation'
            varargout{end+1} = this.ExistGrpNavigation; %#ok<AGROW>
            
        case 'ExistGrpSoundSpeedProfile'
            varargout{end+1} = this.ExistGrpSoundSpeedProfile; %#ok<AGROW>
            
        case 'ExistGrpDepth'
            varargout{end+1} = this.ExistGrpDepth; %#ok<AGROW>
            
        case 'ExistGrpWaterColumnDataIFRData'
            varargout{end+1} = this.ExistGrpWaterColumnDataIFRData; %#ok<AGROW>
            
        case 'ExistGrpWCD'
            varargout{end+1} = this.ExistGrpWCD; %#ok<AGROW>
            
        case 'ExistGrpBeamRawData'
            varargout{end+1} = this.ExistGrpBeamRawData; %#ok<AGROW>
            
        case 'ExistGrpMagPhase'
            varargout{end+1} = this.ExistGrpMagPhase; %#ok<AGROW>

        otherwise
            w = sprintf('cl_reson_s7k/get %s non pris en compte', varargin{k});
            my_warndlg(w, 0);
    end
end
