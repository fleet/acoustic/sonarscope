function SounderName = getInfoSounder(~, nomFic)

a = cl_reson_s7k('nomFic', nomFic);
SounderName = get(a, 'SounderName');
