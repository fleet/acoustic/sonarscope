function [str, Unit, Commentaire] = getListeLayers(this) %#ok<INUSD>

% Layers provenant des datagrams "depth"
str{1}  = 'Depth / Antenna';
str{2}  = 'AcrossDist';
str{3}  = 'AlongDist';
str{4}  = 'BeamDepressionAngle';
str{5}  = 'BeamAzimuthAngle';
str{6}  = 'Range';
str{7}  = 'QualityFactor';
str{8}  = 'IfremerQF'; % 'Reflectivity';
str{9}  = 'DetectionBrightness';
str{10} = 'Detection';
str{11} = 'DetectionColinearity';
str{12} = 'Depth / SeaLevel';
str{13} = 'Mask';

% Layers provenant des datagrams "Raw Range and Beam Angle"
str{14} = 'BeamVerticalDirectionAngle';
str{15} = 'BeamHorizontalDirectionAngle';
str{16} = 's_3dBBeamWithY';
str{17} = 's_3dBBeamWithX';

% Layers construits
str{18} = 'LongueurTrajet';
str{19} = 'Temps universel';
str{20} = 'Roll';
str{21} = 'Pitch';
str{22} = 'Heave';
str{23} = 'Heading';
str{24} = 'Do not use';
str{25} = 'RayPathTravelTime';
str{26} = 'TxBeamIndex';
str{27} = 'DetectionNadirFilter';
str{28} = 'Do not use'; % 'ReflectivityFromSnippets 200%' : Does not exist any more
str{29} = 'MultiPingSequence';
str{30} = 'ReflectivityFromSnippets 100%';
str{31} = 'Do not use'; % 'ReflectivityFromSnippets 1 sample';
str{32} = 'UncertaintyPerCent'; 
str{33} = 'UncertaintyMeters';

% str{26} = 'MasqueDetection';
% str{27} = 'MasqueDepth';
% str{28} = 'ProbaDetection';
% str{29} = 'ProbaDepth';

Unit{1} = 'm';
Unit{2} = 'm';
Unit{3} = 'm';
Unit{4} = 'deg';
Unit{5} = 'deg';
Unit{6} = 'm';
Unit{7} = '';
Unit{8} = ' ';
Unit{9} = 'samples';
Unit{10} = '';
Unit{11} = '';
Unit{12} = 'm';
Unit{13} = '';

Unit{14} = 'deg';
Unit{15} = 'deg';
Unit{16} = 'deg';
Unit{17} = 'deg';

Unit{18} = 'm';
Unit{19} = 'time';
Unit{20} = 'deg';
Unit{21} = 'deg';
Unit{22} = 'deg';
Unit{23} = 'deg';
Unit{24} = '';
Unit{25} = 'ms';
Unit{26} = 'ms';
Unit{27} = '';
Unit{28} = 'dB';
Unit{29} = '';
Unit{30} = 'dB';
Unit{31} = 'dB';
Unit{32} = '%';
Unit{33} = 'm';

Commentaire{1}  = '"Depth" from Depth datagram';
Commentaire{2}  = '"Acrosstrack distance" from Depth datagram';
Commentaire{3}  = '"Alongtrack Distance" from Depth datagram';
Commentaire{4}  = '"Beam Depression Angle" from Depth datagram';
Commentaire{5}  = '"Beam Azimuth Angle" from Depth datagram';
Commentaire{6}  = '"Range (simple way travel meters according C=1500m/s)"';
Commentaire{7}  = '"Quality Factor" from Depth datagram';
Commentaire{8}  = '"Reflectivity" from Depth datagram';
Commentaire{9}  = '"Quality of Detection window" from Depth datagram';
Commentaire{10} = 'Detection mode (1=by amplitude, 2=by phase, 3=Both) : coming from "Quality Factor"';
Commentaire{11} = 'If amplitude detection : Number of samples for gravity calculation : coming from "Quality Factor"';
Commentaire{12} = '"Depth" from se level';
Commentaire{13} = 'Mask, bit 1:brightness, bit2=colinearity, bit3=epuration par histogramme';

% Layers provenant des datagrams "Raw Range and Beam Angle"
Commentaire{14} = 'Vertical Beam Angles / antennas (deg)';
Commentaire{15} = 'Horizontal Beam Angles / antennas (deg)';
Commentaire{16} = 'Longitudinal 3 dB Beam Width (deg)';
Commentaire{17} = 'Transversal 3 dB Beam Width (deg)';

% Layers construits
Commentaire{18} = 'LongueurTrajet : deduced from Range and Mode';
Commentaire{19} = 'Time of acoustic wave arrival to the sounder : deduced form Date, Time and Range';
Commentaire{20} = 'Roll at Time of impact between acoustic wave and seabed : deduced form Date, Time, Roll and Range';
Commentaire{21} = 'Pitch at Time of impact between acoustic wave and seabed : deduced form Date, Time, Pitch and Range';
Commentaire{22} = 'Heave at Time of impact between acoustic wave and seabed : deduced form Date, Time, Heave and Range';
Commentaire{23} = 'Heading at Time of impact between acoustic wave and seabed : deduced form Date, Time, Heading and Range';
Commentaire{24} = 'Deprecated';
Commentaire{25} = 'Aller simple en ms';
Commentaire{26} = 'Transmission beam number';
Commentaire{27} = 'Mask from Nadir Filter';
Commentaire{28} = 'Reflectivity from snippets 200%';
Commentaire{29} = '0 if monoping, 1,2,3,4 if multiping';
Commentaire{30} = 'Reflectivity from snippets 100%';
Commentaire{31} = 'Reflectivity from snippets 1 pixel';
Commentaire{32} = 'Uncertainty in % of dZ/z';
Commentaire{33} = 'Uncertainty in meters';

% Commentaire{26} = 'Mask from Detection';
% Commentaire{27} = 'Mask from Depth';
% Commentaire{28} = 'Proba from Detection';
% Commentaire{29} = 'Proba from Depth';
