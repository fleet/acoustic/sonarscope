function [SonarTime, Latitude, Longitude, Heading, CourseVessel, Immersion, provenance] = getNavFromVariousDatagrams(~, Data, DataDepth)

provenance = 'DataPosition';
if isempty(DataDepth.PingCounter) || all(isnan(DataDepth.Longitude))
    SonarTime = DataDepth.Time;
    
    Longitude    = my_interp1_longitude(Data.Datetime, Data.Longitude, DataDepth.Datetime);
    Latitude     = my_interp1(Data.Datetime, Data.Latitude,     DataDepth.Datetime);
    Heading      = my_interp1(Data.Datetime, Data.Heading,      DataDepth.Datetime);
    CourseVessel = my_interp1(Data.Datetime, Data.CourseVessel, DataDepth.Datetime);
    if isfield(Data, 'VesselHeight')
        Immersion = my_interp1(Data.Datetime, Data.VesselHeight, DataDepth.Datetime);
    else
        Immersion = zeros(size(Data.Latitude), 'single');
        Data.VesselHeight = Immersion;
    end
    
else
    Longitude = DataDepth.Longitude;
    Latitude  = DataDepth.Latitude;
    Heading   = DataDepth.Heading;
    SonarTime = DataDepth.Time;
    
%     SonarSpeed = interp1(Data.Time, Data.Speed, SonarTime);
%     Heading    = interp1(Data.Time, Data.Heading,      SonarTime);
    CourseVessel = my_interp1(Data.Datetime, Data.CourseVessel, DataDepth.Datetime);
    if isfield(Data, 'VesselHeight')
        Immersion = my_interp1(Data.Datetime, Data.VesselHeight, DataDepth.Datetime);
    else
        Immersion = zeros(size(Longitude), 'single');
        Data.VesselHeight = Immersion;
    end
    
    provenance = 'DataDepth';
end
