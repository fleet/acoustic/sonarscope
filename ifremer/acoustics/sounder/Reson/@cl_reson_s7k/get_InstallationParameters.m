function [flag, InstallationParameters, nomFicIP] = get_InstallationParameters(this, InstallationParameters, nomFicIP, varargin)

[varargin, identSondeur] = getPropertyValue(varargin, 'identSondeur', []); %#ok<ASGLU>

if isempty(identSondeur)
    identSondeur = selectionSondeur(this);
end
switch identSondeur
    case {13003; 13016}
        flag = 1;
        InstallationParameters = [];
        nomFicIP = [];
        return
end

flag = 0;
if isempty(InstallationParameters)
    if isempty(nomFicIP)
        FileName = get(this, 'nomFic');
        nomDirS7k = fileparts(FileName);
        switch identSondeur
            case 7150
                Data = read_sonarSettings(this, identSondeur);
                Freq = mode(Data.Frequency / 1000);
                if Freq > ((12+24)/2)
                    nomFicIP = 'InstallationParameters_24kHz.xml';
                else
                    nomFicIP = 'InstallationParameters_12kHz.xml';
                end
            otherwise
                nomFicIP = sprintf('InstallationParameters_%d.xml', identSondeur);
        end
        nomFicIP = fullfile(nomDirS7k, nomFicIP);
        if exist(nomFicIP, 'file')
            InstallationParameters = xml_read(nomFicIP);
            if isempty(InstallationParameters)
                return
            end
        end
    else
        if iscell(nomFicIP)
            nomFicIP = nomFicIP{1};
        end
        if ~exist(nomFicIP, 'file')
            return
        end
        InstallationParameters = xml_read(nomFicIP);
        if isempty(InstallationParameters)
            return
        end
        %     InstallationParameters.TransmitArrayY = 77 % TODO : temporaire
    end
end

flag = 1;
