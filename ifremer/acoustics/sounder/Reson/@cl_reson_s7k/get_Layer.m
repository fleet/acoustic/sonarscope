function Layer = get_Layer(this, nomLayer, varargin)

[varargin, CartoAuto] = getPropertyValue(varargin, 'CartoAuto', 0); %#ok<ASGLU>

switch nomLayer
    case 'Bathymetry'
        ListeLayers = 12;
    case 'ReflectivityFromSnippets'
        ListeLayers = 30;
    otherwise
        Layer = [];
        return
end

[Layer, Carto] = view_depth(this, 'ListeLayers', ListeLayers); %#ok<ASGLU> %, 'Bits', 'AskIt');
