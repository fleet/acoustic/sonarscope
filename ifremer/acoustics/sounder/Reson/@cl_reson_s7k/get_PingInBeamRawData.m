function flag = get_PingInBeamRawData(this, k, N, identSondeur, varargin)

[varargin, ncID]  = getPropertyValue(varargin, 'ncID',  []);
[varargin, grpID] = getPropertyValue(varargin, 'grpID', []);
[varargin, varID] = getPropertyValue(varargin, 'varID', []);
[varargin, Close] = getPropertyValue(varargin, 'Close', 1); %#ok<ASGLU>

if k > N
    my_breakpoint
    flag = 0;
    return
end

if isempty(ncID)
    [nomFicBeamRawData, ~] = ResonBeamRawDataNames(this.nomFic, identSondeur, []);
    ncID = netcdf.open(nomFicBeamRawData.Mat);
    flagOpenHere = 1;
else
    flagOpenHere = 0;
end
if isempty(grpID)
    grpID = netcdf.inqNcid(ncID, 'BeamRawData');
end
if isempty(varID)
    varID = netcdf.inqVarID(grpID, 'PingInBeamRawData');
end
PingInBeamRawData = netcdf.getVar(grpID, varID);

if flagOpenHere || Close
    netcdf.close(ncID);
end

flag = PingInBeamRawData(k);
