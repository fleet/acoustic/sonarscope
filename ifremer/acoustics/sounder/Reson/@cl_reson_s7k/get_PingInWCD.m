function flag = get_PingInWCD(this, k, N, identSondeur)

if k > N
    my_breakpoint
    flag = 0;
    return
end

nomFicWaterColumnData = ResonWaterColumnDataNames(this.nomFic, identSondeur, []);

ncID = netcdf.open(nomFicWaterColumnData.Mat);
grpID = netcdf.inqNcid(ncID, 'WaterColumnDataIFRData');
varID = netcdf.inqVarID(grpID, 'PingInWCD');
PingInWCD = netcdf.getVar(grpID, varID);
netcdf.close(ncID);

flag = PingInWCD(k);
