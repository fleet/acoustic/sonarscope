function SerialNumber = get_SonarSerialNumber(this, identSondeur)

Data = read_7kBeamGeometry(this, identSondeur);
if isempty(Data)
    SerialNumber = [];
else
    % SerialNumber = Data.BeamGeometry(1).RTH.SonarId;
%     SerialNumber = Data.DRF.DeviceIdentifier;
    SerialNumber = Data.DeviceIdentifier(1);
end