function [flag, c, indLayerAsCurrentOne, Selection, Carto] = get_layers(this, nomsLayers, DataTypeCurrentImage, DataTypes, bilan, ModeDependant, Carto, ...
    Selection, UseModel, varargin)

[varargin, flagTideCorrection]       = getPropertyValue(varargin, 'TideCorrection',           0);
[varargin, PreserveMeanValueIfModel] = getPropertyValue(varargin, 'PreserveMeanValueIfModel', 0);
[varargin, MasqueActif]              = getPropertyValue(varargin, 'MasqueActif',              1); %#ok<ASGLU>

c = [];
indLayerAsCurrentOne = [];

listeLayersSupplementaires = [];
DataTypeConditions         = [];
for k2=1:length(bilan)
    DTC = bilan{k2}{1}(1).DataTypeConditions;
    [listeCor, dataTypeCor, flag] = getListeLayersSupplementaires(DTC); %#ok<ASGLU>
    if ~flag
        return
    end
    listeLayersSupplementaires = [listeLayersSupplementaires listeCor];
    DataTypeConditions = [DataTypeConditions DTC];
end
[listeLayersSupplementaires, ia] = unique(listeLayersSupplementaires);
DataTypeConditions = DataTypeConditions(ia);

ListeLayers = [];
for k2=1:length(DataTypes)
    X = Reson_getDefaultLayers(this, 'nomLayer', nomsLayers{k2});
    ListeLayers = [ListeLayers X]; %#ok<*AGROW>
end
ListeLayers = unique([ListeLayers listeLayersSupplementaires]);
ListeLayers = unique(ListeLayers);

% [c, Carto] = import_PingBeam_All_V1(this, 'ListeLayers', ListeLayers, 'Carto', Carto, ...
%     'TideCorrection', flagTideCorrection, 'MasqueActif', MasqueActif);

    TideFile = [];
    
Bits = 'AskIt';
[c, Carto, Bits] = view_depth(this, 'ListeLayers', ListeLayers, 'Carto', Carto, ...
    'TideFile', TideFile, 'Bits', Bits, 'setMaskToAllLayers', 1);
if isempty(c)
    flag = 0;
    return
end

indLayerAsCurrentOne = findIndLayerSonar(c, 1, 'DataType', DataTypeCurrentImage, 'OnlyOneLayer', 'WithCurrentImage', 1);
if isempty(indLayerAsCurrentOne) % Test rajout� par JMA le 07/12/2019
    flag = 0;
    return
end

for k1=1:length(Selection)
    DT = Selection(k1).DataType;
    if DT == cl_image.indDataType('TxAngle')
        DT(2) = cl_image.indDataType('BeamPointingAngle');
    end
    Selection(k1).indLayer = findIndLayerSonar(c, 1, 'DataType', DT);
end

%% Compensation statistique (Ex : courbes de calibration)

for k2=1:length(bilan)
    identLayersConditionCompensation = [];
    for k3=1:length(listeLayersSupplementaires)
        switch DataTypeConditions{k3}
            case {'TxAngle'; 'BeamPointingAngle'} % Pour �viter confusion avec cette version de SSc
                strDT = {'TxAngle'; 'BeamPointingAngle'};
            otherwise
                strDT = DataTypeConditions(k3);
        end
        identDataType = [];
        for k4=1:length(strDT)
            identDataType(k4) = cl_image.indDataType(strDT(k4));
        end
        x = findIndLayerSonar(c, 1, 'DataType', identDataType, 'WithCurrentImage', 1);
        if isempty(x)
            str1 = sprintf('Le layer de type "%s" n''a pas �t� trouv�.', DataTypeConditions{k3});
            str2 = sprintf('Layer of type "%s" was not found.', DataTypeConditions{k3});
            my_warndlg(Lang(str1,str2), 1);
            flag = 0;
            return
        else
            identLayersConditionCompensation(k3) = x;
        end
    end
    if length(UseModel) == 1
        UseModelHere = UseModel;
        PreserveMeanValueIfModelHere = PreserveMeanValueIfModel;
    else
        UseModelHere = UseModel(k2);
        PreserveMeanValueIfModelHere = PreserveMeanValueIfModel(k2);
    end
    [d, flag] = compensationCourbesStats(c(indLayerAsCurrentOne), c(identLayersConditionCompensation), ...
        bilan{k2}, 'ModeDependant', ModeDependant, 'UseModel', UseModelHere, ...
        'PreserveMeanValueIfModel', PreserveMeanValueIfModelHere);
    if ~flag
        return
    end
    c(indLayerAsCurrentOne) = d;
end

flag = 1;
