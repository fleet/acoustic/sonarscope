% Histogramme des type de datagrams contenus dans un fichier .all
%
% Syntax
%   histo_index(aKM)
%
% Input Arguments
%   aKM : Instance de cl_simrad_all
%
% Name-Value Pair Arguments
%   fullListe : 1=affichage que des datagrams presents, 0=tous les datagrams
%
% Name-only Arguments
%
% Output Arguments
%   []                : Auto-plot activation
%   texteTypeDatagram : Resume textuel de l'histogramme
%   Code              : Abscisses
%   histoTypeDatagram : Nb de datagrammes (ordonnees)
%
% Examples
%   nomFic = getNomFicDatabase('RESON7125_20051116_221245.s7k')
%   aKM = cl_simrad_all('nomFic', nomFic);
%   identSondeur = selectionSondeur(aKM);
%   histo_index(aKM, identSondeur)
%   [texteTypeDatagram] = histo_index(aKM, identSondeur)
%   [texteTypeDatagram, Code, histoTypeDatagram] = histo_index(aKM, identSondeur)
%   figure; bar(Code, histoTypeDatagram)
%
% See also cl_simrad_all plot_index Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [texteTypeDatagram, Code, histoTypeDatagram, NbBytes] = histo_index(this, identSondeur, varargin)
    
[varargin, fullListe] = getPropertyValue(varargin, 'fullListe', 0); %#ok<ASGLU>

texteTypeDatagram = [];
Code              = [];
histoTypeDatagram = [];
NbBytes           = [];

%% Lecture du fichier d'index

[flag, DataIndex] = readFicIndex(this, identSondeur);
if ~flag
    return
end

Date             = DataIndex(1,:);
Heure            = DataIndex(2,:);
Position         = DataIndex(3,:); %#ok
Taille           = DataIndex(4,:);
TypeDatagram     = DataIndex(5,:);
PingCounter      = DataIndex(6,:); %#ok
PingSequence     = DataIndex(7,:); %#ok
DeviceIdentifier = DataIndex(8,:); %#ok

Code = min(TypeDatagram):max(TypeDatagram);
histoTypeDatagram = my_hist(TypeDatagram, Code); 
texteTypeDatagram = codesDatagramsS7k(Code, histoTypeDatagram, 'fullListe', fullListe);
NbBytes           = zeros(size(texteTypeDatagram));

subDatagramsNonNuls = find(histoTypeDatagram ~= 0);
histoCodesPresents  = histoTypeDatagram(subDatagramsNonNuls);
for k=1:length(subDatagramsNonNuls)
    sub = (TypeDatagram == Code(subDatagramsNonNuls(k)));
    NbBytes(subDatagramsNonNuls(k)) = sum(Taille(sub));
end

tDeb = cl_time('timeIfr', Date(1), Heure(1));
tFin = cl_time('timeIfr', Date(end), Heure(end));
strTime = ['Begin : ' t2str(tDeb) '    End : ' t2str(tFin)];

if nargout == 0
    plot_histo_index(Code(subDatagramsNonNuls), histoCodesPresents, texteTypeDatagram, this.nomFic, 'Number of Datagrams', strTime)
    %     if get_LevelQuestion >= 3
    NbBytes = NbBytes(subDatagramsNonNuls); % Modif JMA le 08/01/2020
    plot_histo_index(Code(subDatagramsNonNuls), NbBytes, texteTypeDatagram, this.nomFic, 'Number of Bytes', strTime)
    %     end
end

function plot_histo_index(Code, histoTypeDatagram, texteTypeDatagram, TitlePart1, TitlePart2, varargin)

figure('Position', [100 400 1000 500]);
hp = uipanel('Title', 'Histogram', 'FontSize', 12, ...
    'BackgroundColor', 'white',...
    'Position', [.02 .02 .5 .95]);
axes('parent', hp);
bar(histoTypeDatagram);
for k=1:length(histoTypeDatagram)
    XTickLabel{k} = num2str(Code(k)); %#ok
end
set(gca, 'XTickLabel', XTickLabel, 'XTickLabelRotation', 90)

hp = uipanel('Title', 'Legend', 'FontSize',12,...
    'BackgroundColor', 'white', ...
    'Position', [.52 .02 .45 .95]);

uicontrol(hp, 'Style','text', 'Units', 'normalized',...
    'Position', [.1 .1 .8 .8],...
    'String', texteTypeDatagram, 'HorizontalAlignment', 'left');
text(70, max(histoTypeDatagram), texteTypeDatagram, 'VerticalAlignment', 'top');

if exist(TitlePart1, 'file')
    [~, nomFic] = fileparts(TitlePart1);
    Titre = sprintf('%s\n%s', nomFic, TitlePart2);
    %     Titre = [nomFic ' - ' TitlePart2];
else
    %     Titre = [TitlePart1 ' - ' TitlePart2];
    Titre = sprintf('%s\n%s', TitlePart1, TitlePart2);
    
end
if ~isempty(varargin)
    %     Titre = sprintf('%s\n%s', Titre,varargin{1});
end
title(Titre, 'Interpreter', 'none')

