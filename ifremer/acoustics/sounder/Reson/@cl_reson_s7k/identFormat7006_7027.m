function [flagFormat7006, flagFormat7027] = identFormat7006_7027(this, identSondeur, varargin)

[varargin, DataIndex] = getPropertyValue(varargin, 'DataIndex', []); %#ok<ASGLU>

flagFormat7006 = 0;
flagFormat7027 = 0;

%% Lecture du fichier d'index

if isempty(DataIndex)
    [flag, DataIndex] = readFicIndex(this, identSondeur);
    if ~flag
        return
    end
end

TypeDatagram     = DataIndex(5,:);
PingCounter      = DataIndex(6,:);
PingSequence     = DataIndex(7,:);
DeviceIdentifier = DataIndex(8,:);

% Test si 7006 ou 7027 : A continuer : TODO

if any(TypeDatagram == 7006)
    [flag, subDataDepth] = get_subDataCommon(7006, TypeDatagram, PingCounter, PingSequence, DeviceIdentifier, identSondeur);
    if flag && ~isempty(subDataDepth)
        flagFormat7006 = 1;
        return
    end
end

if any(TypeDatagram == 7027)
    [flag, subDataDepth] = get_subDataCommon(7027, TypeDatagram, PingCounter, PingSequence, DeviceIdentifier, identSondeur);
    if flag && ~isempty(subDataDepth)
        flagFormat7027 = 1;
        return
    end
end
