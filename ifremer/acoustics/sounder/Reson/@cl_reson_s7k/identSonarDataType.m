function [DataType, ColormapIndex] = identSonarDataType(~, nomLayer)

ColormapIndex = 3; % jet par defaut

switch nomLayer
    case {'Depth'; 'Depth-Heave'; 'DepthMask'; 'Depth / SeaLevel'; 'Depth / Antenna'}
        DataType = cl_image.indDataType('Bathymetry');
    case 'AcrossDist'
        DataType = cl_image.indDataType('AcrossDist');
    case 'AlongDist'
        DataType = cl_image.indDataType('AlongDist');
    case 'BeamDepressionAngle'
        DataType = cl_image.indDataType('TxAngle');
    case 'Raw-BeamPointingAngle'
        DataType = cl_image.indDataType('RxBeamAngle');
    case 'BeamHorizontalDirectionAngle'
        DataType = cl_image.indDataType('TxAngle');
    case {'BeamAzimuthAngle'; 'BeamVerticalDirectionAngle'}
        DataType = cl_image.indDataType('BeamAzimuthAngle');
    case {'Range'; 'Raw-Range'}
%         DataType = cl_image.indDataType('RayPathSampleNb');
%         DataType = cl_image.indDataType('RayPathLength'); % One way length in meters
        DataType = cl_image.indDataType('TwoWayTravelTimeInSeconds'); % One way length in meters
    case 'LongueurTrajet'
        DataType = cl_image.indDataType('RayPathLength'); % One way length in meters
    case 'QualityFactor'
        DataType = cl_image.indDataType('KongsbergQualityFactor');
    case 'DetectionBrightness'
        DataType = cl_image.indDataType('ResonDetectionBrightness');
    case 'DetectionColinearity'
        DataType = cl_image.indDataType('ResonDetectionColinearity');
    case 'DetectionNadirFilter'
        DataType = cl_image.indDataType('ResonDetectionNadirFilter');
    case 'LengthDetection'
        DataType = 1;
    case 'IfremerQF'
        DataType = cl_image.indDataType('IfremerQF');
        ColormapIndex = 3;
    case 'UncertaintyPerCent'
        DataType = cl_image.indDataType('UncertaintyPerCent');
        ColormapIndex = 3;
    case 'UncertaintyMeters'
        DataType = cl_image.indDataType('UncertaintyMeters');
        ColormapIndex = 3;
    case {'Reflectivity'; 'Raw-Reflectivity'; 'ReflectivityFromSnippets 100%'; 'ReflectivityFromSnippets 200%'; 'ReflectivityFromSnippets 1 sample'}
        DataType = cl_image.indDataType('Reflectivity');
        ColormapIndex = 2;
    case 'ReflectivityFromSnippets'
        DataType = cl_image.indDataType('Reflectivity');
        ColormapIndex = 2;
    case 'Detection'
        DataType = cl_image.indDataType('DetectionType');
    case {'DetecAmplitudeNbSamples'; 'DetecPhaseOrdrePoly'; 'DetecPhaseVariance'}
        DataType = 1;
    case 'Temps universel'
        DataType = cl_image.indDataType('RxTime');
    case 'Raw-TransmitTiltAngle'
%         message = sprintf('Le layer %s ne semble pas etre un layer de Simrad. Il est donc assimile comme "Other"', nomLayer);
%         msgbox(message)
        DataType = 1;
    case 'RayPathTravelTime'
        DataType = cl_image.indDataType('RayPathTravelTime');
    case 'Mask'
        DataType = cl_image.indDataType('Mask');
    case 'Probability'
        DataType = cl_image.indDataType('Proba');
    case 'TxBeamIndex'
        DataType = cl_image.indDataType('TxBeamIndex');
    case 'MbesQualityFactor'
        DataType = cl_image.indDataType('MbesQualityFactor');
        
    case 'Roll'
        DataType = cl_image.indDataType('Roll');
    case 'Pitch'
        DataType = cl_image.indDataType('Pitch');
    case 'Heave'
        DataType = cl_image.indDataType('Heave');
    case 'Heading'
        DataType = cl_image.indDataType('Heading');
    case 'MultiPingSequence'
        DataType = cl_image.indDataType('Mask');
        
    otherwise
%         message = sprintf('Le layer %s ne semble pas etre un layer de Reson. Il est donc assimile comme "Other"', nomLayer);
%         my_warndlg(message, 0);
        DataType = 1;
end
