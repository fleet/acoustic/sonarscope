% Trace de la navigation et des capteurs de vitesse et de cap d'un fichier .all
%
% Syntax
%   import_CaraibesNvi(a)
%
% Input Arguments
%   a : Instance de cl_reson_s7k
%
% Name-Value Pair Arguments
%   fullListe : 1=affichage que des datagrams presents, 0=tous les datagrams
%
% Output Arguments
%
% Examples
%   nomFic = getNomFicDatabase('RESON7125_20051116_221245.s7k')
%   a = cl_reson_s7k('nomFic', nomFic);
%   import_CaraibesNvi(a)
%
%   nomDir = '/home1/jackson/7K'
%   liste = listeFicOnDir2(nomDir, '.s7k')
%   for i=1:3%length(liste)
%       a(i) = cl_reson_s7k('nomFic', liste{i});
%   end
%   import_CaraibesNvi(a)
%
% See also cl_reson_s7k plot_index Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function import_CaraibesNvi(this, nomFicNav, identSondeur, varargin)

global useCacheNetcdf %#ok<GVMIS>

N1 = length(nomFicNav);
str1 = 'Import de la navigation';
str2 = 'Navigation import';
hw1 = create_waitbar(Lang(str1,str2), 'N', N1);
for iNav=1:N1
    my_waitbar(iNav, N1, hw1);
    
    %% Lecture des fichiers de navigation Caraibes
    
    [flag, LatitudeNav, LongitudeNav, tNav, HeadingSensor, SpeedNav] = lecFicNav(nomFicNav{iNav});
    if ~flag
%         messageErreurFichier(nomFicNav{iNav}, 'ReadFailure');
        continue
    end
    tNav_timeMat = tNav.timeMat;
    
    if length(unique(HeadingSensor)) == 1 % Cas des fichiers .nvi mission Victor ODEMAR24
        % TODO Message
        HeadingSensor = [];
    end
    
    NbFic = length(this);
    str1 = sprintf('Import de la navigation %s dans les *.s7k', nomFicNav{iNav});
    str2 = sprintf('Navigation %s import into .s7k', nomFicNav{iNav});
    hw2 = create_waitbar(Lang(str1,str2), 'N', NbFic);
    for iFic=1:NbFic
        my_waitbar(iFic, NbFic, hw2);
        
        %% Lecture de la navigation du fichier .s7k
        
        Data = read_navigation(this(iFic), identSondeur);
        
        if isempty(Data.Latitude)
            [flag, DataDepth, identSondeur] = read_depth_s7k(this(iFic));
            if ~flag
                continue
            end
    
            t = DataDepth.Time.timeMat;
            nbPings = numel(t);
            if nbPings <= 1
                continue
            end
            sub = 1:nbPings;
            Data.Time = DataDepth.Time;
            Data.Datetime = datetime(Data.Time.timeMat, 'ConvertFrom', 'datenum');
            
            % Pour debug
            Data.Longitude                  = [];
            Data.Speed                      = [];
            Data.CourseVessel               = [];
            Data.Heading                    = [];
            Data.VesselHeight               = [];
            Data.HeightAccuracy             = [];
            Data.HorizontalPositionAccuracy = [];

        else
            
            %% Calcul de l'intersection commune entre navigation Caraibes et
            % navigation du fichier .s7k
            
            t = Data.Time.timeMat;
            sub = find((t >= tNav_timeMat(1)) & (t <= tNav_timeMat(end)));
            if isempty(sub)
                % continue
                % on recherche tout de m�me les intervalles correspondant, dans
                % le cas o� l'�chantillonnage ne corresponderait pas entre les
                % deux vecteurs de temps
                t = Data.Time.timeMat;
                if t(1) <= tNav_timeMat(1) % d�but profil < d�but nav
                    if t(end) >= tNav_timeMat(1)  % Fin profil > d�but nav
                        [~, subDeb] = unique(t >= tNav_timeMat(1));
                        if t(end) <= tNav_timeMat(end) % Fin profil < Fin nav
                            sub = subDeb:numel(Data.Latitude);
                        else % Fin profil > Fin nav
                            [~, subFin] = unique(t <= tNav_timeMat(end));
                            sub = subDeb:subFin;
                        end
                    else % Fin profil < d�but nav
                        continue
                    end
                elseif t(1) >= tNav_timeMat(1) && t(1) < tNav_timeMat(end) % d�but nav < d�but profil < fin nav
                    if t(end) <= tNav_timeMat(end) % Fin profil < Fin nav
                        sub = 1:numel(Data.Latitude);
                    else % Fin profil > Fin nav
                        [~, subFin] = unique(t <= tNav_timeMat(end));
                        sub = 1:subFin;
                    end
                else
                    continue
                end
            end
        end
        
        %% Interpolation de la navigation provenant du fichier Caraibes aux
        % instants du fichier .s7k
        
%         t = Data.Time.timeMat;
        Data.Longitude(sub) = my_interp1_longitude(tNav_timeMat, LongitudeNav, t(sub));
        Data.Latitude(sub)  = my_interp1(tNav_timeMat, LatitudeNav,  t(sub));
        if ~isempty(HeadingSensor)
            Data.Heading(sub) = my_interp1(tNav_timeMat, HeadingSensor, t(sub));
            Time = cl_time('timeMat', t);
            Data.CourseVessel(sub) = calCapFromLatLon(Data.Latitude(sub), Data.Longitude(sub), 'Time', Time(sub));
        end
        if ~isfield(Data, 'Speed') || isempty(Data.Speed) || all(isnan(Data.Speed))
            Data.Speed = NaN(size(Data.Longitude), 'single');
            Data.Speed(sub) = my_interp1_longitude(tNav_timeMat, SpeedNav, t(sub));
        end
%         if ~isempty(ImmersionNav)
%             Data.VesselHeight(sub) = my_interp1(tNav_timeMat, -ImmersionNav, t(sub));
%         end

        %% Sauvegarde du fichier de navigation

        [nomDir, nomFic] = fileparts(this(iFic).nomFic);
        nomFicMat  = fullfile(nomDir, 'SonarScope', [nomFic '_navigation.mat']);
        ncFileName = fullfile(nomDir, 'SonarScope', [nomFic '.nc']);
        try
            if useCacheNetcdf == 1 % 1=Mat, 2 et 3=Netcdf
                save(nomFicMat, 'Data')
            else
                Data.Dimensions.nbPoints = length(Data.Latitude);
                NetcdfUtils.Struct2Netcdf(Data, ncFileName, 'Navigation');
            end
        catch %#ok<CTCH>
            messageErreurFichier(nomFicMat, 'WriteFailure');
        end
        
        %% ----------------------------------------
        % MODIF GLT il faut aussi modifier depth.mat
        
        [flag, Data, identSondeur] = read_depth_s7k(this(iFic));
        if ~flag
            continue
        end
        
        t = Data.Time.timeMat;
        sub = find((t >= tNav_timeMat(1)) & (t <= tNav_timeMat(end)));
        if isempty(sub)
            % continue
            % on recherche tout de m�me les intervalles correspondant, dans
            % le cas o� l'�chantillonnage ne corresponderait pas entre les
            % deux vecteurs de temps
            t = Data.Time.timeMat;
            if t(1) <= tNav_timeMat(1) % d�but profil < d�but nav
                if t(end) >= tNav_timeMat(1)  % Fin profil > d�but nav
                    [~, subDeb] = unique(t >= tNav_timeMat(1));
                    if t(end) <= tNav_timeMat(end) % Fin profil < Fin nav
                        sub = subDeb:numel(Data.Latitude);
                    else % Fin profil > Fin nav
                        [~, subFin] = unique(t <= tNav_timeMat(end));
                        sub = subDeb:subFin;
                    end
                else % Fin profil < d�but nav
                    continue
                end
            elseif t(1) >= tNav_timeMat(1) && t(1) < tNav_timeMat(end) % d�but nav < d�but profil < fin nav
                if t(end) <= tNav_timeMat(end) % Fin profil < Fin nav
                    sub = 1:numel(Data.Latitude);
                else % Fin profil > Fin nav
                    [~, subFin] = unique(t <= tNav_timeMat(end));
                    sub = 1:subFin;
                end
            else
                continue
            end
        end
        
%         [sub, sub2] = intersect(Data.Time, tNav); %#ok<NASGU> %glt
%         if isempty(sub)
%             continue
%         end                               
        
        Data.Longitude = my_interp1_longitude(tNav_timeMat, LongitudeNav, t);
        Data.Latitude  = my_interp1(tNav_timeMat, LatitudeNav,  t);
        if ~isempty(HeadingSensor)
            Data.Heading(sub) = my_interp1(tNav_timeMat, HeadingSensor, t(sub));
        end
%         if ~isempty(ImmersionNav) % commenter car pas la m�me taille de matrice !!!!
%             Data.VehicleDepth = my_interp1(tNav_timeMat, -ImmersionNav,  t);
%         end
           
%         PlotUtils.createSScPlot(Data.Longitude, Data.Latitude, '-r');
%         hold on
%         PlotUtils.createSScPlot(DataDepth.Longitude, DataDepth.Latitude, '-k');
%         PlotUtils.createSScPlot(LongitudeNav(sub2), LatitudeNav(sub2), '--g');
       
        [nomDir, nomFic] = fileparts(this(iFic).nomFic);
        nomFicDepthMat = fullfile(nomDir, 'SonarScope', [num2str(identSondeur) '_' nomFic '_depth.mat']);
        try
%             save(nomFicDepthMat, 'Data')
            if useCacheNetcdf == 1 % 1=Mat, 2 et 3=Netcdf
                save(nomFicDepthMat, 'Data')
            else
%                 Data.Dimensions.nbPoints = length(Data.Latitude);
%                 NetcdfUtils.Struct2Netcdf(Data, ncFileName, 'Depth');

                if exist(ncFileName, 'file')
                    ncID = netcdf.open(ncFileName, 'WRITE');
                    grdID = netcdf.inqNcid(ncID, 'Depth');
                    
                    varID = netcdf.inqVarID(grdID, 'Latitude');
%                     LatitudePrevious = netcdf.getVar(grdID, varID);
                    netcdf.putVar(grdID, varID, Data.Latitude);
                    
                    varID = netcdf.inqVarID(grdID, 'Longitude');
%                     LongitudePrevious = netcdf.getVar(grdID, varID);
                    netcdf.putVar(grdID, varID, Data.Longitude);
                    
                    varID = netcdf.inqVarID(grdID, 'Heading');
%                     HeadingPrevious = netcdf.getVar(grdID, varID);
                    netcdf.putVar(grdID, varID, Data.Heading);
                    
                    netcdf.close(ncID);
                end
            end

        catch %#ok<CTCH>
            messageErreurFichier(nomFicDepthMat, 'WriteFailure');
        end
    end
    my_close(hw2);
end
my_close(hw1, 'MsgEnd');
