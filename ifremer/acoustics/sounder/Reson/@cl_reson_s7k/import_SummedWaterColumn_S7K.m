% nomFic = 'G:\Carla\GEOFLAMME\20210409_140953_PP_7150_24kHz.s7k'
% [flag, nomFic] = FtpUtils.getSScDemoFile('0029_20180306_213505_SimonStevin.all', 'OutputDir', 'D:\Temp\ExData');
% if flag
%     s7k = cl_reson_s7k('nomFic', nomFic, 'KeepNcID', true);
%     [flag, A] = import_SummedWaterColumn_S7K(s7k);
%     if flag
%         SonarScope(A)
%     end
% end

function [flag, a] = import_SummedWaterColumn_S7K(this)

% TODO : cette fonction est inspir�e de import_SummedWaterColumn_ALL mais n'est pas compl�tement finalis�e

a = [];

% [~, Carto] = prepare_lectures7k_step1(this.nomFic);

identSondeur = selectionSondeur(this);

[flag, DataIndex] = readFicIndex(this, identSondeur);
if ~flag
    return
end

InstallationParameters = read_sonarInstallationParameters(this, identSondeur, ...
    'DataIndex', DataIndex, 'Mute', 1);

Data7000 = read_sonarSettings(this, identSondeur);
if ~flag
    return
end

Data7004 = read_7kBeamGeometry(this, identSondeur);
if ~flag
    return
end
    
[flag, DataDepth] = read_depth_s7k(this, 'DataIndex', DataIndex, 'identSondeur', identSondeur);
if ~flag
    return
end

nbPings = length(DataDepth.PingCounter);
if nbPings < 2
    str1 = sprintf('Le fichier "%s" ne contient pas suffisamment de pings.', this.nomFic);
    str2 = sprintf('File "%s" does not contain enough depth datagram.', this.nomFic);
    my_warndlg(Lang(str1,str2), 0);
    return
end

%% Lecture des donn�es de la colonne d'eau

sub2 = 1:length(DataDepth.PingCounter);
BeamData = [];
OrigineWC = 1;
for k=1:length(sub2)
    if OrigineWC == 1 % Water Column
        [flag, DataWC, BeamData] = read_WaterColumnData(this, identSondeur, 'DataDepth', DataDepth, ...
            'DataIndex', DataIndex, 'subPing', sub2(k), 'Display', 0);
        if ~flag
            break
        end
    else % Mag & Phase
        [DataWC, BeamData] = read_BeamRawData(this, identSondeur, 'DataDepth', DataDepth, ...
            'DataIndex', DataIndex, 'subPing', sub2(k), 'DisplayLevel', 0);
    end
    if ~isempty(BeamData)
        break
    end
end
if isempty(BeamData)
    flag = 0;
    return
end

%%

R0Max = 1;
R0 = NaN(nbPings,1);
ImageWCPingSample = NaN(nbPings, 1, 'single');
F =  NaN(nbPings, 1, 'single');

% Fig = figure;
Carto = [];
hw = create_waitbar('Processing', 'N', nbPings);
for kPing=1:nbPings
    my_waitbar(kPing, nbPings, hw);

    [A, Carto] = view_WaterColumn(this, 'identSondeur', identSondeur, 'DataIndex', DataIndex, ...
        'InstallationParameters', InstallationParameters, 'DataDepth', DataDepth, 'Data7000', Data7000, ...
        'Data7004', Data7004, 'DataWC', DataWC, 'subPing', kPing, 'Carto', Carto, 'Display', 0);
    if isempty(A)
        continue
    end
    %     'subPing', subPing, 'Carto', Carto', 'NavFileName', nomFicNav, 'nomFicIP', nomFicIP, 'Display', 0);
%     if ~isempty(A)
%         imagesc(A, 'Fig', Fig);
%     end
    
%     ImageWCSampleBeam = reflec_dB2Amp(get(A, 'Image'));
    ImageWCSampleBeam = get(A, 'Image');
    
    SampleBeamData = get_SonarSampleBeamData(A);
    R = floor(SampleBeamData.R1SamplesFiltre);
%     figure(67788787); subplot(1,2,1); imagesc(ImageWCSampleBeam); colormap(jet(256)); colorbar; drawnow;
    
    R(R <= 1) = [];
    R0(kPing) = min(R);
    
    ImageWCSampleBeam((R0(kPing)+1):end,:) = NaN;
    R0Max = max(R0Max, R0(kPing));
    
    X = mean(ImageWCSampleBeam, 2, 'omitnan');
        
    %% Crop de l'image
    
    n = size(ImageWCPingSample,2);
    ImageWCPingSample(kPing,1:length(X)) = X';
    ImageWCPingSample(:,(n+1):end) = NaN;
    
    F(kPing) = SampleBeamData.SampleRate;
end
my_close(hw);

% figure; imagesc(ImageWCPingSample); colormap(jet(256)); colorbar;

Reflectivity = view_depth(this, 'ListeLayers', 30, 'identSondeur',identSondeur, 'DataIndex', DataIndex);

TagSynchroX = Reflectivity.TagSynchroX;
TagSynchroX = strrep(TagSynchroX, 'PingBeam', 'PingSamples');
ImageName = strrep(Reflectivity.Name, 'Snippets100', 'WCAveragedOnBeams');
Comments = 'This image is the mean value of the WC data in the beams direction. This image must be used just for quick control of the data.';

%% Cr�ation de l'instance de l'image de sortie par h�ritage de l'instance de r�flectivit�

FMode = mode(F);

ImageWCPingSampleNew = ImageWCPingSample;
subSup = find(F > FMode);
for k=1:length(subSup)
    coef = F(subSup(k)) / FMode;
    v1 = ImageWCPingSample(subSup(k),:);
    v2 = resample(double(v1), 1000, round(1000*double(coef)));
    ImageWCPingSampleNew(subSup(k),:) = NaN;
    ImageWCPingSampleNew(subSup(k),1:length(v2)) = v2;
    R0(subSup(k)) = round(R0(subSup(k))) / coef;
end
F(subSup) = FMode;
ImageWCPingSample = ImageWCPingSampleNew;
nbPixNonNaN = sum(~isnan(ImageWCPingSample));
n = find(nbPixNonNaN == 0, 1, 'first');
ImageWCPingSample(:,n:end) = [];
ImageWCPingSample(ImageWCPingSample == 0) = NaN;


ImageWCPingSampleNew = ImageWCPingSample;
subInf = find(F < FMode);
for k=1:length(subInf)
    coef = F(subInf(k)) / FMode;
    v1 = ImageWCPingSample(subInf(k),:);
    v2 = resample(double(v1), 1000, round(1000*double(coef)));
    ImageWCPingSampleNew(subInf(k),:) = NaN;
    subNonNaN = find(~isnan(v2));
    if ~isempty(subNonNaN)
        ImageWCPingSampleNew(:,(size(ImageWCPingSampleNew, 2)+1):subNonNaN(end)) = NaN;
        ImageWCPingSampleNew(subInf(k),subNonNaN) = v2(subNonNaN);
    end
    R0(subInf(k)) = round(R0(subInf(k))) / coef;
end
F(subInf) = FMode;
ImageWCPingSample = ImageWCPingSampleNew;
nbPixNonNaN = sum(~isnan(ImageWCPingSample));
% n = find(nbPixNonNaN == 0, 1, 'first');
n = find(nbPixNonNaN ~= 0, 1, 'last') + 1; % Modif JMA le 19/11/2020 pour donn�es Ridha FLUME
ImageWCPingSample(:,n:end) = [];
% ImageWCPingSample(ImageWCPingSample == 0) = NaN;

x = 1:size(ImageWCPingSample,2);
a = inherit(Reflectivity, ImageWCPingSample, 'x', x, 'y', 1:nbPings, ...
    'GeometryType', cl_image.indGeometryType('PingSamples'), 'ColormapIndex', 3, 'YUnit', '# Sample', 'TagSynchroX', TagSynchroX, ...
    'Name', ImageName, 'Comments', Comments);

%% Changement de la fr�quence d'�chantillonnage

a = set(a, 'SampleFrequency', F);
a = set(a, 'SonarHeight',     R0);
