function str = infoLectureDatagrams(~, nomFicMat, NumDatagram)

switch NumDatagram
    case 1000 % Reference Point
        groupName = 'Reference Point';
        my_breakpoint
        
    case 1001 % Sensor Offset Position
        groupName = 'Sensor Offset Position';
        my_breakpoint
        
    case 1002 % Calibrated Sensor Offset Position
        groupName = 'Calibrated Sensor Offset Position';
        my_breakpoint
        
    case 1003 % Position
        groupName = 'Position';
        my_breakpoint
        
    case 1004 % Custom Attitude Information
        groupName = 'Custom Attitude Information';
        my_breakpoint
        
    case 1005 % Tide
        groupName = 'Tide';
        my_breakpoint
        
    case 1006 % Altitude
        groupName = 'Altitude';
        my_breakpoint
        
    case 1007 % Motion Over Ground
        groupName = 'Motion Over Ground';
        my_breakpoint
        
    case 1008 % Depth
        groupName = 'depth';
        
    case 1009 % Sound Velocity Profile
        groupName = 'Sound Velocity Profile';
        my_breakpoint
        
    case 1010 % CTD
        groupName = 'soundSpeedProfile';
        
    case 1011 % Geodesy
        groupName = 'Geodesy';
        my_breakpoint
        
    case 1012 % Attitude
        groupName = 'Roll Pitch Heave';
        
    case 1013 % Heading
        groupName = 'Heading';
        
    case 1014 % Survey Line
        groupName = 'Survey Line';
        my_breakpoint
        
    case 1015 % Navigation
        groupName = 'navigation';
        
    case 1016 % Attitude
        groupName = 'attitude';
        
    case 1050 % Single Record Request
        groupName = 'Single Record Request';
        my_breakpoint
        
    case 1200 % Start Recording
        groupName = 'Start Recording';
        my_breakpoint
        
    case 2000 % XYZ Data
        groupName = 'XYZ Data';
        my_breakpoint
        
    case 7000 % 7k Sonar Settings
        groupName = 'SonarSettings';
        
    case 7001 % 7k Configuration
        groupName = 'Configuration';
        my_breakpoint
        
    case 7002 % 7k Pulse Compression
        groupName = 'Pulse Compression';
        my_breakpoint
        
    case 7004 % 7k Beam Geometry
        groupName = 'depth';
        
    case 7005 % 7k Calibration Data
        groupName = 'Calibration Data';
        my_breakpoint
        
    case 7006 % 7k Bathymetric Data
        groupName = 'depth';
        
    case 7007 % 7k Backscatter Imagery Data
        groupName = 'Backscatter Imagery Data';
        my_breakpoint
        
    case 7008 % 7K Generic Data (Mag & Phase)
        groupName = 'Generic Data (Mag & Phase)';
        
    case 7009 % Vertical Depth
        groupName = 'verticalDepth';
        
    case 7010 % TVG Gain Values
        groupName = 'TVG Gain Values';
        my_breakpoint
        
    case 7011 % 7k Image Data
        groupName = 'Image Data';
        my_breakpoint
        
    case 7017 % (7k Beamformed Data) � FP1 Release 7125/7101
        groupName = 'Beamformed Data';
        my_breakpoint
        
    case 7018 % (7k Beamformed Data) � FP1 Release 7125/7101
        groupName = 'BeamRawData';
        
    case 7021 % TODO Quel est son nom ?
        groupName = 'TODO Quel est son nom ?';
        my_breakpoint
        
    case 7022 % S7k Center Version
        groupName = 'Center Version';
        my_breakpoint
        
    case 7027 % 7k RAW Detection Data
        groupName = 'depth';
        
    case 7028 % BeamRawData
        groupName = 'BeamRawData';
        
    case 7030 % SonarInstallation Parameters
        groupName = 'SonarInstallation Parameters';
        my_breakpoint
        
    case 7038 % Raw I&Q Data
        groupName = 'Raw I&Q Data';
        my_breakpoint
        
    case 7041 % Compressed Beamformed Magnitude Data
        groupName = 'WaterColumnDataIFRData';
        
    case 7042 % Compressed Watercolumn Data
%         groupName = 'Compressed Watercolumn Data';
        groupName = 'WaterColumnDataIFRData'; % Trouv� dans fichier VLIZ
%         my_breakpoint
        
    case 7048 % 7k Calibrated Beam Data
        groupName = 'Calibrated Beam Data';
        my_breakpoint
        
    case 7050 % 7k System Events
        groupName = 'System Events';
        my_breakpoint
        
    case 7051 % 7k System Event Message
        groupName = 'System Event Message';
        my_breakpoint
        
    case 7057 % Calibrated Backscatter Imagery Data
        groupName = 'Calibrated Backscatter Imagery Data';
        my_breakpoint
        
    case 7058 % Calibrated Snippet Data
        groupName = 'BeamRawData'; % 'Calibrated Snippet Data';
        
    case 7200 % 7k File Header (First record of the file)
        groupName = 'File Header';
        my_breakpoint
        
    case 7300 % Last record of the file
        groupName = 'File Header';
        my_breakpoint
        
    case 7400 % Time Message
        groupName = 'Time Message';
        my_breakpoint
        
    case 7504 % ????
        groupName = '????';
        my_breakpoint
        
    case 7500 % 7k Remote Control
        groupName = 'Remote Control';
        my_breakpoint
        
    case 7501 % 7k Remote Control Acknowledge
        groupName = 'Remote Control Acknowledge';
        my_breakpoint
        
    case 7502 % 7k Remote Control Not Acknowledge
        groupName = 'Remote Control Not Acknowledge';
        my_breakpoint
        
    case 7503 % Remote Control Sonar Settings
        groupName = 'Remote Control Sonar Settings';
        my_breakpoint
        
    case 7610 % 7k Sound Velocity
        groupName = 'surfaceSoundSpeed';
        
    case 7611 % 7k Absorption Loss
        groupName = 'Absorption Loss';
        my_breakpoint
        
    case 7612 % 7k Spreading Loss
        groupName = 'Spreading Loss';
        my_breakpoint
        
    otherwise
        groupName = '?';
        my_breakpoint
end

[~, nomFic2, Ext] = fileparts(nomFicMat); %#ok<ASGLU>
if isempty(nomFicMat)
    %     str = sprintf('Loading file %s (datagrams %d)', [nomFic2 Ext], NumDatagram);
    nomFic2 = strrep(nomFic2, ['_' groupName], '');
    str = sprintf('S7K cache makeup : %s : Group %s (catching datagrams%d)', nomFic2, groupName, NumDatagram);
else
    %     str = sprintf('Decode datagrams %d (write file %s)', NumDatagram, [nomFic2 Ext]);
    nomFic2 = strrep(nomFic2, ['_' groupName], '');
    groupName(1) = upper(groupName(1));
    str = sprintf('S7K cache makeup : %s : Group %s (catching datagrams %d)', nomFic2, groupName, NumDatagram);
end
% fprintf('%s\n', str)
my_warndlg([str ' : Begin'], 0);
drawnow

