function [flag, nomFicAdoc, ListSignals, repExport] = params_S7K_SurveyReport_SummaryNavigation(~, repExport, varargin)

% [varargin, QuestionParallelTbx] = getPropertyValue(varargin, 'QuestionParallelTbx', 1); %#ok<ASGLU>

ListSignals = [];

%% Nom du survey

nomFicCompens = fullfile(repExport, '*.adoc');
[flag, nomFicAdoc] = my_uigetfile(nomFicCompens, 'Survey Report file name');
if ~flag
    return
end
repExport = fileparts(nomFicAdoc);

%% S�lection des signaux

% ListSignals = {'Speed'; 'CourseVessel'; 'SoundSpeed'; 'VerticalDepth'; 'VesselHeight'
%             'VerticalDepth-VesselHeight'; 'Heading'; 'Time'; 'Drift'; 'DistanceInterPings'; 'Frequency'
%             'GainSelection'; 'PowerSelection'; 'TxPulseWidth'; 'RangeSelection'}; % 'Mode'; 

ListSignals = getResonListSignals; % TODO : attention Mode 
[sub, flag] = my_listdlgMultiple('List of signals', ListSignals, 'InitialValue', [1 3 4 7 10 11]);
if ~flag
    return
end
ListSignals = ListSignals(sub);
