function [flag, nbPingMax] = params_split_resonS7k_files(~)

nbPingMax = 5000;
str1 = 'D�coupage de fichiers .s7k';
str2 = 'Split of .s7k files';
p = ClParametre('Name', Lang('Nombre max de pings par fichier','Maximum number of pings per file'), ...
    'MinValue', 10, 'MaxValue', 10000, 'Value', nbPingMax, 'Format', '%d');
a = StyledParametreDialog('params', p, 'Title', Lang(str1,str2));
a.sizes = [0 -3 -1 -2 -1 -1 0 0];
a.openDialog;
flag = a.okPressedOut;
if ~flag
    return
end
nbPingMax = a.getParamsValue;
