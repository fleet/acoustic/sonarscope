function plotNavSignal(S7k, signalName, varargin)

[varargin, Mute]         = getPropertyValue(varargin, 'Mute',         0);
[varargin, modePlotData] = getPropertyValue(varargin, 'modePlotData', 1); %#ok<ASGLU>

if ischar(signalName)
    signalName = {signalName};
end

N = length(signalName);
hw = create_waitbar('Plot of .s7k navigation and signals', 'N', N);
for k=1:N
    my_waitbar(k, N, hw);
    if k == 1
        Fig = plot_nav_S7K(S7k);
        if N > 1
            nomFicNavigationFig = my_tempname('.fig');
            savefig(Fig, nomFicNavigationFig);
        end
    else
        Fig = openfig(nomFicNavigationFig);
    end
    plot_position_data(S7k, signalName{k}, 'Fig', Fig, 'Mute', Mute, 'modePlotData', modePlotData);
    Fig.Name = [Fig.Name ' - ' signalName{k}];
end
my_close(hw)
if N > 1
    delete(nomFicNavigationFig);
end
