% Affichage des informations contenues dans les datagrams "SonarSettings" d'un fichier .s7k
%
% Syntax
%   plot_7kConfiguration(a)
%
% Input Arguments
%   a : Instance de cl_reson_s7k
%
% Examples
%   nomFic = getNomFicDatabase('RESON7125_20051116_221245.s7k')
%   nomFic = 'D:\RESONSAT\SHOM\donnees-7111\canyon\20071210_010510_PP-7111.s7k'
%   a = cl_reson_s7k('nomFic', nomFic);
%   identSondeur = selectionSondeur(a);
%   plot_7kConfiguration(a, identSondeur)
%   plot_7kConfiguration(a, identSondeur, 'sub', 1:1000)
%
% See also cl_reson_s7k Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function plot_7kConfiguration(this, identSondeur, varargin)

[varargin, SelectedItems] = getPropertyValue(varargin, 'SelectedItems', []);
[varargin, sub]           = getPropertyValue(varargin, 'sub',           []); %#ok<ASGLU>

Fig = FigUtils.createSScFigure;

NbFic = length(this);
hw = create_waitbar('Plot SonarSettings information', 'N', NbFic);
for k=1:NbFic
    my_waitbar(k, NbFic, hw);
    [~, SelectedItems] = plot_7kConfiguration_unitaire(this(k), identSondeur, sub, SelectedItems, Fig);
end
my_close(hw, 'MsgEnd');


function [flag, SelectedItems] = plot_7kConfiguration_unitaire(this, identSondeur, sub, SelectedItems, Fig)

flag = 0;

%% Lecture des datagrams SonarSettings

ListeIdentSondeur = selectionSondeur(this);
if length(ListeIdentSondeur) == 1
    identSondeur = ListeIdentSondeur;
end
Data = read_7kConfiguration(this, identSondeur);
if isempty(Data)
    return
end

%% Selection des signaux � tracer

if isempty(SelectedItems)
    fields = fieldnames(Data);
    subField = strcmp(fields, 'Time');
    fields(subField) = [];
    [flag, SelectedItems] = uiSelectItems('ItemsToSelect', fields);
    if ~flag
        return
    end
end

%% Affichage

[~, nomFic] = fileparts(this.nomFic);

% FigName = sprintf('Sondeur %d Serial Number  %d', Data.EmModel, Data.SystemSerialNumber);

% figure(Fig);
% set(Fig, 'name', FigName)

hc = findobj(gca, 'Type', 'line');
nbCourbes = length(hc);
ColorOrder = get(gca, 'ColorOrder');
nbCoul = size(ColorOrder,1);
iCoul = mod(nbCourbes-1, nbCoul) + 1;

T = datetime(Data.Time.timeMat, 'ConvertFrom', 'datenum');
% str = timeMat2str([T(1), T(end)]);
% str = [str repmat(' ', 2, 2)]';
% str = sprintf('display(''%s - %s'')', nomFic, str(:));
str = sprintf('display(''%s - %s  %s'')', nomFic, char(T(1)), char(T(end)));

% T = Data.Time;
if isempty(sub)
    sub = 1:length(T);
end

% t = Data.Time.timeMat;
N = length(SelectedItems);
if N == 0
    return
end
for k=1:N
    NomSignal = SelectedItems{k};
    Y = Data.(NomSignal);
    if length(Y) == 1
        Y = repmat(Y, size(sub));
    end
    
    haxe(k) = subplot(N, 1, k); %#ok<AGROW>
    h = PlotUtils.createSScPlot(T(sub), Y(sub)); hold on;
    set(h, 'Color', ColorOrder(iCoul,:))
    set(h, 'ButtonDownFcn', str)
    grid on; title(NomSignal)

    %     msg = sprintf('%s : %s %s', NomSignal, num2strCode(Y));
    %     disp(msg)
end
linkaxes(haxe, 'x'); axis tight; drawnow;

flag = 1;
