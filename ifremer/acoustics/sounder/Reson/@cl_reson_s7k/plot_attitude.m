% Affichage des informations contenues dans les datagrams "attitude" d'un fichier .s7k
%
% Syntax
%   [flag, Fig] = plot_attitude(a)
%
% Input Arguments
%   a : Instance de cl_reson_s7k
%
% Examples
%   nomFic = getNomFicDatabase('RESON7125_20051116_221245.s7k')
%   a = cl_reson_s7k('nomFic', nomFic);
%   plot_attitude(a)
%   plot_attitude(a, 'sub', 1:1000)
%
% See also cl_reson_s7k Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [flag, Fig] = plot_attitude(this, identSondeur, varargin)

[varargin, sub] = getPropertyValue(varargin, 'sub', []);
[varargin, Fig] = getPropertyValue(varargin, 'Fig', []); %#ok<ASGLU>

if isempty(Fig)
    Fig = FigUtils.createSScFigure('Name', 'Attitude datagrams', 'Position', centrageFig(1600, 800));
else
    figure(Fig);
end

%% Boucle sur les fichiers

NbFic = length(this);
hw = create_waitbar('Plot .s7k Attitude data', 'N', NbFic);
for kFic=1:NbFic
    my_waitbar(kFic, NbFic, hw);
    plot_attitude_unitaire(this(kFic), identSondeur, Fig, 'sub', sub, 'Fig', Fig);
end
my_close(hw, 'MsgEnd');
flag = 1;


function plot_attitude_unitaire(this, identSondeur, Fig, varargin)

[varargin, sub] = getPropertyValue(varargin, 'sub', []); %#ok<ASGLU>

%% Lecture des datagrams d'attitude

Data = read_attitude(this, identSondeur);
if isempty(Data) || isempty(Data.Time.timeMat)
    return
end

%% Affichage de la navigation

[~, nomFic] = fileparts(this.nomFic);

% FigName = sprintf('Sondeur %d Serial Number  %d', Data.EmModel, Data.SystemSerialNumber);

figure(Fig);
% set(Fig, 'name', FigName)

hc = findobj(gca, 'Type', 'line');
nbCourbes = length(hc);
ColorOrder = get(gca, 'ColorOrder');
nbCoul = size(ColorOrder,1);
iCoul = mod(nbCourbes-1, nbCoul) + 1;

T = Data.Time.Datetime;
T.Format = 'yyyy-MM-dd HH:mm:ss.SSS';
str = sprintf('display(''%s - %s %s'')', nomFic, char(T(1)), char(T(end)));

fields = {'Roll'; 'Pitch'; 'Heave'; 'Heading'};
Units  = {'deg'; 'deg'; 'm'; 'deg'};

if isempty(sub)
    sub = 1:length(T);
end

for k=1:length(fields)
    Y = Data.(fields{k});
    
    hAxe(k) = subplot(4, 1, k); %#ok<AGROW>
    h = PlotUtils.createSScPlot(T(sub), Y(sub)); hold on;
    set(h, 'Color', ColorOrder(iCoul,:))
    set(h, 'ButtonDownFcn', str)
    Title = sprintf('%s (%s)', fields{k}, Units{k});
    grid on; title(Title);
    
    %     msg = sprintf('%s : %s %s', strFields(k,:), num2strCode(Y));
    %     if ~isempty(units{k})
    %         msg = sprintf('%s (%s)', msg, units{k});
    %     end
    %     disp(msg)
end
linkaxes(hAxe,'x'); axis tight; drawnow;
