% Affichage des informations relatives aux datagrams contenus dans un fichier .s7k
%
% Syntax
%   plot_index(a)
% 
% Input Arguments 
%   a : Instance de cl_reson_s7k
%
% Examples
%   nomFic = getNomFicDatabase('RESON7125_20051116_221245.s7k')
%   a = cl_reson_s7k('nomFic', nomFic);
%   plot_index(a)
%
% See also cl_reson_s7k histo_index Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function plot_index(this, identSondeur)

for k=1:length(this)
    plot_index_unitaire(this(k), identSondeur)
end


function plot_index_unitaire(this, identSondeur)

%% Lecture du fichier d'index

[flag, DataIndex] = readFicIndex(this, identSondeur);
if ~flag
    return
end

Date             = DataIndex(1,:);
Heure            = DataIndex(2,:);
Position         = DataIndex(3,:);
Taille           = DataIndex(4,:);
TypeDatagram     = DataIndex(5,:);
PingCounter      = DataIndex(6,:);
PingSequence     = DataIndex(7,:);
DeviceIdentifier = DataIndex(8,:);

% if get_LevelQuestion >= 3
    [~, nomFic] = fileparts(this.nomFic);
    figure('name', nomFic);
    haxe(1) = subplot(8,1,1); PlotUtils.createSScPlot(Date, '+');             grid on; title('Date');
    haxe(2) = subplot(8,1,2); PlotUtils.createSScPlot(Heure, '+');            grid on; title('Heure');
    haxe(3) = subplot(8,1,3); PlotUtils.createSScPlot(Taille, '+');           grid on; title('Taille');
    haxe(4) = subplot(8,1,4); PlotUtils.createSScPlot(Position, '+');         grid on; title('Position');
    haxe(5) = subplot(8,1,5); PlotUtils.createSScPlot(TypeDatagram, '+');     grid on; title('TypeDatagram');
    haxe(6) = subplot(8,1,6); PlotUtils.createSScPlot(PingCounter, '+');      grid on; title('PingCounter');
    haxe(7) = subplot(8,1,7); PlotUtils.createSScPlot(PingSequence, '+');     grid on; title('PingSequence');
    haxe(8) = subplot(8,1,8); PlotUtils.createSScPlot(DeviceIdentifier, '+'); grid on; title('DeviceIdentifier');

    linkaxes(haxe, 'x'); axis tight; drawnow;
% end

% nomSignal{1} = 'Date';
% nomSignal{2} = 'Heure';
% nomSignal{3} = 'Taille';
% nomSignal{4} = 'Position';
% nomSignal{5} = 'TypeDatagram';
% nomSignal{6} = 'PingCounter';
% 
% signal{1} = Date;
% signal{2} = Heure;
% signal{3} = Taille;
% signal{4} = Position;
% signal{5} = TypeDatagram;
% signal{6} = PingCounter;
% 
% ex2 = cli_visu_caraibes;
% set(ex2, 'lstVec', signal);
% set(ex2, 'lstNomsVec', nomSignal);
% editobj(ex2) ;
% 
