% Trace de la navigation et des capteurs de vitesse et de cap d'un fichier .all
%
% Syntax
%   fig = plot_navigation(a)
%
% Input Arguments
%   a : Instance de cl_reson_s7k
%
% Name-Value Pair Arguments
%   fullListe : 1=affichage que des datagrams presents, 0=tous les datagrams
%
% Output Arguments
%   fig : Numero de la figure de la navigation
%
% Examples
%   nomFic = getNomFicDatabase('RESON7125_20051116_221245.s7k')
%   a = cl_reson_s7k('nomFic', nomFic);
%   plot_navigation(a)
%   plot_navigation(a, 'Wc', 0.05)
%
%   nomDir = '/home1/jackson/7K'
%   liste = listeFicOnDir2(nomDir, '.s7k')
%   for k=1:3%length(liste)
%       a(k) = cl_reson_s7k('nomFic', liste{k});
%   end
%   plot_navigation(a)
%
% See also cl_reson_s7k plot_index Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [Fig, flag] = plot_navigation(this, varargin)

% Plus utilis� en principe

my_breakpoint


[varargin, LineWidth] = getPropertyValue(varargin, 'LineWidth', 3);
[varargin, Fig]       = getPropertyValue(varargin, 'Fig',       []); %#ok<ASGLU>

flag = 1;

S0 = cl_reson_s7k.empty();
% Ident = rand(1);

%% Cr�ation de la fen�tre avec les menus et callbacks sp�cialis�s pour la nav

Fig = plot_navigation_Figure(Fig);

%% Traitement des fichiers

flagNavTrouvee = 0;
NbFic = length(this);
str1 = 'Visualisation de la navigation des .s7k';
str2 = 'Plot .s7k navigation';
hw = create_waitbar(Lang(str1,str2), 'N', NbFic);
for k=1:length(this)
    my_waitbar(k, NbFic, hw);
    
    identSondeur = get(this(k), 'SounderName');
    
    %% Lecture des datagrammes de navigation
    
    Data = read_navigation(this(k), identSondeur);
    if isempty(Data) || isempty(Data.Latitude)
        str1 = sprintf('Il n''y a pas de navigation dans le fichier "%s".',  this(k).nomFic);
        str2 = sprintf('There is no navigation in file "%s".',  this(k).nomFic);
        my_warndlg(Lang(str1,str2), 0, 'Tag', 'PasDeNavigationDansCeFichier');
        continue
    end
%     FigUtils.createSScFigure; PlotUtils.createSScPlot(Data.Heading, 'r'); grid on; hold on; PlotUtils.createSScPlot(Data.CourseVessel, 'b')
    flagNavTrouvee = 1;
        
    [flag, DataDepth] = read_depth_s7k(this(k));
    if ~flag
        continue
    end    

    [SonarTime, Latitude, Longitude, Heading, CourseVessel, Immersion, provenance] = getNavFromVariousDatagrams(S0, Data, DataDepth); %#ok<ASGLU>
    
    plot_navigation(this(k).nomFic, Fig, Longitude, Latitude, ...
        SonarTime.timeMat, Heading, 'LineWidth', LineWidth, ...
        'CourseVessel', CourseVessel, 'Immersion', Immersion);
end
my_close(hw);
if ~flagNavTrouvee
    my_close(Fig)
    flag = 0;
end

%{
% subplot(N, 1, 1);  plot(diff(Time));    grid on; title('diff(Time)');
% subplot(N, 1, 5);  plot(CourseVessel);  grid on; title('CourseVessel');
%}
