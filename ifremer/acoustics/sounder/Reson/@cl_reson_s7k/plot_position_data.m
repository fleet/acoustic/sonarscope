% Trace de la navigation et des capteurs de vitesse et de cap d'un fichier .s7k
%
% Syntax
%   plot_position_data(nomFic, Invite)
%
% Input Arguments
%   nomFic : Liste de fichiers .s7k
%   Invite : VerticalDepth | Heading | Speed | CourseVessel | SoundSpeed
%
% Examples
%   liste = getNomFicDatabase('RESON7125_20051116_221245.s7k')
%   plot_position_data(S0, liste, 'Heading')
%   plot_position_data(S0, liste, 'Speed')
%   plot_position_data(S0, liste, 'CourseVessel')
%   plot_position_data(S0, liste, 'SoundSpeed')
%   plot_position_data(S0, liste, 'VerticalDepth')
%   plot_position_data(S0, liste, 'Mode')
%   plot_position_data(S0, liste, 'Time')
%   plot_position_data(S0, liste, 'Drift')
%
%   plot_position_data(liste, 'Heading', 'Wc', 0.05)
%
% See also cl_reson_s7k plot_index Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function InfoSignal = plot_position_data(this, Invite, varargin)

[varargin, Wc]           = getPropertyValue(varargin, 'Wc',           []);
[varargin, Ordre]        = getPropertyValue(varargin, 'Ordre',        2);
[varargin, Mute]         = getPropertyValue(varargin, 'Mute',         1);
[varargin, modePlotData] = getPropertyValue(varargin, 'modePlotData', 1);
[varargin, Fig]          = getPropertyValue(varargin, 'Fig',          []); %#ok<ASGLU>

InfoSignal = [];

%% Identification du sondeur

NbFic = length(this);
for k=1:NbFic
    identSondeur = selectionSondeur(this(k));
    if ~isempty(identSondeur)
        break
    end
end

%% Pr�paration du graphique

nbColors = 64;
Ident = rand(1);

%% Lecture des valeurs

flagZLimQComputed = 0;
ZLimQ  = repmat([Inf -Inf], NbFic, 1);
ZLimQN = zeros(NbFic, 1);
XLim = [Inf -Inf];
YLim = [Inf -Inf];
ZLim = [Inf -Inf];
Unit = [];
DataUnique = [];
for k=1:NbFic
    [flag, Z, Position, Colors] = read_Invite(this(k), identSondeur, Invite, nbColors, 'Histo');
    if ~flag || isempty(Z) || ~isfield(Z, 'Data') || isempty(Z.Data)
        continue
    end
    
    DataUniqueFile = checkDataUnique(Z.Data(:));
    DataUnique = unique([DataUnique; DataUniqueFile(:)]);
    
    Unit = Z.Unit;
    switch Invite
        case 'Drift'
            ZLim = [-5 5];
        case {'Heading'; 'CourseVessel'}
            ZLim = [0 360];
        otherwise
            ZLim(1) = min(ZLim(1), double(min(Z.Data, [], 'omitnan')));
            ZLim(2) = max(ZLim(2), double(max(Z.Data, [], 'omitnan')));
            [ValStats, ~] = stats(Z.Data);
            ZLimQ(k,1) = ValStats.Quant_25_75(1);
            ZLimQ(k,2) = ValStats.Quant_25_75(2);
            ZLimQN(k)  = length(Z.Data(:));
            flagZLimQComputed = 1;
    end
    XLim(1) = min(XLim(1), min(Position.Longitude, [], 'omitnan'));
    XLim(2) = max(XLim(2), max(Position.Longitude, [], 'omitnan'));
    YLim(1) = min(YLim(1), min(Position.Latitude,  [], 'omitnan'));
    YLim(2) = max(YLim(2), max(Position.Latitude,  [], 'omitnan'));
end
if isinf(XLim)
    return
end

if length(DataUnique) < 20 % MaxNumberDataUnique
    InfoSignal.DataUnique = DataUnique;
else
    InfoSignal.DataUnique = [];
end
InfoSignal.Unit = Unit;

%% Saisie des bornes de la table de couleurs

if Mute
    if flagZLimQComputed
        subNaN = find(isnan(sum(ZLimQ,2)));
        ZLimQ(subNaN,:) = [];
        ZLimQN(subNaN)  = [];
        if isempty(ZLimQN)
            return
        end
        ZLimTemp = sum(ZLimQ.*ZLimQN, 1) / sum(ZLimQN);
        if ZLimTemp(1) == ZLimTemp(2) % Ajout JMA le 11/02/2018 pour signal MinPulseLength
            ZLim = ZLim + [-0.5 0.5];
        else
            ZLim = ZLimTemp;
        end
    end
else
    [flag, ZLim] = saisie_CLim(min(ZLim), max(ZLim), Unit, 'SignalName', Invite, 'hFigHisto', 87698);
    if ~flag
        return
    end
end

%% Trac� des valeurs

if isempty(Fig)
    Fig = figure('name', 'Position datagrams');
else
    figure(Fig);
end
set(Fig, 'UserData', Ident)
if isempty(Unit)
    Titre = Invite;
else
    Titre = sprintf('%s (%s)', Invite, Unit);
end
title(Titre)

W = axis;
hold on;
x = linspace(XLim(1), XLim(1)+100*nbColors*eps(XLim(1)), nbColors);
y = linspace(YLim(1), YLim(2), nbColors);
imagesc(x, y, linspace(ZLim(1), ZLim(2), nbColors)); colormap(Colors), colorbar
hold on;
axis(W)

for k=1:length(this)
    [~, SystemSerialNumber] = plotS7k_navigation_data_unitaire(this(k), identSondeur, Invite, ZLim, Fig, ...
        nbColors, Colors, Wc, Ordre, modePlotData); %#ok<ASGLU>
end

% FigName = sprintf('Serial Number  %d - %s', SystemSerialNumber, Invite);
% figure(Fig);
% set(Fig, 'name', FigName)
end

function [EmModel, SystemSerialNumber] = plotS7k_navigation_data_unitaire(this, identSondeur, Invite, ZLim, Fig, ...
    nbColors, Colors, Wc, Ordre, modePlotData)

EmModel            = [];
SystemSerialNumber = [];

S0 = cl_reson_s7k.empty();

figure(Fig)

%% Lecture des datagrams de navigation

[flag, DataDepth] = read_depth_s7k(this);
if ~flag
    return
end

nomFic = this.nomFic;

DataPosition = read_navigation(this, identSondeur, 'Wc', Wc, 'Ordre', Ordre);
if isempty(DataPosition) || isempty(DataPosition.Latitude)
    return
end

[SonarTime, Latitude, Longitude, Heading, CourseVessel, Immersion, provenance] = getNavFromVariousDatagrams(S0, DataPosition, DataDepth); %#ok<ASGLU>
SonarDatetime = datetime(SonarTime.timeMat, 'ConvertFrom', 'datenum');

[flag, Z] = read_Invite(this, identSondeur, Invite, nbColors, 'DataPosition', DataPosition, 'DataDepth', DataDepth);
if ~flag || isempty(Z.Data)
    return
end

if modePlotData == 1
    Value = interp1(Z.Time, Z.Data, SonarTime);
    if ZLim(2) == ZLim(1)
        ZLim(1) = ZLim(1) - 0.5;
        ZLim(2) = ZLim(2) + 0.5;
    end
    coef = (nbColors-1) / (ZLim(2) - ZLim(1));
    Value = ceil((Value - ZLim(1)) * coef);
    
    %% Affichage de la navigation
    
    % EmModel = DataPosition.EmModel;
    if isfield(DataPosition, 'SystemSerialNumber')
        SystemSerialNumber = DataPosition.SystemSerialNumber;
    else
        SystemSerialNumber = 'Unknown';
    end
    
    for k=1:nbColors
        sub = find(Value == (k-1));
        if ~isempty(sub)
            h = plot(Longitude(sub), Latitude(sub), '+');
            set(h, 'Color', Colors(k,:))
            set_HitTest_Off(h)
        end
    end
    
    sub = find(Value < 0);
    if ~isempty(sub)
        h = plot(Longitude(sub), Latitude(sub), '+');
        set(h, 'Color', Colors(1,:)*0.8)
        set_HitTest_Off(h)
    end
    
    sub = find(Value > (nbColors-1));
    if ~isempty(sub)
        h = plot(Longitude(sub), Latitude(sub), '+');
        set(h, 'Color', Colors(nbColors,:)*0.8)
    end
    
else
    Value = my_interp1(Z.Datetime, Z.Data, SonarDatetime);
    h = scatter(Longitude(:), Latitude(:), [], Value, '.');
    set(h, 'Tag', nomFic)
    set_HitTest_Off(h)
    setappdata(h, 'Signature1', 'scatter')
    
    hC = colorbar;
    set(gca, 'CLim', ZLim);
    GCA = gca;
    set(hC, 'ButtonDownFcn', @callbackColorbar);
    setappdata(hC, 'Signature1', 'Colorbar')
end

axisGeo;
grid on;
% colormap(Colors)
% colorbar % Supprim� le 7/11/2006 car ca remet les infos de colorbar de 0 � 64
drawnow

    function callbackColorbar(varargin)
        try
            [hImcontrast, vSSC] = my_imcontrast(GCA); %#ok<ASGLU>
        catch ME %#ok<NASGU>
%             fprintf('%s\n', getReport(ME));
%             str1 = 'Il est pr�f�rable d''utiliser la version corrig�e de imcontrast. Pour cela, voir sous https://forge.ifremer.fr/frs/?group_id=126';
%             str2 = 'It is more useful to use imcontrast version corrected for adjust data. See https://forge.ifremer.fr/frs/?group_id=126';
%             str1 = sprintf('%s\n\n%s', ME.message, str1);
%             str2 = sprintf('%s\n\n%s', ME.message, str2);
%             my_warndlg(Lang(str1,str2),1);
            hImcontrast = imcontrast(GCA); % Appel sans vSSC
        end
        waitfor(hImcontrast)
        % CLim = get(this.hAxePrincipal, 'CLim');
    end
end

function DataUnique = checkDataUnique(Data)
n = numel(Data);
if n < 100
    DataUnique = unique(Data(:));
else
    sub = floor(linspace(1,n,100));
    DataUnique = unique(Data(sub));
    if length(DataUnique) < 20 % MaxNumberDataUnique : d�finit ind�pendemment � deux endroits dans ce .m
        DataUnique = unique(Data(:));
    end
end
end
