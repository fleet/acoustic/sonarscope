% Affichage des informations contenues dans les datagrams "SonarSettings" d'un fichier .s7k
%
% Syntax
%   plot_sonarSettings(a)
%
% Input Arguments
%   a : Instance de cl_reson_s7k
%
% Examples
%   nomFic = getNomFicDatabase('RESON7125_20051116_221245.s7k')
%   nomFic = 'D:\RESONSAT\SHOM\donnees-7111\canyon\20071210_010510_PP-7111.s7k'
%   a = cl_reson_s7k('nomFic', nomFic);
%   identSondeur = selectionSondeur(a);
%   plot_sonarSettings(a, identSondeur)
%   plot_sonarSettings(a, identSondeur, 'sub', 1:1000)
%
% See also cl_reson_s7k Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [flag, Fig, SelectedItems] = plot_sonarSettings(this, identSondeur, varargin)

[varargin, Fig]           = getPropertyValue(varargin, 'Fig',           []);
[varargin, SelectedItems] = getPropertyValue(varargin, 'SelectedItems', []);
[varargin, Units]         = getPropertyValue(varargin, 'Units',         []);
[varargin, sub]           = getPropertyValue(varargin, 'sub',           []); %#ok<ASGLU>

if isempty(Fig)
    Fig = FigUtils.createSScFigure('Name', 'SonarSettings datagrams', 'Position', centrageFig(1600, 800));
else
    FigUtils.createSScFigure(Fig);
end

%% Boucle sur les fichiers

NbFic = length(this);
hw = create_waitbar('Plot SonarSettings information', 'N', NbFic);
for kFic=1:NbFic
    my_waitbar(kFic, NbFic, hw);
    [flag, SelectedItems] = plot_sonarSettings_unitaire(this(kFic), identSondeur, Fig, SelectedItems, Units, sub);
    if ~flag
        return
    end
end
my_close(hw, 'MsgEnd');
flag = 1;


function [flag, SelectedItems] = plot_sonarSettings_unitaire(this, identSondeur, Fig, SelectedItems, Units, sub)


%% Lecture des datagrams SonarSettings

ListeIdentSondeur = selectionSondeur(this);
if length(ListeIdentSondeur) == 1
    identSondeur = ListeIdentSondeur;
end
Data = read_sonarSettings(this, identSondeur);
if isempty(Data)
    return
end

%% Selection des signaux � tracer

if isempty(SelectedItems)
    fields = fieldnames(Data);
    subField = strcmp(fields, 'Time');
    fields(subField) = [];
    subField = strcmp(fields, 'SystemSerialNumber');
    fields(subField) = [];
    InitValue(1) = find(strcmp(fields, 'GainSelection'));
    InitValue(2) = find(strcmp(fields, 'PowerSelection'));
    InitValue(3) = find(strcmp(fields, 'TxPulseWidth'));
    InitValue(4) = find(strcmp(fields, 'RangeSelection'));
    [flag, SelectedItems] = uiSelectItems('ItemsToSelect', fields, 'InitValue', InitValue);
    if ~flag
        return
    end
end

%% Affichage

[~, nomFic] = fileparts(this.nomFic);

% FigName = sprintf('Sondeur %d Serial Number  %d', Data.EmModel, Data.SystemSerialNumber);

FigUtils.createSScFigure(Fig);
% set(Fig, 'name', FigName)

hc = findobj(gca, 'Type', 'line');
nbCourbes = length(hc);
ColorOrder = get(gca, 'ColorOrder');
nbCoul = size(ColorOrder,1);
iCoul = mod(nbCourbes-1, nbCoul) + 1;

T = Data.Time.Datetime;
if isempty(T)
    return
end

if isempty(sub)
    %     sub = 1:length(T);
    
    % Rustine pour �viter bug fichier 20080222_000106 : PB A RESOUDRE, pb
    % fichier ou pb lecture ??????????????????????????????????????????????????
    sub = find((T > datetime(0, 'ConvertFrom', 'datenum')) & (T < datetime(2222,1,1)));
end
T.Format = 'yyyy-MM-dd HH:mm:ss.SSS';
str = sprintf('display(''%s - %s %s'')', nomFic, char(T(sub(1))), char(T(sub(end))));

N = length(SelectedItems);
if N == 0
    return
end
for k=1:N
    NomSignal = SelectedItems{k};
    Y = Data.(NomSignal);
    
    haxe(k) = subplot(N, 1, k); %#ok<AGROW>
    h = PlotUtils.createSScPlot(T(sub), Y(sub)); hold on;
    set(h, 'Color', ColorOrder(iCoul,:))
    set(h, 'Marker', '.')
    set(h, 'ButtonDownFcn', str)
    if isempty(Units)
        Title = NomSignal;
    else
        Title = sprintf('%s (%s)', NomSignal, Units{k});
    end
    grid on; title(Title)

    
    %     msg = sprintf('%s : %s %s', NomSignal, num2strCode(Y));
    %     disp(msg)
end
linkaxes(haxe, 'x'); axis tight; drawnow;

flag = 1;
