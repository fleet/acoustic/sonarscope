% Affichage des informations contenues dans les datagrams "soundSpeedProfile" d'un fichier .s7k
%
% Syntax
%   plot_soundSpeedProfile(a)
%
% Input Arguments
%   a : Instance de cl_reson_s7k
%
% Examples
%   nomFic = getNomFicDatabase('RESON7125_20051116_221245.s7k')
%   a = cl_reson_s7k('nomFic', nomFic);
%   plot_soundSpeedProfile(a)
%
% See also cl_reson_s7k Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function plot_soundSpeedProfile(this, identSondeur, varargin)

% [varargin, sub] = getPropertyValue(varargin, 'sub', []);
[varargin, Fig] = getPropertyValue(varargin, 'Fig', []); %#ok<ASGLU>

if isempty(Fig)
    for k=1:length(this)
        [~, nom] = fileparts(get(this(k), 'nomFic'));
        Fig(k) = FigUtils.createSScFigure('Name', sprintf('SoundSpeedProfile \n%s', nom));
    end
else
    FigUtils.createSScFigure(Fig);
    Fig = repmat(Fig, 1, length(this));
end


NbFic = length(this);
hw = create_waitbar('Plot .s7k Sound Speed Profile', 'N', NbFic);
for i=1:NbFic
    my_waitbar(i, NbFic, hw);
    plot_soundSpeedProfile_unitaire(this(i), identSondeur, Fig(i))
end
my_close(hw, 'MsgEnd');
    
    
function  plot_soundSpeedProfile_unitaire(this, identSondeur, Fig)

%% Lecture des datagrams d'soundSpeedProfile

Data = read_soundSpeedProfile(this, identSondeur);
if isempty(Data)
    return
end

%% Affichage de la navigation

[~, nomFic] = fileparts(this.nomFic);

if isempty(Data(1).Depth)
    return
end

% FigName = sprintf('Sondeur %d', Data.EmModel);

FigUtils.createSScFigure(Fig);
% set(Fig, 'name', FigName)
grid on; hold on;

hc = findobj(gca, 'Type', 'line');
nbCourbes = length(hc);
ColorOrder = get(gca, 'ColorOrder');
nbCoul = size(ColorOrder,1);
iCoul = mod(nbCourbes-1, nbCoul) + 1;

for k=1:length(Data)
    t = Data(k).TimeStartOfUse.timeMat;
    str = timeMat2str([t(1), t(end)]);
    str = [str repmat(' ', 2, 2)]';
    
    
    for i=1:size(Data(k).Depth,1)
        Z = Data(k).Depth(i,:);
        C = Data(k).SoundSpeed(i,:);
        T = Data(k).WaterTemperature(i,:);
        S = Data(k).Salinity(i,:);
        A = Data(k).Absorption(i,:);
        
        hAxe(1) = subplot(1,4,1);
        h = PlotUtils.createSScPlot(T, Z); grid on; title('Temp (�c)');
        set(h, 'Color', ColorOrder(iCoul,:))
        strProfil = sprintf('display(''%s - %s - %d'')', nomFic, str(:), i);
        set(h, 'ButtonDownFcn', strProfil)
        
        hAxe(2) = subplot(1,4,2);
        h = PlotUtils.createSScPlot(S, Z); grid on; title('Sal (ppt)');
        set(h, 'Color', ColorOrder(iCoul,:))
        set(h, 'ButtonDownFcn', strProfil)
        
        hAxe(3) = subplot(1,4,3);
        h = PlotUtils.createSScPlot(C, Z); grid on; title('Cel (m/s)');
        set(h, 'Color', ColorOrder(iCoul,:))
        set(h, 'ButtonDownFcn', strProfil)
        
        hAxe(4) = subplot(1,4,4);
        h = PlotUtils.createSScPlot(A, Z); grid on; title('Abs (dB/km)');
        set(h, 'Color', ColorOrder(iCoul,:))
        set(h, 'ButtonDownFcn', strProfil)
    end
    linkaxes(hAxe, 'y'); axis tight; drawnow;
end

