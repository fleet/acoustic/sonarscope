% Affichage des informations contenues dans les datagrams "surfaceSoundSpeed" d'un fichier .s7k
%
% Syntax
%   plot_surfaceSoundSpeed(a)
%
% Input Arguments
%   a : Instance de cl_reson_s7k
%
% Examples
%   nomFic = getNomFicDatabase('RESON7125_20051116_221245.s7k')
%   a = cl_reson_s7k('nomFic', nomFic);
%   plot_surfaceSoundSpeed(a)
%
% See also cl_reson_s7k Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function plot_surfaceSoundSpeed(this, identSondeur, varargin)

[varargin, sub] = getPropertyValue(varargin, 'sub', []);
[varargin, Fig] = getPropertyValue(varargin, 'Fig', []); %#ok<ASGLU>

if isempty(Fig)
    Fig = figure('name', 'SoundSpeed datagrams');
else
    figure(Fig);
end


NbFic = length(this);
hw = create_waitbar('Plot .s7k Surface Sound Speed', 'N', NbFic);
for i=1:length(this)
    my_waitbar(i, NbFic, hw);
    plot_surfaceSoundSpeed_unitaire(this(i), sub, Fig)
end
my_close(hw, 'MsgEnd');


function plot_surfaceSoundSpeed_unitaire(this, sub, Fig)

%% Lecture des datagrams d'surfaceSoundSpeed

[flag, Data] = read_depth_s7k(this);
if ~flag
    return
end

%% Affichage de la navigation

[~, nomFic] = fileparts(this.nomFic);

FigName = sprintf('Sondeur %d Serial Number  %d', Data.EmModel, Data.SystemSerialNumber);

figure(Fig);
set(Fig, 'name', FigName)

hc = findobj(gca, 'Type', 'line');
nbCourbes = length(hc);
ColorOrder = get(gca, 'ColorOrder');
nbCoul = size(ColorOrder,1);
iCoul = mod(nbCourbes-1, nbCoul) + 1;

t = Data.Time.timeMat;
str = timeMat2str([t(1), t(end)]);
str = [str repmat(' ', 2, 2)]';
str = sprintf('display(''%s - %s'')', nomFic, str(:));

T = Data.Time;
if isempty(sub)
    sub = 1:length(T);
end

h = plot(T(sub), Data.SoundSpeed); hold on;
set(h, 'Color', ColorOrder(iCoul,:))
set(h, 'ButtonDownFcn', str)
grid on; title('Surface sound speed (m/s)')
