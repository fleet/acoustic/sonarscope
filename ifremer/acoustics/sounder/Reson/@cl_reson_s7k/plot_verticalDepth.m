% Trace de la verticalDepth et des capteurs de vitesse et de cap d'un fichier .all
%
% Syntax
%   fig = plot_verticalDepth(a)
%
% Input Arguments
%   a : Instance de cl_reson_s7k
%
% Name-Value Pair Arguments
%   fullListe : 1=affichage que des datagrams presents, 0=tous les datagrams
%
% Output Arguments
%   fig : Numero de la figure de la verticalDepth
%
% Examples
%   nomFic = getNomFicDatabase('RESON7125_20051116_221245.s7k')
%   a = cl_reson_s7k('nomFic', nomFic);
%   plot_verticalDepth(a)
%
%   nomDir = '/home3/jackson/calimero/EM1002/ZoneB/B20040916_1'
%   nomDir = '/home1/jackson/7K'
%   liste = listeFicOnDir2(nomDir, '.s7k')
%   for k=1:3%length(liste)
%       a(k) = cl_reson_s7k('nomFic', liste{k});
%   end
%   plot_verticalDepth(a)
%
% See also cl_reson_s7k plot_index Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function Fig = plot_verticalDepth(this, identSondeur, varargin)

Ident = rand(1);

[varargin, Fig] = getPropertyValue(varargin, 'Fig', []);
if isempty(Fig)
    Fig = figure('name', 'VerticalDepth datagrams');
else
    figure(Fig);
end
set(Fig, 'Tag', 'cl_reson_s7k/plot_verticalDepth/Nav', 'UserData', Ident)


NbFic = length(this);
hw = create_waitbar('Plot .s7k Vertical Depth', 'N', NbFic);
for k=1:length(this)
    my_waitbar(k, NbFic, hw);
    plot_verticalDepth_unitaire(this(k), identSondeur, Fig, num2str(k), varargin{:});
end
my_close(hw, 'MsgEnd');


function plot_verticalDepth_unitaire(this, identSondeur, Fig, TagLine, varargin)

%% Lecture des datagrams de verticalDepth

Data = read_verticalDepth(this, identSondeur);
if isempty(Data.Latitude)
    return
end

%% Affichage de la verticalDepth

[nomDir, nomFic] = fileparts(this.nomFic);

% FigName = sprintf('Sondeur %d Serial Number  %d', Data.EmModel, Data.SystemSerialNumber);
figure(Fig);
% set(Fig, 'name', FigName)

hc = findobj(gca, 'Type', 'line');
nbCourbes = length(hc);
ColorOrder = get(gca, 'ColorOrder');
nbCoul = size(ColorOrder,1);
iCoul = mod(nbCourbes-1, nbCoul) + 1;

hold on;
h = plot(Data.Longitude, Data.Latitude, varargin{:});
set(h, 'Color', ColorOrder(iCoul,:))
title('Navigation')

t = Data.Time.timeMat;
str = timeMat2str([t(1), t(end)]);
str = [str repmat(' ', 2, 2)]';

% str = sprintf('display(''%s - %s'')', nomFic, str1(:));
str = sprintf('display(''%s - %s | %s %s | %s %s'')', ...
    fullfile(nomDir, nomFic), str(:), ...
    lat2str(Data.Latitude(1)), ...
    lon2str(Data.Longitude(1)), ...
    lat2str(Data.Latitude(end)), ...
    lon2str(Data.Longitude(end)));
set(h, 'ButtonDownFcn', str)
if ~isempty(TagLine)
    set(h, 'Tag', TagLine)
end

axisGeo;
grid on;
drawnow

% figure
% N = 6;
% subplot(N, 1, 1);  plot(diff(Time));    grid on; title('diff(Time)');
% subplot(N, 1, 2);  plot(Lat);           grid on; title('Latitude');
% subplot(N, 1, 3);  plot(Lon);           grid on; title('Longitude');
% subplot(N, 1, 4);  plot(Speed);         grid on; title('Speed');
% subplot(N, 1, 5);  plot(AlongTrackDistance);  grid on; title('AlongTrackDistance');
% subplot(N, 1, 6);  plot(Heading);       grid on; title('Heading');
