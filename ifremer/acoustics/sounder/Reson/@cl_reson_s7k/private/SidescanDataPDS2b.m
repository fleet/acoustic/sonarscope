function [b, Carto] = SidescanDataPDS2b(Data, DataDepth, identSondeur, nomFic, Carto)

nomDir = fileparts(nomFic);
[nomFicSidescan, flagFilesExist] = ResonSidescanPDSNames(nomFic, identSondeur);
nomDirMat = fullfile(nomDir, 'SonarScope');

[N, NbSamples] = size(Data.Amplitude);

% ImageName{1} = 'SidescanReflectivity';
ImageName{1} = 'SidescanPDS';
Unit = 'dB';
ColormapIndex = 2;
DataType = cl_image.indDataType('Reflectivity');
GeometryType = cl_image.indGeometryType('PingSamples');
InitialFileFormat = 'RESONs7k';
y = 1:N;
x = (1:NbSamples) - (NbSamples/2) - 0.5;
x = centrage_magnetique(x);

[flag, IdentSonar] = DeviceIdentifier2IdentSonar(cl_reson_s7k.empty(), identSondeur);
iMode = 1;
SonarDescription = cl_sounder('Sonar.Ident', IdentSonar, 'Sonar.Mode_1', iMode);

[~, nomFicSeul] = fileparts(nomFic);

% OPERATION DE MAINTENANCE A FAIRE ICI : UTILISER LES set_Xxxx
if isfield(Data, 'Amplitude') && ~isempty(Data.Amplitude)
    % +1 ajout� mission BICOSE pour export colonne d'eau dans 3DViewer
    b = cl_image('Image', Data.Amplitude+1, 'Name', [num2str(identSondeur) '_' nomFicSeul ' - ' ImageName{1}], 'Unit', Unit, ...
    'DataType', DataType, 'ColormapIndex', ColormapIndex, ...
    'TagSynchroX', [num2str(identSondeur) '_' nomFic ' - PingSamples'], 'TagSynchroY', nomFic,...
    'x', x, 'XUnit', 'num', 'y', y, 'YUnit', 'Pings', ...
    'InitialFileName',   nomFic, ...
    'InitialFileFormat', InitialFileFormat, ...
    'GeometryType',   GeometryType,...
    'SonarDescription',   SonarDescription);
    b.Writable = false;
else
    b = [];
    return
end


% if flag7057
if isfield(Data, 'RxBeamIndex') && ~isempty(Data.RxBeamIndex)
    % ImageName{2} = 'SidescanBeams';
    ImageName{2} = 'Sidescan';
    Unit = 'num';
    ColormapIndex = 3;
    DataType = cl_image.indDataType('RxBeamIndex'); % 'ReceptionBeam'
    GeometryType = cl_image.indGeometryType('PingSamples');    % SonarD

    b(2) = cl_image('Image', Data.RxBeamIndex, 'Name', [nomFicSeul ' - ' ImageName{2}], 'Unit', Unit, ...
        'DataType', DataType, 'ColormapIndex', ColormapIndex, ...
        'TagSynchroX', [nomFic ' - PingSamples'], 'TagSynchroY', nomFic,...
        'x', x, 'XUnit', 'm', 'y', y, 'YUnit', 'Pings', ...
        'InitialFileName',   nomFic, ...
        'InitialFileFormat', InitialFileFormat, ...
        'GeometryType',   GeometryType,...
        'SonarDescription',   SonarDescription);
end

[subl_Depth, subl_Sidescan] = intersect3(DataDepth.PingCounter, Data.PingCounter, Data.PingCounter);
% subl_Depth = 1:length(y);
T = Data.Time.timeMat;
Data.Time = cl_time('timeMat', T(subl_Sidescan));

nbFais = DataDepth.NbBeams(1);
if mod(nbFais,2)
    ifaisMilieu = ceil(nbFais / 2);
    Height = DataDepth.Depth(subl_Depth,ifaisMilieu);
else
    ifaisMilieu = [floor(nbFais / 2) ceil(nbFais / 2)];
    Height = mean(DataDepth.Depth(subl_Depth,ifaisMilieu),2);
end

fe = Data.SampleRate(subl_Sidescan);
Mode = ones(1,length(subl_Depth));
% resol = get(SonarDescription, 'Proc.RangeResol');
for k=1:length(b)
%     fe = DataDepth.SampleRate(subl_Depth);
    DeltaD = 1500 ./ (2 * fe);
    resol = median(DeltaD);
    
    b(k) = set(b(k), 'SonarRawDataResol', resol, 'SonarResolutionD', resol, 'SonarResolutionX', resol);

    % ---------------------
    % Transfert des signaux
%     b(k) = set(b(k), 'SonarHeight', (DataDepth.VehicleDepth(subl_Depth) * resol));  % Calimero
    
%     X = DeltaD(subl_Depth);
    X = DeltaD; % Modifi� mission BICOSE
    
    b(k) = set(b(k), 'SonarTime', Data.Time);
    b(k) = set(b(k), 'SonarHeading', (DataDepth.Heading(subl_Depth)));
    
    if (identSondeur == 7125) && isfield(DataDepth, 'VehicleDepth') % ROV
        %{
        nbl = length(subl_Depth);
        Depth = NaN(nbl, 1, 'single');
        [~, IndexMin] = min(DataDepth.AcrossDist(subl_Depth,:), [], 2);
        for k2=1:nbl
            Depth(k2) = DataDepth.Depth(subl_Depth(k2),IndexMin(k2));
        end
        %}
        
        % TODO : tout ce qui suit est compl�tement faux
        RangeMinMetres = min(DataDepth.Range, [], 2);
        VD = -abs(DataDepth.VehicleDepth(subl_Depth));
        b(k) = set(b(k), 'SonarImmersion', VD(:));
        b(k) = set(b(k), 'SonarHeight', -RangeMinMetres(subl_Depth) ./ X(:));
    else
        b(k) = set(b(k), 'SonarImmersion', -DataDepth.TransducerDepth(subl_Depth));
        b(k) = set(b(k), 'SonarHeight', -abs(Height(:) ./ X(:)));
    end
    %         b(k) = set(b(k), 'SonarSurfaceSoundSpeed',  flipud(SurfaceSoundSpeed'));
    b(k) = set(b(k), 'SonarPortMode_1', Mode);

    b(k) = set(b(k), 'SampleFrequency', fe);
    b(k) = set(b(k), 'SonarFrequency', DataDepth.Frequency(subl_Depth));

    b(k) = set(b(k), 'SonarRawDataResol', resol, 'SonarResolutionD', resol);

    b(k) = set(b(k), 'SonarRoll',  DataDepth.Roll(subl_Depth));
    b(k) = set(b(k), 'SonarPitch', DataDepth.Pitch(subl_Depth));
    %         %     b(k) = set(b(k), 'SonarYaw',   flipud(Yaw);
    %         %     b(k) = set(b(k), 'SonarYaw',  flipud(Heave);    % BIDOUILLE
    b(k) = set(b(k), 'SonarHeave',   DataDepth.Heave(subl_Depth));
    %         b(k) = set(b(k), 'SonarTide',            zeros(1,length(subl_Image)));
    b(k) = set(b(k), 'SonarPingCounter',     Data.PingCounter(subl_Sidescan));

    b(k) = set(b(k), 'SonarFishLongitude', DataDepth.Longitude(subl_Depth));
    b(k) = set(b(k), 'SonarFishLatitude',  DataDepth.Latitude(subl_Depth));

    % --------------------------------------------
    % Transfert des parametres d'etat de la donnee

    %         EtatSonar.SonarTVG_ConstructAlpha = mean(SonarTVG_ConstructAlpha);
    %         EtatSonar.SonarTVG_IfremerAlpha   = EtatSonar.SonarTVG_ConstructAlpha;
    %         b(k) = set(b(k),  'Sonar_DefinitionENCours', ...
    %             'SonarTVG_ConstructTypeCompens',        EtatSonar.SonarTVG_ConstructTypeCompens, ...
    %             'SonarTVG_ConstructTable',              EtatSonar.SonarTVG_ConstructTable, ...
    %             'SonarTVG_ConstructAlpha',              EtatSonar.SonarTVG_ConstructAlpha, ...
    %             'SonarTVG_ConstructConstante',          EtatSonar.SonarTVG_ConstructConstante, ...
    %             'SonarTVG_ConstructCoefDiverg',         EtatSonar.SonarTVG_ConstructCoefDiverg, ...
    %             'SonarTVG_IfremerAlpha',                EtatSonar.SonarTVG_IfremerAlpha, ...
    %             'SonarAireInso_etat',                   EtatSonar.SonarAireInso_etat, ...
    %             'SonarDiagEmi_etat',                    EtatSonar.SonarDiagEmi_etat, ...
    %             'SonarDiagEmi_origine',                 EtatSonar.SonarDiagEmi_origine, ...
    %             'SonarDiagEmi_ConstructTypeCompens',    EtatSonar.SonarDiagEmi_ConstructTypeCompens, ...
    %             'SonarDiagRec_etat',                    EtatSonar.SonarDiagRec_etat, ...
    %             'SonarDiagRec_origine',                 EtatSonar.SonarDiagRec_origine, ...
    %             'SonarDiagRec_ConstructTypeCompens',    EtatSonar.SonarDiagRec_ConstructTypeCompens, ...
    %             'Sonar_NE_etat',                        EtatSonar.SonarNE_etat, ...
    %             'Sonar_SH_etat',                        EtatSonar.SonarSH_etat, ...
    %             'Sonar_GT_etat',                        EtatSonar.SonarGT_etat, ...
    %             'SonarBS_etat',                         EtatSonar.SonarBS_etat, ...
    %             'SonarBS_origine',                      EtatSonar.SonarBS_origine, ...
    %             'SonarBS_origineBelleImage',            EtatSonar.SonarBS_origineBelleImage, ...
    %             'SonarBS_origineFullCompens',           EtatSonar.SonarBS_origineFullCompens, ...
    %             'SonarBS_IfremerCompensTable',          EtatSonar.SonarBS_IfremerCompensTable);

    %% D�finition d'une carto

    if isempty(Carto)
%         Carto = getDefinitionCarto(b(k), 'nomDirSave', nomDirMat, 'NoGeodetic', 1);
        Carto = getDefinitionCarto(b(k), 'nomDirSave', nomDirMat, 'NoGeodetic', 0);
    end
    b(k) = set(b(k), 'Carto', Carto);

    %% D�finition du nom de l'image

    b(k) = set(b(k), 'Name', [nomFicSeul ' - ' ImageName{k}]);   % On reimpose le Titre car il a ete modifie par b(k) = set(b(k),  'S...
    b(k) = update_Name(b(k));

    %% On impose un rehaussement de contraste � 0.5% pour pouvoir
    % cont�ler l'image avec ErViewer

    StatValues = b(k).StatValues;
    if ~isempty(StatValues)
        b(k).CLim = StatValues.Quant_25_75;
    end


    %     [nomDir, nomFicOut] = fileparts(this.nomFic);
    %     nomFicOut = fullfile(nomDir, 'SonarScope', nomFicOut)
    %     status = export_ermapper(b(k), [nomFicOut '_' ImageName{k}]);
    switch k
        case 1
            export_ermapper(b(k), nomFicSidescan.ReflectivityErs, 'IEEE4ByteReal', NaN);
        case 2
            export_ermapper(b(k), nomFicSidescan.RxBeamIndexErs, 'IEEE4ByteReal', NaN);
    end
end

% SonarScope(b)
