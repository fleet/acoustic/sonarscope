% Transformation en chaine de caracteres d'une instance
%
% Syntax
%   str = char_instance(a)
%
% Input Arguments
%   a : instance de cl_reson_s7k
%
% Name-Value Pair Arguments
%   InfoSonar  : 1=Information de l'image uniquement
%                2=Info de l'information sonar uniquement
%                3=Display de toute l'information disponible (image+sonar) : (3 par defaut)
%
% Output Arguments
%   str : une chaine de caracteres representative de l instance
%
% Examples
%
% See also cl_reson_s7k Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function str = char_instance(this, varargin)

str = [];

if isempty(this)
    return
end

str{end+1} = sprintf('nomFic      <-> %s', this.nomFic);
str{end+1} = sprintf('nomFicIndex <-- %s', this.nomFicIndex);
str{end+1} = sprintf('SounderName <-- %d', this.SounderName);
if ~isempty(this.nomFic)
    identSondeur = selectionSondeur(this);
    strHisto = histo_index(this, identSondeur, 'minimal', 1);
    strHisto = strsplit(strHisto, newline);
    for i=1: length(strHisto)
        str{end+1} = sprintf('                %s', strHisto{i}); %#ok
    end
end

% ---------------------------
% Concatenation en une chaine

str = cell2str(str);
