%% Lecture de l'heure

function [flag, t] = fread_7KTIME(fid)

flag = 0;
t    = [];

Year = fread(fid, 1, 'uint16');
if feof(fid)
    return
end

Day = fread(fid, 1, 'uint16');
if feof(fid)
    return
end

Seconds = fread(fid, 1, 'single');
if feof(fid)
    return
end

Hours = fread(fid, 1, 'uint8');
if feof(fid)
    return
end

Minutes = fread(fid, 1, 'uint8');
if feof(fid)
    return
end

date  = dayJma2Ifr(1, 1, Year) + Day - 1;
heure = 1000 * (Seconds + Minutes * 60 + Hours * 3600);
t = cl_time('timeIfr', date, heure);

flag = 1;
