% Description
%   Read Data Record Frame (structure at the begibbing of every S7K datagram)
%
% Syntax
%   [flag, DRF] = fread_DRF(fid)
%
% Input Arguments
%   fid : fid of the .s7k file
%
% Output Arguments
%   flag : 0=KO, 1=OK
%   DRF  : Structure (see example)
%
% Examples
%   [flag, DRF] = fread_DRF(fid);
% %    DRF :
% %              ProtocolVersion: 5
% %                       Offset: 60
% %                  SyncPattern: 65535
% %                         Size: 456
% %           OptionalDataOffset: 0
% %       OptionalDataIdentifier: 0
% %                         Date: 2459166
% %                         Time: 8.108382265853882e+07
% %                    Reserved1: 1
% %         RecordTypeIdentifier: 7200
% %             DeviceIdentifier: 13003
% %             SystemEnumerator: 0
% %                        Flags: 32768
% %     TotalRecordsInFragmented: 1
% %             FragmentedNumber: 0
%
% Authors : JMA 
% See also cre_ficIndex 
%
% Reference pages in Help browser
%   <a href="matlab:doc fread_DRF">fread_DRF</a>
%-------------------------------------------------------------------------------

function [flag, DRF] = fread_DRF(fid)

flag = 0;

%{
    position = ftell(fid);
    fseek(fid, position, 'bof');
%}
Buffer = fread(fid, 64,  'uint8=>uint8');
if feof(fid)
    DRF = [];
    return
end
id = 0;

X = typecast(Buffer(id + (1:(2*2))), 'uint16');
id = id + 2*2;

DRF.ProtocolVersion = X(1);
DRF.Offset          = X(2);

X = typecast(Buffer(id + (1:(4*4))), 'uint32');
id = id + 4*4;

DRF.SyncPattern            = X(1);
DRF.Size                   = double(X(2));
DRF.OptionalDataOffset     = X(3);
DRF.OptionalDataIdentifier = X(4);

X = typecast(Buffer(id + (1:(2*2))), 'uint16');
id = id + 2*2;

Year = X(1);
Day  = X(2);

Seconds = typecast(Buffer(id + (1:4)), 'single');
id = id + 4;

Hours = Buffer(id + 1);
id = id + 1;

Minutes = Buffer(id + 1);
id = id + 1;

% date  = dayJma2Ifr(1, 1, double(Year)) + double(Day) - 1;
date  = dayYear2Ifr(Day, Year);
heure = 1000 * (double(Seconds) + double(Minutes) * 60 + double(Hours) * 3600);
% DRF.Tag7KTIME = cl_time('timeIfr', date, heure); % Comment� par JMA le
% 28/03/2016 car �a prend beaucoup de temps
DRF.Date = date;
DRF.Time = heure;
% DRF.Tag7KTIME = []; % Pour l'instant, histoire de ne pas obliger les utilisateurs � tout recommencer

DRF.Reserved1= typecast(Buffer(id + (1:2)), 'uint16');
id = id + 2;

X = typecast(Buffer(id + (1:4*2)), 'uint32');
id = id + 4*2;

DRF.RecordTypeIdentifier = X(1);
DRF.DeviceIdentifier     = X(2);

% X = typecast(Buffer(id + (1:2*2)), 'uint16');
% id = id + 2*2;
% 
% DRF.Reserved2        = X(1);
% DRF.SystemEnumerator = X(2);
% 
% DRF.Reserved3 = typecast(Buffer(id + (1:4)), 'uint32');
% id = id + 4;
% 
% X = typecast(Buffer(id + (1:2*2)), 'uint16');
% id = id + 2*2;

X = typecast(Buffer(id + (1:2*6)), 'uint16');
id = id + 2*6;

% DRF.Reserved2        = X(1);
DRF.SystemEnumerator = X(2);
DRF.Flags            = X(5);
% DRF.Reserved4        = X(6);

X = typecast(Buffer(id + (1:(4*3))), 'uint32');
% id = id + 4*3;

% DRF.Reserved5                = X(1);
DRF.TotalRecordsInFragmented = X(2);
DRF.FragmentedNumber         = X(3);

flag = 1;