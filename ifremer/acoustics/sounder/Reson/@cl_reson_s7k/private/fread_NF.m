% -------------------------------------------------------------------------
% Lecture du Network Frame

function [flag, DRF] = fread_NF(fid)

flag = 0;

DRF.Version = fread(fid, 1, 'uint16');
if feof(fid)
    return
end

DRF.Offset = fread(fid, 1, 'uint16');
if feof(fid)
    return
end

DRF.TotalPackets = fread(fid, 1, 'uint32');
if feof(fid)
    return
end

DRF.TotalRecords = fread(fid, 1, 'uint16');
if feof(fid)
    return
end

DRF.TransmissionIdentifier = fread(fid, 1, 'uint16');
if feof(fid)
    return
end

DRF.PacketSize = fread(fid, 1, 'uint32');
if feof(fid)
    return
end

DRF.TotalSize = fread(fid, 1, 'uint32');
if feof(fid)
    return
end

DRF.SequenceNumber = fread(fid, 1, 'uint32');
if feof(fid)
    return
end

DRF.DestinationDeviceIdentifier = fread(fid, 1, 'uint32');
if feof(fid)
    return
end

DRF.DestinationEnumerator = fread(fid, 1, 'uint16');
if feof(fid)
    return
end

DRF.SourceDeviceIdentifier = fread(fid, 1, 'uint32');
if feof(fid)
    return
end

DRF.RecordType = fread(fid, 1, 'uint16');
if feof(fid)
    return
end


flag = 1;

