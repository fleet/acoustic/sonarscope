% Reference Point

function [flag, Data] = fread_RecordType_1000(fid)

my_breakpoint % Jamais rencontré

flag = 0;
Data = [];

%% Read Record Type Header

X = fread(fid, 4, 'single');
if feof(fid)
    return
end

Data.RTH.VehicleXReferencePoint2COG = X(1);
Data.RTH.VehicleYReferencePoint2COG = X(2);
Data.RTH.VehicleZReferencePoint2COG = X(3);
Data.RTH.WaterLevel2COG             = X(4);

%% Read Record Data

Data.RD = [];

%% Read Optional Data

Data.OD = [];

flag = 1;
