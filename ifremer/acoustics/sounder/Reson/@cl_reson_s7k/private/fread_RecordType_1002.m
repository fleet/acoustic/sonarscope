% Calibrated Sensor Offset Position

function [flag, Data] = fread_RecordType_1002(fid)

my_breakpoint % Jamais rencontré

flag = 0;
Data = [];

%% Read Record Type Header

X= fread(fid, 6, 'single');
if feof(fid)
    return
end

Data.RTH.SensorPositionXOffset  = X(1);
Data.RTH.SensorPositionYOffset  = X(2);
Data.RTH.SensorPositionZOffset  = X(3);
Data.RTH.SensorRollAngleOffset  = X(4);
Data.RTH.SensorPitchAngleOffset = X(5);
Data.RTH.SensorYawAngleOffset   = X(6);

%% Read Record Data

Data.RD = [];

%% Read Optional Data

Data.OD = [];

flag = 1;
