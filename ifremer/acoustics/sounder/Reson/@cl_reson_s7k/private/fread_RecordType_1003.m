% Description
%   Reads S7K datagram 1003 (Position)
%
% Syntax
%   [flag, Data] = fread_RecordType_1003(fid)
%
% Input Arguments
%   fid : fid of the S7K file
%
% Output Arguments
%   flag : 0=KO, 1=OK
%   Data : Structure (see example)
%
% Examples
%   [flag, Data] = fread_RecordType_1003(fid);
%   % Data = 
%   %     RTH: [1�1 struct]
%   %      RD: []
%   %      OD: []   
%   % Data.RTH
%   %                 DatumIdentifier: 0
%   %                         Latency: 0
%   %    HeightRelative2DatumOrHeight: -20.596307642421085
%   %                PositionTypeFlag: 0
%   %                        Latitude: 45.624662726574392
%   %                       Longitude: -1.226843864856316e+02
%   %                         UTMZone: 0
%   %                     QualityFlag: 0
%   %               PositioningMethod: 15
%
% Authors : JMA 
% See also cre_ficIndex, fread_DRF 
%
% Reference pages in Help browser
%   <a href="matlab:doc fread_RecordType_1003">fread_RecordType_1003</a>
%-------------------------------------------------------------------------------

function [flag, Data] = fread_RecordType_1003(fid)

flag = 0;
Data = [];

%% Read Record Type Header

Data.RTH.DatumIdentifier = fread(fid, 1, 'uint32');
if feof(fid)
    return
end

Data.RTH.Latency = fread(fid, 1, 'single');
if feof(fid)
    return
end

X = fread(fid, 3, 'double');
if feof(fid)
    return
end

LatitudeOrNorthing = X(1);
LongitudeOrEasting = X(2);

Data.RTH.HeightRelative2DatumOrHeight = X(3);

X = fread(fid, 4, 'char');
if feof(fid)
    return
end

Data.RTH.PositionTypeFlag = X(1);

if Data.RTH.PositionTypeFlag == 0
    Data.RTH.Latitude  = LatitudeOrNorthing * (180/pi);
    Data.RTH.Longitude = LongitudeOrEasting * (180/pi);
else
    Data.RTH.Northing = LatitudeOrNorthing;
    Data.RTH.Easting  = LongitudeOrEasting;
end

Data.RTH.UTMZone           = X(2);
Data.RTH.QualityFlag       = X(3);
Data.RTH.PositioningMethod = X(4);

%% Read Record Data

Data.RD = [];

%% Lecture section OD

Data.OD = [];

flag = 1;
