% Custom Attitude Information

function [flag, Data] = fread_RecordType_1004(fid)

my_breakpoint % Jamais rencontr�

flag = 0;
Data = [];

%% Read Record Type Header

X = fread(fid, 2, 'char');
if feof(fid)
    return
end

Data.RTH.FieldMask = X(1);
Data.RTH.Reserved  = X(2);

Data.RTH.N = fread(fid, 1, 'uint16');
if feof(fid)
    return
end

Data.RTH.Frequency = fread(fid, 1, 'single');

%% Read Record Data

nameOfField = { 'PitchAngle'; 'RollAngle'; 'HeadingAngle'; 'Heave'
                'PitchRate'; 'RollRate'; 'HeadingRate'; 'HeaveRate'};
% D�finition de la structure des datas
for k=1:8
    if bitget(Data.RTH.FieldMask, k)
        fieldStruct(k).length = 1; %#ok<AGROW>
        fieldStruct(k).type   = 'single'; %#ok<AGROW>
        fieldStruct(k).name   = nameOfField(k); %#ok<AGROW>
    end
end

my_warndlg('ATTENTION : format de lecture des datas d''un FIELD � valider', 1);
for k=1:Data.RTH.N
    fieldStruct = readfields(fid, fieldStruct);
    if bitget(Data.RD.FieldMask, 1)
        Data.RD.PitchAngle(k) = fieldStruct.PitchAngle;
    end
    if bitget(Data.RD.FieldMask, 2)
        Data.RD.RollAngle(k) = fieldStruct.RollAngle;
    end
    if bitget(Data.RD.FieldMask, 3)
        Data.RD.HeadingAngle(k) = fieldStruct.HeadingAngle;
    end
    if bitget(Data.RD.FieldMask, 4)
        Data.RD.Heave(k) = fieldStruct.Heave;
    end
    if bitget(Data.RD.FieldMask, 5)
        Data.RD.PitchRate(k) = fieldStruct.PitchRate;
    end
    if bitget(Data.RD.FieldMask, 6)
        Data.RD.RollRate(k) = fieldStruct.RollRate;
    end
    if bitget(Data.RD.FieldMask, 7)
        Data.RD.HeadingRate(k) = fieldStruct.HeadingRate;
    end
    if bitget(Data.RD.FieldMask, 8)
        Data.RD.HeaveRate(k) = fieldStruct.HeaveRate;
    end
end

%% Read Optional Data

Data.OD = [];

flag = 1;