% Tide 

function [flag, Data] = fread_RecordType_1005(fid)

my_breakpoint % Jamais rencontré

flag = 0;
Data = [];

%% Read Record Type Header

Data.RTH.Tide = fread(fid, 1, 'single');
if feof(fid)
    return
end

Data.RTH.Source = fread(fid, 1, 'uint16');
if feof(fid)
    return
end

Data.RTH.Flags = fread(fid, 1, 'char');
if feof(fid)
    return
end

if bitget(Data.RTH.Flags, 1) == 0
    Data.RTH.GaugeIdentifier = fread(fid, 1, 'uint16');
    if feof(fid)
        return
    end
end
if bitget(Data.RTH.Flags, 2) == 0
    Data.RTH.Latency = fread(fid, 1, 'single');
    if feof(fid)
        return
    end

    X = fread(fid, 3, 'double');
    if feof(fid)
        return
    end
    
    LatitudeOrNorthing           = X(1);
    LongitudeOrEasting           = X(2);
    HeightRelative2DatumOrHeight = X(3);

    X = fread(fid, 2, 'char');
    if feof(fid)
        return
    end
    
    Data.RTH.PositionTypeFlag = X(1);

    if Data.RTH.PositionTypeFlag == 0
        Data.RTH.Latitude             = LatitudeOrNorthing;
        Data.RTH.Longitude            = LongitudeOrEasting;
        Data.RTH.HeightRelative2Datum = HeightRelative2DatumOrHeight;
    else
        Data.RTH.Northing = LatitudeOrNorthing;
        Data.RTH.Easting  = LongitudeOrEasting;
        Data.RTH.Height   = HeightRelative2DatumOrHeight;
    end

    Data.RTH.UTMZone = X(2);
end

%% Read Record Data

Data.RD = [];

%% Read Optional Data

Data.OD = [];

flag = 1;