% Description
%   Reads S7K fread_RecordType_1006 7001 (Altitude)
%
% Syntax
%   [flag, Data] = fread_RecordType_7001(fid)
%
% Input Arguments
%   fid : fid of the S7K file
%
% Output Arguments
%   flag : 0=KO, 1=OK
%   Data : Structure (see example)
%
% Examples
%   [flag, Data] = fread_RecordType_7001(fid);
%   % Data = 
%   %     RTH: [1�1 struct]
%   %      RD: []
%   %      OD: []   
%   % Data.RTH
%   %    Distance: 1.036708526611328e+02
%
% Authors : JMA 
% See also cre_ficIndex, fread_DRF 
%
% Reference pages in Help browser
%   <a href="matlab:doc fread_RecordType_7001">fread_RecordType_7001</a>
%-------------------------------------------------------------------------------

function [flag, Data] = fread_RecordType_1006(fid)

%% Read Record Type Header

Data.RTH.Distance = fread(fid, 1, 'single');

%% Read Record Data

Data.RD = [];

%% Read Optional Data
Data.OD = [];

flag = 1;
