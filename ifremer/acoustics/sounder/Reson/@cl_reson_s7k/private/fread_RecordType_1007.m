% Motion Over Ground

function [flag, Data] = fread_RecordType_1007(fid)

my_breakpoint % Jamais rencontr�

flag = 0;
Data = [];

%% Read Record Type Header

X = fread(fid, 2, 'char');
if feof(fid)
    return
end

Data.RTH.FieldMask = X(1);
Data.RTH.Reserved  = X(2);

Data.RTH.N = fread(fid, 1, 'uint16');
if feof(fid)
    return
end

Data.RTH.Frequency = fread(fid, 1, 'single');

%% Read Record Data

nameOfField = { 'XSpeed' 'YSpeed' 'ZSpeed' ...
                'XAcceleration' 'YAcceleration' 'ZAcceleration'};
% D�finition de la structure des datas
if bitget(Data.RTH.FieldMask, 1)
    for k=1:3
        fieldStruct(k).length = 1; %#ok<AGROW>
        fieldStruct(k).type   = 'single'; %#ok<AGROW>
        fieldStruct(k).name   = nameOfField(k); %#ok<AGROW>
    end
end
if bitget(Data.RTH.FieldMask, 2)
    for k=1:3
        fieldStruct(k).length = 1;
        fieldStruct(k).type   = 'single';
        fieldStruct(k).name   = nameOfField(k*2);
    end
end

my_warndlg('ATTENTION : format de lecture des datas d''un FIELD � valider', 1);
for k=1:Data.RTH.N
    fieldStruct = readfields(fid, fieldStruct);
    if bitget(Data.RD.FieldMask, 1)
        Data.RD.XSpeed(k) = fieldStruct.XSpeed;
        Data.RD.YSpeed(k) = fieldStruct.YSpeed;
        Data.RD.ZSpeed(k) = fieldStruct.ZSpeed;
    end
    if bitget(Data.RD.FieldMask, 2)
        Data.RD.XAcceleration(k) = fieldStruct.XAcceleration;
        Data.RD.YAcceleration(k) = fieldStruct.YAcceleration;
        Data.RD.ZAcceleration(k) = fieldStruct.ZAcceleration;
    end
end

%% Read Optional Data

Data.OD = [];

flag = 1;