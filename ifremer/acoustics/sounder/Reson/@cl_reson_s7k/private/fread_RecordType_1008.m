% Description
%   Reads S7K datagram 1008 (Depth)
%
% Syntax
%   [flag, Data] = fread_RecordType_1008(fid)
%
% Input Arguments
%   fid : fid of the S7K file
%
% Output Arguments
%   flag : 0=KO, 1=OK
%   Data : Structure (see example)
%
% Examples
%   [flag, Data] = fread_RecordType_1008(fid);
%   % Data = 
%   %     RTH: [1�1 struct]
%   %      RD: []
%   %      OD: []   
%   % Data.RTH
%   %    DepthDescriptor: 1
%   %     Correctionflag: 1
%   %           Reserved: 0
%   %              Depth: -0.850000023841858
%
% Authors : JMA 
% See also cre_ficIndex, fread_DRF 
%
% Reference pages in Help browser
%   <a href="matlab:doc fread_RecordType_1008">fread_RecordType_1008</a>
%-------------------------------------------------------------------------------

% TODO : optimiser la lecture avec typecast acr cette fonctions est
% gourmande en temps cpu

function [flag, Data] = fread_RecordType_1008(fid)

flag = 0;
Data = [];

%% Read Record Type Header

X = fread(fid, 2, 'char');
if feof(fid)
    return
end

Data.RTH.DepthDescriptor = X(1);
Data.RTH.Correctionflag  = X(2);

% Lecture de l'ent�te de la FRAME.
Data.RTH.Reserved = fread(fid, 1, 'uint16');
if feof(fid)
    return
end

% Lecture de l'ent�te de la FRAME.
Data.RTH.Depth = fread(fid, 1, 'single');
if feof(fid)
    return
end

%% Read Record Data

Data.RD = [];

%% Read Optional Data

Data.OD = [];

flag = 1;
