% Sound Velocity Profile

function [flag, Data] = fread_RecordType_1009(fid)

my_breakpoint % Jamais rencontré

flag = 0;
Data = [];

%% Read Record Type Header

X = fread(fid, 2, 'char');
if feof(fid)
    return
end

Data.RTH.PositionFlag = X(1);
% Data.RTH.Reserved     = X(2);

Data.RTH.Reserved = fread(fid, 1, 'uint16');
if feof(fid)
    return
end

X = fread(fid, 2, 'double');
if feof(fid)
    return
end

Data.RTH.Latitude  = X(1);
Data.RTH.Longitude = X(2);

Data.RTH.N = fread(fid, 1, 'uint32');
if feof(fid)
    return
end

%% Read Record Data

X =  fread(fid, [2 Data.RTH.N], 'single');
if feof(fid)
    flag = 0;
    return
end

Data.RD.Depth = X(1,:)';
Data.RD.SoundVelocity = X(2,:)';

% %Lecture des Data de la FRAME.
% for i=1:Data.RTH.N
%     Data.RD.Depth(i) = fread(fid, 1, 'single');
%     if feof(fid)
%         flag = 0;
%         return
%     end
%     Data.RD.SoundVelocity(i) = fread(fid, 1, 'single');
%     if feof(fid)
%         flag = 0;
%         return
%     end
% end

%% Read Optional Data

Data.OD = [];

flag = 1;
