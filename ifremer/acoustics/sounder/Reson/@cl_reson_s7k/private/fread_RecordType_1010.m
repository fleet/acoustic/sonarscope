% Description
%   Reads S7K datagram 1010 (CTD)
%
% Syntax
%   [flag, Data] = fread_RecordType_1010(fid)
%
% Input Arguments
%   fid : fid of the S7K file
%
% Output Arguments
%   flag : 0=KO, 1=OK
%   Data : Structure (see example)
%
% Examples
%   [flag, Data] = fread_RecordType_1010(fid);
%   % Data = 
%   %     RTH: [1�1 struct]
%   %      RD: [1�1 struct]
%   %      OD: []   
%   % Data.RTH
%   %                  Frequency: 12500
%   %    SoundVelocitySourceFlag: 1
%   %     SoundVelocityAlgorithm: 0
%   %           ConductivityFlag: 1
%   %               PressureFlag: 1
%   %               PositionFlag: 0
%   %      SampleContentValidity: 31
%   %                   Reserved: 0
%   %                   Latitude: 0
%   %                  Longitude: 0
%   %                 SampleRate: 0
%   %                          N: 3730
%   % Data.RD
%   %        Conductivity: [1�3730 single]
%   %            Salinity: [1�3730 single]
%   %    WaterTemperature: [1�3730 single]
%   %            Pressure: [1�3730 single]
%   %               Depth: [1�3730 single]
%   %       SoundVelocity: [1�3730 single]
%   %          Absorption: [1�3730 single]
%
% Authors : JMA 
% See also cre_ficIndex, fread_DRF 
%
% Reference pages in Help browser
%   <a href="matlab:doc fread_RecordType_1010">fread_RecordType_1010</a>
%-------------------------------------------------------------------------------

function [flag, Data] = fread_RecordType_1010(fid)

flag = 0;
Data = [];

rad2pi = 180/pi;

%% Read Record Type Header

Data.RTH.Frequency = fread(fid, 1, 'single');
if feof(fid)
    return
end

X = fread(fid, 6, 'uint8');
if feof(fid)
    return
end
Data.RTH.SoundVelocitySourceFlag = X(1);
Data.RTH.SoundVelocityAlgorithm  = X(2);
Data.RTH.ConductivityFlag        = X(3);
Data.RTH.PressureFlag            = X(4);
Data.RTH.PositionFlag            = X(5);
Data.RTH.SampleContentValidity   = X(6);

Data.RTH.Reserved = fread(fid, 1, 'uint16');
if feof(fid)
    return
end

X = fread(fid, 2, 'double');

if feof(fid)
    return
end
Data.RTH.Latitude  = X(1) * rad2pi;
Data.RTH.Longitude = X(2) * rad2pi;

Data.RTH.SampleRate = fread(fid, 1, 'single');
if feof(fid)
    return
end

Data.RTH.N = fread(fid, 1, 'uint32');
if feof(fid)
    return
end
if Data.RTH.N > 1e6
    my_warndlg('fread_RecordType_1010 RTH.N > 1e6, surely wrong', 0);
    return
end

%% Read Record Data

% if Data.RTH.ConductivityFlag == 0
    Data.RD.Conductivity = NaN(1,Data.RTH.N, 'single');
% else
    Data.RD.Salinity = NaN(1,Data.RTH.N, 'single');
% end

Data.RD.WaterTemperature = NaN(1,Data.RTH.N, 'single');

% if Data.RTH.PressureFlag == 0
    Data.RD.Pressure = NaN(1,Data.RTH.N, 'single');
% else
    Data.RD.Depth = NaN(1,Data.RTH.N, 'single');
% end
Data.RD.SoundVelocity = NaN(1,Data.RTH.N, 'single');
Data.RD.Absorption = NaN(1,Data.RTH.N, 'single');
    
% A repenser en une seule lecture
for k=1:Data.RTH.N
    if bitget(Data.RTH.SampleContentValidity, 1) == 1
        if Data.RTH.ConductivityFlag == 0
            Data.RD.Conductivity(k) = fread(fid, 1, 'single');
        else
            Data.RD.Salinity(k) = fread(fid, 1, 'single');
        end
    end
    if feof(fid)
        return
    end

    if bitget(Data.RTH.SampleContentValidity, 2) == 1
        Data.RD.WaterTemperature(k) = fread(fid, 1, 'single');
    end
    if feof(fid)
        return
    end

    if bitget(Data.RTH.SampleContentValidity, 3) == 1
        if Data.RTH.PressureFlag == 0
            Data.RD.Pressure(k) = fread(fid, 1, 'single');
        else
            Data.RD.Depth(k) = fread(fid, 1, 'single');
        end
    end
    if feof(fid)
        return
    end

    if bitget(Data.RTH.SampleContentValidity, 4) == 1
        Data.RD.SoundVelocity(k) = fread(fid, 1, 'single');
    end
    if feof(fid)
        return
    end

    if bitget(Data.RTH.SampleContentValidity, 5) == 1
        Data.RD.Absorption(k) = fread(fid, 1, 'single');
    end
end
% figure; plot(Data.RD.SoundVelocity, -Data.RD.Depth); grid on

%% Read Optional Data

Data.OD = [];

flag = 1;