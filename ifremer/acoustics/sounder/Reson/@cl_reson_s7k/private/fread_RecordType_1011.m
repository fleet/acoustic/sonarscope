% Geodesy 

function [flag, Data] = fread_RecordType_1011(fid)

my_breakpoint % Jamais rencontré

flag = 0;
Data = [];

%% Read Record Type Header

Data.RTH.SpheroidName = fread(fid, 32, 'char');
if feof(fid)
    return
end

X = fread(fid, 2, 'double');
if feof(fid)
    return
end

Data.RTH.SemiMajorAxis     = X(1);
Data.RTH.InverseFlattening = X(2);

X = fread(fid, 48, 'char');
if feof(fid)
    return
end

Data.RTH.Reserved  = X(1:16);
Data.RTH.DatumName = X(17:48);

Data.RTH.DataCalculationMethod = fread(fid, 1, 'uint32');
if feof(fid)
    return
end

Data.RTH.NumberOfParameters = fread(fid, 1, 'char');
if feof(fid)
    return
end

X = fread(fid, 7, 'double');
if feof(fid)
    return
end

Data.RTH.DX    = X(1);
Data.RTH.DY    = X(2);
Data.RTH.DZ    = X(3);
Data.RTH.RX    = X(4);
Data.RTH.RY    = X(5);
Data.RTH.RZ    = X(6);
Data.RTH.Scale = X(7);

X = fread(fid, 69, 'char');
if feof(fid)
    return
end

Data.RTH.Reserved2         = X(1:35);
Data.RTH.GridName          = X(36:67);
Data.RTH.GridDistanceUnits = X(68);
Data.RTH.GridAngularUnits  = X(69);

X = fread(fid, 6, 'double');
if feof(fid)
    return
end

Data.RTH.LatitudeOfOrigin   = X(1);
Data.RTH.CentralMeridian    = X(2);
Data.RTH.FalseEasting       = X(3);
Data.RTH.FalseNorthing      = X(4);
Data.RTH.CentralScaleFactor = X(5);
Data.RTH.CustomIdentifier   = X(6);

Data.RTH.Reserved3 = fread(fid, 50, 'char');

%% Read Record Data

Data.RD = [];

%% Read Optional Data

Data.OD = [];

flag = 1;