% Description
%   Reads S7K datagram 7012 (Roll Pitch Heave)
%
% Syntax
%   [flag, Data] = fread_RecordType_1012(fid)
%
% Input Arguments
%   fid : fid of the S7K file
%
% Output Arguments
%   flag : 0=KO, 1=OK
%   Data : Structure (see example)
%
% Examples
%   [flag, Data] = fread_RecordType_1012(fid);
%   % Data = 
%   %     RTH: [1�1 struct]
%   %      RD: []
%   %      OD: []   
%   % Data.RTH
%   %      Roll: -0.009446403943002
%   %     Pitch: -0.001758820144460
%   %     Heave: -0.020277990028262
%
% Authors : JMA 
% See also cre_ficIndex, fread_DRF 
%
% Reference pages in Help browser
%   <a href="matlab:doc fread_RecordType_1012">fread_RecordType_1012</a>
%-------------------------------------------------------------------------------

function [flag, Data] = fread_RecordType_1012(fid)

flag = 0;
Data = [];

X = fread(fid, 3, 'single');
if feof(fid)
    return
end

%% Read Record Type Header

Data.RTH.Roll  = X(1);
Data.RTH.Pitch = X(2);
Data.RTH.Heave = X(3);

%% Read Recod Data

Data.RD = [];

%% Read Optional Data

Data.OD = [];

flag = 1;