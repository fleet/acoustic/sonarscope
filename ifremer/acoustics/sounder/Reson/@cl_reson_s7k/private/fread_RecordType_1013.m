% Description
%   Reads S7K datagram 7013 (Heading)
%
% Syntax
%   [flag, Data] = fread_RecordType_1013(fid)
%
% Input Arguments
%   fid : fid of the S7K file
%
% Output Arguments
%   flag : 0=KO, 1=OK
%   Data : Structure (see example)
%
% Examples
%   [flag, Data] = fread_RecordType_1013(fid);
%   % Data = 
%   %     RTH: [1�1 struct]
%   %      RD: []
%   %      OD: []   
%   % Data.RTH
%   %     Heading: 5.378101825714111
%
% Authors : JMA 
% See also cre_ficIndex, fread_DRF 
%
% Reference pages in Help browser
%   <a href="matlab:doc fread_RecordType_1013">fread_RecordType_1013</a>
%-------------------------------------------------------------------------------

function [flag, Data] = fread_RecordType_1013(fid)

%% Read Record Type Header

Data.RTH.Heading = fread(fid, 1, 'single');

%% Read Recod Data

Data.RD = [];

%% Read Optional Data

Data.OD = [];

flag = 1;
