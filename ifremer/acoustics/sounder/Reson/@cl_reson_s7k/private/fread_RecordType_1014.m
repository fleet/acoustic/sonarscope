% Survey Line

function [flag, Data] = fread_RecordType_1014(fid)

my_breakpoint % Jamais rencontr�

flag = 0;
Data = [];

%% Read Record Type Header

X = fread(fid, 2, 'uint16');
if feof(fid)
    return
end

Data.RTH.WayPointcount = X(1);
Data.RTH.PositionType  = X(2);

Data.RTH.Radius = fread(fid, 1, 'single');
if feof(fid)
    return
end

Data.RTH.LineName = fread(fid, 64, 'char');

%% Read Record Data

% TODO : on peut faire tout �a beaucoup plus simplement : attendre d'avoir
% un exemple pour v�rifier fread(fid, [2 Data.RTH.N] ou [Data.RTH.N 2], 'single');
X = fread(fid, 2*Data.RTH.N, 'single');
for k=1:Data.RTH.N
    LatitudeorNorthing = X(2*k-1);
    LongitudeOrEasting = X(2*k);
    if Data.RTH.PositionType == 0
        Data.RD.Latitude(k)  = LatitudeorNorthing;
        Data.RD.Longitude(k) = LongitudeOrEasting;
    else
        Data.RD.Northing(k) = LatitudeorNorthing;
        Data.RD.Easting(k)  = LongitudeOrEasting;
    end
end

%% Read Optional Data

Data.OD = [];

flag = 1;