% Description
%   Reads S7K datagram 1015 (Navigation)
%
% Syntax
%   [flag, Data] = fread_RecordType_1015(fid)
%
% Input Arguments
%   fid : fid of the S7K file
%
% Output Arguments
%   flag : 0=KO, 1=OK
%   Data : Structure (see example)
%
% Examples
%   [flag, Data] = fread_RecordType_1015(fid);
%   % Data = 
%   %     RTH: [1�1 struct]
%   %      RD: [1�1 struct]
%   %      OD: []   
%   % Data.RTH
%   %              VerticalReference: 1
%   %                       Latitude: 45.624654595259592
%   %                      Longitude: -1.226843720055795e+02
%   %     HorizontalPositionAccuracy: 0.020956683903933
%   %                   VesselHeight: -20.605602264404297
%   %                 HeightAccuracy: -0.017234155908227
%   %                SpeedOverGround: 1.795766592025757
%   %               CourseOverGround: 3.074213777116946e+02
%   %                        Heading: 3.079848136767073e+02
%
% Authors : JMA 
% See also cre_ficIndex, fread_DRF 
%
% Reference pages in Help browser
%   <a href="matlab:doc fread_RecordType_1015">fread_RecordType_1015</a>
%-------------------------------------------------------------------------------

% TODO : optimiser la lecture avec typecast car cette fonctions est
% gourmande en temps cpu (c'est pas s�r que l'on gagne quelque-chose avec
% les typecast)

function [flag, Data] = fread_RecordType_1015(fid)

flag = 0;
Data = [];

rad2deg = 180/pi;

%% Read Record Type Header

Data.RTH.VerticalReference = fread(fid, 1, 'char');
if feof(fid)
    return
end

X = fread(fid, 2, 'double') * rad2deg;
if feof(fid)
    return
end

Data.RTH.Latitude  = X(1);
Data.RTH.Longitude = X(2);

X = fread(fid, 6, 'single');
if feof(fid)
    return
end

Data.RTH.HorizontalPositionAccuracy	= X(1);
Data.RTH.VesselHeight               = X(2);
Data.RTH.HeightAccuracy             = X(3);
Data.RTH.SpeedOverGround            = X(4);
Data.RTH.CourseOverGround           = X(5) * rad2deg;
Data.RTH.Heading                    = X(6) * rad2deg;

%% Read Recod Data

Data.RD = [];

%% Read Optional Data

Data.OD = [];

flag = 1;
