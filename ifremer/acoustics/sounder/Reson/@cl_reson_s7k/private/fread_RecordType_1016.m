% Description
%   Reads S7K datagram 7016 (Attitude)
%
% Syntax
%   [flag, Data] = fread_RecordType_1016(fid)
%
% Input Arguments
%   fid : fid of the S7K file
%
% Output Arguments
%   flag : 0=KO, 1=OK
%   Data : Structure (see example)
%
% Examples
%   [flag, Data] = fread_RecordType_1016(fid);
%   % Data = 
%   %     RTH: [1�1 struct]
%   %      RD: []
%   %      OD: []   
%   % Data.RTH
%   %                  N: 1
%   %     TimeDifference: 0
%   %               Roll: -0.557667870258616
%   %              Pitch: -0.107192008328508
%   %              Heave: -0.020220523700118
%   %            Heading: 3.080761196455320e+02
%
% Authors : JMA 
% See also cre_ficIndex, fread_DRF 
%
% Reference pages in Help browser
%   <a href="matlab:doc fread_RecordType_1016">fread_RecordType_1016</a>
%-------------------------------------------------------------------------------

function [flag, Data] = fread_RecordType_1016(fid)

flag = 0;
Data = [];

% position = ftell(fid);
% fseek(fid, position, 'bof');

%% Read Record Type Header

Data.RTH.N = fread(fid, 1, 'uint8');
if feof(fid)
    return
end

if nargout == 1
    flag = 1;
    return
end

if Data.RTH.N == 0
    flag = -1;
    return
end

%{
Buffer = fread(fid, (2+4*4) * Data.RTH.N, 'uint8=>uint8');
if feof(fid)
    return
end
id = 0;

for k=1:Data.RTH.N
    Data.RTH.TimeDifference(k) = typecast(Buffer(id + (1:2)), 'uint16');
    id = id + 2;
    
    X = typecast(Buffer(id + (1:4*4)), 'single');
    id = id + 4*4;
    
    Data.RTH.Roll(k)    = X(1);
    Data.RTH.Pitch(k)   = X(2);
    Data.RTH.Heave(k)   = X(3);
    Data.RTH.Heading(k) = X(4);
end
%}  
  
% TODO : le temps d'execution est quasiment le m�me car Data.RTH.N est toujours �gal � 1

for k=1:Data.RTH.N
    Data.RTH.TimeDifference(k) = fread(fid, 1, 'uint16');
    if feof(fid)
        return
    end

    X = fread(fid, 4, 'single');
    if feof(fid)
        return
    end
    
    Data.RTH.Roll(k)    = X(1);
    Data.RTH.Pitch(k)   = X(2);
    Data.RTH.Heave(k)   = X(3);
    Data.RTH.Heading(k) = X(4);
end

rad2pi = 180/pi;
Data.RTH.Roll    = Data.RTH.Roll    * rad2pi;
Data.RTH.Pitch   = Data.RTH.Pitch   * rad2pi;
Data.RTH.Heading = Data.RTH.Heading * rad2pi;

%{
figure;
subplot(5,1,1); plot(Data.RTH.Roll);  grid on; title('Roll (deg)')
subplot(5,1,2); plot(Data.RTH.Pitch); grid on; title('Pitch (deg)')
subplot(5,1,3); plot(Data.RTH.Heave); grid on; title('Heave (m)')
subplot(5,1,4); plot(Data.RTH.Heading); grid on; title('Heading (deg)')
subplot(5,1,5); plot(Data.RTH.TimeDifference); grid on; title('TimeDifference (ms)')
%}

%% Read Recod Data

Data.RD = [];

%% Read Optional Data

Data.OD = [];

flag = 1;