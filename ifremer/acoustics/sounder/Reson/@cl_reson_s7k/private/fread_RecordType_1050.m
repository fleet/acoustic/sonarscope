 % Single Record Request
 
 function [flag, Data] = fread_RecordType_1050(fid)

Data = [];

% Lecture de l'ent�te de la FRAME.
Data.RTH.Reserved = fread(fid, 2, 'double');

%Lecture des Data de la FRAME.
Data.RD = [];
%Lecture des Optional Data de la FRAME.
Data.OD = [];

flag = 1;
