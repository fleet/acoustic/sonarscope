% Start Recording

function [flag, Data] = fread_RecordType_1200(fid)

my_breakpoint % Jamais rencontré

flag = 0;
Data = [];

%% Read Record Type Header

X = fread(fid, 4, 'uint32');
if feof(fid)
    return
end

Data.RTH.PingCounter                     = X(1);
Data.RTH.NumberOfChannels                = X(2);
Data.RTH.TotalBytesOfChannelDatatoFollow = X(3);
Data.RTH.DataType                        = X(4);

X = fread(fid, 5, 'char');
if feof(fid)
    return
end

Data.RTH.ChannelNumer   = X(1);
Data.RTH.ChannelType    = X(2);
Data.RTH.DataType       = X(3);
Data.RTH.Polarity       = X(4);
Data.RTH.BytesPerSample = X(5);

X = fread(fid, 6, 'uint32');
if feof(fid)
    return
end

Data.RTH.Reserved1       = X(1:3);
Data.RTH.NumberOfSamples = X(4);
Data.RTH.StartTime       = X(5);
Data.RTH.SampleInterval  = X(6);

X = fread(fid, 2, 'single');
if feof(fid)
    return
end

Data.RTH.Range   = X(1);
Data.RTH.Voltage = X(2);

Data.RTH.Name = fread(fid, 16, 'char');
if feof(fid)
    return
end

Data.RTH.CustomDataDescriptor = fread(fid, 1, 'uint16');
if feof(fid)
    return
end

Data.RTH.Reserved2 = fread(fid, 18, 'char');

%% Read Record Data

Data.RD = [];

%% Read Optional Data

Data.OD = [];

flag = 1;