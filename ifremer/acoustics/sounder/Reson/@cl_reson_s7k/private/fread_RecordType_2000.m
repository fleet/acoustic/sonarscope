% XYZ Data

function [flag, Data] = fread_RecordType_2000(fid)

my_breakpoint % Jamais rencontré

flag = 0;
Data = [];

%% Read Record Type Header

Data.RTH.Heading = fread(fid, 1, 'single');
if feof(fid)
    return
end

Data.RTH.FramesInDataRecord = fread(fid, 1, 'uint32');
if feof(fid)
    return
end

%% Read Record Data

for k=1:Data.RTH.FramesInDataRecord
    Data.RD.s7KTime(k) = fread(fid, 10, 'char');
    if feof(fid)
        flag = 0;
        return
    end
    
    X = fread(fid, 3, 'double');
    if feof(fid)
        flag = 0;
        return
    end
    
    Data.RD.X(k) = X(1);
    Data.RD.Y(k) = X(2);
    Data.RD.Z(k) = X(3);

    Data.RD.Tide(k) = fread(fid, 1, 'uint32');
    if feof(fid)
        flag = 0;
        return
    end

    X = fread(fid, 2, 'single');
    if feof(fid)
        flag = 0;
        return
    end
    
    Data.RD.Height(k) = X(1);
    Data.RD.Heave(k)  = X(2);

    Data.RD.Reserved(k) = fread(fid, 4, 'char');
end

%% Read Optional Data

Data.OD = [];

flag = 1;