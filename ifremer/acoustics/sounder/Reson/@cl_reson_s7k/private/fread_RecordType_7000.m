% Description
%   Reads S7K datagram 7200 (Sonar Settings)
%
% Syntax
%   [flag, Data] = fread_RecordType_7000(fid)
%
% Input Arguments
%   fid : fid of the S7K file
%
% Output Arguments
%   flag : 0=KO, 1=OK
%   Data : Structure (see example)
%
% Examples
%   [flag, Data] = fread_RecordType_7000(fid);
%   % Data = 
%   %     RTH: [1�1 struct]
%   %      RD: []
%   %      OD: []   
%   % Data.RTH
%   %                                   SonarId: 1195
%   %                               PingCounter: 191671
%   %                         MultiPingSequence: 0
%   %                                 Frequency: 190000
%   %                                SampleRate: 78125
%   %                          ReceiverBandwith: 80000
%   %                              TxPulseWidth: 1.999999949475750e-04
%   %                     TxPulseTypeIdentifier: 1
%   %                TxPulseEnveloppeIdentifier: 1
%   %                  TxPulseEnvelopeParameter: 0
%   %                           TxPulseReserved: 1
%   %                               MaxPingRate: 50
%   %                                PingPeriod: 0.045969329774380
%   %                            RangeSelection: 24.438819885253906
%   %                            PowerSelection: 227
%   %                             GainSelection: -30
%   %                              ControlFlags: 1091584
%   %                      ProjectorMagicNumber: 1
%   %               BottomDetectRangeFilterFlag: 0
%   %               BottomDetectDepthFilterFlag: 0
%   %        ProjectorBeamSteeringAngleVertical: 0
%   %      ProjectorBeamSteeringAngleHorizontal: 0
%   %          ProjectionBeam_3dB_WidthVertical: 0.015707964077592
%   %        ProjectionBeam_3dB_WidthHorizontal: 2.967059612274170
%   %                  ProjectionBeamFocalPoint: 10000000
%   %         ProjectionBeamWeightingWindowType: 0
%   %    ProjectionBeamWeightingWindowParameter: 0
%   %                     HydrophoneMagicNumber: 0
%   %                ReceiveBeamWeightingWindow: 0
%   %                             TransmitFlags: 0
%   %                    TxRollCompensationFlag: 0
%   %                   TxPitchCompensationFlag: 0
%   %             ReceiveBeamWeightingParameter: 0
%   %                              ReceiveFlags: 0
%   %                    RxRollCompensationFlag: 0
%   %                   RxPitchCompensationFlag: 0
%   %                   RxHeaveCompensationFlag: 0
%   %                          ReceiveBeamWidth: 0.015699999406934
%   %         BottomDetectionFilterInfoMinRange: 0
%   %         BottomDetectionFilterInfoMaxRange: 5
%   %         BottomDetectionFilterInfoMinDepth: 0
%   %         BottomDetectionFilterInfoMaxDepth: 5
%   %                                Absorption: 0
%   %                             SoundVelocity: 1.449171020507813e+03
%   %                                 Spreading: 50
%   %                                  Reserved: 0
%
% Authors : JMA 
% See also cre_ficIndex, fread_DRF 
%
% Reference pages in Help browser
%   <a href="matlab:doc fread_RecordType_7000">fread_RecordType_7000</a>
%-------------------------------------------------------------------------------

% TODO : optimiser la lecture avec typecast acr cette fonctions est
% gourmande en temps cpu

function [flag, Data] = fread_RecordType_7000(fid)

flag = 0;
Data = [];

%% Read Record Type Header

Data.RTH.SonarId = fread(fid, 1, 'uint64');
if feof(fid)
    return
end

Data.RTH.PingCounter = fread(fid, 1, 'uint32');
if feof(fid)
    return
end

Data.RTH.MultiPingSequence = fread(fid, 1, 'uint16');
if feof(fid)
    return
end
Data.RTH.MultiPingSequence = reversePingSequence(Data.RTH.MultiPingSequence);

X = fread(fid, 4, 'single');
if feof(fid)
    return
end
Data.RTH.Frequency        = X(1); % / 1000; % /1000 rajout� par JMA le 19/03/2016
Data.RTH.SampleRate       = X(2);
Data.RTH.ReceiverBandwith = X(3);
Data.RTH.TxPulseWidth     = X(4);

X = fread(fid, 2, 'uint32');
if feof(fid)
    return
end
Data.RTH.TxPulseTypeIdentifier      = X(1);
Data.RTH.TxPulseEnveloppeIdentifier = X(1);

Data.RTH.TxPulseEnvelopeParameter = fread(fid, 1, 'single');
if feof(fid)
    return
end

Data.RTH.TxPulseReserved = fread(fid, 1, 'uint32');
if feof(fid)
    return
end

X = fread(fid, 5, 'single');
if feof(fid)
    return
end

Data.RTH.MaxPingRate    = X(1);
Data.RTH.PingPeriod     = X(2);
Data.RTH.RangeSelection = X(3);
Data.RTH.PowerSelection = X(4);
Data.RTH.GainSelection  = X(5);

X = fread(fid, 2, 'uint32');
if feof(fid)
    return
end
Data.RTH.ControlFlags         = X(1);
Data.RTH.ProjectorMagicNumber = X(2);

Data.RTH.BottomDetectRangeFilterFlag = bitget(Data.RTH.ControlFlags, 9);
Data.RTH.BottomDetectDepthFilterFlag = bitget(Data.RTH.ControlFlags, 10);

X = fread(fid, 5, 'single');
if feof(fid)
    return
end
Data.RTH.ProjectorBeamSteeringAngleVertical   = X(1);
Data.RTH.ProjectorBeamSteeringAngleHorizontal = X(2);
Data.RTH.ProjectionBeam_3dB_WidthVertical     = X(3);
Data.RTH.ProjectionBeam_3dB_WidthHorizontal   = X(4);
Data.RTH.ProjectionBeamFocalPoint             = X(5);

Data.RTH.ProjectionBeamWeightingWindowType = fread(fid, 1, 'uint32');
if feof(fid)
    return
end

Data.RTH.ProjectionBeamWeightingWindowParameter = fread(fid, 1, 'single');
if feof(fid)
    return
end

X = fread(fid, 3, 'uint32');
if feof(fid)
    return
end

Data.RTH.HydrophoneMagicNumber      = X(2);
Data.RTH.ReceiveBeamWeightingWindow = X(3);
Data.RTH.TransmitFlags              = X(1);
Data.RTH.TxRollCompensationFlag  = bitget(Data.RTH.TransmitFlags, 1);
Data.RTH.TxPitchCompensationFlag = bitget(Data.RTH.TransmitFlags, 2);

Data.RTH.ReceiveBeamWeightingParameter = fread(fid, 1, 'single');
if feof(fid)
    return
end

Data.RTH.ReceiveFlags = fread(fid, 1, 'uint32');
if feof(fid)
    return
end
Data.RTH.RxRollCompensationFlag  = bitget(Data.RTH.ReceiveFlags, 1);
Data.RTH.RxPitchCompensationFlag = bitget(Data.RTH.ReceiveFlags, 2);
Data.RTH.RxHeaveCompensationFlag = bitget(Data.RTH.ReceiveFlags, 3);

X = fread(fid, 8, 'single');
if feof(fid)
    return
end
Data.RTH.ReceiveBeamWidth                   = X(1);
Data.RTH.BottomDetectionFilterInfoMinRange  = X(2);
Data.RTH.BottomDetectionFilterInfoMaxRange  = X(3);
Data.RTH.BottomDetectionFilterInfoMinDepth  = X(4);
Data.RTH.BottomDetectionFilterInfoMaxDepth  = X(5);
Data.RTH.Absorption                         = X(6);
Data.RTH.SoundVelocity                      = X(7);
Data.RTH.Spreading                          = X(8);

Data.RTH.Reserved = fread(fid, 1, 'uint16');

%% Read Record Data

Data.RD = [];

%% Read Optional Data

Data.OD = [];

flag = 1;
