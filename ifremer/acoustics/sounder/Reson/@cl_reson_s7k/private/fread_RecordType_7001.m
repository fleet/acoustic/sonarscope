% Description
%   Reads S7K datagram 7001 (Configuration)
%
% Syntax
%   [flag, Data] = fread_RecordType_7001(fid)
%
% Input Arguments
%   fid : fid of the S7K file
%
% Output Arguments
%   flag : 0=KO, 1=OK
%   Data : Structure (see example)
%
% Examples
%   [flag, Data] = fread_RecordType_7001(fid);
%   % Data = 
%   %     RTH: [1�1 struct]
%   %      RD: [1�1 struct]
%   %      OD: []   
%   % Data.RTH
%   %    SonarId: 0
%   %          N: 1
%   % Data.RD
%   %     DeviceMagicNumber: 0
%   %     DeviceDescription: {[64�1 char]}
%   %    DeviceSerialNumber: 0
%   %      DeviceInfoLength: 3815
%   %            DeviceInfo: {[3815�1 char]}
%   % Data.RD.DeviceInfo{1}'
%   %    '<?xml version="1.0" encoding="US-ASCII"?>
%   %     
%   %     <SB7125LF>
%   %       <Name deviceid="7125" subsystemid="1" enumerator="0" xml_reson="7.1.1.2.1.29">7125 (200kHzEA)</Name>
%   %       <SystemInfo auv="yes" projectid="generic" name="7125">7125 Sonar</SystemInfo>
%   %       <SonarType type="0" unit="nb">Bathymetric sonar</SonarType>
%   %       <ArrayType type="1" number="7200" unit="nb">Flat array</ArrayType>
%   %       <RxElements min="0" max="127" spacing="0.0032" unit="meters" input_mux="2">Receive ceramics</RxElements>
%   %       <RxBeams min="0" max="255" unit="nb" max_sin="75">Receive beams</RxBeams>
%   %       <RxBeamSpacing uniform="yes">Receiver beamspacing</RxBeamSpacing>
%   %       <RxBeamWidth center_beamwidth="1.0" uniformacross="yes" uniformalong="yes" across="0.008726646" along="0.471238898" unit="rad" proj_beamwidth="2.0">Receiver beamspacing</RxBeamWidth>
%   %       <RxBeamWedge coverage="128.10" min_coverage="45.0" max_coverage="128.10" unit="deg">Receiver beam wedge info</RxBeamWedge>
%   %       <RxBeamStabilization type="0">Receiver beam stabilization</RxBeamStabilization>
%   %       <TxType type="standard" number="2163" unit="nb">Standard</TxType>
%   %       <TxBeams min="0" max="0" unit="nb">Transmit beams</TxBeams>
%   %       <TxBeamSteering steerable="no" maxx="0.0" minx="0.0" maxz="0.0" minz="0.0" unit="rad">Transmit beam steering</TxBeamSteering>
%   %       <TxBeamSpacing uniform="yes" angles="0.0" unit="rad">Transmit beamspacing</TxBeamSpacing>
%   %       <TxBeamWidth variable="no" maxx="0.0174533" minx="0.0174533" maxz="2.094395102" minz="2.094395102" unit="rad">Transmit beamwidth</TxBeamWidth>
%   %       <TxBeamStabilization type="0">Transmit beam stabilization</TxBeamStabilization>
%   %       <TxPulseLength min="0.000033" max="0.000300" type="Rectangular" unit="s" measured="10e-6">Transmit pulse length</TxPulseLength>
%   %       <TxDelay base="0.697e-3" units="sec">Transmit Delay</TxDelay>
%   %       <Frequency chirp="no" min="200000.0" max="200000.0" center="200000.0" unit="hz">Transmit frequency</Frequency>
%   %       <SampleRate rate="34482.75862" unit="hz">Receiver sample rate</SampleRate>
%   %       <Power min="170.0" max="220.0" tx_power_tweak="-17.0" shared="no" unit="dB//uPa">Transmit power</Power>
%   %       <Gain min="0.0" max="83.0" tvg_limit="83" unit="dB">Receiver gain</Gain>
%   %       <Range min="5.0" max="750.0" unit="m">Operating range</Range>
%   %       <RangeSet    size="22" _1="5" _2="8" _3="10" _4="15" _5="20" _6="25" _7="30" _8="35" _9="40" _10="50" _11="75" _12="100" _13="125" _14="150" _15="175" _16="200" _17="250" _18="300" _19="350" _20="400" _21="500" _22="750" unit="m">Valid Range Set</RangeSet>
%   %       <PingRate min="0.0" max="50.0" ratio="1.0" freerun="no" unit="p/s">Ping rate</PingRate>
%   %       <Motion rollable="yes" pitchable="no" heavable="no" roll="1" pitch="0" heave="0" roll_ON="yes" pitch_ON="yes">Motion compensation factor</Motion>
%   %       <FWInfo type="single" pps="new" bite="new" bf_upm_level="3">Firmware Info</FWInfo>
%   %       <FWFiles bitfile="bf128x256x32_20090923_20100628.bit" BITEfile="7K_Bite_7125_200kHz_2B.htm">Firmware Sonar Specific Files</FWFiles>
%   %       <FWFilterFiles size="0">Firmware Filter Sonar Specific Files</FWFilterFiles>
%   %       <FPGA TxRxDelayOffset="0" tx_skip="0" rx_skip="0" lo_if="600000.0" delay="697e-6" offset_size="3" _1="300" _2="458" _3="346">FPGA Sonar Specific Values</FPGA>
%   %       <DownLink register="yes" remote="yes">Downlink Commands</DownLink>
%   %       <RDR limitsize="yes" maxsize="256000000" units="Bytes" format="short" xxfilename="c:\somefile.s7k">Raw data recording</RDR>
%   %       <StartState APTable="7125_200kHz_Default.apc" maxpower="0.0" xxping="yes" selected="yes" xxrdr="off" udp="on" calibrate="no" swiothrottlems="1000">Initial overwrite values</StartState>
%   %       <BottomDetection method="G2">BD Method (G1_Simple, G1_BlendFilt, G2)</BottomDetection>
%   %       <Warnings PPS="on">Warning overrides</Warnings>
%   %       <GUIState wedgethrottlems="160">Initial overwrite values</GUIState>
%   %     </SB7125LF> '
%
% Authors : JMA 
% See also cre_ficIndex, fread_DRF 
%
% Reference pages in Help browser
%   <a href="matlab:doc fread_RecordType_7001">fread_RecordType_7001</a>
%-------------------------------------------------------------------------------

function [flag, Data] = fread_RecordType_7001(fid)

Data = [];
flag = 0;

%% Read Record Type Header

Data.RTH.SonarId = fread(fid, 1, 'uint64');
if feof(fid)
    return
end

Data.RTH.N = fread(fid, 1, 'uint32');
if feof(fid)
    return
end

% my_warndlg('Calculer la taille du champ Device Info pour le fichier en cours (voir DRF.DeviveIdentifier)', 1);

%{
    position = ftell(fid);
    fseek(fid, position, 'bof');
%}

%% Read Record Data

for k=1:Data.RTH.N
    Data.RD.DeviceMagicNumber(k) = fread(fid, 1, 'uint32');
    if feof(fid)
        return
    end
    Data.RD.DeviceDescription{k} = fread(fid, 64, 'char=>char');
    if feof(fid)
        return
    end
    Data.RD.DeviceSerialNumber(k) = fread(fid, 1, 'double');
    if feof(fid)
        return
    end
    Data.RD.DeviceInfoLength(k) = fread(fid, 1, 'uint32');
    if feof(fid)
        return
    end
    Data.RD.DeviceInfo{k} = fread(fid, Data.RD.DeviceInfoLength(k), 'char=>char');
    if feof(fid)
        return
    end
end

%% Read Optional Data

Data.OD = [];

flag = 1;