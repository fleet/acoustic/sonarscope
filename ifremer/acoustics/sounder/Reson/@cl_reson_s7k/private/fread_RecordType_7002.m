% 7k Pulse Compression

function [flag, Data] = fread_RecordType_7002(fid)

my_breakpoint % Jamais rencontré

flag = 0;
Data = [];

%% Read Record Type Header

Data.RTH.SonarId = fread(fid, 1, 'uint64');
if feof(fid)
    return
end

X = fread(fid, 2, 'uint32');
if feof(fid)
    return
end

Data.RTH.PingCounter = X(1);
Data.RTH.Operation   = X(2);

X = fread(fid, 2, 'single');
if feof(fid)
    return
end

Data.RTH.StartFrequency = X(1);
Data.RTH.StopFrequency  = X(2);

%% Read Record Data

Data.RD = [];

%% Read Optional Data

Data.OD = [];

flag = 1;