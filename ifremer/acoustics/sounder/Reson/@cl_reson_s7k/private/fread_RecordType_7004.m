% Description
%   Reads S7K datagram 7004 (Beam Geometry)
%
% Syntax
%   [flag, Data] = fread_RecordType_7004(fid)
%
% Input Arguments
%   fid : fid of the S7K file
%
% Output Arguments
%   flag : 0=KO, 1=OK
%   Data : Structure (see example)
%
% Examples
%   [flag, Data] = fread_RecordType_7004(fid);
%   % Data = 
%   %     RTH: [1�1 struct]
%   %      RD: [1�1 struct]
%   %      OD: []   
%   % Data.RTH
%   %   SonarId: 1195
%   %         N: 256
%   % Data.RD
%   %      BeamVerticalDirectionAngle: [1�256 double]
%   %    BeamHorizontalDirectionAngle: [1�256 double]
%   %                  s_3dBBeamWithY: [1�256 double]
%   %                  s_3dBBeamWithX: [1�256 double]
%
% Authors : JMA 
% See also cre_ficIndex, fread_DRF 
%
% Reference pages in Help browser
%   <a href="matlab:doc fread_RecordType_7004">fread_RecordType_7004</a>
%-------------------------------------------------------------------------------

function [flag, Data] = fread_RecordType_7004(fid)

flag = 1;

%% Read Record Type Header

Data.RTH.SonarId = fread(fid, 1, 'uint64');
if feof(fid)
    flag = 0;
    return
end

Data.RTH.N = fread(fid, 1, 'uint32');
if feof(fid)
    flag = 0;
    return
end

% if ~any(Data.RTH.N == [201 301 440 880 256 512    554]) % 7111, 7150 7125    ???? 554 rencontr� sur fichier 20090511_175914_PP-7150.s7k
if any(Data.RTH.N > 880) % Modifi� le 15/02/2010
    str = sprintf('Try to read number of beams byte=%d on .s7k file. Returned value is %d that is considered as a mistake.', ...
        ftell(fid)-4, Data.RTH.N);
    my_warndlg(str, 0, 'Tag', 'Data.RTH.NTropGrand');
    flag = 0;
    return
end

if nargout == 1
    return
end

%% Read Record Data

rad2deg = 180/pi;
Data.RD.BeamVerticalDirectionAngle = fread(fid, Data.RTH.N, 'single')' * rad2deg;
if feof(fid)
    flag = 0;
    return
end

Data.RD.BeamHorizontalDirectionAngle = fread(fid, Data.RTH.N, 'single')' * rad2deg;
if feof(fid)
    flag = 0;
    return
end

Data.RD.s_3dBBeamWithY = fread(fid, Data.RTH.N, 'single')' * rad2deg;
if feof(fid)
    flag = 0;
    return
end

Data.RD.s_3dBBeamWithX = fread(fid, Data.RTH.N, 'single')' * rad2deg;

%% Read Optional Data

Data.OD = [];
