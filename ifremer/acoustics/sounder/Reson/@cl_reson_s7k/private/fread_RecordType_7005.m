% 7k Calibration Data

function [flag, Data] = fread_RecordType_7005(fid)

my_breakpoint % Jamais rencontr�

flag = 0;
Data = [];

%% Read Record Type Header

Data.RTH.SonarId = fread(fid, 1, 'uint64');
if feof(fid)
    return
end

Data.RTH.N = fread(fid, 1, 'uint16');
if feof(fid)
    return
end

%% Read Record Data

% TODO ; refaire �a mieux que �a (lecture[2 Data.RTH.N] ou [2 Data.RTH.N]),
% attendre qu'on ait de la donn�e pour valider
X = fread(fid, 2*Data.RTH.N, 'single');
if feof(fid)
    flag = 0;
    return
end
for k=1:Data.RTH.N
    Data.RD.ReceiverGain(k)  = X(2*k)-1;
    Data.RD.ReceiverPhase(k) = X(2*k);
end

%% Read Optional Data

Data.OD = [];

flag = 1;
