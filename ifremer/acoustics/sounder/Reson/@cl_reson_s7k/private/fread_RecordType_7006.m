% Description
%   Reads S7K datagram 7006 (Bathymetric Data)
%
% Syntax
%   [flag, PingNumber, Sequence, nbFaisceaux, Data] = fread_RecordType_7006(fid)
%
% Input Arguments
%   fid : fid of the S7K file
%
% Output Arguments
%   flag        : 0=KO, 1=OK
%   PingNumber  : 
%   Sequence    : 
%   nbFaisceaux : 
%   Data        : Structure (see example)
%
% Examples
%   [flag, PingNumber, Sequence, nbFaisceaux, Data] = fread_RecordType_7006(fid);
%   % Data = 
%   %     RTH: [1�1 struct]
%   %      RD: [1�1 struct]
%   %      OD: []   
%   % Data.RTH
%   %              SonarId: 0
%   %          PingCounter: 10633
%   %    MultiPingSequence: 1
%   %                    N: 880
%   % Data.RD
%   %            Range: [880�1 double]
%   %          Quality: [880�1 double]
%   %        Intensity: [880�1 double]
%   %    MinFilterInfo: [880�1 double]
%   %    MaxFilterInfo: [880�1 double]
%   % Data.OD
%   %                  Frequency: 12.500000000000000
%   %                   Latitude: -12.836970576818759
%   %                  Longitude: 45.685312878504256
%   %                    Heading: 20.991153781855779
%   %               HeightSource: 0
%   %                       Tide: 0
%   %                       Roll: 0.239999987468701
%   %                      Pitch: 0.209999995705220
%   %                      Heave: 0.239013567566872
%   %               VehicleDepth: 0.819999992847443
%   %                  BeamDepth: [1�880 single]
%   %     BeamAlongTrackDistance: [1�880 single]
%   %    BeamAcrossTrackDistance: [1�880 single]
%   %              PointingAngle: [1�880 single]
%   %               AzimuthAngle: [1�880 single]
%
% Authors : JMA 
% See also cre_ficIndex, fread_DRF 
%
% Reference pages in Help browser
%   <a href="matlab:doc fread_RecordType_7006">fread_RecordType_7006</a>
%-------------------------------------------------------------------------------

function [flag, PingNumber, Sequence, nbFaisceaux, Data] = fread_RecordType_7006(fid, DRF)

flag        = 0;
Data        = [];
PingNumber  = [];
Sequence    = [];
nbFaisceaux = [];

rad2deg = 180/pi;

%Lecture de l'ent�te de la FRAME.
Data.RTH.SonarId = fread(fid, 1, 'uint64');
if feof(fid)
    return
end

Data.RTH.PingCounter = fread(fid, 1, 'uint32');
if feof(fid)
    return
end
PingNumber = Data.RTH.PingCounter;

Data.RTH.MultiPingSequence = fread(fid, 1, 'uint16');
if feof(fid)
    return
end
Data.RTH.MultiPingSequence = reversePingSequence(Data.RTH.MultiPingSequence);
Sequence = Data.RTH.MultiPingSequence;

Data.RTH.N = fread(fid, 1, 'uint32');
if feof(fid)
    return
end
if Data.RTH.N > 880
    str = sprintf('Try to read number of beams byte=%d on .s7k file. Returned value is %d that is considered as a mistake.', ...
        ftell(fid)-4, Data.RTH.N);
    my_warndlg(str, 0, 'Tag', 'Data.RTH.NTropGrand');
    return
end
nbFaisceaux = Data.RTH.N;

if nargout == 4
    flag = 1;
    return
end

X = fread(fid, 2, 'uint8');
if feof(fid)
    return
end

Data.RTH.LayerCompensationFlag = X(1);
Data.RTH.SoundVelocityFlag     = X(2);

Data.RTH.SoundVelocity = fread(fid, 1, 'single');
if feof(fid)
    return
end

%Lecture des Data de la FRAME.

Data.RD.Range = fread(fid, Data.RTH.N, 'single');
if feof(fid)
    flag = 0;
    return
end

Data.RD.Quality = fread(fid, Data.RTH.N, 'uint8');
if feof(fid)
    flag = 0;
    return
end

Data.RD.Intensity = fread(fid, Data.RTH.N, 'single');
if feof(fid)
    return
end

%% Read Record Data

nbOcRTH = 88 + Data.RTH.N*9;
% fprintf('DRF.OptionalDataOffset=%d  nbOcRTH=%d\n', DRF.OptionalDataOffset, nbOcRTH);

% Lecture des Optional Data de la FRAME.
if DRF.OptionalDataOffset ~= nbOcRTH
    Data.RD.MinFilterInfo = fread(fid, Data.RTH.N, 'single');
    if feof(fid)
        return
    end

    Data.RD.MaxFilterInfo = fread(fid, Data.RTH.N, 'single');
    if feof(fid)
        return
    end
    %{
    figure; plot(Data.RD.MinFilterInfo); grid on; hold on; plot(Data.RD.MaxFilterInfo)
    %}
else
    Data.RD.MinFilterInfo = [];
    Data.RD.MinFilterInfo = [];
end

%% Read Optional Data

if DRF.OptionalDataOffset ~= 0
    Data.OD.Frequency = fread(fid, 1, 'single');
    if feof(fid)
        return
    end
    
    X = fread(fid, 2, 'double');
    if feof(fid)
        flag = 0;
        return
    end
    
    Data.OD.Latitude  = X(1);
    Data.OD.Longitude = X(2);
    
    Data.OD.Heading = fread(fid, 1, 'single');
    if feof(fid)
        return
    end
    
    Data.OD.HeightSource = fread(fid, 1, 'uint8');
    if feof(fid)
        return
    end
    
    X = fread(fid, 5, 'single');
    if feof(fid)
        flag = 0;
        return
    end

    Data.OD.Tide         = X(1);
    Data.OD.Roll         = X(2);
    Data.OD.Pitch        = X(3);
    Data.OD.Heave        = X(4);
    Data.OD.VehicleDepth = X(5);

    Data.OD.BeamDepth               = NaN(1, Data.RTH.N, 'single');
    Data.OD.BeamAlongTrackDistance  = NaN(1, Data.RTH.N, 'single');
    Data.OD.BeamAcrossTrackDistance = NaN(1, Data.RTH.N, 'single');
    Data.OD.PointingAngle           = NaN(1, Data.RTH.N, 'single');
    Data.OD.AzimuthAngle            = NaN(1, Data.RTH.N, 'single');

    X = fread(fid, 5 * Data.RTH.N, 'single');
    if feof(fid)
        flag = 0;
        return
    end
    id = 0;

    for k=1:Data.RTH.N
%         X = fread(fid, 5, 'single');
        Data.OD.BeamDepth(k)               = X(id+1);
        Data.OD.BeamAlongTrackDistance(k)  = X(id+2);
        Data.OD.BeamAcrossTrackDistance(k) = X(id+3);
        Data.OD.PointingAngle(k)           = X(id+4);
        Data.OD.AzimuthAngle(k)            = X(id+5);
        id = id + 5;
    end

    Data.OD.Heading       = Data.OD.Heading       * rad2deg;
    Data.OD.Longitude     = Data.OD.Longitude     * rad2deg;
    Data.OD.Latitude      = Data.OD.Latitude      * rad2deg;
    Data.OD.Roll          = Data.OD.Roll          * rad2deg;
    Data.OD.Pitch         = Data.OD.Pitch         * rad2deg;
    Data.OD.PointingAngle = Data.OD.PointingAngle * rad2deg;
    Data.OD.AzimuthAngle  = Data.OD.AzimuthAngle  * rad2deg;
    Data.OD.Frequency     = Data.OD.Frequency     * 1e-3;
    Data.OD.VehicleDepth  = -Data.OD.VehicleDepth;
end

flag = 1;

%{
figure;
h(1) = subplot(5,1,1); plot(Data.OD.BeamDepth);                 grid on; title('BeamDepth');
h(2) = subplot(5,1,2); plot(Data.OD.BeamAlongTrackDistance);    grid on; title('BeamAlongTrackDistance');
h(3) = subplot(5,1,3); plot(Data.OD.BeamAcrossTrackDistance);   grid on; title('BeamAcrossTrackDistance');
h(4) = subplot(5,1,4); plot(Data.OD.PointingAngle);             grid on; title('PointingAngle');
h(5) = subplot(5,1,5); plot(Data.OD.AzimuthAngle);              grid on; title('AzimuthAngle');
linkaxes(h,'x')
%}
