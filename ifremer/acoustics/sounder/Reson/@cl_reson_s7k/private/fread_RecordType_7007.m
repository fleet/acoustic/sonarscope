% Description
%   Reads S7K datagram 7007 (Backscatter Imagery Data)
%
% Syntax
%   [flag, PingNumber, Sequence, Data] = fread_RecordType_7007(fid)
%
% Input Arguments
%   fid : fid of the S7K file
%
% Output Arguments
%   flag       : 0=KO, 1=OK
%   PingNumber : 
%   Sequence   : 
%   Data       : Structure (see example)
%
% Examples
%   [flag, PingNumber, Sequence, Data] = fread_RecordType_7007(fid);
%   % Data = 
%   %     RTH: [1�1 struct]
%   %      RD: []
%   %      OD: []   
%   % Data.RTH
%   %                         SonarId: 1195
%   %                     PingCounter: 191671
%   %               MultiPingSequence: 0
%   %                    BeamPosition: 0
%   %                    ControlFlags: 1
%   %                               S: 2635
%   %               Port_3dBBeamWithY: 8.379764816662406e-43
%   %               Port_3dBBeamWithZ: 0
%   %         Startboard_3dBBeamWithY: 0
%   %         Startboard_3dBBeamWithZ: 0
%   %           PortBeamSteeringWithY: 0
%   %           PortBeamSteeringWithZ: 0
%   %    StartboardBeamSteeringAngleY: 0
%   %    StartboardBeamSteeringAngleZ: 0
%   %                               N: 1
%   %               CurrentBeamNumber: 0
%   %                               W: 2
%   %                       DataTypes: 0
%   % Data.RD
%   %          PortBeamAmplitude: [2635�1 double]
%   %    StartboardBeamAmplitude: [2635�1 double]
%   % Data.OD
%   %    Frequency: 0
%   %     Latitude: 7.967837354742770e-308
%   %    Longitude: 1.931445888630727e-318
%
% Authors : JMA 
% See also cre_ficIndex, fread_DRF 
%
% Reference pages in Help browser
%   <a href="matlab:doc fread_RecordType_7007">fread_RecordType_7007</a>
%-------------------------------------------------------------------------------

function [flag, PingNumber, Sequence, Data] = fread_RecordType_7007(fid)

flag       = 0;
Data       = [];
PingNumber = [];
Sequence   = [];

%% Read Record Type Header

%Lecture de l'ent�te de la FRAME.
Data.RTH.SonarId = fread(fid, 1, 'uint64');
if feof(fid)
    return
end

Data.RTH.PingCounter = fread(fid, 1, 'uint32');
if feof(fid)
    return
end
PingNumber = Data.RTH.PingCounter;

Data.RTH.MultiPingSequence = fread(fid, 1, 'uint16');
if feof(fid)
    return
end
Data.RTH.MultiPingSequence = reversePingSequence(Data.RTH.MultiPingSequence);
Sequence = Data.RTH.MultiPingSequence;

if nargout == 3
    flag = 1;
    return
end

Data.RTH.BeamPosition = fread(fid, 1, 'single');
if feof(fid)
    return
end

X = fread(fid, 2, 'uint32');
if feof(fid)
    return
end

Data.RTH.ControlFlags = X(1);
Data.RTH.S            = X(2);

X = fread(fid, 8, 'single');
if feof(fid)
    return
end

Data.RTH.Port_3dBBeamWithY            = X(1);
Data.RTH.Port_3dBBeamWithZ            = X(2);
Data.RTH.Startboard_3dBBeamWithY      = X(3);
Data.RTH.Startboard_3dBBeamWithZ      = X(4);
Data.RTH.PortBeamSteeringWithY        = X(5);
Data.RTH.PortBeamSteeringWithZ        = X(6);
Data.RTH.StartboardBeamSteeringAngleY = X(7);
Data.RTH.StartboardBeamSteeringAngleZ = X(8);

X = fread(fid, 2, 'uint16');
if feof(fid)
    return
end

Data.RTH.N                 = X(1);
Data.RTH.CurrentBeamNumber = X(2);

X = fread(fid, 2, 'char');
if feof(fid)
    return
end

Data.RTH.W         = X(1);
Data.RTH.DataTypes = X(2);

%% Read Record Data

% Data.RD.PortBeamAmplitude       = NaN(Data.RTH.S, Data.RTH.W, 'single');
% Data.RD.StartboardBeamAmplitude = NaN(Data.RTH.S, Data.RTH.W, 'single');

% %Lecture des Data de la FRAME.
% for i=1:Data.RTH.S
%     if Data.RTH.DataTypes == 0
%         Data.RD.PortBeamAmplitude(i,:) = fread(fid, Data.RTH.W, 'char');
%         if feof(fid)
%             flag = 0;
%             return
%         end
%         Data.RD.StartboardBeamAmplitude(i,:) = fread(fid, Data.RTH.W, 'char');
%     else
%         Data.RD.PortBeamPhase(i) = fread(fid, Data.RTH.W, 'char');
%         if feof(fid)
%             flag = 0;
%             return
%         end
%         Data.RD.StartboardBeamPhase(i) = fread(fid, Data.RTH.W, 'char');
%     end
% end

if Data.RTH.DataTypes == 0
    switch Data.RTH.W
        case 1
        case 2
            Data.RD.PortBeamAmplitude = fread(fid, Data.RTH.S, 'uint16');
            if feof(fid)
                flag = 0;
                return
            end
            % figure; plot(Data.RD.PortBeamAmplitude); grid on
            Data.RD.StartboardBeamAmplitude = fread(fid, Data.RTH.S, 'uint16');
        otherwise
            my_warndlg('Beurk : fread_RecordType_7007', 1);
    end
else
    switch Data.RTH.W
        case 1
        case 2
            Data.RD.PortBeamPhase = fread(fid, Data.RTH.S, 'uint16');
            if feof(fid)
                flag = 0;
                return
            end
            % figure; plot(Data.RD.PortBeamPhase); grid on
            Data.RD.StartboardBeamPhase = fread(fid, Data.RTH.S, 'uint16');
        otherwise
            my_warndlg('Beurk : fread_RecordType_7007', 1);
    end
end

if feof(fid)
    return
end

%% Read Optional Data

Data.OD.Frequency = fread(fid, 1, 'single') / 1000;
if feof(fid)
    return
end

X = fread(fid, 2, 'double');
if feof(fid)
    return
end
Data.OD.Latitude  = X(1) * (180/pi);
Data.OD.Longitude = X(2) * (180/pi);

flag = 1;
