% 7K Generic Data (Mag & Phase)

function [flag, PingNumber, Sequence, Data] = fread_RecordType_7008(fid, DRF, varargin)

flag       = 0;
Data       = [];
PingNumber = [];
Sequence   = [];

rad2pi = 180/pi;

%% Read Record Type Header

Data.RTH.SonarId = fread(fid, 1, 'uint64');
if feof(fid)
    return
end

Data.RTH.PingCounter = fread(fid, 1, 'uint32');
if feof(fid)
    return
end
PingNumber = Data.RTH.PingCounter;

X = fread(fid, 3, 'uint16');
if feof(fid)
    return
end

Data.RTH.MultiPingSequence = X(1);
Data.RTH.N                 = X(2);
Data.RTH.Reserved          = X(3);

Data.RTH.MultiPingSequence = reversePingSequence(Data.RTH.MultiPingSequence);
Sequence = Data.RTH.MultiPingSequence;

Data.RTH.Samples = fread(fid, 1, 'uint32');
if feof(fid)
    return
end

X = fread(fid, 2, 'uint8');
if feof(fid)
    return
end

Data.RTH.RecordSubsetFlag = X(1);
Data.RTH.RowColumnFlag    = X(2);

Data.RTH.Reserved2 = fread(fid, 1, 'uint16');
if feof(fid)
    return
end

X = fread(fid, 8, 'ubit4');

% amp=0 : pas de donn�s d'amplitide
% amp=1 : amplitude en uint8
% amp=2 : amplitude en int16
Data.RTH.TypeAmp = X(1);

% phase=0 : pas de donn�s d'amplitide
% phase=1 : amplitude en uint8
% phase=2 : amplitude en uint16
Data.RTH.TypePhase = X(2);

% % TypeIQ=0 : No I&Q
% % TypeIQ=1 : I&Q en uint16
% % TypeIQ=2 : I&Q en uint32
% Data.RTH.TypeIQ = X(3);

%Lecture des Data de la FRAME.

if nargout == 3
    flag = 1;
    return
end

%% Read Record Data

Data.RD.BeamDescriptor        = zeros(1,Data.RTH.N, 'single');
Data.RD.BeginSampleDescriptor = zeros(1,Data.RTH.N, 'single');
Data.RD.EndSampleDescriptor   = zeros(1,Data.RTH.N, 'single');
for k=1:Data.RTH.N
    % TODO : optimiser la lecture avec typecast
    X = fread(fid, 1, 'uint16');
    if feof(fid)
        flag = 0;
        return
    end
    Data.RD.BeamDescriptor(k) = X;
    
    X = fread(fid, 2, 'uint32');
    if feof(fid)
        flag = 0;
        return
    end
    
    Data.RD.BeginSampleDescriptor(k) = X(1);
    Data.RD.EndSampleDescriptor(k)   = X(2);
end
% figure; plot(Data.RD.BeamDescriptor); grid on
% figure; plot(Data.RD.BeginSampleDescriptor); grid on
% figure; plot(Data.RD.EndSampleDescriptor); grid on

if (Data.RTH.TypeAmp == 2) && (Data.RTH.TypePhase == 2)
    X = fread(fid, Data.RTH.N * Data.RTH.Samples * 2, 'int16=>single')';
    if feof(fid)
        flag = 0;
        return
    end
    Am = reshape(X(1:2:end), Data.RTH.N, Data.RTH.Samples);
    sub = (Am < 0);
    Am(sub) = 65535 + Am(sub);
    clear sub
    Data.RD.Amplitude = Am';
    
    Ph = reshape(X(2:2:end), Data.RTH.N, Data.RTH.Samples);    
    Data.RD.Phase  = Ph' * (180 / 32768);
%     SonarScope(Data.RD.Amplitude)
%     SonarScope(Data.RD.Phase)    
else
    % Cas du fichier C:\TEMP\Delphine\20071015_184035_Victor6000.s7k
    %     P = ftell(fid);
    %     fseek(fid, P, 0);
    
    Data.RD.Samples = fread(fid, 100*Data.RTH.N, 'uint16')';
    if feof(fid)
        flag = 0;
        return
    end
%     if (nargout == 1) || (nargin == 2)
%         return
%     end
end

%% Read Optional Data
    
if DRF.OptionalDataOffset == 0
    flag = 1;
    return
end
Frequency  = fread(fid, 1, 'single');
if feof(fid)
    flag = 0;
    return
end

X = fread(fid, 3, 'double');

Latitude  = X(1) * rad2pi;
Longitude = X(2) * rad2pi;
Heading   = X(3) * rad2pi;
    
Data.OD.Frequency = Frequency;
Data.OD.Latitude  = Latitude;
Data.OD.Longitude = Longitude;
Data.OD.Heading   = Heading;

for k=1:Data.RTH.N
    X = fread(fid, 2, 'single');
    if feof(fid)
        flag = 0;
        return
    end
    
    Data.OD.BeamAlongTrackDistance(k)  = X(1);
    Data.OD.BeamAcrossTrackDistance(k) = X(2);
    
    X = fread(fid, 1, 'uint32');
    if feof(fid)
        flag = 0;
        return
    end
    Data.OD.CenterSampleDistance(k) = X;
end

% figure; plot(Data.OD.BeamAlongTrackDistance); grid on; title('BeamAlongTrackDistance')
% figure; plot(Data.OD.BeamAcrossTrackDistance); grid on; title('BeamAcrossTrackDistance')
% figure; plot(Data.OD.CenterSampleDistance); grid on; title('CenterSampleDistance')

flag = 1;