% Description
%   Reads S7K datagram 7009 (Vertical Depth)
%
% Syntax
%   [flag, PingNumber, Sequence, Data] = fread_RecordType_7009(fid)
%
% Input Arguments
%   fid : fid of the S7K file
%
% Output Arguments
%   flag        : 0=KO, 1=OK
%   PingNumber  : 
%   Sequence    : 
%   Data        : Structure (see example)
%
% Examples
%   [flag, PingNumber, Sequence, nbFaisceaux, Data] = fread_RecordType_7009(fid);
%   % Data = 
%   %     RTH: [1�1 struct]
%   %      RD: [1�1 struct]
%   %      OD: []   
%   % Data.RTH
%   %              Frequency: 12500
%   %            PingCounter: 10633
%   %      MultiPingSequence: 1
%   %               Latitude: -12.836970576818759
%   %              Longitude: 45.685312878504256
%   %                Heading: 20.991153781855779
%   %     AlongTrackDistance: 35.474830627441406
%   %    AcrossTrackDistance: 0.011348214931786
%   %          VerticalDepth: -3.368253906250000e+03
%
% Authors : JMA 
% See also cre_ficIndex, fread_DRF 
%
% Reference pages in Help browser
%   <a href="matlab:doc fread_RecordType_7009">fread_RecordType_7009</a>
%-------------------------------------------------------------------------------

function [flag, PingNumber, Sequence, Data] = fread_RecordType_7009(fid)

flag       = 0;
Data       = [];
PingNumber = [];
Sequence   = [];

rad2pi = 180/pi;

%% Read Record Type Header

Data.RTH.Frequency = fread(fid, 1, 'single');
if feof(fid)
    return
end

Data.RTH.PingCounter = fread(fid, 1, 'uint32');
if feof(fid)
    return
end
PingNumber = Data.RTH.PingCounter;

Data.RTH.MultiPingSequence = fread(fid, 1, 'uint16');
if feof(fid)
    return
end
Data.RTH.MultiPingSequence = reversePingSequence(Data.RTH.MultiPingSequence);
Sequence = Data.RTH.MultiPingSequence;

if nargout == 3
    flag = 1;
    return
end

X = fread(fid, 2, 'double');
if feof(fid)
    return
end

Data.RTH.Latitude  = X(1) * rad2pi;
Data.RTH.Longitude = X(2) * rad2pi;

X = fread(fid, 4, 'single');
if feof(fid)
    return
end

Data.RTH.Heading             = X(1) * rad2pi;
Data.RTH.AlongTrackDistance  = X(2);
Data.RTH.AcrossTrackDistance = X(3);
Data.RTH.VerticalDepth       = -X(4);

%% Read Record Data

Data.RD = [];

%% Read Optional Data

Data.OD = [];

flag = 1;
