% Description
%   Reads S7K datagram 7010 (TVG Gain Values)
%
% Syntax
%   [flag, Data] = fread_RecordType_7010(fid)
%
% Input Arguments
%   fid : fid of the S7K file
%
% Output Arguments
%   flag : 0=KO, 1=OK
%   Data : Structure (see example)
%
% Examples
%   [flag, PingNumber, Sequence, nbFaisceaux, Data] = fread_RecordType_7010(fid);
%   % Data = 
%   %     RTH: [1�1 struct]
%   %      RD: []
%   %      OD: []   
%   % Data.RTH
%   %              SonarId: 0
%   %          PingCounter: 6706
%   %    MultiPingSequence: 0
%   %                    N: 10001
%   %             Reserved: [8�1 double]
%   %            GainValue: [10001�1 double]
%
% Authors : JMA 
% See also cre_ficIndex, fread_DRF 
%
% Reference pages in Help browser
%   <a href="matlab:doc fread_RecordType_7010">fread_RecordType_7010</a>
%-------------------------------------------------------------------------------

function [flag, Data] = fread_RecordType_7010(fid)

flag = 0;
Data = [];

%% Read Record Type Header

% Lecture de l'ent�te de la FRAME.
Data.RTH.SonarId = fread(fid, 1, 'uint64');
if feof(fid)
    return
end

%Lecture de l'ent�te de la FRAME.
Data.RTH.PingCounter = fread(fid, 1, 'uint32');
if feof(fid)
    return
end

Data.RTH.MultiPingSequence = fread(fid, 1, 'uint16');
if feof(fid)
    return
end
Data.RTH.MultiPingSequence = reversePingSequence(Data.RTH.MultiPingSequence);

Data.RTH.N = fread(fid, 1, 'uint32');
if feof(fid)
    return
end
if Data.RTH.N > 20000
    str = sprintf('Try to read number of beams byte=%d on .s7k file. Returned value is %d that is considered as a mistake.', ...
        ftell(fid)-4, Data.RTH.N);
    my_warndlg(str, 0, 'Tag', 'Data.RTH.NTropGrand');
    return
end

if nargout == 1
    flag = 1;
    return
end

Data.RTH.Reserved = fread(fid, 8, 'uint32');
if feof(fid)
    return
end

Data.RTH.GainValue = fread(fid, Data.RTH.N, 'single');
if feof(fid)
    return
end

%% Read Record Data

Data.RD = [];

%% Read Optional Data

Data.OD = [];

flag = 1;