% Description
%   Reads S7K datagram 7017 (Detection Data Setup)
%
% Syntax
%   [flag, Data] = fread_RecordType_7017(fid)
%
% Input Arguments
%   fid : fid of the S7K file
%
% Output Arguments
%   flag : 0=KO, 1=OK
%   Data : Structure (see example)
%
% Examples
%   [flag, Data] = fread_RecordType_7017(fid);
%   % Data = 
%   %     RTH: [1�1 struct]
%   %      RD: []
%   %      OD: []   
%   % Data.RTH
%   %                  SonarId: 0
%   %              PingCounter: 88560
%   %        MultiPingSequence: 0
%   %                        N: 253
%   %            DataBlockSize: 34
%   %       DetectionAlgorithm: 2
%   %                    Flags: 0
%   %             MinimumDepth: 0
%   %             MaximumDepth: 300
%   %             MinimumRange: 0
%   %             MaximumRange: 300
%   %       MinimumNadirSearch: 0
%   %       MaximumNadirSearch: 300
%   %    AutomaticFilterWindow: 100
%   %              AppliedRoll: 0
%   %            DepthGateTilt: 0
%
% Authors : JMA 
% See also cre_ficIndex, fread_DRF 
%
% Reference pages in Help browser
%   <a href="matlab:doc fread_RecordType_7017">fread_RecordType_7017</a>
%-------------------------------------------------------------------------------

function [flag, Data] = fread_RecordType_7017(fid, varargin)

flag = 1;

%% Read Record Type Header

Data.RTH.SonarId = fread(fid, 1, 'uint64');
if feof(fid)
    flag = 0;
    return
end

Data.RTH.PingCounter = fread(fid, 1, 'uint32');
if feof(fid)
    flag = 0;
    return
end

Data.RTH.MultiPingSequence = fread(fid, 1, 'uint16');
if feof(fid)
    flag = 0;
    return
end
Data.RTH.MultiPingSequence = reversePingSequence(Data.RTH.MultiPingSequence);

% Number of detection points
Data.RTH.N = fread(fid, 1, 'uint32');
if feof(fid)
    flag = 0;
    return
end

% Size of detection information block in bytes
Data.RTH.DataBlockSize = fread(fid, 1, 'uint32');
if feof(fid)
    flag = 0;
    return
end

% Detection algorithm
Data.RTH.DetectionAlgorithm = fread(fid, 1, 'uint8');
if feof(fid)
    flag = 0;
    return
end

% Flags
Data.RTH.Flags = fread(fid, 1, 'uint32');
if feof(fid)
    flag = 0;
    return
end

% Minimum depth
Data.RTH.MinimumDepth = fread(fid, 1, 'single');
if feof(fid)
    flag = 0;
    return
end

% Maximum  depth
Data.RTH.MaximumDepth = fread(fid, 1, 'single');
if feof(fid)
    flag = 0;
    return
end

% Minimum range 
Data.RTH.MinimumRange  = fread(fid, 1, 'single');
if feof(fid)
    flag = 0;
    return
end

% Maximum  range 
Data.RTH.MaximumRange = fread(fid, 1, 'single');
if feof(fid)
    flag = 0;
    return
end

% Minimum nadir search 
Data.RTH.MinimumNadirSearch  = fread(fid, 1, 'single');
if feof(fid)
    flag = 0;
    return
end

% Maximum nadir search 
Data.RTH.MaximumNadirSearch = fread(fid, 1, 'single');
if feof(fid)
    flag = 0;
    return
end

% Automatic filter window 
Data.RTH.AutomaticFilterWindow = fread(fid, 1, 'uint8');
if feof(fid)
    flag = 0;
    return
end

% Applied roll 
Data.RTH.AppliedRoll = fread(fid, 1, 'single');
if feof(fid)
    flag = 0;
    return
end

% Depth gate tilt 
Data.RTH.DepthGateTilt = fread(fid, 1, 'single');
if feof(fid)
    flag = 0;
    return
end

%% Read Record Data

% Reserved

fread(fid, 14, 'uint32');
if feof(fid)
    flag = 0;
    return
end

Data.RD = [];

%% Read Optional Data

Data.OD = [];

return % Car donn�e pas coh�rente avec description du format 

id = 0;
offsetBeams = (0:(Data.RTH.N-1)) * 6; % Data.RTH.DataBlockSize == 6 ?

% BeamDescriptor
nbBytes = 2;
subBytes = bsxfun(@plus, repmat(offsetBeams, nbBytes,1), (1:nbBytes)'); % Pour la R2016a
% subBytes = repmat(offsetBeams, nbBytes,1) + (1:nbBytes)'; % OK � partir de la version R2016b 
subBytes = id + subBytes(:)';
N_BeamDescriptor = typecast(Buffer(subBytes), 'uint16');
N_BeamDescriptor = N_BeamDescriptor + 1;
id = id + nbBytes;
% FigUtils.createSScFigure; PlotUtils.createSScPlot(N_BeamDescriptor, '*'); grid on; hold on; PlotUtils.createSScPlot(Data.RD.BeamDescriptor, 'or')

% DetectionPoint
nbBytes = 4;
subBytes = bsxfun(@plus, repmat(offsetBeams, nbBytes,1), (1:nbBytes)'); % Pour la R2016a
% subBytes = repmat(offsetBeams, nbBytes,1) + (1:nbBytes)'; % OK � partir de la version R2016b 
subBytes = id + subBytes(:)';
N_DetectionPoint = typecast(Buffer(subBytes), 'single');
id = id + nbBytes;
% FigUtils.createSScFigure; PlotUtils.createSScPlot(N_DetectionPoint, '*'); grid on; hold on; PlotUtils.createSScPlot(Data.RD.DetectionPoint, 'or');

Data.RD.BeamDescriptor = N_BeamDescriptor;
Data.RD.DetectionPoint = N_DetectionPoint;

