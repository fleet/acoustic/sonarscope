% Description
%   Reads S7K datagram 7018 (Beamformed Data � FP1 Release 7125/7101 - Alias Mag&Phase)
%
% Syntax
%   [flag, Data] = fread_RecordType_7018(fid)
%
% Input Arguments
%   fid : fid of the S7K file
%
% Output Arguments
%   flag : 0=KO, 1=OK
%   Data        : Structure (see example)
%
% Examples
%   [flag, PingNumber, Sequence, nbFaisceaux, Data] = fread_RecordType_7018(fid);
%   % Data = 
%   %     RTH: [1�1 struct]
%   %      RD: []
%   %      OD: []   
%   % Data.RTH:
%   %              SonarId: 0
%   %          PingCounter: 6706
%   %    MultiPingSequence: 0
%   %            FirstBeam: 0
%   %                    N: 301
%   %              Samples: 10001
%   %              TypeAmp: 16
%   %            TypePhase: 16
%   %               TypeIQ: 0
%   % Data.RD: 
%   %    Amplitude: [10001�301 single]
%   %        Phase: [10001�301 single]
%
% Authors : JMA 
% See also cre_ficIndex, fread_DRF 
%
% Reference pages in Help browser
%   <a href="matlab:doc fread_RecordType_7018">fread_RecordType_7018</a>
%-------------------------------------------------------------------------------

function [flag, Data] = fread_RecordType_7018(fid, varargin)

Data.OD = [];
flag = 1;

%% Read Record Type Header

Data.RTH.SonarId = fread(fid, 1, 'uint64');
if feof(fid)
    flag = 0;
    return
end

Data.RTH.PingCounter = fread(fid, 1, 'uint32');
if feof(fid)
    flag = 0;
    return
end

Data.RTH.MultiPingSequence = fread(fid, 1, 'uint16');
if feof(fid)
    flag = 0;
    return
end
Data.RTH.MultiPingSequence = reversePingSequence(Data.RTH.MultiPingSequence);

X = fread(fid, 1, 'uint16');
if feof(fid)
    flag = 0;
    return
end

if X == 0 % Old datagrams for 7111 and 7150
    Data.RTH.FirstBeam = X;
    Data.RTH.N = fread(fid, 1, 'uint16');
    if feof(fid)
        flag = 0;
        return
    end
    VersionFormat = 1;
else % New datagrammes avalable for all sounders
    Data.RTH.N = X;
    VersionFormat = 2;
end


if Data.RTH.N > 880
    flag = 0;
    return
end

Data.RTH.Samples = fread(fid, 1, 'uint32');
if feof(fid)
    flag = 0;
    return
end
if Data.RTH.Samples > 20000
    flag = 0;
    return
end

if VersionFormat == 1
    X = fread(fid, 3, 'uint8');
    if feof(fid)
        flag = 0;
        return
    end
    %
    % % amp=0 : pas de donn�s d'amplitide
    % % amp=1 : amplitude en uint8
    % % amp=2 : amplitude en int16
    Data.RTH.TypeAmp = X(1); % fread(fid, 1, 'uint8');
    %
    % % phase=0 : pas de donn�s d'amplitide
    % % phase=1 : amplitude en uint8
    % % phase=2 : amplitude en uint16
    Data.RTH.TypePhase = X(2); % fread(fid, 1, 'uint8');
    %
    % % TypeIQ=0 : No I&Q
    % % TypeIQ=1 : I&Q en uint16
    % % TypeIQ=2 : I&Q en uint32
    Data.RTH.TypeIQ = X(3); % fread(fid, 1, 'uint8');
else
    Data.RTH.TypeAmp   = 16;
    Data.RTH.TypePhase = 16;
    Data.RTH.TypeIQ    = 0;
end

X = fread(fid, 32, 'uint8'); %#ok<NASGU>
if feof(fid)
    flag = 0;
    return
end

if (nargout == 1) || (nargin == 2)
    return
end

%% Read Record Data

if (Data.RTH.TypeAmp == 16) && (Data.RTH.TypePhase == 16)
    X = fread(fid, Data.RTH.N*Data.RTH.Samples*2, 'int16=>single');
    if feof(fid)
        flag = 0;
        return
    end
    %     figure(66666); plot(X); grid on
    %     figure; imagesc(reshape(X(1:end/2), Data.RTH.Samples, Data.RTH.N));
    %     SonarScope((reshape(X(1:end/2), Data.RTH.Samples, Data.RTH.N)))

    Am = reshape(X(1:2:end), Data.RTH.N, Data.RTH.Samples);
    Data.RD.Amplitude = Am';
    clear Am
    
    Ph = reshape(X(2:2:end), Data.RTH.N, Data.RTH.Samples);
    clear X
    Data.RD.Phase = Ph' * (180 / 32768);
    % SonarScope(Data.RD.Amplitude)
    % SonarScope(Data.RD.Phase)
else
    % TODO
end

%% Read Optional Data

Data.OD = [];
