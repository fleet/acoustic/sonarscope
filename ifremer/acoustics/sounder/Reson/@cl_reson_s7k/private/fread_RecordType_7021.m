% Quel est son nom ?

function [flag, Data] = fread_RecordType_7021(fid, DRF)

Data = [];

fread(fid, DRF.Size-64, 'uint8');
flag = -1;
return

%% Read Record Type Header

Data.RTH.SonarId = fread(fid, 1, 'uint64'); %#ok<UNRCH>
if feof(fid)
    return
end

Data.RTH.N = fread(fid, 1, 'uint32');
if feof(fid)
    return
end

my_warndlg('Calculer la taille du champ Device Info pour le fichier en cours (voir DRF.DeviveIdentifier)', 1);

%% Read Record Data

for k=1:Data.RTH.N
    Data.RD.DeviceMagicNumber(k) = fread(fid, 1, 'uint32');
    if feof(fid)
        flag = 0;
        return
    end
    Data.RD.DeviceDescription(k) = fread(fid, 64, 'char');
    if feof(fid)
        flag = 0;
        return
    end
    Data.RD.DeviceSerialNumber(k) = fread(fid, 1, 'double');
    if feof(fid)
        flag = 0;
        return
    end
    
    X = fread(fid, 2, 'uint32');
    if feof(fid)
        flag = 0;
        return
    end    
    
    Data.RD.DeviceInfoLength(k) = X(1);
    Data.RD.DeviceInfo(k)       = X(2);
end

%% Read Optional Data

Data.OD = [];

flag = 1;