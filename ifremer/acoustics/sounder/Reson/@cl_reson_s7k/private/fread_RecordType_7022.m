% Description
%   Reads S7K datagram 7022 (Center Version)
%
% Syntax
%   [flag, Data] = fread_RecordType_7022(fid)
%
% Input Arguments
%   fid : fid of the S7K file
%
% Output Arguments
%   flag : 0=KO, 1=OK
%   Data : Structure (see example)
%
% Examples
%   [flag, Data] = fread_RecordType_7022(fid);
%   % Data = 
%   %     RTH: [1×1 struct]
%   %      RD: []
%   %      OD: []   
%   % Data.RTH
%   %    VersionString: '7kCenter  3.7.11.11   
%
% Authors : JMA 
% See also cre_ficIndex, fread_DRF 
%
% Reference pages in Help browser
%   <a href="matlab:doc fread_RecordType_7022">fread_RecordType_7022</a>
%-------------------------------------------------------------------------------

function [flag, Data] = fread_RecordType_7022(fid)

flag = 0;
Data = [];

%% Read Record Type Header

X = fread(fid, 32, 'uint8');
if feof(fid)
    return
end

Data.RTH.VersionString = char(X');

%% Read Record Data

Data.RD = [];

%% Read Optional Data

Data.OD = [];

flag = 1;
