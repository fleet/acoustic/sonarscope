% Description
%   Reads S7K datagram 7200 (RAW Detection Data)
%
% Syntax
%   [flag, PingNumber, Sequence, nbFaisceaux, Data] = fread_RecordType_7027(fid)
%
% Input Arguments
%   fid : fid of the S7K file
%
% Output Arguments
%   flag        : 0=KO, 1=OK
%   PingNumber  :
%   Sequence    : 
%   nbFaisceaux : 
%   Data        : Structure (see example)
%
% Examples
%   [flag, PingNumber, Sequence, nbFaisceaux, Data] = fread_RecordType_7027(fid);
%   % Data = 
%   %     RTH: [1�1 struct]
%   %      RD: []
%   %      OD: []   
%   % Data.RTH
%   %               SonarId: 1195
%   %           PingCounter: 191671
%   %     MultiPingSequence: 0
%   %                     N: 256
%   %         DataFieldSize: 26
%   %    DetectionAlgorithm: 2
%   %                 Flags: 1
%   %            SampleRate: 78125
%   %               TxAngle: 0
%   % Data.RD
%   %    DetectionPoint: [256�1 single]
%   %           RxAngle: [256�1 single]
%   %             Flags: [256�1 uint32]
%   %           Quality: [256�1 uint32]
%   %       Uncertainty: [256�1 single]
%   %    BeamDescriptor: [256�1 uint16]
%   % Data.OD
%   %                  Frequency: 396
%   %                   Latitude: 15.733608618065002
%   %                  Longitude: -61.563339645806970
%   %                    Heading: 2.4796857e+02
%   %               HeightSource: 0
%   %                       Tide: 0
%   %                       Roll: -1.6200000
%   %                      Pitch: -1.5899999
%   %                      Heave: -0.0011057
%   %               VehicleDepth: -1.1265548e+03
%   %                  BeamDepth: [512�1 single]
%   %     BeamAlongTrackDistance: [512�1 single]
%   %    BeamAcrossTrackDistance: [512�1 single]
%   %              PointingAngle: [512�1 single]
%   %               AzimuthAngle: [512�1 single]
%
% Authors : JMA 
% See also cre_ficIndex, fread_DRF 
%
% Reference pages in Help browser
%   <a href="matlab:doc fread_RecordType_7027">fread_RecordType_7027</a>
%-------------------------------------------------------------------------------

function [flag, PingNumber, Sequence, nbFaisceaux, Data] = fread_RecordType_7027(fid, DRF)

flag        = 0;
Data        = [];
PingNumber  = [];
Sequence    = [];
nbFaisceaux = [];

rad2deg = 180 / pi;

%% Read Record Type Header

Data.RTH.SonarId = fread(fid, 1, 'uint64');
if feof(fid)
    return
end

Data.RTH.PingCounter = fread(fid, 1, 'uint32');
if feof(fid)
    return
end
PingNumber = Data.RTH.PingCounter;

Data.RTH.MultiPingSequence = fread(fid, 1, 'uint16');
if feof(fid)
    return
end
Data.RTH.MultiPingSequence = reversePingSequence(Data.RTH.MultiPingSequence);
Sequence = Data.RTH.MultiPingSequence;

Data.RTH.N = fread(fid, 1, 'uint32');
if feof(fid)
    return
end

if Data.RTH.N > 880
    str = sprintf('Try to read number of beams byte=%d on .s7k file. Returned value is %d that is considered as a mistake.', ...
        ftell(fid)-4, Data.RTH.N);
    my_warndlg(str, 0, 'Tag', 'Data.RTH.NTropGrand');
    return
end
nbFaisceaux = Data.RTH.N;

if nargout == 4
    flag = 1;
    return
end

Data.RTH.DataFieldSize = fread(fid, 1, 'uint32');
if feof(fid)
    return
end

Data.RTH.DetectionAlgorithm = fread(fid, 1, 'uint8');
if feof(fid)
    return
end

Buffer = fread(fid, 19*4, 'uint8=>uint8'); % 19 flottants (4 octets)
if feof(fid)
    return
end

offsetBeams         = 1;
Data.RTH.Flags      = typecast(Buffer(offsetBeams:offsetBeams+3), 'uint32');
offsetBeams         = offsetBeams+4;
Data.RTH.SampleRate = typecast(Buffer(offsetBeams:offsetBeams+3), 'single');
offsetBeams         = offsetBeams+4;
Data.RTH.TxAngle    = typecast(Buffer(offsetBeams:offsetBeams+3), 'single');

% RESERVED            = X(4:19);

%% Read Record Data

Data.RD.DetectionPoint = NaN(1, Data.RTH.N, 'single');
Data.RD.RxAngle        = NaN(1, Data.RTH.N, 'single');
Data.RD.Flags          = NaN(1, Data.RTH.N, 'single');
Data.RD.Quality        = NaN(1, Data.RTH.N, 'single');
Data.RD.Uncertainty    = NaN(1, Data.RTH.N, 'single');
Data.RD.BeamDescriptor = NaN(1, Data.RTH.N, 'single');
% BeamAlreadyDefined     = false(1, Data.RTH.N);

%{
    position = ftell(fid);
    fseek(fid, position, 'bof');
%}

Buffer = fread(fid, Data.RTH.DataFieldSize * Data.RTH.N, 'uint8=>uint8');
if feof(fid)
    return
end

%{
%% Avant test am�lioration performances JMA le 11/03/2017

id = 0;
for k=1:Data.RTH.N
	BeamDescriptor = typecast(Buffer(id + (1:2)), 'uint16');
    id = id + 2;

    BeamDescriptor = BeamDescriptor + 1;
    
	Y = typecast(Buffer(id + (1:8)), 'single');
    id = id + 8;
    
    DetectionPoint = Y(1);
    RxAngle        = Y(2);

	Y = typecast(Buffer(id + (1:8)), 'uint32');
    id = id + 8;

    Flags   = Y(1);
    Quality = Y(2);
    
	Uncertainty = typecast(Buffer(id + (1:4)), 'single');
    id = id + 4;
    
    if BeamAlreadyDefined(BeamDescriptor)
        if Uncertainty < Data.RD.Uncertainty(k)
            Data.RD.BeamDescriptor(k)       = BeamDescriptor;
            Data.RD.DetectionPoint(k)       = DetectionPoint;
            Data.RD.RxAngle(k)              = RxAngle;
            Data.RD.Flags(k)                = Flags;
            Data.RD.Quality(k)              = Quality;
            Data.RD.Uncertainty(k)          = Uncertainty;
        end
    else
        Data.RD.BeamDescriptor(k)           = BeamDescriptor;
        Data.RD.DetectionPoint(k)           = DetectionPoint;
        Data.RD.RxAngle(k)                  = RxAngle;
        Data.RD.Flags(k)                    = Flags;
        Data.RD.Quality(k)                  = Quality;
        Data.RD.Uncertainty(k)              = Uncertainty;
        BeamAlreadyDefined(BeamDescriptor)  = true;
    end
end
%}

%% Test am�lioration performances JMA le 11/03/2017

% TODO : manque la gestion BeamAlreadyDefined, test uncertainty, etc ...

id = 0;
offsetBeams = (0:(Data.RTH.N-1)) * Data.RTH.DataFieldSize;

% BeamDescriptor
nbBytes = 2;
subBytes = bsxfun(@plus, repmat(offsetBeams, nbBytes,1), (1:nbBytes)'); % Pour la R2016a
% subBytes = repmat(offsetBeams, nbBytes,1) + (1:nbBytes)'; % OK � partir de la version R2016b 
subBytes = id + subBytes(:)';
N_BeamDescriptor = typecast(Buffer(subBytes), 'uint16');
N_BeamDescriptor = N_BeamDescriptor + 1;
id = id + nbBytes;
% FigUtils.createSScFigure; PlotUtils.createSScPlot(N_BeamDescriptor, '*'); grid on; hold on; PlotUtils.createSScPlot(Data.RD.BeamDescriptor, 'or')

% DetectionPoint
nbBytes = 4;
subBytes = bsxfun(@plus, repmat(offsetBeams, nbBytes,1), (1:nbBytes)'); % Pour la R2016a
% subBytes = repmat(offsetBeams, nbBytes,1) + (1:nbBytes)'; % OK � partir de la version R2016b 
subBytes = id + subBytes(:)';
N_DetectionPoint = typecast(Buffer(subBytes), 'single');
id = id + nbBytes;
% FigUtils.createSScFigure; PlotUtils.createSScPlot(N_DetectionPoint, '*'); grid on; hold on; PlotUtils.createSScPlot(Data.RD.DetectionPoint, 'or');

% RxAngle
nbBytes = 4;
subBytes = bsxfun(@plus, repmat(offsetBeams, nbBytes,1), (1:nbBytes)'); % Pour la R2016a
% subBytes = repmat(offsetBeams, nbBytes,1) + (1:nbBytes)'; % OK � partir de la version R2016b 
subBytes = id + subBytes(:)';
N_RxAngle = typecast(Buffer(subBytes), 'single');
id = id + nbBytes;
% FigUtils.createSScFigure; PlotUtils.createSScPlot(N_RxAngle, '*'); grid on; hold on; PlotUtils.createSScPlot(Data.RD.RxAngle, 'or');

% Flags
nbBytes = 4;
subBytes = bsxfun(@plus, repmat(offsetBeams, nbBytes,1), (1:nbBytes)'); % Pour la R2016a
% subBytes = repmat(offsetBeams, nbBytes,1) + (1:nbBytes)'; % OK � partir de la version R2016b 
subBytes = id + subBytes(:)';
N_Flags = typecast(Buffer(subBytes), 'uint32');
id = id + nbBytes;
% FigUtils.createSScFigure; PlotUtils.createSScPlot(N_Flags, '*'); grid on; hold on; PlotUtils.createSScPlot(Data.RD.Flags, 'or');

% Quality
nbBytes = 4;
subBytes = bsxfun(@plus, repmat(offsetBeams, nbBytes,1), (1:nbBytes)'); % Pour la R2016a
% subBytes = repmat(offsetBeams, nbBytes,1) + (1:nbBytes)'; % OK � partir de la version R2016b 
subBytes = id + subBytes(:)';
N_Quality = typecast(Buffer(subBytes), 'uint32');
id = id + nbBytes;
% FigUtils.createSScFigure; PlotUtils.createSScPlot(N_Quality, '*'); grid on; hold on; PlotUtils.createSScPlot(Data.RD.Quality, 'or');

% Uncertainty
nbBytes = 4;
subBytes = bsxfun(@plus, repmat(offsetBeams, nbBytes,1), (1:nbBytes)'); % Pour la R2016a
% subBytes = repmat(offsetBeams, nbBytes,1) + (1:nbBytes)'; % OK � partir de la version R2016b 
subBytes = id + subBytes(:)';
N_Uncertainty = typecast(Buffer(subBytes), 'single');
id = id + nbBytes; %#ok<NASGU>
% FigUtils.createSScFigure; PlotUtils.createSScPlot(N_Uncertainty, '*'); grid on; hold on; PlotUtils.createSScPlot(Data.RD.Uncertainty, 'or');

% Signal strength of detection point

if Data.RTH.DataFieldSize == 26
    % TODO
end

Data.RD.BeamDescriptor = N_BeamDescriptor;
Data.RD.DetectionPoint = N_DetectionPoint;
Data.RD.RxAngle        = N_RxAngle;
Data.RD.Flags          = N_Flags;
Data.RD.Quality        = N_Quality;
Data.RD.Uncertainty    = N_Uncertainty;

% Buffer = ftell(fid);
% fseek(fid, Buffer, 'bof');

% if DRF.OptionalDataOffset == 0
%     Data.OD = [];
% else

%% Read Optional Data

if DRF.OptionalDataOffset ~= 0

    %{
    position = ftell(fid);
    fseek(fid, position, 'bof');
    %}
    
    Buffer = fread(fid, 45 + 20*Data.RTH.N, 'uint8=>uint8');
%     Buffer = fread(fid, 45, 'uint8=>uint8'); % TODO
    if feof(fid)
        return
    end
    id = 0;
    
    Data.OD.Frequency = typecast(Buffer(id + (1:4)), 'single');
    id = id + 4;
    
    Data.OD.Latitude = typecast(Buffer(id + (1:8)), 'double');
    id = id + 8;
    
    Data.OD.Longitude = typecast(Buffer(id + (1:8)), 'double');
    id = id + 8;
    
    Data.OD.Heading = typecast(Buffer(id + (1:4)), 'single');
    id = id + 4;
    
    Data.OD.HeightSource = Buffer(id + 1);
    id = id + 1;
    
    Y = typecast(Buffer(id + (1:4*5)), 'single');
    Data.OD.Tide         = Y(1);
    Data.OD.Roll         = Y(2);
    Data.OD.Pitch        = Y(3);
    Data.OD.Heave        = Y(4);
    Data.OD.VehicleDepth = Y(5);
    id = id + 20;

    Data.OD.BeamDepth               = NaN(1, Data.RTH.N, 'single');
    Data.OD.BeamAlongTrackDistance  = NaN(1, Data.RTH.N, 'single');
    Data.OD.BeamAcrossTrackDistance = NaN(1, Data.RTH.N, 'single');
    Data.OD.PointingAngle           = NaN(1, Data.RTH.N, 'single');
    Data.OD.AzimuthAngle            = NaN(1, Data.RTH.N, 'single');

    %{
    position = ftell(fid);
    fseek(fid, position, 'bof');
    %}
    Y = typecast(Buffer(id + (1:(4*5*Data.RTH.N))), 'single');
%     Y = fread(fid, [5 Data.RTH.N], 'uint8=>uint8'); % TODO
%   Data.OD.BeamDepth = Y(:,1); % TODO

%{
    % Avant
    for k=1:Data.RTH.N
        k1 = (k-1) * 5;
        Data.OD.BeamDepth(k)               = Y(k1 + 1);
        Data.OD.BeamAlongTrackDistance(k)  = Y(k1 + 2);
        Data.OD.BeamAcrossTrackDistance(k) = Y(k1 + 3);
        Data.OD.PointingAngle(k)           = Y(k1 + 4);
        Data.OD.AzimuthAngle(k)            = Y(k1 + 5);
    end
    %}
 
    %% Test am�lioration performances JMA le 11/03/2017

    subBeams = (0:(Data.RTH.N-1)) * 5;
    N_BeamDepth = Y(1 + subBeams);
    %     FigUtils.createSScFigure; PlotUtils.createSScPlot(Data.OD.BeamDepth, '*'); grid on; hold on; PlotUtils.createSScPlot(N_BeamDepth, 'or')
    N_BeamAlongTrackDistance = Y(2 + subBeams);
    %     FigUtils.createSScFigure; PlotUtils.createSScPlot(Data.OD.BeamAlongTrackDistance, '*'); grid on; hold on; PlotUtils.createSScPlot(N_BeamAlongTrackDistance, 'or')
    N_BeamAcrossTrackDistance = Y(3 + subBeams);
    %     FigUtils.createSScFigure; PlotUtils.createSScPlot(Data.OD.BeamAcrossTrackDistance, '*'); grid on; hold on; PlotUtils.createSScPlot(N_BeamAcrossTrackDistance, 'or')
    N_PointingAngle = Y(4 + subBeams);
    %     FigUtils.createSScFigure; PlotUtils.createSScPlot(Data.OD.PointingAngle, '*'); grid on; hold on; PlotUtils.createSScPlot(N_PointingAngle, 'or')
    N_AzimuthAngle = Y(5 + subBeams);
    %     FigUtils.createSScFigure; PlotUtils.createSScPlot(Data.OD.AzimuthAngle, '*'); grid on; hold on; PlotUtils.createSScPlot(N_AzimuthAngle, 'or')
    
    Data.OD.BeamDepth               = N_BeamDepth;
    Data.OD.BeamAlongTrackDistance  = N_BeamAlongTrackDistance;
    Data.OD.BeamAcrossTrackDistance = N_BeamAcrossTrackDistance;
    Data.OD.PointingAngle           = N_PointingAngle;
    Data.OD.AzimuthAngle            = N_AzimuthAngle;

    
%     id = id + 20*Data.RTH.N;
    Data.OD.Heading       = Data.OD.Heading       * rad2deg;
    Data.OD.Longitude     = Data.OD.Longitude     * rad2deg;
    Data.OD.Latitude      = Data.OD.Latitude      * rad2deg;
    Data.OD.Roll          = Data.OD.Roll          * rad2deg;
    Data.OD.Pitch         = Data.OD.Pitch         * rad2deg;
    Data.OD.PointingAngle = Data.OD.PointingAngle * rad2deg;
    Data.OD.AzimuthAngle  = Data.OD.AzimuthAngle  * rad2deg;
    Data.OD.Frequency     = Data.OD.Frequency     * 1e-3;
    Data.OD.VehicleDepth  = -Data.OD.VehicleDepth;
end

%{
figure(97686); 
subplot(2,1,1); plot(Data.OD.BeamAcrossTrackDistance, Data.OD.BeamDepth, '-*'); grid on
subplot(2,1,2); plot(Data.OD.PointingAngle, Data.OD.BeamDepth, '-*'); grid on
%}

flag = 1;

%{
FigUtils.createSScFigure;
h(1) = subplot(6,1,1); PlotUtils.createSScPlot(Data.RD.BeamDescriptor, '*'); grid on;
h(2) = subplot(6,1,2); PlotUtils.createSScPlot(Data.RD.DetectionPoint, '*'); grid on;
h(3) = subplot(6,1,3); PlotUtils.createSScPlot(Data.RD.RxAngle, '*'); grid on;
h(4) = subplot(6,1,4); PlotUtils.createSScPlot(Data.RD.Flags, '*'); grid on;
h(5) = subplot(6,1,5); PlotUtils.createSScPlot(Data.RD.Quality, '*'); grid on;
h(6) = subplot(6,1,6); PlotUtils.createSScPlot(Data.RD.Uncertainty, '*'); grid on;
linkaxes(h,'x')
%}
