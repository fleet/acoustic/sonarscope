% Description
%   Reads S7K datagram 7030 (SonarInstallation Parameters)
%
% Syntax
%   [flag, Data] = fread_RecordType_7030(fid)
%
% Input Arguments
%   fid : fid of the S7K file
%
% Output Arguments
%   flag : 0=KO, 1=OK
%   Data : Structure (see example)
%
% Examples
%   [flag, Data] = fread_RecordType_7030(fid);
%   % Data = 
%   %     RTH: [1�1 struct]
%   %      RD: []
%   %      OD: []   
%   % Data.RTH
%   %                         Frequency: 396000
%   %         LengthFirmwareVersionInfo: 12
%   %               FirmwareVersionInfo: 'Version x.xx                                                                                                                    '
%   %         LengthSoftwareVersionInfo: 7
%   %               SoftwareVersionInfo: '3,7,0,8                                                                                                                         '
%   %       Length7ksoftwareVersionInfo: 12
%   %             s7koftwareVersionInfo: 'Version z.zz                                                                                                                    '
%   %          LengthRecordProtocolInfo: 4
%   %                RecordProtocolInfo: '0.54                                                                                                                            '
%   %                    TransmitArrayX: -0.920000016689301
%   %                    TransmitArrayY: 0.790000021457672
%   %                    TransmitArrayZ: -0.027000000700355
%   %                 TransmitArrayRoll: 0.010471975430846
%   %                TransmitArrayPitch: 0
%   %              TransmitArrayHeading: 0
%   %                     ReceiveArrayX: -0.920000016689301
%   %                     ReceiveArrayY: 0.550999999046326
%   %                     ReceiveArrayZ: -0.050999999046326
%   %                  ReceiveArrayRoll: 0.010471975430846
%   %                 ReceiveArrayPitch: 0
%   %               ReceiveArrayHeading: 0
%   %                     MotionSensorX: 0
%   %                     MotionSensorY: 0
%   %                     MotionSensorZ: 0
%   %       MotionSensorRollCalibration: 0
%   %      MotionSensorPitchCalibration: 0
%   %    MotionSensorHeadingCalibration: 0
%   %             MotionSensorTimeDelay: 25
%   %                   PositionSensorX: 0
%   %                   PositionSensorY: 0
%   %                   PositionSensorZ: 0
%   %           PositionSensorTimeDelay: 0
%   %           WaterLineVerticalOffset: 0
%
% Authors : JMA 
% See also cre_ficIndex, fread_DRF 
%
% Reference pages in Help browser
%   <a href="matlab:doc fread_RecordType_7030">fread_RecordType_7030</a>
%-------------------------------------------------------------------------------

function [flag, Data] = fread_RecordType_7030(fid)

flag = 0;
Data = [];

%% Read Record Type Header

Data.RTH.Frequency = fread(fid, 1, 'single');
if feof(fid)
    return
end

Data.RTH.LengthFirmwareVersionInfo = fread(fid, 1, 'uint16');
if feof(fid)
    return
end

Data.RTH.FirmwareVersionInfo = fread(fid, [1 128], '*char');
% char(Data.RTH.FirmwareVersionInfo');
if feof(fid)
    return
end

Data.RTH.LengthSoftwareVersionInfo = fread(fid, 1, 'uint16');
if feof(fid)
    return
end

Data.RTH.SoftwareVersionInfo = fread(fid, [1 128], '*char');
% char(Data.RTH.SoftwareVersionInfo')
if feof(fid)
    return
end

Data.RTH.Length7ksoftwareVersionInfo = fread(fid, 1, 'uint16');
if feof(fid)
    return
end

Data.RTH.s7koftwareVersionInfo = fread(fid, [1 128], '*char');
% char(Data.RTH.s7koftwareVersionInfo')
if feof(fid)
    return
end

Data.RTH.LengthRecordProtocolInfo = fread(fid, 1, 'uint16');
if feof(fid)
    return
end

Data.RTH.RecordProtocolInfo = fread(fid, [1 128], '*char');
% char(Data.RTH.RecordProtocolInfo')
if feof(fid)
    return
end

X = fread(fid, 18, 'single');
if feof(fid)
    return
end

Data.RTH.TransmitArrayX                 = X(1);
Data.RTH.TransmitArrayY                 = X(2);
Data.RTH.TransmitArrayZ                 = X(3);
Data.RTH.TransmitArrayRoll              = X(4);
Data.RTH.TransmitArrayPitch             = X(5);
Data.RTH.TransmitArrayHeading           = X(6);
Data.RTH.ReceiveArrayX                  = X(7);
Data.RTH.ReceiveArrayY                  = X(8);
Data.RTH.ReceiveArrayZ                  = X(9);
Data.RTH.ReceiveArrayRoll               = X(10);
Data.RTH.ReceiveArrayPitch              = X(11);
Data.RTH.ReceiveArrayHeading            = X(12);
Data.RTH.MotionSensorX                  = X(13);
Data.RTH.MotionSensorY                  = X(14);
Data.RTH.MotionSensorZ                  = X(15);
Data.RTH.MotionSensorRollCalibration    = X(16);
Data.RTH.MotionSensorPitchCalibration   = X(17);
Data.RTH.MotionSensorHeadingCalibration = X(18);

Data.RTH.MotionSensorTimeDelay = fread(fid, 1, 'uint16');
if feof(fid)
    return
end

X = fread(fid, 3, 'single');
if feof(fid)
    return
end

Data.RTH.PositionSensorX = X(1);
Data.RTH.PositionSensorY = X(2);
Data.RTH.PositionSensorZ = X(3);

Data.RTH.PositionSensorTimeDelay = fread(fid, 1, 'uint16');
if feof(fid)
    return
end

Data.RTH.WaterLineVerticalOffset = fread(fid, 1, 'single');

%% Read Record Data

Data.RD = [];

%% Read Optional Data

Data.OD = [];

flag = 1;
