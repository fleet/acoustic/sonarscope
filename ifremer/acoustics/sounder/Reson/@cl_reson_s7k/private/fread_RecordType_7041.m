% Description
%   Reads S7K datagram 7041 (Compressed Beamformed Magnitude Data == Water Column Data)
%
% Syntax
%   [flag, Data] = fread_RecordType_7041(fid)
%
% Input Arguments
%   fid : fid of the S7K file
%
% Output Arguments
%   flag : 0=KO, 1=OK
%   Data : Structure (see example)
%
% Examples
%   [flag, Data] = fread_RecordType_7041(fid);
%   % Data = 
%   %     RTH: [1�1 struct]
%   %      RD: [1�1 struct]
%   %      OD: []   
%   % Data.RTH
%   %              SonarId: 0
%   %          PingCounter: 10633
%   %    MultiPingSequence: 2
%   %                    N: 880
%   %                Flags: 13
%   %           SampleRate: 66.178924560546875
%   %           SampleRate: 66.178924560546875
%   %           BeamNumber: [1�880 single]
%   %            BeamAngle: [1�880 single]
%   % Data.RD
%   %    NbSamples: [1�880 double]
%   %    Amplitude: [549�880 single]
%
% Authors : JMA 
% See also cre_ficIndex, fread_DRF 
%
% Reference pages in Help browser
%   <a href="matlab:doc fread_RecordType_7041">fread_RecordType_7041</a>
%-------------------------------------------------------------------------------

function [flag, Data] = fread_RecordType_7041(fid, DRF, varargin)

[varargin, NoRawData]       = getFlag(varargin, 'NoRawData');
[varargin, ConserveValZero] = getFlag(varargin, 'ConserveValueZero'); %#ok<ASGLU>

Data.OD = [];
flag    = 1;
sizeDRF = DRF.Size;

%% Read Record Type Header

Data.RTH.SonarId = fread(fid, 1, 'uint64');
if feof(fid)
    flag = 0;
    return
end

Data.RTH.PingCounter = fread(fid, 1, 'uint32');
if feof(fid)
    flag = 0;
    return
end

X = fread(fid, 3, 'uint16');
if feof(fid)
    flag = 0;
    return
end

Data.RTH.MultiPingSequence = X(1);
Data.RTH.MultiPingSequence = reversePingSequence(Data.RTH.MultiPingSequence);
Data.RTH.N = X(2);

if Data.RTH.N > 880
    flag = 0;
    return
end

Data.RTH.Flags = X(3);

DataSize                 = bitand(Data.RTH.Flags, 3);
DownSamplingMethod       = bitand(bitshift(Data.RTH.Flags, -2), 3); %#ok<NASGU>
FilteringMethod          = bitand(bitshift(Data.RTH.Flags, -4), 7); %#ok<NASGU>
BeamIdentificationMethod = bitand(bitshift(Data.RTH.Flags, -7), 1);

Data.RTH.SampleRate = fread(fid, 1, 'single');
if feof(fid)
    flag = 0;
    return
end

X = fread(fid, 4, 'uint32');  %#ok<NASGU>
if feof(fid)
    flag = 0;
    return
end

if NoRawData
    return
end

Data.RTH.BeamNumber = NaN(1, Data.RTH.N, 'single');
Data.RTH.BeamAngle  = NaN(1, Data.RTH.N, 'single');
Amplitude           = NaN(1, Data.RTH.N, 'single');

%% Read Record Data

Data.RD.NbSamples   = zeros(1, Data.RTH.N);

% Cas particulier mais le plus r�pandu : BeamNumber et DataSize = 0==> Samples en uint16
% Pour recherche d'optimisation
if (BeamIdentificationMethod == 0) && (DataSize == 1)
    % Taille du buffer de donn�e significatives =
    % Taille du datagramme - Taille paquet DRF - Taille RTH - Taille CheckSum.
    sizeBuffer = sizeDRF - 64 - 38 - 4;
    % Lecture en int16 pour am�liorer la vitesse (�viter le typecast sur
    % Buffer).
    buffer = fread(fid, sizeBuffer, 'int16=>int16');
    
    %{
id          = 1; % L'unit� de lecture est deux octets (int16)
for k=1:Data.RTH.N
Data.RTH.BeamNumber(k)  = uint16(buffer(id));
id                      = id + 1;

Data.RD.NbSamples(k)    = typecast(buffer(id:id+1), 'uint32');
id                      = id + 2;
sub                     = 1:Data.RD.NbSamples(k);
subBuf                  = sub + (id-1);

Amplitude(sub, k)       = buffer(subBuf) + 25;
id                      = id + Data.RD.NbSamples(k);
end
    %}
    
    id = 1; % L'unit� de lecture est deux octets (int16)
    for k=1:Data.RTH.N
        if (id-1) <= sizeBuffer
            Data.RTH.BeamNumber(k) = uint16(buffer(id));
            id                     = id + 1;
            Data.RD.NbSamples(k)   = typecast(buffer(id:id+1), 'uint32');
            id                     = id + 2 + Data.RD.NbSamples(k);
        else
%             id;
            Data.RD.NbSamples((k-1):end) = 0;
        end
    end
    Amplitude = NaN(max(Data.RD.NbSamples), Data.RTH.N, 'single');
    id = 1; % L'unit� de lecture est deux octets (int16)
    for k=1:Data.RTH.N
        sub               = 1:Data.RD.NbSamples(k);
        subBuf            = sub + (id + 2);
        Amplitude(sub, k) = buffer(subBuf) + 25;
        id                = id + 3 + Data.RD.NbSamples(k);
    end
    %{
% Tentative d'optimisation pour �viter la boucle mais la derni�re
instruction se r�v�le incompl�te.
cumTabNbSamples = cumsum(Data.RD.NbSamples, 2);
cum1            = [1 cumTabNbSamples+1];
cum1            = arrayfun(@(x, y) x + 3, cum1);
cum2            = diff(cum1);
cum1(end)       = [];

idxBuffer       = arrayfun(@(x, y) (x:x+y-1), cum1, cum2, 'UniformOutput',false);
newSamples      = arrayfun(@(x) double((buffer(x{:}) + 25)),idxBuffer, 'UniformOutput',false);
newRDSamples    = cell(1, Data.RTH.N);
% TODO : � finaliser , [newRDSamples{idxBuffer, 1}] = deal(newSamples);
    %}
    
else
    for k=1:Data.RTH.N
        if BeamIdentificationMethod == 0 % Beam Number
            Data.RTH.BeamNumber(k) = fread(fid, 1, 'uint16');
        else % Beam Angle
            Data.RTH.BeamAngle(k) = fread(fid, 1, 'single');
        end
        
        nbSamples = fread(fid, 1, 'uint32');
        Data.RD.NbSamples(k) = nbSamples;
        if DataSize == 0 % uint8
            X = fread(fid, nbSamples, 'uint8');
            if length(X) == nbSamples
                Amplitude(1:nbSamples, k) = X;
            else
                break
            end
        else % uint16
            X = fread(fid, nbSamples, 'int16');
            if length(X) == nbSamples
                Amplitude(1:nbSamples, k) = X + 25;
            else
                break
            end
        end
    end
end

% Rajout� le 15/01/2012
Amplitude(Amplitude > 10000) = NaN;
if ~ConserveValZero
    % Option ajout�e pour �viter l'�crasement des valeurs 0 dans le cas d'une conversion
    % vers MOVIES. On �vite de passer si l'option est contenue dans le varargin.
    Amplitude(Amplitude == 0) = NaN;
end
Data.RD.Amplitude = Amplitude;

%% Read Optional Data

Data.OD = [];
