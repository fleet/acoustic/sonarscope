% Compressed Water Column Data

function [flag, sData] = fread_RecordType_7042(fid, DRF, varargin)

[varargin, NoRawData]       = getFlag(varargin, 'NoRawData');
[varargin, ConserveValZero] = getFlag(varargin, 'ConserveValueZero'); %#ok<ASGLU>

sData.OD = [];
flag     = 0;
sizeDRF  = DRF.Size;

%% Read Record Type Header

sData.RTH.SonarId = fread(fid, 1, 'uint64');
if feof(fid)
    return
end

sData.RTH.PingCounter = fread(fid, 1, 'uint32');
if feof(fid)
    return
end

X = fread(fid, 2, 'uint16');
if feof(fid)
    return
end

sData.RTH.MultiPingSequence = X(1);
sData.RTH.MultiPingSequence = reversePingSequence(sData.RTH.MultiPingSequence);
sData.RTH.N                 = X(2); % Nb Beams

% Ancien verrou du datagramme 7041
%{
if Data.RTH.NBeams > 880
     flag = 0;
     return
end
%}

X = fread(fid, 3, 'uint32=>uint32');
if feof(fid)
    return
end
sData.RTH.NSamples          = X(1); % Nominal, based on range (indicatif)
sData.RTH.NZipSamples       = X(2);
sData.RTH.Flags             = X(3);
sData.RTH.FirstSample       = fread(fid,1,'uint32');
sData.RTH.SampleRate        = fread(fid,1,'single');
sData.RTH.CompressionFactor = fread(fid,1,'single');
sData.RTH.Reserved          = fread(fid,1,'uint32');

[flag, sDataRD, sampleSize, mag_fmt, phase_fmt] = get_R7042_flags(sData.RTH.Flags);

if NoRawData
    return
end

% Lecture des donn�es d'�chantillons
sData.RTH.BeamNumber = NaN(1, sData.RTH.N, 'single');
sData.RD.BeamAngle   = NaN(1, sData.RTH.N, 'single');
sData.RD.NSamples    = zeros(1, sData.RTH.N);

%% Read Record Data

B = sData.RTH.N;

% TODO, traiter les autres formats possibles de Samples (cf. Code Yoann Ladroit)
if sDataRD.magnitudeOnly && sampleSize == 2
    fmtSample = 'uint16';
end
% Taille du buffer de donn�e significatives =
% Taille du datagramme - Taille paquet DRF - Taille RTH - Taille CheckSum.
sizeBuffer = sizeDRF - 64 - 44 - 4;
% Lecture en uint8 pour am�liorer la vitesse .
buffer = fread(fid, sizeBuffer, 'uint8=>uint8');

% Le nb Samples effectif est lu par paquet. On lit le N Samples depuis le Buffer
% qui remplace le N nominal plus haut.
% Fichier de Pawel P. : 20190910_060150.s7K o� N ~= N Nominal
N = double(typecast(buffer(3:4), 'uint16')); % sData.RTH.NSamples;



% % Ajourt JMA le 19/05/2021 pour donn�es ODOM MB2 de chez Teledyne
if N == 0
    flag = 0;
    return
end



% Offset des n� de beams
sizeDescBeam = double((N * sampleSize) + 2 + 4);
% Offset des n� de faisceaux
offsetBeams  = (0:B-1)*(sizeDescBeam);

% BeamNumber
id      = 0;
nbBytes = 2; % Taille de la variable Beam
% Pour la R2016a subBytes        = bsxfun(@plus, repmat(offsetBeams, nbBytes,1), (1:nbBytes)'); 
% subBytes   = repmat(offsetBeams, nbBytes, 1) + (1:nbBytes)'; % OK � partir de la version R2016b  % Comment� par JMA le 18/02/2021
% subBytes   = id + subBytes(:)'; % Comment� par JMA le 18/02/2021
% beamNumber = typecast(buffer(subBytes), 'uint16'); % Comment� par JMA le 18/02/2021
% beamNumber = beamNumber + 1; % Comment� par JMA le 18/02/2021
% Avancement de la variable Beam
id = id + nbBytes;    

% N Samples
nbBytes = 4; % Taille de la variable N
% subBytes = repmat(offsetBeams, nbBytes,1) + (1:nbBytes)';  % Comment� par JMA le 18/02/2021
% subBytes = id + subBytes(:)'; % Comment� par JMA le 18/02/2021
% NSamples = typecast(buffer(subBytes), 'uint32'); % Comment� par JMA le 18/02/2021
% Avancement de la variable NSamples
id = id + nbBytes;    

% Samples
nbBytes  = sampleSize; % Taille d'un sample
subBytes = repmat(offsetBeams, nbBytes * N,1) + (1:nbBytes * N)';
subBytes = id + subBytes(:)';
try
    Amplitude = typecast(buffer(subBytes), fmtSample);
    Amplitude = reshape(Amplitude, [N B]);
    % figure; imagesc(Amplitude);
    
    N = sData.RTH.FirstSample - 1;
    Amplitude = singleUnlessDouble(Amplitude, NaN);
    sData.RD.Amplitude = [zeros(N,B, class(Amplitude)); Amplitude];
catch ME
    % Chercher pourquoi �a ne correspond pas sur fichier "G:\VLIZ\Norbit\SonarScope\0011_20210217_151016_noline.0001.nc"
    % alors que �a fonctionne sur fichier "G:\Norbit\20210218\20190312_134951.s7k"
    
    %     Amplitude = typecast(buffer, fmtSample);
    %     Amplitude(end+1:(N*B)) = NaN;
    %     Amplitude = reshape(Amplitude(1:(N*B)), [N B]);
    flag = 0;
    return
end


function [flag, sData, sample_size, mag_fmt, phase_fmt] = get_R7042_flags(rthFlags)
% Code de Yoann Ladroit (NIWA, 2019/11/21).
flag = 1;

if isnumeric(rthFlags)
    flag_bin = dec2bin(rthFlags, 32);
else
    flag_bin = rthFlags;
end
sample_size = 0;
mag_fmt     = '';
phase_fmt   = '';

sData.dataTruncatedBeyondBottom  = 0;
sData.magnitudeOnly              = 0;
sData.int8BitCompression         = 0;
sData.downsamplingDivisor        = 0;
sData.downsamplingType           = 0;
sData.int32BitsData              = 0;
sData.compressionFactorAvailable = 0;
sData.segmentNumbersAvailable    = 0;

% Bit 0 : Use maximum bottom detection point in each beam to
% limit data. Data is included up to the bottom detection point
% + 10%. This flag has no effect on systems which do not
% perform bottom detection.
sData.dataTruncatedBeyondBottom = bin2dec(flag_bin(32-0));

% Bit 1 : Include magnitude data only (strip phase)
sData.magnitudeOnly = bin2dec(flag_bin(32-1));

% Bit 2 : Convert mag to dB, then compress from 16 bit to 8 bit
% by truncation of 8 lower bits. Phase compression simply
% truncates lower (least significant) byte of phase data.
sData.int8BitCompression = bin2dec(flag_bin(32-2));

% Bit 3 : Reserved.
sData.Reserved = bin2dec(flag_bin(32-3));

% Bit 4-7 : Downsampling divisor. Value = (BITS >> 4). Only
% values 2-16 are valid. This field is ignored if downsampling
% is not enabled (type = �none�).
sData.downsamplingDivisor = bin2dec(flag_bin(32-7:32-4));

% Bit 8-11 : Downsampling type:
%             0x000 = None
%             0x100 = Middle value
%             0x200 = Peak value
%             0x300 = Average value
sData.downsamplingType = bin2dec(flag_bin(32-11:32-8));

% Bit 12: 32 Bits data
sData.int32BitsData = bin2dec(flag_bin(32-12));

% Bit 13: Compression factor available
sData.compressionFactorAvailable = bin2dec(flag_bin(32-13));

% Bit 14: Segment numbers available
sData.segmentNumbersAvailable = bin2dec(flag_bin(32-14));

% figure the size of a "sample" in bytes based on those flags
if sData.magnitudeOnly
    if sData.int32BitsData && ~sData.int8BitCompression
        % F) 32 bit Mag (32 bits total, no phase)
        sample_size = 4;
        mag_fmt = 'float32';
    elseif ~sData.int32BitsData && sData.int8BitCompression
        % D) 8 bit Mag (8 bits total, no phase)
        sample_size = 1;
        mag_fmt = 'int8';
    elseif ~sData.int32BitsData && ~sData.int8BitCompression
        % B) 16 bit Mag (16 bits total, no phase)
        sample_size = 2;
        mag_fmt = 'uint16';
    else
        % if both flags.int32BitsData and flags.int8BitCompression are
        % =1, then I am not quite sure how it would work given
        % how I understand the file format documentation.
        % Throw error if you ever get this case and look for
        % more information about data format...
        warning('%s: WC compression flag issue',fieldname);
    end
else
    if ~sData.int32BitsData && sData.int8BitCompression
        % C) 8 bit Mag & 8 bit Phase (16 bits total)
        sample_size = 2;
        phase_fmt = 'int8';
        mag_fmt = 'int8';
    elseif ~sData.int32BitsData && ~sData.int8BitCompression
        % A) 16 bit Mag & 16bit Phase (32 bits total)
        sample_size = 4;
        phase_fmt = 'int16';
        mag_fmt = 'uint16';
    else
        % Again, if both flags.int32BitsData and
        % flags.int8BitCompression are = 1, I don't know what the
        % result would be.
        % sample_size = 3;
        % phase_fmt = 'int8';
        % mag_fmt = 'int16';
        
        % There is another weird case: if flags.int32BitsData=1 and
        % flags.int8BitCompression=0, I would assume it would 32
        % bit Mab & 32 bit Phase (64 bits total), but that case
        % does not exist in the documentation. Instead you have
        % a case E) 32 bit Mag & 8 bit Phase (40 bits total),
        % which I don't understand could happen. Also, that
        % would screw the code as we read the data in bytes,
        % aka multiples of 8 bits. We would need to modify the
        % code to work per bit if we ever had such a case.
%         sample_size = 5;
%         phase_fmt = 'int8';
%         mag_fmt = 'int32';
        % Anyway, throw error if you ever get here and look for
        % more information about data format...
    end
end
