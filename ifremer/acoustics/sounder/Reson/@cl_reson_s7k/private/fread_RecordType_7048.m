% 7k Calibrated Beam Data

function [flag, Data] = fread_RecordType_7048(fid)

my_breakpoint % Jamais rencontré

flag = 0;
Data = [];

%% Read Record Type Header

Data.RTH.SonarId = fread(fid, 1, 'uint64');
if feof(fid)
    return
end

Data.RTH.PingCounter = fread(fid, 1, 'uint32');
if feof(fid)
    return
end

X = fread(fid, 3, 'uint16');
if feof(fid)
    return
end

Data.RTH.MultiPingSequence = X(1);
Data.RTH.MultiPingSequence = reversePingSequence(Data.RTH.MultiPingSequence);
Data.RTH.FirstBeam         = X(2);
Data.RTH.N                 = X(3);

if nargout == 1
    flag = 1;
    return
end

Data.RTH.S = fread(fid, 1, 'uint32');
if feof(fid)
    return
end

Reserved = fread(fid, 1, 'uint8'); %#ok<NASGU>
if feof(fid)
    return
end

Data.RTH.ErrorFlag = fread(fid, 1, 'uint8');
if feof(fid)
    return
end

switch Data.RTH.ErrorFlag
    case 0
        Data.RTH.strErrorFlag = 'OK';
    case 1
        Data.RTH.strErrorFlag = 'No calibration';
    case 2
        Data.RTH.strErrorFlag = 'TVG read error (R7010)';
    case 3
        Data.RTH.strErrorFlag = 'CTD not available (R1010)';
    case 4
        Data.RTH.strErrorFlag = 'Invalid or nor available geometry (R7004)';
    case 5
        Data.RTH.strErrorFlag = 'Invalid sonar specifications (XML)';
    case 6
        Data.RTH.strErrorFlag = 'Bottom detect failed';
    case 7
        Data.RTH.strErrorFlag = 'No power (Power is set to zero)';
    case 8
        Data.RTH.strErrorFlag = 'No gain (Gain is too low)';
    case 255
        Data.RTH.strErrorFlag = 'System cannot be calibrated (c7k file missing)';
    otherwise
        Data.RTH.strErrorFlag = 'Reserved for internal errors';
end

Reserved = fread(fid, 8, 'uint32'); %#ok<NASGU>
if feof(fid)
    return
end

%% Read Record Data

% P = ftell(fid)
% fseek(fid, P, 0);

Data.RD.Amplitude = fread(fid, [Data.RTH.S Data.RTH.N], 'single');
if feof(fid)
    return
end
Data.RD.Amplitude(Data.RD.Amplitude < -100) = NaN;
% SonarScope(Data.RD.Amplitude);

%% Read Optional Data

Data.OD = [];

flag = 1;