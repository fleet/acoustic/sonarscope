% 7k System Events

function [flag, Data] = fread_RecordType_7050(fid)

flag = 0;
Data = [];

%Lecture de l'ent�te de la FRAME.
Data.RTH.SonarId = fread(fid, 1, 'uint64');
if feof(fid)
    return
end

Data.RTH.Events = fread(fid, 1, 'uint32');
if feof(fid)
    return
end

%Lecture des Data de la FRAME.
for k=1:Data.RTH.Events
    X = fread(fid, 2, 'uint16');
    if feof(fid)
        flag = 0;
        return
    end
    
    Data.RD.EventType(k)       = X(1);
    Data.RD.EventIdentifier(k) = X(2);
    
    Data.RD.DeviceIdentifier(k) = fread(fid, 1, 'uint32');
    if feof(fid)
        flag = 0;
        return
    end
    
    X = fread(fid, 2, 'uint16');
    if feof(fid)
        flag = 0;
        return
    end    
    
    Data.RD.SystemEnumerator(k)   = X(1);
    Data.RD.EventMessageLength(k) = X(2);
    
    Data.RD.s7KTime(k) = fread(fid, 10, 'char');
    if feof(fid)
        flag = 0;
        return
    end
    Data.RD.EventMessage(k) = fread(fid, Data.RD.EventMessageLength(k), 'char');
end

%Lecture des Optional Data de la FRAME.
Data.OD = [];

flag = 1;