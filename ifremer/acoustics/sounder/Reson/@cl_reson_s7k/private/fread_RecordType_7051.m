% 7k System Event Message

function [flag, Data] = fread_RecordType_7051(fid)

my_breakpoint % Jamais rencontré

flag = 0;
Data = [];

%% Read Record Type Header

Data.RTH.SonarId = fread(fid, 1, 'uint64');
if feof(fid)
    return
end

Data.RTH.EventId = fread(fid, 1, 'uint16');
if feof(fid)
    return
end

X = fread(fid, 2, 'uint16');
if feof(fid)
    return
end

Data.RTH.MessageLength   = X(1);
Data.RTH.EventIdentifier = X(2);

%% Read Record Data

Data.RD.EventMessage = fread(fid, Data.RTH.MessageLength, 'char');

%% Read Optional Data

Data.OD = [];

flag = 1;