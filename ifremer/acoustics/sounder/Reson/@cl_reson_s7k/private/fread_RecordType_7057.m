% Calibrated Backscatter Imagery Data

function [flag, PingNumber, Sequence, Data] = fread_RecordType_7057(fid)

my_breakpoint % Jamais rencontré

flag = 0;
Data = [];

%% Read Record Type Header

Data.RTH.SonarId = fread(fid, 1, 'uint64');
if feof(fid)
    return
end

Data.RTH.PingCounter = fread(fid, 1, 'uint32');
if feof(fid)
    return
end

Data.RTH.MultiPingSequence = fread(fid, 1, 'uint16');
if feof(fid)
    return
end
Data.RTH.MultiPingSequence = reversePingSequence(Data.RTH.MultiPingSequence);

Data.RTH.BeamPosition = fread(fid, 1, 'single');
if feof(fid)
    return
end

X = fread(fid, 2, 'uint32');
if feof(fid)
    return
end
Data.RTH.ControlFlags = X(1);
Data.RTH.S            = X(2);

X = fread(fid, 8, 'single');
if feof(fid)
    return
end
Data.RTH.Port_3dBBeamWithY            = X(1);
Data.RTH.Port_3dBBeamWithZ            = X(2);
Data.RTH.Startboard_3dBBeamWithY      = X(3);
Data.RTH.Startboard_3dBBeamWithZ      = X(4);
Data.RTH.PortBeamSteeringWithY        = X(5);
Data.RTH.PortBeamSteeringWithZ        = X(6);
Data.RTH.StartboardBeamSteeringAngleY = X(7);
Data.RTH.StartboardBeamSteeringAngleZ = X(8);

X = fread(fid, 2, 'uint16');
if feof(fid)
    return
end
Data.RTH.N = X(1);

PingNumber = Data.RTH.PingCounter;
Sequence   = Data.RTH.MultiPingSequence;
if nargout == 3
    flag = 1;
    return
end


Data.RTH.CurrentBeamNumber = X(2);

Data.RTH.W = fread(fid, 1, 'char');
if feof(fid)
    return
end

X = fread(fid, 2, 'uint8');
if feof(fid)
    return
end
Data.RTH.DataTypes = X(1);
Data.RTH.ErrorFlag = X(2);

%% Read Record Data

if Data.RTH.DataTypes == 0
    switch Data.RTH.W
        case 4
            Data.RD.PortBeamAmplitude = fread(fid, Data.RTH.S, 'single');
            if feof(fid)
                flag = 0;
                return
            end
            % figure; plot(Data.RD.PortBeamAmplitude); grid on
            Data.RD.StartboardBeamAmplitude = fread(fid, Data.RTH.S, 'single');
            Data.RD.PortRxBeamIndex         = fread(fid, Data.RTH.S, 'uint16');
            Data.RD.StartboardRxBeamIndex   = fread(fid, Data.RTH.S, 'uint16');
        otherwise
            my_warndlg('Beurk : fread_RecordType_7057', 1);
    end
else
    switch Data.RTH.W
        case 1
        case 2
            Data.RD.PortBeamPhase = fread(fid, Data.RTH.S, 'uint16');
            if feof(fid)
                flag = 0;
                return
            end
            Data.RD.StartboardBeamPhase   = fread(fid, Data.RTH.S, 'uint16');
            Data.RD.PortRxBeamIndex       = fread(fid, Data.RTH.S, 'uint16');
            Data.RD.StartboardRxBeamIndex = fread(fid, Data.RTH.S, 'uint16');
% figure; plot([Data.RD.PortBeamPhase ; Data.RD.StartboardBeamPhase], '*'); grid on;%
% figure; plot([Data.RD.PortRxBeamIndex ; Data.RD.StartboardRxBeamIndex], '*'); grid on;
        otherwise
            my_warndlg('Beurk : fread_RecordType_7057', 1);
    end
end

if feof(fid)
    return
end

%% Read Optional Data

Data.OD.Frequency = fread(fid, 1, 'single') / 1000;
if feof(fid)
    return
end

X = fread(fid, 2, 'double');
if feof(fid)
    return
end
rad2deg = 180/pi;
Data.OD.Latitude  = X(1) * rad2deg;
Data.OD.Longitude = X(2) * rad2deg;

flag = 1;