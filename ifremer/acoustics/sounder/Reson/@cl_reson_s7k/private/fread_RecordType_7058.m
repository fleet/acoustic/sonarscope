% Description
%   Reads S7K datagram 7058 (Calibrated Snippet Data)
%
% Syntax
%   [flag, PingNumber, Sequence, Data] = fread_RecordType_7058(fid)
%
% Input Arguments
%   fid : fid of the S7K file
%
% Output Arguments
%   flag       : 0=KO, 1=OK
%   PingNumber : 
%   Sequence   : 
%   Data       : Structure (see example)
%
% Examples
%   [flag, PingNumber, Sequence, Data] = fread_RecordType_7058(fid);
%   % Data = 
%   %     RTH: [1�1 struct]
%   %      RD: []
%   %      OD: []   
%   % Data.RTH
%   %                SonarId: 1195
%   %            PingCounter: 191671
%   %      MultiPingSequence: 0
%   %                      N: 256
%   %    ForwardLookingSonar: 0
%   %           ControlFlags: 64
%   %               Reserved: [7�1 double]
%   % Data.RD
%   %           BeamDescriptor: [1�256 double]
%   %    BeginSampleDescriptor: [1�256 double]
%   %       BottomDetectSample: [1�256 double]
%   %      EndSampleDescriptor: [1�256 double]
%   %                  Samples: [1�25600 single]
%
% Authors : JMA 
% See also cre_ficIndex, fread_DRF 
%
% Reference pages in Help browser
%   <a href="matlab:doc fread_RecordType_7058">fread_RecordType_7058</a>
%-------------------------------------------------------------------------------

function [flag, PingNumber, Sequence, Data] = fread_RecordType_7058(fid, DRF, NbBeamsBathy, varargin)

% Pos = ftell(fid);
% fseek(fid, Pos, 'bof');

flag = 0;
Data = [];

rad2pi = 180/pi;

%% Read Record Type Header

Data.RTH.SonarId = fread(fid, 1, 'uint64');
if feof(fid)
    return
end

Data.RTH.PingCounter = fread(fid, 1, 'uint32');
if feof(fid)
    return
end

X = fread(fid, 2, 'uint16');
if feof(fid)
    return
end

Data.RTH.MultiPingSequence = X(1);
Data.RTH.MultiPingSequence = reversePingSequence(Data.RTH.MultiPingSequence);
Data.RTH.N                 = X(2);

PingNumber = Data.RTH.PingCounter;
Sequence   = Data.RTH.MultiPingSequence;
if nargout == 3
    flag = 1;
    return
end

Data.RTH.ForwardLookingSonar = fread(fid, 1, 'uint8');
if feof(fid)
    return
end

%% Read Record Data

X = fread(fid, 8, 'uint32');
if feof(fid)
    return
end

Data.RTH.ControlFlags = X(1);
Data.RTH.Reserved = X(2:8);

if isempty(NbBeamsBathy)
    NbBeamsBathy = 1;
end
%Lecture des Data de la FRAME.
% indBeam                       = NaN(1,NbBeamsBathy);
Data.RD.BeamDescriptor        = NaN(1,NbBeamsBathy);
Data.RD.BeginSampleDescriptor = NaN(1,NbBeamsBathy);
Data.RD.BottomDetectSample    = NaN(1,NbBeamsBathy);
Data.RD.EndSampleDescriptor   = NaN(1,NbBeamsBathy);
% NbSamples = 0;

%{
position = ftell(fid);
fseek(fid, position, 'bof');
%}
Buffer = fread(fid, 14 * Data.RTH.N,  'uint8=>uint8');
if feof(fid)
    return
end

%{
%% Avant test am�lioration performances JMA le 11/03/2017

id = 0;
for k=1:Data.RTH.N
k2 = typecast(Buffer(id + (1:2)), 'uint16') + 1;
id = id + 2;

indBeam(k) = k2;
Data.RD.BeamDescriptor(k2) = k2;

X = typecast(Buffer(id + (1:12)), 'uint32');
id = id + 12;

Data.RD.BeginSampleDescriptor(k2) = X(1);
Data.RD.BottomDetectSample(k2)    = X(2);
Data.RD.EndSampleDescriptor(k2)   = X(3);

NbSamples = NbSamples + Data.RD.EndSampleDescriptor(k2) - Data.RD.BeginSampleDescriptor(k2) + 1;
end
%}

%% Test am�lioration performances JMA le 11/03/2017

id = 0;
offsetBeams = (0:(Data.RTH.N-1)) * 14;

% indBeam
nbBytes = 2;
subBytes = bsxfun(@plus, repmat(offsetBeams, nbBytes,1), (1:nbBytes)'); % Pour la R2016a
% subBytes = repmat(offsetBeams, nbBytes,1) + (1:nbBytes)'; % OK � partir de la version R2016b
subBytes = id + subBytes(:)';
N_indBeam = typecast(Buffer(subBytes), 'uint16');
N_indBeam = N_indBeam + 1;

% Rajout� par JMA le 15/05/2017 pour fichier HERMINE 20170410_025416_PP_7150_12kHz.s7k
if any(N_indBeam > 880)% || any(N_indBeam < 0)
    [varargin, nomFicIn] = getPropertyValue(varargin, 'Filename', []); %#ok<ASGLU>
    str1 = sprintf('Nb of beams = %d, ne devrait pas �tre > � 880', max(N_indBeam ));
    str2 = sprintf('Nb of beams = %d, should not be > to 880', max(N_indBeam ));
    fclose(fid);
    renameBadFile(nomFicIn, 'Message', Lang(str1,str2))
    flag = 0;
    return
end

id = id + nbBytes;
% FigUtils.createSScFigure; PlotUtils.createSScPlot(N_indBeam, '*'); grid on; hold on; PlotUtils.createSScPlot(indBeam, 'or')

nbBytes  = 4;
nbBytes2 = 3 * nbBytes; % 3 champs de 4 octets.
subBytes = bsxfun(@plus, repmat(offsetBeams, nbBytes2,1), (1:nbBytes2)'); % Pour la R2016a
subBytes = reshape(subBytes, nbBytes, 3, size(subBytes, 2));
subBytes = id + subBytes(:)';
% Un seul appel � typecast
bufMultiField           = typecast(Buffer(subBytes), 'uint32');
bufMultiField           = reshape(bufMultiField, 3, Data.RTH.N);
N_BeginSampleDescriptor = bufMultiField(1,:)';
N_BottomDetectSample    = bufMultiField(2,:)';
N_EndSampleDescriptor   = bufMultiField(3,:)';
%{
FigUtils.createSScFigure; PlotUtils.createSScPlot(N_BeginSampleDescriptor, '*'); grid on; hold on; PlotUtils.createSScPlot(Data.RD.BeginSampleDescriptor(N_indBeam), 'or')
FigUtils.createSScFigure; PlotUtils.createSScPlot(N_BottomDetectSample, '*'); grid on; hold on; PlotUtils.createSScPlot(Data.RD.BottomDetectSample(N_indBeam), 'or')
FigUtils.createSScFigure; PlotUtils.createSScPlot(N_EndSampleDescriptor, '*'); grid on; hold on; PlotUtils.createSScPlot(Data.RD.EndSampleDescriptor(N_indBeam), 'or')
%}

%{
% BeginSampleDescriptor
nbBytes = 4;
subBytes = bsxfun(@plus, repmat(offsetBeams, nbBytes,1), (1:nbBytes)'); % Pour la R2016a
% subBytes = repmat(offsetBeams, nbBytes,1) + (1:nbBytes)'; % OK � partir de la version R2016b
subBytes = id + subBytes(:)';
N_BeginSampleDescriptor = typecast(Buffer(subBytes), 'uint32');
id = id + nbBytes;
% FigUtils.createSScFigure; PlotUtils.createSScPlot(N_BeginSampleDescriptor, '*'); grid on; hold on; PlotUtils.createSScPlot(Data.RD.BeginSampleDescriptor(N_indBeam), 'or')

% BottomDetectSample
nbBytes = 4;
subBytes = bsxfun(@plus, repmat(offsetBeams, nbBytes,1), (1:nbBytes)'); % Pour la R2016a
% subBytes = repmat(offsetBeams, nbBytes,1) + (1:nbBytes)'; % OK � partir de la version R2016b
subBytes = id + subBytes(:)';
N_BottomDetectSample = typecast(Buffer(subBytes), 'uint32');
id = id + nbBytes;
% FigUtils.createSScFigure; PlotUtils.createSScPlot(N_BottomDetectSample, '*'); grid on; hold on; PlotUtils.createSScPlot(Data.RD.BottomDetectSample(N_indBeam), 'or')

% EndSampleDescriptor
nbBytes = 4;
subBytes = bsxfun(@plus, repmat(offsetBeams, nbBytes,1), (1:nbBytes)'); % Pour la R2016a
% subBytes = repmat(offsetBeams, nbBytes,1) + (1:nbBytes)'; % OK � partir de la version R2016b
subBytes = id + subBytes(:)';
N_EndSampleDescriptor = typecast(Buffer(subBytes), 'uint32');
id = id + nbBytes;
% FigUtils.createSScFigure; PlotUtils.createSScPlot(N_EndSampleDescriptor, '*'); grid on; hold on; PlotUtils.createSScPlot(Data.RD.EndSampleDescriptor(N_indBeam), 'or')
%}

indBeam                                  = N_indBeam;
Data.RD.BeamDescriptor(N_indBeam)        = N_indBeam;
Data.RD.BeginSampleDescriptor(N_indBeam) = N_BeginSampleDescriptor;
Data.RD.BottomDetectSample(N_indBeam)    = N_BottomDetectSample;
Data.RD.EndSampleDescriptor(N_indBeam)   = N_EndSampleDescriptor;

ns = Data.RD.EndSampleDescriptor - Data.RD.BeginSampleDescriptor + 1;
nsTotal = sum(ns(N_indBeam));

%{
figure; hold on; grid on;
plot(Data.RD.BeginSampleDescriptor, '+r');
plot(Data.RD.EndSampleDescriptor, '+b');
plot(Data.RD.BottomDetectSample, '+g');
%}

%{
position = ftell(fid);
fseek(fid, position, 'bof');
%}

sub = indBeam(1:Data.RTH.N);
tabNbSamples = Data.RD.EndSampleDescriptor(sub) - Data.RD.BeginSampleDescriptor(sub) + 1;
sumNbSamples = sum(tabNbSamples);

% position = ftell(fid);
Buffer = fread(fid, sumNbSamples, 'single')';
if feof(fid)
    return
end
% fseek(fid, position, 'bof');
id = 0;

% Data.RD.Samples = NaN(1, 100*NbBeamsBathy, 'single'); % Comment� par JMA le 27/03/2017
n = max(NbBeamsBathy, max(Data.RD.BeamDescriptor)); % Modif JMA le 27/03/2017

Data.RD.Samples = NaN(1, 100*n, 'single'); % Modif JMA le 27/03/2017
for k=1:Data.RTH.N
    k2 = double(indBeam(k));
    NbSamples = ns(k2);
    
    %     Samples = fread(fid, NbSamples, 'single')';
    Samples = Buffer(id + (1:tabNbSamples(k)));
    id = id + tabNbSamples(k);
    
    R0 = Data.RD.BottomDetectSample(k2);
    if isnan(R0)
        continue
    end
    
    Centre      = R0 - Data.RD.BeginSampleDescriptor(k2);
    subSamples  = (1:NbSamples) + 50 - Centre;
    sub         = (subSamples >= 1) & (subSamples <= 100);
    subSamples  = (k2-1)*100 + subSamples;
    idx         = subSamples(sub);
    Data.RD.Samples(idx) = Samples(sub);
end

%% Lecture section OD

if DRF.OptionalDataOffset == 0
    Data.OD = [];
else
    
    %{
position = ftell(fid);
fseek(fid, position, 'bof');
    %}
    
    Frequency = fread(fid, 1, 'single');
    if feof(fid)
        return
    end
    
    X = fread(fid, 2, 'double');
    if feof(fid)
        return
    end
    
    Latitude  = X(1) * rad2pi;
    Longitude = X(2) * rad2pi;
    
    Heading = fread(fid, 1, 'single') * rad2pi;
    if feof(fid)
        return
    end
    
    Data.OD.Frequency = Frequency;
    Data.OD.Latitude  = Latitude;
    Data.OD.Longitude = Longitude;
    Data.OD.Heading   = Heading;
    
    Data.OD.BeamAlongTrackDistance  = NaN(1,NbBeamsBathy);
    Data.OD.BeamAcrossTrackDistance = NaN(1,NbBeamsBathy);
    Data.OD.CenterSampleDistance    = NaN(1,NbBeamsBathy);
    
    %{
position = ftell(fid);
fseek(fid, position, 'bof');
    %}
    Buffer = fread(fid, 12 * Data.RTH.N,  'uint8=>uint8');
    if feof(fid)
        return
    end
    
    % {
    %% Avant test am�lioration performances JMA le 11/03/2017
    
    %{
id = 0;
for k=1:Data.RTH.N
k2 = indBeam(k);
% 	X = fread(fid, 2, 'single');

X = typecast(Buffer(id + (1:8)), 'single');
id = id + 8;

Data.OD.BeamAlongTrackDistance(k2)  = X(1);
Data.OD.BeamAcrossTrackDistance(k2) = X(2);

%     Data.OD.CenterSampleDistance(k2) = fread(fid, 1, 'uint32');

X = typecast(Buffer(id + (1:4)), 'uint32');
id = id + 4;

Data.OD.CenterSampleDistance(k2) = X;
end
    %}
    
    id = 0;
    X1 = typecast(Buffer(id + (1:12*Data.RTH.N)), 'single');
    X2 = typecast(Buffer(id + (1:12*Data.RTH.N)), 'uint32');
    % id = id + 12*Data.RTH.N;
    for k=1:Data.RTH.N
        k1 = (k-1)*3;
        k2 = indBeam(k);
        Data.OD.BeamAlongTrackDistance(k2)  = X1(k1+1);
        Data.OD.BeamAcrossTrackDistance(k2) = X1(k1+2);
        Data.OD.CenterSampleDistance(k2)    = X2(k1+3);
    end
    
    %{
figure; plot(Data.OD.BeamAlongTrackDistance); grid on; title('BeamAlongTrackDistance')
figure; plot(Data.OD.BeamAcrossTrackDistance); grid on; title('BeamAcrossTrackDistance')
figure; plot(Data.OD.CenterSampleDistance); grid on; title('CenterSampleDistance')
    %}
    
    Data.RD.Samples = Data.RD.Samples(1:(100*NbBeamsBathy));
    
    % Bidouille pour corriger l'erreur
    % sub = (Data.RD.Samples > 0);
    % Data.RD.Samples(sub) = -Data.RD.Samples(sub);
end

flag = 1;
