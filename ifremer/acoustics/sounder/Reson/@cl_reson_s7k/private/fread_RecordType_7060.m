function [flag, Data] = fread_RecordType_7060(fid)

my_breakpoint % Jamais rencontré

flag = 0;
Data = [];

%% Read Record Type Header

X = fread(fid, 2, 'uint32');
if feof(fid)
    return
end

Data.RTH.LocalTrackIdentifier  = X(1);
Data.RTH.SystemTrackIdentifier = X(2);

Data.RTH.TimeOfContact = fread(fid, 10, 'char');
if feof(fid)
    return
end

Data.RTH.DatumIdentifier = fread(fid, 1, 'uint16');
if feof(fid)
    return
end

Data.RTH.PositionLatency = fread(fid, 1, 'single');
if feof(fid)
    return
end

X = fread(fid, 2, 'double');
if feof(fid)
    return
end

LatitudeOrNorthing = X(1);
LongitudeOrEasting = X(2);

Data.RTH.UTMZone = fread(fid, 1, 'char');
if feof(fid)
    return
end

HeightRelative2DatumOrHeight = fread(fid, 1, 'double');
if feof(fid)
    return
end

Data.RTH.PositionTypeFlag = fread(fid, 1, 'uint16');
if feof(fid)
    return
end

if Data.RTH.PositionTypeFlag == 1
    Data.RTH.Latitude = LatitudeOrNorthing;
    Data.RTH.Longitude = LongitudeOrEasting;
    Data.RTH.HeightRelative2Datum = HeightRelative2DatumOrHeight;
else if Data.RTH.PositionTypeFlag == 2
    Data.RTH.Northing = LatitudeOrNorthing;
    Data.RTH.Easting = LongitudeOrEasting;
    Data.RTH.Height = HeightRelative2DatumOrHeight;
    end
end

Data.RTH.ClassificationType = fread(fid, 1, 'uint16');
if feof(fid)
    return
end

Data.RTH.BearingToTarget = fread(fid, 1, 'single');
if feof(fid)
    return
end

Data.RTH.BearingFlag = fread(fid, 1, 'uint32');
if feof(fid)
    return
end

X = fread(fid, 2, 'single');
if feof(fid)
    return
end

Data.RTH.RangeToTarget = X(1);
Data.RTH.HoldingTime   = X(2);

Data.RTH.DetectionMethod = fread(fid, 1, 'uint32');
if feof(fid)
    return
end

X = fread(fid, 2, 'single');
if feof(fid)
    return
end

Data.RTH.SNR = X(1);
Data.RTH.TS  = X(2);

Data.RTH.ConfidenceValue = fread(fid, 1, 'uint32');
if feof(fid)
    return
end

X = fread(fid, 4, 'single');
if feof(fid)
    return
end

Data.RTH.TargetAltitude = X(1);
Data.RTH.TargetDepth    = X(2);
Data.RTH.TargetSpeed    = X(3);
Data.RTH.TargetHeading  = X(4);

Data.RTH.Reserved = fread(fid, 2, 'double');
if feof(fid)
    return
end

Data.RTH.TargetTextInformationSize = fread(fid, 1, 'uint32');
if feof(fid)
    return
end

Data.RTH.TargetTextInformation = fread(fid, Data.RTH.TargetTextInformationSize, 'char');

%% Read Record Data

Data.RD = [];

%% Read Optional Data

Data.OD = [];

flag = 1;