% Description
%   Reads S7K datagram 7200 (File Header)
%
% Syntax
%   [flag, Data] = fread_RecordType_7200(fid)
%
% Input Arguments
%   fid : fid of the S7K file
%
% Output Arguments
%   flag : 0=KO, 1=OK
%   Data : Structure (see example)
%
% Examples
%   [flag, Data] = fread_RecordType_7200(fid);
%   % Data = 
%   %     RTH: [1�1 struct]
%   %      RD: [1�1 struct]
%   %      OD: []   
%   % Data.RTH
%   %        FileIdentifier: [16�1 double]
%   %         VersionNumber: 0
%   %              Reserved: 0
%   %     SessionIdentifier: [16�1 char]
%   %        RecordDataSize: 388
%   %                     N: 12
%   % Data.RD
%   %        RecordingName: [64�1 char]
%   %     RecordingProgram: [16�1 char]
%   %      UserDefinedName: [64�1 char]
%   %                Notes: [128�1 char]
%   %     DeviceIdentifier: [1003 1012 1013 1015 1016 7000 7200 7018 7004 7007 7027 7610]
%   %     SystemEnumerator: [0 0 0 0 0 0 0 0 0 0 0 0]
%
% Authors : JMA 
% See also cre_ficIndex, fread_DRF 
%
% Reference pages in Help browser
%   <a href="matlab:doc fread_RecordType_7200">fread_RecordType_7200</a>
%-------------------------------------------------------------------------------

function [flag, Data] = fread_RecordType_7200(fid)

flag = 0;
Data = [];

% PositionCourante = ftell(fid);
% fseek(fid, PositionCourante, 'bof');

%% Read Record Type Header

Data.RTH.FileIdentifier = fread(fid, 16, 'uint8'); % Ancien format
if feof(fid)
    return
end

X = fread(fid, 2, 'uint16');
if feof(fid)
    return
end
Data.RTH.VersionNumber = X(1);
Data.RTH.Reserved      = X(2);

Data.RTH.SessionIdentifier = fread(fid, 16, 'char=>char');
if feof(fid)
    return
end

X = fread(fid, 2, 'uint32');
if feof(fid)
    return
end
Data.RTH.RecordDataSize = X(1);
Data.RTH.N              = X(2);

X = fread(fid, 272, 'char=>char');
if feof(fid)
    return
end

%% Read Recod Data

Data.RD.RecordingName    = X(1:64);    % fread(fid, 64, 'char');
Data.RD.RecordingProgram = X(65:80);   % fread(fid, 16, 'char');
Data.RD.UserDefinedName  = X(81:144);  % fread(fid, 64, 'char');
Data.RD.Notes            = X(145:272); % fread(fid, 128, 'char');

for k=1:Data.RTH.N
    Data.RD.DeviceIdentifier(k) = fread(fid, 1, 'uint32');
    if feof(fid)
        return
    end
    Data.RD.SystemEnumerator(k) = fread(fid, 1, 'uint16');
end

%% Read Optional Data

Data.OD = [];

flag = 1;
