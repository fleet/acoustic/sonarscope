% Description
%   Reads S7K datagram 70300 (File end)
%     Complete files generated using RESON 7kCenter software will always begin with a 7200 record, 
%     and will usually contain a 7300 record as the last record in the file. This record is for 
%     RESON diagnostic use only.
%
% Syntax
%   [flag, Data] = fread_RecordType_7300(fid)
%
% Input Arguments
%   fid : fid of the S7K file
%
% Output Arguments
%   flag : 0=KO, 1=OK
%   Data : Structure (see example)
%
% Examples
%   [flag, Data] = fread_RecordType_7300(fid);
%   % Data = 
%   %     RTH: []
%   %      RD: []
%   %      OD: []   
%
% Authors : JMA 
% See also cre_ficIndex, fread_DRF 
%
% Reference pages in Help browser
%   <a href="matlab:doc fread_RecordType_7300">fread_RecordType_7300</a>
%-------------------------------------------------------------------------------

% TODO : not complete

function [flag, Data] = fread_RecordType_7300(fid, DRF)

Data = [];

fread(fid, DRF.Size-64, 'uint8');

flag = -1;
