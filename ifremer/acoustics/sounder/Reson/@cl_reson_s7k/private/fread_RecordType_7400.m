% Time Message

function [flag, Data] = fread_RecordType_7400(fid)

my_breakpoint % Jamais rencontré

flag = 0;
Data = [];

%% Read Record Type Header

Data.RTH.LeapSecondOffset = fread(fid, 1, 'int8');
if feof(fid)
    return
end

Data.RTH.PulseFlag = fread(fid, 1, 'char');
if feof(fid)
    return
end

Data.RTH.PortIdentifier = fread(fid, 10, 'uint16');
if feof(fid)
    return
end

Data.RTH.Reserved = fread(fid, 1, 'uint32');
if feof(fid)
    return
end

Data.RTH.Reserved = fread(fid, 1, 'uint64');

%% Read Record Data

Data.RD = [];

%% Read Optional Data

Data.OD = [];

flag = 1;
