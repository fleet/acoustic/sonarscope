% 7k Remote Control

function [flag, Data] = fread_RecordType_7500(fid)

my_breakpoint % Jamais rencontré

flag = 0;
Data = [];

%% Read Record Type Header

Data.RTH.RemoteControlID = fread(fid, 1, 'uint32');
if feof(fid)
    return
end

Data.RTH.Ticket = fread(fid, 1, 'uint32');
if feof(fid)
    return
end

Data.RTH.TrackingNumber = fread(fid, 128, 'char');
if feof(fid)
    return
end

%% Read Record Data

my_warndlg('A valider : lecture des Data dynamique',1);
Data.RD.RemoteControlData = fread(fid, 1, 'uint64');

%% Read Optional Data

Data.OD = [];

flag = 1;