% 7k Remote Control Not Acknowledge

function [flag, Data] = fread_RecordType_7502(fid)

my_breakpoint % Jamais rencontré

flag = 0;
Data = [];

%% Read Record Type Header

Data.RTH.Ticket = fread(fid, 1, 'uint32');
if feof(fid)
    return
end

Data.RTH.TrackingNumber = fread(fid, 128, 'char');
if feof(fid)
    return
end

Data.RTH.ErrorCode = fread(fid, 128, 'uint32');

%% Read Record Data


%% Read Optional Data

Data.OD = [];

flag = 1;