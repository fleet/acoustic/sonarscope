% Description
%   Reads S7K datagram 7503 (Remote Control Sonar Settings)
%
% Syntax
%   [flag, Data] = fread_RecordType_7503(fid)
%
% Input Arguments
%   fid : fid of the S7K file
%
% Output Arguments
%   flag : 0=KO, 1=OK
%   Data : Structure (see example)
%
% Examples
%   [flag, Data] = fread_RecordType_7503(fid);
%   % Data = 
%   %     RTH: [1�1 struct]
%   %      RD: []
%   %      OD: []   
%   % Data.RTH
%   %                                   SonarId: 0
%   %                               PingCounter: 88560
%   %                                 Frequency: 200000
%   %                                SampleRate: 3.448275781250000e+04
%   %                          ReceiverBandwith: 0
%   %                              TxPulseWidth: 6.500000017695129e-05
%   %                     TxPulseTypeIdentifier: 0
%   %                  TxPulseEnvelopeParameter: 0
%   %                           TxPulseReserved: 0
%   %                               MaxPingRate: 0
%   %                                PingPeriod: 50
%   %                            RangeSelection: 0.453999996185303
%   %                            PowerSelection: 300
%   %                             GainSelection: 220
%   %                              ControlFlags: 1.108082688000000e+09
%   %                      ProjectorMagicNumber: 151683072
%   %        ProjectorBeamSteeringAngleVertical: 1.401298464324817e-45
%   %      ProjectorBeamSteeringAngleHorizontal: 0
%   %          ProjectionBeam_3dB_WidthVertical: 0
%   %        ProjectionBeam_3dB_WidthHorizontal: 0.017453299835324
%   %                  ProjectionBeamFocalPoint: 2.094395160675049
%   %         ProjectionBeamWeightingWindowType: 0
%   %    ProjectionBeamWeightingWindowParameter: 0
%   %                             TransmitFlags: 0
%   %                     HydrophoneMagicNumber: 0
%   %                ReceiveBeamWeightingWindow: 0
%   %             ReceiveBeamWeightingParameter: 0
%   %                              ReceiveFlags: 0
%   %         BottomDetectionFilterInfoMinRange: 1
%   %         BottomDetectionFilterInfoMaxRange: 5
%   %         BottomDetectionFilterInfoMinDepth: 1
%   %         BottomDetectionFilterInfoMaxDepth: 5
%   %                                Absorption: 41
%   %                             SoundVelocity: 1.488901000976563e+03
%   %                                 Spreading: 33
%   %                                  Reserved: 34
%   %                    TxArrayPositionOffsetX: 0
%   %                    TxArrayPositionOffsetY: 0
%   %                    TxArrayPositionOffsetZ: 0
%   %                                 HeadTiltX: 0
%   %                                 HeadTiltY: 0
%   %                                 HeadTiltZ: 0
%   %                                 PingOnOff: 1
%
% Authors : JMA 
% See also cre_ficIndex, fread_DRF 
%
% Reference pages in Help browser
%   <a href="matlab:doc fread_RecordType_7503">fread_RecordType_7503</a>
%-------------------------------------------------------------------------------

function [flag, Data] = fread_RecordType_7503(fid)

flag = 0;
Data = [];

%% Read Record Type Header

Data.RTH.SonarId = fread(fid, 1, 'uint64');
if feof(fid)
    return
end

Data.RTH.PingCounter = fread(fid, 1, 'uint32');
if feof(fid)
    return
end

X = fread(fid, 4, 'single');
if feof(fid)
    return
end

Data.RTH.Frequency        = X(1);
Data.RTH.SampleRate       = X(2);
Data.RTH.ReceiverBandwith = X(3);
Data.RTH.TxPulseWidth     = X(4);

Data.RTH.TxPulseTypeIdentifier = fread(fid, 1, 'uint32');
if feof(fid)
    return
end

Data.RTH.TxPulseEnvelopeParameter = fread(fid, 1, 'single');
if feof(fid)
    return
end

Data.RTH.TxPulseReserved = fread(fid, 1, 'uint32');
if feof(fid)
    return
end

X = fread(fid, 5, 'single');
if feof(fid)
    return
end

Data.RTH.MaxPingRate    = X(1);
Data.RTH.PingPeriod     = X(2);
Data.RTH.RangeSelection = X(3);
Data.RTH.PowerSelection = X(4);
Data.RTH.GainSelection  = X(5);


X = fread(fid, 2, 'uint32');
if feof(fid)
    return
end

Data.RTH.ControlFlags         = X(1);
Data.RTH.ProjectorMagicNumber = X(2);

X = fread(fid, 6, 'single');
if feof(fid)
    return
end

Data.RTH.ProjectorBeamSteeringAngleVertical   = X(1);
Data.RTH.ProjectorBeamSteeringAngleHorizontal = X(2);
Data.RTH.ProjectionBeam_3dB_WidthVertical     = X(3);
Data.RTH.ProjectionBeam_3dB_WidthHorizontal   = X(4);
Data.RTH.ProjectionBeamFocalPoint             = X(5);
pppp = X(6);  %#ok<NASGU> Pour trouver la synchronisation

Data.RTH.ProjectionBeamWeightingWindowType = fread(fid, 1, 'uint32');
if feof(fid)
    return
end

Data.RTH.ProjectionBeamWeightingWindowParameter = fread(fid, 1, 'single');
if feof(fid)
    return
end

X = fread(fid, 3, 'uint32');
if feof(fid)
    return
end

Data.RTH.TransmitFlags              = X(1);
Data.RTH.HydrophoneMagicNumber      = X(2);
Data.RTH.ReceiveBeamWeightingWindow = X(3);

Data.RTH.ReceiveBeamWeightingParameter = fread(fid, 1, 'single');
if feof(fid)
    return
end

Data.RTH.ReceiveFlags = fread(fid, 1, 'uint32');
if feof(fid)
    return
end

X = fread(fid, 7, 'single');
if feof(fid)
    return
end

Data.RTH.BottomDetectionFilterInfoMinRange = X(1);
Data.RTH.BottomDetectionFilterInfoMaxRange = X(2);
Data.RTH.BottomDetectionFilterInfoMinDepth = X(3);
Data.RTH.BottomDetectionFilterInfoMaxDepth = X(4);
Data.RTH.Absorption                        = X(5);
Data.RTH.SoundVelocity                     = X(6);
Data.RTH.Spreading                         = X(7);

Data.RTH.Reserved = fread(fid, 1, 'uint16');
if feof(fid)
    return
end

X = fread(fid, 6, 'single');
if feof(fid)
    return
end

Data.RTH.TxArrayPositionOffsetX = X(1);
Data.RTH.TxArrayPositionOffsetY = X(2);
Data.RTH.TxArrayPositionOffsetZ = X(3);
Data.RTH.HeadTiltX              = X(4);
Data.RTH.HeadTiltY              = X(5);
Data.RTH.HeadTiltZ              = X(6);

Data.RTH.PingOnOff = fread(fid, 1, 'uint32');

%% Read Record Data

Data.RD = [];

%% Read Optional Data

Data.OD = [];

flag = 1;
