% Description
%   Reads S7K datagram 7200 (Sound Velocity)
%
% Syntax
%   [flag, Data] = fread_RecordType_7610(fid)
%
% Input Arguments
%   fid : fid of the S7K file
%
% Output Arguments
%   flag : 0=KO, 1=OK
%   Data : Structure (see example)
%
% Examples
%   [flag, Data] = fread_RecordType_7610(fid);
%   % Data = 
%   %     RTH: [1�1 struct]
%   %      RD: []
%   %      OD: []   
%   % Data.RTH
%   %     SoundVelocity: 1.449171020507813e+03
%
% Authors : JMA 
% See also cre_ficIndex, fread_DRF 
%
% Reference pages in Help browser
%   <a href="matlab:doc fread_RecordType_7610">fread_RecordType_7610</a>
%-------------------------------------------------------------------------------

function [flag, Data] = fread_RecordType_7610(fid)

Data = [];

%% Read Record Type Header

Data.RTH.SoundVelocity = fread(fid, 1, 'single');

%% Read Record Data

Data.RD = [];

%% Read Optional Data

Data.OD = [];

flag = 1;