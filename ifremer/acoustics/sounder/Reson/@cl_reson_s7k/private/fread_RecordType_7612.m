% 7k Spreading Loss

function [flag, Data] = fread_RecordType_7612(fid)

my_breakpoint % Jamais rencontré

Data = [];

%% Read Record Type Header

Data.RTH.SpreadingLoss = fread(fid, 1, 'single');

%% Read Record Data

Data.RD = [];

%% Read Optional Data

Data.OD = [];

flag = 1;