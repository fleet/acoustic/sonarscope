function [flag, Data] = fread_RecordType_8100(fid)

flag = 0;
Data = [];

%% Read Record Type Header

Data.RTH.Type = fread(fid, 1, 'single');
if feof(fid)
    return
end

Data.RTH.Flags = fread(fid, 1, 'char');
if feof(fid)
    return
end

Data.RTH.DataSize = fread(fid, 1, 'uint16');
if feof(fid)
    return
end

Data.RTH.PacketsFollowingHeader = fread(fid, 1, 'uint16');
if feof(fid)
    return
end

Data.RTH.Reserved = fread(fid, 10, 'char');

%% Read Record Data

Data.RD = [];

%% Read Optional Data

Data.OD = [];

flag = 1;