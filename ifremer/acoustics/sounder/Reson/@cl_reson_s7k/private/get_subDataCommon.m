function [flag, subData, subCaris] = get_subDataCommon(Type, TypeDatagram, PingCounter, PingSequence, DeviceIdentifier, identSondeur)

subCaris = [];

%{
subType = find(TypeDatagram == Type);
FigUtils.createSScFigure;
h(1) = subplot(4,1,1); PlotUtils.createSScPlot(TypeDatagram, '*b'); grid on; title(['TypeDatagram (red is for ' num2str(Type) ')']);
hold on; PlotUtils.createSScPlot(subType, TypeDatagram(subType), '*r');

h(2) = subplot(4,1,2); PlotUtils.createSScPlot(PingCounter, '*b'); grid on; title('PingCounter');
hold on; PlotUtils.createSScPlot(subType, PingCounter(subType), '*r');

h(3) = subplot(4,1,3); PlotUtils.createSScPlot(PingSequence, '*b'); grid on; title('PingSequence');
hold on; PlotUtils.createSScPlot(subType, PingSequence(subType), '*r');

h(4) = subplot(4,1,4); PlotUtils.createSScPlot(DeviceIdentifier, '*b'); grid on; title('DeviceIdentifier');
hold on; PlotUtils.createSScPlot(subType, DeviceIdentifier(subType), '*r');
linkaxes(h, 'x')
%}

flagSondeur = ((DeviceIdentifier < 7111) | (DeviceIdentifier == identSondeur));

sub7000 = find((TypeDatagram == 7000) & flagSondeur);
sub7006 = find((TypeDatagram == 7006) & flagSondeur);
sub7007 = find((TypeDatagram == 7007) & flagSondeur);
sub7008 = find((TypeDatagram == 7008) & flagSondeur);
sub7057 = find((TypeDatagram == 7057) & flagSondeur);
sub7058 = find((TypeDatagram == 7058) & flagSondeur);
sub7027 = find((TypeDatagram == 7027) & flagSondeur);
sub7028 = find((TypeDatagram == 7028) & flagSondeur);
sub7041 = find((TypeDatagram == 7041) & flagSondeur);
sub7042 = find((TypeDatagram == 7042) & flagSondeur);

%% Gestion des historiques des datagrammes

% Avant modif JMA le 20/03/2015
% if isempty(sub7008)
%     if isempty(sub7028)
%         sub7008 = sub7058; % Datagrammes les plus r�cents
%     else
%         sub7008 = sub7028;
%     end
% end

% Apr�s modif JMA le 20/03/2015
if isempty(sub7058)
    if ~isempty(sub7028)
        sub7008 = sub7028;
    end
else
    if Type == 7028
        sub7008 = sub7028;
    else
        sub7008 = sub7058;
    end
end

if isempty(sub7007)
    sub7007 = sub7057; % Datagrammes les plus r�cents
end
if isempty(sub7006)
    sub7006 = sub7027;
end


PingCounter7000 = PingCounter(sub7000);
PingCounter7006 = PingCounter(sub7006);
PingCounter7007 = PingCounter(sub7007);
PingCounter7008 = PingCounter(sub7008);
% PingCounter7041 = PingCounter(sub7041);

% figure; grid on; hold on;
% plot(PingCounter7006, '+b'); 
% plot(PingCounter7007, 'or'); 
% plot(PingCounter7008, 'xk'); 

PingSequence7000 = PingSequence(sub7000);
PingSequence7006 = PingSequence(sub7006);
PingSequence7007 = PingSequence(sub7007);
PingSequence7008 = PingSequence(sub7008);
% PingSequence7041 = PingSequence(sub7041);

if all(PingSequence7006 == 0)
    if ~isempty(PingCounter7007) && ~isempty(PingCounter7008)
        P7000 = PingCounter7000;
        P7006 = PingCounter7006;
        P7007 = PingCounter7007;
        P7008 = PingCounter7008;
        sub = intersect3(P7000, P7006, P7007);
        [sub0, sub7, sub8] = intersect3(P7000(sub), P7007, P7008);
        sub7000 = sub7000(sub(sub0));
        sub7006 = sub7006(sub0);
        sub7007 = sub7007(sub7);
        sub7008 = sub7008(sub8);
        
        % TODO : il manque la d�finition de subCaris mais j'ai peur
        % d'introduire une boulette. Ce manque est trait� maladroitement
        % dans sonar_importBathyFromCaris_S7K.m
        % Il faudra traiter cela s�rieusement si bug rencontr� dans sonar_importBathyFromCaris_S7K
       
    elseif ~isempty(PingCounter7007) && isempty(PingCounter7008)
        P7000 = PingCounter7000;
        P7006 = PingCounter7006;
        P7007 = PingCounter7007;
        [sub0, sub6, sub7] = intersect3(P7000, P7006, P7007);
        sub7000 = sub7000(sub0);
        sub7006 = sub7006(sub6);
        sub7007 = sub7007(sub7);
        switch Type
            case 7000
                subCaris = sub0;
            case 7006
                subCaris = sub6;
            case {7007; 7055}
                subCaris = sub7;
            case 7027 % Ajout JMA le 04/02/2021 : sub7 au pif
                subCaris = sub7;
            otherwise
                messageForDebugInspection
        end
        
    elseif isempty(PingCounter7007) && ~isempty(PingCounter7008)
        P7000 = PingCounter7000;
        P7006 = PingCounter7006;
        P7008 = PingCounter7008;
        [sub0, sub6, sub8] = intersect3(P7000, P7006, P7008);
        sub7000 = sub7000(sub0);
        sub7006 = sub7006(sub6);
        sub7008 = sub7008(sub8);
        switch Type
            case 7000
                subCaris = sub0;
            case 7006
                subCaris = sub6;
            case {7008; 7058}
                subCaris = sub8;
            case 7041 % Ajout JMA le 04/02/2021 : sub8 au pif
                subCaris = sub8;
            otherwise
                messageForDebugInspection
        end
        
    else % isempty(PingCounter7007) && isempty(PingCounter7008)
    end
else
    nbFreq = max(PingSequence7006);
    if ~isempty(PingCounter7007) && ~isempty(PingCounter7008)
        P7000 = (PingCounter7000 - PingCounter7000(1)) * nbFreq + PingSequence7000;
        P7006 = (PingCounter7006 - PingCounter7000(1)) * nbFreq + PingSequence7006;
        P7007 = (PingCounter7007 - PingCounter7000(1)) * nbFreq + PingSequence7007;
        P7008 = (PingCounter7008 - PingCounter7000(1)) * nbFreq + PingSequence7008;
        sub = intersect3(P7000, P7006, P7007);
        
        % TODO : il manque la d�finition de subCaris mais j'ai peur
        % d'introduire une boulette. Ce manque est trait� maladroitement
        % dans sonar_importBathyFromCaris_S7K.m
        % Il faudra traiter cela s�rieusement si bug rencontr� dans sonar_importBathyFromCaris_S7K
        
        % Added PP? 21/07/2008 for file G:\ResonSat2008\Cross7150-12kHz\SonarScope\20080721_060045_PP-7150_12kHz.s7k
        % Il y avait 141 records d'imagerie et 564 records de bathy
%         [sub0, sub7, sub8] = intersect3(P7000(sub), P7007, P7008);
%         sub7000 = sub7000(sub(sub0));
%         sub7006 = sub7006(sub0);
%         sub7007 = sub7007(sub7);
%         sub7008 = sub7008(sub8);
        if length(P7008) < (length(P7007)/4+1)
            my_warndlg('The number of imagery datagrams is too small compared at the bathymetry, I remove it completly.', 0, 'Tag', 'P7007 probl�matique');
            [sub0, sub7] = intersect3(P7000(sub), P7007, P7007);
            sub7000 = sub7000(sub(sub0));
            sub7006 = sub7006(sub0);
            sub7007 = sub7007(sub7);
            sub7008 = [];
        end


    elseif ~isempty(PingCounter7007) && isempty(PingCounter7008)
        P7000 = (PingCounter7000 - PingCounter7000(1)) * nbFreq + PingSequence7000;
        P7006 = (PingCounter7006 - PingCounter7000(1)) * nbFreq + PingSequence7006;
        P7007 = (PingCounter7007 - PingCounter7000(1)) * nbFreq + PingSequence7007;
        [sub0, sub6, sub7] = intersect3(P7000, P7006, P7007);
        sub7000 = sub7000(sub0);
        sub7006 = sub7006(sub6);
        sub7007 = sub7007(sub7);
        
        % Partie ajout�e en aveugle par JMA le 21/03/2017 : Jamais test�
        switch Type
            case 7000
                subCaris = sub0;
            case 7006
                subCaris = sub6;
            case {7007; 7055}
                subCaris = sub7;
            otherwise
                messageForDebugInspection
        end

    
    elseif isempty(PingCounter7007) && ~isempty(PingCounter7008)
        P7000 = (PingCounter7000 - PingCounter7000(1)) * nbFreq + PingSequence7000;
        P7006 = (PingCounter7006 - PingCounter7000(1)) * nbFreq + PingSequence7006;
        P7008 = (PingCounter7008 - PingCounter7000(1)) * nbFreq + PingSequence7008;
        [sub0, sub6, sub8] = intersect3(P7000, P7006, P7008);
        
        % Rajout JMA le 29/05/2016 fichier Carla 20150902_050823_PP_7150_24kHz.s7k
        [~, ordre] = sort(sub0);
        sub0 = sub0(ordre);
        sub6 = sub6(ordre);
        sub8 = sub8(ordre);
        
        sub7000 = sub7000(sub0);
        sub7006 = sub7006(sub6);
        sub7008 = sub7008(sub8);
        
        % Partie ajout�e en aveugle par JMA le 21/03/2017 : Jamais test�
        switch Type
            case 7000
                subCaris = sub0;
            case 7006
                subCaris = sub6;
            case {7008; 7058}
                subCaris = sub8;
            case {7041; 7042; 7048}
%                 subCaris = sub7041; % Rajout JMA le 30/05/2017
                subCaris = []; % Rajout JMA le 30/05/2017
            otherwise
                messageForDebugInspection
        end
        
    else % isempty(PingCounter7007) && isempty(PingCounter7008)
    end
end

%{
figure; grid on; hold on;
plot(PingCounter7000(sub7000), 'sg'); 
plot(PingCounter7006(sub7006), '+b'); 
plot(PingCounter7007(sub7007), 'or'); 
plot(PingCounter7008(sub7008), 'xk'); 

figure; grid on; hold on;
plot(PingSequence7000(sub7000), 'sg');
plot(PingSequence7006(sub7006), '+b'); 
plot(PingSequence7007(sub7007), 'or'); 
plot(PingSequence7008(sub7008), 'xk');
%}

switch Type
    case 7000
        subData = sub7000;
    case {7006 ;7027}
        subData = sub7006;
    case {7007 ;7057}
        subData = sub7007;
    case {7008 ;7028; 7058}
        subData = sub7008;
    case 7041
        subData = sub7041;
    case 7042
        subData = sub7042;
    otherwise
        subData = find(TypeDatagram == Type);
end

flag = 1;