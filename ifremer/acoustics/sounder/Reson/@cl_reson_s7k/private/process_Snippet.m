function [Samples, TxAngle] = process_Snippet(DataDepth, BeamData, iDepth, fe, Frequency, idebBeamBathy, ifinBeamBathy)

subPixels = 1:100;
Range0 = (-49.5:49.5) - 0.5;

NbBeamsBathy = ifinBeamBathy - idebBeamBathy + 1;
TxAngle    = NaN(1, NbBeamsBathy*100, 'single');
TxAngleOLD = NaN(1, NbBeamsBathy*100, 'single');
% Xni        = NaN(1, NbBeamsBathy*100, 'single');

%{
    figure; plot(BeamData.RD.BeamDescriptor, '+'); grid on; title('BeamDescriptor')
    figure; plot(BeamData.RD.BeginSampleDescriptor, '+'); grid on; title('BeginSampleDescriptor')
    figure; plot(BeamData.RD.EndSampleDescriptor, '+'); grid on; title('EndSampleDescriptor')
    figure; plot(BeamData.OD.CenterSampleDistance, '+'); grid on; title('CenterSampleDistance')
%}
Samples = single(BeamData.RD.Samples);

if iDepth > size(DataDepth.BeamDepressionAngle, 1)
    % C'est arriv� sur le fichier R:\EGINA\utilisateurs\sonarscope@ifremer.fr\USAN\PDS\20120118_065038_PP-7150-24kHz-.s7k 
    return
end

% figure; plot(Samples); grid on;
%     Samples(Samples < 100) = NaN;
%     % figure; plot(Samples); grid on;
%     Samples = reflec_Enr2dB(Samples); %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% figure; plot(Samples); grid on;

% figure; plot(Data.RangeCentre(i,:)); grid on; title('RangeCentre');
% figure; plot(Data.Range(i,:)); grid on; title('Range');
AngleFais = DataDepth.BeamDepressionAngle(iDepth,:);
Roll = DataDepth.Roll(iDepth);
% figure; plot(AngleFais); grid on; title('AngleFais');
RangeFais = DataDepth.Range(iDepth,:) .* sign(AngleFais);
% figure; plot(RangeFais); grid on; title('RangeFais');
%     deltaDegParMetre = gradient(AngleFais) ./ gradient(RangeFais);
% figure; plot(gradient(AngleFais)); grid on; title('gradient(AngleFais)');
% figure; plot(gradient(RangeFais)); grid on; title('gradient(RangeFais)');
%     deltaDegParMetre(isinf(deltaDegParMetre)) = 0;
%     deltaDegParMetre(end+1) = deltaDegParMetre(end);
DeltaD = 1500 / (2 * fe);
%     deltaDeg = deltaDegParMetre * DeltaD;

deltaDeg = (180/pi) * DeltaD ./ (RangeFais .* tand(AngleFais));
% figure; plot(deltaDeg); grid on; title('deltaDeg');

Rn = DataDepth.Range(iDepth,:);
Xn = DataDepth.AcrossDist(iDepth,:);
Hn = sqrt(Rn.^2 - Xn.^2);
Tetan = AngleFais;
Zn = Rn .* cosd(Tetan);

if Frequency == 22.5 % 7150 en 24kHz
    Ouverture = 0.5;
elseif Frequency == 23.5 % 7150 en 24kHz
    Ouverture = 0.5;
elseif Frequency == 24.5 % 7150 en 24kHz
    Ouverture = 0.5;
elseif Frequency == 25.5 % 7150 en 24kHz
    Ouverture = 0.5;

    % APPARU pour la SAT
elseif Frequency == 24 % 7150 en 24kHz
    Ouverture = 0.5;
    
    % APPARU pour la SAT2
elseif Frequency == 24.375 % 7150 en 24kHz
    Ouverture = 0.5;
elseif Frequency == 24.125 % 7150 en 24kHz
    Ouverture = 0.5;
elseif Frequency == 23.875 % 7150 en 24kHz
    Ouverture = 0.5;
elseif Frequency == 23.625 % 7150 en 24kHz
    Ouverture = 0.5;
        
    
elseif Frequency == 100 % 7111
    Ouverture = 0.5;

elseif Frequency == 11.625 % 7150 en 12kHz
    Ouverture = 1;
elseif Frequency == 11.875 % 7150 en 12kHz
    Ouverture = 1;
elseif Frequency == 12.125 % 7150 en 12kHz
    Ouverture = 1;
elseif Frequency == 12.375 % 7150 en 12kHz
    Ouverture = 1;
elseif Frequency == 12 % 7150 en 12kHz en mono-ping ?
    Ouverture = 1;

elseif Frequency == 10.5 % 7150 en 12kHz
    Ouverture = 1;
elseif Frequency == 11.5 % 7150 en 12kHz
    Ouverture = 1;
elseif Frequency == 12.5 % 7150 en 12kHz
    Ouverture = 1;
elseif Frequency == 13.5 % 7150 en 12kHz
    Ouverture = 1;

elseif Frequency == 396 % 7125
    Ouverture = 0.57;

else
%     str = sprintf('read_SnippetData : Data.Frequency=%f not interprated', Frequency);
%     disp(str)
% %     my_warndlg(str, 0, 'Tag', 'Data.Frequency not interprated. Ouverture = 10 instead');
    Ouverture = 10;
    %         return
end
%     switch BeamData.RTH.SonarId
%         case 0
%             if isempty(Ouverture)
%                 [rep, validation] = my_listdlg('What MBES is it ?', {'7111'; '7125'; '7150'; '7150'})
%
%             end
%         otherwise
%     end
%     % 1.8 pour la majorit� des faisceaux du 7111
%     % 0.5 pour 7150 en 24kHz et 1 deg en 12kHz
%     % 0.57 pour le 7125
%     Ouverture = 0.9;
%     %         Ouverture = 3.5;

% subMilieu = 49:53;
for iFais=idebBeamBathy:ifinBeamBathy
    if isnan(BeamData.RD.BeamDescriptor(iFais))
        continue
    end
    subSamplesPartiel = subPixels + 100*(iFais-idebBeamBathy);
    TxAngleOLD(subSamplesPartiel) = AngleFais(iFais) + Range0 * deltaDeg(iFais);

    Rni = Rn(iFais) + Range0 * DeltaD;

%     sub = (Rni.^2 > Hn(iFais)^2);
%     Xni(subSamplesPartiel(sub)) = sqrt(Rni(sub).^2 - Hn(iFais)^2) * sign(Tetan(iFais));
    sub = (abs(Rni) > abs(Zn(iFais)));
    if isnan(Roll)
        Roll = 0;
    end
    TxAngle(subSamplesPartiel(sub)) = Roll + acosd(Zn(iFais) ./ Rni(sub)) * sign(Tetan(iFais));

    %         figure(1111); hold off; plot(TxAngle(subSamplesPartiel), Samples(subSamplesTotal)); grid on;
%{
    diag = AntenneSincDep(TxAngle(subSamplesPartiel), [0, AngleFais(iFais), Ouverture, 0]);%figure; plot(diag); grid on; title('diag')
    %         diag(diag < -25) = NaN;
    %         diag(diag < -10) = NaN;


    OuvertureEffective = Ouverture * (1 + cosd(AngleFais(iFais)));
    subOK = (TxAngle(subSamplesPartiel) >= (AngleFais(iFais) -OuvertureEffective/2)) & (TxAngle(subSamplesPartiel) <= (AngleFais(iFais) + OuvertureEffective/2));
    %         subRejet = (TxAngle(subSamplesPartiel) < (AngleFais(iFais) -OuvertureEffective/2)) | (TxAngle(subSamplesPartiel) > (AngleFais(iFais) + OuvertureEffective/2));
    if sum(subOK) > length(subMilieu)
        diag(~subOK) = NaN;
    else
        %             diag(:) = NaN;
        %             diag(subMilieu) = 1;

        subOK(subMilieu) = 1;
        diag(diag < -10) = -10;
        diag(~subOK) = NaN;
    end
    % figure; plot(diag); grid on; title('diag')
    % figure; plot( Samples(subSamplesTotal)); grid on; title('Samples')
    % Samples(subSamplesTotal) = Samples(subSamplesTotal) - diag;
    % figure; plot( Samples(subSamplesTotal)); grid on; title('Samples')
    %         figure(1111); hold on; plot(TxAngle(subSamplesPartiel), Samples(subSamplesTotal), 'r');
%}
end
subSamplesPartiel = 1 + (100*(idebBeamBathy-1)):(100*ifinBeamBathy);
Samples = Samples(subSamplesPartiel);
