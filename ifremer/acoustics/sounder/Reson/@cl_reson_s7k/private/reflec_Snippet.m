function [X100, X1] = reflec_Snippet(BeamData, typeSnippets)

nbBeams = length(BeamData.RD.BeamDescriptor);

% TODO : attention, la premi�re partie �tait ce qui existait pour les
% sondeurs Reson avant la prise en compte des sondeurs Norbit. V�rifier si
% c'est toujours bon

% if isfield(BeamData, 'OD') && isfield(BeamData.OD, 'CenterSampleDistance')
%     nSup100 = ceil( (BeamData.RD.EndSampleDescriptor   - BeamData.OD.CenterSampleDistance) / 2);
%     nInf100 = floor((BeamData.RD.BeginSampleDescriptor - BeamData.OD.CenterSampleDistance) / 2);
%     
%     % isDeb200 = max(50 + nInf200, 1);
%     % isEnd200 = min(50 + nSup200, 100);
%     
%     isDeb100 = max(50 - nInf100, 1);
%     isEnd100 = min(50 + nSup100, 100);
%     
% else % Donn�es Norbit % TODO
    m1 = [BeamData.RD.BottomDetectSample(1) floor((BeamData.RD.BottomDetectSample(1:end-1) + BeamData.RD.BottomDetectSample(2:end)) / 2)];
    m2 = [m1(2:end) BeamData.RD.EndSampleDescriptor(end)];

    d1 = BeamData.RD.BottomDetectSample - m1;
    d2 = BeamData.RD.BottomDetectSample - m2;
    
    d12 = max(abs(d1), abs(d2));
    
    isDeb100 = max(50 - d12, 1);
    isEnd100 = min(50 + d12, 100);

    %{
    FigUtils.createSScFigure;
    PlotUtils.createSScPlot(BeamData.RD.BeginSampleDescriptor, 'k'); grid on; hold on
    PlotUtils.createSScPlot(BeamData.RD.EndSampleDescriptor,   'b');
    PlotUtils.createSScPlot(BeamData.RD.BottomDetectSample,    'r');
    legend({'BeginSampleDescriptor'; 'EndSampleDescriptor'; 'BottomDetectSample'})
    
    FigUtils.createSScFigure;
    PlotUtils.createSScPlot(difBottomDetectSample, '.r'); grid on;
    
    FigUtils.createSScFigure;
    PlotUtils.createSScPlot(nInf100, 'k'); grid on; hold on
    PlotUtils.createSScPlot(nSup100, 'b');
    PlotUtils.createSScPlot(BeamData.RD.BottomDetectSample, 'r');
    legend({'BeginSampleDescriptor'; 'EndSampleDescriptor'; 'BottomDetectSample'})

    FigUtils.createSScFigure;
    PlotUtils.createSScPlot(isDeb100, 'k'); grid on; hold on
    PlotUtils.createSScPlot(isEnd100, 'b');
    legend({'isDeb100'; 'isEnd100'})
%}
% end

%{
    FigUtils.createSScFigure; PlotUtils.createSScPlot(BeamData.RD.Samples, '.'); grid on; title('Samples')
    FigUtils.createSScFigure; PlotUtils.createSScPlot(BeamData.RD.BeamDescriptor, '+'); grid on; title('BeamDescriptor')
    FigUtils.createSScFigure; PlotUtils.createSScPlot(BeamData.RD.BeginSampleDescriptor, '+'); grid on; title('BeginSampleDescriptor')
    FigUtils.createSScFigure; PlotUtils.createSScPlot(BeamData.RD.EndSampleDescriptor, '+'); grid on; title('EndSampleDescriptor')
    FigUtils.createSScFigure; PlotUtils.createSScPlot(BeamData.OD.CenterSampleDistance, '+'); grid on; title('CenterSampleDistance')

    iSampleEnd   = BeamData.OD.CenterSampleDistance + (BeamData.RD.EndSampleDescriptor-BeamData.OD.CenterSampleDistance) / 2;
    iSampleBegin = BeamData.OD.CenterSampleDistance + (BeamData.RD.BeginSampleDescriptor-BeamData.OD.CenterSampleDistance) / 2;
    FigUtils.createSScFigure;
    PlotUtils.createSScPlot(BeamData.RD.BeginSampleDescriptor, 'k');
    grid on;
    hold on;
    PlotUtils.createSScPlot(BeamData.RD.EndSampleDescriptor, 'k');
    PlotUtils.createSScPlot(BeamData.OD.CenterSampleDistance, 'r')
    PlotUtils.createSScPlot(iSampleEnd, 'm');
    PlotUtils.createSScPlot(iSampleBegin, 'm')

    iSampleEnd   = BeamData.OD.CenterSampleDistance + (BeamData.RD.EndSampleDescriptor-BeamData.OD.CenterSampleDistance) / 4;
    iSampleBegin = BeamData.OD.CenterSampleDistance + (BeamData.RD.BeginSampleDescriptor-BeamData.OD.CenterSampleDistance) / 4;
    PlotUtils.createSScPlot(iSampleEnd, 'b');
    PlotUtils.createSScPlot(iSampleBegin, 'b');

    PlotUtils.createSScPlot(iSampleEnd, '+b')
    PlotUtils.createSScPlot(iSampleBegin, '+m')

    FigUtils.createSScFigure; PlotUtils.createSScPlot(BeamData.OD.CenterSampleDistance, '+'); grid on; title('OD.CenterSampleDistance')

    FigUtils.createSScFigure;
    PlotUtils.createSScPlot(nSup, 'k');
    grid on;
    hold on;
    PlotUtils.createSScPlot(nInf, 'k');
%}
% X200 = NaN(1,nbBeams, 'single');
X100 = NaN(1,nbBeams, 'single');
X1   = NaN(1,nbBeams, 'single');

if typeSnippets == 7058
    BeamData.RD.Samples = reflec_dB2Enr(BeamData.RD.Samples); % TODO sans doute reflec_dB2Amp, � v�rifier
end
kFais = 0;
for iFais=1:nbBeams
    kFais = kFais + 1;
    if isnan(BeamData.RD.BeamDescriptor(iFais))
        continue
    end
%     subSamples = (isDeb200(iFais):isEnd200(iFais)) + (iFais-1) * 100;
%     subSamples(subSamples > length(BeamData.RD.Samples)) = [];
%     if ~isempty(subSamples)
%         X = BeamData.RD.Samples(subSamples);
%         X = X(~isnan(X));
%         X200(kFais) = sum(X) / numel(X);
%     end
        
    subSamples = (isDeb100(iFais):isEnd100(iFais)) + (iFais-1) * 100;
    subSamples(subSamples > length(BeamData.RD.Samples)) = [];
    if ~isempty(subSamples)
        X = BeamData.RD.Samples(subSamples);
        if typeSnippets == 7058
            X = X(~isnan(X)); % Seul avant ajout
        else % Ajout JMA le 18/01/2018 pour donn�es 7125
            X = X(~isnan(X) & (X ~= 0));
        end
        if ~isempty(X)
            X100(kFais) = sum(X) / numel(X);
        end
    end
        
    subSamples = 50 + (iFais-1) * 100;
    if subSamples <= length(BeamData.RD.Samples)
        X1(kFais) = BeamData.RD.Samples(subSamples);
    end
end
% X200 = reflec_Enr2dB(X200);
X100 = reflec_Enr2dB(X100);
X1   = reflec_Enr2dB(X1);

%{
    FigUtils.createSScFigure;
    PlotUtils.createSScPlot(X100, '.r'); grid on; hold on
    PlotUtils.createSScPlot(X1,   '.k');
    legend({'X100'; 'X1'})
%}