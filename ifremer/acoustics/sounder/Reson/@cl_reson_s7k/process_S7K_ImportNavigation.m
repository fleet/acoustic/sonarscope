function process_S7K_ImportNavigation(~, listFileNames, nomFicNav)

N = length(listFileNames);
str1 = 'Vérification des .s7k';
str2 = 'Checking .s7k files.';
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    s7k(k) = cl_reson_s7k('nomFic', listFileNames{k}, 'KeepNcID', true); %#ok<AGROW>
end
my_close(hw);
identSondeur = selectionSondeur(s7k(1));
import_CaraibesNvi(s7k, nomFicNav, identSondeur)
