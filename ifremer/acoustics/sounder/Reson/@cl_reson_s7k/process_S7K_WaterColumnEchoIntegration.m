% ListeFicXmlEchogram{1} = 'C:\Demo_SSC-3DViewer\Marmesonet\WC\0307_20091113_163950_Lesuroit_Raw_PolarEchograms.xml';
% ListeFicXmlEchogram{1} = 'C:\Demo_SSC-3DViewer\Marmesonet\WC\0306_20091113_160950_Lesuroit_Raw_PolarEchograms.xml';
% ListeFicXmlEchogram{1} = 'C:\Temp\azerty\0307_20091113_163950_Lesuroit_Raw_PolarEchograms.xml';
% nomDirOut = my_tempdir
% process_S7K_WaterColumnEchoIntegration(ListeFicXmlEchogram, nomDirOut);

function process_S7K_WaterColumnEchoIntegration(~, ListeFicXmlEchogram, ListeFicS7k, nomDirOut, typePolarEchogram, params_ZoneEI, varargin)
 
global isUsingParfor %#ok<GVMIS>

isUsingParfor = 0;

[varargin, suby]    = getPropertyValue(varargin, 'suby',    []);
[varargin, Display] = getPropertyValue(varargin, 'Display', 0);
[varargin, Tag]     = getPropertyValue(varargin, 'Tag',     '');  %#ok<ASGLU>

useParallel = get_UseParallelTbx;

Carto = [];

if ~iscell(ListeFicXmlEchogram)
    ListeFicXmlEchogram = {ListeFicXmlEchogram};
end
if isempty(ListeFicXmlEchogram)
    return
end
nbFic = length(ListeFicXmlEchogram);

%% Question // tbx au cas o� les .all auraient d�j� �t� convertis

if Display == 0
    useParallelHere = useParallel;
else
    useParallelHere = 0;
end

%% Action

str1 = 'Calcul de l''�cho-int�gration de la colonne d''eau des sondeurs Reson';
str2 = 'Processing echo integration of WCD for Reson MBES';
if useParallelHere && (nbFic > 1)
    [hw, DQ] = create_parforWaitbar(nbFic, Lang(str1,str2));
    parfor (k=1:nbFic, useParallelHere)
        process_S7K_WaterColumnEchoIntegration_unitaire(ListeFicS7k{k}, ListeFicXmlEchogram{k}, ...
            nomDirOut, typePolarEchogram, Carto, Display, params_ZoneEI, Tag, ...
            suby, 'isUsingParfor', true);
        send(DQ, k);
    end
    my_close(hw, 'MsgEnd')
else
    hw = create_waitbar(Lang(str1,str2), 'N', nbFic);
    for k=1:nbFic
        my_waitbar(k, nbFic, hw)
        [flag, Carto] = process_S7K_WaterColumnEchoIntegration_unitaire(ListeFicS7k{k}, ListeFicXmlEchogram{k}, ...
            nomDirOut, typePolarEchogram, Carto, Display, params_ZoneEI, Tag, ...
            suby); %#ok<ASGLU>
    end
    my_close(hw, 'MsgEnd')
end


function [flag, Carto] = process_S7K_WaterColumnEchoIntegration_unitaire(ListeFicS7k, ListeFicXmlEchogram, ...
    nomDirOut, typePolarEchogram, Carto, Display, params_ZoneEI, Tag, suby, varargin)

global isUsingParfor %#ok<GVMIS>

[varargin, isUsingParfor] = getPropertyValue(varargin, 'isUsingParfor', isUsingParfor); %#ok<ASGLU>

I0 = cl_image_I0;

[flag, b, Carto] = sonar_ResonEchoIntegration(I0, typePolarEchogram, ...
    ListeFicXmlEchogram, params_ZoneEI, Tag, Display, ...
    'suby', suby, 'nomFicS7k', ListeFicS7k, 'Carto', Carto);
if flag
    for k=1:length(b)
        nomFicEI = b(k).Name;
        nomFicEI = fullfile(nomDirOut, [nomFicEI '.xml']);
        flag = export_xml(b(k), nomFicEI);
        if ~flag
            messageErreurFichier(nomFicEI, 'WriteFailure');
            my_warndlg(Lang(str1,str2), 1);
            break
        end
    end
end
