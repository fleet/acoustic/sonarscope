% Index(1,:) = Date(sub);
% Index(2,:) = Heure(sub);
% Index(3,:) = Position(sub);
% Index(4,:) = Taille(sub);
% Index(5,:) = TypeDatagram(sub);
% Index(6,:) = PingCounter(sub);
% Index(7,:) = PingSequence(sub);
% Index(8,:) = DeviceIdentifier(sub);

function [flag, DataIndex, identSondeur] = readFicIndex(this, identSondeur)

flag      = 1;
DataIndex = [];

nomFicIndex = setNomFicIndex(this, identSondeur);

[~, ~, Ext] = fileparts(nomFicIndex);
if strcmp(Ext, '.index')
    fidIndex = fopen(nomFicIndex, 'r', 'l');
    if fidIndex == -1
        str = ['Impossible d''ouvrir le fichier ' nomFicIndex];
        my_warndlg(str, 0, 'Tag', 'ImpossibleOuvrirFichierIndex');
        return
    end
    DataIndex = fread(fidIndex, [8 Inf], 'uint32');
    fclose(fidIndex);
else
    try
        Data = NetcdfUtils.Netcdf2Struct(nomFicIndex, 'Index', 'ncID', this.ncID);
        DataIndex    = Data.Index;
        identSondeur = Data.SounderName;
    catch
        flag = 0;
    end
end
