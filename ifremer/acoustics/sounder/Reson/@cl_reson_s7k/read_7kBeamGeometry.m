% Lecture des datagrams 
%
% Syntax
%   Data = read_7kBeamGeometry(a)
% 
% Input Arguments 
%   a : Instance de cl_reson_s7k
%
% Output Arguments 
%   Data : Structure contenant les 
%       xxxx : 
%
% Examples
%   nomFic = getNomFicDatabase('RESON7125_20051116_221245.s7k')
%   a = cl_reson_s7k('nomFic', nomFic);
%   Data = read_7kBeamGeometry(a)
%
% See also cl_reson_s7k cl_reson_s7k/plot_position cl_reson_s7k/plot_nav Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function Data = read_7kBeamGeometry(this, identSondeur, varargin)

global useCacheNetcdf %#ok<GVMIS>

[varargin, DataIndex] = getPropertyValue(varargin, 'DataIndex', []); %#ok<ASGLU>

%% Check the number of elements

flag = checkIfOneElement(length(this));
if ~flag
    Data = [];
    return
end

%% On regarde si le fichier .mat existe

[nomDir, nomFic] = fileparts(this.nomFic);
nomFicMat  = fullfile(nomDir, 'SonarScope', [nomFic '_7kBeamGeometry.mat']);
ncFileName = fullfile(nomDir, 'SonarScope', [nomFic '.nc']);

UN = 1;
if UN && exist(nomFicMat, 'file')
    Data = loadmat(nomFicMat, 'nomVar', 'Data');
    Data = simplifyStructure(Data);
    return
end

if UN && exist(ncFileName, 'file') && NetcdfUtils.existGrp(ncFileName, '7kBeamGeometry', 'ncID', this.ncID)
    Data = NetcdfUtils.Netcdf2Struct(ncFileName, '7kBeamGeometry', 'ncID', this.ncID);
    return
end

%% On cr�e au besoin le r�pertoire SonarScope

nomDirMat = fullfile(nomDir, 'SonarScope');
if ~exist(nomDirMat, 'dir')
    status = mkdir(nomDirMat);
    if status
%         disp(sprintf('Le repertoire %s a ete cree avec succes', nomDirMat))
    else
        fprintf('Impossible de c�er le r�pertoire %s.\nJe vous conseille de rendre cela possible, ca permet de formater la donn�e en fichiers matlab,\nce qui permettra d''acc�der beaucoup plus vite � la donn�e.\n', nomDirMat);
        nomFicMat = [];
    end
end

%% Lecture du fichier d'index

if isempty(DataIndex)
    [flag, DataIndex] = readFicIndex(this, identSondeur);
    if ~flag
        Data = [];
        return
    end
end

Position     = DataIndex(3,:);
TypeDatagram = DataIndex(5,:);

sub = (TypeDatagram == 7004);
Position = Position(sub);
clear TypeDatagram

if isempty(Position)
    str = sprintf('No "7004" datagrams in file %s', this.nomFic);
    my_warndlg(str, 0);
    Data = [];
    return
end

%% D�codage du fichier de donn�es

typeIndian = 'l';
[fid, message] = fopen(this.nomFic, 'r', typeIndian);
if fid < 0
    disp(message);
    Data = [];
    return
end

strLoop = infoLectureDatagrams(this, nomFicMat, 7004);
N = length(Position);
for k=1:N
    infoLoop(strLoop, k, N, 'pings')
    
    fseek(fid, Position(k), 'bof');
    
    [flag, DRF] = fread_DRF(fid);
    if ~flag
        break
    end
    [flag, BeamGeometry] = fread_RecordType_7004(fid);
    if ~flag
        break
    end
    

    Data.DRF(k) = DRF;
    Data.BeamGeometry(k) = BeamGeometry;
end
fclose(fid);
my_warndlg([strLoop ' : End'], 0);

Data = simplifyStructure(Data);

%% Sauvegarde de la donn�e dans un fichier .mat

if ~isempty(nomFicMat)
    try
        if useCacheNetcdf == 1 % 1=Mat, 2 et 3=Netcdf
            save(nomFicMat, 'Data')
        else
            NewData = Data;
            [NewData.Dimensions.nbPings, NewData.Dimensions.nbBeams] = size(NewData.BeamHorizontalDirectionAngle);
            NetcdfUtils.Struct2Netcdf(NewData, ncFileName, '7kBeamGeometry');
        end
    catch ME %#ok<NASGU>
        str = sprintf('Erreur ecriture fichier %s', nomFicMat);
        my_warndlg(str, 1);
    end
end


function NewData = simplifyStructure(Data)

if isfield(Data, 'Time')
    NewData = Data;
    return
end

for k=1:length(Data.BeamGeometry)
    NewData.DeviceIdentifier(k,1) = Data.DRF(k).DeviceIdentifier;
    NewData.Date(k,1)             = Data.DRF(k).Date;
    NewData.Time(k,1)             = Data.DRF(k).Time;
            
%                      ProtocolVersion: 5
%                       Offset: 60
%                  SyncPattern: 65535
%                         Size: 14160
%           OptionalDataOffset: 0
%       OptionalDataIdentifier: 0
%                         Date: 2455956
%                         Time: 2.553394001007080e+06
%                    Reserved1: 1
%         RecordTypeIdentifier: 7004
%             DeviceIdentifier: 7150
%             SystemEnumerator: 0
%                        Flags: 1
%     TotalRecordsInFragmented: 0
%             FragmentedNumber: 0    
            
    NewData.SonarId(k,1)                      = Data.BeamGeometry(k).RTH.SonarId;
    NewData.N(k,1)                            = Data.BeamGeometry(k).RTH.N;
    sub = 1:NewData.N(k,1);
    NewData.BeamVerticalDirectionAngle(k,sub)   = Data.BeamGeometry(k).RD.BeamVerticalDirectionAngle;
    NewData.BeamHorizontalDirectionAngle(k,sub) = Data.BeamGeometry(k).RD.BeamHorizontalDirectionAngle;
    NewData.s_3dBBeamWithX(k,sub)               = Data.BeamGeometry(k).RD.s_3dBBeamWithX;
    NewData.s_3dBBeamWithY(k,sub)               = Data.BeamGeometry(k).RD.s_3dBBeamWithY;
end
sub = (NewData.s_3dBBeamWithY == 0);
NewData.BeamVerticalDirectionAngle(sub)   = NaN;
NewData.BeamHorizontalDirectionAngle(sub) = NaN;
NewData.s_3dBBeamWithX(sub)               = NaN;
NewData.s_3dBBeamWithY(sub)               = NaN;
