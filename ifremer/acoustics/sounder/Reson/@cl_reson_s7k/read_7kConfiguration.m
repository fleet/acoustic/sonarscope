% Lecture des datagrams de SonarSettings dans un fichier .7sk
%
% Syntax
%   Data = read_7kConfiguration(this, identSondeur)
% 
% Input Arguments 
%   a : Instance de cl_reson_s7k
%
% Output Arguments 
%   Data : Structure contenant les 
%       
%
% Examples
%   nomFic = getNomFicDatabase('RESON7125_20051116_221245.s7k')
%   a = cl_reson_s7k('nomFic', nomFic);
%   identSondeur = selectionSondeur(a);
%   Data = plot_read_7kConfiguration(a, identSondeur)
%
%   figure; plot(Data.Longitude, Data.Latitude, '+'); grid on
%
% See also cl_reson_s7k cl_reson_s7k/plot_sonarSettings cl_reson_s7k/plot_nav Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function Data = read_7kConfiguration(this, identSondeur)

global useCacheNetcdf %#ok<GVMIS>

%% Check the number of elements

flag = checkIfOneElement(length(this));
if ~flag
    Data = [];
    return
end

%% On regarde si le fichier .mat existe

[nomDir, nomFic] = fileparts(this.nomFic);
nomFicMat  = fullfile(nomDir, 'SonarScope', [num2str(identSondeur) '_' nomFic '_7kConfiguration.mat']);
ncFileName = fullfile(nomDir, 'SonarScope', [nomFic '.nc']);

UN = 1;
if UN && exist(nomFicMat, 'file')
    Data = loadmat(nomFicMat, 'nomVar', 'Data');
    return
end
if UN && exist(ncFileName, 'file') && NetcdfUtils.existGrp(ncFileName, 'SonarSettings', 'ncID', this.ncID)
    Data = NetcdfUtils.Netcdf2Struct(ncFileName, 'SonarSettings', 'ncID', this.ncID);
    return
end

%% On cr�e au besoin le r�pertoire SonarScope

nomDirMat = fullfile(nomDir, 'SonarScope');
if ~exist(nomDirMat, 'dir')
    status = mkdir(nomDirMat);
    if status
% %         disp(sprintf('Le repertoire %s a ete cree avec succes', nomDirMat))
    else
        disp(sprintf('Impossible de c�er le repertoire %s.\nJe vous conseille de rendre cela possible, ca permet de formater la donnee en fichiers matlab,\nce qui permettra d''acceder beaucoup plus vite a la donnee', nomDirMat)) %#ok<DSPS>
        nomFicMat = [];
    end
end

%% Lecture du fichier d'index

[flag, DataIndex] = readFicIndex(this, identSondeur);
if ~flag
    Data = [];
    return
end

Position         = DataIndex(3,:);
TypeDatagram     = DataIndex(5,:);
PingCounter      = DataIndex(6,:);
PingSequence     = DataIndex(7,:);
DeviceIdentifier = DataIndex(8,:);

[flag, sub] = get_subDataCommon(7001, TypeDatagram, PingCounter, PingSequence, DeviceIdentifier, identSondeur);
if ~flag
    Data = [];
    return
end

if isempty(sub)
    % TODO : peut-�tre dire qu'il n'y a pas de 7001 ?
    Data = [];
    return
end

Position = Position(sub);
% PingCounter = PingCounter(sub);
clear TypeDatagram

%% D�codage du fichier de donn�es

typeIndian = 'l';
[fid, message] = fopen(this.nomFic, 'r', typeIndian);
if fid < 0
    disp(message);
    Data = [];
    return
end

strLoop = infoLectureDatagrams(this, nomFicMat, 7001);
N = length(Position);
for k=1:N
    infoLoop(strLoop, k, N, 'records')
    
    fseek(fid, Position(k), 'bof');
    
    flag = fread_DRF(fid);
    if ~flag
        break
    end
    [flag, Data] = fread_RecordType_7001(fid);
    if ~flag
        break
    end
end
fclose(fid);
my_warndlg([strLoop ' : End'], 0);

%% Sauvegarde de la donn�e dans un fichier .mat

if ~isempty(nomFicMat)
    try
        if useCacheNetcdf == 1 % 1=Mat, 2 et 3=Netcdf
            save(nomFicMat, 'Data')
        else
%             Data.Dimensions.nbPings = length(Data.PingCounter);
            NetcdfUtils.Struct2Netcdf(Data, ncFileName, 'SonarSettings');
        end
    catch %#ok<CTCH>
        str = sprintf('Erreur ecriture fichier %s', nomFicMat);
        my_warndlg(str, 1);
    end
end
