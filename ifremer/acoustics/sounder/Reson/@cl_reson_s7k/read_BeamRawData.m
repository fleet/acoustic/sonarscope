% Lecture des datagrams "RawData" dans un fichier .s7k
%
% Syntax
%   Data = read_BeamRawData(a, ...)
%
% Input Arguments
%   a : Instance de cl_reson_s7k
%
% Name-only Arguments
%   MasqueDetection : Suppression de valeurs masqu�es par MasqueDetection
%   MasqueDepth     : Suppression de valeurs masqu�es par Masque sur
%                     l'histogramme des Depth
%
% Output Arguments
%   Data : Structure contenant
%
% Examples
%   nomFic = getNomFicDatabase('RESON7125_20051116_221245.s7k')
%   a = cl_reson_s7k('nomFic', nomFic);
%   Data = read_BeamRawData(a)
%
%   b = view_depth(a);
%   b = editobj(b);
%
% See also cl_reson_s7k cl_reson_s7k/plot_position cl_reson_s7k/plot_nav Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [Data, BeamDataOut, subPing] = read_BeamRawData(this, identSondeur, varargin)

global useCacheNetcdf %#ok<GVMIS>

[varargin, DataDepth]    = getPropertyValue(varargin, 'DataDepth',    []);
[varargin, DataIndex]    = getPropertyValue(varargin, 'DataIndex',    []);
[varargin, subPing]      = getPropertyValue(varargin, 'subPing',      []);
[varargin, DisplayLevel] = getPropertyValue(varargin, 'DisplayLevel', 1);
[varargin, Mute]         = getPropertyValue(varargin, 'Mute',         0);
[varargin, flagMessage]  = getPropertyValue(varargin, 'flagMessage',  1); %#ok<ASGLU>

%% Check the number of elements

flag = checkIfOneElement(length(this));
if ~flag
    Data = [];
    BeamDataOut = [];
    return
end

%% R�cup�ration de la fr�quence d'�chantillonnage

Data7000 = read_sonarSettings(this, identSondeur, 'DataIndex', DataIndex);
Data = Data7000;
if isempty(Data7000)
    Data = [];
    BeamDataOut = [];
    return
end
VolatileSonarSettings_PingCounter = Data7000.PingCounter;

%% Lecture de la bathy

if isempty(DataDepth)
    [flag, DataDepth, identSondeur] = read_depth_s7k(this, 'DataIndex', DataIndex);
    if ~flag
        return
    end
end

if isempty(DataDepth.NbBeams)
    Data = [];
    BeamDataOut = [];
    return
else
    NbBeamsBathy = DataDepth.NbBeams(1);
end

%% On regarde si le fichier .mat existe

[nomFicBeamRawData, flagFilesExist] = ResonBeamRawDataNames(this.nomFic, identSondeur, []);

[~, ~, Ext] = fileparts(nomFicBeamRawData.Mat);
UN = 1;
flagMat = 0;
if UN && flagFilesExist && strcmp(Ext, '.mat')
    Data = loadmat(nomFicBeamRawData.Mat, 'nomVar', 'Data');
    flagMat = 1;
end
if  UN && flagFilesExist && strcmp(Ext, '.nc') && NetcdfUtils.existGrp(nomFicBeamRawData.Mat, 'BeamRawData', 'ncID', this.ncID)
    Data = NetcdfUtils.Netcdf2Struct(nomFicBeamRawData.Mat, 'BeamRawData', 'ncID', this.ncID);
    flagMat = 1;
end

%% Recherche automatique des num�ros de faisceaux limites

if flagMat
    try
        Data.Dimensions.nbPings = length(Data.PingCounter);
    catch
        Data = [];
        BeamDataOut = [];
        return
    end
else
    NbSoundes = sum(~isnan(DataDepth.Depth));
    NbSoundes = NbSoundes / sum(NbSoundes);
    NbSoundes = cumsum(NbSoundes);
    idebBeamBathy = find(NbSoundes > 0.02, 1, 'first');
    ifinBeamBathy = find(NbSoundes < 0.98, 1, 'last');
    subBeamsReson = idebBeamBathy:ifinBeamBathy;
    subBeamsSsc   = 1:length(subBeamsReson);
    
    %% Lecture du fichier d'index
    
    if isempty(DataIndex)
        [flag, DataIndex] = readFicIndex(this, identSondeur);
        if ~flag
            Data = [];
            return
        end
    end
    Position         = DataIndex(3,:);
    TypeDatagram     = DataIndex(5,:);
    PingCounter      = DataIndex(6,:);
    PingSequence     = DataIndex(7,:);
    DeviceIdentifier = DataIndex(8,:);
    
    % Lecture des datagrams TVG si ils existent.
    % Le fichier G:\SantaBarbara200805\20080513RAW\20080513_184723.s7k
    % produisait une erreue car il y avait 1 ping de moins en TVG qu'ne
    % donn�e BeamData
    %     isEmpty7010 = isempty(find(TypeDatagram == 7010, 1));
    isEmpty7010 = ~any(TypeDatagram == 7010);
    if isEmpty7010
        NbPings7010 = Inf;
    else
        NbPings7010 = length(find(TypeDatagram == 7010));
    end
    
    %     isEmpty7008 = isempty(find(TypeDatagram == 7008, 1));
    %     isEmpty7058 = isempty(find(TypeDatagram == 7058, 1));
    %     isEmpty7048 = isempty(find(TypeDatagram == 7048, 1));
    %     isEmpty7018 = isempty(find(TypeDatagram == 7018, 1));
    isEmpty7008 = ~any(TypeDatagram == 7008);
    isEmpty7018 = ~any(TypeDatagram == 7018);
    isEmpty7028 = ~any(TypeDatagram == 7028);
    isEmpty7058 = ~any(TypeDatagram == 7058);
    isEmpty7048 = ~any(TypeDatagram == 7048);
    if ~isEmpty7018 % Mag&Phase
        [flag, subBeamRawData] = get_subDataCommon(7018, TypeDatagram, PingCounter, PingSequence, DeviceIdentifier, identSondeur);
        if ~flag
            BeamDataOut = [];
            return
        end
    elseif ~isEmpty7008 % Ancien Mag&Phase
        [flag, subBeamRawData] = get_subDataCommon(7008, TypeDatagram, PingCounter, PingSequence, DeviceIdentifier, identSondeur);
        if ~flag
            BeamDataOut = [];
            return
        end
    elseif ~isEmpty7058 % Backscatter
        [flag, subBeamRawData] = get_subDataCommon(7058, TypeDatagram, PingCounter, PingSequence, DeviceIdentifier, identSondeur);
        if ~flag
            BeamDataOut = [];
            return
        end
    elseif ~isEmpty7028 % Ancien backscatter
        [flag, subBeamRawData] = get_subDataCommon(7028, TypeDatagram, PingCounter, PingSequence, DeviceIdentifier, identSondeur);
        if ~flag
            BeamDataOut = [];
            return
        end
    else
        if ~Mute
            str1 = sprintf('Il n''y a pas de Mag&Phase dans le fichier "%s" (7008 ou 7018 ou 7028 ou 7058).', this.nomFic);
            str2 = sprintf('There is no Mag&Phase in "%s" (7008 or 7018 or 7028 or 7058).', this.nomFic);
            my_warndlg(Lang(str1,str2), flagMessage, 'Tag', 'PasDeMag&PhaseDansCeFichier');
        end
        BeamDataOut = [];
        Data = [];
        return
    end
    
    if isempty(subBeamRawData)
        my_warndlg('No BeamRawData data in this file', 1);
        BeamDataOut = [];
        return
    end
    
    
    % PingCounter = PingCounter(subBeamRawData); % CHERCHER LA CORRESPONDANCE DES PINGS COUNTER PLUTOT QUE 1:min()
    %     subBeamRawData = subBeamRawData(1:min([NbPings7010, length(subBeamRawData), length(VolatileSonarSettings_PingCounter)]));
    
    % Modif JMA le 15/12/2010 � l'arrache pour fichier NOAA 7111 7111_20101107_175549
    subBeamRawData = subBeamRawData(1:min([NbPings7010, length(subBeamRawData), length(VolatileSonarSettings_PingCounter), length(DataDepth.PingCounter)]));
    Data.sub7000 = 1:length(subBeamRawData); % Rajout� le 31/10/2014 side effect de WC dans PDS
    Data.subWC   = 1:length(subBeamRawData); % Rajout� le 31/10/2014 side effect de WC dans PDS
    
    Position = Position(subBeamRawData);

    % Taille   = Taille(subBeamRawData);
    clear TypeDatagram
    
    %% Decodage du fichier de donn�es
    
    [fid, message] = fopen(this.nomFic, 'r', 'l');
    if fid < 0
        my_warndlg(['cl_reson_s7k:read_BeamRawData' message], 0);
        BeamDataOut = [];
        return
    end
    
    %% Lecture du nombre de faisceaux
    
    fseek(fid, Position(1), 'bof');
    [flag, DRF] = fread_DRF(fid); %#ok<ASGLU>
    if ~flag
        BeamDataOut = [];
        return
    end
    
    N = length(Position);
    DateDatagram  = NaN(1, N);
    HeureDatagram = NaN(1, N);
    
%     Data.Position                = Position;
    Data.PingCounter             = NaN(1, N, 'single');
    Data.MultiPingSequence       = NaN(1, N, 'single');
    Data.NbBeams                 = NaN(1, N, 'single');
    
    Data.BeamDescriptor          = NaN(N, 1, 'single');
    Data.BeginSampleDescriptor   = NaN(N, 1, 'single');
    Data.CenterSampleDistance    = NaN(N, 1, 'single');
    Data.BeamAcrossTrackDistance = NaN(N, 1, 'single');
    
    Data.Frequency = Data.Frequency / 1000; % Ajout JMA le 20/03/2016
    
    if ~isEmpty7018
        NumDatagram = 7018;
    elseif ~isEmpty7008
        NumDatagram = 7008;
    elseif ~isEmpty7058
        NumDatagram = 7058;
    elseif ~isEmpty7028
        NumDatagram = 7028;
    end
    
    strLoop = infoLectureDatagrams(this, nomFicBeamRawData.Mat, NumDatagram);
    for k=1:N
        infoLoop(strLoop, k, N, 'pings')
        
        fseek(fid, Position(k), 'bof');
        
        [flag, DRF] = fread_DRF(fid);
        if ~flag || (DRF.RecordTypeIdentifier ~= NumDatagram)
            N = k - 1;
            break
        end
        %         if DRF.Size == 492834816
        %             %% Erreur sur fichier
        %             my_warndlg('Error reading .s7k.', 1, 'Tag', 'Data.RTH.NTropFrand');
        %             break
        %         end
        
        if ~isEmpty7018
            [flag, BeamData] = fread_RecordType_7018(fid, 'NoRawData');
        elseif ~isEmpty7008
            [flag, PingNumber, Sequence, BeamData] = fread_RecordType_7008(fid, DRF, 'NoRawData'); %#ok<ASGLU>
        elseif ~isEmpty7058
            [flag, PingNumber, Sequence, BeamData] = fread_RecordType_7058(fid, DRF, NbBeamsBathy);%#ok<ASGLU> %, Range(iDepth,:));
            BeamData.RD.Samples = reflec_dB2Enr(single(BeamData.RD.Samples));
        elseif ~isEmpty7028
            [flag, PingNumber, Sequence, BeamData] = fread_RecordType_7028(fid, NbBeamsBathy);%#ok<ASGLU> %, Range(iDepth,:));
%             BeamData.RD.Samples = reflec_dB2Enr(single(BeamData.RD.Samples)); % Comment� par JMA le 05/02/2021 pour donn�es Carla 
        end
        if ~flag
            break
        end
        
        DateDatagram(k)  = DRF.Date;
        HeureDatagram(k) = DRF.Time;
        
        if k == 1
            Data.SystemSerialNumber = DRF.DeviceIdentifier;
        end
        Data.EmModel = BeamData.RTH.SonarId;
        
        if (DRF.OptionalDataOffset ~= 0) &&  ~isempty(BeamData.OD) && isfield(BeamData.OD, 'Frequency')
            Data.Frequency = BeamData.OD.Frequency / 1000; % En kHz
        end
        
        Data.PingCounter(k)       = BeamData.RTH.PingCounter;
        Data.MultiPingSequence(k) = BeamData.RTH.MultiPingSequence;
        Data.NbBeams(k)           = BeamData.RTH.N;
        %     Data.DataSampleType(k)  = BeamData.RTH.DataSampleType;
        
        if isfield(BeamData, 'RD') && isfield(BeamData.RD, 'BeamDescriptor')
            subOK = find(subBeamsReson <= length(BeamData.RD.BeamDescriptor));
            Data.BeamDescriptor(k,subBeamsSsc(subOK))          = BeamData.RD.BeamDescriptor(subBeamsReson(subOK));
            Data.BeginSampleDescriptor(k,subBeamsSsc(subOK))   = BeamData.RD.BeginSampleDescriptor(subBeamsReson(subOK));
            Data.EndSampleDescriptor(k,subBeamsSsc(subOK))     = BeamData.RD.EndSampleDescriptor(subBeamsReson(subOK));
        end
        if (DRF.OptionalDataOffset ~= 0) &&  ~isempty(BeamData.OD) && isfield(BeamData.OD, 'CenterSampleDistance')
            Data.CenterSampleDistance(k,subBeamsSsc(subOK))    = BeamData.OD.CenterSampleDistance(subBeamsReson(subOK));
            Data.BeamAcrossTrackDistance(k,subBeamsSsc(subOK)) = BeamData.OD.BeamAcrossTrackDistance(subBeamsReson(subOK));
        end
    end
    fclose(fid);
    my_warndlg([strLoop ' : end'], 0);
    
    Data.MinFilterInfo = DataDepth.MinFilterInfo;
    Data.MaxFilterInfo = DataDepth.MaxFilterInfo;
    
    if (k ~= N) || (length(Data.PingSequence) ~= N)
        N = k-1; % Au cas o� on est sorti en erreur
        subN = 1:N;
        DateDatagram  = DateDatagram(subN);
        HeureDatagram = HeureDatagram(subN);
        Position      = Position(subN);
        Data.PingCounter       = Data.PingCounter(1:N);
        Data.DateDatagram      = DateDatagram;
        Data.HeureDatagram     = HeureDatagram;
        Data.MultiPingSequence = Data.MultiPingSequence(subN);
        Data.NbBeams           = Data.NbBeams(subN);
        Data.MinFilterInfo     = Data.MinFilterInfo(subN);
        Data.MaxFilterInfo     = Data.MaxFilterInfo(subN);
        
        Data.Frequency                              = Data.Frequency(subN);
        Data.PingSequence                           = Data.PingSequence(subN);
        Data.SampleRate                             = Data.SampleRate(subN);
        Data.ReceiverBandwith                       = Data.ReceiverBandwith(subN);
        Data.TxPulseWidth                           = Data.TxPulseWidth(subN);
        Data.TxPulseTypeIdentifier                  = Data.TxPulseTypeIdentifier(subN);
        Data.TxPulseEnvelopeParameter               = Data.TxPulseEnvelopeParameter(subN);
        Data.TxPulseReserved                        = Data.TxPulseReserved(subN);
        Data.MaxPingRate                            = Data.MaxPingRate(subN);
        Data.PingPeriod                             = Data.PingPeriod(subN);
        Data.RangeSelection                         = Data.RangeSelection(subN);
        Data.PowerSelection                         = Data.PowerSelection(subN);
        Data.GainSelection                          = Data.GainSelection(subN);
        Data.ControlFlags                           = Data.ControlFlags(subN);
        Data.ProjectorMagicNumber                   = Data.ProjectorMagicNumber(subN);
        Data.ProjectorBeamSteeringAngleVertical     = Data.ProjectorBeamSteeringAngleVertical(subN);
        Data.ProjectorBeamSteeringAngleVertical     = Data.ProjectorBeamSteeringAngleVertical(subN);
        Data.ProjectorBeamSteeringAngleHorizontal   = Data.ProjectorBeamSteeringAngleHorizontal(subN);
        Data.ProjectionBeam_3dB_WidthVertical       = Data.ProjectionBeam_3dB_WidthVertical(subN);
        Data.ProjectionBeam_3dB_WidthHorizontal     = Data.ProjectionBeam_3dB_WidthHorizontal(subN);
        Data.ProjectionBeamFocalPoint               = Data.ProjectionBeamFocalPoint(subN);
        Data.ProjectionBeamWeightingWindowType      = Data.ProjectionBeamWeightingWindowType(subN);
        Data.ProjectionBeamWeightingWindowParameter = Data.ProjectionBeamWeightingWindowParameter(subN);
        Data.TransmitFlags                     = Data.TransmitFlags(subN);
        Data.HydrophoneMagicNumber             = Data.HydrophoneMagicNumber(subN);
        Data.ReceiveBeamWeightingWindow        = Data.ReceiveBeamWeightingWindow(subN);
        Data.ReceiveBeamWeightingParameter     = Data.ReceiveBeamWeightingParameter(subN);
        Data.ReceiveFlags                      = Data.ReceiveFlags(subN);
        Data.ReceiveBeamWidth                  = Data.ReceiveBeamWidth(subN);
        Data.BottomDetectionFilterInfoMinRange = Data.BottomDetectionFilterInfoMinRange(subN);
        Data.BottomDetectionFilterInfoMaxRange = Data.BottomDetectionFilterInfoMaxRange(subN);
        Data.BottomDetectionFilterInfoMinDepth = Data.BottomDetectionFilterInfoMinDepth(subN);
        Data.BottomDetectionFilterInfoMaxDepth = Data.BottomDetectionFilterInfoMaxDepth(subN);
        Data.BottomDetectRangeFilterFlag       = Data.BottomDetectRangeFilterFlag(subN);
        Data.BottomDetectDepthFilterFlag       = Data.BottomDetectDepthFilterFlag(subN);
        Data.BottomDetectDepthFilterFlag       = Data.BottomDetectDepthFilterFlag(subN);
        Data.Absorption                        = Data.Absorption(subN);
        Data.SoundVelocity                     = Data.SoundVelocity(subN);
        Data.Spreading                         = Data.Spreading(subN);
        Data.TxRollCompensationFlag            = Data.TxRollCompensationFlag(subN);
        Data.TxRollCompensationValue           = Data.TxRollCompensationValue(subN);
        Data.TxPitchCompensationFlag           = Data.TxPitchCompensationFlag(subN);
        Data.TxPitchCompensationValue          = Data.TxPitchCompensationValue(subN);
        Data.RxRollCompensationFlag            = Data.RxRollCompensationFlag(subN);
        Data.RxRollCompensationValue           = Data.RxRollCompensationValue(subN);
        Data.RxPitchCompensationFlag           = Data.RxPitchCompensationFlag(subN);
        Data.RxPitchCompensationValue          = Data.RxPitchCompensationValue(subN);
        Data.RxHeaveCompensationFlag           = Data.RxHeaveCompensationFlag(subN);
        Data.RxHeaveCompensationValue          = Data.RxHeaveCompensationValue(subN);
        Data.sub7000                           = Data.sub7000(subN);
        Data.subWC                             = Data.subWC(subN);
        Data.BeamDescriptor                    = Data.BeamDescriptor(subN);
        Data.BeginSampleDescriptor             = Data.BeginSampleDescriptor(subN);
        Data.CenterSampleDistance              = Data.CenterSampleDistance(subN);
        Data.BeamAcrossTrackDistance           = Data.BeamAcrossTrackDistance(subN);
    end
    
    Data.PingCounter = unwrapPingCounter(Data.PingCounter);
    Data.Time = timeIfr2Mat(DateDatagram, HeureDatagram);
    Data.Position = Position;
    Data.isEmpty7008 = isEmpty7008;
    Data.isEmpty7058 = isEmpty7058;
    Data.isEmpty7018 = isEmpty7018;
    Data.isEmpty7048 = isEmpty7048;
    Data.isEmpty7028 = isEmpty7028;
    
    %% Sauvegarde de la donn�e dans un fichier .mat
    
    if ~isempty(nomFicBeamRawData.Mat) && ~isempty(Data.PingCounter)
        try
            if strcmp(Ext, '.mat')
                save(nomFicBeamRawData.Mat, 'Data')
            else
                Data.Dimensions.nbPings = length(Data.PingCounter);
                Data.Dimensions.nbBeams = size(Data.BeginSampleDescriptor,2);
                Data.PingInBeamRawData = zeros(Data.Dimensions.nbPings, 1, 'uint8');
                NetcdfUtils.Struct2Netcdf(Data, nomFicBeamRawData.Mat, 'BeamRawData', 'ncID', this.ncID);
            end
        catch ME %#ok<NASGU,CTCH>
            str = sprintf('Write failure file %s', nomFicBeamRawData.Mat);
            my_warndlg(str, 1);
        end
    end
end

%% Lecture des donn�es Mag&Phase du ping

N = length(Data.PingCounter);
if isempty(subPing)
    subPing = 1:N;
else
    if strcmp(subPing, 'None')
        BeamDataOut = [];
        return
    else
        subPing(subPing > N) = [];
        if isempty(subPing)
            BeamDataOut = [];
            return
        end
    end
end
N = length(subPing);

[fid, message] = fopen(this.nomFic, 'r', 'l');
if fid < 0
    BeamDataOut = [];
    my_warndlg(['cl_reson_s7k:read_BeamRawData' message], 0);
    return
end

if useCacheNetcdf ~= 1 % Le cache est en netcdf. On lit une fois la variable PingInWCD au lieu de l'appeler � chaque ping (appel � la fonction get_PingInWCD(this, k, N, identSondeur))
    [nomFicBeamRawData, flagFilesExist] = ResonBeamRawDataNames(this.nomFic, identSondeur, []);
    if flagFilesExist
        ncID = netcdf.open(nomFicBeamRawData.Mat);
        grpID = netcdf.inqNcid(ncID, 'BeamRawData');
        varID = netcdf.inqVarID(grpID, 'PingInBeamRawData');
        PingInBeamRawData = netcdf.getVar(grpID, varID);
        netcdf.close(ncID);
    end
end

ncID  = [];
grpID = [];
varID = [];
flagVirtualMemory = (N > 10);
[~, nomFicSeul] = fileparts(this.nomFic);
strLoop = sprintf('S7K cache makeup Or Reading data : %s : Group BeamRawData', nomFicSeul);
if N > 1
    my_warndlg([strLoop ' : Begin'], 0);
end
for k=1:N
    kPing = subPing(k);
    
    [nomFicBeamRawData, flagFilesExist] = ResonBeamRawDataNames(this.nomFic, identSondeur, kPing);
    [~, ~, Ext] = fileparts(nomFicBeamRawData.Mat);
    if flagFilesExist && strcmp(Ext, '.mat')
        strLoop = sprintf('S7K reading data : %s : Group BeamRawData', nomFicSeul);
        infoLoop(strLoop, k, N, 'pings')
        
        X = loadmat(nomFicBeamRawData.AP, 'nomVar', 'BeamData');
        if isempty(X)
            BeamDataOut = [];
            return
        end
        BeamDataOut(k) = X; %#ok<AGROW>
        
%     elseif flagFilesExist && strcmp(Ext, '.nc') && NetcdfUtils.existGrp(nomFicBeamRawData.Mat, getPingGrpTreeNames(kPing), 'ncID', this.ncID)
%     elseif flagFilesExist && strcmp(Ext, '.nc') && get_PingInBeamRawData(this, k, N, identSondeur, 'ncID', ncID, 'ncID', grpID, 'varID', varID, 'Close', 0)
    elseif flagFilesExist && strcmp(Ext, '.nc') && PingInBeamRawData(kPing)
        strLoop = sprintf('S7K reading data : %s : Group BeamRawData', nomFicSeul);
        infoLoop(strLoop, k, N, 'pings')
        
        X = NetcdfUtils.Netcdf2Struct(nomFicBeamRawData.Mat, getPingGrpTreeNames(kPing), 'ncID', this.ncID);
        if isempty(X)
            BeamDataOut = [];
            return
        end
        BeamDataOut(k) = X; %#ok<AGROW>

    else
        nomDirImage = fileparts(nomFicBeamRawData.AP);
        if ~exist(nomDirImage, 'dir')
            [flag, msg] = mkdir(nomDirImage);
            if ~flag
                my_warndlg(msg, 1);
                BeamDataOut = [];
                return
            end
        end
        
        fseek(fid, Data.Position(kPing), 'bof');
        
        [flag, DRF] = fread_DRF(fid);
        if ~flag
            BeamDataOut = [];
            break
        end
        
        if ~Data.isEmpty7018
            [flag, BeamData] = fread_RecordType_7018(fid);
            codeDatagram = 7018;
        elseif ~Data.isEmpty7008
            [flag, PingNumber, Sequence, BeamData] = fread_RecordType_7008(fid, DRF); %#ok<ASGLU>
            codeDatagram = 7008;
        elseif ~Data.isEmpty7058
            [flag, PingNumber, Sequence, BeamData] = fread_RecordType_7058(fid, DRF, NbBeamsBathy);%#ok<ASGLU> %, Range(iDepth,:));
            BeamData.RD.Samples = reflec_dB2Enr(single(BeamData.RD.Samples));
            codeDatagram = 7058;
        elseif ~Data.isEmpty7028
            [flag, PingNumber, Sequence, BeamData] = fread_RecordType_7028(fid, NbBeamsBathy);%#ok<ASGLU> %, Range(iDepth,:));
%             BeamData.RD.Samples = reflec_dB2Enr(single(BeamData.RD.Samples)); % Comment� par JMA le 05/02/2021 pour donn�es Carla 
            codeDatagram = 7028;
        end
        strLoop = sprintf('S7K cache makeup : %s : Group BeamRawData (catching datagrams %d)', nomFicSeul, codeDatagram);
        infoLoop(strLoop, k, N, 'pings')
        
        if ~flag
            BeamDataOut = [];
            break
        end
        BeamData.iPing = kPing;
        
        try
            if strcmp(Ext, '.mat')
                save(nomFicBeamRawData.AP, 'BeamData')
            else
                if isfield(BeamData.RD, 'Amplitude')
%                     BeamData.RD.Dimensions.nbBeams   = length(BeamData.RD.BeamDescriptor);
%                     BeamData.RD.Dimensions.nbSamples = length(BeamData.RD.Samples);
%                     BeamData.OD.Dimensions.nbBeams   = length(BeamData.OD.CenterSampleDistance);
                    [this, ncID, grpID, varID] = set_PingInBeamRawData(this, kPing, Data.Dimensions.nbPings, identSondeur, 'ncID', ncID, 'grpID', grpID, 'varID', varID, 'Close', 0);
                    NetcdfUtils.Struct2Netcdf(BeamData, nomFicBeamRawData.Mat, getPingGrpTreeNames(kPing), 'ncID', ncID);
                    PingInBeamRawData(kPing) = 1;
                end
            end
        catch %#ok<CTCH>
            str = sprintf('Erreur ecriture fichier %s', nomFicBeamRawData.AP);
            my_warndlg(str, 1);
        end
        
        BeamDataOut(k).RTH = BeamData.RTH; %#ok<AGROW>
        BeamDataOut(k).RD  = BeamData.RD; %#ok<AGROW>
        if ~isempty(BeamData.OD)
            BeamDataOut(k).OD = BeamData.OD; %#ok<AGROW>
        end
    end
    
    if flagVirtualMemory
        if isfield(BeamDataOut(k).RD, 'Amplitude')
            % Ce n'est pas syst�matique si on n'a pas de 7008, 7041, 7048
            BeamDataOut(k).RD.Amplitude = cl_memmapfile('Value', BeamDataOut(k).RD.Amplitude, 'LogSilence', 1); %#ok<AGROW>
            BeamDataOut(k).RD.Phase     = cl_memmapfile('Value', BeamDataOut(k).RD.Phase,     'LogSilence', 1); %#ok<AGROW>
        end
    end
    
    if kPing > length(DataDepth.PingCounter)
        Data = [];
        BeamDataOut = [];
        return
    end
    
    BeamDataOut(k).RD.Range = DataDepth.Range(kPing,:); %#ok<AGROW>
    BeamDataOut(k).RD.BeamDepressionAngle = DataDepth.BeamDepressionAngle(kPing,:); %#ok<AGROW>
    
    if ~isfield(Data, 'isEmpty7028')
        Data.isEmpty7028 = 1; % Pour compatibilit� avec fichiers g�n�r�s avant introduction de ce champ.
    end
    
    if Data.isEmpty7028 || ~isfield(DataDepth, 'Flags')
        I = DataDepth.QualityFactor(kPing,:);
        subNan = isnan(I);
        W = warning;
        warning('off', 'all')
        I = bitand(uint8(I),12);
        I = single(bitshift(uint8(I),-2));
        warning(W)
        I(subNan) = NaN;
        BeamDataOut(k).RD.QualityFactor = I; %#ok<AGROW>
        
        fe = Data7000.SampleRate(kPing);
        %         coef = 2*fe / 1500;
        coef = fe; % Modif JMA le 19/03/2016
        if DisplayLevel > 1
            if length(N) > 10
                figure(10000+kPing);
            else
                figure(10000);
            end
            subplot(2,1,1);
        end
        subPh    = find(I == 2);
        subAmp   = find(I == 1);
        subBlend = find(I == 3);
    else
        I = DataDepth.Flags(kPing,:);
        subNan = isnan(I);
        W = warning;
        warning('off', 'all')
        I = bitand(uint8(I),3);
        
        warning(W)
        I(subNan) = NaN;
        BeamDataOut(k).RD.QualityFactor = I; %#ok<AGROW>
        
        fe = Data7000.SampleRate(kPing);
        %         coef = 2*fe / 1500;
        coef = fe / 1000; % Modif JMA le 19/03/2016
        if DisplayLevel > 1
            if length(N) > 10
                figure(10000+kPing);
            else
                figure(10000);
            end
        end
        subPh    = find(I == 2);
        subAmp   = find(I == 1);
        subBlend = find(I == 3);
    end
    
    RPh    = BeamDataOut(k).RD.Range(subPh)    * coef;
    RAmp   = BeamDataOut(k).RD.Range(subAmp)   * coef;
    RBlend = BeamDataOut(k).RD.Range(subBlend) * coef;
    if DisplayLevel > 1
        hp = subplot(2,1,1);
        delete(get(hp, 'children'))
        PlotUtils.createSScPlot(hp, subPh,    RPh,    '+r'); grid on; hold on;
        PlotUtils.createSScPlot(hp, subAmp,   RAmp,   '+k'); grid on;
        PlotUtils.createSScPlot(hp, subBlend, RBlend, '+m'); grid on; hold off;
        set(gca, 'YDir', 'reverse');
        title(sprintf('S7k detection for Ping %d', kPing))
    end
    
    sinA = sind(BeamDataOut(k).RD.BeamDepressionAngle);
    cosA = cosd(BeamDataOut(k).RD.BeamDepressionAngle);
    
    R = 1 + BeamDataOut(k).RD.Range * coef; % Ajout� par JMA le 20/03/2016
    XPh    = sinA(subPh)     .* R(subPh);
    XAmp   = sinA(subAmp)    .* R(subAmp);
    XBlend = sinA(subBlend)  .* R(subBlend);
    ZPh    = -cosA(subPh)    .* R(subPh);
    ZAmp   = -cosA(subAmp)   .* R(subAmp);
    ZBlend = -cosA(subBlend) .* R(subBlend);
    if DisplayLevel > 1
        hp = subplot(2,1,2);
        delete(get(hp, 'children'))
        PlotUtils.createSScPlot(hp, XPh,    ZPh,    '+r'); grid on; hold on;
        PlotUtils.createSScPlot(hp, XAmp,   ZAmp,   '+k'); grid on;
        PlotUtils.createSScPlot(hp, XBlend, ZBlend, '+m'); grid on; hold off
    end
end
fclose(fid);
if N > 1
    my_warndlg([strLoop ' : End'], 0);
end
if ~isempty(ncID)
    netcdf.close(ncID);
end


function grpNames = getPingGrpTreeNames(kPing)

grpNames = {'MagPhase'; ['P' num2str(kPing, '%05d')]};
