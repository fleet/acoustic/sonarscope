function [flag, Z, DataPosition, Colors] = read_Invite(this, identSondeur, Invite, nbColors, varargin)

[varargin, DataPosition] = getPropertyValue(varargin, 'DataPosition', []);
[varargin, DataDepth]    = getPropertyValue(varargin, 'DataDepth',    []);

[varargin, flagHisto] = getFlag(varargin, 'Histo'); %#ok<ASGLU>

Z = [];
Colors = jet(nbColors);

S0 = cl_reson_s7k.empty();

if isempty(DataPosition)
    DataPosition = read_navigation(this, identSondeur);
end

if isempty(DataDepth)
    [flag, DataDepth] = read_depth_s7k(this);
    if ~flag
        return
    end
end

[SonarTime, Latitude, Longitude, Heading, CourseVessel, Immersion, provenance] = getNavFromVariousDatagrams(S0, DataPosition, DataDepth); %#ok<ASGLU>

switch Invite
    case 'Frequency'
        Data = read_sonarSettings(this, identSondeur);
        if isempty(Data) || isempty(Data.Frequency)
            flag = 0;
            return
        end
        Z.Data = Data.Frequency / 1000;
        Z.Time = Data.Time;
        Z.Unit = 'kHz';
        
    case 'Time'
        Z.Data = SonarTime.timeMat;
        Z.Time = SonarTime;
        Z.Unit = '';
        
    case 'Heading'
        Colors = hsv(nbColors);
        Z.Data = Heading;
        Z.Time = SonarTime;
        Z.Unit = 'deg';
        
    case 'Speed'
%         if ~isfield(DataPosition, 'Speed') % Ajout JMA le 25/09/2021 pour donn�es Norbit "Les oursini�res" de Lucie. TODO : cela devrait �tre cr�� en amont
%             [~, DataPosition.Speed] = calCapFromLatLon(DataPosition.Latitude, DataPosition.Longitude, 'Time', DataPosition.Time);
%         end
        Z.Data = DataPosition.Speed;
        Z.Time = DataPosition.Time;
        Z.Unit = 'm/s';
        
    case 'CourseVessel'
        Colors = hsv(nbColors);
        Z.Data = CourseVessel;
        Z.Time = SonarTime;
        Z.Unit = 'deg';
        
    case 'Drift'
        Z.Data = CourseVessel(:) - Heading(:);
        Z.Time = SonarTime;
        Z.Unit = 'deg';
        
    case 'SoundSpeed'
        Z.Data = DataDepth.SoundSpeed;
        Z.Time = DataDepth.Time;
        Z.Unit = 'm/s';
        
        if all(isnan(Z.Data))
            DataSurfaceSoundSpeed = read_SurfaceSoundSpeed(this, identSondeur);
            if ~isempty(DataSurfaceSoundSpeed)
                Z.Data = DataSurfaceSoundSpeed.SoundVelocity;
                Z.Time = DataSurfaceSoundSpeed.Time;
                Z.Unit = 'm/s';
            end
        end
        
    case 'VerticalDepth' % SoundSpeed
        Data = read_verticalDepth(this, identSondeur);
        if isempty(Data) || ~isfield(Data, 'VerticalDepth') || isempty(Data.VerticalDepth)
            Z.Data = mean(DataDepth.Depth,2);
            Z.Time = DataDepth.Time;
        else
            Z.Data = Data.VerticalDepth;
            Z.Time = Data.Time;
        end
        Z.Unit = 'm';
        
        %{
        MeanVal = mean(DataDepth.Depth(:,:), 2, 'omitnan');
        Z.Time     = DataDepth.Time;
        Z.Datatime = DataDepth.Datetime;
        Z.Data     = MeanVal;
        Z.Unit     = 'm';
%}
        
    case 'VesselHeight'
        Data = read_navigation(this, identSondeur);
        Z.Data = Data.VesselHeight;
        Z.Time = Data.Time;
        Z.Unit = 'm';
        
    case {'VerticalDepth - VesselHeight'; 'VerticalDepth-VesselHeight'}
        Data = read_verticalDepth(this, identSondeur);
        if isempty(Data) || ~isfield(Data, 'VerticalDepth') || isempty(Data.VerticalDepth)
            flag = 0;
            return
        end
        Z1 = Data.VerticalDepth;
        T1 = Data.Time;
        
        Data = read_navigation(this, identSondeur);
        Z2 = Data.VesselHeight;
        T2 = Data.Time;
        
        Z.Time = T1;
        Z.Data = Z1 - my_interp1(T2, Z2, T1);
        Z.Unit = 'm';
        
    case 'VerticalDepthStd'
        Data = read_verticalDepth(this, identSondeur);
        ConstanteDeTemps = 1*60 ; % 1 mn
        [MeanVal, StdVal] = filtreAttitude(Data.Time, Data.VerticalDepth(:), ConstanteDeTemps); %#ok<ASGLU>
        Z.Data = StdVal;
        Z.Time = Data.Time;
        Z.Unit = 'm';
                
    case {'Distance Inter Pings'; 'DistanceInterPings'}
        Fuseau = floor((mean(Longitude+180, 'omitnan'))/6 + 1);
        if isnan(Fuseau)
            Z.Data = [];
            Z.Time = [];
            Z.Unit = 'm';
        else
            [~, D] = legs(Latitude, Longitude);
            D = D * (nm2km(1) * 1000);

            Wc = 0.05;
            [B,A] = butter(2,Wc);
            D = my_filtfilt(B, A, D, Wc);
%             xFishFilt = my_filtfilt(B, A, xFish, Wc);
%             yFishFilt = my_filtfilt(B, A, yFish, Wc);
%             if any(isnan(xFishFilt))
%                 my_breakpoint % TODO NaN propag� partout sur Croix de Sein : � corriger
%             else
%                xFish = xFishFilt;
%                yFish = yFishFilt;
%             end
%             D = sqrt(gradient(xFish).^2 + gradient(yFish).^2);
            if length(D) < 3
                flag = 0;
                return
            end
            D(1:2) = D(3);
            D(end-1:end) = D(end-2);
            D(end+1) = D(end);
            
            if flagHisto
                [N, bins] = histo1D(D);
                sub = (N ~= 0);
                figure(87698); semilogy(bins, N, '-');
                grid on; hold on;
                title('Histogram of Distance Inter Pings')
                semilogy(bins(sub), N(sub), '*');
            end
            Z.Data = D;
            Z.Time = SonarTime;
            Z.Unit = 'm';
        end
        
    case 'GainSelection'
        Data = read_sonarSettings(this, identSondeur);
        Z.Data = Data.GainSelection;
        Z.Time = Data.Time;
        Z.Unit = 'dB';
        
    case 'PowerSelection'
        Data = read_sonarSettings(this, identSondeur);
        Z.Data = Data.PowerSelection;
        Z.Time = Data.Time;
        Z.Unit = 'dB';
        
    case 'TxPulseWidth'
        Data = read_sonarSettings(this, identSondeur);
        Z.Data = Data.TxPulseWidth;
        Z.Time = Data.Time;
        Z.Unit = 'ms';
        
    case 'RangeSelection'
        Data = read_sonarSettings(this, identSondeur);
        Z.Data = Data.RangeSelection;
        Z.Time = Data.Time;
        Z.Unit = 'm';
        
    case 'RollMean'
        [flag, Z] = create_signal_Attitude(this, identSondeur, 'Roll', 'Mean');
        if ~flag
            return
        end
        
    case 'RollStd'
        [flag, Z] = create_signal_Attitude(this, identSondeur, 'Roll', 'Std');
        if ~flag
            return
        end
        
    case 'PitchMean'
        [flag, Z] = create_signal_Attitude(this, identSondeur, 'Pitch', 'Mean');
        if ~flag
            return
        end
        
    case 'PitchStd'
        [flag, Z] = create_signal_Attitude(this, identSondeur, 'Pitch', 'Std');
        if ~flag
            return
        end        
    case 'HeaveMean'
        [flag, Z] = create_signal_Attitude(this, identSondeur, 'Heave', 'Mean');
        if ~flag
            return
        end    
        
    case 'HeaveStd'
        [flag, Z] = create_signal_Attitude(this, identSondeur, 'Heave', 'Std');
        if ~flag
            return
        end
        
    case 'MeanQF'
        dataFromPP = (identSondeur == 7150) || (identSondeur == 7111);
        if dataFromPP % Donn�es Ifremer du PP?
            I = DataDepth.Reflectivity(:,:);
        else
            my_breakpoint % Pas test�
            I = DataDepth.QualityFactor(:,:);
            I = -log10(I)-1;
        end
        Z.Time     = DataDepth.Time;
        Z.Datatime = DataDepth.Datetime;
        Z.Data     = mean(I, 2, 'omitnan');
        Z.Unit     = '';
        
    case 'NbOfBeams'
        MeanVal = sum(~isnan(DataDepth.Depth(:,:)), 2);
        Z.Time     = DataDepth.Time;
        Z.Datatime = DataDepth.Datetime;
        Z.Data     = MeanVal;
        Z.Unit     = '';
        
    case 'NbOfBeamsStd'
        MeanVal = sum(~isnan(DataDepth.Depth(:,:)), 2);
        ConstanteDeTemps = 1*60 ; % 1 mn
        [~, StdVal] = filtreAttitude(DataDepth.Time, MeanVal, ConstanteDeTemps);
        Z.Time     = DataDepth.Time;
        Z.Datatime = DataDepth.Datetime;
        Z.Data     = StdVal;
        Z.Unit     = '';
        
    case 'DepthMean'
        MeanVal = mean(DataDepth.Depth(:,:), 2, 'omitnan');
        Z.Time     = DataDepth.Time;
        Z.Datatime = DataDepth.Datetime;
        Z.Data     = MeanVal;
        Z.Unit     = 'm';
        
    case 'DepthStd'
        MeanVal = mean(DataDepth.Depth(:,:), 2, 'omitnan');
        ConstanteDeTemps = 1*60 ; % 1 mn
        [~, StdVal] = filtreAttitude(DataDepth.Time, MeanVal, ConstanteDeTemps);
        Z.Time     = DataDepth.Time;
        Z.Datatime = DataDepth.Datetime;
        Z.Data     = StdVal;
        Z.Unit     = 'm';

    otherwise
        str = sprintf('%s not yet available in plot_position_data', Invite);
        my_warndlg(str, 1);
end
Z.Datetime = datetime(Z.Time.timeMat, 'ConvertFrom', 'datenum');
flag = 1;
