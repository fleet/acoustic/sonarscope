% Lecture des datagrams "depth" dans un fichier .s7k
%
% Syntax
%   Data = read_SidescanDataIFR(a, ...)
%
% Input Arguments
%   a : Instance de cl_reson_s7k
%
% Name-only Arguments
%   MasqueDetection : Suppression de valeurs masqu�es par MasqueDetection
%   MasqueDepth     : Suppression de valeurs masqu�es par Masque sur
%                     l'histogramme des Depth
%
% Output Arguments
%   Data : Structure contenant Depth, AcrossDist, AlongDist, AlongDist,
%          BeamAzimuthAngle, Range, QualityFactor, LengthDetection et Reflectivity
%
% Examples
%   nomFic = getNomFicDatabase('RESON7125_20051116_221245.s7k')
%   a = cl_reson_s7k('nomFic', nomFic);
%   Data = read_SidescanDataIFR(a)
%   figure; imagesc(Data.Depth)
%   figure; imagesc(Data.AcrossDist)
%   figure; imagesc(Data.AlongDist)
%   figure; imagesc(Data.BeamDepressionAngle)
%   figure; imagesc(Data.BeamAzimuthAngle)
%   figure; imagesc(Data.Range)
%   figure; imagesc(Data.QualityFactor)
%   figure; imagesc(Data.LengthDetection)
%   figure; imagesc(Data.Reflectivity)
%
%   b = view_depth(a);
%   b = editobj(b);
%
% See also cl_reson_s7k cl_reson_s7k/plot_position cl_reson_s7k/plot_nav Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [Data, b, Carto] = read_SidescanDataIFR(this, identSondeur, varargin)

[varargin, Carto] = getPropertyValue(varargin, 'Carto', []); %#ok<ASGLU>

b = [];

%% Check the number of elements

flag = checkIfOneElement(length(this));
if ~flag
    Data = [];
    return
end

%% R�cuperation de la fr�quence d'�chantillonnage

Data = read_sonarSettings(this, identSondeur);
fe = Data.SampleRate; % Hz
% VolatileSonarSettings_PingCounter = Data.PingCounter;

%% Lecture de la bathy

[flag, DataDepth, identSondeur] = read_depth_s7k(this);
if ~flag
    return
end

%% On regarde si le fichier .mat existe

[nomDir, nomFic] = fileparts(this.nomFic);
[nomFicSidescan, flagFilesExist] = ResonSidescanIFRNames(this.nomFic, identSondeur);
[~, ~, Ext] = fileparts(nomFicSidescan.Mat);

UN = 1;
if UN && flagFilesExist
    if strcmp(Ext, '.mat')
        Data = loadmat(nomFicSidescan.Mat, 'nomVar', 'Data');
    elseif strcmp(Ext, '.nc') && NetcdfUtils.existGrp(nomFicSidescan.Mat, 'SidescanIFRData', 'ncID', this.ncID)
        Data = NetcdfUtils.Netcdf2Struct(nomFicSidescan.Mat, 'SidescanIFRData', 'ncID', this.ncID);
    end
    
    clear b
    [flag, b(1)] = cl_image.import_ermapper(nomFicSidescan.ReflectivityErs);
    if ~flag
        return
    end
    [flag, b(2)] = cl_image.import_ermapper(nomFicSidescan.RxBeamIndexErs);
    if ~flag
        return
    end
    [flag, b(3)] = cl_image.import_ermapper(nomFicSidescan.TxAngleErs);
    if ~flag
        return
    end
    [flag, b(4)] = cl_image.import_ermapper(nomFicSidescan.NbpErs);
    if ~flag
        return
    end
    
    if isempty(Carto)
        Carto  = get(b(1), 'Carto');
    else
        for k=1:length(b)
            b(k) = set(b(k), 'Carto', Carto);
        end
    end
    
    for k=1:length(b)
        InitialFileName = this.nomFic;
        InitialFileFormat = 'ResonS7k';
        b(k) = set(b(k), 'TagSynchroY', [num2str(identSondeur) '_' nomFic], ...
            'InitialFileName',   InitialFileName, ...
            'InitialFileFormat', InitialFileFormat);
    end
    
    b(1) = set(b(1), 'TagSynchroX', [num2str(identSondeur) '_' nomFic ' - PingSamples']);
    b(2) = set(b(2), 'TagSynchroX', [num2str(identSondeur) '_' nomFic ' - PingSamples']);
    b(3) = set(b(3), 'TagSynchroX', [num2str(identSondeur) '_' nomFic ' - PingSamples']);
    b(4) = set(b(4), 'TagSynchroX', [num2str(identSondeur) '_' nomFic ' - PingSamples']);
    return
end

%% On cr�e au besoin le repertoire SonarScope

nomDirMat = fullfile(nomDir, 'SonarScope');
if ~exist(nomDirMat, 'dir')
    status = mkdir(nomDirMat);
    if status
        %         disp(sprintf('Le repertoire %s a ete cree avec succes', nomDirMat))
    else
        fprintf('Impossible de ceer le repertoire %s.\nJe vous conseille de rendre cela possible, ca permet de formater la donnee en fichiers matlab,\nce qui permettra d''acceder beaucoup plus vite a la donnee.\n', nomDirMat)
        nomFicMat = [];
    end
end

%% Lecture du fichier d'index

[flag, DataIndex] = readFicIndex(this, identSondeur);
if ~flag
    Data = [];
    return
end

Position         = DataIndex(3,:);
TypeDatagram     = DataIndex(5,:);
PingCounter      = DataIndex(6,:);
PingSequence     = DataIndex(7,:);
DeviceIdentifier = DataIndex(8,:);

isEmpty7008 = isempty(find(TypeDatagram == 7008, 1));
isEmpty7058 = isempty(find(TypeDatagram == 7058, 1));
isEmpty7028 = isempty(find(TypeDatagram == 7028, 1));
if ~isEmpty7058
    [flag, subDataSnippet] = get_subDataCommon(7058, TypeDatagram, PingCounter, PingSequence, DeviceIdentifier, identSondeur);
    if ~flag
        return
    end
elseif ~isEmpty7008
    [flag, subDataSnippet] = get_subDataCommon(7008, TypeDatagram, PingCounter, PingSequence, DeviceIdentifier, identSondeur);
    if ~flag
        return
    end
% elseif ~isEmpty7058
%     [flag, subDataSnippet] = get_subDataCommon(7058, TypeDatagram, PingCounter, PingSequence, DeviceIdentifier, identSondeur);
%     if ~flag
%         return
%     end
elseif ~isEmpty7028
    [flag, subDataSnippet] = get_subDataCommon(7028, TypeDatagram, PingCounter, PingSequence, DeviceIdentifier, identSondeur);
    if ~flag
        return
    end
else
    my_warndlg('No 7008 nor 7058 datagrams in this file', 1);
    return
end
clear DeviceIdentifier

if isempty(subDataSnippet)
    my_warndlg('No Snippet data in this file', 1);
    return
end


% subDataSnippet = subDataSnippet(1:min(length(subDataSnippet), length(VolatileSonarSettings_PingCounter)));
%
% Position = Position(subDataSnippet);
% Taille   = Taille(subDataSnippet);
clear TypeDatagram

% [subl_Depth, subl_Sidescan] = intersect3(DataDepth.PingCounter, PingCounter(subDataSnippet), PingCounter(subDataSnippet));
% subDataSnippet = subDataSnippet(subl_Sidescan);

PS  = PingSequence(subDataSnippet);
subNon0 = (PS ~= 0);
PS(subNon0) = PS(subNon0) - 1;
PSnippets = PingCounter(subDataSnippet) * 4 + PS;

PS  = DataDepth.MultiPingSequence;
subNon0 = (PS ~= 0);
PS(subNon0) = PS(subNon0) - 1;
PDepth = DataDepth.DepthPingCounterBeforeUnwrap * 4 + PS;

% [subl_Sidescan, subl_Depth] = intersect3(PingCounter(subDataSnippet), DataDepth.PingCounter, DataDepth.PingCounter);
[subl_Sidescan, subl_Depth] = intersect3(PSnippets, PDepth, PDepth);
subDataSnippet = subDataSnippet(subl_Sidescan);
if isempty(subl_Depth)
    my_warndlg('No snippets data in this file', 1);
    return
end

Position = Position(subDataSnippet);

%% D�codage du fichier de donn�es

typeIndian = getIndianAll(this.nomFic);
% flagEndian = strcmp(typeIndian, 'b');
[fid, message] = fopen(this.nomFic, 'r', typeIndian);
if fid < 0
    my_warndlg(['cl_reson_s7k:read_SidescanDataIFR' message], 0);
    return
end

%% Lecture du nombre de faisceaux

fseek(fid, Position(1), 'bof');
flag = fread_DRF(fid);
if ~flag
    return
end

N = length(Position);

DateDatagram  = NaN(1, N);
HeureDatagram = NaN(1, N);

Data.PingCounter             = NaN(1, N, 'single');
Data.MultiPingSequence       = NaN(1, N, 'single');
Data.NbBeams                 = NaN(1, N, 'single');

Data.BeamDescriptor          = NaN(N, 1, 'single');
Data.BeginSampleDescriptor   = NaN(N, 1, 'single');
Data.Reflectivity            = NaN(N, 1, 'single');
Data.CenterSampleDistance    = NaN(N, 1, 'single');
Data.BeamAcrossTrackDistance = NaN(N, 1, 'single');

if isempty(nomFicSidescan.Mat)
    [nomDir, nomFic, ext] = fileparts(this.nomFic); %#ok
    str = sprintf('Loading file %s', nomFic);
else
    [~, nomFic]  = fileparts(this.nomFic);
    [~, nomFic2] = fileparts(nomFicSidescan.Mat);
    str = sprintf('Loading file %s\nCreation file %s', nomFic, nomFic2);
end


[nomDirFic, nomFicSauve] = fileparts(nomFicSidescan.ReflectivityErs);
nomDirImage = fullfile(nomDirFic, nomFicSauve);
if ~exist(nomDirImage, 'dir')
    if exist(nomDirImage, 'file')
        delete(nomDirImage)
    end
    flag = mkdir(nomDirFic, nomFicSauve); %#ok
end
nomFicSauve = fullfile(nomDirImage, nomFicSauve);
fidReflectivity = fopen(nomFicSauve, 'w+');
if fidReflectivity == -1
    str = ['Write failure file ' nomFicSauve];
    my_warndlg(str, 1);
    return
end

[nomDirFic, nomFicSauve] = fileparts(nomFicSidescan.RxBeamIndexErs);
nomDirImage = fullfile(nomDirFic, nomFicSauve);
if ~exist(nomDirImage, 'dir')
    if exist(nomDirImage, 'file')
        delete(nomDirImage)
    end
    flag = mkdir(nomDirFic, nomFicSauve); %#ok
end
nomFicSauve = fullfile(nomDirImage, nomFicSauve);
fidRxBeamIndex = fopen(nomFicSauve, 'w+');
if fidRxBeamIndex == -1
    str = ['Write failure file ' nomFicSauve];
    my_warndlg(str, 1);
    return
end

[nomDirFic, nomFicSauve] = fileparts(nomFicSidescan.TxAngleErs);
nomDirImage = fullfile(nomDirFic, nomFicSauve);
if ~exist(nomDirImage, 'dir')
    if exist(nomDirImage, 'file')
        delete(nomDirImage)
    end
    flag = mkdir(nomDirFic, nomFicSauve); %#ok
end
nomFicSauve = fullfile(nomDirImage, nomFicSauve);
fidTxAngle = fopen(nomFicSauve, 'w+');
if fidTxAngle == -1
    str = ['Write failure file ' nomFicSauve];
    my_warndlg(str, 1);
    return
end

[nomDirFic, nomFicSauve] = fileparts(nomFicSidescan.NbpErs);
nomDirImage = fullfile(nomDirFic, nomFicSauve);
if ~exist(nomDirImage, 'dir')
    if exist(nomDirImage, 'file')
        delete(nomDirImage)
    end
    flag = mkdir(nomDirFic, nomFicSauve); %#ok
end
nomFicSauve = fullfile(nomDirImage, nomFicSauve);
fidNbp = fopen(nomFicSauve, 'w+');
if fidNbp == -1
    str = ['Write failure file ' nomFicSauve];
    my_warndlg(str, 1);
    return
end

% Grosse bidouille :bug � corriger � partir du fichier C:\Temp\BUG-IMAGERIE_7150\20090511_205839_PP-7150.s7k
% subl_Depth = 1:min(size(DataDepth.Range,1), length(subDataSnippet));


NbBeamsBathy = DataDepth.NbBeams(1);
Range        = DataDepth.Range(subl_Depth,:);
QF           = DataDepth.QualityFactor(subl_Depth,:);
QF(isnan(QF)) = 0;

% Brightness
I = single(bitand(uint8(QF),1));
if any(I(:))
    Range(I == 0) = NaN;
end

% Colinearity'
I = single(bitand(uint8(QF),2)) / 2;
if any(I(:))
    Range(I == 0) = NaN;
end

% NadirFilter'
I = single(bitand(uint8(QF),32)) / 32;

% EN ATTENDANT QUE RESON INVERSE CE BIT
I = 1 - I; %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% EN ATTENDANT QUE RESON INVERSE CE BIT

if any(I(:))
    Range(I == 0) = NaN;
end

subNegatif = (DataDepth.AcrossDist(subl_Depth,:) < 0);
if  all(subNegatif(:) == 0)
    % Cas des fichiers de l'UNH (sans PDS)
    subNegatif = (DataDepth.BeamDepressionAngle(subl_Depth,:) < 0);
end
Range(subNegatif) = -Range(subNegatif);
clear subNegatif

RangeMax = max(Range, [], 2, 'omitnan');
RangeMin = min(Range, [], 2, 'omitnan');
DeltaD = 1500 ./ (2 * fe(subl_Depth)');
ColMin = min(floor(RangeMin(:) ./ DeltaD(:)))-1;
ColMax = max(ceil( RangeMax(:) ./ DeltaD(:)));
NbCol = ColMax - ColMin + 1;

hw = create_waitbar(str, 'N', N);
for kPing=N:-1:1
    iDepth = subl_Depth(kPing); %N-kPing+1;
    my_waitbar((N-kPing+1), N, hw);
    
    fseek(fid, Position(kPing), 'bof');
    
    [flag, DRF] = fread_DRF(fid);
    if ~flag
        break
    end
    
    if ~isEmpty7058
        [flag, PingNumber, Sequence, BeamData] = fread_RecordType_7058(fid, DRF, DataDepth.NbBeams(iDepth));%#ok<ASGLU> %, Range(iDepth,:));
        BeamData.RD.Samples = reflec_dB2Enr(single(BeamData.RD.Samples));
    elseif ~isEmpty7008
        [flag, PingNumber, Sequence, BeamData] = fread_RecordType_7008(fid, DRF); %#ok<ASGLU>
        BeamData.RD.Samples = single(BeamData.RD.Samples) .^ 2;
    elseif ~isEmpty7028
        [flag, PingNumber, Sequence, BeamData] = fread_RecordType_7028(fid, DataDepth.NbBeams(iDepth));%#ok<ASGLU> %, Range(iDepth,:));
        BeamData.RD.Samples = reflec_dB2Enr(single(BeamData.RD.Samples)/32768); % TODO
    end
    if ~flag
        break
    end
    
    %     if ~isfield(BeamData, 'OD')
    %         str1 = 'Ce fichier ne contient pas de donn�es optionnelles, il est impossible de replacer les �chantillons sonar';
    %         str2 = 'TODO';
    %         my_warndlg(Lang(str1,str2), 1);
    %     end
    
    if ~isfield(BeamData, 'OD')
        % Cas de donn�es de l'UNH sans Bottom Detector !!!!!
        nbBeams = size(DataDepth.Range, 2);
        BeamData.OD.CenterSampleDistance    =  BeamData.RD.EndSampleDescriptor - BeamData.RD.BeginSampleDescriptor;
        BeamData.OD.BeamAcrossTrackDistance =  cosd(DataDepth.BeamDepressionAngle) .* (DataDepth.Range ./ (2 * repmat(fe(:), 1, nbBeams)));
        BeamData.OD.Frequency = 100; % En Hz % TODO O� peut-on r�cup�rer la fr�quence ?
    end
    
    subBeamsReson = 1:DataDepth.NbBeams(iDepth);
    subBeamsSsc = subBeamsReson;
    
    CenterSampleDistance    = BeamData.OD.CenterSampleDistance(subBeamsReson);
    BeamAcrossTrackDistance = BeamData.OD.BeamAcrossTrackDistance(subBeamsReson);
    %     if BeamAcrossTrackDistance(iFais) < 0
    %         signe = -1;
    %     else
    %         signe = 1;
    %     end
    signe = sign(BeamAcrossTrackDistance);
    RangeCentre  = CenterSampleDistance .* signe;
    
    DateDatagram(kPing)  = DRF.Date;
    HeureDatagram(kPing) = DRF.Time;
    
    %     BeamData.RTH
    %     BeamData.RD
    
    Data.SystemSerialNumber = DRF.DeviceIdentifier;
    Data.EmModel            = BeamData.RTH.SonarId;
    Data.Frequency          = BeamData.OD.Frequency / 1000; % En kHz
    
    Data.PingCounter(kPing)     = BeamData.RTH.PingCounter;
    Data.MultiPingSequence(kPing) = BeamData.RTH.MultiPingSequence;
    Data.NbBeams(kPing)         = BeamData.RTH.N;
    %     Data.DataSampleType(kPing)  = BeamData.RTH.DataSampleType;
    
    Data.BeamDescriptor(kPing,subBeamsSsc)          = BeamData.RD.BeamDescriptor(subBeamsReson);
    Data.BeginSampleDescriptor(kPing,subBeamsSsc)   = BeamData.RD.BeginSampleDescriptor(subBeamsReson);
    Data.EndSampleDescriptor(kPing,subBeamsSsc)     = BeamData.RD.EndSampleDescriptor(subBeamsReson);
    Data.CenterSampleDistance(kPing,subBeamsSsc)    = BeamData.OD.CenterSampleDistance(subBeamsReson);
    Data.BeamAcrossTrackDistance(kPing,subBeamsSsc) = BeamData.OD.BeamAcrossTrackDistance(subBeamsReson);
    
    [Samples, Angles] = process_Snippet(DataDepth, BeamData, iDepth, fe(kPing), Data.Frequency, 1, DataDepth.NbBeams(iDepth));
    
    [Reflec, RxBeamIndex, Nbp, Angles] = calcul_Snippet2Sample(NbCol, ColMin, subBeamsReson, BeamData.RD.BeamDescriptor, Samples, Angles, RangeCentre);
    
    fwrite(fidTxAngle,      Angles,        'float32');
    fwrite(fidNbp,          Nbp,           'float32');
    fwrite(fidReflectivity, Reflec,        'float32');
    fwrite(fidRxBeamIndex,  RxBeamIndex,   'float32');
end
my_close(hw)
fclose(fid);
fclose(fidReflectivity);
fclose(fidRxBeamIndex);
fclose(fidTxAngle);
fclose(fidNbp);

NbSamples = length(Reflec);
clear BeamData Beams DRF PingCounter PingSequence Position Samples Taille TxAngle
clear VolatileSonarSettings_PingCounter fid fidIndex fidReflectivity fidRxBeamIndex
clear fidTxAngle fidNbp flag hw hTitre kPing iDepth isEmpty7008 isEmpty7058 message
clear nomDir nomDirFic nomDirImage nomFic2 nomFicIndex nomFicSauve str subDataSnippet
clear subBeamsSsc subBeamsReson typeIndian varargin

Data.PingCounter = unwrapPingCounter(Data.PingCounter);
Data.Time = cl_time('timeIfr', DateDatagram, HeureDatagram);
clear DateDatagram HeureDatagram

GeometryType = cl_image.indGeometryType('PingSamples');
InitialFileFormat = 'RESONs7k';
y = N:-1:1;
x = double(ColMin:ColMax);


[~, IdentSonar] = DeviceIdentifier2IdentSonar(cl_reson_s7k.empty(), identSondeur);

iMode = 1;
SonarDescription = cl_sounder('Sonar.Ident', IdentSonar, 'Sonar.Mode_1', iMode);

Reflectivity = cl_memmapfile('FileName', nomFicSidescan.Reflectivity, 'FileCreation', 0, ...
    'Format', 'single', 'Size', [N NbSamples], 'ValNaN', NaN, 'ErMapper', 1);

nomFic = [num2str(identSondeur) '_' nomFic];
DataType = cl_image.indDataType('Reflectivity');
b = cl_image('Image', Reflectivity, 'Name', [nomFic ' - SidescanIFR'], 'Unit', 'dB', ...
    'DataType', DataType, 'ColormapIndex', 2, ...
    'TagSynchroX', [nomFic ' - PingSamples'], 'TagSynchroY', nomFic,...
    'x', x, 'XUnit', 'num', 'y', y, 'YUnit', 'Pings', ...
    'InitialFileName',   this.nomFic, ...
    'InitialFileFormat', InitialFileFormat, ...
    'GeometryType',   GeometryType,...
    'SonarDescription',   SonarDescription);
b.Writable = false;

% EN ATTENDANT DE POUVOIR CREER UN SIGNAL HORIZONTAL QUI SERAIT RxBeamIndex
RxBeamIndex = cl_memmapfile('FileName', nomFicSidescan.RxBeamIndex, 'FileCreation', 0, ...
    'Format', 'single', 'Size', [N NbSamples], 'ValNaN', NaN, 'ErMapper', 1);
DataType = cl_image.indDataType('RxBeamIndex');
b(2) = cl_image('Image', RxBeamIndex, 'Name', [nomFic ' - SidescanIFR'], 'Unit', 'num', ...
    'DataType', DataType, 'ColormapIndex', 3, ...
    'TagSynchroX', [nomFic ' - PingSamples'], 'TagSynchroY', nomFic,...
    'x', x, 'XUnit', 'm', 'y', y, 'YUnit', 'Pings', ...
    'InitialFileName',   this.nomFic, ...
    'InitialFileFormat', InitialFileFormat, ...
    'GeometryType',   GeometryType,...
    'SonarDescription',   SonarDescription);
b(2).Writable = false;


TxAngle = cl_memmapfile('FileName', nomFicSidescan.TxAngle, 'FileCreation', 0, ...
    'Format', 'single', 'Size', [N NbSamples], 'ValNaN', NaN, 'ErMapper', 1);
DataType = cl_image.indDataType('TxAngle');
b(3) = cl_image('Image', TxAngle, 'Name', [nomFic ' - SidescanIFR'], 'Unit', 'Deg', ...
    'DataType', DataType, 'ColormapIndex', 3, ...
    'TagSynchroX', [nomFic ' - PingSamples'], 'TagSynchroY', nomFic,...
    'x', x, 'XUnit', 'm', 'y', y, 'YUnit', 'Pings', ...
    'InitialFileName',   this.nomFic, ...
    'InitialFileFormat', InitialFileFormat, ...
    'GeometryType',   GeometryType,...
    'SonarDescription',   SonarDescription);
b(3).Writable = false;



Nbp = cl_memmapfile('FileName', nomFicSidescan.Nbp, 'FileCreation', 0, ...
    'Format', 'single', 'Size', [N NbSamples], 'ValNaN', NaN, 'ErMapper', 1);
DataType = cl_image.indDataType('AveragedPtsNb');
b(4) = cl_image('Image', Nbp, 'Name', [nomFic ' - SidescanIFR'], 'Unit', 'num', ...
    'DataType', DataType, 'ColormapIndex', 3, ...
    'TagSynchroX', [nomFic ' - PingSamples'], 'TagSynchroY', nomFic,...
    'x', x, 'XUnit', 'm', 'y', y, 'YUnit', 'Pings', ...
    'InitialFileName',   this.nomFic, ...
    'InitialFileFormat', InitialFileFormat, ...
    'GeometryType',   GeometryType,...
    'SonarDescription',   SonarDescription);
b(4).Writable = false;

% NbBeamsBathy = DataDepth.NbBeams(1);
if mod(NbBeamsBathy,2)
    ifaisMilieu = ceil(NbBeamsBathy / 2);
    Height = DataDepth.Depth(subl_Depth,ifaisMilieu);
else
    ifaisMilieu = [floor(NbBeamsBathy / 2) ceil(NbBeamsBathy / 2)];
    Height = mean(DataDepth.Depth(subl_Depth,ifaisMilieu),2);
end

DeltaD = 1500 ./ (2 * fe(subl_Depth));
Height = round(Height(:) ./ DeltaD(:));

DeltaD = 1500 ./ (2 * fe);
resol = median(DeltaD);
ResolPing = DeltaD(subl_Depth);


% ---------------------
% Transfert des signaux
%     b(k) = set(b(k), 'SonarHeight', (DataDepth.VehicleDepth(subl_Depth) * resol));  % Calimero


% subl_Depth = 1:length(y);
Mode = ones(1,length(y));
% resol = get(SonarDescription, 'Proc.RangeResol');
for k=1:length(b)
    b(k) = set(b(k), 'SonarRawDataResol', resol, 'SonarResolutionD', resol, 'SonarResolutionX', resol);
    
    % ---------------------
    % Transfert des signaux
    %     b(k) = set(b(k), 'SonarHeight',             (DataDepth.VehicleDepth(subl_Depth) * resol));  % Calimero
    
    %     X = Height;
    %     b(k) = set(b(k), 'SonarHeight',             flipud(-abs(X(:))));
    if (identSondeur == 7125) && isfield(DataDepth, 'VehicleDepth') % ROV
        RangeMinMetres = min(DataDepth.Range, [], 2);
        VD = DataDepth.VehicleDepth(subl_Depth);
        X = (VD(:)-RangeMinMetres(subl_Depth));
        b(k) = set(b(k), 'SonarImmersion', flipud(X(:)));
        X = -RangeMinMetres(subl_Depth) ./ ResolPing(:);
        b(k) = set(b(k), 'SonarHeight', flipud(X(:)));
    else
        %       X = DataDepth.TransducerDepth(subl_Depth);
        %       b(k) = set(b(k), 'SonarImmersion', flipud(X(:)));
        b(k) = set(b(k), 'SonarImmersion', (DataDepth.TransducerDepth(subl_Depth)));
        %         b(k) = set(b(k), 'SonarHeight', -abs(Height(:) ./ X(:)));
        X = -abs(Height(:));
        b(k) = set(b(k), 'SonarHeight', flipud(X(:)));
    end
    
    
    b(k) = set(b(k), 'SonarTime', timeMat2cl_time(fliplr(Data.Time.timeMat)));
    
    X = DataDepth.Heading(subl_Depth);
    b(k) = set(b(k), 'SonarHeading', flipud(X(:)));
    
    
    % X = SurfaceSoundSpeed;
    %         b(k) = set(b(k), 'SonarSurfaceSoundSpeed',  flipud(X(:)));
    
    X = Mode;
    b(k) = set(b(k), 'SonarPortMode_1', flipud(X(:)));
    
    X = fe(subl_Depth);
    b(k) = set(b(k), 'SampleFrequency', flipud(X(:)));
    b(k) = set(b(k), 'SonarFrequency', DataDepth.Frequency(subl_Depth));
    
    b(k) = set(b(k), 'SonarRawDataResol', resol, 'SonarResolutionD', resol);
    
    X =  DataDepth.Roll(subl_Depth);
    b(k) = set(b(k), 'SonarRoll', flipud(X(:)));
    
    X = DataDepth.Pitch(subl_Depth);
    b(k) = set(b(k), 'SonarPitch', flipud(X(:)));
    
    % X = Yaw;
    %         %     b(k) = set(b(k), 'SonarYaw',   flipud(X(:));
    
    X = DataDepth.Heave(subl_Depth);
    b(k) = set(b(k), 'SonarHeave',  flipud(X(:)));
    
    % X  = zeros(1,length(subl_Image));
    %         b(k) = set(b(k), 'SonarTide',  flipud(X(:)));
    
    X = Data.PingCounter;
    b(k) = set(b(k), 'SonarPingCounter',     flipud(X(:)));
    
    X = DataDepth.Longitude(subl_Depth);
    b(k) = set(b(k), 'SonarFishLongitude', flipud(X(:)));
    
    X = DataDepth.Latitude(subl_Depth);
    b(k) = set(b(k), 'SonarFishLatitude',  flipud(X(:)));
    
    % --------------------------------------------
    % Transfert des parametres d'etat de la donnee
    
    %         EtatSonar.SonarTVG_ConstructAlpha = mean(SonarTVG_ConstructAlpha);
    %         EtatSonar.SonarTVG_IfremerAlpha   = EtatSonar.SonarTVG_ConstructAlpha;
    %         b(k) = set(b(k),  'Sonar_DefinitionENCours', ...
    %             'SonarTVG_ConstructTypeCompens',        EtatSonar.SonarTVG_ConstructTypeCompens, ...
    %             'SonarTVG_ConstructTable',              EtatSonar.SonarTVG_ConstructTable, ...
    %             'SonarTVG_ConstructAlpha',              EtatSonar.SonarTVG_ConstructAlpha, ...
    %             'SonarTVG_ConstructConstante',          EtatSonar.SonarTVG_ConstructConstante, ...
    %             'SonarTVG_ConstructCoefDiverg',         EtatSonar.SonarTVG_ConstructCoefDiverg, ...
    %             'SonarTVG_IfremerAlpha',                EtatSonar.SonarTVG_IfremerAlpha, ...
    %             'SonarAireInso_etat',                   EtatSonar.SonarAireInso_etat, ...
    %             'SonarDiagEmi_etat',                    EtatSonar.SonarDiagEmi_etat, ...
    %             'SonarDiagEmi_origine',                 EtatSonar.SonarDiagEmi_origine, ...
    %             'SonarDiagEmi_ConstructTypeCompens',    EtatSonar.SonarDiagEmi_ConstructTypeCompens, ...
    %             'SonarDiagRec_etat',                    EtatSonar.SonarDiagRec_etat, ...
    %             'SonarDiagRec_origine',                 EtatSonar.SonarDiagRec_origine, ...
    %             'SonarDiagRec_ConstructTypeCompens',    EtatSonar.SonarDiagRec_ConstructTypeCompens, ...
    %             'Sonar_NE_etat',                        EtatSonar.SonarNE_etat, ...
    %             'Sonar_SH_etat',                        EtatSonar.SonarSH_etat, ...
    %             'Sonar_GT_etat',                        EtatSonar.SonarGT_etat, ...
    %             'SonarBS_etat',                         EtatSonar.SonarBS_etat, ...
    %             'SonarBS_origine',                      EtatSonar.SonarBS_origine, ...
    %             'SonarBS_origineBelleImage',            EtatSonar.SonarBS_origineBelleImage, ...
    %             'SonarBS_origineFullCompens',           EtatSonar.SonarBS_origineFullCompens, ...
    %             'SonarBS_IfremerCompensTable',          EtatSonar.SonarBS_IfremerCompensTable);
    
    % ----------------------
    % Definition d'une carto
    
    if isempty(Carto)
        %         Carto = getDefinitionCarto(b(k), 'nomDirSave', nomDirMat, 'NoGeodetic', 1);
        Carto = getDefinitionCarto(b(k), 'nomDirSave', nomDirMat, 'NoGeodetic', 0);
    end
    b(k) = set(b(k), 'Carto', Carto);
    b(k) = compute_stats( b(k));
    
    % ----------------------------
    % Definition du nom de l'image
    
    %     b(k) = set(b(k), 'Name', [nomFic ' - ' ImageName{k}]);   % On reimpose le Titre car il a ete modifie par b(k) = set(b(k),  'S...
    b(k) = update_Name(b(k));
    
    % ----------------------------------------------------------
    % On impose un rehaussement de contraste � 0.5% pour pouvoir
    % cont�ler l'image avec ErViewer
    
    StatValues = b(k).StatValues;
    if ~isempty(StatValues)
        b(k).CLim = StatValues.Quant_25_75;
    end
    
    switch k
        case 1
            status = export_ermapper_ers(b(k), nomFicSidescan.ReflectivityErs, 'IEEE4ByteReal', NaN);
            if ~status
                return
            end
            status = export_ermapper_sigV(b(k), nomFicSidescan.ReflectivityErs);
            if ~status
                return
            end
            
        case 2
            status = export_ermapper_ers(b(k), nomFicSidescan.RxBeamIndexErs, 'IEEE4ByteReal', NaN);
            if ~status
                return
            end
            status = export_ermapper_sigV(b(k), nomFicSidescan.RxBeamIndexErs);
            if ~status
                return
            end
            
        case 3
            status = export_ermapper_ers(b(k), nomFicSidescan.TxAngleErs, 'IEEE4ByteReal', NaN);
            if ~status
                return
            end
            status = export_ermapper_sigV(b(k), nomFicSidescan.TxAngleErs);
            if ~status
                return
            end
            
        case 4
            status = export_ermapper_ers(b(k), nomFicSidescan.NbpErs, 'IEEE4ByteReal', NaN);
            if ~status
                return
            end
            status = export_ermapper_sigV(b(k), nomFicSidescan.NbpErs);
            if ~status
                return
            end
    end
end

%% Sauvegarde de la donnee dans un fichier .mat

if ~isempty(nomFicSidescan.Mat)
    try
        if strcmp(Ext, '.mat')
            save(nomFicSidescan.Mat, 'Data')
        else
            [Data.Dimensions.nbPings, Data.Dimensions.nbSamples] = size(Data.CenterSampleDistance);
            NetcdfUtils.Struct2Netcdf(Data, nomFicSidescan.Mat, 'SidescanIFRData');
        end
    catch %#ok<CTCH>
        str = sprintf('Erreur ecriture fichier %s', nomFicMat);
        my_warndlg(str, 1);
    end
end
