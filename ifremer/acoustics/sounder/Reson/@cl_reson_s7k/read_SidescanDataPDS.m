function [Data, b, Carto] = read_SidescanDataPDS(this, identSondeur, varargin)

[varargin, Carto] = getPropertyValue(varargin, 'Carto', []); %#ok<ASGLU>

b = [];

I0 = cl_image_I0;

%% Check the number of elements

flag = checkIfOneElement(length(this));
if ~flag
    Data = [];
    return
end

%% R�cuperation de la frequence d'�chantillonnage

Data = read_sonarSettings(this, identSondeur);
% fe = Data.SampleRate; % Hz
% VolatileSonarSettings_PingCounter = Data.PingCounter;

[flag, DataDepth, identSondeur] = read_depth_s7k(this);
if ~flag
    return
end

%% On regarde si le fichier .mat existe

[nomDir, nomFic] = fileparts(this.nomFic);
[nomFicSidescan, flagFilesExist] = ResonSidescanPDSNames(this.nomFic, identSondeur);
[~, ~, Ext] = fileparts(nomFicSidescan.Mat);

UN = 1;
if UN && flagFilesExist
    if strcmp(Ext, '.mat')
        Data = loadmat(nomFicSidescan.Mat, 'nomVar', 'Data');
    elseif strcmp(Ext, '.nc') && NetcdfUtils.existGrp(ncFileName, 'SidescanPDSData', 'ncID', this.ncID)
        Data = NetcdfUtils.Netcdf2Struct(ncFileName, 'SidescanPDSData', 'ncID', this.ncID);
    end

    clear b
    [flag, b(1)] = cl_image.import_ermapper(nomFicSidescan.ReflectivityErs);
    if ~flag
        return
    end
    
    [flag, X] = cl_image.import_ermapper(nomFicSidescan.RxBeamIndexErs);
    if flag
        b(2) = X;
    else
        % Cas d'un datagramme qui n'est pas 7057
    end
    
    if isempty(Carto)
        Carto  = get(b(1), 'Carto');
    else
        for k=1:length(b)
            b(k) = set(b(k), 'Carto', Carto); %#ok<AGROW>
        end
    end
    
    for k=1:length(b)
        InitialFileName = this.nomFic;
        InitialFileFormat = 'ResonS7k';
        b(k) = set(b(k), 'TagSynchroY', [num2str(identSondeur) '_' nomFic], ...
            'TagSynchroX', [num2str(identSondeur) '_' nomFic ' - PingSamples'], ...
            'InitialFileName',   InitialFileName, ...
            'InitialFileFormat', InitialFileFormat); %#ok<AGROW>
    end
    return
end

%% On cr�e au besoin le r�pertoire SonarScope

nomDirMat = fullfile(nomDir, 'SonarScope');
if ~exist(nomDirMat, 'dir')
    status = mkdir(nomDirMat);
    if status
        %         disp(sprintf('Le repertoire %s a ete cree avec succes', nomDirMat))
    else
        fprintf('Impossible de ceer le repertoire %s.\nJe vous conseille de rendre cela possible, ca permet de formater la donnee en fichiers matlab,\nce qui permettra d''acc�der beaucoup plus vite � la donn�e.\n', nomDirMat)
        nomFicSidescan.Mat = [];
    end
end

[nomDirFic, nomFicSauve] = fileparts(nomFicSidescan.ReflectivityErs);
nomDirImage = fullfile(nomDirFic, nomFicSauve);
if ~exist(nomDirImage, 'dir')
    if exist(nomDirImage, 'file')
        delete(nomDirImage)
    end
    flag = mkdir(nomDirFic, nomFicSauve); %#ok
end

%% Lecture du fichier d'index

[flag, DataIndex] = readFicIndex(this, identSondeur);
if ~flag
    Data = [];
    return
end

Position         = DataIndex(3,:);
TypeDatagram     = DataIndex(5,:);
PingCounter      = DataIndex(6,:);
PingSequence     = DataIndex(7,:);
DeviceIdentifier = DataIndex(8,:);

isEmpty7007 = isempty(find(TypeDatagram == 7007, 1));
isEmpty7057 = isempty(find(TypeDatagram == 7057, 1));
if ~isEmpty7007
    [flag, subData] = get_subDataCommon(7007, TypeDatagram, PingCounter, PingSequence, DeviceIdentifier, identSondeur);
    if ~flag
        return
    end
    flag7057 = false;
elseif ~isEmpty7057
    [flag, subData] = get_subDataCommon(7057, TypeDatagram, PingCounter, PingSequence, DeviceIdentifier, identSondeur);
    if ~flag
        return
    end
    flag7057 = true;
else
    my_warndlg('No 7007 nor 7057 datagrams in this file', 0, 'Tag', 'No7007Nor7057Datagrams');
    return
end
clear DeviceIdentifier

PS  = PingSequence(subData);
subNon0 = (PS ~= 0);
PS(subNon0) = PS(subNon0) - 1;
PSnippets = PingCounter(subData) * 4 + PS;

PS  = DataDepth.MultiPingSequence;
subNon0 = (PS ~= 0);
PS(subNon0) = PS(subNon0) - 1;
PDepth = DataDepth.DepthPingCounterBeforeUnwrap * 4 + PS;

% [subl_Sidescan, subl_Depth] = intersect3(PingCounter(subDataSnippet), DataDepth.PingCounter, DataDepth.PingCounter);
[subl_Sidescan, subl_Depth] = intersect3(PSnippets, PDepth, PDepth);
subData = subData(subl_Sidescan);
if isempty(subl_Depth)
    my_warndlg('No snippets data in this file', 1);
    return
end

Position = Position(subData);

% Taille   = Taille(subData);
clear TypeDatagram

%% D�codage du fichier de donn�es

typeIndian = getIndianAll(this.nomFic);
% flagEndian = strcmp(typeIndian, 'b');
[fid, message] = fopen(this.nomFic, 'r', typeIndian);
if fid < 0
    my_warndlg(['cl_reson_s7k:read_SidescanDataPDS' message], 0);
    return
end

%% Lecture du nombre de faisceaux

fseek(fid, Position(1), 'bof');
flag = fread_DRF(fid);
if ~flag
    return
end

N = length(Position);

DateDatagram  = NaN(N, 1);
HeureDatagram = NaN(N, 1);

MatInit = NaN(N, 1, 'single');
Data.PingCounter       = MatInit;
Data.MultiPingSequence = MatInit;

if isempty(nomFicSidescan.Mat)
    [nomDir, nomFic, ext] = fileparts(this.nomFic); %#ok
    str = sprintf('Loading file %s', nomFic);
else
    [nomDir, nomFic, ext] = fileparts(this.nomFic); %#ok
    [nomDir, nomFic2, ext] = fileparts(nomFicSidescan.Mat); %#ok
    str = sprintf('Loading file %s\nCreation file %s', nomFic, nomFic2);
end

hw = create_waitbar(str, 'N', N);
% hTitre = get(get(hw, 'Children'), 'Title');
% set(hTitre, 'Interpreter', 'none', 'String', str);
for k=1:N
    my_waitbar(k, N, hw);
    
    fseek(fid, Position(k), 'bof');
    
    [flag, DRF] = fread_DRF(fid);
    if ~flag
        break
    end
    
    if flag7057
        [flag, PingNumber, Sequence, BeamData] = fread_RecordType_7057(fid); %#ok<ASGLU>
    else
        [flag, PingNumber, Sequence, BeamData] = fread_RecordType_7007(fid); %#ok<ASGLU>
    end
    if ~flag
        break
    end
    
    DateDatagram(k)  = DRF.Date;
    HeureDatagram(k) = DRF.Time;
    
    %     BeamData.RTH
    %     BeamData.RD
    
    Data.SystemSerialNumber = DRF.DeviceIdentifier;
    Data.EmModel            = BeamData.RTH.SonarId;
    
    Data.PingCounter(k)     = BeamData.RTH.PingCounter;
    Data.MultiPingSequence(k) = BeamData.RTH.MultiPingSequence;
    Data.NbSamples(k)       = BeamData.RTH.S;
end
my_close(hw)

NbSamples = max(Data.NbSamples);
Data.Amplitude   = NaN(N, 2*NbSamples, 'single');
if flag7057
    Data.RxBeamIndex = NaN(N, 2*NbSamples, 'single');
end

% hTitre = get(get(hw, 'Children'), 'Title');
% set(hTitre, 'Interpreter', 'none', 'String', str);
hw = create_waitbar(str, 'N', N);
for k=1:N
    my_waitbar(k, N, hw);
    
    fseek(fid, Position(k), 'bof');
    [flag, DRF] = fread_DRF(fid); %#ok<ASGLU>
    
    if flag7057
        [flag, PingNumber, Sequence, BeamData] = fread_RecordType_7057(fid); %#ok<ASGLU>
    else
        [flag, PingNumber, Sequence, BeamData] = fread_RecordType_7007(fid); %#ok<ASGLU>
    end
    if ~flag
        break
    end
    
    subc = (1:2*Data.NbSamples(k)) + NbSamples - Data.NbSamples(k);
    Amp = [fliplr(BeamData.RD.PortBeamAmplitude') BeamData.RD.StartboardBeamAmplitude'];
    
    if ~isempty(Amp)
        if flag7057
            Data.Amplitude(k,subc)   = Amp;
            RxBeamIndex = [fliplr(BeamData.RD.PortRxBeamIndex')   BeamData.RD.StartboardRxBeamIndex'];
            Data.RxBeamIndex(k,subc) = RxBeamIndex;
        else
            Data.Amplitude(k,subc) = reflec_Amp2dB(Amp);
        end
    end
end
my_close(hw, 'MsgEnd')
fclose(fid);

Data.PingCounter = unwrapPingCounter(Data.PingCounter);

Data.Time = cl_time('timeIfr', DateDatagram, HeureDatagram);

%% Sauvegarde de la donnee dans un fichier .mat

if ~isempty(nomFicSidescan.Mat)
    try
        if strcmp(Ext, '.mat')
            save(nomFicSidescan.Mat, 'Data')
        else
%             [Data.Dimensions.nbPings, Data.Dimensions.nbSamples] = size(Data.Amplitude);
            NetcdfUtils.Struct2Netcdf(Data, nomFicSidescan.Mat, 'Snippets');
        end
    catch ME %#ok<NASGU>
        str = sprintf('Erreur ecriture fichier %s', nomFicSidescan.Mat);
        my_warndlg(str, 1);
    end
end

b = SidescanDataPDS2b(Data, DataDepth, identSondeur, this.nomFic, Carto);
