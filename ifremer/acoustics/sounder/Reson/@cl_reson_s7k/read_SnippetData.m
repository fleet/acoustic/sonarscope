% Lecture des datagrams "depth" dans un fichier .s7k
%
% Syntax
%   Data = read_SnippetData(a, ...)
%
% Input Arguments
%   a : Instance de cl_reson_s7k
%
% Name-only Arguments
%   MasqueDetection : Suppression de valeurs masqu�es par MasqueDetection
%   MasqueDepth     : Suppression de valeurs masqu�es par Masque sur
%                     l'histogramme des Depth
%
% Output Arguments
%   Data : Structure contenant Depth, AcrossDist, AlongDist, AlongDist,
%          BeamAzimuthAngle, Range, QualityFactor, LengthDetection et Reflectivity
%
% Examples
%   nomFic = getNomFicDatabase('RESON7125_20051116_221245.s7k')
%   a = cl_reson_s7k('nomFic', nomFic);
%   Data = read_SnippetData(a)
%   figure; imagesc(Data.Depth)
%   figure; imagesc(Data.AcrossDist)
%   figure; imagesc(Data.AlongDist)
%   figure; imagesc(Data.BeamDepressionAngle)
%   figure; imagesc(Data.BeamAzimuthAngle)
%   figure; imagesc(Data.Range)
%   figure; imagesc(Data.QualityFactor)
%   figure; imagesc(Data.LengthDetection)
%   figure; imagesc(Data.Reflectivity)
%
%   b = view_depth(a);
%   b = editobj(b);
%
% See also cl_reson_s7k cl_reson_s7k/plot_position cl_reson_s7k/plot_nav Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [Data, b, Carto] = read_SnippetData(this, identSondeur, varargin)

[varargin, Carto] = getPropertyValue(varargin, 'Carto', []); %#ok<ASGLU>

b = [];

%% Check the number of elements

flag = checkIfOneElement(length(this));
if ~flag
    Data = [];
    return
end

%% R�cuperation de la fr�quence d'�chantillonnage

Data = read_sonarSettings(this, identSondeur);
fe = Data.SampleRate; % Hz
VolatileSonarSettings_PingCounter = Data.PingCounter;

[flag, DataDepth, identSondeur] = read_depth_s7k(this);
if ~flag
    return
end

%% On regarde si le fichier .mat existe

[nomDir, nomFic] = fileparts(this.nomFic);
[nomFicSnippet, flagFilesExist] = ResonSnippetNames(this.nomFic, identSondeur);
[~, ~, Ext] = fileparts(nomFicSnippet.Mat);

UN = 1;
if UN && flagFilesExist
    if strcmp(Ext, '.mat')
        Data = loadmat(nomFicSnippet.Mat, 'nomVar', 'Data');
    elseif strcmp(Ext, '.nc') && NetcdfUtils.existGrp(nomFicSnippet.Mat, 'Snippets', 'ncID', this.ncID)
        Data = NetcdfUtils.Netcdf2Struct(nomFicSnippet.Mat, 'Snippets', 'ncID', this.ncID);
    end
end

if UN && flagFilesExist
    clear b
    [flag, b(1)] = cl_image.import_ermapper(nomFicSnippet.ReflectivityErs);
    if ~flag
        return
    end
    [flag, b(2)] = cl_image.import_ermapper(nomFicSnippet.RxBeamIndexErs);
    if ~flag
        return
    end
    [flag, b(3)] = cl_image.import_ermapper(nomFicSnippet.TxAngleErs);
    if ~flag
        return
    end
    
    if isempty(Carto)
        Carto  = get(b(1), 'Carto');
    else
        for k=1:length(b)
            b(k) = set(b(k), 'Carto', Carto);
        end
    end
    
    for k=1:length(b)
        InitialFileName = this.nomFic;
        InitialFileFormat = 'ResonS7k';
        b(k) = set(b(k), 'TagSynchroY', [num2str(identSondeur) '_' nomFic], ...
            'InitialFileName',   InitialFileName, ...
            'InitialFileFormat', InitialFileFormat);
    end
    
    b(1) = set(b(1), 'TagSynchroX', [num2str(identSondeur) '_' nomFic ' - Snippet']);
    b(2) = set(b(2), 'TagSynchroX', [num2str(identSondeur) '_' nomFic ' - Snippet']);
    b(3) = set(b(3), 'TagSynchroX', [num2str(identSondeur) '_' nomFic ' - Snippet']);
    return
end

%% On cr�e au besoin le r�pertoire SonarScope

nomDirMat = fullfile(nomDir, 'SonarScope');
if ~exist(nomDirMat, 'dir')
    status = mkdir(nomDirMat);
    if status
        %         disp(sprintf('Le repertoire %s a ete cree avec succes', nomDirMat))
    else
        fprintf('Impossible de c�er le r�pertoire %s.\nJe vous conseille de rendre cela possible, ca permet de formater la donnee en fichiers matlab,\nce qui permettra d''acceder beaucoup plus vite � la donn�e.\n', nomDirMat)
        nomFicMat = [];
    end
end

%% Lecture du fichier d'index

[flag, DataIndex] = readFicIndex(this, identSondeur);
if ~flag
    Data = [];
    return
end

Position         = DataIndex(3,:);
TypeDatagram     = DataIndex(5,:);
PingCounter      = DataIndex(6,:);
PingSequence     = DataIndex(7,:);
DeviceIdentifier = DataIndex(8,:);

isEmpty7008 = isempty(find(TypeDatagram == 7008, 1));
isEmpty7058 = isempty(find(TypeDatagram == 7058, 1));
isEmpty7028 = isempty(find(TypeDatagram == 7028, 1));
if ~isEmpty7008
    [flag, subDataSnippet] = get_subDataCommon(7008, TypeDatagram, PingCounter, PingSequence, DeviceIdentifier, identSondeur);
    if ~flag
        return
    end
elseif ~isEmpty7058
    [flag, subDataSnippet] = get_subDataCommon(7058, TypeDatagram, PingCounter, PingSequence, DeviceIdentifier, identSondeur);
    if ~flag
        return
    end
elseif ~isEmpty7028
    [flag, subDataSnippet] = get_subDataCommon(7028, TypeDatagram, PingCounter, PingSequence, DeviceIdentifier, identSondeur);
    if ~flag
        return
    end
else
    my_warndlg('No 7008 nor 7058 datagrams in this file', 1);
    return
end
clear DeviceIdentifier

if isempty(subDataSnippet)
    my_warndlg('No Snippet data in this file', 1);
    return
end

%% Recherche automatique des num�ros de faisceau limites

NbSoundes = sum(~isnan(DataDepth.Range));
NbSoundes = NbSoundes / sum(NbSoundes);
NbSoundes = cumsum(NbSoundes);
idebBeamBathy = find(NbSoundes > 0.02, 1, 'first');
ifinBeamBathy = find(NbSoundes < 0.98, 1, 'last');

% PingCounter = PingCounter(subDataSnippet); % CHERCHER LA CORRESPONDANCE DES PINGS COUNTER PLUTOT QUE 1:min()
subDataSnippet = subDataSnippet(1:min(length(subDataSnippet), length(VolatileSonarSettings_PingCounter)));

Position = Position(subDataSnippet);
% Taille   = Taille(subDataSnippet);
clear TypeDatagram

%% D�codage du fichier de donn�es

typeIndian = getIndianAll(this.nomFic);
% flagEndian = strcmp(typeIndian, 'b');
[fid, message] = fopen(this.nomFic, 'r', typeIndian);
if fid < 0
    my_warndlg(['cl_reson_s7k:read_SnippetData' message], 0);
    return
end

%% Lecture du nombre de faisceaux

fseek(fid, Position(1), 'bof');
flag = fread_DRF(fid);
if ~flag
    return
end

N = length(Position);

DateDatagram  = NaN(1, N);
HeureDatagram = NaN(1, N);

Data.PingCounter             = NaN(1, N, 'single');
Data.MultiPingSequence       = NaN(1, N, 'single');
Data.NbBeams                 = NaN(1, N, 'single');

Data.BeamDescriptor          = NaN(N, 1, 'single');
Data.BeginSampleDescriptor   = NaN(N, 1, 'single');
Data.Reflectivity            = NaN(N, 1, 'single');
Data.CenterSampleDistance    = NaN(N, 1, 'single');
Data.BeamAcrossTrackDistance = NaN(N, 1, 'single');

if isempty(nomFicSnippet.Mat)
    [nomDir, nomFic, ext] = fileparts(this.nomFic); %#ok
    str = sprintf('Loading file %s', nomFic);
else
    [~, nomFic]  = fileparts(this.nomFic);
    [~, nomFic2] = fileparts(nomFicSnippet.Mat);
    str = sprintf('Loading file %s\nCreation file %s', nomFic, nomFic2);
end


[nomDirFic, nomFicSauve] = fileparts(nomFicSnippet.ReflectivityErs);
nomDirImage = fullfile(nomDirFic, nomFicSauve);
if ~exist(nomDirImage, 'dir')
    if exist(nomDirImage, 'file')
        delete(nomDirImage)
    end
    flag = mkdir(nomDirFic, nomFicSauve); %#ok
end

nomFicSauve = fullfile(nomDirImage, nomFicSauve);
fidReflectivity = fopen(nomFicSauve, 'w+');
if fidReflectivity == -1
    str = ['Write failure file ' nomFicSauve];
    my_warndlg(str, 1);
    return
end

[nomDirFic, nomFicSauve] = fileparts(nomFicSnippet.RxBeamIndexErs);
nomDirImage = fullfile(nomDirFic, nomFicSauve);
if ~exist(nomDirImage, 'dir')
    if exist(nomDirImage, 'file')
        delete(nomDirImage)
    end
    flag = mkdir(nomDirFic, nomFicSauve); %#ok
end

nomFicSauve = fullfile(nomDirImage, nomFicSauve);
fidRxBeamIndex = fopen(nomFicSauve, 'w+');
if fidRxBeamIndex == -1
    str = ['Write failure file ' nomFicSauve];
    my_warndlg(str, 1);
    return
end

[nomDirFic, nomFicSauve] = fileparts(nomFicSnippet.TxAngleErs);
nomDirImage = fullfile(nomDirFic, nomFicSauve);
if ~exist(nomDirImage, 'dir')
    if exist(nomDirImage, 'file')
        delete(nomDirImage)
    end
    flag = mkdir(nomDirFic, nomFicSauve); %#ok
end

nomFicSauve = fullfile(nomDirImage, nomFicSauve);
fidTxAngle = fopen(nomFicSauve, 'w+');
if fidTxAngle == -1
    str = ['Write failure file ' nomFicSauve];
    my_warndlg(str, 1);
    return
end

subBeamsReson = idebBeamBathy:ifinBeamBathy;
subBeamsSsc   = 1:length(subBeamsReson);

NbBeamsBathy = DataDepth.NbBeams(1);

Beams = repmat(single(subBeamsReson), 100, 1);
Beams = Beams(:);
% TxAngle = NaN(1, NbBeamsBathy*100, 'single');
% TxAngleOLD = NaN(1, NbBeamsBathy*100, 'single');
% Xni= NaN(1, NbBeamsBathy*100, 'single');

flagEmpty = 1;
hw = create_waitbar(str, 'N', N);
% hTitre = get(get(hw, 'Children'), 'Title');
% set(hTitre, 'Interpreter', 'none', 'String', str);
for k=N:-1:1
    iDepth = k; %N-k+1;
    my_waitbar((N-k+1), N, hw);
    
    fseek(fid, Position(k), 'bof');
    
    [flag, DRF] = fread_DRF(fid);
    if ~flag
        break
    end
    
    if ~isEmpty7008
        [flag, PingNumber, Sequence, BeamData] = fread_RecordType_7008(fid, DRF);
    elseif ~isEmpty7058
        [flag, PingNumber, Sequence, BeamData] = fread_RecordType_7058(fid, DRF, NbBeamsBathy);%, DataDepth.Range(iDepth,:));
    elseif ~isEmpty7028
        [flag, PingNumber, Sequence, BeamData] = fread_RecordType_7028(fid, NbBeamsBathy);%, DataDepth.Range(iDepth,:));
    end
    if ~flag
        break
    end
    
    DateDatagram(k)  = DRF.Date;
    HeureDatagram(k) = DRF.Time;
    
    %     BeamData.RTH
    %     BeamData.RD
    
    Data.SystemSerialNumber = DRF.DeviceIdentifier;
    Data.EmModel            = BeamData.RTH.SonarId;
    %     if ~isfield(BeamData, 'OD')
    %         Data = [];
    %         return
    %     end
    
    Data.PingCounter(k)       = BeamData.RTH.PingCounter;
    Data.MultiPingSequence(k) = BeamData.RTH.MultiPingSequence;
    Data.NbBeams(k)           = BeamData.RTH.N;
    %     Data.DataSampleType(k)  = BeamData.RTH.DataSampleType;
    
    
    
    Data.BeamDescriptor(k,subBeamsSsc)          = BeamData.RD.BeamDescriptor(subBeamsReson);
    Data.BeginSampleDescriptor(k,subBeamsSsc)   = BeamData.RD.BeginSampleDescriptor(subBeamsReson);
    Data.EndSampleDescriptor(k,subBeamsSsc)     = BeamData.RD.EndSampleDescriptor(subBeamsReson);
    if isfield(BeamData, 'OD')
        Data.Frequency                              = BeamData.OD.Frequency / 1000; % En kHz
        Data.CenterSampleDistance(k,subBeamsSsc)    = BeamData.OD.CenterSampleDistance(subBeamsReson);
        Data.BeamAcrossTrackDistance(k,subBeamsSsc) = BeamData.OD.BeamAcrossTrackDistance(subBeamsReson);
    end
    
    [Samples, TxAngle] = process_Snippet(DataDepth, BeamData, iDepth, fe(k), Data.Frequency, idebBeamBathy, ifinBeamBathy);
    %{
figure; plot(BeamData.RD.BeamDescriptor, '+'); grid on; title('BeamDescriptor')
figure; plot(BeamData.RD.BeginSampleDescriptor, '+'); grid on; title('BeginSampleDescriptor')
figure; plot(BeamData.RD.EndSampleDescriptor, '+'); grid on; title('EndSampleDescriptor')
figure; plot(BeamData.OD.CenterSampleDistance, '+'); grid on; title('CenterSampleDistance')
% figure; plot(Samples); grid on;
% figure; plot(Samples); grid on;
    %}
    fwrite(fidTxAngle,      TxAngle, 'float32');
    fwrite(fidReflectivity, Samples, 'float32');
    fwrite(fidRxBeamIndex,  Beams,   'float32');
    
    % figure; plot(Samples, '+'); grid on; title('Samples')
    % figure; plot(TxAngle, '+'); grid on; title('TxAngle')
    flagEmpty = 0;
end
my_close(hw)
fclose(fid);
fclose(fidReflectivity);
fclose(fidRxBeamIndex);
fclose(fidTxAngle);

if flagEmpty
    fprintf('There is no seabed reflectivity datagrams (snippets) in the file %s\n', this.nomFic);
    return
end

NbSamples = length(Samples);
clear BeamData Beams DRF PingCounter PingSequence Position Samples TxAngle
clear VolatileSonarSettings_PingCounter fid fidIndex fidReflectivity fidRxBeamIndex
clear fidTxAngle flag hw hTitre i iDepth isEmpty7008 isEmpty7058 message
clear nomDir nomDirFic nomDirImage nomFic2 nomFicIndex nomFicSauve str subDataSnippet
clear subBeamsReson subBeamsSsc typeIndian varargin

Data.PingCounter = unwrapPingCounter(Data.PingCounter);
Data.Time = cl_time('timeIfr', DateDatagram, HeureDatagram);
clear DateDatagram HeureDatagram

GeometryType = cl_image.indGeometryType('PingSnippets');
InitialFileFormat = 'RESONs7k';
y = N:-1:1;

x = 1 + ((double(idebBeamBathy)-1)*100 : (double(ifinBeamBathy))*100 - 1);

[flag, IdentSonar] = DeviceIdentifier2IdentSonar(this, identSondeur);
iMode = 1;
SonarDescription = cl_sounder('Sonar.Ident', IdentSonar, 'Sonar.Mode_1', iMode);

Reflectivity = cl_memmapfile('FileName', nomFicSnippet.Reflectivity, 'FileCreation', 0, ...
    'Format', 'single', 'Size', [N NbSamples], ...
    'ValNaN', NaN, 'ErMapper', 1);

nomFic = [num2str(identSondeur) '_' nomFic];
DataType = cl_image.indDataType('Reflectivity');
b = cl_image('Image', Reflectivity, 'Name', nomFic, 'Unit', 'dB', ...
    'DataType', DataType, 'ColormapIndex', 2, ...
    'TagSynchroX', [nomFic ' - Snippet'], 'TagSynchroY', nomFic,...
    'x', x, 'XUnit', 'm', 'y', y, 'YUnit', 'Pings', ...
    'InitialFileName',   this.nomFic, ...
    'InitialFileFormat', InitialFileFormat, ...
    'GeometryType',   GeometryType,...
    'SonarDescription',   SonarDescription);
b.Writable = false;

% EN ATTENDANT DE POUVOIR CREER UN SIGNAL HORIZONTAL QUI SERAIT RxBeamIndex
RxBeamIndex = cl_memmapfile('FileName', nomFicSnippet.RxBeamIndex, 'FileCreation', 0, ...
    'Format', 'single', 'Size', [N NbSamples], ...
    'ValNaN', NaN, 'ErMapper', 1);

DataType = cl_image.indDataType('RxBeamIndex');
b(2) = cl_image('Image', RxBeamIndex, 'Name', nomFic, 'Unit', 'num', ...
    'DataType', DataType, 'ColormapIndex', 3, ...
    'TagSynchroX', [nomFic ' - Snippet'], 'TagSynchroY', nomFic,...
    'x', x, 'XUnit', 'm', 'y', y, 'YUnit', 'Pings', ...
    'InitialFileName',   this.nomFic, ...
    'InitialFileFormat', InitialFileFormat, ...
    'GeometryType',   GeometryType,...
    'SonarDescription',   SonarDescription);
b(2).Writable = false;

TxAngle = cl_memmapfile('FileName', nomFicSnippet.TxAngle, 'FileCreation', 0, ...
    'Format', 'single', 'Size', [N NbSamples], ...
    'ValNaN', NaN, 'ErMapper', 1);

DataType = cl_image.indDataType('TxAngle');
b(3) = cl_image('Image', TxAngle, 'Name', nomFic, 'Unit', 'num', ...
    'DataType', DataType, 'ColormapIndex', 3, ...
    'TagSynchroX', [nomFic ' - Snippet'], 'TagSynchroY', nomFic,...
    'x', x, 'XUnit', 'm', 'y', y, 'YUnit', 'Pings', ...
    'InitialFileName',   this.nomFic, ...
    'InitialFileFormat', InitialFileFormat, ...
    'GeometryType',   GeometryType,...
    'SonarDescription',   SonarDescription);
b(3).Writable = false;

% NbBeamsBathy = DataDepth.NbBeams(1);
if mod(NbBeamsBathy,2)
    ifaisMilieu = ceil(NbBeamsBathy / 2);
    Height = DataDepth.Depth(:,ifaisMilieu);
else
    ifaisMilieu = [floor(NbBeamsBathy / 2) ceil(NbBeamsBathy / 2)];
    Height = mean(DataDepth.Depth(:,ifaisMilieu),2);
end


subl_Depth = 1:length(y);
Mode = ones(1,length(y));
resol = get(SonarDescription, 'Proc.RangeResol');
for k=1:length(b)
    b(k) = set(b(k), 'SonarRawDataResol', resol, 'SonarResolutionD', resol, 'SonarResolutionX', resol);
    
    % ---------------------
    % Transfert des signaux
    %     b(k) = set(b(k), 'SonarHeight',             (DataDepth.VehicleDepth(subl_Depth) * resol));  % Calimero
    X = Height;
    b(k) = set(b(k), 'SonarHeight',             flipud(-abs(X(:))));
    
    b(k) = set(b(k), 'SonarTime',               timeMat2cl_time(fliplr(Data.Time.timeMat)));
    
    X = DataDepth.Heading(subl_Depth);
    b(k) = set(b(k), 'SonarHeading',            flipud(X(:)));
    
    X = DataDepth.TransducerDepth(subl_Depth);
    b(k) = set(b(k), 'SonarImmersion',          flipud(X(:)));
    
    % X = SurfaceSoundSpeed;
    %         b(k) = set(b(k), 'SonarSurfaceSoundSpeed',  flipud(X(:)));
    
    X = Mode;
    b(k) = set(b(k), 'SonarPortMode_1',         flipud(X(:)));
    
    X = fe(subl_Depth);
    b(k) = set(b(k), 'SampleFrequency', flipud(X(:)));
    b(k) = set(b(k), 'SonarFrequency', DataDepth.Frequency(subl_Depth));
    
    b(k) = set(b(k), 'SonarRawDataResol', resol, 'SonarResolutionD', resol);
    
    X =  DataDepth.Roll(subl_Depth);
    b(k) = set(b(k), 'SonarRoll', flipud(X(:)));
    
    X = DataDepth.Pitch(subl_Depth);
    b(k) = set(b(k), 'SonarPitch', flipud(X(:)));
    
    % X = Yaw;
    %         %     b(k) = set(b(k), 'SonarYaw',   flipud(X(:));
    
    X = DataDepth.Heave(subl_Depth);
    b(k) = set(b(k), 'SonarHeave',  flipud(X(:)));
    
    % X  = zeros(1,length(subl_Image));
    %         b(k) = set(b(k), 'SonarTide',  flipud(X(:)));
    
    X = Data.PingCounter;
    b(k) = set(b(k), 'SonarPingCounter',     flipud(X(:)));
    
    X = DataDepth.Longitude(subl_Depth);
    b(k) = set(b(k), 'SonarFishLongitude', flipud(X(:)));
    
    X = DataDepth.Latitude(subl_Depth);
    b(k) = set(b(k), 'SonarFishLatitude',  flipud(X(:)));
    
    % --------------------------------------------
    % Transfert des parametres d'etat de la donnee
    
    %         EtatSonar.SonarTVG_ConstructAlpha = mean(SonarTVG_ConstructAlpha);
    %         EtatSonar.SonarTVG_IfremerAlpha   = EtatSonar.SonarTVG_ConstructAlpha;
    %         b(k) = set(b(k),  'Sonar_DefinitionENCours', ...
    %             'SonarTVG_ConstructTypeCompens',        EtatSonar.SonarTVG_ConstructTypeCompens, ...
    %             'SonarTVG_ConstructTable',              EtatSonar.SonarTVG_ConstructTable, ...
    %             'SonarTVG_ConstructAlpha',              EtatSonar.SonarTVG_ConstructAlpha, ...
    %             'SonarTVG_ConstructConstante',          EtatSonar.SonarTVG_ConstructConstante, ...
    %             'SonarTVG_ConstructCoefDiverg',         EtatSonar.SonarTVG_ConstructCoefDiverg, ...
    %             'SonarTVG_IfremerAlpha',                EtatSonar.SonarTVG_IfremerAlpha, ...
    %             'SonarAireInso_etat',                   EtatSonar.SonarAireInso_etat, ...
    %             'SonarDiagEmi_etat',                    EtatSonar.SonarDiagEmi_etat, ...
    %             'SonarDiagEmi_origine',                 EtatSonar.SonarDiagEmi_origine, ...
    %             'SonarDiagEmi_ConstructTypeCompens',    EtatSonar.SonarDiagEmi_ConstructTypeCompens, ...
    %             'SonarDiagRec_etat',                    EtatSonar.SonarDiagRec_etat, ...
    %             'SonarDiagRec_origine',                 EtatSonar.SonarDiagRec_origine, ...
    %             'SonarDiagRec_ConstructTypeCompens',    EtatSonar.SonarDiagRec_ConstructTypeCompens, ...
    %             'Sonar_NE_etat',                        EtatSonar.SonarNE_etat, ...
    %             'Sonar_SH_etat',                        EtatSonar.SonarSH_etat, ...
    %             'Sonar_GT_etat',                        EtatSonar.SonarGT_etat, ...
    %             'SonarBS_etat',                         EtatSonar.SonarBS_etat, ...
    %             'SonarBS_origine',                      EtatSonar.SonarBS_origine, ...
    %             'SonarBS_origineBelleImage',            EtatSonar.SonarBS_origineBelleImage, ...
    %             'SonarBS_origineFullCompens',           EtatSonar.SonarBS_origineFullCompens, ...
    %             'SonarBS_IfremerCompensTable',          EtatSonar.SonarBS_IfremerCompensTable);
    
    % ----------------------
    % Definition d'une carto
    
    if isempty(Carto)
        %         Carto = getDefinitionCarto(b(k), 'nomDirSave', nomDirMat, 'NoGeodetic', 1);
        Carto = getDefinitionCarto(b(k), 'nomDirSave', nomDirMat, 'NoGeodetic', 0);
    end
    b(k) = set(b(k), 'Carto', Carto);
    b(k) = compute_stats( b(k));
    
    % ----------------------------
    % Definition du nom de l'image
    
    %     b(k) = set(b(k), 'Name', [nomFic ' - ' ImageName{k}]);   % On reimpose le Titre car il a ete modifie par b(k) = set(b(k),  'S...
    b(k) = update_Name(b(k));
    
    % ----------------------------------------------------------
    % On impose un rehaussement de contraste � 0.5% pour pouvoir
    % cont�ler l'image avec ErViewer
    
    StatValues = b(k).StatValues;
    if ~isempty(StatValues)
        b(k).CLim = StatValues.Quant_25_75;
    end
    
    switch k
        case 1
            status = export_ermapper_ers(b(k), nomFicSnippet.ReflectivityErs, 'IEEE4ByteReal', NaN);
            if ~status
                return
            end
            status = export_ermapper_sigV(b(k), nomFicSnippet.ReflectivityErs);
            if ~status
                return
            end
            
        case 2
            status = export_ermapper_ers(b(k), nomFicSnippet.RxBeamIndexErs, 'IEEE4ByteReal', NaN);
            if ~status
                return
            end
            status = export_ermapper_sigV(b(k), nomFicSnippet.RxBeamIndexErs);
            if ~status
                return
            end
            
        case 3
            status = export_ermapper_ers(b(k), nomFicSnippet.TxAngleErs, 'IEEE4ByteReal', NaN);
            if ~status
                return
            end
            status = export_ermapper_sigV(b(k), nomFicSnippet.TxAngleErs);
            if ~status
                return
            end
    end
end

%% Sauvegarde de la donnee dans un fichier .mat

if ~isempty(nomFicSnippet.Mat)
    try
        if strcmp(Ext, '.mat')
            save(nomFicSnippet.Mat, 'Data')
        else
            [Data.Dimensions.nbPings, Data.Dimensions.nbSamples] = size(Data.CenterSampleDistance);
            NetcdfUtils.Struct2Netcdf(Data, nomFicSnippet.Mat, 'Snippets');
        end
    catch
        str = sprintf('Erreur ecriture fichier %s', nomFicMat);
        my_warndlg(str, 1);
    end
end
