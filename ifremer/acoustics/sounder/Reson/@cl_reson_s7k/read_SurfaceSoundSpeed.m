function Data = read_SurfaceSoundSpeed(this, identSondeur, varargin)

global useCacheNetcdf %#ok<GVMIS>

[varargin, DataIndex] = getPropertyValue(varargin, 'DataIndex', []); %#ok<ASGLU>

%% Check the number of elements

flag = checkIfOneElement(length(this));
if ~flag
    Data = [];
    return
end

%% On regarde si le fichier .mat existe

[nomDir, nomFic] = fileparts(this.nomFic);
nomFicMat  = fullfile(nomDir, 'SonarScope', [num2str(identSondeur) '_' nomFic '_surfaceSoundSpeed.mat']);
ncFileName = fullfile(nomDir, 'SonarScope', [nomFic '.nc']);

UN = 1;
if UN && exist(nomFicMat, 'file')
    Data = loadmat(nomFicMat, 'nomVar', 'Data');
    return
end
if UN && exist(ncFileName, 'file') && NetcdfUtils.existGrp(ncFileName, 'SurfaceSoundSpeed', 'ncID', this.ncID)
    Data = NetcdfUtils.Netcdf2Struct(ncFileName, 'SurfaceSoundSpeed', 'ncID', this.ncID);
    return
end

%% On cr�e au besoin le r�pertoire SonarScope

nomDirMat = fullfile(nomDir, 'SonarScope');
if ~exist(nomDirMat, 'dir')
    status = mkdir(nomDirMat);
    if status
%         disp(sprintf('Le repertoire %s a ete cree avec succes', nomDirMat))
    else
        fprintf('Impossible de ceer le repertoire %s.\nJe vous conseille de rendre cela possible, ca permet de formater la donn�e en fichiers matlab,\nce qui permettra d''acc�der beaucoup plus vite � la donn�e.\n', nomDirMat);
        nomFicMat = [];
    end
end

%% On recup�re le numero de serie car pas donn� dans le datagram contrairement � Simrad

% SystemSerialNumber = get_SonarSerialNumber(this);

%% Lecture du fichier d'index

if isempty(DataIndex)
    [flag, DataIndex] = readFicIndex(this, identSondeur);
    if ~flag
        Data = [];
        return
    end
end

Position     = DataIndex(3,:);
TypeDatagram = DataIndex(5,:);

sub = (TypeDatagram == 7610);
Position = Position(sub);
clear TypeDatagram

%% D�codage du fichier de donn�es

typeIndian = 'l';
[fid, message] = fopen(this.nomFic, 'r', typeIndian);
if fid < 0
    disp(message);
    Data = [];
    return
end

N = length(Position);
MatInit = zeros(1, N);
Data.Frequency           = MatInit;
Data.PingCounter         = MatInit;
Data.MultiPingSequence   = MatInit;
Data.Latitude            = MatInit;
Data.Longitude           = MatInit;
Data.Heading             = MatInit;
Data.AlongTrackDistance  = MatInit;
Data.AcrossTrackDistance = MatInit;
Data.VerticalDepth       = MatInit;
Data.SoundVelocity       = MatInit;
DateDatagram             = zeros(1, N);
HeureDatagram            = zeros(1, N);

[~, nomFicSeul] = fileparts(this.nomFic);
strLoop = infoLectureDatagrams(this, nomFicSeul, 7610);
for k=1:N
    infoLoop(strLoop, k, N, 'pings')
    
    fseek(fid, Position(k), 'bof');
    
    [flag, DRF] = fread_DRF(fid);
    if ~flag
        break
    end
    [flag, SP] = fread_RecordType_7610(fid);
    if ~flag
        break
    end

    DateDatagram(k)  = DRF.Date;
    HeureDatagram(k) = DRF.Time;
    
	Data.SoundVelocity(k) = SP.RTH.SoundVelocity;
end
fclose(fid);
my_warndlg([strLoop ' : End'], 0);

Data.SoundVelocity(Data.SoundVelocity < 1) = NaN;
Data.Time = cl_time('timeIfr', DateDatagram, HeureDatagram);

%% Pr�paration remplacement cl_time par datetime

Data.Datetime = datetime(Data.Time.timeMat', 'ConvertFrom', 'datenum');

%% Sauvegarde de la donn�e dans un fichier .mat

if ~isempty(nomFicMat)
    try
        if useCacheNetcdf == 1 % 1=Mat, 2 et 3=Netcdf
            save(nomFicMat, 'Data')
        else
            Data.Dimensions.nbPoints = length(Data.PingCounter);
            NetcdfUtils.Struct2Netcdf(Data, ncFileName, 'SurfaceSoundSpeed');
        end
    catch
        str = sprintf('Erreur ecriture fichier %s', nomFicMat);
        my_warndlg(str, 1);
    end
end

