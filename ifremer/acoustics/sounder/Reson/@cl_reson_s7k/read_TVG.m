% Lecture des datagrams de TVG dans un fichier .7sk
%
% Syntax
%   Data = read_TVG(a, identSondeur)
%
% Input Arguments
%   a            : Instance de cl_reson_s7k
%   identSondeur :
%
% Output Arguments
%   Data : Structure contenant les
%
% Examples
%   nomFic = getNomFicDatabase('RESON7125_20051116_221245.s7k')
%   a = cl_reson_s7k('nomFic', nomFic);
%   identSondeur = selectionSondeur(a);
%   Data = read_TVG(a, identSondeur)
%
%   figure; plot(Data.Longitude, Data.Latitude, '+'); grid on
%
% See also cl_reson_s7k cl_reson_s7k/plot_TVG cl_reson_s7k/plot_nav Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function Data = read_TVG(this, identSondeur, varargin)

global useCacheNetcdf %#ok<GVMIS>

[varargin, DataIndex] = getPropertyValue(varargin, 'DataIndex', []);
[varargin, subPing]   = getPropertyValue(varargin, 'subPing',   []); %#ok<ASGLU>

%% Check the number of elements

flag = checkIfOneElement(length(this));
if ~flag
    Data = [];
    return
end

%% On regarde si le fichier .mat existe

[nomDir, nomFic] = fileparts(this.nomFic);
nomFicMat  = fullfile(nomDir, 'SonarScope', [num2str(identSondeur) '_' nomFic '_TVG.mat']);
ncFileName = fullfile(nomDir, 'SonarScope', [nomFic '.nc']);

UN = 1;
if UN && exist(nomFicMat, 'file')
    Data = loadmat(nomFicMat, 'nomVar', 'Data');
    Data = Extract(Data, subPing);
    return
end
if UN && exist(ncFileName, 'file') && NetcdfUtils.existGrp(ncFileName, 'TVG', 'ncID', this.ncID)
    Data = NetcdfUtils.Netcdf2Struct(ncFileName, 'TVG', 'ncID', this.ncID);
    Data = Extract(Data, subPing);
    return
end

%% On cr�e au besoin le r�pertoire SonarScope

nomDirMat = fullfile(nomDir, 'SonarScope');
if ~exist(nomDirMat, 'dir')
    status = mkdir(nomDirMat);
    if status
        % %         disp(sprintf('Le repertoire %s a ete cree avec succes', nomDirMat))
    else
        fprintf('Impossible de ceer le repertoire %s.\nJe vous conseille de rendre cela possible, ca permet de formater la donnee en fichiers matlab,\nce qui permettra d''acceder beaucoup plus vite a la donnee\n', nomDirMat)
        nomFicMat = [];
    end
end

%% Lecture du fichier d'index

if isempty(DataIndex)
    [flag, DataIndex] = readFicIndex(this, identSondeur);
    if ~flag
        Data = [];
        return
    end
end

Position         = DataIndex(3,:);
TypeDatagram     = DataIndex(5,:);
PingCounter      = DataIndex(6,:);
PingSequence     = DataIndex(7,:);
DeviceIdentifier = DataIndex(8,:);

[flag, sub] = get_subDataCommon(7010, TypeDatagram, PingCounter, PingSequence, DeviceIdentifier, identSondeur);
if ~flag
    return
end

Position = Position(sub);
clear TypeDatagram

%% D�codage du fichier de donnees

typeIndian = 'l';
[fid, message] = fopen(this.nomFic, 'r', typeIndian);
if fid < 0
    disp(message);
    Data = [];
    return
end


N = length(Position);
MatInit = zeros(1, N);

Data.PingCounter       = MatInit;
Data.MultiPingSequence = MatInit;
Data.N                 = MatInit;
Data.GainValue         = MatInit';

DateDatagram  = zeros(1, N);
HeureDatagram = zeros(1, N);


if isempty(nomFicMat)
    [nomDir, nomFic] = fileparts(this.nomFic); %#ok
    str = sprintf('Loading file %s', nomFic);
else
    [nomDir, nomFic] = fileparts(this.nomFic); %#ok
    [nomDir, nomFic2] = fileparts(nomFicMat); %#ok
    str = sprintf('Loading file %s\nCreation file %s', nomFic, nomFic2);
end
hw = create_waitbar(str, 'N', N);
for k=1:N
    my_waitbar(k, N, hw);
    
    fseek(fid, Position(k), 'bof');
    
    [flag, DRF] = fread_DRF(fid);
    if ~flag
        break
    end
    [flag, TVG] = fread_RecordType_7010(fid);
    if ~flag
        break
    end
    
    Data.PingCounter(k)       = TVG.RTH.PingCounter;
    Data.MultiPingSequence(k) = TVG.RTH.MultiPingSequence;
    
    %     Data.SystemSerialNumber = SystemSerialNumber;
    Data.SystemSerialNumber   = DRF.DeviceIdentifier;
    Data.EmModel              = TVG.RTH.SonarId;
    
    DateDatagram(k)  = DRF.Date;
    HeureDatagram(k) = DRF.Time;
    
    Data.N(k)         = TVG.RTH.N;
    Data.GainValue(k,1: Data.N(k)) = TVG.RTH.GainValue;
end
my_close(hw)
fclose(fid);

Data.Time = cl_time('timeIfr', DateDatagram, HeureDatagram);
Data.PingCounter = unwrapPingCounter(Data.PingCounter);
Data.GainValue(Data.GainValue == 0) = NaN;

%% Sauvegarde de la donnee dans un fichier .mat

if ~isempty(nomFicMat)
    try
        if useCacheNetcdf == 1 % 1=Mat, 2 et 3=Netcdf
            save(nomFicMat, 'Data')
        else
            [Data.Dimensions.nbTVG, Data.Dimensions.nbSamples] = size(Data.GainValue);
            NetcdfUtils.Struct2Netcdf(Data, ncFileName, 'TVG');
        end
    catch
        str = sprintf('Erreur ecriture fichier %s', nomFicMat);
        my_warndlg(str, 1);
    end
end

Data = Extract(Data, subPing);


function DataOut = Extract(Data, subPing)

if isempty(subPing) || isempty(Data) || ~isfield(Data, 'N') || isempty(Data.N)
    DataOut = [];
    return
end
DataOut.PingCounter        = Data.PingCounter(subPing);
DataOut.MultiPingSequence  = Data.MultiPingSequence(subPing);
DataOut.N                  = Data.N(subPing);
DataOut.GainValue          = Data.GainValue(subPing);

T = Data.Time.timeMat;
DataOut.Time = cl_time('timeMat', T(subPing));
