function [flag, Data, BeamDataOut, subPing] = read_WaterColumnData(this, identSondeur, varargin)

global useCacheNetcdf %#ok<GVMIS>

[varargin, DataDepth]  = getPropertyValue(varargin, 'DataDepth',  []);
[varargin, Data7000]   = getPropertyValue(varargin, 'Data7000',   []);
[varargin, DataWC]     = getPropertyValue(varargin, 'DataWC',     []);
[varargin, DataIndex]  = getPropertyValue(varargin, 'DataIndex',  []);
[varargin, subPing]    = getPropertyValue(varargin, 'subPing',    []);
[varargin, Display]    = getPropertyValue(varargin, 'Display',    1);
[varargin, flagNoPing] = getPropertyValue(varargin, 'flagNoPing', 0);
[varargin, Mute]       = getPropertyValue(varargin, 'Mute',       0);

%% Check the number of elements

flag = checkIfOneElement(length(this));
if ~flag
    Data = [];
    BeamDataOut = [];
    return
end

%% R�cup�ration de la fr�quence d'�chantillonnage

if isempty(Data7000)
    Data7000 = read_sonarSettings(this, identSondeur, 'DataIndex', DataIndex);
    if isempty(Data7000)
        Data = [];
        BeamDataOut = [];
        return
    end
end
Data = Data7000;

%% Lecture de la bathy

if isempty(DataDepth)
    [flag, DataDepth, identSondeur] = read_depth_s7k(this, 'DataIndex', DataIndex, 'identSondeur', identSondeur);
    if ~flag
        return
    end
end
% NbBeamsBathy = DataDepth.NbBeams(1);

%% On regarde si le fichier .mat existe

[nomFicWaterColumnData, flagFilesExist] = ResonWaterColumnDataNames(this.nomFic, identSondeur, []);

[~, ~, Ext] = fileparts(nomFicWaterColumnData.Mat);
if isempty(DataWC)
    UN = 1;
    flagMat = 0;
    if UN && flagFilesExist && strcmp(Ext, '.mat')
        Data = loadmat(nomFicWaterColumnData.Mat, 'nomVar', 'Data');
        flagMat = 1;
    end
%     if  UN && flagFilesExist && strcmp(Ext, '.nc') && NetcdfUtils.existGrp(nomFicWaterColumnData.Mat, 'WaterColumnDataIFRData')
%     if  UN && flagFilesExist && strcmp(Ext, '.nc') && get(this, 'ExistGrpWaterColumnDataIFRData')
    if  UN && flagFilesExist && strcmp(Ext, '.nc') && this.ExistGrpWaterColumnDataIFRData
        Data = NetcdfUtils.Netcdf2Struct(nomFicWaterColumnData.Mat, 'WaterColumnDataIFRData', 'ncID', this.ncID);
        flagMat = 1;
    end
else
    Data = DataWC;
    flagMat = 1;
end

%% Recherche automatique des num�ros de faisceaux limites

[~, nomFicSeul] = fileparts(this.nomFic);
if flagMat
    Data.Dimensions.nbPings = length(Data.PingCounter);
    if isempty(DataIndex)
        [flag, DataIndex] = readFicIndex(this, identSondeur);
        if ~flag
            Data = [];
            BeamDataOut = [];
            return
        end
    end
    
    TypeDatagram = DataIndex(5,:);
    clear DataIndex
    
    isEmpty7042 = ~any(TypeDatagram == 7042);
    isEmpty7041 = ~any(TypeDatagram == 7041);
    if ~isEmpty7041
        codeDatagram = 7041;
    elseif ~isEmpty7042
        codeDatagram = 7042;
    else
         codeDatagram = 9999;
    end
else
    NbSoundes = sum(~isnan(DataDepth.Range));
    NbSoundes = NbSoundes / sum(NbSoundes);
    NbSoundes = cumsum(NbSoundes);
    idebBeamBathy = find(NbSoundes > 0.02, 1, 'first');
    ifinBeamBathy = find(NbSoundes < 0.98, 1, 'last');
    subBeamsReson = idebBeamBathy:ifinBeamBathy;
    subBeamsSsc   = 1:length(subBeamsReson);
    
    %% Lecture du fichier d'index
    
    if isempty(DataIndex)
        [flag, DataIndex] = readFicIndex(this, identSondeur);
        if ~flag
            Data = [];
            BeamDataOut = [];
            return
        end
    end
    
    Position         = DataIndex(3,:);
%     Taille           = DataIndex(4,:);
    TypeDatagram     = DataIndex(5,:);
    PingCounter      = DataIndex(6,:);
    PingSequence     = DataIndex(7,:);
    DeviceIdentifier = DataIndex(8,:);
    clear DataIndex
    
    % Lecture des datagrams TVG si ils existent.
    % Le fichier G:\SantaBarbara200805\20080513RAW\20080513_184723.s7k
    % produisait une erreue car il y avait 1 ping de moins en TVG qu'ne
    % donn�e BeamData
    %     %     isEmpty7010 = isempty(find(TypeDatagram == 7010, 1));
    %     isEmpty7010 = ~any(TypeDatagram == 7010);
    %     if isEmpty7010
    %         NbPings7010 = Inf;
    %     else
    %         NbPings7010 = length(find(TypeDatagram == 7010));
    %     end
    
    isEmpty7041 = ~any(TypeDatagram == 7041);
    isEmpty7042 = 1;
    if ~isEmpty7041
        % TODO : compl�ter get_subDataCommon pour le 7041 lorsque PingCounter et PingSequence seront renseign�s
        [flag, subWaterColumnData] = get_subDataCommon(7041, TypeDatagram, PingCounter, PingSequence, DeviceIdentifier, identSondeur);
        if ~flag
            BeamDataOut = [];
            return
        end
        codeDatagram = 7041;
    else
        isEmpty7042 = ~any(TypeDatagram == 7042);
        if ~isEmpty7042
            % TODO : compl�ter get_subDataCommon pour le 7042 lorsque PingCounter et PingSequence seront renseign�s
            [flag, subWaterColumnData] = get_subDataCommon(7042, TypeDatagram, PingCounter, PingSequence, DeviceIdentifier, identSondeur);
            if ~flag
                BeamDataOut = [];
                return
            end
            codeDatagram = 7042;
        end
    end
    if isEmpty7041 && isEmpty7042
        strLoop = sprintf('S7K cache makeup : %s : Group WaterColumnDataIFRData : No 7041 neither 7042 datagrams found', nomFicSeul);
        my_warndlg(strLoop, 0, 'Tag', 'No704xInThisFile');
        BeamDataOut = [];
        flag = 0;
        return
    end
    clear DeviceIdentifier
    
    if isempty(subWaterColumnData)
        my_warndlg('No Snippet data in this file', 1);
        BeamDataOut = [];
        return
    end
    
    if max(PingSequence(subWaterColumnData)) > 1 % Cas du multiping
        PCWC            = PingCounter(subWaterColumnData) * 4 + PingSequence(subWaterColumnData);
        PCSonarSettings = Data7000.PingCounter            * 4 + Data7000.PingSequence;
    else
        PCWC            = PingCounter(subWaterColumnData);
        PCSonarSettings = Data7000.PingCounter;
    end
    [~, subWC, sub7000] = intersect(PCWC, PCSonarSettings);
    %     Position = Position(subWaterColumnData(subWC));
    subWaterColumnData = subWaterColumnData(subWC);
    Position = Position(subWaterColumnData); %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Modif JMA le 28/10/2014 : refaire le traitement de tous les fichiers
        
    Data.sub7000 = sub7000;
    Data.subWC   = subWC;
    Data.Frequency                              = Data.Frequency(sub7000);
    Data.PingSequence                           = Data.PingSequence(sub7000);
    Data.PingCounter                            = Data.PingCounter(sub7000);
    Data.SampleRate                             = Data.SampleRate(sub7000);
    Data.ReceiverBandwith                       = Data.ReceiverBandwith(sub7000);
    Data.TxPulseWidth                           = Data.TxPulseWidth(sub7000);
    Data.TxPulseTypeIdentifier                  = Data.TxPulseTypeIdentifier(sub7000);
    Data.TxPulseEnvelopeParameter               = Data.TxPulseEnvelopeParameter(sub7000);
    Data.TxPulseReserved                        = Data.TxPulseReserved(sub7000);
    Data.MaxPingRate                            = Data.MaxPingRate(sub7000);
    Data.PingPeriod                             = Data.PingPeriod(sub7000);
    Data.RangeSelection                         = Data.RangeSelection(sub7000);
    Data.PowerSelection                         = Data.PowerSelection(sub7000);
    Data.GainSelection                          = Data.GainSelection(sub7000);
    Data.ControlFlags                           = Data.ControlFlags(sub7000);
    Data.ProjectorMagicNumber                   = Data.ProjectorMagicNumber(sub7000);
    Data.ProjectorBeamSteeringAngleVertical     = Data.ProjectorBeamSteeringAngleVertical(sub7000);
    Data.ProjectorBeamSteeringAngleHorizontal   = Data.ProjectorBeamSteeringAngleHorizontal(sub7000);
    Data.ProjectionBeam_3dB_WidthVertical       = Data.ProjectionBeam_3dB_WidthVertical(sub7000);
    Data.ProjectionBeam_3dB_WidthHorizontal     = Data.ProjectionBeam_3dB_WidthHorizontal(sub7000);
    Data.ProjectionBeamFocalPoint               = Data.ProjectionBeamFocalPoint(sub7000);
    Data.ProjectionBeamWeightingWindowType      = Data.ProjectionBeamWeightingWindowType(sub7000);
    Data.ProjectionBeamWeightingWindowParameter = Data.ProjectionBeamWeightingWindowParameter(sub7000);
    Data.TransmitFlags                          = Data.TransmitFlags(sub7000);
    Data.HydrophoneMagicNumber                  = Data.HydrophoneMagicNumber(sub7000);
    Data.ReceiveBeamWeightingWindow             = Data.ReceiveBeamWeightingWindow(sub7000);
    Data.ReceiveBeamWeightingParameter          = Data.ReceiveBeamWeightingParameter(sub7000);
    Data.ReceiveFlags                           = Data.ReceiveFlags(sub7000);
    Data.ReceiveBeamWidth                       = Data.ReceiveBeamWidth(sub7000);
    Data.BottomDetectionFilterInfoMinRange      = Data.BottomDetectionFilterInfoMinRange(sub7000);
    Data.BottomDetectionFilterInfoMaxRange      = Data.BottomDetectionFilterInfoMaxRange(sub7000);
    Data.BottomDetectionFilterInfoMinDepth      = Data.BottomDetectionFilterInfoMinDepth(sub7000);
    Data.BottomDetectionFilterInfoMaxDepth      = Data.BottomDetectionFilterInfoMaxDepth(sub7000);
    Data.BottomDetectRangeFilterFlag            = Data.BottomDetectRangeFilterFlag(sub7000);
    Data.BottomDetectDepthFilterFlag            = Data.BottomDetectDepthFilterFlag(sub7000);
    Data.Absorption                             = Data.Absorption(sub7000);
    Data.SoundVelocity                          = Data.SoundVelocity(sub7000);
    Data.Spreading                              = Data.Spreading(sub7000);
    Data.TxRollCompensationFlag                 = Data.TxRollCompensationFlag(sub7000);
    Data.TxRollCompensationValue                = Data.TxRollCompensationValue(sub7000);
    Data.TxPitchCompensationFlag                = Data.TxPitchCompensationFlag(sub7000);
    Data.TxPitchCompensationValue               = Data.TxPitchCompensationValue(sub7000);
    Data.RxRollCompensationFlag                 = Data.RxRollCompensationFlag(sub7000);
    Data.RxRollCompensationValue                = Data.RxRollCompensationValue(sub7000);
    Data.RxPitchCompensationFlag                = Data.RxPitchCompensationFlag(sub7000);
    Data.RxPitchCompensationValue               = Data.RxPitchCompensationValue(sub7000);
    Data.RxHeaveCompensationFlag                = Data.RxHeaveCompensationFlag(sub7000);
    Data.RxHeaveCompensationValue               = Data.RxHeaveCompensationValue(sub7000);
    if isa(Data.Time, 'cl_time')
        T = Data.Time.timeMat;
    else
        T = Data.Time;
    end
    Data.Time = cl_time('timeMat', T(sub7000));
    Data.Datetime = datetime(Data.Time.timeMat', 'ConvertFrom', 'datenum');
    
    
    % PingCounter = PingCounter(subWaterColumnData); % CHERCHER LA CORRESPONDANCE DES PINGS COUNTER PLUTOT QUE 1:min()
    %     subWaterColumnData = subWaterColumnData(1:min([NbPings7010, length(subWaterColumnData), length(VolatileSonarSettings_PingCounter)]));
    
    % Modif JMA le 15/12/2010 � l'arrache pour fichier NOAA 7111 7111_20101107_175549
    % subWaterColumnData = subWaterColumnData(1:min([NbPings7010, length(subWaterColumnData), length(VolatileSonarSettings_PingCounter), length(DataDepth.PingCounter)]));
    
    % Taille   = Taille(subWaterColumnData);
    clear TypeDatagram
    
    %% Decodage du fichier de donn�es
    
    [fid, message] = fopen(this.nomFic, 'r', 'l');
    if fid < 0
        my_warndlg(['cl_reson_s7k:read_WaterColumnData' message], 0);
        BeamDataOut = [];
        return
    end
    
    %% Lecture du nombre de faisceaux
    
    fseek(fid, Position(1), 'bof');
    flag = fread_DRF(fid);
    if ~flag
        BeamDataOut = [];
        return
    end
    
    N = length(Position);
    
    DateDatagram  = NaN(1, N);
    HeureDatagram = NaN(1, N);
    
    Data.PingCounter             = NaN(1, N, 'single');
    Data.MultiPingSequence       = NaN(1, N, 'single');
    Data.NbBeams                 = NaN(1, N, 'single');
    
    Data.BeamDescriptor          = NaN(N, 1, 'single');
    Data.BeginSampleDescriptor   = NaN(N, 1, 'single');
    Data.CenterSampleDistance    = NaN(N, 1, 'single');
    Data.BeamAcrossTrackDistance = NaN(N, 1, 'single');
    
    strLoop = infoLectureDatagrams(this, nomFicWaterColumnData.Mat, codeDatagram);
    for k=1:N
        infoLoop(strLoop, k, N, 'pings')
        
        fseek(fid, Position(k), 'bof');
        
        [flag, DRF] = fread_DRF(fid);
        if ~flag || (DRF.RecordTypeIdentifier ~= codeDatagram) % Ajout JMA le 06/12/2019
            N = k -1;
            break
        end
        %         if DRF.Size == 492834816
        %             %% Erreur sur fichier
        %             my_warndlg('Error reading .s7k.', 1, 'Tag', 'Data.RTH.NTropFrand');
        %             break
        %         end
        
        if ~isEmpty7041
            [flag, BeamData] = fread_RecordType_7041(fid, DRF, varargin{:}); % DRF.Size pour lecture en une seule fois
        elseif ~isEmpty7042
            [flag, BeamData] = fread_RecordType_7042(fid, DRF, varargin{:}); % DRF.Size pour lecture en une seule fois
        end
        if ~flag
            break
        end
        
        DateDatagram(k)  = DRF.Date;
        HeureDatagram(k) = DRF.Time;
        
        if k == 1
            Data.SystemSerialNumber = DRF.DeviceIdentifier;
        end
        Data.EmModel = BeamData.RTH.SonarId;
        
        if (DRF.OptionalDataOffset ~= 0) &&  ~isempty(BeamData.OD) && isfield(BeamData.OD, 'Frequency')
            Data.Frequency = BeamData.OD.Frequency / 1000; % En kHz
        end
        
        Data.PingCounter(k)       = BeamData.RTH.PingCounter;
        Data.MultiPingSequence(k) = BeamData.RTH.MultiPingSequence;
        Data.NbBeams(k)           = BeamData.RTH.N;
        %     Data.DataSampleType(k)  = BeamData.RTH.DataSampleType;
        
        if isfield(BeamData, 'RD') && isfield(BeamData.RD, 'BeamDescriptor')
            Data.BeamDescriptor(k,subBeamsSsc)          = BeamData.RD.BeamDescriptor(subBeamsReson);
            Data.BeginSampleDescriptor(k,subBeamsSsc)   = BeamData.RD.BeginSampleDescriptor(subBeamsReson);
            Data.EndSampleDescriptor(k,subBeamsSsc)     = BeamData.RD.EndSampleDescriptor(subBeamsReson);
        end
        if (DRF.OptionalDataOffset ~= 0) &&  ~isempty(BeamData.OD) && isfield(BeamData.OD, 'CenterSampleDistance')
            Data.CenterSampleDistance(k,subBeamsSsc)    = BeamData.OD.CenterSampleDistance(subBeamsReson);
            Data.BeamAcrossTrackDistance(k,subBeamsSsc) = BeamData.OD.BeamAcrossTrackDistance(subBeamsReson);
        end
    end
    my_warndlg([strLoop ' : End'], 0);
    
    %     my_close(hw)
    fclose(fid);

    
    if k ~= N
        N = k-1; % Au cas o� on est sorti en erreur
        
        if N == 0
            BeamDataOut = [];
            return
        end
        
        Data.PingCounter       = Data.PingCounter(1:N);
        Data.DateDatagram      = DateDatagram(1:N);
        Data.HeureDatagram     = HeureDatagram(1:N);
        Data.MultiPingSequence = Data.MultiPingSequence(1:N);
        Data.NbBeams           = Data.NbBeams(1:N);
%       Data.MinFilterInfo     = Data.MinFilterInfo(1:N);
%       Data.MaxFilterInfo     = Data.MaxFilterInfo(1:N);
    end
    
    Data.PingCounter = unwrapPingCounter(Data.PingCounter);
    Data.Time = timeIfr2Mat(DateDatagram, HeureDatagram);
    Data.Position = Position;
    
    Data.isEmpty7041 = isEmpty7041;
    Data.isEmpty7042 = isEmpty7042;
    
    subPingDepth = intersect(1:length(DataDepth.PingCounter), Data.sub7000);
%     DataDepth.PingCounter = unwrapPingCounter(DataDepth.PingCounter);
%     subPingDepth = intersect(DataDepth.PingCounter, Data.PingCounter);

    Data.MinFilterInfo = DataDepth.MinFilterInfo(subPingDepth);
    Data.MaxFilterInfo = DataDepth.MaxFilterInfo(subPingDepth);
    
    %% Sauvegarde de la donn�e dans un fichier .mat
    
    if ~isempty(nomFicWaterColumnData.Mat)
        try
            [~, ~, Ext] = fileparts(nomFicWaterColumnData.Mat);
            if strcmp(Ext, '.mat')
                save(nomFicWaterColumnData.Mat, 'Data')
            else
                Data.Dimensions.nbPings = length(Data.PingCounter);
                %             Data.Dimensions.nbBeams = length(Data.MinFilterInfo);
%                 Data.PingInWCD = false(Data.Dimensions.nbPings, 1);
                Data.PingInWCD = zeros(Data.Dimensions.nbPings, 1, 'uint8');
                NetcdfUtils.Struct2Netcdf(Data, nomFicWaterColumnData.Mat, 'WaterColumnDataIFRData', 'ncID', this.ncID);
            end
        catch ME %#ok<NASGU>
            str = sprintf('Erreur ecriture fichier %s', nomFicWaterColumnData.Mat);
            my_warndlg(str, 1);
        end
    end
end

if flagNoPing
    return
end

N = length(DataDepth.PingCounter);
if isempty(subPing)
    subPing = 1:N;
else
    if strcmp(subPing, 'None')
        BeamDataOut = [];
        return
    else
        subPing(subPing > N) = [];
        if isempty(subPing)
            BeamDataOut = [];
            return
        end
    end
end
% N = length(subPing);

[fid, message] = fopen(this.nomFic, 'r', 'l');
if fid < 0
    BeamDataOut = [];
    my_warndlg(['cl_reson_s7k:read_WaterColumnData' message], 0);
    return
end

[~, subNonTrouves] = setdiff(subPing, Data.sub7000);
if ~isempty(subNonTrouves)
    %subNonTrouves = Data.Heading(subNonTrouves)
    nearestPing = zeros(1,length(subNonTrouves));
    for k=1:length(subNonTrouves)
        [~, kmin] = min(Data.sub7000 - subNonTrouves(k));
        nearestPing(k) = Data.sub7000(kmin);
    end
    if ~Mute
        str1 = sprintf('Le(s) ping(s) "%s" n''existe(nt) pas en WC dans le fichier "%s\nLes pings les plus proches sont %s"', ...
            num2strCode(subNonTrouves), this.nomFic, num2strCode(nearestPing));
        str2 = sprintf('WC ping "%s" does not exist in WC in "%s"\nThe nearest one is %s', ...
            num2strCode(subNonTrouves), this.nomFic, num2strCode(nearestPing));
        my_warndlg(Lang(str1,str2), 0, 'Tag', 'PingWCNonExistant');
    end
end

%  [sub1, sub2, sub3, tdeb, tfin] = intersect3(t1, t2, t3)
[subIntersect, ~, ib] = intersect(subPing, Data.sub7000);
if isempty(subIntersect)
    str1 = 'Ce ping n''existe pas en WC';
    str2 = 'This ping does not exist in WC';
    my_warndlg(Lang(str1,str2), 0, 'Tag', 'PingDemandeNonTrouve');
    BeamDataOut = [];
    return
end
sub7000 = subIntersect;
% subWC   = Data.subWC(ib);

subWC   = ib; % Modif JMA le 17/01/2021 : � l'essai
subPing = subIntersect; % TODO : lorsque �a sera valid� alors remplacer tous les sub7000 par subPing

if useCacheNetcdf ~= 1 % Le cache est en netcdf. On lit une fois la variable PingInWCD au lieu de l'appeler � chaque ping (appel � la fonction get_PingInWCD(this, k, N, identSondeur))
    [nomFicWaterColumnData, flagFilesExist] = ResonWaterColumnDataNames(this.nomFic, identSondeur, []);
    if flagFilesExist
        if isempty(this.ncID)
            ncID = netcdf.open(nomFicWaterColumnData.Mat);
        else
            ncID = this.ncID;
        end
        grpID = netcdf.inqNcid(ncID, 'WaterColumnDataIFRData');
        varID = netcdf.inqVarID(grpID, 'PingInWCD');
        PingInWCD = netcdf.getVar(grpID, varID);
        if isempty(this.ncID)
            netcdf.close(ncID);
        end
    end
end



% Test JMA le 07/05/2021 pour �viter des "open" pour chaque ping dans "set_PingInWCD"
if isempty(this.ncID)
    this.ncID = netcdf.open(nomFicWaterColumnData.Mat, 'WRITE');
end



N = length(subWC);
flagVirtualMemory = (N > 10);
% hw = create_waitbar('Loading Water Column : 1/2', 'N', N);
[~, nomFicSeul] = fileparts(nomFicWaterColumnData.Mat);
strLoop = sprintf('S7K cache makeup : %s : Group WCD (catching datagrams %d)', nomFicSeul, codeDatagram);
if N > 1
    my_warndlg([strLoop ' : Begin'], 0);
end
for k=1:N
    %     my_waitbar(k, N, hw)
    infoLoop(strLoop, k, N, 'pings')
    
    kPing = sub7000(k); % Ou de mani�re �quivalente sub7000(subIntersect);
%     kPing = subPingDepth(k); % Ou de mani�re �quivalente sub7000(subIntersect);
    iPingWC = subWC(k);
    %     [flag, iPingWC] = reson_findPingFromTime(DataDepth, kPing, Data);
    %     if ~flag
    %         return
    %     end
    
    [nomFicWaterColumnData, flagFilesExist] = ResonWaterColumnDataNames(this.nomFic, identSondeur, kPing);
    
    [~, ~, Ext] = fileparts(nomFicWaterColumnData.Mat);
    if flagFilesExist && strcmp(Ext, '.mat')
        X = loadmat(nomFicWaterColumnData.AP, 'nomVar', 'BeamData');
        if isempty(X)
            BeamDataOut = [];
            my_warndlg([strLoop ' : Failed'], 0);
            return
        end
        BeamDataOut(k) = X; %#ok<AGROW>
%     elseif flagFilesExist && strcmp(Ext, '.nc') && NetcdfUtils.existGrp(nomFicWaterColumnData.Mat, getPingGrpTreeNames(kPing))
%     elseif flagFilesExist && strcmp(Ext, '.nc') && get_PingInWCD(this, k, N, identSondeur) % TODO : conserver cette ligne en commentaire
    elseif flagFilesExist && strcmp(Ext, '.nc') && PingInWCD(iPingWC)
        try
            X = NetcdfUtils.Netcdf2Struct(nomFicWaterColumnData.Mat, getPingGrpTreeNames(iPingWC), 'ncID', this.ncID);
            if isempty(X)
                BeamDataOut = [];
                my_warndlg([strLoop ' : Failed'], 0);
                return
            end
            BeamDataOut(k) = X; %#ok<AGROW>
        catch
%             getPingGrpTreeNames(kPing)
        end

    else
        nomDirImage = fileparts(nomFicWaterColumnData.AP);
        if ~exist(nomDirImage, 'dir')
            [flag, msg] = mkdir(nomDirImage);
            if ~flag
                my_warndlg(msg, 1);
                BeamDataOut = [];
                my_warndlg([strLoop ' : Failed'], 0);
                return
            end
        end
        
        fseek(fid, Data.Position(iPingWC), 'bof');
        
        [flag, DRF] = fread_DRF(fid);
        if ~flag
            BeamDataOut = [];
            break
        end
        
        if ~Data.isEmpty7041
            [flag, BeamData] = fread_RecordType_7041(fid, DRF, varargin{:});
        elseif ~Data.isEmpty7042
            if DRF.RecordTypeIdentifier ~= 7042 % Ajout JMA le 06/12/2019
                BeamDataOut = [];
                flag = 0;
                break
            end
            [flag, BeamData] = fread_RecordType_7042(fid, DRF, varargin{:});
        end
        if ~flag
            BeamDataOut = [];
            break
        end
        %         BeamData.iPing = kPing;
        BeamData.iPing = kPing;
        
        try
            if strcmp(Ext, '.mat')
                save(nomFicWaterColumnData.AP, 'BeamData')
            else
                NetcdfUtils.Struct2Netcdf(BeamData, nomFicWaterColumnData.Mat, getPingGrpTreeNames(iPingWC), 'ncID', this.ncID);
                this = set_PingInWCD(this, iPingWC, Data.Dimensions.nbPings, identSondeur);
                PingInWCD(iPingWC) = 1;
            end

        catch ME %#ok<NASGU>
            str = sprintf('Erreur ecriture fichier %s', nomFicWaterColumnData.AP);
            my_warndlg(str, 1);
        end
        
        BeamDataOut(k) = BeamData; %#ok<AGROW>
    end
    
    % TODO : Rustine pour corriger la mise � NaN de la r�flectivit� en dB : en
    % attente correction RESON
    %      BeamDataOut(k).RD.Amplitude(isnan(BeamDataOut(k).RD.Amplitude)) = 1; %#ok<AGROW> % Comment� par JMA le 26/09/2014
    
    
    if flagVirtualMemory
        BeamDataOut(k).RD.Amplitude = cl_memmapfile('Value', BeamDataOut(k).RD.Amplitude, 'LogSilence', 1); %#ok<AGROW>
    end
    
    fe = BeamDataOut(k).RTH.SampleRate;
    
    
    
    % Ajout JMA le 19/05/2021 pour donn�es ODOM MB2 de chez Teledyne
    if fe == 0
        fe = Data.SampleRate(iPingWC);
    end
    
    
    
    
    if iPingWC > length(Data.SoundVelocity)
        BeamDataOut = [];
        break
    end
    
    coef = 2*fe / Data.SoundVelocity(iPingWC);
    %     coef = 2*fe / Data.SoundVelocity(kPing); % Modif JMA le 07/01/2020 pour fichier Norbit G:\Norbit\20191218_Demo_Norbit\20191217_170122.s7k ping 181
    
    BeamDataOut(k).RD.Range = DataDepth.Range(kPing,:) * fe; %#ok<AGROW>
    
    BeamDataOut(k).RD.BeamDepressionAngle = DataDepth.BeamDepressionAngle(kPing,:); %#ok<AGROW>
    if isfield(DataDepth, 'TypeDatagram')
        if DataDepth(1).TypeDatagram == 7006
            I = DataDepth.QualityFactor(kPing,:);
            subNan = isnan(I);
            W = warning;
            warning('off', 'all')
            I = bitand(uint8(I),12);
            I = single(bitshift(uint8(I),-2));
            warning(W)
            I(subNan) = NaN;
            BeamDataOut(k).RD.QualityFactor = I; %#ok<AGROW>
        elseif DataDepth(1).TypeDatagram == 7027
            I = DataDepth.Flags(kPing,:);
            subNan = isnan(I);
            W = warning;
            warning('off', 'all')
            % Bit 0: 1 � Magnitude based detection
            % Bit 1: 1 � Phase based detection
            I = bitand(uint8(I),3);
            warning(W)
            I(subNan) = NaN;
            BeamDataOut(k).RD.QualityFactor = DataDepth.QualityFactor(kPing,:); %#ok<AGROW>
        end
    else
        % Conservation pour le cas des fichreis en cache avant l'introduction du champo TypeDatagram.
        I = DataDepth.QualityFactor(kPing,:);
        subNan = isnan(I);
        W = warning;
        warning('off', 'all')
        I = bitand(uint8(I),12);
        I = single(bitshift(uint8(I),-2));
        warning(W)
        I(subNan) = NaN;
        BeamDataOut(k).RD.QualityFactor = I; %#ok<AGROW>
    end
    
    subPh    = find(I == 2);
    subAmp   = find(I == 1);
    subBlend = find(I == 3);
    
    RPh    = BeamDataOut(k).RD.Range(subPh);
    RAmp   = BeamDataOut(k).RD.Range(subAmp);
    RBlend = BeamDataOut(k).RD.Range(subBlend);
    if Display
        if length(N) > 10
            figure(11000+kPing);
        else
            figure(11000)
        end
        hp = subplot(2,1,1);
        delete(get(hp, 'children'))
        PlotUtils.createSScPlot(hp, subPh,    RPh,    '+r'); hold on;
        PlotUtils.createSScPlot(hp, subAmp,   RAmp,   '+k');
        PlotUtils.createSScPlot(hp, subBlend, RBlend, '+m'); grid on; hold off;
        set(gca, 'YDir', 'reverse');
        title(sprintf('S7k detection for Ping %d', kPing))
    end
    
    sinA = sind(BeamDataOut(k).RD.BeamDepressionAngle);
    cosA = cosd(BeamDataOut(k).RD.BeamDepressionAngle);
    R = 1 + BeamDataOut(k).RD.Range;
    R = R / coef;
    XPh    = sinA(subPh)     .* R(subPh);
    XAmp   = sinA(subAmp)    .* R(subAmp);
    XBlend = sinA(subBlend)  .* R(subBlend);
    ZPh    = -cosA(subPh)    .* R(subPh);
    ZAmp   = -cosA(subAmp)   .* R(subAmp);
    ZBlend = -cosA(subBlend) .* R(subBlend);
    if Display
        hp = subplot(2,1,2);
        delete(get(hp, 'children'))
        PlotUtils.createSScPlot(hp, XPh,    ZPh,    '+r'); hold on;
        PlotUtils.createSScPlot(hp, XAmp,   ZAmp,   '+k');
        PlotUtils.createSScPlot(hp, XBlend, ZBlend, '+m'); grid on; hold off
    end
end
% my_close(hw)
fclose(fid);
if N > 1
    my_warndlg([strLoop ' : End'], 0);
end


function grpNames = getPingGrpTreeNames(kPing)

grpNames = {'WCD'; ['P' num2str(kPing, '%05d')]};
