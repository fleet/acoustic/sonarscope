function Data = read_attitude(this, identSondeur, varargin)

Data = read_attitude_1016(this, identSondeur, varargin{:});
if isempty(Data) || isempty(fieldnames(Data)) || isempty(Data.Roll)
    Data = read_attitude_1012(this, identSondeur, varargin{:});
    if ~isempty(Data) && ~isempty(Data.Roll)
        str1 = sprintf('C''est l''attitude lue dans les datagrammes 1012 au lieu de 1016 qui a �t� prise pour le fichier\n"%s"', this.nomFic);
        str2 = sprintf('Datagrammes 1012 are used instead of 1016 in file \n"%s"', this.nomFic);
        my_warndlg(Lang(str1,str2), 0, 'Tag', 'LectureAttitude1012OK', 'TimeDelay', 60);
    end
end


function Data = read_attitude_1016(this, identSondeur, varargin)

global useCacheNetcdf %#ok<GVMIS>

[varargin, DataIndex] = getPropertyValue(varargin, 'DataIndex', []); %#ok<ASGLU>

%% Check the number of elements

flag = checkIfOneElement(length(this));
if ~flag
    Data = [];
    return
end

%% On regarde si le fichier .mat existe

[nomDir, nomFic] = fileparts(this.nomFic);
nomFicMat  = fullfile(nomDir, 'SonarScope', [nomFic '_attitude.mat']);
ncFileName = fullfile(nomDir, 'SonarScope', [nomFic '.nc']);

UN = 1;
if UN && exist(nomFicMat, 'file') && checkFileCreation(nomFicMat, '26/09/2008')
    Data = loadmat(nomFicMat, 'nomVar', 'Data');
    % Pr�paration remplacement cl_time par datetimr
    if ~isempty(fieldnames(Data))
        Data.Datetime = datetime(Data.Time.timeMat', 'ConvertFrom', 'datenum');
    end
    return
end
if UN && exist(ncFileName, 'file') && NetcdfUtils.existGrp(ncFileName, 'Attitude', 'ncID', this.ncID)
    Data = NetcdfUtils.Netcdf2Struct(ncFileName, 'Attitude', 'ncID', this.ncID);
    if ~isempty(fieldnames(Data))
        if isa(Data.Time, 'cl_time')
            Data.Datetime = datetime(Data.Time.timeMat', 'ConvertFrom', 'datenum');
        else
            Data.Datetime = datetime(Data.Time, 'ConvertFrom', 'datenum');
        end
    end
    return
end

%% On cr�e au besoin le repertoire SonarScope

nomDirMat = fullfile(nomDir, 'SonarScope');
if ~exist(nomDirMat, 'dir')
    [status, msg] = mkdir(nomDirMat); %#ok
    if status
%         disp(sprintf('Le repertoire %s a ete cree avec succes', nomDirMat))
    else
        fprintf('Impossible de c�er le r�pertoire %s.\nJe vous conseille de rendre cela possible, ca permet de formater la donn�e en fichiers matlab,\nce qui permettra d''acc�der beaucoup plus vite � la donn�e.\n', nomDirMat);
        nomFicMat = [];
    end
end

%% Lecture du fichier d'index

if isempty(DataIndex)
    [flag, DataIndex] = readFicIndex(this, identSondeur);
    if ~flag
        Data = [];
        return
    end
end

Position     = DataIndex(3,:);
TypeDatagram = DataIndex(5,:);

sub = (TypeDatagram == 1016);
Position = Position(sub);
clear TypeDatagram

%% D�codage du fichier de donnees

typeIndian = 'l';
[fid, message] = fopen(this.nomFic, 'r', typeIndian);
if fid < 0
    disp(message);
    Data = [];
    return
end

Data.SystemSerialNumber = [];

N = length(Position);
Data.Roll      = NaN(0, 'single');
Data.Pitch     = NaN(0, 'single');
Data.Heave     = NaN(0, 'single');
Data.Heading   = NaN(0, 'single');
DateDatagram   = NaN(0);
HeureDatagram  = NaN(0);
TimeDifference = zeros(0);
[~, nomFicSeul] = fileparts(this.nomFic);
strLoop = infoLectureDatagrams(this, nomFicSeul, 1016);
for k=1:N
    infoLoop(strLoop, k, N, 'pings')
    
    fseek(fid, Position(k), 'bof');
    
    [flag, DRF] = fread_DRF(fid);
    if ~flag
        break
    end
    [flag, ATT] = fread_RecordType_1016(fid);
    if ~flag
        break
    end
    
    Data.SystemSerialNumber = identSondeur; % SystemSerialNumber;
    
    Data.Roll     = [Data.Roll;     ATT.RTH.Roll(:)];
    Data.Pitch    = [Data.Pitch;    ATT.RTH.Pitch(:)];
    Data.Heave    = [Data.Heave;    ATT.RTH.Heave(:)];
    Data.Heading  = [Data.Heading;  ATT.RTH.Heading(:)];
    DateDatagram  = [DateDatagram;  repmat(DRF.Date, length(ATT.RTH.Roll), 1)]; %#ok<AGROW>
    HeureDatagram = [HeureDatagram; repmat(DRF.Time, length(ATT.RTH.Roll), 1)]; %#ok<AGROW>
    
    if isfield(ATT.RTH, 'TimeDifference')
        TimeDifference = [TimeDifference; ATT.RTH.TimeDifference(:)]; %#ok<AGROW>
    end
end
fclose(fid);
my_warndlg([strLoop ' : End'], 0);

subNaN = isnan(Data.Roll);
Data.Roll(subNaN)      = [];
Data.Pitch(subNaN)     = [];
Data.Heave(subNaN)     = [];
Data.Heading(subNaN)   = [];
DateDatagram(subNaN)   = [];
HeureDatagram(subNaN)  = [];
TimeDifference(subNaN) = [];

Data.Roll      = Data.Roll';
Data.Pitch     = Data.Pitch';
Data.Heave     = Data.Heave';
Data.Heading   = Data.Heading';
DateDatagram   = DateDatagram';
HeureDatagram  = HeureDatagram';
TimeDifference = TimeDifference';

Data.Time     = cl_time('timeIfr', DateDatagram, HeureDatagram) + TimeDifference;
Data.Datetime = datetime(Data.Time.timeMat', 'ConvertFrom', 'datenum');

%% Sauvegarde de la donn�e dans un fichier .mat

if ~isempty(nomFicMat)
    try
        if useCacheNetcdf == 1 % 1=Mat, 2 et 3=Netcdf
            save(nomFicMat, 'Data')
        else
            if ~isempty(Data.Roll)
                Data.Dimensions.nbPings = length(Data.Roll);
                NetcdfUtils.Struct2Netcdf(Data, ncFileName, 'Attitude', 'ncID', this.ncID);
            end
        end
    catch ME %#ok<NASGU>
        str = sprintf('Erreur ecriture fichier %s', nomFicMat);
        my_warndlg(str, 1);
    end
end


function Data = read_attitude_1012(this, identSondeur, varargin)

global useCacheNetcdf %#ok<GVMIS>

[varargin, DataIndex] = getPropertyValue(varargin, 'DataIndex', []); %#ok<ASGLU>

%% Check the number of elements

flag = checkIfOneElement(length(this));
if ~flag
    Data = [];
    return
end

%% On regarde si le fichier .mat existe

[nomDir, nomFic] = fileparts(this.nomFic);
nomFicMat  = fullfile(nomDir, 'SonarScope', [nomFic '_attitude.mat']);
ncFileName = fullfile(nomDir, 'SonarScope', [nomFic '.nc']);

UN = 1;
if UN && exist(nomFicMat, 'file') && checkFileCreation(nomFicMat, '26/09/2008')
    Data = loadmat(nomFicMat, 'nomVar', 'Data');
    return
end
if UN && exist(ncFileName, 'file') && NetcdfUtils.existGrp(ncFileName, 'Attitude', 'ncID', this.ncID)
    Data = NetcdfUtils.Netcdf2Struct(ncFileName, 'Attitude', 'ncID', this.ncID);
    if isempty(fieldnames(Data))
        Data = [];
        return
    end
    return
end

%% On cr�e au besoin le r�pertoire SonarScope

nomDirMat = fullfile(nomDir, 'SonarScope');
if ~exist(nomDirMat, 'dir')
    [status, msg] = mkdir(nomDirMat); %#ok
    if status
%         disp(sprintf('Le repertoire %s a ete cree avec succes', nomDirMat))
    else
        fprintf('Impossible de c�er le r�pertoire %s.\nJe vous conseille de rendre cela possible, ca permet de formater la donn�e en fichiers matlab,\nce qui permettra d''acc�der beaucoup plus vite � la donn�e.\n', nomDirMat)
        nomFicMat = [];
    end
end

%% Lecture du fichier d'index

if isempty(DataIndex)
    [flag, DataIndex] = readFicIndex(this, identSondeur);
    if ~flag
        Data = [];
        return
    end
end

Position     = DataIndex(3,:);
TypeDatagram = DataIndex(5,:);

sub = (TypeDatagram == 1012);
Position = Position(sub);
clear TypeDatagram

%% D�codage du fichier de donn�es

typeIndian = 'l';
[fid, message] = fopen(this.nomFic, 'r', typeIndian);
if fid < 0
    disp(message);
    Data = [];
    return
end

N = length(Position);

Data.SystemSerialNumber = [];
Data.Roll               = [];
Data.Pitch              = [];
Data.Heave              = [];
Data.Heading            = [];

DateDatagram            = [];
HeureDatagram           = [];

strLoop = infoLectureDatagrams(this, nomFicMat, 1012);
for k=1:N
    infoLoop(strLoop, k, N, 'pings')
    
    fseek(fid, Position(k), 'bof');
    
    [flag, DRF] = fread_DRF(fid);
    if ~flag
        break
    end
    [flag, ATT] = fread_RecordType_1012(fid);
    if ~flag
        break
    end
    
    Data.SystemSerialNumber = identSondeur; % SystemSerialNumber;
    
    Data.Roll  = [Data.Roll  ATT.RTH.Roll];
    Data.Pitch = [Data.Pitch ATT.RTH.Pitch];
    Data.Heave = [Data.Heave ATT.RTH.Heave];
    
    DateDatagram  = [DateDatagram  DRF.Date]; %#ok
    HeureDatagram = [HeureDatagram DRF.Time]; %#ok
end
fclose(fid);

Data.Time = cl_time('timeIfr', DateDatagram, HeureDatagram);

%% Pr�paration remplacement cl_time par datetime

Data.Datetime = datetime(Data.Time.timeMat', 'ConvertFrom', 'datenum');

%% Sauvegarde de la donn�e dans un fichier .mat

if ~isempty(nomFicMat)
    try
        if useCacheNetcdf == 1 % 1=Mat, 2 et 3=Netcdf
            save(nomFicMat, 'Data')
        else
            Data.Dimensions.nbPings = length(Data.Roll);
            NetcdfUtils.Struct2Netcdf(Data, ncFileName, 'Attitude', 'ncID', this.ncID);
        end
    catch ME %#ok<NASGU>
        str = sprintf('Erreur ecriture fichier %s', nomFicMat);
        my_warndlg(str, 1);
    end
end
