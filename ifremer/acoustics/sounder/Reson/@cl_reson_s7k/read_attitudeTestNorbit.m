function Data = read_attitudeTestNorbit(this, identSondeur)

% TODO : pas de lecture et sauvegarde en Netcdf dans cette fonction qui
% sans doute temporaire

Data = read_attitude_1016(this, identSondeur);
if isempty(Data) || isempty(Data.Roll)
    Data = read_attitude_1012(this, identSondeur);
    if ~isempty(Data) && ~isempty(Data.Roll)
        str1 = sprintf('C''est l''attitude lue dans les datagrammes 1012 au lieu de 1016 qui a �t� prise pour le fichier\n"%s"', this.nomFic);
        str2 = sprintf('Datagrammes 1012 are used instead of 1016 in file \n"%s"', this.nomFic);
        my_warndlg(Lang(str1,str2), 0, 'Tag', 'LectureAttitude1012OK', 'TimeDelay', 60);
    end
end


function Data = read_attitude_1016(this, identSondeur)

%% Check the number of elements

flag = checkIfOneElement(length(this));
if ~flag
    Data = [];
    return
end

%% On regarde si le fichier .mat existe

[nomDir, nomFic] = fileparts(this.nomFic);
nomFicMat = fullfile(nomDir, 'SonarScope', [nomFic '_attitude.mat']);
UN = 1;
if UN && exist(nomFicMat, 'file') && checkFileCreation(nomFicMat, '26/09/2008')
    Data = loadmat(nomFicMat, 'nomVar', 'Data');
    % Pr�paration remplacement cl_time par datetimr
    Data.Datetime = datetime(Data.Time.timeMat', 'ConvertFrom', 'datenum');
    return
% else
%     try
%         delete(nomFicMat)
%     catch
%     end
end

%% On cr�e au besoin le repertoire SonarScope

nomDirMat = fullfile(nomDir, 'SonarScope');
if ~exist(nomDirMat, 'dir')
    [status, msg] = mkdir(nomDirMat); %#ok
    if status
%         disp(sprintf('Le repertoire %s a ete cree avec succes', nomDirMat))
    else
        fprintf('Impossible de c�er le r�pertoire %s.\nJe vous conseille de rendre cela possible, ca permet de formater la donn�e en fichiers matlab,\nce qui permettra d''acc�der beaucoup plus vite � la donn�e.\n', nomDirMat);
        nomFicMat = [];
    end
end

%% Lecture du fichier d'index

[flag, DataIndex] = readFicIndex(this, identSondeur);
if ~flag
    Data = [];
    return
end

Position     = DataIndex(3,:);
TypeDatagram = DataIndex(5,:);

sub = (TypeDatagram == 1016);
Position = Position(sub);
clear TypeDatagram

%% D�codage du fichier de donnees

typeIndian = 'l';
[fid, message] = fopen(this.nomFic, 'r', typeIndian);
if fid < 0
    disp(message);
    Data = [];
    return
end

Data.SystemSerialNumber = [];

N = length(Position);
Roll     = cell(1,N);
Pitch    = cell(1,N);
Heave    = cell(1,N);
Heading  = cell(1,N);
Datetime = cell(1,N);
strLoop = infoLectureDatagrams(this, nomFicMat, 1016);
for k=1:N
    infoLoop(strLoop, k, N, 'pings')
    
    fseek(fid, Position(k), 'bof');
    
    [flag, DRF] = fread_DRF(fid);
    if ~flag
        break
    end
    [flag, ATT] = fread_RecordType_1016(fid);
    if ~flag
        break
    end
    
    Data.SystemSerialNumber = identSondeur; % SystemSerialNumber;
    
    Roll{k}    = ATT.RTH.Roll;
    Pitch{k}   = ATT.RTH.Pitch;
    Heave{k}   = ATT.RTH.Heave;
    Heading{k} = ATT.RTH.Heading;
    
    if isfield(ATT.RTH, 'TimeDifference')
        Time = cl_time('timeIfr', DRF.Date, DRF.Time) + cumsum(ATT.RTH.TimeDifference);
        Datetime{k} = datetime(Time.timeMat', 'ConvertFrom', 'datenum');
    end
end
fclose(fid);
my_warndlg([strLoop ' : End'], 0);

Data.Roll     = [Roll{:}];
Data.Pitch    = [Pitch{:}];
Data.Heave    = [Heave{:}];
Data.Heading  = [Heading{:}];

Data.Datetime = [Datetime{:}];
Data.Time     = cl_time('timeMat', datenum(Data.Datetime));

%% Sauvegarde de la donn�e dans un fichier .mat

if ~isempty(nomFicMat)
    try
        save(nomFicMat, 'Data')
    catch ME %#ok<NASGU>
        str = sprintf('Erreur ecriture fichier %s', nomFicMat);
        my_warndlg(str, 1);
    end
end


function Data = read_attitude_1012(this, identSondeur)

%% Check the number of elements

flag = checkIfOneElement(length(this));
if ~flag
    Data = [];
    return
end

%% On regarde si le fichier .mat existe

[nomDir, nomFic] = fileparts(this.nomFic);
nomFicMat = fullfile(nomDir, 'SonarScope', [nomFic '_attitude.mat']);
UN = 1;
if UN && exist(nomFicMat, 'file') && checkFileCreation(nomFicMat, '26/09/2008')
    Data = loadmat(nomFicMat, 'nomVar', 'Data');
    return
% else
%     try
%         delete(nomFicMat)
%     catch
%     end
end

%% On cr�e au besoin le r�pertoire SonarScope

nomDirMat = fullfile(nomDir, 'SonarScope');
if ~exist(nomDirMat, 'dir')
    [status, msg] = mkdir(nomDirMat); %#ok
    if status
%         disp(sprintf('Le repertoire %s a ete cree avec succes', nomDirMat))
    else
        fprintf('Impossible de c�er le r�pertoire %s.\nJe vous conseille de rendre cela possible, ca permet de formater la donn�e en fichiers matlab,\nce qui permettra d''acc�der beaucoup plus vite � la donn�e.\n', nomDirMat)
        nomFicMat = [];
    end
end

%% On recup�re le numero de serie car pas donne dans le datagram contrairement � Simrad

% SystemSerialNumber = get_SonarSerialNumber(this, identSondeur);

%% Lecture du fichier d'index

[flag, DataIndex] = readFicIndex(this, identSondeur);
if ~flag
    Data = [];
    return
end

Position     = DataIndex(3,:);
TypeDatagram = DataIndex(5,:);

sub = (TypeDatagram == 1012);
Position = Position(sub);
clear TypeDatagram

%% D�codage du fichier de donn�es

typeIndian = 'l';
[fid, message] = fopen(this.nomFic, 'r', typeIndian);
if fid < 0
    disp(message);
    Data = [];
    return
end

N = length(Position);

Data.SystemSerialNumber = [];
Data.Roll               = [];
Data.Pitch              = [];
Data.Heave              = [];
Data.Heading            = [];

DateDatagram            = [];
HeureDatagram           = [];

strLoop = infoLectureDatagrams(this, nomFicMat, 1012);
for k=1:N
    infoLoop(strLoop, k, N, 'pings')
    
    fseek(fid, Position(k), 'bof');
    
    [flag, DRF] = fread_DRF(fid);
    if ~flag
        break
    end
    [flag, ATT] = fread_RecordType_1012(fid);
    if ~flag
        break
    end
    
    Data.SystemSerialNumber = identSondeur; % SystemSerialNumber;
    
    Data.Roll  = [Data.Roll  ATT.RTH.Roll];
    Data.Pitch = [Data.Pitch ATT.RTH.Pitch];
    Data.Heave = [Data.Heave ATT.RTH.Heave];
    
    DateDatagram  = [DateDatagram  DRF.Date]; %#ok
    HeureDatagram = [HeureDatagram DRF.Time]; %#ok
end
fclose(fid);

Data.Time = cl_time('timeIfr', DateDatagram, HeureDatagram);

%% Pr�paration remplacement cl_time par datetime

Data.Datetime = datetime(Data.Time.timeMat', 'ConvertFrom', 'datenum');

%% Sauvegarde de la donn�e dans un fichier .mat

if ~isempty(nomFicMat)
    try
        save(nomFicMat, 'Data')
    catch ME %#ok<NASGU>
        str = sprintf('Erreur ecriture fichier %s', nomFicMat);
        my_warndlg(str, 1);
    end
end
