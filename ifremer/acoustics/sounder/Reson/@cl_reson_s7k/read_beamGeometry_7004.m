% Lecture des datagrams de g�motrie des faiceaux dans un fichier .s7k
%
% Syntax
%   Data = read_beamGeometry_7004(a, identSondeur)
% 
% Input Arguments 
%   a : Instance de cl_reson_s7k
%
% Output Arguments 
%   Data : Structure contenant les infos
%
% Examples
%   nomFic = getNomFicDatabase('RESON7125_20051116_221245.s7k')
%   a = cl_reson_s7k('nomFic', nomFic);
%   Data = read_beamGeometry_7004(a, identSondeur)
%
%   figure; plot(Data.Longitude, Data.Latitude, '+'); grid on
%
% See also cl_reson_s7k cl_reson_s7k/view_depth Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function Data = read_beamGeometry_7004(this, identSondeur)

global useCacheNetcdf %#ok<GVMIS>

%% Check the number of elements

flag = checkIfOneElement(length(this));
if ~flag
    Data = [];
    return
end

%% On regarde si le fichier .mat existe identSondeur

[nomDir, nomFic] = fileparts(this.nomFic);
nomFicMat  = fullfile(nomDir, 'SonarScope', [num2str(identSondeur) '_' nomFic '_beamGeometry.mat']);
ncFileName = fullfile(nomDir, 'SonarScope', [nomFic '.nc']);

UN = 1;
if UN && exist(nomFicMat, 'file') && checkFileCreation(nomFicMat, '02/10/2008')
    Data = loadmat(nomFicMat, 'nomVar', 'Data');
    return
end
if UN && exist(ncFileName, 'file') && NetcdfUtils.existGrp(ncFileName, 'BeamGeometry', 'ncID', this.ncID)
    Data = NetcdfUtils.Netcdf2Struct(ncFileName, 'BeamGeometry', 'ncID', this.ncID);
    return
end

%% On cr�e au besoin le repertoire SonarScope

nomDirMat = fullfile(nomDir, 'SonarScope');
if ~exist(nomDirMat, 'dir')
    [status, msg] = mkdir(nomDirMat); %#ok
    if status
%         disp(sprintf('Le repertoire %s a ete cree avec succes', nomDirMat))
    else
        strWrn = [  'Impossible de cr�er le r�pertoire %s.\nJe vous conseille de rendre cela possible, ' ...
                    '�a permet de formatter la donn�e en fichiers Matlab,\nce qui permettra d''acc�der '...
                    'beaucoup plus vite � la donn�e\n'];
        fprintf(strWrn, nomDirMat)
        nomFicMat = [];
    end
end

%% Lecture du fichier d'index

[flag, DataIndex] = readFicIndex(this, identSondeur);
if ~flag
    Data = [];
    return
end

Position     = DataIndex(3,:);
TypeDatagram = DataIndex(5,:);

sub = (TypeDatagram == 7004);
Position = Position(sub);
clear TypeDatagram

%% Decodage du fichier de donn�es

typeIndian = 'l';
[fid, message] = fopen(this.nomFic, 'r', typeIndian);
if fid < 0
    disp(message);
    Data = [];
    return
end

N = length(Position);

if N == 0
    str = sprintf('No 7004 datagram in file %s (BeamGeometry)', this.nomFic);
    my_warndlg(str, 0, 'Tag', 'No7004Datagram');
end

DateDatagram  = [];
HeureDatagram = [];
for k=1:N
    fseek(fid, Position(k), 'bof');
    
    [flag, DRF] = fread_DRF(fid);
    if ~flag
        break
    end
    
    [flag, Data7004] = fread_RecordType_7004(fid);
    if ~flag
        N = k-1;
        break
    end
    DateDatagram(k)  = DRF.Date; %#ok<AGROW>
    HeureDatagram(k) = DRF.Time; %#ok<AGROW>

    FieldNames 	= fieldnames(Data7004.RD);
    subc        = 1:Data7004.RTH.N;

    for j=1:length(FieldNames)
        Data.(FieldNames{j})(k,subc) = Data7004.RD.(FieldNames{j});
    end
    %     Data.DRF(i).SystemSerialNumber = DeviceIdentifier; % SystemSerialNumber;
end
fclose(fid);

Data.Time = cl_time('timeIfr', DateDatagram, HeureDatagram);

%% Sauvegarde de la donnee dans un fichier .mat

if ~isempty(nomFicMat)
    try
        if useCacheNetcdf == 1 % 1=Mat, 2 et 3=Netcdf
            save(nomFicMat, 'Data')
        else
            [Data.Dimensions.nbRows, Data.Dimensions.nbBeams] = size(Data.BeamVerticalDirectionAngle);
            NetcdfUtils.Struct2Netcdf(Data, ncFileName, 'Depth');
        end
    catch
        str = sprintf('Erreur ecriture fichier %s', nomFicMat);
        my_warndlg(str, 1);
    end
end
