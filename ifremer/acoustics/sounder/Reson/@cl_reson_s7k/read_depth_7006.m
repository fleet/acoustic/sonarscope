function [Data, nomFicMat, ncFileName] = read_depth_7006(this, identSondeur, varargin)

global useCacheNetcdf %#ok<GVMIS>

[varargin, DataIndex] = getPropertyValue(varargin, 'DataIndex', []); %#ok<ASGLU>

nomFicMat  = [];
ncFileName = [];

%% Check the number of elements

flag = checkIfOneElement(length(this));
if ~flag
    Data = [];
    return
end

%% R�cup�ration de la fr�quence d'�chantillonnage

Data7000 = read_sonarSettings(this, identSondeur, 'DataIndex', DataIndex);

%% On regarde si le fichier .mat existe

% TESTER LA DATE COMME DANS SIMRAD

[nomDir, nomFic] = fileparts(this.nomFic);
nomFicMat  = fullfile(nomDir, 'SonarScope', [num2str(identSondeur) '_' nomFic '_depth.mat']);
ncFileName = fullfile(nomDir, 'SonarScope', [nomFic '.nc']);

UN = 1;
if UN && exist(nomFicMat, 'file') && checkFileCreation(nomFicMat, '29/02/2016')
    Data = loadmat(nomFicMat, 'nomVar', 'Data');
    if isfield(Data, 'ReflectivityFromSnippets100')
        % Pr�paration remplacement cl_time par datetime
        Data.Datetime = datetime(Data.Time.timeMat', 'ConvertFrom', 'datenum');
        return
    end
end
if UN && exist(ncFileName, 'file') && NetcdfUtils.existGrp(ncFileName, 'Depth', 'ncID', this.ncID)
    Data = NetcdfUtils.Netcdf2Struct(ncFileName, 'Depth', 'ncID', this.ncID);
    if isfield(Data, 'ReflectivityFromSnippets100')
        % Pr�paration remplacement cl_time par datetime
        Data.Datetime = datetime(Data.Time.timeMat(:), 'ConvertFrom', 'datenum');
        return
    end
    return
end

%% On cr�e au besoin le r�pertoire SonarScope

nomDirMat = fullfile(nomDir, 'SonarScope');
if ~exist(nomDirMat, 'dir')
    status = mkdir(nomDirMat);
    if status
        special_message('CreationRepertoireReussie', nomDirMat)
    else
        special_message('CreationRepertoireEchec', nomDirMat)
        nomFicMat = [];
    end
end

%% Lecture du fichier d'index

if isempty(DataIndex)
    [flag, DataIndex] = readFicIndex(this, identSondeur);
    if ~flag
        Data = [];
        return
    end
end

Position         = DataIndex(3,:);
TypeDatagram     = DataIndex(5,:);
PingCounter      = DataIndex(6,:);
PingSequence     = DataIndex(7,:);
DeviceIdentifier = DataIndex(8,:);
    
[flag, subDataDepth, subCaris] = get_subDataCommon(7006, TypeDatagram, PingCounter, PingSequence, DeviceIdentifier, identSondeur);
if ~flag
    Data = [];
    return
end

Position7006 = Position(subDataDepth);
% Taille   = Taille(subDataDepth);
PingSequence = PingSequence(subDataDepth);
PingCounter  = PingCounter(subDataDepth);

if max(PingSequence) > 1 % Cas du multiping
    PCDepth         = (PingCounter          * 4) + PingSequence;
    PCSonarSettings = (Data7000.PingCounter * 4) + Data7000.PingSequence;
else
    PCDepth         = PingCounter;
    PCSonarSettings = Data7000.PingCounter;
end
[~, subDepth, sub7000] = intersect(PCDepth, PCSonarSettings);


% Ajout JMA le 28/10/2015 pour fichier
% I:\MarieClaireFabri\Cassidaigne\ESSROV2010_RESON7150\20100911_231044_PP-7150-24kHz-.s7k
[~, subOrdre] = sort(subDepth);
subDepth = subDepth(subOrdre);
sub7000  = sub7000(subOrdre);
% Fin ajout JMA le 28/10/2015


Position7006 = Position7006(subDepth);
% PingSequence = PingSequence(subDepth);
% PingCounter  = PingCounter(subDepth);
 
%% D�codage du fichier de donn�es

typeIndian = getIndianAll(this.nomFic);
[fid, message] = fopen(this.nomFic, 'r', typeIndian);
if fid < 0
    my_warndlg(['cl_reson_s7k:read_depth_7006' message], 0);
    return
end

%% Lecture du 1002 : Coordonn�es du capteur apr�s calibration

%{
subData1002 = (TypeDatagram == 1002);
Position1002 = Position(subData1002);
N = length(Position1002);
DateDatagram1002  = zeros(1,N);
HeureDatagram1002 = zeros(1,N);

str1 = 'Lecture des datagrammes 1002';
str2 = 'Reading 1002 datagrams';
strLoop = Lang(str1,str2);
for k2=1:N
    infoLoop(strLoop, k2, N, 'records')
    
    fseek(fid, Position1002(k2), 'bof');
    [flag, DRF] = fread_DRF(fid);
    if ~flag
        break
    end
    DateDatagram1002(k2)  = DRF.Date;
    HeureDatagram1002(k2) = DRF.Time;
    
    [flag, Data1002] = fread_RecordType_1002(fid);
    if ~flag
        break
    end
end
%}

%% Lecture du 7004

subData7004 = (TypeDatagram == 7004);
Position7004 = Position(subData7004);
N = length(Position7004);
DateDatagram7004             = zeros(1,N);
HeureDatagram7004            = zeros(1,N);
BeamVerticalDirectionAngle   = zeros(N, 1);
BeamHorizontalDirectionAngle = zeros(N, 1);
s_3dBBeamWithY               = zeros(N, 1);
s_3dBBeamWithX               = zeros(N, 1);

strLoop = infoLectureDatagrams(this, nomFicMat, 7004);
for k2=1:N
	infoLoop(strLoop, k2, N, 'records')
    
    fseek(fid, Position7004(k2), 'bof');
    [flag, DRF] = fread_DRF(fid);
    if ~flag
        N = k2-1;
        break
    end
    DateDatagram7004(k2)  = DRF.Date;
    HeureDatagram7004(k2) = DRF.Time;
    
    [flag, Data7004] = fread_RecordType_7004(fid);
    if ~flag
        N = k2-1;
        break
    end
    subc = 1:Data7004.RTH.N;
    BeamVerticalDirectionAngle(k2,subc)   = Data7004.RD.BeamVerticalDirectionAngle;
    BeamHorizontalDirectionAngle(k2,subc) = Data7004.RD.BeamHorizontalDirectionAngle;
    s_3dBBeamWithY(k2,subc)               = Data7004.RD.s_3dBBeamWithY;
    s_3dBBeamWithX(k2,subc)               = Data7004.RD.s_3dBBeamWithX;
end
my_warndlg([strLoop ' : End'], 0);

DateDatagram7004  = DateDatagram7004(1:N);
HeureDatagram7004 = HeureDatagram7004(1:N);
BeamVerticalDirectionAngle   = BeamVerticalDirectionAngle(1:N,:);
BeamHorizontalDirectionAngle = BeamHorizontalDirectionAngle(1:N,:);
s_3dBBeamWithY               = s_3dBBeamWithY(1:N,:);
s_3dBBeamWithX               = s_3dBBeamWithX(1:N,:);

Data7004.Time = cl_time('timeIfr', DateDatagram7004, HeureDatagram7004);
% figure(7890988);
% subplot(4,1,1); plot(BeamVerticalDirectionAngle'); grid on; title('BeamVerticalDirectionAngle');
% subplot(4,1,2); plot(BeamHorizontalDirectionAngle'); grid on; title('BeamHorizontalDirectionAngle');
% subplot(4,1,3); plot(s_3dBBeamWithY'); grid on; title('s_3dBBeamWithX');
% subplot(4,1,4); plot(s_3dBBeamWithX'); grid on; title('s_3dBBeamWithX');

%% Lecture du 1008

subData1008  = (TypeDatagram == 1008);
Position1008 = Position(subData1008);
N = length(Position1008);
DateDatagram1008  = NaN(1,N);
HeureDatagram1008 = NaN(1,N);
Immersion1008     = NaN(N,1);

strLoop = infoLectureDatagrams(this, nomFicMat, 1008);
for k2=1:N
    infoLoop(strLoop, k2, N, 'records')
    
    fseek(fid, Position1008(k2), 'bof');
    [flag, DRF] = fread_DRF(fid);
    if ~flag
        N = k2-1;
        break
    end
    DateDatagram1008(k2)  = DRF.Date;
    HeureDatagram1008(k2) = DRF.Time;
    
    [flag, Data1008] = fread_RecordType_1008(fid);
    if ~flag
        N = k2-1;
        break
    end
    Immersion1008(k2) = -Data1008.RTH.Depth;
end
my_warndlg([strLoop ' : End'], 0);

DateDatagram1008  = DateDatagram1008(1:N);
HeureDatagram1008 = HeureDatagram1008(1:N);
Immersion1008     = Immersion1008(1:N,:);
Data1008.Time     = cl_time('timeIfr', DateDatagram1008, HeureDatagram1008);
% figure(7890948);
% plot(Immersion1008'); grid on; title('Immersion1008');
    
%% Lecture des datagrams "VerticalDepth"

% ATTENTION : IL FAUDRAIT S'ASSURER QUE PingCounter EST LE MEME POUR ASSURER
% LE SYNCHONISME
DataVerticalDepth = read_verticalDepth(this, identSondeur, 'DataIndex', DataIndex);

%% Lecture du 7006

N = length(Position7006);
% N = min(N, length(DataVerticalDepth.PingCounter));
MatInit = NaN(1, N, 'single');

% SampleRate PingSequence
Data.SampleRate = Data7000.SampleRate(sub7000);
% Data.PingSequence = Data7000.PingSequence(sub7000;
Data.ProjectorBeamSteeringAngleVertical = Data7000.ProjectorBeamSteeringAngleVertical(sub7000) * (180/pi);

Data.PingCounter       = MatInit;
Data.MultiPingSequence = MatInit;
Data.Heading           = MatInit;
Data.SoundSpeed        = MatInit;
Data.TransducerDepth   = MatInit;
Data.NbBeams           = MatInit;
% Data.TransducerDepthOffsetMultiplier      = MatInit;
Data.Frequency    = MatInit;
Data.HeightSource = MatInit;
Data.Tide         = MatInit;
Data.Roll         = MatInit;
Data.Pitch        = MatInit;
Data.Heave        = MatInit;
Data.VehicleDepth = MatInit;

MatInit = NaN(1, N, 'double');
Data.Latitude  = MatInit;
Data.Longitude = MatInit;
DateDatagram   = NaN(1, N);
HeureDatagram  = NaN(1, N);

MatInit = NaN(N, 1, 'single');
Data.Depth               = MatInit;
Data.AcrossDist          = MatInit;
Data.AlongDist           = MatInit;
Data.BeamDepressionAngle = MatInit;
Data.BeamAzimuthAngle    = MatInit;
Data.Range               = MatInit;
Data.QualityFactor       = MatInit;
Data.MinFilterInfo       = MatInit;
Data.MaxFilterInfo       = MatInit;

% Data.LengthDetection            = MatInit;
Data.Reflectivity                 = MatInit;
Data.BeamVerticalDirectionAngle   = MatInit;
Data.BeamHorizontalDirectionAngle = MatInit;
Data.s_3dBBeamWithY               = MatInit;
Data.s_3dBBeamWithX               = MatInit;
Data.subCaris                     = subCaris(:);

strLoop = infoLectureDatagrams(this, nomFicMat, 7006);
for k=1:N
    infoLoop(strLoop, k, N, 'records')

    fseek(fid, Position7006(k), 'bof');

    [flag, DRF] = fread_DRF(fid);
    if ~flag
        break
    end
    
    [flag, PingNumber, Sequence, nbFaisceaux, DEPTH] = fread_RecordType_7006(fid, DRF); %#ok<ASGLU>
    if ~flag
        N = k -1;
        break
    end
    
    DateDatagram(k)  = DRF.Date;
    HeureDatagram(k) = DRF.Time;
    
    Data.SystemSerialNumber = DRF.DeviceIdentifier;
    Data.EmModel            = DEPTH.RTH.SonarId;
    Data.PingCounter(k)     = DEPTH.RTH.PingCounter;
    Data.MultiPingSequence(k) = DEPTH.RTH.MultiPingSequence;

    Data.SoundSpeed(k)      = DEPTH.RTH.SoundVelocity;
    Data.TransducerDepth(k) = 0;%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% -6.41; %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    Data.NbBeams(k)         = DEPTH.RTH.N;
    
    if isfield(DEPTH, 'OD') % Fichier provenant de PDS
        Data.Heading(k)      = DEPTH.OD.Heading;
        Data.Frequency(k)    = DEPTH.OD.Frequency;
        Data.Latitude(k)     = DEPTH.OD.Latitude;
        Data.Longitude(k)    = DEPTH.OD.Longitude;
        Data.HeightSource(k) = DEPTH.OD.HeightSource;
        Data.Tide(k)         = DEPTH.OD.Tide;
        Data.Roll(k)         = DEPTH.OD.Roll;
        Data.Pitch(k)        = DEPTH.OD.Pitch;
        Data.Heave(k)        = DEPTH.OD.Heave;
        Data.VehicleDepth(k) = DEPTH.OD.VehicleDepth;
    else % Fichier provenant du 7K center
        if ~isempty(DEPTH.RD.MinFilterInfo)&& ~all(DEPTH.RD.MaxFilterInfo == 0)
%             Data.MinFilterInfo(k,subc) = DEPTH.RD.MinFilterInfo;
%             Data.MaxFilterInfo(k,subc) = DEPTH.RD.MaxFilterInfo;
            Data.MinFilterInfo(k) = min(DEPTH.RD.MinFilterInfo);
            Data.MaxFilterInfo(k) = max(DEPTH.RD.MaxFilterInfo);
        end
    end
    
    subc = 1:min(Data.NbBeams(k), size(BeamVerticalDirectionAngle,2));
    Data.Range(k,subc)         = DEPTH.RD.Range(subc); % TwoWayTravelTimeInSeconds
%     Data.Range(k,subc)         = Data.SampleRate(k) * DEPTH.RD.Range; % Nombre de samples
    Data.QualityFactor(k,subc) = DEPTH.RD.Quality(subc);
    Data.Reflectivity(k,subc)  = DEPTH.RD.Intensity(subc);
    
    ind7004 = 1;
    Data.BeamVerticalDirectionAngle(k,subc)   = BeamVerticalDirectionAngle(ind7004,subc);
    Data.BeamHorizontalDirectionAngle(k,subc) = BeamHorizontalDirectionAngle(ind7004,subc);
    Data.s_3dBBeamWithY (k,subc)              = s_3dBBeamWithY(ind7004,subc);
    Data.s_3dBBeamWithX(k,subc)               = s_3dBBeamWithX(ind7004,subc);
    
    if k == 1
        MatInit = NaN(N, Data.NbBeams(k), 'single');
        Data.Depth               = MatInit;
        Data.AcrossDist          = MatInit;
        Data.AlongDist           = MatInit;
        Data.BeamDepressionAngle = MatInit;
        Data.BeamAzimuthAngle    = MatInit;
    end
    if isfield(DEPTH, 'OD')
        for k2=1:min(Data.NbBeams(k), size(BeamVerticalDirectionAngle,2))
            n = k2;
            Depth               = DEPTH.OD.BeamDepth(k2);
            AcrossDist          = DEPTH.OD.BeamAcrossTrackDistance(k2);
            AlongDist           = DEPTH.OD.BeamAlongTrackDistance(k2);
            BeamDepressionAngle = DEPTH.OD.PointingAngle(k2);
            BeamAzimuthAngle    = DEPTH.OD.AzimuthAngle(k2);

            Data.Depth(k,n)               = -Depth;
            Data.AcrossDist(k,n)          = AcrossDist;
            Data.AlongDist(k,n)           = AlongDist;
            Data.BeamDepressionAngle(k,n) = BeamDepressionAngle;
            Data.BeamAzimuthAngle(k,n)    = BeamAzimuthAngle;
            if DEPTH.RD.Range(k2) == 0 % TODO : attention, ajout pour la moulinette s7k2hac le 08/09/2016 (modif r�alis�e avec Roger)
                if k2 > (Data.NbBeams(k) /2)
                    Data.BeamDepressionAngle(k,n) = BeamDepressionAngle;
                    Data.BeamAzimuthAngle(k,n)    = 360 - BeamAzimuthAngle - 270;
                else
                    Data.BeamDepressionAngle(k,n) = -BeamDepressionAngle;
                    Data.BeamAzimuthAngle(k,n)    = BeamAzimuthAngle - 270;
                end
            else
                if ~isempty(DataVerticalDepth.AcrossTrackDistance) && (AcrossDist > DataVerticalDepth.AcrossTrackDistance(min(k, length(DataVerticalDepth.AcrossTrackDistance))))
                    Data.BeamDepressionAngle(k,n) = BeamDepressionAngle;
                    Data.BeamAzimuthAngle(k,n)    = 360 - BeamAzimuthAngle - 270;
                else
                    Data.BeamDepressionAngle(k,n) = -BeamDepressionAngle;
                    Data.BeamAzimuthAngle(k,n)    = BeamAzimuthAngle - 270;
                end
            end
        end
    else
%         Data.BeamDepressionAngle(k,:) = BeamHorizontalDirectionAngle(k,:);
        Data.BeamDepressionAngle(k,1:length(subc)) = BeamHorizontalDirectionAngle(k,subc); % Modifi� par JMA mission BICOSE
%         Data.BeamAzimuthAngle(k,:)    = 360 - BeamVerticalDirectionAngle(k,:) - 270;
        Data.BeamAzimuthAngle(k,1:length(subc)) = 360 - BeamVerticalDirectionAngle(k,subc) - 270; % Modifi� par JMA mission BICOSE
        
        % Rajout� mission BICOSE
        Data.Depth(k,1:length(subc))      = NaN;
        Data.AcrossDist(k,1:length(subc)) = NaN;
        Data.AlongDist(k,1:length(subc))  = NaN;
    end
end
% my_close(hw)
fclose(fid);
my_warndlg([strLoop ' : End'], 0);

subl = 1:N;
Data.Time = cl_time('timeIfr', DateDatagram(subl), HeureDatagram(subl));
Data.SampleRate                     = Data.SampleRate(subl);
Data.ProjectorBeamSteeringAngleVertical = Data.ProjectorBeamSteeringAngleVertical(subl);
Data.PingCounter                    = Data.PingCounter(subl);
Data.MultiPingSequence              = Data.MultiPingSequence(subl);
Data.Heading                        = Data.Heading(subl);
Data.SoundSpeed                     = Data.SoundSpeed(subl);
Data.TransducerDepth                = Data.TransducerDepth(subl);
Data.NbBeams                        = Data.NbBeams(subl);
% Data.TransducerDepthOffsetMultiplier = Data.TransducerDepthOffsetMultiplier(subl);
Data.Frequency                      = Data.Frequency(subl);
Data.HeightSource                   = Data.HeightSource(subl);
Data.Tide                           = Data.Tide(subl);
Data.Roll                           = Data.Roll(subl);
Data.Pitch                          = Data.Pitch(subl);
Data.Heave                          = Data.Heave(subl);
Data.VehicleDepth                   = Data.VehicleDepth(subl);
Data.Latitude                       = Data.Latitude(subl);
Data.Longitude                      = Data.Longitude(subl);
Data.Depth                          = Data.Depth(subl,:);
Data.AcrossDist                     = Data.AcrossDist(subl,:);
Data.AlongDist                      = Data.AlongDist(subl,:);
Data.BeamDepressionAngle            = Data.BeamDepressionAngle(subl,:);
Data.BeamAzimuthAngle               = Data.BeamAzimuthAngle(subl,:);
Data.Range                          = Data.Range(subl,:);
Data.QualityFactor                  = Data.QualityFactor(subl,:);
Data.Reflectivity                   = Data.Reflectivity(subl,:);
Data.BeamVerticalDirectionAngle     = Data.BeamVerticalDirectionAngle(subl,:);
Data.BeamHorizontalDirectionAngle   = Data.BeamHorizontalDirectionAngle(subl,:);
Data.s_3dBBeamWithY                 = Data.s_3dBBeamWithY(subl,:);
Data.s_3dBBeamWithX                 = Data.s_3dBBeamWithX(subl,:);

Data.TxRollCompensationFlag         = Data7000.TxRollCompensationFlag(sub7000(subl));
Data.TxRollCompensationValue        = Data7000.TxRollCompensationValue(sub7000(subl));
Data.TxPitchCompensationFlag        = Data7000.TxPitchCompensationFlag(sub7000(subl));
Data.TxPitchCompensationValue       = Data7000.TxPitchCompensationValue(sub7000(subl));
Data.RxRollCompensationFlag         = Data7000.RxRollCompensationFlag(sub7000(subl));
Data.RxRollCompensationValue        = Data7000.RxRollCompensationValue(sub7000(subl));
Data.RxPitchCompensationFlag        = Data7000.RxPitchCompensationFlag(sub7000(subl));
Data.RxPitchCompensationValue       = Data7000.RxPitchCompensationValue(sub7000(subl));
Data.RxHeaveCompensationFlag        = Data7000.RxHeaveCompensationFlag(sub7000(subl));
Data.RxHeaveCompensationValue       = Data7000.RxHeaveCompensationValue(sub7000(subl));
Data.MinFilterInfo                  = Data.MinFilterInfo(subl);
Data.MaxFilterInfo                  = Data.MinFilterInfo(subl);

Data.DepthPingCounterBeforeUnwrap = Data.PingCounter;
Data.PingCounter = unwrapPingCounter(Data.PingCounter);

if ~isempty(Immersion1008) && any(~isnan(Immersion1008))
    Data.VehicleDepth = interp1(Data1008.Time, Immersion1008, Data.Time);
end

%% Traitement des non-valeurs

% Data.Mask = zeros(size(Data.Depth), 'single');
% subNan = find((Data.Range == 0) | (Data.Range <= 2));
subNan = find(Data.Range == 0); % Modif JMA le 28/02/2016
if ~isempty(subNan)
    Data.Depth(subNan)               = NaN;
    Data.AcrossDist(subNan)          = NaN;
    Data.AlongDist(subNan)           = NaN;
%   Data.BeamDepressionAngle(subNan) = NaN;
    Data.BeamAzimuthAngle(subNan)    = NaN;
    Data.Range(subNan)               = NaN;
    Data.QualityFactor(subNan)       = NaN;
%   Data.LengthDetection(subNan)     = NaN;
    Data.Reflectivity(subNan)        = NaN;
end

Data.Mask = Data.QualityFactor;
Data.Mask(subNan) = 0;

Mask = uint8(Data.Mask);
C = bitget(Mask, 6);
Mask = bitset(Mask, 6, ~C);

Mask = bitand(Mask, uint8(bin2dec('00111111')));
Mask = bitor( Mask, uint8(bin2dec('11000000')));
Data.Mask = Mask;
Data.MaskOrigin = Mask;

%% Cr�ation de la r�flectivit� par snippets

% [flag, ReflectivityPingBeam200, ReflectivityPingBeam100, ReflectivityPingBeam1] = snippets2PingBeam(this, Data, identSondeur);
[flag, ReflectivityPingBeam100, ReflectivityPingBeam1] = snippets2PingBeam(this, Data, identSondeur, 'DataIndex', DataIndex);
if flag && ~isempty(ReflectivityPingBeam100)
%     Data.ReflectivityFromSnippets    = ReflectivityPingBeam200;
    Data.ReflectivityFromSnippets100 = ReflectivityPingBeam100(:,1:size(Data.Range,2));
    Data.ReflectivityFromSnippets1   = ReflectivityPingBeam1(:,1:size(Data.Range,2));
else
%     Data.ReflectivityFromSnippets  = [];
    Data.ReflectivityFromSnippets100 = [];
    Data.ReflectivityFromSnippets1   = [];
end

% Pr�paration remplacement cl_time par datetime
Data.Datetime = datetime(Data.Time.timeMat(:), 'ConvertFrom', 'datenum');

%% Marquage pour orientation des filtres post�rieurs

Data.TypeDatagram = 7006;

%% Suppression des pings o� aucune valeur de Range n'est fournie

% Car probl�matique pour les fichiers .nc de colonne d'eau fichier "G:\Carla\TestRobotS7K\DirOut\20151112_235255_PP_7150_12kHz.s7k"
% Comment� car on perd la correspondance entre bathy et WC
% Data = cleanData(Data);

%% Sauvegarde de la donn�e dans un fichier .mat

if ~isempty(nomFicMat)
    try
        if useCacheNetcdf == 1 % 1=Mat, 2 et 3=Netcdf
            save(nomFicMat, 'Data')
        else
            [Data.Dimensions.nbPings, Data.Dimensions.nbBeams] = size(Data.Range);
            NetcdfUtils.Struct2Netcdf(Data, ncFileName, 'Depth', 'ncID', this.ncID);
        end
    catch %#ok<CTCH>
        str = sprintf('Erreur �criture fichier %s', nomFicMat);
        my_warndlg(str, 1);
    end
end


%{
function Data = cleanData(Data)

[nbPings, nbBeams] = size(Data.Range);
subRangeEmpty = (sum(isnan(Data.Range),2) == nbBeams);
if sum(subRangeEmpty)
    return
end

fieldNames = fields(Data);
for kf=1:length(fieldNames)
    x = Data.(fieldNames{kf});
    if isvector(x) && (length(x) == nbPings)
        Data.(fieldNames{kf}) = x(~subRangeEmpty);
    else
        [n1,n2] = size(x);
        if n1 == nbPings
            Data.(fieldNames{kf}) = x(~subRangeEmpty,:);
        elseif n2 == nbPings % Au cas o� ?
            my_breakpoint
        end
    end
end
%}