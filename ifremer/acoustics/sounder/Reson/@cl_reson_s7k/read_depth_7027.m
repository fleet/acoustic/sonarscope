% Lecture des datagrams "depth" dans un fichier .s7k
%
% Syntax
%   Data = read_depth_7027(s7k, identSondeur, ...)
% 
% Input Arguments
%   s7k            : Instance de cl_reson_s7k
%   identSondeur : 
%
% Name-only Arguments
%   MasqueDetection : Suppression de valeurs masqu�es par MasqueDetection
%   MasqueDepth     : Suppression de valeurs masqu�es par Masque sur
%                     l'histogramme des Depth
%
% Output Arguments
%   Data : Structure contenant Depth, AcrossDist, AlongDist, AlongDist,
%          BeamAzimuthAngle, Range, QualityFactor, LengthDetection et Reflectivity
%
% Examples
%   nomFic = getNomFicDatabase('RESON7125_20051116_221245.s7k')
%   s7k = cl_reson_s7k('nomFic', nomFic);
%   identSondeur = selectionSondeur(s7k);
%   Data = read_depth_7027(s7k, identSondeur)
%   figure; imagesc(Data.Depth)
%   figure; imagesc(Data.AcrossDist)
%   figure; imagesc(Data.AlongDist)
%   figure; imagesc(Data.BeamDepressionAngle)
%   figure; imagesc(Data.BeamAzimuthAngle)
%   figure; imagesc(Data.Range)
%   figure; imagesc(Data.QualityFactor)
%   figure; imagesc(Data.LengthDetection)
%   figure; imagesc(Data.Reflectivity)
%
%   b = view_depth(s7k);
%   b = editobj(b);
%
% See also cl_reson_s7k cl_reson_s7k/plot_position cl_reson_s7k/plot_nav Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function Data = read_depth_7027(this, identSondeur, varargin)

global useCacheNetcdf %#ok<GVMIS>

[varargin, DataIndex] = getPropertyValue(varargin, 'DataIndex', []); %#ok<ASGLU>

%% Check the number of elements

flag = checkIfOneElement(length(this));
if ~flag
    Data = [];
    return
end

%% R�cup�ration de la fr�quence d'�chantillonnage

Data7000 = read_sonarSettings(this, identSondeur, 'DataIndex', DataIndex);

%% On regarde si le fichier .mat existe

% TESTER LA DATE COMME DANS SIMRAD

[nomDir, nomFic] = fileparts(this.nomFic);
nomFicMat  = fullfile(nomDir, 'SonarScope', [num2str(identSondeur) '_' nomFic '_depth.mat']);
ncFileName = fullfile(nomDir, 'SonarScope', [nomFic '.nc']);

UN = 1; 
if UN && exist(nomFicMat, 'file') && checkFileCreation(nomFicMat, '10/04/2008')
    Data = loadmat(nomFicMat, 'nomVar', 'Data');
    if isfield(Data, 'ReflectivityFromSnippets100')
        return
    end
end
if UN && exist(ncFileName, 'file') && NetcdfUtils.existGrp(ncFileName, 'Depth', 'ncID', this.ncID)
    Data = NetcdfUtils.Netcdf2Struct(ncFileName, 'Depth', 'ncID', this.ncID);
    % Pr�paration remplacement cl_time par datetime
    Data.Datetime = datetime(Data.Time.timeMat, 'ConvertFrom', 'datenum');
    if isfield(Data, 'ReflectivityFromSnippets100')
        return
    end
    return
end

%% On cr�e au besoin le r�pertoire SonarScope

nomDirMat = fullfile(nomDir, 'SonarScope');
if ~exist(nomDirMat, 'dir')
    status = mkdir(nomDirMat);
    if status
        special_message('CreationRepertoireReussie', nomDirMat)
    else
        special_message('CreationRepertoireEchec', nomDirMat)
        nomFicMat = [];
    end
end

%% Lecture du fichier d'index

if isempty(DataIndex)
    [flag, DataIndex] = readFicIndex(this, identSondeur);
    if ~flag
        Data = [];
        return
    end
end

Position         = DataIndex(3,:);
TypeDatagram     = DataIndex(5,:);
PingCounter      = DataIndex(6,:);
PingSequence     = DataIndex(7,:);
DeviceIdentifier = DataIndex(8,:);

[flag, subDataDepth] = get_subDataCommon(7027, TypeDatagram, PingCounter, PingSequence, DeviceIdentifier, identSondeur);
if ~flag
    Data = [];
    return
end

Position7027 = Position(subDataDepth);
% Taille   = Taille(subDataDepth);
% PingSequence = PingSequence(subDataDepth);
% PingCounter  = PingCounter(subDataDepth);


% if max(PingSequence) > 1 % Cas du multiping
%     PCDepth         = PingCounter*4+PingSequence;
%     PCSonarSettings = Data7000.PingCounter*4+Data7000.PingSequence;
% else
%     PCDepth         = PingCounter;
%     PCSonarSettings = Data7000.PingCounter;
% end
% [Index, subDepth, sub7000] = intersect(PCDepth, PCSonarSettings);
% Position7027 = Position7027(subDepth);
% PingSequence = PingSequence(subDepth);
% PingCounter  = PingCounter(subDepth);


 % & (PingCounter == VolatileSonarSettings_PingCounter)
 
%% D�codage du fichier de donn�es

typeIndian = getIndianAll(this.nomFic);
% flagEndian = strcmp(typeIndian, 'b');
[fid, message] = fopen(this.nomFic, 'r', typeIndian);
if fid < 0
    my_warndlg(['cl_reson_s7k:read_depth_7027' message], 0);
    return
end

%% Lecture du 1002 : Coordonn�es du capteur apr�s calibration

subData1002 = (TypeDatagram == 1002);
Position1002 = Position(subData1002);
N = length(Position1002);
DateDatagram1002  = zeros(1,N);
HeureDatagram1002 = zeros(1,N);
for k=1:N
    fseek(fid, Position1002(k), 'bof');
    [flag, DRF] = fread_DRF(fid);
    if ~flag
        break
    end
    DateDatagram1002(k)  = DRF.Date;
    HeureDatagram1002(k) = DRF.Time;
    
    [flag, Data1002] = fread_RecordType_1002(fid); %#ok<ASGLU>
    if ~flag
        break
    end
end

%% Lecture du 7004

subData7004  = (TypeDatagram == 7004);
Position7004 = Position(subData7004);
N            = length(Position7004);
DateDatagram7004             = zeros(1,N);
HeureDatagram7004            = zeros(1,N);
BeamVerticalDirectionAngle   = zeros(N, 1);
BeamHorizontalDirectionAngle = zeros(N, 1);
s_3dBBeamWithY               = zeros(N, 1);
s_3dBBeamWithX               = zeros(N, 1);

for k=1:N
    fseek(fid, Position7004(k), 'bof');
    [flag, DRF] = fread_DRF(fid);
    if ~flag
        N = k-1;
        break
    end
    DateDatagram7004(k)  = DRF.Date;
    HeureDatagram7004(k) = DRF.Time;
    
    [flag, Data7004] = fread_RecordType_7004(fid);
    if ~flag
        N = k-1;
        break
    end
    subc = 1:Data7004.RTH.N;
    BeamVerticalDirectionAngle(k,subc)   = Data7004.RD.BeamVerticalDirectionAngle;
    BeamHorizontalDirectionAngle(k,subc) = Data7004.RD.BeamHorizontalDirectionAngle;
    s_3dBBeamWithY(k,subc)               = Data7004.RD.s_3dBBeamWithY;
    s_3dBBeamWithX(k,subc)               = Data7004.RD.s_3dBBeamWithX;
end
clear TypeDatagram

DateDatagram7004             = DateDatagram7004(1:N);
HeureDatagram7004            = HeureDatagram7004(1:N);
BeamVerticalDirectionAngle   = BeamVerticalDirectionAngle(1:N,:);
BeamHorizontalDirectionAngle = BeamHorizontalDirectionAngle(1:N,:);
s_3dBBeamWithY               = s_3dBBeamWithY(1:N,:);
s_3dBBeamWithX               = s_3dBBeamWithX(1:N,:);

Data7004.Time = cl_time('timeIfr', DateDatagram7004, HeureDatagram7004);
% figure(7890988);
% subplot(4,1,1); plot(BeamVerticalDirectionAngle'); grid on; title('BeamVerticalDirectionAngle');
% subplot(4,1,2); plot(BeamHorizontalDirectionAngle'); grid on; title('BeamHorizontalDirectionAngle');
% subplot(4,1,3); plot(s_3dBBeamWithY'); grid on; title('s_3dBBeamWithX');
% subplot(4,1,4); plot(s_3dBBeamWithX'); grid on; title('s_3dBBeamWithX');
 
%% Lecture des datagrams "VerticalDepth"

% ATTENTION : IL FAUDRAIT S'ASSURER QUE PingCounter EST LE MEME POUR ASSURER
% LE SYNCHONISME
DataVerticalDepth = read_verticalDepth(this, identSondeur, 'DataIndex', DataIndex);

%% Lecture du 7027

N = length(Position7027);
% N = min(N, length(DataVerticalDepth.PingCounter));
MatInit = NaN(1, N, 'single');

DateDatagram  = NaN(1, N);
HeureDatagram = NaN(1, N);

% SampleRate PingSequence
Data.SampleRate = MatInit; % Data7000.SampleRate(sub7000);
% Data.PingSequence = Data7000.PingSequence(sub7000;
Data.ProjectorBeamSteeringAngleVertical = MatInit;% Data7000.ProjectorBeamSteeringAngleVertical(sub7000) * (180/pi);

Data.PingCounter       = MatInit;
Data.MultiPingSequence = MatInit;
Data.Heading           = MatInit;
Data.SoundSpeed        = MatInit;
Data.TransducerDepth   = MatInit;
Data.NbBeams           = MatInit;
% Data.TransducerDepthOffsetMultiplier = MatInit;
Data.Frequency    = MatInit;
Data.HeightSource = MatInit;
Data.Tide         = MatInit;
Data.Roll         = MatInit;
Data.Pitch        = MatInit;
Data.Heave        = MatInit;
Data.VehicleDepth = MatInit;

MatInit = NaN(1, N, 'double');
Data.Latitude  = MatInit;
Data.Longitude = MatInit;

MatInit = NaN(N, 1, 'single');
Data.Depth               = MatInit;
Data.AcrossDist          = MatInit;
Data.AlongDist           = MatInit;
Data.BeamDepressionAngle = MatInit;
Data.BeamAzimuthAngle    = MatInit;
Data.Range               = MatInit;
Data.QualityFactor       = MatInit;
Data.MinFilterInfo       = MatInit;
Data.MaxFilterInfo       = MatInit;

% Data.LengthDetection            = MatInit;
Data.Reflectivity                 = MatInit;
Data.BeamVerticalDirectionAngle   = MatInit;
Data.BeamHorizontalDirectionAngle = MatInit;
Data.s_3dBBeamWithY               = MatInit;
Data.s_3dBBeamWithX               = MatInit;

% if isempty(nomFicMat)
%     [nomDir, nomFic] = fileparts(this.nomFic);
%     str = sprintf('Loading file %s', nomFic);
% else
%     [nomDir, nomFic] = fileparts(this.nomFic);
%     [nomDir, nomFic2] = fileparts(nomFicMat);
%     str = sprintf('Loading file %s\nCreation file %s', nomFic, nomFic2);
% end

flagDejaNotifie = 0;

[~, nomFicSeul] = fileparts(this.nomFic);
strLoop = infoLectureDatagrams(this, nomFicSeul, 7027);
for k=1:N
    infoLoop(strLoop, k, N, 'pings')

    fseek(fid, Position7027(k), 'bof');

    [flag, DRF] = fread_DRF(fid);
    if ~flag
        break
    end
    
%     % BUG Nouveau format ??????????????????
%     DRF.OptionalDataOffset = 1;
%     % BUG Nouveau format ??????????????????
    
    [flag, PingNumber, Sequence, nbFaisceaux, DEPTH] = fread_RecordType_7027(fid, DRF); %#ok<ASGLU>
    if ~flag
        N = k -1;
        break
    end
    
    if ~isfield(DEPTH, 'OD') && ~flagDejaNotifie
%         my_warndlg('OD is missing in this file', 0);
        flagDejaNotifie = 1;
    end
    
    DateDatagram(k)  = DRF.Date;
    HeureDatagram(k) = DRF.Time;

%     DEPTH.RTH
%     DEPTH.OD
    
    Data.SystemSerialNumber   = DRF.DeviceIdentifier;
    Data.EmModel              = DEPTH.RTH.SonarId;
    Data.PingCounter(k)       = DEPTH.RTH.PingCounter;
    Data.MultiPingSequence(k) = DEPTH.RTH.MultiPingSequence;

%     Data.SoundSpeed(k)    = DEPTH.RTH.SoundVelocity;
    Data.TransducerDepth(k) = 0;%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% -6.41; %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    Data.NbBeams(k)         = DEPTH.RTH.N;
    
    if isfield(DEPTH, 'OD') && ~isempty(DEPTH.OD) % Fichier provenant de PDS
        Data.Heading(k)      = DEPTH.OD.Heading;
        Data.Frequency(k)    = DEPTH.OD.Frequency;
        Data.Latitude(k)     = DEPTH.OD.Latitude;
        Data.Longitude(k)    = DEPTH.OD.Longitude;
        Data.HeightSource(k) = DEPTH.OD.HeightSource;
        Data.Tide(k)         = DEPTH.OD.Tide;
        Data.Roll(k)         = DEPTH.OD.Roll;
        Data.Pitch(k)        = DEPTH.OD.Pitch;
        Data.Heave(k)        = DEPTH.OD.Heave;
        Data.VehicleDepth(k) = DEPTH.OD.VehicleDepth;
    else % Fichier provenant du 7K center
%         if ~isempty(DEPTH.RD.MinFilterInfo)&& ~all(DEPTH.RD.MaxFilterInfo == 0)
% %             Data.MinFilterInfo(k,subc) = DEPTH.RD.MinFilterInfo;
% %             Data.MaxFilterInfo(k,subc) = DEPTH.RD.MaxFilterInfo;
%             Data.MinFilterInfo(k) = min(DEPTH.RD.MinFilterInfo);
%             Data.MaxFilterInfo(k) = max(DEPTH.RD.MaxFilterInfo);
%         end
    end
    
%     subc = 1:Data.NbBeams(k);
    subc = DEPTH.RD.BeamDescriptor;
    Data.SampleRate(k) = DEPTH.RTH.SampleRate;
    R = DEPTH.RD.DetectionPoint / DEPTH.RTH.SampleRate;
%     R(abs(R) > 1000)   = NaN;
    Data.Range(k,subc) = R; % Range en m�tres
%     Data.Range(k,subc)         =  % Nombre de samples
    Data.QualityFactor(k,subc) = DEPTH.RD.Quality;
    Data.Reflectivity(k,subc)  = DEPTH.RD.Uncertainty;
    Data.Flags(k,subc)         = DEPTH.RD.Flags;
    
%{
FigUtils.createSScFigure; PlotUtils.createSScPlot(R, '-*'); grid on; title('Range in meters');
FigUtils.createSScFigure; PlotUtils.createSScPlot(DEPTH.RD.BeamDescriptor, '-*'); grid on; title('BeamDescriptor');
FigUtils.createSScFigure; PlotUtils.createSScPlot(DEPTH.RD.DetectionPoint, '-*'); grid on; title('DetectionPoint');
FigUtils.createSScFigure; PlotUtils.createSScPlot(DEPTH.RD.RxAngle, '-*'); grid on; title('RxAngle');
FigUtils.createSScFigure; PlotUtils.createSScPlot(DEPTH.RD.Flags, '-*'); grid on; title('Flags');
FigUtils.createSScFigure; PlotUtils.createSScPlot(DEPTH.RD.Quality, '-*'); grid on; title('Quality');
FigUtils.createSScFigure; PlotUtils.createSScPlot(DEPTH.RD.Uncertainty, '-*'); grid on; title('Uncertainty');
%}
    
    if ~isempty(BeamVerticalDirectionAngle) % Test rajpout� le 05/12/2019 pour donn�es Norbit
        ind7004 = 1;
        Data.BeamVerticalDirectionAngle(k,subc)   = BeamVerticalDirectionAngle(ind7004,subc);
        Data.BeamHorizontalDirectionAngle(k,subc) = BeamHorizontalDirectionAngle(ind7004,subc);
        Data.s_3dBBeamWithY (k,subc)              = s_3dBBeamWithY(ind7004,subc);
        Data.s_3dBBeamWithX(k,subc)               = s_3dBBeamWithX(ind7004,subc);
    end
    
    if k == 1
%         MatInit = NaN(N, Data.NbBeams(k), 'single');
        MatInit = NaN(N, max(DEPTH.RD.BeamDescriptor), 'single'); % Modif JMA le 29/11/2019 pour donn�es Norbit
        Data.Depth               = MatInit;
        Data.AcrossDist          = MatInit;
        Data.AlongDist           = MatInit;
        Data.BeamDepressionAngle = MatInit;
        Data.BeamAzimuthAngle    = MatInit;
    end
    if isfield(DEPTH, 'OD') && ~isempty(DEPTH.OD)
        for k2=1:Data.NbBeams(k)
            n = DEPTH.RD.BeamDescriptor(k2);
            Depth               = DEPTH.OD.BeamDepth(k2);
            AcrossDist          = DEPTH.OD.BeamAcrossTrackDistance(k2);
            AlongDist           = DEPTH.OD.BeamAlongTrackDistance(k2);
            BeamDepressionAngle = DEPTH.OD.PointingAngle(k2);
            BeamAzimuthAngle    = DEPTH.OD.AzimuthAngle(k2);

            Data.Depth(k,n)               = -Depth;
            Data.AcrossDist(k,n)          = AcrossDist;
            Data.AlongDist(k,n)           = AlongDist;
            Data.BeamDepressionAngle(k,n) = BeamDepressionAngle;
            Data.BeamAzimuthAngle(k,n)    = BeamAzimuthAngle;
%           if ~isempty(DataVerticalDepth.AcrossTrackDistance) && (AcrossDist > DataVerticalDepth.AcrossTrackDistance(k))
%           Correction effectu�e par Arnaud � bord du PP? % TODO
            if ~isempty(DataVerticalDepth.AcrossTrackDistance) && (AcrossDist > DataVerticalDepth.AcrossTrackDistance(min(k, length(DataVerticalDepth.AcrossTrackDistance))))
                Data.BeamDepressionAngle(k,n) = BeamDepressionAngle;
                Data.BeamAzimuthAngle(k,n)    = 360 - BeamAzimuthAngle - 270;
            else
                Data.BeamDepressionAngle(k,n) = -BeamDepressionAngle;
                Data.BeamAzimuthAngle(k,n)    = BeamAzimuthAngle - 270;
            end
        end
    else
%         Data.BeamDepressionAngle(k,:) = BeamHorizontalDirectionAngle(k,:);
%         Data.BeamAzimuthAngle(k,:)    = 360 - BeamVerticalDirectionAngle(k,:) - 270;

        %% Rajout� par JMA le 29/11/2019 pour donn�es Norbit
        for k2=1:Data.NbBeams(k)
            n = DEPTH.RD.BeamDescriptor(k2);
            BeamDepressionAngle = DEPTH.RD.RxAngle(k2);
            Data.BeamDepressionAngle(k,n) = BeamDepressionAngle;
        end
        Data.BeamDepressionAngle(k,:) = Data.BeamDepressionAngle(k,:) * (180/pi);
    end
end
fclose(fid);
my_warndlg([strLoop ' : End'], 0);

subl = 1:N;
Data.SampleRate                         = Data.SampleRate(subl);
Data.ProjectorBeamSteeringAngleVertical = Data.ProjectorBeamSteeringAngleVertical(subl);
Data.PingCounter                        = Data.PingCounter(subl);
Data.MultiPingSequence                  = Data.MultiPingSequence(subl);
Data.Heading                            = Data.Heading(subl);
Data.SoundSpeed                         = Data.SoundSpeed(subl);
Data.TransducerDepth                    = Data.TransducerDepth(subl);
Data.NbBeams                            = Data.NbBeams(subl);
% Data.TransducerDepthOffsetMultiplier  = Data.TransducerDepthOffsetMultiplier(subl);
Data.Frequency                          = Data.Frequency(subl);
Data.HeightSource                       = Data.HeightSource(subl);
Data.Tide                               = Data.Tide(subl);
Data.Roll                               = Data.Roll(subl);
Data.Pitch                              = Data.Pitch(subl);
Data.Heave                              = Data.Heave(subl);
Data.VehicleDepth                       = Data.VehicleDepth(subl);
Data.Latitude                           = Data.Latitude(subl);
Data.Longitude                          = Data.Longitude(subl);
Data.Depth                              = Data.Depth(subl,:);
Data.AcrossDist                         = Data.AcrossDist(subl,:);
Data.AlongDist                          = Data.AlongDist(subl,:);
try
    Data.BeamDepressionAngle              = Data.BeamDepressionAngle(subl,:);
    Data.BeamAzimuthAngle                 = Data.BeamAzimuthAngle(subl,:);
catch
end
Data.Range                              = Data.Range(subl,:);
Data.QualityFactor                      = Data.QualityFactor(subl,:);
% if isfield(Data, 'Flags')
    Data.Flags                              = Data.Flags(subl,:);
% end
Data.Reflectivity                       = Data.Reflectivity(subl,:);
Data.BeamVerticalDirectionAngle         = Data.BeamVerticalDirectionAngle(subl,:);
Data.BeamHorizontalDirectionAngle       = Data.BeamHorizontalDirectionAngle(subl,:);
Data.s_3dBBeamWithY                     = Data.s_3dBBeamWithY(subl,:);
Data.s_3dBBeamWithX                     = Data.s_3dBBeamWithX(subl,:);

% Data.TxRollCompensationFlag           = Data7000.TxRollCompensationFlag(sub7000(subl));
% Data.TxRollCompensationValue          = Data7000.TxRollCompensationValue(sub7000(subl));
% Data.TxPitchCompensationFlag          = Data7000.TxPitchCompensationFlag(sub7000(subl));
% Data.TxPitchCompensationValue         = Data7000.TxPitchCompensationValue(sub7000(subl));
% Data.RxRollCompensationFlag           = Data7000.RxRollCompensationFlag(sub7000(subl));
% Data.RxRollCompensationValue          = Data7000.RxRollCompensationValue(sub7000(subl));
% Data.RxPitchCompensationFlag          = Data7000.RxPitchCompensationFlag(sub7000(subl));
% Data.RxPitchCompensationValue         = Data7000.RxPitchCompensationValue(sub7000(subl));
% Data.RxHeaveCompensationFlag          = Data7000.RxHeaveCompensationFlag(sub7000(subl));
% Data.RxHeaveCompensationValue         = Data7000.RxHeaveCompensationValue(sub7000(subl));
try
    Data.MinFilterInfo = Data.MinFilterInfo(subl);
    Data.MaxFilterInfo = Data.MaxFilterInfo(subl);
catch
end

Data.DepthPingCounterBeforeUnwrap = Data.PingCounter;
Data.PingCounter = unwrapPingCounter(Data.PingCounter);

Data.Time = cl_time('timeIfr', DateDatagram(subl), HeureDatagram(subl));

%% Traitement des non-valeurs

% Data.Mask = zeros(size(Data.Depth), 'single');
Data.Mask = zeros(size(Data.Depth), 'single');
% subNan = find((Data.Depth == 0) | (Data.Range == 0));
% subNan = find((Data.Range == 0) | (Data.Range <= 1));
subNan = find(Data.Range == 0);
if ~isempty(subNan)
    Data.Depth(subNan)               = NaN;
    Data.AcrossDist(subNan)          = NaN;
    Data.AlongDist(subNan)           = NaN;
    Data.BeamDepressionAngle(subNan) = NaN;
    Data.BeamAzimuthAngle(subNan)    = NaN;
    Data.Range(subNan)               = NaN;
    Data.QualityFactor(subNan)       = NaN;
    Data.Flags(subNan)               = NaN;
%   Data.LengthDetection(subNan)     = NaN;
    Data.Reflectivity(subNan)        = NaN;
end

Data.Mask = Data.QualityFactor;
Data.Mask(subNan) = 0;
Data.MaskOrigin = Mask;

% Mask = uint8(Data.Mask);
% C = bitget(Mask, 1);
% Mask = bitset(Mask, 6, ~C);
% 
% % Mask = bitand(Mask, uint8(bin2dec('11111')));
% % Mask = bitor(Mask, uint8(bin2dec('11100000')));
% Mask = bitand(Mask, uint8(bin2dec('111111')));
% Mask = bitor(Mask, uint8(bin2dec('11000000')));
% Data.Mask = Mask;

%% Cr�ation de la r�flectivit� par snippets

% [flag, ReflectivityPingBeam] = snippets2PingBeam(this, Data, identSondeur);
% if flag && ~isempty(ReflectivityPingBeam)
%     Data.ReflectivityFromSnippets = ReflectivityPingBeam;
% else
%     Data.ReflectivityFromSnippets = [];
% end

%% Cr�ation de la r�flectivit� par snippets

% [flag, ReflectivityPingBeam200, ReflectivityPingBeam100, ReflectivityPingBeam1] = snippets2PingBeam(this, Data, identSondeur);
[flag, ReflectivityPingBeam100, ReflectivityPingBeam1] = snippets2PingBeam(this, Data, identSondeur, 'DataIndex', DataIndex);
if flag && ~isempty(ReflectivityPingBeam100)
%     Data.ReflectivityFromSnippets    = ReflectivityPingBeam200;
    Data.ReflectivityFromSnippets100 = ReflectivityPingBeam100;
    Data.ReflectivityFromSnippets1   = ReflectivityPingBeam1;
else
%     Data.ReflectivityFromSnippets    = [];
    Data.ReflectivityFromSnippets100 = [];
    Data.ReflectivityFromSnippets1   = [];
end

%% Marquage pour orientation des filtres post�rieurs

Data.TypeDatagram = 7027; % a remplac� le 7006

%% Frequency

%{
T7000 = datetime(Data7000.Time.timeMat, 'ConvertFrom', 'datenum');
T7000.Format = 'yyyy-MM-dd HH:mm:ss.SSSSS';
T7000(1:50)'
Data7000.Frequency(1:50)'

TDepth = datetime(Data.Time.timeMat, 'ConvertFrom', 'datenum');
TDepth.Format = 'yyyy-MM-dd HH:mm:ss.SSSSS';
TDepth(1:50)'
Frequency = my_interp1(T7000, Data7000.Frequency, TDepth, 'nearest');
Frequency(1:50)'
figure; plot(T7000, Data7000.Frequency, '+k'); grid on; hold on; plot(TDepth, Frequency, 'xr')
%}
Data.Frequency = my_interp1(Data7000.Time, Data7000.Frequency, Data.Time, 'nearest');

% % Pour comparaison avec la fr�quence calcul�e dans
% % create_signalsFromSonarSettings. C'est my_interp1 qui est la plus
% % fiable pour la fr�quence
% pppp = my_interp1_Extrap_PreviousThenNext(Data7000.Time, Data7000.Frequency, Data.Time); % Modif JMA le 08/01/2021 pour cache S7K en Netcdf


%% Sauvegarde de la donn�e dans un fichier .mat

if ~isempty(nomFicMat)
    try
        if useCacheNetcdf == 1 % 1=Mat, 2 et 3=Netcdf
            save(nomFicMat, 'Data')
        else
            [Data.Dimensions.nbPings, Data.Dimensions.nbBeams] = size(Data.Range);
            NetcdfUtils.Struct2Netcdf(Data, ncFileName, 'Depth');
        end
    catch ME  %#ok<NASGU>
        str = sprintf('Erreur ecriture fichier %s', ncFileName);
        my_warndlg(str, 1);
    end
end
