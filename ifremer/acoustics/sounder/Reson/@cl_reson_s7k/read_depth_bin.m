function [flag, Data] = read_depth_bin(s7k, varargin)

[varargin, flagMemmapfile] = getPropertyValue(varargin, 'Memmapfile', 0); %#ok<ASGLU>

flag = 0;
Data = [];

%% Lecture du r�pertoire SonarScope

[nomDir, nom] = fileparts(s7k.nomFic);
nomDir = fullfile(nomDir, 'SonarScope', nom);
if ~exist(nomDir, 'dir')
    return
end

%% Lecture du fichier XML d�crivant la donn�e

nomFicXml = fullfile(nomDir, 'Depth.xml');
flag = exist(nomFicXml, 'file');
if ~flag
    return
end
Data = xml_read(nomFicXml);

%% Lecture du r�pertoire Depth

nomDirDepth = fullfile(nomDir, 'Depth');
if ~exist(nomDirDepth, 'dir')
    flag = 0;
    return
end

%% Lecture des fichiers binaires des signaux

for k=1:length(Data.Signals)
    [flag, Signal] = readSignal(nomDir, Data.Signals(k), Data.nbPings, 'Memmapfile', flagMemmapfile);
    if ~flag
        return
    end
    Data.(Data.Signals(k).Name) = Signal;
end

%% Lecture des fichiers binaires des signaux pointes

nomFicXml = fullfile(nomDir, [Data.SignalsPointer(1).Storage '.xml']);
flag = exist(nomFicXml, 'file');
if ~flag
    flag = 0;
    return
end
DataCommun = xml_read(nomFicXml);

for k=1:length(Data.SignalsPointer)
    [flag, Signal] = readSignalPointe(nomDir, Data.Signals, DataCommun, Data.SignalsPointer(k), Data.nbPings, 'Memmapfile', flagMemmapfile);
    if ~flag
        return
    end
    Data.(Data.SignalsPointer(k).Name) = Signal;
end

%% Lecture des fichiers binaires des images

for k=1:length(Data.Images)
    [flag, Image] = readImage(nomDir, Data.Images(k), Data.nbPings, Data.nbBeams, 'Memmapfile', flagMemmapfile);
    if ~flag
        return
    end
    Data.(Data.Images(k).Name) = Image;
end

% Data = rmfield(Data, {'Signals'; 'Images'; 'Comments'});
