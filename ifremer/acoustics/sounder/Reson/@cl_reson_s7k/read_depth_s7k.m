function [flag, DataDepth, identSondeur, flagFormat7006, flagFormat7027, nomFicMat, ncFileName] = read_depth_s7k(this, varargin)

[varargin, DataIndex]    = getPropertyValue(varargin, 'DataIndex',    []);
[varargin, identSondeur] = getPropertyValue(varargin, 'identSondeur', []); %#ok<ASGLU>

flag           = 0;
DataDepth      = [];
flagFormat7006 = [];
flagFormat7027 = [];
nomFicMat      = [];
ncFileName     = [];

if isempty(identSondeur)
    identSondeur = selectionSondeur(this);
end
if isempty(DataIndex)
    [flag, DataIndex] = readFicIndex(this, identSondeur);
    if ~flag
        return
    end
end

[nomDir, nomFic] = fileparts(this.nomFic);
nomFicMat  = fullfile(nomDir, 'SonarScope', [num2str(identSondeur) '_' nomFic '_depth.mat']);
ncFileName = fullfile(nomDir, 'SonarScope', [nomFic '.nc']);

[flagFormat7006, flagFormat7027] = identFormat7006_7027(this, identSondeur, 'DataIndex', DataIndex);
if flagFormat7027
    DataDepth = read_depth_7027(this, identSondeur, 'DataIndex', DataIndex);
    if isempty(DataDepth) || isempty(DataDepth.PingCounter)
        str1 = sprintf('Il n''y a pas de datagramme "7027" dans le fichier %s.s7k : on arr�te sa lecture.', NomImage);
        str2 = sprintf('There is no datagram "7027" in file %s.s7k : we stop the import.', NomImage);
        renameBadFile(this.nomFic, 'Message', Lang(str1,str2)) % on renomme le fichier en .bad
        return
    end
elseif flagFormat7006
    DataDepth = read_depth_7006(this, identSondeur, 'DataIndex', DataIndex);
    if isempty(DataDepth) || isempty(DataDepth.PingCounter)
        str1 = sprintf('Il n''y a pas de datagramme "7006" dans le fichier %s.s7k : on arr�te sa lecture.', NomImage);
        str2 = sprintf('There is no datagram "7006" in file %s.s7k : we stop the import.', NomImage);
        renameBadFile(this.nomFic, 'Message', Lang(str1,str2)) % on renomme le fichier en .bad
        return
    end
else
    str1 = sprintf('Le fichier "%s" ne contient pas de bathym�trie.', this.nomFic);
    str2 = sprintf('File "%s" does not contain any depth datagram.', this.nomFic);
    renameBadFile(this.nomFic, 'Message', Lang(str1,str2)) % on renomme le fichier en .bad
    return
end
nbPings = length(DataDepth.PingCounter);
if nbPings < 2
    str1 = sprintf('Le fichier "%s" ne contient pas suffisamment de pings.', this.nomFic);
    str2 = sprintf('File "%s" does not contain enough depth datagram.', this.nomFic);
    renameBadFile(this.nomFic, 'Message', Lang(str1,str2)) % on renomme le fichier en .bad
    return
end

if isempty(DataDepth) || isempty(DataDepth.PingCounter)
    return
end

%% Pr�paration remplacement cl_time par datetime

DataDepth.Datetime = datetime(DataDepth.Time.timeMat', 'ConvertFrom', 'datenum');

flag = 1;
