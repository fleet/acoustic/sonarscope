function Data = read_navigation(this, identSondeur, varargin)

[varargin, DataIndex] = getPropertyValue(varargin, 'DataIndex', []);

Data = read_navigation_1015(this, identSondeur, 'DataIndex', DataIndex, varargin{:});
if isempty(Data) || isempty(Data.Latitude)
    Data = read_navigation_1003(this, identSondeur, 'DataIndex', DataIndex);
    if ~isempty(Data) && ~isempty(Data.Latitude)
        str1 = sprintf('C''est l''attitude lue dans les datagrammes 1003 au lieu de 1015 qui a �t� prise pour le fichier\n"%s"', this.nomFic);
        str2 = sprintf('Datagrammes 1003 are used instead of 1015 in file \n"%s"', this.nomFic);
        my_warndlg(Lang(str1,str2), 0, 'Tag', 'LectureAttitude1012OK', 'TimeDelay', 60);
    end
end


function Data = read_navigation_1015(this, identSondeur, varargin)

global useCacheNetcdf %#ok<GVMIS>

[varargin, DataIndex] = getPropertyValue(varargin, 'DataIndex', []); %#ok<ASGLU>

%% Check the number of elements

Data = [];
flag = checkIfOneElement(length(this));
if ~flag
    return
end

%% On regarde si le fichier .mat existe

[nomDir, nomFic] = fileparts(this.nomFic);
nomFicMat  = fullfile(nomDir, 'SonarScope', [nomFic '_navigation.mat']);
ncFileName = fullfile(nomDir, 'SonarScope', [nomFic '.nc']);

UN = 1;
if UN && exist(nomFicMat, 'file') && checkFileCreation(nomFicMat, '27/01/2009')
    Data = loadmat(nomFicMat, 'nomVar', 'Data');
    % Pr�paration remplacement cl_time par datetime
    Data.Datetime = datetime(Data.Time.timeMat, 'ConvertFrom', 'datenum');
    return
end
if UN && exist(ncFileName, 'file') && NetcdfUtils.existGrp(ncFileName, 'Navigation', 'ncID', this.ncID)
    Data = NetcdfUtils.Netcdf2Struct(ncFileName, 'Navigation', 'ncID', this.ncID);
    return
end

%% On cr�e au besoin le r�pertoire SonarScope

nomDirMat = fullfile(nomDir, 'SonarScope');
if ~exist(nomDirMat, 'dir')
    status = mkdir(nomDirMat);
    if status
%         disp(sprintf('Le repertoire %s a ete cree avec succes', nomDirMat))
    else
        fprintf('Impossible de c�er le r�pertoire %s.\nJe vous conseille de rendre cela possible, �a permet de formater la donn�e en fichiers matlab,\nce qui permettra d''acc�der beaucoup plus vite � la donn�e?\n', nomDirMat);
        nomFicMat = [];
    end
end

%% Lecture du fichier d'index

if isempty(DataIndex)
    [flag, DataIndex] = readFicIndex(this, identSondeur);
    if ~flag
        Data = [];
        return
    end
end

Position     = DataIndex(3,:);
TypeDatagram = DataIndex(5,:);

sub = (TypeDatagram == 1015);
Position = Position(sub);
clear TypeDatagram

%% D�codage du fichier de donnees

typeIndian = 'l';
[fid, message] = fopen(this.nomFic, 'r', typeIndian);
if fid < 0
    disp(message);
    Data = [];
    return
end

N = length(Position);

MatInit = zeros(1, N, 'double');
Data.Latitude  = MatInit;
Data.Longitude = MatInit;

MatInit = zeros(1, N, 'single');
Data.Speed                      = MatInit;
Data.CourseVessel               = MatInit;
Data.Heading                    = MatInit;
Data.VesselHeight               = MatInit;
Data.HeightAccuracy             = MatInit;
Data.HorizontalPositionAccuracy = MatInit;
Data.Datetime                   = NaT(1, N);

DateDatagram  = zeros(1, N);
HeureDatagram = zeros(1, N);

[~, nomFicSeul] = fileparts(this.nomFic);
strLoop = infoLectureDatagrams(this, nomFicSeul, 1015);
for k=1:N
    infoLoop(strLoop, k, N, 'records')
    
    fseek(fid, Position(k), 'bof');
    
    [flag, DRF] = fread_DRF(fid);
    if ~flag
        break
    end
    [flag, NAV] = fread_RecordType_1015(fid);
    if ~flag
        break
    end
    
    DateDatagram(k)  = DRF.Date;
    HeureDatagram(k) = DRF.Time;

%     Data.PingCounter(k)      = ;
%     Data.SystemSerialNumber  = SystemSerialNumber;
    Data.SystemSerialNumber  = DRF.DeviceIdentifier;
    Data.Latitude(k)         = NAV.RTH.Latitude;     % en deg
    Data.Longitude(k)        = NAV.RTH.Longitude;    % en deg
%     Data.FixPosition(k)      = ;    % en m
    Data.Speed(k)            = NAV.RTH.SpeedOverGround;    % en m/s
    Data.CourseVessel(k)     = NAV.RTH.CourseOverGround;    % en deg
    Data.Heading(k)          = NAV.RTH.Heading;    % en deg
    
    Data.HorizontalPositionAccuracy(k) = NAV.RTH.HorizontalPositionAccuracy;  % en m
    Data.VesselHeight(k)               = NAV.RTH.VesselHeight;   % en m
    Data.HeightAccuracy(k)             = NAV.RTH.HeightAccuracy; % en m
    Data.VerticalReference             = NAV.RTH.VerticalReference;
end
fclose(fid);
my_warndlg([strLoop ' : End'], 0);

%% Sauvegarde de la donn�e dans un fichier .mat

if N
    Data.Time     = cl_time('timeIfr', DateDatagram, HeureDatagram);
    Data.Datetime = datetime(Data.Time.timeMat', 'ConvertFrom', 'datenum');

    if ~isempty(nomFicMat)
        try
            if useCacheNetcdf == 1 % 1=Mat, 2 et 3=Netcdf
                save(nomFicMat, 'Data')
            else
                Data.Dimensions.nbPoints = length(Data.Latitude);
                NetcdfUtils.Struct2Netcdf(Data, ncFileName, 'Navigation', 'ncID', this.ncID);
            end
        catch %#ok<CTCH>
            messageErreurFichier(nomFicMat, 'WriteFailure');
        end
    end
end


function Data = read_navigation_1003(this, identSondeur, varargin)

global useCacheNetcdf %#ok<GVMIS>

[varargin, DataIndex] = getPropertyValue(varargin, 'DataIndex', []); %#ok<ASGLU>

%% Check the number of elements

Data = [];
flag = checkIfOneElement(length(this));
if ~flag
    return
end

%% On regarde si le fichier .mat existe

[nomDir, nomFic] = fileparts(this.nomFic);
nomFicMat  = fullfile(nomDir, 'SonarScope', [nomFic '_navigation.mat']);
ncFileName = fullfile(nomDir, 'SonarScope', [nomFic '.nc']);

UN = 1;
if UN && exist(nomFicMat, 'file') && checkFileCreation(nomFicMat, '27/01/2009')
    Data = loadmat(nomFicMat, 'nomVar', 'Data');
    return
end
if UN && exist(ncFileName, 'file') && NetcdfUtils.existGrp(ncFileName, 'Navigation', 'ncID', this.ncID)
    Data = NetcdfUtils.Netcdf2Struct(ncFileName, 'Navigation', 'ncID', this.ncID);
    return
end

%% On cr�e au besoin le repertoire SonarScope

nomDirMat = fullfile(nomDir, 'SonarScope');
if ~exist(nomDirMat, 'dir')
    status = mkdir(nomDirMat);
    if ~status
        fprintf('Impossible de c�er le r�pertoire %s.\nJe vous conseille de rendre cela possible, �a permet de formater la donn�e en fichiers matlab,\nce qui permettra d''acc�der beaucoup plus vite � la donn�e?\n', nomDirMat);
        nomFicMat = [];
    end
end

%% Lecture du fichier d'index

if isempty(DataIndex)
    [flag, DataIndex] = readFicIndex(this, identSondeur);
    if ~flag
        Data = [];
        return
    end
end

Position     = DataIndex(3,:);
TypeDatagram = DataIndex(5,:);

sub = (TypeDatagram == 1003);
Position = Position(sub);
clear TypeDatagram

%% D�codage du fichier de donnees

typeIndian = 'l';
[fid, message] = fopen(this.nomFic, 'r', typeIndian);
if fid < 0
    disp(message);
    Data = [];
    return
end

N = length(Position);

MatInit = zeros(1, N, 'double');
Data.Latitude  = MatInit;
Data.Longitude = MatInit;

MatInit = zeros(1, N, 'single');
Data.Speed                      = MatInit;
Data.CourseVessel               = MatInit;
Data.Heading                    = NaN(1, N, 'double');
Data.VesselHeight               = MatInit;
Data.HeightAccuracy             = MatInit;
Data.HorizontalPositionAccuracy = MatInit;
Data.Datetime                   = NaT(1, N);

DateDatagram  = zeros(1, N);
HeureDatagram = zeros(1, N);

if isempty(nomFicMat)
    [nomDir, nomFic] = fileparts(this.nomFic); %#ok
    str = sprintf('Loading file %s', nomFic);
else
    [nomDir, nomFic] = fileparts(this.nomFic); %#ok
    [nomDir, nomFic2] = fileparts(nomFicMat); %#ok
    str = sprintf('Loading file %s\nCreation file %s', nomFic, nomFic2);
end

hw = create_waitbar(str, 'N', N);
for k=1:N
	my_waitbar(k, N, hw);
    
    fseek(fid, Position(k), 'bof');
    
    [flag, DRF] = fread_DRF(fid);
    if ~flag
        break
    end
    [flag, NAV] = fread_RecordType_1003(fid);
    if ~flag
        break
    end
    
    DateDatagram(k)  = DRF.Date;
    HeureDatagram(k) = DRF.Time;

%     Data.PingCounter(k)      = ;
%     Data.SystemSerialNumber  = SystemSerialNumber;
    Data.SystemSerialNumber  = DRF.DeviceIdentifier;
    Data.Latitude(k)         = NAV.RTH.Latitude;     % en deg
    Data.Longitude(k)        = NAV.RTH.Longitude;    % en deg
%     Data.FixPosition(k)      = ;    % en m
%     Data.Speed(k)            = NAV.RTH.SpeedOverGround;    % en m/s
%     Data.CourseVessel(k)     = NAV.RTH.CourseOverGround;    % en deg
%     Data.Heading(k)          = NAV.RTH.Heading;    % en deg
    
%     Data.HorizontalPositionAccuracy(k)  = NAV.RTH.HorizontalPositionAccuracy;  % en m
%     Data.VesselHeight(k)                = NAV.RTH.VesselHeight;   % en m
%     Data.HeightAccuracy(k)              = NAV.RTH.HeightAccuracy; % en m
%     Data.VerticalReference              = NAV.RTH.VerticalReference;
end
my_close(hw)
fclose(fid);

%% Sauvegarde de la donn�e dans un fichier .mat

if N
    Data.Time = cl_time('timeIfr', DateDatagram, HeureDatagram);
    % Pr�paration remplacement cl_time par datetimr
    Data.Datetime = datetime(Data.Time.timeMat, 'ConvertFrom', 'datenum');

    if ~isempty(nomFicMat)
        try
            if useCacheNetcdf == 1 % 1=Mat, 2 et 3=Netcdf
                save(nomFicMat, 'Data')
            else
                Data.Dimensions.nbPoints = length(Data.Latitude);
                NetcdfUtils.Struct2Netcdf(Data, ncFileName, 'Navigation', 'ncID', this.ncID);
            end
        catch %#ok<CTCH>
            messageErreurFichier(nomFicMat, 'WriteFailure');
        end
    end
end
