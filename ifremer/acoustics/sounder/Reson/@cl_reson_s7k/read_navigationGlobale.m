function [flag, TimeNav, LatNav, LonNav, HeadingNav, ...
    TimeDepth, Latitude, Longitude, Heading] = read_navigationGlobale(this, varargin)

[varargin, Navigation]   = getPropertyValue(varargin, 'Navigation',   []);
[varargin, nomFicNav]    = getPropertyValue(varargin, 'NavFileName',  []);
[varargin, DataDepth]    = getPropertyValue(varargin, 'DataDepth',    []);
[varargin, DataPosition] = getPropertyValue(varargin, 'DataPosition', []);
[varargin, DataIndex]    = getPropertyValue(varargin, 'DataIndex',    []);
[varargin, identSondeur] = getPropertyValue(varargin, 'identSondeur', []);

% Pour rattraper une maladresse
if isempty(nomFicNav)
    [varargin, nomFicNav] = getPropertyValue(varargin, 'nomFicNav', []); %#ok<ASGLU>
end

TimeNav    = [];
LatNav     = [];
LonNav     = [];
HeadingNav = [];
TimeDepth  = [];
Latitude   = [];
Longitude  = [];
Heading    = [];

if isempty(DataDepth)
    [flag, DataDepth] = read_depth_s7k(this, 'DataIndex', DataIndex, 'identSondeur', identSondeur);
    if ~flag
        return
    end
end

TimeDepth = [];
if all(isnan(DataDepth.Roll)) % Fichier non complété par PDS
    if isempty(DataPosition)
        DataPosition = read_navigation(this, identSondeur, 'DataIndex', DataIndex);
    end
    %{
figure; plot(DataPosition.Longitude, DataPosition.Latitude, 'k');
hold on; plot(DataDepth.Longitude, DataDepth.Latitude, 'r'); title('Navigation'); grid on;
legend({'Datagram 7006 (bathy)'; 'Datagram 1015 (navigation)'});
    %}
    
    if isempty(DataPosition) || isempty(DataPosition.Latitude) || all(isnan(DataPosition.Latitude))
        %         Longitude = NaN(nbPings,1);
        %         Latitude  = NaN(nbPings,1);
        %         Heading   = NaN(nbPings,1);
        
        % Début insertion code pour WC de 7KCenter
        if isempty(Navigation)
            if ~isempty(nomFicNav)
                if iscell(nomFicNav)
                    if length(nomFicNav) > 1
                        str1 = 'Il n''est pas encore prévu de pouvoir utiliser plusieurs fichiers de nav en même temps, désolé. Si vous avez vraiment besoin de cela faites moi en la demande, signé JMA.';
                        str2 = 'It is not possible to use several navigation files for the moment. If you really need it, please tell me, signed JMA.';
                        my_warndlg(Lang(str1,str2), 1);
                        flag = 0;
                        return
                    end
                    nomFicNav = nomFicNav{1};
                end
                [flag, LatNav, LonNav, TimeNav, HeadingNav] = lecFicNav(nomFicNav);
                if ~flag
                    return
                end
                
                % figure; plot(LonNav, LatNav); grid on
            end
        else
            LatNav     = Navigation.LatNav;
            LonNav     = Navigation.LonNav;
            TimeNav    = Navigation.TimeNav;
            HeadingNav = Navigation.HeadingNav;
%             ImmersionNav = Navigation.ImmersionNav;
        end
        % Fin insertion code pour WC de 7KCenter
        
    else % Cas où la navigation est présente dans le fichier
        Longitude = my_interp1_longitude(DataPosition.Time, DataPosition.Longitude, DataDepth.Time, 'linear', 'extrap');
        Latitude  = my_interp1(DataPosition.Time, DataPosition.Latitude,  DataDepth.Time, 'linear', 'extrap');
        Heading   = my_interp1_headingDeg(DataPosition.Time, DataPosition.Heading,   DataDepth.Time, 'linear', 'extrap');
        
        if all(isnan(Heading))
            Heading = calCapFromLatLon(Latitude, Longitude);
        end
        
        %                          Speed: [1x774 single]
        %                   CourseVessel: [1x774 single]
        %                   VesselHeight: [1x774 single]
        % Bidouille +++ : il est temps de tout reécrire !!!
        LatNav     = Latitude;
        LonNav     = Longitude;
        TimeNav    = DataDepth.Time;
        HeadingNav = Heading;
    end
else % Cas où les données ont été complétées par PDS (la navigation est présente dans les depth datagrams)
    Latitude  = DataDepth.Latitude;
    Longitude = DataDepth.Longitude;
    Heading   = DataDepth.Heading;
    TimeDepth = DataDepth.Time;
    
    % Bidouille +++ : il est temps de tout reécrire !!!
    LatNav     = Latitude;
    LonNav     = Longitude;
    TimeNav    = TimeDepth;
    HeadingNav = Heading;
end

if isempty(Latitude) %#ok<IFBDUP>
    flag = 1; % 0; % Laissé à 1 intentionnellement
else
    flag = 1;
end
