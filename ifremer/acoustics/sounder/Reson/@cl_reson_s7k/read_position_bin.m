function [flag, Data] = read_position_bin(a)

flag = 0;
Data = [];

%% Lecture du r�pertoire SonarScope

[nomDir, nom] = fileparts(a.nomFic);
nomDir = fullfile(nomDir, 'SonarScope', nom);
if ~exist(nomDir, 'dir')
    return
end

%% Lecture du fichier XML d�crivant la donn�e 

nomFicXml = fullfile(nomDir, 'Position.xml');
flag = exist(nomFicXml, 'file');
if ~flag
    return
end
Data = xml_read(nomFicXml);

%% Lecture du r�pertoire Position

nomDirPosition = fullfile(nomDir, 'Position');
if ~exist(nomDirPosition, 'dir')
    flag = 0;
    return
end

%% Lecture des fichiers binaires des signaux

for k=1:length(Data.Signals)
    [flag, Signal] = readSignal(nomDir, Data.Signals(k), Data.NbSamples);
    if ~flag
        return
    end
    Data.(Data.Signals(k).Name) = Signal;
end
% Data = rmfield(Data, {'Signals'; 'Comments'});

%% Pr�paration remplacement cl_time par datetimr

Data.Datetime = datetime(Data.Time.timeMat', 'ConvertFrom', 'datenum');
