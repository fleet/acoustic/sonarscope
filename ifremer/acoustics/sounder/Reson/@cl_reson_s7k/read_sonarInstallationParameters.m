function Data = read_sonarInstallationParameters(this, identSondeur, varargin)

global useCacheNetcdf %#ok<GVMIS>

[varargin, Mute]      = getPropertyValue(varargin, 'Mute',      0);
[varargin, DataIndex] = getPropertyValue(varargin, 'DataIndex', []); %#ok<ASGLU>

%% Check the number of elements

Data = [];

flag = checkIfOneElement(length(this));
if ~flag
    return
end

%% Cas des sondeurs Norbit

% Ajout JMA le 10/05/2021
switch identSondeur
    case {13003; 13016}
        Data = 'None';
        return
end

%% On regarde si le fichier .mat existe identSondeur

[nomDir, nomFic] = fileparts(this.nomFic);
nomFicMat  = fullfile(nomDir, 'SonarScope', [num2str(identSondeur) '_' nomFic '_sonarInstallationParameters.mat']);
ncFileName = fullfile(nomDir, 'SonarScope', [nomFic '.nc']);

UN = 1;
if UN && exist(nomFicMat, 'file') && checkFileCreation(nomFicMat, '02/10/2008')
    Data = loadmat(nomFicMat, 'nomVar', 'Data');
    return
end
if UN && exist(ncFileName, 'file') && NetcdfUtils.existGrp(ncFileName, 'SonarInstallationParameters', 'ncID', this.ncID)
    Data = NetcdfUtils.Netcdf2Struct(ncFileName, 'SonarInstallationParameters', 'ncID', this.ncID);
    return
end

%% On cr�e au besoin le r�pertoire SonarScope

nomDirMat = fullfile(nomDir, 'SonarScope');
if ~exist(nomDirMat, 'dir')
    [status, msg] = mkdir(nomDirMat); %#ok
    if status
        %         disp(sprintf('Le repertoire %s a ete cree avec succes', nomDirMat))
    else
        fprintf('Impossible de c�er le r�pertoire %s.\nJe vous conseille de rendre cela possible, �a permet de formater la donn�e en fichiers matlab,\nce qui permettra d''acceder beaucoup plus vite � la donn�e.\n', nomDirMat)
        nomFicMat = [];
    end
end

% -------------------------------------------------------------
% On recup�re le num�ro de s�rie car pas donn� dans le datagram
% contrairement � Simrad

% SystemSerialNumber = get_SonarSerialNumber(this, identSondeur);

%% Lecture du fichier d'index

if isempty(DataIndex)
    [flag, DataIndex] = readFicIndex(this, identSondeur);
    if ~flag
        return
    end
end

Position     = DataIndex(3,:);
TypeDatagram = DataIndex(5,:);

sub = (TypeDatagram == 7030);
Position = Position(sub);
clear TypeDatagram

%% D�codage du fichier de donn�es

typeIndian = 'l';
[fid, message] = fopen(this.nomFic, 'r', typeIndian);
if fid < 0
    disp(message);
    Data = [];
    return
end

N = length(Position);

if (N == 0) && ~Mute
    nomFicXML  = fullfile(nomDir, 'InstallationParameters.xml');
    if exist(nomFicXML, 'file') && checkFileCreation(nomFicXML, '02/10/2008')
        Data = xml_read(nomFicXML);
        return
    else
        [~, nomFicSeul] = fileparts(this.nomFic);
        str = sprintf('S7K cache makeup : %s : Group Depth (catching datagrams 7030) : No such datagram (sonarInstallationParameters)', nomFicSeul);
        my_warndlg(str, 0, 'Tag', 'No7030Datagram');
    end
end

DateDatagram  = [];
HeureDatagram = [];
k2 = 0;
for k1=1:N
    fseek(fid, Position(k1), 'bof');
    
    [flag, DRF] = fread_DRF(fid);
    if ~flag
        break
    end
    
    if identSondeur == DRF.DeviceIdentifier
        [flag, PAR] = fread_RecordType_7030(fid);
        if ~flag
            break
        end
        k2 = k2 + 1;
        DateDatagram(k2)  = DRF.Date; %#ok<AGROW>
        HeureDatagram(k2) = DRF.Time; %#ok<AGROW>
        
        FieldNames = fieldnames(PAR.RTH);
        for k3=1:length(FieldNames)
            Data.(FieldNames{k3})(k2,:) = PAR.RTH.(FieldNames{k3});
        end
        %     Data.DRF(k1).SystemSerialNumber = DeviceIdentifier; % SystemSerialNumber;
    end
end
fclose(fid);

if (N == 0) && (identSondeur == 7150)
    Data7000 = read_sonarSettings(this, identSondeur);
    
    if Data7000.Frequency(1) < 18
        nomFicMatSOS = getNomFicDatabase('PP_7150_12kHz_sonarInstallationParameters.mat');
    else
        nomFicMatSOS = getNomFicDatabase('PP_7150_24kHz_sonarInstallationParameters.mat');
    end
    Data = loadmat(nomFicMatSOS, 'nomVar', 'Data');
    
    if ~isempty(nomFicMat)
        try
            if useCacheNetcdf == 1 % 1=Mat, 2 et 3=Netcdf
                save(nomFicMat, 'Data')
            else
                NetcdfUtils.Struct2Netcdf(Data, ncFileName, 'SonarInstallationParameters', 'ncID', this.ncID);
            end
        catch
            str = sprintf('Erreur ecriture fichier %s', nomFicMat);
            my_warndlg(str, 1);
        end
    end
end

% Sauvegarde de la donn�e dans un fichier .mat
if N > 0
    Data.Time = cl_time('timeIfr', DateDatagram, HeureDatagram);
    
    if ~isempty(nomFicMat)
        try
            if useCacheNetcdf == 1 % 1=Mat, 2 et 3=Netcdf
                save(nomFicMat, 'Data')
            else
                NetcdfUtils.Struct2Netcdf(Data, ncFileName, 'SonarInstallationParameters', 'ncID', this.ncID);
            end
        catch
            str = sprintf('Erreur ecriture fichier %s', nomFicMat);
            my_warndlg(str, 1);
        end
    end
end
