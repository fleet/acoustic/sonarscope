function Data = read_sonarSettings(this, identSondeur, varargin)

global useCacheNetcdf %#ok<GVMIS>

[varargin, DataIndex] = getPropertyValue(varargin, 'DataIndex', []); %#ok<ASGLU>

%% Check the number of elements

flag = checkIfOneElement(length(this));
if ~flag
    Data = [];
    return
end

%% On regarde si le fichier .mat existe

[nomDir, nomFic] = fileparts(this.nomFic);
nomFicMat  = fullfile(nomDir, 'SonarScope', [num2str(identSondeur) '_' nomFic '_SonarSettings.mat']);
ncFileName = fullfile(nomDir, 'SonarScope', [nomFic '.nc']);

UN = 1;
if UN && exist(nomFicMat, 'file') && checkFileCreation(nomFicMat, '10/04/2008')
    Data = loadmat(nomFicMat, 'nomVar', 'Data');
    return
end
if UN && exist(ncFileName, 'file') && NetcdfUtils.existGrp(ncFileName, 'SonarSettings', 'ncID', this.ncID)
    Data = NetcdfUtils.Netcdf2Struct(ncFileName, 'SonarSettings', 'ncID', this.ncID);
    return
end

%% On cr�e au besoin le r�pertoire SonarScope

nomDirMat = fullfile(nomDir, 'SonarScope');
if ~exist(nomDirMat, 'dir')
    status = mkdir(nomDirMat);
    if status
% %         disp(sprintf('Le repertoire %s a ete cree avec succes', nomDirMat))
    else
        fprintf('Impossible de c�er le r�pertoire %s.\nJe vous conseille de rendre cela possible, �a permet de formater la donnee en fichiers matlab,\nce qui permettra d''acceder beaucoup plus vite a la donnee\n', nomDirMat)
        nomFicMat = [];
    end
end

%% Lecture du fichier d'index

if isempty(DataIndex)
    [flag, DataIndex] = readFicIndex(this, identSondeur);
    if ~flag
        Data = [];
        return
    end
end

Position         = DataIndex(3,:);
TypeDatagram     = DataIndex(5,:);
PingCounter      = DataIndex(6,:);
PingSequence     = DataIndex(7,:);
DeviceIdentifier = DataIndex(8,:);

[flag, sub] = get_subDataCommon(7000, TypeDatagram, PingCounter, PingSequence, DeviceIdentifier, identSondeur);
if ~flag
    return
end

Position = Position(sub);
PingCounter = PingCounter(sub);
clear TypeDatagram

%% D�codage du fichier de donn�es

typeIndian = 'l';
[fid, message] = fopen(this.nomFic, 'r', typeIndian);
if fid < 0
    disp(message);
    Data = [];
    return
end

N = length(Position);
MatInit = zeros(1, N);

Data.Frequency                              = MatInit;
Data.PingSequence                           = MatInit;
Data.PingCounter                            = MatInit;
Data.SampleRate                             = MatInit;
Data.ReceiverBandwith                       = MatInit;
Data.TxPulseWidth                           = MatInit;
Data.TxPulseTypeIdentifier                  = MatInit;
Data.TxPulseEnvelopeParameter               = MatInit;
Data.TxPulseReserved                        = MatInit;
Data.MaxPingRate                            = MatInit;
Data.PingPeriod                             = MatInit;
Data.RangeSelection                         = MatInit;
Data.PowerSelection                         = MatInit;
Data.GainSelection                          = MatInit;
Data.ControlFlags                           = MatInit;
Data.ProjectorMagicNumber                   = MatInit;
Data.ProjectorBeamSteeringAngleVertical     = MatInit;
Data.ProjectorBeamSteeringAngleHorizontal	= MatInit;
Data.ProjectionBeam_3dB_WidthVertical       = MatInit;
Data.ProjectionBeam_3dB_WidthHorizontal     = MatInit;
Data.ProjectionBeamFocalPoint               = MatInit;
Data.ProjectionBeamWeightingWindowType      = MatInit;
Data.ProjectionBeamWeightingWindowParameter = MatInit;
Data.TransmitFlags                          = MatInit;
Data.HydrophoneMagicNumber                  = MatInit;
Data.ReceiveBeamWeightingWindow             = MatInit;
Data.ReceiveBeamWeightingParameter          = MatInit;
Data.ReceiveFlags                           = MatInit;
Data.ReceiveBeamWidth                       = MatInit;
Data.BottomDetectionFilterInfoMinRange      = MatInit;
Data.BottomDetectionFilterInfoMaxRange      = MatInit;
Data.BottomDetectionFilterInfoMinDepth      = MatInit;
Data.BottomDetectionFilterInfoMaxDepth      = MatInit;

Data.BottomDetectRangeFilterFlag            = MatInit;
Data.BottomDetectDepthFilterFlag            = MatInit;

Data.Absorption                             = MatInit;
Data.SoundVelocity                          = MatInit;
Data.Spreading                              = MatInit;
Data.TxRollCompensationFlag                 = MatInit;
Data.TxRollCompensationValue                = MatInit;
Data.TxPitchCompensationFlag                = MatInit;
Data.TxPitchCompensationValue               = MatInit;
Data.RxRollCompensationFlag                 = MatInit;
Data.RxRollCompensationValue                = MatInit;
Data.RxPitchCompensationFlag                = MatInit;
Data.RxPitchCompensationValue               = MatInit;
Data.RxHeaveCompensationFlag                = MatInit;
Data.RxHeaveCompensationValue               = MatInit;

DateDatagram  = zeros(1, N);
HeureDatagram = zeros(1, N);

% [nomDir, nomFic2] = fileparts(nomFicMat);
% if isempty(nomFicMat)
%     str1 = sprintf('Chargement fichier : %s', nomFic2);
%     str2 = sprintf('Loading file : %s', nomFic2);
% else
%     str1 = sprintf('Cr�ation fichier : %s', nomFic2);
%     str2 = sprintf('Creation file : %s', nomFic2);
% end
% fprintf('%s\n', Lang(str1,str2))
strLoop = infoLectureDatagrams(this, nomFic, 7000);
for k=1:N
    infoLoop(strLoop, k, N, 'pings')
    
    fseek(fid, Position(k), 'bof');
    
    [flag, DRF] = fread_DRF(fid);
    if ~flag
        break
    end
    [flag, SONAR] = fread_RecordType_7000(fid);
    if ~flag
        break
    end
    
%     Data.SystemSerialNumber = SystemSerialNumber;
    Data.SystemSerialNumber = DRF.DeviceIdentifier;

    
    DateDatagram(k)  = DRF.Date;
    HeureDatagram(k) = DRF.Time;
    
    Data.Frequency(k)                           = SONAR.RTH.Frequency;
    Data.SampleRate(k)                          = SONAR.RTH.SampleRate;
    Data.PingSequence(k)                        = SONAR.RTH.MultiPingSequence;
    Data.PingCounter(k)                         = SONAR.RTH.PingCounter;
    Data.ReceiverBandwith(k)                    = SONAR.RTH.ReceiverBandwith;
    Data.TxPulseWidth(k)                        = SONAR.RTH.TxPulseWidth;
    Data.TxPulseTypeIdentifier(k)               = SONAR.RTH.TxPulseTypeIdentifier;
    Data.TxPulseEnvelopeParameter(k)            = SONAR.RTH.TxPulseEnvelopeParameter;
    Data.TxPulseReserved(k)                     = SONAR.RTH.TxPulseReserved;
    Data.MaxPingRate(k)                         = SONAR.RTH.MaxPingRate;
    Data.PingPeriod(k)                          = SONAR.RTH.PingPeriod;
    Data.RangeSelection(k)                      = SONAR.RTH.RangeSelection;
    Data.PowerSelection(k)                      = SONAR.RTH.PowerSelection;
    Data.GainSelection(k)                       = SONAR.RTH.GainSelection;
    Data.ControlFlags(k)                        = SONAR.RTH.ControlFlags;
    Data.ProjectorMagicNumber(k)                = SONAR.RTH.ProjectorMagicNumber;
    Data.ProjectorBeamSteeringAngleVertical(k)  = SONAR.RTH.ProjectorBeamSteeringAngleVertical;
    Data.ProjectorBeamSteeringAngleHorizontal(k) = SONAR.RTH.ProjectorBeamSteeringAngleHorizontal;
    Data.ProjectionBeam_3dB_WidthVertical(k)    = SONAR.RTH.ProjectionBeam_3dB_WidthVertical;
    Data.ProjectionBeam_3dB_WidthHorizontal(k)  = SONAR.RTH.ProjectionBeam_3dB_WidthHorizontal;
    Data.ProjectionBeamFocalPoint(k)            = SONAR.RTH.ProjectionBeamFocalPoint;
    Data.ProjectionBeamWeightingWindowType(k)   = SONAR.RTH.ProjectionBeamWeightingWindowType;
    Data.ProjectionBeamWeightingWindowParameter(k) = SONAR.RTH.ProjectionBeamWeightingWindowParameter;
    Data.TransmitFlags(k)                       = SONAR.RTH.TransmitFlags;
    Data.HydrophoneMagicNumber(k)               = SONAR.RTH.HydrophoneMagicNumber;
    Data.ReceiveBeamWeightingWindow(k)          = SONAR.RTH.ReceiveBeamWeightingWindow;
    Data.ReceiveBeamWeightingParameter(k)       = SONAR.RTH.ReceiveBeamWeightingParameter;
    Data.ReceiveFlags(k)                        = SONAR.RTH.ReceiveFlags;
    Data.ReceiveBeamWidth(k)                    = SONAR.RTH.ReceiveBeamWidth;
    
    Data.BottomDetectRangeFilterFlag(k)         = SONAR.RTH.BottomDetectRangeFilterFlag;
    Data.BottomDetectDepthFilterFlag(k)         = SONAR.RTH.BottomDetectDepthFilterFlag;
    
    Data.BottomDetectionFilterInfoMinRange(k)   = SONAR.RTH.BottomDetectionFilterInfoMinRange;
    Data.BottomDetectionFilterInfoMaxRange(k)   = SONAR.RTH.BottomDetectionFilterInfoMaxRange;
    Data.BottomDetectionFilterInfoMinDepth(k)   = SONAR.RTH.BottomDetectionFilterInfoMinDepth;
    Data.BottomDetectionFilterInfoMaxDepth(k)   = SONAR.RTH.BottomDetectionFilterInfoMaxDepth;
    Data.Absorption(k)                          = SONAR.RTH.Absorption;
    Data.SoundVelocity(k)                       = SONAR.RTH.SoundVelocity;
    Data.Spreading(k)                           = SONAR.RTH.Spreading;
    
    Data.TxRollCompensationFlag(k)   = SONAR.RTH.TxRollCompensationFlag;
    Data.TxRollCompensationValue(k)  = 0;
    Data.TxPitchCompensationFlag(k)  = SONAR.RTH.TxPitchCompensationFlag;
    Data.TxPitchCompensationValue(k) = 0;
    Data.RxRollCompensationFlag(k)   = SONAR.RTH.RxRollCompensationFlag;
    Data.RxRollCompensationValue(k)  = 0;
    Data.RxPitchCompensationFlag(k)  = SONAR.RTH.RxPitchCompensationFlag;
    Data.RxPitchCompensationValue(k) = 0;
    Data.RxHeaveCompensationFlag(k)  = SONAR.RTH.RxHeaveCompensationFlag;
    Data.RxHeaveCompensationValue(k) = 0;
end
fclose(fid);
my_warndlg([strLoop ' : End'], 0);

Data.Time = cl_time('timeIfr', DateDatagram, HeureDatagram);
Data.PingCounter = PingCounter;
    
%% Sauvegarde de la donnee dans un fichier .mat

if ~isempty(nomFicMat)
    try
        if useCacheNetcdf == 1 % 1=Mat, 2 et 3=Netcdf
            save(nomFicMat, 'Data')
        else
            Data.Dimensions.nbPings = length(Data.PingCounter);
            NetcdfUtils.Struct2Netcdf(Data, ncFileName, 'SonarSettings', 'ncID', this.ncID);
        end
    catch ME %#ok<NASGU>
        str = sprintf('Write error file %s', nomFicMat);
        my_warndlg(str, 1);
    end
end
