function [flag, Data] = read_sonarSettings_bin(a)

flag = 0;
Data = [];

%% Lecture du r�pertoire SonarScope

[nomDir, nom] = fileparts(a.nomFic);
nomDir = fullfile(nomDir, 'SonarScope', nom);
if ~exist(nomDir, 'dir')
    return
end

%% Lecture du fichier XML d�crivant la donn�e 

nomFicXml = fullfile(nomDir, 'SonarSettings.xml');
flag = exist(nomFicXml, 'file');
if ~flag
    return
end
Data = xml_read(nomFicXml);

%% Lecture du r�pertoire SonarSettings

nomDirSonarSettings = fullfile(nomDir, 'SonarSettings');
if ~exist(nomDirSonarSettings, 'dir')
    flag = 0;
    return
end

%% Lecture des fichiers binaires des signaux

for i=1:length(Data.Signals)
    [flag, Signal] = readSignal(nomDir, Data.Signals(i), Data.NbSamples);
    if ~flag
        return
    end
    Data.(Data.Signals(i).Name) = Signal;
end
% Data = rmfield(Data, {'Signals'; 'Comments'});
