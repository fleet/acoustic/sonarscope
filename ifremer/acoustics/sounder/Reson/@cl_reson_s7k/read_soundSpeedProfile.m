function Data = read_soundSpeedProfile(this, identSondeur, varargin)

global useCacheNetcdf %#ok<GVMIS>

[varargin, DataIndex] = getPropertyValue(varargin, 'DataIndex', []); %#ok<ASGLU>

%% Check the number of elements

flag = checkIfOneElement(length(this));
if ~flag
    Data = [];
    return
end

%% On regarde si le fichier .mat existe

[nomDir, nomFic] = fileparts(this.nomFic);
nomFicMat  = fullfile(nomDir, 'SonarScope', [nomFic '_soundSpeedProfile.mat']);
ncFileName = fullfile(nomDir, 'SonarScope', [nomFic '.nc']);

UN = 1;
if UN && exist(nomFicMat, 'file') && checkFileCreation(nomFicMat, '26/09/2008')
    Data = loadmat(nomFicMat, 'nomVar', 'Data');
    %{
    figure;
    h(1) = subplot(1,4,1); plot(Data.WaterTemperature', Data.Depth'); grid on; title('WaterTemperature')
    h(2) = subplot(1,4,2); plot(Data.Salinity',   Data.Depth'); grid on; title('Salinity')
    h(3) = subplot(1,4,3); plot(Data.SoundSpeed', Data.Depth'); grid on; title('SoundSpeed')
    h(4) = subplot(1,4,4); plot(Data.Absorption', Data.Depth'); grid on; title('Absorption')
    linkaxes(h, 'y')
    %}
    return
end
if UN && exist(ncFileName, 'file') && NetcdfUtils.existGrp(ncFileName, 'SoundSpeedProfile', 'ncID', this.ncID)
    Data = NetcdfUtils.Netcdf2Struct(ncFileName, 'SoundSpeedProfile', 'ncID', this.ncID);
    if ~isfield(Data, 'SoundSpeed')
        Data = [];
        return
    end
    
    Data = structDim1FieldsDimNM_to_structDimNFieldsDim1M(Data);
    
    for k=1:length(Data)
        Data(k).TimeProfileMeasurement = cl_time('timeMat', Data(k).TimeProfileMeasurement);
        Data(k).TimeStartOfUse         = cl_time('timeMat', Data(k).TimeStartOfUse);
    end
    
    %{
    % Comment� par JMA le 13/04/2021 ATTENTION DANGER !!!
    if isequal(size(Data.SoundSpeed), [Data.nbPoints Data.nbSSP])
        Data.SoundSpeed = Data.SoundSpeed';
        Data.Depth = Data.Depth';
        if isfield(Data, 'WaterTemperature')
            Data.WaterTemperature = Data.WaterTemperature';
        end
        if isfield(Data, 'Salinity')
            Data.Salinity = Data.Salinity';
        end
        if isfield(Data, 'Absorption')
            Data.Absorption = Data.Absorption';
        end
    end
    %}
    return
end

%% On cr�e au besoin le repertoire SonarScope

nomDirMat = fullfile(nomDir, 'SonarScope');
if ~exist(nomDirMat, 'dir')
    status = mkdir(nomDirMat);
    if status
%         disp(sprintf('Le repertoire %s a ete cree avec succes', nomDirMat))
    else
        fprintf(['Impossible de cr�er le repertoire %s.\nJe vous conseille de rendre cela possible, ca permet de formater la donnee en fichiers matlab,\n' ...
                    'ce qui permettra d''acceder beaucoup plus vite a la donnee\n'], nomDirMat);
        nomFicMat = [];
    end
end

%% Lecture du fichier d'index

if isempty(DataIndex)
    [flag, DataIndex] = readFicIndex(this, identSondeur);
    if ~flag
        Data = [];
        return
    end
end

Position     = DataIndex(3,:);
TypeDatagram = DataIndex(5,:);

sub = (TypeDatagram == 1010);
Position = Position(sub);
clear TypeDatagram

if isempty(Position)
    Data = [];
    return
end

%% D�codage du fichier de donn�es

typeIndian = 'l';
[fid, message] = fopen(this.nomFic, 'r', typeIndian);
if fid < 0
    disp(message);
    return
end

N = length(Position);
MatInit = NaN(N,1);
Data.TimeStartOfUse         = cl_time('timeMat', MatInit);
Data.TimeProfileMeasurement = cl_time('timeMat', MatInit);

MatInit = NaN(N,1, 'single');
Data.SoundSpeed = MatInit;
Data.Depth      = MatInit;

kCTD = 0;
strLoop = infoLectureDatagrams(this, nomFicMat, 1010);
for k=1:N
    infoLoop(strLoop, k, N, 'records')
    
    fseek(fid, Position(k), 'bof');
    
    [flag, DRF] = fread_DRF(fid);
    if ~flag
        break
    end
    if DRF.Size > 1e5
        break
    end
    [flag, CTD] = fread_RecordType_1010(fid);
    if ~flag
        continue
    end
    if isempty(CTD.RD.SoundVelocity)
        continue
    end
    
    kCTD = kCTD + 1;
    T = cl_time('timeIfr', DRF.Date, DRF.Time);
    Data(kCTD).TimeStartOfUse         = T; %#ok<AGROW>
    Data(kCTD).TimeProfileMeasurement = T; %#ok<AGROW>
   
    Data(kCTD).Depth            = -CTD.RD.Depth; %#ok<AGROW>
    Data(kCTD).WaterTemperature = CTD.RD.WaterTemperature; %#ok<AGROW>
    Data(kCTD).Salinity         = CTD.RD.Salinity; %#ok<AGROW>
    Data(kCTD).SoundSpeed       = CTD.RD.SoundVelocity; %#ok<AGROW>
    Data(kCTD).Absorption       = CTD.RD.Absorption; %#ok<AGROW>
end
my_warndlg([strLoop ' : End'], 0);

Data = Data(1:kCTD);
    
% Data.TimeStartOfUse         = Data.TimeStartOfUse(1:N);
% Data.TimeProfileMeasurement = Data.TimeProfileMeasurement(1:N);
% 
% Data.SoundSpeed       = Data.SoundSpeed(1:N,:);
% Data.Depth            = Data.Depth(1:N,:);
% if isfield(Data, 'WaterTemperature')
%     N = min(N, size(Data.WaterTemperature,2));
%     Data.WaterTemperature = Data.WaterTemperature(1:N,:);
%     Data.Salinity         = Data.Salinity(1:N,:);
%     Data.Absorption       = Data.Absorption(1:N,:);
% end

fclose(fid);

%% Sauvegarde de la donn�e dans un fichier .mat

if ~isempty(nomFicMat)
    ncFileName = fullfile(nomDir, 'SonarScope', [nomFic '.nc']);
    try
        if useCacheNetcdf == 1 % 1=Mat, 2 et 3=Netcdf
            save(nomFicMat, 'Data')
        else
            nbSSP = length(Data);
            for k=1:nbSSP
                Data(k).nbSSP    = nbSSP; % Pour pouvoir remettre das le bon sens � la lecture
                Data(k).nbPoints = length(Data(k).Depth);
            end
            [Data, nbFields] = structDimNFieldsDim1M_to_structDim1FieldsDimNM(Data);
            [Data.Dimensions.nbSSP, Data.Dimensions.nbPoints] = size(Data.SoundSpeed);
            Data.Dimensions.nbFields = nbFields;
            NetcdfUtils.Struct2Netcdf(Data, ncFileName, 'SoundSpeedProfile', 'ncID', this.ncID);
        end
    catch ME %#ok<NASGU>
        messageErreurFichier(nomFicMat, 'WriteFailure');
    end
end
