function Data = read_verticalDepth(this, identSondeur, varargin)

global useCacheNetcdf %#ok<GVMIS>

[varargin, DataIndex] = getPropertyValue(varargin, 'DataIndex', []); %#ok<ASGLU>

%% Check the number of elements

flag = checkIfOneElement(length(this));
if ~flag
    Data = [];
    return
end

%% On regarde si le fichier .mat existe

[nomDir, nomFic] = fileparts(this.nomFic);
nomFicMat  = fullfile(nomDir, 'SonarScope', [num2str(identSondeur) '_' nomFic '_verticalDepth.mat']);
ncFileName = fullfile(nomDir, 'SonarScope', [nomFic '.nc']);

UN = 1;
if UN && exist(nomFicMat, 'file')
    Data = loadmat(nomFicMat, 'nomVar', 'Data');
    if ~isfield(Data, 'Datetime') % Pour ancien cache
        Data.Datetime = datetime(Data.Time.timeMat, 'ConvertFrom', 'datenum');
    end
    return
end
if UN && exist(ncFileName, 'file') && NetcdfUtils.existGrp(ncFileName, 'VerticalDepth', 'ncID', this.ncID)
    Data = NetcdfUtils.Netcdf2Struct(ncFileName, 'VerticalDepth', 'ncID', this.ncID);
    if ~isfield(Data, 'Datetime') % Pour ancien cache
        if ~isfield(Data, 'Time')
            Data = [];
            return
        end
        Data.Datetime = datetime(Data.Time.timeMat, 'ConvertFrom', 'datenum');
    end
    return
end

%% On cr�e au besoin le r�pertoire SonarScope

nomDirMat = fullfile(nomDir, 'SonarScope');
if ~exist(nomDirMat, 'dir')
    status = mkdir(nomDirMat);
    if status
%         disp(sprintf('Le repertoire %s a ete cree avec succes', nomDirMat))
    else
        fprintf('Impossible de c�er le r�pertoire %s.\nJe vous conseille de rendre cela possible, ca permet de formater la donn�e en fichiers matlab,\nce qui permettra d''acc�der beaucoup plus vite � la donn�e.\n', nomDirMat);
        nomFicMat = [];
    end
end

%% On recup�re le numero de serie car pas donn� dans le datagram contrairement � Simrad

% SystemSerialNumber = get_SonarSerialNumber(this);

%% Lecture du fichier d'index

if isempty(DataIndex)
    [flag, DataIndex] = readFicIndex(this, identSondeur);
    if ~flag
        Data = [];
        return
    end
end

Position     = DataIndex(3,:);
TypeDatagram = DataIndex(5,:);

sub = (TypeDatagram == 7009);
Position = Position(sub);
clear TypeDatagram

%% D�codage du fichier de donn�es

typeIndian = 'l';
[fid, message] = fopen(this.nomFic, 'r', typeIndian);
if fid < 0
    disp(message);
    Data = [];
    return
end

N = length(Position);
MatInit = zeros(1, N);
Data.Frequency           = MatInit;
Data.PingCounter         = MatInit;
Data.MultiPingSequence   = MatInit;
Data.Latitude            = MatInit;
Data.Longitude           = MatInit;
Data.Heading             = MatInit;
Data.AlongTrackDistance  = MatInit;
Data.AcrossTrackDistance = MatInit;
Data.VerticalDepth       = MatInit;
DateDatagram             = zeros(1, N);
HeureDatagram            = zeros(1, N);

[~, nomFicSeul] = fileparts(this.nomFic);
strLoop = infoLectureDatagrams(this, nomFicSeul, 7009);
for k=1:N
    infoLoop(strLoop, k, N, 'pings')
    
    fseek(fid, Position(k), 'bof');
    
    [flag, DRF] = fread_DRF(fid);
    if ~flag
        break
    end
    [flag, PingNumber, Sequence, VD] = fread_RecordType_7009(fid);
    if ~flag
        break
    end

    DateDatagram(k)  = DRF.Date;
    HeureDatagram(k) = DRF.Time;
    
    Data.Frequency(k)           = VD.RTH.Frequency;
    Data.PingCounter(k)         = VD.RTH.PingCounter;
    Data.MultiPingSequence(k)   = VD.RTH.MultiPingSequence;
    Data.Latitude(k)            = VD.RTH.Latitude;
    Data.Longitude(k)           = VD.RTH.Longitude;
    Data.Heading(k)             = VD.RTH.Heading;
    Data.AlongTrackDistance(k)  = VD.RTH.AlongTrackDistance;
    Data.AcrossTrackDistance(k) = VD.RTH.AcrossTrackDistance;
    Data.VerticalDepth(k)       = VD.RTH.VerticalDepth;
end
fclose(fid);
my_warndlg([strLoop ' : End'], 0);

Data.Time = cl_time('timeIfr', DateDatagram, HeureDatagram);
Data.Datetime = datetime(Data.Time.timeMat, 'ConvertFrom', 'datenum');

%% Sauvegarde de la donn�e dans un fichier .mat

if ~isempty(nomFicMat)
    try
        if useCacheNetcdf == 1 % 1=Mat, 2 et 3=Netcdf
            save(nomFicMat, 'Data')
        else
            Data.Dimensions.nbPings = length(Data.PingCounter);
            NetcdfUtils.Struct2Netcdf(Data, ncFileName, 'VerticalDepth', 'ncID', this.ncID);
        end
    catch
        str = sprintf('Erreur ecriture fichier %s', nomFicMat);
        my_warndlg(str, 1);
    end
end
