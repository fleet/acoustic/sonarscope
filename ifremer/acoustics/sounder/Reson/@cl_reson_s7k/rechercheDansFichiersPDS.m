function rechercheDansFichiersPDS(this, S7kCenter, nomFicOut, Carto)

EPS = 1 / (24*3600*10000);

E2 = get(Carto, 'Ellipsoide.Excentricite') ^ 2;
A  = get(Carto, 'Ellipsoide.DemiGrandAxe');

fidCsv = fopen(nomFicOut, 'w+');
if fidCsv == -1
	messageErreurFichier(nomFicOut, 'WriteFailure');
end
fprintf(fidCsv, 'Nom du profil;Nom fichier 7kc;Nu Ping 7kc;Nu faisceau;Qualit� (x/3);Nom fichier PDS;Nu Ping PDS;Nu faisceau;Heure;Profondeur (m);Distance trans (m);Angle du faisceau (deg);Latitude;Longitude;Latitude;Longitude;Qualit� (x/3);Indice profil\n');

[nomDir, nomFic3d] = fileparts(nomFicOut);
nomFic3d = fullfile(nomDir, [nomFic3d '_3D.csv']);
fid3DV = fopen(nomFic3d, 'w+');
fprintf(fid3DV, '"_E","type","lat(deg)","lon(deg)","immersion","ref","layer","topPoint","line","bottomPoint","pointColor","pointShape","pointSize"\n');

% strCouleur = {'yellow'; 'red'; 'black'; 'brown'; 'teal'; 'purple'; 'blue'; 'gray'; 'white'; 'orange'; 'green'};
strCouleur = {'yellow'; 'red'; 'brown'; 'teal'; 'purple'; 'blue'; 'gray'; 'white'; 'orange'; 'green'};
nbCouleurs = length(strCouleur);
strShape   = {'plain';'point'};
nbShapes   = length(strShape);

subS7kCenter = 1:length(S7kCenter);
id = 0;
for k=1:length(this)
    if isempty(S7kCenter)
        break
    end
    
    if isempty(this(k))
        str = sprintf('%s n''existe pas', this(k).nomFic);
        my_warndlg(str, 1);
        fclose(fidCsv);
        return
    end
    [flag, DataDepth] = read_depth_s7k(this(k));
    if ~flag
        continue
    end
    
    %         Range = DataDepth.Range(iPing, :);
    %         FigUtils.createSScFigure; PlotUtils.createSScPlot(Range, 'r'); grid on
    T = DataDepth.Time.timeMat;
    
    subsTrouve = [];
    fprintf('%s\n', this(k).nomFic);
    [~, nomFicS7kPDS, ext] = fileparts(this(k).nomFic);
    nomFicS7kPDS = [nomFicS7kPDS ext]; %#ok<AGROW>
    for s=1:length(S7kCenter)
        S = S7kCenter(s);
        if (S.Time < T(1)) || (S.Time > T(end))
            continue
        end
        
        t = cl_time('timeMat', T(1));
        strTDeb = t2str(t);
        t = cl_time('timeMat', T(end));
        strTFin = t2str(t);
        
        fprintf('%s - %s : %s\n', S.strTime, strTDeb, strTFin);
%         iPingPDS = find((DataDepth.PingCounter == S.PingCounter) & (DataDepth.MultiPingSequence == S.PingSequence));
%         iPingPDS = find(nearlyEqual(DataDepth.Time.timeMat, S.Time, EPS) & (DataDepth.MultiPingSequence == S.PingSequence));
        iPingPDS = find((DataDepth.Time.timeMat == S.Time) & (DataDepth.MultiPingSequence == S.PingSequence));
        
        if isempty(iPingPDS)
            [Ecart, iPingPDS_Min] = min(abs(DataDepth.Time.timeMat - S.Time));
            figure(754674); plot(abs(DataDepth.Time.timeMat - S.Time)); grid on
            if (iPingPDS_Min ~= 1) && (iPingPDS_Min ~= length(DataDepth.PingCounter))
                iPingPDS = iPingPDS_Min;
            end
        end
        
%         iPingPDSSansMultiPing = find(DataDepth.PingCounter == S.PingCounter);
%         iPingPDSSansMultiPing = find(nearlyEqual(DataDepth.Time.timeMat, S.Time, EPS)); %#ok<EFIND>
        iPingPDSSansMultiPing = find((DataDepth.Time.timeMat == S.Time)); %#ok<EFIND>
        if isempty(iPingPDS)
            % TODO : Le sondeur est en mode multiping et l'info n'est pas report�e
            % dans S.PingSequence fichier 20120118_065038_PP-7150-24kHz-.s7k
            if ~isempty(iPingPDSSansMultiPing)
                iPingPDS = find(DataDepth.PingCounter == S.PingCounter);
                fprintf(1,'iPingPDS = %d PDSSequence = %d mais pingSequence pas report� dans fichier 7KCenter\n', ...
                    iPingPDS, DataDepth.MultiPingSequence(iPingPDS));
            end
            continue
        end
      
        %{
        % Contr�les
        S.Time - T(iPingPDS)
        FigUtils.createSScFigure; PlotUtils.createSScPlot(S.Range, 'r+'); grid on; hold on; PlotUtils.createSScPlot(DataDepth.Range(iPingPDS,:), 'bx')
        %}
        
        iBeam = S.iBeam;
        DataDepth.MultiPingSequence(iPingPDS);
        Heading = double(DataDepth.Heading(iPingPDS));
        AlongD  = DataDepth.AlongDist(iPingPDS, iBeam);
        Depth   = DataDepth.Depth(iPingPDS, iBeam);
        AcrossD = DataDepth.AcrossDist(iPingPDS, iBeam);
        AngleFaisceau    = DataDepth.BeamDepressionAngle(iPingPDS, iBeam);
        [LatPDS, LonPDS] = calculLatLonFromAlongAcross(E2, A, DataDepth.Latitude(iPingPDS), DataDepth.Longitude(iPingPDS), Heading, AlongD, AcrossD);

        nuProfil = str2double(S.nomProfil(2:end));
        fprintf(fidCsv, '%s;%s;%d;%d;%d;%s;%d;%d;%s;%f;%f;%f;%s;%s;%10.6f;%11.7f;%d;%d\n', ...
            S.nomProfil, S.nomFic7KCenter, S.iPing, S.iBeam, S.iQual, ...
            nomFicS7kPDS, iPingPDS, iBeam, S.strTime, Depth, AcrossD, AngleFaisceau, lat2str(LatPDS), lon2str(LonPDS), LatPDS, LonPDS, S.iQual, nuProfil);
        
        id = id + 1;
        couleur = 1 + mod(nuProfil, nbCouleurs);
        shape   = 1 + mod(floor(nuProfil/nbCouleurs), nbShapes);
        fprintf(fid3DV, '"%d","POINT","%10.6f","%11.7f","0","RELATIVE","-","VRAI","VRAI","FAUX","%s",%s,"45",\n', id, LatPDS, LonPDS, strCouleur{couleur}, strShape{shape});
    
        subsTrouve = [subsTrouve s]; %#ok<AGROW>
    end
    if ~isempty(subsTrouve)
        n = length(S7kCenter);
        S7kCenter(subsTrouve) = [];
        subS7kCenter(subsTrouve) = [];
        fprintf('%d - %d =  %d : %s\n', n, length(subsTrouve), length(S7kCenter), num2str(subS7kCenter))
    end
    
%     S7kCenter.Time = T(iPing);
%     S7kCenter.PingCounter = DataDepth.PingCounter(iPing);
%     S7kCenter.PingSequence = DataDepth.MultiPingSequence(iPing);
%     S7kCenter.iBeam = iBeam;
%     S7kCenter.strTime = datestr(T(iPing), FormatHeure);
end
fclose(fidCsv);
fclose(fid3DV);

%% Test si des lignes n'ont pas �t� trouv�es

N = length(S7kCenter);
if N ~=0
    str1 = 'Certains points n''ont pas �t� trouv�s, la liste est donn�e dans la fen�tre de commande';
    str2 = 'TODO';
    my_warndlg(Lang(str1,str2), 1);
    for k=1:N
        S = S7kCenter(k);
    end
end

