function resetMask_S7K(~, nomFic, varargin)

if ~iscell(nomFic)
    nomFic = {nomFic};
end

str1 = 'Remise � z�ro des masks des fichiers .all';
str2 = 'Reset masks from .all files';
N = length(nomFic);
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
    my_waitbar(k, N, hw);
    flag = resetMask_S7K_unitaire(nomFic{k});
    if ~flag
        return
    end
end
my_close(hw, 'MsgEnd')


function flag = resetMask_S7K_unitaire(nomFicS7k)

global useCacheNetcdf %#ok<GVMIS>

flag = 0;

this = cl_reson_s7k('nomFic', nomFicS7k);
if isempty(this)
    return
end
identSondeur = selectionSondeur(this);

[nomDir, nomFic] = fileparts(nomFicS7k);
nomFicMat  = fullfile(nomDir, 'SonarScope', [num2str(identSondeur) '_' nomFic '_depth.mat']);
ncFileName = fullfile(nomDir, 'SonarScope', [nomFic '.nc']);

try
    if exist(nomFicMat, 'file') && checkFileCreation(nomFicMat, '10/04/2008')
        Data = loadmat(nomFicMat, 'nomVar', 'Data');
    end
    if exist(ncFileName, 'file') && NetcdfUtils.existGrp(ncFileName, 'Depth', 'ncID', this.ncID)
        Data = NetcdfUtils.Netcdf2Struct(ncFileName, 'Depth', 'ncID', this.ncID);
    end
catch ME %#ok<CTCH>
    fprintf('%s\n', getReport(ME));
    messageErreurFichier(nomFicMat, 'ReadFailure', 'Message', ME.message);
    return
end

% Voir ce qui est fait dans sonar_importBathyFromCaraibesMBG_S7K : c'est pas top, on perd d�finitivement les infos Type de d�tection, Brightness, Colinearity
% Data.Mask(Data.Mask == 224) = 231;
% Data.Mask(Data.Mask == 0)   = 1;
if isfield(Data, 'MaskOrigin')
    Data.Mask = Data.MaskOrigin;
else
    str1 = 'TODO';
    str2 = sprintf('WARNING : the cache file "%s" was cereated before a release that corrected a bug in the "Mask reset". redo the cache if you absolutely need this function.', ncFileName);
    my_warndlg(Lang(str1,str2), 1);
end

try
    if useCacheNetcdf == 1 % 1=Mat, 2 et 3=Netcdf
        save(nomFicMat, 'Data')
    else
        [Data.Dimensions.nbPings, Data.Dimensions.nbBeams] = size(Data.BeamDepressionAngle);
        NetcdfUtils.Struct2Netcdf(Data, ncFileName, 'Depth');
    end
catch ME %#ok<CTCH>
    fprintf('%s\n', getReport(ME));
    messageErreurFichier(nomFicMat, 'WriteFailure', 'Message', ME.message);
    return
end
flag = 1;
