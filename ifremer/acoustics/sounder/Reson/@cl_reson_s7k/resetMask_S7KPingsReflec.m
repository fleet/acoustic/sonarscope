function resetMask_S7KPingsReflec(this, nomFic, varargin) %#ok<INUSL>

if ~iscell(nomFic)
    nomFic = {nomFic};
end

identSondeur = [];
str1 = 'Remise � z�ro des masks de r�flectivit� des fichiers .s7k en cours.';
str2 = 'Reset Reflectivity masks from .s7k files';
N = length(nomFic);
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
    my_waitbar(k, N, hw);
    [flag, identSondeur] = resetMask_S7KPingsReflec_unitaire(nomFic{k}, identSondeur);
    if ~flag
        return
    end
end
my_close(hw, 'MsgEnd')


function [flag, identSondeur] = resetMask_S7KPingsReflec_unitaire(nomFic, identSondeur)

global useCacheNetcdf %#ok<GVMIS>

flag = 0;

%% Lecture de l'image

this = cl_reson_s7k('nomFic', nomFic);
if isempty(this)
    return
end
if isempty(identSondeur)
    identSondeur = selectionSondeur(this);
end

[flag, Data, identSondeur, ~, ~, nomFicMat, ncFileName] = read_depth_s7k(this, 'identSondeur', identSondeur);
if isempty(Data)
    return
end

%% Nettoyage de la r�flectivit�

if isfield(Data, 'MaskPingsReflectivity') && ~isempty(Data.MaskPingsReflectivity)
    Data.MaskPingsReflectivity(:) = 0;

    try
        if useCacheNetcdf == 1 % 1=Mat, 2 et 3=Netcdf
            save(nomFicMat, 'Data')
        else
            [Data.Dimensions.nbPings, Data.Dimensions.nbBeams] = size(Data.BeamDepressionAngle);
            NetcdfUtils.Struct2Netcdf(Data, ncFileName, 'Depth');
        end
        flag = 1;
    catch ME %#ok<NASGU>
        messageErreurFichier(nomFicMat, 'WriteFailure');
        flag = 0;
    end
end
