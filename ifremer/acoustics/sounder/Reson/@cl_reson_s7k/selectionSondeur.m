% Lecture des datagrams "depth" dans un fichier .s7k
%
% Syntax
%   Data = selectionSondeur(a, ...)
% 
% Input Arguments
%   a : Instance de cl_reson_s7k
%
% Examples
%   nomFic = getNomFicDatabase('RESON7125_20051116_221245.s7k')
%   a = cl_reson_s7k('nomFic', nomFic);
%   b = view_depth(a);
%
% See also cl_reson_s7k cl_reson_s7k/plot_position cl_reson_s7k/plot_nav Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function identSondeur = selectionSondeur(this)

%% Check the number of elements

flag = checkIfOneElement(length(this));
if ~flag
    return
end

% TODO : voir APPENDIX B du document "D:\perso-jma\Documents\Reson\DFD_7k_Version_3.10.pdf"
listeDevice = [1002 7111 7125 7150 8101 20 13003 13016 14000]; % Ajout 1002 le 19/05/2021 pour sondeur ODOM MB2 de chez Teledyne
[~, ~, Ext] = fileparts(this.nomFicIndex);
if strcmp(Ext, '.index')
    for k=1:length(listeDevice)
        [nomDir, nom, ext] = fileparts(this.nomFicIndex);
        nom = sprintf('%s_%s', num2str(listeDevice(k)), nom);
        nomFicIndex = fullfile(nomDir, [nom ext]);
        flagDevice(k) = exist(nomFicIndex, 'file'); %#ok<AGROW>
        strListeDevice{k} = num2str(listeDevice(k)); %#ok<AGROW>
    end
    subDevice = find(flagDevice == 2);
else
    Data  = NetcdfUtils.Netcdf2Struct(this.nomFicIndex, 'Index', 'ncID', this.ncID);
    Index = Data.Index;
    DeviceIdentifier = unique(Index(8,:));
    for k=1:length(listeDevice)
        flagDevice(k) = ismember(listeDevice(k), DeviceIdentifier); %#ok<AGROW>
        strListeDevice{k} = num2str(listeDevice(k)); %#ok<AGROW>
    end
    subDevice = find(flagDevice);
end

%%

if isempty(subDevice)
    identSondeur = [];
    str1 = 'TODO';
    str2 = sprintf('Insufficient datagrams to determine the device identifier, rename in .bad file "%s"', this.nomFic);
    renameBadFile(this.nomFic, 'Message', Lang(str1,str2)) % on renomme le fichier en .bad
    return
end

strListeDevice = strListeDevice(flagDevice == 2);
switch length(subDevice)
    case 0
        my_warndlg('No file available', 1);
        identSondeur = [];
    case 1
        identSondeur = listeDevice(subDevice);
    otherwise
        str1 = 'Choix (messag � compl�ter)';
        str2 = 'Select one';
        [rep, flag] = my_listdlg(Lang(Lang(str1,str2)), strListeDevice, 'SelectionMode', 'Single');
        if ~flag
            identSondeur = [];
            return
        end
        identSondeur = listeDevice(subDevice(rep));
end
