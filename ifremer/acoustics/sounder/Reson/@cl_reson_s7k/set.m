% Acces en ecriture des proprietes d'une instance
%
% Syntax
%   b = set(a, ...)
%
% Input Arguments
%   a : Une instance de la classe cl_reson_s7k
%
% Name-Value Pair Arguments
%   nomFic : Nom(s) du fichier .s7k
%
% Output Arguments
%   [] : Sauve l'instance dans a
%   b  : Instance de la classe
%
% Examples
%   nomFic = getNomFicDatabase('RESON7125_20051116_221245.s7k')
%   a = cl_reson_s7k
%   a = set(a, 'nomFic', nomFic)
%
% See also cl_reson_s7k cl_reson_s7k/get Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function varargout = set(this, varargin)

global useCacheNetcdf %#ok<GVMIS>

[varargin, nomFic]   = getPropertyValue(varargin, 'nomFic',   []);
[varargin, KeepNcID] = getPropertyValue(varargin, 'KeepNcID', false); %#ok<ASGLU>

if ~isempty(nomFic)
    flag = exist(nomFic, 'file');
    if ~flag 
        varargout{1} = cl_reson_s7k.empty();
        return
    end
    if sizeFic(nomFic) == 0
        varargout{1} = cl_reson_s7k.empty();
        mes = messageErreurFichier(nomFic, 'ReadFailure');
        renameBadFile(nomFic, 'Message', mes)
        return
    end
    
    nomFic = windows_replaceVolumeLetter(nomFic);
    this.nomFic = nomFic;
    [nomDir, nomFicSeul] = fileparts(nomFic);
    
    if useCacheNetcdf == 1 % 1=Mat, 2 et 3=Netcdf
        this.nomFicIndex = fullfile(nomDir, 'SonarScope', [nomFicSeul '.index']);
    else
        this.nomFicIndex = fullfile(nomDir, 'SonarScope', [nomFicSeul '.nc']);
        if exist(this.nomFicIndex, 'file')
            ncID = netcdf.open(this.nomFicIndex, 'WRITE');
            [this.ExistGrpIndex, ncID]              = NetcdfUtils.existGrp(this.nomFicIndex, 'Index',                  'ncID', ncID);
            if this.ExistGrpIndex
                [this.ExistGrpAttitude, ncID]       = NetcdfUtils.existGrp(this.nomFicIndex, 'Attitude',               'ncID', ncID);
                this.ExistGrpVerticalDepth          = NetcdfUtils.existGrp(this.nomFicIndex, 'VerticalDepth',          'ncID', ncID);
                this.ExistGrpNavigation             = NetcdfUtils.existGrp(this.nomFicIndex, 'Navigation',             'ncID', ncID);
                this.ExistGrpSoundSpeedProfile      = NetcdfUtils.existGrp(this.nomFicIndex, 'SoundSpeedProfile',      'ncID', ncID);
                this.ExistGrpDepth                  = NetcdfUtils.existGrp(this.nomFicIndex, 'Depth',                  'ncID', ncID);
                this.ExistGrpWaterColumnDataIFRData = NetcdfUtils.existGrp(this.nomFicIndex, 'WaterColumnDataIFRData', 'ncID', ncID);
                this.ExistGrpWCD                    = NetcdfUtils.existGrp(this.nomFicIndex, 'WCD',                    'ncID', ncID);
%                 this.ExistGrpMagPhase             = NetcdfUtils.existGrp(this.nomFicIndex, 'MagPhase',               'ncID', ncID);
                this.ExistGrpMagPhase               = NetcdfUtils.existGrp(this.nomFicIndex, 'MagPhase',               'ncID', ncID);%, 'Close', ~KeepNcID);
                if KeepNcID
                    this.ncID = ncID;
                else
                    netcdf.close(ncID);
                end
            else
                netcdf.close(ncID);
            end
            varargout{1} = this;
            return
        end
    end
    
    flagCreFicIndex = 1;
    
    nomFicIndex = setNomFicIndex(this, 7111);
    flag = exist(nomFicIndex, 'file');
    if flag && (sizeFic(nomFicIndex) ~= 0)
        flagCreFicIndex = 0;
        this.SounderName = 7111;
    end
    
    nomFicIndex = setNomFicIndex(this, 7125);
    flag = exist(nomFicIndex, 'file');
    if flag && (sizeFic(nomFicIndex) ~= 0)
        flagCreFicIndex = 0;
        this.SounderName = 7125;
    end
    
    nomFicIndex = setNomFicIndex(this, 7150);
    flag = exist(nomFicIndex, 'file');
    if flag && (sizeFic(nomFicIndex) ~= 0)
        flagCreFicIndex = 0;
        this.SounderName = 7150;
    end
    
    nomFicIndex = setNomFicIndex(this, 20); % Shallow Survey 2015
    flag = exist(nomFicIndex, 'file');
    if flag && (sizeFic(nomFicIndex) ~= 0)
        flagCreFicIndex = 0;
        this.SounderName = 20;
    end
    
    nomFicIndex = setNomFicIndex(this, 13016); % Shallow Survey 2015
    flag = exist(nomFicIndex, 'file');
    if flag && (sizeFic(nomFicIndex) ~= 0)
        flagCreFicIndex = 0;
        this.SounderName = 13016;
    end
    
    nomFicIndex = setNomFicIndex(this, 13003); % Shallow Survey 2015
    flag = exist(nomFicIndex, 'file');
    if flag && (sizeFic(nomFicIndex) ~= 0)
        flagCreFicIndex = 0;
        this.SounderName = 13003;
    end
    
    if flagCreFicIndex
        this = cre_ficIndex(this);
        
        % Ajout JMA le 02/10/2021
        view_depth(this, 'ListeLayers', 6, 'memeReponses', 2);
%         prepare_lectures7k_step1(nomFic); % Commenté par JMA le 04/20/2021 car ça bmoqie le //Tbx !!!
    end
end

%% Sortie

if nargout == 0
    assignin('caller', inputname(1), this);
else
    varargout{1} = this;
end
