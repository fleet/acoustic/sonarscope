function nomFicIndex = setNomFicIndex(this, identSondeur)

global useCacheNetcdf %#ok<GVMIS>

if useCacheNetcdf == 1 % 1=Mat, 2 et 3=Netcdf
    [nomDir, nom, ext] = fileparts(this.nomFicIndex);
    nom = sprintf('%s_%s', num2str(identSondeur), nom);
    nomFicIndex = fullfile(nomDir, [nom ext]);
else
    [nomDir, nom, ext] = fileparts(this.nomFicIndex);
    nomFicIndex = fullfile(nomDir, [nom ext]);
end
