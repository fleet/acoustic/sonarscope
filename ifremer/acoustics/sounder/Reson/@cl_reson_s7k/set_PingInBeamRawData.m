function [this, ncID, grpID, varID] = set_PingInBeamRawData(this, k, N, identSondeur, varargin)

[varargin, ncID]  = getPropertyValue(varargin, 'ncID',  []);
[varargin, grpID] = getPropertyValue(varargin, 'grpID', []);
[varargin, varID] = getPropertyValue(varargin, 'varID', []);
[varargin, Close] = getPropertyValue(varargin, 'Close', 1); %#ok<ASGLU>

if k > N
    my_breakpoint
    return
end


if isempty(ncID)
    [nomFicWaterColumnData, flagFilesExist] = ResonBeamRawDataNames(this.nomFic, identSondeur, []); %#ok<ASGLU>
    ncID = netcdf.open(nomFicWaterColumnData.Mat, 'WRITE');
end

if isempty(grpID)
    grpID = netcdf.inqNcid(ncID, 'BeamRawData');
end
if isempty(varID)
    varID = netcdf.inqVarID(grpID, 'PingInBeamRawData');
end

PingInBeamRawData = netcdf.getVar(grpID, varID);

PingInBeamRawData(k) = 1;

netcdf.putVar(grpID, varID, uint8(PingInBeamRawData));

if Close
    netcdf.close(ncID);
end
