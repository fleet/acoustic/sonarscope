function this = set_PingInWCD(this, k, N, identSondeur)

if k > N
    my_breakpoint
    return
end

[nomFicWaterColumnData, flagFilesExist] = ResonWaterColumnDataNames(this.nomFic, identSondeur, []);

if isempty(this.ncID)
    ncID = netcdf.open(nomFicWaterColumnData.Mat, 'WRITE');
else
    ncID = this.ncID;
end

grpID = netcdf.inqNcid(ncID, 'WaterColumnDataIFRData');
varID = netcdf.inqVarID(grpID, 'PingInWCD');
PingInWCD = netcdf.getVar(grpID, varID);

PingInWCD(k) = 1;

netcdf.putVar(grpID, varID, uint8(PingInWCD));

if isempty(this.ncID)
    netcdf.close(ncID);
end
