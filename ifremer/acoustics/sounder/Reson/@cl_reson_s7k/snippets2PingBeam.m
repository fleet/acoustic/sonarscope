% function [flag, ReflectivityPingBeam200, ReflectivityPingBeam100, ReflectivityPingBeam1] = snippets2PingBeam(this, DataDepth, identSondeur)
function [flag, ReflectivityPingBeam100, ReflectivityPingBeam1] = snippets2PingBeam(this, DataDepth, identSondeur, varargin)

[varargin, DataIndex] = getPropertyValue(varargin, 'DataIndex', []); %#ok<ASGLU>

ReflectivityPingBeam1   = [];
ReflectivityPingBeam100 = [];
% ReflectivityPingBeam200 = [];

%% Check the number of elements

flag = checkIfOneElement(length(this));
if ~flag
    return
end
[~, nomFicSeul] = fileparts(this.nomFic);

%% R�cuperation de la fr�quence d'�chantillonnage

Data = read_sonarSettings(this, identSondeur);
% VolatileSonarSettings_PingCounter = Data.PingCounter;

%% Lecture du fichier d'index

if isempty(DataIndex)
    [flag, DataIndex] = readFicIndex(this, identSondeur, 'DataIndex', DataIndex);
    if ~flag
        return
    end
end

Position         = DataIndex(3,:);
TypeDatagram     = DataIndex(5,:);
PingCounter      = DataIndex(6,:);
PingSequence     = DataIndex(7,:);
DeviceIdentifier = DataIndex(8,:);

isEmpty7008 = isempty(find(TypeDatagram == 7008, 1));
isEmpty7058 = isempty(find(TypeDatagram == 7058, 1));
isEmpty7028 = isempty(find(TypeDatagram == 7028, 1));
if ~isEmpty7058
    [flag, subDataSnippet] = get_subDataCommon(7058, TypeDatagram, PingCounter, PingSequence, DeviceIdentifier, identSondeur);
    if ~flag
        return
    end
    codeDatagram = 7058;
    
elseif ~isEmpty7008
    [flag, subDataSnippet] = get_subDataCommon(7008, TypeDatagram, PingCounter, PingSequence, DeviceIdentifier, identSondeur);
    if ~flag
        return
    end
    codeDatagram = 7008;
    
elseif ~isEmpty7028
    [flag, subDataSnippet] = get_subDataCommon(7028, TypeDatagram, PingCounter, PingSequence, DeviceIdentifier, identSondeur);
    if ~flag
        return
    end
    codeDatagram = 7028;
    
else
    strLoop = sprintf('S7K cache makeup : %s : Group Depth : getting reflectivity from snippets : No 7008 nor 7058 datagrams found', nomFicSeul);
    my_warndlg(strLoop, 0, 'Tag', 'NoSnippetsInThisFile');
    return
end
clear DeviceIdentifier

if isempty(subDataSnippet)
    my_warndlg('No Snippet data in this file', 1);
    return
end

%% Recherche automatique des num�ros de faisceau limites

% [subl_Depth, subl_Sidescan] = intersect3(DataDepth.PingCounter, PingCounter(subDataSnippet), PingCounter(subDataSnippet));
PS  = PingSequence(subDataSnippet);
subNon0 = (PS ~= 0);
PS(subNon0) = PS(subNon0) - 1;
PSnippets = PingCounter(subDataSnippet) * 4 + PS;

PS  = DataDepth.MultiPingSequence;
subNon0 = (PS ~= 0);
PS(subNon0) = PS(subNon0) - 1;
PDepth = DataDepth.DepthPingCounterBeforeUnwrap * 4 + PS;

% [subl_Sidescan, subl_Depth] = intersect3(PingCounter(subDataSnippet), DataDepth.PingCounter, DataDepth.PingCounter);
[subl_Sidescan, subl_Depth] = intersect3(PSnippets, PDepth, PDepth);
subDataSnippet = subDataSnippet(subl_Sidescan);
if isempty(subl_Depth)
    my_warndlg('No snippets data in this file', 1);
    return
end

Position = Position(subDataSnippet);
% Taille   = Taille(subDataSnippet);
clear TypeDatagram

%% D�codage du fichier de donn�es

typeIndian = getIndianAll(this.nomFic);
% flagEndian = strcmp(typeIndian, 'b');
[fid, message] = fopen(this.nomFic, 'r', typeIndian);
if fid < 0
    my_warndlg(['cl_reson_s7k:read_SnippetData' message], 0);
    return
end

%% Lecture du nombre de faisceaux

fseek(fid, Position(1), 'bof');
flag = fread_DRF(fid);
if ~flag
    return
end

N = length(Position);

DateDatagram  = NaN(1, N);
HeureDatagram = NaN(1, N);

Data.PingCounter       = NaN(1, N, 'single');
Data.MultiPingSequence = NaN(1, N, 'single');
Data.NbBeams           = NaN(1, N, 'single');

% NbBeamsBathy = max(DataDepth.NbBeams);
ReflectivityPingBeam1   = NaN(size(DataDepth.Depth), 'single');
ReflectivityPingBeam100 = NaN(size(DataDepth.Depth), 'single');
% ReflectivityPingBeam200 = NaN(size(DataDepth.Depth), 'single');
    
strLoop = sprintf('S7K cache makeup : %s : Group Depth (catching datagrams %d) : getting reflectivity from snippets', nomFicSeul, codeDatagram);
my_warndlg([strLoop ' : Begin'], 0);

for k=1:N
    infoLoop(strLoop, k, N, 'pings')

    fseek(fid, Position(k), 'bof');

    [flag, DRF] = fread_DRF(fid);
    if ~flag
        break
    end
    
    if ~isEmpty7058
        [flag, PingNumber, Sequence, BeamData] = fread_RecordType_7058(fid, DRF, DataDepth.NbBeams(k), ...
            'Filename', this.nomFic);%, DataDepth.Range(iDepth,:));
        typeSnippets = 7058;
        if ~flag
            str1 = sprintf('Le datagramme 7058 semble beuguer pour le fichier %s. ReflectivityFromSnippets sera vide.', this.nomFic);
            str2 = sprintf('Datagramme 7058 seems corrupted for %s. ReflectivityFromSnippets will be empty.', this.nomFic);
            my_warndlg(Lang(str1,str2), 0, 'Tag', 'BugLecture7058');
            flag = 0;
%             my_close(hw)
            fclose(fid);
            my_warndlg([strLoop ' : Failed'], 0);
            return
        end    
    elseif ~isEmpty7028
        [flag, PingNumber, Sequence, BeamData] = fread_RecordType_7028(fid, DataDepth.NbBeams(k));%, DataDepth.Range(iDepth,:));
        typeSnippets = 7028;
    elseif ~isEmpty7008 % GLT : j'ai invers� les deux, le 7028 �tant plus r�cent.
        [flag, PingNumber, Sequence, BeamData] = fread_RecordType_7008(fid, DRF);
        typeSnippets = 7008;
    end
    if ~flag
        break
    end
    
    DateDatagram(k)  = DRF.Date;
    HeureDatagram(k) = DRF.Time;

    Data.SystemSerialNumber = DRF.DeviceIdentifier;
    Data.EmModel            = BeamData.RTH.SonarId;
    
    Data.PingCounter(k)       = BeamData.RTH.PingCounter;
    Data.MultiPingSequence(k) = BeamData.RTH.MultiPingSequence;
    Data.NbBeams(k)           = BeamData.RTH.N;
%     Data.DataSampleType(k)  = BeamData.RTH.DataSampleType;

    Data.BeamDescriptor        = BeamData.RD.BeamDescriptor;
    Data.BeginSampleDescriptor = BeamData.RD.BeginSampleDescriptor;
    Data.EndSampleDescriptor   = BeamData.RD.EndSampleDescriptor;
    if isfield(BeamData, 'OD') && ~isempty(BeamData.OD)
        Data.Frequency               = BeamData.OD.Frequency / 1000; % En kHz
        Data.CenterSampleDistance    = BeamData.OD.CenterSampleDistance;
        Data.BeamAcrossTrackDistance = BeamData.OD.BeamAcrossTrackDistance;
        %{
        figure; plot(BeamData.OD.BeamAlongTrackDistance); grid on; title('BeamData.OD.BeamAlongTrackDistance')
        figure; plot(BeamData.OD.BeamAcrossTrackDistance); grid on; title('BeamData.OD.BeamAcrossTrackDistance')
        figure; plot(BeamData.OD.CenterSampleDistance); grid on; title('BeamData.OD.CenterSampleDistance')
        %}
    end
    
    [X100, X1] = reflec_Snippet(BeamData, typeSnippets);
    
    switch identSondeur
        case 7111 % Valid� sur donn�es du SHOM
            % TODO
            %{
unique(Data.ReceiveBeamWidth) * 180/pi = 1.499999401411068
unique(Data.ProjectionBeam_3dB_WidthVertical) * 180/pi = 2.999998802822136
figure; plot(Data.ReceiveBeamWidth); grid
figure; plot(Data.ProjectionBeam_3dB_WidthVertical); grid
figure; plot(Data.CenterSampleDistance); grid
figure; plot(Data.SampleRate); grid
figure; plot(Data.SoundVelocity); grid
            %}
            
            Datetime = datetime(Data.Time.timeMat, 'ConvertFrom', 'datenum');
            if Datetime(1) < datetime(2014, 1, 1) % TODO : trouver la date pr�cise
                SoundSpeed_mps  = Data.SoundVelocity(k);
                SampleFrequency = Data.SampleRate(k);
                Tp_s = Data.TxPulseWidth(k);
                TxBeamWidth_rad = Data.ProjectionBeam_3dB_WidthVertical(k);
                RxBeamWidth_rad = Data.ReceiveBeamWidth(k);
                r_m = Data.CenterSampleDistance * (SoundSpeed_mps / (2*SampleFrequency));
                rn_metres = min(r_m);
                A0 = (SoundSpeed_mps * Tp_s * TxBeamWidth_rad .* r_m) ./ (2 * sqrt(1 - (rn_metres^2 ./ (r_m.^2))));
                AN = (RxBeamWidth_rad .* TxBeamWidth_rad) .* (r_m.^2);
                A0 = reflec_Enr2dB(A0);
                AN = reflec_Enr2dB(AN);
                Offset = min([A0; AN]);
                
                % Memo : voir analysis7111 pour visualiser les parties de
                % la donn�es qui sont recevables pour faire une analyse.
                Offset = 0; % TODO : en attendant de trouver un fichier qui permette de statuer
            else
                Offset = 0;
            end

%             figure(16252); plot(A0, 'b'); grid on; hold on; plot(AN, 'r'); hold off;
            
%             Offset = 0;
%             Offset = reflec_Amp2dB(Data.TxPulseWidth(k));
%             Offset = - Data.GainSelection(k);
%             Offset = min([A0; AN]) - Data.GainSelection(k);
%             Offset = 2 * min([A0; AN]);
%             Offset = 4 * min([A0; AN]);
%             Offset = 2*min([A0; AN]) - Data.GainSelection(k);
%             Offset = min([A0; AN]) - Data.GainSelection(k) / 2;
%             figure; plot(A0, 'b'); grid on; hold on; plot(AN, 'k'); plot(Offset, 'r')
            
        case 7125 % TODO : voir plus haut, donn�es Annaelle
            Offset = -(Data.PowerSelection(k) - 215) / 2; % Test sur donn�e Jean-Guy 7125_20160310_083050_VWFSDeneb
            
            TCor = reflec_Amp2dB(Data.TxPulseWidth(k)) / 13.3;
            Offset = Offset - TCor;
            
            TCor = (21-Data.GainSelection(k)) * 0.74;
            Offset = Offset + TCor;
            
            %                Offset = 0;
            
        case 7150 % TODO
            Offset = 0;
            
        otherwise
            Offset = 0;
    end
    ReflectivityPingBeam1(  subl_Depth(k),1:length(X1))   = X1   + Offset;
    ReflectivityPingBeam100(subl_Depth(k),1:length(X100)) = X100 + Offset;
    %         ReflectivityPingBeam200(subl_Depth(k),1:length(X200)) = X200 + Offset;
    
%     [Samples, TxAngle] = process_Snippet(DataDepth, BeamData, iDepth, fe(k), Data.Frequency, idebBeamBathy, ifinBeamBathy);
end
fclose(fid);
my_warndlg([strLoop ' : End'], 0);
