function flag = sonar_S7K_CleanReflectivity(~, liste)

if ischar(liste)
    liste = {liste};
end

Carto = [];
FigPosition = [];

identSondeur = [];
N = length(liste);
str1 = 'Nettoyage de la réflectivité des fichiers .s7k';
str2 = 'Reflectivity cleaning of .s7k files';
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    fprintf('%s\n%d / %d : %s\n', repmat('-',1,100), k, N, liste{k});
    
    [flag, Carto, FigPosition, identSondeur] = sonar_S7K_CleanReflectivity_unitaire(liste{k}, Carto, FigPosition, identSondeur);
    if ~flag
        break
    end
    
    %% On demande si on veut traiter les autres fichiers
    
    if k ~= N
        flag = question_ContinueNextFile();
        if ~flag
            break
        end
    end
end
my_close(hw, 'MsgEnd')


function [flag, Carto, FigPosition, identSondeur] = sonar_S7K_CleanReflectivity_unitaire(nomFic, Carto, FigPosition, identSondeur)

global useCacheNetcdf %#ok<GVMIS>

flag = 0;

%% Check if file exists

if ~exist(nomFic, 'file')
    str1 = sprintf('Le fichier "%s" n''existe pas.', nomFic);
    str2 = sprintf('"%s" does not exist.', nomFic);
    my_warndlg(Lang(str1,str2), 0);
    return
end

%% Création de l'instance cl_simrad_all

this = cl_reson_s7k('nomFic', nomFic);
if isempty(this)
    return
end
if isempty(identSondeur)
    identSondeur = selectionSondeur(this);
end

[flag, Data, ~, ~, ~, nomFicMat, ncFileName] = read_depth_s7k(this, 'identSondeur', identSondeur);
if isempty(Data)
    return
end

%% Lecture des layers

if isfield(Data, 'MaskPingsReflectivity') && ~isempty(Data.MaskPingsReflectivity)
    MaskPingsReflectivity = Data.MaskPingsReflectivity(:);
else
    MaskPingsReflectivity = zeros(size(Data.PingCounter), 'single');
end
ReflecRaw = Data.ReflectivityFromSnippets100(:,:);
Reflectivity = ReflecRaw;
Reflectivity(Data.Mask(:,:) == 1) = NaN;
Reflectivity(MaskPingsReflectivity == 1,:) = NaN;

MeanValue = mean(Reflectivity, 2, 'omitnan');
MeanValue0 = MeanValue;

if sum(~isnan(MeanValue)) < 2
    flag = 0;
    return
else
    [~, nomFicSeul] = fileparts(nomFic);
    ObjReflecRaw    = cl_image('Image', ReflecRaw',    'Name', [nomFicSeul ' - Raw data'], 'ColormapIndex', 2);
    ObjReflecRaw.CLim = '1%';

    ObjReflectivity = cl_image('Image', Reflectivity', 'Name', nomFicSeul, 'ColormapIndex', 2);
    ObjReflectivity.CLim = '1%';
    
    ObjReflectivity0 = ObjReflectivity;
    ObjReflectivity.Name = [nomFicSeul ' - Masked'];

    if isempty(FigPosition) || ~ishandle(FigPosition)
        FigPosition = figure;
    else
        figure(FigPosition);
    end
    FigPosition.WindowState = 'maximized';
    h = get(FigPosition, 'children');
    delete(h);
    
    hc(3) = subplot(3,1,3); plot(MeanValue', '-o');  c = colorbar; c.Visible = 'Off'; axis tight; title('Mean per ping'); grid on;
    hc(2) = subplot(3,1,2); imagesc(ObjReflectivity, 'Axe', hc(2)); %, 'CLim', '1%'
    hc(1) = subplot(3,1,1); imagesc(ObjReflecRaw,    'Axe', hc(1)); %, 'CLim', '1%'
    linkaxes(hc, 'x'); axis tight; drawnow;
    
    str1 = 'Voulez-vous nettoyer la réflectivité ?';
    str2 = 'Do you want to clean up the reflectivity ?';
    [rep, flag] = my_questdlg(Lang(str1,str2));
    if ~flag
        return
    end
    if rep == 2
        return
    end
    
    ImageName = ObjReflecRaw.Name;
    nomImage  = strtok(ImageName, ' ');
    x = 1:length(MeanValue);
    I = get_Image(ObjReflectivity);
    y = 1:size(I,1);
    CLim2 = ObjReflectivity.CLim;
    imageData2 = ClImageData('cData', I, 'xData', x, 'yData', y, 'colormapData', gray(256), 'clim', CLim2, 'name', ImageName);
    
    ySampleImage = YSample('name', nomImage,    'data', ones(1, length(x), 'single')); 
    ySample      = YSample('name', 'MeanValue', 'data', MeanValue,         'single', 'marker', '.'); 
            
    xSampleRawOne = XSample( 'name', 'Pings', 'data', x);
    signalOne     = ClSignal('Name', 'Reflectivity of ',    'xSample', xSampleRawOne, 'ySample', ySampleImage);
    signalOne(2)  = ClSignal('Name', 'Mean value per ping', 'xSample', xSampleRawOne, 'ySample', ySample);
    
    Title = 'Reflectivity data cleaning (define ping flags)';
    s = SignalDialog(signalOne, 'Title', Title, 'cleanInterpolation', 0, 'imageDataList', imageData2);%, 'waitAnswer', false);
    s.openDialog();
    
    if s.okPressedOut
        X = MeanValue;
        MeanValue = ySample.data;
        subOK = (MeanValue(:) == X(:));
        Reflectivity(~subOK,:) = NaN;
        MeanValue = mean(Reflectivity, 2, 'omitnan');    

        ObjReflectivity = cl_image('Image', Reflectivity', 'Name', 'Reflec after current data cleaning', 'ColormapIndex', 2);
        ObjReflectivity.CLim = '1%';
        
        if isempty(FigPosition) || ~ishandle(FigPosition)
            FigPosition = figure;
        else
            figure(FigPosition);
        end
        FigPosition.WindowState = 'maximized';
        h = get(FigPosition, 'children');
        delete(h);
        hc(1) = subplot(4,1,1); imagesc(ObjReflectivity0, 'Axe', hc(1)); %, 'CLim', '1%'
        hc(2) = subplot(4,1,2); plot(MeanValue0'); c = colorbar; c.Visible = 'Off'; axis tight; title('Mean per ping'); grid on;
        hc(3) = subplot(4,1,3); imagesc(ObjReflectivity,  'Axe', hc(3)); %,  'CLim', '1%'
        hc(4) = subplot(4,1,4); plot(MeanValue');  c = colorbar; c.Visible = 'Off'; axis tight; title('Mean per ping after current data cleaning'); grid on;
        linkaxes(hc, 'x'); axis tight; drawnow;
        
        str1 = 'Acceptez-vous définitivement ce masque ?';
        str2 = 'Do you definitively accept this mask ?';
        [ind, flag] = my_questdlg(Lang(str1,str2));
        
        my_close(FigPosition); % Ajout JMA le 16/11/2021
        
        if ~flag || (ind == 2)
            flag = 0;
            return
        end
    else
        return
    end
end
if all(subOK)
    return
end

MaskPingsReflectivity(~subOK) = 1;
Data.MaskPingsReflectivity = MaskPingsReflectivity;

try
    if useCacheNetcdf == 1 % 1=Mat, 2 et 3=Netcdf
        save(nomFicMat, 'Data')
    else
        [Data.Dimensions.nbPings, Data.Dimensions.nbBeams] = size(Data.BeamDepressionAngle);
        NetcdfUtils.Struct2Netcdf(Data, ncFileName, 'Depth');
    end
    flag = 1;
catch ME %#ok<NASGU>
    messageErreurFichier(nomFicMat, 'WriteFailure');
    flag = 0;
end

