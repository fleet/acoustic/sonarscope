function sonar_importBathyFromCaraibesMBG_S7K(this, listFileNames, nomFicMBG, varargin) %#ok<INUSL>

useParallel = get_UseParallelTbx;

if ~iscell(listFileNames)
    listFileNames = {listFileNames};
end

if ~iscell(nomFicMBG)
    nomFicMBG = {nomFicMBG};
end
I0 = cl_image_I0;

%% R�cup�ration du type de sondeur dans le premier .S7K

this = cl_reson_s7k('nomFic', listFileNames{1});
if isempty(this)
    return
end

[flag, SonarIdent] = DeviceIdentifier2IdentSonar(this, selectionSondeur(this));
if ~flag
    return
end

%% S�lection des flag Caraibes

[flag, ListeFlagsInvalides] = paramsFlagCaraibes;
if ~flag
    return
end

%% Question // tbx au cas o� les .all auraient d�j� �t� convertis

N_MBG = length(nomFicMBG);
N_S7K = length(listFileNames);
if (N_MBG > 1) || (N_S7K > 1)
    useParallelHere = useParallel;
else
    useParallelHere = 0;
end

%% Lecture des heures d�but et fin des fichiers .mbg

str1 = 'Inspection des fichiers MBG.';
str2 = 'Check MBG files.';
if useParallelHere && (N_MBG > 1)
    [hw, DQ] = create_parforWaitbar(N_MBG, Lang(str1,str2));
    k = 1;
    MBG = import_CaraibesMbg(I0, nomFicMBG{k}, 'Sonar.Ident', SonarIdent, ...
        'ListeFlagsInvalides', ListeFlagsInvalides, 'indLayerImport', 3, ...
        'isUsingParfor', true);
    SonarTimeMbg = get(MBG, 'SonarTime');
    SonarTimeMbg = SonarTimeMbg.timeMat;
    TimeMatMbg(k,:) = SonarTimeMbg(1,[1 end]);
    send(DQ, 1); % pour prendre en compte le traitement du premier fichier qui a �t� fait sans la // Tbx
    parfor (k=2:N_MBG, useParallelHere)
        MBG = import_CaraibesMbg(I0, nomFicMBG{k}, 'Sonar.Ident', SonarIdent, ...
            'ListeFlagsInvalides', ListeFlagsInvalides, 'indLayerImport', 3, ...
            'isUsingParfor', true);
        SonarTimeMbg = get(MBG, 'SonarTime');
        SonarTimeMbg = SonarTimeMbg.timeMat;
        TimeMatMbg(k,:) = SonarTimeMbg(1,[1 end]);
        send(DQ, k);
    end
    my_close(hw, 'MsgEnd')
else
    hw = create_waitbar(Lang(str1,str2), 'N', N_MBG);
    for k=1:N_MBG
        my_waitbar(k, N_MBG, hw);
        MBG = import_CaraibesMbg(I0, nomFicMBG{k}, 'Sonar.Ident', SonarIdent, ...
            'ListeFlagsInvalides', ListeFlagsInvalides, 'indLayerImport', 3);
        SonarTimeMbg = get(MBG, 'SonarTime');
        SonarTimeMbg = SonarTimeMbg.timeMat;
        TimeMatMbg(k,:) = SonarTimeMbg(1,[1 end]); %#ok<AGROW>
    end
    my_close(hw, 'MsgEnd')
end

% tri des heures de d�but croissante
[TimeMatMbg,idx] = sortrows(TimeMatMbg, 1);
nomFicMBG = nomFicMBG(idx);

%% Traitement des fichiers .s7k

str1 = 'Importation des masques de Caraibes en cours.';
str2 = 'Import masks from Caraibes MBG files.';
if useParallelHere && (N_S7K > 1)
    [hw, DQ] = create_parforWaitbar(N_S7K, Lang(str1,str2));
    k = 1;
    sonar_importBathyFromCaraibesMBG_S7K_unitaire(listFileNames{k}, nomFicMBG, TimeMatMbg, ...
        SonarIdent, ListeFlagsInvalides, 'isUsingParfor', true);
    send(DQ, 1); % pour prendre en compte le traitement du premier fichier qui a �t� fait sans la // Tbx
    parfor (k=2:N_S7K, useParallelHere)
        sonar_importBathyFromCaraibesMBG_S7K_unitaire(listFileNames{k}, nomFicMBG, TimeMatMbg, ...
            SonarIdent, ListeFlagsInvalides, 'isUsingParfor', true);
        send(DQ, k);
    end
    my_close(hw, 'MsgEnd')
else
    hw = create_waitbar(Lang(str1,str2), 'N', N_S7K);
    for k=1:N_S7K
        my_waitbar(k, N_S7K, hw);
        
        flag = sonar_importBathyFromCaraibesMBG_S7K_unitaire(listFileNames{k}, nomFicMBG, TimeMatMbg, ...
            SonarIdent, ListeFlagsInvalides);
        
        if ~flag && (k ~= N_S7K)
            flag = question_ContinueNextFile();
            if ~flag 
                break
            end
        end
    end
    my_close(hw, 'MsgEnd')
end


function flag = sonar_importBathyFromCaraibesMBG_S7K_unitaire(nomFicS7k, nomFicMBG, TimeMatMbg, SonarIdent, ListeFlagsInvalides, varargin)

global useCacheNetcdf %#ok<GVMIS>
global isUsingParfor %#ok<GVMIS>

[varargin, isUsingParfor] = getPropertyValue(varargin, 'isUsingParfor', isUsingParfor); %#ok<ASGLU>

I0 = cl_image_I0;

%% Fichier .s7k

this = cl_reson_s7k('nomFic', nomFicS7k);
if isempty(this)
    flag = 0;
    return
end

[flag, Data, identSondeur] = read_depth_s7k(this);
if ~flag
    return
end

%% Recherche des pings Kongsberg vides

SonarTimeS7K = Data.Time.timeMat;

if isempty(SonarTimeS7K)
    fprintf('\n\t%s --> probl�me fichier !!! \n\n',  nomFicS7k);
    return
end

%% Lecture du fichier MBG

for k=1:length(nomFicMBG)
    if SonarTimeS7K(end) < TimeMatMbg(k,1) || SonarTimeS7K(1) > TimeMatMbg(k,2)
        continue
    end
    
    MBG = import_CaraibesMbg(I0, nomFicMBG{k}, 'Sonar.Ident', SonarIdent, ...
        'ListeFlagsInvalides', ListeFlagsInvalides, 'indLayerImport', 3);
    SonarTimeMbg = get(MBG, 'SonarTime');
    SonarTimeMbg = SonarTimeMbg.timeMat;
    % premier essai avec le temps
    
    %     datetime(SonarTimeMbg((501:508)+70), 'ConvertFrom', 'datenum', 'Format','d-MMM-y HH:mm:ss.SS')
    
    T0 = SonarTimeS7K(1);
    x = datevec(T0);
    x(6) = floor(x(6));
    T0 = datenum(x);
    
    %   datetime(T0, 'ConvertFrom', 'datenum', 'Format','d-MMM-y HH:mm:ss.SSS')
    %   datetime(SonarTimeS7K((1:8)+70), 'ConvertFrom', 'datenum', 'Format','d-MMM-y HH:mm:ss.SSS')
    
    SonarTimeS7KMinusT0 = SonarTimeS7K - T0;
    SonarTimeMbgMinusT0 = SonarTimeMbg - T0;
    
    %     SonarTimeS7KMinusT0 = floor(SonarTimeS7KMinusT0 * (24*3600*1000));
    SonarTimeS7KMinusT0 = round(SonarTimeS7KMinusT0 * (24*3600*1000));
    %     SonarTimeMbgMinusT0 = floor(SonarTimeMbgMinusT0 * (24*3600*1000));
    %     SonarTimeMbgMinusT0 = floor(SonarTimeMbgMinusT0 * (24*3600*1000)); % -0.5 (mail MPC)
    SonarTimeMbgMinusT0 = round(SonarTimeMbgMinusT0 * (24*3600*1000)); % -0.5 (mail MPC)
    
    % Ajout JMA le 30/05/2017 pour fichier HERMINE
    % 20170403_015001_PP_7150_12kHz.s7k (mail Charline le 30/05/2017)
    
    %{
SonarTimeS7KMinusT0(1:8)
SonarTimeMbgMinusT0(501:508)

round(SonarTimeS7KMinusT0(1:8), 1)
round(SonarTimeMbgMinusT0(501:508), 1)

SonarTimeS7KMinusT0((1:8)+70)
SonarTimeMbgMinusT0((501:508)+70)

round(SonarTimeS7KMinusT0((1:8)+70), 1)
round(SonarTimeMbgMinusT0((501:508)+70), 1)

round(SonarTimeS7KMinusT0((1:8)+70)*2, 1)/2
round(SonarTimeMbgMinusT0((501:508)+70)*2, 1)/2
    %}
    
    %     SonarTimeS7KMinusT0 = round(SonarTimeS7KMinusT0*2, -1)/2;
    %     SonarTimeMbgMinusT0 = round(SonarTimeMbgMinusT0*2, -1)/2;
    % Fin ajout JMA le 30/05/2017 pour fichier HERMINE
    
    %     sub1 = 1:length(SonarTimeS7KMinusT0);
    %     sub2 = 1:length(SonarTimeMbgMinusT0);
%     FigUtils.createSScFigure; PlotUtils.createSScPlot(SonarTimeS7KMinusT0, '+b'); grid on; hold on; PlotUtils.createSScPlot(SonarTimeMbgMinusT0, 'xr');
    if size(SonarTimeMbgMinusT0, 1) == 1
        [~, subS7K_t, subMbg_t] = intersect(SonarTimeS7KMinusT0, SonarTimeMbgMinusT0(:));
%     FigUtils.createSScFigure; PlotUtils.createSScPlot(subS7K_t, '+b'); grid on; hold on; PlotUtils.createSScPlot(subMbg_t, 'xr');
        [~, subS7K_tPlus1ms,  subMbg_tPlus1ms]  = intersect(SonarTimeS7KMinusT0+1, SonarTimeMbgMinusT0(:));
        [~, subS7K_tMinus1ms, subMbg_tMinus1ms] = intersect(SonarTimeS7KMinusT0-1, SonarTimeMbgMinusT0(:));
        [~, subS7K_tPlus2ms,  subMbg_tPlus2ms]  = intersect(SonarTimeS7KMinusT0+2, SonarTimeMbgMinusT0(:));
        [~, subS7K_tMinus2ms, subMbg_tMinus2ms] = intersect(SonarTimeS7KMinusT0-2, SonarTimeMbgMinusT0(:));
        [~, subS7K_tPlus3ms,  subMbg_tPlus3ms]  = intersect(SonarTimeS7KMinusT0+3, SonarTimeMbgMinusT0(:));
        [~, subS7K_tMinus3ms, subMbg_tMinus3ms] = intersect(SonarTimeS7KMinusT0-3, SonarTimeMbgMinusT0(:));
        [~, subS7K_tPlus4ms,  subMbg_tPlus4ms]  = intersect(SonarTimeS7KMinusT0+4, SonarTimeMbgMinusT0(:));
        [~, subS7K_tMinus4ms, subMbg_tMinus4ms] = intersect(SonarTimeS7KMinusT0-4, SonarTimeMbgMinusT0(:));
    else
        [~, subS7K_t, subMbg_t] = intersect(SonarTimeS7KMinusT0, SonarTimeMbgMinusT0(:,1));
        [~, subS7K_tPlus1ms,  subMbg_tPlus1ms]  = intersect(SonarTimeS7KMinusT0+1, SonarTimeMbgMinusT0(:,1));
        [~, subS7K_tMinus1ms, subMbg_tMinus1ms] = intersect(SonarTimeS7KMinusT0-1, SonarTimeMbgMinusT0(:,1));
        [~, subS7K_tPlus2ms,  subMbg_tPlus2ms]  = intersect(SonarTimeS7KMinusT0+2, SonarTimeMbgMinusT0(:,1));
        [~, subS7K_tMinus2ms, subMbg_tMinus2ms] = intersect(SonarTimeS7KMinusT0-2, SonarTimeMbgMinusT0(:,1));
        [~, subS7K_tPlus3ms,  subMbg_tPlus3ms]  = intersect(SonarTimeS7KMinusT0+3, SonarTimeMbgMinusT0(:,1));
        [~, subS7K_tMinus4ms, subMbg_tMinus4ms] = intersect(SonarTimeS7KMinusT0-4, SonarTimeMbgMinusT0(:,1));
    end
    
    subS7K_t = sort([subS7K_t; subS7K_tPlus1ms; subS7K_tPlus2ms; subS7K_tPlus3ms; subS7K_tPlus4ms; subS7K_tMinus1ms; subS7K_tMinus2ms; subS7K_tMinus3ms; subS7K_tMinus4ms]);
    subMbg_t = sort([subMbg_t; subMbg_tPlus1ms; subMbg_tPlus2ms; subMbg_tPlus3ms; subMbg_tPlus4ms; subMbg_tMinus1ms; subMbg_tMinus2ms; subMbg_tMinus3ms; subMbg_tMinus4ms]);
    
    %{
SonarTimeS7KMinusT0(1:8)
SonarTimeMbgMinusT0(501:508)

SonarTimeS7KMinusT0((1:8)+70)
SonarTimeMbgMinusT0((501:508)+70)

SonarTimeS7KMinusT0((1:8)+114)
SonarTimeMbgMinusT0((501:508)+114)

figure; plot(SonarTimeS7KMinusT0(subS7K_t) - SonarTimeMbgMinusT0(subMbg_t), '*'); grid on
    %}
    
    if isempty(subS7K_t)
        disp('Pas d''intersection.... Bizarre !');
        continue
    end
    
    subS7K = subS7K_t;
    subMbg = subMbg_t;
    
    %{
figure; plot(SonarTimeS7KMinusT0(subS7K) - SonarTimeMbgMinusT0(subMbg), '*'); grid on
    %}
    
    % Sortie �cran
    fprintf('\n\t%s\n\t%s\n\t--> %d / %d ping impacted\n',  nomFicS7k, nomFicMBG{k}, numel(subS7K), numel(SonarTimeS7K));
    
    X = get_val_ij(MBG, subMbg, []);
    X = isnan(X);
    mask = Data.Mask(subS7K,:);
    
    switch identSondeur
        case 7111
            X = X(:,1:301);
%             mask(X) = 224; % 231 ?
            mask(X) = 0; % Modif JMA le 26/05/2021
        case 7125
%             X = X(:,1:256); % Comment� par JMA le 12/01/2019 pour donn�es ROV o� nb beams = 512
            mask(X) = 0;
        case 7150
            X = X(:,1:size(mask,2)); % pour g�rer les cas de variation du nombre de faisceaux.
%             mask(X) = 224; % 231 ?
            mask(X) = 0; % Modif JMA le 26/05/2021
    end
    
    %% Masquage
    
    Data.Mask(subS7K,:) = mask;
    [nomDir, nomFic] = fileparts(this.nomFic);
    nomFicMat  = fullfile(nomDir, 'SonarScope', [num2str(identSondeur) '_' nomFic '_depth.mat']);
    ncFileName = fullfile(nomDir, 'SonarScope', [nomFic '.nc']);
    try
        if useCacheNetcdf == 1 % 1=Mat, 2 et 3=Netcdf
            save(nomFicMat, 'Data')
        else
            [Data.Dimensions.nbPings, Data.Dimensions.nbBeams] = size(Data.BeamDepressionAngle);
            NetcdfUtils.Struct2Netcdf(Data, ncFileName, 'Depth');
        end
        flag = 1;
    catch ME %#ok<CTCH>
        fprintf('%s\n', getReport(ME));
        messageErreurFichier(nomFicMat, 'WriteFailure', 'Message', ME.message);
        flag = 0;
    end
end
