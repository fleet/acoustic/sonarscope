function sonar_importBathyFromCaris_S7K(this, nomFicS7K, nomFicTxt, varargin) %#ok<INUSL>

useParallel = get_UseParallelTbx;

if ~iscell(nomFicS7K)
    nomFicS7K = {nomFicS7K};
end
if ~iscell(nomFicTxt)
    nomFicTxt = {nomFicTxt};
end

NbLigEntete  = [];
Separator    = [];
ValNaN       = [];
nColumns     = [];
GeometryType = 7;
IndexColumns = [];
DataType     = [];
Carto        = cl_carto([]);
TypeAlgo     = [];

str1 = 'Importation des masques de Caris';
str2 = 'Import masks from CARIS';
N = length(nomFicS7K);
if useParallel && (N > 2)
    [hw, DQ] = create_parforWaitbar(N, Lang(str1,str2));
    k = 1;
    [flag, NbLigEntete, Separator, ValNaN, nColumns, GeometryType, IndexColumns, DataType, Carto, TypeAlgo] ...
        = sonar_importBathyFromCaris_S7K_unitaire(nomFicS7K{k}, nomFicTxt{k}, NbLigEntete, Separator, ValNaN, nColumns, ...
        GeometryType, IndexColumns, DataType, Carto, TypeAlgo);
    if ~flag
        close(hw)
        return
    end
    %     parfor (k=1:N, useParallel)
    send(DQ, k);
    parfor k=2:N
        sonar_importBathyFromCaris_S7K_unitaire(nomFicS7K{k}, nomFicTxt{k}, NbLigEntete, Separator, ValNaN, nColumns, ...
            GeometryType, IndexColumns, DataType, Carto, TypeAlgo, 'isUsingParfor', true);
        send(DQ, k);
    end
    my_close(hw, 'MsgEnd')
else
    hw = create_waitbar(Lang(str1,str2), 'N', N);
    for k=1:N
        my_waitbar(k, N, hw);
        [flag, NbLigEntete, Separator, ValNaN, nColumns, GeometryType, IndexColumns, DataType, Carto, TypeAlgo] ...
            = sonar_importBathyFromCaris_S7K_unitaire(nomFicS7K{k}, nomFicTxt{k}, NbLigEntete, Separator, ValNaN, nColumns, ...
            GeometryType, IndexColumns, DataType, Carto, TypeAlgo); %#ok<ASGLU>
    end
    my_close(hw, 'MsgEnd')
end


function [flag, NbLigEntete, Separator, ValNaN, nColumns, GeometryType, IndexColumns, DataType, Carto, TypeAlgo] = ...
    sonar_importBathyFromCaris_S7K_unitaire(nomFicS7k, nomFicTxt, NbLigEntete, Separator, ValNaN, nColumns, ...
    GeometryType, IndexColumns, DataType, Carto, TypeAlgo, varargin)

global useCacheNetcdf %#ok<GVMIS>
global isUsingParfor %#ok<GVMIS>

[varargin, isUsingParfor] = getPropertyValue(varargin, 'isUsingParfor', isUsingParfor); %#ok<ASGLU>

%% Test si il existe un fichier .txt

if isempty(nomFicTxt)
    flag = 1;
    return
end

%% Lecture de l'image

this = cl_reson_s7k('nomFic', nomFicS7k);
if isempty(this)
    flag = 0;
    return
end

[flag, Data, identSondeur] = read_depth_s7k(this);
if ~flag
    return
end

%% Recherche des pings Kongsberg vides

% X = ~isnan(Data.Depth);
% Y = sum(X,2);
% subPingsVides = find(Y == 0);
% clear X Y

%% Lecture du fichier Caris

[nbPings, nbBeams] = size(Data.Depth);
[flag, MaskCaris] = read_CarisASCII(nomFicTxt, nbPings, nbBeams);
if ~flag
    return
end

subAll = 1:nbPings;

%% Masquage

subCaris = Data.subCaris;

% TODO : il manque la d�finition de subCaris dans cl_reson_s7k/get_subDataCommon mais j'ai peur
% d'introduire une boulette. Ce manque est trait� maladroitement
% ici dans sonar_importBathyFromCaris_S7K.m
% Il faudra traiter cela s�rieusement si bug rencontr�

if isempty(subCaris)
    subCaris = subAll;
end

% Data.Mask(subAll,:) = Data.Mask(subAll,:) & ~MaskCaris(subCaris,:);
X = Data.Mask(subAll,:);
try
    X(MaskCaris(subCaris,:) == 1) = 0;
catch
    X(MaskCaris(:,:) == 1) = 0;
end
Data.Mask(subAll,:) = X;

[nomDir, nomFic] = fileparts(this.nomFic);
nomFicMat  = fullfile(nomDir, 'SonarScope', [num2str(identSondeur) '_' nomFic '_depth.mat']);
ncFileName = fullfile(nomDir, 'SonarScope', [nomFic '.nc']);
try
    if useCacheNetcdf == 1 % 1=Mat, 2 et 3=Netcdf
        save(nomFicMat, 'Data')
    else
        [Data.Dimensions.nbPings, Data.Dimensions.nbBeams] = size(Data.BeamDepressionAngle);
        NetcdfUtils.Struct2Netcdf(Data, ncFileName, 'Depth');
    end
    flag = 1;
catch ME %#ok<NASGU>
    messageErreurFichier(nomFicMat, 'WriteFailure');
    flag = 0;
end
