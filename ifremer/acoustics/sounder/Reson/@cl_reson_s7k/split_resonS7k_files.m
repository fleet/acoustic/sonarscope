function split_resonS7k_files(this, nomFic, nbPingMax) %#ok<INUSL>

if ~iscell(nomFic)
    nomFic = {nomFic};
end

for k=1:length(nomFic)
    unitaire_split_resonS7k_files(nomFic{k}, nbPingMax)
end


function unitaire_split_resonS7k_files(nomFic, nbPingMax)

this = cl_reson_s7k('nomFic', nomFic);
identSondeur = selectionSondeur(this);

%% Lecture du fichier d'index

[flag, DataIndex] = readFicIndex(this, identSondeur);
if ~flag
    return
end

Date             = DataIndex(1,:);
Heure            = DataIndex(2,:);
Position         = DataIndex(3,:);
Taille           = DataIndex(4,:);
TypeDatagram     = DataIndex(5,:);
PingCounter      = DataIndex(6,:);
PingSequence     = DataIndex(7,:);
DeviceIdentifier = DataIndex(8,:);

[flag, subDataDepth] = get_subDataCommon(7006, TypeDatagram, PingCounter, PingSequence, DeviceIdentifier, identSondeur);
if ~flag
    return
end

Position7006 = Position(subDataDepth);

NbRecorsDepth = length(Position7006);
% nbPingMax = 500
if NbRecorsDepth < nbPingMax
%     [nomDir, nomFicSimple] = fileparts(nomFic);
%     nomFicSimple = sprintf('%s_%02d.s7k', nomFicSimple, 0);
%     nomFicOut = fullfile(nomDir, nomFicSimple);
%     flag = copyfile(nomFic, nomFicOut);
%     if ~flag
%         str = [Lang('Impossible de creer le fichier', 'Impossible to create the file') nomFicOut];
%         my_warndlg(str, 1);
        str1 = sprintf('Le nombre de pings du fichier "%s" est %d.\nLe nombre de pings donn� pour la d�coupe est %d, til est inf�rieur au nombre de pings du fichier.', nomFic, NbRecorsDepth, nbPingMax);
        str2 = sprintf('The number of pings for file "%s" is %d.\nThe number of pings delivered for splitting the file is %d, that is lower than the number of pings of the file.', nomFic, NbRecorsDepth, nbPingMax);
        my_warndlg(Lang(str1,str2), 0);
        return
%     end
%     return
end

fid = fopen(nomFic, 'r', 'l'); 
if fid == -1
    messageErreurFichier(nomFic, 'ReadFailure');
    return
end

%% Traitement du fichier
                    
nbFics = ceil(NbRecorsDepth/nbPingMax);
str = Lang('D�coupe des fichiers .s7k en cours. Attendez, SVP', 'Spliting .s7k files');
hw = create_waitbar(str, 'N', nbFics);
for iFic=1:nbFics
    [nomDir, nomFicSimple] = fileparts(nomFic);
    nomFicSimple2 = sprintf('%s_%02d.s7k', nomFicSimple, iFic);
    nomFicOut = fullfile(nomDir, nomFicSimple2);
    
    subDepth = (1:nbPingMax) + (iFic-1) * nbPingMax;
    subDepth(subDepth > NbRecorsDepth) = [];
    subRecords = find_records_utiles(subDataDepth(subDepth), Date, Heure, TypeDatagram);
    
    fidOut = fopen(nomFicOut, 'w+', 'l');
    if fidOut == -1
        str = [Lang('Impossible de cr�er le fichier', 'Impossible to create the file') nomFicOut];
        my_warndlg(str, 1);
        return
    end
    
    for k=1:length(subRecords)
        fseek(fid, Position(subRecords(k)), 'bof');
        X = fread(fid, Taille(subRecords(k)), 'uint8');
    
        fwrite(fidOut, X, 'uint8');
%     PositionCourante = ftell(fid);
%     fseek(fid, PositionCourante, 'bof');

        my_waitbar(iFic, nbFics, hw);
    end
    fclose(fidOut);
end
my_close(hw, 'MsgEnd');
fclose(fid);

%% Renommage du fichier .all et .wcd si le fichier a bien �t� impact�
    
nomFicNew = fullfile(nomDir, [nomFicSimple '.s7kSplitted']);
[status, message] = movefile(nomFic, nomFicNew, 'f');
if status
    strFR = sprintf('Le fichier "%s" a �t� renomm� en "%s"', nomFic, nomFicNew);
    strUS = sprintf('File "%s" was renamed as "%s"', nomFic, nomFicNew);
    my_warndlg(Lang(strFR,strUS), 0, 'Tag', 'ImpossibleDeRenommerCeFichier');
else
    strFR = sprintf('Echec de renommage de "%s" en "%s", faites le � la main : \nMessage matlab : %s', nomFic, nomFicNew, message);
    strUS = sprintf('It was impossible to rename "%s" in "%s", do it manualy : \nMatlab message : %s', nomFic, nomFicNew, message);
    my_warndlg(Lang(strFR,strUS), 0, 'Tag', 'ImpossibleDeRenommerCeFichier');
end


function subRecordsTotal = find_records_utiles(subDepth, Date, Heure, TypeDatagram)

tDepth = cl_time('timeIfr', Date(subDepth), Heure(subDepth));
tDepth = tDepth.timeMat;
% [tDepthMin, indexDepthMin] = min(tDepth);
% [tDepthMax, indexDepthMax] = max(tDepth);

listeTypeDatagram = unique(TypeDatagram);

subRecordsTotal = [];
for k=1:length(listeTypeDatagram)
    sub = find(TypeDatagram == listeTypeDatagram(k));
    if length(sub) == 1
        subRecords = sub;
    else
        tSub = cl_time('timeIfr', Date(sub), Heure(sub));
        tSub = tSub.timeMat;

        subInterieur = find((tSub >= min(tDepth)) & (tSub <= max(tDepth)));
        if isempty(subInterieur)
            subRecords = sub;
        else
            if subInterieur(1) == 1
                iDeb = [];
            else
                iDeb = (subInterieur(1) - 1);
            end
            
            if subInterieur(end) == length(sub)
                iFin = [];
            else
                iFin = (subInterieur(end) + 1);
            end
            subRecords = sub([iDeb subInterieur iFin]);
        end
    end
    subRecordsTotal = [subRecordsTotal subRecords]; %#ok<AGROW>
end
subRecordsTotal = sort(subRecordsTotal);
