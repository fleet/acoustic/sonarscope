function [flag, Values, strValues, Carto] = summary_Lines(this, strHeaderAllFields, Carto, varargin)

global isUsingParfor %#ok<GVMIS>

[varargin, isUsingParfor] = getPropertyValue(varargin, 'isUsingParfor', isUsingParfor); %#ok<ASGLU>

flag = 0;
Values    = {};
strValues = {};

identSondeur = selectionSondeur(this);
if isempty(identSondeur)
    return
end
nomFicS7k = this.nomFic;
[nomDir, nomFic] = fileparts(nomFicS7k);

%% 7K Settings

Data7000 = read_sonarSettings(this, identSondeur);
if isempty(Data7000)
    WCRangeSelectionMedian = NaN;
    nbWCRangeSelection = NaN;
    
    WCPowerSelectionMedian = NaN;
    nbWCPowerSelection = NaN;
    
    WCGainSelectionMedian = NaN;
    nbWCGainSelection = NaN;
    nbPingSeq = repmat(-1, 1, 5);
else
    WCRangeSelectionMedian = nanmode(Data7000.RangeSelection);
    nbWCRangeSelection = length(unique(Data7000.RangeSelection));
    
    WCPowerSelectionMedian = nanmode(Data7000.PowerSelection);
    nbWCPowerSelection = length(unique(Data7000.PowerSelection));
    
    WCGainSelectionMedian = nanmode(Data7000.GainSelection);
    nbWCGainSelection = length(unique(Data7000.GainSelection));
    nbPingSeq = my_hist(Data7000.PingSequence, 0:4);
end
Frequency = Data7000.Frequency;
Frequency = mode(Frequency(Frequency ~= 0)) / 1000;

%% Lecture des layers

[c, Carto] = view_depth(this, 'ListeLayers', [1 2 4 8 30], 'Carto', Carto, 'Bits', [1 2 6]); % 28 remplac� par 30 (200% remplac� par 100%) le 20/03/2017
if isempty(c)
    return
end

identLayerBathy = cl_image.indDataType('Bathymetry');
[indLayerBathy, nomLayer] = findIndLayerSonar(c, 1, 'DataType', identLayerBathy, 'WithCurrentImage', 1); %#ok<ASGLU> %, 'Tag', []); %#ok<ASGLU>

identLayerAcrossDist = cl_image.indDataType('AcrossDist');
[indLayerAcrossDist, nomLayer] = findIndLayerSonar(c, 1, 'DataType', identLayerAcrossDist, 'WithCurrentImage', 1); %#ok<ASGLU> %, 'Tag', []); %#ok<ASGLU>

identLayerTxAngle = cl_image.indDataType('TxAngle');
[indLayerTxAngle, nomLayer] = findIndLayerSonar(c, 1, 'DataType', identLayerTxAngle, 'WithCurrentImage', 1);%#ok<ASGLU> %, 'Tag', []); %#ok<ASGLU>

identLayerIfremerQF = cl_image.indDataType('IfremerQF');
[indLayerIfremerQF, nomLayer] = findIndLayerSonar(c, 1, 'DataType', identLayerIfremerQF, 'WithCurrentImage', 1);%#ok<ASGLU> %, 'Tag', []); %#ok<ASGLU>

identLayerReflectivity = cl_image.indDataType('Reflectivity');
[indLayerReflectivity, nomLayer] = findIndLayerSonar(c, 1, 'DataType', identLayerReflectivity, 'WithCurrentImage', 1);%#ok<ASGLU> %, 'Tag', []); %#ok<ASGLU>

%% Depth

if isempty(c)
    return
end

Roll = get(c(1), 'Roll');
valStats = stats(Roll(:));
RollMean = valStats.Moyenne;
RollStd  = valStats.Sigma;

Pitch = get(c(1), 'Pitch');
valStats = stats(Pitch(:));
PitchMean = valStats.Moyenne;
PitchStd  = valStats.Sigma;

Heave = get(c(1), 'Heave');
valStats = stats(Heave(:));
HeaveMean = valStats.Moyenne;
HeaveStd  = valStats.Sigma;

Heading = get(c(1), 'Heading');
valStats = stats(Heading(:));
HeadingMedian = valStats.Mediane;

SonarFrequency = get(c(1), 'SonarFrequency');
if all(SonarFrequency == 0)
    SonarFrequency(:) = Frequency;
end
SonarFrequencyUnique = unique(SonarFrequency);
MultiPingSequence = length(SonarFrequencyUnique);

% RecurrenceInterPings

T = get(c(1), 'SonarTime');
t = datetime(T.timeMat, 'ConvertFrom', 'datenum');
if MultiPingSequence == 4
    t(SonarFrequency ~= SonarFrequencyUnique(1)) = [];
end
RecurrenceInterPings = t(2:end) - t(1:(end-1));
RecurrenceInterPings = median(RecurrenceInterPings);

if isempty(indLayerBathy)
    DepthMean = NaN;
    DepthMin  = NaN;
    DepthMax  = NaN;
else
    DepthStats = c(indLayerBathy).StatValues;
    DepthMean = DepthStats.Moyenne;
    DepthMin  = DepthStats.Min;
    DepthMax  = DepthStats.Max;
end

if isempty(indLayerAcrossDist)
    SwathWidth = NaN;
else
    AcrossDist = get(c(2), 'Image');
    minAcrossDist = min(AcrossDist, [], 2);
    maxAcrossDist = max(AcrossDist, [], 2);
    % FigUtils.createSScFigure; PlotUtils.createSScPlot(-XBab, 'r'); hold on; grid on; plot(XTri, 'g');
    PingRange  = maxAcrossDist - minAcrossDist;
    SwathWidth = median(PingRange, 'omitnan');
end

if isempty(indLayerTxAngle)
    AngularRange = NaN;
else
    Angles    = get(c(indLayerTxAngle), 'Image');
    minAngles = min(Angles, [], 2);
    maxAngles = max(Angles, [], 2);
    minAngles = mean(minAngles, 'omitnan');
    maxAngles = mean(maxAngles, 'omitnan');
    AngularRange = maxAngles - minAngles;
end

SurfaceSoundSpeed = mean(get(c(1), 'SonarSurfaceSoundSpeed'), 'omitnan');

% nbSonarSurfaceSoundSpeed = NaN;

nbPings = c(1).nbRows;
if nbPings == 0
    str1 = sprintf('Le fichier "%s" est vide.', nomFicS7k);
    str2 = sprintf('File "%s" is empty.', nomFicS7k);
    my_warndlg(Lang(str1,str2), 0);
    return
end
nbBeams = c(1).nbColumns;

LatitudeMedian  = median(get(c(1), 'SonarFishLatitude'), 'omitnan');
LongitudeMedian = median(get(c(1), 'SonarFishLongitude'), 'omitnan');

try
    % La navigation peut-�tre vide.
    [flag, Z] = read_Invite(this, identSondeur, 'Distance Inter Pings', 0);
    if flag
        InterPingDistance = mean(Z.Data, 'omitnan');
        LineLength = sum(Z.Data, 'omitnan');
        LineArea   = sum(PingRange .* Z.Data(:), 'omitnan');
    else
        InterPingDistance = NaN;
        LineLength        = NaN;
        LineArea          = NaN;
    end
catch
    InterPingDistance = NaN;
    LineLength        = NaN;
    LineArea          = NaN;
end


if ~isnan(HeadingMedian)
    DistanceInterBeams = SwathWidth / nbBeams;
    [typeLine, strDirection] = classifyProfileFromHeading(Heading(:), ...
        'DistanceInterPings', InterPingDistance, 'DistanceInterBeams', DistanceInterBeams);
else
    typeLine      = '';
    strDirection  = '';
end

%{
SonarImmersion         <-> -5.99 * ones(497,1) (m)
SonarHeight            <-> 497*1 vals from -568.8033 to -260.9796 (m)
SonarHeave             <-> 497*1 vals from -0.037176 to 0.045546 (deg)
SonarTide              <-> 0 * ones(497,1) (m)
SonarSampleFrequency   <-> 497*1 vals from 376.3926 to 3011.1411 (Hz)
%}

SonarBathyCel = get(c(1), 'SonarBathyCel');
NbSoundSpeedProfiles = size(SonarBathyCel, 1);

SonarSampleFrequency = get(c(1), 'SonarSampleFrequency');
SnippetsSamplingRateMedian = nanmode(SonarSampleFrequency);
nbSnippetsSamplingRates = length(unique(SonarSampleFrequency));

if isempty(indLayerIfremerQF)
    MeanQF = NaN;
else
    valStats = c(indLayerIfremerQF).StatValues;
    MeanQF = valStats.Moyenne;
end

if isempty(indLayerReflectivity)
    MeanBS = NaN;
else
    valStats = c(indLayerReflectivity).StatValues;
    MeanBS = valStats.Mediane;
end

%% Lectures des infos de WC

[flag, DataWC] = read_WaterColumnData(this, identSondeur, 'subPing', 'None');
if flag
    if isfield(DataWC, 'subWC')
        nbPingsWC = length(DataWC.subWC);
    else
        % TODO : Detect� sur
        % G:\TVISTOUL\CARTOGRAPHIE\20141119_153802_PP_7150_24kHz.s7k
        % Voir pourquoi
        nbPingsWC = 0;
    end
else
    nbPingsWC = 0;
end

TxPulseWidthMedian = median(DataWC.TxPulseWidth, 'omitnan') * 1000;
nbPulseLength = length(unique(DataWC.TxPulseWidth));

AbsorptionMedian  = mean(DataWC.Absorption / 10, 'omitnan');

BottomDetectDepthFilterFlag = median(DataWC.BottomDetectDepthFilterFlag, 'omitnan');
%{
% Comment� pour l'instant car il faut aller lire la valeur dans chaque
datagramme de WC. On attend la refonte pour avoir un acc�s direct � cette
information
WCSamplingRateMedian = median(DataWC.SampleRate, 'omitnan');
nbWCSamplingRates = length(unique(DataWC.SampleRate));
%}

% TODO : rajouter l'ouverture angulaire

%{
ReceiverBandwith: [1x172 double]
TxPulseWidth: [1x172 double]
TxPulseTypeIdentifier: [1x172 double]
TxPulseEnvelopeParameter: [1x172 double]
TxPulseReserved: [1x172 double]
MaxPingRate: [1x172 double]
PingPeriod: [1x172 double]
ControlFlags: [1x172 double]
ProjectorBeamSteeringAngleVertical: [1x172 double]
ProjectorBeamSteeringAngleHorizontal: [1x172 double]
ProjectionBeam_3dB_WidthVertical: [1x172 double]
ProjectionBeam_3dB_WidthHorizontal: [1x172 double]
ProjectionBeamFocalPoint: [1x172 double]
ProjectionBeamWeightingWindowType: [1x172 double]
ProjectionBeamWeightingWindowParameter: [1x172 double]
TransmitFlags: [1x172 double]
HydrophoneMagicNumber: [1x172 double]
ReceiveBeamWeightingWindow: [1x172 double]
ReceiveBeamWeightingParameter: [1x172 double]
ReceiveFlags: [1x172 double]
ReceiveBeamWidth: [1x172 double]
BottomDetectionFilterInfoMinRange: [1x172 double]
BottomDetectionFilterInfoMaxRange: [1x172 double]
BottomDetectionFilterInfoMinDepth: [1x172 double]
BottomDetectionFilterInfoMaxDepth: [1x172 double]
BottomDetectRangeFilterFlag: [1x172 double]
BottomDetectDepthFilterFlag: [1x172 double]
Spreading: [1x172 double]
TxRollCompensationFlag: [1x172 double]
TxRollCompensationValue: [1x172 double]
TxPitchCompensationFlag: [1x172 double]
TxPitchCompensationValue: [1x172 double]
RxRollCompensationFlag: [1x172 double]
RxRollCompensationValue: [1x172 double]
RxPitchCompensationFlag: [1x172 double]
RxPitchCompensationValue: [1x172 double]
RxHeaveCompensationFlag: [1x172 double]
RxHeaveCompensationValue: [1x172 double]
BeamDescriptor: [174x1 single]
BeginSampleDescriptor: [174x1 single]
CenterSampleDistance: [174x1 single]
BeamAcrossTrackDistance: [174x1 single]
MinFilterInfo: [174x1 single]
MaxFilterInfo: [174x1 single]
%}

%% Lectures des infos de Mag&Phase

if this.ExistGrpBeamRawData
    DataMP = read_BeamRawData(this, identSondeur, 'subPing', 'None', 'flagMessage', 0);
    if isempty(DataMP)
        nbPingsMP = 0;
    else
        if DataMP.isEmpty7018
            nbPingsMP = 0;
        else
            if isfield(DataMP, 'subWC')
                nbPingsMP = length(DataMP.subWC);
            else
                nbPingsMP = 0;
                % TODO mais aussi d�tect� sur le meme fichier posant pb pour WC
            end
        end
    end
else
    nbPingsMP = 0;
end

%% Lecture de la navigation

DataNav = read_navigation(this, identSondeur);
if isfield(DataNav, 'Speed')
    SpeedMedian = median(DataNav.Speed, 'omitnan');
else
    [~, Speed] = calCapFromLatLon(DataNav.Latitude, DataNav.Longitude, 'Time', DataNav.Time);
    SpeedMedian = median(Speed, 'omitnan');
end
SpeedMedian = SpeedMedian * (3600/1852);
%{
Latitude: [1x2607 double]
Longitude: [1x2607 double]
CourseVessel: [1x2607 single]
Heading: [1x2607 single]
VesselHeight: [1x2607 single]
HeightAccuracy: [1x2607 single]
HorizontalPositionAccuracy: [1x2607 single]
SystemSerialNumber: 7003
VerticalReference: 1
Time: [1x1 cl_time]
%}

%% Prepare the information data

Values    = cell(length(strHeaderAllFields), 1);
strValues = cell(length(strHeaderAllFields), 1);
kField = 1;

% Directory
Values{kField}    = nomDir;
strValues{kField} = nomDir;
kField = kField + 1;

% File name
Values{kField}    = nomFic;
strValues{kField} = nomFic;
kField = kField + 1;

% 'Sounder name'
Values{kField}    = identSondeur;
strValues{kField} = num2str(identSondeur);
kField = kField + 1;

% 'Nb pings'
Values{kField}    = nbPings;
strValues{kField} = num2str(nbPings);
kField = kField + 1;

% 'Nb beams'
Values{kField}    = nbBeams;
strValues{kField} = num2str(nbBeams);
kField = kField + 1;

% Main frequency (kHz)
Values{kField}    = nanmode(SonarFrequency);
strValues{kField} = num2str(nanmode(SonarFrequency));
kField = kField + 1;

% NbSwaths
Values{kField}    = MultiPingSequence;
strValues{kField} = num2str(MultiPingSequence);
kField = kField + 1;

% Depth (mean)
Values{kField}    = DepthMean;
strValues{kField} = num2str(DepthMean, '%0.2f');
kField = kField + 1;

% Depth (MIN)
Values{kField}    = DepthMin;
strValues{kField} = num2str(DepthMin, '%0.2f');
kField = kField + 1;

% Depth (MAX)
Values{kField}    = DepthMax;
strValues{kField} = num2str(DepthMax, '%0.2f');
kField = kField + 1;

% swath width (m)
Values{kField}    = SwathWidth;
strValues{kField} = num2str(SwathWidth, '%0.1f');
kField = kField + 1;

% Angular range (deg)
Values{kField}    = AngularRange;
strValues{kField} = num2str(AngularRange, '%0.1f');
kField = kField + 1;

% Swath width on Depth
Values{kField}    = abs(SwathWidth / DepthMean);
strValues{kField} = num2str(abs(SwathWidth / DepthMean), '%0.1f');
kField = kField + 1;

% Inter ping distance (m)
Values{kField}    = InterPingDistance;
strValues{kField} = num2str(InterPingDistance, '%0.2f');
kField = kField + 1;

% Recurrence inter ping (s)
Values{kField}    = seconds(RecurrenceInterPings);
strValues{kField} = num2str(seconds(RecurrenceInterPings), '%5.2f');
kField = kField + 1;

% Line length (m)
Values{kField}    = LineLength;
strValues{kField} = num2str(round(LineLength));
kField = kField + 1;

% Surface couverte de la ligne (km2)
Values{kField}    = LineArea * 1e-6;
strValues{kField} = num2str(LineArea * 1e-6, '%5.2f');
kField = kField + 1;

% Mean QF
Values{kField}    = MeanQF;
strValues{kField} = num2str(MeanQF, '%0.1f');
kField = kField + 1;

% Mean of backscatter
Values{kField}    = MeanBS;
strValues{kField} = num2str(MeanBS, '%0.1f');
kField = kField + 1;

% WC
Values{kField}    = length(find(nbPingsWC ~= 0));
strValues{kField} = num2str(length(find(nbPingsWC ~= 0)));
kField = kField + 1;

% Mag&Phase
Values{kField}    = length(find(nbPingsMP ~= 0));
strValues{kField} = num2str(length(find(nbPingsMP ~= 0)));
kField = kField + 1;

% Heading (median)
Values{kField}    = HeadingMedian;
strValues{kField} = num2str(HeadingMedian, '%0.0f');
kField = kField + 1;

Values{kField}    = strDirection;
strValues{kField} = strDirection;
kField = kField + 1;

Values{kField}    = typeLine;
strValues{kField} = typeLine;
kField = kField + 1;

% Speed (median)
Values{kField}    = SpeedMedian;
strValues{kField} = num2str(SpeedMedian, '%0.1f');
kField = kField + 1;

% NbSoundSpeedProfiles
Values{kField}    = NbSoundSpeedProfiles;
strValues{kField} = num2str(NbSoundSpeedProfiles);
kField = kField + 1;

% Sound velocity (m/s)
Values{kField}    = SurfaceSoundSpeed;
strValues{kField} = num2str(SurfaceSoundSpeed, '%0.1f');
kField = kField + 1;

% Absorption (dB/km)
Values{kField}    = AbsorptionMedian;
strValues{kField} = num2str(AbsorptionMedian);
kField = kField + 1;

% Latitude
Values{kField}    = LatitudeMedian;
strValues{kField} = lat2str(LatitudeMedian);
kField = kField + 1;

% Longitude
Values{kField}    = LongitudeMedian;
strValues{kField} = lon2str(LongitudeMedian);
kField = kField + 1;

% Nb SoundSpeedProfiles
% Values{kField}    = SonarSurfaceSoundSpeed;
% strValues{kField} = num2str(SonarSurfaceSoundSpeed);
% kField = kField + 1;

% Roll (mean)
Values{kField}    = RollMean;
strValues{kField} = num2str(RollMean, '%5.2f');
kField = kField + 1;

% Roll (std)
Values{kField}    = RollStd;
strValues{kField} = num2str(RollStd, '%5.2f');
kField = kField + 1;

% Pitch (mean)
Values{kField}    = PitchMean;
strValues{kField} = num2str(PitchMean, '%5.2f');
kField = kField + 1;

% Pitch (std)
Values{kField}    = PitchStd;
strValues{kField} = num2str(PitchStd, '%5.2f');
kField = kField + 1;

% Heave (mean)
Values{kField}    = HeaveMean;
strValues{kField} = num2str(HeaveMean, '%5.2f');
kField = kField + 1;

% Heave (std)
Values{kField}    = HeaveStd;
strValues{kField} = num2str(HeaveStd, '%5.2f');
kField = kField + 1;

% Pulse length (ms)
Values{kField}    = TxPulseWidthMedian;
strValues{kField} = num2str(TxPulseWidthMedian);
kField = kField + 1;

% Nb Pulse lengths
Values{kField}    = nbPulseLength;
strValues{kField} = num2str(nbPulseLength);
kField = kField + 1;

% Resolution (m)
Values{kField}    = 1500 * (TxPulseWidthMedian * 1e-3) / 2;
strValues{kField} = num2str(1500 * (TxPulseWidthMedian * 1e-3) / 2, '%0.2f');
kField = kField + 1;

% Snippets sampling rate (kHz - median)
SnippetsSamplingRateMedian = SnippetsSamplingRateMedian * 1e-3 * 1e-3; % TODO pourquoi 2 fois ?
Values{kField}    = SnippetsSamplingRateMedian; % TODO
strValues{kField} = num2str(SnippetsSamplingRateMedian, '%0.2f');
kField = kField + 1;

% Nb Snippets sampling rates
Values{kField}    = nbSnippetsSamplingRates;
strValues{kField} = num2str(nbSnippetsSamplingRates);
kField = kField + 1;

%{
% Comment� pour l'instant car il faut aller lire la valeur dans chaque
datagramme de WC. On attend la refonte pour avoir un acc�s direct � cette
information
% WC sampling rate (Hz - median)
Values{kField}    = WCSamplingRateMedian;
strValues{kField} = num2str(WCSamplingRateMedian);
kField = kField + 1;

% Nb Snippets sampling rates
Values{kField} =(nbWCSamplingRates;
strValues{kField} = num2str(nbWCSamplingRates);
kField = kField + 1;
%}

% Range selection (m - median)
Values{kField}    = WCRangeSelectionMedian;
strValues{kField} = num2str(WCRangeSelectionMedian);
kField = kField + 1;

% Nb Range selection
Values{kField}    = nbWCRangeSelection;
strValues{kField} = num2str(nbWCRangeSelection);
kField = kField + 1;

% Power selection (m - median)
Values{kField}    = WCPowerSelectionMedian;
strValues{kField} = num2str(WCPowerSelectionMedian);
kField = kField + 1;

% Nb Power selection
Values{kField}    = nbWCPowerSelection;
strValues{kField} = num2str(nbWCPowerSelection);
kField = kField + 1;

% Gain selection (dB - median)
Values{kField}    = WCGainSelectionMedian;
strValues{kField} = num2str(WCGainSelectionMedian);
kField = kField + 1;

% Nb Gain selection
Values{kField}    = nbWCGainSelection;
strValues{kField} = num2str(nbWCGainSelection);
kField = kField + 1;


%{
% Nb pings monoping
Values{kField} = '';
strValues{kField} = '';
kField = kField + 1;

% Nb pings multiping
Values{kField} = '';
strValues{kField} = '';
kField = kField + 1;
%}

% Nb pings with WC
Values{kField}    = nbPingsWC;
strValues{kField} = num2str(nbPingsWC);
kField = kField + 1;

% Nb pings without WC
Values{kField}    = nbPings - nbPingsWC;
strValues{kField} = num2str(nbPings - nbPingsWC);
kField = kField + 1;

% Nb pings with Mag&Phase
Values{kField}    = nbPingsMP;
strValues{kField} = num2str(nbPingsMP);
kField = kField + 1;

% Nb pings without Mag&Phase
Values{kField}    = nbPings - nbPingsMP;
strValues{kField} = num2str(nbPings - nbPingsMP);
kField = kField + 1;

% BottomDetectDepthFilterFlag
Values{kField}    = BottomDetectDepthFilterFlag;
strValues{kField} = num2str(BottomDetectDepthFilterFlag);
kField = kField + 1;

% Nombre de pings sequence

for k2=1:5
    Values{kField}    = nbPingSeq(k2);
    strValues{kField} = num2str(nbPingSeq(k2));
    kField = kField + 1;
end

% File size
Values{kField}    = sizeFic(nomFicS7k);
strValues{kField} = num2strMoney(sizeFic(nomFicS7k));
kField = kField + 1;

% Time beginning, end and duration
T = get(c(1), 'SonarTime');
format longG
t = datetime(T.timeMat, 'ConvertFrom', 'datenum');

Values{kField}    = t(1);
strValues{kField} = char(t(1));
kField = kField + 1;

% Time end
Values{kField}    = t(end);
strValues{kField} = char(t(end));
kField = kField + 1;

% Duration
Duration = t(end) - t(1);
Values{kField}    = minutes(Duration);
strValues{kField} = char(Duration);
kField = kField + 1; %#ok<NASGU>

format long
flag = 1;
