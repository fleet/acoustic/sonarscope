% Cr�ation d'une image PingSamples � partir d'un fichier .s7k
%
% Syntax
%   b = view_BeamRawData(s7k, ...);
%
% Input Arguments
%   s7k : Instance de cl_simrad_all
%
% Name-Value Pair Arguments
%   subPing : Numeros de lignes � traiter
%
% Name-only Arguments
%   FaisceauxPairs  : Pour cr�er une image uniquement avec les faisceaux pairs
%   FaisceauxImairs : Pour cr�er une image uniquement avec les faisceaux impairs
%
% Output Arguments
%   b          : Instance de cli_image
%   subl_Image : Numeros de lignes communs avec la bathy (index relatis � seabedImage)
%   subl_Depth : Numeros de lignes communs avec la bathy (index relatis � Depth)
%
% Examples
%   b = view_BeamRawData(s7k);
%   SonarScope(b);
%
%   b = view_BeamRawData(s7k, 'subPing', 1:100);
%   SonarScope(b);
%
% See also cl_simrad_all cl_simrad_all/plot_position cl_simrad_all/plot_nav Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [b, Carto, SonarDescription] = view_BeamRawData(this, varargin)

persistent persistent_IdentSonar

b = [];
[varargin, DisplayLevel]           = getPropertyValue(varargin, 'DisplayLevel',            1);
[varargin, Carto]                  = getPropertyValue(varargin, 'Carto',                  []);
[varargin, nomFicNav]              = getPropertyValue(varargin, 'NavFileName',            []);
[varargin, nomFicIP]               = getPropertyValue(varargin, 'nomFicIP',               []);
[varargin, InstallationParameters] = getPropertyValue(varargin, 'InstallationParameters', []);
[varargin, Navigation]             = getPropertyValue(varargin, 'Navigation',             []);
[varargin, flagMessage]            = getPropertyValue(varargin, 'flagMessage',             1);
[varargin, SonarDescription]       = getPropertyValue(varargin, 'SonarDescription',       []);
[varargin, DataDepth]              = getPropertyValue(varargin, 'DataDepth',              []);
[varargin, DataIndex]              = getPropertyValue(varargin, 'DataIndex',              []);
[varargin, identSondeur]           = getPropertyValue(varargin, 'identSondeur',           []);
[varargin, Mute]                   = getPropertyValue(varargin, 'Mute',                   0);

S0 = cl_sounder([]);

%% Check the number of elements

flag = checkIfOneElement(length(this));
if ~flag
    return
end

%%

[varargin, subPing] = getPropertyValue(varargin, 'subPing', []); %#ok<ASGLU>

if isempty(identSondeur)
    identSondeur = selectionSondeur(this);
end
if isempty(DataIndex)
    [flag, DataIndex] = readFicIndex(this, identSondeur);
    if ~flag
        return
    end
end

if isempty(InstallationParameters)
    InstallationParameters = read_sonarInstallationParameters(this, identSondeur, ...
        'DataIndex', DataIndex, 'Mute', Mute);
end

%% Lecture du fichier de navigation

if isempty(Navigation)
    if isempty(nomFicNav)
        LatNav     = [];
        LonNav     = [];
        TimeNav    = [];
        HeadingNav = [];
    else
        if iscell(nomFicNav)
            if length(nomFicNav) > 1
                str1 = 'Il n''est pas encore pr�vu de pouvoir utiliser plusieurs fichiers de nav en m�me temps, d�sol�. Si vous avez vraiment besoin de cela faites moi en la demande, sign� JMA.';
                str2 = 'It is not possible to use several navigation files for the moment. If you really need it, please tell me, signed JMA.';
                my_warndlg(Lang(str1,str2), 1);
                return
            end
            nomFicNav = nomFicNav{1};
        end
        [flag, LatNav, LonNav, TimeNav, HeadingNav] = lecFicNav(nomFicNav);
        if ~flag
            LatNav     = [];
            LonNav     = [];
            TimeNav    = [];
            HeadingNav = [];
        end
        
        % figure; plot(LonNav, LatNav); grid on
    end
else
    LatNav     = Navigation.LatNav;
    LonNav     = Navigation.LonNav;
    TimeNav    = Navigation.TimeNav;
    HeadingNav = Navigation.HeadingNav;
end

%% Lecture du fichier "InstallationParameters"

[flag, InstallationParameters] = get_InstallationParameters(this, InstallationParameters, nomFicIP);
if ~flag
    return
end

%%
% subPing = 1:50:1000

[Data, BeamDataOut, subPing] = read_BeamRawData(this, identSondeur, 'subPing', subPing, ...
    'DisplayLevel', DisplayLevel, 'flagMessage', flagMessage, 'DataDepth', DataDepth, 'DataIndex', DataIndex);
% if isempty(Data) || isempty(BeamDataOut) || ~isfield(BeamDataOut(1).RD, 'Amplitude')
%     return
% end

if isempty(Data) || isempty(BeamDataOut) || ~isfield(BeamDataOut(1), 'RD')...
        || isempty(BeamDataOut(1).RD) || ~isfield(BeamDataOut(1).RD, 'Amplitude')
    return
end


%{
switch Data.SystemSerialNumber
case 7111
AbsorptionCoefficient = 35;
case 7150
if Data.Frequency(1) < 18000
AbsorptionCoefficient = 1.1; %DataPing.Absorption; % dB : Attention, valeur rentr�e � la main
else
AbsorptionCoefficient = 4;
end
end
% [Alpha, AlphaSum] = AttenuationGarrison(Data.Frequency(1)/1000, 0:500:6000, 12, 35)
%}

DataTVG = read_TVG(this, identSondeur, 'subPing', subPing, 'DataIndex', DataIndex);

DataSoundSpeedProfile = []; % read_soundSpeedProfile(this, identSondeur);
Data7004 = read_7kBeamGeometry(this, identSondeur, 'DataIndex', DataIndex);
% % figure; plot(Data7004.BeamGeometry(1).RD.BeamHorizontalDirectionAngle); grid on
% figure; plot(Data7004.BeamHorizontalDirectionAngle(1,:)); grid on

N = length(subPing);
% hw = create_waitbar('Loading Amplitude & Phase files : 2/2', 'N', N);
b = repmat(cl_image,2,N);
str1 = 'Lecture de l''amplitude et de la phase';
str2 = 'Reading magnitude and phase';
strLoop = Lang(str1,str2);
if N ~= 1
    my_warndlg([strLoop ' : Begin'], 0);
end
for k=1:N
    %     my_waitbar(k, N, hw)
    infoLoop(strLoop, k, N, 'pings')
    
    iPing = subPing(k);
    
    DataPing = Extraction(Data, iPing);
%     DataPing.TxAngle = Data7004.BeamGeometry(iPing).RD.BeamHorizontalDirectionAngle;
    DataPing.TxAngle = Data7004.BeamHorizontalDirectionAngle(iPing,:);
    
    if ~isempty(Navigation) || ~isempty(nomFicNav)
        Longitude = my_interp1_longitude(TimeNav, LonNav,     DataPing.Time, 'linear', 'extrap');
        Latitude  = my_interp1(          TimeNav, LatNav,     DataPing.Time, 'linear', 'extrap');
        Heading   = my_interp1(          TimeNav, HeadingNav, DataPing.Time, 'linear', 'extrap');
        DataPing.Latitude  = Latitude;
        DataPing.Longitude = Longitude;
        DataPing.Heading   = Heading;
    end
    
    Amplitude = BeamDataOut(k).RD.Amplitude;
    Phase     = BeamDataOut(k).RD.Phase;
    
    SampleRate = DataPing.SampleRate; % Hz
    DataPing.DataTVG = DataTVG;
    
    %     DataPing.R1SamplesFiltre = BeamDataOut(k).RD.Range * SampleRate / (1500/2);
    DataPing.R1SamplesFiltre = BeamDataOut(k).RD.Range * SampleRate; % Ajout JMA le 20/03/2016
    
    % Cr�ation de l'instance de classe
    
    IdentSonar = find_IdentSonar(S0, num2str(DataPing.SystemSerialNumber), ...
        'Frequency', DataPing.Frequency/1000);
    if isempty(persistent_IdentSonar) || isempty(SonarDescription) || (IdentSonar ~= persistent_IdentSonar)
        SonarDescription = cl_sounder('Sonar.Ident', IdentSonar, 'Sonar.Mode_1', 1, ...
            'Sonar.SystemSerialNumber', DataPing.SystemSerialNumber);
    end
    persistent_IdentSonar = IdentSonar;
    
    [~, TagSynchro] = fileparts(this.nomFic);
    %     NomImage = sprintf('%s_%d', TagSynchro, BeamDataOut(k).iPing);
%     TagSynchro = sprintf('%s_%d', TagSynchro, BeamDataOut(k).iPing);
    TagSynchro = sprintf('%s_%d', TagSynchro, iPing);
    NomImage = TagSynchro;
    
    b(1,k) = cl_image;
    if isa(Amplitude, 'cl_memmapfile')
        b(1,k) = set_Image(b(1,k), Amplitude);
    else
        if N > 1
            b(1,k) = set_Image(b(1,k), cl_memmapfile('Value', Amplitude, 'LogSilence', 1));
        else
            b(1,k) = set_Image(b(1,k), Amplitude, 'NoMemmapfile');
        end
    end
    
    b(1,k).x                 = 1:size(Amplitude,2);
    b(1,k).y                 = 1:size(Amplitude,1);
    b(1,k).YDir              = 2;
    b(1,k).Name              = NomImage;
    % b(1,k.InitialImageName = nomLayer;
    b(1,k).Unit              = 'Amp';
    b(1,k).TagSynchroX       = TagSynchro;
    b(1,k).TagSynchroY       = TagSynchro;
    b(1,k).XUnit             = 'Beam #';
    b(1,k).YUnit             = 'Sample #';
    b(1,k).InitialFileName   = this.nomFic;
    b(1,k).InitialFileFormat = 'RESONs7k';
    b(1,k).Comments          = sprintf('Reson PingCounter : %d', DataPing.PingCounter);
    b(1,k).GeometryType      = cl_image.indGeometryType('SampleBeam');
    b(1,k) = set_SonarDescription(b(1,k), SonarDescription);
    %     b(1,k).Comments    = Comments{ListeLayers(k)};
    %     b(1,k).strMaskBits = strMaskBits;
    
    b(1,k) = set_SampleBeamData(b(1,k), DataPing);
    
    b(1,k).DataType = cl_image.indDataType('Reflectivity');
    b(1,k).ColormapIndex = 3;
    
    TempsPropagation = (1:size(Amplitude,1)) / SampleRate;
    %     b(1,k) = set_SonarTime(b(1,k), DataPing.Time+TempsPropagation / (24*3600));
    b(1,k) = set_SonarTime(b(1,k), TempsPropagation(:));
    %     cl_time('timeMat', DataPing.Time+TempsPropagation)
    
    if ~isempty(DataSoundSpeedProfile)
        SoundSpeedProfile.Z = DataSoundSpeedProfile.Depth;
        SoundSpeedProfile.T = DataSoundSpeedProfile.WaterTemperature;
        SoundSpeedProfile.S = DataSoundSpeedProfile.Salinity;
        SoundSpeedProfile.C = DataSoundSpeedProfile.SoundSpeed;
        b(1,k) = set_SonarBathyCel(b(1,k), SoundSpeedProfile);
    end
    
    if ~isempty(InstallationParameters)
        b(1,k) = set_ResonInstallationParameters(b(1,k), InstallationParameters);
    end
    
    b(1,k) = update_Name(b(1,k));
    b(1,k).Writable = false;
    
    %     b(1,k) = compute_stats(b(1,k));
    %     b(1,k).TagSynchroContrast = num2str(rand(1));
    %     StatValues = b(1,k).StatValues;
    %     CLim = [StatValues.Min StatValues.Max];
    %     b(1,k).CLim = CLim;
    
    % ----------------------
    % Definition d'une carto
    
    while isempty(Carto)
        %         Carto = getDefinitionCarto(b(1,k), 'NoGeodetic', 1);
        Carto = getDefinitionCarto(b(1,k), 'NoGeodetic', 0);
    end
    b(1,k) = set_Carto(b(1,k), Carto);
    
    
    %     b(2,k) = set_Image(b(1,k), cl_memmapfile('Value',
    %     BeamDataOut(k).RD.Phase(subl,:), 'LogSilence', 1));    Phase = BeamDataOut(k).RD.Phase(subl,:);
    if N > 1
        b(2,k) = set_Image(b(1,k), cl_memmapfile('Value', Phase, 'LogSilence', 1));
    else
        b(2,k) = set_Image(b(1,k), Phase, 'NoMemmapfile');
    end
    
    b(2,k).Name               = NomImage;
    % b(2,k).InitialImageName = nomLayer;
    b(2,k).Unit               = 'deg';
    b(2,k).TagSynchroX        = TagSynchro;
    b(2,k).TagSynchroY        = TagSynchro;
    b(2,k).TagSynchroContrast = num2str(rand(1));
    b(2,k).XUnit              = 'Beam #';
    b(2,k).YUnit              = 'Sample #';
    b(2,k).InitialFileName    = this.nomFic;
    b(2,k).InitialFileFormat  = 'RESONs7k';
    
    b(2,k).GeometryType = cl_image.indGeometryType('SampleBeam');
    b(2,k) = set_SonarDescription(b(2,k), SonarDescription);
    %     b(2,k).Comments = Comments{ListeLayers(k)};
    %     b(2,k).strMaskBits = strMaskBits;
    b(2,k) = set_SampleBeamData(b(2,k), DataPing);
    
    b(2,k).DataType = cl_image.indDataType('TxAngle');
    b(2,k).ColormapIndex = 17;
    
    if ~isempty(DataSoundSpeedProfile)
        b(2,k) = set_SonarBathyCel( b(2,k), SoundSpeedProfile);
    end
    
    if ~isempty(InstallationParameters)
        b(2,k) = set_ResonInstallationParameters(b(2,k), InstallationParameters);
    end
    
    b(2,k) = compute_stats(b(2,k));
    b(2,k).TagSynchroContrast = num2str(rand(1));
    StatValues = b(2,k).StatValues;
    b(2,k).CLim = [StatValues.Min StatValues.Max];
    
    b(2,k) = update_Name(b(2,k));
    b(2,k).Writable = false;
end
if N ~= 1
    my_warndlg([strLoop ' : End'], 0);
end
b = b(:);
% my_close(hw)

%% Edition de l'objet cli_image

if nargout == 0
    SonarScope(b);
end


function DataPing = Extraction(Data, k)

DataPing.Frequency                              = Data.Frequency(k);
DataPing.MultiPingSequence                      = Data.MultiPingSequence(k);
DataPing.PingCounter                            = Data.PingCounter(k);
DataPing.SystemSerialNumber                     = Data.SystemSerialNumber;
DataPing.SampleRate                             = Data.SampleRate(k);
DataPing.PingSequence                           = Data.PingSequence(k);
DataPing.ReceiverBandwith                       = Data.ReceiverBandwith(k);
DataPing.TxPulseWidth                           = Data.TxPulseWidth(k);
DataPing.TxPulseTypeIdentifier                  = Data.TxPulseTypeIdentifier(k);
DataPing.TxPulseEnvelopeParameter               = Data.TxPulseEnvelopeParameter(k);
DataPing.TxPulseReserved                        = Data.TxPulseReserved(k);
DataPing.MaxPingRate                            = Data.MaxPingRate(k);
DataPing.PingPeriod                             = Data.PingPeriod(k);
DataPing.RangeSelection                         = Data.RangeSelection(k);
DataPing.PowerSelection                         = Data.PowerSelection(k);
DataPing.GainSelection                          = Data.GainSelection(k);
DataPing.ControlFlags                           = Data.ControlFlags(k);
DataPing.ProjectorMagicNumber                   = Data.ProjectorMagicNumber(k);
DataPing.ProjectorBeamSteeringAngleVertical     = Data.ProjectorBeamSteeringAngleVertical(k);
DataPing.ProjectorBeamSteeringAngleHorizontal   = Data.ProjectorBeamSteeringAngleHorizontal(k);
DataPing.ProjectionBeam_3dB_WidthVertical       = Data.ProjectionBeam_3dB_WidthVertical(k);
DataPing.ProjectionBeam_3dB_WidthHorizontal     = Data.ProjectionBeam_3dB_WidthHorizontal(k);
DataPing.ProjectionBeamFocalPoint               = Data.ProjectionBeamFocalPoint(k);
DataPing.ProjectionBeamWeightingWindowType      = Data.ProjectionBeamWeightingWindowType(k);
DataPing.ProjectionBeamWeightingWindowParameter = Data.ProjectionBeamWeightingWindowParameter(k);
DataPing.TransmitFlags                          = Data.TransmitFlags(k);
DataPing.HydrophoneMagicNumber                  = Data.HydrophoneMagicNumber(k);
DataPing.ReceiveBeamWeightingWindow             = Data.ReceiveBeamWeightingWindow(k);
DataPing.ReceiveBeamWeightingParameter          = Data.ReceiveBeamWeightingParameter(k);
DataPing.ReceiveFlags                           = Data.ReceiveFlags(k);
DataPing.ReceiveBeamWidth                       = Data.ReceiveBeamWidth(k);
DataPing.BottomDetectionFilterInfoMinRange      = Data.BottomDetectionFilterInfoMinRange(k);
DataPing.BottomDetectionFilterInfoMaxRange      = Data.BottomDetectionFilterInfoMaxRange(k);
DataPing.BottomDetectionFilterInfoMinDepth      = Data.BottomDetectionFilterInfoMinDepth(k);
DataPing.BottomDetectionFilterInfoMaxDepth      = Data.BottomDetectionFilterInfoMaxDepth(k);

DataPing.BottomDetectRangeFilterFlag            = Data.BottomDetectRangeFilterFlag(k);
DataPing.BottomDetectDepthFilterFlag            = Data.BottomDetectDepthFilterFlag(k);

DataPing.Absorption                             = Data.Absorption(k);
DataPing.SoundVelocity                          = Data.SoundVelocity(k);
DataPing.Spreading                              = Data.Spreading(k);
DataPing.PingCounter                            = Data.PingCounter(k);
DataPing.NbBeams                                = Data.NbBeams(k);
DataPing.BeamDescriptor                         = Data.BeamDescriptor(k);
DataPing.BeginSampleDescriptor                  = Data.BeginSampleDescriptor(k);
DataPing.CenterSampleDistance                   = Data.CenterSampleDistance(k);
DataPing.BeamAcrossTrackDistance                = Data.BeamAcrossTrackDistance(k);
DataPing.EmModel                                = Data.EmModel;
DataPing.Position                               = Data.Position(k);
DataPing.isEmpty7008                            = Data.isEmpty7008;
DataPing.isEmpty7058                            = Data.isEmpty7058;

T = Data.Time;
if isa(T, 'cl_time')
    T = T.timeMat;
end
DataPing.Time = cl_time('timeMat', T(k));
