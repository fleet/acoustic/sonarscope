% Cr�ation d'une image PingSamples � partir d'un fichier .s7k
%
% Syntax
%   [b,S] = view_SidescanImageIFR(s7k, ...);
%
% Input Arguments
%   s7k : Instance de cl_reson_s7k
%
% Name-Value Pair Arguments
%   subl        : Numeros de lignes a traiter
%
% Name-only Arguments
%   FaisceauxPairs  : Pour cr�er une image uniquement avec les faisceaux pairs
%   FaisceauxImairs : Pour cr�er une image uniquement avec les faisceaux impairs
%
% Output Arguments
%   b          : Instance de cli_image
%   subl_Image : Numeros de lignes communs avec la bathy (index relatis � seabedImage)
%   subl_Depth : Numeros de lignes communs avec la bathy (index relatis � Depth)
%
% Examples
%   b = view_SidescanImageIFR(a);
%   SonarScope(b);
%
%   b = view_SidescanImageIFR(s7k, 'subl', 1:100);
%   SonarScope(b);
%
% See also cl_simrad_all cl_simrad_all/plot_position cl_simrad_all/plot_nav Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function varargout = view_SidescanImageIFR(this, varargin)

persistent persistent_ListeLayers persistent_Carto

varargout{1} = [];
varargout{2} = [];
varargout{3} = [];
varargout{4} = [];
varargout{5} = [];

%% Check the number of elements

flag = checkIfOneElement(length(this));
if ~flag
    return
end

%%

[varargin, subl]         = getPropertyValue(varargin, 'subl',         []);
[varargin, Carto]        = getPropertyValue(varargin, 'Carto',        []);
[varargin, memeReponses] = getPropertyValue(varargin, 'memeReponses', false);
[varargin, ListeLayers]  = getPropertyValue(varargin, 'ListeLayers',  []); %#ok<ASGLU>

%% Lecture de la donn�e

if ~memeReponses && ~isempty(persistent_Carto)
    Carto = persistent_Carto;
end

identSondeur = selectionSondeur(this);

[DataSeabedImage, b, Carto] = read_SidescanDataIFR(this, identSondeur, 'Carto', Carto);
if isempty(b)
    return
end
if ~memeReponses
    persistent_Carto = Carto;
end

if isempty(ListeLayers)
    if ~memeReponses
        if length(b) == 1
            ListeLayers = 1;
        else
            clear str
            for i=1:length(b)
                str{i} = b(i).Name; %#ok
            end
            
            ListeLayers = my_listdlg('Layers :', str, 'InitialValue', 1);
            if isempty(ListeLayers)
                return
            end
        end
        persistent_ListeLayers = ListeLayers;
    else
        ListeLayers = persistent_ListeLayers;
    end
end
b = b(ListeLayers);


%% Recherche de la zone commune

nbPings = length(DataSeabedImage.PingCounter);
if isempty(subl)
    subl = 1:nbPings;
end
subl((subl < 1) | (subl > nbPings)) = [];

[flag, DataDepth] = read_depth_s7k(this);
if ~flag
    return
end
    
%% Recherche de la zone commune

[pppp, subl_Depth, subl_Seabed] = intersect(DataDepth.PingCounter, DataSeabedImage.PingCounter(subl)); %#ok

if ~isequal(subl, 1:nbPings)
    for i=1:numel(b)
        ImageName = b(i).Name;
        b(i) = extraction(b(i), 'suby', subl_Seabed);
        b(i).Name = ImageName;
    end
end

%% Edition de l'objet cli_image

if nargout == 0
    SonarScope(b);
else
    varargout{1} = b;
    varargout{2} = Carto;
    varargout{3} = subl_Seabed;
    varargout{4} = subl_Depth;
    varargout{5} = memeReponses;
end
