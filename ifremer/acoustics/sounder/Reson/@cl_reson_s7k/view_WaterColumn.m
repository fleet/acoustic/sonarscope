function [b, Carto, RMax1_Precedent, SonarDescription] = view_WaterColumn(this, varargin)

persistent persistent_IdentSonar

[varargin, Carto]                  = getPropertyValue(varargin, 'Carto',                  []);
[varargin, Display]                = getPropertyValue(varargin, 'Display',                1);
[varargin, nomFicNav]              = getPropertyValue(varargin, 'NavFileName',            []);
[varargin, nomFicIP]               = getPropertyValue(varargin, 'nomFicIP',               []);
[varargin, RMax1_Precedent]        = getPropertyValue(varargin, 'RMax1_Precedent',        []);
[varargin, subPing]                = getPropertyValue(varargin, 'subPing',                []);
[varargin, InstallationParameters] = getPropertyValue(varargin, 'InstallationParameters', []);
[varargin, Navigation]             = getPropertyValue(varargin, 'Navigation',             []);
[varargin, SonarDescription]       = getPropertyValue(varargin, 'SonarDescription',       []);
[varargin, DataDepth]              = getPropertyValue(varargin, 'DataDepth',              []);
[varargin, Data7000]               = getPropertyValue(varargin, 'Data7000',               []);
[varargin, DataWC]                 = getPropertyValue(varargin, 'DataWC',                 []);
[varargin, Data7004]               = getPropertyValue(varargin, 'Data7004',               []);
[varargin, DataPosition]           = getPropertyValue(varargin, 'DataPosition',           []);
[varargin, DataIndex]              = getPropertyValue(varargin, 'DataIndex',              []);
[varargin, identSondeur]           = getPropertyValue(varargin, 'identSondeur',           []);
[varargin, Mute]                   = getPropertyValue(varargin, 'Mute',                   0); %#ok<ASGLU>

b = [];
S0 = cl_sounder([]);

%% Check the number of elements

flag = checkIfOneElement(length(this));
if ~flag
    return
end

%%

if isempty(identSondeur)
    identSondeur = selectionSondeur(this);
end

if isempty(DataIndex)
    [flag, DataIndex] = readFicIndex(this, identSondeur);
    if ~flag
        return
    end
end

%% Lecture du fichier de navigation

% switch identSondeur
%     case {13003; 13016}
%         InstallationParameters = 'None';
%     otherwise
        if isempty(InstallationParameters)
            InstallationParameters = read_sonarInstallationParameters(this, identSondeur, ...
                'DataIndex', DataIndex, 'Mute', Mute);
        end
% end

%% Lecture de la donn�e de bathy

if isempty(DataDepth)
    [flag, DataDepth] = read_depth_s7k(this, 'DataIndex', DataIndex, 'identSondeur', identSondeur);
    if ~flag
        return
    end
end

nbPings = length(DataDepth.PingCounter);
if nbPings < 2
    str1 = sprintf('Le fichier "%s" ne contient pas suffisamment de pings.', this.nomFic);
    str2 = sprintf('File "%s" does not contain enough depth datagram.', this.nomFic);
    my_warndlg(Lang(str1,str2), 0);
    return
end

%% Lecture et interpolation de la donn�e "Position"

[flag, TimeNav, LatNav, LonNav, HeadingNav, TimeDepth, Latitude, Longitude, Heading] ...
    = read_navigationGlobale(this, 'identSondeur', identSondeur, 'DataIndex', DataIndex, ...
    'DataDepth', DataDepth, 'DataPosition', DataPosition, 'Navigation', Navigation, 'nomFicNav', nomFicNav);
if ~flag
    return
end

%% Lecture du fichier "InstallationParameters"

% TODO : d�j� trait� plus haut
if ~isequal(InstallationParameters, 'None')
    if isempty(InstallationParameters)
        [flag, InstallationParameters] = get_InstallationParameters(this, InstallationParameters, nomFicIP, ...
            'identSondeur', identSondeur);
        if ~flag
            return
        end
    end
end

%%

% subPing = 1:50:1000

[flag, Data, BeamDataOut, subPing] = read_WaterColumnData(this, identSondeur, 'subPing', subPing, 'Display', Display, ...
    'DataDepth', DataDepth, 'Data7000', Data7000, 'DataWC', DataWC, 'DataIndex', DataIndex);
% if isempty(Data) || isempty(BeamDataOut) || ~isfield(BeamDataOut(1).RD, 'Amplitude')
%     return
% end

if ~flag || isempty(Data) || isempty(BeamDataOut) || ~isfield(BeamDataOut(1), 'RD')...
        || isempty(BeamDataOut(1).RD) || ~isfield(BeamDataOut(1).RD, 'Amplitude')
    return
end

% PingCounter  = Data.PingCounter;
% PingSequence = Data.PingSequence;
%     if max(PingSequence) > 1 % Cas du multiping
%         PCWC    = PingCounter*4+PingSequence;
%         PCDepth = DataDepth.PingCounter*4+DataDepth.MultiPingSequence;
%     else
%         PCWC         = PingCounter;
%         PCSonarSettings = Data7000.PingCounter;
%     end
%     [~, subWC, sub7000] = intersect(PCWC, PCSonarSettings);


% DataTVG = read_TVG(this, identSondeur, 'subPing', subPing);

DataSoundSpeedProfile = []; % read_soundSpeedProfile(this, identSondeur);
if isempty(Data7004)
    Data7004 = read_7kBeamGeometry(this, identSondeur);
end
% % figure; plot(Data7004.BeamGeometry(1).RD.BeamHorizontalDirectionAngle); grid on
% figure; plot(Data7004.BeamHorizontalDirectionAngle(1,:)); grid on

N = length(BeamDataOut);
hw = create_waitbar('Loading WCD files : 2/2', 'N', N);
b = repmat(cl_image, 1,N);
for k=1:N
    my_waitbar(k, N, hw)
    %     iPing = subPing(k);
    iPing = find(Data.sub7000 == subPing(k));
    %     iPing = Data.sub7000(k);
    
    [flag, DataPing] = Extraction(Data, iPing);
    if ~flag
        b = [];
        return
    end
%     DataPing.TxAngle = Data7004.BeamGeometry(iPing).RD.BeamHorizontalDirectionAngle;
    kPing = min(iPing, size(Data7004.BeamHorizontalDirectionAngle,1)); %% TODO : Correctif pour fichier PDS
    DataPing.TxAngle = Data7004.BeamHorizontalDirectionAngle(kPing,:);
    
    %     if length(DataPing.TxAngle) ~= length(BeamDataOut(k).RD.BeamDepressionAngle)
    %         b = [];
    %         return
    %     end
    
    fe = BeamDataOut(k).RTH.SampleRate;
    
    
    
    
    % Ajout JMA le 19/05/2021 pour donn�es ODOM MB2 de chez Teledyne
    if fe == 0
        fe = DataPing.SampleRate;
    end
    
    

    
    DataPing.SampleRate = fe;
    
    switch DataPing.SystemSerialNumber
        case 1002 % Ajourt JMA le 19/05/2021 pour donn�es ODOM MB2 de chez Teledyne
            RangeMax = 100; % (m)
        case  7111
            RangeMax = 1000; % (m)
        case 7150
            if Data.Frequency(1) < 18000 % 12 kHz
                RangeMax = 12800; % (m)
            else % 24 kHz
                RangeMax =6400; % (m)
            end
        case 7125
            RangeMax = 100; % (m)
        case {13003; 13016} % Nlle gamme de sondeur NORBIT
            RangeMax = 500; % (m)
    end
    
    RangeMax = min(floor(2* fe * RangeMax / 1500), size(BeamDataOut(k).RD.Amplitude,1));
    
    Amplitude = BeamDataOut(k).RD.Amplitude(1:RangeMax,:);
    if isempty(Amplitude)
        %         b = [];
        b = b(1:(k-1));
        return
    end
    
    MinAmp =  min(Amplitude(:));
    if MinAmp < 0
        MinAmp = min(MinAmp, -13);
        Amplitude = Amplitude - MinAmp + 1;
    end
    %     Amplitude = reflec_Enr2dB(Amplitude);
    
    nbBeams = size(Amplitude,2);
    %     R1SamplesFiltre = repmat(RangeMax, 1, nbBeams);
    %     DataPing.R1SamplesFiltre = R1SamplesFiltre;
    DataPing.R1SamplesFiltre = BeamDataOut(k).RD.Range;
    
    if all(isnan(BeamDataOut(k).RD.Range))
        b = [];
        return
    end
    
    DataPing.TransmitSectorNumber = ones(1,nbBeams);
    DataPing.TransmitSectorNumber(floor(nbBeams/2):end) = 2;

        
    if isa(Data.Time, 'cl_time')
        T = Data.Time.timeMat;
        DataPing.Time = cl_time('timeMat', T(k));
    else
        DataPing.Time = cl_time('timeMat', Data.Time(k));
    end

    
    if ~isempty(Navigation) || ~isempty(nomFicNav) % Cas d'une navigation ext�rieure
        
        % TODO : Italie - JMA
        
        Longitude = my_interp1_longitude(TimeNav, LonNav,     DataPing.Time, 'linear', 'extrap');
        Latitude  = my_interp1(TimeNav,           LatNav,     DataPing.Time, 'linear', 'extrap');
        Heading   = my_interp1(TimeNav,           HeadingNav, DataPing.Time, 'linear', 'extrap');
        DataPing.Latitude  = Latitude;
        DataPing.Longitude = Longitude;
        DataPing.Heading   = Heading;
        
        %         iPingDepth = find(DataDepth.PingCounter == DataPing.PingCounter);
        %         iPingDepth = find(DataDepth.PingCounter == BeamDataOut(k).RTH.PingCounter);
        
        %         iPingDepth = iPing;
        %         iPingDepth = Data.sub7000(k);
        iPingDepth = subPing(k);
        
        if isempty(iPingDepth)
            DataPing.AcrossDist        = [];
            DataPing.AlongDist         = [];
            DataPing.BeamPointingAngle = [];
            DataPing.Depth             = [];
        else
            DataPing.AcrossDist        = DataDepth.AcrossDist(iPingDepth, :);
            DataPing.AlongDist         = DataDepth.AlongDist(iPingDepth, :);
            DataPing.BeamPointingAngle = DataDepth.BeamDepressionAngle(iPingDepth, :);
            DataPing.Depth             = DataDepth.Depth(iPingDepth, :);
        end
    else
        if isempty(TimeDepth)
            DataPing.AcrossDist        = [];
            DataPing.AlongDist         = [];
            DataPing.BeamPointingAngle = [];
            DataPing.Depth             = [];
        else
            Lon = my_interp1_longitude(TimeDepth, Longitude, DataPing.Time, 'linear', 'extrap');
            Lat = my_interp1(TimeDepth, Latitude,  DataPing.Time, 'linear', 'extrap');
            Head   = my_interp1(TimeDepth, Heading,  DataPing.Time, 'linear', 'extrap');
            DataPing.Latitude  = Lat;
            DataPing.Longitude = Lon;
            DataPing.Heading   = Head;
            
            % TODO : rechercher le ping correspondant
            %             iPingDepth = find(DataDepth.PingCounter == DataPing.PingCounter);
            %             iPingDepth = find(DataDepth.PingCounter == BeamDataOut(k).RTH.PingCounter);
            %             iPingDepth = iPing;
            iPingDepth = subPing(k);
            if isempty(iPingDepth)
                DataPing.AcrossDist        = [];
                DataPing.AlongDist         = [];
                DataPing.BeamPointingAngle = [];
                DataPing.Depth             = [];
            else
                DataPing.AcrossDist        = DataDepth.AcrossDist(iPingDepth, :);
                DataPing.AlongDist         = DataDepth.AlongDist(iPingDepth, :);
                DataPing.BeamPointingAngle = DataDepth.BeamDepressionAngle(iPingDepth, :);
                DataPing.Depth             = DataDepth.Depth(iPingDepth, :);
            end
        end
    end
    
    % Cr�ation de l'instance de classe
    
    IdentSonar = find_IdentSonar(S0, num2str(DataPing.SystemSerialNumber), ...
        'Frequency', DataPing.Frequency/1000);
    if isempty(persistent_IdentSonar) || isempty(SonarDescription) || (IdentSonar ~= persistent_IdentSonar)
        SonarDescription = cl_sounder('Sonar.Ident', IdentSonar, 'Sonar.Mode_1', 1, ...
            'Sonar.SystemSerialNumber', DataPing.SystemSerialNumber);
    end
    persistent_IdentSonar = IdentSonar;
    
    [~, TagSynchro] = fileparts(this.nomFic);
    TagSynchro = sprintf('%s_%d', TagSynchro, BeamDataOut(k).iPing);
    NomImage = TagSynchro;
    
    b(1,k) = cl_image;
    
    if IdentSonar == 34 % Norbit
        Amplitude = singleUnlessDouble(Amplitude, -1);
    end
    
    if isa(Amplitude, 'cl_memmapfile')
        b(1,k) = set_Image(b(1,k), Amplitude);
    else
        if N > 1
            b(1,k) = set_Image(b(1,k), cl_memmapfile('Value', Amplitude, 'LogSilence', 1));
        else
            b(1,k) = set_Image(b(1,k), Amplitude, 'NoMemmapfile');
        end
    end
    b(1,k).Name              = [NomImage '_WC'];
    b(1,k).x                 = 1:size(Amplitude,2);
    b(1,k).y                 = 1:size(Amplitude,1);
    b(1,k).YDir              = 2;
    % b(1,k).InitialImageName = nomLayer;
    b(1,k).Unit              = 'Amp';
    b(1,k).TagSynchroX       = TagSynchro;
    b(1,k).TagSynchroY       = [TagSynchro '_WC'];
    b(1,k).XUnit             = 'Beam #';
    b(1,k).YUnit             = 'Sample #';
    b(1,k).InitialFileName   = this.nomFic;
    b(1,k).InitialFileFormat = 'RESONs7k';
    b(1,k).Comments = sprintf('Reson PingCounter : %d', DataPing.PingCounter);
    
    b(1,k).GeometryType = cl_image.indGeometryType('SampleBeam');
    b(1,k) = set_SonarDescription(b(1,k), SonarDescription);
    
    b(1,k) = set_SampleBeamData(b(1,k), DataPing);
    
    b(1,k).DataType = cl_image.indDataType('Reflectivity');
    b(1,k).ColormapIndex = 3;
    
    if ~isempty(DataSoundSpeedProfile)
        SoundSpeedProfile.Z = DataSoundSpeedProfile.Depth;
        SoundSpeedProfile.T = DataSoundSpeedProfile.WaterTemperature;
        SoundSpeedProfile.S = DataSoundSpeedProfile.Salinity;
        SoundSpeedProfile.C = DataSoundSpeedProfile.SoundSpeed;
        b(1,k) = set_SonarBathyCel(b(1,k), SoundSpeedProfile);
    end
    
    %{
    % A tester : code copi� de view_BeamRawData
    TempsPropagation = (1:size(Amplitude,1)) / SampleRate;
    b(1,k) = set_SonarTime(b(1,k), TempsPropagation(:));
    %}
    
    if ~isempty(InstallationParameters) && ~isequal(InstallationParameters, 'None')
        b(1,k) = set_ResonInstallationParameters(b(1,k), InstallationParameters);
    end
    
    b(1,k) = update_Name(b(1,k));
    b(1,k).Writable = false;
    
    %% D�finition d'une carto
    
    while isempty(Carto)
        %         Carto = getDefinitionCarto(b(1,k), 'NoGeodetic', 1);
        Carto = getDefinitionCarto(b(1,k), 'NoGeodetic', 0);
    end
    b(1,k) = set_Carto(b(1,k), Carto);
    
end
b = b(:);
my_close(hw)

%% Edition de l'objet cli_image

if nargout == 0
    SonarScope(b);
end


function [flag, DataPing] = Extraction(Data, k)

try
    DataPing.Frequency                              = Data.Frequency(k);
    DataPing.MultiPingSequence                      = Data.MultiPingSequence(k);
    DataPing.PingCounter                            = Data.PingCounter(k);
    DataPing.SystemSerialNumber                     = Data.SystemSerialNumber;
    DataPing.SampleRate                             = Data.SampleRate(k);
    DataPing.PingSequence                           = Data.PingSequence(k);
    DataPing.ReceiverBandwith                       = Data.ReceiverBandwith(k);
    DataPing.TxPulseWidth                           = Data.TxPulseWidth(k);
    DataPing.TxPulseTypeIdentifier                  = Data.TxPulseTypeIdentifier(k);
    DataPing.TxPulseEnvelopeParameter               = Data.TxPulseEnvelopeParameter(k);
    DataPing.TxPulseReserved                        = Data.TxPulseReserved(k);
    DataPing.MaxPingRate                            = Data.MaxPingRate(k);
    DataPing.PingPeriod                             = Data.PingPeriod(k);
    DataPing.RangeSelection                         = Data.RangeSelection(k);
    DataPing.PowerSelection                         = Data.PowerSelection(k);
    DataPing.GainSelection                          = Data.GainSelection(k);
    DataPing.ControlFlags                           = Data.ControlFlags(k);
    DataPing.ProjectorMagicNumber                   = Data.ProjectorMagicNumber(k);
    DataPing.ProjectorBeamSteeringAngleVertical     = Data.ProjectorBeamSteeringAngleVertical(k);
    DataPing.ProjectorBeamSteeringAngleHorizontal   = Data.ProjectorBeamSteeringAngleHorizontal(k);
    DataPing.ProjectionBeam_3dB_WidthVertical       = Data.ProjectionBeam_3dB_WidthVertical(k);
    DataPing.ProjectionBeam_3dB_WidthHorizontal     = Data.ProjectionBeam_3dB_WidthHorizontal(k);
    DataPing.ProjectionBeamFocalPoint               = Data.ProjectionBeamFocalPoint(k);
    DataPing.ProjectionBeamWeightingWindowType      = Data.ProjectionBeamWeightingWindowType(k);
    DataPing.ProjectionBeamWeightingWindowParameter = Data.ProjectionBeamWeightingWindowParameter(k);
    DataPing.TransmitFlags                          = Data.TransmitFlags(k);
    DataPing.HydrophoneMagicNumber                  = Data.HydrophoneMagicNumber(k);
    DataPing.ReceiveBeamWeightingWindow             = Data.ReceiveBeamWeightingWindow(k);
    DataPing.ReceiveBeamWeightingParameter          = Data.ReceiveBeamWeightingParameter(k);
    DataPing.ReceiveFlags                           = Data.ReceiveFlags(k);
    DataPing.ReceiveBeamWidth                       = Data.ReceiveBeamWidth(k);
    DataPing.BottomDetectionFilterInfoMinRange      = Data.BottomDetectionFilterInfoMinRange(k);
    DataPing.BottomDetectionFilterInfoMaxRange      = Data.BottomDetectionFilterInfoMaxRange(k);
    DataPing.BottomDetectionFilterInfoMinDepth      = Data.BottomDetectionFilterInfoMinDepth(k);
    DataPing.BottomDetectionFilterInfoMaxDepth      = Data.BottomDetectionFilterInfoMaxDepth(k);
    
    DataPing.BottomDetectRangeFilterFlag            = Data.BottomDetectRangeFilterFlag(k);
    DataPing.BottomDetectDepthFilterFlag            = Data.BottomDetectDepthFilterFlag(k);
    
    DataPing.Absorption                             = Data.Absorption(k);
    DataPing.SoundVelocity                          = Data.SoundVelocity(k);
    DataPing.Spreading                              = Data.Spreading(k);
    DataPing.PingCounter                            = Data.PingCounter(k);
    DataPing.NbBeams                                = Data.NbBeams(k);
    DataPing.BeamDescriptor                         = Data.BeamDescriptor(k);
    DataPing.BeginSampleDescriptor                  = Data.BeginSampleDescriptor(k);
    DataPing.CenterSampleDistance                   = Data.CenterSampleDistance(k);
    DataPing.BeamAcrossTrackDistance                = Data.BeamAcrossTrackDistance(k);
    DataPing.EmModel                                = Data.EmModel;
    DataPing.Position                               = Data.Position(k);
%     
%     T = Data.Time;
%     if isa(T, 'cl_time')
%         DataPing.Time = T;
%     else
%         DataPing.Time = cl_time('timeMat', T(k));
%     end
    
    flag = 1;
    
catch  %#ok<CTCH> % Cas fichier D:\Temp\Herve\BugS7k\20130916_110818.s7k o� PingCounter est plus petit que les autres variables
    str1 = sprintf('Erreur d�codage cl_reson_s7k/view_WaterColumn/Extraction ping %d', k);
    str2 = sprintf('Erreur reading cl_reson_s7k/view_WaterColumn/Extraction ping %d', k);
    my_warndlg(Lang(str1,str2), 0, 'Tag', 'ErreurVewWaterColumn');
    DataPing = [];
    flag = 0;
end
