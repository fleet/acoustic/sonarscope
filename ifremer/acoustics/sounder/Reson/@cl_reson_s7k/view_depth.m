% Visualisation du contenu des datagrams "depth" contenus dans un fichier .all
%
% Syntax
%   [b, Carto] = view_depth(a)
%
% Input Arguments
%   a : Instance de cl_reson_s7k
%
% Name-Value Pair Arguments
%   subl         : Numeros de lignes a traiter
%   subc         : Numeros de faisceaux a traiter
%   TagSynchroY  : Tag de synchronisation en Y
%   ListeLayers  : Numeros des layers [] par defaut
%   Carto        : Parametres cartographiques ([] par defaut)
%   memeReponses : true=On pose les questions � chaque appel, palse=On m�morise les r�ponses
%
% Output Arguments
%   b     : Instance de cli_image
%   Carto : Parametres cartographiques
%   Bits  : TODO : sortir ce param�tre
%
% Examples
%   nomFic = getNomFicDatabase('RESON7125_20051116_221245.s7k')
%   a = cl_reson_s7k('nomFic', nomFic);
%   b = view_depth(a);
%   b = editobj(b);
%
% See also cl_reson_s7k cl_reson_s7k/plot_position cl_reson_s7k/plot_nav Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [c, Carto, Bits, DataDepth, identSondeur] = view_depth(this, varargin)

persistent persistent_ListeLayers persistent_Carto persistent_ListeMasqueBits

[varargin, subl]               = getPropertyValue(varargin, 'subl',               []);
[varargin, subc]               = getPropertyValue(varargin, 'subc',               []);
[varargin, ListeLayers]        = getPropertyValue(varargin, 'ListeLayers',        []);
[varargin, Carto]              = getPropertyValue(varargin, 'Carto',              []);
[varargin, TideFile]           = getPropertyValue(varargin, 'TideFile',           []);
[varargin, memeReponses]       = getPropertyValue(varargin, 'memeReponses',       false);
[varargin, Bits]               = getPropertyValue(varargin, 'Bits',               []);
[varargin, setMaskToAllLayers] = getPropertyValue(varargin, 'setMaskToAllLayers', false);
[varargin, isUsingParfor]      = getPropertyValue(varargin, 'isUsingParfor',      false);
[varargin, ComputeBathymetryIfNeeded] = getPropertyValue(varargin, 'ComputeBathymetryIfNeeded', true);

[varargin, identSondeur] = getPropertyValue(varargin, 'identSondeur', []);
[varargin, DataIndex]    = getPropertyValue(varargin, 'DataIndex',    []);

DataDepth    = [];
c = cl_image.empty;

A0 = cl_simrad_all([]);

%% Check the number of elements

flag = checkIfOneElement(length(this));
if ~flag
    return
end

%%

if isempty(identSondeur)
    identSondeur = selectionSondeur(this);
    if isempty(identSondeur)
        return
    end
end
if isempty(DataIndex)
    [flag, DataIndex] = readFicIndex(this, identSondeur);
    if ~flag
        return
    end
end

dataFromPP = (identSondeur == 7150) || (identSondeur == 7111);

[nomDir, NomImage] = fileparts(this.nomFic);
NomImage = [num2str(identSondeur) '_' NomImage];
NomImageInitial = NomImage;
[varargin, TagSynchroY] = getPropertyValue(varargin, 'TagSynchroY', NomImage); %#ok<ASGLU>

if isempty(TideFile) || isequal(TideFile, 0)
    Tide = [];
else
    [flag, Tide] = read_Tide(TideFile);
    if ~flag
        return
    end
    figTide = findobj(0,'Type','figure', 'Tag', 'TideFig');
    if isempty(figTide)
        [~, nomFicSeul, Ext] = fileparts(TideFile);
        Title = sprintf(' Tile file : %s', [nomFicSeul Ext]);
        figTide = FigUtils.createSScFigure('Tag', 'TideFig');
        PlotUtils.createSScPlot(Tide.Datetime, Tide.Value); grid on; title(Title, 'Interpreter', 'None');
    end
end

[strNomLayers, Unit, Comments] = getListeLayers(this);

if isempty(ListeLayers)
    if ~memeReponses
        ListeLayers = my_listdlg('Layers :', strNomLayers, 'InitialValue', [2:6 8 10 12 30]); % 28 remplac� par 30 (200% remplac� par 100%) le 20/03/2017
        if isempty(ListeLayers)
            return
        end
        persistent_ListeLayers = ListeLayers;
    else
        ListeLayers = persistent_ListeLayers;
    end
end

%% Lecture de la donn�e de bathy

[flag, DataDepth, identSondeur, flagFormat7006, flagFormat7027] = read_depth_s7k(this, 'DataIndex', DataIndex);
if ~flag
    return
end
nbPings = length(DataDepth.PingCounter);

[flag, IdentSonar] = DeviceIdentifier2IdentSonar(this, DataDepth.SystemSerialNumber);
if ~flag
    msg = sprintf('Sondeur %d pas inclus dans cl_reson_s7k/view_depth', DataDepth.SystemSerialNumber);
    my_warndlg(msg, 1);
    return
end

%% Calcul de la bathy si premi�re lecture de donn�es Norbit

if (IdentSonar == 34) && ComputeBathymetryIfNeeded % Norbit
    if all(isnan(DataDepth.Depth(:)))
        [flag, DataDepth, Carto] = ComputeNorbitBathymetry(this, Carto, 'identSondeur', identSondeur, 'DataIndex', DataIndex);
        if ~flag
            return
        end
    end
end

%% Lecture des datagrams SonarInstallationParameters

DataSonarInstallationParameters = read_sonarInstallationParameters(this, identSondeur, 'DataIndex', DataIndex);
if isfield(DataSonarInstallationParameters, 'WaterLineVerticalOffset')
    ReceiveArrayZ = DataSonarInstallationParameters.ReceiveArrayZ;
    WaterLineVerticalOffset = DataSonarInstallationParameters.WaterLineVerticalOffset;
    TransducerDepth = ReceiveArrayZ - WaterLineVerticalOffset;
    
    % Sauvegarde du fichier au format XML pour la WC
    
    flag = write_installationParameters_bin(this, identSondeur);
    if ~flag
        return
    end
else
    [~, nomFicSeul] = fileparts(this.nomFic);
    Mute = 0;
    if ~Mute
        str = sprintf('S7K cache makeup : %s : Group Depth (catching datagrams 7030) : No such datagram (sonarInstallationParameters)', nomFicSeul);
        my_warndlg(str, 0, 'Tag', 'No7030Datagram');
    end
    
    %     my_warndlg('No 7030 datagrams (Installation parameters).', 1);
    %     varargout{1} = [];
    %     varargout{2} = [];
    %     return
    ReceiveArrayZ = 0;
    WaterLineVerticalOffset = 0;
    TransducerDepth = ReceiveArrayZ - WaterLineVerticalOffset;
end

%% Lecture des datagrams soundSpeedProfile

DataSoundSpeedProfile = read_soundSpeedProfile(this, identSondeur, 'DataIndex', DataIndex);
if isempty(DataSoundSpeedProfile)
    %     return
end

%% Lecture et interpolation de la donnee "Attitude"

DataAttitude = read_attitude(this, identSondeur, 'DataIndex', DataIndex);
if isempty(DataAttitude) || isempty(DataAttitude.Roll)
    Roll  = DataDepth.Roll;
    Pitch = DataDepth.Pitch;
    Heave = DataDepth.Heave;
    
    if isempty(Roll) || isempty(Pitch) ||isempty(Heave)
        subVide = find((ListeLayers >= 20) & (ListeLayers < 24));
        if ~isempty(subVide)
            str1 = 'Vous avez demand� le chargement de layers issus des datagrams "Attitude" mais ces datagrams n''existent pas dans votre fichier .all';
            str2 = 'TODO';
            my_warndlg(Lang(str1,str2), 0, 'Tag', 'Y''a pas DataAttitude');
            ListeLayers(subVide) = [];
        end
    end
else
    if all(isnan(DataDepth.Roll)) % Fichier non compl�t� par PDS
        Roll  = my_interp1(DataAttitude.Datetime, DataAttitude.Roll,   DataDepth.Datetime, 'linear', 'extrap');
        Pitch = my_interp1(DataAttitude.Datetime, DataAttitude.Pitch,  DataDepth.Datetime, 'linear', 'extrap');
        Heave = my_interp1(DataAttitude.Datetime, DataAttitude.Heave,  DataDepth.Datetime, 'linear', 'extrap');
    else % Fichier compl�t� par PDS
        Roll  = DataDepth.Roll;
        Pitch = DataDepth.Pitch;
        Heave = DataDepth.Heave;
    end
end

%% Lecture et interpolation de la donn�e "Position"

% if isfield(DataDepth, 'Latitude') && ~isempty(DataDepth.Latitude) && any(~isnan(DataDepth.Latitude))
%     Latitude  = DataDepth.Latitude;
%     Longitude = DataDepth.Longitude;
%     Heading   = DataDepth.Heading;
% else
if all(isnan(DataDepth.Roll)) % Fichier non compl�t� par PDS
    DataPosition = read_navigation(this, identSondeur, 'DataIndex', DataIndex);
    %{
figure; plot(DataPosition.Longitude, DataPosition.Latitude, 'k');
hold on; plot(DataDepth.Longitude, DataDepth.Latitude, 'r'); title('Navigation'); grid on;
legend({'Datagram 7006 (bathy)'; 'Datagram 1015 (navigation)'});
    %}
    
    if isempty(DataPosition) || isempty(DataPosition.Latitude) || all(DataPosition.Latitude(:) < -90)
        Latitude  = NaN(nbPings,1);
        Longitude = NaN(nbPings,1);
        Heading   = NaN(nbPings,1);
    else
        if ~isfield(DataPosition, 'Datetime')
            DataPosition.Datetime = datetime(DataPosition.Time.timeMat, 'Convertfrom', 'datenum');
        end
        Latitude  = my_interp1(           DataPosition.Datetime, DataPosition.Latitude,  DataDepth.Datetime);
        Longitude = my_interp1_longitude( DataPosition.Datetime, DataPosition.Longitude, DataDepth.Datetime);
        Heading   = my_interp1_headingDeg(DataPosition.Datetime, DataPosition.Heading,   DataDepth.Datetime);
    end
else
    Latitude  = DataDepth.Latitude;
    Longitude = DataDepth.Longitude;
    Heading   = DataDepth.Heading;
end

%% Cr�ation des signaux provenant des datagrammes runtime

[~, signalContainerFromSonarSettings] = create_signalsFromSonarSettings(this, DataDepth.Datetime, identSondeur);

%% Cr�ation des signaux additionnels

[~, signalContainerFromOthers] = create_additionalSignals(this, DataDepth, identSondeur, ...
    'DataIndex', DataIndex, 'DataAttitude', DataAttitude);

%% Lecture et interpolation de la donn�e "SurfaceSoundSpeed"

SurfaceSoundSpeed = DataDepth.SoundSpeed;
if all(isnan(SurfaceSoundSpeed))
    DataSoundSpeed = read_SurfaceSoundSpeed(this, identSondeur, 'DataIndex', DataIndex);
    if isempty(DataSoundSpeed) ||~isfield(DataSoundSpeed, 'Datetime') || isempty(DataSoundSpeed.Datetime)
        SurfaceSoundSpeed = NaN(size(DataDepth.Datetime));
    else
        SurfaceSoundSpeed  = my_interp1(DataSoundSpeed.Datetime, DataSoundSpeed.SoundVelocity,  DataDepth.Datetime, 'linear', 'extrap');
    end
end

%% Lecture et interpolation de la donnee "Mode"

Mode = ones(nbPings,1);

%% Cr�ation de l'instance cli_image

% disp('Ici, DataVolatileSonarSettings.RTH.Absorption = 1')
DataVolatileSonarSettings.RTH.Absorption = 1; %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
BSStatus = init_BSStatus('Absorption', DataVolatileSonarSettings.RTH.Absorption);
TagSynchroX = [NomImage ' - PingBeam'];

%% Param�tres du sondeur d�finis pour le mode majoritaire

if ~any(Mode)
    Mode(:) = 1; % IMPOSE AUTORITAIREMENT (Cas des donn�es du NIWA)
end
ModeMajoritaire = median(Mode);
SonarDescription = cl_sounder('Sonar.Ident', IdentSonar, 'Sonar.Mode_1', ModeMajoritaire, ...
    'Sonar.SystemSerialNumber', DataDepth.SystemSerialNumber);
resolutionMajoritaire = get_RangeResol(SonarDescription, ModeMajoritaire);
resolution            = get_RangeResol(SonarDescription, Mode);

InitialFileFormat = 'ResonS7k';
% [~, InitialFileName] = fileparts(this.nomFic);
GeometryType = cl_image.indGeometryType('PingBeam');   % BathyFais
y = 1:nbPings; %DataDepth.PingCounter;
YUnit = 'Ping';

%% Rajout JMA le 08/03/2016 pour pr�paration d�mo BDA pour le SHOM

if all(isnan(Roll))
    Roll(:)  = 0;
    Pitch(:) = 0;
    Heave(:) = 0;
end

%% Lecture des diff�rents layers de l'image et stockage dans l'instance cli_image

% fprintf('Reading file : %s\n', NomImage);

init2Layers = true;
nbLayers  = length(ListeLayers);
% FlagNaN   = [];
Immersion = [];
CLim = cell(nbLayers, 1);
Colors  = 'krgmyb';
Markers = 'o*+x';
for k=1:nbLayers
    nomLayer = strNomLayers{ListeLayers(k)};
    switch nomLayer
        case {'Depth / SeaLevel'; 'Depth / Antenna'}
            nomLayerAll = 'Depth';
        case {'IfremerQF'; 'UncertaintyPerCent'; 'UncertaintyMeters'}
            nomLayerAll = 'Reflectivity';
        case {'DetectionBrightness'
                'MasqueDetection'
                'ProbaDetection'
                'DetectionColinearity'
                'DetectionNadirFilter'}
            nomLayerAll = 'QualityFactor';
        case 'Detection'
            if flagFormat7027
                nomLayerAll = 'Flags';
            else
                nomLayerAll = 'QualityFactor';
            end
        case {'MasqueDepth'; 'ProbaDepth'; 'Depth-Heave'}
            nomLayerAll = 'Depth';
        case {'LongueurTrajet'; 'Temps universel'; 'Roll'; 'Pitch'; 'Heave'; 'Heading'; 'RayPathTravelTime'}
            nomLayerAll = 'Range';
        case 'TxBeamIndex'
            nomLayerAll = 'BeamDepressionAngle';
        case 'MultiPingSequence'
            nomLayerAll = 'Mask'; % On donne un nom pour ne pas passer dans le test d'existence "if ~isfield(DataDepth, nomLayerAll)"
            
%         case 'ReflectivityFromSnippets 200%'
%             nomLayerAll = 'ReflectivityFromSnippets';
        case 'ReflectivityFromSnippets 100%'
            nomLayerAll = 'ReflectivityFromSnippets100';
        case 'ReflectivityFromSnippets 1 sample'
            nomLayerAll = 'ReflectivityFromSnippets1';
            
        otherwise
            nomLayerAll = nomLayer;
    end
    %RAJOUTER CA
    % BeamHorizontalDirectionAngle
    
    if ~isfield(DataDepth, nomLayerAll)
        str1 = sprintf('Le layer "%s" n''existe pas pour le fichier "%s"', nomLayerAll, this.nomFic);
        str2 = sprintf('"%s" does not exist for "%s"', nomLayerAll, this.nomFic);
        my_warndlg(Lang(str1,str2), 0);
        %         return
        continue
    end
    
    if isempty(subl)
        subl = 1:size(DataDepth.(nomLayerAll),1);
    end
    
    if isempty(subc)
        subc = 1:size(DataDepth.(nomLayerAll),2);
    end
    
    if init2Layers
        TwoWayTravelTime_s = DataDepth.Range(subl,subc);
        init2Layers = false;
    end
    
    if isempty(subl) || isempty(subc)
        str1 = sprintf('Le layer "%s" n''existe pas pour le fichier "%s"', nomLayerAll, this.nomFic);
        str2 = sprintf('"%s" does not exist for "%s"', nomLayerAll, this.nomFic);
        my_warndlg(Lang(str1,str2), 0);
        %         return
        continue
    end
    
    %% Lecture de l'image
    
    %     my_warndlg(['Reading file : ' NomImage ' - Layer : ' nomLayer], 0);
    
    switch ListeLayers(k)
        case 6
            I = TwoWayTravelTime_s;
            
        case {28; 30; 31} % ReflectivityFromSnippets
            if isfield(DataDepth, nomLayerAll) && ~isempty(DataDepth.(nomLayerAll)(:,:))
                if strcmp(nomLayerAll, 'ReflectivityFromSnippets') && isempty(DataDepth.ReflectivityFromSnippets)
                    str1 = sprintf('Le layer "ReflectivityFromSnippets" n''existe pas pour le fichier "%s". D�s�lectionnez le dans la liste des layers.', NomImage);
                    str2 = sprintf('The layer "ReflectivityFromSnippets" does not exist in "%s". Unselect it in the list of layers.', NomImage);
                    my_warndlg(Lang(str1,str2), 0, 'Tag', 'ReflectivityFromSnippetsDoesNotExist');
                    I = NaN(length(subl), length(subc), 'single');
                else
                    I = DataDepth.(nomLayerAll)(subl,subc);
                    if isfield(DataDepth, 'MaskPingsReflectivity') && ~isempty(DataDepth.MaskPingsReflectivity)
                        MaskPingsReflectivity = DataDepth.MaskPingsReflectivity(:);
                        for kPing=1:length(MaskPingsReflectivity)
                            if MaskPingsReflectivity(kPing)
                                I(kPing,:) = NaN;
                            end
                        end
                    end
                end
            else
                str1 = sprintf('Le layer "%s" n''existe pas pour le fichier "%s"', nomLayerAll, this.nomFic);
                str2 = sprintf('No "%s" layer in "%s"', nomLayerAll, this.nomFic);
                my_warndlg(Lang(str1,str2), 0);
                %                 return
                continue
            end
        case {1;2;3;4;5;7;9;10;11;12;13;14;15;16;17;27}
            I = DataDepth.(nomLayerAll)(subl,subc);
            
            if strcmp(nomLayerAll, 'Depth')
                if ~isempty(Tide)
                    TideValue = my_interp1(Tide.Datetime, Tide.Value, DataDepth.Datetime(subl));
                    kColor  = randi(length(Colors));
                    kMarker = randi(length(Markers));
                    FigUtils.createSScFigure(figTide); hold on; PlotUtils.createSScPlot(DataDepth.Datetime, TideValue, ['-' Markers(kMarker) Colors(kColor)]); hold off;
                    
                    % Mettre un message si pas d'intersection entre fichier de
                    % mar�e et donn�e
                    if any(isnan(TideValue))
                        strTide = char(Tide.Datetime);
                        strFile = char(DataDepth.Datetime);
                        str1 = sprintf('Pas d''intersection trouv�e entre les donn�es de mar�e fournies et le fichier %s : Je pr�f�re supprimer ces donn�es plut�t que de ne pas faire la correction.\n\nFile time :\n%s\n\nTide time : \n%s', ...
                            this.nomFic, strFile, strTide);
                        str2 =  sprintf('No intersection found between Tide data and file %s  : I prefer to supress this data rather than not to do the correction.\n\nFile time :\n%s\n\nTide time : \n%s', ...
                            this.nomFic, strFile, strTide);
                        my_warndlg(Lang(str1,str2), 0, 'Tag', 'NoIntersectionTide');
                    end
                    I = bsxfun(@plus, I, TideValue(:));
                end
                
                %                 TxDelay_PingBeam_s = 0; % zeros(length(subl), length(subc));
                %                 He = get_HeaveRx(A0, DataAttitude, DataDepth.Datetime, TwoWayTravelTime_s(subl, subc), TxDelay_PingBeam_s);
                
                % TODO : mettre � jour ce calcul comme cela est fait pour les .all : Heave = (HeaveRx + HeaveTx_PingBeam) / 2;
                % {
                %                 HeaveRx = get_HeaveRx(A0, DataAttitude, DataDepth.Datetime, TwoWayTravelTime_s(subl,subc), TxDelay_PingBeam_s);
                % HeaveRx = Heave(:);
                
                %                 ySampleHeaveTx = findYSampleListByTag(signalsFromAttitude, 'HeaveTx');
                %                 for k2=length(ySampleHeaveTx):-1:1
                %                     %     HeaveTxVal(:,k2) = signalsFromAttitude(indSignal).ySample(k).data;
                %                     HeaveTxVal(:,k2) = ySampleHeaveTx(k2).data;
                %                 end
                %                 HeaveTx_PingBeam = get_PingBeamMatrixFromPingSignal(HeaveTxVal, DataRaw.TransmitSectorNumber(subRaw,:));
                %                 HeaveTx_PingBeam = Heave;
                %                 pppp = HeaveRx - HeaveTx_PingBeam(:); SonarScope(pppp)
                %                 He = HeaveRx;
                %                 He = HeaveTx_PingBeam(:);
                %                 He = (HeaveRx + HeaveTx_PingBeam(:)) / 2;
                % }
                
                %                 I = I + He;
                %                 Suppression de la prise en compte de HE valid�e sur
                %                 fichier 20151126_035140_PP-7111-100kHz-.s7k
                
                switch nomLayer
                    case 'Depth / SeaLevel'
%                         if isfield(DataDepth, 'VehicleDepth') && ~isempty(DataDepth.VehicleDepth) && ~all(isnan(DataDepth.VehicleDepth))
%                             Immersion = DataDepth.VehicleDepth(subl);
%                             % TODO : Immersion = 0.85 mais TransducerDepth = -5.99 pour PP? 7150 !!!
%                             I = bsxfun(@minus, I, Immersion(:)); % Conforme � Caraibes
%                         end
                    case 'Depth / Antenna' % TODO : faire le claire dans tout �a
                         if isfield(DataDepth, 'VehicleDepth') && ~isempty(DataDepth.VehicleDepth) && ~all(isnan(DataDepth.VehicleDepth))
%                             Immersion = DataDepth.VehicleDepth(subl);
%                             I = bsxfun(@minus, I, Immersion(:)+TransducerDepth);
                            I = I - TransducerDepth;
                        end
               end
            end
            
        case 18
            I = generateDistanceTrajet(DataDepth.Range, resolution, subl, subc);
            
        case 19
            I = get_DatetimeRx(A0, TwoWayTravelTime_s, zeros(size(TwoWayTravelTime_s)), DataDepth.Datetime);
            I = datenum(I); % TODO : int�grer datetime dans la visu SSc
            
        case 20
            I = get_RollRx(A0, DataAttitude, DataDepth.Datetime, TwoWayTravelTime_s(subl, subc), zeros(length(subl), length(subc)));
            
        case 21
            I = get_PitchRx(A0, DataAttitude, DataDepth.Datetime, TwoWayTravelTime_s(subl, subc), zeros(length(subl), length(subc)));
            
        case 22
            I = get_HeaveRx(A0, DataAttitude, DataDepth.Datetime, TwoWayTravelTime_s(subl, subc), zeros(length(subl), length(subc)));
            
        case 23
            I = get_HeadingRx(A0, DataAttitude, DataDepth.Datetime, TwoWayTravelTime_s(subl, subc), zeros(length(subl), length(subc)));
            
        case 25
            I = TwoWayTravelTime_s(subl, subc);
            
        case 26
            I = DataDepth.(nomLayerAll)(subl,subc);
            
        case 29
            I = DataDepth.Mask(subl,subc);
            for k2=1:length(subl)
                I(k2,:) = DataDepth.MultiPingSequence(subl(k2));
            end
            
        case 8 % IfremerQF
            if dataFromPP % Donn�es Ifremer du PP?
                I = DataDepth.(nomLayerAll)(subl,subc);
            else
                I = DataDepth.(nomLayerAll)(subl,subc);
                I = -log10(I)-1;
            end
            
        case 32 % UncertaintyPerCent
            if dataFromPP % Donn�es Ifremer du PP?
                I = DataDepth.(nomLayerAll)(subl,subc);
                I = (10 .^ -I) * 100;
            else
                I = DataDepth.(nomLayerAll)(subl,subc);
                I = I * 1000;
            end
            
        case 33 % UncertaintyMeters
            if dataFromPP % Donn�es Ifremer du PP?
                I = DataDepth.(nomLayerAll)(subl,subc);
                I = (10 .^ -I);
                I = -(I .* DataDepth.Depth(subl,subc));
            else
                I = DataDepth.(nomLayerAll)(subl,subc);
                I = -10*(I .* DataDepth.Depth(subl,subc));
            end
    end
    
    if memeReponses && ~isempty(persistent_ListeMasqueBits) && isempty(Bits)
        Bits = persistent_ListeMasqueBits;
    end
    if ischar(Bits) % 'AskIt'
        Bits = [];
    end
    if isempty(Bits) % memeReponses
        if (k == 1)
            str1 = sprintf('S�lection des bits du facteur de qualit� � utiliser pour masquer la donn�e "%s"', nomLayer);
            str2 = sprintf('Quality factors selected for "%s" validation.', nomLayer);
            strMaskBits = {'Brightness'; 'Colinearity'; 'Amplitude'; 'Phase'; 'Do not Use'; 'Nadir Filter'}; % ; 'Mask by Histogram'};
            if flagFormat7006
                InitialValue = [1 2 6];
            else
                InitialValue = 1;
            end
            if (get_LevelQuestion >= 3) && ~isUsingParfor
                [Bits, flag] = my_listdlgMultiple(Lang(str1,str2), strMaskBits, 'InitialValue', InitialValue);
                if ~flag
                    return
                end
            else
                Bits = InitialValue;
            end
            if ~isempty(Bits)
                setMaskToAllLayers = true;
            end
            persistent_ListeMasqueBits = Bits;
        else
            Bits = persistent_ListeMasqueBits;
        end
    else
        if flagFormat7027 % Cas du MMR Ajout JMA le 10/06/2021
            Bits = 1;
        end
    end
    
    DepthMask = true(length(subl), length(subc));
    for k2=1:length(Bits)
        Val = 2^(Bits(k2)-1);
        K = single(bitand(uint8(DataDepth.Mask),Val)) / Val;
        subK = (K ~= 1);
        DepthMask(subK) = false;
        clear subK
    end
    %         if isfield(DataDepth, 'Depth') && ~isempty(DataDepth.Depth)
    %             DepthMask(isnan(DataDepth.Depth(subl,subc))) = false;
    %         end
    if isfield(DataDepth, 'Range') && ~isempty(DataDepth.Range)
        DepthMask(isnan(DataDepth.Range(subl,subc))) = false;
    end
    
    setMaskToAllLayers = true;              % Ajout JMA le 26/05/2021
    DepthMask(DataDepth.Mask == 0) = false; % Ajout JMA le 26/05/2021
    
    strMaskBits = {};
    switch nomLayer
        case 'Detection'
            if flagFormat7027
                subNan = isnan(I);
                W = warning;
                warning('off', 'all')
                I = bitand(uint8(I),3);
                warning(W)
                I(subNan) = NaN;
            else
                subNan = isnan(I);
                W = warning;
                warning('off', 'all')
                %                 I = bitand(uint8(I),12);
                I = bitand(uint8(I),15);
                I = single(bitshift(uint8(I),-2));
                warning(W)
                I(subNan) = NaN;
                
                %                 subNan = isnan(DataDepth.Depth(subl,subc));
                subNan = isnan(DataDepth.Range(subl,subc));
                I(subNan) = NaN;
                CLim{k} = [0 2];
            end
        case 'DetectionBrightness'
            subNan = isnan(I);
            I(subNan) = 0;
            I = single(bitand(uint8(I),1));
            I(subNan) = NaN;
        case 'DetectionColinearity'
            subNan = isnan(I);
            I(subNan) = 0;
            I = single(bitand(uint8(I),2)) / 2;
            I(subNan) = NaN;
        case 'DetectionNadirFilter'
            subNan = isnan(I);
            I(subNan) = 0;
            I = single(bitand(uint8(I),32)) / 32;
            
            % EN ATTENDANT QUE RESON INVERSE CE BIT
            I = 1 - I; %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % EN ATTENDANT QUE RESON INVERSE CE BIT
            
            I(subNan) = NaN;
            
        case 'Mask'
            %             strMaskBits = {'Brightness'; 'Colinearity'; 'Amplitude'; 'Phase'; 'Do not use'; 'Nadir Filter'; 'Mask by Histogram'};
            strMaskBits = {'Brightness'; 'Colinearity'; 'Amplitude'; 'Phase'; 'Do not use'; 'Nadir Filter'};
            
        otherwise
            
            if flagFormat7006
                %             if strcmp(nomLayer, 'Range')
                TD = DataDepth.QualityFactor(subl,subc);
                subNaN = isnan(TD);
                TD(subNaN) = 0;
                TD = bitand(uint8(TD),12);
                TD = single(bitshift(uint8(TD),-2));
                I(TD == 0) = NaN;
                I(subNaN) = NaN;
                %             end
            end
    end
    
    %% Case of Reflectivity
    
    switch nomLayer
        case 'Reflectivity'
            if identSondeur == 7125
                I = reflec_Amp2dB(I);
            end
        case 'ReflectivityFromSnippets 200%'
            NomImage = [NomImageInitial '_Snippets200'];
            if identSondeur == 7125
                I = reflec_Amp2dB(I);
            end
        case 'ReflectivityFromSnippets 100%'
            NomImage = [NomImageInitial '_Snippets100'];
            if identSondeur == 7125
                I = reflec_Amp2dB(I);
            end
        case 'ReflectivityFromSnippets 1 sample'
            NomImage = [NomImageInitial '_1samp'];
            if identSondeur == 7125
                I = reflec_Amp2dB(I);
            end
    end
    
    %% Cr�ation de l'instance cl_image
    
    b = cl_image;
    b = set_Image(b, I);
% 	b = compute_stats(b);
    
    StatValues = b.StatValues;
    if isempty(StatValues)
        str = sprintf('The layer "%s" is empty.', nomLayer);
        my_warndlg(str, 0);
        continue
    end
    if isnan(StatValues.Min)
        str = sprintf('The layer "%s" is empty.', nomLayer);
        my_warndlg(str, 0);
        continue
    end
    
    b.Name              = NomImage;
    b.InitialImageName  = nomLayer;
    b.Unit              = Unit{ListeLayers(k)};
    b.TagSynchroX       = TagSynchroX;
    b.TagSynchroY       = TagSynchroY;
    b.XUnit             = 'Beam #';
    b.YUnit             = YUnit;
    b.InitialFileName   = this.nomFic;
    b.InitialFileFormat = InitialFileFormat;
    b.GeometryType      = GeometryType;
    b = set_SonarDescription( b, SonarDescription);
    b.Comments          = Comments{ListeLayers(k)};
    b.strMaskBits       = strMaskBits;
    b.x                 = subc;
    b.y                 = y(subl);
    
    [DataType, ColormapIndex] = identSonarDataType(this, strNomLayers{ListeLayers(k)});
    b.DataType = DataType;
    b.ColormapIndex = ColormapIndex;
    
    %% Transfert des signaux
    
    %     b = set_SonarHeight(b, -DataDepth.VehicleDepth);
    %     disp('Ici DataDepth.VehicleDepth remplac� car donn�es non conformes')
    nbFais = size(DataDepth.Depth,2);
    if mod(nbFais,2)
        ifaisMilieu = ceil(nbFais / 2);
        b = set_SonarHeight(b, -abs(DataDepth.Depth(subl,ifaisMilieu)));
    else
        ifaisMilieu = [floor(nbFais / 2) ceil(nbFais / 2)];
        b = set_SonarHeight(b, -abs(mean(DataDepth.Depth(subl,ifaisMilieu),2)));
    end
    
    b = set_SonarTime(b, DataDepth.Datetime(subl));
    if ~isempty(Heading)
        b = set_SonarHeading(b, Heading(subl));
    end
    
    if isfield(DataDepth, 'ProjectorBeamSteeringAngleVertical')
        X = DataDepth.ProjectorBeamSteeringAngleVertical(subl);
        b = set_SonarTxAlongAngle(b, X(:));
    end
    
    if isempty(Immersion)
        X = repmat(TransducerDepth, length(subl), 1);
        b = set_SonarImmersion(b, X);
    else
        b = set_SonarImmersion(b, Immersion);
    end
    
    X = SurfaceSoundSpeed(subl);
    b = set_SonarSurfaceSoundSpeed(b, X(:));
    b = set_SonarPortMode_1(b, Mode(subl));
    
    b = set_SonarRawDataResol(b, resolutionMajoritaire);
    b = set_SonarResolutionD(b, resolutionMajoritaire);
    
    b = set_SonarSampleFrequency(b, DataDepth.SampleRate(:));
    
    if all(isnan(DataDepth.Frequency))
        b = set_SonarFrequency(b, DataDepth.MultiPingSequence(:));
    else
        b = set_SonarFrequency(b, DataDepth.Frequency(:));
    end
    
    if ~isempty(Roll)
        X = Roll(subl);
        b = set_SonarRoll(b, X(:));
        X = Pitch(subl);
        b = set_SonarPitch(b, X(:));
        %     b = set(b, 'SonarYaw',   Yaw);
    end
    X = Heave(subl);
    b = set_SonarHeave(b, X(:));
    b = set_SonarTide(b, zeros(length(subl), 1));
    X = DataDepth.PingCounter(subl);
    b = set_SonarPingCounter(b, X(:));
    
    b = set_SonarFishLongitude(b, Longitude(subl));
    b = set_SonarFishLatitude(b, Latitude(subl));
        
    %% Vertical signals from runtime datagrams
    
    b = set_SignalsVert(b, [signalContainerFromSonarSettings(:) signalContainerFromOthers(:)]); % signalContainerFromRuntime

    %% Parametres d'installation du sondeur
    
    % TODO : le datagramme 7030 n'est pas pr�sent dans les fichiers
    % provenant du 7KCenter, rrrhhaaaa !!!!!!!!!!!!!!!!!!!!!!! quels cons !
    % TODO : attention, fonction uniquement Kongsberg
    b = set_ResonInstallationParameters(b, DataSonarInstallationParameters, this.nomFic);
    
    %% Transfert des parametres d'etat de la donnee
    
    b = set_BSStatus(b, BSStatus);
    
    %% Profil de c�l�rit�
    
    if ~isempty(DataSoundSpeedProfile)
        b = set_SonarBathyCel(b, DataSoundSpeedProfile);
    end
    
    %% D�finition d'une carto
    
    while isempty(Carto)
        if ~memeReponses || isempty(persistent_Carto)
            nomFicCarto = fullfile(nomDir, 'Carto_Directory.def');
            if exist(nomFicCarto, 'file')
                Carto = import_xml(cl_carto([]), nomFicCarto);
                if isempty(Carto)
                    
                    % Au cas o� le fichier soit � l'ancienne m�thode
                    
                    Carto = import(cl_carto([]), nomFicCarto);
                    flag = export_xml(Carto, nomFicCarto);
                    if flag
                        %                 delete(nomFic)
                    end
                end
            else
                Carto = getDefinitionCarto(b, 'NoGeodetic', 0);
            end
            persistent_Carto = Carto;
        else
            Carto = persistent_Carto;
        end
    end
    b = set_Carto(b, Carto);
    
    
    if ListeLayers(k) == 26
        [DataType, ColormapIndex] = identSonarDataType(this, 'BeamDepressionAngle');
        b.DataType      = DataType;
        b.ColormapIndex = ColormapIndex;
        
        b = sonar_secteur_emission(b, []);
    end
    
    %% D�finition du nom de l'image
    
    if strcmp(strNomLayers{ListeLayers(k)}, 'MultiPingSequence')
        b = update_Name(b, 'Append', strNomLayers{ListeLayers(k)});
    else
        b = update_Name(b);
    end
    
    %% Ajout de l'image dans l'instance clc_image
    
    c = [c b]; %#ok<AGROW>
end

%% Apply masks

if setMaskToAllLayers
    %     for k=1:length(ListeLayers)
    for k=1:numel(c)
        b = c(k);
        I = get(b, 'Image');
        
        switch strNomLayers{ListeLayers(k)}
            case {'Mask'; 'DetectionBrightness'; 'DetectionColinearity'; 'DetectionNadirFilter'}
            otherwise
                %I(~Mask) = NaN;
                %                 I((Mask <= 224) | ~DepthMask) = NaN;
                % TODO avec maskdepth I(Mask<=224 ) = NaN; % modif GLT 23/09/2016 pour la prise en compte r�elle des masque
                
                % Modif GLT annul�e par JMA car �a masque toutes les donn�es du Reson 7125 du ROV
                I(~DepthMask) = NaN; %
        end
        
        b = set_Image(b, I); % 'PredefinedStats', []
        b = compute_stats(b);
        StatValues = b.StatValues;
        if isempty(CLim{k})
            CLimLayer = [StatValues.Min StatValues.Max];
        else
            CLimLayer = CLim{k};
        end
        b.CLim = CLimLayer;
        
        c(k) = b;
    end
end


function I = generateDistanceTrajet(Range, resolution, subl, subc)
I = Range(subl,subc) .* repmat(resolution(subl)/2, 1, length(subc));
