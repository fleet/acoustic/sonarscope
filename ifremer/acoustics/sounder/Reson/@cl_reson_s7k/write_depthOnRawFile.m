% Lecture des datagrams "depth" dans un fichier .all
%
% Syntax
%   Data = write_depthOnRawFile(a)
%
% Input Arguments
%   a : Instance de cl_reson_s7k
%
% Output Arguments
%   Data : Structure contenant les
%       Heading            : Cap du navire (deg)
%       PingCounter        : Numeor de Ping
%       EmModel            : Numero du model su dondeur
%       SystemSerialNumber : Numero de serie du sondeur%   PingCounter        : Numeor de Ping
%
% Examples
%   nomFicIn  = getNomFicDatabase('RESON7125_20051116_221245.s7k')
%   nomFicOut = [my_tempdir filesep 'toto.s7k']
%   copyfile(nomFicIn, nomFicOut)
%   a = cl_reson_s7k('nomFic', nomFicOut);
%   [flag, Data, identSondeur] = read_depth_s7k(a);
%   figure; imagesc(Data.Depth); colorbar
%   histo1D(Data.Depth)
%
%   [MasqueDetection, MasqueDetectionNaN] = cleanDetection(Data.QualityFactor);
%   figure; imagesc(MasqueDetection); colorbar
%   figure; imagesc(MasqueDetectionNaN); colorbar
%   figure; imagesc(Data.Depth .* MasqueDetectionNaN); colorbar
%   histo1D(Data.Depth .* MasqueDetectionNaN)
%
%   [MasqueDepth, MasqueDepthNaN] = cleanDepth(Data.Depth);
%   figure; imagesc(MasqueDepth); colorbar
%   figure; imagesc(MasqueDepthNaN); colorbar
%   Depth = Data.Depth .* MasqueDetectionNaN .* MasqueDepthNaN;
%   figure; imagesc(Data.Depth .* MasqueDetectionNaN .* MasqueDepthNaN); colorbar
%   histo1D(Data.Depth .* MasqueDetectionNaN .* MasqueDepthNaN)
%
%   write_masque(a, Data.Masque | MasqueDetection | MasqueDepth)
%   write_depthOnRawFile(a, 'Depth', Depth)
%
%   nomFicVerif = [my_tempdir filesep 'titi.all']
%   copyfile(nomFicOut, nomFicVerif)
%   b = cl_reson_s7k('nomFic', nomFicVerif);
%  [flag, X, identSondeur] = read_depth_s7k(b);
%   figure; imagesc(X.Depth); colorbar
%
% See also cl_reson_s7k cl_reson_s7k/plot_position cl_reson_s7k/plot_nav Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function write_depthOnRawFile(this, identSondeur, varargin)

global useCacheNetcdf %#ok<GVMIS>

%% Check the number of elements

flag = checkIfOneElement(length(this));
if ~flag
    return
end

%% On regarde si l'utilisateur a donn� l'ensemble de la structure Data

[varargin, Data] = getPropertyValue(varargin, 'Data', []);
if isempty(Data)
    [flag, Data, identSondeur] = read_depth_s7k(this);
    if ~flag
        return
    end
end

%% On regarde si l'utilisateur demande la sauvegarde de Depth

[varargin, Mask] = getPropertyValue(varargin, 'Mask', []);
if ~isempty(Mask)
    if isequal(size(Data.Depth), size(Mask))
        Data.Mask = single(Mask);
    else
        str1 = sprintf('La taille des matrices doit �tre la m�me.  Data.Depth=%s  Depth=%s', ...
            num2strCode(size(Data.Depth)), num2strCode(size(Mask)));
        str2 = sprintf('The matrices must be the same sizer.  Data.Depth=%s  Depth=%s', ...
            num2strCode(size(Data.Depth)), num2strCode(size(Mask)));
        my_warndlg(Lang(str1,str2), 1);
        return
    end
end

%% On regarde si l'utilisateur demande la sauvegarde de Depth

[varargin, Depth] = getPropertyValue(varargin, 'Depth', []);
if ~isempty(Depth)
    if isequal(size(Data.Depth), size(Depth))
        Data.Depth = single(Depth);
    else
        str1 = sprintf('La taille des matrices doit �tre la m�me.  Data.Depth=%s  Depth=%s', ...
            num2strCode(size(Data.Depth)), num2strCode(size(Depth)));
        str2 = sprintf('The matrices must be the same sizer.  Data.Depth=%s  Depth=%s', ...
            num2strCode(size(Data.Depth)), num2strCode(size(Depth)));
        my_warndlg(Lang(str1,str2), 1);
        return
    end
end

%% On regarde si l'utilisateur demande la sauvegarde de AcrossDist

[varargin, AcrossDist] = getPropertyValue(varargin, 'AcrossDist', []);
if ~isempty(AcrossDist)
    if isequal(size(Data.AcrossDist), size(AcrossDist))
        Data.AcrossDist = single(AcrossDist);
    else
        str = sprintf('La taille des matrices doit etre la m�me.  Data.AcrossDist=%s  AcrossDist=%s', ...
            num2strCode(size(Data.AcrossDist)), num2strCode(size(AcrossDist)));
        my_warndlg(str, 1);
        return
    end
end

%% On regarde si l'utilisateur demande la sauvegarde de AlongDist

[varargin, AlongDist] = getPropertyValue(varargin, 'AlongDist', []);
if ~isempty(AlongDist)
    if isequal(size(Data.AlongDist), size(AlongDist))
        Data.AlongDist = single(AlongDist);
    else
        str = sprintf('La taille des matrices doit etre la m�me.  Data.AlongDist=%s  AlongDist=%s', ...
            num2strCode(size(Data.AlongDist)), num2strCode(size(AlongDist)));
        my_warndlg(str, 1);
        return
    end
end

%% On regarde si l'utilisateur demande la sauvegarde de BeamDepressionAngle

[varargin, BeamDepressionAngle] = getPropertyValue(varargin, 'BeamDepressionAngle', []);
if ~isempty(BeamDepressionAngle)
    if isequal(size(Data.BeamDepressionAngle), size(BeamDepressionAngle))
        Data.BeamDepressionAngle = single(BeamDepressionAngle);
    else
        str = sprintf('La taille des matrices doit etre la m�me.  Data.BeamDepressionAngle=%s  BeamDepressionAngle=%s', ...
            num2strCode(size(Data.BeamDepressionAngle)), num2strCode(size(BeamDepressionAngle)));
        my_warndlg(str, 1);
        return
    end
end

%% On regarde si l'utilisateur demande la sauvegarde de BeamAzimuthAngle

[varargin, BeamAzimuthAngle] = getPropertyValue(varargin, 'BeamAzimuthAngle', []);
if ~isempty(BeamAzimuthAngle)
    if isequal(size(Data.BeamAzimuthAngle), size(BeamAzimuthAngle))
        Data.BeamAzimuthAngle = single(BeamAzimuthAngle);
    else
        str = sprintf('La taille des matrices doit etre la m�me.  Data.BeamAzimuthAngle=%s  BeamAzimuthAngle=%s', ...
            num2strCode(size(Data.BeamAzimuthAngle)), num2strCode(size(BeamAzimuthAngle)));
        my_warndlg(str, 1);
        return
    end
end

%% On regarde si l'utilisateur demande la sauvegarde de Range

[varargin, Range] = getPropertyValue(varargin, 'Range', []);
if ~isempty(Range)
    if isequal(size(Data.Range), size(Range))
        Data.Range = single(Range);
    else
        str = sprintf('La taille des matrices doit etre la m�me.  Data.Range=%s  Range=%s', ...
            num2strCode(size(Data.Range)), num2strCode(size(Range)));
        my_warndlg(str, 1);
        return
    end
end

%% On regarde si l'utilisateur demande la sauvegarde de QualityFactor

[varargin, QualityFactor] = getPropertyValue(varargin, 'QualityFactor', []);
if ~isempty(QualityFactor)
    if isequal(size(Data.QualityFactor), size(QualityFactor))
        Data.QualityFactor = uint8(QualityFactor);  % uint8 ??????????????????
    else
        str = sprintf('La taille des matrices doit etre la m�me.  Data.QualityFactor=%s  QualityFactor=%s', ...
            num2strCode(size(Data.QualityFactor)), num2strCode(size(QualityFactor)));
        my_warndlg(str, 1);
        return
    end
end

%% On regarde si l'utilisateur demande la sauvegarde de Reflectivity

[varargin, Reflectivity] = getPropertyValue(varargin, 'Reflectivity', []); %#ok<ASGLU>
if ~isempty(Reflectivity)
    if isequal(size(Data.Reflectivity), size(Reflectivity))
        Data.Reflectivity = single(Reflectivity);
    else
        str = sprintf('The size of the matrices must be the same.  Data.Reflectivity=%s  Reflectivity=%s', ...
            num2strCode(size(Data.Reflectivity)), num2strCode(size(Reflectivity)));
        my_warndlg(str, 1);
        return
    end
end

%% Sauvegarde de la donn�e dans un fichier .mat

[nomDir, nomFic] = fileparts(this.nomFic);
nomFicMat  = fullfile(nomDir, 'SonarScope', [num2str(identSondeur) '_' nomFic '_depth.mat']);
ncFileName = fullfile(nomDir, 'SonarScope', [nomFic '.nc']);
if ~isempty(nomFicMat)
    try
        if useCacheNetcdf == 1 % 1=Mat, 2 et 3=Netcdf
            save(nomFicMat, 'Data')
        else
            [Data.Dimensions.nbPings, Data.Dimensions.nbBeams] = size(Data.BeamDepressionAngle);
            NetcdfUtils.Struct2Netcdf(Data, ncFileName, 'Depth');
        end
    catch
        str = sprintf('Erreur ecriture fichier %s', nomFicMat);
        my_warndlg(str, 1);
    end
end
