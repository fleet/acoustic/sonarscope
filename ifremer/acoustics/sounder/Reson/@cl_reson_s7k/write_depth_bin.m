function flag = write_depth_bin(a, identSondeur, varargin)

[varargin, Data]            = getPropertyValue(varargin, 'Data',                   []);
[~, PointerOnSonarSettings] = getPropertyValue(varargin, 'PointerOnSonarSettings', []);

%% Lecture de la donn�e Depth dans les anciens dormats SonarScope

if isempty(Data)
    [flag, Data, identSondeur] = read_depth_s7k(a);
    if ~flag
        return
    end
end

[nbPings, nbBeams] = size(Data.Depth);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% AUX il faut trouver la
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% correspondance

if isempty(PointerOnSonarSettings)
    Data.PointerOnSonarSettings = 1:nbPings;
else
    Data.PointerOnSonarSettings = PointerOnSonarSettings;
end


%% Cr�ation de la structure d�crivant la donn�e

Info.Title                  = 'Depth';
Info.Model                  = sprintf('Reson %d', identSondeur);
Info.SystemSerialNumber     = Data.SystemSerialNumber;
Info.nbPings                = nbPings;
Info.nbBeams                = nbBeams;
Info.Comments               = 'Sounder ping rate';

Info.Signals                = [];
Info.SignalsPointer         = [];

Info.Signals(end+1).Name      = 'PointerOnSonarSettings';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = '';
Info.Signals(end).Direction	  = 'FirstValue=FirstPing';
Info.Signals(end).FileName    = fullfile('Depth', 'PointerOnSonarSettings.bin');
Info.Signals(end).Tag         = verifKeyWord('SounderPointerOnSonarSettings');

Info.Signals(end+1).Name      = 'Time';
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = 'days since JC';
Info.Signals(end).Direction	= 'FirstValue=FirstPing';
% Info.Signals(end).FileName    = fullfile('Depth', 'Time.bin');
Info.Signals(end).FileName    = fullfile('Depth', 'Time.bin');
Info.Signals(end).Tag         = verifKeyWord('SounderTime');

Info.Signals(end+1).Name      = 'PingCounter';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).Direction   = 'FirstValue=FirstPing';
Info.Signals(end).FileName    = fullfile('Depth', 'PingCounter.bin');
Info.Signals(end).Tag         = verifKeyWord('SounderPingCounter');
% Info.SignalsPointer(end+1).Name  = 'PingCounter';
% Info.SignalsPointer(end).Storage = 'SonarSettings';

Info.Signals(end+1).Name      = 'MultiPingSequence';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).Direction   = 'FirstValue=FirstPing';
Info.Signals(end).FileName    = fullfile('Depth', 'MultiPingSequence.bin');
Info.Signals(end).Tag         = verifKeyWord('SounderPingSequence');
% Info.SignalsPointer(end+1).Name  = 'MultiPingSequence';
% Info.SignalsPointer(end).Storage = 'SonarSettings';

Info.Signals(end+1).Name      = 'SoundSpeed';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'm/s';
Info.Signals(end).Direction   = 'FirstValue=FirstPing';
Info.Signals(end).FileName    = fullfile('Depth', 'SoundSpeed.bin');
Info.Signals(end).Tag         = verifKeyWord('SounderSoundSpeed');

Info.Signals(end+1).Name      = 'TransducerDepth';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'm';
Info.Signals(end).Direction   = 'FirstValue=FirstPing';
Info.Signals(end).FileName    = fullfile('Depth', 'TransducerDepth.bin');
Info.Signals(end).Tag         = verifKeyWord('SounderHeight');

Info.Signals(end+1).Name      = 'NbBeams';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).Direction   = 'FirstValue=FirstPing';
Info.Signals(end).FileName    = fullfile('Depth', 'NbBeams.bin');
Info.Signals(end).Tag         = verifKeyWord('SounderNbBeams');

% Info.Signals(end+1).Name      = 'SampleRate';
% Info.Signals(end).Storage     = 'single';
% Info.Signals(end).Unit        = 'Hz';
% Info.Signals(end).Direction   = 'FirstValue=FirstPing';
% Info.Signals(end).FileName    = fullfile('Depth', 'SamplingRate.bin');
% Info.Signals(end).Tag         = verifKeyWord('SounderSamplingRate');
Info.SignalsPointer(end+1).Name  = 'SampleRate';
Info.SignalsPointer(end).Storage = 'SonarSettings';

% Info.Signals(end+1).Name      = 'TransducerDepthOffsetMultiplier';
% Info.Signals(end).Storage     = 'single';
% Info.Signals(end).Unit        = '';
% Info.Signals(end).Direction   = 'FirstValue=FirstPing';
% Info.Signals(end).FileName    = fullfile('Depth', 'TransducerDepthOffsetMultiplier.bin');
% Info.Signals(end).Tag         = verifKeyWord('Auxillary');

if isfield(Data, 'Roll')
    Info.Signals(end+1).Name      = 'Roll';
    Info.Signals(end).Storage     = 'single';
    Info.Signals(end).Unit        = 'deg';
    Info.Signals(end).Direction   = 'FirstValue=FirstPing';
    Info.Signals(end).FileName    = fullfile('Depth', 'Roll.bin');
    Info.Signals(end).Tag         = verifKeyWord('SounderRoll');
    
    Info.Signals(end+1).Name      = 'Pitch';
    Info.Signals(end).Storage     = 'single';
    Info.Signals(end).Unit        = 'deg';
    Info.Signals(end).Direction   = 'FirstValue=FirstPing';
    Info.Signals(end).FileName    = fullfile('Depth', 'Pitch.bin');
    Info.Signals(end).Tag         = verifKeyWord('SounderPitch');
    
    Info.Signals(end+1).Name      = 'Heave';
    Info.Signals(end).Storage     = 'single';
    Info.Signals(end).Unit        = 'm';
    Info.Signals(end).Direction   = 'FirstValue=FirstPing';
    Info.Signals(end).FileName    = fullfile('Depth', 'Heave.bin');
    Info.Signals(end).Tag         = verifKeyWord('SounderHeave');
    
    Info.Signals(end+1).Name      = 'Heading';
    Info.Signals(end).Storage     = 'single';
    Info.Signals(end).Unit        = 'deg';
    Info.Signals(end).Direction   = 'FirstValue=FirstPing';
    Info.Signals(end).FileName    = fullfile('Depth', 'Heading.bin');
    Info.Signals(end).Tag         = verifKeyWord('SounderHeading');
    
    Info.Signals(end+1).Name      = 'Frequency';
    Info.Signals(end).Storage     = 'single';
    Info.Signals(end).Unit        = 'Hz';
    Info.Signals(end).Direction   = 'FirstValue=FirstPing';
    Info.Signals(end).FileName    = fullfile('Depth', 'Frequency.bin');
    Info.Signals(end).Tag         = verifKeyWord('SounderFrequency');
    
    Info.Signals(end+1).Name      = 'Latitude';
    Info.Signals(end).Storage     = 'double';
    Info.Signals(end).Unit        = 'deg';
    Info.Signals(end).Direction   = 'FirstValue=FirstPing';
    Info.Signals(end).FileName    = fullfile('Depth', 'Latitude.bin');
    Info.Signals(end).Tag         = verifKeyWord('SounderLatitude');
    
    Info.Signals(end+1).Name      = 'Longitude';
    Info.Signals(end).Storage     = 'double';
    Info.Signals(end).Unit        = 'deg';
    Info.Signals(end).Direction   = 'FirstValue=FirstPing';
    Info.Signals(end).FileName    = fullfile('Depth', 'Longitude.bin');
    Info.Signals(end).Tag         = verifKeyWord('SounderLongitude');
    
    Info.Signals(end+1).Name      = 'Tide';
    Info.Signals(end).Storage     = 'single';
    Info.Signals(end).Unit        = 'm';
    Info.Signals(end).Direction   = 'FirstValue=FirstPing';
    Info.Signals(end).FileName    = fullfile('Depth', 'Tide.bin');
    Info.Signals(end).Tag         = verifKeyWord('SounderTide');
    
    Info.Signals(end+1).Name      = 'VehicleDepth';
    Info.Signals(end).Storage     = 'single';
    Info.Signals(end).Unit        = 'm';
    Info.Signals(end).Direction   = 'FirstValue=FirstPing';
    Info.Signals(end).FileName    = fullfile('Depth', 'VehicleDepth.bin');
    Info.Signals(end).Tag         = verifKeyWord('SounderVehicleDepth');
    
    Info.Signals(end+1).Name      = 'HeightSource';
    Info.Signals(end).Storage     = 'single';
    Info.Signals(end).Unit        = '';
    Info.Signals(end).Direction   = 'FirstValue=FirstPing';
    Info.Signals(end).FileName    = fullfile('Depth', 'HeightSource.bin');
    Info.Signals(end).Tag         = verifKeyWord('SounderHeightSource');
    
    %     Info.Signals(end+1).Name      = 'ProjectorBeamSteeringAngleVertical';
    %     Info.Signals(end).Storage     = 'single';
    %     Info.Signals(end).Unit        = 'deg';
    %     Info.Signals(end).Direction   = 'FirstValue=FirstPing';
    %     Info.Signals(end).FileName    = fullfile('Depth', 'ProjectorBeamSteeringAngleVertical.bin');
    %     Info.Signals(end).Tag         = verifKeyWord('SounderProjectorBeamSteeringAngleVertical');
    Info.SignalsPointer(end+1).Name  = 'ProjectorBeamSteeringAngleVertical';
    Info.SignalsPointer(end).Storage = 'SonarSettings';
end

if isfield(Data, 'MinFilterInfo')
    Info.Signals(end+1).Name      = 'MinFilterInfo';
    Info.Signals(end).Storage     = 'single';
    Info.Signals(end).Unit        = 'Sample';
    Info.Signals(end).Direction   = 'FirstValue=FirstPing';
    Info.Signals(end).FileName    = fullfile('Depth', 'MinFilterInfo.bin');
    Info.Signals(end).Tag         = verifKeyWord('SounderMinFilterInfo');
    
    Info.Signals(end+1).Name      = 'MaxFilterInfo';
    Info.Signals(end).Storage     = 'single';
    Info.Signals(end).Unit        = 'Sample';
    Info.Signals(end).Direction   = 'FirstValue=FirstPing';
    Info.Signals(end).FileName    = fullfile('Depth', 'MaxFilterInfo.bin');
    Info.Signals(end).Tag         = verifKeyWord('SounderMaxFilterInfo');
end

if isfield(Data, 'TxRollCompensationFlag')
    %     Info.Signals(end+1).Name      = 'TxRollCompensationFlag';
    %     Info.Signals(end).Storage     = 'single';
    %     Info.Signals(end).Unit        = '';
    %     Info.Signals(end).Direction   = 'FirstValue=FirstPing';
    %     Info.Signals(end).FileName    = fullfile('Depth', 'TxRollCompensationFlag.bin');
    %     Info.Signals(end).Tag         = verifKeyWord('SounderTxRollCompensationFlag');
    Info.SignalsPointer(end+1).Name  = 'TxRollCompensationFlag';
    Info.SignalsPointer(end).Storage = 'SonarSettings';
    
    %     Info.Signals(end+1).Name      = 'TxRollCompensationValue';
    %     Info.Signals(end).Storage     = 'single';
    %     Info.Signals(end).Unit        = 'deg';
    %     Info.Signals(end).Direction   = 'FirstValue=FirstPing';
    %     Info.Signals(end).FileName    = fullfile('Depth', 'TxRollCompensationValue.bin');
    %     Info.Signals(end).Tag         = verifKeyWord('SounderTxRollCompensationValue');
    Info.SignalsPointer(end+1).Name  = 'TxRollCompensationValue';
    Info.SignalsPointer(end).Storage = 'SonarSettings';
    
    %     Info.Signals(end+1).Name      = 'TxPitchCompensationFlag';
    %     Info.Signals(end).Storage     = 'single';
    %     Info.Signals(end).Unit        = '';
    %     Info.Signals(end).Direction   = 'FirstValue=FirstPing';
    %     Info.Signals(end).FileName    = fullfile('Depth', 'TxPitchCompensationFlag.bin');
    %     Info.Signals(end).Tag         = verifKeyWord('SounderTxPitchCompensationFlag');
    Info.SignalsPointer(end+1).Name  = 'TxPitchCompensationFlag';
    Info.SignalsPointer(end).Storage = 'SonarSettings';
    
    %     Info.Signals(end+1).Name      = 'TxPitchCompensationValue';
    %     Info.Signals(end).Storage     = 'single';
    %     Info.Signals(end).Unit        = 'deg';
    %     Info.Signals(end).Direction   = 'FirstValue=FirstPing';
    %     Info.Signals(end).FileName    = fullfile('Depth', 'TxPitchCompensationValue.bin');
    %     Info.Signals(end).Tag         = verifKeyWord('SounderTxPitchCompensationValue');
    Info.SignalsPointer(end+1).Name  = 'TxPitchCompensationValue';
    Info.SignalsPointer(end).Storage = 'SonarSettings';
    
    %     Info.Signals(end+1).Name      = 'RxRollCompensationFlag';
    %     Info.Signals(end).Storage     = 'single';
    %     Info.Signals(end).Unit        = '';
    %     Info.Signals(end).Direction   = 'FirstValue=FirstPing';
    %     Info.Signals(end).FileName    = fullfile('Depth', 'RxRollCompensationFlag.bin');
    %     Info.Signals(end).Tag         = verifKeyWord('SounderRxRollCompensationFlag');
    Info.SignalsPointer(end+1).Name  = 'RxRollCompensationFlag';
    Info.SignalsPointer(end).Storage = 'SonarSettings';
    
    %     Info.Signals(end+1).Name      = 'RxRollCompensationValue';
    %     Info.Signals(end).Storage     = 'single';
    %     Info.Signals(end).Unit        = 'deg';
    %     Info.Signals(end).Direction   = 'FirstValue=FirstPing';
    %     Info.Signals(end).FileName    = fullfile('Depth', 'RxRollCompensationValue.bin');
    %     Info.Signals(end).Tag         = verifKeyWord('SounderRxRollCompensationValue');
    Info.SignalsPointer(end+1).Name  = 'RxRollCompensationValue';
    Info.SignalsPointer(end).Storage = 'SonarSettings';
    
    %     Info.Signals(end+1).Name      = 'RxPitchCompensationFlag';
    %     Info.Signals(end).Storage     = 'single';
    %     Info.Signals(end).Unit        = '';
    %     Info.Signals(end).Direction   = 'FirstValue=FirstPing';
    %     Info.Signals(end).FileName    = fullfile('Depth', 'RxPitchCompensationFlag.bin');
    %     Info.Signals(end).Tag         = verifKeyWord('SounderRxPitchCompensationFlag');
    Info.SignalsPointer(end+1).Name  = 'RxPitchCompensationFlag';
    Info.SignalsPointer(end).Storage = 'SonarSettings';
    
    %     Info.Signals(end+1).Name      = 'RxPitchCompensationValue';
    %     Info.Signals(end).Storage     = 'single';
    %     Info.Signals(end).Unit        = 'deg';
    %     Info.Signals(end).Direction   = 'FirstValue=FirstPing';
    %     Info.Signals(end).FileName    = fullfile('Depth', 'RxPitchCompensationValue.bin');
    %     Info.Signals(end).Tag         = verifKeyWord('SounderRxPitchCompensationValue');
    Info.SignalsPointer(end+1).Name  = 'RxPitchCompensationValue';
    Info.SignalsPointer(end).Storage = 'SonarSettings';
    
    %     Info.Signals(end+1).Name      = 'RxHeaveCompensationFlag';
    %     Info.Signals(end).Storage     = 'single';
    %     Info.Signals(end).Unit        = '';
    %     Info.Signals(end).Direction   = 'FirstValue=FirstPing';
    %     Info.Signals(end).FileName    = fullfile('Depth', 'RxHeaveCompensationFlag.bin');
    %     Info.Signals(end).Tag         = verifKeyWord('SounderRxHeaveCompensationFlag');
    Info.SignalsPointer(end+1).Name  = 'RxHeaveCompensationFlag';
    Info.SignalsPointer(end).Storage = 'SonarSettings';
    
    %     Info.Signals(end+1).Name      = 'RxHeaveCompensationValue';
    %     Info.Signals(end).Storage     = 'single';
    %     Info.Signals(end).Unit        = 'm';
    %     Info.Signals(end).Direction   = 'FirstValue=FirstPing';
    %     Info.Signals(end).FileName    = fullfile('Depth', 'RxHeaveCompensationValue.bin');
    %     Info.Signals(end).Tag         = verifKeyWord('SounderRxHeaveCompensationValue');
    Info.SignalsPointer(end+1).Name  = 'RxHeaveCompensationValue';
    Info.SignalsPointer(end).Storage = 'SonarSettings';
end


Info.Images(1).Name        = 'Depth';
Info.Images(1).Storage     = 'single';
Info.Images(1).Unit        = 'm';
Info.Images(1).Direction   = 'FirstValue=FirstPing';
Info.Images(1).FileName    = fullfile('Depth', 'Depth.bin');
Info.Images(1).Tag         = verifKeyWord('SounderDepth');

Info.Images(end+1).Name      = 'AcrossDist';
Info.Images(end).Storage     = 'single';
Info.Images(end).Unit        = 'm';
Info.Images(end).Direction   = 'FirstValue=FirstPing';
Info.Images(end).FileName    = fullfile('Depth', 'AcrossDist.bin');
Info.Images(1).Tag           = verifKeyWord('SounderAcrossDist');

Info.Images(end+1).Name      = 'AlongDist';
Info.Images(end).Storage     = 'single';
Info.Images(end).Unit        = 'm';
Info.Images(end).Direction   = 'FirstValue=FirstPing';
Info.Images(end).FileName    = fullfile('Depth', 'AlongDist.bin');
Info.Images(end).Tag         = verifKeyWord('SounderAlongDist');

Info.Images(end+1).Name      = 'BeamDepressionAngle';
Info.Images(end).Storage     = 'single';
Info.Images(end).Unit        = 'm';
Info.Images(end).Direction   = 'FirstValue=FirstPing';
Info.Images(end).FileName    = fullfile('Depth', 'BeamDepressionAngle.bin');
Info.Images(end).Tag         = verifKeyWord('SounderBeamAngle');

Info.Images(end+1).Name      = 'BeamAzimuthAngle';
Info.Images(end).Storage     = 'single';
Info.Images(end).Unit        = 'm';
Info.Images(end).Direction   = 'FirstValue=FirstPing';
Info.Images(end).FileName    = fullfile('Depth', 'BeamAzimuthAngle.bin');
Info.Images(end).Tag         = verifKeyWord('SounderAzimuthAngle');

Info.Images(end+1).Name      = 'Range';
Info.Images(end).Storage     = 'single';
% Devrait �tre chang� car �a n'a pas trop de sens
Info.Images(end).Unit          = 'One way traveltime in samples';
Info.Images(end).Direction   = 'FirstValue=FirstPing';
Info.Images(end).FileName    = fullfile('Depth', 'Range.bin');
Info.Images(end).Tag         = verifKeyWord('SounderTwoWayTravelTimeInSamples');

Info.Images(end+1).Name      = 'QualityFactor';
Info.Images(end).Storage     = 'single';
Info.Images(end).Unit        = '';
Info.Images(end).Direction   = 'FirstValue=FirstPing';
Info.Images(end).FileName    = fullfile('Depth', 'QualityFactor.bin');
Info.Images(end).Tag         = verifKeyWord('SounderSimradQF');

% Info.Images(end+1).Name      = 'LengthDetection';
% Info.Images(end).Storage     = 'single';
% Info.Images(end).Unit        = 'm';
% Info.Images(end).Direction   = 'FirstValue=FirstPing';
% Info.Images(end).FileName    = fullfile('Depth', 'LengthDetection.bin');
% Info.Images(end).Tag         = verifKeyWord('SounderDetectionNbSamples');

Info.Images(end+1).Name      = 'Reflectivity';
Info.Images(end).Storage     = 'single';
Info.Images(end).Unit        = 'dB';
Info.Images(end).Direction   = 'FirstValue=FirstPing';
Info.Images(end).FileName    = fullfile('Depth', 'Reflectivity.bin');
Info.Images(end).Tag         = verifKeyWord('SounderReflectivity');

% Provient des datagrammes 7004
Info.Images(end+1).Name      = 'BeamVerticalDirectionAngle';
Info.Images(end).Storage     = 'single';
Info.Images(end).Unit        = 'deg';
Info.Images(end).Direction   = 'FirstValue=FirstPing';
Info.Images(end).FileName    = fullfile('Depth', 'BeamVerticalDirectionAngle.bin');
Info.Images(end).Tag         = verifKeyWord('SounderBeamVerticalDirectionAngle');

% Provient des datagrammes 7004
Info.Images(end+1).Name      = 'BeamHorizontalDirectionAngle';
Info.Images(end).Storage     = 'single';
Info.Images(end).Unit        = 'deg';
Info.Images(end).Direction   = 'FirstValue=FirstPing';
Info.Images(end).FileName    = fullfile('Depth', 'BeamHorizontalDirectionAngle.bin');
Info.Images(end).Tag         = verifKeyWord('SounderBeamHorizontalDirectionAngle');

% Provient des datagrammes 7004
Info.Images(end+1).Name      = 's_3dBBeamWithY';
Info.Images(end).Storage     = 'single';
Info.Images(end).Unit        = 'deg';
Info.Images(end).Direction   = 'FirstValue=FirstPing';
Info.Images(end).FileName    = fullfile('Depth', 's_3dBBeamWithY.bin');
Info.Images(end).Tag         = verifKeyWord('Sounders_3dBBeamWithY');

% Provient des datagrammes 7004
Info.Images(end+1).Name      = 's_3dBBeamWithX';
Info.Images(end).Storage     = 'single';
Info.Images(end).Unit        = 'deg';
Info.Images(end).Direction   = 'FirstValue=FirstPing';
Info.Images(end).FileName    = fullfile('Depth', 's_3dBBeamWithX.bin');
Info.Images(end).Tag         = verifKeyWord('Sounders_3dBBeamWithX');

Info.Images(end+1).Name      = 'Mask';
Info.Images(end).Storage     = 'single';
Info.Images(end).Unit        = '';
Info.Images(end).Direction   = 'FirstValue=FirstPing';
Info.Images(end).FileName    = fullfile('Depth', 'Mask.bin');
Info.Images(end).Tag         = verifKeyWord('SounderMask');

%% Cr�ation du r�pertoire SonarScope

[nomDir, nom] = fileparts(a.nomFic);
nomDir = fullfile(nomDir, 'SonarScope', nom);
if ~exist(nomDir, 'dir')
    status = mkdir(nomDir);
    if ~status
        messageErreur(nomDir)
        flag = 0;
        return
    end
end

%% Cr�ation du fichier XML d�crivant la donn�e

nomFicXml = fullfile(nomDir, 'Depth.xml');
xml_write(nomFicXml, Info);
flag = exist(nomFicXml, 'file');
if ~flag
    messageErreur(nomFicXml)
    return
end

%% Cr�ation du r�pertoire Depth

nomDirDepth = fullfile(nomDir, 'Depth');
if ~exist(nomDirDepth, 'dir')
    status = mkdir(nomDirDepth);
    if ~status
        messageErreur(nomDirDepth)
        flag = 0;
        return
    end
end

%% Cr�ation des fichiers binaires des signaux

for i=1:length(Info.Signals)
    flag = writeSignal(nomDir, Info.Signals(i), Data.(Info.Signals(i).Name));
    if ~flag
        return
    end
end

%% Cr�ation des fichiers binaires des images

for i=1:length(Info.Images)
    flag = writeImage(nomDir, Info.Images(i), Data.(Info.Images(i).Name));
    if ~flag
        return
    end
end
