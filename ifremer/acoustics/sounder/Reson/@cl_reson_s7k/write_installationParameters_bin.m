function flag = write_installationParameters_bin(this, identSondeur, varargin)

[varargin, Data] = getPropertyValue(varargin, 'Data', []); %#ok<ASGLU>

%% Lecture de la donn�e InstallationParameters dans les anciens formats SonarScope

if isempty(Data)
    Data = read_sonarInstallationParameters(this, identSondeur);
    if isempty(Data) || isempty(Data.Time) || (isa(Data.Time, 'cl_time') && isempty(Data.Time.timeMat))
        flag = 0;
        return
    end
end

%% Cr�ation de la structure d�crivant la donn�e

Info.Title              = 'InstallationParameters';
Info.Model              = '?????????????????????????????????????????';
Info.SystemSerialNumber = '?????????????????????????????????????????'; %Data.SystemSerialNumber;
Info.Comments           = 'One per file ???????????????????????????????';

% Info.SurveyLineNumber = Data.SurveyLineNumber;
if ischar(Data.Time)
    Info.Time = Data.Time;
elseif isa(Data.Time, 'double')
    Data.Time = cl_time('timeMat', Data.Time);
else
    Info.Time = t2str(Data.Time);
end
% Info.SerialNumberOfSecondSonarHead = Data.SerialNumberOfSecondSonarHead;

names = fieldnames(Data);
for k=1:length(names)
    Info.(names{k}) = Data.(names{k});
end

%% Cr�ation du r�pertoire SonarScope

% Modif JMA le 18/01/2021 (pour ne plus cr�er de r�pertoires par .s7k)
[nomDir, nom] = fileparts(this.nomFic); %#ok<ASGLU>
% nomDir = fullfile(nomDir, 'SonarScope', nom);
% if ~exist(nomDir, 'dir')
%     status = mkdir(nomDir);
%     if ~status
%         messageErreur(nomDir)
%         flag = 0;
%         return
%     end
% end

%% Cr�ation du fichier XML d�crivant la donn�e

nomFicXml = fullfile(nomDir, 'InstallationParameters.xml');
xml_write(nomFicXml, Info);
flag = exist(nomFicXml, 'file');
if ~flag
    messageErreur(nomFicXml)
    return
end

%% Cr�ation du fichier XML "InstallationParameters_xxkHz.xml" sur le r�pertoire des donn�es pour une utilisation plus facile des WC

nomDir = fileparts(nomDir);
nomDirSave = nomDir;
nomDir = fileparts(nomDir);
if (length(nomDir) == 3) && strcmp(nomDir(2:3), ':\')
   nomDir = nomDirSave;
end

Freq = Info.Frequency / 1000;
if Freq > 200 % == 396
    nomFicXml = fullfile(nomDir, 'InstallationParameters_7125.xml');
elseif Freq == 100
    nomFicXml = fullfile(nomDir, 'InstallationParameters_7111.xml');
elseif Freq > ((12+24)/2)
    nomFicXml = fullfile(nomDir, 'InstallationParameters_24kHz.xml');
else
    nomFicXml = fullfile(nomDir, 'InstallationParameters_12kHz.xml');
end

try
    xml_write(nomFicXml, Info);
    flag = exist(nomFicXml, 'file');
    if ~flag
        messageErreur(nomFicXml)
        return
    end
catch
    if AnneeVersion < 2021
        str = jsonencode(Info);
    else
        str = jsonencode(Info, 'PrettyPrint', true);
    end
    nomFicJs = strrep(nomFicXml, '.xml', '.json');
    fid = fopen(nomFicJs, 'w+');
    if fid ~= 1
        fprintf(fid, '%s', str);
        fclose(fid);
    end
end

% [flag, pppp] = read_installationParameters_bin(this)
