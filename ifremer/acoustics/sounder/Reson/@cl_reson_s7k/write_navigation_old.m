function Data = write_navigation_old(this, identSondeur, timeMat, Latitude, Longitude, Heading, VesselHeight, ...
    Speed, CourseVessel, HeightAccuracy, HorizontalPositionAccuracy)

global useCacheNetcdf %#ok<GVMIS>

%% Check the number of elements

flag = checkIfOneElement(length(this));
if ~flag
    Data = [];
    return
end

%% Lecture de la navigation

Data = read_navigation(this, identSondeur);

Data.Latitude                   = Latitude;
Data.Longitude                  = Longitude;
Data.VesselHeight               = VesselHeight;
Data.Heading                    = Heading;
Data.Speed                      = single(Speed);
Data.CourseVessel               = single(CourseVessel);
Data.HeightAccuracy             = HeightAccuracy;
Data.HorizontalPositionAccuracy = HorizontalPositionAccuracy;

Data.Time = cl_time('timeMat', datenum(timeMat));

%% Sauvegarde du fichier de navigation

[nomDir, nomFic] = fileparts(this.nomFic);
nomFicMat  = fullfile(nomDir, 'SonarScope', [nomFic '_navigation.mat']);
ncFileName = fullfile(nomDir, 'SonarScope', [nomFic '.nc']);

if useCacheNetcdf == 1 % 1=Mat, 2 et 3=Netcdf
    save(nomFicMat, 'Data')
else
    Data.Dimensions.nbSamples = length(Data.Latitude);
    NetcdfUtils.Struct2Netcdf(Data, ncFileName, 'Navigation');
end

