% Lecture des datagrams "depth" dans un fichier .all
%
% Syntax
%   Data = write_signaux(a)
%
% Input Arguments
%   a : Instance de cl_reson_s7k
%
% Output Arguments
%   Data : Structure contenant les
%       Heading            : Cap du navire (deg)
%       PingCounter        : Numeor de Ping
%       EmModel            : Numero du model su dondeur
%       SystemSerialNumber : Numero de serie du sondeur%   PingCounter        : Numeor de Ping
%
% Examples
%   nomFicIn  = getNomFicDatabase('RESON7125_20051116_221245.s7k')
%   nomFicOut = [my_tempdir filesep 'toto.s7k']
%   copyfile(nomFicIn, nomFicOut)
%   a = cl_reson_s7k('nomFic', nomFicOut);
%   [flag, Data, identSondeur] = read_depth_s7k(a);
%   figure; imagesc(Data.Depth); colorbar
%   histo1D(Data.Depth)
%
%   [MasqueDetection, MasqueDetectionNaN] = cleanDetection(Data.QualityFactor);
%   figure; imagesc(MasqueDetection); colorbar
%   figure; imagesc(MasqueDetectionNaN); colorbar
%   figure; imagesc(Data.Depth .* MasqueDetectionNaN); colorbar
%   histo1D(Data.Depth .* MasqueDetectionNaN)
%
%   [MasqueDepth, MasqueDepthNaN] = cleanDepth(Data.Depth);
%   figure; imagesc(MasqueDepth); colorbar
%   figure; imagesc(MasqueDepthNaN); colorbar
%   Depth = Data.Depth .* MasqueDetectionNaN .* MasqueDepthNaN;
%   figure; imagesc(Data.Depth .* MasqueDetectionNaN .* MasqueDepthNaN); colorbar
%   histo1D(Data.Depth .* MasqueDetectionNaN .* MasqueDepthNaN)
%
%   write_masque(a, Data.Masque | MasqueDetection | MasqueDepth)
%   write_signaux(a, 'Depth', Depth)
%
%   nomFicVerif = [my_tempdir filesep 'titi.all']
%   copyfile(nomFicOut, nomFicVerif)
%   b = cl_reson_s7k('nomFic', nomFicVerif);
%   [flag, X, identSondeur] = read_depth_s7k(b);
%   figure; imagesc(X.Depth); colorbar
%
% See also cl_reson_s7k cl_reson_s7k/plot_position cl_reson_s7k/plot_nav Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function write_signaux(this, identSondeur, varargin)

global useCacheNetcdf %#ok<GVMIS>

%% Check the number of elements

flag = checkIfOneElement(length(this));
if ~flag
    return
end

%% On regarde si l'utilisateur a donne l'ensemble de la structure Data

[varargin, Data] = getPropertyValue(varargin, 'Data', []);

if isempty(Data)
    [flag, Data, identSondeur] = read_depth_s7k(this);
    if ~flag
        return
    end
end

%% On regarde si l'utilisateur demande la sauvegarde de Heading

[varargin, X] = getPropertyValue(varargin, 'Heading', []);
if ~isempty(X)
    %     if isequal(size(Data.Heading), size(X))
    if isequal(length(Data.Heading), length(X))
        Data.Heading(:) = single(X(:));
    else
        str = sprintf('La taille des matrices doit etre la m�me.  Data.Heading=%s  Heading=%s', ...
            num2strCode(size(Data.Heading)), num2strCode(size(X)));
        my_warndlg(str, 1);
        return
    end
end

%% On regarde si l'utilisateur demande la sauvegarde de SoundSpeed

[varargin, X] = getPropertyValue(varargin, 'SoundSpeed', []);
if ~isempty(X)
    if isequal(length(Data.SoundSpeed), length(X))
        Data.SoundSpeed = single(X);
    else
        str = sprintf('La taille des matrices doit etre la m�me.  Data.SoundSpeed=%s  SoundSpeed=%s', ...
            num2strCode(size(Data.SoundSpeed)), num2strCode(size(X)));
        my_warndlg(str, 1);
        return
    end
end

%% On regarde si l'utilisateur demande la sauvegarde de Latitude

[varargin, X] = getPropertyValue(varargin, 'Latitude', []);
if ~isempty(X)
    if isequal(length(Data.Latitude), length(X))
        Data.Latitude = X;
    else
        str = sprintf('La taille des matrices doit etre la m�me.  Data.Latitude=%s  Latitude=%s', ...
            num2strCode(size(Data.Latitude)), num2strCode(size(X)));
        my_warndlg(str, 1);
        return
    end
end

%% On regarde si l'utilisateur demande la sauvegarde de Longitude

[varargin, X] = getPropertyValue(varargin, 'Longitude', []);
if ~isempty(X)
    if isequal(length(Data.Longitude), length(X))
        Data.Longitude = X;
    else
        str = sprintf('La taille des matrices doit etre la m�me.  Data.Longitude=%s  Longitude=%s', ...
            num2strCode(size(Data.Longitude)), num2strCode(size(X)));
        my_warndlg(str, 1);
        return
    end
end

%% On regarde si l'utilisateur demande la sauvegarde de Tide

[varargin, X] = getPropertyValue(varargin, 'Tide', []);
if ~isempty(X)
    if isequal(length(Data.Tide), length(X))
        Data.Tide = single(X);
    else
        str = sprintf('La taille des matrices doit etre la m�me.  Data.Tide=%s  Tide=%s', ...
            num2strCode(size(Data.Tide)), num2strCode(size(X)));
        my_warndlg(str, 1);
        return
    end
end

%% On regarde si l'utilisateur demande la sauvegarde de VehicleDepth

[varargin, X] = getPropertyValue(varargin, 'VehicleDepth', []);
if ~isempty(X)
    if isequal(length(Data.VehicleDepth), length(X))
        Data.VehicleDepth = single(X);
    else
        str = sprintf('La taille des matrices doit etre la m�me.  Data.VehicleDepth=%s  VehicleDepth=%s', ...
            num2strCode(size(Data.VehicleDepth)), num2strCode(size(X)));
        my_warndlg(str, 1);
        return
    end
end

if ~isempty(varargin)
    str = sprintf('%s pas encore prevu dans cl_reson_s7k/write_signaux', varargin{1});
    my_warndlg(str, 1);
end

% A FAIRE ?????????????????????????????
% Data.PingCounter
% Data.TransducerDepth
% Data.NbBeams
% Data.TransducerDepthOffsetMultiplier
% Data.Frequency
% Data.HeightSource
% Data.Roll
% Data.Pitch
% Data.Heave

%% Sauvegarde de la donnee dans un fichier .mat

[nomDir, nomFic] = fileparts(this.nomFic);
nomFicMat  = fullfile(nomDir, 'SonarScope', [num2str(identSondeur) '_' nomFic '_depth.mat']);
ncFileName = fullfile(nomDir, 'SonarScope', [nomFic '.nc']);

if ~isempty(nomFicMat)
    try
        if useCacheNetcdf == 1 % 1=Mat, 2 et 3=Netcdf
            save(nomFicMat, 'Data')
        else
            [Data.Dimensions.nbPings, Data.Dimensions.nbBeams] = size(Data.BeamDepressionAngle);
            NetcdfUtils.Struct2Netcdf(Data, ncFileName, 'Depth');
        end
    catch
        str = sprintf('Erreur ecriture fichier %s', nomFicMat);
        my_warndlg(str, 1);
    end
end
