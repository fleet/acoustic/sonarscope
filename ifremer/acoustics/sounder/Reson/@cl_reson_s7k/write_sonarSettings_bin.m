function flag = write_sonarSettings_bin(a, identSondeur)

%% Lecture de la donn�e SonarSettings dans les anciens dormats SonarScope

Data = read_sonarSettings(a, identSondeur);
if isempty(Data) || isempty(Data.Time) || isempty(Data.Time.timeMat)
    flag = 0;
    return
end

nbSamples = length(Data.Time.timeMat);

%% Cr�ation de la structure d�crivant la donn�e

Info.Title                  = 'SonarSettings';
Info.Model                  = sprintf('Reson %d', identSondeur);
Info.SystemSerialNumber     = Data.SystemSerialNumber;
Info.NbSamples              = nbSamples;
Info.Comments               = 'Sounder sampling rate';

Info.Signals(1).Name          = 'Time';
Info.Signals(1).Storage       = 'double';
Info.Signals(1).Unit          = 'days since JC';
Info.Signals(1).Direction	  = 'FirstValue=FirstPing';
Info.Signals(1).FileName      = fullfile('SonarSettings', 'Time.bin');
Info.Signals(1).Tag           = verifKeyWord('SounderTime');

Info.Signals(end+1).Name      = 'PingCounter';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).Direction   = 'FirstValue=FirstPing';
Info.Signals(end).FileName    = fullfile('SonarSettings', 'PingCounter.bin');
Info.Signals(end).Tag         = verifKeyWord('SounderPingCounter');

Info.Signals(end+1).Name      = 'Frequency';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'Hz';
Info.Signals(end).Direction   = 'FirstValue=FirstPing';
Info.Signals(end).FileName    = fullfile('SonarSettings', 'Frequency.bin');
Info.Signals(end).Tag         = verifKeyWord('SounderFrequency');

Info.Signals(end+1).Name      = 'PingSequence';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).Direction   = 'FirstValue=FirstPing';
Info.Signals(end).FileName    = fullfile('SonarSettings', 'MultiPingSequence.bin');
Info.Signals(end).Tag         = verifKeyWord('SounderPingSequence');

Info.Signals(end+1).Name      = 'SampleRate';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'Hz';
Info.Signals(end).Direction   = 'FirstValue=FirstPing';
Info.Signals(end).FileName    = fullfile('SonarSettings', 'SampleRate.bin');
Info.Signals(end).Tag         = verifKeyWord('SounderSamplingRate');

Info.Signals(end+1).Name      = 'ReceiverBandwith';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'Hz';
Info.Signals(end).Direction   = 'FirstValue=FirstPing';
Info.Signals(end).FileName    = fullfile('SonarSettings', 'ReceiverBandwith.bin');
Info.Signals(end).Tag         = verifKeyWord('SounderReceiverBandwith????');

Info.Signals(end+1).Name      = 'TxPulseWidth';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'ms';
Info.Signals(end).Direction   = 'FirstValue=FirstPing';
Info.Signals(end).FileName    = fullfile('SonarSettings', 'TxPulseWidth.bin');
Info.Signals(end).Tag         = verifKeyWord('SounderPulseLength');

Info.Signals(end+1).Name      = 'TxPulseTypeIdentifier';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).Direction   = 'FirstValue=FirstPing';
Info.Signals(end).FileName    = fullfile('SonarSettings', 'TxPulseTypeIdentifier.bin');
Info.Signals(end).Tag         = verifKeyWord('Auxillary');

Info.Signals(end+1).Name      = 'GainSelection';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '???';
Info.Signals(end).Direction   = 'FirstValue=FirstPing';
Info.Signals(end).FileName    = fullfile('SonarSettings', 'GainSelection.bin');
Info.Signals(end).Tag         = verifKeyWord('Auxillary');

Info.Signals(end+1).Name      = 'TxPulseEnvelopeParameter';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '???';
Info.Signals(end).Direction   = 'FirstValue=FirstPing';
Info.Signals(end).FileName    = fullfile('SonarSettings', 'TxPulseEnvelopeParameter.bin');
Info.Signals(end).Tag         = verifKeyWord('Auxillary');

Info.Signals(end+1).Name      = 'TxPulseReserved';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '???';
Info.Signals(end).Direction   = 'FirstValue=FirstPing';
Info.Signals(end).FileName    = fullfile('SonarSettings', 'TxPulseReserved.bin');
Info.Signals(end).Tag         = verifKeyWord('Auxillary');

Info.Signals(end+1).Name      = 'MaxPingRate';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '???';
Info.Signals(end).Direction   = 'FirstValue=FirstPing';
Info.Signals(end).FileName    = fullfile('SonarSettings', 'MaxPingRate.bin');
Info.Signals(end).Tag         = verifKeyWord('Auxillary');

Info.Signals(end+1).Name      = 'PingPeriod';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '???';
Info.Signals(end).Direction   = 'FirstValue=FirstPing';
Info.Signals(end).FileName    = fullfile('SonarSettings', 'PingPeriod.bin');
Info.Signals(end).Tag         = verifKeyWord('Auxillary');

Info.Signals(end+1).Name      = 'RangeSelection';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '???';
Info.Signals(end).Direction   = 'FirstValue=FirstPing';
Info.Signals(end).FileName    = fullfile('SonarSettings', 'RangeSelection.bin');
Info.Signals(end).Tag         = verifKeyWord('Auxillary');

Info.Signals(end+1).Name      = 'PowerSelection';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '???';
Info.Signals(end).Direction   = 'FirstValue=FirstPing';
Info.Signals(end).FileName    = fullfile('SonarSettings', 'PowerSelection.bin');
Info.Signals(end).Tag         = verifKeyWord('Auxillary');

Info.Signals(end+1).Name      = 'ControlFlags';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '???';
Info.Signals(end).Direction   = 'FirstValue=FirstPing';
Info.Signals(end).FileName    = fullfile('SonarSettings', 'ControlFlags.bin');
Info.Signals(end).Tag         = verifKeyWord('Auxillary');

Info.Signals(end+1).Name      = 'ProjectorMagicNumber';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '???';
Info.Signals(end).Direction   = 'FirstValue=FirstPing';
Info.Signals(end).FileName    = fullfile('SonarSettings', 'ProjectorMagicNumber.bin');
Info.Signals(end).Tag         = verifKeyWord('Auxillary');

Info.Signals(end+1).Name      = 'ProjectorBeamSteeringAngleVertical';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '???';
Info.Signals(end).Direction   = 'FirstValue=FirstPing';
Info.Signals(end).FileName    = fullfile('SonarSettings', 'ProjectorBeamSteeringAngleVertical.bin');
Info.Signals(end).Tag         = verifKeyWord('Auxillary');

Info.Signals(end+1).Name      = 'ProjectorBeamSteeringAngleHorizontal';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '???';
Info.Signals(end).Direction   = 'FirstValue=FirstPing';
Info.Signals(end).FileName    = fullfile('SonarSettings', 'ProjectorBeamSteeringAngleHorizontal.bin');
Info.Signals(end).Tag         = verifKeyWord('Auxillary');

Info.Signals(end+1).Name      = 'ProjectionBeam_3dB_WidthVertical';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '???';
Info.Signals(end).Direction   = 'FirstValue=FirstPing';
Info.Signals(end).FileName    = fullfile('SonarSettings', 'ProjectionBeam_3dB_WidthVertical.bin');
Info.Signals(end).Tag         = verifKeyWord('Auxillary');

Info.Signals(end+1).Name      = 'ProjectionBeam_3dB_WidthVertical';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '???';
Info.Signals(end).Direction   = 'FirstValue=FirstPing';
Info.Signals(end).FileName    = fullfile('SonarSettings', 'ProjectionBeam_3dB_WidthVertical.bin');
Info.Signals(end).Tag         = verifKeyWord('Auxillary');

Info.Signals(end+1).Name      = 'ProjectionBeam_3dB_WidthHorizontal';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '???';
Info.Signals(end).Direction   = 'FirstValue=FirstPing';
Info.Signals(end).FileName    = fullfile('SonarSettings', 'ProjectionBeam_3dB_WidthHorizontal.bin');
Info.Signals(end).Tag         = verifKeyWord('Auxillary');

Info.Signals(end+1).Name      = 'ProjectionBeamFocalPoint';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '???';
Info.Signals(end).Direction   = 'FirstValue=FirstPing';
Info.Signals(end).FileName    = fullfile('SonarSettings', 'ProjectionBeamFocalPoint.bin');
Info.Signals(end).Tag         = verifKeyWord('Auxillary');

Info.Signals(end+1).Name      = 'ProjectionBeamWeightingWindowParameter';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '???';
Info.Signals(end).Direction   = 'FirstValue=FirstPing';
Info.Signals(end).FileName    = fullfile('SonarSettings', 'ProjectionBeamWeightingWindowParameter.bin');
Info.Signals(end).Tag         = verifKeyWord('Auxillary');

Info.Signals(end+1).Name      = 'ProjectionBeamWeightingWindowType';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '???';
Info.Signals(end).Direction   = 'FirstValue=FirstPing';
Info.Signals(end).FileName    = fullfile('SonarSettings', 'ProjectionBeamWeightingWindowType.bin');
Info.Signals(end).Tag         = verifKeyWord('Auxillary');

Info.Signals(end+1).Name      = 'TransmitFlags';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '???';
Info.Signals(end).Direction   = 'FirstValue=FirstPing';
Info.Signals(end).FileName    = fullfile('SonarSettings', 'TransmitFlags.bin');
Info.Signals(end).Tag         = verifKeyWord('Auxillary');

Info.Signals(end+1).Name      = 'HydrophoneMagicNumber';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '???';
Info.Signals(end).Direction   = 'FirstValue=FirstPing';
Info.Signals(end).FileName    = fullfile('SonarSettings', 'HydrophoneMagicNumber.bin');
Info.Signals(end).Tag         = verifKeyWord('Auxillary');

Info.Signals(end+1).Name      = 'ReceiveBeamWeightingWindow';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '???';
Info.Signals(end).Direction   = 'FirstValue=FirstPing';
Info.Signals(end).FileName    = fullfile('SonarSettings', 'ReceiveBeamWeightingWindow.bin');
Info.Signals(end).Tag         = verifKeyWord('Auxillary');

Info.Signals(end+1).Name      = 'ReceiveBeamWeightingParameter';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '???';
Info.Signals(end).Direction   = 'FirstValue=FirstPing';
Info.Signals(end).FileName    = fullfile('SonarSettings', 'ReceiveBeamWeightingParameter.bin');
Info.Signals(end).Tag         = verifKeyWord('Auxillary');

Info.Signals(end+1).Name      = 'ReceiveFlags';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '???';
Info.Signals(end).Direction   = 'FirstValue=FirstPing';
Info.Signals(end).FileName    = fullfile('SonarSettings', 'ReceiveFlags.bin');
Info.Signals(end).Tag         = verifKeyWord('Auxillary');

Info.Signals(end+1).Name      = 'ReceiveBeamWidth';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '???';
Info.Signals(end).Direction   = 'FirstValue=FirstPing';
Info.Signals(end).FileName    = fullfile('SonarSettings', 'ReceiveBeamWidth.bin');
Info.Signals(end).Tag         = verifKeyWord('Auxillary');

Info.Signals(end+1).Name      = 'BottomDetectionFilterInfoMinRange';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '???';
Info.Signals(end).Direction   = 'FirstValue=FirstPing';
Info.Signals(end).FileName    = fullfile('SonarSettings', 'BottomDetectionFilterInfoMinRange.bin');
Info.Signals(end).Tag         = verifKeyWord('Auxillary');

Info.Signals(end+1).Name      = 'BottomDetectionFilterInfoMaxRange';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '???';
Info.Signals(end).Direction   = 'FirstValue=FirstPing';
Info.Signals(end).FileName    = fullfile('SonarSettings', 'BottomDetectionFilterInfoMaxRange.bin');
Info.Signals(end).Tag         = verifKeyWord('Auxillary');

Info.Signals(end+1).Name      = 'BottomDetectionFilterInfoMinDepth';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '???';
Info.Signals(end).Direction   = 'FirstValue=FirstPing';
Info.Signals(end).FileName    = fullfile('SonarSettings', 'BottomDetectionFilterInfoMinDepth.bin');
Info.Signals(end).Tag         = verifKeyWord('Auxillary');

Info.Signals(end+1).Name      = 'BottomDetectionFilterInfoMaxDepth';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '???';
Info.Signals(end).Direction   = 'FirstValue=FirstPing';
Info.Signals(end).FileName    = fullfile('SonarSettings', 'BottomDetectionFilterInfoMaxDepth.bin');
Info.Signals(end).Tag         = verifKeyWord('Auxillary');

Info.Signals(end+1).Name      = 'BottomDetectRangeFilterFlag';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '???';
Info.Signals(end).Direction   = 'FirstValue=FirstPing';
Info.Signals(end).FileName    = fullfile('SonarSettings', 'BottomDetectRangeFilterFlag.bin');
Info.Signals(end).Tag         = verifKeyWord('Auxillary');

Info.Signals(end+1).Name      = 'BottomDetectDepthFilterFlag';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '???';
Info.Signals(end).Direction   = 'FirstValue=FirstPing';
Info.Signals(end).FileName    = fullfile('SonarSettings', 'BottomDetectDepthFilterFlag.bin');
Info.Signals(end).Tag         = verifKeyWord('Auxillary');

Info.Signals(end+1).Name      = 'Absorption';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '???';
Info.Signals(end).Direction   = 'FirstValue=FirstPing';
Info.Signals(end).FileName    = fullfile('SonarSettings', 'Absorption.bin');
Info.Signals(end).Tag         = verifKeyWord('Auxillary');

Info.Signals(end+1).Name      = 'SoundVelocity';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '???';
Info.Signals(end).Direction   = 'FirstValue=FirstPing';
Info.Signals(end).FileName    = fullfile('SonarSettings', 'SoundVelocity.bin');
Info.Signals(end).Tag         = verifKeyWord('Auxillary');

Info.Signals(end+1).Name      = 'Spreading';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '???';
Info.Signals(end).Direction   = 'FirstValue=FirstPing';
Info.Signals(end).FileName    = fullfile('SonarSettings', 'Spreading.bin');
Info.Signals(end).Tag         = verifKeyWord('Auxillary');

Info.Signals(end+1).Name      = 'TxRollCompensationFlag';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '???';
Info.Signals(end).Direction   = 'FirstValue=FirstPing';
Info.Signals(end).FileName    = fullfile('SonarSettings', 'TxRollCompensationFlag.bin');
Info.Signals(end).Tag         = verifKeyWord('Auxillary');

Info.Signals(end+1).Name      = 'TxRollCompensationValue';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '???';
Info.Signals(end).Direction   = 'FirstValue=FirstPing';
Info.Signals(end).FileName    = fullfile('SonarSettings', 'TxRollCompensationValue.bin');
Info.Signals(end).Tag         = verifKeyWord('Auxillary');

Info.Signals(end+1).Name      = 'TxPitchCompensationFlag';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '???';
Info.Signals(end).Direction   = 'FirstValue=FirstPing';
Info.Signals(end).FileName    = fullfile('SonarSettings', 'TxPitchCompensationFlag.bin');
Info.Signals(end).Tag         = verifKeyWord('Auxillary');

Info.Signals(end+1).Name      = 'TxPitchCompensationValue';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '???';
Info.Signals(end).Direction   = 'FirstValue=FirstPing';
Info.Signals(end).FileName    = fullfile('SonarSettings', 'TxPitchCompensationValue.bin');
Info.Signals(end).Tag         = verifKeyWord('Auxillary');

Info.Signals(end+1).Name      = 'RxRollCompensationFlag';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '???';
Info.Signals(end).Direction   = 'FirstValue=FirstPing';
Info.Signals(end).FileName    = fullfile('SonarSettings', 'RxRollCompensationFlag.bin');
Info.Signals(end).Tag         = verifKeyWord('Auxillary');

Info.Signals(end+1).Name      = 'RxRollCompensationValue';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '???';
Info.Signals(end).Direction   = 'FirstValue=FirstPing';
Info.Signals(end).FileName    = fullfile('SonarSettings', 'RxRollCompensationValue.bin');
Info.Signals(end).Tag         = verifKeyWord('Auxillary');

Info.Signals(end+1).Name      = 'RxPitchCompensationFlag';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '???';
Info.Signals(end).Direction   = 'FirstValue=FirstPing';
Info.Signals(end).FileName    = fullfile('SonarSettings', 'RxPitchCompensationFlag.bin');
Info.Signals(end).Tag         = verifKeyWord('Auxillary');

Info.Signals(end+1).Name      = 'RxPitchCompensationValue';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '???';
Info.Signals(end).Direction   = 'FirstValue=FirstPing';
Info.Signals(end).FileName    = fullfile('SonarSettings', 'RxPitchCompensationValue.bin');
Info.Signals(end).Tag         = verifKeyWord('Auxillary');

Info.Signals(end+1).Name      = 'RxHeaveCompensationFlag';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '???';
Info.Signals(end).Direction   = 'FirstValue=FirstPing';
Info.Signals(end).FileName    = fullfile('SonarSettings', 'RxHeaveCompensationFlag.bin');
Info.Signals(end).Tag         = verifKeyWord('Auxillary');

Info.Signals(end+1).Name      = 'RxHeaveCompensationValue';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '???';
Info.Signals(end).Direction   = 'FirstValue=FirstPing';
Info.Signals(end).FileName    = fullfile('SonarSettings', 'RxHeaveCompensationValue.bin');
Info.Signals(end).Tag         = verifKeyWord('Auxillary');

%% Cr�ation du r�pertoire SonarScope

[nomDir, nom] = fileparts(a.nomFic);
nomDir = fullfile(nomDir, 'SonarScope', nom);
if ~exist(nomDir, 'dir')
    status = mkdir(nomDir);
    if ~status
        messageErreur(nomDir)
        flag = 0;
        return
    end
end

%% Cr�ation du fichier XML d�crivant la donn�e 

nomFicXml = fullfile(nomDir, 'SonarSettings.xml');
xml_write(nomFicXml, Info);
flag = exist(nomFicXml, 'file');
if ~flag
    messageErreur(nomFicXml)
    return
end

%% Cr�ation du r�pertoire SonarSettings

nomDirSonarSettings = fullfile(nomDir, 'SonarSettings');
if ~exist(nomDirSonarSettings, 'dir')
    status = mkdir(nomDirSonarSettings);
    if ~status
        messageErreur(nomDirSonarSettings)
        flag = 0;
        return
    end
end

%% Cr�ation des fichiers binaires des signaux

for k=1:length(Info.Signals)
    flag = writeSignal(nomDir, Info.Signals(k), Data.(Info.Signals(k).Name));
    if ~flag
        return
    end
end
