function status = BDA_compare_Matlab_C(nomFunc, NomVar, valM, valC, iPing, iBeam, seuil)

status = 1;

if isempty(valM) && isempty(valC)
    return
end

if isempty(iBeam)
    iBeam = -1;
end
if isempty(iPing)
    iPing = -1;
end

status = BDA_createMes(nomFunc, ['length(' NomVar ')'], length(valM), length(valC), iPing, iBeam, 0);
if ~status
    return
end

valMax = max(abs(valM - valC));
testSeuil = (valMax > seuil);
testIsNaN = ~isequal(isnan(valC), isnan(valM));
if testSeuil || testIsNaN
%     str = sprintf('%s : "%s" Ping %d Beam %d testIsNaN %d testSeuil %d seuil %f', nomFunc, NomVar, iPing, iBeam, testIsNaN, testSeuil, seuil);
    str = sprintf('Fct;%s;Var;%s;Ping;%d;Beam;%d;testIsNaN;%d;testSeuil;%d;seuil;%s;valMax;%s', ...
        nomFunc, NomVar, iPing, iBeam, testIsNaN, testSeuil, num2str(seuil), num2str(valMax));
    figure('Name', [' BDA : divergence C / Matlab Ping ' num2str(iPing)]);
    h(1) = subplot(4,1,1); PlotUtils.createSScPlot(valM, '-+b'); hold on; PlotUtils.createSScPlot(valC, '-xr'); grid on; legend({'M';'C'});
    title(strrep(str, ';', ' '), 'Interpreter', 'None')
    h(2) = subplot(4,1,2); PlotUtils.createSScPlot(valM - valC, '-*'); grid on;
    title('M-C')
    h(3) = subplot(4,1,3); PlotUtils.createSScPlot(isnan(valM), '-b+'); hold on; PlotUtils.createSScPlot(isnan(valC), '-rx'); grid on; legend({'M';'C'});
    title('isNaN')
    h(4) = subplot(4,1,4); PlotUtils.createSScPlot(isnan(valM)-isnan(valC), '-b+'); grid on;
    title('isnan(M) - isnan(C)')
    linkaxes(h, 'x'); axis tight; drawnow;
    
    BDA_dispMessage(str)
    status = 0;
end
