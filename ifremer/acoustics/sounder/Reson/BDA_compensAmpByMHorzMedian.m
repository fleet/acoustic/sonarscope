function AmpImage = BDA_compensAmpByMHorzMedian(AmpImage)

for iSample=1:size(AmpImage,1)
    Med = median(AmpImage(iSample,:), 'omitnan');
    if Med == 0
        AmpImage(iSample,:) = 0;
    else
        AmpImage(iSample,:) = AmpImage(iSample,:) / Med;
    end
end
