function status = BDA_createMes(nomFunc, nomVar, valM, valC, iPing, iBeam, seuil)

status = 1;

if isempty(iBeam)
    iBeam = -1;
end

if isempty(iPing)
    iPing = -1;
end
diff = abs(valM-valC);
if diff > seuil
    str = sprintf('Fct;%s;Var;%s;Ping;%d;Beam;%d;valM;%s;valC;%s;seuil;%s;diff;%s', ...
        nomFunc, nomVar, iPing, iBeam, num2str(valM), num2str(valC), num2str(seuil), num2str(diff));
    BDA_dispMessage(str)
    status = 0;
end

