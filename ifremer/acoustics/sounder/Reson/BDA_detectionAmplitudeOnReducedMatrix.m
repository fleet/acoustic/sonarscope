function [R0, RMax1, RMax2, R1SamplesFiltre, iBeamMax0, AmpPingNormHorzMax, MaskWidth, iBeamBeg, iBeamEnd] = ...
    BDA_detectionAmplitudeOnReducedMatrix(...
        MatAmplitude, BeamAngles, Frequency, ...
        DepthMinPing, DepthMaxPing, ...
        PulseDuration, SampleRate, ReceiveBeamWidth, PingSequence, SystemSerialNumber, ProjectionBeam_3dB_WidthVertical, ...
        DisplayLevel, iPing, IdentAlgo, varargin)

global TestPlatformCmatlab %#ok<GVMIS>

%% Data reduction in the Time domain

if any(TestPlatformCmatlab.SampleBeam_ReduceMatrixAmplitude == [1 3 4])
    [AmplitudeReduced_M, step_M] = ...
        SampleBeam_ReduceMatrixAmplitude(MatAmplitude);
end

if any(TestPlatformCmatlab.SampleBeam_ReduceMatrixAmplitude == [2 3 4])
    [AmplitudeReduced_C, step_C] = SampleBeam_ReduceMatrixAmplitude_reson(single(MatAmplitude));
end

if any(TestPlatformCmatlab.SampleBeam_ReduceMatrixAmplitude == [3 4])
   if ~isequal(AmplitudeReduced_C, AmplitudeReduced_M)
        figure('Name', [' BDA : divergence C / Matlab Ping ' num2str(iPing)]);
        imagesc(AmplitudeReduced_C - AmplitudeReduced_M); colorbar;
        str = sprintf('SampleBeam_ReduceMatrixAmplitude RESON\ncalled in sonar_detectionAmplitudeOnReducedMatrix');
        title(str, 'Interpreter', 'None')
    end
    if step_M ~= step_C
        str = sprintf('Erreur step SampleBeam_ReduceMatrixAmplitude_reson: %f %f', step_M, step_C);
        my_warndlg(str, 0, 'Tag', 'TestPlatformCmatlab.SampleBeam_ReduceMatrixAmplitude');
    end
end

switch TestPlatformCmatlab.SampleBeam_ReduceMatrixAmplitude
    case {1; 3}
        AmplitudeReduced = AmplitudeReduced_M;
        step             = step_M;
    case {2; 4}
        AmplitudeReduced = AmplitudeReduced_C;
        step             = step_C;
end

%% Preprocessing of Mask on Amplitude

if SystemSerialNumber == 7150
    % Correction de la valeur de ReceiveBeamWidth qui est fausse
    
    if Frequency(1) < 18000 % 12 kHz
        ReceiveBeamWidth = 1; % (deg)
    else % 24 kHz
        ReceiveBeamWidth = 0.5; % (deg)
    end

    if any(TestPlatformCmatlab.SampleBeam_PreprocAmpMask == [1 3 4])
        [R0_M, RMax1_M, RMax2_M, AmpPingNormHorzMax_M, iBeamMax0_M, R1SamplesFiltre_M, MaskWidth_M, iBeamBeg_M, iBeamEnd_M] = ...
            SampleBeam_PreprocAmpMask_7150_V4(...
            AmplitudeReduced, step, BeamAngles, ReceiveBeamWidth, ...
            SampleRate, PulseDuration, DepthMinPing, DepthMaxPing, IdentAlgo, ...
            DisplayLevel, iPing, PingSequence, SystemSerialNumber);
        subR1MaxNaN = isnan(RMax1_M);
        MaskWidth_M(subR1MaxNaN) = NaN;
    end
    
    if any(TestPlatformCmatlab.SampleBeam_PreprocAmpMask == [2 3 4])
        Rmin = (abs(DepthMinPing) ./ cosd(BeamAngles)) * 2 * (SampleRate / 1500);
        Rmax = (abs(DepthMaxPing) ./ cosd(BeamAngles)) * 2 * (SampleRate / 1500);  
        [R0_C, RMax1_C, RMax2_C, AmpPingNormHorzMax_C, iBeamMax0_C, R1SamplesFiltre_C, MaskWidth_C, iBeamBeg_C, iBeamEnd_C] = ...
            SampleBeam_PreprocAmpMask_7150_V4_reson(...
            single(MatAmplitude), ...
            double(step), double(Rmin), double(Rmax), double(PulseDuration), double(ProjectionBeam_3dB_WidthVertical), ...
            double(SampleRate), double(BeamAngles), double(IdentAlgo));
    end
        
 else
     if any(TestPlatformCmatlab.SampleBeam_PreprocAmpMask == [1 3 4])
        [R0_M, RMax1_M, RMax2_M, R1SamplesFiltre_M, iBeamMax0_M, AmpPingNormHorzMax_M, MaskWidth_M, iBeamBeg_M, iBeamEnd_M] = ...
             SampleBeam_PreprocAmpMask_7111_V6(...
             AmplitudeReduced, step, BeamAngles, ReceiveBeamWidth, ...
             SampleRate, PulseDuration, DepthMinPing, DepthMaxPing, IdentAlgo, DisplayLevel, iPing);
     end
     
    if any(TestPlatformCmatlab.SampleBeam_PreprocAmpMask == [2 3 4])
        Rmin = (abs(DepthMinPing) ./ cosd(BeamAngles)) * 2 * (SampleRate / 1500);
        Rmax = (abs(DepthMaxPing) ./ cosd(BeamAngles)) * 2 * (SampleRate / 1500);  
        [R0_C, RMax1_C, RMax2_C, R1SamplesFiltre_C, iBeamMax0_C, AmpPingNormHorzMax_C, MaskWidth_C, iBeamBeg_C, iBeamEnd_C] = ...
            SampleBeam_PreprocAmpMask_7111_V6_reson(...
            single(MatAmplitude), ...
            double(step), double(Rmin), double(Rmax), double(PulseDuration), double(ProjectionBeam_3dB_WidthVertical), ...
            double(SampleRate), double(BeamAngles), double(IdentAlgo));
    end
 
end


if any(TestPlatformCmatlab.SampleBeam_PreprocAmpMask == [3 4] | ...
        TestPlatformCmatlab.SampleBeam_PreprocAmpMask == [3 4])
    if abs(R0_C - R0_M) > 0
        BDA_createMes('SampleBeam_PreprocAmpMask', 'R0', R0_M, R0_C, iPing, [], 0);
    end

    BDA_compare_Matlab_C('SampleBeam_PreprocAmpMask', 'RMax1',              RMax1_M,              RMax1_C,              iPing, [], TestPlatformCmatlab.Seuil_SampleBeam_PreprocAmpMask_RMax1);
    BDA_compare_Matlab_C('SampleBeam_PreprocAmpMask', 'RMax2',              RMax2_M,              RMax2_C,              iPing, [], TestPlatformCmatlab.Seuil_SampleBeam_PreprocAmpMask_RMax2);
    BDA_compare_Matlab_C('SampleBeam_PreprocAmpMask', 'AmpPingNormHorzMax', AmpPingNormHorzMax_M, AmpPingNormHorzMax_C, iPing, [], TestPlatformCmatlab.Seuil_SampleBeam_PreprocAmpMask_AmpPingNormHorzMax);

    if iBeamMax0_C ~= iBeamMax0_M
        BDA_createMes('SampleBeam_PreprocAmpMask', 'iBeamMax', iBeamMax0_M, iBeamMax0_C, iPing, [], 0);
    end

    BDA_compare_Matlab_C('SampleBeam_PreprocAmpMask', 'R1SamplesFiltre', R1SamplesFiltre_M, R1SamplesFiltre_C, iPing, [], TestPlatformCmatlab.Seuil_SampleBeam_PreprocAmpMask_R1SamplesFiltre);
    BDA_compare_Matlab_C('SampleBeam_PreprocAmpMask', 'MaskWidth',       MaskWidth_M,       MaskWidth_C,       iPing, [], TestPlatformCmatlab.Seuil_SampleBeam_PreprocAmpMask_MaskWidth);

    if iBeamBeg_C ~= iBeamBeg_M
        BDA_createMes('SampleBeam_PreprocAmpMask', 'iBeamBeg', iBeamBeg_M, iBeamBeg_C, iPing, [], 0);
    end
    if iBeamEnd_C ~= iBeamEnd_M
        BDA_createMes('SampleBeam_PreprocAmpMask', 'iBeamEnd', iBeamEnd_M, iBeamEnd_C, iPing, [], 0);
    end
end
    
 
if any(TestPlatformCmatlab.SampleBeam_PreprocAmpMask == [1 3] | ...
        TestPlatformCmatlab.SampleBeam_PreprocAmpMask == [1 3])
        R0                  = R0_M;
        RMax1               = RMax1_M;
        RMax2               = RMax2_M;
        R1SamplesFiltre     = R1SamplesFiltre_M;
        iBeamMax0           = iBeamMax0_M;
        AmpPingNormHorzMax  = AmpPingNormHorzMax_M;
        MaskWidth           = MaskWidth_M;
        iBeamBeg            = iBeamBeg_M;
        iBeamEnd            = iBeamEnd_M;
else
        R0                  = R0_C;
        RMax1               = RMax1_C;
        RMax2               = RMax2_C;
        R1SamplesFiltre     = R1SamplesFiltre_C;
        iBeamMax0           = iBeamMax0_C;
        AmpPingNormHorzMax  = AmpPingNormHorzMax_C;
        MaskWidth           = MaskWidth_C;
        iBeamBeg            = iBeamBeg_C;
        iBeamEnd            = iBeamEnd_C;
end


%{
    figure(56567); imagesc(reflec_Enr2dB(MatAmplitude)); colorbar;
    hold on;
    plot(repmat(R0 ,1,length(RMax1)), '*c');
    plot(RMax2, 'ok')
    plot(RMax1, '*k')
    hold off
%}
