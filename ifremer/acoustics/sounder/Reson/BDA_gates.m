function [DepthMinPing, DepthMaxPing] = BDA_gates(GateDepth, BottomDetectDepthFilterFlag, BottomDetectionFilterInfoMinDepth, BottomDetectionFilterInfoMaxDepth, Draught)

switch GateDepth.Flag
    case 1 % Aucune
        DepthMaxPing = 13000;
        DepthMinPing = 1;
    case 2 % Celle d�finie dans les datagrammes si elle existe (sinon aucune)
        if BottomDetectDepthFilterFlag
            DepthMinPing = BottomDetectionFilterInfoMinDepth;
            DepthMaxPing = BottomDetectionFilterInfoMaxDepth;
        else
            DepthMaxPing = 13000;
            DepthMinPing = 1;
            
            %{
                if SystemSerialNumber == 7111
                    DepthMaxPing = 960; % 7700 * 1500 / (2*fe) impose pour r�soudre pb fichier 20080220_134819 ping 60
                    DepthMinPing = 2;
                else
                    if Frequency(1) < 18000 % 12 kHz
                        DepthMaxPing = 20000; % A REGLER !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                        DepthMinPing = 100; % A REGLER !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                    else % 24 kHz
                        DepthMaxPing = 10000; % A REGLER !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                        DepthMinPing = 50; % A REGLER !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                    end
                end
            %}
        end
    case 3 % Personelle
        DepthMaxPing = GateDepth.DepthMax;
        DepthMinPing = GateDepth.DepthMin;
end
DepthMaxPing = abs(DepthMaxPing) - Draught;
DepthMinPing = abs(DepthMinPing) - Draught;
