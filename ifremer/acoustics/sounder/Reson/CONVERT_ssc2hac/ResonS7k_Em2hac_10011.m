function flag = ResonS7k_Em2hac_10011(file, nomDir, iTuple, NbFaisceaux, Data, seuildB, seuilDeg, varargin)

flag = 0; %#ok<NASGU>

[varargin, idxWCPing]  = getPropertyValue(varargin, 'WCPingIndex',       []);
[varargin, iMultiPing] = getPropertyValue(varargin, 'MultipingSequence', 0);
[varargin, dInterfero] = getPropertyValue(varargin, 'DistanceInterfero', 4); %#ok<ASGLU>

% typeData            = 1; % Type de donn�e MagAndPhase
% Tuple 10040 Ping C-16 compressed 16 bytes format pour chaque
% faisceau
TimeFraction        = Data.MagAndPhase.time_fraction(iTuple);    %0.0000;
TimeCPU             = Data.MagAndPhase.time_cpu(iTuple);         %InfoPing.startdatatime + InfoPing.datatime;
TransceiverMode     = 0;

[~, rootFic, ~]     = fileparts(nomDir);

% Suppression de l'identificateur du sondeur qui pr�fixe le r�pertoire et pas les fichiers de WC.
% L'index de Ping WC peut avoir saut� un n� en cas de Multiping par exemple.
idx                = regexp(rootFic, '_');
nomFicMagAndP      = fullfile(nomDir, [rootFic(idx(1)+1:end) '_BeamRawDataAP_' num2str(idxWCPing) '.mat']);
% Ouverture du fichier des �chantillons pour lecture par faisceau par faisceau.
if exist(nomFicMagAndP, 'file')
    S               = load(nomFicMagAndP);
    matAmplitude    = S.BeamData.RD.Amplitude;
    matPhase        = S.BeamData.RD.Phase;
    % Traitement �quivalent � celui effectu� dans view_WaterColumn du S7K pour �viter les valeurs n�gatives
    % avant le passage en dB (verrue mise en place par JMA pour contourner un bug RESON).
    MinAmp =  min(matAmplitude(:));
    if MinAmp < 0
        MinAmp = min(MinAmp, -13);
        matAmplitude = matAmplitude - MinAmp + 1;
    end
    
else
    messageErreurFichier(nomFicMagAndP);
    flag = 0;
    return
end

NSamplesEcrits  = 0;

for iBeam = 1:NbFaisceaux   %InfoPing.faisceaux
    
    % Calcul du nombre d'�chantillons � stocker et d�j� stock�s.
    nbSamplesALire          = double(Data.MagAndPhase.NbSamples(idxWCPing));
    
    softChannelId           = Data.MagAndPhase.ChannelIdent(iMultiPing, iBeam);        % num�ro du faisceau  - à it�rer de 1 � 128
    rangeBottom             = Data.MagAndPhase.rangebottom(idxWCPing,iBeam);
    
    %% Traitement du paquets d'�chantillons (Ping, Faisceau), traitement de
    % la compression.
    
    thetaS                  = Data.MagAndPhase.angles_athwart(idxWCPing, iBeam);
    lambdaPing              = Data.MagAndPhase.soundSpeed(idxWCPing)/Data.InstallParams.Frequency;
    phiTrad                 = deg2rad(matPhase(1:nbSamplesALire, iBeam)');
    thetaT                  = rad2deg(asin(sind(thetaS) + (lambdaPing/(2*pi*dInterfero)*phiTrad)));
    % On ram�ne l'angle theta_t calcul� en r�f�rence au faisceau correspondant :
    dataSamples             = thetaT - thetaS;
    
    % 10/11/2016, GLU-QO : Pas de seuillage des valeurs car on ne sait pas compresser
    % les tuples 10011 pas plus que MOVIES ne sait pas les relire.
    N                       = numel(dataSamples);
    compressThetaT          = dataSamples*10;
    % Transcodage des valeurs pour interpr�ter les valeurs n�gatives :
    % Par ex, -2 est cod�e par 65534. Les valeurs positives ne sont pas
    % modifi�es.
    compressThetaT = typecast(int16(compressThetaT), 'uint16');
    
    % Pas de compression des angles Along (forc�s � 0).
    compressThetaL          = 0; %#ok<NASGU>
    
    %{
Deb : TODO. A finaliser pour la compression.
% On ram�ne l'angle theta_t calcul� en r�f�rence au faisceau correspondant:
[flag, N, compressThetaT] = ResonS7k_compression10011(dataSamples, nbSamplesALire, seuilDeg(idxWCPing,iBeam));
%     if ~flag
%         return
%     end

% Les champs � �crire sont une composition des valeurs d'angles Along (ThetaLongitudinal, tous forc� � 0)
% et AthWart (ThetaTransverse).
% Pour tous les angles sup�rieurs au seuil, on met � 1 le bit 32 de
% poids fort.
% Masquage avec 2^31 (10000...0) pour signifier la compression des
% valeurs. On �crit les valeurs en uint32 non-sign�s pour exploiter le
% bit de poids fort qu'on vient de formatter.
pppp                    = 2^31 +  uint32(compressPhases(sub));
compressPhases(sub==1)  = pppp;
% Fin TODO
    %}
    
    compressPhases          = compressThetaT;
    flag                    = fcnWrite_10011_phase( file, TimeFraction, TimeCPU, compressPhases, N, ...
        softChannelId, TransceiverMode, idxWCPing, ...
        rangeBottom);
    if ~flag
        return
    end
    
    
    %% Traitement du paquets d'�chantillons d'amplitude(Ping, Faisceau), traitement de
    % la compression.
    [flag, N, compressMag] = ResonS7k_compression10040(matAmplitude(1:nbSamplesALire, iBeam)', nbSamplesALire, seuildB);
    if ~flag
        return
    end
    
    sub                     = (compressMag > 32767);
    pppp                    = compressMag(sub);
    pppp                    = -32768+(pppp-32768)-1;
    compressMag(sub==1) = pppp;
    
    flag                    = fcnWrite_10040_mag( file, TimeFraction, TimeCPU, compressMag, N, ...
        softChannelId, TransceiverMode, idxWCPing, ...
        rangeBottom);
    if ~flag
        return
    end
    
    NSamplesEcrits          = NSamplesEcrits + N;
    
    
end

flag = 1;

% === Fonctions nested ==========================================
% ---------------------------------------------------------------
%
% Function : fcnWrite_10011_phase
%
% ---------------------------------------------------------------
function flag = fcnWrite_10011_phase(   file, TimeFraction, TimeCPU, compressPhases, N, SoftChannelId, ...
    TransceiverMode, PingNumber, rangeBottom)

flag = 0; %#ok<NASGU>

% Les angles sont stock�s sur 4 octets : athwart (bits de 0 � 15) et along ( de 16 � 30).
nbBytesSample   = 4;
TupleType       = 10011;


% Taille du tuple (=taille 10040. Le champ de Padding de 10040 ne
% compte pas dans tous les cas).
TupleSize       = 26 + nbBytesSample*N;

% iMultiPing d�signe l'Id du sondeur et permet de r�cup�rer de fa�on
% unique les n� de channels associ�s.
fwrite(file.fid, TupleSize,'uint32');
fwrite(file.fid, TupleType,'uint16');
fwrite(file.fid, TimeFraction,'uint16');
fwrite(file.fid, TimeCPU,'uint32');
fwrite(file.fid, SoftChannelId,'uint16');
fwrite(file.fid, TransceiverMode,'uint16');
fwrite(file.fid, PingNumber,'uint32');
if rangeBottom==0
    fwrite(file.fid, 2147483647,'int32');
else
    % La d�tection de fond se fait directement depuis les lectures de datagrammes RESON.;
    pppp = int32(rangeBottom)*1000;
    fwrite(file.fid, pppp,'int32');
end

% Ecriture du nombre d'�chantillons � venir.
fwrite(file.fid, N,'uint32');

% Ecriture dans le fichier HAC.
% le codage est en unit32 pour permettre toute la dynamique car la valeur
% est pr�-formatt�e avant l'appel.
fwrite(file.fid, int32(compressPhases),'*int32');

% Ecriture de la fin du tuple
TupleAttribute  = 0;
Backlink        = TupleSize+10;
fwrite(file.fid, TupleAttribute,'int32');
fwrite(file.fid, Backlink,'uint32');

flag = 1;

% ---------------------------------------------------------------
%
% Function : fcnWrite_10040_mag
% Equivalent � l'�criture des tuples 10040.
%
% ---------------------------------------------------------------
function flag = fcnWrite_10040_mag(   file, TimeFraction, TimeCPU, compressMag, N, SoftChannelId, ...
    TransceiverMode, PingNumber, rangeBottom)

flag = 0; %#ok<NASGU>

% Les angles sont stock�s sur 4 octets : athwart (bits de 0 � 15) et along ( de 16 � 30).
nbBytesSample   = 2;
TupleType       = 10040;

% Taille du tuple (=taille 10040. Le champ de Padding de 10040 ne
% compte pas dans tous les cas).
TupleSize       = 26 + nbBytesSample*N;

% iMultiPing d�signe l'Id du sondeur et permet de r�cup�rer de fa�on
% unique les n� de channels associ�s.
fwrite(file.fid, TupleSize,             'uint32');
fwrite(file.fid, TupleType,             'uint16');
fwrite(file.fid, TimeFraction,          'uint16');
fwrite(file.fid, TimeCPU,               'uint32');
fwrite(file.fid, SoftChannelId,         'uint16');
fwrite(file.fid, TransceiverMode,       'uint16');
fwrite(file.fid, PingNumber,            'uint32');

if rangeBottom==0
    fwrite(file.fid, 2147483647,'int32');
else
    % La d�tection de fond se fait directement depuis les lectures de datagrammes RESON.;
    pppp = int32(rangeBottom)*1000;
    fwrite(file.fid, pppp,'int32');
end

% Ecriture du nombre d'�chantillons � venir.
fwrite(file.fid, N,                 'uint32');

% Ecriture dans le fichier HAC.
fwrite(file.fid, int16(compressMag),'*int16');

% Ecriture de la fin du tuple
TupleAttribute  = 0;
Backlink        = TupleSize+10;
fwrite(file.fid, TupleAttribute,    'int32');
fwrite(file.fid, Backlink,          'uint32');

flag = 1;
