function flag = ResonS7k_Em2hac_10040(file, nomDir, iTuple, NbFaisceaux, Data, SeuildB, varargin)

flag                = 0; %#ok<NASGU>

[varargin, idxWCPing] = getPropertyValue(varargin, 'WCPingIndex', []);
[varargin, iMultiPing] = getPropertyValue(varargin, 'MultipingSequence', 0); %#ok<ASGLU>

% Tuple 10040 Ping C-16 compressed 16 bytes format pour chaque
% faisceau
TupleType           = 10040;
TimeFraction        = Data.DataSample.time_fraction(iTuple);    %0.0000;
TimeCPU             = Data.DataSample.time_cpu(iTuple);         %InfoPing.startdatatime + InfoPing.datatime;
TransceiverMode     = 0;
typeData            = 0; % Type de donn�e MagAndPhase

% Ecriture du n� de Ping Depth.
PingNumber          = idxWCPing; % Data.DataSample.Ping(iTuple);

[~, rootFic]  = fileparts(nomDir);

% Suppression de l'identificateur du sondeur qui pr�fixe le r�pertoire et pas les fichiers de WC.
% L'index de Ping WC peut avoir saut� un n� en cas de Multiping par exemple.
idx                = regexp(rootFic, '_');
nomFicWC           = fullfile(nomDir, [rootFic(idx(1)+1:end) '_WaterColumnDataAP_' num2str(idxWCPing) '.mat']);
% Ouverture du fichier des �chantillons pour lecture par faisceau par faisceau.
if exist(nomFicWC, 'file')
    S               = load(nomFicWC);
    matAmplitude    = S.BeamData.RD.Amplitude;
    % Traitement �quivalent � celui effectu� dans view_WaterColumn du S7K pour �viter les valeurs n�gatives
    % avant le passage en dB (verrue mise en place par JMA pour contourner un bug RESON).
    MinAmp =  min(matAmplitude(:));
    if MinAmp < 0
        MinAmp = min(MinAmp, -13);
        matAmplitude = matAmplitude - MinAmp + 1;
    end
    
else
    messageErreurFichier(nomFicWC);
    flag = 0;
    return
end

NSamplesEcrits = 0;
for iBeam = 1:NbFaisceaux   %InfoPing.faisceaux
    
    % Calcul du nombre d'�chantillons � stocker et d�j� stock�s.
    NbSamplesALire  = double(Data.DataSample.NbSamples(iTuple,iBeam));
    SoftChannelId   = Data.DataSample.ChannelIdent(iMultiPing, iBeam);        % num�ro du faisceau  - à it�rer de 1 � 128
    
    % Traitement du paquets d'�chantillons (Ping, Faisceau), traitement de
    % la compression.
    [flag, N, CompressSamples] = ResonS7k_compression10040(matAmplitude(1:NbSamplesALire, iBeam)', NbSamplesALire, SeuildB);
    if ~flag
        return
    end
    %{
% Calcul de compl�ment � deux pour les valeurs d�passant 32767
for s=1:numel(CompressSamples)
if CompressSamples(s) > 32767
CompressSamples(s) = twos_comp(double(CompressSamples(s)), 16);
CompressSamples(s) = CompressSamples(s) - 1;
end

end
    %}
    sub                     = (CompressSamples > 32767);
    pppp                    = CompressSamples(sub);
    pppp                    = -32768+(pppp-32768)-1;
    CompressSamples(sub==1) = pppp;
    
    NSamplesEcrits  = NSamplesEcrits + N;
    TupleSize       = 26 + 2*N;
    
    % iMultiPing d�signe l'Id du sondeur et permet de r�cup�rer de fa�on
    % unique les n� de channels associ�s.
    fwrite(file.fid, TupleSize,                 'uint32');
    fwrite(file.fid, TupleType,                 'uint16');
    fwrite(file.fid, TimeFraction,              'uint16');
    fwrite(file.fid, TimeCPU,                   'uint32');
    fwrite(file.fid, SoftChannelId,             'uint16');
    fwrite(file.fid, TransceiverMode,           'uint16');
    fwrite(file.fid, PingNumber,                'uint32');
    
    if Data.DataSample.rangebottom(iTuple,iBeam)==0
        fwrite(file.fid, 2147483647,    'int32');
        %  fwrite(file.fid, ((24/cos(WC.angles_athwart(iTuple,i)*pi/180))*1000),'int32');
    else
        % La d�tection de fond se fait directement depuis les lectures de datagrammes RESON.;
        pppp = int32(Data.DataSample.rangebottom(iTuple,iBeam))*1000;
        fwrite(file.fid, pppp,          'int32');
        %  fwrite(file.fid, ((24/cos(WC.angles_athwart(iTuple,i)*pi/180))*1000),'int32');
        
    end
    
    % Ecriture du nombre d'�chantillons � venir.
    fwrite(file.fid, N,     'uint32');
    
    % Ecriture dans le fichier HAC.
    fwrite(file.fid, int16(CompressSamples),    '*int16');
    
    % Ecriture de la fin du tuple
    TupleAttribute  = 0;
    Backlink        = TupleSize+10;
    fwrite(file.fid, TupleAttribute,    'int32');
    fwrite(file.fid, Backlink,          'uint32');
    
end

flag = 1;
