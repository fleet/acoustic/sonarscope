function flag = ResonS7k_Em2hac_10100(file, iTuple, NbFaisceaux, Data, varargin)

flag = 0; %#ok<NASGU>


[varargin, iMultiPing] = getPropertyValue(varargin, 'MultipingSequence', 0);
[varargin, subStruct] = getPropertyValue(varargin, 'SubStruct', 'DataSample'); %#ok<ASGLU>

persistent sTuplePrev;
persistent tabMultiPing;

if strcmpi(subStruct, 'DataSample')
    typeData = 0; % type de donn�e Water Column;
else
    typeData = 1; % type de donn�e MagAndPhase;
end

% Doit �tre r�initialis� entre deux sessions de lancement.
if (iTuple == 1 && typeData == 0)
    sTuplePrev      = [];
    tabMultiPing    = [];
end

% Allocation de m�moire.
sTupleCrt(NbFaisceaux) = struct(    'SoftwareChannelIdentifier', [], ...
    'TVGMaxRange', [], ...
    'TVGMinRange', [], ...
    'TVTEvaluationMode', [], ...
    'TVTEvaluationInterval', [], ...
    'TVTEvaluationNoPings', [], ...
    'TVTEvaluationStartingTVTPingNum', [], ...
    'TVToffsetparameter', [], ...
    'TVTAmplificationParameter', [], ...
    'TupleAttribute', [], ...
    'Backlink', [] );

% Conservation du iMultiPing pour identifier si il a d�j� �t� rencontr�.
idxMultiPing = find(tabMultiPing == iMultiPing, 1, 'first');

% Tuple 10100 General threshold
for k=1:NbFaisceaux
    TupleSize                                   = 34;
    TupleType                                   = 10100;
    TimeFraction                                = Data.(subStruct).time_fraction(iTuple); %0.0000;
    TimeCPU                                     = Data.(subStruct).time_cpu(iTuple);      %InfoPing.startdatatime+InfoPing.datatime; %heure locale en seconde depuis le 1er janvier 1970
    % iMultiPing d�signe l'Id du sondeur et permet de r�cup�rer de fa�on
    % unique les n� de channels associ�s.
    sTupleCrt(k).SoftwareChannelIdentifier       = Data.(subStruct).ChannelIdent(iMultiPing, k);        % num�ro du faisceau  - à it�rer de 1 � 128
    sTupleCrt(k).TVGMaxRange                     = 6553.5*10;                % le max
    sTupleCrt(k).TVGMinRange                     = 6553.5*10;
    sTupleCrt(k).TVTEvaluationMode               = 0;
    sTupleCrt(k).TVTEvaluationInterval           = 65535;
    sTupleCrt(k).TVTEvaluationNoPings            = 65535;
    sTupleCrt(k).TVTEvaluationStartingTVTPingNum = 4294967295;
    sTupleCrt(k).TVToffsetparameter              = -2147.483648*1000000; %Seuil d�archivage = not available
    sTupleCrt(k).TVTAmplificationParameter       = 0;
    sTupleCrt(k).TupleAttribute                  = 0;
    sTupleCrt(k).Backlink                        = 44;
    
    
    % Pr�paration pour comparaison des donn�es dans une structure neutre
    if isempty(idxMultiPing)
        sTupleSaved = [];
    else
        sTupleSaved = sTuplePrev(idxMultiPing,k);
    end
    
    
    % Ecriture des tuples que si les donn�es diff�rents du datagramme
    % pr�c�dent.
    if isempty(sTupleSaved) || ~isequal(sTupleSaved, sTupleCrt(k))
        fwrite(file.fid, TupleSize,'uint32');
        fwrite(file.fid, TupleType,'uint16');
        fwrite(file.fid, TimeFraction,'uint16');
        fwrite(file.fid, TimeCPU,'uint32');
        fwrite(file.fid, sTupleCrt(k).SoftwareChannelIdentifier,'uint16');
        fwrite(file.fid, sTupleCrt(k).TVGMaxRange,'uint16');
        fwrite(file.fid, sTupleCrt(k).TVGMinRange,'uint16');
        fwrite(file.fid, sTupleCrt(k).TVTEvaluationMode,'uint16');
        fwrite(file.fid, sTupleCrt(k).TVTEvaluationInterval,'uint16');
        fwrite(file.fid, sTupleCrt(k).TVTEvaluationNoPings,'uint16');
        fwrite(file.fid, sTupleCrt(k).TVTEvaluationStartingTVTPingNum,'uint32');
        fwrite(file.fid, sTupleCrt(k).TVToffsetparameter,'int32');
        fwrite(file.fid, sTupleCrt(k).TVTAmplificationParameter,'uint32');
        fwrite(file.fid, sTupleCrt(k).TupleAttribute,'int32');
        fwrite(file.fid, sTupleCrt(k).Backlink,'uint32');
    end
end

% Conservation des donn�es dans une structure
if isempty(idxMultiPing)
    tabMultiPing(end+1) = iMultiPing;
end
idxMultiPing = find(tabMultiPing == iMultiPing, 1, 'first');
if isempty(sTuplePrev)
    sTuplePrev = sTupleCrt;
else
    sTuplePrev(idxMultiPing,:) = sTupleCrt;
end

flag = 1;
