function flag = ResonS7k_Em2hac_41(file, iTuple, NbFaisceaux, Data, varargin)

flag = 0; %#ok<NASGU>

persistent sTuplePrev;
persistent tabMultiPing;

[varargin, iMultiPing]       = getPropertyValue(varargin, 'MultipingSequence', 0);
[varargin, flagForceWriting] = getPropertyValue(varargin, 'FlagForceWriting',  0);
[varargin, subStruct]        = getPropertyValue(varargin, 'SubStruct',         'DataSample'); %#ok<ASGLU>

if strcmpi(subStruct, 'DataSample')
    typeData = 0; % type de donn�e Water Column;
else
    typeData = 1; % type de donn�e MagAndPhase;
end

% Doit �tre r�initialis� entre deux sessions de lancement : on commence
% toujours par le WC !
if (iTuple == 1 && typeData == 0)
    sTuplePrev          = [];
    tabMultiPing        = [];
end

% Allocation de m�moire.
sTupleCrt(NbFaisceaux) = struct( 'AttitudeSensorId', [], ...
    'PlatformType',     [], ...
    'AlongOffset',      [], ...
    'AthwartOffset',    [], ...
    'ElevationOffset',  [], ...
    'Remarks',          [], ...
    'Space',            [], ...
    'TupleAttribute',   [], ...
    'Backlink',         [] );

% Conservation du iMultiPing pour identifier si il a d�j� �t� rencontr�.
idxMultiPing = find(tabMultiPing == iMultiPing, 1, 'first');

for k = 1:NbFaisceaux
    % Tuple 41 attitude sondeur pour chaque faisceau
    TupleSize                       = 54;
    TupleType                       = 41;
    TimeFraction                    = Data.(subStruct).time_fraction(iTuple); %0.0000;
    TimeCPU                         = Data.(subStruct).time_cpu(iTuple);      %InfoPing.startdatatime + InfoPing.datatime;
    % iMultiPing d�signe l'Id du sondeur et permet de r�cup�rer de fa�on
    % unique les n� de channels associ�s.
    sTupleCrt(k).TransceiverChannelNumber = Data.(subStruct).ChannelIdent(iMultiPing, k);        % num�ro du faisceau  - à it�rer de 1 � 128
    
    % Enregistrement des informations utiles du Tuple pour comparaison
    % avec le Ping pr�c�dent.
    sTupleCrt(k).AttitudeSensorId    = 2;
    sTupleCrt(k).PlatformType        = 0;
    sTupleCrt(k).AlongOffset         = floor(Data.InstallParams.AlongshipOffsetRelToAttSensor*100); % Motion sensor 1 along location in m
    sTupleCrt(k).AthwartOffset       = floor(Data.InstallParams.AthwartshipOffsetRelToAttSensor*100); % Motion sensor 1 athwart location in m
    sTupleCrt(k).ElevationOffset     = floor(Data.InstallParams.VerticalOffsetRelToAttSensor*100); % Motion sensor 1 vertical location in m
    sTupleCrt(k).Remarks             = '123456789012345678901234567890';
    sTupleCrt(k).Space               = 0;
    sTupleCrt(k).TupleAttribute      = 0;
    sTupleCrt(k).Backlink            = 64;
    
    % Pr�paration pour comparaison des donn�es dans une structure neutre
    if isempty(idxMultiPing)
        sTupleSaved = [];
    else
        sTupleSaved = sTuplePrev(idxMultiPing,k);
    end
    
    % Ecriture des tuples que si les donn�es diff�rent du datagramme
    % pr�c�dent.
    if isempty(sTupleSaved) || ~isequal(sTupleSaved, sTupleCrt(k)) || flagForceWriting
        fwrite(file.fid, TupleSize,'uint32');
        fwrite(file.fid, TupleType,'uint16');
        fwrite(file.fid, TimeFraction,'uint16');
        fwrite(file.fid, TimeCPU,'uint32');
        fwrite(file.fid, sTupleCrt(k).AttitudeSensorId,'uint16');
        fwrite(file.fid, sTupleCrt(k).TransceiverChannelNumber,'uint16');
        fwrite(file.fid, sTupleCrt(k).PlatformType,'uint16');
        fwrite(file.fid, sTupleCrt(k).AlongOffset,'int16');
        fwrite(file.fid, sTupleCrt(k).AthwartOffset,'int16');
        fwrite(file.fid, sTupleCrt(k).ElevationOffset,'int16');
        fwrite(file.fid, sTupleCrt(k).Remarks,'char');
        fwrite(file.fid, sTupleCrt(k).Space,'uint16');
        fwrite(file.fid, sTupleCrt(k).TupleAttribute,'int32');
        fwrite(file.fid, sTupleCrt(k).Backlink,'uint32');
    end
end

% Conservation des donn�es dans une structure
if isempty(idxMultiPing)
    tabMultiPing(end+1) = iMultiPing;
end
idxMultiPing = find(tabMultiPing == iMultiPing, 1, 'first');
if isempty(sTuplePrev)
    sTuplePrev = sTupleCrt;
else
    sTuplePrev(idxMultiPing,:) = sTupleCrt;
end

flag = 1;
