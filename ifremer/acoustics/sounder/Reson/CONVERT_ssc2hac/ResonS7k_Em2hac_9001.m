function [flag, isWrited] = ResonS7k_Em2hac_9001(file, iTuple, NumberSWChannels, SounderName, Data, varargin)

[varargin, iMultiPing] = getPropertyValue(varargin, 'MultipingSequence', 0);
[varargin, subStruct] = getPropertyValue(varargin, 'SubStruct', 'DataSample'); %#ok<ASGLU>

persistent sTuplePrev;
persistent tabMultiPing;

% Type data sert � discriminer WaterColumn/MagAndPhase.
% Si WaterColumn ==> Monoping ou MultiPing. Le iMultiPing �quivaut � d�clarer autant de sondeurs.
% Si MagAndPhase ==> Monoping. L'identifiant du sondeur sert de cl� pour discriminer WaterColum et MagAndPhase
if strcmpi(subStruct, 'DataSample')
    typeData = 0; % type de donn�e Water Column;
else
    typeData = 1; % type de donn�e MagAndPhase;
end

% Doit �tre r�initialis� entre deux sessions de lancement : on commence
% toujours par le WC !
if (iTuple == 1 && typeData == 0)
    sTuplePrev          = [];
    tabMultiPing        = [];
end


flag        = 0; %#ok<NASGU>

% Flag d'indication de l'�criture du tuple
isWrited    = 0;

% D�claration des types de donn�es : cela est utile � la comparaison sur
% les valeurs et pour l'�criture.
fieldsType = {	'uint32', ...
    'uint32', ...
    'uint32', ...
    'uint16', ...
    'uint16', ...
    'uint16', ...
    'uint16', ...
    'uint16', ...
    'uint16', ...
    'uint32', ...
    'uint32', ...
    'uint32', ...
    'uint16', ...
    'uint16', ...
    'uint32', ...
    'uint32', ...
    'uint32', ...
    'int16', ...
    'int16', ...
    'int16', ...
    'int16', ...
    'int16', ...
    'uint16', ...
    'uint32', ...
    'uint16', ...
    'uint16', ...
    'uint16', ...
    'uint16', ...
    'uint16', ...
    'int16', ...
    'uint16', ...
    'int16', ...
    'int16', ...
    'int16', ...
    'uint32', ...
    'uint32', ...
    'char', ...
    'int32', ...
    'uint32'};


% Allocation de m�moire.
sTupleCrt(NumberSWChannels) = struct('SamplingRate',                            [], ...
    'SamplingInterval',                         [], ...
    'AcousticFrequency',                        [], ...
    'TransceiverChannelNumber',                 [], ...
    'TypeOfData',                               [], ...
    'TimeVariedGainMultiplier',                 [], ...
    'TVGBlankingMode',                          [], ...
    'TVGMinimumRange',                          [], ...
    'TVGMaximumRange',                          [], ...
    'BlankingUpToRange',                        [], ...
    'SampleRange',                              [], ...
    'InstallationDepthOfTransducer',            [], ...
    'PlatformIdentifier',                       [], ...
    'Space',                                    [], ...
    'AlongshipOffsetRelToAttSensor',			[], ...
    'AthwartshipOffsetRelToAttSensor',			[], ...
    'VerticalOffsetRelToAttSensor',             [], ...
    'AlongshipAngleOffsetOfTransducer',			[], ...
    'AthwartshipAngleOffsetOfTransducer',		[], ...
    'RotationAngleOffsetOfTransducer',			[], ...
    'AlongshipAngleOffsetToMainAxisOfAcBeam',	[], ...
    'AthwartshipAngleOffsetToMainAxisOfAcBeam',	[], ...
    'AbsorptionOfSound',                        [], ...
    'PulseDuration',                            [], ...
    'PulseShapeMode',                           [], ...
    'BandWidth',                                [], ...
    'TransducerShapeMode',                      [], ...
    'BeamAlongship3dBBeamWidth',                [], ...
    'BeamAthwartship3dBBeamWidth',              [], ...
    'BeamEquivalentTwoWayBeamAngle',			[], ...
    'CalibrationSourceLevel',                   [], ...
    'CalibrationReceivingSensitivity',			[], ...
    'SLPlusVR',                                 [], ...
    'BottomDetectionMinimumLevel',              [], ...
    'BottomDetectionMinimumDepth',              [], ...
    'BottomDetectionMaximumDepth',              [], ...
    'Remarks',                                  [], ...
    'TupleAttribute',                           [], ...
    'Backlink',                                 []);

% Arrondi de la c�l�rit�.
soundSpeed = fix(Data.(subStruct).soundSpeed(iTuple)*10)/10;

% Liste des champs pour factoriser l'�criture des champs avec le type
% associ�.
% f = fieldnames(sTupleCrt);

% Identifiant sondeur
EchosounderDocIdentifier    = iMultiPing+typeData;

% Conservation du iMultiPing pour identifier si il a d�j� �t� rencontr�.
if typeData == 0
    idxMultiPing = find(tabMultiPing == iMultiPing, 1, 'first');
else
    idxMultiPing = find(tabMultiPing == EchosounderDocIdentifier, 1, 'first');
end

for k=1:NumberSWChannels
    TupleSize                                               = 146;
    TupleType                                               = 9001;
    % iMultiPing d�signe l'Id du sondeur et permet de r�cup�rer de fa�on
    % unique les n� de channels associ�s.
    % Num�ro du faisceau  - à it�rer de 1 � 128
    SoftwareChannelIdentifier                               = Data.(subStruct).ChannelIdent(iMultiPing, k);
    % Enregistrement des informations utiles du Tuple pour comparaison
    % avec le Ping pr�c�dent.
    iField = 1;
    sTupleCrt(k).SamplingRate                                = cast(Data.(subStruct).sampling(iTuple), fieldsType{iField});
    iField                                          = iField + 1;
    sTupleCrt(k).SamplingInterval                            = cast((1/Data.(subStruct).sampling(iTuple))*soundSpeed/2*1e+6, fieldsType{iField});
    iField                                          = iField + 1;
    sTupleCrt(k).AcousticFrequency                           = cast(Data.(subStruct).frequency(iTuple,k)*10, fieldsType{iField});
    iField                                          = iField + 1;
    sTupleCrt(k).TransceiverChannelNumber                    = cast(k, fieldsType{iField});
    iField                                          = iField + 1;
    sTupleCrt(k).TypeOfData                                  = cast(11, fieldsType{iField});    % 11 = cast(mean Sv (volume backscattering strength in dB)*
    iField                                          = iField + 1;
    sTupleCrt(k).TimeVariedGainMultiplier                    = cast(20, fieldsType{iField});
    iField                                          = iField + 1;
    sTupleCrt(k).TVGBlankingMode                             = cast(65535, fieldsType{iField});
    iField                                          = iField + 1;
    sTupleCrt(k).TVGMinimumRange                             = cast(65535, fieldsType{iField});
    iField                                          = iField + 1;
    sTupleCrt(k).TVGMaximumRange                             = cast(65535, fieldsType{iField});
    iField                                          = iField + 1;
    sTupleCrt(k).BlankingUpToRange                           = cast(4294967295, fieldsType{iField});
    iField                                          = iField + 1;
    sTupleCrt(k).SampleRange                                 = cast(4294967295, fieldsType{iField});
    iField                                          = iField + 1;
    sTupleCrt(k).InstallationDepthOfTransducer               = cast(abs(Data.InstallParams.VerticalOffsetRelToAttSensor*1e4), fieldsType{iField}); % Motion sensor 1 vertical location in m
    iField                                          = iField + 1;
    sTupleCrt(k).PlatformIdentifier                          = cast(2, fieldsType{iField});
    iField                                          = iField + 1;
    sTupleCrt(k).Space                                       = cast(0, fieldsType{iField});
    iField                                          = iField + 1;
    sTupleCrt(k).AlongshipOffsetRelToAttSensor               = cast(Data.InstallParams.AlongshipOffsetRelToAttSensor*10000, fieldsType{iField}); % Motion sensor 1 along location in m
    iField                                          = iField + 1;
    sTupleCrt(k).AthwartshipOffsetRelToAttSensor             = cast(Data.InstallParams.AthwartshipOffsetRelToAttSensor*10000, fieldsType{iField}); % Motion sensor 1 athwart location in m
    iField                                          = iField + 1;
    sTupleCrt(k).VerticalOffsetRelToAttSensor                = cast(abs(Data.InstallParams.VerticalOffsetRelToAttSensor*10000), fieldsType{iField}); % Motion sensor 1 vertical location in m
    iField                                          = iField + 1;
    sTupleCrt(k).AlongshipAngleOffsetOfTransducer            = cast(Data.InstallParams.AlongshipAngleOffsetOfTransducer*100, fieldsType{iField}); % Transducer 1 pitch in 0.01 degrees
    iField                                          = iField + 1;
    sTupleCrt(k).AthwartshipAngleOffsetOfTransducer          = cast(Data.InstallParams.AthwartshipAngleOffsetOfTransducer*100, fieldsType{iField}); % Transducer 1 roll in 0.01 degrees
    iField                                          = iField + 1;
    sTupleCrt(k).RotationAngleOffsetOfTransducer             = cast(Data.InstallParams.RotationAngleOffsetOfTransducer*100, fieldsType{iField}); % Transducer 1 heading in 0.01 degrees
    iField                                          = iField + 1;
    sTupleCrt(k).AlongshipAngleOffsetToMainAxisOfAcBeam      = cast(Data.(subStruct).angles_along(iTuple, k)*100, fieldsType{iField});
    iField                                          = iField + 1;
    sTupleCrt(k).AthwartshipAngleOffsetToMainAxisOfAcBeam    = cast(Data.(subStruct).angles_athwart(iTuple, k)*100, fieldsType{iField});
    iField                                          = iField + 1;
    sTupleCrt(k).AbsorptionOfSound                           = cast(Data.RunTime.AbsorptionCoeff(iTuple)*100, fieldsType{iField});     % en 0.01 dB/km Absorption of sound in the propagation medium
    iField                                          = iField + 1;
    sTupleCrt(k).PulseDuration                               = cast(Data.RunTime.PulseLength(iTuple)*1e4, fieldsType{iField});
    iField                                          = iField + 1;
    sTupleCrt(k).PulseShapeMode                              = cast(65535, fieldsType{iField});
    iField                                          = iField + 1;
    sTupleCrt(k).BandWidth                                   = cast(Data.RunTime.BandWidth(iTuple)*100, fieldsType{iField});
    iField                                          = iField + 1;
    sTupleCrt(k).TransducerShapeMode                         = cast(3, fieldsType{iField});
    iField                                          = iField + 1;
    sTupleCrt(k).BeamAlongship3dBBeamWidth                   = cast(Data.RunTime.BeamWidthAlong(iTuple)*10, fieldsType{iField});  % Half power (3dB) beam width of the transducer in the alongship direction.
    iField                                          = iField + 1;
    sTupleCrt(k).BeamAthwartship3dBBeamWidth                 = cast((Data.RunTime.BeamWidthAthwart(iTuple)/cosd(Data.(subStruct).angles_athwart(iTuple,k)))*10, ...
        fieldsType{iField});
    iField                                          = iField + 1;
    sTupleCrt(k).BeamEquivalentTwoWayBeamAngle               = cast(-32768,      fieldsType{iField});                % Equivalent two way beam opening solid angle. MacLennan and Simmonds, Fisheries Acoustics1992, section 2.3.
    iField                                          = iField + 1;
    sTupleCrt(k).CalibrationSourceLevel                      = cast(65535,       fieldsType{iField});
    iField                                          = iField + 1;
    sTupleCrt(k).CalibrationReceivingSensitivity             = cast(-32768,      fieldsType{iField});
    iField                                          = iField + 1;
    sTupleCrt(k).SLPlusVR                                    = cast(-32768,      fieldsType{iField});
    iField                                          = iField + 1;
    sTupleCrt(k).BottomDetectionMinimumLevel                 = cast(-32768,      fieldsType{iField});
    iField                                          = iField + 1;
    sTupleCrt(k).BottomDetectionMinimumDepth                 = cast(4294967295,  fieldsType{iField});
    iField                                          = iField + 1;
    sTupleCrt(k).BottomDetectionMaximumDepth                 = cast(4294967295,  fieldsType{iField});
    iField                                          = iField + 1;
    sTupleCrt(k).Remarks                                     = cast(sprintf('%-40s', ['Sounder Name : ' SounderName(1:5)]), ...
        fieldsType{iField}); % sur 40 caract�res
    
    iField                                          = iField + 1;
    sTupleCrt(k).TupleAttribute                              = cast(0, fieldsType{iField});
    iField                                          = iField + 1;
    sTupleCrt(k).Backlink                                    = cast(156, fieldsType{iField});
    
    % Pr�paration pour comparaison des donn�es dans une structure neutre
    if isempty(idxMultiPing)
        sTupleSaved = [];
    else
        sTupleSaved = sTuplePrev(idxMultiPing,k);
    end
    
    % Ecriture des valeurs si rien n'�tait sauvegard� au pr�lable ou si ce qui �tait
    % sauvegard� �tait diff�rent.
    if isempty(sTupleSaved) || ~isequal(sTupleSaved, sTupleCrt(k))
        fwrite(file.fid, TupleSize,'uint32');
        fwrite(file.fid, TupleType,'uint16');
        fwrite(file.fid, SoftwareChannelIdentifier,'uint16');
        fwrite(file.fid, EchosounderDocIdentifier,'uint32');
        
        % Ecriture des champs selon leur type.
        %{
% Trop Lent
for iField=1:numel(f)
fwrite(file.fid, sTupleCrt(k).(f{iField}), fieldsType{iField});
end
        %}
        iField = 1;
        fwrite(file.fid, sTupleCrt(k).SamplingRate,                                 fieldsType{iField}); iField = iField + 1;
        fwrite(file.fid, sTupleCrt(k).SamplingInterval,                             fieldsType{iField}); iField = iField + 1;
        fwrite(file.fid, sTupleCrt(k).AcousticFrequency,                            fieldsType{iField}); iField = iField + 1;
        fwrite(file.fid, sTupleCrt(k).TransceiverChannelNumber,                     fieldsType{iField}); iField = iField + 1;
        fwrite(file.fid, sTupleCrt(k).TypeOfData,                                   fieldsType{iField}); iField = iField + 1;
        fwrite(file.fid, sTupleCrt(k).TimeVariedGainMultiplier,                     fieldsType{iField}); iField = iField + 1;
        fwrite(file.fid, sTupleCrt(k).TVGBlankingMode,                              fieldsType{iField}); iField = iField + 1;
        fwrite(file.fid, sTupleCrt(k).TVGMinimumRange,                              fieldsType{iField}); iField = iField + 1;
        fwrite(file.fid, sTupleCrt(k).TVGMaximumRange,                              fieldsType{iField}); iField = iField + 1;
        fwrite(file.fid, sTupleCrt(k).BlankingUpToRange,                            fieldsType{iField}); iField = iField + 1;
        fwrite(file.fid, sTupleCrt(k).SampleRange,                                  fieldsType{iField}); iField = iField + 1;
        fwrite(file.fid, sTupleCrt(k).InstallationDepthOfTransducer,                fieldsType{iField}); iField = iField + 1;
        fwrite(file.fid, sTupleCrt(k).PlatformIdentifier,                           fieldsType{iField}); iField = iField + 1;
        fwrite(file.fid, sTupleCrt(k).Space,                                        fieldsType{iField}); iField = iField + 1;
        fwrite(file.fid, sTupleCrt(k).AlongshipOffsetRelToAttSensor,                fieldsType{iField}); iField = iField + 1;
        fwrite(file.fid, sTupleCrt(k).AthwartshipOffsetRelToAttSensor,              fieldsType{iField}); iField = iField + 1;
        fwrite(file.fid, sTupleCrt(k).VerticalOffsetRelToAttSensor,                 fieldsType{iField}); iField = iField + 1;
        fwrite(file.fid, sTupleCrt(k).AlongshipAngleOffsetOfTransducer,             fieldsType{iField}); iField = iField + 1;
        fwrite(file.fid, sTupleCrt(k).AthwartshipAngleOffsetOfTransducer,           fieldsType{iField}); iField = iField + 1;
        fwrite(file.fid, sTupleCrt(k).RotationAngleOffsetOfTransducer,              fieldsType{iField}); iField = iField + 1;
        fwrite(file.fid, sTupleCrt(k).AlongshipAngleOffsetToMainAxisOfAcBeam,       fieldsType{iField}); iField = iField + 1;
        fwrite(file.fid, sTupleCrt(k).AthwartshipAngleOffsetToMainAxisOfAcBeam ,    fieldsType{iField}); iField = iField + 1;
        fwrite(file.fid, sTupleCrt(k).AbsorptionOfSound,                            fieldsType{iField}); iField = iField + 1;
        fwrite(file.fid, sTupleCrt(k).PulseDuration,                                fieldsType{iField}); iField = iField + 1;
        fwrite(file.fid, sTupleCrt(k).PulseShapeMode,                               fieldsType{iField}); iField = iField + 1;
        fwrite(file.fid, sTupleCrt(k).BandWidth,                                    fieldsType{iField}); iField = iField + 1;
        fwrite(file.fid, sTupleCrt(k).TransducerShapeMode,                          fieldsType{iField}); iField = iField + 1;
        fwrite(file.fid, sTupleCrt(k).BeamAlongship3dBBeamWidth,                    fieldsType{iField}); iField = iField + 1;
        fwrite(file.fid, sTupleCrt(k).BeamAthwartship3dBBeamWidth,                  fieldsType{iField}); iField = iField + 1;
        fwrite(file.fid, sTupleCrt(k).BeamEquivalentTwoWayBeamAngle,                fieldsType{iField}); iField = iField + 1;
        fwrite(file.fid, sTupleCrt(k).CalibrationSourceLevel,                       fieldsType{iField}); iField = iField + 1;
        fwrite(file.fid, sTupleCrt(k).CalibrationReceivingSensitivity,              fieldsType{iField}); iField = iField + 1;
        fwrite(file.fid, sTupleCrt(k).SLPlusVR,                                     fieldsType{iField}); iField = iField + 1;
        fwrite(file.fid, sTupleCrt(k).BottomDetectionMinimumLevel,                  fieldsType{iField}); iField = iField + 1;
        fwrite(file.fid, sTupleCrt(k).BottomDetectionMinimumDepth,                  fieldsType{iField}); iField = iField + 1;
        fwrite(file.fid, sTupleCrt(k).BottomDetectionMaximumDepth,                  fieldsType{iField}); iField = iField + 1;
        fwrite(file.fid, sTupleCrt(k).Remarks,                                      fieldsType{iField}); iField = iField + 1;
        fwrite(file.fid, sTupleCrt(k).TupleAttribute,                               fieldsType{iField}); iField = iField + 1;
        fwrite(file.fid, sTupleCrt(k).Backlink,                                     fieldsType{iField});
        
        % Marquage de la r��criture pour communiquer au tuple 41 la r��criture obligatoire.
        if ~isWrited
            isWrited = 1;
        end
    end
end

% Conservation des donn�es dans une structure
if isempty(idxMultiPing)
    if typeData == 0
        tabMultiPing(end+1) = iMultiPing;
    else
        tabMultiPing(end+1) = EchosounderDocIdentifier;
    end
end
if typeData == 0
    idxMultiPing = find(tabMultiPing == iMultiPing, 1, 'first');
else
    idxMultiPing = find(tabMultiPing == EchosounderDocIdentifier, 1, 'first');
end
if isempty(sTuplePrev)
    sTuplePrev = sTupleCrt;
else
    sTuplePrev(idxMultiPing,:) = sTupleCrt;
end


flag = 1;
