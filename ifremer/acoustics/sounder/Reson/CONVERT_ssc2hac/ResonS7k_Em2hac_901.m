function flag = ResonS7k_Em2hac_901(file, iTuple, NumberSWChannels, SounderName, Data, varargin)

[varargin, iMultiPing] = getPropertyValue(varargin, 'MultiPingSequence', []);
[varargin, subStruct] = getPropertyValue(varargin, 'SubStruct', 'DataSample'); %#ok<ASGLU>

persistent sTuplePrev;
persistent tabMultiPing;

% Type data sert � discriminer WaterColumn/MagAndPhase.
% Si WaterColumn ==> Monoping ou MultiPing. Le iMultiPing �quivaut � d�clarer autant de sondeurs.
% Si MagAndPhase ==> Monoping. L'identifiant du sondeur sert de cl� pour discriminer WaterColum et MagAndPhase
if strcmpi(subStruct, 'DataSample')
    typeData = 0; % type de donn�e WaterColumn;
else
    typeData = 1; % type de donn�e MagAndPhase;
end

% Doit �tre r�initialis� entre deux sessions de lancement : on commence
% toujours par le WC !
if (iTuple == 1 && typeData == 0)
    sTuplePrev          = [];
    tabMultiPing        = [];
end

flag        = 0; %#ok<NASGU>
% Arrondi � 0.1 pr�s.
soundSpeed  = fix(Data.(subStruct).soundSpeed(iTuple)*10)/10;
% Facteur d'�chelle
soundSpeed  = soundSpeed*10;

% D�claration des types de donn�es : cela est utile � la comparaison sur
% les valeurs et pour l'�criture.
fieldsType = {  'uint16'; ...
    'uint16'; ...
    'uint16'; ...
    'uint16'; ...
    'char'; ...
    'int32'; ...
    'uint32'};

% Tuple 220 MBES Echo sounder
TupleSize                           = 118;
TupleType                           = 901;
% Identifiant sondeur
EchosounderDocIdentifier            = iMultiPing+typeData;
% Enregistrement des informations utiles du Tuple pour comparaison
% avec le Ping pr�c�dent.
iField = 1;
sTupleCrt.SoundSpeed        = cast(soundSpeed, fieldsType{iField});
iField             = iField + 1;
sTupleCrt.PingInterval      = cast(0.0, fieldsType{iField});   % not available
iField             = iField + 1;
sTupleCrt.TriggerMode       = cast(65535, fieldsType{iField});	% not available
iField             = iField + 1;
sTupleCrt.Space             = cast(0, fieldsType{iField});
iField             = iField + 1;
sTupleCrt.Remarks           = cast(sprintf('%-100s', ['Sounder Name : ' SounderName(1:5)]), ...
    fieldsType{iField});                   % sur 100 caract�res
iField             = iField + 1;
sTupleCrt.TupleAttribute    = cast(0, fieldsType{iField});
iField             = iField + 1;
sTupleCrt.Backlink          = cast(128, fieldsType{iField});

% Conservation du iMultiPing pour identifier si il a d�j� �t� rencontr�.
if typeData == 0
    idxMultiPing = find(tabMultiPing == iMultiPing, 1, 'first');
else
    idxMultiPing = find(tabMultiPing == EchosounderDocIdentifier, 1, 'first');
end

% Pr�paration pour comparaison des donn�es dans une structure neutre
if isempty(idxMultiPing)
    sTupleSaved = [];
else
    sTupleSaved = sTuplePrev(idxMultiPing);
end

% Liste des champs pour factoriser l'�criture des champs avec le type
% associ�.
f   = fieldnames(sTupleCrt);

% Ecriture des valeurs si rien n'�tait sauvegard� au pr�lable ou si ce qui �tait
% sauvegard� �tait diff�rent.
if isempty(sTupleSaved) || ~isequal(sTupleSaved, sTupleCrt)
    fwrite(file.fid, TupleSize,                      'uint32');
    fwrite(file.fid, TupleType,                      'uint16');
    fwrite(file.fid, NumberSWChannels,               'uint16');
    fwrite(file.fid, EchosounderDocIdentifier,       'uint32');
    % Ecriture des champs selon leur type.
    % Forme d'�criture avec champs dynamiques qui peut �tre relativement
    % lente.
    for iField=1:numel(f)
        fwrite(file.fid, sTupleCrt.(f{iField}), fieldsType{iField});
    end
    
end


% Recherche de la pr�sence de iMultiPing dans les TuplePrev d�j�
% enregistr�s.
if isempty(idxMultiPing)
    if typeData == 0
        tabMultiPing(end+1) = iMultiPing;
    else
        tabMultiPing(end+1) = EchosounderDocIdentifier;
    end
    
end
if typeData == 0
    idxMultiPing = find(tabMultiPing == iMultiPing, 1, 'first');
else
    idxMultiPing = find(tabMultiPing == EchosounderDocIdentifier, 1, 'first');
end
% Conservation des donn�es dans une structure
if isempty(sTuplePrev)
    sTuplePrev = sTupleCrt;
else
    sTuplePrev(idxMultiPing) = sTupleCrt;
end

flag = 1;
