% Compression d'un paquet d'�chantillons de Mag and Phase d'un faisceau d'un ping.
%
% Syntax
%   ResonS7k_compression10011(nomFic, ADU)
% 
% Input Arguments
%   ADU
% 
% Name-Value Pair Arguments
%  ADU
%
% Remarks : Ce fichier calcule les paquets d'�chantillons li�s � un Ping
% avec compression inh�rente au paquet 10011.
%
% Examples
%     NbFaisceaux   = 288;
%     NbPingsTot    = 430;
%     iPing         = 1;
%     Sampling      = 29.516000366210939;
%     SoundSpeed    = 1.498199951171875e+002;
%     NbToRead      = 754;
%     [flag, NbSamples, Samples] = ResonS7k_compression10011(DataSamples, NbSamplesToRead, SeuilDeg)
%
% See also S�rie Em2hac Authors
% Authors : GLU (2016/08/10)
% -------------------------------------------------------------------------

function [flag, NbSamples, Samples] = ResonS7k_compression10011(DataPhase, NbSamplesToRead, SeuilDeg)

flag = 0;


% Traitement des samples.
NbSampThres = NaN(NbSamplesToRead, 1);
% SampSupThres        = NaN(NbSamplesToRead, 1);
nbBlanc   = 0;
NbSamples = 0;

for j = 1:NbSamplesToRead
    if (DataPhase(j) <= SeuilDeg)%|| SampleValue<(max(WC.amplitudes(index,:,j))-LS))
        nbBlanc = nbBlanc+1;
    else
        if nbBlanc > 0
            NbSampThres(j-1) = nbBlanc;
            NbSamples        = NbSamples+1;
            nbBlanc          = 0;
        end
        % Calcul de la valeur.
        NbSamples = NbSamples+1;
    end
end
% Gestion de la sortie de boucle - cas des NaN en fin de paquets.
if nbBlanc > 0
    NbSampThres(j+1) = nbBlanc;
    NbSamples        = NbSamples+1;
end

%% Calcul de la formule des �chos sous forme vectorielle.
sub                 = (DataPhase <= SeuilDeg); 
SampSupThres        = DataPhase;
SampSupThres(sub)   = NaN;
% Application du facteur d'�chelle pour le codage du format HAC
SampSupThres        = SampSupThres * 10;

% Trancodage en hexa, y compris pour une valeur n�gative.
pppp                = SampSupThres(~sub);
%  Voir http://www.mathworks.com/matlabcentral/newsreader/view_thread/138885
% pppp                = int16(typecast(bitand(uint16(typecast(int16(pppp),'uint16')),32767),'int16'));
pppp                = typecast(bitand(typecast(int16(pppp), 'uint16'), 32767), 'int16');
SampSupThres(~sub)  = pppp;

%% Calcul de masque pour la compression.

% Trancodage en hexa, y compris pour une valeur n�gative.
sub  = ~isnan(NbSampThres);
if ~all(sub == 0)
    pppp                = NbSampThres(sub);
    numSamplesLastNaN   = pppp(end);
    pppp                = typecast(int16(pppp),'uint16');
    NbThresMask         = bitor(pppp, 32768);
end

%% Merge des deux matrices en supprimant les NaN devenus inutiles.

% R�cup�ration des indices o� la valeur (> Seuil) est rest�e � NaN
sub3    = find(isnan(SampSupThres));
if ~isempty(sub3)
    % Calcul des paquets de NaN (Ecart entre samples > 1). 
    % Attention : la diff�rence supprime le dernier intervalle.
    sub4    = diff(sub3)~=1;
    % R�cup�ration des indices avec ajout du dernier intervalle.
    sub5    = [sub3(sub4) find(isnan(SampSupThres), 1, 'last')-(numSamplesLastNaN-1)];
    % Merge des deux matrices (masquage pr�calcul�).
    SampSupThres(sub5) = NbThresMask;
end

% On ne retient que les valeurs significatives.
sub     = ~isnan(SampSupThres);
Samples = SampSupThres(sub);

% Pas de padding dans le cas du 10011.

flag = 1;
