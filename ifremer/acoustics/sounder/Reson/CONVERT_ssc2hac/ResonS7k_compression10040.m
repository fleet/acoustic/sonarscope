% Compression d'un paquet d'�chantillons d'un faisceau d'un ping.
%
% Syntax
%   ResonS7k_compression10040(nomFic, ADU)
% 
% Input Arguments
%   ADU
% 
% Name-Value Pair Arguments
%  ADU
%
% Remarks : Ce fichier calcule les paquets d'�chantillons li�s � un Ping
% avec compression inh�rente au paquet 10040.
%
% Examples
%     NbFaisceaux   = 288;
%     NbPingsTot    = 430;
%     iPing         = 1;
%     Sampling      = 29.516000366210939;
%     SoundSpeed    = 1.498199951171875e+002;
%     NbToRead      = 754;
%     [flag, NbSamples, Samples] = ResonS7k_compression10040(DataSamples, NbSamplesToRead, SeuildB)
%
% See also S�rie Em2hac Authors
% Authors : GLU (2016/08/10)
% -------------------------------------------------------------------------

function [flag, NbSamples, Samples] = ResonS7k_compression10040(DataSamples, NbSamplesToRead, SeuildB)

flag = 0;


% Traitement des samples.
NbSampThres = NaN(NbSamplesToRead, 1);
% SampSupThres        = NaN(NbSamplesToRead, 1);
nbBlanc   = 0;
NbSamples = 0;

% % Sortie des facteurs multiplicateurs.
% a = 40 - TVGFunctionApplied;
% b = SoundSpeed / (2*Sampling);
% c = (pi/180)^2 * BeamWidth.along * (BeamWidth.athwart / cos(WCAngleAthwart * (pi/180)));
% d = SoundSpeed * PulseLength / 2;

DataSamples     = 20*log10(DataSamples);

for j = 1:NbSamplesToRead
    if (DataSamples(j) <= SeuildB)%|| SampleValue<(max(WC.amplitudes(index,:,j))-LS))
        nbBlanc = nbBlanc+1;
    else
        if nbBlanc > 0
            NbSampThres(j-1) = nbBlanc;
            NbSamples        = NbSamples+1;
            nbBlanc          = 0;
        end
        % Calcul de la valeur.
        % SampSupThres(j) = (DataSamples(j)-10*log(j*SoundSpeed/(2*Sampling)))*100;
        % SampSupThres(j) = (DataSamples(j)+(40-TVGFunctionApplied)*log10(j*SoundSpeed/(2*Sampling)) - 10*log10((pi/180)^2*BeamWidth.along*(BeamWidth.athwart/cos(WCAngleAthwart*pi/180))*(j*SoundSpeed/(2*Sampling))^2*(SoundSpeed*PulseLength/2)))*100;
        % SampSupThres(j) = (DataSamples(j)+(a)*log10(j*b) - 10*log10(c*(j*b)^2*d))*100;
        NbSamples = NbSamples+1;
    end
end
% Gestion de la sortie de boucle - cas des NaN en fin de paquets.
if nbBlanc > 0
    NbSampThres(j+1) = nbBlanc;
    NbSamples        = NbSamples+1;
end

%% Calcul de la formule des �chos sous forme vectorielle.
sub             = (DataSamples <= SeuildB); 
B               = DataSamples;
B(sub)          = NaN;

% J               = 1:NbSamplesToRead;
% SampSupThres    = (B+(a).*log10(J.*b) - 10*log10(c.*(J.*b).^2.*d))*100;


% Jb              = J.*b;
% X               = (a-20) .* log10(Jb) + (B - 10*log10(c) - 10*log10(d));
% Facteur d'�chelle 0.001 pour les Volts et 0.01 en dB dans le format HAC.
SampSupThres    = B * 100;


% Trancodage en hexa, y compris pour une valeur n�gative.
pppp                = SampSupThres(~sub);
%  Voir http://www.mathworks.com/matlabcentral/newsreader/view_thread/138885
% pppp                = int16(typecast(bitand(uint16(typecast(int16(pppp),'uint16')),32767),'int16'));
pppp                = typecast(bitand(typecast(int16(pppp), 'uint16'), 32767), 'int16');
SampSupThres(~sub)  = pppp;

%% Calcul de masque pour la compression.

% Trancodage en hexa, y compris pour une valeur n�gative.
sub  = ~isnan(NbSampThres);
if ~all(sub == 0)
    pppp                = NbSampThres(sub);
    numSamplesLastNaN   = pppp(end);
    pppp                = typecast(int16(pppp),'uint16');
    NbThresMask         = bitor(pppp, 32768);
end

%% Merge des deux matrices en supprimant les NaN devenus inutiles.

% R�cup�ration des indices o� la valeur (> Seuil) est rest�e � NaN
sub3    = find(isnan(SampSupThres));
if ~isempty(sub3)
    % Calcul des paquets de NaN (Ecart entre samples > 1). 
    % Attention : la diff�rence supprime le dernier intervalle.
    sub4    = diff(sub3)~=1;
    % R�cup�ration des indices avec ajout du dernier intervalle.
    sub5    = [sub3(sub4) find(isnan(SampSupThres), 1, 'last')-(numSamplesLastNaN-1)];
    % Merge des deux matrices (masquage pr�calcul�).
    SampSupThres(sub5) = NbThresMask;
end

% On ne retient que les valeurs signficatives.
sub     = ~isnan(SampSupThres);
Samples = SampSupThres(sub);

% Ecriture d'une valeur de Padding (en l'occurence = valeur de d�passement du seuil).
if (NbSamples/2 - round(NbSamples/2)) ~= 0
    pppp      = 32768;
    Samples   = [Samples pppp];
    NbSamples = NbSamples+1;
end

flag = 1;
