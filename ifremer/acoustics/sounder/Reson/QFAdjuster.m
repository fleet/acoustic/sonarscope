%
% % nomDir = my_tempdir;
% % nomDir = checkBDFromErmapperFiles(nomDir)
%
%
% nomDir = 'C:\TestBD7150Canyon20080624\24kHz\20080220_194014'
% nomDir = 'C:\TestBD7150Canyon20080624\12kHz\20080220_220128'
% listeFic = listeFicOnDir(nomDir, '.ers');
% [flag, a] = cl_image.import_ermapper(listeFic);
% strDataType = get(a(1), 'strDataType');
% for i=1:length(a)
%     DataType(i) = a(i).DataType; %#ok<AGROW>
%     disp(strDataType{DataType(i)})
% end
%
% indMbesQualityFactor = find(DataType == cl_image.indDataType('MbesQualityFactor'));
% indBathymetry        = find(DataType == cl_image.indDataType('Bathymetry'));
%
% Bathymetry = get(a(indBathymetry), 'Image');
% QF         = get(a(indMbesQualityFactor(end)), 'Image');
% QF3        = get(a(indMbesQualityFactor(end-2)), 'Image');
%
% obj = QFAdjuster(Bathymetry, QF, QF3)
% obj = editobj(obj)
% obj = QFAdjuster(obj)
% get_Seuil(obj)
% get_StatusQF3(obj)
% obj = set_Seuil(obj, 2.5)
% obj = QFAdjuster(obj)
% obj = set_StatusQF3(obj, 0)
% obj = QFAdjuster(obj)
% Mask = get_Mask(obj);
% figure; imagesc(Mask); colorbar;


classdef QFAdjuster < handle
    properties (SetAccess = public, GetAccess = public)
        Seuil
        StatusQF3
        Bathy
        QF
        QF3
        Position
        Titre
    end
    
    properties (SetAccess = private, GetAccess = private)
        Figure
        handleAxesBathy
        handleAxesQF
        handleImageBathy
        handleImageQF
        handleAxesProfilBathy
        handleAxesProfilQF
        handlePlotProfilBathy
        handlePlotProfilQF
        handlePlotProfilBathy2
        handlePlotProfilQF2
        handleImageProfilBathy2
        handleImageProfilQF2
        CLimit
        Slider
        uiQF3
        uiSeuil
        ui3D
        uiImage
        uiSun
        uiContraste
    end % properties
    
    methods
        function obj = QFAdjuster(varargin)
            [varargin, Titre] = getPropertyValue(varargin, 'Tag', []);
            if length(varargin) == 3
                obj.Bathy   = varargin{1};
                obj.QF      = varargin{2};
                obj.QF3     = varargin{3};
                
                obj.Seuil     = min(obj.QF(:));
                obj.StatusQF3 = 0;
                obj.Titre = Titre;
            else
                obj = varargin{1};
            end
        end
        
        function varargout = editobj(obj)
            if isempty(obj.Position)
                obj.Figure = figure('Colormap', jet(256));
            else
                obj.Figure = figure('Colormap', jet(256), 'Position', obj.Position);
            end
            set(obj.Figure, 'DeleteFcn', @(src,event)deleteFigure_cb(obj,src,event))
            if ~isempty(obj.Titre)
                set(obj.Figure, 'Name', obj.Titre)
            end
            
            % Création des panels
            
            hPanelButtons = uipanel('Position', [0.02 0.92 0.96 0.06]);
            hPanelImages  = uipanel('Position', [0.02 0.22 0.96 0.70]);
            hPanelPlots   = uipanel('Position', [0.02 0.02 0.96 0.18]);
            
            obj.uiImage = uicontrol('Style', 'pushbutton ',...
                'Parent', hPanelButtons,...
                'String', 'Image', ...
                'Units', 'normalized', ...
                'Position', [0.01 0.1 0.08 0.90],...
                'Callback', @(src,event)AffichageImage_cb(obj, src, event));
            
            obj.uiSun = uicontrol('Style', 'pushbutton ',...
                'Parent', hPanelButtons,...
                'String', 'Sun', ...
                'Units', 'normalized', ...
                'Position', [0.11 0.1 0.08 0.90],...
                'Callback', @(src,event)AffichageSun_cb(obj, src, event));
            
            obj.ui3D = uicontrol('Style', 'pushbutton ',...
                'Parent', hPanelButtons,...
                'String', '3D', ...
                'Units', 'normalized', ...
                'Position', [0.21 0.1 0.08 0.90],...
                'Callback', @(src,event)Affichage3D_cb(obj, src, event));
            
            obj.uiContraste = uicontrol('Style', 'togglebutton ',...
                'Parent', hPanelButtons,...
                'String', '0.5 %', ...
                'Units', 'normalized', ...
                'Position', [0.31 0.1 0.08 0.90],...
                'Callback', @(src,event)Contraste_cb(obj, src, event));
            
            
            % Création du bouton poussoir QF3
            if isempty(obj.StatusQF3)
                obj.StatusQF3 = 0;
            end
            obj.uiQF3 = uicontrol('Style', 'togglebutton ',...
                'Parent', hPanelButtons,...
                'String', 'QF3', ...
                'Value', obj.StatusQF3,...
                'Units', 'normalized', ...
                'Position', [0.65 0.1 0.08 0.90],...
                'Callback', @(src,event)Affichage_cb(obj, src, event));
            
            % Création de l'axe de la bathy
            [DrawModeName, DrawModeValue] = getDrawModePropertyNameAndValue;
            obj.handleAxesBathy  = axes(...
                'XLimMode','manual','YLimMode','manual',...
                DrawModeName, DrawModeValue,...
                'Units', 'normalized', ...
                'Position', [0.05 0.05 0.4 0.9],...
                'Parent',hPanelImages);
            obj.handleImageBathy = image(obj.Bathy,'CDataMapping','scaled', 'Parent',obj.handleAxesBathy); colorbar('WestOutside');
            axis xy;
            title('Bathymetry')
            set(obj.handleImageBathy, 'ButtonDownFcn', @(src,event)ClickPing_cb(obj, src, event))
            
            hold on;
            obj.handleImageProfilBathy2 = plot([1 size(obj.Bathy,2)], [1 1], 'k', 'Parent',obj.handleAxesBathy);
            hold off;
            
            % Création de l'axe du QF
            [DrawModeName, DrawModeValue] = getDrawModePropertyNameAndValue;
            obj.handleAxesQF     = axes(...
                'XLimMode','manual','YLimMode','manual',...
                DrawModeName, DrawModeValue,...
                'Units', 'normalized', ...
                'Position', [0.50 0.05 0.4 0.9],...
                'Parent',hPanelImages);
            obj.handleImageQF = image(obj.QF,'CDataMapping','scaled', 'Parent', obj.handleAxesQF); colorbar;
            axis xy;
            title('Quality Factor')
            set(obj.handleImageQF, 'ButtonDownFcn', @(src,event)ClickPing_cb(obj, src, event))
            obj.CLimit = get(obj.handleAxesQF, 'CLim');
            
            hold on;
            obj.handleImageProfilQF2 = plot([1 size(obj.QF,2)], [1 1], 'k', 'Parent',obj.handleAxesQF);
            hold off;
            
            
            linkaxes([obj.handleAxesBathy obj.handleAxesQF]); axis tight; drawnow;
            
            
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % Création de l'axe du profil bathy
            [DrawModeName, DrawModeValue] = getDrawModePropertyNameAndValue;
            obj.handleAxesProfilBathy  = axes(...
                'XLimMode','manual','YLimMode','manual',...
                DrawModeName, DrawModeValue,...
                'Units', 'normalized', ...
                'Position', [0.18 0.15 0.27 0.8],...
                'Parent', hPanelPlots);
            obj.handlePlotProfilBathy  = plot(obj.Bathy(1,:), 'Parent',obj.handleAxesProfilBathy);
            hold on;
            obj.handlePlotProfilBathy2 = plot(obj.Bathy(1,:), 'k', 'Parent',obj.handleAxesProfilBathy);
            dragplot
            hold off;
            grid on;
            
            % Création de l'axe du QF
            [DrawModeName, DrawModeValue] = getDrawModePropertyNameAndValue;
            obj.handleAxesProfilQF     = axes(...
                'XLimMode','manual','YLimMode','manual',...
                DrawModeName, DrawModeValue,...
                'Units', 'normalized', ...
                'Position', [0.50 0.15 0.27 0.8],...
                'Parent', hPanelPlots);
            obj.handlePlotProfilQF = plot(obj.QF(1,:), 'Parent',obj.handleAxesProfilQF);
            hold on;
            obj.handlePlotProfilQF2 = plot(obj.Bathy(1,:), 'k', 'Parent',obj.handleAxesProfilQF);
            dragplot
            hold off;
            grid on;
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            
            
            % Création du slider
            
            if isempty(obj.Seuil)
                obj.Seuil = obj.CLimit(1);
            else
                obj.Seuil = max(obj.Seuil, obj.CLimit(1));
                obj.Seuil = min(obj.Seuil, obj.CLimit(2));
            end
            obj.Slider = uicontrol('Style','slider',...
                'Parent', hPanelImages,...
                'Max', obj.CLimit(2),...
                'Min', obj.CLimit(1),...
                'Value', obj.Seuil,...
                'Units', 'normalized', ...
                'Position', [0.925 0.05 0.05 0.85],...
                'SliderStep', [.005 .002],...
                'Callback', @(src,event)Affichage_cb(obj, src, event));
            
            
            
            % Création du bouton poussoir QF3
            uicontrol('Style', 'Text',...
                'Parent', hPanelButtons,...
                'String', 'Threshold', ...
                'Units', 'normalized', ...
                'FontWeight', 'bold', ...
                'Position', [0.75 0.0 0.15 0.65]);
            
            obj.uiSeuil = uicontrol('Style', 'Edit',...
                'Parent', hPanelButtons,...
                'String', num2str(obj.Seuil), ...
                'Units', 'normalized', ...
                'FontWeight', 'bold', ...
                'BackgroundColor', [1 1 1], ...
                'Position', [0.9 0.05 0.1 0.85], ...
                'Callback', @(src,event)EditionSeuil_cb(obj,src,event));
            
            
            Affichage_cb(obj, [], [])
            set(obj.Figure, 'WindowButtonMotionFcn', @(src,event)MovePointer_cb(obj,src,event))
            
            if nargout ~= 0
                uiwait(obj.Figure);
                varargout{1} = obj;
            end
        end % QFAdjuster
        
        
        function Seuil = get_Seuil(obj)
            Seuil = obj.Seuil;
        end
        
        function obj = set_Seuil(obj, Seuil)
            obj.Seuil = Seuil;
        end
        
        function StatusQF3 = get_StatusQF3(obj)
            StatusQF3 = obj.StatusQF3;
        end
        
        function obj = set_StatusQF3(obj, StatusQF3)
            obj.StatusQF3 = StatusQF3;
        end
        
        function obj = set_Position(obj, Position)
            obj.Position = Position;
        end
        
        function Position = get_Position(obj)
            Position = obj.Position;
        end
        
        function display(obj)
            fprintf('Seuil     : %f\n', obj.Seuil);
            fprintf('StatusQF3 : %f\n', obj.StatusQF3);
            sz = size(obj.Bathy);
            fprintf('Mask      : [%d %d] logical\n', sz(1), sz(2))
        end
        
        function Mask = get_Mask(obj)
            subNaN = isnan(obj.Bathy) | (obj.Bathy > -12);
            subQF  = (obj.QF < obj.Seuil);
            subQF3 = (obj.QF3 == 0);
            
            Mask = subNaN | subQF;
            if obj.StatusQF3
                Mask = Mask | subQF3;
                nQF3  = sum(subQF3(:));
            else
                nQF3 = 0;
            end
            
            N     = numel(obj.Bathy);
            NbNaN = sum(subNaN(:));
            nQF   = sum(subQF(:));
            
            disp('---------------------------------------------------------------------------')
            fprintf('Total number of beams                       : %d\n', N)
            fprintf('Number of missing beams                     : %d (%.2f%%)\n', NbNaN, 100*NbNaN/N)
            fprintf('Threshold on QF                             : %f\n', obj.Seuil);
            fprintf('Number of beams rejected by QF              : %d (%.2f%%)\n', nQF, 100*nQF/N)
            fprintf('Number of beams rejected by QF3             : %d (%.2f%%)\n', nQF3, 100*nQF3/N)
            
            sub2 = (subQF & subQF3);
            n = sum(sub2(:));
            fprintf('Number of common beams rejected by QF & QF3 : %d (%.2f%%)\n', n, 100*n/N)
            n = sum(Mask(:));
            fprintf('Total number of rejected beams              : %d (%.2f%%)\n', n, 100*n/N)
        end
        
        function Contraste_cb(obj, src, event) %#ok<INUSD>
            CLim = get_CLim(obj);
            set(obj.handleAxesBathy, 'CLim', CLim)
            
            %             [Val, str] = stats(obj.QF);
            %             Status = get(obj.uiContraste, 'Value');
            %             if Status
            %                 set(obj.handleAxesQF, 'CLim', Val.Quant_25_75)
            %             else
            %                 set(obj.handleAxesQF, 'CLim', [Val.Min Val.Max])
            %             end
        end
        
        function MovePointer_cb(obj, src, event) %#ok<INUSD>
            PointBathy = get(obj.handleAxesBathy, 'CurrentPoint');
            PointQF    = get(obj.handleAxesQF, 'CurrentPoint');
            
            if (PointBathy(1,2) >= 1) && (PointBathy(1,2) <= size(obj.Bathy,1)) ...
                    && (PointBathy(1,1) >= 1) && (PointBathy(1,1) <= size(obj.Bathy,2))
                iLig = floor(PointBathy(1,2));
            elseif (PointQF(1,2) >= 1) && (PointQF(1,2) <= size(obj.QF,1)) ...
                    && (PointQF(1,1) >= 1) && (PointQF(1,1) <= size(obj.QF,2))
                iLig = floor(PointQF(1,2));
            else
                iLig = [];
            end
            set(obj.handlePlotProfilBathy, 'YData', obj.Bathy(iLig,:))
            set(obj.handlePlotProfilQF,    'YData', obj.QF(iLig,:))
            
        end
        
        
        function ClickPing_cb(obj, src, event) %#ok<INUSD>
            SelectionType = get(obj.Figure, 'SelectionType');
            PointBathy = get(obj.handleAxesBathy, 'CurrentPoint');
            iLig = floor(PointBathy(1,2));
            if strcmp(SelectionType, 'normal')
                set(obj.handlePlotProfilBathy2,  'YData', obj.Bathy(iLig,:))
                set(obj.handlePlotProfilQF2,     'YData', obj.QF(iLig,:))
                set(obj.handleImageProfilBathy2, 'YData', [iLig iLig])
                set(obj.handleImageProfilQF2,    'YData', [iLig iLig])
            elseif strcmp(SelectionType, 'alt')
                Y = NaN(1,size(obj.Bathy,2));
                set(obj.handlePlotProfilBathy2,  'YData', Y)
                set(obj.handlePlotProfilQF2,     'YData', Y)
                set(obj.handleImageProfilBathy2, 'YData', [NaN NaN])
                set(obj.handleImageProfilQF2,    'YData', [NaN NaN])
            else
                set(obj.handlePlotProfilBathy2,  'YData', obj.Bathy(iLig,:))
                set(obj.handlePlotProfilQF2,     'YData', obj.QF(iLig,:))
                set(obj.handleImageProfilBathy2, 'YData', [iLig iLig])
                set(obj.handleImageProfilQF2,    'YData', [iLig iLig])
                figure;
                PlotUtils.createSScPlot(obj.Bathy(iLig,:)); grid on; title(['Ping ' num2str(iLig)])
                figure(obj.Figure)
            end
        end
        
        function EditionSeuil_cb(obj, src, event) %#ok<INUSD>
            obj.Seuil = str2double(get(obj.uiSeuil, 'String'));
            obj.Seuil = max(obj.Seuil, obj.CLimit(1));
            obj.Seuil = min(obj.Seuil, obj.CLimit(2));
            set(obj.uiSeuil, 'String', num2str(obj.Seuil))
            set(obj.Slider, 'Value', obj.Seuil)
            Affichage_cb(obj, [], [])
        end
        
        function Affichage_cb(obj, src, event) %#ok<INUSD>
            obj.StatusQF3 = get(obj.uiQF3, 'Value');
            obj.Seuil  = get(obj.Slider, 'Value');
            
            Mask = get_Mask(obj);
            %
            x = obj.Bathy;
            x(Mask) = NaN;
            set(obj.handleImageBathy, 'CData', x)
            
            x = obj.QF;
            x(Mask) = NaN;
            set(obj.handleImageQF, 'CData', x)
            
            set(obj.handleAxesQF,'CLim', obj.CLimit)
            
            str = sprintf('Acuuracy : %3.1f/1000', 1000*10^-obj.Seuil);
            set(obj.uiSeuil, 'String', num2str(obj.Seuil), 'TooltipString', str)
            
            drawnow
        end % QF3_cb
        
        function deleteFigure_cb(obj, src, event) %#ok<INUSD>
            obj.Position = get(obj.Figure, 'Position');
        end
        
        function AffichageImage_cb(obj, src, event) %#ok<INUSD>
            CLim = get_CLim(obj);
            x = obj.Bathy;
            x(get_Mask(obj)) = NaN;
            figure; imagesc(x, CLim); axis xy; colorbar;
        end
        
        function AffichageSun_cb(obj, src, event) %#ok<INUSD>
            CLim = get_CLim(obj);
            x = obj.Bathy;
            x(get_Mask(obj)) = NaN;
            x = fillValNaN(x, [3 3], NaN);
            RGB = ombrage(x, 'az', 220, 'CLim', CLim); % sunShading ?
            figure; imagesc(RGB); axis xy; colorbar; title('Sun ilumination after [3 3] interpolation');
        end
        
        function Affichage3D_cb(obj, src, event) %#ok<INUSD>
            CLim = get_CLim(obj);
            x = obj.Bathy;
            x(get_Mask(obj)) = NaN;
            x = fillValNaN(x, [3 3], NaN);
            
            a = cl_image('Image', x);
            b = sunShading(a(1), 'az', 220, 'CLim', CLim);
            
            nomFicOut = my_tempname('.sd');
            flag = export_fledermaus(a, nomFicOut, 'LayerMapping', b, 'NoMessage');
            if flag
                winopen(nomFicOut)
            end
        end
        
        function CLim = get_CLim(obj)
            Status = get(obj.uiContraste, 'Value');
            X = obj.Bathy;
            X = X(~get_Mask(obj));
            Val = stats(X);
            if Status
                CLim = Val.Quant_25_75;
            else
                CLim = [Val.Min Val.Max];
            end
        end
        
    end % methods
end % classdef
