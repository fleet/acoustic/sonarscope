function R0Strip = Reson7150_R0Strip_estimationSeuil(Amp, Start, EndSample, SeuilAmp, Rmin, Rmax, Fig, CLim, iPing, Display)

NbStrips = 81;
nbBeams   = size(Amp,2);
nbSamples = size(Amp,1);

if ~isempty(Fig)
    figure(Fig); hold off;
    if isempty(CLim)
        imagesc(Amp);
    else
        imagesc(Amp, CLim);
    end
    title(sprintf('Ping  %d', iPing)); colorbar;
end

nbBeams5 = nbBeams / NbStrips;
R0Strip = NaN(1,nbBeams);
r0Strip = NaN(1,NbStrips);
for iStrip=1:NbStrips
    k1 = 1 + floor((iStrip-1) * nbBeams5);
    k2 = floor(iStrip * nbBeams5);
    sub = k1:k2;
    RminStrip = min(Rmin(sub));
    RmaxStrip = max(Rmax(sub));
    MaxEndSample = max(EndSample(sub));
    
    MaxEndSample = min(MaxEndSample, ceil(RmaxStrip));
    StartStrip = max(Start, floor(RminStrip));
    if StartStrip == nbSamples
        continue
    end
    
    MoyHorz = mean(Amp(Start:min(nbSamples,MaxEndSample),sub), 2, 'omitnan')';
%     figure; plot(MoyHorz); grid on;
    R = find(MoyHorz > 2.5, 1, 'first');
    if ~isempty(R)
        d = find(MoyHorz(R:-1:1) < 1, 1, 'first');
        if isempty(d)
            r0Strip(iStrip) = R;
        else
            r0Strip(iStrip) = R - d;
        end
    end
end
r0Strip = r0Strip + Start;

% figure; plot(r0Strip, '*'); grid on; set(gca, 'YDir', 'Reverse');
Rap = r0Strip / min(r0Strip);
subEqual = find(Rap == 1);

for k=1:length(subEqual)
    if (subEqual(k) > 2) && (Rap(subEqual(k)-1) > 1.9)
        r0Strip(subEqual(k)-1) = (r0Strip(subEqual(k)-2) + r0Strip(subEqual(k))) / 2;
        r0Strip(subEqual(k)-1) = r0Strip(subEqual(k)-1)-1;
    end
    if (subEqual(k) < (NbStrips-1)) && (Rap(subEqual(k)+1) > 1.9)
        r0Strip(subEqual(k)+1) = (r0Strip(subEqual(k)+2) + r0Strip(subEqual(k))) / 2;
        r0Strip(subEqual(k)+1) = r0Strip(subEqual(k)+1)-1;
    end
end
% figure; plot(Rap, '*'); grid on;



subNan = find(isnan(r0Strip));
if ~isempty(subNan)
    subNotNan = find(~isnan(r0Strip));
    r0Strip(subNan) = floor(interp1(subNotNan, r0Strip(subNotNan), subNan));
end
for iStrip=1:NbStrips
    k1 = 1 + floor((iStrip-1) * nbBeams5);
    k2 = floor(iStrip * nbBeams5);
    sub = k1:k2;
    R0Strip(sub) = r0Strip(iStrip);
end

