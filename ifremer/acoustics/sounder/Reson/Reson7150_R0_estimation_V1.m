function [R0, R0Strip, flagBeams, QF] = Reson7150_R0_estimation_V1(Amp, NbStrips, Start, EndSample, SeuilAmp, Rmin, Rmax, Fig, CLim, iPing, Display)

global TestPlatformCmatlab %#ok<GVMIS>

nbBeams   = size(Amp,2);
nbSamples = size(Amp,1);

if ~isempty(Fig)
    figure(Fig); hold off;
    if isempty(CLim)
        imagesc(Amp);
    else
        imagesc(Amp, CLim);
    end
    title(sprintf('Ping  %d', iPing)); colormap(jet(256)); colorbar;
end

nbBeams5 = nbBeams / NbStrips;
R1Max       = NaN(1,NbStrips);
R2Max       = NaN(1,NbStrips);
R3Max       = NaN(1,NbStrips);
flagR       = zeros(1,NbStrips);
R0Strip     = NaN(1,nbBeams);
r0Strip     = NaN(1,NbStrips);
r0Strip_M   = NaN(1,NbStrips);
r0Strip_C   = NaN(1,NbStrips);
flagBeams_M = zeros(1,nbBeams);
flagBeams_C = zeros(1,nbBeams);
QF          = NaN(1,nbBeams);
for iStrip=1:NbStrips
    k1 = 1 + floor((iStrip-1) * nbBeams5);
    k2 = floor(iStrip * nbBeams5);
    sub = k1:k2;
    RminStrip = min(Rmin(sub));
    RmaxStrip = max(Rmax(sub));
    MaxEndSample = max(EndSample(sub));
    
    MaxEndSample = min(MaxEndSample, ceil(RmaxStrip));
    StartStrip = max(Start, floor(RminStrip));
    if StartStrip == nbSamples
        continue
    end
    
    %{
    % TODO Donn�es USAN Leg2
    243 / 248 : P:\WATERCOLUMN\P40\20120126_161051.s7k
Warning: Integer operands are required for colon operator when used as index 
> In Reson7150_R0_estimation_V1 at 40
    %}
     if any(TestPlatformCmatlab.estimationR0OnStrip == [1 3 4])
         [R1Max(iStrip), R2Max(iStrip), R3Max(iStrip), S1, S2, S3, TypeSignal, flagR1, flagR2, flagR3,  r0Strip_M(iStrip), qf_M] = ...
             estimationR0OnStrip(Amp(1:min(nbSamples,MaxEndSample),sub), StartStrip, SeuilAmp, Display);
         if flagR1 || flagR2 || flagR3
             flagBeams_M(sub) = 1;
         end
     end
    
    if any(TestPlatformCmatlab.estimationR0OnStrip == [2 3 4])
        [r0Strip_C(iStrip), flagBeams_C(sub)] = ...
            estimationR0OnStrip_reson(Amp(1:min(nbSamples,MaxEndSample),sub), StartStrip, SeuilAmp, Display);
    end
        
    switch TestPlatformCmatlab.estimationR0OnStrip
        case {1; 3}
            r0Strip(iStrip)   = r0Strip_M(iStrip);
        case {2; 4}
            r0Strip(iStrip)   = r0Strip_C(iStrip);
    end
    
    %if max(abs(r0Strip(iStrip) - r0Strip_C(iStrip))) > 0
    %    figure;
    %    plot(r0Strip(iStrip) - r0Strip_C(iStrip));
    %end
    
    
    switch TestPlatformCmatlab.estimationR0OnStrip
        case {1; 3}
            flagBeams = flagBeams_M;
            qf        = qf_M;
        case {2; 4}
            flagBeams   = flagBeams_C;
            qf        = qf_M; % qf_C; TODO : Didier Riou (VU) : sortir qf de estimationR0OnStrip_reson -> param�tre de sortie inexistant dans estimationR0OnStrip_reson
    end
    
    QF(sub) = qf;

    if ~isempty(Fig)
        plot_signal(Fig, S1, S2, S3, StartStrip, R1Max(iStrip), R2Max(iStrip), R3Max(iStrip), ...
            sub, TypeSignal)
    end
    flagR(iStrip) = flagR1 | flagR2 | flagR3;

    if flagR(iStrip)
        if r0Strip(iStrip) <= 10
            r0Strip(iStrip)= NaN;
        end
    else
        r0Strip(iStrip)= NaN;
    end
end

if any(TestPlatformCmatlab.estimationR0OnStrip == [3 4])
     BDA_compare_Matlab_C('estimationR0OnStrip', 'r0Strip',   r0Strip_M,   r0Strip_C,   iPing, [], TestPlatformCmatlab.Seuil_estimationR0OnStrip_r0Strip);
     BDA_compare_Matlab_C('estimationR0OnStrip', 'flagBeams', flagBeams_M, flagBeams_C, iPing, [], TestPlatformCmatlab.Seuil_estimationR0OnStrip_flagBeams);
end

% figure; plot(r0Strip, '*'); grid on; set(gca, 'YDir', 'Reverse');
Rap = r0Strip / min(r0Strip);
subEqual = find(Rap == 1);

% TODO TODO TODO TODO TODO TODO TODO TODO TODO 
% BEWARE : r0Strip(subEqual(k)-2)) can be NaN :

for k=1:length(subEqual)
    if (subEqual(k) > 2) && (Rap(subEqual(k)-1) > 1.9)
        r0Strip(subEqual(k)-1) = (r0Strip(subEqual(k)-2) + r0Strip(subEqual(k))) / 2;
        r0Strip(subEqual(k)-1) = r0Strip(subEqual(k)-1)-1;
    end
    if (subEqual(k) < (NbStrips-1)) && (Rap(subEqual(k)+1) > 1.9)
        r0Strip(subEqual(k)+1) = (r0Strip(subEqual(k)+2) + r0Strip(subEqual(k))) / 2;
        r0Strip(subEqual(k)+1) = r0Strip(subEqual(k)+1)-1;
    end
end
% figure; plot(Rap, '*'); grid on; grid on; set(gca, 'YDir', 'Reverse');

flagBeams = imdilate(flagBeams, ones(1,length(sub)));
R0 = floor(min(r0Strip));

%{
figure; 
h(1) = subplot(2,1,1); plot(r0Strip, '*'); grid on; set(gca, 'YDir', 'Reverse');
h(2) = subplot(2,1,2); plot(gradient(r0Strip) ./ r0Strip, '*'); grid on;
linkaxes(h, 'x')
%}

subNan = find(isnan(r0Strip));
if ~isempty(subNan)
    subNotNan = find(~isnan(r0Strip));
    %  MODIFICATION / REFERENCE : 20/04/2008
    if length(subNotNan) < 2
        R0 = NaN;
        return
    end
%     r0Strip(subNan) = floor(interp1(subNotNan, r0Strip(subNotNan), subNan));
    r0Strip(subNan) = floor(interp1(subNotNan, r0Strip(subNotNan), subNan));
end
for iStrip=1:NbStrips
    k1 = 1 + floor((iStrip-1) * nbBeams5);
    k2 = floor(iStrip * nbBeams5);
    sub = k1:k2;
    R0Strip(sub) = r0Strip(iStrip);
end

if ~isempty(Fig)
    figure(Fig)
    hold on;
    hc = plot([1 size(Amp,2)], [R0 R0],'k'); set(hc, 'LineWidth', 2);
    hold off;
%     title(sprintf('Ping %d', iPing))
end


function [R1, R2, R3, S1Cum, S2Cum, S3Cum, TypeSignal, flagR1Amp, flagR2Amp, flagR3Amp, R0Strip, QF] = estimationR0OnStrip(Amp, Start, SeuilAmp, Display)

MoyHorz = mean(  Amp(Start:end,:), 2, 'omitnan')';
MedHorz = median(Amp(Start:end,:), 2, 'omitnan')';
nbSamples = length(MoyHorz);
subNonNaN = find(~isnan(MoyHorz));

if isempty(subNonNaN)
%     R1 = 0;
%     R2 = 0;
%     R3 = 0;
%     S1Cum = 0;
%     S2Cum = 0;
%     S3Cum = 0;
%     TypeSignal = 0;
%     flagR1Amp = 0;
%     flagR2Amp = 0;
%     flagR3Amp = 0;
%     R0Strip = 0;
%     QF = 0;
    R1 = NaN;
    R2 = NaN;
    R3 = NaN;
    S1Cum = NaN;
    S2Cum = NaN;
    S3Cum = NaN;
    TypeSignal = NaN;
    flagR1Amp = 0;
    flagR2Amp = 0;
    flagR3Amp = 0;
    R0Strip = NaN;
    QF = NaN;
    return
end

S1 = MoyHorz - min(MoyHorz);
S1 = S1 / max(MoyHorz);
S1 = S1 .^ 2;
S1Cum = NaN(size(subNonNaN));
S1Cum(subNonNaN) = cumsum(S1(subNonNaN));
S1Cum = S1Cum / S1Cum(subNonNaN(end));

S2 = NaN(1,nbSamples);
S2(subNonNaN) = (MoyHorz(subNonNaN) - median(MoyHorz(subNonNaN)));
S2 = S2 / max(S2);
S2Cum = NaN(size(subNonNaN));
S2Cum(subNonNaN) = cumsum(S2(subNonNaN));
S2Cum = S2Cum / S2Cum(subNonNaN(end));

MoyMinusMed = MoyHorz - MedHorz;
S3 = NaN(1,nbSamples);
Med = median(MoyMinusMed(subNonNaN)); % =0 en principe
S3(subNonNaN) = (MoyMinusMed(subNonNaN) - Med);
% S3 = S3 / max(S3);
S3Cum = NaN(size(subNonNaN));
S3Cum(subNonNaN) = cumsum(S3(subNonNaN));
S3Cum = S3Cum / S3Cum(subNonNaN(end));

if Display == 4
    figure(5557);
    h = preallocateGraphics(0,0);
    h(end+1) = subplot(4,2,1); plot(S1, 'k'); grid on; title('S1');
    h(end+1) = subplot(4,2,2); plot(S1Cum, 'k'); grid on; title('S1Cum');

    h(end+1) = subplot(4,2,3); plot(S2, 'r'); grid on; title('S2');
    h(end+1) = subplot(4,2,4); plot(S2Cum, 'r'); grid on; title('S2Cum');

    h(end+1) = subplot(4,2,5); plot(S3, 'g'); grid on; title('S3');
    h(end+1) = subplot(4,2,6); plot(S3Cum, 'g'); grid on; title('S3Cum');

    h(end+1) = subplot(4,2,7); plot(S1, 'k'); hold on; plot(S2,'r'); plot(S3, 'g'); grid on; hold off;
    h(end+1) = subplot(4,2,8); plot(S1Cum, 'k'); hold on; plot(S2Cum,'r'); plot(S3Cum, 'g'); grid on; hold off;
    % legend({'S1'; 'S2'; 'S3'})
    linkaxes(h,'x')
end

Seuil = 0.5;
R1 = find(S1Cum >= Seuil, 1, 'first') + (Start-1);
R2 = find(S2Cum >= Seuil, 1, 'first') + (Start-1);
R3 = find(S3Cum >= Seuil, 1, 'first') + (Start-1);

if isempty(R1)
    R1 = NaN;
end

if isempty(R2)
    R2 = NaN;
end

if isempty(R3)
    R3 = NaN;
end

flagR1Noise = (max(abs(S1Cum)) < 2);

% Modification SB 16/05/2008 pour fichier 20080512_175721 ping 151, must be
% estimated on several files
% flagR2Noise = (max(abs(S2Cum)) < 2);
flagR2Noise = (min(S2Cum) > -0.6) && (max(abs(S2Cum)) < 2);

% MODIFICATION / REFERENCE : 20/04/2008
% flagR3Noise = (max(abs(S3Cum)) < 2);
% flagR3Noise = (min(S3Cum) > -0.75) && (max(S3Cum) < 1.75);
flagR3Noise = (min(S3Cum) > -0.6) && (max(S3Cum) < 1.6);

flagR1Amp = testAmplitude(MoyHorz, Start, R1, nbSamples, SeuilAmp);
flagR2Amp = testAmplitude(MoyHorz, Start, R2, nbSamples, SeuilAmp);
flagR3Amp = testAmplitude(MoyHorz, Start, R3, nbSamples, SeuilAmp);

Slope1 = testHeaviside(subNonNaN, S1Cum(subNonNaN));
Slope2 = testHeaviside(subNonNaN, S2Cum(subNonNaN));
Slope3 = testHeaviside(subNonNaN, S3Cum(subNonNaN));
if Display == 4
    figure(8546); hold off; plot(Slope1, 'k-*'); grid on; hold on; plot(Slope2, 'r-*'); plot(Slope3, 'g-*'); 
end

if ~flagR1Noise || ~flagR1Amp
    Slope1 = 0;
end
if ~flagR2Noise || ~flagR2Amp
    Slope2 = 0;
end
if ~flagR3Noise || ~flagR3Amp
    Slope3 = 0;
end
[SlopeMax, TypeSignal] = max([max(Slope1) max(Slope2) max(Slope3)]);
if SlopeMax < 0.01
    TypeSignal = 0;
    flagR1Amp = 0;
    flagR2Amp = 0;
    flagR3Amp = 0;
end


% ------------
% Final search

% {
switch TypeSignal
    case 1
        R0Strip = R1;
        QF = getQF(S1Cum(subNonNaN));
    case 2
        R0Strip = R2;
        QF = getQF(S2Cum(subNonNaN));
    case 3
        R0Strip = R3;
        QF = getQF(S3Cum(subNonNaN));
    otherwise
        R0Strip = NaN;
        QF = 0;
        return
end
y = MoyHorz;
n = length(y);
sub = (R0Strip-10:R0Strip+10) - (Start-1);
% if sub(1) <= 10
%     sub = 1:21;
% end
% if (sub(end)+10) > length(y)
%     sub = (n-20):n;
% end
if sub(1) < 1
    sub = 1:21;
end
if sub(end) > length(y)
    sub = (n-20):n;
end
if sub(1) < 1
    return
end

% Added PP? 30/06/2008
sub(isnan(y(sub))) = [];
 
yf = y(sub)-min(y(sub));
yf = yf .^ 2;
yf = cumsum(yf);

% MODIFICATION / REFERENCE : 20/04/2008
yf = yf - min(yf);

yf = yf / max(yf);

R0Strip = find(yf > 0.1, 1, 'first'); 
R0Strip = sub(R0Strip) + (Start-1);
R0Strip = R0Strip - 1;
% }

function plot_signal(Fig, S1, S2, S3, Start, R1, R2, R3, sub, TypeSignal)
figure(Fig)
hold on;

M = mean(sub);
R = (sub(end) - sub(1)) * 3/4;

y = setRange(S1);
y = M + y * R;
h(1) = PlotUtils.createSScPlot(y, (1:length(y))+(Start-1), 'k'); grid on;
% set(h, 'Color', [0.8 0.8 0.8])

y = setRange(S2);
y = M + y * R;
h(2) = PlotUtils.createSScPlot(y, (1:length(y))+(Start-1), 'r'); grid on;

y = setRange(S3);
y = M + y * R;
h(3) = PlotUtils.createSScPlot(y, (1:length(y))+(Start-1), 'g'); grid on;

if (TypeSignal ~= 0) && ~isnan(TypeSignal)
    set(h(TypeSignal), 'LineWidth', 3)
end

if ~isnan(R1)
    k = R1 - (Start-1);
    k = max(1,k);
    k = min(k, length(S2));
    y = setRange(S1);
    y = M + y * R;
    if TypeSignal == 1
        hold on;
        h = plot(y(k), R1, 'k*');
        set(h, 'MarkerSize', 16)
        h = plot(y(k), R1, 'ko');
        set(h, 'MarkerSize', 16)
    else
        hold on;
        h = plot(y(k), R1, 'kx');
        set(h, 'MarkerSize', 16)
    end
end
if ~isnan(R2)
    k = R2 - (Start-1);
    k = max(1,k);
    k = min(k, length(S2));
    y = setRange(S2);
    y = M + y * R;
    if TypeSignal == 2
        hold on;
        h = plot(y(k), R2, 'r*');
        set(h, 'MarkerSize', 16)
        h = plot(y(k), R2, 'ro');
        set(h, 'MarkerSize', 16)
    else
        hold on;
        h = plot(y(k), R2, 'rx');
        set(h, 'MarkerSize', 16)
    end
end
if ~isnan(R3)
    k = R3 - (Start-1);
    k = max(1,k);
    k = min(k, length(S3));
    y = setRange(S3);
    y = M + y * R;
    if TypeSignal == 3
        hold on;
        h = plot(y(k), R3, 'g*');
        set(h, 'MarkerSize', 16)
        h = plot(y(k), R3, 'go');
        set(h, 'MarkerSize', 16)
    else
        hold on;
        h = plot(y(k), R3, 'gx');
        set(h, 'MarkerSize', 16)
    end
end
hold off;


function y = setRange(x)
y = x - min(x);
y = y / max(y);
y = (y -0.5);

function flag = testAmplitude(MoyHorz, Start, R0, nbSamples, SeuilAmp)

if isnan(R0)
    flag = false;
    return
end

R0 = R0 - (Start-1);

if R0 <= 1
    A = MoyHorz(1);
elseif R0 == length(MoyHorz)
    A = MoyHorz(end);
else 
    A = max(MoyHorz(R0-1:R0+1));
end

if Start ~= 1
    SeuilAmp = interp1([1 nbSamples], [SeuilAmp SeuilAmp/2], R0);
end

if A < SeuilAmp
    flag = false;
else
    flag = true;
end


function Slope = testHeaviside(x,S)
% 49 30 25
Slope = NaN(1,25);
for k=1:25
    L1 = k/100;
    L2 = 1-L1;
    iBeg = find(S >= L1, 1, 'first');
    iEnd = find(S <= L2, 1, 'last');
    i50  = find(S >= 0.5, 1, 'first');
%     if iEnd > iBeg
%         Slope(k) = (1-2*L1) / (x(iEnd)-x(iBeg));
%     end
    if iEnd > i50
        S1 = (0.5 - L1) / (x(iEnd)-x(i50));
    else
        S1 = NaN;
    end
    if i50 > iBeg
        S2 = (0.5 - L1) / (x(i50)-x(iBeg));
    else
        S2 = NaN;
    end
    Slope(k) = max(S1, S2);
end


function QF = getQF(S)
%{
figure(7889);
h(1) = subplot(2,1,1); plot(S); grid on;
%}
S = S - min(S);
S = S / max(S);
%{
h(2) = subplot(2,1,2); plot(S); grid on;
linkaxes(h, 'x')
%}

x1 = find(S > 0.1, 1, 'first');
x2 = find(S > 0.5, 1, 'first');
if isempty(x1) || (x2 == x1)
    QF = 0;
else
    QF = log10(.25 * length(S) / (x2 - x1));
end
%{
title(['QF = ' num2str(QF)])
QF;
%}
