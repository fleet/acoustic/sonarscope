% Génération automatique des noms de fichiers SonarScope de l'imagerie Simrad
%
% Syntax
%   [nomFicBeamRawData, flagFilesExist] = ResonBeamRawDataNames(nomFicAll)
%
% Input Arguments
%   nomFicAll : Nom du fichier .s7m
%
% Output Arguments
%   nomFicBeamRawData : Structure contenant les noms des différents fichiers SonarScope.
%   flagFilesExist : 1=Files exist, 0=files no not exist
%
% Examples
%   nomFicS7k = getNomFicDatabase('xxxxxxxxxxxxxxxx.s7k');
%   nomFicBeamRawData = ResonBeamRawDataNames(nomFicS7k)
%
% See also read_SnippetData Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function [nomFicBeamRawData, flagFilesExist] = ResonBeamRawDataNames(nomFicS7k, identSondeur, iPing)

global useCacheNetcdf %#ok<GVMIS>

[nomDir, nomFicSeul] = fileparts(nomFicS7k);

if useCacheNetcdf == 1 % 1=Mat, 2 et 3=Netcdf
    nomFicBeamRawData.Mat = fullfile(nomDir, 'SonarScope', [num2str(identSondeur) '_' nomFicSeul '_BeamRawDataIFRData.mat']);
else
    nomFicBeamRawData.Mat = fullfile(nomDir, 'SonarScope', [nomFicSeul '.nc']);
end

if isempty(iPing)
    flagFilesExist = exist(nomFicBeamRawData.Mat, 'file');
    return
end

if useCacheNetcdf == 1 % 1=Mat, 2 et 3=Netcdf
    nomFicBeamRawData.AP  = fullfile(nomDir, 'SonarScope', [num2str(identSondeur) '_' nomFicSeul], [nomFicSeul '_BeamRawDataAP_' num2str(iPing) '.mat']);
    flagFilesExist = exist(nomFicBeamRawData.Mat, 'file') && exist(nomFicBeamRawData.AP, 'file');
else
    nomFicBeamRawData.AP = nomFicBeamRawData.Mat;
    flagFilesExist = exist(nomFicBeamRawData.Mat, 'file');
end
