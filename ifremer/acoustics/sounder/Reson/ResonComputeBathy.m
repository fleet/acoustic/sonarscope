function ResonComputeBathy(listeFicS7K, nbPlotPerImage)

if isempty(listeFicS7K)
    return
end

choixLayer = [6 4 20 21];

Bits = 'AskIt';

Carto = [];
N = length(listeFicS7K);
str1 = 'Calcul des mosa�ques individuelles.';
str2 = 'Computing individual mosaics.';
hw = create_waitbar(Lang(str1,str2), 'N', N);
for iFic=1:N
    nomFic = listeFicS7K{iFic};
    
    my_waitbar(iFic, N, hw)
    fprintf('%s\n%d / %d : %s\n', repmat('-',1,100), iFic, N, nomFic);
    
    if ~exist(listeFicS7K{iFic}, 'file')
        str1 = sprintf('Le fichier "%s" n''existe pas.', listeFicS7K{iFic});
        str2 = sprintf('"%s" does not exist.', listeFicS7K{iFic});
        my_warndlg(Lang(str1,str2), 0, 'Tag', 'FichierAllNExistePAs');
        continue
    end
    
    s7k = cl_reson_s7k('nomFic', nomFic, 'KeepNcID', true);
    if isempty(s7k)
        continue
    end
    
    %% Lecture des layers BeamPointingAngles, Range, RollRx et PitchRx
    
    [c, Carto, Bits] = view_depth(s7k, 'ListeLayers', choixLayer, 'Carto', Carto, ...
        'Bits', Bits, 'setMaskToAllLayers', 1);
    if isempty(c)
        continue
    end
    
    %% Calcul de la bathy
    
    [flag, c] = sonar_range2depth_EM(c(1), c(2), c(3), c(4), c([]), 'nbPlotPerImage', nbPlotPerImage);
    if ~flag
        continue
    end
    
    %% Sauvegarde des r�sultats
    
    identSondeur = selectionSondeur(s7k);
    [nomDir, nomFic] = fileparts(nomFic);
    nomFicMat = fullfile(nomDir, 'SonarScope', [num2str(identSondeur) '_' nomFic '_depth.mat']);
    if ~exist(nomFicMat, 'file')
        continue
    end
    Data = loadmat(nomFicMat, 'nomVar', 'Data');
        
    Data.Depth            = get_Image(c(5));
    Data.AcrossDist       = get_Image(c(4));
    Data.AlongDist        = get_Image(c(3));
    Data.BeamAzimuthAngle = get_Image(c(1));

    flag = savemat(nomFicMat, 'Data', Data);
    if ~flag
        messageErreurFichier(nomFicMat, 'WriteFailure')
    end
end
my_close(hw, 'MsgEnd')
