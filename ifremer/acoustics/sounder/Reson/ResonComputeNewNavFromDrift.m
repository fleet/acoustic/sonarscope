function ResonComputeNewNavFromDrift(listeFicS7K, varargin)

if isempty(listeFicS7K)
    return
end

choixLayer = [12 2 3];

Bits = 'AskIt';

nomFicTxt = my_tempname('.txt');
fid = fopen(nomFicTxt, 'w+');
fprintf(fid, 'Ping,Beam,Time,Lat,Lon,Filename\n');

k = 0;
Carto = [];
N = length(listeFicS7K);
str1 = 'TODO';
str2 = 'Fix Rascas Navigation';
hw = create_waitbar(Lang(str1,str2), 'N', N);
for iFic=1:N
    nomFic = listeFicS7K{iFic};
    
    my_waitbar(iFic, N, hw)
    fprintf('%s\n%d / %d : %s\n', repmat('-',1,100), iFic, N, nomFic);
    
    if ~exist(listeFicS7K{iFic}, 'file')
        str1 = sprintf('Le fichier "%s" n''existe pas.', listeFicS7K{iFic});
        str2 = sprintf('"%s" does not exist.', listeFicS7K{iFic});
        my_warndlg(Lang(str1,str2), 0, 'Tag', 'FichierAllNExistePAs');
        continue
    end
    
    s7k = cl_reson_s7k('nomFic', nomFic, 'KeepNcID', true);
    if isempty(s7k)
        continue
    end
    
    %% Lecture des layers BeamPointingAngles, Range, RollRx et PitchRx
    
    [Layers_PingBeam, Carto, Bits] = view_depth(s7k, 'ListeLayers', choixLayer, 'Carto', Carto, ...
        'Bits', Bits, 'setMaskToAllLayers', 1);
    if isempty(Layers_PingBeam)
        continue
    end
    
    %% Calcul des coordonnées géographiques
    
    [flag, LatLong_PingBeam] = sonarCalculCoordGeo(Layers_PingBeam, 1);
    if ~flag
        continue
    end 
    
    %% Affichage de l'image en mode modal
    
    Layers_PingBeam = [Layers_PingBeam LatLong_PingBeam]; %#ok<AGROW>
    
    c = cli_image;
    c = add_Image(c, Layers_PingBeam);
    c = SonarScope(c, 'visuProfilX', 1, 'visuProfilY', 1);
    
    %% Résupération des coordonnées de Rascas
    
    Points = get(c, 'Point');
    iPing = Points(1,2);
    iCol  = Points(1,1);
    LatPoint = get_val_ij(LatLong_PingBeam(1), iPing, iCol);
    LonPoint = get_val_ij(LatLong_PingBeam(2), iPing, iCol);
    
    str = sprintf('You have identified the Rs=ascas at the coordinates\n%s\n%s\nDo you validate this point ?', lat2str(LatPoint), lon2str(LonPoint));
    [rep, flag] = my_questdlg(str);
    if ~flag
        break
    end
    if rep == 1
        k = k + 1;
        T = get(Layers_PingBeam(1), 'Time');
        T = T(iPing);
        H = get(Layers_PingBeam(1), 'Time'); %#ok<NASGU>
        fprintf(fid, '%d,%d,%s,%s,%s,%s\n', iPing, iCol, t2str(T), lat2str(LatPoint), lon2str(LonPoint), nomFic);
        
        listFicName{k} = nomFic; %#ok<AGROW,NASGU>
        listTime       = T.timeMat; %#ok<NASGU>
        listPing(k)    = iPing; %#ok<AGROW,NASGU>
        listBeam(k)    = iCol; %#ok<AGROW,NASGU>
        listLat(k)     = LatPoint;  %#ok<AGROW>
        listLon(k)     = LonPoint; %#ok<AGROW>
    end
end
my_close(hw, 'MsgEnd')
my_close(fid)

FigUtils.createSScFigure; 
PlotUtils.createSScPlot(listLon, listLat, '*'); grid on; axisGeo;
