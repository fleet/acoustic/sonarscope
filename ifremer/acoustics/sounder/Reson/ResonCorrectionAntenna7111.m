function Correction = ResonCorrectionAntenna7111(BeamAngles, SampleRate)
   
TypeCorrection = 2;
switch TypeCorrection
    case 1 % Correction XL
        r = 2.5; % Correspond = 0.312m
        rReson =  2.5; % Nombre presque entier d'échantillons pour r
        Correction = -2*rReson + r * (1 + cosd(BeamAngles));
    case 2 % Correction Reson
        r = 0.312; % m
        Correction = (1 + cosd(BeamAngles)) * (r * SampleRate / 1500);
    case 3 % Test
        Correction = 0;
    case 4 % Test
        r = 0.312; % m
        Correction = (1 + cosd(BeamAngles)) * (r * 2 * SampleRate / 1500);
end

if nargout == 0
    FigUtils.createSScFigure; PlotUtils.createSScPlot(BeamAngles, Correction);
    grid on; title('Correction antenne 711');
    xlabel('Angles (deg)'); ylabel('Correction (Sample)')
end