function [AmPingRange, AmpPingQF, AmpPingSamplesNb, AmpPingQF2] = ResonDetectionAmplitudeImage(Amp, R0, BeamAngles, ...
    SystemSerialNumber, SampleRate, TxPulseWidth, DisplayLevel, BeamsToDisplay, iPing, varargin)

global TestPlatformCmatlab %#ok<GVMIS>

[varargin, IdentAlgo] = getPropertyValue(varargin, 'IdentAlgo', 1); %#ok<ASGLU>

nbBeams   = size(Amp,2);
nbSamples = size(Amp,1);

AmPingRange_M    = NaN(1,nbBeams);
AmPingRange_C    = NaN(1,nbBeams);
AmpPingQF_M      = NaN(1,nbBeams);
AmpPingQF_C      = NaN(1,nbBeams);

AmpPingSamplesNb_M = NaN(1,nbBeams);
AmpPingQF2_M       = NaN(1,nbBeams);

for iBeam=1:nbBeams
    subSample = find(~isnan(Amp(:,iBeam)));
    amp = Amp(subSample,iBeam);
    
    
    if any(TestPlatformCmatlab.detectionAmplitudePing == [1 3 4])
        [AmPingRange_M(iBeam), AmpPingQF_M(iBeam), AmpPingSamplesNb_M(iBeam), AmpPingQF2_M(iBeam)] = ...
            detectionAmplitudePing(subSample, amp, R0, SampleRate, TxPulseWidth, ...
            iPing, iBeam, BeamAngles(iBeam), DisplayLevel, BeamsToDisplay, nbSamples, SystemSerialNumber, ...
            'IdentAlgo', IdentAlgo);
    end
    
    if any(TestPlatformCmatlab.detectionAmplitudePing == [2 3 4])
        if isempty(subSample)
            AmPingRange_C(iBeam) = NaN;
            AmpPingQF_C(iBeam)   = NaN;
        else
            [AmPingRange_C(iBeam), AmpPingQF_C(iBeam)] = detectionAmplitudePing_reson(...
                double(iBeam), double(subSample), single(amp), double(SystemSerialNumber), ...
                double(SampleRate), double(TxPulseWidth), double(nbSamples), double(IdentAlgo), double(R0));
        end
    end
end

if any(TestPlatformCmatlab.detectionAmplitudePing == [3 4])
    BDA_compare_Matlab_C('detectionAmplitudePing', 'AmPingRange', AmPingRange_M, AmPingRange_C, iPing, iBeam, TestPlatformCmatlab.Seuil_detectionAmplitudePing_AmPingRange);
    BDA_compare_Matlab_C('detectionAmplitudePing', 'AmpPingQF',   AmpPingQF_M,   AmpPingQF_C,   iPing, iBeam, TestPlatformCmatlab.Seuil_detectionAmplitudePing_AmpPingQF);
end

switch TestPlatformCmatlab.detectionAmplitudePing
    case {1; 3}
        AmPingRange      = AmPingRange_M;
        AmpPingQF        = AmpPingQF_M;
        AmpPingSamplesNb = AmpPingSamplesNb_M;
        AmpPingQF2       = AmpPingQF2_M;
    case {2; 4}
        AmPingRange      = AmPingRange_C;
        AmpPingQF        = AmpPingQF_C;
        
        % TODO : sortir les variables de la fonction C
        AmpPingSamplesNb = AmpPingSamplesNb_M;
        AmpPingQF2       = AmpPingQF2_M;
end

if SystemSerialNumber == 7111
    AmPingRange = AmPingRange + ResonCorrectionAntenna7111(BeamAngles, SampleRate);
end
