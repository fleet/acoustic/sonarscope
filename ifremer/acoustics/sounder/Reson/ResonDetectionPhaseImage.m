function [PhasePingRange, PhasePingNbSamples, PhasePingPente, PhasePingEqm, PhasePingQF] = ...
    ResonDetectionPhaseImage(Amp, Ph, BeamAngles, R0, SoundVelocity, SampleRate, Frequency, ...
    SystemSerialNumber, PingCounter, DisplayLevel, BeamsToDisplay)

nbBeams = size(Ph,2);
PhasePingRange     = NaN(1,nbBeams);
PhasePingEqm       = NaN(1,nbBeams);
PhasePingPente     = NaN(1,nbBeams);
PhasePingNbSamples = NaN(1,nbBeams);
PhasePingQF        = NaN(1,nbBeams);
nbSamples=size(Ph,1);
% TxPulseWidth  = Amplitude.Sonar.SampleBeamData.TxPulseWidth;

algo = 2;
switch algo
    case 1 % George
        phs = Ph / (180 / pi / 10430);
        
        switch SystemSerialNumber
            case 7111
            case 7150
                if Frequency > 18000      % 24kHz and m-pings
                    frs = 2; % 24 kHz
                else
                    frs = 1; % 12 kHZ
                end
                
                H = R0 * 1500/2 / SampleRate;
                %                 Hmin = Amplitude.Sonar.SampleBeamData.RangeSelection / 4;
                %                 Hmax = Amplitude.Sonar.SampleBeamData.RangeSelection *( 3/4);
                Hmin = H * 0.8;
                Hmax = H * 1.2;
                [depthBD, tet, rangeBD, ASS, PHS, RR, SNR, S1, S2] = bottomrelief7150(Amp, phs, ...
                    SoundVelocity, SampleRate, frs, Hmin, Hmax, BeamAngles* (pi/180)); %#ok<ASGLU> %, 7, TxPulseWidth);
                
                figure;
                h(1) = subplot(3,1,1); plot(depthBD, '*'); grid on; title('depthBD')
                h(2) = subplot(3,1,2); plot(rangeBD, '*'); grid on; title('deprangeBDthBD')
                h(3) = subplot(3,1,3); plot(SNR, '*'); grid on; title('SNR')
                linkaxes(h, 'x')
                PhasePingRange = depthBD;
                
                %     function [H, tet, PhaseBeamRange, ASS, PHS, RR, SNR, S1, S2] = bottomrelief7150(amp, phs, c, fs, fg, Hmin, Hmax, tet)
                
        end
        
    case 2
        iPing = PingCounter;
        
        % Ca serait peut-�tre mieux de ramener ce calcul dans la d�finition
        % de SampleBeamData
        switch SystemSerialNumber
            case 7111
                DeltaTeta = 1.8;
            case 7150
                if Frequency < 18000
                    DeltaTeta = 1;
                else
                    DeltaTeta =0.5;
                end
            case 7125
                % my_warndlg('DeltaTeta must be fixed here for 7125', 0, 'Tag', 'DeltaTetaMustBeFixed');
                if Frequency > 300000
                    DeltaTeta = 0.5;
                else
                    DeltaTeta=1;
                end
            otherwise
                DeltaTeta = 1.5; % EML3003D
        end
        
        
        for iBeam=1:nbBeams
            subSample = find(~isnan(Ph(:,iBeam)) & ~isnan(Amp(:,iBeam)));
            ph = Ph(subSample,iBeam);
            amp = Amp(subSample,iBeam);
            
            if SystemSerialNumber == 7111
                [PhaseBeamRange, PhaseBeamQF, PhaseBeamPente, PhaseBeamNbSamples, PhaseBeamEqm] = ...
                    ResonDetectionPhasePing_7111(nbSamples,subSample, ph, amp.^2, iPing, iBeam, BeamAngles(iBeam), ...
                    DeltaTeta, R0, nbBeams, DisplayLevel);
            else
                [PhaseBeamRange, PhaseBeamQF, PhaseBeamPente, PhaseBeamNbSamples, PhaseBeamEqm] = ...
                    ResonDetectionPhasePing_7150(subSample, ph, amp, iPing, iBeam, BeamAngles(iBeam), ...
                    DeltaTeta, R0, nbBeams, DisplayLevel);
            end
            
            if isnan(PhaseBeamRange) || (PhaseBeamRange > subSample(end)) || (PhaseBeamRange < subSample(1))
                PhasePingRange(iBeam)     = NaN;
                PhasePingEqm(iBeam)       = NaN;
                PhasePingPente(iBeam)     = NaN;
                PhasePingNbSamples(iBeam) = NaN;
                PhasePingQF(iBeam)        = NaN;
            else
                PhasePingRange(iBeam)     = PhaseBeamRange;
                
                %                 X                         = PhaseBeamEqm(~isnan(PhaseBeamEqm));
                %                 PhasePingEqm(iBeam)       = X(end);
                PhasePingEqm(iBeam)       = PhaseBeamEqm;
                
                %                 PhasePingPente(iBeam)     = PhaseBeamPente(end);
                PhasePingPente(iBeam)     = PhaseBeamPente;
                
                PhasePingNbSamples(iBeam) = PhaseBeamNbSamples;
                PhasePingQF(iBeam)        = PhaseBeamQF;
            end
        end
        if SystemSerialNumber == 7111
            PhasePingRange = PhasePingRange + ResonCorrectionAntenna7111(BeamAngles, SampleRate);
        end
end