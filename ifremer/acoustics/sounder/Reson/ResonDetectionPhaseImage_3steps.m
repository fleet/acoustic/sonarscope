function [PhasePingRange, PhasePingNbSamples, PhasePingPente, PhasePingEqm, PhasePingQF] = ...
    ResonDetectionPhaseImage_3steps(Amp, Ph, BeamAngles, R0, SampleRate, Frequency, ...
    nbSamplesMoyennes, SystemSerialNumber, PingCounter, TxPulseWidth, DisplayLevel, BeamsToDisplay)

global TestPlatformCmatlab %#ok<GVMIS>

nbBeams = size(Ph,2);

% TxPulseWidth  = Amplitude.Sonar.SampleBeamData.TxPulseWidth;

iPing = PingCounter;

% Ca serait peut-�tre mieux de ramener ce calcul dans la d�finition
% de SampleBeamData
switch SystemSerialNumber
    case 7111
        DeltaTeta = 1.8;
    case 7150
        if Frequency < 18000
            DeltaTeta = 1;
        else
            DeltaTeta =0.5;
        end
    case 7125
        % my_warndlg('DeltaTeta must be fixed here for 7125', 0, 'Tag', 'DeltaTetaMustBeFixed');
        if Frequency > 300000
            DeltaTeta = 0.5;
        else
            DeltaTeta=1;
        end
    otherwise
        DeltaTeta = 1.5; % EML3003D
end

if any(TestPlatformCmatlab.ResonDetectionPhasePing_3steps == [1 3 4])
    PhasePingRange_M     = NaN(1,nbBeams);
    PhasePingEqm_M       = NaN(1,nbBeams);
    PhasePingPente_M     = NaN(1,nbBeams);
    PhasePingNbSamples_M = NaN(1,nbBeams);
    PhasePingQF_M        = NaN(1,nbBeams);
end

if any(TestPlatformCmatlab.ResonDetectionPhasePing_3steps == [2 3 4])
    PhasePingRange_C     = NaN(1,nbBeams);
    PhasePingEqm_C       = NaN(1,nbBeams);
    PhasePingPente_C     = NaN(1,nbBeams);
    PhasePingNbSamples_C = NaN(1,nbBeams);
    PhasePingQF_C        = NaN(1,nbBeams);
end

for iBeam=1:nbBeams
    subSample = find(~isnan(Ph(:,iBeam)) & ~isnan(Amp(:,iBeam)));
    if isempty(subSample)
        continue
    end
    
    if length(subSample) < 11
        if any(TestPlatformCmatlab.ResonDetectionPhasePing_3steps == [1 3 4])
            PhasePingRange_M(iBeam)     = NaN;
            PhasePingEqm_M(iBeam)       = NaN;
            PhasePingPente_M(iBeam)     = NaN;
            PhasePingNbSamples_M(iBeam) = NaN;
            PhasePingQF_M(iBeam)        = NaN;
        end
        if any(TestPlatformCmatlab.ResonDetectionPhasePing_3steps == [2 3 4])
            PhasePingRange_C(iBeam)     = NaN;
            PhasePingEqm_C(iBeam)       = NaN;
            PhasePingPente_C(iBeam)     = NaN;
            PhasePingNbSamples_C(iBeam) = NaN;
            PhasePingQF_C(iBeam)        = NaN;
        end
    end
    
    ph = Ph(subSample,iBeam);
    amp = Amp(subSample,iBeam);
    
    if any(TestPlatformCmatlab.ResonDetectionPhasePing_3steps == [1 3 4])
        [PhaseBeamRange_M, PhaseBeamQF_M, PhaseBeamPente_M, PhaseBeamNbSamples_M, PhaseBeamEqm_M] = ...
            ResonDetectionPhasePing_3steps(subSample, ph, amp, iPing, iBeam, BeamAngles(iBeam), ...
            BeamAngles(end), DeltaTeta, R0, nbBeams, nbSamplesMoyennes, SampleRate, TxPulseWidth, ...
            DisplayLevel, BeamsToDisplay);
    end
    
    if any(TestPlatformCmatlab.ResonDetectionPhasePing_3steps == [2 3 4])
        [PhaseBeamRange_C, PhaseBeamQF_C, PhaseBeamPente_C, PhaseBeamNbSamples_C, PhaseBeamEqm_C] = ...
            ResonDetectionPhasePing_3steps_reson(...
            double(subSample), ...
            single(ph), single(amp), single(iPing), ...
            double(iBeam), double(BeamAngles(iBeam)), double(BeamAngles(end)), double(DeltaTeta), double(R0), ...
            double(nbBeams), double(nbSamplesMoyennes), double(SampleRate), double(TxPulseWidth), double(BeamAngles));
    end
    
    if any(TestPlatformCmatlab.ResonDetectionPhasePing_3steps == [1 3 4])
            if isnan(PhaseBeamRange_M) || (PhaseBeamRange_M > subSample(end)) || (PhaseBeamRange_M < subSample(1))
                PhasePingRange_M(iBeam)     = NaN;
                PhasePingEqm_M(iBeam)       = NaN;
                PhasePingPente_M(iBeam)     = NaN;
                PhasePingNbSamples_M(iBeam) = NaN;
                PhasePingQF_M(iBeam)        = NaN;
            else
                PhasePingRange_M(iBeam)     = PhaseBeamRange_M;
                PhasePingEqm_M(iBeam)       = PhaseBeamEqm_M;
                PhasePingPente_M(iBeam)     = PhaseBeamPente_M;
                PhasePingNbSamples_M(iBeam) = PhaseBeamNbSamples_M;
                PhasePingQF_M(iBeam)        = PhaseBeamQF_M;
            end
    end
    if any(TestPlatformCmatlab.ResonDetectionPhasePing_3steps == [2 3 4])
            if isnan(PhaseBeamRange_C) || (PhaseBeamRange_C > subSample(end)) || (PhaseBeamRange_C < subSample(1))
                PhasePingRange_C(iBeam)     = NaN;
                PhasePingEqm_C(iBeam)       = NaN;
                PhasePingPente_C(iBeam)     = NaN;
                PhasePingNbSamples_C(iBeam) = NaN;
                PhasePingQF_C(iBeam)        = NaN;
            else
                PhasePingRange_C(iBeam)     = PhaseBeamRange_C;
                PhasePingEqm_C(iBeam)       = PhaseBeamEqm_C;
                PhasePingPente_C(iBeam)     = PhaseBeamPente_C;
                PhasePingNbSamples_C(iBeam) = PhaseBeamNbSamples_C;
                PhasePingQF_C(iBeam)        = PhaseBeamQF_C;
            end
    end
end

if any(TestPlatformCmatlab.ResonDetectionPhasePing_3steps == [3 4])
    BDA_compare_Matlab_C('ResonDetectionPhasePing_3steps', 'PhasePingRange',     PhasePingRange_M,     PhasePingRange_C,     iPing, iBeam, TestPlatformCmatlab.Seuil_ResonDetectionPhasePing_3steps_PhasePingRange);
    BDA_compare_Matlab_C('ResonDetectionPhasePing_3steps', 'PhasePingEqm (deg)', PhasePingEqm_M,       PhasePingEqm_C,       iPing, iBeam, TestPlatformCmatlab.Seuil_ResonDetectionPhasePing_3steps_PhasePingEqm);
    BDA_compare_Matlab_C('ResonDetectionPhasePing_3steps', 'PhasePingPente',     PhasePingPente_M,     PhasePingPente_C,     iPing, iBeam, TestPlatformCmatlab.Seuil_ResonDetectionPhasePing_3steps_PhasePingPente);
    BDA_compare_Matlab_C('ResonDetectionPhasePing_3steps', 'PhasePingNbSamples', PhasePingNbSamples_M, PhasePingNbSamples_C, iPing, iBeam, TestPlatformCmatlab.Seuil_ResonDetectionPhasePing_3steps_PhasePingNbSamples);
    BDA_compare_Matlab_C('ResonDetectionPhasePing_3steps', 'PhasePingQF',        PhasePingQF_M,        PhasePingQF_C,        iPing, iBeam, TestPlatformCmatlab.Seuil_ResonDetectionPhasePing_3steps_PhasePingQF);
end

switch TestPlatformCmatlab.ResonDetectionPhasePing_3steps
    case {1; 3}
        PhasePingRange      = PhasePingRange_M;
        PhasePingNbSamples  = PhasePingNbSamples_M;
        PhasePingPente      = PhasePingPente_M;
        PhasePingEqm        = PhasePingEqm_M;
        PhasePingQF         = PhasePingQF_M;
    case {2; 4}
        PhasePingRange      = PhasePingRange_C;
        PhasePingNbSamples  = PhasePingNbSamples_C;
        PhasePingPente      = PhasePingPente_C;
        PhasePingEqm        = PhasePingEqm_C;
        PhasePingQF         = PhasePingQF_C;
end

if SystemSerialNumber == 7111
    PhasePingRange = PhasePingRange + ResonCorrectionAntenna7111(BeamAngles, SampleRate);
end
