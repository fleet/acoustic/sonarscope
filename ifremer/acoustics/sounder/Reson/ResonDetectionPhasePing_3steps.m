function [PhaseBeamRange, PhaseBeamQF, PhaseBeamPente, PhaseBeamNbSamples, PhaseBeamEqm] = ...
    ResonDetectionPhasePing_3steps(x, phase, amp, iPing, iBeam, BeamAngle, MaxBeamAngle, ...
    DeltaTeta, R0, nbBeams, nbSamplesMoyennes, SampleRate, TxPulseWidth, DisplayLevel, BeamsToDisplay)

global TestPlatformCmatlab %#ok<GVMIS>

% DisplayLevel = 3;

PhaseBeamRange     = NaN;
PhaseBeamEqm       = NaN;
PhaseBeamPente     = NaN;
PhaseBeamNbSamples = NaN;
PhaseBeamQF        = NaN;

%% Copy of inputs in work variables

x1     = x;
phase1 = phase;
amp1   = amp;
amp2   = amp .^ 2;

%% Determination of the number of samples corresponding to a beam spreading

% Sample number inside the whole beam
Beta = 1.2;
NechBeam = R0 * DeltaTeta * (pi/180) * sind(abs(BeamAngle)) / (2 * Beta * cosd(BeamAngle)^2);

% Sample number inside the resolution cell (sounding horizontal spacing)
NechRes = ceil(2 * abs(2 * R0 .* sind(BeamAngle) * (tand(MaxBeamAngle) / nbBeams)));

NechRes = min(NechBeam, NechRes);
NechRes = max(NechRes, 6);


% % TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST
% % Rustine pour corriger le mauvais calcul dans le cas d'une pente
% apparu sur fichier 20080221_223341
NechRes = min(NechRes, 50);
% % TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST


% NechRampMin = 13;
NechRampMin = 7; % Modif Brest 10/07/2008

%% If number of samples is less than 11 we do not try to process the sounding

n = length(x);
if Reson_testLengthPhase1(x, NechRampMin)
    return
end
NechSondeMin = ceil(n / 20);
NechSondeMin = max(NechSondeMin, NechRampMin);
NechSondeMin = max(NechSondeMin, NechRes);

%% D�termination of the sounding

N = 3;
k = 0;
flag = 1;
while flag
    k = k + 1;
    
    if any(TestPlatformCmatlab.ResonDetectionPhaseBeam == [1 3 4])
        [flag_M, R1_M, QF_M, PhaseBeamPente1_M, PhaseBeamEqm1_M, PhaseBeamNbSamples1_M, x1_M, phase1_M, amp1_M, amp2_M] = ...
            ResonDetectionPhaseBeam(x1, phase1, amp1, amp2, R0, N, k, iPing, iBeam, BeamAngle, NechSondeMin, ...
            nbSamplesMoyennes, SampleRate, TxPulseWidth, DisplayLevel, BeamsToDisplay);
    end
    
    if any(TestPlatformCmatlab.ResonDetectionPhaseBeam == [2 3 4])
        % Cast en single obligatoire
        [flag_C, R1_C, QF_C, PhaseBeamPente1_C, PhaseBeamEqm1_C, PhaseBeamNbSamples1_C, x1_C, phase1_C, amp1_C, amp2_C] = ...
            ResonDetectionPhaseBeam_reson(...
            double(x1), ...
            single(phase1), single(amp1), single(amp2), ...
            double(R0), double(N), double(k), ...
            single(iPing), ...
            double(iBeam), double(BeamAngle), double(NechSondeMin),  double(nbSamplesMoyennes), double(SampleRate), ...
            double(TxPulseWidth), double(DisplayLevel), double(BeamsToDisplay));
    end
    
    if any(TestPlatformCmatlab.ResonDetectionPhaseBeam == [3 4])
        if flag_M || flag_C
            BDA_createMes('ResonDetectionPhaseBeam', 'flag', flag_M, flag_C, iPing, iBeam, 0);
            
            seuil = 0.01;
            BDA_createMes('ResonDetectionPhaseBeam', 'R1', R1_M, R1_C, iPing, iBeam, seuil);

            seuil = 0.01;
            BDA_createMes('ResonDetectionPhaseBeam', 'QF', QF_M, QF_C, iPing, iBeam, seuil);

            seuil = 0.001;
            BDA_createMes('ResonDetectionPhaseBeam', 'PhaseBeamPente1', PhaseBeamPente1_M, PhaseBeamPente1_C, iPing, iBeam, seuil);

            seuil = 0.01;
            BDA_createMes('ResonDetectionPhaseBeam', 'PhaseBeamEqm1', PhaseBeamEqm1_M, PhaseBeamEqm1_C, iPing, iBeam, seuil);

            seuil = 0.01;
            BDA_createMes('ResonDetectionPhaseBeam', 'PhaseBeamNbSamples1', PhaseBeamNbSamples1_M, PhaseBeamNbSamples1_C, iPing, iBeam, seuil);
            
            BDA_createMes('ResonDetectionPhaseBeam', 'length(x1)', length(x1_M), length(x1_C), iPing, iBeam, 0);
           
            [~, ia, ib] = intersect(x1_M, x1_C);
            BDA_compare_Matlab_C('ResonDetectionPhaseBeam', 'x1',     x1_M(ia),     x1_C(ib),     iPing, iBeam, TestPlatformCmatlab.Seuil_ResonDetectionPhaseBeam_x1);
            BDA_compare_Matlab_C('ResonDetectionPhaseBeam', 'phase1', phase1_M(ia), phase1_C(ib), iPing, iBeam, TestPlatformCmatlab.Seuil_ResonDetectionPhaseBeam_phase1);
            BDA_compare_Matlab_C('ResonDetectionPhaseBeam', 'amp1',   amp1_M(ia),   amp1_C(ib),   iPing, iBeam, TestPlatformCmatlab.Seuil_ResonDetectionPhaseBeam_amp1);
        end
    end
    
    switch TestPlatformCmatlab.ResonDetectionPhaseBeam
        case {1; 3}
            R1                  = R1_M;
            QF                  = QF_M;
            PhaseBeamPente1     = PhaseBeamPente1_M;
            PhaseBeamEqm1       = PhaseBeamEqm1_M;
            PhaseBeamNbSamples1 = PhaseBeamNbSamples1_M;
            x1                  = x1_M;
            phase1              = phase1_M;
            amp1                = amp1_M;
            amp2                = amp2_M;
            flag                = flag_M;
        case {2; 4}
            R1                  = R1_C;
            QF                  = QF_C;
            PhaseBeamPente1     = PhaseBeamPente1_C;
            PhaseBeamEqm1       = PhaseBeamEqm1_C;
            PhaseBeamNbSamples1 = PhaseBeamNbSamples1_C;
            x1                  = x1_C;
            phase1              = phase1_C;
            amp1                = amp1_C;
            amp2                = amp2_C;
            flag                = flag_C;
    end
    
    if flag
        PhaseBeamQF        = QF;
        PhaseBeamRange     = R1;
        PhaseBeamPente     = PhaseBeamPente1;
        PhaseBeamEqm       = PhaseBeamEqm1;
        PhaseBeamNbSamples = PhaseBeamNbSamples1;
    else
        if k == 1
            % if QF > 2.8
            if QF > 3
                PhaseBeamQF        = QF;
                PhaseBeamRange     = R1;
                PhaseBeamPente     = PhaseBeamPente1;
                PhaseBeamEqm       = PhaseBeamEqm1;
                PhaseBeamNbSamples = PhaseBeamNbSamples1;
            end
            return
        end
        
        if (DisplayLevel == 3) && any(iBeam == BeamsToDisplay)
            figure(23456);
            for k2=k+1:N
                h = subplot(N, 1, k2);
                delete(h)
                drawnow
            end
        end
        
        break
    end
    if k == 3
        break
    end
end


%% Polynome d'ordre 1

function [flag, R, QF, Pente, Eqm, PhaseBeamNbSamples, x, phase, amp, amp2] = ResonDetectionPhaseBeam(x, ...
    phase, amp, amp2, R0, N, k, iPing, iBeam, BeamAngle, NechRes, nbSamplesMoyennes, SampleRate, TxPulseWidth, ...
    DisplayLevel, BeamsToDisplay)

R     = NaN;
QF    = NaN;
Pente = NaN;
Eqm   = NaN;
PhaseBeamNbSamples  = NaN;

%% Artifice pour �viter d'�tre pi�g� par le double �cho

M = x(floor(length(x)/2));
if (M > (R0*1.9)) && (M < (R0*2.1))
    amp(:) = 1;
end

% % ----------------------------------------------------
% % Test EM3002
%
% if M < (R0*1.2)
%     amp(:) = 1;
% end

x     = x(:);
phase = phase(:);
amp   = amp(:);
amp2  = amp2(:);

subNonNaN = find(~isnan(phase) & ~isnan(amp));
% Question Yvegeniy 23/06/2008
% if Reson_testLengthPhase(x, NechRes)
if Reson_testLengthPhase(x(subNonNaN), NechRes)
    flag = 0;
    return
end

x     = x(subNonNaN);
phase = phase(subNonNaN);
amp   = amp(subNonNaN);
amp2  = amp2(subNonNaN);

switch k
    case 1
        [a, b, R, Eqm] = regressionLineairePonderee(x, phase, amp2);
    case 2
        [a, b, R, Eqm] = regressionLineairePonderee(x, phase, amp);
    case 3
        [a, b, R, Eqm] = regressionLineaire(x, phase);
end
if isnan(a)
    flag = 0;
    return
end
Pente = abs(a);
% QF    = Pente * sqrt(length(x)/R0) ./ Eqm;
% QF    = QF * 300;

% Pente in deg/sample
% R in samples
% length(x) in samples
% Eqm in deg

% QF = Pente * R * sqrt(length(x))/ Eqm;
% QF = Pente * R * sqrt(length(x)/nbSamplesMoyennes)/ Eqm;
dTPhi   = (Eqm / sqrt(length(x)/nbSamplesMoyennes)) / Pente;
dTPulse = (TxPulseWidth * SampleRate) / sqrt(12);
dT = sqrt(dTPhi*dTPhi + dTPulse*dTPulse);
QF = R / dT;
if QF > 0
    QF = log10(QF);
else
    QF = 0;
end

phase_poly  = a*x + b;
Diff        = phase - phase_poly;
Eqm         = std(Diff);
PhaseBeamNbSamples = length(x);
flag = 1;

%{
figure(6557);
h(1) = subplot(2,1,1); plot(x, phase, '*'); hold on; plot(x, a*x+b, 'k'); grid on; title('Avec ponder = Amp'); hold off
h(2) = subplot(2,1,2); plot(x, amp, '*'); grid on;
linkaxes(h, 'x')
%}


% figure; plot(x, phase, '*'); hold on; plot(x, phase_poly, 'k'); grid on; title('Avec ponder = Amp^2')
% figure; plot(x, phase, '+k'); grid on;

% DisplayLevel=0;
if (DisplayLevel == 3) && any(iBeam == BeamsToDisplay)
    figure(23456);
    subplot(N, 2, 2*k-1);
    PlotUtils.createSScPlot(x, phase, '+k'); grid on;
    hold on; plot(x, phase_poly, 'b');
    plot(R, 0, 'or');
    hold off;
    title(sprintf('iPing=%d  iBeam=%d  BeamAngle=%6.2f  R=%6.1f  QF=%f ', iPing, iBeam, BeamAngle, R, QF))
    drawnow
    
    subplot(N, 2, 2*k);
    PlotUtils.createSScPlot(x, amp, '+k'); grid on;
    hold on; plot([R R], [0 max(amp)], 'r');
    hold off;
    title(sprintf('iPing=%d  iBeam=%d  BeamAngle=%6.2f  R=%6.1f  QF=%f ', iPing, iBeam, BeamAngle, R, QF))
    drawnow
end

%% Suppression of outliers

subNonOutliers = find(abs(Diff) <= (2*Eqm));
if Reson_testLengthPhase(x(subNonOutliers), NechRes)
    flag = 0;
    return
end
x     = x(subNonOutliers);
phase = phase(subNonOutliers);
amp   = amp(subNonOutliers);
amp2  = amp2(subNonOutliers);
phase_poly = phase_poly(subNonOutliers);

%% Centrage autour de R

r = floor(R);
d = min(r-x(1), x(end)-r);
if (d > 1) %  && ((d/n) > 0.2)
    sub = find((x > (R-d)) & (x < R+d) & (phase_poly > -180) & (phase_poly < 180));
    if Reson_testLengthPhase(x(sub), NechRes)
        flag = 0;
        return
    end
    x     = x(sub);
    phase = phase(sub);
    amp   = amp(sub);
    amp2  = amp2(sub);
else
    flag = 0;
    return
end

%% Reduction de 1/8 de part et d'autre

n = length(x);
% sub = ceil(n/8):floor(n*7/8);
sub = ceil(n/6):floor(n*5/6);
% sub = ceil(n/4):floor(n*3/4);
% n = length(sub);
% if (n < NechRes) || ((x(sub(end))-x(sub(1)))/n > 1.5)
if Reson_testLengthPhase(x(sub), NechRes)
    flag = 0;
    return
end
x     = x(sub);
phase = phase(sub);
amp   = amp(sub);
amp2  = amp2(sub);

%{
figure;
h(1) = subplot(2,1,1); PlotUtils.createSScPlot(x,amp); grid on;
h(2) = subplot(2,1,2); PlotUtils.createSScPlot(x,phase); grid on;
linkaxes(h, 'x');
%}
