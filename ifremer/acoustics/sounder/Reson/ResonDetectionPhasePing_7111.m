
function [PhaseBeamRange, PhaseBeamQF, PhaseBeamPente, PhaseBeamNbSamples, PhaseBeamEqm] = ...
    ResonDetectionPhasePing_7111(x, phase, amp, iPing, iBeam, BeamAngle, DeltaTeta, R0, nbBeams, DisplayLevel, varargin)

PhaseBeamRange     = NaN;
PhaseBeamEqm       = NaN;
PhaseBeamPente     = NaN;
PhaseBeamNbSamples = NaN;
PhaseBeamQF        = NaN;

%% Copy of inputs in work variables

x1      = x;
phase1  = phase;
amp1    = amp;

%% Determination of the number of samples corresponding to a beam spreading

% Sample number inside the whole beam
Beta = 1.2;
NechBeam = R0 * DeltaTeta * (pi/180) * sind(abs(BeamAngle)) / (2 * Beta * cosd(BeamAngle)^2);

% Sample number inside the resolution cell (sounding horizontal spacing)
NechRes = ceil(1.3 * abs(2 * R0 .* sind(BeamAngle) * (tand(BeamAngle(end)) / nbBeams)));

NechRes = min(NechBeam, NechRes);
NechRes = max(NechRes, 6);
NechRampMin = 13;

%% If number of samples is less than 11 we do not try to process the sounding

n = length(x);
% if (n < NechRampMin) || ((n < 150) && (((x(end)-x(1))/n) > 1.5))
if Reson_testLengthPhase(x, NechRampMin)
    return
end
NechSondeMin = ceil(n / 20);
NechSondeMin = max(NechSondeMin, NechRampMin);
NechSondeMin = max(NechSondeMin, NechRes);

%% Determination of the sounding

N = 4;
i = 0;
flag = 1;
while flag
    i = i + 1;
    [flag, R1, QF, PhaseBeamPente1, PhaseBeamEqm1, PhaseBeamNbSamples1, x1, phase1, amp1] = ...
        ResonDetectionPhaseBeam(x1, phase1, amp1, R0, N, i, iPing, iBeam, BeamAngle, NechSondeMin, DisplayLevel);
    if ~flag
        if DisplayLevel == 3
            figure(23456);
            for k=i+1:N*2
                h = subplot(N, 2, k);
                delete(h)
                drawnow
            end
        end

        break
    end
end
PhaseBeamQF         = QF;
PhaseBeamRange      = R1;
PhaseBeamPente      = PhaseBeamPente1;
PhaseBeamEqm        = PhaseBeamEqm1;
PhaseBeamNbSamples  = PhaseBeamNbSamples1;


%% Polynome d'ordre 1

function [flag, R, QF, Pente, Eqm, PhaseBeamNbSamples, x, phase, amp] = ResonDetectionPhaseBeam(x, phase, amp, R0, N, i, iPing, iBeam, BeamAngle, NechRes, DisplayLevel)

R     = NaN;
QF    = NaN;
Pente = NaN;
Eqm   = NaN;
PhaseBeamNbSamples  = NaN;

x     = x(:);
phase = phase(:);
amp   = amp(:);

subNonNaN = find(~isnan(phase) & ~isnan(amp));
% n = length(x);
% if (n < NechRes) || ((n < 150) && (((x(end)-x(1))/n) > 1.5))
if Reson_testLengthPhase(x, NechRes)
    flag = 0;
    return
end

x     = x(subNonNaN);
phase = phase(subNonNaN);
amp   = amp(subNonNaN);

[a, b, R, Eqm] = regressionLineairePonderee(x, phase, amp);
if isnan(a)
    flag = 0;
    return
end
Pente = abs(a);%atand(a);% - BeamAngle;
QF    = Pente * sqrt(length(x)/R0) ./ Eqm;
QF    = QF * 300;

QF = log10(QF);

phase_poly  = a*x + b;
Diff        = phase - phase_poly;
Eqm         = std(Diff);
PhaseBeamNbSamples = length(x);
flag = 1;

%{
figure(6557);
h(1) = subplot(2,1,1); plot(x, phase, '*'); hold on; plot(x, a*x+b, 'k'); grid on; title('Avec ponder = Amp'); hold off
h(2) = subplot(2,1,2); plot(x, amp, '*'); grid on;
linkaxes(h, 'x')
%}


% figure; plot(x, phase, '*'); hold on; plot(x, phase_poly, 'k'); grid on; title('Avec ponder = Amp^2')
% figure; plot(x, phase, '+k'); grid on;

% DisplayLevel=0;
if DisplayLevel == 3
    figure(23456);
    if i > 2*N
        i = N;
    end
    subplot(N, 2, i);

    PlotUtils.createSScPlot(x, phase, '+k'); grid on;
    hold on; plot(x, phase_poly, 'b');
    plot(R, 0, 'or');
    hold off;
    title(sprintf('iPing=%d  iBeam=%d  BeamAngle=%6.2f  R=%6.1f  QF=%f ', iPing, iBeam, BeamAngle, R, QF))
%     for k=i+1:N
%         subplot(N, 1, k);
%         plot(x, NaN(size(x)), '+k'); grid on;
%     end
    drawnow
end

%% Suppression of outliers

subNonOutliers = find(abs(Diff) <= (2*Eqm));
% n = length(subNonOutliers);
% if (n < NechRes) || ((x(subNonOutliers(end))-x(subNonOutliers(1)))/n > 1.5)
if Reson_testLengthPhase(x(subNonOutliers), NechRes)
    flag = 0;
    return
end
x     = x(subNonOutliers);
phase = phase(subNonOutliers);
amp   = amp(subNonOutliers);

%% Centrage autour de R

n = length(x);
r = floor(R);
d = min(r-x(1), x(end)-r);
if (d > 1) && ((d/n) > 0.2) 
    sub = find((x > (R-d)) & (x < R+d));
    n = length(sub);
    if (n < NechRes) || ((x(sub(end))-x(sub(1)))/n > 1.5)
        flag = 0;
        return
    end
    x     = x(sub);
    phase = phase(sub);
    amp   = amp(sub);
else
    flag = 0;
    return
end

%% Reduction de 1/8 de part et d'autre

n = length(x);
sub = ceil(n/8):floor(n*7/8);
% n = length(sub);
% if (n < NechRes) || ((x(sub(end))-x(sub(1)))/n > 1.5)
if Reson_testLengthPhase(x(sub), NechRes)
    flag = 0;
    return
end
x     = x(sub);
phase = phase(sub);
amp   = amp(sub);

