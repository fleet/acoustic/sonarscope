function Masque = ResonMasqueAmplitude(SystemSerialNumber, Amp, Teta, ...
    R0, RMax1, RMax2, R1SamplesFiltre, ...
    NbSamplesForPulseLength, MaskWidth, DisplayLevel, iPing, IdentAlgo)

switch SystemSerialNumber
    case 7150
        Masque = ResonMasqueAmplitude_7150_V1(Amp, ...
            R0, RMax1, RMax2, R1SamplesFiltre, ...
            NbSamplesForPulseLength, MaskWidth, DisplayLevel, iPing, IdentAlgo, Teta);
    otherwise % 7111, 7125
        Masque = ResonMasqueAmplitude_7111_V2(Amp, Teta, ...
            R0, RMax1, RMax2, R1SamplesFiltre, ...
            NbSamplesForPulseLength, MaskWidth, DisplayLevel, iPing, IdentAlgo);
end

if (IdentAlgo == 2) && ~isnan(R0)
    m = median(Amp, 2, 'omitnan');
    % figure; plot(m); grid;
    sub = max(1, floor(R0*0.8)):min(floor(R0*1.2), length(m));
    [valMax, H] = max(m(sub));
    H = H + sub(1) - 1;
    sub2 = find(m(sub) >= (valMax/2));
    w = sub2(end) - sub2(1);
    sub2 = sub(sub2);
    
    % Masque au dessus de la borne haute du sp�culaire
    Masque(1:(sub2(1)-1),:) = 0;
    
    % D�termination de deltaTeta
    deltaTeta = 1.4 * sqrt(w / H) * (180/pi);
    %     n = floor(length(Teta) / 2);
    
    % TODO : calcul faux, il faut se centrer sur le faisceau o� il y a sp�culaire
    
    [~, iTetaSpeculaire] = min(R1SamplesFiltre, [], 'omitnan');
    angleCentral = Teta(iTetaSpeculaire);
    subTeta = (abs(Teta + angleCentral) < deltaTeta);
    Masque(1:(sub2(end)-1), ~subTeta) = 0;
end
