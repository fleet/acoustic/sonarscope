function Masque = ResonMasqueAmplitude_7111_V2(Amp, Teta, ...
    R0, RMax1, RMax2, R1SamplesFiltre, ...
    NbSamplesForPulseLength, MaskWidth, DisplayLevel, iPing, IdentAlgo, varargin)

if isnan(R0)
    Masque = false(size(Amp));
    return
end

nbBeams = size(Amp,2);
if (nbBeams == 101) || (nbBeams == 201) || (nbBeams == 160) || (nbBeams == 256) % 160=EM3002D
    Masque = ResonMasqueAmplitude_7111_201(Amp, R0, RMax1, RMax2, R1SamplesFiltre, ...
        NbSamplesForPulseLength, MaskWidth, DisplayLevel, iPing, varargin{:});
else
    AnglesNominaux = get_7111_NominalBeamAngles;
    subBeamsNominaux = floor(my_interp1(Teta, 1:nbBeams, AnglesNominaux, 'linear', 'extrap'));
    %{
    figure; hold on;
    plot(subBeamsNominaux, '-or'); grid on;
    %}    
    sub = (subBeamsNominaux >= 1) & (subBeamsNominaux <= nbBeams);
    subBeamsNominaux = subBeamsNominaux(sub);
%     nbBeamsNominaux = length(subBeamsNominaux);
    %{
    figure; hold on;
    plot(subBeamsNominaux, '-or'); grid on;
    subBeamsNominauxManuels = [1 8 16 23 28 34 39 44 48 52 56 60 63 67 70 72 75 77 79]
    PlotUtils.createSScPlot(2:length(subBeamsNominauxManuels)+1, subBeamsNominauxManuels, '-xb');
    grid on; hold off;
    figure; plot(diff(subBeamsNominaux), '*'); grid on
    %}
    subBeamsNominaux = unique(subBeamsNominaux);
    
    sub = isnan(RMax1(subBeamsNominaux));
    subBeamsNominaux(sub) = [];
    
    % Added SB 16/05/2008
    if isempty(subBeamsNominaux)
        return
    end
    
%     nbBeamsNominaux = length(subBeamsNominaux);
    %{
    figure; hold on;
    plot(subBeamsNominaux, '-or'); grid on;
    figure; plot(diff(subBeamsNominaux), '*'); grid on
    %}
    Amp                = Amp(:,subBeamsNominaux);
%     Teta               = Teta(subBeamsNominaux);
    RMax1              = RMax1(subBeamsNominaux);
    RMax2              = RMax2(subBeamsNominaux);
    R1SamplesFiltre    = R1SamplesFiltre(subBeamsNominaux);
    MaskWidth  = MaskWidth(subBeamsNominaux);

    Masque = false(size(Amp,1),301);
    Masque150 = ResonMasqueAmplitude_7111_201(Amp, R0, RMax1, RMax2, R1SamplesFiltre, ...
        NbSamplesForPulseLength, MaskWidth, DisplayLevel, iPing, varargin{:});
    Masque(:,subBeamsNominaux) = Masque150;
    
    for i=1:(length(subBeamsNominaux)-1)
        k1 = subBeamsNominaux(i)+1;
        k2 = subBeamsNominaux(i+1)-1;
        for k=k1:k2
            Masque(:,k) = Masque(:,subBeamsNominaux(i)) | Masque(:,subBeamsNominaux(i+1));
        end
    end
    
    k = subBeamsNominaux(1);
    for i=(k-1):-1:1
        Masque(:,i) = Masque(:,k);
    end
    k = subBeamsNominaux(end);
    for i=(k+1):301
        Masque(:,i) = Masque(:,k);
    end

% % Modification to be tested seriously, do not is in 7kcenter for the moment
%     k = subBeamsNominaux(1);
%     delta = floor(2*(subBeamsNominaux(2) - subBeamsNominaux(1))) + 1;
%     for i=(k-1):-1:max(1,k-delta)
%         Masque(:,i) = Masque(:,k);
%     end
%     k = subBeamsNominaux(end);
%     delta = floor(2*(subBeamsNominaux(end) - subBeamsNominaux(end-1))) + 1;
%     for i=(k+1):min(k+delta,301)
%         Masque(:,i) = Masque(:,k);
%     end
end

function Masque = ResonMasqueAmplitude_7111_201(Amp, ...
    R0, RMax1, RMax2, R1SamplesFiltre, ...
    NbSamplesForPulseLength, MaskWidth, DisplayLevel, iPing, varargin)

Masque    = false(size(Amp));
nbBeams   = size(Amp,2);
% nbSamples = size(Amp,1);

% Beta = 1.2; % Coefficient d'�largissement du lobe du � la pond�ration
% w = ceil(R0 .* (sind(abs(Teta)) ./ (cosd(Teta).^2.33)) * (tand(ReceiveBeamWidth) / Beta)); % 2.33 pour le 7111, 3 pour le 7150

% w = ceil(R0 * 2*abs(1 ./ cosd(Teta) - (1 ./ cosd(Teta-ReceiveBeamWidth/2))));
% w = w + 4 * NbSamplesForPulseLength;

w = MaskWidth;

% figure; plot(w); grid on
% w = w + NbSamplesForPulseLength;
% nbSamplePhasRampeMin = max(20, floor(nbSamples/20));
% w = max(w, repmat(nbSamplePhasRampeMin, 1, nbBeams));

% Formule XL pour le nombre min dc'�chantillons pour la rampe de phase: 
% w = ceil(abs(2 * R0 .* sind(Teta) * (tand(Teta(end)) / nbBeams)));

% Formule de Jim
% w = floor(R/10 ./ cosd(Teta));

if DisplayLevel > 1
    hFig = FigUtils.createSScFigure(567878);
    set(hFig, 'Name', 'Mask Determination')
    imagesc(reflec_Enr2dB(Amp)); colormap(jet(256)); colorbar;
    hold on;
    NMax = size(Amp,1);
    PlotUtils.createSScPlot(RMax1, 'or'); plot(max(RMax1-w, 0), 'r'); plot(min(RMax1+w, NMax), 'r');
    PlotUtils.createSScPlot(RMax2, 'ok'); plot(max(RMax2-w, 0), 'k'); plot(min(RMax2+w, NMax), 'k');
    h = PlotUtils.createSScPlot([1 nbBeams], [R0 R0], '-m');
    set(h, 'LineWidth', 2)
    hold off;
    title(sprintf('Ping %d', iPing))
end

WMin = min(MaskWidth);

% Elargissement n�cessaire pour faisceau 154 du ping 1 du fichier
% F:\Feb2008\7111\Toulon\20080227_091035
% L'�cart entre les 2 derni�res valeurs de RMax1 �tait sup�rieur � la
% largeur de la fen�tre, on avait donc un masque avec un trou
w = max(w,2*gradient(RMax1));
%     figure; plot(w,'b'); grid on; hold on; plot(max(w,ww),'r')

step = 2*NbSamplesForPulseLength;
for iBeam=1:nbBeams
    w1 = w(iBeam);
    if ~isnan(w1)
        w2 = w(iBeam);
        if ~isnan(RMax1(iBeam))
            sub = RMax1(iBeam)-w1:RMax1(iBeam)+w2;
            
            if (RMax1(iBeam) / R0)  < 1.1
                sub = RMax1(iBeam)-w1:RMax1(iBeam)+2*w2;
            end
            
            sub(sub > size(Amp,1)) = [];
            
            sub(sub < (R0-step)) = [];
            sub(sub < 1) = [];

            if RMax1(iBeam) > (R0*1.25)
                sub(sub < (R0+WMin)) = [];
            end
            
            Masque(sub, iBeam) = 1;
        end
        
        if ~isnan(RMax2(iBeam)) && (RMax2(iBeam) ~= RMax1(iBeam))
            sub = RMax2(iBeam)-w1:RMax2(iBeam)+w2;
            sub(sub > size(Amp,1)) = [];
            
            sub(sub < (R0-step)) = [];
            sub(sub < (1)) = [];
            
            if RMax2(iBeam) > (R0*1.25)
                sub(sub < (R0+WMin)) = [];
            end
            
            Masque(sub, iBeam) = 1;
        end
        
        R1SamplesFiltre = floor(R1SamplesFiltre);
        if ~isnan(R1SamplesFiltre(iBeam)) && (R1SamplesFiltre(iBeam) ~= RMax1(iBeam))
            sub = R1SamplesFiltre(iBeam)-w1:R1SamplesFiltre(iBeam)+w2;
            sub(sub > size(Amp,1)) = [];
            sub(sub < (R0-step)) = [];
            if R1SamplesFiltre(iBeam) > (R0*1.25)
                sub(sub < (R0+WMin)) = [];
            end
            Masque(sub, iBeam) = 1;
        end
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %     % Test for time optimization of C implementation
        iDeb = find(Masque(:, iBeam), 1, 'first');
        iFin = find(Masque(:, iBeam), 1, 'last');
        Masque(iDeb:iFin, iBeam) = 1;
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    end
end

se = [1 1 1 1 1];
Masque = imdilate(Masque, se);


