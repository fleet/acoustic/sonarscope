function Masque = ResonMasqueAmplitude_7150_V1(Amp, R0, RMax1, RMax2, R1SamplesFiltre, ...
    NbSamplesForPulseLength, MaskWidth, DisplayLevel, iPing, IdentAlgo, Teta, varargin)

if isnan(R0)
    Masque = false(size(Amp));
    return
end

Masque  = false(size(Amp));
nbBeams = size(Amp,2);

w = MaskWidth;
w = w + 4 * NbSamplesForPulseLength;

if DisplayLevel > 1
    hFig = FigUtils.createSScFigure(567878);
    set(hFig, 'Name', 'Mask Determination')
    imagesc(reflec_Enr2dB(Amp)); colormap(jet(256)); colorbar;
    hold on;
    PlotUtils.createSScPlot(RMax1, 'or'); plot(RMax1-w, 'r'); plot(RMax1+w, 'r');
    PlotUtils.createSScPlot(RMax2, 'ok'); plot(RMax2-w, 'k'); plot(RMax2+w, 'k');
    h = plot([1 nbBeams], [R0 R0], '-m');
    set(h, 'LineWidth', 2)
    hold off;
    title(sprintf('Ping %d', iPing))
end

WMin = min(MaskWidth);

% Elargissement n�cessaire pour faisceau 154 du ping 1 du fichier
% F:\Feb2008\7111\Toulon\20080227_091035
% L'�cart entre les 2 derni�res valeurs de RMax1 �tait sup�rieur � la
% largeur de la fen�tre, on avait donc un masque avec un trou
w = max(w, 2*gradient(R1SamplesFiltre));
%  figure; plot(w,'b'); grid on; hold on; plot(max(w,ww),'r')

step = ceil(size(Amp,1) / 200);
for iBeam=1:nbBeams
    w1 = w(iBeam);
    w2 = w(iBeam);
    w1 = ceil(w1);
    w2 = ceil(w2*2);

    if ~isnan(RMax1(iBeam))
        sub = RMax1(iBeam)-w1:RMax1(iBeam)+w2;
        sub(sub > size(Amp,1)) = [];
        sub(sub < (R0-step)) = [];
        if RMax1(iBeam) > (R0*1.15)
            sub(sub < (R0+WMin)) = [];
        end        
        Masque(sub, iBeam) = 1;
    end
    
    if ~isnan(RMax2(iBeam)) && (RMax2(iBeam) ~= RMax1(iBeam))
        sub = RMax2(iBeam)-w1:RMax2(iBeam)+w2;
        sub(sub > size(Amp,1)) = [];
        sub(sub < (R0-step)) = [];
        if RMax2(iBeam) > (R0*1.15)
            sub(sub < (R0+WMin)) = [];
        end
        Masque(sub, iBeam) = 1;
    end
    
    R1SamplesFiltre = floor(R1SamplesFiltre);
    if ~isnan(R1SamplesFiltre(iBeam)) && (R1SamplesFiltre(iBeam) ~= RMax1(iBeam))
        sub = R1SamplesFiltre(iBeam)-w1:R1SamplesFiltre(iBeam)+w2;
        sub(sub > size(Amp,1)) = [];

        sub(sub < (R0-step)) = [];
        if R1SamplesFiltre(iBeam) > (R0*1.15)
            sub(sub < (R0+WMin)) = [];
        end
        Masque(sub, iBeam) = 1;
    end
end

se = [1 1 1 1 1];
Masque = imdilate(Masque, se);

%{
% En cours de test le 01/12/2014
if IdentAlgo == 2
    m = median(Amp, 2, 'omitnan');
    % figure; plot(m); grid;
    sub = floor(R0*0.8):floor(R0*1.2);
    [valMax, H] = max(m(sub));
    H = H + sub(1) - 1;
    sub2 = find(m(sub) >= (valMax/2));
    w = sub2(end) - sub2(1);
    sub2 = sub(sub2);
    
    % Masque au dessus de la borne haute du sp�culaire
    Masque(1:(sub2(1)-1),:) = 0;
    
    % D�termination de deltaTeta
    deltaTeta = 1.4 * sqrt(w / H) * (180/pi);
    %     n = floor(length(Teta) / 2);
    
    % TODO : calcul faux, il faut se centrer sur le faisceau o� il y a sp�culaire
    
    [~, iTetaSpeculaire] = min(R1SamplesFiltre, [], 'omitnan');
    angleCentral = Teta(iTetaSpeculaire);
    subTeta = (abs(Teta + angleCentral) < deltaTeta);
    Masque(1:(sub2(end)-1), ~subTeta) = 0;
end
%}
