% Determination of a mask on phase

function [Masque, Ph] = ResonMasquePhase_V5(Ph, Amp, R0, iBeam0, RMax1, DisplayLevel)

nbBeams   = size(Ph,2);
nbSamples = size(Ph,1);

Moitie = iBeam0;
subBab = 1:Moitie;
subTri = (Moitie+1):nbBeams;

Masque = false(size(Ph));
if isnan(R0)
    return
end

Seuil = 5; % 20 pour 7111 301 f&isceaux et 7150

R0 = max(R0, min(RMax1));
R0 = floor(R0 * 1.02);

for iSample=floor(R0):nbSamples
    subBeams = find(~isnan(Ph(iSample,subBab)));
    subEnd = find(diff(subBeams) ~= 1);
    if isempty(subEnd)
        subBeg = 1;
        subEnd = length(subBeams);
    else
        subBeg = [1 subEnd+1];
        subEnd(end+1) = length(subBeams); %#ok<AGROW>
    end
    for k=1:length(subBeg)
        KsubBeams = subBeams(subBeg(k):subEnd(k));
        if length(KsubBeams) > Seuil
            ph  = Ph(iSample,  subBab(KsubBeams));
            amp = Amp(iSample, subBab(KsubBeams));

            [indiceR, Sigma] = calcul_barycentre(1:length(amp), amp.^2); % Detection on Energy
            if ~isnan(indiceR)
                indiceR = floor(indiceR);
                
                if Sigma > 2
                    [m, ph] = masquePh2(ph, indiceR, Sigma, DisplayLevel);
                else
                    [m, ph] = masquePh3(ph, indiceR, DisplayLevel);
                end
                
                %{
        figure(3254);
        subplot(2,1,1); plot(subBab(KsubBeams), amp, '-*'); grid on;
        hold on;
        C = subBab(KsubBeams(indiceR));
        plot(C, amp(indiceR), '*r');
        plot([C-Sigma C+Sigma], [amp(indiceR) amp(indiceR)], '-k');
        hold off;

        subplot(2,1,2); plot(subBab(KsubBeams), ph, '-*'); grid on;
        hold on;
        C = subBab(KsubBeams(indiceR));
        plot(C, ph(indiceR), '*r');
        plot([C-Sigma C+Sigma], [ph(indiceR) ph(indiceR)], '-k');
        hold off;
                %}
                
                Masque(iSample,subBab(KsubBeams)) = Masque(iSample,subBab(KsubBeams)) | m;
                Ph(iSample, subBab(KsubBeams)) = ph;
            end
        else
            Masque(iSample,subBab(KsubBeams)) = 1;
        end
    end

    subBeams = find(~isnan(Ph(iSample, subTri)));
    subEnd = find(diff(subBeams) ~= 1);
    if isempty(subEnd)
        subBeg = 1;
        subEnd = length(subBeams);
    else
        subBeg = [1 subEnd+1];
        subEnd(end+1) = length(subBeams); %#ok<AGROW>
    end
    for k=1:length(subBeg)
        KsubBeams = subBeams(subBeg(k):subEnd(k));
        if length(KsubBeams) > Seuil
            ph  = Ph(iSample,  subTri(KsubBeams));
            amp = Amp(iSample, subTri(KsubBeams));
            %   figure; plot(subTri(KsubBeams), ph, '-*'); grid

            [indiceR, Sigma] = calcul_barycentre(1:length(amp), amp.^2); % Detection on Energy
            indiceR = floor(indiceR);

            if Sigma > 2
                [m, ph] = masquePh2(ph, indiceR, Sigma, DisplayLevel);
            else
                [m, ph] = masquePh3(ph, indiceR, DisplayLevel);
            end
            Masque(iSample,subTri(KsubBeams)) = Masque(iSample,subTri(KsubBeams)) | m;
            Ph(iSample, subTri(KsubBeams)) = ph;
        else
            Masque(iSample,subTri(KsubBeams)) = 1;
        end
    end
end

for iBeam=1:nbBeams
    sub = iBeam-10:iBeam+10;
    sub(sub < 1) = []; 
    sub(sub > nbBeams) = []; 
    if all(isnan(RMax1(sub)))
        Masque(:,iBeam) = 0;
    end
end

sub = (RMax1 <= R0);
Masque(:,sub) = 0;


function [m, ph] = masquePh2(ph, iMax, Sigma, DisplayLevel)

n = length(ph);
iBeg = iMax - 2*Sigma;
iEnd = iMax + 2*Sigma;
if iBeg < 1
    iEnd = floor(iEnd - iBeg);
    iBeg = 1;
    flagFirstOrLastBeam = true;
elseif iEnd > n
    iBeg = floor(iEnd-n);
    iEnd = n;
    flagFirstOrLastBeam = true;
else
    iBeg = floor(iBeg);
    iEnd = floor(iEnd);
    flagFirstOrLastBeam = false;
end
iBeg = max(iBeg, 1);
iEnd = min(iEnd, n);

ph(1:(iBeg-1)) = NaN;
ph((iEnd+1):end) = NaN;

m = zeros(1,n);
m = (m == 1);

if isnan(iMax)
    return
end

for k=iMax:(n-1)
    if abs(ph(k+1)-ph(k)) < 80%*2
        m(k) = true;
        m(k+1) = true;
    else
        break
    end
end
for k=(iMax-1):-1:1
    if abs(ph(k+1)-ph(k)) < 80%*2
        m(k) = true;
        m(k+1) = true;
    else
        break
    end
end

sub = find(abs(ph) > 150);
ph(sub) = NaN;
m(sub) = false;

if ~flagFirstOrLastBeam
    if all(ph(m) < 0) || all(ph(m) > 0)
        m(:) = false;
    end
end

if DisplayLevel == 4
    if isempty(m)
        return
    end
    figure(63445);
    h(1) = subplot(2,1,1); plot(ph); grid on; title('Phase')
    h(2) = subplot(2,1,2); plot(m, '-o'); grid on; title('Window')
    linkaxes(h, 'x')
    pause(0.2)
end

function [m, ph] = masquePh3(ph, iMax, DisplayLevel)

n = length(ph);
iBeg = 1;
iEnd = n;
flagFirstOrLastBeam = true;

ph(1:(iBeg-1)) = NaN;
ph((iEnd+1):end) = NaN;

m = zeros(1,n);
m = (m == 1);
if isnan(iMax)
    return
end

for k=iMax:(n-1)
    if abs(ph(k+1)-ph(k)) < 160
        m(k) = true;
        m(k+1) = true;
    else
        break
    end
end
for k=(iMax-1):-1:1
    if abs(ph(k+1)-ph(k)) < 160
        m(k) = true;
        m(k+1) = true;
    else
        break
    end
end

if ~flagFirstOrLastBeam
    if all(ph(m) < 0) || all(ph(m) > 0)
        m(:) = false;
    end
end

if DisplayLevel == 4
    if isempty(m)
        return
    end
    figure(63445);
    h(1) = subplot(2,1,1); plot(ph); grid on; title('Phase')
    h(2) = subplot(2,1,2); plot(m, '-o'); grid on; title('Window')
    linkaxes(h, 'x')
    pause(0.2)
end

