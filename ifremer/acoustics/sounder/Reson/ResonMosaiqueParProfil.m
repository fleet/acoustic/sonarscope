% [flag, listeFicS7K, lastDir] = uiSelectFiles('ExtensionFiles', '.s7k', 'RepDefaut', 'G:\MARMESONET\Leg2')
% gridSize = 5;
% ResonMosaiqueParProfil(listeFicS7K, gridSize)

function flag = ResonMosaiqueParProfil(repExport, listeFicS7K, gridSize, CorFile, ModeDependant, selectionData, Window, skipExistingMosaic, varargin) %#ok<INUSL>

[varargin, TideFile] = getPropertyValue(varargin, 'TideFile', []); %#ok<ASGLU>

flag = 0;

% if TypeGrid == 1
    resol = gridSize;
% else
%     resol = 6;
% end
Carto =[];

if isempty(listeFicS7K)
    return
end

if ischar(listeFicS7K)
    listeFicS7K = {listeFicS7K};
end

if selectionData ==  1
    choixLayer = 30; % ReflectivityFromSnippets TODO � v�rifier  % 28 remplac� par 30 (200% remplac� par 100%) le 20/03/2017
    nomDataType = 'Reflectivity';
else
    choixLayer = 12; % DepthMask
    nomDataType = 'Bathymetry';
end

Bits = 'AskIt';

flagAtLeastOneFileHasBeenProcessed = 0;
N = length(listeFicS7K);
str1 = 'Calcul des mosa�ques individuelles.';
str2 = 'Computing individual mosaics.';
hw = create_waitbar(Lang(str1,str2), 'N', N);
for iFic=1:N
    nomFic = listeFicS7K{iFic};
    
    my_waitbar(iFic, N, hw)
    fprintf('%s\n%d / %d : %s\n', repmat('-',1,100), iFic, N, nomFic);
    
    if ~exist(listeFicS7K{iFic}, 'file')
        str1 = sprintf('Le fichier "%s" n''existe pas.', listeFicS7K{iFic});
        str2 = sprintf('"%s" does not exist.', listeFicS7K{iFic});
        my_warndlg(Lang(str1,str2), 0, 'Tag', 'FichierAllNExistePAs');
        continue
    end
    
    s7k = cl_reson_s7k('nomFic', nomFic, 'KeepNcID', true);
    if isempty(s7k)
        continue
    end
%     identSondeur = selectionSondeur(s7k);
    
    [c, Carto, Bits] = view_depth(s7k, 'ListeLayers', [choixLayer 2 3 4], 'Carto', Carto, ...
        'TideFile', TideFile, 'Bits', Bits, 'setMaskToAllLayers', 1);
    if isempty(c)
        continue
    end
    if length(c) < 4 % Un layer est manquant
        continue
    end
    
    Layer_PingBeam = c(1);
    if Layer_PingBeam.nbRows <= 1
        clear Layer_PingBeam c
        continue
    end
    
    AcrossDistance    = c(2);% + 0.6;
    AlongDistance     = c(3);% - 24.6 - 9.3 / 2; %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% JMA JMA JMA
    Angle_PingBeam    = c(4);
    Heading           = get(Layer_PingBeam, 'Heading');
    FishLatitude      = get(Layer_PingBeam, 'FishLatitude');
    FishLongitude     = get(Layer_PingBeam, 'FishLongitude');
%   SurfaceSoundSpeed = get(Layer_PingBeam, 'SurfaceSoundSpeed');
%   SonarHeave        = get(Layer_PingBeam, 'SonarHeave');
%   SonarImmersion    = get(Layer_PingBeam, 'SonarImmersion');
    
    if all(isnan(FishLatitude)) || all(isnan(FishLongitude))
        str1 = sprintf('Le fichier %s ne contient pas de navigation, il doit s''agir d''un fichier provenant du 7KCenter.', nomFic);
        str2 = sprintf('%s file does not contain any navigation data, it seems to be a file coming from the 7KCenter.', nomFic);
        my_warndlg(Lang(str1,str2), 0, 'Tag', 's7kNavigationEmpty');
        continue
    end
    
    %% Compensation statistique
    
    if ~isempty(CorFile)
        CourbeConditionnelle = load_courbesStats(CorFile);
        if isempty(CourbeConditionnelle)
            return
        end
        bilan = CourbeConditionnelle.bilan;
        
        switch bilan{1}(1).DataTypeConditions{1}
            case 'TxAngle'
                indConditionnel = 4;
            otherwise
                c(5) =  sonar_secteur_emission(Angle_PingBeam, []);
                indConditionnel = 5;
        end
        Layer_PingBeam = compensationCourbesStats(c(1), c(indConditionnel), bilan);
    end
    
    
    %% Calcul des coordonn�ess g�ographiques
    
    [flag, LatLon_PingBeam] = sonar_calcul_coordGeo_BathyFais(Layer_PingBeam, AcrossDistance, AlongDistance, ...
        Heading, FishLatitude, FishLongitude);
    if ~flag
        %         return
        continue
    end
    
    %% Estimation de la r�solution si elle n'est pas donn�e
    
    if isempty(resol)
        FishLatitude   = get(AcrossDistance, 'FishLatitude');
        FishLongitude  = get(AcrossDistance, 'FishLongitude');
        SonarDistPings = compute_InterPingDistance(FishLatitude, FishLongitude);
        resolAlong = median(SonarDistPings, 'omitnan');
        
        statsAcrossDistance = AcrossDistance.StatValues;
        resolAcross = statsAcrossDistance.Range / (AcrossDistance.nbColumns - 1);
        
        resol = nearestExpectedGridSize(max(resolAlong, resolAcross));
    end
    
    %% Calcul de la mosaique
    
    [flag, Layer_LatLong, Angle_LatLong] = sonar_mosaique2(Layer_PingBeam, Angle_PingBeam, ...
        LatLon_PingBeam(1), LatLon_PingBeam(2), resol, 'AlongInterpolation', 1);
    if ~flag
        %         return Layer_LatLong, Angle_LatLong
        continue
    end
    %     [flag, Layer_LatLong] = sonar_mosaique(Layer_PingBeam, LatLon_PingBeam(1), LatLon_PingBeam(2), resol, 'AlongInterpolation',  1);
    %     [flag, Angle_LatLong] = sonar_mosaique(Angle_PingBeam, LatLon_PingBeam(1), LatLon_PingBeam(2), resol, 'AlongInterpolation',  1);
    clear LatLon_PingBeam
    
    %% Interpolation
    
    Layer_LatLong = WinFillNaN(Layer_LatLong, 'window', [5 5]);
    Angle_LatLong = WinFillNaN(Angle_LatLong, 'window', [5 5]);
    
    %% Sauvegarde des mosaiques
    
    InitialImageName = strrep(Layer_LatLong.InitialImageName, '_Compensation_Compensation', '_Compensation');
    
    InitialImageName = [InitialImageName ' - ' num2str(rand(1))]; %#ok<AGROW>
    Layer_LatLong.InitialImageName = InitialImageName;
    
    [~, nomFic] = fileparts(nomFic);
    nomFicErs = fullfile(repExport, [nomFic '_' nomDataType '_LatLong.ers']);
    flag = export_ermapper(Layer_LatLong, nomFicErs);
    if ~flag
        return
    end
    
    InitialImageName = [Angle_LatLong.InitialImageName ' - ' num2str(rand(1))];
    Angle_LatLong.InitialImageName = InitialImageName;
    
    nomFicErs = fullfile(repExport, [nomFic '_RxBeamAngle_LatLong.ers']);
    flag = export_ermapper(Angle_LatLong, nomFicErs);
    if ~flag
        str1 = sprintf('Probl�me d''�criture de "%s", arr�t du traitement.', nomFicErs);
        str2 = sprintf('Write error for "%s", sopping the processing.', nomFicErs);
        my_warndlg(Lang(str1,str2), 1);
        return
    end
    flagAtLeastOneFileHasBeenProcessed = 1;
end
my_close(hw, 'MsgEnd')

flag = flagAtLeastOneFileHasBeenProcessed;
