function PhaseFiltree = ResonPhaseFilter(Phase)

x = cosd(Phase);
y = sind(Phase);

[B,A] = butter(1,0.1);
nbBeams = size(x,2);
xf = x;
yf = y;
for iBeam=1:nbBeams
    sub = ~isnan(x(:,iBeam));
    xf(sub,iBeam) = filtfilt(B, A, double(x(sub,iBeam)));
    yf(sub,iBeam) = filtfilt(B, A, double(y(sub,iBeam)));
end
PhaseFiltree = atan2(yf,xf) * (180/pi);

Residu = Phase - PhaseFiltree;
sub = abs(Residu) > 20;

PhaseFiltree(sub) = NaN;
