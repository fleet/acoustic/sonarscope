% Génération automatique des noms de fichiers SonarScope de l'imagerie Simrad
%
% Syntax
%   [nomFicSidescan, flagFilesExist] = ResonSidescanIFRNames(nomFicAll)
%
% Input Arguments
%   nomFicAll : Nom du fichier .s7m
%
% Output Arguments
%   nomFicSidescan : Structure contenant les noms des différents fichiers SonarScope.
%   flagFilesExist : 1=Files exist, 0=files no not exist
%
% Examples
%   nomFicS7k = getNomFicDatabase('xxxxxxxxxxxxxxxx.s7k');
%   nomFicSidescan = ResonSidescanIFRNames(nomFicS7k)
%
% See also read_SnippetData Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function [nomFicSidescan, flagFilesExist] = ResonSidescanIFRNames(nomFicS7k, identSondeur)

global useCacheNetcdf %#ok<GVMIS>

[nomDir, nomFicSeul] = fileparts(nomFicS7k);
if useCacheNetcdf == 1 % 1=Mat, 2 et 3=Netcdf
    nomFicSidescan.Mat = fullfile(nomDir, 'SonarScope', [num2str(identSondeur) '_' nomFicSeul '_SidescanIFRData.mat']);
else
    nomFicSidescan.Mat = fullfile(nomDir, 'SonarScope', [nomFicSeul '.nc']);
end

nomFicSidescan.ReflectivityErs  = fullfile(nomDir, 'SonarScope', [num2str(identSondeur) '_' nomFicSeul '_SidescanIFRReflectivity.ers']);
nomFicSidescan.Reflectivity     = fullfile(nomDir, 'SonarScope', [num2str(identSondeur) '_' nomFicSeul '_SidescanIFRReflectivity'], [num2str(identSondeur) '_' nomFicSeul '_SidescanIFRReflectivity']);
nomFicSidescan.ReflectivityMat  = fullfile(nomDir, 'SonarScope', [num2str(identSondeur) '_' nomFicSeul '_SidescanIFRReflectivity'], [num2str(identSondeur) '_' nomFicSeul '_SidescanIFRReflectivity_ers.mat']);
nomFicSidescan.ReflectivitySigV = fullfile(nomDir, 'SonarScope', [num2str(identSondeur) '_' nomFicSeul '_SidescanIFRReflectivity'], [num2str(identSondeur) '_' nomFicSeul '_SidescanIFRReflectivity_sigV']);

nomFicSidescan.RxBeamIndexErs  = fullfile(nomDir, 'SonarScope', [num2str(identSondeur) '_' nomFicSeul '_SidescanIFRRxBeamIndex.ers']);
nomFicSidescan.RxBeamIndex     = fullfile(nomDir, 'SonarScope', [num2str(identSondeur) '_' nomFicSeul '_SidescanIFRRxBeamIndex'], [num2str(identSondeur) '_' nomFicSeul '_SidescanIFRRxBeamIndex']);
nomFicSidescan.RxBeamIndexMat  = fullfile(nomDir, 'SonarScope', [num2str(identSondeur) '_' nomFicSeul '_SidescanIFRRxBeamIndex'], [num2str(identSondeur) '_' nomFicSeul '_SidescanIFRRxBeamIndex_ers.mat']);
nomFicSidescan.RxBeamIndexSigV = fullfile(nomDir, 'SonarScope', [num2str(identSondeur) '_' nomFicSeul '_SidescanIFRRxBeamIndex'], [num2str(identSondeur) '_' nomFicSeul '_SidescanIFRRxBeamIndex_sigV']);

nomFicSidescan.TxAngleErs  = fullfile(nomDir, 'SonarScope', [num2str(identSondeur) '_' nomFicSeul '_SidescanIFRTxAngle.ers']);
nomFicSidescan.TxAngle     = fullfile(nomDir, 'SonarScope', [num2str(identSondeur) '_' nomFicSeul '_SidescanIFRTxAngle'], [num2str(identSondeur) '_' nomFicSeul '_SidescanIFRTxAngle']);
nomFicSidescan.TxAngleMat  = fullfile(nomDir, 'SonarScope', [num2str(identSondeur) '_' nomFicSeul '_SidescanIFRTxAngle'], [num2str(identSondeur) '_' nomFicSeul '_SidescanIFRTxAngle_ers.mat']);
nomFicSidescan.TxAngleSigV = fullfile(nomDir, 'SonarScope', [num2str(identSondeur) '_' nomFicSeul '_SidescanIFRTxAngle'], [num2str(identSondeur) '_' nomFicSeul '_SidescanIFRTxAngle_sigV']);

nomFicSidescan.NbpErs  = fullfile(nomDir, 'SonarScope', [num2str(identSondeur) '_' nomFicSeul '_SidescanIFRNbp.ers']);
nomFicSidescan.Nbp     = fullfile(nomDir, 'SonarScope', [num2str(identSondeur) '_' nomFicSeul '_SidescanIFRNbp'], [num2str(identSondeur) '_' nomFicSeul '_SidescanIFRNbp']);
nomFicSidescan.NbpMat  = fullfile(nomDir, 'SonarScope', [num2str(identSondeur) '_' nomFicSeul '_SidescanIFRNbp'], [num2str(identSondeur) '_' nomFicSeul '_SidescanIFRNbp_ers.mat']);
nomFicSidescan.NbpSigV = fullfile(nomDir, 'SonarScope', [num2str(identSondeur) '_' nomFicSeul '_SidescanIFRNbp'], [num2str(identSondeur) '_' nomFicSeul '_SidescanIFRNbp_sigV']);

flagFilesExist = exist(nomFicSidescan.Mat, 'file') && ...
        exist(nomFicSidescan.Reflectivity, 'file') &&  ...
        exist(nomFicSidescan.ReflectivityErs, 'file') &&  ...
        exist(nomFicSidescan.ReflectivityMat, 'file') &&  ...
        exist(nomFicSidescan.ReflectivitySigV, 'file') &&  ...
        exist(nomFicSidescan.RxBeamIndex, 'file') &&  ...
        exist(nomFicSidescan.RxBeamIndexErs, 'file') &&  ...
        exist(nomFicSidescan.RxBeamIndexMat, 'file') &&  ...
        exist(nomFicSidescan.RxBeamIndexSigV, 'file') && ...
        exist(nomFicSidescan.Nbp, 'file') &&  ...
        exist(nomFicSidescan.NbpErs, 'file') &&  ...
        exist(nomFicSidescan.NbpMat, 'file') &&  ...
        exist(nomFicSidescan.NbpSigV, 'file') && ...
        exist(nomFicSidescan.TxAngle, 'file') &&  ...
        exist(nomFicSidescan.TxAngleErs, 'file') &&  ...
        exist(nomFicSidescan.TxAngleMat, 'file') &&  ...
        exist(nomFicSidescan.TxAngleSigV, 'file');% %&&  ...
