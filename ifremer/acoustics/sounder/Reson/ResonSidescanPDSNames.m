% Génération automatique des noms de fichiers SonarScope de l'imagerie Simrad
%
% Syntax
%   [nomFicSidescan, flagFilesExist] = ResonSidescanPDSNames(nomFicAll)
%
% Input Arguments
%   nomFicAll : Nom du fichier .s7m
%
% Output Arguments
%   nomFicSidescan : Structure contenant les noms des différents fichiers SonarScope.
%   flagFilesExist : 1=Files exist, 0=files no not exist
%
% Examples
%   nomFicS7k = getNomFicDatabase('xxxxxxxxxxxxxxxx.s7k');
%   nomFicSidescan = ResonSidescanPDSNames(nomFicS7k)
%
% See also read_SnippetData Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function [nomFicSidescan, flagFilesExist] = ResonSidescanPDSNames(nomFicS7k, identSondeur)

global useCacheNetcdf %#ok<GVMIS>

[nomDir, nomFicSeul] = fileparts(nomFicS7k);
if useCacheNetcdf == 1 % 1=Mat, 2 et 3=Netcdf
    nomFicSidescan.Mat = fullfile(nomDir, 'SonarScope', [num2str(identSondeur) '_' nomFicSeul '_SidescanPDSData.mat']);
else
    nomFicSidescan.Mat = fullfile(nomDir, 'SonarScope', [nomFicSeul '.nc']);
end

nomFicSidescan.ReflectivityErs  = fullfile(nomDir, 'SonarScope', [num2str(identSondeur) '_' nomFicSeul '_SidescanPDSReflectivity.ers']);
nomFicSidescan.Reflectivity     = fullfile(nomDir, 'SonarScope', [num2str(identSondeur) '_' nomFicSeul '_SidescanPDSReflectivity'], [num2str(identSondeur) '_' nomFicSeul '_SidescanPDSReflectivity']);
nomFicSidescan.ReflectivityMat  = fullfile(nomDir, 'SonarScope', [num2str(identSondeur) '_' nomFicSeul '_SidescanPDSReflectivity'], [num2str(identSondeur) '_' nomFicSeul '_SidescanPDSReflectivity_ers.mat']);
nomFicSidescan.ReflectivitySigV = fullfile(nomDir, 'SonarScope', [num2str(identSondeur) '_' nomFicSeul '_SidescanPDSReflectivity'], [num2str(identSondeur) '_' nomFicSeul '_SidescanPDSReflectivity_sigV']);

nomFicSidescan.RxBeamIndexErs  = fullfile(nomDir, 'SonarScope', [num2str(identSondeur) '_' nomFicSeul '_SidescanPDSRxBeamIndex.ers']);
nomFicSidescan.RxBeamIndex     = fullfile(nomDir, 'SonarScope', [num2str(identSondeur) '_' nomFicSeul '_SidescanPDSRxBeamIndex'], [num2str(identSondeur) '_' nomFicSeul '_SidescanPDSRxBeamIndex']);
nomFicSidescan.RxBeamIndexMat  = fullfile(nomDir, 'SonarScope', [num2str(identSondeur) '_' nomFicSeul '_SidescanPDSRxBeamIndex'], [num2str(identSondeur) '_' nomFicSeul '_SidescanPDSRxBeamIndex_ers.mat']);
nomFicSidescan.RxBeamIndexSigV = fullfile(nomDir, 'SonarScope', [num2str(identSondeur) '_' nomFicSeul '_SidescanPDSRxBeamIndex'], [num2str(identSondeur) '_' nomFicSeul '_SidescanPDSRxBeamIndex_sigV']);

nomFicSidescan.TxAngleErs  = fullfile(nomDir, 'SonarScope', [num2str(identSondeur) '_' nomFicSeul '_SidescanPDSTxAngle.ers']);
nomFicSidescan.TxAngle     = fullfile(nomDir, 'SonarScope', [num2str(identSondeur) '_' nomFicSeul '_SidescanPDSTxAngle'], [num2str(identSondeur) '_' nomFicSeul '_SidescanPDSTxAngle']);
nomFicSidescan.TxAngleMat  = fullfile(nomDir, 'SonarScope', [num2str(identSondeur) '_' nomFicSeul '_SidescanPDSTxAngle'], [num2str(identSondeur) '_' nomFicSeul '_SidescanPDSTxAngle_ers.mat']);
nomFicSidescan.TxAngleSigV = fullfile(nomDir, 'SonarScope', [num2str(identSondeur) '_' nomFicSeul '_SidescanPDSTxAngle'], [num2str(identSondeur) '_' nomFicSeul '_SidescanPDSTxAngle_sigV']);

nomFicSidescan.NbpErs  = fullfile(nomDir, 'SonarScope', [num2str(identSondeur) '_' nomFicSeul '_SidescanPDSNbp.ers']);
nomFicSidescan.Nbp     = fullfile(nomDir, 'SonarScope', [num2str(identSondeur) '_' nomFicSeul '_SidescanPDSNbp'], [num2str(identSondeur) '_' nomFicSeul '_SidescanPDSNbp']);
nomFicSidescan.NbpMat  = fullfile(nomDir, 'SonarScope', [num2str(identSondeur) '_' nomFicSeul '_SidescanPDSNbp'], [num2str(identSondeur) '_' nomFicSeul '_SidescanPDSNbp_ers.mat']);
nomFicSidescan.NbpSigV = fullfile(nomDir, 'SonarScope', [num2str(identSondeur) '_' nomFicSeul '_SidescanPDSNbp'], [num2str(identSondeur) '_' nomFicSeul '_SidescanPDSNbp_sigV']);

flagFilesExist = exist(nomFicSidescan.Mat, 'file') && checkFileCreation(nomFicSidescan.Mat, '02/10/2008') && ...
        exist(nomFicSidescan.ReflectivityErs, 'file') &&  ...
        exist(nomFicSidescan.ReflectivityMat, 'file');
