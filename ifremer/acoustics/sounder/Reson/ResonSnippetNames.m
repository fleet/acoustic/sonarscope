% Génération automatique des noms de fichiers SonarScope de l'imagerie Simrad
%
% Syntax
%   [nomFicSnippet, flagFilesExist] = ResonSnippetNames(nomFicAll)
%
% Input Arguments
%   nomFicAll      : Nom du fichier .s7m

%
% Output Arguments
%   nomFicSnippet : Structure contenant les noms des différents fichiers SonarScope.
%   flagFilesExist : 1=Files exist, 0=files no not exist
%
% Examples
%   nomFicS7k = getNomFicDatabase('xxxxxxxxxxxxxxxx.s7k');
%   nomFicSnippet = ResonSnippetNames(nomFicS7k)
%
% See also read_SnippetData Authors
% Authors : JMA
% VERSION    : $Id: entete.m,v 1.2 2003/11/12 12:46:11 augustin Exp $
%-------------------------------------------------------------------------------

function [nomFicSnippet, flagFilesExist] = ResonSnippetNames(nomFicS7k, identSondeur)

global useCacheNetcdf %#ok<GVMIS>

[nomDir, nomFicSeul] = fileparts(nomFicS7k);
if useCacheNetcdf == 1 % 1=Mat, 2 et 3=Netcdf
    nomFicSnippet.Mat = fullfile(nomDir, 'SonarScope', [num2str(identSondeur) '_' nomFicSeul '_SnippetData.mat']);
else
    nomFicSnippet.Mat = fullfile(nomDir, 'SonarScope', [nomFicSeul '.nc']);
end

nomFicSnippet.ReflectivityErs  = fullfile(nomDir, 'SonarScope', [num2str(identSondeur) '_' nomFicSeul '_SnippetReflectivity.ers']);
nomFicSnippet.Reflectivity     = fullfile(nomDir, 'SonarScope', [num2str(identSondeur) '_' nomFicSeul '_SnippetReflectivity'], [num2str(identSondeur) '_' nomFicSeul '_SnippetReflectivity']);
nomFicSnippet.ReflectivityMat  = fullfile(nomDir, 'SonarScope', [num2str(identSondeur) '_' nomFicSeul '_SnippetReflectivity'], [num2str(identSondeur) '_' nomFicSeul '_SnippetReflectivity_ers.mat']);
nomFicSnippet.ReflectivitySigV = fullfile(nomDir, 'SonarScope', [num2str(identSondeur) '_' nomFicSeul '_SnippetReflectivity'], [num2str(identSondeur) '_' nomFicSeul '_SnippetReflectivity_sigV']);

nomFicSnippet.RxBeamIndexErs  = fullfile(nomDir, 'SonarScope', [num2str(identSondeur) '_' nomFicSeul '_SnippetRxBeamIndex.ers']);
nomFicSnippet.RxBeamIndex     = fullfile(nomDir, 'SonarScope', [num2str(identSondeur) '_' nomFicSeul '_SnippetRxBeamIndex'], [num2str(identSondeur) '_' nomFicSeul '_SnippetRxBeamIndex']);
nomFicSnippet.RxBeamIndexMat  = fullfile(nomDir, 'SonarScope', [num2str(identSondeur) '_' nomFicSeul '_SnippetRxBeamIndex'], [num2str(identSondeur) '_' nomFicSeul '_SnippetRxBeamIndex_ers.mat']);
nomFicSnippet.RxBeamIndexSigV = fullfile(nomDir, 'SonarScope', [num2str(identSondeur) '_' nomFicSeul '_SnippetRxBeamIndex'], [num2str(identSondeur) '_' nomFicSeul '_SnippetRxBeamIndex_sigV']);

nomFicSnippet.TxAngleErs  = fullfile(nomDir, 'SonarScope', [num2str(identSondeur) '_' nomFicSeul '_SnippetTxAngle.ers']);
nomFicSnippet.TxAngle     = fullfile(nomDir, 'SonarScope', [num2str(identSondeur) '_' nomFicSeul '_SnippetTxAngle'], [num2str(identSondeur) '_' nomFicSeul '_SnippetTxAngle']);
nomFicSnippet.TxAngleMat  = fullfile(nomDir, 'SonarScope', [num2str(identSondeur) '_' nomFicSeul '_SnippetTxAngle'], [num2str(identSondeur) '_' nomFicSeul '_SnippetTxAngle_ers.mat']);
nomFicSnippet.TxAngleSigV = fullfile(nomDir, 'SonarScope', [num2str(identSondeur) '_' nomFicSeul '_SnippetTxAngle'], [num2str(identSondeur) '_' nomFicSeul '_SnippetTxAngle_sigV']);

flagFilesExist = exist(nomFicSnippet.Mat, 'file') && ...
        exist(nomFicSnippet.Reflectivity, 'file') &&  ...
        exist(nomFicSnippet.ReflectivityErs, 'file') &&  ...
        exist(nomFicSnippet.ReflectivityMat, 'file') &&  ...
        exist(nomFicSnippet.ReflectivitySigV, 'file') &&  ...
        exist(nomFicSnippet.RxBeamIndex, 'file') &&  ...
        exist(nomFicSnippet.RxBeamIndexErs, 'file') &&  ...
        exist(nomFicSnippet.RxBeamIndexMat, 'file') &&  ...
        exist(nomFicSnippet.RxBeamIndexSigV, 'file') && ...
        exist(nomFicSnippet.TxAngle, 'file') &&  ...
        exist(nomFicSnippet.TxAngleErs, 'file') &&  ...
        exist(nomFicSnippet.TxAngleMat, 'file') &&  ...
        exist(nomFicSnippet.TxAngleSigV, 'file');% %&&  ...
    