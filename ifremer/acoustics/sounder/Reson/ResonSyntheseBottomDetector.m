function [PingRange, TypeDetection, QualityFactor] = ResonSyntheseBottomDetector(AmpPingRange, AmpPingQF, ...
    PhasePingRange, PhasePingQF, PhasePingPente, PhasePingEqm, iBeamBeg, iBeamEnd)

nbBeams = length(AmpPingRange);
PingRange     = NaN(1, nbBeams);
TypeDetection = NaN(1, nbBeams);
QualityFactor = NaN(1, nbBeams);
QF_tresh = 2.1;

subAmpTrouve  = ~isnan(AmpPingRange);

endOfMask = (AmpPingRange == -1);

PhasePingPenteSeuil = 36;
PhasePingEqmSeuil = 45;

subPhase = (subAmpTrouve | endOfMask) & ~isnan(PhasePingRange) & (PhasePingQF >= QF_tresh) ...
    & (PhasePingPente <= PhasePingPenteSeuil) & (PhasePingEqm <= PhasePingEqmSeuil);

% Restriction to PhasePingQF > 2.2 for pings out of [iBeamBeg, iBeamEnd]
if isnan(iBeamBeg)
    iBeamBeg = 1;
end
if isnan(iBeamEnd)
    iBeamEnd = nbBeams;
end
sub = [1:iBeamBeg iBeamEnd:nbBeams];
subPhase(sub) = subPhase(sub) & (PhasePingQF(sub) > QF_tresh); % Question :Why 2.2, Answer : because

% Suppression of isolated phase detections
X = subPhase;
for iBeam=2:(nbBeams-1)
    if isequal(X(iBeam-1:iBeam+1), [0 1 0])
        subPhase(iBeam) = 0;
    end
end
% figure; plot(subPhase, '*'); grid on; hold on; plot(X, 'ok'); hold off

% Suppression of 2 isolated phase detections
X = subPhase;
for iBeam=1:(nbBeams-4)
    if isequal(X(iBeam:iBeam+3), [0 1 1 0])
        subPhase(iBeam+1:iBeam+2) = 0;
    end
end
% figure; plot(subPhase, '*'); grid on; hold on; plot(X, 'ok'); hold off

PingRange(subPhase) = PhasePingRange(subPhase);
TypeDetection(subPhase) = 2;
QualityFactor(subPhase) = PhasePingQF(subPhase);

%% On compl�te avec l'amplitude

subAmp = isnan(PingRange) & ~isnan(AmpPingRange) & (AmpPingQF >= QF_tresh);% no QF2 use anymore

PingRange(subAmp)     = AmpPingRange(subAmp);
TypeDetection(subAmp) = 1;
QualityFactor(subAmp) = AmpPingQF(subAmp);

subPhase = subAmpTrouve & isnan(PingRange) & ~isnan(PhasePingRange) & (PhasePingQF >= QF_tresh) & (PhasePingPente <= 36);
PingRange(subPhase) = PhasePingRange(subPhase);
TypeDetection(subPhase) = 2;
QualityFactor(subPhase) = PhasePingQF(subPhase);

%{
figure(5534); clear h
h(1) = subplot(4,3,1); plot(PhasePingRange, '-*'); grid on; title('PhasePingRange');
h(2) = subplot(4,3,4); plot(PhasePingQF, '-*');    grid on; title('PhasePingQF');
h(3) = subplot(4,3,7); plot(PhasePingPente, '-*'); grid on; title('PhasePingPente');
hold on; plot([1 nbBeams], [PhasePingPenteSeuil PhasePingPenteSeuil], 'r'); hold off;
h(4) = subplot(4,3,10); plot(PhasePingEqm, '-*'); grid on; title('PhasePingEqm');
hold on; plot([1 nbBeams], [PhasePingEqmSeuil PhasePingEqmSeuil], 'r'); hold off;

h(5) = subplot(4,3,2); plot(AmpPingRange, '-*'); grid on; title('AmpPingRange');
h(6) = subplot(4,3,5); plot(AmpPingQF, '-*'); grid on; title('AmpPingQF');
% h(10) = subplot(4,3,8); plot(AmpPingQF2, '-*'); grid on; title('AmpPingQF2');

h(7) = subplot(4,3,3); plot(QualityFactor, '-*'); grid on; title('QualityFactor');
h(8) = subplot(4,3,6); plot(PingRange, '-*'); grid on; title('PingRange');
h(9) = subplot(4,3,9); plot(TypeDetection, '-*'); grid on; title('TypeDetection');
linkaxes(h, 'x')
%}

%{
figure; clear h
h(1) = subplot(4,1,1); plot(AmpPingRange, 'k'); hold on; plot(PhasePingRange, 'b'); grid on;title('Bathy', 'Interpreter', 'none')
h(2) = subplot(4,1,2); plot(AmpPingQF, 'k'); grid on; title('AmpPingQF', 'Interpreter', 'none')
h(3) = subplot(4,1,3); plot(PhasePingQF, 'k'); grid on; title('PhasePingQF', 'Interpreter', 'none')
h(4) = subplot(4,1,4); plot(PhasePingPente, 'k'); grid on; title('PhasePingPente', 'Interpreter', 'none')
linkaxes(h, 'x')
%}
