function Phase = ResonUnwrappingPhase_Horz(Ph, iBeamMax0, RMax1)
% Phase = Ph;
% return

nbBeams   = size(Ph,2);
nbSamples = size(Ph,1);

% Added Brest 05/06/2008
subOK = find(~isnan(RMax1));
subKO = find(isnan(RMax1));
RMax1(subKO) = interp1(subOK, RMax1(subOK), subKO, 'linear', 'extrap');
RMax1 = floor(max(1,min(RMax1, nbSamples)));

% Phase = NaN(nbSamples, nbBeams, 'single');
Phase = Ph;
sub = 1:iBeamMax0;
for iBeam=1:iBeamMax0
    r1 = min(RMax1(iBeam:iBeam+1));
    if isnan(r1)
        continue
    end
    r2 = max(RMax1(iBeam:iBeam+1));
    for iSample=r1:r2
        [ph, subOK] = unwrapPh_Horz(Ph(iSample,sub), iBeam);
        Phase(iSample,sub(subOK)) = ph(subOK);
    end
end
sub = iBeamMax0:nbBeams;
offset = iBeamMax0-1;
for iBeam=iBeamMax0:nbBeams-1
    r1 = min(RMax1(iBeam:iBeam+1));
    if isnan(r1)
        continue
    end
    r2 = max(RMax1(iBeam:iBeam+1));
    for iSample=r1:r2
        [ph, subOK] = unwrapPh_Horz(Ph(iSample,sub), iBeam-offset);
        Phase(iSample,sub(subOK)) = ph(subOK);
    end
end


function [ph, subOK] = unwrapPh_Horz(ph, centre)

Seuil = 270; % 180
subOK = false(1, length(ph));
% subOK = zeros(1, length(ph), 'uint8') == 0;
n = length(ph);
for i=centre:(n-1)
    if isnan(ph(i+1))
        break
    end
    if (ph(i+1)-ph(i)) > Seuil
        ph(i+1) = ph(i+1)-360;
    elseif (ph(i+1)-ph(i)) < -Seuil
        ph(i+1) = ph(i+1)+360;
    end
    subOK(i) = true;
end
for i=(centre-1):-1:1
    if isnan(ph(i))
        break
    end
    if (ph(i+1)-ph(i)) > Seuil
        ph(i) = ph(i)+360;
    elseif (ph(i+1)-ph(i)) < -Seuil
        ph(i) = ph(i)-360;
    end
    subOK(i) = true;
end
