function Phase = ResonUnwrappingPhase_Vert(Ph, RMax1)

nbBeams   = size(Ph,2);
nbSamples = size(Ph,1);

Phase = NaN(nbSamples, nbBeams, 'single');
for iBeam=1:nbBeams
    if ~isnan(RMax1(iBeam))
        [ph, subOK] = unwrapPh_Vert(Ph(:,iBeam), RMax1(iBeam));
        Phase(subOK,iBeam) = ph(subOK);
    end
end

function [ph, subOK] = unwrapPh_Vert(ph, centre)

Seuil = 350; % 180
subOK = false(1, length(ph));
n = length(ph);
for i=centre:(n-1)
    if isnan(ph(i+1))
        break
    end
    if (ph(i+1)-ph(i)) > Seuil
        ph(i+1) = ph(i+1)-360;
    elseif (ph(i+1)-ph(i)) < -Seuil
        ph(i+1) = ph(i+1)+360;
    end
    subOK(i) = true;
end
for i=(centre-1):-1:1
    if isnan(ph(i))
        break
    end
    if (ph(i+1)-ph(i)) > Seuil
        ph(i) = ph(i)+360;
    elseif (ph(i+1)-ph(i)) < -Seuil
        ph(i) = ph(i)-360;
    end
    subOK(i) = true;
end
