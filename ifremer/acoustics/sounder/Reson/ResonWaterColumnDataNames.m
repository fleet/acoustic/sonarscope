function [nomFicWaterColumnData, flagFilesExist] = ResonWaterColumnDataNames(nomFicS7k, identSondeur, iPing)

global useCacheNetcdf %#ok<GVMIS>

[nomDir, nomFicSeul] = fileparts(nomFicS7k);

if useCacheNetcdf == 1 % 1=Mat, 2 et 3=Netcdf
    nomFicWaterColumnData.Mat = fullfile(nomDir, 'SonarScope', [num2str(identSondeur) '_' nomFicSeul '_WaterColumnDataIFRData.mat']);
else
    nomFicWaterColumnData.Mat = fullfile(nomDir, 'SonarScope', [nomFicSeul '.NC']);
end

if isempty(iPing)
    flagFilesExist = exist(nomFicWaterColumnData.Mat, 'file');
    return
end

if useCacheNetcdf == 1 % 1=Mat, 2 et 3=Netcdf
    nomFicWaterColumnData.AP  = fullfile(nomDir, 'SonarScope', ...
        [num2str(identSondeur) '_' nomFicSeul], ...
        [nomFicSeul '_WaterColumnDataAP_' num2str(iPing) '.mat']);
    flagFilesExist = exist(nomFicWaterColumnData.Mat, 'file') && exist(nomFicWaterColumnData.AP, 'file');
else
    nomFicWaterColumnData.AP = nomFicWaterColumnData.Mat;
    flagFilesExist = exist(nomFicWaterColumnData.Mat, 'file');
end
