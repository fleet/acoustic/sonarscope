
function DataDepth = Reson_BottomDetector_initDataDepth(N)

DataDepth.VehicleDepth      = NaN(N,1,'single');
DataDepth.Time              = [];
DataDepth.Heading           = NaN(N,1,'single');
DataDepth.TransducerDepth   = NaN(N,1,'single');
DataDepth.SurfaceSoundSpeed = NaN(N,1,'single');
DataDepth.Mode              = NaN(N,1,'single');
DataDepth.SampleRate        = NaN(N,1,'single');
DataDepth.Frequency         = NaN(N,1,'single');
DataDepth.Roll              = NaN(N,1,'single');
DataDepth.Pitch             = NaN(N,1,'single');
DataDepth.Heave             = NaN(N,1,'single');
DataDepth.Tide              = NaN(N,1,'single');
DataDepth.PingCounter       = NaN(N,1,'single');
DataDepth.Longitude         = NaN(N,1,'double');
DataDepth.Latitude           = NaN(N,1,'double');

