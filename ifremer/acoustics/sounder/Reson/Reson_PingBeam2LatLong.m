% Realisation de la mosaique de donnees des "DepthDatagrams" d'une campagne
%
% Syntax
%   [flag, Mosa, repImport, repExport] = Reson_PingBeam2LatLong(repImport, repExport, ...)
%
% Name-Value Pair Arguments
%   nomDir : Nom du r�pertoire des donnees
%   resol  : Pas de la grille en m�tres
%   Backup : P�riode de sauvegarde des fichiers de sortie
%   Layers : Liste des numeros de layers a mosaiquer (voir cl_reson_s7k/view_depth)
%            (Un seul layer pour le moment)
%   Rayon  : Rayon utilis� lors de l'interpolation
%
% Output Arguments
%   []     : Auto-plot activation
%   flag   : Description du parametre (unite).
%   a      : Instances de cl_image contenant la mosaique et la mosaique angulaire
%   nomDir : Nom du r�pertoire des images cr��es
%
% Examples
%   Reson_PingBeam2LatLong
%   flag, a] = Reson_PingBeam2LatLong('NomDir', '/home1/jackson/FEMME2005/Donnees', 'Resol', 10);
%   SonarScope(a)
%   [flag, a] = Reson_PingBeam2LatLong('NomDir', 'D:\NIWA_DATA\TAN0105', 'Resol', 10, 'Backup', 5);
%   SonarScope(a)
%
% See also Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function [flag, Mosa, repImport, repExport] = Reson_PingBeam2LatLong(repImport, repExport, varargin)

% TODO : disp('Supprimer l''interpolation de la mosaique de reception')

[varargin, XLim]        = getPropertyValue(varargin, 'XLim',        []);
[varargin, YLim]        = getPropertyValue(varargin, 'YLim',        []);
[varargin, resol]       = getPropertyValue(varargin, 'Resol',       []);
[varargin, Layers]      = getPropertyValue(varargin, 'Layers',      []);
% [varargin, Backup]    = getPropertyValue(varargin, 'Backup',      []);
[varargin, LimitAngles] = getPropertyValue(varargin, 'LimitAngles', []);
[varargin, window]      = getPropertyValue(varargin, 'window',      []); %#ok<ASGLU>

Mosa = cl_image; %GLT multi-image

S0 = cl_reson_s7k.empty();
E0 = cl_ermapper([]);

QL = get_LevelQuestion;

%% Choix du layer � mosa�quer

if isempty(Layers)
    listeLayers = getListeLayers(S0);
    str1 = 'Layer � mosa�quer';
    str2 = 'Layer to mosaic';
    [choixLayer, flag] = my_listdlg(Lang(str1,str2), listeLayers, ...
        'InitialValue', 12, 'SelectionMode', 'Multiple');
    if ~flag
        return
    end
    %     LayerName = listeLayers{choixLayer};
    LayerName = listeLayers(choixLayer); %GLT : pour mosa�que multi layer
end

%% Recherche des fichiers .s7k

[flag, liste] = uiSelectFiles('ExtensionFiles', '.s7k', 'RepDefaut', repImport);
if ~flag || isempty(liste)
    return
end
nbProfils = length(liste);
repImport = fileparts(liste{1});

[flag, resol] = get_gridSize('Value', -resol, 'Initial', 10);
if ~flag
    return
end

%% NEW : Utilisation d'un fichier de d�coupe GLT

if QL >= 1
    str1 = 'Voulez-vous utiliser une d�coupe horaire ?';
    str2 = 'Consider only some time intervals ?';
    [decoupeHoraire, flag] = my_questdlg(Lang(str1,str2), 'Init', 2);
    if flag && decoupeHoraire < 2
        [flag, decFile] = uiSelectFiles('ExtensionFiles', {'.txt'; '.cut'}, 'AllFilters', 1, 'RepDefaut', repImport);
        if ~flag || isempty(decFile)
            return
        else
            SonarTime = lecFicDecCar(decFile{1}); % lecture des heures de d�but et fin de profil;
            Ndecoupe = size(SonarTime, 1);
        end
    else
        decoupeHoraire = 0;
    end
else
    decoupeHoraire = 0;
end

%% Backup frequency

[flag, Backup] = saisie_backupFrequency(nbProfils);
if ~flag
    return
end

%% Si Mosaique de R�flectivit�, on propose de faire une compensation

[flag, CorFile] = QuestionCompensationCurve(repImport);
if ~flag
    return
end

%% Noms des fichiers de sauvegarde des mosaiques

DataType = cellfun(@identSonarDataType, repmat({S0},1,numel(LayerName)), LayerName); % GLT : pour choix multiple layer
NomGenerique = repmat({'Mosaic'}, 1, numel(LayerName));
[NomGenerique{DataType == cl_image.indDataType('Bathymetry')}] = deal('DTM');

for k=1:length(DataType) %multi-layer
    if k > 1
        lastpath = fileparts(nomFicErMapperLayer{k-1});
        nomFicErMapperLayer{k} = initNomFicErs(E0, NomGenerique{k}, ...
            cl_image.indGeometryType('LatLong'), DataType(k), lastpath, 'resol', resol); %#ok<AGROW>
    else
        [nomFicErMapperLayer{k}, flag] = initNomFicErs(E0,NomGenerique{k}, ...
            cl_image.indGeometryType('LatLong'), DataType(k), repExport, 'resol', resol); %#ok<AGROW>
        if ~flag
            return
        end
    end
end %multi-layer

[nomDir, TitreDepth] = fileparts(nomFicErMapperLayer{end});
DataType = identSonarDataType(S0, 'BeamDepressionAngle');
[nomFicErMapperAngle, flag] = initNomFicErs(E0, TitreDepth, cl_image.indGeometryType('LatLong'), DataType, nomDir, 'confirm', (QL >= 3));
if ~flag
    return
end
[repExport, TitreAngle] = fileparts(nomFicErMapperAngle);

%% Si Mosaique de Depth, on propose de faire la correction de mar�e

if any((choixLayer == 1) | (choixLayer == 25) | (choixLayer == 12))
    [flag, flagTideCorrection] = question_TideCorrection('flagS7K', 1);
    if ~flag
        return
    end
    if flagTideCorrection % Depth ou Depth-Heave
        ListeExension = {'*.mar;*.tide;*.tid;*.txt','MATLAB Files (*.mar,*.tide,*.tid,*.txt)';
            '*.mar',  'Tide file (*.mar)'; ...
            '*.tide','Tide file (*.tide)'; ...
            '*.tid','Tide file (*.tid)'; ...
            '*.txt','Tide file (*.txt)'; ...
            '*.*',  'All Files (*.*)'};
        [flag, TideFile] = my_uigetfile(ListeExension, 'Give a tide file (cancel instead)', repImport);
        if ~flag
            TideFile = [];
        end
    else
        TideFile = [];
    end
else
    TideFile = [];
end

%% Limites angulaires

[flag, LimitAngles] = question_AngularLimits('LimitAngles', LimitAngles, 'QL', 2);
if ~flag
    return
end

%% Filtre QF

if QL == 3
    str1 = 'Voulez-vous filter la donn�e en fonction du facteur de qualit� ?';
    str2 = 'Do you want to do filter the data according to the Quality Factor ?';
    [repFiltreQF, flag] = my_questdlg(Lang(str1,str2));
    if ~flag
        return
    end
    if repFiltreQF == 1
        str1 = 'Saisie du seuil du QF en dessous duquel on rejette les donn�es.';
        str2 = 'Definition of the QF threshold for which the data will be suppressed.';
        [flag, seuilQF] = inputOneParametre(Lang(str1,str2), Lang('Seuil', 'Threshold'), ...
            'Value', 2.85, 'MinValue', 0, 'MaxValue', 3);
        if ~flag
            return
        end
    end
else
    repFiltreQF = 2;
end

%% Filtre spike

if (QL == 3) && any((choixLayer == 1) | (choixLayer == 25) | (choixLayer == 12)) % Depth ou Depth-Heave
    str1 = 'Voulez-vous faire une correction de spikes ?';
    str2 = 'Do you want to do a spike removal ?';
    [repSpikeRemoval, flag] = my_questdlg(Lang(str1,str2), 'ColorLevel', 3);
    if ~flag
        return
    end
    if repSpikeRemoval == 1
        str1 = 'Correction de spikes';
        str2 = 'Spike removal';
        p    = ClParametre('Name', Lang('Seuil', 'Threshold'), 'Unit', 'Metres', 'MinValue', 0.1, 'MaxValue', 50, 'Value', 3);
        p(2) = ClParametre('Name', Lang('Marge', 'Margin'),    'Unit', 'Pixels', 'MinValue', 0,   'MaxValue', 10, 'Value', 0, 'Format', '%d');
        a = StyledParametreDialog('params', p, 'Title', Lang(str1,str2));
        a.sizes = [0 -2 0 0 0 -1 -1 0];
        a.openDialog;
        flag = a.okPressedOut;
        if ~flag
            return
        end
        value = a.getParamsValue;
        seuilSpike = value(1);
        MargeSpike = value(2);
    end
else
    repSpikeRemoval = 2;
end

%% Gestion du recouvrement entre profils GLT le 10/05/2016

[flag, CoveringPriority, TetaLimite] = question_LinesOverlapping;
if ~flag
    return
end

%% Filtre moustache

if (QL == 3) && any((choixLayer == 1) | (choixLayer == 25) | (choixLayer == 12))    % Depth ou Depth-Heave
    str1 = 'Voulez-vous faire une correction de moustache ?';
    str2 = 'Do you want to do a moustahe removal ?';
    [repMoustacheRemoval, flag] = my_questdlg(Lang(str1,str2), 'ColorLevel', 3);
    if ~flag
        return
    end
    if repMoustacheRemoval == 1
        str1 = 'Correction de moustache';
        str2 = 'Moustahe removal';
        [flag, AngleMoustache] = inputOneParametre(Lang(str1,str2), Lang('Angle max', 'Max Angle'), ...
            'Unit', 'Deg', 'Value', 12, 'MinValue', 0, 'MaxValue', 25);
        if ~flag
            return
        end
    end
else
    repMoustacheRemoval = 2;
end

%% Mosaic interpolation window

[flag, window] = question_MosaicInterpolationWindow(window);
if ~flag
    return
end

%% Cr�ation ou reprise de la mosa�que ?

Carto = [];
if any(cellfun(@exist, nomFicErMapperLayer, repmat({'file'}, 1, length(nomFicErMapperLayer)))) ||...
        exist(nomFicErMapperAngle, 'file')
    [flag, rep] = question_CompleteExistingMosaic;
    if ~flag
        return
    end
    if rep == 1
        for k=1:length(nomFicErMapperLayer)
            [flag, Z_Mosa_All{k}] = cl_image.import_ermapper(nomFicErMapperLayer{k}); %#ok<AGROW>
            if ~flag
                return
            end
        end
        Carto = get(Z_Mosa_All{end}, 'Carto');
        
        [flag, A_Mosa_All]  = cl_image.import_ermapper(nomFicErMapperAngle);
        if ~flag
            return
        end
    else
        Z_Mosa_All = cell(1,numel(LayerName));
        A_Mosa_All = [];
    end
else
    Z_Mosa_All = cell(1, numel(LayerName));
    A_Mosa_All = [];
end

%% Traitement des fichiers

% Bits = [6] % Nadir filter seulement
% Bits = [1 2]; % Brihtness et Colinearity seulement
Bits = 'AskIt';

str1 = 'Traitement : S7K PingBeam2LatLong';
str2 = 'Processing : S7K PingBeam2LatLong';
hw = create_waitbar(Lang(str1,str2), 'N', nbProfils);
%----------------------------------------------------------------------
%                       Boucle Fichiers
%----------------------------------------------------------------------
for kFic=1:nbProfils
    
    my_waitbar(kFic, nbProfils, hw)
    
    fprintf('\n--------------------------------------------------------------------------------------\n');
    str = sprintf('Processing file %d/%d : %s', kFic, nbProfils, liste{kFic});
    fprintf('%s\n', str);
    
    s7k = cl_reson_s7k('nomFic', liste{kFic}, 'KeepNcID', true);
    %     [b, Carto] = view_depth(s7k, 'LayerName', [24 2 3 4], 'Carto', Carto,
    %     'TideFile', TideFile); % Depth-Heave
    [cc, Carto, Bits] = view_depth(s7k, 'ListeLayers', [choixLayer 2 3 4 8], 'Carto', Carto, ...
        'TideFile', TideFile, 'Bits', Bits, 'setMaskToAllLayers', 1);
    %     Data7000 = read_sonarSettings(s7k, selectionSondeur(s7k)); % Pour compensation du power
    if isempty(cc)
        continue
    end
    
    %% Boucle Multi-layer

    Z = [];
    for iLayer=1:numel(choixLayer)
        c = [cc(iLayer), cc(numel(choixLayer)+1:end)];
        if length(c) < 5 %~= 5 % Modif GLT multi-layer
            str1 = sprintf('Les layers n�cessaires � cette op�ration n''ont pas �t� trouv�s pour le fichier %s', liste{kFic});
            str2 = sprintf('The expected layers were not found for file %s', liste{kFic});
            my_warndlg(Lang(str1,str2), 0);
            continue
        end
        switch choixLayer(iLayer)
            case 2
                c(1) = c(2);
            case 3
                c(1) = c(3);
            case 4
                c(1) = c(4);
        end
        
        Z = c(1);
        if Z.nbRows <= 1
            clear c
            Z = [];
            continue
        end
        
        AcrossDistance    = c(2);
        AlongDistance     = c(3);
        A                 = c(4);
        QF                = c(5);
        Heading           = get(Z, 'Heading');
        FishLatitude      = get(Z, 'FishLatitude');
        FishLongitude     = get(Z, 'FishLongitude');
%         SurfaceSoundSpeed = get(Z, 'SurfaceSoundSpeed');
%         SonarHeave        = get(Z, 'SonarHeave');
%         SonarImmersion    = get(Z, 'SonarImmersion');
        
        if all(isnan(FishLatitude)) || all(isnan(FishLongitude))
            str1 = sprintf('Le fichier %s ne contient pas de navigation, il doit s''agir d''un fichier provenant du 7KCenter.', liste{kFic});
            str2 = sprintf('%s file does not contain any navigation data, it seems to be a file coming from the 7KCenter.', liste{kFic});
            my_warndlg(Lang(str1,str2), 0, 'Tag', 's7kNavigationEmpty');
            continue
        end
        
        %% Gestion de la limite angulaire
        
        if ~isempty(LimitAngles)
            layerMasque = ROI_auto(A, 1, 'MaskReflec', A, [], LimitAngles, 1);
            A = masquage(A, 'LayerMask', layerMasque, 'valMask', 1);
            Z = masquage(Z, 'LayerMask', layerMasque, 'valMask', 1);
        end
        
        %% Filtre QF
        
        if repFiltreQF == 1
            Z = filterBathyQF(Z, QF, seuilQF);
        end
        
        %% Filtre spike
        
        if repSpikeRemoval == 1
            Z =  filterSpikeBathy(Z, seuilSpike, 'Margin', MargeSpike);
            Z = Z(1);
        end
        
        %% Filtre moustache
        
        if repMoustacheRemoval == 1
            [flag, Z] = Sonar_MoustacheRemoval(Z, A, AngleMoustache);
            if ~flag
                continue
            end
        end
        
        %% Compensation statistique
        
        for k=1:length(CorFile)
            CourbeConditionnelle = load_courbesStats(CorFile{k});
            bilan = CourbeConditionnelle.bilan;

            switch bilan{1}(1).DataTypeConditions{1}
                case 'TxAngle'
                    indConditionnel = 4;
                otherwise
                    c(5) =  sonar_secteur_emission(A, []);
                    indConditionnel = 5;
            end
            Z = compensationCourbesStats(c(1), c(indConditionnel), bilan);
        end
        
        %% Compensation horizontal MOZ4 pour la r�flectivit�(a supprimer) 7150
        
        %         if ~isdeployed
        %             % compensation POWER
        % %             Z = Z - repmat(Data7000.PowerSelection', 1, Z.nbColumns) ; % 7111
        %             % Z = Z - repmat(Data7000.GainSelection',  1, Z.nbColumns)/5 ; %7150
        %             % Compensation rampe de power
        %             DataType = Z.DataType;
        %             if DataType == cl_image.indDataType('Reflectivity')
        %                 [~, Z] = compensation(Z, 'Type', 6);
        %                 Z = Z(2);
        %             end
        %         end
    end
    if isempty(Z)
        continue
    end
    
    %% Recherche des pings dans la plage horaire
    
    suby   = 1:Z.nbRows;
    subyOK = false(size(suby));
    
    if decoupeHoraire && ~isempty(SonarTime)
        SonarTimeALL = get(Z, 'SonarTime');
        SonarTimeALL = SonarTimeALL.timeMat';
        for k=1:Ndecoupe % TODO
            subyOK = subyOK(:) | (...
                (SonarTimeALL >= SonarTime(k,1)) & ...
                (SonarTimeALL <= SonarTime(k,2)) );
        end
        clear SonarTimeALL
        if all(~subyOK)
            clear Z AcrossDistance AlongDistance A c subyOK
            continue
        end
    end
    
    %% Recherche des pings dans la zone d'�tude
    
    if isempty(XLim) || isempty(YLim)
        suby = 1:Z.nbRows;
    else
        SonarFishLatitude  = get(Z, 'SonarFishLatitude');
        SonarFishLongitude = get(Z, 'SonarFishLongitude');
        suby = find((SonarFishLongitude >= XLim(1)) & ...
            (SonarFishLongitude <= XLim(2)) & ...
            (SonarFishLatitude >= YLim(1)) & ...
            (SonarFishLatitude <= YLim(2)));
        clear SonarFishLatitude SonarFishLongitude
        if isempty(suby)
            clear Z AcrossDistance AlongDistance A c
            continue
        end
    end
    suby = suby(subyOK);
    
    %% Calcul des coordonn�es g�ographiques
    
    [flag, LatLon] = sonar_calcul_coordGeo_BathyFais(Z, AcrossDistance, AlongDistance, ...
        Heading, FishLatitude, FishLongitude, 'suby', suby);
    if ~flag
        %         return
        continue
    end
    
    [flag, Z_Mosa, A_Mosa] = sonar_mosaique2(Z, A, LatLon(1), LatLon(2), resol, 'AlongInterpolation',  1);
    if ~flag
        %         return
        continue
    end
    if isempty(A_Mosa)
        str1 = sprintf('Le fichier "%s" ne semble pas contenir le layer demand�.', liste{kFic});
        str2 = sprintf('"%s" does not seem to contain the required layer.', liste{kFic});
        my_warndlg(Lang(str1,str2), 0, 'Tag', 'NoLayer');
        continue
    end
    %     [flag, Z_Mosa] = sonar_mosaique(Z, LatLon(1), LatLon(2), resol, 'AlongInterpolation',  1);
    %     [flag, A_Mosa] = sonar_mosaique(A, LatLon(1), LatLon(2), resol, 'AlongInterpolation',  1);
    clear LatLon
    
    %% Interpolation
    %         Z_Mosa = WinFillNaN(Z_Mosa, 'window', [5 5]);
    %         A_Mosa = WinFillNaN(A_Mosa, 'window', [5 5]);
    %
    if ~isequal(window, [1 1])
        if Z_Mosa.DataType == cl_image.indDataType('RxTime')
            methodInterpolation = 'max';
        else
            methodInterpolation = 'mean';
        end
        Z_Mosa = WinFillNaN(Z_Mosa, 'window', window, 'NoStats', 'method', methodInterpolation);
        A_Mosa = WinFillNaN(A_Mosa, 'window', window, 'NoStats');
        %             if MosHeading
        %                 H_Mosa = WinFillNaNHeading(H_Mosa, 'window', window, 'NoStats');
        %             end
    end
    
    %% Assemblage de la mosq�que finale
    
    if iLayer == 1 %GLT
        [Z_Mosa_All{iLayer}, A_Mosa_All] = mosaique([Z_Mosa_All{iLayer} Z_Mosa], 'Angle', [A_Mosa_All A_Mosa], ...
            'FistImageIsCollector', 1, 'CoveringPriority', CoveringPriority, 'TetaLimite', TetaLimite);
        Z_Mosa_All{iLayer}.Name = TitreDepth;
        A_Mosa_All.Name = TitreAngle;
    else
        Z_Mosa_All{iLayer} = mosaique([Z_Mosa_All{iLayer} Z_Mosa], 'Angle', [A_Mosa_All A_Mosa], ...
            'FistImageIsCollector', 1, 'CoveringPriority', CoveringPriority, 'TetaLimite', TetaLimite);
        Z_Mosa_All{iLayer}.Name = TitreDepth;
    end
    clear c
    
    %% Sauvegarde des mosaiques dans des fichiers ErMapper
    
    if (mod(kFic,Backup) == 0) || (kFic == nbProfils)
        Z_Mosa_All{iLayer} = majCoordonnees( Z_Mosa_All{iLayer});
        %             A_Moza_All = majCoordonnees(A_Moza_All);
        export_ermapper(Z_Mosa_All{iLayer}, nomFicErMapperLayer{iLayer});
        % export_ermapper(A_Moza_All, nomFicErMapperAngle);
        if iLayer == 1 % GLT multi-layer
            A_Mosa_All = majCoordonnees(A_Mosa_All);
            export_ermapper(A_Mosa_All, nomFicErMapperAngle);
        end
    end
end % GLT : fin boucle layer

%% Suppression des fichier memmapfile

deleteFileOnListe(cl_memmapfile.empty, gcbf)

if isempty(Z_Mosa_All{1})
    flag = 0;
    return
else
    % GLT Multi-layer
    for iLayer = 1:numel(choixLayer)
        Mosa(iLayer) = Z_Mosa_All{iLayer};
    end
    Mosa(end+1) = A_Mosa_All;
    % /GLT Multi-layer
    
    my_close(hw, 'MsgEnd')
    flag = 1;
end
