function [PhaseImage2, nbSamplesMoyennes] = Reson_phaseFilter(PhaseImage, AmpImage, RMax1, MaskWidth)

nbSamples = size(AmpImage, 1);
nbBeams   = size(AmpImage, 2);

PhaseImage = PhaseImage * (pi/180);
a = AmpImage .* cos(PhaseImage);
b = AmpImage .* sin(PhaseImage);

aF = a;
bF = b;
n = 4;
for iBeam=1:nbBeams
    if isnan(RMax1(iBeam))
        continue
    end
    ideb = RMax1(iBeam) - 2*MaskWidth(iBeam);
    ifin = RMax1(iBeam) + 2*MaskWidth(iBeam);
    ideb = max(ideb,n);
    ifin = min(ifin,nbSamples-n);
    % n2 = 2;h4 = repmat(1/(2*n2+1), (2*n2+1), 1); figure; PlotUtils.createSScPlot(aF(:,iBeam), '-+b'); grid on; hold on; PlotUtils.createSScPlot(conv(double(aF(:,iBeam)), h4, 'same'), '-xr');
    for iSample=ideb:ifin
        sub = (iSample-n):(iSample+n);
        aFSub = aF(sub,iBeam);
        bFSub = bF(sub,iBeam);
        subsub = ~isnan(aFSub);
        aF(iSample,iBeam) = mean(aFSub(subsub));
        bF(iSample,iBeam) = mean(bFSub(subsub));
    end
end

PhaseImage2 = atan2(bF, aF) * (180/pi);
nbSamplesMoyennes = 2*n + 1;

% pppp = PhaseImage2 - PhaseImage;
% SonarScope(pppp)
