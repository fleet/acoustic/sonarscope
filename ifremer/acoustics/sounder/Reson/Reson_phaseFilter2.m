function [PhaseImage2, nbSamplesMoyennes] = Reson_phaseFilter2(PhaseImage, AmpImage, RMax1, MaskWidth)

nbSamples = size(AmpImage, 1);
nbBeams   = size(AmpImage, 2);

PhaseImage = PhaseImage * (pi/180);
a = AmpImage .* cos(PhaseImage);
b = AmpImage .* sin(PhaseImage);

aF = a;
bF = b;
n = 2;
h4 = repmat(1/(2*n+1), (2*n+1), 1);
for iBeam = 1:nbBeams
    if isnan(RMax1(iBeam))
        continue
    end
    ideb = RMax1(iBeam) - 2*MaskWidth(iBeam);
    ifin = RMax1(iBeam) + 2*MaskWidth(iBeam);  
    ideb = max(ideb,n);
    ifin = min(ifin,nbSamples-n);
    
    sub = ideb:ifin;
    
    x = aF(sub,iBeam);
    x = x(:);
    x = x';
    aF(sub,iBeam) = conv(double(x), h4, 'same');
%     figure; PlotUtils.createSScPlot(x, '-+b'); grid on; hold on; PlotUtils.createSScPlot(conv(double(x), h4, 'same'), '-xr');
    
    x = bF(sub,iBeam);
    x = x(:);
    x = x';
    bF(sub,iBeam) = conv(double(x), h4, 'same');
end

PhaseImage2 = atan2(bF, aF) * (180/pi);

nbSamplesMoyennes = 4*n + 1;
% pppp = PhaseImage2 - PhaseImage;
% SonarScope(pppp)
