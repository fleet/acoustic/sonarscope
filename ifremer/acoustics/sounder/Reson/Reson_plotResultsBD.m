% nbSamples = size(AmpImage, 1);
% ImageName = extract_ImageName(Amplitude);

function Reson_plotResultsBD(nbSamples, PhasePingRange, AmpPingQF, PhasePingQF, SampleRate, TD, ...
    ImageName, iBeamBeg, iBeamEnd)

Coef = 1500 / (2 * SampleRate);
subAmp = find(TD == 1);
subPh  = find(TD == 2);
subAmpKO = find((TD == 1) & (AmpPingQF   < 2.5));
subPhKO  = find((TD == 2) & (PhasePingQF < 2.5));

%     figure(12321);
Fig = FigUtils.createSScFigure;
set(Fig, 'Name', ImageName)

h(1) = subplot(5,2,1);
hc = PlotUtils.createSScPlot(AmpPingRange, 'k');
set(hc, 'Tag', 'BottomDetector', 'UserData', ImageName);
hold on; grid on;
hc = PlotUtils.createSScPlot(PhasePingRange, 'r');
set(hc, 'Tag', 'BottomDetector', 'UserData', ImageName);
set(h(1), 'YDir', 'Reverse');
set(h(1), 'YLim', [0 nbSamples]);
title('Range');
hold off;
%{
    MasqueAmp
%}

h(2) = subplot(5,2,2);
hc = PlotUtils.createSScPlot(subAmp, AmpPingRange(subAmp), 'k+');
set(hc, 'Tag', 'BottomDetector', 'UserData', ImageName);
hold on; grid on;
hc = PlotUtils.createSScPlot(subPh,  PhasePingRange(subPh), '+r');
set(hc, 'Tag', 'BottomDetector', 'UserData', ImageName);
set(h(2), 'YDir', 'Reverse');
set(h(1), 'YLim', [0 nbSamples]);
title('Range');
PlotUtils.createSScPlot(subPhKO,  PhasePingRange(subPhKO), 'og');
PlotUtils.createSScPlot(subAmpKO, AmpPingRange(subAmpKO), 'og');
hold off;

h(3) = subplot(5,2,3);
PlotUtils.createSScPlot(-(AmpPingRange * Coef) .* cosd(BeamAngles), 'k');
hold on; grid on;
PlotUtils.createSScPlot(-(PhasePingRange * Coef) .* cosd(BeamAngles), 'r');
title('Z'); hold off;

h(4) = subplot(5,2,4);
Z = -(AmpPingRange(subAmp) * Coef) .* cosd(BeamAngles(subAmp));
PlotUtils.createSScPlot(subAmp, Z-Draught, 'k+');
hold on; grid on;
Z = -(AmpPingRange(subAmpKO) * Coef) .* cosd(BeamAngles(subAmpKO));
PlotUtils.createSScPlot(subAmpKO, Z, 'og');

Z = -(PhasePingRange(subPh) * Coef) .* cosd(BeamAngles(subPh));
PlotUtils.createSScPlot(subPh, Z-Draught, '+r');
Z = -(PhasePingRange(subPhKO) * Coef) .* cosd(BeamAngles(subPhKO));
PlotUtils.createSScPlot(subPhKO,  Z, 'og');
title('Z'); hold off;

h(5) = subplot(5,2,5);
PlotUtils.createSScPlot(AmpPingQF, 'k'); grid on;
hold on; grid on;
PlotUtils.createSScPlot(PhasePingQF, 'r'); grid on;
hold off; title('PingQF');
%     set(h(5), 'YLim', [min([AmpPingQF PhasePingQF]) max([4, max(PhasePingQF), max(AmpPingQF)])])

h(6) = subplot(5,2,6);
PlotUtils.createSScPlot(subAmp, AmpPingQF(subAmp), 'k+'); grid on;
hold on; grid on;
PlotUtils.createSScPlot(subAmpKO, AmpPingQF(subAmpKO), 'og');
PlotUtils.createSScPlot(subPh,  PhasePingQF(subPh), '+r'); grid on;
PlotUtils.createSScPlot(subPhKO,  PhasePingQF(subPhKO), 'og');
hold off; title('PingQF');
set(h(6), 'YLim', [2 max([4, max(PhasePingQF(subPh)), max(AmpPingQF(subAmp))])])

h(7) = subplot(5,2,7);
PlotUtils.createSScPlot(AmpPingSamplesNb, 'k+'); grid on;
title('AmpPingSamplesNb');

h(8) = subplot(5,2,8);
PlotUtils.createSScPlot(AmpPingQF2, 'k+'); grid on;
hold off; title('AmpPingQF2');

h(9) = subplot(5,2,9);
PlotUtils.createSScPlot(PhasePingNbSamples, 'k+'); grid on;
hold off; title('PhasePingNbSamples');

h(10) = subplot(5,2,10);
PlotUtils.createSScPlot(BeamAngles, 'k+'); grid on;
hold off; title('BeamAngles');

for i=1:length(h)
    axes(h(i));
    hold on;
    plot([iBeamBeg iBeamBeg], get(h(i), 'YLim'), 'g');
    plot([iBeamEnd iBeamEnd], get(h(i), 'YLim'), 'g');
    hold off
end

linkaxes(h, 'x')

% if SystemSerialNumber == 7111
%     Correction = ResonCorrectionAntenna7111(BeamAngles, SampleRate);
%     AmpPingRangeOnImage   = AmpPingRange - Correction;
%     PhasePingRangeOnImage = PhasePingRange - Correction;
%     PingRangeOnImage      = PingRange - Correction;
% else
%     AmpPingRangeOnImage   = AmpPingRange;
%     PhasePingRangeOnImage = PhasePingRange;
%     PingRangeOnImage      = PingRange;
% end


%     figure(12317);
Fig = FigUtils.createSScFigure;
ImageName = extract_ImageName(Amplitude);
set(Fig, 'Name', ImageName)

XAmp =  (AmpPingRange * Coef) .* sind(BeamAngles);
ZAmp = -(AmpPingRange * Coef) .* cosd(BeamAngles);
hc = PlotUtils.createSScPlot(XAmp(subAmp), ZAmp(subAmp)-Draught, '+k');
set(hc, 'Tag', 'BottomDetector', 'UserData', ImageName);
hold on; grid on;
PlotUtils.createSScPlot(XAmp(subAmpKO), ZAmp(subAmpKO)-Draught, 'og');

XPh =  (PhasePingRange * Coef) .* sind(BeamAngles);
ZPh = -(PhasePingRange * Coef) .* cosd(BeamAngles);
hc = PlotUtils.createSScPlot(XPh(subPh), ZPh(subPh)-Draught, '+r');
set(hc, 'Tag', 'BottomDetector', 'UserData', ImageName);
PlotUtils.createSScPlot(XPh(subPhKO), ZPh(subPhKO)-Draught, 'og');

iBeamBeg = max(iBeamBeg, find(~isnan(XPh) | ~isnan(XAmp), 1, 'first'));
if isnan(XPh(iBeamBeg))
    h = PlotUtils.createSScPlot([0 XAmp(iBeamBeg)], [0 ZAmp(iBeamBeg)-Draught], 'g');
else
    h = PlotUtils.createSScPlot([0 XPh(iBeamBeg)], [0 ZPh(iBeamBeg)-Draught], 'g');
end
set(h, 'LineWidth', 3)
set(h, 'Tag', 'BottomDetector')

iBeamEnd = min(iBeamEnd, find(~isnan(XPh) | ~isnan(XAmp), 1, 'last'));
if isnan(XPh(iBeamEnd))
    h = PlotUtils.createSScPlot([0 XAmp(iBeamEnd)], [0, ZAmp(iBeamEnd)-Draught], 'g');
else
    h = PlotUtils.createSScPlot([0 XPh(iBeamEnd)], [0, ZPh(iBeamEnd)-Draught], 'g');
end
set(h, 'LineWidth', 3)
set(h, 'Tag', 'BottomDetector')
title('Bathy');

%% Plot des tol�rances (QF ramen� en �cart-type) autour de la d�tection

dZsurZ = 10.^(-QualityFactor);
Z1     = NaN(size(QualityFactor), 'single');
Z2     = NaN(size(QualityFactor), 'single');
Z1(subAmp) = ZAmp(subAmp) .* (1+2*dZsurZ(subAmp));
Z2(subAmp) = ZAmp(subAmp) .* (1-2*dZsurZ(subAmp));
Z1(subPh) = ZPh(subPh) .* (1+2*dZsurZ(subPh));
Z2(subPh) = ZPh(subPh) .* (1-2*dZsurZ(subPh));

Xp = NaN(size(Z1));
Z1p = NaN(size(Z1));
Z2p = NaN(size(Z1));
Xp(subAmp) = XAmp(subAmp);
Z1p(subAmp) = Z1(subAmp);
Z2p(subAmp) = Z2(subAmp);
PlotUtils.createSScPlot(Xp, Z1p, 'k');
PlotUtils.createSScPlot(Xp, Z2p, 'k');
Xp = NaN(size(Z1));
Z1p = NaN(size(Z1));
Z2p = NaN(size(Z1));
Xp(subPh) = XPh(subPh);
Z1p(subPh) = Z1(subPh);
Z2p(subPh) = Z2(subPh);
PlotUtils.createSScPlot(Xp, Z1p, 'r');
PlotUtils.createSScPlot(Xp, Z2p, 'r');
hold off;