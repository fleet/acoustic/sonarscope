function flag = Reson_testLengthPhase(x, NechRampMin)

% figure; plot(x, '*'); grid on;
n = length(x);
% flag = (n < NechRampMin) || ((n < 50) && (((x(end)-x(1))/n) > 1.7));

% Brest 11/07/2008 for fichier 20080219_231309 Ping 148 Beam 62 : this
% modification is due to NechRampMin = 7; in
% ResonDetectionPhasePing_3steps : Warning, this was done for 7111, is
% there possible to get a side effect on 7150 ?
flag = (n < NechRampMin) || ((n < 50) && (((x(end)-x(1))/n) > 2));
