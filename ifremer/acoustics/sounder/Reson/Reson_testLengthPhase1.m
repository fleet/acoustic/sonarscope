function flag = Reson_testLengthPhase1(x, NechRampMin)

% figure; plot(x, '*'); grid on;
n = length(x);
flag = (n < NechRampMin);
