% Prelecture des fichiers .s7k : Nettoyage a partir de l'analyse histogrammique de Range 
%
% Syntax
%   status = S7K_CleanBathyAndRange(liste)
% 
% Input Arguments 
%   liste : Nom de fichier ou liste de noms
%
% Output Arguments 
%   status : 0 si arret par Cancel, 1 sinon
%
% Examples
%   nomFic = getNomFicDatabase('EM1002_0001_20040916_054013_raw.s7k')
%   S7K_CleanBathyAndRange(nomFic);
%
%   nomDir = fileparts(nomFic)
%   liste = listeFicOnDepthDir(nomDir, '.s7k')
%   S7K_CleanBathyAndRange(liste);
%
%   % nomDir = '/home1/jackson/MarcRoche'
%   % liste = listeFicOnDepthDir(nomDir, '.s7k')
%   % save /home1/jackson/MarcRoche/listeFic.mat liste
% 
%   while ~isempty(liste)
%       status = S7K_CleanBathyAndRange(liste{1});
%       if status
%           liste(1) = [];
%           save listeFic liste
%       else
%           break
%       end
%   end
%
% See also cl_simrad_all Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function status = S7K_CleanBathyAndRange(varargin)

global useCacheNetcdf %#ok<GVMIS>

status = 1;
if nargin == 0
    [flag, nomDir] = my_uigetdir(pwd, 'Pick a Directory');
    if ~flag
        return
    end
    liste = listeFicOnDepthDir(nomDir, '.s7k');
else
    liste = varargin{1};
end

if ~iscell(liste)
    liste = {liste};
end

FirstTime = 1;
ScreenSize = get(0, 'ScreenSize');
fig = figure('Position', centrageFig(ScreenSize(3)-100, ScreenSize(4)-150));
Bits = [1 2]; % Brightness et collinearity
for k=1:length(liste)
    OK = 2;
    [nomDir, nomFic] = fileparts(liste{k});
    
    s7k = cl_reson_s7k('nomFic', liste{k}, 'KeepNcID', true);
        
    [flag, Data, identSondeur] = read_depth_s7k(s7k);
    if ~flag
        continue
    end
    
    DepthMask = ones(size(Data.Depth), 'single');
    for k2=1:length(Bits)
        Val = 2^(Bits(k2)-1);
        K = single(bitand(uint8(Data.Mask),Val)) / Val;
        subK = (K ~= 1);
        DepthMask(subK) = NaN;
        clear subK
    end
    
    Range      = Data.Range;
    Depth      = Data.Depth;
    AcrossDist = Data.AcrossDist;
    while OK == 2
        disp(liste{k})
        figure(fig)

        subplot(1,3,1)
        imagesc(Range .* DepthMask); colorbar; axis xy; colormap(jet(256));
        title('Range')

        subplot(1,3,2)
        imagesc(Depth .* DepthMask); colorbar; axis xy; colormap(jet(256));
        title([nomFic ' Depth'], 'interpreter', 'none')

        subplot(1,3,3)
        imagesc(AcrossDist .* DepthMask); colorbar; axis xy; colormap(jet(256));
        title([nomFic ' AcrossDist'], 'interpreter', 'none')

        if FirstTime
            if my_verLessThanR2014b % R2014b
                str = sprintf('Resize the figure %d if necessary', fig);
            else
                str = sprintf('Resize the figure %d if necessary', fig.Number);
            end
            my_warndlg(str, 1);
            FirstTime = 0;
        end


        %             'Les bornes Min et Max de l''image sont-elles correctes'
        [OK, flag] = my_questdlg(Lang('Voyez-vous toutes les couleurs de l''image "Range" ?', ...
            'Do you see all the colors on the "Range" image ?'));
        if ~flag
            return
        end
        if OK == 2
            I = Range;
        else
            [OK, flag] = my_questdlg(Lang('Voyez-vous toutes les couleurs de l''image "Depth" ?', ...
                'Do you see all the colors on the "Depth" image ?'));
            if ~flag
                return
            end
            if OK == 2
                I = Depth;
            else
                [OK, flag] = my_questdlg(Lang('Voyez-vous toutes les couleurs de l''image "AcrossDist" ?', ...
                    'Do you see all the colors on the "AcrossDist" image ?'));
                if ~flag
                    return
                end
                if OK == 2
                    I = AcrossDist;
                end
            end
        end

        figure(fig)
        if OK == 1
            continue
        end

        bins = linspace(min(I(:)), max(I(:)), 200);

        [N, bins, ~, SubBins] = histo1D(I, bins);
        sub = getFlaggedBinsOfAnHistogram(bins, N);

        %% On recup�re les modifications

%         MaxRangePrecedent  = max(Range(:));
        for k2=1:length(sub)
            subPoubelle = SubBins{sub(k2)};
            
%             % Comment� par JMA le 14/01/2021 : pas si simple, je reviens en arri�re
            Range(subPoubelle)      = NaN;
            Depth(subPoubelle)      = NaN;
            AcrossDist(subPoubelle) = NaN;
            Data.Mask(subPoubelle) = 0; % Rajout JMA le 14/01/2021
        end
        if ~isempty(sub)
            nomFicMat  = fullfile(nomDir, 'SonarScope', [num2str(identSondeur) '_' nomFic '_depth.mat']);
            ncFileName = fullfile(nomDir, 'SonarScope', [nomFic '.nc']);
            try
                if useCacheNetcdf == 1 % 1=Mat, 2 et 3=Netcdf
                    save(nomFicMat, 'Data')
                else
                    [Data.Dimensions.nbPings, Data.Dimensions.nbBeams] = size(Data.BeamDepressionAngle);
                    NetcdfUtils.Struct2Netcdf(Data, ncFileName, 'Depth');
                end

%                 MaxRange = max(Range(:));
%                 if MaxRange ~= MaxRangePrecedent
%                 end
            catch
                str = sprintf('Erreur ecriture fichier %s', nomFicMat);
                my_warndlg(str, 1);
            end
        end
    end
end
my_close(fig)
