function [flag, nomFicNav, nomFicIP, repImport] = S7K_NavigationFileAndInstallationParameters(repImport, varargin)

[varargin, Init] = getPropertyValue(varargin, 'Init', 2); %#ok<ASGLU>

nomFicNav = [];
nomFicIP  = [];

%% Fichier de navigation

str1 = 'Disposez-vous d''un fichier de navigation au cas o� ce fichier .s7k ne contiendrait pas de navigation (cas des fichiers sortant du 7KCenter) ?';
str2 = 'Do you have a navigation file in case this .s7k file would have been produced by the 7KCenter ?';
[rep, flag] = my_questdlg(Lang(str1,str2), 'Init', Init);
if ~flag
    return
end
if rep == 1
    str1 = 'SonarScope - Entrez le nom de fichier de navigation au format Caraibes (.nvi)';
    str2 = 'SonarScope - Please select the navigation file (Caraibes format .nvi)';
    [flag, nomFicNav] = uiSelectFiles('Entete', Lang(str1,str2), 'ExtensionFiles', {'.nvi'; '.txt'; '.csv'}, ...
        'AllFilters', 1, 'RepDefaut', repImport);
    if ~flag
        return
    end
    repImport = fileparts(nomFicNav{1});
end

%% Fichier de Installation Parameters

str1 = 'Disposez-vous d''un fichier XML de "Installation Parameters" ?';
str2 = 'Do you have an "Installation Parameters" XML file ?';
[rep, flag] = my_questdlg(Lang(str1,str2), 'Init', Init);
if ~flag
    return
end
if rep == 1
    str1 = 'SonarScope - Entrez le nom de fichier "InstallationParameters"';
    str2 = 'SonarScope - Please select the "InstallationParameters" file';
    [flag, nomFicIP] = uiSelectFiles('Entete', Lang(str1,str2), 'ExtensionFiles', '.xml', ...
        'ChaineIncluse', 'InstallationParameters', 'RepDefaut', repImport);
    if ~flag
        return
    end
    repImport = fileparts(nomFicIP{1});
end

flag = 1;
