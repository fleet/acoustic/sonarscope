% Cr�ation d'un fichier HAC � partir d'une arborescence SSC_ en provenance
% d'un fichier ou de plusieurs fichiers S7K.
%
% Syntax
%   S7K_ssc2hac(...)
%
% Name-Value Pair Arguments
%   nomFic      : Nom d'un fichier ou liste de fichiers .s7k.
%   nomDir      : Nom du r�pertoire
%   SeuilSample : seuil de compression (-64 dB par d�faut)
%
% Examples
%   TODO
%
%   flag = S7K_ssc2hac('NomFicAll', nomFic{5});
%   flag = S7K_ssc2hac('NomFicAll', nomFic, 'RepDefaut', 'C:\Temp\Test_HAC_Creation');
%
%   nomDir  = 'F:\SonarScopeData\From AndreOgor';
%   flag    = S7K_ssc2hac('NomDir', nomDir, 'RepDefaut', 'C:\Temp\Test_HAC_Creation');
%
% See also Authors
% Authors : GLU
%-----------------------------------------------------------------------

function flag = S7K_ssc2hac(varargin)

flag = 0;
[varargin, nomDir] = getPropertyValue(varargin, 'NomDir',    []);
[varargin, nomFic] = getPropertyValue(varargin, 'NomFicAll', []);
% Seuil = -64 dB pour les EM, passage � -100 dB pour les autres
[varargin, seuilSampledB] = getPropertyValue(varargin, 'SeuilSample',       -100);
[varargin, dInterfero]    = getPropertyValue(varargin, 'DistanceInterfero', 3.9);
[varargin, RepDefaut]     = getPropertyValue(varargin, 'RepDefaut',         my_tempdir);

if isempty(nomDir)
    if isempty(nomFic)
        [flag, nomFic] = uiSelectFiles('ExtensionFiles', '.s7k', 'RepDefaut', RepDefaut);
    else
        if ischar(nomFic)
            nomFic = {nomFic};
        end
    end
else
    nomFic = listeFicOnDir('C:\IfremerToolboxDataPublic\Level1', '.s7k');
end

path = fileparts(nomFic{1});
if isempty(RepDefaut)
    [flag, pathOut] = my_uigetdir(path);
    if ~flag
        return
    end
else
    pathOut = RepDefaut;
end

N    = length(nomFic);
str1 = 'Conversion des fichiers .s7k en .hac';
str2 = 'Converting .s7k files into .hac';
hw   = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    
    flag = S7K_ssc2hac_unitaire(nomFic{k}, seuilSampledB, pathOut, 'DistanceInterfero', dInterfero);
    if ~flag
        return
    end
end
my_close(hw, 'MsgEnd')

flag = 1;


function flag = S7K_ssc2hac_unitaire(nomFic, seuilSampledB, DirOut, varargin)

global DEBUG %#ok<GVMIS> 

[varargin, dInterfero] = getPropertyValue(varargin, 'DistanceInterfero', 4);

s7k = cl_reson_s7k('nomFic', nomFic, 'KeepNcID', true);

%% Lecture des descriptions de datagrammes pour indexation de ceux-ci.

[pathFile, rootFicName] = fileparts(nomFic);

nomFicHac = fullfile(DirOut, [rootFicName '_s7k2hac.hac']);
if exist(nomFicHac, 'file')
    str1 = 'Le fichier HAC existe d�j�. Voulez-vous l''�craser ?';
    str2 = 'Do you really want to overwrite existing file ?';
    [rep, validation] = my_questdlg(Lang(str1,str2), 'Init', 1, 'WaitingTime', 10); %#ok<*ASGLU>
    if isempty(rep) || rep == 2
        return
    end
end

% nameDatagramsSSC = {'Attitude', 'Position', 'WaterColumn', 'SoundSpeed', 'RunTime', 'InstallationParameters'};
nameDatagramsSSC = {'Attitude', 'Position', 'WaterColumn', 'SoundSpeed', 'RunTime', 'InstallationParameters', 'MagAndPhase'};

identSondeur = selectionSondeur(s7k);
% Composition du r�pertoire tenant compte du type de sondeur.
nomDir     = fullfile(pathFile, 'SonarScope', [num2str(identSondeur) '_' rootFicName]);
minLp      = 1;
maxLp      = numel(nameDatagramsSSC);
sDataDepth = [];
sData7004  = [];

%% Lecture du fichier d'index

nomFicIndex = setNomFicIndex(s7k, identSondeur);
fidIndex    = fopen(nomFicIndex, 'r', 'l');
if fidIndex == -1
    str = ['Impossible d''ouvrir le fichier ' nomFicIndex];
    my_warndlg(str, 1);
    return
end

Index = fread(fidIndex, [8 Inf], 'uint32');
fclose(fidIndex);

Date             = Index(1,:); %#ok<*NASGU>
Heure            = Index(2,:);
Position         = Index(3,:);
Taille           = Index(4,:);
TypeDatagram     = Index(5,:);
PingCounter      = Index(6,:);
PingSequence     = Index(7,:);
DeviceIdentifier = Index(8,:);

% D�tection de pr�sence des Mag&Phase.
isAny7018     = any(TypeDatagram == 7018);
isAnyPosition = any(TypeDatagram == 7015) || any(TypeDatagram == 1003);
isAny7030     = any(TypeDatagram == 7030);
isAnyAttitude = any(TypeDatagram == 1016) || any(TypeDatagram == 1012);
isAny7041     = any(TypeDatagram == 7041);

%% Boucle de lecture des datagrammes.

for lp=minLp:maxLp
    % Chargement des datagrammes identifi�s.
    sTmpData = [];
    switch nameDatagramsSSC{lp}
        case 'Attitude'                 % HAC : 10140, S7K : 1016
            if isAnyAttitude
                sTmpData = read_attitude(s7k, identSondeur);
            end
            
        case 'Position'                 % HAC : 20,  S7K : 1015
            if isAnyPosition
                sTmpData = read_navigation(s7k, identSondeur);
            end
            
        case 'SoundSpeed'               % HAC : 901, 1010
            sTmpData = read_soundSpeedProfile(s7k, identSondeur);
            
        case 'InstallationParameters'   % HAC : 9001, S7K  : 7030
            if isAny7030
                sTmpData = read_sonarInstallationParameters(s7k, identSondeur);
            end
            
        case 'WaterColumn'              % HAC : 10040, S7K  : 7041, 7000 (SonarSettings)
            [flag, sTmpData, sBeamDataOutWC, subPingWC] = read_WaterColumnData(s7k, identSondeur, 'Display', 0, 'ConserveValueZero');
            
            % Chargement des donn�es de d�tection de fond pour compl�ter les donn�es de WaterColumn.
            if isempty(sDataDepth)
                [flag, sDataDepth] = reloadDataDepthForWatercolumn(s7k);
                if ~flag
                    return
                end
                % Sauvegarde des datagrammes de Depth qui contiennent des donn�es d'acquisition (Freq, Angles des pointages, ...)
                sDataFile.Depth = sDataDepth;
            end
            
            if isempty(sData7004)
                % Lecture de datagramme Beam Geometry pour �tude et analyse.
                sData7004 = read_beamGeometry_7004(s7k, identSondeur);
            end
            
            % Chargement des donn�es li�es au datagramme de Depth  et � la donn�e WC.
            N   = length(sBeamDataOutWC); % Modifi� par JMA le 26/09/2014
            hw  = create_waitbar('Loading Amplitude & Phase files : 2/2', 'N', N);
            for k=1:N
                my_waitbar(k, N, hw)
                
                % R�cup�ration de la SampleRate des Watercolumn dans le datagramme associ�.
                % En sortie de read_WaterColumnData.m, le sTmpData est un panach� de DataDepth et DataWC.
                sTmpData.SampleRate(k)    = sBeamDataOutWC(k).RTH.SampleRate;
                sTmpData.RangeBottom(k,:) = sBeamDataOutWC(k).RD.Range;
                sTmpData.NbSamples(k,:)   = sBeamDataOutWC(k).RD.NbSamples;
                
                % Pour info : La r�cup�ration de l'amplitude s'effectue dans la gestion des tuples 10040 en pointant sur
                % les fichiers de cache de la conversion S7K2HAC.
            end
            my_close(hw)
            
            
        case 'MagAndPhase'              % HAC : 10030, S7K  : 7018, 7058, 7000 (SonarSettings)
            if isAny7018
                [sTmpData, sBeamDataOutMagAndP, subPingRawData]  = read_BeamRawData(s7k, identSondeur, 'Display', 0);
                
                % Chargement des donn�es de d�tection de fond pour compl�ter les donn�es de WaterColumn.
                if isempty(sDataDepth)
                    [flag, sDataDepth] = reloadDataDepthForWaterColumn(s7k);
                    if ~flag
                        return
                    end
                    % Sauvegarde des datagrammes de Depth qui contiennent des donn�es d'acquisition (Freq, Angles des pointages, ...)
                    sDataFile.Depth = sDataDepth;
                end
                
                if isempty(sData7004)
                    % Lecture de datagramme Beam Geometry pour �tude et analyse.
                    sData7004 = read_beamGeometry_7004(s7k, identSondeur);
                end
                % Chargement des donn�es li�es au datagramme de Depth  et � la donn�e WC.
                N   = length(sBeamDataOutMagAndP); % Modifi� par JMA le 26/09/2014
                hw  = create_waitbar('Loading Amplitude & Phase files : 2/2', 'N', N);
                for k=1:N
                    my_waitbar(k, N, hw)
                    
                    sTmpData.RangeBottom(k,:) = sBeamDataOutMagAndP(k).RD.Range * sTmpData.SampleRate(k);
                    sTmpData.NbSamples(k)     = sBeamDataOutMagAndP(k).RTH.Samples;
                    
                    % Pour info : La r�cup�ration de l'amplitude s'effectue dans la gestion des tuples 10040 en pointant sur
                    % les fichiers de cache de la conversion S7K2HAC.
                end
                my_close(hw)
            end
            
            
    end
    
    % Aggr�gation des datas S7K dans la structure � convertir pour le format HAC
    sDataFile.(nameDatagramsSSC{lp}) = sTmpData;
    
end

%% test si il y a bien du WC dans ce fichier

if ~isfield(sDataFile, 'WaterColumn')
    str1 = sprintf('D�sol�, le fichier "%s" ne contient pas de "Water Column".', nomFic);
    str2 = sprintf('Sorry, "%s" do not contain any Water Column data.', nomFic);
    my_warndlg(Lang(str1,str2), 0, 'Tag', 'NoWCInThisFile');
    flag = 1;
    return
end

%% Reconstruction des n� de pings/s�quence des paquets Depth et WC.

% Traitement d'unicit� des Pings/Multi-Pings : on recompose les couples N� Ping/N� Multiping
tabWCResonPingCounter = num2cell(arrayfun(@(x) (x.RTH.PingCounter), sBeamDataOutWC));

tabWCResonMultiPingSequence = sDataFile.WaterColumn.MultiPingSequence(sDataFile.WaterColumn.subWC);
% Incr�mentation du tableau si on passe en Monoping � un moment ou un
% autre afin d'utiliser le MultiPingSequence comme indice de tableau.
if any(tabWCResonMultiPingSequence == 0)
    tabWCResonMultiPingSequence = tabWCResonMultiPingSequence + 1;
end
tabWCResonMultiPingSequence = num2cell(tabWCResonMultiPingSequence);


% Tri des paquets par Date.
WCTime        = num2cell(sDataFile.WaterColumn.Time(sDataFile.WaterColumn.subWC));
WCPingCounter = num2cell(sDataFile.WaterColumn.PingCounter(sDataFile.WaterColumn.subWC));

tabResonMAndPPingCounter = [];
if isAny7018
    tabResonMAndPPingCounter = num2cell(arrayfun(@(x) (x.RTH.PingCounter), sBeamDataOutMagAndP));
end

%% Paquet InstallParams (HAC : 9001, S7K : 7030)
nomFicIP = [];
if isempty(sDataFile.InstallationParameters)
    %{
nomFicIP = 'E:\SonarScopeAnnexes\SonarScopeDataArchive\From Carla\S7K2HAC\7041F_7018\InstallationParameters_24kHz.xml';
sDataFile.InstallationParameters = xml_read(nomFicIP);
    %}
    % Chargement d'un fichier de Installation Parameters annexe
    
    str1 = 'Disposez-vous d''un fichier XML de "Installation Parameters" ?';
    str2 = 'Do you have an "Installation Parameters" XML file ?';
    [rep, flag] = my_questdlg(Lang(str1,str2), 'Init', 2);
    if ~flag
        return
    end
    if rep == 1
        str1 = 'SonarScope - Entrez le nom de fichier "InstallationParameters"';
        str2 = 'SonarScope - Please select the "InstallationParameters" file';
        [flag, nomFicIP] = uiSelectFiles('Entete', Lang(str1,str2), 'ExtensionFiles', '.xml', ...
            'ChaineIncluse', 'InstallationParameters', 'RepDefaut', pathFile);
        
        % Modif JMA le 22/01/2018 : 'InstallationParameters|IP' en 'InstallationParameters'
        
        if ~flag
            return
        end
        repImport = fileparts(nomFicIP{1});
        
    end
    if iscell(nomFicIP)
        nomFicIP = nomFicIP{1};
    end
    sDataFile.InstallationParameters = xml_read(nomFicIP);
end

sDataTuple.InstallParams.SounderId                          = 2;
sDataTuple.InstallParams.Frequency                          = sDataFile.InstallationParameters.Frequency;
sDataTuple.InstallParams.AlongshipOffsetRelToAttSensor      = sDataFile.InstallationParameters.ReceiveArrayY;
sDataTuple.InstallParams.AthwartshipOffsetRelToAttSensor    = sDataFile.InstallationParameters.ReceiveArrayX;
sDataTuple.InstallParams.VerticalOffsetRelToAttSensor       = sDataFile.InstallationParameters.ReceiveArrayZ;
sDataTuple.InstallParams.AlongshipAngleOffsetOfTransducer   = rad2deg(sDataFile.InstallationParameters.TransmitArrayPitch);   % deg
sDataTuple.InstallParams.AthwartshipAngleOffsetOfTransducer = rad2deg(sDataFile.InstallationParameters.TransmitArrayRoll);   % deg
sDataTuple.InstallParams.RotationAngleOffsetOfTransducer    = rad2deg(sDataFile.InstallationParameters.TransmitArrayHeading); % deg

%% Paquet Position (20, code S7K : 1015) ou  extraction depuis un fichier annexe.

nomFicNav = [];
if isempty(sDataFile.Position)
    
    %% Fichier de navigation
    
    str1 = 'Disposez-vous d''un fichier de navigation permettant de positionner les donn�es ?';
    str2 = 'Do you have a navigation file that would allow to locate the data ?';
    [rep, flag] = my_questdlg(Lang(str1,str2), 'Init', 2);
    if ~flag
        return
    end
    if rep == 1
        str1 = 'SonarScope - Entrez le nom de fichier de navigation au format Caraibes (.nvi)';
        str2 = 'SonarScope - Please select the navigation file (Caraibes format .nvi)';
        [flag, nomFicNav] = uiSelectFiles('Entete', Lang(str1,str2), 'ExtensionFiles', '.nvi', 'RepDefaut', pathFile);
        if ~flag
            return
        end
    end
    if isempty(nomFicNav)
        str1 = sprintf('D�sol�, le fichier "%s" ne contient pas de "donn�e de navigation".', nomFic);
        str2 = sprintf('Sorry, "%s" do not contain any Navigation data.', nomFic);
        my_warndlg(Lang(str1,str2), 0, 'Tag', 'NoNavInThisFile');
        flag = 1;
        return
    else
        if iscell(nomFicNav)
            if length(nomFicNav) > 1
                str1 = 'Il n''est pas encore pr�vu de pouvoir utiliser plusieurs fichiers de nav en m�me temps, d�sol�. Si vous avez vraiment besoin de cela faites moi en la demande, sign� JMA.';
                str2 = 'It is not possible to use several navigation files for the moment. If you really need it, please tell me, signed JMA.';
                my_warndlg(Lang(str1,str2), 1);
                return
            end
            nomFicNav = nomFicNav{1};
        end
        % Lecture du fichier de type Caraibes (nvi)
        [flag, LatNav, LonNav, TimeNav, HeadingNav, SpeedNav, ImmNav, Altitude, Type, Pitch, Roll, Heave] = lecFicNav(nomFicNav);
        if ~flag
            str1 = sprintf('D�sol�, le fichier de navigation "%s" n''est pas exploitable.', nomFicNav);
            str2 = sprintf('Sorry, "%s" navigation file is not good format.', nomFicNav);
            my_warndlg(Lang(str1,str2), 0, 'Tag', 'NoNavInThisFile');
            flag = 1;
            return
        end
        sDataFile.Position.Time.timeMat     = datenum(t2strIso8601(TimeNav), 'yyyy-mm-DDTHH:MM:SS.FFF');
        sDataFile.Position.Time.timeMat     = sDataFile.Position.Time.timeMat';
        sDataFile.Position.Latitude         = LatNav;
        sDataFile.Position.Longitude        = LonNav;
        sDataFile.Position.Heading          = HeadingNav;
        sDataFile.Position.Speed            = SpeedNav;
        % figure; plot(LonNav, LatNav); grid on
    end
    
end

PositionTime        = num2cell(sDataFile.Position.Time.timeMat);
PositionPingCounter = num2cell(1:numel(sDataFile.Position.Time.timeMat));

IndexPosTuple       = num2cell((1:numel(PositionTime)));  % Index des ping
IndexWCTuple        = num2cell((1:numel(WCTime)));        % Index des ping trait�s.

%% Fichier d'attitude extrait explicitement

nomFicAtt = [];
if isempty(sDataFile.Attitude)
    str1 = 'Disposez-vous d''un fichier d''attitude permettant de positionner les donn�es ?';
    str2 = 'Do you have a attitude file that would allow to locate the data ?';
    [rep, flag] = my_questdlg(Lang(str1,str2), 'Init', 2);
    if ~flag
        return
    end
    if rep == 1
        str1 = 'SonarScope - Entrez le nom de fichier d''attitude au format Ascii .txt';
        str2 = 'SonarScope - Please select the navigation file (Ascii .txt)';
        [flag, nomFicAtt] = uiSelectFiles('Entete', Lang(str1,str2), 'ExtensionFiles', '.txt', 'RepDefaut', pathFile);
        if ~flag
            return
        end
    end
    if isempty(nomFicAtt)
        str1 = sprintf('D�sol�, le fichier "%s" ne contient pas de "donn�e d''attitude".', nomFic);
        str2 = sprintf('Sorry, "%s" do not contain any Attitude data.', nomFic);
        my_warndlg(Lang(str1,str2), 0, 'Tag', 'NoAttInThisFile');
        flag = 1;
        return
    else
        if iscell(nomFicAtt)
            if length(nomFicAtt) > 1
                str1 = 'Il n''est pas encore pr�vu de pouvoir utiliser plusieurs fichiers d''atttitude en m�me temps, d�sol�. Si vous avez vraiment besoin de cela faites moi en la demande, sign� JMA.';
                str2 = 'It is not possible to use several attitude files for the moment. If you really need it, please tell me, signed JMA.';
                my_warndlg(Lang(str1,str2), 1);
                return
            end
            nomFicAtt = nomFicAtt{1};
        end
        % Lecture du fichier de type Caraibes (nvi)
        [flag, TimeAtt, ~, ~, ~, Heading, Pitch, Roll, Heave] = lecFicNav_DelphINS(nomFicAtt);
        if ~flag
            str1 = sprintf('D�sol�, le fichier "%s" n''est pas exploitable.', nomFicAtt);
            str2 = sprintf('Sorry, "%s" attitude file is not good format.', nomFicAtt);
            my_warndlg(Lang(str1,str2), 0, 'Tag', 'NoAttInThisFile');
            flag = 1;
            return
        end
        sDataFile.Attitude.Time.timeMat     = datenum(t2strIso8601(TimeAtt), 'yyyy-mm-DDTHH:MM:SS.FFF');
        sDataFile.Attitude.Time.timeMat     = sDataFile.Attitude.Time.timeMat';
        sDataFile.Attitude.Roll             = Roll;
        sDataFile.Attitude.Pitch            = Pitch;
        sDataFile.Attitude.Heading          = Heading;
        sDataFile.Attitude.Heave            = Heave;
    end
end

%% Construction de la structure de Pings en aggr�gant les Position et les Samples
% Cr�ation d'un tableau de structures : les tuples Position constitue un sous-ensemble
% au m�me titre que les tuples "10000" qui rendent implicites la d�claratoin de certains tuples
% dans le process de cr�ation du fichier HAC.
% WCResonPingCounter    : N� de ping Depth
% WCMultiPingSequence   : N� de ping WC �ludant les pings manquants (<=
% WCPingIndex) correspondant au n� du sondeur .
% WCPingIndex           : N� de ping WC index� sur ceux des N� Pings Depth
% MagAndPPingCounter    : N� de ping Mag&Phase si pr�sents.
try
    
    Data1 = struct('Date',      PositionTime, ...
        'Type',                 20, ...
        'PingCounter',          PositionPingCounter, ...
        'Index',                IndexPosTuple, ...
        'WCResonPingCounter',   [], ...
        'WCMultiPingSequence',  [], ...
        'WCPingIndex',          [], ...
        'MAndPPingCounter',     []);
    
    % Cr�ation d'un tableau de structures.
    if iscolumn(sDataFile.WaterColumn.sub7000)
        % Cas du fichier 20150902_030244_PP_7150_24kHz !!!
        sDataFile.WaterColumn.sub7000 = sDataFile.WaterColumn.sub7000';
    end
    Data2 = struct('Date',      WCTime, ...
        'Type',                 10000, ...
        'PingCounter',          WCPingCounter, ...
        'Index',                IndexWCTuple, ...
        'WCResonPingCounter',   tabWCResonPingCounter, ...
        'WCMultiPingSequence',  tabWCResonMultiPingSequence, ...
        'WCPingIndex',          num2cell(sDataFile.WaterColumn.sub7000), ...
        'MAndPPingCounter',     tabResonMAndPPingCounter);
catch ME
    % Cas du fichier 20150920_085601_PP_7150_24kHz.s7k
    strWrn = sprintf('%s', Lang('Mauvaise concordance des nombres de pings WC et nb de Multisequence. Merci de v�rifier le contenu du fichier.',...
        'Bad correspondance between number of Pings WC and multisequence number. Please, check file content'));
    hWrn = my_warndlg(strWrn , Lang('Pb de nombre de pings', 'Number of pings problem'));
    flag = 0;
    return
end


% Concat�nation des tableaux de structures pour aboutir � un seul tableau.
listPing = struct('Date',   {Data1.Date Data2.Date}, ...
    'Type',                 {Data1.Type Data2.Type}, ...
    'PingCounter',          {Data1.PingCounter Data2.PingCounter}, ...
    'Index',                {Data1.Index Data2.Index}, ...
    'WCResonPingCounter',   {Data1.WCResonPingCounter Data2.WCResonPingCounter}, ...
    'WCMultiPingSequence',  {Data1.WCMultiPingSequence Data2.WCMultiPingSequence} ,...
    'WCPingIndex',          {Data1.WCPingIndex Data2.WCPingIndex}, ...
    'MagAndPPingCounter',   {Data1.MAndPPingCounter Data2.MAndPPingCounter});

% Tri des tuples selon les ordres d'arriv�e par date dans le fichier s7K.
[~, order] = sort([listPing.Date]);
listPing   = listPing(order);

% -----------------------------------------------
% Calcul de l'histogramme des types de datagramme
% Historisation des types de Tuples �crits (cf. le s�quencement de processTuples)

tmpType = [ listPing.Type ones(1,numel(PositionTime))*30 ones(1,numel(WCTime))*220 ...
    ones(1,numel(WCTime)) * 2200 ...
    ones(1,numel(WCTime)) * 41];
bins                = min(tmpType):max(tmpType);
histoTypeDatagram   = my_hist(tmpType,bins);
list                = unique(tmpType);
histoTypeDatagram   = histoTypeDatagram(histoTypeDatagram~=0);

if DEBUG
    % listeTupleType = unique([Ping(:).Type]);
    listeTupleType = unique(tmpType);
    nbTupleTypes   = length(listeTupleType);
    tabNbBytes     = NaN(1,nbTupleTypes );
    
    TupleSize = sDataFile.TupleSize(:);
    for i=1:nbTupleTypes
        sub = (TupleType == listeTupleType(i));
        tabNbBytes(i) = sum(TupleSize(sub));
        %     fprintf('Tuplem %3d    Nb of Bytes %10d\n', listeTupleType(i), nbBytes(i));
    end
    histoTupleType  = my_hist(tmpType, listeTupleType);
    texteTupleType  = codesTuplesHac(listeTupleType, histoTupleType, 'fullListe', 0);
    
    % tabNbBytes = zeros(size(histoTupleType));
    % tabNbBytes(histoTupleType ~= 0) = nbBytes;
    plot_histo_index_tab(listeTupleType, histoTupleType, texteTupleType, tabNbBytes, nomFic);
    
end

nbMax    = numel(listPing);
nbTuples = histoTypeDatagram(list == listPing(1).Type);
fprintf('Packet 1 - %s  %s\n', datestr(listPing(1).Date(1)), codesTuplesHac(listPing(1).Type, nbTuples));
nbTuples = histoTypeDatagram(list == listPing(end).Type);
fprintf('Packet %d - %s  %s\n', nbMax, datestr(listPing(end).Date(1)), codesTuplesHac(listPing(end).Type, nbTuples));

%% Traitement des paquets : chargement des donn�es utiles au HAC depuis les caches de SonarScope.

% sDataTuple.Position.Ping                = sDataFile.Position.PingCounter;
Time                               = sDataFile.Position.Time.timeMat;
% D�composition du time.
sDataTuple.Position.time_cpu       = floor((Time-datenum('19700101','yyyymmdd'))*86400);
sDataTuple.Position.time_fraction  = ((Time-datenum('19700101','yyyymmdd'))*86400-sDataTuple.Position.time_cpu)*10000;
sDataTuple.Position.latitude       = sDataFile.Position.Latitude*1e6;
sDataTuple.Position.longitude      = sDataFile.Position.Longitude*1e6;
sDataTuple.Position.speed          = sDataFile.Position.Speed;
sDataTuple.Position.heading        = sDataFile.Position.Heading;

%% Paquet Attitude (HAC : 10140)
% sDataTuple.Attitude.Ping        = sDataFile.Attitude.PingCounter;
Time                              = sDataFile.Attitude.Time.timeMat;
% D�composition du time.
sDataTuple.Attitude.time_cpu      = floor((Time-datenum('19700101','yyyymmdd'))*86400);
sDataTuple.Attitude.time_fraction = ((Time-datenum('19700101','yyyymmdd'))*86400-sDataTuple.Attitude.time_cpu)*10000;
sDataTuple.Attitude.roll          = sDataFile.Attitude.Roll;
sDataTuple.Attitude.pitch         = sDataFile.Attitude.Pitch;
sDataTuple.Attitude.heave         = sDataFile.Attitude.Heave;
sDataTuple.Attitude.heading       = sDataFile.Attitude.Heading;

%% Paquet InstallParams (HAC : 901, S7K : 1010)
% Le champ sDataFile.SoundSpeed d�pend des profils de c�l�rit� XBT r�inject�s dans le
% fichier d'acquisition.
% sDataTuple.DataSample.soundSpeed          = sDataFile.SoundSpeed.SoundSpeed;
% Le champ sDataTuple.WaterColumn.SoundVelocity d�pend du sondeur de coque.
sDataTuple.DataSample.soundSpeed = sDataFile.WaterColumn.SoundVelocity;

%% Paquet Water Column  (HAC : 10040 en particulier, S7K : 7041)

sDataTuple.RunTime.PulseLength      = sDataFile.WaterColumn.TxPulseWidth*1e3;            % en msec.
sDataTuple.RunTime.BeamWidthAlong   = rad2deg(sDataFile.WaterColumn.ProjectionBeam_3dB_WidthVertical);
sDataTuple.RunTime.BeamWidthAthwart = rad2deg(sDataFile.WaterColumn.ReceiveBeamWidth);   % rad2deg(sDataFile.WaterColumn.ProjectionBeam_3dB_WidthHorizontal);
sDataTuple.RunTime.BandWidth        = sDataFile.WaterColumn.ReceiverBandwith*1e3;        % en kHz
sDataTuple.RunTime.AbsorptionCoeff  = sDataFile.WaterColumn.Absorption/10;               % Conforme au code ResonS7k_Summray.m

nbPingsWC = numel(WCPingCounter);                    % WCDatagrams.Dimensions.nbPings;
nbBeams   = unique([sDataFile.WaterColumn.NbBeams]); % WCDatagrams.Dimensions.nbRx;


%% Chargement des donn�es angulaires de pointage du WC.

seuilDeg        = NaN(nbPingsWC, nbBeams); % Inutile si absence de MagAndPhase.
channelIdent    = [];
if isAny7041 % Pour le cas o� nous aurions pas de WC (est-ce possible ?)
    structDataOrig = 'WaterColumn';
    
    [flag, sDataTuple.DataSample] = fcnLoadGenericDataSampleStruct( sDataFile, structDataOrig, subPingWC, nbPingsWC, ...
        nbBeams, sData7004);
    
    [channelIdent, ~, ~] = unique([listPing(:).WCMultiPingSequence]);
    % Num�rotation des faisceaux pour les distinguer les multi-ping comme autant de sondeurs..
    sDataTuple.DataSample.ChannelIdent = 1:(numel(channelIdent)*nbBeams);
    sDataTuple.DataSample.ChannelIdent= reshape(sDataTuple.DataSample.ChannelIdent, nbBeams, numel(channelIdent));
    sDataTuple.DataSample.ChannelIdent= sDataTuple.DataSample.ChannelIdent';
    
    if ~flag
        return
    end
end
if isAny7018 % si pr�sence de Mag&Phase
    structDataOrig = 'MagAndPhase';
    [flag, sDataTuple.MagAndPhase] = fcnLoadGenericDataSampleStruct( sDataFile, structDataOrig, subPingWC, nbPingsWC, ...
        nbBeams, sData7004);
    if isempty(channelIdent)
        [channelIdent, ~, ~] = unique([listPing(:).WCMultiPingSequence]);
        sDataTuple.MagAndPhase.ChannelIdent	= 1:(numel(channelIdent)*nbBeams);
    else
        % Continuit� des num�ros de channels les WaterColumn ont d�j� �t� d�clar�s (v. plus haut).
        sDataTuple.MagAndPhase.ChannelIdent = sDataTuple.DataSample.ChannelIdent(end)+(1:(numel(channelIdent)*nbBeams));
    end
    
    % Num�rotation des faisceaux pour les distinguer les multi-ping comme autant de sondeurs..
    sDataTuple.MagAndPhase.ChannelIdent = reshape(sDataTuple.MagAndPhase.ChannelIdent, nbBeams, numel(channelIdent));
    sDataTuple.MagAndPhase.ChannelIdent = sDataTuple.MagAndPhase.ChannelIdent';
    % Calcul du seuil angulaire pour le traitement de compression des phases.
    %{
% 10/11/2016, GLU-QO : Pas de seuillage des valeurs car on ne sait pas compresser
% les tuples 10011 pas plus que MOVIES ne sait pas les relire.
for iPing=1:nbPingsWC
scaleFactor         = 1.5; % Facteur de marge
% seuilDeg(iPing, :)  = scaleFactor*sDataTuple.RunTime.BeamWidthAthwart(iPing)./cosd(sDataTuple.MagAndPhase.angles_athwart(iPing, :));
end
    %}
    seuilDeg = -Inf(nbPingsWC, nbBeams);
    if ~flag
        return
    end
end


%% Ecriture du fichier

nomFicOut = fullfile(pathFile, [rootFicName '_s7k2hac.hac_tmp']);
% Boucle sur l'ensemble des paquets recens�s dans les formats Kongsberg.
if exist(nomFicOut, 'file')
    delete(nomFicOut);
end
% Ouverture du fichier pour enrichissement � chaque paquet.
fid = fopen(nomFicOut, 'w+');

% Chargement des donn�es pour les datagrammes identifi�s.
SounderName = ['Reson' num2str(sDataFile.WaterColumn.SystemSerialNumber)];

% Traitement des tuples selon le type de fichier en jeu.
file.Class = class(s7k);
file.fid   = fid;


% Lancement du processus d'�criture.
flag = processTuples(file, nomDir, listPing, nbBeams, SounderName, sDataTuple, ...
    seuilSampledB, 'SeuilDeg', seuilDeg, 'Distance', dInterfero);

if ~flag
    return
end

fclose(fid);

try
    movefile(nomFicOut, nomFicHac, 'f')
catch %#ok<CTCH>
    str1 = sprintf('Le fichier "%s" n''a pas pu �tre renomm� en "%s", tentez de le faire manuellement apr�s �tre sorti de SonarScope.', nomFicOut, nomFicHac);
    str2 = sprintf('"%s" could not be moved in "%s", try to do it by hand after having closed SonarScope.', nomFicOut, nomFicHac);
    my_warndlg(Lang(str1,str2), 1);
end


% === Fonctions nested ==========================================
% ---------------------------------------------------------------
%
% Function : fcnWriteGenericDataSampleStruct
%
% ---------------------------------------------------------------
function [flag, sDataSample] = fcnLoadGenericDataSampleStruct( sDataFile, structDataOrig, subPingWC, nbPingsWC, ...
    nbBeams, sData7004)

flag        = 0;


% Configuration de valeurs :
% - des fr�quences pour le cas o� elles ne sont pas d�crites dans les datagrammes Depth (paquet OD du Reson) : cas des donn�es
% Mag&Phase.
% - des angles Along car la variation des angles issues provoquent la r��criture des tuples 901/9001
% MonoPing (0) et MultiPing (1 � 4)
defMultiPing.index          = 0:4;
% en 24 Khz
defMultiPing(1).freq        = [24 24.5 22.5 25.5 23.5];  % en Khz
defMultiPing(1).anglesAlong = [0 -0.75 -0.25 0.25 0.75]; % en deg
% en 12 Khz, TODO � finaliser (GLU le 2016/11/22)
defMultiPing(2).freq        = [12 12.5 10.5 14.2 13.5];  % en Khz
defMultiPing(2).anglesAlong = [0 -1.5 -0.5 0.5 1.5];     % en deg

sDataSample = [];

% Identification du dataset de la fr�quence concern�e en prenant la 1�re
% fr�q comme �chantillon.
freqValToIdentify = sDataFile.Depth.Frequency(1);
if isnan(freqValToIdentify)
    freqValToIdentify = sDataFile.WaterColumn.Frequency(1)/1e3;
    idxFreqIdentified = find(arrayfun(@(x) (min(x.freq) <= freqValToIdentify & freqValToIdentify <= max(x.freq)), defMultiPing));
else
    idxFreqIdentified = find(arrayfun(@(x) ~isempty(find(x.freq == freqValToIdentify, 1)), defMultiPing));
end

% Traitement des fr�quences � NaN (cas des fichiers Mag&Phase o� la donn�e
% peut �tre absente).
idx         = sDataFile.(structDataOrig).MultiPingSequence;
idx         = idx + 1;
% R�partition des valeurs de fr�quences trouv�es dans le tableau d�tect� � Nan.
anglesAlong = repmat(defMultiPing(idxFreqIdentified).anglesAlong(idx)', 1, nbBeams);

% Interpolation du pitch selon le datagramme Attitude.
% formule de compensation des angles Athwart (v. LB):
%   alongship_a_o_beam      = Txangle_datagramma_7000 en � + alongship_a_o_trsd en � + pitch en �
%   N/A : athwartship_a_o_beam    = RxBeamAngle_datagramma_7000 en � + athwartship_a_o_trsd en � + roll en �

% Sans compensation :
% angleAlong    = sDataFile.Depth.ProjectorBeamSteeringAngleVertical(subPingWC(:));

%{
% Compensation de l'angle along par les valeurs de Pitch.
anglesAlong         =   sDataFile.Depth.ProjectorBeamSteeringAngleVertical(subPingWC(:)) + ...
rad2deg(sDataFile.InstallationParameters.TransmitArrayPitch) + ...
sDataFile.Depth.Pitch(subPingWC(:));

% sDataTuple.DataSample.angles_athwart  = BeamDepressionAngle

angleAthwart    =   sDataFile.Depth.BeamDepressionAngle(subPingWC,:) + ...
rad2deg(sDataFile.InstallationParameters.TransmitArrayRoll) + ...
repmat(sDataFile.Depth.Roll(subPingWC(:))',1, nbFaisceaux);
%}

% R�cup�ration des angles Verticaux pour int�gration aux angles athwart.
% On recherche les angles tout juste pr�c�dents les pings WC.
% Voir pour l'angle Athwart le traitement par SSC du Roll dans
% Inspir�e du traitement dans cl_simrad_all\import_PingBeam_All_V2\get_RollRx.m.
dateJulianWC   = sDataFile.WaterColumn.Time;
dateJulianWC   = repmat(dateJulianWC, nbBeams, 1);
dateJulian7004 = datenum(t2strIso8601(sData7004.Time), 'yyyy-mm-DDTHH:MM:SS.FFF');
dateJulian7004 = dateJulian7004';
dateJulian7004 = repmat(dateJulian7004, nbBeams, 1);

anglesAthwart  = sData7004.BeamHorizontalDirectionAngle;

if all(isnan(anglesAthwart))
    % interpolation infructueuse : on se contente de r�p�ter la 1�re s�rie
    % d'angle Athwart autant de fois que de pings WC.
    anglesAthwart = repmat(sData7004.BeamHorizontalDirectionAngle(1,:), nbPingsWC, 1);
end
if ~isequal(size(anglesAthwart), [nbPingsWC, nbBeams])
    anglesAthwart = repmat(sData7004.BeamHorizontalDirectionAngle(1,:), nbPingsWC, 1);
    % TODO : vrai pb d'interpolation temporelles des angles (voir abvec JMA selon l'exemple du dessus).
    % angleAthwart = my_interp1(dateJulian7004, sData7004.BeamHorizontalDirectionAngle(:), dateJulianWC, 'linear', 'extrap');
    % angleAthwart = angleAthwart';
end

sDataSample.angles_athwart = anglesAthwart;
sDataSample.angles_along   = anglesAlong;

% Traitement des fr�quences � NaN (cas des fichiers Mag&Phase o� la donn�e
% peut �tre absente).
sDataSample.frequency = repmat(sDataFile.Depth.Frequency(subPingWC)', 1, nbBeams)*100;
subFreq               = isnan(sDataFile.Depth.Frequency(subPingWC));
idxFreq               = sDataFile.(structDataOrig).MultiPingSequence(subFreq);
idxFreq               = idxFreq + 1;
% Superflu si les fr�q. sont significatives.
Index = repmat(defMultiPing(idxFreqIdentified).freq(idxFreq)', 1, nbBeams)*100;
if ~isempty(Index)
    % R�partition des valeurs de fr�quences trouv�es dans le tableau d�tect� � Nan.
    sDataSample.frequency(subFreq,:) = Index;
end

sDataSample.Ping          = sDataFile.(structDataOrig).PingCounter;
% D�composition du time.
sDataSample.time_cpu      = floor((sDataFile.(structDataOrig).Time-datenum('19700101','yyyymmdd'))*86400);
sDataSample.time_fraction = ((sDataFile.(structDataOrig).Time-datenum('19700101','yyyymmdd'))*86400 - ...
    sDataSample.time_cpu)*10000;

sDataSample.sampling   = sDataFile.(structDataOrig).SampleRate;     % SamplingFrequency
sDataSample.soundSpeed = sDataFile.(structDataOrig).SoundVelocity;
sDataSample.NbSamples  = sDataFile.(structDataOrig).NbSamples;

% Calcul de l'�chantillonnage en m�tres pour calculer la distance de d�tection de fond.
samplingInterval = (1./sDataFile.(structDataOrig).SampleRate).*sDataFile.(structDataOrig).SoundVelocity/2;

% Application du Sampling Interval pour calculer la d�tection de fond � ins�rer dans le tuple 10040.
szRB = size(sDataFile.(structDataOrig).RangeBottom,2);
sDataSample.rangebottom = repmat(samplingInterval', 1, szRB) .* sDataFile.(structDataOrig).RangeBottom; % DataSample.Depth. % DataSample.DetectedRangeInSamples;

flag = 1;
