function [R0, RMax1, RMax2, R1SamplesFiltre, iBeamMax0, AmpPingNormHorzMax, MaskWidth, iBeamBeg, iBeamEnd] = ...
    SampleBeam_PreprocAmpMask_7111_V6(Amp, step, ...
    Teta, ReceiveBeamWidth, SampleRate, TxPulseWidth, DepthMin, DepthMax, IdentAlgo, DisplayLevel, iPing)

global TestPlatformCmatlab %#ok<GVMIS>

coefEnlargeMask = 4;
R0Marge = 0;

% NbStrips = floor(size(Amp,2)/14); % Modifi� par JM1 le 02/07/2014 pour test
NbStrips = floor(size(Amp,2)/8); % Modifi� par JM1 le 23/09/2014 pour test

Rmin = (abs(DepthMin) ./ cosd(Teta)) * 2 * (SampleRate / 1500) / step;
Rmax = (abs(DepthMax) ./ cosd(Teta)) * 2 * (SampleRate / 1500) / step;
% figure; plot(Rmin); grid on; hold on; plot(Rmax); hold off;

nbBeams   = size(Amp,2);
Amp = Amp(1:end-2,:);
nbSamples = size(Amp,1);

%% Computation of the minimum range 

if DisplayLevel == 3
    Fig1 = [];
    Fig2 = [];
    Fig3 = 5656;
else
    Fig1 = [];
    Fig2 = [];
    Fig3 = [];
end

display_image(Fig1, Amp, 'Originale')
Start = 2;
% if isempty(iPing)
%     iPing = 1; % Rajout� par JMA le 03/07/2015 pour fichier Mag&Phase EM710 d'Herv� (0056_20150615_234546_ATL_EM710.all)
% end
% 
%% Reson_R0_estimation_V6

% SeuilCum = 0.5;
SeuilAmp = 10; % 4 pour 7150 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% [R0_OnOriginal, R0StripOriginal] = Reson_R0_estimation_V6(Amp, NbStrips, Start, repmat(nbSamples, 1, nbBeams), SeuilAmp, SeuilCum, Rmin, Rmax, Fig3+1, [], iPing, DisplayLevel);
[R0_OnOriginal, R0StripOriginal] = Reson7150_R0_estimation_V1(Amp, NbStrips, Start, repmat(nbSamples, 1, nbBeams), SeuilAmp, Rmin, Rmax, Fig3+1, [], iPing, DisplayLevel);

%% Matrix_compensation

if any(TestPlatformCmatlab.matrix_compensation == [1 3 4])
    Amp_M = matrix_compensation_Matlab(Amp);
end

if any(TestPlatformCmatlab.matrix_compensation == [2 3 4])
    Amp_C = matrix_compensation_reson(...
        single(Amp));
end

if any(TestPlatformCmatlab.matrix_compensation == [3 4])
    subNonNaN = ~isnan(Amp_M);
    if  ~isequal(Amp_C(subNonNaN), Amp_M(subNonNaN))
        figure('Name', [' BDA : divergence C / Matlab Ping ' num2str(iPing)]);
        imagesc(Amp_C - Amp_M); colorbar
        str = sprintf('matrix_compensation RESON\ncalled in SampleBeam_PreprocAmpMask_7111_V6');
        title(str, 'Interpreter', 'None')
%         imagesc(Amp_C - Amp_M, [-20 200]); colorbar
        SonarScope(Amp_C - Amp_M)
    end
end

switch TestPlatformCmatlab.matrix_compensation
    case {1; 3}
        Amp = Amp_M;
    case {2; 4}
        Amp = Amp_C;
end

display_image(Fig2, Amp, 'Compensated')

%% Reson_R0_estimation_V6

SeuilAmp = 4;
Start = 1;
% SeuilCum = 0.5;
% [R0, R0Strip] = Reson_R0_estimation_V6(    Amp, NbStrips, Start, repmat(nbSamples, 1, nbBeams), SeuilAmp, SeuilCum, Rmin, Rmax, Fig3, [0 4], iPing, DisplayLevel);
[R0, R0Strip] = Reson7150_R0_estimation_V1(Amp, NbStrips, Start, repmat(nbSamples, 1, nbBeams), SeuilAmp, Rmin, Rmax, Fig3+1, [], iPing, DisplayLevel);

% % R0+4 needed for ping 9 of Canyon 20080220_134819
% if R0_OnOriginal > (R0+4)
% % This condition had to be canceled for file 20080219_231309 because fish
% sholes were detected

if IdentAlgo ~= 3
    if R0_OnOriginal > (R0+R0Marge)
        R0 = R0_OnOriginal;
        subFaux = find(R0Strip < R0_OnOriginal);
        R0Strip(subFaux) = R0StripOriginal(subFaux);
    end
end

subTrajetDouble = find((R0Strip >= (1.9*R0StripOriginal)) & (R0Strip <= (2.1*R0StripOriginal)));
R0Strip(subTrajetDouble) = R0StripOriginal(subTrajetDouble);
% figure; plot(R0Strip ./ R0StripOriginal, '*'); grid on;

if isnan(R0)
    R0 = NaN;
    RMax1 = NaN(1,nbBeams);
    RMax2 = NaN(1,nbBeams);
    R1SamplesFiltre = NaN(1,nbBeams);
    iBeamMax0 = NaN;
    AmpPingNormHorzMax = NaN(1,nbBeams);
    MaskWidth = NaN(1,nbBeams);
    iBeamBeg = NaN;
    iBeamEnd = NaN;
    return
end

SeuilAmp = 2;
Start = max(R0-2, 1);
% SeuilCum = 0.5;

R0Strip2 = R0Strip;
R02 = R0;

% [R0, R0Strip, flagBeams] = Reson_R0_estimation_V6(    Amp, NbStrips, Start, 2*R0Strip, SeuilAmp, SeuilCum, Rmin, Rmax, Fig3*2, [0 4], iPing, DisplayLevel);
[R0, R0Strip, flagBeams] = Reson7150_R0_estimation_V1(Amp, NbStrips, Start, 2*R0Strip, SeuilAmp, Rmin, Rmax, Fig3+1, [], iPing, DisplayLevel);
if isnan(R0)
    R0 = 1;
end

if R0 < R02
    sub = find(R0Strip < R02);
    R0Strip(sub) = R0Strip2(sub);
    R0 = R02;
end

% Cas survenu sur fichier F:\Feb2008\7111\Rock\20080219_233937.s7k ping 920
% Still useful ???????????????????????????????
if R0 == 1
    R0                  = NaN;
    RMax1               = NaN(1,nbBeams);
    RMax2               = NaN(1,nbBeams);
    iBeamMax0           = NaN(1,nbBeams);
    AmpPingNormHorzMax  = NaN(1,nbBeams);
    MaskWidth           = NaN(1,nbBeams);
    R1SamplesFiltre     = NaN(1,nbBeams);
    iBeamBeg            = NaN;
    iBeamEnd            = NaN;
    return
end

AMedian = Inf(1,nbBeams);
for iBeam=1:nbBeams
    if flagBeams(iBeam)
        AMedian(iBeam) = median(Amp(:,iBeam), 'omitnan');
    end
end

for iBeam=1:nbBeams
    if flagBeams(iBeam)
        iSampleBeg = max(floor(R0-3 + (R0Strip(iBeam)-R0)/3), R0-3);
        iSampleEnd = min(floor(1.4*R0Strip(iBeam)), nbSamples);
        
        iSampleBeg = max(iSampleBeg, floor(Rmin(iBeam)));
        iSampleEnd = min(iSampleEnd, ceil(Rmax(iBeam)));
        
        if iSampleEnd >= 1
            Amp(iSampleEnd:end,iBeam) = NaN;
        end
        Amp(1:iSampleBeg,iBeam) = NaN;
    else
        Amp(:,iBeam) = NaN;
    end
end

Offset = R0-1;
Amp = Amp(Offset:end,:);
nbSamples = size(Amp,1);

%% Search for the maximum amplitude along each beam starting from R0

RMax1 = ones(1,nbBeams) * nbSamples;
AMax = zeros(1,nbBeams);
for iBeam=1:nbBeams
    if flagBeams(iBeam)
        [aMax, rMax1] = max(Amp(:,iBeam));
        if aMax > 2*AMedian(iBeam)
            AMax(iBeam) =  aMax;
            RMax1(iBeam) = rMax1;
        end
    end
end
% RMax1Max = RMax1 + (Offset-1);

%{
figure; imagesc(1:size(Amp,2), (1:size(Amp,1)), Amp); colormap(jet(256)); colorbar; hold on; plot(RMax1, 'xr'); hold off;
%}

for iBeam=1:nbBeams
    if flagBeams(iBeam)
        a = Amp(:,iBeam);
        iDeb = find(isnan(a(1:RMax1(iBeam))), 1, 'last');
        
        % Add JM 28/05/2014 Begin
        if isempty(iDeb)
            iDeb = 0;  
        end
        % Add JM 28/05/2014 End
        
        X = min(RMax1(iBeam)-iDeb, nbSamples-RMax1(iBeam));
        sub = RMax1(iBeam)-X:RMax1(iBeam)+X;
        sub(sub < 1) = [];
        sub(sub>length(a)) = [];
        if length(sub) < 5
            continue
        end
        rMax1 = detectionAmplitudePingOnCompensatedImage(1:size(Amp,1), a(sub), iPing, iBeam, Teta(iBeam), DisplayLevel, nbSamples);
        if ~isnan(rMax1)
            RMax1(iBeam) = rMax1 + (sub(1)-1);
        end
    end
end
RMax1 = floor(RMax1 + (Offset-1) - 0.5); % -0.5 apparently necessary (view on file 20080222_014920
RMax1(flagBeams == 0) = NaN;


%{
figure; imagesc(1:size(Amp,2), (1:size(Amp,1))+R0-2, Amp); colormap(jet(256)); colorbar; hold on; plot(RMax1, 'xr'); plot(RMax1, 'ok'); hold on;
%}


%% Identification of the beam number corresponding to R0

pppp = min(RMax1);
sub = find(RMax1 == pppp);
if isempty(sub)
    R0 = NaN;
    RMax1 = NaN(1,nbBeams);
    RMax2 = NaN(1,nbBeams);
    R1SamplesFiltre = NaN(1,nbBeams);
    iBeamMax0 = NaN;
    AmpPingNormHorzMax = NaN(1,nbBeams);
    MaskWidth = NaN(1,nbBeams);
    iBeamBeg = NaN;
    iBeamEnd = NaN;
    return
end
iBeamMax0Full = floor((sub(1) + sub(end))/2);

pppp = RMax1;
AMaxMoyen = mean(AMax, 'omitnan');
pppp(AMax < AMaxMoyen) = Inf;
[qqqq, iBeamMax0] = min(pppp); %#ok<ASGLU>
sub = find(pppp == qqqq);
iBeamMax0 = floor((sub(1) + sub(end))/2);

MS2 = nbBeams / 2;
if abs(iBeamMax0Full-MS2) < abs(iBeamMax0-MS2)
    iBeamMax0 = iBeamMax0Full;
end

if (iBeamMax0 == 1) || (iBeamMax0 == nbBeams)
    iBeamMax0 = floor(nbBeams/2);
else
    k = iBeamMax0;
    while 1
        k = k + 1;
        if k >= nbBeams
            break
        end
        if floor(RMax1(k)) <= floor(RMax1(k+1))
            iBeamMax0 = k + 1;
        else
            break
        end
    end
    k = iBeamMax0;
    while 1
        k = k - 1;
        if k <= 1
            break
        end
        if floor(RMax1(k-1)) <= floor(RMax1(k))
            iBeamMax0 = k - 1;
        else
            break
        end
    end
end

%{
FigUtils.createSScFigure; 
h(1) = subplot(2,1,1);
PlotUtils.createSScPlot(RMax1); grid on; set(h(1), 'YDir', 'reverse');
h(2) = subplot(2,1,2);
PlotUtils.createSScPlot(AMax); grid on; hold on;
PlotUtils.createSScPlot([1 length(AMax)], [AMaxMoyen AMaxMoyen], 'r');
PlotUtils.createSScPlot(iBeamMax0, AMax(iBeamMax0), 'k*');
hold off;
linkaxes(h, 'x')
%}

AMax2 = AMax .^ 2;

pppp = cumsum(AMax2(1:iBeamMax0));
pppp = pppp / max(pppp);
iBeamBeg = find(pppp >= 0.005, 1, 'first');

pppp = cumsum(AMax2(iBeamMax0:end));
pppp = pppp / max(pppp);
iBeamEnd = iBeamMax0 - 1 + find(pppp > 0.995, 1, 'first');

%{
FigUtils.createSScFigure;
MeanAMax2 = mean(AMax2);
PlotUtils.createSScPlot(AMax2); grid on; hold on;
PlotUtils.createSScPlot([1 length(AMax2)], [MeanAMax2 MeanAMax2], 'r');
PlotUtils.createSScPlot(iBeamBeg, AMax2(iBeamBeg), '*r'); 
PlotUtils.createSScPlot(iBeamEnd, AMax2(iBeamEnd), '*r'); 
hold off;
%}

%% Second serie of maximals

RMax2 = RMax1;
for iBeam=2:(nbBeams-1)
    RMax2(iBeam) = (RMax1(iBeam-1)+RMax1(iBeam+1)) / 2;
end
%{
figure; imagesc(Amp); hold on; plot(RMax1, '*k'); plot(RMax2, 'ob')
%}

R0 = min(R0,min(RMax1));
if DisplayLevel == 3
    hFig = figure(56557);
    set(hFig, 'Name', 'Range detection on reduced Amplitude image')
    imagesc(1:size(Amp,2), (1:size(Amp,1))+(Offset-1), Amp); colormap(jet(256)); colorbar;
    hold on;
%     plot(1:nbBeams, repmat(R0 ,1,nbBeams), '*c');
    plot(1:nbBeams, RMax1, '*c')
    plot(1:nbBeams, RMax2, 'ok')
    hold off
end

%% Local Time spreading of the signal around the maximum

R1Samples = floor((RMax1 -0.5) * step);
R1SamplesFiltre = suppres_steps_V2(R1Samples, Teta);

R1Samples = OriginalRange(RMax1, 0, step);
R2Samples = OriginalRange(RMax2, 0, step);

Z1 = R1Samples .* cosd(Teta);
Z2 = R2Samples .* cosd(Teta);

MaskWidth1 = ceil(Z1.* abs(( 1./ cosd(abs(Teta) + ReceiveBeamWidth/2))  - ( 1./ cosd(abs(Teta) - ReceiveBeamWidth/2))));
MaskWidth2 = ceil(Z2.* abs(( 1./ cosd(abs(Teta) + ReceiveBeamWidth/2))  - ( 1./ cosd(abs(Teta) - ReceiveBeamWidth/2))));
Slope = 0;
MaskWidth = ceil(max(MaskWidth1,MaskWidth2) .* cosd(Teta) ./ cosd(Teta+Slope));
MaskWidth = max(step, MaskWidth);
%{
figure; plot(MaskWidth1, 'k'); hold on; grid on; plot(MaskWidth2, 'k'); plot(MaskWidth, 'b')
%}

%% Commun apr�s cette ligne

NbSamplesForPulseLength = nbSamplesForPulseLength(SampleRate, TxPulseWidth);

%% Increase the pulse width

MaskWidth = MaskWidth + (coefEnlargeMask * NbSamplesForPulseLength);
AmpPingNormHorzMax  = AMax;

%% Change of time scale and origin for RO, RMax1 & RMax2

RMax1 = floor((RMax1 -0.5) * step);
RMax2 = floor((RMax2 -0.5) * step);
R1SamplesFiltre = floor(R1SamplesFiltre);
R0 = OriginalRange(R0, 0, step);


function R = OriginalRange(r, RMin, step)
R = (r+(RMin-1)-0.5) * step;

function display_image(Fig, Amp, Titre)
if isempty(Fig)
    return
end
figure(Fig);
ha = subplot(1,7,1);
Moy = mean(  Amp, 2, 'omitnan')';
Med = median(Amp, 2, 'omitnan')';
nbSamples = length(Med);
plot(Moy, 1:nbSamples); grid on; set(ha, 'YDir', 'Reverse'); title('Mean (b)- Med (r)'); 
hold on; plot(Med, 1:nbSamples, 'r'); hold off;
subplot(1,7,2:6)
imagesc(Amp); colorbar; title(Titre)
ha = subplot(1,7,7);
plot(Moy-Med, 1:nbSamples); grid on; set(ha, 'YDir', 'Reverse'); title('Mean-Median')


function Amp = matrix_compensation_Matlab(Amp)
MedianHorz = median(Amp, 2, 'omitnan')';
nbSamples  = size(Amp,1);
for iSample=1:nbSamples
    if MedianHorz(iSample) == 0
        Amp(iSample,:) = NaN;
    else
        Amp(iSample,:) = Amp(iSample,:) / MedianHorz(iSample);
    end
end


