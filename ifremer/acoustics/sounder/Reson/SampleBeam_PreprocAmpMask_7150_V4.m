function [R0, RMax1, RMax2, AmpPingNormHorzMax, iBeamMax0, R1SamplesFiltre, MaskWidth, iBeamBeg, iBeamEnd] = ...
    SampleBeam_PreprocAmpMask_7150_V4(Amp, step, Teta, ReceiveBeamWidth, SampleRate, TxPulseWidth, ...
    DepthMin, DepthMax, IdentAlgo, DisplayLevel, iPing, PingSequence, SystemSerialNumber)

global TestPlatformCmatlab %#ok<GVMIS>

switch SystemSerialNumber
    case 7150
        coefEnlargeMask = 1;
        R0Marge = 4;
    case 7111
        coefEnlargeMask = 4;
        R0Marge = 0; % Tel que dans code pr�c�dent
%         R0Marge = 8; % Test� le 10/09/2014 pour fichier EvalHydro, Canyon, 7111, 20080220_165919
    otherwise
        % TODO : tester sur donn�es 7125
        coefEnlargeMask = 4;
        R0Marge = 0;
end

% TODO : tester
% NbStrips = 81;
NbStrips = floor(size(Amp,2)/14); % Modifi� par JMA le 02/07/2014 pour test


Rmin = (abs(DepthMin) ./ cosd(Teta)) * 2 * (SampleRate / 1500) / step;
Rmax = (abs(DepthMax) ./ cosd(Teta)) * 2 * (SampleRate / 1500) / step;
% figure; plot(Rmin); grid on; hold on; plot(Rmax); hold off;

nbBeams   = size(Amp,2);
Amp = Amp(1:end-2,:);
nbSamples = size(Amp,1);
    
%% Computation of the minimum range 

if DisplayLevel == 3
    Fig1 = [];
    Fig2 = [];
    Fig3 = 5656;
else
    Fig1 = [];
    Fig2 = [];
    Fig3 = [];
end

% AmpOriginal = Amp;
display_image(Fig1, Amp, 'Originale')
Start = 2;

%% Reson7150_R0_estimation_V1

if any(TestPlatformCmatlab.Reson7150_R0_estimation_V1 == [1 3 4])
    [R0_OnOriginal_M, R0StripOriginal_M] = Reson7150_R0_estimation_V1(Amp, NbStrips, Start, repmat(nbSamples, 1, nbBeams), 10, Rmin, Rmax, Fig3+1, [], iPing, DisplayLevel);
end

if any(TestPlatformCmatlab.Reson7150_R0_estimation_V1 == [2 3 4])
    [R0_OnOriginal_C, R0StripOriginal_C] = Reson7150_R0_estimation_V1_reson(...
        single(Amp), ...
        double(NbStrips), double(Start), repmat(double(nbSamples), double(NbStrips), nbBeams), double(10), double(Rmin), double(Rmax), double(step));
end

if any(TestPlatformCmatlab.Reson7150_R0_estimation_V1 == [3 4])
    compareR0StripOriginal(R0StripOriginal_C, R0StripOriginal_M, R0_OnOriginal_C, R0_OnOriginal_M, [], [], iPing)
end

switch TestPlatformCmatlab.Reson7150_R0_estimation_V1
    case {1; 3}
        R0_OnOriginal   = R0_OnOriginal_M;
        R0StripOriginal = R0StripOriginal_M;
    case {2; 4}
        R0_OnOriginal   = R0_OnOriginal_C;
        R0StripOriginal = R0StripOriginal_C;
end

%% Matrix_compensation

if any(TestPlatformCmatlab.matrix_compensation == [1 3 4])
    Amp_M = matrix_compensation_Matlab(Amp);
end

if any(TestPlatformCmatlab.matrix_compensation == [2 3 4])
    Amp_C = matrix_compensation_reson(...
        single(Amp));
end

if any(TestPlatformCmatlab.matrix_compensation == [3 4])
    subNonNaN = ~isnan(Amp_M);
    if  ~isequal(Amp_C(subNonNaN), Amp_M(subNonNaN))
        figure('Name', [' BDA : divergence C / Matlab Ping ' num2str(iPing)]);
        imagesc(Amp_C - Amp_M); colorbar
        str = sprintf('matrix_compensation RESON\ncalled in SampleBeam_PreprocAmpMask_7150_V4');
        title(str, 'Interpreter', 'None')
%         imagesc(Amp_C - Amp_M, [-20 200]); colorbar
        SonarScope(Amp_C - Amp_M)
    end
end

switch TestPlatformCmatlab.matrix_compensation
    case {1; 3}
        Amp = Amp_M;
    case {2; 4}
        Amp = Amp_C;
end

display_image(Fig2, Amp, 'Compensated')

%% Reson7150_R0_estimation_V1

SeuilAmp = 4;
Start = 1;

if any(TestPlatformCmatlab.Reson7150_R0_estimation_V1 == [1 3 4])
    [R0_M, R0Strip_M] = Reson7150_R0_estimation_V1(Amp, NbStrips, Start, repmat(nbSamples, 1, nbBeams), SeuilAmp, Rmin, Rmax, Fig3, [0 4], iPing, DisplayLevel);
end

if any(TestPlatformCmatlab.Reson7150_R0_estimation_V1 == [2 3 4])
    [R0_C, R0Strip_C] = Reson7150_R0_estimation_V1_reson(...
    	single(Amp), ...
        double(NbStrips), double(Start), repmat(double(nbSamples), double(1), double(nbBeams)), double(SeuilAmp), double(Rmin), double(Rmax), double(step));
end


if any(TestPlatformCmatlab.Reson7150_R0_estimation_V1 == [3 4])
    compareR0StripOriginal(R0StripOriginal_C, R0StripOriginal_M, R0_OnOriginal_C, R0_OnOriginal_M, [], [], iPing)
end

switch TestPlatformCmatlab.Reson7150_R0_estimation_V1
    case {1; 3}
        R0      = R0_M;
        R0Strip = R0Strip_M;
    case {2; 4}
        R0      = R0_C;
        R0Strip = R0Strip_C;
end

%{
pppp = R0StripOriginal ./ R0Strip;
figure; plot(pppp, '+'); grid on;
figure; plot(R0StripOriginal, 'xk'); grid on; hold on; plot(R0Strip, '+r'); set(gca, 'YDir', 'Reverse');
%}

if ~isnan(R0_OnOriginal) && isnan(R0)
    R0 = R0_OnOriginal;
end

if IdentAlgo ~= 3
    if R0_OnOriginal > (R0+R0Marge)
        R0 = R0_OnOriginal;
        subFaux = find(R0Strip < R0_OnOriginal);
        R0Strip(subFaux) = R0StripOriginal(subFaux);
    end
end

subTrajetDouble = find((R0Strip >= (1.9*R0StripOriginal)) & (R0Strip <= (2.1*R0StripOriginal)));
R0Strip(subTrajetDouble) = R0StripOriginal(subTrajetDouble);
% figure; plot(R0Strip ./ R0StripOriginal, '*'); grid on;

if isnan(R0)
    R0 = NaN;
    RMax1 = NaN(1,nbBeams);
    RMax2 = NaN(1,nbBeams);
    R1SamplesFiltre = NaN(1,nbBeams);
    iBeamMax0 = NaN;
    AmpPingNormHorzMax = NaN(1,nbBeams);
    MaskWidth = NaN(1,nbBeams);
    iBeamBeg = [];
    iBeamEnd = [];
    return
end

SeuilAmp = 2;
Start = max(R0-2, 1);

R0Strip2 = R0Strip;
R02 = R0;

if any(TestPlatformCmatlab.Reson7150_R0_estimation_V1 == [1 3 4])
    [R0_M, R0Strip_M, flagBeams_M] = Reson7150_R0_estimation_V1(Amp, NbStrips, Start, 2*R0Strip, SeuilAmp, Rmin, Rmax, Fig3*2, [0 4], iPing, DisplayLevel);
end

if any(TestPlatformCmatlab.Reson7150_R0_estimation_V1 == [2 3 4])
    [R0_C, R0Strip_C, flagBeams_C] = Reson7150_R0_estimation_V1_reson(...
        single(Amp), ...
        double(NbStrips), double(Start), double(2*R0Strip), double(SeuilAmp), double(Rmin), double(Rmax), double(step));
end

if any(TestPlatformCmatlab.Reson7150_R0_estimation_V1 == [3 4])
    compareR0StripOriginal(R0StripOriginal_C, R0StripOriginal_M, R0_OnOriginal_C, R0_OnOriginal_M, flagBeams_C, flagBeams_M, iPing)
end

switch TestPlatformCmatlab.Reson7150_R0_estimation_V1
    case {1; 3}
        R0        = R0_M;
        R0Strip   = R0Strip_M;
        flagBeams = flagBeams_M;
    case {2; 4}
        R0        = R0_C;
        R0Strip   = R0Strip_C;
        flagBeams = flagBeams_C;
end

if isnan(R0)
    R0 = 1;
end

% Ajout provenant de 711_V6 : � tester sur 7150 %%%%%%%%%%%%%%%%%%%%%%%%%%
if R0 < R02
    sub = find(R0Strip < R02);
    R0Strip(sub) = R0Strip2(sub);
    R0 = R02;
end

%{
% Test PP? 22/07/2008
sub = find(isnan(R0Strip) & ~isnan(R0StripOriginal) & (R0StripOriginal > max(R0Strip)));
% figure; plot(R0Strip, '+b'); grid on; hold on; plot(sub, R0StripOriginal(sub), 'xk');
R0Strip(sub) = R0StripOriginal(sub);
flagBeams(sub) = 1;
%}

%{
figure; 
h(1) = subplot(2,1,1); plot(R0Strip, '*'); grid on; set(gca, 'YDir', 'Reverse'); 
h(2) = subplot(2,1,2); plot(QF, '*'); grid on;
linkaxes(h, 'x')
%}

% figure; plot(R0Strip, 'b+'); grid on; hold on; plot(R0StripOriginal, 'rx'); set(gca, 'YDir', 'Reverse');

% figure; plot(R0Strip, 'b+'); grid on; hold on; plot(R0StripSeuil, 'rx'); plot(min(R0Strip, R0StripSeuil), 'k'); set(gca, 'YDir', 'Reverse');
% R0Strip = min(R0Strip, R0StripSeuil);

% % Cas survenu sur fichier F:\Feb2008\7111\Rock\20080219_233937.s7k ping 920
% Still useful ???????????????????????????????
if R0 == 1
    R0                  = NaN;
    RMax1               = NaN(1,nbBeams);
    RMax2               = NaN(1,nbBeams);
    R1SamplesFiltre     = NaN(1,nbBeams);
    iBeamMax0           = NaN(1,nbBeams);
    AmpPingNormHorzMax  = NaN(1,nbBeams);
    MaskWidth           = NaN(1,nbBeams);
    iBeamBeg = [];
    iBeamEnd = [];
    return
end

AMedian = Inf(1,nbBeams);
for iBeam=1:nbBeams
    if flagBeams(iBeam)
        AMedian(iBeam) = median(Amp(:,iBeam), 'omitnan');
    end
end

for iBeam=1:nbBeams
    if flagBeams(iBeam)
        iSampleBeg = max(floor(R0-3 + (R0Strip(iBeam)-R0)/3), R0-3);
        iSampleEnd = min(floor(1.4*R0Strip(iBeam)), nbSamples);

        iSampleBeg = max(iSampleBeg, floor(Rmin(iBeam)));
        iSampleEnd = min(iSampleEnd, ceil(Rmax(iBeam)));
        
        if iSampleEnd >= 1
            Amp(iSampleEnd:end,iBeam) = NaN;
        end
        Amp(1:iSampleBeg,iBeam) = NaN;
    else
        Amp(:,iBeam) = NaN;
    end
end

Offset = R0-1;
Amp = Amp(Offset:end,:);
nbSamples = size(Amp,1);

%% Search for the maximum amplitude along each beam starting from R0

RMax1 = ones(1,nbBeams) * nbSamples;
AMax = NaN(1,nbBeams);
for iBeam=1:nbBeams
    if flagBeams(iBeam)
%         [AMax(iBeam), RMax1(iBeam)] = max(Amp(:,iBeam));

% Added SB 16/05/2008
        [aMax, rMax1] = max(Amp(:,iBeam));
        if aMax > 2*AMedian(iBeam)
            AMax(iBeam) =  aMax;
            RMax1(iBeam) = rMax1;
        end
    end
end
% RMax1Max = RMax1 + (Offset-1);

%{
figure; imagesc(1:size(Amp,2), (1:size(Amp,1)), Amp); hold on; plot(RMax1, 'xr'); hold off;
%}

%% Search for the maximum amplitude along each beam starting from R0

for iBeam=1:nbBeams
    if flagBeams(iBeam)
        a = Amp(:,iBeam);
        iDeb = find(isnan(a(1:RMax1(iBeam))), 1, 'last');
        
        % Add JM 28/05/2014 Begin
        if isempty(iDeb)
            iDeb = 0;  
        end
        % Add JM 28/05/2014 End
        
        X = min(RMax1(iBeam)-iDeb, nbSamples-RMax1(iBeam));
        sub = RMax1(iBeam)-X:RMax1(iBeam)+X;
        sub(sub < 1) = [];
        sub(sub>length(a)) = [];
        if length(sub) < 5
            continue
        end
        
        %% detectionAmplitudePingOnCompensatedImage
        
        if any(TestPlatformCmatlab.detectionAmplitudePingOnCompensatedImage == [1 3 4])
            rMax1_M = detectionAmplitudePingOnCompensatedImage(1:size(Amp,1), a(sub), iPing, iBeam, Teta(iBeam), DisplayLevel, nbSamples);
        end
        
        if any(TestPlatformCmatlab.detectionAmplitudePingOnCompensatedImage == [2 3 4])
            inputSignal = a;
            subSample = find(~isnan(inputSignal));
            inputSignal(isnan(inputSignal)) = 0;
            if subSample(end) < sub(end)
                iEnd = subSample(end);
            else
                iEnd = sub(end);
            end
            rMax1_C = detectionAmplitudePingOnCompensatedImage_reson(...
                single(inputSignal), ...
                double(sub(1)), double(iEnd), double(RMax1(iBeam)));
        end
        
        if any(TestPlatformCmatlab.detectionAmplitudePingOnCompensatedImage == [3 4])
            MOut(iBeam) = rMax1_M + sub(1); %#ok<AGROW>
            COut(iBeam) = rMax1_C; %#ok<AGROW>
            if max(abs((rMax1_M + sub(1)) - rMax1_C)) > 0
                str = sprintf('C and Matlab detectionAmplitudePingOnCompensatedImage comparison failed :\nresult %d -> %d\n', rMax1_M + sub(1), rMax1_C);
                my_warndlg(str, 0, 'Tag', 'TestPlatformCmatlab.detectionAmplitudePingOnCompensatedImage');
                
            end
        end
        
        switch TestPlatformCmatlab.detectionAmplitudePingOnCompensatedImage
            case {1; 3}
                rMax1 = rMax1_M;
            case {2; 4}
                rMax1 = rMax1_C - sub(1);
        end
        
        if ~isnan(rMax1) && (rMax1 > 1) % Rajout� le 18/06/2014 pour faisceau 354 ping 15 du fichier  U:\data\augustin\Reson\EvalHydro\Mar2008\7150\Canaries\24_MULTI\20080328_090049.s7k
        	RMax1(iBeam) = rMax1 + (sub(1)-1-0.5);
        end  
    end
end

if any(TestPlatformCmatlab.detectionAmplitudePingOnCompensatedImage == [3 4])
    if max(abs(COut - MOut)) > 0
        figure('Name', 'Erreur detectionAmplitudePingOnCompensatedImage', 'NumberTitle', 'off');
        plot(COut - MOut, '-*'); grid on;
    end
end

flagBeams(RMax1 == nbSamples) = 0;
RMax1 = floor(RMax1 + (Offset-1) + 0.5);
RMax1(flagBeams == 0) = NaN;

if all(isnan(RMax1))
    R0 = NaN;
    RMax1 = NaN(1,nbBeams);
    RMax2 = NaN(1,nbBeams);
    R1SamplesFiltre = NaN(1,nbBeams);
    iBeamMax0 = NaN;
    AmpPingNormHorzMax = NaN(1,nbBeams);
    MaskWidth = NaN(1,nbBeams);
    iBeamBeg = [];
    iBeamEnd = [];
    if PingSequence == 0
        str1 = sprintf('Le ping %d semble ne pas contenir de signal.',  iPing);
        str2 = sprintf('Ping %d seems to be empty of signal.',  iPing);
    else
        str1 = sprintf('Le ping %d, PingSequence=%d semble ne pas contenir de signal.',  iPing, PingSequence);
        str2 = sprintf('Ping %d, PingSequence=%d seems to be empty of signal.',  iPing, PingSequence);
    end
    my_warndlg(Lang(str1,str2), 0, 'Tag', 'ImageBizarrePourDetectionEnveloppeSignal');
    return
end

%{
figure; imagesc(1:size(Amp,2), (1:size(Amp,1))+R0-2, Amp); hold on; plot(RMax1, 'xr'); plot(RMax1Max, 'ok'); hold on;
%}


%% Identification of the beam number corresponding to R0

pppp = min(RMax1);
sub = find(RMax1 == pppp);
if isempty(sub)
    R0 = NaN;
    RMax1 = NaN(1,nbBeams);
    RMax2 = NaN(1,nbBeams);
    R1SamplesFiltre = NaN(1,nbBeams);
    iBeamMax0 = NaN;
    AmpPingNormHorzMax = NaN(1,nbBeams);
    MaskWidth = NaN(1,nbBeams);
    iBeamBeg = NaN;
    iBeamEnd = NaN;
    return
end
iBeamMax0Full = floor((sub(1) + sub(end))/2);

pppp = RMax1;
AMaxMoyen = mean(AMax, 'omitnan');
pppp(AMax < AMaxMoyen) = Inf;

% [~, iBeamMax0] = min(pppp);
qqqq = min(pppp);
sub = find(pppp == qqqq);
iBeamMax0 = floor((sub(1) + sub(end))/2);

MS2 = nbBeams / 2;
if abs(iBeamMax0Full-MS2) < abs(iBeamMax0-MS2)
    iBeamMax0 = iBeamMax0Full;
end

if (iBeamMax0 == 1) || (iBeamMax0 == nbBeams)
    iBeamMax0 = floor(nbBeams/2);
else
    k = iBeamMax0;
    while 1
        k = k + 1;
        if k >= nbBeams
            break
        end
        if floor(RMax1(k)) <= floor(RMax1(k+1))
            iBeamMax0 = k + 1;
        else
            break
        end
    end
    k = iBeamMax0;
    while 1
        k = k - 1;
        if k <= 1
            break
        end
        if floor(RMax1(k-1)) <= floor(RMax1(k))
            iBeamMax0 = k - 1;
        else
            break
        end
    end
end
%{
figure; 
h(1) = subplot(2,1,1);
plot(RMax1); grid on; set(h(1), 'YDir', 'reverse');
h(2) = subplot(2,1,2);
plot(AMax); grid on; hold on;
plot([1 length(AMax)], [AMaxMoyen AMaxMoyen], 'r');
plot(iBeamMax0, AMax(iBeamMax0), 'k*');
hold off;
linkaxes(h, 'x')
%}

AMax2 = AMax .^ 2;
AMax2(isnan(AMax2)) = 0;
pppp = cumsum(AMax2(1:iBeamMax0));
pppp = pppp / max(pppp);
iBeamBeg = find(pppp >= 0.005, 1, 'first');

pppp = cumsum(AMax2(iBeamMax0:end));
pppp = pppp / max(pppp);
iBeamEnd = iBeamMax0 - 1 + find(pppp > 0.995, 1, 'first');

%{
figure;
plot(AMax2); grid on; hold on;
plot([1 length(AMax2)], [MeanAMax2 MeanAMax2], 'r');
plot(iBeamBeg, AMax2(iBeamBeg), '*r'); 
plot(iBeamEnd, AMax2(iBeamEnd), '*r'); 
hold off;
%}
% subBeamsNaN = [1:(iBeamBeg-1) (iBeamEnd+1):nbBeams];
% RMax1(subBeamsNaN) = NaN;
% Amp(:,subBeamsNaN) = NaN;

%% Second serie of maximals

RMax2 = RMax1;
for iBeam=2:(nbBeams-1)
    RMax2(iBeam) = (RMax1(iBeam-1)+RMax1(iBeam+1)) / 2;
end
%{
figure; imagesc(Amp); hold on; plot(RMax1, '*k'); plot(RMax2, 'ob')
%}

R0 = min(R0,min(RMax1));

if DisplayLevel == 3
    hFig = figure(56557);
    set(hFig, 'Name', 'Range detection on reduced Amplitude image')
    imagesc(1:size(Amp,2), (1:size(Amp,1))+(Offset-1), Amp); colorbar;
    hold on;
%     plot(1:nbBeams, repmat(R0 ,1,nbBeams), '*c');
    plot(1:nbBeams, RMax1, '*c')
    plot(1:nbBeams, RMax2, 'ok')
    hold off
end

%% Local Time spreading of the signal around the maximum

R1Samples = floor((RMax1 -0.5) * step);
R1SamplesFiltre = suppres_steps_V2(R1Samples);
% [R1SamplesFiltre, subR1SamplesFiltreNaN] = suppres_steps_V3(R1Samples);

%{
figure; PlotUtils.createSScPlot(R1Samples); grid on; hold on; PlotUtils.createSScPlot(R1SamplesFiltre, 'r'); hold off; title('R1Samples')
%}
Z = -R1SamplesFiltre .* cosd(Teta);
X = R1SamplesFiltre .* sind(Teta);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% To be used for Lisboa files (june 2008)
% ReceiveBeamWidth = ReceiveBeamWidth * 2;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

MaskWidth = ceil(-Z.* abs(( 1./ cosd(abs(Teta) + ReceiveBeamWidth/2))  - ( 1./ cosd(abs(Teta) - ReceiveBeamWidth/2))));
% figure; plot(Teta, '-*'); grid
% figure; plot(MaskWidth, '-*'); grid

%% Gradient

if any(TestPlatformCmatlab.gradient == [1 3 4])
    GZ_M = gradient(Z);
    GX_M = gradient(X);
end

if any(TestPlatformCmatlab.gradient == [2 3 4])
    GZ_C = gradient_reson(double(Z));
    GX_C = gradient_reson(double(X));
end

if any(TestPlatformCmatlab.gradient == [3 4])
    if(max(abs(GZ_M - GZ_C)) > 0.001)
        FigUtils.createSScFigure('Name', 'Erreur gradient RESON', 'NumberTitle', 'off');
        h(1) = subplot(2,1,1); PlotUtils.createSScPlot(GZ_M, 'b'); hold on; PlotUtils.createSScPlot(GZ_C, 'r'); grid on;
        title('gradient'); legend({'M'; 'C'})
        h(2) = subplot(2,1,2); PlotUtils.createSScPlot(GZ_M - GZ_C, '-*'); grid on;
        title('Ecart gradient Matlab RESON'); legend('M - C');
        linkaxes(h, 'x')
    end
end

switch TestPlatformCmatlab.gradient
    case {1; 3}
        GZ = GZ_M;
        GX = GX_M;
    case {2; 4}
        GZ = GZ_C;
        GX = GX_C;
end

Slope = atan2(GZ, GX) * (180/pi);

% figure; PlotUtils.createSScPlot(gradient(X), 'b'); grid on; title('gradient(X)');
% figure; PlotUtils.createSScPlot(gradient(Z), 'b'); grid on; title('gradient(Z)');

%{
figure;
h(1) = subplot(2,1,1); plot(X, Slope); grid on; title('Slope');
h(2) = subplot(2,1,2); plot(X,Z); grid on;
linkaxes(h, 'x')
figure;
h(1) = subplot(2,1,1); plot(Slope); grid on; title('Slope');
h(2) = subplot(2,1,2); plot(Z); grid on;
linkaxes(h, 'x')
%}

MaskWidth0 = MaskWidth;
MaskWidth  = ceil(MaskWidth .* cosd(Teta) ./ cosd(min(85, abs(Teta-Slope))));
MaskWidth(isnan(Slope)) = NaN;
% figure; PlotUtils.createSScPlot(MaskWidth); grid on;

% Test Brest 20/06/2008 trial to retreive Yevgeniy Mask to get good mask
% wher there is a slope (Ping Nu 2 (PingCounter=12)on Canyon 24 kHz 20080220_131710)
DiffMaskWidth = MaskWidth - MaskWidth0;
DiffMaskWidth(DiffMaskWidth < 0) = 0;
DiffMaskWidth2 = DiffMaskWidth;
L = 10;
for iBeam=1+L:nbBeams-L
    S = DiffMaskWidth(iBeam-L:iBeam+L);
    DiffMaskWidth2(iBeam) = max(abs(S));
end
MaskWidth = MaskWidth0 + DiffMaskWidth2;

%{
figure;
h(1)= subplot(3,3,1); plot(X,Z); grid on; title('Bathy');
h(2)= subplot(3,3,4); plot(X,Slope); grid on; title('Slope');
h(3)= subplot(3,3,7); plot(X,MaskWidth); grid on; title('MaskWidth'); hold on; plot(X,MaskWidth0, 'k'); hold off;
h(4)= subplot(3,3,[2 5 8]); plot(X,Z); grid on; title('Bathy'); axis equal;
linkaxes(h, 'x')
h2(1)= subplot(3,3,3); plot(Z); grid on; title('Bathy');
h2(2)= subplot(3,3,6); plot(Slope); grid on; title('Slope');
h2(3)= subplot(3,3,9); plot(MaskWidth); grid on; title('MaskWidth'); hold on; plot(MaskWidth0, 'k'); hold off;
linkaxes(h2, 'x')
figure; plot(Teta, 'b'); hold on; plot(Teta+Slope, 'k'); hold off; title('Teta'); grid on;
figure; plot(Teta, 'b'); hold on; plot(Teta-Slope, 'k'); hold off; title('Teta'); grid on;
figure; plot(abs(Teta), 'b'); hold on; plot(abs(Teta-Slope), 'k'); hold off; title('Teta'); grid on;
figure; plot(abs(Teta-Slope)); title('abs(Teta-Slope)'); grid on;

figure; PlotUtils.createSScPlot(MaskWidth); grid on; title('MaskWidth'); hold on; PlotUtils.createSScPlot(MaskWidth0, 'k'); hold off;

NbSamplesForPulseLength = nbSamplesForPulseLength(SampleRate, TxPulseWidth);
figure; PlotUtils.createSScPlot(MaskWidth + 4 * NbSamplesForPulseLength); grid on; title('MaskWidth'); hold on; PlotUtils.createSScPlot(MaskWidth0 + 4 * NbSamplesForPulseLength, 'k'); hold off; 
%}

MaskWidth = max(MaskWidth, MaskWidth0);
MaskWidth = max(step, MaskWidth);

%{
% Test for file 20080220_134108 ping 436
MaskWidthDiff = MaskWidth - MaskWidth0;
nb = 3;
for iBeam=(1+nb):(nbBeams-nb)
    MaskWidthDiff(iBeam) = max(MaskWidth(iBeam-nb:iBeam+nb));
end
MaskWidth = MaskWidth0 + MaskWidthDiff;
%}

%% Commun apr�s cette ligne

NbSamplesForPulseLength = nbSamplesForPulseLength(SampleRate, TxPulseWidth);

%% Increase the pulse width

MaskWidth = MaskWidth + (coefEnlargeMask * NbSamplesForPulseLength);
AmpPingNormHorzMax = AMax;

%% Change of time scale and origin for RO, RMax1 & RMax2

RMax1 = floor((RMax1 -0.5) * step);
RMax2 = floor((RMax2 -0.5) * step);
R0 = OriginalRange(R0, 0, step);

%{
figure; PlotUtils.createSScPlot(RMax1, 'b'); grid on; title('RMax1 + Mask', 'Interpreter', 'none'); 
hold on;
PlotUtils.createSScPlot(R1SamplesFiltre, 'r') 
PlotUtils.createSScPlot(RMax1+MaskWidth, 'k')
PlotUtils.createSScPlot(RMax1-MaskWidth, 'k')
hold off; set(gca, 'YDir', 'Reverse');

figure; PlotUtils.createSScPlot(Slope, 'b'); grid on; title('Slope', 'Interpreter', 'none');
figure; PlotUtils.createSScPlot(MaskWidth, 'b'); grid on; title('MaskWidth', 'Interpreter', 'none');
%}


function R = OriginalRange(r, RMin, step)
R = (r+(RMin-1)-0.5) * step;

function display_image(Fig, Amp, Titre)
if isempty(Fig)
    return
end
figure(Fig);
ha = subplot(1,7,1);
Moy = mean(  Amp, 2, 'omitnan')';
Med = median(Amp, 2, 'omitnan')';
nbSamples = length(Med);
plot(Moy, 1:nbSamples); grid on; set(ha, 'YDir', 'Reverse'); title('Mean (b)- Med (r)'); 
hold on; plot(Med, 1:nbSamples, 'r'); hold off;
subplot(1,7,2:6)
imagesc(Amp); colorbar; title(Titre)
ha = subplot(1,7,7);
plot(Moy-Med, 1:nbSamples); grid on; set(ha, 'YDir', 'Reverse'); title('Mean-Median')


function Amp = matrix_compensation_Matlab(Amp)
MedianHorz = median(Amp, 2, 'omitnan')';
nbSamples  = size(Amp,1);
for iSample=1:nbSamples
    if MedianHorz(iSample) == 0
        Amp(iSample,:) = NaN;
    else
        Amp(iSample,:) = Amp(iSample,:) / MedianHorz(iSample);
    end
end


function compareR0StripOriginal(R0StripOriginal_C, R0StripOriginal_M, R0_OnOriginal_C, R0_OnOriginal_M, flagBeams_C, flagBeams_M, iPing)

global TestPlatformCmatlab %#ok<GVMIS>

BDA_compare_Matlab_C('Reson7150_R0_estimation_V1', 'R0StripOriginal', R0StripOriginal_M, R0StripOriginal_C, iPing, [], TestPlatformCmatlab.Seuil_Reson7150_R0_estimation_V1_R0StripOriginal);
BDA_compare_Matlab_C('Reson7150_R0_estimation_V1', 'flagBeams',       flagBeams_M,       flagBeams_C,       iPing, [], TestPlatformCmatlab.Seuil_Reson7150_R0_estimation_V1_flagBeams);

if (R0_OnOriginal_C ~= R0_OnOriginal_M) && ~isnan(R0_OnOriginal_M)
    BDA_createMes('Reson7150_R0_estimation_V1', 'R0_OnOriginal', R0_OnOriginal_M, R0_OnOriginal_C, iPing, [], 0);
end
