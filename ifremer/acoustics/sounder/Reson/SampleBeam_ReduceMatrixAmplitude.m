function [AmplitudeReduite, step, yAmplitudeReduite, R0_FirstEstimation] = SampleBeam_ReduceMatrixAmplitude(Amp, varargin)

[varargin, StepMax] = getPropertyValue(varargin, 'StepMax', []); %#ok<ASGLU>

nbBeams   = size(Amp,2);
nbSamples = size(Amp,1);

N_m = 0.04 * nbSamples + 190;
%N_m=0.04*nbSamples+190;

step = max(1,round(nbSamples/N_m));

if ~isempty(StepMax)
    step = min(step, StepMax);
end

if step == 1
    AmplitudeReduite = Amp;
    yAmplitudeReduite = 1:nbSamples;
    MoyHorz1 = mean(AmplitudeReduite, 2, 'omitnan');
    [~, R0_FirstEstimation] = max(MoyHorz1);
    return
end

n = max(1,floor(nbSamples/step)*step);
n_reduit = n/step;
AmplitudeReduite = NaN(n_reduit, nbBeams, 'single');
for iBeam=1:nbBeams
    a = reshape(Amp(1:n,iBeam), step, n/step);
    AmplitudeReduite(:,iBeam) = mean(a,1)';
    %     [Max, R(iBeam)] = max(AmplitudeReduite(:,iBeam));
end

AmplitudeFiltree = AmplitudeReduite;
for iBeam=2:(nbBeams-1)
    AmplitudeFiltree(:,iBeam) = mean(AmplitudeReduite(:,iBeam-1:iBeam+1), 2);
end
AmplitudeReduite = AmplitudeFiltree;

yAmplitudeReduite = floor(((1+step)/2) + (0:(n_reduit-1))*step);

%% Determination of R0

MoyHorz1 = mean(AmplitudeReduite, 2, 'omitnan');
[~, R0_FirstEstimation] = max(MoyHorz1);


