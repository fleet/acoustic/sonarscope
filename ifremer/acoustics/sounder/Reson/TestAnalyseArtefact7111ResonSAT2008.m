% save C:\TEMP\RollPitch Roll Pitch

figure; PlotUtils.createSScPlot(Roll); grid
figure; plot(-Roll', 'b'); hold on; plot(Pitch, 'r'); grid on; legend({'Roll', 'Pitch'});

N = length(Roll);
n = 11;
C = NaN(1,N);
for i=n+1:N-n
    sub = i-n:i+n;
    C(i) = corr(Roll(sub)', Pitch(sub)');
end
figure; PlotUtils.createSScPlot(C); grid
figure; PlotUtils.createSScPlot(C,1:length(C)); grid
figure; PlotUtils.createSScPlot(abs(C),1:length(C)); grid
