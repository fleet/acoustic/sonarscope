function [tetav, Diff] = VraisAngles_7150_12kHz

tetamax = 75;   % Angle inclinaison max
% tetamax = 75.3869;
NbFx = 880;     % Nbre faisceaux
% df = 1500;      % ecart de frequence
f0 = 12000;     % freq moyenne 12 kHz
EcartFr = 1000; % �cart entre les fr�quences multiping

iteta = 1:880;
% teta = zeros(NbFx);

teta = atand(2 * tand(tetamax)/NbFx * (iteta-440.5));   % angles nominaux

tetar = 1 + 2 * round ((teta - 1) / 2); % angle impair le plus proche

gamanom = teta - tetar;    % ecart angle nominal par rapport angles impairs

tetav = zeros(4, NbFx);

for ifr = 1:4
    df = (ifr - 2.5) * EcartFr;     % ecart frequentiel / 12 kHz
    dgama = -gamanom * df / f0;    % ecart angulaire lie a la frequence
    tetav(ifr,:) = teta(:) + dgama(:);           % angle vrai de pointage
end

H = 600;
Rv = H./ cosd(tetav);
% Xv = H * tand(tetav);


Zf = zeros(4, NbFx);
Xf = zeros(4, NbFx);

for ifr=1:4
    Zf(ifr,:) = Rv(ifr,:).* cosd(teta);
    Xf(ifr,:) = Rv(ifr,:).* sind(teta);
end

Diff = tetav - repmat(teta,4,1);
tetav = flipud(tetav);
%{
Zv = ones(1, NbFx)* H;
C = 'kbrmc';
figure; plot(teta, Zv);
for ifr=1:4
    hold on; plot(teta(:), Zf(ifr,:),C(ifr));
end

figure;plot(tetav');

figure;
plot(teta);
hold on;
plot(tetar,'r');
hold on;
plot(tetav,'g');
%}

