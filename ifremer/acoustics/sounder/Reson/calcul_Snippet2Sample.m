function [ReflecPingSamplesdB, RxBeamIndexPingSamples, NbpPingSamples, AnglesPingSamples] = calcul_Snippet2Sample(NbCol, ColMin, subBeamsReson, BeamDescriptor, SamplesPingSnippetEnr, AnglesPingSnippet, RangeCentrePingBeam)

% NbBeams = length(RangeCentrePingBeam);
NbBeams = length(BeamDescriptor);

BeamIndexPingSnippet    = NaN(1, 100*NbBeams, 'single');
RangePingSnippet        = NaN(1, 100*NbBeams, 'single');
AnglesCentrePingSnippet = NaN(1, 100*NbBeams, 'single');

ReflecPingSamplesdB       = zeros(1, NbCol, 'single');
RxBeamIndexPingSamples    = zeros(1, NbCol, 'single');
NbpPingSamples            = zeros(1, NbCol, 'single');
AnglesPingSamples         = NaN(  1, NbCol, 'single');
IndexBeamMoyenPingSamples = zeros(1, NbCol, 'single');

% figure; plot(RangeCentrePingBeam, '+'); grid on; title('RangeCentrePingBeam')
% figure; plot(AnglesPingSnippet, '+'); grid on; title('AnglesPingSnippet')

%{
n = length(RangeCentrePingBeam);
r = abs(RangeCentrePingBeam);
Diff = diff(r);
Deb = NaN(1,n);
Deb(1) = r(1);
Deb(2:n) = r(2:n) - floor(Diff(1:n-1)/2);
Fin = NaN(1,n);
Fin(1:n-1) = r(1:n-1) + floor(Diff/2);
Fin(n) = r(n);
figure; plot(r, '*k'); grid on; hold on; plot(Deb, '*r'); plot(Fin, '*g')
%}

Range0 = (-49.5:49.5) - 0.5;
% for iFais=1:size(BeamDescriptor, 2) % DataSnippets.NbBeams(subDataSnippets(i))
for k=1:length(subBeamsReson)
    iFais = subBeamsReson(k);
    if isempty(BeamDescriptor(iFais))
        continue
    end
    subc = (1+(iFais-1)*100):(iFais*100);
    subc2 = (1+(k-1)*100):(k*100);
    
    if subc(51) > length(AnglesPingSnippet)
        continue
    end
    
    RangePingSnippet(subc)        = Range0 * sign(RangeCentrePingBeam(k)) + RangeCentrePingBeam(k);
    AnglesCentrePingSnippet(subc) = AnglesPingSnippet(subc2(51));
    BeamIndexPingSnippet(subc)    = BeamDescriptor(k);
end
%     figure; plot(AnglesCentrePingSnippet, '+'); grid on; title('AnglesCentrePingSnippet')
%     figure; plot(RangePingSnippet, '+'); grid on; title('RangePingSnippet')AnglesCentrePingSnippet
%     figure; plot(BeamIndexPingSnippet, '+'); grid on; title('BeamIndexPingSnippet')

subcR = round(RangePingSnippet) + (1 - ColMin);
% indCentreFaisceauPingSamples = RangeCentrePingBeam - ColMin + 1;
%     figure; plot(subcR, '+'); grid on; title('subcR')
%     figure; plot(SamplesPingSnippetEnr, '+'); grid on; title('SamplesPingSnippetEnr')
%     figure; plot(indCentreFaisceauPingSamples, '+'); grid on; title('indCentreFaisceauPingSamples')
%     figure; plot(AnglesPingSnippet); grid on; title('AnglesPingSnippet')

%     SamplesPingSnippetEnr   = this.Image(suby(sub1(i)), :);


% TODO : bug ici
if length(SamplesPingSnippetEnr) == length(subcR)
    subNonNaN =  find(~isnan(SamplesPingSnippetEnr) & (subcR >= 1) & (subcR <= NbCol));
else
    subsubcR  = 1:length(SamplesPingSnippetEnr);
    subNonNaN = find(~isnan(SamplesPingSnippetEnr) & (subcR(subsubcR) >= 1) & (subcR(subsubcR) <= NbCol));
end

SamplesPingSnippetEnr = SamplesPingSnippetEnr(subNonNaN);
BeamIndexPingSnippet  = BeamIndexPingSnippet(subNonNaN);
subcR = subcR(subNonNaN);
subcR = subcR(~isnan(subcR));
AnglesCentrePingSnippet = AnglesCentrePingSnippet(subNonNaN);
AnglesPingSnippet       = AnglesPingSnippet(subNonNaN);

% figure; plot(AnglesCentrePingSnippet); grid on; title('AnglesCentrePingSnippet')
% figure; plot(subNonNaN, subcR, '+'); grid on; title('subcR')

for k=1:length(subcR)
    j = subcR(k);
    ReflecPingSamplesdB(j) = ReflecPingSamplesdB(j) + SamplesPingSnippetEnr(k);
    NbpPingSamples(j) = NbpPingSamples(j) + 1;
    IndexBeamMoyenPingSamples(j) = IndexBeamMoyenPingSamples(j) + BeamIndexPingSnippet(k);
    if isnan(AnglesPingSamples(j))
        AnglesPingSamples(j) = AnglesPingSnippet(k);
    else
%         AngleCentre = AnglesPingSamples(indCentreFaisceauPingSamples(BeamIndexPingSnippet(k)));
        AngleCentre = AnglesCentrePingSnippet(k);
        if abs(AnglesPingSamples(j) - AngleCentre) > abs(AnglesPingSnippet(k) - AngleCentre)
            AnglesPingSamples(j) = AnglesPingSnippet(k);
        end
    end
end
% figure; plot(AnglesPingSamples, '+'); grid on; title('AnglesPingSamples')
for k=1:NbCol
    if NbpPingSamples(k) == 0
        ReflecPingSamplesdB(k) = NaN;
        RxBeamIndexPingSamples(k) = NaN;
    else
        ReflecPingSamplesdB(k) = ReflecPingSamplesdB(k) / NbpPingSamples(k);
        RxBeamIndexPingSamples(k) = round(IndexBeamMoyenPingSamples(k) / NbpPingSamples(k));
    end
end
subNaN = (NbpPingSamples == 0);
ReflecPingSamplesdB(subNaN)    = NaN;
RxBeamIndexPingSamples(subNaN) = NaN;
ReflecPingSamplesdB            = reflec_Enr2dB(ReflecPingSamplesdB);
% ReflecPingSamplesdB            = reflec_Enr2dB(ReflecPingSamplesdB - 1); % Modifi� mission BICOSE

subxxx = find(isinf(ReflecPingSamplesdB));
if ~isempty(subxxx)
    ReflecPingSamplesdB(subxxx) = NaN;
end


ReflecPingSamplesdB(subNaN)    = NaN;
RxBeamIndexPingSamples(subNaN) = NaN;
NbpPingSamples(subNaN)         = NaN;
AnglesPingSamples(subNaN)      = NaN;
