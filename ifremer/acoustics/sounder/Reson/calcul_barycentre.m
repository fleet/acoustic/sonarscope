% Calcul du centre de gravit� d'un signal
%
% Syntax
%   [x0, S] = calcul_barycentre(x, y)
%
% Input Arguments
%   x : Abscisses
%   y : Valeur du signal
%
% Output Arguments
%   [] : Auto-plot activation
%   x0 : Centre de gravit�
%   S  : Ecart-type
%
% Examples
%   x = 0:0.1:20;
%   y = normpdf(x, 10, 3)
%   y = y + 0.05 * rand(size(y));
%   FigUtils.createSScFigure; PlotUtils.createSScPlot(x, y); grid on;
%   calcul_barycentre(x, y)
%   [x0, S] = calcul_barycentre(x, y)
%
% See also calcul_barycentre1 Authors
% Authors  : JMA
%-------------------------------------------------------------------------------

function [x0, S] = calcul_barycentre(x, y)

global TestPlatformCmatlab %#ok<GVMIS>

if isempty(TestPlatformCmatlab)
    TestPlatformCmatlab.barycentre =1;
end

if any(TestPlatformCmatlab.barycentre == [1 3 4])
    [AmpBeamRange0_M, Sigma0_M] = calcul_barycentre_Matlab(y, x, 1); % Detection on square of Energy
end

if any(TestPlatformCmatlab.barycentre == [2 3 4])
    [AmpBeamRange0_C, Sigma0_C] = calcul_barycentre_reson(...
        single(y), ...
        double(x), double(1)); % Detection on square of Energy
    if Sigma0_C == -1
        Sigma0_C = NaN;
    end
end

if any(TestPlatformCmatlab.barycentre == [3 4])
    if (AmpBeamRange0_M ~= AmpBeamRange0_C) || (Sigma0_M ~= Sigma0_C) % Single / double
        status = BDA_createMes('calcul_barycentre', 'AmpBeamRange0', AmpBeamRange0_M, AmpBeamRange0_C, [], [], 0);
        if ~status
            status;
        end
        status = BDA_createMes('calcul_barycentre', 'Sigma0', Sigma0_M, Sigma0_C, [], [], 0);
        if ~status
            status;
        end
    end
end

switch TestPlatformCmatlab.barycentre
    case {1; 3}
        x0 = AmpBeamRange0_M;
        S  = Sigma0_M;
    case {2; 4}
        x0 = AmpBeamRange0_C;
        S  = Sigma0_C;
end

% Pour correction bug fichier
% EvalHysto\Mars2008\7150\Canaries\12-Single\20080328_083233 ping 22
% faisceau 684 ou 685
x0 = max(x0, x(1));
x0 = min(x0, x(end));
% Fin correction bug

if nargout == 0
    FigUtils.createSScFigure;
    PlotUtils.createSScPlot(x, y); grid on; hold on;
    Title = sprintf('Center of gravity on y^2 : x0=%s   S=%s  (horz red line = xo-3*S : xo+3*S)', num2str(x0), num2str(S));
    h = PlotUtils.createSScPlot([x0 x0], [0 max(y)], 'r');
    set(h, 'LineWidth', 2);
    h = PlotUtils.createSScPlot([x0-3*S x0+3*S], [0 0], 'r');
    set(h, 'LineWidth', 4); hold off;
    hold off;
    title(Title)
end


function [x0, S] = calcul_barycentre_Matlab(y, x, ordre)

x = x(:);
y = y(:);
if ordre ~= 1
    y = y .^ 2;
end
y  = y / sum(y);
x0  = sum(x .*  y);
VX = sum((x-x0).^2 .*  y);

if VX > 0
    S = sqrt(VX);
else
    S = NaN;
    x0 = NaN;
end