% nomDir = my_tempdir;
% nomDir = checkBDFromErmapperFiles(nomDir)
% checkBDFromErmapperFiles(nomDir, 'NoModal')

function nomDir = checkBDFromErmapperFiles(nomDir, varargin)

[varargin, flagNoModal] = getFlag(varargin, 'NoModal'); %#ok<ASGLU>

I0 = cl_image_I0;
obj = [];
while 1
    [flag, Name] = my_uigetdir(nomDir, 'Enter a directory that contains ErMapper files');
    if flag
        nomDir = Name;
    else
        return
    end

    listeFic = listeFicOnDir(nomDir, '.ers');
    [flag, a] = cl_image.import_ermapper(listeFic);
    if ~flag
        return
    end
    for k=1:length(a)
        DataType(k) = a(k).DataType; %#ok<AGROW>
    end

    indMbesQualityFactor = find(DataType == cl_image.indDataType('MbesQualityFactor'));
    indBathymetry        = find(DataType == cl_image.indDataType('Bathymetry'));

    Bathymetry = get(a(indBathymetry), 'Image'); %#ok<FNDSB>
    QF         = get(a(indMbesQualityFactor(end)), 'Image');
    QF3        = get(a(indMbesQualityFactor(end-2)), 'Image');
    clear a
    
    Bathymetry = flipud(Bathymetry);
    QF  = flipud(QF);
    QF3 = flipud(QF3);

    a = QFAdjuster(Bathymetry, QF, QF3, 'Tag', nomDir);
    if ~isempty(obj)
        Seuil     = get_Seuil(obj);
        StatusQF3 = get_StatusQF3(obj);
        Position  = get_Position(obj);
        a = set_Seuil(a, Seuil);
        a = set_StatusQF3(a, StatusQF3);
        a = set_Position(a, Position);
    end

    if flagNoModal
        editobj(a);
    else
        obj = editobj(a);
    end
end
