% nomDir = my_tempdir;
% nomDir='C:\Temp\BD7111'
% nomDir = checkBDFromS7k(nomDir)
% nomDir = checkBDFromS7k(nomDir, 'NoModal')

function nomDir = checkBDFromS7k(nomDir, varargin)

[varargin, flagNoModal] = getFlag(varargin, 'NoModal'); %#ok<ASGLU>

obj = [];

[flag, nomFic, repImport] = uiSelectFiles('ExtensionFiles', '.s7k', 'RepDefaut', nomDir);
if ~flag
    return
end

nomDir = fileparts(nomFic{1});

for k=1:length(nomFic)
    s7k = cl_reson_s7k('nomFic', nomFic{k}, 'KeepNcID', true);
    identSondeur = selectionSondeur(s7k);
    Data = read_Depth(s7k, identSondeur);

    Bathymetry = Data.Depth;
    if all(isnan(Bathymetry))
        Bathymetry = -Data.Range .* cosd(Data.BeamDepressionAngle);
    end
    QF  = Data.Reflectivity;
    QF3 = Data.QualityFactor;
    
    subNan = isnan(Bathymetry);
    QF3(subNan) = 0;
    QF3 = single(bitand(uint8(QF3),1));
    QF3(subNan) = NaN;


    [~, Titre] = fileparts(nomFic{k});
    a = QFAdjuster(Bathymetry, QF, QF3, 'Tag', Titre);
    if ~isempty(obj)
        Seuil     = get_Seuil(obj);
        StatusQF3 = get_StatusQF3(obj);
        Position  = get_Position(obj);
        a = set_Seuil(a, Seuil);
        a = set_StatusQF3(a, StatusQF3);
        a = set_Position(a, Position);
    end

    if flagNoModal
        editobj(a);
    else
        obj = editobj(a);
    end
end
