% Codes des datagrams des sondeurs Reson s7k
%
% Syntax
%   texteTypeDatagram = codesDatagramsS7k(...)
%
% Name-Value Pair Arguments
%   tabHistoTypeDatagram : Histogramme des types de datagram
%
% Name-Value Pair Arguments
%   fullListe : 0=affichage que des datagrams presents, 1=tous les datagrams
%
% Output Arguments
%   texteTypeDatagram : Chaine de caracteres
%       '1000' = Reference point
%       '1001' = Sensor offset position
%
% Examples
%   codesDatagramsS7k
%
% See also Authors
% Authors : JMA
%-----------------------------------------------------------------------

function texteTypeDatagram = codesDatagramsS7k(varargin)

[varargin, fullListe] = getPropertyValue(varargin, 'fullListe', 0);

LabelsTypeDatagram{1000} = 'Reference point';
LabelsTypeDatagram{1001} = 'Sensor Offset Position';
LabelsTypeDatagram{1002} = 'Calibrated Sensor Offset Position';
LabelsTypeDatagram{1003} = 'Position';
LabelsTypeDatagram{1004} = 'Custom Attitude Information';
LabelsTypeDatagram{1005} = 'Tide';
LabelsTypeDatagram{1006} = 'Altitude';
LabelsTypeDatagram{1007} = 'Motion Over Ground';
LabelsTypeDatagram{1008} = 'Depth';
LabelsTypeDatagram{1009} = 'Sound Velocity Profile';
LabelsTypeDatagram{1010} = 'CTD';
LabelsTypeDatagram{1011} = 'Geodesy';
LabelsTypeDatagram{1012} = 'Roll Pitch Heave';
LabelsTypeDatagram{1013} = 'Heading';
LabelsTypeDatagram{1014} = 'Survey Line';
LabelsTypeDatagram{1015} = 'Navigation';
LabelsTypeDatagram{1016} = 'Attitude';
LabelsTypeDatagram{1050} = 'Generic sensor calibration parameters';
LabelsTypeDatagram{1200} = 'Generic side-scan sonar';
LabelsTypeDatagram{1201} = 'Reserved for generic sub-bottom profiler';
LabelsTypeDatagram{1202} = 'Reserved for generic embedded device data';
LabelsTypeDatagram{1500} = 'Reserved for future QC record';
LabelsTypeDatagram{2000} = 'XYZ Data';
LabelsTypeDatagram{7000} = '7k Sonar Settings';
LabelsTypeDatagram{7001} = '7k Configuration';
LabelsTypeDatagram{7002} = '7k Match Filter';
LabelsTypeDatagram{7004} = '7k Beam Geometry';
LabelsTypeDatagram{7005} = '7k Calibration Data';
LabelsTypeDatagram{7006} = '7k Bathymetric Data';
LabelsTypeDatagram{7007} = '7k Backscatter Imagery Data';
LabelsTypeDatagram{7008} = '7k Generic Data';
LabelsTypeDatagram{7009} = 'Vertical Depth';
LabelsTypeDatagram{7010} = 'TVG Gain Values';
LabelsTypeDatagram{7011} = '7k Image Data';
LabelsTypeDatagram{7018} = '7018';
LabelsTypeDatagram{7021} = 'Reserved';
LabelsTypeDatagram{7027} = '7k New Bathymetric Data';
LabelsTypeDatagram{7028} = '7028';
LabelsTypeDatagram{7022} = '7k Center Version';
LabelsTypeDatagram{7030} = 'Sonar Installation Parameters';
LabelsTypeDatagram{7038} = 'Raw I & Q Data';
LabelsTypeDatagram{7041} = '7k Water Column Data';
LabelsTypeDatagram{7042} = '7k Water Column Data New';
LabelsTypeDatagram{7048} = '7k Calibrayed Beam Data';
LabelsTypeDatagram{7050} = '7k System Events';
LabelsTypeDatagram{7051} = '7k System Event Message';
LabelsTypeDatagram{7052} = '7k Data Storage Status Information';
LabelsTypeDatagram{7055} = 'Calibration Status';
LabelsTypeDatagram{7057} = 'Calibrated Backscatter Imagery Data';
LabelsTypeDatagram{7058} = '7k Calibrated Snippet Data';
LabelsTypeDatagram{7060} = '7k Target Data';
LabelsTypeDatagram{7200} = '7k File Header';
LabelsTypeDatagram{7300} = 'Reserved';
LabelsTypeDatagram{7310} = '7k Trigger';
LabelsTypeDatagram{7311} = '7k Trigger Sequence Setup';
LabelsTypeDatagram{7312} = '7k Trigger Sequence Done';
LabelsTypeDatagram{7400} = 'Time Message';
LabelsTypeDatagram{7401} = 'Reserved for future time messages';
LabelsTypeDatagram{7500} = '7k Remote Control';
LabelsTypeDatagram{7501} = '7k Remote Control Acknowledge';
LabelsTypeDatagram{7502} = '7k Remote Control Not Acknowledge';
LabelsTypeDatagram{7503} = '7k Remote Control Sonar Settings';
LabelsTypeDatagram{7504} = 'Reserved';
LabelsTypeDatagram{7511} = 'Reserved';
LabelsTypeDatagram{7515} = 'Reserved';
LabelsTypeDatagram{7600} = '7k Roll';
LabelsTypeDatagram{7601} = '7k Pitch';
LabelsTypeDatagram{7610} = '7k Sound Velocity';
LabelsTypeDatagram{7611} = '7k Absorption Loss';
LabelsTypeDatagram{7612} = '7k Spreading Loss';
LabelsTypeDatagram{7900} = 'Reserved';
LabelsTypeDatagram{8100} = 'Embedded 8100 Series Sonar Data';

if ~isempty(varargin)
    code                 = varargin{1};
    tabHistoTypeDatagram = varargin{2};
else
    code                 = [];
    tabHistoTypeDatagram = zeros(size(LabelsTypeDatagram));
end

texteTypeDatagram = '';
for k=1:length(LabelsTypeDatagram)
    if ~isempty(LabelsTypeDatagram{k})
        sub = find(k == code);
        if isempty(sub)
            if fullListe
                texteTypeDatagram = sprintf('%s\n%d = %s (0)', ...
                    texteTypeDatagram, k, LabelsTypeDatagram{k});
            end
        else
            if fullListe
                texteTypeDatagram = sprintf('%s\n%d = %s (%d datagrams)', ...
                    texteTypeDatagram, code(sub), LabelsTypeDatagram{k}, tabHistoTypeDatagram(sub));
            else
                if tabHistoTypeDatagram(sub) ~= 0
                    texteTypeDatagram = sprintf('%s\n%d = %s (%d datagrams)', ...
                        texteTypeDatagram, code(sub), LabelsTypeDatagram{k}, tabHistoTypeDatagram(sub));
                    %                 else
                    %                     fprintf('Code=%d not processed in codesDatagramsS7k, please report to JMA\n', code);
                end
            end
        end
    end
end
