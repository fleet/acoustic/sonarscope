%% convert_Hypack7k_to_s7k.m

% Converts Hypack .7k file logged from Reson 7125 to .s7k file. 

% S. Greenaway
% Feb 5, 2010, modified May 5, 2010

% This script writes a compliant Reson .s7k file from a Hypack .7k file by 
% removing the Hypack headers and Reson Network frames. 
% The code writes the Hypack messages to a temporary file, then reads that
% file for the Reson Network headers.
% The saved file has the same name stub as the .7k file, but with an
% extension of .s7k.  The temporary file (TEMPORARY.dat) is deleted when
% the .s7k has been written. 


clear all
close all

pathname = 'C:\Documents and Settings\grice\Desktop\Data\MBES\Testing\';
files = dir([pathname '2010_6X_1032027.7K']);

N_files = length(files);

for file_indx = 1:1:N_files
    filename = files(file_indx).name;
    name_stub = filename(1:length(filename)-3);


    % open Hypack .7k file for reading
    fid = fopen([pathname filename],'r','ieee-le');
    %open temp file for writing in same directory
    wfid = fopen([pathname 'TEMPORARY.dat'], 'w', 'ieee-le');

    messageAmtRead = 0;
    messageSize = 0;
    messageCount = 0;
    

    %% Read the Hypack Header
    stopp = 0;
    while ~stopp
    
         %read the Hypack header
         messageSize = fread(fid,1,'int32');   
         messageAmtRead = 0;
         messageCount = messageCount + 1;
       %  disp([num2str(messageCount) ', ' num2str(messageSize)])

         %read and write Hypack message without header
         messageData = fread(fid, messageSize, 'uint8');
         fwrite(wfid, messageData);
         
         %check for end of file - this is kind of stupid, but eof is not
         %set until you actually try to read the byte after the last
         %written byte. 
         
         eof_test = fread(fid,1);
         if isempty(eof_test)
             stopp = 1;
         else fseek(fid, -1, 'cof');
         end

    end
       clear eof_test
       fclose(fid)
       fclose(wfid)
    %%   
    % then go through and strip the reson network frames- kind of kludgy to
    % close and then reopen
    fid = fopen([pathname 'TEMPORARY.dat'],'r','ieee-le');
    wfid = fopen([pathname name_stub '.s7k'],'w','ieee-le');   

    stopp = 0;
    packetCount = 0;
    disp(['writing file: ' name_stub '.s7k']); 

     while ~stopp  

        %% read the Data Network Frame
        protocolVers1 = fread(fid,1,'uint16');    
        offset1 = fread(fid,1,'uint16');
        totalPackets = fread(fid,1,'uint32');   %
        totalRecords = fread(fid, 1,'uint16');
        tansmissionID = fread(fid,1,'uint16');  %will be the same for packets from same record
        packetSize = fread(fid,1,'uint32');
        totalSize = fread(fid,1,'uint32');
        seqNum = fread(fid,1,'uint32');
        destDevID = fread(fid,1,'uint32');
        destEnum = fread(fid,1,'uint16');
        sourceEnum = fread(fid,1,'uint16');
        sourceDevID = fread(fid,1,'uint32');

      %  disp([num2str(double(totalPackets)) ', ' num2str(double(seqNum)) ', ' num2str(double(packetSize)) ', ' num2str(double(tansmissionID))])

        % We will just assume that the packets are saved in order, and just
        % strip off the network frames. 
        % read all the data as individual bytes, so endiness or does not
        % matter

        packetData = fread(fid, packetSize - offset1, 'int8');
        fwrite(wfid, packetData, 'int8');    

        packetCount = packetCount + 1;

         %check for end of file
         eof_test = fread(fid,1);
         if isempty(eof_test)
             stopp = 1;
         else fseek(fid, -1, 'cof');
         end
     end
    fclose(fid);
    fclose(wfid);
    delete([pathname 'TEMPORARY.dat']);
    clear eof_test
    disp(['Finished with ' name_stub])
end
disp('FINISHED!')  
