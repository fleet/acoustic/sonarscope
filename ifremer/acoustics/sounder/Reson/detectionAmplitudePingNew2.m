function [AmpBeamRange, AmpBeamQF, AmpBeamSamplesNb, AmpBeamQF2,N] = detectionAmplitudePingNew2(x, Amp, R0, SampleRate, TxPulseWidth, ...
    iPing, iBeam, BeamAngles, DisplayLevel, BeamsToDisplay, nbSamples,SystemSerialNumber, varargin)

[varargin, IdentAlgo] = getPropertyValue(varargin, 'IdentAlgo', 1); %#ok<ASGLU>

x = x(:);
Amp = Amp(:);

%% First test : checking if the signal is not empty

subSample = find(~isnan(Amp) & (Amp ~=0 ));
if isempty(subSample) || (x(end) > nbSamples)
    AmpBeamRange     = NaN;
    AmpBeamQF        = NaN;
    AmpBeamSamplesNb = NaN;
    AmpBeamQF2       = NaN;
    N=NaN;
    return
end

%% Second test (15/12/2010) : if the last sample of the signal corresponds
% to the maximum sample reachable in range then do not compute the
% sounding because it will be either a biased one or a false detection

if x(end) == nbSamples
    AmpBeamRange     = -1; % will be used in ResonSyntheseBottomDetector.m to determine which amplitude sounding to delete
    AmpBeamQF        = NaN;
    AmpBeamSamplesNb = NaN;
    AmpBeamQF2       = NaN;
    N=NaN;
    return
end

x        = x(subSample);
Amp      = Amp(subSample);
N_0      = SampleRate * TxPulseWidth;
N_corr   = N_0 * sqrt(2)/2;
val_qf   = 0;
val_qf_1 = 0;
val_qf_2 = 0;
Sigma2   = 0;

if SystemSerialNumber == 7111
    theta = 1.8/sqrt(2);
else
    theta = 1/sqrt(2);
end

%% First center of gravity computed on energy

if IdentAlgo == 2
    [AmpBeamRange0, Sigma0] = calcul_barycentre(x, Amp.^2); % Detection on Energy if flat bottom
    
    %------------- Test JMA fin mission BICOSE -----------------
    subOut = testDoubleShape(x, Amp, iBeam, R0, AmpBeamRange0, 1, 1);
    if ~isempty(subOut)
        x   = x(subOut);
        Amp = Amp(subOut);
        % AVANT TVISTOUL
        %[AmpBeamRange0, Sigma0] = calcul_barycentre(x, Amp.^2); % Detection on Energy if flat bottom
        % APRES TVISTOUL : TODO Didier Riou
        [AmpBeamRange0, Sigma0] = calcul_barycentre(x, Amp.^4); % Detection on Energy if flat bottom
    end
    %------------- Fin test JMA fin mission BICOSE -----------------
    
else
    [AmpBeamRange0, Sigma0] = calcul_barycentre(x, Amp.^4); % Detection on square of Energy
end

%% Restriction to the new domain

% sub1 = find((x >= (AmpBeamRange0-4*Sigma0)) & (x <= (AmpBeamRange0+4*Sigma0))); % Modif 25/03/3013
sub1 = find((x >= (AmpBeamRange0-2*Sigma0)) & (x <= (AmpBeamRange0+2*Sigma0)));
AmpBeamSamplesNb = length(sub1);

%% Testing the signal length : if its standard deviation is lower
% than the physical length of the signal then we don't need to compute
% another center of gravity and we keep this sounding

if 4*Sigma0 +1 <= (N_0 + 2*(AmpBeamRange0 * (1 / cosd(theta/2) - 1)))
    Sigma1 = 2*Sigma0;
    val_qf_0 = 1;
else
    
    %% Test of the shape of the enveloppe : if the remaining signal is not
    % approximatively bell-shaped, then we dissmiss the sounding since its QF
    % will be biased
    val_qf_0 = test_shape(Amp,x);
    
    %     if isempty(sub1)
    if length(sub1) < 2 % Modif JMA le 21/10/2014
        AmpBeamRange1 = AmpBeamRange0;
        Sigma1        = Sigma0;
    else
        val_qf_1 = test_shape(Amp(sub1),x(sub1));
        
        %% Computation of the second center of gravity on amplitude
        
        %         [AmpBeamRange1, Sigma1] = calcul_barycentre(x(sub1), Amp(sub1));
        [AmpBeamRange1, Sigma1] = calcul_barycentre(x(sub1), Amp(sub1)); % Detection on Amplitude
        
        
        %------------- Test JMA fin mission BICOSE -----------------
        %{
subOut = testDoubleShape(x(sub1), Amp(sub1), iBeam, R0, AmpBeamRange1, 2);
if ~isempty(subOut)
sub1 = sub1(subOut);
[AmpBeamRange1, Sigma1] = calcul_barycentre(x(sub1), Amp(sub1)); % Detection on Amplitude
end
        %}
        %------------- Fin test JMA fin mission BICOSE -----------------
        
        
        sub2 = find((x >= (AmpBeamRange1-2*Sigma1)) & (x <= (AmpBeamRange1+2*Sigma1)));
        if isempty(sub2)
            AmpBeamRange2 = AmpBeamRange1;
            Sigma2        = Sigma1;
        else
            val_qf_2 = test_shape(Amp(sub1),x(sub1));
            %val_qf_1=1;
            
            %% Computation of the third center of gravity on amplitude
            
            [AmpBeamRange2, Sigma2] = calcul_barycentre(x(sub2), Amp(sub2)); % Detection on Amplitude
            
            
            %------------- Test JMA fin mission BICOSE -----------------
            subOut = testDoubleShape(x(sub2), Amp(sub2), iBeam, R0, AmpBeamRange2, 3, iPing);
            if ~isempty(subOut)
                sub2 = sub2(subOut);
                [AmpBeamRange2, Sigma2] = calcul_barycentre(x(sub2), Amp(sub2)); % Detection on Amplitude
            end
            %------------- Fin test JMA fin mission BICOSE -----------------
            
            
        end
    end
    
end

%% Definition of the QF parameters and tests
%% New test : if the length of the signal is not longer than the length of
% the physical signal, then, dissmiss the sounding

if ((2*Sigma2+1)/N_corr > 1) && val_qf_2
    val_qf       = val_qf_2;
    AmpBeamRange = AmpBeamRange2;% - N_0 / 2;
    %     N            = (2*Sigma2+1) / N_corr;
    %     dTAmp        = B * sqrt(((4/pi-1) / 12) * ((N-1) * (N+1) / N))* N_corr;
    %     dTAmpYoann = calculEcartTypeTempsArriveeYoann(Sigma2, N_corr);
    %     dTAmp = calculEcartTypeTempsArrivee(Sigma2, N_0, 0.35);
    dTAmp = calculEcartTypeTempsArrivee(Sigma2, N_0, 0.40); % Modif Xavier 08/01/2014
    
elseif ((2*Sigma1+1)/N_corr > 1) && val_qf_1
    val_qf       = val_qf_1;
    AmpBeamRange = AmpBeamRange1; % - N_0 / 2;
    %     dTAmpYoann = calculEcartTypeTempsArriveeYoann(Sigma1, N_corr);
    %     dTAmp = calculEcartTypeTempsArrivee(Sigma1, N_0, 0.44);
    dTAmp = calculEcartTypeTempsArrivee(Sigma1, N_0, 0.40); % Modif Xavier 08/01/2014
    
elseif ((4*Sigma0+1)/(N_corr) > 1) && val_qf_0
    val_qf       = val_qf_0;
    AmpBeamRange = AmpBeamRange0; % - N_0 / 2;
    %     dTAmpYoann = calculEcartTypeTempsArriveeYoann(Sigma1, N_corr);
    %     dTAmp = calculEcartTypeTempsArrivee(Sigma0, N_0, 0.44);
    dTAmp = calculEcartTypeTempsArrivee(Sigma0, N_0, 0.40); % Modif Xavier 08/01/2014
    
else
    AmpBeamRange = AmpBeamRange0; % - N_0 / 2;
    dTAmp = -1;
    N = NaN;
end

if dTAmp > 0
    AmpBeamQF = AmpBeamRange / dTAmp;
    AmpBeamQF = log10(AmpBeamQF);
    %     AmpBeamQFYoann = AmpBeamRange / dTAmpYoann;
    %     AmpBeamQFYoann = log10(AmpBeamQFYoann);
else
    AmpBeamQF = NaN;
end

%% if the enveloppe didn't pass the bell shape test, don't validate its QF

if ~val_qf
    AmpBeamQF = NaN;
end


if (DisplayLevel == 3) && any(iBeam == BeamsToDisplay)
    if rem(iBeam,100) == 0
        figure(iBeam);
        plot(x,Amp)
        hold on;
        plot(x(sub1),Amp(sub1),'r')
        plot(ones(size(ylim))*AmpBeamRange2,ylim);
        grid on;
        drawnow;
        title(['Angle :' num2str(BeamAngles) '  Beam number :' num2str(iBeam) '  QF :' num2str(AmpBeamQF), '  AmpBeamRange=', num2str(AmpBeamRange)])
    end
    FigUtils.createSScFigure(3538);
    hAxe = subplot(1,1,1);
    if val_qf_0
        PlotUtils.createSScPlot(x, Amp, 'k'); grid on; hold on;
    end
    if val_qf_1
        PlotUtils.createSScPlot(x(sub1), Amp(sub1), 'b'); grid on; hold on;
    end
    if val_qf_2
        PlotUtils.createSScPlot(x(sub2), Amp(sub2), 'r'); grid on; hold on;
    end
    %     title(sprintf('iPing=%d   iBeam=%d   Angle=%f  AmpBeamRange=%f   Sigma=%f    AmpBeamQF=%f', ...
    %         iPing, iBeam, BeamAngles, AmpBeamRange2, Sigma2, AmpBeamQF))
    title(sprintf('iPing=%d   iBeam=%d   Angle=%f  AmpBeamQF=%f (First iteration=black, second=blue, third=red)', ...
        iPing, iBeam, BeamAngles, AmpBeamQF))
    
    M = max(Amp);
    if val_qf_0
        h = PlotUtils.createSScPlot([AmpBeamRange0 AmpBeamRange0], [0 M], 'k');
        set(h, 'LineWidth', 2);
        h = PlotUtils.createSScPlot([AmpBeamRange0-Sigma1 AmpBeamRange0+Sigma0], [0 0], 'k');
        set(h, 'LineWidth', 4);
        h = PlotUtils.createSScPlot([AmpBeamRange0-2*Sigma1 AmpBeamRange0+2*Sigma0], [0 0], 'k');
        set(h, 'LineWidth', 2);
    end
    
    if val_qf_1
        h = PlotUtils.createSScPlot([AmpBeamRange1 AmpBeamRange1], [0 M], 'b');
        set(h, 'LineWidth', 2);
        h = PlotUtils.createSScPlot([AmpBeamRange1-Sigma1 AmpBeamRange1+Sigma1], [M/3 M/3], 'b');
        set(h, 'LineWidth', 4);
        h = PlotUtils.createSScPlot([AmpBeamRange1-2*Sigma1 AmpBeamRange1+2*Sigma1], [M/3 M/3], 'b');
        set(h, 'LineWidth', 2);
    end
    
    if val_qf_2
        h = PlotUtils.createSScPlot([AmpBeamRange2 AmpBeamRange2], [0 M], 'r');
        set(h, 'LineWidth', 2);
        h = PlotUtils.createSScPlot([AmpBeamRange2-Sigma1 AmpBeamRange2+Sigma1], [2*M/3 2*M/3], 'r');
        set(h, 'LineWidth', 4);
        %       h = PlotUtils.createSScPlot([AmpBeamRange2-3*Sigma1 AmpBeamRange2+3*Sigma1], [M/2 M/2], 'r');
        %       set(h, 'LineWidth', 2);
    end
    
    hold off;
    set(hAxe, 'XLim', [1 nbSamples])
    drawnow
end

%% The computation of the QF2 is no longer necessary, we do it but will not use it (we keep it just in case)

W = max(3*Sigma1, 11);
sub3 = find((x >= (AmpBeamRange-W)) & (x <= (AmpBeamRange+W)));
if length(sub3) < 7
    sub3 = 1:length(x);
end

y  = cumsum(Amp(sub3));
y  = y / y(end);
x0 = x(sub3);
x1 = x0(find(y > 0.25, 1, 'first'));
x2 = x0(find(y > 0.75, 1, 'first'));
if x2 == x1
    AmpBeamQF2 = log10(100);
else
    AmpBeamQF2 = .5 * length(y) / (x2 - x1);
end


function val_qf = test_shape(Amp, x)

global TestPlatformCmatlab %#ok<GVMIS>

% figure; plot(x, Amp); grid

if any(TestPlatformCmatlab.test_shape == [1 3 4])
    val_qf_M = test_shape_Matlab(Amp, x);
end

if any(TestPlatformCmatlab.test_shape == [2 3 4])
    val_qf_C = test_shape_reson(...
        single(Amp), ...
        double(x));
end

if any(TestPlatformCmatlab.test_shape == [3 4])
    status = BDA_createMes('test_shape', 'val_qf_', val_qf_M, val_qf_C, [], [], 0);
    if ~status
        status;
    end
end

switch TestPlatformCmatlab.test_shape
    case {1; 3}
        val_qf = val_qf_M;
    case {2; 4}
        val_qf = val_qf_C;
end



function subOut = testDoubleShape(x, Amp, iBeam, R0, AmpBeamRange, LineWidth, iPing)
global TestPlatformCmatlab %#ok<GVMIS>

% figure; plot(x, Amp); grid

if any(TestPlatformCmatlab.testDoubleShape == [1 3 4])
    subOut_M = testDoubleShapeMatlab(x, Amp, R0, AmpBeamRange, LineWidth);
end

if any(TestPlatformCmatlab.testDoubleShape == [2 3 4])
    subOut_C = test_double_shape_reson(...
        double(x), ...
        single(Amp), ...
        double(iBeam), double(R0), ...
        single(AmpBeamRange), ...
        double(LineWidth));
end

if any(TestPlatformCmatlab.testDoubleShape == [3 4])
    status = BDA_compare_Matlab_C('testDoubleShape', 'subOut', subOut_M, subOut_C, iPing, iBeam, TestPlatformCmatlab.Seuil_testDoubleShape_subOut);
    if ~status
        status;
    end
end

switch TestPlatformCmatlab.testDoubleShape
    case {1; 3}
        subOut = subOut_M;
    case {2; 4}
        subOut = subOut_C;
end

function val_qf = test_shape_Matlab(Amp, x)
level_abs   = 0.75;
bell_int_1  = cumsum(Amp.^2/sum(Amp.^2));
x_bell_1    = (x-(x(end)+x(1))/2);
x_bell_1    = x_bell_1/x_bell_1(end)/2;
[~,u_min_1] = min(abs(x_bell_1+0.3));
[~,u_max_1] = min(abs(x_bell_1-0.3));
level_temp = abs(bell_int_1(u_max_1) - bell_int_1(u_min_1));
if level_temp > level_abs
    val_qf = 1;
else
    val_qf = 0;
end

function dTAmp = calculEcartTypeTempsArrivee(Sigma, N_0, Coeff)
% UnSurN = 1/(2*Sigma) + N_0/(2*Sigma + N_0);
UnSurN = N_0/Sigma;
dTAmp = Coeff * Sigma * sqrt(UnSurN);

%{
function dTAmp = calculEcartTypeTempsArriveeYoann(Sigma, N_corr)
B     = 1.04;%%sinc shaped enveloppe
N     = (2*Sigma+1) / N_corr;
dTAmp = B * sqrt(((4/pi-1) / 12) * ((N-1) * (N+1) / N))* N_corr;
%}

function subOut = testDoubleShapeMatlab(x, Amp, R0, AmpBeamRange, LineWidth)

% figure; plot(x, Amp); grid on;
% figure(7965); plot(Amp); grid on;

%testProcheSpeculaire = ((AmpBeamRange/R0) < 1.1);
testProcheSpeculaire = ((AmpBeamRange/R0) < 1.1);
if ~testProcheSpeculaire
    subOut = [];
    return
end

nAmp  = length(Amp);

% AVANT TVISTOUL
% nAmp2 = floor(nAmp/2);
% subAmp1 = 1:min(nAmp2+3-1,nAmp);
% subAmp2 = max(nAmp2-3, 1):nAmp;

% APRES TVISTOUL
% TODO Didier Riou
iMiddle = round(AmpBeamRange - x(1)) + 1;
subAmp1 = 1:min(iMiddle,nAmp);
subAmp2 = max(iMiddle, 1):nAmp;
if (length(subAmp1) <= 5) || (length(subAmp2) <= 5)
    %     subOut = 1:nAmp;
    subOut = [];
    return
end


rSubAmp1 = calcul_barycentre(subAmp1, Amp(subAmp1)); % Detection on Amplitude
delta1 = min(abs(rSubAmp1-subAmp1(1)), abs(rSubAmp1-subAmp1(end)));
subAmp1 = max(subAmp1(1),floor(rSubAmp1-delta1)):min(subAmp1(end),floor(rSubAmp1+delta1));

rSubAmp2 = calcul_barycentre(subAmp2, Amp(subAmp2)); % Detection on Amplitude
delta2 = min(abs(rSubAmp2-subAmp2(1)), abs(rSubAmp2-subAmp2(end)));
subAmp2 = max(subAmp2(1),floor(rSubAmp2-delta2)):min(subAmp2(end),floor(rSubAmp2+delta1));


% testAmp0 = test_shape(Amp, x);
testAmp1 = test_shape(Amp(subAmp1), x(subAmp1));
testAmp2 = test_shape(Amp(subAmp2), x(subAmp2));

% fprintf('%d %d %d %d\n', testAmp0, testAmp1, testAmp2,  iBeam);

% if all([testAmp0, testAmp1, testAmp2 testProcheSpeculaire])
if testAmp1 && testAmp2
    Max1 = max(Amp(subAmp1));
    Max2 = max(Amp(subAmp2));
    
    if (Max2 > (Max1/20))
        if get_LevelQuestion >= 3
            FigUtils.createSScFigure(12321);
            h = plot(x(subAmp1), Amp(subAmp1));
            set(h, 'LineWidth', LineWidth);
            grid on; hold on;
            h = plot(x(subAmp2), Amp(subAmp2), 'r');
            set(h, 'LineWidth', LineWidth);
            title('Double shape detected : Second part conserved here')
            hold off;
            %         fprintf('Condition [1 1 1] rencontrée pour iBeam %d  Rap=%f\n', iBeam, AmpBeamRange/R0);
        end
        subOut = subAmp2;
    elseif (Max1 > (Max2/20))
        if get_LevelQuestion >= 3
            FigUtils.createSScFigure(12321);
            h = plot(x(subAmp1), Amp(subAmp1), 'r');
            set(h, 'LineWidth', LineWidth);
            grid on; hold on;
            h = plot(x(subAmp2), Amp(subAmp2));
            set(h, 'LineWidth', LineWidth);
            title('Double shape detected : First part conserved here')
            hold off;
            %         fprintf('Condition [1 1 1] rencontrée pour iBeam %d  Rap=%f\n', iBeam, AmpBeamRange/R0);
        end
        subOut = subAmp1;
    else
        subOut = [];
    end
else
    subOut = [];
end
