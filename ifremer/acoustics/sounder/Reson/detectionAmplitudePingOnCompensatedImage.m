
%% D�tection sur le barycentre
% Equivalent to detectionAmplitudePingOnCompensatedImage except for the
% last 3 instructions

function AmpBeamRange = detectionAmplitudePingOnCompensatedImage(x, Amp, iPing, iBeam, ...
    BeamAngles, DisplayLevel, nbSamples)

x   = x(:);
Amp = Amp(:);

subSample = find(~isnan(Amp));
if isempty(subSample) || (x(end) > nbSamples)
    AmpBeamRange = NaN;
    return
end

x   = x(subSample);
Amp = Amp(subSample);

%% Calcul du barycentre

[AmpBeamRange, Sigma] = calcul_barycentre(x, Amp.^2); % Detection on Energy

%% Restriction du domaine et recalcul du barycentre

sub = find((x >= (AmpBeamRange-2*Sigma)) & (x <= (AmpBeamRange+2*Sigma)));
if isempty(sub)
    return
end

if sub(end) == size(Amp,1)
    % Signal touches the end of the matrix : we probably have not all the
    % signal, should use a specific method : how to do it ?
    [AmpBeamRange, Sigma] = calcul_barycentre(x(sub), Amp(sub)); % Detection on Amplitude
else
    [AmpBeamRange, Sigma] = calcul_barycentre(x(sub), Amp(sub).^2); % Detection on Energy
end

%% Restriction du domaine et recalcul du barycentre

sub = find((x >= (AmpBeamRange-Sigma)) & (x <= (AmpBeamRange+Sigma)));
if length(sub) <= 3
    return
end

[AmpBeamRange, Sigma] = calcul_barycentre(x(sub), Amp(sub).^2); % Detection on Energy

% Correctif � confirmer (identifi� sur fichier
% F:\Feb2008\7111\Spikes\20080302_195235.s7k)
AmpBeamRange = AmpBeamRange + 0.5;

if DisplayLevel == 3
    figure(3536);
    hAxe = subplot(1,1,1);
    plot(x, Amp); grid on; hold on;
    title(sprintf('iPing=%d   iBeam=%d   Angle=%f  AmpBeamRange=%f   Sigma=%f', iPing, iBeam, BeamAngles, AmpBeamRange, Sigma))
    h = plot([AmpBeamRange AmpBeamRange], [0 max(Amp)], 'r');
    set(h, 'LineWidth', 2);
    h = plot([AmpBeamRange-3*Sigma AmpBeamRange+3*Sigma], [0 0], 'r');
    set(h, 'LineWidth', 4); hold off;
    hold off;
    set(hAxe, 'XLim', [1 nbSamples])
    drawnow
end
