
%% Detection on center of gravity
%% Version 16/12/2010

function [AmpBeamRange, AmpBeamQF, AmpBeamSamplesNb, AmpBeamQF2,N] = detectionAmplitudePing_AvantModifXLESSSYPEN(x, Amp, R0, SampleRate, TxPulseWidth, ...
    iPing, iBeam, BeamAngles, DisplayLevel, BeamsToDisplay, nbSamples,SystemSerialNumber)


x = x(:);
Amp = Amp(:);

%% First test : checking if the signal is not empty

subSample = find(~isnan(Amp) & (Amp ~=0 ));
if isempty(subSample) || (x(end) > nbSamples)
    AmpBeamRange     = NaN;
    AmpBeamQF        = NaN;
    AmpBeamSamplesNb = NaN;
    AmpBeamQF2       = NaN;
    N=NaN;
    return
end

%% Second test (15/12/2010) : if the last sample of the signal corresponds
% to the maximum sample reachable in range then do not compute the
% sounding because it will be either a biased one or a false detection

if x(end)==nbSamples
    AmpBeamRange     = -1; %will be used in ResonSyntheseBottomDetector.m to determine which amplitude sounding to delete
    AmpBeamQF        = NaN;
    AmpBeamSamplesNb = NaN;
    AmpBeamQF2       = NaN;
    N=NaN;
    return
end

x   = x(subSample);
Amp = Amp(subSample);
N_0    = SampleRate * TxPulseWidth;
N_corr = N_0 * sqrt(2)/2;
B   = 1.04;%%sinc shaped enveloppe
val_qf=0;
val_qf_1 = 0;
val_qf_2 = 0;
% Sigma0=0;
% Sigma1=0;
Sigma2=0;

%
if SystemSerialNumber == 7111
    theta=1.8/sqrt(2);
else
    theta=1/sqrt(2);
end

%% First center of gravity computed on energy

[AmpBeamRange0, Sigma0] = calcul_barycentre(x, Amp.^4);
% [AmpBeamRange0, Sigma0] = calcul_barycentre(x, Amp);

%% Restriction to the new domain

sub1 = find((x >= (AmpBeamRange0-4*Sigma0)) & (x <= (AmpBeamRange0+4*Sigma0)));
AmpBeamSamplesNb = length(sub1);

%% Testing the signal length : if its standard deviation is lower
% than the physical length of the signal then we don't need to compute
% another center of gravity and we keep this sounding

if 4*Sigma0 +1 <= (N_0 + 2*(AmpBeamRange0 * (1 / cosd(theta/2) - 1)))
    Sigma1   = 2*Sigma0;
    val_qf_0 = 1;
else
    
    %% Test of the shape of the enveloppe : if the remaining signal is not
    % approximatively bell-shaped, then we dissmiss the sounding since its QF
    % will be biased
    val_qf_0=test_shape(Amp,x);
    
    
    if isempty(sub1)
        AmpBeamRange1 = AmpBeamRange0;
        Sigma1        = Sigma0;
    else
        val_qf_1 = test_shape(Amp(sub1),x(sub1));
        %% Computation of the second center of gravity on amplitude
%         [AmpBeamRange1, Sigma1] = calcul_barycentre(x(sub1), Amp(sub1));
        [AmpBeamRange1, Sigma1] = calcul_barycentre(x(sub1), Amp(sub1));
        sub2 = find((x >= (AmpBeamRange1-2*Sigma1)) & (x <= (AmpBeamRange1+2*Sigma1)));
        if isempty(sub2)
            AmpBeamRange2 = AmpBeamRange1;
            Sigma2        = Sigma1;
        else
            val_qf_2 = test_shape(Amp(sub1),x(sub1));
            %val_qf_1=1;
            %% Computation of the third center of gravity on amplitude
            [AmpBeamRange2, Sigma2] = calcul_barycentre(x(sub2), Amp(sub2));
            
        end
    end
    
end

%{
% Test JMA 07/03/2011
if ~val_qf_0
    val_qf_2 = 0;
end
if ~val_qf_1
    val_qf_2 = 0;
end
%}


%% Definition of the QF parameters and tests
%% New test : if the length of the signal is not longer than the length of
% the physical signal, then, dissmiss the sounding

if ((2*Sigma2+1)/N_corr > 1) && val_qf_2
    val_qf       = val_qf_2;
    AmpBeamRange = AmpBeamRange2;% - N_0 / 2;
    N            = (2*Sigma2+1) / N_corr;
    dTAmp        = B * sqrt(((4/pi-1) / 12) * ((N-1) * (N+1) / N))* N_corr;
    
elseif ((2*Sigma1+1)/N_corr > 1) && val_qf_1
    val_qf       = val_qf_1;
    AmpBeamRange = AmpBeamRange1; % - N_0 / 2;
    N            = (2*Sigma1+1) / N_corr;
    dTAmp        = B * sqrt(((4/pi-1) / 12) * ((N-1) * (N+1) / N)) * N_corr;
    
elseif ((4*Sigma0+1)/(N_corr) > 1) && val_qf_0
    val_qf       = val_qf_0;
    AmpBeamRange = AmpBeamRange0; % - N_0 / 2;
    N            = (2*Sigma1+1) / N_corr;
    dTAmp        = B * sqrt(((4/pi-1) / 12) * ((N-1) * (N+1) / N))* N_corr;
    
else
    AmpBeamRange = AmpBeamRange0; % - N_0 / 2;
    dTAmp = -1;
    N = NaN;
end


if dTAmp > 0
    AmpBeamQF = AmpBeamRange / dTAmp;
    AmpBeamQF = log10(AmpBeamQF);
else
    AmpBeamQF = NaN;
end

%% if the enveloppe didn't pass the bell shape test, don't validate its QF

if ~val_qf
    AmpBeamQF = NaN;
end


if (DisplayLevel == 3) && any(iBeam == BeamsToDisplay)
    if rem(iBeam,20)==0
        figure(iBeam);
        plot(x,Amp)
        hold on;
        plot(x(sub1),Amp(sub1),'r')
        plot(ones(size(ylim))*AmpBeamRange2,ylim);
        grid on;
        drawnow;
        title(['Angle :' num2str(BeamAngles) '  Beam number :' num2str(iBeam) '  QF :' num2str(AmpBeamQF), '  N=', num2str(N)])
    end
    FigUtils.createSScFigure(3538);
    hAxe = subplot(1,1,1);
    PlotUtils.createSScPlot(x, Amp, 'k'); grid on; hold on;
    PlotUtils.createSScPlot(x(sub1), Amp(sub1), 'b'); grid on; hold on;
    %     PlotUtils.createSScPlot(x(sub2), Amp(sub2), 'r'); grid on; hold on;
    %     title(sprintf('iPing=%d   iBeam=%d   Angle=%f  AmpBeamRange=%f   Sigma=%f    AmpBeamQF=%f', ...
    %         iPing, iBeam, BeamAngles, AmpBeamRange2, Sigma2, AmpBeamQF))
    title(sprintf('iPing=%d   iBeam=%d   Angle=%f  AmpBeamQF=%f', ...
        iPing, iBeam, BeamAngles, AmpBeamQF))
    
    M = max(Amp);
    h = PlotUtils.createSScPlot([AmpBeamRange0 AmpBeamRange0], [0 M], 'k');
    set(h, 'LineWidth', 2);
    h = PlotUtils.createSScPlot([AmpBeamRange0-Sigma1 AmpBeamRange0+Sigma0], [0 0], 'k');
    set(h, 'LineWidth', 4);
    h = PlotUtils.createSScPlot([AmpBeamRange0-3*Sigma1 AmpBeamRange0+3*Sigma0], [0 0], 'k');
    set(h, 'LineWidth', 2);
    
    h = PlotUtils.createSScPlot([AmpBeamRange1 AmpBeamRange1], [0 M], 'b');
    set(h, 'LineWidth', 2);
    h = PlotUtils.createSScPlot([AmpBeamRange1-Sigma1 AmpBeamRange1+Sigma1], [M/2 M/2], 'b');
    set(h, 'LineWidth', 4);
    h = PlotUtils.createSScPlot([AmpBeamRange1-3*Sigma1 AmpBeamRange1+3*Sigma1], [M/2 M/2], 'b');
    set(h, 'LineWidth', 2);
    
    h = PlotUtils.createSScPlot([AmpBeamRange2 AmpBeamRange2], [0 M], 'r');
    set(h, 'LineWidth', 2);
    h = PlotUtils.createSScPlot([AmpBeamRange2-Sigma1 AmpBeamRange2+Sigma1], [M/2 M/2], 'r');
    set(h, 'LineWidth', 4);
    h = PlotUtils.createSScPlot([AmpBeamRange2-3*Sigma1 AmpBeamRange2+3*Sigma1], [M/2 M/2], 'r');
    set(h, 'LineWidth', 2);
    
    h = PlotUtils.createSScPlot([AmpBeamRange AmpBeamRange], [0 M], 'M');
    set(h, 'LineWidth', 3);
    
    hold off;
    set(hAxe, 'XLim', [1 nbSamples])
    drawnow
end

%% The computation of the QF2 is no longer necessary, we do it but will not use it (we keep it just in case)

W = max(3*Sigma1, 11);
sub3 = find((x >= (AmpBeamRange-W)) & (x <= (AmpBeamRange+W)));
if length(sub3) < 7
    sub3 = 1:length(x);
end

y  = cumsum(Amp(sub3));
y  = y / y(end);
x0 = x(sub3);
x1 = x0(find(y > 0.25, 1, 'first'));
x2 = x0(find(y > 0.75, 1, 'first'));
if x2 == x1
    AmpBeamQF2 = log10(100);
else
    AmpBeamQF2 = .5 * length(y) / (x2 - x1);
end

function val_qf=test_shape(Amp,x)
level_abs   = 0.75;
bell_int_1  = cumsum(Amp.^2/sum(Amp.^2));
x_bell_1    = (x-(x(end)+x(1))/2);
x_bell_1    = x_bell_1/x_bell_1(end)/2;
[~,u_min_1] = min(abs(x_bell_1+0.3));
[~,u_max_1] = min(abs(x_bell_1-0.3));

level_temp = abs(bell_int_1(u_max_1) - bell_int_1(u_min_1));
if level_temp > level_abs
    val_qf = 1;
else
    val_qf = 0;
end