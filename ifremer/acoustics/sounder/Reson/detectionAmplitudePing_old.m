
%% D�tection sur le barycentre

function [AmpBeamRange, AmpBeamQF, AmpBeamSamplesNb, AmpBeamQF2,N] = detectionAmplitudePing_old(x, Amp, R0, SampleRate, TxPulseWidth, ...
    iPing, iBeam, BeamAngles, DisplayLevel, BeamsToDisplay, nbSamples, SystemSerialNumber)


x = x(:);
Amp = Amp(:);

% Amp = sqrt(FiltreAmp(Amp.^2));
% Amp = FiltreAmp(Amp);
%Amp(Amp<max(Amp(:))/(20))=NaN;
subSample = find(~isnan(Amp) & (Amp ~=0 ));
if isempty(subSample) || (x(end) > nbSamples)
    AmpBeamRange     = NaN;
    AmpBeamQF        = NaN;
    AmpBeamSamplesNb = NaN;
    AmpBeamQF2       = NaN;
    N=NaN;
    return
end

x   = x(subSample);
Amp = Amp(subSample);

% --------------------
% Calcul du barycentre

% level_abs=0.7;
% 
% bell_int = cumsum(Amp.^2/sum(Amp.^2));
% x_bell    = (x-(x(end)+x(1))/2);
% x_bell    = x_bell/x_bell(end)/2;
% [~,u_min] = min(abs(x_bell+0.3));
% [~,u_max] = min(abs(x_bell-0.3));
% 
% level_temp=abs(bell_int(u_max)-bell_int(u_min));
% if level_temp>level_abs
%     val_qf = 1;
% else
%     val_qf = 0; 
% end


[AmpBeamRange0, Sigma0] = calcul_barycentre(x, Amp.^2);

% ------------------------------------------------
% Restriction du domaine et recalcul du barycentre

sub1 = find((x >= (AmpBeamRange0-2*Sigma0)) & (x <= (AmpBeamRange0+2*Sigma0)));
AmpBeamSamplesNb = length(sub1);
% %test de la validit� de la cloche :

level_abs=0.7;

bell_int_1 = cumsum(Amp(sub1).^2/sum(Amp(sub1).^2));
x_bell_1    = (x(sub1)-(x(sub1(end))+x(sub1(1)))/2);
x_bell_1    = x_bell_1/x_bell_1(end)/2;
[~,u_min_1] = min(abs(x_bell_1+0.3));
[~,u_max_1] = min(abs(x_bell_1-0.3));


level_temp=abs(bell_int_1(u_max_1)-bell_int_1(u_min_1));
if level_temp>level_abs
    val_qf = 1;
else
    val_qf = 0; 
end


if length(sub1) > 3
    [AmpBeamRange1, Sigma1] = calcul_barycentre(x(sub1), Amp(sub1));
    %     if (6*Sigma) > length(sub1)
    %         Sigma = Sigma*2;
    %     end
else
    AmpBeamRange1 = AmpBeamRange0;
    Sigma1 = Sigma0;
end


AmpBeamRange=AmpBeamRange1;

% QF computation 02/02/2010

%N=sigma_test;
N_0 = SampleRate*TxPulseWidth;
B   = 1.04;
N   = 2*Sigma1/N_0;

if N > 1
    dTAmp   = B*sqrt((4/pi-1)/12*(N-1)*(N+1)/(N))*N_0;
else
    N=2;
    dTAmp   = B*sqrt((4/pi-1)/12*(N-1)*(N+1)/(N))*N_0;
end
dT      = dTAmp;
AmpBeamQF = AmpBeamRange / (dT);


if AmpBeamQF > 0
    AmpBeamQF = log10(AmpBeamQF);
else
    AmpBeamQF = NaN;
end


if ~val_qf
    AmpBeamQF = NaN;
end

if (DisplayLevel == 3)  && any(iBeam == BeamsToDisplay)
    figure(3538);
    hAxe = subplot(1,1,1);
    PlotUtils.createSScPlot(x, Amp, 'k'); grid on; hold on;
    PlotUtils.createSScPlot(x(sub1), Amp(sub1), 'b'); grid on; hold on;
%     PlotUtils.createSScPlot(x(sub2), Amp(sub2), 'r'); grid on; hold on;
%     title(sprintf('iPing=%d   iBeam=%d   Angle=%f  AmpBeamRange=%f   Sigma=%f    AmpBeamQF=%f', ...
%         iPing, iBeam, BeamAngles, AmpBeamRange2, Sigma2, AmpBeamQF))
    title(sprintf('iPing=%d   iBeam=%d   Angle=%f  AmpBeamQF=%f', ...
        iPing, iBeam, BeamAngles, AmpBeamQF))
    
    M = max(Amp);
    h = PlotUtils.createSScPlot([AmpBeamRange0 AmpBeamRange0], [0 M], 'k');
    set(h, 'LineWidth', 2);
    h = PlotUtils.createSScPlot([AmpBeamRange0-Sigma1 AmpBeamRange0+Sigma0], [0 0], 'k');
    set(h, 'LineWidth', 4);
    h = PlotUtils.createSScPlot([AmpBeamRange0-3*Sigma1 AmpBeamRange0+3*Sigma0], [0 0], 'k');
    set(h, 'LineWidth', 2);
    
    h = PlotUtils.createSScPlot([AmpBeamRange1 AmpBeamRange1], [0 M], 'b');
    set(h, 'LineWidth', 2);
    h = PlotUtils.createSScPlot([AmpBeamRange1-Sigma1 AmpBeamRange1+Sigma1], [M/2 M/2], 'b');
    set(h, 'LineWidth', 4);
    h = PlotUtils.createSScPlot([AmpBeamRange1-3*Sigma1 AmpBeamRange1+3*Sigma1], [M/2 M/2], 'b');
    set(h, 'LineWidth', 2);
    
    hold off;
    set(hAxe, 'XLim', [1 nbSamples])
    drawnow
end

% ---------------------------------------------------------------
% Restriction du domaine et calcul du deuxi�me facteur de qualit�

W = max(3*Sigma1, 11);
sub3 = find((x >= (AmpBeamRange1-W)) & (x <= (AmpBeamRange1+W)));
if length(sub3) < 7
    sub3 = 1:length(x);
end

y  = cumsum(Amp(sub3));
y  = y / y(end);
x0 = x(sub3);
x1 = x0(find(y > 0.25, 1, 'first'));
x2 = x0(find(y > 0.75, 1, 'first'));
if x2 == x1
    AmpBeamQF2 = log10(100);
else
    AmpBeamQF2 = .5 * length(y) / (x2 - x1);
end

%{
function AmpF = FiltreAmp(Amp)
% AmpF = Amp;
% N = 3;
% for iSample=(N+1):(length(Amp)-N)
%     AmpF(iSample) = mean(AmpF(iSample-N:iSample+N));
% end

h = fspecial('gaussian', 5);
AmpF = filtfilt(h(3,:)/sum(h(3,:)), 1, Amp);
%}