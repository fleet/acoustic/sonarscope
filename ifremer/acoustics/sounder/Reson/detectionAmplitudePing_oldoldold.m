
%% Détection sur le barycentre

function [AmpBeamRange2, AmpBeamQF, AmpBeamSamplesNb, AmpBeamQF2] = detectionAmplitudePing_oldoldold(x, Amp, R0, SampleRate, TxPulseWidth, ...
    iPing, iBeam, BeamAngles, DisplayLevel, BeamsToDisplay, nbSamples)

x = x(:);
Amp = Amp(:);

% Amp = sqrt(FiltreAmp(Amp.^2));
% Amp = FiltreAmp(Amp);

subSample = find(~isnan(Amp) & (Amp ~=0 ));
if isempty(subSample) || (x(end) > nbSamples)
    AmpBeamRange2     = NaN;
    AmpBeamQF        = NaN;
    AmpBeamSamplesNb = NaN;
    AmpBeamQF2       = NaN;
    return
end

x   = x(subSample);
Amp = Amp(subSample);

% --------------------
% Calcul du barycentre

[AmpBeamRange0, Sigma0] = calcul_barycentre(x, Amp.^2);

% ------------------------------------------------
% Restriction du domaine et recalcul du barycentre

sub1 = find((x >= (AmpBeamRange0-2*Sigma0)) & (x <= (AmpBeamRange0+2*Sigma0)));
AmpBeamSamplesNb = length(sub1);

% [AmpBeamRange, Sigma] = calcul_barycentre(x(sub1), Amp(sub1).^2);

if length(sub1) > 3
    [AmpBeamRange1, Sigma1] = calcul_barycentre(x(sub1), Amp(sub1));
%     if (6*Sigma) > length(sub1)
%         Sigma = Sigma*2;
%     end
else
    AmpBeamRange1 = AmpBeamRange0;
    Sigma1 = Sigma0;
end

%{
Y1 = Amp(sub1) / sum(Amp(sub1));
Y2 = normpdf(x(sub1), AmpBeamRange1, Sigma1);
Residu = Y1 - Y2;
stdResidu = std(Residu .^2);
if (DisplayLevel == 3)  && any(iBeam == BeamsToDisplay)
    figure(5576);
    subplot(2,1,1); plot(x(sub1), Y1); grid on; hold on;
        title(sprintf('Signal & normpdf : iPing=%d   iBeam=%d   Angle=%f  AmpBeamRange1=%f', ...
        iPing, iBeam, BeamAngles, AmpBeamRange1))
    plot(x(sub1), Y2, 'r')
    hold off

    subplot(2,1,2); plot(x(sub1), Residu); grid on;
    title(['Residu : ' num2str(stdResidu * 5000) ' * 5000'])
end
% Sigma1 = Sigma1 / min(stdResidu * 5000, 1);
%}

% ------------------------------------------------
% Restriction du domaine et recalcul du barycentre

sub2 = find((x >= (AmpBeamRange1-Sigma1)) & (x <= (AmpBeamRange1+Sigma1)));
if length(sub2) > 3 % 7 avant
    AmpBeamSamplesNb = length(sub2);
    [AmpBeamRange2, Sigma2] = calcul_barycentre(x(sub2), Amp(sub2).^2);
else
    AmpBeamRange2 = AmpBeamRange1;
    Sigma2 = Sigma1;
end

% AmpBeamQF = 4.35 * AmpBeamRange2 / sqrt(2 * Sigma1);
dTAmp   = 0.23 * sqrt(2 * Sigma1);
dTPulse = TxPulseWidth * SampleRate / sqrt(12);
% dT      = sqrt(dTAmp*dTAmp + dTPulse*dTPulse);
dT      = dTAmp;
AmpBeamQF = AmpBeamRange2 / dT;

if AmpBeamQF > 0
    AmpBeamQF = log10(AmpBeamQF);
else
    AmpBeamQF = 0;
end

if (DisplayLevel == 3)  && any(iBeam == BeamsToDisplay)
    figure(3538);
    hAxe = subplot(1,1,1);
    PlotUtils.createSScPlot(x, Amp, 'k'); grid on; hold on;
    PlotUtils.createSScPlot(x(sub1), Amp(sub1), 'b'); grid on; hold on;
    PlotUtils.createSScPlot(x(sub2), Amp(sub2), 'r'); grid on; hold on;
    title(sprintf('iPing=%d   iBeam=%d   Angle=%f  AmpBeamRange=%f   Sigma=%f    AmpBeamQF=%f', ...
        iPing, iBeam, BeamAngles, AmpBeamRange2, Sigma2, AmpBeamQF))
    
    M = max(Amp);
    h = PlotUtils.createSScPlot([AmpBeamRange0 AmpBeamRange0], [0 M], 'k');
    set(h, 'LineWidth', 2);
    h = PlotUtils.createSScPlot([AmpBeamRange0-Sigma1 AmpBeamRange0+Sigma0], [0 0], 'k');
    set(h, 'LineWidth', 4);
    h = PlotUtils.createSScPlot([AmpBeamRange0-3*Sigma1 AmpBeamRange0+3*Sigma0], [0 0], 'k');
    set(h, 'LineWidth', 2);

    h = PlotUtils.createSScPlot([AmpBeamRange1 AmpBeamRange1], [0 M], 'b');
    set(h, 'LineWidth', 2);
    h = PlotUtils.createSScPlot([AmpBeamRange1-Sigma1 AmpBeamRange1+Sigma1], [M/2 M/2], 'b');
    set(h, 'LineWidth', 4);
    h = PlotUtils.createSScPlot([AmpBeamRange1-3*Sigma1 AmpBeamRange1+3*Sigma1], [M/2 M/2], 'b');
    set(h, 'LineWidth', 2);

    h = PlotUtils.createSScPlot([AmpBeamRange2 AmpBeamRange2], [0 M], 'r');
    set(h, 'LineWidth', 2);
    h = PlotUtils.createSScPlot([AmpBeamRange2-Sigma2 AmpBeamRange2+Sigma2], [M M], 'r');
    set(h, 'LineWidth', 4);
    h = PlotUtils.createSScPlot([AmpBeamRange2-3*Sigma2 AmpBeamRange2+3*Sigma2], [M M], 'r');
    set(h, 'LineWidth', 2); hold off;

    hold off;
    set(hAxe, 'XLim', [1 nbSamples])
    drawnow
end

% ---------------------------------------------------------------
% Restriction du domaine et calcul du deuxième facteur de qualité

W = max(3*Sigma1, 11);
sub3 = find((x >= (AmpBeamRange2-W)) & (x <= (AmpBeamRange2+W)));
if length(sub3) < 7
    sub3 = 1:length(x);
end

y  = cumsum(Amp(sub3));
y  = y / y(end);
x0 = x(sub3);
x1 = x0(find(y > 0.25, 1, 'first'));
x2 = x0(find(y > 0.75, 1, 'first'));
if x2 == x1
    AmpBeamQF2 = log10(100);
else
    AmpBeamQF2 = .5 * length(y) / (x2 - x1);
end

%{
function AmpF = FiltreAmp(Amp)
% AmpF = Amp;
% N = 3;
% for iSample=(N+1):(length(Amp)-N)
%     AmpF(iSample) = mean(AmpF(iSample-N:iSample+N));
% end

h = fspecial('gaussian', 5);
AmpF = filtfilt(h(3,:)/sum(h(3,:)), 1, Amp);
%}
