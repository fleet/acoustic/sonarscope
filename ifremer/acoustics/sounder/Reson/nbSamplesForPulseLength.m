function NbSamplesForPulseLength = nbSamplesForPulseLength(SampleRate, TxPulseWidth)

NbSamplesForPulseLength = ceil(TxPulseWidth * SampleRate);