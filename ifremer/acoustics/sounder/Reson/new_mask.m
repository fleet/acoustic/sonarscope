function [sum_sig]=new_mask(amp,phase,win,win_filt,filt)

nb_samples=size(amp,1);
nb_beams=size(amp,2);
sig_complex=amp.*exp(1i*phase * (pi/180));
delta_theta=nan(nb_samples,nb_beams);

if filt==1
sig_complex=ifft(fft(sig_complex,[],1).*repmat(fft([ones(1,win_filt/2) zeros(1,nb_samples-win_filt) ones(1,win_filt/2)]'),1,nb_beams),[],1)/win_filt;
end

delta_theta(2:end,:)=angle(sig_complex(1:end-1,:).*conj(sig_complex(2:end,:)));
r_theta_sq=abs(sig_complex).^2;

% figure(2000)
% imagesc(delta_theta)
sum_sig=nan(nb_samples,nb_beams);
for k=win/2+1:nb_samples-win/2
    sum_sig(k,: )= 0.5 * sum(r_theta_sq(k-win/2:k+win/2,:).*delta_theta(k-win/2:k+win/2,:), 1, 'omitnan');
end



%Code de C.Sint�s
% signalFilt(noPing,:)=((ifft(fft((a(noPing,1:2190))).*(fft([ones(1,10) zeros(1,2170) ones(1,10)]))))/20);
%
% deltaTheta=angle(signalFilt(noPing,1:2189).*conj(signalFilt(noPing,2:2190)));
%
% rThetaSq=abs(signalFilt(noPing,1:2189)).^2;
%
% for cpt=windowsize/2+1:2189-windowsize/2
%
%                 som(cpt)=0.5*sum(rThetaSq(cpt-windowsize/2:cpt+windowsize/2).*deltaTheta(cpt-                windowsize/2:cpt+windowsize/2)) ;
%
% end