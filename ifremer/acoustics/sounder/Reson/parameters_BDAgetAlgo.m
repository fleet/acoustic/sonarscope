function [flag, IdentAlgo] = parameters_BDAgetAlgo

IdentAlgo = [];

str1 = 'Classique';
str2 = 'Usual';
str{1} = Lang(str1,str2);
str1 = 'Fond plat (limite les cornes d''Eric)';
str2 = 'Flat bottom (limits the Eric horns)';
str{2} = Lang(str1,str2);
str1 = 'Sp�cial pour colonne d''eau tr�s bruit�e';
str2 = 'Special for very noisy WC data';
str{3} = Lang(str1,str2);

str1 = 'Choix de l''algorithme - 1/2';
str2 = 'Algorithm selection - 1/2';
[IdentAlgo1, flag] = my_listdlg(Lang(str1,str2), str, 'SelectionMode', 'Single');
if ~flag
    return
end

str1 = 'Classique';
str2 = 'Usual';
str{1} = Lang(str1,str2);
str1 = 'H�ritage depth min et max du ping pr�c�dent';
str2 = 'Min & Max depth heritage from previous ping';
str{2} = Lang(str1,str2);
str1 = 'Double d�tection pour ajustement de la Depth Gate';
str2 = 'Double detection for gate adjustment';
str{3} = Lang(str1,str2);
str1 = 'Choix de l''algorithme - 2/2';
str2 = 'Algorithm selection - 2/2';
[IdentAlgo2, flag] = my_listdlg(Lang(str1,str2), str, 'SelectionMode', 'Single');
if ~flag
    return
end

IdentAlgo = (IdentAlgo2-1)*3 + IdentAlgo1;
