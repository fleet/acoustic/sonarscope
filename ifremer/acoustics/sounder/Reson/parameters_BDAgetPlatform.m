function flag = parameters_BDAgetPlatform(varargin)

global TestPlatformCmatlab %#ok<GVMIS>

[varargin, Default] = getFlag(varargin, 'Default'); %#ok<ASGLU>

if Default
    TestPlatformCmatlab.barycentre                               = 1;
    TestPlatformCmatlab.gradient                                 = 1;
    TestPlatformCmatlab.test_shape                               = 1;
    TestPlatformCmatlab.matrix_compensation                      = 1;
    TestPlatformCmatlab.SampleBeam_ReduceMatrixAmplitude         = 1;
    TestPlatformCmatlab.ResonSyntheseBottomDetector              = 1;
    TestPlatformCmatlab.ResonDetectionPhaseBeam                  = 1;
    TestPlatformCmatlab.Reson7150_R0_estimation_V1               = 1;
    TestPlatformCmatlab.Reson_phaseFilter                        = 1;
    TestPlatformCmatlab.estimationR0OnStrip                      = 1;
    TestPlatformCmatlab.testDoubleShape                          = 1;
    TestPlatformCmatlab.ResonMasquePhase_V5                      = 1;
    TestPlatformCmatlab.detectionAmplitudePingOnCompensatedImage = 1;
    TestPlatformCmatlab.ResonDetectionPhasePing_3steps           = 1;
    TestPlatformCmatlab.detectionAmplitudePing                   = 1;
    TestPlatformCmatlab.SampleBeam_PreprocAmpMask                = 1;
    TestPlatformCmatlab.BDA_compensAmpByMHorzMedian              = 1;
    TestPlatformCmatlab.sonar_BDA                                = 1;
    return
end

TestPlatformCmatlab.Seuil_ResonSyntheseBottomDetector_PingRange     = 0.001;
TestPlatformCmatlab.Seuil_ResonSyntheseBottomDetector_TD            = 0;
TestPlatformCmatlab.Seuil_ResonSyntheseBottomDetector_QualityFactor = 0.001;

TestPlatformCmatlab.Seuil_detectionAmplitudePing_AmPingRange = 0.5; % Seuil un peu �lev�
TestPlatformCmatlab.Seuil_detectionAmplitudePing_AmpPingQF   = 0.1;

TestPlatformCmatlab.Seuil_estimationR0OnStrip_r0Strip   = 0.01;
TestPlatformCmatlab.Seuil_estimationR0OnStrip_flagBeams = 0;

TestPlatformCmatlab.Seuil_testDoubleShape_subOut = 0;

TestPlatformCmatlab.Seuil_ResonDetectionPhasePing_3steps_PhasePingRange     = 0.1;
TestPlatformCmatlab.Seuil_ResonDetectionPhasePing_3steps_PhasePingEqm       = 0.01;
TestPlatformCmatlab.Seuil_ResonDetectionPhasePing_3steps_PhasePingPente     = 0.001;
TestPlatformCmatlab.Seuil_ResonDetectionPhasePing_3steps_PhasePingNbSamples = 0.01;
TestPlatformCmatlab.Seuil_ResonDetectionPhasePing_3steps_PhasePingQF        = 0.01;

TestPlatformCmatlab.Seuil_SampleBeam_PreprocAmpMask_RMax1              = 0;
TestPlatformCmatlab.Seuil_SampleBeam_PreprocAmpMask_RMax2              = 0;
TestPlatformCmatlab.Seuil_SampleBeam_PreprocAmpMask_AmpPingNormHorzMax = 0;
TestPlatformCmatlab.Seuil_SampleBeam_PreprocAmpMask_R1SamplesFiltre    = 0;
TestPlatformCmatlab.Seuil_SampleBeam_PreprocAmpMask_MaskWidth          = 0;

TestPlatformCmatlab.Seuil_Reson7150_R0_estimation_V1_R0StripOriginal = 0;
TestPlatformCmatlab.Seuil_Reson7150_R0_estimation_V1_flagBeams       = 0;

TestPlatformCmatlab.Seuil_ResonDetectionPhaseBeam_x1     = 1;
TestPlatformCmatlab.Seuil_ResonDetectionPhaseBeam_phase1 = 0.00001;
TestPlatformCmatlab.Seuil_ResonDetectionPhaseBeam_amp1   = 0.00001;

TestPlatformCmatlab.Seuil_sonar_BDA_PingRange     = 0.1;
TestPlatformCmatlab.Seuil_sonar_BDA_QualityFactor = 0.01;
TestPlatformCmatlab.Seuil_sonar_BDA_TD            = 0;


% 1=Matlab
% 2=C
% 3=comparaison Matlab/C avec conservation des r�sultats Matlab
% 4=comparaison Matlab/C avec conservation des r�sultats C

% Niveau 1
TestPlatformCmatlab.barycentre                               = 1; % OK sur pings 1 et 82
TestPlatformCmatlab.gradient                                 = 1; % OK sur ping1 1 et 82
TestPlatformCmatlab.test_shape                               = 1; % OK sur pings 1 et 82
TestPlatformCmatlab.matrix_compensation                      = 1; % OK sur pings 1 et 82
TestPlatformCmatlab.SampleBeam_ReduceMatrixAmplitude         = 1; % OK sur pings 1 et 82
TestPlatformCmatlab.ResonSyntheseBottomDetector              = 1; % OK sur pings 1 et 82
TestPlatformCmatlab.ResonDetectionPhaseBeam                  = 1; % OK sur pings 1 et 82

TestPlatformCmatlab.Reson7150_R0_estimation_V1               = 1; % 1 diff�rence sur flag sur ping 82 et quelques  diff�rences sur ping 1 : ecart acceptable (28/08/2014)
TestPlatformCmatlab.Reson_phaseFilter                = 2; % Recontr�ler Reson_phaseFilter.m
TestPlatformCmatlab.estimationR0OnStrip                      = 1; % TODO, une seule des variables de sortie est accessible, les autres sont en variables locales interm�diaires (obligtoirement [1, 3 ou 4])
                                                                  % Atteint que si Reson7150_R0_estimation_V1 version Matlab est atteint

% Niveau 2 d�pendant de test_shape
TestPlatformCmatlab.testDoubleShape                          = 1; % OK sur pings 1 et 82

% Niveau 2 d�pendant de calcul_barycentre uniquement
TestPlatformCmatlab.ResonMasquePhase_V5                = 2; % Non identique mais le r�sultat C est meilleur. A tester sur 7111 !
TestPlatformCmatlab.detectionAmplitudePingOnCompensatedImage = 1; % Petites diff�rences

% Niveau 2 d�pendant uniquement de ResonDetectionPhaseBeam
TestPlatformCmatlab.ResonDetectionPhasePing_3steps           = 1; % OK sur pings 1 et 82

% Niveau 2 d�pendant de calcul_barycentre et test_shape
TestPlatformCmatlab.detectionAmplitudePing                  = 1; % OK sur pings 1 et 82

% Niveau 3
TestPlatformCmatlab.SampleBeam_PreprocAmpMask                = 1; % Grosses diff�rences (ping 82)

% TODO
TestPlatformCmatlab.sonar_BDA                                = 1; % TODO
TestPlatformCmatlab.BDA_compensAmpByMHorzMedian              = 1; % TODO

flag = 1;
