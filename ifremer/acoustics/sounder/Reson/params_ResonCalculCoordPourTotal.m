function [flag, nomDir7KCenter, nomDirPDS, nomFicIn, nomFicOut, nomFicCarto, repImport] = params_ResonCalculCoordPourTotal(repImport)

persistent persistent_nomDirFicIn persistent_nomDirPDS persistent_nomDir7KCenter persistent_nomFicCarto persistent_nomFicOut
 
%  % Phase_D1 : 
%  nomFicIn       = 'P:\USAN\Phase_D1\Bulles_D1\Phase_D1_pings_beams.txt';
%  nomDirPDS      = 'R:\USAN-General\PDS_LEG2';
%  nomDir7KCenter = 'P:\USAN\Phase_D1\7KCenter_D1';
%  nomFicCarto    = 'R:\USAN-General\Carto.def';
%  nomFicOut      = 'P:\USAN\Phase_D1\Bulles_D1\Phase_D1_pings_beams_Complete.csv';
%  flag = CalculCoordPourTotal(cl_reson_s7k.empty(), nomDir7KCenter, nomDirPDS, nomFicIn, nomFicOut, nomFicCarto)

nomDir7KCenter  = [];
nomDirPDS       = [];
nomFicIn        = [];
nomFicOut       = [];
nomFicCarto     = [];

%% nomFicIn

if isempty(persistent_nomDirFicIn)
    lastDir = repImport;
else
    lastDir = persistent_nomDirFicIn;
end
[flag, ListeFic, lastDir] = uiSelectFiles('ExtensionFiles', '.txt', 'RepDefaut', lastDir);
if ~flag
    return
end
nomFicIn = ListeFic{1};
persistent_nomDirFicIn = lastDir;
repImport = lastDir;

%% nomDirPDS

if isempty(persistent_nomDirPDS)
    lastDir = repImport;
else
    lastDir = persistent_nomDirPDS;
end

str1 = 'Nom du répertoire contenant les fichiers .s7k "PDS".';
str2 = 'Name of the directory containing the "PDS" .s7k files.';
[flag, lastDir] = my_uigetdir(lastDir, Lang(str1,str2));
if ~flag
    return
end
nomDirPDS = lastDir;
persistent_nomDirPDS = lastDir;
repImport = lastDir;

%% nomDir7KCenter

if isempty(persistent_nomDir7KCenter)
    lastDir = repImport;
else
    lastDir = persistent_nomDir7KCenter;
end

str1 = 'Nom du répertoire contenant les fichiers .s7k "7KCenter".';
str2 = 'Name of the directory containing the "7KCenter" .s7k files.';
[flag, lastDir] = my_uigetdir(lastDir, Lang(str1,str2));
if ~flag
    return
end
nomDir7KCenter = lastDir;
persistent_nomDir7KCenter = lastDir;
repImport = lastDir;

%% nomFicCarto

if isempty(persistent_nomFicCarto)
    lastDir = repImport;
else
    lastDir = persistent_nomFicCarto;
end
[flag, ListeFic] = uiSelectFiles('ExtensionFiles', '.def', 'RepDefaut', lastDir);
if ~flag
    return
end
nomFicCarto = ListeFic;
persistent_nomFicCarto = fileparts(nomFicOut);
repImport = persistent_nomFicCarto;

%% nomFicOut

if isempty(persistent_nomFicOut)
    lastDir = repImport;
else
    lastDir = persistent_nomFicOut;
end
nomFic = fullfile(lastDir, 'Bubbles_pings_beams_Complete.csv');
str1 = 'Nom du fichier de sortie.';
str2 = 'Name of the output file.';
[flag, nomFicOut] = my_uiputfile(nomFic, Lang(str1,str2));
if ~flag
    return
end
persistent_nomFicOut = fileparts(nomFicOut);
repImport = persistent_nomFicOut;
