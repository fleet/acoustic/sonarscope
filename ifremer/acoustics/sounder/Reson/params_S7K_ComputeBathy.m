function [flag, nbPlotPerImage] = params_S7K_ComputeBathy

nbPlotPerImage = [];

%% Warning

 str1 = 'TODO';
 str2 = sprintf('This processing should be used ONLY if your files do not contain depth, across and along distances.\nAt Ifremer, we use PDS on board our vessels, PDS computes the bathymetry and adds the resut in the optional data part of the .s7k datagrams.\nThis processing will replace the bathymetry layer (if it already exists) in the SonarScope cahe directory.\n\nDo we continue ?');
%  my_warndlg(Lang(str1,str2), 1);     
[rep, flag] = my_questdlg(Lang(str1,str2), 'Init', 2);
if ~flag || (rep == 2)
    return
end

%% 

str1 = 'Contr�le du calcul de la bathym�trie (0 pour aller le plus vite)';
str2 = 'Control of the bathymetry computation (set to 0 for speed)';
p = ClParametre('Name', Lang('TODO', 'Nb plots per image'), ...
    'MinValue', 0, 'MaxValue', 100, 'Value', 3, 'Format', '%d');
a = StyledParametreDialog('params', p, 'Title', Lang(str1,str2));
% a.Help = 'http://flotte.ifremer.fr/fleet/Presentation-of-the-fleet/On-board-software/SonarScope';
a.sizes = [0 -2 -1 -2 -1 -1 0 0];
a.openDialog;
flag = a.okPressedOut;
if ~flag
    return
end
nbPlotPerImage = a.getParamsValue;
