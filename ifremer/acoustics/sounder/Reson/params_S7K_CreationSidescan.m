function [flag, repExport] = params_S7K_CreationSidescan(repExport)

str1 = 'Nom du r�pertoire o� les images sidescan seront sauvegard�s.';
str2 = 'Name of the directory where the sidescan images will be saved.';
[flag, repExport] = my_uigetdir(repExport, Lang(str1,str2));
if ~flag
    return
end
