function [flag, DisplayLevel, nomFicAvi, GateDepth, nomDirErs, IdentAlgo, skipAlreadyProcessedFiles, ...
    repImport, repExport] = params_S7K_DetectionPhaseAndAmplitude(listFileNames, repImport, repExport)

DisplayLevel  = 3;
nomFicAvi     = [];
nomDirErs     = [];
IdentAlgo     = [];
skipAlreadyProcessedFiles = [];
            
%% R�glage de la fen�tre d'observation en Z

[flag, GateDepth] = parameters_gateDepthBDA;
if ~flag
    return
end

%% Saisie de mode de d�tection (a revalider quand un travail sera fait sur les �paves)

[flag, IdentAlgo] = parameters_BDAgetAlgo;
if ~flag
    return
end

%% TestPlatformCmatlab

flag = parameters_BDAgetPlatform;
if ~flag
    return
end

%% Nom de r�pertoire o� sauvegarder les fichiers

str1 = 'R�pertoire de sauvegarde des fichiers';
str2 = 'Directory to save files';
[flag, nomDirErs] = my_uigetdir(repExport, Lang(str1,str2));
if ~flag
    return
end

%% Skip existingResults

str1 = 'Voulez-vous ignorer les fichiers d�j� trait�s ?';
str2 = 'Do you want to skip the already processed files ?';
[rep, flag] = my_questdlg(Lang(str1,str2));
if ~flag
    return
end
skipAlreadyProcessedFiles = (rep == 1);

%% Saisie du niveau de visualisation

[flag, DisplayLevel] = question_DetailLevelBDA('Default', 1);
if ~flag
    return
end

%% Cr�ation d'un film ?

if DisplayLevel >= 2
    [CreateMovie, flag] = my_questdlg(Lang('Cr�ation d''un film ?', 'Create a movie ?'));
    if CreateMovie == 1
        [~, filename] = fileparts(listFileNames{1});
        nomFicAvi = fullfile(repExport, [filename '_BottomDetector.avi']);
        [flag, nomFicAvi] = my_uiputfile('*.avi', 'Give a file name', nomFicAvi);
        if ~flag
            return
        end
        repExport = fileparts(nomFicAvi);
    end
end
