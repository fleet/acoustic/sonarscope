function [flag, nomFicCsv, repExport] = params_S7K_Summary(DefaultCsvName, repExport)

%% Name of the output .csv file

Filtre = fullfile(repExport, DefaultCsvName);
str1 = 'Nom du fichier r�sum� :';
str2 = 'Summary file name';
[flag, nomFicCsv] = my_uiputfile('*.csv', Lang(str1,str2), Filtre);
if ~flag
    return
end
repExport = fileparts(nomFicCsv);
