function [flag, Side, NomDirXML, TailleMax, Tag, ColormapIndex, Video, CLim, repExport] = params_S7K_WC_Export3DViewer(repExport)

persistent persistent_CLim

NomDirXML     = [];
TailleMax     = [];
Video         = [];
CLim          = [];
ColormapIndex = [];
Tag           = [];

%% C�t�

str1 = 'De quel c�t� voulez-vous extraire la donn�e de Colonne d''eau ?';
str2 = 'What side do you want to extract Water Column';
[Side, flag] = my_questdlg(Lang(str1,str2), Lang('B�bord', 'Port'), Lang('Tribord', 'Starboard'));
if ~flag
    return
end

%% Taille max des images

[flag, TailleMax] = inputOneParametre('Maximum size (M pixels)', 'Value', ...
    'Value', 4, 'Unit', 'mega pixels', 'MinValue', 0.5);
if ~flag
    return
end
TailleMax = TailleMax * 1e6;

%% ColormapIndex

ColormapIndex = 3;

%% Video

Video = 1;

%% Bornes de rehaussement de contraste

if isempty(persistent_CLim)
    minVal   = 0;
    maxVal   = 100;
else
    minVal   = persistent_CLim(1);
    maxVal   = persistent_CLim(2);
end

[flag, CLim] = saisie_CLim(minVal, maxVal, 'Amp', 'SignalName', 'S7K WC data');
if ~flag
    return
end
persistent_CLim = CLim;

%% Nom du r�pertoire de sortie

str1 = 'Nom du r�pertoire o� seront cr��s les fichiers. Utilisez le bouton "Create a new directory" si n�cessaire.';
str2 = 'Name of the directory to save this individual files. Use the "Create a new directory" button if necessary.';
[flag, NomDirXML] = my_uigetdir(repExport, Lang(str1,str2));
if ~flag
    return
end
repExport = NomDirXML;

%% Tag suppl�mentaire dans le mom des images

[flag, Tag] = question_AddTagInOutputFile;
if ~flag
    return
end
