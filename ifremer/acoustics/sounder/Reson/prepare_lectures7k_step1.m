% Prelecture des fichiers .all : Transcodage des datagrams runtime, attitude, clock,
% verticalDepth, navigation, SoundSpeed, soundSpeedProfile, depth et rawRangeBeamAngle
%
% Syntax
%   [flag, Carto] = prepare_lectures7k_step1(liste)
%
% Input Arguments
%   liste : Nom de fichier ou liste de noms de fichiers ou repertoires
%
% Examples
%   nomFic = '/home1/jackson/7K/20050905_021342.s7k';
%   [flag, Carto] = prepare_lectures7k_step1(nomFic)
%
%   nomDir = fileparts(nomFic);
%   liste = listeFicOnDepthDir(nomDir, '.s7k')
%   [flag, Carto] = prepare_lectures7k_step1(liste)
%
% See also cl_reson_s7k Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [flag, Carto] = prepare_lectures7k_step1(liste, varargin)

global isUsingParfor %#ok<GVMIS> 
isUsingParfor = 0; % Why ?

[varargin, Carto] = getPropertyValue(varargin, 'Carto', []); %#ok<ASGLU>

flag = 0;

useParallel = get_UseParallelTbx;

% Carto = []; % Comment� par JMA le 11/03/2019 : je ne sais pas pourquoi cette varaiable �tait remise � []

if ~iscell(liste)
    liste = {liste};
end

if isempty(liste)
    return
end

%% Traitement premier fichier

kFin = 0;
N = length(liste);
if isempty(Carto)
    for k=1:N
        s7k = cl_reson_s7k('nomFic', liste{k}, 'KeepNcID', true);
        [~, Carto] = view_depth(s7k, 'ListeLayers', 6, 'memeReponses', 2, 'Carto', Carto);
        clear s7k
        if ~isempty(Carto)
            prepare_lectures7k_step1_unitaire(liste{k}, 'Carto', Carto);
            kFin = k;
            break
        end
    end
end
kFin = kFin + 1;

%% Fichiers suivants

str1 = 'V�rification / Cr�ation du cache des fichiers S7K';
str2 = 'Check / Create cache files of S7K files';
if useParallel && (N > 2)
    [hw, DQ] = create_parforWaitbar(N, Lang(str1,str2));
    parfor (k=kFin:N, useParallel)
%     parfor (k=kFin:N, 1)
%     for k=kFin:N
        try
            prepare_lectures7k_step1_unitaire(liste{k}, 'Carto', Carto, 'isUsingParfor', true);
            fprintf(1, 'prepare_lectures7k_step1_unitaire OK for file "%s"\n', liste{k});
        catch ME
            fprintf(1, '\nBug with //Tbx for file "%s"\n', liste{k});
            ME.getReport()
        end
        send(DQ, k);
    end
    my_close(hw, 'MsgEnd')
else
    hw = create_waitbar(Lang(str1,str2), 'N', N);
    for k=kFin:N
        my_waitbar(k, N, hw)
        prepare_lectures7k_step1_unitaire(liste{k}, 'Carto', Carto);
    end
    my_close(hw, 'MsgEnd')
end
flag = 1;


function [flag, Carto] = prepare_lectures7k_step1_unitaire(s7kFilename, varargin)

global isUsingParfor %#ok<GVMIS>

[varargin, isUsingParfor] = getPropertyValue(varargin, 'isUsingParfor', isUsingParfor);
[varargin, Carto]         = getPropertyValue(varargin, 'Carto',         []); %#ok<ASGLU>

flag = 0;

%% Check if the file has been converted

[nomDir, nomFic] = fileparts(s7kFilename);
ncFileName = fullfile(nomDir, 'SonarScope', [nomFic '.nc']);
[pathname, filename] = fileparts(ncFileName);
statusFileName = fullfile(pathname, [filename '.status.txt']);
if exist(statusFileName, 'file')
    return
end
ncFileName = fullfile(nomDir, 'SonarScope', [nomFic '.NC']);

% fprintf(1, '---------------------------------------------------------------\n')

s7k = cl_reson_s7k('nomFic', s7kFilename, 'KeepNcID', false);
if isempty(s7k)
    return
end

s7k = cl_reson_s7k('nomFic', s7kFilename, 'KeepNcID', true);
if ~isempty(Carto)
    try
        AlreadyChecked = netcdf.getAtt(s7k.ncID, -1, 'AlreadyChecked');
        if AlreadyChecked
            flag = 1;
            return
        end
    catch
        AlreadyChecked = 0; %#ok<NASGU>
    end
end

identSondeur = selectionSondeur(s7k);
if isempty(identSondeur)
    return
end

[flag, DataIndex] = readFicIndex(s7k, identSondeur);
if ~flag
    return
end


nomFicMat = fullfile(nomDir, 'SonarScope', [nomFic '_attitude.mat']);
if ~exist(nomFicMat, 'file') && ~(exist(ncFileName, 'file') && get(s7k, 'ExistGrpAttitude'))
    Data = read_attitude(s7k, identSondeur, 'DataIndex', DataIndex); %#ok<NASGU>
    clear Data
end

nomFicMat = fullfile(nomDir, 'SonarScope', [nomFic '_verticalDepth.mat']);
if ~exist(nomFicMat, 'file') && ~(exist(ncFileName, 'file') && get(s7k, 'ExistGrpVerticalDepth'))
    Data = read_verticalDepth(s7k, identSondeur, 'DataIndex', DataIndex); %#ok<NASGU>
    clear Data
end

nomFicMat = fullfile(nomDir, 'SonarScope', [nomFic '_navigation.mat']);
if ~exist(nomFicMat, 'file') && ~(exist(ncFileName, 'file') && get(s7k, 'ExistGrpNavigation'))
    Data = read_navigation(s7k, identSondeur, 'DataIndex', DataIndex); %#ok<NASGU>
    clear Data
end

nomFicMat = fullfile(nomDir, 'SonarScope', [nomFic '_soundSpeedProfile.mat']);
if ~exist(nomFicMat, 'file') && ~(exist(ncFileName, 'file') && get(s7k, 'ExistGrpSoundSpeedProfile'))
    Data = read_soundSpeedProfile(s7k, identSondeur, 'DataIndex', DataIndex); %#ok<NASGU>
    clear Data
end

% Modif (lignes comment�es) JMA le 13/02/2018 pour que Carto soit extrait sinon sa d�finition �tait demand�e dans la cr�ation des WC et entrainait un arr�t des la //Tbx

nomFicMat = fullfile(nomDir, 'SonarScope', [num2str(identSondeur) '_' nomFic '_depth.mat']);
if ~exist(nomFicMat, 'file') && ~(exist(ncFileName, 'file') && get(s7k, 'ExistGrpDepth'))
    if ~isempty(s7k) && isempty(Carto)
        [~, Carto] = view_depth(s7k, 'ListeLayers', 6, 'memeReponses', 2, 'Carto', Carto, ...
            'identSondeur', identSondeur, 'DataIndex', DataIndex, 'isUsingParfor', isUsingParfor);
    end
end

%% Water column

[nomFicWaterColumnData, flagFilesExist] = ResonWaterColumnDataNames(s7kFilename, identSondeur, []); %#ok<ASGLU>
[~, ~, Ext] = fileparts(nomFicWaterColumnData.Mat);
if strcmp(Ext, '.xml')
    read_WaterColumnData(s7k, identSondeur, 'flagNoPing', 1);
elseif ~(exist(ncFileName, 'file') && get(s7k, 'ExistGrpWaterColumnDataIFRData'))
    read_WaterColumnData(s7k, identSondeur, 'Display', 0, 'flagNoPing', 1, 'DataIndex', DataIndex);
end

if ~(exist(ncFileName, 'file') && get(s7k, 'ExistGrpWCD'))
    read_WaterColumnData(s7k, identSondeur, 'Display', 0, 'flagNoPing', 0, 'Mute', 1, 'DataIndex', DataIndex);
end

%% Mag&Phase

% {
% Comment� car �a traite les snippets
[~, nomFicSeul] = fileparts(s7kFilename);
if ~(exist(ncFileName, 'file') && get(s7k, 'ExistGrpMagPhase')) && (length(nomFicSeul) == 15)
    read_BeamRawData(s7k, identSondeur, 'DisplayLevel', 0, 'Mute', 1, 'DataIndex', DataIndex);
end
% }

%% Sidescan % A supprimer apr�s BICOSE

%{
Carto  = [];
str1 = 'Pr�traitement des fichiers S7K (3/3)';
str2 = 'Preprocessing S7K files, second step (3/3)';
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
my_waitbar(k, N, hw)
s7k = cl_reson_s7k('nomFic', liste{k});
if isempty(s7k)
continue
end
fprintf(1, '---------------------------------------------------------------\nread_sidescan  %d/%d : %s\n', k, N, liste{k});

[b, Carto, subl_Image, subl_Depth]  = view_SidescanImagePDS(s7k, 'Carto', Carto);
end
my_close(hw, 'MsgEnd')
%}

%% Ecriture de l'attribut Already checked

netcdf.putAtt(s7k.ncID, -1, 'AlreadyChecked', 1);
clear s7k

s7k = cl_reson_s7k('nomFic', s7kFilename);

fid = fopen(statusFileName, 'wt+');
fprintf(fid, 'AlreadyChecked = 1\n');
fprintf(fid, 'ExistGrpIndex = %d\n',                  s7k.ExistGrpIndex);
fprintf(fid, 'ExistGrpAttitude = %d\n',               s7k.ExistGrpAttitude);
fprintf(fid, 'ExistGrpVerticalDepth = %d\n',          s7k.ExistGrpVerticalDepth);
fprintf(fid, 'ExistGrpNavigation = %d\n',             s7k.ExistGrpNavigation);
fprintf(fid, 'ExistGrpSoundSpeedProfile = %d\n',      s7k.ExistGrpSoundSpeedProfile);
fprintf(fid, 'ExistGrpDepth = %d\n',                  s7k.ExistGrpDepth);
fprintf(fid, 'ExistGrpWaterColumnDataIFRData = %d\n', s7k.ExistGrpWaterColumnDataIFRData);
fprintf(fid, 'ExistGrpWCD = %d\n',                    s7k.ExistGrpWCD);
fprintf(fid, 'ExistGrpBeamRawData = %d\n',            s7k.ExistGrpBeamRawData);
fprintf(fid, 'ExistGrpMagPhase = %d\n',               s7k.ExistGrpMagPhase);
fprintf(fid, '\n');
str = char(s7k);
for k=1:size(str,1)
    fprintf(fid, '%s\n', str(k,:));
end
fprintf(fid, '\nThis file is created to check quickly if the file has been converted otherwise we should open the netcdf file and this operation is VERY costful when there are plenty of variables in the file (that is the case when there is WCD).\n');
fclose(fid);

%% Renommage de l'extension .NC en .nc pour indiquer � l'utilisateur que le fichier est termin�

% my_rename(ncFileName, strrep(ncFileName, '.NC', '.nc'));
% fprintf(1, '--- Apr�s my_rename ---\n')
