% Pretraitement des fichiers .s7k de .Reson.
% Cette operation n'est pas necessaire mais on gagnera du temps lors des
% premieres lectures de fichier
%
% [Fig1, flag] = preprocessS7kFiles(...)
%
% Name-Value Pair Arguments
%   nom : Nom d'un repertoire contenant des .s7k ou nom d'un fichier .s7k
%   ou tableau de cellules contenant des noms de fichiers .s7k
%
% Name-Value Pair Arguments
%   Option        : {'All of them'} | 'Index' | 'Navigation' | 'Attitude'
%                   | 'SoundSpeedProfile' | 'SurfaceSoundSpeed' | 'VerticalDepth'
%
% Output Arguments
%   fig : Numero de la figure de la navigation si navigation demandee
%   flag : 0/1
%
% Examples
%   preprocessS7kFiles
%
%   nomDir = '/home2/rayleigh/calimero/EM300'
%   liste = listeFicOnDir2(nomDir, '.s7k')
%   preprocessS7kFiles(liste{1})
%   preprocessS7kFiles(liste{1:3})
%   preprocessS7kFiles(liste)
%
%   preprocessS7kFiles(nomDir)
%
% See also Ex-cl_reson_s7k_01 cl_reson_s7k Authors
% Authors : JMA
%------------------------------------------------------------------------------------------

function [Fig1, flag] = preprocessS7kFiles(varargin)

[varargin, Tasks]         = getPropertyValue(varargin, 'Tasks',         []);
[varargin, flagPlotIndex] = getPropertyValue(varargin, 'flagPlotIndex', 1);
[varargin, flagPlotHisto] = getPropertyValue(varargin, 'flagPlotHisto', 1);
[varargin, LineWidth]     = getPropertyValue(varargin, 'LineWidth',     3);
[varargin, Mute]          = getPropertyValue(varargin, 'Mute',          1);

Fig1 = [];

useParallel = get_UseParallelTbx;

if isempty(varargin)
    [flag, listeNomFic] = my_uigetfile('*.s7k', 'MultiSelect','on');
    if ~flag
        return
    end
    if ~iscell(listeNomFic)
        listeNomFic = {listeNomFic};
    end
else
    if iscell(varargin{1})
        listeNomFic = varargin{:};
    else
        if isfolder(varargin{1})
            listeNomFic = listeFicOnDir2(varargin{1}, '.s7k');
        else
            listeNomFic = varargin(:);
        end
    end
end

%% Instances de cl_reson_s7k

NbFic = length(listeNomFic);

if ~Mute
    flag = messageParallelTbx(NbFic > 1);
    if ~flag
        return
    end
end

str1 = 'V�rification des .s7k';
str2 = 'Checking .s7k files';
if useParallel && (NbFic > 1)
    [hw, DQ] = create_parforWaitbar(NbFic, Lang(str1,str2));
    parfor (k=1:NbFic, useParallel)
        cl_reson_s7k('nomFic', listeNomFic{k}, 'KeepNcID', true);
        send(DQ, k);
    end
    my_close(hw, 'MsgEnd')
end
% else % Comment� par JMA le 06/05/2021 car le ncID cr�� dans un worker de la //Tbx ne peut pas �tre utilis�
    hw = create_waitbar(Lang(str1,str2), 'N', NbFic);
    for k=1:NbFic
        my_waitbar(k, NbFic, hw)
        X = cl_reson_s7k('nomFic', listeNomFic{k}, 'KeepNcID', true);
        if ~isempty(X)
            s7k(k) = X; %#ok<AGROW>
        end
    end
    my_close(hw, 'MsgEnd')
% end

% disp('Ici (preprocessS7kFiles) bug a trouver si on decommente l''affichage de l''instance cl_reson_s7k')
% a

%% Traitement

S0 = cl_reson_s7k.empty();
Option = S7K_listSignals(S0);
Option = Option(Tasks);

histoTypeDatagram = zeros(NbFic, 9999);
tabNbBytes        = zeros(NbFic, 9999);
if any(contains(Option, 'Index'))
    k2 = 0;
    NbFic = length(s7k);
    str1 = 'Trac� des info des fichiers d''index';
    str2 = 'Plot index files';
    hw = create_waitbar(Lang(str1,str2), 'N', NbFic);
    for k=1:NbFic
        my_waitbar(k, NbFic, hw);
        
        identSondeur = selectionSondeur(s7k(k));
        if isempty(identSondeur)
            continue
        end
        
        % Visualisation graphique des types de datagrammes
        if flagPlotIndex
            plot_index(s7k(k), identSondeur)
        end
        
        % Histogramme des types de datagrammes
        if flagPlotHisto
            histo_index(s7k(k), identSondeur);
        end
        
        k2 = k2 + 1;
        [~, X1, X2, X3] = histo_index(s7k(k), identSondeur);
        
        Tmp = zeros(1,9999);
        Tmp(X1) = X2;
        histoTypeDatagram(k,:) = Tmp;
        
        Tmp = zeros(1,9999);
        Tmp(X1) = X3;
        tabNbBytes(k,:)= Tmp;
    end
    my_close(hw);
    
    
    Code = find(sum(histoTypeDatagram, 1) ~= 0);
    sumHistoTypeDatagram = sum(histoTypeDatagram(:,Code), 1);
    sumTabNbBytes = sum(tabNbBytes(:,Code), 1);
    texteTypeDatagram = codesDatagramsS7k(Code, sumHistoTypeDatagram);

    [~, ~, Fig1] = plot_histo_index(Code, sumHistoTypeDatagram, texteTypeDatagram, sumTabNbBytes, 'Datagrams');
    flag = 1;
end

for k=1:NbFic
    identSondeur = selectionSondeur(s7k(k));
    if ~isempty(identSondeur)
        break
    end
end

%% Affichage des donn�es contenues dans les datagrammes "position"

if any(contains(Option, 'Navigation'))
    [Fig1, flag] = plot_nav_S7K(s7k, 'LineWidth', LineWidth);
    if ~flag
        return
    end
end

%% Traac� de la nav dans Google Earth

% if any(contains(Option, 'NavigationGoogleEarth'))
%     plot_navigationOnGoogleEarth(s7k, identSondeur);
% end

%% Affichage des donn�es contenues dans les datagrammes "attitude"

if any(contains(Option, 'Attitude'))
    plot_attitude(s7k, identSondeur);
end

%% Affichage des donn�es contenues dans les datagrammes "SonarSettings"

if any(contains(Option, 'SonarSettings'))
    plot_sonarSettings(s7k, identSondeur);
end

%% Affichage des donn�es contenues dans les datagrammes "7k Configuration"
    
if any(contains(Option, '7k Configuration'))
    plot_7kConfiguration(s7k, identSondeur);
end

%% Affichage des donn�es vecteurs contenues dans les datagrammes "soundSpeedProfile"

if any(contains(Option, 'SoundSpeedProfile'))
    plot_soundSpeedProfile(s7k, identSondeur)
end

%% Affichage des donn�es vecteurs contenues dans les datagrammes "Surface Sound Speed"

if any(contains(Option, 'SurfaceSoundSpeed'))
    plot_surfaceSoundSpeed(s7k, identSondeur)
    plot_surfaceSoundSpeed_3D(s7k, identSondeur)
end

%% Affichage des donn�es vecteurs contenues dans les datagrammes "VerticalDepth"
    
if any(contains(Option, 'VerticalDepth'))
    plot_verticalDepth(s7k, identSondeur)
end

%% Lecture des datagrams "Depth"
    
if any(contains(Option, 'Depth'))
    NbFic = length(listeNomFic);
    hw = create_waitbar('Plot .s7k Preprocessing Depth', 'N', NbFic);
    for k=1:NbFic
        my_waitbar(k, NbFic, hw);
        read_depth_s7k(s7k(k), 'identSondeur', identSondeur);
    end
    my_close(hw, 'MsgEnd');
end
