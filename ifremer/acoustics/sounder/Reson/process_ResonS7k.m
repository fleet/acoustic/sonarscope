function [repImport, repExport] = process_ResonS7k(listFileNames, Action, repImport, repExport)

% TODO : continuer les modifs : ramener les traitements dans
% callback_sonar_S7K et supprimer process_ResonS7k

switch Action      
    case 'ExportCaraibesNvi'
        filtre = fullfile(repExport, 'NavigationFroms7k.nvi');
        [flag, nomFicNav] = my_uiputfile('*.nvi', 'Give a file name', filtre);
        if ~flag
            return
        end
        [pathname, nomFicNav] = fileparts(nomFicNav);
        repExport = pathname;
        nomFicNav = fullfile(pathname, [nomFicNav '.nvi']);
        
        if ~iscell(listFileNames)
            listFileNames = {listFileNames};
        end
        for k=1:length(listFileNames)
            s7k(k) = cl_reson_s7k('nomFic', listFileNames{k}, 'KeepNcID', true); %#ok<AGROW>
        end
        
        identSondeur = selectionSondeur(s7k(1));
        flag = export_CaraibesNvi(s7k, nomFicNav, identSondeur);
        if flag
            plot_navigation_NVI(nomFicNav);
            pathname = fileparts(nomFicNav);
            repExport = pathname;
        end
        
    case 'ExportAttitude'
        if ~iscell(listFileNames)
            listFileNames = {listFileNames};
        end
        [nomDir, nomFicSeul] = fileparts(listFileNames{1});
        nomFicAtt = fullfile(nomDir, ['att_' nomFicSeul '.txt']);
        [flag, nomFicAtt] = my_uiputfile('*.txt', 'Give a file name', nomFicAtt);
        if ~flag
            return
        end
        [pathname, nomFicAtt] = fileparts(nomFicAtt);
        repExport = pathname;
        nomFicAtt = fullfile(pathname, [nomFicAtt '.txt']);
        
        for k=1:length(listFileNames)
            s7k(k) = cl_reson_s7k('nomFic', listFileNames{k}, 'KeepNcID', true); %#ok<AGROW>
        end
        
        % Proc�dure d'�criture de l'attitude.
        identSondeur = selectionSondeur(s7k(1));
        flag         = export_Attitude_s7k(s7k, nomFicAtt, identSondeur);        
        % TODO : voir avec JMA, l'appel de l'outil de Pierre M.
        if flag
            pathname = fileparts(nomFicAtt);
            repExport = pathname;
        end
        
    otherwise
        my_breakpoint
end
