function flag = process_ResonS7k_EchogramsGlobeFlyTexture(nomFicS7k, nomFicNav, nomFicIP, TypeWCOutput, Names, ...
    OrigineWC, ChoixNatDecibel, CLimRaw, CLimComp, TagName, MaskBeyondDetection, varargin)

global isUsingParfor %#ok<GVMIS>
isUsingParfor = 0;

[varargin, StepCompute]  = getPropertyValue(varargin, 'StepCompute',  1);
[varargin, StepDisplay]  = getPropertyValue(varargin, 'StepDisplay',  0);
[varargin, subBeams]     = getPropertyValue(varargin, 'subBeams',     []);
[varargin, subPings]     = getPropertyValue(varargin, 'subPings',     []);
[varargin, XLim]         = getPropertyValue(varargin, 'XLim',         []);
[varargin, YLim]         = getPropertyValue(varargin, 'YLim',         []);
[varargin, MaxImageSize] = getPropertyValue(varargin, 'MaxImageSize', 1000);
[varargin, typeAlgoWC]   = getPropertyValue(varargin, 'typeAlgoWC',   1);
[varargin, Carto]        = getPropertyValue(varargin, 'Carto',        []);

[varargin, skipAlreadyProcessedFiles] = getPropertyValue(varargin, 'skipAlreadyProcessedFiles', 0); %#ok<ASGLU>

useParallel = get_UseParallelTbx;

if ~iscell(nomFicS7k)
    nomFicS7k = {nomFicS7k};
end

flag = 1;

%% Lecture du fichier de navigation

if iscell(nomFicNav)
    if length(nomFicNav) > 1
        str1 = 'Il n''est pas encore pr�vu de pouvoir utiliser plusieurs fichiers de nav en m�me temps, d�sol�. Si vous avez vraiment besoin de cela faites moi en la demande, sign� JMA.';
        str2 = 'It is not possible to use several navigation files for the moment. If you really need it, please tell me, signed JMA.';
        my_warndlg(Lang(str1,str2), 1);
        return
    end
    nomFicNav = nomFicNav{1};
end

%% V�rification si des fichiers ont d�j� �t� trait�s

N = length(nomFicS7k);
flagProcess = true(1, N);
for k=1:N
    flagFicXmlRaw    = exist(Names(k).nomFicXmlRaw,  'file');
    flagFicXmlComp   = exist(Names(k).nomFicXmlComp, 'file');
    [nomDir, nomFic] = fileparts(Names(k).nomFicXmlRaw);
    flagDirXmlRaw    = exist(fullfile(nomDir, nomFic), 'file');
    [nomDir, nomFic] = fileparts(Names(k).nomFicXmlComp);
    flagDirXmlComp   = exist(fullfile(nomDir, nomFic), 'file');
    if flagFicXmlRaw && flagFicXmlComp && flagDirXmlRaw && flagDirXmlComp
        if skipAlreadyProcessedFiles
            str1 = sprintf('Le fichier "%s" a d�j� �t� trait�. On le bypasse.', nomFicS7k{k});
            str2 = sprintf('File "%s" has been processed previously. We skip it.', nomFicS7k{k});
            my_warndlg(Lang(str1,str2), 0, 'Tag', 'skipAlreadyProcessedFiles');
        end
        flagProcess(k) = ~skipAlreadyProcessedFiles;
    end
end
nomFicS7k = nomFicS7k(flagProcess);
N = length(nomFicS7k);

%% Question limites de la matrice3D

statusDepthAndAcrossDistLimits = [];

%% Question // tbx au cas o� les .s7k auraient d�j� �t� convertis

if StepDisplay == 0
    useParallelHere = useParallel;
else
    useParallelHere = 0;
end

%% Fichiers suivants

str1 = 'Reson Water Column : Traitement des fichiers en cours.';
str2 = 'Reson Water Column : Processing files.';
if useParallelHere && (N > 1)
    [~, Carto] = prepare_lectures7k_step1(nomFicS7k{1}, 'Carto', Carto);
    [hw, DQ] = create_parforWaitbar(N, Lang(str1,str2));
    parfor (k=1:N, useParallelHere)
        process_ResonS7k_EchogramsGlobeFlyTexture_unitaire(nomFicS7k{k}, Carto, nomFicNav, nomFicIP, TypeWCOutput, Names(k), ...
            StepCompute, StepDisplay, OrigineWC, ChoixNatDecibel, CLimRaw, CLimComp, TagName, MaskBeyondDetection, ...
            subBeams, subPings, XLim, YLim, typeAlgoWC, MaxImageSize, statusDepthAndAcrossDistLimits, 'isUsingParfor', true);
        send(DQ, k);
    end
    my_close(hw, 'MsgEnd', 'TimeDelay', 60)
else
    hw = create_waitbar(Lang(str1,str2), 'N', N);
    for k=1:N
        my_waitbar(k, N, hw)
        Carto = process_ResonS7k_EchogramsGlobeFlyTexture_unitaire(nomFicS7k{k}, Carto, nomFicNav, nomFicIP, TypeWCOutput, Names(k), ...
            StepCompute, StepDisplay, OrigineWC, ChoixNatDecibel, CLimRaw, CLimComp, TagName, MaskBeyondDetection, ...
            subBeams, subPings, XLim, YLim, typeAlgoWC, MaxImageSize, statusDepthAndAcrossDistLimits);
    end
    my_close(hw, 'MsgEnd', 'TimeDelay', 60)
end


function Carto = process_ResonS7k_EchogramsGlobeFlyTexture_unitaire(nomFicS7k, Carto, nomFicNav, nomFicIP, TypeWCOutput, Names, ...
    StepCompute, StepDisplay, OrigineWC, ChoixNatDecibel, CLimRaw, CLimComp, TagName, MaskBeyondDetection, ...
    subBeams, subPings, XLim, YLim, typeAlgoWC, MaxImageSize, statusDepthAndAcrossDistLimits, varargin)

global isUsingParfor %#ok<GVMIS>

[varargin, isUsingParfor] = getPropertyValue(varargin, 'isUsingParfor', isUsingParfor); %#ok<ASGLU>

I0 = cl_image_I0;

a = cl_reson_s7k('nomFic', nomFicS7k); % , 'KeepNcID', true
if isempty(a)
    return
end
    
[flag, Carto] = sonar_ResonProcessWC_GlobeFlyTexture(I0, nomFicS7k, 'TypeWCOutput', TypeWCOutput, ...
    'Names', Names, 'NoInteractivity', 'Carto', Carto, ...
    'StepCompute', StepCompute, 'StepDisplay', StepDisplay, 'OrigineWC', OrigineWC, ...
    'nomFicNav', nomFicNav, 'nomFicIP', nomFicIP, ...
    'ChoixNatDecibel', ChoixNatDecibel, 'CLimRaw', CLimRaw, 'CLimComp', CLimComp, 'subBeams', subBeams, 'subPings', subPings, ...
    'XLim', XLim, 'YLim', YLim, 'TagName', TagName, 'MaskBeyondDetection', MaskBeyondDetection, ...
    'typeAlgoWC', typeAlgoWC, 'MaxImageSize', MaxImageSize, 'statusDepthAndAcrossDistLimits', statusDepthAndAcrossDistLimits); %#ok<ASGLU>
