function flag = process_ResonS7k_EchogramsPhase(nomFicS7k, nomFicNav, nomFicIP, TypeWCOutput, Names, ...
    StepCompute, StepDisplay, CLim, TagName, MaskBeyondDetection, varargin)

global isUsingParfor %#ok<GVMIS>
isUsingParfor = 0;

[varargin, subBeams]     = getPropertyValue(varargin, 'subBeams',     []);
[varargin, subPings]     = getPropertyValue(varargin, 'subPings',     []);
[varargin, MaxImageSize] = getPropertyValue(varargin, 'MaxImageSize', 1000);
[varargin, XLim]         = getPropertyValue(varargin, 'XLim',         []);
[varargin, YLim]         = getPropertyValue(varargin, 'YLim',         []);
[varargin, Carto]        = getPropertyValue(varargin, 'Carto',        []);
[varargin, skipAlreadyProcessedFiles]      = getPropertyValue(varargin, 'skipAlreadyProcessedFiles',      []);
[varargin, statusDepthAndAcrossDistLimits] = getPropertyValue(varargin, 'statusDepthAndAcrossDistLimits', []); %#ok<ASGLU>


useParallel = get_UseParallelTbx;

if ~iscell(nomFicS7k)
    nomFicS7k = {nomFicS7k};
end

%% V�rification si des fichiers ont d�j� �t� trait�s

N = length(nomFicS7k);
flagProcess = true(1, N);
for k=1:N
    flagFicXmlPhase  = exist(Names(k).nomFicXmlPhase, 'file');
    [nomDir, nomFic] = fileparts(Names(k).nomFicXmlPhase);
    flagDirXmlPhase  = exist(fullfile(nomDir, nomFic), 'file');
    if flagFicXmlPhase && flagDirXmlPhase
        if skipAlreadyProcessedFiles
            str1 = sprintf('Le fichier "%s" a d�j� �t� trait�. On le bypasse.', nomFicS7k{k});
            str2 = sprintf('File "%s" has been processed previously. We skip it.', nomFicS7k{k});
            my_warndlg(Lang(str1,str2), 0, 'Tag', 'skipAlreadyProcessedFiles');
        end
        flagProcess(k) = ~skipAlreadyProcessedFiles;
    end
end
nomFicS7k = nomFicS7k(flagProcess);

%% Traitement des fichiers

N = length(nomFicS7k);

%% Question // tbx au cas o� les .all auraient d�j� �t� convertis

if StepDisplay == 0
    useParallelHere = useParallel;
else
    useParallelHere = 0;
end

%% Action

str1 = 'Calcul de l''�cho-int�gration de la colonne d''eau des sondeurs Reson en cours.';
str2 = 'Processing echo integration of WCD for Reson MBES.';
if useParallelHere && (N > 2)
    [hw, DQ] = create_parforWaitbar(N, Lang(str1,str2));
    parfor (k=1:N, useParallelHere)
        process_ResonS7k_EchogramsPhase_unitaire(nomFicS7k{k}, Names(k), TypeWCOutput, Carto, MaxImageSize, ...
            StepCompute, StepDisplay, nomFicNav, nomFicIP, CLim, subBeams, subPings, XLim, YLim, TagName, MaskBeyondDetection, ...
            statusDepthAndAcrossDistLimits, 'isUsingParfor', true);
        send(DQ, k);
    end
    my_close(hw, 'MsgEnd')
else
    hw = create_waitbar(Lang(str1,str2), 'N', N);
    for k=1:N
        my_waitbar(k, N, hw)
        [flag, Carto] = process_ResonS7k_EchogramsPhase_unitaire(nomFicS7k{k}, Names(k), TypeWCOutput, Carto, MaxImageSize, ...
            StepCompute, StepDisplay, nomFicNav, nomFicIP, CLim, subBeams, subPings, XLim, YLim, TagName, MaskBeyondDetection, ...
            statusDepthAndAcrossDistLimits);
    end
    my_close(hw, 'MsgEnd')
end


function [flag, Carto] = process_ResonS7k_EchogramsPhase_unitaire(nomFicS7k, Names, TypeWCOutput, Carto, MaxImageSize, ...
    StepCompute, StepDisplay, nomFicNav, nomFicIP, CLim, subBeams, subPings, XLim, YLim, TagName, MaskBeyondDetection, ...
    statusDepthAndAcrossDistLimits, varargin)

global isUsingParfor %#ok<GVMIS>

[varargin, isUsingParfor] = getPropertyValue(varargin, 'isUsingParfor', isUsingParfor); %#ok<ASGLU>

I0 = cl_image_I0;

a = cl_reson_s7k('nomFic', nomFicS7k); % , 'KeepNcID', true
if isempty(a)
    return
end

[flag, Carto] = sonar_ResonProcessWCPhase(I0, nomFicS7k, 'TypeWCOutput', TypeWCOutput, ...
    'Names', Names, 'Carto', Carto, ...
    'StepCompute', StepCompute, 'StepDisplay', StepDisplay, ...
    'nomFicNav', nomFicNav, 'nomFicIP', nomFicIP, ...
    'CLim', CLim, 'subBeams', subBeams, 'subPings', subPings, ...
    'XLim', XLim, 'YLim', YLim, 'TagName', TagName, 'MaskBeyondDetection', MaskBeyondDetection, ...
    'MaxImageSize', MaxImageSize, 'statusDepthAndAcrossDistLimits', statusDepthAndAcrossDistLimits);
