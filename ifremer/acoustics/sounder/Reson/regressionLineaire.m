% x = 1e9 + (2500:2700);
% x = (2500:2700);
% x = (2500:2700) - 2500;
% phase = (pi * x + 10) + 100*(rand(size(x))-0.5);
% [a, b, x0, Eqm] = regressionLineaire(x, phase)
% figure; plot(x, phase, '*'); hold on; plot(x, a*x+b, 'k'); grid on

function [a, b, x0, Eqm] = regressionLineaire(x, phase)

subNaN = ~isnan(x) & ~isnan(phase);
x     = x(subNaN);
phase = phase(subNaN);

xd = x(1);
deltax = x(end) - x(1);
if deltax == 0
    a   = NaN;
    b   = NaN;
    x0  = NaN;
    Eqm = NaN;
else
    Alpha = 1 / deltax;
    xPrime = Alpha * (x - xd);

    [A, B, X0, Eqm] = calcul(xPrime, phase);

    a  = Alpha * A;
    b  = B - a * xd;
    x0 = X0 / Alpha + xd;
end


function [A, B, X0, Eqm] = calcul(x, phase)

SumW  = length(x);
SumX  = sum(x);
SumX2 = sum(x.^2);
SumXY = sum(x .* phase);
SumY  = sum(phase);
delta = SumW * SumX2 - SumX * SumX;

if delta ~= 0 %&& (denom > 0)
    %* Calculate slope, intercept and correlation of linear fit */
    B = (SumX2 * SumY - SumX * SumXY) / delta;
    A = (SumW * SumXY - SumX * SumY) / delta;
    
    X0 = -(SumX2 * SumY - SumX * SumXY) / (SumW * SumXY - SumX * SumY);
    
    Eqm = sqrt(sum((phase - (A*x+B)).^2)  / SumW);
    
%     figure; plot(x, phase, '*'); hold on; plot(x, A*x+B, 'k'); grid on
%     figure; plot(x, (phase - (A*x+B)), '*'); grid on
else
    A   = NaN;
    B   = NaN;
    Eqm = NaN;
    X0  = NaN;
end
