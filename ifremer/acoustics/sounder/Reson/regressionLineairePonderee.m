% x = 1e9 + (2500:2700);
% x = (2500:2700);
% x = (2500:2700) - 2500;
% w = rand(size(x));
% % w(:) = 1;
% phase = (pi * x + 10) + 100*(rand(size(x))-0.5);
% [a, b, x0, Eqm] = regressionLineairePonderee(x, phase, w)
% figure; plot(x, phase, '*'); hold on; plot(x, a*x+b, 'k'); grid on

function [a, b, x0, Eqm] = regressionLineairePonderee(x, phase, w)

subNaN = ~isnan(x) & ~isnan(phase) & ~isnan(w);
x     = x(subNaN);
phase = phase(subNaN);
w     = w(subNaN);

w = w / max(w);

xd = x(1);
deltax = x(end) - x(1);
if deltax == 0
    a   = NaN;
    b   = NaN;
    x0  = NaN;
    Eqm = NaN;
else
    Alpha = 1 / deltax;
    xPrime = Alpha * (x - xd);

    [A, B, X0, Eqm] = calcul(xPrime, phase, w);

    %{
    % Ajout JMA le 02/09/2014 pour test
    % Ceci pourrait �tre tent� avec un if d'une variable globale activable en temps r�el par ctrl-F12 + menu
    [A, B, X0, Eqm, sub1, sub2] = calcul(xPrime, phase, w);
    if ~isempty(sub1) || ~isempty(sub2)
        phase(sub1) = phase(sub1) - 360;
        phase(sub2) = phase(sub2) + 360;
        [A, B, X0, Eqm, sub1, sub2] = calcul(xPrime, phase, w);
    end
    % Fin jout JMA le 02/09/2014 pour test
    %}
    
    a  = Alpha * A;
    b  = B - a * xd;
    x0 = X0 / Alpha + xd;
end


function [A, B, X0, Eqm] = calcul(x, phase, w)
% function [A, B, X0, Eqm, sub1, sub2] = calcul(x, phase, w)
% sub1, sub2 Ajout JMA le 02/09/2014 pour test

SumW  = sum(w);
SumX  = sum(w .* x);
SumX2 = sum(w .* x.^2);
SumXY = sum(w .* x .* phase);
SumY  = sum(w .* phase);
% SumY2 = sum(w .* phase.^2);
delta = SumW * SumX2 - SumX * SumX;
% denom = delta * (SumW * SumY2 - SumY * SumY);

if delta ~= 0 %&& (denom > 0)
    %* Calculate slope, intercept and correlation of linear fit */
    B = (SumX2 * SumY - SumX * SumXY) / delta;
    A = (SumW * SumXY - SumX * SumY) / delta;
    
%     X0 = -B/A;
    X0 = -(SumX2 * SumY - SumX * SumXY) / (SumW * SumXY - SumX * SumY);
    
    Eqm = sqrt(sum(w .* (phase - (A*x+B)).^2)  / SumW);
    
    %{
    % Ajout JMA le 02/09/2014 pour test
    difPhase = phase - (A*x+B);
%     figure; plot(x, phase, '*'); hold on; plot(x, A*x+B, 'k'); grid on
%     figure; plot(x, difPhase, '*'); grid on
    sub1 = find(difPhase > 180);
    sub2 = find(difPhase < -180);
    % Fin ajout JMA le 02/09/2014
    %}
else
    A   = NaN;
    B   = NaN;
    Eqm = NaN;
    X0  = NaN;
    %{
    % Ajout JMA le 02/09/2014 pour test
    sub1 = [];
    sub2 = [];
    % Fin ajout JMA le 02/09/2014
    %}
end
