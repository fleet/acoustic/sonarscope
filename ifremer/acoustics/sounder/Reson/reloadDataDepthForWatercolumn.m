function [flag, sDataDepth] = reloadDataDepthForWatercolumn(this)

% Modifs JMA le 25/02/2021 sans test

identSondeur = selectionSondeur(this);

[flag, sDataDepth] = read_depth_s7k(this, 'identSondeur', identSondeur);
if ~flag || isempty(sDataDepth) || isempty(sDataDepth.PingCounter)
%     str1 = sprintf('Il n''y a pas de datagramme "7006" dans le fichier %s.s7k : on arr�te sa lecture.', NomImage);
%     str2 = sprintf('There is no datagram "7006" in file %s.s7k : we stop the import.', NomImage);
%     my_warndlg(Lang(str1,str2), 0, 'Tag', 'ResonPasDe70006');
% else
    str1 = sprintf('Le fichier "%s" ne contient pas de bathym�trie.', get(this, 'nomFic'));
    str2 = sprintf('File "%s" does not contain any depth datagram.', get(this, 'nomFic'));
    my_warndlg(Lang(str1,str2), 0);
    return
end

nbPings = length(sDataDepth.PingCounter);
if nbPings < 2
    str1 = sprintf('Le fichier "%s" ne contient pas suffisamment de pings.', get(this, 'nomFic'));
    str2 = sprintf('File "%s" does not contain enough depth datagram.', get(this, 'nomFic'));
    my_warndlg(Lang(str1,str2), 0);
    return
end

flag = 1;
