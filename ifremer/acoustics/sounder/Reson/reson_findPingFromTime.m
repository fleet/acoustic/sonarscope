function [flag, iPing2] = reson_findPingFromTime(Data1, iPing1, Data2)

flag   = 0;
iPing2 = [];

if isempty(Data1) || isempty(Data2) || isempty(iPing1)
    return
end

if isa(Data1.Time, 'cl_time')
    t1 = Data1.Time.timeMat;
    cl_time('timeMat', t1(iPing1))
else
    t1 = Data1.Time;
end

if isa(Data2.Time, 'cl_time')
    t2 = Data2.Time.timeMat;
    cl_time('timeMat', t2)
else
    t2 = Data2.Time;
end
%     iPingWC = find(Data.PingCounter == Data1.PingCounter(iPing1));

% iPing2 = find(Data2.PingCounter == Data1.PingCounter(iPing1));

diff = abs(t2 - t1(iPing1));
%     figure; plot(t1, '*'); grid
%     figure; plot(t2, '*'); grid
%     figure; plot(t1-t2, '*'); grid
%     figure; plot(diff, '*'); grid
[minTrouve, iPing2] = min(diff);

flag = 1;

