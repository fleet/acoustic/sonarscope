function MultiPingSequence = reversePingSequence(MultiPingSequence)

return

%{
if MultiPingSequence ~= 0
    MultiPingSequence = 5 - MultiPingSequence;
end
%}

% Essai r�alis� le 30/05/2018 pour essayer de mettre les pings dans in
% ordre plus lin�aire dans le sens d'avance du bateau. Pour la bathy, �a a
% l'air de fonctionner si pn commente les instructions ci-dessous dans
% la fonction read_depth_s7k.
% Pour les donn�es de WC, cela n'a rien chang� donc on revient en arri�re

%{
% I:\MarieClaireFabri\Cassidaigne\ESSROV2010_RESON7150\20100911_231044_PP-7150-24kHz-.s7k
[~, subOrdre] = sort(subDepth);
subDepth = subDepth(subOrdre);
sub7000  = sub7000(subOrdre);
% Fin ajout JMA le 28/10/2015
%}
