%% BDA sur un ping

function [PingRange, TD, QualityFactor, AmpPingRange, PhasePingRange, AmpPingQF, PhasePingQF, ...
    AmpPingSamplesNb, AmpPingQF2, PhasePingNbSamples, SampleRate, BeamAngles, iBeamBeg, iBeamEnd, SystemSerialNumber, ...
    AmpPingNormHorzMax, PhasePingPente, PhasePingEqm, DepthPingPrev, R0, RMax1, RMax2, R1SamplesFiltre, iBeamMax0, MaskWidth] = ...
    sonar_BDA(...
    AmpImage, PhaseImage, DepthMinPing, DepthMaxPing, BeamAngles, ...
    Frequency, SampleRate, TxPulseWidth, ReceiveBeamWidth, SystemSerialNumber, ...
    IdentAlgo, DisplayLevel, iPing, PingSequence, BeamsToDisplay, ...
    ProjectionBeam_3dB_WidthVertical, DepthPingPrev, varargin)

[varargin, EmModel] = getPropertyValue(varargin, 'EmModel', []); %#ok<ASGLU>

IdentAlgo1 = 1 + mod(IdentAlgo-1, 3);
IdentAlgo2 = 1 + (IdentAlgo - IdentAlgo1)/3;

if (IdentAlgo2 == 2) % Cas o� on h�rite de la d�tection du ping pr�c�dent pour ajuster la "Depth Gate"
    if ~isempty(DepthPingPrev)
        DepthMinPing = max(DepthMinPing, min(DepthPingPrev) * 0.8);
        DepthMaxPing = min(DepthMaxPing, max(DepthPingPrev) * 1.1);
    end
end
    
[PingRange, TD, QualityFactor, AmpPingRange, PhasePingRange, AmpPingQF, PhasePingQF, ...
    AmpPingSamplesNb, AmpPingQF2, PhasePingNbSamples, SampleRate, BeamAngles, iBeamBeg, iBeamEnd, SystemSerialNumber, ...
    AmpPingNormHorzMax, PhasePingPente, PhasePingEqm, R0, RMax1, RMax2, R1SamplesFiltre, iBeamMax0, MaskWidth] = ...
    sonar_BDA_ping(...
    AmpImage, PhaseImage, DepthMinPing, DepthMaxPing, BeamAngles, ...
    Frequency, SampleRate, TxPulseWidth, ReceiveBeamWidth, SystemSerialNumber, ...
    IdentAlgo1, DisplayLevel, iPing, PingSequence, BeamsToDisplay, ...
    ProjectionBeam_3dB_WidthVertical, EmModel);
DepthPingPrev = PingRange .* cosd(BeamAngles) / (2 * (SampleRate / 1500));

if (IdentAlgo2 == 3) % Cas o� on relance une deuxi�me fois l'algorithme pour optimiser la "Depth Gate".
    DepthPing = PingRange .* cosd(BeamAngles) / (2 * (SampleRate / 1500));
    % figure; plot(DepthPing); grid on;
    DepthMinPing = min(DepthPing) * 0.8;
    DepthMaxPing = max(DepthPing) * 1.1;
    [PingRange, TD, QualityFactor, AmpPingRange, PhasePingRange, AmpPingQF, PhasePingQF, ...
        AmpPingSamplesNb, AmpPingQF2, PhasePingNbSamples, SampleRate, BeamAngles, iBeamBeg, iBeamEnd, SystemSerialNumber, ...
        AmpPingNormHorzMax, PhasePingPente, PhasePingEqm, R0, RMax1, RMax2, R1SamplesFiltre, iBeamMax0, MaskWidth] = ...
        sonar_BDA_ping(...
        AmpImage, PhaseImage, DepthMinPing, DepthMaxPing, BeamAngles, ...
        Frequency, SampleRate, TxPulseWidth, ReceiveBeamWidth, SystemSerialNumber, ...
        IdentAlgo1, DisplayLevel, iPing, PingSequence, BeamsToDisplay, ...
        ProjectionBeam_3dB_WidthVertical, EmModel);
end
    

function [PingRange, TD, QualityFactor, AmpPingRange, PhasePingRange, AmpPingQF, PhasePingQF, ...
    AmpPingSamplesNb, AmpPingQF2, PhasePingNbSamples, SampleRate, BeamAngles, iBeamBeg, iBeamEnd, SystemSerialNumber, ...
    AmpPingNormHorzMax, PhasePingPente, PhasePingEqm, R0, RMax1, RMax2, R1SamplesFiltre, iBeamMax0, MaskWidth] = ...
    sonar_BDA_ping(...
    AmpImage, PhaseImage, DepthMinPing, DepthMaxPing, BeamAngles, ...
    Frequency, SampleRate, TxPulseWidth, ReceiveBeamWidth, SystemSerialNumber, ...
    IdentAlgo, DisplayLevel, iPing, PingSequence, BeamsToDisplay, ...
    ProjectionBeam_3dB_WidthVertical, EmModel)

global TestPlatformCmatlab %#ok<GVMIS>

%% Normalization by median value % Reintroduit le 07/07/2010 pour essai

if IdentAlgo == 3
    if any(TestPlatformCmatlab.BDA_compensAmpByMHorzMedian == [1 3 4])
        AmpImage_M = BDA_compensAmpByMHorzMedian(AmpImage);
    end
    
    if any(TestPlatformCmatlab.BDA_compensAmpByMHorzMedian == [2 3 4])
        AmpImage_C = BDA_compensAmpByMHorzMedian_reson(AmpImage);
    end
    
    if any(TestPlatformCmatlab.BDA_compensAmpByMHorzMedian == [3 4])
        compareSonar_BDA_images('BDA_compensAmpByMHorzMedian', 'AmpImage',  AmpImage_M,  AmpImage_C,  0, iPing)
    end
    
    switch TestPlatformCmatlab.BDA_compensAmpByMHorzMedian
        case {1; 3}
            AmpImage = AmpImage_M;
        case {2; 4}
            AmpImage = AmpImage_C;
    end
end

%% D�termination du masque sur matrice r�duite

[R0, RMax1, RMax2, R1SamplesFiltre, iBeamMax0, AmpPingNormHorzMax, MaskWidth, iBeamBeg, iBeamEnd] = ...
    BDA_detectionAmplitudeOnReducedMatrix(...
    AmpImage, BeamAngles, Frequency, ...
    DepthMinPing, DepthMaxPing, ...
    TxPulseWidth, SampleRate, ReceiveBeamWidth, PingSequence, SystemSerialNumber, ProjectionBeam_3dB_WidthVertical, ...
    DisplayLevel, iPing, IdentAlgo);

% TODO 
if isnan(R0)
    % TODO : return. Il serait bon de fortir avec un flag et d'�viter de
    % faire des tas de calculs qui ne servent � rien.
end

SampleRate         = SampleRate(1); % TODO SampleRate(1)
SystemSerialNumber = SystemSerialNumber(1); % TODO

Frequency = Frequency(1);

%% Reson_phaseFilter

RMax1Reson = RMax1;
RMax1Reson(isnan(RMax1Reson)) = 0;

if any(TestPlatformCmatlab.Reson_phaseFilter == [1 3 4])
    [PhaseImage_M, nbSamplesMoyennes_M] = Reson_phaseFilter2(PhaseImage, AmpImage, RMax1, MaskWidth); % Fonctions Matlab
    %     [PhaseImage_M, nbSamplesMoyennes] = Reson_phaseFilter(PhaseImage, AmpImage, RMax1, MaskWidth); % Reecriture similaire � du C
    %{
    [PhaseImage_M1, nbSamplesMoyennes_M1] = Reson_phaseFilter(PhaseImage, AmpImage, RMax1, MaskWidth); % Fonctions Matlab
    compareSonar_BDA_images('Reson_phaseFilter', 'PhaseImage Matlab/Matlab', PhaseImage_M1, PhaseImage_M, 0, iPing)
    compareSonar_BDA_images('Reson_phaseFilter', 'PhaseImage Matlab/Matlab', sind(PhaseImage_M1), sind(PhaseImage_M), 0, iPing)
    SonarScope(sind(PhaseImage_M1) - sind(PhaseImage_M))
    %}
end


if any(TestPlatformCmatlab.Reson_phaseFilter == [2 3 4])
    if isempty(EmModel)
        EmModel = 'Reson';
    end
    switch EmModel
        case {2040; 2045; 302; 304; 710; 712; 714; 122; 124}
            [PhaseImage_C, nbSamplesMoyennes_C] = Reson_phaseFilter2(PhaseImage, AmpImage, RMax1, MaskWidth); % Fonctions Matlab
        otherwise
            % Version BDA impl�ment�e dans les sondeurs Reson
            [PhaseImage_C, nbSamplesMoyennes_C] = Reson_phaseFilter_reson(...
                single(PhaseImage), single(AmpImage), ...
                double(RMax1Reson), double(MaskWidth), ...
                double(SampleRate), double(TxPulseWidth));
            %}
            
            % Version modifi�e par JMA le 14/06/2018 pour donn�es Mag&Phase EM2040 NIWA mission QUOI mais �a ne r�soud pas le pb :
            %{
            PI = single(PhaseImage);
            AI = single(AmpImage);
            subNaN = isnan(PI) | isnan(AI);
            PI(subNaN) = 0;
            AI(subNaN) = 1;
            [PhaseImage_C, nbSamplesMoyennes_C] = Reson_phaseFilter_reson(...
                PI, AI, ...
                double(RMax1Reson), double(MaskWidth), ...
                double(SampleRate), double(TxPulseWidth));
            PhaseImage_C(subNaN) = NaN;
            %}
    end
end

if any(TestPlatformCmatlab.Reson_phaseFilter == [3 4])
    compareSonar_BDA_images('Reson_phaseFilter', 'PhaseImage', PhaseImage_M, PhaseImage_C, 0, iPing)
    %   SonarScope(PhaseImage_C - PhaseImage_M)
end

switch TestPlatformCmatlab.Reson_phaseFilter
    case {1; 3}
        PhaseImage = PhaseImage_M;
        nbSamplesMoyennes = nbSamplesMoyennes_M;
    case {2; 4}
        PhaseImage = PhaseImage_C;
        nbSamplesMoyennes = nbSamplesMoyennes_C;
end

% Test pour Yvegeniy 16/11/2008
PhaseImage(isnan(AmpImage)) = NaN;

%% Masquage � partir de l'amplitude

NbSamplesForPulseLength = nbSamplesForPulseLength(SampleRate(1), TxPulseWidth);
MasqueAmp = ResonMasqueAmplitude(SystemSerialNumber, AmpImage, BeamAngles, ...
    R0, RMax1, RMax2, R1SamplesFiltre, ...
    NbSamplesForPulseLength, MaskWidth, DisplayLevel, iPing, IdentAlgo);
AmpImage(MasqueAmp == 0) = NaN;
PhaseImage(MasqueAmp == 0) = NaN;

%% D�tection de l'amplitude

[AmpPingRange, AmpPingQF, AmpPingSamplesNb, AmpPingQF2] = ...
    ResonDetectionAmplitudeImage(AmpImage, R0, BeamAngles, ...
    SystemSerialNumber, SampleRate, TxPulseWidth, DisplayLevel, BeamsToDisplay, iPing, 'IdentAlgo', IdentAlgo);

%% D�roulement de la phase

% if SystemSerialNumber ~= 7111
%     Ph = ResonUnwrappingPhase_Horz(PhaseImage, iBeamMax0, RMax1);
%     PhaseImage = Ph;
% end

%% Masquage � partir de la phase

if any(TestPlatformCmatlab.ResonMasquePhase_V5 == [1 3 4])
    [MasquePhase_M, PhaseImage_M] = ResonMasquePhase_V5(PhaseImage, AmpImage, R0, iBeamMax0, RMax1,  DisplayLevel);
end

if any(TestPlatformCmatlab.ResonMasquePhase_V5 == [2 3 4])
    switch EmModel
        case {2040; 2045; 302; 304; 710; 712; 714; 122; 124}
            [MasquePhase_C, PhaseImage_C] = ResonMasquePhase_V5(PhaseImage, AmpImage, R0, iBeamMax0, RMax1,  DisplayLevel);
        otherwise
            [MasquePhase_C, PhaseImage_C] = ResonMasquePhase_V5_reson(...
                single(PhaseImage), single(AmpImage), ...
                double(R0), double(iBeamMax0), double(RMax1), double(MaskWidth), double(TxPulseWidth), double(SampleRate));
    end
end

if any(TestPlatformCmatlab.ResonMasquePhase_V5 == [3 4])
    %     compareSonar_BDA_images('ResonMasquePhase_V5', 'PhaseImageOrigin/M',  PhaseImage,  PhaseImage_M,  0, iPing)
    %     compareSonar_BDA_images('ResonMasquePhase_V5', 'PhaseImageOrigin/C',  PhaseImage,  PhaseImage_C,  0, iPing)
    compareSonar_BDA_images('ResonMasquePhase_V5', 'PhaseImage',  PhaseImage_M,  PhaseImage_C,  0, iPing)
    compareSonar_BDA_images('ResonMasquePhase_V5', 'MasquePhase', MasquePhase_M, MasquePhase_C, 0, iPing)
end

switch TestPlatformCmatlab.ResonMasquePhase_V5
    case {1; 3}
        MasquePhase = MasquePhase_M;
        PhaseImage  = PhaseImage_M;
    case {2; 4}
        MasquePhase = MasquePhase_C;
        PhaseImage  = PhaseImage_C;
end

%{
if TestPlatformCmatlab.ResonMasquePhase_V5 == 3
    PhaseImagewithNanC = PhaseImage;
    PhaseImagewithNanC(isnan(PhaseImagewithNanC)) = 9999;
    PhaseImageReson = ResonMasquePhase_V5_reson(PhaseImagewithNanC, AmpImage, R0, iBeamMax0, RMax1, MaskWidth, TxPulseWidth, SampleRate);
end

[MasquePhase, PhaseImage] = ResonMasquePhase_V5(PhaseImage, AmpImage, R0, iBeamMax0, RMax1,  DisplayLevel);

if TestPlatformCmatlab.ResonMasquePhase_V5 == 3
    if max(abs(PhaseImageReson(:) - PhaseImage(:))) > 0
%{
        figure;
        title('Ecart Masque _phase_mask  ResonMasquePhase_V5');
        imagesc(MasqueReson - Masque, [-180 180]);
        colorbar;
%}
        figure;
        title('Ecart Masque _phase_mask  ResonMasquePhase_V5');
        plot(PhaseImage(:,1),'b');
        figure;
        plot(PhaseImageReson(:,1),'r');
    end
end
%}

PhaseImage(MasquePhase == 0) = NaN;

%% D�tection � partir de la phase

[PhasePingRange, PhasePingNbSamples, PhasePingPente, PhasePingEqm, PhasePingQF] = ...
    ResonDetectionPhaseImage_3steps(AmpImage, PhaseImage, BeamAngles, R0, SampleRate, ...
    Frequency, nbSamplesMoyennes, SystemSerialNumber, iPing, TxPulseWidth, ...
    DisplayLevel, BeamsToDisplay);

%% Synth�se

if any(TestPlatformCmatlab.ResonSyntheseBottomDetector == [1 3 4])
    [PingRange_M, TD_M, QualityFactor_M] = ResonSyntheseBottomDetector(AmpPingRange, AmpPingQF, ...
        PhasePingRange, PhasePingQF, PhasePingPente, PhasePingEqm, iBeamBeg, iBeamEnd);
end

if any(TestPlatformCmatlab.ResonSyntheseBottomDetector == [2 3 4]) % TODO [PingRange_C, TD_C, QualityFactor_C]
    [PingRange_C, TD_C, QualityFactor_C] = ResonSyntheseBottomDetector_reson(...
        double(AmpPingRange), double(AmpPingQF), double(PhasePingRange), double(PhasePingQF), ...
        double(PhasePingPente), double(PhasePingEqm), double(SampleRate), double(TxPulseWidth));
end

if any(TestPlatformCmatlab.ResonSyntheseBottomDetector == [3 4])
    BDA_compare_Matlab_C('ResonSyntheseBottomDetector', 'PingRange',     PingRange_M,     PingRange_C,     iPing, [], TestPlatformCmatlab.Seuil_ResonSyntheseBottomDetector_PingRange);
    BDA_compare_Matlab_C('ResonSyntheseBottomDetector', 'TD',            TD_M,            TD_C,            iPing, [], TestPlatformCmatlab.Seuil_ResonSyntheseBottomDetector_TD);
    BDA_compare_Matlab_C('ResonSyntheseBottomDetector', 'QualityFactor', QualityFactor_M, QualityFactor_C, iPing, [], TestPlatformCmatlab.Seuil_ResonSyntheseBottomDetector_QualityFactor);
end

switch TestPlatformCmatlab.ResonSyntheseBottomDetector
    case {1; 3}
        PingRange     = PingRange_M;
        TD            = TD_M;
        QualityFactor = QualityFactor_M;
    case {2; 4}
        PingRange     = PingRange_C;
        TD            = TD_C;
        QualityFactor = QualityFactor_C;
end


% TODO : faire dans le m�me stlyle que BDA_compare_Matlab_C
function compareSonar_BDA_images(nomFct, NomVar, valM, valC, seuil, iPing)
if ~isequal(valM, valC) || (max(abs(valM(:)-valC(:))) > seuil) % ceinture et bretelles pour traiter les NaN
    str = sprintf('%s : variable %s \ncalled in sonar_BDA', nomFct, NomVar);
    figure('Name', [' BDA : divergence C / Matlab Ping ' num2str(iPing)]);
    h(1) = subplot(1,3,1); imagesc(valM); colorbar
    title(str, 'Interpreter', 'None')
    h(2) = subplot(1,3,2); imagesc(valC); colorbar
    title('C')
    if strcmp(NomVar, 'PhaseImage')
        X = mod((valM-valC) + (360+180), 360);
        X = X - 180;
        h(3) = subplot(1,3,3); imagesc(X, [-25 25]); colorbar
    else
        h(3) = subplot(1,3,3); imagesc(valM-valC); colorbar
    end
    title('M-C')
    linkaxes(h, 'xy')
    %     SonarScope(valM-valC)
    %     SonarScope(cosd(valM-valC))
end
