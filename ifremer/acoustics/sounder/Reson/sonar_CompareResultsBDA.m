function flag = sonar_CompareResultsBDA(nomDirMatlab, nomDirC)

flag = 0;

I0 = cl_image_I0;

listeDirM = listeDirOnDir(nomDirMatlab);

N = length(listeDirM);
str1 = 'Traitement en cours';
str2 = 'Processing';
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k1=1:N
    my_waitbar(k1, N, hw)
    str1 = sprintf('Comparaison du répertoire %d/%d : "%s"', k1, N, listeDirM(k1).name);
    str2 = sprintf('Comparison of %d/%d : "%s" images', k1, N, listeDirM(k1).name);
    fprintf('\n%s\n', Lang(str1,str2));
    
    nomDir2 = fullfile(nomDirC, listeDirM(k1).name);
    if ~exist(nomDir2, 'dir')
        str1 = sprintf('Le répertoire "%s" n''existe pas.', nomDir2);
        str2 = sprintf('Directory "%s" does not exist.', nomDir2);
        my_warndlg(Lang(str1,str2), 0);
        continue
    end
    nomDir1 = fullfile(nomDirMatlab, listeDirM(k1).name);
    listeFicErs1 = listeFicOnDir(nomDir1, '.ers');
    listeFicErs2 = listeFicOnDir(nomDir2, '.ers');
    
    for k2=1:length(listeFicErs1)
        nomFic1 = listeFicErs1{k2};
        nomFic2 = listeFicErs2{k2};
        
        [~, nomImage, ext] = fileparts(nomFic1);
        nomImage = fullfile(nomImage, ext);
        
        [flag, a1] = cl_image.import_ermapper(nomFic1);
        if ~flag
            messageErreurFichier(nomFic1, 'ReadFailure');
        end
        
        [flag, a2] = cl_image.import_ermapper(nomFic2);
        if ~flag
            messageErreurFichier(nomFic1, 'ReadFailure');
        end
        
        if a1 == a2
            str1 = sprintf('OK pour "%s"', nomImage);
            str2 = sprintf('OK for "%s"', nomImage);
            fprintf('    %s\n', Lang(str1,str2));
        else
            str1 = sprintf('Différent pour "%s"', nomImage);
            str2 = sprintf('Different for "%s"', nomImage);
            fprintf('    %s\n', Lang(str1,str2));
            
            T1 = a1.Name;
            T2 = a2.Name;
            a1.Name = [T1 '_Matlab'];
            a2.Name = [T2 'C'];
            
            a2.TagSynchroX = a1.TagSynchroX;
            
            ImageName = a1.Name;
            if contains(ImageName, '_Amp') || contains(ImageName, '_Phase')
                continue
            end
            
            switch a1.DataType
                case cl_image.indDataType('Bathymetry')
                    DiffRelative = abs((a1-a2) / a1) * 100;
                    b1 = isnan(a1);
                    b2 = isnan(a2);
                    c = inherit(a1, b2-b1, 'AppendName', 'DiffIsnan');
                    SonarScope([a1 a2 a1-a2 DiffRelative c])
                    
                case cl_image.indDataType('MbesQualityFactor')
                    SonarScope([a1 a2 abs(a1-a2)])
                    
                case cl_image.indDataType('DetectionType')
                    Diff = a1-a2;
                    Diff.CLim = [-2 2];
                    SonarScope([a1 a2 Diff])
            end
        end
    end
end
my_close(hw, 'MsgEnd')