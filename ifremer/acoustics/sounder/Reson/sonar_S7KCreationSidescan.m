function [flag, a] = sonar_S7KCreationSidescan(nomFic, nomDirOut, varargin)

flag  = 0;
Carto = [];

if ischar(nomFic)
    nomFic = {nomFic};
end

for k=1:length(nomFic)
    [flag, a(k), Carto] = sonar_S7KCreationSidescan_unitaire(nomFic{k}, Carto); %#ok<AGROW>
    if ~flag
        continue
    end
    
    %% Save images in ErMapper files
    
    NomImage = a(k).Name;
    nomFicXml = fullfile(nomDirOut, [NomImage '.xml']);
    flag = export_xml(a(k), nomFicXml);
    
%     nomFicErs = fullfile(nomDirOut, [NomImage '.ers']);
%     flag = export_ermapper(a(k), nomFicErs);
    
    %% Mise en memmapfile
    
    a(k) = optimiseMemory(a(k), 'SizeMax', 0); %#ok<AGROW>

end


function [flag, that, Carto] = sonar_S7KCreationSidescan_unitaire(nomFic, Carto)

flag = 0;
that = [];

s7k = cl_reson_s7k('nomFic', nomFic, 'KeepNcID', true);
identSondeur = selectionSondeur(s7k);

DataNAV      = read_navigation(s7k, identSondeur);
DataAttitude = read_attitude(s7k, identSondeur);
Data7030     = read_sonarInstallationParameters(s7k, identSondeur);

Data = read_BeamRawData(s7k, identSondeur, 'subPing', 'None');
if isempty(Data)
    return
end
N = length(Data.PingCounter);
if N == 0
    return
end
subPing = 1:N;

Time   = NaN(N, 1);
Latitude  = NaN(N, 1);     
Longitude = NaN(N, 1);     
Height             = NaN(N, 1, 'single');
TransducerDepth   = NaN(N, 1, 'single');
SurfaceSoundSpeed = NaN(N, 1, 'single');
SampleRate        = NaN(N, 1, 'single');
Frequency         = NaN(N, 1, 'single');
Roll              = NaN(N, 1, 'single');
Pitch             = NaN(N, 1, 'single');
Heave             = NaN(N, 1, 'single');
Heading           = NaN(N, 1, 'single');
PingCounter       = NaN(N, 1, 'single');

ReflectivityBab  = NaN(N, 1, 'single');
ReflectivityTri  = NaN(N, 1, 'single');

Compteur = [];
str1 = sprintf('D�tection du fond en cours sur\n%s\n', nomFic);
str2 = sprintf('Processing Bottom Detection on\n%s\n', nomFic);
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
    iPing = subPing(k);
    my_waitbar(k, N, hw)

    [Compteur, strTFin] = estimation_temps_traitement(k, N, Compteur);
    fprintf('%5d / %5d : iPing=%d  %s - %s\n', k, N, iPing, nomFic, strTFin);
        
    % Temporaire
%     RMax1_Precedent = [];
    
    [b, Carto] = view_BeamRawData(s7k, 'subPing', iPing, 'Carto', Carto, 'DisplayLevel', 0);
    if isempty(b)
        N = k - 1;
        break
    end
    
    Amp = b(1);
    Sonar = get(Amp, 'Sonar');
    SampleBeamData = Sonar.SampleBeamData;
    
    if k == 1
        nbBeams           = b(1).nbColumns;
        InitialFileName   = b(1).InitialFileName;
        NomImage = extract_ImageName(b(1));
        [~, TagSynchro]   = fileparts(InitialFileName);
        SonarDescription  = get(b(1), 'SonarDescription');
        identSondeur      = get(SonarDescription, 'Sonar.SystemSerialNumber');
        TagSynchroX       = [num2str(identSondeur) '_' TagSynchro '_' num2str(rand(1))];
        TagSynchroY       = TagSynchroX;
        InitialFileFormat = b(1).InitialFileFormat;
        SonarDescription  = get(b(1), 'SonarDescription');
%       InitialImageName  = b(1).InitialImageName;
    end
    
    Time(k)   = SampleBeamData.Time.timeMat;
    Height(k) = SampleBeamData.R0;
    
    if isempty(Data7030) || ~isfield(Data7030, 'DRF')
        TransducerDepth(k) = 0;
    else
        TransducerDepth(k) = Data7030.DRF(1).ReceiveArrayZ - Data7030.DRF(1).WaterLineVerticalOffset;
    end

    SurfaceSoundSpeed(k) = SampleBeamData.SoundVelocity;
    SampleRate(k)        = SampleBeamData.SampleRate;
    Frequency(k)         = SampleBeamData.Frequency;
    PingCounter(k)       = SampleBeamData.PingCounter;

    objTime = cl_time('timeMat', SampleBeamData.Time.timeMat);
    if isempty(DataAttitude) || ~isfield(DataAttitude, 'Roll') || isempty(DataAttitude.Roll)
        Roll(k)    = 0;
        Pitch(k)   = 0;
        Heave(k)   = 0;
        Heading(k) = 0;
    else
        Roll(k)    = interp1(DataAttitude.Time, DataAttitude.Roll,    objTime);
        Pitch(k)   = interp1(DataAttitude.Time, DataAttitude.Pitch,   objTime);
        Heave(k)   = interp1(DataAttitude.Time, DataAttitude.Heave,   objTime);
        Heading(k) = interp1(DataAttitude.Time, DataAttitude.Heading, objTime);
    end
    
    if isempty(DataNAV) || ~isfield(DataNAV, 'Latitude') || isempty(DataNAV.Longitude)
        Longitude(k) = 0;
        Latitude(k)  = 0;
    else
        Longitude(k) = interp1(DataNAV.Time, DataNAV.Longitude, objTime);
        Latitude(k)  = interp1(DataNAV.Time, DataNAV.Latitude,  objTime);
    end
    %{
    DataDepth.VehicleDepth(k) = 0;
    DataDepth.Mode(k)               = 1;
    DataDepth.PingCounter(k)        = SampleBeamData.PingCounter;
    %}
    
        
    %% Constitution de l'image
        
    nMiddle = floor(nbBeams/2);
    
    val = max_lig(Amp, 'subx', 1:nMiddle); % 'LayerMask', this.Images(indLayerMask), 'valMask', valMask);
    ReflectivityBab(k,1:length(val)) = val;

    val = max_lig(Amp, 'subx', (nMiddle+1):nbBeams);
    ReflectivityTri(k,1:length(val)) = val;
    
   clear b
end
my_close(hw, 'MsgEnd')

if N == 0
    return
end

nbSamples = size(ReflectivityBab,2);
Reflectivity = [fliplr(ReflectivityBab) NaN(N,1, 'single') ReflectivityTri];
Reflectivity = reflec_Amp2dB(Reflectivity);

%% Image de la synth�se de d�tection

DataType     = cl_image.indDataType('Reflectivity');
GeometryType = cl_image.indGeometryType('PingSamples');
that = cl_image('Image', Reflectivity, 'Name', NomImage, 'Unit', 'dB', ...
    'x', (1:(2*nbSamples+1)) -(nbSamples+1), 'y', 1:N, ...
    'DataType', DataType, 'GeometryType', GeometryType, ...
    'XUnit', 'Samples #', 'YUnit', 'Pings #', ...
    'TagSynchroX', TagSynchroX, 'TagSynchroY', TagSynchroY, ...
    'InitialFileFormat', InitialFileFormat, 'InitialFileName', InitialFileName);
% that.InitialImageName = InitialImageName;

that = set_Carto(that, Carto);
that = update_Name(that);
that = set_SonarTime(             that, cl_time('timeMat', Time));
that = set_SonarDescription(      that, SonarDescription);
that = set_SonarHeight(           that, -abs(Height));
that = set_SonarImmersion(        that, TransducerDepth);
that = set_SonarSurfaceSoundSpeed(that, SurfaceSoundSpeed);
that = set_SonarSampleFrequency(  that, SampleRate);
that = set_SonarFrequency(        that, Frequency);
that = set_SonarRoll(             that, Roll);
that = set_SonarPitch(            that, Pitch);
that = set_SonarHeave(            that, Heave);
that = set_SonarHeading(          that, Heading);
that = set_SonarFishLongitude(    that, Longitude);
that = set_SonarFishLatitude(     that, Latitude);
that = set_SonarPingCounter(      that, PingCounter);

resolutionMajoritaire =  mean(SurfaceSoundSpeed./(2*SampleRate));
that = set_SonarRawDataResol(     that, resolutionMajoritaire);
that = set_SonarResolutionD(      that, resolutionMajoritaire);
that = set_SonarPortMode_1(       that, ones(size(PingCounter), 'single'));

%{
MultiPingSequence: 0
PingSequence: 0
Absorption: 45
that = set_SonarTide(that, DataDepth.Tide(1:N));
%}

%% C'est fini

flag = 1;
