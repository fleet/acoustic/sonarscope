function [flag, Carto] = sonar_SurveyDetectionPhaseAndAmplitude(nomFic, DisplayLevel, nomFicAvi, GateDepth, nomDirErs, IdentAlgo, skipAlreadyProcessedFiles, varargin)

[varargin, Carto] = getPropertyValue(varargin, 'Carto', []); %#ok<ASGLU>

flag = 0;

if isempty(nomFicAvi)
    mov = [];
else
	mov  = VideoWriter(nomFicAvi,'Uncompressed AVI'); % FORGE 353
end

if ischar(nomFic)
    nomFic = {nomFic};
end

N = length(nomFic);
str1 = 'Traitement du BDA des fichier .s7k en cours';
str2 = 'BDA processing on .s7k files';
hw = create_waitbar(Lang(str1,str2), 'N', N);
for kFic=1:N
    my_waitbar(kFic, N, hw)
    
    if skipAlreadyProcessedFiles && alreadyProcessed(nomFic{kFic}, nomDirErs)
        continue
    end
    
    [flag, b, mov, Carto] = sonar_SurveyDetectionPhaseAndAmplitude_unitaire(nomFic{kFic}, DisplayLevel, mov, Carto, GateDepth, IdentAlgo);
    if ~flag
        continue
    end
    
    % Send images in virtual memory
    %     b = optimiseMemory(b, 'SizeMax', 0); % Finalement comment� car on perdait du temps pour rien
    
    %% Save images in ErMapper files
    
    if ~isempty(nomDirErs)
        for k=1:length(b)
            NomImage = b(k).Name;
            NomDir = extract_ImageName(b(k));
            NomDir = NomDir(1:15);
            NomDir = fullfile(nomDirErs, NomDir);
            if k == 1
                if isfolder(NomDir)
                    try
                        rmdir(NomDir, 's');
                    catch %#ok<CTCH>
                        str1 = sprintf('%s n''a pas pu �tre supprim�, v�rifiez si u est ouvert dans un logiciel quelconque.', NomDir);
                        str2 = sprintf('%s could not be removed, check if any software is using a file of it.', NomDir);
                        my_warndlg(Lang(str1,str2), 0);
                    end
                end
                try
                    mkdir(NomDir);
                catch %#ok<CTCH>
                    str1 = sprintf('%s n''a pas pu �tre cr��.', NomDir);
                    str2 = sprintf('%s could not be created.', NomDir);
                    my_warndlg(Lang(str1,str2), 0);
                end
            end
            nomFicErs = fullfile(NomDir, NomImage);
            flag = export_ermapper(b(k), nomFicErs);
            
            if b(k).DataType == cl_image.indDataType('Bathymetry')
                NomFicAlgo = get_nomFicAlg(cl_ermapper_ers([]), nomFicErs, 'Shading');
                try
                    winopen(NomFicAlgo)
                catch %#ok<CTCH>
                    str1 = sprintf('%s n''a pas pu �tre visualis�.', NomFicAlgo);
                    str2 = sprintf('%s could not be displayed.', NomFicAlgo);
                    my_warndlg(Lang(str1,str2), 0);
                end
            end
        end
    end
    clear b
end
my_close(hw, 'MsgEnd')

%% Close .avi file and display it

if ~isempty(nomFicAvi)
    close(mov);
    if isdeployed
        winopen(nomFicAvi)
    else
        my_implay(nomFicAvi)
    end
end

function [flag, that, mov, Carto] = sonar_SurveyDetectionPhaseAndAmplitude_unitaire(nomFic, DisplayLevel, mov, Carto, GateDepth, IdentAlgo)

global TestPlatformCmatlab %#ok<GVMIS>

flag = 0;

Draught = get_DraughtShip;

that = cl_image.empty;

s7k = cl_reson_s7k('nomFic', nomFic, 'KeepNcID', true);
if isempty(s7k)
    return
end

identSondeur = selectionSondeur(s7k);
Data = read_BeamRawData(s7k, identSondeur, 'subPing', 'None', 'flagMessage', 0);
if isempty(Data)
    return
end
N = length(Data.PingCounter);
if N == 0
    return
end
subPing = 1:N;
% if N>=50
%     subPing = 1:50;
% else
%     subPing = 1:N;
% end

N = length(subPing);

DataNAV      = read_navigation(s7k, identSondeur);
DataAttitude = read_attitude(s7k, identSondeur);
Data7030     = read_sonarInstallationParameters(s7k, identSondeur);

DepthPingPrev = [];
Compteur = [];
str1 = sprintf('D�tection du fond en cours sur %s', nomFic);
str2 = sprintf('Processing Bottom Detection on %s', nomFic);
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
    iPing = subPing(k);
    my_waitbar(k, N, hw)
    
    [Compteur, strTFin] = estimation_temps_traitement(k, N, Compteur);
    str = sprintf('%5d/%5d : iPing=%d  %s - %s\n', k, N, iPing, nomFic, strTFin);
    my_warndlg(str, 0);
    %     fprintf('%s\n', str);
    
    [b, Carto] = view_BeamRawData(s7k, 'subPing', iPing, 'Carto', Carto, ...
        'DisplayLevel', DisplayLevel, 'flagMessage', 0);
    if isempty(b)
        N = k - 1;
        break
    end
    
    if k == 1
        X = get(b(1), 'Sonar');
        SampleBeamData = X.SampleBeamData;
        if (SampleBeamData.SystemSerialNumber == 7150) && ...
                (SampleBeamData.Frequency < 18000)
            [tetav, DifferentielAngles] = VraisAngles_7150_12kHz; %#ok<ASGLU>
        else
            tetav = [];
        end
        
        nbBeams   = b(1).nbColumns;
        nbSamples = b(1).nbRows;
        SampleTime = (1:nbSamples) / SampleBeamData.SampleRate;
        SampleTime = SampleTime/(3600*24);
        
        BeamsToDisplay = 1:nbBeams;
        
        Range               = NaN(N, nbBeams, 'single');
        AmpImageRange       = NaN(N, nbBeams, 'single');
        PhaseImageRange     = NaN(N, nbBeams, 'single');
        AmpImageQF          = NaN(N, nbBeams, 'single');
        AmpImageNbSamples   = NaN(N, nbBeams, 'single');
        AmpImageMax         = NaN(N, nbBeams, 'single');
        AmpImageQF2         = NaN(N, nbBeams, 'single');
        AmpImageQF3         = zeros(N, nbBeams, 'single');
        Angles              = NaN(N, nbBeams, 'single');
        PhaseImageQF        = NaN(N, nbBeams, 'single');
        PhaseImagePente     = NaN(N, nbBeams, 'single');
        PhaseImageNbSamples = NaN(N, nbBeams, 'single');
        PhaseImageEqm       = NaN(N, nbBeams, 'single');
        TypeDetection       = NaN(N, nbBeams, 'single');
        SyntheseQF          = NaN(N, nbBeams, 'single');
        Bathy               = NaN(N, nbBeams, 'single');
        %         AcrossDistance      = NaN(N, nbBeams, 'single');
        
        
        DataDepth = Reson_BottomDetector_initDataDepth(N);
        Time      = NaN(1, N);
        
        InitialFileName = b(1).InitialFileName;
        NomImage = extract_ImageName(b(1));
        [~, TagSynchro] = fileparts(InitialFileName);
        SonarDescription =  get(b(1), 'SonarDescription');
        identSondeur = get(SonarDescription, 'Sonar.SystemSerialNumber');
        TagSynchroX = [num2str(identSondeur) '_' TagSynchro '_' num2str(rand(1))];
        TagSynchroY = TagSynchroX;
        InitialFileFormat = b(1).InitialFileFormat;
        GeometryType = cl_image.indGeometryType('PingBeam');
        DataType = cl_image.indDataType('RayPathSampleNb');
        % nbCol = b(1).nbColumns;
        InitialImageName = b(1).InitialImageName;
        
        drawnow
    end
    
    Amp   = b(1);
    Phase = b(2);
    
    %% BDA sur un ping
    
    AmpImage   = get(Amp,   'Image');
    PhaseImage = get(Phase, 'Image');
    
    DataPing = get(Amp, 'SampleBeamData');
    SonarDescription = get(Amp, 'SonarDescription');
    Frequency = get(SonarDescription, 'Sonar.Freq');
    BeamAngles = DataPing.TxAngle;
    
    BottomDetectDepthFilterFlag         = DataPing.BottomDetectDepthFilterFlag;
    BottomDetectionFilterInfoMinDepth   = DataPing.BottomDetectionFilterInfoMinDepth;
    BottomDetectionFilterInfoMaxDepth   = DataPing.BottomDetectionFilterInfoMaxDepth;
    TxPulseWidth                        = DataPing.TxPulseWidth; % s
    SampleRate                          = DataPing.SampleRate; % Hz
    ReceiveBeamWidth                    = DataPing.ReceiveBeamWidth * (180/pi); % deg
    PingSequence                        = DataPing.PingSequence;
    SystemSerialNumber                  = DataPing.SystemSerialNumber;
    ProjectionBeam_3dB_WidthVertical    = DataPing.ProjectionBeam_3dB_WidthVertical;
    SoundVelocity                       = DataPing.SoundVelocity;
    %     MultiPingSequence                   = DataPing.MultiPingSequence;
    
    [DepthMinPing, DepthMaxPing] = BDA_gates(GateDepth, BottomDetectDepthFilterFlag, BottomDetectionFilterInfoMinDepth, BottomDetectionFilterInfoMaxDepth, Draught);
    
    if any(TestPlatformCmatlab.sonar_BDA == [1 3 4])
        [PingRange_M, TD_M, QualityFactor_M, AmpPingRange_M, PhasePingRange_M, AmpPingQF_M, PhasePingQF_M, ...
            AmpPingSamplesNb_M, AmpPingQF2_M, PhasePingNbSamples_M, SampleRate_M, BeamAngles_M, iBeamBeg_M, iBeamEnd_M, SystemSerialNumber_M, ...
            AmpPingNormHorzMax_M, PhasePingPente_M, PhasePingEqm_M, DepthPingPrev] = ...
            sonar_BDA(...
            AmpImage, PhaseImage, DepthMinPing, DepthMaxPing, BeamAngles, ...
            Frequency, SampleRate, TxPulseWidth, ReceiveBeamWidth, SystemSerialNumber, ...
            IdentAlgo, DisplayLevel, iPing, PingSequence, BeamsToDisplay, ...
            ProjectionBeam_3dB_WidthVertical, DepthPingPrev);
    end
    
    if any(TestPlatformCmatlab.sonar_BDA == [2 3 4])
        [PingRange_C, TD_C, QualityFactor_C, AmpPingRange_C, PhasePingRange_C, AmpPingQF_C, PhasePingQF_C, ...
            SampleRate_C, BeamAngles_C, iBeamBeg_C, iBeamEnd_C, SystemSerialNumber_C] = ...
            sonar_BDA_reson(...
            single(AmpImage), single(PhaseImage), ...
            double(DepthMinPing), double(DepthMaxPing), double(BeamAngles), ...
            double(Frequency), double(SampleRate), double(TxPulseWidth), double(ProjectionBeam_3dB_WidthVertical), ...
            double(SoundVelocity), double(SystemSerialNumber), double(IdentAlgo));
    end
    
    if any(TestPlatformCmatlab.sonar_BDA == [3 4])
        BDA_compare_Matlab_C('sonar_BDA', 'PingRange',     PingRange_M,     PingRange_C,     iPing, [], TestPlatformCmatlab.Seuil_sonar_BDA_PingRange);
        BDA_compare_Matlab_C('sonar_BDA', 'QualityFactor', QualityFactor_M, QualityFactor_C, iPing, [], TestPlatformCmatlab.Seuil_sonar_BDA_QualityFactor);
        BDA_compare_Matlab_C('sonar_BDA', 'TD',            TD_M,            TD_C,            iPing, [], TestPlatformCmatlab.Seuil_sonar_BDA_TD);
        BDA_compare_Matlab_C('sonar_BDA', 'QualityFactor', QualityFactor_M, QualityFactor_C, iPing, [], TestPlatformCmatlab.Seuil_sonar_BDA_QualityFactor);
        BDA_compare_Matlab_C('sonar_BDA', 'AmpPingRange', AmpPingRange_M, AmpPingRange_C, iPing, [], TestPlatformCmatlab.Seuil_sonar_BDA_TD);
        BDA_compare_Matlab_C('sonar_BDA', 'PhasePingRange', PhasePingRange_M, PhasePingRange_C, iPing, [], TestPlatformCmatlab.Seuil_sonar_BDA_TD);
        BDA_compare_Matlab_C('sonar_BDA', 'AmpPingQF', AmpPingQF_M, AmpPingQF_C, iPing, [], TestPlatformCmatlab.Seuil_sonar_BDA_TD);
        BDA_compare_Matlab_C('sonar_BDA', 'PhasePingQF', PhasePingQF_M, PhasePingQF_C, iPing, [], TestPlatformCmatlab.Seuil_sonar_BDA_TD);
        if (SampleRate_M ~= SampleRate_C)
            BDA_createMes('sonar_BDA', 'SampleRate', SampleRate_M, SampleRate_C, [], [], 0);
        end
        BDA_compare_Matlab_C('sonar_BDA', 'BeamAngles', BeamAngles_M, BeamAngles_C, iPing, [], TestPlatformCmatlab.Seuil_sonar_BDA_TD);
        if (iBeamBeg_M ~= iBeamBeg_C)
            BDA_createMes('sonar_BDA', 'iBeamBeg', iBeamBeg_M, iBeamBeg_C, [], [], 0);
        end
        if (iBeamEnd_M ~= iBeamEnd_C)
            BDA_createMes('sonar_BDA', 'iBeamEnd', iBeamEnd_M, iBeamEnd_C, [], [], 0);
        end
        if (SystemSerialNumber_M ~= SystemSerialNumber_C)
            BDA_createMes('sonar_BDA', 'SystemSerialNumber', SystemSerialNumber_M, SystemSerialNumber_C, [], [], 0);
        end
    end
    
    switch TestPlatformCmatlab.sonar_BDA
        case {1; 3}
            PingRange             = PingRange_M;
            TD                    = TD_M;
            QualityFactor         = QualityFactor_M;
            AmpPingRange          = AmpPingRange_M;
            PhasePingRange        = PhasePingRange_M;
            AmpPingQF             = AmpPingQF_M;
            PhasePingQF           = PhasePingQF_M;
            AmpPingSamplesNb      = AmpPingSamplesNb_M;
            AmpPingQF2            = AmpPingQF2_M;
            PhasePingNbSamples    = PhasePingNbSamples_M;
            SampleRate            = SampleRate_M;
            BeamAngles            = BeamAngles_M;
            iBeamBeg              = iBeamBeg_M;
            iBeamEnd              = iBeamEnd_M;
            SystemSerialNumber    = SystemSerialNumber_M;
            AmpPingNormHorzMax    = AmpPingNormHorzMax_M;
            PhasePingPente        = PhasePingPente_M;
            PhasePingEqm          = PhasePingEqm_M;
        case {2; 4}
            PingRange             = PingRange_C;
            TD                    = TD_C;
            QualityFactor         = QualityFactor_C;
            AmpPingRange          = AmpPingRange_C;
            PhasePingRange        = PhasePingRange_C;
            AmpPingQF             = AmpPingQF_C;
            PhasePingQF           = PhasePingQF_C;
            AmpPingSamplesNb      = NaN(size(PingRange_C)); % AmpPingSamplesNb_M;
            AmpPingQF2            = NaN(size(PingRange_C)); % AmpPingQF2_M;
            PhasePingNbSamples    = NaN(size(PingRange_C)); % PhasePingNbSamples_M;
            SampleRate            = SampleRate_C;
            BeamAngles            = BeamAngles_C;
            iBeamBeg              = iBeamBeg_C;
            iBeamEnd              = iBeamEnd_C;
            SystemSerialNumber    = SystemSerialNumber_C;
            AmpPingNormHorzMax    = NaN(size(PingRange_C)); % AmpPingNormHorzMax_M;
            PhasePingPente        = NaN(size(PingRange_C)); % PhasePingPente_M;
            PhasePingEqm          = NaN(size(PingRange_C)); % PhasePingEqm_M;
    end
    
    %% Display results
    
    if DisplayLevel == 2
        figure(12321);
        h(1) = subplot(4,1,1); plot(AmpPingRange, 'k'); hold on; grid on;
        PlotUtils.createSScPlot(PhasePingRange, 'r'); set(h(1), 'YDir', 'Reverse');
        set(h(1), 'YLim', [0 nbSamples]); title('Range'); hold off;
        
        h(2) = subplot(4,1,2);
        Coef = 1500 / (2 * SampleRate);
        PlotUtils.createSScPlot(-(AmpPingRange * Coef) .*cosd(BeamAngles), 'k'); hold on; grid on;
        PlotUtils.createSScPlot(-(PhasePingRange * Coef).*cosd(BeamAngles), 'r'); title('Z'); hold off;
        
        h(3) = subplot(4,1,3);
        PlotUtils.createSScPlot(AmpPingQF, 'k'); grid on; title('AmpPingQF', 'Interpreter', 'none')
        hold on
        PlotUtils.createSScPlot(PhasePingQF, 'r'); grid on; title('PhasePingQF', 'Interpreter', 'none')
        set(h(3), 'YLim', [2 max(4, max([AmpPingQF PhasePingQF]))])
        hold off
        
        subplot(4,1,4);
        z_Amp = -(AmpPingRange * Coef).*cosd(BeamAngles);
        z_Phs = -(PhasePingRange * Coef).*cosd(BeamAngles);
        PlotUtils.createSScPlot((AmpPingRange * Coef).*sind(BeamAngles), z_Amp, 'k'); hold on; grid on; %axis equal;
        PlotUtils.createSScPlot((PhasePingRange * Coef) .*sind(BeamAngles), z_Phs, 'r'); title('Bathy'); hold off;
        
        for k2=1:length(h)
            %             axes(h(k2));
            hold(h(k2), 'on');
            plot(h(k2), [iBeamBeg iBeamBeg], get(h(k2), 'YLim'), 'g');
            plot(h(k2), [iBeamEnd iBeamEnd], get(h(k2), 'YLim'), 'g');
            hold(h(k2), 'off');
        end
        
        linkaxes(h, 'x')
    end
    
    if DisplayLevel == 2
        ImageName = extract_ImageName(Phase);
        ScreenSize = get(0,'ScreenSize');
        Fig = figure(12349);
        if ~isempty(mov)
            set(Fig, 'Position', floor([10 40 ScreenSize(3)-20 ScreenSize(4)-140]/4)*4);
        end
        
        AmpImage   = get(Amp, 'Image');
        %       PhaseImage = get(Phase, 'Image');
        %       AmpImage(AmpImage < 0) = NaN;
        
        Amp_dB = reflec_Enr2dB(AmpImage);
        imagesc(Amp_dB); colormap(jet(256)); colorbar;
        title(sprintf('Bottom detection on %s', ImageName), 'Interpreter', 'none')
        hold on;
        PlotUtils.createSScPlot(abs(PhasePingRange), 'r'); PlotUtils.createSScPlot(abs(AmpPingRange), 'k');
        subAmp = find(TD == 1);
        PlotUtils.createSScPlot(subAmp, abs(PingRange(subAmp)), 'ok');
        subPhase = find(TD == 2);
        PlotUtils.createSScPlot(subPhase, abs(PingRange(subPhase)), 'or');
        hold off;
        
        %         hImage(2) = subplot(1,2,2);
        %         I = get(Phase, 'Image');
        %         imagesc(-abs(I)); colormap(jet(256)); colorbar;
        %         title(sprintf('Bottom detection on %s', ImageName), 'Interpreter', 'none')
        %         hold on;
        %         PlotUtils.createSScPlot(abs(PhasePingRange), 'b'); PlotUtils.createSScPlot(abs(AmpPingRange), 'k');
        %         PlotUtils.createSScPlot(subAmp, abs(PingRange(subAmp)), 'ok');
        %         PlotUtils.createSScPlot(subPhase, abs(PingRange(subPhase)), 'ob');
        %         hold off;
        %
        %         linkaxes(hImage)
        
        if ~isempty(mov)
            I =  getframe(Fig);
            I = I.cdata;
            size(I)
            % OLD mov = addframe(mov, I);
            writeVideo(mov,I); % FORGE 353
        end
        
        drawnow
    end
    clear Phase Amp
    
    subBeams = 1:length(PingRange);
    Range(k,subBeams)         = PingRange;
    TypeDetection(k,subBeams) = TD;
    SyntheseQF(k,subBeams)    = QualityFactor;
    
    AmpImageRange(k,subBeams)        = AmpPingRange;
    AmpImageQF(k,subBeams)           = AmpPingQF;
    AmpImageNbSamples(k,subBeams)    = AmpPingSamplesNb;
    AmpImageMax(k,subBeams)          = AmpPingNormHorzMax;
    AmpImageQF2(k,subBeams)          = AmpPingQF2;
    AmpImageQF3(k,iBeamBeg:iBeamEnd) = 1;
    subNaN = isnan(TD);
    AmpImageQF3(k,subNaN) = NaN;
    
    PhaseImageRange(k,subBeams)     = PhasePingRange;
    PhaseImageQF(k,subBeams)        = PhasePingQF;
    PhaseImagePente(k,subBeams)     = PhasePingPente;
    PhaseImageNbSamples(k,subBeams) = PhasePingNbSamples;
    PhaseImageEqm(k,subBeams)       = PhasePingEqm;
    
%     if isempty(tetav)
	Angles(k,subBeams) = BeamAngles;
%     else
%         %         ifr = Phase(k).Sonar.SampleBeamData.MultiPingSequence;
%         %         Angles(k,:) = SampleBeamData.TxAngle + DifferentielAngles(ifr,:);
%         
%         Angles(k,subBeams) = BeamAngles;
%     end
    
    
    DataDepth.VehicleDepth(k) = 0;
    %     Time(k) = SampleBeamData.Time; % Modifi� le 23/03/2013 mission ESSSYPEN
    %     objTime = cl_time('timeMat', SampleBeamData.Time);
    Time(k) = SampleBeamData.Time.timeMat;
    objTime = SampleBeamData.Time;
    
    if isempty(Data7030) || ~isfield(Data7030, 'DRF')
        DataDepth.TransducerDepth(k) = 0; % TODO : r�cup�rer dans fichier externe InstallationParameters_xxkHz.xml
    else
        DataDepth.TransducerDepth(k) = Data7030.DRF(1).ReceiveArrayZ - Data7030.DRF(1).WaterLineVerticalOffset;
    end
    
    DataDepth.SurfaceSoundSpeed(k) = SampleBeamData.SoundVelocity;
    DataDepth.Mode(k)              = 1;
    DataDepth.SampleRate(k)        = SampleBeamData.SampleRate;
    DataDepth.Frequency(k)         = SampleBeamData.Frequency/1000;
    
    if isempty(DataAttitude) || ~isfield(DataAttitude, 'Roll') || isempty(DataAttitude.Roll)
        DataDepth.Roll(k)    = 0;
        DataDepth.Pitch(k)   = 0;
        DataDepth.Heave(k)   = 0;
        DataDepth.Heading(k) = 0;
        RollTx  = 0;
        PitchTx = 0;
        HeaveTx = 0;
        RollRx  = zeros(1,nbBeams);
        PitchRx = zeros(1,nbBeams);
        HeaveRx = zeros(1,nbBeams);
    else
        DataDepth.Roll(k)    = interp1(DataAttitude.Time, DataAttitude.Roll,    objTime);
        DataDepth.Pitch(k)   = interp1(DataAttitude.Time, DataAttitude.Pitch,   objTime);
        DataDepth.Heave(k)   = interp1(DataAttitude.Time, DataAttitude.Heave,   objTime);
        DataDepth.Heading(k) = interp1(DataAttitude.Time, DataAttitude.Heading, objTime);
        
        % Modif JMA le 23/06/2014 pour ping 73 fichier
        % EvalHydro/Feb2008/7111/Rock/20080219_233937
        %         iPingRange = floor(PingRange);
        iPingRange = floor(PingRange);
        iPingRange(iPingRange > length(SampleTime)) = NaN;
        
        subNonNan = find(~isnan(iPingRange));
        
        RollTx  = DataDepth.Roll(k);
        PitchTx = DataDepth.Pitch(k);
        HeaveTx = DataDepth.Heave(k);
        
        RollRx  = NaN(1,nbBeams);
        PitchRx = NaN(1,nbBeams);
        HeaveRx = NaN(1,nbBeams);
        
        tRx = SampleBeamData.Time + SampleTime(iPingRange(subNonNan));
        % Modif pour fichier 20080219_233937
        %         HeaveRx(subNonNan) = interp1(DataAttitude.Time.timeMat, DataAttitude.Heave, tRx);
        %         PitchRx(subNonNan) = interp1(DataAttitude.Time.timeMat, DataAttitude.Pitch, tRx);
        %         RollRx(subNonNan)  = interp1(DataAttitude.Time.timeMat, DataAttitude.Roll,  tRx);
        HeaveRx(subNonNan) = interp1(DataAttitude.Time, DataAttitude.Heave, tRx);
        PitchRx(subNonNan) = interp1(DataAttitude.Time, DataAttitude.Pitch, tRx);
        RollRx(subNonNan)  = interp1(DataAttitude.Time, DataAttitude.Roll,  tRx);
    end
    
    DataDepth.Tide(k)        = 0;
    DataDepth.PingCounter(k) = SampleBeamData.PingCounter;
    
    if isempty(DataNAV) || ~isfield(DataNAV, 'Latitude') || isempty(DataNAV.Longitude)
        DataDepth.Longitude(k) = 0;
        DataDepth.Latitude(k)  = 0;
    else
        DataDepth.Longitude(k) = interp1(DataNAV.Time, DataNAV.Longitude, objTime);
        DataDepth.Latitude(k)  = interp1(DataNAV.Time, DataNAV.Latitude,  objTime);
    end
    
    if isa(Data7030.Time, 'cl_time')
        Data7030.Time = Data7030.Time.timeMat;
    end
    if isempty(Data7030) || isempty(Data7030.Time)
        %         Bathy(k,:) = calcul_bathy_simple(Range(k,:), SampleRate, Angles(k,:), SystemSerialNumber);
        Bathy(k,:) = calcul_bathy_simple(Range(k,:), SampleRate, Angles(k,:), Draught);
    else
        Bathy(k,:) = calcul_bathy(Range(k,:), SampleRate, Angles(k,:), ...
            RollTx, PitchTx, HeaveTx, RollRx, PitchRx, HeaveRx, Data7030, SystemSerialNumber, Draught);
    end
    
    clear b
end
my_close(hw, 'MsgEnd')
if N == 0
    return
end

my_close(12321)
my_close(10000)

%% Image de la synth�se de d�tection

that = cl_image('Image', Range(1:N,:));
that.Name              = NomImage;
that.InitialImageName  = InitialImageName;
that.Unit              = 'Sample';
that.TagSynchroX       = TagSynchroX;
that.TagSynchroY       = TagSynchroY;
that.XUnit             = 'Beam #';
that.YUnit             = 'Ping';
that.InitialFileName   = InitialFileName;
that.InitialFileFormat = InitialFileFormat;
that.GeometryType      = GeometryType;
that.DataType          = DataType;
that = set_SonarDescription(that, SonarDescription);
that.Comments          = 'Range coming from Phase';
%     that.strMaskBits = strMaskBits;
that.x                 = 1:nbBeams;
that.y                 = 1:N;
that.YDir              = 1;
that.ColormapIndex     = 3;

that = set_Carto(that, Carto);

% ----------------------------
% D�finition du nom de l'image

that = update_Name(that);

DataDepth.Time = cl_time('timeMat', Time(1:N));

%% Transfert des signaux

that = set_SonarHeight(that, -abs(DataDepth.VehicleDepth(1:N)));
that = set_SonarTime(that, DataDepth.Time);
that = set_SonarImmersion(that, DataDepth.TransducerDepth(1:N));
that = set_SonarSurfaceSoundSpeed(that, DataDepth.SurfaceSoundSpeed(1:N)');
that = set_SonarPortMode_1(that, DataDepth.Mode(1:N));
that = set_SonarPingCounter(that, subPing);

%     that = set_SonarRawDataResol(that, resolutionMajoritaire);
%     that = set_SonarResolutionD(that, resolutionMajoritaire);

that = set_SonarSampleFrequency(that, DataDepth.SampleRate(1:N));
that = set_SonarFrequency(that, DataDepth.Frequency(1:N));

that = set_SonarRoll(that, DataDepth.Roll(1:N));
that = set_SonarPitch(that, DataDepth.Pitch(1:N));

that = set_SonarHeave(that, DataDepth.Heave(1:N));
that = set_SonarTide(that, DataDepth.Tide(1:N));
that = set_SonarPingCounter(that, DataDepth.PingCounter(1:N));

that = set_SonarFishLongitude(that, DataDepth.Longitude(1:N));
that = set_SonarFishLatitude(that, DataDepth.Latitude(1:N));
that = set_SonarHeading(that, DataDepth.Heading(1:N));




% % A SUPPRIMER AU PLUS VITE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % En attendant de pouvoir r�cup�rer la position dans les datagrams j'en
% % cr�e une de synth�se
% delataLon = (5.9868 - 5.9936) / 500;
% deltaLat = (43.0604 - 43.034) / 500;
% Longitude = 5.9936 + delataLon * subPing;
% Latitude  = 43.034 + deltaLat * subPing;
% Heading = repmat(349.3376, N, 1);
%
% that = set_SonarFishLongitude(that, Longitude);
% that = set_SonarFishLatitude(that, Latitude);
% that = set_SonarHeading(that, Heading);
% % A SUPPRIMER AU PLUS VITE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%% Image de la d�tection en amplitude

n = 2;
that(n) = replace_Image(that(1), AmpImageRange(1:N,:));
that(n) = compute_stats(that(n));
that(n).TagSynchroContrast = num2str(rand(1)); 
StatValues       = that(n).StatValues;
that(n).CLim     = [StatValues.Min StatValues.Max]; 
that(n).Unit     = 'Deg/Sample';
that(n).DataType = DataType;
that(n) = update_Name(that(n), 'Append', 'Amp');
clear AmpImageRange

%% Image de l'amplitude max dans le processus de detection du masque

n = n+1;
that(n) = replace_Image(that(1), AmpImageMax(1:N,:));
that(n) = compute_stats(that(n));
that(n).TagSynchroContrast = num2str(rand(1)); 
StatValues = that(n).StatValues;
that(n).CLim     = [StatValues.Min StatValues.Max]; 
that(n).Unit     = '';
that(n).DataType = cl_image.indDataType('Reflectivity');
that(n) = update_Name(that(n), 'Append', 'Amp');
that(n).ColormapIndex = 2;
clear AmpImageMax

%% Image du nombre de points utilis�s dans la d�tection en amplitude

n = n+1;
that(n) = replace_Image(that(1), AmpImageNbSamples(1:N,:));
that(n) = compute_stats(that(n));
that(n).TagSynchroContrast = num2str(rand(1)); 
StatValues = that(n).StatValues;
that(n).CLim     = [StatValues.Min StatValues.Max]; 
that(n).Unit     = '';
that(n).DataType = cl_image.indDataType('MbesDetectionNbSamples');
that(n) = update_Name(that(n), 'Append', 'Amp');

%% Image du facteur de qualit� en amplitude

n = n+1;
that(n) = replace_Image(that(1), AmpImageQF(1:N,:));
that(n) = compute_stats(that(n));
that(n).TagSynchroContrast = num2str(rand(1)); 
StatValues = that(n).StatValues;
that(n).CLim     = [StatValues.Min StatValues.Max]; 
% that(n).CLim = [-max(CLim) max(CLim)];
that(n).Unit     = '';
that(n).DataType = cl_image.indDataType('MbesQualityFactor');
that(n) = update_Name(that(n), 'Append', 'Amp');
% that(n).ColormapIndex = 15; % 15=NIWA1
clear AmpImageQF

%% Image du facteur de qualit� en amplitude

n = n+1;
AmpImageQF2(isinf(AmpImageQF2)) = NaN;
that(n) = replace_Image(that(1), AmpImageQF2(1:N,:));
that(n) = compute_stats(that(n));
that(n).TagSynchroContrast = num2str(rand(1)); 
StatValues = that(n).StatValues;
that(n).CLim     = [StatValues.Min StatValues.Max]; 
that(n).Unit     = '';
that(n).DataType = cl_image.indDataType('MbesQualityFactor');
that(n) = update_Name(that(n), 'Append', 'Amp_QF2');
% that(n).ColormapIndex = 15; % 15=NIWA1
clear AmpImageQF2

%% Image du mask des amplitudes significatives

n = n+1;
AmpImageQF3(isinf(AmpImageQF3)) = NaN;
that(n) = replace_Image(that(1), AmpImageQF3(1:N,:));
that(n) = compute_stats(that(n));
that(n).TagSynchroContrast = num2str(rand(1)); 
that(n).CLim               = [-1 1];
that(n).Unit               = '';
that(n).DataType           = cl_image.indDataType('Mask');
that(n) = update_Name(that(n), 'Append', 'Colinearity');

%% Image de la d�tection en phase

n = n+1;
that(n) = replace_Image(that(1), PhaseImageRange(1:N,:));
that(n) = compute_stats(that(n));
that(n).TagSynchroContrast = num2str(rand(1)); 
StatValues = that(n).StatValues;
that(n).CLim     = [StatValues.Min StatValues.Max]; 
that(n).Unit     = 'Deg/Sample';
that(n).DataType = DataType;
that(n) = update_Name(that(n), 'Append', 'Phase');
clear PhaseImageRange

%% Image du nombre de points utilis�s dans la d�tection en phase

n = n+1;
that(n) = replace_Image(that(1), PhaseImageNbSamples(1:N,:));
that(n) = compute_stats(that(n));
that(n).TagSynchroContrast = num2str(rand(1)); 
StatValues = that(n).StatValues;
that(n).CLim     = [StatValues.Min StatValues.Max]; 
that(n).Unit     = '';
that(n).DataType = cl_image.indDataType('MbesDetectionNbSamples');
that(n) = update_Name(that(n), 'Append', 'Phase');
clear PhaseImageNbSamples

%% Image du facteur de qualit� en phase

n = n+1;
that(n) = replace_Image(that(1), PhaseImageQF(1:N,:));
that(n) = compute_stats(that(n));
that(n).TagSynchroContrast = num2str(rand(1)); 
StatValues = that(n).StatValues;
that(n).CLim     = [StatValues.Min StatValues.Max]; 
% that(n).CLim = [-max(CLim) max(CLim)];
that(n).Unit     = '';
that(n).DataType = cl_image.indDataType('MbesQualityFactor');
that(n) = update_Name(that(n), 'Append', 'Phase');
% that(n).ColormapIndex = 15; % 15=NIWA1

%% Image des pentes de phase

n = n+1;
that(n) = replace_Image(that(1), PhaseImagePente(1:N,:));
that(n) = compute_stats(that(n));
that(n).TagSynchroContrast = num2str(rand(1)); 
StatValues = that(n).StatValues;
that(n).CLim     = [StatValues.Min StatValues.Max]; 
that(n).Unit     = 'Deg/Sample';
that(n).DataType = cl_image.indDataType('MbesPhaseSlope');
that(n) = update_Name(that(n), 'Append', 'Phase');
clear PhaseImagePente

%% Image des pentes de PhaseImageEqm

n = n+1;
that(n) = replace_Image(that(1), PhaseImageEqm(1:N,:));
that(n) = compute_stats(that(n));
that(n).TagSynchroContrast = num2str(rand(1)); 
StatValues = that(n).StatValues;
that(n).CLim     = [StatValues.Min StatValues.Max]; 
that(n).Unit     = 'Deg';
that(n).DataType = cl_image.indDataType('MbesPhaseEqm');
that(n) = update_Name(that(n), 'Append', 'Phase');
clear PhaseImageEqm

%% Image des angles

n = n+1;
that(n) = replace_Image(that(1), Angles(1:N,:));
that(n) = compute_stats(that(n));
that(n).TagSynchroContrast = num2str(rand(1)); 
StatValues = that(n).StatValues;
that(n).CLim     = [StatValues.Min StatValues.Max]; 
that(n).Unit     = 'Deg';
that(n).DataType = cl_image.indDataType('TxAngle');
that(n) = update_Name(that(n));
% clear Angles % Used later

%% Image du type de d�tection

n = n+1;
that(n) = replace_Image(that(1), TypeDetection(1:N,:));
that(n) = compute_stats(that(n));
that(n).TagSynchroContrast = num2str(rand(1)); 
that(n).CLim               = [0 2];
that(n).Unit               = 'Amp/Phase';
that(n).DataType           = cl_image.indDataType('DetectionType');
that(n) = update_Name(that(n));

%% Image de la synth�se du facteur de qualit�

n = n+1;
that(n) = replace_Image(that(1), SyntheseQF(1:N,:));
that(n) = compute_stats(that(n));
that(n).TagSynchroContrast = num2str(rand(1)); 
StatValues = that(n).StatValues;
that(n).CLim     = [StatValues.Min StatValues.Max]; 
% that(n).CLim = [-max(CLim) max(CLim)];
that(n).Unit     = 'log10(z/dz)';
that(n).DataType = cl_image.indDataType('MbesQualityFactor');
that(n) = update_Name(that(n));
% that(n).ColormapIndex = 15; % 15=NIWA1

%% Masque du facteur de qualit� � 2.5

n = n+1;
that(n) = replace_Image(that(1), SyntheseQF(1:N,:) >= 2.5);
that(n) = compute_stats(that(n));
that(n).TagSynchroContrast = num2str(rand(1)); 
that(n).CLim               = [-1 1];
that(n).Unit               = 'Bool';
that(n).DataType           = cl_image.indDataType('Mask');
that(n) = update_Name(that(n), 'Append', 'Brightness');

%% Image de la pseudo bathym�trie

n = n+1;
% BriCol = (SyntheseQF(1:N,:) >= 2.2) & (AmpImageQF3(1:N,:) == 1);
BriCol = (AmpImageQF3(1:N,:) == 1); % Il est pr�f�rable de faire le masque apr�s coup
Z = Bathy(1:N,:);
Z(~BriCol) = NaN;
that(n) = replace_Image(that(1), Z);
that(n) = compute_stats(that(n));
that(n).TagSynchroContrast = num2str(rand(1)); 
StatValues = that(n).StatValues;
CLim = [StatValues.Min StatValues.Max];
that(n).CLim     = CLim; 
that(n).Unit     = 'm';
that(n).DataType = cl_image.indDataType('Bathymetry');
that(n) = update_Name(that(n));
clear Bathy

%% Image de la distance transversale

Coef = 1500 / (2 * SampleRate);
X = NaN(N,nbBeams, 'single');
for k=1:N
    Teta = Angles(k,:) - DataDepth.Roll(k);
    D = Range(k,:) * Coef;
    X(k,:) = D .* sind(Teta);
end
n = n+1;
that(n) = replace_Image(that(1), X);
that(n) = compute_stats(that(n));
that(n).TagSynchroContrast = num2str(rand(1)); 
StatValues = that(n).StatValues;
that(n).CLim     = [StatValues.Min StatValues.Max]; 
that(n).Unit     = 'm';
that(n).DataType = cl_image.indDataType('AcrossDist');
that(n) = update_Name(that(n));

%% Image de la distance longitudinale

n = n+1;
that(n) = replace_Image(that(1), X*0);
that(n) = compute_stats(that(n));
that(n).TagSynchroContrast = num2str(rand(1)); 
StatValues = that(n).StatValues;
that(n).CLim     = [StatValues.Min StatValues.Max]; 
that(n).Unit     = 'm';
that(n).DataType = cl_image.indDataType('AlongDist');
that(n) = update_Name(that(n));
clear X

%% Image du nombre de samples utilis�s pour la d�tection

n = n+1;
X = AmpImageNbSamples(1:N,:);
X2 = PhaseImageQF(1:N,:);
subPhase = (TypeDetection(1:N,:) == 2);
X(subPhase) = X2(subPhase);
that(n) = replace_Image(that(1), X);
that(n) = compute_stats(that(n));
that(n).TagSynchroContrast = num2str(rand(1)); 
StatValues = that(n).StatValues;
that(n).CLim     = [StatValues.Min StatValues.Max]; 
that(n).Unit     = '';
that(n).DataType = cl_image.indDataType('MbesDetectionNbSamples');
that(n) = update_Name(that(n));
clear X

%% C'est fini

that = that([2:n 1]);
flag = 1;



% function Z = calcul_bathy(Range, SampleRate, Angles, RollTx, PitchTx, HeaveTx, RollRx, PitchRx, HeaveRx, Data7030, SystemSerialNumber)
function Z = calcul_bathy(Range, SampleRate, Angles, RollTx, PitchTx, HeaveTx, RollRx, PitchRx, HeaveRx, Data7030, SystemSerialNumber, Draught)

Coef = -1500 / (2 * SampleRate);
rad2deg = 180/pi;

if SystemSerialNumber == 7111
    if isfield(Data7030, 'DRF') && isfield(Data7030.DRF, 'TransmitArrayRoll')
        Teta = Angles - RollTx + Data7030.DRF(1).TransmitArrayRoll*rad2deg; % A v�rifier
    else
        Teta = Angles - RollTx;
    end
else
    Teta = Angles;
end
D = Range * Coef;
Z = D .* cosd(Teta);

if isempty(Data7030.Time)
    %     Z = Z - 5.9; % Pourquoi-Pas?
    Z = Z - Draught; % Pourquoi-Pas?
elseif isfield(Data7030, 'DRF')
    %     Z = Z + Data7030.DRF.ReceiveArrayZ - Data7030.DRF(1).WaterLineVerticalOffset; % -5.9
    Z = Z - Data7030.DRF(1).WaterLineVerticalOffset; % -5.9
else
    %     Z = Z - 5.9; % Pourquoi-Pas?
    Z = Z - Draught; % Pourquoi-Pas?
end

% ----------------
% Heave correction

if isfield(Data7030, 'DRF') % Modif pour fichier 20080219_233937
    Heave = heave_correction(RollTx, PitchTx, HeaveTx, RollRx, PitchRx, HeaveRx, Data7030.DRF(1));
    % figure(6565); plot(Heave); grid on;
    Z = Z + Heave;
end


function Heave = heave_correction(RollTx, PitchTx, HeaveTx, RollRx, PitchRx, HeaveRx, DRF)
rad2deg = 180/pi;
% Heave = HeaveTx + (Pinduit(RollTx+DRF.TransmitArrayRoll*rad2deg, PitchTx+DRF.TransmitArrayPitch*rad2deg, DRF, 1) + HeaveTx + ...
%     Pinduit(RollRx+DRF.ReceiveArrayRoll*rad2deg, PitchRx+DRF.ReceiveArrayPitch*rad2deg, DRF, 2) + HeaveRx) / 2;
Heave = HeaveTx / 2 ...
    + (Pinduit(RollTx+DRF.TransmitArrayRoll*rad2deg, PitchTx+DRF.TransmitArrayPitch*rad2deg, DRF, 1) ...
    + Pinduit(RollRx+DRF.ReceiveArrayRoll*rad2deg, PitchRx+DRF.ReceiveArrayPitch*rad2deg, DRF, 2) + HeaveRx) / 2;
% Heave = HeaveTx + (Pinduit(RollTx, PitchTx, DRF, 1) + HeaveTx + ...
%                    Pinduit(RollRx, PitchRx, DRF, 2) + HeaveRx) / 2;

% Pinduit(0, 0, DRF, 1)

function P = Pinduit(Roll, Pitch, DRF, Mode)
Cx = DRF.MotionSensorX;
Cy = DRF.MotionSensorY;
Cz = DRF.MotionSensorZ;

if Mode == 1 % Tx
    Sx = DRF.TransmitArrayX;
    Sy = DRF.TransmitArrayY;
    Sz = DRF.TransmitArrayZ;
else % 2= Rx
    Sx = DRF.ReceiveArrayX;
    Sy = DRF.ReceiveArrayY;
    Sz = DRF.ReceiveArrayZ;
end

% 1
P = sind(Pitch) * (Sy-Cy) ...
    + cosd(Pitch) .* sind(Roll) * (Sx-Cx) ...
    + cosd(Roll) .* cosd(Pitch) * (Sz-Cz);

% 3
% P = sind(Pitch) * (Sy-Cy) ...
%     - cosd(Pitch) .* sind(Roll) * (Sx-Cx) ...
%     + cosd(Roll) .* cosd(Pitch) * (Sz-Cz);

% Les 2 autres cas ont �t� �limin�s




% function Z = calcul_bathy_simple(Range, SampleRate, Angles, SystemSerialNumber)
function Z = calcul_bathy_simple(Range, SampleRate, Angles, Draught)

Coef = -1500 / (2 * SampleRate);
Teta = Angles;
D = Range * Coef;
Z = D .* cosd(Teta);

% Z = Z - 5.9; % Pourquoi-Pas?
Z = Z - Draught;


function flag = alreadyProcessed(nomFic, nomDirErs)
flag = 0;
if isempty(nomDirErs)
    return
end

[~, nomFicS7k] = fileparts(nomFic);
nomDir = fullfile(nomDirErs, nomFicS7k);
if ~exist(nomDir, 'dir')
    return
end

if ~existErs(nomDir, nomFicS7k, 'PingBeam_Bathymetry')
    return
end
if ~existErs(nomDir, nomFicS7k, 'PingBeam_DetectionType')
    return
end
if ~existErs(nomDir, nomFicS7k, 'PingBeam_MbesQualityFactor')
    return
end
flag = 1;

function flag = existErs(nomDir, nomFicS7k, str)
flag = 0;
if ~exist(fullfile(nomDir, [nomFicS7k '_1_' str]), 'dir')
    return
end
if ~exist(fullfile(nomDir, [nomFicS7k '_1_' str '.xml']), 'file')
    return
end
if ~exist(fullfile(nomDir, [nomFicS7k '_1_' str '.ers']), 'file')
    return
end
flag = 1;
