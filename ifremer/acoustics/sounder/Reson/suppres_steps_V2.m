
function y = suppres_steps_V2(R, Teta)

sub = find(~isnan(R)); 

iBeg = sub(1);
iEnd = sub(1);
N = length(sub);

% if N ~= (sub(end)-sub(1)+1)
%     N
% end

i = 1;
while (i < N)
    while (i < N) && (round(R(sub(i+1))) == round(R(sub((i)))))
        iEnd(end) = sub(i+1);
        i = i + 1;
    end
    if i ~= N
        iBeg(end+1) = sub(i+1); %#ok<AGROW>
        iEnd(end+1) = sub(i+1); %#ok<AGROW>
        i = i + 1;
    end
end
if length(iBeg) == 1
    y = R;
    return
end

iCenter = (iBeg + iEnd) / 2;
%{
figure; plot(R, 'ob'); grid on; hold on; plot(iBeg, R(iBeg), '+r'); plot(iEnd, R(iEnd), 'xg'); plot(iCenter, R(iEnd), 'k')
%}

nx = length(R);
% xTeta = interp1(1:nx, Teta, iCenter);
% Z = interp1(iCenter, R(iBeg).*cosd(xTeta), 1:nx, 'linear', 'extrap');
% y = Z ./ cosd(Teta);

y = interp1(iCenter, R(iBeg), 1:nx, 'linear', 'extrap');

y(1:(sub(1)-1)) = NaN;
y((sub(end)+1):nx) = NaN;
%{
figure; plot(R, 'k'); grid on; hold on; plot(y, 'r');
%}

