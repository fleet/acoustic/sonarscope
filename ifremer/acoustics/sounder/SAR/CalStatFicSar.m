% Calcul de la courbe de moyenne des pixels d'un fichier SAR brut
%
% Syntax
%   moy = CalStatFicSar(nomFic, ...)
%
% Input Arguments
%   nomFic  : nom du fichier
%
% Name-Value Pair Arguments
%   subLig 	: index des lignes a utiliser pour le calcul de la moyenne
%
% Output Arguments
%   moy : Moyenne des pixels
%
% Examples
%   nomFic = getNomFicDatabase('SAR_0035.01.im');
%   moy = CalStatFicSar( nomFic );
%   plot(moy(1,:), 'r'); grid on; hold on; plot(moy(2,:), 'g');
%   title(['Niveaux de gris : ' nomFic ]); hold off;
%
% See also getNbEnrFicSar LecFicSar LecFicAntennes Authors
% Authors : JMA
% VERSION  : $Id: CalStatFicSar.m,v 1.4 2002/06/20 10:23:01 augustin Exp $
%------------------------------------------------------------------------------

function moy = CalStatFicSar( nomFic, varargin )

if nargin == 0
    help CalStatFicSar
    return
end


% ---------------
% Initialisations

nbOcFic = sizeFic( nomFic );
if nbOcFic == -1
    return;
end
nb_oc_enr = 7644;
nbEnr = nbOcFic / nb_oc_enr;
subLig = 1:nbEnr;
nomFicOut = [];

for i = 1:length(varargin)
    if isnumeric(varargin{i}) % C'est l'echantillonage des enregistrements
        subLig = varargin{i};
    end
end

% Ouverture du fichier
% --------------------

fidIn = fopen(nomFic, 'r');
if ~isempty( nomFicOut )
    fidOut =  fopen(nomFicOut, 'w+');
end


sub = find(subLig <= nbEnr);
subLig = subLig(sub);

nLig = size(subLig, 2);

HB = zeros(1, nLig);
HT = zeros(1, nLig);
Immersion = zeros(1, nLig);
Cap = zeros(1, nLig);
Vitesse = zeros(1, nLig);

% Lecture des infos
% -----------------

meanReflecAngulaireB = zeros(1, 900);
nbReflecAngulaireB = zeros(1, 900);

meanReflecAngulaireT = zeros(1, 900);
nbReflecAngulaireT = zeros(1, 900);

meanReflecTempsB = zeros(1, 3000);
nbReflecTempsB = zeros(1, 3000);

meanReflecTempsT = zeros(1, 3000);
nbReflecTempsT = zeros(1, 3000);

%d = (1:3000) * 0.25;
%ARkm = 2 * d / 1000;	% Trajet Aller/Retour en km

for i=1:nLig

    % Calcul de la position du debut de l'enregistrement
    % --------------------------------------------------

    position = (subLig(i) - 1) * nb_oc_enr;

    % Lecture de l'enregistrement
    % ---------------------------

    status = fseek( fidIn, position, 'bof');
    enr = (fread( fidIn, nb_oc_enr, 'char'))';

    % Recuperation des donnees
    % ------------------------

    HB(i) = str2double(char(enr(33:38)));
    HT(i) = str2double(char(enr(40:45)));
    charDate = char(enr(190:197));
    date = dayStr2Ifr( sprintf('%s19%s',charDate(1:6), charDate(7:8)) );
    charHeure = char(enr(199:210));
    heure = hourStr2Ifr( charHeure );

    switch char(enr(213))	% Il y a un probleme de decalage d'1 octet dans les fichiers
        case ','
            sublat = 212:224;
            sublon = 226:239;
        otherwise
            sublat = 213:224;
            sublon = 227:239;
    end
    Immersion(i) = str2double(char(enr(241:249)));
    Cap(i) = str2double(char(enr(281:285)));

    NIb = enr(792:3791);
    NIt = enr(3832:6831);
    Sondeur = enr(6871:6871+760-1);

    % Initialisations
    % ---------------

    IH = HB(i) / 0.25;
    NIb(1:IH-1) = NaN;
    NIt(1:IH-1) = NaN;


    % Calcul des statistiques
    % -----------------------

    sub = find(~isnan(NIb));
    meanReflecTempsB(sub) = meanReflecTempsB(sub) + NIb(sub);
    nbReflecTempsB(sub) = nbReflecTempsB(sub) + 1;

    sub = find(~isnan(NIt));
    meanReflecTempsT(sub) = meanReflecTempsT(sub) + NIt(sub);
    nbReflecTempsT(sub) = nbReflecTempsT(sub) + 1;


    % Affichage
    % ---------

    if(mod(i, 10) == 0)
         fprintf('%d/%d %d %s %s HB=%fm HT=%fm I=%f C=%f V=%f\n', ...
            i, nLig, subLig(i), charDate, charHeure, HB(i), HT(i), Immersion(i), Cap(i), Vitesse(i) ) ;
    end
end

% Fermeture du fichier
% --------------------

fclose(fidIn);

% Calcul des courbes de  relectivite moyenne
% ------------------------------------------

sub = find( nbReflecTempsB ~= 0 );
meanReflecTempsB(sub) = meanReflecTempsB(sub) ./ nbReflecTempsB(sub);
sub = find( nbReflecTempsT ~= 0 );
meanReflecTempsT(sub) = meanReflecTempsT(sub) ./ nbReflecTempsT(sub);

moy = [meanReflecTempsB; meanReflecTempsT];
