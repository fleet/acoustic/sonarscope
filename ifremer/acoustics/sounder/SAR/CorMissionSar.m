% Correction de la reflectivite sur toute une mission SAR
%
% Syntax
%   CorMissionSar( nomDirIn, nomDirOut, sub, Z, T, S )
%
% Input Arguments 
%   nomDirIn  : Nom generique des fichiers d'entree
%   nomDirOut : Nom generique des fichiers de sortie
%   sub 	  : Liste des profils a traiter
%   Z, T, S   : Profil de thermo-salinometrie, Z en m, T en deg, S en 0/00
%
% Examples
%   nomFic = getNomFicDatabase('BATHYCEL_North-East_Atlantic_Winter.csv');
%   [Z, S, T] = lecCsv(nomFic);
%
%   nomDir = fileparts(getNomFicDatabase('SAR_0035.01.im'))
%   CorMissionSar(nomDir, my_tempdir, 35, Z, T, S);
%
% See also getNbEnrFicSar LecFicSar LecFicAntennes Authors
% Authors : JMA
% VERSION  : $Id: CorMissionSar.m,v 1.3 2002/06/20 10:23:01 augustin Exp $
%------------------------------------------------------------------------------

function CorMissionSar(nomDirIn, nomDirOut, sub, Z, T, S)

for i=1:size(sub, 2)
	for k=1:99
		nomFicIN  = fullfile(nomDirIn,  sprintf('%04d.%02d.im', sub(i), k));
		nomFicOut = fullfile(nomDirOut, sprintf('%04d.%02d.im', sub(i), k));
		if exist(nomFicIN)
			if i ~= 1
				close;
                close;
                close;
			end
			fprintf('Processing file : %s\n', nomFicIN);
			CorFicSar( nomFicIN, Z, T, S, nomFicOut );
		end
	end
end
