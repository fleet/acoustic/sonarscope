% Lecture d'un fichier SAR brut
%
% Syntax
%   EcrFicSarSpeedy(nomFic, BT...)
%
% Input Arguments
%   nomFic : Nom du fichier
%   BT     : Donnees sonar
%
% Name-Value Pair Arguments
%   subl  : index des lignes a extraire
%
% Output Arguments
%
% Examples
%
% See also LecFicSarSpeedy Authors
% Authors : JMA
%------------------------------------------------------------------------------

function EcrFicSarSpeedy(nomFic, BT, varargin)

if ~exist(nomFic)
    fprintf('Le fichier %s n''existe pas\n', nomFic)
    return
end

% -------------------------------------
% Lecture du nombre d'octets du fichier

nbOcFic = sizeFic(nomFic);
if nbOcFic == -1
    return
end

% ---------------
% Initialisations

nb_oc_enr = 7644;
nbEnr     = nbOcFic / nb_oc_enr;

[varargin, subl] = getPropertyValue(varargin, 'subl', 1:nbEnr); %#ok<ASGLU>

if length(subl) ~= 1
    pas = unique(diff(subl));
    if length(pas) ~= 1
        disp('extraction contigue SVP')
        return
    end
    if pas ~= 1
        disp('extraction contigue SVP')
        return
    end
end

% --------------------
% Ouverture du fichier

fidIn = fopen(nomFic, 'r+');

% ---------------------------------------------------
% Positionnement sur le premier enregistrement a lire

position = (subl(1) - 1) * nb_oc_enr;
status   = fseek( fidIn, position, 'bof');

% ----------------------------------------
% Lecture du bloc d'enregistrements a lire

enr = (fread( fidIn, [nb_oc_enr length(subl)], 'char'))';

% -------------------------
% Desassemblage des donnees

enr(:, 3832:6831) = fliplr(BT(:,1:3000));
enr(:, 792:3791)  = BT(:,3001:end);

% ------------------------------------------
% Repositionnement en debut d'enregistrement

status = fseek( fidIn, position, 'bof');

% ----------------------------
% Ecriture de l'enregistrement

fwrite( fidIn, enr', 'char');

% --------------------
% Fermeture du fichier

fclose(fidIn);

