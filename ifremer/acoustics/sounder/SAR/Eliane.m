% Fichier a Eliane : depouillement mission MARMARA
%
% See also getNbEnrFicSar LecFicSar LecFicAntennes Authors
% Authors : JMA
%------------------------------------------------------------------------------

nomFic = '/home1/doppler/augustin/Campagnes/Sar/0004.02.im';
N = getNbEnrFicSar(nomFic);
pas = floor(N/100);
t = LecFicSar( nomFic, 't', 1:pas:N ); %#ok<NASGU>

BTMARMARA = LecFicSar( nomFic, 'BT', 2700:10:5000 );
imagesc(-BTMARMARA, [-15, 0]); colorbar; colormap(jet(256)); title('Image Brute')
colormap(gray(256)) ;

mMARMARA = mean(BTMARMARA);
figure;
plot(mMARMARA); grid on; title('Image Brute')
figure;
plot(reflec_Enr2dB(mMARMARA)); grid on; title('mMARMARA')


latitude = 40.76;
longitude = 29.00;
nomFic = '/home/rayleigh/tmsias/augustin/temp.mat';
lecLevitusMat( nomFic, latitude, longitude, 2)

Z = -1250;
T = 9.12;
S = 38;
CorFicSar( '/home1/doppler/augustin/Campagnes/Sar/0004.02.im', Z, T, S , '/home1/doppler/augustin/Campagnes/Sar/1004.02.im');

alphaBabMaison = AttenuationGarrison( 170, Z, T, S); %#ok<NASGU>
alphaTriMaison = AttenuationGarrison( 190, Z, T, S); %#ok<NASGU>

BTCor = zeros(size(MARMARA));
for i=1:size(BT, 1)
	BTCor(i,:) = MARMARA(i,:) - m;
end
figure
imagesc(-BTCor , [-10 10]); colorbar; colormap(gray(256)); title('Image Brute')

mCor=mean(BTCor);
figure;
plot(mCor); grid on; title('Image Corrigee')



% ----------------------------------------------------------------
% ----------------------------------------------------------------
% Mission ZAIANGO SAR

nomFic = '/home1/doppler/augustin/Campagnes/Sar/0056.02.im';
N = getNbEnrFicSar(nomFic);
pas = floor(N/100);
t = LecFicSar( nomFic, 't', 1:pas:N );

BTZAIANGO = LecFicSar( nomFic, 'BT', 1000:10:3100 );
imagesc(-BTZAIANGO, [-30, 0]); colorbar; colormap(gray(256)); title('Image Brute')

mZAIANGO = mean(BTZAIANGO);
figure;
plot(mZAIANGO); grid on; title('Image Brute')
figure;
plot(reflec_Enr2dB(mZAIANGO)); grid on; title('mZAIANGO')

latitude = -7.29;
longitude = 12.00;
nomFic = '/home/rayleigh/tmsias/augustin/temp.nc';
lecLevitus( nomFic, latitude, longitude, 1)

Z1 = -350;
Z2 = -475;
T1 = 9.5;
T2 = 7.5;
S = 35;

alphaBabMaison1 = AttenuationGarrison( 170, Z1, T1, S);
alphaTriMaison1 = AttenuationGarrison( 190, Z1, T1, S);
alphaBabMaison = AttenuationGarrison( 170, Z2, T2, S);
alphaTriMaison = AttenuationGarrison( 190, Z2, T2, S);


BTCor = zeros(size(BT));
for i=1:size(BT, 1)
	BTCor(i,:) = BT(i,:) - m;
end
figure
imagesc(-BTCor , [-10 10]); colorbar; colormap(gray(256)); title('Image Brute')

mCor=mean(BTCor);
figure;
plot(mCor); grid on; title('Image Corrigee')


% ----------------------------------------------------------------
% ----------------------------------------------------------------

% Comparaison de MARMARA et ZAIANGO

figure
plot(mZAIANGO ./ mMARMARA); grid on; title('mZAIANGO - mMARMARA')

figure
plot(reflec_Enr2dB(mZAIANGO) - reflec_Enr2dB(mMARMARA)); grid on; title('mZAIANGO - mMARMARA')


% ----------------------------------------------------------------
% ----------------------------------------------------------------

% Lecture des eprom

% ----------------------------------------------------
% Lecture et affichage des lois de TVG De l'eprom Nu 1

figure
[D, a0, DeltaG, LimiteGMax] = getParamEprom(1, 1:8);
TVG = TvgSar(D, a0, DeltaG, LimiteGMax);
plot(TVG'); grid on;

figure
plot(TVG(8,:)); grid on;

% ----------------------------------------------------
% Lecture et affichage des lois de TVG De l'eprom Nu 2

figure
[D, a0, DeltaG, LimiteGMax] = getParamEprom(2, 1:8);
TVG = TvgSar(D, a0, DeltaG, LimiteGMax);
plot(TVG'); grid on;

figure
plot(TVG(8,:)); grid on;

% ----------------------------------------------------
% Lecture et affichage des lois de TVG De l'eprom Nu 3

figure
[D, a0, DeltaG, LimiteGMax] = getParamEprom(3, 1:8);
TVG = TvgSar(D, a0, DeltaG, LimiteGMax);
plot(TVG'); grid on;

figure
plot(TVG(8,:)); grid on;

% ----------------------------------------------------
% Lecture et affichage des lois de TVG De l'eprom Nu 4

figure
[D, a0, DeltaG, LimiteGMax] = getParamEprom(4, 1:8);
TVG = TvgSar(D, a0, DeltaG, LimiteGMax);
plot(TVG'); grid on;

figure
plot(TVG([2 6 8],:)'); grid on;
