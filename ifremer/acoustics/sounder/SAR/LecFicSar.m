% Lecture et traitement d'un fichier SAR brut
%
% Syntax
%   [...] = LecFicSar( nomFic, options )
%
% Input Arguments 
%   nomFic  : Nom du fichier
%
% Name-Value Pair Arguments 
%   'B'	     : lecture de voie babord
%   'T'	     : lecture de voie tribord
%   'BT'     : lecture des voies babord et tribord
%   'S'	     : lecture du sondeur de sediment
%   'HB'     : Hauteur calculee a babord (m)
%   'HT'     : Hauteur calculee a tribord (m)
%   'HB'     : Hauteur calculee a babord (m)
%   'I'      : Immersion (m)
%   'C'      : Cap (deg)
%   'V'      : Vitesse (m/s)
%   't'      : Date et heure
%   'subLig' : index des lignes a extraire
%   'stats'  : Affichage des stats
%   'Lat'    : Latitudes
%   'Lon'    : Longitudes
%
% Output Arguments
%   Varaiables au nombre et dans l'ordre des parametres optionnels demandes
%
% Examples
%   nomFic = getNomFicDatabase('SAR_0035.01.im')
%   B = LecFicSar(nomFic, 'B', 1:500);
%     fig = figure; imagesc(-B); colorbar; colormap(gray(256));
%
%   [T, HB, HT] = LecFicSar(nomFic, 'T', 'HB', 'HT', 1:500);
%     figure; imagesc(-T); colorbar; axis xy; colormap(gray(256));
%     hold on; plot(HB*4, 1:size(HB, 2),'r'); plot(HB*4, 1:size(HB, 2),'g');
%
%   [BT, HB, HT] = LecFicSar(nomFic, 'BT', 'HB', 'HT', 1:500);
%     fig = figure; imagesc(-BT); colorbar; axis xy; colormap(gray(256));
%     hold on; plot(3000-HB*4, 1:size(HB, 2),'r'); plot(3000+HT*4, 1:size(HB, 2),'g');
%
%   S = LecFicSar(nomFic, 'S', 1:500);
%     fig = figure; imagesc(-S); colorbar; axis xy; colormap(gray(256));
%     figure; imagesc(S, [0,50]); colorbar; colormap(gray(256));
%
% See also Ore2Sar navOre2ascii Oretech/chantier LecFicSar CorFicSar Authors
% Authors : JMA
%------------------------------------------------------------------------------

function varargout = LecFicSar(nomFic, varargin)

%% Lecture du nombre d'octets du fichier

nbOcFic = sizeFic(nomFic);
if nbOcFic == -1
    return;
end

%% Initialisations

nb_oc_enr = 7644;
IndiceB = 0;
IndiceT = 0;
IndiceBT = 0;
IndiceS = 0;
IndiceHB = 0;
IndiceHT = 0;
IndiceI = 0;
IndiceC = 0;
IndiceV = 0;
Indicet = 0;
IndiceLat = 0;
IndiceLon = 0;
nbEnr = nbOcFic / nb_oc_enr;
stats = 0;

disp(sprintf('Nombre d''enregistrements : %d', nbEnr)); %#ok<DSPS>

subLig = 1:nbEnr;

indiceOut = 1;
for i = 1:length(varargin)
    if( strcmp(varargin{i}, 'B'))
        IndiceB = indiceOut;
        indiceOut = indiceOut + 1;
    elseif( strcmp(varargin{i}, 'T'))
        IndiceT = indiceOut;
        indiceOut = indiceOut + 1;
    elseif( strcmp(varargin{i}, 'BT'))
        IndiceBT = indiceOut;
        indiceOut = indiceOut + 1;
    elseif( strcmp(varargin{i}, 'S'))
        IndiceS = indiceOut;
        indiceOut = indiceOut + 1;
    elseif( strcmp(varargin{i}, 'HB'))
        IndiceHB = indiceOut;
        indiceOut = indiceOut + 1;
    elseif( strcmp(varargin{i}, 'HT'))
        IndiceHT = indiceOut;
        indiceOut = indiceOut + 1;
    elseif( strcmp(varargin{i}, 'I'))
        IndiceI = indiceOut;
        indiceOut = indiceOut + 1;
    elseif( strcmp(varargin{i}, 'C'))
        IndiceC = indiceOut;
        indiceOut = indiceOut + 1;
    elseif( strcmp(varargin{i}, 'V'))
        IndiceV = indiceOut;
        indiceOut = indiceOut + 1;
    elseif( strcmp(varargin{i}, 't'))
        Indicet = indiceOut;
        indiceOut = indiceOut + 1;
    elseif( strcmp(varargin{i}, 'Lat'))
        IndiceLat = indiceOut;
        indiceOut = indiceOut + 1;
    elseif( strcmp(varargin{i}, 'Lon'))
        IndiceLon = indiceOut;
        indiceOut = indiceOut + 1;
    elseif( strcmp(varargin{i}, 'stats'))
        stats = 1;
    elseif isnumeric(varargin{i})	% C'est l'echantillonage des enregistrements
        subLig = varargin{i};
    end
end

%% Ouverture du fichier

fidIn = fopen(nomFic, 'r');

sub = (subLig <= nbEnr);
subLig = subLig(sub);

nLig = size(subLig, 2);

if IndiceB ~= 0
    B = zeros(nLig, 3000);
end
if IndiceT ~= 0
    T = zeros(nLig, 3000);
end
if IndiceBT ~= 0
    BT = zeros(nLig, 6000);
end
if IndiceS ~= 0
    S = zeros(nLig, 760);
end
if Indicet ~= 0
    t = zeros(2, nLig);
end

HB = zeros(1, nLig);
HT = zeros(1, nLig);
Immersion = zeros(1, nLig);
Cap = zeros(1, nLig);
Vitesse = zeros(1, nLig);
latitude = zeros(1, nLig);
longitude = zeros(1, nLig);

%% Lecture des infos

meanReflecAngulaireB = zeros(1, 900);
nbReflecAngulaireB = zeros(1, 900);

meanReflecAngulaireT = zeros(1, 900);
nbReflecAngulaireT = zeros(1, 900);

meanReflecTempsB = zeros(1, 3000);
nbReflecTempsB = zeros(1, 3000);

meanReflecTempsT = zeros(1, 3000);
nbReflecTempsT = zeros(1, 3000);

%d = (1:3000) * 0.25;
%ARkm = 2 * d / 1000;	% Trajet Aller/Retour en km

for i=1:nLig
    
    %% Calcul de la position du debut de l'enregistrement
    
    position = (subLig(i) - 1) * nb_oc_enr;
    
    %% Lecture de l'enregistrement
    
    status = fseek( fidIn, position, 'bof'); %#ok<NASGU>
    enr = (fread( fidIn, nb_oc_enr, 'char'))';
    
    %% R�cuperation des donn�es
    
    HB(i) = str2double(char(enr(33:38)));
    HT(i) = str2double(char(enr(40:45)));
    charDate = char(enr(190:197));
    
    annee = str2num(charDate(7:8)); %#ok<ST2NM>
    if annee > 80
        siecle = 19;
    else
        siecle = 20;
    end
    
    date = dayStr2Ifr( sprintf('%s%d%s', charDate(1:6), siecle, charDate(7:8)) );
    charHeure = char(enr(199:210));
    heure = hourStr2Ifr( charHeure );
    
    if Indicet ~= 0
        t(1, i) = date;
        t(2, i) = heure;
    end
    
    switch char(enr(213))	% Il y a un probleme de decalage d'1 octet dans les fichiers
    case ','
        sublat = 212:224;
        sublon = 226:239;
    otherwise
        sublat = 213:224;
        sublon = 227:239;
    end
    latitude(i) = str2lat(char(enr(sublat)));
    longitude(i) = str2lon(char(enr(sublon)));
    Immersion(i) = str2double(char(enr(241:249)));
    Cap(i) = str2double(char(enr(281:285)));
    
    reflecB = enr(792:3791);
    reflecT = enr(3832:6831);
    Sondeur = enr(6871:6871+760-1);
    
%     IH = HB(i) / 0.25;
    %reflecB(1:IH-1) = 0;
    %reflecT(1:IH-1) = 0;
    
    %reflecB = reflec_Enr2dB(reflecB);
    %reflecT = reflec_Enr2dB(reflecT);
    
    %% Initialisations
    
    angle_pixels = calAngle(HB(i));
    iAngle = 1 + floor(angle_pixels * 10);
    iAngle(isnan(iAngle)) = 1;
    
    [nuprom, nuLoi, GainFixeBab, GainFixeTri, ShBab, ShTri] = getNuLoi(date, heure); %#ok<ASGLU>
    
    %% Calcul des statistiques voie babord
    
    sub = find(~isnan(reflecB));
    meanReflecAngulaireB(iAngle(sub)) = meanReflecAngulaireB(iAngle(sub)) + reflecB(sub);
    nbReflecAngulaireB(iAngle(sub)) = nbReflecAngulaireB(iAngle(sub)) + 1;
    
    meanReflecTempsB(sub) = meanReflecTempsB(sub) + reflecB(sub);
    nbReflecTempsB(sub) = nbReflecTempsB(sub) + 1;
    
    if IndiceB ~= 0
        B(i,:) = reflecB;
    end
    
    %% Calcul des statistiques voie tribord
    
    sub = find(~isnan(reflecT));
    meanReflecAngulaireT(iAngle(sub)) = meanReflecAngulaireT(iAngle(sub)) + reflecT(sub);
    nbReflecAngulaireT(iAngle(sub)) = nbReflecAngulaireT(iAngle(sub)) + 1;
    
    meanReflecTempsT(sub) = meanReflecTempsT(sub) + reflecT(sub);
    nbReflecTempsT(sub) = nbReflecTempsT(sub) + 1;
    
    if IndiceT ~= 0
        T(i,:) = reflecT;
    end
    
    %% Lecture des 2 voies
    
    if( IndiceBT ~= 0)
        BT(i,3001:6000 ) = reflecB;
        BT(i,1:3000) = fliplr(reflecT);
    end
    
    %% Lecture de la voie sondeur
    
    if IndiceS ~= 0
        S(i,:) = Sondeur;
    end
    
    %% Affichage
    
    if (mod(i, 10) == 1) || (i == nbEnr)
        disp( sprintf('%d/%d %d %s %s HB=%fm HT=%fm I=%f C=%f V=%f nuLoi=%d GFb=%f GFt=%f', ...
            i, nLig, subLig(i), charDate, charHeure, HB(i), HT(i), Immersion(i), Cap(i), Vitesse(i), nuLoi, GainFixeBab, GainFixeTri ) ); %#ok<DSPS>
    end
end

%% Fermeture du fichier

fclose(fidIn);

if stats
    
    %% Calcul des courbes de  r�flectivit� moyenne
    
    sub = find( nbReflecAngulaireB ~= 0 );
    meanReflecAngulaireB(sub) = meanReflecAngulaireB(sub) ./ nbReflecAngulaireB(sub); %#ok<NASGU>
    
    sub = find( nbReflecAngulaireT ~= 0 );
    meanReflecAngulaireT(sub) = meanReflecAngulaireT(sub) ./ nbReflecAngulaireT(sub); %#ok<NASGU>
    
    sub = find( nbReflecTempsB ~= 0 );
    meanReflecTempsB(sub) = meanReflecTempsB(sub) ./ nbReflecTempsB(sub);
    meanReflecTempsT(sub) = meanReflecTempsT(sub) ./ nbReflecTempsT(sub);
    
    
    
    %% Trace de la navigation
    
    figure(9999);
    plot(longitude, latitude); hold on; grid on; title('Navigation');
    
    %% Trac� de la r�flectivit� angulaire
    
    %figure;
    %angles = (1:900)/10;
    %plot(angles, meanReflecAngulaireB, 'r'); hold on;
    %plot(angles, meanReflecAngulaireT, 'g'); grid on;
    %title(nomFic);
    
    % Trace de la reflectivite angulaire
    
    %gainMin = min([meanReflecAngulaireB meanReflecAngulaireT]);
    %figure;
    %angles = (1:900)/10;
    %polar((pi / 180) * (-angles - 90), meanReflecAngulaireB - gainMin, 'r'); hold on;
    %polar((pi / 180) * ( angles - 90), meanReflecAngulaireT - gainMin, 'g');
    %title(nomFic);
    
    %% Trace de la reflectivite en fonction du temps
    
    figure;
    plot(meanReflecTempsB, 'r'); hold on;
    plot(meanReflecTempsT, 'g'); grid on;
    title(nomFic);
    
end

%% Sortie des param�tres

if IndiceB ~= 0
    varargout{IndiceB} = B;
end

if IndiceT ~= 0
    varargout{IndiceT} = T;
end

if IndiceBT ~= 0
    varargout{IndiceBT} = BT;
end

if IndiceS ~= 0
    varargout{IndiceS} = S;
end

if IndiceHB ~= 0
    varargout{IndiceHB} = HB;
end

if IndiceHT ~= 0
    varargout{IndiceHT} = HT;
end

if IndiceI ~= 0
    varargout{IndiceI} = Immersion;
end

if IndiceC ~= 0
    varargout{IndiceC} = Cap;
end

if IndiceV ~= 0
    varargout{IndiceV} = Vitesse;
end

if IndiceLat ~= 0
    varargout{IndiceLat} = latitude;
end

if IndiceLon ~= 0
    varargout{IndiceLon} = longitude;
end

if Indicet ~= 0
    varargout{Indicet} = t;
end


%% Calcul de l'angle d'emission pour une hauteur H (hypothese fond plat)

function angles = calAngle( H )

if H == 0
    angles = NaN(1,3000);
    return
end

id = floor(H / 0.25);
d = (1:3000) * 0.25;
d(1:id) = NaN;
angles = (180 / pi ) * acos(H ./ d);
