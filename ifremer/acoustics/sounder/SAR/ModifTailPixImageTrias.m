% Modification de la taille du pixel d'une image TRIAS. !! PB DATA
%
% Syntax
%   ModifTailPixImageTrias(nomFic, TaillePixelEnMetres)
%
% Input Arguments 
%   nomFic              : Nom du fichier
%	TaillePixelEnMetres : taille du pixel en metres
%
% Examples
%   ModifTailPixImageTrias( '/tmp/pppp.imo', 10 );
%
% See also LecFicImageTrias Authors
% Authors : JMA
% VERSION  : $Id: ModifTailPixImageTrias.m,v 1.3 2002/06/20 10:23:01 augustin Exp $
%------------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HISTORIQUE DEVELOPPEMENT
%   02/11/00 - JMA - Creation
%   02/04/01 - JMA - Mise en ref.
% ----------------------------------------------------------------------------

function ModifTailPixImageTrias(nomFic, TaillePixelEnMetres)

if nargin == 0
    help ModifTailPixImageTrias
    return
end

fid = fopen(nomFic, 'r+');
entete = fread(fid, 32, 'int32');
fprintf('Taille pixel du fichier avant modofication : %f m\n', entete(22) / 1000);
entete(22) = TaillePixelEnMetres * 1000;
fseek(fid, 0, 'bof');
fwrite(fid, entete, 'int32');
fclose(fid);
