function flag = S7K_WCExport3DViewer(nomFic, nomDirOut, Tag, CLim, TailleMax, Side, ColormapIndex, Video)
 
if ~iscell(nomFic)
    nomFic = {nomFic};
end

flag = exist(nomDirOut, 'dir');
if ~flag
    str1 = sprintf('Le r�pertoire "%s" n''existe pas.', nomDirOut);
    str2 = sprintf('Directory "%s"does not exist.', nomDirOut);
    my_warndlg(Lang(str1,str2), 1);
    return
end

Fig   = [];
Carto = [];
N = length(nomFic);
str1 = 'Export de la colonne d''eau des fichiers S7K dans GLOBE';
str2 = 'Exporting S7K Water Column images to GLOBE';
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    [flag, Carto, Fig] = S7K_WCExport3DViewer_unitaire(nomFic{k}, nomDirOut, Tag, CLim, TailleMax, Side, ColormapIndex, Video, Carto, Fig);
    if ~flag
        % Message SVP
    end
end
my_close(hw, 'MsgEnd')



function [flag, Carto, Fig] = S7K_WCExport3DViewer_unitaire(nomFic, nomDirOut, Tag, CLim, TailleMax, Side, ColormapIndex, Video, Carto, Fig)

flag = 0;

%% Pr�caution car soft mal branl�? a supprimer apr�s int�gration travaux Roger

prepare_lectures7k_step1(nomFic)

%% Cr�ation de l'instance cl_reson_s7k

s7k = cl_reson_s7k('nomFic', nomFic, 'KeepNcID', true);
[a, Carto] = view_SidescanImagePDS(s7k, 'Carto', Carto);
if isempty(a)
    return
end

%% Extraction de la colonne d''eau

[flag, b] = Sidescan_ExtractWaterColumn(a, Side);
if ~flag
    return
end

%% Cr�ation du r�pertoire o� seront sauv�s les fichiers

[~, nomDirBin] = fileparts(nomFic);
if isempty(Tag)
    nomDirXml = fullfile(nomDirOut, [nomDirBin '_WaterColumn']);
else
    nomDirXml = fullfile(nomDirOut, [nomDirBin '_WaterColumn_' Tag]);
end

NomFicXML = [nomDirXml '.xml'];

%% Cr�ation des figures des images avec immersion constante

[flag, c] = subbottom_ImmersionConstante(b);
if ~flag
    return
end
c.ColormapIndex = 3;

if Side == 1
    NomFig = [nomDirBin '_Port'];
else
    NomFig = [nomDirBin '_Starbord'];
end
if ~isempty(Tag)
    NomFig = [nomDirBin '_' Tag];
end 
nomFicFigure = fullfile(nomDirOut, [NomFig '.fig']);
NomFicJpg    = fullfile(nomDirOut, [NomFig '.jpg']);
Rot90      = 1;
% NomY       = 'y';
NomY       = 'DistanceAlongNavigation';
TypeWindow = 1;

my_close(Fig)
Fig = imagesc(c, 'Rot90', (Rot90 == 1), 'NomY', NomY, 'TypeWindow', TypeWindow, 'CLim', CLim);
axis equal, axis tight

print(Fig, NomFicJpg, '-djpeg');

[~, NumVer] = VersionsMatlab;
if NumVer > 7.13
    savefig(Fig, nomFicFigure)
    str1 = sprintf('La figure "%s" a �t� sauv�e dans "%s"', NomFig, nomFicFigure);
    str2 = sprintf('The figure "%s" was saved in "%s"', NomFig, nomFicFigure);
    my_warndlg(Lang(str1,str2), 0, 'Tag', 'SaveFigOnDisk');
else
    str1 = 'Cette version matlab ne permet pas de sauver la figure automatiquement.';
    str2 = 'It is not possible to save the figure with this version of Matlab.';
    my_warndlg(Lang(str1,str2), 0, 'Tag', 'SaveFigOnDisk');
end

%% Export vers GLOBE

b.CLim          = CLim;
b.ColormapIndex = ColormapIndex;
b.Video         = Video;
typeOutput = 1; % XML-Bin
flag = sonar_Export3DV_ImagesAlongNavigation_ByBlocks(b, NomFicXML, TailleMax, typeOutput, 'Mute');
if ~flag
    return
end

