function flag = SAR_BackupSignals(nomFic, nomSignal)
 
if ~iscell(nomFic)
    nomFic = {nomFic};
end
 
if ~iscell(nomSignal)
    nomSignal = {nomSignal};
end


for k=1:length(nomFic)
    flag = SAR_BackupSignals_unitaire(nomFic{k}, nomSignal);
    if ~flag
        % Message SVP
    end
end

str1 = 'La sauvegarde des signaux originaux du SAR est termin�e.';
str2 = 'The "save as original SAR signals" is over.';
my_warndlg(Lang(str1,str2), 0);


function flag = SAR_BackupSignals_unitaire(nomFic, nomSignal)

flag = Sar2ssc(nomFic);
if ~flag
    return
end

[flag, Data] = SSc_ReadDatagrams(nomFic, 'Ssc_Sar', 'Memmapfile', -1);
if ~flag
    return
end

Data.OriginTime      = [];
Data.OriginLatitude  = [];
Data.OriginLongitude = [];
Data.OriginImmersion = [];
Data.OriginHeading   = [];
Data.OriginSpeed     = [];
Data.OriginHeight    = [];
       
for k=1:length(nomSignal)
    SignalName = nomSignal{k};
    switch SignalName
        case 'Time'
            Time = Data.Time.timeMat;
            flag = save_signal(nomFic, 'Ssc_Sar', 'OriginTime', Time);
            if ~flag
                return
            end
            
        otherwise
            flag = save_signal(nomFic, 'Ssc_Sar', [SignalName 'Origin'], Data.(SignalName)(:));
            if ~flag
                return
            end
    end
end
