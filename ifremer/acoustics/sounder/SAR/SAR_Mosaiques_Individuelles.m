% [flag, listeFicSAR, lastDir] = uiSelectFiles('ExtensionFiles', '.im', 'RepDefaut', 'I:\Titanic\Donnees')
% gridSize = 0.75
% SAR_Mosaiques_Individuelles(listeFicSAR, gridSize)

function SAR_Mosaiques_Individuelles(repExport, listeFicSAR, gridSize, CorFile, selectionData)
   
if isempty(listeFicSAR)
    return
end

I0 = cl_image_I0;

N = length(listeFicSAR);
str1 = 'Calcul des mosa�ques individuelles.';
str2 = 'Computing individual mosaics.';
hw = create_waitbar(Lang(str1,str2), 'N', N);
for iFic=1:N
    nomFic = listeFicSAR{iFic};
    
    my_waitbar(iFic, N, hw)
    fprintf('%s\n%d / %d : %s\n', repmat('-',1,100), iFic, N, nomFic);
    
    if ~exist(listeFicSAR{iFic}, 'file')
        str1 = sprintf('Le fichier "%s" n''existe pas.', listeFicSAR{iFic});
        str2 = sprintf('"%s" does not exist.', listeFicSAR{iFic});
        my_warndlg(Lang(str1,str2), 0);
        continue
    end
    
    [flag, Reflec_PingSamples] = import_SAR(I0, nomFic, 'subImage', 2);
    if ~flag
        continue
    end
             
    if selectionData == 1 % ReflectivityFromSnippets
        nomDataType = 'Reflectivity';
    else % Bathymetry
        nomDataType = 'Bathymetry';
    end
        
     for k=1:length(CorFile)
         CourbeConditionnelle = load_courbesStats(CorFile{k});
         bilan = CourbeConditionnelle.bilan;
         if strcmp(bilan{1}.GeometryType, 'PingSamples')
             Angle_PingSamples = sonar_lateral_emission(Reflec_PingSamples); %, 'useRoll', useRoll);
             [Reflec_PingSamples, flag] = compensationCourbesStats(Reflec_PingSamples, Angle_PingSamples, bilan);
             if ~flag
                 return
             end
         end
     end
     
     Reflec_PingSamples = set(Reflec_PingSamples, 'SonarResolutionX', gridSize);
     Reflec_PingAcrossDist = sonarLateral_PingRange2PingAcrossDist(Reflec_PingSamples, 'resolutionX', gridSize);
     Angle_PingAcrossDist = sonar_lateral_emission(Reflec_PingAcrossDist(1)); %, 'useRoll', useRoll);
     
     for k=1:length(CorFile)
         CourbeConditionnelle = load_courbesStats(CorFile{k});
         bilan = CourbeConditionnelle.bilan;
         if strcmp(bilan{1}.GeometryType, 'PingAcrossDist')
             [Reflec_PingAcrossDist, flag] = compensationCourbesStats(Reflec_PingAcrossDist, Angle_PingAcrossDist, bilan);
             if ~flag
                 return
             end
         end
     end
     
     [flag, LatLon_PingAcrossDist] = sonarCalculCoordGeo(Reflec_PingAcrossDist, 1);
     if ~flag
         return
     end
     Lat = LatLon_PingAcrossDist(1);
     Lon = LatLon_PingAcrossDist(2);
     clear LatLon_PingAcrossDist
     
     [flag, Reflec_LatLong] = sonar_mosaique(Reflec_PingAcrossDist(1), ...
         Lat, Lon, gridSize, ...
         'AcrossInterpolation', 2, ...
         'AlongInterpolation',  2, ...
         'MethodeRemplissage', 1);
     if ~flag
         continue
     end
     
     [flag, Angle_LatLong] = sonar_mosaique(Angle_PingAcrossDist, ...
         Lat, Lon, gridSize, ...
         'AcrossInterpolation', 2, ...
         'AlongInterpolation',  2, ...
         'MethodeRemplissage', 1);
     if ~flag
         continue
     end
    
    InitialImageName = strrep(Reflec_LatLong.InitialImageName, '_Compensation_Compensation', '_Compensation');
    
    InitialImageName = [InitialImageName ' - ' num2str(rand(1))]; %#ok<AGROW>
    Reflec_LatLong.InitialImageName = InitialImageName;
    
    [~, nomFic] = fileparts(nomFic);
    nomFic = strrep(nomFic, '.', '_');
    strGridSize = num2str(gridSize);
    strGridSize = strrep(strGridSize, '.', ',');
    nomFicErs = fullfile(repExport, [nomFic '_' strGridSize 'm_' nomDataType '_LatLong.ers']);
    flag = export_ermapper(Reflec_LatLong, nomFicErs);
    if ~flag
        return
    end
    
    %% Cr�ation de la mosa�que des angles
    
    InitialImageName = Angle_LatLong.InitialImageName;
%     InitialImageName = [InitialImageName ' - ' num2str(rand(1))]; %#ok<AGROW>
    Angle_LatLong.InitialImageName = InitialImageName;
    
    nomFicErs = fullfile(repExport, [nomFic '_' strGridSize 'm_'  '_Angle_LatLong.ers']);
    flag = export_ermapper(Angle_LatLong, nomFicErs);
    if ~flag
        return
    end
end
my_close(hw, 'MsgEnd')
