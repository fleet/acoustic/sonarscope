function [flag, Mosa, nomDir] = SAR_PingSamples2LatLong(varargin)

persistent persistentMaxSlantRange

% [varargin, XLim]   = getPropertyValue(varargin, 'XLim', []);
% [varargin, YLim]   = getPropertyValue(varargin, 'YLim', []);
[varargin, resol]    = getPropertyValue(varargin, 'Resol', []);
% [varargin, Backup] = getPropertyValue(varargin, 'Backup', []);
[varargin, nomDir]   = getPropertyValue(varargin, 'NomDir', []); %#ok<ASGLU>
% [varargin, window] = getPropertyValue(varargin, 'window', []);

I0 = cl_image_I0;
E0 = cl_ermapper([]);

Mosa = [];

if isempty(nomDir)
    nomDir = pwd;
end

%% Recherche des fichiers .sdf

[flag, liste] = uiSelectFiles('ExtensionFiles', '.im', 'RepDefaut', nomDir);
if ~flag || isempty(liste)
    return
end
nbProfils = length(liste);
nomDir = fileparts(liste{1});

%% Saisie de la r�solution

[flag, resol] = get_gridSize('Value', -resol, 'Initial', 1.5);
if ~flag
    return
end

%% Backup frequency

[flag, Backup] = saisie_backupFrequency(nbProfils);
if ~flag
    return
end

%% Across distance interpolation method

% [flag, AcrossInterpolation] = question_AcrossDistanceInterpolation;
% if ~flag
%     return
% end

%% Mosaic interpolation window

% [flag, window] = question_MosaicInterpolationWindow(window);
% if ~flag
%     return
% end

%% Noms du fichier de sauvegarde de la mosaique de r�flectivit�

DataType = cl_image.indDataType('Reflectivity');
[nomFicErMapperLayer, flag] = initNomFicErs(E0, 'Mosaic', cl_image.indGeometryType('LatLong'), DataType, nomDir, 'resol', resol);
if ~flag
    return
end
[nomDir, TitreDepth] = fileparts(nomFicErMapperLayer);

%% Noms du fichier de sauvegarde de la mosaique des angles d'�mission

DataType = cl_image.indDataType('TxAngle');
[nomFicErMapperAngle, flag] = initNomFicErs(E0, TitreDepth, cl_image.indGeometryType('LatLong'), DataType, nomDir, 'confirm', (get_LevelQuestion >= 3));
if ~flag
    return
end

%% Recuperation des mosaiques existantes

Carto = [];
if exist(nomFicErMapperLayer, 'file') && exist(nomFicErMapperAngle, 'file')
    [flag, rep] = question_CompleteExistingMosaic;
    if ~flag
        return
    end
    if rep == 1
        [flag, Z_Mosa_All] = cl_image.import_ermapper(nomFicErMapperLayer);
        if ~flag
            return
        end
        Carto = get(Z_Mosa_All, 'Carto');
        
        [flag, A_Mosa_All]  = cl_image.import_ermapper(nomFicErMapperAngle);
        if ~flag
            return
        end
    else
        Z_Mosa_All = [];
        A_Mosa_All = [];
    end
else
    Z_Mosa_All = [];
    A_Mosa_All = [];
end

%% Si Mosaique de Reflectivite, on propose de faire une compensation statistique ou une calibration

% [flag, TypeCalibration, CorFile, bilan, DataTypeConditions, alpha, ModelTxDiag, CourbeBS] = params_SonarCompensation(nomDir);
[flag, TypeCalibration, CorFile, bilan] = params_SonarCompensation(nomDir);
if ~flag
    return
end

%% Limites angulaires

[flag, LimitAngles] = question_AngularLimits('QL', 3);
if ~flag
    return
end

%% Limites en distance oblique

str1 = 'Voulez-vous limiter les profils en distance obliques ?';
str2 = 'Do you want to limitate the profiles by oblique distances  ?';
[choix, flag] = my_questdlg(Lang(str1,str2), 'Init', 2);
if ~flag
    return
end
if choix == 1
    if isempty(persistentMaxSlantRange)
        MaxSlantRange = 550;
    else
        MaxSlantRange = persistentMaxSlantRange;
    end
    
    str1 = 'Limitation en distance oblique.';
    str2 = 'Oblique distance limitation';
    [flag, MaxSlantRange] = inputOneParametre(Lang(str1,str2), Lang('Distance min','Min distance'), ...
        'Value', MaxSlantRange, 'MinValue', 400, 'MaxValue', 750, 'Unit', 'm');
    if ~flag
        return
    end
    
    persistentMaxSlantRange = MaxSlantRange;
else
    MaxSlantRange = [];
end

%% Traitement des fichiers

% profile on
% tic


SonarTVG_IfremerAlpha = [];
for k1=1:nbProfils
    fprintf('Processing file %d/%d : %s\n', k1, nbProfils, liste{k1});
    
    %% Lecture des layers "Range", "AcrossDistance", "AlongDistance" et "BeamDepressionAngle"
    
    [flag, c, Carto, SonarTVG_IfremerAlpha] = import_SAR(I0, liste{k1}, 'Carto', Carto, 'subImage', 2, ...
        'SonarTVG_IfremerAlpha', SonarTVG_IfremerAlpha, 'MaxSlantRange', MaxSlantRange);
    
    nbRows = c.nbRows;
    if nbRows <= 1
        Message = sprintf('The file %s is discarded because it has %d row.', liste{k1}, nbRows);
        my_warndlg(Message, 0);
        continue
    end
    
    FishLatitude  = get(c, 'FishLatitude');
    FishLongitude = get(c, 'FishLongitude');
    Heading       = get(c, 'Heading');
    
    GT_Image = c.GeometryType;
    if TypeCalibration == 1
        GT_Bilan = cl_image.indGeometryType(bilan{1}(1).GeometryType);
        if ~isempty(CorFile) && (GT_Image == GT_Bilan)
            TxAngle_PingSamples = sonar_lateral_emission(c, 'useRoll', 0);
            c = compensationCourbesStats(c, TxAngle_PingSamples, bilan);
            clear TxAngle_PingSamples
        end
    end
    
    Reflectivity_PingAcrossDist = sonarLateral_PingRange2PingAcrossDist(c, 'resolutionX', resol, 'createNbp', 0);
    
    %% Gestion de la limite angulaire
    
    if ~isempty(LimitAngles)
        TxAngle_PingAcrossDist = sonar_lateral_emission(Reflectivity_PingAcrossDist, 'useRoll', 0);
        layerMasque = ROI_auto(TxAngle_PingAcrossDist, 1, 'MaskReflec', TxAngle_PingAcrossDist, [], LimitAngles, 1);
        Reflectivity_PingAcrossDist = masquage(Reflectivity_PingAcrossDist, 'LayerMask', layerMasque, 'valMask', 1);
    end
    
    [flag, LatLon] = sonar_calcul_coordGeo_SonarX(Reflectivity_PingAcrossDist, Heading, ...
        FishLatitude, FishLongitude);
    if ~flag
        continue
    end
    clear c
    
    %% Compensation de l'image si la courbe a �t� calcul�e dans la g�om�trie
    % PingSamples
    
    TxAngle_PingAcrossDist = sonar_lateral_emission(Reflectivity_PingAcrossDist, 'useRoll', 0);
    GT_Image = Reflectivity_PingAcrossDist.GeometryType;
    if TypeCalibration == 1
        GT_Bilan = cl_image.indGeometryType(bilan{1}(1).GeometryType);
        if ~isempty(CorFile) && (GT_Image == GT_Bilan)
            Reflectivity_PingAcrossDist = compensationCourbesStats(Reflectivity_PingAcrossDist, TxAngle_PingAcrossDist, bilan);
        end
    end
    
    nbRows = Reflectivity_PingAcrossDist.nbRows;
    nbCol = Reflectivity_PingAcrossDist.nbColumns;
    pas = floor(nbRows / ((nbRows * nbCol * 4) / 1e7)); % SizePhysTotalMemory
    
    
    NBlocks = ceil(nbRows/pas);
    pas = ceil(nbRows/NBlocks);
    
    for k2=1:pas:nbRows
        subl = max(1,k2-5):min(k2-1+5+pas, nbRows);
        fprintf('Processing pings [%d:%d] Fin=%d\n', subl(1), subl(end), nbRows);
        
        [flag, Z_Mosa, A_Mosa] = sonar_mosaique2(Reflectivity_PingAcrossDist, TxAngle_PingAcrossDist, LatLon(1), LatLon(2), resol, ...
            'AcrossInterpolation', 2, 'AlongInterpolation',  2, ...
            'suby', subl);
        if ~flag
            return
        end
        
        Z_Mosa.Writable = false;
        A_Mosa.Writable = false;
        
        if isempty(Z_Mosa_All)
            Z_Mosa_All = Z_Mosa;
            A_Mosa_All = A_Mosa;
        else
            [Z_Mosa_All, A_Mosa_All] = mosaique([Z_Mosa_All Z_Mosa], 'Angle', [A_Mosa_All A_Mosa], 'FistImageIsCollector', 1);
            clear Z_Mosa A_Mosa
        end
        Z_Mosa_All.Name = 'Reflectivity mosaic';
        A_Mosa_All.Name = 'Emission angle mosaic';
        Z_Mosa_All.Writable = false;
        A_Mosa_All.Writable = false;
    end
    clear Reflectivity_PingAcrossDist TxAngle_PingAcrossDist LatLon
    
    %% Sauvegarde des mosaiques dans des fichiers ErMapper
    
    if (mod(k1,Backup) == 0) || (k1 == nbProfils)
        Z_Mosa_All = majCoordonnees(Z_Mosa_All);
        A_Mosa_All = majCoordonnees(A_Mosa_All);
        
        [~, ImageName] = fileparts(nomFicErMapperLayer);
        Z_Mosa_All.Name = ImageName;
        
        [~, ImageName] = fileparts(nomFicErMapperAngle);
        A_Mosa_All.Name = ImageName;
        
        export_ermapper(Z_Mosa_All, nomFicErMapperLayer);
        export_ermapper(A_Mosa_All, nomFicErMapperAngle);
        
        % D�placement Mikael
        deleteFileOnListe(cl_memmapfile.empty, gcbf)
        % D�placement Mikael
    end
end
flag = 1;

clear Mosa
Mosa(1) = Z_Mosa_All;
Mosa(2) = A_Mosa_All;

% toc
% profile report
