function [Fig, Carto] = SAR_PlotNavigation(nomFic, varargin)

[varargin, Carto]       = getPropertyValue(varargin, 'Carto',       []);
[varargin, EtapeUnique] = getPropertyValue(varargin, 'EtapeUnique', 1);
[varargin, Fig]         = getPropertyValue(varargin, 'Fig',         []); %#ok<ASGLU>

%% Cr�ation de la fen�tre avec les menu et callbacks sp�cialis�s pour la nav

Fig = plot_navigation_Figure(Fig);

%% Traitement des fichiers

NbFic = length(nomFic);
str1 = 'Trac� de la navigation des fichiers SAR';
str2 = 'Plot SAR navigation';
hw = create_waitbar(Lang(str1,str2), 'N', NbFic);
for k=1:NbFic
    my_waitbar(k, NbFic, hw);
    
    flag = Sar2ssc(nomFic{k});
    if ~flag
        return
    end
    
    [flag, Data] = SSc_ReadDatagrams(nomFic{k}, 'Ssc_Sar', 'Memmapfile', -1);
    if ~flag
        return
    end
    
    Longitude  = Data.Longitude(:);
    Latitude   = Data.Latitude(:);
    SonarTime  = Data.Time;
%     SonarSpeed = Data.Speed(:);
    Heading    = Data.Heading(:);
    clear Data
    
    plot_navigation(nomFic{k}, Fig, Longitude, Latitude, SonarTime.timeMat, Heading);
end
my_close(hw, 'MsgEnd');
