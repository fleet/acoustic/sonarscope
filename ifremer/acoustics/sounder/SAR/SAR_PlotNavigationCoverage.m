function [Fig, Carto] = SAR_PlotNavigationCoverage(nomFic, varargin)

[varargin, Carto]       = getPropertyValue(varargin, 'Carto',       []);
[varargin, EtapeUnique] = getPropertyValue(varargin, 'EtapeUnique', 1);
[varargin, Fig]        = getPropertyValue(varargin, 'Fig',        []);
[varargin, MaxRange]    = getPropertyValue(varargin, 'MaxRange',    550); %#ok<ASGLU>

%% Cr�ation de la fen�tre avec les menu et callbacks sp�cialis�s pour la nav

Fig = plot_navigation_Figure(Fig);

%% Traitement des fichiers

ColorOrder = get(gca, 'ColorOrder');
nbCoul = size(ColorOrder,1);

NbFic = length(nomFic);
str1 = 'Trac� de la navigation des fichiers SAR';
str2 = 'Plot SAR navigation';
hw = create_waitbar(Lang(str1,str2), 'N', NbFic);
for k=1:NbFic
    my_waitbar(k, NbFic, hw);
    
    flag = Sar2ssc(nomFic{k});
    if ~flag
        return
    end
    
    [flag, Data] = SSc_ReadDatagrams(nomFic{k}, 'Ssc_Sar', 'Memmapfile', -1);
    if ~flag
        return
    end
    
    resol = 0.25;
    Longitude  = Data.Longitude(:);
    Latitude   = Data.Latitude(:);
    SonarTime  = Data.Time;
%     SonarSpeed = Data.Speed(:);
    Heading    = Data.Heading(:);
    Height     = Data.Height(:) * resol;
    clear Data
    
    plot_navigation(nomFic{k}, Fig, Longitude, Latitude, SonarTime.timeMat, Heading);
    
    iCoul = mod(k-1, nbCoul) + 1;
    plot_Coverage(Fig, Latitude, Longitude, Heading, Height, MaxRange, ColorOrder(iCoul,:), nomFic{k})
end
my_close(hw, 'MsgEnd');


function plot_Coverage(Fig, Latitude, Longitude, Heading, Height, MaxRange, Color, nomFic)

step = 10;
AlongDistance = zeros(size(Latitude));
Height = min(abs(Height), MaxRange);
AcrossDistance = sqrt(MaxRange^2 - Height.^2);
[Lat, Lon] = get_faucheeInLatLong(Latitude, Longitude, Heading, AcrossDistance, -AcrossDistance, AlongDistance, AlongDistance);

subNan = isnan(sum(Lon, 2)) | isnan(sum(Lat,2));
Lon(subNan, :) = [];
Lat(subNan, :) = [];

if isempty(Lon)
    return
end

X1 = Lon(2:end-1,1);
Y1 = Lat(2:end-1,1);
X2 = flipud(Lon(2:end-1,2));
Y2 = flipud(Lat(2:end-1,2));

X1 = reduceByStep(X1, step);
X2 = reduceByStep(X2, step);
Y1 = reduceByStep(Y1, step);
Y2 = reduceByStep(Y2, step);

X = [Lon(1,1); X1; Lon(end,1); Lon(end,2); X2; Lon(1,2); Lon(1,1)];
Y = [Lat(1,1); Y1; Lat(end,1); Lat(end,2); Y2; Lat(1,2); Lat(1,1)];

figure(Fig)
h = patch(X, Y, Color, 'FaceAlpha', 0.5, 'Tag', nomFic);
set_HitTest_Off(h)


function y = reduceByStep(x, step)
k = 0;
y = NaN(floor(length(x) / step), 1);
for i=1:step:length(x)
    k = k + 1;
    sub = i:min(i+step-1, length(x));
    %     y(k) = median(x(sub));
    y(k) = mean(x(sub));
end

