function SonarTVG_IfremerAlpha = SAR_ReadSonarTVG_IfremerAlpha(nomFic)

if iscell(nomFic)
    nomFic = nomFic{1};
end

nomDir = fileparts(nomFic);
nomFicXml = fullfile(nomDir, 'SonarTVG_IfremerAlpha_Folder');
if exist(nomFicXml, 'file')
    SonarTVG_IfremerAlpha = xml_mat_read(nomFicXml);
    if ~isempty(SonarTVG_IfremerAlpha)
        str1 = sprintf('Le fichier "%s" est interprété directement', nomFicXml);
        str2 = sprintf('File "%s" is used by default.', nomFicXml);
        my_warndlg(Lang(str1,str2), 0, 'Tag', 'SonarTVG_IfremerAlpha_Folder', 'TimeDelay', 10);
    end
else
    SonarTVG_IfremerAlpha = [];
end

