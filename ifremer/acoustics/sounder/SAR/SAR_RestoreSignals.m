function flag = SAR_RestoreSignals(nomFic, nomSignal)
 
if ~iscell(nomFic)
    nomFic = {nomFic};
end
 
if ~iscell(nomSignal)
    nomSignal = {nomSignal};
end

for k=1:length(nomFic)
    flag = SAR_RestoreSignals_unitaire(nomFic{k}, nomSignal);
    if ~flag
        % Message SVP
    end
end

str1 = 'La restauration des signaux originaux du SAR est termin�e.';
str2 = 'The restoration of SAR signals is over.';
my_warndlg(Lang(str1,str2), 0);


function flag = SAR_RestoreSignals_unitaire(nomFic, nomSignal)

flag = Sar2ssc(nomFic);
if ~flag
    return
end

[flag, Data] = SSc_ReadDatagrams(nomFic, 'Ssc_Sar', 'Memmapfile', -1);
if ~flag
    return
end

Data.Time      = [];
Data.Latitude  = [];
Data.Longitude = [];
Data.Immersion = [];
Data.Heading   = [];
Data.Speed     = [];
Data.Height    = [];
       
for k=1:length(nomSignal)
    SignalName = nomSignal{k};
    switch SignalName
        case 'Time'
            Time = Data.OriginTime.timeMat;
            flag = save_signal(nomFic, 'Ssc_Sar', 'Time', Time);
            if ~flag
                return
            end
            
        otherwise
            flag = save_signal(nomFic, 'Ssc_Sar', SignalName, Data.([SignalName 'Origin'])(:));
            if ~flag
                return
            end
    end
end
