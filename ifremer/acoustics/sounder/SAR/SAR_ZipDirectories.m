function SAR_ZipDirectories(nomFic, varargin)

if ~iscell(nomFic)
    nomFic = {nomFic};
end

N = length(nomFic);
hw = create_waitbar('Processing', 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    zipSonarScopeCache(nomFic{k})
end
my_close(hw, 'MsgEnd')
