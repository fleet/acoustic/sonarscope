% [flag, dataFileName] = FtpUtils.getSScDemoFile('0014.04.im', 'OutputDir', 'D:\Temp\ExData');
% flag = Sar2ssc(dataFileName);
%
% [flag, Data1] = SSc_ReadDatagrams(                dataFileName, 'Ssc_Sar', 'Memmapfile', -1)
% [flag, Data2] = SScCacheNetcdfUtils.readWholeData(dataFileName, 'Ssc_Sar', 'Memmapfile', -1)

function flag = Sar2ssc(dataFileName)

if ischar(dataFileName)
    dataFileName = {dataFileName};
end

N = length(dataFileName);
flag = zeros(1,N);
for k=1:N
    flag(k) = Sar2ssc_unitaire(dataFileName{k});
end
flag = all(flag);


function flag = Sar2ssc_unitaire(dataFileName)

FormatVersion = 20120402;

flag = exist(dataFileName, 'file');
if ~flag
    str1 = sprintf('Le fichier "%s" n''existe pas', dataFileName);
    str2 = sprintf('File "%s" does not exist', dataFileName);
    my_warndlg(Lang(str1,str2), 1);
    return
end

nbOc = sizeFic(dataFileName);
if nbOc == 0
    flag = 0;
    return
end

%% G�n�ration des XML et des binaires

[nomDir, nomFicSeul] = fileparts(dataFileName);
nomDirRacine = fullfile(nomDir, 'SonarScope', nomFicSeul);

unzipSonarScopeCache(nomDirRacine);

flag = exist(nomDirRacine, 'dir');
if ~flag
    mkdir(nomDirRacine);
else
    flag = [];

    nomFicXml = fullfile(nomDirRacine, 'Ssc_Sar.xml');
    flag(end+1) = exist(nomFicXml, 'file');

    if all(flag) && (getFormatVersion(nomDirRacine) == FormatVersion)
        flag = 1;
        return
    end
end

%% Transcodage

flag = Sar2ssc_file(dataFileName, nomDirRacine, FormatVersion);


function FormatVersion = getFormatVersion(nomDirRacine)

nomFicXml = fullfile(nomDirRacine, 'Ssc_Sar.xml');
flag = exist(nomFicXml, 'file');        
if ~flag
    special_message('FichierNonExistant', nomFicXml)
    return
end
% edit(nomFicXml)
Datagrams = xml_read(nomFicXml);
if ~isfield(Datagrams, 'FormatVersion')
    FormatVersion = 0;
    return
end
FormatVersion = Datagrams.FormatVersion;




function flag = Sar2ssc_file(dataFileName, nomDirRacine, FormatVersion)


%% Lecture du nombre d'octets du fichier

nbOcFic = sizeFic(dataFileName);
if nbOcFic == -1
    return
end

%% Ouverture du fichier

fidIn = fopen(dataFileName, 'r');

%% Initialisations

nb_oc_enr = 7644;
nbRows = floor(nbOcFic / nb_oc_enr);

% disp(sprintf('Nombre d''enregistrements : %d', nbRows));

subl = 1:nbRows;

try
    Data.Sonar = NaN([nbRows, 6001], 'single');
    Data.Sbp   = NaN([nbRows, 760], 'single');
catch %#ok<CTCH>
    Data.Sonar = cl_memmapfile('Value', NaN, 'Size', [nbRows, 6000], 'Format', 'single');
    Data.Sbp   = cl_memmapfile('Value', NaN, 'Size', [nbRows,  760], 'Format', 'single');
end

Date           = NaN(nbRows, 1, 'single');
Heure          = NaN(nbRows, 1, 'single');
Data.Height    = NaN(nbRows, 1, 'single');
Data.Immersion = NaN(nbRows, 1, 'single');
Data.Heading   = NaN(nbRows, 1, 'single');
Data.Speed     = NaN(nbRows, 1, 'single');
Data.Latitude  = NaN(nbRows, 1, 'double');
Data.Longitude = NaN(nbRows, 1, 'double');

[~, nomFicSeul] = fileparts(dataFileName);
str1 = sprintf('Importation de l''image SAR "%s"', nomFicSeul);
str2 = sprintf('Importing SAR image "%s"', nomFicSeul);
hw = create_waitbar(Lang(str1,str2), 'N', nbRows);
for k=1:nbRows
    my_waitbar(k, nbRows, hw);

    %% Calcul de la position du debut de l'enregistrement
    
    position = (subl(k)-1) * nb_oc_enr;
    
    %% Lecture de l'enregistrement
    
    fseek(fidIn, position, 'bof');
    enr = (fread(fidIn, nb_oc_enr, 'uint8'))';
    
    %% Recuperation des donnees
    
    Data.Height(k,1) = str2double(char(enr(33:38)));
%     Data.Height(k,2) = str2double(char(enr(40:45)));
    charDate = char(enr(190:197));
    
    annee = str2num(charDate(7:8));%#ok
    if annee > 80
        siecle = 19;
    else
        siecle = 20;
    end
    
    Date(k) = dayStr2Ifr( sprintf('%s%d%s', charDate(1:6), siecle, charDate(7:8)) );
    charHeure = char(enr(199:210));
    Heure(k) = hourStr2Ifr( charHeure );
       
    switch char(enr(213))	% Il y a un probleme de decalage d'1 octet dans les fichiers
    case ','
        sublat = 212:224;
        sublon = 226:239;
    otherwise
        sublat = 213:224;
        sublon = 227:239;
    end
    Data.Latitude(k)  =  str2lat(char(enr(sublat)));
    Data.Longitude(k) =  str2lon(char(enr(sublon)));
    Data.Immersion(k) = -str2double(char(enr(241:249)));
    Data.Heading(k)   =  str2double(char(enr(281:285)));
    
    reflecB = enr(792:3791);
    reflecT = enr(3832:6831);
    Sondeur = enr(6871:6871+760-1);
    
    %% Lecture des 2 voies      

    Data.Sonar(k,1:3000)     = fliplr(reflecB);     %reflectivit� babord
    Data.Sonar(k,3002:6001 ) = reflecT;             %reflectivit� tribord
    
    %% Lecture de la voie sondeur
    
    Data.Sbp(k,:) = Sondeur;
    
end
my_close(hw, 'MsgEnd')

%% Detection si on est sur une donn�e avant refonte : port�e 500m

if sum(sum(Data.Sonar(:,1:999))) == 0
    Data.Sonar = Data.Sonar(:,1001:5001);
end

%% Fermeture du fichier

fclose(fidIn);
ResolSample = 0.25;
Data.Height = Data.Height / ResolSample;

Data.Time = cl_time('timeIfr', Date, Heure);
Data.OriginTime      = Data.Time;
Data.OriginLatitude  = Data.Latitude;
Data.OriginLongitude = Data.Longitude;
Data.OriginImmersion = Data.Immersion;
Data.OriginHeading   = Data.Heading;
Data.OriginSpeed     = Data.Speed;
Data.OriginHeight    = Data.Height(:);

%% Cr�ation du fichier XML

Info.Title                  = 'SAR_Data';
Info.Constructor            = 'ThomsonSintra';
Info.TimeOrigin             = '01/01/-4713';
Info.Comments               = 'Binary Data';
Info.FormatVersion          = FormatVersion;
Info.ResolSample            = ResolSample;

Info.Dimensions.nbPings        = size(Data.Sonar,1);
Info.Dimensions.nbSamplesSonar = size(Data.Sonar,2);
Info.Dimensions.nbSamplesSbp   = size(Data.Sbp,2);
        
Info.Signals(1).Name          = 'Time';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = 'days since JC';
Info.Signals(end).FileName    = fullfile('Ssc_Sar','Time.bin');
Info.Signals(end).Tag         = verifKeyWord('SounderTime');

Info.Signals(end+1).Name      = 'OriginTime';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = 'days since JC';
Info.Signals(end).FileName    = fullfile('Ssc_Sar','OriginTime.bin');
Info.Signals(end).Tag         = verifKeyWord('SounderTime');
        
Info.Signals(end+1).Name      = 'Latitude';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_Sar','Latitude.bin');
Info.Signals(end).Tag         = verifKeyWordSegy('Data');

Info.Signals(end+1).Name      = 'OriginLatitude';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_Sar','OriginLatitude.bin');
Info.Signals(end).Tag         = verifKeyWordSegy('Data');
        
Info.Signals(end+1).Name      = 'Longitude';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_Sar','Longitude.bin');
Info.Signals(end).Tag         = verifKeyWordSegy('Data');
        
Info.Signals(end+1).Name      = 'OriginLongitude';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_Sar','OriginLongitude.bin');
Info.Signals(end).Tag         = verifKeyWordSegy('Data');
        
Info.Signals(end+1).Name      = 'Immersion';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_Sar','Immersion.bin');
Info.Signals(end).Tag         = verifKeyWordSegy('Data');
        
Info.Signals(end+1).Name      = 'OriginImmersion';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_Sar','OriginImmersion.bin');
Info.Signals(end).Tag         = verifKeyWordSegy('Data');
        
Info.Signals(end+1).Name      = 'Heading';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_Sar','Heading.bin');
Info.Signals(end).Tag         = verifKeyWordSegy('Data');
        
Info.Signals(end+1).Name      = 'OriginHeading';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_Sar','OriginHeading.bin');
Info.Signals(end).Tag         = verifKeyWordSegy('Data');
        
Info.Signals(end+1).Name      = 'Speed';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_Sar','Speed.bin');
Info.Signals(end).Tag         = verifKeyWordSegy('Data');
        
Info.Signals(end+1).Name      = 'OriginSpeed';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_Sar','OriginSpeed.bin');
Info.Signals(end).Tag         = verifKeyWordSegy('Data');
        
Info.Signals(end+1).Name      = 'Height';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'Sample';
Info.Signals(end).FileName    = fullfile('Ssc_Sar','Height.bin');
Info.Signals(end).Tag         = verifKeyWordSegy('Data');
        
Info.Signals(end+1).Name      = 'OriginHeight';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_Sar','OriginHeight.bin');
Info.Signals(end).Tag         = verifKeyWordSegy('Data');
      
Info.Images(1).Name          = 'Sonar';
Info.Images(end).Dimensions  = 'nbPings, nbSamplesSonar';
Info.Images(end).Storage     = 'single';
Info.Images(end).Unit        = '';
Info.Images(end).FileName    = fullfile('Ssc_Sar','Sonar.bin');
Info.Images(end).Tag         = verifKeyWordSegy('Data');
        
Info.Images(end+1).Name      = 'Sbp';
Info.Images(end).Dimensions  = 'nbPings, nbSamplesSbp';
Info.Images(end).Storage     = 'single';
Info.Images(end).Unit        = '';
Info.Images(end).FileName    = fullfile('Ssc_Sar','Spb.bin');
Info.Images(end).Tag         = verifKeyWordSegy('Data');


%% Cr�ation du fichier XML d�crivant la donn�e 

nomFicXml = fullfile(nomDirRacine, 'Ssc_Sar.xml');
xml_write(nomFicXml, Info);
flag = exist(nomFicXml, 'file');
if ~flag
    messageErreur(nomFicXml)
    return
end

%% Cr�ation du r�pertoire SegyData

nomDirSegyData = fullfile(nomDirRacine, 'Ssc_Sar');
if ~exist(nomDirSegyData, 'dir')
    status = mkdir(nomDirSegyData);
    if ~status
        messageErreur(nomDirSegyData)
        flag = 0;
        return
    end
end

%% Cr�ation des fichiers binaires des signaux

for k=1:length(Info.Signals)
    flag = writeSignal(nomDirRacine, Info.Signals(k), Data.(Info.Signals(k).Name));
    if ~flag
        return
    end
end

%% Cr�ation des fichiers binaires des images

for k=1:length(Info.Images)
    flag = writeImage(nomDirRacine, Info.Images(k), Data.(Info.Images(k).Name));
    if ~flag
        return
    end
end

flag = 1;
