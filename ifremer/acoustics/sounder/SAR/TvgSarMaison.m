% Calcul de la loi de TVG realiste
%
% Syntax
%   TVG = TvgSarMaison(alpha, H)
%
% Input Arguments
%   alpha : Coefficient d'attenuation en dB/km
%   H     : Hauteur du SAR en m
%
% Output Arguments
%   TVG : tableau du TVG pour chaque echantillon sonar (3000)
%
% Examples
%   TVG = TvgSarMaison(40, 80);
%   plot(TVG); grid on;;
%
% See also TvgSar Authors
% Authors : JMA
%------------------------------------------------------------------------------

function TVG = TvgSarMaison(alpha, H)

% Psi = 0.5 * pi / 180;
% B = 2500;
C = 1500;
RKM = 1e-3 * C * ((1:3000)/ 3000) / 2;
R = RKM * 1000;

k = 1;
for j=1:length(H)
    sub = find(R <= H(j));
    x = NaN(1, length(sub));
    
    sub = find(R > H(j));
    x(sub) = (R(sub).^2 - H(j).^2).^0.5;
    
    T = atan(x / H(j));
    
    for i=1:length(alpha)
        %TVG(k,:) = 30 * log10(R) + 2. * alpha(i) * RKM  - 10 * log10((C * Psi) ./ (2 * B * sin(T))) - 20 * log10(cos(T));
        TVG(k,:) = 30 * log10(R) + 2. * alpha(i) * RKM  + 10 * log10(sin(T)) - 10 * log10(cos(T));
        k = k + 1;
    end
end
