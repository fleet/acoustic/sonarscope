% Constitution d'une base de donnees images du SAR
% On fait correspondre a chaque fichier de donnees brutes un fichier tif de
% resolution 4 fois moins grande. On passe ainsi d'une resolution de 25 cm
% environ a 1M. La reduction se fait par moyennage.
% Si le SAR avancait a 2 moeuds, la "resolution" longitudinale est de 1 m environ,
% on obtient ainsi des images a peu pres orthonormees.
%
% Syntax
%   createDataSetSar(w, ...)
%
% Input Arguments
%   w : Liste des fichiers a traiter
%
% Name-Value Pair Arguments
%   reduc  : Coefficient de reduction de la donnee (4 par defaut)
%   repTif : Repertoire de stockage des fichiers tif
%
% Name-only Arguments
%   DonneesAnciennes : Flag pour dire si les donnees etaient limitees a 500m
%
% Examples
%   w = listeFicOnDir('/home/fresnel/BdImages/Titanic/Donnees', '.im')
%   createDataSetSar(w, 'reduc', 4, 'repTif', '/home/fresnel/BdImages/Titanic/Tif')
%   createResumeDataSetSar(w)
%   z = listeFicOnDir('/home/fresnel/BdImages/Titanic/Tif', '.tif')
%   createVignette(z, 'reduc', 4, 'repJpg', '/home/fresnel/BdImages/Titanic/Miniatures')
%
%   w = listeFicOnDir('/home/fresnel/BdImages/Fournaise/Donnees', '.im')
%   createDataSetSar(w, 'reduc', 4, 'repTif', '/home/fresnel/BdImages/Fournaise/Tif')
%   createResumeDataSetSar(w)
%   z = listeFicOnDir('/home/fresnel/BdImages/Fournaise/Tif', '.tif')
%   createVignette(z, 'reduc', 4, 'repJpg', '/home/fresnel/BdImages/Fournaise/Miniatures')
%
%   w = listeFicOnDir('/home/fresnel/BdImages/Hudsar/Donnees', '.im')
%   createDataSetSar(w, 'reduc', 4, 'repTif', '/home/fresnel/BdImages/Hudsar/Tif')
%   createResumeDataSetSar(w)
%   z = listeFicOnDir('/home/fresnel/BdImages/Hudsar/Tif', '.tif')
%   createVignette(z, 'reduc', 4, 'repJpg', '/home/fresnel/BdImages/Hudsar/Miniatures')
%
%   w = listeFicOnDir('/home2/uranie/NERIS1/DATA_SAR/ZONEA/image', '.im')
%   createDataSetSar(w, 'reduc', 4, 'repTif', '/home/fresnel/BdImages/NERIS1/Tif')
%   createResumeDataSetSar(w)
%   z = listeFicOnDir('/home/fresnel/BdImages/NERIS1/Tif', '.tif')
%   createVignette(z, 'reduc', 4, 'repJpg', '/home/fresnel/BdImages/NERIS1/Miniatures')
%
% See also listeFicOnDir createVignette LecFicSarSpeedy createResumeDataSetSar Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function createDataSetSar(w, varargin)

if ~iscell(w)
    w = {w};
end

% -----------------------------------------
% Coefficient de reduction de la resolution

[varargin, reduc] = getPropertyValue(varargin, 'reduc', 1);

% ---------------------------------------
% Repertoire de stockage des fichiers tif

rep = fileparts(w{1});
[varargin, repTif] = getPropertyValue(varargin, 'repTif', rep);

% -----------------------------------------------------
% Flag pour dire si les donnees etaient limitees a 500m

[varargin, DonneesAnciennes] = getFlag(varargin, 'DonneesAnciennes'); %#ok<ASGLU>

%% Traitement des fichiers

N = length(w);
hw = create_waitbar('Prossing', 'N', N);
for i=1:N
    my_waitbar(i, N, hw)
    [rep, nomFic] = fileparts(w{i});
    
    nbEnr = getNbEnrFicSar(w{i});
    BT_Tif = zeros(nbEnr, 6000/reduc);
    
    pas = 1000;
    for k=1:ceil(nbEnr/pas)
        deb = 1+(k-1)*pas;
        fin = min(nbEnr, k*pas);
        subl = deb:fin;
        
        [BT, str] = LecFicSarSpeedy(w{i}, 'subl', subl);
        
        disp(str)
        
        if DonneesAnciennes
            BT = BT(:,1001:5000);
        end
        
        %% R�duction du fichier par moyennege
        
        if reduc ~= 1
            BT_Tif(subl,:) = downsize(BT, 'pasy', 1, 'pasx', reduc, 'Method', 'mean');
        else
            BT_Tif(subl,:) = BT;
        end
        
    end
    
    %% Sauvegarde du fichier tif
    
    nomFicTif = fullfile(repTif, [nomFic '.tif']);
    imwrite(BT_Tif/128, nomFicTif, 'tif')
end
my_close(hw, 'MsgEnd')
