% Constitution d'une base de donnees images du SAR
% On fait correspondre a chaque fichier de donnees brutes un fichier tif de
% resolution 4 fois moins grande. On passe ainsi d'une resolution de 25 cm
% environ a 1M. La reduction se fait par moyennage.
% Si le SAR avancait a 2 moeuds, la "resolution" longitudinale est de 1 m environ,
% on obtient ainsi des images a peu pres orthonormees.
%
% Syntax
%   createResumeDataSetSar(w, ...)
%
% Input Arguments
%   w : Liste des fichiers a traiter
%
% Name-Value Pair Arguments
%   reduc  : Coefficient de reduction de la donnee (4 par defaut)
%   repTif : Repertoire de stockage des fichiers tif
%
% Name-only Arguments
%   DonneesAnciennes : Flag pour dire si les donnees etaient limitees a 500m
%
% Examples
%   w = listeFicOnDir('/home/fresnel/Titanic/Donnees', '.im')
%   createResumeDataSetSar(w)
%
%   w = listeFicOnDir('/home/fresnel/Fournaise/Donnees', '.im')
%   createResumeDataSetSar(w)
%
%   w = listeFicOnDir('/home/fresnel/Hudsar/Donnees', '.im')
%   createResumeDataSetSar(w)
%
% See also listeFicOnDir createVignette LecFicSarSpeedy Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function createResumeDataSetSar(w, varargin)

%% Cas o� on donne le nom du fichier en caract�res

if ~iscell(w)
    w = {w};
end

%% R�pertoire de stockage des fichiers tif

rep = fileparts(w{1});
[varargin, repTif] = getPropertyValue(varargin, 'repTif', rep); %#ok<ASGLU>

%% Traitement des fichiers

N = length(w);
hw = create_waitbar('Provcessing', 'N', N);
for i=1:N
    my_waitbar(i, N, hw)
%     [rep, nomFic, ext] = fileparts(w{i});
    
    str = LecBornesFicSar(w{i});
    disp(str)
end
my_close(hw, 'MsgEnd')
