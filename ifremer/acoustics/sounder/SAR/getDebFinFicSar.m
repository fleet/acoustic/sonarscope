% Lecture des heures debut et fin d'un fichier SAR brut
%
% Syntax
%   getDebFinFicSar(nomFic)
%
% Input Arguments 
%   nomFic  : nom du fichier
%
% Examples
%   nomFic = getNomFicDatabase('SAR_0035.01.im')
%   getDebFinFicSar(nomFic)
%
% See also getNbEnrFicSar LecFicSar Authors
% Authors : JMA
% VERSION  : $Id: getDebFinFicSar.m,v 1.3 2002/06/20 10:23:01 augustin Exp $
%------------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HISTORIQUE DEVELOPPEMENT
%   02/11/00 - JMA - Creation
%   02/04/01 - JMA - Mise en ref.
% ----------------------------------------------------------------------------

function getDebFinFicSar(nomFic)

if nargin == 0
    help getDebFinFicSar
    return
end

nbEnr = getNbEnrFicSar( nomFic );

disp('-------------------------------------------------------------');
disp(nomFic);

LecFicSar( nomFic, 1);
LecFicSar( nomFic, nbEnr);

disp('-------------------------------------------------------------');
