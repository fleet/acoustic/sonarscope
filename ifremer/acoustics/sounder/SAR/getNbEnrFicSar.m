% Lecture du nombre d'enregistrements d'un fichier SAR brut
%
% Syntax
%   nbEnr = getNbEnrFicSar( nomFic )
%
% Input Arguments 
%   nomFic  : nom du fichier
%
% Output Arguments
%   nbEnr : Nombre d'enregistrements
%
% Examples
%   nomFic = getNomFicDatabase('SAR_0035.01.im')
%   nbEnr = getNbEnrFicSar( nomFic )
%
% See also getNbEnrFicSar LecFicSar Authors
% Authors : JMA
%------------------------------------------------------------------------------

function nbEnr = getNbEnrFicSar( nomFic )

%% Lecture du nombre d'octets du fichier

nbOcFic = sizeFic(nomFic);

nb_oc_enr = 7644;
nbEnr = nbOcFic / nb_oc_enr;
