% Identification du nu de loi utilise pour un ping
%
% Syntax
%   [nuprom, nuLoi, GainFixeBab, GainFixeTri, ShBab, ShTri] = getNuLoi(datePing, heurePing)
%
% Input Arguments 
%   datePing  : date du ping
%   heurePing : heure du ping
%
% Output Arguments
%	nuprom		: Numero de la prom utilisee
%	nuLoi		: Numero de la loi de l'eprom utilisee
%	GainFixeBab	: Gain fixe utilise a babord
%	GainFixeTri	: Gain fixe utilise a tribord
%	ShBab		: Sensibilite de l'antenne babord
%	ShTri		: Sensibilite de l'antenne tribord
%
% Examples
%   [nuprom, nuLoi, GainFixeBab, GainFixeTri, ShBab, ShTri] = getNuLoi(dayStr2Ifr('14/04/1999'), hourStr2Ifr('10:00:00'))
%   [nuprom, nuLoi, GainFixeBab, GainFixeTri, ShBab, ShTri] = getNuLoi(dayStr2Ifr('15/04/1999'), hourStr2Ifr('10:00:00'))
%   [nuprom, nuLoi, GainFixeBab, GainFixeTri, ShBab, ShTri] = getNuLoi(dayStr2Ifr('16/04/1999'), hourStr2Ifr('09:00:00'))
%   [nuprom, nuLoi, GainFixeBab, GainFixeTri, ShBab, ShTri] = getNuLoi(dayStr2Ifr('16/04/1999'), hourStr2Ifr('19:00:00'))
%   [nuprom, nuLoi, GainFixeBab, GainFixeTri, ShBab, ShTri] = getNuLoi(dayStr2Ifr('17/04/1999'), hourStr2Ifr('08:00:00'))
%   [nuprom, nuLoi, GainFixeBab, GainFixeTri, ShBab, ShTri] = getNuLoi(dayStr2Ifr('17/04/1999'), hourStr2Ifr('08:20:00'))
%   [nuprom, nuLoi, GainFixeBab, GainFixeTri, ShBab, ShTri] = getNuLoi(dayStr2Ifr('17/04/1999'), hourStr2Ifr('08:35:00'))
%   [nuprom, nuLoi, GainFixeBab, GainFixeTri, ShBab, ShTri] = getNuLoi(dayStr2Ifr('17/04/1999'), hourStr2Ifr('08:44:00'))
%   [nuprom, nuLoi, GainFixeBab, GainFixeTri, ShBab, ShTri] = getNuLoi(dayStr2Ifr('17/04/1999'), hourStr2Ifr('09:00:00'))
%   [nuprom, nuLoi, GainFixeBab, GainFixeTri, ShBab, ShTri] = getNuLoi(dayStr2Ifr('17/04/1999'), hourStr2Ifr('10:00:00'))
%
% See also TvgSar Authors
% Authors : JMA
% VERSION  : $Id: getNuLoi.m,v 1.3 2002/06/20 10:23:01 augustin Exp $
%------------------------------------------------------------------------------

function [nuprom, nuLoi, GainFixeBab, GainFixeTri, ShBab, ShTri] = getNuLoi(datePing, heurePing)

nuprom = 2;
GainFixeTri = 30;

% Antenne	170 kHz		190 kHz
% erramer nu 1	-188.6		-183.6
% erramer nu 2	-185.1		-183.8
% erramer nu 3	-183.1

%ShBab = -188.6;	 % Antenne erramer1 a babord
%ShTri = -183.8;	 % Antenne erramer2 a tribord
ShBab = -183;	 % Antenne erramer1 a babord
ShTri = -183;	 % Antenne erramer2 a tribord

% ------------------
% Missioin Essar991

ih = 1;
dateChange(ih) = dayStr2Ifr('14/04/1999');
heureChange(ih) = hourStr2Ifr('00:00:00');
NuLoi(ih) = 8;
TabGainFixeBab(1) = 28;

ih = ih + 1;
dateChange(ih) = dayStr2Ifr('18/04/1999');
heureChange(ih) = hourStr2Ifr('07:38:40');
NuLoi(ih) = 8;
TabGainFixeBab(ih) = 34;

ih = ih + 1;
dateChange(ih) = dayStr2Ifr('18/04/1999');
heureChange(ih) = hourStr2Ifr('07:43:30');
NuLoi(ih) = 8;
TabGainFixeBab(ih) = 21;

ih = ih + 1;
dateChange(ih) = dayStr2Ifr('18/04/1999');
heureChange(ih) = hourStr2Ifr('08:15:00');
NuLoi(ih) = 3;
TabGainFixeBab(ih) = 21;

ih = ih + 1;
dateChange(ih) = dayStr2Ifr('18/04/1999');
heureChange(ih) = hourStr2Ifr('08:29:30');
NuLoi(ih) = 4;
TabGainFixeBab(ih) = 21;

ih = ih + 1;
dateChange(ih) = dayStr2Ifr('18/04/1999');
heureChange(ih) = hourStr2Ifr('08:39:20');
NuLoi(ih) = 7;
TabGainFixeBab(ih) = 21;

ih = ih + 1;
dateChange(ih) = dayStr2Ifr('18/04/1999');
heureChange(ih) = hourStr2Ifr('08:48:30');
NuLoi(ih) = 4;
TabGainFixeBab(ih) = 21;

ih = ih + 1;
dateChange(ih) = dayStr2Ifr('18/04/1999');
heureChange(ih) = hourStr2Ifr('09:09:00');
NuLoi(ih) = 8;
TabGainFixeBab(ih) = 21;

% -----------------
% Missioin Essar992

%ih = ih + 1;
%dateChange(ih) = dayStr2Ifr('26/09/1999');
%heureChange(ih) = hourStr2Ifr('14:00:00');
%NuLoi(ih) = 2;
%TabGainFixetri(ih) = 20;
%TabGainFixeBab(ih) = 20;

ih = ih + 1;
dateChange(ih) = dayStr2Ifr('26/09/1999');
heureChange(ih) = hourStr2Ifr('14:33:36');
NuLoi(ih) = 8;
TabGainFixetri(ih) = 20;
TabGainFixeBab(ih) = 20;

ih = ih + 1;
dateChange(ih) = dayStr2Ifr('26/09/1999');
heureChange(ih) = hourStr2Ifr('14:59:10');
NuLoi(ih) = 2;
TabGainFixetri(ih) = 20;
TabGainFixeBab(ih) = 20;

ih = ih + 1;
dateChange(ih) = dayStr2Ifr('26/09/1999');
heureChange(ih) = hourStr2Ifr('15:16:55');
NuLoi(ih) = 3;
TabGainFixetri(ih) = 20;
TabGainFixeBab(ih) = 20;

ih = ih + 1;
dateChange(ih) = dayStr2Ifr('26/09/1999');
heureChange(ih) = hourStr2Ifr('15:28:07');
NuLoi(ih) = 4;
TabGainFixetri(ih) = 20;
TabGainFixeBab(ih) = 20;

ih = ih + 1;
dateChange(ih) = dayStr2Ifr('26/09/1999');
heureChange(ih) = hourStr2Ifr('15:46:05');
NuLoi(ih) = 5;
TabGainFixetri(ih) = 20;
TabGainFixeBab(ih) = 20;

ih = ih + 1;
dateChange(ih) = dayStr2Ifr('26/09/1999');
heureChange(ih) = hourStr2Ifr('15:48:01');
NuLoi(ih) = 6;
TabGainFixetri(ih) = 20;
TabGainFixeBab(ih) = 20;

ih = ih + 1;
dateChange(ih) = dayStr2Ifr('26/09/1999');
heureChange(ih) = hourStr2Ifr('15:54:49');
NuLoi(ih) = 7;
TabGainFixetri(ih) = 20;
TabGainFixeBab(ih) = 20;

% Mission Zaiango
ih = ih + 1;
dateChange(ih) = dayStr2Ifr('01/01/2000');
heureChange(ih) = hourStr2Ifr('00:00:00');
NuLoi(ih) = 8;
TabGainFixetri(ih) = 20;
TabGainFixeBab(ih) = 20;





% -----------------------------------------------------------------------------------------------

if timeIfr2Duration([datePing, heurePing], [dateChange(1), heureChange(1)]) < 0
    str = sprintf('Loi TVG non communiquee pour %s %s, j''en prends une au hazard.', dayIfr2str(datePing), hourIfr2str(heurePing));
    my_warndlg(str, 0, 'Tag', 'NuLoiTVGSarNonDefinie');
%     ih = ih + 1;
%     NuLoi(ih) = 8;
%     TabGainFixeBab(1) = 28;
end

for i=2:length(dateChange)
    nbMilisecondesAv = timeIfr2Duration([datePing, heurePing], [dateChange(i-1), heureChange(i-1)]);
    nbMilisecondesAp = timeIfr2Duration([datePing, heurePing], [dateChange(i), heureChange(i)]);
    if(nbMilisecondesAv >= 0) && (nbMilisecondesAp < 0)
        nuLoi = NuLoi(i-1);
        GainFixeBab = TabGainFixeBab(i-1);
        return
    end
end

nuLoi = NuLoi(length(dateChange));
GainFixeBab = 21;
