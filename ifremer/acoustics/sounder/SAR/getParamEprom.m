% Lecture des parametres d'une loi de TVG d'une Eprom
%
% Syntax
%   [D, a0, DeltaG, LimiteGMax] = getParamEprom(nuprom, nuLoi)
%
% Input Arguments 
%   nuprom : Numero de la prom (1 a 5)
%   nuLoi  : Numero de la loi utilisee dans la prom (1 a 8)
%
% Output Arguments
%   D		   : Coefficient de la divergence geometrique
%   a0		   : Coefficient d'attenuation en db/km
%   DeltaG     : Variation de gain
%   LimiteGMax : Limite de la variation de la TVG (500 ou 750 m)
%
% Examples
%   [D, a0, DeltaG, LimiteGMax] = getParamEprom(2, 8)
%   [D, a0, DeltaG, LimiteGMax] = getParamEprom(2, 1:8)
%
% See also getNuLoi TvgSar Authors
% Authors : JMA
% VERSION  : $Id: getParamEprom.m,v 1.2 2002/06/20 10:23:01 augustin Exp $
%------------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HISTORIQUE DEVELOPPEMENT
%   02/11/00 - JMA - Creation
%   02/04/01 - JMA - Mise en ref.
% ----------------------------------------------------------------------------

function [D, a0, DeltaG, LimiteGMax] = getParamEprom(nuprom, nuLoi)

if nargin == 0
    help getParamEprom
    return
end


for i = 1:length(nuLoi)
    switch nuprom
    case 1
        switch nuLoi(i)
        case 1
            D(i) = 30; a0(i) = 57; DeltaG(i) = -5;	LimiteGMax(i) = 750;
        case 2
            D(i) = 30; a0(i) = 57; DeltaG(i) = 0;	LimiteGMax(i) = 750;
        case 3
            D(i) = 30; a0(i) = 57; DeltaG(i) = 5;	LimiteGMax(i) = 750;
        case 4
            D(i) = 30; a0(i) = 57; DeltaG(i) = 10;	LimiteGMax(i) = 750;
        case 5
            D(i) = 50; a0(i) = 57; DeltaG(i) = -5;	LimiteGMax(i) = 750;
        case 6
            D(i) = 50; a0(i) = 57; DeltaG(i) = 0;	LimiteGMax(i) = 750;
        case 7
            D(i) = 50; a0(i) = 57; DeltaG(i) = 5;	LimiteGMax(i) = 750;
        case 8
            D(i) = 30; a0(i) = 34; DeltaG(i) = 0;	LimiteGMax(i) = 500;
        otherwise
            disp('Numero de loi non prevu');
            return
        end
    case 2
        switch nuLoi(i)
        case 1
            D(i) = 30; a0(i) = 38; DeltaG(i) = -10;	LimiteGMax(i) = 750;
        case 2
            D(i) = 30; a0(i) = 38; DeltaG(i) = 0;	LimiteGMax(i) = 750;
        case 3
            D(i) = 30; a0(i) = 38; DeltaG(i) = 10;	LimiteGMax(i) = 750;
        case 4
            D(i) = 30; a0(i) = 38; DeltaG(i) = 20;	LimiteGMax(i) = 750;
        case 5
            D(i) = 50; a0(i) = 38; DeltaG(i) = -10;	LimiteGMax(i) = 750;
        case 6
            D(i) = 50; a0(i) = 38; DeltaG(i) = 0;	LimiteGMax(i) = 750;
        case 7
            D(i) = 50; a0(i) = 38; DeltaG(i) = 10;	LimiteGMax(i) = 750;
        case 8
            D(i) = 30; a0(i) = 34; DeltaG(i) = 0;	LimiteGMax(i) = 500;
        otherwise
            disp('Numero de loi non prevu');
            return
        end
    case 3
        switch nuLoi(i)
        case 1
            D(i) = 30; a0(i) = 26; DeltaG(i) = -5;	LimiteGMax(i) = 750;
        case 2
            D(i) = 30; a0(i) = 26; DeltaG(i) = 0;	LimiteGMax(i) = 750;
        case 3
            D(i) = 30; a0(i) = 26; DeltaG(i) = 5;	LimiteGMax(i) = 750;
        case 4
            D(i) = 30; a0(i) = 26; DeltaG(i) = 10;	LimiteGMax(i) = 750;
        case 5
            D(i) = 50; a0(i) = 26; DeltaG(i) = -5;	LimiteGMax(i) = 750;
        case 6
            D(i) = 50; a0(i) = 26; DeltaG(i) = 0;	LimiteGMax(i) = 750;
        case 7
            D(i) = 50; a0(i) = 26; DeltaG(i) = 5;	LimiteGMax(i) = 750;
        case 8
            D(i) = 30; a0(i) = 34; DeltaG(i) = 0;	LimiteGMax(i) = 500;
        otherwise
            disp('Numero de loi non prevu');
            return
        end
    case 4
        switch nuLoi(i)
        case 1
            D(i) = 30; a0(i) = 38.5; DeltaG(i) = -5;	LimiteGMax(i) = 500;
        case 2
            D(i) = 30; a0(i) = 38.5; DeltaG(i) = 0;	LimiteGMax(i) = 500;
        case 3
            D(i) = 30; a0(i) = 38.5; DeltaG(i) = 5;	LimiteGMax(i) = 500;
        case 4
            D(i) = 30; a0(i) = 38.5; DeltaG(i) = 10;	LimiteGMax(i) = 500;
        case 5
            D(i) = 50; a0(i) = 26; DeltaG(i) = -5;	LimiteGMax(i) = 500;
        case 6
            D(i) = 50; a0(i) = 26; DeltaG(i) = 0;	LimiteGMax(i) = 500;
        case 7
            D(i) = 50; a0(i) = 26; DeltaG(i) = 5;	LimiteGMax(i) = 500;
        case 8
            D(i) = 30; a0(i) = 34; DeltaG(i) = 0;	LimiteGMax(i) = 500;
        otherwise
            disp('Numero de loi non prevu');
            return
        end
    case 5
        switch nuLoi(i)
        case 1
            D(i) = 30; a0(i) = 46; DeltaG(i) = -7.5;	LimiteGMax(i) = 750;
        case 2
            D(i) = 30; a0(i) = 46; DeltaG(i) = 0;	LimiteGMax(i) = 750;
        case 3
            D(i) = 30; a0(i) = 46; DeltaG(i) = 7.5;	LimiteGMax(i) = 750;
        case 4
            D(i) = 30; a0(i) = 46; DeltaG(i) = 15;	LimiteGMax(i) = 750;
        case 5
            D(i) = 50; a0(i) = 46; DeltaG(i) = -7.5;	LimiteGMax(i) = 750;
        case 6
            D(i) = 50; a0(i) = 46; DeltaG(i) = 0;	LimiteGMax(i) = 750;
        case 7
            D(i) = 50; a0(i) = 46; DeltaG(i) = 7.5;	LimiteGMax(i) = 750;
        case 8
            D(i) = 50; a0(i) = 46; DeltaG(i) = 15;	LimiteGMax(i) = 750;
        otherwise
            disp('Numero de loi non prevu');
            return
        end
    otherwise
        disp('Numero de prom non prevu');
        return
    end
end
