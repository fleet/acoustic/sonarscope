% Lecture des tensions de commande contenue dans une eprom
%
% Syntax
%   [loiEpromSonar, loiEpromSondeur] = lecEprom(nomFic)
%
% Input Arguments 
%   nomFic  : Nom du fichier binaire de l'Eprom
%
% Output Arguments
%   loiEpromSonar   : Lois TVG 1 a 8 du sonar
%   loiEpromSondeur : Lois TVG 1 a 8 du sondeur
%
% Examples
%   nomFic = getNomFicDatabase('tvg2.bin')
%   [loiEpromSonar, loiEpromSondeur] = lecEprom(nomFic);
%   figure; plot(loiEpromSonar); grid on;
%   title(['loiEprom Sonar ' nomFic]); legend('1','2','3','4','5','6','7','8');
%
% See also TvgSar Authors
% Authors : JMA
% VERSION  : $Id: lecEprom.m,v 1.3 2002/06/20 10:23:01 augustin Exp $
%------------------------------------------------------------------------------

function [loiEpromSonar, loiEpromSondeur] = lecEprom(nomFic)

fid = fopen(nomFic,'r');
x = fread(fid);
x = x';
n = size(x, 2);
m = 2048*8;
if n < m
    x = [x zeros(1, m-n)];
end

x = reshape(x, 2048, 8);
loiEpromSondeur = x(1:1045, :);
loiEpromSonar = x(1046:end, :);
fclose(fid);
