% Lecture et traitement d'un fichier SAR brut
%
% Syntax
% CorFicSar( nomFic, Z, T, S, ... )
%
% Input Arguments 
%   nomFic  : nom du fichier
%   Z, T, S : Profil de thermo-salinometrie, Z en m, T en deg, S en 0/00
%
% Name-Value Pair Arguments 
%   subLig 	  : Index des lignes a utiliser pour le calcul de la moyenne
%   nomFicOut : Nom du fichier de sortie corrige
%
% Examples
%   nomFic = getNomFicDatabase('BATHYCEL_North-East_Atlantic_Winter.csv');
%   [Z, S, T] = lecCsv(nomFic);
%
%   nomFic = getNomFicDatabase('SAR_0035.01.im')
%   nomFicOut = fullfile(my_tempdir, 'SAR_0035.01.im')
%   CorFicSar( nomFic, Z, T, S, 1:50 );
%   CorFicSar( nomFic, Z, T, S, nomFicOut, 1:50 );
%   CorFicSar( nomFic, Z, T, S, nomFicOut );
%
%   [BT, HB, HT] = LecFicSar( nomFicOut, 'BT', 'HB', 'HT' );
%   fig = figure; imagesc(-BT); colorbar; axis xy; colormap(gray(256)); SourisButtonClbk('Figure', fig);
%
% See also getNbEnrFicSar LecFicSar LecFicAntennes Authors
% Authors : JMA
%------------------------------------------------------------------------------


function CorFicSar( nomFic, Z, T, S , varargin )

%% Mission Zaiango

alphaBabMaison = 32.5; %#ok<*NASGU>
alphaTriMaison = 35.6;
OffetTribordSurBabord = 8;	%10;


alphaBabMaison = AttenuationGarrison( 170, Z, T, S ) %#ok<*NOPRT>
alphaTriMaison = AttenuationGarrison( 190, Z, T, S )

% Mission Essar991
% ----------------

%alphaBabMaison = 43.5;
%alphaTriMaison = 47.0;
%OffetTribordSurBabord = 0;	%10;

% Mission Essar992
% ----------------

%alphaBabMaison = 23;
%alphaTriMaison = 26;
%OffetTribordSurBabord = 5;	%10;

% Lecture du nombre d'octets du fichier
% -------------------------------------

nbOcFic = sizeFic( nomFic );
if( nbOcFic == -1 )
	return;
end

% Lecture du fichier de directivite des antennes
% ----------------------------------------------

nomFicBab = getNomFicDatabase('SAR_saev12.ifr');
nomFicTri = getNomFicDatabase('SAR_sarer43.ifr');
[AntennesAnglesBab, DVb] = LecFicAntennes( nomFicBab );
[AntennesAnglesTri, DVt] = LecFicAntennes( nomFicTri );

% Centrage des diagrammes
% -----------------------

offsetBab = -7;
offsetTri = -1.5;
AntennesAnglesBab = AntennesAnglesBab - offsetBab;
AntennesAnglesTri = AntennesAnglesTri - offsetTri;

% Orientation des diagrammes par rapport a l'axe du SAR
% Les diagrammes mesures sont une vue de l'avant
% -----------------------------------------------------

%AntennesAnglesBab =  AntennesAnglesBab;
 AntennesAnglesTri = -AntennesAnglesTri;

% Orientation des diagrammes en lateral
% -------------------------------------

depointageBab = (90-25);
depointageTri = (90-25);
AntennesAnglesBab = AntennesAnglesBab + depointageBab;   
AntennesAnglesTri = AntennesAnglesTri + depointageTri;

DVb = DVb - max(DVb);
DVt = DVt - max(DVt);

% Initialisations
% ---------------

nb_oc_enr = 7644;
nbEnr = nbOcFic / nb_oc_enr;
subLig = 1:nbEnr;
nomFicOut = [];

for i=1:length(varargin)
	if( isnumeric(varargin{i}) )	% C'est l'echantillonage des enregistrements
		subLig = varargin{i};
	elseif ischar(varargin{i})
		nomFicOut = varargin{i};
		if exist(nomFicOut, 'file')
            [rep, flag] = my_questdlg('Ce fichier existe déjà, voulez-vous poursuivre ?', 'Init', 2);
			if ~flag || (rep == 2)
				return
			end
		end
	end
end

% -------------------- 
% Ouverture du fichier

fidIn = fopen(nomFic, 'r');
if ~isempty( nomFicOut )
	fidOut =  fopen(nomFicOut, 'w+');
end


sub = find(subLig <= nbEnr);
subLig = subLig(sub);

nLig = size(subLig, 2);

HB = zeros(1, nLig);
HT = zeros(1, nLig);
Immersion = zeros(1, nLig);
Cap = zeros(1, nLig);
Vitesse = zeros(1, nLig);

% -----------------
% Lecture des infos

meanReflecAngulaireB = zeros(1, 900);
nbReflecAngulaireB = zeros(1, 900);

meanReflecAngulaireT = zeros(1, 900);
nbReflecAngulaireT = zeros(1, 900);

meanReflecTempsB = zeros(1, 3000);
nbReflecTempsB = zeros(1, 3000);

meanReflecTempsT = zeros(1, 3000);
nbReflecTempsT = zeros(1, 3000);

%d = (1:3000) * 0.25;
%ARkm = 2 * d / 1000;	% Trajet Aller/Retour en km

for i=1:nLig

	% Calcul de la position du debut de l'enregistrement
	% --------------------------------------------------

	position = (subLig(i) - 1) * nb_oc_enr;

	% Lecture de l'enregistrement
	% ---------------------------

	status = fseek( fidIn, position, 'bof');
	enr = (fread( fidIn, nb_oc_enr, 'char'))';

	% Recuperation des donnees
	% ------------------------

	HB(i) = str2double(char(enr(33:38)));
	HT(i) = str2double(char(enr(40:45)));
	charDate = char(enr(190:197));
	annee = str2num(charDate(7:8));
	if( annee > 80 )
		siecle = 19;
	else
		siecle = 20;
	end
	
	date = dayStr2Ifr( sprintf('%s%d%s', charDate(1:6), siecle, charDate(7:8)) );
	charHeure = char(enr(199:210));
	heure = hourStr2Ifr( charHeure );

	switch char(enr(213))	% Il y a un probleme de decalage d'1 octet dans les fichiers
	case ','
		sublat = 212:224;
		sublon = 226:239;
	otherwise
		sublat = 213:224;
		sublon = 227:239;
	end
	Immersion(i) = str2double(char(enr(241:249)));
	Cap(i) = str2double(char(enr(281:285)));

	NIb = enr(792:3791);
	NIt = enr(3832:6831);
	Sondeur = enr(6871:6871+760-1);

	% Initialisations
	% ---------------

	angle_pixels = calAngle(HB(i));
	iAngle = 1 + floor(angle_pixels * 10);
	iAngle(find(isnan(iAngle))) = 1;
	IH = HB(i) / 0.25;
	NIb(1:IH-1) = 0;
	NIt(1:IH-1) = 0;
	
	[nuprom, nuLoi, GFb, GFt, SHb, SHt] = getNuLoi(date, heure);
	[D, a0, DeltaG, LimiteGMax] = getParamEprom(nuprom, nuLoi);
	TVG_SAR = TvgSar(D, a0, DeltaG, LimiteGMax);		% Loi theorique

	GM = 36;
	GC = 20 * log10(51);
	NEb = 227;
	NEt = 225;

	% Passage en dB
	% -------------

	NCb = 2 * reflec_Enr2dB(NIb);
	NCt = 2 * reflec_Enr2dB(NIt);

	% Calcul du Niveau acoustique recu
	% --------------------------------

	NRb = NCb - SHb - TVG_SAR - GFb - GM - GC;
	NRt = NCt - SHt - TVG_SAR - GFt - GM - GC;

	% Traitements des antennes
	% ------------------------

	sub = find(~isnan(angle_pixels));
	indicesBab = interp1(AntennesAnglesBab, 1:size(AntennesAnglesBab, 2), angle_pixels(sub), 'nearest');
	indicesTri = interp1(AntennesAnglesTri, 1:size(AntennesAnglesTri, 2), angle_pixels(sub), 'nearest');

	TVGb = TvgSarMaison(alphaBabMaison, HB(i));
	TVGt = TvgSarMaison(alphaTriMaison, HB(i));

	%BSb(sub) = NRb(sub) - NEb - DVb(indicesBab) + TVGb(sub);
	%BSt(sub) = NRt(sub) - NEt - DVt(indicesTri) + TVGt(sub) + OffetTribordSurBabord;
	BSb(sub) = NRb(sub) - NEb + TVGb(sub);
	BSt(sub) = NRt(sub) - NEt + TVGt(sub) + OffetTribordSurBabord;

	% Traitement de Lambert
	% --------------------

	%NCb = NCb - Lambert(angle_pixels);
	%NCt = NCt - Lambert(angle_pixels);

	% Calcul des statistiques voie babord
	% -----------------------------------

	sub = find(~isnan(NCb));
	meanReflecAngulaireB(iAngle(sub)) = meanReflecAngulaireB(iAngle(sub)) + BSb(sub);
	nbReflecAngulaireB(iAngle(sub)) = nbReflecAngulaireB(iAngle(sub)) + 1;

	meanReflecTempsB(sub) = meanReflecTempsB(sub) + BSb(sub);
	nbReflecTempsB(sub) = nbReflecTempsB(sub) + 1;

	% Calcul des statistiques voie tribord
	% ------------------------------------

	sub = find(~isnan(NCt));
	meanReflecAngulaireT(iAngle(sub)) = meanReflecAngulaireT(iAngle(sub)) + BSt(sub);
	nbReflecAngulaireT(iAngle(sub)) = nbReflecAngulaireT(iAngle(sub)) + 1;

	meanReflecTempsT(sub) = meanReflecTempsT(sub) + BSt(sub);
	nbReflecTempsT(sub) = nbReflecTempsT(sub) + 1;

	% Ecriture de l'enregistrement dans le fichier de sortie
	% ------------------------------------------------------

	if ~isempty( nomFicOut )
		%enr(792:3791) = reflec_dB2Enr((BSb + SHb + GFb + GM + GC + NEb)/2);
		%enr(3832:6831) =  reflec_dB2Enr((BSt + SHb + GFb + GM + GC + NEb)/2);
		
		%enr(792:3791)  = 10.^((BSb+100)/20);
		%enr(3832:6831) = 10.^((BSt+100)/20);
		
		sub = find(~isnan(angle_pixels));
		minBSb = min(BSb(sub));
		maxBSb = max(BSb(sub));
		minBSt = min(BSt(sub));
		maxBSt = max(BSt(sub));
		%disp(sprintf('minBSb=%f  maxBSb=%f  minBSt=%f  maxBSt=%f', minBSb, maxBSb, minBSt, maxBSt));
		
		
		subNaN = find(isnan(BSb));
		subNonNaN = find(~isnan(BSb));
		BSb(subNaN) = interp1(subNonNaN, BSb(subNonNaN), subNaN, 'nearest');

		subNaN = find(isnan(BSt));
		subNonNaN = find(~isnan(BSt));
		BSt(subNaN) = interp1(subNonNaN, BSt(subNonNaN), subNaN, 'nearest');


		enr(792:3791)  = max(0, min(255, 2 * (BSb + 110)));
		enr(3832:6831) = max(0, min(255, 2 * (BSt + 110)));
		%enr(792:3791)
		%pause
		

		subAngleNul = find(isnan(angle_pixels));
		enr( 791 + subAngleNul) = 0;
		enr(3831 + subAngleNul) = 0;

		fwrite( fidOut, enr, 'char');
	end


	% Affichage
	% ---------

	if(mod(i, 1) == 0)
		 fprintf('%d/%d %d %s %s HB=%fm HT=%fm I=%f C=%f V=%f nuLoi=%d\n', ...
		i, nLig, subLig(i), charDate, charHeure, HB(i), HT(i), Immersion(i), Cap(i), Vitesse(i), nuLoi ) ;
	end
end

% Fermeture du fichier
% --------------------

fclose(fidIn);
if ~isempty( nomFicOut )
	fclose(fidOut);
end

% Calcul des courbes de  relectivite moyenne
% ------------------------------------------

sub = find( nbReflecAngulaireB ~= 0 );
meanReflecAngulaireB(sub) = meanReflecAngulaireB(sub) ./ nbReflecAngulaireB(sub);

sub = find( nbReflecAngulaireT ~= 0 );
meanReflecAngulaireT(sub) = meanReflecAngulaireT(sub) ./ nbReflecAngulaireT(sub);

sub = find( nbReflecTempsB ~= 0 );
meanReflecTempsB(sub) = meanReflecTempsB(sub) ./ nbReflecTempsB(sub);
meanReflecTempsT(sub) = meanReflecTempsT(sub) ./ nbReflecTempsT(sub);

gainMin = min([meanReflecAngulaireB meanReflecAngulaireT DVb DVt]);

% Trace de la reflectivite angulaire
% ----------------------------------

figure;
angles = (1:900)/10;
plot(angles, meanReflecAngulaireB, 'r'); hold on;
plot(angles, meanReflecAngulaireT, 'g'); grid on;
save '/tmp/diag.mat' angles meanReflecAngulaireB meanReflecAngulaireT

title(nomFic);

%plot(AntennesAnglesBab, DVb, 'r');
%plot(AntennesAnglesTri, DVt, 'g');
%set(gca, 'XLim', [0 90]);

% Trace de la reflectivite angulaire
% ----------------------------------

figure;
plot(meanReflecTempsB, 'r'); hold on;
plot(meanReflecTempsT, 'g'); grid on;
title(nomFic);

% Calcul de l'angle d'emission pour une hauteur H (hypothese fond plat)
% ---------------------------------------------------------------------

function angles = calAngle( H )

if( H == 0)
	angles = NaN(1,3000);
	return;
end

id = floor(H / 0.25);
d = (1:3000) * 0.25;
d(1:id) = NaN;
angles = (180 / pi ) * acos(H ./ d);
