% Lecture et traitement d'un fichier SAR brut
%
% Syntax
%   ExtracSarIm( nomFicIn, nomFicOut, subLig )
%
% Input Arguments 
%   nomFicIn  : nom du fichier d'entree
%   nomFicOut : nom du fichier de sortie
%   subLig    : index des lignes a utiliser pour le calcul de la moyenne
%
% Examples
%   nomFicIn = getNomFicDatabase('SAR_0035.01.im')
%   nomFicOut = fullfile(my_tempdir, 'SAR_0035.01.im')
%   ExtracSarIm( nomFicIn, nomFicOut, 1:50);
%   B = LecFicSar( nomFicOut, 'B');
%   figure; imagesc(-B); colorbar; colormap(gray(256));
%
% See also getNbEnrFicSar LecFicSar LecFicAntennes Authors
% Authors : JMA
%------------------------------------------------------------------------------

function ExtracSarIm(nomFicIn, nomFicOut, subLig)

%% Lecture du nombre d'octets du fichier

nbOcFic = sizeFic(nomFicIn);
if nbOcFic == -1
    return
end

%% Initialisations

nb_oc_enr = 7644;
nbEnr = nbOcFic / nb_oc_enr;

if exist(nomFicOut, 'file')
    rep = questdlg('Ce fichier existe déjà, que faut''il faire ?', 'ATTENTION', ...
        'Quitter', 'Continuer', 'Quitter');
    if strcmp(rep, 'Quitter')
        return
    end
end

%% Ouverture du fichier

fidIn = fopen(nomFicIn, 'r');
if ~isempty(nomFicOut)
    fidOut = fopen(nomFicOut, 'w+');
end


sub = find(subLig <= nbEnr);
subLig = subLig(sub);

nLig = size(subLig, 2);

HB = zeros(1, nLig);
HT = zeros(1, nLig);
Immersion = zeros(1, nLig);
Cap = zeros(1, nLig);
Vitesse = zeros(1, nLig);

%% Lecture des infos

for i=1:nLig
    
    % Calcul de la position du début de l'enregistrement
    
    position = (subLig(i) - 1) * nb_oc_enr;
    
    % Lecture de l'enregistrement
    
    status = fseek( fidIn, position, 'bof');
    enr = (fread( fidIn, nb_oc_enr, 'char'))';
    
    % Récuperation des données
    
    HB(i) = str2double(char(enr(33:38)));
    HT(i) = str2double(char(enr(40:45)));
    charDate = char(enr(190:197));
    date = dayStr2Ifr( sprintf('%s19%s',charDate(1:6), charDate(7:8)) );
    charHeure = char(enr(199:210));
    heure = hourStr2Ifr( charHeure );
    
    switch char(enr(213))	% Il y a un probleme de decalage d'1 octet dans les fichiers
    case ','
        sublat = 212:224;
        sublon = 226:239;
    otherwise
        sublat = 213:224;
        sublon = 227:239;
    end
    Immersion(i) = str2double(char(enr(241:249)));
    Cap(i) = str2double(char(enr(281:285)));
    
    % Ecriture de l'enregistrement dans le fichier de sortie
    
    fwrite( fidOut, enr, 'char');
    
    % Affichage
    
    if(mod(i, 10) == 0)
         fprintf('%d/%d %d %s %s HB=%fm HT=%fm I=%f C=%f V=%f\n', ...
            i, nLig, subLig(i), charDate, charHeure, HB(i), HT(i), Immersion(i), Cap(i), Vitesse(i) ) ;
    end
end

%% Fermeture du fichier

fclose(fidIn);
fclose(fidOut);
