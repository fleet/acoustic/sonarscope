% Lecture d'un fichier d'antennes du SAR
%
% Syntax
%   [angles, gain] = LecFicAntennes( nomFic )
%
% Input Arguments
%   nomFic  : Nom du fichier
%
% Output Arguments
%   angles : Angles du diagrammes de directivite
%   gain   : Gain (dB) du diagrammes de directivite
%
% Examples
%   nomFicBab = getNomFicDatabase('SAR_saev12.ifr')
%   nomFicTri = getNomFicDatabase('SAR_sarer43.ifr')
%   [anglesBab, gainBab] = LecFicAntennes( nomFicBab );
%   [anglesTri, gainTri] = LecFicAntennes( nomFicTri );
%
%	depointageBab = 25; offsetBab = -7;
%	depointageTri = 25; offsetTri = -1.5;
%
%	modeleBab = AntenneSinc( anglesBab, [2.5, offsetBab,  60] );
%	modeleTri = AntenneSinc( anglesTri, [1.95, offsetTri, 52] );
%
%	representation = 3; % 1= trigonometrique, 2=Bernard Leduc, 3=Sonar vu de l'arriere, 4=Traitement
%	switch representation
%	case 1	% Trigonometrique (Convention de traitement)
%		anglesBab = anglesBab;
%		anglesTri = anglesTri;
%		labelRepresentation = 'Representation trigonometrique';
%	case 2	% Bernard Leduc
%		anglesBab = -anglesBab;
%		anglesTri = -anglesTri;
%		labelRepresentation = 'Representation Bernard Leduc';
%	case 3	% Sonar vu de l'arriere
%		anglesBab = 180 + (anglesBab - offsetBab + depointageBab);
%		anglesTri =     - (anglesTri - offsetTri + depointageTri);
%		labelRepresentation = 'Representation Sonar vu de l''arriere';
%	case 4	% Sonar vu de l'arriere
%		anglesBab = -(anglesBab - offsetBab) + depointageBab;
%		anglesTri =  (anglesTri - offsetTri) + depointageTri;
%		labelRepresentation = 'Representation pour traitement';
%	end
%
%	figure(1); hold off; polar(anglesBab * pi / 180, 70+gainBab); title(['Babord  ' labelRepresentation]);
%	figure(2); hold off; polar(anglesTri * pi / 180, 70+gainTri); title(['Tribord  ' labelRepresentation]);
%	figure(1); hold on; polar(anglesBab * pi / 180, 70+modeleBab, 'r');
%	figure(2); hold on; polar(anglesTri * pi / 180, 70+modeleTri, 'r');
%
%	figure(3)
%	hold off; plot(anglesBab, gainBab); grid on;
%	hold on;
%	plot( anglesBab, 2 * modeleBab, 'r' ); grid on; zoom on; title(['Babord  ' labelRepresentation]);
%
%	figure(4)
%	hold off; plot(anglesTri, gainTri); grid on;
%	hold on;
%	plot( anglesTri, 2 * modeleTri, 'r' ); grid on; zoom on; title(['Tribord  ' labelRepresentation]);
%
%	gainMin = min([gainBab gainTri]);
%	gainMin = -70;
%	figure(5); hold off;  polar(anglesBab * pi / 180, gainBab - gainMin, 'r');
%	hold on; polar(anglesTri * pi / 180, gainTri - gainMin, 'g'); title(labelRepresentation);
%	polar(anglesBab * pi / 180, 2 * modeleBab - gainMin, 'b');
%	polar(anglesTri * pi / 180, 2 * modeleTri - gainMin, 'b');
%
% See also CorFicsar Authors
% Authors : JMA
%------------------------------------------------------------------------------

function [angles, gain] = LecFicAntennes( nomFic )

M = dlmread(nomFic, '\t');

M(end,:) = [];

gain = M(:,2);
angles = M(:,1);
sub = find(angles > 180);
angles(sub) = angles(sub) - 360;

[angles, index] = sort(angles);
gain = 4 * reflec_Enr2dB(gain(index));

angles = angles';
gain = gain';
