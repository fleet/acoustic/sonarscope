% Lecture d'une image TRIAS !! PB DATA
%
% Syntax
%   A = LecFicImageTrias(nomFic)
%   A = LecFicImageTrias(nomFic, subLig, subCol)
%
% Input Arguments 
%   nomFic : Nom du fichier
%
% Name-Value Pair Arguments 
%	subLig : Liste des lignes a lire
%	subCol : Liste des colonnes a lire
%
% Output Arguments
%   A : Image
%
% Examples
%   A = LecFicImageTrias( '/tmp/pppp.imo', 1:5:1820, 1:5:4900 );
%   fig = figure; imagesc(-A); colorbar; colormap(gray(256));
%
% See also Authors
% Authors : JMA
%------------------------------------------------------------------------------

function A = LecFicImageTrias(nomFic, varargin)

fid = fopen(nomFic, 'r');
entete = fread(fid, 32, 'int32');

nbRows = entete(18);
nbCol = entete(19);
itpix = entete(21);

if nargin <= 1
	subLig = 1:nbRows;
else
	subLig = varargin{1};
end

if nargin == 3
	subCol = varargin{2};
else
	subCol = 1:nbCol;
end

nbLigA = size(subLig, 2);
nbColA = size(subCol, 2);
A = uint8(zeros(nbLigA, nbColA));

fprintf('\tnbLig = %d   nbCol = %d\n', nbRows, nbCol);
fprintf('\tTaille du pixel : %f m\n', entete(22) / 1000);

for iLig = 1:nbLigA
	offset = nbCol * subLig(iLig);
	fseek(fid, offset, 'bof');
	ligne = fread(fid, nbCol, 'int8');
	ligne = double(ligne');
	A(iLig, :) = ligne(subCol);
end
A = double(A);

fclose(fid);
