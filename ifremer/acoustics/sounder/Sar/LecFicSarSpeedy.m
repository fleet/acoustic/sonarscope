% Lecture d'un fichier SAR brut
%
% Syntax
%   [...] = LecFicSarSpeedy(nomFic, ...)
%
% Input Arguments
%   nomFic  : Nom du fichier
%
% Name-Value Pair Arguments
%   subl  : index des lignes a extraire
%
% Output Arguments
%   BT : Donnees sonar
%
% Examples
%   nomFic = getNomFicDatabase('SAR_0035.01.im')
%   BT = LecFicSarSpeedy( nomFic, 1:500 );
%     fig = figure; imagesc(-BT); colorbar; colormap(gray(256));
%
% See also LecFicSar Authors
% Authors : JMA
%------------------------------------------------------------------------------

function [BT, str] = LecFicSarSpeedy(nomFic, varargin)

% Maintenance Auto : try
if nargin == 0
    help LecFicSarSpeedy
    return
end

if ~exist(nomFic)
    fprintf('Le fichier %s n''existe pas\n', nomFic)
    return
end

% -------------------------------------
% Lecture du nombre d'octets du fichier

nbOcFic = sizeFic(nomFic);
if nbOcFic == -1
    return
end

% ---------------
% Initialisations

nb_oc_enr   = 7644;
nbEnr       = nbOcFic / nb_oc_enr;

[varargin, subl] = getPropertyValue(varargin, 'subl', 1:nbEnr); %#ok<ASGLU>

if length(subl) ~= 1
    pas = unique(diff(subl));
    if length(pas) ~= 1
        disp('extraction contigue SVP')
        return
    end
    if pas ~= 1
        disp('extraction contigue SVP')
        return
    end
end

% --------------------
% Ouverture du fichier

fidIn = fopen(nomFic, 'r');

% ---------------------------------------------------
% Positionnement sur le premier enregistrement a lire

position = (subl(1) - 1) * nb_oc_enr;
status   = fseek( fidIn, position, 'bof');

% ----------------------------------------
% Lecture du bloc d'enregistrements a lire

enr = (fread( fidIn, [nb_oc_enr length(subl)], 'char'))';

% ------------------------------------------------------------
% Assemblage des donnees pour obtenir une representation sonar
% classique

BT = [fliplr(enr(:, 3832:6831)) enr(:, 792:3791)];

% -----------------------------------------
% Lecture de la date du debut de de la fin

position = (subl(1) - 1) * nb_oc_enr;
status   = fseek( fidIn, position, 'bof');
enr = (fread( fidIn, nb_oc_enr, 'char'))';
charDate = char(enr(190:197));
annee = str2num(charDate(7:8));
if annee > 80
    siecle = 19;
else
    siecle = 20;
end
dateDeb = sprintf('%s%d%s', charDate(1:6), siecle, charDate(7:8));
heureDeb = char(enr(199:210));

position = (subl(end) - 1) * nb_oc_enr;
status   = fseek( fidIn, position, 'bof');
enr = (fread( fidIn, nb_oc_enr, 'char'))';
charDate = char(enr(190:197));
annee = str2num(charDate(7:8));
if annee > 80
    siecle = 19;
else
    siecle = 20;
end
dateFin = sprintf('%s%d%s', charDate(1:6), siecle, charDate(7:8));
heureFin = char(enr(199:210));

[rep, nom, ext] = fileparts(nomFic);
str = sprintf('%s : %5d lines : %s  %s - %s  %s', [nom ext], length(subl), dateDeb, heureDeb, dateFin, heureFin);

% --------------------
% Fermeture du fichier

fclose(fidIn);

% Maintenance Auto : catch
% Maintenance Auto :     err('Sar', 'LecFicSarSpeedy', lasterr)
% Maintenance Auto : end
