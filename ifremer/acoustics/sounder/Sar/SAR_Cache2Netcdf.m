% [flag, dataFileName] = FtpUtils.getSScDemoFile('0014.04.im', 'OutputDir', 'D:\Temp\ExData');
%
% SAR_Cache2Netcdf(dataFileName)

function flag = SAR_Cache2Netcdf(dataFileName)

%% Cr�ation du fichier Netcdf

[flag, nomFicNc] = SScCacheNetcdfUtils.createSScNetcdfFile(dataFileName);
if ~flag
    return
end

%% Transfert des donn�es Ssc_Sar

flag = SScCacheNetcdfUtils.Cache2Netcdf(nomFicNc, dataFileName, 'Sar');
if ~flag
    return
end

%% The End

flag = 1;
