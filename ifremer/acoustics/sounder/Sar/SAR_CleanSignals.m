% nomFic = 'D:\Temp\ExData\ExSAR\0014.04.im';
% flag = SAR_CleanSignals(nomFic)

function flag = SAR_CleanSignals(nomFic)

if ~iscell(nomFic)
    nomFic = {nomFic};
end

flag = Sar2ssc(nomFic);
if ~flag
    return
end

Format1 = 'V�rification de la %s.\n\nUne fen�tre SignalScope va s''ouvrir, le signal de %s est en noir et une version filtr�e est en rouge. Zoomez sur le signal pour voir si la version filtr�e est meilleure (elle rem�die souvent � des marches d''escalier dues � une quantification de la mesure de positionnemen).\n\nSi vous conservez le filtre et que vous clickez sur Save et Quit c''est lui qui sera sauvegard�.';
Format2 = 'Checking %s.\n\nA SignalScope window is going to be open, %s signal is the black one, the red one is a filtered version. Zoom on the signal to check if the filter is efficient (it often solves problems of stair steps due to signal quantification).\n\nIf you conserve the filter AND you click on Save and Quit il will be the red curve which will be saved.';
Format3 = 'V�rification de la %s.\n\nUne fen�tre SignalScope va s''ouvrir. Zoomez sur le signal pour voir si un filtrage s''impose.';
Format4 = 'Checking %s.\n\nA SignalScope window is going to be opened. Zoom on the signal to check if a filter is necessary.';

%% Traitement des fichiers

N = length(nomFic);
str1 = 'Contr�le qualit� des signaux des fichiers SDF';
str2 = 'SDF Quality Control processing';
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
    my_waitbar(k, N, hw);
    
    [flag, Data] = SSc_ReadDatagrams(nomFic{k}, 'Ssc_Sar', 'Memmapfile', -1);
    if ~flag
        return
    end
    
    Longitude  = Data.Longitude(:);
    Latitude   = Data.Latitude(:);
    SonarTime  = Data.Time;
    Immersion  = Data.Immersion(:);
    Heading    = Data.Heading(:);
    clear Data
    
    %% Calcul de la navigation du poisson
    
    flag = checkFishLatLon(nomFic{k}, SonarTime, Latitude, Longitude, Heading, Immersion, Format1, Format2, Format3, Format4);
    if ~flag
        break
    end
    
    %% Affichage de l'image dans SonarScope pour v�rifier la hauteur
    
    str1 = sprintf('Voulez-vous recalculer ou nettoyer le signal de hauteur du fichier "%s" ?', nomFic{k});
    str2 = sprintf('Do you want to reprocess or modify the "Height" for file "%s" ?', nomFic{k});
    [rep, flag] = my_questdlg(Lang(str1,str2));
    if ~flag
        return
    end
    if rep == 1
        if k == 1
            messageCheckHeight();
        end
        
        I0 = cl_image_I0;
        [flag, b] = import_SAR(I0, nomFic{k}, 'subImage', 2);
        if ~flag
            break
        end
        
        c = SonarScope(b(1), 'visuProfilY', 1, 'profilY.Type', 'Height', 'visuProfilX', 1, 'ButtonSave', 1);
        SonarHeight = get(c(1), 'SonarHeight');
        
        flag = save_signal(nomFic{k}, 'Ssc_Sar', 'Height', SonarHeight);
        if ~flag
            return
        end
    end
    
    %% On demande si on veut traiter les autres fichiers
    
    if k ~= N
        flag = question_ContinueNextFile();
        if ~flag
            break
        end
    end
    
end
my_close(hw, 'MsgEnd')


function flag = checkFishLatLon(nomFic, SonarTime, FishLatitude, FishLongitude, Heading, Immersion, Format1, Format2, Format3, Format4)

[~, nomFicSeul] = fileparts(nomFic);

% Format1 = 'V�rification de la %s.\n\nUne fen�tre SignalScope va s''ouvrir, le signal de %s est en noir et une version filtr�e est en rouge. Zoomez sur le signal pour voir si la version filtr�e est meilleure (elle rem�die souvent � des marches d''escalier dues � une quantification de la mesure de positionnemen).\n\nSi vous conservez le filtre et que vous clickez sur Save et Quit c''est lui qui sera sauvegard�.';
% Format2 = 'Checking %s.\n\nA SignalScope window is going to be open, %s signal is the black one, the red one is a filtered version. Zoom on the signal to check if the filter is efficient (it often solves problems of stair steps due to signal quantification).\n\nIf you conserve the filter AND you click on Save and Quit il will be the red curve which will be saved.';
% Format3 = 'V�rification de la %s.\n\nUne fen�tre SignalScope va s''ouvrir. Zoomez sur le signal pour voir si un filtrage s''impose.';
% Format4 = 'Checking %s.\n\nA SignalScope window is going to be opened. Zoom on the signal to check if a filter is necessary.';

%% Trac� de la navigation

% FigUtils.createSScFigure; 
% h(1) = subplot(); 

%% Contr�le de la latitude et de la longitude du bateau

str1 = sprintf('Voulez-vous v�rifier la latitude du poisson pour "%s" ?', nomFicSeul);
str2 = sprintf('Check latitude and longitude of the fish for "%s" ?', nomFicSeul);
[rep, flag] = my_questdlg(Lang(str1,str2));
if ~flag
    return
end
if rep == 1
    FiltreLonLat.Type = 4;
    
    str1 = sprintf(Format1, 'Longitude', 'Longitude');
    str2 = sprintf(Format2, 'Longitude', 'Longitude');
    my_warndlg(Lang(str1,str2), 1);
    [FishLongitude, FiltreLonLat] = controleSignalNEW('FishLongitude', FishLongitude, 'Filtre', FiltreLonLat);
    flag = save_signal(nomFic, 'Ssc_Sar', 'Longitude', FishLongitude);
    if ~flag
        return
    end
        
    str1 = sprintf(Format1, 'Latitude', 'Latitude');
    str2 = sprintf(Format2, 'Latitude', 'Latitude');
    my_warndlg(Lang(str1,str2), 1);
    FishLatitude = controleSignalNEW('FishLatitude', FishLatitude, 'Filtre', FiltreLonLat);
    flag = save_signal(nomFic, 'Ssc_Sar', 'Latitude', FishLatitude);
    if ~flag
        return
    end
end

%% Contr�le du cap calcul�

str1 = sprintf('Voulez-vous v�rifier le cap du poisson pour "%s" ?', nomFicSeul);
str2 = sprintf('Check the fish heading for "%s" ?', nomFicSeul);
[rep, flag] = my_questdlg(Lang(str1,str2));
if ~flag
    return
end
if rep == 1
    FiltreHeading.Type  = 4;
    str1 = sprintf(Format3, 'Heading', 'Heading');
    str2 = sprintf(Format4, 'Heading', 'Heading');
    my_warndlg(Lang(str1,str2), 1);
    [Heading, FiltreHeading] = controleSignalNEW('Heading', Heading, 'Filtre', FiltreHeading, 'DataType', SampleType.ANGLE);
    
    str1 = 'Que voulez-vous faire de ce cap ?';
    str2 = 'What do you want to do with this heading ?';
    str3 = 'L''tiliser tel que';
    str4 = 'Use it as is';
    str5 = 'Le recalculer � partir de la navigation';
    str6 = 'Compute from the navigation';
    [rep, flag] = my_questdlg(Lang(str1,str2), Lang(str3,str4), Lang(str5,str6));
    if ~flag
        return
    end
    if rep == 2
        Heading = calCapFromLatLon(FishLatitude, FishLongitude, 'Time', SonarTime);
        Heading = controleSignalNEW('Heading', Heading, 'Filtre', FiltreHeading);
    end
    
    flag = save_signal(nomFic, 'Ssc_Sar', 'Heading', Heading);
    if ~flag
        return
    end
end

%% Contr�le de l'immersion

str1 = sprintf('Voulez-vous v�rifier l''immersion du poisson pour "%s" ?', nomFicSeul);
str2 = sprintf('Check the fish Immersion for "%s" ?', nomFicSeul);
[rep, flag] = my_questdlg(Lang(str1,str2));
if ~flag
    return
end
if rep == 1
    str1 = sprintf(Format3, 'Immersion', 'Immersion');
    str2 = sprintf(Format4, 'Immersion', 'Immersion');
    my_warndlg(Lang(str1,str2), 1);
    Immersion = controleSignalNEW('Immersion', Immersion);
    flag = save_signal(nomFic, 'Ssc_Sar', 'Immersion', Immersion);
    if ~flag
        return
    end
end

%% Contr�le de l'heure

str1 = sprintf('Voulez-vous v�rifier l''heure pour "%s" ?', nomFicSeul);
str2 = sprintf('Check the time for "%s" ?', nomFicSeul);
[rep, flag] = my_questdlg(Lang(str1,str2));
if ~flag
    return
end
if rep == 1
    str1 = sprintf(Format3, 'Time', 'Time');
    str2 = sprintf(Format4, 'Time', 'Time');
    my_warndlg(Lang(str1,str2), 1);
    timeMat = controleSignalNEW('SonarTime', SonarTime.timeMat);
    flag = save_signal(nomFic, 'Ssc_Sar', 'Time', timeMat);
    if ~flag
        return
    end
end
