function [flag, Mosa, nomDir] = SAR_CreatePseudoDTM(varargin)

[varargin, resol]    = getPropertyValue(varargin, 'Resol', []);
% [varargin, Backup] = getPropertyValue(varargin, 'Backup', []);
[varargin, nomDir]   = getPropertyValue(varargin, 'NomDir', []); %#ok<ASGLU>

Mosa = [];
I0 = cl_image_I0;
E0 = cl_ermapper([]);

if isempty(nomDir)
    nomDir = pwd;
end

%% Recherche des fichiers .im

[flag, liste] = uiSelectFiles('ExtensionFiles', '.im', 'RepDefaut', nomDir);
if ~flag || isempty(liste)
    return
end
nbProfils = length(liste);
nomDir = fileparts(liste{1});

%% Saisie de la résolution

[flag, resol] = get_gridSize('Value', -resol, 'Initial', 1.5);
if ~flag
    return
end

%% Backup frequency

[flag, Backup] = saisie_backupFrequency(nbProfils);
if ~flag
    return
end

%% Noms du fichier de sauvegarde de la mosaique de réflectivité

DataType = cl_image.indDataType('Bathymetry');
[nomFicErMapperLayer, flag] = initNomFicErs(E0, 'DTM', cl_image.indGeometryType('LatLong'), DataType, nomDir, 'resol', resol);
if ~flag
    return
end
[nomDir, TitreDepth] = fileparts(nomFicErMapperLayer);

%% Noms du fichier de sauvegarde de la mosaique des angles d'émission

DataType = cl_image.indDataType('TxAngle');
[nomFicErMapperAngle, flag] = initNomFicErs(E0, TitreDepth, cl_image.indGeometryType('LatLong'), DataType, nomDir, 'confirm', (get_LevelQuestion >= 3));
if ~flag
    return
end
[pppp, TitreAngle] = fileparts(nomFicErMapperAngle);%#ok

%% Récuperation des mosaiques existantes

Carto = [];
if exist(nomFicErMapperLayer, 'file') && exist(nomFicErMapperAngle, 'file')
    [flag, rep] = question_CompleteExistingMosaic;
    if ~flag
        return
    end
    if rep == 1
        [flag, Z_Mosa_All] = cl_image.import_ermapper(nomFicErMapperLayer);
        if ~flag
            return
        end
        Carto = get(Z_Mosa_All, 'Carto');
        
        [flag, A_Mosa_All] = cl_image.import_ermapper(nomFicErMapperAngle);
        if ~flag
            return
        end
    else
        Z_Mosa_All = [];
        A_Mosa_All = [];
    end
else
    Z_Mosa_All = [];
    A_Mosa_All = [];
end

%% Traitement des fichiers

SonarTVG_IfremerAlpha = [];
for k1=1:nbProfils
    fprintf('Processing file %d/%d : %s\n', k1, nbProfils, liste{k1});
    
    %% Lecture des layers "Range", "AcrossDistance", "AlongDistance" et "BeamDepressionAngle"
    
    [flag, c, Carto, SonarTVG_IfremerAlpha] = import_SAR(I0, liste{k1}, 'Carto', Carto, 'subImage', 2, ...
        'SonarTVG_IfremerAlpha', SonarTVG_IfremerAlpha);
    
    nbRows = c.nbRows;
    if nbRows <= 1
        Message = sprintf('The file %s is discarded because it has %d row.', liste{k1}, nbRows);
        my_warndlg(Message, 0);
        continue
    end
    
    FishLatitude   = get(c, 'FishLatitude');
    FishLongitude  = get(c, 'FishLongitude');
    Heading        = get(c, 'Heading');
    
    PseudoBathy = sonarLateral_PingRange2PingAcrossDist(c, 'resolutionX', resol, 'createNbp', 0);
    PseudoBathy = sonar_lateral_LayerDepth(PseudoBathy);
    TxAngle_PingAcrossDist = sonar_lateral_emission(PseudoBathy, 'useRoll', 0);
    
    [flag, LatLon] = sonar_calcul_coordGeo_SonarX(PseudoBathy, Heading, ...
        FishLatitude, FishLongitude);
    if ~flag
        continue
    end
    clear c
    
    nbRows = PseudoBathy.nbRows;
    nbCol = PseudoBathy.nbColumns;
    pas = floor(nbRows / ((nbRows * nbCol * 4) / 1e7)); % SizePhysTotalMemory
    
    NBlocks = ceil(nbRows/pas);
    pas = ceil(nbRows/NBlocks);
    
    for k2=1:pas:nbRows
        subl = max(1,k2-5):min(k2-1+5+pas, nbRows);
        fprintf('Processing pings [%d:%d] Fin=%d\n', subl(1), subl(end), nbRows);
        
        [flag, Z_Mosa, A_Mosa] = sonar_mosaique2(PseudoBathy, TxAngle_PingAcrossDist, LatLon(1), LatLon(2), resol, ...
            'AcrossInterpolation', 2, 'AlongInterpolation',  2, ...
            'suby', subl);
        if ~flag
            return
        end
        
        Z_Mosa.Writable = false;
        A_Mosa.Writable = false;
        
        if isempty(Z_Mosa_All)
            Z_Mosa_All = Z_Mosa;
            A_Mosa_All = A_Mosa;
        else
            [Z_Mosa_All, A_Mosa_All] = mosaique([Z_Mosa_All Z_Mosa], 'Angle', [A_Mosa_All A_Mosa], 'FistImageIsCollector', 1);
            clear Z_Mosa A_Mosa
        end
        Z_Mosa_All.Name = 'Reflectivity mosaic';
        A_Mosa_All.Name = 'Emission angle mosaic';
        Z_Mosa_All.Writable = false;
        A_Mosa_All.Writable = false;
    end
    clear PseudoBathy TxAngle_PingAcrossDist LatLon
    
    %% Sauvegarde des mosaiques dans des fichiers ErMapper
    
    if (mod(k1,Backup) == 0) || (k1 == nbProfils)
        Z_Mosa_All = majCoordonnees(Z_Mosa_All);
        A_Mosa_All = majCoordonnees(A_Mosa_All);
        
        [~, ImageName] = fileparts(nomFicErMapperLayer);
        Z_Mosa_All.Name = ImageName;
        
        [~, ImageName] = fileparts(nomFicErMapperAngle);
        A_Mosa_All.Name = ImageName;
        
        export_ermapper(Z_Mosa_All, nomFicErMapperLayer);
        export_ermapper(A_Mosa_All, nomFicErMapperAngle);
        
        % Déplacement Mikael
        deleteFileOnListe(cl_memmapfile.empty, gcbf)
        % Déplacement Mikael
    end
end
flag = 1;

clear Mosa
Mosa(1) = Z_Mosa_All;
Mosa(2) = A_Mosa_All;
