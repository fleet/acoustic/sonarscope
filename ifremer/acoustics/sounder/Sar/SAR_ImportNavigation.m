function SAR_ImportNavigation(nomFicSAR, nomFicNav)

if ~iscell(nomFicSAR)
    nomFicSAR = {nomFicSAR};
end

Carto = [];
N = length(nomFicSAR);
str1 = 'Importation de la navigation dans les .im';
str2 = 'Navigation Import in the .im files';
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
    my_waitbar(k, N, hw);
    [flag, Carto] = SAR_ImportNavigation_unitaire(nomFicSAR{k}, nomFicNav, Carto);
    if ~flag
        my_close(hw)
        return
    end
end
my_close(hw, 'MsgEnd')



function [flag, Carto] = SAR_ImportNavigation_unitaire(nomFicSAR, nomFicNav, Carto)

flag = Sar2ssc(nomFicSAR);
if ~flag
    return
end

[flag, Data] = SSc_ReadDatagrams(nomFicSAR, 'Ssc_Sar', 'Memmapfile', -1);
if ~flag
    return
end

T = Data.Time.timeMat;
LatitudeImage  = Data.Latitude(:);
LongitudeImage = Data.Longitude(:);
clear Data
DonneesModifiees = 0;
for k=1:length(nomFicNav)
    [flag, Latitude, Longitude, TExternalNavigation] = lecFicNav(nomFicNav{k});
    if ~flag
        messageErreurFichier(nomFicNav{k}, 'ReadFailure');
        return
    end
    
    TExternalNavigation = TExternalNavigation.timeMat;
    
    % Test si le fichier est dans la nav
    
    %         if any(T) > TExternalNavigation(end)
    if T(1) > TExternalNavigation(end)
        str1 = sprintf('Le fichier SAR "%s" est post�rieur � la navigation "%s".', nomFicSAR, nomFicNav{k});
        str2 = sprintf('"%s" is posterior to "%s".', nomFicSAR, nomFicNav{k});
        my_warndlg(Lang(str1,str2), 0, 'Tag', nomFicSAR);
        continue
    end
    
    %         if any(T) < TExternalNavigation(1)
    if T(end) < TExternalNavigation(1)
        str1 = sprintf('Le fichier SAR "%s" est ant�rieur � la navigation "%s".', nomFicSAR, nomFicNav{k});
        str2 = sprintf('"%s" is anterior to "%s".', nomFicSAR, nomFicNav{k});
        my_warndlg(Lang(str1,str2), 0, 'Tag', nomFicSAR);
        continue
    end
    
    LongitudeSimrad = my_interp1_longitude(TExternalNavigation, Longitude, T);
    LatitudeSimrad  = my_interp1(TExternalNavigation, Latitude,  T);
    
    subNonNaN = find(~isnan(LongitudeSimrad(:,1)));
    if ~isempty(subNonNaN)
        LatitudeImage(subNonNaN, 1)  = LatitudeSimrad(subNonNaN, 1);
        LongitudeImage(subNonNaN, 1) = LongitudeSimrad(subNonNaN, 1);
        DonneesModifiees = 1;
    end
end

if DonneesModifiees
    flag = save_signal(nomFicSAR, 'Ssc_Sar', 'Latitude', LatitudeImage);
    if ~flag
        return
    end
    
    flag = save_signal(nomFicSAR, 'Ssc_Sar', 'Longitude', LongitudeImage);
    if ~flag
        return
    end
end


