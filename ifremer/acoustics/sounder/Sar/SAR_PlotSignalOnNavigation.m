function SAR_PlotSignalOnNavigation(nomFic, Invite, varargin)

% [varargin, Carto] = getPropertyValue(varargin, 'Carto', []);

if ~iscell(nomFic)
    nomFic = {nomFic};
end

NbFic = length(nomFic);
nbColors = 64;
Colors = jet(nbColors);

Ident = rand(1);

Fig = figure;
set(Fig, 'UserData', Ident)
hold on;

%% Boucle sur les fichiers

XLim = [Inf -Inf];
YLim = [Inf -Inf];
ZLim = [Inf -Inf];
hw = create_waitbar('SAR files : reading signal', 'N', NbFic);
for k=1:NbFic
    my_waitbar(k, NbFic, hw);
    
    flag = Sar2ssc(nomFic{k});
    if ~flag
        return
    end
    
    [flag, Data] = SSc_ReadDatagrams(nomFic{k}, 'Ssc_Sar', 'Memmapfile', -1);
    if ~flag
        return
    end
    
    Latitude  = Data.Latitude(:);
    Longitude = Data.Longitude(:);
    [Signal, Unit] = get_Signal(Data, Invite);
    
    switch lower(Invite)
        case {lower('heading'); lower('SensorhHeading')}
            ZLim = [0 360];
        case lower('HeightSamples')
            ZLim(1) = min(ZLim(1), double(min(Signal, [], 'omitnan')));
            ZLim(2) = max(ZLim(2), double(max(Signal, [], 'omitnan')));
        case lower('HeightMeters')
            ZLim(1) = min(ZLim(1), double(min(Signal, [], 'omitnan')));
            ZLim(2) = max(ZLim(2), double(max(Signal, [], 'omitnan')));
        case lower('Depth')
            ZLim(1) = min(ZLim(1), double(min(Signal, [], 'omitnan')));
            ZLim(2) = max(ZLim(2), double(max(Signal, [], 'omitnan')));
        otherwise
            ZLim(1) = min(ZLim(1), double(min(Signal(:), [], 'omitnan')));
            ZLim(2) = max(ZLim(2), double(max(Signal(:), [], 'omitnan')));
    end
    clear Data
    XLim(1) = min(XLim(1), min(Longitude(:), [], 'omitnan'));
    XLim(2) = max(XLim(2), max(Longitude(:), [], 'omitnan'));
    YLim(1) = min(YLim(1), min(Latitude(:),  [], 'omitnan'));
    YLim(2) = max(YLim(2), max(Latitude(:),  [], 'omitnan'));
end
my_close(hw)

figure(Fig);
% if isempty(Unit)
%     title(Invite)
% else
%     title([Invite ' (' Unit ')']);
% end

%% Saisie des bornes de la table de couleurs

[flag, ZLim] = saisie_CLim(min(ZLim), max(ZLim), Unit, 'SignalName', Invite, 'hFigHisto', 87698);
if ~flag
    return
end

%% Trace de la navigation de tous les fichiers

Fig = SAR_PlotNavigation(nomFic, 'Fig', Fig, 'EtapeUnique', 1);
if isempty(Unit)
    title(Invite);
else
    title([Invite ' (' Unit ')'])
end

%% Trac� de l'�chelle des couleurs du signal

% figure(Fig);
W = axis;
x = linspace(XLim(1), XLim(1)+100*nbColors*eps(XLim(1)), nbColors);
y = linspace(YLim(1), YLim(2), nbColors);
h = imagesc(x, y, linspace(ZLim(1), ZLim(2), nbColors)); colormap(Colors), colorbar %#ok<NASGU>
% delete(h)
hold on;
axis(W)

%% Trac� des signaux

hw = create_waitbar('SAR files : plotting signal', 'N', NbFic);
for k=1:NbFic
    my_waitbar(k, NbFic, hw)
    
	[flag, Data] = SSc_ReadDatagrams(nomFic{k}, 'Ssc_Sar', 'Memmapfile', -1);
    if ~flag
        return
    end
    
    Latitude  = Data.Latitude(:);
    Longitude = Data.Longitude(:);
    
    Signal = get_Signal(Data, Invite);
    
    clear Data
    plot_position_data_unitaire(nomFic{k}, Latitude, Longitude, Signal, ZLim, Fig, nbColors, Colors);
end
my_close(hw, 'MsgEnd')
figure(Fig);


function plot_position_data_unitaire(nomFic, Latitude, Longitude, Signal, ZLim, Fig, nbColors, Colors)

figure(Fig)

%% Trac�

% nbSounders = size(Signal,2);
nbSounders = 1;
for iCote=1:nbSounders
    figure(Fig(iCote))
    ZData = Signal(:,iCote);
    subNonNaN = find(~isnan(ZData));
    if length(subNonNaN) <= 1
        return
    end
    subNaN = find(isnan(ZData));
    ZData(subNaN) = interp1(subNonNaN, ZData(subNonNaN), subNaN, 'linear', 'extrap');
    %     Value = interp1(Time, ZData, Time);
    Value = ZData;
    if ZLim(2) == ZLim(1)
        ZLim(1) = ZLim(1) - 0.5;
        ZLim(2) = ZLim(2) + 0.5;
    end
    coef = (nbColors-1) / (ZLim(2) - ZLim(1));
    Value = ceil((Value - ZLim(1)) * coef);
    
    %% Affichage de la navigation
    
    for k=1:nbColors
        sub = find(Value == (k-1));
        if ~isempty(sub)
            h = plot(Longitude(sub), Latitude(sub), '+');
            set(h, 'Color', Colors(k,:), 'Tag', nomFic)
            set_HitTest_Off(h)
        end
    end
    
    sub = find(Value < 0);
    if ~isempty(sub)
        h = plot(Longitude(sub), Latitude(sub), '+');
        set(h, 'Color', Colors(1,:)*0.8, 'Tag', nomFic)
        set_HitTest_Off(h)
    end
    
    sub = find(Value > (nbColors-1));
    if ~isempty(sub)
        h = plot(Longitude(sub), Latitude(sub), '+');
        set(h, 'Color', Colors(nbColors,:)*0.8, 'Tag', nomFic)
    end
end

axisGeo;
grid on;
% colorbar % Supprim� le 7/11/2006 car ca remet les infos de colorbar de 0
% � 64
drawnow



function [Signal, Unit] = get_Signal(Data, Invite)

Unit = '';
resol = 0.25;
switch lower(Invite)
    case lower('HeightSamples')
        Signal = -abs(Data.Height(:));
    case lower('HeightMeters')
        Signal = -abs(Data.Height(:)) * resol;
    case lower('Depth')
        Signal = -abs(Data.Height(:) * resol) - abs(Data.Immersion(:));
    otherwise
        Signal = Data.(Invite)(:);
end

