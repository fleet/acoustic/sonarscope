function FigOut = SAR_PlotSignals(nomFic, varargin)

[varargin, Fig] = getPropertyValue(varargin, 'Fig', []); %#ok<ASGLU>

if ischar(nomFic)
    nomFic = {nomFic};
end

N = length(nomFic);
ListeFig = NaN(1,N);
for k=1:N
    [flag, FigOut] = SAR_PlotSignals_unitaire(nomFic{k}, Fig);
    if flag
        ListeFig(k) = FigOut;
    end
end

function [flag, Fig] = SAR_PlotSignals_unitaire(nomFic, Fig)

flag = Sar2ssc(nomFic);
if ~flag
    return
end

[flag, Data] = SSc_ReadDatagrams(nomFic, 'Ssc_Sar', 'Memmapfile', -1);
if ~flag
    return
end

Height          = Data.Height(:);
OriginHeight    = Data.OriginHeight(:);
Immersion       = Data.Immersion(:);
OriginImmersion = Data.OriginImmersion(:);
Heading         = Data.Heading(:);
OriginHeading   = Data.OriginHeading(:);
Latitude        = Data.Latitude(:);
OriginLatitude  = Data.OriginLatitude(:);
Longitude       = Data.Longitude(:);
OriginLongitude = Data.OriginLongitude(:);
Speed           = Data.Speed(:);
OriginSpeed     = Data.OriginSpeed(:);
clear Data


if isempty(Fig) || ~ishandle(Fig)
    Fig = figure('Name', ['  -  SAR Signals from ' nomFic]);
else
    Fig = figure(Fig);
    set(Fig, 'Name', ['  -  SAR Signals from ' nomFic]);
end
hAxe = preallocateGraphics(0,0); nA = 7; k = 1;

hAxe(1)     = subplot(nA,2,k);
PlotUtils.createSScPlot(OriginHeight, 'g'); hold on; PlotUtils.createSScPlot(Height); hold off;
grid on; title('SonarHeight (m)');
k = k+2;

hAxe(end+1) = subplot(nA,2,k);
PlotUtils.createSScPlot(OriginImmersion, 'g'); hold on; PlotUtils.createSScPlot(Immersion); hold off;
grid on; title('Immersion (m)'); k = k+2;

hAxe(end+1) = subplot(nA,2,k);
PlotUtils.createSScPlot(OriginHeight+OriginImmersion, 'g'); hold on; PlotUtils.createSScPlot(Height+Immersion); hold off;
grid on; title('SonarHeight+Immersion (m)'); k = k+2;

hAxe(end+1) = subplot(nA,2,k);
PlotUtils.createSScPlot(OriginHeading, 'g'); hold on; PlotUtils.createSScPlot(Heading); hold off;
grid on; title('Heading (deg)'); k = k+2;

hAxe(end+1) = subplot(nA,2,k);
PlotUtils.createSScPlot(OriginSpeed, 'g'); hold on; PlotUtils.createSScPlot(Speed); hold off;
grid on; title('Speed (m/s)'); k = k+2;

hAxe(end+1) = subplot(nA,2,k);
PlotUtils.createSScPlot(OriginLatitude, 'g'); hold on; PlotUtils.createSScPlot(Latitude); hold off;
grid on; title('Latitude'); k = k+2;

hAxe(end+1) = subplot(nA,2,k);
PlotUtils.createSScPlot(OriginLongitude, 'g'); hold on; PlotUtils.createSScPlot(Longitude); hold off;
grid on; title('Longitude'); k = k+2;

linkaxes(hAxe, 'x');

sub = (0:(nA-1))*2;
sub = sub+2 ;
subplot(nA, 2, sub); PlotUtils.createSScPlot(Longitude, Latitude); grid on; title('Navigation');
% k = k+1;
