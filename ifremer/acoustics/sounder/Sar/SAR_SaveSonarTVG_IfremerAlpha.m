function SAR_SaveSonarTVG_IfremerAlpha(nomFic, SonarTVG_IfremerAlpha)

if isempty(SonarTVG_IfremerAlpha) % Ceinture et bretelles
    return
end

if iscell(nomFic)
    nomFic = nomFic{1};
end

nomDir = fileparts(nomFic);
nomFicXml = fullfile(nomDir, 'SonarTVG_IfremerAlpha_Folder');

str1 = sprintf('Voulez-vous sauver les valeurs de "SonarTVG_IfremerAlpha_Folder" dans le r�pertoire "%s" afin qu''elles soient trouv�es automatiquement par la suite ?', nomDir);
str2 = sprintf('Do you want to save the values of "SonarTVG_IfremerAlpha_Folder" in the directory "%s" in order they can be used automatically later ?', nomDir);
[rep, flag] = my_questdlg(Lang(str1,str2));
if ~flag
    return
end

if rep == 1
    
    % if exist(nomFicXml, 'file')
    %     SonarTVG_IfremerAlpha = xml_mat_read(nomFicXml);
    % else
    %     SonarTVG_IfremerAlpha = [];
    % end
    
    xml_write(nomFicXml, SonarTVG_IfremerAlpha);
    flag = exist(nomFicXml, 'file');
    if ~flag
        messageErreurFichier(nomFicXml, 'WriteFailure');
        return
    end
end
