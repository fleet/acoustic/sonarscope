function flag = SAR_SubbottomExport3DViewer(nomFic, nomDirOut, Tag, CLim, NbSamplesMax, TailleMax, CeleriteSediments)
 
if ~iscell(nomFic)
    nomFic = {nomFic};
end

flag = exist(nomDirOut, 'dir');
if ~flag
    str1 = sprintf('Le r�pertoire "%s" n''existe pas.', nomDirOut);
    str2 = sprintf('Directory "%s"does not exist.', nomDirOut);
    my_warndlg(Lang(str1,str2), 1);
    return
end

N = length(nomFic);
str1 = 'Export du sondeur de s�diment des fichiers SAR dans GLOBE';
str2 = 'Exporting SAR subbottom images to GLOBE';
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:length(nomFic)
    my_waitbar(k, N, hw)
    flag = SAR_SubbottomExport3DViewer_unitaire(nomFic{k}, nomDirOut, Tag, CLim, NbSamplesMax, TailleMax, CeleriteSediments);
    if ~flag
        % Message SVP
    end
end
my_close(hw, 'MsgEnd')


function flag = SAR_SubbottomExport3DViewer_unitaire(nomFic, nomDirOut, Tag, CLim, NbSamplesMax, TailleMax, CeleriteSediments)

I0 = cl_image_I0;
[flag, a] = import_SAR(I0, nomFic, 'subImage', 3);
if ~flag
    return
end

%% Cr�ation du r�pertoire

[~, nomDirBin] = fileparts(nomFic);
if isempty(Tag)
    nomDirXml = fullfile(nomDirOut, [nomDirBin '_SubBottom']);
else
    nomDirXml = fullfile(nomDirOut, [nomDirBin '_SubBottom_' Tag]);
end


% if ~exist(nomDirXml, 'dir')
%     flag = mkdir(nomDirXml);
%     if ~flag
%         messageErreurFichier(nomDirXml, 'WriteFailure');
%         return
%     end
% end
NomFicXML = [nomDirXml '.xml'];

%% Lecture de l''image

[flag, b] = subbottom_suppressWC(a);
if ~flag
    return
end
nbCol = b.nbColumns;
subx = 1:min(NbSamplesMax, nbCol);

b.CLim = CLim;
% b .ColormapIndex = ColormapIndex;
b.Video = 2;
typeOutput = 1; % XML-Bin
flag = sonar_Export3DV_ImagesAlongNavigation_ByBlocks(b, NomFicXML, TailleMax, typeOutput, ...
    'subx', subx, 'Mute', 'CeleriteSediments', CeleriteSediments);

