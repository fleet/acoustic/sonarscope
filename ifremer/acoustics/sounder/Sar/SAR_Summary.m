function flag = SAR_Summary(nomFic, nomFicSummary)
 
if ~iscell(nomFic)
    nomFic = {nomFic};
end

fid = fopen(nomFicSummary, 'w+t');
if fid == -1
    messageErreurFichier(nomFicSummary, 'WriteFailure');
    flag = 0;
    return
end

for k=1:length(nomFic)
    flag = SAR_Summary_unitaire(nomFic{k}, fid);
    if ~flag
        % Message SVP
    end
end
fclose(fid);

try
    winopen(nomFicSummary)
catch %#ok<CTCH>
end


function flag = SAR_Summary_unitaire(nomFic, fid)

flag = Sar2ssc(nomFic);
if ~flag
    return
end

[flag, Data] = SSc_ReadDatagrams(nomFic, 'Ssc_Sar', 'Memmapfile', -1);
if ~flag
    return
end

fprintf(fid, '%s  :  %s\n', nomFic, summary(Data.Time));
clear Data
