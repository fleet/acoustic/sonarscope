function flag = SAR_WCExport3DViewer(nomFic, nomDirOut, Tag, CLim, TailleMax, Side, ColormapIndex, Video)
 
SonarTVG_IfremerAlpha = [];

if ~iscell(nomFic)
    nomFic = {nomFic};
end

flag = exist(nomDirOut, 'dir');
if ~flag
    str1 = sprintf('Le r�pertoire "%s" n''existe pas.', nomDirOut);
    str2 = sprintf('Directory "%s"does not exist.', nomDirOut);
    my_warndlg(Lang(str1,str2), 1);
    return
end

N = length(nomFic);
str1 = 'Export de la colonne d''eau des fichiers SAR dans GLOBE';
str2 = 'Exporting SAR Water Column images to GLOBE';
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:length(nomFic)
    my_waitbar(k, N, hw)
    [flag, SonarTVG_IfremerAlpha] = SAR_WCExport3DViewer_unitaire(nomFic{k}, nomDirOut, Tag, CLim, TailleMax, Side, ColormapIndex, Video, SonarTVG_IfremerAlpha);
    if ~flag
        % Message SVP
    end
end
my_close(hw, 'MsgEnd')


function [flag, SonarTVG_IfremerAlpha] = SAR_WCExport3DViewer_unitaire(nomFic, nomDirOut, Tag, CLim, TailleMax, Side, ColormapIndex, Video, ...
    SonarTVG_IfremerAlpha)

I0 = cl_image_I0;

[flag, a, Carto, SonarTVG_IfremerAlpha] = import_SAR(I0, nomFic, 'subImage', 2, 'SonarTVG_IfremerAlpha', SonarTVG_IfremerAlpha);
if ~flag
    return
end

%% Extraction de la colonne d''eau

[flag, b] = Sidescan_ExtractWaterColumn(a, Side);
if ~flag
    return
end

%% Cr�ation du r�pertoire

[~, nomDirBin] = fileparts(nomFic);
if isempty(Tag)
    nomDirXml = fullfile(nomDirOut, [nomDirBin '_WaterColumn']);
else
    nomDirXml = fullfile(nomDirOut, [nomDirBin '_WaterColumn_' Tag]);
end

NomFicXML = [nomDirXml '.xml'];

%% Export vers 3DV

b.CLim          = CLim;
b.ColormapIndex = ColormapIndex;
b.Video         = Video;
typeOutput = 1; % XML-Bin
flag = sonar_Export3DV_ImagesAlongNavigation_ByBlocks(b, NomFicXML, TailleMax, typeOutput, 'Mute');
if ~flag
    return
end

