% Exemples de travaux menes sur le Sar. A n'utiliser qu'n edition et copy/paste
%
% See also LecFicSar Authors
% Authors : JMA
% VERSION  : $Id: SarChantier.m,v 1.4 2002/06/20 10:23:01 augustin Exp $
%------------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HISTORIQUE DEVELOPPEMENT
%   02/11/00 - JMA - Creation
%   02/04/01 - JMA - Mise en ref.
% ----------------------------------------------------------------------------

function SarChantier

help SarChantier
return

% close(1000:1001)
% iModeRecSpacing = 1;
% angles = CalculFaisceauxEm300( 1, 70, iModeRecSpacing, 135);
%
% figure(1000);
% plot(angles, 'r+'); grid on; hold on;
% title('Repartition des faisceaux')
% figure(1001)
% plot(gradient(angles), 'r+'); grid on; hold on;
% title('Variation de la repartition des faisceaux')
%
% iModeRecSpacing = 2;
% angles = CalculFaisceauxEm300( 1, 70, iModeRecSpacing, 135);
%
% figure(1000);
% plot(angles, 'k+'); grid on;
% figure(1001)
% plot(gradient(angles), 'k+'); grid on;
%
% iModeRecSpacing = 3;
% angles = CalculFaisceauxEm300( 1, 70, iModeRecSpacing, 135);
%
% figure(1000);
% plot(angles, 'b+'); grid on;
% figure(1001)
% plot(gradient(angles), 'b+'); grid on;
%
% figure(1000);
% legend({'Equi Angle';'Equi Distant';'Optimized for Imagery'});
% figure(1001);
% legend({'Equi Angle';'Equi Distant';'Optimized for Imagery'});

%----------------------------------------------------------------------

nomFic = '/home1/doppler/augustin/Campagnes/ZAISAR/IM_brut/SAR_0035.01.im';
N = getNbEnrFicSar(nomFic)
pas = floor(N/1000)
BT = LecFicSar( nomFic, 'BT', 1:pas:N );
imagesc(BT, [0 50]); colormap(jet(256)); title('Image Brute')

m = mean(BT);
figure;
plot(m); grid on; title('Image Brute')


nomFic = '/home1/doppler/augustin/Campagnes/ZAISAR/IM_brut/SAR_0035.02.im';
N = getNbEnrFicSar(nomFic)
pas = floor(N/1000)
BT = LecFicSar( nomFic, 'BT', 1:pas:N );
imagesc(BT); colorbar; colormap(jet(256)); S; title('Image Brute')

m = mean(BT);
figure;
plot(m); grid on; title('Image Brute')


Z = -5000
T = 2
S = 35

alphaBabMaison = AttenuationGarrison( 170, Z, T, S )
alphaTriMaison = AttenuationGarrison( 190, Z, T, S )

nomFic = '/home1/doppler/augustin/Campagnes/ZAISAR/IM_brut/SAR_0035.01.im';
nomFicCor = '/home1/doppler/augustin/Campagnes/ZAISAR/IM_cor/SAR_0035.01.im';
CorFicSar( nomFic, Z, T, S, nomFicCor );

nomFic = '/home1/doppler/augustin/Campagnes/ZAISAR/IM_brut/SAR_0035.02.im';
nomFicCor = '/home1/doppler/augustin/Campagnes/ZAISAR/IM_cor/SAR_0035.02.im';
CorFicSar( nomFic, Z, T, S, nomFicCor );


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
nomFicCor = '/home1/doppler/augustin/Campagnes/ZAISAR/0044.02.im';
N = getNbEnrFicSar(nomFicCor)
pas = floor(N/1000)
BT2 = LecFicSar( nomFicCor, 'BT', 1:pas:N );
imagesc(BT2); colorbar; colormap(jet(256)); S; title('Image Brute')

mcor=mean(BT2)
figure;
plot(mcor); grid on; title('Image Brute')




nomFic = '/home1/doppler/augustin/Campagnes/ZAISAR/sarbrut014401.imo';
PrintHeaderNetCDF( nomFic, 'REFLEC');
[image, resolution] = LecFicLayerCaraibes( nomFic, 'REFLECTIVITY', 1:10:8266);
imagesc(image); colorbar; S; title('Image originale traitee par Caraibes')

m = my_nanmean(image);
figure;
plot(m); grid on; title('Image originale traitee par Caraibes')





nomFicCor = '/home1/doppler/augustin/Campagnes/ZAISAR/IM_cor/0044.02.im';
N = getNbEnrFicSar(nomFicCor)
pas = floor(N/1000)
BT2 = LecFicSar( nomFicCor, 'BT', 1:pas:N );
imagesc(BT2); colorbar; colormap(jet(256)); S; title('Image corrigee')

mcor = mean(BT2)
figure;
plot(mcor); grid on; title('Image corrigee')






nomFic = '/home1/doppler/augustin/Campagnes/ZAISAR/IM_cor/sarcor014401.imo';
PrintHeaderNetCDF( nomFic, 'REFLEC');
[image, resolution] = LecFicLayerCaraibes( nomFic, 'REFLECTIVITY', 1:10:8266);
imagesc(image); colorbar; S; title('Image corrigee traitee par Caraibes')

m = my_nanmean(image);
figure;
plot(m); grid on; title('Image corrigee traitee par Caraibes')




mcor=mean(BT2)
figure;
plot(mcor); grid on;

% --------------------------------------------------------------
% Image 0001.01

nomFicCor = '/home1/doppler/augustin/Campagnes/ZAISAR/0001.01.im';
N = getNbEnrFicSar(nomFicCor)
pas = floor(N/1000)
BT2 = LecFicSar( nomFicCor, 'BT', 1:pas:N );
imagesc(BT2); colorbar; colormap(jet(256)); S; title('Image Brute')

mbrute=mean(BT2(13:17,:));
figure;
plot(mbrute); grid on; title('Image Brute')


[nuprom, nuLoi, GFb, GFt, SHb, SHt] = getNuLoi(dayStr2Ifr('22/02/2000'), hourStr2Ifr('19:22:48'))
[D, a0, DeltaG, LimiteGMax] = getParamEprom(nuprom, nuLoi);
TVG_SAR = TvgSar(D, a0, DeltaG, LimiteGMax);		% Loi theorique
figure;
plot(TVG_SAR); grid on; title('TVG_SAR');

mbrute_dB = 2 * reflec_Enr2dB( mbrute);
figure;
plot(mbrute_dB); grid on; title('mbrute_dB');

mbrute_dB_sansTVG = mbrute_dB - [fliplr(TVG_SAR) TVG_SAR];
figure;
plot(mbrute_dB_sansTVG); grid on; title('mbrute_dB_sansTVG');

nomFicCor = '/home1/doppler/augustin/Campagnes/ZAISAR/0001.01.imc';
N = getNbEnrFicSar(nomFicCor)
pas = floor(N/1000)
BT2 = LecFicSar( nomFicCor, 'BT', 1:pas:N );
imagesc(BT2); colorbar; colormap(jet(256)); S; title('Image Corrigee')

mcor=mean(BT2(150:end,:));
figure;
plot(mcor); grid on; title('Image Corrigee')


% --------------------------------------------------------------
%




Z = -1400
T = 4
S = 35

Z = -5000
T = 2
S = 35

alphaBabMaison = AttenuationGarrison( 170, Z, T, S )
alphaTriMaison = AttenuationGarrison( 190, Z, T, S )


nomFic = '/home1/doppler/augustin/Campagnes/ZAISAR/0044.02.im';
nomFicCor = '/home1/doppler/augustin/Campagnes/ZAISAR/IM_cor/0044.02.im';
CorFicSar( nomFic, Z, T, S, nomFicCor );

nomFic = '/home1/doppler/augustin/Campagnes/ZAISAR/0056.02.im';
nomFicCor = '/home1/doppler/augustin/Campagnes/ZAISAR/0056.02.imc';
CorFicSar( nomFic, Z, T, S, nomFicCor );

nomFic = '/home1/doppler/augustin/Campagnes/ZAISAR/0001.01.im';
nomFicCor = '/home1/doppler/augustin/Campagnes/ZAISAR/0001.01.imc';
CorFicSar( nomFic, Z, T, S, nomFicCor );

nomFic = '/home1/doppler/augustin/Campagnes/ZAISAR/0001.03.im';
nomFicCor = '/home1/doppler/augustin/Campagnes/ZAISAR/0001.03.imc';
CorFicSar( nomFic, Z, T, S, nomFicCor );

nomFic = '/home1/doppler/augustin/Campagnes/ZAISAR/0001.04.im';
nomFicCor = '/home1/doppler/augustin/Campagnes/ZAISAR/0001.04.imc';
CorFicSar( nomFic, Z, T, S, nomFicCor );



BT = LecFicSar( nomFic, 'BT', 1:700 );
imagesc(BT); colorbar; colormap(jet(256)); S;



N = getNbEnrFicSar(nomFicCor)
pas = floor(N/1000)
BT2 = LecFicSar( nomFicCor, 'BT', 1:pas:N );
imagesc(BT2); colorbar; colormap(jet(256)); S;

N = getNbEnrFicSar(nomFicCor)
pas = floor(N/1000)
BT2 = LecFicSar( nomFicCor, 'BT', 1:pas:N );
imagesc(BT2); colorbar; colormap(jet(256)); S;

mcor=mean(BT2)
figure;
plot(mcor); grid on;

nomFic='/home1/doppler/augustin/Campagnes/ZAISAR/0001.01.im'
N = getNbEnrFicSar(nomFic)
pas = floor(N/1000)
BT = LecFicSar( nomFic, 'BT', 1:pas:N );
imagesc(BT); colorbar; colormap(jet(256)); S;

m=mean(BT)
figure;
plot(m); grid on;
