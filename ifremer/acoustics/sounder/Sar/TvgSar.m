% Calcul d'une loi de TVG
%
% Syntax
%   TVG = TvgSar(D, a0, DeltaG)
%
% Input Arguments 
%   D     : Coefficient de la divergence geometrique
%   a0    : Coefficient d'attenuation en db/km
%   Delta : Variation de gain
%
% Output Arguments
%   TVG   : Loi de TVG
%
% Examples
%   [D, a0, DeltaG, LimiteGMax] = getParamEprom(2, 8)
%   TVG = TvgSar(D, a0, DeltaG, LimiteGMax);
%   plot(TVG); grid on;
%
%   [D, a0, DeltaG, LimiteGMax] = getParamEprom(2, 1:8)
%   TVG = TvgSar(D, a0, DeltaG, LimiteGMax);
%   plot(TVG'); grid on;
%
% See also getNuLoi lecEprom TvgSar Authors
% Authors : JMA
% VERSION  : $Id: TvgSar.m,v 1.2 2002/06/20 10:23:01 augustin Exp $
%------------------------------------------------------------------------------

function TVG = TvgSar(D, a0, DeltaG, LimiteGMax)

C = 1500;
RKM = 1e-3 * C * ((1:3000)/ 3000) / 2;
R = RKM * 1000;

for i=1:length(D)
    G0 =  D(i) * log10(R) + 2. * a0(i) * RKM;
    B  = 80 -  D(i) * log10(LimiteGMax(i)) - 2. * a0(i) * (LimiteGMax(i)/1000);
    G2 = G0 + B + DeltaG(i);
    TVG(i,:) = max(10, min(G2, 80));
end
