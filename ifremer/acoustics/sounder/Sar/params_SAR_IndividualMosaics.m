function [flag, ListeFic, gridSize, CorFile, selectionData, repImport, repExport] = params_SAR_IndividualMosaics(repImport, repExport)

gridSize      = [];
CorFile       = [];
selectionData = [];

%% Liste des fichiers

[flag, ListeFic, lastDir] = uiSelectFiles('ExtensionFiles', '.im', 'AllFilters', 1, ...
    'RepDefaut', repImport);
if ~flag
    return
end
repImport = lastDir;

%% Pas de la grille

[flag, gridSize] = get_gridSize('Value', 1.5);
if ~flag
    return
end

%% Nom du répertoire des mosaiques individuelles

[flag, nomDir] = question_NomDirIndividualMosaics(repExport);
if ~flag
    return
end
repExport = nomDir;

%% Choix du layer

listeData{1} = 'Reflectivity';
listeData{2} = 'Pseudo Bathymetry';
str1 = 'Choix du layer :';
str2 = 'Select the layer :';
[selectionData, flag] = my_listdlg(Lang(str1,str2), listeData, 'SelectionMode', 'Single');
if ~flag
    return
end


%% Compensation statistique

% TODO : remplacer ce qui suit par [flag, CorFile, ModeDependant, UseModel, PreserveMeanValueIfModel] = QuestionCompensationCurve(nomDirImport, varargin)
[rep, flag] = my_questdlg(Lang('Voulez-vous faire une compensation ?', 'Do you want to do a compensation ?'));
if ~flag
    return
end
if rep == 1
    [flag, CorFile] = uiSelectFiles('ExtensionFiles', '.mat', 'RepDefaut', repImport);
    if ~flag
        return
    end
else
    CorFile = {};
end

persistentGridSize = gridSize;
