function [flag, listeFic, MaxRange, repImport] = params_SAR_PlotNavigationCoverage(repImport)

MaxRange = [];

%% Liste des fichiers

[flag, listeFic, repImport] = uiSelectFiles('ExtensionFiles', '.im', 'RepDefaut', repImport);
if ~flag
    return
end

%% MaxRange

str1 = 'Trac� de la navigation avec couverture';
str2 = 'Coverage parameter';
p = ClParametre('Name', Lang('Port�e Max', 'Max range'), ...
    'Unit', 'm',  'MinValue', 0, 'MaxValue', 750, 'Value', 550);
a = StyledParametreDialog('params', p, 'Title', Lang(str1,str2));
% a.Help = 'http://flotte.ifremer.fr/fleet/Presentation-of-the-fleet/On-board-software/SonarScope';
a.sizes = [0 -2 -1 -2 -1 -1 -1 0];
a.openDialog;
flag = a.okPressedOut;
if ~flag
    return
end
MaxRange = a.getParamsValue;
