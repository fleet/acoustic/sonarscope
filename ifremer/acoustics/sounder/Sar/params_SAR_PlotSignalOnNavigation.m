function [flag, listeFic, nomSignal, repImport] = params_SAR_PlotSignalOnNavigation(repImport)

nomSignal = [];

%% Liste des fichiers

[flag, listeFic, repImport] = uiSelectFiles('ExtensionFiles', '.im', 'RepDefaut', repImport);
if ~flag
    return
end

%% Select a signal

listeSignaux    = {'Heading'; 'HeightMeters'; 'HeightSamples';    'Time'; 'Date'; 'Hour'; 'PingNumber'; 'Immersion';     'Depth'};
listeSignauxIHM = {'Heading'; 'Height (m)';   'Height (Samples)'; 'Time'; 'Date'; 'Hour'; 'PingNumber'; 'Immersion (m)'; 'Depth (m)'};
[flag, sub] = question_SelectASignal(listeSignauxIHM);
if ~flag
    return
end
nomSignal = listeSignaux{sub};
