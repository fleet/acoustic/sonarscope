function [flag, listeFic, nomSignal, repImport] = params_SAR_RestoreSignals(repImport)

nomSignal = [];

%% Liste des fichiers

[flag, listeFic, repImport] = uiSelectFiles('ExtensionFiles', '.im', 'RepDefaut', repImport);
if ~flag
    return
end

%% Nom du signal

listeSignaux = {'Latitude'; 'Longitude'; 'Height'; 'Heading'; 'Time'; 'Immersion'};
str1 = 'Sélectionnez les signaux';
str2 = 'Select the signals';
[rep, flag] = my_listdlg(Lang(str1,str2), listeSignaux, 'InitialValue', []);
if ~flag
    return
end
nomSignal = listeSignaux(rep);
