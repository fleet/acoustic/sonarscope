function [flag, listeFic, nomDirOut, Tag, CLim, NbSamplesMax, TailleMax, CeleriteSediments, repImport, repExport] ...
    = params_SAR_SubbottomExport3DViewer(repImport, repExport)

persistent persistent_CLim persistent_NbSamples persistent_CeleriteSediments persistent_TailleMax

nomDirOut    = [];
Tag          = [];
CLim         = [0 120];
NbSamplesMax = 760;
TailleMax    = [];
CeleriteSediments = [];

%% Liste des fichiers

[flag, listeFic, repImport] = uiSelectFiles('ExtensionFiles', '.im', 'RepDefaut', repImport);
if ~flag
    return
end

%% Saisie du nom de r�pertoire o� seront stock�es les mosaiques individuelles

str1 = 'Nom du r�pertoire d''accueil des fichiers d''export pour le GLOBE.';
str2 = 'Name of the directory to save this individual files. Use the "Create a new directory" button if necessary.';
[flag, nomDirOut] = my_uigetdir(repExport, Lang(str1,str2));
if ~flag
    return
end
repExport = nomDirOut;

%% Bornes de rehaussement de contraste

if isempty(persistent_CLim)
    minVal   = 0;
    maxVal   = 120;
else
    minVal   = persistent_CLim(1);
    maxVal   = persistent_CLim(2);
end

[flag, CLim] = saisie_CLim(minVal, maxVal, '', 'SignalName', 'SAR WC data');
if ~flag
    return
end
persistent_CLim = CLim;

%% Taille max des images

if isempty(persistent_TailleMax)
    TailleMax = 4;
else
    TailleMax = persistent_TailleMax / 1e6;
end

[flag, TailleMax] = inputOneParametre('Maximum size (M pixels)', 'Value', ...
    'Value', TailleMax, 'Unit', 'mega pixels', 'MinValue', 0.5);
if ~flag
    return
end
TailleMax = TailleMax * 1e6;
persistent_TailleMax = TailleMax;

%% P�n�tration max

if isempty(persistent_NbSamples)
    NbSamplesMax = Inf;
else
    NbSamplesMax = persistent_NbSamples;
end
str1 = 'Limitation verticale';
str2 = 'Vertical limitation';
str9  = 'P�n�tration max';
str10 = 'Max length of penetration';
[flag, NbSamplesMax] = inputOneParametre(Lang(str1,str2), Lang(str9,str10), ...
    'Value', NbSamplesMax, 'Unit', 'samples', 'MinValue', 0);
if ~flag
    return
end
persistent_NbSamples = NbSamplesMax;


%% Taille max des images

if isempty(persistent_CeleriteSediments)
    CeleriteSediments = 1600;
else
    CeleriteSediments = persistent_CeleriteSediments;
end

str1 = 'C�l�rit� dans les s�diments (m/s)';
str2 = 'Sound speed in sediments';
[flag, CeleriteSediments] = inputOneParametre(Lang(str1,str2), 'Value', ...
    'Value', CeleriteSediments, 'Unit', 'm/s', 'MinValue', 1500, 'MaxValue', 2500);
if ~flag
    return
end

persistent_CeleriteSediments = CeleriteSediments;

%% Tag suppl�mentaire dans le mom des images

[flag, Tag] = question_AddTagInOutputFile;
if ~flag
    return
end
