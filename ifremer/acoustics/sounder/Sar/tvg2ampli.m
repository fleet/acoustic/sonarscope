% Calcul de la tension de commande du preampli en fonction d'un gain desire en sortie
%
% Syntax
%   TensionCommande = tvg2ampli(GainE)
%
% Input Arguments 
%   GainE  : Gain desire a la sortie du preampli
%
% Output Arguments
%   TensionCommande : Tension de commande du preampli
%
% Examples
%   nomFic = getNomFicDatabase('tvg2.bin')
%   Eprom = lecEprom(nomFic);
%   figure; plot(Eprom); grid on;
%   title(['Lois TVG lues dans l''eprom nu. 2 : fichier ' nomFic]); legend('1','2','3','4','5','6','7','8');
%
%   [D, a0, DeltaG, LimiteGMax] = getParamEprom(2, 1:8);
%   GainE = TvgSar(D, a0, DeltaG, LimiteGMax);
%   figure; plot(GainE'); grid on; title('Lois TVG de l''eprom nu. 2');
%
%   TensionCommande = tvg2ampli(GainE);
%   figure; plot(TensionCommande'); grid on;
%   title('Tensions de commande correspondant a l''eprom nu. 2'); legend('1','2','3','4','5','6','7','8');
%
% See also lecEprom TvgSar Authors
% Authors : JMA
% VERSION  : $Id: tvg2ampli.m,v 1.3 2002/06/20 10:23:01 augustin Exp $
%------------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HISTORIQUE DEVELOPPEMENT
%   02/11/00 - JMA - Creation
%   02/04/01 - JMA - Mise en ref.
% ----------------------------------------------------------------------------

function TensionCommande = tvg2ampli(GainE)

if nargin == 0
        help tvg2ampli
        return
end

P = [6.221891736e-14 -2.478532058e-11 3.615041034e-9 -2.619658594e-7 1.009347513e-5 -2.081038376e-4 1.923232663e-3 -1.224469998e-2 11.48312996];

TensionCommande = polyval(P, GainE);
