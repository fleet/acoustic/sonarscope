function SEGY_CleanSignals(this, listFileNames, HeightDetection) %#ok<INUSL>

if ~iscell(listFileNames)
    listFileNames = {listFileNames};
end

%% Traitement des fichiers

I0 = cl_image_I0;
Carto = [];
Video = 1;

str1 = 'D�tection de la hauteur des fichiers SEGY';
str2 = 'Height detection of SEGY files';
N = length(listFileNames);
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
    my_waitbar(k, N, hw);
    
    str1 = sprintf('Traitement du fichier %d/%d : "%s" en cours.', k, N, listFileNames{k});
    str2 = sprintf('Processing file %d/%d : "%s".', k, N, listFileNames{k});
    fprintf(1,'%s\n', Lang(str1,str2));
    
    %% V�rification de l'existence du fichier Status
    
    [nomDir, nomFicSeul] = fileparts(listFileNames{k});
    nomFicXmlStatus = fullfile(nomDir, [nomFicSeul '_Status.xml']);
    if exist(nomFicXmlStatus, 'file')
        str1 = sprintf('Il semble que le fichier "%s" ait d�j� �t� trait�. Que voulez-vous faire ?', nomFicSeul);
        str2 = sprintf('It seems file "%s" has been already processed. What do you want to do ?', nomFicSeul);
        str3 = 'Passer au fichier suivant';
        str4 = 'Skip it';
        str5 = 'Le retraiter';
        str6 = 'Reprocess it';
        [rep, flag] = my_questdlg(Lang(str1,str2), Lang(str3,str4), Lang(str5,str6));
        if ~flag
            my_close(hw)
            return
        end
        if rep == 1
            continue
        end
        Info = xml_read(nomFicXmlStatus);
        CLim  = Info.Clim_Apres;
        Video = Info.Video;
    else
        CLim = [];
    end
    
    %% Lecture du fichier
    
    seg = cl_segy('nomFic', listFileNames{k});
    
    %% Pr�-d�tection de la hauteur
    
    if k == 1
        [flag, indLayer] = selectionLayers(seg); % Modifi� par JMA le 29/04/2015 � Norderey
        if ~flag
            my_close(hw)
            return
        end
    end
    
    [flag, b, Carto] = import_SEGY(I0, listFileNames{k}, 'Carto', Carto, 'listLayers', indLayer, 'RawData', 0); % On lit le dernier layer
    if ~flag
        continue
    end
    
    if ~isempty(CLim) && all(~isnan(CLim))
        b.CLim = CLim;
    end
    if ~isempty(Video)
        b.Video = Video;
    end
    
    Height = get(b, 'SonarHeight');
    if isempty(Height) || (all(Height == 0))
%         Immersion = get(b, 'Immersion');
%         Resol = get(b, 'SonarRawDataResol');
%         [flag, c] = subbottom_ImmersionConstante(b);
%         if flag
%             [~, d] = subbottom_compute_height(c, 'TypeSignal', HeightDetection.TypeSignal, 'CleaningHeight', 0, 'ComputingUnit', HeightDetection.TypeUnit);
%             Height = get(d, 'Height');
%             b = set(b, 'SonarHeight', (Height - Immersion) / Resol);
%         else
            [~, b] = subbottom_compute_height(b, 'TypeSignal', HeightDetection.TypeSignal, 'CleaningHeight', 0, 'ComputingUnit', HeightDetection.TypeUnit);
%         end
    end
    
    %% Affichage de l'image dans SonarScope pour v�rifier la hauteur
    
    if k == 1
        messageCheckHeight();
    end
    
    Immersion = get(b, 'Immersion');
    Resol = get(b, 'SonarRawDataResol');
    [flag, c] = subbottom_ImmersionConstante(b);
    if flag
        b = c;
    end
    
    Clim_Avant = b.CLim;
    b = SonarScope(b, 'visuProfilY', 1, 'profilY.Type', 'Height', 'visuProfilX', 1, 'ButtonSave', 1);
    Clim_Apres = b(end).CLim;
    
    %% V�rification si le contraste a bien �t� r�gl�
    
    if isequal(Clim_Avant, Clim_Apres)
        str1 = 'Il semblerait que vous ayez oubli� de r�gler le contraste, voulez-vous sonarscopier l''image de nouveau ?';
        str2 = 'It seems you have forgotten to adjust the contrast, do you want to edit the image again ?';
        [rep, flag] = my_questdlg(Lang(str1,str2));
        if ~flag
            my_close(hw)
            return
        end
        if rep == 1
            b = SonarScope(b(end), 'visuProfilY', 1, 'profilY.Type', 'Height', 'visuProfilX', 1, 'ButtonSave', 1);
            Clim_Apres = b.CLim;
            if isequal(Clim_Avant, Clim_Apres)
                str1 = 'Y''a de la fatigue dans l''air on dirait, tant pis pour le contraste.';
                str2 = 'It seems you are tired, let us drop the contrast for the moment.';
                my_warndlg(Lang(str1,str2), 0, 'Tag', 'ControleSignal', 'TimeDelay', 60);
            end
        end
    end
    Video = b(end).Video;
    
    %% Sauvegarde de la hauteur
    
    Height_m = get(b(end), 'Height');
    Height_sample = (Height_m - Immersion) / Resol;
    flag = save_signal(seg, 'Height', Height_sample(:,1), 'nomDirBin', 'Ssc_Height');
    if ~flag
        my_close(hw)
        return
    end
    
    %% Sauvegarde de CLim
    
    CLim = b(end).CLim;
    flag = save_ProcessingParameters(seg, 'Ssc_ProcessingParameters.xml', 'CLim', CLim);
    if ~flag
        my_close(hw)
        return
    end
    
    %% Cr�ation du fichier de status du fichier
    
    Info.Clim_Apres = Clim_Apres;
    Info.Video      = Video;
    Info.Height     = 1;
    xml_write(nomFicXmlStatus, Info);
    flag = exist(nomFicXmlStatus, 'file');
    if ~flag
        messageErreur(nomFicXmlStatus)
    end
    
    %% On demande si on veut traiter les autres fichiers
    
    if k ~= N
        flag = question_ContinueNextFile();
        if ~flag
            break
        end
    end
end
my_close(hw, 'MsgEnd')
