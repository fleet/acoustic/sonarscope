function flag = SEGY_EditNav(~, listFileNames, varargin)

if ~iscell(listFileNames)
    listFileNames = {listFileNames};
end

N = length(listFileNames);
if N == 0
    flag = 0;
    return
end

nav = ClNavigation.empty();
navFilename = {};

Carto = [];
str1 = 'Lecture de la navigation SEGY';
str2 = 'Reading the SEGY navigation';
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
    my_waitbar(k, N, hw);
    
    % Lecture des datagrams de navigation
    
    seg = cl_segy('nomFic', listFileNames{k});
    if isempty(seg)
        continue
    end
    
    [flag, DataTrace] = read_TraceHeaders_bin(seg, 'Memmapfile', 0);
    if ~flag
        continue
    end
    
    [flag, DataPosition, Carto] = read_position(seg, 'Carto', Carto, varargin{:});
    if ~flag
        continue
    end
    
    sz = numel(DataPosition.Latitude);
    % Interpolation si le fichier de Nav ne fait pas la m�me taille que le nb de Traces du profil sismique.
    if sz ~= DataTrace.Dimensions.NbTraceHeaders
        Longitude = interp1(1:sz, DataPosition.Longitude, linspace(1, sz, DataTrace.Dimensions.NbTraceHeaders));
        Latitude  = interp1(1:sz, DataPosition.Latitude,  linspace(1, sz, DataTrace.Dimensions.NbTraceHeaders));
        Longitude = Longitude';
        Latitude  = Latitude';
        TimeNav = datetime(DataTrace.YearDataRecorded(:), 1, 1, ...
            DataTrace.HourOfDay(:), DataTrace.MinuteOfHour(:), DataTrace.SecondOfMinute(:)) ...
            + hours((DataTrace.DayOfYear(:) - 1) * 24);
    else
        Latitude  = DataPosition.Latitude;
        Longitude = DataPosition.Longitude;
        
        % TODO : supprimer les doublons (ou les r�interpoler ?)
        
        sub = (diff(DataPosition.Time) == 0);
        Latitude(sub)  = [];
        Longitude(sub) = [];
        TimeNav = DataPosition.Time;
        TimeNav(sub) = [];
    end
    if isempty(Latitude)
        continue
    end
    
    % On n'affiche que la 1�re ligne longitudinale de la matrice de donn�es.
    
    Longitude = Longitude(:,1);
    Latitude  = Latitude(:,1);
    SonarTime = TimeNav;
    Heading   = [];
    
    if all(Heading == 0)
        Heading = calCapFromLatLon(Latitude, Longitude);
    end
    
    timeSample = XSample('name', 'time', 'data', SonarTime);
    
    latSample = YSample('data', Latitude, 'marker', '.');
    latSignal = ClSignal('ySample', latSample, 'xSample', timeSample);
    
    lonSample = YSample('data', Longitude, 'marker', '.');
    lonSignal = ClSignal('ySample', lonSample, 'xSample', timeSample);
    
    headingVesselSample = YSample('name', 'Heading', 'unit', 'deg', 'data', Heading, 'marker', '.');
    headingVesselSignal = ClSignal('ySample', headingVesselSample, 'xSample', timeSample);
        
%     immersionSample = YSample('name', 'Immersion', 'unit', 'm', 'data', zeros(size(Heading), 'single'), 'marker', '.');
%     immersionSignal = ClSignal('ySample', immersionSample,  'xSample', timeSample);

    [~, name] = fileparts(listFileNames{k});
    nav(end+1) = ClNavigation(latSignal, lonSignal, ...
        'headingVesselSignal', headingVesselSignal, ...
        'name', name); %#ok<AGROW>
%         'immersionSignal',     immersionSignal, ...
    navFilename{end+1} = listFileNames{k}; %#ok<AGROW>
end
my_close(hw, 'MsgEnd');

%% Create the GUI

a = NavigationDialog(nav, 'Title', 'Navigation', 'CleanInterpolation', true);
% a.editProperties();
pause(0.5)
a.openDialog();
if ~a.okPressedOut
    return
end

%% Save the results

firstTime = 1;
N = length(nav);
str1 = 'Ecriture de la navigation SEGY';
str2 = 'Saving the SEGY navigation';
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
    my_waitbar(k, N, hw);
    
    seg = cl_segy('nomFic', navFilename{k});

    [~, DataTrace] = read_TraceHeaders_bin(seg, 'Memmapfile', 0);
    [~, DataPosition, Carto] = read_position(seg, 'Carto', Carto, varargin{:});
    
    Latitude  = DataPosition.Latitude;
    Longitude = DataPosition.Longitude;
%     Heading   = DataPosition.Heading;
%     if all(Heading == 0)
%         Heading = calCapFromLatLon(Latitude, Longitude);
%     end
%     Immersion = DataPosition.Immersion;
%     T = datetime(DataPosition.Time.timeMat, 'ConvertFrom', 'datenum');
    
    LatitudeNew  = nav(k).getLatitudeSignalSample().data;
    LongitudeNew = nav(k).getLongitudeSignalSample().data;
%     ImmersionNew = nav(k).getImmersionSignalSample().data;
%     HeadingNew   = nav(k).getHeadingVesselSignalSample().data;
%     TNew         = nav(k).getTimeSignalSample().data;
    
    flagModified = 0;
    if ~isequal(LatitudeNew(:), Latitude(:))
        DataPosition.Latitude = LatitudeNew;
        flagModified = 1;
    end
    if ~isequal(LongitudeNew(:), Longitude(:))
        DataPosition.Longitude = LongitudeNew;
        flagModified = 1;
    end
%     if ~isequal(ImmersionNew, Immersion)
%         DataPosition.Immersion = ImmersionNew;
%         flagModified = 1;
%     end
%     if ~isequal(HeadingNew, Heading)
%         DataPosition.Heading = HeadingNew;
%         flagModified = 1;
%     end
    %         if ~isequal(TNew, T)
    %             DataPosition.xxx = TNew;
    %         end
    
    %% Sauvegarde des signaux dans le r�pertoire cache
    
    if flagModified
        if firstTime
            str1 = 'Voulez-vous sauvegarder les modifications dans le cache de SonarScope ?';
            str2 = 'Do you want to save the modifications in the SonarScope cache directory ?';
            [rep, flag] = my_questdlg(Lang(str1,str2));
            if ~flag || (rep == 2)
                return
            end
            firstTime = 0;
        end

        flag = write_position(seg, DataPosition, 'DataTrace', DataTrace, 'Carto', Carto); % CoordinateUnits
        if ~flag
            my_close(hw)
            return
        end
    end
end
my_close(hw, 'MsgEnd');
