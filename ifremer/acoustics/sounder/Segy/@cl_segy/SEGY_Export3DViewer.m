function flag = SEGY_Export3DViewer(~, listeFicSEGY, listLayers, nomDirOut, OrigineCLim, CLim, ColormapIndex, Tag, ...
    NbSamplesMax, IdentPartData, TailleMax, Marge, Video, typeOutput)

Carto = [];

N = length(listeFicSEGY);
str1 = 'Export des fichiers SEGY dans GLOBE';
str2 = 'Exporting SEGY files to GLOBE';
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
     my_waitbar(k, N, hw)
    [flag, Carto] = SEGY_Export3DViewer_unitaire(listeFicSEGY{k}, listLayers, nomDirOut, OrigineCLim, CLim, ColormapIndex, Tag, Carto, ...
        NbSamplesMax, IdentPartData, TailleMax, Marge, Video, typeOutput);
end
my_close(hw, 'MsgEnd')


function [flag, Carto] = SEGY_Export3DViewer_unitaire(nomFicSegy, listLayers, nomDirOut, OrigineCLim, CLim, ColormapIndex, Tag, Carto, ...
    NbSamplesMax, IdentPartData, TailleMax, Marge, Video, typeOutput)

%% Cr�ation du r�pertoire

[~, nomDirBin] = fileparts(nomFicSegy);
strIdentPartData = {'SubBottom'; 'WaterColumn'; 'Both'};
if isempty(Tag)
    nomDirXml = fullfile(nomDirOut, [nomDirBin '_' strIdentPartData{IdentPartData}]);
else
    nomDirXml = fullfile(nomDirOut, [nomDirBin '_' strIdentPartData{IdentPartData} '_' Tag]);
end

% nomDirRoot = fileparts(nomDirXml);
% if ~exist(nomDirXml, 'dir')
%     flag = mkdir(nomDirXml);
%     if ~flag
%         messageErreurFichier(nomDirXml, 'WriteFailure');
%         return
%     end
% end
NomFicXML = [nomDirXml '.xml'];

%% Instance cl_segy

a = cl_segy('nomFic', nomFicSegy);

%% Lecture de l''image

I0 = cl_image_I0;
[flag, b, Carto] = import_SEGY(I0, nomFicSegy, 'listLayers', listLayers, 'Carto', Carto);
if ~flag
    return
end

if OrigineCLim == 2
    [flag, CLimXML] = read_ProcessingParameters(a, 'Ssc_ProcessingParameters.xml', 'CLim');
    if flag
        if isempty(CLimXML)
            str1 = sprintf('Le fichier "Ssc_ProcessingParameters.xml" associ� � "%s" ne contient pas de balise "CLim". On utilise le rehaussement de contraste par d�faut de l''image.', nomFicSegy);
            str2 = sprintf('"Ssc_ProcessingParameters.xml" associated to "%s" does not contain the marker "CLim". I will use the default contrast enhancement of the image.', nomFicSegy);
            my_warndlg(Lang(str1,str2), 0, 'TimeDelay', 60);
            CLim = b.CLim;
        else
            CLim = CLimXML;
        end
    else
        str1 = sprintf('Le fichier "Ssc_ProcessingParameters.xml" n''a pas encore �t� g�n�r� pour "%s". On utilise le rehaussement de contraste par d�faut de l''image.', nomFicSegy);
        str2 = sprintf('"Ssc_ProcessingParameters.xml" has not been generated for "%s". I will use the default contrast enhancement of the image.', nomFicSegy);
        my_warndlg(Lang(str1,str2), 0, 'TimeDelay', 60);
        CLim = b.CLim;
    end
end

switch IdentPartData
    case 1 % Subbottom
        [flag, a] = subbottom_suppressWC(b, 'Marge', Marge);
        if ~flag
            return
        end
        nbCol = a.nbColumns;
        subx = 1:min(NbSamplesMax, nbCol);
        
    case 2 % Water Column
        nbCol = b.nbColumns;
        if NbSamplesMax > nbCol
            str1 = sprintf('Le fichier "%s" contient seulement %d colonnes or la visualisation est demand�e � partir de %d', ...
                nomFicSegy, nbCol, NbSamplesMax);
            str2 = sprintf('"%s" has only %d columns but the display is asked to begin from column %d', ...
                nomFicSegy, nbCol, NbSamplesMax);
            my_warndlg(Lang(str1,str2), 0);
            flag = -1;
            return
        end
        subx = max(1,NbSamplesMax):nbCol;
        [flag, a] = subbottom_keepOnly_waterColumn(b, 'subx', subx, 'Marge', Marge); % Marge ???
        if ~flag
            return
        end
        subx = [];
        
    case 3 % Both
        nbCol = b.nbColumns;
        if NbSamplesMax > nbCol
            str1 = sprintf('Le fichier "%s" contient seulement %d colonnes or la visualisation est demand�e � partir de %d', ...
                nomFicSegy, nbCol, NbSamplesMax);
            str2 = sprintf('"%s" has only %d columns but the display is asked to begin from column %d', ...
                nomFicSegy, nbCol, NbSamplesMax);
            my_warndlg(Lang(str1,str2), 0);
            flag = -1;
            return
        end
        nbCol = b.nbColumns;
        subx = max(1,NbSamplesMax):nbCol;
        [a, flag] = extraction(b, 'subx', subx);
        if ~flag
            return
        end
        subx = [];
end

a.CLim          = CLim;
a.ColormapIndex = ColormapIndex;
a.Video         = Video;
flag = sonar_Export3DV_ImagesAlongNavigation_ByBlocks(a, NomFicXML, TailleMax, typeOutput, 'subx', subx, 'Mute');
