function SEGY_ImportNavigation(~, nomFicSEGY, nomFicNav)

if ~iscell(nomFicSEGY)
    nomFicSEGY = {nomFicSEGY};
end

Carto = [];
N = length(nomFicSEGY);
hw = create_waitbar('SEGY navigation import', 'N', N);
for k=1:N
    my_waitbar(k, N, hw);
    [flag, Carto] = SEGY_ImportNavigation_unitaire(nomFicSEGY{k}, nomFicNav, Carto);
    if ~flag
        my_close(hw)
        return
    end
end
my_close(hw, 'MsgEnd')


function [flag, Carto] = SEGY_ImportNavigation_unitaire(nomFicSEGY, nomFicNav, Carto)

this = cl_segy('nomFic', nomFicSEGY);
if isempty(this)
    flag = 0;
    return
end

[flag, DataPosition] = read_position(this);
if ~flag
    return
end

% T = DataPosition.Time.timeMat;
LatitudeImage  = DataPosition.Latitude(:);
LongitudeImage = DataPosition.Longitude(:);
clear DataPosition
DonneesModifiees = 0;

for k=1:length(nomFicNav)
    [flag, Data] = import_nav(nomFicNav{k});
    Longitude    = Data.Nav.Lon;
    Latitude     = Data.Nav.Lat;

    subNonNaN = find(~isnan(Longitude(:)));
    if ~isempty(subNonNaN)
        LatitudeImage(subNonNaN, 1)  = Latitude(subNonNaN);
        LongitudeImage(subNonNaN, 1) = Longitude(subNonNaN);
        DonneesModifiees = 1;
    end
    % Interpolation en cas de mise � disposition uniquement des donn�es extr�mes (Data from CSIRO).
    NbTraces = get(this, 'nbTraces');
    if numel(Data.Nav.Lat) < NbTraces
        LongitudeImage = linspace(Data.Nav.Lon(1), Data.Nav.Lon(2), NbTraces);
        LatitudeImage  = linspace(Data.Nav.Lat(1), Data.Nav.Lat(2), NbTraces);
    end
    %% Ecriture de la nav dans le fichiers cache
    % Renseignement du fichier annexe de Nav pour l'instance cl_ExRaw
    % courante.
    if ~isempty(nomFicNav{k})
        this.NavigationFile = nomFicNav{k};
    end
end

if DonneesModifiees
    DataPosition.Latitude  = LatitudeImage;
    DataPosition.Longitude = LongitudeImage;
    flag = write_position(this, DataPosition);
    if ~flag
        return
    end

    % Ecriture du fichier de r�f�rence de Nav.
    flag = write_NavigationFile(this, this.NavigationFile);
    if ~flag
        return
    end

end
