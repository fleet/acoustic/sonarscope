function [Fig, Carto] = SEGY_PlotNavigation(~, nomFic, varargin)

[varargin, Carto] = getPropertyValue(varargin, 'Carto', []);
[varargin, Fig]   = getPropertyValue(varargin, 'Fig',   []);

%% Cr�ation de la fen�tre avec les menu et callbacks sp�cialis�s pour la nav

Fig = plot_navigation_Figure(Fig);

%% Traitement des fichiers

NbFic = length(nomFic);
str1 = 'Trac� de la navigation des fichiers SEGY';
str2 = 'Plot SEGY navigation';
hw = create_waitbar(Lang(str1,str2), 'N', NbFic);
for k=1:NbFic
    my_waitbar(k, NbFic, hw);
    
    % Lecture des datagrams de navigation
    
    a = cl_segy('nomFic', nomFic{k});
    if isempty(a)
        continue
    end
    
    [flag, DataTrace] = read_TraceHeaders_bin(a, 'Memmapfile', 1);
    if ~flag
        continue
    end
    
    [flag, Data, Carto] = read_position(a, 'Carto', Carto, varargin{:});
    if ~flag
        continue
    end
    
    sz = numel(Data.Latitude);
    % Interpolation si le fichier de Nav ne fait pas la m�me taille que le
    % nb de Traces du profil sismique.
    if sz ~= DataTrace.Dimensions.NbTraceHeaders
        Longitude = interp1(1:sz, Data.Longitude, linspace(1, sz, DataTrace.Dimensions.NbTraceHeaders));
        Latitude  = interp1(1:sz, Data.Latitude,  linspace(1, sz, DataTrace.Dimensions.NbTraceHeaders));
        Longitude = Longitude';
        Latitude  = Latitude';
    else
        Latitude  = Data.Latitude;
        Longitude = Data.Longitude;
    end
    if ~flag
        str1 = sprintf('La navigation n''a pas pu �tre d�cod�e dans le fichier "%s".', nomFic{k});
        str2 = sprintf('The navigation could not be read in file "%s".', nomFic{k});
        my_warndlg(Lang(str1,str2), 0, 'Tag', 'ErreurLectureNavSEGY', 'TimeDelay', 60);
        continue
    end
    
    % On n'affiche que la 1�re ligne longitudinale de la matrice de
    % donn�es.
    
    %{
    TimeNav = datetime(DataTrace.YearDataRecorded(:,1), 1, 1, ...
    DataTrace.HourOfDay(:,1), DataTrace.MinuteOfHour(:,1), DataTrace.SecondOfMinute(:,1)) ...
    + hours((DataTrace.DayOfYear(:,1) - 1) * 24);
    %}

    TimeNav = Data.Time; % Modif JMA le 16/09/2019
    
    if ~flag || isempty(Latitude)
        continue
    end
    
    Longitude = Longitude(:,1);
    Latitude  = Latitude(:,1);
    SonarTime = TimeNav;
    Heading   = [];
    
    plot_navigation(nomFic{k}, Fig, Longitude, Latitude, SonarTime, Heading);
end
my_close(hw, 'MsgEnd');
