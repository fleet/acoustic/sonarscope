% Trace de la navigation et des capteurs de vitesse et de cap d'un fichier .all
%
% Syntax
%   plot_position_data(nomFic, nomSignal)
% 
% Input Arguments 
%   nomFic : Liste de fichiers .all
%   nomSignal : Height | Heading | Speed | CourseVessel | SoundSpeed 
%
% Examples
%   nomDir = '/home3/jackson/calimero/EM1002/ZoneB/B20040916_1'
%   nomDir = '/home1/jackson/FEMME2005'
%   nomDir = 'D:\NIWA_DATA\TAN0105'
%   liste = listeFicOnDir2(nomDir, '.all')
%   plot_position_data(liste, 'Heading')
%   plot_position_data(liste, 'Speed')
%   plot_position_data(liste, 'CourseVessel')
%   plot_position_data(liste, 'SoundSpeed')
%   plot_position_data(liste, 'Height')
%   plot_position_data(liste, 'Mode')
%   plot_position_data(liste, 'Time')
%   plot_position_data(liste, 'Drift')
%   plot_position_data(liste, 'Tide')
%
% See also cl_simrad_all plot_index Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function SEGY_PlotSignalOnNavigation(this, Fig1, nomFic, nomSignal, varargin) %#ok<INUSL>

if ~iscell(nomFic)
    nomFic = {nomFic};
end

NbFic = length(nomFic);
nbColors = 64;
Colors = jet(nbColors);

Ident = rand(1);

figure(Fig1);
set(Fig1, 'UserData', Ident)
hold on;

%% Boucle sur les fichiers

XLim = [Inf -Inf];
YLim = [Inf -Inf];
ZLim = [Inf -Inf];
hw = create_waitbar('SEGY files : reading signal', 'N', NbFic);
for k=1:NbFic
    my_waitbar(k, NbFic, hw);
    this = cl_segy('nomFic', nomFic{k});
    
    [flag, DataTrace]  = read_TraceHeaders_bin(this, 'Memmapfile', 1);
    if ~flag
        continue
    end
    
    [flag, Data] = read_position(this, 'DataTrace', DataTrace);
    if ~flag
        continue
    end
    Longitude = Data.Longitude(:);
    Latitude  = Data.Latitude(:);
    
    if strcmp(nomSignal, 'Tide')
        [pathname, nomficseul] = fileparts(this.nomFic);
        nomFicTide = fullfile(pathname, 'SonarScope', nomficseul, 'Tide.mat');
        if exist(nomFicTide, 'file')
            Signal = loadmat(nomFicTide, 'nomVar', 'Tide');
        else
            continue
        end
    else
        [flag, Signal] = get_signal(this, nomSignal, 'DataTrace', DataTrace);
        if ~flag
            continue
        end
    end
    
    if strcmp(nomSignal, 'Heading')
        Colors = hsv(nbColors);
    end
    
    if strcmp(nomSignal, 'Heading')
        ZLim = [0 360];
    else
        ZLim(1) = min(ZLim(1), double(min(Signal.Value(:), [], 'omitnan')));
        ZLim(2) = max(ZLim(2), double(max(Signal.Value(:), [], 'omitnan')));
    end
    XLim(1) = min(XLim(1), min(Longitude(:), [], 'omitnan'));
    XLim(2) = max(XLim(2), max(Longitude(:), [], 'omitnan'));
    YLim(1) = min(YLim(1), min(Latitude(:),  [], 'omitnan'));
    YLim(2) = max(YLim(2), max(Latitude(:),  [], 'omitnan'));
end
my_close(hw)

figure(Fig1);
if isempty(Signal.Unit)
    title(nomSignal)
else
    title([nomSignal ' (' Signal.Unit ')']);
end

%% Saisie des bornes de la table de couleurs

[flag, ZLim] = saisie_CLim(min(ZLim), max(ZLim), Signal.Unit, 'SignalName', nomSignal, 'hFigHisto', 87698);
if ~flag
    return
end

%%

figure(Fig1);
W = axis;
x = linspace(XLim(1), XLim(1)+100*nbColors*eps(XLim(1)), nbColors);
y = linspace(YLim(1), YLim(2), nbColors);
h = imagesc(x, y, linspace(ZLim(1), ZLim(2), nbColors)); colormap(Colors), colorbar %#ok<NASGU>
% delete(h)
hold on;
axis(W)

hw = create_waitbar('SEGY files : plotting signal', 'N', NbFic);
for k=1:NbFic
    my_waitbar(k, NbFic, hw);
    this = cl_segy('nomFic', nomFic{k});
    plot_position_data_unitaire(this, nomSignal, ZLim, Fig1, nbColors, Colors);
end
my_close(hw, 'MsgEnd')

% if isempty(Z.Unit)
%     FigName = sprintf('Sondeur %d Serial Number  %d - %s', EmModel, SystemSerialNumber, nomSignal);
% else
%     FigName = sprintf('Sondeur %d Serial Number  %d - %s (%s)', EmModel, SystemSerialNumber, nomSignal, Z.Unit);
% end
figure(Fig1);


function plot_position_data_unitaire(this, nomSignal, ZLim, Fig1, nbColors, Colors)

figure(Fig1)
nomFic = this.nomFic;

[flag, DataTrace]  = read_TraceHeaders_bin(this, 'Memmapfile', 1);
if ~flag
    return
end

%% Lecture de la navigation et du signal

[flag, Data] = read_position(this, 'DataTrace', DataTrace);

if ~flag
    return
end
Longitude = Data.Longitude(:);
Latitude  = Data.Latitude(:);
TimeNav   = Data.Time;

%% Lecture du signal

if strcmp(nomSignal, 'Tide')
    [pathname, nomficseul] = fileparts(this.nomFic);
    nomFic = fullfile(pathname, 'SonarScope', nomficseul, 'Tide.mat');
    if exist(nomFic, 'file')
        Signal = loadmat(nomFic, 'nomVar', 'Tide');
    else
        pi
    end
else
    [flag, Signal] = get_signal(this, nomSignal, 'DataTrace', DataTrace);
end
if ~flag
    return
end

%% Trac�

figure(Fig1)
ZData = Signal.Value(:,1);
subNonNaN = find(~isnan(ZData));
if length(subNonNaN) <= 1
    return
end
subNaN = find(isnan(ZData));
ZData(subNaN) = interp1(subNonNaN, ZData(subNonNaN), subNaN, 'linear', 'extrap');

Value = my_interp1(Signal.Time, ZData, TimeNav);

% Value = ZData;
if ZLim(2) == ZLim(1)
    ZLim(1) = ZLim(1) - 0.5;
    ZLim(2) = ZLim(2) + 0.5;
end
coef = (nbColors-1) / (ZLim(2) - ZLim(1));
Value = ceil((Value - ZLim(1)) * coef);

%% Affichage de la navigation

for k=1:nbColors
    sub = find(Value == (k-1));
    if ~isempty(sub)
        h = plot(Longitude(sub), Latitude(sub), '+');
        set(h, 'Color', Colors(k,:), 'Tag', nomFic)
        set_HitTest_Off(h)
    end
end

sub = find(Value < 0);
if ~isempty(sub)
    h = plot(Longitude(sub), Latitude(sub), '+');
    set(h, 'Color', Colors(1,:)*0.8, 'Tag', nomFic)
    set_HitTest_Off(h)
end

sub = find(Value > (nbColors-1));
if ~isempty(sub)
    h = plot(Longitude(sub), Latitude(sub), '+');
    set(h, 'Color', Colors(nbColors,:)*0.8, 'Tag', nomFic)
end


axisGeo;
grid on;
% colorbar % Supprim� le 7/11/2006 car ca remet les infos de colorbar de 0
% � 64
drawnow
