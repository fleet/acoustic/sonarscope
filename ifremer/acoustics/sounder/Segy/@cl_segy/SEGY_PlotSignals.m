function flag = SEGY_PlotSignals(~, nomFic)

if ischar(nomFic)
    nomFic = {nomFic};
end

Fig1 = plot_navigation_Figure([]);

for k=1:length(nomFic)
    flag = SEGY_PlotSignals_unitaire(nomFic{k}, Fig1);
end


function flag = SEGY_PlotSignals_unitaire(nomFic, Fig1)

%% Lecture du fichier

this = cl_segy('nomFic', nomFic, 'Memmapfile', 0);
if isempty(this)
    flag = 0;
    return
end

[~, nomFigure, ext] = fileparts(nomFic);
nomFigure = [nomFigure ext];

%% Lecture de l'entête génèrale

[flag, DataHeader] = SSc_ReadDatagrams(nomFic, 'Ssc_Header');
if ~flag
    return
end

%% Affichage le l'entête génèrale

S = DataHeader;
S = rmfield(S, 'Dimensions');
S = rmfield(S, 'Signals');
S = rmfield(S, 'Strings');
S = rmfield(S, 'TextualFileHeader');
S = rmfield(S, 'NavigationFile');
if isfield(S, 'time')
    S = rmfield(S, 'time');
end
% struct2table(S, 'Title', [nomFigure 'File Header']);
S.filename = nomFic;
try
    SummaryDataHeader = struct2table(S) %#ok<NASGU,NOPRT>
catch ME %#ok<NASGU>
end

%% Lecture des entêtes de traces

[flag, DataTrace] = read_TraceHeaders_bin(this, 'Memmapfile', 0);
if ~flag
    return
end

%% Affichage des signaux

S = DataTrace;
S = rmfield(S, 'Dimensions');
S = rmfield(S, 'Signals');
% struct2table(S, 'Title', [nomFigure 'File Header']);
try
    SummaryDataTrace = struct2table(S); %#ok<NASGU>
catch ME %#ok<NASGU>
end

%% Plot des signaux sélectionnés par l'utilisateur

names = fieldnames(S);
str = {};
nbNames = length(names);
sub = false(1,nbNames);
for k=1:nbNames
    val = S.(names{k});
    switch class(val)
        case 'char'
        case {'double'; 'single'; 'int32'; 'uint32'; 'int16'; 'uint16'; 'int8'; 'uint8'}
            Tab = 50;
            s = names{k};
            n = length(s);
            strSignal = s;
            strSignal(n+1:Tab) = '.';
            s = num2strCode(val);
            n = length(s);
            strSignal(Tab:(Tab+n+1)) = [': ' s];
%             strSignal = strrep(strSignal, ' ', '.');
            str{end+1} = strSignal; %#ok<AGROW>
            sub(k) = true;
        otherwise
    end
end
names = names(sub);

str1 = 'Signaux';
str2 = 'Signals';
[rep, flag] = my_listdlgMultiple(Lang(str1,str2), str);
if ~flag
    return
end
names = names(rep);

FigUtils.createSScFigure('Name', nomFigure);
N = length(names);
for k=1:N
    h1(k) = subplot(N,1,k); %#ok<AGROW>
    PlotUtils.createSScPlot(S.(names{k})); grid on; title(names{k})
end
linkaxes(h1, 'x')

%% Récupération de la navigation

[flag, DataPosition] = read_position(this, 'DataTrace', DataTrace);
if ~flag
    return
end
plot_navigation(nomFic, Fig1, DataPosition.Longitude, DataPosition.Latitude, DataPosition.Time, []);

%% Signaux reconstitués

% Récupération de l'immersion

[flag, Immersion] = get_signal(this, 'Immersion', 'DataTrace', DataTrace);
if ~flag
    return
end
% figure; plot(Immersion.Value); grid

% Récupération de la fréquence d'échantillonnage

[flag, SamplingFrequency] = get_signal(this, 'SamplingFrequency', 'DataTrace', DataTrace);
if ~flag
    return
end


% Récupération de la résolution

[flag, Resolution] = get_signal(this, 'Resolution', 'DataTrace', DataTrace);
if ~flag
    return
end

% Heave

[flag, Heave] = get_signal(this, 'Heave', 'DataTrace', DataTrace);
if ~flag
    return
end

% Tracé des signaux reconstitués

FigUtils.createSScFigure('Name', [nomFigure ' - Built signals']);
h2(1) = subplot(4,1,1); PlotUtils.createSScPlot(Immersion.Time,         Immersion.Value);           grid on; title(['Immersion ('           Immersion.Unit ')'])
h2(2) = subplot(4,1,2); PlotUtils.createSScPlot(SamplingFrequency.Time, SamplingFrequency.Value);   grid on; title(['SamplingFrequency ('   SamplingFrequency.Unit ')'])
h2(3) = subplot(4,1,3); PlotUtils.createSScPlot(Resolution.Time,        Resolution.Value);          grid on; title(['Resolution ('          Resolution.Unit ')'])
h2(4) = subplot(4,1,4); PlotUtils.createSScPlot(Heave.Time,             Heave.Value);               grid on; title(['Heave ('               Heave.Unit ')'])
linkaxes(h2, 'x')

flag = 1;