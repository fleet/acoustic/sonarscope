function SEGY_ProcesChirp(this, listFileNames, objParams) %#ok<INUSL>

if isempty(objParams)
    return
end

if ~iscell(listFileNames)
    listFileNames = {listFileNames};
end

%% Traitement des fichiers

Carto = [];
I0 = cl_image_I0;

N = length(listFileNames);
str1 = 'Déconvolution du signal des fichiers SEGY';
str2 = 'Chirp processing of SEGY files';
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
    my_waitbar(k, N, hw);
    
    str1 = sprintf('Traitement du fichier %d/%d : "%s" en cours.', k, N, listFileNames{k});
    str2 = sprintf('Processing file %d/%d : "%s".', k, N, listFileNames{k});
    fprintf(1,'%s\n', Lang(str1,str2));
    
    %% Lecture du fichier
    
    seg = cl_segy('nomFic', listFileNames{k});
    
    %% Importation de l'image
    
    [flag, nbLayers] = getNumberLayers(seg);
    if ~flag
        my_close(hw)
        return
    end
    [flag, b, Carto] = import_SEGY(I0, listFileNames{k}, 'Carto', Carto, 'listLayers', nbLayers, 'RawData', 1); % On lit le dernier layer
    if ~flag
        my_close(hw)
        return
    end
    
    [flag, b] = subbottom_sbp_processing(b, objParams);
    if ~flag
        my_close(hw)
        return
    end
end
my_close(hw, 'MsgEnd')
