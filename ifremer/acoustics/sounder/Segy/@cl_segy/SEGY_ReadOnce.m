function SEGY_ReadOnce(~, listFileNames)

if ~iscell(listFileNames)
    listFileNames = {listFileNames};
end

useParallel = get_UseParallelTbx;

str1 = 'Prétraitement des fichiers SEGY';
str2 = 'Preprocessing SEGY files';
N = length(listFileNames);
if useParallel && (N > 1)
    [hw, DQ] = create_parforWaitbar(N, Lang(str1,str2));
    parfor k=1:N
        fprintf('%d/%d - %s\n', k, N, listFileNames{k});
        try
            a = cl_segy('nomFic', listFileNames{k}); %#ok<NASGU>
            fprintf('  %s : OK\n', listFileNames{k});
        catch ME
            ErrorReport = getReport(ME)
            SendEmailSupport(ErrorReport)
            fprintf(' : ERROR\n');
        end
        send(DQ, k);
    end
    my_close(hw, 'MsgEnd')
else
    hw = create_waitbar(Lang(str1,str2), 'N', N);
    for k=1:N
        my_waitbar(k, N, hw)
        fprintf('%d/%d - %s\n', k, N, listFileNames{k});
        try
            a = cl_segy('nomFic', listFileNames{k}); %#ok<NASGU>
            fprintf('  %s : OK\n', listFileNames{k});
        catch ME
            ErrorReport = getReport(ME) %#ok<NOPRT>
            SendEmailSupport(ErrorReport)
            fprintf(' : ERROR\n');
        end
    end
    my_close(hw, 'MsgEnd')
end