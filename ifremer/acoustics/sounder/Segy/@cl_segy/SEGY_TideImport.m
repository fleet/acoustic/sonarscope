function SEGY_TideImport(~, listFileNames, NomFicTide)

if ~iscell(listFileNames)
    listFileNames = {listFileNames};
end

%% Lecture de la mar�e

[flag, FullTide] = read_Tide(NomFicTide);
if ~flag
    return
end

%% Import de la mar�e dans les fichiers SEGY

Carto = [];
nbFic = length(listFileNames);
str1 = 'Importation de la mar�e dans les SEGY';
str2 = 'Tide Import in the SEGY files';
hw = create_waitbar(Lang(str1,str2), 'N', nbFic);
for k=1:nbFic
    my_waitbar(k, nbFic, hw);
    [flag, Carto] = SEGY_TideImport_unitaire(listFileNames{k}, FullTide, Carto); 
    if ~flag
        continue
    end
end
my_close(hw, 'MsgEnd')


function [flag, Carto] = SEGY_TideImport_unitaire(nomFic, FullTide, Carto)

flag = 0;

seg = cl_segy('nomFic', nomFic);
if isempty(seg)
    return
end

[flag, DataPosition, Carto] = read_position(seg, 'Carto', Carto);
if ~flag
    return
end

X = my_interp1(FullTide.Datetime, FullTide.Value, DataPosition.Time);
if isempty(X)
    return
end

X(isnan(X)) = 0;
Tide.Time  = DataPosition.Time;
Tide.Value = X;
Tide.Unit  = 'm';

[pathname, nomficseul] = fileparts(seg.nomFic);
nomFic = fullfile(pathname, 'SonarScope', nomficseul, 'Tide.mat');
try
    save(nomFic, 'Tide');
catch ME
    ME.getReport
end

flag = 1;
