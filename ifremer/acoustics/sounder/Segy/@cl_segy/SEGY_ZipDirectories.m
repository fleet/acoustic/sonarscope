function SEGY_ZipDirectories(this, nomFic, varargin) %#ok<INUSL>

if ~iscell(nomFic)
    nomFic = {nomFic};
end

% H0 = cl_hac([]);
N = length(nomFic);
hw = create_waitbar('Processing', 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    zipSonarScopeCache(nomFic{k})
end
my_close(hw, 'MsgEnd')
