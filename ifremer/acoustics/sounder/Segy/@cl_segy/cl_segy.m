% Constructeur de cl_segy (Fichier de sismique, sondeurs de s�diments contenue dans une .hac)
%
% Syntax
%   a = cl_segy(...)
% 
% Name-Value Pair Arguments
%   nomFic : Nom(s) du fichier .segy
%  
% Output Arguments 
%   a : Instance de cl_segy
%
% Examples
%   nomFic = fullfile(  'F:\SonarScopeData\SEGY From JMA\LisiblePar_read_segy', ...
%                       'AUV0001_D20091123_T050935_process.seg');
%   nomFic = fullfile(  'F:\SonarScopeData\SEGY From JMA\Marmesonet\SDS-auv_Marmesonet1', ...
%                       'AUV001_PR04_proc.seg');
%   nomFic = fullfile(  'F:\SonarScopeData\SEGY From JMA\Geo-Azur', ...
%                       '20101206053318_CH1_35R.seg');
%   a = cl_segy('nomFic', nomFic, 'Memmapfile', 1)
%
% See also cl_hac Authors
% Authors : GLU
% ----------------------------------------------------------------------------

function this = cl_segy(varargin)

%% Definition de la structure

this.nomFic           = '';
% this.Image          = [];
this.SegyTraceHeaders = [];
this.SegyHeader       = [];
this.NbTraces         = 0;
this.NbSamples        = 0;
this.NbProfils        = 0;

% Fichier de Navigation Annexe.
this.NavigationFile  = 0;

%% Cr�ation de l'instance

this = class(this, 'cl_segy');

%% On regarde si l'objet a �t� cr�� uniquement pour atteindre une m�thode de la classe

if (nargin == 1) && isempty(varargin{1})
    return
end

%% Initialisation des valeurs transmises lors de l'appel au constructeur

this = set(this, varargin{:});
