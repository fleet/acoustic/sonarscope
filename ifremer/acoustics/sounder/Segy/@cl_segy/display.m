% Representation externe d'une instance sur le terminal
%
% Syntax
%   display(this)
%
% Input Arguments 
%   this : une instance de cl_segy
%
% Examples
%   nomFic = fullfile(  'F:\SonarScopeData\SEGY From JMA\LisiblePar_read_segy', 
%                       'AUV0001_D20091123_T050935_process.seg');
%   nomFic = fullfile(  'F:\SonarScopeData\SEGY From JMA\Marmesonet\SDS-auv_Marmesonet1', 
%                       'AUV001_PR04_proc.seg');
%   a = cl_segy('nomFic', nomFic)
%   display(a)
%   a
%
% See also cl_segy cl_segy/char Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function display(this)

s = whos('this');
sz = s.size;

fprintf('Name\tSize\tClass\n');
fprintf('%s\t%s\t%s\n', inputname(1), num2strCode(sz), s.class);

disp(char(this));
