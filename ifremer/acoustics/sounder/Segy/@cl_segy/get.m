% Acces en lecture des proprietes d'une instance
%
% Syntax
%   [...] = get(this, ...)
% 
% Input Arguments 
%   this : Une instance de la classe cl_segy
%
% Name-Value Pair Arguments
%   nomFic : Nom du fichier .segy
%
% Output Arguments
%   PropertyValues associees aux PropertyNames passes en argument
% 
% Examples
%   nomFic = fullfile(  'F:\SonarScopeData\SEGY From JMA\Marmesonet\SDS-auv_Marmesonet1', 
%                       'AUV001_PR04_proc.seg');
%   a = cl_segy('nomFic', nomFic)
%   nomFic = get(a, 'nomFic')
%
% See also cl_segy cl_segy/char Authors
% Authors : GLU
% ----------------------------------------------------------------------------

function varargout = get(this, varargin)

%% Boucle de parcours des arguments

varargout = {};
for k=1:length(varargin)
    switch varargin{k}
        case 'nomFic'
            varargout{end+1} = this.nomFic; %#ok<AGROW>
            
        case 'nbPings'
            % TODO : il faut cr�er une propri�t� "nbPings" dans l'instance,
            % ce qui perm�trait d'acc�der directement � cette donn�e
            [flag, DataTrace]  = read_TraceHeaders_bin(this, 'Memmapfile', 1);
            varargout{end+1} = length(DataTrace.DayOfYear); %#ok<AGROW>

        case 'TextualFileHeader'
            % A am�liorer avec regexprep : 
            s = regexprep(char(this.SegyHeader.TextualFileHeader)', 'C+[0-9]', '\n');
            for j=1:9
                s = regexprep(s, ['C ' num2str(j)], ['\r' num2str(j)]);
            end
            varargout{end+1} = s; %#ok<AGROW>
            
        case 'DataTracePerEnsemble'
            varargout{end+1} = this.SegyHeader.DataTracePerEnsemble; %#ok<AGROW>
            
        case 'SegyHeader'
            varargout{end+1} = this.SegyHeader; %#ok<AGROW>

        case 'SegyTraceHeaders'
            varargout{end+1} = this.SegyTraceHeaders; %#ok<AGROW>

        case 'Rev'
            if this.SegyHeader.SegyFormatRevisionNumber == 0
                SegyFormatRevisionNumber = 0;
            elseif this.SegyHeader.SegyFormatRevisionNumber == 1
                SegyFormatRevisionNumber = 100;
            end    
            pppp = find([this.SegyHeader.Rev.SegyFormatRevisionNumber], SegyFormatRevisionNumber);
            varargout{end+1} = this.SegyHeader.Rev(pppp); %#ok<AGROW>
        
        case 'AcqFreq'
            varargout{end+1} = this.SegyHeader.(dt*1e-6); %#ok<AGROW>
            
        case 'NavigationFile'
            varargout{end+1} = this.NavigationFile; %#ok<AGROW>

        case 'DataFormat'
            switch this.SegyHeader.DataSampleFormat
                case 1
                    format='int32';
                case 2
                    format='int32';
                case 3
                    format='int16';
                case 4
                    format='';
                case 5
                    format='float32';
                case 6
                    format='';
                case 7
                    format='';
                case 8
                    format='int8';
            end
            varargout{end+1} = [num2str(this.SegyHeader.DataSampleFormat) ' | ' format]; %#ok<AGROW>

        case 'nbTraces'
            varargout{end+1} = this.NbTraces; %#ok<AGROW>
            
        otherwise
            w = sprintf('cl_segy/get %s non pris en compte', varargin{k});
            my_warndlg(w, 0);
            varargout{end+1} = []; %#ok<AGROW>
    end
end
