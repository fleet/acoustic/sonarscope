function [flag, nbLayers] = getNumberLayers(this, nomFic)

nbLayers = [];

%% Cr�ation de l'instance

if nargin == 2
    this = cl_segy('nomFic', nomFic, 'Memmapfile', 0);
else
    nomFic = this.nomFic;
end
if isempty(this)
    flag = 0;
    return
end

%% Test pour choisir la nature de la donn�e.

[dirName, fileName, ~]  = fileparts(nomFic);
nomDirSsc               = fullfile(dirName, 'SonarScope', fileName);
nomFicDataXml           = fullfile(nomDirSsc, 'Ssc_Data_SBPProcess_1.xml');
if exist(nomFicDataXml, 'file')
    nbLayers = 2;
else
    nbLayers = 1;
end

flag = 1;
