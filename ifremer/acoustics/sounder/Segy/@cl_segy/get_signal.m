function [flag, Signal] = get_signal(this, nomSignal, varargin)

[varargin, DataTrace] = getPropertyValue(varargin, 'DataTrace', []); %#ok<ASGLU>

Signal = [];

if isempty(DataTrace)
    [flag, DataTrace]  = read_TraceHeaders_bin(this, 'Memmapfile', 1);
    if ~flag
        return
    end
end

%Signal commun � bcp de signaux.
[flag, Time, subOK, subKO]  = get_time(this, 'DataTrace', DataTrace); %#ok<ASGLU>
if ~flag
    return
end
switch nomSignal
    case 'Time'
        Signal.Time  = Time;
        Signal.Value = datenum(Time);
        Signal.Unit  = 'Time';
        
    case 'Date'
        Signal.Time  = Time;
        Signal.Value = floor(datenum(Time));
        Signal.Unit  = 'Date';
        
    case 'Hour'
        Signal.Time  = Time;
        Signal.Value = (datenum(Time) - floor(datenum(Time))) * 24;
        Signal.Unit  = 'Hour';
        
    case 'SamplingFrequency'
        SamplingFrequency = 1e6 ./ double(DataTrace.dt(:));
        Signal.Time  = Time;
        Signal.Value = SamplingFrequency;
        Signal.Unit  = 'Hz';
        
    case 'Resolution'
        Celerity = DataTrace.WeatheringVelocity(:);
        
        % Cas des segy de GOLD : toutes les c�l�rit�s sont � z�ro TODO :
        % voir avec Anne ce qu'il faut faire
        Celerity(Celerity == 0) = 1500;
        
        resol = double(Celerity) .* double(DataTrace.dt(:)) * 1e-6 / 2;
        Signal.Time  = Time;
        Signal.Value = resol;
        Signal.Unit  = 'm';
        
        
    case 'Heading' % TODO : pb points �gaux 2 � 2
        [flag, Data] = read_position(this, 'DataTrace', DataTrace);
        if ~flag
            return
        end
        Longitude = Data.Longitude(:);
        Latitude  = Data.Latitude(:);
        Signal.Time  = Time;
        Signal.Value = calCapFromLatLon(Latitude, Longitude);
        Signal.Unit  = 'deg';
        
    case 'Height'
        [flag, Data] = readHeightSegyInCache(this.nomFic);
        if flag
            % Height = Data.Height + Offset;
            Signal.Value = Data.Height;
        else
            flag = 1;
            if isfield(Signal, 'Time')
                Signal.Value = zeros(length(Signal.Time), 1, 'single');
            else
                Signal.Value = zeros(length(DataTrace.TraceNumber), 1, 'single');
            end
        end
        Signal.Time  = Time;
        Signal.Unit  = 'sample';
        
    case 'PingNumber'
        Signal.Time  = Time;
        Signal.Value = DataTrace.TraceNumber(:,:);
        Signal.Unit  = '';
        
    case 'Immersion'
        Celerity = DataTrace.WeatheringVelocity(:);
        Celerity(Celerity == 0) = 1500;
        
        DeltaImmer = double(Celerity) .* double(DataTrace.DelayRecordingTime(:)) * (1.e-3 / 2);
%       Immersion = DataTrace.SourceSurfaceElevation(:) ./ double(abs(DataTrace.ElevationScalar(:)));
        Immersion = DataTrace.SourceDepth(:) ./ DataTrace.ElevationScalar(:); % Modif JMA le 06/06/2019 (donn�es SBP NIWA QUOI)
        
        Signal.Time  = Time;
        %         Signal.Value = Offset + pppp; % !!! GLULUGLU: r�gression sur les
        %         fichiers AUV.
        Signal.Value = Immersion - DeltaImmer;
        Signal.Unit  = '';
        
    case 'Depth'
        [flag, Height] = get_signal(this, 'Height', 'DataTrace', DataTrace);
        if ~flag
            return
        end
        [flag, Immersion] = get_signal(this, 'Immersion', 'DataTrace', DataTrace);
        if ~flag
            return
        end
        resol = 1500 * double(DataTrace.dt(:)) * 1e-6 / 2;
        
%         Signal.Time  = Immersion.Time;
        Signal.Time  = Time; % Modif JMA le 11/09/2019  Y'avait peut-�tre une raison pour qu'on prenne l'heure dans Immarsion ?
        % TODO : v�rifier les unit�s
        Signal.Value = -Height.Value .* resol - Immersion.Value .* resol;
        Signal.Unit  = 'm';
        
    case 'Heave'
        Heave = double(DataTrace.CompenseMvt(:));
        Signal.Time  = Time;
        Signal.Value =  Heave*1e-3;
        Signal.Unit  = 'm';
        
    otherwise
        str1 = sprintf('%s pas encore programm� dans cl_segy/get_signal', nomSignal);
        str2 = sprintf('%s not yet backuped dans cl_segy/get_signal', nomSignal);
        my_warndlg(Lang(str1,str2), 1);
        flag = 0;
end

% sub = (diff(Signal.Time) == 0);
% Signal.Time(sub)  = [];
% Signal.Value(sub) = [];