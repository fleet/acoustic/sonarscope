function [flag, Time, subOK, subKO] = get_time(this, varargin)

[varargin, DataTrace] = getPropertyValue(varargin, 'DataTrace', []); %#ok<ASGLU>

if isempty(DataTrace)
    [flag, DataTrace] = read_TraceHeaders_bin(this, 'Memmapfile', 1);
    if ~flag
        Time = [];
        return
    end
end
Time = datetime(DataTrace.YearDataRecorded(:), 1, 1, ...
    DataTrace.HourOfDay(:), DataTrace.MinuteOfHour(:), DataTrace.SecondOfMinute(:)) ...
    + hours((DataTrace.DayOfYear(:) - 1) * 24);

% D�but ajout JMA le 11/09/2019
sub = (diff(Time) == 0);
subOK = find(~sub);
subKO = find(sub);
Time(subKO) = interp1(subOK, Time(subOK), subKO);
% Fin ajout JMA le 11/09/2019

flag = 1;
