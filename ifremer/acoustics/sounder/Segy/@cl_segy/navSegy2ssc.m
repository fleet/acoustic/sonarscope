%
% Conversion d'une navigation SEGY vers le Cache SSC.
%
% Syntax
%   flag = navSegy2ssc(nomFicNav)
%
% Input Arguments
%   a           : instance de la classe cl_segy.
%   nomFicNav   : Nom du fichier de navigation
%   DataTrace   : structure DataTrace
%   DataPosition : Comportant les nlles positions.
%
% Output Arguments
%   flag        : indicateur de fonctionnement
%   DataTrace   : structure DataTrace modifi�e des nouvelles positions
%
% Examples
%   TODO
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [flag, DataTrace] = navSegy2ssc(a, nomFicNav, DataTrace, DataPosition, flagMemmapfile, varargin)

flag            = 0;
FormatVersion   = 20110914;

[varargin, DataProcess] = getPropertyValue(varargin, 'flagDataProcess', []); %#ok<ASGLU>

nbOc = sizeFic(nomFicNav);
if nbOc == 0
    return
end

[nomDir, nomFicSeul, ~] =  fileparts(get(a, 'nomFic'));
strStatusData = '';

[nomDir, nomFicSeul] = fileparts(nomFicNav);
nomDirRacine = fullfile(nomDir, 'SonarScope', nomFicSeul);
if isempty(DataProcess)
    % D�tection d'un traitement pr�alable du SEGY.
    nomFicXml               = fullfile(nomDirRacine, 'Ssc_TraceHeaders_SBPProcess.xml');
    if exist(nomFicXml, 'file')
        strStatusData = '_SBPProcess';
    end
else
    if DataProcess == 1
        strStatusData = '_SBPProcess';
    end
end

unzipSonarScopeCache(nomDirRacine);

%% Lecture du fichier XML d�crivant la donn�e

nomFicXml = fullfile(nomDirRacine, ['Ssc_TraceHeaders' strStatusData '.xml']);
flag = exist(nomFicXml, 'file');
if ~flag
    return
end
% edit(nomFicXml)
DataXml = xml_mat_read(nomFicXml);
% gen_object_display(Data)

%% G�n�ration des XML et des binaires
if ~exist(nomDirRacine, 'dir')
    mkdir(nomDirRacine);
else
    flag = [];
    
    % Test d'existence des fichiers de cache d�j� g�n�r�s
    flag(end+1) = exist(fullfile(nomDirRacine, 'Ssc_TraceHeaders.xml'), 'file');
    flag(end+1) = exist(fullfile(nomDirRacine, 'Ssc_TraceHeaders'), 'dir');
    
    if any(flag==0) && any(getFormatVersion(nomDirRacine) ~= FormatVersion)
        flag = 1;
        return
    end
end

% try
msg = 'OK';

if isempty(DataPosition.Latitude)
    msg = [];
end

% catch %#ok<CTCH>
%      str1 = sprintf('La lecture d''un fichier de Navigation Annexe au SEGY a �chou� sur le fichier %s', nomFicNav);
%      str2 = sprintf('Navigation data from Annex File does not seem working for file %s', nomFicNav);
%      my_warndlg(Lang(str1,str2), 0);
%      return
% end

if isempty(msg)
    str1 = sprintf('La lecture d''un fichier de Navigation Annexe au SEGY a �chou� sur le fichier %s', nomFicNav);
    str2 = sprintf('Navigation data from Annexe File does not seem working for file %s', nomFicNav);
    my_warndlg(Lang(str1,str2), 0, 'Tag', nomFicNav);
end

if ~exist(nomDirRacine, 'dir')
    if isempty(msg)
        str1 = sprintf('La lecture d''un fichier de Navigation Annexe au SEGY a �chou� sur le fichier %s', nomFicNav);
        str2 = sprintf('Navigation data from Annexe File does not seem working for file %s', nomFicNav);
        my_warndlg(Lang(str1,str2), 0);
        msg = Lang(str1,str2);
    end
    my_warndlg(msg, 0, 'Tag', nomFicNav);
    
    flag = 0;
    return
end
% my_close(hw)

%% Reformmatage des donn�es de position au cas o� elles ne sont pas �chantillonn�es comme le fichier Segy d'origine.
sizeOrig                = numel(DataPosition.Latitude);
Data.SourceX            = interp1(1:sizeOrig, DataPosition.Longitude, linspace(1, sizeOrig, DataTrace.Dimensions.NbTraceHeaders));
Data.SourceY            = interp1(1:sizeOrig, DataPosition.Latitude, linspace(1, sizeOrig, DataTrace.Dimensions.NbTraceHeaders));
Data.CoordinateUnits  	= interp1(1:sizeOrig, DataPosition.coordUnits, linspace(1, sizeOrig, DataTrace.Dimensions.NbTraceHeaders));
Data.cdpX               = Data.SourceX';
Data.cdpY               = Data.SourceY';
Data.SourceX            = Data.SourceX';
Data.SourceY            = Data.SourceY';
Data.CoordinateUnits    = Data.CoordinateUnits';
% Gestion des facteurs d'�chelle des coordonn�es.
coordScaleFactor        = ones(DataTrace.Dimensions.NbTraceHeaders, DataTrace.Dimensions.NbLines, 'double'); % Source Group Scalar, GroupX, GroupY
Data.SourceGroupScalar  = coordScaleFactor;
Data.GroupX             = coordScaleFactor;
Data.GroupY             = coordScaleFactor;

%% Ecriture des donn�es r�cup�r�es depuis un fichier Annexe de navigation.

Info.Signals(1).Name          = 'SourceX';
Info.Signals(end).Dimensions  = 'NbTraceHeaders, NbLines'; %tabDim{i};
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile('Ssc_TraceHeaders', 'SourceX.bin');
Info.Signals(end).Tag         = verifKeyWordSegy('SourceX');

Info.Signals(end+1).Name      = 'SourceY';
Info.Signals(end).Dimensions  = 'NbTraceHeaders, NbLines'; %tabDim{i};
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile('Ssc_TraceHeaders', 'SourceY.bin');
Info.Signals(end).Tag         = verifKeyWordSegy('SourceY');

Info.Signals(end+1).Name      = 'GroupX';
Info.Signals(end).Dimensions  = 'NbTraceHeaders, NbLines'; %tabDim{i};
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile('Ssc_TraceHeaders', 'GroupX.bin');
Info.Signals(end).Tag         = verifKeyWordSegy('GroupX');

Info.Signals(end+1).Name      = 'GroupY';
Info.Signals(end).Dimensions  = 'NbTraceHeaders, NbLines'; %tabDim{i};
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile('Ssc_TraceHeaders', 'GroupY.bin');
Info.Signals(end).Tag         = verifKeyWordSegy('GroupY');

Info.Signals(end+1).Name      = 'cdpX';
Info.Signals(end).Dimensions  = 'NbTraceHeaders, NbLines'; %tabDim{i};
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile('Ssc_TraceHeaders', 'cdpX.bin');
Info.Signals(end).Tag         = verifKeyWordSegy('cdpX');

Info.Signals(end+1).Name      = 'cdpY';
Info.Signals(end).Dimensions  = 'NbTraceHeaders, NbLines'; %tabDim{i};
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile('Ssc_TraceHeaders', 'cdpY.bin');
Info.Signals(end).Tag         = verifKeyWordSegy('cdpY');

Info.Signals(end+1).Name      = 'SourceGroupScalar';
Info.Signals(end).Dimensions  = 'NbTraceHeaders, NbLines'; %tabDim{i};
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile('Ssc_TraceHeaders', 'SourceGroupScalar.bin');
Info.Signals(end).Tag         = verifKeyWordSegy('SourceGroupScalar');

Info.Signals(end+1).Name      = 'CoordinateUnits';
Info.Signals(end).Dimensions  = 'NbTraceHeaders, NbLines'; %tabDim{i};
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile('Ssc_TraceHeaders', 'CoordinateUnits.bin');
Info.Signals(end).Tag         = verifKeyWordSegy('CoordinateUnits');


%% Cr�ation du r�pertoire SegyTraceHeaders

nomDirSegyTraceHeaders = fullfile(nomDirRacine, 'Ssc_TraceHeaders');
if ~exist(nomDirSegyTraceHeaders, 'dir')
    status = mkdir(nomDirSegyTraceHeaders);
    if ~status
        messageErreur(nomDirSegyTraceHeaders)
        flag = 0;
        return
    end
end

%% Cr�ation des fichiers binaires des signaux

for i=1:length(Info.Signals)
    flag = writeSignal(nomDirRacine, Info.Signals(i), [Data.(Info.Signals(i).Name)]);
    if ~flag
        return
    end
end

flag = 1;


% Sont r�percut�es dans cdpX et cdpY  les valeurs de Sources X et Y apr�s
% cr�ation des fichiers binaires.
sz = [DataTrace.Dimensions.NbTraceHeaders DataTrace.Dimensions.NbLines];
if ~flagMemmapfile
    DataTrace.cdpX              = Data.SourceX;
    DataTrace.cdpY              = Data.SourceY;
    DataTrace.SourceX           = Data.SourceX;
    DataTrace.SourceY           = Data.SourceY;
    DataTrace.SourceGroupScalar = Data.SourceGroupScalar;
    DataTrace.GroupX            = Data.GroupX;
    DataTrace.GroupY            = Data.GroupY;
    DataTrace.CoordinateUnits   = Data.CoordinateUnits;
else
    pppp = fullfile(nomDir, 'SonarScope', nomFicSeul, 'Ssc_TraceHeaders', 'cdpX.bin');
    DataTrace.cdpX            = cl_memmapfile('Filename', pppp, 'Value', Data.cdpX, 'Size', sz, 'Format', 'double');
    
    pppp = fullfile(nomDir, 'SonarScope', nomFicSeul, 'Ssc_TraceHeaders', 'cdpY.bin');
    DataTrace.cdpY            = cl_memmapfile('Filename', pppp, 'Value', Data.cdpY, 'Size', sz, 'Format', 'double');
    
    pppp = fullfile(nomDir, 'SonarScope', nomFicSeul, 'Ssc_TraceHeaders', 'SourceX.bin');
    DataTrace.SourceX         = cl_memmapfile('Filename', pppp, 'Value', Data.SourceX, 'Size', sz, 'Format', 'double');
    
    pppp = fullfile(nomDir, 'SonarScope', nomFicSeul, 'Ssc_TraceHeaders', 'SourceY.bin');
    DataTrace.SourceY         = cl_memmapfile('Filename', pppp, 'Value', Data.SourceY, 'Size', sz, 'Format', 'double');
    
    pppp = fullfile(nomDir, 'SonarScope', nomFicSeul, 'Ssc_TraceHeaders', 'CoordinateUnits.bin');
    DataTrace.CoordinateUnits = cl_memmapfile('Filename', pppp, 'Value', Data.CoordinateUnits, 'Size', sz, 'Format', 'single');
    
    pppp = fullfile(nomDir, 'SonarScope', nomFicSeul, 'Ssc_TraceHeaders', 'SourceGroupScalar.bin');
    DataTrace.SourceGroupScalar = cl_memmapfile('Filename', pppp,'Value', Data.SourceGroupScalar, 'Size', sz, 'Format', 'double');
    
    pppp = fullfile(nomDir, 'SonarScope', nomFicSeul, 'Ssc_TraceHeaders', 'GroupX.bin');
    DataTrace.GroupX = cl_memmapfile('Filename', pppp,'Value', Data.GroupX, 'Size', sz, 'Format', 'double');
    
    pppp = fullfile(nomDir, 'SonarScope', nomFicSeul, 'Ssc_TraceHeaders', 'GroupY.bin');
    DataTrace.GroupY = cl_memmapfile('Filename', pppp,'Value', Data.GroupY, 'Size', sz, 'Format', 'double');
end


function FormatVersion = getFormatVersion(nomDirRacine)

nomFicXml = fullfile(nomDirRacine, 'Ssc_Header.xml');
flag = exist(nomFicXml, 'file');
if ~flag
    special_message('FichierNonExistant', nomFicXml)
    return
end
% edit(nomFicXml)
Datagrams = xml_read(nomFicXml);
if ~isfield(Datagrams, 'FormatVersion')
    FormatVersion = 0;
    return
end
FormatVersion = Datagrams.FormatVersion;

