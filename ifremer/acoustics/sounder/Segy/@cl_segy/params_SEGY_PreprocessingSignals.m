function [flag, HeightDetection] = params_SEGY_PreprocessingSignals(~)

HeightDetection = [];

%% Type de detection de hauteur

str1 = 'Au cas o� il y aurait une d�tection de hauteur � faire, quel est le type de signal � consid�rer ?';
str2 = 'In case you have to do a height detection what is the signal type to consider ?';
str3 = 'Alternatif';
str4 = 'Alternating';
str5 = 'Montant';
str6 = 'Rising edge';
[rep, flag] = my_questdlg(Lang(str1,str2), Lang(str3,str4), Lang(str5,str6), 'Init', 2);
if ~flag
    return
end
HeightDetection.TypeSignal = rep;

if HeightDetection.TypeSignal == 2
    str = {'dB'; 'Amp'; 'Enr'};
    [rep, flag] = my_listdlg('Detection on values converted in :', str, 'SelectionMode', 'Single');
    if ~flag
        return
    end
    HeightDetection.TypeUnit = str{rep};
else
    HeightDetection.TypeUnit = [];
end
