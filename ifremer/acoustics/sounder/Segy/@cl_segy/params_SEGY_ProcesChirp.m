function [flag, objParams] = params_SEGY_ProcesChirp(~)

objParams = [];

%% Chirp or not chirp ?

str1 = 'Etes-vous s�r que le signal profient d''un signal � compression d''impulsion ?';
str2 = 'Are you sure the data is coming from a chirp transmission signal ?';
[rep, flag] = my_questdlg(Lang(str1,str2));
if ~flag
    return
end
flag = (rep == 1);
if ~flag
    return
end

%% Traitements Ifremer

[flag, objParams] = ProcessingSegyDialog.paramsProcessingSegy();
if ~flag
    return
end

% TODO : il serait bon de demander les fr�quences du chirp au cas o� les
% valeurs ne seraient pas d�finies dans le fichier
% Voir fonction sbp_processing o� ces param�tres sont demand�s � chaque
% fichier

