function [flag, DataTrace] = SourceLatLong2XY(a, DataTrace, Latitude, Longitude, varargin)

[varargin, Carto] = getPropertyValue(varargin, 'Carto', []); %#ok<ASGLU>

% TODO : attention, si des modifs sont apport�es dans cette fonction, il
% faut absolument mettre � jour la fonction r�ciproque SourceXY2LatLong

flag = 1;
if strfind(a.nomFic, 'cube')
    % Pour IFREMER, on suppose les donn�es 3D (pr�fixe = cube) de positionnement
    % pr�trait�es sous format DMS uniquement localis�es dans les champs cdpX et cdPY.
    posY = convert_dms2ddec(Latitude);
    posX = convert_dms2ddec(Longitude);
    DataTrace.cdpX = double(posX(:));
    DataTrace.cdpY = double(posY(:));
    return
end




%{
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if (DataTrace.CoordinateUnits(1) == 0) || (DataTrace.CoordinateUnits(1) == 2) || (DataTrace.CoordinateUnits(1) == 1) || (DataTrace.CoordinateUnits(1) == 4) % GLU : � v�rifier, modif JMA le 18/08/2017
if sign(unique(DataTrace.SourceGroupScalar)) == 1
Latitude  = Latitude  / double(unique(DataTrace.SourceGroupScalar));
Longitude = Longitude / double(unique(DataTrace.SourceGroupScalar));
else
Latitude  = Latitude  * double(abs(unique(DataTrace.SourceGroupScalar)));
Longitude = Longitude * double(abs(unique(DataTrace.SourceGroupScalar)));
end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%}



% Flag d'application des SourceGroupScalar selon la nature de la position.
flagApplyScaleFactor = 0;
if size(unique(DataTrace.CoordinateUnits), 1) == 1
    if (DataTrace.CoordinateUnits(1) == 0)
        posY = Latitude(:)  * 1e6;
        posX = Longitude(:) * 1e6;
        flagApplyScaleFactor = 1;
        
    elseif (DataTrace.CoordinateUnits(1) == 1)
        % Si les positions sont en Deg * 1e6 (M�tres ou Pieds dans la doc).
        if isempty(Carto)
            nomFic = get(a, 'nomFic');
            C0 = cl_carto([]);
            Carto = define_carto_Folder(C0([]), nomFic);
            if isempty(Carto)
                [flag, Carto] = editParams(cl_carto(), [], [], 'MsgInit');
                if ~flag
                    return
                end
                nomDir = fileparts(nomFic);
                flag = saveCarto(Carto, nomDir, 'Carto_Directory.def');
                if ~flag
                    str1 = sprintf('Le fichier "%s" n''a pas pu �tre cr��, v�rifiez les droits en �criture dans le r�pertoire.', nomDir);
                    str2 = sprintf('"%s" could not be written, check write permissions on the directory please.', nomDir);
                    my_warndlg(Lang(str1,str2), 1);
                    flag = 1;
                    % Carto = [];
                end
            end
        end
        % https://forge.ifremer.fr/tracker/?func=detail&atid=535&aid=570&group_id=126
        % Sym�trie de traitement comme dans SourceXY2Laton.m
        [posX, posY] = latlon2xy(Carto, Latitude(:), Longitude(:));
        
    elseif DataTrace.CoordinateUnits(1) == 2
        % Si les positions sont en Second Arc
        posY = Latitude(:)  * 3600;
        posX = Longitude(:) * 3600;
        
    elseif DataTrace.CoordinateUnits(1) == 3
        % Si les positions en Degr�s d�cimaux
        posY = Latitude(:);
        posX = Longitude(:);
        
    elseif DataTrace.CoordinateUnits(1) == 4
        % Si les positions en DDDMMSS.ss
        posY = convert_ddec2dms(Latitude);
        posX = convert_ddec2dms(Longitude);
    else
        flag = 0;
        return
    end
end

if flagApplyScaleFactor == 1
    [C,~,~]= unique(DataTrace.SourceGroupScalar(:));
    if numel(C) == 1
        if sign(C) == 1 % C positif
            scaleFactor = 1/C;
        else
            scaleFactor = abs(C);
        end
    elseif all(unique(DataTrace.SourceGroupScalar) > 0)
        scaleFactor = 1./DataTrace.SourceGroupScalar(:);
    else
        scaleFactor = abs(DataTrace.SourceGroupScalar(:));
    end
    
    posY = posY * scaleFactor;
    posX = posX * scaleFactor;
end

% D�termination des valeurs de poistionnement signficatives.
if size(unique(DataTrace.SourceX), 1) == 1
    if size(unique(DataTrace.GroupX), 1) == 1
        if size(unique(DataTrace.cdpX), 1) == 1
            str1 = 'R�cup�ration dres positions probl�mariques !';
            str2 = 'Problem with position !';
            my_warndlg(Lang(str1,str2), 1);
            return
        else
            DataTrace.cdpX = double(posX(:));
            DataTrace.cdpY = double(posY(:));
        end
    else
        DataTrace.GroupX = double(posX(:));
        DataTrace.GroupY = double(posY(:));
    end
else
    DataTrace.SourceX = double(posX(:));
    DataTrace.SourceY = double(posY(:));
end

% On ne multiplie pas par le facteur d'�chelle si les coord sont en DMS.
% if DataTrace.CoordinateUnits(1) ~=4
% Pour la stockage de DataTrace.SourceX etY, la prise en compte du facteur
% d'�chelle SourgeGroupScalar est invers�e vis � vis de la conversion lors de l'import (qui �tait XY 2 LatLong)
% if (DataTrace.CoordinateUnits(1) == 1)
if (DataTrace.CoordinateUnits(1) == 0) || (DataTrace.CoordinateUnits(1) == 2) || (DataTrace.CoordinateUnits(1) == 1) || (DataTrace.CoordinateUnits(1) == 4) % GLU : � v�rifier, midif JMA le 24/11/2014
    if sign(unique(DataTrace.SourceGroupScalar)) == 1
        % Fichier GeoLittomer/'HA0051_D20120412_T074618.SEG' : erreur si on
        % prend en compte ce facteur d'�chelle
        DataTrace.SourceX = DataTrace.SourceX / double(unique(DataTrace.SourceGroupScalar));
        DataTrace.SourceY = DataTrace.SourceY / double(unique(DataTrace.SourceGroupScalar));
    else
        DataTrace.SourceX = DataTrace.SourceX * double(abs(unique(DataTrace.SourceGroupScalar)));
        DataTrace.SourceY = DataTrace.SourceY * double(abs(unique(DataTrace.SourceGroupScalar)));
    end
end
% end
