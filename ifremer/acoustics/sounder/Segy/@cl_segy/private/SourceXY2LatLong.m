function [flag, Nav, Carto] = SourceXY2LatLong(a, DataTrace, varargin)

persistent persistent_navFileName

[varargin, Carto] = getPropertyValue(varargin, 'Carto', []); %#ok<ASGLU>

% TODO : attention, si des modifs sont apport�es dans cette fonction, il
% faut absolument mettre � jour la fonction r�ciproque SourceLatLong2XY

Nav.NavigationFile = get(a, 'NavigationFile');
flag = 0;
if strfind(a.nomFic, 'cube')
    % Pour IFREMER, on suppose les donn�es 3D (pr�fixe = cube) de positionnement
    % pr�trait�es sous format DMS uniquement localis�es dans les champs cdpX et cdPY.
    posX          = DataTrace.cdpX(:,:)';
    posX          = reshape(posX,  DataTrace.Dimensions.NbLines,DataTrace.Dimensions.NbTraceHeaders);
    posX          = double(posX);
    Nav.Longitude = convert_dms2ddec(posX);
    
    posY         = DataTrace.cdpY(:,:)';
    posY         = reshape(posY,  DataTrace.Dimensions.NbLines,DataTrace.Dimensions.NbTraceHeaders);
    % posY       = double(DataTrace.cdpY(:));
    posY         = double(posY);
    Nav.Latitude = convert_dms2ddec(posY);
    Nav.CoordinateUnits = DataTrace.CoordinateUnits;
    flag = 1;
    return
end

% D�termination des valeurs de positionnement significatives.
if size(unique(DataTrace.SourceX(:)), 1) == 1
    if size(unique(DataTrace.GroupX(:)), 1) == 1
        if size(unique(DataTrace.cdpX(:)), 1) == 1
            str1 = sprintf('R�cup�ration des positions probl�matiques.\nFile : %s', a.nomFic);
            str2 = sprintf('Problem with positions from file\n%s !', a.nomFic);
            my_warndlg(Lang(str1,str2), 0, 'Tag', 'PbNavigationInGegy', 'displayItEvenIfSSc', 1, 'TimeDelay', 300);
            Nav.Latitude            = [];
            Nav.Longitude           = [];
            Nav.CoordinateUnits     = [];
            return
        else
            posX = double(DataTrace.cdpX(:));
            posY = double(DataTrace.cdpY(:));
        end
    else
        posX = double(DataTrace.GroupX(:));
        posY = double(DataTrace.GroupY(:));
    end
else
    posX = double(DataTrace.SourceX(:));
    posY = double(DataTrace.SourceY(:));
end

% if (DataTrace.CoordinateUnits(1) == 0) || (DataTrace.CoordinateUnits(1) == 1)
if (DataTrace.CoordinateUnits(1) == 0) || (DataTrace.CoordinateUnits(1) == 2) || (DataTrace.CoordinateUnits(1) == 1) || (DataTrace.CoordinateUnits(1) == 4) % GLU : � v�rifier, midif JMA le 24/11/2014
    if sign(unique(DataTrace.SourceGroupScalar)) == 1
        % Fichier GeoLittomer/'HA0051_D20120412_T074618.SEG' : erreur si on
        % prend en compte ce facteur d'�chelle
%         posY = posY * double(unique(DataTrace.SourceGroupScalar));
%         posX = posX * double(unique(DataTrace.SourceGroupScalar));
        posY = posY(:) .* double(DataTrace.SourceGroupScalar(:));
        posX = posX(:) .* double(DataTrace.SourceGroupScalar(:));
    else
%         posY = posY / double(abs(unique(DataTrace.SourceGroupScalar)));
%         posX = posX / double(abs(unique(DataTrace.SourceGroupScalar)));
%         posY = posY(:) ./ double(abs(DataTrace.SourceGroupScalar(:)));
%         posX = posX(:) ./ double(abs(DataTrace.SourceGroupScalar(:)));
        posY = posY(:) ./ double(abs(DataTrace.SourceGroupScalar(:)));
        posX = posX(:) ./ double(abs(DataTrace.SourceGroupScalar(:)));
    end
end
        
if numel(unique(DataTrace.CoordinateUnits(:))) > 1
    % TODO : Fichier QUOI 'E:\QUOI_OUT\tan1806\SBP\SEGY\20180713065824_001.sgy'
    pppp = unique(DataTrace.CoordinateUnits(:));
    Nav.Latitude        = [];
    Nav.Longitude       = [];
    Nav.CoordinateUnits = [];
    return
end

CoordinateUnits = mode(DataTrace.CoordinateUnits(:));
if CoordinateUnits == 1
    % Si les positions sont en Deg * 1e6 (M�tres ou Pieds dans la doc).
    % Hypo : les donn�es sont en UTM.
    % Ancienne m�thode.
    if isempty(Carto)
        nomFic = get(a, 'nomFic');
        C0 = cl_carto([]);
        Carto = define_carto_Folder(C0([]), nomFic);
        if isempty(Carto)
            [flag, Carto] = editParams(cl_carto(), [], [], 'MsgInit');
            if ~flag
                return
            end
            nomDir = fileparts(nomFic);
            flag = saveCarto(Carto, nomDir, 'Carto_Directory.def');
            if ~flag
                str1 = sprintf('Le fichier "%s" n''a pas pu �tre cr��, v�rifiez les droits en �criture dans le r�pertoire.', nomDir);
                str2 = sprintf('"%s" could not be written, check write permissions on the directory please.', nomDir);
                my_warndlg(Lang(str1,str2), 1);
                flag = 1; %#ok<NASGU>
                % Carto = [];
            end
        end
    end
    
    [Nav.Latitude, Nav.Longitude] = xy2latlon(Carto, posX(:), posY(:));
    Nav.CoordinateUnits           = DataTrace.CoordinateUnits;
    Nav.Carto                     = Carto;
    % figure; PlotUtils.createSScPlot(Nav.Latitude, Nav.Longitude);
    
    %% Autre calcul : avec la mapping Toolbox.
    %{
utmstruct       = defaultm('utm');
utmstruct.zone  = Zone;
utmstruct.geoid = referenceEllipsoid('wgs84');
utmstruct       = defaultm(utmstruct) ;
[lat2,lon2]     = minvtran(utmstruct,posX,posY);
figure; PlotUtils.createSScPlot(lat2, lon2);
    %}
    
elseif DataTrace.CoordinateUnits(1) == 2
    % Si les positions sont en Second Arc
    Nav.Latitude        = posY(:) / 3600; % TODO : v�rifier si �a passe en R2010a car �a passe en R2011b (pb cl_memmapfile)
    Nav.Longitude       = posX(:) / 3600;
    Nav.CoordinateUnits = DataTrace.CoordinateUnits;
    
elseif DataTrace.CoordinateUnits(1) == 3
    % Si les positions en Degr�s d�cimaux
    Nav.Latitude        = posY(:);
    Nav.Longitude       = posX(:);
    Nav.CoordinateUnits = DataTrace.CoordinateUnits;
    
elseif DataTrace.CoordinateUnits(1) == 4
    % Si les positions en Degr�s minute, secondes d�cimales
    Nav.Latitude        = convert_dms2ddec(posY);
    Nav.Longitude       = convert_dms2ddec(posX);
    Nav.CoordinateUnits = DataTrace.CoordinateUnits;
else
    strFR = sprintf('Il semble que les coordonn�es soient donn�es en Arc Second. Le confirmez-vous ?');
    strUS = sprintf('It seems the coordinates are given in Arc Seconds coordinates. Do you confirm it ?');
    [rep, flag] = my_questdlg(Lang(strFR, strUS), Lang('Oui', 'Yes'), Lang('Non', 'No'), 'Init', 1);
    if flag == 1 % Cancel
        if rep == 1
            % Si les positions sont en Second Arc
            Nav.Latitude        = posY(:) / 3600;
            Nav.Longitude       = posX(:) / 3600;
            Nav.CoordinateUnits = DataTrace.CoordinateUnits;
            %                 flag = 0;
            flag = 1; % Modif JMA le 17/10/2012
            return
        else % if rep == 2
            strFR = sprintf(['Pour utiliser ces coordonn�es, vous devez saisir les param�tres cartographiques.\n ' ...
                'Vous pouvez �galement importer un fichier\n ' ...
                'Que voulez-vous faire']);
            strUS = sprintf(['To use this coordinates, you have to define the cartographic parameters.\n' ...
                'You can also import a nav file.\n ' ...
                'What do you want to do ?']);
            [rep, flag] = my_questdlg(Lang(strFR, strUS), Lang('Saisir les param�tres cartographiques', 'Input Cartographic Params'), ...
                Lang('Importer un fichier de nav.', 'Import a nav. file'), 'Init', 1);
            if ~flag
                return
            end
            if rep == 1
                % Interrogation des param�tres cartographiques.
                Carto = cl_carto([]);
                [flag, Carto] = editParams(Carto, [],[], 'NoGeodetic', 1);
                [Nav.Latitude, Nav.Longitude]   = xy2latlon(Carto, posX, posY);
                Nav.CoordinateUnits             = ones(numel(Nav.Latitude), 1)*3;
                Nav.NavigationFile              = 'internal';
                disp('Type de coordonn�es � g�rer - GLU - TODO');
                return
                
            elseif rep == 2
                
                if isempty(persistent_navFileName)
                    DefaultName = my_tempdir;
                else
                    DefaultName = persistent_navFileName;
                end
                str1 = 'Nom du fichier de navigation associ� au SEGY.';
                str2 = 'Name of the file associated to the SEGY.';
                [flag, navFileName] = my_uigetfile({'*.txt;*.nav;*.dat' , 'All (*.txt,*.nav,*.dat)'
                    '*.txt', 'ASCII'
                    '*.nav', 'ASCII'
                    '*.dat', 'ASCII'
                    '*.*',  'All Files (*.*)'}, Lang(str1,str2), DefaultName);
                if ~flag
                    return
                end
                persistent_navFileName  = navFileName;
                [flag, Nav]             = readNavFromExternalFile(navFileName);
                Nav.NavigationFile      = navFileName;
                return
            end
        end
    else
        return
    end
    
end
flag = 1;
