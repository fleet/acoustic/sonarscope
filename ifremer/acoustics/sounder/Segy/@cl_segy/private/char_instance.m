% Transformation en chaine de caracteres d'une instance
%
% Syntax
%   str = char_instance(a)
%
% Input Arguments
%   a : instance de cl_image
%
% Name-Value Pair Arguments
%   InfoSonar  : 1=Information de l'image uniquement
%                2=Info de l'information sonar uniquement
%                3=Display de toute l'information disponible (image+sonar) : (3 par defaut)
%
% Output Arguments
%   str : une chaine de caracteres representative de l instance
%
% Examples
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function str = char_instance(this, varargin)

if ~isempty(this.nomFic)
%     strUnit = {'Unknown' , 'Pascal (Pa)', 'Volts (v)', 'Millivolts (mV)', 'Amperes (A)', ...
%                 'Meters (m)', 'Meters per second (m/s)', 'Meters per second squared (m/s2)', ...
%                 'Newton (N)', 'Watt (W)', 'Other (should be described in Data Sample Measurement Units Stanza)'};
% 
%     strSampleFormat = {'int32' , 'int32', 'int16', '', 'float32', ...
%                         '', '', 'int8'};
% 
    str = [];
    str{end+1} = sprintf('NomFic                <-> %s', this.nomFic);
    str{end+1} = sprintf('DataName              <-> %s', 'Seismic');
    str{end+1} = sprintf('NavigationFile        <-> %s', this.NavigationFile);
    
    [flag, DataTrace]  = read_TraceHeaders_bin(this, 'Memmapfile', 1);
    if flag
        listeFields = fieldnames(DataTrace);
        N = length(listeFields);
        for k=1:N
            X = DataTrace.(listeFields{k})(:);
            switch class(X)
                case 'struct'
                case 'cell'
                case 'char'
                    str{end+1} = sprintf('\t\tTraceHeader.%s\t: %s', listeFields{k}, X); %#ok<AGROW>
                case {'single'; 'double'}
                    str{end+1} = sprintf('\t\tTraceHeader.%s\t: %s', listeFields{k}, num2strCode(X)); %#ok<AGROW>
                otherwise
                    class(X)
            end
        end
    end


    %{
    str{end+1} = sprintf('Memory                <-- %s', where(this));
    str{end+1} = sprintf('nbRows                <-- %d', size(this.Image,1)); % this.nbRows);
    str{end+1} = sprintf('nbColumns             <-- %d', size(this.Image,2)); % this.nbColumns);
    % str{end+1} = sprintf('ValNaN            <-> %s', num2str(this.ValNaN));
    str{end+1} = sprintf('DataSampleFormat      <-- %d', this.SegyHeader.DataSampleFormat);
    for i=1:length(strSampleFormat)
        if i == this.SegyHeader.DataSampleFormat
            str{end+1} = sprintf('                  {%d <--> %s}', i, strSampleFormat{i}); %#ok
        else
            str{end+1} = sprintf('                   %d <--> %s', i, strSampleFormat{i}); %#ok
        end
    end
    str{end+1} = sprintf('TraceMeasurementUnit  <-- %d', this.SegyTraceHeaders(1).TraceValueMeasurementUnit);
    for i=1:length(strUnit)
        if i == this.SegyTraceHeaders(1).TraceValueMeasurementUnit+1
            str{end+1} = sprintf('                  {%d <--> %s}', i-1, strUnit{i}); %#ok
        else
            str{end+1} = sprintf('                   %d <--> %s', i-1, strUnit{i}); %#ok
        end
    end

    %}

    % ---------------------------
    % Concatenation en une chaine

    str = cell2str(str);
else
    str = [];
end
