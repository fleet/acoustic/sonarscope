function [flag, Tide] = readTide_mat(this)

Tide = [];
[pathname, nomficseul] = fileparts(this.nomFic);
nomFic = fullfile(pathname, 'SonarScope', nomficseul, 'Tide.mat');
if exist(nomFic, 'file')
    Tide = loadmat(nomFic, 'nomVar', 'Tide');
    flag = 1;
else
    flag = 0;
end
