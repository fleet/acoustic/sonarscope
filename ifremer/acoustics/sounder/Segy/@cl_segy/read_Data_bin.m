function [flag, Data] = read_Data_bin(a, varargin)

[varargin, Memmapfile]  = getPropertyValue(varargin, 'Memmapfile',      0);
[varargin, DataProcess] = getPropertyValue(varargin, 'flagDataProcess', []);
[varargin, SubBloc]     = getPropertyValue(varargin, 'subBloc',         1);
[varargin, RawData]     = getPropertyValue(varargin, 'RawData',         0); %#ok<ASGLU>

flag = 0;
Data = [];
strStatusData = '';

if ~RawData
    if isempty(DataProcess)
        % D�tection d'un traitement pr�alable du SEGY.
        [dirName, fileName, ~] = fileparts(a.nomFic);
        nomDirSsc              = fullfile(dirName, 'SonarScope', fileName);
        nomFicXml              = fullfile(nomDirSsc, 'Ssc_TraceHeaders_SBPProcess.xml');
        if exist(nomFicXml, 'file')
            strStatusData = '_SBPProcess';
        end
    elseif DataProcess == 1
        strStatusData = '_SBPProcess';
    end
end

%% Lecture du r�pertoire SonarScope (� remettre en service si retour � l'ancienne mouture)

% [nomDir, nom] = fileparts(a.nomFic);
% nomDir = fullfile(nomDir, 'SonarScope', nom);
% if ~exist(nomDir, 'dir')
%     return
% end

%% Test d'existence du r�pertoire Ssc_Data (� remettre en service si retour � l'ancienne mouture)

% nomDirSsc = fullfile(nomDir, ['Ssc_Data' strStatusData '_1']);
% if ~exist(nomDirSsc, 'dir')
%     flag = 0;
%     return
% end

%% Lecture du fichier XML d�crivant la donn�e

for k1=1:numel(SubBloc)
    
    % Nouvelle mouture : JMA le 07/02/2020 GLU : � v�rifier
    % {
    [flag, ~, ~, X] = SSc_ReadDatagrams(a.nomFic, ['Ssc_Data' strStatusData '_' num2str(k1)], 'Memmapfile', Memmapfile);
    if ~flag
        return
    end
    fieldName = fieldnames(X);
    Data.(fieldName{1}){k1} = X.Data;
    % }
    
    % Ancienne mouture
    %{
    nomFicXml = fullfile(nomDir, ['Ssc_Data' strStatusData '_' num2str(k1)  '.xml']);
    flag = exist(nomFicXml, 'file');
    if ~flag
        return
    end
    % edit(nomFicXml)
    DataXML = xml_mat_read(nomFicXml);
    % gen_object_display(Data)
    
    % Lecture des fichiers binaires des images
    
    for k2=1:length(DataXML.Images)
        [flag, Image] = read Ssc Signal(nomDir, DataXML.Images(k2), DataXML.Dimensions, 'Memmapfile', Memmapfile);
        if ~flag
            return
        end
        Data.(DataXML.Images(k2).Name){k1} = Image;
    end
    %}
end
