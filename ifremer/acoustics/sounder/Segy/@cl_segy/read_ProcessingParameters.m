function [flag, Var] = read_ProcessingParameters(this, nomFicXml, nomVar)

Var  = [];
flag = 0;

%% Nom du fichier

[nomDir, nom] = fileparts(this.nomFic);
nomFicXml = fullfile(nomDir, 'SonarScope', nom, nomFicXml);

%% Lecture si il existe d�j�

if ~exist(nomFicXml, 'file')
    return
end

XML = xml_read(nomFicXml);

%% Lecture de la variable

if isfield(XML, nomVar)
    Var = XML.(nomVar);
    flag = 1;
end

