function [flag, Data] = read_TraceHeaders_bin(a, varargin)

[varargin, Memmapfile]  = getPropertyValue(varargin, 'Memmapfile',      0);
[varargin, DataProcess] = getPropertyValue(varargin, 'flagDataProcess', []); %#ok<ASGLU>

% flag = 0;
% Data = [];
strStatusData = '';

if isempty(DataProcess)
    % D�tection d'un traitement pr�alable du SEGY.
    [dirName, fileName, ~]  = fileparts(a.nomFic);
    nomDirSsc               = fullfile(dirName, 'SonarScope', fileName);
    nomFicXml               = fullfile(nomDirSsc, 'Ssc_TraceHeaders_SBPProcess.xml');
    if exist(nomFicXml, 'file')
        strStatusData = '_SBPProcess';
    end
elseif DataProcess == 1
	strStatusData = '_SBPProcess';
end

%% Lecture du fichier

% Nouvelle mouture : TODO GLU : � valider
[flag, Data] = SSc_ReadDatagrams(a.nomFic, ['Ssc_TraceHeaders' strStatusData], 'Memmapfile', Memmapfile);
if ~flag
    return
end

% Ancienne mouture
%{
%% Lecture du r�pertoire SonarScope

[nomDir, nom] = fileparts(a.nomFic);
nomDir = fullfile(nomDir, 'SonarScope', nom);
if ~exist(nomDir, 'dir')
    return
end

%% Lecture du fichier XML d�crivant la donn�e

nomFicXml = fullfile(nomDir, ['Ssc_TraceHeaders' strStatusData '.xml']);
flag = exist(nomFicXml, 'file');
if ~flag
    return
end
% edit(nomFicXml)
Data = xml_mat_read(nomFicXml);
% gen_object_display(Data)

%% Test d'existence du r�pertoire Ssc_Header

nomDirDepth = fullfile(nomDir, ['Ssc_TraceHeaders' strStatusData]);
if ~exist(nomDirDepth, 'dir')
    flag = 0;
    return
end

%% Lecture des fichiers binaires des signaux

for k=1:length(Data.Signals)
    %     if (k == 67) || (k == 81) % TODO : supprimer tout �a quand Roger aura corrig� le bug
    %         Memmapfile = 1;
    %     else
    %         Memmapfile = 0;
    %     end
    [flag, Signal] = read Ssc Signal(nomDir, Data.Signals(k), Data.Dimensions, 'Memmapfile', Memmapfile);
    if ~flag
        return
    end
    Data.(Data.Signals(k).Name) = Signal;
    %     figure; plot(Signal(:)); grid on;
end
%}
