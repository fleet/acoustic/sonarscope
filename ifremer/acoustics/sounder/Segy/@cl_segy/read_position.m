function [flag, Data, Carto] = read_position(this, varargin)

[varargin, DataTrace]  = getPropertyValue(varargin, 'DataTrace',  []);
[varargin, Memmapfile] = getPropertyValue(varargin, 'Memmapfile', 1);
[varargin, Carto]      = getPropertyValue(varargin, 'Carto',      []); %#ok<ASGLU>

if isempty(DataTrace)
    [flag, DataTrace]  = read_TraceHeaders_bin(this, 'Memmapfile', Memmapfile);
    if ~flag
        Data = [];
        return
    end
end

% Lecture des Lat/Long/CoordinateUnits
[flag, Data, Carto] = SourceXY2LatLong(this, DataTrace, 'Carto', Carto);
if ~flag
    Data = [];
    return
end

[flag, Time, subOK, subKO] = get_time(this, 'DataTrace', DataTrace);
if ~flag
    return
end
Data.Time = Time;

if ~isempty(subKO)
    Data.Latitude(subKO)  = interp1(subOK, Data.Latitude(subOK), subKO);
    Data.Longitude(subKO) = interp1(subOK, Data.Longitude(subOK), subKO);
end
