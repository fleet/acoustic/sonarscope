
function flag = save_Height(this, Height)

InitialFileName = this.nomFic;
[nomDirRacine, nomFic] = fileparts(InitialFileName);

Data.Height = Height;

Info.Signature1      = 'SonarScope';
Info.Signature2      = 'SegySubbottomHeight';
Info.FormatVersion   = 20110420;
Info.Name            = InitialFileName;

Info.Dimensions.nbPings  = length(Height);

Info.Signals(1).Name          = 'Height';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'Ifremer';
Info.Signals(end).FileName    = fullfile('Ssc_Height', 'Height.bin');

%% Cr�ation du fichier XML d�crivant la donn�e

nomDirRacine = fullfile(nomDirRacine, 'SonarScope');
if ~exist(nomDirRacine, 'dir')
    flag = mkdir(nomDirRacine);
    if ~flag
        messageErreurFichier(nomDirRacine, 'WriteFailure');
        return
    end
end

nomDirRacine = fullfile(nomDirRacine, nomFic);
if ~exist(nomDirRacine, 'dir')
    flag = mkdir(nomDirRacine);
    if ~flag
        messageErreurFichier(nomDirRacine, 'WriteFailure');
        return
    end
end

NomFicXML = fullfile(nomDirRacine, 'Ssc_Height.xml');
xml_write(NomFicXML, Info);
flag = exist(NomFicXML, 'file');
if ~flag
    messageErreur(NomFicXML)
    return
end

%% Cr�ation du r�pertoire Ssc_Height

nomDirSsc = fullfile(nomDirRacine, 'Ssc_Height');
if ~exist(nomDirSsc, 'dir')
    status = mkdir(nomDirSsc);
    if ~status
        messageErreur(nomDirSsc)
        flag = 0;
        return
    end
end

%% Cr�ation des fichiers binaires des signaux

for i=1:length(Info.Signals)
    flag = writeSignal(nomDirRacine, Info.Signals(i), Data.(Info.Signals(i).Name));
    if ~flag
        return
    end
end

flag = 1;


