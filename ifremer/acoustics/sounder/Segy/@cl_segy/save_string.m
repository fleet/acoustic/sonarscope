function flag = save_string(this, NomDataset, NomLayer, X, varargin)

[varargin, nomDirBin] = getPropertyValue(varargin, 'nomDirBin', ['Ssc_' NomDataset]); %#ok<ASGLU>

[nomDir, nom] = fileparts(this.nomFic);
nomFicXml = fullfile(nomDir, 'SonarScope', nom, [nomDirBin '.xml']);
if ~exist(nomFicXml, 'file')
    flag = 0;
    return
end

XML = xml_read(nomFicXml);
nomDirRacine = fullfile(nomDir, 'SonarScope', nom);

k = find(strcmp({XML.Strings.Name}, NomLayer));
if isempty(k)
    str1 = sprintf('String "%s" non trouv� dans "%s', NomLayer, nomFicXml);
    str2 = sprintf('String "%s" not found in "%s', NomLayer, nomFicXml);
    my_warndlg(Lang(str1,str2), 1);
    return
end
flag = writeString(nomDirRacine, XML.Strings(k), X);
