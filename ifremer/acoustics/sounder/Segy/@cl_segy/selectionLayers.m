function [flag, listLayers] = selectionLayers(this, nomFic)

listLayers = [];

%% Cr�ation de l'instance

if nargin == 2
    this = cl_segy('nomFic', nomFic, 'Memmapfile', 0);
else
    nomFic = this.nomFic;
end
if isempty(this)
    flag = 0;
    return
end

%% Test pour choisir la nature de la donn�e.

[dirName, fileName] = fileparts(nomFic);
nomDirSsc           = fullfile(dirName, 'SonarScope', fileName);
nomFicDataXml       = fullfile(nomDirSsc, 'Ssc_Data_SBPProcess_1.xml');
if exist(nomFicDataXml, 'file')
    str1 = 'VType de donn�s du SBP';
    str2 = 'SBP data type';
    str{1} = Lang('Donn�es brutes', 'Raw data');
    str{2} = Lang('Donn�es trait�es', 'Processed data');
    [listLayers, flag] = my_listdlg(Lang(str1,str2), str, 'InitialValue', 2, 'SelectionMode', 'Multiple');
    if ~flag
        return
    end
    % dataToImport = dataToImport - 1;
else
    listLayers = 1;
end

flag = 1;
