% Acces en ecriture des proprietes d'une instance
%
% Syntax
%   b = set(a, ...)
%
% Input Arguments
%   a : Une instance de la classe cl_segy
%
% Name-Value Pair Arguments
%   nomFic : Nom(s) du fichier .segy
%
% Output Arguments
%   [] : Sauve l'instance dans a
%   b  : Instance de la classe
%
% Examples
%   nomFic = fullfile(  'F:\SonarScopeData\SEGY From JMA\Marmesonet\SDS-auv_Marmesonet1',
%                       'AUV001_PR04_proc.seg');
%   a = cl_segy
%   a = set(a, 'nomFic', nomFic)
%
%
% See also cl_segy cl_segy/get Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function varargout = set(this, varargin)

[varargin, nomFic]         = getPropertyValue(varargin, 'nomFic',     []);
[varargin, nomFicNav]      = getPropertyValue(varargin, 'nomFicNav',  []);
[varargin, flagMemmapfile] = getPropertyValue(varargin, 'Memmapfile', 0); %#ok<ASGLU>

if ~isempty(nomFic)
    nomFic = windows_replaceVolumeLetter(nomFic);
    this.nomFic = nomFic;
    [flag, dimData] = segy2ssc(nomFic, 'Memmapfile', flagMemmapfile);
    if ~flag
        varargout{1} = [];
        return
    end
    [flag, FileHeader] = SSc_ReadDatagrams(nomFic, 'Ssc_Header');
    this.SegyHeader    = FileHeader;
    this.NbTraces      = dimData.NbTraces;
    this.NbSamples     = dimData.NbSamples;
    this.NbProfils     = dimData.NbProfils;
    % [flag, TraceHeader]   = read_TraceHeaders_bin(this, 'Memmapfile', 1);
    % this.SegyTraceHeaders = TraceHeader;
    if ~flag
        varargout{1} = [];
        return
    end
    
end
if ~isempty(nomFicNav)% Récupération du fichier de Navigation.
    this.NavigationFile = nomFicNav;
else
    this.NavigationFile = FileHeader.NavigationFile{1};
end

%% Sortie

if nargout == 0
    assignin('caller', inputname(1), this);
else
    varargout{1} = this;
end
