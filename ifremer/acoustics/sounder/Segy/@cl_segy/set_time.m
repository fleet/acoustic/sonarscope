function flag = set_time(this, Time, varargin)

[varargin, DataTrace] = getPropertyValue(varargin, 'DataTrace', []); %#ok<ASGLU>

if isempty(DataTrace)
    [flag, DataTrace]  = read_TraceHeaders_bin(this, 'Memmapfile', 1);
    if ~flag
        return
    end
end

n = length(DataTrace.YearDataRecorded);
clear DataTrace % Pour �viter lockage fichier en memmapfile
flag = (length(Time.timeMat) == n); % Bug annonc� ici : datetime maintenant. A corriger quand le pb se posera
if ~flag
    str1 = 'Mise � jour de l''heure impossible dans cl_segy/set_time car les dimensions sont diff�rentes';
    str2 = 'TODO';
    my_warndlg(Lang(str1,str2), 1);
    return
end

[~, ~, Annee] = dayIfr2Jma(Time.date);
[Heure, Minute, Seconde] = hourIfr2Hms(Time.heure);

Un = ones(1, n);
YearDataRecorded = Annee;
DayOfYear        = floor(Time.date) + (1 - dayJma2Ifr(Un, Un, Annee));
HourOfDay        = Heure;
MinuteOfHour     = Minute;
SecondOfMinute   = Seconde;

flag = save_signal(this, 'YearDataRecorded', YearDataRecorded);
if ~flag
    return
end

flag = save_signal(this, 'DayOfYear', DayOfYear);
if ~flag
    return
end

flag = save_signal(this, 'HourOfDay', HourOfDay);
if ~flag
    return
end

flag = save_signal(this, 'MinuteOfHour', MinuteOfHour);
if ~flag
    return
end

flag = save_signal(this, 'SecondOfMinute', SecondOfMinute);
if ~flag
    return
end
