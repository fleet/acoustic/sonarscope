function flag = write_NavigationFile(this, nomFicNav)


%% Ecriture des signaux : nom du fichier de navigation
flag = save_string(this, 'Header', 'NavigationFile', nomFicNav);
if ~flag
    return
end

