function flag = write_position(this, DataPosition, varargin)

[varargin, DataTrace] = getPropertyValue(varargin, 'DataTrace', []);

if isempty(DataTrace)
    [flag, DataTrace]  = read_TraceHeaders_bin(this, 'Memmapfile', 1);
    if ~flag
        return
    end
end

[flag, DataTrace] = SourceLatLong2XY(this, DataTrace, DataPosition.Latitude, DataPosition.Longitude, varargin{:});
if ~flag
    return
end

%% Ecriture des signaux

DataTrace.cdpX = DataTrace.cdpX(:); % Pour �viter blockage fichier en memmapfile
flag = save_signal(this, 'cdpX', DataTrace.cdpX);
if ~flag
    return
end

DataTrace.cdpY = DataTrace.cdpY(:); % Pour �viter blockage fichier en memmapfile
flag = save_signal(this, 'cdpY', DataTrace.cdpY);
if ~flag
    return
end

DataTrace.GroupX = DataTrace.GroupX(:); % Pour �viter blockage fichier en memmapfile
flag = save_signal(this, 'GroupX', DataTrace.GroupX);
if ~flag
    return
end

DataTrace.GroupY = DataTrace.GroupY(:); % Pour �viter lockage fichier en memmapfile
flag = save_signal(this, 'GroupY', DataTrace.GroupY);
if ~flag
    return
end

if ~isempty(DataPosition.Longitude)
    DataTrace.SourceGroupScalar = DataTrace.SourceGroupScalar(:);
    flag = save_signal(this, 'SourceGroupScalar', DataTrace.SourceGroupScalar);
    if ~flag
        return
    end
    
    DataTrace.SourceX = DataTrace.SourceX(:); % Pour �viter lockage fichier en memmapfile
    flag = save_signal(this, 'SourceX', DataTrace.SourceX);
    if ~flag
        return
    end
    
    DataTrace.SourceY = DataTrace.SourceY(:); % Pour �viter lockage fichier en memmapfile
    flag = save_signal(this, 'SourceY', DataTrace.SourceY);
    if ~flag
        return
    end
end
