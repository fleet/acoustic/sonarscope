% Conversion du bloc sismique pour visualisation dans  SonarScope-3DViewer V1
% On fait comme si c'�taient des Water Column
%
% addpath('C:\Documents and Settings\augustin\My Documents\SEGY\S4M\Geophysics_3.0')
% CEau = 1478; % m/s
% NomFicSegY = 'F:\SonarScopeData\SEGY From JMA\LisiblePar_read_segy_file\cube_jma.seg'
%
% NomFicSegY = fullfile(PathName, nomFic);
% NomDirXML = my_tempdir
% flag = SEGYMAT2SSC3DViewerV1(NomFicSegY, NomDirXML, CEau)
% flag = SEGYMAT2SSC3DViewerV1(NomFicSegY, NomDirXML, CEau, 'BathyFile', 'C:\Demos\Marmara\marmara_50m_LatLong_Bathymetry.ers')

function flag = SEGYMAT2SSC3DViewerV1(NomFicSegY, NomDirXML, CEau, varargin)

[varargin, BathyFile] = getPropertyValue(varargin, 'BathyFile', []);
[varargin, TypeSignal] = getPropertyValue(varargin, 'TypeSignal', 1); %#ok<ASGLU>

if isempty(BathyFile)
    Bathy = [];
else
    [flag, Bathy] = cl_image.import_ermapper(BathyFile);
    if ~flag
        return
    end
end

if iscell(NomFicSegY)
    N = length(NomFicSegY);
    if N == 1
        flag = SEGYMAT2SSC3DViewerV11_unitaire(NomFicSegY{1}, NomDirXML, CEau, Bathy, TypeSignal);
    else
        [pathname, filename] = fileparts(NomDirXML);
        for k=1:N
            filename = sprintf('%s_%02d', filename, k);
            NomDirXML = fullfile(pathname, filename);
            flag = SEGYMAT2SSC3DViewerV11_unitaire(NomFicSegY, NomDirXML, CEau, Bathy, TypeSignal);
        end
    end
else
    flag = SEGYMAT2SSC3DViewerV11_unitaire(NomFicSegY, NomDirXML, CEau, Bathy, TypeSignal);
end


function flag = SEGYMAT2SSC3DViewerV11_unitaire(NomFicSegY, NomDirXML, CEau, TypeSignal)

str1 = 'Lecture du fichier SEGY en cours';
str2 = 'Reading SEGY file.';
WorkInProgress(Lang(str1,str2))

%% Toolbox S4M
[seismic, text_header, binary_header] = read_segy_file(NomFicSegY); %#ok<ASGLU>

%% Toolbox SEGYMAT
% Lecture des Data, TraceHeader et BinaryHeader

[Data,SegyTraceHeaders,SegyHeader] = ReadSegy(NomFicSegY);
if isempty(Data)
    return
end

sz = [SegyHeader.ns SegyHeader.DataTracePerEnsemble SegyHeader.Line];
V1 = reshape(Data, sz);

% % % % seismic.header_info
% % % %
% % % % sz = [binary_header(8) binary_header(1) binary_header(2)];
% % % % V1 = reshape(Data, sz);


Lon = convert_dms2ddec([SegyTraceHeaders(:).SourceX]);
Lat = convert_dms2ddec([SegyTraceHeaders(:).SourceY]);
Lon = Lon';
Lat = Lat';
% SonarScope(Lat)
% SonarScope(Lon)

%% Cr�ation des pseudo images verticales Dim 2

nbPings = sz(2);
WC.lonBab    =  NaN(1, nbPings);
WC.latBab    =  NaN(1, nbPings);
WC.lonCentre =  NaN(1, nbPings);
WC.latCentre =  NaN(1, nbPings);
WC.lonTri    =  NaN(1, nbPings);
WC.latTri    =  NaN(1, nbPings);
WC.iPing     =  NaN(1, nbPings);
WC.xBab      =  NaN(1, nbPings);
WC.xTri      =  NaN(1, nbPings);
WC.lonBabDepointe      =  NaN(1, nbPings);
WC.latBabDepointe      =  NaN(1, nbPings);
WC.lonTriDepointe      =  NaN(1, nbPings);
WC.latTriDepointe      =  NaN(1, nbPings);

TEnd = SegyHeader.dt*SegyHeader.ns / 1000000; % dt en us, Temps en s
TDeb = SegyHeader.dt*1 / 1000000; % dt en us, Temps en s
DepthEnd = -SegyTraceHeaders(end).WeatheringVelocity * TEnd / 2;
DepthDeb = -SegyTraceHeaders(1).WeatheringVelocity * TDeb / 2;

DepthEnd = DepthEnd + 140; % TODO : ATTENTION BIDOUILLE : on rajoute artificiellement 140 m
% pour s'adapter � la donn�e bathy : trouver l'origine du bug, CEau = 1478;
% % m/s

% % % % % n3s2 = floor(sz(3)/2); ??????

n3s2 = 1;


map = gray(256);
CLim = [-13000 13000];
[nomDir2, nomFic] = fileparts(NomDirXML);
nomDir2 = fullfile(nomDir2, nomFic);
if ~exist(nomDir2, 'dir')
    flag = mkdir(nomDir2);
    if ~flag
        messageErreurFichier(nomDir2, 'WriteFailure');
        return
    end
end

% Fig = figure;

Z0 = zeros(floor(sz(1) * TDeb / TEnd), sz(3), 'uint8');

%% D�tection du premier �cho et mise � 0 des �chantillons de la colonne d'eau

[nomDir2, nomFic2] = fileparts(NomFicSegY);
nomFicH = fullfile(nomDir2, [nomFic2 '_Height.mat']);
if exist(nomFicH, 'file')
    H = loadmat(nomFicH, 'nomVar', 'H');
else
    FiltreSonar = [2 0.1];
    [B,A] = butter(FiltreSonar(1), FiltreSonar(2));
    FiltreSonarA = A;
    FiltreSonarB = B;
    % % % % %     H = NaN(sz(2), sz(3), 'single');
    H = NaN(sz(3), sz(2), 'single');
    hw = create_waitbar('Searching Height on Bloc3D_dim2', 'N', nbPings);
    for k=1:nbPings
        my_waitbar(k, nbPings, hw)
        I = squeeze(V1(:,k,:));
        
        H(:,k) = subbottom_compute_height_image(I', TypeSignal, FiltreSonarA, FiltreSonarB);
    end
    my_close(hw, 'MsgEnd')
    
    %% Nettoyage de la courbe
    
    Title = ['Edit Height - ' NomFicSegY];
    xSample = XSample('name', 'Pings',  'data', 1:length(H));
    ySample = YSample('Name', 'Height', 'data', H);
    signal = ClSignal('Name', Title, 'xSample', xSample, 'ySample', ySample);
    s = SignalDialog(signal, 'Title', 'Edit signal', 'cleanInterpolation', 1);
    s.openDialog();
    if s.okPressedOut
        H = ySample.data;
    end
    %% Sauvegarde de la courbe
    
    save(nomFicH, 'H')
end

%{
pppp = SonarScope(H)
H = get(pppp(end), 'Image');
save(nomFicH, 'H')
%}

nomDir2 = fullfile(NomDirXML, 'Bloc3D_dim2');
hw = create_waitbar('Exporting Bloc3D_dim2', 'N', nbPings);
for k=1:nbPings
    my_waitbar(k, nbPings, hw)
    
    %%
    WC.lonBab(k)    =  Lon(k,1);
    WC.latBab(k)    =  Lat(k,1);
    
    WC.lonCentre(k) =  Lon(k,n3s2);
    WC.latCentre(k) =  Lat(k,n3s2);
    WC.lonTri(k)    =  Lon(k,end);
    WC.latTri(k)    =  Lat(k,end);
    WC.iPing(k)     =  k;
    
    xBab = NaN; % -12.5 * n3s2;
    xTri = NaN; %  12.5 * n3s2;
    WC.xBab(k)      =  xBab;
    WC.xTri(k)      =  xTri;
    WC.Depth(k)     =  DepthEnd;
    WC.Date(k)      =  0;
    WC.Hour(k)      =  0;
    WC.Immersion    =  0;
    
    WC.lonBabDepointe(k) =  NaN;
    WC.latBabDepointe(k) =  Lat(k,1);
    WC.lonTriDepointe(k) =  Lon(k,end);
    WC.latTriDepointe(k) =  Lat(k,end);
    
    if ~isempty(Bathy)
        val = NaN(1,size(Lon,2), 'single');
        for k2=1:size(Lon,2)
            val(k2) = get_val_xy(Bathy, Lon(k,k2), Lat(k,k2));
        end
    end
    
    I = squeeze(V1(:,k,:));
    I = gray2ind(mat2gray(I, double(CLim)), size(map,1));
    
    I(I == 0) = 1;
    for j=1:sz(3)
        % % % %         sub0 = 1:H(k,j);
        sub0 = 1:H(j,k);
        I(sub0,j) = 0;
    end
    I = [Z0; I]; %#ok<AGROW>
    
    %{
figure(5624); plot(val, '*-r'); grid on
figure(Fig);
imagesc(I); colormap(gray(256)); axis equal; axis tight;
imagesc([1 size(I,2)], [0 DepthEnd], I); colormap(gray(256)); axis xy
title(['Dim2 : slice ' num2str(k) '/' num2str(nbPings)])
hold on; PlotUtils.createSScPlot(val, '*-r'); hold off;
    %}
    
    if ~isempty(Bathy)
        for k2=1:size(Lon,2)
            kdeb = floor(abs(size(I,1) * val(k2) / DepthEnd));
            I(1:kdeb,k2) = NaN;
        end
    end
    
    if ~exist(nomDir2, 'dir')
        flag = mkdir(nomDir2);
        if ~flag
            messageErreurFichier(nomDir2, 'WriteFailure');
            return
        end
    end
    
    nomSousRepertoire = sprintf('%03d', floor(k/100));
    nomDir3 = fullfile(nomDir2, nomSousRepertoire);
    if ~exist(nomDir3, 'dir')
        flag = mkdir(nomDir3);
        if ~flag
            messageErreurFichier(nomDir3, 'WriteFailure');
            return
        end
    end
    
    nomFic1 = sprintf('%05d.png', k);
    nomFic2 = fullfile(nomDir3, nomFic1);
    imwrite(I, map, nomFic2, 'Transparency', 0)
end
my_close(hw, 'MsgEnd')

[~, NomWC] = fileparts(nomDir2);
WC.Name = NomWC;
WC.nomFicAll = '';
WC.nbColumns = sz(3);
WC.nbRows    = sz(1);
WC.RawDataType = 'uint8';
WC.nbPings = nbPings;

NomFicXml = [nomDir2 '.xml'];
Survey = [];
if isempty(Survey)
    Survey.Name           = 'Unkown';
    Survey.Vessel         = 'Unkown';
    Survey.Sounder        = 'Unkown';
    Survey.ChiefScientist = 'Unkown';
end

codeUnit = SegyTraceHeaders.TraceValueMeasurementUnit; 
flag = WC_writeXmlPng(NomFicXml, WC, Survey, codeUnit);
if ~flag
    return
end

%% Cr�ation des pseudo images verticales Dim 3

nbPings = sz(3);
WC.lonBab    =  NaN(1, nbPings);
WC.latBab    =  NaN(1, nbPings);
WC.lonCentre =  NaN(1, nbPings);
WC.latCentre =  NaN(1, nbPings);
WC.lonTri    =  NaN(1, nbPings);
WC.latTri    =  NaN(1, nbPings);
WC.iPing     =  NaN(1, nbPings);
WC.xBab      =  NaN(1, nbPings);
WC.xTri      =  NaN(1, nbPings);
WC.lonBabDepointe      =  NaN(1, nbPings);
WC.latBabDepointe      =  NaN(1, nbPings);
WC.lonTriDepointe      =  NaN(1, nbPings);
WC.latTriDepointe      =  NaN(1, nbPings);

n3s2 = floor(sz(2)/2);

map = gray(256);
CLim = [-13000 13000];
[nomDir2, nomFic] = fileparts(NomDirXML);
nomDir2 = fullfile(nomDir2, nomFic);
if ~exist(nomDir2, 'dir')
    flag = mkdir(nomDir2);
    if ~flag
        messageErreurFichier(nomDir2, 'WriteFailure');
        return
    end
end
nomDir2 = fullfile(NomDirXML, 'Bloc3D_dim3');

% Z0 = zeros(floor(sz(1)/4), sz(2), 'uint8');
Z0 = zeros(floor(sz(1) * TDeb / TEnd), sz(2), 'uint8');
hw = create_waitbar('Exporting Bloc3D_dim3', 'N', nbPings);
for k=1:nbPings
    my_waitbar(k, nbPings, hw)
    
    WC.lonBab(k)    =  Lon(1,k);
    WC.latBab(k)    =  Lat(1,k);
    
    WC.lonCentre(k) =  Lon(n3s2,k);
    WC.latCentre(k) =  Lat(n3s2,k);
    WC.lonTri(k)    =  Lon(end,k);
    WC.latTri(k)    =  Lat(end,k);
    WC.iPing(k)     =  k;
    
    xBab = NaN; % -12.5 * n3s2;
    xTri = NaN; %  12.5 * n3s2;
    WC.xBab(k)      =  xBab;
    WC.xTri(k)      =  xTri;
    WC.Depth(k)     =  DepthEnd;
    WC.Date(k)      =  0;
    WC.Hour(k)      =  0;
    
    WC.lonBabDepointe(k) =  NaN;
    WC.latBabDepointe(k) =  Lat(1,k);
    WC.lonTriDepointe(k) =  Lon(end,k);
    WC.latTriDepointe(k) =  Lat(end,k);
    
    if ~isempty(Bathy)
        val = NaN(1,size(Lon,1), 'single');
        for k2=1:size(Lon,1)
            val(k2) = get_val_xy(Bathy, Lon(k2,k), Lat(k2,k));
        end
    end
    
    %%
    
    I = squeeze(V1(:,:,k));
    I = gray2ind(mat2gray(I, double(CLim)), size(map,1));
    %     J = diff(I);
    
    %     figure(Fig); imagesc(I); colormap(gray(256)); axis equal; axis tight;
    %     title(['Dim3 : slice ' num2str(k) '/' num2str(nbPings)])
    
    I(I == 0) = 1;
    
    for j=1:sz(2)
        % % % %         sub0 = 1:H(j,k);
        sub0 = 1:H(k,j);
        I(sub0,j) = 0;
    end
    I = [Z0; I]; %#ok<AGROW>
    
    %{
figure(5624); plot(val, '*-r'); grid on
figure(Fig);
imagesc(I); colormap(gray(256)); axis equal; axis tight;
imagesc([1 size(I,2)], [0 DepthEnd], I); colormap(gray(256)); axis xy
title(['Dim2 : slice ' num2str(k) '/' num2str(nbPings)])
hold on; PlotUtils.createSScPlot(val, '*-r'); hold off;
    %}
    
    if ~isempty(Bathy)
        for k2=1:size(Lon,1)
            kdeb = floor(abs(size(I,1) * val(k2) / DepthEnd));
            I(1:kdeb,k2) = NaN;
        end
    end
    
    if ~exist(nomDir2, 'dir')
        flag = mkdir(nomDir2);
        if ~flag
            messageErreurFichier(nomDir2, 'WriteFailure');
            return
        end
    end
    
    nomSousRepertoire = sprintf('%03d', floor(k/100));
    nomDir3 = fullfile(nomDir2, nomSousRepertoire);
    if ~exist(nomDir3, 'dir')
        flag = mkdir(nomDir3);
        if ~flag
            messageErreurFichier(nomDir3, 'WriteFailure');
            return
        end
    end
    
    nomFic1 = sprintf('%05d.png', k);
    nomFic2 = fullfile(nomDir3, nomFic1);
    imwrite(I, map, nomFic2, 'Transparency', 0)
    
end
my_close(hw, 'MsgEnd')

[~, NomWC] = fileparts(nomDir2);
WC.Name = NomWC;
WC.nomFicAll = '';
WC.nbColumns = sz(2);
WC.nbRows    = sz(1);
WC.RawDataType = 'uint8';
WC.nbPings = nbPings;

NomFicXml = [nomDir2 '.xml'];
Survey = [];
if isempty(Survey)
    Survey.Name           = 'Unkown';
    Survey.Vessel         = 'Unkown';
    Survey.Sounder        = 'Unkown';
    Survey.ChiefScientist = 'Unkown';
end

flag = WC_writeXmlPng(NomFicXml, WC, Survey, codeUnit);

%     my_close(Fig);

