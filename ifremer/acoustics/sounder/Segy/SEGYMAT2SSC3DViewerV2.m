% NomFicSegY = 'F:\SonarScopeData\SEGY From JMA\LisiblePar_read_segy\AUV0001_D20091123_T050935_process.seg';
% NomFicSegY = 'F:\SonarScopeData\SEGY From JMA\LisiblePar_read_segy_file\cube_jma.seg'
% [path, name, ~, ~] = fileparts(NomFicSegY);
% NomFicXML = fullfile(path, [name '.xml']);
% flag = SEGYMAT2SSC3DViewerV2(NomFicSegY, NomFicXML)

function flag = SEGYMAT2SSC3DViewerV2(NomFicSegY, NomFicXML)

[varargin, TypeSignal] = getPropertyValue(varargin, 'TypeSignal', 1); %#ok<ASGLU>

CLim = [];
map  = [];

if iscell(NomFicSegY)
    N = length(NomFicSegY);
    if N == 1
        flag = SEGYMAT2SSC3DViewerV2_unitaire(NomFicSegY{1}, NomFicXML, CLim, map, TypeSignal);
    else
        [pathname, filename] = fileparts(NomFicXML);
        for k=1:N
            filename = sprintf('%s_%02d', filename, k);
            NomFicXML = fullfile(pathname, filename);
            [flag, CLim, map] = SEGYMAT2SSC3DViewerV2_unitaire(NomFicSegY, NomFicXML, CLim, map, TypeSignal);
        end
    end
else
    flag = SEGYMAT2SSC3DViewerV2_unitaire(NomFicSegY, NomFicXML, CLim, map, TypeSignal);
end

function [flag, CLim, map] = SEGYMAT2SSC3DViewerV2_unitaire(NomFicSegY, NomFicXML, CLim, map, TypeSignal)


% [flag, seismic] = read_segy(NomFicSegY);
% [flag, seismic] = read_segyOLD(NomFicSegY);
% % % % profile on
[Data,SegyTraceHeaders,SegyHeader] = ReadSegy(NomFicSegY);
% % % % profile report

[~, name] = fileparts(NomFicSegY);
seismic.name            = name; % Seismic.name
seismic.from            = mfilename; % Seismic.from
seismic.NbPings         = size(SegyTraceHeaders,2);
seismic.PingNumber      = [SegyTraceHeaders(:).TraceSequenceLine];
seismic.TxLat           = convert_dms2ddec([SegyTraceHeaders(:).SourceX]);
seismic.TxLon           = convert_dms2ddec([SegyTraceHeaders(:).SourceY]);
seismic.Year            = [SegyTraceHeaders(:).YearDataRecorded];
seismic.Day             = [SegyTraceHeaders(:).DayOfYear];
seismic.Time            = [SegyTraceHeaders(:).HourOfDay]*3600 + ...
    [SegyTraceHeaders(:).MinuteOfHour]*60 + ...
    [SegyTraceHeaders(:).SecondOfMinute];


Year   = [SegyTraceHeaders.YearDataRecorded]'; % Seismic.?????
Day    = [SegyTraceHeaders.DayOfYear]'; % Seismic.?????
Hour   = [SegyTraceHeaders.HourOfDay]'; % Seismic.?????
Minute = [SegyTraceHeaders.MinuteOfHour]'; % Seismic.?????
Second = [SegyTraceHeaders.SecondOfMinute]'; % Seismic.?????

seismic.Date        = dayJma2Ifr(1, 1, Year) + (Day-1); % TODO : v�rifier si Year est un tableau alors passer ones(size(Year) ayu lieu de 1
seismic.Hour        = hourHms2Ifr(Hour, Minute, Second);
seismic.PingNumber  = [SegyTraceHeaders.TraceSequenceLine]';

seismic.LongitudeNadir = convert_dms2ddec([SegyTraceHeaders.SourceX]');
seismic.LatitudeNadir  = convert_dms2ddec([SegyTraceHeaders.SourceY]');

seismic.LatitudeTop    = seismic.LatitudeNadir;
seismic.LongitudeTop   = seismic.LongitudeNadir;

seismic.TimeStartRecording = [SegyTraceHeaders.DelayRecordingTime]' / 1000;


seismic.DepthTop = [SegyTraceHeaders.SourceSurfaceElevation]' ./ [SegyTraceHeaders.SourceGroupScalar]';
C = [SegyTraceHeaders.SubWeatheringVelocity]';
% Recherche de la fr�quence d'acquistion
% % TextualFileHeader = char(SegyHeader.TextualFileHeader)';
% % k = strfind(TextualFileHeader, 'ACQUISITION FREQUENCY :') + 24;
% % fe = str2double(TextualFileHeader(k:k+4));
fe = 1/(SegyHeader.dt*1e-6); % Freq = 1/intervalle d'acquisition en us.
seismic.DepthBottom = seismic.DepthTop - C .* [SegyTraceHeaders.ns]' ./ (2 * fe);

if isempty(Data)
    return
end

% % % % % Data.Reflectivity = seismic.traces;
Data.Reflectivity = Data;


if isempty(CLim)
    CLim = [min(Data.Reflectivity(:)) max(Data.Reflectivity(:))];
    map  = gray(256);
    map = flipud(map);
    str1 = sprintf('%s\n%s', 'Voulez-vous redefinir le contraste ?', 'Si oui, une fen�tre SonarScope va s''ouvrir afin que vous r�gleriez le contraste. N''oubliez pas de faire "Save" avant de quitter. Le m�me rehaussement de contraste sera utilis� pour toutes les images.');
    str2 = sprintf('%s\n%s', 'Do you want to redefine the contrast ?', 'If you answer yes, a SonarScope window will open, please fix the contrast and click on "Save" before "Quit". The save contrast will be used for all the images.');
    [rep, validation] = my_questdlg(Lang(str1,str2), 'Init', 1);
    if validation == 1
        if rep == 1
            a = cl_image('Image', Data.Reflectivity, 'ColormapIndex', 2, 'Video', 2);
            a = SonarScope(a);
            CLim  = a.CLim;
            map   = a.Colormap;
            Video = a.Video;
            if Video == 2
                map = flipud(map);
            end
        end
    end
end

nbPings = size(Data.Reflectivity,2);
nbRows  = size(Data.Reflectivity,1);

% map = gray(256);
% CLim = [0 0.05];

%% Lecture de la hauteur si elle existe

[flagHeight, DataComplementaire] = readHeightSegyInCache(NomFicSegY);
if flagHeight
    Height = DataComplementaire.Height;
    
else
    % D�tection du premier �cho et mise � 0 des �chantillons de la colonne d'eau
    % GLUGLUGLU le 02/08/11 : � tester  !!!!
    [nomDir2, nomFic2] = fileparts(NomFicSegY);
    nomFicH = fullfile(nomDir2, [nomFic2 '_Height.mat']);
    if exist(nomFicH, 'file')
        Height = loadmat(nomFicH, 'nomVar', 'H');
    else
        FiltreSonar = [2 0.1];
        [B,A] = butter(FiltreSonar(1), FiltreSonar(2));
        FiltreSonarA = A;
        FiltreSonarB = B;
        Height = NaN(sz(3), sz(2), 'single');
        hw = create_waitbar('Searching Height on Bloc3D_dim2', 'N', nbPings);
        for k=1:nbPings
            my_waitbar(k, nbPings, hw)
            I = Data.Reflectivity;
            Height(:,k) = subbottom_compute_height_image(I', TypeSignal, FiltreSonarA, FiltreSonarB);
        end
        my_close(hw, 'MsgEnd')
        
        
        %% Nettoyage de la courbe
        
        Title = ['Edit Height - ' NomFicSegY];
        xSample = XSample('name', 'Pings',  'data', 1:length(Height));
        ySample = YSample('Name', 'Height', 'data', Height);
        signal = ClSignal('Name', Title, 'xSample', xSample, 'ySample', ySample);
        s = SignalDialog(signal, 'Title', 'Edit signal', 'cleanInterpolation', 1);
        s.openDialog();
        if s.okPressedOut
            Height = ySample.data;
        end
        
        %% Sauvegarde de la courbe
       
        save(nomFicH, 'Height')
    end
    
end

%% Cr�ation du r�pertoire

[nomDirXml, nomDirBin] = fileparts(NomFicXML);
nomDirXml = fullfile(nomDirXml, nomDirBin);
nomDirRoot = fileparts(nomDirXml);
if ~exist(nomDirXml, 'dir')
    flag = mkdir(nomDirXml);
    if ~flag
        messageErreurFichier(nomDirXml, 'WriteFailure');
        return
    end
end

%% Cr�ation de l'image PNG

subNaN = isnan(Data.Reflectivity);
I = gray2ind(mat2gray(Data.Reflectivity, double(CLim)), size(map,1));
I(I == 0) = 1;
I(subNaN) = 0;
% I = flipud(I); % A confirmer

if flagHeight
    for k=1:length(Height)
        subWC = 1:(floor(Height(k)));
        I(subWC,k) = NaN;
    end
end

nomFic1 = '00000.png';
nomFic2 = fullfile(nomDirXml, nomFic1);
imwrite(I, map, nomFic2, 'Transparency', 0)

%% Definition of the structure

Info.Signature1      = 'SonarScope';
Info.Signature2      = 'SeismicAlongNav';
Info.FormatVersion   = 20100624;
Info.Name            = seismic.name;
Info.ExtractedFrom   = seismic.from;

Info.Survey         = 'Unknown';
Info.Vessel         = 'Unknown';
Info.Sounder        = 'Unknown';
Info.ChiefScientist = 'Unknown';


Info.Dimensions.nbPings  = nbPings;
Info.Dimensions.nbSlices = 1;
Info.Dimensions.nbRows   = nbRows;


Data.AcrossDistance =  0;
Data.SliceExists    = true;
Data.Date           = seismic.Date;
Data.Hour           = seismic.Hour;
Data.PingNumber     = seismic.PingNumber;
Data.LatitudeNadir  = seismic.LatitudeNadir;
Data.LongitudeNadir = seismic.LongitudeNadir;
Data.LatitudeTop    = seismic.LatitudeTop;
Data.LongitudeTop   = seismic.LongitudeTop;
Data.DepthTop       = seismic.DepthTop;
Data.DepthBottom    = seismic.DepthBottom;


Info.Signals(1).Name          = 'Date';
Info.Signals(end).Dimensions  = '1, nbPings';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'Ifremer';
Info.Signals(end).FileName    = fullfile(nomDirBin,'Date.bin');

Info.Signals(end+1).Name      = 'Hour';
Info.Signals(end).Dimensions  = '1, nbPings';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'nb milliseconds since midnight';
Info.Signals(end).FileName    = fullfile(nomDirBin,'Hour.bin');

Info.Signals(end+1).Name      = 'PingNumber';
Info.Signals(end).Dimensions  = '1, nbPings';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = ' ';
Info.Signals(end).FileName    = fullfile(nomDirBin,'PingNumber.bin');

Info.Signals(end+1).Name      = 'LatitudeNadir';
Info.Signals(end).Dimensions  = '1, nbPings';
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile(nomDirBin, 'LatitudeNadir.bin');

Info.Signals(end+1).Name      = 'LongitudeNadir';
Info.Signals(end).Dimensions  = '1, nbPings';
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile(nomDirBin, 'LongitudeNadir.bin');

Info.Signals(end+1).Name      = 'SliceExists';
Info.Signals(end).Dimensions  = 'nbSlices, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile(nomDirBin,'SliceExists.bin');

Info.Signals(end+1).Name      = 'DepthTop';
Info.Signals(end).Dimensions  = '1, nbPings';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'm';
Info.Signals(end).FileName    = fullfile(nomDirBin, 'DepthTop.bin');

Info.Signals(end+1).Name      = 'DepthBottom';
Info.Signals(end).Dimensions  = '1, nbPings';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'm';
Info.Signals(end).FileName    = fullfile(nomDirBin, 'DepthBottom.bin');

Info.Signals(end+1).Name      = 'AcrossDistance';
Info.Signals(end).Dimensions  = 'nbSlices, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'm';
Info.Signals(end).FileName    = fullfile(nomDirBin, 'AcrossDistance.bin');


Info.Images(1).Name           = 'LatitudeTop';
Info.Images(end).Dimensions   = 'nbSlices, nbPings';
Info.Images(end).Storage      = 'double';
Info.Images(end).Unit         = 'deg';
Info.Images(end).FileName     = fullfile(nomDirBin, 'LatitudeTop.bin');

Info.Images(end+1).Name       = 'LongitudeTop';
Info.Images(end).Dimensions   = 'nbSlices, nbPings';
Info.Images(end).Storage      = 'double';
Info.Images(end).Unit         = 'deg';
Info.Images(end).FileName     = fullfile(nomDirBin,'LongitudeTop.bin');

% Info.Images(end+1).Name       = 'Reflectivity';
% Info.Images(end).Dimensions   = 'nbRows, nbPings, nbSlices';
% Info.Images(end).Storage      = 'single';
% Info.Images(end).Unit         = 'dB';
% Info.Images(end).FileName     = fullfile(nomDirBin,'Reflectivity.bin');


%% Cr�ation du fichier XML d�crivant la donn�e

% NomFicXML = fullfile(nomDirRacine, [TypeDataVoxler '.xml']);
xml_write(NomFicXML, Info);
flag = exist(NomFicXML, 'file');
if ~flag
    messageErreur(NomFicXML)
    return
end

%% Cr�ation du r�pertoire Info

nomDirInfo = nomDirXml;
if ~exist(nomDirInfo, 'dir')
    status = mkdir(nomDirInfo);
    if ~status
        messageErreur(nomDirInfo)
        flag = 0;
        return
    end
end

%% Cr�ation des fichiers binaires des signaux

for i=1:length(Info.Signals)
    flag = writeSignal(nomDirRoot, Info.Signals(i), Data.(Info.Signals(i).Name));
    if ~flag
        return
    end
end

%% Cr�ation des fichiers binaires des signaux

for i=1:length(Info.Images)
    flag = writeImage(nomDirRoot, Info.Images(i), Data.(Info.Images(i).Name));
    if ~flag
        return
    end
end

flag = 1;




