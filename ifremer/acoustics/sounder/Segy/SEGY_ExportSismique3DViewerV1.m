% Conversion du bloc sismique pour visualisation dans  SonarScope-3DViewer V1
% On fait comme si c'�taient des Water Column
%
% addpath('C:\Documents and Settings\augustin\My Documents\SEGY\S4M\Geophysics_3.0')
% CEau = 1478; % m/s
% NomFicSegY = 'C:\Temp\Marmesonet\Cube_Marmara\hr3d\Study\Marmara\cube\cube_jma.seg';
% NomDirXML = my_tempdir
% flag = SEGY_ExportSismique3DViewerV1(NomFicSegY, NomDirXML, CEau)
% flag = SEGY_ExportSismique3DViewerV1(NomFicSegY, NomDirXML, CEau, 'BathyFile', 'C:\Demos\Marmara\marmara_50m_LatLong_Bathymetry.ers')

function flag = SEGY_ExportSismique3DViewerV1(NomFicSegY, NomDirXML, CEau, varargin)

[varargin, BathyFile]  = getPropertyValue(varargin, 'BathyFile', []);
[varargin, TypeSignal] = getPropertyValue(varargin, 'TypeSignal', 1); %#ok<ASGLU>

if isempty(BathyFile)
    Bathy = [];
else
    [flag, Bathy] = cl_image.import_ermapper(BathyFile);
    if ~flag
        return
    end
end

if iscell(NomFicSegY)
    N = length(NomFicSegY);
    if N == 1
        flag = SEGY_ExportSismique3DViewerV1_unitaire(NomFicSegY{1}, NomDirXML, CEau, Bathy, TypeSignal);
    else
        [pathname, filename] = fileparts(NomDirXML);
        for k=1:N
            filename = sprintf('%s_%02d', filename, k);
            NomDirXML = fullfile(pathname, filename);
            flag = SEGY_ExportSismique3DViewerV1_unitaire(NomFicSegY, NomDirXML, CEau, Bathy, TypeSignal);
        end
    end
else
    flag = SEGY_ExportSismique3DViewerV1_unitaire(NomFicSegY, NomDirXML, CEau, Bathy, TypeSignal);
end

function flag = SEGY_ExportSismique3DViewerV1_unitaire(NomFicSegY, NomDirXML, CEau, Bathy, TypeSignal)

str1 = 'Lecture du fichier SEGY en cours';
str2 = 'Reading SEGY file.';
WorkInProgress(Lang(str1,str2))

[seismic, text_header, binary_header] = read_segy_file(NomFicSegY); %#ok<ASGLU>

%{
% Tentative de remplacement de la lecture read_segy_file par la fonction de
% lecture de Anne
% Comment� car tr�s tr�s tr�s long
[flag, Sub] = read_segy(NomFicSegY);
if ~flag
return
end
%}

seismic.header_info

sz = [binary_header(8) binary_header(1) binary_header(2)];
V1 = reshape(seismic.traces, sz);

% Test pour �vider la partie sup�rieure : pas terrible. Vaut mieux prendre
% la bathy
% FirstEcho = NaN(sz(2:3), 'single');
% for i1=1:size(V1,2)
%     i1
%     for i2=1:size(V1,3)
%         Signal = squeeze(V1(:,i1,i2));
%         H = find(abs(Signal) > (max(abs(Signal))/5));
%         if ~isempty(H)
%             H = H(1);
%             %         figure(67857); plot((Signal)); grid on;
%             %         hold on; plot([H H], [0 max(Signal)], 'r'); hold off; drawnow;
%             %         pause(0.5)
%             FirstEcho(i1,i2) = H;
%         end
%     end
% end
% SonarScope(FirstEcho)
%
% FirstEcho = loadmat('C:\Temp\Marmesonet\Cube_Marmara\hr3d\Study\Marmara\cube\FirstEcho.mat', 'nomVar', 'Image');
% FirstEcho(isnan(FirstEcho)) = 0;
% for i1=1:size(V1,2)
%     i1
%     for i2=1:size(V1,3)
%         V1(1:FirstEcho(i1,i2),i1,i2) = NaN;
%     end
% end

%% Recherche des coordonn�es

% pppp = ds_header(seismic);

%{
ds_header(seismic)
Headers of data set "cube_jma - shifted":
MNEMONIC MIN_VAL   MAX_VAL   MIN_STEP  MAX_STEP UNITS   DESCRIPTION
cdp            70       542     -472       2    n/a     CDP number
seq_cdp        12      1746        0       2    n/a     Trace sequence number within CDP ensemble
sou_x          70       542     -472       2    unknown X coordinate of source
sou_y          12      1746        0       2    unknown Y coordinate of source
rec_x       43438    338438  -295000    1250    unknown X coordinate of receiver
rec_y        7188   1090938        0    1250    unknown Y coordinate of receiver
cdp_x    27414457  27494063    -5809    4008    n/a     X-coordinate of CDP
cdp_y    40473210  40495865    -4041   17468    n/a     Y-coordinate of CDP
trc_type        1         1        0       0    n/a     Trace type (1=live,2=dead,3=dummy,...)
lag           500       500        0       0    ms      Lag time between shot and recording start
%}

for k=1:size(seismic.header_info, 1)
    flag = strfind(seismic.header_info{k,1}, 'cdp_x');
    if flag
        idx(1) = k;
    end
    flag = strfind(seismic.header_info{k,1}, 'cdp_y');
    if flag
        idx(2) = k;
    end
    flag = validatestring('lag', seismic.header_info{k,1});
    if flag
        idxLag = k;
    end
    flag = validatestring('codeUnit', seismic.header_info{k,1});
    if flag
        idxMeasUnit = k;
    end
end

C = seismic.headers(idx,:);
Deg = floor(C / 1e6);
C = C - Deg * 1e6;
Minute = floor(C / 1e4);
C = C - Minute * 1e4;
Second = C / 100;
Coor = Deg + Minute/60 + Second/3600;
Lat = reshape(Coor(2,:), sz(2:3));
Lon = reshape(Coor(1,:), sz(2:3));
% SonarScope(Lat)
% SonarScope(Lon)

lag = seismic.headers(idxLag,:);
lag = lag(1);

codeUnit = seismic.headers(idxMeasUnit,:);
codeUnit = codeUnit(1);

%% Cr�ation des pseudo images verticales Dim 2

nbPings = sz(2);
WC.lonBab    =  NaN(1, nbPings);
WC.latBab    =  NaN(1, nbPings);
WC.lonCentre =  NaN(1, nbPings);
WC.latCentre =  NaN(1, nbPings);
WC.lonTri    =  NaN(1, nbPings);
WC.latTri    =  NaN(1, nbPings);
WC.iPing     =  NaN(1, nbPings);
WC.xBab      =  NaN(1, nbPings);
WC.xTri      =  NaN(1, nbPings);
WC.lonBabDepointe      =  NaN(1, nbPings);
WC.latBabDepointe      =  NaN(1, nbPings);
WC.lonTriDepointe      =  NaN(1, nbPings);
WC.latTriDepointe      =  NaN(1, nbPings);

TEnd = binary_header(6) / 1000; % Temps en s
TDeb = binary_header(8) / 1000; % Temps en s
DepthEnd = -CEau * TEnd / 2;
DepthDeb = -CEau * TDeb / 2;

DepthEnd = DepthEnd + 140; % TODO : ATTENTION BIDOUILLE : on rajoute artificiellement 140 m
% pour s'adapter � la donn�e bathy : trouver l'origine du bug, CEau = 1478;
% % m/s

n3s2 = floor(sz(3)/2);


map = gray(256);
CLim = [-13000 13000];
[nomDir2, nomFic] = fileparts(NomDirXML);
nomDir2 = fullfile(nomDir2, nomFic);
if ~exist(nomDir2, 'dir')
    flag = mkdir(nomDir2);
    if ~flag
        messageErreurFichier(nomDir2, 'WriteFailure');
        return
    end
end

% Fig = figure;

Z0 = zeros(floor(sz(1) * TDeb / TEnd), sz(3), 'uint8');

%% D�tection du premier �cho et mise � 0 des �chantillons de la colonne d'eau

[nomDir2, nomFic2] = fileparts(NomFicSegY);
nomFicH = fullfile(nomDir2, [nomFic2 '_Height.mat']);
if exist(nomFicH, 'file')
    H = loadmat(nomFicH, 'nomVar', 'H');
else
    FiltreSonar = [2 0.1];
    [B,A] = butter(FiltreSonar(1), FiltreSonar(2));
    FiltreSonarA = A;
    FiltreSonarB = B;
    H = NaN(sz(2), sz(3), 'single');
    hw = create_waitbar('Searching Height on Bloc3D_dim2', 'N', nbPings);
    for k=1:nbPings
        my_waitbar(k, nbPings, hw)
        I = squeeze(V1(:,k,:));
        H(k,:) = subbottom_compute_height_image(I', TypeSignal, FiltreSonarA, FiltreSonarB);
    end
    my_close(hw, 'MsgEnd')
    
    hw = create_waitbar('Manual cleaning Height on Bloc3D_dim2', 'N', nbPings);
    for k=1:nbPings
        my_waitbar(k, nbPings, hw)
        
        Title = ['Edit Height - ' nomFic];
        xSample = XSample('name', 'Pings', 'data', 1:size(H,2));
        ySample = YSample('Name', 'Height', 'data', H(k,:));
        signal = ClSignal('Name', Title, 'xSample', xSample, 'ySample', ySample);
        s = SignalDialog(signal, 'Title', 'Edit signal', 'cleanInterpolation', 1);
        s.openDialog();
        if s.okPressedOut
            H(k,:) = ySample.data;
        end
    end
    my_close(hw)
    save(nomFicH, 'H')
end
%{
pppp = SonarScope(H)
H = get(pppp(end), 'Image');
save(nomFicH, 'H')
%}

nomDir2 = fullfile(NomDirXML, 'Bloc3D_dim2');
hw = create_waitbar('Exporting Bloc3D_dim2', 'N', nbPings);
for k=1:nbPings
    my_waitbar(k, nbPings, hw)
    
    %%
    WC.lonBab(k)    =  Lon(k,1);
    WC.latBab(k)    =  Lat(k,1);
    
    WC.lonCentre(k) =  Lon(k,n3s2);
    WC.latCentre(k) =  Lat(k,n3s2);
    WC.lonTri(k)    =  Lon(k,end);
    WC.latTri(k)    =  Lat(k,end);
    WC.iPing(k)     =  k;
    
    xBab = NaN; % -12.5 * n3s2;
    xTri = NaN; %  12.5 * n3s2;
    WC.xBab(k)      =  xBab;
    WC.xTri(k)      =  xTri;
    WC.Depth(k)     =  DepthEnd;
    WC.Date(k)      =  0;
    WC.Hour(k)      =  0;
    WC.Immersion    =  0;
    
    WC.lonBabDepointe(k) =  NaN;
    WC.latBabDepointe(k) =  Lat(k,1);
    WC.lonTriDepointe(k) =  Lon(k,end);
    WC.latTriDepointe(k) =  Lat(k,end);
    
    if ~isempty(Bathy)
        val = NaN(1,size(Lon,2), 'single');
        for k2=1:size(Lon,2)
            val(k2) = get_val_xy(Bathy, Lon(k,k2), Lat(k,k2));
        end
    end
    
    I = squeeze(V1(:,k,:));
    I = gray2ind(mat2gray(I, double(CLim)), size(map,1));
    
    I(I == 0) = 1;
    for j=1:sz(3)
        sub0 = 1:H(k,j);
        I(sub0,j) = 0;
    end
    I = [Z0; I]; %#ok<AGROW>
    
    %{
figure(5624); plot(val, '*-r'); grid on
figure(Fig);
imagesc(I); colormap(gray(256)); axis equal; axis tight;
imagesc([1 size(I,2)], [0 DepthEnd], I); colormap(gray(256)); axis xy
title(['Dim2 : slice ' num2str(k) '/' num2str(nbPings)])
hold on; PlotUtils.createSScPlot(val, '*-r'); hold off;
    %}
    
    
    if ~isempty(Bathy)
        for k2=1:size(Lon,2)
            kdeb = floor(abs(size(I,1) * val(k2) / DepthEnd));
            I(1:kdeb,k2) = NaN;
        end
    end
    
    if ~exist(nomDir2, 'dir')
        flag = mkdir(nomDir2);
        if ~flag
            messageErreurFichier(nomDir2, 'WriteFailure');
            return
        end
    end
    
    nomSousRepertoire = sprintf('%03d', floor(k/100));
    nomDir3 = fullfile(nomDir2, nomSousRepertoire);
    if ~exist(nomDir3, 'dir')
        flag = mkdir(nomDir3);
        if ~flag
            messageErreurFichier(nomDir3, 'WriteFailure');
            return
        end
    end
    
    nomFic1 = sprintf('%05d.png', k);
    nomFic2 = fullfile(nomDir3, nomFic1);
    imwrite(I, map, nomFic2, 'Transparency', 0)
    
    
end
my_close(hw, 'MsgEnd')

[~, NomWC] = fileparts(nomDir2);
WC.Name = NomWC;
WC.nomFicAll = '';
WC.nbColumns = sz(3);
WC.nbRows    = sz(1);
WC.RawDataType = 'uint8';
WC.nbPings = nbPings;

NomFicXml = [nomDir2 '.xml'];
Survey = [];
if isempty(Survey)
    Survey.Name           = 'Unkown';
    Survey.Vessel         = 'Unkown';
    Survey.Sounder        = 'Unkown';
    Survey.ChiefScientist = 'Unkown';
end

flag = WC_writeXmlPng(NomFicXml, WC, Survey, Unit);
if ~flag
    return
end






%% Cr�ation des pseudo images verticales Dim 3

nbPings = sz(3);
WC.lonBab    =  NaN(1, nbPings);
WC.latBab    =  NaN(1, nbPings);
WC.lonCentre =  NaN(1, nbPings);
WC.latCentre =  NaN(1, nbPings);
WC.lonTri    =  NaN(1, nbPings);
WC.latTri    =  NaN(1, nbPings);
WC.iPing     =  NaN(1, nbPings);
WC.xBab      =  NaN(1, nbPings);
WC.xTri      =  NaN(1, nbPings);
WC.lonBabDepointe      =  NaN(1, nbPings);
WC.latBabDepointe      =  NaN(1, nbPings);
WC.lonTriDepointe      =  NaN(1, nbPings);
WC.latTriDepointe      =  NaN(1, nbPings);

n3s2 = floor(sz(2)/2);


map = gray(256);
CLim = [-13000 13000];
[nomDir2, nomFic] = fileparts(NomDirXML);
nomDir2 = fullfile(nomDir2, nomFic);
if ~exist(nomDir2, 'dir')
    flag = mkdir(nomDir2);
    if ~flag
        messageErreurFichier(nomDir2, 'WriteFailure');
        return
    end
end
nomDir2 = fullfile(NomDirXML, 'Bloc3D_dim3');

% Z0 = zeros(floor(sz(1)/4), sz(2), 'uint8');
hw = create_waitbar('Exporting Bloc3D_dim3', 'N', nbPings);
Z0 = zeros(floor(sz(1) * TDeb / TEnd), sz(2), 'uint8');
for k=1:nbPings
    my_waitbar(k, nbPings, hw)
    
    
    %%
    
    WC.lonBab(k)    =  Lon(1,k);
    WC.latBab(k)    =  Lat(1,k);
    
    WC.lonCentre(k) =  Lon(n3s2,k);
    WC.latCentre(k) =  Lat(n3s2,k);
    WC.lonTri(k)    =  Lon(end,k);
    WC.latTri(k)    =  Lat(end,k);
    WC.iPing(k)     =  k;
    
    xBab = NaN; % -12.5 * n3s2;
    xTri = NaN; %  12.5 * n3s2;
    WC.xBab(k)      =  xBab;
    WC.xTri(k)      =  xTri;
    WC.Depth(k)     =  DepthEnd;
    WC.Date(k)      =  0;
    WC.Hour(k)      =  0;
    
    WC.lonBabDepointe(k) =  NaN;
    WC.latBabDepointe(k) =  Lat(1,k);
    WC.lonTriDepointe(k) =  Lon(end,k);
    WC.latTriDepointe(k) =  Lat(end,k);
    
    if ~isempty(Bathy)
        val = NaN(1,size(Lon,1), 'single');
        for k2=1:size(Lon,1)
            val(k2) = get_val_xy(Bathy, Lon(k2,k), Lat(k2,k));
        end
    end
    
    %%
    
    I = squeeze(V1(:,:,k));
    I = gray2ind(mat2gray(I, double(CLim)), size(map,1));
    %     J = diff(I);
    
    %     figure(Fig); imagesc(I); colormap(gray(256)); axis equal; axis tight;
    %     title(['Dim3 : slice ' num2str(k) '/' num2str(nbPings)])
    
    I(I == 0) = 1;
    
    for j=1:sz(2)
        sub0 = 1:H(j,k);
        I(sub0,j) = 0;
    end
    I = [Z0; I]; %#ok<AGROW>
    
    %{
figure(5624); plot(val, '*-r'); grid on
figure(Fig);
imagesc(I); colormap(gray(256)); axis equal; axis tight;
imagesc([1 size(I,2)], [0 DepthEnd], I); colormap(gray(256)); axis xy
title(['Dim2 : slice ' num2str(k) '/' num2str(nbPings)])
hold on; PlotUtils.createSScPlot(val, '*-r'); hold off;
    %}
    
    if ~isempty(Bathy)
        for k2=1:size(Lon,1)
            kdeb = floor(abs(size(I,1) * val(k2) / DepthEnd));
            I(1:kdeb,k2) = NaN;
        end
    end
    
    if ~exist(nomDir2, 'dir')
        flag = mkdir(nomDir2);
        if ~flag
            messageErreurFichier(nomDir2, 'WriteFailure');
            return
        end
    end
    
    nomSousRepertoire = sprintf('%03d', floor(k/100));
    nomDir3 = fullfile(nomDir2, nomSousRepertoire);
    if ~exist(nomDir3, 'dir')
        flag = mkdir(nomDir3);
        if ~flag
            messageErreurFichier(nomDir3, 'WriteFailure');
            return
        end
    end
    
    nomFic1 = sprintf('%05d.png', k);
    nomFic2 = fullfile(nomDir3, nomFic1);
    imwrite(I, map, nomFic2, 'Transparency', 0)
    
end
my_close(hw, 'MsgEnd')

[~, NomWC] = fileparts(nomDir2);
WC.Name        = NomWC;
WC.nomFicAll   = '';
WC.nbColumns   = sz(2);
WC.nbRows      = sz(1);
WC.RawDataType = 'uint8';
WC.nbPings     = nbPings;

NomFicXml = [nomDir2 '.xml'];
Survey = [];
if isempty(Survey)
    Survey.Name           = 'Unkown';
    Survey.Vessel         = 'Unkown';
    Survey.Sounder        = 'Unkown';
    Survey.ChiefScientist = 'Unkown';
end

flag = WC_writeXmlPng(NomFicXml, WC, Survey, codeUnit);

% if ishandle(Fig)
%     my_close(Fig);
% end

