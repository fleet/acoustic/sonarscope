%{
Conversion du bloc sismique pour d�veloppement SonarScope-3DViewer V2

addpath('C:\Documents and Settings\augustin\My Documents\SEGY\S4M\Geophysics_3.0')
CEau = 1490; % m/s
NomFicSegY = 'C:\Temp\Marmesonet\Cube_Marmara\hr3d\Study\Marmara\cube\cube_jma.seg';
NomFicXML = my_tempname('.xml')
flag = SEGY_ExportSismique3DViewerV2(NomFicSegY, NomFicXML, CEau)
%}

function flag = SEGY_ExportSismique3DViewerV2(NomFicSegY, NomFicXML, CEau)

if iscell(NomFicSegY)
    N = length(NomFicSegY);
    if N == 1
        flag = SEGY_ExportSismique3DViewerV2_unitaire(NomFicSegY{1}, NomFicXML, CEau);
    else
        [pathname, filename] = fileparts(NomFicXML);
        for k=1:N
            filename = sprintf('%s_%02d', filename, k);
            NomFicXML = fullfile(pathname, filename);
            flag = SEGY_ExportSismique3DViewerV2_unitaire(NomFicSegY, NomFicXML, CEau);
        end
    end
else
    flag = SEGY_ExportSismique3DViewerV2_unitaire(NomFicSegY, NomFicXML, CEau);
end

function flag = SEGY_ExportSismique3DViewerV2_unitaire(NomFicSegY, NomFicXML, CEau)

str1 = 'Lecture du fichier SEGY en cours';
str2 = 'Reading SEGY file.';
WorkInProgress(Lang(str1,str2))

[seismic, text_header, binary_header] = read_segy_file(NomFicSegY);

nx = binary_header(2);
ny = binary_header(1);
nz = binary_header(8);

Reflectivity_zyx = reshape(seismic.traces, [nz ny nx]);
% Data.Reflectivity_zyx = reshape(seismic.traces, [nz ny nx]);
% Data.Reflectivity_zxy = permute(Data.Reflectivity_zyx,[1 3 2]);
% Data.Reflectivity_xyz = permute(Data.Reflectivity_zyx,[3 2 1]);

%% Recherche des coordonn�es

for k=1:size(seismic.header_info, 1)
	flag = strfind(seismic.header_info{k,1}, 'cdp_x');
    if flag
        idx(1) = k;
    end
	flag = strfind(seismic.header_info{k,1}, 'cdp_y');
    if flag
        idx(2) = k;
    end
end

C = seismic.headers(idx,:);
Deg = floor(C / 1e6);
C = C - Deg * 1e6;
Minute = floor(C / 1e4);
C = C - Minute * 1e4;
Second = C / 100;
Coor = Deg + Minute/60 + Second/3600;
Data.Latitude  = reshape(Coor(2,:), [ny nx]);
Data.Longitude = reshape(Coor(1,:), [ny nx]);
% SonarScope(Lat)
% SonarScope(Lon)



TEnd = binary_header(6) / 1000; % Temps en s
TDeb = binary_header(8) / 1000; % Temps en s
Data.t = linspace(TDeb, TEnd, nz);

Depth = -CEau * Data.t / 2;
Depth = Depth + 140; % TODO : ATTENTION BIDOUILLE : on rajoute artificiellement 140 m pour s'adapter � la donn�e bathy : trouver l'origine du bug

Data.z = Depth;

[nomDirRacine, nomFicSeul] = fileparts(NomFicXML);
% nomDirRacine = fullfile(nomDirRacine, nomFicSeul);

%% G�n�ration du XML SonarScope

Info.Signature1     = 'SonarScope';
Info.Signature2     = '3DSeismic';
Info.FormatVersion  = 20100923;
Info.Name           = 'MARMESONET 3D SEISMIC';
Info.ExtractedFrom  = 'SEGY';
Info.Survey         = 'Marmesonet Leg2';
Info.Vessel         = 'Suro�t';
Info.Sounder        = 'HR3D';
Info.ChiefScientist = 'Louis G�li';
Info.ProcessedBy    = 'Yannick Thomas IFREMER';
Info.Comments       = 'Donn�� sous �chantillonn�e par 2 en x et en y';

Info.Dimensions.nx          = nx;
Info.Dimensions.ny          = ny;
Info.Dimensions.nz          = nz;

Info.Signals(1).Name          = 't';
Info.Signals(end).Dimensions  = '1, nz';
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = 's';
Info.Signals(end).FileName    = fullfile(nomFicSeul,'t.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'z';
Info.Signals(end).Dimensions  = '1, nz';
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = 'm';
Info.Signals(end).FileName    = fullfile(nomFicSeul,'z.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');


Info.Images(1).Name           = 'Latitude';
Info.Images(end).Dimensions   = 'ny, nx';
Info.Images(end).Storage      = 'single';
Info.Images(end).Unit         = 'deg';
Info.Images(end).FileName     = fullfile(nomFicSeul,'Latitude.bin');
Info.Images(end).Tag          = verifKeyWord('TODO');

Info.Images(end+1).Name       = 'Longitude';
Info.Images(end).Dimensions   = 'ny, nx';
Info.Images(end).Storage      = 'single';
Info.Images(end).Unit         = 'deg';
Info.Images(end).FileName     = fullfile(nomFicSeul,'Longitude.bin');
Info.Images(end).Tag          = verifKeyWord('TODO');

% Info.Images(end+1).Name       = 'Reflectivity_zyx';
% Info.Images(end).Dimensions   = 'nz, ny, nx';
% Info.Images(end).Storage      = 'single';
% Info.Images(end).Unit         = 'Amp';
% Info.Images(end).FileName     = fullfile(nomFicSeul,'Reflectivity_zyx.bin');
% Info.Images(end).Tag          = verifKeyWord('TODO');
% 
% Info.Images(end+1).Name       = 'Reflectivity_zxy';
% Info.Images(end).Dimensions   = 'nz, nx, ny';
% Info.Images(end).Storage      = 'single';
% Info.Images(end).Unit         = 'Amp';
% Info.Images(end).FileName     = fullfile(nomFicSeul,'Reflectivity_zxy.bin');
% Info.Images(end).Tag          = verifKeyWord('TODO');
% 
% Info.Images(end+1).Name       = 'Reflectivity_xyz';
% Info.Images(end).Dimensions   = 'nx, ny, nz';
% Info.Images(end).Storage      = 'single';
% Info.Images(end).Unit         = 'Amp';
% Info.Images(end).FileName     = fullfile(nomFicSeul,'Reflectivity_xyz.bin');
% Info.Images(end).Tag          = verifKeyWord('TODO');

%% Cr�ation du fichier XML d�crivant la donn�e 

% NomFicXML = fullfile(nomDirRacine, [nomFicSeul '.xml']);
xml_write(NomFicXML, Info);
flag = exist(NomFicXML, 'file');
if ~flag
    messageErreur(NomFicXML)
    return
end

%% Cr�ation du r�pertoire 

nomDirData = fullfile(nomDirRacine, nomFicSeul);
if ~exist(nomDirData, 'dir')
    status = mkdir(nomDirData);
    if ~status
        messageErreur(nomDirData)
        flag = 0;
        return
    end
end

%% Cr�ation des fichiers binaires des signaux

for i=1:length(Info.Signals)
    flag = writeSignal(nomDirRacine, Info.Signals(i), Data.(Info.Signals(i).Name));
    if ~flag
        return
    end
end


%% Cr�ation des fichiers binaires des images

for i=1:length(Info.Images)
    flag = writeImage(nomDirRacine, Info.Images(i), Data.(Info.Images(i).Name));
    if ~flag
        return
    end
end

%% Cr�ation des images PNG 

for k=1:ny
    figure(564); imagesc(squeeze(Reflectivity_zyx(:,k,:)), [-13463  13071]); colormap(gray(256)); drawnow
    savePNG(Reflectivity_zyx(:,k,:), k, NomFicXML, 'dim2')
end


for k=1:nx
    figure(564); imagesc(squeeze(Reflectivity_zyx(:,:,k)), [-13463  13071]); colormap(gray(256)); drawnow
    savePNG(Reflectivity_zyx(:,:,k), k, NomFicXML, 'dim3')
end

flag = 1;

function savePNG(X, k, NomFicXML, nomDim)

[nomDirRacine, nomFic] = fileparts(NomFicXML);
nomDirRacine = fullfile(nomDirRacine, nomFic);

X = squeeze(X);
CLim = [-13000 13000];
map = gray(256);

I = gray2ind(mat2gray(X, double(CLim)), size(map,1));

nomDirRacine = fullfile(nomDirRacine, nomDim);
if ~exist(nomDirRacine, 'dir')
    flag = mkdir(nomDirRacine);
    if ~flag
        messageErreurFichier(nomDirRacine, 'WriteFailure');
        return
    end
end

nomSousRepertoire = sprintf('%03d', floor(k/100));
nomDir3 = fullfile(nomDirRacine, nomSousRepertoire);
if ~exist(nomDir3, 'dir')
    flag = mkdir(nomDir3);
    if ~flag
        messageErreurFichier(nomDir3, 'WriteFailure');
        return
    end
end

nomFic1 = sprintf('%05d.png', k);
nomFic2 = fullfile(nomDir3, nomFic1);
imwrite(I, map, nomFic2, 'Transparency', 0)
