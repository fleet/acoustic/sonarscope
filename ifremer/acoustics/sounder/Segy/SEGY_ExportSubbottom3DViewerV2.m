% NomFicSegY = 'C:\Temp\SubbottomData\AUV0001_D20091123_T050935_process.seg';
% WC_createSubbottomForAltran(NomFicSegY)

function flag = SEGY_ExportSubbottom3DViewerV2(NomFicSegY, NomFicXML, CEau)

CLim = [];
map  = [];

if iscell(NomFicSegY)
    N = length(NomFicSegY);
    if N == 1
        flag = SEGY_ExportSubbottom3DViewerV2_unitaire(NomFicSegY{1}, NomFicXML, CEau, CLim, map);
    else
        [pathname, filename] = fileparts(NomFicXML);
        for k=1:N
            filename = sprintf('%s_%02d', filename, k);
            NomFicXML = fullfile(pathname, filename);
            [flag, CLim, map] = SEGY_ExportSubbottom3DViewerV2_unitaire(NomFicSegY, NomFicXML, CEau, CLim, map);
        end
    end
else
    flag = SEGY_ExportSubbottom3DViewerV2_unitaire(NomFicSegY, NomFicXML, CEau, CLim, map);
end

function [flag, CLim, map] = SEGY_ExportSubbottom3DViewerV2_unitaire(NomFicSegY, NomFicXML, CEau, CLim, map)


% TODO : remplacer read_segyOLD par read_segy : mais y'a des trucs �
% rajouter dans read_segy : � voir avec Anne.

% [flag, seismic] = read_segy(NomFicSegY);
[flag, seismic] = read_segyOLD(NomFicSegY);
if ~flag
    return
end

Data.Reflectivity = seismic.traces;

if isempty(CLim)
    str1 = 'Une fen�tre SonarScope va s''ouvrir afin que vous r�gliez le contraste. N''oubliez pas de faire "Save" avant de quitter. Le m�me rehaussement de contraste sera utilis� pour toutes les images.';
    str2 = 'A SonarScope window will open please set the contrast and click on "Save" before "Quit". The value will be used for all the images.';
    my_warndlg(Lang(str1,str2), 1);
    
    a = cl_image('Image', Data.Reflectivity, 'ColormapIndex', 2, 'Video', 2);
    a = SonarScope(a);
    CLim  = a.CLim;
    map   = a.Colormap;
    Video = a.Video;
    if Video == 2
        map = flipud(map);
    end
end

% nbPings  = size(Data.Reflectivity,2);
% nbRows   = size(Data.Reflectivity,1);

% map = gray(256);
% CLim = [0 0.05];


%% Lecture de la hauteur si elle existe

[flagHeight, DataComplementaire] = readHeightSegyInCache(NomFicSegY);
if flagHeight
    Height = DataComplementaire.Height;
end

%% Cr�ation de l'image

subNaN = isnan(Data.Reflectivity);
I = gray2ind(mat2gray(Data.Reflectivity, double(CLim)), size(map,1));
I(I == 0) = 1;
I(subNaN) = 0;
% I = flipud(I); % A confirmer

if flagHeight
    for k=1:length(Height)
        subWC = 1:(floor(Height(k)));
        I(subWC,k) = NaN;
    end
end

%% Cr�ation du fichier XML-Bin

flag = create_XMLBin_ImagesAlongNavigation(I, NomFicXML, map, seismic);
