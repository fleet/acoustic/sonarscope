% GetSegyTraceData : Get Segy trace data if filehandle
% 
% Call : 
%
%   tracedata=GetSegyTraceData(segyid,ns,SegyHeader,SkipData
%

%
% (C) 2001-2004 Thomas Mejer Hansen, tmh@gfy.ku.dk/thomas@cultpenguin.com
%
%    This program is free software; you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published by
%    the Free Software Foundation; either version 2 of the License, or
%    (at your option) any later version.
%
%    This program is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%    GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with this program; if not, write to the Free Software
%    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
% 
% GLU le 22/08/11 : optimisation de la Toolbox SEGYMAT 1.4
% - suppression de toutes les virgules en fin de test If
% - ajout du kind sur les instructions exist.

function tracedata=GetSegyTraceData(segyid,ns,SegyHeader,SkipData) %#ok<INUSD>
%
% Get Segy Trace Data. 
%
%

SkipData=[];  
  
if nargin==0
    SegymatVerbose(exist('segyid', 'var'))
    SegymatVerbose([mfilename,' : SEGYID not specified - exiting'])
    tracedata=[];
    return
end
if nargin==1
  SegymatVerbose([mfilename,' : NS not specified - exiting'])
  tracedata=[];
  return
end
if nargin==2
    SegyHeader.DataFormat='float32'; 
    SegyHeader.BytesPerSample=32;
    SegyHeader.DataSampleFormat=5; % IEEE
    SegymatVerbose(['Dataformat not specified -  dataformat->',SegyHeader.DataFormat])
end

if isempty(SkipData)==1
    SkipData=0; 
end

Revision=SegyHeader.SegyFormatRevisionNumber;
if Revision>0, Revision=1; end
Format=SegyHeader.Rev(Revision+1).DataSampleFormat(SegyHeader.DataSampleFormat).format;  

BPS=SegyHeader.Rev(Revision+1).DataSampleFormat(SegyHeader.DataSampleFormat).bps;  
if (SkipData==1)
    SkipBytes=ns*BPS/8;
    fseek(segyid,SkipBytes,'cof');
    tracedata=[];
else  
    % SegymatVerbose([mfilename,' : ',Format,'. ns=',num2str(ns)])
    try 
        
        % DEB GLU le 10/02/2012 : Déformattage de la donnée mais pb
        % identification du format (normalement en Fixed Point) !!!! 
        pppp   = fread(segyid,ns,Format);
        
        % Cas du ficier SHALLOWSURVEY : LINE001_SINGLEBOOM.sgy
        if (strcmp(Format, 'int16' ) && Revision == 0 && SegyHeader.DataSampleFormat == 3)
            % % % %              Format = 'uint16'; % Solution de A. PACAULT.
            % FIN GLU le 10/02/2012 : Déformattage de la donnée
            pppp2  = typecast(int16(pppp), 'uint16');
            
            % Calcul de standard déviation pour décoder correctement le format.
            % On normalise sur [0, 1] tout d'abord la gamme de signal.
            % On ne teste que les 100 1ers échantillons ou la comparaison
            % de signal est significative.
            nSampleTest = min(ns, 100);
            dummy       = double(pppp(1:nSampleTest));
            dummy       = dummy/max(abs(dummy));
            dummy2      = double(pppp2(1:nSampleTest));
            dummy2      = dummy2/max(abs(dummy2));
            
            if (std(dummy) < std(dummy2))
                tracedata = pppp;
            else
                tracedata = pppp2;
            end
        else
            tracedata = pppp;
        end
            
    catch %#ok<CTCH>
        SegymatVerbose([mfilename,' : Error using fread - Possibly ''ns'' is negative -' ...
            ' check byteorder-'])
        tracedata=[];
    end
    
    
    if (strcmp(Format,'uint32')==1) % IBM FLOATING POINT
        % CONVERT FROM FLOATING POINT
        verbose=1;
        if verbose>1, SegymatVerbose([mfilename,'Converting from IBM, DataFormat :',SegyHeader.DataFormat]); end
        % DEB GLU le 22/08/11 : optimisation de la Toolbox SEGYMAT 1.4
        % try
        [flag, tracedata]=ibm2num(uint32(tracedata));
        if ~flag
            return
        end

        % catch
          % SegymatVerbose([mfilename,' : SOMETHING BAD HAPPENED WHEN CONVERTING FROM IBM FLOATS TO IEEE. ARE YOU SURE DATA ARE IBM FLOAT FORMATTED ?' ])
          % tracedata=0.*tracedata;
          % return

        % end
        % FIN GLU le 22/08/11 : optimisation de la Toolbox SEGYMAT 1.4
    end
    
end
