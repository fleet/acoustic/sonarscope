% ReadSegy : Reads a SEG Y rev 1 formatted file
%
% Call :
% [Data,SegyTraceHeaders,SegyHeader]=ReadSegy(filename);
%
% To read time slice 0.5<t<5 :
% [Data,SegyTraceHeaders,SegyHeader]=ReadSegy(filename,'trange',.5,3);
% Skip every 5th trace :
% [Data,SegyTraceHeaders,SegyHeader]=ReadSegy(filename,'jump',5);
% Read data in a CDP header range : 5000<cdp<5800 :
% (change cdp to any other valid TraceHeader value)
% [Data,SegyTraceHeaders,SegyHeader]=ReadSegy(filename,'minmax','cdp'5000,5800);
% Read only the header values (Data will return empty)
% [Data,SegyTraceHeaders,SegyHeader]=ReadSegy(filename,'SkipData',1);
%
% SEG-Y format revision number can be '0' (1975) or
% '100' (similar to '1') (2002).
% By default the SEG-Y format revision number is read in the
% binary header, but this can be overruled using :
% [Data,SegyTraceHeaders,SegyHeader]=ReadSegy(filename,'revision',0);
%
% Read using a specific Data Sample Format :
% Rev 0, IBM FLOATING POINT
% [Data,SegyTraceHeaders,SegyHeader]=ReadSegy(filename,'revision',0,'dsf',1);
% Rev 1, IEEE FLOATING POINT
% [Data,SegyTraceHeaders,SegyHeader]=ReadSegy(filename,'revision',1,'dsf',5);
%
% A SegyHeader can be forced on the SEG-Y file using :
% [Data,SegyTraceHeaders,SegyHeader]=ReadSegy(filename,'SegyHeader',SegyHeader);
% The SegyHeader can be obtain by GetSegyHeader(segyfilename), and
% then edited.
%
% To read using little endian :
% [Data,SegyTraceHeaders,SegyHeader]=ReadSegy(filename,'endian','l');
%
% Combine any combination of the above
% [Data,SegyTraceHeaders,SegyHeader]=ReadSegy(filename,'jump',1,'minmax','cdp',5300,5400);
%
%
% Plot the data using e.g.
% imagesc([SegyTraceHeaders.cdp],SegyHeader.time,Data);
% wiggle([SegyTraceHeaders.TraceNumber],SegyHeader.time,Data);
%
% (C) 2003-2004, Thomas Mejer Hansen, tmh@gfy.ku.dk
%

% Implemented using the syntax of the SEG-Y revised format :
% SEGY-Y rev 0, SEG-Y rev 1 as described in
% http://seg.org/publications/tech-stand/
%
% Extended Textual Header is not yet tested
% If you would like it implemented, please send me an SEGY file with
% that sort of information, as well as a description of the segy file
%
%
% (C) 2001-2004, Thomas Mejer Hansen, tmh@gfy.ku.dk/thomas@cultpenguin.com
%
%    This program is free software; you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published by
%    the Free Software Foundation; either version 2 of the License, or
%    (at your option) any later version.
%
%    This program is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%    GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with this program; if not, write to the Free Software
%    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
%
%
%
%
% 0.1 : INitial Release
% 0.2 : Added SkipData var, to skip reading of data.
% 0.3 : May 01, 2002
%       Added ability to read in ever 'jump' traces.
%       Added ability to read in time range.
%       Added abiliy to read in header range (ex. mincdp to maxcdp).
% GLU le 22/08/11 : optimisation de la Toolbox SEGYMAT 1.4
% - suppression de toutes les virgules en fin de test If
% - ajout du kind sur les instructions exist.
% - ajout du traitement en memmapfile


% TODO : WHEN READING ONLY PART OF DATASET MAKE SURE TO ADJUST THE SEGY
% HEADER ACCORDINGLY !!!!!!

function [flag, Data,SegyTraceHeaders,SegyHeader,HeaderInfo]=ReadSegy(filename,varargin)

[varargin, flagMemmapfile] = getPropertyValue(varargin, 'Memmapfile', 1);

if isoctave
    doWaitBar=0; % [1] show progress bar gui
    mfilename='ReadSegy';
else
    doWaitBar=1;
    mfilename='ReadSegy';
end
% Initialisations en cas de sortie impromptue.
flag = 0;
Data                = [];
% SegyTraceHeaders    = [];
if exist('SegyHeader', 'var')==0
    SegyHeader          = [];
end
HeaderInfo          = [];

% GLU : tentative d'identification du type de données Sismique ou SUBOP.
% Particularité observé sur les cubes : le Jobid donne le nombre de profils
% ????
if contains(filename, 'cube')
    dimData = 3;
else
    dimData = 2;
end

dsf             =[];
revision        =[];
endian_tight    =[];
tmin            =[];
tmax            =[];
headermin       =[];
headermax       =[];
header          =[];
jump            =[];
SkipData        =[];

% Analyse de l'endianness du fichier.
endian = getEndianness(filename);

SegymatVerbose([mfilename,' : reading ',filename])

% GLU le 22/08/11 : optimisation de la Toolbox SEGYMAT 1.4
if ~(exist(filename, 'file'))
    SegymatVerbose([mfilename,' : ', filename,' does not exist !'])
    Data                =[];
    SegyTraceHeaders    =[];
    SegyHeader          =[];
    HeaderInfo          =[];
    return
end

% IF ONLY 'filename', AND one outpuet HAS BEEN
% SPECIFIED AS IN/OUTPUT, THEN USE THE FAST
% ALGORITHM FOR READING.
if (nargin==1)&&(nargout==1)
    [Data]=ReadSegyFast(filename);
    return
end

SegymatVerbose([mfilename,' - Checking Varargin'],90)

ninput=nargin;
% NEXT TWO LINES TO ENUSRE THAT VARARGIN CAN BE PASSED TO FUNCTION
if ninput==2
    % CALL USING VARARGIN
    ninput=1+length(varargin{1});
    varargin=varargin{1};
else
    % DIRECT CALL
    ninput=length(varargin);
end


% TRANSFORM VARARGING INTO PARAMETERS
cargin=1;
while (cargin<ninput)
    SegymatVerbose([mfilename,' - Converting varargin, ',num2str(cargin)],90)
    
    if strcmp(varargin{cargin},'jump')
        cargin=cargin+1;
        eval(['jump=',num2str(varargin{cargin}),';']);
        SegymatVerbose(['JUMP : Read only every ',num2str(jump),'th trace'])
    end
    
    if strcmp(varargin{cargin},'minmax')
        cargin=cargin+1;
        eval(['header=''',varargin{cargin},''';']);
        cargin=cargin+1;
        eval(['headermin=',num2str(varargin{cargin}),';']);
        cargin=cargin+1;
        eval(['headermax=',num2str(varargin{cargin}),';']);
        SegymatVerbose(['MIN MAX : Using header ',header,' from ',num2str(headermin),' to ',num2str(headermax)])
    end
    
    if strcmp(varargin{cargin},'trange')
        cargin=cargin+1;
        eval(['tmin=',num2str(varargin{cargin}),';']);
        cargin=cargin+1;
        eval(['tmax=',num2str(varargin{cargin}),';']);
        SegymatVerbose(['TRANGE : tmin=',num2str(tmin),' tmax=',num2str(tmax)])
    end
    
    % ENDIAN FORMAT
    if strcmp(varargin{cargin},'endian')
        cargin=cargin+1;
        eval('endian_tight=varargin{cargin};')
        if endian_tight=='l'
            SegymatVerbose('USING LITTLE ENDIAN TYPE')
            endian='ieee-le';
        else
            SegymatVerbose('USING BIG ENDIAN TYPE')
        end
    end
    
    if strcmp(varargin{cargin},'revision')
        cargin=cargin+1;
        eval(['revision=',num2str(varargin{cargin}),';']);
        SegymatVerbose(['USING REVISION : rev=',num2str(revision)])
    end
    
    if strcmp(varargin{cargin},'dsf')
        cargin=cargin+1;
        eval(['dsf=',num2str(varargin{cargin}),';']);
        SegymatVerbose(['USING Data Sample Format : dsf=',num2str(dsf)])
    end
    
    if strcmp(varargin{cargin},'SkipData')
        cargin=cargin+1;
        eval(['SkipData=',num2str(varargin{cargin}),';']);
        SegymatVerbose('SKIPPNG DATA - READING ONLY HEADERS');
    end
    
    if strcmp(varargin{cargin},'SegyHeader')
        cargin=cargin+1;
        SegyHeader=varargin{cargin};
        SegymatVerbose('USING LOADED SEGYHEADER');
    end
    
    cargin=cargin+1;
    
end


if isempty(SkipData)==1
    SegymatVerbose([mfilename,' : Skip data is not set (defautls to 0)'],90)
    SkipData=0; % [0] READ ONLY HEADER VALUES, [1] READ IN ALL DATA
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% OPEN FILE HANDLE

if exist('endian', 'var')==1
    SegymatVerbose([mfilename,' : ENDIAN : ',endian],1)
    segyid = fopen(filename,'r',endian);
else
    endian='ieee-be';
    SegymatVerbose([mfilename,' : ENDIAN SET TO ',endian],0)
    segyid = fopen(filename,'r','ieee-be');  % ALL DISK FILES ARE IN BIG
end                                        % ENDIAN FORMAT, ACCORDING TO
% SEGY Y rev 1


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% BINARY HEADERS
if isempty(SegyHeader)
    % % % if exist('SegyHeader', 'var')==0
    SegyHeader=GetSegyHeader(segyid);
else
    SegymatVerbose([mfilename,' - Using supplied SegyHeader'])
end

% APPLY CHANGES TO SEGY HEADER IF NEEDE
if isempty(revision)==0
    SegyHeader.SegyFormatRevisionNumber=revision;
    SegymatVerbose([mfilename,' - Manually set SEG-Y revision to ',num2str(revision)])
end
if isempty(dsf)==0
    SegyHeader.DataSampleFormat=dsf;
end



% JUST SOME INFORMATION TO WRITE TO SCREEN :

Revision=SegyHeader.SegyFormatRevisionNumber;
if Revision>0, Revision=1; end

if (SegyHeader.DataSampleFormat>length(SegyHeader.Rev(Revision+1).DataSampleFormat))
    SegymatVerbose([mfilename,' : WARNING : YOU HAVE SELECTED (OR THE FILE IS FORMATTED SUCH THAT) A DATASAMPLE FORMAT THAT IS NOT DEFINED. \nREMEBER IEEE IS NOT SPECIFIED IN THE SEGY REV0 STANDARD !'])
    
    if (Revision==0)
        SegymatVerbose([mfilename,' : TRYING TO USE REVISION 1 AS OPPOSED TO REVISION 0'])
        Revision=1;
        
        if (SegyHeader.DataSampleFormat>length(SegyHeader.Rev(Revision+1).DataSampleFormat))
            SegymatVerbose([mfilename,' : FATAL ERROR : STILL THE DATASAMPLE FORMAT IS NOT SUPPRTED - EXITING (Report error to tmh@gfy.ku.dk)'])
        else
            SegymatVerbose([mfilename,' : APPARENT SUCCES CHANING FROM Revision 0 to 1 - Continuing'])
            SegyHeader.SegyFormatRevisionNumber=1; % FORCING REVISION TO BE 1 !!!
        end
        
    end
    
end

FormatName=SegyHeader.Rev(Revision+1).DataSampleFormat(SegyHeader.DataSampleFormat).name;
Format=SegyHeader.Rev(Revision+1).DataSampleFormat(SegyHeader.DataSampleFormat).format;
BPS=SegyHeader.Rev(Revision+1).DataSampleFormat(SegyHeader.DataSampleFormat).bps;
txt=['SegyRevision ',sprintf('%0.4g',Revision),', ',FormatName,'(',num2str(SegyHeader.DataSampleFormat),')'];


ns=SegyHeader.ns;
% YOU CAN FORCE FixedLengthTraceFlag=1;
% This will make the code much faster (especially when using
% the 'jump' option) but reading data with varying trace lengt will fail.
% It is here since many old data sets with Constant trace length
% has FixedLengthTraceFlag=0;
%
% As of version 1.01 this has been enable by default.
% Change the variable below to '0' if you do not want this behaviour
%
SegyHeader.FixedLengthTraceFlag=1;


SegymatVerbose([mfilename,' : Reading Data'],90);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% READ DATA
%Segy=fread(segyid,4000,'float32');
fseek(segyid,0,'eof');
% DEB GLU le 22/08/11 : optimisation de la Toolbox SEGYMAT 1.4
DataEnd=ftell(segyid);

DataStart=3600+3200*SegyHeader.NumberOfExtTextualHeaders;
fseek(segyid,DataStart,'bof');       % Go to the beginning of the file


ntraces=round((DataEnd-DataStart)./(240+(SegyHeader.ns)*(BPS/8)));

SegymatVerbose(['Number of Samples Per Trace=',num2str(SegyHeader.ns)])
SegymatVerbose(['Number of Traces=',num2str(ntraces)])
if (ntraces~=round(ntraces))
    SegymatVerbose('Trace lengths seems to vary. trying to read the file anyway')
end

existJump=~isempty(jump);
existHeader=~isempty(header);
existTmin=~isempty(tmin);
existTmax=~isempty(tmax);


% Définition des dimensions :
% - ns : échantillonnage de la donnée, le long de la profondeur
% - la trace correspond à la notion de profils verticaux
% - la line correspond à la notion de slice (coupe parallèle le long d'une navigation).
if existJump==1
    out_ntraces=ceil(ntraces/jump);
else
    % Traitement du cas 3D.
    % Dans le cas des cubes, le JobId donnent le nb de profils !
    if dimData == 3
        % % % %     if SegyHeader.Line ~= SegyHeader.Job && SegyHeader.Job > 0
        % nbLineMaxBloc   = min(SegyHeader.Line, 2);
        [flag, nbLineMaxBloc]   = computeNbLinesPerBloc(SegyHeader.Line);
        out_ntraces             = [SegyHeader.Job nbLineMaxBloc];
        nbLineMax               = SegyHeader.Line;
        % Initialisation des var de sorties.
        nbBlocs                 = ceil(nbLineMax/nbLineMaxBloc);
        CubeMemmapFile          = cell(nbBlocs,1);
    else
        out_ntraces     = ntraces;
        % Une seule slice longitudinale
        nbLineMaxBloc   = 1;
        nbLineMax       = 1;
        CubeMemmapFile  = {};
    end
end

% DEB GLU : le 23/08/2011, ajout du traitement en Memmapfile pour les gros fichiers;
if flagMemmapfile
    DataMemmapFile  = cl_memmapfile('Size', [ns out_ntraces], 'Format', 'single');
    % else
    %     tmpData = zeros(ns, out_ntraces, 'single');
end
% FIN GLU : le 23/08/2011, ajout du traitement en Memmapfile pour les gros fichiers;


% DEB GLU le 22/08/11 : optimisation de la Toolbox SEGYMAT 1.4
% dwaitbar=20;
% if existJump
%     if DataEnd/jump>1e+6
%         dwaitbar=10;
%     end
%     if DataEnd/jump>1e+7
%         dwaitbar=50;
%     end
%     if DataEnd/jump>1e+8
%         dwaitbar=200;
%     end
% end
% FIN GLU le 22/08/11 : optimisation de la Toolbox SEGYMAT 1.4

traceinfile=0;

% GLU le 22/08/11 : optimisation de la Toolbox SEGYMAT 1.4
% toc_old=toc;
tic
if doWaitBar == 1
    hw = create_waitbar(['Reading Segy - ',txt]);
end

% LOOP OVER TRACES

t0=now;
tlast=t0;
pos0=ftell(segyid);

iLine       = 1; % Nb de lignes lues dans le fichier
iLineInBloc = 1; % Nb de lignes du bloc courant
iBloc       = 1; % Num du bloc courant.
while (iLine <= nbLineMax)
    % while (~feof(segyid))
    
%     mes = ['N° de ligne (max = ' num2str(nbLineMax) ') : ' num2str(iLine) ' Num Bloc : ' num2str(iBloc)];
%     fprintf('\n%s\n', mes); % Parcours des dimensions en Line-Job de Out_NTraces.
    outtrace = 0;
    for iDim=1:out_ntraces(1)
        traceinfile = traceinfile+1;
        
        usetrace = 1; % DEFAULT USING TRACE WHEN [1].
        
        ishow = 10000;
        itime = 1 / (24*3600) * 2; % Min time between verbose info to screen
        if (((traceinfile/ishow) == round(traceinfile/ishow)) && ((now-tlast) > itime))
            
            tnow  = now;
            tlast = tnow;
            posnow = ftell(segyid);
            tend = t0+DataEnd.*(tnow-t0)./(posnow-pos0);
            tleft = (tend-tnow)*3600*24;
            txt = sprintf('Reading trace %d/%d, (%5.0fs left) (est end %s)',traceinfile,ntraces,tleft,datestr(tend));
            %          toc_old=toc;
            SegymatVerbose(txt)
        end
        TraceStart=ftell(segyid);
        
        % IF 'JUMP' IS SET THEN CHECK IF THIS TRACE SHOULD BE SKIPPED
        if existJump==1
            if (traceinfile/jump)~=round(traceinfile/jump), usetrace=0; end
        end
        
        if ((usetrace==0)&&(SegyHeader.FixedLengthTraceFlag==1))
            % SKIP FORWARD IN FILE'
            skip=240+(BPS/8)*SegyHeader.ns;
            fseek(segyid,skip,'cof');
            SegymatVerbose([num2str(traceinfile),' - SKIPPING TRACE ... ',num2str(outtrace)],2)
        elseif (SegyHeader.FixedLengthTraceFlag==0)
            SegymatVerbose(sprintf('Using Fixed Length Trace as SegyHeader.FixedLengthTraceFlag=0'),10);
            [flag, SingleSegyTraceHeaders]=GetSegyTraceHeader(segyid,TraceStart,Format,SegyHeader.ns,[]);
            if ~flag
                my_close(hw);
                return
            end
            SingleSegyData.data=GetSegyTraceData(segyid,SingleSegyTraceHeaders.ns,SegyHeader);
        else
            [flag, SingleSegyTraceHeaders]=GetSegyTraceHeader(segyid,TraceStart,Format,SegyHeader.ns,[]);
            if ~flag
                my_close(hw);
                return
            end
            SingleSegyData.data=GetSegyTraceData(segyid,SegyHeader.ns,SegyHeader);
            
            if SingleSegyTraceHeaders.TraceNumber<1
                SingleSegyTraceHeaders.TraceNumber=traceinfile;
                SegymatVerbose(sprintf('TraceNumber malformatetd. Setting TraceNumber=%d',traceinfile),10);
            end
            
            SegymatVerbose(sprintf('ns=%d, Trace in line : %d, Trace in file : %d, ns=%10.5f dt=%10.5f',SingleSegyTraceHeaders.ns,SingleSegyTraceHeaders.TraceSequenceLine,SingleSegyTraceHeaders.TraceSequenceFile,SingleSegyTraceHeaders.ns,SingleSegyTraceHeaders.dt),10)
            
            
        end
        
        
        % IF HEADER MIN MAX HAS BEEN CHOSEN, THEN CHECK THAT TRACE IS GOOD ENOUGH
        if ((existHeader==1)&&(usetrace==1))
            headervalue=getfield(SingleSegyTraceHeaders,header); %#ok<GFLD>
            if ((headervalue<headermin)||(headervalue>headermax))
                usetrace=0;
            end
        end
        
        % USE THIS TRACE IF usetrace=1 !!
        if usetrace==1
            %% IF TIME RANGE IS SPECIFIED, THEN EXTRACT THIS
            if (existTmin==1)&&(existTmax==1)
                % NEXT LINE SHOULD CONSIDER THAT ns in Trace and Segy Header could vary !!!
                origtrange=(1:1:SegyHeader.ns).*SegyHeader.dt.*1e-6+SingleSegyTraceHeaders.DelayRecordingTime.*1e-3;
                gooddata=find(origtrange>tmin & origtrange<tmax);
                SingleSegyData.data=SingleSegyData.data(gooddata);
                % CHECK NEXT LINE TAHT DelatRec... is in micro seconds
                SingleSegyTraceHeaders.DelayRecordingTime=tmin;
                SingleSegyTraceHeaders.ns=length(gooddata);
                ns=length(gooddata); %  for use below
            end
            
            outtrace=outtrace+1;
            
            if (outtrace==1 && iLine == 1)
                % Preallocate RAM
                ta1=now;
                SegymatVerbose(sprintf('Pre allocating RAM ntraces=%d out_traces=%d',ntraces,out_ntraces(iDim)));
                if numel(out_ntraces) ==1
                    SegyData=repmat(struct('data',zeros(ns,1)),out_ntraces, 1);
                    SegyTraceHeaders=repmat(SingleSegyTraceHeaders, out_ntraces, 1);
                else
                    SegyData=repmat(struct('data',zeros(ns,1)),out_ntraces(1), out_ntraces(2));
                    % GLU OLD SegyTraceHeaders=repmat(SingleSegyTraceHeaders,out_ntraces(1), out_ntraces(2));
                end
                %whos SegyData SegyTraceHeaders
                %save T1
                ta2=now;
                t0=t0+ta2-ta1;
            end
            
            % DEB GLU : le 23/08/2011, ajout du traitement en Memmapfile pour les gros fichiers;
            if flagMemmapfile
                if numel(out_ntraces) == 1
                    DataMemmapFile(:,outtrace)                 = SingleSegyData.data;
                else
                    DataMemmapFile(:,outtrace, iLineInBloc)    = SingleSegyData.data;
                end
                clear SingleSegyData;
            else
                if numel(out_ntraces) == 1
                    SegyData(outtrace).data             = SingleSegyData.data;
                else
                    SegyData(outtrace, iLine).data      = SingleSegyData.data;
                end
                
            end
            % FIN GLU : le 23/08/2011, ajout du traitement en Memmapfile pour les gros fichiers;
            
            % Peu importe la dimension de cette structure, on recopie
            % toujours la même donnée.
            if numel(out_ntraces)==1
                SegyTraceHeaders(outtrace, 1)=SingleSegyTraceHeaders;
            else
                SegyTraceHeaders(outtrace, iLine)=SingleSegyTraceHeaders;
            end
            
            if doWaitBar==1
                % if ((outtrace/dwaitbar)==round(outtrace/dwaitbar))
                if (size(out_ntraces,2) ~=1)
                    % my_waitbar(ftell(segyid), DataEnd, hw);
                    waitbar((iBloc*nbLineMaxBloc)*out_ntraces(1)/(nbLineMax*out_ntraces(1)), hw);
                else
                    my_waitbar(outtrace, out_ntraces(1), hw);
                end
                % end
            end
            
        end
    end % Fin de la boucle sur out_traces(1)
    
    %DEBUG figure(12345); imagesc(DataMemmapFile(:,:,iLine)); colormap('gray'); colorbar; title(['Line : ' num2str(iLine)]);
    iLine = iLine + 1;
    outtrace = 1;
    % Gestion des blocs pour les fichiers 3D de taille importante :
    % découpage en autant de Memmapfile que nécessaire
    if (nbLineMax > 1)
        if (iLineInBloc >= nbLineMaxBloc)
            CubeMemmapFile{iBloc} = DataMemmapFile;
            % Restauration d'un Datamemmapfile vierge pour un nouveau bloc de
            % donnée
            if flagMemmapfile
                CubeMemmapFile{iBloc} = DataMemmapFile;
                clear DataMemmapFile
                % Calcul du nombre de traces pour le bloc prochain.
                nbTracesToProcess   = min(SegyHeader.Line - (iBloc*nbLineMaxBloc), nbLineMaxBloc);
                if nbTracesToProcess > 0
                    out_ntraces         = [out_ntraces(1) nbTracesToProcess];
                    DataMemmapFile      = cl_memmapfile('Size', [ns out_ntraces], 'Format', 'single');
                end
            end
            iLineInBloc = 1;
            iBloc       = iBloc + 1;
        else
            iLineInBloc = iLineInBloc + 1;
        end
    end
    
    
end
%save T2
%whos SegyData SegyTraceHeaders

if outtrace==0
    SegymatVerbose(sprintf('%s : No traces read!',mfilename));
    SegyTraceHeaders=[];
    Data=[];
    return
end

if doWaitBar==1
    try
        my_close(hw);
    catch exception
        str = getReport(exception); %#ok<NASGU>
    end
end
SegymatVerbose([mfilename,' : Elapsed time ', num2str(toc), '  ended at ', datestr(now)]);
% t=outtrace;

% Write time to SegyHeader
SegyHeader.ns = ns;
SegyHeader.time = (1:1:SegyHeader.ns) .* SegyHeader.dt ./ 1e+6 + SegyTraceHeaders(1).DelayRecordingTime ./ 1e+3;

% Make sure that only read SegyTraceHEaders are returned
% % % % if outtrace~=out_ntraces(:)
% % % %     SegyTraceHeaders=SegyTraceHeaders(1:out_ntraces(1));
% % % % end

% MOVE DATA from SegyData.data to a regular variable
% THIS STEP COULD BE AVOIDED WHEN FixedTraceLength=1, which is almost
% allways the case... Should speed up things and reduce memory reqs.
if SkipData==1
    Data=[];
else
    
    try
        % DEB GLU : le 23/08/2011, ajout du traitement en Memmapfile pour les gros fichiers;
        if flagMemmapfile
            if isempty(CubeMemmapFile)
                Data = DataMemmapFile;
                clear DataMemmapFile;
            else
                % Si le nombre de blocs prévus n'a pas été enregistrés.
                if isempty(CubeMemmapFile{end})
                    CubeMemmapFile{iBloc} = DataMemmapFile;
                end
                
                Data = CubeMemmapFile;
                clear CubeMemmapFile;
            end
        else
            Data = [SegyData(1:out_ntraces(1)).data];
        end
        % FIN GLU : le 23/08/2011, ajout du traitement en Memmapfile pour les gros fichiers;
        
        
    catch  %#ok<CTCH>
        if flagMemmapfile
            Data = cl_memmapfile('Size', [ns out_ntraces], 'Format', 'single');
        else
            Data = zeros(ns,out_ntraces(1));
        end
        % GLU : A traiter
        for i=1:out_ntraces(1)
            try
                Data(:,i)=SegyData(i).data;
            catch exception
                errmsg=getReport(exception);
                if isempty(SegyData(i).data)
                    errmsg = 'Empty data in trace';
                elseif contains(errmsg, 'In an assignment  A(:,matrix) = B, the number of rows in A and B')
                    nns = length(SegyData(i).data);
                    if nns < ns
                        errmsg = 'Length of trace varies - padding with zeros';
                        Data(1:nns,i)=SegyData(i).data;
                    else
                        errmsg = 'Length of trace varies - truncating';
                        Data(:,i)=SegyData(i).data(1:ns);
                    end
                end
                SegymatVerbose(sprintf('Had a problem at trace %d : %s', i, errmsg))
            end
        end
    end
end

flag = 1;

function [flag, nbLinesPerBloc] = computeNbLinesPerBloc(nbLines)

flag            = 1;

nbLinesPerBloc  = 100;

while nbLines < nbLinesPerBloc
    nbLinesPerBloc  = nbLinesPerBloc/2;
end
