% Sac2Segy : Reads SAC formatted data into a SegyMAT (SGY) structure
%
% CALL :
%   [Data,SegyTraceHeader,SegyHeader] = Sac2Segy(files_in,segyfile_out,varargin)
%
%   files_in : Either a single filename or a strcture of filenames
%            files_in = 'd1.SAC';
%            or
%            files_in{1} = 'd1.SAC';
%            files_in{2} = 'd2.SAC';
%
% Examples :
%   [D,STH,SH] = Sac2Segy('','test.segy','FixedLengthTraceFlag',1);
%              converts all SAC files into one SEGY file (test.segy), using
%              a FixedLengthTraceFlag of 1. This is compatible with mosty
%              any SEGY reader.
%
%   [D,STH,SH] = Sac2Segy('','test.segy','FixedLengthTraceFlag',0);
%              converts all SAC files into one SEGY file (test.segy), using
%              a FixedLengthTraceFlag of 0, allowing varying trace length of SEGY files
%              This is only compatible with revision 1 of the SEGY format.
%
%   [D,STH,SH] = Sac2Segy('file.sac');
%              convert file.sac to file.segy
%
%   [D,STH,SH] = Sac2Segy('file.sac','another_file.segy');
%              convert file.sac to another_file.segy
%
%
%   Force little endian byte format for SAC file:
%   Sac2Segy('file.sac','test.sgy','endian','l');
%
% Relies on sac2mat.m 
%
% Download SAC files from : http://www.iris.edu/hq/ssn/events
% 
function [Data, STH, SegyHeader] = Sac2Segy(files_in, segyfile_out, varargin) 

endian = 'b'; % Default big endian SAC files

if nargin == 0
    d = dir('*.sac');
    if isempty(d)
        SegymatVerbose(sprintf('No SAC files found.', mfilename));
        Data       = [];
        STH        = [];
        SegyHeader = [];
        return
    end
    for k=1:length(d)
        files_in{k} = d(k).name;
    end
end


if nargin > 0
    if isempty(files_in)
        d = dir('*.sac');
        for k=1:length(d)
            files_in{k} = d(k).name;
        end
    end
    if ischar(files_in) && (nargin == 1)
        [~, f] = fileparts(files_in);
        segyfile_out = [f, '.segy'];
    end
end

ninput = nargin;
% NEXT TWO LINES TO ENUSRE THAT VARARGIN CAN BE PASSED TO FUNCTION
if ninput == 3
    % CALL USING VARARGIN
    ninput = 2 + length(varargin{1});
    varargin = varargin{1};
else
    % DIRECT CALL
    ninput = length(varargin);
end


% TRANSFORM VARARGING INTO PARAMETERS
cargin = 1;
while cargin < ninput
    SegymatVerbose([mfilename, ' - Converting varargin, ', varargin{cargin}],-1)

    if strcmp(varargin{cargin},'FixedLengthTraceFlag')
        cargin = cargin+1;
        eval(['FixedLengthTraceFlag = ',           num2str(varargin{cargin}),';']);
        SegymatVerbose(['FixedLengthTraceFlag = ', num2str(FixedLengthTraceFlag),'.'])
    end

    if strcmp(varargin{cargin},'endian')
        cargin = cargin+1;
        eval(['endian = ''',(varargin{cargin}),''';']);
        SegymatVerbose(['endian = ',endian,'.'])
    end

    cargin = cargin+1;
end

if exist('sac2mat.m', 'file') ~= 2
    SegymatVerbose('sac2mat needs to be in your path',0)
    SegymatVerbose('Get it from http://mgstat.sourceforge.net/',0)
    return
end

try
    [SACdata,SeisData] = sac2mat(files_in,endian);
catch MESS
    if (strfind(MESS.message,'Out of memory'))
        SegymatVerbose('Out of memory error calling ''sac2mat'', suggesting endian type error',0)
        SegymatVerbose('  Try manyally setting the endian type',0)
        return
    end
end
ntraces = size(SeisData,2);
ns = [SACdata.trcLen];
% ns_max = max([SACdata.trcLen]);
% data = size(ns_max,ntraces);

% GET dt
% ONE SHOULD MULTIPLY WITH 1e+6 USING SEGY FORMAT
% HOWEEVER SINCE DT IS WRITTEN IN
% UINT16 FORMAT, AND SAC DT IS
% USUALLY VERY HIGH WE MUST CHOOSE
% TO MULTIPLY ONLY WITH 1000.
for k=1:length(ns)
    dt(k) = SACdata(k).times.delta.*1e+3; %#ok<AGROW>
end

% -------------------------------------------
% SET UP SegyHeader structure.
% -------------------------------------------


% IF A SPECFIC REVISION HAS BEEN CHOSEN, USE THAT
if exist('revision') == 1
    if revision == 1
        SegyHeader.SegyFormatRevisionNumber = 100;
    else
        SegyHeader.SegyFormatRevisionNumber = 0;
    end
    SegymatVerbose([mfilename,' :  Using user specified SEG Y revision : ',num2str(revision)],1)
else
    SegyHeader.SegyFormatRevisionNumber = 100;
end


% IF A SPECFIC DATA SAMPLING FORMAT HAS BEEN SELECTED USE THAT
if exist('dsf') == 1
    SegyHeader.DataSampleFormat = dsf;
    SegymatVerbose([mfilename,' :  Using user specified Data Sample Format : ',num2str(revision)],1)
else
    SegyHeader.DataSampleFormat = 5;
end

if exist('FixedLengthTraceFlag') == 1
    SegyHeader.FixedLengthTraceFlag = FixedLengthTraceFlag;
else
    if ntraces == 1
        SegyHeader.FixedLengthTraceFlag = 1;
    else
        if length(unique(ns)) == 1
            SegyHeader.FixedLengthTraceFlag = 1;
        else
            SegyHeader.FixedLengthTraceFlag = 0;
        end
    end
end
%SegyHeader.FixedLengthTraceFlag = 1;


SegyHeader.dt     = dt(1);
SegyHeader.dtOrig = dt(1);

if exist('TextualFileHeader'), SegyHeader.TextualFileHeader = TextualFileHeader; end
if exist('Job') == 1, SegyHeader.Job = Job; end
if exist('Line') == 1, SegyHeader.Line = Line; end
if exist('Reel') == 1, SegyHeader.Reel = Reel; end
if exist('DataTracePerEnsemble') == 1, SegyHeader.DataTracePerEnsemble = DataTracePerEnsemble; end
if exist('AuxiliaryTracePerEnsemble') == 1, SegyHeader.AuxiliaryTracePerEnsemble = AuxiliaryTracePerEnsemble; end
if exist('ns') == 1, SegyHeader.ns = ns(1); end
if exist('nsOrig') == 1, SegyHeader.nsOrig = nsOrig(1); end
if exist('EnsembleFold') == 1, SegyHeader.EnsembleFold = EnsembleFold;  end
if exist('TraceSorting') == 1, SegyHeader.TraceSorting = TraceSorting; end
if exist('VerticalSumCode') == 1, SegyHeader.VerticalSumCode = VerticalSumCode; end

% -------------------------------------------
% SETUP SEGY TRACE HEADER
% -------------------------------------------

for k=1:ntraces
    if k == 1
        STH(k) = InitSegyTraceHeader(ns(k),dt(k));
    else
        STH(k) = STH(1);
    end
    if SegyHeader.FixedLengthTraceFlag == 1
        STH(k).ns = max(ns(k));
    else
        STH(k).ns = ns(k);
    end
    STH(k).TraceSequenceFile = k;

    % EVENT DATA
    STH(k).YearDataRecorded = SACdata(k).event.nzyear;
    STH(k).DayOfYear        = SACdata(k).event.nzjday;
    STH(k).HourOfDay        = SACdata(k).event.nzhour;
    STH(k).MinuteOfHour     = SACdata(k).event.nzmin;
    STH(k).SecondOfMinut    = SACdata(k).event.nzsec;

    % TRIMES
    try
        STH(k).dt = dt(k);
    catch
        keyboard
    end

    % STATION DATA
    STH(k).Inline3D               = SACdata(k).station.stla;
    STH(k).Crossline3D            = SACdata(k).station.stlo;
    STH(k).cdpX                   = SACdata(k).station.stla;
    STH(k).cdpY                   = SACdata(k).station.stlo;
    STH(k).ReceiverGroupElevation = SACdata(k).station.stel;
    STH(k).ReceiverDatumElevation = SACdata(k).station.stel;
    %SACdata(k).station.stdp
    %SACdata(k).station.cmpaz
    %SACdata(k).station.cmpinc
    %SACdata(k).station.kstnm
    %SACdata(k).station.kcmpnm
    %SACdata(k).station.knetwk

    Data(:,k) = SeisData(:,k);
    %if ns(k)<max(ns)
    %    Data((ns(k)+1):max(ns),k) = NaN;
    %end

end

if SegyHeader.FixedLengthTraceFlag == 1
    ins = find(ns == max(ns));
    ins = ins(1);
    SegyHeader.ns = ns(ins);
    SegyHeader.dt = dt(ins);
    for k=1:ntraces
        STH(k).ns = ns(ins);
        STH(k).dt = dt(ins);
    end
end

% WRITE SEGY STRUCTURE IF REQUESTED
if (nargin > 1) || exist('segyfile_out', 'var')
    SegyHeader = WriteSegyStructure(segyfile_out,SegyHeader,STH,Data);
end
