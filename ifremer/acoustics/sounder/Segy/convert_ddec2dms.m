% Conversion d'une latitude ou longitude degr�e/minute/second en degr�e
% d�cimaux.
%
% Syntax
%   y = convert_dms2ddec( x )
%
% Input Arguments
%   x  : double donn�es en dms concat�n�
%
% Output Arguments
%   x  : en deg d�cimaux
%
% Examples
%   x = 27445398; % 27� 44 ' 53.98''
%   x = [27445398;-27456731]
%   y = convert_dms2ddec(x)
%   x2 = convert_ddec2dms(y)
%
% See also str2lat str2lon Authors
% Authors : JMA, GLU
%--------------------------------------------------------------------------------

function tabdms = convert_ddec2dms(ddec)

% TODO : attention, v�rifier quelle convention si valeurs n�gatives :
% entiers < 0 ou val + 360 ?
dms = my_degrees2dms(ddec);
signe = sign(dms(:,1));
dms(:,1) = abs(dms(:,1));
tabdms = dms(:,1) * 1e4 + dms(:,2) * 1e2 + dms(:,3);
tabdms = signe .* tabdms;
