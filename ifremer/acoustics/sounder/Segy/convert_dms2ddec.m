% Conversion d'une latitude ou longitude degr�e/minute/second en degr�e
% d�cimaux.
%
% Syntax
%   y = convert_dms2ddec(x)
%
% Input Arguments
%   x  : double donn�es en dms concat�n�
%
% Output Arguments
%   x  : en deg d�cimaux
%
% Examples
%   x1 = 274453.98; % 27� 44 ' 53.98''
%   y1 = convert_dms2ddec(x1)
%   x2 = convert_ddec2dms(y1)
%
% See also str2lat str2lon Authors
% Authors : JMA, GLU
%--------------------------------------------------------------------------------

function ddec = convert_dms2ddec(tabdms)

tabdms = double(tabdms(:));

signe = sign(tabdms);
dms   = abs(tabdms);
d     = floor(dms / 1e4);
m     = floor((dms - d*1e4) / 1e2);
s     = (dms - d*1e4 - m*1e2);
ddec  = signe .* (d + m/60 + s/3600);
