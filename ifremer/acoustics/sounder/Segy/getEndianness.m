% D�tection de l'endianness d'un fichier SegY.
%
% Syntax
%   endian = getEndianness('toto.segy')
%
% Input Arguments
%   nomFic : nom du ficiher SegY.
%
% Output Arguments
%	endian : ieee-be ou ieee-le
%
% Examples
%   nomFic = 'F:\SonarScopeData\SEGY From JMA\Shallow Survey\line22.seg';
%   endian = getEndianness(nomFic)
%
% See also SEGYMAY
% Authors : GLU
% VERSION  : $Id: getEndianness.m,v 1.0 2011/11/21 10:03:25 rgallou Exp $
%--------------------------------------------------------------------------------
function endian = getEndianness(nomFic)
    endian = 'ieee-be';
    segyid = fopen(nomFic,'r',endian);
    
    % Lecture de la variable de d�tection du format pour conna�tre le statut Little ou Big Endian.
    fseek(segyid, 3224, 'bof');
    DataSampleFormat=fread(segyid,1,'int16');            % 3225-3326
    
    if DataSampleFormat < 1 || DataSampleFormat > 8
        endian = 'ieee-le';
    end
    
    fclose(segyid);

    