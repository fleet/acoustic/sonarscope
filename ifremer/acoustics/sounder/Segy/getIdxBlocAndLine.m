function [flag, idxBloc, idxLine, modeVisu] = getIdxBlocAndLine(NbBlocs, NbLinesByBloc, NbTraces)

flag = 0; %#ok<NASGU>
idxBloc = [];
idxLine = [];
str1 = 'Voulez-vous afficher des profils transversaux ou longitudinaux ?';
str2 = 'Do you want to display transversal or longitudinal profils ? ';

[modeVisu, flag] = my_questdlg(Lang(str1,str2), 'Init', 1, 'WaitingTime', 10, ...
    'Transversal','Longitudinal');
if ~flag || isempty(modeVisu)
    return
end
if modeVisu == 1
    str1 = ['Liste des lignes tranversales � charger (min/max) : 1/' num2str(sum(NbLinesByBloc))];
    str2 = ['Transversal lines numbers to load (min/max) : 1/' num2str(sum(NbLinesByBloc))];
    rep = ['1 ' num2str(floor(sum(NbLinesByBloc)/2)) ' ' num2str(sum(NbLinesByBloc))];
else
    str1 = ['Liste des lignes tranversales � charger (min/max) : 1/' num2str(NbTraces)];
    str2 = ['Longitudinal lines numbers to load (min/max) : 1/' num2str(NbTraces)];
    rep = ['1 ' num2str(floor(NbTraces/2)) ' ' num2str(NbTraces)];
end
% D�signation des index dans la limites des dimensions du cube.
[rep, flag] = my_inputdlg(Lang(str1,str2), rep);
if ~flag
    return
end
subLine = eval(['[' rep{1} ']']); % Lignes sismiques � extraire parmis les blocs


%% Calcul des indices de blocs et de lignes � extraire.
pppp    = cumsum(NbLinesByBloc);
idxBloc = ones(numel(subLine), 1);
% On compare item par item, l'appartenance � un sous-bloc de donn�es
% sismiques.
if modeVisu == 1
    for k=1:numel(subLine)
        for j=1:numel(pppp)
            if (subLine(k) <= pppp(j))
                idxBloc(k) = j;
                break;
            end
        end
    end
    % Calcul des indices de lignes dans les blocs respectifs.
    idxLine = zeros(numel(subLine),1);
    for k=1:numel(subLine)
        idxLine(k) = subLine(k)-(idxBloc(k)-1)*NbLinesByBloc(1);
    end
else
    idxBloc = 1:NbBlocs;
    % Les lignes restent tel quel.
    idxLine = subLine;
end


flag = 1;
