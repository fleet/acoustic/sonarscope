function [flag, nbBlocs, nbLinesByBloc] = getNbBlocsData(nomDir, nomFicSeul, nbLines, DataProcess) %#ok<INUSD>

flag            = 1;

% if DataProcess == 1
%     strStatusData = '_SBPProcess';
% else
%     strStatusData   = '';
% end

nomDirRacine    = fullfile(nomDir, 'SonarScope', nomFicSeul);
% Pour l'instant, pas de traitement SUBOP sur les multi-blocs.
listFiles       = dir(fullfile(nomDirRacine, 'Ssc_Data*.xml'));
pppp = {listFiles.name};
pppp = strfind(pppp, 'SBPProcess');
% On d�compte le fichier trait�s;
dummy = cell2mat(pppp);
if ~isempty(dummy)
    nbBlocs = numel(listFiles)-1;
else
    nbBlocs = numel(listFiles);
end

if nbBlocs == 1
    nbBlocs         = 1;
    nbLinesByBloc   = nbLines;
%     nomFicXml       = fullfile(nomDirRacine, ['Ssc_Data' strStatusData '_1.xml']);
%     data            = xml_mat_read(nomFicXml);
else
    nbLinesByBloc   = zeros(nbBlocs, 1);
    for k=1:nbBlocs
        nomFicXml = fullfile(nomDirRacine, ['Ssc_Data_' num2str(k) '.xml']);
        if exist(nomFicXml, 'file')
%             data(k)          = xml_mat_read(nomFicXml);
            nbLinesByBloc(k) = data(k).Dimensions.NbLines;
        else
            flag = 0;
            return       
        end
    end
end