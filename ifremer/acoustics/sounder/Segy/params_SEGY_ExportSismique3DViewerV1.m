function [flag, NomFicSegY, NomXML, CEau, repImport, repExport] = params_SEGY_ExportSismique3DViewerV1(Version, repImport, repExport)

persistent persistentCEau

CEau   = [];
NomXML = [];

[flag, NomFicSegY, repImport] = uiSelectFiles('ExtensionFiles', {'.seg'; '.sgy'; '.segy'}, ...
    'RepDefaut', repImport);
if ~flag
    return
end

if strcmp(Version, 'V1')
    str1 = 'Nom du r�pertoire o� seront cr��s les deux XML';
    str2 = 'Name the destination folder for the 2 XML';
    [flag, NomXML] = my_uigetdir(repExport, Lang(str1,str2));
    if ~flag
        return
    end
else
    [~, nomFic] = fileparts(NomFicSegY{1});
    Filtre = fullfile(repImport, [nomFic '.xml']);
    [flag, NomXML] = my_uiputfile({'*.xml'; '*.xml'}, Lang('Nom du fichier', 'Give a file name'), Filtre);
    if ~flag
        return
    end
    repExport = fileparts(NomXML);
end

if isempty(persistentCEau)
    CEau = 1478;
else
    CEau = persistentCEau;
end

[flag, CEau] = inputOneParametre(Lang('C�l�rit� dans l''eau', 'Sound velocity in water'), 'Value', ...
    'Value', CEau, 'Unit', 'm/s', 'MinValue', 1300, 'MaxValue', 1600);
if ~flag
    return
end

persistentCEau = CEau;
