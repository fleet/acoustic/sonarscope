function [flag, listeFic, nomSignal, repImport] = params_SEGY_PlotNavigationAndSignal(repImport)

nomSignal = [];

%% Liste des fichiers

[flag, listeFic, repImport] = uiSelectFiles('ExtensionFiles', {'.seg'; '.sgy'; '.segy'}, 'AllFilters', 1, 'RepDefaut', repImport);
if ~flag
    return
end

%% Select a signal

% TODO : r�cup�rer la liste de mani�re automatique
listeSignaux = {'Heading'; 'Height'; 'Heave'; 'Time'; 'Date'; 'Hour'; 'PingNumber'; 'Immersion'; 'Depth'; 'Tide'};
[flag, sub] = question_SelectASignal(listeSignaux);
if ~flag
    return
end
nomSignal = listeSignaux{sub};

%{
                        SegyMAT_TraceStart: [2477x1 cl_memmapfile]
                         TraceSequenceLine: [2477x1 cl_memmapfile]
                         TraceSequenceFile: [2477x1 cl_memmapfile]
                               FieldRecord: [2477x1 cl_memmapfile]
                               TraceNumber: [2477x1 cl_memmapfile]
                         EnergySourcePoint: [2477x1 cl_memmapfile]
                                       cdp: [2477x1 cl_memmapfile]
                                  cdpTrace: [2477x1 cl_memmapfile]
                   TraceIdenitifactionCode: [2477x1 cl_memmapfile]
                             NSummedTraces: [2477x1 cl_memmapfile]
                            NStackedTraces: [2477x1 cl_memmapfile]
                                   DataUse: [2477x1 cl_memmapfile]
                                    offset: [2477x1 cl_memmapfile]
                    ReceiverGroupElevation: [2477x1 cl_memmapfile]
                    SourceSurfaceElevation: [2477x1 cl_memmapfile]
                               SourceDepth: [2477x1 cl_memmapfile]
                    ReceiverDatumElevation: [2477x1 cl_memmapfile]
                      SourceDatumElevation: [2477x1 cl_memmapfile]
                          SourceWaterDepth: [2477x1 cl_memmapfile]
                           GroupWaterDepth: [2477x1 cl_memmapfile]
                           ElevationScalar: [2477x1 cl_memmapfile]
                         SourceGroupScalar: [2477x1 cl_memmapfile]
                                    GroupX: [2477x1 cl_memmapfile]
                                    GroupY: [2477x1 cl_memmapfile]
                           CoordinateUnits: [2477x1 cl_memmapfile]
                        WeatheringVelocity: [2477x1 cl_memmapfile]
                     SubWeatheringVelocity: [2477x1 cl_memmapfile]
                          SourceUpholeTime: [2477x1 cl_memmapfile]
                           GroupUpholeTime: [2477x1 cl_memmapfile]
                    SourceStaticCorrection: [2477x1 cl_memmapfile]
                     GroupStaticCorrection: [2477x1 cl_memmapfile]
                        TotalStaticApplied: [2477x1 cl_memmapfile]
                                  LagTimeA: [2477x1 cl_memmapfile]
                                  LagTimeB: [2477x1 cl_memmapfile]
                        DelayRecordingTime: [2477x1 cl_memmapfile]
                             MuteTimeStart: [2477x1 cl_memmapfile]
                               MuteTimeEND: [2477x1 cl_memmapfile]
                                        ns: [2477x1 cl_memmapfile]
                                        dt: [2477x1 cl_memmapfile]
                                  GainType: [2477x1 cl_memmapfile]
                    InstrumentGainConstant: [2477x1 cl_memmapfile]
                     InstrumentInitialGain: [2477x1 cl_memmapfile]
                                Correlated: [2477x1 cl_memmapfile]
                       SweepFrequenceStart: [2477x1 cl_memmapfile]
                         SweepFrequenceEnd: [2477x1 cl_memmapfile]
                               SweepLength: [2477x1 cl_memmapfile]
                                 SweepType: [2477x1 cl_memmapfile]
                SweepTraceTaperLengthStart: [2477x1 cl_memmapfile]
                  SweepTraceTaperLengthEnd: [2477x1 cl_memmapfile]
                                 TaperType: [2477x1 cl_memmapfile]
                      AliasFilterFrequency: [2477x1 cl_memmapfile]
                          AliasFilterSlope: [2477x1 cl_memmapfile]
                      NotchFilterFrequency: [2477x1 cl_memmapfile]
                          NotchFilterSlope: [2477x1 cl_memmapfile]
                           LowCutFrequency: [2477x1 cl_memmapfile]
                          HighCutFrequency: [2477x1 cl_memmapfile]
                               LowCutSlope: [2477x1 cl_memmapfile]
                              HighCutSlope: [2477x1 cl_memmapfile]
                              TimeBaseCode: [2477x1 cl_memmapfile]
                          TimeBaseCodeText: [2477x3 char]
                     TraceWeightningFactor: [2477x1 cl_memmapfile]
                  GeophoneGroupNumberRoll1: [2477x1 cl_memmapfile]
    GeophoneGroupNumberFirstTraceOrigField: [2477x1 cl_memmapfile]
     GeophoneGroupNumberLastTraceOrigField: [2477x1 cl_memmapfile]
                                   GapSize: [2477x1 cl_memmapfile]
                                OverTravel: [2477x1 cl_memmapfile]
                                      cdpX: [2477x1 cl_memmapfile]
                                      cdpY: [2477x1 cl_memmapfile]
                                  Inline3D: [2477x1 cl_memmapfile]
                               Crossline3D: [2477x1 cl_memmapfile]
                                 ShotPoint: [2477x1 cl_memmapfile]
                           ShotPointScalar: [2477x1 cl_memmapfile]
                 TraceValueMeasurementUnit: [2477x1 cl_memmapfile]
             TraceValueMeasurementUnitText: [2477x9 char]
              TransductionConstantMantissa: [2477x1 cl_memmapfile]
                 TransductionConstantPower: [2477x1 cl_memmapfile]
                          TransductionUnit: [2477x1 cl_memmapfile]
                           TraceIdentifier: [2477x1 cl_memmapfile]
                         ScalarTraceHeader: [2477x1 cl_memmapfile]
                                SourceType: [2477x1 cl_memmapfile]
             SourceEnergyDirectionMantissa: [2477x1 cl_memmapfile]
             SourceEnergyDirectionExponent: [2477x1 cl_memmapfile]
                 SourceMeasurementMantissa: [2477x1 cl_memmapfile]
                 SourceMeasurementExponent: [2477x1 cl_memmapfile]
                     SourceMeasurementUnit: [2477x1 cl_memmapfile]
                              MilliSeconds: [2477x1 cl_memmapfile]
                            UnassignedInt1: [2477x1 cl_memmapfile]
                            UnassignedInt2: [2477x1 cl_memmapfile]
                    SegyMAT_TraceDataStart: [2477x1 cl_memmapfile]
                    %}
                    