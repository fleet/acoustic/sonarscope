% TODO : cette fonction devrait devenir une m�thode d'une classe cl_segy

function [flag, Data] = readHeightSegyInCache(nomFic)

flag = 0;
Data = [];

%% Lecture du r�pertoire SonarScope

[nomDirRacine, nomFic] = fileparts(nomFic);

nomDirRacine = fullfile(nomDirRacine, 'SonarScope');
if ~exist(nomDirRacine, 'dir')
    return
end

nomDirRacine = fullfile(nomDirRacine, nomFic);
if ~exist(nomDirRacine, 'dir')
    return
end


%% Lecture du fichier XML d�crivant la donn�e

NomFicXML = fullfile(nomDirRacine, 'Ssc_Height.xml');
if ~exist(NomFicXML, 'file')
    return
end
Data = xml_read(NomFicXML);

%% Lecture des fichiers binaires des signaux

nomDirRoot = nomDirRacine;
for i=1:length(Data.Signals)
    [flag, Signal] = readSignal(nomDirRoot, Data.Signals(i), Data.Dimensions.nbPings);
    if ~flag
        return
    end
    Data.(Data.Signals(i).Name) = Signal;
end

