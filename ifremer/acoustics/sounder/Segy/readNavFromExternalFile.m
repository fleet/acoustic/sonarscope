% R�cup�ration des donn�es de navigation annexes rempla�ant les
% donn�es internes.
%
% Syntax
%   [flag, Nav] = readNavFromExternalFile(a, nomFicNav) 
%
% Input Arguments
%   a : instance de type cl_segy
%   nomFicNav : nom de fichier de navigation externe.
% 
% Output Arguments
%   flag : indicateur de bon fonctionnement
%   Nav : structure comportant les Latitudes, Longitudes, l'unit� de
%   coordonn�es correspondantes (3 = degr�s d�cimaux en Segy Rev1).
%
% Examples 
%   TODO
%
% See also cl_image Authors
% Authors : JMA
% ----------------------------------------------------------------------------
function [flag, Nav] = readNavFromExternalFile(nomFicNav)


    % Tentative de lecture de la nav en Latitude/Longitude :
    % (txt id hemisphere lat cardinale E-O lon)
    strFR = 'La position est-elle donn�e en Lat-Lon ?';
    strUS = 'Is the position give in Lat-Lon format ?';
    [rep, flag] = my_questdlg(Lang(strFR, strUS), 'Init', 2, 'WaitingTime', 10);
    if ~flag
        return
    end
    if rep == 1
%         [flag, Nav.Longitude, Nav.Latitude] = readNavInLatLonFmt(nomFicNav);
        fidi = fopen(nomFicNav);
        data = textscan(4, '%f%f%*d%*s%*[^\n]', 'HeaderLines', 1, 'EndOfLine','\n', 'EmptyValue', NaN, 'ExpChars', 'eEdD', ...
            'Delimiter', {'\t',',',' ',';'}, 'MultipleDelimsAsOne', 1);
        fclose(fidi);
        flag = 0;
        Lat = data{1};
        Lon = data{2};
        %Nav.coordUnits                      = ones(length(Nav.Longitude),1)*3; % En degr�es d�cimaux
    elseif rep == 2
        % Tentative de lecture de la nav en UTM
        % (txt id X(m) Y(m))
        % Saisie de la zone UTM en question
        [flag, X, Y]    = readNavInUTMFmt(nomFicNav);
        % Ancienne m�thode.
        [flag, Carto] = editParams(cl_carto(), [], [], 'MsgInit');
        
        [Lat, Lon] = xy2latlon(Carto, X(:), Y(:));
        % Nouvelle m�thode : D�codage gr�ce � la Mapping Toolbox.
        % Zone = utmzoneui;
        % utmstruct       = defaultm('utm'); 
        % utmstruct.zone  = Zone; 
        % utmstruct.geoid = wgs84Ellipsoid; 
        % utmstruct       = defaultm(utmstruct);
        % [Lat, Lon]      = minvtran(utmstruct, X(:), Y(:));
        % figure; PlotUtils.createSScPlot(lon, lat, '*'); title('Navigation UTM');
    end
    % Conservation des coordonn�es en Lat-Long.
    Nav.Latitude    = Lat;
    Nav.Longitude   = Lon;
    Nav.coordUnits  = ones(length(Lat),1)*3; % En degr�es d�cimaux
    if ~flag           
        return
    end

