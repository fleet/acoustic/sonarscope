function [flag, Longitude, Latitude] = readNavInLatLonFmt(nomFicNav)

flag = 1;

fid = fopen(nomFicNav);
M = textscan(fid, '%s %d %s %f %s %f');
fclose(fid);

% Signe des Latitudes.
sub = strcmp(M{3},'S');
M{4}(sub) = -1*M{4}(sub);
Latitude = M{4};

% Signe des Longitudes.
sub = strcmp(M{5},'W');
M{6}(sub) = -1*M{6}(sub);
Longitude = M{6};

