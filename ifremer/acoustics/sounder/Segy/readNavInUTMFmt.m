function [flag, X, Y] = readNavInUTMFmt(nomFicNav)

flag = 1;

A = importdata(nomFicNav);
if isempty(A.data)

    fid = fopen(nomFicNav);
    M = textscan(fid, '%s %f %f %f');
    fclose(fid);

    % Signe des Latitudes.
    X = M{3};

    % Signe des Longitudes.
    Y = M{4};
else
    % Signe des Latitudes.
    X = A.data(:,1);

    % Signe des Longitudes.
    Y = A.data(:,2);    
end
