function [flag, Longitude, Latitude] = readNavSegyFile(nomFicNav, varargin)

[varargin, charDelimiter] = getPropertyValue(varargin, 'delimiter', '');
[varargin, NbLinesHeader] = getPropertyValue(varargin, 'NbLinesHeader', 0); %#ok<ASGLU>


NLignesTotal = countLines( nomFicNav );
NbPings      = NLignesTotal - NbLinesHeader;
flag = 1;

fid = fopen(nomFicNav);
M = textscan(fid, '%s %d %s %f %s %f', 'delimiter', charDelimiter);
fclose(fid);

idLat = 4;
idLon = 6;
if size(M, 2) <=6
    % Exemple de format de Nav (Marie Picot):
    % $APSEC	08/11/1998	10:30:43.000	NATAM	S	-6.333159167	E	8.000536	1
    fid = fopen(nomFicNav);
    M = textscan(fid, '%s %s %s %s %s %f %s %f %d', NbPings, 'delimiter', charDelimiter, 'MultipleDelimsAsOne', 1);
    fclose(fid);
    idLat = 6;
    idLon = 8;
end

% Signe des Latitudes.
sub = strcmp(M{idLat-1},'S');
M{idLat}(sub) = -1*M{idLat}(sub);
Latitude = M{idLat};

% Signe des Longitudes.
sub = strcmp(M{idLon-1},'W');
M{idLon}(sub) = -1*M{idLon}(sub);
Longitude = M{idLon};
