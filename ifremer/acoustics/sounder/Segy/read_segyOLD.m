% nomFicSegy = 'C:\Temp\SubbottomData\AUV0001_D20091123_T050935_process.seg';
% [flag, seismic] = read_segy(nomFicSegy)

function [flag, seismic] = read_segyOLD(nomFicSegy, varargin)

[varargin, FirstTr] = getPropertyValue(varargin, 'FirstTr', 1);
[varargin, LastTr]  = getPropertyValue(varargin, 'LastTr',  Inf);
[varargin, NbEch]   = getPropertyValue(varargin, 'NbEch',   Inf); %#ok<ASGLU>

%{
% Tentative de remplacement de litsegy par read_segy_file
% Le pb est que l'on ne trouve pas les headers de date et heure, ...
[Seismic, text_header, binary_header] = read_segy_file(nomFicSegy); %#ok<ASGLU>

%% Recherche des coordonnées

for k=1:size(Seismic.header_info, 1)
k
Seismic.header_info{k,1}
end
for k=1:size(Seismic.header_info, 1)
flag = strfind(Seismic.header_info{k,1}, 'cdp_x');
if flag
idx(1) = k;
end
flag = strfind(Seismic.header_info{k,1}, 'cdp_y');
if flag
idx(2) = k;
end
end

C = Seismic.headers(idx,:);
Deg = floor(C / 1e6);
C = C - Deg * 1e6;
Minute = floor(C / 1e4);
C = C - Minute * 1e4;
Second = C / 100;
Coor = Deg + Minute/60 + Second/3600;
Data.Latitude  = reshape(Coor(2,:), [ny nx]);
Data.Longitude = reshape(Coor(1,:), [ny nx]);
% SonarScope(Lat)
% SonarScope(Lon)
%}


[EBCReelHeader, BinReelHeader, BinTraceHeader, TraceData] = litsegy(nomFicSegy, FirstTr, LastTr, NbEch);

%{
Names = fieldnames(BinTraceHeader);
for k=1:length(Names)
k
figure(57452); plot([BinTraceHeader.(Names{k})]); title(Names{k}, 'Interpreter', 'none')
pause
end
%}

if isempty(EBCReelHeader)
    flag = 0;
    return
end

seismic.name    = 'TODO'; % Seismic.name
seismic.from    = 'TODO'; % Seismic.from

Year   = [BinTraceHeader.YearDataRecorded]'; % Seismic.?????
Day    = [BinTraceHeader.Day]'; % Seismic.?????
Hour   = [BinTraceHeader.Hour]'; % Seismic.?????
Minute = [BinTraceHeader.Minute]'; % Seismic.?????
Second = [BinTraceHeader.Second]'; % Seismic.?????

seismic.Date        = dayJma2Ifr(1, 1, Year) + (Day-1);
seismic.Hour        = hourHms2Ifr(Hour, Minute, Second);
seismic.PingNumber  = [BinTraceHeader.TraceSequenceLine]';
seismic.Tide        = zeros(size(seismic.PingNumber), 'single');

seismic.LatitudeNadir  = convertPosition([BinTraceHeader.YSourceCoordinate]');
seismic.LongitudeNadir = convertPosition([BinTraceHeader.XSourceCoordinate]');

seismic.LatitudeTop    = seismic.LatitudeNadir;
seismic.LongitudeTop   = seismic.LongitudeNadir;

seismic.TimeStartRecording = [BinTraceHeader.LagTimeShotRecording]' / 1000;


seismic.DepthTop = [BinTraceHeader.SourceSurfaceElevation]' ./ [BinTraceHeader.ScalarForElevationDepth]';
C = [BinTraceHeader.SubWeatheringVelocity]';
k = strfind(EBCReelHeader, 'ACQUISITION FREQUENCY :') + 24;
fe = str2double(EBCReelHeader(k:k+4));

seismic.DepthBottom = seismic.DepthTop - C .* [BinTraceHeader.NumberOfSamples]' ./ (2 * fe);

seismic.traces  = TraceData;

seismic.EBCReelHeader = EBCReelHeader;
flag = 1;

function y = convertPosition(x)

Signe = sign(x);
x = abs(x);
Deg = floor(x ./ 1e6);
x = x - Deg * 1e6;
Min = floor(x ./ 1e4);
CentiSec = x - Min .* 1e4;
y = Signe .* (Deg + Min./60 + CentiSec./360000);
