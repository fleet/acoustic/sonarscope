%**************************************************************************
% function [header] = sbp_header(b,c)
%--------------------------------------------------------------------------
% programme pour l'exploitation des donn�es des SDS de l'ifremer
%--------------------------------------------------------------------------
% �criture des donn�es des ent�tes du fichier segy dans la structure
% "header"
%
% INPUT  : 
% - b : BinaryHeader du fichier SEGY : 400 bytes
% - c : TraceHeader du fichier SEGY : 240 bytes pour chaque trace
%
% OUTPUT : header, structure qui contient : 
%   - PingNumber:      num�ro de tir 
%   - LineNumber:      num�ro de profil
%   - RxZOffset:       profondeur du r�cepteur (m)
%   - MRUDepth:        profondeur de la r�f�rence (m)
%   - TxZOffset:       profondeur de la source (m)
%   - WaterDepthAtTx:  profondeur d'eau � l'�mission (m)
%   - WaterDepthAtRx:  profondeur d'eau � la r�ception (m)
%   - TxLon:           longitude � l'�mission (�)
%   - TxLat:           latitude � l'�mission (�)
%   - RxLon:           longitude � la r�ception (�)
%   - RxLat:           latitude � la r�ception (�)
%   - RecordingLag:    d�lai d'enregistrement (ms)
%   - CdpLon:          longitude du point milieu (�)
%   - CdpLat:          latitude du point milieu (�)
%   - WaterVelocity:   vitesse de propagation dans l'eau (m/s)
%   - SampleFreq:      fr�quence d'acquisition (Hz)
%   - LowCutFrq:       filtre passe haut � la r�ception (Hz)
%   - HighCutFrq:      filtre passe bas � la r�ception (Hz)
%   - LowCutSlope:     pente du filtre passe haut (dB/octave)
%   - HighCutSlope:    pente du filtre passe bas (dB/octave)
%   - Year:            ann�e
%   - Day:             jour (jour julian)
%   - Time:            temps depuis minuit (s)
%   - RxGain:          gain en r�ception (gain multiplicatif)
%   - TxLevel:         niveau d'�mission (%)
%   - CompenseMvt:     compensation des mouvements du navire (�s)
%   - StartChirpFrq:   fr�quence de d�but de chirp (Hz)
%   - EndChirpFrq:     fr�quence de fin de chirp (Hz)
%   - ChirpLength:     dur�e du chirp (ms)
%   - NbSamples:       nombre d'�chantillons par tir
%   - NbPings:         nombre de tirs
%**************************************************************************
% ifremer - pacault - 17/01/2012
% % Modif :
% GLU le 06/06/2012 pour int�grer les calculs avec le format en sortie de
% SEGYMAT avec d�codage par xtf2ssc.m
%**************************************************************************

function header = sbp_header(b,c)

header.PingNumber       = c.TraceSequenceLine(:); % c.TraceSequenceLine;
header.LineNumber       = b.Line(:); % b.LineNumber; 
header.RxZOffset        = c.ReceiverGroupElevation(:)/100; % c.ReceiverElevation/100;
header.MRUDepth         = double(c.SourceSurfaceElevation(:)/100);
header.SourceDepth      = c.SourceDepth(:)/100;
header.WaterDepthAtTx   = c.SourceWaterDepth(:)/100; % c.WaterDepthAtSource/100;
header.WaterDepthAtRx   = c.GroupWaterDepth(:)/100; % c.WaterDepthAtReceiver/100;
header.TxLon            = convert_dms2ddec(c.SourceX(:)); % convert_dms2ddec(c.XSourceCoordinate);
header.TxLat            = convert_dms2ddec(c.SourceY(:)); % convert_dms2ddec(c.YSourceCoordinate);
header.RxLon            = convert_dms2ddec(c.GroupX(:)); % convert_dms2ddec(c.XReceiverCoordinate); 
header.RxLat            = convert_dms2ddec(c.GroupY(:)); %convert_dms2ddec(c.YReceiverCoordinate);
header.RecordingLag     = c.DelayRecordingTime(:); %c.LagTimeShotRecording;
header.CdpLon           = convert_dms2ddec(c.cdpX(:)); % convert_dms2ddec(c.XcdpCoordinate);
header.CdpLat           = convert_dms2ddec(c.cdpY(:)); % convert_dms2ddec(c.XcdpCoordinate);
header.WaterVelocity    = c.WeatheringVelocity(1); %c.WeatheringVelocity(1);
header.SampleFreq       = double(1e6/b.dt(:)); % 1e6/b.ReelSampleInterval;
header.LowCutFrq        = c.LowCutFrequency(1); % c.LowCutFrq(1);
header.HighCutFrq       = c.HighCutFrequency(1);% c.HighCutFrq(1);
header.LowCutSlope      = c.LowCutSlope(1);% c.LowCutSlope(1);
header.HighCutSlope     = c.HighCutSlope(1);% c.HighCutSlope(1);
header.Year             = c.YearDataRecorded(:); %c.YearDataRecorded
header.Day              = c.DayOfYear(:); %c.Day
header.Time             = c.HourOfDay(:)*60*60 + ...
                          c.MinuteOfHour(:)*60 + ...
                          c.SecondOfMinute(:) + ...
                          c.MilliSeconds(:)/1000; 
% c.Hour*60*60 + ...
%                           c.Minute*60 + ...
%                           c.Second + ...
%                           c.Millisecond/1000; 
header.RxGain           = double(10^(c.InstrumentGainConstant(1)/20)); %10^(c.InstrumentGainConstant(1)/20);
header.TxLevel          = double(c.SourceMeasurementMantissa(1) * 10^(c.SourceMeasurementExponent(1)));
% c.SourceMeasurementMantisse(1) * 10^(c.SourceMeasurementExponent(1));
header.CompenseMvt      = c.CompenseMvt(:); % 
header.StartChirpFrq    = double(b.SweepFrequencyStart(:)); % b.StartSweepFrq;
header.EndChirpFrq      = double(b.SweepFrequencyEnd(:)); %b.EndSweepFrq;
header.ChirpLength      = double(b.SweepLength(:));
header.NbSamples        = b.ns(:); % b.ReelSamplesPerTrace;
header.NbPings          = c.Dimensions.NbTraceHeaders; % size(c.TraceSequenceLine,2);


%% Fonction pour convertir les degr�s, minutes, secondes en d�gr�s d�cimaux

function ddec = convert_dms2ddec(dms)
signe   = sign(dms);
dms     = abs(dms);
d       = floor(dms/1e6);
m       = floor((dms - d*1e6) /1e4);
s       = (dms - d*1e6 - m*1e4) /1e2;
ddec    = signe .*(d + m/60 + s/3600);
