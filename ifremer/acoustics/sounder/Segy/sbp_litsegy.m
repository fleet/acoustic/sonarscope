function [TextHeader,BinaryHeader,ExtendedHeader,TraceHeader,Data] = sbp_litsegy(fname)
%**************************************************************************
% function [TextHeader,BinaryHeader,ExtendedHeader,TraceHeader,Data] = sbp_litsegy(fname)
%--------------------------------------------------------------------------
% programme pour l'exploitation des données des SDS de l'ifremer
%--------------------------------------------------------------------------
% Fonction qui permet de lire un fichier SEGY.
% Hypothèse : toutes les traces ont le même nombre d'échantillons.
%
% INPUT :
% - fname : nom du fichier à lire
%
% OUTPUT :
% - TextHeader: 3200 bytes (40 lignes de 80 caractères, en ASCII ou EBCDIC)
% - BinaryHeader: 400 bytes
% - ExtendedHeader : n*3200 bytes (option)
% - TraceHeader : 240 bytes pour chaque trace
% - Data : données au format IBM, float32, int8, int16 ou int32
%**************************************************************************
% ifremer - pacault - 17/01/2012
%**************************************************************************

fid = fopen(fname,'rb');
if (fid==-1)
   disp(['Le fichier ' fname ' n''existe pas']);
   return;
end
Tout = fread(fid,inf,'*uint8');
fclose(fid);

%==========================================================================
% On fait un test pour savoir si les données sont écrites en big/little-endian.
% Les octets 3225:3226 du header ont une valeur comprise entre 1 et 8 (format).
% Si l'octet 3225 vaut 0 => le fichier est big-endian.
test = Tout(3225:3226);
if (test(1)==0 && test(2)==0)
    disp('Format du fichier inconnu ou non indiqué');
    return;
elseif (test(1)==0)   
    Endianness = 0; endian = 'ieee-be'; %big-endian
else
    Endianness = 1; endian = 'ieee-le'; %little-endian
end
 
%==========================================================================
% Lecture du Text Header
TextHeader = decode_text_header(Tout(1:3200)); 

%==========================================================================
% Lecture du Binary Header
BinaryHeader = decode_binary_header(Tout(3200+1:3200+400),Endianness);
Tout(1:3200+400) = [];

%==========================================================================
% Lecture d'un éventuel Extended Header 
if BinaryHeader.NbOfTextualHeader ~=0
    ExtendedHeader = decode_text_header(Tout(1:3200*BinaryHeader.NbOfTextualHeader));
    Tout(1:3200*BinaryHeader.NbOfTextualHeader) = [];
else ExtendedHeader = '';
end

%==========================================================================
% Détermination de la taille des données
if BinaryHeader.FixedLengthFlag~=1
    disp('Hypothèse pour la lecture du fichier : toutes les traces ont le même nombre d''échantillons');
    disp('Pour ce fichier : BinaryHeader.FixedLengthFlag = 0');
end

NbEch   = BinaryHeader.ReelSamplesPerTrace;
Tailles = [4 4 2 4 4 0 0 1];
NbTr    = length(Tout)/(240+Tailles(BinaryHeader.DataSampleFormat)*NbEch);
pbsize  = 0;
if NbTr~=floor(NbTr)
    pbsize = 1; NbTr = floor(NbTr); Tout((240+Tailles(BinaryHeader.DataSampleFormat)*NbEch)*NbTr+1:end) = []; 
end
%==========================================================================
% Lecture des Trace Header  
Tout        = reshape(Tout,length(Tout)/NbTr,NbTr);
TraceHeader = decode_trace_header(Tout(1:240,:),Endianness);
clear Tout;

%==========================================================================
% Lecture des Données  
fid = fopen(fname,'rb',endian);
if (fid==-1)
    disp(['Problème lors de la réouverture du fichier ' fname]);
   return;
end
fseek(fid,3200*(1+BinaryHeader.NbOfTextualHeader)+400,'bof');

if     (BinaryHeader.DataSampleFormat==5)	%float32
    Data = fread(fid,inf,'float32');

elseif (BinaryHeader.DataSampleFormat==2)	%int32
    Data = fread(fid,inf,'int32');

elseif (BinaryHeader.DataSampleFormat==3)	%int16
    Data = fread(fid,inf,'int16');

elseif (BinaryHeader.DataSampleFormat==8)	%int8
    Data = fread(fid,inf,'int8');

elseif (BinaryHeader.DataSampleFormat==1)	%IBM
    octets = fread(fid,Tailles(BinaryHeader.DataSampleFormat)*Binary.ReelSamplesPerTrace,'*uint8');
    Data = IBM2double(octets,Endianness);
else			
  disp(['On n''a pas encore codé le format ' num2str(BinReelHeader.DataSampleFormat)]);
  return; 		
end
fclose(fid);

if pbsize==1, Data((240/Tailles(BinaryHeader.DataSampleFormat)+NbEch)*NbTr+1:end) = []; end
Data = reshape(Data,length(Data)/NbTr, NbTr); 
Data(1:240/Tailles(BinaryHeader.DataSampleFormat),:) = [];


%==========================================================================
% Lecture du Text Header (ou de l'Extended Header) du fichier
function [TextHeader] = decode_text_header(Tout)
EBCDIC_To_ASCII = [0  ,1  ,2  ,3  ,4  ,5  ,6  ,7  ,8  ,9  ,10 ,11 ,12 ,13 ,14 ,15 ,...
   					 16 ,17 ,18 ,19 ,20 ,21 ,22 ,23 ,24 ,25 ,26 ,27 ,28 ,29 ,30 ,31 ,...
                   32 ,33 ,34 ,35 ,36 ,37 ,38 ,39 ,40 ,41 ,42 ,43 ,44 ,45 ,46 ,47 ,...
                   46 ,46 ,50 ,51 ,52 ,53 ,54 ,55 ,56 ,57 ,58 ,59 ,60 ,61 ,46 ,63 ,...
                   32 ,46 ,46 ,46 ,46 ,46 ,46 ,46 ,46 ,46 ,46 ,46 ,60 ,40 ,43 ,124,...
                   38 ,46 ,46 ,46 ,46 ,46 ,46 ,46 ,46 ,46 ,33 ,36 ,42 ,41 ,59 ,94 ,...
                   45 ,47 ,46 ,46 ,46 ,46 ,46 ,46 ,46 ,46 ,124,44 ,37 ,95 ,62 ,63 ,...
                   46 ,46 ,46 ,46 ,46 ,46 ,46 ,46 ,46 ,46 ,58 ,35 ,64 ,39 ,61 ,34 ,...
                   46 ,97 ,98 ,99 ,100,101,102,103,104,105,46 ,46 ,46 ,46 ,46 ,46 ,...
                   46 ,106,107,108,109,110,111,112,113,114,46 ,46 ,46 ,46 ,46 ,46 ,...
                   46 ,126,115,116,117,118,119,120,121,122,46 ,46 ,46 ,91 ,46 ,46 ,...
                   46 ,46 ,46 ,46 ,46 ,46 ,46 ,46 ,46 ,46 ,46 ,46 ,46 ,93 ,46 ,46 ,...
                   123,65 ,66 ,67 ,68 ,69 ,70 ,71 ,72 ,73 ,46 ,46 ,46 ,46 ,46 ,46 ,...
                   125,74 ,75 ,76 ,77 ,78 ,79 ,80 ,81 ,82 ,46 ,46 ,46 ,46 ,46 ,46 ,...
                   92 ,46 ,83 ,84 ,85 ,86 ,87 ,88 ,89 ,90 ,46 ,46 ,46 ,46 ,46 ,46 ,...
                   48 ,49 ,50 ,51 ,52 ,53 ,54 ,55 ,56 ,57 ,46 ,46 ,46 ,46 ,46 ,46];
             
% Test pour savoir si le fichier est ASCII ou EBCDIC.
% Dans la plupart des cas, le header texte commence par C.
% Si on ne trouve pas un C en EBCDIC, on suppose que le header est en ASCII.
if char(EBCDIC_To_ASCII(Tout(1)+1))=='C'
    Tout = EBCDIC_To_ASCII(Tout+1); 
end
TextHeader = reshape(Tout,80,length(Tout)/80);
TextHeader = char(TextHeader(:)');
return

%==========================================================================
% Lecture du Binary Header du fichier.
function [BinaryHeader] = decode_binary_header(Tout,Endianness)

BinaryHeader.JobId                  = ChampInt32(Tout(  1:  4),Endianness);
BinaryHeader.LineNumber             = ChampInt32(Tout(  5:  8),Endianness);
BinaryHeader.ReelNumber             = ChampInt32(Tout(  9: 12),Endianness);
BinaryHeader.DataTracesPerRecord	= ChampInt16(Tout( 13: 14),Endianness); 
BinaryHeader.AuxTracesPerRecord     = ChampInt16(Tout( 15: 16),Endianness);
BinaryHeader.ReelSampleInterval     = ChampInt16(Tout( 17: 18),Endianness);
BinaryHeader.FieldSampleInterval	= ChampInt16(Tout( 19: 20),Endianness);
BinaryHeader.ReelSamplesPerTrace	= ChampInt16(Tout( 21: 22),Endianness);
BinaryHeader.FieldSamplesPerTrace	= ChampInt16(Tout( 23: 24),Endianness);
BinaryHeader.DataSampleFormat		= ChampInt16(Tout( 25: 26),Endianness); %1 = 32-bit IBM floating point
                                                                            %2 = 32-bit fixed-point (integer)
                                                                            %3 = 16-bit fixed-point (integer)
                                                                            %4 = 32-bit fixed-point with gain code (integer)
                                                                            %5 = 32-bit floating point (IEEE)
                                                                            %6 = not currently used
                                                                            %7 = not currently used
                                                                            %8 = 8-bit fixed-point (integer)
BinaryHeader.CDPFold				= ChampInt16(Tout( 27: 28),Endianness);
BinaryHeader.TraceSorting			= ChampInt16(Tout( 29: 30),Endianness);
BinaryHeader.VerticalSumming		= ChampInt16(Tout( 31: 32),Endianness);
BinaryHeader.StartSweepFrq			= ChampInt16(Tout( 33: 34),Endianness);
BinaryHeader.EndSweepFrq			= ChampInt16(Tout( 35: 36),Endianness);
BinaryHeader.SweepLength			= ChampInt16(Tout( 37: 38),Endianness);
BinaryHeader.SweepType				= ChampInt16(Tout( 39: 40),Endianness); % 1 = linear, 2 = parabolic, 3 = exponential, 4 = other
BinaryHeader.SweepChannelIndex		= ChampInt16(Tout( 41: 42),Endianness);
BinaryHeader.SweepStartTaper		= ChampInt16(Tout( 43: 44),Endianness);
BinaryHeader.SweepEndTaper			= ChampInt16(Tout( 45: 46),Endianness);
BinaryHeader.TaperType				= ChampInt16(Tout( 47: 48),Endianness); % 1 = linear, 2 = cos square, 3 = other
BinaryHeader.CorrelatedTraces		= ChampInt16(Tout( 49: 50),Endianness); % 1 = no, 2 = yes
BinaryHeader.BinaryGainRecovered	= ChampInt16(Tout( 51: 52),Endianness); % 1 = yes, 2 = no
BinaryHeader.AmplitudeRecovery		= ChampInt16(Tout( 53: 54),Endianness);
BinaryHeader.MeasurementSystem		= ChampInt16(Tout( 55: 56),Endianness);
BinaryHeader.SignalPolarity         = ChampInt16(Tout( 57: 58),Endianness);
BinaryHeader.VibratoryPolarity		= ChampInt16(Tout( 59: 60),Endianness);
BinaryHeader.UnAssigned1            = Tout(61:300);
BinaryHeader.SegyRevNumber          = ChampInt16(Tout(301:302),Endianness);
BinaryHeader.FixedLengthFlag        = ChampInt16(Tout(303:304),Endianness);
BinaryHeader.NbOfTextualHeader      = ChampInt16(Tout(305:306),Endianness);
BinaryHeader.UnAssigned2            = Tout(307:400);
return 

%==========================================================================
% Lecture du Trace Header du fichier.
function [TraceHeader] = decode_trace_header(Tout,Endianness)

TraceHeader.TraceSequenceLine           = ChampInt32(Tout(  1:  4,:),Endianness);
TraceHeader.TraceSequenceReel           = ChampInt32(Tout(  5:  8,:),Endianness);
TraceHeader.FieldRecordNumber           = ChampInt32(Tout(  9: 12,:),Endianness);
TraceHeader.TraceSequenceRecord         = ChampInt32(Tout( 13: 16,:),Endianness);
TraceHeader.ShotPointNumber             = ChampInt32(Tout( 17: 20,:),Endianness);
TraceHeader.EnsembleNumber              = ChampInt32(Tout( 21: 24,:),Endianness);
TraceHeader.TraceSequenceCDP            = ChampInt32(Tout( 25: 28,:),Endianness);
TraceHeader.TraceIdentificationCode     = ChampInt16(Tout( 29: 30,:),Endianness);
TraceHeader.NumberVerticalSummedTr      = ChampInt16(Tout( 31: 32,:),Endianness);
TraceHeader.NumberHorizontalStackedTr   = ChampInt16(Tout( 33: 34,:),Endianness);
TraceHeader.DataUse                     = ChampInt16(Tout( 35: 36,:),Endianness);
TraceHeader.DistanceSPtoReceiver        = ChampInt32(Tout( 37: 40,:),Endianness);
TraceHeader.ReceiverElevation           = ChampInt32(Tout( 41: 44,:),Endianness);
TraceHeader.SourceSurfaceElevation      = ChampInt32(Tout( 45: 48,:),Endianness);
TraceHeader.SourceDepth                 = ChampInt32(Tout( 49: 52,:),Endianness);
TraceHeader.DatumElevationAtReceiver    = ChampInt32(Tout( 53: 56,:),Endianness);
TraceHeader.DatumElevationAtSource      = ChampInt32(Tout( 57: 60,:),Endianness);
TraceHeader.WaterDepthAtSource          = ChampInt32(Tout( 61: 64,:),Endianness);
TraceHeader.WaterDepthAtReceiver        = ChampInt32(Tout( 65: 68,:),Endianness);
TraceHeader.ScalarForElevationDepth     = ChampInt16(Tout( 69: 70,:),Endianness);	% + = multiplier, - = divisor
TraceHeader.ScalarForCoordinates        = ChampInt16(Tout( 71: 72,:),Endianness);	% + = multiplier, - = divisor
TraceHeader.XSourceCoordinate           = ChampInt32(Tout( 73: 76,:),Endianness);
TraceHeader.YSourceCoordinate           = ChampInt32(Tout( 77: 80,:),Endianness);
TraceHeader.XReceiverCoordinate         = ChampInt32(Tout( 81: 84,:),Endianness);
TraceHeader.YReceiverCoordinate         = ChampInt32(Tout( 85: 88,:),Endianness);
TraceHeader.CoordinateUnits             = ChampInt16(Tout( 89: 90,:),Endianness);    %1=length in meters or feet, 2=arc seconds
TraceHeader.WeatheringVelocity          = ChampInt16(Tout( 91: 92,:),Endianness);
TraceHeader.SubWeatheringVelocity       = ChampInt16(Tout( 93: 94,:),Endianness);
TraceHeader.UpholeTimeAtSource          = ChampInt16(Tout( 95: 96,:),Endianness);
TraceHeader.UpholeTimeAtReceiver        = ChampInt16(Tout( 97: 98,:),Endianness);
TraceHeader.SourceStaticCorrection      = ChampInt16(Tout( 99:100,:),Endianness);
TraceHeader.ReceiverStaticCorrection    = ChampInt16(Tout(101:102,:),Endianness);
TraceHeader.TotalStaticCorrection       = ChampInt16(Tout(103:104,:),Endianness);
TraceHeader.LagTimeHeaderBreak          = ChampInt16(Tout(105:106,:),Endianness);
TraceHeader.LagTimeBreakShot            = ChampInt16(Tout(107:108,:),Endianness);
TraceHeader.LagTimeShotRecording        = ChampInt16(Tout(109:110,:),Endianness);
TraceHeader.StartMuteTime               = ChampInt16(Tout(111:112,:),Endianness);
TraceHeader.EndMuteTime                 = ChampInt16(Tout(113:114,:),Endianness);
TraceHeader.NumberOfSamples             = ChampInt16(Tout(115:116,:),Endianness);
TraceHeader.SampleInterval              = ChampInt16(Tout(117:118,:),Endianness);
TraceHeader.InstrumentGainCode          = ChampInt16(Tout(119:120,:),Endianness);
TraceHeader.InstrumentGainConstant      = ChampInt16(Tout(121:122,:),Endianness);
TraceHeader.InstrumentGainDB            = ChampInt16(Tout(123:124,:),Endianness);
TraceHeader.Correlated                  = ChampInt16(Tout(125:126,:),Endianness);	%1=no, 2=yes
TraceHeader.StartSweepFrq               = ChampInt16(Tout(127:128,:),Endianness);
TraceHeader.EndSweepFrq                 = ChampInt16(Tout(129:130,:),Endianness);
TraceHeader.SweepLength                 = ChampInt16(Tout(131:132,:),Endianness);
TraceHeader.SweepType                   = ChampInt16(Tout(133:134,:),Endianness);
TraceHeader.SweepStartTaper             = ChampInt16(Tout(135:136,:),Endianness);
TraceHeader.SweepEndTaper               = ChampInt16(Tout(137:138,:),Endianness);
TraceHeader.TaperType                   = ChampInt16(Tout(139:140,:),Endianness);
TraceHeader.AliasFilterFrq              = ChampInt16(Tout(141:142,:),Endianness);
TraceHeader.AliasFilterSlope            = ChampInt16(Tout(143:144,:),Endianness);
TraceHeader.NotchFilterFrq              = ChampInt16(Tout(145:146,:),Endianness);
TraceHeader.NotchFilterSlope            = ChampInt16(Tout(147:148,:),Endianness);
TraceHeader.LowCutFrq                   = ChampInt16(Tout(149:150,:),Endianness);
TraceHeader.HighCutFrq                  = ChampInt16(Tout(151:152,:),Endianness);
TraceHeader.LowCutSlope                 = ChampInt16(Tout(153:154,:),Endianness);
TraceHeader.HighCutSlope                = ChampInt16(Tout(155:156,:),Endianness);
TraceHeader.YearDataRecorded            = ChampInt16(Tout(157:158,:),Endianness);
TraceHeader.Day                         = ChampInt16(Tout(159:160,:),Endianness);
TraceHeader.Hour                        = ChampInt16(Tout(161:162,:),Endianness);
TraceHeader.Minute                      = ChampInt16(Tout(163:164,:),Endianness);
TraceHeader.Second                      = ChampInt16(Tout(165:166,:),Endianness);
TraceHeader.TimeBasis                   = ChampInt16(Tout(167:168,:),Endianness);
TraceHeader.TraceWeightingFactor        = ChampInt16(Tout(169:170,:),Endianness);
TraceHeader.GeophoneNumberRollSwitch    = ChampInt16(Tout(171:172,:),Endianness);
TraceHeader.GeophoneNumberFirstTrace    = ChampInt16(Tout(173:174,:),Endianness);
TraceHeader.GeophoneNumberLastTrace     = ChampInt16(Tout(175:176,:),Endianness);
TraceHeader.GapSize                     = ChampInt16(Tout(177:178,:),Endianness);
TraceHeader.TaperOvertravel             = ChampInt16(Tout(179:180,:),Endianness);
TraceHeader.XcdpCoordinate              = ChampInt32(Tout(181:184,:),Endianness);
TraceHeader.YcdpCoordinate              = ChampInt32(Tout(185:188,:),Endianness);
TraceHeader.InLineNb                    = ChampInt32(Tout(189:192,:),Endianness);
TraceHeader.CdpNb                       = ChampInt32(Tout(193:196,:),Endianness);
TraceHeader.ShotPointNb                 = ChampInt32(Tout(197:200,:),Endianness);
TraceHeader.ScalarToShotPonitNb         = ChampInt16(Tout(201:202,:),Endianness);
TraceHeader.TraceValueMesUnit           = ChampInt16(Tout(203:204,:),Endianness);
TraceHeader.TransductionConstantMantisse= ChampInt32(Tout(205:208,:),Endianness);
TraceHeader.TransductionConstantExponent= ChampInt16(Tout(209:210,:),Endianness); 
TraceHeader.TransductionUnit            = ChampInt16(Tout(211:212,:),Endianness); 
TraceHeader.TraceIdentifier             = ChampInt16(Tout(213:214,:),Endianness);
TraceHeader.ScalarToTime                = ChampInt16(Tout(215:216,:),Endianness);
TraceHeader.SourceType                  = ChampInt16(Tout(217:218,:),Endianness);
TraceHeader.SourceEnergyDirectionInteger= ChampInt32(Tout(219:222,:),Endianness); 
TraceHeader.SourceEnergyDirectionDecimal= ChampInt16(Tout(223:224,:),Endianness); 
TraceHeader.SourceMeasurementMantisse   = ChampInt32(Tout(225:228,:),Endianness); 
TraceHeader.SourceMeasurementExponent   = ChampInt16(Tout(229:230,:),Endianness); 
TraceHeader.SourceMeasurementUnit       = ChampInt16(Tout(231:232,:),Endianness);
TraceHeader.Millisecond                 = ChampInt16(Tout(233:234,:),Endianness);
TraceHeader.CompenseMvt                 = ChampInt32(Tout(235:238,:),Endianness);
TraceHeader.Unassigned                  = Tout(239:240,:);
return

%==========================================================================
%Fonction qui permet de lire les traces: conversion format IBM -> float (double)  
function V = IBM2double(Vin,Endianness)
sz  = size(Vin);
Vin = double(Vin);
Vin = reshape(Vin,4,numel(Vin)/4);
if (Endianness==1)
    Vin = flipud(Vin);
end
Signe    = Vin(1:4:end)>128; 							       %Le bit de signe est allumé->Négatif
Exposant = bitand(Vin(1:4:end),127);						   %On récupère les 7 premiers bits du 1 octet -> C'est l'exposant
Mantisse = 256*256*Vin(2:4:end)+256*Vin(3:4:end)+Vin(4:4:end); %La mantisse, c'est les 3 derniers octets, ORés comme il faut.
clear Vin; 

V = 16.^(Exposant-70).*Mantisse;
V(Signe) = -V(Signe);
V = reshape(V,sz(1)/4,sz(2));
return

%==========================================================================
%Fonction pour transformer la représentation binaire d'un int16 en sa valeur.
function Vin = ChampInt16(Vin,Endianness)
sz  = size(Vin);
Vin = double(reshape(Vin,2,numel(Vin)/2));
if (Endianness==1)
    Vin = flipud(Vin);
end
Vin = [256 1]*Vin;
Vin = reshape(Vin,sz(1)/2,sz(2));
Vin = mod(Vin+2^15,2^16)-2^15;
return

%==========================================================================
%Fonction pour transformer la représentation binaire d'un int32 en sa valeur.
function Vin = ChampInt32(Vin,Endianness)
sz  = size(Vin);
Vin = double(reshape(Vin,4,numel(Vin)/4));
if (Endianness==1)
    Vin = flipud(Vin);
end
Vin = [256^3 256^2 256 1]*Vin;
Vin = reshape(Vin,sz(1)/4,sz(2));
Vin = mod(Vin+2^31,2^32)-2^31;
return


