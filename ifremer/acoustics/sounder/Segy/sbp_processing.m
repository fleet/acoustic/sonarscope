%**************************************************************************
% function [h,Data] = sbp_processing_jma(directory, filename)
%--------------------------------------------------------------------------
% programme pour l'exploitation des donn�es des SDS de l'ifremer
%--------------------------------------------------------------------------
% INPUT :
% - directory : nom du r�pertoire dans lequel se trouve le fichier
% - filename : nom du fichier *.SEG � lire (fichier acquis avec SUBOP)
% - params   : liste de param�tre de filtrage.
%
% OUTPUT :
% - h:    header de trace (cf. description dans le programme sbp_header.m)
%         un nouveau param�tre h.final_RecordingLag est mis � jour avec les 
%         �ventuelles corrections de pilonnement + immersion (pour l'AUV)
%         (valeurs en float)
% - Data: donn�es trait�es (remarque : le d�lai + corrections pilonnement
%         et immersion ne sont pas appliqu�s)
%
% traitement des donn�es SDS :
%   - choix du navire
%   - lecture des donn�es segy
%   - option : filtrage des donn�es dans la bande fmin-200 fmax+200 Hz
%   - option : correlation avec le signal �mis (valeur : 0 ou 1)
%   - option : correction de divergence sph�rique (valeur : 0 ou 1)
%   - option : enveloppe du signal (valeur : 0 ou 1)
%   - option : conversion en dB (valeur : 0 ou 1)
%   - option : application des corrections de pilonnement (valeur : 0 ou 1)
%   - option : correction de l'immersion pour l'AUV (valeur : 0 ou 1)
%**************************************************************************
% ifremer - pacault - 17/01/2012
%**************************************************************************


% % % % %--- s�lection du bateau --------------------------------------------------
% % % % navire      = 4; % 1=PP (Pourquoi pas?)
% % % %                  % 2=AT (Atalante)
% % % %                  % 3=SU (Suro�t)
% % % %                  % 4=HA (Haliotis)
% % % %                  % 5=AUV (Asterx ou Idefx)
% % % %                  % 6=SP (Sparker)
% % % % 
% % % % %--- s�lection des traitements � appliquer --------------------------------
% % % % filtre      = 1; % filtrage des donn�es dans la bande fmin-200 fmax+200 Hz (valeur : 0 ou 1)
% % % %                  % pour sparker : filtre 100-2000Hz
% % % % correl      = 1; % correlation avec le signal �mis (valeur : 0 ou 1) - 0 pour sparker
% % % % divspher    = 1; % correction de divergence sph�rique (valeur : 0 ou 1)
% % % % enveloppe   = 1; % enveloppe du signal (valeur : 0 ou 1) - 0 pour sparker
% % % % dB          = 0; % conversion en dB (valeur : 0 ou 1)
% % % % mvt         = 1; % application des corrections de pilonnement (valeur : 0 ou 1)
% % % % immersion   = 1; % correction de l'immersion pour l'AUV (valeur : 0 ou 1)
% % % %                  
%==========================================================================

function [h, SegyData] = sbp_processing(directory, filename, paramsProcessing)

%{ 
OLD
fname           = [directory filename];
[a,b,x,c,Data]  = sbp_litsegy(fname); clear a x;
h = sbp_header(b,c);
%}

% Appel de la nouvelle proc�dure de lecture des donn�es.
nomFic = fullfile(directory,filename);
a = cl_segy('nomFic', nomFic);

[flag, SegyTraceHeaders] = read_TraceHeaders_bin(a, 'Memmapfile', 0);
if ~flag
    return
end

[flag, SegyHeader] = SSc_ReadDatagrams(nomFic, 'Ssc_Header');
if ~flag
    return
end

[flag, SegyData] = read_Data_bin(a, 'RawData', 1, 'Memmapfile', 0);
if ~flag
    return
end

h = sbp_header(SegyHeader, SegyTraceHeaders);

%% R�cup�ration des filtres.

filtre      = paramsProcessing.FilterProcessing; % filtrage des donn�es dans la bande fmin-200 fmax+200 Hz (valeur : 0 ou 1)
                 % pour sparker : filtre 100-2000Hz
correl      = paramsProcessing.CorrelationProcessing; % correlation avec le signal �mis (valeur : 0 ou 1) - 0 pour sparker
divspher    = paramsProcessing.SphericalDivergence;   % correction de divergence sph�rique (valeur : 0 ou 1)
enveloppe   = paramsProcessing.Envelop;               % enveloppe du signal (valeur : 0 ou 1) - 0 pour sparker
dB          = paramsProcessing.dB;                    % conversion en dB (valeur : 0 ou 1)
mvt         = paramsProcessing.HeaveCorrection;       % application des corrections de pilonnement (valeur : 0 ou 1)
immersion   = paramsProcessing.ImmersionCorrection;   % correction de l'immersion pour l'AUV (valeur : 0 ou 1)

saveProcess = paramsProcessing.SaveProcessingData;
% Filtrage
filtres     = [paramsProcessing.FilterProcessing paramsProcessing.CorrelationProcessing paramsProcessing.SphericalDivergence ...
                paramsProcessing.Envelop paramsProcessing.dB];
% Traitements de positionnement
filtresPos = [paramsProcessing.HeaveCorrection paramsProcessing.ImmersionCorrection];

%% Initialisation des traitements

navire = paramsProcessing.selectNav;
        
% Saisie des param�tres de traitement du Chirp.
if (h.StartChirpFrq == 0) || (h.EndChirpFrq == 0)
    str1 = 'Les intervalles de fr�quence ne sont pas renseign�s ! Merci de les saisir. ';
    str2 = 'The interval frequency are not able ! Please, input them.';
    my_warndlg(Lang(str1,str2), 1);
    
    [flag, FreqStart, FreqEnd, Duration] = UiUtils.CreateDialog_ChirpParametres('FreqStart', 1800, 'FreqEnd', 5300, 'Duration', 80);
    if ~flag
        return
    end
    h.StartChirpFrq = FreqStart;
    h.EndChirpFrq   = FreqEnd;
    h.ChirpLength   = Duration;
end

if filtre == 1
    if navire ~= 7
        [Buttb, Butta] = butter(4, [h.StartChirpFrq-200 h.EndChirpFrq+200]/h.SampleFreq*2);
        
    elseif h.SampleFreq == 0
        str1 = 'La fr�quence d''�chantillonnage n''est pas renseign�e ! Pas de filtrage ';
        str2 = 'The sampling frequency is not able ! No filtering';
        my_warndlg(Lang(str1,str2), 1);
        filtre = 0;
        
    else
        if navire==6
            [Buttb, Butta] = butter(4, [100 2000]/h.SampleFreq*2);
        else
            [Buttb, Butta] = butter(4, [h.StartChirpFrq-200 h.EndChirpFrq+200]/h.SampleFreq*2);
        end
    end
end

if correl == 1
    Chirp = sbp_chirp(h, navire);
    if isnan(Chirp)
        correl = 0;
    end
end

h.final_RecordingLag = h.RecordingLag;
if mvt == 1
    h.final_RecordingLag = h.final_RecordingLag + h.CompenseMvt * 1e-3;
end
if immersion == 1
    [Buttb3,Butta3]      = butter(4,3/100);
    ImmersionFiltr       = filtfilt(Buttb3, Butta3, [h.MRUDepth]);
    shift_imm            = ImmersionFiltr * 2 / h.WaterVelocity*1000;
    h.final_RecordingLag = h.final_RecordingLag + shift_imm;
end

%% Application des traitements
% 10 arbitraire valid� sur le fichier HA0051_D20120412_T074618
% Offset = floor(h.final_RecordingLag-10); 
% sz     = size(SegyData.Data{1});
if any(filtres)
    for itr = 1:h.NbPings
        % Trace = double(SegyData.Data(:,itr));
        Trace = double(SegyData.Data{1}(itr,:));
        Trace = Trace';
        
        if filtre
            Trace = filtfilt(Buttb, Butta, Trace);
        end
        
        if correl
            Trace = 2 / h.SampleFreq / (h.ChirpLength * 1e-3) .* xcorr(Trace,Chirp);
            Trace(1:h.NbSamples-1,:) = [];
        end
        
        if divspher
            Vecteur = (1:h.NbSamples)' / h.SampleFreq;
            VecteurSdico = (Vecteur + h.RecordingLag(itr)*1e-3) * h.WaterVelocity;
            Trace = Trace .* VecteurSdico;
        end
        
        if enveloppe
            Trace = abs(hilbert(Trace));
        end
        
        if dB
            Trace = 20*log10(Trace);
        end
        
        if mvt            
%             T = NaN(size(Trace),'single');
%             if Offset(itr) >= 1
%                 sub1 = (Offset(itr):sz(2));
%                 sub2 = (1:length(sub1));
%             elseif Offset(itr) <= -1
%                 sub2 = (-Offset(itr):sz(2));
%                 sub1 = (1:length(sub2));
%             end
%             T(sub1) = Trace(sub2);
%             Trace = T;
        end
        
        SegyData.Data{1}(itr,:) = Trace';
    end
end

SegyData.Data = SegyData.Data{1};

%% Traitement de la sauvegarde si demande.

if saveProcess
    [~, nomFicSeul] = fileparts(filename);
    nomDirRacine    = fullfile(directory, 'SonarScope', nomFicSeul);
    FormatSample    = getFormatSampleData(SegyHeader.DataSampleFormat(:));
    FormatVersion   = 20110914;
    if any(filtresPos)
        SegyTraceHeaders2 = SegyTraceHeaders;
        SegyTraceHeaders2.DelayRecordingTime = h.final_RecordingLag;
        SegyTraceHeaders2 = rmfield(SegyTraceHeaders2,'Title');
        SegyTraceHeaders2 = rmfield(SegyTraceHeaders2,'TimeOrigin');
        SegyTraceHeaders2 = rmfield(SegyTraceHeaders2,'FormatVersion');
        SegyTraceHeaders2 = rmfield(SegyTraceHeaders2,'Constructor');
        SegyTraceHeaders2 = rmfield(SegyTraceHeaders2,'Dimensions');
        SegyTraceHeaders2 = rmfield(SegyTraceHeaders2,'Comments');
        SegyTraceHeaders2 = rmfield(SegyTraceHeaders2,'Signals');

        % Sauvegarde des recalculs d'immersion et de compensation de Mvt.
        flag = segy2ssc_TraceHeaders(nomDirRacine, FormatVersion, 'SegyTraceHeaders', SegyTraceHeaders2, 'FormatSample', FormatSample, 'SBPStatusData', 1);
        if ~flag
            return
        end
    end
    
    if any(filtres)
        % Sauvegarde de la donn�e trait�e sous le nom Data_Process
        flag = segy2ssc_Data(nomDirRacine, FormatVersion, SegyData.Data, 'FormatSample', FormatSample, 'SBPStatusData', 1);
        if ~flag
            return
        end
    end
end
