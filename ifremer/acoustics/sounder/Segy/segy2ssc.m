%   nomFic = fullfile(  'F:\SonarScopeData\SEGY From JMA\Marmesonet\SDS-auv_Marmesonet1', ...
%                       'AUV001_PR04_proc.seg');
%   flag = segy2ssc(nomFic, 'Memmapfile', 0);
% -------------------------------------------------------------------------

function [flag, DimData] = segy2ssc(nomFic, varargin)

[varargin, flagMemmapfile] = getPropertyValue(varargin, 'Memmapfile', 1); %#ok<ASGLU>

DimData = [];
flag    = 0;
% Modif du format :
% - GLU le 26/11/2012 : Ajout de l'information NavigationFile (fichier de
% navigation annexe).
FormatVersion = 20121126;

nbOc = sizeFic(nomFic);
if nbOc == 0
    flag = 0;
    return
end

%% G�n�ration des XML et des binaires

[nomDir, nomFicSeul] = fileparts(nomFic);
nomDirRacine = fullfile(nomDir, 'SonarScope', nomFicSeul);

unzipSonarScopeCache(nomDirRacine);

if exist(nomDirRacine, 'dir')
    flag = [];
    
    flag(end+1) = exist(fullfile(nomDirRacine, 'Ssc_TraceHeaders.xml'), 'file');
    flag(end+1) = exist(fullfile(nomDirRacine, 'Ssc_TraceHeaders'), 'dir');
    
    flag(end+1) = exist(fullfile(nomDirRacine, 'Ssc_Header.xml'), 'file');
    flag(end+1) = exist(fullfile(nomDirRacine, 'Ssc_Header'), 'dir');
    
    if all(flag) && (getFormatVersion(nomDirRacine) == FormatVersion)
        nomFicXml = fullfile(nomDirRacine, 'Ssc_TraceHeaders.xml');
        DataXML = xml_mat_read(nomFicXml);
        DimData.NbTraces    = DataXML.Dimensions.NbTraceHeaders;
        DimData.NbProfils   = DataXML.Dimensions.NbLines;
        % Test de pr�sence des donn�es en mono-bloc ou multi-bloc.
        if exist(fullfile(nomDirRacine, 'Ssc_Data.xml'), 'file') && exist(fullfile(nomDirRacine, 'Ssc_Data'), 'dir')
            flag = 1;
        elseif exist(fullfile(nomDirRacine, 'Ssc_Data_1.xml'), 'file') && exist(fullfile(nomDirRacine, 'Ssc_Data_1'), 'dir')
            flag = 1;
            nomFicXml = fullfile(nomDirRacine, 'Ssc_Data_1.xml');
        end
        if flag == 1
            DataXML = xml_mat_read(nomFicXml);
            DimData.NbSamples   = DataXML.Dimensions.ns;
            return
        end
    end
else
    mkdir(nomDirRacine);
end

try
    msg = 'OK';
    % For�age du traitement Memmapfile selon la taille du fichier �
    % traiter.
    % attribFiles = dir(nomFic);
    if nbOc > 200e6
        flagMemmapfile = 1;
    end
    [flag, SegyData, SegyTraceHeaders, SegyHeader] = ReadSegy(nomFic, 'Memmapfile', flagMemmapfile);
    if ~flag
        return
    end
    % Pb sur Fichier Marmesonet/3DLine000.seg et fichier de Carla JR269-14_mig.seg
    if SegyHeader.TraceSorting ~= 4
        % Le transpose n'est pas valable avec des Memmapfile --> voir
        % JMA le traitement sur les memmapfile pour remettre le flag_Memmapfile � 0 par d�faut.
        SegyData = my_transpose(SegyData);
    end
    
    if isempty(SegyData)
        msg = [];
    end
catch %#ok<CTCH>
    str1 = sprintf('La biblioth�que SEGYMAT semble avoir rencontr� un probl�me sur le fichier %s', nomFic);
    str2 = sprintf('SEGYMAT library does not seem working for file %s', nomFic);
    my_warndlg(Lang(str1,str2), 0);
    return
end

if isempty(msg)
    str1 = sprintf('La biblioth�que SEGYMAT semble avoir rencontr� un probl�me sur le fichier %s', nomFic);
    str2 = sprintf('SEGYMAT library does not seem working for file %s', nomFic);
    my_warndlg(Lang(str1,str2), 0, 'Tag', nomFic);
end

if ~exist(nomDirRacine, 'dir')
    if isempty(msg)
        str1 = sprintf('La biblioth�que SEGYMAT semble avoir rencontr� un probl�me sur le fichier %s', nomFic);
        str2 = sprintf('SEGYMAT library does not seem working for file %s', nomFic);
        msg = Lang(str1,str2);
    end
    my_warndlg(msg, 0, 'Tag', nomFic);
    
    flag = 0;
    return
end

%% Datagrammes SegyHeader

% Transcodage du SEGY Textual File Header.
str = SegyHeader.TextualFileHeader;
str = decode_text_header(str);
str = reshape(str,80,length(str)/80);
str = char(str(:)');
SegyHeader.TextualFileHeader = str;
flag = segy2ssc_Header(nomDirRacine, FormatVersion, 'SegyHeader', SegyHeader);
if ~flag
    return
end

flag = segy2ssc_TraceHeaders(nomDirRacine, FormatVersion, 'SegyTraceHeaders', SegyTraceHeaders);
if ~flag
    return
end

FormatSample    = getFormatSampleData(SegyHeader.DataSampleFormat);
% Si la donn�e est d�compos�e en sous-blocs de donn�es.
if iscell(SegyData)
    NbBlocData      = numel(SegyData(:));
    for k=1:NbBlocData
        tmpSegyData = SegyData{k};
        flag        = segy2ssc_Data(nomDirRacine, FormatVersion, tmpSegyData, 'IndiceBloc', k, 'FormatSample', FormatSample);
    end
else
    tmpSegyData     = SegyData;
    flag            = segy2ssc_Data(nomDirRacine, FormatVersion, tmpSegyData, 'FormatSample', FormatSample);
end

clear SegyData;
if size(tmpSegyData,3) ~= 1
    DimData.NbTraces     = size(tmpSegyData, 2);
    DimData.NbSamples    = size(tmpSegyData, 1);
    DimData.NbProfils    = size(tmpSegyData, 3);
else
    DimData.NbTraces     = size(tmpSegyData, 1);
    DimData.NbSamples    = size(tmpSegyData, 2);
    DimData.NbProfils    = size(tmpSegyData, 3);
end

if ~flag
    return
end

flag = 1;


function FormatVersion = getFormatVersion(nomDirRacine)

nomFicXml = fullfile(nomDirRacine, 'Ssc_Header.xml');
flag = exist(nomFicXml, 'file');
if ~flag
    special_message('FichierNonExistant', nomFicXml)
    return
end
% edit(nomFicXml)
Datagrams = xml_mat_read(nomFicXml);
if ~isfield(Datagrams, 'FormatVersion')
    FormatVersion = 0;
    return
end
FormatVersion = Datagrams.FormatVersion;
