function flag = segy2ssc_Data(nomDirRacine, FormatVersion, SegyData, varargin)

% G�n�ration des XML et Binaires pour chacun des blocs de donn�es
% (d�composition de celle-ci si elle est trop volumineuse).

[varargin, numBlocData]   = getPropertyValue(varargin, 'IndiceBloc',    1);
[varargin, FormatSample]  = getPropertyValue(varargin, 'FormatSample',  'single');
[varargin, SBPStatusData] = getPropertyValue(varargin, 'SBPStatusData', 0); %#ok<ASGLU>

if SBPStatusData
    strStatusData = '_SBPProcess';
else
    strStatusData = '';
end

strNumBlocData = ['_' num2str(numBlocData)];

% Nommage en Data.Data pour �tre conforme aux autres conversion de formats.
if isa(SegyData, 'cl_memmapfile')
    Data.Data = get(SegyData, 'Value');
else
    Data.Data = SegyData;
end

%{
for i=1:length(Datagrams.Variables)
fprintf(1,'%d  %s\n', i, Datagrams.Variables(i).Name)
end
%}

clear SegyData;

%% G�n�ration du XML SonarScope

Info.Title                  = 'SegyData';
Info.Constructor            = 'TODO';
Info.TimeOrigin             = '01/01/-4713';
Info.Comments               = 'Binary Data';
Info.FormatVersion          = FormatVersion;

if size(Data.Data,3) ~= 1
    Info.Dimensions.NbTraceHeaders  = size(Data.Data,2);
    Info.Dimensions.ns              = size(Data.Data,1);
    Info.Dimensions.NbLines         = size(Data.Data,3);
else
    Info.Dimensions.NbTraceHeaders  = size(Data.Data,1);
    Info.Dimensions.ns              = size(Data.Data,2);
    Info.Dimensions.NbLines         = size(Data.Data,3);
end

Info.Images(1).Name          = 'Data';
if size(Data.Data,3) ~= 1
    Info.Images(end).Dimensions  = 'ns, NbTraceHeaders, NbLines';
else
    Info.Images(end).Dimensions  = 'NbTraceHeaders, ns, NbLines';
end
Info.Images(end).Storage     = FormatSample;
Info.Images(end).Unit        = '';
Info.Images(end).FileName    =  fullfile(['Ssc_Data' strStatusData strNumBlocData],'Data.bin');
Info.Images(end).Tag         = verifKeyWordSegy('Data');


%% Cr�ation du fichier XML d�crivant la donn�e

nomFicXml = fullfile(nomDirRacine, ['Ssc_Data' strStatusData strNumBlocData '.xml']);
xml_write(nomFicXml, Info);
flag = exist(nomFicXml, 'file');
if ~flag
    messageErreur(nomFicXml)
    return
end

%% Cr�ation du r�pertoire SegyData

nomDirSegyData = fullfile(nomDirRacine, ['Ssc_Data' strStatusData strNumBlocData]);
if ~exist(nomDirSegyData, 'dir')
    status = mkdir(nomDirSegyData);
    if ~status
        messageErreur(nomDirSegyData)
        flag = 0;
        return
    end
end

%% Cr�ation des fichiers binaires des images
for i=1:length(Info.Images)
    flag = writeImage(nomDirRacine, Info.Images(i), Data.(Info.Images(i).Name));
    if ~flag
        return
    end
end

flag = 1;

