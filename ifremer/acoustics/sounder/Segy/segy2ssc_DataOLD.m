function flag = segy2ssc_DataOLD(nomDirRacine, FormatVersion, varargin)

% Il n'y pas dans le cas du Segy de fichier XML g�n�r� pr�alablement par
% l'Etape 1 du SEGY 2 SSC. On se retrouve directement en �tape 2.

[varargin, SegyData]      = getPropertyValue(varargin, 'SegyData',     []);
[varargin, FormatSample]  = getPropertyValue(varargin, 'FormatSample', 'single');
[varargin, SBPStatusData] = getPropertyValue(varargin, 'SBPStatusData', 0); %#ok<ASGLU>

if SBPStatusData
    strStatusData = '_SBPProcess';
else
    strStatusData = '';
end

if isa(SegyData, 'cl_memmapfile')
    Data.Data = get(SegyData, 'Value');
else
    Data.Data = SegyData;
end

%{
for i=1:length(Datagrams.Variables)
fprintf(1,'%d  %s\n', i, Datagrams.Variables(i).Name)
end
%}

% Nommage en Data.Data pour �tre conforme aux autres conversion de formats.
Data.Data = SegyData;

clear SegyData;

%% G�n�ration du XML SonarScope

Info.Title                  = 'SegyData';
Info.Constructor            = 'TODO';
Info.TimeOrigin             = '01/01/-4713';
Info.Comments               = 'Binary Data';
Info.FormatVersion          = FormatVersion;

Info.Dimensions.NbTraceHeaders  = size(Data.Data,1);
Info.Dimensions.ns              = size(Data.Data,2);

Info.Images(1).Name          = 'Data';
Info.Images(end).Dimensions  = 'NbTraceHeaders, ns';
Info.Images(end).Storage     = FormatSample;
Info.Images(end).Unit        = '';
Info.Images(end).FileName    = fullfile(['Ssc_Data' strStatusData],'Data.bin');
Info.Images(end).Tag         = verifKeyWordSegy('Data');


%% Cr�ation du fichier XML d�crivant la donn�e

nomFicXml = fullfile(nomDirRacine, ['Ssc_Data' strStatusData '.xml']);
xml_write(nomFicXml, Info);
flag = exist(nomFicXml, 'file');
if ~flag
    messageErreur(nomFicXml)
    return
end

%% Cr�ation du r�pertoire SegyData

nomDirSegyData = fullfile(nomDirRacine, ['Ssc_Data' strStatusData]);
if ~exist(nomDirSegyData, 'dir')
    status = mkdir(nomDirSegyData);
    if ~status
        messageErreur(nomDirSegyData)
        flag = 0;
        return
    end
end

%% Cr�ation des fichiers binaires des images
for i=1:length(Info.Images)
    flag = writeImage(nomDirRacine, Info.Images(i), Data.(Info.Images(i).Name));
    if ~flag
        return
    end
end

flag = 1;

