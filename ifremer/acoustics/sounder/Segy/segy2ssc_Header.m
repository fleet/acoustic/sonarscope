function flag = segy2ssc_Header(nomDirRacine, FormatVersion, varargin)

% Il n'y pas dans le cas du Segy de fichier XML g�n�r� pr�alablement par
% l'Etape 1 du SEGY 2 SSC. On se retrouve directement en �tape 2.

[varargin, SegyHeader] = getPropertyValue(varargin, 'SegyHeader', []); %#ok<ASGLU>

% nomDirSignal = fullfile(nomDirRacine, 'SonarScope');

%{
for i=1:length(Datagrams.Variables)
fprintf(1,'%d  %s\n', i, Datagrams.Variables(i).Name)
end
%}

% % % % FIXME : L'introduction de saut de lignes n'est pas prise en compte dans
% % % % le XML sur le Textual file Header.
% % % pattern = 'C(\s|[0123456789])[0123456789]?';
% % % bidon = regexprep(SegyHeader.TextualFileHeader, pattern, '\r\n');
% % % Data.TextualFileHeader          =   bidon;

Data.TextualFileHeader          =   SegyHeader.TextualFileHeader;
Data.Job 						=	SegyHeader.Job;
Data.Line 						=	SegyHeader.Line;
Data.Reel 						=	SegyHeader.Reel;
Data.DataTracePerEnsemble 		=	SegyHeader.DataTracePerEnsemble;
Data.AuxiliaryTracePerEnsemble 	=	SegyHeader.AuxiliaryTracePerEnsemble;
Data.dt                         =	SegyHeader.dt;
Data.dtOrig                     =	SegyHeader.dtOrig;
Data.ns                         =	SegyHeader.ns;
Data.nsOrig                     =	SegyHeader.nsOrig;
Data.DataSampleFormat           =	SegyHeader.DataSampleFormat;
Data.EnsembleFold               =	SegyHeader.EnsembleFold;
Data.TraceSorting               =	SegyHeader.TraceSorting;
Data.VerticalSumCode            =	SegyHeader.VerticalSumCode;
Data.SweepFrequencyStart 		=	SegyHeader.SweepFrequencyStart;
Data.SweepFrequencyEnd          =	SegyHeader.SweepFrequencyEnd;
Data.SweepLength                =	SegyHeader.SweepLength;
Data.SweepType                  =	SegyHeader.SweepType;
Data.SweepChannel               =	SegyHeader.SweepChannel;
Data.SweepTaperlengthStart 		=	SegyHeader.SweepTaperlengthStart;
Data.SweepTaperLengthEnd 		=	SegyHeader.SweepTaperLengthEnd;
Data.TaperType                  =	SegyHeader.TaperType;
Data.CorrelatedDataTraces 		=	SegyHeader.CorrelatedDataTraces;
Data.BinaryGain                 =	SegyHeader.BinaryGain;
Data.AmplitudeRecoveryMethod 	=	SegyHeader.AmplitudeRecoveryMethod;
Data.MeasurementSystem          =	SegyHeader.MeasurementSystem;
Data.ImpulseSignalPolarity 		=	SegyHeader.ImpulseSignalPolarity;
Data.VibratoryPolarityCode 		=	SegyHeader.VibratoryPolarityCode;
% Data.Unassigned1                =	SegyHeader.Unassigned1;
Data.SegyFormatRevisionNumber 	=	SegyHeader.SegyFormatRevisionNumber;
Data.FixedLengthTraceFlag 		=	SegyHeader.FixedLengthTraceFlag;
Data.NumberOfExtTextualHeaders 	=	SegyHeader.NumberOfExtTextualHeaders;
% Data.Unassigned2                =	SegyHeader.Unassigned2;
Data.time                       =	SegyHeader.time;

%% Ecriture du fichier de r�f�rence de Nav si les positions Lat/Lon sont correctes.
% La donn�e de navigation peut �tre point�e a posteriori via Import Nav.
Data.NavigationFile             = 'internal';

% ----------------------------
% G�n�ration du XML SonarScope

Info.Title                  = 'SegyHeader';
Info.Constructor            = 'TODO';
% Info.TextualFileHeader      = Data.TextualFileHeader;
Info.TimeOrigin             = '01/01/-4713';
Info.Comments               = 'Binary File Header';
Info.FormatVersion          = FormatVersion;

Info.Dimensions.NbHeaders   = 1;
Info.Dimensions.NSamples    = Data.ns;

Info.Signals(1).Name          = 'Job';
Info.Signals(end).Dimensions  = 'NbHeaders, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_Header','Job.bin');
Info.Signals(end).Tag         = verifKeyWordSegy('Job');

Info.Signals(end+1).Name      = 'Reel';
Info.Signals(end).Dimensions  = 'NbHeaders, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_Header','Reel.bin');
Info.Signals(end).Tag         = verifKeyWordSegy('Reel');

Info.Signals(end+1).Name      = 'Line';
Info.Signals(end).Dimensions  = 'NbHeaders, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_Header','Line.bin');
Info.Signals(end).Tag         = verifKeyWordSegy('Line');

Info.Signals(end+1).Name      = 'DataTracePerEnsemble';
Info.Signals(end).Dimensions  = 'NbHeaders, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_Header','DataTracePerEnsemble.bin');
Info.Signals(end).Tag         = verifKeyWordSegy('DataTracePerEnsemble');

Info.Signals(end+1).Name      = 'AuxiliaryTracePerEnsemble';
Info.Signals(end).Dimensions  = 'NbHeaders, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_Header','AuxiliaryTracePerEnsemble.bin');
Info.Signals(end).Tag         = verifKeyWordSegy('AuxiliaryTracePerEnsemble');

Info.Signals(end+1).Name      = 'DataTracePerEnsemble';
Info.Signals(end).Dimensions  = 'NbHeaders, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_Header','DataTracePerEnsemble.bin');
Info.Signals(end).Tag         = verifKeyWordSegy('DataTracePerEnsemble');

Info.Signals(end+1).Name      = 'AuxiliaryTracePerEnsemble';
Info.Signals(end).Dimensions  = 'NbHeaders, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_Header','AuxiliaryTracePerEnsemble.bin');
Info.Signals(end).Tag         = verifKeyWordSegy('AuxiliaryTracePerEnsemble');

Info.Signals(end+1).Name      = 'dt';
Info.Signals(end).Dimensions  = 'NbHeaders, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'us';
Info.Signals(end).FileName    = fullfile('Ssc_Header','dt.bin');
Info.Signals(end).Tag         = verifKeyWordSegy('dt');

Info.Signals(end+1).Name      = 'dtOrig';
Info.Signals(end).Dimensions  = 'NbHeaders, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'us';
Info.Signals(end).FileName    = fullfile('Ssc_Header','dtOrig.bin');
Info.Signals(end).Tag         = verifKeyWordSegy('dtOrig');

Info.Signals(end+1).Name      = 'ns';
Info.Signals(end).Dimensions  = 'NbHeaders, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_Header','ns.bin');
Info.Signals(end).Tag         = verifKeyWordSegy('ns');

Info.Signals(end+1).Name      = 'nsOrig';
Info.Signals(end).Dimensions  = 'NbHeaders, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_Header','nsOrig.bin');
Info.Signals(end).Tag         = verifKeyWordSegy('nsOrig');

Info.Signals(end+1).Name      = 'DataSampleFormat';
Info.Signals(end).Dimensions  = 'NbHeaders, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'us';
Info.Signals(end).FileName    = fullfile('Ssc_Header','DataSampleFormat.bin');
Info.Signals(end).Tag         = verifKeyWordSegy('DataSampleFormat');

Info.Signals(end+1).Name      = 'EnsembleFold';
Info.Signals(end).Dimensions  = 'NbHeaders, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_Header','EnsembleFold.bin');
Info.Signals(end).Tag         = verifKeyWordSegy('EnsembleFold');

Info.Signals(end+1).Name      = 'TraceSorting';
Info.Signals(end).Dimensions  = 'NbHeaders, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_Header','TraceSorting.bin');
Info.Signals(end).Tag         = verifKeyWordSegy('TraceSorting');

Info.Signals(end+1).Name      = 'VerticalSumCode';
Info.Signals(end).Dimensions  = 'NbHeaders, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_Header','VerticalSumCode.bin');
Info.Signals(end).Tag         = verifKeyWordSegy('VerticalSumCode');

Info.Signals(end+1).Name      = 'SweepFrequencyStart';
Info.Signals(end).Dimensions  = 'NbHeaders, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'Hz';
Info.Signals(end).FileName    = fullfile('Ssc_Header','SweepFrequencyStart.bin');
Info.Signals(end).Tag         = verifKeyWordSegy('SweepFrequencyStart');

Info.Signals(end+1).Name      = 'SweepFrequencyEnd';
Info.Signals(end).Dimensions  = 'NbHeaders, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'Hz';
Info.Signals(end).FileName    = fullfile('Ssc_Header','SweepFrequencyEnd.bin');
Info.Signals(end).Tag         = verifKeyWordSegy('SweepFrequencyEnd');

Info.Signals(end+1).Name      = 'SweepLength';
Info.Signals(end).Dimensions  = 'NbHeaders, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'ms';
Info.Signals(end).FileName    = fullfile('Ssc_Header','SweepLength.bin');
Info.Signals(end).Tag         = verifKeyWordSegy('SweepLength');

Info.Signals(end+1).Name      = 'SweepType';
Info.Signals(end).Dimensions  = 'NbHeaders, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_Header','SweepType.bin');
Info.Signals(end).Tag         = verifKeyWordSegy('SweepType');

Info.Signals(end+1).Name      = 'SweepChannel';
Info.Signals(end).Dimensions  = 'NbHeaders, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_Header','SweepChannel.bin');
Info.Signals(end).Tag         = verifKeyWordSegy('SweepChannel');

Info.Signals(end+1).Name      = 'SweepTaperlengthStart';
Info.Signals(end).Dimensions  = 'NbHeaders, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'ms';
Info.Signals(end).FileName    = fullfile('Ssc_Header','SweepTaperlengthStart.bin');
Info.Signals(end).Tag         = verifKeyWordSegy('SweepTaperlengthStart');

Info.Signals(end+1).Name      = 'SweepTaperLengthEnd';
Info.Signals(end).Dimensions  = 'NbHeaders, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_Header','SweepTaperLengthEnd.bin');
Info.Signals(end).Tag         = verifKeyWordSegy('SweepTaperLengthEnd');

Info.Signals(end+1).Name      = 'TaperType';
Info.Signals(end).Dimensions  = 'NbHeaders, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_Header','TaperType.bin');
Info.Signals(end).Tag         = verifKeyWordSegy('TaperType');

Info.Signals(end+1).Name      = 'CorrelatedDataTraces';
Info.Signals(end).Dimensions  = 'NbHeaders, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_Header','CorrelatedDataTraces.bin');
Info.Signals(end).Tag         = verifKeyWordSegy('CorrelatedDataTraces');

Info.Signals(end+1).Name      = 'BinaryGain';
Info.Signals(end).Dimensions  = 'NbHeaders, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_Header','BinaryGain.bin');
Info.Signals(end).Tag         = verifKeyWordSegy('BinaryGain');

Info.Signals(end+1).Name      = 'AmplitudeRecoveryMethod';
Info.Signals(end).Dimensions  = 'NbHeaders, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_Header','AmplitudeRecoveryMethod.bin');
Info.Signals(end).Tag         = verifKeyWordSegy('AmplitudeRecoveryMethod');

Info.Signals(end+1).Name      = 'MeasurementSystem';
Info.Signals(end).Dimensions  = 'NbHeaders, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_Header','MeasurementSystem.bin');
Info.Signals(end).Tag         = verifKeyWordSegy('MeasurementSystem');

Info.Signals(end+1).Name      = 'ImpulseSignalPolarity';
Info.Signals(end).Dimensions  = 'NbHeaders, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_Header','ImpulseSignalPolarity.bin');
Info.Signals(end).Tag         = verifKeyWordSegy('ImpulseSignalPolarity');

Info.Signals(end+1).Name      = 'VibratoryPolarityCode';
Info.Signals(end).Dimensions  = 'NbHeaders, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile('Ssc_Header','VibratoryPolarityCode.bin');
Info.Signals(end).Tag         = verifKeyWordSegy('VibratoryPolarityCode');

% Info.Signals(end+1).Name      = 'Unassigned1';
% Info.Signals(end).Dimensions  = 'NbHeaders, 1';
% Info.Signals(end).Storage     = 'single';
% Info.Signals(end).Unit        = '';
% Info.Signals(end).FileName    = fullfile('Ssc_Header','Unassigned1.bin');
% Info.Signals(end).Tag         = verifKeyWordSegy('Unassigned1');
%
Info.Signals(end+1).Name      = 'SegyFormatRevisionNumber';
Info.Signals(end).Dimensions  = 'NbHeaders, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_Header','SegyFormatRevisionNumber.bin');
Info.Signals(end).Tag         = verifKeyWordSegy('SegyFormatRevisionNumber');

Info.Signals(end+1).Name      = 'FixedLengthTraceFlag';
Info.Signals(end).Dimensions  = 'NbHeaders, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_Header','FixedLengthTraceFlag.bin');
Info.Signals(end).Tag         = verifKeyWordSegy('FixedLengthTraceFlag');

Info.Signals(end+1).Name      = 'NumberOfExtTextualHeaders';
Info.Signals(end).Dimensions  = 'NbHeaders, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_Header','NumberOfExtTextualHeaders.bin');
Info.Signals(end).Tag         = verifKeyWordSegy('NumberOfExtTextualHeaders');

% Info.Signals(end+1).Name      = 'Unassigned2';
% Info.Signals(end).Dimensions  = 'NbHeaders, 1';
% Info.Signals(end).Storage     = 'single';
% Info.Signals(end).Unit        = '';
% Info.Signals(end).FileName    = fullfile('Ssc_Header','Unassigned2.bin');
% Info.Signals(end).Tag         = verifKeyWordSegy('Unassigned2');
%
Info.Signals(end+1).Name      = 'time';
Info.Signals(end).Dimensions  = '1, NSamples';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 's';
Info.Signals(end).FileName    = fullfile('Ssc_Header','time.bin');
Info.Signals(end).Tag         = verifKeyWordSegy('time');

Info.Strings(1).Name          = 'TextualFileHeader';
Info.Strings(end).Dimensions  = 'NbHeaders, 1';
% Info.Strings(end).Storage     = 'char';
% Info.Strings(end).Unit        = '';
Info.Strings(end).FileName    = fullfile('Ssc_Header','TextualFileHeader.txt');
Info.Strings(end).Tag         = verifKeyWordSegy('TODO');

Info.Strings(end+1).Name          = 'NavigationFile';
Info.Strings(end).Dimensions    = '1, 1';
% Info.Strings(end).Storage       = 'char';
% Info.Strings(end).Unit          = '';
Info.Strings(end).FileName      = fullfile('Ssc_Header', 'NavigationFile.bin');

%% Cr�ation du fichier XML d�crivant la donn�e

nomFicXml = fullfile(nomDirRacine, 'Ssc_Header.xml');
xml_write(nomFicXml, Info);
flag = exist(nomFicXml, 'file');
if ~flag
    messageErreur(nomFicXml)
    return
end

%% Cr�ation du r�pertoire Clock

nomDirSegyHeader = fullfile(nomDirRacine, 'Ssc_Header');
if ~exist(nomDirSegyHeader, 'dir')
    status = mkdir(nomDirSegyHeader);
    if ~status
        messageErreur(nomDirSegyHeader)
        flag = 0;
        return
    end
end

%% Cr�ation des fichiers binaires des signaux
for i=1:length(Info.Signals)
    flag = writeSignal(nomDirRacine, Info.Signals(i), Data.(Info.Signals(i).Name));
    if ~flag
        return
    end
end

%% Cr�ation des fichiers binaires des Strings
for i=1:length(Info.Strings)
    flag = writeStrings(nomDirRacine, Info.Strings(i), {Data.(Info.Strings(i).Name)});
    if ~flag
        return
    end
end


flag = 1;

