function flag = segy2ssc_TraceHeaders(nomDirRacine, FormatVersion, varargin)

% Il n'y pas dans le cas du Segy de fichier XML g�n�r� pr�alablement par
% l'Etape 1 du SEGY 2 SSC. On se retrouve directement en �tape 2.

[varargin, DataTrace]     = getPropertyValue(varargin, 'SegyTraceHeaders', []);
[varargin, SBPStatusData] = getPropertyValue(varargin, 'SBPStatusData',    0); %#ok<ASGLU>

if SBPStatusData
    strStatusData = '_SBPProcess';
else
    strStatusData = '';
end

%% G�n�ration du XML SonarScope

Info.Title         = 'SegyTraceHeaders';
Info.Constructor   = 'TODO';
Info.TimeOrigin    = '01/01/-4713';
Info.Comments      = 'Binary File Header';
Info.FormatVersion = FormatVersion;

if SBPStatusData==0
    Info.Dimensions.NbTraceHeaders = size(DataTrace,1);
else
    Info.Dimensions.NbTraceHeaders = size(DataTrace.SegyMAT_TraceDataStart,1);
end
Info.Dimensions.NbLines = size(DataTrace,2);
f           = fieldnames(DataTrace);
nbFields    = length(f);
tabUnit     = { '','','','','','','','','','','','','','','','','','','','',...
    '','','','','','','','','','ms','ms','','','ms','ms','ms','ms','ms','',...
    'us','','dB','dB','','Hz','Hz','ms','','ms','ms','','Hz','dB/octave','Hz','dB/octave','Hz','Hz','dB/octave','dB/octave',...
    '','','','','','','','','','','','','','','','','','',...
    '','','','','','','','','','','','','','','ms','',''};

tabStorage  = repmat({'single'}, 1, nbFields);

tabStorage{strcmp(f, 'cdpX')}               = 'double';
tabStorage{strcmp(f, 'cdpY')}               = 'double';
tabStorage{strcmp(f, 'GroupX')}             = 'double';
tabStorage{strcmp(f, 'GroupY')}             = 'double';
tabStorage{strcmp(f, 'SourceX')}            = 'double';
tabStorage{strcmp(f, 'SourceY')}            = 'double';
tabStorage{strcmp(f, 'SourceGroupScalar')}  = 'double';

Dim = {'NbTraceHeaders, NbLines'};

% % % % Dim{end+1}  = ['NbTraceHeaders,' num2str(maxLenTimeBaseCodeText)];
% % % % % Dimension de la variable TraceValueMeasurementUnitText.
% % % % Dim{end+1}  = ['NbTraceHeaders,' num2str(maxLenTraceValueMeasurementUnitText)];
% % % % tabDim      = { Dim{1},Dim{1},Dim{1},Dim{1},Dim{1},Dim{1},Dim{1},Dim{1},...
% % % %                 Dim{1},Dim{1},Dim{1},Dim{1},Dim{1},Dim{1},Dim{1},Dim{1},...
% % % %                 Dim{1},Dim{1},Dim{1},Dim{1},Dim{1},Dim{1},Dim{1},Dim{1},...
% % % %                 Dim{1},Dim{1},Dim{1},Dim{1},Dim{1},Dim{1},Dim{1},Dim{1},...
% % % %                 Dim{1},Dim{1},Dim{1},Dim{1},Dim{1},Dim{1},Dim{1},Dim{1},...
% % % %                 Dim{1},Dim{1},Dim{1},Dim{1},Dim{1},Dim{1},Dim{1},Dim{1},...
% % % %                 Dim{1},Dim{1},Dim{1},Dim{1},Dim{1},Dim{1},Dim{1},Dim{1},...
% % % %                 Dim{1},Dim{1},Dim{1},Dim{1},Dim{1},Dim{1},Dim{1},Dim{1},...
% % % %                 Dim{1},Dim{1},Dim{2},Dim{1},Dim{1},Dim{1},Dim{1},Dim{1},...
% % % %                 Dim{1},Dim{1},Dim{1},Dim{1},Dim{1},Dim{1},Dim{1},Dim{1},...
% % % %                 Dim{3},Dim{1},Dim{1},Dim{1},Dim{1},Dim{1},Dim{1},Dim{1},...
% % % %                 Dim{1},Dim{1},Dim{1},Dim{1},Dim{1},Dim{1},Dim{1},Dim{1}};
tabDim = repmat(Dim{1}, [size(f,1),1]);
str = 1;
sig = 1;
for i=1:numel(f)
    if ~strcmp(tabStorage{i}, 'char')
        Info.Signals(sig).Name        = f{i};
        Info.Signals(end).Dimensions  = tabDim(i,:); %tabDim{i};
        Info.Signals(end).Storage     = tabStorage{i};
        Info.Signals(end).Unit        = tabUnit{i};
        Info.Signals(end).FileName    = fullfile(['Ssc_TraceHeaders' strStatusData], [f{i} '.bin']);
        Info.Signals(end).Tag         = verifKeyWordSegy(f{i});
        sig = sig+1;
    else
        Info.Strings(str).Name        = f{i};
        Info.Strings(end).Dimensions  = 'NbTraceHeaders, NbLines';
        % Info.Strings(end).Storage   = 'char';
        % Info.Strings(end).Unit      = '';
        Info.Strings(end).FileName    = fullfile(['Ssc_TraceHeaders' strStatusData], [f{i} '.bin']);
        str = str + 1;
    end
end


%% Cr�ation du fichier XML d�crivant la donn�e

nomFicXml = fullfile(nomDirRacine, ['Ssc_TraceHeaders' strStatusData '.xml']);
xml_write(nomFicXml, Info);
flag = exist(nomFicXml, 'file');
if ~flag
    messageErreur(nomFicXml)
    return
end

%% Cr�ation du r�pertoire SegyTraceHeaders

nomDirSegyTraceHeaders = fullfile(nomDirRacine, ['Ssc_TraceHeaders' strStatusData]);
if ~exist(nomDirSegyTraceHeaders, 'dir')
    status = mkdir(nomDirSegyTraceHeaders);
    if ~status
        messageErreur(nomDirSegyTraceHeaders)
        flag = 0;
        return
    end
end

%% Cr�ation des fichiers binaires des signaux

for i=1:length(Info.Signals)
    flag = writeSignal(nomDirRacine, Info.Signals(i), [DataTrace.(Info.Signals(i).Name)]);
    if ~flag
        return
    end
end

flag = 1;
