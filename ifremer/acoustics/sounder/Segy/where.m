function [str, MemMapFileName] = where(this)
if isa(this.Image, 'cl_memmapfile')
    MemMapFileName = get(this.Image, 'FileName');
    str = ['Virtual memory on ' MemMapFileName];

    % ----------------------------------------------------------
    % On verifie si c'est bien un fichier avec l'extension
    % ".memmapfile" car in ne faudrait pas detruire des fichiers
    % ErMapper

    [~, ~, Ext] = fileparts(MemMapFileName);
    if ~strcmp(Ext, '.memmapfile')
        MemMapFileName = [];
    end

else
    str = 'On RAM';
    MemMapFileName = [];
end