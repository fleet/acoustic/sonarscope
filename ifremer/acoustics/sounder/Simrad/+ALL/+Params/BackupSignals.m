% Description
%   Params for BackupSignals
%
% Syntax
%    [flag, NomSignals] = ALL.Params.BackupSignals(listFileNames)
%
% Input Arguments
%   listFileNames : List of the .all files
%
% Output Arguments
%   flag       : 0=KO, 1=OK
%   NomSignals : List of the signal names
%
% Examples
%    [flag, NomSignals] = ALL.Params.BackupSignals(listFileNames)
%
% Author : JMA
% See also Authors

function [flag, NomSignals] = BackupSignals(listFileNames)

NomSignals = [];

if ischar(listFileNames)
    listFileNames = {listFileNames};
end

%% Oblig� de cr�er au moins un fichier pour pouvoir r�cup�rer la liste des signaux

aKM = cl_simrad_all('nomFic', listFileNames{1});
[flag, Data] = read_depth_bin(aKM, 'XMLOnly', 1);
if ~flag
    return
end

%% Select a signal

Option = {'Navigation'; 'Attitude'};
for k=1:length(Data.Signals)
    switch Data.Signals(k).Name
        case {'MaxNbBeamsPossible'; 'NbBeams'; 'PingCounter'; 'SystemSerialNumber'; 'SamplingRate'}
        otherwise
            Option{end+1} = Data.Signals(k).Name; %#ok<AGROW>
    end
end
[flag, sub] = question_SelectASignal(Option, 'InitialValue', []);
if ~flag || isempty(sub)
    return
end
NomSignals = Option(sub);
