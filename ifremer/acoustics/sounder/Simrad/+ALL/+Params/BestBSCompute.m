% Description
%   Params for BestBSCompute
%
% Syntax
%     [flag, repExport, InfoCompensationCurve] = ALL.Params.BestBSCompute(repImport, repExport, varargin)
%
% Input Arguments
%   repExport : The SonarScope default directory for file exports
%
% Name-Value Pair Arguments
%   askQuestionSignatureConfSonar : 
%
% Output Arguments
%   flag                  : 0=KO, 1=OK
%   repExport             : The new SonarScope default directory for file exports
%   InfoCompensationCurve : 
%
% Examples
%     [flag, repExport, InfoCompensationCurve] = ALL.Params.BestBSCompute(repImport, repExport)
%
% Author : JMA
% See also Authors

function [flag, repExport, InfoCompensationCurve] = BestBSCompute(repImport, repExport, varargin)

% message_MBES_OneConfiguration

[varargin, askQuestionSignatureConfSonar] = getPropertyValue(varargin, 'askQuestionSignatureConfSonar', 0); %#ok<ASGLU>

InfoCompensationCurve = [];

%% Courbe de compensation

% if AskQstComp % QL > 1
    [flag, FileNameCompensation, ModeDependant, UseModel, PreserveMeanValueIfModel] = QuestionCompensationCurve(repImport);
    if ~flag
        return
    end
    InfoCompensationCurve.FileName      = FileNameCompensation;
    InfoCompensationCurve.ModeDependant = ModeDependant;
    InfoCompensationCurve.UseModel      = UseModel;
    InfoCompensationCurve.PreserveMeanValueIfModel = PreserveMeanValueIfModel;
% end
