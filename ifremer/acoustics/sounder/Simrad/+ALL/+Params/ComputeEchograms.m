function [flag, repImport, repExport, TypeWCOutput, Names, StepCompute, StepDisplay, ...
    CorrectionTVG, CLimRaw, CompReflec, ModeDependant, flagTideCorrection, typeAlgoWC, MaskAfterDetection, ...
    skipAlreadyProcessedFiles, MaxImageSize, Tag, typeOutputFormat] = ComputeEchograms(listFileNames, repImport, repExport, varargin)

persistent LastCLimRaw

Names                     = [];
StepCompute               = 1;
StepDisplay               = 1;
CorrectionTVG             = 30;
CLimRaw                   = [];
CompReflec                = [];
ModeDependant             = [];
flagTideCorrection        = 0;
typeAlgoWC                = 2;
TypeWCOutput              = 1; % Images individuelles
MaxImageSize              = [];
skipAlreadyProcessedFiles = 0;
Tag                       = [];
typeOutputFormat          = [];

QL = get_LevelQuestion;

if ischar(listFileNames)
    listFileNames = {listFileNames};
end

%% Masquage au del� de la d�tection

[flag, MaskAfterDetection] = question_MaskWCBeyondDetection;
if ~flag
    return
end

%% MaxImageSize

[flag, MaxImageSize] = question_WCMaxImageSize;
if ~flag
    return
end

%% Type d'algorithme

[flag, typeAlgoWC] = question_WCRaytracingAlgorithm('InitialValue', 2);
if ~flag
    return
end

%% Correction de mar�e

[flag, flagTideCorrection] = question_TideCorrection;
if ~flag
    return
end

%% CorrectionTVG : comment� le 02/11/2018 car �a doit �tre am�lior� : prendre en compte ce qui est d�finit dans le datagramme (TVG function applied et demander � l'utilisateur si il veut 20, 30 ou 40 log

[flag, CorrectionTVG] = question_CompensationTVG_WC;
if ~flag
    return
end

%% Courbes de compensation

if QL > 1
    [flag, CompReflec, ModeDependant, UseModel, PreserveMeanValueIfModel] = QuestionCompensationCurve(repImport); %#ok<ASGLU>
    if ~flag
        return
    end
else
    CompReflec    = [];
    ModeDependant = 0;
end

%% Rehaussement de contraste des donn�es brutes

if isempty(LastCLimRaw)
    CLimRaw = [-64 10];
else
    CLimRaw = LastCLimRaw;
end

str1 = 'IFREMER - SonarScope : Bornes du rehaussement de contraste';
str2 = 'IFREMER - SonarScope : Color limits (contrast enhancement)';
p(1) = ClParametre('Name', Lang('Valeur min','Min value'), ...
    'Unit', 'dB',  'MinValue', -100, 'MaxValue', 0, 'Value', CLimRaw(1));
p(2) = ClParametre('Name', Lang('Valeur max','Max value'), ...
    'Unit', 'dB',  'MinValue', -50, 'MaxValue', 50, 'Value', CLimRaw(2));
a = StyledParametreDialog('params', p, 'Title', Lang(str1,str2));
% a.Help = 'http://flotte.ifremer.fr/fleet/Presentation-of-the-fleet/On-board-software/SonarScope';
a.sizes = [0 -3 -1 -2 -1 -1 -1 0];
a.openDialog;
flag = a.okPressedOut;
if ~flag
    return
end
CLimRaw = a.getParamsValue;
LastCLimRaw = CLimRaw;

%% Pas d'�chantillonnage des pings

[flag, StepCompute] = question_ComputeEchograms;
if ~flag
    return
end

%% Tag suppl�mentaire dans le mom des images

[flag, Tag] = question_AddTagInOutputFile;
if ~flag
    return
end
if ~isempty(Tag)
    Tag = ['_' Tag];
end

%% Skip existing files

N = length(listFileNames);
[flag, skipAlreadyProcessedFiles] = question_SkipAlreadyProcessedFiles(N);
if ~flag
    return
end

%% Pas de visualisation des pings

[flag, StepDisplay] = WCD.Params.question_VisuEchograms;
if ~flag
    return
end

%% Type de fichier en sortie

if nargout == 18 % TODO : pas top mais c'est vou� � dispara�tre car on ne fera plus que des exports Netcdf � terme
    str = {'XML-Bin'; 'Netcdf'};
    [typeOutputFormat, flag] = my_listdlg('Type of output file', str, 'SelectionMode', 'Single', 'InitialValue', 2);
    if ~flag
        return
    end
end

%% Select directory

str1 = 'Choisissez ou cr�ez le r�pertoire o� seront stock�s les fichiers ';
str2 = 'Select or create the directory in which the files will be created.';
[flag, repExport] = my_uigetdir(repExport, Lang(str1,str2));
if ~flag
    return
end

%% G�n�ration des noms des fichiers

clear Names
for k=1:N
    [~, nomFic] = fileparts(listFileNames{k});
    Name.nomFicXmlRaw  = fullfile(repExport, [nomFic Tag '_Raw_PolarEchograms.xml']);
    Name.nomFicXmlComp = fullfile(repExport, [nomFic Tag '_Comp_PolarEchograms.xml']);
    Name.nomFicNetcdf  = fullfile(repExport, [nomFic Tag '_PolarEchograms.g3d.nc']);
    Name.AreaName = [];
    Names(k) = Name; %#ok<AGROW>
end

%% Message //Tbx

if (StepDisplay == 0) && (N > 1)
    flag = messageParallelTbx(1);
    if ~flag
        return
    end
end
