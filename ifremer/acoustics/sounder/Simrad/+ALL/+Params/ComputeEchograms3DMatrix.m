function [flag, repImport, repExport, TypeWCOutput, Names, ...
    StepCompute, StepDisplay, CorrectionTVG, ...
    CLimRaw, CompReflec, ModeDependant, flagTideCorrection, typeAlgoWC, ...
    MaskBeyondDetection, skipAlreadyProcessedFiles, ...
    minAcrossDist, maxAcrossDist, NbPingsMax, TypeFileWCOutput, MaxImageSize] = ComputeEchograms3DMatrix(listFileNames, repImport, repExport)

persistent LastMinAcrossDist LastMaxAcrossDist LastNbPingsMax

QL = get_LevelQuestion;

minAcrossDist    = -Inf;
maxAcrossDist    = Inf;
NbPingsMax       = [];
TypeFileWCOutput = [];

[flag, repImport, repExport, TypeWCOutput, Names, ...
    StepCompute, StepDisplay, CorrectionTVG, ...
    CLimRaw, CompReflec, ModeDependant, flagTideCorrection, typeAlgoWC, ...
    MaskBeyondDetection, skipAlreadyProcessedFiles, MaxImageSize, Tag] = ALL.Params.ComputeEchograms(listFileNames, repImport, repExport);
if ~flag
    return
end
TypeWCOutput = 2;

%% Type de fichier en sortie

if QL >= 3
    str = {'XML-Bin'; 'Netcdf'};
    [TypeFileWCOutput, flag] = my_listdlg('Type of output file', str, 'SelectionMode', 'Single', 'InitialValue', 2);
    if ~flag
        return
    end
else
    fprintf('The3DMatrix output format is set to "Netcdf" automatically because QL=3\n');
    TypeFileWCOutput = 2;
end

%% Prepare images for NWW

if ~isempty(Tag)
    Tag = ['_' Tag];
end

N = length(listFileNames);
pathname = repExport;
clear Names
for k=1:N
    [~, nomFic] = fileparts(listFileNames{k});
    Name.nomFicXmlRaw  = fullfile(pathname, [nomFic Tag '_WCCubeRaw.xml']);
    Name.nomFicXmlComp = fullfile(pathname, [nomFic Tag '_WCCubeComp.xml']);
    Name.nomFicEchoes  = fullfile(pathname, [nomFic  '_Echoes.xml']);
    Name.nomFicKML     = fullfile(pathname, [nomFic  '_Envelopes.kml']);
    
    Name.AreaName = [];
    Names(k) = Name; %#ok<AGROW>
end

%% Distances latérales min et max

if isempty(LastMinAcrossDist)
    minAcrossDist = -Inf;
    maxAcrossDist = Inf;
else
    minAcrossDist = LastMinAcrossDist;
    maxAcrossDist = LastMaxAcrossDist;
end

[flag, minAcrossDist, maxAcrossDist] = question_WCLimits3DMatrix(minAcrossDist, maxAcrossDist, ...
    'minMinAcrossDist', minAcrossDist, 'maxMaxAcrossDist', maxAcrossDist);
if ~flag
    return
end
LastMinAcrossDist = minAcrossDist;
LastMaxAcrossDist = maxAcrossDist;

%% Nombre max de pings par volume

if isempty(LastNbPingsMax)
    NbPingsMax = 2000;
else
    NbPingsMax = LastNbPingsMax;
end

str1 = 'Nombre max de pings par block';
str2 = 'Define the maximum number of pings of every 3D binary Matrix.';
[flag, NbPingsMax] = inputOneParametre(Lang(str1,str2), 'NbPingsMax', ...
    'Value', NbPingsMax, 'MinValue', 100);
if ~flag
    return
end
LastNbPingsMax = NbPingsMax;

%% Message //Tbx

if (StepDisplay == 0) && (N > 1)
    flag = messageParallelTbx(1);
    if ~flag
        return
    end
end
