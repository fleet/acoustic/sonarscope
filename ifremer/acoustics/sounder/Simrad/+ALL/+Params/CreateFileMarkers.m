% Description
%   CreateFileMarkers
%
% Syntax
%    flag, nomFicMarker, PlotNav] = ALL.Params.CreateFileMarkers(repExport, TypeDatagram)
%
% Intput Arguments
%   repExport    : 
%   TypeDatagram : 
%
% Output Arguments
%   flag         : 
%   nomFicMarker : 
%   PlotNav      : 
%
% Examples
%    flag, nomFicMarker, PlotNav] = ALL.Params.CreateFileMarkers(repExport, TypeDatagram)
%
% Author : JMA
% See also Authors

function [flag, nomFicMarker, PlotNav] = CreateFileMarkers(repExport, TypeDatagram)

PlotNav = [];

% if get_LevelQuestion == 1
% %     SonarScopeHelp('TutorialSurvey_Processing_All_DataCleaning_ByComparisonToADtm_UsingAThreshold.pdf');
% end

%% Nom du fichier de markers

filtre = fullfile(repExport, ['Markers_' TypeDatagram '.txt']);
[flag, nomFicMarker] = my_uiputfile({'*.txt'}, Lang('Nom du fichier de markers', 'Give a file name for markers'), filtre);
if ~flag
    return
end

%% Question "Plot the navigation ?"

str1 = 'Trav� de la navigation avec les markers ?';
str2 = 'Plot the navigation with the markers ?';
[rep, flag] = my_questdlg(Lang(str1,str2));
if ~flag
    return
end
PlotNav = (rep == 1);
