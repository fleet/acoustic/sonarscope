% Description
%   Select the way to fill the flagged samples of navigation in the SignalDialog window
%
% Syntax
%    [flag, CleanInterpolation] = ALL.Params.EditNav
%
% Output Arguments
%   flag               : 0=KO, 1=OK
%   CleanInterpolation : 'Interpolate' or 'Let them as NaN'
%
% Examples
%    [flag, CleanInterpolation] = ALL.Params.EditNav
%
% Author : JMA
% See also Authors

function [flag, CleanInterpolation] = EditNav

CleanInterpolation = [];

str{1} = 'Interpolate';
str{2} = 'Let them as NaN';
[rep, flag] = my_listdlg('Select the way to fill the flagged samples in the SignalDialog window', str, 'SelectionMode', 'Single');
if ~flag
    return
end

CleanInterpolation = (rep == 1);
