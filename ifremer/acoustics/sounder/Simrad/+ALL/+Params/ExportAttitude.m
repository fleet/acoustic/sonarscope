% Description
%   Params for ExportAttitude
%
% Syntax
%    [flag, typeFormat] = ALL.Params.ExportAttitude
%
% Output Arguments
%   flag       : 0=KO, 1=OK
%   typeFormat : Type of format (ASCII / NetCDF)
%
% Examples
%    [flag, typeFormat] = ALL.Params.ExportAttitude
%
% Author : JMA
% See also Authors

function [flag, typeFormat] = ExportAttitude

str{1} = 'ASCII';
str{2} = 'NetCDF';
[typeFormat, flag] = my_listdlg('Type of format', str, 'SelectionMode', 'Single');
if ~flag
    return
end
