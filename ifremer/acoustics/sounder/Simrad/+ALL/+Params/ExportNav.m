% Description
%   Select the output .xml file for Nav & Attitude for GLOBE display
%
% Syntax
%    [flag, nomFicXML, repExport] = ALL.Params.ExportNav(repExport)
%
% Input Arguments
%   repExport : The SonarScope default directory for file exports
%
% Output Arguments
%   flag      : 0=KO, 1=OK
%   nomFicXML : The .xml output file
%   repExport : The directory name of the .xml file
%
% Examples
%    [flag, nomFicXML, repExport] = ALL.Params.ExportNav(repExport)
%
% Author : JMA
% See also Authors

function [flag, nomFicXML, repExport] = ExportNav(repExport)

[flag, nomFicXML] = my_uiputfile('*.xml', 'IFREMER - SonarScope : XML file for Nav & Attitude for GLOBE file', repExport);
if ~flag
    return
end
repExport = fileparts(nomFicXML);
