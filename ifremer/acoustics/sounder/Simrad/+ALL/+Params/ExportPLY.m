% Description
%   Select the parameters of the CloudCompare PLY file export
%
% Syntax
%    [flag, LayerNames, repExport] = ALL.Params.ExportPLY(repExport)
%
% Input Arguments
%   repExport : The SonarScope default directory for file exports
%
% Output Arguments
%   flag       : 0=KO, 1=OK
%   LayerNames : Name of the layer to export
%   repExport  : The directory name of the .ply file
%
% Examples
%    [flag, LayerNames, repExport] = ALL.Params.ExportPLY(repExport)
%
% Author : JMA
% See also Authors

function [flag, LayerNames, repExport] = ExportPLY(repExport)

LayerNames = {}; 

%% Choix du layer

listeLayers = ALL.getListeLayers;
str1 = 'Layer � mosa�quer';
str2 = 'Layer to mosaic';

% % TODO : rebrancher la s�lection multiple quand l'export multivariable
% % sera branch� dans my_plywrite
% [choixLayer, flag] = my_listdlgMultiple(Lang(str1,str2), listeLayers,...
%     'InitialValue', 1);

InitialValue = find(ismember(listeLayers, 'Depth+Draught-Heave'));
if isempty(InitialValue)
    InitialValue = 1;
end
[choixLayer, flag] = my_listdlg(Lang(str1,str2), listeLayers,...
    'SelectionMode', 'Single', 'InitialValue', InitialValue);
if ~flag
    return
end
LayerNames = listeLayers(choixLayer);

%% R�pertoire d'export des .ply

str1 = 'Nom du r�pertoire o� les fichiers .ply seront copi�s.';
str2 = 'Name of the directory where the .ply files will be copied.';
[flag, repOut] = my_uigetdir(repExport, Lang(str1,str2));
if ~flag
    return
end
repExport = repOut;
