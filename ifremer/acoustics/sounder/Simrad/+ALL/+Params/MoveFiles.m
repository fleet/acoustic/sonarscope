% Description
%   Select the name of the directory where the .all, .wcd and cache directories will be moved
%
% Syntax
%    [flag, nomFicXML, repExport] = ALL.Params.MoveFiles(repExport)
%
% Input Arguments
%   repExport : The SonarScope default directory for file exports
%
% Output Arguments
%   flag      : 0=KO, 1=OK
%   repExport : The output directory name
%
% Examples
%    [flag, nomFicXML, repExport] = ALL.Params.MoveFiles(repExport)
%
% Author : JMA
% See also Authors

function [flag, repExport] = MoveFiles(repExport)

str1 = 'Nom du r�pertoire o� les .all, .wcd et r�pertoires de cache seront d�plac�s.';
str2 = 'Name of the directory where the .all, .wcd and cache directories will be moved.';
[flag, repOut] = my_uigetdir(repExport, Lang(str1,str2));
if ~flag
    return
end
repExport = repOut;
