% Description
%   Params for ReduceCsvFile
%
% Syntax
%    [flag, nomDirOut, timeStep] = ALL.Params.ReduceCsvFile(nomDir)
%
% Output Arguments
%   flag      : 0=KO, 1=OK
%   nomDirOut : Dir name for the output files
%   timeStep  : Time step in seconds
%
% Examples
%    [flag, nomDirOut, timeStep] = ALL.Params.ReduceCsvFile(my_tempdir)
%
% Author : JMA
% See also Authors

function [flag, nomDirOut, timeStep] = ReduceCsvFile(nomDirIn)

persistent persistent_nomDirOut persistent_timeStep

timeStep = 1;

%% Saisie du répertoire de sortie

if isempty(persistent_nomDirOut)
    persistent_nomDirOut = nomDirIn;
else
    nomDirIn = persistent_nomDirOut;
end

[flag, nomDirOut] = my_uigetdir(nomDirIn, 'Select or create the output folder');
if ~flag
    return
end
persistent_nomDirOut = nomDirIn;

%% Saisie du pas temporel d'échantillonnage

if isempty(persistent_timeStep)
    persistent_timeStep = timeStep;
else
    timeStep = persistent_timeStep;
end

 [flag, timeStep] = inputOneParametre('Reduction of timetables in .csv files', 'Time sampling', 'Value', timeStep, 'Unit', 's');
 if ~flag
    return
end
persistent_timeStep = timeStep;
