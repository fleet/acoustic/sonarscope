% Description
%   Select the filtering parameter of the swell
%
% Syntax
%    [flag, Periode] = ALL.Params.RidhaWaveFilter
%
% Output Arguments
%   flag    : 0=KO, 1=OK
%   Periode : Output parameter
%
% Examples
%    [flag, Periode] = ALL.Params.RidhaWaveFilter
%
% Author : JMA
% See also Authors

function [flag, Periode] = RidhaWaveFilter

%% P�riode de la houle

Periode = [5 10 20 50 100 200 500 1000];
for k=1:length(Periode)
    str{k} = sprintf('1/%d', Periode(k)); %#ok<AGROW>
end
[rep, flag] = my_listdlg('Liste of items', str, 'SelectionMode', 'Single');%, 'InitialValue', 2
if ~flag
    reeturn
end
Periode = Periode(rep);
