% Description
%   parameters for RnD_BottomDetectionSynthese
%
% Syntax
%     [flag, DisplayLevel, nomFicAvi, DepthMin, DepthMax, nomDirErs, BeamsToDisplay, IdentAlgo, repExport] ...
%     = ALL.Params.RnD_BottomDetectionSynthese(listFileNames, repExport)
%
% Input Arguments
%   listFileNames : The list of .all files
%   repExport     : The SonarScope default directory for file import
%
% Output Arguments
%   flag           : 0=KO, 1=OK
%   DisplayLevel   : Level of Intermediate display
%   nomFicAvi      : Name of the .avi file
%   DepthMin       : Min depth
%   DepthMax       : Max depth
%   nomDirErs      : Name of the directory to save ERS files
%   BeamsToDisplay : Level of Intermediate display
%   IdentAlgo      : (not used)
%   repExport      : The directory name of the .ply file
%
% Examples
%     [flag, DisplayLevel, nomFicAvi, DepthMin, DepthMax, nomDirErs, BeamsToDisplay, IdentAlgo, repExport] ...
%     = ALL.Params.RnD_BottomDetectionSynthese(listFileNames, repExport)
%
% Author : JMA
% See also Authors

function [flag, DisplayLevel, nomFicAvi, DepthMin, DepthMax, nomDirErs, BeamsToDisplay, ...
    IdentAlgo, repExport] = RnD_BottomDetectionSynthese(listFileNames, repExport)
 
DisplayLevel   = 3;
nomFicAvi      = [];
DepthMax       = [];
DepthMin       = [];
nomDirErs      = [];
BeamsToDisplay = [];
IdentAlgo      = 1;

%% Saisie de la distance maximale recherch�e

str1 = 'TODO';
str2 = 'Please give min and max Depth values if you can do it, keep initial values if not';
p(1) = ClParametre('Name', Lang('Profondeur min', 'Min Depth'), ...
    'Unit', 'm',  'MinValue', 1, 'MaxValue', 12000, 'Value', 1);
p(2) = ClParametre('Name', Lang('Profondeur max', 'Max Depth'), ...
    'Unit', 'm',  'MinValue', 10, 'MaxValue', 12000, 'Value', 12000);
a = StyledParametreDialog('params', p, 'Title', Lang(str1,str2));
% a.Help = 'http://flotte.ifremer.fr/fleet/Presentation-of-the-fleet/On-board-software/SonarScope';
a.sizes = [0 -3 -1 -2 -1 -1 -1 0];
a.openDialog;
flag = a.okPressedOut;
if ~flag
    return
end
Val = a.getParamsValue;
DepthMax = -max(Val);
DepthMin = -min(Val);

%% Saisie de mode de d�tection (� revalider quand un travail sera fait sur les �paves)

% [IdentAlgo, flag] = my_questdlg('Detection mode ?', 'Seabed', 'Wreck');
% if ~flag
%     return
% end

%% Nom de r�pertoire o� sauvegarder les fichiers

[rep, flag] = my_questdlg(Lang('Voulez-vous sauver les fichiers dans un r�pertoire ?', 'Save the files in a directory ?'));
if ~flag
    return
end
if rep == 2
    nomDirErs = [];
else
    [flag, nomDirErs] = my_uigetdir(repExport, Lang('R�pertoire de sauvegarde des fichiers', 'Directory to save files'));
    if ~flag
        return
    end
    repExport = nomDirErs;
end

%% Saisie du niveau de visualisation

%  BeamsToDisplay = 1:200
[flag, DisplayLevel] = question_DetailLevelBDA('Default', 1);
if ~flag
    return
end

%% Cr�ation d'un film ?

if DisplayLevel >= 2
    [CreateMovie, flag] = my_questdlg(Lang('Cr�ation d''un film ?', 'Create a movie ?'));
    if CreateMovie == 1
        [~, filename] = fileparts(listFileNames{1});
        nomFicAvi = fullfile(repExport, [filename '_BottomDetector.avi']);
        [flag, nomFicAvi] = my_uiputfile('*.avi', 'Give a file name', nomFicAvi);
        if ~flag
            return
        end
        repExport = fileparts(nomFicAvi);
    end
end
