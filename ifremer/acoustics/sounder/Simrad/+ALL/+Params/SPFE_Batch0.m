% Description
%   Params for SPFE_Batch0
%
% Syntax
%    [flag, surveyDirNames, skipSurveys, repImport] = ALL.Params.SPFE_Batch0(repImport)
%
% Input Arguments
%   repImport : The SonarScope default directory for input files
%
% Output Arguments
%   flag           : 0=KO, 1=OK
%   surveyDirNames : 
%   skipSurveys    : 
%   repImport      : The SonarScope default directory for input files
%
% Examples
%   repImport = 'F:\SPFE\SSC_TESTS_MARS2022';
%  [flag, surveyDirNames, ALim, gridSize, skipSurveys, repImport] = ALL.Params.SPFE_Batch0(repImport)
%    flag = ALL.Process.SPFE_Batch0(nomDir, ALim, gridSize, skipSurveys)
%
% Author : JMA
% See also Authors ALL.Process.SPFE_Batch0

function [flag, surveyDirNames, skipSurveys, repImport] = SPFE_Batch0(repImport)


surveyDirNames = {};
skipSurveys    = [];

%% R�pertoire des survey

[flag, nomDir] = my_uigetdir(repImport, 'Select the upper directory (the one that contains the survey folders)');
if ~flag
    return
end
repImport = nomDir;

%% S�lection des surveys

listeDir = listeDirOnDir(nomDir);
N = length(listeDir);
for k=N:-1:1
    surveyDirNames{k} = listeDir(k).name;
end
[rep, flag] = my_listdlgMultiple('List of survey directories', surveyDirNames, 'InitialValue', 1:N);
if ~flag
    return
end
surveyDirNames = surveyDirNames(rep);
N = length(surveyDirNames);

if N == 0
    flag = false;
    return
end
for k=1:N
    surveyDirNames{k} = fullfile(nomDir, surveyDirNames{k});
end


%% Skip surveys that are already processed

[flag, skipSurveys] = question_SkipAlreadyProcessedFiles(2, 'Title', 'Skip already processed surveys ?');
if ~flag
    return
end
