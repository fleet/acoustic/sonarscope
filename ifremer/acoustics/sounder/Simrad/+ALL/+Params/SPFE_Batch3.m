% Description
%   Params for SPFE_Batch3
%
% Syntax
%     [flag, surveyDirNames] = ALL.Params.SPFE_Batch3(this, repImport, ...)
%
% Input Arguments
%   this      : Instances de cl_image
%   repImport : The SonarScope default directory for input files
%
% Name-Value Pair Arguments
%   Mask : Ask uestion for a mask (Default : false)
%
% Output Arguments
%   flag               : 0=KO, 1=OK
%   surveyDirNames     :
%
% Examples
%   repImport = 'F:\SPFE\SSC_TESTS_MARS2022';
%   [flag, surveyDirNames, gridS] = ALL.Params.SPFE_Batch3(this, repImport)
%      flag = ALL.Process.SPFE_Batch3(surveyDirNames)
%
% Author : JMA
% See also Authors ALL.Process.SPFE_Batch3

function [flag, surveyDirNames, repImport] = SPFE_Batch3(repImport)

surveyDirNames = {};

%% R�pertoire des survey

[flag, nomDir] = my_uigetdir(repImport, 'Select the upper directory (the one that contains the survey folders)');
if ~flag
    return
end
repImport = nomDir;

%% S�lection des surveys

listeDir = listeDirOnDir(nomDir);
N = length(listeDir);
for k=N:-1:1
    surveyDirNames{k} = listeDir(k).name;
end
[rep, flag] = my_listdlgMultiple('List of survey directories', surveyDirNames, 'InitialValue', 1:N);
if ~flag
    return
end
surveyDirNames = surveyDirNames(rep);
N = length(surveyDirNames);

if N == 0
    flag = false;
    return
end
for k=1:N
    surveyDirNames{k} = fullfile(nomDir, surveyDirNames{k});
end
