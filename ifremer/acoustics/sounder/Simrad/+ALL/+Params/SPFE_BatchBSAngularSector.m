% Description
%   Params for SPFE_BatchBSAngularSector
%
% Syntax
%    [flag, surveyDirNames, ALim, gridSize, skipSurveys, repImport] = ALL.Params.SPFE_BatchBSAngularSector(repImport)
%
% Input Arguments
%   repImport : The SonarScope default directory for input files
%
% Output Arguments
%   flag           : 0=KO, 1=OK
%   surveyDirNames : 
%   ALim           : 
%   gridSize       : 
%   skipSurveys    : 
%   repImport      : The SonarScope default directory for input files
%
% Examples
%   repImport = 'F:\SPFE\SSC_TESTS_MARS2022';
%  [flag, surveyDirNames, ALim, gridSize, skipSurveys, repImport] = ALL.Params.SPFE_BatchBSAngularSector(repImport)
%    flag = ALL.Process.SPFE_BatchBSAngularSector(nomDir, ALim, gridSize, skipSurveys)
%
% Author : JMA
% See also Authors ALL.Process.SPFE_BatchBSAngularSector

function [flag, surveyDirNames, ALim, gridSize, skipSurveys, repImport] = SPFE_BatchBSAngularSector(repImport)

persistent persistent_ALim persistent_gridSize

surveyDirNames = {};
ALim           = [];
gridSize       = [];
skipSurveys    = [];

%% R�pertoire des survey

[flag, nomDir] = my_uigetdir(repImport, 'Select the upper directory (the one that contains the survey folders)');
if ~flag
    return
end
repImport = nomDir;

%% S�lection des surveys

listeDir = listeDirOnDir(nomDir);
N = length(listeDir);
for k=N:-1:1
    surveyDirNames{k} = listeDir(k).name;
end
[rep, flag] = my_listdlgMultiple('List of survey directories', surveyDirNames, 'InitialValue', 1:N);
if ~flag
    return
end
surveyDirNames = surveyDirNames(rep);
N = length(surveyDirNames);

if N == 0
    flag = false;
    return
end
for k=1:N
    surveyDirNames{k} = fullfile(nomDir, surveyDirNames{k});
end

%% Secteur angulaire

if isempty(persistent_ALim)
    ALim = [30 50];
else
    ALim = persistent_ALim;
end
[flag, ALim] = saisie_CLim(ALim(1), ALim(2), 'deg', 'SignalName', 'Angles');
if ~flag
    return
end
persistent_ALim = ALim;

%% Grid size

if isempty(persistent_gridSize)
    gridSize = 1;
else
    gridSize = persistent_gridSize;
end

str1 = 'Donnez un pas de grille pour les mosa�ques individuelles';
str2 = 'Give a grid size for the individual mosaics.';
[flag, gridSize] = inputOneParametre(Lang(str1,str2), 'Value', ...
    'Value', gridSize, 'Unit', 'm', 'MinValue', 0);
if ~flag
    return
end
persistent_gridSize = gridSize;

%% Skip surveys that are already processed

[flag, skipSurveys] = question_SkipAlreadyProcessedFiles(2, 'Title', 'Skip already processed surveys ?');
if ~flag
    return
end
