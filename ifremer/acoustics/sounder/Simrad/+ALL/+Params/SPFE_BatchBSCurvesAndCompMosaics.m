% Description
%   Params for SPFE_BatchBSCurvesAndCompMosaics
%
% Syntax
%     [flag, surveyDirNames, gridSize, skipSurveys, flagInteractivity, indLayeMaskLatLong, valMask, repImport] ...
%         = ALL.Params.SPFE_BatchBSCurvesAndCompMosaics(this, repImport, ...)
%
% Input Arguments
%   this      : Instances de cl_image
%   repImport : The SonarScope default directory for input files
%
% Name-Value Pair Arguments
%   Mask : Ask uestion for a mask (Default : false)
%
% Output Arguments
%   flag               : 0=KO, 1=OK
%   surveyDirNames     :
%   gridSize           :
%   skipSurveys        : 
%   flagInteractivity  : 
%   indLayeMaskLatLong : 
%   valMask            :
%   repImport          : The SonarScope default directory for input files
%
% Examples
%   repImport = 'F:\SPFE\SSC_TESTS_MARS2022';
%   [flag, surveyDirNames, gridSize, skipSurveys, flagInteractivity, indLayeMaskLatLong, valMask, repImport] ...
%   = ALL.Params.SPFE_BatchBSCurvesAndCompMosaics(this, repImport, ...)
%      flag = ALL.Process.SPFE_BatchBSCurvesAndCompMosaics(surveyDirNames, gridSize, skipSurveys, flagInteractivity, MaskLatLong, valMask)
%
% Author : JMA
% See also Authors ALL.Process.SPFE_BatchBSCurvesAndCompMosaics

function [flag, surveyDirNames, gridSize, skipSurveys, flagInteractivity, indLayeMaskLatLong, valMask, repImport] ...
    = SPFE_BatchBSCurvesAndCompMosaics(this, repImport, varargin)

persistent persistent_gridSize persistent_Interactivity

surveyDirNames     = {};
gridSize           = [];
skipSurveys        = [];
flagInteractivity  = [];
indLayeMaskLatLong = [];
valMask            = [];

QL = get_LevelQuestion;

%% R�pertoire des survey

[flag, nomDir] = my_uigetdir(repImport, 'Select the upper directory (the one that contains the survey folders)');
if ~flag
    return
end
repImport = nomDir;

%% S�lection des surveys

listeDir = listeDirOnDir(nomDir);
N = length(listeDir);
for k=N:-1:1
    surveyDirNames{k} = listeDir(k).name;
end
[rep, flag] = my_listdlgMultiple('List of survey directories', surveyDirNames, 'InitialValue', 1:N);
if ~flag
    return
end
surveyDirNames = surveyDirNames(rep);
N = length(surveyDirNames);

if N == 0
    flag = false;
    return
end
for k=1:N
    surveyDirNames{k} = fullfile(nomDir, surveyDirNames{k});
end

%% Grid size

if isempty(persistent_gridSize)
    gridSize = 1;
else
    gridSize = persistent_gridSize;
end

str1 = 'Donnez un pas de grille pour les mosa�ques individuelles';
str2 = 'Give a grid size for the individual mosaics.';
[flag, gridSize] = inputOneParametre(Lang(str1,str2), 'Value', ...
    'Value', gridSize, 'Unit', 'm', 'MinValue', 0);
if ~flag
    return
end
persistent_gridSize = gridSize;

%% Skip surveys that are already processed

[flag, skipSurveys] = question_SkipAlreadyProcessedFiles(2, 'Title', 'Skip already processed surveys ?');
if ~flag
    return
end

%% Recherche d'un masque d�fini en LatLong

if QL == 3
    str1 = 'Voulez-vous traiter la donn�e au travers d''un masque ?';
    str2 = 'Do you want to compute the data through a mask ?';
    [flagMask, flag] = my_questdlg(Lang(str1,str2), 'Init', 2');
    if ~flag
        return
    end
    if flagMask == 1
        identLayerMask = cl_image.indDataType('Mask');
        identLayerSegm = cl_image.indDataType('Segmentation');
        [indLayeMaskLatLong, nomsLayerMask] = findLayers(this, 'GeometryType', cl_image.indGeometryType('LatLong'), ...
            'DataType', [identLayerMask identLayerSegm]);
        if isempty(indLayeMaskLatLong)
            str1 = 'Il n''y a pas d''image de type "Mask" en g�om�trie "LatLong".';
            str2 = 'There is no image of "Mask" DataType and "LatLong" geometry".';
            my_warndlg(Lang(str1,str2), 1);
            flag = 0;
            return
        end

        str1 = 'S�lectionner le mask ?';
        str2 = 'Select the Mask layer';
        [rep, flag] = my_listdlg(Lang(str1,str2), nomsLayerMask, 'SelectionMode', 'Single');
        if ~flag
            return
        end
        indLayeMaskLatLong = indLayeMaskLatLong(rep);

        % TODO : une seule valeur pour le moment car SimradAll_Statistics_Curves pas encore pr�t pour multi masks
        valMask = selectValMask(this(indLayeMaskLatLong), 'SelectionMode', 'Single');
        %     CurveInfo.MaskLatLon.nomsLayerMask = nomsLayerMask;
        %     CurveInfo.MaskLatLon.valMask       = valMask;
    end
end

%% Niveaux d'interactivit�

if isempty(persistent_Interactivity)
    flagInteractivity = [0 1 1 0 1];
else
    flagInteractivity = persistent_Interactivity;
end

p    = ClParametre('Name', 'BS individual curves',     'MinValue', 0, 'MaxValue', 1, 'Value', 0, 'Format', '%d');
p(2) = ClParametre('Name', 'BS averaged curve',        'MinValue', 0, 'MaxValue', 1, 'Value', 1, 'Format', '%d');
p(3) = ClParametre('Name', 'BS optim dialog',          'MinValue', 0, 'MaxValue', 1, 'Value', 1, 'Format', '%d');
p(4) = ClParametre('Name', 'DiagTx individual curves', 'MinValue', 0, 'MaxValue', 1, 'Value', 0, 'Format', '%d');
p(5) = ClParametre('Name', 'DiagTx averaged curve',    'MinValue', 0, 'MaxValue', 1, 'Value', 1, 'Format', '%d');
a = StyledParametreDialog('params', p, 'Title', 'Batch BS Curves & Comp Mosaics');
a.sizes = [0 -2 0 -1 0 -1 0 0];
a.openDialog;
flag = a.okPressedOut;
if ~flag
    return
end
flagInteractivity = a.getParamsValue;
persistent_Interactivity = flagInteractivity;
