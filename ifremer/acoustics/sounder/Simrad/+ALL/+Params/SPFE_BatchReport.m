% Description
%   Params for SPFE_BatchReport
%
% Syntax
%    [flag, surveyDirNames, CsvOutputFile, repImport] = ALL.Params.SPFE_BatchReport(repImport)
%
% Input Arguments
%   repImport : The SonarScope default directory for file import
%
% Output Arguments
%   flag           : 0=KO, 1=OK
%   surveyDirNames : Name of the directory names
%   CsvOutputFile  : Name of the output .csv file
%   repImport      : The new SonarScope default directory for file import
%
% Examples
%    repImport = 'F:\SPFE\SSC_TESTS_MARS2022;
%    [flag, surveyDirNames, CsvOutputFile, repImport] = ALL.Params.SPFE_BatchReport(repImport)
%      flag = ALL.Process.SPFE_BatchReport(surveyDirNames, CsvOutputFile) 
%
% Author : JMA
% See also Authors ALL.Process.SPFE_BatchReport

function [flag, surveyDirNames, CsvOutputFile, repImport] = SPFE_BatchReport(repImport)

surveyDirNames = {};
CsvOutputFile  = [];

%% R�pertoire des survey

[flag, nomDir] = my_uigetdir(repImport, 'Select the upper directory (the one that contains the survey folders)');
if ~flag
    return
end
repImport = nomDir;

%% S�lection des surveys

listeDir = listeDirOnDir(nomDir);
N = length(listeDir);
for k=N:-1:1
    surveyDirNames{k} = listeDir(k).name;
end
[rep, flag] = my_listdlgMultiple('List of survey directories', surveyDirNames, 'InitialValue', 1:N);
if ~flag
    return
end
surveyDirNames = surveyDirNames(rep);
N = length(surveyDirNames);

if N == 0
    flag = false;
    return
end
for k=1:N
    surveyDirNames{k} = fullfile(nomDir, surveyDirNames{k});
end

%% Name of the output file

% CsvOutputFile = fullfile(dirName, [nameSurvey '_BS.csv']);
[flag, CsvOutputFile] = my_uiputfile('.csv', 'Save as', nomDir);
if ~flag
    return
end
