% Description
%   Params for SPFE_BatchReport
%
% Syntax
%    [flag, nomFicPoints, nomDirOut, pourcentageDepth, repImport, repExport] = ALL.Params.SPFE_CreateMasksForMarcRoche(repImport, repExport)
%
% Input Arguments
%   repImport : The SonarScope default directory for file import
%   repExport : The SonarScope default directory for file export
%
% Output Arguments
%   flag             : 0=KO, 1=OK
%   nomFicPoints     : 
%   nomDirOut        : 
%   pourcentageDepth : 
%   repImport        : The SonarScope default directory for file import
%   repExport        : The SonarScope default directory for file export
%
% Examples
%    [flag, nomFicPoints, nomDirOut, pourcentageDepth, repImport, repExport] = ALL.Params.SPFE_CreateMasksForMarcRoche(repImport, repExport)
%      ALL_CreateMasksForMarcRoche(aKM, nomFicPoints, nomDirOut, pourcentageDepth)
%
% Author : JMA
% See also Authors ALL.Process.SPFE_BatchReport


function [flag, nomFicPoints, nomDirOut, pourcentageDepth, repImport, repExport] = SPFE_CreateMasksForMarcRoche(repImport, repExport)

persistent persistent_PourcentageDepth

nomDirOut        = [];
pourcentageDepth = [];

%% Nom du fichier de points géographiques

[flag, nomFicPoints] = uiSelectFiles('ExtensionFiles', '.txt', 'AllFilters', 1, 'RepDefaut', repImport, 'MessageParallelTbx', 1);
if ~flag
    return
end

%% Largeur de la frame

if isempty(persistent_PourcentageDepth)
    pourcentageDepth = 100;
else
    pourcentageDepth = persistent_PourcentageDepth;
end

p    = ClParametre('Name', '% of Depth',    'Unit', '%', 'MinValue', 50,   'MaxValue', 200, 'Value', pourcentageDepth);
a = StyledParametreDialog('params', p, 'Title', 'Mask caracteristics');
a.sizes = [0 -2 0 0 0 -2 -1 0];
a.openDialog;
if ~flag
    return
end
pourcentageDepth = a.getParamsValue;
persistent_PourcentageDepth = pourcentageDepth;

%% Sélection des répertoire de sortie

[flag, nomDirOut] = my_uigetdir(repExport, 'Enter a directory for output Mask files');
if ~flag
    return
end
repExport = nomDirOut;
