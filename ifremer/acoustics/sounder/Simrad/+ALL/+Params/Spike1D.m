% Description
%   Select the output .xml file for Nav & Attitude for GLOBE display
%
% Syntax
%    [flag, nbRepeats, Threshold, StepDisplay] = ALL.Params.Spike1D
%
% Output Arguments
%   flag        : 0=KO, 1=OK
%   nbRepeats   : Nb iterations
%   Threshold   : Threshold
%   StepDisplay : Display ping recurrence
%
% Examples
%    [flag, nbRepeats, Threshold, StepDisplay] = ALL.Params.Spike1D
%
% Author : JMA
% See also Authors

function [flag, nbRepeats, Threshold, StepDisplay] = Spike1D 

persistent persistent_StepDisplay

nbRepeats   = [];
Threshold   = [];
StepDisplay = 0;

str1 = 'Paramètres du filtrage de spike sur des pings de fichiers .all';
str2 = 'Ping spike filter parameters on .all files';

p    = ClParametre('Name', Lang('Itérations', 'Nb iterations'),     'Value', 1, 'MinValue', 1, 'MaxValue', 5);
p(2) = ClParametre('Name', Lang('Seuil', 'Threshold'), 'Unit', 'm', 'Value', 0, 'MinValue', 0);
a = StyledSimpleParametreDialog('params', p, 'Title', Lang(str1,str2));
a.openDialog;
flag = a.okPressedOut;
if ~flag
    return
end
value = a.getParamsValue;

nbRepeats = value(1);
Threshold = value(2);

%% Récurence de l'affichage des pings

if isempty(persistent_StepDisplay)
    StepDisplay = 0;
else
    StepDisplay = persistent_StepDisplay;
end

str1 = 'Récurrence d''affichage des pings';
str2 = 'Recurrence of ping s display';
Title = Lang(str1,str2);

str1 = 'Récurrence (0 = pas de visu)';
str2 = 'Recurrence (0 = no plot)';
p = ClParametre('Name', Lang(str1,str2), 'Unit', 'Pings',  'MinValue', 0, 'MaxValue', 1000, 'Value', StepDisplay, 'Format', '%d');
a = StyledParametreDialog('params', p, 'Title', Title);
% a.Help = 'http://flotte.ifremer.fr/fleet/Presentation-of-the-fleet/On-board-software/SonarScope';
a.sizes = [0 -2 0 -2 0 -1 -1 0];
a.openDialog;
flag = a.okPressedOut;
if ~flag
    return
end
StepDisplay = a.getParamsValue;

persistent_StepDisplay = StepDisplay;
