% Description
%   Parameters for the summary of the datagrams
%
% Syntax
%    [flag, listDatagrams, nomFicSummary, repExport] = ALL.Params.SummaryDatagrams(DefaultCsvName, repExport)
%
% Input Arguments
%   DefaultCsvName : Default name for the summary file
%   repExport      : The SonarScope default directory for file exports
%
% Output Arguments
%   flag          : 0=KO, 1=OK
%   listDatagrams : List of the selected datagrams
%   nomFicSummary : File name of the .csv summary file
%   repExport     : The directory name of the .xml file
%
% Examples
%    [flag, listDatagrams, nomFicSummary, repExport] = ALL.Params.SummaryDatagrams(DefaultCsvName, repExport)
%
% Author : JMA
% See also Authors

function [flag, listDatagrams, nomFicSummary, repExport] = SummaryDatagrams(DefaultCsvName, repExport)

listDatagrams = [];
nomFicSummary = [];

%% S�lection des items du r�sum�

str{1}     = 'Depth';
str{end+1} = 'RawRangeBeamAngle';
str{end+1} = 'SeabedImage';
str{end+1} = 'Runtime';
str{end+1} = 'SurfaceSoundSpeed';
str{end+1} = 'SoundSpeedProfile';
str{end+1} = 'WaterColumn';
str{end+1} = 'InstallationParameters';
str{end+1} = 'Attitude';
str{end+1} = 'Clock';
str{end+1} = 'Position';
str{end+1} = 'Heading';
str{end+1} = 'Height';
str{end+1} = 'VerticalDepth';
str{end+1} = 'QualityFactor';
str{end+1} = 'StaveData';
str{end+1} = 'ExtraParameters';

str1 = 'Datagramms � r�sumer.';
str2 = 'Datagrams to be summarized.';
[SelectionDatagrams, flag] = my_listdlg(Lang(str1,str2), str, 'InitialValue', 1:length(str));
if ~flag
    return
end
listDatagrams = str(SelectionDatagrams);

%% D�finition du fichier .csv

str1 = 'IFREMER - SonarScope - Nom du fichier R�sum�';
str2 = 'IFREMER - SonarScope - Summary file name';
[flag, nomFicSummary] = my_uiputfile('*.csv', Lang(str1,str2), fullfile(repExport, DefaultCsvName));
if ~flag
    return
end
repExport = fileparts(nomFicSummary);
