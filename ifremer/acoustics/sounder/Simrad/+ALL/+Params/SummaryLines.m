% Description
%   Parameters for the summary of the datagrams
%
% Syntax
%    [flag, nomFicSummary, listeItems, repExport] = ALL.Params.SummaryLines(DefaultCsvName, repExport)
%
% Input Arguments
%   DefaultCsvName : Default name for the summary file
%   repExport      : The SonarScope default directory for file exports
%
% Output Arguments
%   flag          : 0=KO, 1=OK
%   nomFicSummary : File name of the .csv summary file
%   listeItems    : List of items to summay (asked only if Question Leve = 3)
%   repExport     : The directory name of the .xml file
%
% Examples
%    [flag, nomFicSummary, listeItems, repExport] = ALL.Params.SummaryLines(DefaultCsvName, repExport)
%
% Author : JMA
% See also Authors

function [flag, nomFicSummary, listeItems, repExport] = SummaryLines(DefaultCsvName, repExport)

nomFicSummary = []; 

%% S�lection des items

strHeader = ALL.createListAllItems;
[flag, listeItems] = ALL.Params.Summary_Lines(strHeader);
if ~flag
    return
end

%% D�finition du fichier .csv

str1 = 'IFREMER - SonarScope - Nom du fichier R�sum�';
str2 = 'IFREMER - SonarScope - Summary file name';
[flag, nomFicSummary] = my_uiputfile('*.csv', Lang(str1,str2), fullfile(repExport, DefaultCsvName));
if ~flag
    return
end
repExport = fileparts(nomFicSummary);
