% Description
%   Selection of the list of the items of the summary
%
% Syntax
%    [flag, listeItems] = ALL.Params.Summary_Lines(strHeader)
%
% Input Arguments
%   strHeader : List of the available items
%
% Output Arguments
%   flag       : 0=KO, 1=OK
%   listeItems : Selected list of items
%
% Examples
%    [flag, listeItems] = ALL.Params.Summary_Lines(strHeader)
%
% Author : JMA
% See also Authors

function [flag, listeItems] = Summary_Lines(strHeader)

QL = get_LevelQuestion;
if QL < 3
    flag = 1;
    listeItems = [];
    return
end

%% Recherche du fichier d'initialisation personnel

nomFicDefaut = fullfile(my_tempdir, 'AllSummaryItemsDefault.txt');
if exist(nomFicDefaut, 'file')
    T = readtable(nomFicDefaut);
    if size(T,1) == length(strHeader)
        ident = T.ident;
    else
        ident = 1:length(strHeader);
    end
else
    ident = 1:length(strHeader);
end

%% Choix des items

str1 = 'Liste des items du r�sum�.';
str2 = 'List of the items of the summary.';
[flag, ~, listeItems] = uiSelectItems('Titre', Lang(str1,str2), ...
    'ItemsToSelect', strHeader, 'InitValue', ident, 'ItemsOnRightPanel', ident, 'ItemNames', 'Parameters');
if ~flag
    return
end

%% Sauvegarde du choix des items dans le fichier d'initialisation

ident = listeItems;
FieldName = strHeader(ident);
FieldName = FieldName(:);
ident     = ident(:);
T = table(FieldName, ident);
writetable(T, nomFicDefaut)
