% Description
%   Params of WC_CreatePingBeamEI
%
% Syntax
%    [flag, typeAlgoWC, resol, Tag, nomDirOut, Display, repExport] = ALL.Params.WC_CreatePingBeamEI(repExport)
%
% Input Arguments
%   repExport : The SonarScope default directory for file exports
%
% Output Arguments
%   flag       : 0=KO, 1=OK
%   typeAlgoWC :
%   resol      :
%   Tag        :
%   nomDirOut  :
%   Display    :
%   repExport  : The directory name of the .xml file
%
% Examples
%    [flag, typeAlgoWC, resol, Tag, nomDirOut, Display, repExport] = ALL.Params.WC_CreatePingBeamEI(repExport)
%
% Author : JMA
% See also Authors

function [flag, typeAlgoWC, resol, Tag, nomDirOut, Display, repExport] = WC_CreatePingBeamEI(repExport)
 
Tag        = '';
typeAlgoWC = 1;
nomDirOut  = [];
Display    = 0;

%% Grid size

[flag, resol] = get_gridSize; %('value', NaN);
if ~flag
    return
end

%% Nom du r�pertoire d'export des r�sultats

str1 = 'Nom du r�pertoire d''export des r�sultats.';
str2 = 'Name of the results output directory.';
[flag, nomDirOut] = my_uigetdir(repExport, Lang(str1,str2));
if ~flag
    return
end

%% Visualisation interm�diaire des r�sultats

str1 = ['Visualisation des r�sultats dans des fen�tres matlab ?' newline '(R�pondez Non si vous voulez profiter du calcul parallele)'];
str2 = ['Display the results in matlab figures ? ' newline '(say No if you want to take advantage of //Tbx)'];
[ind, flag] = my_questdlg(Lang(str1,str2), 'Init', 2);
if ~flag
    return
end
Display = (ind == 1);
