% Description
%   Params for WC_ExportRawData23DV
%
% Syntax
%    [flag, nomDirOut, TypeDataWC, Tag, MaskAfterDetection, ...
%     CLimRaw, flagTideCorrection, typeAlgoWC, repExport] =  WC_ExportRawData23DV(repExport)
%
% Input Arguments
%   repExport : The SonarScope default directory for file exports
%
% Output Arguments
%   flag               : 0=KO, 1=OK
%   nomDirOut          : 
%   TypeDataWC         : 
%   Tag                : 
%   MaskAfterDetection : 
%   CLimRaw            : 
%   flagTideCorrection : 
%   typeAlgoWC         : 
%   repExport          : The directory name of the .xml file
%
% Examples
%    [flag, nomDirOut, TypeDataWC, Tag, MaskAfterDetection, ...
%     CLimRaw, flagTideCorrection, typeAlgoWC, repExport] =  WC_ExportRawData23DV(repExport)
%
% Author : JMA
% See also Authors

function [flag, nomDirOut, TypeDataWC, Tag, MaskAfterDetection, ...
    CLimRaw, flagTideCorrection, typeAlgoWC, repExport] =  WC_ExportRawData23DV(repExport)
 
persistent LastCLimRaw

nomDirOut           = [];
Tag                 = '';
MaskAfterDetection  = 0;
CLimRaw             = [];
flagTideCorrection  = 0;
typeAlgoWC          = 1;

%% Type de donn�es

str1 = 'Type des donn�es de la colonne d''eau';
str2 = 'Data type for the water column';
str = {Lang('Donn�es brutes', 'Raw data'); Lang('Donn�es compens�es', 'Compensated data')};
[TypeDataWC, flag] = my_listdlg(Lang(str1,str2), str, 'InitialValue', 1:2);
if ~flag
    return
end

%% Rehaussement de contraste des donn�es brutes

if any(TypeDataWC == 1)
    str1 = 'Rehaussement de contraste';
    str2 = 'Select the contrast enhancement method';
    [rep, flag] = my_questdlg(Lang(str1,str2), 'Auto', Lang('Manuel', 'Manual'));
    if ~flag
        return
    end
    if rep == 2
        if isempty(LastCLimRaw)
            CLimRaw = [-64 40];
        else
            CLimRaw = LastCLimRaw;
        end
         
        str1 = 'IFREMER - SonarScope : Bornes du rehaussement de contraste';
        str2 = 'IFREMER - SonarScope : Color limits (contrast enhancement)';
        p(1) = ClParametre('Name', Lang('Valeur min', 'Min value'), ...
            'Unit', 'dB',  'MinValue', CLimRaw(1), 'MaxValue', 0, 'Value', CLimRaw(1));
        p(2) = ClParametre('Name', Lang('Valeur max', 'Max value'), ...
            'Unit', 'dB',  'MinValue', 0, 'MaxValue', CLimRaw(2), 'Value', CLimRaw(2));
        a = StyledParametreDialog('params', p, 'Title', Lang(str1,str2));
        % a.Help = 'http://flotte.ifremer.fr/fleet/Presentation-of-the-fleet/On-board-software/SonarScope';
        a.sizes = [0 -3 -1 -2 -1 -1 -1 0];
        a.openDialog;
        flag = a.okPressedOut;
        if ~flag
            return
        end
        CLimRaw = a.getParamsValue;
        LastCLimRaw = CLimRaw;
    end
end

%% Masquage au del� de la d�tection

[flag, MaskAfterDetection] = question_MaskWCBeyondDetection;
if ~flag
    return
end

%% Tag suppl�mentaire dans le mom des images

[flag, Tag] = question_AddTagInOutputFile;
if ~flag
    return
end

%% Prise en compte de la mar�e

str1 = 'Voulez-vous tenir compte de la mar�e ?';
str2 = 'Do you want to take the tide into account ?';
[rep, flag] = my_questdlg(Lang(str1,str2), 'Init', 2);
if ~flag
    return
end
flagTideCorrection = (rep == 1);

%% Type d'algorithme

[flag, typeAlgoWC] = question_WCRaytracingAlgorithm('InitialValue', 2);
if ~flag
    return
end

%% R�pertoire cible

str1 = 'Choisissez ou cr�ez le r�pertoire o� seront stock�s les fichiers.';
str2 = 'Select or create the directory in which the files will be created.';
[flag, nomDirOut] = my_uigetdir(repExport, Lang(str1,str2));
if ~flag
    return
end
repExport = nomDirOut;
