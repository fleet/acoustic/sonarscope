% Description
%   Params for WaterColumnUncompressFiles
%
% Syntax
%    [flag, ListeFic, DeleteFile, repImport] = ALL.Params.WaterColumnUncompressFiles(repImport)
%
% Input Arguments
%   repImport : The SonarScope default directory for file imports
%
% Output Arguments
%   flag       : 0=KO, 1=OK
%   ListeFic   : 
%   DeleteFile : 
%   repImport  : The new SonarScope default directory for file imports
%
% Examples
%    [flag, ListeFic, DeleteFile, repImport] = ALL.Params.WaterColumnUncompressFiles(repImport)
%
% Author : JMA
% See also Authors

function [flag, ListeFic, DeleteFile, repImport] = WaterColumnUncompressFiles(repImport)

DeleteFile = 0;

[flag, ListeFic, lastDir] = uiSelectFiles('ExtensionFiles', '.zip', 'AllFilters', 1, ...
    'RepDefaut', repImport, 'ChaineExclue', 'SonarScopeLog');
if ~flag
    return
end
repImport = lastDir;

str1 = 'Suppression des fichiers apr�s la compression ?';
str2 = 'Supress files after compression ?';
[rep, flag] = my_questdlg(Lang(str1,str2));
if ~flag
    return
end
DeleteFile = (rep == 1);
