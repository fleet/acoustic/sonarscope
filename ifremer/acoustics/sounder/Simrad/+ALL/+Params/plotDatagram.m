% Description
%   Params for plotDatagram
%
% Syntax
%    [flag, listOptions] = ALL.Params.plotDatagram
%
% Output Arguments
%   flag        : 0=KO, 1=OK
%   listOptions : 
%
% Examples
%    [flag, listOptions] = ALL.Params.plotDatagram
%
% Author : JMA
% See also Authors

function [flag, listOptions] = plotDatagram

listOptions = [];

Option = {'Index'; 'Position'; 'Runtime'; 'Attitude'; 'Clock'; 'SoundSpeedProfile'; 'SurfaceSoundSpeed'; 'Height'; 'SeabedImageParameters'; 'Installation parameters'};
[rep, flag] = my_listdlg(Lang('Sélectionnez un ou plusieurs signaux', 'Select some signals'), Option);
if ~flag
    return
end
listOptions = Option(rep);
