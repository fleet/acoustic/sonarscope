function ComputeEchogramsGlobe(this, TypeWCOutput, Names, varargin)

[varargin, StepDisplay] = getPropertyValue(varargin, 'StepDisplay', 0);

if isempty(this)
    return
end

useParallel = get_UseParallelTbx;

%% D�tection si on peut faire du calcul parall�le

N = length(this);
if (StepDisplay == 0) && (N > 2)
    useParallelHere = useParallel;
else
    useParallelHere = 0;
end

%% Traitement des fichiers

Carto = [];
str1 = 'Traitement de la colonne d''eau Kongsberg en g�om�trie DepthAcrossDist';
str2 = 'Kongsberg Water Column processing in DepthAcrossDist geometry';
if useParallelHere
    % R�cup�ration de Carto
    nomDir = fileparts(get(this(1), 'nomFic'));
    [flag, Carto] = test_ExistenceCarto_Folder(nomDir);
    if ~flag || isempty(Carto)
        [~, Carto] = get_Layer(this(1), 'Depth', 'Carto', Carto);
    end
    
    [hw, DQ] = create_parforWaitbar(N, Lang(str1,str2));
    parfor (kFic=1:N, useParallelHere)
        process_SimradAll_EchogramsGlobeFlyTexture_unitaire(this(kFic), ...
            Carto, TypeWCOutput, Names(kFic), StepDisplay, 'isUsingParfor', true, varargin{:}); %#ok<PFBNS>
        send(DQ, kFic);
    end
    my_close(hw, 'MsgEnd');
else
    hw = create_waitbar(Lang(str1,str2), 'N', N);
    for kFic=1:N
        my_waitbar(kFic, N, hw)
        Carto = process_SimradAll_EchogramsGlobeFlyTexture_unitaire(this(kFic), ...
            Carto, TypeWCOutput, Names(kFic), StepDisplay, varargin{:});
    end
    my_close(hw, 'MsgEnd');
end


function Carto = process_SimradAll_EchogramsGlobeFlyTexture_unitaire(aKM, Carto, TypeWCOutput, Names, StepDisplay, varargin)

global isUsingParfor %#ok<GVMIS>

[varargin, minAcrossDist]             = getPropertyValue(varargin, 'minAcrossDist',             []);
[varargin, maxAcrossDist]             = getPropertyValue(varargin, 'maxAcrossDist',             []);
[varargin, suby]                      = getPropertyValue(varargin, 'suby',                      []);
[varargin, CorrectionTVG]             = getPropertyValue(varargin, 'CorrectionTVG',             30);
[varargin, CLimRaw]                   = getPropertyValue(varargin, 'CLimRaw',                   []);
[varargin, CompReflec]                = getPropertyValue(varargin, 'CompReflec',                []);
[varargin, ModeDependant]             = getPropertyValue(varargin, 'ModeDependant',             []);
[varargin, StepCompute]               = getPropertyValue(varargin, 'StepCompute',               1);
[varargin, flagTideCorrection]        = getPropertyValue(varargin, 'TideCorrection',            0);
[varargin, typeAlgoWC]                = getPropertyValue(varargin, 'typeAlgoWC',                2);
[varargin, MaskBeyondDetection]       = getPropertyValue(varargin, 'MaskBeyondDetection',       0);
[varargin, skipAlreadyProcessedFiles] = getPropertyValue(varargin, 'skipAlreadyProcessedFiles', 0);
[varargin, IndNavUnit]                = getPropertyValue(varargin, 'IndNavUnit',                []);
[varargin, MaxImageSize]              = getPropertyValue(varargin, 'MaxImageSize',              []);
[varargin, Mute]                      = getPropertyValue(varargin, 'Mute',                      0);
[varargin, isUsingParfor]             = getPropertyValue(varargin, 'isUsingParfor',             isUsingParfor); %#ok<ASGLU>

%% Test si le fichier a d�j� �t� trait�

if skipAlreadyProcessedFiles
    if ~isempty(Names.nomFicNetcdf) && exist(Names.nomFicNetcdf, 'file')
        return
    end
end

%% Traitement du fichier

ListeLayersV1 = 24;
ListeLayersV2 = SelectionLayers_V1toV2(ListeLayersV1);

VersionDatagram = version_datagram(aKM);
switch VersionDatagram
    case 'V1'
        [b, Carto] = import_PingBeam_All_V1(aKM, 'Carto', Carto, 'ListeLayers', ListeLayersV1, ...
            'TideCorrection', flagTideCorrection, 'IndNavUnit', IndNavUnit, 'Mute', Mute); % TODO : pas s�r que flagTideCorrection soit utile ici
        b = b(1);
    case 'V2'
        [b, Carto] = import_PingBeam_All_V2(aKM, 'Carto', Carto, 'ListeLayers', ListeLayersV2, ...
            'TideCorrection', flagTideCorrection, 'IndNavUnit', IndNavUnit, 'Mute', Mute); % TODO : pas s�r que flagTideCorrection soit utile ici
end
if isempty(b)
    return
end

if isempty(suby)
    subyFile = 1:b.nbRows;
else
    subyFile = suby;
end
% nbPings = length(subyFile);

sonar_SimradProcessWC_GlobeFlyTexture(b, get(aKM, 'nomFic'), ...
    'TypeWCOutput',        TypeWCOutput, ...
    'Names',               Names, ...
    'Carto',               Carto, ...
    'StepCompute',         StepCompute, ...
    'suby',                subyFile, ...
    'minAcrossDist',       minAcrossDist, ...
    'maxAcrossDist',       maxAcrossDist, ...
    'CorrectionTVG',       CorrectionTVG, ...
    'CLimRaw',             CLimRaw, ...
    'CompReflec',          CompReflec, ...
    'ModeDependant',       ModeDependant, ...
    'TideCorrection',      flagTideCorrection, ...
    'typeAlgoWC',          typeAlgoWC, ...
    'MaskBeyondDetection', MaskBeyondDetection, ...
    'IndNavUnit',          IndNavUnit, ...
    'MaxImageSize',        MaxImageSize, ...
    'StepDisplay',         StepDisplay, ...
    'Mute',                Mute);

%  profile report

%% Garbage Collector de Java

java.lang.System.gc
