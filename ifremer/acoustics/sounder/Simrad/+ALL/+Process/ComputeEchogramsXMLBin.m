function flag = ComputeEchogramsXMLBin(this, TypeWCOutput, Names, varargin)

global isUsingParfor %#ok<GVMIS>

flag = 0;

isUsingParfor = 0;

[varargin, Mute]        = getPropertyValue(varargin, 'Mute',        0);
[varargin, StepDisplay] = getPropertyValue(varargin, 'StepDisplay', 0);

useParallel = get_UseParallelTbx;

if isempty(this)
    return
end

%% D�tection si on peut faire du calcul parall�le

N = numel(this);
if (StepDisplay == 0) && (N > 2)
    useParallelHere = useParallel;
else
    useParallelHere = 0;
end

%% Traitement des fichiers

Carto = [];
str1 = 'Traitement de la colonne d''eau Kongsberg en g�om�trie DepthAcrossDist';
str2 = 'Kongsberg Water Column processing in DepthAcrossDist geometry';
if useParallelHere
    % R�cup�ration de Carto
    nomFic = get_nomFic(this(1));
    if iscell(nomFic)
        nomFic = nomFic{1};
    end
    nomDir = fileparts(nomFic);
    [flag, Carto] = test_ExistenceCarto_Folder(nomDir);
    if ~flag || isempty(Carto)
        [~, Carto] = get_Layer(this(1), 'Depth', 'Carto', Carto);
    end
    
    % Lancement du traitement avec la // Tbx
    [hw, DQ] = create_parforWaitbar(N, Lang(str1,str2));
    parfor (kFic=1:N, useParallelHere)
        ComputeEchogramsXMLBin_unitaire(this(kFic), Carto, TypeWCOutput, Names(kFic), StepDisplay, ...
            'isUsingParfor', true, varargin{:}); %#ok<PFBNS>
        send(DQ, kFic);
    end
    my_close(hw, 'MsgEnd');
else
    hw = create_waitbar(Lang(str1,str2), 'N', N, 'NoWaitbar', Mute);
    for kFic=1:N
        my_waitbar(kFic, N, hw)
        Carto = ComputeEchogramsXMLBin_unitaire(this(kFic), Carto, TypeWCOutput, Names(kFic), StepDisplay, ...
            'Mute', Mute, varargin{:});
    end
    my_close(hw, 'MsgEnd');
end
flag = 1;


function Carto = ComputeEchogramsXMLBin_unitaire(aKM, Carto, TypeWCOutput, Names, StepDisplay, varargin)

global isUsingParfor %#ok<GVMIS>

[varargin, minAcrossDist]             = getPropertyValue(varargin, 'minAcrossDist',             []);
[varargin, maxAcrossDist]             = getPropertyValue(varargin, 'maxAcrossDist',             []);
[varargin, suby]                      = getPropertyValue(varargin, 'suby',                      []);
[varargin, nbPingsMax]                = getPropertyValue(varargin, 'nbPingsMax',                500);
[varargin, CorrectionTVG]             = getPropertyValue(varargin, 'CorrectionTVG',             30);
[varargin, CLimRaw]                   = getPropertyValue(varargin, 'CLimRaw',                   []);
[varargin, CLimComp]                  = getPropertyValue(varargin, 'CLimComp',                  []);
[varargin, CompReflec]                = getPropertyValue(varargin, 'CompReflec',                []);
[varargin, ModeDependant]             = getPropertyValue(varargin, 'ModeDependant',             []);
[varargin, StepCompute]               = getPropertyValue(varargin, 'StepCompute',               1);
[varargin, flagTideCorrection]        = getPropertyValue(varargin, 'TideCorrection',            0);
[varargin, typeAlgoWC]                = getPropertyValue(varargin, 'typeAlgoWC',                2);
[varargin, MaskBeyondDetection]       = getPropertyValue(varargin, 'MaskBeyondDetection',       0);
[varargin, skipAlreadyProcessedFiles] = getPropertyValue(varargin, 'skipAlreadyProcessedFiles', 0);
[varargin, IndNavUnit]                = getPropertyValue(varargin, 'IndNavUnit',                []);
[varargin, Mute]                      = getPropertyValue(varargin, 'Mute',                      0);
[varargin, isUsingParfor]             = getPropertyValue(varargin, 'isUsingParfor',             isUsingParfor); %#ok<ASGLU>

%% Test si le fichier a d�j� �t� trait�

if skipAlreadyProcessedFiles
    if ~isempty(Names.nomFicXmlRaw) ...
            && exist(Names.nomFicXmlRaw, 'file') ...
            && exist(Names.nomFicXmlRaw(1:end-4), 'dir') ...
            && ~isempty(Names.nomFicXmlComp) ...
            && exist(Names.nomFicXmlComp, 'file') ...
            && exist(Names.nomFicXmlComp(1:end-4), 'dir')
        return
    end
end

%% Traitement du fichier

ListeLayersV1 = 24;
ListeLayersV2 = SelectionLayers_V1toV2(ListeLayersV1);

VersionDatagram = version_datagram(aKM);
switch VersionDatagram
    case 'V1'
        [b, Carto] = import_PingBeam_All_V1(aKM, 'Carto', Carto, 'ListeLayers', ListeLayersV1, ...
            'TideCorrection', flagTideCorrection, 'IndNavUnit', IndNavUnit, 'Mute', Mute); % TODO : pas s�r que flagTideCorrection soit utile ici
        b = b(1);
    case 'V2'
        [b, Carto] = import_PingBeam_All_V2(aKM, 'Carto', Carto, 'ListeLayers', ListeLayersV2, ...
            'TideCorrection', flagTideCorrection, 'IndNavUnit', IndNavUnit, 'Mute', Mute); % TODO : pas s�r que flagTideCorrection soit utile ici
end
if isempty(b)
    return
end

if isempty(suby)
    subyFile = 1:b.nbRows;
else
    subyFile = suby;
end
nbPings = length(subyFile);

if (nbPings <= nbPingsMax) || (TypeWCOutput == 1)
    sonar_SimradProcessWC(b, get_nomFic(aKM), ...
        'TypeWCOutput',        TypeWCOutput, ...
        'Names',               Names, ...
        'Carto',               Carto, ...
        'StepCompute',         StepCompute, ...
        'StepDisplay',         StepDisplay, ...
        'suby',                subyFile, ...
        'minAcrossDist',       minAcrossDist, ...
        'maxAcrossDist',       maxAcrossDist, ...
        'CorrectionTVG',       CorrectionTVG, ...
        'CLimRaw',             CLimRaw, ...
        'CompReflec',          CompReflec, ...
        'ModeDependant',       ModeDependant, ...
        'TideCorrection',      flagTideCorrection, ...
        'typeAlgoWC',          typeAlgoWC, ...
        'MaskBeyondDetection', MaskBeyondDetection, ...
        'IndNavUnit',          IndNavUnit, ...
        'NoWaitbar',           Mute);
else
    str1 = 'Traitement par tron�ons des donn�es Water Column Kongsberg';
    str2 = 'Kongsberg Water Column processing by blocks';
    hw2 = create_waitbar(Lang(str1,str2), 'N', nbPings, 'NoWaitbar', Mute);
    for k=1:nbPingsMax:nbPings
        my_waitbar(k, nbPings, hw2)
        subsuby = k:(k+nbPingsMax-1);
        subsuby(subsuby > nbPings) = [];
        subsuby = subyFile(subsuby);
        X = sprintf('_%02d.xml', floor(k/nbPingsMax));
        K = sprintf('_%02d.kml', floor(k/nbPingsMax));
        Name2.nomFicXmlRaw  = strrep(Names.nomFicXmlRaw, '.xml', X);
        Name2.nomFicXmlComp = strrep(Names.nomFicXmlComp, '.xml', X);
%         Name2.nomFicEchoes  = strrep(Names.nomFicEchoes, '.xml', X);
        if ~isempty(Names.nomFicKML)
            Name2.nomFicKML = strrep(Names.nomFicKML, '.kml', K);
        end
        Name2.AreaName = Names.AreaName;
        sonar_SimradProcessWC(b, get_nomFic(aKM), ...
            'TypeWCOutput',        TypeWCOutput, ...
            'Names',               Name2, ...
            'Carto',               Carto, ...
            'StepCompute',         StepCompute, ...
            'StepDisplay',         StepDisplay, ...
            'suby',                subsuby, ...
            'minAcrossDist',       minAcrossDist, ...
            'maxAcrossDist',       maxAcrossDist, ...
            'CorrectionTVG',       CorrectionTVG, ...
            'CLimRaw',             CLimRaw, ...
            'CLimComp',            CLimComp, ...
            'CompReflec',          CompReflec, ...
            'ModeDependant',       ModeDependant, ...
            'TideCorrection',      flagTideCorrection, ...
            'typeAlgoWC',          typeAlgoWC, ...
            'MaskBeyondDetection', MaskBeyondDetection, ...
            'IndNavUnit',          IndNavUnit, ...
            'NoWaitbar',           Mute);
    end
    my_close(hw2, 'MsgEnd');
end
%     profile report

%% Garbage Collector de Java

java.lang.System.gc
