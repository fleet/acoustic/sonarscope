% Description
%   MoveFiles
%
% Syntax
%    ALL.Process.MoveFiles(ListFicAll, repOut)
%
% Input Arguments
%   repExport : The SonarScope default directory for file exports
%
% Output Arguments
%   ListFicAll : 
%   repOut     : 
%
% Examples
%    ALL.Process.MoveFiles(ListFicAll, repOut)
%
% Author : JMA
% See also Authors

function MoveFiles(ListFicAll, repOut)

if ~iscell(ListFicAll)
    ListFicAll = {ListFicAll};
end

%% D�placement des fichiers .all

nbFic = length(ListFicAll);
str1 = 'D�placement des fichiers .all';
str2 = 'Moving the .all files';
hw = create_waitbar(Lang(str1,str2), 'N', nbFic);
for k=1:nbFic
    my_waitbar(k, nbFic, hw);
    [nomDirIn, nomFic, ext] = fileparts(ListFicAll{k});
    if strcmp(nomDirIn, repOut)
        str1 = sprintf('Le fichier "%s" est d�j� sur le r�pertoire "%s"', [nomDirIn ext], repOut);
        str2 = sprintf('"%s" is already in "%s"', [nomDirIn ext], repOut);
        my_warndlg(Lang(str1,str2), 0, 'Tag', 'ALL_MoveFiles');
    end
    
     [flag, MESSAGE] = movefile(ListFicAll{k}, repOut);
     if flag
         [flag, ncFileName] = existCacheNetcdf(ListFicAll{k});
         if flag % Netcdf
             nomDirSScOut = fullfile(repOut, 'SonarScope');
             if ~exist(nomDirSScOut, 'dir')
                 [flag, MESSAGE] = mkdir(nomDirSScOut);
                 if ~flag
                     str1 = sprintf('Le r�pertoire "%s" n''a pas pu �tre cr�� : %s', nomDirSScOut, MESSAGE);
                     str2 = sprintf('"Directory %s" could not be created  : %s', nomDirSScOut, MESSAGE);
                     my_warndlg(Lang(str1,str2), 0, 'Tag', 'ALL_MoveFiles');
                     return
                 end
             end
             [flag, MESSAGE] = movefile(ncFileName, nomDirSScOut);
             if ~flag
                 MESSAGE
             end
         else % XML-Bin
             % Test d'existence du r�pertoire SonarScope
             nomDirSScIn = fullfile(nomDirIn, 'SonarScope');
             if exist(nomDirSScIn, 'dir')
                 nomDirCache = fullfile(nomDirSScIn, nomFic);
                 % Test d'existence du r�pertoire cache du fichier
                 if exist(nomDirCache, 'dir')
                     % Test d'existence du r�pertoire SonarScope dans le
                     % r�pertoire de destination
                     nomDirSScOut = fullfile(repOut, 'SonarScope');
                     if ~exist(nomDirSScOut, 'dir')
                         [flag, MESSAGE] = mkdir(nomDirSScOut);
                         if ~flag
                             str1 = sprintf('Le r�pertoire "%s" n''a pas pu �tre cr�� : %s', nomDirSScOut, MESSAGE);
                             str2 = sprintf('"Directory %s" could not be created  : %s', nomDirSScOut, MESSAGE);
                             my_warndlg(Lang(str1,str2), 0, 'Tag', 'ALL_MoveFiles');
                             return
                         end
                     end
                     
                     [flag, MESSAGE] = movefile(nomDirCache, nomDirSScOut);
                     if ~flag
                         str1 = sprintf('Le r�pertoire "%s" n''a pas pu �tre copi� dans "%s" : %s', nomDirCache, nomDirSScOut, MESSAGE);
                         str2 = sprintf('"Directory %s" could not be copied in "%s" : %s', nomDirCache, nomDirSScOut, MESSAGE);
                         my_warndlg(Lang(str1,str2), 0, 'Tag', 'ALL_MoveFiles');
                     end
                 end
             end
         end
         
         %% Move .wcd if exists
         
         [nomDir, nomFic] = fileparts(ListFicAll{k});
         nomFicWCD = fullfile(nomDir, [nomFic '.wcd']);
         if exist(nomFicWCD, 'file')
             [flag, MESSAGE] = movefile(nomFicWCD, repOut);
             if ~flag
                 str1 = sprintf('Le fichier "%s" n''a pas pu �tre copi� sur le r�pertoire "%s" : %s', nomFicWCD, repOut, MESSAGE);
                 str2 = sprintf('"%s" could not be copied in "%s" : %s', nomFicWCD, repOut, MESSAGE);
                 my_warndlg(Lang(str1,str2), 0, 'Tag', 'ALL_MoveFiles');
             end
         end
         
         %% Move .txt if exists (Caris flags)
         
         nomFicTxt = fullfile(nomDir, [nomFic '.txt']);
         if exist(nomFicTxt, 'file')
             [flag, MESSAGE] = movefile(nomFicTxt, repOut);
             if ~flag
                 str1 = sprintf('Le fichier "%s" n''a pas pu �tre copi� sur le r�pertoire "%s" : %s', nomFicTxt, repOut, MESSAGE);
                 str2 = sprintf('"%s" could not be copied in "%s" : %s', nomFicTxt, repOut, MESSAGE);
                 my_warndlg(Lang(str1,str2), 0, 'Tag', 'ALL_MoveFiles');
             end
         end
              
     else
        str1 = sprintf('Le fichier "%s" n''a pas pu �tre copi� sur le r�pertoire "%s" : %s', [nomDirIn ext], repOut, MESSAGE);
        str2 = sprintf('"%s" could not be copied in "%s" : %s', [nomDirIn ext], repOut, MESSAGE);
        my_warndlg(Lang(str1,str2), 0, 'Tag', 'ALL_MoveFiles');
     end
end
my_close(hw, 'MsgEnd')
