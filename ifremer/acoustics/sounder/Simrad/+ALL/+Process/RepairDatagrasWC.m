% Description
%   RepairDatagrasWC 
%
% Syntax
%    ALL.Process.RepairDatagrasWC(nomFicIn)
%
% Input Arguments
%   repExport : The SonarScope default directory for file exports
%
% Examples
%   nomFicIn  = 'C:\Temp\0012_20201016_060010_raw.all';
%   ALL.Process.RepairDatagrasWC(nomFicIn)
%
% Author : JMA
% See also Authors

function RepairDatagrasWC(nomFicIn)

[flag, listCorruptedWCDPingNumbers] = detectCorruptedWCDDatagrams(nomFicIn);
if ~flag
    return
end
if isempty(listCorruptedWCDPingNumbers)
    return
end

fprintf('The Water Column Data for file "%s" is being repaired, please wait.\n', nomFicIn)

nomFicOut = [nomFicIn 'NEW'];
flag = repareWCDatagrams(nomFicIn, nomFicOut, listCorruptedWCDPingNumbers);
if flag
    delete(nomFicIn);
    [flag, msg] = movefile(nomFicOut, nomFicIn);
    if ~flag
        my_warndlg(msg, 1);
    end
end


function flag = repareWCDatagrams(nomFicIn, nomFicOut, listCorruptedWCDPingNumbers)

flag = 0;

fidIn = fopen(nomFicIn);
if fidIn == -1
    return
end

fidOut = fopen(nomFicOut, 'w+');
if fidOut == -1
    fclose(fidIn);
    return
end

k = 0;
while ~feof(fidIn)
    nbBytesInDatagram = fread(fidIn, 1, 'uint32');
    if isempty(nbBytesInDatagram) || (nbBytesInDatagram == 0)
        break
    end
    STX            = fread(fidIn, 1, 'uint8');
    TypeDatagram   = fread(fidIn, 1, 'uint8');
    EMModel        = fread(fidIn, 1, 'uint16');
    Date           = fread(fidIn, 1, 'uint32');
    nbMilliseconds = fread(fidIn, 1, 'uint32');
    
    switch TypeDatagram
        case 107 % WC
            PingCounter         = fread(fidIn, 1, 'uint16');
            SystemSerialCounter = fread(fidIn, 1, 'uint16');
            NumberOfDatagrams   = fread(fidIn, 1, 'uint16');
            DatagramNumber      = fread(fidIn, 1, 'uint16');

            if ismember(PingCounter, listCorruptedWCDPingNumbers)
                Offset = nbBytesInDatagram - 20;
                fseek(fidIn, Offset, 0);
            else
                fwrite(fidOut, nbBytesInDatagram, 'uint32');
                fwrite(fidOut, STX,               'uint8');
                fwrite(fidOut, TypeDatagram,      'uint8');
                fwrite(fidOut, EMModel,           'uint16');
                fwrite(fidOut, Date,              'uint32');
                fwrite(fidOut, nbMilliseconds,    'uint32');

                fwrite(fidOut, PingCounter,         'uint16');
                fwrite(fidOut, SystemSerialCounter, 'uint16');
                fwrite(fidOut, NumberOfDatagrams,   'uint16');
                fwrite(fidOut, DatagramNumber,      'uint16');

                n = nbBytesInDatagram - 20;
                X = fread(fidIn, [1 n], 'uint8');
                fwrite(fidOut, X,   'uint8');
            end

        otherwise
            fwrite(fidOut, nbBytesInDatagram, 'uint32');
            fwrite(fidOut, STX,               'uint8');
            fwrite(fidOut, TypeDatagram,      'uint8');
            fwrite(fidOut, EMModel,           'uint16');
            fwrite(fidOut, Date,              'uint32');
            fwrite(fidOut, nbMilliseconds,    'uint32');
            
            n = nbBytesInDatagram - 12;
            X = fread(fidIn, [1 n], 'uint8');
            fwrite(fidOut, X,   'uint8');
    end
    
    if feof(fidIn)
        break
    end
    k = k + 1;
end

fclose(fidIn);
fclose(fidOut);
flag = 1;


function [flag, listCorruptedWCDPingNumbers] = detectCorruptedWCDDatagrams(nomFicIn)

flag = 0;

fidIn = fopen(nomFicIn);
if fidIn == -1
    return
end

listCorruptedWCDPingNumbers = [];
PingCounterPrecedent = -1;
k = 0;
while ~feof(fidIn)
    nbBytesInDatagram = fread(fidIn, 1, 'uint32');
    if feof(fidIn) || isempty(nbBytesInDatagram)
        break
    end
    
    STX = fread(fidIn, 1, 'uint8');
    if feof(fidIn)
        break
    end
    TypeDatagram = fread(fidIn, 1, 'uint8');
    if feof(fidIn)
        break
    end
    EMModel = fread(fidIn, 1, 'uint16');
    if feof(fidIn)
        break
    end
    Date = fread(fidIn, 1, 'uint32');
    if feof(fidIn)
        break
    end
    nbMilliseconds = fread(fidIn, 1, 'uint32');
    if feof(fidIn)
        break
    end
    
    % Ajout JMA le 18/11/2020 pour fichier de Charline
    if (nbBytesInDatagram == 0) && (STX == 0) && (TypeDatagram == 0) && (EMModel == 0) && (Date == 0) && (nbMilliseconds == 0)
        break
    end
    
    switch TypeDatagram
        case 107 % WC
            X = fread(fidIn, 4, 'uint16');
            if feof(fidIn)
                break
            end
            
            PingCounter         = X(1);
            SystemSerialCounter = X(2); %#ok<NASGU>
            NumberOfDatagrams   = X(3);
            DatagramNumber      = X(4);
            
            if PingCounter ~= PingCounterPrecedent
                PingCounterPrecedent = PingCounter;
                if (NumberOfDatagrams > 1) && (DatagramNumber ~= 1)
                    listCorruptedWCDPingNumbers(end+1) = PingCounter; %#ok<AGROW>
                end
            end
            
            Offset = nbBytesInDatagram - 20;
        otherwise
            Offset = nbBytesInDatagram - 12;
    end
    fseek(fidIn, Offset, 0);
    k = k + 1;
    
    if feof(fidIn)
        break
    end
end

fclose(fidIn);
flag = 1;