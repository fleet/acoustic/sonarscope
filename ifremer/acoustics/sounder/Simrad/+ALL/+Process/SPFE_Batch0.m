% Description
%   Params for SPFE_Batch0
%
% Syntax
%    flag = SPFE_Batch0(surveyDirNames, ALim, gridSize, skipSurveys) 
%
% Input Arguments
%   surveyDirNames : 
%   skipSurveys    : 
%
% Output Arguments
%   flag : 0=KO, 1=OK
%
% Examples
%     repImport = 'F:\SPFE\SSC_TESTS_MARS2022';
%    [flag, surveyDirNames, skipSurveys, repImport] = ALL.Params.SPFE_Batch0(repImport)
%  flag = ALL.Process.SPFE_Batch0(surveyDirNames, skipSurveys)
%
% Author : JMA
% See also Authors ALL.Params.SPFE_Batch0

function flag = SPFE_Batch0(surveyDirNames, skipSurveys) 

logFileId = getLogFileId;

N = length(surveyDirNames);
if N == 0
    flag = false;
    logFileId.warn('SPFE_Batch0', 'Exit because surveyDirNames is empty');
    return
end

Carto = cl_carto('Ellipsoide.Type', 11, 'Projection.Type', 3, 'Projection.UTM.Fuseau', 31);
Carto = cartoTemporaireSPFE(Carto); % TODO : supprimer quand le pb sera r�solu : action GLU

flag = false(1, N);
hw = create_waitbar('Processing SPFE_Batch0', 'N', N);
for k=1:N
    flag(k) = SPFE_Batch0_unitaire(surveyDirNames{k}, skipSurveys, Carto, logFileId);
    my_waitbar(k, N, hw)
end
my_close(hw, 'MsgEnd')
flag = all(flag);


function flag = SPFE_Batch0_unitaire(surveyDirName, skipSurveys, Carto, logFileId)

flag = false;
I0   = cl_image_I0;

subPathname = '100_SPFE_BATCH0';

[~, nameSurvey] = fileparts(surveyDirName);
resultsDirName = fullfile(surveyDirName, subPathname);

%% Test si le r�pertoire existe

if ~exist(resultsDirName, 'dir')
    status = mkdir(resultsDirName);
    if ~status
        messageErreur(resultsDirName)
        flag = 0;
        return
    end
end

%% Test si le r�pertoire a d�j� �t� trait�

nomFicSurveyStatus = fullfile(resultsDirName, 'SurveyProcessingStatus.txt');
if skipSurveys && exist(nomFicSurveyStatus, 'file')
    logFileInfo(logFileId, nameSurvey, 'Already processed')
    flag = true;
    return
end

%% Test existence du fichier .grd

dirNameGRD = fullfile(surveyDirName, '90_BATHYMETRY_DTM_G');
if ~exist(dirNameGRD, 'dir')
    msg = sprintf('Exit because directory "%s" does not exist', dirNameGRD);
    logFileWarn(logFileId, nameSurvey, msg);
    return
end
filenameGRD = listeFicOnDir(dirNameGRD, '.grd');
if isempty(filenameGRD)
    msg = sprintf('Exit because no .grd file found in "%s"', dirNameGRD);
    logFileWarn(logFileId, nameSurvey, msg);
    flag = false;
    return
end
filenameGRD = filenameGRD{1};

%% Lecture du fichier .grd

logFileInfo(logFileId, nameSurvey, ['Reading ' filenameGRD ' is starting'])
DataType = cl_image.indDataType('Bathymetry');
GeometryType = cl_image.indGeometryType('GeoYX');
Unit = 'm';
[flag, grdUTM] = import_surfer(I0, filenameGRD, 'DataType', DataType, ...
    'GeometryType', GeometryType, 'Unit', Unit,  'Carto', Carto);
if ~flag
    msg = sprintf('Exit because "%s" could not be read', filenameGRD);
    logFileWarn(logFileId, nameSurvey, msg);
    return
end
% imagesc(grdUTM)
logFileInfo(logFileId, nameSurvey, ['Reading ' filenameGRD ' is over'])

%% GeoYX => LatLong

logFileInfo(logFileId, nameSurvey, ['GeoYX2LatLong "' filenameGRD '" is starting'])
MNT = GeoYX2LatLong(grdUTM, 'NoWaitbar', 1);
% imagesc(MNT)
clear grdUTM
logFileInfo(logFileId, nameSurvey, ['GeoYX2LatLong "' filenameGRD '" is over'])

%% Export en .ers

ImageName = MNT.Name;
filenameERS = fullfile(dirNameGRD, ImageName);
options = paramsExportErMapper(MNT);
logFileInfo(logFileId, nameSurvey, ['Exporting "' filenameERS '" is starting'])
flag = export_ermapper(MNT, filenameERS, 'options', options);
if ~flag
    msg = sprintf('Exit because "%s" could not be saved on disk', filenameERS);
    logFileWarn(logFileId, nameSurvey, msg);
    return
end
logFileInfo(logFileId, nameSurvey, ['Exporting "' filenameERS '" is over'])

%% Recherche de tous les .all

dirNameALL = fullfile(surveyDirName, '00_RAW_TIDE_DRAUGHT_AC_DATA');
str = sprintf('Checking .all files in directory %s ', dirNameALL);
logFileInfo(logFileId, nameSurvey, [str 'is starting'])
[filenameALL, filenameKMALL] = listeFicOnDir(dirNameALL, '.all', '.kmall');
filenameALL = [filenameALL; filenameKMALL];
if isempty(filenameALL)
    msg = sprintf('Exit because no .all files found in "%s"', dirNameALL);
    logFileWarn(logFileId, nameSurvey, msg);
    flag = 0;
    return
end

[flag, aKM] = ALL.checkFiles(filenameALL); % //Tbx
if ~flag
    return
end
filenameALL = get_nomFic(aKM);
logFileInfo(logFileId, nameSurvey, [str 'is over'])

%% Calcul des angles d'incidence

logFileInfo(logFileId, nameSurvey, 'Computing Incidence Angles is starting')
flag = ALL_IncidenceAngleFromOneDTM(aKM, MNT); % //Tbx
if all(~flag)
    msg = sprintf('Exit because Incidence Angles could not be processed for "%s"', dirNameALL);
    logFileWarn(logFileId, nameSurvey, msg);
    flag = 0;
    return
elseif any(~flag)
    msg = sprintf('Incidence Angles coud be computed only for %d/%d files of directory "%s"', sum(flag), length(flag), dirNameALL);
    subBadFiles = find(~flag);
    for k=1:length(subBadFiles)
        msg = sprintf('%s\n\t failed for %s', msg, filenameALL{k});
    end
    logFileInfo(logFileId, nameSurvey, msg);
end
filenameALL = filenameALL(flag);
aKM = aKM(flag);
clear MNT
logFileInfo(logFileId, nameSurvey, 'Computing Incidence Angles is over')

%% Cr�ation de ReflectivityBestBS dans le cache

logFileInfo(logFileId, nameSurvey, 'Computing BestBS is starting')
flag = ALL_BestBSCompute(aKM, []); % //Tbx
if ~flag
    msg = sprintf('Exit because layer "ReflectivityBestBS" could not be processed for "%s"', filenameALL{1});
    logFileWarn(logFileId, nameSurvey, msg);
    return
end
logFileInfo(logFileId, nameSurvey, 'Computing BestBS is over')

%% Cr�ation du fichier SurveyProcessingStatus.txt

logFileInfo(logFileId, nameSurvey, 'Writing SurveyProcessingStatus file is starting')
fid = fopen(nomFicSurveyStatus, 'w+');
fprintf(fid, '%s : SPFE_BatchBSAngularSector is over\n', nameSurvey);
fclose(fid);
logFileInfo(logFileId, nameSurvey, 'Writing SurveyProcessingStatus file is over')

%% Message de fin de traitement

logFileInfo(logFileId, nameSurvey, 'Processing Batch0 is over')


function logFileInfo(logFileId, nameSurvey, msg)
logFileId.info('SPFE_Batch0', [nameSurvey ' - ' msg]);

function logFileWarn(logFileId, nameSurvey, msg)
logFileId.warn('SPFE_Batch0', [nameSurvey ' - ' msg]);
