% Description
%   SPFE_Batch3
%
% Syntax
%     flag = ALL.Process.SPFE_Batch3(surveyDirNames)
%
% Input Arguments
%   surveyDirNames     :
%
% Output Arguments
%   flag : 0=KO, 1=OK
%
% Examples
%   repImport = 'F:\SPFE\SSC_TESTS_MARS2022';
%     [flag, repImport] = ALL.Params.SPFE_Batch3(this, repImport)
%   flag = ALL.Process.SPFE_Batch3(surveyDirNames, gridSize, skipSurveys, flagInteractivity, MaskLatLong, valMask, false)
%
% Author : JMA
% See also Authors ALL.Process.SPFE_Batch3

function flag = SPFE_Batch3(surveyDirNames)

logFileId = getLogFileId;

N = length(surveyDirNames);
if N == 0
    flag = false;
    logFileId.warn('BatchBSCurvesAndCompMosaics', 'Exit because surveyDirNames is empty');
    return
end

flag = false(1, N);
hw = create_waitbar('Processing BatchBSCurvesAndCompMosaics', 'N', N);
for k=1:N
    flag(k) = SPFE_Batch3_unitaire(surveyDirNames{k}, logFileId);
    my_waitbar(k, N, hw)
end
my_close(hw, 'MsgEnd')
flag = all(flag);



function flag = SPFE_Batch3_unitaire(surveyDirName, logFileId)

flag = 0;
[~, nameSurvey] = fileparts(surveyDirName);

%% Test existence du r�pertoire 102_SPFE_BATCH2

nomDirBatch2 = fullfile(surveyDirName, '102_SPFE_BATCH2');
if ~exist(nomDirBatch2, 'dir')
    msg = sprintf('Exit because directory "%s" does not exist', nomDirBatch2);
    logFileWarn(logFileId, nameSurvey, msg);
    return
end

%% Recherche des paires de fichiers Reflectivity/IncidenceAngle pr�sents dans le r�pertoire

listeFicReflectivity = listeFicOnDir(nomDirBatch2, '.ers', 'Filtre', '_Reflectivity_LatLong');
if isempty(listeFicReflectivity)
    msg = sprintf('Exit because directory "%s" does not contain any "_Reflectivity_LatLong.ers" files', nomDirBatch2);
    logFileWarn(logFileId, nameSurvey, msg);
    return
end

n = length(listeFicReflectivity);
flagFiles = true(1,n);
for k=n:-1:1
    listeFicIncidenceAngle{k} = strrep(listeFicReflectivity{k}, '_Reflectivity_LatLong', '_IncidenceAngle_LatLong');
    if ~exist(listeFicIncidenceAngle{k}, 'file')
        flagFiles(k) = false;
    end
end
listeFicReflectivity   = listeFicReflectivity(flagFiles);
listeFicIncidenceAngle = listeFicIncidenceAngle(flagFiles);
if isempty(listeFicReflectivity)
    msg = sprintf('Exit because directory "%s" does not contain any "_Reflectivity_LatLong.ers"  and "_IncidenceAngle_LatLong" associated files', nomDirBatch2);
    logFileWarn(logFileId, nameSurvey, msg);
    return
end

%% Calcul des courbes de r�flectivit� en fonction de l'angle d'incidence

% Coul = 'rgbcmyk';
for k=1:length(listeFicReflectivity)
%     kMod = 1 + mod(k-1, length(Coul));

    [flag, layerReflectivity(k)] = cl_image.import_ermapper(listeFicReflectivity{k}); %#ok<AGROW> 
    if ~flag
        continue
    end
    [flag, layerIncidenceAngle(k)] = cl_image.import_ermapper(listeFicIncidenceAngle{k}); %#ok<AGROW> 
    if ~flag
        continue
    end

    nomCourbe = layerReflectivity(k).Name;
    nomCourbe = strrep(nomCourbe, '_BestBS_Compensation_MaskIn_WinFillNaN_Extraction_LatLong_Reflectivity', '');

    Selection.Name  = layerIncidenceAngle(k).Name;
    Selection.TypeVal  = 'Layer';
    Selection.indLayer = 1;
    Selection.DataType = layerIncidenceAngle(k).DataType;
    Selection.Unit     = 'deg';

    X = courbesStats(layerReflectivity(k), layerReflectivity(k).Name, ...
        nomCourbe, ['InitialFileName : ' layerReflectivity(k).InitialFileName], [], ...
        layerIncidenceAngle(k), Selection, 'stepBins', 1, 'GeometryType', 'LatLong', ...
        'Sampling', 1, 'MeanCompType', 2, 'NomFic', listeFicReflectivity{k});
    if isempty(X)
        continue
    end
    CourbesStatistique(k) = X.CourbesStatistiques; %#ok<AGROW> 
end

[flag, fig] = plot_CourbesStats(CourbesStatistique, 'Stats', 0);
if ~flag
    return
end

%% Moyennage des courbes

[flag, CourbesStatistiquesOut, CourbesStatistiquesBiaisOut] = CourbesStatsUnionWithHoles(CourbesStatistique, ...
    'nomCourbe', [nameSurvey ' - Mean curve'], 'MeanCompType', 2, 'XLim4Compens', [40 50]);
if ~flag
    return
end
flag = plot_CourbesStats(CourbesStatistiquesOut, 'Stats', 0, 'Fig', fig, 'Coul', 'k', 'LineWidth', 2);
if ~flag
    return
end

flag = plot_CourbesStats(CourbesStatistiquesBiaisOut, 'Stats', 0);
if ~flag
    return
end

% Export de la courbe de compensation
CourbeConditionnelle = CourbesStatistiquesBiaisOut;
nomDirBatch3 = fullfile(surveyDirName , '103_SPFE_BATCH3');
nomFicSave = fullfile(nomDirBatch3, [nameSurvey '-FlatteningCompensation.mat']);
save(nomFicSave, 'CourbeConditionnelle');

%% Compensation et sauvegarde de toutes les images

bilan = CourbesStatistiquesBiaisOut.bilan;
for k=1:length(listeFicReflectivity)
    [nomDir, nomFic] = fileparts(listeFicIncidenceAngle{k});
    nomFic = fullfile(nomDir, nomFic);
    nomFic = strrep(nomFic, 'IncidenceAngle', 'RxBeamAngle');
    [flag, layerAngle(k)] = cl_image.import_ermapper(nomFic); %#ok<AGROW> % 
    if ~flag
        continue
    end

    % Compensation des images
    [layerReflectivityCompens(k), flag] = compensationCourbesStats(layerReflectivity(k), layerIncidenceAngle(k), bilan); %#ok<AGROW> 
    if ~flag
        continue
    end
    %     imagesc(layerReflectivityCompens(k));

    % Export des images
    [~, nomFic] = fileparts(listeFicReflectivity{k});
    nomFic = fullfile(nomDirBatch3, nomFic);
    options = paramsExportErMapper(layerReflectivityCompens(k));
    flag = export_ermapper(layerReflectivityCompens(k), nomFic, 'options', options);
    if ~flag
        continue
    end

    % TODO : est-ce qu'on recopie les fichiers RxBeamAngle dans ce r�pertoire ?
end

%% Assemblage de toutes les images

[flag, ReflectivityMosCompens, AngleMos] = mergeMosaics(layerReflectivityCompens, 'Angle', layerAngle);
if ~flag
    return
end
ReflectivityMosCompens.Name = strrep(ReflectivityMosCompens.Name, 'Mosaic', [nameSurvey '-Flattened']);
% imagesc(ReflectivityMosCompens)

% Export de la mosa�que compens�e
nomFic = fullfile(nomDirBatch3, ReflectivityMosCompens.Name);
options = paramsExportErMapper(ReflectivityMosCompens);
flag = export_ermapper(ReflectivityMosCompens, [nomFic '.ers'], 'options', options);
if ~flag
    return
end

nomFic = fullfile(nomDirBatch3, ReflectivityMosCompens.Name);
flag = export_surfer(ReflectivityMosCompens, [nomFic '.grd']);
if ~flag
    return
end

% Export de la mosa�que des angles
nomFic = strrep(nomFic, 'Reflectivity', 'RxBeamAngle');
options = paramsExportErMapper(AngleMos);
flag = export_ermapper(AngleMos, [nomFic '.ers'], 'options', options);
if ~flag
    return
end

flag = export_surfer(AngleMos, [nomFic '.grd']);
if ~flag
    return
end
