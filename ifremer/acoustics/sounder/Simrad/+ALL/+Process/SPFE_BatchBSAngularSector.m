% Description
%   Params for SPFE_BatchBSAngularSector
%
% Syntax
%    flag = SPFE_BatchBSAngularSector(surveyDirNames, ALim, gridSize, skipSurveys, flagBSCorr) 
%
% Input Arguments
%   surveyDirNames : 
%   ALim           : 
%   gridSize       : 
%   skipSurveys    : 
%   flagBSCorr     : 
%
% Output Arguments
%   flag : 0=KO, 1=OK
%
% Examples
%     repImport = 'F:\SPFE\SSC_TESTS_MARS2022';
%    [flag, surveyDirNames, ALim, gridSize, skipSurveys, repImport] = ALL.Params.SPFE_BatchBSAngularSector(repImport)
%  flag = ALL.Process.SPFE_BatchBSAngularSector(surveyDirNames, ALim, gridSize, skipSurveys, false)
%
% Author : JMA
% See also Authors ALL.Params.SPFE_BatchBSAngularSector

function flag = SPFE_BatchBSAngularSector(surveyDirNames, ALim, gridSize, skipSurveys, flagBSCorr) 

logFileId = getLogFileId;

N = length(surveyDirNames);
if N == 0
    flag = false;
    logFileId.warn('SPFE_BatchBSAngularSector', 'Exit because directory of surveys is empty');
    return
end

Carto = cl_carto('Ellipsoide.Type', 11, 'Projection.Type', 3, 'Projection.UTM.Fuseau', 31);
Carto = cartoTemporaireSPFE(Carto); % TODO : supprimer quand le pb sera r�solu : action GLU

flag = false(1, N);
hw = create_waitbar('Processing SPFE_BatchBSAngularSector', 'N', N);
for k=1:N
    flag(k) = SPFE_BatchBSAngularSector_unitaire(surveyDirNames{k}, ALim, gridSize, ...
        skipSurveys, Carto, flagBSCorr, logFileId);
    my_waitbar(k, N, hw)
end
my_close(hw, 'MsgEnd')
flag = all(flag);


function flag = SPFE_BatchBSAngularSector_unitaire(surveyDirName, ALim, gridSize, skipSurveys, Carto, flagBSCorr, logFileId)

if flagBSCorr
    subPathname = '102_SPFE_BATCH2'; % Modif cf doc Marc le 12/01/2024
else
    subPathname = '101_SPFE_BATCH1';
end

%% Compensation statistique

if flagBSCorr
    [~, nameSurvey] = fileparts(surveyDirName);
    [flag, InfoCompensationCurve] = ALL.Process.getBSCorrCurves(surveyDirName, logFileId, nameSurvey);
    if ~flag
        return
    end
else
    InfoCompensationCurve = [];
end

[dirName, nameSurvey] = fileparts(surveyDirName);
dirNameSurvey  = fullfile(dirName, nameSurvey);
resultsDirName = fullfile(dirNameSurvey, subPathname);

%% Test si le r�pertoire existe

if ~exist(resultsDirName, 'dir')
    status = mkdir(resultsDirName);
    if ~status
        messageErreur(resultsDirName)
        flag = 0;
        return
    end
end

%% Test si le r�pertoire a d�j� �t� trait�

nomFicSurveyStatus = fullfile(resultsDirName, 'SurveyProcessingStatus.txt');
if skipSurveys && exist(nomFicSurveyStatus, 'file')
    logFileInfo(logFileId, nameSurvey, 'Already processed')
    flag = true;
    return
end

%% Recherche de tous les .all

dirNameALL = fullfile(dirNameSurvey, '00_RAW_TIDE_DRAUGHT_AC_DATA');
str = sprintf('Searching .all files in directory %s ', dirNameALL);
logFileInfo(logFileId, nameSurvey, [str 'is starting'])
[filenameALL, filenameKMALL] = listeFicOnDir(dirNameALL, '.all', '.kmall');
filenameALL = [filenameALL; filenameKMALL];
if isempty(filenameALL)
    msg = sprintf('Exit because no .all files found in "%s"', dirNameALL);
    logFileWarn(logFileId, nameSurvey, msg);
    flag = 0;
    return
end

[flag, aKM] = ALL.checkFiles(filenameALL); % //Tbx
if ~flag
    return
end
logFileInfo(logFileId, nameSurvey, [str 'is over'])

%% Mosaiques individuelles

Window = [3 3];

InfoMos.TypeMos                     = 2;
InfoMos.TypeGrid                    = 1;
InfoMos.flagMasquageDual            = 1;
InfoMos.Import.MasqueActif          = 1;
InfoMos.Import.SlotFilename         = [];
InfoMos.Import.SonarTime            = [];
InfoMos.Covering.Priority           = 1;
InfoMos.Covering.SpecularLimitAngle = [];
InfoMos.LimitAngles                 = [];
InfoMos.gridSize                    = gridSize;
InfoMos.skipExistingMosaic          = 0;
InfoMos.DirName                     = fullfile(dirNameSurvey, subPathname); % Fusionner info avec ligne suivante
if ~exist(InfoMos.DirName, 'dir')
    flag = mkdir(InfoMos.DirName);
    if ~flag
        logFileInfo(logFileId, nameSurvey, ['Could not create directory ' InfoMos.DirName])
        return
    end
end

%% Cr�ation des mosa�ques de IncidenceAngle

logFileInfo(logFileId, nameSurvey, 'Computing mosaics of IncidenceAngle is starting')
LayerName = 'IncidenceAngle';
flag = createMosaicAndSaveResults(dirNameSurvey, aKM, nameSurvey, subPathname, LayerName, InfoMos, logFileId, ...
    [], Window, [], Carto, ALim); % //Tbx
if ~flag
    return
end
logFileInfo(logFileId, nameSurvey, 'Computing mosaics of IncidenceAngle is over')

%% Cr�ation des mosa�ques de ReflectivityBestBS

logFileInfo(logFileId, nameSurvey, 'Computing mosaics of ReflectivityBestBS is starting')
LayerName = 'ReflectivityBestBS';
flag = createMosaicAndSaveResults(dirNameSurvey, aKM, nameSurvey, subPathname, LayerName, InfoMos, logFileId, ...
    1, Window, InfoCompensationCurve, Carto, ALim, 'BestBS', 1);
if ~flag
    return
end
logFileInfo(logFileId, nameSurvey, 'Computing mosaics of ReflectivityBestBS is over')

%% Cr�ation du fichier SurveyProcessingStatus.txt

logFileInfo(logFileId, nameSurvey, 'Writing SurveyProcessingStatus file is starting')
fid = fopen(nomFicSurveyStatus, 'w+');
fprintf(fid, '%s : SPFE_BatchBSAngularSector is over\n', nameSurvey);
fclose(fid);
logFileInfo(logFileId, nameSurvey, 'Writing SurveyProcessingStatus file is over')


function flag = createMosaicAndSaveResults(dirNameSurvey, aKM, nameSurvey, subPathname, LayerName, InfoMos, logFileId, ...
    ModeDependant, Window, InfoCompensationCurve, Carto, ALim, varargin)

[varargin, BestBS] = getPropertyValue(varargin, 'BestBS', 0); %#ok<ASGLU> 

I0 = cl_image_I0;

if contains(LayerName, 'Reflectivity')
    Filtre = {'_Reflectivity_LatLong'; '_RxBeamAngle_LatLong'};
end
if contains(LayerName, 'IncidenceAngle')
    Filtre = {'_IncidenceAngle_LatLong'; '_RxBeamAngle_LatLong'};
end

%% Contr�le si l'angle d'incidence a �t� calcul�

filenameALL = get_nomFic(aKM);
flagFile = ALL_IsLayerPopulated(aKM, 'IncidenceAngle');
sub = find(flagFile == 1);
if isempty(sub)
    msg = ['ALL_IncidenceAngleFromOneDTM Exit because  no "' LayerName '" layers were found in the SSc cache files'];
    logFileWarn(logFileId, nameSurvey, msg);
    return
end
filenameALL = filenameALL(sub);

%% Calcul des mosa�ques individuelles de r�flectivit�

logFileInfo(logFileId, nameSurvey, ['Computing individual mosaics for "' InfoMos.DirName '" is starting'])
SameReflectivityStatus = 0;
flag = SimradAllMosaiqueParProfil(I0, 1, filenameALL, ...
    ModeDependant, LayerName, Window, SameReflectivityStatus, InfoCompensationCurve, ...
    InfoMos, 'Carto', Carto, 'flagMarcRoche', true, 'Mute', -1, 'SpecialDiagTxCompensated', 1);
if ~flag
    msg = sprintf('Exit because Individual mosaics could not be processed or saved for "%s"', InfoMos.DirName);
    logFileWarn(logFileId, nameSurvey, msg);
    return
end
logFileInfo(logFileId, nameSurvey, ['Computing individual mosaics for "' InfoMos.DirName '" is over'])

%% Import des mosaiques individuelles

logFileInfo(logFileId, nameSurvey, ['Importing individual mosaics for "' InfoMos.DirName '" is starting'])
filenameERS = listeFicOnDir(InfoMos.DirName, '.ers', 'Filtre', Filtre);
[flag, LayersLatLong] = cl_image.import_ermapper(filenameERS);
if ~flag
    msg = sprintf('Individual mosaics could not be read from %s', InfoMos.DirName);
    logFileWarn(logFileId, nameSurvey, msg);
    return
end
logFileInfo(logFileId, nameSurvey, ['Importing individual mosaics for "' InfoMos.DirName '" is over'])

%% Cr�ation des masques

logFileInfo(logFileId, nameSurvey, ['Creating masks for "' InfoMos.DirName '" is starting'])
valInterval  = [-fliplr(ALim); ALim];
valMask      = [1 1];
MasksLatLong = ROI_auto(LayersLatLong(2:2:end), 1, 'Mask', [], [], valInterval, valMask);
logFileInfo(logFileId, nameSurvey, ['Creating masks for "' InfoMos.DirName '" is over'])

%% Masquage des mosa�ques de r�flectivit�

logFileInfo(logFileId, nameSurvey, ['Masking mosaics for "' InfoMos.DirName '" is starting'])
[ReflecMaskedLatLong, flag] = masquage(LayersLatLong(1:2:end), 'LayerMask', MasksLatLong, 'valMask', 1);
if ~flag
    msg = sprintf('Masking Reflectivity layers was impossible for %s', InfoMos.DirName);
    logFileWarn(logFileId, nameSurvey, msg);
    return
end
logFileInfo(logFileId, nameSurvey, ['Masking mosaics for "' InfoMos.DirName '" is over'])
XStep = get(LayersLatLong(1), 'XStep');
clear MasksLatLong LayersLatLong

%% Assemblage des mosa�ques

logFileInfo(logFileId, nameSurvey, ['Assembling mosaics for "' InfoMos.DirName '" is starting'])
bufferZoneSize = 10 * XStep;
[flag, AssembledReflectivity] = mergeMosaics(ReflecMaskedLatLong, 'CoveringPriority', 5, 'bufferZoneSize', bufferZoneSize);
if ~flag
    msg = sprintf('Masked mosaics merging failed for %s', InfoMos.DirName);
    logFileWarn(logFileId, nameSurvey, msg);
    return
end
logFileInfo(logFileId, nameSurvey, ['Assembling mosaics for "' InfoMos.DirName '" is over'])

%% Exports de la mosa�que

% Export Surfer
filename = sprintf('%s_%02d%02d.grd', nameSurvey, floor(ALim));
filenameAssembledMosaic = fullfile(dirNameSurvey, subPathname, filename);
logFileInfo(logFileId, nameSurvey, ['Exporting "' filenameAssembledMosaic '" is starting'])
flag = export_surfer(AssembledReflectivity, filenameAssembledMosaic);
if ~flag
    msg = sprintf('%s could not be saved', filenameAssembledMosaic);
    logFileWarn(logFileId, nameSurvey, msg);
    return
end
logFileInfo(logFileId, nameSurvey, ['Exporting "' filenameAssembledMosaic '" is over'])

% Export .ers
filename = sprintf('%s_%02d%02d_LatLong_%s.ers', nameSurvey, floor(ALim), get_DataTypeName(AssembledReflectivity));
filenameAssembledMosaic = fullfile(dirNameSurvey, subPathname, filename);
logFileInfo(logFileId, nameSurvey, ['Exporting "' filenameAssembledMosaic '" is starting'])
flag = export_ermapper(AssembledReflectivity, filenameAssembledMosaic);
if ~flag
    msg = sprintf('%s could not be saved', filenameAssembledMosaic);
    logFileWarn(logFileId, nameSurvey, msg);
    return
end
logFileInfo(logFileId, nameSurvey, ['Exporting "' filenameAssembledMosaic '" is over'])

clear AssembledReflectivity

%% Export Surfer de toutes les mosaiques masqu�es individuelles

logFileInfo(logFileId, nameSurvey, ['Exporting masked mosaics for "' InfoMos.DirName '" is starting'])
for k=1:length(ReflecMaskedLatLong)
    ImageName = ReflecMaskedLatLong(k).Name;
    Append = sprintf('Step1_%0d_%0d', ALim(1), ALim(2));
    ImageName = strrep(ImageName, 'BSCompensNo_AireInsoCompensIfremer_DiagEmiCompensationConstructeurAnalytique_TVG-SSc_WinFillNaN', Append);
    ImageName = strrep(ImageName, '_Extraction', '');
    ImageName = strrep(ImageName, '_MaskIn', '');
    ImageName = strrep(ImageName, '_WinFillNaN', '');
    ReflecMaskedLatLong(k).Name = ImageName;
    filenameMosaic = fullfile(dirNameSurvey, subPathname, [ImageName '.grd']); % ALim
    flag = export_surfer(ReflecMaskedLatLong(k), filenameMosaic);
    if ~flag
        msg = sprintf('%s could not be saved', filenameMosaic);
        logFileWarn(logFileId, nameSurvey, msg);
        return
    end
end
logFileInfo(logFileId, nameSurvey, ['Exporting masked mosaics for "' InfoMos.DirName '" is over'])

%% Message de fin de traitement

logFileInfo(logFileId, nameSurvey, ['Processing over for ' InfoMos.DirName])


function logFileInfo(logFileId, nameSurvey, msg)
logFileId.info('SPFE_BatchBSAngularSector', [nameSurvey ' - ' msg]);

function logFileWarn(logFileId, nameSurvey, msg)
logFileId.warn('SPFE_BatchBSAngularSector', [nameSurvey ' - ' msg]);
