% Description
%   SPFE_BatchBSCurvesAndCompMosaics
%
% Syntax
%     flag = ALL.Process.SPFE_BatchBSCurvesAndCompMosaics(surveyDirNames, gridSize, skipSurveys, flagInteractivity, MaskLatLong, valMask, flagBSCorr)
%
% Input Arguments
%   surveyDirNames     :
%   gridSize           :
%   skipSurveys        : 
%   flagInteractivity  : 
%   indLayeMaskLatLong : 
%   valMask            :
%   flagBSCorr         : 
%
% Output Arguments
%   flag : 0=KO, 1=OK
%
% Examples
%   repImport = 'F:\SPFE\SSC_TESTS_MARS2022';
%     [flag, surveyDirNames, gridSize, skipSurveys, flagInteractivity, indLayeMaskLatLong, valMask, repImport] ...
%     = ALL.Params.SPFE_BatchBSCurvesAndCompMosaics(this, repImport, ...)
%   flag = ALL.Process.SPFE_BatchBSCurvesAndCompMosaics(surveyDirNames, gridSize, skipSurveys, flagInteractivity, MaskLatLong, valMask, false)
%
% Author : JMA
% See also Authors ALL.Process.SPFE_BatchBSCurvesAndCompMosaics

function flag = SPFE_BatchBSCurvesAndCompMosaics(surveyDirNames, gridSize, skipSurveys, flagInteractivity, MaskLatLong, valMask, flagBSCorr)

logFileId = getLogFileId;

N = length(surveyDirNames);
if N == 0
    flag = false;
    logFileId.warn('BatchBSCurvesAndCompMosaics', 'Exit because surveyDirNames is empty');
    return
end

if ~isempty(MaskLatLong)
    MaskLatLong = Virtualmemory2Ram(MaskLatLong);
end

Carto = cl_carto('Ellipsoide.Type', 11, 'Projection.Type', 3, 'Projection.UTM.Fuseau', 31);
Carto = cartoTemporaireSPFE(Carto); % TODO : supprimer quand le pb sera r�solu : action GLU

flag = false(1, N);
hw = create_waitbar('Processing BatchBSCurvesAndCompMosaics', 'N', N);
for k=1:N
    flag(k) = SPFE_BatchBSCurvesAndCompMosaics_unitaire(surveyDirNames{k}, gridSize, ...
        skipSurveys, Carto, flagInteractivity, MaskLatLong, valMask, flagBSCorr, logFileId);
    my_waitbar(k, N, hw)
end
my_close(hw, 'MsgEnd')
flag = all(flag);


function flag = SPFE_BatchBSCurvesAndCompMosaics_unitaire(surveyDirName, gridSize, skipSurveys, Carto, flagInteractivity, ...
    MaskLatLong, valMask, flagBSCorr, logFileId)

[dirName, nameSurvey] = fileparts(surveyDirName);
dirNameSurvey  = fullfile(dirName, nameSurvey);

if flagBSCorr
    if isempty(MaskLatLong)
        resultsDirName = fullfile(dirNameSurvey, '103_SPFE_BATCH3');
        maskName = [];
    else
        resultsDirName = fullfile(dirNameSurvey, '103_SPFE_BATCH3_masl');
        maskName = extract_ImageName(MaskLatLong);
        maskName = strrep(maskName, '_Extraction', '');
    end
else
    if isempty(MaskLatLong)
        resultsDirName = fullfile(dirNameSurvey, '104_SPFE_BATCH4'); % Modif cf doc Marc le 12/01/2024
        maskName = [];
    else
        resultsDirName = fullfile(dirNameSurvey, '104_SPFE_BATCH4_mask');
        maskName = extract_ImageName(MaskLatLong);
        maskName = strrep(maskName, '_Extraction', '');
    end
end

%% Test si le r�pertoire a d�j� �t� trait�

nomFicSurveyStatus = fullfile(resultsDirName, 'BatchBSCurvesAndCompMosaics.txt');
if skipSurveys && exist(nomFicSurveyStatus, 'file')
    logFileInfo(logFileId, nameSurvey, 'Already processed')
    flag = true;
    return
end

%% Recherche de tous les .all

dirNameALL = fullfile(dirNameSurvey, '00_RAW_TIDE_DRAUGHT_AC_DATA');
str = sprintf('Searching .all files in directory %s ', dirNameALL);
logFileInfo(logFileId, nameSurvey, [str 'is starting'])
[filenameALL, filenameKMALL] = listeFicOnDir(dirNameALL, '.all', '.kmall');
filenameALL = [filenameALL; filenameKMALL];
if isempty(filenameALL)
    msg = sprintf('Exit because no .all files found in "%s"', dirNameALL);
    logFileWarn(logFileId, nameSurvey, msg);
    flag = 0;
    return
end
logFileInfo(logFileId, nameSurvey, [str 'is over'])

%% Recherche des fichiers pour lesquels on a pu calculer les angles d'incidence

logFileInfo(logFileId, nameSurvey, 'Selection of files that have incidence angle is starting')
N = length(filenameALL);
flagEmpty = ones(1,N);
for k=1:N
    aKM = cl_simrad_all('nomFic', filenameALL{k});
    tmp = get_Layer(aKM, 'IncidenceAngle', 'Carto', Carto);
    if isempty(tmp)
        continue
    end
    [flag, flagEmpty(k)] = isEmptyImage(tmp);
    if ~flag
        continue
    end
end
filenameALL = filenameALL(~flagEmpty);
logFileInfo(logFileId, nameSurvey, 'Selection of files that have incidence angle is over')

if isempty(filenameALL)
    logFileWarn(logFileId, nameSurvey, 'Exit because no incidence angle was found for the .all files of this survey')
    flag = false;
    return
end

%% Compensation statistique

if flagBSCorr
    [flag, InfoCompensationCurve] = ALL.Process.getBSCorrCurves(dirNameSurvey, logFileId, nameSurvey);
    if ~flag
        return
    end
else
    InfoCompensationCurve = [];
end

%% Step1 sur premier fichier

logFileInfo(logFileId, nameSurvey, ['Reading file "' filenameALL{1} '"'])
aKM = cl_simrad_all('nomFic', filenameALL{1});
if isempty(aKM)
    msg = sprintf('Exit because "%s" could not be read', filenameALL{1});
    logFileWarn(logFileId, nameSurvey, msg);
    flag = 0;
    return
end

logFileInfo(logFileId, nameSurvey, ['Reading layers of "' filenameALL{1} '" is starting'])
Version = version_datagram(aKM);
ListeLayersV1 = [2 3 4 10 24 25 26 27 40 41 43 47];
switch Version
    case 'V1'
        [LayersPingBeam] = import_PingBeam_All_V1(aKM, 'ListeLayers', ListeLayersV1, ...
            'Carto', Carto, 'MasqueActif', 1);
    case 'V2'
        ListeLayersV2 = SelectionLayers_V1toV2([ListeLayersV1 2 3 4]);
        [LayersPingBeam] = import_PingBeam_All_V2(aKM, 'ListeLayers', ListeLayersV2, ...
            'Carto', Carto, 'MasqueActif', 1);
end
if isempty(LayersPingBeam)
    logFileWarn(logFileId, nameSurvey, 'Exit because no layers could be imported');
    flag = 0;
    return
end
logFileInfo(logFileId, nameSurvey, ['Reading layers of "' filenameALL{1} '" is over'])

%% Compute Best BS Step1

logFileInfo(logFileId, nameSurvey, ['Computing Step1 for "' filenameALL{1} '" is starting'])
[~, indLayerRange] = findAnyLayerRange(LayersPingBeam, 1);
indLayerReflectivity = findIndLayerSonar(LayersPingBeam, 1, 'strDataType', 'Reflectivity', 'OnlyOneLayer');
[flag, LayerStep1] = SonarBestBSConstructeur(LayersPingBeam, indLayerReflectivity, 'Range', LayersPingBeam(indLayerRange));
if ~flag
    msg = sprintf('Exit because BS-Step1 could not be processed for "%s"', filenameALL{1});
    logFileWarn(logFileId, nameSurvey, msg);
    return
end
logFileInfo(logFileId, nameSurvey, ['Computing Step1 for "' filenameALL{1} '" is over'])

%% BS estimation

LayersPingBeam = [LayerStep1 LayersPingBeam];

bins{1} = -85:85;
indLayerIncidenceAngle = findIndLayerSonar(LayersPingBeam, 1, 'strDataType', 'IncidenceAngle');

Selection.Name{1}  = LayersPingBeam(indLayerIncidenceAngle).Name;
Selection.TypeVal  = 'Layer';
Selection.indLayer = indLayerIncidenceAngle;
Selection.DataType = LayersPingBeam(indLayerIncidenceAngle).DataType;
Selection.Unit     = 'deg';

CourbeBias = 0;
if isempty(maskName)
    nomCourbe = ['BS_' nameSurvey '_Reflectivity_IncidenceAngle']; %['BS : ' nameSurvey]; % TODO
else
    nomCourbe = ['BS_' nameSurvey '_' maskName '_Reflectivity_IncidenceAngle']; %['BS : ' nameSurvey]; % TODO
end
Commentaire  = {'Computed by BatchBSCurvesAndCompMosaics'};
Coul         = {};
nbPMin       = 10;
SelectMode   = [];
SelectSwath  = [];
SelectFM     = [];
MeanCompType = 2; % TODO : v�rifier la signification
SameReflectivityStatus = 1;
% InfoCompensationCurve.FileName                 = {};
% InfoCompensationCurve.ModeDependant            = 0;
% InfoCompensationCurve.UseModel                 = [];
% InfoCompensationCurve.PreserveMeanValueIfModel = [];

%     if iscell(bilan)
%         bilan_k2 = bilan{k2};
%     else
%         bilan_k2 = bilan(k2);
%     end

logFileInfo(logFileId, nameSurvey, 'Computing BS angular curves is starting')
[flag, LayersPingBeam, indCourbeBS, Fig] = SimradAll_Statistics_CurvesBS(LayersPingBeam, 1, filenameALL, ...
    bins, Selection, CourbeBias, nomCourbe, Commentaire, ...
    Coul, nbPMin, SelectMode, SelectSwath, SelectFM, MeanCompType, ...
    MaskLatLong, valMask, SameReflectivityStatus, InfoCompensationCurve, 'XLim', [-70 70], ...
    'CleanCurves', flagInteractivity(1:3), 'Carto', Carto, 'MaskOverlapingIncidenceAngle', 1, 'flagMarcRoche', 1);
if ~flag
    logFileWarn(logFileId, nameSurvey, 'Exit : Computing BS angular curves failed');
    return
end
my_close(Fig);
logFileInfo(logFileId, nameSurvey, 'Computing BS angular curves is over')

%% Export de la courbe

logFileInfo(logFileId, nameSurvey, 'Export of BS curve and model is starting')
if ~exist(resultsDirName, 'dir')
    status = mkdir(resultsDirName);
    if ~status
        messageErreur(nomDir)
        flag = 0;
        return
    end
end
BsCurveModelFilename = fullfile(resultsDirName, [nomCourbe '.mat']);
exportCourbesStats(LayersPingBeam(1), BsCurveModelFilename, 'sub', indCourbeBS, 'Tag', 'BS', 'Mute', 1); % TODO : overwrite
logFileInfo(logFileId, nameSurvey, 'Export of BS curve and model is over')

%% Compute curves

bins{2} = [1 2]; % TODO : attention : EM3002 ici
indLayerBeamPointingAngle = findIndLayerSonar(LayersPingBeam, 1, 'strDataType', 'BeamPointingAngle');
indLayerTxBeamIndex       = findIndLayerSonar(LayersPingBeam, 1, 'strDataType', 'TxBeamIndex');
        
Selection.Name{1}  = LayersPingBeam(indLayerBeamPointingAngle).Name;
Selection.TypeVal  = 'Layer';
Selection.indLayer = indLayerBeamPointingAngle;
Selection.DataType = LayersPingBeam(indLayerBeamPointingAngle).DataType;
Selection.Unit     = 'deg';
                
Selection(2).Name{1}  = LayersPingBeam(indLayerTxBeamIndex).Name;
Selection(2).TypeVal  = 'Layer';
Selection(2).indLayer = indLayerTxBeamIndex;
Selection(2).DataType = LayersPingBeam(indLayerTxBeamIndex).DataType;
Selection(2).Unit     = 's';

% Comment� pour l'instant
% InfoCompensationCurve.FileName{1} = BsCurveModelFilename;
% InfoCompensationCurve.ModeDependant            = 0;
% InfoCompensationCurve.UseModel                 = 1;
% InfoCompensationCurve.PreserveMeanValueIfModel = 0;

% 
if isempty(InfoCompensationCurve)
    InfoCompensationCurve.FileName{1}              = BsCurveModelFilename;
    InfoCompensationCurve.ModeDependant            = 0;
    InfoCompensationCurve.UseModel                 = 1;
    InfoCompensationCurve.PreserveMeanValueIfModel = 0;
else
    InfoCompensationCurve.FileName{end+1}                 = BsCurveModelFilename;
    InfoCompensationCurve.ModeDependant                   = 1;
    InfoCompensationCurve.UseModel(end+1)                 = 1;
    InfoCompensationCurve.PreserveMeanValueIfModel(end+1) = 1;
end
        
logFileInfo(logFileId, nameSurvey, 'Computing DiagTx compensation curves is starting')
if isempty(maskName)
    nomCourbe = ['TxDiag_' nameSurvey '_Reflectivity_BeamPointingAngle_TxBeamIndex']; %['BS : ' nameSurvey]; % TODO
else
    nomCourbe = ['TxDiag_' nameSurvey  '_' maskName '_Reflectivity_BeamPointingAngle_TxBeamIndex']; %['BS : ' nameSurvey]; % TODO
end
[flag, LayersPingBeam, Fig] = SimradAll_Statistics_Curves(LayersPingBeam, 1, filenameALL, ...
    bins, Selection, CourbeBias, nomCourbe, Commentaire, ...
    Coul, nbPMin, SelectMode, SelectSwath, SelectFM, MeanCompType, ...
    MaskLatLong, valMask, SameReflectivityStatus, InfoCompensationCurve, ...
    'Mute', -1, 'CleanCurves', flagInteractivity(4:5), 'flagMarcRoche', 1);
if ~flag
    logFileWarn(logFileId, nameSurvey, 'Exit : Computing DiagTx angular curves failed');
    return
end
my_close(Fig);
logFileInfo(logFileId, nameSurvey, 'Computing DiagTx compensation curves is over')

%% Export de la courbe

logFileInfo(logFileId, nameSurvey, 'Export of DiagTx compensation curves is starting')
TxDiagCurveModelFilename = fullfile(resultsDirName, [nomCourbe '.mat']);
exportCourbesStats(LayersPingBeam(1), TxDiagCurveModelFilename, 'Tag', 'TxDiag', 'last', 'Mute', 1); % TODO : overwrite
logFileInfo(logFileId, nameSurvey, 'Export of DiagTx compensation curves is over')

%% Mosaique compens�e

LayerName     = 'ReflectivityFromSnippets';
Window        = [3 3];
ModeDependant =  0;

% TODO : ajouter � InfoCompensationCurve
if ~flagBSCorr
    InfoCompensationCurve.FileName{1}              = BsCurveModelFilename;
    InfoCompensationCurve.FileName{2}              = TxDiagCurveModelFilename;
    InfoCompensationCurve.ModeDependant            = 0;
    InfoCompensationCurve.UseModel                 = [1 0];
    InfoCompensationCurve.PreserveMeanValueIfModel = [1 0];
end

InfoMos.TypeMos                     = 1;
InfoMos.TypeGrid                    = 1;
InfoMos.flagMasquageDual            = 1;
InfoMos.Import.MasqueActif          = 1;
InfoMos.Import.SlotFilename         = [];
InfoMos.Import.SonarTime            = [];
InfoMos.Covering.Priority           = 1;
InfoMos.Covering.SpecularLimitAngle = [];
InfoMos.LimitAngles                 = [];
InfoMos.gridSize                    = gridSize;
InfoMos.Backup                      = 10;

strGridSize = num2str(gridSize);
strGridSize = strrep(strGridSize, '.', ',');
baseName = sprintf('_MOS_%sm_', strGridSize);
InfoMos.Unique.nomFicErMapperLayer    = fullfile(resultsDirName, [nameSurvey baseName 'LatLong_Reflectivity.ers']);
InfoMos.Unique.nomFicErMapperAngle    = fullfile(resultsDirName, [nameSurvey baseName 'LatLong_RxAngleEarth.ers']);
InfoMos.Unique.CompleteExistingMosaic = 0;

logFileInfo(logFileId, nameSurvey, 'Compensated Reflectivity mosaic is starting')
[flag, LayerReflectivityLatLong] = SimradAllMosaiqueParProfil(LayersPingBeam, 1, filenameALL, ...
    ModeDependant, LayerName, Window, ...
    SameReflectivityStatus, InfoCompensationCurve, InfoMos, ...
    'Carto', Carto, 'MaskLatLong', MaskLatLong, 'flagMarcRoche', true);
if ~flag
    logFileWarn(logFileId, nameSurvey, 'Compensated Reflectivity mosaic failed')
    return
end
logFileInfo(logFileId, nameSurvey, 'Compensated Reflectivity mosaic is over')

%% Mosaique des angles d'incidence

LayerName = 'IncidenceAngle';
InfoMos.Unique.nomFicErMapperLayer = fullfile(resultsDirName, [nameSurvey baseName 'LatLong_IncidenceAngle.ers']);
SameReflectivityStatus = 0;

logFileInfo(logFileId, nameSurvey, 'Compensated IncidenceAngle mosaic is starting')
[flag, LayerIncidenceAngleLatLong] = SimradAllMosaiqueParProfil(LayersPingBeam, 1, filenameALL, ...
    ModeDependant, LayerName, Window, ...
    SameReflectivityStatus, [], InfoMos, ...
    'Carto', Carto, 'MaskLatLong', MaskLatLong);
if ~flag
    logFileWarn(logFileId, nameSurvey, 'Compensated IncidenceAngle mosaic failed')
    return
end
LayerIncidenceAngleLatLong(1).ColormapIndex = 17;
CLim = LayerIncidenceAngleLatLong(1).CLim;
LayerIncidenceAngleLatLong(1).CLim = [-max(abs(CLim)) max(abs(CLim))];
logFileInfo(logFileId, nameSurvey, 'Compensated IncidenceAngle mosaic is over')

%% Masquage de la mosaique

% if ~isempty(MaskLatLong)
%     logFileInfo(logFileId, nameSurvey, 'Masking mosaic is starting')
%     [LayerReflectivityLatLong, flag] = masquage(LayerReflectivityLatLong, 'LayerMask', MaskLatLong, 'valMask', valMask);
%     if ~flag
%         logFileWarn(logFileId, nameSurvey, 'Masking mosaic failed')
%         return
%     end
%     logFileInfo(logFileId, nameSurvey, 'Masking mosaic is over')
% end

%% Export des mosa�ques

ImageName = LayerReflectivityLatLong(1).Name;
filenameERS = fullfile(resultsDirName, ImageName);
logFileInfo(logFileId, nameSurvey, ['Exporting "' filenameERS '"'])
flag = export_ermapper(LayerReflectivityLatLong(1), filenameERS);
if ~flag
    msg = sprintf('Exit because "%s" could not be saved on disk', filenameERS);
    logFileWarn(logFileId, nameSurvey, msg);
    return
end

ImageName = LayerIncidenceAngleLatLong(1).Name;
filenameERS = fullfile(resultsDirName, ImageName);
logFileInfo(logFileId, nameSurvey, ['Exporting "' filenameERS '"'])
flag = export_ermapper(LayerIncidenceAngleLatLong(1), filenameERS);
if ~flag
    msg = sprintf('Exit because "%s" could not be saved on disk', filenameERS);
    logFileWarn(logFileId, nameSurvey, msg);
    return
end

ImageName = LayerReflectivityLatLong(2).Name;
filenameERS = fullfile(resultsDirName, ImageName);
logFileInfo(logFileId, nameSurvey, ['Exporting "' filenameERS '"'])
flag = export_ermapper(LayerReflectivityLatLong(2), filenameERS);
if ~flag
    msg = sprintf('Exit because "%s" could not be saved on disk', filenameERS);
    logFileWarn(logFileId, nameSurvey, msg);
    return
end

%% Transformation g�om�trique

logFileInfo(logFileId, nameSurvey, 'LatLong2GeoYX of compensated mosaic is starting')
[mos_GeoYX, flag] = LatLong2GeoYX(LayerReflectivityLatLong);
if ~flag
    return
end
logFileInfo(logFileId, nameSurvey, 'LatLong2GeoYX of compensated mosaic is over')

%% Export surfer

ImageName = mos_GeoYX(1).Name;
filenameSurfer = fullfile(resultsDirName, [ImageName '.grd']);
logFileInfo(logFileId, nameSurvey, ['Exporting "' filenameSurfer '" is starting'])
flag = export_surfer(mos_GeoYX(1), filenameSurfer);
if ~flag
    msg = sprintf('%s could not be saved', filenameSurfer);
    logFileWarn(logFileId, nameSurvey, msg);
    return
end
logFileInfo(logFileId, nameSurvey, ['Exporting "' filenameSurfer '" is over'])

%% Cr�ation du fichier 

fid = fopen(nomFicSurveyStatus, 'w+');
fprintf(fid, '%s : BatchBSCurvesAndCompMosaics is over\n', nameSurvey);
fclose(fid);

%% Message de fin de traitement

logFileInfo(logFileId, nameSurvey, 'Processing over for BatchBSCurvesAndCompMosaics')


function logFileInfo(logFileId, nameSurvey, msg)
logFileId.info('BatchBSCurvesAndCompMosaics', [nameSurvey ' - ' msg]);

function logFileWarn(logFileId, nameSurvey, msg)
logFileId.warn('BatchBSCurvesAndCompMosaics', [nameSurvey ' - ' msg]);
