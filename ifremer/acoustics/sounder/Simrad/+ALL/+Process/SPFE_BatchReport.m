% Description
%   Process SPFE_BatchReport
%
% Syntax
%    flag = ALL.Process.SPFE_BatchReport(surveyDirNames, CsvOutputFile)
%
% Input Arguments
%   surveyDirNames : Name of the directory names
%   CsvOutputFile  : Name of the output .csv file
%
% Output Arguments
%   flag : 0=KO, 1=OK
%
% Examples
%      repImport = 'F:\SPFE\SSC_TESTS_MARS2022;
%      [flag, surveyDirNames, CsvOutputFile, repImport] = ALL.Params.SPFE_BatchReport(repImport)
%    flag = ALL.Process.SPFE_BatchReport(surveyDirNames, CsvOutputFile) 
%
% Author : JMA
% See also Authors ALL.Params.SPFE_BatchReport

function flag = SPFE_BatchReport(surveyDirNames, CsvOutputFile) 

logFileId = getLogFileId;
sep = getCSVseparator;

N = length(surveyDirNames);
if N == 0
    flag = false;
    logFileId.warn('SPFE_BatchReport', 'Exit because surveyDirNames is empty');
    return
end

%% Lecture des tables

flag = false(1, N);
hw = create_waitbar('Processing SPFE_BatchReport', 'N', N);
for k=1:N
    [flag(k), tOut] = SPFE_BatchReport_unitaire(surveyDirNames{k}, logFileId);
    if flag(k)
        T{k} = tOut; %#ok<AGROW>
    else
        T{k} = table.empty; %#ok<AGROW>
    end
    my_waitbar(k, N, hw)
end
my_close(hw, 'MsgEnd')
if all(~flag)
    flag = 0;
    return
end

T = T(flag);
surveyDirNames = surveyDirNames(flag);

%% Plot des courbes

Fig = FigUtils.createSScFigure('Position', centrageFig(1600, 800));
for k=1:length(T)
    Val = T{k}.Variables;
    [~, Title{k}] = fileparts(surveyDirNames{k}); %#ok<AGROW>
    
	FigUtils.createSScFigure;
    PlotUtils.createSScPlot(Val(:,1), Val(:,2), '.'); grid on; hold on;
    PlotUtils.createSScPlot(Val(:,1), Val(:,4), 'b'); xlabel('Incidence angle (deg)'); ylabel('BS (dB)'); title(Title{k});
    
    figure(Fig); PlotUtils.createSScPlot(Val(:,1), Val(:,4)); grid on; hold on; xlabel('Incidence angle (deg)'); ylabel('BS (dB)'); title('All surveys');
end
figure(Fig); legend(Title, 'location', 'eastoutside', 'Interpreter', 'None');

%% On compl�tes les tables pour qu'elles aient toutes le m�me nombre de lignes

for k=1:length(T)
    n(k) = size(T{k}, 1); %#ok<AGROW>
end
nMax = max(n);
for k=1:length(T)
    nb = nMax - n(k);
    if nb ~= 0
        [~, nameSurvey] = fileparts(surveyDirNames{k});
        TEnd = my_table(nameSurvey, NaN(nb,1), NaN(nb,1), NaN(nb,1), NaN(nb,1));
        T{k} = [T{k}; TEnd];
    end
end

%% Concat�nation des tables

TAll = T{1};
for k=2:length(T)
    TAll  = [TAll T{k}]; %#ok<AGROW>
end

%% Export des courbes dans le .csv

writetable(TAll, CsvOutputFile, 'Delimiter', sep)

flag = 1;


function [flag, T] = SPFE_BatchReport_unitaire(surveyDirName, logFileId)

flag = 0;
T    = [];

[dirName, nameSurvey] = fileparts(surveyDirName);
dirNameSurvey  = fullfile(dirName, nameSurvey);

logFileInfo(logFileId, nameSurvey, 'Processing SPFE_BatchReport is starting')

%%

% resultsPathname = fullfile(dirNameSurvey, '100_BS_MOSAIC_FULL_PER_LINE_G');
resultsPathname = fullfile(dirNameSurvey, '102_BS_CURVESinMASK');
if exist(resultsPathname, 'dir')
%     BSCurveFilename = fullfile(resultsPathname, ['BS_' nameSurvey '_Reflectivity_IncidenceAngle.mat']);
    BSCurveFilename = listeFicOnDir(resultsPathname, '.mat', 'Filtre', '_Reflectivity_IncidenceAngle', 'FiltreExclude', 'Residuals');
    if isempty(BSCurveFilename)
        return
    end
    if iscell(BSCurveFilename)
        BSCurveFilename = BSCurveFilename{1};
    end
    if exist(BSCurveFilename, 'file')
        [CourbeConditionnelle, flag] = load_courbesStats(BSCurveFilename);
        if flag
            x      = CourbeConditionnelle.bilan{1}.x;
            y      = CourbeConditionnelle.bilan{1}.y;
            std    = CourbeConditionnelle.bilan{1}.std;
            model  = CourbeConditionnelle.bilan{1}.model;
            yModel = model.compute('x', x);
            
%             FigUtils.createSScFigure; plot(x, y, '.'); grid on; hold on; plot(x, yModel, 'b'); xlabel('Incidence angle (deg)'); ylabel('BS (dB)'); title(nameSurvey);
            
            T = my_table(nameSurvey, x(:), y(:), std(:), yModel(:));
%             head(T)
        end
    else
        return
    end
else
    return
end

%% Message de fin de traitement

logFileInfo(logFileId, nameSurvey, 'Processing SPFE_BatchReport is over')


function logFileInfo(logFileId, nameSurvey, msg)
logFileId.info('SPFE_BatchReport', [nameSurvey ' - ' msg]);

function logFileWarn(logFileId, nameSurvey, msg)
logFileId.warn('SPFE_BatchReport', [nameSurvey ' - ' msg]);


function T = my_table(nameSurvey, x, y, std, yModel)

T = table(x(:), y(:), std(:), yModel(:));

% % Noms des variables incompatible en R2018b compil�e mais OK en R2019b
% T.Properties.VariableNames{1} = [nameSurvey ' - Incidence angles'];
% T.Properties.VariableNames{2} = [nameSurvey ' - Bs'];
% T.Properties.VariableNames{3} = [nameSurvey ' - std'];
% T.Properties.VariableNames{4} = [nameSurvey ' - Model'];
% T.Properties.VariableUnits = {'deg', 'dB', 'dB', 'dB'};

T.Properties.VariableNames{1} = [nameSurvey '_IncidenceAngles'];
T.Properties.VariableNames{2} = [nameSurvey '_Bs'];
T.Properties.VariableNames{3} = [nameSurvey '_std'];
T.Properties.VariableNames{4} = [nameSurvey '_Model'];
T.Properties.VariableUnits = {'deg', 'dB', 'dB', 'dB'};
