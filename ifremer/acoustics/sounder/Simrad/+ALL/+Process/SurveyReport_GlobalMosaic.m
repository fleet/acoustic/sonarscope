% Description
%   SurveyReport_GlobalMosaic
%
% Syntax
%     flag = ALL.Process.SurveyReport_GlobalMosaic(adocFileName, listeNomFic, gridSize, ...)
%
% Input Arguments 
%   adocFileName :
%   listeNomFic  :
%   gridSize     :
%
% Name-Value Pair Arguments
%   IndNavUnit                        : 
%   AreaName                          : 
%   InfoCompensationCurveReflectivity : 
%
% Output Arguments
%   flag : 0=KO, 1=OK

%
% Examples
%     flag = ALL.Process.SurveyReport_GlobalMosaic(adocFileName, listeNomFic, gridSize)
%
% Author : JMA
% See also Authors

function flag = SurveyReport_GlobalMosaic(adocFileName, listeNomFic, gridSize, varargin)

[varargin, IndNavUnit] = getPropertyValue(varargin, 'IndNavUnit', []);
[varargin, AreaName]   = getPropertyValue(varargin, 'AreaName',   []);
[varargin, InfoCompensationCurveReflectivity] = getPropertyValue(varargin, 'InfoCompensationCurveReflectivity', []); %#ok<ASGLU>

A0 = cl_simrad_all([]);

%% Create directory for files

[nomDirSummary, SurveyName] = fileparts(adocFileName);
nomDirFiles = fullfile(nomDirSummary, SurveyName, 'ERS');

%% Nom du fichier ADOC

[nomDirSummary, SurveyName] = fileparts(adocFileName);

%% MNT

Tag = num2str(randi(100000));
    
NomLayer = 'Depth+Draught-Heave';
if isempty(AreaName)
    nomImage = SurveyName;
else
    AE = strrep(AreaName, ' ', '');
    AE = rmblank(AE);
    AE = strrep(AE, '-', '_');
    AE = strrep(AE, '+', '_');
    AE = strrep(AE, '[', '');
    AE = strrep(AE, ']', '');
    AE = str2Filename(AE);
    nomImage = [SurveyName '-' AE];
end
[flag, a] = SimradAllMosaiqueParametersByDefault(A0, listeNomFic, nomImage, NomLayer, ...
    gridSize, nomDirFiles, 'IndNavUnit', IndNavUnit);
if ~flag
    return
end

a(1).CLim          = '0.5%';
a(1).ColormapIndex = 20;

[nomFicFig, nomFicPng, nomFicKmz, nomFicGeotif, TypeGeotif, ImageName] = exportMosaicsFig2PngAndFigFiles(a(1), nomDirSummary, SurveyName, Tag);

AdocUtils.addMosaic(adocFileName, nomFicFig, nomFicPng, nomFicKmz, nomFicGeotif, TypeGeotif, ImageName, Tag, ...
    'Title1', 'Mosaics', 'AreaName', AreaName, 'listDataFiles', listeNomFic);

b = sunShadingGreatestSlope(a(1), 1);

[nomFicFig, nomFicPng, nomFicKmz, nomFicGeotif, TypeGeotif, ImageName] = exportMosaicsFig2PngAndFigFiles(b, nomDirSummary, SurveyName, Tag);

AdocUtils.addMosaic(adocFileName, nomFicFig, nomFicPng, nomFicKmz, nomFicGeotif, TypeGeotif, ImageName, Tag);

%% Carte des pentes

[a, flag] = slopeMax(a(1));
if ~flag
    return
end

a.CLim          = '0.5%';
a.ColormapIndex = 20;

[nomFicFig, nomFicPng, nomFicKmz, nomFicGeotif, TypeGeotif, ImageName] = exportMosaicsFig2PngAndFigFiles(a(1), nomDirSummary, SurveyName, Tag);

AdocUtils.addMosaic(adocFileName, nomFicFig, nomFicPng, nomFicKmz, nomFicGeotif, TypeGeotif, ImageName, Tag);

%% Mosaique de réflectivité

NomLayer = 'ReflectivityFromSnippets';
if isempty(AreaName)
    nomImage = SurveyName;
else
    AE = str2Filename(AreaName);
    AE = strrep(AE, ' ', '');
    AE = rmblank(AE);
    AE = strrep(AE, '-', '_');
    AE = strrep(AE, '+', '_');
    AE = strrep(AE, '[', '');
    AE = strrep(AE, ']', '');
    nomImage = [SurveyName '-' AE];
end

[flag, a] = SimradAllMosaiqueParametersByDefault(A0, listeNomFic, nomImage, NomLayer, gridSize, nomDirFiles, ...
    'IndNavUnit', IndNavUnit);
if ~flag
    return
end

a(1).CLim = '1%';

[nomFicFig, nomFicPng, nomFicKmz, nomFicGeotif, TypeGeotif, ImageName, nomFicPngInverseVideo, nomFicKmzInverseVideo] ...
    = exportMosaicsFig2PngAndFigFiles(a(1), nomDirSummary, SurveyName, Tag);

AdocUtils.addMosaic(adocFileName, nomFicFig, nomFicPng, nomFicKmz, nomFicGeotif, TypeGeotif, ImageName, Tag, ...
    'nomFicPngInverseVideo', nomFicPngInverseVideo, 'nomFicKmzInverseVideo', nomFicKmzInverseVideo);

%% Mosaique de réflectivité avec fichiers de compensation

if ~isempty(InfoCompensationCurveReflectivity)
    NomLayer = 'ReflectivityFromSnippets';
    nomImage = [nomImage '-Comp'];
    [flag, a] = SimradAllMosaiqueParametersByDefault(A0, listeNomFic, nomImage, NomLayer, gridSize, nomDirFiles, ...
        'IndNavUnit', IndNavUnit, ...
        'InfoCompensationCurve', InfoCompensationCurveReflectivity);
    if ~flag
        return
    end
    
    a(1).CLim = '3%';
    
    [nomFicFig, nomFicPng, nomFicKmz, nomFicGeotif, TypeGeotif, ImageName, nomFicPngInverseVideo, nomFicKmzInverseVideo] ...
        = exportMosaicsFig2PngAndFigFiles(a(1), nomDirSummary, SurveyName, ['Comp-' Tag]);

    InfoCompensationCurveReflectivity = AdocUtils.exportCompensationCurve(InfoCompensationCurveReflectivity, nomDirSummary, SurveyName, Tag);

    AdocUtils.addMosaic(adocFileName, nomFicFig, nomFicPng, nomFicKmz, nomFicGeotif, TypeGeotif, [ImageName ' - Compensated'], ['Comp-' Tag], ...
        'nomFicPngInverseVideo', nomFicPngInverseVideo, 'nomFicKmzInverseVideo', nomFicKmzInverseVideo, ...
        'InfoCompensationCurve', InfoCompensationCurveReflectivity);
end

%% Réouverture du fichier Adoc

AdocUtils.openAdoc(adocFileName);
