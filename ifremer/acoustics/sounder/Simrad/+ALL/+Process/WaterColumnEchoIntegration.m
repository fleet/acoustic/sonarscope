% Description
%   WaterColumnEchoIntegration
%
% Syntax
%    ALL.Process.WaterColumnEchoIntegration(ListeFicXmlEchogram, ListeFicAll, nomDirOut, typePolarEchogram, ...)
%
% Input Arguments
%   ListeFicXmlEchogram :
%   ListeFicAll         :
%   nomDirOut           :
%   typePolarEchogram   :
%
% Name-Value Pair Arguments
%   suby          : 
%   Display       : 
%   params_ZoneEI : 
%   Tag           : 
%   Info          : 
%
% Examples
%    ALL.Process.WaterColumnEchoIntegration(ListeFicXmlEchogram, ListeFicAll, nomDirOut, typePolarEchogram)
%
% Author : JMA
% See also Authors

function WaterColumnEchoIntegration(ListeFicXmlEchogram, ListeFicAll, nomDirOut, typePolarEchogram, varargin)

global isUsingParfor %#ok<GVMIS>
isUsingParfor = 0;

[varargin, suby]          = getPropertyValue(varargin, 'suby',          []);
[varargin, Display]       = getPropertyValue(varargin, 'Display',       0);
[varargin, params_ZoneEI] = getPropertyValue(varargin, 'params_ZoneEI', []);
[varargin, Tag]           = getPropertyValue(varargin, 'Tag',           '');
[varargin, Info]          = getPropertyValue(varargin, 'Info',          ''); %#ok<ASGLU>

useParallel = get_UseParallelTbx;

Carto = [];

if ~iscell(ListeFicXmlEchogram)
    ListeFicXmlEchogram = {ListeFicXmlEchogram};
end
if isempty(ListeFicXmlEchogram)
    return
end
nbFic = length(ListeFicXmlEchogram);

%% Question // tbx au cas o� les .all auraient d�j� �t� convertis

if Display == 0
    useParallelHere = useParallel;
else
    useParallelHere = 0;
end

%% Action

str1 = 'Calcul de l''�cho-int�gration de la colonne d''eau des sondeurs Kongsberg en cours';
str2 = 'Processing echo integration of WCD for Kongsberg MBES';
msg = Lang(str1,str2);
if ~isempty(Info)
    msg = sprintf('%s\n%s', Info, msg);
end
if useParallelHere && (nbFic > 2)
    [hw, DQ] = create_parforWaitbar(nbFic, msg);
    parfor (k=1:nbFic, useParallelHere)
        process_ALL_WaterColumnEchoIntegration_unitaire(ListeFicAll{k}, ListeFicXmlEchogram{k}, ...
            nomDirOut, typePolarEchogram, Carto, Display, params_ZoneEI, Tag, suby, ...
            'isUsingParfor', true);
        send(DQ, k);
    end
    my_close(hw, 'MsgEnd', 'TimeDelay', 10)
else
    hw = create_waitbar(msg, 'N', nbFic);
    for k=1:nbFic
        my_waitbar(k, nbFic, hw)
        [~, Carto] = process_ALL_WaterColumnEchoIntegration_unitaire(ListeFicAll{k}, ListeFicXmlEchogram{k}, ...
            nomDirOut, typePolarEchogram, Carto, Display, params_ZoneEI, Tag, suby);
    end
    my_close(hw, 'MsgEnd', 'TimeDelay', 10)
end


function [flag, Carto] = process_ALL_WaterColumnEchoIntegration_unitaire(ListeFicAll, ListeFicXmlEchogram, ...
    nomDirOut, typePolarEchogram, Carto, Display, params_ZoneEI, Tag, suby, varargin)

global isUsingParfor %#ok<GVMIS>

[varargin, isUsingParfor] = getPropertyValue(varargin, 'isUsingParfor', isUsingParfor); %#ok<ASGLU>

I0 = cl_image_I0;

[flag, b, Carto] = sonar_SimradEchoIntegration(I0, typePolarEchogram, ListeFicXmlEchogram, params_ZoneEI, Tag, Display, ...
    'nomFicAll', ListeFicAll, 'Carto', Carto, 'suby', suby);
if ~flag
    return
end

for k=1:length(b)
    nomFicEI = b(k).Name;
    nomFicEI = fullfile(nomDirOut, [nomFicEI '.xml']);
    flag = export_xml(b(k), nomFicEI);
    if ~flag
        messageErreurFichier(nomFicEI, 'WriteFailure');
        return
    end
end
