function [flag, InfoCompensationCurve] = getBSCorrCurves(dirNameSurvey, logFileId, nameSurvey)

flag                  = 0;
InfoCompensationCurve = [];

%% Vérification si le répertoire 01_BSCorr existe

nomDirTxCurves = fullfile(dirNameSurvey, '01_BSCorr');
if ~exist(nomDirTxCurves, 'dir')
    logFileInfo(logFileId, nameSurvey, ['Directory ' nomDirTxCurves ' does not exist'])
    return
end

%% recherche des fichiers xx_comp.mat dans le répertoire 01_BSCorr

listeFicTxCurves = listeFicOnDir(nomDirTxCurves, '.mat', 'Filtre', '_comp');
if isempty(listeFicTxCurves)
    logFileInfo(logFileId, nameSurvey, ['Directory ' nomDirTxCurves ' is empty (no xxx_comp.mat files found)'])
    return
end

%% Lecture des courbes

for k2=1:length(listeFicTxCurves)
    CourbeConditionnelle = load_courbesStats(listeFicTxCurves{k2});
    if isempty(CourbeConditionnelle)
        logFileInfo(logFileId, nameSurvey, ['Tx curve ' listeFicTxCurves{k2} ' is empty'])
        return
    end
    bilan = CourbeConditionnelle.bilan;
    if isfield(bilan{1}(1), 'Mode') && ~isempty(bilan{1}(1).Mode) && (bilan{1}(1).Mode ~= 0)
        UseModel(k2)  = 1; %#ok<AGROW>
    else
        UseModel(k2)  = 0; %#ok<AGROW>
    end
    PreserveMeanValueIfModel(k2) = 0; %#ok<AGROW>
end

InfoCompensationCurve.FileName      = listeFicTxCurves;
InfoCompensationCurve.ModeDependant = 1;
InfoCompensationCurve.UseModel      = UseModel;
InfoCompensationCurve.PreserveMeanValueIfModel = PreserveMeanValueIfModel;
flag = 1;


function logFileInfo(logFileId, nameSurvey, msg)
logFileId.info('SPFE_BatchReport', [nameSurvey ' - ' msg]);

% function logFileWarn(logFileId, nameSurvey, msg)
% logFileId.warn('SPFE_BatchReport', [nameSurvey ' - ' msg]);
