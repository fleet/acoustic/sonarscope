% Description
%   Params for checkFiles
%
% Syntax
%    [flag, this, isDual, DataInstallationParameters, IndNavUnit] = ALL.checkFiles(liste, ...)
%
% Input Arguments
%   liste : List of .all files
%
% Name-Value Pair Arguments
%   useCacheNetcdf  : 
%   ShowProgressBar : 
%   flagIndNavUnit  : 
%
% Output Arguments
%   flag                       : 0=KO, 1=OK
%   this                       : 
%   isDual                     : 
%   DataInstallationParameters : 
%   IndNavUnit                 : 
%
% Examples
%    [flag, this, isDual, DataInstallationParameters, IndNavUnit] = ALL.checkFiles(liste)
%
% Author : JMA
% See also Authors

function [flag, this, isDual, DataInstallationParameters, IndNavUnit] = checkFiles(liste, varargin)

global isUsingParfor   %#ok<GVMIS>
global IfrTbxResources %#ok<GVMIS>
global IfrTbx          %#ok<GVMIS>
global useCacheNetcdf  %#ok<GVMIS>
global useParallel     %#ok<GVMIS>

isUsingParfor = 0;

[varargin, useCacheNetcdf]  = getPropertyValue(varargin, 'useCacheNetcdf',  useCacheNetcdf);
[varargin, ShowProgressBar] = getPropertyValue(varargin, 'ShowProgressBar', true);
[varargin, flagIndNavUnit]  = getPropertyValue(varargin, 'flagIndNavUnit',  false); %#ok<ASGLU>

useParallel = get_UseParallelTbx;
logFileId   = getLogFileId;

useCacheNetcdfHere = useCacheNetcdf;
logFileIdHere      = logFileId;

flag                       = 0;
isDual                     = [];
DataInstallationParameters = [];
IndNavUnit                 = [];
this                       = cl_simrad_all([]);

if ~iscell(liste)
    liste = {liste};
end

N = length(liste);
if N == 0
    return
end

isDual = false(1,N);

%% S�lection du sensor de navigation

if flagIndNavUnit
    for kFic=1:N
        [flag, allKM1] = checkKMFile(liste{kFic}, ShowProgressBar,  useCacheNetcdfHere, logFileIdHere);
        if flag
            [flag, IndNavUnit] = params_ALL_IndexNavigationSystem(allKM1);
            if flag
                break
            else
                return
            end
        end
    end
end

%% D�codage en parall�le

N = length(liste);
if useParallel && (N > 2)
    %% Lancement du premier fichier sans le parall�lisme pour pouvoir r�pondre aux questions
    
    cl_simrad_all('nomFic', liste{1});
    
    %% Lancement des d�codages en parall�le
    
    Val_IfrTbxResources = IfrTbxResources;
    Val_IfrTbx = IfrTbx;
    str1 = 'V�rification des fichiers 1/2 .all';
    str2 = 'Checking .all files 1/2 with // Tbx';
    [hw, DQ] = create_parforWaitbar(N, Lang(str1,str2));
    send(DQ, 1);
    useParallelHere = useParallel;
    parfor (k=2:N, useParallel)
%     for k=2:N
%         all2ssc(liste{k}, 'isUsingParfor', true, ...
%             'IfrTbxResources', Val_IfrTbxResources, 'IfrTbx', Val_IfrTbx, ...
%             'useCacheNetcdf', useCacheNetcdfHere, 'useParallel', useParallelHere, ...
%             'logFileId', logFileIdHere);
        
        % Modif JMA le 12/10/2021
        [~, ~, Ext] = fileparts(liste{k});
        switch Ext
            case '.all'
                all2ssc(liste{k}, 'isUsingParfor', true, ...
                    'IfrTbxResources', Val_IfrTbxResources, 'IfrTbx', Val_IfrTbx, ...
                    'useCacheNetcdf', useCacheNetcdfHere, 'useParallel', useParallelHere, ...
                    'logFileId', logFileIdHere);
            case '.kmall'
                KmallXsf2ssc(liste{k}, 'isUsingParfor', true, ...
                    'IfrTbxResources', Val_IfrTbxResources, 'IfrTbx', Val_IfrTbx, ...
                    'useCacheNetcdf', useCacheNetcdfHere, 'useParallel', useParallelHere, ...
                    'logFileId', logFileIdHere);
        end
        send(DQ, k);
    end
    my_close(hw)
    
    redoAll2sscIfNetcdfFailed(liste, useCacheNetcdf, logFileIdHere)
end

%% Cr�ation des instances et r�cup�ration de quelques infos

listAll = repmat(cl_simrad_all, N, 1);
flagOK  = false(N,1);
DataInstallationParameters = cell(N,1);
str1 = 'Consultation pr�liminaire des fichiers 2/2 .all';
str2 = 'Checking the .all files 2/2';
if useParallel && (N > 1)
    [hw, DQ] = create_parforWaitbar(N, Lang(str1,str2));
    useParallelHere = useParallel;
    parfor (kFic=1:N, useParallelHere)
        [flag2, allKM1, DataInstallation1, isDualFile1] = checkKMFile(liste{kFic}, ShowProgressBar, ...
            useCacheNetcdfHere, logFileIdHere, 'isUsingParfor', 1);
        if flag2
            flagOK(kFic) = 1;
            listAll(kFic) = allKM1;
            isDual(kFic) = isDualFile1;
            DataInstallationParameters{kFic} = DataInstallation1;
        end
        send(DQ, kFic);
    end
    my_close(hw);
else
    hw = create_waitbar(Lang(str1,str2), 'N', N);
    for kFic=1:N
        [flag, allKM1, DataInstallation1, isDualFile1] = checkKMFile(liste{kFic}, ShowProgressBar, ...
            useCacheNetcdfHere, logFileIdHere);
        if flag
            flagOK(kFic) = 1;
            listAll(kFic) = allKM1;
            isDual(kFic) = isDualFile1;
            DataInstallationParameters{kFic} = DataInstallation1;
        end
        my_waitbar(kFic, N, hw)
    end
    my_close(hw)
end
listAll = listAll(flagOK);
if isempty(listAll)
    flag = 0;
    return
end
isDual  = isDual(flagOK);
DataInstallationParameters = DataInstallationParameters(flagOK);
DataInstallationParameters = DataInstallationParameters(1);
this = listAll;

%% Message "aucun fichier trouv�"

if isempty(listAll)
    str1 = 'Aucun fichier n''a �t� trouv�. V�rifiez le nom du r�pertoire. Vous pouvez utiliser le menu "Info", "Utilities", "Remplacement d''une cha�ne de caract�res par une autre dans un fichier" pour r�soudre le probl�me.';
    str2 = 'No file has been found. Please check the folder name. You can use "Info", "Utilities", "Find & Replace" to solve the problem.';
    my_warndlg(Lang(str1,str2), 1);
    return
end

%% Suppression des fichier memmapfile

deleteFileOnListe(cl_memmapfile.empty, gcbf); % Ajout JMA le 23/10/2022 pour tester si �a r�soud les pbs de Carla (arr�t robot si beaucoup de fichiers � traiter)

flag = 1;



function [flag, allKM, DataInstallationParameters, isDualFile] = checkKMFile(nomFic, ...
    ShowProgressBar, useCacheNetcdfHere, logFileIdHere, varargin)

global isUsingParfor  %#ok<GVMIS>
global useCacheNetcdf %#ok<GVMIS>
global logFileId %#ok<GVMIS>

[varargin, isUsingParfor] = getPropertyValue(varargin, 'isUsingParfor', 0);  %#ok<ASGLU>

useCacheNetcdf = useCacheNetcdfHere;
logFileId      = logFileIdHere;

flag = 0;
DataInstallationParameters = [];
isDualFile = [];

allKM = cl_simrad_all('nomFic', nomFic, 'ShowProgressBar', ShowProgressBar);
if isempty(allKM)
    return
end

[flag, DataInstallationParameters, isDualFile] = read_installationParameters_bin(allKM);
if ~flag
    isDualFile = 0; % Cas des fichiers hydromapp�s du NIWA
end


function redoAll2sscIfNetcdfFailed(liste, useCacheNetcdf, logFileId)

if useCacheNetcdf ~= 3
    return
end

N = length(liste);
for k=1:N
    [pathname, filename] = fileparts(liste{k});
    XMLBinDirname = fullfile(pathname, 'SonarScope', filename);
    if exist(XMLBinDirname, 'dir')
        msg = sprintf('SonarScope detected that the Netcdf file failed for "%s", it is trying to redo it now',  liste{k});
        logFileId.info('cl_simrad_all/CheckFiles', msg);
        ALL_Cache2Netcdf(liste{k}, 'deleteXMLBin', 1);
    end
end
