% Description
%   Select the output .xml file for Nav & Attitude for GLOBE display
%
% Syntax
%    strSummary = ALL.createListAllItems
%
% Output Arguments
%   strSummary : 
%
% Examples
%    strSummary = ALL.createListAllItems
%
% Author : JMA
% See also Authors

function strSummary = createListAllItems

strSummary = {};
strSummary{end+1} = 'Directory';
strSummary{end+1} = 'File name';
strSummary{end+1} = 'EmModel';
strSummary{end+1} = 'Mode';
strSummary{end+1} = 'strMode';

%% Modes suppl�mentaires pour EM2040

strSummary{end+1} = 'Mode2 if EM2040';
strSummary{end+1} = 'strMode2';
strSummary{end+1} = 'Mode3 if EM2040';
strSummary{end+1} = 'strMode3';

%% Suite
strSummary{end+1} = 'NbSwaths';
strSummary{end+1} = 'FM';
strSummary{end+1} = 'WC';
strSummary{end+1} = 'NbPings';
strSummary{end+1} = 'Nb Soundings (Bathy)';
strSummary{end+1} = 'Nb Beams (WC)';
strSummary{end+1} = 'Main frequency (kHz)';
strSummary{end+1} = 'Total pulse length (ms)';
strSummary{end+1} = 'Effective pulse length (ms)';
strSummary{end+1} = 'RT-IA pulse length (ms)';
strSummary{end+1} = 'Sampling freq Bathy (Hz)';
strSummary{end+1} = 'Sampling freq WC (Hz)';
strSummary{end+1} = 'Nyquist Bathy';
strSummary{end+1} = 'Nyquist WC';
strSummary{end+1} = 'Nb SoundSpeedProfiles';
strSummary{end+1} = 'Mean Depth (m)';
strSummary{end+1} = 'Std Depth (m)';
strSummary{end+1} = 'Min Depth (m)';
strSummary{end+1} = 'Max Depth (m)';
strSummary{end+1} = 'Valid raw soundings (%)';
strSummary{end+1} = 'Cleaned soundings (%)';
strSummary{end+1} = 'Amplitude detections (%)';
strSummary{end+1} = 'AUV Immersion (m)';
strSummary{end+1} = 'AUV Altitude over seabed (m)';

strSummary{end+1} = 'BSMean (dB)';
strSummary{end+1} = 'BSN';
strSummary{end+1} = 'BSO';
strSummary{end+1} = 'BSN-BSO';

strSummary{end+1} = 'Swath Width (m)';
strSummary{end+1} = 'Angular Range (deg)';
strSummary{end+1} = 'Swath width / Depth';
strSummary{end+1} = 'Surface Sound Speed (m/s)';
strSummary{end+1} = 'Absorption coef (dB/km)';
strSummary{end+1} = 'Speed (m/s)';
strSummary{end+1} = 'Speed (nds)';
strSummary{end+1} = 'Dist inter ping (m)';
strSummary{end+1} = 'Dist max inter beam (m)';
strSummary{end+1} = 'Ratio Dist inter Beams/Pings';
strSummary{end+1} = 'Recurrence inter ping (s)';
strSummary{end+1} = 'Line length (m)';
strSummary{end+1} = 'Line coverage (km2)';
strSummary{end+1} = 'Latitude';
strSummary{end+1} = 'Longitude';
strSummary{end+1} = 'GPS mode';
strSummary{end+1} = 'QFAmp';
strSummary{end+1} = 'QFPhase';

strSummary{end+1} = 'Spike Filter';
strSummary{end+1} = 'Range Gates';
strSummary{end+1} = 'Phase Ramp';
strSummary{end+1} = 'Slope Filter';
strSummary{end+1} = 'Detection Mode';
strSummary{end+1} = 'Sector Tracking';
strSummary{end+1} = 'Aeration Filter';
strSummary{end+1} = 'Interference Filter';
strSummary{end+1} = 'Penetration Filter';
strSummary{end+1} = 'Heading (deg)';
strSummary{end+1} = 'Direction';
strSummary{end+1} = 'Type Line';
strSummary{end+1} = 'Roll (mean)';
strSummary{end+1} = 'Roll (std)';
strSummary{end+1} = 'Pitch (mean)';
strSummary{end+1} = 'Pitch (std)';
strSummary{end+1} = 'Heave (mean)';
strSummary{end+1} = 'Heave (std)';
strSummary{end+1} = 'Pings with RTK (%)';
strSummary{end+1} = 'Tide (m)';
strSummary{end+1} = 'Nb pings single swath';
strSummary{end+1} = 'Nb pings double swath';
strSummary{end+1} = 'Nb pings with WC';
strSummary{end+1} = 'Nb pings without WC';
strSummary{end+1} = 'WC TVGFunctionApplied';
strSummary{end+1} = 'WC TVGOffset (dB)';
strSummary{end+1} = 'Nb pings with FM';
strSummary{end+1} = 'Nb pings without FM';
for k=0:8
    strSummary{end+1} = sprintf('Nb pings Mode %d', k); %#ok<AGROW>
end
strSummary{end+1} = 'File size';
strSummary{end+1} = 'Stave Data';
strSummary{end+1} = 'Mag & Phase';

%% Syst�mes de navigation

strSummary{end+1} = 'Nb position systems';
strSummary{end+1} = 'Active position #';

%% Centrales d'attitude

strSummary{end+1} = 'Nb attitude systems';
strSummary{end+1} = 'Active attitude #';

%% Param�tres d'installation

strSummary{end+1} = 'OSV';
strSummary{end+1} = 'PSV';
strSummary{end+1} = 'SID';
strSummary{end+1} = 'Is Dual Head';

%% Heure d�but et fin

strSummary{end+1} = 'Time beginning';
strSummary{end+1} = 'Time end';
strSummary{end+1} = 'Duration';

%% Num�ros de pings KM d�but et fin

strSummary{end+1} = 'KM #Ping beginning';
strSummary{end+1} = 'KM #Ping end';

%% D�calages temporels 

strSummary{end+1} = 'PU-ZBA (ms)';
strSummary{end+1} = 'PU-GGA (ms)';
