% Description
%   getInfoSounder
%
% Syntax
%    [SounderName, ShipName] = ALL.getInfoSounder(nomFic)
%
% Input Arguments
%   nomFic : 
%
% Output Arguments
%   SounderName : 
%   ShipName    : 
%
% Examples
%    [SounderName, ShipName] = ALL.getInfoSounder(nomFic)
%
% Author : JMA
% See also Authors

function [SounderName, ShipName] = getInfoSounder(nomFic)

aKM = cl_simrad_all('nomFic', nomFic);

SounderName = get(aKM, 'EmModel');
[~, nomFic] = fileparts(nomFic);
ShipName = nomFic(22:end);
