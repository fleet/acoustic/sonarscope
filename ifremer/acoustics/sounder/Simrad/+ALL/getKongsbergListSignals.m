% Description
%   getKongsbergListSignals
%
% Syntax
%    ListSignals = ALL.getKongsbergListSignals
%
% Output Arguments
%   ListSignals : 
%
% Examples
%    ListSignals = ALL.getKongsbergListSignals
%
% Author : JMA
% See also Authors

function ListSignals = getKongsbergListSignals
 
ListSignals = {'Depth ref seafloor'
'Heading'
'Mode'
'EM2040Mode2'
'EM2040Mode3'
'InterPingsDistance'
'NbSwaths'
'PresenceWC'
'SurfaceSoundSpeed'
'BSN'
'BSO'
'BSN-BSO'
'PresenceRTK'
'Tide'
'AbsorptionCoeff'
'MeanQF'
'FM-Mode'
'SwathWidth'
'AngularCoverage'
'SwathWidth / Altitude'
'Speed'
'SamplingRate'
'Drift'
'PingCounter'
'AUV Immersion'
'AUV Altitude'
'MeanRoll'
'StdRoll'
'MeanPitch'
'StdPitch'
'MeanHeave'
'StdHeave'
'MeanTrueHeave'
'StdTrueHeave'
'Draught'
'TxBeamWidth'
'TVGCrossOver'
'NbRxBeams'
'MinPulseLength'
'CourseVessel'
'Time'
'Rn'
'TVGStart'
'TVGStop'
'EmModel'
'SystemSerialNumber'
'SectorTrackingOrRobustBottomDetection'
'MaxAngle'
'1/CurvatureRadius'
'y'
'TVGOffsetWC'
'TVGFunctionAppliedWC'};