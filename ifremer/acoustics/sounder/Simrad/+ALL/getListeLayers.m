function [str, Unit, Comments, strSsc] = getListeLayers

% Layers provenant des datagrams "depth"
str{1}  = 'Depth';
str{2}  = 'AcrossDist';
str{3}  = 'AlongDist';
str{4}  = 'BeamDepressionAngle';
str{5}  = 'BeamAzimuthAngle';
str{6}  = 'TwoWayTravelTimeInSamples';
str{7}  = 'QualityFactor';
str{8}  = 'LengthDetection';
str{9}  = 'Reflectivity';
str{10} = 'Detection';
str{11} = 'DetecAmplitudeNbSamples';
str{12} = 'DetecPhaseOrdrePoly';
str{13} = 'DetecPhaseVariance';

% Layers provenant des datagrams "Raw Range and Beam Angle"
str{14} = 'Raw-BeamPointingAngle';
str{15} = 'Raw-TxTiltAngle';
str{16} = 'Raw-Range';
str{17} = 'Raw-Reflectivity';

% Layers construits
str{18} = 'LongueurTrajet';
str{19} = 'Temps universel';
str{20} = 'Roll';
str{21} = 'Pitch';
str{22} = 'Heave';
str{23} = 'Heading';
str{24} = 'Depth+Draught-Heave';
str{25} = 'TwoWayTravelTimeInSeconds';
str{26} = 'TxBeamIndex';
str{27} = 'ReflectivityFromSnippets';
str{28} = 'TxBeamIndexSwath';
str{29} = 'Mask';

str{30} = 'IndexNumberBeam';
str{31} = 'InfoBeamSortDirection';
str{32} = 'InfoBeamNbSamples';
str{33} = 'InfoBeamCentralSample';
str{34} = 'Speed';
str{35} = 'IfremerQF';
str{36} = 'BeamAngles/Earth';
str{37} = 'SoundSpeed';
str{38} = 'Mode'; %GLT
str{39} = 'CentralFrequency';
str{40} = 'AbsorptionCoefficientRT';
str{41} = 'AbsorptionCoefficientSSc';
str{42} = 'AbsorptionCoefficientUser';
str{43} = 'IncidenceAngle';
str{44} = 'SlopeAcross';
str{45} = 'SlopeAlong';
str{46} = 'BathymetryFromDTM';
str{47} = 'InsonifiedAreaSSc';
str{48} = 'TxAngleKjell';
str{49} = 'PortStarboard';
str{50} = 'ReflectivityFromSnippetsWithoutSpecularRestablishment';
str{51} = 'ReflectivityBestBS';
str{52} = 'WCSignalWidth';
str{53} = 'SonarAlongBeamAngle';

strSsc{1}  = 'Bathymetry             (Depth)';
strSsc{2}  = 'AcrossDist             (Acrosstrack distance)';
strSsc{3}  = 'AlongDist              (Alongtrack distance)';
strSsc{4}  = 'TxAngle                (Beam depression angle)';
strSsc{5}  = 'BeamAzimuthAngle       (Beam azimuth angle)';
strSsc{6}  = 'TwoWayTravelTimeInSamples';
strSsc{7}  = 'KongsbergQualityFactor (Quality factor)';
strSsc{8}  = 'LengthDetection        (Length of detection window)';
strSsc{9}  = 'Reflectivity';
strSsc{10} = 'DetectionType';
strSsc{11} = 'DetecAmplitudeNbSamples';
strSsc{12} = 'DetecPhaseOrdrePoly';
strSsc{13} = 'DetecPhaseVariance';

% Layers provenant des datagrams "Raw Range and Beam Angle"
strSsc{14} = 'Raw-BeamPointingAngle';
strSsc{15} = 'Raw-TxTiltAngle';
strSsc{16} = 'Raw-Range';
strSsc{17} = 'Raw-Reflectivity';

% Layers construits
strSsc{18} = 'RayPathRange            (LongueurTrajet)';
strSsc{19} = 'RxTime                  (RxBeamTime)';
strSsc{20} = 'Roll                    (Roll at RxBeamTime)';
strSsc{21} = 'Pitch                   (Pitch at RxBeamTime)';
strSsc{22} = 'Heave                   (Heave at RxBeamTime)';
strSsc{23} = 'Heading                 (RoHeadingll at RxBeamTime)';
strSsc{24} = 'Bathymetry              (Depth+Draught-Heave)';
strSsc{25} = 'TwoWayTravelTimeInSeconds';
strSsc{26} = 'TxBeamIndex             (Sector number)';
strSsc{27} = 'ReflectivityFromSnippets';
strSsc{28} = 'TxBeamIndexSwath    (TxBeamIndex & PingSwath index)';
strSsc{29} = 'SonarScope Mask';

% Layers de la seabed image donn�s en PingBeam

strSsc{30} = 'IndexNumberBeam         (From Seabed Image)';
strSsc{31} = 'InfoBeamSortDirection   (From Seabed Image)';
strSsc{32} = 'InfoBeamNbSamples       (From Seabed Image)';
strSsc{33} = 'InfoBeamCentralSample   (From Seabed Image)';

% 
strSsc{34} = 'Speed';
strSsc{35} = 'Ifremer QF';
strSsc{36} = 'Beam Angles / Earth';
strSsc{37} = 'Mean Sound Speed in the water column';
strSsc{38} = 'Mode'; % GLT
strSsc{39} = 'Sector frequency';
strSsc{40} = 'Mean Absorption Coeff RT (used in Real Time)';
strSsc{41} = 'Mean Absorption Coeff SSc (computed from the sound speed profiles)';
strSsc{42} = 'Mean Absorption Coeff User (computed from survey processing)';
strSsc{43} = 'IncidenceAngle (computed from survey processing)';
strSsc{44} = 'SlopeAcross (computed from survey processing)';
strSsc{45} = 'SlopeAlong (computed from survey processing)';
strSsc{46} = 'BathymetryFromDTM (computed from survey processing)';
strSsc{47} = 'Insonified Area SSc (computed from the slopes)';
strSsc{48} = 'TxAngleKjell';
strSsc{49} = 'PortStarboard';
strSsc{50} = 'ReflectivityFromSnippetsWithoutSpecularRestablishment';
strSsc{51} = 'ReflectivityBestBS';
strSsc{52} = 'WCSignalWidth';
strSsc{52} = 'SonarAlongBeamAngle';

%% Unit�s

Unit{1} = 'm';
Unit{2} = 'm';
Unit{3} = 'm';
Unit{4} = 'deg';
Unit{5} = 'deg';
Unit{6} = 'samples';
Unit{7} = '';
Unit{8} = 'samples';
Unit{9} = 'dB';
Unit{10} = '';
Unit{11} = '';
Unit{12} = '';
Unit{13} = '';

Unit{14} = 'deg';
Unit{15} = 'deg';
Unit{16} = 'samples';
Unit{17} = 'dB';

Unit{18} = 'm';
Unit{19} = 'time';
Unit{20} = 'deg';
Unit{21} = 'deg';
Unit{22} = 'm';
Unit{23} = 'deg';
Unit{24} = 'm';
Unit{25} = 's';
Unit{26} = 'ms';
Unit{27} = 'dB';
Unit{28} = '';
Unit{29} = ' ';

Unit{30} = ' ';
Unit{31} = ' ';
Unit{32} = ' ';
Unit{33} = ' ';
Unit{34} = 'm/s';
Unit{35} = '10Log(z/dz)';
Unit{36} = 'deg';
Unit{37} = 'm/s';
Unit{38} = ''; %GLT
Unit{39} = 'kHz';
Unit{40} = 'dB/km';
Unit{41} = 'dB/km';
Unit{42} = 'dB/km';
Unit{43} = 'deg';
Unit{44} = 'deg';
Unit{45} = 'deg';
Unit{46} = 'm';
Unit{47} = 'dB';
Unit{48} = 'deg';
Unit{49} = '';
Unit{50} = 'dB';
Unit{51} = 'dB';
Unit{52} = 's';
Unit{53} = 'deg';

Comments{1}  = '"Depth" from Depth datagram';
Comments{2}  = '"Acrosstrack distance" from Depth datagram';
Comments{3}  = '"Alongtrack Distance" from Depth datagram';
Comments{4}  = '"Beam Depression Angle" from Depth datagram';
Comments{5}  = '"Beam Azimuth Angle" from Depth datagram';
Comments{6}  = '"Range (simple way travel in number of samples)" from Depth datagram';
Comments{7}  = '"Quality Factor" from Depth datagram';
Comments{8}  = '"Length of Detection window" from Depth datagram';
Comments{9}  = '"Reflectivity" from Depth datagram';
Comments{10} = 'Detection mode (1=by amplitude, 2=by phase) : coming from "Quality Factor"';
Comments{11} = 'If amplitude detection : Number of samples for gravity calculation : coming from "Quality Factor"';
Comments{12} = 'If Phase detection : Poly order to fit the zero phase range : coming from "Quality Factor"';
Comments{13} = 'If Phase detection : Normalized variance of the fit : coming from "Quality Factor"';

% Layers provenant des datagrams "Raw Range and Beam Angle"
Comments{14} = '"Beam Pointing Angle" from Raw range and beam angle datagram';
Comments{15} = '"Transmit Tilt Angle" from Raw range and beam angle datagram';
Comments{16} = '"Range (double way travel time)" from Raw range and beam angle datagram';
Comments{17} = '"Reflectivity" from Raw range and beam angle datagram';

% Layers construits
Comments{18} = 'LongueurTrajet : deduced from Range and Mode';
Comments{19} = 'Time of acoustic wave arrival to the sounder : deduced form Date, Time and Range';
Comments{20} = 'Roll at Time of impact between acoustic wave and seabed : deduced form Date, Time, Roll and Range';
Comments{21} = 'Pitch at Time of impact between acoustic wave and seabed : deduced form Date, Time, Pitch and Range';
Comments{22} = 'Heave at Time of impact between acoustic wave and seabed : deduced form Date, Time, Heave and Range';
Comments{23} = 'Heading at Time of impact between acoustic wave and seabed : deduced form Date, Time, Heading and Range';
Comments{24} = 'Depth minus Heave : deduced form Depth, Date, Time and Heave';
Comments{25} = 'Aller simple en ms';
Comments{26} = 'Transmission beam number';
Comments{27} = 'Reflecty from seabed image datagrams transformed in PingBeam';
Comments{28} = 'Tx beam Index + SwathNumber * NbTxBeams';
Comments{29} = 'Mask on beams';

Comments{30} = 'Beam number';
Comments{31} = 'Sorting direction';
Comments{32} = 'Nb of samples of the snippet';
Comments{33} = 'Index of the sample corresponding to the beam detection';
Comments{34} = 'Speed of the ship';
Comments{35} = 'Quality factor from Ifremer';
Comments{36} = 'Beam Angles / Earth';
Comments{37} = 'Mean sound speed in the water column';
Comments{38} = 'Mode of sounder'; %GLT
Comments{39} = 'Sector frequency';
Comments{40} = '"Mean absorption coeff" deduced from "seabed image" datagrams';
Comments{41} = '"Mean absorption coeff" deduced from the sound speed profiles datagrams';
Comments{42} = '"Mean absorption coeff" computed from Survey Processing';
Comments{43} = '"IncidenceAngle" computed from Survey Processing';
Comments{44} = '"SlopeAcross" computed from Survey Processing';
Comments{45} = '"SlopeAlong" computed from Survey Processing';
Comments{46} = '"BathymetryFromDTM" computed from Survey Processing';
Comments{47} = '"Insonified Area" computed with slopes'; % Attention, cette chaine de caract�res est test�e dans paramsFonctionSonarAireInsonifiee + ...
Comments{48} = 'Beam angles for DiagTx estimation';
Comments{49} = '1=Port side, 2=Starboard side';
Comments{50} = 'Same as ReflectivityFromSnippets but without s pecular re-establishment';
Comments{51} = 'ReflectivityBestBS';
Comments{52} = 'WC signal width in seconds';
Comments{53} = 'SonarAlongBeamAngle';

% str{end+1} = 'Mask';
% strSsc{end+1} = 'SonarScope Mask';
% Unit{end+1} = ' ';
% Comments{end+1} = 'Mask on beams';
