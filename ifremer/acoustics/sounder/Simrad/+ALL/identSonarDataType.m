% Description
%   identSonarDataType
%
% Syntax
%    [DataType, ColormapIndex, NomGenerique] = ALL.identSonarDataType(nomLayer)
%
% Input Arguments
%   nomLayer : 
%
% Output Arguments
%   DataType      : 
%   ColormapIndex : 
%   NomGenerique  : 
%
% Examples
%    [DataType, ColormapIndex, NomGenerique] = ALL.identSonarDataType('AcrossDist')
%
% Author : JMA
% See also Authors

function [DataType, ColormapIndex, NomGenerique] = identSonarDataType(nomLayer)

ColormapIndex = 3; % jet par defaut
NomGenerique  = 'MOS';

switch nomLayer
    case {'Depth'; 'Depth-Heave'; 'Depth+Draught-Heave'; 'Depth+Draught-Heave+Tide'; 'Depth / SeaLevel'; 'Depth / Antenna'}
        DataType = cl_image.indDataType('Bathymetry');
        NomGenerique = 'DTM';
        
    case 'AcrossDist'
        DataType = cl_image.indDataType('AcrossDist');
        
    case 'AlongDist'
        DataType = cl_image.indDataType('AlongDist');
        
    case 'BeamDepressionAngle'
%         DataType = cl_image.indDataType('TxAngle');
        DataType = cl_image.indDataType('BeamPointingAngle'); % Modif JMA le 02/01/2019 : attention, peut-�tre des effets de bord
        
    case 'TxAngleKjell'
        DataType = cl_image.indDataType('TxAngleKjell');
        
    case {'Raw-BeamPointingAngle'; 'BeamPointingAngle'}
        DataType = cl_image.indDataType('BeamPointingAngle');
        
    case 'BeamAzimuthAngle'
        DataType = cl_image.indDataType('BeamAzimuthAngle');
        
    case {'Range'; 'Raw-Range'}
        DataType = cl_image.indDataType('RayPathSampleNb');
        
    case 'LongueurTrajet'
        DataType = cl_image.indDataType('RayPathLength');  % One way length in meters
        
    case 'QualityFactor'
        DataType = cl_image.indDataType('KongsbergQualityFactor');
        
    case 'LengthDetection'
        DataType = cl_image.indDataType('KongsbergLengthDetection');
        
    case {'Reflectivity'; 'Raw-Reflectivity'; 'ReflectivityFromSnippets'; 'ReflectivityFromSnippetsWithoutSpecularRestablishment'; 'ReflectivityFromSnippets 100%'; 'ReflectivityBestBS'; 'Reflectivity1'; 'Reflectivity2'}
        DataType = cl_image.indDataType('Reflectivity');
        ColormapIndex = 2;
        
    case 'Detection'
        DataType = cl_image.indDataType('DetectionType');
        
    case 'DetecAmplitudeNbSamples'
        DataType = cl_image.indDataType('KongsbergAmplitudeNbSamples');
        
    case 'DetecPhaseOrdrePoly'
        DataType = cl_image.indDataType('KongsbergPhaseOrdrePoly');
        
    case 'DetecPhaseVariance'
        DataType = cl_image.indDataType('KongsbergPhaseVariance');
        
    case 'Temps universel'
        DataType = cl_image.indDataType('RxTime');
        NomGenerique = 'T';
        
    case {'Raw-TransmitTiltAngle'; 'Raw-TxTiltAngle'; 'TxTiltAngle'}
        DataType = cl_image.indDataType('TxTiltAngle');
        
    case 'RayPathTravelTime'
        DataType = cl_image.indDataType('RayPathTravelTime');
        
    case {'Masque', 'Mask'}
        DataType = cl_image.indDataType('Mask');
        
    case 'Proba'
        DataType = cl_image.indDataType('Probability');
        
    case {'EmissionBeam'; 'TransmitSectorNumber'; 'TxBeamIndex'}
        DataType = cl_image.indDataType('TxBeamIndex');
        
    case 'Roll'
        DataType = cl_image.indDataType('Roll');
        
    case 'Pitch'
        DataType = cl_image.indDataType('Pitch');
        
    case 'Heave'
        DataType = cl_image.indDataType('Heave');
        
    case 'Heading'
        DataType = cl_image.indDataType('Heading');
        
    case 'BeamIBA'
        DataType = cl_image.indDataType('KongsbergIBA');
        
    case 'RealTimeCleaningInfo'
        DataType = cl_image.indDataType('KongsbergRealTimeCleaningInfo');
        
    case 'DetectionInfo'
        DataType = cl_image.indDataType('KongsbergDetectionInfo');
        
    case 'TwoWayTravelTime'
        DataType = cl_image.indDataType('KongsbergTwoWayTravelTime');
        
    case {'TwoWayTravelTimeInSeconds'; 'TwoWayTTAmpDetect'; 'TwoWayTTPhaseDetect'}
        DataType = cl_image.indDataType('TwoWayTravelTimeInSeconds');
        
    case 'TwoWayTravelTimeInSamples'
        DataType = cl_image.indDataType('TwoWayTravelTimeInSamples');
        
    case {'IfremerQF'; 'AmpDetectQF'; 'PhaseDetectQF'}
        DataType = cl_image.indDataType('IfremerQF');
        
	case 'UncertaintyPerCent'
        DataType = cl_image.indDataType('UncertaintyPerCent');
        
	case 'UncertaintyMeters'
        DataType = cl_image.indDataType('UncertaintyMeters');
  
    case 'InfoBeamSortDirection'
        DataType = cl_image.indDataType('KongsbergSnippetSortDirection');
        
    case 'InfoBeamNbSamples'
        DataType = cl_image.indDataType('KongsbergSnippetNbSamples');
        
    case 'InfoBeamCentralSample'
        DataType = cl_image.indDataType('KongsbergSnippetCentralSample');
        
    case 'InfoBeamDetectionInfo'
        DataType = cl_image.indDataType('KongsbergSnippetDetectionInfo');
        
    case 'KongsbergSnippetRxBeamIndex'
        DataType = cl_image.indDataType('KongsbergSnippetRxBeamIndex');
                
    case 'RxBeamAngle'
        DataType = cl_image.indDataType('RxBeamAngle');
        
    case 'SoundSpeed'
        DataType = cl_image.indDataType('SoundVelocity');
        
    case 'Mode'
         DataType = cl_image.indDataType('Mode');
         
    case 'TxBeamIndexSwath'
         DataType = cl_image.indDataType('TxBeamIndexSwath');
         
    case {'MeanAbsorptionCoeff'; 'AbsorptionCoefficientRT'; 'AbsorptionCoefficientSSc'; 'AbsorptionCoefficientUser'}
         DataType = cl_image.indDataType('AbsorptionCoeff');
         
    case 'InsonifiedAreaSSc' % {'InsonifiedAreaRT'; 'InsonifiedAreaSSc'}
         DataType = cl_image.indDataType('InsonifiedAreadB');
         
    case 'WCSignalWidth'
         DataType = cl_image.indDataType('WCSignalWidth');
         
    case 'CentralFrequency'
         DataType = cl_image.indDataType('Frequency');
         
    case 'IncidenceAngle'
         NomGenerique = 'INC';
         DataType = cl_image.indDataType('IncidenceAngle');
         ColormapIndex = 17;    % jetSym par defaut
         
    case 'SlopeAcross'
         DataType = cl_image.indDataType('SlopeAcross');
         
    case 'SlopeAlong'
         DataType = cl_image.indDataType('SlopeAlong');
         
    case 'BathymetryFromDTM'
         DataType = cl_image.indDataType('Bathymetry');
         
    case 'BeamAngles/Earth'
        DataType = cl_image.indDataType('RxAngleEarth');
        
    case 'Speed'
        DataType = cl_image.indDataType('Speed');
        
    case 'PortStarboard'
        DataType = cl_image.indDataType('PortStarboard');

    case 'SonarAlongBeamAngle'
        DataType = cl_image.indDataType('SonarAlongBeamAngle');

    case 'detection_time_varying_gain'
        DataType = cl_image.indDataType('TVG');
        
    otherwise
%         message = sprintf('Le layer %s ne semble pas etre un layer de Simrad. Il est donc assimile comme "Other"', nomLayer);
%         my_warndlg(message, 0);
        DataType = 1;
end

