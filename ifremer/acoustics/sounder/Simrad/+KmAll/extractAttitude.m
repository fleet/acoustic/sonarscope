function [flag, XML, Data] = extractAttitude(a)

sDataKmAll = a.sDataKmAll;

%{
Variables ayant disparu dans les .kmall :
	SensorStatus
    Heave
	
Nouvelles variables :
    heading_rate: [1�1 struct]
    pitch_rate: [1�1 struct]
    roll_rate: [1�1 struct]
    Dim: [1�1 struct]
    vertical_offset: [1�1 struct]
    Vendor_specific: [1�1 struct]
%}
    
XML  = [];
Data = [];

%% Variables

%% Indices des samples correspondant au sensor utilis� en temps r�el

subSensorStatus = get_SKM_selectionSensorStatus(a, -1);

%% Time : OK

DataBin.Datetime = get_SKM_Datetime(a, 'sub', subSensorStatus); % Pas fini (ne rend l'heure que du premier sample de chaque datagramme)
DataBin.Time = cl_time('timeMat', datenum(DataBin.Datetime));

%% Heading : OK

DataBin.Heading = get_SKM_Heading_deg(a, 'sub', subSensorStatus);
FieldUnits.Heading = 'deg'; 
% figure; plot(DataBin.Heading, '.-b'); grid on; title('Heading');

%% Roll : OK

DataBin.Roll = get_SKM_Roll_deg(a, 'sub', subSensorStatus);
FieldUnits.Roll = 'deg';
% figure; plot(DataBin.Roll, '.-b'); grid on; title('Roll');

N = length(DataBin.Roll);

%% Pitch : OK

DataBin.Pitch = get_SKM_Pitch_deg(a, 'sub', subSensorStatus);
FieldUnits.Pitch = 'deg';
% figure; plot(DataBin.Pitch, '.-b'); grid on; title('Pitch');

%% Heave : valeurs diff�rentes

DataBin.Heave = get_SKM_Heave_m(a, 'sub', subSensorStatus);
DataBin.Heave = single(DataBin.Heave);
FieldUnits.Heave = 'm';
% figure; plot(DataBin.Heave, '.-b'); grid on; title('Heave');

%% vertical_offset

% vertical_offset % TODO GLU : Nouvelle variable diff�rente de Heave du .all
%{
        DataBin.Vertical_offset = Hdf5Utils.read_value(sDataKmAll.Platform.Attitude.Sensor000.vertical_offset);
DataBin.Vertical_offset = single(DataBin.Vertical_offset);
% Unit = Hdf5Utils.read_unit(sDataKmAll.Platform.Attitude.Sensor000.vertical_offset)
FieldUnits.Vertical_offset = 'm';
% figure; plot(DataAttitude.Heave, 'k'); grid on; hold on; plot(DataBin.Heave, '.r'); title('Heave');
%}

%% SensorStatus : Valeurs diff�rentes

DataBin.SensorStatus = get_SKM_SensorStatus(a, 'sub', subSensorStatus);
DataBin.SensorStatus = uint16(DataBin.SensorStatus);
FieldUnits.SensorStatus = '';
% figure; plot(DataBin.SensorStatus, '.-b'); grid on; title('SensorStatus');

%% SensorSystemDescriptor

DataBin.SensorSystemDescriptor = ones(N,1, 'uint8'); % TODO : lire les donn�es de toutes les centrales et cr�er l'index de la centrale active
FieldUnits.SensorSystemDescriptor = '';

%% Variable pr�d�finies par SonarScope (non pr�sentes dans les fichiers KM)

DataBin.Tide      = zeros([N,1], 'single');
DataBin.Draught   = zeros([N,1], 'single');
DataBin.TrueHeave = zeros([N,1], 'single');

FieldUnits.Tide      = 'm';
FieldUnits.Draught   = 'm';
FieldUnits.TrueHeave = 'm';

%% Cr�ation du XML

Fields = fields(DataBin);

XML.Dimensions.NbSamples = N;

XML.Title         = 'Attitude';
XML.Constructor   = 'Kongsberg';
XML.EmModel       = double(sDataKmAll.dgmSKM(1).Header.echoSounderId);
XML.TimeOrigin    = '01/01/-4713';
XML.Comments      = 'Sensor sampling rate'; % ne rien mettre apr�s premoers tests
XML.FormatVersion = 20101118;

XML.SystemSerialNumber = double(sDataKmAll.dgmSKM(1).Header.systemId);

Fields = Fields(~contains(Fields, 'Datetime')); 
for k=1:length(Fields)
    XML.Signals(k).Dimensions.Name      = 'NbSamples';
    XML.Signals(k).Dimensions.Length    = N;
    XML.Signals(k).Dimensions.Unlimited = 0;
    
    Values  = DataBin.(Fields{k});
    Storage = class(Values);
    switch Storage
        case 'cl_time'
            Storage = 'double';
            Unit    = 'days since JC';
            Values  = Values.timeMat;
        otherwise
            Unit = FieldUnits.(Fields{k});
    end
    
    XML.Signals(k).Name      = Fields{k};
    XML.Signals(k).Storage   = Storage;
    XML.Signals(k).Unit      = Unit;
    XML.Signals(k).Populated = 1;
    XML.Signals(k).Stats     = stats(Values);
    
    %{
    try
        for k2=1:length(DataAttitude.Signals)
            if strcmp(XML.Signals(k2).Name, DataAttitude.Signals(k2).Name)
                break
            end
        end
        fprintf('XML.Signals(%d).Unit = ''%s'';\n', k2, DataAttitude.Signals(k2).Unit);
    catch
        XML.Signals(k).Name;
    end
    %}
end

%{
XML.Signals(3).Unit = '';
%}

FieldsData = fields(DataBin);
FieldsXML  = fields(XML);
for k=1:length(FieldsXML)
    Data.(FieldsXML{k}) = XML.(FieldsXML{k});
end

for k=1:length(FieldsData)
    Data.(FieldsData{k}) = DataBin.(FieldsData{k});
end

flag = 1;

% DataAttitude
% Data
