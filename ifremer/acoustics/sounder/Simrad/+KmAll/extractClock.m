function [flag, XML, Data] = extractClock(a)

%{
Variables ayant disparu dans les .kmall :
	xxx
	
Nouvelles variables :
    xxxx
%}

%{
DataClock = 
                 Title: 'Clock'
           Constructor: 'Kongsberg'
               EmModel: 304
    SystemSerialNumber: 160
            TimeOrigin: '01/01/-4713'
              Comments: 'Sounder ping rate'
         FormatVersion: 20101118
            Dimensions: [1�1 struct]
               Signals: [1�4 struct]
                  Time: [1�1 cl_time]
              Datetime: [528�1 datetime]
           PingCounter: [528�1 uint16]
          ExternalTime: [1�1 cl_time]
      ExternalDatetime: [528�1 datetime]
                   PPS: [528�1 uint8]
%}
    
XML  = [];
Data = [];

%% Variables

%% Time : OK

DataBin.Datetime = get_SCL_Datetime(a);
DataBin.Time = cl_time('timeMat', datenum(DataBin.Datetime));

N = length(DataBin.Time);

%% Offset : OK

Offset= get_SCL_Duration(a);
DataBin.ExternalDatetime = DataBin.Datetime + Offset;
DataBin.ExternalIime = cl_time('timeMat', datenum(DataBin.ExternalDatetime));

%% PingCounter : OK

DataBin.PingCounter =( 0:(N-1))';
FieldUnits.PingCounter = '';

%% PPS : OK

DataBin.PPS = get_SCL_sensorStatus(a);
FieldUnits.PPS = '';

%% Cr�ation du XML

Fields = fields(DataBin);

XML.Dimensions.NbSamples = N;

XML.Title         = 'Clock';
XML.Constructor   = 'Kongsberg';
XML.EmModel       = double(a.sDataKmAll.dgmSCL(1).Header.echoSounderId);
XML.TimeOrigin    = '01/01/-4713';
XML.Comments      = 'Sensor sampling rate'; % ne rien mettre apr�s premoers tests
XML.FormatVersion = 20101118;

XML.SystemSerialNumber = double(a.sDataKmAll.dgmSCL(1).Header.systemId);

Fields = Fields(~contains(Fields, 'Datetime')); 
for k=1:length(Fields)
    XML.Signals(k).Dimensions.Name      = 'NbSamples';
    XML.Signals(k).Dimensions.Length    = N;
    XML.Signals(k).Dimensions.Unlimited = 0;
    
    Values  = DataBin.(Fields{k});
    Storage = class(Values);
    switch Storage
        case 'cl_time'
            Storage = 'double';
            Unit    = 'days since JC';
            Values  = Values.timeMat;
        otherwise
            Unit = FieldUnits.(Fields{k});
    end
    
    XML.Signals(k).Name      = Fields{k};
    XML.Signals(k).Storage   = Storage;
    XML.Signals(k).Unit      = Unit;
    XML.Signals(k).Populated = 1;
    XML.Signals(k).Stats     = stats(Values);
    
    %{
    try
        for k2=1:length(DataClock.Signals)
            if strcmp(XML.Signals(k2).Name, DataClock.Signals(k2).Name)
                break
            end
        end
        fprintf('XML.Signals(%d).Unit = ''%s'';\n', k2, DataClock.Signals(k2).Unit);
    catch
        XML.Signals(k).Name;
    end
    %}
end

%{
XML.Signals(3).Unit = '';
%}

FieldsData = fields(DataBin);
FieldsXML  = fields(XML);
for k=1:length(FieldsXML)
    Data.(FieldsXML{k}) = XML.(FieldsXML{k});
end

for k=1:length(FieldsData)
    Data.(FieldsData{k}) = DataBin.(FieldsData{k});
end

flag = 1;

% DataClock
% Data
