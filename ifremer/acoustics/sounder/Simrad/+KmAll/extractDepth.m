function [flag, XML, Data] = extractDepth(this, DataAttitude, DataSeabedXSF, DataRawXML)

%{
Variables ayant disparu dans les .kmall :
    xxx

Nouvelles variables :
    xxx
%}


%{
Title: 'Depth'
Constructor: 'Kongsberg'
EmModel: 304
                                  ListeSystemSerialNumber: 160
TimeOrigin: '01/01/-4713'
Comments: 'Sounder ping rate'
Version: 'V2'
FormatVersion: 20101118
IdentAlgoSnippets2OneValuePerBeam: 2
                                               Dimensions: [1�1 struct]
                                                  Signals: [1�12 struct]
                                                   Images: [1�23 struct]
												  
Time: [1�1 cl_time]
Datetime: [84�1 datetime]
PingCounter: [84�1 single]
SystemSerialNumber: [84�1 single]
Heading: [84�1 single]
SoundSpeed: [84�1 single]
TransducerDepth: [84�1 single]
MaxNbBeamsPossible: [84�1 single]
NbBeams: [84�1 single]
SamplingRate: [84�1 single]
ScanningInfo: [84�1 single]
MaskPingsBathy: [84�1 single]
MaskPingsReflectivity: [84�1 single]
									
Depth: [84�800 single]
AcrossDist: [84�800 single]
AlongDist: [84�800 single]
                                          LengthDetection: [84�800 single]
                                            QualityFactor: [84�800 single]
BeamIBA: [84�800 single]
DetectionInfo: [84�800 single]
                                     RealTimeCleaningInfo: [84�800 single]
Reflectivity: [84�800 single]
                                 ReflectivityFromSnippets: [84�800 single]
    ReflectivityFromSnippetsWithoutSpecularRestablishment: [84�800 single]
                                  AbsorptionCoefficientRT: [84�800 single]
AbsorptionCoefficientSSc: [84�800 single]
AbsorptionCoefficientUser: [84�800 single]
IncidenceAngle: [84�800 single]
ReflectivityBestBS: [84�800 single]
SlopeAcross: [84�800 single]
SlopeAlong: [84�800 single]
BathymetryFromDTM: [84�800 single]
InsonifiedAreaSSc: [84�800 single]
WCSignalWidth: [84�800 single]
Mask: [84�800 uint8]
                                               MaskBackup: [84�800 uint8]	
%}
    
XML  = [];
Data = [];

%% Time et Datetime : OK

Signals.Datetime = get_MRZ_Datetime(this);
Signals.Time = cl_time('timeMat', datenum(Signals.Datetime));

N = length(Signals.Datetime);

%% PingCounter : OK

Signals.PingCounter = get_MRZ_PingCnt(this);
FieldUnits.PingCounter = '';
% figure; plot(Signals.PingCounter); grid on;

%% SystemSerialNumber % TODO GLU

EmModel = double(this.sDataKmAll.dgmMRZ(1).Header.echoSounderId);
Signals.SystemSerialNumber = repmat(single(EmModel), N, 1); % TODO : trouver o� r�cup�rer cette info !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
FieldUnits.SystemSerialNumber = '';

%% Heading : OK

Signals.Heading = get_MRZ_HeadingVessel_deg(this);
FieldUnits.Heading = 'deg';
% figure; plot(Signals.Heading); grid on; 

%% SoundSpeed : OK

Signals.SoundSpeed = get_MRZ_SoundSpeedAtTxDepth_mPerSec(this);
FieldUnits.SoundSpeed = 'm/s';
% figure; plot(Signals.SoundSpeed); grid on;

%% TransducerDepth : OK

Signals.TransducerDepth = get_MRZ_TxTransducerDepth_m(this);
FieldUnits.TransducerDepth = 'm';
% my_figure; my_plot(Signals.TransducerDepth); grid on;

%% MaxNbBeamsPossible : OK

Signals.MaxNbBeamsPossible = get_MRZ_NumSoundingsMaxMain(this);
Signals.MaxNbBeamsPossible = single(Signals.MaxNbBeamsPossible);
FieldUnits.MaxNbBeamsPossible = '';
% figure; plot(Signals.MaxNbBeamsPossible); grid on;

%% NbBeams : OK

Signals.NbBeams = get_MRZ_NumSoundingsValidMain(this);
Signals.NbBeams = single(Signals.NbBeams);
FieldUnits.NbBeams = '';
% figure; plot(Signals.NbBeams); grid on;

%% SamplingRate : OK

Signals.SamplingRate = get_MRZ_SeabedImageSampleRate(this);
FieldUnits.SamplingRate = 'Hz';
% figure; plot(Signals.SamplingRate); grid on;

%% ScanningInfo % TODO GLU

% DataDepth.ScanningInfo
%{
Signals.ScanningInfo = Hdf5Utils.read_value(Hdf5Data.Sonar.Beam_group1.Vendor_specific.tx_xxxxxxx);
%}
Signals.ScanningInfo = zeros(N, 1, 'single');
FieldUnits.ScanningInfo = '';
% figure; plot(DataDepth.ScanningInfo, 'k'); grid on; hold on; plot(Signals.ScanningInfo, '.r');

%% MaskPingsBathy

Signals.MaskPingsBathy = zeros(N, 1, 'single');
FieldUnits.MaskPingsBathy = '';
% figure; plot(DataDepth.MaskPingsBathy, 'k'); grid on; hold on; plot(Signals.MaskPingsBathy, '.r');

%% MaskPingsReflectivity

Signals.MaskPingsReflectivity = zeros(N, 1, 'single');
FieldUnits.MaskPingsReflectivity = '';
% figure; plot(DataDepth.MaskPingsReflectivity, 'k'); grid on; hold on; plot(Signals.MaskPingsReflectivity, '.r');

%% Depth : Diff�rence de 0.904 m � 0.917 m sur fichier D:\XSF\FromKmall\0039_20180905_222154_raw.xsf.nc

Images.Depth = get_MRZ_Depth(this);

HeaveTx = interp1(DataAttitude.Datetime, DataAttitude.Heave, Signals.Datetime); % Ajout JMA le 24/07/2021
Images.Depth = Images.Depth + Signals.TransducerDepth;                          % Ajout JMA le 24/07/2021
Images.Depth = Images.Depth - HeaveTx;                                          % Ajout JMA le 24/07/2021

FieldUnits.Depth = 'm';
% figure; imagesc(Images.Depth); colormap(jet(256)); colorbar; title('Images.Depth')

nbBeams = size(Images.Depth, 2);

%% AcrossDist : OK

Images.AcrossDist = get_MRZ_AcrossDist(this);
FieldUnits.AcrossDist = 'm';
% figure; imagesc(Images.AcrossDist); colormap(jet(256)); colorbar; title('Images.AcrossDist')

%% AlongDist : OK

Images.AlongDist = get_MRZ_AlongDist(this);
FieldUnits.AlongDist = 'm';
% figure; imagesc(Images.AlongDist); colormap(jet(256)); colorbar; title('Images.AlongDist')

%% LengthDetection : TODO GLU : je ne retrouve pas cette info alors que Sonar Record Viewer la mentionne comme "Number samples:"

Images.LengthDetection = get_MRZ_DetectionWindowLength_sec(this);
FieldUnits.LengthDetection = '';
% figure; imagesc(Images.LengthDetection); colormap(jet(256)); colorbar; title('Images.LengthDetection')

%% QualityFactor % TODO GLU

Images.QualityFactor = get_MRZ_QualityFactor(this);
FieldUnits.QualityFactor = '';
% figure; imagesc(Images.QualityFactor); colormap(jet(256)); colorbar; title('Images.QualityFactor')

%% BeamIBA : OK mais valeurs toutes � 0

Images.BeamIBA = get_MRZ_BeamIBA(this);
FieldUnits.BeamIBA = 'deg';
% figure; imagesc(Images.BeamIBA); colormap(jet(256)); colorbar; title('Images.BeamIBA')

%% DetectionInfo : TODO factoriser ce code avec extractRawRangeBeamAngle

Images.DetectionInfo = get_MRZ_DetectionType(this);
% Images.DetectionInfo = get_MRZ_DetectionMode(this);
Images.DetectionInfo = single(Images.DetectionInfo);
FieldUnits.DetectionInfo = '';

% sub = (Images.DetectionInfo == 0);
% Images.DetectionInfo(sub)  = 16;
% Images.DetectionInfo(~sub) = 17;

% figure; imagesc(Images.DetectionInfo); colormap(jet(256)); colorbar; title('Images.DetectionInfo')

%% RealTimeCleaningInfo : TODO : quelle est la correspondance ?

Images.RealTimeCleaningInfo = zeros(size(Images.Depth), 'single');
FieldUnits.RealTimeCleaningInfo = '';
% figure; imagesc(Images.RealTimeCleaningInfo); colormap(jet(256)); colorbar; title('Images.RealTimeCleaningInfo')

%% Reflectivity : OK

% Images.Reflectivity = get_MRZ_Reflectivity1_dB(this);
Images.Reflectivity = get_MRZ_Reflectivity2_dB(this);
FieldUnits.Reflectivity = 'dB';
% figure; imagesc(Images.Reflectivity); colormap(gray(256)); colorbar; title('Images.Reflectivity')

%% ReflectivityFromSnippets % TODO JMA OK mais diff�rence due � correction sp�culaire dans SSc

% TODO : voir computeReflectivityByBeam

%{
X = Hdf5Utils.read_value(Hdf5Data.Sonar.Beam_group1.Bathymetry.seabed_image_samples_r);
Images.ReflectivityFromSnippets  = arrayfun(@(x) reflec_Amp2dB(mean(reflec_dB2Amp(x{:}))), X);
Images.ReflectivityFromSnippets = single(Images.ReflectivityFromSnippets');
%}
Images.ReflectivityFromSnippets = get_MRZ_Reflectivity2_dB(this);

% DataSeabedXSF.BSN
% DataSeabedXSF.BSO
% DataSeabedXSF.TVGN

deltaBS = DataSeabedXSF.BSN - DataSeabedXSF.BSO;
Rn = DataSeabedXSF.TVGN;
TVGCrossOver = DataSeabedXSF.TVGCrossOver;
Range = DataRawXML.TwoWayTravelTime;

% pppp = Images.ReflectivityFromSnippets;

for k=1:size(Images.ReflectivityFromSnippets, 1)
%     Images.ReflectivityFromSnippets(k,:) = CorSpec(Images.ReflectivityFromSnippets(k,:), deltaBS(k,:), Range(k,:), Rn(k,:), TVGCrossOver(k,:), false);
    Images.ReflectivityFromSnippets(k,:) = CorSpec(Images.ReflectivityFromSnippets(k,:), deltaBS(k,:), Range(k,:), Rn(k,:), TVGCrossOver(k,:), true); % Modif JMA le 25/07/2021 car Rn pas bien estim� dans extractSeabedImage
end

FieldUnits.ReflectivityFromSnippets = 'dB';
% figure; imagesc(Images.ReflectivityFromSnippets); colormap(gray(256)); colorbar; title('Images.Reflectivity')

%% ReflectivityFromSnippetsWithoutSpecularRestablishment : TODO JMA

Images.ReflectivityFromSnippetsWithoutSpecularRestablishment = Images.Reflectivity;
FieldUnits.ReflectivityFromSnippetsWithoutSpecularRestablishment = 'dB';
% figure; imagesc(DataDepth.ReflectivityFromSnippetsWithoutSpecularRestablishment); colormap(gray(256)); colorbar; title('DataDepth.ReflectivityFromSnippetsWithoutSpecularRestablishment')
% figure; imagesc(Images.ReflectivityFromSnippetsWithoutSpecularRestablishment); colormap(gray(256)); colorbar; title('Images.ReflectivityFromSnippetsWithoutSpecularRestablishment')

%% AbsorptionCoefficientRT % TODO JMA 

Images.AbsorptionCoefficientRT = zeros(size(Images.Depth), 'single');
FieldUnits.AbsorptionCoefficientRT = 'dB/km';
% figure; imagesc(Images.AbsorptionCoefficientRT); colormap(gray(256)); colorbar; title('Images.AbsorptionCoefficientRT')

%% AbsorptionCoefficientSSc % TODO JMA 

Images.AbsorptionCoefficientSSc = zeros(size(Images.Depth), 'single');
FieldUnits.AbsorptionCoefficientSSc = 'dB/km';
% figure; imagesc(Images.AbsorptionCoefficientSSc); colormap(gray(256)); colorbar; title('Images.AbsorptionCoefficientSSc')

%% AbsorptionCoefficientUser % TODO JMA 

Images.AbsorptionCoefficientUser = zeros(size(Images.Depth), 'single');
FieldUnits.AbsorptionCoefficientUser = 'dB/km';
% figure; imagesc(Images.AbsorptionCoefficientUser); colormap(gray(256)); colorbar; title('Images.AbsorptionCoefficientUser')

%% IncidenceAngle % TODO JMA 

Images.IncidenceAngle = zeros(size(Images.Depth), 'single');
FieldUnits.IncidenceAngle = 'deg';
% figure; imagesc(Images.IncidenceAngle); colormap(gray(256)); colorbar; title('Images.IncidenceAngle')

%% ReflectivityBestBS % TODO JMA 

Images.ReflectivityBestBS = zeros(size(Images.Depth), 'single');
FieldUnits.ReflectivityBestBS = 'dB';
% figure; imagesc(DataDepth.ReflectivityBestBS); colormap(gray(256)); colorbar; title('DataDepth.ReflectivityBestBS')

%% SlopeAcross % TODO JMA 

Images.SlopeAcross = zeros(size(Images.Depth), 'single');
FieldUnits.SlopeAcross = 'deg';
% figure; imagesc(DataDepth.SlopeAcross); colormap(gray(256)); colorbar; title('DataDepth.SlopeAcross')

%% SlopeAlong % TODO JMA 

Images.SlopeAlong = zeros(size(Images.Depth), 'single');
FieldUnits.SlopeAlong = 'deg';
% figure; imagesc(DataDepth.SlopeAlong); colormap(gray(256)); colorbar; title('DataDepth.SlopeAlong')

%% BathymetryFromDTM % TODO JMA 

Images.BathymetryFromDTM = zeros(size(Images.Depth), 'single');
FieldUnits.BathymetryFromDTM = 'm';
% figure; imagesc(DataDepth.BathymetryFromDTM); colormap(gray(256)); colorbar; title('DataDepth.BathymetryFromDTM')

%% InsonifiedAreaSSc % TODO JMA 

Images.InsonifiedAreaSSc = zeros(size(Images.Depth), 'single');
FieldUnits.InsonifiedAreaSSc = 'db/m2';
% figure; imagesc(DataDepth.InsonifiedAreaSSc); colormap(gray(256)); colorbar; title('DataDepth.InsonifiedAreaSSc')

%% WCSignalWidth

Images.WCSignalWidth = zeros(size(Images.Depth), 'single');
FieldUnits.WCSignalWidth = 'Hz'; % TODO : Hz ou kHz ?
% figure; imagesc(DataDepth.WCSignalWidth); colormap(gray(256)); colorbar; title('DataDepth.WCSignalWidth')

%% Mask

Images.Mask = zeros(size(Images.Depth), 'single');
FieldUnits.Mask = '';
% figure; imagesc(DataDepth.Mask); colormap(gray(256)); colorbar; title('DataDepth.Mask')

%% MaskBackup % TODO JMA : bizarre les valeurs de DataDepth.MaskBackup  !!!

Images.MaskBackup = zeros(size(Images.Depth), 'single');
FieldUnits.MaskBackup = '';
% figure; imagesc(DataDepth.MaskBackup); colormap(gray(256)); colorbar; title('DataDepth.MaskBackup')

%% Cr�ation du XML

XML.Title         = 'Depth';
XML.Constructor   = 'Kongsberg';
XML.EmModel       = double(this.sDataKmAll.dgmMRZ(1).Header.echoSounderId);
XML.TimeOrigin    = '01/01/-4713';
XML.Comments      = 'Sounder ping rate';
XML.Version       = 'V2';
XML.FormatVersion = 20101118;
XML.IdentAlgoSnippets2OneValuePerBeam = 2;

XML.Dimensions.nbPings    = N;
XML.Dimensions.nbBeams    = nbBeams;
XML.Dimensions.nbSounders = 1;

% Dans datagramme Installation SN=10001 ?
XML.ListeSystemSerialNumber = double(this.sDataKmAll.dgmMRZ(1).Header.systemId);

Fields = fields(Signals);
Fields = Fields(~contains(Fields, 'Datetime')); 
for k=1:length(Fields)
    XML.Signals(k).Dimensions.Name      = 'nbPings';
    XML.Signals(k).Dimensions.Length    = N;
    XML.Signals(k).Dimensions.Unlimited = 0;
    
    Values  = Signals.(Fields{k});
    Storage = class(Values);
    switch Storage
        case 'cl_time'
            Storage = 'double';
            Unit    = 'days since JC';
            Values  = Values.timeMat;
        otherwise
            Unit = FieldUnits.(Fields{k});
    end
    
    XML.Signals(k).Name      = Fields{k};
    XML.Signals(k).Storage   = Storage;
    XML.Signals(k).Unit      = Unit;
    XML.Signals(k).Populated = 1;
    XML.Signals(k).Stats     = stats(Values);
end


Fields = fields(Images);
for k=1:length(Fields)
    XML.Images(k).Dimensions.Name      = 'nbPings';
    XML.Images(k).Dimensions.Length    = N;
    XML.Images(k).Dimensions.Unlimited = 0;
    
    XML.Images(k).Dimensions(2).Name      = 'nbBeams';
    XML.Images(k).Dimensions(2).Length    = nbBeams;
    XML.Images(k).Dimensions(2).Unlimited = 0;
    
    Values  = Images.(Fields{k});
    Storage = class(Values);
    switch Storage
        case 'cl_time'
            Storage = 'double';
            Unit    = 'days since JC';
            Values  = Values.timeMat;
        case 'datetime'
            continue
        otherwise
            Unit = FieldUnits.(Fields{k});
    end
    
    XML.Images(k).Name      = Fields{k};
    XML.Images(k).Storage   = Storage;
    XML.Images(k).Unit      = Unit;
    XML.Images(k).Populated = 1;
    XML.Images(k).Stats     = stats(Values);
    
    %{
    try
        for k2=1:length(DataPosition.Images)
            if strcmp(XML.Images(k2).Name, DataPosition.Images(k2).Name)
                break
            end
        end
        fprintf('XML.Images(%d).Unit = ''%s'';\n', k2, DataPosition.Images(k2).Unit);
    catch
        XML.Images(k).Name;
    end
    %}
end

FieldsXML  = fields(XML);
for k=1:length(FieldsXML)
    Data.(FieldsXML{k}) = XML.(FieldsXML{k});
end

FieldsData = fields(Signals);
for k=1:length(FieldsData)
    Data.(FieldsData{k}) = Signals.(FieldsData{k});
end

FieldsData = fields(Images);
for k=1:length(FieldsData)
    Data.(FieldsData{k}) = Images.(FieldsData{k});
end

% DataPosition
% Data

flag = 1;
