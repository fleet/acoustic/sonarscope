function [flag, DataInstallParams] = extractInstallParameters(a)

sDataKmAll = a.sDataKmAll;

%% Time et Datetime : OK

DataInstallParams.Datetime = get_IIP_Datetime(a); % Pas fini (ne rend l'heure que du premier sample de chaque datagramme)
DataInstallParams.Time = cl_time('timeMat', datenum(DataInstallParams.Datetime));

%% Section installationParameters

str = get_IIP_Install_txt(a);
mots = strsplit(str, ',');

%% TRAI_TX1

k = contains(mots, 'TRAI_TX1');
TRAI_TX1 = mots{k};
strParams = strsplit(TRAI_TX1, {';'; ':'});
mots(k) = [];

ind = find(contains(strParams, 'IPX='));
if ~isempty(ind)
    strParams(ind) = [];
    % DataInstallParams.??? = str2double(strParams{ind}(3:end));
end

ind = find(contains(strParams, 'IPY='));
if ~isempty(ind)
    strParams(ind) = [];
    % DataInstallParams.??? = str2double(strParams{ind}(3:end));
end

ind = find(contains(strParams, 'IPZ='));
if ~isempty(ind)
    strParams(ind) = [];
    % DataInstallParams.??? = str2double(strParams{ind}(3:end));
end

ind = find(contains(strParams, 'ICX='));
if ~isempty(ind)
    strParams(ind) = [];
    % DataInstallParams.??? = str2double(strParams{ind}(3:end));
end

ind = find(contains(strParams, 'ICY='));
if ~isempty(ind)
    strParams(ind) = [];
    % DataInstallParams.??? = str2double(strParams{ind}(3:end));
end

ind = find(contains(strParams, 'ICZ='));
if ~isempty(ind)
    strParams(ind) = [];
    % DataInstallParams.??? = str2double(strParams{ind}(3:end));
end

ind = find(contains(strParams, 'ISX='));
if ~isempty(ind)
    strParams(ind) = [];
    % DataInstallParams.??? = str2double(strParams{ind}(3:end));
end

ind = find(contains(strParams, 'ISY='));
if ~isempty(ind)
    strParams(ind) = [];
    % DataInstallParams.??? = str2double(strParams{ind}(3:end));
end

ind = find(contains(strParams, 'ISZ='));
if ~isempty(ind)
    strParams(ind) = [];
    % DataInstallParams.??? = str2double(strParams{ind}(3:end));
end

ind = contains(strParams, 'N=');
DataInstallParams.S1N = str2double(strParams{ind}(3:end));

ind = contains(strParams, 'X=');
DataInstallParams.S1X = str2double(strParams{ind}(3:end));

ind = contains(strParams, 'Y=');
DataInstallParams.S1Y = str2double(strParams{ind}(3:end));

ind = contains(strParams, 'Z=');
DataInstallParams.S1Z = str2double(strParams{ind}(3:end));

ind = contains(strParams, 'R=');
DataInstallParams.S1R = str2double(strParams{ind}(3:end));

ind = contains(strParams, 'P=');
DataInstallParams.S1P = str2double(strParams{ind}(3:end));

ind = contains(strParams, 'H=');
DataInstallParams.S1H = str2double(strParams{ind}(3:end));

ind = contains(strParams, 'S=');
X = str2double(strParams{ind}(3:end));
DataInstallParams.S1S = find(X == [0.5 1 2]); % (0=0.5º,1=1º,2=2º)

%% TRAI_RX1

k = contains(mots, 'TRAI_RX1');
TRAI_RX1 = mots{k};
strParams = strsplit(TRAI_RX1, {';'; ':'});
mots(k) = [];

ind = find(contains(strParams, 'IX='));
if ~isempty(ind)
    strParams(ind) = [];
    % DataInstallParams.??? = str2double(strParams{ind}(3:end));
end

ind = find(contains(strParams, 'IY='));
if ~isempty(ind)
    strParams(ind) = [];
    % DataInstallParams.??? = str2double(strParams{ind}(3:end));
end

ind = find(contains(strParams, 'IZ='));
if ~isempty(ind)
    strParams(ind) = [];
    % DataInstallParams.??? = str2double(strParams{ind}(3:end));
end

ind = contains(strParams, 'N=');
DataInstallParams.S2N = str2double(strParams{ind}(3:end));

ind = contains(strParams, 'X=');
DataInstallParams.S2X = str2double(strParams{ind}(3:end));

ind = contains(strParams, 'Y=');
DataInstallParams.S2Y = str2double(strParams{ind}(3:end));

ind = contains(strParams, 'Z=');
DataInstallParams.S2Z = str2double(strParams{ind}(3:end));

ind = contains(strParams, 'R=');
DataInstallParams.S2R = str2double(strParams{ind}(3:end));

ind = contains(strParams, 'P=');
DataInstallParams.S2P = str2double(strParams{ind}(3:end));

ind = contains(strParams, 'H=');
DataInstallParams.S2H = str2double(strParams{ind}(3:end));

ind = contains(strParams, 'G=');
DataInstallParams.GO1 = str2double(strParams{ind}(3:end));

ind = find(contains(strParams, 'S='));
if ~isempty(ind)
    DataInstallParams.S2S = str2double(strParams{ind}(3:end)); % Différent de .all et SonarRecord Viewer
end
% X = str2double(strParams{ind}(3:end));
% DataInstallParams.S2S = find(X == [0.5 1 2]); % (0=0.5º,1=1º,2=2º)

%% TRAI_RX2

k = contains(mots, 'TRAI_RX2');
if any(k)
    TRAI_RX2 = mots{k};
    strParams = strsplit(TRAI_RX2, {';'; ':'});
    mots(k) = [];

    ind = find(contains(strParams, 'IX='));
    if ~isempty(ind)
        strParams(ind) = [];
        % DataInstallParams.??? = str2double(strParams{ind}(3:end));
    end

    ind = find(contains(strParams, 'IY='));
    if ~isempty(ind)
        strParams(ind) = [];
        % DataInstallParams.??? = str2double(strParams{ind}(3:end));
    end

    ind = find(contains(strParams, 'IZ='));
    if ~isempty(ind)
        strParams(ind) = [];
        % DataInstallParams.??? = str2double(strParams{ind}(3:end));
    end

    ind = contains(strParams, 'N=');
    DataInstallParams.S3N = str2double(strParams{ind}(3:end));

    ind = contains(strParams, 'X=');
    DataInstallParams.S3X = str2double(strParams{ind}(3:end));

    ind = contains(strParams, 'Y=');
    DataInstallParams.S3Y = str2double(strParams{ind}(3:end));

    ind = contains(strParams, 'Z=');
    DataInstallParams.S3Z = str2double(strParams{ind}(3:end));

    ind = contains(strParams, 'R=');
    DataInstallParams.S3R = str2double(strParams{ind}(3:end));

    ind = contains(strParams, 'P=');
    DataInstallParams.S3P = str2double(strParams{ind}(3:end));

    ind = contains(strParams, 'H=');
    DataInstallParams.S3H = str2double(strParams{ind}(3:end));

    ind = contains(strParams, 'G=');
    DataInstallParams.GO1 = str2double(strParams{ind}(3:end));

    ind = find(contains(strParams, 'S='));
    if ~isempty(ind)
        DataInstallParams.S3S = str2double(strParams{ind}(3:end)); % Différent de .all et SonarRecord Viewer
    end
    % X = str2double(strParams{ind}(3:end));
    % DataInstallParams.S2S = find(X == [0.5 1 2]); % (0=0.5º,1=1º,2=2º)
end

%% POSI_1

k = contains(mots, 'POSI_1');
POSI_1 = mots{k};
strParams = strsplit(POSI_1, {';'; ':'});
mots(k) = [];

ind = contains(strParams, 'X=');
DataInstallParams.P1X = str2double(strParams{ind}(3:end));

ind = contains(strParams, 'Y=');
DataInstallParams.P1Y = str2double(strParams{ind}(3:end));

ind = contains(strParams, 'Z=');
DataInstallParams.P1Z = str2double(strParams{ind}(3:end));

ind = contains(strParams, 'D=');
DataInstallParams.P1D = str2double(strParams{ind}(3:end));

ind = contains(strParams, 'G=');
DataInstallParams.P1G = strParams{ind}(3:end);

ind = contains(strParams, 'T=');
DataInstallParams.P1T = strParams{ind}(3:end);

ind = contains(strParams, 'C=');
DataInstallParams.P1C = strParams{ind}(3:end);

ind = contains(strParams, 'F=');
DataInstallParams.P1F = strParams{ind}(3:end);

ind = contains(strParams, 'Q=');
DataInstallParams.P1Q = strParams{ind}(3:end);

ind = contains(strParams, 'I=');
DataInstallParams.P1I = strParams{ind}(3:end);

ind = contains(strParams, 'U=');
DataInstallParams.P1U = strParams{ind}(3:end);

%% POSI_2

k = contains(mots, 'POSI_2');
POSI_2 = mots{k};
strParams = strsplit(POSI_2, {';'; ':'});
mots(k) = [];

% ind = contains(strParams, 'N=');
% DataInstallParams.S2N = str2double(strParams{ind}(3:end));

%% POSI_3

k = contains(mots, 'POSI_3');
POSI_3 = mots{k};
strParams = strsplit(POSI_3, {';'; ':'});
mots(k) = [];

% ind = contains(strParams, 'N=');
% DataInstallParams.S2N = str2double(strParams{ind}(3:end));

%% ATTI_1

k = contains(mots, 'ATTI_1');
ATTI_1 = mots{k};
strParams = strsplit(ATTI_1, {';'; ':'});
mots(k) = [];

ind = contains(strParams, 'X=');
DataInstallParams.MSX = str2double(strParams{ind}(3:end));

ind = contains(strParams, 'Y=');
DataInstallParams.MSY = str2double(strParams{ind}(3:end));

ind = contains(strParams, 'Z=');
DataInstallParams.MSZ = str2double(strParams{ind}(3:end));

ind = contains(strParams, 'R=');
DataInstallParams.MSR = str2double(strParams{ind}(3:end));

ind = contains(strParams, 'P=');
DataInstallParams.MSP = str2double(strParams{ind}(3:end));

ind = contains(strParams, 'D=');
DataInstallParams.MSD = str2double(strParams{ind}(3:end)) * 1000;

ind = contains(strParams, 'H=');
DataInstallParams.MSH = str2double(strParams{ind}(3:end));

ind = contains(strParams, 'U=');
DataInstallParams.MSU = strParams{ind}(3:end);

ind = contains(strParams, 'I=');
DataInstallParams.MSI = strParams{ind}(3:end);

ind = contains(strParams, 'F=');
DataInstallParams.MSF = strParams{ind}(3:end);

ind = contains(strParams, 'M=');
DataInstallParams.MSM = strParams{ind}(3:end);

%% ATTI_2

k = contains(mots, 'ATTI_2');
ATTI_2 = mots{k};
strParams = strsplit(ATTI_2, {';'; ':'});
mots(k) = [];

ind = find(contains(strParams, 'X='));
if ~isempty(ind)
    DataInstallParams.NSX = str2double(strParams{ind}(3:end));
end

ind = find(contains(strParams, 'Y='));
if ~isempty(ind)
    DataInstallParams.NSY = str2double(strParams{ind}(3:end));
end

ind = find(contains(strParams, 'Z='));
if ~isempty(ind)
    DataInstallParams.NSZ = str2double(strParams{ind}(3:end));
end

ind = find(contains(strParams, 'R='));
if ~isempty(ind)
    DataInstallParams.NSR = str2double(strParams{ind}(3:end));
end

ind = find(contains(strParams, 'P='));
if ~isempty(ind)
    DataInstallParams.NSP = str2double(strParams{ind}(3:end));
end

ind = find(contains(strParams, 'D='));
if ~isempty(ind)
    DataInstallParams.NSD = str2double(strParams{ind}(3:end)) * 1000;
end

ind = find(contains(strParams, 'H='));
if ~isempty(ind)
    DataInstallParams.NSH = str2double(strParams{ind}(3:end));
end

ind = find(contains(strParams, 'U='));
if ~isempty(ind)
    DataInstallParams.NSU = strParams{ind}(3:end);
end

ind = find(contains(strParams, 'I='));
if ~isempty(ind)
    DataInstallParams.NSI = strParams{ind}(3:end);
end

ind = find(contains(strParams, 'F='));
if ~isempty(ind)
    DataInstallParams.NSF = strParams{ind}(3:end);
end

ind = find(contains(strParams, 'M='));
if ~isempty(ind)
    DataInstallParams.NSM = strParams{ind}(3:end);
end

%% CLCK

k = contains(mots, 'CLCK');
CLCK = mots{k};
strParams = strsplit(CLCK, {';'; ':'});
mots(k) = [];

% ind = contains(strParams, 'X=');
% DataInstallParams.NSX = str2double(strParams{ind}(3:end));

%{
{'CLCK'}    {'F=ZDA'}    {'S=CLK'}    {'A=On rising edge'}    {'I=Serial port 3'}    {'Q=OK'}
CLCK:F=ZDA
S=CLK
CLS  =  0,
CLO  =  0,
%}

%% EMXI

k = contains(mots, 'EMXI');
EMXI = mots{k};
strParams = strsplit(EMXI, {';'; ':'});
mots(k) = [];

ind = contains(strParams, 'SWLZ=');
DataInstallParams.WLZ = str2double(strParams{ind}(6:end));

%% CPU

k = contains(mots, 'CPU:');
CPU = mots{k};
DataInstallParams.PSV = CPU(5:end);
mots(k) = [];

%% CBMF

k = contains(mots, 'CBMF:');
CBMF = mots{k};
DataInstallParams.BSV = CBMF(6:end);
mots(k) = [];

%% RSV

k = find(contains(mots, 'RX:'));
RX = mots{k(1)};
DataInstallParams.RSV = RX(5:end);

%% Ouverture angulaire en Rx

k = find(contains(mots, 'RX:'));
RX = mots{k(2)};
DataInstallParams.RxAcrossBeamWidthDeg = str2double(RX(7:end-3)); % Creation nouvelle variable
mots(k) = [];

%% Ouverture angulaire en Tx

k = find(contains(mots, 'TX:'));
TX = mots{k(2)};
strParams = strsplit(TX, {';'; ':'});
DataInstallParams.TxAlongBeamWidthDeg = str2double(strParams{end}(1:end-3)); % Creation nouvelle variable
mots(k) = [];

%% EMXV

k = contains(mots, 'EMXV:');
EMXV = mots{k};
DataInstallParams.EmModel = str2double(EMXV(8:end));
mots(k) = [];

%% SMH

% k = contains(mots, 'SMH');
% EMXV = mots{k};
% DataInstallParams.EmModel = str2double(EMXV(8:end));

%% DPHI

%% SVPI

%% SN

k = contains(mots, 'SN');
SN = mots{k};
DataInstallParams.SN = str2double(SN(5:end));
mots(k) = []; %#ok<NASGU>

% TODO : SMH et S1S = 320 sur SonarRecord Viewer du .all convertit, 10001 sur XSF !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

%{
mots

mots =

  1×13 cell array

  Columns 1 through 6

    {'OSCV:Empty'}    {'↵PU_0'}    {'↵IP=157.237.14…'}    {'↵UDP=1997'}    {'↵TYPE=CON_TECH…'}    {'↵VERSIONS:'}

  Columns 7 through 11

    {'↵VXW:6.9 SMP J…'}    {'↵FILTER:1.1.0 …'}    {'↵VERSIONS-END'}    {'↵SERIALno-END'}    {'DPHI:U=NOT_SET'}

  Columns 12 through 13

    {'SVPI:U=NOT_SET'}    {0×0 char}    
%}

% DataInstallParamsControle
% DataInstallParams

% pppp = get_IIP_Install_txt(a) % Pas au point par rapport code de cette fonction ci !!!


DataInstallParams.isDual = 0; % Imposé par défaut (demande de savoir si il y a 2 têtes)

DataInstallParams.Title         = 'InstallationParameters';
DataInstallParams.Constructor   = 'Kongsberg';
DataInstallParams.TimeOrigin    = '01/01/-4713';
DataInstallParams.Comments      = 'One per file';
DataInstallParams.FormatVersion = 20101118;

DataInstallParams.SystemSerialNumber = double(sDataKmAll.dgmIIP(1).Header.systemId);
% DataInstallParams.SystemSerialNumberSecondHead: 0

DataInstallParams.Dimensions.NbDatagrams = 1; % 2 dans .all !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

% DataInstallParamsControle.Signals(1)
%   struct with fields:
%           Name: 'Time'
%     Dimensions: [1×1 struct]
%        Storage: 'double'
%           Unit: 'days since JC'
%      Populated: 1
%          Stats: [1×1 struct]
%          
% DataInstallParamsControle.Signals(2)
%   struct with fields:
% 
%           Name: 'SurveyLineNumber'
%     Dimensions: [1×1 struct]
%        Storage: 'single'
%           Unit: []
%      Populated: 1
%          Stats: [1×1 struct]

% DataInstallParams.Time: [1×1 cl_time]
% DataInstallParams.Datetime: [2×1 datetime]
% DataInstallParams.SurveyLineNumber: [2×1 single]

% GLU : comment fait-on pour récupérer l'heure et SurveyLineNumber à partir du XSF !!!!!!!!!!!!!!!!!!!!!!!!!!!!!


% Head1.SystemSerialNumber          
% Head1.SystemSerialNumberSecondHead 
    
flag = 1;


%{
 % Variables ayant une valeur différente entre .all et .kmall
% .all :	
P1T: 1
P1Q: 0
% .kmall :
P1T: 'Datagram'
P1Q: 'Off'


% Variables des .all ayant disparu dans .kmall
   SMH: 160
    R1S: 160
    TSV: '1.31 18.03.13'
    OSV: 'SIS 5.0.3'
    STC: 0
    DSX: 0
    DSY: 0
    DSZ: 0
    DSD: 0
    DSO: 0
    DSF: 1
    DSH: 'IN'
    P1M: 1
    P2M: 0
    P2T: 0
    P2Q: 0
    P2X: 0
    P2Y: 0
    P2Z: 0
    P2D: 0
    P2G: 'WGS84'
    P3M: 0
    P3T: 0
    P3Q: 0
    P3X: 0
    P3Y: 0
    P3Z: 0
    P3D: 0
    P3G: 'WGS84'
    P3S: 0
    MRP: 'RP'
     MSG: 0
     NRP: 'RP'
     NSG: 0
     PPS: 2
     CLS: 0
     CLO: 0
     SNL: 0
     APS: 0
     ARO: 2
     AHE: 2
     AHS: 2
     VSN: 1
						  
						  
% Variables nouvelles dans .kmall						  
K>> DataInstallParamsKmAll
    S1N: 0
    S2N: 0
    P1C: 'On'
    P1F: 'GGA'
    P1I: 'Serial port 1'
    P1U: 'ACTIVE'
    MSH: 0
    MSU: 'ACTIVE'
    MSI: 'Net port 2'
    MSF: 'KM Binary'
    MSM: 'Rotation'
    NSH: 0
    NSU: 'PASSIVE'
    NSI: 'Serial port 2'
    NSF: 'EM Attitude'
    NSM: 'Rotation'
    RxAcrossBeamWidthDeg: 1
    TxAlongBeamWidthDeg: 0.500000000000000
    SN: 10001
%}
