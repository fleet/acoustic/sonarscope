function [flag, XML, Data] = extractPosition(this)

%{
Variables ayant disparu dans les .kmall :
                      PingCounter: [528�1 uint16]
                         Quality: [528�1 single]
                     CourseVessel: [528�1 single]
               PositionDescriptor: [528�1 uint8]	

Nouvelles variables :
    []
%}
    
XML  = [];
Data = [];

%% Variables

%% Time : OK

DataBin.Datetime = get_SPO_Datetime(this);
DataBin.Time = cl_time('timeMat', datenum(DataBin.Datetime));

N = length(DataBin.Time);

%% Latitude : OK

% subSensorStatus = get_SKM_selectionSensorStatus(this, -1);
% ppppLat = get_SKM_Latitude_deg(this, 'sub', subSensorStatus);
% ppppLon = get_SKM_Longitude_deg(this, 'sub', subSensorStatus);
% % my_figure; my_plot(ppppLon, ppppLat, '.-b'); grid on;

DataBin.Latitude = get_SPO_CorrectedLat_deg(this);
FieldUnits.Latitude = 'deg';
% figure; plot(DataBin.Latitude, '.-b'); grid on;

%% Longitude : OK

DataBin.Longitude = get_SPO_CorrectedLon_deg(this);
FieldUnits.Longitude = 'deg';
% figure; plot(DataBin.Longitude, '.-b'); grid on;
% my_figure; my_plot(DataBin.Longitude, DataBin.Latitude, '.-b'); grid on;

%% Heading : Valeurs tr�s diff�rentes

% DataBin.Heading = get_CPO_CourseOverGround_deg(this);
DataBin.Heading = get_SPO_CourseOverGround_deg(this);
FieldUnits.Heading = 'deg';
% figure; plot(DataBin.Heading, '.-b'); grid on;

%% Speed : OK

DataBin.Speed = get_SPO_SpeedOverGround_mPerSec(this);
FieldUnits.Speed = 'm/s';
% figure; plot(DataBin.Speed, '.-b'); grid on;

%% AltitudeOfIMU : OK

DataBin.AltitudeOfIMU = get_SPO_EllipsoidHeightReRefPoint_m(this);
FieldUnits.AltitudeOfIMU = 'm';
% figure;plot(DataBin.AltitudeOfIMU, '.-b'); grid on;

%% system_serial_number

%{
% system_serial_number % Nouvelle variable ?
DataBin.system_serial_number = Hdf5Utils.read_value(Hdf5Data.Platform.Position.Sensor000.Vendor_specific.system_serial_number);
DataBin.system_serial_number = single(DataBin.system_serial_number);
FieldUnits.system_serial_number = '';
% figure; plot(DataPosition.QualityIndicator, 'k'); grid on; hold on; plot(DataBin.system_serial_number, '.r');
%}

%% PositionDescriptor

DataBin.PositionDescriptor = ones(N, 1, 'single');
FieldUnits.PositionDescriptor = '';
% figure; plot(DataBin.PositionDescriptor); grid on;

%% data_received_from_sensor

% {
% data_received_from_sensor % Nouvelle variable ?

NMEA = get_SPO_PosDataFromSensor(this);
if ~isempty(NMEA)
    [flag, Heure1, Lat1, Lon1, GPS] = read_Navigation_NMEA(NMEA); %#ok<ASGLU>
    if flag
        DataBin.GPSDatetime = dateshift(DataBin.Datetime, 'start', 'day') + duration(0, 0, Heure1/1000);
        DataBin.GPSTime = cl_time('timeMat', datenum(DataBin.GPSDatetime));
        
        DataBin.QualityIndicator              = single([GPS.QualityIndicator]');
        DataBin.NumberSatellitesUsed          = single([GPS.NumberSatellitesUsed]');
        DataBin.HorizontalDilutionOfPrecision = single([GPS.HorizontalDilutionOfPrecision]');
        DataBin.AltitudeOfIMU                 = single([GPS.AltitudeOfIMU]');
        DataBin.GeoidalSeparation             = single([GPS.GeoidalSeparation]');
        DataBin.AgeOfDifferentialCorrections  = single([GPS.AgeOfDifferentialCorrections]');
        DataBin.DGPSRefStationIdentity        = [GPS.DGPSRefStationIdentity]';
        
        FieldUnits.QualityIndicator              = '';
        FieldUnits.NumberSatellitesUsed          = '';
        FieldUnits.HorizontalDilutionOfPrecision = 'm';
        FieldUnits.AltitudeOfIMU                 = 'm';
        FieldUnits.GeoidalSeparation             = '';
        FieldUnits.AgeOfDifferentialCorrections  = '';
        FieldUnits.DGPSRefStationIdentity        = '';

        %{
        figure; plot(DataBin.QualityIndicator, '.r'); grid on;
        figure; plot(DataBin.NumberSatellitesUsed, '.r'); grid on;
        figure; plot(DataBin.HorizontalDilutionOfPrecision, '.r'); grid on;
        figure; plot(DataBin.AltitudeOfIMU, '.r'); grid on;
        figure; plot(DataBin.GeoidalSeparation, '.r'); grid on;
        figure; plot(DataBin.AgeOfDifferentialCorrections, '.r'); grid on;
        %}
    end
end

%% sensor_status

%{
% sensor_status % Nouvelle variable ?
DataBin.system_serial_number = Hdf5Utils.read_value(Hdf5Data.Platform.Position.Sensor000.Vendor_specific.sensor_status);
DataBin.sensor_status = single(DataBin.system_serial_number);
FieldUnits.sensor_status = '';
% figure; plot(DataPosition.NumberSatellitesUsed, 'k'); grid on; hold on; plot(DataBin.sensor_status, '.r');
%}

%% Cr�ation arbitraire de PingCounter

DataBin.PingCounter = (1:N)';
FieldUnits.PingCounter = '';

%% Cr�ation du XML

Fields = fields(DataBin);

XML.Dimensions.NbSamples = N;

XML.Title         = 'Position';
XML.Constructor   = 'Kongsberg';
XML.EmModel       = double(this.sDataKmAll.dgmSPO(1).Header.echoSounderId);
XML.TimeOrigin    = '01/01/-4713';
XML.Comments      = 'Sensor sampling rate'; % ne rien mettre apr�s premoers tests
XML.FormatVersion = 20101118;

XML.SystemSerialNumber = double(this.sDataKmAll.dgmSPO(1).Header.systemId);

Fields = Fields(~contains(Fields, 'Datetime')); 
for k=1:length(Fields)
    XML.Signals(k).Dimensions.Name      = 'NbSamples';
    XML.Signals(k).Dimensions.Length    = N;
    XML.Signals(k).Dimensions.Unlimited = 0;
    
    Values  = DataBin.(Fields{k});
    Storage = class(Values);
    switch Storage
        case 'cl_time'
            Storage = 'double';
            Unit    = 'days since JC';
            Values  = Values.timeMat;
        otherwise
            Unit = FieldUnits.(Fields{k});
    end
    
    XML.Signals(k).Name      = Fields{k};
    XML.Signals(k).Storage   = Storage;
    XML.Signals(k).Unit      = Unit;
    XML.Signals(k).Populated = 1;
    XML.Signals(k).Stats     = stats(Values);
    
    %{
    try
        for k2=1:length(DataPosition.Signals)
            if strcmp(XML.Signals(k2).Name, DataPosition.Signals(k2).Name)
                break
            end
        end
        fprintf('XML.Signals(%d).Unit = ''%s'';\n', k2, DataPosition.Signals(k2).Unit);
    catch
        XML.Signals(k).Name;
    end
    %}
end

%{
XML.Signals(3).Unit = '';
%}

FieldsData = fields(DataBin);
FieldsXML  = fields(XML);
for k=1:length(FieldsXML)
    Data.(FieldsXML{k}) = XML.(FieldsXML{k});
end

for k=1:length(FieldsData)
    Data.(FieldsData{k}) = DataBin.(FieldsData{k});
end

flag = 1;

% DataPosition
% Data
