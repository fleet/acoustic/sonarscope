function [flag, XML, Data] = extractRawRangeBeamAngle(this)

%{
Variables ayant disparu dans les .kmall :
    xxx

Nouvelles variables :
    xxx
%}


%{
DataRaw = 
Title: 'RawRangeBeamAngle'
Constructor: 'Kongsberg'
EmModel: 304
ListeSystemSerialNumber: 160
TimeOrigin: '01/01/-4713'
Comments: 'Sounder ping rate'
                     Version: 'V1' % TODO JM1 : V2 en principe !!!
FormatVersion: 20101118

Dimensions: [1�1 struct]
       nbPings: 84
          nbTx: 8
          nbRx: 800
    nbSounders: 1

                     Signals: [1�15 struct]
                      Images: [1�8 struct]

Time: [1�1 cl_time]
Datetime: [84�1 datetime]
PingCounter: [84�1 single]
SystemSerialNumber: [84�1 single]
SoundSpeed: [84�1 single]
NbBeams: [84�1 single]
SamplingRate: [84�1 single]
                   TiltAngle: [84�8 single]
FocusRange: [84�8 single]
SignalLength: [84�8 single]
SectorTransmitDelay: [84�8 single]
CentralFrequency: [84�8 single]
         MeanAbsorptionCoeff: [84�8 single]
SignalWaveformIdentifier: [84�8 single]
TransmitSectorNumberTx: [84�8 single]
SignalBandwith: [84�8 single]

BeamPointingAngle: [84�800 single]
TransmitSectorNumberRx: [84�800 single]
DetectionInfo: [84�800 single]
             LengthDetection: [84�800 single]
               QualityFactor: [84�800 single]
TwoWayTravelTime: [84�800 single]
Reflectivity: [84�800 single]
        RealTimeCleaningInfo: [84�800 single]	
%}
    
XML  = [];
Data = [];

%% Time et Datetime : OK

Signals.Datetime = get_MRZ_Datetime(this);
Signals.Time = cl_time('timeMat', datenum(Signals.Datetime));

N = length(Signals.Datetime);

%% PingCounter : OK - Valeurs diff�rentes mais .kmall conforme � Sonar Record Viewer

Signals.PingCounter = get_MRZ_PingCnt(this);
FieldUnits.PingCounter = '';
% figure; plot(Signals.PingCounter, '.-b'); grid on;

%% SystemSerialNumber % OK

SSN = double(this.sDataKmAll.dgmMRZ(1).Header.systemId);
Signals.SystemSerialNumber = repmat(single(SSN), N, 1); % TODO : trouver o� r�cup�rer cette info !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
FieldUnits.SystemSerialNumber = '';

%% SoundSpeed : OK

Signals.SoundSpeed = get_MRZ_SoundSpeedAtTxDepth_mPerSec(this);
FieldUnits.SoundSpeed = 'm/s';
% figure; plot(Signals.SoundSpeed, '.-b'); grid on;

%% NbBeams : OK

Signals.NbBeams = get_MRZ_NumSoundingsValidMain(this);
Signals.NbBeams = single(Signals.NbBeams);
FieldUnits.NbBeams = '';
% figure; plot(Signals.NbBeams, '.-b'); grid on;

%% SamplingRate : OK

Signals.SamplingRate = get_MRZ_SeabedImageSampleRate(this);
FieldUnits.SamplingRate = 'Hz';
% figure; plot(Signals.SamplingRate, '.-b'); grid on;

%% TiltAngle : OK 

Signals.TiltAngle = get_MRZ_TiltAngleReTx_deg(this);
FieldUnits.TiltAngle = 'deg';
% figure; plot(Signals.TiltAngle, '.-b'); grid on;

nbTx = size(Signals.TiltAngle, 2);

%% FocusRange : OK

Signals.FocusRange = get_MRZ_TxFocusRange_m(this);
FieldUnits.FocusRange = 'm';
% figure; plot(Signals.FocusRange, '.-b'); grid on;

%% SignalLength : TODO : valeurs compl�tement diff�rentes, rapport de 0.414474699946222 entre les 2 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

% Signals.SignalLength = get_MRZ_MaxTotalTxPulseLength_sec(this);
% Signals.SignalLength = get_MRZ_MaxEffTxPulseLength_sec(this);
Signals.SignalLength = get_MRZ_TotalSignalLength_sec(this);
FieldUnits.SignalLength = 's';
% figure; plot(Signals.SignalLength, '.-b'); grid on;

%% SectorTransmitDelay : OK

Signals.SectorTransmitDelay = get_MRZ_SectorTransmitDelay_sec(this);
Signals.SectorTransmitDelay = Signals.SectorTransmitDelay * 1000;
FieldUnits.SectorTransmitDelay = 'ms';
% Signals.SectorTransmitDelay(1,:)
% figure; plot(Signals.SectorTransmitDelay, '.-b'); grid on;

%% CentralFrequency : OK

Signals.CentralFrequency = get_MRZ_CentreFreq_Hz(this);
FieldUnits.CentralFrequency = 'Hz';
% figure; plot(Signals.CentralFrequency, '.-b'); grid on;

%% TransmitSectorNumberRx : OK

Images.TransmitSectorNumberRx = get_MRZ_TxSectorNumber(this);
Images.TransmitSectorNumberRx = single(Images.TransmitSectorNumberRx);
Images.TransmitSectorNumberRx = Images.TransmitSectorNumberRx + 1;
FieldUnits.TransmitSectorNumberRx = '';
% figure; imagesc(Images.TransmitSectorNumberRx); colormap(jet(256)); colorbar; title('Images.TransmitSectorNumberRx')

%% MeanAbsorptionCoeff : OK

RxMeanAbsorptionCoeff = get_MRZ_MeanAbsCoeff_dBPerkm(this);
% figure; imagesc(RxMeanAbsorptionCoeff); colormap(jet(256)); colorbar; title('RxMeanAbsorptionCoeff')
Signals.MeanAbsorptionCoeff = getTxMeanAbsorptionCoeff(RxMeanAbsorptionCoeff, Images.TransmitSectorNumberRx);
FieldUnits.MeanAbsorptionCoeff = 'dB/km';
% figure; plot(Signals.MeanAbsorptionCoeff, '.-b'); grid on;

%% SignalWaveformIdentifier : OK mais toutes les valeurs sont = 0

Signals.SignalWaveformIdentifier = get_MRZ_SignalWaveForm(this);
Signals.SignalWaveformIdentifier = single(Signals.SignalWaveformIdentifier);
FieldUnits.SignalWaveformIdentifier = '';
% figure; plot(Signals.SignalWaveformIdentifier, '.-b'); grid on;

%% TransmitSectorNumberTx : OK

% Signals.TransmitSectorNumberTx = get_MRZ_NumTxSectors(this);
Signals.TransmitSectorNumberTx = get_MRZ_TxSectorNumb(this);
Signals.TransmitSectorNumberTx = 1 + single(Signals.TransmitSectorNumberTx);
FieldUnits.TransmitSectorNumberTx = '';
% figure; plot(Signals.TransmitSectorNumberTx, '.-b'); grid on;

%% SignalBandwith : OK

Signals.SignalBandwith = get_MRZ_TxBandwidth_Hz(this);
FieldUnits.SignalBandwith = 'Hz';
% figure; plot(Signals.SignalBandwith, '.-b'); grid on;




%% BeamPointingAngle : OK

Images.BeamPointingAngle = get_MRZ_BeamAngleReRx_deg(this);
% get_MRZ_BeamAngleReRxCorrection_deg(this);
Images.BeamPointingAngle = -single(Images.BeamPointingAngle);
FieldUnits.BeamPointingAngle = 'deg';
% figure; imagesc(Images.BeamPointingAngle); colormap(jet(256)); colorbar; title('Images.BeamPointingAngle')

nbRx = size(Images.BeamPointingAngle, 2);

%% DetectionInfo % TODO factoriser ce code avec extractDepth et utiliser bitand, etc : 145 == 2

Images.DetectionInfo = get_MRZ_DetectionType(this);
% Images.DetectionInfo = get_MRZ_DetectionMode(this);
Images.DetectionInfo = single(Images.DetectionInfo);
FieldUnits.DetectionInfo = '';

% sub = (Images.DetectionInfo == 0);
% Images.DetectionInfo(sub)  = 16;
% Images.DetectionInfo(~sub) = 17;

% figure; imagesc(Images.DetectionInfo); colormap(jet(256)); colorbar; title('Images.DetectionInfo')

%% LengthDetection % TODO : coefficient � trouver (8.336807002917882e+02)

Images.LengthDetection = get_MRZ_DetectionWindowLength_sec(this);
FieldUnits.LengthDetection = 'sample';
% figure; imagesc(Images.LengthDetection); colormap(jet(256)); colorbar; title('Images.LengthDetection')


%% QualityFactor : coefficient � trouver, Attention : c'est le m�me code que dans extractDepth : faire une seule fonction

Images.QualityFactor = get_MRZ_QualityFactor(this);
FieldUnits.QualityFactor = '';
% figure; imagesc(Images.QualityFactor); colormap(jet(256)); colorbar; title('Images.QualityFactor')

%% TwoWayTravelTime : OK

Images.TwoWayTravelTime = get_MRZ_TwoWayTravelTime_sec(this);
FieldUnits.TwoWayTravelTime = 's';
% figure; imagesc(Images.TwoWayTravelTime); colormap(jet(256)); colorbar; title('Images.TwoWayTravelTime')

%% Reflectivity : OK : TODO c'est le m�me code que dans extractDepth : faire un seule fonction

% Images.Reflectivity = get_MRZ_Reflectivity1_dB(this);
Images.Reflectivity = get_MRZ_Reflectivity2_dB(this);
FieldUnits.Reflectivity = 'dB';
% figure; imagesc(Images.Reflectivity); colormap(gray(256)); colorbar; title('Images.Reflectivity')

%% RealTimeCleaningInfo

Images.RealTimeCleaningInfo = zeros(size(Images.BeamPointingAngle), 'single');
FieldUnits.RealTimeCleaningInfo = '';

%% Cr�ation du XML

XML.Title         = 'RawRangeBeamAngle';
XML.Constructor   = 'Kongsberg';
XML.EmModel       = double(this.sDataKmAll.dgmMRZ(1).Header.echoSounderId);
XML.TimeOrigin    = '01/01/-4713';
XML.Comments      = 'Sounder ping rate';
XML.Version       = 'V2';
XML.FormatVersion = 20101118;
XML.IdentAlgoSnippets2OneValuePerBeam = 2;

XML.Dimensions.nbPings    = N;
XML.Dimensions.nbTx       = nbTx;
XML.Dimensions.nbRx       = nbRx;
XML.Dimensions.nbSounders = 1;

% Dans datagramme Installation SN=10001 ?
XML.ListeSystemSerialNumber = double(this.sDataKmAll.dgmMRZ(1).Header.systemId);

Fields = fields(Signals);
Fields = Fields(~contains(Fields, 'Datetime')); 
for k=1:length(Fields)
    XML.Signals(k).Dimensions.Name      = 'nbPings';
    XML.Signals(k).Dimensions.Length    = N;
    XML.Signals(k).Dimensions.Unlimited = 0;
    
    Values  = Signals.(Fields{k});
    Storage = class(Values);
    switch Storage
        case 'cl_time'
            Storage = 'double';
            Unit    = 'days since JC';
            Values  = Values.timeMat;
        otherwise
            Unit = FieldUnits.(Fields{k});
    end
    
    if size(Values, 2) == nbTx
        XML.Signals(k).Dimensions(2).Name      = 'nbTx';
        XML.Signals(k).Dimensions(2).Length    = nbTx;
        XML.Signals(k).Dimensions(2).Unlimited = 0;
    end
    
    XML.Signals(k).Name      = Fields{k};
    XML.Signals(k).Storage   = Storage;
    XML.Signals(k).Unit      = Unit;
    XML.Signals(k).Populated = 1;
    XML.Signals(k).Stats     = stats(Values);
    
    %{
    try
        for k2=1:length(DataPosition.Signals)
            if strcmp(XML.Signals(k2).Name, DataPosition.Signals(k2).Name)
                break
            end
        end
        fprintf('XML.Signals(%d).Unit = ''%s'';\n', k2, DataPosition.Signals(k2).Unit);
    catch
        XML.Signals(k).Name;
    end
    %}
end


Fields = fields(Images);
for k=1:length(Fields)
    XML.Images(k).Dimensions.Name      = 'nbPings';
    XML.Images(k).Dimensions.Length    = N;
    XML.Images(k).Dimensions.Unlimited = 0;
    
    XML.Images(k).Dimensions(2).Name      = 'nbRx';
    XML.Images(k).Dimensions(2).Length    = nbRx;
    XML.Images(k).Dimensions(2).Unlimited = 0;
    
    Values  = Images.(Fields{k});
    Storage = class(Values);
    switch Storage
        case 'cl_time'
            Storage = 'double';
            Unit    = 'days since JC';
            Values  = Values.timeMat;
        case 'datetime'
            continue
        otherwise
            Unit = FieldUnits.(Fields{k});
    end
    
    XML.Images(k).Name      = Fields{k};
    XML.Images(k).Storage   = Storage;
    XML.Images(k).Unit      = Unit;
    XML.Images(k).Populated = 1;
    XML.Images(k).Stats     = stats(Values);
end

FieldsXML  = fields(XML);
for k=1:length(FieldsXML)
    Data.(FieldsXML{k}) = XML.(FieldsXML{k});
end

FieldsData = fields(Signals);
for k=1:length(FieldsData)
    Data.(FieldsData{k}) = Signals.(FieldsData{k});
end

FieldsData = fields(Images);
for k=1:length(FieldsData)
    Data.(FieldsData{k}) = Images.(FieldsData{k});
end

% DataPosition
% Data

flag = 1;


function TxMeanAbsorptionCoeff = getTxMeanAbsorptionCoeff(RxMeanAbsorptionCoeff, TransmitSectorNumberRx)

[nbPings, nbRx] = size(RxMeanAbsorptionCoeff); %#ok<ASGLU>
nbTx = max(TransmitSectorNumberRx(:));
TxMeanAbsorptionCoeff = NaN(nbPings, nbTx, 'single');
for k=1:nbPings
    [~, ia] = unique(TransmitSectorNumberRx(k,:));
    TxMeanAbsorptionCoeff(k,1:length(ia)) = RxMeanAbsorptionCoeff(k, ia);
end
