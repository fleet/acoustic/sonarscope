function [flag, XML, Data] = extractSVP(this)
    
XML  = [];
Data = [];

%% Variables

%% Time : OK

DataBin.Datetime = get_SVP_Datetime(this);
DataBin.Time = cl_time('timeMat', datenum(DataBin.Datetime));

DataBin.DatetimeStartOfUse = DataBin.Datetime;
DataBin.TimeStartOfUse = cl_time('timeMat', datenum(DataBin.DatetimeStartOfUse));

N = length(DataBin.DatetimeStartOfUse);

%{
Y = get_SVP_NumSamples(this)
Y = get_SVP_SensorFormat(this)
Y = get_SVP_Absorption(this)
%}

if N > 1
    my_breakpoint % Adapter ce qui suit quand il y a plusieurs profils
end

% TODO GLU : l'heure est fausse dans XSF mais on la r�cup�re bien dans Vendor_specific
%{
DataSSP.DatetimeStartOfUse : 2018-09-05 22:21:54.434
DataBin.Datetime           : 1970-01-18 18:43:06.114 (provenant de Hdf5Data.Environment.Sound_speed_profile.time)
DataBin.Datetime           : 2018-09-05 22:21:54.434 (provenant de Hdf5Data.Environment.Sound_speed_profile.Vendor_specific.datagram_time)
%}

%% Depth : OK

DataBin.Depth = get_SVP_Depth(this);
% figure; plot(DataBin.Depth, '.-b'); grid on;

%% SoundSpeed : OK

DataBin.SoundSpeed = get_SVP_Celerity(this);
% figure; plot(DataBin.SoundSpeed, 'b'); grid on;
% figure; plot(DataBin.SoundSpeed, DataBin.Depth, '.-b'); grid on;

%% Temperature : OK

DataBin.Temperature = get_SVP_Temp(this);
% figure; plot(DataBin.Temperature, 'b'); grid on;

%% Salinity : OK

DataBin.Salinity = get_SVP_Salinity(this);
% figure; plot(DataBin.Salinity, 'b'); grid on;

%% Latitude
%{
% Latitude % TODO GLU : valeur fausse
DataBin.Latitude = Hdf5Utils.read_value(Hdf5Data.Environment.Sound_speed_profile.lat);
% Unit = Hdf5Utils.read_unit(Hdf5Data.Environment.Sound_speed_profile.lat)
% figure; plot(DataSSP.Latitude, 'k'); grid on; hold on; plot(DataBin.Latitude, '.r');
%}

%% Longitude

%{
% Longitude % TODO GLU : valeur fausse
DataBin.Longitude = Hdf5Utils.read_value(Hdf5Data.Environment.Sound_speed_profile.lon);
% Unit = Hdf5Utils.read_unit(Hdf5Data.Environment.Sound_speed_profile.lon)
% figure; plot(DataSSP.Longitude, 'k'); grid on; hold on; plot(DataBin.Longitude, '.r');
%}

%% PingCounter

DataBin.PingCounter = (1:N)';

%% TimeProfileMeasurement et DatetimeProfileMeasurement

% TODO GLU
DataBin.TimeProfileMeasurement     = DataBin.TimeStartOfUse;
DataBin.DatetimeProfileMeasurement = DataBin.DatetimeStartOfUse;

%% NbEntries : OK

DataBin.NbEntries = get_SVP_NumSamples(this);

%% DepthResolution : Valeur diff�rente

DataBin.DepthResolution = min(abs(diff(DataBin.Depth)));

%% Cr�ation du XML

XML.Title         = 'SoundSpeedProfile';
XML.Constructor   = 'Kongsberg';
XML.EmModel       = double(this.sDataKmAll.dgmSVP(1).Header.echoSounderId);
XML.TimeOrigin    = '01/01/-4713';
XML.Comments      = '';
XML.FormatVersion = 20101118;

XML.Dimensions.nbProfiles = N;
XML.Dimensions.nbPoints   = max(DataBin.NbEntries);

XML.SystemSerialNumber = double(this.sDataKmAll.dgmSVP(1).Header.systemId);

Dimensions = struct('Name', 'nbProfiles', 'Length', 1, 'Unlimited', 0);

XML.Signals.Name       = 'TimeStartOfUse';
XML.Signals.Storage    = 'double';
XML.Signals.Unit       = 'days since JC';
XML.Signals.Populated  = 1;
XML.Signals.Stats      = stats(DataBin.TimeStartOfUse.timeMat);
XML.Signals.Dimensions = Dimensions;

XML.Signals(end+1).Name     = 'TimeProfileMeasurement';
XML.Signals(end).Storage    = 'double';
XML.Signals(end).Unit       = 'days since JC';
XML.Signals(end).Populated = 1;
XML.Signals(end).Stats      = stats(DataBin.TimeProfileMeasurement.timeMat);
XML.Signals(end).Dimensions = Dimensions;

XML.Signals(end+1).Name     = 'PingCounter';
XML.Signals(end).Storage    = 'uint16';
XML.Signals(end).Unit       = '';
XML.Signals(end).Populated  = 1;
XML.Signals(end).Stats      = stats(DataBin.PingCounter);
XML.Signals(end).Dimensions = Dimensions;

XML.Signals(end+1).Name     = 'NbEntries';
XML.Signals(end).Storage    = 'uint16';
XML.Signals(end).Unit       = '';
XML.Signals(end).Populated  = 1;
XML.Signals(end).Stats      = stats(DataBin.NbEntries);
XML.Signals(end).Dimensions = Dimensions;

XML.Signals(end+1).Name     = 'DepthResolution';
XML.Signals(end).Storage    = 'single';
XML.Signals(end).Unit       = 'm';
XML.Signals(end).Populated  = 1;
XML.Signals(end).Stats      = stats(DataBin.DepthResolution);
XML.Signals(end).Dimensions = Dimensions;

Dimensions(2) = struct('Name', 'nbPoints', 'Length', XML.Dimensions.nbPoints, 'Unlimited', 0);

XML.Images.Name             = 'Depth';
XML.Images.Storage          = 'single';
XML.Images.Unit             = 'm';
XML.Images.Populated        = 1;
XML.Images.Stats            = stats(DataBin.Depth);
XML.Images(end).Dimensions = Dimensions;

XML.Images(end+1).Name      = 'SoundSpeed';
XML.Images(end).Storage     = 'single';
XML.Images(end).Unit        = 'm/s';
XML.Images(end).Populated   = 1;
XML.Images(end).Stats       = stats(DataBin.SoundSpeed);
XML.Images(end).Dimensions = Dimensions;

XML.Images(end+1).Name      = 'Temperature';
XML.Images(end).Storage     = 'single';
XML.Images(end).Unit        = 'deg';
XML.Images(end).Populated   = 1;
XML.Images(end).Stats       = stats(DataBin.Temperature);
XML.Images(end).Dimensions = Dimensions;

XML.Images(end+1).Name      = 'Salinity';
XML.Images(end).Storage     = 'single';
XML.Images(end).Unit        = '1/1000';
XML.Images(end).Populated   = 1;
XML.Images(end).Stats       = stats(DataBin.Salinity);
XML.Images(end).Dimensions = Dimensions;

FieldsData = fields(DataBin);
FieldsXML  = fields(XML);
for k=1:length(FieldsXML)
    Data.(FieldsXML{k}) = XML.(FieldsXML{k});
end

for k=1:length(FieldsData)
    Data.(FieldsData{k}) = DataBin.(FieldsData{k});
end

flag = 1;

% DataSSP
% Data
