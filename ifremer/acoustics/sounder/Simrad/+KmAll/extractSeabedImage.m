function [flag, XML, Data] = extractSeabedImage(this, DataRuntimeKmAll)

%{
Variables ayant disparu dans les .kmall :
    xxx

Nouvelles variables :
    xxx
%}


%{
DataSeabed = 
Title: 'SeabedImage59h'
Constructor: 'Kongsberg'
EmModel: 304
ListeSystemSerialNumber: 160
SystemSerialNumber: [84�1 single]
TimeOrigin: '01/01/-4713'
Comments: 'Sensor sampling rate'
Version: 'V2'
FormatVersion: 20101118

                 Dimensions: [1�1 struct]
DataSeabed.Dimensions
nbPings: 84
nbBeams: 800
nbSounders: 1
    nbSamplesEntries: 357456

                    Signals: [1�12 struct]
                     Images: [1�4 struct]

Time: [1�1 cl_time]
Datetime: [84�1 datetime]
PingCounter: [84�1 single]
SamplingRate: [84�1 single]
                       TVGN: [84�1 single]
TVGNRecomputed: [84�1 single]
                        BSN: [84�1 single]
                        BSO: [84�1 single]
                TxBeamWidth: [84�1 single]
               TVGCrossOver: [84�1 single]
NbBeams: [84�1 single]

      InfoBeamSortDirection: [84�800 single]
      InfoBeamDetectionInfo: [84�800 single]
InfoBeamNbSamples: [84�800 single]
InfoBeamCentralSample: [84�800 single]
Entries: [357456�1 single]
%}
    
XML  = [];
Data = [];

%% Time et Datetime : OK

Signals.Datetime = get_MRZ_Datetime(this);
Signals.Time = cl_time('timeMat', datenum(Signals.Datetime));

N = length(Signals.Datetime);

%% PingCounter : OK

Signals.PingCounter = get_MRZ_PingCnt(this);
FieldUnits.PingCounter = '';
% figure; plot(Signals.PingCounter); grid on;

%% SystemSerialNumber % TODO GLU

EmModel = double(this.sDataKmAll.dgmMRZ(1).Header.echoSounderId);
Signals.SystemSerialNumber = repmat(single(EmModel), N, 1); % TODO : trouver o� r�cup�rer cette info !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
FieldUnits.SystemSerialNumber = '';

%% NbBeams : OK

% WCBeamNumber = get_MRZ_WCBeamNumber(this);
Signals.NbBeams = get_MRZ_NumSoundingsMaxMain(this);
FieldUnits.NbBeams = '';
% figure; plot(Signals.NbBeams); grid on;

nbBeams = max(Signals.NbBeams);

%% SamplingRate : OK

Signals.SamplingRate = get_MRZ_WCSampleRate(this);
FieldUnits.SamplingRate = 'Hz';
% figure; plot(Signals.SamplingRate, '.r'); grid on;

%% TVGN % TODO GLU : je n'ai pas trouv� la correspondance, je recalcule la valeur mais �a ne correspond pas parfaitement, diff�rence de l'ordre de 50 �chantillons

TVGNInSeconds = get_MRZ_TwoWayTravelTime_sec(this);

TVGNInSeconds = min(TVGNInSeconds, [], 2);
Signals.TVGN = TVGNInSeconds .* Signals.SamplingRate;

FieldUnits.TVGN = 'samples';
% my_figure;my_plot(Signals.TVGN); grid on;

%% TVGNRecomputed

Signals.TVGNRecomputed = NaN([N, 1], 'single');
FieldUnits.TVGNRecomputed = 'samples';
% figure; plot(Signals.TVGNRecomputed); grid on;

%% BSN : OK

Signals.BSN = get_MRZ_BSnormal_dB(this);
FieldUnits.BSN = 'dB';
% figure; plot(Signals.BSN); grid on;

%% BSO : OK

Signals.BSO = get_MRZ_BSoblique_dB(this);
FieldUnits.BSO = 'dB';
% figure; plot(Signals.BSO); grid on; hold on; plot(Signals.BSN, 'r');

%% TVGCrossOver : OK

Signals.TVGCrossOver = my_interp1_Extrap_PreviousThenNext(DataRuntimeKmAll.Datetime, DataRuntimeKmAll.TVGCrossAngle, Signals.Datetime);
FieldUnits.TVGCrossOver = 'deg';
% figure; plot(Signals.TVGCrossOver); grid on;

%% TxBeamWidth

Signals.TxBeamWidth = NaN([N, 1], 'single');
FieldUnits.TxBeamWidth = 'deg';
% figure;plot(Signals.TxBeamWidth, '.r'); grid on;

%% InfoBeamSortDirection

% DataSeabed.InfoBeamSortDirection
%{
Images.InfoBeamSortDirection = Hdf5Utils.read_value(Hdf5Data.Sonar.Beam_group1.Bathymetry.detection_z);
Images.InfoBeamSortDirection = single(Images.InfoBeamSortDirection');
%}
Images.InfoBeamSortDirection = ones([N, nbBeams], 'single');
FieldUnits.InfoBeamSortDirection = '';
% figure; imagesc(DataSeabed.InfoBeamSortDirection); colormap(jet(256)); colorbar; title('DataSeabed.InfoBeamSortDirection')
% figure; imagesc(Images.InfoBeamSortDirection); colormap(jet(256)); colorbar; title('Images.InfoBeamSortDirection')
% figure; imagesc(DataSeabed.InfoBeamSortDirection-Images.InfoBeamSortDirection); colormap(jet(256)); colorbar; title('DataSeabed.InfoBeamSortDirection - Images.InfoBeamSortDirection')      

%% InfoBeamDetectionInfo % TODO GLU

% DataSeabed.InfoBeamDetectionInfo
%{
Images.InfoBeamDetectionInfo = Hdf5Utils.read_value(Hdf5Data.Sonar.Beam_group1.Bathymetry.detection_z);
Images.InfoBeamDetectionInfo = single(Images.InfoBeamDetectionInfo');
%}
Images.InfoBeamDetectionInfo = zeros([N, nbBeams], 'single');
FieldUnits.InfoBeamDetectionInfo = '';
% figure; imagesc(DataSeabed.InfoBeamDetectionInfo); colormap(jet(256)); colorbar; title('DataSeabed.InfoBeamDetectionInfo')
% figure; imagesc(Images.InfoBeamDetectionInfo); colormap(jet(256)); colorbar; title('Images.InfoBeamDetectionInfo')
% figure; imagesc(DataSeabed.InfoBeamDetectionInfo-Images.InfoBeamDetectionInfo); colormap(jet(256)); colorbar; title('DataSeabed.InfoBeamDetectionInfo - Images.InfoBeamDetectionInfo')      

%% InfoBeamCentralSample : OK

Images.InfoBeamCentralSample = get_MRZ_SIcentreSample(this);
Images.InfoBeamCentralSample = single(Images.InfoBeamCentralSample);
FieldUnits.InfoBeamCentralSample = 'sample';
% figure; imagesc(Images.InfoBeamCentralSample); colormap(jet(256)); colorbar; title('Images.InfoBeamCentralSample')
% figure; imagesc(DataSeabed.InfoBeamCentralSample-Images.InfoBeamCentralSample); colormap(jet(256)); colorbar; title('DataSeabed.InfoBeamCentralSample - Images.InfoBeamCentralSample')      

%% Entries % TODO JMA : OK mais correction sp�cumaire � faire

% DataSeabed.Entries

[X, Y2] = get_MRZ_Samples(this);

Images.InfoBeamNbSamples = double(get_MRZ_SInumSamples(this));
nbSamplesEntries = sum(Images.InfoBeamNbSamples(:));
Signals.Entries = NaN(nbSamplesEntries, 1, 'single');
kDeb = 1;
for kPing=1:N
    s = X{kPing};
    n = length(s);
    kFin = kDeb + n - 1;
    Signals.Entries(kDeb:kFin) = s;
    kDeb = kDeb + n;
end
FieldUnits.Entries = 'dB';
% figure; plot(Signals.Entries); grid on; title('Signals.Entries')
% figure; plot(DataSeabed.Entries-Signals.Entries); grid on; title('DataSeabed.Entries - Signals.Entries')      
% my_figure; my_plot(DataSeabed.Entries); grid on; hold on; my_plot(Signals.Entries, '.r');

%% InfoBeamNbSamples : OK

FieldUnits.InfoBeamNbSamples = '';
% figure; imagesc(DataSeabed.InfoBeamNbSamples); colormap(jet(256)); colorbar; title('DataSeabed.InfoBeamNbSamples')
% figure; imagesc(Images.InfoBeamNbSamples); colormap(jet(256)); colorbar; title('Images.InfoBeamNbSamples')
% figure; imagesc(DataSeabed.InfoBeamNbSamples-Images.InfoBeamNbSamples); colormap(jet(256)); colorbar; title('DataSeabed.InfoBeamNbSamples - Images.InfoBeamNbSamples')      

%% Cr�ation du XML

XML.Title         = 'SeabedImage59h';
XML.Constructor   = 'Kongsberg';
XML.EmModel       = double(this.sDataKmAll.dgmMRZ(1).Header.echoSounderId);
XML.TimeOrigin    = '01/01/-4713';
XML.Comments      = 'Sounder ping rate';
XML.Version       = 'V2';
XML.FormatVersion = 20101118;
XML.IdentAlgoSnippets2OneValuePerBeam = 2;

XML.Dimensions.nbPings          = N;
XML.Dimensions.nbBeams          = nbBeams;
XML.Dimensions.nbSounders       = 1; % TODO !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
XML.Dimensions.nbSamplesEntries = nbSamplesEntries;

% Dans datagramme Installation SN=10001 ?
XML.ListeSystemSerialNumber = double(this.sDataKmAll.dgmMRZ(1).Header.systemId);

Fields = fields(Signals);
Fields = Fields(~contains(Fields, 'Datetime')); 
for k=1:(length(Fields)-1)
    XML.Signals(k).Dimensions.Name      = 'nbPings';
    XML.Signals(k).Dimensions.Length    = N;
    XML.Signals(k).Dimensions.Unlimited = 0;
    
    Values  = Signals.(Fields{k});
    Storage = class(Values);
    switch Storage
        case 'cl_time'
            Storage = 'double';
            Unit    = 'days since JC';
            Values  = Values.timeMat;
        otherwise
            Unit = FieldUnits.(Fields{k});
    end

    XML.Signals(k).Name      = Fields{k};
    XML.Signals(k).Storage   = Storage;
    XML.Signals(k).Unit      = Unit;
    XML.Signals(k).Populated = 1;
    XML.Signals(k).Stats     = stats(Values);
end

k = length(Fields);
XML.Signals(k).Dimensions.Name      = 'nbSamplesEntries';
XML.Signals(k).Dimensions.Length    = nbSamplesEntries;
XML.Signals(k).Dimensions.Unlimited = 0;

Values  = Signals.(Fields{k});
Storage = class(Values);
Unit    = FieldUnits.(Fields{k});

XML.Signals(k).Name      = Fields{k};
XML.Signals(k).Storage   = Storage;
XML.Signals(k).Unit      = Unit;
XML.Signals(k).Populated = 1;
XML.Signals(k).Stats     = stats(Values);


Fields = fields(Images);
for k=1:length(Fields)
    XML.Images(k).Dimensions.Name      = 'nbPings';
    XML.Images(k).Dimensions.Length    = N;
    XML.Images(k).Dimensions.Unlimited = 0;
    
    XML.Images(k).Dimensions(2).Name      = 'nbBeams';
    XML.Images(k).Dimensions(2).Length    = nbBeams;
    XML.Images(k).Dimensions(2).Unlimited = 0;
    
    Values  = Images.(Fields{k});
    Storage = class(Values);
    switch Storage
        case 'cl_time'
            Storage = 'double';
            Unit    = 'days since JC';
            Values  = Values.timeMat;
        case 'datetime'
            continue
        otherwise
            Unit = FieldUnits.(Fields{k});
    end
    
    XML.Images(k).Name      = Fields{k};
    XML.Images(k).Storage   = Storage;
    XML.Images(k).Unit      = Unit;
    XML.Images(k).Populated = 1;
    XML.Images(k).Stats     = stats(Values);
    
    %{
    try
        for k2=1:length(DataPosition.Images)
            if strcmp(XML.Images(k2).Name, DataPosition.Images(k2).Name)
                break
            end
        end
        fprintf('XML.Images(%d).Unit = ''%s'';\n', k2, DataPosition.Images(k2).Unit);
    catch
        XML.Images(k).Name;
    end
    %}
end

FieldsXML  = fields(XML);
for k=1:length(FieldsXML)
    Data.(FieldsXML{k}) = XML.(FieldsXML{k});
end

FieldsData = fields(Signals);
for k=1:length(FieldsData)
    Data.(FieldsData{k}) = Signals.(FieldsData{k});
end

FieldsData = fields(Images);
for k=1:length(FieldsData)
    Data.(FieldsData{k}) = Images.(FieldsData{k});
end

flag = 1;
