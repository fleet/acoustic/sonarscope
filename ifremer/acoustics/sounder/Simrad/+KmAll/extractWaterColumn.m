function [flag, XML, Data] = extractWaterColumn(this)

%{
Variables ayant disparu dans les .kmall :
    xxx

Nouvelles variables :
    xxx
%}

%{
DataWaterColumn = 
Title: 'WaterColumn'
Constructor: 'Kongsberg'
EmModel: 304
ListeSystemSerialNumber: 160
TimeOrigin: '01/01/-4713'
Comments: 'Sounder ping rate'
FormatVersion: 20101118

                 Dimensions: [1�1 struct]
nbPings: 84
nbRx: 512
nbSounders: 1
nbTx: 8
nbSamplesWC: 66719614

                    Signals: [1�18 struct]
                     Images: [1�7 struct]

Time: [1�1 cl_time]
Datetime: [84�1 datetime]
PingCounter: [84�1 single]
SystemSerialNumber: [84�1 single]
NumberOfDatagrams: [84�1 single]
DatagramNumbers: [84�1 single]
        TotalOfReceiveBeams: [84�1 single]
                        NRx: [84�1 single]
SoundSpeed: [84�1 single]
SamplingFreq: [84�1 single]
                TxTimeHeave: [84�1 single]
TVGFunctionApplied: [84�1 single]
TVGOffset: [84�1 single]
idebAmp: [84�1 double]
FlagPings: [84�1 uint8]
                  TiltAngle: [84�8 single]
CentralFrequency: [84�8 single]
TransmitSectorNumberTx: [84�8 single]

BeamPointingAngle: [84�512 single]
StartRangeNumber: [84�512 single]
NumberOfSamples: [84�512 single]
DetectedRangeInSamples: [84�512 single]
TransmitSectorNumber: [84�512 single]
               RxBeamNumber: [84�512 single]
              DatagramOrder: [84�512 single]
Amplitude: [66719614�1 int8]
%}
    
XML  = [];
Data = [];

if ~isfield(this.sDataKmAll, 'dgmMWC')
    flag = 1;
    return
end

%% Time et Datetime : OK

Signals.Datetime = get_MWC_Datetime(this); % Attente retour Roger
Signals.Time = cl_time('timeMat', datenum(Signals.Datetime));

N = length(Signals.Datetime);

%% PingCounter : OK

Signals.PingCounter = get_MRZ_PingCnt(this, 'dgmName', 'MWC');
FieldUnits.PingCounter = '';
% figure; plot(Signals.PingCounter); grid on;

%% SystemSerialNumber % TODO GLU

EmModel = double(this.sDataKmAll.dgmMRZ(1).Header.echoSounderId); % MWC
Signals.SystemSerialNumber = repmat(single(EmModel), N, 1); % TODO : trouver o� r�cup�rer cette info !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
FieldUnits.SystemSerialNumber = '';

%% NumberOfDatagrams % TODO : est-ce que le convertisseur kmall2Xsf agr�ge les datagrammes multiples si rencontr�s .

Signals.NumberOfDatagrams = get_MRZ_NumOfDgms(this, 'dgmName', 'MWC');
% Signals.NumberOfDatagrams = ones(N, 1, 'single');
FieldUnits.NumberOfDatagrams = '';

%% DatagramNumbers

Signals.DatagramNumbers = get_MRZ_DgmNum(this, 'dgmName', 'MWC');
% Signals.DatagramNumbers = ones(N, 1, 'single');
FieldUnits.DatagramNumbers = '';

%% SoundSpeed : OK

Signals.SoundSpeed = get_MWC_SoundVelocity(this);
FieldUnits.SoundSpeed = 'm/s';
% figure; plot(Signals.SoundSpeed); grid on;

%% SamplingFreq % Ok mais tester si NaN pour savoir s'il y a WC

Signals.SamplingFreq = get_MWC_SampleFreq_Hz(this);
FieldUnits.SamplingFreq = 'Hz';
% figure; plot(Signals.SamplingFreq); grid on;

%% TxTimeHeave % TODO GLU : les valeurs ne correspondent pas

Signals.TxTimeHeave = get_MWC_heave_m(this);
FieldUnits.TxTimeHeave = 'm';
% my_figure; my_plot(Signals.TxTimeHeave); grid on;

%% TVGFunctionApplied : OK

Signals.TVGFunctionApplied = get_MWC_TVGfunctionApplied(this);
Signals.TVGFunctionApplied = single(Signals.TVGFunctionApplied);
FieldUnits.TVGFunctionApplied = '';
% figure; plot(Signals.TVGFunctionApplied, '.r'); grid on;

%% TVGOffset : OK

Signals.TVGOffset = get_MWC_TVGoffset_dB(this);
Signals.TVGOffset = single(Signals.TVGOffset);
FieldUnits.TVGOffset = '';
% figure; plot(Signals.TVGOffset, '.r'); grid on;

%% FlagPings

Signals.FlagPings = zeros(N, 1, 'single');
FieldUnits.FlagPings = '';

%% TiltAngle : TODO : pas les m�me valeurs 

Signals.TiltAngle = get_MWC_TiltAngleReTx_deg(this);
FieldUnits.TiltAngle = 'deg';
% figure; plot(Signals.TiltAngle); grid on;

nbTx = size(Signals.TiltAngle, 2);

%% CentralFrequency

Signals.CentralFrequency = get_MWC_CentreFreq_Hz(this);
FieldUnits.CentralFrequency = 'Hz';
% figure; plot(Signals.CentralFrequency); grid on;

%%  TransmitSectorNumberTx

Signals.TransmitSectorNumberTx = get_MWC_TxSectorNumb(this);
Signals.TransmitSectorNumberTx = single(Signals.TransmitSectorNumberTx);
FieldUnits.TransmitSectorNumberTx = 'Hz';
% figure; plot(Signals.TransmitSectorNumberTx); grid on;

%% StartRangeNumber : OK mais valeurs toutes � 0, difficile de savoir si c'est vraiment OK

% Amount of time during reception where samples are discarded. The number of discarded sample is given by blanking_interval*sample_interval

% sample_interval   = Hdf5Utils.read_value(Hdf5Data.Sonar.Beam_group1.sample_interval);
% blanking_interval = Hdf5Utils.read_value(Hdf5Data.Sonar.Beam_group1.blanking_interval);
Signals.StartRangeNumber = get_MWC_StartRangeSampleNum(this);
% Images.StartRangeNumber = single(blanking_interval' .* sample_interval);
FieldUnits.StartRangeNumber = 'sample';
% figure; imagesc(Images.StartRangeNumber); colormap(jet(256)); colorbar; title('Images.StartRangeNumber')


%% idebAmp

Signals.idebAmp = get_MWC_StartRangeSampleNum(this);
% Signals.idebAmp = [0; Signals.idebAmp(1:end-1)] + 1;
FieldUnits.idebAmp = '';
% figure; imagesc(Signals.idebAmp); colormap(jet(256)); colorbar; title('Signals.idebAmp');

%% NumberOfSamples

Images.NumberOfSamples = get_MWC_NumSampleData(this);
FieldUnits.NumberOfSamples = '';
% figure; imagesc(Images.NumberOfSamples); colormap(jet(256)); colorbar; title('Images.NumberOfSamples');

%% Amplitude

% TODO : finaliser apr�s retour Roger

nbSamplesWC = sum(double(Images.NumberOfSamples(:)));
Amplitude = zeros(nbSamplesWC, 1, 'int8');
kDeb = 1;
for kPing=1:N
    buffer = get_MWC_Amplitude(this, kPing);
    nbBeams = size(buffer, 2);
    for kBeam=1:nbBeams
        kd = Signals.idebAmp(kPing,kBeam);
        n = double(Images.NumberOfSamples(kPing,kBeam));
        s = buffer(kd+1:kd+n, kPing);
        kFin = kDeb + n - 1;
%         fprintf('%d  %d  %d  %d  %d\n', kDeb, kFin, kd, n, kFin-kDeb+1)
        Amplitude(kDeb:kFin) = 2 * s;
        kDeb = kDeb + n;
    end
end
FieldUnits.Amplitude = '';
% figure; plot(Amplitude); grid on; title('Amplitude')

%% TotalOfReceiveBeams

Signals.TotalOfReceiveBeams = repmat(single(nbBeams), N, 1);
FieldUnits.TotalOfReceiveBeams = '';
% figure; plot(Signals.TotalOfReceiveBeams); grid on; title('Signals.TotalOfReceiveBeams')

%% NRx: [84�1 single]

Signals.NRx = repmat(single(nbBeams), N, 1);
FieldUnits.NRx = '';
% figure; plot(Signals.NRx); grid on; title('Signals.NRx')

%% BeamPointingAngle : OK

Images.BeamPointingAngle = get_MWC_BeamPointAngReVertical_deg(this);
FieldUnits.BeamPointingAngle = 'deg';
% figure; imagesc(Images.BeamPointingAngle); colormap(jet(256)); colorbar; title('Images.BeamPointingAngle')

nbRx = size(Images.BeamPointingAngle, 2);

%% DetectedRangeInSamples : OK

Images.DetectedRangeInSamples = get_MWC_DetectedRangeInSamples(this);
Images.DetectedRangeInSamples = single(Images.DetectedRangeInSamples);
FieldUnits.DetectedRangeInSamples = 'sample';
% figure; imagesc(Images.DetectedRangeInSamples); colormap(jet(256)); colorbar; title('Images.DetectedRangeInSamples')

%% TransmitSectorNumber : OK

Images.TransmitSectorNumber = get_MWC_BeamTxSectorNum(this);
Images.TransmitSectorNumber = single(Images.TransmitSectorNumber);
FieldUnits.TransmitSectorNumber = '';
% figure; imagesc(Images.TransmitSectorNumber); colormap(jet(256)); colorbar; title('Images.TransmitSectorNumber')

%% RxBeamNumber

%{
Images.RxBeamNumber = Hdf5Utils.read_value(Hdf5Data.Sonar.Beam_group1.Vendor_specific.raw_receive_transducer_index);
Images.RxBeamNumber = single(Images.RxBeamNumber');
%}
Images.RxBeamNumber = repmat(single(1:nbBeams), N, 1);
FieldUnits.RxBeamNumber = '';
% figure; imagesc(DataWaterColumn.RxBeamNumber); colormap(jet(256)); colorbar; title('DataSeabed.RxBeamNumber')
% figure; imagesc(Images.RxBeamNumber); colormap(jet(256)); colorbar; title('Images.RxBeamNumber')
% figure; imagesc(DataWaterColumn.RxBeamNumber-Images.RxBeamNumber); colormap(jet(256)); colorbar; title('DataWaterColumn.RxBeamNumber - Images.RxBeamNumber')      

%% DatagramOrder

Images.DatagramOrder = ones(N, nbBeams, 'single');
FieldUnits.DatagramOrder = '';

%% Cr�ation du XML

XML.Title         = 'WaterColumn';
XML.Constructor   = 'Kongsberg';
XML.EmModel       = double(this.sDataKmAll.dgmMWC(1).Header.echoSounderId);
XML.TimeOrigin    = '01/01/-4713';
XML.Comments      = 'Sounder ping rate';
XML.Version       = 'V2';
XML.FormatVersion = 20101118;
XML.IdentAlgoSnippets2OneValuePerBeam = 2;

XML.Dimensions.nbPings     = N;
XML.Dimensions.nbTx        = nbTx;
XML.Dimensions.nbRx        = nbRx;
XML.Dimensions.nbSounders  = 1;
XML.Dimensions.nbSamplesWC = nbSamplesWC;

XML.ListeSystemSerialNumber = double(this.sDataKmAll.dgmMWC(1).Header.systemId);

Signals.Amplitude = Amplitude;
Fields = fields(Signals);
Fields = Fields(~contains(Fields, 'Datetime')); 
for k=1:(length(Fields)-1)
    XML.Signals(k).Dimensions.Name      = 'nbPings';
    XML.Signals(k).Dimensions.Length    = N;
    XML.Signals(k).Dimensions.Unlimited = 0;
    
    Values  = Signals.(Fields{k});
    Storage = class(Values);
    switch Storage
        case 'cl_time'
            Storage = 'double';
            Unit    = 'days since JC';
            Values  = Values.timeMat;
        otherwise
            Unit = FieldUnits.(Fields{k});
    end
        
    if size(Values, 2) == nbTx
        XML.Signals(k).Dimensions(2).Name      = 'nbTx';
        XML.Signals(k).Dimensions(2).Length    = nbTx;
        XML.Signals(k).Dimensions(2).Unlimited = 0;
    end
    
    XML.Signals(k).Name      = Fields{k};
    XML.Signals(k).Storage   = Storage;
    XML.Signals(k).Unit      = Unit;
    XML.Signals(k).Populated = 1;
    XML.Signals(k).Stats     = stats(Values);
end

k = length(Fields);
XML.Signals(k).Dimensions.Name      = 'nbSamplesWC';
XML.Signals(k).Dimensions.Length    = nbSamplesWC;
XML.Signals(k).Dimensions.Unlimited = 0;

Values  = Signals.(Fields{k});
Storage = class(Values);
Unit    = FieldUnits.(Fields{k});

XML.Signals(k).Name      = Fields{k};
XML.Signals(k).Storage   = Storage;
XML.Signals(k).Unit      = Unit;
XML.Signals(k).Populated = 1;
XML.Signals(k).Stats     = stats(Values);

Fields = fields(Images);
for k=1:length(Fields)
    XML.Images(k).Dimensions.Name      = 'nbPings';
    XML.Images(k).Dimensions.Length    = N;
    XML.Images(k).Dimensions.Unlimited = 0;
    
    XML.Images(k).Dimensions(2).Name      = 'nbRx';
    XML.Images(k).Dimensions(2).Length    = nbRx;
    XML.Images(k).Dimensions(2).Unlimited = 0;
    
    Values  = Images.(Fields{k});
    Storage = class(Values);
    switch Storage
        case 'cl_time'
            Storage = 'double';
            Unit    = 'days since JC';
            Values  = Values.timeMat;
        case 'datetime'
            continue
        otherwise
            Unit = FieldUnits.(Fields{k});
    end
    
    XML.Images(k).Name      = Fields{k};
    XML.Images(k).Storage   = Storage;
    XML.Images(k).Unit      = Unit;
    XML.Images(k).Populated = 1;
    XML.Images(k).Stats     = stats(Values);
    
    %{
    try
        for k2=1:length(DataPosition.Images)
            if strcmp(XML.Images(k2).Name, DataPosition.Images(k2).Name)
                break
            end
        end
        fprintf('XML.Images(%d).Unit = ''%s'';\n', k2, DataPosition.Images(k2).Unit);
    catch
        XML.Images(k).Name;
    end
    %}
end

FieldsXML  = fields(XML);
for k=1:length(FieldsXML)
    Data.(FieldsXML{k}) = XML.(FieldsXML{k});
end

FieldsData = fields(Signals);
for k=1:length(FieldsData)
    Data.(FieldsData{k}) = Signals.(FieldsData{k});
end

FieldsData = fields(Images);
for k=1:length(FieldsData)
    Data.(FieldsData{k}) = Images.(FieldsData{k});
end

flag = 1;
