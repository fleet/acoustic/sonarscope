% nomDir = 'E:\SonarScopeAnnexes\SonarScopeDataArchive\Tests_Format_Kongsberg\KmAll';
% nomFic = fullfile(nomDir, '0005_EM2040_140_20160906_1354.kmall');
% nomFic = 'F:\Ridha\dataEM2040P\0038_20210609_105428_200kHz.kmall'
% nomFic = 'D:\XSF\FromKmall\0039_20180905_222154_raw.kmall'
% nomFic = 'D:\XSF\FromKmall\0001_20210806_112038.kmall'
% nomFic = 'D:\Temp\Ridha\XSF\KmallOnly\0097_20210814_175745.kmall'
% nomFic = 'E:\SPFE\20221010\TEST_FMPL_EM2040_22-600\aaaa\0008_20221012_122625.kmall'
% [flag, ncFileName]= KmAll.kmall2ssc(nomFic);

function [flag, ncFileName]= kmall2ssc(nomFic)

FormatVersion = 20160901;

% global MAX_SCL_DATALENGTH
% MAX_SCL_DATALENGTH = 64;

%% Test d'existence du fichier

flag = exist(nomFic, 'file');
if ~flag
    str1 = sprintf('Le fichier "%s" n''existe pas.', nomFic);
    str2 = sprintf('File "%s" does not exist.', nomFic);
    my_warndlg(Lang(str1,str2), 1);
    return
end

[nomDirRacine, nomFicSeul] = fileparts(nomFic);

%% Test d'existence du Cache complet

flag = [];
flag(end+1) = exist(nomDirRacine, 'dir');

nomDirRacine = fullfile(nomDirRacine, 'SonarScope', nomFicSeul);

flag(end+1) = exist(fullfile(nomDirRacine, 'Ssc_RunTime.xml'), 'file');
flag(end+1) = exist(fullfile(nomDirRacine, 'Ssc_RunTime'), 'dir');
  
if all(flag) && (getFormatVersion(nomDirRacine) >= FormatVersion)
    flag = 1;
    return
end

%% Cr�ation du r�pertoire cache

nomDirRacine = fileparts(nomFic);
nomDirRacine = fullfile(nomDirRacine, 'SonarScope');
if ~exist(nomDirRacine, 'dir')
    [flag, message] = mkdir(nomDirRacine);
    if ~flag
        messageErreurFichier(nomDirRacine, 'WriteFailure', 'Message', message);
        return
    end
end

%% Lecture en Matlab pour l'instant du fichier .kmall

a = ClKmall('Filename', nomFic);

flag = exist(nomFic, 'file');
if ~flag
    str1 = sprintf('Le fichier "%s" n''existe pas.', nomFic);
    str2 = sprintf('File "%s" does not exist.', nomFic);
    my_warndlg(Lang(str1,str2), 1);
    return
end

%% D�but d'interpr�tation des variables.

zRefDepth      = get_MRZ_Depth(a);
yRefDepth      = get_MRZ_AcrossDist(a);
xRefDepth      = get_MRZ_AlongDist(a);
txSectorNumber = get_MRZ_TxSectorNumber(a);
alpha          = get_MRZ_MeanAbsCoeff_dBPerkm(a);

figure; imagesc(zRefDepth); colormap(jet(256)); colorbar; title('Z (m)')
figure; 
subplot(2,1,1); imagesc(xRefDepth);      colormap(jet(256)); colorbar; title('AlongDist')
subplot(2,1,2); imagesc(txSectorNumber); colormap(jet(256)); colorbar; title('txSectorNumber')
figure; imagesc(yRefDepth);              colormap(jet(256)); colorbar; title('AcrossDist')
figure; imagesc(alpha);                  colormap(jet(256)); colorbar; title('alpha')

%% Cr�ation du fichier Netcdf

[flag, ncFileName] = SScCacheNetcdfUtils.createSScNetcdfFile(nomFic);
if ~flag
    return
end

%% Transfert des donn�es Runtime

[flag, XML, DataRuntimeKmAll] = KmAll.extractRuntime(a);
if ~flag
    return
end

flag = NetcdfUtils.XMLBin2Netcdf(XML, DataRuntimeKmAll, ncFileName, 'Runtime');
if ~flag
    return
end

%% Datagrammes InstallationParameters

[flag, DataInstallParamsKmAll] = KmAll.extractInstallParameters(a);
if ~flag
    return
end

if ~isempty(DataInstallParamsKmAll)
    flag = NetcdfUtils.XMLBin2Netcdf(DataInstallParamsKmAll, DataInstallParamsKmAll, ncFileName, 'InstallationParameters');
    if ~flag
        return
    end
end

%% Datagrammes Attitude

[flag, XML, DataAttitudeKmAll] = KmAll.extractAttitude(a);
if ~flag
    return
end

flag = NetcdfUtils.XMLBin2Netcdf(XML, DataAttitudeKmAll, ncFileName, 'Attitude');
if ~flag
    return
end

%% Datagrammes Clock

[flag, XML, Data] = KmAll.extractClock(a);
if ~flag
    return
end

flag = NetcdfUtils.XMLBin2Netcdf(XML, Data, ncFileName, 'Clock');
if ~flag
    return
end

%% Datagrammes SVP

[flag, XML, Data] = KmAll.extractSVP(a);
if ~flag
    return
end

flag = NetcdfUtils.XMLBin2Netcdf(XML, Data, ncFileName, 'SoundSpeedProfile');
if ~flag
    return
end

%% Datagrammes Position

[flag, XML, Data] = KmAll.extractPosition(a);
if ~flag
    return
end

flag = NetcdfUtils.XMLBin2Netcdf(XML, Data, ncFileName, 'Position');
if ~flag
    return
end

%% Datagrammes Raw : En cours de v�rification

[flag, XML, DataRawKmAll] = KmAll.extractRawRangeBeamAngle(a);
if ~flag
    return
end

flag = NetcdfUtils.XMLBin2Netcdf(XML, DataRawKmAll, ncFileName, 'RawRangeBeamAngle');
if ~flag
    return
end

%% Datagrammes Seabed : TODO v�rification

[flag, XML, DataSeabedKmAll] = KmAll.extractSeabedImage(a, DataRuntimeKmAll);
if ~flag
    return
end

flag = NetcdfUtils.XMLBin2Netcdf(XML, DataSeabedKmAll, ncFileName, 'SeabedImage59h');
if ~flag
    return
end

%% Datagrammes Depth : TODO v�rification

[flag, XML, Data] = KmAll.extractDepth(a, DataAttitudeKmAll, DataSeabedKmAll, DataRawKmAll);
if ~flag
    return
end

flag = NetcdfUtils.XMLBin2Netcdf(XML, Data, ncFileName, 'Depth');
if ~flag
    return
end

%% Datagrammes WaterColumn : bug dans NetcdfUtils.XMLBin2Netcdf pour l'instant : TODO v�rification

[flag, XML, Data] = KmAll.extractWaterColumn(a);
if ~flag
    return
end

%{
if ~isempty(XML)
    flag = NetcdfUtils.XMLBin2Netcdf(XML, Data, ncFileName, 'WaterColumn');
    if ~flag
        return
    end
end

%% Datagrammes Height

% Cr�ation du cache � partir du .kmall (pas branch� tant que tous les XSF.extractXxxx ne sont pas termin�s)
flag = NetcdfUtils.XMLBin2Netcdf(XML, Data, ncFileName, 'Height');
if ~flag
    return
end
%}

%% Cr�ation des attributs globaux

attID = -1; % netcdf.getConstant('GLOBAL');
ncID = netcdf.open(ncFileName, 'WRITE');
netcdf.putAtt(ncID, attID, 'Constructor',         'Kongsberg');
netcdf.putAtt(ncID, attID, 'EmModel',             304);
netcdf.putAtt(ncID, attID, 'SystemSerialNumber',  160);
netcdf.putAtt(ncID, attID, 'DatagramVersion',     'V2');
netcdf.putAtt(ncID, attID, 'DatagramSousVersion', '');
netcdf.putAtt(ncID, attID, 'Software',            'SonarScope');
netcdf.putAtt(ncID, attID, 'Support',             'sonarscope@ifremer.fr');
netcdf.close(ncID);

my_rename(ncFileName, strrep(ncFileName, '.NC', '.nc'));

% Version = 'V2';
%}





function FormatVersion = getFormatVersion(nomDirRacine)

nomFicXml = fullfile(nomDirRacine, 'Ssc_Sample.xml');
flag = exist(nomFicXml, 'file');        
if ~flag
    special_message('FichierNonExistant', nomFicXml)
    return
end
% edit(nomFicXml)
Datagrams = xml_read(nomFicXml);
if ~isfield(Datagrams, 'FormatVersion')
    FormatVersion = 0;
    return
end
FormatVersion = Datagrams.FormatVersion;
