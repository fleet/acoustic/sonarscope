function [flag, sDgmData] = readDgmCHE(fid, dgmHeader, varargin)

[varargin, revFormat] = getPropertyValue(varargin, 'RevFormat', 'E'); %#ok<ASGLU>

sInfoDgm.E.nbField   = 1;
sInfoDgm.E.typeField = {'single'};
sInfoDgm.E.nameField = {'heave_m'};

[flag, sDgmDataBody] = KmAll.readDgmMBody(fid, 'RevFormat', revFormat);
if ~flag
    return
end
sDgmData.Body = sDgmDataBody;

% Pas de modif.
sInfoDgm.H = sInfoDgm.E;
sInfoDgm.I = sInfoDgm.H; % Ajout JMA le 17/10/2022 % TODO GLU : y a t'il des modifs à faire ?

sInfoDgm   = sInfoDgm.(revFormat);                    

[flag, sDgmData.dgmCHE] = KmAll.readDgmGeneric(fid, sInfoDgm);
if ~flag
    return
end

sDgmData.Header = dgmHeader;
