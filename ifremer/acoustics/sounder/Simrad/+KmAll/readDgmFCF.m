function [flag, sDgmData] = readDgmFCF(fid, nbBytes, dgmHeader, varargin)

[varargin, revFormat] = getPropertyValue(varargin, 'RevFormat', 'E'); %#ok<ASGLU>

sDgmData.Header = dgmHeader;

%% Lecture du datagramme Partition

[flag, sDgmDataPartition] = KmAll.readDgmMPartition(fid, 'RevFormat', revFormat);
if ~flag
    return
end
sDgmData.Partition = sDgmDataPartition;

%% Lecture du datagramme FCommon

[flag, sDgmDataFCommon] = KmAll.readDgmFCommon(fid, nbBytes, 'RevFormat', revFormat);
if ~flag
    return
end
sDgmData.FCommon = sDgmDataFCommon;
