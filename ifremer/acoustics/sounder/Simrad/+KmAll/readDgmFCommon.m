function [flag, dgmData] = readDgmFCommon(fid, nbBytes, varargin)

[varargin, revFormat] = getPropertyValue(varargin, 'RevFormat', 'E'); %#ok<ASGLU>

sInfoDgm.E.typeField = {'uint16', 'int8', 'uint8', 'uint32'};
sInfoDgm.E.nameField = {'numBytesCmnPart', 'fileStatus', 'padding1', 'numBytesFile'};
sInfoDgm.E.nbField   = [1, 1, 1, 1];

% Pas de modif.
sInfoDgm.H = sInfoDgm.E;
sInfoDgm.I = sInfoDgm.H; % Ajout JMA le 17/10/2022 % TODO GLU : y a t'il des modifs à faire ?

sInfoDgm        = sInfoDgm.(revFormat);                    
sInfoDgm.name   = [];
[flag, dgmData] = KmAll.readDgmGeneric(fid, sInfoDgm);

% Lecture du nom de fichier
dgmData.filename        = fread(fid, int32(dgmData.numBytesCmnPart) - 8, '*char');

% Backscatter calibration file. (origine du + 2 ???? mais ca fonctionne sur le fichier )
dgmData.bsCalibrationFile = fread(fid, double(nbBytes) - double(dgmData.numBytesCmnPart) + 2, '*char');

flag = 1;