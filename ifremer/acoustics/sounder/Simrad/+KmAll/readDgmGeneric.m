function [flag, sDgmData] = readDgmGeneric(fid, infoField)

flag = 0; %#ok<NASGU>

% Calcul de la taille du buffer
for k=numel(infoField.typeField):-1:1
    switch infoField.typeField{k}
        case {'uint8'; 'int8'}
             nbBytes = 1;
             
        case {'uint16'; 'int16'}
             nbBytes = 2;
             
        case {'uint32'; 'int32';'single'}
             nbBytes = 4;
             
        case {'uint64'; 'int64'; 'double'}
             nbBytes = 8;
             
        otherwise
            fprintf('readDgmGeneric "%s" : TODO\n', infoField.typeField{k});
            nbBytes = sizeOf(zeros(1,1,infoField.typeField{k}));
    end
    sizeVar(k) = nbBytes * infoField.nbField(k);
end
sizeBuf = sum(sizeVar(:));

% Lecture du buffer et transformation dans les champs cibles
buf = fread(fid, sizeBuf, 'uint8=>uint8');

%% Traitement du buffer pour affectation des champs.

for k=1:numel(infoField.typeField)
    if k == 1
        iBeg = 1;
        iEnd = sizeVar(k);
    else
        iBeg = iBeg + sizeVar(k-1);
        iEnd = iEnd + sizeVar(k);
    end
    sDgmData.(infoField.nameField{k}) = typecast(buf(iBeg:iEnd), infoField.typeField{k});

%{
    % Test d'amélioration des performances
    tmp = buf(iBeg:iEnd);
    switch infoField.typeField{k}
        case 'uint8'
            sDgmData.(infoField.nameField{k}) = buf(iBeg:iEnd);
        case 'uint16'
            tmp = uint16(reshape(tmp, 2, []));
            sDgmData.(infoField.nameField{k}) = tmp(1,:) + tmp(2,:)*256;
        case 'uint32'
            tmp = uint32(reshape(tmp, 4, []));
            sDgmData.(infoField.nameField{k}) = tmp(1,:) + tmp(2,:)*256 + tmp(3,:)*65536 + tmp(4,:)*16777216;
%         case {'single'; 'double'}
%             sDgmData.(infoField.nameField{k}) = typecast(buf(iBeg:iEnd), infoField.typeField{k});
        otherwise
            sDgmData.(infoField.nameField{k}) = typecast(buf(iBeg:iEnd), infoField.typeField{k});
    end
%}

    if (infoField.nbField(k) > 1) && (strcmpi(infoField.typeField{k}, 'uint8') || strcmpi(infoField.typeField{k}, 'int8'))
        % Traitement du tableau d'octets comme tableau de char
        sDgmData.(infoField.nameField{k}) = char(sDgmData.(infoField.nameField{k})');
    end
end
flag = 1;
