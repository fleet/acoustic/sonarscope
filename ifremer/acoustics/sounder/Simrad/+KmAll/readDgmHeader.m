% Reading KmALL raw data file datagram header

function [flag, dgmHeader] = readDgmHeader(fid)

% Pas d'identification du format lors de la 1ere lecture de DgmHeader 
sInfoDgm.nbField   = [1, 4, 1, 1, 1, 1, 1];
sInfoDgm.typeField = {'uint32', 'uint8', 'uint8', 'uint8', 'uint16', 'uint32', 'uint32'};
sInfoDgm.nameField = {'numBytesDgm', 'dgmType', 'dgmVersion', 'systemId', 'echoSounderId', 'time_sec', 'time_nanosec'};

[flag, dgmHeader] = KmAll.readDgmGeneric(fid, sInfoDgm);
if ~flag
    return
end

dJRef          = datenum('19700101_000000', 'yyyymmdd_HHMMSS');
nbSecPerDay    = 86400;
dateSecRef0000 = dJRef * nbSecPerDay + ...
                 double(dgmHeader.time_sec) + 1.e-9 * double(dgmHeader.time_nanosec);
dateJulianMeas = dateSecRef0000 / nbSecPerDay;

dgmHeader.Datetime = datestr(dateJulianMeas);
