function [flag, sDgmData] = readDgmIIP(fid, nbBytes, dgmHeader)

sDgmData.Header = dgmHeader;

sDgmData.numBytesDgm = fread(fid, 1, 'uint16');
sDgmData.info        = fread(fid, 1, 'uint16');
sDgmData.status      = fread(fid, 1, 'uint16');

sDgmData.install_txt = fread(fid, nbBytes, 'uint8');
sDgmData.install_txt = char(sDgmData.install_txt');

% Identification du format du fichier (à partir de la Rev H)
sDgmData.revFormat = 'E';
[a, b, c] = regexp(sDgmData.install_txt, 'KMALL:Rev .'); %#ok<ASGLU>
if ~isempty(b)
    revFormat          = sDgmData.install_txt(b);
    sDgmData.revFormat = revFormat;
end

flag = 1;
