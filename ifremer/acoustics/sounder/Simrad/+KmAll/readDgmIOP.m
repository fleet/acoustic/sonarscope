function [flag, sDgmData] = readDgmIOP(fid, nbBytes, dgmHeader)

sDgmData.Header = dgmHeader;

sDgmData.numBytesDgm = fread(fid, 1, 'uint16');
sDgmData.info        = fread(fid, 1, 'uint16');
sDgmData.status      = fread(fid, 1, 'uint16');

sDgmData.runtime_txt = fread(fid, nbBytes, 'uint8');
sDgmData.runtime_txt = char(sDgmData.runtime_txt');

flag = 1;
