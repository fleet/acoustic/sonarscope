function [flag, sDgmData] = readDgmMBody(fid, varargin)

[varargin, revFormat] = getPropertyValue(varargin, 'RevFormat', 'E'); %#ok<ASGLU>

sInfoDgm.E.typeField = {'uint16', 'uint16', 'uint8', 'uint8', 'uint8', ...
                        'uint8', 'uint8', 'uint8', 'uint8', 'uint8'};
sInfoDgm.E.nameField = {'numBytesCmnPart', 'pingCnt', 'rxFansPerPing', 'rxFanIndex', 'swathsPerPing', ...
                        'swathAlongPosition', 'txTransducerInd', 'rxTransducerInd', 'numRxTransducers', 'algorithmType'};
                    
% Pas de modif.
sInfoDgm.H = sInfoDgm.E;
sInfoDgm.I = sInfoDgm.H; % Ajout JMA le 17/10/2022 % TODO GLU : y a t'il des modifs � faire ?

%% Lecture g�n�rique des infos

sInfoDgm         = sInfoDgm.(revFormat);                    
sInfoDgm.name    = [];
sInfoDgm.nbField = ones(1,numel(sInfoDgm.typeField));
[flag, sDgmData] = KmAll.readDgmGeneric(fid, sInfoDgm);
