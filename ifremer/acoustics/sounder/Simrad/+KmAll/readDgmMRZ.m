function [flag, sDgmData] = readDgmMRZ(fid, dgmHeader, varargin)

[varargin, revFormat] = getPropertyValue(varargin, 'RevFormat', 'E'); %#ok<ASGLU>

sDgmData.Header = dgmHeader;

%% Lecture du datagramme Partition

[flag, sDgmDataPartition] = KmAll.readDgmMPartition(fid, 'RevFormat', revFormat);
if ~flag
    return
end
sDgmData.Partition = sDgmDataPartition;

%% Lecture du datagramme Body

[flag, sDgmDataBody] = KmAll.readDgmMBody(fid, 'RevFormat', revFormat);
if ~flag
    return
end
sDgmData.Body = sDgmDataBody;

%% Lecture du datagramme Ping Info

[flag, sDgmDataPingInfo] = KmAll.readDgmMRZPingInfo(fid, 'RevFormat', revFormat);
if ~flag
    return
end
sDgmData.PingInfo = sDgmDataPingInfo;

%% Lecture du datagramme TxSectorInfo

for k=1:sDgmData.PingInfo.TxSectorInfo.numTxSectors
    [flag, sDgmDataTxSectorInfo] = KmAll.readDgmMRZTxSectorInfo(fid, 'RevFormat', revFormat);
    if ~flag
        return
    end
    sDgmData.TxSectorInfo(k) = sDgmDataTxSectorInfo;
end

%% Lecture du datagramme Rx Info

[flag, sDgmDataRxInfo] = KmAll.readDgmMRZRxInfo(fid, 'RevFormat', revFormat);
if ~flag
    return
end
sDgmData.RxInfo = sDgmDataRxInfo;

%% Lecture du datagramme Extra Detection classes

for k=1:sDgmData.RxInfo.numExtraDetectionClasses
    [flag, sDgmDataExtDetClassInfo] = KmAll.readDgmMRZExtDetClassInfo(fid, 'RevFormat', revFormat);
    if ~flag
        return
    end
    sDgmData.ExtDetClassInfo(k) = sDgmDataExtDetClassInfo;
end

%% Lecture du datagramme Sounding

nbSounding = sDgmData.RxInfo.numExtraDetectionClasses + sDgmData.RxInfo.numSoundingsMaxMain; 
for k=1:nbSounding
    [flag, sDgmDataSounding] = KmAll.readDgmMRZSounding(fid, 'RevFormat', revFormat);
    if ~flag
        return
    end
    sDgmData.Sounding(k) = sDgmDataSounding;
end

% Calcul du nombre de Samples � lire pour ce datagramme.
listNbSamples = arrayfun(@(x) (x.SeabedImage.SInumSamples), sDgmData.Sounding);

SISamples_desidB = fread(fid, sum(listNbSamples), 'int16=>int16');
sDgmData.Samples = SISamples_desidB;
