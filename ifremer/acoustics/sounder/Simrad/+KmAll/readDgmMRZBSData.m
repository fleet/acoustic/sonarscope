function [flag, sDgmData] = readDgmMRZBSData(fid, varargin)

[varargin, revFormat] = getPropertyValue(varargin, 'RevFormat', 'E'); %#ok<ASGLU>

%% Lecture des champs de d�buts de datagramme.

sInfoDgm.E.typeField = {'single','single','single',...
                        'single','single','single', ...
                        'single'};
sInfoDgm.E.nameField = {'meanAbsCoeff_dBPerkm','reflectivity1_dB','reflectivity2_dB',...
                        'receiverSensitivityApplied_dB','sourceLevelApplied_dB','BScalibration_dB',...
                        'TVG_dB'};

% Pas de modif.
sInfoDgm.H = sInfoDgm.E;
sInfoDgm.I = sInfoDgm.H; % Ajout JMA le 17/10/2022 % TODO GLU : y a t'il des modifs � faire ?

sInfoDgm         = sInfoDgm.(revFormat);                    
sInfoDgm.name    = [];
sInfoDgm.nbField = ones(1,numel(sInfoDgm.typeField));
[flag, sDgmData] = KmAll.readDgmGeneric(fid, sInfoDgm);
