function [flag, sDgmData] = readDgmMRZDetectionInfo(fid, varargin)

[varargin, revFormat] = getPropertyValue(varargin, 'RevFormat', 'E'); %#ok<ASGLU>

%% Lecture des champs de d�buts de datagramme.

sInfoDgm.E.typeField = {'uint8','uint8','uint8','uint8','uint8', ...
                        'uint8','uint8','uint16', 'single','single',...
                        'single','single','single','single'};

sInfoDgm.E.nameField = {'detectionType','detectionMethod','rejectionInfo1','rejectionInfo2','postProcessingInfo',...
                        'detectionClass','detectionConfidenceLevel','padding','rangeFactor','qualityFactor',...
                        'detectionUncertaintyVer_m','detectionUncertaintyHor_m','detectionWindowLength_sec','echoLength_sec'};

% Pas de modif.
sInfoDgm.H = sInfoDgm.E;
sInfoDgm.I = sInfoDgm.H; % Ajout JMA le 17/10/2022 % TODO GLU : y a t'il des modifs � faire ?

sInfoDgm         = sInfoDgm.(revFormat);                    
sInfoDgm.name    = [];
sInfoDgm.nbField = ones(1,numel(sInfoDgm.typeField));
[flag, sDgmData] = KmAll.readDgmGeneric(fid, sInfoDgm);
