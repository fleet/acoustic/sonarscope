function [flag, sDgmData] = readDgmMRZGeoDepthPts(fid, varargin)

[varargin, revFormat] = getPropertyValue(varargin, 'RevFormat', 'E'); %#ok<ASGLU>

sInfoDgm.E.typeField = {'single', 'single', 'single', 'single', 'single', ...
                        'single', 'uint16'};

sInfoDgm.E.nameField = {'deltaLatitude_deg', 'deltaLongitude_deg', 'z_reRefPoint_m', 'y_reRefPoint_m', 'x_reRefPoint_m', ...
                        'beamIncAngleAdj_deg', 'realTimeCleanInfo'};

% Pas de modif.
sInfoDgm.H = sInfoDgm.E;
sInfoDgm.I = sInfoDgm.H; % Ajout JMA le 17/10/2022 % TODO GLU : y a t'il des modifs � faire ?

%% Lecture g�n�rique des infos.

sInfoDgm         = sInfoDgm.(revFormat);                    
sInfoDgm.name    = [];
sInfoDgm.nbField = ones(1,numel(sInfoDgm.typeField));
[flag, X]        = KmAll.readDgmGeneric(fid, sInfoDgm);
if ~flag
    return
end
sDgmData = X;
