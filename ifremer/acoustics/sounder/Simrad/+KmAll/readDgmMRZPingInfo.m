function [flag, sDgmData] = readDgmMRZPingInfo(fid, varargin)

[varargin, revFormat] = getPropertyValue(varargin, 'RevFormat', 'E'); %#ok<ASGLU>

%% Lecture des champs de d�buts de datagramme.

sInfoDgm.E.typeField = {'uint16', 'uint16'};
sInfoDgm.E.nameField = { 'numBytesInfoData', 'padding0'};

% Pas de modif.
sInfoDgm.H = sInfoDgm.E;
sInfoDgm.I = sInfoDgm.H; % Ajout JMA le 17/10/2022 % TODO GLU : y a t'il des modifs � faire ?

sInfoDgm         = sInfoDgm.(revFormat);
sInfoDgm.name    = [];
sInfoDgm.nbField = ones(1,numel(sInfoDgm.typeField));
[flag, sDgmData] = KmAll.readDgmGeneric(fid, sInfoDgm);
if ~flag
    return
end

%% Traitement du Ping Info

sInfoDgm.E.name = 'PingInfo';
sInfoDgm.E.typeField = {'single', 'uint8', 'uint8', 'uint8', 'uint8', ...
    'uint8',  'uint8',  'uint16', 'single', 'single', ...
    'single', 'single', 'single', 'single', 'single', ...
    'single', 'single', 'single', 'single', 'int16', ...
    'int16',  'uint8',  'uint8',  'uint16', 'uint32', ...
    'single', 'single', 'single', 'uint16', 'uint16', ...
    'single'};
sInfoDgm.E.nameField = {'pingRate_Hz', 'beamSpacing', 'depthMode', 'subDepthMode', 'distanceBtwSwath', ...
    'detectionMode', 'pulseForm', 'padding1', 'frequencyMode_Hz', 'freqRangeLowLim_Hz', ...
    'freqRangeHighLim_Hz', 'maxTotalTxPulseLength_sec', 'maxEffTxPulseLength_sec', 'maxEffTxBandWidth_Hz', 'absCoeff_dBPerkm', ...
    'portSectorEdge_deg', 'starbSectorEdge_deg', 'portMeanCov_deg', 'starbMeanCov_deg', 'portMeanCov_m', ...
    'starbMeanCov_m', 'modeAndStabilisation', 'runtimeFilter1', 'runtimeFilter2', 'pipeTrackingStatus', ...
    'transmitArraySizeUsed_deg', 'receiveArraySizeUsed_deg', 'transmitPower_dB', 'SLrampUpTimeRemaining', ...
    'padding2', 'yawAngle_deg'};

% TODO GLU : ajout JMA le 24/06/2021
sInfoDgm.H.name = 'PingInfo';
sInfoDgm.H.typeField = { 'single', 'uint8', 'uint8', 'uint8', 'uint8', ...
    'uint8',  'uint8',  'uint16', 'single', 'single',  ...
    'single', 'single', 'single', 'single', 'single', ...
    'single', 'single', 'single', 'single', 'int16', ...
    'int16',  'uint8',  'uint8',  'uint16', 'uint32', ...
    'single', 'single', 'single', 'uint16', 'uint16', ...
    'single'};
sInfoDgm.H.nameField = {'pingRate_Hz', 'beamSpacing', 'depthMode', 'subDepthMode', 'distanceBtwSwath', ...
    'detectionMode', 'pulseForm', 'padding1', 'frequencyMode_Hz', 'freqRangeLowLim_Hz', ...
    'freqRangeHighLim_Hz', 'maxTotalTxPulseLength_sec', 'maxEffTxPulseLength_sec', 'maxEffTxBandWidth_Hz', 'absCoeff_dBPerkm', ...
    'portSectorEdge_deg', 'starbSectorEdge_deg', 'portMeanCov_deg', 'starbMeanCov_deg', 'portMeanCov_m', ...
    'starbMeanCov_m', 'modeAndStabilisation', 'runtimeFilter1', 'runtimeFilter2', 'pipeTrackingStatus', ...
    'transmitArraySizeUsed_deg', 'receiveArraySizeUsed_deg', 'transmitPower_dB', 'SLrampUpTimeRemaining', ...
    'padding2', 'yawAngle_deg'};

sInfoDgm.I = sInfoDgm.H; % Ajout JMA le 17/10/2022 % TODO GLU : y a t'il des modifs � faire ?

sInfoDgm         = sInfoDgm.(revFormat);
sInfoDgm.nbField = ones(1, numel(sInfoDgm.typeField));
[flag, X] = KmAll.readDgmGeneric(fid, sInfoDgm);
if ~flag
    return
end
sDgmData.General = X;

%% Lecture du datagramme TxSector Info

sInfoDgm.name      = 'TxSectorInfo';
sInfoDgm.typeField = {'uint16', 'uint16'};
sInfoDgm.nameField = { 'numTxSectors', 'numBytesPerTxSector'};

sInfoDgm.nbField = ones(1, numel(sInfoDgm.typeField));
[flag, X] = KmAll.readDgmGeneric(fid, sInfoDgm);
if ~flag
    return
end
sDgmData.TxSectorInfo = X;

%% Lecture du datagramme TxTime1stPulse

[flag, sDgmData.TxTime1stPulse] = KmAll.readDgmMRZTxTime1stPulse(fid, 'RevFormat', revFormat);
if ~flag
    return
end

%% Lecture de donn�es additionnelles

if contains(revFormat, 'H') || contains(revFormat, 'I') % Ajout JMA le 17/10/2022
    sInfoDgm.typeField = {'single', 'uint8', 'uint8', 'uint16'};
    sInfoDgm.nameField = {'bsCorrectionOffset_dB', 'lambertsLawApplied', 'iceWindow', 'activeModes'};
    
    sInfoDgm.name         = [];
    sInfoDgm.nbField      = ones(1,numel(sInfoDgm.typeField));
    [flag, sDgmMRZParams] = KmAll.readDgmGeneric(fid, sInfoDgm);
    if ~flag
        return
    end
    
    Fields = fieldnames(sDgmMRZParams);
    nbFields = length(Fields);
    for k=1:nbFields
        sDgmData.General.(Fields{k}) = sDgmMRZParams.(Fields{k});
    end
end
