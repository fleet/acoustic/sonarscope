function [flag, sDgmData] = readDgmMRZRxInfo(fid, varargin)

[varargin, revFormat] = getPropertyValue(varargin, 'RevFormat', 'E'); %#ok<ASGLU>

%% Lecture des champs de d�buts de datagramme.

sInfoDgm.E.typeField = {'uint16','uint16','uint16','uint16','single', ...
                        'single','single','single','uint16','uint16',...
                        'uint16','uint16'};

sInfoDgm.E.nameField = {'numBytesRxInfo','numSoundingsMaxMain','numSoundingsValidMain','numBytesPerSounding','WCSampleRate',...
                        'seabedImageSampleRate','BSnormal_dB','BSoblique_dB','extraDetectionAlarmFlag','numExtraDetections',...
                        'numExtraDetectionClasses','numBytesPerClass'};

% Pas de modif.
sInfoDgm.H = sInfoDgm.E;
sInfoDgm.I = sInfoDgm.H; % Ajout JMA le 17/10/2022 % TODO GLU : y a t'il des modifs � faire ?

sInfoDgm         = sInfoDgm.(revFormat);                    
sInfoDgm.name    = [];
sInfoDgm.nbField = ones(1,numel(sInfoDgm.typeField));
[flag, sDgmData] = KmAll.readDgmGeneric(fid, sInfoDgm);
