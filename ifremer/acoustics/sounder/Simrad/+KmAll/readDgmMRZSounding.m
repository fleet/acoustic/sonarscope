function [flag, sDgmData] = readDgmMRZSounding(fid, varargin)

[varargin, revFormat] = getPropertyValue(varargin, 'RevFormat', 'E'); %#ok<ASGLU>

%% Lecture des champs de d�buts de datagramme.

sInfoDgm.typeField = {'uint16', 'uint8'};
sInfoDgm.nameField = {'soundingIndex', 'txSectorNumber'};

% Pas de modif.

sInfoDgm.name    = [];
sInfoDgm.nbField = ones(1,numel(sInfoDgm.typeField));
[flag, sDgmData] = KmAll.readDgmGeneric(fid, sInfoDgm);
if ~flag
    return
end

%% Traitement du Detection Info

[flag, pppp] = KmAll.readDgmMRZDetectionInfo(fid, 'RevFormat', revFormat);
if ~flag
    return
end
sDgmData.DetInfo = pppp;

%% Lecture du datagramme WC Parameters

sInfoDgm.name      = 'WCParams';
sInfoDgm.typeField = {'uint16', 'uint16', 'single'};
sInfoDgm.nameField = {'WCBeamNumber', 'WCRangeSamples', 'WCNomBeamAngleAcross'};

sInfoDgm.nbField = ones(1,numel(sInfoDgm.typeField));
[flag, pppp]     = KmAll.readDgmGeneric(fid, sInfoDgm);
if ~flag
    return
end

sDgmData.(sInfoDgm.name) = pppp;

%% Lecture du datagramme BSData

[flag, sDgmDataBSData] = KmAll.readDgmMRZBSData(fid, 'RevFormat', revFormat);
if ~flag
    return
end
sDgmData.BSData = sDgmDataBSData;

%% Lecture du datagramme Range and angle Data

sInfoDgm.name      = 'RangeAngleData';
sInfoDgm.typeField = {'single', 'single', 'single', 'single'};
sInfoDgm.nameField = {'beamAngleReRx_deg', 'beamAngleReRxCorrection_deg', 'twoWayTravelTime_sec', 'twoWayTravelTimeCorrection_sec'};

sInfoDgm.nbField = ones(1,numel(sInfoDgm.typeField));
[flag, X] = KmAll.readDgmGeneric(fid, sInfoDgm);
if ~flag
    return
end
sDgmData.(sInfoDgm.name) = X;

%% Lecture du datagramme Georeferenced Depth Points.

[flag, sDgmDataGeoDZPts] = KmAll.readDgmMRZGeoDepthPts(fid, 'RevFormat', revFormat);
if ~flag
    my_breakpoint
end
sDgmData.GeoDepthPts = sDgmDataGeoDZPts;

%% Lecture du datagramme Seabed Image

sInfoDgm.name      = 'SeabedImage';
sInfoDgm.typeField = {'uint16', 'uint16', 'uint16'};
sInfoDgm.nameField = {'SIstartRange_samples', 'SIcentreSample', 'SInumSamples'};

sInfoDgm.nbField = ones(1,numel(sInfoDgm.typeField));
[flag, X] = KmAll.readDgmGeneric(fid, sInfoDgm);
if ~flag
    return
end
sDgmData.(sInfoDgm.name) = X;
