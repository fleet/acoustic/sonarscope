function [flag, sDgmData] = readDgmMRZTxTime1stPulse(fid, varargin)

[varargin, revFormat] = getPropertyValue(varargin, 'RevFormat', 'E'); %#ok<ASGLU>

%% Lecture des champs de d�buts de datagramme.

sInfoDgm.E.typeField = {'single', 'single', 'single', 'single', 'single',...
                        'single', 'uint8',  'uint8',  'uint8',  'uint8',...
                        'double', 'double', 'single'};
sInfoDgm.E.nameField = {'headingVessel_deg', 'soundSpeedAtTxDepth_mPerSec', 'txTransducerDepth_m', 'z_waterLevelReRefPoint_m', ...
                        'x_kmallToall_m', 'y_kmallToall_m', 'latLongInfo', 'posSensorStatus',...
                        'attitudeSensorStatus', 'padding3', 'latitude_deg', 'longitude_deg', 'ellipsoidHeightReRefPoint_m'};
                    
% Pas de modif.
sInfoDgm.H = sInfoDgm.E;
sInfoDgm.I = sInfoDgm.H; % Ajout JMA le 17/10/2022 % TODO GLU : y a t'il des modifs � faire ?

sInfoDgm         = sInfoDgm.(revFormat);                    
sInfoDgm.name    = [];
sInfoDgm.nbField = ones(1,numel(sInfoDgm.typeField));
[flag, sDgmData] = KmAll.readDgmGeneric(fid, sInfoDgm);
