function [flag, sDgmData] = readDgmMWC(fid, dgmHeader, varargin)

[varargin, revFormat] = getPropertyValue(varargin, 'RevFormat', 'E'); %#ok<ASGLU>

sDgmData.Header = dgmHeader;

%% Lecture du datagramme Partition

[flag, sDgmDataPartition] = KmAll.readDgmMPartition(fid, 'RevFormat', revFormat);
if ~flag
    return
end
sDgmData.Partition = sDgmDataPartition;

%% Lecture du datagramme Body

[flag, sDgmDataBody] = KmAll.readDgmMBody(fid, 'RevFormat', revFormat);
if ~flag
    return
end
sDgmData.Body = sDgmDataBody;

%% Lecture du datagramme Tx Info

[flag, sDgmDataTxInfo]  = KmAll.readDgmMWCTxInfo(fid, 'RevFormat', revFormat);
if ~flag
    return
end
sDgmData.TxInfo = sDgmDataTxInfo;

%% Lecture du datagramme TxSectorData

for k=1:sDgmData.TxInfo.numTxSectors
    [flag, sDgmDataTxSectorData] = KmAll.readDgmMWCTxSectorData(fid, 'RevFormat', revFormat);
    if ~flag
        return
    end
    sDgmData.TxSectorData(k) = sDgmDataTxSectorData;
end

%% Lecture du datagramme Rx Info

[flag, sDgmDataRxInfo] = KmAll.readDgmMWCRxInfo(fid, 'RevFormat', revFormat);
if ~flag
    return
end
sDgmData.RxInfo = sDgmDataRxInfo;

%% Lecture du datagramme Beam Data

[flag, sDgmDataRxBeam]  = KmAll.readDgmMWCRxBeamData(fid, sDgmData.RxInfo.numBeams, sDgmDataRxInfo.phaseFlag, 'RevFormat', revFormat);
if ~flag
    return
end
sDgmData.RxBeamData = sDgmDataRxBeam;

