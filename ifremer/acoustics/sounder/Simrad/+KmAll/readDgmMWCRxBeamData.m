function [flag, sDgmData] = readDgmMWCRxBeamData(fid, nbRxBeams, phaseFlag, varargin)

[varargin, revFormat] = getPropertyValue(varargin, 'RevFormat', 'E'); %#ok<ASGLU>

%% Lecture des champs de d�buts de datagramme.

sInfoDgm.E.name      = 'WCRxBeamData';
sInfoDgm.E.typeField = {'single','uint16','uint16',...
                        'uint16','uint16'};

sInfoDgm.E.nameField = {'beamPointAngReVertical_deg', 'startRangeSampleNum', 'detectedRangeInSamples',...
                        'beamTxSectorNum', 'numSampleData'}; % 'sampleAmplitude05dB'};
                    
sInfoDgm.H.name      = 'WCRxBeamData';
sInfoDgm.H.typeField = {'single','uint16','uint16',...
                        'uint16','uint16', 'single'};

sInfoDgm.H.nameField = {'beamPointAngReVertical_deg', 'startRangeSampleNum', 'detectedRangeInSamples',...
                        'beamTxSectorNum', 'numSampleData', 'detectedRangeInSamplesHighResolution'}; % 'sampleAmplitude05dB'};

% pppp = repmat([], 1, 5);
% sDgmData.Samples        = struct(sInfoDgm.nameField);
sInfoDgm         = sInfoDgm.(revFormat);                    
sInfoDgm.nbField = ones(1,numel(sInfoDgm.typeField));
for k=1:nbRxBeams
    [flag, pppp]     = KmAll.readDgmGeneric(fid, sInfoDgm);
    if ~flag
        return
    end
    
    % Lecture du nombre d'octets pour chaque beam.
    % numBytesPerBeamEntry  = fread(fid, 1, 'uint8');
    
    % Read sample amplitude
    WCSamples_desidB    = fread(fid, pppp.numSampleData, 'int8');
    pppp.Amplitude      = WCSamples_desidB;
    
    switch phaseFlag
        case 1
            % Read sample phase
            pppp.Phase   = fread(fid, pppp.numSampleData, 'int8');

        case 2
            % Read sample phase
            pppp.Phase = fread(fid, pppp.numSampleData, 'int16');
    end
    sDgmData(k) = pppp;
end
