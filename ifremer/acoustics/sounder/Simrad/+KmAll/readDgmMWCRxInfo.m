function [flag, sDgmData] = readDgmMWCRxInfo(fid, varargin)

[varargin, revFormat] = getPropertyValue(varargin, 'RevFormat', 'E'); %#ok<ASGLU>

%% Lecture des champs de d�buts de datagramme.

sInfoDgm.E.typeField = {'uint16', 'uint16', 'uint8', 'uint8', 'uint8', ...
                        'int8', 'single', 'single'};

sInfoDgm.E.nameField  = {'numBytesRxInfo', 'numBeams', 'numBytesPerBeamEntry', 'phaseFlag', 'TVGfunctionApplied',...
                         'TVGoffset_dB', 'sampleFreq_Hz', 'soundVelocity_mPersect'};

% Pas de modif.
sInfoDgm.H = sInfoDgm.E;
sInfoDgm.I = sInfoDgm.H; % Ajout JMA le 17/10/2022 % TODO GLU : y a t'il des modifs � faire ?

sInfoDgm         = sInfoDgm.(revFormat);                    
sInfoDgm.name    = [];
sInfoDgm.nbField = ones(1,numel(sInfoDgm.typeField));
[flag, sDgmData] = KmAll.readDgmGeneric(fid, sInfoDgm);
