function [flag, sDgmData] = readDgmSCL(fid, nbBytesDgm, dgmHeader, varargin) % Datagram Clock

[varargin, revFormat] = getPropertyValue(varargin, 'RevFormat', 'E'); %#ok<ASGLU>

sDgmData.Header = dgmHeader;

%% Lecture du datagramme Common

[flag, sDgmCommon] = KmAll.readDgmSCommon(fid, 'RevFormat', revFormat);
if ~flag
    return
end
sDgmData.Common = sDgmCommon;

%% Lecture du datagramme SCLDataSensor

sizeOfDgm     = 8; % sizeof (avant les Datas)
sizeDgmCommon = 8;

sizeBytesDataSensor = double(nbBytesDgm) - sizeDgmCommon - sizeOfDgm;  % 8 pour les octets lus avant les data

[flag, sDgmSCLDataSensor] = KmAll.readDgmSCLDataSensor(fid, sizeBytesDataSensor, 'RevFormat', revFormat);
if ~flag
    return
end

sDgmData.DataSensor = sDgmSCLDataSensor;
