function [flag, sDgmData] = readDgmSCLDataSensor(fid, nbBytes, varargin)

[varargin, revFormat] = getPropertyValue(varargin, 'RevFormat', 'E'); %#ok<ASGLU>

sInfoDgm.E.nbField   = [1, 1, nbBytes];
sInfoDgm.E.typeField = {'single', 'int32', 'uint8'};
sInfoDgm.E.nameField = {'offset_sec', 'clockdevPU_nanosec', 'dataFromSensor'};

% Pas de modif.
sInfoDgm.H = sInfoDgm.E;
sInfoDgm.I = sInfoDgm.H; % Ajout JMA le 17/10/2022 % TODO GLU : y a t'il des modifs � faire ?

%% Lecture g�n�rique des infos

sInfoDgm = sInfoDgm.(revFormat);                    

[flag, sDgmData] = KmAll.readDgmGeneric(fid, sInfoDgm);
