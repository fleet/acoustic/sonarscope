function [flag, sDgmData] = readDgmSKM(fid, dgmHeader, varargin) % Attitude

[varargin, revFormat] = getPropertyValue(varargin, 'RevFormat', 'E'); %#ok<ASGLU>

sDgmData.Header = dgmHeader;

%% Lecture du datagramme Common

[flag, sDgmData.SKMInfo] = KmAll.readDgmSKMInfo(fid, 'RevFormat', revFormat);
if ~flag
    return
end

%% Lecture du datagramme SKMSamples

nbSensors = sDgmData.SKMInfo.numSamplesArray; 
for k=1:nbSensors
    [flag, X] = KmAll.readDgmSKMSamples(fid, 'RevFormat', revFormat);
    if ~flag
        return
    end
    sDgmData.SKMSamples(k) = X;
end
