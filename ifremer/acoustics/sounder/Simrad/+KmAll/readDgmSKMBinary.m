function [flag, sDgmData] = readDgmSKMBinary(fid, varargin)

[varargin, revFormat] = getPropertyValue(varargin, 'RevFormat', 'E'); %#ok<ASGLU>

revFormat = 'E'; %Ajout JMA le 19/08/21 pour éviter bug

%% Champs racines de KMBinary

sInfoDgm.E.nbField   = [4, 1, 1, 1, 1, 1];
sInfoDgm.E.typeField = {'uint8', 'uint16', 'uint16', 'uint32', 'uint32', 'uint32'};
sInfoDgm.E.nameField = {'dgmType', 'numBytesDgm', 'dgmVersion', 'time_sec', 'time_nanosec', 'status'};

% Pas de modif.
sInfoDgm.H = sInfoDgm.E;
sInfoDgm.I = sInfoDgm.H; % Ajout JMA le 17/10/2022 % TODO GLU : y a t'il des modifs à faire ?

sInfoDgm         = sInfoDgm.(revFormat);                    
sInfoDgm.name    = [];
[flag, sDgmData] = KmAll.readDgmGeneric(fid, sInfoDgm);
if ~flag
    return
end

%% Champs position de KMBinary

sInfoDgm.E.typeField = {'double', 'double', 'single'};
sInfoDgm.E.nameField = {'latitude_deg', 'longitude_deg', 'ellipsoidHeight_m'};

sInfoDgm         = sInfoDgm.(revFormat);                    
sInfoDgm.name    = [];
sInfoDgm.nbField = ones(1,numel(sInfoDgm.typeField));
[flag, sDgmData.Position] = KmAll.readDgmGeneric(fid, sInfoDgm);
if ~flag
    return
end

%% Champs Attitude de KMBinary

sInfoDgm.E.typeField = {'single', 'single', 'single', 'single'};
sInfoDgm.E.nameField = {'roll_deg', 'pitch_deg', 'heading_deg', 'heave_m'};

sInfoDgm         = sInfoDgm.(revFormat);                    
sInfoDgm.name    = [];
sInfoDgm.nbField = ones(1,numel(sInfoDgm.typeField));
[flag, sDgmData.Attitude] = KmAll.readDgmGeneric(fid, sInfoDgm);
if ~flag
    return
end

%% Champs Rates de KMBinary

sInfoDgm.E.typeField = {'single', 'single', 'single'};
sInfoDgm.E.nameField = {'rollRate', 'pitchRate', 'yawRate'};

sInfoDgm         = sInfoDgm.(revFormat);                    
sInfoDgm.name    = [];
sInfoDgm.nbField = ones(1,numel(sInfoDgm.typeField));
[flag, sDgmData.Rates] = KmAll.readDgmGeneric(fid, sInfoDgm);
if ~flag
    return
end

%% Champs Velocities de KMBinary

sInfoDgm.E.typeField = {'single', 'single', 'single'};
sInfoDgm.E.nameField = {'velNorth', 'velEast', 'velDown'};

sInfoDgm         = sInfoDgm.(revFormat);                    
sInfoDgm.name    = [];
sInfoDgm.nbField = ones(1,numel(sInfoDgm.typeField));
[flag, sDgmData.Vel] = KmAll.readDgmGeneric(fid, sInfoDgm);
if ~flag
    return
end

%% Champs Errors in data. Sensor data quality, as standard deviations. de KMBinary

sInfoDgm.E.typeField = {'single', 'single', 'single', 'single', 'single', 'single', 'single'};
sInfoDgm.E.nameField = {'latitudeError_m', 'latitudeError_m', 'ellipsoidHeightError_m', ...
                        'rollError_deg', 'pitchError_deg', 'headingError_deg', 'heaveError_m'};

sInfoDgm             = sInfoDgm.(revFormat);                    
sInfoDgm.name        = [];
sInfoDgm.nbField     = ones(1,numel(sInfoDgm.typeField));
[flag, sDgmData.Err] = KmAll.readDgmGeneric(fid, sInfoDgm);
if ~flag
    return
end

%% Champs Acceleration de KMBinary

sInfoDgm.E.typeField = {'single', 'single', 'single'};
sInfoDgm.E.nameField = {'northAcceleration', 'eastAcceleration', 'downAcceleration'};

sInfoDgm             = sInfoDgm.(revFormat);                    
sInfoDgm.name        = [];
sInfoDgm.nbField     = ones(1,numel(sInfoDgm.typeField));
[flag, sDgmData.Acc] = KmAll.readDgmGeneric(fid, sInfoDgm);
if ~flag
    return
end
