function [flag, sDgmData] = readDgmSKMInfo(fid, varargin)

[varargin, revFormat] = getPropertyValue(varargin, 'RevFormat', 'E'); %#ok<ASGLU>

sInfoDgm.E.typeField = {'uint16', 'uint8', 'uint8', 'uint16', 'uint16', 'uint16', 'uint16'};
sInfoDgm.E.nameField = {'numBytesInfoPart', 'sensorSystem', 'sensorStatus', 'sensorInputFormat', ...
                        'numSamplesArray', 'numBytesPerSample', 'padding'};

% Pas de modif.
sInfoDgm.H = sInfoDgm.E;
sInfoDgm.I = sInfoDgm.H; % Ajout JMA le 17/10/2022 % TODO GLU : y a t'il des modifs � faire ?

%% Lecture g�n�rique des infos

sInfoDgm         = sInfoDgm.(revFormat);                    
sInfoDgm.name    = [];
sInfoDgm.nbField = ones(1,numel(sInfoDgm.typeField));
[flag, sDgmData] = KmAll.readDgmGeneric(fid, sInfoDgm);
if ~flag
    return
end
