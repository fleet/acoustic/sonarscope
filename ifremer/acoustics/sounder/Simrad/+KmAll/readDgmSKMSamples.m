function [flag, sDgmData] = readDgmSKMSamples(fid, varargin)

[varargin, revFormat] = getPropertyValue(varargin, 'RevFormat', 'E'); %#ok<ASGLU>

%% Sensor attitude data block. Data given timestamped, not corrected

[flag, sDgmData.KMBinary] = KmAll.readDgmSKMBinary(fid, 'RevFormat', revFormat);
if ~flag
    return
end

% Pour traduire les temps en dateJulian (ref : 0000-00-00) Date de fr� pour Kongsberg.
dJRef          = datenum('19700101_000000', 'yyyymmdd_HHMMSS');
nbSecPerDay    = 86400;
dateSecRef0000 = dJRef * nbSecPerDay + ...
                 double(sDgmData.KMBinary.time_sec) + 1.e-9 * double(sDgmData.KMBinary.time_nanosec);
dateJulianMeas = dateSecRef0000/nbSecPerDay;
dateMeasure    = datestr(dateJulianMeas); %#ok<NASGU>

%% delayed heave. Included if available from sensor

sInfoDgm.E.typeField = {'uint32', 'uint32', 'single'};
sInfoDgm.E.nameField = {'time_sec', 'time_nanosec', 'delayedHeave_m'};

% Pas de modif.
sInfoDgm.H = sInfoDgm.E;
sInfoDgm.I = sInfoDgm.H; % Ajout JMA le 17/10/2022 % TODO GLU : y a t'il des modifs � faire ?

% Lecture g�n�rique des infos

sInfoDgm         = sInfoDgm.(revFormat);                    
sInfoDgm.name    = [];
sInfoDgm.nbField = ones(1,numel(sInfoDgm.typeField));

[flag, sDgmData.KMdelayedHeave] = KmAll.readDgmGeneric(fid, sInfoDgm);
if ~flag
    return
end
