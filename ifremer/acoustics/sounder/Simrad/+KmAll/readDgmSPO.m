function [flag, sDgmData] = readDgmSPO(fid, nbBytesDgm, dgmHeader, varargin) % Datagram Clock

[varargin, revFormat] = getPropertyValue(varargin, 'RevFormat', 'E'); %#ok<ASGLU>

sDgmData.Header = dgmHeader;

%% Lecture du datagramme Common

[flag, sDgmCommon] = KmAll.readDgmSCommon(fid, 'RevFormat', revFormat);
if ~flag
    return
end
sDgmData.Common = sDgmCommon;

%% Lecture du datagramme SCLDataSensor

sizeOfDgm        = 40; % sizeof (avant les Datas)
sizeDgmCommon    = 8;
sizeBytesPosData = double(nbBytesDgm) - sizeDgmCommon - sizeOfDgm; % 40 pour les octets lus avant les data

[flag, sDgmSPODataBlock] = KmAll.readDgmSPODataBlock(fid, sizeBytesPosData, 'RevFormat', revFormat);
if ~flag
    return
end

sDgmData.DataSensor = sDgmSPODataBlock;
