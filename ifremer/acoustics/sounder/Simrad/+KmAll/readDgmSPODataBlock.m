function [flag, sDgmData] = readDgmSPODataBlock(fid, nbBytes, varargin)

[varargin, revFormat] = getPropertyValue(varargin, 'RevFormat', 'E'); %#ok<ASGLU>

sInfoDgm.E.nbField   = [1, 1, 1, 1, 1, 1, 1, 1, nbBytes];
sInfoDgm.E.typeField = {'uint32', 'uint32', 'single', 'double', 'double', ...
                        'single', 'single', 'single', 'int8'};

sInfoDgm.E.nameField = {'timeFromSensor_sec', 'timeFromSensor_nanosec', 'posFixQuality_m', 'correctedLat_deg', 'correctedLon_deg', ...
                        'speedOverGround_mPerSec', 'courseOverGround_deg', 'ellipsoidHeightReRefPoint_m', 'posDataFromSensor'};
% Pas de modif.
sInfoDgm.H = sInfoDgm.E;
sInfoDgm.I = sInfoDgm.H; % Ajout JMA le 17/10/2022 % TODO GLU : y a t'il des modifs � faire ?

%% Lecture g�n�rique des infos

sInfoDgm         = sInfoDgm.(revFormat);                    
sInfoDgm.name    = [];
[flag, sDgmData] = KmAll.readDgmGeneric(fid, sInfoDgm);
