function [flag, sDgmData] = readDgmSVP(fid, dgmHeader, varargin)

[varargin, revFormat] = getPropertyValue(varargin, 'RevFormat', 'E'); %#ok<ASGLU>

sInfoDgm.E.nbField   = [1, 1, 4, 1, 1, 1];
sInfoDgm.E.typeField = {'uint16', 'uint16', 'uint8', 'uint32', 'double', 'double'};
sInfoDgm.E.nameField = {'numBytesCmnPart', 'numSamples', 'sensorFormat', 'time_sec', 'latitude', 'longitude'};

% Pas de modif.
sInfoDgm.H = sInfoDgm.E;
sInfoDgm.I = sInfoDgm.H; % Ajout JMA le 17/10/2022 % TODO GLU : y a t'il des modifs à faire ?

sInfoDgm         = sInfoDgm.(revFormat);                    
[flag, sDgmData] = KmAll.readDgmGeneric(fid, sInfoDgm);
if ~flag
    return
end

for n=1:sDgmData.numSamples
    [flag, X] = KmAll.readDgmSVPPoint(fid,  'RevFormat', revFormat);
    if ~flag
        return
    end
    sDgmData.profil(n) = X;
end

sDgmData.Header = dgmHeader;
