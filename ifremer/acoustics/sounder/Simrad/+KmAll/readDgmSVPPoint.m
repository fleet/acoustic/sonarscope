function [flag, sDgmData] = readDgmSVPPoint(fid, varargin)

[varargin, revFormat] = getPropertyValue(varargin, 'RevFormat', 'E'); %#ok<ASGLU>

sInfoDgm.E.typeField = {'single', 'single', 'single', 'single', 'single'};
sInfoDgm.E.nameField = {'depth_m', 'soundVelocity_mPerSec', 'absCoeff_dBPerkm', 'temp_C', 'salinity'};

% Pas de modif.
sInfoDgm.H = sInfoDgm.E;
sInfoDgm.I = sInfoDgm.H; % Ajout JMA le 17/10/2022 % TODO GLU : y a t'il des modifs à faire ?

sInfoDgm         = sInfoDgm.(revFormat);                    
sInfoDgm.nbField = ones(1, numel(sInfoDgm.typeField));
[flag, sDgmData] = KmAll.readDgmGeneric(fid, sInfoDgm);
