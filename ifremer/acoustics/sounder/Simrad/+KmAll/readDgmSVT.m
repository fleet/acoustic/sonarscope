function [flag, sDgmData] = readDgmSVT(fid, dgmHeader, varargin)

[varargin, revFormat] = getPropertyValue(varargin, 'RevFormat', 'E'); %#ok<ASGLU>

sInfoDgm.E.typeField = {'uint16', 'uint16', 'uint16', 'uint16', 'uint16', 'uint16', 'single', 'single'};
sInfoDgm.E.nameField = {'numBytesInfoPart', 'sensorStatus', 'sensorInputFormat', 'numSamplesArray', 'numBytesPerSample', ...
                        'sensorDataContents', 'filterTime_sec', 'soundVelocity_mPerSec_offset'};

% Pas de modif.
sInfoDgm.H = sInfoDgm.E;
sInfoDgm.I = sInfoDgm.H; % Ajout JMA le 17/10/2022 % TODO GLU : y a t'il des modifs à faire ?

sInfoDgm                 = sInfoDgm.(revFormat);                    
sInfoDgm.nbField         = ones(1,numel(sInfoDgm.typeField));
[flag, sDgmData.SVTInfo] = KmAll.readDgmGeneric(fid, sInfoDgm);
if ~flag
    return
end

for k=1:sDgmData.SVTInfo.numSamplesArray
    [flag, X] = KmAll.readDgmSVTSample(fid,  'RevFormat', revFormat);
    if ~flag
        return
    end
    sDgmData.SVTSample(k) = X;
end

sDgmData.Header = dgmHeader;
