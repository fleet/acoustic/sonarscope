function [flag, sDgmData] = readDgmSVTSample(fid, varargin)

[varargin, revFormat] = getPropertyValue(varargin, 'RevFormat', 'E'); %#ok<ASGLU>

sInfoDgm.E.typeField = {'uint32', 'uint32', 'single', 'single', 'single', 'single'};
sInfoDgm.E.nameField = {'time_sec', 'time_nanosec', 'soundVelocity_mPerSec', 'temp_C', 'pressure_Pa', 'salinity'};

% Pas de modif.
sInfoDgm.H = sInfoDgm.E;
sInfoDgm.I = sInfoDgm.H; % Ajout JMA le 17/10/2022 % TODO GLU : y a t'il des modifs à faire ?

sInfoDgm         = sInfoDgm.(revFormat);                    
sInfoDgm.nbField = ones(1, numel(sInfoDgm.typeField));
[flag, sDgmData] = KmAll.readDgmGeneric(fid, sInfoDgm);
