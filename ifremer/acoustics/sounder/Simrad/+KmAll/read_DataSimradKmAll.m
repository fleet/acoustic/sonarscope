% Lecture d'un fichier Data Kongsberg au format kmall.
%
% Syntax
%    [flag, sData] = KmAll.read_DataSimradKmAll(nomFic)
%
% Name-Value Pair Arguments
%   nomFic : Nom(s) du fichier .raw
%
% Output Arguments
%   flag  : 1=OK, 0=KO
%   sData : Donn�es structur�es du fichier.
%
% Examples
%   nomFicRaw = 'E:\SonarScopeAnnexes\SonarScopeDataArchive\Tests_Format_Kongsberg\KmAll\0005_EM2040_140_20160906_1354.kmall';
%   [flag, sData] = KmAll.read_DataSimradKmAll(nomFicRaw);
%
%   % Format Rev E
%   nomFicRaw = 'E:\SonarScopeAnnexes\SonarScopeDataArchive\Tests_Format_Kongsberg\KmAll\0011_EM2040_140_20160906_1358.kmall';
%   [flag, sData] = KmAll.read_DataSimradKmAll(nomFicRaw);
%
%   % Format Rev H
%   nomFicRaw = 'E:\SonarScopeAnnexes\SonarScopeDataArchive\Tests_Format_Kongsberg\KmAll\0038_20210609_105428.kmall';
%   [flag, sData] = KmAll.read_DataSimradKmAll(nomFicRaw);
%
%   nomDir = 'E:\SonarScopeAnnexes\SonarScopeDataArchive\Tests_Format_Kongsberg\KmAll';
%   listFiles = dir(fullfile(nomDir, '*.kmall'));
%   for k=1:1% 2:numel(listFiles)
%       nomFic = fullfile(nomDir, listFiles(k).name);
%       [flag, sData] = KmAll.read_DataSimradKmAll(nomFic);
%   end
%
% See also cl_hac Authors
% Authors : GLU
% ----------------------------------------------------------------------------

function [flag, sData] = read_DataSimradKmAll(nomFic, varargin)

[varargin, flagReadData]  = getPropertyValue(varargin, 'FlagReadData', true); % Si false, on ne lit que les headers.
[varargin, revFormat]     = getPropertyValue(varargin, 'RevFormat',    'E');  %#ok<ASGLU> Default Revision format KMALL Kongsberg
% Identified in IIP (Install Parameters reading)

flag        = 0;
sData.index = [];

nbChannels   = [];
nbPings      = [];
nbSamplesMax = [];

flag = exist(nomFic, 'file');
if ~flag
    str1 = sprintf('Le fichier "%s" n''existe pas.', nomFic);
    str2 = sprintf('File "%s" does not exist.', nomFic);
    my_warndlg(Lang(str1,str2), 1);
    return
end

fid = fopen(nomFic, 'r');
if fid == -1
    messageErreurFichier(nomFic, 'ReadFailure');
    flag = 0;
    return
end

% Read file
[nomDir, nomFicSeul] = fileparts(nomFic);
pppp          = dir(nomFic);
nbOctetsTotal = pppp.bytes;
NbIter        = 1000;
pasWaitBar    = nbOctetsTotal/NbIter;

str = sprintf('Reading %s', nomFic);
hw = create_waitbar(str, 'N', NbIter);
% Initialisation de la Waitbar pour forcer les �carts lors du calcul de temporation.
my_waitbar(1, NbIter, hw);

listDatagram  = {'SVP', 'SCL', 'IIP','IOP', 'SHA', 'MRZ', 'SPO', 'SKM', 'MWC', 'CHE', 'CPO', 'SVT'};
% cntDgm      = zeros(1,numel(listDatagram));
labelDatagram = {'Sound Velocity Profile', 'Clock', 'Install Params.','Runtime Params.', ...
                 'Sensor Data Heading', 'MRZ', 'Data Position', 'KM Binary Format', 'Water Column', ...
                 'Compatibility heave sensor datagram', 'Compatibility position sensor datagram', ...
                 'Sound Velocity at Transducer'};
nbDgm = zeros(numel(listDatagram), 1);
szDgm = zeros(numel(listDatagram), 1);

while ~feof(fid) && ftell(fid) < nbOctetsTotal
    posCrt = ftell(fid);
    [flag, dgmHeader] = KmAll.readDgmHeader(fid); %#ok<*NASGU>
    if ~flag
        break
    end
    
    nbOctetsLus = ftell(fid);
    iWaitBar    = floor(nbOctetsLus/pasWaitBar);
    my_waitbar(iWaitBar, NbIter, hw);
    
    if feof(fid)
        break
    end
    
    % Comptage des nbs de datagrammes.
    idDgm = find(arrayfun(@(x) strcmp(x, dgmHeader.dgmType(2:end)),listDatagram));
    szDgm(idDgm) = szDgm(idDgm) + double(dgmHeader.numBytesDgm);
    nbDgm(idDgm) = nbDgm(idDgm) + 1;
    flagDgmRead  = true;
    
    sizeOfDgmHeader = 20; 	% Taille du dgmHeader
    sizeOfSizeDgm   = 4;    % sizeof(sizeDgm)
	sizeOfDgmParams = 6;    % Taille des datagrammes IIP et IOP sans le champ Texte;
    sData.index(end+1).dgmType = dgmHeader.dgmType;
    sData.index(end).crtPtr    = ftell(fid);
    
    switch dgmHeader.dgmType
        case '#SHA' % Lecture des datas de caract�risation des sensors
            % Imperatif pour la lecture de EMdgmSdataInfo
            disp('#SHA"')
            
        case '#CHE' % Compatibility (C) data for heave (HE) 
            if flagReadData
                [flag, sData.dgmCHE(nbDgm(idDgm))] = KmAll.readDgmCHE(fid, ...
                    dgmHeader, 'RevFormat', revFormat); %#ok<*NASGU>
                if ~flag
                    break
                end
             end
            
        case '#CPO' % Compatibility (C) data for position (PO)
            if flagReadData
                [flag, sData.dgmCPO((nbDgm(idDgm)))] = KmAll.readDgmCPO(fid, dgmHeader.numBytesDgm - sizeOfDgmHeader - sizeOfSizeDgm, ...
                    dgmHeader, 'RevFormat', revFormat); %#ok<*NASGU>
                if ~flag
                    break
                end
            end
            
        case '#FCF' % Backscatter calibration (C) file (F) datagram
            if flagReadData
                nbBytes              = dgmHeader.numBytesDgm - sizeOfDgmHeader - sizeOfDgmParams - sizeOfSizeDgm;
%                 [flag, sData.dgmFCF(nbDgm(idDgm))] = KmAll.readDgmFCF(fid, nbBytes, ...
                [flag, sData.dgmFCF] = KmAll.readDgmFCF(fid, nbBytes, dgmHeader, 'RevFormat', revFormat); %#ok<*NASGU>
                nomFicCalib          = fullfile(nomDir, [nomFicSeul '_dgmFCF_CalibFile.txt']);
                fidCalibFile         = fopen(nomFicCalib, 'w');
                fwrite(fidCalibFile, sData.dgmFCF.FCommon.bsCalibrationFile, 'uint8'); % TODO : 1 seulement ?
                fclose(fidCalibFile);
                if ~flag
                    break
                end
            end
            
        case '#IIP' % Installation parameters and sensor setup
            if flagReadData
                nbBytes = dgmHeader.numBytesDgm - sizeOfDgmHeader - sizeOfDgmParams - sizeOfSizeDgm;
                [flag, sData.dgmIIP(nbDgm(idDgm))] = KmAll.readDgmIIP(fid, nbBytes, ...
                    dgmHeader); %#ok<*NASGU>
                revFormat = sData.dgmIIP.revFormat;
                if ~flag
                    break
                end
            end
            
        case '#IOP' % Runtime parameters as chosen by operator
            if flagReadData
                nbBytes = dgmHeader.numBytesDgm - sizeOfDgmHeader - sizeOfDgmParams - sizeOfSizeDgm;
                [flag, sData.dgmIOP(nbDgm(idDgm))] = KmAll.readDgmIOP(fid, nbBytes, ...
                    dgmHeader); %#ok<*NASGU>
                if ~flag
                    break
                end
            end
            
        case '#SVP' % Sensor (S) data from sound velocity (V) profile (P) or CTD 
            if flagReadData
                [flag, sData.dgmSVP(nbDgm(idDgm))] = KmAll.readDgmSVP(fid, ...
                    dgmHeader, 'RevFormat', revFormat); %#ok<*NASGU>
                if ~flag
                    break
                end
            end
            
        case '#MRZ' % Multibeam (M) raw range (R) and depth(Z) datagram
            if flagReadData
                [flag, sData.dgmMRZ(nbDgm(idDgm))] = KmAll.readDgmMRZ(fid, ...
                    dgmHeader, 'RevFormat', revFormat); %#ok<*NASGU>
                if ~flag
                    break
                end
            end
            
        case '#SCL' % Clock
            if flagReadData
                [flag, sData.dgmSCL(nbDgm(idDgm))] = KmAll.readDgmSCL(fid, dgmHeader.numBytesDgm - sizeOfDgmHeader - sizeOfSizeDgm, ...
                    dgmHeader, 'RevFormat', revFormat); %#ok<*NASGU>
                if ~flag
                    break
                end
            end
            
        case '#SPO' % Position sensor
            if flagReadData
                [flag, sData.dgmSPO(nbDgm(idDgm))] = KmAll.readDgmSPO(fid, dgmHeader.numBytesDgm - sizeOfDgmHeader - sizeOfSizeDgm, ...
                    dgmHeader, 'RevFormat', revFormat); %#ok<*NASGU>
                if ~flag
                    break
                end
            end
            
        case '#SKM' % Data from attitude and attitude velocity sensors
            if flagReadData
                [flag, sData.dgmSKM((nbDgm(idDgm)))] = KmAll.readDgmSKM(fid, ...
                    dgmHeader, 'RevFormat', revFormat); %#ok<*NASGU>
                if ~flag
                    break
                end
            end
            
        case '#MWC' % Water column
            if flagReadData
                [flag, sData.dgmMWC((nbDgm(idDgm)))] = KmAll.readDgmMWC(fid, ...
                    dgmHeader, 'RevFormat', revFormat); %#ok<*NASGU>
                if ~flag
                    break
                end
                % val = [sData.dgmMWC.RxBeamData(:).beamPointAngReVertical_deg]
                % figure;
                % plot(1:numel(val),val);
            end
                        
        case '#SVT' % Sound Velocity at Transducer. 
            if flagReadData
                [flag, sData.dgmSVT(nbDgm(idDgm))] = KmAll.readDgmSVT(fid, ...
                    dgmHeader, 'RevFormat', revFormat); %#ok<*NASGU>
                if ~flag
                    break
                end
            end
            
        otherwise
            my_breakpoint
            flagDgmRead = false;
            [~, rootFic] = fileparts(nomFic);
            % On n'imprime qu'une fois.
            if nbDgm(idDgm) == 1
                fprintf('==> Datagram non-reconnu %s. TODO : Finaliser le balayage du fichier %s\n', dgmHeader.dgmType, rootFic);
            end
            if flagReadData
                % Indexation du ptr - 4 octects de taille de dgm.
                fseek(fid, posCrt + dgmHeader.numBytesDgm - 4, 'bof');
            end
    end
    
    % Lecture des 4 derniers octets ponctuant le dgm si il a �t� lu.
    if flagReadData
        sizeDgm = fread(fid, 1, 'int32');
        if sizeDgm ~= dgmHeader.numBytesDgm
            my_breakpoint
        end    
    end
    
    if ~flagReadData
        fseek(fid, posCrt + dgmHeader.numBytesDgm, 'bof');
    end
end
my_close(hw, 'MsgEnd', 'TimeDelay', 10);
fclose(fid);

%% Bilan des datagrammes

indexDgm          = 1:numel(nbDgm);
tabNbBytes        = szDgm * 10; % TODO GLU : pourquoi la multiplication par 10 ?
texteTypeDatagram = arrayfun(@(x,y,z) sprintf('%d : %s    -    %s', x, y{:}, z{:}), indexDgm, listDatagram, labelDatagram, 'UniformOutput', false);

sData.infoDatagrams.filename          = nomFicSeul;
sData.infoDatagrams.listDatagram      = listDatagram;
sData.infoDatagrams.nbDgm             = nbDgm;
sData.infoDatagrams.texteTypeDatagram = texteTypeDatagram;
sData.infoDatagrams.tabNbBytes        = tabNbBytes;

flag = 1;
