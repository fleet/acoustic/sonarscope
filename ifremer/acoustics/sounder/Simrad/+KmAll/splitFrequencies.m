% splitFrequenciesKmallFile(cl_simrad_all([]), 'F:\Ridha\dataEM2040P\0018_20210609_095616.kmall');
% Multi-frequences : 
% splitFrequenciesKmallFile(cl_simrad_all([]), 'F:\Ridha\dataEM2040P\0038_20210609_105428.kmall');

function splitFrequencies(~, nomFicKmall)

logFileId = getLogFileId;

if ~iscell(nomFicKmall)
    nomFicKmall = {nomFicKmall};
end

sizeOfDgmHeader = 20; 	% Taille du dgmHeader
sizeOfSizeDgm   = 4;    % sizeof(sizeDgm)
sizeOfDgmParams = 6;    % Taille des datagrammes IIP et IOP sans le champ Texte;

N = length(nomFicKmall);
str1 = 'S�paration des fr�quences des .all.';
str2 = 'Split frequencies .all files.';
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k1=1:N
    my_waitbar(k1, N, hw);
    
    msg = sprintf('Begin %s', nomFicKmall{k1});
    logFileId.info('splitAllFile', msg);
    
    % Lecture des index de fichiers.
    [flag, sData] = KmAll.read_DataSimradKmAll(nomFicKmall{k1}, 'FlagReadData', false, 'FlagPlotHisto', false);
    if ~flag
        continue
    end
    
    %% Identitification du format
    
    fid = fopen(nomFicKmall{k1}, 'r');
    if fid == - 1
        return
    end
    % Lecture du 1�re Datagram Header
    [flag, dgmHeader] = KmAll.readDgmHeader(fid);
    nbBytes = dgmHeader.numBytesDgm - sizeOfDgmHeader - sizeOfDgmParams - sizeOfSizeDgm;

    % Lecture du format dans le dgm IIP
    idx     = find(arrayfun(@(x) contains(x.dgmType, 'IIP'), sData.index));
    dataPtr = [sData.index(idx).crtPtr];
    fseek(fid, dataPtr(idx), 'bof');
    % Lecture du datagramme Partition
    [flag, sDgmDataIIP] = KmAll.readDgmIIP(fid, nbBytes); 
    if ~flag
        break
    end
    fclose(fid);
    revFormat = sDgmDataIIP.revFormat;
    
    %% Pointage vers les dgm TxSectorInfo pour identifier les fr�q uniques
    
    idx             = find(arrayfun(@(x) contains(x.dgmType, 'MRZ'), sData.index));
    dataPtr         = [sData.index(idx).crtPtr];
    listFrequencies = getListFrequencies(nomFicKmall{k1}, dataPtr, 'RevFormat', revFormat);
    if length(listFrequencies) == 1
        msg = sprintf('%s contains only one frequency', nomFicKmall{k1});
        logFileId.info('splitAllFile', msg);
        continue
    end
    
    splitFrequencies_bin(nomFicKmall{k1}, fix(listFrequencies), numel(dataPtr), 'RevFormat', revFormat);
    
    msg = sprintf('End %s', nomFicKmall{k1});
    logFileId.info('splitAllFile', msg);
end
my_close(hw, 'MsgEnd', 'TimeDelay', 60)



function listFrequencies = getListFrequencies(nomFicKmall, dataPtr, varargin)

[varargin, revFormat] = getPropertyValue(varargin, 'RevFormat', 'E'); %#ok<ASGLU>

listFrequencies = [];
fid = fopen(nomFicKmall, 'r');
if fid == - 1
    return
end

listFrequencies = [];
nbPingMRZ       = 0;
pppp            = dir(nomFicKmall);
nbOctetsTotal   = pppp.bytes;
NbIter          = 1000;
pasWaitBar      = nbOctetsTotal/NbIter;

hw = create_waitbar('Identify frequencies', 'N', NbIter);
my_waitbar(1, NbIter, hw); % Initialisation de la Waitbar pour forcer les �carts lors du calcul de temporation.
for k=1:numel(dataPtr)
    nbOctetsLus = ftell(fid);
    iWaitBar = floor(nbOctetsLus/pasWaitBar);
    my_waitbar(iWaitBar, NbIter, hw);
    
    fseek(fid, dataPtr(k), 'bof');
    nbPingMRZ = nbPingMRZ + 1;
    
    % Lecture du datagramme Partition
    [flag, sDgmDataPartition] = KmAll.readDgmMPartition(fid, 'RevFormat', revFormat); %#ok<ASGLU>
    if ~flag
        break
    end
    
    % Lecture du datagramme Body
    [flag, sDgmDataBody] = KmAll.readDgmMBody(fid, 'RevFormat', revFormat); %#ok<ASGLU>
    if ~flag
        break
    end
    
    % Lecture du datagramme Ping Info
    [flag, sDgmDataPingInfo]  = KmAll.readDgmMRZPingInfo(fid, 'RevFormat', revFormat);
    if ~flag
        break
    end
    
    % Lecture du datagramme TxSectorInfo
    for n=1:sDgmDataPingInfo.TxSectorInfo.numTxSectors
        [flag, X] = KmAll.readDgmMRZTxSectorInfo(fid, 'RevFormat', revFormat);
        if ~flag
            break
        end
        sDgmDataTxSectorInfo(n) = X; %#ok<AGROW>
    end
    
    % On ajoute que la 1�re Freq.
    listFrequencies = [listFrequencies 1e-3 * roundToSup(sDgmDataTxSectorInfo(1).centreFreq_Hz)]; %#ok<AGROW>
end
if ishandle(hw)
    my_close(hw, 'MsgEnd');
end
fclose(fid);

% Identification des fr�quences uniques.
listFrequencies = unique(listFrequencies);



function splitFrequencies_bin(nomFicKmall, listFrequencies, nbDatagrams, varargin)

[varargin, revFormat] = getPropertyValue(varargin, 'RevFormat', 'E'); %#ok<ASGLU>

nbFreq               = length(listFrequencies);
[pathname, filename] = fileparts(nomFicKmall);
for k=1:nbFreq
    filenameFreq = sprintf('%s_%dkHz', filename, listFrequencies(k));
    filenameFreq = fullfile(pathname, [filenameFreq '.kmall']);
    fidOut(k) = fopen(filenameFreq, 'w+'); %#ok<AGROW>
end

fid = fopen(nomFicKmall, 'r');
if fid == - 1
    return
end

fidBis = fopen(nomFicKmall, 'r');
if fidBis == - 1
    return
end

kDatagram   = 0;
hw          = create_waitbar(['Processing split :' filename], 'N', nbDatagrams);
position    = uint64(0);
nbPingMRZ   = 0;
pppp            = dir(nomFicKmall);
nbOctetsTotal   = pppp.bytes;

while ftell(fid) ~= nbOctetsTotal
    fseek(fid, position, 'bof');
    % Output loop if file ended.
    if ftell(fid) == nbOctetsTotal
        break
    end

    [flag, dgmHeader] = KmAll.readDgmHeader(fid);
    if ~flag
        my_breakpoint
    end
    tmptime        = double(dgmHeader.time_sec) + (double(dgmHeader.time_nanosec) / 1e9);
    dgmHeader.Time = datetime(tmptime, 'ConvertFrom', 'posixtime');

    [flag, buffer] = readTheDatagram(fidBis, dgmHeader);
    if ~flag
        break
    end
    
    switch dgmHeader.dgmType
        case '#MRZ'
            nbPingMRZ = nbPingMRZ + 1;

            % Lecture du datagramme Partition
            [flag, sDgmDataPartition] = KmAll.readDgmMPartition(fid, 'RevFormat', revFormat);
            if ~flag
                break
            end
            
            % Lecture du datagramme Body
            [flag, sDgmDataBody] = KmAll.readDgmMBody(fid, 'RevFormat', revFormat); %#ok<ASGLU>
            if ~flag
                break
            end
            
            % Lecture du datagramme Ping Info
            [flag, sDgmDataPingInfo]  = KmAll.readDgmMRZPingInfo(fid, 'RevFormat', revFormat);
            if ~flag
                break
            end
            % Lecture du datagramme TxSectorInfo
            for n=1:sDgmDataPingInfo.TxSectorInfo.numTxSectors
                [flag, X] = KmAll.readDgmMRZTxSectorInfo(fid, 'RevFormat', revFormat);
                if ~flag
                    break
                end
                sDgmDataTxSectorInfo(n) = X; %#ok<AGROW>
            end

            for k=1:sDgmDataPartition.numOfDgms
                Freq = roundToSup(1e-3 * sDgmDataTxSectorInfo(1).centreFreq_Hz);
                k2   = (Freq == listFrequencies);
                flag = writeTheDatagram(fidOut(k2), buffer);
                if ~flag
                    break
                end
            end

        case {'#SKM'; '#SPO'; '#SVT'; '#IIP'; '#IOP'; '#SVP'; '#CPO'; '#SCL'; '#SHA'; '#MWC'}
            for k2=1:nbFreq
                flag = writeTheDatagram(fidOut(k2), buffer);
                if ~flag
                    break
                end
            end
            
        otherwise
            my_breakpoint
    end
    
    position = position + uint64(dgmHeader.numBytesDgm);
    kDatagram = kDatagram + 1;
    my_waitbar(kDatagram, nbDatagrams, hw)
end
my_close(hw);
fclose(fid);
fclose(fidBis);

% Close write file
for k=1:length(listFrequencies)
    fclose(fidOut(k));
end


function flag = writeTheDatagram(fidOut, buffer)
    n = fwrite(fidOut, buffer, 'uint8');
    flag = (n == length(buffer));


function [flag, dgm] = readTheDatagram(fid, dgmHeader)
% position = ftell(fid) - 20;
% fseek(fid, position, 'bof');
dgm = fread(fid, dgmHeader.numBytesDgm, 'uint8');
% fseek(fid, position, 'bof');
flag = 1;
