function ALL_AbsorpCoeff_Manual(this, alphaUser)

%% Cr�ation des layers de coefficient d'att�nuation

N = length(this);
str1 = 'Cr�ation des layers "AbsorptionCoeff" pour les fichiers .all';
str2 = 'Create "AbsorptionCoeff" layers for .all files';
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    ALL_AbsorpCoeff_Manual_unitaire(this(k), alphaUser)
end
my_close(hw, 'MsgEnd')


function ALL_AbsorpCoeff_Manual_unitaire(this, alphaUser)

[Depth, Carto] = get_Layer(this, 'Depth', 'CartoAuto', 1);
SonarFrequency = get_Layer(this, 'Frequency', 'Carto', Carto);

LayerDepth       = get_Image(Depth);
LayerTxFrequency = get_Image(SonarFrequency);

LayerAbsorptionCoeff = NaN(size(LayerDepth), 'single');
[B,~,J] = unique(LayerTxFrequency);
nbFreq = length(B);
J = reshape(J, size(LayerTxFrequency));

AlphaSum = interp1(alphaUser.Freq, alphaUser.Alpha, B, 'linear', 'extrap');

V2 = NaN(size(LayerDepth), 'single');
for iFreq=1:nbFreq
    subFreq = (J == iFreq);
    V1 = AlphaSum(iFreq);
    subOK = ~isnan(LayerDepth);
    V2(subOK) = V1;
    LayerAbsorptionCoeff(subFreq) = V2(subFreq);
end
clear V2
%   SonarScope(LayerAbsorptionCoeff)

%% Save AbsorptionCoeff in SonarScope cache directory

flag = ALL_writeAbsorptionCoefficientUser(this, LayerAbsorptionCoeff, 1);
if ~flag
    return
end
