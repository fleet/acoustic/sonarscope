function ALL_AbsorpCoeff_ODAS(this, nomFicODAS)

%% Read ODAS file

[~, ~, Ext] = fileparts(nomFicODAS);
switch lower(Ext)
    case '.txt'
        [flag, t, X] = read_yyyymmddhhmmssXxxx(nomFicODAS); %, 'CleanData', 1);
        tODAS = t.timeMat;
        OdasTemp = X(:,1);
        OdasSal =  X(:,2);
        
    case '.abs'
        [flag, Odas] = read_ODAS(nomFicODAS, 'CleanData', 1);
        tODAS = Odas.Time.timeMat;
        OdasTemp = Odas.Temp;
        OdasSal =  Odas.Sal;
        
    otherwise
        str1 = sprintf('L''extention "%s" n''est pas encore pr�vue dans la fonction "ALL_AbsorpCoeff_ODAS".', Ext);
        str2 = sprintf('Extention "%s" is not available in function "ALL_AbsorpCoeff_ODAS" yet.', Ext);
        my_warndlg(Lang(str1,str2), 1);
        flag = 0;
end
if ~flag
    return
end
% c = set_tZTS(cl_SSP, tODAS', [0 -1000]', OdasTemp', OdasSal');

%% Cr�ation des layers de coefficient d'att�nuation

N = length(this);
str1 = 'Cr�ation des layers "AbsorptionCoeff" pour les fichiers .all';
str2 = 'Create "AbsorptionCoeff" layers for .all files';
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    
    [Depth, Carto, ~, sublDepth, nbPingsXML] = get_Layer(this(k), 'Depth', 'CartoAuto', 1);
    SonarFrequency = get_Layer(this(k), 'Frequency', 'Carto', Carto);
    
    TimePing = get(Depth, 'Time');
    TimePing = TimePing.timeMat;
    
    LayerDepth       = get_Image(Depth);
    LayerTxFrequency = get_Image(SonarFrequency);
    
    sub2 = my_interp1_Extrap_PreviousThenNext(tODAS, 1:length(tODAS), TimePing);
    c = set_tZTS(cl_SSP, (tODAS(sub2))', [0 -1000]', (OdasTemp(sub2))', (OdasSal(sub2))');
%     plot(c, 'sub2', 1:1000:length(sub2), 'Freq', 300)
%     plot(c, 'sub2', 1:1000:length(sub2), 'Freq', [200 300 400])
    
    % ind = my_interp1_Extrap_PreviousThenNext(tODAS, 1:length(tODAS), TimePing);
    LayerAbsorptionCoeff = NaN(size(LayerDepth), 'single');
    [B,~,J] = unique(LayerTxFrequency);
    B(isnan(B)) = [];
    nbFreq = length(B);
    J = reshape(J, size(LayerTxFrequency));
    [~, AlphaSum] = absorption(c, B);
    [nbPings, nbBeams] = size(LayerDepth);
    AlphaSum = double(AlphaSum);
    
    X1 = 1:nbPings;
    Z1 = c.Z;
    Z2 = double(LayerDepth);
    V2 = NaN(size(LayerDepth), 'single');
    for iFreq=1:nbFreq
        subFreq = (J == iFreq);
        V1 = AlphaSum(:,:,iFreq);
        X2 = repmat((1:nbPings)', 1, nbBeams);
        subOK = ~isnan(LayerDepth);
        V2(subOK) = interp2(X1, Z1, V1, X2(subOK), Z2(subOK));
        LayerAbsorptionCoeff(subFreq) = V2(subFreq);
    end
    clear V2

    if size(LayerDepth, 2) ~= nbPingsXML % Ajout JMA le 17/01/2016 pour fichier SPFE 0007_20160308_125259_Belgica.all
        pppp = LayerAbsorptionCoeff;
        LayerAbsorptionCoeff = NaN(nbPingsXML, size(LayerDepth, 2), 'single');
        LayerAbsorptionCoeff(sublDepth,:) = pppp;
        clear pppp
    end

%   SonarScope(LayerAbsorptionCoeff)
    
    %% Save AbsorptionCoeff in SonarScope cache directory
    
    flag = ALL_writeAbsorptionCoefficientUser(this(k), LayerAbsorptionCoeff, 1);
    if ~flag
        return
    end
end
my_close(hw, 'MsgEnd')
