% nomFic = 'D:\Temp\SPFE\Brest-Belgique\0000_20150614_001702_Belgica.all';
% aKM = cl_simrad_all('nomFic', nomFic);
% ALL_AbsorpCoeff_RT(aKM)
% nomFic = 'D:\Temp\EM2040\0000_20130802_152942_Europe.all';
% aKM = cl_simrad_all('nomFic', nomFic);
% ALL_AbsorpCoeff_RT(aKM)
%
% ALL_AbsorpCoeff_RT(aKM, nomFic)

function [flag, DataDepth] = ALL_AbsorpCoeff_RT(this, varargin)

[varargin, DataDepth]   = getPropertyValue(varargin, 'DataDepth',   []);
[varargin, DataRuntime] = getPropertyValue(varargin, 'DataRuntime', []);
[varargin, DataRaw]     = getPropertyValue(varargin, 'DataRaw',     []);
[varargin, DataSeabed]  = getPropertyValue(varargin, 'DataSeabed',  []); %#ok<ASGLU>

if isempty(DataDepth)
    [flag, DataDepth] = read_depth_bin(this, 'Memmapfile', 0);
    if ~flag
        return
    end
end

[flag, signalContainerAbsorptionCoeff_RT] = create_signal_AbsorptionCoeff_RT(this, 'DataDepth', DataDepth, ...
    'DataRaw', DataRaw, 'DataSeabed', DataSeabed, 'DataRuntime', DataRuntime);
if ~flag
    return
end
for k=length(signalContainerAbsorptionCoeff_RT.signalList.ySample):-1:1
    MeanAlpha(:,k) = signalContainerAbsorptionCoeff_RT.signalList.ySample(k).data;
end

%% Lecture des layers

[TxBeamIndex, Carto] = get_Layer(this, 'TxBeamIndex', 'CartoAuto', 1, 'Mute', 1, ...
    'DataDepth', DataDepth, 'DataSeabed', DataSeabed); % , 'DataRaw', DataRaw % Bug si on passe DataRaw

% 'DataSeabed', DataSeabed, 'DataInstalParam', DataInstalParam, ...
% 'DataRuntime', DataRuntime, 'DataSSP', DataSSP, 'DataAttitude', DataAttitude);

if isempty(TxBeamIndex)
    flag = 0;
    return
end

RawPingCounter = get(TxBeamIndex, 'PingCounter');
TxBeamIndex    = get_Image(TxBeamIndex);

if DataDepth.EmModel == 300
    SonarFrequency = get_Layer(this, 'Frequency', 'Carto', Carto);
    if isempty(SonarFrequency)
        flag = 0;
        return
    end
    LayerTxFrequency = get_Image(SonarFrequency);
    [B,~,J] = unique(LayerTxFrequency);
    B = B(~isnan(B));
%     nbFreq = length(B);
    J = reshape(J, size(LayerTxFrequency));
end

if size(DataDepth.PingCounter, 1) == 1
    DataDepth.PingCounter = DataDepth.PingCounter';
end
if size(RawPingCounter, 1) == 1
    RawPingCounter = RawPingCounter';
end
[sublDepth, sublRaw] = intersect3(DataDepth.PingCounter(:,1), RawPingCounter(:,1), RawPingCounter(:,1)); %#ok<ASGLU>

%% Calcul du coefficient d'absorption

LayerAbsorptionCoeff = NaN(DataDepth.Dimensions.nbPings,  DataDepth.Dimensions.nbBeams, 'single');
for iPing=1:length(sublDepth)
    %     for iFreq=1:max(TxBeamIndex(sublDepth(iPing),:))
    %         subFreq = find(TxBeamIndex(sublDepth(iPing),:) == iFreq);
    for iFreq=1:max(TxBeamIndex(iPing,:)) % Modif JMA le 23/05/2016 donn�e F:\SPFE\HBMC1213\0001_20120509_033505_Belgica.all
        subFreq = find(TxBeamIndex(iPing,:) == iFreq);
        if ~isempty(subFreq)
            if size(MeanAlpha, 2) == 1 % Pour traiter les fichiers anciens o� il y avait une seule valeur de alpha donn�e pour l'ensemble des secteurs. En attente de modif avec infos de Xavier
                switch DataDepth.EmModel
                    case 300
                        subFreq = (J(sublDepth(iPing),:) == iFreq);
                        alphaSIS = MeanAlpha(sublDepth(iPing),1);
                        %                         LayerAbsorptionCoeff(sublDepth(iPing),subFreq) = alphaSIS + 0.34 * (B(iFreq)-31) + 1.14; % Empirique, test� sur donn�es The Brothers
                        if iFreq <= length(B)
                            LayerAbsorptionCoeff(sublDepth(iPing),subFreq) = alphaSIS * (B(iFreq) / 31.6).^2;
                        else % Cas fichier de Jose Maria Cordero Ros, mail du 31/01/2020 Donn�es dans r�pertoire D:\Temp\SPAIN
                            LayerAbsorptionCoeff(sublDepth(iPing),subFreq) = alphaSIS * (B(end) / 31.6).^2;
                        end
                    otherwise
                        iFreq2 = 1;
                        LayerAbsorptionCoeff(sublDepth(iPing),subFreq) = MeanAlpha(sublDepth(iPing),iFreq2);
                end
            else
                iFreq2 = iFreq;
                LayerAbsorptionCoeff(sublDepth(iPing),subFreq) = MeanAlpha(sublDepth(iPing),iFreq2);
            end
        end
    end
end
% SonarScope(LayerAbsorptionCoeff)

if nargout == 2
    DataDepth
    whos LayerAbsorptionCoeff
end

%% Save AbsorptionCoeff in SonarScope cache directory

[nomDir, nomFicSeul] = fileparts(this.nomFic);
nomDirRacine = fullfile(nomDir, 'SonarScope', nomFicSeul);

Info.Storage  = 'single';
Info.FileName = fullfile('Ssc_Depth', 'AbsorptionCoefficientRT.bin');
flag = writeImage(nomDirRacine, Info, LayerAbsorptionCoeff);
if ~flag
    return
end
