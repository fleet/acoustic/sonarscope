function flag = ALL_AbsorpCoeff_SIS(this, nomFicABS)

if ischar(nomFicABS)
    nomFicABS = {nomFicABS};
end

%% Read .abs file

tabABS = {};
tAbs = datetime.empty();
fAbs = [];
N = length(nomFicABS);
str1 = 'Lecture des fichiers .abs';
str2 = 'Reading .abs files';
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    
    [flag, ABS] = read_SISabs(nomFicABS{k});
    if flag
        tabABS{end+1} = ABS; %#ok<AGROW>
        tAbs(end+1) = ABS.Datetime(1); %#ok<AGROW>
        fAbs(end+1) = ABS.Frequency; %#ok<AGROW>
    end
end
my_close(hw)

[timeUnique, ~, tInd] = unique(tAbs);
n = length(timeUnique);
tSSP = NaN(1,n);
for k=1:n
    sub = find(tInd == k);
    tSSP(k) = timeUnique(k);
%     FigUtils.createSScFigure; PlotUtils.createSScPlot(fAbs(sub), '-*'); title(t2str(cl_time('timeMat', tSSP(k))))
    [freqUnique, fInd] = unique(fAbs(sub));
    tab2ABS{k} = tabABS(sub(fInd)); %#ok<AGROW>
end

%% Cr�ation des layers de coefficient d'att�nuation

N = length(this);
str1 = 'Cr�ation des layers "AbsorptionCoeff" pour les fichiers .all';
str2 = 'Create "AbsorptionCoeff" layers for .all files';
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    
    [Depth, Carto] = get_Layer(this(k), 'Depth', 'CartoAuto', 1);
    SonarFrequency = get_Layer(this(k), 'Frequency', 'Carto', Carto);
    
    TimePing = get(Depth, 'Time');
    TimePing = TimePing.timeMat;
    
    LayerDepth       = get_Image(Depth);
    LayerTxFrequency = get_Image(SonarFrequency);
    
    sub2 = my_interp1_Extrap_PreviousThenNext(tSSP, 1:length(tSSP), TimePing);
    
    LayerAbsorptionCoeff = NaN(size(LayerDepth), 'single');
    [B,~,J] = unique(LayerTxFrequency);
    nbFreq = length(B(~isnan(B)));
    J = reshape(J, size(LayerTxFrequency));
    subNaN = isnan(LayerTxFrequency);
    J(subNaN) = NaN;
    
    flagLayerModifie = 0;
    [nbPings, nbBeams] = size(LayerDepth);
    X1 = 1:nbPings;
    Z2 = LayerDepth;
    V2 = NaN(size(LayerDepth), 'single');
    B = B(1:nbFreq);
    for iFreq=1:nbFreq
        freqSector = B(iFreq);
        indInf = find(freqUnique <= freqSector, 1, 'last');
        indSup = find(freqUnique  > freqSector, 1, 'first');
        
        [BPings, ~, JPings] = unique(sub2);
        
%         if length(BPings) > 1
%             messageForDebugInspection
%         end
        
        for kSub=1:length(BPings)
            subPings = find(JPings == BPings(kSub));
            if isempty(subPings)
                continue
            end
            
            s1 = tab2ABS{sub2(kSub)}{indInf};
            s2 = tab2ABS{sub2(kSub)}{indSup};
            r = (freqSector - freqUnique(indInf)) / (freqUnique(indSup) - freqUnique(indInf));
            AlphaSum = (1-r) * s1.alpha2 + r * s2.alpha2;
            Z1 = s1.Z;
            
            subFreq = (J == iFreq);
            V1 = repmat(AlphaSum, 1, length(X1));
            X2 = repmat((1:nbPings)', 1, nbBeams);
            subOK = ~isnan(LayerDepth);
            V2(subOK) = interp2(X1, Z1, V1, X2(subOK), Z2(subOK));
            
            V3 = LayerAbsorptionCoeff(subPings,:);
            V3(subFreq) = V2(subFreq);
            LayerAbsorptionCoeff(subPings,:) = V3;
            
            flagLayerModifie = 1;
        end
    end
    clear V2
%     SonarScope(LayerAbsorptionCoeff)
    
    %% Save AbsorptionCoeff in SonarScope cache directory

    flag = ALL_writeAbsorptionCoefficientUser(this(k), LayerAbsorptionCoeff, flagLayerModifie);
    if ~flag
        return
    end
end
my_close(hw, 'MsgEnd')
