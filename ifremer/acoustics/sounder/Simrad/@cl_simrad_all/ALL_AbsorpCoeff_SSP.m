% nomFic = 'D:\Temp\SPFE\Brest-Belgique\0000_20150614_001702_Belgica.all';
% aKM = cl_simrad_all('nomFic', nomFic);
% ALL_AbsorpCoeff_SSP(aKM)

% TODO : r�aliser une fonction qui fait cela dans cl_image pour que �a
% devienne g�n�ral

function flag = ALL_AbsorpCoeff_SSP(this, varargin)

[varargin, IndNavUnit] = getPropertyValue(varargin, 'IndNavUnit', []);
[varargin, DataDepth]  = getPropertyValue(varargin, 'DataDepth',  []);
[varargin, Tag]        = getPropertyValue(varargin, 'Tag',        'SSc'); %#ok<ASGLU>

nomDir = fileparts(this.nomFic);
nomFicSalinity = fullfile(nomDir, 'SonarScope', 'DefaultSalinity.txt');

%% Lecture de DataDepth

if isempty(DataDepth)
    [flag, DataDepth] = read_depth_bin(this, 'Memmapfile', 0);
    if ~flag
        return
    end
end

%% Lecture des layers

[Depth, Carto] = get_Layer(this, 'Depth+Draught-Heave', 'CartoAuto', 1, 'Mute', 1);
if isempty(Depth)
    flag = 0;
    return
end

SonarFrequency = get_Layer(this, 'Frequency', 'Carto', Carto, 'Mute', 1);

%     TimePing = get(Depth, 'Time');
%     TimePing = TimePing.timeMat;

LayerDepth = get_Image(Depth);

if isempty(SonarFrequency)
    sd = get_SonarDescription(Depth);
    freqNominaleSonar = get(sd, 'Sonar.Freq');
    LayerTxFrequency = zeros(size(LayerDepth), 'single') + freqNominaleSonar;
    pppp = get_Layer(this, 'Raw-Reflectivity', 'Carto', Carto, 'Mute', 1);
    RawPingCounter = get(pppp, 'PingCounter');
else
    LayerTxFrequency = get_Image(SonarFrequency);
    RawPingCounter = get(SonarFrequency, 'PingCounter');
end
Immersion = get(Depth, 'Immersion');
TimeDepth = get(Depth, 'SonarTime');
TimeDepth = TimeDepth.timeMat;

if size(DataDepth.PingCounter, 1) == 1
    DataDepth.PingCounter = DataDepth.PingCounter';
end
if size(RawPingCounter, 1) == 1
    RawPingCounter = RawPingCounter';
end
sublDepth = intersect3(DataDepth.PingCounter(:,1), RawPingCounter(:,1), RawPingCounter(:,1));

LayerAbsorptionCoeff = NaN(DataDepth.Dimensions.nbPings, DataDepth.Dimensions.nbBeams, 'single');
flag_LayerAbsorptionCoeffUpdated = 0;

[Frequencies, ~, J] = unique(LayerTxFrequency);
Frequencies = Frequencies(~isnan(Frequencies));
nbFreq = length(Frequencies);
J = reshape(J, size(LayerTxFrequency));

%% Lecture de la navigation

[flag, DataPosition] = read_position_bin(this, 'IndNavUnit', IndNavUnit);
if ~flag
    return
end
TimeNav = DataPosition.Time;
TimeNav = TimeNav.timeMat;

%% Lecture des profils de c�l�rit�

[flag, DataSSP] = SSc_ReadDatagrams(this.nomFic, 'Ssc_SoundSpeedProfile');
if ~flag
    return
end
%   FigUtils.createSScFigure; PlotUtils.createSScPlot(DataSSP.SoundSpeed', DataSSP.Depth'); grid on

% Correctid tant que XSF est non corrig�

[n1, n2] = size(DataSSP.Depth);
if n1 > n2
    DataSSP.Depth = DataSSP.Depth';
    DataSSP.SoundSpeed = DataSSP.SoundSpeed';
    if isfield(DataSSP, 'Temperature')
        DataSSP.Temperature = DataSSP.Temperature';
        DataSSP.Salinity = DataSSP.Salinity';
    end
end


TimeStartOfUse = DataSSP.TimeStartOfUse;
TimeStartOfUse = TimeStartOfUse.timeMat;
TimeStartOfUse(end+1) = Inf;
nbSSP = size(DataSSP.SoundSpeed,1);
for k2=1:nbSSP
    CeleritySoundSpeedProfile = DataSSP.SoundSpeed(k2,:);
    DepthSoundSpeedProfile    = DataSSP.Depth(k2,:);
    
    subNaN = isnan(CeleritySoundSpeedProfile) | (CeleritySoundSpeedProfile == 0);
    DepthSoundSpeedProfile(subNaN) = [];
    CeleritySoundSpeedProfile(subNaN) = [];
    % FigUtils.createSScFigure; PlotUtils.createSScPlot(CeleritySoundSpeedProfile, DepthSoundSpeedProfile); grid on;
    
    tSSP = TimeStartOfUse(k2);
    subPings = find((TimeDepth >= TimeStartOfUse(k2)) & (TimeDepth < TimeStartOfUse(k2+1)));
    if isempty(subPings)
        continue
    end
    
    subNav = find((TimeNav >= TimeStartOfUse(k2)) & (TimeNav < TimeStartOfUse(k2+1)));
    if isempty(subNav)
        continue
    end
    
    Lat = median(DataPosition.Latitude(subNav),  'omitnan');
    Lon = median(DataPosition.Longitude(subNav), 'omitnan');
    
    %% R�cup�ration de la salinit� : tout �a pour r�cup�rer une valeur plausible de la salinit� !!!
    % Ca serait mieux que KM nous donne le maximum d'info : Salinit� mais
    % aussi profil de Temp, �a �viterait d'�tre oblig� de recalculer la
    % temp�rature � partir du profil de c�l�rit�
    
    instanceSPPLevitus = set_tLevitus(cl_SSP, tSSP, Lat, Lon);
    Sal = instanceSPPLevitus.Sal;

    if isempty(Sal) || isnan(mean(Sal, 'omitnan')) % C'est arriv� avec Lat = 74.466011825000010, Lon = -82.753842199999994 (St-Laurent)
        if exist(nomFicSalinity, 'file')
            fid = fopen(nomFicSalinity, 'r');
            line = fgetl(fid);
            fclose(fid);
            mots = strsplit(line);
            Sal = str2double(mots{end});
        else
            str1 = sprintf('La salinit� n''a pas �t� trouv�e dans la base Levitus. La valeur que vous allez\nsaisir sera consign�e dans un fichier "DefaultSalinity.txt" dans le r�pertoire des .all.\nCette valeur sera utilis�e automatiquement si pas trouv�e dans Levitus.');
            str2 = sprintf('The salinity was not found in the Levitus database. The value that you are\ngoing to set will be saved in "DefaultSalinity.txt" in the directory of the .all files.\nThis value will be used automaticaly when not found in Levitus');
            p = ClParametre('Name', Lang('Salinit�','Salinity'), ...
                'Unit', '1/1000',  'MinValue', 0, 'MaxValue', 50, 'Value', 35, 'Mute', 0);
            a = ParametreDialog('params', p, 'Title', Lang(str1,str2));
            % a.Help = 'http://flotte.ifremer.fr/fleet/Presentation-of-the-fleet/On-board-software/SonarScope';
            a.sizes = [0 -3 -1 -2 -1 -1 -1 0];
            a.openDialog;
            flag = a.okPressedOut;
            if ~flag
                return
            end
            Sal = a.getParamsValue;

            fid = fopen(nomFicSalinity, 'w+');
            fprintf(fid, 'Salinity = %s', num2str(Sal));
            fclose(fid);
        end
    else
        Sal = mean(Sal, 'omitnan');
        fid = fopen(nomFicSalinity, 'w+');
        fprintf(fid, 'Salinity = %s', num2str(Sal));
        fclose(fid);
    end

    %% Set a new SSP instance
    
    [~, ordre] = unique(-DepthSoundSpeedProfile);
    DepthSoundSpeedProfile    = DepthSoundSpeedProfile(ordre);
    CeleritySoundSpeedProfile = CeleritySoundSpeedProfile(ordre);
    if numel(Sal) ~= 1
        Sal = Sal(ordre);
    end
    
    instanceSPPnew = set_tZC(cl_SSP, tSSP, DepthSoundSpeedProfile', CeleritySoundSpeedProfile', Sal);
    %     plot(instanceSPPnew, 'Freq', 200)
    
    %% Calcul du coefficient d'absorption
    
    %     whos instanceSPPnew Frequencies Immersion subPings
    [~, AlphaSum] = absorption(instanceSPPnew, Frequencies, 'Immersion', mean(Immersion(subPings), 'omitnan'));
    if isempty(AlphaSum)
        continue
    end

    subPings = single(subPings);
    X1 = subPings;
    Z1 = instanceSPPnew.Z;
    Z2 = LayerDepth(subPings,:);
    nbPingsSub = length(subPings);
    V2 = NaN(nbPingsSub, DataDepth.Dimensions.nbBeams, 'single');
    
    % ind = my_interp1_Extrap_PreviousThenNext(tSSP, 1:length(tSSP), TimePing);
    
    for iFreq=1:nbFreq
        subFreq = (J(subPings,:) == iFreq);
        
        V1 = AlphaSum(:,:,iFreq);
        X2 = repmat(subPings, 1, DataDepth.Dimensions.nbBeams);
        subOK = ~isnan(LayerDepth(subPings,:));
        
        if length(V1) == 1 % Cas EM2040 AUV mission Herv� 18/02/2018
            V2(subOK) = V1;
        else
            V1 = repmat(V1, 1, nbPingsSub);
            if length(X1) == 1
                V2(subOK) = interp1(Z1, V1, Z2(subOK));
            else
                V2(subOK) = interp2(X1, Z1, V1, X2(subOK), Z2(subOK));
            end
        end
        V3 = LayerAbsorptionCoeff(sublDepth(subPings),:);
        V3(subFreq) = V2(subFreq);
        LayerAbsorptionCoeff(sublDepth(subPings),:) = V3;
        flag_LayerAbsorptionCoeffUpdated = 1;
    end
    % SonarScope(LayerAbsorptionCoeff)
end

%% Save AbsorptionCoeff in SonarScope cache directory

if flag_LayerAbsorptionCoeffUpdated
    % SonarScope(LayerAbsorptionCoeff)
    [nomDir, nomFicSeul] = fileparts(this.nomFic);
    
    nomFicNc = fullfile(nomDir, 'SonarScope', [nomFicSeul '.nc']);
    if exist(nomFicNc, 'file')
         flag = save_signalNetcdf(this, 'Ssc_Depth', ['AbsorptionCoefficient' Tag], LayerAbsorptionCoeff);
    else
        nomDirRacine = fullfile(nomDir, 'SonarScope', nomFicSeul);
        
        Info.Storage  = 'single';
        Info.FileName = fullfile('Ssc_Depth', ['AbsorptionCoefficient' Tag '.bin']);
        flag = writeImage(nomDirRacine, Info, LayerAbsorptionCoeff);
        if ~flag
            return
        end
    end
else
    %     messageForDebugInspection
end
