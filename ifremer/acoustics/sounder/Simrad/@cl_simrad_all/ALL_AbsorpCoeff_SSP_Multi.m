function flag = ALL_AbsorpCoeff_SSP_Multi(this)

flag = 0;

%% Cr�ation des layers de coefficient d'att�nuation

N = length(this);
str1 = 'Cr�ation des layers "AbsorptionCoeff" pour les fichiers .all';
str2 = 'Create "AbsorptionCoeff" layers for .all files';
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    flag = ALL_AbsorpCoeff_SSP(this(k), 'Tag', 'User');
end
my_close(hw, 'MsgEnd')
