% TODO : � d�composer en 3 : BS, DiagTx et autre

function flag = ALL_BestBSCompute(this, InfoCompensationCurve)

useParallel = get_UseParallelTbx;

%% Lecture des fichiers de compensation si il y en a

DataTypeConditionsComp = {};
if isempty(InfoCompensationCurve) || isempty(InfoCompensationCurve.FileName)
    bilan         = [];
    ModeDependant = 0;
    UseModel      = [];
    PreserveMeanValueIfModel = [];
else
    FileNameCompensation = InfoCompensationCurve.FileName;
    if ischar(FileNameCompensation)
        FileNameCompensation = {FileNameCompensation};
    end
    ModeDependant = InfoCompensationCurve.ModeDependant;
    
    for k2=1:length(FileNameCompensation)
        CourbeConditionnelle = load_courbesStats(FileNameCompensation{k2});
        if isempty(CourbeConditionnelle)
            flag = 0;
            return
        end
        bilan{k2} = CourbeConditionnelle.bilan; %#ok<AGROW>
        for k3=1:length(bilan{k2})
            for k4=1:length(bilan{k2}{k3}(1).DataTypeConditions)
                DataTypeConditionsComp{end+1} = bilan{k2}{k3}(1).DataTypeConditions{k4}; %#ok<AGROW>
            end
        end
        if isfield(InfoCompensationCurve, 'UseModel') && ~isempty(InfoCompensationCurve.UseModel)
            UseModel(k2) = InfoCompensationCurve.UseModel(k2); %#ok<AGROW>
            PreserveMeanValueIfModel(k2) = InfoCompensationCurve.PreserveMeanValueIfModel(k2); %#ok<AGROW>
        else
            UseModel(k2) = 0; %#ok<AGROW>
            PreserveMeanValueIfModel(k2) = 0; %#ok<AGROW>
        end
    end
end

%% Loop on files

N = length(this);
flagFile = false(1,N);
str1 = 'Calcul du layer ReflectivityBestBS sur les fichiers .all';
str2 = 'Processing ReflectivityBestBS layer of .all files';
msgWaitbar = Lang(str1,str2);
if useParallel && (N > 2)
    [hw, DQ] = create_parforWaitbar(N, msgWaitbar);
    parfor k=1:N
        send(DQ, k);
        flagFile(k) = ALL_BestBSCompute_unitaire(this(k), ...
            DataTypeConditionsComp, bilan, ModeDependant, UseModel, PreserveMeanValueIfModel, ...
            'isUsingParfor', true);
    end
else
    hw = create_waitbar(msgWaitbar, 'N', N);
    for k=1:N
        my_waitbar(k, N, hw)
        flagFile(k) = ALL_BestBSCompute_unitaire(this(k), ...
            DataTypeConditionsComp, bilan, ModeDependant, UseModel, PreserveMeanValueIfModel);
    end
end
my_close(hw, 'MsgEnd');
flag = all(flagFile);


function flag = ALL_BestBSCompute_unitaire(this, ...
    DataTypeConditionsComp, bilan, ModeDependant, UseModel, PreserveMeanValueIfModel, varargin)

global isUsingParfor %#ok<GVMIS>

[varargin, isUsingParfor] = getPropertyValue(varargin, 'isUsingParfor', isUsingParfor); %#ok<ASGLU>

str1 = 'Traitement du fichier : ';
str2 = 'Processing : ';
fprintf('----------------------------------------------------------\n%s %s\n', Lang(str1,str2), this.nomFic);

[listeCor, ~, flag] = getListeLayersSupplementaires(DataTypeConditionsComp); % nomsLayersSynchronized
if ~flag
    return
end

Version = version_datagram(this);
if isempty(Version)
    return
end

% TODO : r�duire la liste au stricte n�cessaire (supprimer AcrossDist, alongDist, Detection, etc ...)
% ListeLayersV1 = unique([2 3 4 6 10 14 24 25 26 27 28 36 40 41 43 47 listeCor]); % TODO : tester
% ListeLayersV1=unique([14 25 26 27 28 40 41 42 43 47 listeCor]);

% Modif JMA le 13/02/2024 : suppression du layer UserAbsorptionCoeff (42) car image remplie de z�ros pour les .kmall
% Il serait pr�f�rable que ce layer ne soit pas cr�� en avance ou alors
% qu'il soit juste initialis� (param�tre ??? pr�vu � cet effet)
ListeLayersV1 = unique([14 25 26 27 28 40 41 43 47 listeCor]);

switch Version
    case 'V1'
        c = import_PingBeam_All_V1(this, 'ListeLayers', ListeLayersV1, 'CartoAuto', 1);
        if isempty(c)
            return
        end
    case 'V2'
        ListeLayers = SelectionLayers_V1toV2(ListeLayersV1);
        c = import_PingBeam_All_V2(this, 'ListeLayers', ListeLayers, 'CartoAuto', 1);
        if isempty(c)
            return
        end
end
identReflectivity = cl_image.indDataType('Reflectivity');

%% Compensation statistique (Ex : courbes de calibration)

if ~isempty(bilan) && (length(bilan) == 1) && iscell(bilan{1})
    bilan = bilan{1};
end
for k2=1:length(bilan)
    indLayerAsCurrentOne = findIndLayerSonar(c, 1, 'DataType', identReflectivity, 'OnlyOneLayer', 'WithCurrentImage', 1);
    
    identLayersConditionCompensation = [];
    for k3=1:length(DataTypeConditionsComp)
        identDataType = cl_image.indDataType(DataTypeConditionsComp{k3});
        x = findIndLayerSonar(c, 1, 'DataType', identDataType, 'WithCurrentImage', 1);
        if isempty(x)
            str1 = sprintf('Le layer de type "%s" n''a pas �t� trouv�.', DataTypeConditionsComp{k3});
            str2 = sprintf('Layer of type "%s" was not found.', DataTypeConditionsComp{k3});
            my_warndlg(Lang(str1,str2), 1);
            flag = 0;
            return
        else
            identLayersConditionCompensation(k3) = x; %#ok<AGROW>
        end
    end
    [d, flag] = compensationCourbesStats(c(indLayerAsCurrentOne), c(identLayersConditionCompensation), ...
        bilan(k2), 'ModeDependant', ModeDependant, 'UseModel', UseModel(k2), ...
        'PreserveMeanValueIfModel', PreserveMeanValueIfModel(k2));
    if ~flag
        return
    end
    c(indLayerAsCurrentOne) = d;
end

%% Mise en conformit� de la r�flectivit�

indLayer = findLayers(c, 'DataType', identReflectivity);
[~, indLayerRange] = findAnyLayerRange(c, 1, 'WithCurrentImage', 1, 'AtLeastOneLayer');
if isempty(indLayerRange)
    flag = 0;
    return
end

[flag, d] = SonarBestBSConstructeur(c, indLayer, 'Range', c(indLayerRange), ...
    'ChangeInsonifiedArea', 1, 'Mute', 1); %#ok<ASGLU>
if ~flag
    return
end
% SonarScope(d);
