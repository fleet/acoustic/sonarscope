function ALL_CleanBathy(this)

N = length(this);
str1 = 'Nettoyage des donn�es ALL bas� sur la bathym�trie';
str2 = 'ALL Data cleaning based on Bathy';
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
    my_waitbar(k, N, hw);
    str1 = sprintf('Traitement du fichier %d/%d : "%s" en cours.', k, N, this(k).nomFic);
    str2 = sprintf('Processing file %d/%d : "%s".', k, N, this(k).nomFic);
    fprintf(1,'%s\n', Lang(str1,str2));
    
    ALL_CleanBathythis_unitaire(this(k));
    
    %% On demande si on veut traiter les autres fichiers
    
    if k ~= N
        flag = question_ContinueNextFile();
        if ~flag
            break
        end
    end
end
my_close(hw, 'MsgEnd')


function flag = ALL_CleanBathythis_unitaire(this)

%% V�rification de DataDepth

[flag, DataDepth] = read_depth_bin(this); %, 'Memmapfile', Memmapfile);
if ~flag
    return
end

%% Lecture des layers

[b, ~, ~, sublDepth] = get_Layer(this, 'Bathymetry', 'CartoAuto', 1); % 'Bathymetry' et �quivalent � 'Depth+Draught-Heave' dans get_Layer
if isempty(b)
    flag = 0;
    return
end

%% Lancement de SonarScope

c = SonarScope(b, 'ButtonSave', 1); %, 'visuProfilX', 1, 'visuProfilY', 1, 'NbVoisins', 20); %, 'TypeInteractionSouris', 9);

%% R�cup�ration de DetectionType

Mask = isnan(c(end));

%% Report dans le masque

M = DataDepth.Mask(sublDepth,:);
M(Mask) = true;
DataDepth.Mask(sublDepth,:) = M;

%% Ecriture du masque

flag = existCacheNetcdf(this.nomFic);
if flag
    flag = save_signalNetcdf(this, 'Ssc_Depth', 'Mask', M);
else
    flag = write_depth_bin(this, DataDepth);
end
