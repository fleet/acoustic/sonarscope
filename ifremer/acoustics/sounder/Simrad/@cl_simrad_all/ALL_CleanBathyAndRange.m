% Prelecture des fichiers .all : Nettoyage � partir de l'analyse histogrammique de Range
%
% Syntax
%   status = ALL_CleanBathyAndRange(aKM)
%
% Input Arguments
%   aKM : Instances de cl_simradAll
%
% Output Arguments
%   status : 0 si arret par Cancel, 1 sinon
%
% Examples
%   nomFic = getNomFicDatabase('EM1002_0001_20040916_054013_raw.all')
%   ALL_CleanBathyAndRange(nomFic);
%
%   nomDir = fileparts(nomFic)
%   liste = listeFicOnDepthDir(nomDir, '.all')
%   ALL_CleanBathyAndRange(liste);
%
% See also cl_simrad_all Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function flag = ALL_CleanBathyAndRange(this)

flag = 0;

NbFic = length(this);
str1 = 'Nettoyage histogrammique des fichiers .all .';
str2 = 'Histogramic cleaning of .all files.';
hw = create_waitbar(Lang(str1,str2), 'N', NbFic);
for k=1:NbFic
    my_waitbar(k, NbFic, hw)
    fprintf('%s\n%d / %d : %s\n', repmat('-',1,100), k, NbFic, this(k).nomFic);
    flag = ALL_CleanBathyAndRange_unitaire(this(k));
end
my_close(hw, 'MsgEnd')


function flag = ALL_CleanBathyAndRange_unitaire(this)

FirstTime   = 1;
fig         = [];
FigPosition = [];


[flag, DataDepth] = read_depth_bin(this, 'Memmapfile', -1);
if ~flag
    return
end

[flag, DataAttidude] = read_attitude_bin(this);
if flag
    Heave = my_interp1(DataAttidude.Datetime, DataAttidude.Heave, DataDepth.Datetime);
else
    Heave = zeros(DataDepth.Dimension.nbPings, 1);
end

[~, nomFic] = fileparts(this.nomFic);

if strcmp(DataDepth.Version, 'V1')
    Range = DataDepth.Range(:,:);
else
    [flag, DataRaw] = SSc_ReadDatagrams(this.nomFic, 'Ssc_RawRangeBeamAngle', 'Memmapfile', -1);
    if ~flag
        % TODO : sortir un message
        return
    end
    Range = DataRaw.TwoWayTravelTime(:,:);
end
Depth = DataDepth.Depth(:,:);
Depth = Depth - Heave(:,1); % TODO : traiter b�bord et tribord s�par�ment
subNaN = (DataDepth.Mask ~= 0);
Range(subNaN) = NaN;
Depth(subNaN) = NaN;

% TODO / Attention danger. tester l'intersection entre DataRaw et DataDepth !!!!
% TODO : Ajouter lecture de QFIfremer si pr�sent et pr�senter en 3i�me
% colonne : pas pertinent pour mission QUOI au travers de l'histogramme
% 3D Bathy/QF : deux taches non s�parables en fonction du QF

% TODO : d�composer l'action en subpings de longueur < hauteur de l'�cran

OK = 2;
disp('---------------------------------------------------------------')
while OK == 2
    [fig, FigPosition] = display_images(fig, nomFic, Range, Depth, FigPosition);
    
    if FirstTime
        str1 = 'Cette op�ration agit uniquement sur un masque que vous pouvez compl�ter ou remettre � z�ro en cliquant sur l''item ci-dessous.';
        str2 = 'This process operates only on a Mask you can append or reinitialize.';
        str3 = 'Compl�ter';
        str4 = 'Append';
        str5 = 'Remise � z�ro';
        str6 = 'Reinitialize';
        [rep, flag] = my_questdlg(Lang(str1,str2), Lang(str3,str4), Lang(str5,str6));
        if ~flag
            return
        end
        if rep == 2
            MaxRangePrecedent = max(Range(:));
            if strcmp(DataDepth.Version, 'V1')
                Range = DataDepth.Range(:,:);
            else
                try
                    Range = DataRaw.TwoWayTravelTimeInSeconds(:,:);
                catch
                    Range = DataRaw.TwoWayTravelTime(:,:);
                end
            end
            Depth = DataDepth.Depth(:,:);
            Depth  = Depth - Heave;
            [fig, FigPosition] = display_images(fig, nomFic, Range, Depth, FigPosition);
            
            DataDepth.Mask(:,:) = false;
            
            flag = existCacheNetcdf(this.nomFic);
            if flag
                flag = save_signalNetcdf(this, 'Ssc_Depth', 'Mask', DataDepth.Mask(:,:));
            else
                flag = write_depth_bin(this, DataDepth);
            end
            
            MaxRange = max(Range(:));
            if MaxRange ~= MaxRangePrecedent
                [rep, flag] = my_questdlg(Lang('TODO', ...
                    'Maximum of "RayPathRange" has changed, it can improve "Seabed Imagery" file size and processing performances. Do you want to take advantage of it ?'));
                if ~flag
                    return
                end
                if rep == 1
                    nomFicSeabed = SimradSeabedNames_PingSamples(this.nomFic);
                    supresssSeabedFiles(nomFicSeabed)
                end
            end
        end
        FirstTime = 0;
    end
    
    str1 = 'Voyez-vous toutes les couleurs de l''image "Depth" ?';
    str2 = 'Do you see all the colors on the "Depth" image ?';
    [OK, flag] = my_questdlg(Lang(str1,str2));
    if ~flag
        return
    end
    if OK == 2
        NomLayer = 'Depth';
    else
        if get_LevelQuestion >= 2
            str1 = 'Voyez-vous toutes les couleurs de l''image "RayPathRange" ?';
            str2 = 'Do you see all the colors on the "RayPathRange" image ?';
            [OK, flag] = my_questdlg(Lang(str1,str2));
            if ~flag
                return
            end
            if OK == 2
                NomLayer = 'Range';
            end
        end
    end
    
    %         figure(fig)
    if OK == 1
        return
    end
    
    switch NomLayer
        case 'Depth'
            I = Depth;
        case 'Range'
            I = Range;
    end
    
    bins = linspace(min(I(:)), max(I(:)), 200);
    
    [N, bins, ~, SubBins] = histo1D(I, bins, 'Sampling', 1);
    sub = getFlaggedBinsOfAnHistogram(bins, N);
    
    MaxRangePrecedent = max(Range(:));
    
    %% On recup�re les modifications
    
    if ~isempty(sub)
        for k2=1:length(sub)
            subPoubelle = SubBins{sub(k2)};
            Range(subPoubelle) = NaN;
            Depth(subPoubelle) = NaN;
            DataDepth.Mask(subPoubelle) = 1;
        end
        [fig, FigPosition] = display_images(fig, nomFic, Range, Depth, FigPosition);
        
        flag = existCacheNetcdf(this.nomFic);
        if flag
            flag = save_signalNetcdf(this, 'Ssc_Depth', 'Mask', DataDepth.Mask(:,:));
        else
            flag = write_depth_bin(this, DataDepth);
        end
        
        MaxRange = max(Range(:));
        if MaxRange ~= MaxRangePrecedent
            rep = 1;
            %{
MaxRangePrecedent = MaxRange;
[rep, flag] = my_questdlg(Lang('TODO', ...
'Maximum of "RayPathRange" has changed, it can improve "Seabed Imagery" file size and processing performances. Do you want to take advantage of it ?'));
if ~flag
return
end
            %}
            if rep == 1
                nomFicSeabed = SimradSeabedNames_PingSamples(this.nomFic);
                supresssSeabedFiles(nomFicSeabed)
            end
        end
    end
end
my_close(fig)


function [fig, FigPosition] = display_images(fig, nomFic, Range, Depth, FigPosition)

if isempty(fig) || ~ishandle(fig)
    if isempty(FigPosition)
        ScreenSize = get(0, 'ScreenSize');
        fig = figure('Position', centrageFig(950, ScreenSize(4)-150));
    else
        fig = figure('Position', FigPosition);
    end
end

figure(fig)
FigPosition = get(fig, 'Position');
subplot(1,2,1)
imagesc(Depth); colormap(jet(256)); colorbar; axis xy;
title([nomFic ' Depth'], 'interpreter', 'none')

subplot(1,2,2)
imagesc(Range); colormap(jet(256)); colorbar; axis xy;
title('Range')



function supresssSeabedFiles(nomFicSeabed)

if exist(nomFicSeabed.ReflectivityErs, 'file')
    try
        delete(nomFicSeabed.ReflectivityErs)
    catch %#ok<CTCH>
        str1 = sprintf('Impossible de supprimer le fichier "%s", sans doute est-il ouvert.', nomFicSeabed.ReflectivityErs);
        str2 = sprintf('File "%s", could not be deleted, perhaps it is open somewhere.', nomFicSeabed.ReflectivityErs);
        my_warndlg(Lang(str1,str2), 1);
    end
end

if exist(nomFicSeabed.BeamsErs, 'file')
    try
        delete(nomFicSeabed.BeamsErs)
    catch %#ok<CTCH>
        str1 = sprintf('Impossible de supprimer le fichier "%s", sans doute est-il ouvert.', nomFicSeabed.BeamsErs);
        str2 = sprintf('File "%s", could not be deleted, perhaps it is open somewhere.', nomFicSeabed.BeamsErs);
        my_warndlg(Lang(str1,str2), 1);
    end
end

if exist(nomFicSeabed.Reflectivity, 'dir')
    try
        rmdir(nomFicSeabed.Reflectivity, 's')
    catch %#ok<CTCH>
        str1 = sprintf('Impossible de supprimer le r�pertoire "%s", sans doute est-il ouvert.', nomFicSeabed.Reflectivity);
        str2 = sprintf('Directory "%s", could not be deleted, perhaps it is open somewhere.', nomFicSeabed.Reflectivity);
        my_warndlg(Lang(str1,str2), 1);
    end
end

if exist(nomFicSeabed.Beams, 'dir')
    try
        rmdir(nomFicSeabed.Beams, 's')
    catch %#ok<CTCH>
        str1 = sprintf('Impossible de supprimer le r�pertoire "%s", sans doute est-il ouvert.', nomFicSeabed.Beams);
        str2 = sprintf('Directory "%s", could not be deleted, perhaps it is open somewhere.', nomFicSeabed.Beams);
        my_warndlg(Lang(str1,str2), 1);
    end
end
