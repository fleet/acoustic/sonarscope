function flag = ALL_CleanBathyFromDTM_Histo(this, DTM, flagTideCorrection)

identBathymetry   = cl_image.indDataType('Bathymetry');
flag = testSignature(DTM, 'GeometryType', cl_image.indGeometryType('LatLong'), 'DataType', identBathymetry);
if ~flag
    return
end

FigPosition = [];

N = length(this);
str1 = 'Nettoyage de la bathym�trie des fichiers .all par comparaison � un MNT de r�f�rence.';
str2 = 'Bathymetry cleaning of .all files by comparison to a DTM.';
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    fprintf('%s\n%d / %d : %s\n', repmat('-',1,100), k, N, this(k).nomFic);
    
    [flag, DTM, FigPosition] = ALL_CleanBathyFromDTM_Histo_unitaire(this(k), DTM, FigPosition, flagTideCorrection);
    
    %% On demande si on veut traiter les autres fichiers
    
    if k ~= N
        flag = question_ContinueNextFile();
        if ~flag
            break
        end
    end
end
my_close(hw, 'MsgEnd')


function [flag, DTM, FigPosition] = ALL_CleanBathyFromDTM_Histo_unitaire(this, DTM, FigPosition, flagTideCorrection)

%% Lecture de DataDepth

[flag, DataDepth] = read_depth_bin(this, 'Memmapfile', 1);
if ~flag
    return
end

% RAjout� le 16/04/2015 � Barcelone pour fichier Patrick Jaussaud
% G:\PatrickJaussaud\ESNAUT2014\03122014-03\03122014-03-science\Cu\SMF_2040ForTrainingCourse\Mode2\0003_20141203_221723_raw.all
% o� DataDepth.depth fait [1010, 400] mais a ne fait que 996 pings, ce qui
% DataDepth.PingCounter qui sont � NaN
% correspond � des 
subPings = ~isnan(sum(DataDepth.PingCounter(:,:), 2));

%% Lecture des layers

a = get_Layer(this, {'Bathymetry'; 'AcrossDist'; 'AlongDist'}, 'CartoAuto', 1, 'TideCorrection', flagTideCorrection);
if isempty(a)
    flag = 0;
    return
end
Version = version_datagram(this);
switch Version
    case 'V1'
    case 'V2'
        % TODO : Cas de la V2 o� l'ordre des layers ramen�s ne respecte pas celui demand�
        a = a([3 1 2]);
end
DepthOrigine = get_Image(a(1));
DepthOrigine = DepthOrigine(:,:);

%% Aspiration du MNT en g�om�trie PingBeam

[flag, b] = sonarMosaiqueInv(a, 1, DTM(1));
if ~flag
    return
end
DepthFromDTM = get_Image(b);
DepthFromDTM = DepthFromDTM(:,:);

subNaN = isnan(DepthFromDTM) | isnan(DepthOrigine);
DepthFromDTM(subNaN) = NaN;
DepthOrigine(subNaN) = NaN;

%% Calcul de la diff�rence : bathy - MNT

Residuals = DepthFromDTM - DepthOrigine;

%% Nettoyage

OK = 2;
fig = [];

% Residuals = get_Image(c);
%     subNaN = (DataDepth.Mask ~= 0);
%     Residuals(subNaN)      = NaN;

disp('---------------------------------------------------------------')
while OK == 2
    [fig, FigPosition] = display_images(fig, this.nomFic, DepthFromDTM, DepthOrigine, Residuals, FigPosition);
    
    I = Residuals;
    Mask = DataDepth.Mask(subPings,:);
    
    MinVal = min(I(:));
    MaxVal = max(I(:));
    
    if isnan(MinVal)
        str1 = sprintf('Le fichier "%s" ne semble pas avoir de zone commune avec le MNT, on passe au suivant.', this.nomFic);
        str2 = sprintf('File "%s" does not seem to have a common part with the DTM, we switch to the next one.', this.nomFic);
        my_warndlg(Lang(str1,str2), 1);
        my_close(fig)
        return
    end
    bins = linspace(MinVal, MaxVal, 200);
    
    [N, bins, ~, SubBins] = histo1D(I, bins, 'Sampling', 1);  
	sub = getFlaggedBinsOfAnHistogram(bins, N);
    
    %% On recup�re les modifications
    
    if isempty(sub)
        my_close(fig)
        return
    end
    for k=1:length(sub)
        subPoubelle = SubBins{sub(k)};
        Residuals(subPoubelle) = NaN;
        DepthOrigine(subPoubelle) = NaN;
        Mask(subPoubelle) = 1;
    end
    [fig, FigPosition] = display_images(fig, this.nomFic, DepthFromDTM, DepthOrigine, Residuals, FigPosition);
    
    DataDepth.Mask(subPings, :) = Mask;
    flag = write_depth_bin(this, DataDepth);
    
    % TODO : a remettre en route quand le reste sera rod�
    %{
        MaxRange = max(Range(:));
        if MaxRange ~= MaxRangePrecedent
            MaxRangePrecedent = MaxRange;
            [rep, flag] = my_questdlg(Lang('TODO', ...
                'Maximum of "RayPathRange" has changed, it can improve "Seabed Imagery" file size and processing performances. Do you want to take advantage of it ?'));
            if ~flag
                return
            end
            if rep == 1
                nomFicSeabed = SimradSeabedNames_PingSamples(this.nomFic);
                supresssSeabedFiles(nomFicSeabed)
            end
        end
    %}
end
my_close(fig)


function [fig, FigPosition] = display_images(fig, nomFic, DepthFromDTM, DepthOrigine, Residuals, FigPosition)

[~, nomFic] = fileparts(nomFic);

if isempty(fig) || ~ishandle(fig)
    if isempty(FigPosition)
%         ScreenSize = get(0, 'ScreenSize');
%         fig = figure('Position', centrageFig(950, ScreenSize(4)-150));
        fig = figure;
        placeFigure('Fig', fig, 'Where', 'Full')
    else
        fig = figure('Position', FigPosition);
    end
end


figure(fig)
FigPosition = get(fig, 'Position');

subplot(1,3,1)
imagesc(DepthFromDTM); colormap(jet(256)); colorbar; axis xy;
title([nomFic 'Depth from DTM'], 'interpreter', 'none')

subplot(1,3,2)
imagesc(DepthOrigine); colormap(jet(256)); colorbar; axis xy;
title('Current Depth')

subplot(1,3,3)
imagesc(Residuals); colormap(jet(256)); colorbar; axis xy;
title([nomFic ' Residuals'], 'interpreter', 'none')


% TODO : fonction � mutualiser avec nettoyage histogrammique
%{
function supresssSeabedFiles(nomFicSeabed)

if exist(nomFicSeabed.ReflectivityErs, 'file')
    try
        delete(nomFicSeabed.ReflectivityErs)
    catch %#ok<CTCH>
        str1 = sprintf('Impossible de supprimer le fichier "%s", sans doute est-il ouvert.', nomFicSeabed.ReflectivityErs);
        str2 = sprintf('File "%s", could not be deleted, perhaps it is open somewhere.', nomFicSeabed.ReflectivityErs);
        my_warndlg(Lang(str1,str2), 1);
    end
end

if exist(nomFicSeabed.BeamsErs, 'file')
    try
        delete(nomFicSeabed.BeamsErs)
    catch %#ok<CTCH>
        str1 = sprintf('Impossible de supprimer le fichier "%s", sans doute est-il ouvert.', nomFicSeabed.BeamsErs);
        str2 = sprintf('File "%s", could not be deleted, perhaps it is open somewhere.', nomFicSeabed.BeamsErs);
        my_warndlg(Lang(str1,str2), 1);
    end
end

if exist(nomFicSeabed.Reflectivity, 'dir')
    try
        rmdir(nomFicSeabed.Reflectivity, 's')
    catch %#ok<CTCH>
        str1 = sprintf('Impossible de supprimer le r�pertoire "%s", sans doute est-il ouvert.', nomFicSeabed.Reflectivity);
        str2 = sprintf('Directory "%s", could not be deleted, perhaps it is open somewhere.', nomFicSeabed.Reflectivity);
        my_warndlg(Lang(str1,str2), 1);
    end
end

if exist(nomFicSeabed.Beams, 'dir')
    try
        rmdir(nomFicSeabed.Beams, 's')
    catch %#ok<CTCH>
        str1 = sprintf('Impossible de supprimer le r�pertoire "%s", sans doute est-il ouvert.', nomFicSeabed.Beams);
        str2 = sprintf('Directory "%s", could not be deleted, perhaps it is open somewhere.', nomFicSeabed.Beams);
        my_warndlg(Lang(str1,str2), 1);
    end
end
%}