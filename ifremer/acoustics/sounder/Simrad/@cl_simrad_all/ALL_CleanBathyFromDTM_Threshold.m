function flag = ALL_CleanBathyFromDTM_Threshold(this, MNT, Threshold, MaskIfNoDTM, flagTideCorrection)

identBathymetry   = cl_image.indDataType('Bathymetry');
flag = testSignature(MNT, 'GeometryType', cl_image.indGeometryType('LatLong'), 'DataType', identBathymetry);
if ~flag
    return
end

% useParallel = get_UseParallelTbx;

N = length(this);
str1 = 'Nettoyage de la bathym�trie des fichiers .all par comparaison � un MNT de r�f�rence.';
str2 = 'Bathymetry cleaning of .all files by comparison to a DTM.';
% if useParallel && (N > 1) % Contreproductif pour donn�es mission QUOI
%     [hw, DQ] = create_parforWaitbar(N, Lang(str1,str2));
%     parfor (k=1:N, useParallel)
%         fprintf('%s\n%d / %d : %s\n', repmat('-',1,100), k, N, this(k).nomFic);
%         unitaire_sonar_Simrad_CleanFromDTM(this(k), MNT, Threshold, MaskIfNoDTM, flagTideCorrection);
%         send(DQ, iFic);
%     end
%     my_close(hw, 'MsgEnd')
% else
    hw = create_waitbar(Lang(str1,str2), 'N', N);
    for k=1:N
        my_waitbar(k, N, hw)
        fprintf('%s\n%d / %d : %s\n', repmat('-',1,100), k, N, this(k).nomFic);
        
        flag = unitaire_sonar_Simrad_CleanFromDTM(this(k), MNT, Threshold, MaskIfNoDTM, flagTideCorrection);
    end
    my_close(hw, 'MsgEnd')
% end


function flag = unitaire_sonar_Simrad_CleanFromDTM(this, MNT, Threshold, MaskIfNoDTM, flagTideCorrection)

%% Lecture de DataDepth

[flag, DataDepth] = read_depth_bin(this, 'Memmapfile', 0);
if ~flag
    return
end
% RAjout� le 16/04/2015 � Barcelone pour fichier Patrick Jaussaud
% G:\PatrickJaussaud\ESNAUT2014\03122014-03\03122014-03-science\Cu\SMF_2040ForTrainingCourse\Mode2\0003_20141203_221723_raw.all
% o� DataDepth.depth fait [1010, 400] mais a ne fait que 996 pings, ce qui
% DataDepth.PingCounter qui sont � NaN
% correspond � des 
subPings = ~isnan(DataDepth.PingCounter);

%% Lecture des layers

a = get_Layer(this, {'Bathymetry'; 'AcrossDist'; 'AlongDist'}, 'CartoAuto', 1, 'TideCorrection', flagTideCorrection);

Version = version_datagram(this);
switch Version
    case 'V1'
    case 'V2'
        % TODO : Cas de la V2 o� l'ordre des layers ramen�s ne respecte pas celui demand�
        a = a([3 1 2]);
end

%% Aspiration du MNT en g�om�trie PingBeam

[flag, b] = sonarMosaiqueInv(a, 1, MNT);
if ~flag
    return
end

%% Calcul de la diff�rence : bathy - MNT

c = b - a(1);

%% Nettoyage

Mask = DataDepth.Mask(subPings,:);
Residuals = get_Image(c);
Residuals = Residuals(:,:);

subPoubelle = (Residuals < -Threshold) | (Residuals > Threshold);
Mask(subPoubelle) = 1;

if MaskIfNoDTM
    subPoubelle = isnan(b);
    Mask(subPoubelle) = 1;
end

DataDepth.Mask(subPings, :) = Mask;
flag = write_depth_bin(this, DataDepth);
