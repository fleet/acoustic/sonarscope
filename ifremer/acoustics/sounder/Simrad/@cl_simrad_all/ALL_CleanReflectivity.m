function ALL_CleanReflectivity(this, varargin)

[varargin, Memmapfile] = getPropertyValue(varargin, 'Memmapfile', 0); %#ok<ASGLU>

%% Traitement des fichiers

N = length(this);
str1 = 'Nettoyage des donn�es ALL bas� sur la r�flectivit�.';
str2 = 'ALL Data cleaning based on reflectivity.';
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
    my_waitbar(k, N, hw);
    
    str1 = sprintf('Traitement du fichier %d/%d : "%s" en cours.', k, N, this(k).nomFic);
    str2 = sprintf('Processing file %d/%d : "%s".', k, N, this(k).nomFic);
    fprintf(1,'%s\n', Lang(str1,str2));
    
    %% V�rification de l'existence du fichier Status
    
    [flag, DataDepth] = read_depth_bin(this(k), 'Memmapfile', Memmapfile);
    if ~flag
        continue
    end
    
    %% Lecture des layers
    
    [b, ~, ~, sublDepth] = get_Layer(this(k), 'ReflectivityFromSnippets', 'CartoAuto', 1);
    if isempty(b)
        continue
    end
    
    %% Lancement de SonarScope
    
    c = SonarScope(b, 'ButtonSave', 1); %, 'visuProfilX', 1, 'visuProfilY', 1, 'NbVoisins', 20); %, 'TypeInteractionSouris', 9);
    
    %% R�cup�ration du layer
    
    str1 = 'S�lectionnez le layer � partir duquel vous voulez r�cup�rer le masque.';
    str2 = 'Select the layer that will be used to get the mask.';
    DT = cl_image.indDataType('Reflectivity');
    ind = findIndLayerSonar(c, 1, 'DataType', DT, 'WithCurrentImage', 1, 'OnlyOneLayer', 'AtLeastOneLayer', ...
        'TitleWindowGUI', Lang(str1,str2));
    if isempty(ind)
        continue
    end
    
    Mask = isnan(c(ind));
    
    %% Report dans le masque
    
    M = DataDepth.Mask(sublDepth,:);
    M(Mask) = true;
    DataDepth.Mask(sublDepth,:) = M;
    
    %% Ecriture du masque

    flag = existCacheNetcdf(this(k).nomFic);
    if flag
        flag = save_signalNetcdf(this(k), 'Ssc_Depth', 'Mask', DataDepth.Mask);
    else
        flag = write_depth_bin(this(k), DataDepth);
    end
    
    %% On demande si on veut traiter les autres fichiers
    
    if k ~= N
        flag = question_ContinueNextFile();
        if ~flag
            break
        end
    end
end
my_close(hw, 'MsgEnd')
