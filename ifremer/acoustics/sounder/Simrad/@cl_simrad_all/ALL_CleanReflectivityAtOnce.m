function ALL_CleanReflectivityAtOnce(this)

%% Lecture de la r�flectivit�

ImageEmpilee = [];
N = length(this);
str1 = 'Nettoyage des donn�es ALL bas� sur la r�flectivit�.';
str2 = 'ALL Data cleaning based on reflectivity on piled files.';
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
    my_waitbar(k, N, hw);
    str1 = sprintf('Traitement du fichier %d/%d : "%s" en cours.', k, N, this(k).nomFic);
    str2 = sprintf('Processing file %d/%d : "%s".', k, N, this(k).nomFic);
    fprintf(1, '%s\n', Lang(str1,str2));
        
    %% Lecture de la r�flectivit�
    
    [flag, DataDepth] = read_depth_bin(this(k));
    if ~flag
        continue
    end
    nbPings(k) = size(DataDepth.ReflectivityFromSnippets, 1); %#ok<AGROW>
    
    %% Lecture des layers

    im = DataDepth.ReflectivityFromSnippets;
    im(DataDepth.Mask == 1) = NaN;
    
    % TODO : Ridha, est-tu s�r de vouloir garder ces 2 instructions ?
    im(im < -45) = NaN;
    im(im > 0)   = NaN;

    %% Filtre spike
    
    im = filterSpike(im, 3, 0, 0, [], []);
    im = im(:,:,1);

    %% Concataination de l'image
    
    ImageEmpilee = [ImageEmpilee; im]; %#ok<AGROW>
end

%% Lancement de SonarScope en mode modal

c = SonarScope(ImageEmpilee, 'ButtonSave', 1, 'ColormapIndex', 2);

Mask = isnan(c(1));

%% Ecriture des masques

ind = 1;
for k=1:N
    
    %% Extraction du masque du fichier
    
    M = Mask(ind:(ind+nbPings(k)-1),:);
    
    %% Ecriture du masque
    
    flag = existCacheNetcdf(this(k).nomFic);
    if flag
        flag = save_signalNetcdf(this(k), 'Ssc_Depth', 'Mask', M);
    else
        flag = write_depth_bin(this(k), DataDepth);
    end
    
    ind = ind + nbPings(k);
end
my_close(hw, 'MsgEnd')
