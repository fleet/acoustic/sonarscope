function ALL_CreateFileMarkers(this, TypeDatagram, nomFicMarker, PlotNav, varargin)

[varargin, IndNavUnit] = getPropertyValue(varargin, 'IndNavUnit', []); %#ok<ASGLU>

%% Traitement des fichiers

Ligne = {};
NbFic = length(this);
str1 = 'Lecture des .all en cours.';
str2 = 'Reading the .all files';
hw = create_waitbar(Lang(str1,str2), 'N', NbFic);
for k=1:NbFic
    my_waitbar(k, NbFic, hw);
    
    a = this(k);
    if isempty(a)
        fprintf('fichier vide : %s\n', a.nomFic);
        continue
    end
    
    switch TypeDatagram
        case 'Runtime'
            [flag, DataRuntime] = SSc_ReadDatagrams(a.nomFic, 'Ssc_Runtime'); % Non test� ici
            if ~flag
                continue
            end
            T = DataRuntime.Time.timeMat;
            
        case 'SoundSpeedProfile'
            [flag, DataSSP] = SSc_ReadDatagrams(a.nomFic, 'Ssc_SoundSpeedProfile', 'Memmapfile', -1); % Non test� ici
            if ~flag
                continue
            end
            T = DataSSP.TimeStartOfUse.timeMat;
            
        case 'InstallationParamaters'
            [flag, DataInstallationParameters] = read_installationParameters_bin(a);
            if ~flag
                continue
            end
            try
                T = DataInstallationParameters.Time.timeMat;
            catch % Pour XSF
                T = datenum(DataInstallationParameters.Datetime);
            end
        otherwise
            'Pas fait'
            return
    end
    
    for k2=1:length(T)
        Ligne{end+1} = timeMat2str(T(k2)); %#ok<AGROW>
    end
end
my_close(hw, 'MsgEnd');

%% Cr�ation du fichier des markers

fid = fopen(nomFicMarker, 'w+t');
for k=1:length(Ligne)
    fprintf(fid, 'SonarTime         : %s\n', Ligne{k});
end
fclose(fid);

%% Affichage de la navigation et des markers

if PlotNav
    Fig = ALL_PlotNav(this, 'Name', ['Navigation & "' TypeDatagram '" datagrams'], 'IndNavUnit', IndNavUnit);
    plot_navigation_ImportMarkers('Fig', Fig, 'NomFicMarkers', nomFicMarker)
end
