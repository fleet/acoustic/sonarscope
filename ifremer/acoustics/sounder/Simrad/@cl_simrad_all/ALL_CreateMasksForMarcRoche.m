function ALL_CreateMasksForMarcRoche(this, nomFicPoints, nomDirOut, pourcentageDepth)

useParallel = get_UseParallelTbx;

%% Lecture du fichier de points

if iscell(nomFicPoints)
    nomFicPoints = nomFicPoints{1};
end

fid = fopen(nomFicPoints, 'r');
header = fgetl(fid); %#ok<NASGU>
C = textscan(fid, '%s%f%f', 'delimiter', ';');
fclose(fid);

NameMask  = C{1};
LonCentre = C{2};
LatCentre = C{3};

% Fig = plot_navigation_Figure([]);
% SonarTime = zeros(size(LonCentre));
% plot_navigation('Centers', Fig, LonCentre, LatCentre, SonarTime, []);
% hold on;

FigUtils.createSScFigure; PlotUtils.createSScPlot(LonCentre, LatCentre, '*'); grid on; axisGeo; hold on;
Axe = gca;
drawnow
% XLim = get(Axe, 'XLim');
% YLim = get(Axe, 'YLim');

%% Cr�ation des masques correspondant � chaque fichier .all
        
N = length(this);
str1 = 'Parcours des .all';
str2 = 'Processing .all files';
if useParallel && (N > 1)
	[hw, DQ] = create_parforWaitbar(N, Lang(str1,str2));
    parfor (k=1:N, useParallel)
        fprintf('\n%d / %d : %s', k, N,  this(k).nomFic);
        ALL_CreateMasksForMarcRoche_unitaire(this(k), NameMask, LatCentre, LonCentre, nomDirOut, pourcentageDepth, ...
            'isUsingParfor', 1);
        send(DQ, k);
    end
    my_close(hw, 'MsgEnd')
else
    hw = create_waitbar(Lang(str1,str2), 'N', N);
    for k=1:N
        my_waitbar(k, N, hw)
        fprintf('\n%d / %d : %s', k, N,  this(k).nomFic);
        ALL_CreateMasksForMarcRoche_unitaire(this(k), NameMask, LatCentre, LonCentre, nomDirOut, pourcentageDepth, 'Axe', Axe);
    end
    my_close(hw, 'MsgEnd')
end

% if ~useParallel && ishandle(Axe)
%     set(Axe, 'XLim', XLim, 'YLim', YLim)
%     PlotUtils.createSScPlot(LonCentre, LatCentre, '*'); grid on; axisGeo; hold on;
% end


function ALL_CreateMasksForMarcRoche_unitaire(this, NameMask, LatCentre, LonCentre, nomDirOut, pourcentageDepth, varargin)

global isUsingParfor %#ok<GVMIS>

[varargin, Axe]            = getPropertyValue(varargin, 'Axe',            []);
[varargin, isUsingParfor]  = getPropertyValue(varargin, 'isUsingParfor',  0); %#ok<ASGLU>

Window = [5 5];

nbPoints = length(LatCentre);

[X, Carto] = get_Layer(this, {'Depth'; 'AcrossDist'; 'AlongDist'; 'ReflectivityFromSnippets'}, 'CartoAuto', 1, 'Mute', 1);
Depth      = X(1);
AcrossDist = X(2);
AlongDist  = X(3);
Reflec     = X(4);

Z = get_Image(Depth);
Z = Z * pourcentageDepth/100;

b = [Depth AcrossDist AlongDist];
[flag, c] = sonarCalculCoordGeo(b, 1, 'NoWaitbar', 1);
if ~flag
    return
end
Lat = c(1);
Lon = c(2);

[X1, Y1] = latlon2xy(Carto, get_Image(Lat), get_Image(Lon));

[XCentre, YCentre] = latlon2xy(Carto, LatCentre, LonCentre);
% figure; plot(X1(:), Y1(:), '.'); grid on; hold on; plot(XCentre, YCentre, '*r');

for k2=1:nbPoints
    d = sqrt((X1(:) - XCentre(k2)) .^ 2 + (Y1(:) - YCentre(k2)) .^2);
    p = (d < -Z(:));
    n = sum(p);
    if n > 0
        iMilieu = floor(size(X1,2) / 2);
        p = reshape(p, size(Z));
        nb = sum(p,2);
        %             figure; plot(nb); grid on;
        [~,iPing] = max(nb);
        d = sqrt((X1(:,iMilieu) - X1(iPing,iMilieu)) .^ 2 + (Y1(:,iMilieu) - Y1(iPing,iMilieu)).^ 2);
        subPingMask = find(d < abs(mean(Z(iPing,:), 'omitnan')));
        % figure; plot(subPingMask); grid on;
        
        M = true(length(subPingMask), size(X1, 2));
        
        identMask  = cl_image.indDataType('Mask');
        MaskPingBeam = inherit(Depth, M, 'subx', 1:size(X1, 2), 'suby', subPingMask, ...
            'Unit', ' ', 'DataType', identMask, 'ValNaN', 0, 'AppendName', NameMask{k2});
        
        Lat2    = extraction(Lat,    'suby', subPingMask);
        Lon2    = extraction(Lon,    'suby', subPingMask);
        Reflec2 = extraction(Reflec, 'suby', subPingMask);
        
        [flag, gridSize] = computeMeanDistanceInterPings(Reflec2);
        if ~flag
            continue
        end
        
        [flag, MaskLatLong] = sonar_mosaique(MaskPingBeam, Lat2, Lon2, gridSize, 'NoWaitbar', 1);
        if ~flag
            continue
        end
        MaskLatLong = WinFillNaN(MaskLatLong, 'window', Window);
        
        %             nomFicMask = fullfile(nomDirOut, [NameMask{k2} '.ers']);
        nomFicMask = MaskLatLong.Name;
        nomFicMask = fullfile(nomDirOut, [nomFicMask '.ers']);
        flag = export_ermapper(MaskLatLong, nomFicMask);
        if ~flag
            return
        end
        
        fprintf('Association : %s - %s ping %d\n', NameMask{k2}, this.nomFic, iPing);
        
        [flag, Reflec2LatLong] = sonar_mosaique(Reflec2, Lat2, Lon2, gridSize, 'NoWaitbar', 1); %#ok<ASGLU>
        Reflec2LatLong = WinFillNaN(Reflec2LatLong, 'window', Window);
        Reflec2LatLong.CLim = '1%';
        
        nomFicReflec = strrep(nomFicMask, 'Mask.ers', 'Reflectivity.ers');
        flag = export_ermapper(Reflec2LatLong, nomFicReflec); %#ok<NASGU>
        
        nomFicReflec = strrep(nomFicMask, 'Mask.ers', 'Reflectivity.tif');
        flag = export_geoTiff32Bits(Reflec2LatLong, nomFicReflec, 'Mute', 1); %#ok<NASGU>
        
        nomFicReflec = strrep(nomFicMask, 'Mask.ers', 'Reflectivity.jpg');
        flag = export_tif(Reflec2LatLong, nomFicReflec, 'Mute', 1); %#ok<NASGU>
        
        if ishandle(Axe)
            imagesc(Reflec2LatLong, 'Axe', Axe);
            % set(Axe, 'XLim', XLim, 'YLim', YLim)
            PlotUtils.createSScPlot(LonCentre(k2), LatCentre(k2), '*r');
        end
    end
end
