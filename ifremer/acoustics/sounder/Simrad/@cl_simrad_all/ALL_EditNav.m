function flag = ALL_EditNav(this, varargin)

[varargin, CleanInterpolation] = getPropertyValue(varargin, 'CleanInterpolation', true);
[varargin, IndNavUnit]         = getPropertyValue(varargin, 'IndNavUnit',         []); %#ok<ASGLU>

N = length(this);
str1 = 'Edition de la navigation.';
str2 = 'Edit the navigation';
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
        
    %% Lecture de la navigation
    
    [flag, DataPosition] = read_position_bin(this(k), 'IndNavUnit', IndNavUnit, 'LogSilence', 0);
    if ~flag
        continue
    end
    Latitude  = DataPosition.Latitude(:,:);
    Longitude = DataPosition.Longitude(:,:);
    Datetime  = DataPosition.Datetime;
    
    %% Extraction de l'immersion
    
    [flag, Z, DataDepth, DataAttitude] = create_signal_Immersion(this(k));
    if flag == 1
        for k2=1:size(Z.Data,2)
            tabImmersion{k}(:,k2) = my_interp1(Z.Datetime, Z.Data(:,k2), Datetime); %#ok<AGROW>
        end
        pppp = tabImmersion{k}(:,1);
        immersionSample = YSample('name', 'Immersion', 'unit', 'm', 'data', pppp, 'marker', '.');
    else
        immersionSample = YSample.empty;
    end
    
    %% Extraction du cap (ne pas relire l'attitude une deuxi�me fois)
    
    headingVesselSample = my_interp1(DataAttitude.Datetime, DataAttitude.Heading, Datetime);
    headingVesselSample = YSample('name', 'Heading', 'unit', 'deg', 'data', headingVesselSample, 'marker', '.');
    
    %% Cr�ation des instances ClNavigation

    T = Datetime;
    timeSample = XSample('name', 'time', 'data', T);
    
    latSample = YSample('data', Latitude, 'marker', '.');
    latSignal = ClSignal('ySample', latSample, 'xSample', timeSample);
   
    lonSample = YSample('data', Longitude, 'marker', '.');
	lonSignal = ClSignal('ySample', lonSample, 'xSample', timeSample);
   
    headingVesselSignal = ClSignal('ySample', headingVesselSample, 'xSample', timeSample);
    immersionSignal     = ClSignal('ySample', immersionSample,     'xSample', timeSample);
    
    [~, name] = fileparts(this(k).nomFic);
    nav(k) = ClNavigation(latSignal, lonSignal, ...
        'immersionSignal',     immersionSignal, ...
        'headingVesselSignal', headingVesselSignal, ...
        'name', name); %#ok<AGROW>
%     nav(k) = ClNavigation(latSignal, lonSignal, ...
%         'name', name); %#ok<AGROW>
end
my_close(hw);

%% Create the GUI

a = NavigationDialog(nav, 'Title', 'Navigation', 'CleanInterpolation', CleanInterpolation);
% a.editProperties();
pause(0.5)
a.openDialog();
if ~a.okPressedOut
    flag = 0;
    return
end
    
%% Export des navigations 

str1 = 'Voulez-vous sauvegarder cette nouvelle navigation dans le cache SSc des .all ?';
str2 = 'Do you want to save this navigation in the SSc cach directories of the .all ?';
[rep, flag] = my_questdlg(Lang(str1,str2));
if ~flag
    return
end

%% Sauvegarde des signaux corrig�s dans le r�pertoire cache de SSc
    
if rep == 1
    nav = a.signalContainerList;
    str1 = 'Sauvegarde de la navigation.';
    str2 = 'Export of the navigation';
    hw = create_waitbar(Lang(str1,str2), 'N', N);
    for k=1:N
        my_waitbar(k, N, hw)
        
        Latitude             = nav(k).getLatitudeSignalSample().data;
        Longitude            = nav(k).getLongitudeSignalSample().data;
        tabImmersion{k}(:,1) = nav(k).getImmersionSignalSample().data;
        Heading              = nav(k).getHeadingVesselSignalSample().data;
        tImmersion           = nav(k).getTimeSignalSample().data;
        
        %% Save Time
        
% TODO : pour l'instant pas modifiable dans NavigationDialog
%         flag = save_signal(this(k), 'Ssc_Position', 'Time',  Time);
%         if ~flag
%             return
%         end
        
        %% Save Latitude

        flag = save_signal(this(k), 'Ssc_Position', 'Latitude',  Latitude);
        if ~flag
            return
        end
        
        %% Save Longitude
        
        flag = save_signal(this(k), 'Ssc_Position', 'Longitude', Longitude);
        if ~flag
            return
        end
        
        %% Save Immersion
        
        if ~isempty(tabImmersion{k})
            [flag, Z, DataDepth, DataAttitude] = set_KM_TransducerDepth(this(k), tImmersion, tabImmersion{k}); %#ok<ASGLU> %, 'DataDepth', DataDepth, 'DataAttitude', DataAttitude);
            if flag
                % figure; plot(Z.Datetime, Z.Data); grid on
                TransducerDepth = Z.Data;
                flag = save_signal(this(k), 'Ssc_Depth', 'TransducerDepth', TransducerDepth);
                if ~flag
                    str1 = sprintf('L''import de la navigation pour le fichier "%s" n''a pas pu se faire.', this(k).nomFic);
                    str2 = sprintf('The import of navigation for "%s" failed.', this(k).nomFic);
                    my_warndlg(Lang(str1,str2), 0);
                    return
                end
            end
        end
        
        %% Save Heading
        
        if ~isempty(Heading)
            Heading = my_interp1(tImmersion, Heading, DataDepth.Datetime, 'linear', 'extrap');
            flag = save_signal(this(k), 'Ssc_Depth', 'Heading', Heading);
            if ~flag
                str1 = sprintf('L''import de la navigation pour le fichier "%s" n''a pas pu se faire.', this(k).nomFic);
                str2 = sprintf('The import of navigation for "%s" failed.', this(k).nomFic);
                my_warndlg(Lang(str1,str2), 0);
                return
            end
        end
    end
end
my_close(hw, 'MsgEnd');

flag = 1;
