function ALL_ExportAttitude(this, typeFormat, varargin)

[varargin, nomDirOut] = getPropertyValue(varargin, 'nomDirOut', []); %#ok<ASGLU>

str1 = 'Export de l''attitude des fichiers .all';
str2 = 'Export attitude of .all files';
N = length(this);
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
    my_waitbar(k, N, hw);
    switch typeFormat
        case 1 % ASCII
            flag = ALL_ExportAttitude_ASCII_unitaire(this(k), nomDirOut);
            if ~flag
                return
            end
            
        case 2 % NetCDF
            flag = ALL_ExportAttitude_NetCDF_unitaire(this(k), nomDirOut);
            if ~flag
                return
            end
    end
end
my_close(hw, 'MsgEnd')

%% Concat�nation des fichiers individuels en un seul fichier

switch typeFormat
    case 1 % ASCII
        ALL_ExportAttitude_OneFile_ASCII(this, nomDirOut)
    case 2 % NetCDF
        ALL_ExportAttitude_OneFile_NetCDF(this, nomDirOut)
end


function ALL_ExportAttitude_OneFile_ASCII(this, nomDirOut)

%% Ecriture du fichier

if isempty(nomDirOut)
    nomDirOut = fileparts(this(1).nomFic);
end

nomFicCsv = fullfile(nomDirOut, 'AllFiles-Attitude.csv');
fidOut = fopen(nomFicCsv, 'w+t');

%% Boucle sur les fichiers ASCII

flagFirstTime = 1;
for k=1:length(this)
    [~, nomFicSeul] = fileparts(this(k).nomFic);
    nomFicCsv = fullfile(nomDirOut, [nomFicSeul '-Attitude.csv']);
    fidIn = fopen(nomFicCsv, 'r');
    if fidIn == -1
        messageErreurFichier(nomFicCsv, 'WriteFailure');
        continue
    end
    
    while feof(fidIn) == 0
        str = fgetl(fidIn);
        if flagFirstTime
            strHeader = str;
            flagFirstTime = 0;
        else
            if strcmp(str, strHeader)
                continue
            end
        end
        fprintf(fidOut, '%s\n', str);
    end
    
    fclose(fidIn);
end
fclose(fidOut);


function ALL_ExportAttitude_OneFile_NetCDF(this, nomDirOut)

%% Ecriture du fichier

if isempty(nomDirOut)
    nomDirOut = fileparts(this(1).nomFic);
end

nomFicOut = fullfile(nomDirOut, 'AllFiles-Attitude.nc');
if exist(nomFicOut, 'file')
    delete(nomFicOut)
end

%% Boucle sur les fichiers ASCII

for k=1:length(this)
    [~, nomFicSeul] = fileparts(this(k).nomFic);
    nomFicIn = fullfile(nomDirOut, [nomFicSeul '-Attitude.nc']);
    
    finfo = ncinfo(nomFicIn);
    nomDim = finfo.Dimensions.Name;
    
    for k2=1:length(finfo.Variables)
        X = ncread(nomFicIn, finfo.Variables(k2).Name);
        if k == 1
            Var{k2} = X; %#ok<AGROW>
        else
            Var{k2} = [Var{k2}; X]; %#ok<AGROW>
        end
    end
end

%% Get dimensions

nbSamples = length(Var{1});

%% Write variables

for k2=1:length(finfo.Variables)
    nccreate(nomFicOut, finfo.Variables(k2).Name, ...
        'Dimensions', {nomDim, nbSamples}, ...
        'FillValue', NaN);
    ncwrite(nomFicOut, finfo.Variables(k2).Name,  Var{k2});
end



function flag = ALL_ExportAttitude_ASCII_unitaire(this, nomDirOut)

%% Comma or not comma ?

sep = getCSVseparator;

%% Lecture de la navigation

[flag, DataHeight] = SSc_ReadDatagrams(this.nomFic, 'Ssc_Height');
if ~flag
    return
end

%% Lecture de l'attitude

[flag, DataAttitude] = read_attitude_bin(this, 'Memmapfile', -1); 
if ~flag
    return
end
TimeAttitude = DataAttitude.Datetime;

%% Interpolation des donn�es de position

QualityIndicator  = my_interp1_Extrap_PreviousThenNext(DataHeight.Datetime, double(DataHeight.HeightType), TimeAttitude);
GeoidalSeparation = interp1(DataHeight.Datetime, double(DataHeight.Height),     TimeAttitude, 'linear',   'extrap');
k0 = find(~isnan(QualityIndicator), 1, 'first');
QualityIndicator(1:k0) = QualityIndicator(k0);

%% Ecriture du fichier

if isempty(nomDirOut)
    [nomDirOut, nomFicSeul] = fileparts(this.nomFic);
end

nomFicCsv = fullfile(nomDirOut, [nomFicSeul '-Attitude.csv']);
fid = fopen(nomFicCsv, 'w+t');
if fid == -1
     messageErreurFichier(nomFicCsv, 'WriteFailure');
    return
end

fprintf(fid, 'Time%sRoll%sPitch%sHeave%sHeading%sQuality%sAltitude\n', sep, sep, sep, sep, sep, sep);

TimeAttitude.Format = 'yyyyMMdd HH:mm:ss.SSS';
N = length(QualityIndicator);
for k=1:N
	fprintf(fid, '%s%s%f%s%f%s%f%s%f%s%d%s%f\n', char(TimeAttitude(k)), ...
        sep, DataAttitude.Roll(k), sep, DataAttitude.Pitch(k), sep, DataAttitude.Heave(k), sep, DataAttitude.Heading(k), ...
        sep, QualityIndicator(k), sep, GeoidalSeparation (k));
end
fclose(fid);



function flag = ALL_ExportAttitude_NetCDF_unitaire(this, nomDirOut)

%% Lecture de la navigation

[flag, DataHeight] = SSc_ReadDatagrams(this.nomFic, 'Ssc_Height');
if ~flag
    return
end

%% Lecture de l'attitude

[flag, DataAttitude] = read_attitude_bin(this, 'Memmapfile', -1); 
if ~flag
    return
end
TimeAttitude = DataAttitude.Datetime;

%% Interpolation des donn�es de position

QualityIndicator  = my_interp1_Extrap_PreviousThenNext(DataHeight.Datetime, double(DataHeight.HeightType), TimeAttitude);
GeoidalSeparation = interp1(DataHeight.Datetime, double(DataHeight.Height),     TimeAttitude, 'linear',   'extrap');
k0 = find(~isnan(QualityIndicator), 1, 'first');
QualityIndicator(1:k0) = QualityIndicator(k0);

%% Ecriture du fichier

if isempty(nomDirOut)
    [nomDirOut, nomFicSeul] = fileparts(this.nomFic);
end

nomFicNetCDF = fullfile(nomDirOut, [nomFicSeul '-Attitude.nc']);
if exist(nomFicNetCDF, 'file')
    delete(nomFicNetCDF)
end

%% Get dimensions

nbSamples = length(TimeAttitude);

%% Prepare TimeTx

format longG
TimeUTC = posixtime(TimeAttitude);

%% Write Time UTC

nccreate(nomFicNetCDF, 'TimeUTC', ...
        'Dimensions', {'nbSamples', nbSamples}, ...
        'FillValue', NaN);
ncwrite(nomFicNetCDF, 'TimeUTC', TimeUTC);

%% Write Roll

nccreate(nomFicNetCDF, 'Roll', ...
        'Dimensions', {'nbSamples', nbSamples}, ...
        'FillValue', NaN);
ncwrite(nomFicNetCDF, 'Roll', DataAttitude.Roll(:));

%% Write PitchTx

nccreate(nomFicNetCDF, 'Pitch', ...
        'Dimensions', {'nbSamples', nbSamples}, ...
        'FillValue', NaN);
ncwrite(nomFicNetCDF, 'Pitch', DataAttitude.Pitch(:));

%% Write Heave

nccreate(nomFicNetCDF, 'Heave', ...
        'Dimensions', {'nbSamples', nbSamples}, ...
        'FillValue', NaN);
ncwrite(nomFicNetCDF, 'Heave', DataAttitude.Heave(:));

%% Write Heading

nccreate(nomFicNetCDF, 'Heading', ...
        'Dimensions', {'nbSamples', nbSamples}, ...
        'FillValue', NaN);
ncwrite(nomFicNetCDF, 'Heading', DataAttitude.Heading(:));

%% Write QualityIndicator

nccreate(nomFicNetCDF, 'QualityIndicator', ...
        'Dimensions', {'nbSamples', nbSamples}, ...
        'FillValue', NaN);
ncwrite(nomFicNetCDF, 'QualityIndicator', QualityIndicator);

%% Write GeoidalSeparation 

nccreate(nomFicNetCDF, 'GeoidalSeparation', ...
        'Dimensions', {'nbSamples', nbSamples}, ...
        'FillValue', NaN);
ncwrite(nomFicNetCDF, 'GeoidalSeparation', GeoidalSeparation );

format long
