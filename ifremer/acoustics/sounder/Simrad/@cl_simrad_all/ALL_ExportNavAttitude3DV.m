function ALL_ExportNavAttitude3DV(this, nomFicXml, varargin)

[varargin, IndNavUnit] = getPropertyValue(varargin, 'IndNavUnit', []); %#ok<ASGLU>

k = 0;
ColorOrder = get(gca, 'ColorOrder');
nbFic = length(this);
str1 = 'Export de la navigation et de l''attitude des .all dans GLOBE';
str2 = 'Navigation and attitude export of the .all files into GLOBE.';
hw = create_waitbar(Lang(str1,str2), 'N', nbFic);
for kFic=1:nbFic
    my_waitbar(kFic, nbFic, hw);
    [flag, T, Lat1, Lon1, Immersion1, Heading1, Roll1, Pitch1, Heave1, ...
        Name1, Label1] = get_PositionAttitude_file(this(kFic), IndNavUnit);
    if ~flag
        continue
    end
    k = k + 1;
    Lat{k}       = Lat1;       %#ok<AGROW>
    Lon{k}       = Lon1;       %#ok<AGROW>
    Immersion{k} = Immersion1; %#ok<AGROW>
    Heading{k}   = Heading1;   %#ok<AGROW>
    Roll{k}      = Roll1;      %#ok<AGROW>
    Pitch{k}     = Pitch1;     %#ok<AGROW>
    Heave{k}     = Heave1;     %#ok<AGROW>
    Name{k}      = Name1;      %#ok<AGROW>
    Label{k}     = Label1;     %#ok<AGROW>
    
    kMod = 1 + mod(k-1, size(ColorOrder,1));
    Color{k} = ColorOrder(kMod,:); %#ok<AGROW>
    Time{k}  = datetime(T, 'ConvertFrom', 'datenum'); %#ok<AGROW>
end
my_close(hw, 'MsgEnd')

if k == 0
    return
end

plot_navigationExportXML(Lat, Lon, Time, Color, Name, Label, nomFicXml, ...
    'Immersion', Immersion, 'Heading', Heading, 'Roll', Roll, 'Pitch', Pitch, 'Heave', Heave);


% %% Cr�ation du fichier XML d�crivant la donn�e 
% 
% DPref.ItemName  = 'item';
% DPref.StructItem = false;
% DPref.CellItem   = true;
% xml_write(nomFicXml, Nav, 'Nav', DPref);
% flag = exist(nomFicXml, 'file');
% if ~flag
%     messageErreur(nomFicXml)
%     return
% end

function [flag, Time, Lat, Lon, Immersion, Heading, Roll, Pitch, Heave, Name, Label] ...
    = get_PositionAttitude_file(this, IndNavUnit)

Time      = [];
Lat       = [];
Lon       = [];
Immersion = [];
Heading   = [];
Roll      = [];
Pitch     = [];
Heave     = [];
Name      = [];
Label     = [];

[flag, DataDepth] = read_depth_bin(this, 'Memmapfile', 1);
if ~flag
    return
end
Time      = DataDepth.Time.timeMat;
Immersion = -DataDepth.TransducerDepth(:);

[flag, DataPosition] = read_position_bin(this, 'IndNavUnit', IndNavUnit);
if ~flag
    return
end

[~,Name] = fileparts(this.nomFic);
Name = [Name '.all'];

T = DataPosition.Time;
T = T.timeMat;
Label = sprintf(' %s %s ', timeMat2str(T(1)), timeMat2str(T(end)));

Lat = my_interp1(DataPosition.Time, DataPosition.Latitude, DataDepth.Time);
Lon = my_interp1_longitude(DataPosition.Time, DataPosition.Longitude, DataDepth.Time);

[flag, DataAttitude] = read_attitude_bin(this);
if ~flag
    return
end

Roll    = my_interp1(DataAttitude.Time, DataAttitude.Roll,  DataDepth.Time);
Pitch   = my_interp1(DataAttitude.Time, DataAttitude.Pitch, DataDepth.Time);
Heave   = my_interp1(DataAttitude.Time, DataAttitude.Heave,  DataDepth.Time);
Heading = my_interp1_headingDeg(DataAttitude.Time, DataAttitude.Heading,  DataDepth.Time);
