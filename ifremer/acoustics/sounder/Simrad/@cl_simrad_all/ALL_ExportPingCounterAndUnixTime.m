function ALL_ExportPingCounterAndUnixTime(this, typeFormat, varargin)

[varargin, nomDirOut] = getPropertyValue(varargin, 'nomDirOut', []); %#ok<ASGLU>

str1 = 'Export du numéro de ping et de l''heure Unix des fichiers .all';
str2 = 'Export ping number and unix time of .all files';
N = length(this);
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
    my_waitbar(k, N, hw);
    switch typeFormat
        case 1 % ASCII
            flag = ALL_ExportPingCounterAndUnixTime_ASCII_unitaire(this(k), nomDirOut);
            if ~flag
                return
            end
            
        case 2 % NetCDF
            flag = ALL_ExportPingCounterAndUnixTime_NetCDF_unitaire(this(k), nomDirOut);
            if ~flag
                return
            end
    end
end
my_close(hw, 'MsgEnd')

%% Concaténation des fichiers individuels en un seul fichier

switch typeFormat
    case 1 % ASCII
        ALL_ExportPingCounterAndUnixTime_OneFile_ASCII(this, nomDirOut)
    case 2 % NetCDF
        ALL_ExportPingCounterAndUnixTime_OneFile_NetCDF(this, nomDirOut)
end


function ALL_ExportPingCounterAndUnixTime_OneFile_ASCII(this, nomDirOut)

%% Ecriture du fichier

if isempty(nomDirOut)
    nomDirOut = fileparts(this(1).nomFic);
end

nomFicCsv = fullfile(nomDirOut, 'AllFiles-PingCounterAndUnixTime.csv');
fidOut = fopen(nomFicCsv, 'w+t');

%% Boucle sur les fichiers ASCII

flagFirstTime = 1;
for k=1:length(this)
    [~, nomFicSeul] = fileparts(this(k).nomFic);
    nomFicCsv = fullfile(nomDirOut, [nomFicSeul '-PingCounterAndUnixTime.csv']);
    fidIn = fopen(nomFicCsv, 'r');
    if fidIn == -1
        messageErreurFichier(nomFicCsv, 'WriteFailure');
        continue
    end
    
    while feof(fidIn) == 0
        str = fgetl(fidIn);
        if flagFirstTime
            strHeader = str;
            flagFirstTime = 0;
        else
            if strcmp(str, strHeader)
                continue
            end
        end
        fprintf(fidOut, '%s\n', str);
    end
    
    fclose(fidIn);
end
fclose(fidOut);


function ALL_ExportPingCounterAndUnixTime_OneFile_NetCDF(this, nomDirOut)

%% Ecriture du fichier

if isempty(nomDirOut)
    nomDirOut = fileparts(this(1).nomFic);
end

nomFicOut = fullfile(nomDirOut, 'AllFiles-PingCounterAndUnixTime.nc');
if exist(nomFicOut, 'file')
    delete(nomFicOut)
end

%% Boucle sur les fichiers ASCII

for k=1:length(this)
    [~, nomFicSeul] = fileparts(this(k).nomFic);
    nomFicIn = fullfile(nomDirOut, [nomFicSeul '-PingCounterAndUnixTime.nc']);
    
    finfo = ncinfo(nomFicIn);
    nomDim = finfo.Dimensions.Name;
    
    for k2=1:length(finfo.Variables)
        X = ncread(nomFicIn, finfo.Variables(k2).Name);
        if k == 1
            Var{k2} = X; %#ok<AGROW>
        else
            Var{k2} = [Var{k2}; X]; %#ok<AGROW>
        end
    end
end

%% Get dimensions

nbSamples = length(Var{1});

%% Write variables

for k2=1:length(finfo.Variables)
    nccreate(nomFicOut, finfo.Variables(k2).Name, ...
        'Dimensions', {nomDim, nbSamples}, ...
        'FillValue', NaN);
    ncwrite(nomFicOut, finfo.Variables(k2).Name,  Var{k2});
end



function flag = ALL_ExportPingCounterAndUnixTime_ASCII_unitaire(this, nomDirOut)

%% Comma or not comma ?

sep = getCSVseparator;

%% Lecture de la navigation

[flag, DataDepth] = read_depth_bin(this, 'Memmapfile', 0);
if ~flag
    return
end
TimeDepth = DataDepth.Datetime;

%% Ecriture du fichier

if isempty(nomDirOut)
    [nomDirOut, nomFicSeul] = fileparts(this.nomFic);
end

nomFicCsv = fullfile(nomDirOut, [nomFicSeul '-PingCounterAndUnixTime.csv']);
fid = fopen(nomFicCsv, 'w+t');
if fid == -1
     messageErreurFichier(nomFicCsv, 'WriteFailure');
    return
end

fprintf(fid, 'Time%sPingCounter\n', sep);

TimeDepth = posixtime(TimeDepth);
for k=1:DataDepth.Dimensions.nbPings
	fprintf(fid, '%13.3f%s%f\n', TimeDepth(k), sep, DataDepth.PingCounter(k));
end
fclose(fid);



function flag = ALL_ExportPingCounterAndUnixTime_NetCDF_unitaire(this, nomDirOut)

%% Lecture de la navigation

[flag, DataDepth] = read_depth_bin(this, 'Memmapfile', -1);
if ~flag
    return
end
TimeDepth = DataDepth.Datetime;

%% Ecriture du fichier

if isempty(nomDirOut)
    [nomDirOut, nomFicSeul] = fileparts(this.nomFic);
end

nomFicNetCDF = fullfile(nomDirOut, [nomFicSeul '-PingCounterAndUnixTime.nc']);
if exist(nomFicNetCDF, 'file')
    delete(nomFicNetCDF)
end

%% Get dimensions

nbSamples = length(TimeDepth);

%% Prepare TimeTx

format longG
TimeUTC = posixtime(TimeDepth);

%% Write Time UTC

nccreate(nomFicNetCDF, 'TimeUTC', ...
        'Dimensions', {'nbSamples', nbSamples}, ...
        'FillValue', NaN);
ncwrite(nomFicNetCDF, 'TimeUTC', TimeUTC);

%% Write PingCounter

nccreate(nomFicNetCDF, 'PingCounter', ...
        'Dimensions', {'nbSamples', nbSamples}, ...
        'FillValue', NaN);
ncwrite(nomFicNetCDF, 'PingCounter', DataDepth.PingCounter(:));
format long
