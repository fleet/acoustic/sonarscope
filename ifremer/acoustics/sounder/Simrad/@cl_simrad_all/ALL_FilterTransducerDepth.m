function ALL_FilterTransducerDepth(this)

FilterTD.Type  = 1;
FilterTD.Ordre = 0;
FilterTD.Wc    = 0;

%% Warning

str1 = sprintf('Assurez vous d''avoir sauvegard� le signal avant d''entreprendre cette op�ration.\n\n"Voir : Survey processings / .all / Utilities / Manage signals\n\nVoulez-vous poursuivre ?');
str2 = sprintf('Be sure you have saved the signal before filtering.\n\n"See : Survey processings / .all / Utilities / Manage signals\n\nDo you want to continue ?');
[rep, flag] = my_questdlg(Lang(str1,str2), 'Init', 2);
if ~flag || (rep == 2)
    return
end

%% Filtrage de TransducerDepth dans les fichiers .all

nbFic = length(this);
str1 = 'Filtrage de "Transducer depth" dans les .all.';
str2 = 'Filter "Transducer depth" in the .all files.';
hw = create_waitbar(Lang(str1,str2), 'N', nbFic);
for k=1:nbFic
    my_waitbar(k, nbFic, hw);
    
    %% Leacture de la donn�e
    
    [flag, DataDepth] = read_depth_bin(this(k), 'Memmapfile', 0);
    if ~flag
        return
    end
    
    %% Affichage de la bathy ombr�e
    
    b = get_Layer(this(k), 'Depth', 'CartoAuto', 1);
    b.ColormapIndex = 20;
    [b, flag] = sunShading(b);
    if flag
        Fig = imagesc(b, 'Rot90', 1);
        placeFigure('Fig', Fig, 'Where', 'Top')
    end
    
    %% Contr�le du signal
    
    [~, Title, Ext] = fileparts(this(k).nomFic);
    TransducerDepth = DataDepth.TransducerDepth;
    [TransducerDepthCleaned, FilterTD] = controleSignalNEW('TransducerDepth', TransducerDepth(:,1), 'Filtre', FilterTD, ...
        'Title', [Title Ext]);
    
    %% Save signal
    
    TransducerDepth(:,1) = TransducerDepthCleaned;
    flag = save_signal(this(k), 'Ssc_Depth', 'TransducerDepth', TransducerDepth);
    if ~flag
        break
    end
    
    my_close(Fig);
end
my_close(hw, 'MsgEnd')
