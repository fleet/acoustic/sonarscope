function ALL_GPSAccuracy(this, varargin)

[varargin, IndNavUnit] = getPropertyValue(varargin, 'IndNavUnit', []); %#ok<ASGLU>

Lat = [];
Lon = [];
nbFic = length(this);
str1 = 'Importation de la navigation dans les .all';
str2 = 'Navigation Import in the .all files';
hw = create_waitbar(Lang(str1,str2), 'N', nbFic);
for k=1:nbFic
    my_waitbar(k, nbFic, hw);
    
    [flag, DataPosition] = read_position_bin(this(k), 'IndNavUnit', IndNavUnit, 'Memmapfile', 1);
    if ~flag
        continue
    end
    
    Lat = [Lat ; DataPosition.Latitude(:)]; %#ok<AGROW>
    Lon = [Lon ; DataPosition.Longitude(:)]; %#ok<AGROW>
    
%     [flag, DataAttitude] = read_attitude_bin(this(k), 'Memmapfile', 1);
%     if ~flag
%         return
%     end
    
end
my_close(hw, 'MsgEnd')

if isempty(Lat)
    return
end

subNaN = isnan(Lat) | isnan(Lon);
Lat(subNaN) = [];
Lon(subNaN) = [];

%% D�finition d'une cartographie assurant un minimum de d�formation : Mercator avec �chelle conserv�e sur la latitude moyenne

a = cl_carto('Ellipsoide.Type', 11, 'Projection.Type', 2, 'Projection.Mercator.long_merid_orig', round(mean(Lon)), 'Projection.Mercator.lat_ech_cons', floor(mean(Lat)));
[x, y] = latlon2xy(a, Lat, Lon);
x = x - median(x);
y = y - median(y);
d = sqrt(x.^2 + y.^2);
d_std = std(d);


%% Calcul de l'histogramme 2D de la r�partition

resol = d_std / 20;
ix = floor(x / resol);
iy = floor(y / resol);
min_ix = min(ix);
min_iy = min(iy);
max_ix = max(ix);
max_iy = max(iy);
nx = max_ix - min_ix + 1;
ny = max_iy - min_iy + 1;
N = zeros(ny, nx, 'uint32');
xN = (1:nx) + min_ix;
yN = (1:ny) + min_iy;
xN = xN * resol;
yN = yN * resol;
ix = ix - ( min_ix - 1);
iy = iy - ( min_iy - 1);
for k=1:length(d)
    N(iy(k),ix(k)) =  N(iy(k),ix(k)) + 1;
end

%% Trac� des points de nav et du cercle englobant � 1 sigma

FigUtils.createSScFigure; plot(x, y, '.b'); grid on; hold on; h = plot_circle(0, 0, d_std, 'r'); hold off; axis equal; 
xlabel('x (m)'); ylabel('y (m)');
title(sprintf('GPS points - Sigma %f m', d_std));
set(h, 'LineWidth', 3);

%% Trac� de la distance en fonction des pings

FigUtils.createSScFigure; PlotUtils.createSScPlot(d, '.b'); grid on; hold on; h = PlotUtils.createSScPlot([1 length(x)], [d_std d_std], 'r'); hold off; axis tight;
xlabel('Samples'); ylabel('Distance / median values (m)');
title(sprintf('GPS dispersion - Sigma %f m', d_std));
set(h, 'LineWidth', 3);

%% Trac� de l'histogramme de r�partition et du cercle englobant � 1 sigma

FigUtils.createSScFigure; imagesc(xN, yN, N, [0 max(N(:))]); colorbar; axis xy; hold on; h = plot_circle(0, 0, d_std, 'r'); hold off; axis equal;
xlabel('x (m)'); ylabel('y (m)');
title(sprintf('Histogramme of GPS dispersion - Sigma %f m', d_std));
set(h, 'LineWidth', 3);

NLog = log10(double(N));
FigUtils.createSScFigure; imagesc(xN, yN, NLog, [0 max(NLog(:))]); colorbar; axis xy; hold on; h = plot_circle(0, 0, d_std, 'r'); hold off; axis equal; 
xlabel('x (m)'); ylabel('y (m)');
title(sprintf('Log1� of histogramme of GPS dispersion - Sigma %f m', d_std));
set(h, 'LineWidth', 3);

