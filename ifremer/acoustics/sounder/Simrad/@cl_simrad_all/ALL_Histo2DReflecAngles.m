function [flag, a] = ALL_Histo2DReflecAngles(this, nomDirXml)

binsIA = -75:75;
binsR  = -70:0.5:20;

%% Calcul des histogrammes individuels

N = length(this);
str1 = 'Calcul des histogrammes 2D sur la réflectivité en fonction ds angles en cours';
str2 = 'Processing 2D Histograms of Reflectivity / RxBeamAngles';
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    
    [A, Carto] = get_Layer(this(k), 'RxBeamAngle', 'CartoAuto', 1);
    R          = get_Layer(this(k), 'ReflectivityFromSnippets', 'Carto', Carto);
    
    h(k) = histo2D(R, A, 'binsI1', binsR, 'binsI2', binsIA); %#ok<AGROW>
    imagesc(h(k), 'Fig', 4758);
    
    [nomDir, nomFic] = fileparts(nomFicAll);
    if isempty(nomDirXml)
        nomDirXml = nomDir;
    end
    nomFicXml = fullfile(nomDirXml, [nomFic '.xml']);
    flag = export_xml(h(k), nomFicXml);
    if ~flag
        continue
    end
end
my_close(hw, 'MsgEnd')

%% Calcul de la somme de tous les histogrammes individuels

XLim = [binsIA(1) binsIA(end)];
YLim = [binsR(1)  binsR(end)];
ImageName = sprintf('Sum_%d_Images',  N);
[flag, a] = MultiImages_Sum(h, XLim, YLim, ImageName);
if flag
    imagesc(a, 'Fig', 4758);
    nomFicXml = fullfile(nomDirXml, [ImageName '.xml']);
    flag = export_xml(a, nomFicXml);
end
