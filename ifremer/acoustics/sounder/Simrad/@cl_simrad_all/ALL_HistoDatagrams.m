function Fig = ALL_HistoDatagrams(this)

useParallel = get_UseParallelTbx;

str1 = 'Calcul de l''histogramme des types de datagramms des .all';
str2 = 'Compute histogram of .all datagrams';
NbFic = length(this);
histoTypeDatagram = zeros(NbFic, 114);
tabNbBytes        = zeros(NbFic, 114);
if useParallel && (NbFic > 1)
    [hw, DQ] = create_parforWaitbar(NbFic, Lang(str1,str2));
    parfor (k=1:NbFic, useParallel)
        a = this(k);
        
        [~, X1, X2, X3] = histo_index_unitaire(a, 0);
        
        Tmp = zeros(1,114);
        Tmp(X1) = X2;
        histoTypeDatagram(k,:) = Tmp;
        
        Tmp = zeros(1,114);
        Tmp(X1) = X3;
        tabNbBytes(k,:)= Tmp;
        send(DQ, k);
    end
    my_close(hw, 'MsgEnd')
else
    hw = create_waitbar(Lang(str1,str2), 'N', NbFic);
    for k=1:NbFic
        my_waitbar(k, NbFic, hw)
        a = this(k);
        
        [~, X1, X2, X3] = histo_index_unitaire(a, 0);
        
        Tmp = zeros(1,114);
        Tmp(X1) = X2;
        histoTypeDatagram(k,:) = Tmp;
        
        Tmp = zeros(1,114);
        Tmp(X1) = X3;
        tabNbBytes(k,:)= Tmp;
    end
    my_close(hw, 'MsgEnd')
end

Code = find(sum(histoTypeDatagram, 1) ~= 0);
sumHistoTypeDatagram = sum(histoTypeDatagram(:,Code), 1);
sumTabNbBytes = sum(tabNbBytes(:,Code), 1);
texteTypeDatagram = codesDatagramsSimrad(Code, sumHistoTypeDatagram);

if isempty(Code)
    Fig = [];
else
    [~, ~, Fig] = plot_histo_index(Code, sumHistoTypeDatagram, texteTypeDatagram, sumTabNbBytes, 'Datagrams');
end
