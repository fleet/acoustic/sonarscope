function ALL_ImmersionImport(this, nomFicImmersion, varargin)

[varargin, IndNavUnit] = getPropertyValue(varargin, 'IndNavUnit',      []); %#ok<ASGLU>

logFileId = getLogFileId;

if ~iscell(nomFicImmersion)
    nomFicImmersion = {nomFicImmersion};
end

%% Traitement des fichiers .all

Carto = [];
N = length(this);
str1 = 'Import de l''immersion dans les .all';
str2 = 'Immersion import into the .all files';
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
    my_waitbar(k, N, hw);
    msg = sprintf('File %d/%d : %s', k, N, this(k).nomFic);
    logFileId.info('ALL_ImmersionImport', msg);
        
    [flag, Carto] = import_external_nav_unitaire(this(k), nomFicImmersion, Carto);
    if ~flag
        msg = sprintf('Failed for file %s', this(k).nomFic);
        logFileId.warn('ALL_ImmersionImport', msg);
    end
end
my_close(hw, 'MsgEnd')


function [success, Carto] = import_external_nav_unitaire(this, nomFicImmersion, Carto)

success = false;
for k=1:length(nomFicImmersion)
    T = readtable(nomFicImmersion{k}, 'Format', '%{dd/MM/yyyy}D %s %f');
    Immersion.Datetime = T.Date + duration(T.Heure);
    Immersion.Datetime.Format = 'yyyy/MM/dd HH:mm:ss.SSS';
    Immersion.Value = T.(3);
    
    [flag, Z] = set_KM_TransducerDepth(this, Immersion.Datetime, Immersion.Value);
    if flag
        % figure; plot(Z.Immersion.Datetime, Z.Data); grid on
        TransducerDepth = Z.Data;
        
        flag = existCacheNetcdf(this.nomFic);
        if flag
            success = save_signalNetcdf(this, 'Ssc_Depth', 'TransducerDepth', TransducerDepth);
        else
            flag = save_signal(this, 'Ssc_Depth', 'TransducerDepth', TransducerDepth);
            if flag
                success = true;
            else
                str1 = sprintf('L''import de la navigation pour le fichier "%s" n''a pas pu se faire.', this.nomFic);
                str2 = sprintf('The import of navigation for "%s" failed.', this.nomFic);
                my_warndlg(Lang(str1,str2), 0);
                return
            end
        end
    end
end
