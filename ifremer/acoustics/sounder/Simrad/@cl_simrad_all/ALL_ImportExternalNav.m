function ALL_ImportExternalNav(this, nomFicNav, varargin)

[varargin, IndNavUnit]          = getPropertyValue(varargin, 'IndNavUnit',      []);
[varargin, flagImportImmersion] = getPropertyValue(varargin, 'ImportImmersion', 1); %#ok<ASGLU>

if ~iscell(nomFicNav)
    nomFicNav = {nomFicNav};
end

%% Plot de la navigation des .all

Fig1 = ALL_PlotNav(this, 'Color', 'k');

title('.all files navigation')

%% Plot de la navigation externe

% Fig2 = [];
Fig2 = Fig1;
N = length(nomFicNav);
str1 = 'Plot navigation files';
str2 = 'Plot navigation files';
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
	my_waitbar(k, N, hw)
    [flag, Latitude, Longitude, tNav, ~, ~, Immersion] = lecFicNav(nomFicNav{k});
    if ~flag
        return
    end
    LatNewNav{k}    = Latitude; %#ok<AGROW>
    LonNewNav{k}    = Longitude; %#ok<AGROW>
    TimeNewNav{k}   = tNav; %#ok<AGROW>
    ImmersionNav{k} = Immersion; %#ok<AGROW>
	[Fig2, h2] = plot_navigation(nomFicNav{k}, Fig2, LonNewNav{k}, LatNewNav{k}, TimeNewNav{k}.timeMat, [], ...
        'Color', 'r'); %#ok<ASGLU>
end
my_close(hw, 'MsgEnd')
title('External navigations')

%% Question 

pause(3) % TODO : moyen d�tourn� pour laisser le temps � l'op�rateur d'agrandir la fen�tre
str1 = 'Faites vous confiance � la navigation externe (couleur rouge) ? Voulez-vous vraiment l''importer dans les .all (dans les r�pertoires de cache de SSc devrais-je dire) ?';
str2 = 'Do you trust the external navigation (red) ? Do you really want to import it into the .all (I should say into the SSc cache directories) ?';
[rep, flag] = my_questdlg(Lang(str1,str2));
if ~flag || (rep == 2)
    return
end

%% Traitement des fichiers .all

Carto = [];
nomFicBug = string.empty();
validation = 0;
N = length(this);
str1 = 'Import de la navigation dans les .all';
str2 = 'Navigation import into the .all files';
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
    my_waitbar(k, N, hw);
    [flag, Carto, validation] = ALL_ImportExternalNav_unitaire(this(k), nomFicNav, LonNewNav, LatNewNav, TimeNewNav, ...
        ImmersionNav, flagImportImmersion, Carto, validation, IndNavUnit);
    if ~flag
        nomFicBug(end+1) = this(k).nomFic; %#ok<AGROW>
    end
end
my_close(hw, 'MsgEnd')

if ~isempty(nomFicBug)
    msg = sprintf('The files here under failed when I/O was done :\n');
    for k=1:length(nomFicBug)
        msg = sprintf('%s    %s\n', msg, nomFicBug(k));
    end
    my_warndlg(msg, 1);
end


function [flag, Carto, validation] = ALL_ImportExternalNav_unitaire(this, nomFicNav, LonNewNav, LatNewNav, TimeNewNav, ...
    ImmersionNav, flagImportImmersion, Carto, validation, IndNavUnit)

nomFicSimrad = this.nomFic;

%% Lecture de l'image

[flag, DataPosition] = read_position_bin(this, 'IndNavUnit', IndNavUnit);
if ~flag
    return
end

if isempty(ImmersionNav{1})
    if ~flag
        return
    end
else
    [flag, DataDepth]    = read_depth_bin(this, 'Memmapfile', 1);
    if ~flag
        return
    end
    [flag, DataAttitude] = read_attitude_bin(this, 'Memmapfile', 0);
end

TPosition = DataPosition.Datetime;

flagDonneesModifiees = 0;
LatitudeImage  = DataPosition.Latitude;  % NaN(DataPosition.Dimensions.NbSamples, 1);
LongitudeImage = DataPosition.Longitude; % NaN(DataPosition.Dimensions.NbSamples, 1);
for k=1:length(nomFicNav)
    tNav = TimeNewNav{k}.timeMat;
    DatetimeNav = datetime(tNav, 'ConvertFrom', 'datenum');
    
    LongitudeNew = my_interp1_longitude(DatetimeNav, LonNewNav{k}, TPosition);
    LatitudeNew  = my_interp1(          DatetimeNav, LatNewNav{k}, TPosition);
    if ~isempty(ImmersionNav{k})
        TDepth = DataDepth.Datetime;
%         TAttitude = DataAttitude.Datetime;
        ImmersionImage = my_interp1(DatetimeNav, ImmersionNav{k}, TDepth);        
%         HeaveImage     = my_interp1(TAttitude, DataAttitude.Heave, TDepth);        
    end
    
    subNonNaN = find(~isnan(LongitudeNew(:,1)));
    if ~isempty(subNonNaN)
        if validation ~= 2
            [flag, validation] = checkNewImportNew(nomFicNav{k}, LatitudeNew(subNonNaN,1), LongitudeNew(subNonNaN,1), ...
                LatitudeImage(subNonNaN,1), LongitudeImage(subNonNaN,1), nomFicSimrad);
            if ~flag
                return
            end
        end
        if (validation == 1) || (validation == 2)
            LatitudeImage(subNonNaN,1)  = LatitudeNew(subNonNaN,1);
            LongitudeImage(subNonNaN,1) = LongitudeNew(subNonNaN,1);
            if isempty(ImmersionNav{k})
                ImmersionImage = [];
            end
            flagDonneesModifiees = 1;
        end
    end
end

if flagDonneesModifiees
    flag = save_signal(this, 'Ssc_Position', 'Latitude', LatitudeImage);
    if ~flag
        str1 = sprintf('L''import de la navigation pour le fichier "%s" n''a pas pu se faire.', nomFicSimrad);
        str2 = sprintf('The import of navigation for "%s" failed.', nomFicSimrad);
        my_warndlg(Lang(str1,str2), 0);
        return
    end
    
    flag = save_signal(this, 'Ssc_Position', 'Longitude', LongitudeImage);
    if ~flag
        str1 = sprintf('L''import de la navigation pour le fichier "%s" n''a pas pu se faire.', nomFicSimrad);
        str2 = sprintf('The import of navigation for "%s" failed.', nomFicSimrad);
        my_warndlg(Lang(str1,str2), 0);
        return
    end
    
    % TransducerDepth
    % {
    % Comment� pour mission SUBSAINTES et d�comment� pour AUV
    if flagImportImmersion && ~isempty(ImmersionImage) && ~all(isnan(ImmersionImage))
        [flag, Z] = set_KM_TransducerDepth(this, TDepth, ImmersionImage, 'DataDepth', DataDepth, 'DataAttitude', DataAttitude);
        if flag
            % figure; plot(Z.Datetime, Z.Data); grid on
            TransducerDepth = Z.Data;
            flag = save_signal(this, 'Ssc_Depth', 'TransducerDepth', TransducerDepth);
            if ~flag
                str1 = sprintf('L''import de la navigation pour le fichier "%s" n''a pas pu se faire.', nomFicSimrad);
                str2 = sprintf('The import of navigation for "%s" failed.', nomFicSimrad);
                my_warndlg(Lang(str1,str2), 0);
                return
            end
        end
    end
    % }
end


function [flag, validation] = checkNewImportNew(nomFicNvi, LatitudeNew, LongitudeNew, LatitudeImage, LongitudeImage, nomFicSimrad)

[~, nomFicSimrad, ExtSimrad] = fileparts(nomFicSimrad);
[~, nomFicNvi, ExtNvi] = fileparts(nomFicNvi);
Title = sprintf('%s\n%s', [nomFicSimrad ExtSimrad], [nomFicNvi ExtNvi]);

Fig = FigUtils.createSScFigure;
PlotUtils.createSScPlot(LongitudeImage, LatitudeImage, 'b+'); grid on; hold on;
PlotUtils.createSScPlot(LongitudeNew,   LatitudeNew,   'rx');
title(Title, 'Interpreter', 'none'); xlabel('Lon (deg)'); ylabel('Lat (deg)'); axisGeo;
legend({'Nav .all'; 'External nav'})

str1 = sprintf('Acceptez-vous cette nouvelle navigation pour le fichier "%s" ?', nomFicNvi);
str2 = sprintf('Do you accept this new navigation "%s" ?', nomFicNvi);
% [rep, flag] = my_questdlg(Lang(str1,str2), 'WaitingTime', 10);
str{1} = Lang('Oui','Yes');
str{2} = Lang('Oui pour celle-ci et pour toutes les suivantes','Yes for this one and for all the next ones');
str{3} = Lang('Non','No');

[validation, flag] = my_listdlg(Lang(str1,str2), str, 'InitialValue', 1, 'SelectionMode', 'Single');
if ~flag
    return
end
my_close(Fig);
