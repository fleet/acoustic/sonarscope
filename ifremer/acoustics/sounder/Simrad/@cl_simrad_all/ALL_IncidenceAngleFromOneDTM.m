function flag = ALL_IncidenceAngleFromOneDTM(this, DTM, varargin) 

[varargin, TypeAlgoNormales] = getPropertyValue(varargin, 'TypeAlgoNormales', 1);
% [varargin, versionAlgo]      = getPropertyValue(varargin, 'versionAlgo',      2); %#ok<ASGLU>

[varargin, versionAlgo]      = getPropertyValue(varargin, 'versionAlgo',      1); %#ok<ASGLU> % Modif JMA le 04/10/2022 suite � discussion avec Ridha

useParallel = get_UseParallelTbx;
logFileId   = getLogFileId;

%% Test si l'image visualis�e est bien un MNT de r�f�rence

DataType = cl_image.indDataType('Bathymetry');
flag = testSignature(DTM, 'DataType', DataType, 'GeometryType', cl_image.indGeometryType('LatLong'));
if ~flag
    logFileId.warn('ALL_IncidenceAngleFromOneDTM', 'Exit because the signature of the current image is not "Bathymetry_LatLong"');
    return
end

%% Calcul de la pente Nord-Sud et Est-West

[SlopeN_LatLong, flag] = slopeAzimuth(DTM, 'Azimuth', 180); %, 'subx', subxZ, 'suby', subyZ); % Modif JMA le 25/08/2016
if ~flag
    logFileId.warn('ALL_IncidenceAngleFromOneDTM', 'Exit because slopeAzimuth 180 could not be processed');
    return
end

[SlopeW_LatLong, flag] = slopeAzimuth(DTM, 'Azimuth', 270); %, 'subx', subxZ, 'suby', subyZ);
if ~flag
    logFileId.warn('ALL_IncidenceAngleFromOneDTM', 'Exit because slopeAzimuth 270 could not be processed');
    return
end

%% Pour �viter disfonctionnement de cl_memmapfile/delete si //TBx

DTM = Virtualmemory2Ram(DTM);

%% Traitement des fichiers .all

N = length(this);
flag = false(N,1);
flagWaitbar = (N == 1);
Titre = DTM.Name;
str1 = ['Calcul des angles d''incidence de ' Titre];
str2 = ['Computing Incidence Angles of ' Titre];
if useParallel && (N > 1)
    logFileIdHere = logFileId;
    [hw, DQ] = create_parforWaitbar(N, Lang(str1,str2));
    parfor k=1:N
        try
            flag(k) = ALL_IncidenceAngleFromOneDTM_unitaire(this(k), DTM, k, N, SlopeN_LatLong, SlopeW_LatLong, ...
                flagWaitbar, versionAlgo, TypeAlgoNormales, ...
                'isUsingParfor', true, 'logFileId', logFileIdHere);
            send(DQ, k);
        catch ME
            ME.getReport
        end
    end
    my_close(hw, 'MsgEnd', 'TimeDelay', 60)
else
    % profile on
    hw = create_waitbar(Lang(str1,str2), 'N', N);
    for k=1:N
        my_waitbar(k, N, hw)
        flag(k) = ALL_IncidenceAngleFromOneDTM_unitaire(this(k), DTM, k, N, SlopeN_LatLong, SlopeW_LatLong, ...
            flagWaitbar, versionAlgo, TypeAlgoNormales);
    end
    my_close(hw, 'MsgEnd', 'TimeDelay', 60)
    % profile report
end


function flag = ALL_IncidenceAngleFromOneDTM_unitaire(this, DTM, iFic, N, SlopeN_LatLong, SlopeW_LatLong, ...
    flagWaitbar, versionAlgo, TypeAlgoNormales, varargin)

global isUsingParfor %#ok<GVMIS>
global logFileId %#ok<GVMIS>

[varargin, logFileId]     = getPropertyValue(varargin, 'logFileId',     logFileId);
[varargin, isUsingParfor] = getPropertyValue(varargin, 'isUsingParfor', isUsingParfor); %#ok<ASGLU

flag = 0;

msg = sprintf('%d / %d : %s', iFic, N, this.nomFic);
logFileId.info('ALL_IncidenceAngleFromOneDTM', msg);

[nomDir, nomFicSeul] = fileparts(this.nomFic); %#ok<ASGLU>

Version = version_datagram(this);
switch Version
    case 'V1'
        ListeLayers = [2 3 4 5 15 20 21 24 25 26 36 53]; % TODO avril 2021 : Ajouter AlongBeamPoitingAngle dans import V1
        
    case 'V2'
        ListeLayers = Simrad_getDefaultLayersV2;
        
        ListeLayers.SelectionLayers{1,1} = [0 0 1 1 0 0 0 0 0 0 0]; % [0 0 1 1 0 0 0 0 0 0 1]; % Sans Reflec
        ListeLayers.SelectionLayers{1,2} = [1 1 0 0 0 0 0 0 0 0 0];
        ListeLayers.SelectionLayers{2,1} = [1 1 1];
        ListeLayers.SelectionLayers{2,2} = [1 0 0 0 0 0 0 0 0 0 1 1 0 0 1 0 1 0 0 1];
        ListeLayers.SelectionLayers{3,1} = 0;
        ListeLayers.SelectionLayers{3,2} = [0 0 0 0];
end

switch Version
    case 'V1'
        [Layers_PingBeam, Carto, suby] = import_PingBeam_All_V1(this, 'CartoAuto', 1, 'ListeLayers', ListeLayers); %#ok<ASGLU>
        if isempty(Layers_PingBeam)
            logFileId.warn('ALL_IncidenceAngleFromOneDTM_unitaire', ['Exit because no layer could be read from "' this.nomFic '"']);
            return
        end
        nbRows = Layers_PingBeam(1).nbRows;
    case 'V2'
        [Layers_PingBeam, Carto, suby] = import_PingBeam_All_V2(this, 'CartoAuto', 1, 'ListeLayers', ListeLayers); %#ok<ASGLU>
        if isempty(Layers_PingBeam)
            logFileId.warn('ALL_IncidenceAngleFromOneDTM_unitaire', ['Exit because no layer could be read from "' this.nomFic '"']);
            return
        end
        nbRows = Layers_PingBeam(1).nbRows;
end
if nbRows <= 1
    logFileId.warn('ALL_IncidenceAngleFromOneDTM_unitaire', ['Exit because nbRows<= 1 for "' this.nomFic '" layers']);
    return
end
%     nbRows = Layers_PingBeam(1).nbRows;
for iLayer=1:length(Layers_PingBeam)
    DataType(iLayer) = Layers_PingBeam(iLayer).DataType; %#ok<AGROW>
end

indLayerBathyPingBeam      = find(DataType == cl_image.indDataType('Bathymetry'));
indLayerAcrossDistPingBeam = find(DataType == cl_image.indDataType('AcrossDist'));
indLayerAlongDistPingBeam  = find(DataType == cl_image.indDataType('AlongDist'));
indTxBeamIndex             = find(DataType == cl_image.indDataType('TxBeamIndex'));
indAlongBeamPoitingAngle   = find(DataType == cl_image.indDataType('SonarAlongBeamAngle'));

Bathymetry_PingBeam = Layers_PingBeam(indLayerBathyPingBeam);
[~, indLayerRange] = findAnyLayerRange(Layers_PingBeam, indLayerBathyPingBeam);
if isempty(indLayerRange)
    logFileId.warn('ALL_IncidenceAngleFromOneDTM_unitaire', ['Exit because no range layer was found for "' this.nomFic '"']);
    return
end
indImageAngle = (DataType == cl_image.indDataType('RxAngleEarth'));
indImageBeamPointingAngle = (DataType == cl_image.indDataType('BeamPointingAngle'));

%% Calcul des coordonn�es g�ograpiques

[flag, LatLong_PingBeam] = sonarCalculCoordGeo(Layers_PingBeam, indLayerBathyPingBeam, 'Mute', 1);
if ~flag
    logFileId.warn('ALL_IncidenceAngleFromOneDTM_unitaire', ['Exit because function "sonarCalculCoordGeo" failed for "' this.nomFic '"']);
    return
end

%% Projection inverse de SlopeAcross_LatLong en SlopeAcross_PingBeam

[flag, SlopeN_PingBeam] = sonar_mosaiqueInv(Layers_PingBeam(indLayerBathyPingBeam), [], LatLong_PingBeam(1), LatLong_PingBeam(2), ...
    SlopeN_LatLong, [], [], 'flagWaitbar', flagWaitbar);
if ~flag
    logFileId.warn('ALL_IncidenceAngleFromOneDTM_unitaire', ['Exit because function "sonar_mosaiqueInv" failed for "' this.nomFic '"']);
    return
end
SlopeN_PingBeam.ColormapIndex = 3;
SlopeN_PingBeam = update_Name(SlopeN_PingBeam, 'Name', nomFicSeul);

%% Projection inverse de SlopeAlong_LatLong en SlopeAlong_PingBeam

[flag, SlopeW_PingBeam] = sonar_mosaiqueInv(Layers_PingBeam(indLayerBathyPingBeam), [], LatLong_PingBeam(1), LatLong_PingBeam(2), ...
    SlopeW_LatLong, [], [], 'flagWaitbar', flagWaitbar);
if ~flag
    logFileId.warn('ALL_IncidenceAngleFromOneDTM_unitaire', ['Exit because function "sonar_mosaiqueInv" failed for "' this.nomFic '"']);
    return
end
SlopeW_PingBeam.ColormapIndex = 3;
SlopeW_PingBeam = update_Name(SlopeW_PingBeam, 'Name', nomFicSeul);

%% Coordonn�es dans la projection

statsLat = LatLong_PingBeam(1).StatValues;
statsLon = LatLong_PingBeam(2).StatValues;
lat_ech_cons    = floor(statsLat.Moyenne);
long_merid_orig = floor(statsLon.Moyenne);
Carto = cl_carto('Ellipsoide.Type', 11, 'Projection.Type', 2, ...
    'Projection.Mercator.lat_ech_cons',    lat_ech_cons, ...
    'Projection.Mercator.long_merid_orig', long_merid_orig);
LatLong_PingBeam(1) = set_Carto(LatLong_PingBeam(1), Carto);
LatLong_PingBeam(2) = set_Carto(LatLong_PingBeam(2), Carto);
[flag, XY_PingBeam] = sonarCalculCoordXY(LatLong_PingBeam, 1);
if ~flag
    logFileId.warn('ALL_IncidenceAngleFromOneDTM_unitaire', ['Exit because function "sonarCalculCoordXY" failed for "' this.nomFic '"']);
    return
end

%% Calcul des acrossDist et AlonDist dans le r�f�rentiel antenne

[acrossDistRefTxAntenna, alongDistRefTxAntenna] = getDistancesRefTxAntennas(Layers_PingBeam(indLayerAcrossDistPingBeam), Layers_PingBeam(indLayerAlongDistPingBeam));

%% Snell-Descartes

[flag, AnglesRxEarth, IBA] = SnellDescartes(Layers_PingBeam(indImageAngle), Layers_PingBeam(indLayerBathyPingBeam));
if ~flag
    logFileId.warn('ALL_IncidenceAngleFromOneDTM_unitaire', ['Exit because function "SnellDescartes" failed for "' this.nomFic '"']);
    return
end

%% Calcul du layer d'angle d'incidence et des pentes

if isempty(indAlongBeamPoitingAngle)
    % Calcul de l'azimuth du faisceau par rapport au r�f�rentiel navire
    [acrossDistRefTxAntenna, alongDistRefTxAntenna] = getDistancesRefTxAntennas(Layers_PingBeam(indLayerAcrossDistPingBeam), Layers_PingBeam(indLayerAlongDistPingBeam));
    azimuth = atan2d(alongDistRefTxAntenna, acrossDistRefTxAntenna);
    
    % Conserver cette fonction quelques temps car la fonction sonar_angle_incidenceHROV peut r�server des surprises
    [flag, a] = sonar_angle_incidence(AnglesRxEarth, azimuth, SlopeN_PingBeam, SlopeW_PingBeam, ...
        Layers_PingBeam(indLayerRange), Layers_PingBeam(indTxBeamIndex), 'flagWaitbar', flagWaitbar);
else
    % Angle d�pointage longitudinal pris en compte le 07/04/2021
    switch versionAlgo
        case 1 % Algo JMA sur fond plat
            [flag, a] = sonar_angle_incidenceHROV(AnglesRxEarth, Layers_PingBeam(indAlongBeamPoitingAngle), SlopeN_PingBeam, SlopeW_PingBeam, ...
                Layers_PingBeam(indLayerRange), Layers_PingBeam(indTxBeamIndex), acrossDistRefTxAntenna, alongDistRefTxAntenna, ...
                Layers_PingBeam(indLayerBathyPingBeam), 'flagWaitbar', flagWaitbar);
            
        case 2 % Algo IMEN sur MNT
%             [flag, a] = sonar_angle_incidenceHROV_IMEN(Layers_PingBeam(indImageAngle), Layers_PingBeam(indAlongBeamPoitingAngle), ...
%                 -SlopeW_PingBeam, -SlopeN_PingBeam, ...
%                 Layers_PingBeam(indLayerRange), Layers_PingBeam(indTxBeamIndex), IBA, 'flagWaitbar', flagWaitbar);
            [flag, a] = sonar_angle_incidenceHROV_IMEN_Final(Layers_PingBeam(indImageBeamPointingAngle), Layers_PingBeam(indAlongBeamPoitingAngle), ...
                -SlopeW_PingBeam, -SlopeN_PingBeam, Bathymetry_PingBeam, XY_PingBeam(1), XY_PingBeam(2), ...
                Layers_PingBeam(indLayerRange), Layers_PingBeam(indTxBeamIndex), IBA, ...
                'TypeAlgoNormales', TypeAlgoNormales, 'flagWaitbar', flagWaitbar);
        case 3 % Algo IMEN sur Tree
            %         XY_PingBeam(1) = filterRectangle(XY_PingBeam(1), 'window', [9 9]);
            %         XY_PingBeam(2) = filterRectangle(XY_PingBeam(2), 'window', [9 9]);
            
            [flag, a] = sonar_angle_incidenceHROV_IMEN_Tree(Layers_PingBeam(indImageBeamPointingAngle), Layers_PingBeam(indAlongBeamPoitingAngle), ...
                -SlopeW_PingBeam, -SlopeN_PingBeam, Bathymetry_PingBeam, XY_PingBeam(1), XY_PingBeam(2), ...
                Layers_PingBeam(indLayerRange), Layers_PingBeam(indTxBeamIndex), IBA, ...
                'TypeAlgoNormales', TypeAlgoNormales, 'flagWaitbar', flagWaitbar);
    end
end
if ~flag
    logFileId.warn('ALL_IncidenceAngleFromOneDTM_unitaire', ['Exit because function "sonar_angle_incidence" failed for "' this.nomFic '"']);
    return
end

IncidenceAngle_PingBeam = a(1);
SlopeAcross_PingBeam    = a(2);
SlopeAlong_PingBeam     = a(3);
InsonifiedArea_PingBeam = a(4);
WCSignalWidth_PingBeam  = a(5);

SlopeAcross_PingBeam    = update_Name(SlopeAcross_PingBeam,    'Name', nomFicSeul);
SlopeAlong_PingBeam     = update_Name(SlopeAlong_PingBeam,     'Name', nomFicSeul);
IncidenceAngle_PingBeam = update_Name(IncidenceAngle_PingBeam, 'Name', nomFicSeul);
InsonifiedArea_PingBeam = update_Name(InsonifiedArea_PingBeam, 'Name', nomFicSeul);
WCSignalWidth_PingBeam  = update_Name(WCSignalWidth_PingBeam,  'Name', nomFicSeul);

%% Projection inverse de Bathymetry_LatLong en PingBeam

[flag, Bathymetry_PingBeam] = sonar_mosaiqueInv(Layers_PingBeam(indLayerBathyPingBeam), [], LatLong_PingBeam(1), LatLong_PingBeam(2), ...
    DTM, [], [], 'flagWaitbar', flagWaitbar);
if ~flag
    logFileId.warn('ALL_IncidenceAngleFromOneDTM_unitaire', ['Exit because function "sonar_mosaiqueInv" failed for "' this.nomFic '"']);
    return
end
Bathymetry_PingBeam.ColormapIndex = 3;
Bathymetry_PingBeam = update_Name(Bathymetry_PingBeam, 'Name', nomFicSeul);

%% Lecture du fichier XML d�crivant la donn�e

[flag, XML] = SSc_ReadDatagrams(this.nomFic, 'Ssc_Depth', 'XMLOnly', 1);
if ~flag
    logFileId.warn('ALL_IncidenceAngleFromOneDTM_unitaire', ['Exit because function "SSc_ReadDatagrams" failed for "' this.nomFic '"']);
    return
end

%% Save IncidenceAngle_PingBeam in SonarScope cache directory

X = get_Image(IncidenceAngle_PingBeam);
X = X(:,:);
if (size(X,1) ~= XML.Dimensions.nbPings) || (size(X,2) ~= XML.Dimensions.nbBeams)
    Y = NaN(XML.Dimensions.nbPings, XML.Dimensions.nbBeams, class(X));
    try
        Y(suby,:) = X;
    catch % Bidouille infame pour �viter bug fichier SPFE 0012_20100225_031309_Belgica.all
        suby(suby > size(X,1)) = [];
        Y(suby,:) = X(1:length(suby),:);
    end
    X = Y;
end
flag = save_image(this, 'Ssc_Depth', 'IncidenceAngle', X);
if ~flag
    logFileId.warn('ALL_IncidenceAngleFromOneDTM_unitaire', ['Exit because function "save_image" failed when exporting "IncidenceAngle" into "' this.nomFic '"']);
    return
end

%% Save SlopeAcross_PingBeam in SonarScope cache directory

X = get_Image(SlopeAcross_PingBeam);
X = X(:,:);
if (size(X,1) ~= XML.Dimensions.nbPings) || (size(X,2) ~= XML.Dimensions.nbBeams)
    Y = NaN(XML.Dimensions.nbPings, XML.Dimensions.nbBeams, class(X));
    Y(suby,:) = X(1:length(suby),:);
    X = Y;
end
flag = save_image(this, 'Ssc_Depth', 'SlopeAcross', X);
if ~flag
    logFileId.warn('ALL_IncidenceAngleFromOneDTM_unitaire', ['Exit because function "save_image" failed when exporting "SlopeAcross" into "' this.nomFic '"']);
    return
end

%% Save SlopeAlong_PingBeam in SonarScope cache directory

X = get_Image(SlopeAlong_PingBeam);
X = X(:,:);
if (size(X,1) ~= XML.Dimensions.nbPings) || (size(X,2) ~= XML.Dimensions.nbBeams)
    Y = NaN(XML.Dimensions.nbPings, XML.Dimensions.nbBeams, class(X));
    Y(suby,:) = X(1:length(suby),:);
    X = Y;
end
flag = save_image(this, 'Ssc_Depth', 'SlopeAlong', X);
if ~flag
    logFileId.warn('ALL_IncidenceAngleFromOneDTM_unitaire', ['Exit because function "save_image" failed when exporting "SlopeAlong" into "' this.nomFic '"']);
    return
end

%% Save Bathymetry_PingBeam in SonarScope cache directory

X = get_Image(Bathymetry_PingBeam);
X = X(:,:);
if (size(X,1) ~= XML.Dimensions.nbPings) || (size(X,2) ~= XML.Dimensions.nbBeams)
    Y = NaN(XML.Dimensions.nbPings, XML.Dimensions.nbBeams, class(X));
    Y(suby,:) = X(1:length(suby),:);
    X = Y;
end
flag = save_image(this, 'Ssc_Depth', 'BathymetryFromDTM', X);
if ~flag
    logFileId.warn('ALL_IncidenceAngleFromOneDTM_unitaire', ['Exit because function "save_image" failed when exporting "BathymetryFromDTM" into "' this.nomFic '"']);
    return
end

%% Save InsonifiedArea_PingBeam in SonarScope cache directory

X = get_Image(InsonifiedArea_PingBeam);
X = X(:,:);
if (size(X,1) ~= XML.Dimensions.nbPings) || (size(X,2) ~= XML.Dimensions.nbBeams)
    Y = NaN(XML.Dimensions.nbPings, XML.Dimensions.nbBeams, class(X));
    Y(suby,:) = X(1:length(suby),:);
    X = Y;
end
flag = save_image(this, 'Ssc_Depth', 'InsonifiedAreaSSc', X);
if ~flag
    logFileId.warn('ALL_IncidenceAngleFromOneDTM_unitaire', ['Exit because function "save_image" failed when exporting "InsonifiedAreaSSc" into "' this.nomFic '"']);
    return
end

%% Save WCSignalWidth_PingBeam in SonarScope cache directory

X = get_Image(WCSignalWidth_PingBeam);
X = X(:,:);
if (size(X,1) ~= XML.Dimensions.nbPings) || (size(X,2) ~= XML.Dimensions.nbBeams)
    Y = NaN(XML.Dimensions.nbPings, XML.Dimensions.nbBeams, class(X));
    Y(suby,:) = X(1:length(suby),:);
    X = Y;
end
flag = save_image(this, 'Ssc_Depth', 'WCSignalWidth', X);
if ~flag
    logFileId.warn('ALL_IncidenceAngleFromOneDTM_unitaire', ['Exit because function "save_image" failed when exporting " WCSignalWidth" into "' this.nomFic '"']);
    return
end
