function flagFile = ALL_IsLayerPopulated(aKM, layerName)

n = length(aKM);
flagFile = false(1,n);
filenameALL = get_nomFic(aKM);
for k=1:n
    [flag, DataDepth] = read_depth_bin(aKM(k), [], 'Memmapfile', -1);
    if ~flag
        return
    end
    flagFile(k) = ALL_CacheIsLayerPopulated(filenameALL{k}, DataDepth.Images, layerName);
end
