function ALL_PatchTestRoll(this, resol)

if length(this) ~= 2
    str1 = 'Il faut 2 fichiers acquis sur la ligne mais en sens oppos�s pour faire cette calibration en roulis.';
    str2 = 'You need 2 lines in opposite directions to perform this roll biais identification.';
    my_warndlg(Lang(str1,str2), 1);
    return
end

%% Cr�ation des mosa�ques

for k=1:2
    if isempty(resol) || isnan(resol)
        Invite = 'Inter Pings Distance';
        nbColors = 64;
        [flag, Signal] = read_Invite(this(k), Invite, nbColors, 0, -1);
        if ~flag || isempty(Signal.Data)
            return
        end
        resol = 2 * mean(Signal.Data, 'omitnan');
        resol = nearestExpectedGridSize(resol);
        str1 = sprintf('La taille de la grille est %s m', num2str(resol));
        str2 = sprintf('The grid size that is used is %s m', num2str(resol));
        my_warndlg(Lang(str1,str2), 1);
    end
    
    [Z(k), Carto]     = get_Layer(this(k), 'Depth+Draught-Heave', 'CartoAuto', 1); %#ok<AGROW>
    AcrossDistance(k) = get_Layer(this(k), 'AcrossDist',  'Carto', Carto); %#ok<AGROW>
    AlongDistance(k)  = get_Layer(this(k), 'AlongDist',   'Carto', Carto); %#ok<AGROW>
    A(k)              = get_Layer(this(k), 'RxBeamAngle', 'Carto', Carto); %#ok<AGROW>
end

%% D�termination du biais en roulis

[flag, RollBias] = sonar_PatchtestRoll(Z, AcrossDistance, AlongDistance, A, resol); %#ok<ASGLU>
if ~flag
    return
end
