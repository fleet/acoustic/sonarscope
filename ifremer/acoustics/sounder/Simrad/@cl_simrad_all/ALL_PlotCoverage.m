% Trac� de la navigation et de la couverture d'un fichier .all
%
% Syntax
%   ALL_PlotCoverage(nomFic)
%
% Input Arguments
%   nomFic : Liste de fichiers .all
%
% Examples
%   nomDir = 'D:\NIWA_DATA\TAN0105'
%   listFileNames = listeFicOnDir2(nomDir, '.all')
%   [flag, All] = ALL.checkFiles(listFileNames);
%   ALL_PlotCoverage(All)
%
% See also cl_simrad_all plot_index Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function ALL_PlotCoverage(this, varargin)

[varargin, IndNavUnit] = getPropertyValue(varargin, 'IndNavUnit', []);
[varargin, Fig]        = getPropertyValue(varargin, 'Fig',        []); %#ok<ASGLU>

Ident = rand(1);

if isempty(Fig)
    Fig = figure('name', 'Position datagrams');
else
    figure(Fig);
end
set(Fig, 'UserData', Ident)
hold on;

ColorOrder = get(gca, 'ColorOrder');
nbCoul = size(ColorOrder,1);

nbFiles = length(this);
hw = create_waitbar('Plot .all coverage', 'N', nbFiles);
for k=1:nbFiles
    my_waitbar(k, nbFiles, hw);
    iCoul = mod(k-1, nbCoul) + 1;
    [EmModel, SystemSerialNumber] = ALL_PlotCoverage_unitaire(this(k), Fig, ColorOrder(iCoul,:), this(k).nomFic, IndNavUnit);
end
my_close(hw, 'MsgEnd');

FigName = sprintf('Sondeur %d Serial Number  %d - Coverage', EmModel, SystemSerialNumber(1));
figure(Fig);
set(Fig, 'name', FigName)
drawnow


function [EmModel, SystemSerialNumber] = ALL_PlotCoverage_unitaire(this, Fig, Color, nomFic, IndNavUnit)

EmModel            = get(this, 'EmModel');
SystemSerialNumber = get(this, 'SystemSerialNumber');

Memmapfile = -1;

%% Lecture des datagrams de navigation

[flag, DataPosition] = read_position_bin(this, 'IndNavUnit', IndNavUnit, 'Memmapfile', 0);
if ~flag || isempty(DataPosition) || isempty(DataPosition.Latitude)
    return
end
tNav = DataPosition.Datetime;

[flag, Data_Depth] = read_depth_bin(this, 'Memmapfile', Memmapfile);
if ~flag || isempty(Data_Depth)
    return
end

NbPings = size(Data_Depth.AcrossDist, 1);
% step = min(max(1, floor(NbPings/100)), 100);

T = Data_Depth.Datetime;

% NbPingsDessines = ceil(NbPings/step);
NbPingsDessines = NbPings;
TDepth = datetime(zeros(NbPingsDessines, 1), 'ConvertFrom', 'datenum');
A_Bab  = zeros(NbPingsDessines, 1);
A_Tri  = zeros(NbPingsDessines, 1);
D_Bab  = zeros(NbPingsDessines, 1);
D_Tri  = zeros(NbPingsDessines, 1);

Data_DepthAcrossDist = Data_Depth.AcrossDist(:,:);
Data_DepthAlongDist  = Data_Depth.AlongDist(:,:);

k = 1;
kPing = 1;
D = Data_DepthAcrossDist(kPing,:);
A = Data_DepthAlongDist(kPing,:);
iDeb = find(~isnan(D), 1, 'first');
iFin = find(~isnan(D), 1, 'last');
if ~isempty(iDeb) && ~isempty(iFin)
    D_Bab(k)  = D(iDeb);
    D_Tri(k)  = D(iFin);
    A_Bab(k)  = A(iDeb);
    A_Tri(k)  = A(iFin);
    TDepth(k) = T(kPing);
    k = k+1;
end

for kPing=1:NbPings
    D = Data_DepthAcrossDist(kPing,:);
    A = Data_DepthAlongDist(kPing,:);
    iDeb = find(~isnan(D), 1, 'first');
    iFin = find(~isnan(D), 1, 'last');
    if isempty(iDeb) || isempty(iFin) || isnat(T(kPing))
        continue
    end
    D_Bab(k) = D(iDeb);
    D_Tri(k) = D(iFin);
    A_Bab(k) = A(iDeb);
    A_Tri(k) = A(iFin);
    TDepth(k) = T(kPing);
    k = k+1;
end

kPing = NbPings;
D = Data_DepthAcrossDist(kPing,:);
A = Data_DepthAlongDist(kPing,:);
iDeb = find(~isnan(D), 1, 'first');
iFin = find(~isnan(D), 1, 'last');
if ~isempty(iDeb) && ~isempty(iFin)
    D_Bab(k) = D(iDeb);
    D_Tri(k) = D(iFin);
    A_Bab(k) = A(iDeb);
    A_Tri(k) = A(iFin);
    TDepth(k) = T(kPing);
end

TDepth(k+1:end) = [];
A_Bab(k+1:end)  = [];
A_Tri(k+1:end)  = [];
D_Bab(k+1:end)  = [];
D_Tri(k+1:end)  = [];

tDepth = TDepth;

Longitude = my_interp1_longitude(tNav, DataPosition.Longitude, tDepth);
if all(isnan(Longitude))
    return
end

Latitude  = my_interp1(           tNav, DataPosition.Latitude,   tDepth);
Heading   = my_interp1_headingDeg(tNav, DataPosition.Heading(:), tDepth);

D_BabFiltre = filtrageButterSignal(D_Bab, 0.1, 2);
D_TriFiltre = filtrageButterSignal(D_Tri, 0.1, 2);
% figure; plot(D_Bab, '.');  grid on; hold on; plot(D_Tri, '.'); plot(D_BabFiltre, '.-r'); plot(D_TriFiltre, '.-g')
D_Bab = D_BabFiltre;
D_Tri = D_TriFiltre;

A_BabFiltre = filtrageButterSignal(A_Bab, 0.1, 2);
A_TriFiltre = filtrageButterSignal(A_Tri, 0.1, 2);
% figure; plot(A_Bab, '.');  grid on; hold on; plot(A_Tri, '.'); plot(A_BabFiltre, '.-r'); plot(A_TriFiltre, '.-g')
A_Bab = A_BabFiltre;
A_Tri = A_TriFiltre;

[Lat, Lon] = get_faucheeInLatLong(Latitude, Longitude, Heading, D_Bab, D_Tri, A_Bab, A_Tri);

Lon1 = [Lon(:,1); flipud(Lon(:,2))];
Lat1 = [Lat(:,1); flipud(Lat(:,2))];
subNaN = isnan(Lon1);
Lon1(subNaN) = [];
Lat1(subNaN) = [];
Lon1(end+1) = Lon1(1);
Lat1(end+1) = Lat1(1);
[Y, X, ~, tol] = reducem(Lat1, Lon1); %#ok<ASGLU> % tol); % Todo : tester variable tol = d * (180 / (pi * 6378137)) ?
% [Y, X, ~, tol] = reducem(Lat1, Lon1, tol*5);
% figure; plot(Lon1, Lat1, '.-b'); grid on; hold on; plot(X, Y, '.-r'); grid on

%{
subNan = isnan(sum(Lon, 2)) | isnan(sum(Lat,2));
Lon(subNan, :) = [];
Lat(subNan, :) = [];

if isempty(Lon)
    return
end

X1 = Lon(2:end-1,1);
Y1 = Lat(2:end-1,1);
X2 = flipud(Lon(2:end-1,2));
Y2 = flipud(Lat(2:end-1,2));

X1 = reduceByStep(X1, step);
X2 = reduceByStep(X2, step);
Y1 = reduceByStep(Y1, step);
Y2 = reduceByStep(Y2, step);

X = [Lon(1,1); X1; Lon(end,1); Lon(end,2); X2; Lon(1,2); Lon(1,1)];
Y = [Lat(1,1); Y1; Lat(end,1); Lat(end,2); Y2; Lat(1,2); Lat(1,1)];
%}

figure(Fig)
h = patch(X, Y, Color, 'FaceAlpha', 0.5, 'Tag', nomFic);
set_HitTest_Off(h)

%% Affichage de la navigation

axisGeo;
% oldWarn = warning('off', 'MATLAB:handle_graphics:exceptions:SceneNode'); % Exception rajout�e par JMA le 24/06/2019
drawnow
% warning(oldWarn)

%{
function y = reduceByStep(x, step)
k = 0;
y = NaN(floor(length(x) / step), 1);
for kPing=1:step:length(x)
    k = k + 1;
    sub = kPing:min(kPing+step-1, length(x));
    %     y(k) = median(x(sub));
    y(k) = mean(x(sub));
end
%}