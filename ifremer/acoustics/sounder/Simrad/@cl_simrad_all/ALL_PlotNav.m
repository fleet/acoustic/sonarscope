% Trace de la navigation et des capteurs de vitesse et de cap d'un fichier .all
%
% Syntax
%   fig = ALL_PlotNav(aKM)
%
% Input Arguments
%   aKM : Instance de cl_simrad_all
%
% Name-Value Pair Arguments
%   fullListe : 1=affichage que des datagrams presents, 0=tous les datagrams
%
% Output Arguments
%   fig : Numero de la figure de la navigation
%
% Examples
%   nomFic = getNomFicDatabase('EM1002_BELGICA_005053_raw.all')
%   aKM = cl_simrad_all('nomFic', nomFic);
%   ALL_PlotNav(aKM)
%
%   liste = listeFicOnDir2(nomDir, '.all')
%   for k=1:length(liste)
%       aKM(k) = cl_simrad_all('nomFic', liste{k});
%   end
%   ALL_PlotNav(aKM)
%
% See also cl_simrad_all plot_index Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function Fig = ALL_PlotNav(this, varargin)

[varargin, Fig]        = getPropertyValue(varargin, 'Fig',        []);
[varargin, Name]       = getPropertyValue(varargin, 'Name',       []);
[varargin, Title]      = getPropertyValue(varargin, 'Title',      []);
[varargin, IndNavUnit] = getPropertyValue(varargin, 'IndNavUnit', []);
[varargin, Color]      = getPropertyValue(varargin, 'Color',      []); %#ok<ASGLU>

%% Cr�ation de la fen�tre avec les menus et callbacks sp�cialis�s pour la nav

Fig = plot_navigation_Figure(Fig, 'Name', Name);

%% Traitement des fichiers

NbFic = length(this);
str1 = 'Trac� de la navigation des .all';
str2 = 'Plot .all navigation';
hw = create_waitbar(Lang(str1,str2), 'N', NbFic);
for k=1:NbFic
    my_waitbar(k, NbFic, hw);
    
    [flag, DataPosition] = read_position_bin(this(k), 'IndNavUnit', IndNavUnit, 'Memmapfile', 0);
    if ~flag || isempty(DataPosition.Latitude)
        messageErreurFichier(this(k).nomFic, 'ReadFailure', 'Message', 'Error when reading the XML file');
        continue
    end
    
    Time      = DataPosition.Datetime;
    Longitude = DataPosition.Longitude;
    Latitude  = DataPosition.Latitude;
    Heading   = DataPosition.Heading;
    
    switch DataPosition.EmModel % Astuce en attendant que le cl_memmapfile soit adapt� au Netcdf
        case {120; 122; 124; 300; 302; 304; 710; 712; 714}
            Immersion = [];
        otherwise
            [flag, X] = create_signal_Immersion(this(k));
            if flag
                if size(X.Data, 2) == 2
                    clear Immersion
                    Immersion(:,1) = my_interp1(X.Datetime, X.Data(:,1), Time);
                    Immersion(:,2) = my_interp1(X.Datetime, X.Data(:,2), Time);
                else
                    Immersion = my_interp1(X.Datetime, X.Data, Time);
                end
            else
                Immersion = [];
            end
    end
    
    plot_navigation(this(k).nomFic, Fig, Longitude, Latitude, Time, Heading, ...
        'Title', Title, 'Immersion', Immersion, 'Color', Color);
end
% hAxe = gca;
my_close(hw, 'MsgEnd', 'TimeDelay', 60);
