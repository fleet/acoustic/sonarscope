function ALL_PlotNavSignal(this, signalNames, varargin)

[varargin, Title]        = getPropertyValue(varargin, 'Title',        []);
[varargin, Fig]          = getPropertyValue(varargin, 'Fig',          []);
[varargin, IndNavUnit]   = getPropertyValue(varargin, 'IndNavUnit',   []);
[varargin, Name]         = getPropertyValue(varargin, 'Name',         []);
[varargin, Mute]         = getPropertyValue(varargin, 'Mute',         0);
[varargin, Step]         = getPropertyValue(varargin, 'Step',         1);
[varargin, modePlotData] = getPropertyValue(varargin, 'modePlotData', 1); %#ok<ASGLU>

if ischar(signalNames)
    signalNames = {signalNames};
end

N = length(signalNames);
hw = create_waitbar('Plot of .all navigation and signals', 'N', N);
for k=1:N
    my_waitbar(k, N, hw);
    if k == 1
        Fig = ALL_PlotNav(this, 'IndNavUnit', IndNavUnit, 'Title', Title, 'Fig', Fig, 'Name', Name);
        if N > 1
            nomFicNavigationFig = my_tempname('.fig');
            savefig(Fig, nomFicNavigationFig);
        end
    else
        Fig = openfig(nomFicNavigationFig);
    end
    plot_position_data(this, signalNames{k}, 'Fig', Fig, 'Step', Step, 'Mute', Mute, 'modePlotData', modePlotData);
    Fig.Name = [Fig.Name ' - ' signalNames{k}];
end
my_close(hw, 'MsgEnd');
if N > 1
    delete(nomFicNavigationFig);
end
