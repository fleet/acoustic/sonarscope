function ALL_RTK_Clean(this, varargin)

[varargin, IndNavUnit] = getPropertyValue(varargin, 'IndNavUnit', []); %#ok<ASGLU>

sameFilterForAllFiles = 0;

%% Nettoyage des donn�es d'Altitude RTK dans les fichiers .all

FiltreHeight = [];
nbFic = length(this);
str1 = 'V�rification de la donn�e Height RTK des .all';
str2 = 'Check RTK data of .all files';
hw = create_waitbar(Lang(str1,str2), 'N', nbFic);
for k=1:nbFic
    my_waitbar(k, nbFic, hw);
    [sameFilterForAllFiles, FiltreHeight] = ALL_RTK_Clean_unitaire(this(k), IndNavUnit, sameFilterForAllFiles, FiltreHeight);
end
my_close(hw, 'MsgEnd')


function [sameFilterForAllFiles, FiltreHeight] = ALL_RTK_Clean_unitaire(this, IndNavUnit, sameFilterForAllFiles, FiltreHeight)

%% Lecture de l'attitude

[flag, DataAttitude] = read_attitude_bin(this, 'Memmapfile', 1);
if flag
    T3 = DataAttitude.Datetime;
end

%% Lecture de la hauteur RTK

[flag, DataHeight] = SSc_ReadDatagrams(this.nomFic, 'Ssc_Height');
if ~flag
    str1 = sprintf('Le fichier "%s" n''a pas de datagrammes de "Height".', this.nomFic);
    str2 = sprintf('"%s" has no "Height" datagrams.', this.nomFic);
    my_warndlg(Lang(str1,str2), 0, 'Tag', 'DatagramHeightIsNoGood');
    return
end

%     [fig1, fig2] = plot_DatagramHeight(this);

%% Lecture de la navigation

[flag, DataPosition] = read_position_bin(this, 'IndNavUnit', IndNavUnit, 'Memmapfile', 1);
if ~flag
    str1 = sprintf('Le fichier "%s" n''a pas de datagrammes de "Position".', this.nomFic);
    str2 = sprintf('"%s" has no "Position" datagrams.', this.nomFic);
    my_warndlg(Lang(str1,str2), 0, 'Tag', 'DatagramPositionIsNoGood');
    return
end

%     figure(fig1);
%     subplot(3,2,2); PlotUtils.createSScPlot(DataPosition.PingCounter(:), DataPosition.PingCounter(:),      'y*');
%     subplot(3,2,4); PlotUtils.createSScPlot(DataPosition.PingCounter(:), DataPosition.AltitudeOfIMU(:),    'y');
%     subplot(3,2,6); PlotUtils.createSScPlot(DataPosition.PingCounter(:), DataPosition.QualityIndicator(:), 'y*');
%     figure(fig2);
%     subplot(3,1,1); plot(DataPosition.Time, DataPosition.PingCounter(:), '*r'); axis tight;
%     subplot(3,1,3); plot(DataPosition.Time, DataPosition.QualityIndicator(:), '*r'); axis tight; legend({'From Height datagram'; 'From Position datagram'})

%% Recopie du facteur de qualit� de Position dans Height

QualityIndicator = DataPosition.QualityIndicator(:);
TimePosition     = DataPosition.Time.timeMat;
TimeHeight       = DataHeight.Time.timeMat;

[subPosition, subHeight] = intersect3(TimePosition, TimeHeight, TimeHeight);

HeightType = zeros(1,DataHeight.Dimensions.NbSamples);
HeightType(subHeight) = QualityIndicator(subPosition);

flag = save_signal(this, 'Ssc_Height', 'HeightType', HeightType);
if ~flag
    return
end
QualityIndicator = QualityIndicator(subPosition);
TimePosition = TimePosition(subPosition);
TimeHeight   = TimeHeight(subHeight);

%% Recherche des occurrences RTK

subRTK = find((QualityIndicator == 4) | (QualityIndicator == 5));
subPosition45 = intersect3(TimePosition, TimeHeight(subRTK), TimeHeight(subRTK));
QI = QualityIndicator;

if isempty(subPosition45)
    str1 = sprintf('Le facteur de qualit� RTK ne donne aucune mesure valid�e pour "%s"', this.nomFic);
    str2 = sprintf('RTK QualityFactor does not give any validated data for "%s"', this.nomFic);
    my_warndlg(Lang(str1,str2), 0, 'Tag', 'QualityIndicatorNoRTK');
    FigUtils.createSScFigure(54221);
    subplot(2,1,1); PlotUtils.createSScPlot(QualityIndicator, '*'); grid on; title(Lang(str1,str2), 'Interpreter', 'none')
    subplot(2,1,2); PlotUtils.createSScPlot(DataHeight.Height(:)); grid on; title(['Height for ' this.nomFic], 'Interpreter', 'none')
    return
end

% FigUtils.createSScFigure; PlotUtils.createSScPlot(subPosition, DataPosition.AltitudeOfIMU(subPosition), 'b'); grid on; hold on; PlotUtils.createSScPlot(subHeight, DataHeight.Height(subHeight), 'r')

% Flags = false(1,DataHeight.Dimensions.NbSamples);
% Flags(subPosition45) = true;

if sameFilterForAllFiles
    if length(subPosition45) > 10
        X = DataHeight.Height;
        if (FiltreHeight.Wc ~= 0) && (FiltreHeight.Ordre ~= 0)
            X(subPosition45) = filtrageButterSignal(DataHeight.Height(subPosition45), FiltreHeight.Wc, FiltreHeight.Ordre, []);
        end
    end
else
    
    % TODO : � tester et faire une fonction commune � RDF, ALL, CINNA
    
    %% Division en sections
    
    [~, nomFic2, Ext] = fileparts(this.nomFic);
    val = DataHeight.Height;
    kFinSections = find(diff(subPosition45) ~= 1);
    kFinSections = kFinSections(:)';
    kDebSections = [1 kFinSections+1];
    kFinSections = [kFinSections length(subPosition45)];
    for k2=1:length(kFinSections)
        subSection = subPosition45(kDebSections(k2):kFinSections(k2));
        Title = sprintf('Edit RTK %s - Section %d/%d', [nomFic2 Ext], k2, length(kFinSections));
        T = DataHeight.Datetime(subSection);
        xSample = XSample('name', 'Pings',  'data', T);
        ySample = YSample('Name', 'Height', 'data', DataHeight.Height(subSection));
        signal  = ClSignal('Name', Title, 'xSample', xSample, 'ySample', ySample);
        
        d = diff(T3);
        subd = (d > 0);
        Heave = my_interp1(T3(subd), DataAttitude.Heave(subd), T);
        ySample = YSample('Name', 'MRU-Heave', 'data', -Heave);
        signal(2) = ClSignal('Name', Title, 'xSample', xSample, 'ySample', ySample);
        
        ySample = YSample('Name', 'Quality indicator', 'data', QI(subSection), 'marker', '.');
        signal(3) = ClSignal('Name', Title, 'xSample', xSample, 'ySample', ySample);
        
        if isempty(FiltreHeight)
            filterDataParametre = [];
            filtreOK = 0;
        else
            filterDataParametre = FilterDataParametre('FilterType', FilterDataParametre.FilterNameList(1), ...
                'ButterworthWc', num2str(FiltreHeight.Wc), 'ButterworthOrder', FiltreHeight.Ordre);
            filtreOK = 1;
        end
        
        s = SignalDialog(signal, 'Title', 'Edit signal', 'cleanInterpolation', 1, 'filterDataParametre', filterDataParametre);
        s.openDialog();
        if s.okPressedOut
            valSection = signal(1).ySample.data;
            val(subSection) = valSection;
            %  plot(h2, T, valSection, 'r');
            
            if filtreOK
                % Recup�ration du parametres de filtrage
                filterDataParametre= signalDialog.filterDataParametre;
                % Conversion FilterDataParametre en 'Filtre'
                type = 1;
                order = filterDataParametre.butterworthOrderParam.Value;
                wc = filterDataParametre.butterworthWcParam.Value;
                FiltreHeight = struct('Type', type, 'Ordre', order, 'Wc', wc);
            end
        end
    end
    X = val;
end
if ~isequal(DataHeight.Height, X)
    DataHeight.Height = X;
    figure(55876);
    subplot(2,1,1); plot(DataHeight.Time, DataHeight.Height); grid on; hold on, title('RTK data after data cleaning.')
    subplot(2,1,2); plot(DataHeight.Time, DataHeight.HeightType, '.-'); grid on; hold on, title('HeightType')
    
    HeightType = zeros(1,DataHeight.Dimensions.NbSamples);
    HeightType(subPosition45) = DataPosition.QualityIndicator(subPosition45);
    
    % Save Height dand le fichiers cache
    
    flag = save_signal(this, 'Ssc_Height', 'HeightType', HeightType);
    if ~flag
        return
    end
    
    flag = save_signal(this, 'Ssc_Height', 'Height', DataHeight.Height);
    if ~flag
        return
    end
end
%     my_close(fig1);
%     my_close(fig2);

% TODO MHO
% Comment� le 13/12/2019 en attente de r�cup�ration du filtre : travaux MHO
% en cours
%{
    if ~sameFilterForAllFiles && (nbFic ~= 1)
        str1 = 'TODO';
        str2 = 'Do you want to process the next files automatically with the same filter (if any) ?';
        [rep, flag] = my_questdlg(Lang(str1,str2));
        if ~flag
            break
        end
        if rep == 1
            sameFilterForAllFiles = 1;
        end
    end
%}
