function ALL_RTK_ComputeTide(this, varargin)

[varargin, IndNavUnit] = getPropertyValue(varargin, 'IndNavUnit', []); %#ok<ASGLU>

%% Import de la mar�e dans les fichiers .all

k2 = 0;
RTKGeoid   = [];
nbFic = length(this);
str1 = 'Calcul de la mar�e � partir de la donn�e Height RTK des .all';
str2 = 'Compute Tide signal from RTK Height of .all files';
hw = create_waitbar(Lang(str1,str2), 'N', nbFic);
for k=1:nbFic
    my_waitbar(k, nbFic, hw);
    aKM(k2+1) = this(k); %#ok<AGROW>
   
    [nomDir, nomFic2, Ext] = fileparts(this(k).nomFic);
    
    %% Lecture de la hauteur RTK
    
    [flag, DataHeight] = SSc_ReadDatagrams(this(k).nomFic, 'Ssc_Height');
    if ~flag
        str1 = sprintf('Le fichier "%s" n''a pas de datagrammes de "Height".', this(k).nomFic);
        str2 = sprintf('"%s" has no "Height" datagrams.', this(k).nomFic);
        my_warndlg(Lang(str1,str2), 0, 'Tag', 'DatagramHeightIsNoGood');
        continue
    end
    
    %% Lecture de Depth
    
    %{
    [flag, DataDepth] = read_depth_bin(this(k), 'Memmapfile', 1);
    if ~flag
        continue
    end
    %}
    
    %% Lecture et interpolation de la donn�e "Position"
    
    [flag, DataPosition] = read_position_bin(this(k), 'IndNavUnit', IndNavUnit, 'Memmapfile', 1);
    if ~flag
        continue
    end
    Longitude = my_interp1_longitude(DataPosition.Datetime, DataPosition.Longitude, DataHeight.Datetime, 'linear', 'extrap');
    Latitude  = my_interp1(          DataPosition.Datetime, DataPosition.Latitude,  DataHeight.Datetime, 'linear', 'extrap');

    %% Calcul de la mar�e
    
%     HeightRTK = NaN(DataDepth.Dimensions.nbPings,1);
    if ~isempty(DataHeight) && ~isempty(DataHeight.Height)
        DataHeightRTKTime = DataHeight.Datetime;
        HeightType        = DataHeight.HeightType(:);
%         DataDepthTime     = DataDepth.Time.timeMat;
        subRTK = find((HeightType == 4) | (HeightType == 5));
        if isempty(subRTK)
            str = sprintf('%s has no RTK data', this(k).nomFic);
            my_warndlg(str, 0);
        else
            RTK_HeightGGA = interp0(DataHeightRTKTime(subRTK), DataHeight.Height(subRTK), DataHeight.Datetime);
%             HeightRTK = RTK_HeightGGA;
%             RTK_FixPosition = interp0(DataHeightRTKTime, single(HeightType), DataDepthTime);
%             RTK_FixPosition(isnan(RTK_FixPosition)) = 0;
            
            [flag, TideRTK, flagRTK, RTKGeoid] = processRTK(RTK_HeightGGA, nomDir, Longitude, Latitude, ...
                'RTKGeoid', RTKGeoid, 'UseRTK', 1);
            if ~flag
%                 continue
                my_close(hw, 'MsgEnd')
                return
            end
            figure(77642); plot(DataHeight.Datetime, TideRTK); grid on; title(['Tide for ' nomFic2, Ext], 'Interpreter', 'None'); hold on;
            
            %% Save Height dans le fichier cache
            
            flag = save_attitude(this(k), 'Tide', DataHeight.Time, TideRTK);
            if ~flag
                break
            end
            
            k2 = k2 + 1;
            tabTideDatetime{k2} = DataHeight.Datetime; %#ok<AGROW>
            tabTideRTK{k2} = TideRTK; %#ok<AGROW>
        end
    end
end
my_close(hw, 'MsgEnd')

%% Edition de la mar�e

%  Voir RDF_RTK_EditTide
N = k2;
if N == 0
    return
end

for k=1:N
    [~, nomFic] = fileparts(aKM(k).nomFic);
    xSample(k) = XSample('name', 'Pings',  'data', tabTideDatetime{k}); %#ok<AGROW>
    ySample(k) = YSample('Name', 'Height', 'data', tabTideRTK{k}); %#ok<AGROW>
end

signal = ClSignal('Name', nomFic, 'xSample', xSample, 'ySample', ySample);
s = SignalDialog(signal, 'Title', 'Edit signal', 'cleanInterpolation', 1);
s.openDialog();
if s.okPressedOut
    sc = s.signalContainerList.signalList;
    for k=1:length(sc.xSample)
        TimeRTK = sc.xSample(k).data;
        TideRTK = sc.ySample(k).data;
        flag = save_attitude(aKM(k), 'Tide', TimeRTK, TideRTK);
        if ~flag
            return
        end
    end
end
