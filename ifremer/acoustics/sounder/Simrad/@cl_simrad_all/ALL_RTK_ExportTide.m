function ALL_RTK_ExportTide(this, nonFicTideIn, nomFicTideOut)

%% Import de la mar�e dans les fichiers .all

TDeb    = [];
TimeRTK = {};
TideRTK = {};
% Height     = {};
% HeightType = {};

nbFic = length(this);
str1 = 'Cr�ation de la mar�e � partir de la donn�e Height RTK des .all';
str2 = 'Create Tide signal from RTK Height of .all files';
hw = create_waitbar(Lang(str1,str2), 'N', nbFic);
for k=1:nbFic
    my_waitbar(k, nbFic, hw);
    
    %% Lecture de la hauteur RTK
    
    [flag, DataAttitude] = read_attitude_bin(this(k));
    if ~flag
        str1 = sprintf('Le fichier "%s" n''a pas de datagrammes de "Height".', this(k).nomFic);
        str2 = sprintf('"%s" has no "Height" datagrams.', this(k).nomFic);
        my_warndlg(Lang(str1,str2), 0, 'Tag', 'DatagramHeightIsNoGood');
        continue
    end
    
    if ~isfield(DataAttitude, 'Tide')
        str1 = sprintf('Pas de mar�e dans "%s"', this(k).nomFic);
        str2 = sprintf('No tide value in "%s', this(k).nomFic);
        my_warndlg(Lang(str1,str2), 0, 'Tag', 'DatagramHeightIsNoGood');
        continue
    end
    
    TimeRTK{end+1} = DataAttitude.Time.timeMat; %#ok<AGROW>
    TideRTK{end+1} = DataAttitude.Tide(:); %#ok<AGROW>
    TDeb(end+1)    = TimeRTK{end}(1); %#ok<AGROW>
end
my_close(hw, 'MsgEnd')

if isempty(TDeb)
    str1 = 'Ces fichiers n''ont pas de datagrammes de type "Height".';
    str2 = 'These files have no "Height" datagrams.';
    my_warndlg(Lang(str1,str2), 1, 'Tag', 'DatagramHeightIsNoGood');
    return
end

%% Ordre croissant

[~, ordre] = sort(TDeb);
TimeRTK    = TimeRTK(ordre);
% Height     = Height(ordre);
% HeightType = HeightType(ordre);
TideRTK    = TideRTK(ordre);

TimeRTK    = vertcat(TimeRTK{:});
% Height     = vertcat(Height{:});
% HeightType = vertcat(HeightType{:});
TideRTK    = vertcat(TideRTK{:});

% Flags = (HeightType == 4) - (HeightType == 5);
Flags = isnan(TideRTK);
subNaN = find(~Flags);

% FigUtils.createSScFigure; PlotUtils.createSScPlot(TimeRTK, TideRTK); grid on; hold on; PlotUtils.createSScPlot(TimeRTK(subRetour), TideRTKRetour(subRetour), 'r')

%% Lecture de la mar�e d'entr�e

if ~isempty(subNaN) && ~isempty(nonFicTideIn)
    [flag, TideIn] = read_Tide(nonFicTideIn);
    if ~flag
        return
    end
    
    %% On compl�te la mar�e RTK par la mar�e classique
    
    TideRTK(subNaN) = interp1(TideIn.Time.timeMat, TideIn.Value, TimeRTK(subNaN));
end

%% Ecriture du fichier de mar�e

fid = fopen(nomFicTideOut, 'w+t');
if fid == -1
    messageErreurFichier(nomFicTideOut, 'WriteFailure');
    return
end
dateMat = datevec(TimeRTK);
year    = dateMat(:,1);
ms      = dateMat(:,end)-floor(dateMat(:,end));
dateMat(:,1)     = dateMat(:,3);
dateMat(:,3)     = year;
dateMat(:,end)   = floor(dateMat(:,end));
dateMat(:,end+1) = floor(ms*1000);
dateMat          = cat(2, dateMat, -TideRTK);
dataOk = ~isnan(TideRTK);
dateMat = dateMat(dataOk,:);
fprintf(fid, '%02d/%02d/%4d  %02d:%02d:%02d.%03d  %f\n', dateMat');
fclose(fid);
