function ALL_RTK_Plot(this)

%% Import de la mar�e dans les fichiers .all

fig2 = [];
Color = 'brgkm';
nbFic = length(this);
str1 = 'Visualisation des donn�es RTK des .all';
str2 = 'Plot RTK data of .all files';
hw = create_waitbar(Lang(str1,str2), 'N', nbFic);
for k=1:nbFic
    my_waitbar(k, nbFic, hw);
	[~, fig2] = plot_DatagramHeight(this(k), 'fig1', 'No', 'fig2', fig2, 'Color', Color(1 + mod(k-1,length(Color))));
end
my_close(hw, 'MsgEnd')
