function ALL_RTK_Raz(this)

%% Import de la mar�e dans les fichiers .all

nbFic = length(this);
str1 = 'RAZ des donn�es RTK des .all';
str2 = 'Reset RTK data of .all files.';
hw = create_waitbar(Lang(str1,str2), 'N', nbFic);
for k=1:nbFic
    my_waitbar(k, nbFic, hw);
    
    flag = existCacheNetcdf(this(k).nomFic);
    if flag
        flag = ALL_RTK_RazNetcdf(this(k)); %#ok<NASGU>
    else
        flag = ALL_RTK_RazXMLBin(this(k)); %#ok<NASGU>
    end
end
my_close(hw, 'MsgEnd')


function flag = ALL_RTK_RazXMLBin(this)

% Temporaire, histoire de tout remettre � z�ro
FormatVersion = 20101118;
[nomDir, nomFicSeul] = fileparts(this.nomFic);
nomDirRacine = fullfile(nomDir, 'SonarScope', nomFicSeul);
flag = all2ssc_Height(nomDirRacine, FormatVersion); %#ok<NASGU>

[flag, DataAttitude] = read_attitude_bin(aKM);
if isfield(DataAttitude, 'Tide')
    DataAttitude.Tide(:) = 0;
    flag = save_attitude(aKM, 'Tide', DataAttitude.Time, DataAttitude.Tide);
end


function flag = ALL_RTK_RazNetcdf(this)

[nomDirRacine, nomFic] = fileparts(this.nomFic);
nomFicNc = fullfile(nomDirRacine, 'SonarScope', [nomFic '.nc']);
if ~exist(nomFicNc, 'file')
    flag = 0;
    return
end

ncID  = netcdf.open(nomFicNc, 'WRITE');

grpID = netcdf.inqNcid(ncID, 'Attitude');
varID = netcdf.inqVarID(grpID, 'Tide');
Tide = netcdf.getVar(grpID, varID);
Tide(:) = NaN;
NetcdfUtils.putVarVal(grpID, varID, Tide, 0)

try
    grpID = netcdf.inqNcid(ncID, 'Height');
    varID = netcdf.inqVarID(grpID, 'HeightInitial');
    HeightInitial = netcdf.getVar(grpID, varID);
    varID = netcdf.inqVarID(grpID, 'Height');
    NetcdfUtils.putVarVal(grpID, varID, HeightInitial, 0)
catch ME
    ME.getReport
end

netcdf.close(ncID);

flag = 1;
