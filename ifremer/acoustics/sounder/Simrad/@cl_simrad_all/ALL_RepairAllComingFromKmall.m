function flag = ALL_RepairAllComingFromKmall(this)

for k=1:length(this)
    flag = ALL_RepairAllComingFromKmall_unitaire(this(k));
end


function flag = ALL_RepairAllComingFromKmall_unitaire(this)

%% Lecture de Dataruntime

[flag, DataRuntime] = SSc_ReadDatagrams(this.nomFic, 'Ssc_Runtime');
if ~flag
    return
end

%% Lecture des donn�es RawDepth

% [flag, DataDepth] = read_depth_bin(this, 'Memmapfile', -1);
% if ~flag
%     return
% end

%% Lecture des donn�es RawRange

[flag, DataRaw] = read_rawRangeBeamAngle(this);
if ~flag
    return
end
% DataRaw.CentralFrequency
% DataRaw.SignalLength

%% Cr�ation des signaux Runtime

[~, signalContainerFromRuntime] = create_signalsFromRuntime(this, DataRaw.Datetime, 'DataRuntime', DataRuntime);
% signalContainerFromRuntime.plot
% signalContainerFromRuntime.editProperties

Mode      = signalContainerFromRuntime(1).signalList(1).ySample.data;
SwathMode = signalContainerFromRuntime(1).signalList(2).ySample.data;

%% Red�termination du PingSequence par une heuristique

PingSequence = DataRaw.PingSequence; % H�las, ce PingSequence est d�termin� � partir des fr�quences qui sont fausses dans le .all donc, il faut le red�terminer

subDoubleSwath = find(SwathMode > 1);
if ~isempty(subDoubleSwath)
    % SonarScope(DataRaw.TiltAngle)
    % SonarScope(DataDepth.AlongDist)
    dt = diff(DataRaw.Datetime(subDoubleSwath));
    dt.Format = 'hh:mm:ss.SSS';
%     figure; plot(dt, '*'); grid
    C = unique(dt);
    minDt = C(1);
    subSwathArriere = (dt <= 2*minDt);
    PingSequence = [1; subSwathArriere + 1];
%     figure; plot(PingSequence); grid
end

%% Inspection des fr�quences des secteurs et des longueurs d'impulsion

nomFicRecherche = sprintf('EM%d.xml', DataRaw.EmModel);
nomFicXML = getNomFicDatabase(nomFicRecherche, 'NoMessage');
X = xml_mat_read(nomFicXML); % lecture du fichier de param�tres

% X.Sonar.Name
for k=1:length(SwathMode)
    if SwathMode(k) == 1 % CW
            SwathNumber = 1;
            nbSectors = X.Sonar.Mode1(SwathNumber).Mode2(Mode(k)).TxNbBeams;
            subSectors = 1:nbSectors;
    else % Double swath up ou down
        if PingSequence(k) == 1 % First swath of Double swath mode
            SwathNumber = 2;
            nbSectors = X.Sonar.Mode1(SwathNumber).Mode2(Mode(k)).TxNbBeams;
            n = nbSectors / 2;
            subSectors = 1:n;
        else % Second swath of Double swath mode
            SwathNumber = 2;
            nbSectors = X.Sonar.Mode1(SwathNumber).Mode2(Mode(k)).TxNbBeams;
            n = nbSectors / 2;
            subSectors = (n+1):nbSectors;
        end
    end
    %     X.Sonar.Mode1(SwathNumber).Mode2(Mode(k))
    PulseLength = X.Sonar.Mode1(SwathNumber).Mode2(Mode(k)).PulseLength;
    Tmp = X.Sonar.Mode1(SwathNumber).Mode2(Mode(k)).TxBeam(:);
    if isempty(Tmp)
        continue
    end
    Freq = [Tmp.Freq];
    SignalWaveformIdentifier = strcmp(X.Sonar.Mode1(SwathNumber).Mode2(Mode(k)).SignalType, 'FM');
    
    if length(PulseLength) == nbSectors
        PulseLength = PulseLength(subSectors);
    end
    if length(Freq) == nbSectors
        Freq = Freq(subSectors);
    end
    if length(SignalWaveformIdentifier) == nbSectors
        SignalWaveformIdentifier = SignalWaveformIdentifier(subSectors);
    end
    
    subIci = 1:length(Freq);
    DataRaw.TiltAngle(k,subIci) = 0; % On ne peut pas les deviner, on impose 0 pour que l'algo de calcul des angles d'incidence d�livre quelque-chose d'acceptable
%     DataRaw.FocusRange(k,:)
    DataRaw.SignalLength(k,subIci) = PulseLength;
%     DataRaw.SectorTransmitDelay(k,:) = 
    DataRaw.CentralFrequency(k,subIci) = Freq;
%     DataRaw.MeanAbsorptionCoeff
    DataRaw.SignalWaveformIdentifier(k,subIci) = SignalWaveformIdentifier;
%     DataRaw.TransmitSectorNumberTx
    SignalBandwith = 1.5 * (1/min(PulseLength)); % les BW en FM est la m�me que pour les secteurs centraux en CW
    DataRaw.SignalBandwith(k,subIci) = SignalBandwith;
end

%% Save DataRaw

flag = save_signalNetcdf(this, 'Ssc_RawRangeBeamAngle', 'TiltAngle', DataRaw.TiltAngle);
if ~flag
    return
end

flag = save_signalNetcdf(this, 'Ssc_RawRangeBeamAngle', 'SignalLength', DataRaw.SignalLength / 1000);
if ~flag
    return
end

flag = save_signalNetcdf(this, 'Ssc_RawRangeBeamAngle', 'SignalBandwith', DataRaw.SignalBandwith * 1000);
if ~flag
    return
end

flag = save_signalNetcdf(this, 'Ssc_RawRangeBeamAngle', 'CentralFrequency', DataRaw.CentralFrequency * 1000);
if ~flag
    return
end

flag = save_signalNetcdf(this, 'Ssc_RawRangeBeamAngle', 'SignalWaveformIdentifier', DataRaw.SignalWaveformIdentifier);
if ~flag
    return
end

% flag = save_signalNetcdf(this, 'Ssc_RawRangeBeamAngle', 'PingSequence', DataRaw.PingSequence);
% if ~flag
%     return
% end

%% Recreate the layers done in all2ssc

% flag = ALL_AbsorpCoeff_SSP(this, 'DataDepth', DataDepth);
flag = ALL_AbsorpCoeff_SSP(this);
if ~flag
    return
end

%% The End

flag = 1;


%{
function flag = write_depth_mask(this, Data)

flag = existCacheNetcdf(this.this.nomFic);
if flag
    flag = write_depth_maskNetcdf(this, Data);
else
    flag = write_depth_maskXMLBin(this, Data);
end


function flag = write_depth_maskNetcdf(this, Data)

%% Ecriture des fichiers binaires des images

Image = Data.Mask(:,:);

if isa(Image, 'single') || isa(Image, 'double')
    Image(isnan(Image)) = 0;
    Image = uint8(Image);
end

flag = save_signalNetcdf(this, 'Ssc_Depth', 'Mask', Image);
if ~flag
    return
end
%}
