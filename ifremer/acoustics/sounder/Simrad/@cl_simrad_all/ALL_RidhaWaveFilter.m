function ALL_RidhaWaveFilter(this, Periode)

%% Filtrage de TransducerDepth dans les fichiers .all

nbFic = length(this);
str1 = 'Filtre ondulatrions de la bathymétrie dans les .all.';
str2 = 'Wave filter of bathymetry of .all files.';
hw = create_waitbar(Lang(str1,str2), 'N', nbFic);
for k=1:nbFic
    my_waitbar(k, nbFic, hw);
    
    %% Leacture de la donnée
    
    [flag, DataDepth] = read_depth_bin(this(k), 'Memmapfile', 0);
    if ~flag
        return
    end
    
    %% Suppression du heave
    
    if size(DataDepth.TransducerDepth,2) == 1
        depth = DataDepth.Depth - DataDepth.TransducerDepth;
    else
        n = size(DataDepth.Depth,2);
        ns2 = floor(n / 2);
        subBab = 1:ns2;
        subTri = (ns2+1):n;
        depth(:,subBab) = bsxfun(@minus, DataDepth.Depth(:,subBab), DataDepth.TransducerDepth(:,1));
        depth(:,subTri) = bsxfun(@minus, DataDepth.Depth(:,subTri), DataDepth.TransducerDepth(:,2));
    end
    
    %% Algorithme
    
    depthCorr = nan(size(depth));
    for k2=1:size(depth,2)
        ind = find(~isnan(depth(:,k2)));
        dep = depth(ind,k2);
        depf = ButterworthLowPassFilter(dep, 1, 1/Periode, 8);
        depthCorr(ind,k2) = depf;
    end
    % diffim = depthCorr - depth;
    % figure; plot(diffim(100,:)); grid on;
%     depthCorr2 = nan(size(depth));
%     for k1=1:size(depth,1)
%         ind = find(~isnan(depthCorr(k1,:)));
%         dep = depthCorr(k1,ind);
%         depf = ButterworthLowPassFilter(dep, 1, 0.05, 8);
%         depthCorr2(k1,ind) = depf;
%     end
    
    %% Rétablissement du heave
    
    if size(DataDepth.TransducerDepth,2) == 1
        DataDepth.Depth = depthCorr + DataDepth.TransducerDepth;
    else
        n = size(DataDepth.Depth,2);
        ns2 = floor(n / 2);
        subBab = 1:ns2;
        subTri = (ns2+1):n;
        DataDepth.Depth(:,subBab) = bsxfun(@plus, depthCorr(:,subBab), DataDepth.TransducerDepth(:,1));
        DataDepth.Depth(:,subTri) = bsxfun(@plus, depthCorr(:,subTri), DataDepth.TransducerDepth(:,2));
    end
    
    %% Ecriture du résultat
    
    flag = write_depth_bin(this(k), DataDepth);
    if ~flag
        % TODO : Message
    end
    
end
my_close(hw, 'MsgEnd')


function x = ButterworthLowPassFilter(x, fe, f0, nb)

transpose = 0;
n = length(x);
if n == size(x,1)
    transpose = 1;
    x = x';
end

% Window size
n2 = ceil(n/2);

nt = n + 2*(n2-1);
% Mirror processing to correct for edge effects
if rem(nt,2) == 0
    x = [fliplr(x(2:n2)), x, fliplr(x(n-n2+1:n-1))];
else
    x = [fliplr(x(2:n2)), x, fliplr(x(n-n2+2:n-1))];
end

f = [0:(nt/2), (-nt/2+1):-1] * fe / nt;
xf = 1 ./ sqrt(1 + (f/f0) .^ nb); % Butterworth filter

x = real(ifft(xf .* fft(x)));

x = x(n2:n+n2-1);

if transpose == 1
    x = x';
end
