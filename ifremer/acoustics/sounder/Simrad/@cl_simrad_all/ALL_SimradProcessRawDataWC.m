function [flag, Carto] = ALL_SimradProcessRawDataWC(this, DTM, varargin)

[varargin, Names]               = getPropertyValue(varargin, 'Names',               []);
[varargin, suby]                = getPropertyValue(varargin, 'suby',                []); % TODO : remplacer par subPings
[varargin, CorrectionTVG]       = getPropertyValue(varargin, 'CorrectionTVG',       30);
[varargin, MaskBeyondDetection] = getPropertyValue(varargin, 'MaskBeyondDetection', 0);
[varargin, CLimRaw]             = getPropertyValue(varargin, 'CLimRaw',             [-64 0]);
[varargin, CLimComp]            = getPropertyValue(varargin, 'CLimComp',            [-5 35]);
[varargin, flagTideCorrection]  = getPropertyValue(varargin, 'TideCorrection',      0);
[varargin, typeAlgoWC]          = getPropertyValue(varargin, 'typeAlgoWC',          1);
[varargin, IndNavUnit]          = getPropertyValue(varargin, 'IndNavUnit',          []); %#ok<ASGLU>

Carto = [];

%% Garbage Collector de Java

java.lang.System.gc

%% Lecture des donn�es

[flag, DataDepth] = read_depth_bin(this, 'Memmapfile', 0);
if ~flag
    return
end

[flag, DataWC] = read_WaterColumn_bin(this, 'Memmapfile', 0);
if ~flag
    str1 = sprintf('Il n''y a pas de datagrammes de Water Column dans le fichier "%s".', this.nomFic);
    str2 = sprintf('No Water Column datagrams in file "%s".', this.nomFic);
    my_warndlg(Lang(str1,str2), 0, 'TimeDelay', 60);
    return
end

[flag, DataRaw] = read_rawRangeBeamAngle(this, 'MemmapFile', 0);
if ~flag
    return
end

[flag, DataInstalParam, isDual] = read_installationParameters_bin(this);
if ~flag
    return
end

[flag, IdentSonar] = SimradModel2IdentSonar(DataWC.EmModel, DataDepth.EmModel, 'isDual', isDual);
if ~flag
    return
end

% TODO : am�liorer la transmission du mode
SonarDescription = cl_sounder('Sonar.Ident', IdentSonar, 'Sonar.Mode_1', 1, ...
    'Sonar.SystemSerialNumber', DataWC.SystemSerialNumber);

if isempty(suby)
    suby = 1:DTM.nbRows;
else
    %  suby = suby(1):suby(end);
end

%% Recherche des pings communs entre sources de donn�es

% subPingImage     =  DTM.Sonar.PingCounter(suby);
SonarPingCounter = get(DTM, 'SonarPingCounter');
subPingImage     = SonarPingCounter(suby);
subPingFichier   = DataWC.PingCounter(:,1);
subPingDataDepth = DataDepth.PingCounter(:,1);
[subThis, subWC, subDepth] = intersect3(subPingImage, subPingFichier, subPingDataDepth);
subThis = suby(subThis);
if isempty(subThis)
    flag = false;
    return
end

%%

% % Comment� par JMA le 27/03/2021
% [nomDir, nom] = fileparts(this.nomFic);
% nomDir = fullfile(nomDir, 'SonarScope', nom);
% flag = exist(nomDir, 'dir');
% if ~flag
%     return
% end
% nomFicXml = fullfile(nomDir, 'ALL_WaterColumn.xml');
% flag = exist(nomFicXml, 'file');
% if ~flag
%     nomFicXml = fullfile(nomDir, 'WCD_WaterColumn.xml');
%     flag = exist(nomFicXml, 'file');
%     if ~flag
%         return
%     end
% end
% DataALL_WaterColumn = xml_mat_read(nomFicXml);

DataALL_WaterColumn = DataWC; % Modif JMA le le 27/03/2021

% TODO GLU : c'est ici qu'on vient lire les donn�es WC

%% Recherches des bornes des images si export pour Voxler

[flag, DataPosition] = read_position_bin(this, 'IndNavUnit', IndNavUnit, 'Memmapfile', 0);
if ~flag
    return
end

%% Cr�ation des r�pertoires

if ~isempty(Names.nomFicXmlRaw)
    [nomDirExportRaw, nomFic] = fileparts(Names.nomFicXmlRaw);
    nomDirExportRaw = fullfile(nomDirExportRaw, nomFic);
    DataWCRaw.name = nomFic;
    if exist(nomDirExportRaw, 'dir')
        rmdir(nomDirExportRaw,'s')
    end
end

if ~isempty(Names.nomFicXmlComp)
    [nomDirExportComp, nomFic] = fileparts(Names.nomFicXmlComp);
    nomDirExportComp = fullfile(nomDirExportComp, nomFic);
    DataWCComp.name = nomFic;
    if exist(nomDirExportComp, 'dir')
        rmdir(nomDirExportComp, 's')
    end
end

%% Initialisation des tableaux

nbPings = length(subThis);

%% Pr�paration de la structure

nbBeams = DataWC.Dimensions.nbRx;
T = get(DTM, 'SonarTime');
TimePing = T(suby);

if ~isempty(Names.nomFicXmlRaw)
    DataWCRaw.from            = 'Unkown';
    DataWCRaw.PingNumber      = suby;
    DataWCRaw.Date            = TimePing.date;
    DataWCRaw.Hour            = TimePing.heure;
    DataWCRaw.LatitudeTop     = NaN(nbPings,nbBeams);
    DataWCRaw.LongitudeTop    = NaN(nbPings,nbBeams);
    DataWCRaw.LatitudeBottom  = NaN(nbPings,nbBeams);
    DataWCRaw.LongitudeBottom = NaN(nbPings,nbBeams);
    DataWCRaw.DepthTop        = NaN(nbPings,nbBeams);
    DataWCRaw.DepthBottom     = NaN(nbPings,nbBeams);
    DataWCRaw.ImageSegment    = NaN(nbPings,nbBeams);
end
if ~isempty(Names.nomFicXmlComp)
    DataWCComp.from            = 'Unkown';
    DataWCComp.PingNumber      = suby;
    DataWCComp.Date            = TimePing.date;
    DataWCComp.Hour            = TimePing.heure;
    DataWCComp.LatitudeTop     = NaN(nbPings,nbBeams);
    DataWCComp.LongitudeTop    = NaN(nbPings,nbBeams);
    DataWCComp.LatitudeBottom  = NaN(nbPings,nbBeams);
    DataWCComp.LongitudeBottom = NaN(nbPings,nbBeams);
    DataWCComp.DepthTop        = NaN(nbPings,nbBeams);
    DataWCComp.DepthBottom     = NaN(nbPings,nbBeams);
    DataWCComp.ImageSegment    = NaN(nbPings,nbBeams);
end


%% Lecture et interpolation de la donnee "Attitude"

% [flag, DataAttitude] = read_attitude_bin(this, 'Memmapfile', 1);
[flag, DataAttitude] = read_attitude_bin(this, 'Memmapfile', 0);
if ~flag || isempty(DataAttitude) || isempty(DataAttitude.Roll)
    return
end
[~, DataRuntime] = SSc_ReadDatagrams(this.nomFic, 'Ssc_Runtime', 'LogSilence', 0); % Non test�

% TDepth = DataDepth.Time.timeMat;
% TDepth = cl_time('timeMat', TDepth(subDepth));
% Roll  = my_interp1(DataAttitude.Time, DataAttitude.Roll,   TDepth, 'linear', 'extrap');
% Pitch = my_interp1(DataAttitude.Time, DataAttitude.Pitch,  TDepth, 'linear', 'extrap');
% Heave = my_interp1(DataAttitude.Time, DataAttitude.Heave,  TDepth, 'linear', 'extrap');

% if isempty(Tide) && isfield(DataAttitude, 'Tide') && ~isempty(DataAttitude.Tide)
%     Tide.Time  = DataAttitude.Time;
%     Tide.Value = DataAttitude.Tide;
% end
%
% if isempty(Draught) && isfield(DataAttitude, 'Draught') && ~isempty(DataAttitude.Draught)
%     Draught.Time  = DataAttitude.Time;
%     Draught.Value = DataAttitude.Draught;
% end

% if isempty(Draught) && isfield(DataAttitude, 'TrueHeave') && ~isempty(DataAttitude.TrueHeave)
%     TrueHeave.Time  = DataAttitude.Time;
%     TrueHeave.Value = DataAttitude.TrueHeave;
%     'TrueHeave Jamais test�, �a risque de beuguer'
%     Heave = my_interp1(TrueHeave.Time, TrueHeave.Value,  DataDepth.Time, 'linear', 'extrap');
% end

%% Recherche de la position longitudinale de l'antenne d'�mission par rapport au poinr de r�f�rence du sondeur

SonarShip = get(DTM, 'SonarShip');
Carto = get_Carto(DTM);

%% Boucle sur les pings

subPingOK = false(1,nbPings);
kImage = 0;
[~, nomFicSeul, Ext] = fileparts(this.nomFic);
str1 = sprintf('Traitement des pings %d � %d de WC de %s', subThis(1), subThis(end), [nomFicSeul Ext]);
str2 = sprintf('Processing WC pings %d to %d of %s', subThis(1), subThis(end), [nomFicSeul Ext]);
hw = create_waitbar(Lang(str1,str2), 'N', nbPings);
for k=1:nbPings
    my_waitbar(k, nbPings, hw)
    
    [b, Carto, TransmitSectorNumber, DetectedRangeInSamples] = import_WaterColumn_All(this, ...
        DataDepth, DataWC, DataRaw, DataPosition, DataAttitude, DataRuntime, DataInstalParam, SonarDescription, ...
        'Carto', Carto, 'subPing', subThis(k), 'Mute', 1, 'CorrectionTVG', CorrectionTVG, ...
        'XML_ALL_WaterColumn', DataALL_WaterColumn);
    if isempty(b)
        continue
    end
    b = b(1);
    subPingOK(k) = true;
    kImage = kImage + 1;
    
    subNaN = find(DetectedRangeInSamples < 5);
    if ~isempty(subNaN)
        subNonNaN = find(DetectedRangeInSamples >= 5);
        %         DetectedRangeInSamples(subNaN) = floor(interp1(subNonNaN, DetectedRangeInSamples(subNonNaN), subNaN, 'linear', 'extrap'));
        DetectedRangeInSamples(subNaN) = floor(interp1(subNonNaN, DetectedRangeInSamples(subNonNaN), subNaN, 'linear')); % Modif JMA le 05/10/2016
        %         DetectedRangeInSamples(isnan(DetectedRangeInSamples)) = 0; % Ajout JMA le 05/10/2016 ! Erreur, les donn�es sont visualis�es au point 0,0 (Afrique) dans 3DV
        DetectedRangeInSamples(isnan(DetectedRangeInSamples)) = max(DetectedRangeInSamples); % Ajout JMA le 05/10/2016
    end
    
    if ~isempty(Names.nomFicXmlComp)
        comp = compensation_WaterColumn(b, TransmitSectorNumber, 'NoStats');
        if MaskBeyondDetection
            [flag, X] = MaskVert(comp, 'kY', DetectedRangeInSamples+1, 'NoStats');
            if flag
                comp = X;
            end
        end
    end
    if MaskBeyondDetection
        % On fait la compensation avant d'avoir fait le masquage sinon
        % l'op�ration est moins bonne
        [flag, X] = MaskVert(b, 'kY', DetectedRangeInSamples+1, 'NoStats');
        if flag
            b = X;
        end
    end
    
    [flag, lat0, lon0, LatitudeBottom, LongitudeBottom, DepthBottom] = calcul_positionRawDataWC(b, ...
        'SonarShip', SonarShip, 'typeAlgoWC', typeAlgoWC);
    if ~flag
        return
    end
    %     FigUtils.createSScFigure(67676); PlotUtils.createSScPlot(LongitudeBottom, LatitudeBottom, '*b'); grid on; hold on; PlotUtils.createSScPlot(lon0, lat0, 'or')
    
    SampleBeamData = get(b, 'SampleBeamData');
    TransducerDepth = SampleBeamData.TransducerDepth;
    
    if ~isempty(Names.nomFicXmlRaw)
        DataWCRaw.LatitudeTop(k,:)     = lat0 + zeros(1, nbBeams);
        DataWCRaw.LongitudeTop(k,:)    = lon0 + zeros(1, nbBeams);
        DataWCRaw.LatitudeBottom(k,:)  = LatitudeBottom;
        DataWCRaw.LongitudeBottom(k,:) = LongitudeBottom;
        DataWCRaw.DepthTop(k,:)        = -TransducerDepth(1) + zeros(1, nbBeams); % (1) rajout� par JMA le 31/08/2�16 pour fichier EM2040 Thalia !!!
        DataWCRaw.DepthBottom(k,:)     = -TransducerDepth(1) + DepthBottom;
        DataWCRaw.ImageSegment(k,:)    = TransmitSectorNumber;
        DataWCRaw.EndData(k,:)         = DetectedRangeInSamples;
        
        %% TODO TODO TODO TODO 24/11/2016 FLOWS voir import_WaterColumn_All et sonar_SimradProcessWC
        Tide = get(b, 'Tide');
        TideValue = Tide(1);
        if flagTideCorrection && ~isnan(TideValue)
            DataWCRaw.DepthTop(k,:)    = DataWCRaw.DepthTop(k,:)    + TideValue; % Ajout JMA le 16/10/2016
            DataWCRaw.DepthBottom(k,:) = DataWCRaw.DepthBottom(k,:) + TideValue; % Ajout JMA le 16/10/2016
        end
    end
    
    if ~isempty(Names.nomFicXmlComp)
        DataWCComp.LatitudeTop(k,:)     = lat0 + zeros(1, nbBeams);
        DataWCComp.LongitudeTop(k,:)    = lon0 + zeros(1, nbBeams);
        DataWCComp.LatitudeBottom(k,:)  = LatitudeBottom;
        DataWCComp.LongitudeBottom(k,:) = LongitudeBottom;
        DataWCComp.DepthTop(k,:)        = -TransducerDepth(1) + zeros(1, nbBeams);
        DataWCComp.DepthBottom(k,:)     = -TransducerDepth(1) + DepthBottom;
        DataWCComp.ImageSegment(k,:)    = TransmitSectorNumber;
        DataWCComp.EndData(k,:)         = DetectedRangeInSamples;
        
        %% TODO TODO TODO TODO 24/11/2016 FLOWS voir import_WaterColumn_All et sonar_SimradProcessWC
        Tide = get(b, 'Tide');
        TideValue = Tide(1);
        if flagTideCorrection && ~isnan(TideValue)
            DataWCComp.DepthTop(k)    = DataWCComp.DepthTop(k)    + TideValue; % Ajout JMA le 16/10/2016
            DataWCComp.DepthBottom(k) = DataWCComp.DepthBottom(k) + TideValue; % Ajout JMA le 16/10/2016
        end
    end
    
    %% Cr�ation si n�cessaire du r�pertoire d'archivage de l'image
    
    kPing = kImage;
%     kPing = subThis(k); % TODO : en attente v�rif GLOBE 
    
    if ~isempty(Names.nomFicXmlRaw)
        kRep = floor(kPing/100);
        [nomDir, nomFic2] = fileparts(Names.nomFicXmlRaw);
        nomDir = fullfile(nomDir, nomFic2);
        if ~exist(nomDir, 'dir')
            flag = mkdir(nomDir);
            if ~flag
                messageErreurFichier(nomDirPng, 'WriteFailure');
                return
            end
        end
        nomDirPngRaw = fullfile(nomDir, num2str(kRep, '%03d'));
        if ~exist(nomDirPngRaw, 'dir')
            flag = mkdir(nomDirPngRaw);
            if ~flag
                messageErreurFichier(nomDirPngRaw, 'WriteFailure');
                return
            end
        end
    end
    
    if ~isempty(Names.nomFicXmlComp)
        kRep = floor(kPing/100);
        [nomDir, nomFic2] = fileparts(Names.nomFicXmlComp);
        nomDir = fullfile(nomDir, nomFic2);
        if ~exist(nomDir, 'dir')
            flag = mkdir(nomDir);
            if ~flag
                messageErreurFichier(nomDirPngComp, 'WriteFailure');
                return
            end
        end
        nomDirPngComp = fullfile(nomDir, num2str(kRep, '%03d'));
        if ~exist(nomDirPngComp, 'dir')
            flag = mkdir(nomDirPngComp);
            if ~flag
                messageErreurFichier(nomDirPngComp, 'WriteFailure');
                return
            end
        end
    end
    
    %% Cr�ation de l'image

    subNaN = findNaN(b);
    if ~isempty(Names.nomFicXmlRaw)
        
        %% Export .png
        
        nomFicPng = sprintf('%05d.png', kPing);
        nomFicPng = fullfile(nomDirPngRaw, nomFicPng);
        DataWCRaw.SliceName{ kImage} = nomFicPng;
        DataWCComp.SliceName{kImage} = nomFicPng;
        
        [bInd, flag] = Intensity2Indexed(b, 'CLim', CLimRaw, 'NoStats');
        if ~flag
            return
        end
        map = b.Colormap;
        
        nbRows = bInd.nbRows;
        %     nbColumns = bInd.nbColumns;
%         x = get(bInd, 'x');
        I = get(bInd, 'Image');
        I(I == 0) = 1;
        % I(I<5) = 5;
        I(subNaN) = 0;
        
        sumX = sum(I == 0);
        subX = find(sumX ~= nbRows);
        if isempty(subX)
            %% TODO message d'erreur
            flag = 0;
            return
        end
%         x = x(subX);
%         nbColumns = length(x);
        imwrite(I(:,subX), map, nomFicPng, 'Transparency', 0);
        
        %% Export .tif
        
        nomFicPng = sprintf('%05d.tif', kPing);
        nomFicPng = fullfile(nomDirPngRaw, nomFicPng);
        DataWCRaw.SliceName{ kImage} = nomFicPng;
        DataWCComp.SliceName{kImage} = nomFicPng;
        I = get_Image(b);
        I = I(:,:);
        [nbRows, nbColumns] = size(I);
        export_ImageTif32bits(I, nomFicPng, 'Mute', 1);
    end
    
    if ~isempty(Names.nomFicXmlComp)
        nomFicPng = sprintf('%05d.png', kPing);
        nomFicPng = fullfile(nomDirPngComp, nomFicPng);
        comp.CLim = CLimComp;
        [compInd, flag] = Intensity2Indexed(comp, 'NoStats');
        if ~flag
            return
        end
        map = b.Colormap;
        I = get(compInd, 'Image');
        I(I == 0) = 1;
        I(subNaN) = 0;
        imwrite(I(:,subX), map, nomFicPng, 'Transparency', 0)
        
        %% Export .tif
        
        nomFicPng = sprintf('%05d.tif', kPing);
        nomFicPng = fullfile(nomDirPngComp, nomFicPng);
        I = get_Image(comp);
        I = I(:,:);
        [nbRows, nbColumns] = size(I);
        export_ImageTif32bits(I, nomFicPng, 'Mute', 1);
     end
end
my_close(hw, 'MsgEnd')
% close_waitbar(hw, 'SummaryLatency', 60)
% toc
% profile report

%% Ecriture du fichier des coordonn�es des �chogrammes polaires

% FigUtils.createSScFigure; PlotUtils.createSScPlot(DataWCRaw.LongitudeBottom, DataWCRaw.LatitudeBottom, '+b')
if ~isempty(Names.nomFicXmlRaw)
    DataWCRaw.Tide  = zeros(nbPings,1);
    DataWCRaw.Heave = zeros(nbPings,1);
    DataWCRaw = supressInvalidPings(DataWCRaw, subPingOK);
    flag = write_XML_ImagesAlongNavigation(Names.nomFicXmlRaw, DataWCRaw, nbColumns, nbRows);
end

if ~isempty(Names.nomFicXmlComp)
    DataWCComp.Tide  = zeros(nbPings,1);
    DataWCComp.Heave = zeros(nbPings,1);
    DataWCComp = supressInvalidPings(DataWCComp, subPingOK);
    flag = write_XML_ImagesAlongNavigation(Names.nomFicXmlComp, DataWCComp, nbColumns, nbRows);
end
% if (nbPings > 1) && (k == 1)
%     return
% end



function X = supressInvalidPings(X, subPingOK)

X.PingNumber = X.PingNumber(subPingOK);
X.Date       = X.Date(subPingOK);
X.Hour       = X.Hour(subPingOK);
X.Tide       = X.Tide(subPingOK);
X.Heave      = X.Heave(subPingOK);

% X.SliceName = X.SliceName(subPingOK);

X.LatitudeTop     = X.LatitudeTop(    subPingOK,:);
X.LongitudeTop    = X.LongitudeTop(   subPingOK,:);
X.LatitudeBottom  = X.LatitudeBottom( subPingOK,:);
X.LongitudeBottom = X.LongitudeBottom(subPingOK,:);
X.DepthTop        = X.DepthTop(       subPingOK,:);
X.DepthBottom     = X.DepthBottom(    subPingOK,:);
X.ImageSegment    = X.ImageSegment(   subPingOK,:);
X.EndData         = X.EndData(        subPingOK,:);
