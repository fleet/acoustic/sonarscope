function ALL_Spike1D(this, varargin)

% TODO : afficher le r�sultat et demander si on valide comme dans ALL_Spike2D

[varargin, StepDisplay] = getPropertyValue(varargin, 'StepDisplay', 0);
[varargin, nbRepeats]   = getPropertyValue(varargin, 'nbRepeats',   1);
[varargin, Threshold]   = getPropertyValue(varargin, 'Threshold',   []); %#ok<ASGLU>

if StepDisplay == 0
    Fig = [];
else
    Fig = FigUtils.createSScFigure;
end

NbFic = length(this);
str1 = 'Traitement ALL_Spike1D';
str2 = 'Processing ALL_Spike1D';
hw = create_waitbar(Lang(str1,str2), 'N', NbFic);
for k=1:NbFic
    my_waitbar(k, NbFic, hw);
    Fig = ALL_Spike1Dthis_unitaire(this(k), nbRepeats, Threshold, Fig, StepDisplay);
end
my_close(hw, 'MsgEnd')
my_close(Fig)
    

function Fig = ALL_Spike1Dthis_unitaire(this, nbRepeats, Threshold, Fig, StepDisplay)

[flag, DataDepth] = read_depth_bin(this);
if ~flag
    return
end

[~, nomFic] = fileparts(this.nomFic);

str1 = sprintf('Traitement du fichier %s', nomFic);
str2 = sprintf('Processing file %s', nomFic);
hw2 = create_waitbar(Lang(str1,str2), 'N', DataDepth.Dimensions.nbPings);
for kPing=1:DataDepth.Dimensions.nbPings
    my_waitbar(kPing, DataDepth.Dimensions.nbPings, hw2);
    
    Depth = DataDepth.Depth(kPing,:);
    Mask  = DataDepth.Mask(kPing,:);
    
    for k2=1:nbRepeats
        subNaN = find(~isnan(Depth) & (Mask == 0));
        val = Depth(subNaN);
        [~, ~, subKO] = signal_spike_filter(val, 'Bathy', 'Fig', [], 'Threshold', Threshold, 'Interp', 0);
        subPoubelle = subNaN(subKO);
        
        if ~isempty(Fig) && (mod(kPing, StepDisplay) == 0)
            figure(Fig);
            subplot(nbRepeats, 1, k2); PlotUtils.createSScPlot(val, 'b'); grid; hold on; PlotUtils.createSScPlot(subKO, val(subKO), 'or'); hold off;
            drawnow
        end
        
        Depth(subPoubelle) = NaN;
        Mask(subPoubelle) = 1;
        DataDepth.Mask(kPing,subPoubelle) = 1;
    end
end
my_close(hw2, 'MsgEnd')

flag = existCacheNetcdf(this.nomFic);
if flag
    flag = save_signalNetcdf(this, 'Ssc_Depth', 'Mask', DataDepth.Mask(:,:));
else
    flag = write_depth_bin(this, DataDepth);
end
if ~flag
    return
end
