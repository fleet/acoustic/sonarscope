% Prelecture des fichiers .all : Nettoyage � partir de l'analyse histogrammique de Range
%
% Syntax
%   status = ALL_Spike2D(a)
%
% Input Arguments
%   a : Instances de cl_simradAll
%
% Output Arguments
%   status : 0 si arret par Cancel, 1 sinon
%
% Examples
%   nomFic = getNomFicDatabase('EM1002_0001_20040916_054013_raw.all')
%   [flag, aKM] = ALL.checkFiles(nomFic);
%   ALL_Spike2D(aKM);
%
%   nomDir = fileparts(nomFic)
%   listFileNames = listeFicOnDepthDir(nomDir, '.all')
%   [flag, aKM] = ALL.checkFiles(listFileNames);
%   ALL_Spike2D(aKM);
%
% See also cl_simrad_all Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function ALL_Spike2D(this, Seuil, SeuilRehabilitation, Marge)

ScreenSize = get(0, 'ScreenSize');
fig = figure('Position', centrageFig(950, ScreenSize(4)-150));
NbFic = length(this);
str1 = 'Nettoyage des fichiers .all';
str2 = 'Cleaning .all files';
hw = create_waitbar(Lang(str1,str2), 'N', NbFic);
for k=1:NbFic
    my_waitbar(k, NbFic, hw);
    disp(this(k).nomFic)
    
    ALL_Spike2Dunitaire(this(k), Seuil, SeuilRehabilitation, Marge, fig)
    
    %% On demande si on veut traiter les autres fichiers
    
    if k ~= NbFic
        flag = question_ContinueNextFile();
        if ~flag
            break
        end
    end
end
my_close(hw)
my_close(fig)


function ALL_Spike2Dunitaire(this, Seuil, SeuilRehabilitation, Marge, fig)

[~, nomFic] = fileparts(this.nomFic);

[flag, DataDepth] = read_depth_bin(this, 'Memmapfile', 0);
if ~flag
    return
end

DepthAllPings = DataDepth.Depth(:,:);

[flag, DataAttidude] = read_attitude_bin(this);
if flag
    HeaveAllPings = my_interp1(DataAttidude.Datetime, DataAttidude.Heave, DataDepth.Datetime);
else
    HeaveAllPings = zeros(DataDepth.Dimension.nbPings, 1);
end
DepthAllPings = DepthAllPings - HeaveAllPings;

MaskSubPings  = DataDepth.Mask(:,:);
DepthSubPings = DepthAllPings(:,:);

subNaN = (MaskSubPings ~= 0);
DepthSubPings(subNaN) = NaN;

% TODO : voir si ca pose des pbs de mettre 'OtherImages', 0
DepthMaskSubPings = filterSpike(DepthSubPings, Seuil, SeuilRehabilitation, Marge, [], []);

DepthMaskSubPings = DepthMaskSubPings(:,:,1);

%% Visualisation du r�sultat

figure(fig)
display_images(fig, nomFic, DepthSubPings, DepthMaskSubPings)

%% Validation et sauvegarde du r�sultat

str1 = 'Voulez-vous h�riter du filtrage de spike ?';
str2 = 'Do you want to apply the spike filter ?';
[OK, flag] = my_questdlg(Lang(str1,str2), 'Init', 2);
if ~flag
    return
end

if OK == 1
    subPoubelle = isnan(DepthMaskSubPings);
    MaskSubPings(~subPoubelle) = 0;
    MaskSubPings(subPoubelle)  = 1;
    
    DataDepth.Mask(:,:) = MaskSubPings;
    
    DepthSubPings(subPoubelle) = NaN;
    
    display_images(fig, nomFic, DepthSubPings, DepthMaskSubPings)
    
    flag = existCacheNetcdf(this.nomFic);
    if flag
        flag = save_signalNetcdf(this, 'Ssc_Depth', 'Mask', DataDepth.Mask(:,:));
    else
        flag = write_depth_bin(this, DataDepth);
    end
    if ~flag
        return
    end
end



function display_images(fig, nomFic, Depth, DepthMask)

figure(fig)

subplot(1,2,1)
imagesc(Depth); colorbar; axis xy; colormap(jet(256));
title([nomFic ' Depth'], 'interpreter', 'none')

subplot(1,2,2)
imagesc(DepthMask); colorbar; axis xy; colormap(jet(256));
title([nomFic ' Despiked Depth'], 'interpreter', 'none')
