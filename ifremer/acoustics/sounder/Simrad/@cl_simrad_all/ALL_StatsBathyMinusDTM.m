function flag = ALL_StatsBathyMinusDTM(this, MNT, CleanCurves)

MNT = Virtualmemory2Ram(MNT);

identBathymetry   = cl_image.indDataType('Bathymetry');
flag = testSignature(MNT, 'GeometryType', cl_image.indGeometryType('LatLong'), 'DataType', identBathymetry);
if ~flag
    return
end

% useParallel = get_UseParallelTbx;

N = length(this);
str1 = 'Nettoyage de la bathym�trie des fichiers .all par comparaison � un MNT de r�f�rence.';
str2 = 'Bathymetry cleaning of .all files by comparison to a DTM.';
% if useParallel && (N > 1) % Contreproductif pour donn�es mission QUOI
%     [hw, DQ] = create_parforWaitbar(N, Lang(str1,str2));
%     flag = 0;
%     k = N;
%     while flag == 0
%         [flag, X] = ALL_StatsBathyMinusDTM_unitaire(this(k), MNT);
%         if flag
%             CurvesA(k) = X; %#ok<AGROW>
%         end
%         N = k-1;
%     end
%     parfor (k=1:N, useParallel)
%         fprintf('%s\n%d / %d : %s\n', repmat('-',1,100), k, N, this(k).nomFic);
%         [flag, X] = ALL_StatsBathyMinusDTM_unitaire(this(k), MNT, 'isUsingParfor', true);
%         if flag
%             CurvesA(k) = X;
%         end
%         send(DQ, iFic);
%     end
%     my_close(hw, 'MsgEnd')
% else
    CurvesA = [];
    hw = create_waitbar(Lang(str1,str2), 'N', N);
    for k=1:N
        my_waitbar(k, N, hw)
        fprintf('%s\n%d / %d : %s\n', repmat('-',1,100), k, N, this(k).nomFic);
        
        [flag, X1, X2, X3, X4] = ALL_StatsBathyMinusDTM_unitaire(this(k), MNT);
        if flag
            if isempty(CurvesA)
                CurvesA = X1;
                CurvesB = X2;
                CurvesC = X3;
                CurvesD = X4;
            else
                CurvesA(end+1) = X1; %#ok<AGROW>
                CurvesB(end+1) = X2; %#ok<AGROW>
                CurvesC(end+1) = X3; %#ok<AGROW>
                CurvesD(end+1) = X4; %#ok<AGROW>
            end
        end
    end
    my_close(hw, 'MsgEnd')
% end
if isempty(CurvesA)
    return
end

cleanAndPlotCurves(CurvesA, CleanCurves, 'Bathy Minus DTM (m)')
cleanAndPlotCurves(CurvesB, CleanCurves, 'Bathy Minus DTM ,DetectionType (m)')
cleanAndPlotCurves(CurvesC, CleanCurves, 'Bathy Minus DTM percentage (%)')
cleanAndPlotCurves(CurvesD, CleanCurves, 'Bathy Minus DTM percentage,DetectionType (%)')



function cleanAndPlotCurves(CurvesA, CleanCurves, CurveName)

L = 1000; % 1200
H =  600; % 700

%% Affichage des courbes

[flag, FigCurvesA] = plot_CourbesStats(CurvesA, 'Stats', 0, 'Position', centrageFig(L, H), 'Titre', CurveName);
if ~flag
    return
end

%% Nettoyage des courbes

if CleanCurves
    Title = 'SignalDialog window : please clean up the curves';
    [flag, CurvesACleaned] = CourbesStatsClean(CurvesA, 'Stats', 0, 'NoPlot', 'Title', Title);
    if ~flag || isempty(CurvesACleaned)
        return
    end
else
    CurvesACleaned = CurvesA;
end

%% Plot des courbes nettoy�es

[flag, FigCurvesACleaned] = plot_CourbesStats(CurvesACleaned, 'Stats', 0, ...
    'Position', centrageFig(L, H), 'Titre', CurveName, 'Coul', [0.5 0.5 0.5]);
if ~flag
    return
end
close(FigCurvesA);

%% Moyennage des courbes

[flag, CurvesAMean] = CourbesStatsUnionWithHoles(CurvesACleaned, 'nbPMin', 10, 'nomCourbe', CurveName);
if ~flag
    return
end

%% Nettoyage de la courbe moyenn�e

if CleanCurves
    Title = 'SignalDialog window : please clean up the curve';
    [flag, CurvesAMeanCleaned] = CourbesStatsClean(CurvesAMean, 'Stats', 0, 'NoPlot', 'Title', Title);
    if ~flag
        return
    end
else
    CurvesAMeanCleaned = CurvesAMean;
end

%% Visualisation de la courbe moyenn�e

[~, FigCurvesACleaned] = plot_CourbesStats(CurvesAMeanCleaned, 'Stats', 0, 'Fig', FigCurvesACleaned, ...
    'Coul', 'b', 'LineWidth', 2, 'Position', centrageFig(L, H), 'Titre', [CurveName ' - B']);

[~, FigCurvesAMeanCleaned] = plot_CourbesStats(CurvesAMeanCleaned, 'Stats', 0, 'Fig', [], ...
    'LineWidth', 2, 'Position', centrageFig(L, H), 'Titre', [CurveName ' - A']);

[~, FigCurvesAMeanCleanedMeanPlusStd] = plot_CourbesStats(CurvesAMeanCleaned, 'Stats', 1, 'Fig', [], ...
    'LineWidth', 2, 'Position', centrageFig(L, H), 'Titre', [CurveName ' - C']);

for k=1:length(CurvesAMeanCleaned.bilan{1})
    CurvesAMeanCleaned.bilan{1}(k).nomZoneEtude = strrep(CurvesAMeanCleaned.bilan{1}(k).nomZoneEtude, 'Bias', 'Std');
end
[~, FigCurvesAMeanCleanedStd] = plot_CourbesStats(CurvesAMeanCleaned, 'Stats', 2, 'Fig', [], ...
    'LineWidth', 2, 'Position', centrageFig(L, H), 'Titre', ['Std of ' [CurveName ' - D']]);



function [flag, CurvesA, CurvesB, CurvesC, CurvesD] = ALL_StatsBathyMinusDTM_unitaire(this, MNT, varargin)

global isUsingParfor %#ok<GVMIS>

[varargin, isUsingParfor] = getPropertyValue(varargin, 'isUsingParfor', false); %#ok<ASGLU>

CurvesA = [];
CurvesB = [];
CurvesC = [];
CurvesD = [];

%% Tri des layers dans l'ordre : Bathy, xxx et yyy

Version = version_datagram(this);
switch Version
    case 'V1'
        a = get_Layer(this, {'Bathymetry'; 'AcrossDist'; 'AlongDist'; 'BeamPointingAngle',; 'DetectionType'}, 'CartoAuto', 1, 'TideCorrection', 1);
    case 'V2'
        a = get_Layer(this, {'Bathymetry'; 'AcrossDist'; 'AlongDist'; 'BeamPointingAngle',; 'DetectionType'}, 'CartoAuto', 1, 'TideCorrection', 1);
        % TODO : Cas de la V2 o� l'ordre des layers ramen�s ne respecte pas celui demand�
        a = a([3 1 2 5 4]);
end
Bathy_PingBeam = a(1);

%% Check if the tide is present

Tide = get(a(1), 'Tide');
if isempty(Tide) || all(isnan(Tide))
    str = sprintf('No tide for "%s"\n', this.nomFic);
    my_warndlg(str, 0, 'Tag', 'NoTideHere', 'displayItEvenIfSSc', 1, 'TimeDelay', 60);
end

%% Aspiration du MNT en g�om�trie PingBeam

[flag, DTM_PingBeam] = sonarMosaiqueInv(a, 1, MNT);
if ~flag
    return
end

%% Calcul de la diff�rence : bathy - MNT

Diff_DTM_PingBeam = DTM_PingBeam - Bathy_PingBeam;
Layers_PingBeam = [Diff_DTM_PingBeam a];

%% Calcul de la courbe angulaire de Z-MNT

nbPMin = 10; % TODO
Tag = [];
[nomDir, nomCourbe] = fileparts(this.nomFic);
Comment{1} = sprintf('Dir name : %s', nomDir);

Selection.Name     = nomCourbe;
Selection.TypeVal  = 'Layer';
Selection.indLayer = findIndLayerSonar(Layers_PingBeam, 1, 'strDataType', 'BeamPointingAngle');
Selection.DataType = cl_image.indDataType('BeamPointingAngle');
Selection.Unit     = 'deg';
        
X = courbesStats(Layers_PingBeam(1), Tag, ...
    nomCourbe, Comment{1}, [], ...
    Layers_PingBeam, Selection, 'stepBins', 1, ...
    'GeometryType', 'PingBeam', ...
    'Sampling', 1, 'Coul', [], 'nbPMin', nbPMin);
if isempty(X)
    flag = 0;
    return
end

CurvesA = X.CourbesStatistiques;

%% Calcul de la courbe angulaire  de Z-MNT en fonction de DetectionType

Selection(2).Name     = nomCourbe;
Selection(2).TypeVal  = 'Layer';
Selection(2).indLayer = findIndLayerSonar(Layers_PingBeam, 1, 'strDataType', 'DetectionType');
Selection(2).DataType = cl_image.indDataType('DetectionType');
Selection(2).Unit     = '';

X = courbesStats(Layers_PingBeam(1), Tag, ...
    nomCourbe, Comment{1}, [], ...
    Layers_PingBeam, Selection, 'stepBins', [1, 1], ...
    'GeometryType', 'PingBeam', ...
    'Sampling', 1, 'Coul', [], 'nbPMin', nbPMin);
if isempty(X)
    flag = 0;
    return
end
      
CurvesB = X.CourbesStatistiques;

%% Calcul de la diff�rence : bathy - MNT / MNT

c =  100 * (Diff_DTM_PingBeam) / abs(Bathy_PingBeam);
Layers_PingBeam = [c a];

%% Calcul de la courbe angulaire de Z-MNT / MNT

nbPMin = 10; % TODO
Tag = [];
[nomDir, nomCourbe] = fileparts(this.nomFic);
Comment{1} = sprintf('Dir name : %s', nomDir);

clear Selection
Selection.Name     = nomCourbe;
Selection.TypeVal  = 'Layer';
Selection.indLayer = findIndLayerSonar(Layers_PingBeam, 1, 'strDataType', 'BeamPointingAngle');
Selection.DataType = cl_image.indDataType('BeamPointingAngle');
Selection.Unit     = 'deg';
        
X = courbesStats(Layers_PingBeam(1), Tag, ...
    nomCourbe, Comment{1}, [], ...
    Layers_PingBeam, Selection, 'stepBins', 1, ...
    'GeometryType', 'PingBeam', ...
    'Sampling', 1, 'Coul', [], 'nbPMin', nbPMin);
if isempty(X)
    flag = 0;
    return
end

CurvesC = X.CourbesStatistiques;

%% Calcul de la courbe angulaire  de Z-MNT en fonction de DetectionType

Selection(2).Name     = nomCourbe;
Selection(2).TypeVal  = 'Layer';
Selection(2).indLayer = findIndLayerSonar(Layers_PingBeam, 1, 'strDataType', 'DetectionType');
Selection(2).DataType = cl_image.indDataType('DetectionType');
Selection(2).Unit     = '';

X = courbesStats(Layers_PingBeam(1), Tag, ...
    nomCourbe, Comment{1}, [], ...
    Layers_PingBeam, Selection, 'stepBins', [1, 1], ...
    'GeometryType', 'PingBeam', ...
    'Sampling', 1, 'Coul', [], 'nbPMin', nbPMin);
if isempty(X)
    flag = 0;
    return
end
      
CurvesD = X.CourbesStatistiques;
