function ALL_Summary_Datagrams(aKM, listDatagrams, nomFicSummary)

%% S�lection des items du r�sum�

if isempty(listDatagrams)
    listDatagrams{1}     = 'Depth';
    listDatagrams{end+1} = 'RawRangeBeamAngle';
    listDatagrams{end+1} = 'SeabedImage';
    listDatagrams{end+1} = 'Runtime';
    listDatagrams{end+1} = 'SurfaceSoundSpeed';
    listDatagrams{end+1} = 'SoundSpeedProfile';
    listDatagrams{end+1} = 'WaterColumn';
    listDatagrams{end+1} = 'InstallationParameters';
    listDatagrams{end+1} = 'Attitude';
    listDatagrams{end+1} = 'Clock';
    listDatagrams{end+1} = 'Position';
    listDatagrams{end+1} = 'Heading';
    listDatagrams{end+1} = 'Height';
    listDatagrams{end+1} = 'VerticalDepth';
    listDatagrams{end+1} = 'QualityFactor';
    listDatagrams{end+1} = 'StaveData';
    listDatagrams{end+1} = 'ExtraParameters';
end

%%

fid = fopen(nomFicSummary, 'w+t');
NbFic = length(aKM);
str1 = 'Cr�ation du r�summ� des datagrammes des .all';
str2 = 'Creating .all datagrams summary';
hw = create_waitbar(Lang(str1,str2), 'N', NbFic);
for iFic=1:NbFic
    my_waitbar(iFic, NbFic, hw);
    summary_Datagrams(aKM(iFic), fid, listDatagrams)
end
my_close(hw, 'MsgEnd');
if fid ~= 1
    fclose(fid);
end
try
    winopen(nomFicSummary)
catch %#ok<CTCH>
end
