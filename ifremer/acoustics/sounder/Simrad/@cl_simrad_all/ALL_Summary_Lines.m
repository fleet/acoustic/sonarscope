function [FigTable, nomFicConf, ShortSummary] = ALL_Summary_Lines(this, nomFicSummary, varargin)

[varargin, IndNavUnit] = getPropertyValue(varargin, 'IndNavUnit', []);
[varargin, listeItems] = getPropertyValue(varargin, 'listeItems', []);
[varargin, Openfile]   = getPropertyValue(varargin, 'Openfile',   []); %#ok<ASGLU>
FigTable     = [];
nomFicConf   = {};
ShortSummary = [];

useParallel = get_UseParallelTbx;
logFileId   = getLogFileId;

%% Comma or not comma ?

sep = getCSVseparator;

%% Cr�ation de la liste des items

strHeader = ALL.createListAllItems;

% indStrMode   = find(strcmp(strHeader, 'strMode'));
% indNbSwaths   = find(strcmp(strHeader, 'NbSwaths'));
% indFM   = find(strcmp(strHeader, 'FM'));
indNbPings      = find(strcmp(strHeader, 'NbPings'));
indDepthMin     = find(strcmp(strHeader, 'Min Depth (m)'));
indDepthMax     = find(strcmp(strHeader, 'Max Depth (m)'));
indTimeBegin    = find(strcmp(strHeader, 'Time beginning'));
indTimeEnd      = find(strcmp(strHeader, 'Time end'));
indTimeDuration = find(strcmp(strHeader, 'Duration'));
indEmModel      = find(strcmp(strHeader, 'EmModel'));
indSwath1       = find(strcmp(strHeader, 'Nb pings single swath'));
indSwath2       = find(strcmp(strHeader, 'Nb pings double swath'));
indFM0          = find(strcmp(strHeader, 'Nb pings without FM'));
indFM1          = find(strcmp(strHeader, 'Nb pings with FM'));
indLineLength   = find(strcmp(strHeader, 'Line length (m)'));
indLineCoverage = find(strcmp(strHeader, 'Line coverage (km2)'));

nbModes1 = 8; % TODO : attention pour EM2040C (EM2045 SHOM, ENSTA), Mode1 de 1 � 48 (fr�quence de 180 kHz � 3xx par pas de 10 kHz
for k1=nbModes1:-1:1
    str = sprintf('Nb pings Mode %d', k1);
    indNbPingsPerMode1(k1) = find(strcmp(strHeader, str));
end

indMode2IfEM2040 = find(strcmp(strHeader, 'Mode2 if EM2040'));
indMode3IfEM2040 = find(strcmp(strHeader, 'Mode3 if EM2040'));

%% S�lection des items

% [flag, listeItems] = ALL.Params.Summary_Lines(strHeader);
% if ~flag
%     return
% end
if isempty(listeItems)
    listeItems = 1:length(strHeader);
end
strHeader = strHeader(listeItems);

%% Cr�ation du fichier r�sum�

fid = fopen(nomFicSummary, 'w+t');
if fid == -1
    messageErreurFichier(nomFicSummary, 'WriteFailure');
    str1 = sprintf('"%s" semble �tre ouvert par un autre programme, fermez cette application et reessayer ce traitement..', nomFicSummary);
    str2 = sprintf('"%s" seems to be locked by another program, please close it and retry this operation.', nomFicSummary);
    my_warndlg(Lang(str1,str2), 1);
    return
end

%% Ecriture de l'ent�te

for k=1:length(strHeader)
    fprintf(fid, '%s%s', strHeader{k}, sep);
end
fprintf(fid, '\n');

%% Boucle de calcul sur les fichiers

NbFic   = length(this);
nbItems = length(listeItems);

% Mode  = zeros(1, NbFic);
nbPingsPerMode1 = zeros(nbModes1, NbFic);

Mode2IfEM2040 = zeros(1, NbFic);
Mode3IfEM2040 = zeros(1, NbFic);

nbPingsPerSwath     = zeros(2, NbFic);
nbPingsPerWaveForm  = zeros(2, NbFic);

strSummary  = cell(NbFic, nbItems);

SurveyDepthMin     =  Inf(1, NbFic);
SurveyDepthMax     = -Inf(1, NbFic);
SurveyTimeBegin    = datetime(0, 0, 0, 0, 0,  Inf(1, NbFic));
SurveyTimeEnd      = datetime(0, 0, 0, 0, 0, -Inf(1, NbFic));
SurveyNbPings      = zeros(1, NbFic);
SurveyDuration     = zeros(1, NbFic);
SurveyLineLength   = zeros(1, NbFic);
SurveyLineCoverage = zeros(1, NbFic);
SurveyEmModel      = cell(1, NbFic);

% tic
% TODO : tout �a est mal boutiqu�. Il faut sortir des tableaux de booleen de dimension [nbPings,1] pour chacun des modes, single/double swath, ... et faire un et logique pour savoir si il y a v�ritablement des pings � cette configuration
str1 = 'Cr�ation du r�sum� des profils';
str2 = 'Creating .all lines summary';
if useParallel && (NbFic > 1)
    [hw, DQ] = create_parforWaitbar(NbFic, Lang(str1,str2));
    logFileIdHere = logFileId;
    parfor (iFic=1:NbFic, useParallel)
%         fprintf('File %d/%d : %s\n', iFic, NbFic, this(iFic).nomFic);
        msg = sprintf('File %d/%d : %s', iFic, NbFic, this(iFic).nomFic);
        logFileIdHere.info('ALL_Summary_Lines', msg); %#ok<PFBNS>
        
        [flag, Values, strValues] = summary_Lines(this(iFic), 'IndNavUnit', IndNavUnit, ...
            'isUsingParfor', true, 'logFileId', logFileIdHere);
        if flag
            strValues = strValues(listeItems);
            strSummary(iFic,:) = strValues;
%             Mode(iFic)  = Values{indMode};
            for k1=1:nbModes1
                nbPingsPerMode1(k1,iFic) = Values{indNbPingsPerMode1(k1)}; %#ok<PFBNS>
            end
            
            Mode2IfEM2040(iFic) = Values{indMode2IfEM2040};
            Mode3IfEM2040(iFic) = Values{indMode3IfEM2040};
            
            nbPingsPerSwath(:,iFic)    = [Values{indSwath1} Values{indSwath2}];
            nbPingsPerWaveForm(:,iFic) = [Values{indFM0} Values{indFM1}];
            
            SurveyNbPings(iFic)      = Values{indNbPings};
            SurveyTimeBegin(iFic)    = Values{indTimeBegin};
            SurveyTimeEnd(iFic)      = Values{indTimeEnd};
            SurveyDepthMin(iFic)     = Values{indDepthMin};
            SurveyDepthMax(iFic)     = Values{indDepthMax};
            SurveyDuration(iFic)     = Values{indTimeDuration};
            SurveyEmModel{iFic}      = Values{indEmModel};
            SurveyDuration(iFic)     = Values{indTimeDuration};
            SurveyLineLength(iFic)   = Values{indLineLength};
            SurveyLineCoverage(iFic) = Values{indLineCoverage};
        end
        send(DQ, iFic);
    end
    my_close(hw, 'MsgEnd')
else
    hw = create_waitbar(Lang(str1,str2), 'N', NbFic);
    for iFic=1:NbFic
        my_waitbar(iFic, NbFic, hw);
%         fprintf('File %d/%d : %s\n', iFic, NbFic, this(iFic).nomFic);
        msg = sprintf('File %d/%d : %s', iFic, NbFic, this(iFic).nomFic);
        logFileId.info('ALL_Summary_Lines', msg);

        [flag, Values, strValues] = summary_Lines(this(iFic), 'IndNavUnit', IndNavUnit); %, 'isUsingParfor', true);
        if flag
            strValues = strValues(listeItems);
            strSummary(iFic,:) = strValues;

            for k1=1:nbModes1
                nbPingsPerMode1(k1,iFic) = Values{indNbPingsPerMode1(k1)};
            end
            
            Mode2IfEM2040(iFic) = Values{indMode2IfEM2040};
            Mode3IfEM2040(iFic) = Values{indMode3IfEM2040};

            nbPingsPerSwath(:,iFic)    = [Values{indSwath1} Values{indSwath2}];
            nbPingsPerWaveForm(:,iFic) = [Values{indFM0}    Values{indFM1}];
           
            SurveyNbPings(iFic)      = Values{indNbPings};
            SurveyTimeBegin(iFic)    = Values{indTimeBegin};
            SurveyTimeEnd(iFic)      = Values{indTimeEnd};
            SurveyDepthMin(iFic)     = Values{indDepthMin};
            SurveyDepthMax(iFic)     = Values{indDepthMax};
            SurveyDuration(iFic)     = Values{indTimeDuration};
            SurveyEmModel{iFic}      = Values{indEmModel};
            SurveyLineLength(iFic)   = Values{indLineLength};
            SurveyLineCoverage(iFic) = Values{indLineCoverage};
       end
    end
    my_close(hw, 'MsgEnd')
end
% toc

%% Ecriture du fichier

for iFic=1:NbFic
    for k=1:nbItems
        fprintf(fid, '%s%s', strSummary{iFic,k}, sep);
    end
    fprintf(fid, '\n');
end
fclose(fid);

%% Affichage des r�sultats dans une table

FigTable = figure;
t = uitable;
set(t, 'ColumnName', strHeader, 'Data', strSummary)
set(t, 'RearrangeableColumns', 'On')
set(t, 'Unit', 'normalized', 'Position', [0 0 1 1])

%% Affichage des r�sultats dans un �diteur
if Openfile
try
    winopen(nomFicSummary)
catch %#ok<CTCH>
end
end

%% Recherche des configurations

for k=length(this):-1:1
    listeNomFic{k} = this(k).nomFic;
end
listeConfigs = getListConfigurationsAll(SurveyEmModel, listeNomFic, nbPingsPerMode1, Mode2IfEM2040, Mode3IfEM2040, nbPingsPerSwath, nbPingsPerWaveForm);
[nomDir, nomFicCSV] = fileparts(nomFicSummary);
nomFicConf = createFileListsPerConfig(listeConfigs, nomDir, nomFicCSV);

%% Calcul de certains indicateurs

ShortSummary.SurveyNbPings      = sum(SurveyNbPings, 'omitnan');
ShortSummary.SurveyTimeBegin    = min(SurveyTimeBegin);
ShortSummary.SurveyTimeEnd      = max(SurveyTimeEnd);
ShortSummary.SurveyDepthMin     = min(SurveyDepthMin);
ShortSummary.SurveyDepthMax     = max(SurveyDepthMax);
ShortSummary.SurveyDuration     = sum(SurveyDuration, 'omitnan');
ShortSummary.SurveyLineLength   = sum(SurveyLineLength, 'omitnan');
ShortSummary.SurveyLineCoverage = sum(SurveyLineCoverage, 'omitnan');
ShortSummary.SurveySounderName  = SurveyEmModel{1};
ShortSummary.SurveySounderCie   = 'Kongsberg EM';


function nomFicConf = createFileListsPerConfig(listeConfigs, nomDir, nomFicCSV)

nomFicConf = {};
for k=1:length(listeConfigs)
    nomFicSeul = sprintf('%s_%s.txt', nomFicCSV, listeConfigs(k).Files.Conf);
    nomFicConf{k} = fullfile(nomDir, nomFicSeul); %#ok<AGROW>
    fid = fopen(nomFicConf{k}, 'w+t');
    if fid == -1
        messageErreurFichier(nomFicConf{k}, 'WriteFailure');
        return
    end
    for k4=1:length(listeConfigs(k).Files.index)
        fprintf(fid, '%s\n', listeConfigs(k).Files.str{k4});
    end
    fclose(fid);
end
