function flag = ALL_SurveyReport_Navigation(this, adocFileName, listSignals, varargin)

[varargin, flagPlotAttitude] = getPropertyValue(varargin, 'PlotAttitude', 1); % TODO : 0 plut�t. Voir avec Ridha
[varargin, IndNavUnit]       = getPropertyValue(varargin, 'IndNavUnit',   []);
[varargin, OpenAdoc]         = getPropertyValue(varargin, 'OpenAdoc',     1); %#ok<ASGLU>

dosNetUse('Init')

Tag = num2str(randi(100000));

N = length(this);
for k=N:-1:1
    listDataFiles{k} = this(k).nomFic;
end
[nomDirSummary, SurveyName] = fileparts(adocFileName);

%% Plot Attitude

if flagPlotAttitude
    for k=1:20:N
        sub = k:min(N,k+20-1);
        Fig = plot_attitude(this(sub));
        [nomFicFig, nomFicPng] = AdocUtils.exportFigAndPng(Fig, nomDirSummary, SurveyName, [Tag num2str(k, '-%03d')], 'Attitude');
        my_close(Fig);
        if k == 1
            SectionName = 'Attitude plots';
        else
            SectionName = [];
        end
        strTitle = sprintf('Attitude [%d-%d]', sub(1), sub(end));
        AdocUtils.addNavigation(adocFileName, nomFicFig, nomFicPng, strTitle, 'SectionName', SectionName, ...
            'listDataFiles', {this(sub).nomFic});
    end
end
    
%% Plot navigation

Fig = ALL_PlotNav(this, 'IndNavUnit', IndNavUnit);
[nomFicFig, nomFicPng] = AdocUtils.exportFigAndPng(Fig, nomDirSummary, SurveyName, Tag, 'Navigation');
nomFicKmz = fullfile(nomDirSummary, SurveyName, 'KMZ', [SurveyName '-Navigation' Tag '.kmz']);
plot_navigation_ExportGoogleEarth('NomFicKmz', nomFicKmz);
nomFicShp = fullfile(nomDirSummary, SurveyName, 'SHP', [SurveyName '-Navigation.shp']);
plot_navigation_ShapeFile('Fig', Fig, 'nomFicShp', nomFicShp, 'geometry', 2);
nomFicXml3DV = fullfile(nomDirSummary, SurveyName, '3DV', [SurveyName '-Navigation' Tag '.xml']);
plot_navigation_ExportXML('NomFicXml', nomFicXml3DV)
my_close(Fig);
AdocUtils.addNavigation(adocFileName, nomFicFig, nomFicPng, 'Navigation', 'SectionName', 'Navigation plots', ...
    'GoogleEarthFilename', nomFicKmz, 'Globe3DVFilename', nomFicXml3DV, 'listDataFiles', listDataFiles);
nomFicNavigationFig = nomFicFig;

%% Plot navigation & coverage

% Fig = ALL_PlotNav(this, 'Title', 'Position and coverage', 'IndNavUnit', IndNavUnit);
Fig = openfig(nomFicNavigationFig);
title('Position and coverage') ;
ALL_PlotCoverage(this, 'Fig', Fig, 'IndNavUnit', IndNavUnit);
[nomFicFig, nomFicPng] = AdocUtils.exportFigAndPng(Fig, nomDirSummary, SurveyName, Tag, 'NavigationCoverage');
nomFicKml = fullfile(nomDirSummary, SurveyName, 'KMZ', [SurveyName '-Coverage-' Tag '.kml']);
[SounderName, ShipName] = ALL.getInfoSounder(listDataFiles{1});
Comment = plot_navigation_RasterMask('Fig', Fig, 'nomFicKml', nomFicKml, 'SurveyName', SurveyName, ...
    'SounderName', SounderName, 'ShipName', ShipName, 'SounderCie', 'Kongsberg');
my_close(Fig);
AdocUtils.addNavigation(adocFileName, nomFicFig, nomFicPng, 'NavigationCoverage', 'GoogleEarthFilename', nomFicKml, 'Comment', Comment);

%% Plot des signaux

nbSignals = length(listSignals);
str1 = 'Trac� de la navigation et des signaux';
str2 = 'Plot Navigation and signals.';
hw = create_waitbar(Lang(str1,str2), 'N', nbSignals);
for k=1:nbSignals
    my_waitbar(k, nbSignals, hw)
%     Fig = ALL_PlotNav(this, 'IndNavUnit', IndNavUnit);
    Fig = openfig(nomFicNavigationFig);
    title(listSignals{k}) ;
    InfoSignal = plot_position_data(this, listSignals{k}, 'Fig', Fig, 'Mute', 1, 'IndNavUnit', IndNavUnit);
    suffix = strrep(listSignals{k}, '/', 'On'); % pour SwathWith/Depth
    [nomFicFig, nomFicPng] = AdocUtils.exportFigAndPng(Fig, nomDirSummary, SurveyName, Tag, ['Navigation-' suffix]);
    my_close(Fig);
    AdocUtils.addNavigation(adocFileName, nomFicFig, nomFicPng, ['Navigation and ' listSignals{k}], 'InfoSignal', InfoSignal);
end
my_close(hw)

%% Ouverture du fichier Adoc

if OpenAdoc
    AdocUtils.openAdoc(adocFileName);
end
dosNetUse('Clear')

flag = 1;
