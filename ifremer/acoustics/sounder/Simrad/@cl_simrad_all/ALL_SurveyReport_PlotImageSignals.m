function flag = ALL_SurveyReport_PlotImageSignals(this, nomFicAdoc, varargin)

[varargin, indexConfigs] = getPropertyValue(varargin, 'indexConfigs', 1:5); %#ok<ASGLU>

flag = 1;

N = numel(this);
str1 = 'Rapport Affichage d''Images et de Signaux : Fichiers';
str2 = 'Plot Image and Signals report : Files';
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
    my_waitbar(k, N, hw);
    flag = ALL_SurveyReport_PlotImageSignals_unitaire(this(k), nomFicAdoc, indexConfigs);
    if ~flag
        return
    end
end
my_close(hw, 'MsgEnd')

%% R�ouverture du fichier Adoc

AdocUtils.openAdoc(nomFicAdoc);


function flag = ALL_SurveyReport_PlotImageSignals_unitaire(this, nomFicAdoc, indexConfigs)

EmModel = get(this, 'EmModel');

%% Nom du fichier ADOC

[nomDirSummary, SurveyName] = fileparts(nomFicAdoc);
    
%% Loop on files

N = numel(indexConfigs);
str1 = 'Rapport Affichage d''Images et de Signaux : Configurations';
str2 = 'Plot Image and Signals report : Configurations';
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    
    %% Lecture de tous les layers qui vont �tre utiles
    
    [~, nomFicSeul, Ext] = fileparts(this.nomFic);
    if k == 1
        Title1 = ['Image and Signals '  nomFicSeul Ext];
    else
        Title1 = [];
    end
    
    %% Bathymetry

    switch indexConfigs(k)
        case 1 % 'Bathy and Attitude'
            Layer = get_Layer(this, 'Bathymetry', 'CartoAuto', 1);
            
        otherwise
            Layer = get_Layer(this, 'ReflectivityFromSnippets', 'CartoAuto', 1);
    end
    if isempty(Layer)
        continue
    end
   
    LineStyle = '-';
    Marker    = 'none';
    switch indexConfigs(k)
        case 1 % 'Bathy and Attitude'
            NameOfThePlot = 'BathyAndAttitude'; % TODO : am�liorer cela
            Title2 = 'Bathy and Attitude';
            listSignals = {'Roll'; 'Pitch'; 'Heave'; 'Heading'};
            
        case 2 % 'Reflectivity and Modes'
            NameOfThePlot = 'ReflectivityAndModes'; % TODO : am�liorer cela
            Title2 = 'Reflectivity and Modes';
            switch EmModel
                case {2040; 2045}
                    listSignals = {'Mode1'; 'Mode2'; 'Mode3'};
                otherwise
                    listSignals = {'Mode1'};
            end
            
        case 3 % 'Reflectivity and Specular'
            NameOfThePlot = 'ReflectivityAndSpecular'; % TODO : am�liorer cela
            Title2 = 'Reflectivity and Specular';
            listSignals = {'BSN'; 'BSO'; 'BSN-BSO'; 'TVGCrossOver'};
            
        case 4 % 'Reflectivity and Pulse length'
            NameOfThePlot = 'ReflectivityAndPulseLength'; % TODO : am�liorer cela
            Title2 = 'Reflectivity and Pulse length';
            listSignals = {'PulseLength_Total'; 'PulseLength_Nominal'; 'PulseLength_Effective'; 'PulseLength_UsedByKMinIA'};

        case 5 % 'Reflectivity and Absorption coefficient'
            NameOfThePlot = 'ReflectivityAndAbsorptionCoefficient'; % TODO : am�liorer cela
            Title2 = 'Reflectivity and Absorption coefficient';
            listSignals = {'AbsorptionCoeff_RT'};
            LineStyle = 'none';
            Marker    = '.';
    end
    
    [flag, nomFicFig, nomFicPng] = AdocFile_PlotImageSignalsExportFiles(Layer, listSignals, ...
        nomDirSummary, SurveyName, NameOfThePlot, nomFicSeul, LineStyle, Marker);
    if ~flag
        return
    end
    
    AdocUtils.addImageAndSignals(nomFicAdoc, nomFicFig, nomFicPng, ...
        'Title1', Title1, 'Title2', Title2); %, 'Intro', Intro);
end
my_close(hw, 'MsgEnd')



function [flag, nomFicFig, nomFicPng] = AdocFile_PlotImageSignalsExportFiles(this, listSignals, nomDirSummary, SurveyName, ...
    NameOfThePlot, nomFicSeul, LineStyle, Marker, varargin)

[varargin, CLim] = getPropertyValue(varargin, 'CLim', []); %#ok<ASGLU>

if isempty(CLim)
    CLim = '0.5%';
end
this.CLim = CLim;

Fig = FigUtils.createSScFigure('Position', centrageFig(1600, 800));

n = length(listSignals);
ha = subplot(n+1, 1, 1);
imagesc(this, 'Axe', ha, 'Rot90', 1, 'Location', 'East'); % 'LOCATION', 'EastOutside'

[~, Time] = getValSignalIfAny(this, 'Time');

for k=1:n
    ha(k+1) = subplot(n+1, 1, k+1);
    
    [flag, Signal, Unit, strLegend] = getValSignalIfAny(this, listSignals{k});
    if ~flag
        continue
    end
    
    if isempty(Signal)
        title(ha(k+1), [listSignals{k} ' - Empty']);
    else
        if strcmp(listSignals{k}, 'Time')
            Signal = datetime(Signal, 'ConvertFrom', 'datenum');
        end
        hc = PlotUtils.createSScPlot(ha(k+1), Signal, 'LineStyle', LineStyle, 'Marker', Marker); grid(ha(k+1), 'on');
        set(hc, 'UserData', Time)
        if isempty(Unit)
            Title = listSignals{k};
        else
            Title = sprintf('%s (%s)', listSignals{k}, Unit);
        end
        
        if ~isempty(strLegend)
            str = sprintf('%d=%s', 1, strLegend{1});
            for k2=2:length(strLegend)
                str = sprintf('%s, %d=%s', str, k2, strLegend{k2});
            end
            Title = sprintf('%s (%s)', Title, str);
        end
        
        title(ha(k+1), Title, 'Interpreter', 'none');
    end
end
linkaxes(ha, 'x');

% ImageName = this.Name;
FileName = NameOfThePlot;

nomDirPlotImageSignals = fullfile(nomDirSummary, SurveyName, 'PlotImageSignals');
if ~exist(nomDirPlotImageSignals, 'dir')
    mkdir(nomDirPlotImageSignals)
end

nomDirPlotImageSignals = fullfile(nomDirPlotImageSignals, nomFicSeul);
if ~exist(nomDirPlotImageSignals, 'dir')
    mkdir(nomDirPlotImageSignals)
end

nomFicFig = fullfile(nomDirPlotImageSignals, [FileName '.fig']);
savefig(Fig, nomFicFig)

nomFicPng = fullfile(nomDirPlotImageSignals, [FileName '.png']);
fig2fic(Fig, nomFicPng)
close(Fig);
