function ALL_SurveyReport_SonarEquation(this, nomFicAdoc, NomFicMNT, flagCreateCSV)

I0 = cl_image_I0;

%% Nom du fichier ADOC

[nomDirSummary, SurveyName] = fileparts(nomFicAdoc);

%% Lecture du MNT de r�f�rence

[flag, MNT] = import_RefDTM(NomFicMNT);
if ~flag
    return
end
MNT.CLim          = '0.5%';
MNT.ColormapIndex = 20;
gridSize = MNT.YStepMetric;

%% Calcul des layers : pentes, angle d'incidence, aire insonifi�e

ALL_IncidenceAngleFromOneDTM(this, MNT);

%% Reference DTM

[nomFicFig, nomFicPng, nomFicKmz, nomFicGeotif, DataType, TypeGeotif] = exportRefDTMFig2PngAndFigFiles(MNT, nomDirSummary, SurveyName);
AdocUtils.addReferenceDTM(nomFicAdoc, nomFicFig, nomFicPng, nomFicKmz, nomFicGeotif, DataType, TypeGeotif);

MNTSunShading = sunShadingGreatestSlope(MNT, 1);
[nomFicFig, nomFicPng, nomFicKmz, nomFicGeotif, DataType, TypeGeotif] = exportRefDTMFig2PngAndFigFiles(MNTSunShading, nomDirSummary, SurveyName);
AdocUtils.addReferenceDTM(nomFicAdoc, nomFicFig, nomFicPng, nomFicKmz, nomFicGeotif, DataType, TypeGeotif);
    
%% Loop on files

listNomFicGeotif = {};
N = numel(this);
str1 = 'Raport Equation Sonar';
str2 = 'Sonar equation report';
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    fprintf('\n-----------------------------------------------------------------------------------------------\n');
    fprintf('%d/%d : Processing SonarEquation for %s\n\n', k, N, this(k).nomFic);
    
    %% Lecture de tous les layers qui vont �tre utiles
    
    b = import_PingBeam_All(this(k));
    
    [~, nomFicSeul, Ext] = fileparts(this(k).nomFic);
    
    kReflecSSc       = findIndLayerSonar(b, 1, 'strDataType', 'Reflectivity',              'WithCurrentImage', 1,  'Tag',         'FromSnippets');
    kReflecKM        = findIndLayerSonar(b, 1, 'strDataType', 'Reflectivity',              'WithCurrentImage', 1, '~Tag',         'FromSnippets');
    kBathyRT         = findIndLayerSonar(b, 1, 'strDataType', 'Bathymetry',                'WithCurrentImage', 1, '~TagComments', 'FromDTM');
    kBathyDTM        = findIndLayerSonar(b, 1, 'strDataType', 'Bathymetry',                'WithCurrentImage', 1,  'TagComments', 'FromDTM');
    kTxBeamIndex     = findIndLayerSonar(b, 1, 'strDataType', 'TxBeamIndex',               'WithCurrentImage', 1);
    kAnglesEarth     = findIndLayerSonar(b, 1, 'strDataType', 'RxAngleEarth',              'WithCurrentImage', 1);
    kRange           = findIndLayerSonar(b, 1, 'strDataType', 'TwoWayTravelTimeInSeconds', 'WithCurrentImage', 1);
    kIncidenceAngles = findIndLayerSonar(b, 1, 'strDataType', 'IncidenceAngle',            'WithCurrentImage', 1);
    kSlopeAcross     = findIndLayerSonar(b, 1, 'strDataType', 'SlopeAcross',               'WithCurrentImage', 1);
    kSlopeAlong      = findIndLayerSonar(b, 1, 'strDataType', 'SlopeAlong',                'WithCurrentImage', 1);
    kAlphaRT         = findIndLayerSonar(b, 1, 'strDataType', 'AbsorptionCoeff',           'WithCurrentImage', 1,  'Tag', '_RT_');   
    kAlphaSSc        = findIndLayerSonar(b, 1, 'strDataType', 'AbsorptionCoeff',           'WithCurrentImage', 1,  'Tag', '_SSc_');   
    kIASSc           = findIndLayerSonar(b, 1, 'strDataType', 'InsonifiedAreadB',          'WithCurrentImage', 1);
    kKongsbergIBA    = findIndLayerSonar(b, 1, 'strDataType', 'KongsbergIBA',              'WithCurrentImage', 1);
    kBeamAngles      = findIndLayerSonar(b, 1, 'strDataType', {'BeamPointingAngle'; 'TxAngle'}, 'WithCurrentImage', 1);
     
    if isempty(kBathyDTM)
        str1 = sprintf('Le calcul des angles d''incidence ne semble pas avoir �t� fait pour le fichier "%s"', this(k).nomFic);
        str2 = sprintf('The incidence angles do not seem to be computed for "%s"', this(k).nomFic);
        my_warndlg(Lang(str1,str2), 1);
        continue
    end
    
    %% Calcul des coordonn�es g�ographiques
    
    [~, LatLong_PingBeam] = sonarCalculCoordGeo(b, kReflecSSc);
    
    %% Bathymetry
    
    Title3 = 'RT';
    Intro = 'This data is the bathymetry obtained from the XYZ datagrams (corrected of Heave).';
    b(kBathyRT) = update_Name(b(kBathyRT), 'Append', Title3);
    [nomFicFig, nomFicPng, nomFicImageTif, TypeImageTif_PingBeam, TypeImageTif_LatLong, nomFicHistoPng, ...
        nomFicLatLongFig, nomFicLatLongPng, nomFicKmz, nomFicGeotif] = AdocFile_SonarEquationExportFiles(...
        b(kBathyRT), LatLong_PingBeam, gridSize, ...
        nomDirSummary, SurveyName, nomFicSeul, 'FigHisto', 1);
    AdocUtils.addPingBeamAndLatLongImages(nomFicAdoc, nomFicFig, nomFicPng, nomFicImageTif, TypeImageTif_PingBeam, TypeImageTif_LatLong, nomFicHistoPng, ...
        nomFicLatLongFig, nomFicLatLongPng, nomFicKmz, nomFicGeotif, ...
        'Title1', ['Sonar Equation file '  nomFicSeul Ext], ...
        'Title2', 'Bathymetry', 'Title3', Title3, 'Intro', Intro);
    listNomFicGeotif{end+1} = nomFicGeotif; %#ok<AGROW>
    
    Title3 = 'DTM';
    Intro = 'This data is the bathymetry obtained from the DTM.';
    b(kBathyDTM) = update_Name(b(kBathyDTM), 'Append', Title3);
    [nomFicFig, nomFicPng, nomFicImageTif, TypeImageTif_PingBeam, TypeImageTif_LatLong, nomFicHistoPng, ...
        nomFicLatLongFig, nomFicLatLongPng, nomFicKmz, nomFicGeotif] = AdocFile_SonarEquationExportFiles(...
        b(kBathyDTM), LatLong_PingBeam, gridSize, ...
        nomDirSummary, SurveyName, nomFicSeul, 'FigHisto', 1);
    AdocUtils.addPingBeamAndLatLongImages(nomFicAdoc, nomFicFig, nomFicPng, nomFicImageTif, TypeImageTif_PingBeam, TypeImageTif_LatLong, nomFicHistoPng, ...
        nomFicLatLongFig, nomFicLatLongPng, nomFicKmz, nomFicGeotif, ...
        'Title3', Title3, 'Intro', Intro);
    listNomFicGeotif{end+1} = nomFicGeotif; %#ok<AGROW>
    
    Title3 = 'Residuals';
    Intro = 'This data is the difference between both bathymetries (DTM - RT)';
    Bathy_Residuals = b(kBathyDTM) - b(kBathyRT);
    Bathy_Residuals = update_Name(Bathy_Residuals, 'Append', Title3, 'regexprep', {'Minus'; ''});
    [nomFicFig, nomFicPng, nomFicImageTif, TypeImageTif_PingBeam, TypeImageTif_LatLong, nomFicHistoPng, ...
        nomFicLatLongFig, nomFicLatLongPng, nomFicKmz, nomFicGeotif] = AdocFile_SonarEquationExportFiles(...
        Bathy_Residuals, LatLong_PingBeam, gridSize, ...
        nomDirSummary, SurveyName, nomFicSeul, 'FigHisto', 1);
    AdocUtils.addPingBeamAndLatLongImages(nomFicAdoc, nomFicFig, nomFicPng, nomFicImageTif, TypeImageTif_PingBeam, TypeImageTif_LatLong, nomFicHistoPng, ...
        nomFicLatLongFig, nomFicLatLongPng, nomFicKmz, nomFicGeotif, ...
        'Title3', Title3, 'Intro', Intro);
    listNomFicGeotif{end+1} = nomFicGeotif; %#ok<AGROW>
    
    %% Slopes
    
    Title3 = 'Slope Across';
    Intro = 'This data is the across slope obtained from the DTM.';
    [nomFicFig, nomFicPng, nomFicImageTif, TypeImageTif_PingBeam, TypeImageTif_LatLong, nomFicHistoPng, ...
        nomFicLatLongFig, nomFicLatLongPng, nomFicKmz, nomFicGeotif] = AdocFile_SonarEquationExportFiles(...
        b(kSlopeAcross), LatLong_PingBeam, gridSize, ...
        nomDirSummary, SurveyName, nomFicSeul, 'FigHisto', 1);
    AdocUtils.addPingBeamAndLatLongImages(nomFicAdoc, nomFicFig, nomFicPng, nomFicImageTif, TypeImageTif_PingBeam, TypeImageTif_LatLong, nomFicHistoPng, ...
        nomFicLatLongFig, nomFicLatLongPng, nomFicKmz, nomFicGeotif, ...
        'Title2', 'Slopes', 'Title3', Title3, 'Intro', Intro);
    listNomFicGeotif{end+1} = nomFicGeotif; %#ok<AGROW>
    
    Title3 = 'Slope Along';
    Intro = 'This data is the along slope obtained from the DTM.';
    [nomFicFig, nomFicPng, nomFicImageTif, TypeImageTif_PingBeam, TypeImageTif_LatLong, nomFicHistoPng, ...
        nomFicLatLongFig, nomFicLatLongPng, nomFicKmz, nomFicGeotif] = AdocFile_SonarEquationExportFiles(...
        b(kSlopeAlong), LatLong_PingBeam, gridSize, ...
        nomDirSummary, SurveyName, nomFicSeul, 'FigHisto', 1);
    AdocUtils.addPingBeamAndLatLongImages(nomFicAdoc, nomFicFig, nomFicPng, nomFicImageTif, TypeImageTif_PingBeam, TypeImageTif_LatLong, nomFicHistoPng, ...
        nomFicLatLongFig, nomFicLatLongPng, nomFicKmz, nomFicGeotif, ...
        'Title3', Title3, 'Intro', Intro);
    listNomFicGeotif{end+1} = nomFicGeotif; %#ok<AGROW>
        
    %% Angles
    
    Title3 = 'Beam Pointing Angles';
    Intro = 'This data is the beam pointing angles vs the hull.';
    [nomFicFig, nomFicPng, nomFicImageTif, TypeImageTif_PingBeam, TypeImageTif_LatLong, nomFicHistoPng, ...
        nomFicLatLongFig, nomFicLatLongPng, nomFicKmz, nomFicGeotif] = AdocFile_SonarEquationExportFiles(...
        b(kBeamAngles), LatLong_PingBeam, gridSize, ...
        nomDirSummary, SurveyName, nomFicSeul, 'FigHisto', 1);
    AdocUtils.addPingBeamAndLatLongImages(nomFicAdoc, nomFicFig, nomFicPng, nomFicImageTif, TypeImageTif_PingBeam, TypeImageTif_LatLong, nomFicHistoPng, ...
        nomFicLatLongFig, nomFicLatLongPng, nomFicKmz, nomFicGeotif, ...
        'Title2', 'Angles', 'Title3', Title3, 'Intro', Intro);
    listNomFicGeotif{end+1} = nomFicGeotif; %#ok<AGROW>
    
    Title3 = 'Angles Earth';
    Intro = 'This data is the beam angles vs the nadir.';
    [nomFicFig, nomFicPng, nomFicImageTif, TypeImageTif_PingBeam, TypeImageTif_LatLong, nomFicHistoPng, ...
        nomFicLatLongFig, nomFicLatLongPng, nomFicKmz, nomFicGeotif] = AdocFile_SonarEquationExportFiles(...
        b(kAnglesEarth), LatLong_PingBeam, gridSize, ...
        nomDirSummary, SurveyName, nomFicSeul, 'FigHisto', 1);
    AdocUtils.addPingBeamAndLatLongImages(nomFicAdoc, nomFicFig, nomFicPng, nomFicImageTif, TypeImageTif_PingBeam, TypeImageTif_LatLong, nomFicHistoPng, ...
        nomFicLatLongFig, nomFicLatLongPng, nomFicKmz, nomFicGeotif, ...
        'Title3', Title3, 'Intro', Intro);
    listNomFicGeotif{end+1} = nomFicGeotif; %#ok<AGROW>
    
    Title3 = 'Incidence Angles';
    Intro = 'This data is the incidence angles.';
    b(kIncidenceAngles).ColormapIndex = 17;
    [nomFicFig, nomFicPng, nomFicImageTif, TypeImageTif_PingBeam, TypeImageTif_LatLong, nomFicHistoPng, ...
        nomFicLatLongFig, nomFicLatLongPng, nomFicKmz, nomFicGeotif] = AdocFile_SonarEquationExportFiles(...
        b(kIncidenceAngles), LatLong_PingBeam, gridSize, ...
        nomDirSummary, SurveyName, nomFicSeul, 'FigHisto', 1);
    AdocUtils.addPingBeamAndLatLongImages(nomFicAdoc, nomFicFig, nomFicPng, nomFicImageTif, TypeImageTif_PingBeam, TypeImageTif_LatLong, nomFicHistoPng, ...
        nomFicLatLongFig, nomFicLatLongPng, nomFicKmz, nomFicGeotif, ...
        'Title3', Title3, 'Intro', Intro);
    listNomFicGeotif{end+1} = nomFicGeotif; %#ok<AGROW>
    
    Title3 = 'Residuals';
    Intro = 'This data is the difference between angles (IncidenceAngles - AnglesEarth).';
    Angles_Residuals = b(kIncidenceAngles) - b(kAnglesEarth);
    Angles_Residuals = update_Name(Angles_Residuals, 'Append', Title3, 'regexprep', {'Minus'; ''});
    Angles_Residuals.ColormapIndex = 17;
    [nomFicFig, nomFicPng, nomFicImageTif, TypeImageTif_PingBeam, TypeImageTif_LatLong, nomFicHistoPng, ...
        nomFicLatLongFig, nomFicLatLongPng, nomFicKmz, nomFicGeotif] = AdocFile_SonarEquationExportFiles(...
        Angles_Residuals, LatLong_PingBeam, gridSize, ...
        nomDirSummary, SurveyName, nomFicSeul, 'FigHisto', 1);
    AdocUtils.addPingBeamAndLatLongImages(nomFicAdoc, nomFicFig, nomFicPng, nomFicImageTif, TypeImageTif_PingBeam, TypeImageTif_LatLong, nomFicHistoPng, ...
        nomFicLatLongFig, nomFicLatLongPng, nomFicKmz, nomFicGeotif, ...
        'Title3', Title3, 'Intro', Intro);
    listNomFicGeotif{end+1} = nomFicGeotif; %#ok<AGROW>
   
    %% Snell-Descartes
    
    if ~isempty(kKongsbergIBA)
        Title3 = 'SnellDescartes_RT';
        Intro = 'This data is the AngleSnellDescartes_RT (IBA).';
        b(kKongsbergIBA) = update_Name(b(kKongsbergIBA), 'Append', Title3);
        b(kKongsbergIBA).ColormapIndex = 3;
        [nomFicFig, nomFicPng, nomFicImageTif, TypeImageTif_PingBeam, TypeImageTif_LatLong, nomFicHistoPng, ...
            nomFicLatLongFig, nomFicLatLongPng, nomFicKmz, nomFicGeotif] = AdocFile_SonarEquationExportFiles(...
            b(kKongsbergIBA), LatLong_PingBeam, gridSize, ...
            nomDirSummary, SurveyName, nomFicSeul, 'FigHisto', 1);
        AdocUtils.addPingBeamAndLatLongImages(nomFicAdoc, nomFicFig, nomFicPng, nomFicImageTif, TypeImageTif_PingBeam, TypeImageTif_LatLong, nomFicHistoPng, ...
            nomFicLatLongFig, nomFicLatLongPng, nomFicKmz, nomFicGeotif, ...
            'Title3', Title3, 'Intro', Intro);
        listNomFicGeotif{end+1} = nomFicGeotif; %#ok<AGROW>
    end

    [~, AngleSnellDescartes] = SnellDescartes(b(kAnglesEarth), b(kBathyRT));
    AngleSnellDescartes = AngleSnellDescartes - b(kAnglesEarth);
    Title3 = 'SnellDescartes_SSc';
    Intro = 'This data is the AngleSnellDescartes_SSc.';
    AngleSnellDescartes = update_Name(AngleSnellDescartes, 'Append', Title3, 'regexprep', {'Minus'; ''});
    AngleSnellDescartes.ColormapIndex = 17;
    [nomFicFig, nomFicPng, nomFicImageTif, TypeImageTif_PingBeam, TypeImageTif_LatLong, nomFicHistoPng, ...
        nomFicLatLongFig, nomFicLatLongPng, nomFicKmz, nomFicGeotif] = AdocFile_SonarEquationExportFiles(...
        AngleSnellDescartes, LatLong_PingBeam, gridSize, ...
        nomDirSummary, SurveyName, nomFicSeul, 'FigHisto', 1);
    AdocUtils.addPingBeamAndLatLongImages(nomFicAdoc, nomFicFig, nomFicPng, nomFicImageTif, TypeImageTif_PingBeam, TypeImageTif_LatLong, nomFicHistoPng, ...
        nomFicLatLongFig, nomFicLatLongPng, nomFicKmz, nomFicGeotif, ...
        'Title3', Title3, 'Intro', Intro);
    listNomFicGeotif{end+1} = nomFicGeotif; %#ok<AGROW>

    if ~isempty(kKongsbergIBA)
        Title3 = 'SnellDescartes_Residuals';
        Intro = 'This data is the difference between angles (AngleSnellDescartes_SSc + AngleSnellDescartes_RT).';
        AngleSnellDescartes_Residuals = abs(AngleSnellDescartes) + b(kKongsbergIBA);
        AngleSnellDescartes_Residuals = update_Name(AngleSnellDescartes_Residuals, 'Append', Title3, 'regexprep', {'_SnellDescartes_SSc_abs_Plus'; ''});
        AngleSnellDescartes_Residuals.ColormapIndex = 3;
        [nomFicFig, nomFicPng, nomFicImageTif, TypeImageTif_PingBeam, TypeImageTif_LatLong, nomFicHistoPng, ...
            nomFicLatLongFig, nomFicLatLongPng, nomFicKmz, nomFicGeotif] = AdocFile_SonarEquationExportFiles(...
            AngleSnellDescartes_Residuals, LatLong_PingBeam, gridSize, ...
            nomDirSummary, SurveyName, nomFicSeul, 'FigHisto', 1);
        AdocUtils.addPingBeamAndLatLongImages(nomFicAdoc, nomFicFig, nomFicPng, nomFicImageTif, TypeImageTif_PingBeam, TypeImageTif_LatLong, nomFicHistoPng, ...
            nomFicLatLongFig, nomFicLatLongPng, nomFicKmz, nomFicGeotif, ...
            'Title3', Title3, 'Intro', Intro);
        listNomFicGeotif{end+1} = nomFicGeotif; %#ok<AGROW>
    end

    %% Absorption coefficient
    
    Title3 = 'RT';
    Intro = 'This data is the absorption coefficient used in real Time (RT).';
    [nomFicFig, nomFicPng, nomFicImageTif, TypeImageTif_PingBeam, TypeImageTif_LatLong, nomFicHistoPng, ...
        nomFicLatLongFig, nomFicLatLongPng, nomFicKmz, nomFicGeotif] = AdocFile_SonarEquationExportFiles(...
        b(kAlphaRT), LatLong_PingBeam, gridSize, ...
        nomDirSummary, SurveyName, nomFicSeul, 'FigHisto', 1);
    AdocUtils.addPingBeamAndLatLongImages(nomFicAdoc, nomFicFig, nomFicPng, nomFicImageTif, TypeImageTif_PingBeam, TypeImageTif_LatLong, nomFicHistoPng, ...
        nomFicLatLongFig, nomFicLatLongPng, nomFicKmz, nomFicGeotif, ...
        'Title2', 'Absorption coefficient', 'Title3', Title3, 'Intro', Intro);
    listNomFicGeotif{end+1} = nomFicGeotif; %#ok<AGROW>
    
    Title3 = 'SSc';
    Intro = 'This data is the absorption coefficient recomputed by SonarScope (SSc).';
    [nomFicFig, nomFicPng, nomFicImageTif, TypeImageTif_PingBeam, TypeImageTif_LatLong, nomFicHistoPng, ...
        nomFicLatLongFig, nomFicLatLongPng, nomFicKmz, nomFicGeotif] = AdocFile_SonarEquationExportFiles(...
        b(kAlphaSSc), LatLong_PingBeam, gridSize, ...
        nomDirSummary, SurveyName, nomFicSeul, 'FigHisto', 1);
    AdocUtils.addPingBeamAndLatLongImages(nomFicAdoc, nomFicFig, nomFicPng, nomFicImageTif, TypeImageTif_PingBeam, TypeImageTif_LatLong, nomFicHistoPng, ...
        nomFicLatLongFig, nomFicLatLongPng, nomFicKmz, nomFicGeotif, ...
        'Title3', Title3, 'Intro', Intro);
    listNomFicGeotif{end+1} = nomFicGeotif; %#ok<AGROW>

    Title3 = 'Residuals';
    Intro = 'This data is the difference between absorption coefficients (Alpha_SSc - Alpha_RT).';
    Alpha_Residuals = b(kAlphaSSc) - b(kAlphaRT);
    Alpha_Residuals = update_Name(Alpha_Residuals, 'Append', Title3, 'regexprep', {'Minus'; ''});
    [nomFicFig, nomFicPng, nomFicImageTif, TypeImageTif_PingBeam, TypeImageTif_LatLong, nomFicHistoPng, ...
        nomFicLatLongFig, nomFicLatLongPng, nomFicKmz, nomFicGeotif] = AdocFile_SonarEquationExportFiles(...
        Alpha_Residuals, LatLong_PingBeam, gridSize, ...
        nomDirSummary, SurveyName, nomFicSeul, 'FigHisto', 1);
    AdocUtils.addPingBeamAndLatLongImages(nomFicAdoc, nomFicFig, nomFicPng, nomFicImageTif, TypeImageTif_PingBeam, TypeImageTif_LatLong, nomFicHistoPng, ...
        nomFicLatLongFig, nomFicLatLongPng, nomFicKmz, nomFicGeotif, ...
        'Title3', Title3, 'Intro', Intro);
    listNomFicGeotif{end+1} = nomFicGeotif; %#ok<AGROW>

    %% TVG
    
    Title3 = 'RT';
    Intro = 'This data is the TVG used in real Time (RT).';
    [~, TVG_RT] = sonar_TVG_RT(b(kReflecSSc), 'Range', b(kRange), 'AbsorptionCoeffRT', b(kAlphaRT));
    TVG_RT = update_Name(TVG_RT, 'Append', Title3, 'regexprep', {'_FromSnippets_RT'; ''});
    [nomFicFig, nomFicPng, nomFicImageTif, TypeImageTif_PingBeam, TypeImageTif_LatLong, nomFicHistoPng, ...
        nomFicLatLongFig, nomFicLatLongPng, nomFicKmz, nomFicGeotif] = AdocFile_SonarEquationExportFiles(...
        TVG_RT, LatLong_PingBeam, gridSize, ...
        nomDirSummary, SurveyName, nomFicSeul, 'FigHisto', 1);
    AdocUtils.addPingBeamAndLatLongImages(nomFicAdoc, nomFicFig, nomFicPng, nomFicImageTif, TypeImageTif_PingBeam, TypeImageTif_LatLong, nomFicHistoPng, ...
        nomFicLatLongFig, nomFicLatLongPng, nomFicKmz, nomFicGeotif, ...
        'Title2', 'TVG', 'Title3', Title3, 'Intro', Intro);
    listNomFicGeotif{end+1} = nomFicGeotif; %#ok<AGROW>
    
    Title3 = 'SSc';
    Intro = 'This data is the TVG recomputed by SonarScope (SSc).';
    [~, TVG_SSc] = sonar_TVG_SSc(b(kReflecSSc), 'Range', b(kRange), 'AbsorptionCoeffSSc', b(kAlphaSSc));
    TVG_SSc = update_Name(TVG_SSc, 'Append', Title3, 'regexprep', {'_FromSnippets_SSc'; ''});
    [nomFicFig, nomFicPng, nomFicImageTif, TypeImageTif_PingBeam, TypeImageTif_LatLong, nomFicHistoPng, ...
        nomFicLatLongFig, nomFicLatLongPng, nomFicKmz, nomFicGeotif] = AdocFile_SonarEquationExportFiles(...
        TVG_SSc, LatLong_PingBeam, gridSize, ...
        nomDirSummary, SurveyName, nomFicSeul, 'FigHisto', 1);
    AdocUtils.addPingBeamAndLatLongImages(nomFicAdoc, nomFicFig, nomFicPng, nomFicImageTif, TypeImageTif_PingBeam, TypeImageTif_LatLong, nomFicHistoPng, ...
        nomFicLatLongFig, nomFicLatLongPng, nomFicKmz, nomFicGeotif, ...
        'Title3', Title3, 'Intro', Intro);
    listNomFicGeotif{end+1} = nomFicGeotif; %#ok<AGROW>

    Title3 = 'Residuals';
    Intro = 'This data is the difference between both TVG (TVG_SSc - TVG_RT).';
    TVG_Residuals = TVG_SSc - TVG_RT;
    TVG_Residuals = update_Name(TVG_Residuals, 'Append', Title3, 'regexprep', {'Minus'; ''});
    [nomFicFig, nomFicPng, nomFicImageTif, TypeImageTif_PingBeam, TypeImageTif_LatLong, nomFicHistoPng, ...
        nomFicLatLongFig, nomFicLatLongPng, nomFicKmz, nomFicGeotif] = AdocFile_SonarEquationExportFiles(...
        TVG_Residuals, LatLong_PingBeam, gridSize, ...
        nomDirSummary, SurveyName, nomFicSeul, 'FigHisto', 1);
    AdocUtils.addPingBeamAndLatLongImages(nomFicAdoc, nomFicFig, nomFicPng, nomFicImageTif, TypeImageTif_PingBeam, TypeImageTif_LatLong, nomFicHistoPng, ...
        nomFicLatLongFig, nomFicLatLongPng, nomFicKmz, nomFicGeotif, ...
        'Title3', Title3, 'Intro', Intro);
    listNomFicGeotif{end+1} = nomFicGeotif; %#ok<AGROW>
    
    %% Insonified Area
    
    Title3 = 'RT';
    Intro = 'This data is the Insonified Area compensation used in real Time (RT).';
    [~, IA_RT] = sonar_InsonifiedArea_KM(b(kReflecSSc), b(kRange), b(kBeamAngles), 'TxBeamIndex', b(kTxBeamIndex));
    IA_RT = update_Name(IA_RT, 'Append', Title3, 'regexprep', {'_FromSnippets_Factory'; ''});
    [nomFicFig, nomFicPng, nomFicImageTif, TypeImageTif_PingBeam, TypeImageTif_LatLong, nomFicHistoPng, ...
        nomFicLatLongFig, nomFicLatLongPng, nomFicKmz, nomFicGeotif] = AdocFile_SonarEquationExportFiles(...
        IA_RT, LatLong_PingBeam, gridSize, ...
        nomDirSummary, SurveyName, nomFicSeul, 'FigHisto', 1);
    AdocUtils.addPingBeamAndLatLongImages(nomFicAdoc, nomFicFig, nomFicPng, nomFicImageTif, TypeImageTif_PingBeam, TypeImageTif_LatLong, nomFicHistoPng, ...
        nomFicLatLongFig, nomFicLatLongPng, nomFicKmz, nomFicGeotif, ...
        'Title2', 'Insonified Area', 'Title3', Title3, 'Intro', Intro);
    listNomFicGeotif{end+1} = nomFicGeotif; %#ok<AGROW>
    
    Title3 = 'SSc';
    Intro = 'This data is the Insonified Area recomputed by SonarScope (SSc).';
    IA_SSc = b(kIASSc);
    IA_SSc = update_Name(IA_SSc, 'Append', Title3);
    [nomFicFig, nomFicPng, nomFicImageTif, TypeImageTif_PingBeam, TypeImageTif_LatLong, nomFicHistoPng, ...
        nomFicLatLongFig, nomFicLatLongPng, nomFicKmz, nomFicGeotif] = AdocFile_SonarEquationExportFiles(...
        IA_SSc, LatLong_PingBeam, gridSize, ...
        nomDirSummary, SurveyName, nomFicSeul, 'FigHisto', 1);
    AdocUtils.addPingBeamAndLatLongImages(nomFicAdoc, nomFicFig, nomFicPng, nomFicImageTif, TypeImageTif_PingBeam, TypeImageTif_LatLong, nomFicHistoPng, ...
        nomFicLatLongFig, nomFicLatLongPng, nomFicKmz, nomFicGeotif, ...
        'Title3', Title3, 'Intro', Intro);
    listNomFicGeotif{end+1} = nomFicGeotif; %#ok<AGROW>

    Title3 = 'Residuals';
    Intro = 'This data is the difference between both Insonified Area compensations (IA_SSc - IA_RT).';
    IA_Residuals = IA_SSc - IA_RT;
    IA_Residuals = update_Name(IA_Residuals, 'Append', Title3, 'regexprep', {'SSc_Minus'; ''});
    [nomFicFig, nomFicPng, nomFicImageTif, TypeImageTif_PingBeam, TypeImageTif_LatLong, nomFicHistoPng, ...
        nomFicLatLongFig, nomFicLatLongPng, nomFicKmz, nomFicGeotif] = AdocFile_SonarEquationExportFiles(...
        IA_Residuals, LatLong_PingBeam, gridSize, ...
        nomDirSummary, SurveyName, nomFicSeul, 'FigHisto', 1);
    AdocUtils.addPingBeamAndLatLongImages(nomFicAdoc, nomFicFig, nomFicPng, nomFicImageTif, TypeImageTif_PingBeam, TypeImageTif_LatLong, nomFicHistoPng, ...
        nomFicLatLongFig, nomFicLatLongPng, nomFicKmz, nomFicGeotif, ...
        'Title3', Title3, 'Intro', Intro);
    listNomFicGeotif{end+1} = nomFicGeotif; %#ok<AGROW>
    
    %% Lambert correction
    
    Title3 = 'Lambert_RT';
    Intro = 'This data is the Lambert compensation used in real Time (RT).';
    [~, Lambert_RT] = sonar_Lambert_KM(b(kReflecSSc), b(kRange));
    Lambert_RT = update_Name(Lambert_RT, 'Append', Title3, 'regexprep', {'_FromSnippets_LambertKM'; ''});
    [nomFicFig, nomFicPng, nomFicImageTif, TypeImageTif_PingBeam, TypeImageTif_LatLong, nomFicHistoPng, ...
        nomFicLatLongFig, nomFicLatLongPng, nomFicKmz, nomFicGeotif] = AdocFile_SonarEquationExportFiles(...
        Lambert_RT, LatLong_PingBeam, gridSize, ...
        nomDirSummary, SurveyName, nomFicSeul, 'FigHisto', 1);
    AdocUtils.addPingBeamAndLatLongImages(nomFicAdoc, nomFicFig, nomFicPng, nomFicImageTif, TypeImageTif_PingBeam, TypeImageTif_LatLong, nomFicHistoPng, ...
        nomFicLatLongFig, nomFicLatLongPng, nomFicKmz, nomFicGeotif, ...
        'Title2', 'Lambert correction', 'Title3', Title3, 'Intro', Intro);
    listNomFicGeotif{end+1} = nomFicGeotif; %#ok<AGROW>
    
    Title3 = 'Lambert_SSc';
    Intro = AdocUtils.importantMessage('This data is the Lambert compensation that *could be used* to replace the RT one (SSc).', ...
        'This compensation is not applied by default, it is done just if required on demand.');
    [~, Lambert_SSc] = sonar_BelleImage_Lambert(b(kReflecSSc), 1, [], 3, b(kBathyRT), b(kAnglesEarth));
    Lambert_SSc = update_Name(Lambert_SSc, 'Append', Title3, 'regexprep', {'_FromSnippets_AngEarth'; ''});
    [nomFicFig, nomFicPng, nomFicImageTif, TypeImageTif_PingBeam, TypeImageTif_LatLong, nomFicHistoPng, ...
        nomFicLatLongFig, nomFicLatLongPng, nomFicKmz, nomFicGeotif] = AdocFile_SonarEquationExportFiles(...
        Lambert_SSc, LatLong_PingBeam, gridSize, ...
        nomDirSummary, SurveyName, nomFicSeul, 'FigHisto', 1);
    AdocUtils.addPingBeamAndLatLongImages(nomFicAdoc, nomFicFig, nomFicPng, nomFicImageTif, TypeImageTif_PingBeam, TypeImageTif_LatLong, nomFicHistoPng, ...
        nomFicLatLongFig, nomFicLatLongPng, nomFicKmz, nomFicGeotif, ...
        'Title3', Title3, 'Intro', Intro);
    listNomFicGeotif{end+1} = nomFicGeotif; %#ok<AGROW>

    Title3 = 'Lambert_Residuals';
    Intro = 'This data is the difference between both Lambert compensations (Lambert_SSc - Lambert_RT).';
    Lambert_Residuals = Lambert_SSc - Lambert_RT;
    Lambert_Residuals = update_Name(Lambert_Residuals, 'Append', Title3, 'regexprep', {'_Lambert_SSc_Minus'; ''});
    [nomFicFig, nomFicPng, nomFicImageTif, TypeImageTif_PingBeam, TypeImageTif_LatLong, nomFicHistoPng, ...
        nomFicLatLongFig, nomFicLatLongPng, nomFicKmz, nomFicGeotif] = AdocFile_SonarEquationExportFiles(.../
        Lambert_Residuals, LatLong_PingBeam, gridSize, ...
        nomDirSummary, SurveyName, nomFicSeul, 'FigHisto', 1);
    AdocUtils.addPingBeamAndLatLongImages(nomFicAdoc, nomFicFig, nomFicPng, nomFicImageTif, TypeImageTif_PingBeam, TypeImageTif_LatLong, nomFicHistoPng, ...
        nomFicLatLongFig, nomFicLatLongPng, nomFicKmz, nomFicGeotif, ...
        'Title3', Title3, 'Intro', Intro);
    listNomFicGeotif{end+1} = nomFicGeotif; %#ok<AGROW>
    
    %% R�flectivity
    
    % ReflecKM
    Title3 = 'KM';
    Intro = AdocUtils.importantMessage('This data is reflectivity given by beam by Kongsberg Maritime(KM).', ...
        'No Lambert correction is applied on this data.');
    b(kReflecKM) = update_Name(b(kReflecKM), 'Append', Title3);
    [nomFicFig, nomFicPng, nomFicImageTif, TypeImageTif_PingBeam, TypeImageTif_LatLong, nomFicHistoPng, ...
        nomFicLatLongFig, nomFicLatLongPng, nomFicKmz, nomFicGeotif] = AdocFile_SonarEquationExportFiles(...
        b(kReflecKM), LatLong_PingBeam, gridSize, ...
        nomDirSummary, SurveyName, nomFicSeul, 'FigHisto', 1);
    AdocUtils.addPingBeamAndLatLongImages(nomFicAdoc, nomFicFig, nomFicPng, nomFicImageTif, TypeImageTif_PingBeam, TypeImageTif_LatLong, nomFicHistoPng, ...
        nomFicLatLongFig, nomFicLatLongPng, nomFicKmz, nomFicGeotif, ...
        'Title2', 'Reflectivity', 'Title3', Title3, 'Intro', Intro);
    AdocUtils.addBackscatterStatus(nomFicAdoc, 'Backscatter status of ReflecKM', b(kReflecKM));
    listNomFicGeotif{end+1} = nomFicGeotif; %#ok<AGROW>
    
    % ReflecSSc
    Title3 = 'SSc';
    Intro = AdocUtils.importantMessage('This data is reflectivity by beam recomputed by SonarScope(SSc). It is the mean value of the seabed images samples.', ...
        'The KM Lambert correction is applied on this data.');
    b(kReflecSSc) = update_Name(b(kReflecSSc), 'Append', Title3, 'regexprep', {'_FromSnippets'; ''});
    [nomFicFig, nomFicPng, nomFicImageTif, TypeImageTif_PingBeam, TypeImageTif_LatLong, nomFicHistoPng, ...
        nomFicLatLongFig, nomFicLatLongPng, nomFicKmz, nomFicGeotif] = AdocFile_SonarEquationExportFiles(...
        b(kReflecSSc), LatLong_PingBeam, gridSize, ...
        nomDirSummary, SurveyName, nomFicSeul, 'FigHisto', 1);
    AdocUtils.addPingBeamAndLatLongImages(nomFicAdoc, nomFicFig, nomFicPng, nomFicImageTif, TypeImageTif_PingBeam, TypeImageTif_LatLong, nomFicHistoPng, ...
        nomFicLatLongFig, nomFicLatLongPng, nomFicKmz, nomFicGeotif, ...
        'Title3', Title3, 'Intro', Intro);
    AdocUtils.addBackscatterStatus(nomFicAdoc, 'Backscatter status of ReflecSSc', b(kReflecSSc));
    listNomFicGeotif{end+1} = nomFicGeotif; %#ok<AGROW>

    % ReflecSScWithoutLambert
    Title3 = 'SSc Without Lambert';
    [~, ReflecSScWithoutLambert] = SonarBSCompensNone(b, kReflecSSc);
    ReflecSScWithoutLambert = update_Name(ReflecSScWithoutLambert, 'Append', Title3, 'regexprep', {'_SSc_BSCompensNo'; ''});
    Intro = AdocUtils.importantMessage('This data is ReflecSSc *without* the Lambert compensation.', ...
        'The KM Lambert correction has been removed.');
    [nomFicFig, nomFicPng, nomFicImageTif, TypeImageTif_PingBeam, TypeImageTif_LatLong, nomFicHistoPng, ...
        nomFicLatLongFig, nomFicLatLongPng, nomFicKmz, nomFicGeotif] = AdocFile_SonarEquationExportFiles(...
        ReflecSScWithoutLambert, LatLong_PingBeam, gridSize, ...
        nomDirSummary, SurveyName, nomFicSeul, 'FigHisto', 1);
    AdocUtils.addPingBeamAndLatLongImages(nomFicAdoc, nomFicFig, nomFicPng, nomFicImageTif, TypeImageTif_PingBeam, TypeImageTif_LatLong, nomFicHistoPng, ...
        nomFicLatLongFig, nomFicLatLongPng, nomFicKmz, nomFicGeotif, ...
        'Title3', Title3, 'Intro', Intro);
    AdocUtils.addBackscatterStatus(nomFicAdoc, 'Backscatter status of ReflecWithoutLambert', ReflecSScWithoutLambert);
    listNomFicGeotif{end+1} = nomFicGeotif; %#ok<AGROW>

    % Reflectivity_Residuals
    Title3 = 'Residuals';
    Intro = 'This data is the difference between SSc and KM reflectivities per beam (ReflecSScWithoutLambert - ReflecKM).';
    
    [~, flagLambertComp] = isBSLambertCompensated(b(kReflecKM));
    if flagLambertComp
        Reflectivity_Residuals = b(kReflecSSc) - b(kReflecKM);
    else
        Reflectivity_Residuals = ReflecSScWithoutLambert - b(kReflecKM);
    end
    
    Reflectivity_Residuals = update_Name(Reflectivity_Residuals, 'Append', Title3, 'regexprep', {'_Minus'; ''});
    [nomFicFig, nomFicPng, nomFicImageTif, TypeImageTif_PingBeam, TypeImageTif_LatLong, nomFicHistoPng, ...
        nomFicLatLongFig, nomFicLatLongPng, nomFicKmz, nomFicGeotif] = AdocFile_SonarEquationExportFiles(...
        Reflectivity_Residuals, LatLong_PingBeam, gridSize, ...
        nomDirSummary, SurveyName, nomFicSeul, 'FigHisto', 1);
    AdocUtils.addPingBeamAndLatLongImages(nomFicAdoc, nomFicFig, nomFicPng, nomFicImageTif, TypeImageTif_PingBeam, TypeImageTif_LatLong, nomFicHistoPng, ...
        nomFicLatLongFig, nomFicLatLongPng, nomFicKmz, nomFicGeotif, ...
        'Title3', Title3, 'Intro', Intro);
    listNomFicGeotif{end+1} = nomFicGeotif; %#ok<AGROW>
   
    % ReflecSScStep1
    [~, ReflecSScStep1] = SonarBestBSConstructeur(b, kReflecSSc, 'Range', b(kRange));
    Title3 = 'SScStep1';
%     Intro = 'Look at backscatter status to understand this reflectivity.';
    Intro = AdocUtils.importantMessage('Look at backscatter status to understand this reflectivity.', 'Tx Beam diagrams are not set to SSc status here because this would imply subsequent work for other sw to reproduce what is done in SSc.');
%     ReflecSScStep1 = update_Name(ReflecSScStep1, 'Append', Title3);
    [nomFicFig, nomFicPng, nomFicImageTif, TypeImageTif_PingBeam, TypeImageTif_LatLong, nomFicHistoPng, ...
        nomFicLatLongFig, nomFicLatLongPng, nomFicKmz, nomFicGeotif] = AdocFile_SonarEquationExportFiles(...
        ReflecSScStep1, LatLong_PingBeam, gridSize, ...
        nomDirSummary, SurveyName, nomFicSeul, 'FigHisto', 1);
    AdocUtils.addPingBeamAndLatLongImages(nomFicAdoc, nomFicFig, nomFicPng, nomFicImageTif, TypeImageTif_PingBeam, TypeImageTif_LatLong, nomFicHistoPng, ...
        nomFicLatLongFig, nomFicLatLongPng, nomFicKmz, nomFicGeotif, ...
        'Title3', Title3, 'Intro', Intro);
    AdocUtils.addBackscatterStatus(nomFicAdoc, 'Backscatter status of ReflecSScStep1', ReflecSScStep1);
    listNomFicGeotif{end+1} = nomFicGeotif; %#ok<AGROW>

    % Reflectivity_Residuals2
    Title3 = 'Residuals2';
    Intro = 'This data is the difference between ReflecSScStep1 and ReflecSScWithoutLambert (ReflecSScStep1 - ReflecSScWithoutLambert).';
    Reflectivity_Residuals2 = ReflecSScStep1 - ReflecSScWithoutLambert;
    Reflectivity_Residuals2.CLim = '0.5%';
    Reflectivity_Residuals2.ColormapIndex = 3;
    Reflectivity_Residuals2 = update_Name(Reflectivity_Residuals2, 'Append', Title3, 'regexprep', {'_Minus'; ''});
    [nomFicFig, nomFicPng, nomFicImageTif, TypeImageTif_PingBeam, TypeImageTif_LatLong, nomFicHistoPng, ...
        nomFicLatLongFig, nomFicLatLongPng, nomFicKmz, nomFicGeotif] = AdocFile_SonarEquationExportFiles(...
        Reflectivity_Residuals2, LatLong_PingBeam, gridSize, ...
        nomDirSummary, SurveyName, nomFicSeul, 'FigHisto', 1);
    AdocUtils.addPingBeamAndLatLongImages(nomFicAdoc, nomFicFig, nomFicPng, nomFicImageTif, TypeImageTif_PingBeam, TypeImageTif_LatLong, nomFicHistoPng, ...
        nomFicLatLongFig, nomFicLatLongPng, nomFicKmz, nomFicGeotif, ...
        'Title3', Title3, 'Intro', Intro);
    listNomFicGeotif{end+1} = nomFicGeotif; %#ok<AGROW>
        
    %% Geographic coordinates
        
    Title3 = 'Latitude';
    Intro = 'This data is the Latitude.';
    LatLong_PingBeam(1) = update_Name(LatLong_PingBeam(1), 'Append', Title3, 'regexprep', {'FromSnippets'; ''});
    [nomFicFig, nomFicPng, nomFicImageTif, TypeImageTif_PingBeam, TypeImageTif_LatLong, nomFicHistoPng, ...
        nomFicLatLongFig, nomFicLatLongPng, nomFicKmz, nomFicGeotif] = AdocFile_SonarEquationExportFiles(...
        LatLong_PingBeam(1), LatLong_PingBeam, gridSize, ...
        nomDirSummary, SurveyName, nomFicSeul, 'FigHisto', 1);
    AdocUtils.addPingBeamAndLatLongImages(nomFicAdoc, nomFicFig, nomFicPng, nomFicImageTif, TypeImageTif_PingBeam, TypeImageTif_LatLong, nomFicHistoPng, ...
        nomFicLatLongFig, nomFicLatLongPng, nomFicKmz, nomFicGeotif, ...
        'Title2', 'Geographic coordinates', 'Title3', Title3, 'Intro', Intro);
    listNomFicGeotif{end+1} = nomFicGeotif; %#ok<AGROW>
    
    Title3 = 'Longitude';
    Intro = 'This data is the Longitude.';
    LatLong_PingBeam(2) = update_Name(LatLong_PingBeam(2), 'Append', Title3, 'regexprep', {'FromSnippets'; ''});
    [nomFicFig, nomFicPng, nomFicImageTif, TypeImageTif_PingBeam, TypeImageTif_LatLong, nomFicHistoPng, ...
        nomFicLatLongFig, nomFicLatLongPng, nomFicKmz, nomFicGeotif] = AdocFile_SonarEquationExportFiles(...
        LatLong_PingBeam(2), LatLong_PingBeam, gridSize, ...
        nomDirSummary, SurveyName, nomFicSeul, 'FigHisto', 1);
    AdocUtils.addPingBeamAndLatLongImages(nomFicAdoc, nomFicFig, nomFicPng, nomFicImageTif, TypeImageTif_PingBeam, TypeImageTif_LatLong, nomFicHistoPng, ...
        nomFicLatLongFig, nomFicLatLongPng, nomFicKmz, nomFicGeotif, ...
        'Title3', Title3, 'Intro', Intro);
    listNomFicGeotif{end+1} = nomFicGeotif; %#ok<AGROW>

	[~, GeoYXCoordinates] = sonarCalculCoordXY(LatLong_PingBeam, 1);
    
    Title3 = 'GeoX';
    Intro = 'This data is the X coordinates.';
    [nomFicFig, nomFicPng, nomFicImageTif, TypeImageTif_PingBeam, TypeImageTif_LatLong, nomFicHistoPng, ...
        nomFicLatLongFig, nomFicLatLongPng, nomFicKmz, nomFicGeotif] = AdocFile_SonarEquationExportFiles(...
        GeoYXCoordinates(1), LatLong_PingBeam, gridSize, ...
        nomDirSummary, SurveyName, nomFicSeul, 'FigHisto', 1);
    AdocUtils.addPingBeamAndLatLongImages(nomFicAdoc, nomFicFig, nomFicPng, nomFicImageTif, TypeImageTif_PingBeam, TypeImageTif_LatLong, nomFicHistoPng, ...
        nomFicLatLongFig, nomFicLatLongPng, nomFicKmz, nomFicGeotif, ...
        'Title3', Title3, 'Intro', Intro);
    listNomFicGeotif{end+1} = nomFicGeotif; %#ok<AGROW>
    
    Title3 = 'GeoY';
    Intro = 'This data is Y coordinates.';
    [nomFicFig, nomFicPng, nomFicImageTif, TypeImageTif_PingBeam, TypeImageTif_LatLong, nomFicHistoPng, ...
        nomFicLatLongFig, nomFicLatLongPng, nomFicKmz, nomFicGeotif] = AdocFile_SonarEquationExportFiles(...
        GeoYXCoordinates(2), LatLong_PingBeam, gridSize, ...
        nomDirSummary, SurveyName, nomFicSeul, 'FigHisto', 1);
    AdocUtils.addPingBeamAndLatLongImages(nomFicAdoc, nomFicFig, nomFicPng, nomFicImageTif, TypeImageTif_PingBeam, TypeImageTif_LatLong, nomFicHistoPng, ...
        nomFicLatLongFig, nomFicLatLongPng, nomFicKmz, nomFicGeotif, ...
        'Title3', Title3, 'Intro', Intro);
     listNomFicGeotif{end+1} = nomFicGeotif; %#ok<AGROW>
   
    %% Export of layers PingBeam into a NetCDF file
    
    nomFicNetCDF_PingBeam = fullfile(nomDirSummary, SurveyName, 'SonarEquation', nomFicSeul, [nomFicSeul '-Layers-PingBeam.nc']);
    Layers = [b(kBathyRT); b(kBathyDTM); Bathy_Residuals
        b(kSlopeAcross); b(kSlopeAlong)
        b(kBeamAngles); b(kAnglesEarth); b(kIncidenceAngles); Angles_Residuals
        b(kAlphaRT); b(kAlphaSSc); Alpha_Residuals
        TVG_RT; TVG_SSc; TVG_Residuals
        IA_RT; IA_SSc; IA_Residuals
        Lambert_RT; Lambert_SSc; Lambert_Residuals
        b(kReflecKM); b(kReflecSSc); ReflecSScWithoutLambert; Reflectivity_Residuals; ReflecSScStep1; Reflectivity_Residuals2
        LatLong_PingBeam(1); LatLong_PingBeam(2)
        GeoYXCoordinates(1); GeoYXCoordinates(2)
        AngleSnellDescartes];
    
    if ~isempty(kKongsbergIBA)
        Layers = [Layers
            b(kKongsbergIBA)
            AngleSnellDescartes_Residuals]; %#ok<AGROW>
    end
    nomFicNetCDF_PingBeam = exportPingBeamLayersIntoNetCDF(Layers, nomFicNetCDF_PingBeam);
        
    %% Export of layers LatLong into a NetCDF file
    
    nomFicNetCDF_LatLong = fullfile(nomDirSummary, SurveyName, 'SonarEquation', nomFicSeul, [nomFicSeul '-Layers-LatLong.nc']);
    
    nomFicNetCDF_LatLong = exportLatLongLayersIntoNetCDF(I0, nomFicNetCDF_LatLong, listNomFicGeotif);
    
    AdocUtils.addNetcdfFile(nomFicAdoc, nomFicSeul, nomFicNetCDF_PingBeam, nomFicNetCDF_LatLong, SurveyName)

    %% Export of layers into an ASCII file
    
    if flagCreateCSV
        nomFicASCII  = fullfile(nomDirSummary, SurveyName, 'ASCII', [nomFicSeul '-Layers.csv']);
        Layers = [b(kBathyRT)
            b(kBathyDTM)
            Bathy_Residuals
            b(kSlopeAcross)
            b(kSlopeAlong)
            b(kBeamAngles)
            b(kAnglesEarth)
            b(kIncidenceAngles)
            Angles_Residuals
            b(kAlphaRT)
            b(kAlphaSSc)
            Alpha_Residuals
            TVG_RT
            TVG_SSc
            TVG_Residuals
            IA_RT
            IA_SSc
            IA_Residuals
            Lambert_RT
            Lambert_SSc
            Lambert_Residuals
            b(kReflecKM)
            b(kReflecSSc)
            ReflecSScWithoutLambert
            Reflectivity_Residuals
            ReflecSScStep1
            Reflectivity_Residuals2
            LatLong_PingBeam(1)
            LatLong_PingBeam(2)
            GeoYXCoordinates(1)
            GeoYXCoordinates(2)];
        exportPingBeamLayersIntoASCII(Layers, nomFicASCII);
        AdocUtils.addAsciiFile(nomFicAdoc, nomFicASCII, SurveyName)
    end
end
my_close(hw, 'MsgEnd')

%% R�ouverture du fichier Adoc

AdocUtils.openAdoc(nomFicAdoc);


function [nomFicFig, nomFicPng, nomFicImageTif, TypeImageTif_PingBeam, TypeImageTif_LatLong, nomFicHistoPng, ...
    nomFicLatLongFig, nomFicLatLongPng, nomFicKmz, nomFicGeotif] = ...
    AdocFile_SonarEquationExportFiles(Image, LatLong_PingBeam, gridSize, ...
    nomDirSummary, SurveyName, nomFicSeul, varargin)

[varargin, FigHisto] = getPropertyValue(varargin, 'FigHisto', 0);
[varargin, CLim]     = getPropertyValue(varargin, 'CLim',     '0.5%'); %#ok<ASGLU>

nomFicHistoPng   = [];
nomFicLatLongFig = [];
nomFicLatLongPng = [];
nomFicKmz        = [];
nomFicGeotif     = [];

Image.CLim    = CLim;
CLim          = Image.CLim;
ColormapIndex = Image.ColormapIndex;

Fig = imagesc(Image);
ImageName = Image.Name;
FileName  = ImageName;

nomDirSonarEquation = fullfile(nomDirSummary, SurveyName, 'SonarEquation');
if ~exist(nomDirSonarEquation, 'dir')
    mkdir(nomDirSonarEquation)
end

nomDirSonarEquation = fullfile(nomDirSonarEquation, nomFicSeul);
if ~exist(nomDirSonarEquation, 'dir')
    mkdir(nomDirSonarEquation)
end

nomFicFig = fullfile(nomDirSonarEquation, [FileName '.fig']);
savefig(Fig, nomFicFig)

nomFicPng = fullfile(nomDirSonarEquation, [FileName '.png']);
fig2fic(Fig, nomFicPng)
close(Fig);

nomFicImageTif = fullfile(nomDirSonarEquation, [FileName '.tif']);
TypeImageTif_PingBeam = 'Tif 32 bits file';
export_geoTiff32Bits(Image, nomFicImageTif, 'Mute', 1);

% nomFicXML = fullfile(nomDirSonarEquation, [FileName '.xml']);
% TODO : export_info_xml plante pour l'instant sur la structure Carto - Ellipsoid
% export_info_xml(Image, nomFicXML);

if FigHisto
    nomFicHistoPng = fullfile(nomDirSonarEquation, [FileName '-Histo.png']);
	histo1D(Image)
    Fig = gcf;
    fig2fic(Fig, nomFicHistoPng)
    close(Fig);
end

[flag, Layer_LatLong] = sonar_mosaique(Image, ...
    LatLong_PingBeam(1), LatLong_PingBeam(2), gridSize, 'AcrossInterpolation', 2, 'NoWaitbar', 1);
if ~flag
    return
end

InitialImageName = Layer_LatLong.InitialImageName;
% InitialImageName = strrep(InitialImageName, '_Compensation_Compensation', '_Compensation');
% InitialImageName = [InitialImageName ' - ' num2str(rand(1))];
ImageName = Layer_LatLong.Name;
Layer_LatLong = WinFillNaN(Layer_LatLong, 'window', [5 5]);
Layer_LatLong.Name             = ImageName;
Layer_LatLong.InitialImageName = InitialImageName;

Layer_LatLong.CLim = CLim;
Layer_LatLong.ColormapIndex = ColormapIndex;

Fig = imagesc(Layer_LatLong);
% ImageName = Layer_LatLong.Name;
FileName = ImageName;

nomFicLatLongFig = fullfile(nomDirSonarEquation, [FileName '.fig']);
savefig(Fig, nomFicLatLongFig)

nomFicLatLongPng = fullfile(nomDirSonarEquation, [FileName '.png']);
fig2fic(Fig, nomFicLatLongPng)
close(Fig);

nomFicKmz = fullfile(nomDirSonarEquation, [FileName '.kmz']);
export_GoogleEarth(Layer_LatLong, 'nomFic', nomFicKmz, 'flagFillNaN', 1);
winopen(nomFicKmz)

nomFicGeotif = fullfile(nomDirSonarEquation, [FileName '.tif']);
if Layer_LatLong.ImageType == 1 % '1=Intensity' | '2=RGB' | '3=Indexed' | '4=Boolean'     
    TypeImageTif_LatLong = 'Geotif 32 bits file (ready for GIS)';
    export_geoTiff32Bits(Layer_LatLong, nomFicGeotif, 'Mute', 1);
else
    TypeImageTif_LatLong = 'Classical Geotif file';
    export_tif(Layer_LatLong, nomFicGeotif);
%     winopen(nomFicGeotif)
end



function [nomFicFig, nomFicPng, nomFicKmz, nomFicGeotif, DataType, TypeGeotif] = exportRefDTMFig2PngAndFigFiles(Image, nomDirSummary, SurveyName)

DataType = Image.DataType;

Fig = imagesc(Image);
ImageName = Image.Name;

nomDir = fullfile(nomDirSummary, SurveyName, 'SonarEquation', 'ReferenceDTM');
if ~exist(nomDir, 'dir')
    mkdir(nomDir)
end

nomFicFig = fullfile(nomDir, [ImageName '.fig']);
savefig(Fig, nomFicFig)

nomFicPng = fullfile(nomDir, [ImageName '.png']);
fig2fic(Fig, nomFicPng)
close(Fig);

nomFicKmz = fullfile(nomDir, [ImageName '.kmz']);
export_GoogleEarth(Image, 'nomFic', nomFicKmz, 'flagFillNaN', 1);
winopen(nomFicKmz)

nomFicGeotif = fullfile(nomDir, [ImageName '.tif']);
if Image.ImageType == 1 % '1=Intensity' | '2=RGB' | '3=Indexed' | '4=Boolean'
    TypeGeotif = 'Geotif 32 bits file (ready for GIS)';
    export_geoTiff32Bits(Image, nomFicGeotif, 'Mute', 1);
else
    TypeGeotif = 'Classical Geotif file';
    export_tif(Image, nomFicGeotif);
%     winopen(nomFicGeotif)
end


function [flag, MNT] = import_RefDTM(NomFicMNT)

I0 = cl_image_I0;

[~, ~, Ext] = fileparts(NomFicMNT);

switch lower(Ext)
    case 'tif'
        [flag, MNT] = import_geotiff(I0, NomFicMNT, 'DataType', cl_image.indDataType('Bathymetry'), 'Unit', 'm');
    otherwise
        [flag, MNT] = import(I0, NomFicMNT);
end
if ~flag
    return
end

%% Contr�les

flag = testSignature(MNT, 'DataType', cl_image.indDataType('Bathymetry'), 'GeometryType', cl_image.indGeometryType('LatLong'));



function b = import_PingBeam_All(this)

Version = version_datagram(this);

switch Version
    case 'V2'
        nomFicListLayers = getNomFicDatabase('ListeLayersCalib_V2.mat');
        ListeLayers = loadmat(nomFicListLayers, 'nomVar', 'ListeLayers');
        b = import_PingBeam_All_V2(this, 'ListeLayers', ListeLayers, 'CartoAuto', 1);
        
    case 'V1'
%         nomFicListLayers = getNomFicDatabase('ListeLayersCalib_V1.mat');
%         ListeLayers = loadmat(nomFicListLayers, 'nomVar', 'ListeLayers');
        b = import_PingBeam_All_V1(this, 'ListeLayers', [2 3 27 9 24 46 26 4 36 25 43 44 45 40 41 47], 'CartoAuto', 1);
end
