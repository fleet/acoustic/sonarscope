function flag = ALL_SurveyReport_SoundSpeedProfile(this, adocFileName, varargin)

[varargin, IndNavUnit] = getPropertyValue(varargin, 'IndNavUnit', []); %#ok<ASGLU>

dosNetUse('Init')

Tag = num2str(randi(100000));
for k=length(this):-1:1
    listDataFiles{k} = this(k).nomFic;
end

%% Trac�s graphiques

[FigNav, FigSSP_Full, FigSSP_Partial, nbEffectiveSSP] = plot_soundSpeedProfile(this, 'IndNavUnit', IndNavUnit);
    
%% Report for Navigation and SPP locations

[nomDirSummary, SurveyName] = fileparts(adocFileName);
[nomFicFig, nomFicPng] = AdocUtils.exportFigAndPng(FigNav, nomDirSummary, SurveyName, Tag, 'NavigationAndSSP');
% nomFicKmz = fullfile(nomDirSummary, SurveyName, 'KMZ', [SurveyName '-Navigation' Tag '.kmz']);
% plot_navigation_ExportGoogleEarth('NomFicKmz', nomFicKmz);
my_close(FigNav);
% AdocUtils.addNavigation(adocFileName, nomFicFig, nomFicPng, 'Navigation', 'GoogleEarthFilename', nomFicKmz, 'listDataFiles', listDataFiles);
Comment = sprintf('Number of effective SSP : %d', nbEffectiveSSP);
AdocUtils.addNavigation(adocFileName, nomFicFig, nomFicPng, 'SSP locations', 'SectionName', 'Sound Speed Profiles', ...
    'listDataFiles', listDataFiles, 'Comment', Comment);

%% Sound Speed Profiles

[nomFicFig, nomFicPng] = AdocUtils.exportFigAndPng(FigSSP_Full, nomDirSummary, SurveyName, Tag, 'FullSSP');
my_close(FigSSP_Full);
AdocUtils.addNavigation(adocFileName, nomFicFig, nomFicPng, 'Full SSP');

if ~isempty(FigSSP_Partial)
    [nomFicFig, nomFicPng] = AdocUtils.exportFigAndPng(FigSSP_Partial, nomDirSummary, SurveyName, Tag, 'PartialSSP');
    my_close(FigSSP_Partial);
    AdocUtils.addNavigation(adocFileName, nomFicFig, nomFicPng, 'Partial SSP');
end

%% Ouverture du fichier Adoc

AdocUtils.openAdoc(adocFileName);
dosNetUse('Clear')

flag = 1;
