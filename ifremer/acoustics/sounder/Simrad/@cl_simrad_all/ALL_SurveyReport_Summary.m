function flag = ALL_SurveyReport_Summary(this, adocFileName, varargin)

[varargin, IndNavUnit] = getPropertyValue(varargin, 'IndNavUnit', []);
[varargin, OpenAdoc]   = getPropertyValue(varargin, 'OpenAdoc',   1); 
[varargin, Openfile]   = getPropertyValue(varargin, 'Openfile',   1); %#ok<ASGLU>

flag = 0;
dosNetUse('Init')

%% Create summary

[nomDirSummary, SurveyName] = fileparts(adocFileName);
nomFicSummary = fullfile(nomDirSummary, SurveyName, [SurveyName '-SummaryLines.csv']);
[Fig, nomFicConf, ShortSummary] = ALL_Summary_Lines(this, nomFicSummary, 'IndNavUnit', IndNavUnit, 'Openfile',Openfile,'Mute', 1);
if isempty(Fig)
    return
end
nomFicFig = fullfile(nomDirSummary, SurveyName, 'FIG', [SurveyName '-SummaryLines.fig']);
savefig(Fig, nomFicFig)
my_close(Fig);

%% Create histogram of datagrams

Fig = ALL_HistoDatagrams(this);
if isempty(Fig)
    nomFicFigHistoDatagrams = [];
    nomFicPngHistoDatagrams = [];
else
    Tag = num2str(randi(100000));
    [nomFicFigHistoDatagrams, nomFicPngHistoDatagrams] = AdocUtils.exportFigAndPng(Fig, nomDirSummary, SurveyName, Tag, 'Datagrams');
    my_close(Fig);
end
AdocUtils.addSummary(adocFileName, nomFicSummary, nomFicFig, nomFicConf, ShortSummary, nomFicFigHistoDatagrams, nomFicPngHistoDatagrams);

%% Ouverture du fichier Adoc

if OpenAdoc
    AdocUtils.openAdoc(adocFileName);
end

dosNetUse('Clear')
flag = 1;
