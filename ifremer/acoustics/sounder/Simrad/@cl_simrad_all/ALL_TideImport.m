function ALL_TideImport(this, NomFicTide, NomVar, varargin)

[varargin, Mute] = getPropertyValue(varargin, 'Mute', 0); %#ok<ASGLU> 

if ischar(NomFicTide)
    NomFicTide = {NomFicTide};
end

N = length(NomFicTide);
str1 = sprintf('Importation des fichiers %s', NomVar);
str2 = sprintf('Import the %s files', NomVar);
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:length(NomFicTide)
    my_waitbar(k, N, hw);
    ALL_TideImport_unitaire(this, NomFicTide{k}, NomVar, Mute)
end
my_close(hw, 'MsgEnd')


function ALL_TideImport_unitaire(this, NomFicTide, NomVar, Mute)

logFileId = getLogFileId;

useParallel = get_UseParallelTbx;

%% Lecture de la mar�e

[flag, Tide] = read_Tide(NomFicTide, 'CleanData', 1);
if ~flag
    return
end
TideTime  = Tide.Time;
TideValue = Tide.Value;

%% Import de la mar�e dans les fichiers .all

N = length(this);
str1 = sprintf('Importation de "%s" dans les .all.', NomVar);
str2 = sprintf('%s Import in the .all files.', NomVar);
if useParallel && (N > 1)
    logFileIdHere = logFileId;
    [hw, DQ] = create_parforWaitbar(N, Lang(str1,str2));
    parfor k=1:N
        try
            if ~Mute
                [~, nomFicKM] = fileparts(this(k).nomFic);
                [~, nomFicTI] = fileparts(NomFicTide);
                fprintf('KM file %d / %d : %s   <----   %s\n', k, N, nomFicKM, nomFicTI);
            end
            save_attitude(this(k), 'Tide', TideTime, TideValue, 'logFileId', logFileIdHere);
            send(DQ, k);
        catch ME
            ME.getReport
        end
    end
    my_close(hw, 'MsgEnd')
else
    hw = create_waitbar(Lang(str1,str2), 'N', N);
    for k=1:N
        my_waitbar(k, N, hw);
        if ~Mute
            fprintf('KM file %d / %d : %s\n', k, N, this(k).nomFic);
        end
        save_attitude(this(k), NomVar, TideTime, TideValue, 'Mute', Mute);
    end
    my_close(hw, 'MsgEnd')
end
