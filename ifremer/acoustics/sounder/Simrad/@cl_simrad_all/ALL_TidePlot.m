function ALL_TidePlot(this)

logFileId = getLogFileId;

%% Import de la mar�e dans les fichiers .all

kPlot = 0;
nbFic = length(this);
str1 = 'Visualisation de la mar�e dans les .all.';
str2 = 'Plot Tide from .all files.';
hw = create_waitbar(Lang(str1,str2), 'N', nbFic);
for k=1:nbFic
    my_waitbar(k, nbFic, hw);
    
    [flag, DataAttitude] = read_attitude_bin(this(k));
    if ~flag
        continue
    end
    
    T = datetime(DataAttitude.Time.timeMat, 'ConvertFrom', 'datenum');
    
    if isfield(DataAttitude, 'Tide')
        kPlot = kPlot + 1;
        if kPlot == 1
            FigUtils.createSScFigure;
            h = subplot(1,1,1);
        end
        
        PlotUtils.createSScPlot(h, T, DataAttitude.Tide); grid on; hold on;
    
        [~, nomFic] = fileparts(this(k).nomFic);
        str{kPlot} = nomFic; %#ok<AGROW>
    else
        mes = sprintf('No tide has been imported in file "%s"', this(k).nomFic);
        logFileId.info('ALLTidePlot', mes);
    end
end
my_close(hw, 'MsgEnd')
if kPlot ~= 0
    legend(str, 'Interpreter', 'None');
end
