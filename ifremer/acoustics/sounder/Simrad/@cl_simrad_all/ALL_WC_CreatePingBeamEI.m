function ALL_WC_CreatePingBeamEI(this, resol, nomDirOut, varargin)

global isUsingParfor %#ok<GVMIS>

[varargin, Tag]        = getPropertyValue(varargin, 'Tag',        []);
[varargin, Display]    = getPropertyValue(varargin, 'Display',    0);
[varargin, typeAlgoWC] = getPropertyValue(varargin, 'typeAlgoWC', 1); %#ok<ASGLU>

useParallel = get_UseParallelTbx;

isUsingParfor = 0;

%% Question // tbx au cas o� les .all auraient d�j� �t� convertis

if Display == 0
    useParallelHere = useParallel;
else
    useParallelHere = 0;
end

%%

N = length(this);
Carto = [];
str1 = 'Traitement de la colonne d''eau Kongsberg en g�om�trie SampleBeam';
str2 = 'Kongsberg Water Column processing in SampleBeam geometry';
if useParallelHere && (N > 1)
    % R�cup�ration de Carto
    nomDir = fileparts(this(1).nomFic);
    [flag, Carto] = test_ExistenceCarto_Folder(nomDir);
    if ~flag || isempty(Carto)
        [~, Carto] = get_Layer(this(1), 'Depth', 'Carto', Carto);
    end
    
    % Lancement du traitement avec la // Tbx
    [hw, DQ] = create_parforWaitbar(N, Lang(str1,str2));
    parfor (kFic=1:N, useParallelHere)
        ALL_WC_CreatePingBeamEI_unitaire(this(kFic), Carto, typeAlgoWC, resol, Tag, Display, nomDirOut, 'isUsingParfor', true);
        send(DQ, kFic);
    end
    my_close(hw, 'MsgEnd')
else
    hw = create_waitbar(Lang(str1,str2), 'N', N);
    for kFic=1:N
        my_waitbar(kFic, N, hw)
        [~, Carto] = ALL_WC_CreatePingBeamEI_unitaire(this(kFic), Carto, typeAlgoWC, resol, Tag, Display, nomDirOut);
    end
    my_close(hw, 'MsgEnd');
end


function [flag, Carto] = ALL_WC_CreatePingBeamEI_unitaire(this, Carto, typeAlgoWC, resol, Tag, Display, nomDirOut, varargin)

global isUsingParfor %#ok<GVMIS>

[varargin, isUsingParfor] = getPropertyValue(varargin, 'isUsingParfor', isUsingParfor); %#ok<ASGLU>

%% Lecture de la bathy

CorrectionTVG      = 30;
IndNavUnit         = [];
flagTideCorrection = 0;

ListeLayers = SelectionLayers_V1toV2([24 14]);
[Layers_PingBeam, Carto] = import_PingBeam_All_V2(this, 'ListeLayers', ListeLayers, 'Carto', Carto);
%  SonarScope(Bathy)

%% Lecture des donn�es

Bathy              = Layers_PingBeam(1);
BeamPointingAngles = Layers_PingBeam(2);

[flag, DataDepth] = read_depth_bin(this, 'Memmapfile', 0);
if ~flag
    return
end

[flag, DataWC] = read_WaterColumn_bin(this, 'Memmapfile', 0);
if ~flag
    str1 = sprintf('Il n''y a pas de datagrammes de Water Column dans le fichier "%s".', this.nomFic);
    str2 = sprintf('No Water Column datagrams in file "%s".', this.nomFic);
    my_warndlg(Lang(str1,str2), 0, 'TimeDelay', 60);
    return
end

[flag, DataRaw] = read_rawRangeBeamAngle(this, 'MemmapFile', 0);
if ~flag
    return
end

[flag, DataInstalParam, isDual] = read_installationParameters_bin(this);
if ~flag
    return
end

[flag, IdentSonar] = SimradModel2IdentSonar(DataWC.EmModel, DataDepth.EmModel, 'isDual', isDual);
if ~flag
    return
end

% TODO : am�liorer la transmission du mode
SonarDescription = cl_sounder('Sonar.Ident', IdentSonar, 'Sonar.Mode_1', 1, ...
    'Sonar.SystemSerialNumber', DataWC.SystemSerialNumber);

%% Recherche des pings communs entre sources de donn�es

subPingImage     = get(Bathy, 'PingCounter');
subPingImage     = subPingImage(:,1);
subPingFichier   = DataWC.PingCounter(:,1);
subPingDataDepth = DataDepth.PingCounter(:,1);
[suby, subWC, subDepth] = intersect3(subPingImage, subPingFichier, subPingDataDepth(DataDepth.MaskPingsBathy == 0)); %#ok<ASGLU> % subRaw
if isempty(suby)
    flag = false;
    return
end

%% Recherches des bornes des images si export pour Voxler

[flag, DataPosition] = read_position_bin(this, 'IndNavUnit', IndNavUnit, 'Memmapfile', 0);
if ~flag
    return
end

%% Initialisation des tableaux

nbPings = length(suby);

%% Pr�paration de la structure

nbBeams = DataWC.Dimensions.nbRx;
% TimePing = Bathy.Sonar.Time(suby);
TimePing = get(Bathy, 'Time');
Date = TimePing.date;
Hour = TimePing.heure;

DataWCRaw.from            = 'Unkown';
DataWCRaw.PingNumber      = suby;
DataWCRaw.Date            = Date(suby);
DataWCRaw.Hour            = Hour(suby);
DataWCRaw.LatitudeTop     = NaN(nbPings,nbBeams);
DataWCRaw.LongitudeTop    = NaN(nbPings,nbBeams);
DataWCRaw.LatitudeBottom  = NaN(nbPings,nbBeams);
DataWCRaw.LongitudeBottom = NaN(nbPings,nbBeams);
DataWCRaw.DepthTop        = NaN(nbPings,nbBeams);
DataWCRaw.DepthBottom     = NaN(nbPings,nbBeams);
DataWCRaw.ImageSegment    = NaN(nbPings,nbBeams);
DataWCRaw.EITopRaw        = NaN(nbPings, nbBeams, 'single');
DataWCRaw.EIBottomRaw     = NaN(nbPings, nbBeams, 'single');
DataWCRaw.EITopComp       = NaN(nbPings, nbBeams, 'single');
DataWCRaw.EIBottomComp    = NaN(nbPings, nbBeams, 'single');

%% Lecture et interpolation de la donnee "Attitude"

[flag, DataAttitude] = read_attitude_bin(this, 'Memmapfile', 0);
if ~flag || isempty(DataAttitude) || isempty(DataAttitude.Roll)
    return
end
[~, DataRuntime] = SSc_ReadDatagrams(this.nomFic, 'Ssc_Runtime', 'LogSilence', 0); % Non test�

TDepth = DataDepth.Time.timeMat;
TDepth = cl_time('timeMat', TDepth(subDepth));
Roll = my_interp1(DataAttitude.Time, DataAttitude.Roll,   TDepth, 'linear', 'extrap');

%% Recherche de la position longitudinale de l'antenne d'�mission par rapport au point de r�f�rence du sondeur

SonarShip = get(Bathy, 'SonarShip');

%% Boucle sur les pings

Angles = get_Image(BeamPointingAngles);
subPingOK = false(1,nbPings);
kImage = 0;
[~, nomFicSeul, Ext] = fileparts(this.nomFic);
str1 = sprintf('Traitement WC de %s', [nomFicSeul Ext]);
str2 = sprintf('Processing WC of %s', [nomFicSeul Ext]);
hw = create_waitbar(Lang(str1,str2), 'N', nbPings);
for k=1:nbPings
    my_waitbar(k, nbPings, hw)
    
    [bRaw, Carto, TransmitSectorNumber, DetectedRangeInSamples, WCSignalWidthInSeconds] = import_WaterColumn_All(this, ...
        DataDepth, DataWC, DataRaw, DataPosition, DataAttitude, DataRuntime, DataInstalParam, SonarDescription, ...
        'Carto', Carto, 'subPing', suby(k), 'Mute', 1, 'CorrectionTVG', CorrectionTVG, ...
        'XML_ALL_WaterColumn', DataWC);
    if isempty(bRaw)
        continue
    end
    [~, bComp] = compensation_WaterColumnPlus(bRaw);

    subPingOK(k) = true;
    kImage = kImage + 1;
    
    subNaN = find(DetectedRangeInSamples < 5);
    if ~isempty(subNaN)
        subNonNaN = find(DetectedRangeInSamples >= 5);
        %         DetectedRangeInSamples(subNaN) = floor(interp1(subNonNaN, DetectedRangeInSamples(subNonNaN), subNaN, 'linear', 'extrap'));
        DetectedRangeInSamples(subNaN) = floor(interp1(subNonNaN, DetectedRangeInSamples(subNonNaN), subNaN, 'linear')); % Modif JMA le 05/10/2016
        %         DetectedRangeInSamples(isnan(DetectedRangeInSamples)) = 0; % Ajout JMA le 05/10/2016 ! Erreur, les donn�es sont visualis�es au point 0,0 (Afrique) dans 3DV
        DetectedRangeInSamples(isnan(DetectedRangeInSamples)) = max(DetectedRangeInSamples); % Ajout JMA le 05/10/2016
    end
    
    [flag, lat0, lon0, LatitudeBottom, LongitudeBottom, DepthBottom] = calcul_positionRawDataWC(bRaw, ...
        'SonarShip', SonarShip, 'typeAlgoWC', typeAlgoWC);
    if ~flag
        return
    end
    %     FigUtils.createSScFigure(67676); PlotUtils.createSScPlot(LongitudeBottom, LatitudeBottom, '*b'); grid on; hold on; PlotUtils.createSScPlot(lon0, lat0, 'or')
    
    SampleBeamData  = get(bRaw, 'SampleBeamData');
    TransducerDepth = SampleBeamData.TransducerDepth;
    
    DataWCRaw.LatitudeTop(k,:)     = lat0 + zeros(1, nbBeams);
    DataWCRaw.LongitudeTop(k,:)    = lon0 + zeros(1, nbBeams);
    DataWCRaw.LatitudeBottom(k,:)  = LatitudeBottom;
    DataWCRaw.LongitudeBottom(k,:) = LongitudeBottom;
    DataWCRaw.DepthTop(k,:)        = -TransducerDepth(1) + zeros(1, nbBeams); % (1) rajout� par JMA le 31/08/2016 pour fichier EM2040 Thalia !!!
    DataWCRaw.DepthBottom(k,:)     = -TransducerDepth(1) + DepthBottom;
    DataWCRaw.ImageSegment(k,:)    = TransmitSectorNumber;
    DataWCRaw.EndData(k,:)         = DetectedRangeInSamples;
    
    %% TODO TODO TODO TODO 24/11/2016 FLOWS voir import_WaterColumn_All et sonar_SimradProcessWC
    
    TideValue = get(bRaw, 'Tide');
    TideValue = TideValue(1);
    if flagTideCorrection && ~isnan(TideValue)
        DataWCRaw.DepthTop(k,:)    = DataWCRaw.DepthTop(k,:)    + TideValue; % Inutile mais on laisse quand m�me
        DataWCRaw.DepthBottom(k,:) = DataWCRaw.DepthBottom(k,:) + TideValue; % Inutile mais on laisse quand m�me
    end
    
    IRaw  = get_Image(bRaw);
    IComp = get_Image(bComp);
    
    [~, nbBeams] = size(IRaw);
    R0 = min(DetectedRangeInSamples);
    SampleRate = SampleBeamData.SampleRate;
    
    %% Calcul de l'�paisseur th�orique du signal de colonne d'eau en fonction de l'angle d'incidence, de l'ouverture du fausceau
    
% %     A = DataWC.BeamPointingAngle(subWC(k),:);
% %     TxPulseWidth = bRaw.Sonar.SampleBeamData.TxPulseWidth
%     Teta       = SampleBeamData.RxAnglesEarth_WC;
% %   SoundVelocity = SampleBeamData.SoundVelocity
%     Rmin = (abs(DetectedRangeInSamples) ./ cosd(Teta)) * 2 * (SampleRate / 1500);
%     % figure; plot(Rmin, 'r+'); grid on; hold on; plot(Rmin, 'r'); hold off;
    
    if isempty(WCSignalWidthInSeconds) || all(isnan(WCSignalWidthInSeconds))
        Rmin = zeros(1, nbBeams, 'single');
    else
        WCSignalWidthInSeconds(WCSignalWidthInSeconds > 1)  = NaN;
        WCSignalWidthInSeconds(WCSignalWidthInSeconds <= 0) = NaN; % TODO : � r�gler en amont dans ALL_IncidenceAngleFromOneDTM
        WCSignalWidthInSamples_DepthDatagram = WCSignalWidthInSeconds * SampleRate;
%         figure(26575); plot(WCSignalWidthInSamples_DepthDatagram); grid on;
        PingAngles = Angles(suby(k),:);
        
        % TODO : faire intervenir l'angle de roulis +++
        
        WCSignalWidthInSamples_WCDatagram = my_interp1(PingAngles, WCSignalWidthInSamples_DepthDatagram, SampleBeamData.RxAnglesEarth_WC, 'nearest');
%         Rmin = ceil(2.5 * WCSignalWidthInSamples_WCDatagram);
        Rmin = ceil(WCSignalWidthInSamples_WCDatagram);
        subNonNaN = find(~isnan(Rmin));
        Rmin = my_interp1_Extrap_PreviousThenNext(subNonNaN, Rmin(subNonNaN), 1:length(Rmin));
        Rmin(isnan(Rmin)) = 0;
    end
    
    %{
    if Display && (mod(k,Display) == 0)
        figure(876245);
        imagesc(IRaw); colormap(jet(256)); colorbar;
        str = sprintf('%s  : %d / %d', nomFicSeul, k, nbPings);
        title(str, 'Interpreter', 'none')
        hold on;
        plot(DetectedRangeInSamples, 'k');
        plot([1 nbBeams], [R0 R0]);
        plot(DetectedRangeInSamples - 2*Rmin, 'k');
        plot(DetectedRangeInSamples + 2*Rmin, 'k');
        hold off;
        drawnow
    end
    %}
    
    I1_TopRaw  = mean(IRaw( 1:R0-1,:), 'omitnan');
    I1_TopComp = mean(IComp(1:R0-1,:), 'omitnan');
    
    kLimit = DetectedRangeInSamples - floor(2*Rmin);
    for kBeam=1:nbBeams
        IRaw (kLimit(kBeam):end, kBeam) = NaN;
        IComp(kLimit(kBeam):end, kBeam) = NaN;
    end
    I1_BottomRaw  = mean(IRaw( R0:end,:), 'omitnan');
    I1_BottomComp = mean(IComp(R0:end,:), 'omitnan');
    
    DataWCRaw.EIBottomRaw(k,:)  = I1_BottomRaw;
    DataWCRaw.EITopRaw(k,:)     = I1_TopRaw;
    DataWCRaw.EIBottomComp(k,:) = I1_BottomComp;
    DataWCRaw.EITopComp(k,:)    = I1_TopComp;
%     figure(553255); plot(I1_TopRaw, 'r'); hold on; plot(I1_BottomRaw, 'b'); grid on; hold off;
%     drawnow
end
my_close(hw, 'MsgEnd')

if Display
    figure('Name', nomFicSeul);
    hc(1) = subplot(2,2,1); imagesc(DataWCRaw.EITopRaw);     colormap(jet(256)); colorbar; axis xy; title('EITopRaw');
    hc(2) = subplot(2,2,2); imagesc(DataWCRaw.EIBottomRaw);  colormap(jet(256)); colorbar; axis xy; title('EIBottomRaw');
    hc(3) = subplot(2,2,3); imagesc(DataWCRaw.EITopComp);    colormap(jet(256)); colorbar; axis xy; title('EITopComp');
    hc(4) = subplot(2,2,4); imagesc(DataWCRaw.EIBottomComp); colormap(jet(256)); colorbar; axis xy; title('EIBottomComp');
    linkaxes(hc); axis tight; drawnow;
    figure;
    pcolor(DataWCRaw.LongitudeBottom, DataWCRaw.LatitudeBottom, DataWCRaw.EITopComp); shading flat;
    colormap(jet(256)); axisGeo; title([nomFicSeul ' - EITopComp'], 'Interpreter', 'none');
    drawnow
end

%% Cr�ation des instances cl_image des EI en PingBeam

InitialImageName = [nomFicSeul '-EITopComp'];
x = 1:nbBeams;
y = 1:nbPings;

DataType = cl_image.indDataType('Reflectivity');
b = cl_image('Image', DataWCRaw.EITopComp, 'Name', InitialImageName, 'Unit', 'dB', ...
    'x', x, 'y', y, 'xUnit', 'WC Beam #', 'yUnit', 'Ping #', ...
    'DataType', DataType, 'GeometryType', cl_image.indGeometryType('PingBeam'));

DataType = cl_image.indDataType('Latitude');
b(2) = cl_image('Image', DataWCRaw.LatitudeBottom, 'Name', InitialImageName, 'Unit', 'deg', ...
    'x', x, 'y', y, 'xUnit', 'WC Beam #', 'yUnit', 'Ping #', ...
    'DataType', DataType, 'GeometryType', cl_image.indGeometryType('PingBeam'));

DataType = cl_image.indDataType('Longitude');
b(3) = cl_image('Image', DataWCRaw.LongitudeBottom, 'Name', InitialImageName, 'Unit', 'deg', ...
    'x', x, 'y', y, 'xUnit', 'WC Beam #', 'yUnit', 'Ping #', ...
    'DataType', DataType, 'GeometryType', cl_image.indGeometryType('PingBeam'));


LatMean = mean(DataWCRaw.LatitudeBottom(:), 'omitnan');
LonMean = mean(DataWCRaw.LongitudeBottom(:), 'omitnan');
Fuseau = floor((LonMean+180)/6 + 1);
if LatMean >= 0
    H = 'N';
else
    H = 'S';
end
Carto = cl_carto('Ellipsoide.Type', 11, 'Projection.Type', 3, 'Projection.UTM.Fuseau', Fuseau, 'Projection.UTM.Hemisphere', H);

TagSynchroX = [nomFicSeul ' - PingBeamWC'];
TagSynchroY = [nomFicSeul ' - PingBeamWC'];
SonarTime = cl_time('timeIfr', DataWCRaw.Date, DataWCRaw.Hour);
for k=1:length(b)
    b(k).Name              = code_ImageName(b(k));
    b(k) = set_Carto(b(k), Carto);
    b(k).InitialFileName   = this.nomFic;
    b(k).InitialFileFormat = 'SimradAll';
    b(k) = set_SonarTime(b(k), SonarTime);
    b(k).TagSynchroX       = TagSynchroX;
    b(k).TagSynchroY       = TagSynchroY;
end
% SonarScope(b)

%% Sauvegarde des images d'EI

for k=1:length(b)
    nomFicXML = fullfile(nomDirOut, [b(k).Name '.ers']);
    export_xml(b(k), nomFicXML);
end

%% Mosa�que du layer EITopComp

[flag, c] = sonar_mosaique(b(1), b(2), b(3), resol, 'NoWaitbar', 1);
if ~flag
    return
end
c = WinFillNaN(c, 'window', [5 5]);

if isempty(Tag)
    ImageName = [InitialImageName ' - ' num2str(rand(1))];
else
    ImageName = [InitialImageName ' - ' Tag '-' num2str(rand(1))];
end
c = set(c, 'Name', ImageName);            
% SonarScope(c)

%% Export des images

nomFicErs = fullfile(nomDirOut, [c.InitialImageName '.ers']);
flag = export_ermapper(c, nomFicErs);
if ~flag
    return
end
