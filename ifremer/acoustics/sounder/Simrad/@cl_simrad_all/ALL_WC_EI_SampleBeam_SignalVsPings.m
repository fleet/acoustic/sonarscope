% listFileNames = 'D:\Temp\SPFE\B\0000_20180307_090137_SimonStevin.all';
% listFileNames = 'D:\Temp\SPFE\B\0027_20160915_105616_SimonStevin.all';
% % listFileNames{1} = 'D:\Temp\SPFE\A\0029_20180306_213505_SimonStevin.all';
% % listFileNames{2} = 'D:\Temp\SPFE\A\0030_20180306_213905_SimonStevin.all';
% % listFileNames{3} = 'D:\Temp\SPFE\A\0031_20180306_214305_SimonStevin.all';
% nomDirCSVOut  = 'D:\Temp\SPFE\A';
% listFileNames = 'D:\Temp\SPFE\20210305\0028_20200803_121402_SimonStevin.all';
% nomDirCSVOut  = 'D:\Temp\SPFE\20210305';
% [flag, aKM] = ALL.checkFiles(listFileNames);
% DataName      = 'Quantile2';
% TypeUnit      = 'dB';
% CorrectionTVG = 30;
% Quantile1     = 1;
% Quantile2     = 99;
% CLim          = [];
% ALim          = [-60  60];
% BLim          = [];
% params_ZoneEI.Where = 'ASC';
% params_ZoneEI.SeuilHaut = 0.85;
% params_ZoneEI.SeuilBas = 0.96;
% params_ZoneEI.DistanceSecutity = 0;
% params_ZoneEI.DistHaut = 1;
% params_ZoneEI.DistBas = 0;
% Display       = 20;
% Tag           = 'JMA4';
% maskOverlapingBeams = true;
% thresholdValueRaw  =  -35;
% thresholdValueComp =  5;
% ALL_WC_EI_SampleBeam_SignalVsPings(aKM, DataName, TypeUnit, CorrectionTVG, Quantile1, Quantile2, ...
%   CLim, ALim, BLim, params_ZoneEI, Display, nomDirCSVOut, Tag, maskOverlapingBeams, thresholdValueRaw, thresholdValueComp);

function flag = ALL_WC_EI_SampleBeam_SignalVsPings(this, DataName, TypeUnit, CorrectionTVG, Quantile1, Quantile2, ...
    CLim, ALim, BLim, params_ZoneEI, Display, nomDirCSVOut, Tag, maskOverlapingBeams, ...
    thresholdValueRaw, thresholdValueComp, varargin)

[varargin, suby]       = getPropertyValue(varargin, 'suby',       []);
[varargin, IndNavUnit] = getPropertyValue(varargin, 'IndNavUnit', []);
[varargin, Carto]      = getPropertyValue(varargin, 'Carto',      []);
[varargin, Fig1]       = getPropertyValue(varargin, 'Fig1',       []);
[varargin, Fig2]       = getPropertyValue(varargin, 'Fig2',       []); %#ok<ASGLU>

Title = DataName;
if ~isempty(Quantile2)
    Title = [Title ':' num2strCode([Quantile1 Quantile2]) '%'];
end
if ~isempty(ALim)
    Title = [Title ' / Angle limits : ' num2strCode(ALim)];
end
TitleRaw  = ['Raw-'  Title];
TitleComp = ['Comp-' Title];
if ~isempty(Tag)
    TitleRaw  = [TitleRaw  ' - ' Tag];
    TitleComp = [TitleComp ' - ' Tag];
end
InfoImageRaw  = {};
InfoImageComp = {};

%% Cr�ation de la fen�tre avec les menus et callbacks sp�cialis�s pour la nav

kFic = 0;
N = length(this);
nbWCPings = zeros(1,N);
str1 = 'Calcul de l'' EI au dessus du sp�culaire';
str2 = 'Computing EI over specular';
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
    my_waitbar(k, N, hw);
    nomFic = this(k).nomFic;
    
    [flag, All, DataDepth, DataRaw, DataWC, DataSeabed, DataPosition, DataAttitude, DataRuntime, DataInstalParam, DataSSP, ...
        subDepth, subRaw, subSeabed, subWC, FlagPings, Latitude, Longitude, Heading, tWC] = lectureGlobaleALL(nomFic, IndNavUnit, ...
        'subDepth', suby);
    if ~flag
        continue
    end
    tabSubWC{k}  = subWC; %#ok<AGROW>
    nbWCPings(k) = DataWC.Dimensions.nbPings;
    
    %% Test Set DoNotDelete=false � tous les memmapfiles car bug en version //Tbx
    
    flagNetcdf = existCacheNetcdf(nomFic);
    if flagNetcdf
        DataDepth = setMemmapfileDoNotDelete(DataDepth, 'true');
        DataWC    = setMemmapfileDoNotDelete(DataWC,    'true');
        
        DataRaw = renameField(DataRaw, 'TransmitSectorNumber',      'TransmitSectorNumberRx');
        DataRaw = renameField(DataRaw, 'TwoWayTravelTimeInSeconds', 'TwoWayTravelTime');
        DataRaw = setMemmapfileDoNotDelete(DataRaw,   'true');
        DataRaw = renameField(DataRaw, 'TransmitSectorNumberRx', 'TransmitSectorNumber');
        DataRaw = renameField(DataRaw, 'TwoWayTravelTime',       'TwoWayTravelTimeInSeconds');
    end
    
    %% Calcul du signal
    
    kFic = kFic + 1;
    
    [ValueRaw{kFic}, ValueComp{kFic}, Time{kFic}, InfoRaw, InfoComp, Carto, SonarDescription] ...
        = EI_computeValue(All, DataWC, DataDepth, DataRaw, DataSeabed, DataAttitude, DataRuntime, ...
        DataPosition, DataSSP, DataInstalParam, ...
        subDepth, subRaw, FlagPings, params_ZoneEI, DataName, TypeUnit, CorrectionTVG, Quantile1, Quantile2, ...
        ALim, BLim, Carto, Display, Latitude, Longitude, ...
        maskOverlapingBeams, thresholdValueRaw, thresholdValueComp); %#ok<AGROW>
    if ~isempty(InfoRaw)
        InfoImageRaw{kFic}  = InfoRaw; %#ok<AGROW> % TODO : peut �tre un tableau de structure
        InfoImageComp{kFic} = InfoComp; %#ok<AGROW>
    end
    listFicAll{kFic} = nomFic; %#ok<AGROW>
    %     nbPingsWC(kFic)  = DataWC.Dimensions.nbPings; %#ok<AGROW>
    
    %% Plot de la nav
    
    MetriqueRaw  = [ValueRaw{kFic}.(DataName)];
    MetriqueComp = [ValueComp{kFic}.(DataName)];
    
    if all(isnan(MetriqueRaw))
        continue
    end
    
    if isempty(Fig1)
        Fig1 = plot_navigation_Figure(Fig1); % DataName
        colormap(jet(256)); colorbar;
        Fig2 = plot_navigation_Figure(Fig2); % DataName
        colormap(jet(256)); colorbar;
    end
    
    scatter_navigation(nomFic, Fig1, Longitude, Latitude, tWC, Heading, MetriqueRaw, ...
        'Title', TitleRaw,  'Tag', [All.nomFic '-Raw']);
    scatter_navigation(nomFic, Fig2, Longitude, Latitude, tWC, Heading, MetriqueComp, ...
        'Title', TitleComp, 'Tag', [All.nomFic '-Comp']);
    
    %% Test Set DoNotDelete=false � tous les memmapfiles car bug en version //Tbx
    
    if flagNetcdf
        DataDepth = setMemmapfileDoNotDelete(DataDepth, 'false'); %#ok<NASGU>
        DataWC    = setMemmapfileDoNotDelete(DataWC,    'false'); %#ok<NASGU>
        
        DataRaw = renameField(DataRaw, 'TransmitSectorNumber',      'TransmitSectorNumberRx');
        DataRaw = renameField(DataRaw, 'TwoWayTravelTimeInSeconds', 'TwoWayTravelTime');
        DataRaw = setMemmapfileDoNotDelete(DataRaw, 'false');
        DataRaw = renameField(DataRaw, 'TransmitSectorNumberRx', 'TransmitSectorNumber');
        DataRaw = renameField(DataRaw, 'TwoWayTravelTime',       'TwoWayTravelTimeInSeconds'); %#ok<NASGU>
    end
    
    clear DataDepth DataRaw DataWC
    if flagNetcdf
        deleteFileOnListe(cl_memmapfile.empty, []) % N�cessaire si //Tbx ET Netcdf
    end
end
my_close(hw, 'MsgEnd')
if kFic == 0
    return
end

%% Rehaussement de contraste

if isempty(CLim')
    if isempty(CLim)
        X  = [ValueRaw{:}];
        X = [X(:).(DataName)];
        Val = stats(X); %, 'Seuil', [1 95]);
        if Val.NbData == 0
            msg = 'There is no data here, check your filters and try again.';
            my_warndlg(msg, 1);
            return
        end
        CLim = Val.Quant_01_99;
    end
end
if CLim(1) == CLim(2)
    CLim = CLim + [-1 1];
end

set(findobj(Fig1, 'Type', 'axes'), 'CLim', CLim);

X  = [ValueComp{:}];
X = [X(:).(DataName)];
Val = stats(X); %, 'Seuil', [1 95]);
if Val.NbData == 0
    msg = 'There is no data here, check your filters and try again.';
    my_warndlg(msg, 1);
    return
end
CLim2 = Val.Quant_01_99;
if CLim2(1) == CLim2(2)
    CLim2(1) = CLim2(1) - 1;
    CLim2(2) = CLim2(2) + 1;
end
set(findobj(Fig2, 'Type', 'axes'), 'CLim', CLim2);

%% Edition des signaux

clear MetriqueRaw MetriqueComp
if isempty(InfoImageRaw)
    subOKRaw  = [];
    subOKComp = [];
else
    for k=1:length(ValueRaw)
        if ~isempty(ValueRaw{k})
            if (k > length(InfoImageRaw))|| isempty(InfoImageRaw{k})
                Title = sprintf('Signal %d', k);
            else
                [~, Title] = fileparts(InfoImageRaw{k}.AllFilename);
            end
        end
        
        MetriqueRaw{k}  = [ValueRaw{k}.(DataName)]; %#ok<AGROW>
        MetriqueComp{k} = [ValueComp{k}.(DataName)]; %#ok<AGROW>
        
        %         xSampleRaw(k)  = XSample('name', 'Pings', 'data', Time{k}');         %#ok<AGROW>
        %         xSampleComp(k) = XSample('name', 'Pings', 'data', Time{k}');         %#ok<AGROW>
        T = Time{k}; % TODO pour image en background
        xSampleRaw(k)   = XSample('name', 'Pings', 'data', T);         %#ok<AGROW>
        xSampleComp(k)  = XSample('name', 'Pings', 'data', T);         %#ok<AGROW>
        ySampleImage(k) = YSample('name', Title,   'data', ones(1, length(T), 'single'));  %#ok<AGROW> %, 'marker', '.');
        ySampleRaw(k)   = YSample('name', Title,   'data', MetriqueRaw{k},  'marker', '.'); %#ok<AGROW> %, 'marker', '.');
        ySampleComp(k)  = YSample('name', Title,   'data', MetriqueComp{k}, 'marker', '.'); %#ok<AGROW> %, 'marker', '.');
    end
    if isempty(Tag)
        NameSignalRaw  = [DataName '-Raw'];
        NameSignalComp = [DataName '-Comp'];
    else
        NameSignalRaw  = [DataName '-' Tag '-Raw'];
        NameSignalComp = [DataName '-' Tag '-Comp'];
    end
    
    signal    = ClSignal('Name', NameSignalRaw,  'xSample', xSampleRaw,  'ySample', ySampleRaw);
    signal(2) = ClSignal('Name', NameSignalComp, 'xSample', xSampleComp, 'ySample', ySampleComp);
    
    % Plot du signal calcul�
    
    signal.plot;
    
    str1 = 'Voulez-vous �diter les signaux afin de les d�spiquer, nettoyer, filter ?';
    str2 = 'Do you want to edit the signals (in order to despike, clean, smooth it, ...)';
    repeatDataCleaning = true;
    while repeatDataCleaning
        [rep, flag] = my_questdlg(Lang(str1,str2), 'Init', 2); %, 'WaitingTime', 10);
        if flag && (rep == 1)
            str1 = 'TODO';
            str2 = 'Warning : Do not forget to close the SignalDialog window once you have finished the data cleaning in order to to continue the processing.';
            my_warndlg(Lang(str1,str2), 1);
            
            for k=1:length(ValueRaw)
                InfoRaw = InfoImageRaw{k};
                if isempty(InfoRaw)
                    continue
                end
                T = datenum(Time{k}'); % TODO pour image en background
                I    = InfoRaw.DisplayImageRaw.CData;
                y    = InfoRaw.DisplayImageRaw.x;
                %             y    = InfoRaw.DisplayImageRaw.y;
                x = 1:length(T);
                CLim = InfoRaw.DisplayImageRaw.CLim;
                %             HeightBas  = InfoRaw.DisplayImageRaw.HeightBas;
                %             HeightHaut = InfoRaw.DisplayImageRaw.HeightHaut;
                ImageName  = InfoRaw.DisplayImageRaw.Name;
                imageData = ClImageData('cData', flipud(I), 'xData', x, 'yData', y, 'colormapData', jet(256), 'clim', CLim, 'name', ImageName);
                
                xSampleRawOne  = XSample('name', 'Pings', 'data', x);
                signalOne    = ClSignal('Name', NameSignalRaw,  'xSample', xSampleRawOne, 'ySample', ySampleImage(k));
                signalOne(2) = ClSignal('Name', NameSignalRaw,  'xSample', xSampleRawOne, 'ySample', ySampleRaw(k));
                signalOne(3) = ClSignal('Name', NameSignalComp, 'xSample', xSampleRawOne, 'ySample', ySampleComp(k));
                
                % s = SignalDialog(signal, 'Title', DataName, 'cleanInterpolation', 1);%, 'waitAnswer', false);
%                 s = SignalDialog(signalOne, 'Title', DataName, 'cleanInterpolation', 1, 'imageDataList', imageData);%, 'waitAnswer', false);
                s = SignalDialog(signalOne, 'Title', DataName, 'cleanInterpolation', 0, 'imageDataList', imageData);%, 'waitAnswer', false);
                s.openDialog();
                if s.okPressedOut
                    X = MetriqueRaw{k};
                    MetriqueRaw{k} = ySampleRaw(k).data;
                    subOKRaw{k} = (MetriqueRaw{k} == X); %#ok<AGROW>
                    
                    X = MetriqueComp{k};
                    MetriqueComp{k} = ySampleComp(k).data;
                    subOKComp{k} = (MetriqueComp{k} == X); %#ok<AGROW>
                else
                    subOKRaw  = [];
                    subOKComp = [];
                end
            end
            
            %% Suppression des fen�tre pr�c�dentes
            
            for k=1:length(InfoImageRaw)
                if ~isempty(InfoImageRaw{k}) && ishandle(InfoImageRaw{k}.FigRaw)
                    close(InfoImageRaw{k}.FigRaw);
                end
                if ~isempty(InfoImageComp{k}) && ishandle(InfoImageComp{k}.FigComp)
                    close(InfoImageComp{k}.FigComp);
                end
            end
            
            %% Retrac� des fen�tres avec les signaux nettoy�s
            
            [flag, thresholdValueRaw, thresholdValueComp] = saisie_tresholdDisplaWCEchogramEI;
            if flag
                % Trac� des naavigations nettoy�es
                Fig1 = plot_navigation_Figure([]); % DataName
                colormap(jet(256)); colorbar;
                Fig2 = plot_navigation_Figure([]); % DataName
                colormap(jet(256)); colorbar;
                
                N = length(subOKRaw);
                str1 = 'Retrac� de toutes les figures apr�s nettoyage';
                str2 = 'Replay all figures after data cleaning';
                hw = create_waitbar(Lang(str1,str2), 'N', N);
                for k=1:N
                    my_waitbar(k, N, hw);
                    [flag, All, DataDepth, DataRaw, DataWC, DataSeabed, DataPosition, DataAttitude, DataRuntime, DataInstalParam, DataSSP, ...
                        subDepth, subRaw, subSeabed, subWC, FlagPings, Latitude, Longitude, Heading, tWC] = lectureGlobaleALL(listFicAll{k}, IndNavUnit);
                    if ~flag
                        continue
                    end
                    
                    [ValMaxRaw, kRaw]   = max(MetriqueRaw{k});
                    [ValMaxComp, kComp] = max(MetriqueComp{k});
                    
                    if (ValMaxRaw > thresholdValueRaw) || (ValMaxComp > thresholdValueComp)
                        [ARaw, Carto] = import_WaterColumn_All(All, DataDepth, DataWC, DataRaw, DataPosition, DataAttitude, ...
                            DataRuntime, DataInstalParam, SonarDescription, 'Carto', Carto, 'subPing', subDepth(kRaw), 'CorrectionTVG', CorrectionTVG, ...
                            'maskOverlapingBeams', maskOverlapingBeams, 'Mute', 1);
                        
                        [B, Carto] = import_WaterColumn_All(All, DataDepth, DataWC, DataRaw, DataPosition, DataAttitude, ...
                            DataRuntime, DataInstalParam, SonarDescription, 'Carto', Carto, 'subPing', subDepth(kComp), 'CorrectionTVG', CorrectionTVG, ...
                            'maskOverlapingBeams', maskOverlapingBeams, 'Mute', 1);
                        [~, BComp] = compensation_WaterColumnPlus(B);
                        
                        % Affichage des nouvelles images
                        InfoImageRaw{k} = WCD.Process.plotSignalAndBestPing(ARaw, BComp, listFicAll{k}, DataName, ...
                            MetriqueRaw{k}, MetriqueComp{k}, ...
                            Carto, subDepth, params_ZoneEI, ALim, BLim, CorrectionTVG, TypeUnit, maskOverlapingBeams, ...
                            DataDepth, DataRaw, DataWC, DataSeabed, DataInstalParam, DataRuntime, DataSSP, DataAttitude); %#ok<AGROW>
                    else
                        my_breakpoint
                    end
                    
                    scatter_navigation(nomFic, Fig1, Longitude(FlagPings), Latitude(FlagPings), tWC(FlagPings), Heading(FlagPings), MetriqueRaw{k}(FlagPings), ...
                        'Title', TitleRaw,  'Tag', [All.nomFic '-Raw']);
                    scatter_navigation(nomFic, Fig2, Longitude(FlagPings), Latitude(FlagPings), tWC(FlagPings), Heading(FlagPings), MetriqueComp{k}(FlagPings), ...
                        'Title', TitleComp, 'Tag', [All.nomFic '-Comp']);
                end
                my_close(hw, 'MsgEnd')
            end
            
            %% Sauvegarde des flags
            
            str1 = 'Voulez-vous sauver les flags des pings de WC ?';
            str2 = 'Do you want to save the flags of the WC pings ?';
            [ind, flag] = my_questdlg(Lang(str1,str2));
            if ~flag
                return
            end
            if ind == 1
                for k=1:length(subOKRaw)
                    if any(~subOKRaw{k})
                        flag = ALL_writePingFlagsWaterColumn(listFicAll{k}, tabSubWC{k}, subOKRaw{k}, nbWCPings(k));
                        if ~flag
                            continue
                        end
                    end
                end
                
                for k=1:length(ValueRaw)
                    ySampleRaw(k)  = YSample('name', Title, 'data', MetriqueRaw{k});
                    ySampleComp(k) = YSample('name', Title, 'data', MetriqueComp{k});
                end
            end
        else
            subOKRaw  = [];
            subOKComp = [];
            repeatDataCleaning = false;
        end
        
        %% Plot du signal nettoy�
        
        signal.plot;
        
        %% Question redo
        
        str1 = 'Voulez-vous re�diter les signaux ?';
        str2 = 'Do you want to re-edit the signals ?';
    end
end

%% Crossplot de Raw / Comp

FigUtils.createSScFigure;
for k=1:length(ValueRaw)
    if isempty(subOKRaw)
        MR = MetriqueRaw{k};
        MC = MetriqueComp{k};
    else
        MR = MetriqueRaw{k}(subOKRaw{k});
        MC = MetriqueComp{k}(subOKRaw{k});
    end
    PlotUtils.createSScPlot(MR, MC, '*'); hold on; grid on; axis equal;
end
if isempty(Tag)
    Title = ['Crossplot : ' DataName ' , Comp / Raw'];
else
    Title = ['Crossplot : ' DataName '-' Tag ' , Comp / Raw'];
end
xlabel(['Raw ' DataName]); ylabel(['Comp ' DataName]); title(Title);

%% Export des signaux

VarNames = {'PingCounter'; 'NbSamples'; 'Max'; 'Min'; 'Range'; 'Mean'; 'Median'; 'Std'; 'Quantile1'; 'Quantile2'; 'QuantilesRange'; 'Latitude'; 'Longitude'; 'Speed'; 'Acceleration'; 'Heading'; 'PingDirection'; 'Depth'; 'Bathy'};

if ~isempty(InfoImageRaw) && ~isempty(nomDirCSVOut)
    N = length(ValueRaw);
    str1 = 'Export des .csv';
    str2 = 'Export the .csv files';
    hw = create_waitbar(Lang(str1,str2), 'N', N);
    for k=1:N
        %         if ~isempty(ValueRaw{k})
        if isempty(InfoImageRaw{k})
            nomFicSeul = sprintf('Signal%d', k);
        else
            [~, nomFicSeul] = fileparts(InfoImageRaw{k}.AllFilename);
        end
        if isempty(Tag)
            nomFicWithTag = sprintf('%s-WC-EI-Raw', nomFicSeul);
        else
            nomFicWithTag = sprintf('%s-%s-WC-EI-Raw', nomFicSeul, Tag);
        end
        nomFicRawCsv{k} = fullfile(nomDirCSVOut, [nomFicWithTag '.csv']); %#ok<AGROW>
        
        PingCounter    = [ValueRaw{k}.PingCounter];
        NbSamples      = [ValueRaw{k}.NbSamples];
        Max            = [ValueRaw{k}.Max];
        Min            = [ValueRaw{k}.Min];
        Range          = [ValueRaw{k}.Range];
        Mean           = [ValueRaw{k}.Mean];
        Median         = [ValueRaw{k}.Median];
        Std            = [ValueRaw{k}.Std];
        Quantile1      = [ValueRaw{k}.Quantile1];
        Quantile2      = [ValueRaw{k}.Quantile2];
        QuantilesRange = [ValueRaw{k}.QuantilesRange];
        Latitude       = [ValueRaw{k}.Latitude];
        Longitude      = [ValueRaw{k}.Longitude];
        Speed          = [ValueRaw{k}.Speed];
        Acceleration   = [ValueRaw{k}.Acceleration];
        Heading        = [ValueRaw{k}.Heading]; % TODO : existe d�j� : controler 
        Depth          = [ValueRaw{k}.Depth];
        Bathy          = [ValueRaw{k}.Bathy];

        Speed          = single(Speed);
        Acceleration   = single(Acceleration);
        Heading        = single(Heading);
        Depth          = single(Depth);
        Bathy          = single(Bathy);

        
        if isempty(subOKRaw)
            Datetime = Time{k};
        else
            Datetime       = Time{k}(       subOKRaw{k});
            PingCounter    = PingCounter(   subOKRaw{k});
            NbSamples      = NbSamples(     subOKRaw{k});
            Max            = Max(           subOKRaw{k});
            Min            = Min(           subOKRaw{k});
            Range          = Range(         subOKRaw{k});
            Mean           = Mean(          subOKRaw{k});
            Median         = Median(        subOKRaw{k});
            Std            = Std(           subOKRaw{k});
            Quantile1      = Quantile1(     subOKRaw{k});
            Quantile2      = Quantile2(     subOKRaw{k});
            QuantilesRange = QuantilesRange(subOKRaw{k});
            Latitude       = Latitude(      subOKRaw{k});
            Longitude      = Longitude(     subOKRaw{k});
            Speed          = Speed(         subOKRaw{k});
            Acceleration   = Acceleration(  subOKRaw{k});
            Heading        = Heading(       subOKRaw{k}); % TODO : existe d�j� : controler 
            Depth          = Depth(         subOKRaw{k});
            Bathy          = Bathy(         subOKRaw{k});
        end
        PingOrientation = mod(Heading + 90, 360);
        
        [~, OriginFileName, Ext] = fileparts(listFicAll{k});
        flag = exportSignalInCsvFile(nomFicRawCsv{k}, Datetime, PingCounter, ...
            NbSamples, Max, Min, Range, Mean, Median, Std, ...
            Quantile1, Quantile2, QuantilesRange, Latitude, Longitude, ...
            Speed, Acceleration, Heading, Depth, PingOrientation, Bathy, 'VarNames', VarNames, 'OriginFileName', [OriginFileName Ext]);
        if ~flag
            return
        end
        
        if isempty(Tag)
            nomFicWithTag = sprintf('%s-WC-EI-Comp', nomFicSeul);
        else
            nomFicWithTag = sprintf('%s-%s-WC-EI-Comp', nomFicSeul, Tag);
        end
        
        nomFicCompCsv{k} = fullfile(nomDirCSVOut, [nomFicWithTag '.csv']); %#ok<AGROW>
        
        NbSamples      = [ValueComp{k}.NbSamples];
        PingCounter    = [ValueComp{k}.PingCounter];
        Max            = [ValueComp{k}.Max];
        Min            = [ValueComp{k}.Min];
        Range          = [ValueComp{k}.Range];
        Mean           = [ValueComp{k}.Mean];
        Median         = [ValueComp{k}.Median];
        Std            = [ValueComp{k}.Std];
        Quantile1      = [ValueComp{k}.Quantile1];
        Quantile2      = [ValueComp{k}.Quantile2];
        QuantilesRange = [ValueComp{k}.QuantilesRange];
        Latitude       = [ValueComp{k}.Latitude];
        Longitude      = [ValueComp{k}.Longitude];
        Speed          = [ValueComp{k}.Speed];
        Acceleration   = [ValueComp{k}.Acceleration];
        
        if isempty(subOKComp)
            Datetime = Time{k};
        else
            Datetime       = Time{k}(       subOKComp{k});
            PingCounter    = PingCounter(   subOKComp{k});
            NbSamples      = NbSamples(     subOKComp{k});
            Max            = Max(           subOKComp{k});
            Min            = Min(           subOKComp{k});
            Range          = Range(         subOKComp{k});
            Mean           = Mean(          subOKComp{k});
            Median         = Median(        subOKComp{k});
            Std            = Std(           subOKComp{k});
            Quantile1      = Quantile1(     subOKComp{k});
            Quantile2      = Quantile2(     subOKComp{k});
            QuantilesRange = QuantilesRange(subOKComp{k});
            Latitude       = Latitude(      subOKComp{k});
            Longitude      = Longitude(     subOKComp{k});
            Speed          = Speed(         subOKComp{k});
            Acceleration   = Acceleration(  subOKComp{k});
        end
        
        [~, OriginFileName, Ext] = fileparts(listFicAll{k});
        flag = exportSignalInCsvFile(nomFicCompCsv{k}, Datetime, PingCounter, ...
            NbSamples, Max, Min, Range, Mean, Median, Std, ...
            Quantile1, Quantile2, QuantilesRange, Latitude, Longitude, ...
            Speed, Acceleration, Heading, Depth, PingOrientation, Bathy, 'VarNames', VarNames, 'OriginFileName', [OriginFileName Ext]);
        if ~flag
            return
        end
        %         end
        my_waitbar(k, N, hw)
    end
    my_close(hw, 'MsgEnd')
    
    %% Regroupement de tous les fichiers en un seul
    
    if isempty(Tag)
        nomFicWithTag = sprintf('AllFiles-WC-EI-Raw');
    else
        nomFicWithTag = sprintf('AllFiles-%s-WC-EI-Raw', Tag);
    end
    nomFicOut = fullfile(nomDirCSVOut, [nomFicWithTag '.csv']);
    appendFiles(nomFicRawCsv, nomFicOut, 'NbHeaders', 1);
    
    if isempty(Tag)
        nomFicWithTag = sprintf('AllFiles-WC-EI-Comp');
    else
        nomFicWithTag = sprintf('AllFiles-%s-WC-EI-Comp', Tag);
    end
    nomFicOut = fullfile(nomDirCSVOut, [nomFicWithTag '.csv']);
    appendFiles(nomFicCompCsv, nomFicOut, 'NbHeaders', 1);
    
    %% Ecriture du fichier d�crivant les param�tres utilis�s pour l'�cho-int�gration
    
    if isempty(Quantile1)
        Quantile1 = 1;
    end
    if isempty(Quantile2)
        Quantile2 = 99;
    end
    if isempty(ALim)
        ALim = [-Inf Inf];
    end
    if isempty(BLim)
        BLim = [1 Inf];
    end
    
    if isempty(Tag)
        nomFicWithTag = sprintf('%s-WC-EI', nomFicSeul);
    else
        nomFicWithTag = sprintf('%s-%s-WC-EI', nomFicSeul, Tag);
    end
    nomFicTxt = fullfile(nomDirCSVOut, [nomFicWithTag '.txt']);
    fid = fopen(nomFicTxt, 'w+');
    fprintf(fid, 'Parameters used for the generation of statistics :\n\n');
    fprintf(fid, 'Quantile1      : %s\n', num2str(Quantile1));
    fprintf(fid, 'Quantile2      : %s\n', num2str(Quantile2));
    fprintf(fid, 'Angular limits : %s\n', num2strCode(ALim));
    fprintf(fid, 'Beam limits    : %s\n', num2strCode(BLim));
    fprintf(fid, 'Thresholds     : %s\n', num2strCode([params_ZoneEI.SeuilHaut, params_ZoneEI.SeuilBas]));
    fprintf(fid, '\nUnit used to compute the mean value : %s\n', TypeUnit);
    fclose(fid);
end


function [ValueRaw, ValueComp, Time, InfoImageRaw, InfoImageComp, Carto, SonarDescription] ...
    = EI_computeValue(All, DataWC, DataDepth, DataRaw, DataSeabed, DataAttitude, DataRuntime, ...
    DataPosition, DataSSP, DataInstalParam, ...
    subDepth, subRaw, FlagPings, params_ZoneEI, DataName, TypeUnit, CorrectionTVG, Quantile1, Quantile2, ...
    ALim, BLim, Carto, Display, Latitude, Longitude, maskOverlapingBeams, thresholdValueRaw, thresholdValueComp)

useParallel = get_UseParallelTbx;

Time = DataDepth.Datetime(subDepth);
[Heading, speed, ~, acceleration] = calCapFromLatLon(Latitude, Longitude, 'Time', Time);

TransducerDepth = -abs(DataDepth.TransducerDepth(subDepth,:)); % Modif JMA le 25/11/2020 (�changes de mail avec Herv�)
Depth = DataDepth.Depth(subDepth,:);
BeamPointingAngle = DataRaw.BeamPointingAngle(subRaw,:);
BeamPointingAngle = Simrad_AnglesAntenna2ShipFloor(BeamPointingAngle, DataInstalParam, DataInstalParam.isDual);

% Mask  = DataDepth.Mask(subDepth,:);
Mask = (BeamPointingAngle < -10) | (BeamPointingAngle > 10);
clear BeamPointingAngle
Depth(Mask) = NaN;
Bathy = Depth + TransducerDepth(:,1);
Depth = mean(Depth, 2, 'omitnan');
Bathy = mean(Bathy, 2, 'omitnan');

InfoImageRaw  = [];
InfoImageComp = [];

N = length(subDepth);

ValueRaw(N) = WCD.Process.StatsEmpty(NaN, NaN);
ValueComp = ValueRaw;

%% Lecture de InstallationParameters

[~, ~, isDual] = read_installationParameters_bin(All);

%% Lecture du ping de WC

[flag, IdentSonar] = SimradModel2IdentSonar(DataWC.EmModel, DataDepth.EmModel, 'isDual', isDual);
if ~flag
    return
end
SonarDescription = cl_sounder('Sonar.Ident', IdentSonar, 'Sonar.Mode_1', 1, ...
    'Sonar.SystemSerialNumber', DataWC.SystemSerialNumber);

[~, Carto] = import_WaterColumn_All(All, DataDepth, DataWC, DataRaw, DataPosition, DataAttitude, ...
    DataRuntime, DataInstalParam, SonarDescription, 'Carto', Carto, 'subPing', subDepth(1), 'CorrectionTVG', CorrectionTVG, ...
    'maskOverlapingBeams', maskOverlapingBeams, 'Mute', 1);

[~, nomFicSeul, ext] = fileparts(All.nomFic);
str1 = sprintf('Calcul de l'' EI au dessus du sp�culaire pour %s', nomFicSeul);
str2 = sprintf('Computing EI over specular for %s', nomFicSeul);
if useParallel && (N > 1) && (Display == 0)
    % && ~isdeployed % TODO : ~isdeployed tant que pb pas r�solu par MathWorks
    [hw, DQ] = create_parforWaitbar(N, Lang(str1,str2));
    try
        parfor (k=1:N, useParallel)
            subPing = subDepth(k);
            if FlagPings(k)
                Title = sprintf('%s : %d / %d', [nomFicSeul, ext], k, N);
                [flag, StatsRaw, StatsComp] = computeValuePing(All, DataDepth, DataWC, DataRaw, ...
                    DataPosition, DataAttitude, DataRuntime, ...
                    DataInstalParam, SonarDescription, Carto, subPing, ...
                    params_ZoneEI, ALim, BLim, TypeUnit, CorrectionTVG, Quantile1, Quantile2, Title, maskOverlapingBeams, 0, ...
                    'isUsingParfor', true, 'NUMBER_OF_PROCESSORS', 0);
                if flag
                    StatsRaw.Speed         = speed(k);
                    StatsRaw.Acceleration  = acceleration(k);
                    StatsRaw.Latitude      = Latitude(k);
                    StatsRaw.Longitude     = Longitude(k);
                    StatsRaw.Heading       = Heading(k);
                    StatsRaw.Depth         = Depth(k);
                    StatsRaw.Bathy         = Bathy(k);
                    
                    StatsComp.Speed        = speed(k);
                    StatsComp.Acceleration = acceleration(k);
                    StatsComp.Latitude     = Latitude(k);
                    StatsComp.Longitude    = Longitude(k);
                    StatsComp.Heading      = Heading(k);
                    StatsComp.Depth        = Depth(k);
                    StatsComp.Bathy        = Bathy(k);

                    ValueRaw(k)  = StatsRaw;
                    ValueComp(k) = StatsComp;
                else
                    ValueRaw(k)  = WCD.Process.StatsEmpty(Latitude(k), Longitude(k));
                    ValueComp(k) = WCD.Process.StatsEmpty(Latitude(k), Longitude(k));
                end
            else
                ValueRaw(k)  = WCD.Process.StatsEmpty(Latitude(k), Longitude(k));
                ValueComp(k) = WCD.Process.StatsEmpty(Latitude(k), Longitude(k));
            end
            send(DQ, k);
        end
    catch ME
        fprintf('The Parallel Computing Toolbox did NOT work here : isdeployed=%d\n', isdeployed);
        fprintf('------------- ME.message --------------------\n');
        fprintf('%s\n', ME.message);
        fprintf('---------------------------------\n');
        report = getReport(ME, 'extended', 'hyperlinks', 'off');
        fprintf('------------- getReport(ME) --------------------\n');
        fprintf('%s\n', report);
        fprintf('---------------------------------\n');
        my_close(hw, 'MsgEnd', 'TimeDelay', 60*5)
        return
    end
    my_close(hw, 'MsgEnd', 'TimeDelay', 60*5)
else
    hw = create_waitbar(Lang(str1,str2), 'N', N);
    for k=1:N
        subPing = subDepth(k);
        if FlagPings(k)
            flagDisplay =  (mod(k,Display) == 0);
            Title = sprintf('%s : %d / %d', [nomFicSeul, ext], k, N);
            [flag, StatsRaw, StatsComp] = computeValuePing(All, DataDepth, DataWC, DataRaw, ...
                DataPosition, DataAttitude, DataRuntime, ...
                DataInstalParam, SonarDescription, Carto, subPing, ...
                params_ZoneEI, ALim, BLim, TypeUnit, CorrectionTVG, Quantile1, Quantile2, Title, maskOverlapingBeams, flagDisplay);
            if flag
                StatsRaw.Speed         = speed(k);
                StatsRaw.Acceleration  = acceleration(k);
                StatsRaw.Latitude      = Latitude(k);
                StatsRaw.Longitude     = Longitude(k);
                StatsRaw.Heading       = Heading(k);
                StatsRaw.Depth         = Depth(k);
                StatsRaw.Bathy         = Bathy(k);
                
                StatsComp.Speed        = speed(k);
                StatsComp.Acceleration = acceleration(k);
                StatsComp.Latitude     = Latitude(k);
                StatsComp.Longitude    = Longitude(k);
                StatsComp.Heading      = Heading(k);
                StatsComp.Depth        = Depth(k);
                StatsComp.Bathy        = Bathy(k);
                
                ValueRaw(k)  = StatsRaw;
                ValueComp(k) = StatsComp;
            else
                ValueRaw(k)  = WCD.Process.StatsEmpty(Latitude(k), Longitude(k));
                ValueComp(k) = WCD.Process.StatsEmpty(Latitude(k), Longitude(k));
            end
        else
            ValueRaw(k)  = WCD.Process.StatsEmpty(Latitude(k), Longitude(k));
            ValueComp(k) = WCD.Process.StatsEmpty(Latitude(k), Longitude(k));
        end
        my_waitbar(k, N, hw)
    end
    my_close(hw, 'MsgEnd', 'TimeDelay', 60*5)
end

%% Affichage du signal et du ping le plus interessant

MetriqueRaw  = [ValueRaw.(DataName)];
MetriqueComp = [ValueComp.(DataName)];
[ValMaxRaw,  kRaw]  = max(MetriqueRaw);
[ValMaxComp, kComp] = max(MetriqueComp);

if (ValMaxRaw > thresholdValueRaw) || (ValMaxComp > thresholdValueComp)
    [ARaw, Carto] = import_WaterColumn_All(All, DataDepth, DataWC, DataRaw, DataPosition, DataAttitude, ...
        DataRuntime, DataInstalParam, SonarDescription, 'Carto', Carto, 'subPing', subDepth(kRaw), 'CorrectionTVG', CorrectionTVG, ...
        'maskOverlapingBeams', maskOverlapingBeams, 'Mute', 1);
    
    [B, Carto] = import_WaterColumn_All(All, DataDepth, DataWC, DataRaw, DataPosition, DataAttitude, ...
        DataRuntime, DataInstalParam, SonarDescription, 'Carto', Carto, 'subPing', subDepth(kComp), 'CorrectionTVG', CorrectionTVG, ...
        'maskOverlapingBeams', maskOverlapingBeams, 'Mute', 1);
    [~, BComp] = compensation_WaterColumnPlus(B);
    
    [InfoImageRaw, InfoImageComp, Carto] = WCD.Process.plotSignalAndBestPing(ARaw, BComp, All.nomFic, DataName, MetriqueRaw, MetriqueComp, ...
        Carto, subDepth, params_ZoneEI, ALim, BLim, CorrectionTVG, TypeUnit, maskOverlapingBeams, ...
        DataDepth, DataRaw, DataWC, DataSeabed, DataInstalParam, DataRuntime, DataSSP, DataAttitude);
else
    my_breakpoint
end


function [flag, StatsRaw, StatsComp] = computeValuePing(All, DataDepth, DataWC, DataRaw, ...
    DataPosition, DataAttitude, DataRuntime, ...
    DataInstalParam, SonarDescription, Carto, subPing, ...
    params_ZoneEI, ALim, BLim, TypeUnit, CorrectionTVG, Quantile1, Quantile2, Title, maskOverlapingBeams, flagDisplay, varargin)

global isUsingParfor %#ok<GVMIS>
global NUMBER_OF_PROCESSORS %#ok<GVMIS>

[varargin, isUsingParfor]        = getPropertyValue(varargin, 'isUsingParfor',        isUsingParfor);
[varargin, NUMBER_OF_PROCESSORS] = getPropertyValue(varargin, 'NUMBER_OF_PROCESSORS', NUMBER_OF_PROCESSORS); %#ok<ASGLU>

StatsRaw  = [];
StatsComp = [];

A = import_WaterColumn_All(All, DataDepth, DataWC, DataRaw, DataPosition, DataAttitude, ...
    DataRuntime, DataInstalParam, SonarDescription, 'Carto', Carto, 'subPing', subPing, 'CorrectionTVG', CorrectionTVG, ...
    'maskOverlapingBeams', maskOverlapingBeams, 'Mute', 1);
if isempty(A)
    flag = 0;
    return
end

AmplitudeRaw = get_Image(A);

% [flag, RxAnglesEarth_WC] = get_RxAnglesEarth_WC(A);
% if ~flag
%     return
% end

% fe = this.Sonar.SampleBeamData.SampleRate;
% C  = this.Sonar.SampleBeamData.SoundVelocity;
% [TempsSimple, RangeInMeters] = get_RangeInMeters_WC(A, RangeDetectionInSamples, fe, C);

[~, AComp] = compensation_WaterColumnPlus(A);
AmplitudeComp = get_Image(AComp);

[flag, PatchSampleBeam] = getPatchEchoIntegrationOverSpecular(A, params_ZoneEI, ALim, BLim);
if ~flag
    return
end

if flagDisplay
    figure(95743);
    
    subplot(2,1,1);
    imagesc(AmplitudeRaw); colormap(jet(256)); colorbar;
    hold on;
    plot(PatchSampleBeam.R, 'k');
    patch('XData', PatchSampleBeam.XPatch, 'YData', PatchSampleBeam.YPatch, 'EdgeColor','r', 'FaceColor','none');
    hold off;
    title(Title, 'interpreter', 'none')
    
    subplot(2,1,2);
    imagesc(AmplitudeComp); colormap(jet(256)); colorbar;
    hold on;
    plot(PatchSampleBeam.R, 'k');
    patch('XData', PatchSampleBeam.XPatch, 'YData', PatchSampleBeam.YPatch, 'EdgeColor','r', 'FaceColor','none');
    hold off;
    title(Title, 'interpreter', 'none')
    CLim = get(gca, 'CLim');
    CLim(1) = -10;
    set(gca, 'CLim', CLim);
    
    drawnow
end

AmplitudeRaw  = AmplitudeRaw( PatchSampleBeam.subSamples, PatchSampleBeam.subBeams);
AmplitudeComp = AmplitudeComp(PatchSampleBeam.subSamples, PatchSampleBeam.subBeams);
%     figure(95744); imagesc(AmplitudeRaw); colormap(jet(256)); colorbar;
AmplitudeRaw  = AmplitudeRaw(:);
AmplitudeComp = AmplitudeComp(:);
AmplitudeRaw( isnan(AmplitudeRaw))  = [];
AmplitudeComp(isnan(AmplitudeComp)) = [];

switch TypeUnit
    case 'Amp'
        AmplitudeRaw  = reflec_dB2Amp(AmplitudeRaw);
        AmplitudeComp = reflec_dB2Amp(AmplitudeComp);
    case 'Enr'
        AmplitudeRaw  = reflec_dB2Enr(AmplitudeRaw);
        AmplitudeComp = reflec_dB2Enr(AmplitudeComp);
end

StatValuesRaw  = stats(AmplitudeRaw,  'Seuil', [Quantile1 Quantile2]); % TypeUnit
statsValueComp = stats(AmplitudeComp, 'Seuil', [Quantile1 Quantile2]);

StatsRaw.NbSamples  = length(AmplitudeComp);
StatsComp.NbSamples = length(AmplitudeComp);

StatsRaw.PingCounter  = subPing;
StatsComp.PingCounter = subPing;

switch TypeUnit
    case 'dB'
        StatsRaw.Max  = StatValuesRaw.Max;
        StatsComp.Max = statsValueComp.Max;
        
        StatsRaw.Min  = StatValuesRaw.Min;
        StatsComp.Min = statsValueComp.Min;
        
        StatsRaw.Range  = StatValuesRaw.Range;
        StatsComp.Range = statsValueComp.Range;
        
        StatsRaw.Mean  = StatValuesRaw.Moyenne;
        StatsComp.Mean = statsValueComp.Moyenne; % Mean
        
        StatsRaw.Median  = StatValuesRaw.Mediane; % Median
        StatsComp.Median = statsValueComp.Mediane;
        
        StatsRaw.Std  = StatValuesRaw.Sigma; % Std
        StatsComp.Std = statsValueComp.Sigma;
        
        if isfield(StatValuesRaw, 'Quant_x')
            StatsRaw.Quantile1  = StatValuesRaw.Quant_x(1);
            StatsComp.Quantile1 = statsValueComp.Quant_x(1);
            StatsRaw.Quantile2  = StatValuesRaw.Quant_x(2);
            StatsComp.Quantile2 = statsValueComp.Quant_x(2);
        else
            StatsRaw.Quantile1  = StatValuesRaw.Quant_01_99(1);
            StatsComp.Quantile1 = statsValueComp.Quant_01_99(1);
            StatsRaw.Quantile2  = StatValuesRaw.Quant_01_99(2);
            StatsComp.Quantile2 = statsValueComp.Quant_01_99(2);
        end
        
        StatsRaw.QuantilesRange  = StatsRaw.Quantile2  - StatsRaw.Quantile1;
        StatsComp.QuantilesRange = StatsComp.Quantile2 - StatsComp.Quantile1;
        
    case 'Amp'
        StatsRaw.Max  = reflec_Amp2dB(StatValuesRaw.Max);
        StatsComp.Max = reflec_Amp2dB(statsValueComp.Max);
        
        StatsRaw.Min  = reflec_Amp2dB(StatValuesRaw.Min);
        StatsComp.Min = reflec_Amp2dB(statsValueComp.Min);
        
        StatsRaw.Range  = StatsRaw.Max  - StatsRaw.Min;
        StatsComp.Range = StatsComp.Max - StatsComp.Min;
        
        StatsRaw.Mean  = reflec_Amp2dB(StatValuesRaw.Moyenne);
        StatsComp.Mean = reflec_Amp2dB(statsValueComp.Moyenne); % Mean
        
        StatsRaw.Median  = reflec_Amp2dB(StatValuesRaw.Mediane); % Median
        StatsComp.Median = reflec_Amp2dB(statsValueComp.Mediane);
        
        StatsRaw.Std  = (reflec_Amp2dB(StatValuesRaw.Moyenne  + StatValuesRaw.Sigma)  - reflec_Amp2dB(StatValuesRaw.Moyenne  - StatValuesRaw.Sigma))  / 2;
        if isnan(StatsRaw.Std)
            StatsRaw.Std  = (reflec_Amp2dB(StatValuesRaw.Moyenne  + StatValuesRaw.Sigma)) - reflec_Amp2dB(StatValuesRaw.Moyenne);
        end
        StatsComp.Std = (reflec_Amp2dB(statsValueComp.Moyenne + statsValueComp.Sigma) - reflec_Amp2dB(statsValueComp.Moyenne - statsValueComp.Sigma)) / 2;
        if isnan(StatsComp.Std)
            StatsComp.Std  = (reflec_Amp2dB(statsValueComp.Moyenne  + statsValueComp.Sigma)) - reflec_Amp2dB(statsValueComp.Moyenne);
        end
        
        if isfield(StatValuesRaw, 'Quant_x')
            StatsRaw.Quantile1  = reflec_Amp2dB(StatValuesRaw.Quant_x(1));
            StatsComp.Quantile1 = reflec_Amp2dB(statsValueComp.Quant_x(1));
            StatsRaw.Quantile2  = reflec_Amp2dB(StatValuesRaw.Quant_x(2));
            StatsComp.Quantile2 = reflec_Amp2dB(statsValueComp.Quant_x(2));
        else
            StatsRaw.Quantile1  = reflec_Amp2dB(StatValuesRaw.Quant_01_99(1));
            StatsComp.Quantile1 = reflec_Amp2dB(statsValueComp.Quant_01_99(1));
            StatsRaw.Quantile2  = reflec_Amp2dB(StatValuesRaw.Quant_01_99(2));
            StatsComp.Quantile2 = reflec_Amp2dB(statsValueComp.Quant_01_99(2));
        end
        
        StatsRaw.QuantilesRange  = StatsRaw.Quantile2  - StatsRaw.Quantile1;
        StatsComp.QuantilesRange = StatsComp.Quantile2 - StatsComp.Quantile1;
        
    case 'Enr'
        StatsRaw.Max  = reflec_Enr2dB(StatValuesRaw.Max);
        StatsComp.Max = reflec_Enr2dB(statsValueComp.Max);
        
        StatsRaw.Min  = reflec_Enr2dB(StatValuesRaw.Min);
        StatsComp.Min = reflec_Enr2dB(statsValueComp.Min);
        
        StatsRaw.Range  = StatsRaw.Max  - StatsRaw.Min;
        StatsComp.Range = StatsComp.Max - StatsComp.Min;
        
        StatsRaw.Mean  = reflec_Enr2dB(StatValuesRaw.Moyenne);
        StatsComp.Mean = reflec_Enr2dB(statsValueComp.Moyenne); % Mean
        
        StatsRaw.Median  = reflec_Enr2dB(StatValuesRaw.Mediane); % Median
        StatsComp.Median = reflec_Enr2dB(statsValueComp.Mediane);
        
        StatsRaw.Std  = (reflec_Enr2dB(StatValuesRaw.Moyenne  + StatValuesRaw.Sigma)  - reflec_Enr2dB(StatValuesRaw.Moyenne  - StatValuesRaw.Sigma))  / 2;
        if isnan(StatsRaw.Std)
            StatsRaw.Std  = (reflec_Enr2dB(StatValuesRaw.Moyenne  + StatValuesRaw.Sigma)) - reflec_Enr2dB(StatValuesRaw.Moyenne);
        end
        StatsComp.Std = (reflec_Enr2dB(statsValueComp.Moyenne + statsValueComp.Sigma) - reflec_Enr2dB(statsValueComp.Moyenne - statsValueComp.Sigma)) / 2;
        if isnan(StatsComp.Std)
            StatsComp.Std  = (reflec_Enr2dB(statsValueComp.Moyenne  + statsValueComp.Sigma)) - reflec_Enr2dB(statsValueComp.Moyenne);
        end
        
        if isfield(StatValuesRaw, 'Quant_x')
            StatsRaw.Quantile1  = reflec_Enr2dB(StatValuesRaw.Quant_x(1));
            StatsComp.Quantile1 = reflec_Enr2dB(statsValueComp.Quant_x(1));
            StatsRaw.Quantile2  = reflec_Enr2dB(StatValuesRaw.Quant_x(2));
            StatsComp.Quantile2 = reflec_Enr2dB(statsValueComp.Quant_x(2));
        else
            StatsRaw.Quantile1  = reflec_Enr2dB(StatValuesRaw.Quant_01_99(1));
            StatsComp.Quantile1 = reflec_Enr2dB(statsValueComp.Quant_01_99(1));
            StatsRaw.Quantile2  = reflec_Enr2dB(StatValuesRaw.Quant_01_99(2));
            StatsComp.Quantile2 = reflec_Enr2dB(statsValueComp.Quant_01_99(2));
        end
        
        StatsRaw.QuantilesRange  = StatsRaw.Quantile2  - StatsRaw.Quantile1;
        StatsComp.QuantilesRange = StatsComp.Quantile2 - StatsComp.Quantile1;
end
