% listFileNames = 'D:\Temp\SPFE\B\0000_20180307_090137_SimonStevin.all';
% listFileNames = 'D:\Temp\SPFE\B\0027_20160915_105616_SimonStevin.all';
% % listFileNames{1} = 'D:\Temp\SPFE\A\0029_20180306_213505_SimonStevin.all';
% % listFileNames{2} = 'D:\Temp\SPFE\A\0030_20180306_213905_SimonStevin.all';
% % listFileNames{3} = 'D:\Temp\SPFE\A\0031_20180306_214305_SimonStevin.all';
% nomDirCSVOut  = 'D:\Temp\SPFE\A';
% listFileNames = 'D:\Temp\SPFE\20210305\0028_20200803_121402_SimonStevin.all';
% nomDirCSVOut  = 'D:\Temp\SPFE\20210305';
% % listFileNames = 'D:\Temp\SPFE\20230324\0007_20230318_165252_Belgica.all';
% % nomDirCSVOut  = 'D:\Temp\SPFE\20230324';
% [flag, aKM] = ALL.checkFiles(listFileNames);
% DataName      = 'Quantile2';
% TypeUnit      = 'dB';
% CorrectionTVG = 30;
% Quantile1     = 1;
% Quantile2     = 99;
% ALim          = [-60  60];
% BLim          = [];
% params_ZoneEI.Where = 'ASC';
% params_ZoneEI.Seuils = [0.96 0.85 0.75];
% params_ZoneEI.Seuils = [0.96 0.85];
% params_ZoneEI.DistanceSecutity = 0;
% params_ZoneEI.DistHaut = 1;
% params_ZoneEI.DistBas = 0;
% flagComp      = 0;
% CLim          = [];
% Tag           = 'JMA4';
% maskOverlapingBeams = true;
% ALL_WC_EI_SampleBeam_SlicesSignalVsPings(aKM, DataName, TypeUnit, CorrectionTVG, Quantile1, Quantile2, ...
%   ALim, BLim, params_ZoneEI, flagComp, CLim, nomDirCSVOut, Tag, maskOverlapingBeams);

function flag = ALL_WC_EI_SampleBeam_SlicesSignalVsPings(this, DataName, TypeUnit, CorrectionTVG, Quantile1In, Quantile2In, ...
    ALim, BLim, params_ZoneEI, flagComp, CLim, nomDirCSVOut, Tag, maskOverlapingBeams, ...
    varargin)

[varargin, suby]       = getPropertyValue(varargin, 'suby',       []);
[varargin, IndNavUnit] = getPropertyValue(varargin, 'IndNavUnit', []);
[varargin, Carto]      = getPropertyValue(varargin, 'Carto',      []);%#ok<ASGLU>

%% Cr�ation de la fen�tre avec les menus et callbacks sp�cialis�s pour la nav

kFic = 0;
N = length(this);
nbWCPings = zeros(1,N);
str1 = 'Calcul de l'' EI au dessus du sp�culaire';
str2 = 'Computing EI over specular';
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
    my_waitbar(k, N, hw);
    nomFic = this(k).nomFic;

    [flag, All, DataDepth, DataRaw, DataWC, DataSeabed, DataPosition, DataAttitude, DataRuntime, DataInstalParam, DataSSP, ...
        subDepth, subRaw, subSeabed, subWC, FlagPings, Latitude, Longitude, ~, ~] = lectureGlobaleALL(nomFic, IndNavUnit, ...
        'subDepth', suby); %#ok<ASGLU> 
    if ~flag
        continue
    end
    nbWCPings(k) = DataWC.Dimensions.nbPings;

    %% Test Set DoNotDelete=false � tous les memmapfiles car bug en version //Tbx

    flagNetcdf = existCacheNetcdf(nomFic);
    if flagNetcdf
        DataDepth = setMemmapfileDoNotDelete(DataDepth, 'true');
        DataWC    = setMemmapfileDoNotDelete(DataWC,    'true');

        DataRaw = renameField(DataRaw, 'TransmitSectorNumber',      'TransmitSectorNumberRx');
        DataRaw = renameField(DataRaw, 'TwoWayTravelTimeInSeconds', 'TwoWayTravelTime');
        DataRaw = setMemmapfileDoNotDelete(DataRaw, 'true');
        DataRaw = renameField(DataRaw, 'TransmitSectorNumberRx', 'TransmitSectorNumber');
        DataRaw = renameField(DataRaw, 'TwoWayTravelTime',       'TwoWayTravelTimeInSeconds');
    end

    %% Calcul du signal

    kFic = kFic + 1;

    [ValueRaw{kFic}, ValueComp{kFic}, Time{kFic}, Carto] ...
        = EI_computeSlicesValue(All, DataWC, DataDepth, DataRaw, DataSeabed, DataAttitude, DataRuntime, ...
        DataPosition, DataSSP, DataInstalParam, ...
        subDepth, subRaw, FlagPings, params_ZoneEI, DataName, TypeUnit, CorrectionTVG, Quantile1In, Quantile2In, ...
        ALim, BLim, CLim, Carto, flagComp, Latitude, Longitude, ...
        maskOverlapingBeams); %#ok<AGROW>

    listFicAll{kFic} = nomFic; %#ok<AGROW>
    %     nbPingsWC(kFic)  = DataWC.Dimensions.nbPings; %#ok<AGROW>

    %% Plot de la nav

    MetriqueRaw  = [ValueRaw{kFic}.(DataName)];
    %     MetriqueComp = [ValueComp{kFic}.(DataName)];

    if all(isnan(MetriqueRaw))
        continue
    end

    %% Test Set DoNotDelete=false � tous les memmapfiles car bug en version //Tbx

    if flagNetcdf
        DataDepth = setMemmapfileDoNotDelete(DataDepth, 'false'); %#ok<NASGU>
        DataWC    = setMemmapfileDoNotDelete(DataWC,    'false'); %#ok<NASGU>

        DataRaw = renameField(DataRaw, 'TransmitSectorNumber',      'TransmitSectorNumberRx');
        DataRaw = renameField(DataRaw, 'TwoWayTravelTimeInSeconds', 'TwoWayTravelTime');
        DataRaw = setMemmapfileDoNotDelete(DataRaw, 'false');
        DataRaw = renameField(DataRaw, 'TransmitSectorNumberRx', 'TransmitSectorNumber');
        DataRaw = renameField(DataRaw, 'TwoWayTravelTime',       'TwoWayTravelTimeInSeconds'); %#ok<NASGU>
    end

    clear DataDepth DataRaw DataWC
    if flagNetcdf
        deleteFileOnListe(cl_memmapfile.empty, []) % N�cessaire si //Tbx ET Netcdf
    end
end
my_close(hw, 'MsgEnd')
if kFic == 0
    return
end

%% Edition des signaux

clear MetriqueRaw MetriqueComp

nbSlices = size(ValueRaw{1},2);
for k=1:length(ValueRaw)
    for kSlice=1:nbSlices
        [~, Title] = fileparts(this(k).nomFic);
        Title = sprintf('%s - Signal %d - %s - Slice %d-%d %%', Title, k, DataName, int32(params_ZoneEI.Seuils([kSlice kSlice+1]) * 100));
        kSig = (k-1) * nbSlices + kSlice;

        MetriqueRawSlice  = [ValueRaw{k}(:,kSlice).(DataName)];
        MetriqueCompSlice = [ValueComp{k}(:,kSlice).(DataName)];

        T = Time{k}; % TODO pour image en background
        xSampleRaw(kSig)   = XSample('name', 'Pings', 'data', T);         %#ok<AGROW>
        xSampleComp(kSig)  = XSample('name', 'Pings', 'data', T);         %#ok<AGROW>

        ySampleRaw(kSig)   = YSample('name', Title, 'data', MetriqueRawSlice,  'marker', '.'); %#ok<AGROW> %, 'marker', '.');
        ySampleComp(kSig)  = YSample('name', Title, 'data', MetriqueCompSlice, 'marker', '.'); %#ok<AGROW> %, 'marker', '.');
    end
end
if isempty(Tag)
    NameSignalRaw  = [DataName '-Raw'];
    NameSignalComp = [DataName '-Comp'];
else
    NameSignalRaw  = [DataName '-' Tag '-Raw'];
    NameSignalComp = [DataName '-' Tag '-Comp'];
end

signalRaw  = ClSignal('Name', NameSignalRaw,  'xSample', xSampleRaw,  'ySample', ySampleRaw);
signalRaw.plot;

if flagComp
    signalComp = ClSignal('Name', NameSignalComp, 'xSample', xSampleComp, 'ySample', ySampleComp);
    signalComp.plot;
end

%% Export des signaux

nbSlices = length(params_ZoneEI.Seuils) - 1;
VarNames = {'PingCounter'; 'Latitude'; 'Longitude'; 'Speed'; 'Acceleration'; 'Heading'; 'PingDirection'; 'Depth'; 'Bathy'};
for kSlice=1:nbSlices
    strBottom         = sprintf('Bottom_%d', kSlice);
    strTop            = sprintf('Top_%d', kSlice);
    strNbSamples      = sprintf('NbSamples_%d', kSlice);
    strMax            = sprintf('Max_%d', kSlice);
    strMin            = sprintf('Min_%d', kSlice);
    strRange          = sprintf('Range_%d', kSlice);
    strMean           = sprintf('Mean_%d', kSlice);
    strMedian         = sprintf('Median_%d', kSlice);
    strStd            = sprintf('Std_%d', kSlice);
    strQuantile1      = sprintf('Quantile1_%d', kSlice);
    strQuantile2      = sprintf('Quantile2_%d', kSlice);
    strQuantilesRange = sprintf('QuantilesRange_%d', kSlice);
    VarNames = [VarNames; {strBottom; strTop; strNbSamples; strMax; strMin; strRange; strMean; strMedian; strStd; strQuantile1; strQuantile2; strQuantilesRange}]; %#ok<AGROW>
end

if ~isempty(nomDirCSVOut)
    N = length(ValueRaw);
    str1 = 'Export des .csv';
    str2 = 'Export the .csv files';
    hw = create_waitbar(Lang(str1,str2), 'N', N);
    for k=1:N
        clear NbSamples Max Min Range Mean Median Std Quantile1 Quantile2 QuantilesRange PingCounter Latitude Longitude Speed Acceleration Heading Depth Bathy SeuilBottom SeuilTop
        %         nomFicSeul = sprintf('Signal%d', k);
        [~, nomFicSeul] = fileparts(this(k).nomFic);

        if isempty(Tag)
            nomFicWithTag = sprintf('%s-WC-EI-Raw', nomFicSeul);
        else
            nomFicWithTag = sprintf('%s-%s-WC-EI-Raw', nomFicSeul, Tag);
        end
        nomFicRawCsv{k} = fullfile(nomDirCSVOut, [nomFicWithTag '.csv']); %#ok<AGROW>

        for kSlice=nbSlices:-1:1
            NbSamples(kSlice,:)      = [ValueRaw{k}(:,kSlice).NbSamples];
            Max(kSlice,:)            = [ValueRaw{k}(:,kSlice).Max];
            Min(kSlice,:)            = [ValueRaw{k}(:,kSlice).Min];
            Range(kSlice,:)          = [ValueRaw{k}(:,kSlice).Range];
            Mean(kSlice,:)           = [ValueRaw{k}(:,kSlice).Mean];
            Median(kSlice,:)         = [ValueRaw{k}(:,kSlice).Median];
            Std(kSlice,:)            = [ValueRaw{k}(:,kSlice).Std];
            Quantile1(kSlice,:)      = [ValueRaw{k}(:,kSlice).Quantile1]; % TODO : en principe param�tre unique ! Peut-�tre supprim� de l'export par slice
            Quantile2(kSlice,:)      = [ValueRaw{k}(:,kSlice).Quantile2];
            QuantilesRange(kSlice,:) = [ValueRaw{k}(:,kSlice).QuantilesRange];

            PingCounter     = [ValueRaw{k}(:,kSlice).PingCounter];
            Latitude        = [ValueRaw{k}(:,kSlice).Latitude];
            Longitude       = [ValueRaw{k}(:,kSlice).Longitude];
            Speed           = [ValueRaw{k}(:,kSlice).Speed];
            Acceleration    = [ValueRaw{k}(:,kSlice).Acceleration];
            Heading         = [ValueRaw{k}(:,kSlice).Heading]; % TODO : existe d�j� : controler
            Depth           = [ValueRaw{k}(:,kSlice).Depth];
            Bathy           = [ValueRaw{k}(:,kSlice).Bathy];

            Speed           = single(Speed);
            Acceleration    = single(Acceleration);
            Heading         = single(Heading);
            Depth           = single(Depth);
            Bathy           = single(Bathy);

%             SeuilBottom(kSlice,:) = repmat(single(params_ZoneEI.Seuils(1)), 1, length(Bathy));
%             SeuilTop(kSlice,:)    = repmat(single(params_ZoneEI.Seuils(2)), 1, length(Bathy));
            SeuilBottom(kSlice,:) = repmat(single(params_ZoneEI.Seuils(kSlice)),   1, length(Bathy));
            SeuilTop(kSlice,:)    = repmat(single(params_ZoneEI.Seuils(kSlice+1)), 1, length(Bathy));

            Datetime = Time{k};
            PingOrientation = mod(Heading + 90, 360);
        end

        [~, OriginFileName, Ext] = fileparts(listFicAll{k});
        flag = WCD.Process.exportEISignalsInCsvFile(nomFicRawCsv{k}, Datetime, PingCounter, ...
            Latitude, Longitude, Speed, Acceleration, Heading, PingOrientation, Depth, Bathy, ...
            SeuilBottom, SeuilTop, NbSamples, Max, Min, Range, Mean, Median, Std, Quantile1, Quantile2, QuantilesRange, ...
            'VarNames', VarNames, 'OriginFileName', [OriginFileName Ext]);
        if ~flag
            return
        end

        if flagComp
            if isempty(Tag)
                nomFicWithTag = sprintf('%s-WC-EI-Comp', nomFicSeul);
            else
                nomFicWithTag = sprintf('%s-%s-WC-EI-Comp', nomFicSeul, Tag);
            end

            nomFicCompCsv{k} = fullfile(nomDirCSVOut, [nomFicWithTag '.csv']); %#ok<AGROW>

            for kSlice=nbSlices:-1:1
                NbSamples(kSlice,:)      = [ValueComp{k}(:,kSlice).NbSamples];
                Max(kSlice,:)            = [ValueComp{k}(:,kSlice).Max];
                Min(kSlice,:)            = [ValueComp{k}(:,kSlice).Min];
                Range(kSlice,:)          = [ValueComp{k}(:,kSlice).Range];
                Mean(kSlice,:)           = [ValueComp{k}(:,kSlice).Mean];
                Median(kSlice,:)         = [ValueComp{k}(:,kSlice).Median];
                Std(kSlice,:)            = [ValueComp{k}(:,kSlice).Std];
                Quantile1(kSlice,:)      = [ValueComp{k}(:,kSlice).Quantile1]; % TODO : en principe param�tre unique ! Peut-�tre supprim� de l'export par slice
                Quantile2(kSlice,:)      = [ValueComp{k}(:,kSlice).Quantile2];
                QuantilesRange(kSlice,:) = [ValueComp{k}(:,kSlice).QuantilesRange];
            end

            [~, OriginFileName, Ext] = fileparts(listFicAll{k});
            flag = WCD.Process.exportEISignalsInCsvFile(nomFicCompCsv{k}, Datetime, PingCounter, ...
                Latitude, Longitude, Speed, Acceleration, Heading, PingOrientation, Depth, Bathy, ...
                SeuilBottom, SeuilTop, NbSamples, Max, Min, Range, Mean, Median, Std, Quantile1, Quantile2, QuantilesRange, ...
                'VarNames', VarNames, 'OriginFileName', [OriginFileName Ext]);
            if ~flag
                return
            end
        end
        my_waitbar(k, N, hw)
    end
    my_close(hw, 'MsgEnd')

    %% Regroupement de tous les fichiers en un seul

    if isempty(Tag)
        nomFicWithTag = sprintf('AllFiles-WC-EI-Raw');
    else
        nomFicWithTag = sprintf('AllFiles-%s-WC-EI-Raw', Tag);
    end
    nomFicOut = fullfile(nomDirCSVOut, [nomFicWithTag '.csv']);
    appendFiles(nomFicRawCsv, nomFicOut, 'NbHeaders', 1);

    if flagComp
        if isempty(Tag)
            nomFicWithTag = sprintf('AllFiles-WC-EI-Comp');
        else
            nomFicWithTag = sprintf('AllFiles-%s-WC-EI-Comp', Tag);
        end
        nomFicOut = fullfile(nomDirCSVOut, [nomFicWithTag '.csv']);
        appendFiles(nomFicCompCsv, nomFicOut, 'NbHeaders', 1);
    end

    %% Ecriture du fichier d�crivant les param�tres utilis�s pour l'�cho-int�gration

    if isempty(Quantile1In)
        Quantile1In = 1;
    end
    if isempty(Quantile2In)
        Quantile2In = 99;
    end
    if isempty(ALim)
        ALim = [-Inf Inf];
    end
    if isempty(BLim)
        BLim = [1 Inf];
    end

    if isempty(Tag)
        nomFicWithTag = sprintf('%s-WC-EI', nomFicSeul);
    else
        nomFicWithTag = sprintf('%s-%s-WC-EI', nomFicSeul, Tag);
    end
    nomFicTxt = fullfile(nomDirCSVOut, [nomFicWithTag '.txt']);
    fid = fopen(nomFicTxt, 'w+');
    fprintf(fid, 'Parameters used for the generation of statistics :\n\n');
    fprintf(fid, 'Quantile1      : %s\n', num2str(Quantile1In));
    fprintf(fid, 'Quantile2      : %s\n', num2str(Quantile2In));
    fprintf(fid, 'Angular limits : %s\n', num2strCode(ALim));
    fprintf(fid, 'Beam limits    : %s\n', num2strCode(BLim));
    fprintf(fid, 'Thresholds     : %s\n', num2strCode(params_ZoneEI.Seuils));
    fprintf(fid, '\nUnit used to compute the mean value : %s\n', TypeUnit);
    fclose(fid);
end


function [ValueRaw, ValueComp, Time, Carto, SonarDescription] ...
    = EI_computeSlicesValue(All, DataWC, DataDepth, DataRaw, DataSeabed, DataAttitude, DataRuntime, ...
    DataPosition, DataSSP, DataInstalParam, ...
    subDepth, subRaw, FlagPings, params_ZoneEI, DataName, TypeUnit, CorrectionTVG, Quantile1, Quantile2, ...
    ALim, BLim, CLim, Carto, flagComp, Latitude, Longitude, maskOverlapingBeams)

useParallel = get_UseParallelTbx;

Time = DataDepth.Datetime(subDepth);
[Heading, speed, ~, acceleration] = calCapFromLatLon(Latitude, Longitude, 'Time', Time);

nbSlices = length(params_ZoneEI.Seuils) - 1;

TransducerDepth = -abs(DataDepth.TransducerDepth(subDepth,:)); % Modif JMA le 25/11/2020 (�changes de mail avec Herv�)
Depth = DataDepth.Depth(subDepth,:);
BeamPointingAngle = DataRaw.BeamPointingAngle(subRaw,:);
BeamPointingAngle = Simrad_AnglesAntenna2ShipFloor(BeamPointingAngle, DataInstalParam, DataInstalParam.isDual);

% Mask  = DataDepth.Mask(subDepth,:);
Mask = (BeamPointingAngle < -10) | (BeamPointingAngle > 10);
clear BeamPointingAngle
Depth(Mask) = NaN;
Bathy = Depth + TransducerDepth(:,1);
Depth = mean(Depth, 2, 'omitnan');
Bathy = mean(Bathy, 2, 'omitnan');

N = length(subDepth);

ValueRaw(N,nbSlices) = WCD.Process.StatsEmpty(NaN, NaN);
ValueComp = ValueRaw;

%% Lecture de InstallationParameters

[~, ~, isDual] = read_installationParameters_bin(All);

%% Lecture du ping de WC

[flag, IdentSonar] = SimradModel2IdentSonar(DataWC.EmModel, DataDepth.EmModel, 'isDual', isDual);
if ~flag
    return
end
SonarDescription = cl_sounder('Sonar.Ident', IdentSonar, 'Sonar.Mode_1', 1, ...
    'Sonar.SystemSerialNumber', DataWC.SystemSerialNumber);

[~, Carto] = import_WaterColumn_All(All, DataDepth, DataWC, DataRaw, DataPosition, DataAttitude, ...
    DataRuntime, DataInstalParam, SonarDescription, 'Carto', Carto, 'subPing', subDepth(1), 'CorrectionTVG', CorrectionTVG, ...
    'maskOverlapingBeams', maskOverlapingBeams, 'Mute', 1);

[~, nomFicSeul] = fileparts(All.nomFic);
str1 = sprintf('Calcul de l'' EI au dessus du sp�culaire pour %s', nomFicSeul);
str2 = sprintf('Computing EI over specular for %s', nomFicSeul);
if useParallel && (N > 1)
    % && ~isdeployed % TODO : ~isdeployed tant que pb pas r�solu par MathWorks
    [hw, DQ] = create_parforWaitbar(N, Lang(str1,str2));
    try
        parfor (k=1:N, useParallel)
            subPing = subDepth(k);
            if FlagPings(k)
                %             Title = sprintf('%s : %d / %d', [nomFicSeul, ext], k, N);
                [flag, StatsRaw, StatsComp] = computeValuePing(All, DataDepth, DataWC, DataRaw, ...
                    DataPosition, DataAttitude, DataRuntime, ...
                    DataInstalParam, SonarDescription, Carto, subPing, ...
                    params_ZoneEI, ALim, BLim, TypeUnit, CorrectionTVG, Quantile1, Quantile2, maskOverlapingBeams);
                if flag
                    for kSlice=1:nbSlices
                        StatsRaw(kSlice).Speed         = speed(k);
                        StatsRaw(kSlice).Acceleration  = acceleration(k);
                        StatsRaw(kSlice).Latitude      = Latitude(k);
                        StatsRaw(kSlice).Longitude     = Longitude(k);
                        StatsRaw(kSlice).Heading       = Heading(k);
                        StatsRaw(kSlice).Depth         = Depth(k);
                        StatsRaw(kSlice).Bathy         = Bathy(k);

                        StatsComp(kSlice).Speed        = speed(k);
                        StatsComp(kSlice).Acceleration = acceleration(k);
                        StatsComp(kSlice).Latitude     = Latitude(k);
                        StatsComp(kSlice).Longitude    = Longitude(k);
                        StatsComp(kSlice).Heading      = Heading(k);
                        StatsComp(kSlice).Depth        = Depth(k);
                        StatsComp(kSlice).Bathy        = Bathy(k);

                        ValueRaw(k,kSlice)  = StatsRaw(kSlice);
                        ValueComp(k,kSlice) = StatsComp(kSlice);
                    end
                else
                    for kSlice=1:nbSlices
                        ValueRaw(k,kSlice)  = WCD.Process.StatsEmpty(Latitude(k), Longitude(k));
                        ValueComp(k,kSlice) = WCD.Process.StatsEmpty(Latitude(k), Longitude(k));
                    end
                end
            else
                for kSlice=1:nbSlices
                    ValueRaw(k,kSlice)  = WCD.Process.StatsEmpty(Latitude(k), Longitude(k));
                    ValueComp(k,kSlice) = WCD.Process.StatsEmpty(Latitude(k), Longitude(k));
                end
            end
            send(DQ, k);
        end
    catch ME
        fprintf('The Parallel Computing Toolbox did NOT work here : isdeployed=%d\n', isdeployed);
        fprintf('------------- ME.message --------------------\n');
        fprintf('%s\n', ME.message);
        fprintf('---------------------------------\n');
        report = getReport(ME, 'extended', 'hyperlinks', 'off');
        fprintf('------------- getReport(ME) --------------------\n');
        fprintf('%s\n', report);
        fprintf('---------------------------------\n');
        my_close(hw, 'MsgEnd', 'TimeDelay', 60*5)
        return
    end
    my_close(hw, 'MsgEnd', 'TimeDelay', 60*5)
else
    hw = create_waitbar(Lang(str1,str2), 'N', N);
    for k=1:N
        subPing = subDepth(k);
        if FlagPings(k)
            %             Title = sprintf('%s : %d / %d', [nomFicSeul, ext], k, N);
            [flag, StatsRaw, StatsComp] = computeValuePing(All, DataDepth, DataWC, DataRaw, ...
                DataPosition, DataAttitude, DataRuntime, ...
                DataInstalParam, SonarDescription, Carto, subPing, ...
                params_ZoneEI, ALim, BLim, TypeUnit, CorrectionTVG, Quantile1, Quantile2, maskOverlapingBeams);
            if flag
                for kSlice=1:nbSlices
                    StatsRaw(kSlice).Speed         = speed(k);
                    StatsRaw(kSlice).Acceleration  = acceleration(k);
                    StatsRaw(kSlice).Latitude      = Latitude(k);
                    StatsRaw(kSlice).Longitude     = Longitude(k);
                    StatsRaw(kSlice).Heading       = Heading(k);
                    StatsRaw(kSlice).Depth         = Depth(k);
                    StatsRaw(kSlice).Bathy         = Bathy(k);

                    StatsComp(kSlice).Speed        = speed(k);
                    StatsComp(kSlice).Acceleration = acceleration(k);
                    StatsComp(kSlice).Latitude     = Latitude(k);
                    StatsComp(kSlice).Longitude    = Longitude(k);
                    StatsComp(kSlice).Heading      = Heading(k);
                    StatsComp(kSlice).Depth        = Depth(k);
                    StatsComp(kSlice).Bathy        = Bathy(k);

                    ValueRaw(k,kSlice)  = StatsRaw(kSlice);
                    ValueComp(k,kSlice) = StatsComp(kSlice);
                end
            else
                for kSlice=1:nbSlices
                    ValueRaw(k,kSlice)  = WCD.Process.StatsEmpty(Latitude(k), Longitude(k));
                    ValueComp(k,kSlice) = WCD.Process.StatsEmpty(Latitude(k), Longitude(k));
                end
            end
        else
            for kSlice=1:nbSlices
                ValueRaw(k,kSlice)  = WCD.Process.StatsEmpty(Latitude(k), Longitude(k));
                ValueComp(k,kSlice) = WCD.Process.StatsEmpty(Latitude(k), Longitude(k));
            end
        end
        my_waitbar(k, N, hw)
    end
    my_close(hw, 'MsgEnd', 'TimeDelay', 60*5)
end

%% R�cup�ration des signaux

for kSlice=1:nbSlices
    MetriqueRaw(:,kSlice)  = [ValueRaw(:,kSlice).(DataName)];  %#ok<AGROW>
    MetriqueComp(:,kSlice) = [ValueComp(:,kSlice).(DataName)]; %#ok<AGROW>
end

%% Affichage d�tection sur Raw data

flag = WCD.Process.displaySlicesSignal(MetriqueRaw, DataName, All.nomFic, 'Raw', params_ZoneEI, ALim, CorrectionTVG, ...
    TypeUnit, maskOverlapingBeams, DataDepth, DataRaw, DataWC, DataSeabed, DataInstalParam, DataRuntime, DataSSP, DataAttitude, ...
    subDepth, 'CLim', CLim);
if ~flag
    return
end

%% Affichage d�tection sur Comp data

if flagComp
    flag = WCD.Process.displaySlicesSignal(MetriqueComp, DataName, All.nomFic, 'Comp', params_ZoneEI, ALim, CorrectionTVG, ...
        TypeUnit, maskOverlapingBeams, DataDepth, DataRaw, DataWC, DataSeabed, DataInstalParam, DataRuntime, DataSSP, DataAttitude, subDepth);
    if ~flag
        return
    end
end



function [flag, StatsRawSlices, StatsCompSlices] = computeValuePing(All, DataDepth, DataWC, DataRaw, ...
    DataPosition, DataAttitude, DataRuntime, ...
    DataInstalParam, SonarDescription, Carto, subPing, ...
    params_ZoneEI, ALim, BLim, TypeUnit, CorrectionTVG, Quantile1, Quantile2, maskOverlapingBeams, varargin)

global isUsingParfor %#ok<GVMIS>
global NUMBER_OF_PROCESSORS %#ok<GVMIS>

[varargin, isUsingParfor]        = getPropertyValue(varargin, 'isUsingParfor',        isUsingParfor);
[varargin, NUMBER_OF_PROCESSORS] = getPropertyValue(varargin, 'NUMBER_OF_PROCESSORS', NUMBER_OF_PROCESSORS); %#ok<ASGLU>

StatsRawSlices  = [];
StatsCompSlices = [];

A = import_WaterColumn_All(All, DataDepth, DataWC, DataRaw, DataPosition, DataAttitude, ...
    DataRuntime, DataInstalParam, SonarDescription, 'Carto', Carto, 'subPing', subPing, 'CorrectionTVG', CorrectionTVG, ...
    'maskOverlapingBeams', maskOverlapingBeams, 'Mute', 1);
if isempty(A)
    flag = 0;
    return
end

AmplitudeRaw = get_Image(A);

% [flag, RxAnglesEarth_WC] = get_RxAnglesEarth_WC(A);
% if ~flag
%     return
% end

% fe = this.Sonar.SampleBeamData.SampleRate;
% C  = this.Sonar.SampleBeamData.SoundVelocity;
% [TempsSimple, RangeInMeters] = get_RangeInMeters_WC(A, RangeDetectionInSamples, fe, C);

[~, AComp] = compensation_WaterColumnPlus(A);
AmplitudeComp = get_Image(AComp);

nbSlices = length(params_ZoneEI.Seuils) - 1;
for kSlice=1:nbSlices
    params_ZoneEISlice = params_ZoneEI;
    params_ZoneEISlice.Seuils = params_ZoneEISlice.Seuils(kSlice:(kSlice+1));
    [flag, PatchSampleBeam] = getSlicesPatchEchoIntegrationOverSpecular(A, params_ZoneEISlice, ALim, BLim);
    if ~flag
        return
    end

    AmplitudeRawSlice  = AmplitudeRaw( PatchSampleBeam.subSamples, PatchSampleBeam.subBeams);
    AmplitudeCompSlice = AmplitudeComp(PatchSampleBeam.subSamples, PatchSampleBeam.subBeams);
    %     figure(95744); imagesc(AmplitudeRawSlice); colormap(jet(256)); colorbar;
    AmplitudeRawSlice  = AmplitudeRawSlice(:);
    AmplitudeCompSlice = AmplitudeCompSlice(:);
    AmplitudeRawSlice( isnan(AmplitudeRawSlice))  = [];
    AmplitudeCompSlice(isnan(AmplitudeCompSlice)) = [];

    switch TypeUnit
        case 'Amp'
            AmplitudeRawSlice  = reflec_dB2Amp(AmplitudeRawSlice);
            AmplitudeCompSlice = reflec_dB2Amp(AmplitudeCompSlice);
        case 'Enr'
            AmplitudeRawSlice  = reflec_dB2Enr(AmplitudeRawSlice);
            AmplitudeCompSlice = reflec_dB2Enr(AmplitudeCompSlice);
    end

    StatValuesRaw  = stats(AmplitudeRawSlice,  'Seuil', [Quantile1 Quantile2]); % TypeUnit
    statsValueComp = stats(AmplitudeCompSlice, 'Seuil', [Quantile1 Quantile2]);

    StatsRaw.NbSamples  = length(AmplitudeCompSlice);
    StatsComp.NbSamples = length(AmplitudeCompSlice);

    StatsRaw.PingCounter  = subPing;
    StatsComp.PingCounter = subPing;

    switch TypeUnit
        case 'dB'
            StatsRaw.Max  = StatValuesRaw.Max;
            StatsComp.Max = statsValueComp.Max;

            StatsRaw.Min  = StatValuesRaw.Min;
            StatsComp.Min = statsValueComp.Min;

            StatsRaw.Range  = StatValuesRaw.Range;
            StatsComp.Range = statsValueComp.Range;

            StatsRaw.Mean  = StatValuesRaw.Moyenne;
            StatsComp.Mean = statsValueComp.Moyenne; % Mean

            StatsRaw.Median  = StatValuesRaw.Mediane; % Median
            StatsComp.Median = statsValueComp.Mediane;

            StatsRaw.Std  = StatValuesRaw.Sigma; % Std
            StatsComp.Std = statsValueComp.Sigma;

            if isfield(StatValuesRaw, 'Quant_x')
                StatsRaw.Quantile1  = StatValuesRaw.Quant_x(1);
                StatsComp.Quantile1 = statsValueComp.Quant_x(1);
                StatsRaw.Quantile2  = StatValuesRaw.Quant_x(2);
                StatsComp.Quantile2 = statsValueComp.Quant_x(2);
            else
                StatsRaw.Quantile1  = StatValuesRaw.Quant_01_99(1);
                StatsComp.Quantile1 = statsValueComp.Quant_01_99(1);
                StatsRaw.Quantile2  = StatValuesRaw.Quant_01_99(2);
                StatsComp.Quantile2 = statsValueComp.Quant_01_99(2);
            end

            StatsRaw.QuantilesRange  = StatsRaw.Quantile2  - StatsRaw.Quantile1;
            StatsComp.QuantilesRange = StatsComp.Quantile2 - StatsComp.Quantile1;

        case 'Amp'
            StatsRaw.Max  = reflec_Amp2dB(StatValuesRaw.Max);
            StatsComp.Max = reflec_Amp2dB(statsValueComp.Max);

            StatsRaw.Min  = reflec_Amp2dB(StatValuesRaw.Min);
            StatsComp.Min = reflec_Amp2dB(statsValueComp.Min);

            StatsRaw.Range  = StatsRaw.Max  - StatsRaw.Min;
            StatsComp.Range = StatsComp.Max - StatsComp.Min;

            StatsRaw.Mean  = reflec_Amp2dB(StatValuesRaw.Moyenne);
            StatsComp.Mean = reflec_Amp2dB(statsValueComp.Moyenne); % Mean

            StatsRaw.Median  = reflec_Amp2dB(StatValuesRaw.Mediane); % Median
            StatsComp.Median = reflec_Amp2dB(statsValueComp.Mediane);

            StatsRaw.Std  = (reflec_Amp2dB(StatValuesRaw.Moyenne  + StatValuesRaw.Sigma)  - reflec_Amp2dB(StatValuesRaw.Moyenne  - StatValuesRaw.Sigma))  / 2;
            if isnan(StatsRaw.Std)
                StatsRaw.Std  = (reflec_Amp2dB(StatValuesRaw.Moyenne  + StatValuesRaw.Sigma)) - reflec_Amp2dB(StatValuesRaw.Moyenne);
            end
            StatsComp.Std = (reflec_Amp2dB(statsValueComp.Moyenne + statsValueComp.Sigma) - reflec_Amp2dB(statsValueComp.Moyenne - statsValueComp.Sigma)) / 2;
            if isnan(StatsComp.Std)
                StatsComp.Std  = (reflec_Amp2dB(statsValueComp.Moyenne  + statsValueComp.Sigma)) - reflec_Amp2dB(statsValueComp.Moyenne);
            end

            if isfield(StatValuesRaw, 'Quant_x')
                StatsRaw.Quantile1  = reflec_Amp2dB(StatValuesRaw.Quant_x(1));
                StatsComp.Quantile1 = reflec_Amp2dB(statsValueComp.Quant_x(1));
                StatsRaw.Quantile2  = reflec_Amp2dB(StatValuesRaw.Quant_x(2));
                StatsComp.Quantile2 = reflec_Amp2dB(statsValueComp.Quant_x(2));
            else
                StatsRaw.Quantile1  = reflec_Amp2dB(StatValuesRaw.Quant_01_99(1));
                StatsComp.Quantile1 = reflec_Amp2dB(statsValueComp.Quant_01_99(1));
                StatsRaw.Quantile2  = reflec_Amp2dB(StatValuesRaw.Quant_01_99(2));
                StatsComp.Quantile2 = reflec_Amp2dB(statsValueComp.Quant_01_99(2));
            end

            StatsRaw.QuantilesRange  = StatsRaw.Quantile2  - StatsRaw.Quantile1;
            StatsComp.QuantilesRange = StatsComp.Quantile2 - StatsComp.Quantile1;

        case 'Enr'
            StatsRaw.Max  = reflec_Enr2dB(StatValuesRaw.Max);
            StatsComp.Max = reflec_Enr2dB(statsValueComp.Max);

            StatsRaw.Min  = reflec_Enr2dB(StatValuesRaw.Min);
            StatsComp.Min = reflec_Enr2dB(statsValueComp.Min);

            StatsRaw.Range  = StatsRaw.Max  - StatsRaw.Min;
            StatsComp.Range = StatsComp.Max - StatsComp.Min;

            StatsRaw.Mean  = reflec_Enr2dB(StatValuesRaw.Moyenne);
            StatsComp.Mean = reflec_Enr2dB(statsValueComp.Moyenne); % Mean

            StatsRaw.Median  = reflec_Enr2dB(StatValuesRaw.Mediane); % Median
            StatsComp.Median = reflec_Enr2dB(statsValueComp.Mediane);

            StatsRaw.Std  = (reflec_Enr2dB(StatValuesRaw.Moyenne  + StatValuesRaw.Sigma)  - reflec_Enr2dB(StatValuesRaw.Moyenne  - StatValuesRaw.Sigma))  / 2;
            if isnan(StatsRaw.Std)
                StatsRaw.Std  = (reflec_Enr2dB(StatValuesRaw.Moyenne  + StatValuesRaw.Sigma)) - reflec_Enr2dB(StatValuesRaw.Moyenne);
            end
            StatsComp.Std = (reflec_Enr2dB(statsValueComp.Moyenne + statsValueComp.Sigma) - reflec_Enr2dB(statsValueComp.Moyenne - statsValueComp.Sigma)) / 2;
            if isnan(StatsComp.Std)
                StatsComp.Std  = (reflec_Enr2dB(statsValueComp.Moyenne  + statsValueComp.Sigma)) - reflec_Enr2dB(statsValueComp.Moyenne);
            end

            if isfield(StatValuesRaw, 'Quant_x')
                StatsRaw.Quantile1  = reflec_Enr2dB(StatValuesRaw.Quant_x(1));
                StatsComp.Quantile1 = reflec_Enr2dB(statsValueComp.Quant_x(1));
                StatsRaw.Quantile2  = reflec_Enr2dB(StatValuesRaw.Quant_x(2));
                StatsComp.Quantile2 = reflec_Enr2dB(statsValueComp.Quant_x(2));
            else
                StatsRaw.Quantile1  = reflec_Enr2dB(StatValuesRaw.Quant_01_99(1));
                StatsComp.Quantile1 = reflec_Enr2dB(statsValueComp.Quant_01_99(1));
                StatsRaw.Quantile2  = reflec_Enr2dB(StatValuesRaw.Quant_01_99(2));
                StatsComp.Quantile2 = reflec_Enr2dB(statsValueComp.Quant_01_99(2));
            end

            StatsRaw.QuantilesRange  = StatsRaw.Quantile2  - StatsRaw.Quantile1;
            StatsComp.QuantilesRange = StatsComp.Quantile2 - StatsComp.Quantile1;
    end
    if kSlice == 1
        StatsRawSlices  = StatsRaw;
        StatsCompSlices = StatsComp;
    else
        StatsRawSlices(kSlice)  = StatsRaw; %#ok<AGROW>
        StatsCompSlices(kSlice) = StatsComp; %#ok<AGROW>
    end
end
