function ALL_WC_ExportRawData23DV(this, nomDirOut, TypeDataWC, Tag, MaskBeyondDetection, varargin)

global isUsingParfor %#ok<GVMIS>

[varargin, CLimRaw]            = getPropertyValue(varargin, 'CLimRaw',        []);
[varargin, suby]               = getPropertyValue(varargin, 'suby',           []);
[varargin, flagTideCorrection] = getPropertyValue(varargin, 'TideCorrection', 0);
[varargin, typeAlgoWC]         = getPropertyValue(varargin, 'typeAlgoWC',     1); %#ok<ASGLU>

useParallel = get_UseParallelTbx;

%{
Carto = [];

N = length(FileList);
str1 = 'Traitement de la colonne d''eau Kongsberg en géométrie SampleBeam';
str2 = 'Kongsberg Water Column processing in SampleBeam geometry';
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
my_waitbar(k, N, hw)
[flag, Carto] = ALL_WC_ExportRawData23DV_unitaire(FileList{k}, nomDirOut, TypeDataWC, Tag, Carto, ...
MaskBeyondDetection, CLimRaw, suby, flagTideCorrection);
if ~flag
%         return
end
end
my_close(hw, 'MsgEnd');
%}

isUsingParfor = 0;

%%

N = length(this);
Carto = [];
str1 = 'Traitement de la colonne d''eau Kongsberg en géométrie SampleBeam';
str2 = 'Kongsberg Water Column processing in SampleBeam geometry';
if useParallel && (N > 1)
    % Récupération de Carto
    nomDir = fileparts(this(1).nomFic);
    [flag, Carto] = test_ExistenceCarto_Folder(nomDir);
    if ~flag || isempty(Carto)
        [~, Carto] = get_Layer(this(1), 'Depth', 'Carto', Carto);
    end
    
    % Lancement du traitement avec la // Tbx
    [hw, DQ] = create_parforWaitbar(N, Lang(str1,str2));
    parfor (k=1:N, useParallel)
        ALL_WC_ExportRawData23DV_unitaire(this(k), nomDirOut, TypeDataWC, Tag, Carto, ...
            MaskBeyondDetection, CLimRaw, suby, flagTideCorrection, typeAlgoWC, 'isUsingParfor', true);
        send(DQ, k);
    end
    my_close(hw, 'MsgEnd')
else
    hw = create_waitbar(Lang(str1,str2), 'N', N);
    for k=1:N
        my_waitbar(k, N, hw)
        [~, Carto] = ALL_WC_ExportRawData23DV_unitaire(this(k), nomDirOut, TypeDataWC, Tag, Carto, ...
            MaskBeyondDetection, CLimRaw, suby, flagTideCorrection, typeAlgoWC);
    end
    my_close(hw, 'MsgEnd');
end


function [flag, Carto] = ALL_WC_ExportRawData23DV_unitaire(this, nomDirOut, TypeDataWC, Tag, Carto, ...
    MaskBeyondDetection, CLimRaw, suby, flagTideCorrection, typeAlgoWC, varargin)

global isUsingParfor %#ok<GVMIS>

[varargin, isUsingParfor] = getPropertyValue(varargin, 'isUsingParfor', isUsingParfor); %#ok<ASGLU>

[~, nomFic] = fileparts(this.nomFic);

if any(TypeDataWC == 1)
    if isempty(Tag)
        Names.nomFicXmlRaw = fullfile(nomDirOut, [nomFic '_Raw_SampleBeam.xml']);
    else
        Names.nomFicXmlRaw = fullfile(nomDirOut, [nomFic '_' Tag '_Raw_SampleBeam.xml']);
    end
    nomFicXml = Names.nomFicXmlRaw;
else
    Names.nomFicXmlRaw = [];
end

if any(TypeDataWC == 2)
    if isempty(Tag)
        Names.nomFicXmlComp = fullfile(nomDirOut, [nomFic '_Comp_SampleBeam.xml']);
    else
        Names.nomFicXmlComp = fullfile(nomDirOut, [nomFic '_' Tag '_Comp_SampleBeam.xml']);
    end
    nomFicXml = Names.nomFicXmlComp;
else
    Names.nomFicXmlComp = [];
end

[~, nomFic] = fileparts(nomFicXml);
nomFic = strrep(nomFic, '_Raw_SampleBeam', '');
nomFic = strrep(nomFic, '_Raw_VoxlerBin', '');
nomFic = strrep(nomFic, '_Raw_VoxlerPng', '');
nomFic = strrep(nomFic, '_Comp_SampleBeam', '');
nomFic = strrep(nomFic, '_Comp_VoxlerBin', '');
nomFic = strrep(nomFic, '_Comp_VoxlerPng', '');
nomFic = strrep(nomFic, '_WCCubeComp', '');
nomFic = strrep(nomFic, '_WCCubeRaw', '');

Names.AreaName = nomFic;

%% Lecture de la bathy

ListeLayers = SelectionLayers_V1toV2(24);
[Bathy, Carto] = import_PingBeam_All_V2(this, 'ListeLayers', ListeLayers, 'Carto', Carto);
%  SonarScope(Bathy)

%% Création des échogrammes

flag = ALL_SimradProcessRawDataWC(this, Bathy, 'Names', Names, 'MaskBeyondDetection', MaskBeyondDetection, ...
    'CLimRaw', CLimRaw, 'suby', suby, 'TideCorrection', flagTideCorrection, ...
    'typeAlgoWC', typeAlgoWC);

