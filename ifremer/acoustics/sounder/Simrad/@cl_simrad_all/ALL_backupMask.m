function ALL_backupMask(this)

str1 = 'Remise � z�ro des masks des fichiers .all';
str2 = 'Reset masks from .all files';
N = length(this);
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
    my_waitbar(k, N, hw);
    flag = ALL_backupMask_unitaire(this(k));
    if ~flag
        return
    end
end
my_close(hw, 'MsgEnd')


function flag = ALL_backupMask_unitaire(this)

flag = existCacheNetcdf(this.nomFic);
if flag
    flag = backupMaskNetcdf(this);
else
    flag = backupMaskXMLBin(this);
end
