function ALL_exportPLY(this, LayerNames, repExport)

Carto  = [];
xShift = 0;
yShift = 0;

%% Pr�traitement des datagrammes seabedimage

flyFileName = {};
N = length(this);
str1 = 'Export des donn�es au format PTY (CloundCompare)';
str2 = 'Export data to PLY format (CloudCompare)';
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    [flag, filename, Carto, xShift, yShift] = ALL_exportPLY_unitaire(this(k), LayerNames, Carto, ...
        k==1, xShift, yShift, repExport); % GLOBAL_SHIFT n'est pas pros en compte dans winopenCloudCompare
    if flag
        flyFileName{end+1} = filename; %#ok<AGROW>
    end
end
my_close(hw, 'MsgEnd')

%% Ouverture des fichiers dans CloudCompare

% winopen(flyFileName{1}); % OK, un seul fichier
% winopen(flyFileName);    % KO : pas plusieurs fichiers

% Alternative : Lancement de l'appli en utilisant la commande dos
winopenCloudCompare(flyFileName, 'GLOBAL_SHIFT', [xShift yShift 0]); % GLOBAL_SHIFT n'est pas pros en compte dans winopenCloudCompare


function [flag, flyFileName, Carto, xShift, yShift] = ALL_exportPLY_unitaire(this, LayerNames, Carto, FirstFile, xShift, yShift, repExport)

%% Identification des layers � charger

nomsLayers{1} = 'AcrossDist';
nomsLayers{2} = 'AlongDist';
nomsLayers{3} = 'Depth+Draught-Heave';

for k=1:length(LayerNames)
    nomsLayers{end+1} = LayerNames{k}; %#ok<AGROW>
end

%% Lecture des layers

Mute  = 0;
for k=1:length(nomsLayers)
    [Layers_PingBeam(k), Carto] = get_Layer(this, nomsLayers{k}, 'Carto', Carto, 'flagTideCorrection', 1, 'Mute', Mute); %#ok<AGROW>
end

%% Calcul des coordonn�es g�ographiques

NoWaitbar = 0;
[flag, LatLong_PingBeam] = sonarCalculCoordGeo(Layers_PingBeam, 1, 'NoWaitbar', NoWaitbar, 'Mute', Mute);
if ~flag
    return
end

[flag, XY_Layers] = sonarCalculCoordXY(LatLong_PingBeam, 1);
if ~flag
    return
end

if FirstFile
    s = XY_Layers(1).StatValues;
    xShift = s.Min;
    s = XY_Layers(2).StatValues;
    yShift = s.Min;

    xShift = floor(xShift);
    yShift = floor(yShift);

    %{
    % % comment� car %d ne joue pas son r�le
    p    = ClParametre('Name', 'xShift', 'Unit', 'm', 'MinValue', -Inf, 'MaxValue', Inf, 'Value', xShift, 'format', '%d');
    p(2) = ClParametre('Name', 'yShift', 'Unit', 'm', 'MinValue', -Inf, 'MaxValue', Inf, 'Value', yShift, 'format', '%d');
    a = StyledParametreDialog('params', p, 'Title', 'Demo of ParametreDialog');
    a.sizes = [0 -2 0 0 0 -1 -1 0];
    a.openDialog;
    flag = a.okPressedOut;
    if ~flag
        return
    end
    paramsValue = a.getParamsValue;
    %}

    nomVar = {'xShift (m)'; 'yShift (m)'};
    value = {num2strPrecis(-xShift) ; num2strPrecis(-yShift)};
    [value, flag] = my_inputdlg(nomVar, value);
    if ~flag
        return
    end
    xShift = -str2double(value(1));
    yShift = -str2double(value(2));
end

%% Cr�ation du fichier .ply

[~, nomFic] = fileparts(this.nomFic);
flyFileName = fullfile(repExport, [nomFic '.ply']);
flag = sonar_ExportFly(Layers_PingBeam(4:end), XY_Layers(1),  XY_Layers(2),  Layers_PingBeam(3), flyFileName, ...
    'xShift', xShift,  'yShift', yShift);
if ~flag
    return
end
