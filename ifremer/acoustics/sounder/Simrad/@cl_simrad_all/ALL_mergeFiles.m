function repExport = ALL_mergeFiles(this, repExport)

% Filtre sur les datagrammes sur le 1er fichier fourni.
[flag, selectDatagram] = selectDatagramFromFile(this(1), 'DisplayTable', 1);
if ~flag
    return
end

% Cr�ation du fichier all merg� (avec tous les datagrammes hors WC si il en
% contenait).
merge_ALL(this, selectDatagram);
