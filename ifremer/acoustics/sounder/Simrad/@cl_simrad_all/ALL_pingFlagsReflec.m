function flag = ALL_pingFlagsReflec(this)

Carto = [];
FigPosition = [];

N = length(this);
str1 = 'Nettoyage de la réflectivité des fichiers .all';
str2 = 'Reflectivity ping cleaning of .all files';
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    fprintf('%s\n%d / %d : %s\n', repmat('-',1,100), k, N, this(k).nomFic);
    
    [flag, Carto, FigPosition] = ALL_pingFlagsReflec_unitaire(this(k), Carto, FigPosition);
    if ~flag
        break
    end
    
    %% On demande si on veut traiter les autres fichiers
    
    if k ~= N
        flag = question_ContinueNextFile();
        if ~flag
            break
        end
    end
end
my_close(hw, 'MsgEnd')


function [flag, Carto, FigPosition] = ALL_pingFlagsReflec_unitaire(this, Carto, FigPosition)

%% Lecture de Depth

[flag, DataDepth] = read_depth_bin(this, 'Memmapfile', -1);
if ~flag
    return
end

%% Lecture des layers

if isfield(DataDepth, 'MaskPingsReflectivity') && ~isempty(DataDepth.MaskPingsReflectivity)
    MaskPingsReflectivity = DataDepth.MaskPingsReflectivity(:);
else
    MaskPingsReflectivity = zeros(DataDepth.Dimensions.nbPings, 1, 'single');
end
ReflecRaw = DataDepth.ReflectivityFromSnippets(:,:);
Reflectivity = ReflecRaw;
Reflectivity(DataDepth.Mask(:,:) == 1) = NaN;
Reflectivity(DataDepth.MaskPingsReflectivity == 1,:) = NaN;

MeanValue  = mean(Reflectivity, 2, 'omitnan');
MeanValue0 = MeanValue;

if sum(~isnan(MeanValue)) < 2
    flag = 0;
    return
else
    [~, nomFicSeul] = fileparts(this.nomFic);
    ObjReflecRaw    = cl_image('Image', ReflecRaw',    'Name', [nomFicSeul ' - Raw data'], 'ColormapIndex', 2);
    ObjReflecRaw.CLim = '1%';

    ObjReflectivity = cl_image('Image', Reflectivity', 'Name', nomFicSeul, 'ColormapIndex', 2);
    ObjReflectivity.CLim = '1%';
    
    ObjReflectivity0 = ObjReflectivity;
    ObjReflectivity.Name = [nomFicSeul ' - Masked'];

%     if isempty(FigPosition) || ~ishandle(FigPosition)
%         FigPosition = figure;
%     else
%         figure(FigPosition);
%     end
%     FigPosition.WindowState = 'maximized';
%     h = get(FigPosition, 'children');
%     delete(h);
%     
%     hc(3) = subplot(3,1,3); plot(MeanValue', '-o');  c = colorbar; c.Visible = 'Off'; axis tight; title('Mean per ping'); grid on;
%     hc(2) = subplot(3,1,2); imagesc(ObjReflectivity, 'Axe', hc(2)); %, 'CLim', '1%'
%     hc(1) = subplot(3,1,1); imagesc(ObjReflecRaw,    'Axe', hc(1)); %, 'CLim', '1%'
%     linkaxes(hc, 'x');
%     
%     str1 = 'Voulez-vous nettoyer la réflectivité ?';
%     str2 = 'Do you want to clean up the reflectivity ?';
%     [rep, flag] = my_questdlg(Lang(str1,str2));
%     if ~flag
%         return
%     end
%     if rep == 2
%         return
%     end
    
    ImageName = ObjReflecRaw.Name;
    nomImage  = strtok(ImageName, ' ');
    x = 1:length(MeanValue);
    I = get_Image(ObjReflectivity);
    y = 1:size(I,1);
    CLim2 = ObjReflectivity.CLim;
    imageData2 = ClImageData('cData', I, 'xData', x, 'yData', y, 'colormapData', gray(256), 'clim', CLim2, 'name', ImageName);
    
    ySampleImage = YSample('name', nomImage,    'data', ones(1, length(x), 'single')); 
    ySample      = YSample('name', 'MeanValue', 'data', MeanValue,         'single', 'marker', '.'); 
            
    xSampleRawOne = XSample( 'name', 'Pings', 'data', x);
    signalOne     = ClSignal('Name', 'Reflectivity of ',    'xSample', xSampleRawOne, 'ySample', ySampleImage);
    signalOne(2)  = ClSignal('Name', 'Mean value per ping', 'xSample', xSampleRawOne, 'ySample', ySample);
    
    Title = 'Reflectivity data cleaning (define ping flags)';
    s = SignalDialog(signalOne, 'Title', Title, 'cleanInterpolation', 0, 'imageDataList', imageData2);%, 'waitAnswer', false);
    s.openDialog();
    
    if s.okPressedOut
        X = MeanValue;
        MeanValue = ySample.data;
        subOK = (MeanValue(:) == X(:));
        Reflectivity(~subOK,:) = NaN;
        MeanValue = mean(Reflectivity, 2, 'omitnan');    

        ObjReflectivity = cl_image('Image', Reflectivity', 'Name', 'Reflec after current data cleaning', 'ColormapIndex', 2);
        ObjReflectivity.CLim = '1%';
        
        if isempty(FigPosition) || ~ishandle(FigPosition)
            FigPosition = figure;
        else
            figure(FigPosition);
        end
        FigPosition.WindowState = 'maximized';
        h = get(FigPosition, 'children');
        delete(h);
        hc(1) = subplot(4,1,1); imagesc(ObjReflectivity0, 'Axe', hc(1)); %, 'CLim', '1%'
        hc(2) = subplot(4,1,2); plot(MeanValue0'); c = colorbar; c.Visible = 'Off'; axis tight; title('Mean per ping'); grid on;
        hc(3) = subplot(4,1,3); imagesc(ObjReflectivity,  'Axe', hc(3)); %,  'CLim', '1%'
        hc(4) = subplot(4,1,4); plot(MeanValue');  c = colorbar; c.Visible = 'Off'; axis tight; title('Mean per ping after current data cleaning'); grid on;
        linkaxes(hc, 'x');
        
        [ind, flag] = my_questdlg('Do you definitively accept this mask ?');
        if ~flag || (ind == 2)
            flag = 0;
            return
        end
    else
        return
    end
end
if all(subOK)
    return
end

MaskPingsReflectivity(~subOK) = 1;

if isfield(DataDepth, 'MaskPingsReflectivity') && ~isempty(DataDepth.MaskPingsReflectivity)
    flag = save_signal(this, 'Ssc_Depth', 'MaskPingsReflectivity', MaskPingsReflectivity);
    if ~flag
        return
    end
else
    ReflectivityFromSnippets = DataDepth.ReflectivityFromSnippets(:,:);
    for k=1:length(MaskPingsReflectivity)
        if MaskPingsReflectivity(k) == 1
            ReflectivityFromSnippets(k,:) = NaN;
        end
    end
    clear DataDepth
    flag = save_image(this, 'Ssc_Depth', 'ReflectivityFromSnippets', ReflectivityFromSnippets);
end
