function flag = ALL_pingFlagsUsingHeading(this)

Carto = [];
FigPosition = [];

N = length(this);
str1 = 'Nettoyage de la bathymétie des fichiers .all en fonction du cap';
str2 = 'Bathymetry cleaning of .all files using the heading';
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    fprintf('%s\n%d / %d : %s\n', repmat('-',1,100), k, N, this(k).nomFic);
    
    [flag, Carto, FigPosition] = ALL_pingFlagsUsingHeading_unitaire(this(k), Carto, FigPosition);
    if ~flag
        break
    end
    
    %% On demande si on veut traiter les autres fichiers
    
    if k ~= N
        flag = question_ContinueNextFile();
        if ~flag
            break
        end
    end
end
my_close(hw, 'MsgEnd')


function [flag, Carto, FigPosition] = ALL_pingFlagsUsingHeading_unitaire(this, Carto, FigPosition)

[flag, DataDepth] = read_depth_bin(this, 'Memmapfile', -1);
if ~flag
    return
end

%% Lecture des layers

if isfield(DataDepth, 'MaskPingsBathy') && ~isempty(DataDepth.MaskPingsBathy)
    MaskPingsBathy = DataDepth.MaskPingsBathy(:);
else
    MaskPingsBathy = zeros(DataDepth.Dimensions.nbPings, 1, 'single');
end
BathyRaw = DataDepth.Depth(:,:);
Bathy = BathyRaw;
Bathy(DataDepth.Mask(:,:) == 1) = NaN;
Bathy(DataDepth.MaskPingsBathy == 1,:) = NaN;

Heading  = DataDepth.Heading;
Heading0 = Heading;

if sum(~isnan(DataDepth.MaskPingsBathy(:,1))) < 2
    flag = 0;
    return
else
    [~, nomFicSeul] = fileparts(this.nomFic);
    ObjBathyRaw = cl_image('Image', BathyRaw', 'Name', [nomFicSeul ' - Raw data'], 'ColormapIndex', 3);
    ObjBathyRaw.CLim = '1%';

    ObjBathy = cl_image('Image', Bathy', 'Name', nomFicSeul, 'ColormapIndex', 3);
    ObjBathy.CLim = '1%';
    
    ObjBathy0 = ObjBathy;
    ObjBathy.Name = [nomFicSeul ' - Masked'];

%     if isempty(FigPosition) || ~ishandle(FigPosition)
%         FigPosition = figure;
%     else
%         figure(FigPosition);
%     end
%     FigPosition.WindowState = 'maximized';
%     h = get(FigPosition, 'children');
%     delete(h);
%     
%     hc(3) = subplot(3,1,3); plot(Heading', '-o'); c = colorbar; c.Visible = 'Off'; axis tight; title('Heading'); grid on;
%     hc(2) = subplot(3,1,2); imagesc(ObjBathy,    'Axe', hc(2)); %, 'CLim', '1%'
%     hc(1) = subplot(3,1,1); imagesc(ObjBathyRaw, 'Axe', hc(1)); %, 'CLim', '1%'
%     linkaxes(hc, 'x');
%     
%     str1 = 'Voulez-vous masquer les pings en fonction du cap ?';
%     str2 = 'Do you want to flag the pings according to the heading ?';
%     [rep, flag] = my_questdlg(Lang(str1,str2));
%     if ~flag
%         return
%     end
%     if rep == 2
%         return
%     end
    
    ImageName = ObjBathyRaw.Name;
    nomImage = strtok(ImageName, ' ');
    x = 1:length(Heading);
    I = get_Image(ObjBathy);
    y = 1:size(I,1);
    CLim2 = ObjBathy.CLim;
    imageData2 = ClImageData('cData', I, 'xData', x, 'yData', y, 'colormapData', jet(256), 'clim', CLim2, 'name', ImageName);
    
    ySampleImage = YSample('name', nomImage,  'data', ones(1,length(x), 'single'), 'Unit', 'm'); 
    ySample      = YSample('name', 'Heading', 'data', Heading,          'single', 'marker', '.', 'Unit', 'deg'); 
            
    xSampleRawOne = XSample( 'name', 'Pings', 'data', x);
    signalOne     = ClSignal('Name', 'Bathy of ',           'xSample', xSampleRawOne, 'ySample', ySampleImage);
    signalOne(2)  = ClSignal('Name', 'Mean value per ping', 'xSample', xSampleRawOne, 'ySample', ySample);
    
    Title = 'Bathy data cleaning (define ping flags)';
    s = SignalDialog(signalOne, 'Title', Title, 'cleanInterpolation', 0, 'imageDataList', imageData2);%, 'waitAnswer', false);
    s.openDialog();
    
    if s.okPressedOut
        X = Heading;
        Heading = ySample.data;
        subOK = (Heading(:) == X(:));
        Bathy(~subOK,:) = NaN;
%         MeanValue = nanmean(Bathy, 2);    

        ObjBathy = cl_image('Image', Bathy', 'Name', 'Data after curentr data cleaning', 'ColormapIndex', 3);
        ObjBathy.CLim = '1%';
        
        if isempty(FigPosition) || ~ishandle(FigPosition)
            FigPosition = figure;
        else
            figure(FigPosition);
        end
        FigPosition.WindowState = 'maximized';
        h = get(FigPosition, 'children');
        delete(h);
        hc(1) = subplot(4,1,1); imagesc(ObjBathy0, 'Axe', hc(1)); %, 'CLim', '1%'
        hc(2) = subplot(4,1,2); plot(Heading0'); c = colorbar; c.Visible = 'Off'; axis tight; title('Heading'); grid on;
        hc(3) = subplot(4,1,3); imagesc(ObjBathy,  'Axe', hc(3)); %,  'CLim', '1%'
        hc(4) = subplot(4,1,4); plot(Heading');  c = colorbar; c.Visible = 'Off'; axis tight; title('Heading after current data cleaning'); grid on;
        linkaxes(hc, 'x');
        
        [ind, flag] = my_questdlg('Do you definitively accept this mask ?');
        if ~flag || (ind == 2)
            flag = 0;
            return
        end
    else
        return
    end
end
if all(subOK)
    return
end

MaskPingsBathy(~subOK) = 1;

if isfield(DataDepth, 'MaskPingsBathy') && ~isempty(DataDepth.MaskPingsBathy)
    flag = save_signal(this, 'Ssc_Depth', 'MaskPingsBathy', MaskPingsBathy);
    if ~flag
        return
    end
% else
%     BathyFromSnippets = DataDepth.BathyFromSnippets(:,:);
%     for k=1:length(MaskPingsBathy)
%         if MaskPingsBathy(k) == 1
%             BathyFromSnippets(k,:) = NaN;
%         end
%     end
%     clear DataDepth
%     flag = save_image(this, 'Ssc_Depth', 'BathyFromSnippets', BathyFromSnippets);
end
