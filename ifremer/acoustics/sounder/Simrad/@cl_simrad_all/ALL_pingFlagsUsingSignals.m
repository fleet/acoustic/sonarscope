function flag = ALL_pingFlagsUsingSignals(this, layerName, signalNames)

Carto = [];
FigPosition = [];

N = length(this);
str1 = 'Nettoyage de la bathym�trie des fichiers .all';
str2 = 'Bathymetry cleaning of .all files';
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    fprintf('%s\n%d / %d : %s\n', repmat('-',1,100), k, N, this(k).nomFic);
    
    [flag, Carto, FigPosition] = ALL_pingFlagsUsingSignals_unitaire(this(k), Carto, FigPosition, layerName, signalNames);
    if ~flag
        break
    end
    
    %% On demande si on veut traiter les autres fichiers
    
    if k ~= N
        flag = question_ContinueNextFile();
        if ~flag
            break
        end
    end
end
my_close(hw, 'MsgEnd')


function [flag, Carto, FigPosition] = ALL_pingFlagsUsingSignals_unitaire(this, Carto, FigPosition, layerName, signalNames)

%% Lecture de DataDepth

[flag, DataDepth] = read_depth_bin(this, 'Memmapfile', -1);
if ~flag
    return
end
TDatagramDepth = DataDepth.Datetime;

%% Lecture du layer

Layer = get_Layer(this, layerName, 'CartoAuto', 1);
if isempty(Layer)
    flag = 0;
    return
end
TLayer        = get_SonarDatetime(Layer);
ColormapIndex = Layer.ColormapIndex;
ColorMap      = Layer.Colormap;

%% Intersection des temps

[subLayer, subDatagram] = intersect3(TLayer, TDatagramDepth, TDatagramDepth);
if isempty(subLayer)
    flag = 0;
    return
end

DepthRaw = get_Image(Layer);
DepthRaw = DepthRaw(subLayer,:);

MaskPingsBathy = DataDepth.MaskPingsBathy(subDatagram);

Depth = DepthRaw;

Depth(DataDepth.Mask(subDatagram,:) == 1)           = NaN;
Depth(DataDepth.MaskPingsBathy(subDatagram) == 1,:) = NaN;

%% Cr�ation de l'instance SignalDialog

for kSignal=1:numel(signalNames)
    if strcmp(signalNames{kSignal}, 'Vertical profile')
        signalValue{kSignal} = mean(Depth, 2, 'omitnan'); %#ok<AGROW>
        Unit{kSignal} = Layer.Unit; %#ok<AGROW>
    else
        [flag, X, Unit{kSignal}] = getValSignalIfAny(Layer, signalNames{kSignal}, 'suby', subLayer); %#ok<AGROW>
        if ~flag
            return
        end
        signalValue{kSignal} = X; %#ok<AGROW>
    end
    signalValue{kSignal}(MaskPingsBathy == 1) = NaN; %#ok<AGROW>
%     signalValueInitial{kSignal} = signalValue{kSignal}; %#ok<AGROW>
end

[~, nomFicSeul] = fileparts(this.nomFic);
ObjDepthRaw = cl_image('Image', DepthRaw', 'Name', [nomFicSeul ' - Raw data'], 'ColormapIndex', ColormapIndex);
ObjDepthRaw.CLim = '1%';

ObjDepth = cl_image('Image', Depth', 'Name', nomFicSeul, 'ColormapIndex', ColormapIndex);
ObjDepth.CLim = '1%';

ObjDepth0 = ObjDepth;
ObjDepth.Name = [nomFicSeul ' - Masked'];

ImageName = ObjDepthRaw.Name;
nomImage  = strtok(ImageName, ' ');
x = 1:length(signalValue{kSignal});
I = get_Image(ObjDepth);
y = 1:size(I,1);
CLim2 = ObjDepth.CLim;
% ColorMap = ObjDepth.Colormap;
imageData2 = ClImageData('cData', I, 'xData', x, 'yData', y, 'colormapData', ColorMap, 'clim', CLim2, 'name', ImageName);

ySampleImage  = YSample('name', nomImage, 'data', ones(1, length(x), 'single'));
xSampleRawOne = XSample( 'name', 'Pings', 'data', x);
signalOne     = ClSignal('Name', 'Depth of ', 'xSample', xSampleRawOne, 'ySample', ySampleImage);

for kSignal=1:numel(signalNames)
    ySample = YSample('name', nomImage, 'data', signalValue{kSignal}, 'single', 'marker', '.');
    signalOne(1+kSignal) = ClSignal('Name', signalNames{kSignal}, 'xSample', xSampleRawOne, 'ySample', ySample);
end

Title = 'Depth data cleaning (define ping flags)';
s = SignalDialog(signalOne, 'Title', Title, 'cleanInterpolation', 0, 'imageDataList', imageData2);%, 'waitAnswer', false);
s.openDialog();

if s.okPressedOut
    for kSignal=1:numel(signalNames)
        ySample = signalOne(1+kSignal).ySample;
        X = signalValue{kSignal};
        signalValue{kSignal} = ySample.data;
        subOK = (signalValue{kSignal}(:) == X(:));
        Depth(~subOK,:) = NaN;
    end
    
    ObjDepth = cl_image('Image', Depth', 'Name', [nomImage ' after current data cleaning'], 'ColormapIndex', ColormapIndex);
    ObjDepth.CLim = '1%';
    
    if isempty(FigPosition) || ~ishandle(FigPosition)
        FigPosition = figure;
    else
        figure(FigPosition);
    end
    FigPosition.WindowState = 'maximized';
    h = get(FigPosition, 'children');
%     Titre = [signalNames{kSignal} ' (' Unit{kSignal} ')'];
    delete(h);
    hc(1) = subplot(2,1,1); imagesc(ObjDepth0, 'Axe', hc(1)); %, 'CLim', '1%'
    hc(2) = subplot(2,1,2); imagesc(ObjDepth,  'Axe', hc(2)); %, 'CLim', '1%'
%     hc(1) = subplot(4,1,1); imagesc(ObjDepth0, 'Axe', hc(1)); %, 'CLim', '1%'
%     hc(3) = subplot(4,1,3); imagesc(ObjDepth,  'Axe', hc(3)); %, 'CLim', '1%'
%     hc(2) = subplot(4,1,2); plot(signalValueInitial{kSignal}'); c = colorbar; c.Visible = 'Off'; axis tight; title(Titre); grid on;
%     hc(4) = subplot(4,1,4); plot(signalValue{kSignal}');  c = colorbar; c.Visible = 'Off'; axis tight; title([Titre ' after current data cleaning']); grid on;
    linkaxes(hc, 'x');
    
    [ind, flag] = my_questdlg('Do you definitively accept this mask ?');
    if ~flag || (ind == 2)
        flag = 0;
        return
    end
else
    return
end

if all(subOK)
    return
end

%% Sauvegarde du mask dans le cache

MaskPingsBathy(~subOK) = 1;

DataDepth.MaskPingsBathy(subDatagram) = MaskPingsBathy;

if isfield(DataDepth, 'MaskPingsBathy') && ~isempty(DataDepth.MaskPingsBathy)
    flag = save_signal(this, 'Ssc_Depth', 'MaskPingsBathy', DataDepth.MaskPingsBathy);
    if ~flag
        return
    end
else
    % TODO
    %{
    Depth = DataDepth.Depth(:,:);
    for k=1:length(MaskPingsBathy)
        if MaskPingsBathy(k) == 1
            Depth(k,:) = NaN;
        end
    end
    clear DataDepth
    flag = save_image(this, 'Ssc_Depth', 'Depth', Depth);
    %}
end
