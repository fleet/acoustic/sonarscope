function ALL_resetPingsFlagsReflec(this, varargin)

str1 = 'Remise � z�ro des masks de r�flectivit� des fichiers .all en cours.';
str2 = 'Reset Ping flags of Reflectivity from .all files';
N = length(this);
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
    my_waitbar(k, N, hw);
    flag = ALL_resetPingsFlagsReflec_unitaire(this(k));
    if ~flag
        continue
    end
end
my_close(hw, 'MsgEnd')


function flag = ALL_resetPingsFlagsReflec_unitaire(this)

%% Lecture de DataDepth

[flag, DataDepth] = read_depth_bin(this, 'Memmapfile', -1);
if ~flag
    return
end

%% Nettoyage de la r�flectivit�

if isfield(DataDepth, 'MaskPingsReflectivity') && ~isempty(DataDepth.MaskPingsReflectivity)
    nbPings = DataDepth.Dimensions.nbPings;
    clear DataDepth
    flag = save_signal(this, 'Ssc_Depth', 'MaskPingsReflectivity', zeros(nbPings, 1, 'single'));
    if ~flag
        return
    end
else
    if all(DataDepth.MaskPingsReflectivity(:) ~= 0)
        str1 = 'D�sol� ce fichier a �t� import� avant que les flags de r�flectivit� par pings aient �t� introduits dans SonarScope, le nettoyage que vous avez fait est irr�versible. Il ne vous reste plus qu''� supprimer le r�pertoire correspondant � ce fichier dans le r�pertoire SonarScope si vous voulez vraiment annuler le masque que vous avez fait.';
        str2 = 'Sorry, this file has been imported before the introduction of masks on pings for reflectivity, it is impossible to restore the reflectivity. If you really want to restore the original reflectivity you have to delete the sorresponding directory to thr file in the SonarScope directory.';
        str1 = sprintf('%s\n\nNom du fichier : %s', str1, this.nomFic);
        str2 = sprintf('%s\n\nFile name : %s', str2, this.nomFic);
        my_warndlg(Lang(str1,str2), 1);
    end
end
