function ALL_restoreMask(this)

str1 = 'Restauration des masks des fichiers .all';
str2 = 'Restore masks from .all files';
N = length(this);
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
    my_waitbar(k, N, hw);
    flag = ALL_restoreMask_unitaire(this(k));
    if ~flag
    end
end
my_close(hw, 'MsgEnd')


function flag = ALL_restoreMask_unitaire(this)

flag = existCacheNetcdf(this.nomFic);
if flag
    flag = restoreMaskNetcdf(this);
else
    flag = restoreMaskXMLBin(this);
end
