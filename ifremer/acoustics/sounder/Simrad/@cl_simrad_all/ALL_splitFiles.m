function repExport = ALL_splitFiles(this, NomFicPoints, repImport, repExport)

logFileId = getLogFileId;

[flag, NbPings, SplitTime] = get_sonarTimeFromLogFile(NomFicPoints, repImport, repExport);
if ~flag
    return
end

N = length(this);
str1 = 'D�coupe des .all.';
str2 = 'Split the .all files.';
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
    my_waitbar(k, N, hw);
    
    msg = sprintf('Begin %s', this(k).nomFic);
    logFileId.info('ALL_splitFiles', msg);
    
    if k == 1
        [flag, selectDatagram] = selectDatagramFromFile(this(k), 'DisplayTable', 0, 'TimeOnly', 1);
        if ~flag
            return
        end
    end
    % En mode Split, on conserve tous les datagrammes.
    nomFicOut = saucissonne_bin(this(k), NbPings, SplitTime, selectDatagram, 'TimeOnly', 1); %#ok<NASGU>
%     for k2=1:length(nomFicOut)
%         fprintf(fid, '%s\n', nomFicOut{k2});
%     end
    
    msg = sprintf('End %s', this(k).nomFic);
    logFileId.info('ALL_splitFiles', msg);
end
% fclose(fid);
my_close(hw, 'MsgEnd')
