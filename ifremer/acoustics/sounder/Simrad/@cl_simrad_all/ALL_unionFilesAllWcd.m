function repExport = ALL_unionFilesAllWcd(this, repExport)

% TODO : sortir cet question de cette fonction

nomFicListe = fullfile(repExport, 'ListUnionALLandWCDFiles.txt');
str1 = 'Nom du fichier donnant la liste des nouveaux fichiers .all sectionnés';
str2 = 'Give the name to the file that will collect the full names of the splitted .all files';
[flag, nomFicListe] = my_uiputfile('*.txt', Lang(str1,str2), nomFicListe);
if ~flag
    return
end
repExport = fileparts(nomFicListe);

%% Processing

fid = fopen(nomFicListe, 'w+');
N = length(this);
str1 = 'Union des fichiers .all et .wcd';
str2 = 'Union of .all and .wcd files';
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k1=1:N
    my_waitbar(k1, N, hw)
    if contains(this(k).nomFic, '_uniondAllAndWcd.all')
        continue
    end
    [flag, selectDatagram] = selectDatagramFromFile(this(k), 'DisplayTable', 0);
    if flag
        nomFicOut = union_WCDandALL(this(k), selectDatagram);
        fprintf(fid, '%s\n', nomFicOut);
    end
end
my_close(hw, 'MsgEnd')
fclose(fid);
