function flag = ALL_writeAbsorptionCoefficientUser(this, LayerAbsorptionCoeff, flagLayerModifie)

flag = existCacheNetcdf(this.nomFic);
if flag
    flag = ALL_writeAbsorptionCoefficientUserNetcdf(this, LayerAbsorptionCoeff, flagLayerModifie);
else
    flag = ALL_writeAbsorptionCoefficientUserXMLBin(this, LayerAbsorptionCoeff, flagLayerModifie);
end
