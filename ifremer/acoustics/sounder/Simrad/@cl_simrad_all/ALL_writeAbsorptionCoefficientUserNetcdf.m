function flag = ALL_writeAbsorptionCoefficientUserNetcdf(this, AbsorptionCoefficientUser, flagLayerModifie)

if flagLayerModifie == 1
    [nomDir, nomFic] = fileparts(this.nomFic);
    nomFicNc = fullfile(nomDir, 'SonarScope', [nomFic '.nc']);
    if exist(nomFicNc, 'file')
        try
            ncID = netcdf.open(nomFicNc, 'WRITE');
            grpID = netcdf.inqNcid(ncID, 'Depth');
            varID = netcdf.inqVarID(grpID, 'AbsorptionCoefficientUser');
            NetcdfUtils.putVarVal(grpID, varID, AbsorptionCoefficientUser, 1)
            flag = 1;
        catch
            flag = 0;
            messageErreurFichier(nomFicNc, 'WriteFailure');
        end
        netcdf.close(ncID);
    end
else
    str1 = sprintf('Le fichier "%s" n''this pas �t� impact� par ce traitement.', this.nomFic);
    str2 = sprintf('File "%s" was not set by this processing.', this.nomFic);
    my_warndlg(Lang(str1,str2), 0, 'Tag', 'AbsorptionCoeffUserNotSet');
    flag = 1;
end
