function flag = ALL_writeAbsorptionCoefficientUserXMLBin(this, LayerAbsorptionCoeff, flagLayerModifie)

if flagLayerModifie == 1
    [nomDir, nomFicSeul] = fileparts(this.nomFic);
    nomDirRacine = fullfile(nomDir, 'SonarScope', nomFicSeul);
    
    Info.Storage  = 'single';
    Info.FileName = fullfile('Ssc_Depth','AbsorptionCoefficientUser.bin');
    flag = writeImage(nomDirRacine, Info, LayerAbsorptionCoeff);
    if ~flag
        return
    end
else
    str1 = sprintf('Le fichier "%s" n''this pas �t� impact� par ce traitement.', this.nomFic);
    str2 = sprintf('File "%s" was not set by this processing.', this.nomFic);
    my_warndlg(Lang(str1,str2), 0, 'Tag', 'AbsorptionCoeffUserNotSet');
    flag = 1;
end
