function flag = BackupRestoreSignals(this, nomSignals, Sens)

useParallel = get_UseParallelTbx;
logFileId   = getLogFileId;
% Sens = 1=Backup, 2=restore

if ~iscell(nomSignals)
    nomSignals = {nomSignals};
end

N = length(this);
str1 = 'Sauvegarde des signaux';
str2 = 'Backuping signals';
if useParallel && (N > 1)
    [hw, DQ] = create_parforWaitbar(N, Lang(str1,str2));
    logFileIdHere = logFileId;
    parfor (k=1:N, useParallel)
%         fprintf('File %d/%d : %s\n', iFic, NbFic, this(iFic).nomFic);
        msg = sprintf('File %d/%d : %s', k, N, this(k).nomFic);
        logFileIdHere.info('BackupRestoreSignals', msg); %#ok<PFBNS>

        flagHere = BackupSignals_unitaire(this(k), Sens, nomSignals);
        if ~flagHere
            msg = sprintf('Failed for file %s', this(k).nomFic);
            logFileIdHere.warn('BackupRestoreSignals', msg);
        end
        
        send(DQ, k);
    end
    my_close(hw, 'MsgEnd')
    flag = 1;
else
    hw = create_waitbar(Lang(str1,str2), 'N', N);
    for k=1:N
        my_waitbar(k, N, hw)
        
        msg = sprintf('File %d/%d : %s', k, N, this(k).nomFic);
        logFileId.info('BackupRestoreSignals', msg);
        
        flag = BackupSignals_unitaire(this(k), Sens, nomSignals);
        if ~flag
            msg = sprintf('Failed for file %s', this(k).nomFic);
            logFileId.warn('BackupRestoreSignals', msg);
        end
    end
    my_close(hw, 'MsgEnd')
end


function flag = BackupSignals_unitaire(this, Sens, nomSignals)

%% Backup de la navigation

sub = find(strcmp(nomSignals, 'Navigation'));
if ~isempty(sub)
    flag = BackupSignalsNavigation_unitaire(this, Sens);
    if ~flag
        return
    end
end
nomSignals(sub) = [];

%% Backup de l'attitude

sub = find(strcmp(nomSignals, 'Attitude'));
if ~isempty(sub)
    flag = BackupSignalsAttitude_unitaire(this, Sens);
    if ~flag
        return
    end
end
nomSignals(sub) = [];

%% Si il n'y a plus de signal � backuper on sort

if isempty(nomSignals)
    return
end

%% Backup des autres signaux de SSc_Depth

flag = BackupSignalsDepth_unitaire(this, Sens, nomSignals);
if ~flag
    return
end


function flag = BackupSignalsDepth_unitaire(this, Sens, nomSignals)

[flag, XML] = SSc_ReadDatagrams(this.nomFic, 'Ssc_Depth', 'XMLOnly', 1);
if ~flag
    return
end

[nomDir, nom] = fileparts(this.nomFic);
nomDirRacine = fullfile(nomDir, 'SonarScope', nom);

for k=1:length(nomSignals)
    kSignal = find(strcmp({XML.Signals.Name}, nomSignals{k}));
    if isempty(kSignal)
        str1 = sprintf('Signal "%s" non trouv� dans "Depth"', nomSignals{k});
        str2 = sprintf('Signal "%s" not found in "Depth"', nomSignals{k});
        my_warndlg(Lang(str1,str2), 1);
        return
    end
    
    if Sens == 1 % Backup
        flag = BackupOriginalSignal(nomDirRacine, XML.Signals(kSignal), 'Ssc_Depth');
    else % Restore
        flag = RestoreOriginalSignal(nomDirRacine, XML.Signals(kSignal), 'Ssc_Depth');
    end
    
    if ~flag
        str1 = sprintf('Le signal "%s" n''a pas pu �tre sauv� pour "%s".',  nomSignals{k}, nom);
        str2 = sprintf('Signal "%s" could not be saved for "%s".',  nomSignals{k}, nom);
        my_warndlg(Lang(str1,str2), 1);
    end
end


function flag = BackupSignalsNavigation_unitaire(this, Sens)

[flag, XML] = SSc_ReadDatagrams(this.nomFic, 'Ssc_Position', 'XMLOnly', 1);
if ~flag
    return
end

[nomDir, nom] = fileparts(this.nomFic);
nomFicXml = fullfile(nomDir, 'SonarScope', nom, 'Ssc_Position.xml');
nomDirRacine = fullfile(nomDir, 'SonarScope', nom);

nomSignals={'Latitude'; 'Longitude'; 'Time'; 'PingCounter'; 'Speed'; 'CourseVessel'; 'Heading'}; %  Quality  QualityIndicator NumberSatellitesUsed HorizontalDilutionOfPrecision AltitudeOfIMU
for k=1:length(nomSignals)
    kSignal = find(strcmp({XML.Signals.Name}, nomSignals{k}));
    if isempty(kSignal)
        str1 = sprintf('Signal "%s" non trouv� dans "%s', nomSignals{k}, nomFicXml);
        str2 = sprintf('Signal "%s" not found in "%s', nomSignals{k}, nomFicXml);
        my_warndlg(Lang(str1,str2), 1);
        return
    end
    
    if Sens == 1 % Backup
        flag = BackupOriginalSignal(nomDirRacine, XML.Signals(kSignal), 'Ssc_Position');
    else % Restore
        flag = RestoreOriginalSignal(nomDirRacine, XML.Signals(kSignal), 'Ssc_Position');
    end
    
    if ~flag
        str1 = sprintf('Le signal "%s" n''a pas pu �tre sauv� pour "%s".',  nomSignals{k}, nom);
        str2 = sprintf('Signal "%s" could not be saved for "%s".',  nomSignals{k}, nom);
        my_warndlg(Lang(str1,str2), 1);
        return
    end
end


function flag = BackupSignalsAttitude_unitaire(this, Sens)

[flag, XML] = SSc_ReadDatagrams(this.nomFic, 'Ssc_Attitude', 'XMLOnly', 1);
if ~flag
    return
end

[nomDir, nom] = fileparts(this.nomFic);
nomFicXml = fullfile(nomDir, 'SonarScope', nom, 'Ssc_Attitude.xml');
nomDirRacine = fullfile(nomDir, 'SonarScope', nom);

nomSignals={'Roll'; 'Pitch'; 'Heave'; 'Heading'; 'PingCounter'; 'Time'}; %  NbCycles SensorSystemDescriptor  SensorStatus
for k=1:length(nomSignals)
    kSignal = find(strcmp({XML.Signals.Name}, nomSignals{k}));
    if isempty(kSignal)
        str1 = sprintf('Signal "%s" non trouv� dans "%s', nomSignals{k}, nomFicXml);
        str2 = sprintf('Signal "%s" not found in "%s', nomSignals{k}, nomFicXml);
        my_warndlg(Lang(str1,str2), 1);
        return
    end
    
    if Sens == 1 % Backup
        flag = BackupOriginalSignal(nomDirRacine, XML.Signals(kSignal), 'Ssc_Attitude');
    else % Restore
        flag = RestoreOriginalSignal(nomDirRacine, XML.Signals(kSignal), 'Ssc_Attitude');
    end
    
    if ~flag
        str1 = sprintf('Le signal "%s" n''a pas pu �tre sauv� pour "%s".',  nomSignals{k}, nom);
        str2 = sprintf('Signal "%s" could not be saved for "%s".',  nomSignals{k}, nom);
        my_warndlg(Lang(str1,str2), 1);
        return
    end
end
