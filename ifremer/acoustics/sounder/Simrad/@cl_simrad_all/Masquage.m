% Masquage de toutes les donn�es (Depth,AcrossDist, ... )
%
% Syntax
%  DataDepth = Masquage(this, DataDepth, ...)
% 
% Input Arguments 
%   this      : Instance
%   DataDepth : Struture contenant les donn�es
%
% Name-only Arguments
%   MasqueDetection : Masquage par le masque d�duit de la d�tection en amplitude ou phase
%   MasqueDepth     : Masquage par le masque d�duit de l'analyse de la bathy
%
% Output Arguments 
%   DataDepth : Struture contenant les donn�es
%
% Examples
%   [flag, DataDepth] = read_depth_bin(a);
%   figure; imagesc(DataDepth.Depth); colorbar
%   DataDepth = Masquage(this, DataDepth, , 'MasqueDetection', 'MasqueDepth');
%   figure; imagesc(DataDepth.Depth); colorbar
%
% See also cl_simrad_all cl_simrad_all Authors
% Authors : JMA
% -------------------------------------------------------------------------

function DataDepth = Masquage(this, DataDepth, varargin)

[varargin, MasqueDetection] = getFlag(varargin, 'MasqueDetection');
if MasqueDetection
    Masque = read_masque(this);
    DataDepth.Depth                 = DataDepth.Depth                .* Masque.MasqueDetection;
    DataDepth.AcrossDist            = DataDepth.AcrossDist           .* Masque.MasqueDetection;
    DataDepth.AlongDist             = DataDepth.AlongDist            .* Masque.MasqueDetection;
    DataDepth.LengthDetection       = DataDepth.LengthDetection      .* Masque.MasqueDetection;
    DataDepth.QualityFactor         = DataDepth.QualityFactor        .* Masque.MasqueDetection;
    DataDepth.BeamIBA               = DataDepth.BeamIBA              .* Masque.MasqueDetection;
    DataDepth.DetectionInfo         = DataDepth.DetectionInfo        .* Masque.MasqueDetection;
    DataDepth.RealTimeCleaningInfo  = DataDepth.RealTimeCleaningInfo .* Masque.MasqueDetection;
    DataDepth.Reflectivity          = DataDepth.Reflectivity         .* Masque.MasqueDetection;
end

[varargin, MasqueDepth] = getFlag(varargin, 'MasqueDepth'); %#ok<ASGLU>
if MasqueDepth
    Masque = read_masque(this);
    if ~isempty(Masque)
        Masque.MasqueDepth(Masque.MasqueDepth == 0) = NaN;
        DataDepth.Depth                 = DataDepth.Depth                .* Masque.MasqueDepth;
        DataDepth.AcrossDist            = DataDepth.AcrossDist           .* Masque.MasqueDepth;
        DataDepth.AlongDist             = DataDepth.AlongDist            .* Masque.MasqueDepth;
        DataDepth.LengthDetection       = DataDepth.LengthDetection      .* Masque.MasqueDepth;
        DataDepth.QualityFactor         = DataDepth.QualityFactor        .* Masque.MasqueDepth;
        DataDepth.BeamIBA               = DataDepth.BeamIBA              .* Masque.MasqueDepth;
        DataDepth.DetectionInfo         = DataDepth.DetectionInfo        .* Masque.MasqueDepth;
        DataDepth.RealTimeCleaningInfo  = DataDepth.RealTimeCleaningInfo .* Masque.MasqueDepth;
        DataDepth.Reflectivity          = DataDepth.Reflectivity         .* Masque.MasqueDepth;
    end
end
