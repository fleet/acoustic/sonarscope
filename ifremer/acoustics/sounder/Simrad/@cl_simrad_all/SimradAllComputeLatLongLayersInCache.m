function flag = SimradAllComputeLatLongLayersInCache(this, createLatLong, createGeoYX)

logFileId = getLogFileId;
Carto     = [];
Mute      = 0;

% TODO : Saisir IndNavUnit avant d'entrer dans la fonction
[flag, IndNavUnit] = params_ALL_IndexNavigationSystem(this(1));
if ~flag
    return
end

%% Traitement des fichiers

str1 = 'Calcul des layers de coordonnées géographiques dans le cache des fichiers .all';
str2 = 'Computing the geographic coordinates into the cache of the .all files';
N = length(this);
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    msg = sprintf('%d / %d : %s', k, N, this(k).nomFic);
    logFileId.info('SimradAllComputeLatLongLayersInCache', msg);
    
    [flag, Carto] = SimradAllComputeLatLongLayersInCache_unitaire(this(k), createLatLong, createGeoYX, IndNavUnit, Carto, Mute);
    if ~flag
        my_close(hw, 'MsgEnd', 'TimeDelay', 60);
        return
    end
end
my_close(hw, 'MsgEnd', 'TimeDelay', 60);


function [flag, Carto] = SimradAllComputeLatLongLayersInCache_unitaire(this, createLatLong, createGeoYX, IndNavUnit, Carto, Mute)

DataTypes(1)  = cl_image.indDataType('AcrossDist');
nomsLayers{1} = 'AcrossDist';
DataTypes(2)  = cl_image.indDataType('AlongDist');
nomsLayers{2} = 'AlongDist';

%% get layers processed as the current image

[flag, Layers_PingBeam, ~, ~, Carto] = get_layers(this, nomsLayers, DataTypes(1), DataTypes, ...
    [], [], Carto, [], [], 'IndNavUnit', IndNavUnit, 'Mute', Mute);
if ~flag
    return
end

%% Calcul des coordonnées géographiques

indImageLayer = 1;
[flag, LatLong_PingBeam] = sonarCalculCoordGeo(Layers_PingBeam, indImageLayer, 'NoWaitbar', Mute, 'Mute', Mute);
if ~flag
    return
end

%% Sauvegarde des coordonnées géographiques dans le cache

if createLatLong
    X = get_Image(LatLong_PingBeam(1));
    flag = createNewLayerInNetcdfFile(this, X, 'Depth', 'Latitude', 'double', 'deg');
    if ~flag
        return
    end
    
    X = get_Image(LatLong_PingBeam(2));
    flag = createNewLayerInNetcdfFile(this, X, 'Depth', 'Longitude', 'double', 'deg');
    if ~flag
        return
    end
end

%% Calcul des coordonnées métriques

if ~createGeoYX
    return
end

[flag, GeoYX_PingBeam] = sonarCalculCoordXY(Layers_PingBeam, 1);
if ~flag
    return
end

%% Sauvegarde des coordonnées métriques dans le cache

X = get_Image(GeoYX_PingBeam(1));
flag = createNewLayerInNetcdfFile(this, X, 'Depth', 'GeoX', 'double', 'deg');
if ~flag
    return
end

X = get_Image(GeoYX_PingBeam(2));
flag = createNewLayerInNetcdfFile(this, X, 'Depth', 'GeoY', 'double', 'deg');
if ~flag
    return
end