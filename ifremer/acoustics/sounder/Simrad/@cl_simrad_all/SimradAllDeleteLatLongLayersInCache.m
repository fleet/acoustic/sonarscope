function flag = SimradAllDeleteLatLongLayersInCache(this, createLatLong, createGeoYX)

flag = 0;

logFileId = getLogFileId;

%% Traitement des fichiers

str1 = 'Effacement des layers de coordonnées géographiques dans le cache des fichiers .all';
str2 = 'Delete the geographic coordinates into the cache of the .all files';
N = length(this);
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    msg = sprintf('%d / %d : %s', k, N, this(k).nomFic);
    logFileId.info('SimradAllDeleteLatLongLayersInCache', msg);
    
    flag = SimradAllDeleteLatLongLayersInCache_unitaire(this(k), createLatLong, createGeoYX);
    if ~flag
        break
    end
end
my_close(hw, 'MsgEnd', 'TimeDelay', 60);


function flag = SimradAllDeleteLatLongLayersInCache_unitaire(this, createLatLong, createGeoYX)

S0 = cl_simrad_all([]);

%% Sauvegarde des coordonnées géographiques dans le cache

if createLatLong
    flag = disableLayerInNetcdfFile(S0, this.nomFic, 'Depth', 'Latitude');
    if ~flag
        return
    end
    
    flag = disableLayerInNetcdfFile(S0, this.nomFic, 'Depth', 'Longitude');
    if ~flag
        return
    end
end

%% Sauvegarde des coordonnées métriques dans le cache

if createGeoYX
    flag = disableLayerInNetcdfFile(S0, this.nomFic, 'Depth', 'GeoX');
    if ~flag
        return
    end
    
    flag = disableLayerInNetcdfFile(S0, this.nomFic, 'Depth', 'GeoY');
    if ~flag
        return
    end
end
