function [flag, c] = SimradAllMosaiqueParametersByDefault(~, listeFicAll, SurveyName, NomLayer, gridSize, NomDir, varargin)

[varargin, IndNavUnit]            = getPropertyValue(varargin, 'IndNavUnit',            []);
[varargin, InfoCompensationCurve] = getPropertyValue(varargin, 'InfoCompensationCurve', []); %#ok<ASGLU>

switch NomLayer
    case 'ReflectivityFromSnippets'
        Prefix = 'Mos';
        strDataType = 'Reflectivity';
    case 'Depth+Draught-Heave'
        Prefix = 'DTM';
        strDataType = 'Bathymetry';
    otherwise
        Prefix = 'TODO';
        strDataType = 'TODO';
end

if isempty(InfoCompensationCurve)
    InfoCompensationCurve.FileName                 = {};
    InfoCompensationCurve.ModeDependant            = 0;
    InfoCompensationCurve.UseModel                 = [];
    InfoCompensationCurve.PreserveMeanValueIfModel = [];
    SameReflectivityStatus = 0; % 1; TODO : supprimer toutes les r�f�rences � SameReflectivityStatus : remplacer par un status
else
    SameReflectivityStatus = 0;
end

InfoMos.TypeMos          = 1;
InfoMos.TypeGrid         = 1;
InfoMos.flagMasquageDual = 1;
InfoMos.LimitAngles      = [];
InfoMos.Backup           = 10;
InfoMos.gridSize         = gridSize;

InfoMos.Import.MasqueActif  = 1;
InfoMos.Import.SlotFilename = [];
InfoMos.Import.SonarTime    = [];

InfoMos.Covering.Priority           = 1;
InfoMos.Covering.SpecularLimitAngle = [];

strGridSize = num2str(gridSize);
strGridSize = strrep(strGridSize, '.', ',');
str = sprintf('m_LatLong_%s.ers%s', strDataType);
InfoMos.Unique.nomFicErMapperLayer    = fullfile(NomDir, [SurveyName '-' Prefix '_' strGridSize str]);
InfoMos.Unique.nomFicErMapperAngle    = strrep(InfoMos.Unique.nomFicErMapperLayer, str, 'm_LatLong_RxAngleEarth.ers');
InfoMos.Unique.CompleteExistingMosaic = 0;

flagTideCorrection = 1;
ModeDependant      = 0;
Window             = [5 5];

I0 = cl_image;
[flag, c] = SimradAllMosaiqueParProfil(I0, 1, listeFicAll, ...
                ModeDependant, NomLayer, Window, ...
                SameReflectivityStatus, InfoCompensationCurve, InfoMos, ...
                'TideCorrection', flagTideCorrection, 'IndNavUnit', IndNavUnit);            
