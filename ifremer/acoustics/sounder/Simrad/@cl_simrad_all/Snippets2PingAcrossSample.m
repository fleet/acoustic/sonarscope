function [DataSeabed, b, Carto, subPingDepth] = Snippets2PingAcrossSample(this, varargin)

flag = existCacheNetcdf(this.nomFic);
if flag
    [DataSeabed, b, Carto, subPingDepth] = Snippets2PingAcrossSampleNetcdf(this, varargin{:});
else
    [DataSeabed, b, Carto, subPingDepth] = Snippets2PingAcrossSampleXMLBin(this, varargin{:});
end
