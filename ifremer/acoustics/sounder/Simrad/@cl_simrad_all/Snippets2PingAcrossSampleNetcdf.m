function [DataSeabed, b, Carto, subPingDepth] = Snippets2PingAcrossSampleNetcdf(this, varargin)

[varargin, Carto]      = getPropertyValue(varargin, 'Carto',      []);
[varargin, IndNavUnit] = getPropertyValue(varargin, 'IndNavUnit', []);

b  = [];
DataSeabed   = [];
subPingDepth = [];

%% Check the number of elements

flag = checkIfOneElement(length(this));
if ~flag
    return
end

[~, nomFic] = fileparts(this.nomFic);

%% Depth

[flag, DataDepth] = read_depth_bin(this, 'Memmapfile', 1);
if ~flag
    return
end
DataDepth.SamplingRate = DataDepth.SamplingRate(:,:);

%% Seabed image

[flag, DataSeabed, SeabedImageFormat] = read_seabedImage_bin(this, 'Memmapfile', 1);
if ~flag
    return
end

%% Raw range and beam angles

[flag, DataRaw] = read_rawRangeBeamAngle(this, 'Memmapfile', 1);
if ~flag
    return
end

if SeabedImageFormat == 2
    MAxSectors = DataRaw.Dimensions.nbTx;
    %     Freq = DataRaw.CentralFrequency(:,1);
    %     if length(unique(Freq)) == 1
    %         PingSequence = ones(DataRaw.Dimensions.nbPings,1,'single');
    %     else
    %         PingSequence = 1 + (Freq(1:end-1) > Freq(2:end));
    %         if Freq(end) > Freq(end-1)
    %             PingSequence(end+1) = 2;
    %         else
    %             PingSequence(end+1) = 1;
    %         end
    %     end
    PingSequence = DataRaw.PingSequence;
end

%% Facteur de qualit� Ifremer

[~, DataQualityFactor] = SSc_ReadDatagrams(this.nomFic, 'Ssc_QualityFactorFromIfr', 'Memmapfile', -1);

%%

% subPingDepth = 1:50:1000

%% Installation parameters

[flag, DataInstallationParameters, isDual] = read_installationParameters_bin(this, 'Memmapfile', 1);
if ~flag
    return
end

idebAmp_Ping  = zeros(DataSeabed.Dimensions.nbPings, 1);
nbAmp_Ping    = zeros(DataSeabed.Dimensions.nbPings, 1);
nbAmp_Ping(1) = sum(double(DataSeabed.InfoBeamNbSamples(1,:)), 'omitnan');
for kSeabed=2:DataSeabed.Dimensions.nbPings
    nbAmp_Ping(kSeabed) = sum(double(DataSeabed.InfoBeamNbSamples(kSeabed,:)), 'omitnan');
    idebAmp_Ping(kSeabed) = idebAmp_Ping(kSeabed-1) + nbAmp_Ping(kSeabed-1);
end

if isempty(DataQualityFactor)
    [subSeabed, subDepth, subRaw] = intersect3(DataSeabed.PingCounter(:,1), DataDepth.PingCounter(:,1), DataRaw.PingCounter(:,1));
    nbPings = length(subSeabed);
    if isDual && (nbPings == 0)
        [subSeabed, subDepth, subRaw] = intersect3(DataSeabed.PingCounter(:,2), DataDepth.PingCounter(:,2), DataRaw.PingCounter(:,2));
        nbPings = length(subSeabed);
    end
else
    [~, ~, subSeabed, subDepth, subRaw, subQF] = lin_intersect(DataSeabed.PingCounter(:,1), DataDepth.PingCounter(:,1), DataRaw.PingCounter(:,1), DataQualityFactor.PingCounter(:,1)); %#ok<ASGLU>
    nbPings = length(subSeabed);
    if isDual && (nbPings == 0)
        [subSeabed, subDepth, subRaw] = intersect3(DataSeabed.PingCounter(:,2), DataDepth.PingCounter(:,2), DataRaw.PingCounter(:,2));
        nbPings = length(subSeabed);
    end
end
[varargin, subPingDepth] = getPropertyValue(varargin, 'subPingDepth', subDepth); %#ok<ASGLU>

if SeabedImageFormat == 1
    [flag, DataRunTime] = SSc_ReadDatagrams(this.nomFic, 'Ssc_Runtime');
    if ~flag
        return
    end
    SignalLength = interpRuntime(DataRunTime.Time, DataRunTime.TransmitPulseLength, DataRaw.Time);
    SignalLength = SignalLength / 1000;
    
    [TxBeamIndex, Carto, OrigineDepthRaw] = get_Layer(this, 'TxBeamIndex', 'Carto', Carto);
    %         TxBeamIndex = get_Image(TxBeamIndex, 1);
    TxBeamIndex = TxBeamIndex(1); % TODO : pour assurer car get_Image(TxBeamIndex, 1); avant
    TxBeamIndex = get(TxBeamIndex, 'Image');
    if OrigineDepthRaw == 1
        %             subTxBeamIndex = subDepth;
        subTxBeamIndex = 1:size(TxBeamIndex,1);
    else
        subTxBeamIndex = subRaw;
    end
else
    SignalLength = DataRaw.SignalLength;
    TxBeamIndex = DataRaw.TransmitSectorNumber;
    %         [TxBeamIndexSwath, Carto] = get_Layer(this, 'TxBeamIndexSwath', 'Carto', Carto);
    %         TxBeamIndexSwath = get(TxBeamIndexSwath, 'Image');
    subTxBeamIndex = subRaw;
end

if SeabedImageFormat == 1
    SonarSampleFrequency = DataDepth.SamplingRate(subPingDepth,:);
else
    SonarSampleFrequency = DataSeabed.SamplingRate(subSeabed,:);
end

ResolutionD = (1500/2) ./ SonarSampleFrequency;
Lambda      = (1500/2) *  min(SignalLength(subRaw,:), [], 2);
ResolutionX = double(Lambda / 2);
ResolutionX = max(max(ResolutionD,[],2),ResolutionX);

%    ResolutionX = ResolutionX * 12;
%{
figure; plot(ResolutionD); grid; title('ResolutionD')
figure; plot(Lambda); grid; title('Lambda')
figure; plot(ResolutionX); grid; title('ResolutionX')
%}

Mask = DataDepth.Mask(subDepth,:);
AD = DataDepth.AcrossDist(subDepth,:);
AD(Mask ~= 0) = NaN;
clear Mask

% {
indAD = NaN(size(AD), 'single');
for k=1:size(AD,1)
    indAD(k,:) = floor(AD(k,:) / ResolutionX(k));
end
% SonarScope(AD)
% SonarScope(indAD)
% }

XMin = double(min(indAD(:), [], 'omitnan'));
if isnan(XMin)
    str1 = sprintf('Il semble qu''il y ait un pb avec le masque du fichier "%s", Vous pouvez le remettre � z�ro (voir la documentation).', this.nomFic);
    str2 = sprintf('It seems you are in trouble for file "%s", check the mask (look at the documentation).', this.nomFic);
    my_warndlg(Lang(str1,str2), 0, 'Tag', 'XMinInTroubleInSnippets2PingAcrossSample');
    DataSeabed = [];
    return
end
XMax = double(max(indAD(:), [], 'omitnan'));
xImageOut = (XMin-2):(XMax+2); % Faisons simple au d�but
nbColImageOut = length(xImageOut);

try
    Reflectivity_Image = NaN(nbPings, nbColImageOut, 'single');
catch %#ok<CTCH>
    Reflectivity_Image = cl_memmapfile('Value', 0, 'Size', [nbPings, nbColImageOut], 'Format', 'single');
end
try
    TxBeamIndex_Image = NaN(nbPings, nbColImageOut, 'single');
catch %#ok<CTCH>
    TxBeamIndex_Image = cl_memmapfile('Value', 0, 'Size', [nbPings, nbColImageOut], 'Format', 'single');
end
if SeabedImageFormat == 2
    try
        TxBeamIndexSwath_Image = NaN(nbPings, nbColImageOut, 'single');
    catch %#ok<CTCH>
        TxBeamIndexSwath_Image = cl_memmapfile('Value', 0, 'Size', [nbPings, nbColImageOut], 'Format', 'single');
    end
end
try
    NbSamplesMoy_Image = NaN(nbPings, nbColImageOut, 'single');
catch %#ok<CTCH>
    NbSamplesMoy_Image = cl_memmapfile('Value', 0, 'Size', [nbPings, nbColImageOut], 'Format', 'single');
end
try
    RxBeamAngle_Image = NaN(nbPings, nbColImageOut, 'single');
catch %#ok<CTCH>
    RxBeamAngle_Image = cl_memmapfile('Value', 0, 'Size', [nbPings, nbColImageOut], 'Format', 'single');
end
try
    RxBeamIndex_Image = NaN(nbPings, nbColImageOut, 'single');
catch %#ok<CTCH>
    RxBeamIndex_Image = cl_memmapfile('Value', 0, 'Size', [nbPings, nbColImageOut], 'Format', 'single');
end
try
    AlongDist_Image = NaN(nbPings, nbColImageOut, 'single');
catch %#ok<CTCH>
    AlongDist_Image = cl_memmapfile('Value', 0, 'Size', [nbPings, nbColImageOut], 'Format', 'single');
end
try
    Range_Image = NaN(nbPings, nbColImageOut, 'single');
catch %#ok<CTCH>
    Range_Image = cl_memmapfile('Value', 0, 'Size', [nbPings, nbColImageOut], 'Format', 'single');
end

Rn                     = DataSeabed.TVGN(subSeabed,:);
SonarBSN               = DataSeabed.BSN(subSeabed,:);
SonarBSO               = DataSeabed.BSO(subSeabed,:);
SonarTxBeamWidth       = DataSeabed.TxBeamWidth(subSeabed,:);
SonarTVGCrossOver      = DataSeabed.TVGCrossOver(subSeabed,:);
[Version, SousVersion] = version_datagram(this);

%% Cr�ation de l'image

str1 = sprintf('Conversion Snippets vers PingAcrossSample pour "%s" en cours', nomFic);
str2 = sprintf('Converting Snippets to PingAcrossSample for "%s"', nomFic);
N = length(subPingDepth);
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k1=1:N
    my_waitbar(k1, length(subPingDepth), hw)
    iPingSeabed = subSeabed(k1);
    
    %% Cr�ation du ping en AcrossDistSample
    
    [flag, Reflectivity_Ping, NbPicMoy_Ping, RxBeamAngle_Ping, TxBeamIndex_Ping, RxBeamIndex_Ping, AlongDist_Ping, Range_Ping, RangeBeam_Ping] = ...
        Snippets2PingAcrossSample_ping_Netcdf(this, DataSeabed, DataDepth, DataRaw, DataInstallationParameters, TxBeamIndex, SeabedImageFormat, isDual, ...
        iPingSeabed, subDepth(k1), subRaw(k1), subTxBeamIndex(k1), ResolutionX(k1), xImageOut, idebAmp_Ping, nbAmp_Ping);
    if ~flag
        return
    end
    if isempty(Reflectivity_Ping)
        continue
    end
    
    %% Correction du sp�culaire
    % {
    
    % TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO
    % Faire quelque chose de clair entre la V1 et la V2 : toujours
    % BeamDepression Angle ou toujours BeamPointingAngle ???
    
    %{
switch Version
case 'V1'
PingBeamAngles = DataDepth.BeamDepressionAngle(subDepth(k1),:);
case 'V2'
PingBeamAngles = Simrad_AnglesAntenna2ShipFloor(DataRaw.BeamPointingAngle(subRaw(k1),:), DataInstallationParameters, isDual);

end
    %}
    
    % TODO : cr�er un signal SounderIndex dans DataDepth OU une fonction
    % SounderIndex = get_SounderIndex(this)
    % On aurait ensuite
    
    % En attendant la disponibilit� de la fonction on fait le truc �
    % l'arrache
    if isDual
        SounderIndex = NaN(DataDepth.Dimensions.nbBeams, 1);
        iBeamMiddle = floor(DataDepth.Dimensions.nbBeams / 2);
        for k2=1:length(RxBeamIndex_Ping)
            if ~isnan(RxBeamIndex_Ping(k2))
                if RxBeamIndex_Ping(k2) <= iBeamMiddle
                    SounderIndex(k2) = 1;
                else
                    SounderIndex(k2) = 2;
                end
            end
        end
        subBabImage = (SounderIndex == 1);
        %         subTriImage = ~subBabImage; % Modifif JMA le 17/07/2013 : y'avait
        %         des NaN dans SounderIndex, le ~transformait les NaN en sondes
        %         tribord
        subTriImage = (SounderIndex ==2);
        subBabImage = find(subBabImage);
        subTriImage = find(subTriImage);
    else
        subBabImage = 1:DataDepth.Dimensions.nbBeams;
        subTriImage = [];
    end
    
    % TODO : bug ici car pas m�me taille entre DataDepth.Reflectivity et
    % Range  (mise en �vidence sur fichier EM2040 D:\Temp\EM2040\0018_20120222_131427_ShipName.all
    % (import donn�es en PingAcrooDist)
    
    % TODO : RxBeamAngle_Ping = RxBeamAngle_Ping - this.Sonar.Roll(subPingDepth(k1);
    
    if strcmp(Version, 'V1') && strcmp(SousVersion, 'F') && (DataDepth.EmModel == 1002) % Test pour donn�es EM1002 de Noum�a (CALICO - Charline)
        RangeNormal = Rn(k1,:) * (DataDepth.SamplingRate(k1) / 11973); % DataDepth.SamplingRate(1));
    else
        RangeNormal = Rn(k1,:);
    end
    
    Reflectivity_Ping = Simrad_CorrectionSpeculaire(Reflectivity_Ping, ...
        SonarBSN(k1,:), SonarBSO(k1,:), RangeNormal, ...
        RangeBeam_Ping, subBabImage, subTriImage, Version, SousVersion, SonarTVGCrossOver(k1,:));
    % }
    
    Reflectivity_Image(k1,:) = Reflectivity_Ping;
    TxBeamIndex_Image(k1,:)  = TxBeamIndex_Ping;
    NbSamplesMoy_Image(k1,:) = NbPicMoy_Ping;
    RxBeamAngle_Image(k1,:)  = RxBeamAngle_Ping;
    RxBeamIndex_Image(k1,:)  = RxBeamIndex_Ping;
    AlongDist_Image(k1,:)    = AlongDist_Ping;
    Range_Image(k1,:)        = Range_Ping;
    
    if SeabedImageFormat == 2
        S = TxBeamIndex_Ping;
        sMax = max(DataRaw.TransmitSectorNumber(subRaw(k1),:));
        if isnan(sMax)
            sMax = 0;
        end
        TxBeamIndexSwath_Ping = NaN(size(TxBeamIndex_Ping));
        for s=1:sMax
            TxBeamIndexSwath_Ping(S==s) = s + (PingSequence(subRaw(k1))-1) * MAxSectors;
        end
        TxBeamIndexSwath_Image(k1,:)  = TxBeamIndexSwath_Ping;
    end
end
my_close(hw)

% SonarScope(Reflectivity_Image)
% SonarScope(TxBeamIndex_Image)
% SonarScope(TxBeamIndexSwath_Image)
% SonarScope(NbSamplesMoy_Image)
% SonarScope(RxBeamAngle_Image)
% SonarScope(Range_Image)

clear b
y = 1:nbPings;
GeometryType = cl_image.indGeometryType('PingAcrossSample');
ImageName = nomFic;
TagSynchroX = [nomFic ' - PingAcrossSample'];
TagSynchroY = nomFic;

DataType = cl_image.indDataType('Reflectivity');
b(1) = cl_image('Image', Reflectivity_Image, 'Name', ImageName, 'Unit', 'dB', ...
    'x', xImageOut, 'y', y, 'xUnit', '#', 'yUnit', 'Ping', ...
    'DataType', DataType, 'GeometryType', GeometryType, ...
    'ColormapIndex', 2);

DataType = cl_image.indDataType('AveragedPtsNb');
b(2) = cl_image('Image', NbSamplesMoy_Image, 'Name', ImageName, 'Unit', '#', ...
    'x', xImageOut, 'y', y, 'xUnit', '#', 'yUnit', 'Ping', ...
    'DataType', DataType, 'GeometryType', GeometryType);

% TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO
% Cr�er toujours 2 angles clairement d�finis : BeamdepressionAngle et
% BeamPointingAngle

DataType = cl_image.indDataType('TxAngle');
b(3) = cl_image('Image', RxBeamAngle_Image, 'Name', ImageName, 'Unit', 'deg', ...
    'x', xImageOut, 'y', y, 'xUnit', '#', 'yUnit', 'Ping', ...
    'DataType', DataType, 'GeometryType', GeometryType);

DataType = cl_image.indDataType('TxBeamIndex');
b(4) = cl_image('Image', TxBeamIndex_Image, 'Name', ImageName, 'Unit', '#', ...
    'x', xImageOut, 'y', y, 'xUnit', '#', 'yUnit', 'Ping', ...
    'DataType', DataType, 'GeometryType', GeometryType);
b(4).CLim = [0 max(TxBeamIndex_Image(:))];

DataType = cl_image.indDataType('RxBeamIndex');
b(5) = cl_image('Image', RxBeamIndex_Image, 'Name', ImageName, 'Unit', '#', ...
    'x', xImageOut, 'y', y, 'xUnit', '#', 'yUnit', 'Ping', ...
    'DataType', DataType, 'GeometryType', GeometryType, 'CLim', [0 max(RxBeamIndex_Image(:))]);

DataType = cl_image.indDataType('AlongDist');
b(6) = cl_image('Image', AlongDist_Image, 'Name', ImageName, 'Unit', 'm', ...
    'x', xImageOut, 'y', y, 'xUnit', '#', 'yUnit', 'Ping', ...
    'DataType', DataType, 'GeometryType', GeometryType);

DataType = cl_image.indDataType('TwoWayTravelTimeInSamples');
b(7) = cl_image('Image', Range_Image, 'Name', ImageName, 'Unit', '#', ...
    'x', xImageOut, 'y', y, 'xUnit', '#', 'yUnit', 'Ping', ...
    'DataType', DataType, 'GeometryType', GeometryType);

if SeabedImageFormat == 2
    DataType = cl_image.indDataType('TxBeamIndexSwath');
    b(8) = cl_image('Image', TxBeamIndexSwath_Image, 'Name', ImageName, 'Unit', '#', ...
        'x', xImageOut, 'y', y, 'xUnit', '#', 'yUnit', 'Ping', ...
        'DataType', DataType, 'GeometryType', GeometryType);
    b(8).CLim = [0 max(TxBeamIndexSwath_Image(:))];
end

SonarRawDataResol = Lambda(1);
SonarResolutionD  = ResolutionD(1);
SonarResolutionX  = ResolutionX(1);

SonarTime = DataSeabed.Time;
SonarTime = SonarTime.timeMat;
SonarTime = cl_time('timeMat', SonarTime(subSeabed));

SonarPingCounter = DataSeabed.PingCounter(subSeabed,1);

if SeabedImageFormat == 1
    [Bathy, Carto] = get_Layer(this, 'Bathymetry', 'Carto', Carto, 'IndNavUnit', IndNavUnit);
else
    [Bathy, Carto] = get_Layer(this, 'Depth+Draught-Heave', 'Carto', Carto, 'IndNavUnit', IndNavUnit);
end
SonarHeading            = get(Bathy, 'SonarHeading');
SonarSurfaceSoundSpeed  = get(Bathy, 'SonarSurfaceSoundSpeed');
SonarImmersion          = get(Bathy, 'SonarImmersion');
SonarHeight             = get(Bathy, 'SonarHeight');
SonarRoll               = get(Bathy, 'SonarRoll');
SonarPitch              = get(Bathy, 'SonarPitch');
SonarHeave              = get(Bathy, 'SonarHeave');
SonarFishLatitude       = get(Bathy, 'SonarFishLatitude');
SonarFishLongitude      = get(Bathy, 'SonarFishLongitude');
SonarPortMode_1         = get(Bathy, 'SonarPortMode_1');
SonarInterlacing        = get(Bathy, 'SonarInterlacing');
SonarDistPings          = get(Bathy, 'SonarDistPings');
if SeabedImageFormat == 2
    SonarPresenceWC         = get(Bathy, 'SonarPresenceWC');
    SonarPresenceFM         = get(Bathy, 'SonarPresenceFM');
    SonarNbSwaths           = get(Bathy, 'SonarNbSwaths');
end

SonarDescription = get_SonarDescription(Bathy);

for k=1:length(b)
    b(k) = set_Carto(b(k), Carto);
    b(k) = set_SonarDescription(b(k), SonarDescription);
    b(k) = set(b(k), 'TagSynchroX', TagSynchroX, 'TagSynchroY', TagSynchroY);
    b(k) = set(b(k), 'SonarRawDataResol',   SonarRawDataResol);
    b(k) = set(b(k), 'SonarResolutionD',    SonarResolutionD);
    b(k) = set(b(k), 'SonarResolutionX',    SonarResolutionX);
    b(k) = set_SonarTime(b(k),              SonarTime);
    b(k) = set_SonarPingCounter(b(k),       SonarPingCounter);
    b(k) = set_SonarSampleFrequency(b(k),   SonarSampleFrequency);
    
    b(k) = set_SonarImmersion(b(k),         SonarImmersion); % (subDepth) supprim� par JMA le 14/01/2015 � Barcelone (fichier G:\PatrickJaussaud\ESNAUT2014\03122014-03\03122014-03-science\Cu\SMF_2040ForTrainingCourse\0003_20141203_221723_raw.all)
    b(k) = set_SonarHeight(b(k),            SonarHeight);    % (subDepth) ...
    b(k) = set_SonarHeading(b(k),           SonarHeading);   % (subDepth) ...
    if ~isempty(SonarRoll)
        b(k) = set_SonarRoll(b(k),          SonarRoll);      % (subDepth) ...
    end
    if ~isempty(SonarPitch)
        b(k) = set_SonarPitch(b(k),         SonarPitch);     % (subDepth) ...
    end
    b(k) = set_SonarSurfaceSoundSpeed(b(k), SonarSurfaceSoundSpeed);     % (subDepth) ...
    b(k) = set_SonarHeave(b(k),             SonarHeave);                 % (subDepth) ...
    b(k) = set_SonarFishLatitude(b(k),      SonarFishLatitude);          % (subDepth) ...
    b(k) = set_SonarFishLongitude(b(k),     SonarFishLongitude);         % (subDepth) ...
    b(k) = set_SonarPortMode_1(b(k),        SonarPortMode_1);            % (subDepth) ...
    if ~isempty(SonarInterlacing)
        %         b(k) = set_SonarInterlacing(b(k),   SonarInterlacing(subDepth));
        b(k) = set_SonarInterlacing(b(k),   SonarInterlacing);
    end
    if ~isempty(SonarDistPings)
        %         b(k) = set_SonarDistPings(b(k),     SonarDistPings(subDepth));
        b(k) = set_SonarDistPings(b(k),     SonarDistPings);
    end
    if SeabedImageFormat == 2
        b(k) = set_SonarPresenceWC(b(k),    SonarPresenceWC);            % (subDepth) ...
        b(k) = set_SonarPresenceFM(b(k),    SonarPresenceFM);            % (subDepth) ...
        b(k) = set_SonarNbSwaths(b(k),      SonarNbSwaths);              % (subDepth) ...
    end
    
    b(k) = set_SonarTVGN(b(k),              Rn);
    b(k) = set_SonarBSN(b(k),               SonarBSN);
    b(k) = set_SonarBSO(b(k),               SonarBSO);
    b(k) = set_SonarTxBeamWidth(b(k),       SonarTxBeamWidth);
    b(k) = set_SonarTVGCrossOver(b(k),      SonarTVGCrossOver);
    
    if k == 1
        [flag, signalContainerAbsorptionCoeff_RT] = create_signal_AbsorptionCoeff_RT(this, 'DataDepth', DataDepth, 'DataSeabed', DataSeabed, ...
            'DataRaw', DataRaw);
        if flag
            AbsorptionCoeff = signalContainerAbsorptionCoeff_RT.signalList.ySample.data;
            BSStatus = init_BSStatus('EmModel', DataDepth.EmModel, 'Absorption', AbsorptionCoeff, 'SonarBS_etat', 1); % 1 avant intervntion Ir�ne
            b(k) = set_BSStatus(b(k), BSStatus);
            b(k) = set_SonarAbsorptionCoeff_RT(b(k), AbsorptionCoeff(subDepth));
        end
    end
    
    b(k) = set_SonarResolAcrossSample(b(k), ResolutionX);
    
    b(k).InitialFileFormat = Bathy.InitialFileFormat;
    b(k).InitialFileName   = Bathy.InitialFileName;
    % b(k).InitialImageName = Bathy.InitialImageName;
    
    b(k) = update_Name(b(k));
end

%{
SonarScope(b)
%}

%% Edition de l'objet cli_image

if nargout == 0
    SonarScope(b);
end
