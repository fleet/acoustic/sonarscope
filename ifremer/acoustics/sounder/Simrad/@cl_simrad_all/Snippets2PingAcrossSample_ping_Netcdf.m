function [flag, Reflectivity_Ping, NbSamplesMoy_Ping, Angles_Ping, TxBeamIndex_Ping, RxBeamIndex_Ping, AlongDist_Ping, Range_Ping, RangeBeam_Ping] = ...
    Snippets2PingAcrossSample_ping_Netcdf(~, DataSeabed, DataDepth, DataRaw, DataInstallationParameters, TxBeamIndex, SeabedImageFormat, isDual, ...
    kSeabed, kDepth, kRaw, kTxBeamIndex, ResolImageOut, xImageOut, idebAmp_Ping, nbAmp_Ping) % TODO : supprimer this et placer cette fonction dans private

% TODO : a passer en param�tre et laisser la valeur 75 pour les anciens
% sondeurs, cela permet d'�viter des tailles gigantesques d'images
SeuilRejetAngulaire = 88; % Ancienne valeur = 75 mais j'ai d� la porter � 85 deg � cause de l'EM2040 Dual

% TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO
% Faire quelque chose de clair entre la V1 et la V2 : toujours
% BeamDepression Angle ???

if SeabedImageFormat == 1
    Range = floor(DataDepth.Range(kDepth,:));
    try
        DataDepthRxBeamAngle = DataRaw.BeamPointingAngle(kRaw,:);
    catch
        DataDepthRxBeamAngle = DataDepth.BeamDepressionAngle(kDepth,:);
    end
    DataDepthRxBeamAngle = Simrad_AnglesAntenna2ShipFloor(DataDepthRxBeamAngle, DataInstallationParameters, isDual);
else
%     Range = floor(DataRaw.TwoWayTravelTimeInSeconds(kRaw,:) .* DataRaw.SamplingRate(kRaw));
    Range = floor(DataRaw.TwoWayTravelTimeInSeconds(kRaw,:) .* DataSeabed.SamplingRate(kSeabed));
    
%     DataDepthRxBeamAngle = DataRaw.BeamPointingAngle(kRaw,:);
    DataDepthRxBeamAngle = Simrad_AnglesAntenna2ShipFloor(DataRaw.BeamPointingAngle(kRaw,:), DataInstallationParameters, isDual);
end
TxBeamIndex = TxBeamIndex(kTxBeamIndex,:);
nbBeams = DataSeabed.Dimensions.nbBeams;

nbTxBeamIndex = max(TxBeamIndex);
if isnan(nbTxBeamIndex)
    Reflectivity_Ping 	= [];
    Angles_Ping         = [];
    NbSamplesMoy_Ping   = [];
    RxBeamIndex_Ping    = [];
    AlongDist_Ping      = [];
    Range_Ping          = [];
    RangeBeam_Ping      = [];
    TxBeamIndex_Ping    = [];
    flag = 1;
    return
end
nbSamples_Ping = length(xImageOut);
Reflectivity_Ping 	= zeros(nbTxBeamIndex, nbSamples_Ping, 'single');
Angles_Ping         = zeros(nbTxBeamIndex, nbSamples_Ping, 'single');
NbSamplesMoy_Ping   = zeros(nbTxBeamIndex, nbSamples_Ping, 'single');
RxBeamIndex_Ping    = zeros(nbTxBeamIndex, nbSamples_Ping, 'single');
AlongDist_Ping      = zeros(nbTxBeamIndex, nbSamples_Ping, 'single');
Range_Ping          = zeros(nbTxBeamIndex, nbSamples_Ping, 'single');
RangeBeam_Ping      = zeros(nbTxBeamIndex, nbSamples_Ping, 'single');

% Rustine pour corriger le manque d'un beam fichier C:\Temp\UNH\2009data\0012_20090403_030916_ShipName.all

if size(Range,2) ~= nbBeams
    nbBeams = size(Range,2);
    DataSeabed.InfoBeamNbSamples     = DataSeabed.InfoBeamNbSamples(:,1:nbBeams);
    DataSeabed.InfoBeamSortDirection = DataSeabed.InfoBeamSortDirection(:,1:nbBeams);
    DataSeabed.InfoBeamCentralSample = DataSeabed.InfoBeamCentralSample(:,1:nbBeams);
end

DataSeabedInfoBeamSortDirection = DataSeabed.InfoBeamSortDirection(kSeabed,:);
DataSeabedInfoBeamNbSamples     = DataSeabed.InfoBeamNbSamples(kSeabed,:);
DataSeabedInfoBeamCentralSample = DataSeabed.InfoBeamCentralSample(kSeabed,:);

idebAmp = idebAmp_Ping(kSeabed);
n = nbAmp_Ping(kSeabed);

Entries = DataSeabed.Entries((idebAmp+1):(idebAmp+n));

% TODO : Test concluant sur �pave de Douarnenez mais c'est dangereux de
% g�n�raliser : voir avec Xavier ce qu'il convient de faire
% Entries(Entries < -55) = NaN;

Entries = reflec_dB2Amp(Entries);

nbBeamsBySide = floor(nbBeams / 2);
SystemSerialNumber = DataSeabed.SystemSerialNumber(:,:);
nbSounders = length(unique(SystemSerialNumber(~isnan(SystemSerialNumber))));
if nbSounders == 1
    if isDual
        if SeabedImageFormat == 1
%             isBab = DataDepth.BeamDepressionAngle(kDepth,1:nbBeamsBySide) < 0;
            isBab = DataDepth.BeamDepressionAngle(kDepth,:) < 0;
        else
%             isBab = DataRaw.BeamPointingAngle(kRaw,1:nbBeamsBySide) < 0;
            isBab = DataRaw.BeamPointingAngle(kRaw,:) < 0;
        end
        isBab = repmat(isBab, 1, nbBeams);
    else
        isBab = [true(1, nbBeamsBySide) false(1, nbBeamsBySide)];
        isBab(nbBeams) = isBab(nbBeams-1); % 111 = 2*55+1
    end
else
    isBab = [true(1, nbBeamsBySide) false(1, nbBeamsBySide)];
end

DataDepthAcrossDist = DataDepth.AcrossDist(kDepth,:) / ResolImageOut;
DataDepthAlongDist  = DataDepth.AlongDist(kDepth,:);
Mask = DataDepth.Mask(kDepth,:);
Range(Mask ~= 0) = NaN;

nbBeams = length(Range);
subEntries = cell(1,nbBeams);
kPre = 0;
for iBeam=1:nbBeams
    nSamples = DataSeabedInfoBeamNbSamples(iBeam);
    if isnan(nSamples)
        continue
    end
   if DataSeabedInfoBeamSortDirection(iBeam) == 1
        subEntries{iBeam} = kPre + (nSamples:-1:1);
    else
        subEntries{iBeam} = kPre + (1:nSamples);
    end
    kPre = kPre + nSamples;
end

subRange = find(~isnan(Range));
% for ib=2:(length(subRange)-1)
nbRanges = length(subRange);
for ib=1:nbRanges
    iBeam = subRange(ib);
    
    if isnan(Range(iBeam))
        continue
    end
    
    nSamples = DataSeabedInfoBeamNbSamples(iBeam);
    if isnan(nSamples)
        continue
    end
    
    if ib == 1
        if nbRanges == 1
            continue
        else
            iBeamM1 = subRange(1);
            iBeamP1 = subRange(2);
        end
    elseif ib == nbRanges
        iBeamM1 = subRange(nbRanges-1);
        iBeamP1 = subRange(nbRanges);
    else
        iBeamM1 = subRange(ib-1);
        iBeamP1 = subRange(ib+1);
    end
    
%     if isBab(iBeam)
    if DataSeabedInfoBeamSortDirection(iBeam) == -1 % Modifi� par JMA le 05/04/2015
        subSampleBeam = (Range(iBeam) - (DataSeabedInfoBeamNbSamples(iBeam) - DataSeabedInfoBeamCentralSample(iBeam))) + (1:length(subEntries{iBeam}));
    else
%         subSampleBeam = (Range(iBeam) - DataSeabedInfoBeamCentralSample(iBeam) + 1) + (1:length(subEntries{iBeam}));
        subSampleBeam = (Range(iBeam) - DataSeabedInfoBeamCentralSample(iBeam) - 1) + (1:length(subEntries{iBeam})); % Modif JMA le 05/04/2015
    end
    
    %{
    % Code utilis� dans Simrad_maskEntries2 et qui semble correct (v�rifi�
    % avec visu des snippets en g�om�trie SampleBeam)
    % Ici, subSampleBeam devrait �tre �quivalent � r12:rn2
    n2 = length(subEntries{iBeam})
    c2 = DataSeabedInfoBeamCentralSample(iBeam)
    iBeam2 = iBeam
    
%     n2 = double(IBNS(iBeam2)); % Number of samples of the snippet
%     c2 = IBCS(iBeam2);
    rc2 = Range(iBeam2);
    if IBSD(iBeam2) == -1
        r12 = Range(iBeam2) - (n2-c2) + 1;
        rn2 = r12 + n2 - 1;
    else
        r12 = Range(iBeam2) - c2;
        rn2 = r12 + n2 - 1;
    end
    %}
    
    
%     % TODO : A simplifier, on devrait pouvoir faire simple
%     if DataSeabedInfoBeamSortDirection(iBeam) == 1
%         BeamSamples = fliplr(Entries(subEntries{iBeam})); % CPU 28.75
%     else
%         BeamSamples = Entries(subEntries{iBeam});
% %         subSampleBeam = fliplr(subSampleBeam);
%     end
%     %         figure(7777); plot(DataSeabedInfoBeamSortDirection(:), '*');
            BeamSamples = Entries(subEntries{iBeam});


    R1  = Range(iBeamM1);
    R2  = Range(iBeam);
    R3  = Range(iBeamP1);
    Ac1 = DataDepthAcrossDist(iBeamM1);
    Ac2 = DataDepthAcrossDist(iBeam);
    Ac3 = DataDepthAcrossDist(iBeamP1);
    An1 = DataDepthRxBeamAngle(iBeamM1);
    An2 = DataDepthRxBeamAngle(iBeam);
    An3 = DataDepthRxBeamAngle(iBeamP1);
    Al1 = DataDepthAlongDist(iBeamM1);
    Al2 = DataDepthAlongDist(iBeam);
    Al3 = DataDepthAlongDist(iBeamP1);
    
    if isDual
        if (isBab(iBeam) && (Ac3 > 0)) || (~isBab(iBeam) && (Ac1 < 0))
            continue
        end
    end
    
%     subH = find(subSampleBeam < R2);
%     subB = find(subSampleBeam >= R2);
    pppp = subSampleBeam < R2;
    subH = find(pppp);
    subB = find(~pppp);
    RH = subSampleBeam(subH);
    RB = subSampleBeam(subB);
    
    [flag, Reflectivity_Ping, NbSamplesMoy_Ping, Angles_Ping, RxBeamIndex_Ping, AlongDist_Ping, Range_Ping, RangeBeam_Ping] = ...
        projection_ping(subH, subB, RB, R1, R2, R3, Ac1, Ac2, Ac3, An1, An2, An3, Al1, Al2, Al3, ...
        Range, RH, BeamSamples, xImageOut, iBeam, TxBeamIndex, ...
        Reflectivity_Ping, NbSamplesMoy_Ping, Angles_Ping, RxBeamIndex_Ping, AlongDist_Ping, Range_Ping, RangeBeam_Ping, nbSamples_Ping, SeuilRejetAngulaire);
    if ~flag
        continue
    end
end

if all(NbSamplesMoy_Ping == 0)
    Reflectivity_Ping   = [];
    TxBeamIndex_Ping    = [];
    Angles_Ping         = [];
    RxBeamIndex_Ping    = [];
    AlongDist_Ping      = [];
    Range_Ping          = [];
    RangeBeam_Ping      = [];
    flag = 1;
    return
end

Reflectivity_Ping = Reflectivity_Ping ./ NbSamplesMoy_Ping;
AlongDist_Ping    = AlongDist_Ping    ./ NbSamplesMoy_Ping;
Angles_Ping       = Angles_Ping       ./ NbSamplesMoy_Ping;

Reflectivity_Ping(NbSamplesMoy_Ping == 0) = NaN;

% figure; plot(Reflectivity_Ping'); grid
% figure; plot(NbSamplesMoy_Ping'); grid
[NbSamplesMoy_Ping, TxBeamIndex_Ping] = max(NbSamplesMoy_Ping, [], 1);
% figure; plot(I); grid

% n = size(Reflectivity_Ping,2);
R = NaN(1, nbSamples_Ping, 'single');
A = NaN(1, nbSamples_Ping, 'single');
B = NaN(1, nbSamples_Ping, 'single');
C = NaN(1, nbSamples_Ping, 'single');
D = NaN(1, nbSamples_Ping, 'single');
E = NaN(1, nbSamples_Ping, 'single');
for k=1:nbSamples_Ping
    kk = TxBeamIndex_Ping(k);
    R(k) = Reflectivity_Ping(kk,k);
    A(k) = Angles_Ping(kk,k);
    B(k) = RxBeamIndex_Ping(kk,k); % faire un median plutot
    C(k) = AlongDist_Ping(kk,k);
    D(k) = Range_Ping(kk,k);
    E(k) = RangeBeam_Ping(kk,k);
end

Reflectivity_Ping   = reflec_Amp2dB(R);
Angles_Ping         = A;
RxBeamIndex_Ping    = B;
AlongDist_Ping      = C;
Range_Ping          = D;
RangeBeam_Ping      = E;
subNaN = (NbSamplesMoy_Ping == 0);
TxBeamIndex_Ping(subNaN) = NaN;
Angles_Ping(subNaN)      = NaN;
RxBeamIndex_Ping(subNaN) = NaN;
AlongDist_Ping(subNaN)   = NaN;
Range_Ping(subNaN)       = NaN;
RangeBeam_Ping(subNaN)   = NaN;

% FigUtils.createSScFigure(1256); PlotUtils.createSScPlot(xImageOut, Reflectivity_Ping), grid on;

flag = 1;

%{
FigUtils.createSScFigure;
hx(1) = subplot(4,1,1); PlotUtils.createSScPlot(Reflectivity_Ping, '.'), grid on;
hx(2) = subplot(4,1,2); PlotUtils.createSScPlot(RxBeamIndex_Ping, '.'), grid on;
hx(3) = subplot(4,1,3); PlotUtils.createSScPlot(Angles_Ping, '.'), grid on;
hx(4) = subplot(4,1,4); PlotUtils.createSScPlot(NbSamplesMoy_Ping, '.'), grid on;
linkaxes(hx, 'x')
%}

function [Reflectivity_Ping, NbSamplesMoy_Ping, Angles_Ping, RxBeamIndex_Ping, AlongDist_Ping, Range_Ping, RangeBeam_Ping] = ...
    projection(RBeam, R, Ac, An, Al, Samples, xImageOut, iBeam, TxBeamIndex, ...
    Reflectivity_Ping, NbSamplesMoy_Ping, Angles_Ping, RxBeamIndex_Ping, AlongDist_Ping, Range_Ping, RangeBeam_Ping, nbSamples_Ping)

if isnan(TxBeamIndex)
    return
end

% figure(1234)
% plot(Ac, reflec_Amp2dB(Samples), Style); grid on; hold on;

step = xImageOut(2) - xImageOut(1);
sub = floor(1.5 + (Ac - xImageOut(1)) / step);
subOut = (sub > nbSamples_Ping) | (sub < 1); % CPU 18
sub(subOut) = [];
if isempty(sub)
    return
end
Samples(subOut) = [];
R(subOut) = [];
subNonNaN = find(~isnan(sub) & ~isnan(Samples')); % CPU 24
sub = sub(subNonNaN);
if isempty(sub)
    return
end
for k=1:length(sub)
    kSub = sub(k);
    kSubNonNaN = subNonNaN(k);
    Reflectivity_Ping(TxBeamIndex, kSub) = Reflectivity_Ping(TxBeamIndex, kSub) + Samples(kSubNonNaN); % CPU 34
    Angles_Ping(TxBeamIndex, kSub)       = Angles_Ping(TxBeamIndex, kSub) + An(kSubNonNaN);
    AlongDist_Ping(TxBeamIndex, kSub)    = AlongDist_Ping(TxBeamIndex, kSub) + Al(kSubNonNaN);
    NbSamplesMoy_Ping(TxBeamIndex, kSub) = NbSamplesMoy_Ping(TxBeamIndex, kSub) + 1;
%     RxBeamIndex_Ping(TxBeamIndex, kSub)  = iBeam;
%     Range_Ping(TxBeamIndex, kSub)        = R(k);
end
RxBeamIndex_Ping(TxBeamIndex, sub)  = iBeam;
Range_Ping(TxBeamIndex, sub)        = R(subNonNaN);
RangeBeam_Ping(TxBeamIndex, sub)    = RBeam;

function y2 = interp1_Simplified(x1, y1, x2)
a = (y1(2)-y1(1)) / (x1(2)-x1(1));
b = y1(1) - a * x1(1);
y2 = a * x2 + b;



function [flag, Reflectivity_Ping, NbSamplesMoy_Ping, Angles_Ping, RxBeamIndex_Ping, AlongDist_Ping, Range_Ping, RangeBeam_Ping] = ...
    projection_ping(subH, subB, RB, R1, R2, R3, Ac1, Ac2, Ac3, An1, An2, An3, Al1, Al2, Al3, ...
    Range, RH, BeamSamples, xImageOut, iBeam, TxBeamIndex, ...
    Reflectivity_Ping, NbSamplesMoy_Ping, Angles_Ping, RxBeamIndex_Ping, AlongDist_Ping, Range_Ping, RangeBeam_Ping, nbSamples_Ping, SeuilRejetAngulaire)

flag = 0;

%AcH : AcrossDistance for samples over beam range (range < beamRange)
%AcB : AcrossDistance for samples under beam range (range < beamRange)
%AnH : Angle for samples over beam range (range < beamRange)
%AnB : Angle for samples under beam range (range < beamRange)
%AlH : AlongDistance for samples over beam range (range < beamRange)
%AlB : AlongDistance for samples under beam range (range < beamRange)
if (R2 < R1) && (R2 > R3) % H ->[R2, R3], B -> [R1, R2
    if ~isempty(RH)
        AnH = interp1_Simplified([R2, R3], [An2, An3], RH);
        if any(abs(AnH) > SeuilRejetAngulaire)
            return
        end
        AcH = interp1_Simplified([R2, R3], [Ac2, Ac3], RH);
        AlH = interp1_Simplified([R2, R3], [Al2, Al3], RH);
        [Reflectivity_Ping, NbSamplesMoy_Ping, Angles_Ping, RxBeamIndex_Ping, AlongDist_Ping, Range_Ping, RangeBeam_Ping] = ...
            projection(Range(iBeam), RH, AcH, AnH, AlH, BeamSamples(subH), xImageOut, iBeam, TxBeamIndex(iBeam), ...
            Reflectivity_Ping, NbSamplesMoy_Ping, Angles_Ping, RxBeamIndex_Ping, AlongDist_Ping, Range_Ping, RangeBeam_Ping, nbSamples_Ping);
    end
    if ~isempty(RB)
        AnB = interp1_Simplified([R1, R2], [An1, An2], RB);
        if any(abs(AnB) > SeuilRejetAngulaire)
            return
        end
        AcB = interp1_Simplified([R1, R2], [Ac1, Ac2], RB);
        AlB = interp1_Simplified([R1, R2], [Al1, Al2], RB);
        [Reflectivity_Ping, NbSamplesMoy_Ping, Angles_Ping, RxBeamIndex_Ping, AlongDist_Ping, Range_Ping, RangeBeam_Ping] = ...
            projection(Range(iBeam), RB, AcB, AnB, AlB, BeamSamples(subB), xImageOut, iBeam, TxBeamIndex(iBeam), ...
            Reflectivity_Ping, NbSamplesMoy_Ping, Angles_Ping, RxBeamIndex_Ping, AlongDist_Ping, Range_Ping, RangeBeam_Ping, nbSamples_Ping);
    end
elseif (R2 > R1) && (R2 < R3) % H ->[R1, R2], B -> [R2, R3]
    if ~isempty(RH)
        AnH = interp1_Simplified([R1, R2], [An1, An2], RH);
        if any(abs(AnH) > SeuilRejetAngulaire)
            return
        end
        AcH = interp1_Simplified([R1, R2], [Ac1, Ac2], RH);
        AlH = interp1_Simplified([R1, R2], [Al1, Al2], RH);
        [Reflectivity_Ping, NbSamplesMoy_Ping, Angles_Ping, RxBeamIndex_Ping, AlongDist_Ping, Range_Ping, RangeBeam_Ping] = ...
            projection(Range(iBeam), RH, AcH, AnH, AlH, BeamSamples(subH), xImageOut, iBeam, TxBeamIndex(iBeam), ...
            Reflectivity_Ping, NbSamplesMoy_Ping, Angles_Ping, RxBeamIndex_Ping, AlongDist_Ping, Range_Ping, RangeBeam_Ping, nbSamples_Ping);
    end
    if ~isempty(RB)
        AnB = interp1_Simplified([R2, R3], [An2, An3], RB);
        if any(abs(AnB) > SeuilRejetAngulaire)
            return
        end
        AcB = interp1_Simplified([R2, R3], [Ac2, Ac3], RB);
        AlB = interp1_Simplified([R2, R3], [Al2, Al3], RB);
        [Reflectivity_Ping, NbSamplesMoy_Ping, Angles_Ping, RxBeamIndex_Ping, AlongDist_Ping, Range_Ping, RangeBeam_Ping] = ...
            projection(Range(iBeam), RB, AcB, AnB, AlB, BeamSamples(subB), xImageOut, iBeam, TxBeamIndex(iBeam), ...
            Reflectivity_Ping, NbSamplesMoy_Ping, Angles_Ping, RxBeamIndex_Ping, AlongDist_Ping, Range_Ping, RangeBeam_Ping, nbSamples_Ping);
    end
elseif (R2 < R1) && (R2 < R3) % H -> non projetable, B -> [R1, R2] et [R2, R3]
    if ~isempty(RB)
        AnB1 = interp1_Simplified([R1, R2], [An1, An2], RB);
        if any(abs(AnB1) > SeuilRejetAngulaire)
            return
        end
        AnB2 = interp1_Simplified([R2, R3], [An2, An3], RB);
        if any(abs(AnB2) > SeuilRejetAngulaire)
            return
        end
        AcB1 = interp1_Simplified([R1, R2], [Ac1, Ac2], RB);
        AcB2 = interp1_Simplified([R2, R3], [Ac2, Ac3], RB);
        AlB1 = interp1_Simplified([R1, R2], [Al1, Al2], RB);
        AlB2 = interp1_Simplified([R2, R3], [Al2, Al3], RB);
        %         [Reflectivity_Ping, NbSamplesMoy_Ping, Angles_Ping, RxBeamIndex_Ping, AlongDist_Ping, Range_Ping, RangeBeam_Ping] = projection(Range(iBeam), XX, AcH, Entries(subEntries{iBeam}(subH)), '-go')
        [Reflectivity_Ping, NbSamplesMoy_Ping, Angles_Ping, RxBeamIndex_Ping, AlongDist_Ping, Range_Ping, RangeBeam_Ping] = ...
            projection(Range(iBeam), RB, AcB1, AnB1, AlB1, BeamSamples(subB), xImageOut, iBeam, TxBeamIndex(iBeam), ...
            Reflectivity_Ping, NbSamplesMoy_Ping, Angles_Ping, RxBeamIndex_Ping, AlongDist_Ping, Range_Ping, RangeBeam_Ping, nbSamples_Ping);
        [Reflectivity_Ping, NbSamplesMoy_Ping, Angles_Ping, RxBeamIndex_Ping, AlongDist_Ping, Range_Ping, RangeBeam_Ping] = ...
            projection(Range(iBeam), RB, AcB2, AnB2, AlB2, BeamSamples(subB), xImageOut, iBeam, TxBeamIndex(iBeam), ...
            Reflectivity_Ping, NbSamplesMoy_Ping, Angles_Ping, RxBeamIndex_Ping, AlongDist_Ping, Range_Ping, RangeBeam_Ping, nbSamples_Ping);
    end
elseif (R2 > R1) && (R2 > R3) % H -> [R1, R2] et [R2, R3], B -> non projetable
    if ~isempty(RH)
        AnH1 = interp1_Simplified([R1, R2], [An1, An2], RH);
        if any(abs(AnH1) > SeuilRejetAngulaire)
            return
        end
        AnH2 = interp1_Simplified([R2, R3], [An2, An3], RH);
        if any(abs(AnH2) > SeuilRejetAngulaire)
            return
        end
        AcH1 = interp1_Simplified([R1, R2], [Ac1, Ac2], RH);
        AcH2 = interp1_Simplified([R2, R3], [Ac2, Ac3], RH);
        AlH1 = interp1_Simplified([R1, R2], [Al1, Al2], RH);
        AlH2 = interp1_Simplified([R2, R3], [Al2, Al3], RH);
        [Reflectivity_Ping, NbSamplesMoy_Ping, Angles_Ping, RxBeamIndex_Ping, AlongDist_Ping, Range_Ping, RangeBeam_Ping] = ...
            projection(Range(iBeam), RH, AcH1, AnH1, AlH1, BeamSamples(subH), xImageOut, iBeam, TxBeamIndex(iBeam), ...
            Reflectivity_Ping, NbSamplesMoy_Ping, Angles_Ping, RxBeamIndex_Ping, AlongDist_Ping, Range_Ping, RangeBeam_Ping, nbSamples_Ping);
        [Reflectivity_Ping, NbSamplesMoy_Ping, Angles_Ping, RxBeamIndex_Ping, AlongDist_Ping, Range_Ping, RangeBeam_Ping] = ...
            projection(Range(iBeam), RH, AcH2, AnH2, AlH2, BeamSamples(subH), xImageOut, iBeam, TxBeamIndex(iBeam), ...
            Reflectivity_Ping, NbSamplesMoy_Ping, Angles_Ping, RxBeamIndex_Ping, AlongDist_Ping, Range_Ping, RangeBeam_Ping, nbSamples_Ping);
        %         [Reflectivity_Ping, NbSamplesMoy_Ping, Angles_Ping, RxBeamIndex_Ping, AlongDist_Ping, Range_Ping, RangeBeam_Ping] = projection(Range(iBeam), XX, AcB, Entries(subEntries{iBeam}(subB)), '-go')
    end
elseif (R2 == R1) && (R2 > R3)
    if ~isempty(RH)
        AnH = interp1_Simplified([R2, R3], [An2, An3], RH);
        if any(abs(AnH) > SeuilRejetAngulaire)
            return
        end
        AcH = interp1_Simplified([R2, R3], [Ac2, Ac3], RH);
        AlH = interp1_Simplified([R2, R3], [Al2, Al3], RH);
        [Reflectivity_Ping, NbSamplesMoy_Ping, Angles_Ping, RxBeamIndex_Ping, AlongDist_Ping, Range_Ping, RangeBeam_Ping] = ...
            projection(Range(iBeam), RH, AcH, AnH, AlH, BeamSamples(subH), xImageOut, iBeam, TxBeamIndex(iBeam), ...
            Reflectivity_Ping, NbSamplesMoy_Ping, Angles_Ping, RxBeamIndex_Ping, AlongDist_Ping, Range_Ping, RangeBeam_Ping, nbSamples_Ping);
    end
    if ~isempty(RB)
        AnB = linspace(An1, An2, length(RB));
        if any(abs(AnB) > SeuilRejetAngulaire)
            return
        end
        AcB = linspace(Ac1, Ac2, length(RB)); %Ac2; %interp1_Simplified([R1, R2], [Ac1, Ac2], RB);
        AlB = linspace(Al1, Al2, length(RB));
        [Reflectivity_Ping, NbSamplesMoy_Ping, Angles_Ping, RxBeamIndex_Ping, AlongDist_Ping, Range_Ping, RangeBeam_Ping] = ...
            projection(Range(iBeam), RB, AcB, AnB, AlB, BeamSamples(subB), xImageOut, iBeam, TxBeamIndex(iBeam), ...
            Reflectivity_Ping, NbSamplesMoy_Ping, Angles_Ping, RxBeamIndex_Ping, AlongDist_Ping, Range_Ping, RangeBeam_Ping, nbSamples_Ping);
    end
elseif (R2 == R1) && (R2 < R3)
    if ~isempty(RH)
        AnH = linspace(An1, An2, length(RH));
        if any(abs(AnH) > SeuilRejetAngulaire)
            return
        end
        AcH = linspace(Ac1, Ac2, length(RH)); %Ac2; %interp1_Simplified([R1, R2], [Ac1, Ac2], RH);
        AlH = linspace(Al1, Al2, length(RH));
        [Reflectivity_Ping, NbSamplesMoy_Ping, Angles_Ping, RxBeamIndex_Ping, AlongDist_Ping, Range_Ping, RangeBeam_Ping] = ...
            projection(Range(iBeam), RH, AcH, AnH, AlH, BeamSamples(subH), xImageOut, iBeam, TxBeamIndex(iBeam), ...
            Reflectivity_Ping, NbSamplesMoy_Ping, Angles_Ping, RxBeamIndex_Ping, AlongDist_Ping, Range_Ping, RangeBeam_Ping, nbSamples_Ping);
    end
    if ~isempty(RB)
        AnB = interp1_Simplified([R2, R3], [An2, An3], RB);
        if any(abs(AnB) > SeuilRejetAngulaire)
            return
        end
        AcB = interp1_Simplified([R2, R3], [Ac2, Ac3], RB);
        AlB = interp1_Simplified([R2, R3], [Al2, Al3], RB);
        [Reflectivity_Ping, NbSamplesMoy_Ping, Angles_Ping, RxBeamIndex_Ping, AlongDist_Ping, Range_Ping, RangeBeam_Ping] = ...
            projection(Range(iBeam), RB, AcB, AnB, AlB, BeamSamples(subB), xImageOut, iBeam, TxBeamIndex(iBeam), ...
            Reflectivity_Ping, NbSamplesMoy_Ping, Angles_Ping, RxBeamIndex_Ping, AlongDist_Ping, Range_Ping, RangeBeam_Ping, nbSamples_Ping);
    end
elseif (R2 < R1) && (R2 == R3)
    if ~isempty(RH)
        AcH = linspace(Ac2, Ac3, length(RH)); %Ac2; %interp1_Simplified([R2, R3], [Ac2, Ac3], RH);
        AnH = linspace(An2, An3, length(RH));
        AlH = linspace(Al2, Al3, length(RH));
        [Reflectivity_Ping, NbSamplesMoy_Ping, Angles_Ping, RxBeamIndex_Ping, AlongDist_Ping, Range_Ping, RangeBeam_Ping] = ...
            projection(Range(iBeam), RH, AcH, AnH, AlH, BeamSamples(subH), xImageOut, iBeam, TxBeamIndex(iBeam), ...
            Reflectivity_Ping, NbSamplesMoy_Ping, Angles_Ping, RxBeamIndex_Ping, AlongDist_Ping, Range_Ping, RangeBeam_Ping, nbSamples_Ping);
    end
    if ~isempty(RB)
        AnB = interp1_Simplified([R1, R2], [An1, An2], RB);
        if any(abs(AnB) > SeuilRejetAngulaire)
            return
        end
        AcB = interp1_Simplified([R1, R2], [Ac1, Ac2], RB);
        AlB = interp1_Simplified([R1, R2], [Al1, Al2], RB);
        [Reflectivity_Ping, NbSamplesMoy_Ping, Angles_Ping, RxBeamIndex_Ping, AlongDist_Ping, Range_Ping, RangeBeam_Ping] = ...
            projection(Range(iBeam), RB, AcB, AnB, AlB, BeamSamples(subB), xImageOut, iBeam, TxBeamIndex(iBeam), ...
            Reflectivity_Ping, NbSamplesMoy_Ping, Angles_Ping, RxBeamIndex_Ping, AlongDist_Ping, Range_Ping, RangeBeam_Ping, nbSamples_Ping);
    end
elseif (R2 > R1) && (R2 == R3)
    if ~isempty(RH)
        AnH = interp1_Simplified([R1, R2], [An1, An2], RH);
        if any(abs(AnH) > SeuilRejetAngulaire)
            return
        end
        AcH = interp1_Simplified([R1, R2], [Ac1, Ac2], RH);
        AlH = interp1_Simplified([R1, R2], [Al1, Al2], RH);
        [Reflectivity_Ping, NbSamplesMoy_Ping, Angles_Ping, RxBeamIndex_Ping, AlongDist_Ping, Range_Ping, RangeBeam_Ping] = ...
            projection(Range(iBeam), RH, AcH, AnH, AlH, BeamSamples(subH), xImageOut, iBeam, TxBeamIndex(iBeam), ...
            Reflectivity_Ping, NbSamplesMoy_Ping, Angles_Ping, RxBeamIndex_Ping, AlongDist_Ping, Range_Ping, RangeBeam_Ping, nbSamples_Ping);
    end
    if ~isempty(RB)
        AcB = linspace(Ac2, Ac3, length(RB)); %Ac2; %interp1_Simplified([R2, R3], [Ac2, Ac3], RB);
        AnB = linspace(An2, An3, length(RB));
        AlB = linspace(Al2, Al3, length(RB));
        [Reflectivity_Ping, NbSamplesMoy_Ping, Angles_Ping, RxBeamIndex_Ping, AlongDist_Ping, Range_Ping, RangeBeam_Ping] = ...
            projection(Range(iBeam), RB, AcB, AnB, AlB, BeamSamples(subB), xImageOut, iBeam, TxBeamIndex(iBeam), ...
            Reflectivity_Ping, NbSamplesMoy_Ping, Angles_Ping, RxBeamIndex_Ping, AlongDist_Ping, Range_Ping, RangeBeam_Ping, nbSamples_Ping);
    end
elseif R2 == R1
    if ~isempty(RH)
        AcH = linspace(Ac1, Ac3, length(RH));
        AnH = linspace(An1, An3, length(RH));
        AlH = linspace(Al1, Al3, length(RH));
        [Reflectivity_Ping, NbSamplesMoy_Ping, Angles_Ping, RxBeamIndex_Ping, AlongDist_Ping, Range_Ping, RangeBeam_Ping] = ...
            projection(Range(iBeam), RH, AcH, AnH, AlH, BeamSamples(subH), xImageOut, iBeam, TxBeamIndex(iBeam), ...
            Reflectivity_Ping, NbSamplesMoy_Ping, Angles_Ping, RxBeamIndex_Ping, AlongDist_Ping, Range_Ping, RangeBeam_Ping, nbSamples_Ping);
    end
    if ~isempty(RB)
        AcB = linspace(Ac1, Ac3, length(RB));
        AnB = linspace(An1, An3, length(RB));
        AlB = linspace(Al1, Al3, length(RB));
        [Reflectivity_Ping, NbSamplesMoy_Ping, Angles_Ping, RxBeamIndex_Ping, AlongDist_Ping, Range_Ping, RangeBeam_Ping] = ...
            projection(Range(iBeam), RB, AcB, AnB, AlB, BeamSamples(subB), xImageOut, iBeam, TxBeamIndex(iBeam), ...
            Reflectivity_Ping, NbSamplesMoy_Ping, Angles_Ping, RxBeamIndex_Ping, AlongDist_Ping, Range_Ping, RangeBeam_Ping, nbSamples_Ping);
    end
% else
%     messageForDebugInspection
end
flag = 1;
