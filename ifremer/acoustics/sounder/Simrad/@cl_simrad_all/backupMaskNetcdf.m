function flag = backupMaskNetcdf(this)

%% Test d'existence du fichier Netcdf

[nomDir, nomFic] = fileparts(this.nomFic);
nomFicNc = fullfile(nomDir, 'SonarScope', [nomFic '.nc']);
flag = exist(nomFicNc, 'file');
if ~flag
    return
end

%% Transfert Mask => MaskBackup

ncID  = netcdf.open(nomFicNc, 'WRITE');
grpID = netcdf.inqNcid(ncID, 'Depth');
varID = netcdf.inqVarID(grpID, 'Mask');
Mask  = netcdf.getVar(grpID, varID);
varID = netcdf.inqVarID(grpID, 'MaskBackup');
NetcdfUtils.putVarVal(grpID, varID, Mask, 1)
netcdf.close(ncID);
