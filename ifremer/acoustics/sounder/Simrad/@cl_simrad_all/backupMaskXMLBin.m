function flag = backupMaskXMLBin(this)

%% Lecture de l'image

[flag, DataDepth] = read_depth_bin(this, 'Memmapfile', 1);
if ~flag
    return
end

%% Sauvegarde de la version pr�c�dente

k = strcmp({DataDepth.Images.Name}, 'Mask');
[nomDir, nomFic] = fileparts(this.nomFic);
nomFicMask = fullfile(nomDir, 'SonarScope', nomFic, DataDepth.Images(k).FileName);
if ~exist(nomFicMask,'file')
    flag = 0;
    return
end
clear DataDepth

[nomDirMask, nomFicMaskSave] = fileparts(nomFicMask);
nomFicMaskSave = fullfile(nomDirMask, [nomFicMaskSave '_Save.bin']);
flag = copyfile(nomFicMask, nomFicMaskSave);
if flag
    str1 = sprintf('Le fichier "%s" a �t� copi� dans une version de sauvegarde que vous pouvez retrouver par "R�tablir Masks".', nomFicMask);
    str2 = sprintf('File "%s" has been saved in a backup version. You can retreive the mask by "Restore masks".', nomFicMask);
    my_warndlg(Lang(str1,str2), 0, 'Tag', 'SauvegardeFichierMasque');
else
    str1 = sprintf('Le fichier "%s" n''a pas pu �tre copi� dans une version de sauvegarde.', nomFicMask);
    str2 = sprintf('File "%s" could not be saved in a backup version.', nomFicMask);
    my_warndlg(Lang(str1,str2), 0, 'Tag', 'SauvegardeFichierMasque');
end
