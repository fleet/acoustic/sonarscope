% Representation externe d'une instance dans une chaine de caracteres
%
% Syntax 
%   s = char(aKM)
%
% Input Arguments
%   aKM : instance ou tableau d'instances de cl_simrad_all
%
% Output Arguments
%   s : chaine de caracteres
%
% Examples
%   aKM = cl_simrad_all('Image', Lena);
%   str = char(aKM)
%
% See also cl_simrad_all Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function str = char(this)

str = [];
[m, n] = size(this);

if (m*n) > 1

    %% Tableau d'instances

    for i=1:m
        for j=1:n
            str{end+1} = sprintf('-----Element (%d,%d) -------', i, j); %#ok<AGROW>
            str1 = char(this(i, j));
            str{end+1} = str1; %#ok<AGROW>
        end
    end
    str = cell2str(str);

else

    %% Instance unique

    str = char_instance(this);
    str = [repmat(' ', size(str,1), 2) str];
end
