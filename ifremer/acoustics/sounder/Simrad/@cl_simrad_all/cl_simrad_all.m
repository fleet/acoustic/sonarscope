% Constructeur de cl_simrad_all (Imagerie contenue dans une .all)
%
% Syntax
%   aKM = cl_simrad_all(...)
% 
% Name-Value Pair Arguments
%   nomFic : Nom(s) du fichier .all
%  
% Output Arguments 
%   aKM : Instance de cl_simrad_all
%
% Examples
%   nomFic = getNomFicDatabase('EM1002_BELGICA_005053_raw.all')
%   aKM = cl_simrad_all('nomFic', nomFic)
%
% See also plot_index histo_index Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = cl_simrad_all(varargin)

%% D�finition de la structure

this.nomFic             = '';
this.nomFicWC           = '';
this.nomFicIndex        = '';
this.flagRTK            = 0;
this.EmModel            = [];
this.SystemSerialNumber = [];
this.Version            = [];
this.SousVersion        = [];

%% Cr�ation de l'instance

this = class(this, 'cl_simrad_all');

%% On regarde si l'objet a �t� cr�e uniquement pour atteindre une m�thode de la classe

if (nargin == 1) && isempty(varargin{1})
    return
end

%% Initialisation des valeurs transmises lors de l'appel au constructeur

this = set(this, varargin{:});
