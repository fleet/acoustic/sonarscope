% Cr�ation de la r�flectivit� par faisceaux � partir de la r�flectivit� de seabed image datagrams
%
% Syntax
%   flag = computeReflectivityByBeam(aKM, ...)
%
% Input Arguments
%   aKM : Instance de cl_simrad_all
%
% Output Arguments
%   flag  : 1=OK, 0=KO
%
% Examples
%   nomFic = getNomFicDatabase('0011_20110307_211804_IKATERE_02.all');
%   aKM = cl_simrad_all('nomFic', nomFic);
%   flag = computeReflectivityByBeam(aKM);
%
% See also cl_simrad_alli Authors
% Authors  : JMA
%-------------------------------------------------------------------------------

function [flag, DataDepth, DataRaw, DataImage] = computeReflectivityByBeam(this, DataDepth, varargin) %DataSeabedImage.ReflectivityByBeam;

global NUMBER_OF_PROCESSORS %#ok<GVMIS>

[varargin, IdentAlgoSnippets2OneValuePerBeam] = getPropertyValue(varargin, 'IdentAlgoSnippets2OneValuePerBeam', 2); % 1=Moyenne en dB, 2=Amplitude, 3=Energy, 4=Median
[varargin, ForceMatlab] = getPropertyValue(varargin, 'ForceMatlab', 0);
[varargin, Memmapfile]  = getPropertyValue(varargin, 'Memmapfile',  -1); %#ok<ASGLU>

DataRaw = [];

[Version, SousVersion] = version_datagram(this);

%% Lecture de l'imagerie

[flag, DataImage] = read_seabedImage_bin(this, 'Memmapfile', -1);
if ~flag
    return
end
% ImagePingCounter = DataImage.PingCounter(:,:);

%{
FigUtils.createSScFigure;
subplot(8,1,1); plot(DataImage.PingCounter(:,:)); title('PingCounter');
subplot(8,1,2); plot(DataImage.SamplingRate(:,:)); title('SamplingRate');
subplot(8,1,3); plot(DataImage.TVGN(:,:)); title('TVGN');
subplot(8,1,4); plot(DataImage.BSN(:,:)); title('BSN');
subplot(8,1,5); plot(DataImage.BSO(:,:)); title('BSO');
subplot(8,1,6); plot(DataImage.TxBeamWidth(:,:)); title('TxBeamWidth');
subplot(8,1,7); plot(DataImage.TVGCrossOver(:,:)); title('TVGCrossOver');
subplot(8,1,8); plot(DataImage.NbBeams(:,:)); title('NbBeams');

n1 = sum(DataImage.InfoBeamNbSamples(:,:),2)
FigUtils.createSScFigure; plot(n1); grid on;

%       InfoBeamSortDirection: [5714�800 cl_memmapfile]
%       InfoBeamDetectionInfo: [5714�800 cl_memmapfile]
%           InfoBeamNbSamples: [5714�800 cl_memmapfile]
%       InfoBeamCentralSample: [5714�800 cl_memmapfile]

%}


%% Lecture de Raw Range & Beam Angle

switch Version
    case 'V1'
        switch SousVersion
            case 'F' % Th�oriquement que pour EM3000 mais on a de l'EM1002 qui est � ce format et aussi de l'EM120 des allemands
                DataRaw = [];
                Range = DataDepth.Range; % Mofifi� le 06/11/2010
                flag = 1; % Rajout� par JMA le 15/09/2014 pour fichiers NIWA hydromap�s
            case 'f'
                [flag, DataRaw] = SSc_ReadDatagrams(this.nomFic, 'Ssc_RawRangeBeamAngle', 'Memmapfile', Memmapfile);
                if isempty(DataRaw)
                    Range = [];
                    flag = 1;
                else
                    Range = DataRaw.Range;
                end
        end
    case 'V2'
        [flag, DataRaw] = SSc_ReadDatagrams(this.nomFic, 'Ssc_RawRangeBeamAngle', 'Memmapfile', Memmapfile);
        % TODO : faire le m�nage ici ainsi que dans Simrad_maskEntries
        try
            Range = DataRaw.TwoWayTravelTime(:,:);
        catch %#ok<CTCH>
            Range = DataRaw.TwoWayTravelTimeInSeconds(:,:);
        end
        %         SamplingRate = DataRaw.SamplingRate(:,:);
        
        [subDepth, subRaw, subImage] = intersect3(DataDepth.PingCounter(:,1), DataRaw.PingCounter(:,1), DataImage.PingCounter(:,1)); %#ok<ASGLU>
        
        
        SamplingRate = DataImage.SamplingRate(subImage,:);
        %         for iPing=1:DataImage.Dimensions.nbPings
        for iPing=1:length(subImage)
            %             Range(iPing,:) = Range(iPing,:) * SamplingRate(iPing,1); % TODO : Attention si sondeur DUAL
            Range(subRaw(iPing),:) = Range(subRaw(iPing),:) * SamplingRate(iPing,1); % TODO : Attention si sondeur DUAL
        end
end
if ~flag
    return
end

%% Mise en correspondance des pings

sub1 = find(~isnan(DataDepth.PingCounter(:,1)));
sub2 = find(~isnan(DataImage.PingCounter(:,1)));
if isempty(DataRaw) % Fichiers hydromap�s
    sub3 = sub2;
    [sublDepth, sublImage, sublRaw] = intersect3(DataDepth.PingCounter(sub1,1), DataImage.PingCounter(sub2,1), DataImage.PingCounter(sub2,1));
else
    sub3 = find(~isnan(DataRaw.PingCounter(:,1)));
    [sublDepth, sublImage, sublRaw] = intersect3(DataDepth.PingCounter(sub1,1), DataImage.PingCounter(sub2,1), DataRaw.PingCounter(sub3,1));
end
sublDepth = sub1(sublDepth);
sublImage = sub2(sublImage);
if isempty(DataRaw)
    sublRaw = sublDepth;
else
    sublRaw = sub3(sublRaw);
end

%% Lecture du r�pertoire SonarScope

[nomDir, nom] = fileparts(this.nomFic);
nomDir = fullfile(nomDir, 'SonarScope', nom);
if ~exist(nomDir, 'dir')
    return
end

switch Version
    case 'V1'
        nomFicXml = fullfile(nomDir, 'ALL_SeabedImage53h.xml');
    case 'V2'
        nomFicXml = fullfile(nomDir, 'ALL_SeabedImage59h.xml');
end
flag = exist(nomFicXml, 'file');
if ~flag
    special_message('FichierNonExistant', nomFicXml)
    return
end
% edit(nomFicXml)
Datagrams = xml_read(nomFicXml);

[nomDirSignal, nomFicSignal] = fileparts(nomFicXml);
nomDirSignal = fullfile(nomDirSignal, nomFicSignal);

%% Lecture des samples de l'imagerie

k = findIndVariable(Datagrams.Tables, 'Entries');
[flag, Entries] = readSignal(nomDirSignal, Datagrams.Tables(k), []);
if ~flag
    str1 = sprintf('Message pour JMA : Pb lecture "Entries" pour fichier "%s", envoyez le fichier .all correspondant � JMA s''il vous plait.', nomFicXml);
    str2 = sprintf('Message for JMA : There is a pb for reading "Entries" for file "%s", please send the corresponding .all file to JMA.', nomFicXml);
    my_warndlg(Lang(str1,str2), 0, 'Tag', 'ProblemeLectureEntries');
end

switch Datagrams.Tables(k).Storage
    case 'uint8' % Erreur dans .xml ne devrait plus exister
        Datagrams.Tables(k).Storage = 'int8';
        Entries = code2val(Entries, Datagrams.Tables(k), 'single');
        Entries = Entries * 0.5;
    case 'int8'
        Entries = code2val(Entries, Datagrams.Tables(k), 'single');
        if ~isfield(Datagrams.Tables(k), 'ScaleFactor') % Correction oubli GLU
            Entries = Entries * 0.5;
        end
    case 'int16'
        Entries = code2val(Entries, Datagrams.Tables(k), 'single');
end

%% Construction de l'image

nbPings = DataDepth.Dimensions.nbPings;
% nbBeams = DataImage.Dimensions.nbBeams; % Corrig� le 17/03/2010 fichier NIWA 0001_20081027_184032_raw
nbBeams = DataDepth.Dimensions.nbBeams;
InfoBeamNbSamples = DataImage.InfoBeamNbSamples(:,:);
% InfoBeamSortDirection = DataImage.InfoBeamSortDirection(:,:);

%% Correctif bug d'enregistrement de lu ME70 : les pings sont r�p�t�s deux fois

Entries = correctifEntriesPourME70(Entries, DataImage);

%% En cours de test, fait le 04/04/2015

maskEntries = true(size(Entries));

%{
InfoBeamCentralSample = DataImage.InfoBeamCentralSample(:,:);
maskEntries = Simrad_maskEntries2(Entries, InfoBeamNbSamples, InfoBeamCentralSample, InfoBeamSortDirection, ...
sublDepth, sublImage, sublRaw, SeabedImageFormat, isDual, DataDepth, DataRaw, DataImage);
if 1
%}

%% Fin en cours de test

if (NUMBER_OF_PROCESSORS == 0) || ForceMatlab
    ReflectivityByBeam = NaN(nbPings, nbBeams, 'single');
    ReflectivityByBeam = Simrad_CreateReflectivityByBeamNew(ReflectivityByBeam, Entries, InfoBeamNbSamples, ...
        sublDepth, sublImage, maskEntries, IdentAlgoSnippets2OneValuePerBeam); %, DataImage.TVGN(:,:), Range(:,:), sublRaw);
else
    try
        % Maintenant
        nbProc = str2double(getenv('NUMBER_OF_PROCESSORS'));
        pppp = NaN(size(InfoBeamNbSamples,1), nbBeams, 'single');
        
        pppp = Simrad_CreateReflectivityByBeam_mexmc(pppp, Entries, InfoBeamNbSamples, int32(nbProc), IdentAlgoSnippets2OneValuePerBeam);
        ReflectivityByBeam = NaN(nbPings, nbBeams, 'single');
        ReflectivityByBeam(sublDepth,:) = pppp(sublImage,:);
        clear pppp
    catch %#ok<CTCH>
        str1 = 'La tentative d''acc�l�ration logicielle (programme C appel�) a apparemment �chou�, je reviens � la version Matlab.';
        str2 = 'The software acceleration does not seem to work (calling C code), I redo the processing using Matlab code.';
        my_warndlg(Lang(str1,str2), 0, 'Tag', 'BUGSimrad_CreateReflectivityByBeam_mexmc');
        
        ReflectivityByBeam = NaN(nbPings, nbBeams, 'single');
        ReflectivityByBeam = Simrad_CreateReflectivityByBeamNew(ReflectivityByBeam, Entries, InfoBeamNbSamples, ...
        sublDepth, sublImage, maskEntries, IdentAlgoSnippets2OneValuePerBeam); %, DataImage.TVGN(:,:), Range(:,:), sublRaw, IdentAlgoSnippets2OneValuePerBeam);
    end
end
clear pppp InfoBeamNbSamples

ReflectivityByBeam(isinf(ReflectivityByBeam)) = NaN;
ReflectivityByBeamWithoutSpecularRestablishment = ReflectivityByBeam;

%% Correction du sp�culaire

SpecularCompensation = 1;
if SpecularCompensation
    subBabImage = 1:floor(nbBeams/2);
    subTriImage = floor(nbBeams/2)+1:nbBeams;
    BSO = DataImage.BSO(:,:);
    BSN = DataImage.BSN(:,:);
    Rn  = DataImage.TVGN(:,:);
    
    try
        TVGCrossOver = DataImage.TVGCrossOver(:,:);
    catch %#ok<CTCH>
        TVGCrossOver = DataImage.TVGCrossOver;
    end
    
    RangeMinPerPing = min(Range(:,:), [], 2);
%     RangeMinPerPingBab = min(Range(:,subBabImage), [], 2);
%     RangeMinPerPingTri = min(Range(:,subTriImage), [], 2);
%     FigUtils.createSScFigure; PlotUtils.createSScPlot(RangeMinPerPingBab, 'r*'); hold on; PlotUtils.createSScPlot(RangeMinPerPingTri, 'g*'); PlotUtils.createSScPlot(Rn, 'b'); grid on;
    
    N = length(sublDepth);
    for iPing=1:N
        iPingRaw   = sublRaw(iPing);
        iPingImage = sublImage(iPing);
        iPingDepth = sublDepth(iPing);
        R = Range(iPingRaw,:);
        
        % Correctif au cas o� c'est un EM3000D (NIWA)
        R = modifyRForSpecularRestorationForEM3000D(R, DataDepth.EmModel);
        
        if strcmp(Version, 'V1') && strcmp(SousVersion, 'F') && (DataDepth.EmModel == 1002) % Test pour donn�es EM1002 de Noum�a (CALICO - Charline)
            RangeNormal = Rn(iPingImage,:) * (DataDepth.SamplingRate(iPingDepth,:) / 11973); %DataDepth.SamplingRate(1,:));
        else
            RangeNormal = Rn(iPingImage,:);
        end
        
        switch DataDepth.EmModel
            case 304
                RangeNormal = RangeMinPerPing(iPingRaw);
                DataImage.TVGNRecomputed(iPing,:) = RangeNormal;
            case 3020
                %{
                % Cas o� on ne fait pas confiance aux Rn d�livr�s par KM
                if length(RangeNormal) == 2 % EM3002D
                    RangeNormalBab = RangeMinPerPingBab(iPingRaw);
                    RangeNormalTri = RangeMinPerPingTri(iPingRaw);
                    RangeNormal = [RangeNormalBab RangeNormalTri];
                    DataImage.TVGNRecomputed(iPing,:) = RangeNormal;
                end
                %}
                n = length(R);
                subTri = (floor(n/2)+1):n;
                R(subTri) = R(subTri) - 6;
        end
        
        ReflectivityByBeam(iPingDepth,:) = Simrad_CorrectionSpeculaire(ReflectivityByBeam(iPingDepth,:), ...
            BSN(iPingImage,:), BSO(iPingImage,:), RangeNormal, R, ...
            subBabImage, subTriImage, Version, SousVersion, TVGCrossOver(iPingImage,:));
    end
end

if DataDepth.EmModel == 304
	flag = write_SeabedImage_bin(this, 'TVGNRecomputed', DataImage.TVGNRecomputed(:)); %#ok<NASGU>
end
        
% clear DataDepthReflectivity DataDepthBeamDepressionAngle
clear DataDepthBeamDepressionAngle

%% Ecriture du fichier "ReflectivityFromSnippets"

DataDepth.ReflectivityFromSnippets = ReflectivityByBeam;
DataDepth.ReflectivityFromSnippetsWithoutSpecularRestablishment = ReflectivityByBeamWithoutSpecularRestablishment; % re-establishment 

[nomDir, nomFicSeul] = fileparts(this.nomFic);
nomDirRacine = fullfile(nomDir, 'SonarScope', nomFicSeul);

Info.Storage  = 'single';

Info.FileName = fullfile('Ssc_Depth','ReflectivityFromSnippets.bin');
flag = writeImage(nomDirRacine, Info, DataDepth.ReflectivityFromSnippets);
if ~flag
    return
end

Info.FileName = fullfile('Ssc_Depth','ReflectivityFromSnippetsWithoutSpecularRestablishment.bin');
flag = writeImage(nomDirRacine, Info, DataDepth.ReflectivityFromSnippetsWithoutSpecularRestablishment);
if ~flag
    return
end
