function flag = createNewLayerInNetcdfFile(allKM, X, grpName, varName, numType, unit)

nomFic = allKM.nomFic;
[nomDir, nom] = fileparts(nomFic);
ncFileName = fullfile(nomDir, 'SonarScope', [nom '.nc']);
    
flag = NetcdfUtils.createVarDef(ncFileName, grpName, varName, numType, unit, 'Depth');
if flag
    flag = save_image(allKM, 'Ssc_Depth', varName, X);
    if ~flag
        return
    end
end
