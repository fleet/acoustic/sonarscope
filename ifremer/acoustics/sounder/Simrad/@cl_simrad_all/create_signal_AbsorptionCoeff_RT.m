function [flag, signalContainer] = create_signal_AbsorptionCoeff_RT(this, varargin)

% TODO : compléter cette fonction pour les anciens formats (V1 f et F) :
% voir ce qui est fait dans ALL_AbsorpCoeff_RT

[varargin, DataDepth]   = getPropertyValue(varargin, 'DataDepth',   []);
[varargin, DataRuntime] = getPropertyValue(varargin, 'DataRuntime', []);
[varargin, DataRaw]     = getPropertyValue(varargin, 'DataRaw',     []);
[varargin, subDepth]    = getPropertyValue(varargin, 'subDepth',    []); %#ok<ASGLU>

if isempty(DataDepth)
    [flag, DataDepth] = read_depth_bin(this, [], 'Memmapfile', -1);
    if ~flag
        return
    end
end

if isempty(DataRaw)
    [flag, DataRaw] = read_rawRangeBeamAngle(this, 'Memmapfile', -1);
    if ~flag
        return
    end
end

if ~isempty(DataRaw) && isfield(DataRaw, 'MeanAbsorptionCoeff') % V2
    AbsorptionCoeff = DataRaw.MeanAbsorptionCoeff;
    
    Y = AbsorptionCoeff(:,:);
    for k=size(Y,2):-1:1
        X(:,k) = my_interp1_Extrap_PreviousThenNext(DataRaw.PingCounter(:,1), Y(:,k), DataDepth.PingCounter(:,1));
    end
else
    if isempty(DataRuntime)
        [flag, DataRuntime] = SSc_ReadDatagrams(this.nomFic, 'Ssc_Runtime');
        if ~flag
            return
        end
    end
    
    AbsorptionCoeff = DataRuntime.AbsorptionCoeff;
    
    Time = DataRuntime.Datetime;
    Y = AbsorptionCoeff(:,:);
    for k=size(Y,2):-1:1
        X(:,k) = my_interp1_Extrap_PreviousThenNext(Time, Y(:,k), DataDepth.Datetime(:,1));
    end
end

% TsubDepth = datetime(DataDepth.Time.timeMat, 'ConvertFrom', 'datenum');
TsubDepth = DataDepth.Datetime(:,1); % Modif JMA le 04/03/2020 pour lecture cache Netcdf
if ~isempty(subDepth)
    TsubDepth = TsubDepth(subDepth);
    X = X(subDepth,:);
end

timeSample = XSample('name', 'time', 'data', TsubDepth);

nbSectors = size(X,2);

% Creation of ySampleList
ySampleList = YSample.empty;


tagName = 'AbsorptionCoeff_RT';
if nbSectors == 1
    ySampleList(1) = YSample('data', X, 'unit', 'dB/km');
    ySampleList(end).name = tagName;
else
    for k=1:nbSectors
        sampleName = sprintf('%s, Ind=%d', tagName, k);
        ySampleList(k) = YSample('data', X(:,k), 'unit', 'dB/km');
        ySampleList(end).name = sampleName;
    end
end
signal = ClSignal('name', 'SignalFromAbsorptionCoeff_RT', 'xSample', timeSample, 'ySample', ySampleList, 'tag', tagName, 'linkType', 3, 'link', 'TxBeamIndex');
signal.originFilename = this.nomFic;

%% Create signalContainer

signalContainer = SignalContainer('signalList', signal, 'name', 'SignalContainerFromAbsorptionCoeff_RT', 'tag', tagName);
signalContainer.originFilename = this.nomFic;

flag = 1;
