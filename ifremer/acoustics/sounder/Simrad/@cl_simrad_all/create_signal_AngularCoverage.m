function [flag, Z] = create_signal_AngularCoverage(a, varargin)

[varargin, DataDepth] = getPropertyValue(varargin, 'DataDepth', []);
[varargin, DataRaw]   = getPropertyValue(varargin, 'DataRaw',   []); %#ok<ASGLU>

switch version_datagram(a)
    case 'V1'
        if isempty(DataDepth)
            [flag, DataDepth] = read_depth_bin(a, 'Memmapfile', 1);
            if ~flag
                Z.Data = [];
                return
            end
        end
        n1 = size(DataDepth.BeamDepressionAngle,1);
        Z.Data = NaN(n1, 1);
        for k=1:n1
            Teta = DataDepth.BeamDepressionAngle(k,:);
            Teta(isnan(Teta)) = [];
            if ~isempty(Teta)
                Z.Data(k) = abs(Teta(end) - Teta(1));
            end
        end
        Time     = DataDepth.Time;
        Datetime = DataDepth.Datetime;
        
    case 'V2'
        if isempty(DataRaw)
            [flag, DataRaw] = read_rawRangeBeamAngle(a, 'Memmapfile', -1);
            if ~flag
                Z.Data = [];
                return
            end
        end
        n1 = size(DataRaw.BeamPointingAngle,1);
        Z.Data = NaN(n1, 1);
        for k=1:n1
            Teta = DataRaw.BeamPointingAngle(k,:);
            Teta(isnan(Teta)) = [];
            if ~isempty(Teta)
                Z.Data(k) = abs(Teta(end) - Teta(1));
            end
        end
        Time     = DataRaw.Time;
        Datetime = DataRaw.Datetime;
end

Z.Time     = Time;
Z.Datetime = Datetime;
Z.Unit = 'deg';

flag = 1;
