function [flag, Z] = create_signal_BSNMinusBSO(this, varargin)

[varargin, DataSeabed] = getPropertyValue(varargin, 'DataSeabed', []); %#ok<ASGLU>

if isempty(DataSeabed)
    [flag, DataSeabed] = read_seabedImage_bin(this, 'Memmapfile', -1);
    if ~flag
        Z.Data = [];
        return
    end
end

Z.Time     = DataSeabed.Time;
Z.Datetime = DataSeabed.Datetime;
Z.Data     = DataSeabed.BSN(:,:) - DataSeabed.BSO(:,:);
Z.Unit     = 'dB';

flag = 1;
