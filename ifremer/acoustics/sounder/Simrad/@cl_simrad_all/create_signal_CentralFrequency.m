function [flag, Z] = create_signal_CentralFrequency(this, varargin)

[varargin, DataDepth] = getPropertyValue(varargin, 'DataDepth', []);
% [varargin, DataRuntime] = getPropertyValue(varargin, 'DataRuntime', []);
[varargin, DataRaw] = getPropertyValue(varargin, 'DataRaw', []); %#ok<ASGLU>
% [varargin, DataSeabed] = getPropertyValue(varargin, 'DataSeabed', []);

if isempty(DataDepth)
    [flag, DataDepth] = read_depth_bin(this, [], 'Memmapfile', 1);
    if ~flag
        Z.Data = [];
        return
    end
end

if isempty(DataRaw)
    [flag, DataRaw] = read_rawRangeBeamAngle(this, 'Memmapfile', 1);
    if ~flag
        Z.Data = [];
        return
    end
end

if ~isempty(DataRaw) && isfield(DataRaw, 'CentralFrequency')
    CentralFrequency = DataRaw.CentralFrequency;
    Time = DataRaw.Time;
else
    % TODO : apparemment on passe toujours au dessus
    %{
if isempty(DataSeabed)
[flag, DataSeabed] = SSc_ReadDatagrams(this.nomFic, 'Ssc_SeabedImage53h', 'Memmapfile', -1)
if ~flag
DataSeabed = [];
end
end
if isempty(DataSeabed)
if isempty(DataRuntime)
[flag, DataRuntime] = SSc_ReadDatagrams(this.nomFic, 'Ssc_Runtime'); % TODO : contrôler
if ~flag
Z.Data = [];
return
end
end
CentralFrequency = DataRuntime.CentralFrequency;
Time = DataRuntime.Time;
else
CentralFrequency = DataSeabed.MeanAbsorpCoeff;
Time = DataSeabed.Time;
end
    %}
end

Y = CentralFrequency(:,:);
for k=size(Y,2):-1:1
    X(:,k) = my_interp1_Extrap_PreviousThenNext(Time, Y(:,k), DataDepth.Time);
end

Z.Time     = DataDepth.Time;
Z.Datetime = DataDepth.Datetime;
Z.Data     = X / 1000;
Z.Unit     = 'kHz';

flag = 1;
