function [flag, Z] = create_signal_CourseVessel(this, varargin)

[varargin, IndNavUnit]   = getPropertyValue(varargin, 'IndNavUnit',   []);
[varargin, DataPosition] = getPropertyValue(varargin, 'DataPosition', []); %#ok<ASGLU>

if isempty(DataPosition)
    [flag, DataPosition] = read_position_bin(this, 'IndNavUnit', IndNavUnit, 'Memmapfile', 1);
    if ~flag
        Z.Data = [];
        return
    end
end

CourseVessel = DataPosition.CourseVessel(:,:);
CourseVessel(CourseVessel > 360) = NaN;

Z.Data     = CourseVessel;
Z.Time     = DataPosition.Time;
Z.Datetime = DataPosition.Datetime;
Z.Unit     = 'deg';

flag = 1;
