function [flag, Z] = create_signal_CurvatureRadius(a, varargin)

[varargin, IndNavUnit]   = getPropertyValue(varargin, 'IndNavUnit',   []);
[varargin, DataDepth]    = getPropertyValue(varargin, 'DataDepth',    []);
[varargin, DataPosition] = getPropertyValue(varargin, 'DataPosition', []);
[varargin, flagHisto]    = getPropertyValue(varargin, 'flagHisto',     0); %#ok<ASGLU>

if isempty(DataDepth)
    [flag, DataDepth] = read_depth_bin(a, 'Memmapfile', -1);
    if ~flag
        Z.Data = [];
        return
    end
end

T1 = DataDepth.PingCounter(:,1);

if isempty(DataPosition)
    [flag, DataPosition] = read_position_bin(a, 'IndNavUnit', IndNavUnit, 'Memmapfile', -1);
    if ~flag
        flag = 1;
        X = zeros(length(T1), 1);
        Z.Data = X;
        Z.Time = DataDepth.Time;
        Z.Unit = '';
        return
    end
end

Lat = interp1(DataPosition.Datetime, DataPosition.Latitude(:,:),  DataDepth.Datetime(:,1));
Lon = interp1(DataPosition.Datetime, DataPosition.Longitude(:,:), DataDepth.Datetime(:,1));

% TODO : brancher la fonction compute_InterPingDistance � la place de ce
% qui suit
% DistanceInterPings = compute_InterPingDistance(Lat, Lon);

Fuseau = floor((mean(Lon+180, 'omitnan'))/6 + 1);
LatMean = mean(Lat, 'omitnan');
if LatMean >= 0
    Hemi = 'N';
else
    Hemi = 'S';
end
Carto = cl_carto('Ellipsoide.Type', 11, 'Projection.Type', 3, ...
    'Projection.UTM.Fuseau', Fuseau, 'Projection.UTM.Hemisphere', Hemi);
[xFish, yFish] = latlon2xy(Carto, Lat, Lon);

subNonNaN = find(~isnan(xFish));
if length(subNonNaN) < 2
    flag = 1;
    X = zeros(length(T1), 1);
    Z.Data = X;
    Z.Time = DataDepth.Time;
    Z.Unit = '';
    return
end

% FigUtils.createSScFigure; PlotUtils.createSScPlot(xFish, yFish, 'b'); grid on;
Wc = 0.005;
[B,A] = butter(2,Wc);
xFish(subNonNaN) = my_filtfilt(B, A, xFish(subNonNaN), Wc);
yFish(subNonNaN) = my_filtfilt(B, A, yFish(subNonNaN), Wc);
% hold on; PlotUtils.createSScPlot(xFish, yFish, 'r'); grid on;

D = NaN(size(xFish));
r = abs(rayonCourbure(xFish(subNonNaN), yFish(subNonNaN)));
% FigUtils.createSScFigure; PlotUtils.createSScPlot(r, '-*'); grid on;
D(subNonNaN) = r;

n = 5;
if length(subNonNaN) > (3*n)
    D(subNonNaN) = filtfilt(ones(1,n)/n,1, D(subNonNaN));
    
    D(subNonNaN(1:n)) = D(subNonNaN(n));
    D(subNonNaN(end-(n-1):end)) = D(subNonNaN(end-(n-1)));
end

% [Val, str] = stats(D, 'Seuil', [3 95]);
% D(D > (Val.Moyenne+3*Val.Quant_x(2))) = NaN;
if flagHisto
    D(D > 200) = NaN;
    [N, bins] = histo1D(log10(D));
    sub = (N ~= 0);
    figure(87698); PlotUtils.createSScPlot(10.^bins, N, '-');
    grid on; hold on;
    title('Histogram of 1/CurvatureRadius')
    PlotUtils.createSScPlot(10.^bins(sub), N(sub), '*');
end
Z.Time     = DataDepth.Time;
Z.Datetime = DataDepth.Datetime(:,1);
Z.Data     = single(D);
Z.Unit     = 'm';

flag = 1;
