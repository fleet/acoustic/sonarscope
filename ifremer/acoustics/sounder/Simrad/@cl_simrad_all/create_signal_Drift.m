function [flag, Z] = create_signal_Drift(this, varargin)

[varargin, IndNavUnit]   = getPropertyValue(varargin, 'IndNavUnit',   []);
[varargin, DataPosition] = getPropertyValue(varargin, 'DataPosition', []); %#ok<ASGLU>

if isempty(DataPosition)
    [flag, DataPosition] = read_position_bin(this, 'IndNavUnit', IndNavUnit, 'Memmapfile', 1);
    if ~flag
        Z.Data = [];
        return
    end
end

if isfield(DataPosition, 'CourseVessel')
    CourseVessel = DataPosition.CourseVessel(:,:);
    CourseVessel(CourseVessel > 360) = NaN;
    if all(isnan(CourseVessel))
        CourseVessel = calCapFromLatLon(DataPosition.Latitude, DataPosition.Longitude);
    end
else
    CourseVessel = calCapFromLatLon(DataPosition.Latitude, DataPosition.Longitude);
end


Z.Data     = CourseVessel - DataPosition.Heading(:,:);
Z.Time     = DataPosition.Time;
Z.Datetime = DataPosition.Datetime;
Z.Unit     = 'deg';

flag = 1;
