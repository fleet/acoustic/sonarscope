function [flag, Z] = create_signal_Heading(this, varargin)

[varargin, IndNavUnit]   = getPropertyValue(varargin, 'IndNavUnit',   []);
[varargin, DataPosition] = getPropertyValue(varargin, 'DataPosition', []); %#ok<ASGLU>

if isempty(DataPosition)
    [flag, DataPosition] = read_position_bin(this, 'IndNavUnit', IndNavUnit, 'Memmapfile', 1);
    if ~flag
        Z.Data = [];
        return
    end
end

Z.Data     = DataPosition.Heading(:,:);
Z.Time     = DataPosition.Time;
Z.Datetime = DataPosition.Datetime;
Z.Unit     = 'deg';

flag = 1;
