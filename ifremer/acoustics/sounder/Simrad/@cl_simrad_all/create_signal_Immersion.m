function [flag, Z, DataDepth, DataAttitude] = create_signal_Immersion(a, varargin)

[varargin, DataDepth]    = getPropertyValue(varargin, 'DataDepth',    []);
[varargin, DataAttitude] = getPropertyValue(varargin, 'DataAttitude', []);
[varargin, subDepth]     = getPropertyValue(varargin, 'subDepth',     []); %#ok<ASGLU>

if isempty(DataDepth)
    [flag, DataDepth] = read_depth_bin(a, 'Memmapfile', -1);
    if ~flag
        Z.Data = [];
        return
    end
end

if isempty(subDepth)
    subDepth = 1:DataDepth.Dimensions.nbPings;
end

DataDepthDatetime = DataDepth.Datetime(subDepth,1);

if isempty(DataAttitude)
    [flag, DataAttitude] = read_attitude_bin(a, 'Memmapfile', 0);
    if ~flag
        Z.Data = [];
        return
    end
end

try
%     TransducerDepth = -DataDepth.TransducerDepth(subDepth,:);
    TransducerDepth = -abs(DataDepth.TransducerDepth(subDepth,:)); % Modif JMA le 29/04/2019 pour donn�es EM2040 NIWA
catch % TODO : trouver le bug
    %     TransducerDepth = -DataDepth.TransducerDepth(:,:);
    %     TransducerDepth = TransducerDepth();
    TransducerDepth = -abs(DataDepth.TransducerDepth(subDepth));
end

% Heave = my_interp1(DataAttitude.Datetime, DataAttitude.Heave(:), DataDepthDatetime, 'linear', 'extrap');

% clear X 
% if size(TransducerDepth,2) == 2
%     X(:,2) = TransducerDepth(:,2) + Heave;
%     X(:,1) = TransducerDepth(:,1) + Heave;
% else
%     X = TransducerDepth + Heave;
% end

X = TransducerDepth; % Modif JMA le 25/11/2020 (�changes de mail avec Herv�)

Z.Data     = X;
Z.Datetime = DataDepthDatetime;
Z.Unit     = 'm';

flag = 1;
