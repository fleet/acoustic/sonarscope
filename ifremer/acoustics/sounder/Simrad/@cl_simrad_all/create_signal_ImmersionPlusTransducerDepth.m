% TODO : MAYOBS15 changer de signe

function [flag, Z] = create_signal_ImmersionPlusTransducerDepth(this, varargin)

[varargin, DataDepth] = getPropertyValue(varargin, 'DataDepth', []); %#ok<ASGLU>

if isempty(DataDepth)
    [flag, DataDepth] = read_depth_bin(this, [], 'Memmapfile', -1);
    if ~flag
        Z.Data = [];
        return
    end
end

% Modifs JMA le 20/10/2020, pour plots AUV MAYOBS15

[flag, Z] = create_signal_vertDepth(this, 'DataDepth', DataDepth);
if ~flag
    return
end

[flag, TD] = create_signal_TransducerDepth(this, 'DataDepth', DataDepth);
if ~flag
    return
end

Z.Data = TD.Data - Z.Data;

flag = 1;
