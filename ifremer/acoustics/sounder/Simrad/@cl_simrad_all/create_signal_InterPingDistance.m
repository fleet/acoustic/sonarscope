function [flag, Z, speedMpS, speedNds, LengthLine] = create_signal_InterPingDistance(a, varargin)

[varargin, IndNavUnit]   = getPropertyValue(varargin, 'IndNavUnit',   []);
[varargin, DataDepth]    = getPropertyValue(varargin, 'DataDepth',    []);
[varargin, DataPosition] = getPropertyValue(varargin, 'DataPosition', []);
[varargin, flagHisto]    = getPropertyValue(varargin, 'flagHisto',    0);
[varargin, subDepth]     = getPropertyValue(varargin, 'subDepth',     []);
[varargin, Carto]        = getPropertyValue(varargin, 'Carto',        []); %#ok<ASGLU>

speedMpS   = NaN;
speedNds   = NaN;
LengthLine = NaN;

if isempty(DataDepth)
    [flag, DataDepth] = read_depth_bin(a, 'Memmapfile', -1);
    if ~flag
        Z.Data = [];
        return
    end
end

if isempty(subDepth)
    subDepth = 1:DataDepth.Dimensions.nbPings;
end

T1 = DataDepth.PingCounter(subDepth,1);

DataDepthDatetime = DataDepth.Datetime(subDepth,1);

if isempty(DataPosition)
    [flag, DataPosition] = read_position_bin(a, 'IndNavUnit', IndNavUnit, 'Memmapfile', 1);
    if ~flag
        flag = 1;
        X = zeros(length(T1), 1);
        Z.Data     = X;
        Z.Datetime = DataDepthDatetime;
        Z.Unit     = '';
        return
    end
end

if length(DataPosition.Latitude) < 2
    Z.Data = [];
    flag = 0;
    return
end

Lat = my_interp1(DataPosition.Datetime, DataPosition.Latitude(:,:),  DataDepthDatetime);
Lon = my_interp1(DataPosition.Datetime, DataPosition.Longitude(:,:), DataDepthDatetime);

D = compute_InterPingDistance(Lat, Lon, 'Carto', Carto);

if flagHisto
    D(D > 200) = NaN;
    [N, bins] = histo1D(D);
    sub = (N ~= 0);
    
%     figure(87698); PlotUtils.createSScPlot(bins, N, '-');
%     grid on; hold on;
%     title('Histogram of Distance Inter Pings')
%     PlotUtils.createSScPlot(bins(sub), N(sub), '*');
    
    figure(87698); semilogy(bins, N, '-');
    grid on; hold on;
    title('Histogram of Distance Inter Pings')
    semilogy(bins(sub), N(sub), '*');
end
Z.Data     = single(D);
Z.Datetime = DataDepthDatetime;
Z.Unit     = 'm';

LengthLine = sum(D, 'omitnan');
speedMpS = LengthLine / seconds(Z.Datetime(end)-Z.Datetime(1));
speedNds = speedMpS * 1.9438444924;

flag = 1;
