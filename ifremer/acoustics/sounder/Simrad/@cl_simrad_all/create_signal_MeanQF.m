function [flag, Z] = create_signal_MeanQF(this, varargin)

[varargin, DataQualityFactor] = getPropertyValue(varargin, 'DataQualityFactor', []); %#ok<ASGLU>

if isempty(DataQualityFactor)
    [flag, DataQualityFactor] = SSc_ReadDatagrams(this.nomFic, 'Ssc_QualityFactorFromIfr', 'Memmapfile', -1);
    if ~flag
        Z.Data = [];
        return
    end
end

Z.Time     = DataQualityFactor.Time;
Z.Datetime = DataQualityFactor.Datetime;

n1 = size(DataQualityFactor.IfremerQF,1);
Z.Data = NaN(n1, 1);
for k=1:n1
    QF = DataQualityFactor.IfremerQF(k,:);
    Z.Data(k) = mean(QF, 'omitnan');
end

Z.Unit = '';
