function [flag, Z] = create_signal_MinPulseLength(this, varargin)

[varargin, DataRaw] = getPropertyValue(varargin, 'DataRaw', []); %#ok<ASGLU>

if isempty(DataRaw)
    [flag, DataRaw] = read_rawRangeBeamAngle(this, 'Memmapfile', -1);
    if ~flag
        Z.Data = [];
        return
    end
end

Z.Time     = DataRaw.Time;
Z.Datetime = DataRaw.Datetime(:,1);
if isfield(DataRaw, 'SignalLength')
    Z.Data = 1000 * min(DataRaw.SignalLength(:,:), [], 2);
else % Cas du format V1-F EM1002
    [flag, DataRuntime] = SSc_ReadDatagrams(this.nomFic, 'Ssc_Runtime');
    if ~flag
        Z.Data = [];
        return
    end
    Z.Data = my_interp1_Extrap_PreviousThenNext(DataRuntime.Datetime, DataRuntime.TransmitPulseLength, Z.Datetime);
end
Z.Unit = 'ms';

flag = 1;
