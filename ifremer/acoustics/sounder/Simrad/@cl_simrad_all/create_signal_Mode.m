function [flag, Z, DataDepth, DataRuntime] = create_signal_Mode(this, varargin)

[varargin, DataDepth]   = getPropertyValue(varargin, 'DataDepth',   []);
[varargin, DataRuntime] = getPropertyValue(varargin, 'DataRuntime', []);
[varargin, subDepth]    = getPropertyValue(varargin, 'subDepth',    []); %#ok<ASGLU> % D�comment� par JMA le 29/03/2023 pour fichier WCD somm� "D:\Temp\SPFE\20230329\0004_20210301_104739_SimonStevin.all"

if isempty(DataDepth)
    [flag, DataDepth] = read_depth_bin(this, [], 'Memmapfile', -1);
    if ~flag
        Z.Data = [];
        return
    end
end

% D�comment� par JMA le 29/03/2023 pour fichier WCD somm� "D:\Temp\SPFE\20230329\0004_20210301_104739_SimonStevin.all"
if isempty(subDepth)
    subDepth = 1:DataDepth.Dimensions.nbPings;
end
% Fin d�commentation par JMA le 29/03/2023 pour fichier WCD somm� "D:\Temp\SPFE\20230329\0004_20210301_104739_SimonStevin.all"


DataDepthDatetime = DataDepth.Datetime(:,1); % Modif JMA le 04/03/2020 pour lecture cache Netcdf

if isempty(DataRuntime)
    [flag, DataRuntime] = SSc_ReadDatagrams(this.nomFic, 'Ssc_Runtime');
    if ~flag
        Z.Data = [];
        return
    end
end

Mode = my_interp1_Extrap_PreviousThenNext(DataRuntime.Datetime, DataRuntime.Mode, DataDepthDatetime);
if isnan(Mode) % Cas o� l'heure du premier ping est ant�rieure au premier datagramme Runtime (fichier 0004_20181010_172532_Panopee.all de l'ENSTA)
    Mode = DataRuntime.Mode(1);
end
switch DataDepth.EmModel
    case 2040
        Z.strData = {'200 kHz'; '300 kHz'; '400 kHz'};
        
    case 2045
        for k=1:23
            Z.strData{k} = sprintf('%d kHz', 180 + (k-1) * 10);
        end
        
    case 850
        if DataDepth.Dimensions.nbBeams <= 51
            Z.strData = {''; ''; 'Fisheries'; ''; ''};
        else
            Z.strData = {'xshort'; 'short'; 'long'; 'xlong'; 'FM'};
        end
        
    case 304
        Z.strData = {'Very shallow';'Shallow'; 'Medium'; 'Deep'; 'Deeper'; 'Very deep'; 'Extra deep'; 'Extreme Deep'};
                
    otherwise
        Z.strData = {'Very shallow'; 'Shallow'; 'Medium'; 'Deep'; 'Very deep'; 'Extra deep'};
end

% Z.Data = single(Mode');
Z.Data     = single(Mode(subDepth)); % Modif JMA le 02/05/2017
Z.Datetime = DataDepthDatetime(subDepth);
Z.Unit     = '';

flag = 1;

%{
FigUtils.createSScFigure; PlotUtils.createSScPlot(DataRuntime.Time.timeMat, DataRuntime.Mode(:), 'xb'); grid on; hold on;
PlotUtils.createSScPlot(DataDepth.Time.timeMat, Z.Data(:), 'or');
legend({'Runtime'; 'Depth'})
%}
