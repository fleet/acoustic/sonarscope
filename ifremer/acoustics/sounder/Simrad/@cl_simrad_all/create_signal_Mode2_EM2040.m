function [flag, Z] = create_signal_Mode2_EM2040(this, Mode, varargin)

[varargin, DataDepth] = getPropertyValue(varargin, 'DataDepth', []);
[varargin, DataRaw]   = getPropertyValue(varargin, 'DataRaw',   []);
[varargin, subDepth]  = getPropertyValue(varargin, 'subDepth',  []);
[varargin, subRaw]    = getPropertyValue(varargin, 'subRaw',    []); %#ok<ASGLU>

% Z1 = [];

if isempty(DataDepth)
    [flag, DataDepth] = read_depth_bin(this, [], 'Memmapfile', -1);
    if ~flag
        Z.Data = [];
        return
    end
end

if ~((DataDepth.EmModel == 2040) || (DataDepth.EmModel == 2045))
    flag = 0;
    Z.Data = [];
    return
end

if isempty(DataRaw)
    [flag, DataRaw] = read_rawRangeBeamAngle(this, 'Memmapfile', -1);
    if ~flag
        Z.Data = [];
        return
    end
end

% if isempty(subDepth)
%     subDepth = 1:DataDepth.Dimensions.nbPings;
% end
% if isempty(subRaw)
%     subRaw = 1:DataRaw.Dimensions.nbPings;
% end

if isempty(subDepth) || isempty(subRaw)
    [subDepth, subRaw] = intersect3(DataDepth.PingCounter(:,1), DataRaw.PingCounter(:,1), DataRaw.PingCounter(:,1));
end

CentralFrequency = DataRaw.CentralFrequency(subRaw,:) / 1000;
SignalLength     = DataRaw.SignalLength(subRaw,:);

DataRawDatetime   = DataRaw.Datetime(subRaw);
DataDepthDatetime = DataDepth.Datetime(subDepth,1);
if length(Mode) ~= length(subDepth)
    Mode = Mode(subDepth); % TODO : pas terrible tout �a !!! C'est dans le cas o� on ne passerait pas subDepth
end

try
    if DataDepth.EmModel == 2045
        [flag, Z] = create_signal_Mode2_EM2045_SingleSwath(DataDepthDatetime, size(Mode), DataRawDatetime, CentralFrequency); %, SignalLength);
    else
        % TODO : Attention : valable uniquement pour Single Swath pour le
        % moment et bug sur fichier
        % G:\PatrickJaussaud\ESNAUT2014\03122014-03\03122014-03-science\Cu\SMF_2040ForTrainingCourse\0022_20141204_022147_raw.all
        % pour lequel length(Mode) = 1694 et length(Mode) = 1695
        [flag, Z] = create_signal_Mode2_EM2040_unit(this.nomFic, DataDepthDatetime, Mode, DataRawDatetime, CentralFrequency, SignalLength);
    end
catch
    my_breakpoint
    str1 = 'Message pour JMA : bug rencontr� dans create_signal_Mode2_EM2040';
    str2 = 'Message for JMA : bug found in create_signal_Mode2_EM2040';
    my_warndlg(Lang(str1,str2), 0);
    flag = 0;
    Z.Data = [];
end


% TODO : ca peut �tre fait plus simplement avec l'info Scanning et nb de
% sectots. Voir cl_simrad_all/get_SonarModes
function [flag, Z] = create_signal_Mode2_EM2040_unit(nomFic, DataDepthDatetime, Mode, DataRawDatetime, CentralFrequency, SignalLength)

[nbPings, nbTx] = size(CentralFrequency); %#ok<ASGLU>
Mode2 = NaN(nbPings, 1);
for k=1:nbPings
    F = CentralFrequency(k,:);
    T = SignalLength(k,:) * 1000; % ms
    sub = ~isnan(F) & ~isnan(T);
    F = F(sub);
    T = T(sub);
    sub = (F ~= 0);
    F = F(sub);
    T = T(sub);

    if isempty(F) || isempty(T)
        continue
    end

    %% EM2040S

    %% 200 kHz

    %% Single Swath, Normal mode

    % TODO : compl�ter la partie EM2040D, remettre les valeurs de EM2040S et supprimer ensuite T(1:2)
    
    if length(F)>1 && checkFrequenciesAndPulseLength(F(1:2), [190 220], T(1:2), [0.07 0.07], [0.101 0.101]) % Normal mode S : Checked
        Mode(k)  = 1;
        Mode2(k) = 1;
    elseif length(F)>1 && checkFrequenciesAndPulseLength(F(1:2), [190 205], T(1:2), [0.2 0.2], [0.288 0.288]) % Normal mode M : Checked
        Mode(k)  = 1;
        Mode2(k) = 1;
    elseif length(F)>1 && checkFrequenciesAndPulseLength(F(1:2), [190 200], T(1:2), [0.6 0.6], [0.865 0.865]) % Normal mode L : Checked
        Mode(k)  = 1;
        Mode2(k) = 1;
%     elseif isequal(F(1:2), [190 200]) && isequal(T(1:2), [3 3]) % Normal mode FM : Checked
    elseif length(F)>1 && checkFrequenciesAndPulseLength(F(1:2), [190 200], T(1:2), [3 3], []) % Normal mode FM : Checked
        Mode(k)  = 1;
        Mode2(k) = 1;
%     elseif isequal(F(1:2), [190 200]) && isequal(T(1:2), [12 12]) % Normal mode FM : Checked
    elseif length(F)>1 && checkFrequenciesAndPulseLength(F(1:2), [190 200], T(1:2), [12 12], []) % Normal mode FM : Checked
        Mode(k)  = 1;
        Mode2(k) = 1;

        %% Single Swath, Single sector
%     elseif isequal(F, 200) &&( isequal(T, 0.035) || isNearlyEqual(T, 0.051, 0.001)) % Normal mode S : Checked
    elseif checkFrequenciesAndPulseLength(F, 200, T, 0.035, 0.051) % Normal mode S : Checked
        Mode(k)  = 1;
        Mode2(k) = 2;
%     elseif isequal(F, 200) && (isequal(T, 0.07) || isNearlyEqual(T, 0.101, 0.001)) % Normal mode M : Checked
    elseif checkFrequenciesAndPulseLength(F, 200, T, 0.07, 0.101) % Normal mode M : Checked
        Mode(k)  = 1;
        Mode2(k) = 2;
%     elseif isequal(F, 200) && (isequal(T, 0.15) || isNearlyEqual(T, 0.216, 0.001)) % Normal mode L : Checked
    elseif checkFrequenciesAndPulseLength(F, 200, T, 0.15, 0.216) % Normal mode L : Checked
        Mode(k)  = 1;
        Mode2(k) = 2;
%     elseif isequal(F, 200) && isequal(T, 1.5) % Normal mode FM : Checked
    elseif checkFrequenciesAndPulseLength(F, 200, T, 1.5, []) % Normal mode FM : Checked
        Mode(k)  = 1;
        Mode2(k) = 2;

%         %% Single Swath, Scanning
% %     elseif isequal(F, [200 200]) && (isequal(T, [0.035 0.035]) || isNearlyEqual(T, [0.051 0.051], 0.001)) % Normal mode S
%     elseif checkFrequenciesAndPulseLength(F, [200 200], T, [0.035 0.035], [0.051 0.051]) % Normal mode S
%         my_breakpoint
%         Mode(k)  = 1;
%         Mode2(k) = 3;
% %     elseif isequal(F, [200 200]) && (isequal(T, [0.07 0.07]) || isNearlyEqual(T, [0.101 0.101], 0.001)) % Normal mode M
%     elseif checkFrequenciesAndPulseLength(F, [200 200], T, [0.07 0.07], [0.101 0.101]) % Normal mode M
%         my_breakpoint
%         Mode(k)  = 1;
%         Mode2(k) = 3;
% %     elseif isequal(F, [200 200]) && (isequal(T, [0.15 0.15]) || isNearlyEqual(T, [0.216 0.216], 0.001)) % Normal mode L
%     elseif checkFrequenciesAndPulseLength(F, [200 200], T, [0.15 0.15], [0.216 0.216]) % Normal mode L
%         my_breakpoint
%         Mode(k)  = 1;
%         Mode2(k) = 3;
% %     elseif isequal(F, [200 200]) && isequal(T, [1.5 1.5]) % Normal mode FM
%     elseif checkFrequenciesAndPulseLength(F, [200 200], T, [1.5 1.5], []) % Normal mode FM
%         Mode(k)  = 1;
%         Mode2(k) = 3;

        %% Dual Swath, Normal mode
%     elseif (isequal(F, [190 210]) || isequal(F, [230 250])) && isequal(T, [0.1 0.1]) % Normal mode S : Checked
    elseif checkFrequenciesAndPulseLength(F, [190 210], T, [0.1 0.1], []) % Normal mode S : Checked
        Mode(k)  = 1;
        Mode2(k) = 1;
    elseif checkFrequenciesAndPulseLength(F, [230 250], T, [0.1 0.1], []) % Normal mode S : Checked
        Mode(k)  = 1;
        Mode2(k) = 1;
%     elseif (isequal(F, [190 200]) || isequal(F, [210 220]))&& isequal(T, [0.2 0.2]) % Normal mode M : Checked
    elseif checkFrequenciesAndPulseLength(F, [190 200], T, [0.2 0.2], []) % Normal mode M : Checked
        Mode(k)  = 1;
        Mode2(k) = 1;
    elseif checkFrequenciesAndPulseLength(F, [210 220], T, [0.2 0.2], []) % Normal mode M : Checked
        Mode(k)  = 1;
        Mode2(k) = 1;
%     elseif (isequal(F, [190 195]) || isequal(F, [200 205])) && isequal(T, [0.6 0.6]) % Normal mode L : Checked
    elseif checkFrequenciesAndPulseLength(F, [190 195], T, [0.6 0.6], []) % Normal mode L : Checked
        Mode(k)  = 1;
        Mode2(k) = 1;
    elseif checkFrequenciesAndPulseLength(F, [200 205], T, [0.6 0.6], []) % Normal mode L : Checked
        Mode(k)  = 1;
        Mode2(k) = 1;
%     elseif (isequal(F, [190 195]) || isequal(F, [200 205])) && (isequal(T, [3 3]) || isequal(T, [12 12])) % Normal mode FM : Checked
    elseif checkFrequenciesAndPulseLength(F, [190 195], T, [3 3], []) % Normal mode FM : Checked
        Mode(k)  = 1;
        Mode2(k) = 1;
    elseif checkFrequenciesAndPulseLength(F, [200 205], T, [3 3], []) % Normal mode FM : Checked
        Mode(k)  = 1;
        Mode2(k) = 1;
    elseif checkFrequenciesAndPulseLength(F, [190 195], T, [12 12], []) % Normal mode FM : Checked
        Mode(k)  = 1;
        Mode2(k) = 1;
    elseif checkFrequenciesAndPulseLength(F, [200 205], T, [12 12], []) % Normal mode FM : Checked
        Mode(k)  = 1;
        Mode2(k) = 1;

        %% Dual Swath, Single sector
%     elseif (isequal(F, 190) || isequal(F, 240)) &&( isequal(T, 0.035) || isNearlyEqual(T, 0.051, 0.001)) % Normal mode S : Checked
    elseif checkFrequenciesAndPulseLength(F, 190, T, 0.035, 0.051) % Normal mode S : Checked
        Mode(k)  = 1;
        Mode2(k) = 2;
    elseif checkFrequenciesAndPulseLength(F, 240, T, 0.035, 0.051) % Normal mode S : Checked
        Mode(k)  = 1;
        Mode2(k) = 2;
%     elseif (isequal(F, 190) || isequal(F, 220)) && (isequal(T, 0.07) || isNearlyEqual(T, 0.101, 0.001)) % Normal mode M : Checked
    elseif checkFrequenciesAndPulseLength(F, 190, T, 0.07, 0.101) % Normal mode M : Checked
        Mode(k)  = 1;
        Mode2(k) = 2;
    elseif checkFrequenciesAndPulseLength(F, 220, T, 0.07, 0.101) % Normal mode M : Checked
        Mode(k)  = 1;
        Mode2(k) = 2;
%     elseif (isequal(F, 190) || isequal(F, 205)) && (isequal(T, 0.15) || isNearlyEqual(T, 0.216, 0.001)) % Normal mode L : Checked
    elseif checkFrequenciesAndPulseLength(F, 190, T, 0.15, 0.216) % Normal mode L : Checked
        Mode(k)  = 1;
        Mode2(k) = 2;
    elseif checkFrequenciesAndPulseLength(F, 205, T, 0.15, 0.216) % Normal mode L : Checked
        Mode(k)  = 1;
        Mode2(k) = 2;
%     elseif (isequal(F, 190) || isequal(F, 205)) && isequal(T, 1.5) % Normal mode FM : Checked
    elseif checkFrequenciesAndPulseLength(F, 190, T, 1.5, []) % Normal mode FM : Checked
        Mode(k)  = 1;
        Mode2(k) = 2;
    elseif checkFrequenciesAndPulseLength(F, 205, T, 1.5, []) % Normal mode FM : Checked
        Mode(k)  = 1;
        Mode2(k) = 2;

        %% 300 kHz

        %% Single Swath, Normal mode
%     elseif isequal(F, [260 320 290]) && (isequal(T, [0.07 0.07 0.07]) || isNearlyEqual(T, [0.101 0.101 0.101], 0.001)) % Normal mode S : Checked
    elseif checkFrequenciesAndPulseLength(F, [260 320 290], T, [0.07 0.07 0.07], [0.101 0.101 0.101]) % Normal mode S : Checked
        Mode(k)  = 2;
        Mode2(k) = 1;
%     elseif isequal(F, [270 295 282.5]) && (isequal(T, [0.2 0.2 0.2]) || isNearlyEqual(T, [0.288 0.288 0.288], 0.001)) % Normal mode M : Checked
    elseif checkFrequenciesAndPulseLength(F, [270 295 282.5], T, [0.2 0.2 0.2], [0.288 0.288 0.288]) % Normal mode M : Checked
        Mode(k)  = 2;
        Mode2(k) = 1;
%     elseif isequal(F, [270 285 277.5]) && (isequal(T, [0.6 0.6 0.6]) || isNearlyEqual(T, [0.865 0.865 0.865], 0.001)) % Normal mode L : Checked
    elseif checkFrequenciesAndPulseLength(F, [270 285 277.5], T, [0.6 0.6 0.6], [0.865 0.865 0.865]) % Normal mode L : Checked
        Mode(k)  = 2;
        Mode2(k) = 1;
%     elseif isequal(F, [270 280 275]) && isequal(T, [2 2 2]) % Normal mode FM : Checked
    elseif checkFrequenciesAndPulseLength(F, [270 280 275], T, [2 2 2], []) % Normal mode FM : Checked
        Mode(k)  = 2;
        Mode2(k) = 1;
%     elseif isequal(F, [270 280 275]) && isequal(T, [6 6 6]) % Normal mode FM : Checked
    elseif checkFrequenciesAndPulseLength(F, [270 280 275], T, [6 6 6], []) % Normal mode FM : Checked
        Mode(k)  = 2;
        Mode2(k) = 1;

        %% Single Swath, Single sector
%     elseif isequal(F, 300) && (isequal(T, 0.035) || isNearlyEqual(T, 0.051, 0.001)) % Normal mode S : Checked
    elseif checkFrequenciesAndPulseLength(F, 300, T, 0.035, 0.051) % Normal mode S : Checked
        Mode(k)  = 2;
        Mode2(k) = 2;
%     elseif isequal(F, 300) && (isequal(T, 0.07) || isNearlyEqual(T, 0.101, 0.001)) % Normal mode M : Checked
    elseif checkFrequenciesAndPulseLength(F, 300, T, 0.07, 0.101) % Normal mode M : Checked
        Mode(k)  = 2;
        Mode2(k) = 2;
%     elseif isequal(F, 300) && (isequal(T, 0.15) || isNearlyEqual(T, 0.216, 0.001)) % Normal mode L : Checked
    elseif checkFrequenciesAndPulseLength(F, 300, T, 0.15, 0.216) % Normal mode L : Checked
        Mode(k)  = 2;
        Mode2(k) = 2;
%     elseif isequal(F, 300) && isequal(T, 1.5) % Normal mode FM : Checked
    elseif checkFrequenciesAndPulseLength(F, 300, T, 1.5, []) % Normal mode FM : Checked
        Mode(k)  = 2;
        Mode2(k) = 2;

        %% Single Swath, Scaning
%     elseif isequal(F, [290 300 290]) && (isequal(T, [0.035 0.035]) || isNearlyEqual(T, [0.051 0.051], 0.001)) % Normal mode S
    elseif checkFrequenciesAndPulseLength(F, [290 300 290], T, [0.035 0.035], [0.051 0.051]) % Normal mode S
        my_breakpoint
        Mode(k)  = 2;
        Mode2(k) = 3;
%     elseif isequal(F, [290 300 290]) && (isequal(T, [0.07 0.07]) || isNearlyEqual(T, [0.101 0.101], 0.001)) % Normal mode M
    elseif checkFrequenciesAndPulseLength(F, [290 300 290], T, [0.07 0.07], [0.101 0.101]) % Normal mode M
        my_breakpoint
        Mode(k)  = 2;
        Mode2(k) = 3;
%     elseif isequal(F, [290 300 290]) && (isequal(T, [0.15 0.15]) || isNearlyEqual(T, [0.216 0.216], 0.001)) % Normal mode L
    elseif checkFrequenciesAndPulseLength(F, [290 300 290], T, [0.15 0.15], [0.216 0.216]) % Normal mode L
        my_breakpoint
        Mode(k)  = 2;
        Mode2(k) = 3;
%     elseif isequal(F, [290 300 290]) && isequal(T, [1.5 1.5]) % Normal mode FM
    elseif checkFrequenciesAndPulseLength(F, [290 300 290], T, [1.5 1.5], []) % Normal mode FM
        my_breakpoint
        Mode(k)  = 2;
        Mode2(k) = 3;
        my_breakpoint

        %% Dual Swath, Normal mode
%     elseif (isequal(F, [260 320 260]) || isequal(F, [290 350 290])) && (isequal(T, [0.07 0.07 0.07]) || isNearlyEqual(T, [0.101 0.101 0.101], 0.001)) % Normal mode S : Checked
    elseif checkFrequenciesAndPulseLength(F, [260 320 260], T, [0.07 0.07 0.07], [0.101 0.101 0.101]) % Normal mode S : Checked
        Mode(k)  = 2;
        Mode2(k) = 1;
    elseif checkFrequenciesAndPulseLength(F, [290 350 290], T, [0.07 0.07 0.07], [0.101 0.101 0.101]) % Normal mode S : Checked
        Mode(k)  = 2;
        Mode2(k) = 1;
%     elseif (isequal(F, [260 300 270]) || isequal(F, [280 320 290]))&& (isequal(T, [0.2 0.2 0.2]) || isNearlyEqual(T, [0.288 0.288 0.288], 0.001)) % Normal mode M : Checked
    elseif checkFrequenciesAndPulseLength(F, [260 300 270], T, [0.2 0.2 0.2], [0.288 0.288 0.288]) % Normal mode M : Checked
        Mode(k)  = 2;
        Mode2(k) = 1;
    elseif checkFrequenciesAndPulseLength(F, [280 320 290], T, [0.2 0.2 0.2], [0.288 0.288 0.288]) % Normal mode M : Checked
        Mode(k)  = 2;
        Mode2(k) = 1;
%     elseif (isequal(F, [265 285 270]) || isequal(F, [275 290 280])) && (isequal(T, [0.6 0.6 0.6]) || isNearlyEqual(T, [0.865 0.865 0.865], 0.001)) % Normal mode L : Checked
    elseif checkFrequenciesAndPulseLength(F, [265 285 270], T, [0.6 0.6 0.6], [0.865 0.865 0.865]) % Normal mode L : Checked
        Mode(k)  = 2;
        Mode2(k) = 1;
    elseif checkFrequenciesAndPulseLength(F, [275 290 280], T, [0.6 0.6 0.6], [0.865 0.865 0.865]) % Normal mode L : Checked
        Mode(k)  = 2;
        Mode2(k) = 1;
%     elseif (isequal(F, [265 285 270]) || isequal(F, [275 290 280])) && (isequal(T, [2 2 2]) || isequal(T, [6 6 6])) % Normal mode FM : Checked
    elseif checkFrequenciesAndPulseLength(F, [265 285 270], T, [2 2 2], []) % Normal mode FM : Checked
        Mode(k)  = 2;
        Mode2(k) = 1;
    elseif checkFrequenciesAndPulseLength(F, [275 290 280], T, [2 2 2], []) % Normal mode FM : Checked
        Mode(k)  = 2;
        Mode2(k) = 1;
    elseif checkFrequenciesAndPulseLength(F, [265 285 270], T, [6 6 6], []) % Normal mode FM : Checked
        Mode(k)  = 2;
        Mode2(k) = 1;
    elseif checkFrequenciesAndPulseLength(F, [275 290 280], T, [6 6 6], []) % Normal mode FM : Checked
        Mode(k)  = 2;
        Mode2(k) = 1;

        %% Dual Swath, Single sector
%     elseif (isequal(F, 260) || isequal(F, 300))&& isequal(T, 0.045) % Normal mode S : Checked
    elseif checkFrequenciesAndPulseLength(F, 260, T, 0.045, []) % Normal mode S : Checked
        Mode(k)  = 2;
        Mode2(k) = 2;
    elseif checkFrequenciesAndPulseLength(F, 300, T, 0.045, []) % Normal mode S : Checked
        Mode(k)  = 2;
        Mode2(k) = 2;
%     elseif (isequal(F, 270) || isequal(F, 300)) && isequal(T, 0.07) % Normal mode M : Checked
    elseif checkFrequenciesAndPulseLength(F, 270, T, 0.07, []) % Normal mode M : Checked
        Mode(k)  = 2;
        Mode2(k) = 2;
    elseif checkFrequenciesAndPulseLength(F, 300, T, 0.07, []) % Normal mode M : Checked
        Mode(k)  = 2;
        Mode2(k) = 2;
%     elseif (isequal(F, 280) || isequal(F, 300)) && isequal(T, 0.15) % Normal mode L : Checked
    elseif checkFrequenciesAndPulseLength(F, 280, T, 0.15, []) % Normal mode L : Checked
        Mode(k)  = 2;
        Mode2(k) = 2;
    elseif checkFrequenciesAndPulseLength(F, 300, T, 0.15, []) % Normal mode L : Checked
        Mode(k)  = 2;
        Mode2(k) = 2;
%     elseif (isequal(F, 280) || isequal(F, 300)) && isequal(T, 1.5) % Normal mode FM : Checked
    elseif checkFrequenciesAndPulseLength(F, 280, T, 1.5, []) % Normal mode FM : Checked
        Mode(k)  = 2;
        Mode2(k) = 2;
    % elseif checkFrequenciesAndPulseLength(F, 300, T, 1.5, []) % Normal mode FM : Checked
    %     Mode(k)  = 2;
    %     Mode2(k) = 2;

        %         case 3 % 400 kHz
        %% Single Swath, Normal mode
%     elseif isequal(F, [320 380 320]) && (isequal(T, [0.05 0.05 0.05]) || isNearlyEqual(T, [0.073 0.073 0.073], 0.001)) % Normal mode S : Checked
    elseif checkFrequenciesAndPulseLength(F, [320 380 320], T, [0.05 0.05 0.05], [0.073 0.073 0.073]) % Normal mode S : Checked
        Mode(k)  = 3;
        Mode2(k) = 1;
%     elseif isequal(F, [320 380 320]) && (isequal(T, [0.1 0.1 0.1]) || isNearlyEqual(T, [0.145 0.145 0.145], 0.001)) % Normal mode M : Checked
    elseif checkFrequenciesAndPulseLength(F, [320 380 320], T, [0.1 0.1 0.1], [0.145 0.145 0.145]) % Normal mode M : Checked
        Mode(k)  = 3;
        Mode2(k) = 1;
%     elseif isequal(F, [320 380 320]) && (isequal(T, [0.2 0.2 0.2]) || isNearlyEqual(T, [0.288 0.288 0.288], 0.001)) % Normal mode L : Checked
    elseif checkFrequenciesAndPulseLength(F, [320 380 320], T, [0.2 0.2 0.2], [0.288 0.288 0.288]) % Normal mode L : Checked
        Mode(k)  = 3;
        Mode2(k) = 1;

        %% Single Swath, Single sector
%     elseif isequal(F, 380) && (isequal(T, 0.025) || isNearlyEqual(T, 0.037, 0.001)) % Normal mode S : Checked
    elseif checkFrequenciesAndPulseLength(F, 380, T, 0.025, 0.037) % Normal mode S : Checked
        Mode(k)  = 3;
        Mode2(k) = 2;
%     elseif isequal(F, 380) && (isequal(T, 0.05) || isNearlyEqual(T, 0.073, 0.001)) % Normal mode M : Checked
    elseif checkFrequenciesAndPulseLength(F, 380, T, 0.05, 0.073) % Normal mode M : Checked
        Mode(k)  = 3;
        Mode2(k) = 2;
%     elseif isequal(F, 380) && (isequal(T, 0.1) || isNearlyEqual(T, 0.145, 0.001)) % Normal mode L : Checked
    elseif checkFrequenciesAndPulseLength(F, 380, T, 0.1, 0.145) % Normal mode L : Checked
        Mode(k)  = 3;
        Mode2(k) = 2;
%     elseif isequal(F, 380) && isequal(T, 1.5) % Normal mode FM % TODO : pas dans le tableau !
    elseif checkFrequenciesAndPulseLength(F, 380, T, 1.5, []) % Normal mode FM % TODO : pas dans le tableau !
        my_breakpoint
        Mode(k)  = 3;
        Mode2(k) = 2;

%         %% Single Swath, Scanning
% %     elseif isequal(F, [380 380 380]) && isequal(T, [0.025 0.025 0.025]) % Normal mode S
%     elseif checkFrequenciesAndPulseLength(F, [380 380 380], T, [0.025 0.025 0.025], []) % Normal mode S
%         my_breakpoint
%         Mode(k)  = 3;
%         Mode2(k) = 3;
% %     elseif isequal(F, [380 380 380]) && (isequal(T, [0.05 0.05 0.05]) || isNearlyEqual(T, [0.073 0.073 0.073], 0.001)) % Normal mode M
%     elseif checkFrequenciesAndPulseLength(F, [380 380 380], T, [0.05 0.05 0.05], [0.073 0.073 0.073]) % Normal mode M
%         my_breakpoint
%         Mode(k)  = 3;
%         Mode2(k) = 3;
% %     elseif isequal(F, [380 380 380]) && (isequal(T, [0.1 0.1 0.1]) || isNearlyEqual(T, [0.145 0.145 0.145], 0.001)) % Normal mode L
%     elseif checkFrequenciesAndPulseLength(F, [380 380 380], T, [0.1 0.1 0.1], [0.145 0.145 0.145]) % Normal mode L
%         my_breakpoint
%         Mode(k)  = 3;
%         Mode2(k) = 3;


        %% Dual Swath, Normal mode
%     elseif (isequal(F, [320 380 320]) || isequal(F, [290 350 290])) && isequal(T, [0.05 0.05 0.05]) % Normal mode S : Checked
    elseif checkFrequenciesAndPulseLength(F, [320 380 320], T, [0.05 0.05 0.05], []) % Normal mode S : Checked
        Mode(k)  = 3;
        Mode2(k) = 1;
    elseif checkFrequenciesAndPulseLength(F, [290 350 290], T, [0.05 0.05 0.05], []) % Normal mode S : Checked
        Mode(k)  = 3;
        Mode2(k) = 1;
%     elseif (isequal(F, [320 380 320]) || isequal(F, [300 360 300]))&& isequal(T, [0.1 0.1 0.1]) % Normal mode M : Checked
    elseif checkFrequenciesAndPulseLength(F, [320 380 320], T, [0.1 0.1 0.1], []) % Normal mode M : Checked
        Mode(k)  = 3;
        Mode2(k) = 1;
    elseif checkFrequenciesAndPulseLength(F, [300 360 300], T, [0.1 0.1 0.1], []) % Normal mode M : Checked
        Mode(k)  = 3;
        Mode2(k) = 1;
%     elseif (isequal(F, [320 380 320]) || isequal(F, [310 370 310]) ) && isequal(T, [0.2 0.2 0.2]) % Normal mode L : Checked
    elseif checkFrequenciesAndPulseLength(F, [320 380 320], T, [0.2 0.2 0.2], []) % Normal mode L : Checked
        Mode(k)  = 3;
        Mode2(k) = 1;
    elseif checkFrequenciesAndPulseLength(F, [310 370 310], T, [0.2 0.2 0.2], []) % Normal mode L : Checked
        Mode(k)  = 3;
        Mode2(k) = 1;

        %% Dual Swath, Single sector
%     elseif (isequal(F, 380) || isequal(F, 300)) && isequal(T, 0.025) % Normal mode S : Checked
    elseif checkFrequenciesAndPulseLength(F, 380, T,  0.025, []) % Normal mode S : Checked
        Mode(k)  = 3;
        Mode2(k) = 2;
    elseif checkFrequenciesAndPulseLength(F, 300, T,  0.025, []) % Normal mode S : Checked
        Mode(k)  = 3;
        Mode2(k) = 2;
%     elseif (isequal(F, 380) || isequal(F, 340)) && isequal(T, 0.05) % Normal mode M : Checked
    elseif checkFrequenciesAndPulseLength(F, 380, T,  0.05, []) % Normal mode M : Checked
        Mode(k)  = 3;
        Mode2(k) = 2;
    elseif checkFrequenciesAndPulseLength(F, 340, T,  0.05, []) % Normal mode M : Checked
        Mode(k)  = 3;
        Mode2(k) = 2;
%     elseif (isequal(F, 380) || isequal(F, 360)) && isequal(T, 0.1) % Normal mode L : Checked
    elseif checkFrequenciesAndPulseLength(F, 380, T,  0.1, []) % Normal mode L : Checked
        Mode(k)  = 3;
        Mode2(k) = 2;
    elseif checkFrequenciesAndPulseLength(F, 360, T,  0.1, []) % Normal mode L : Checked
        Mode(k)  = 3;
        Mode2(k) = 2;

    %% EM2040D
    
        %% 200 kHz

        %% Single Swath, Normal mode
    elseif checkFrequenciesAndPulseLength(F, [190 220 190 220], T, [0.07 0.07 0.07 0.07], [0.101 0.101 0.101 0.101]) % Normal mode S : Checked
        Mode(k)  = 1;
        Mode2(k) = 1;
    elseif checkFrequenciesAndPulseLength(F, [190 205 190 205], T, [0.2 0.2 0.2 0.2], [0.288 0.288 0.288 0.288]) % Normal mode M : Checked
        Mode(k)  = 1;
        Mode2(k) = 1;
    elseif checkFrequenciesAndPulseLength(F, [190 200 190 200], T, [0.6 0.6 0.6 0.6], [0.865 0.865 0.865 0.865]) % Normal mode L : Checked
        Mode(k)  = 1;
        Mode2(k) = 1;
    elseif checkFrequenciesAndPulseLength(F, [190 200 190 200], T, [3 3 3 3], []) % Normal mode FM : Checked
        Mode(k)  = 1;
        Mode2(k) = 1;
    elseif checkFrequenciesAndPulseLength(F, [190 200 190 200], T, [12 12 12 12], []) % Normal mode FM : Checked
        Mode(k)  = 1;
        Mode2(k) = 1;


        %% Single Swath, Single sector
    elseif checkFrequenciesAndPulseLength(F, [200 200], T, [0.035 0.035], [0.051 0.051]) % Normal mode S : Checked
        Mode(k)  = 1;
        Mode2(k) = 2;
    elseif checkFrequenciesAndPulseLength(F, [200 200], T, [0.07 0.07], [0.101 0.101]) % Normal mode M : Checked
        Mode(k)  = 1;
        Mode2(k) = 2;
    elseif checkFrequenciesAndPulseLength(F, [200 200], T, [0.15 0.15], [0.216 0.216]) % Normal mode L : Checked
        Mode(k)  = 1;
        Mode2(k) = 2;

        %% 300 kHz
        %% Single Swath, Normal mode
    elseif checkFrequenciesAndPulseLength(F, [260 320 320 290], T, [0.07 0.07 0.07 0.07], [0.101 0.101 0.101 0.101]) % Normal mode S : Checked
        Mode(k)  = 2;
        Mode2(k) = 1;
    elseif checkFrequenciesAndPulseLength(F, [270 295 295 282.5], T, [0.2 0.2 0.2 0.2], [0.288 0.288 0.288 0.288]) % Normal mode M : Checked
        Mode(k)  = 2;
        Mode2(k) = 1;
    elseif checkFrequenciesAndPulseLength(F, [270 285 285 277.5], T, [0.6 0.6 0.6 0.6], [0.865 0.865 0.865 0.865]) % Normal mode L : Checked
        Mode(k)  = 2;
        Mode2(k) = 1;
%     elseif isequal(fliplr(F), [270 280 280 275]) && isequal(fliplr(T), [2 2 2 2]) % Normal mode FM : Checked
    elseif checkFrequenciesAndPulseLength(F, [270 280 280 275], T, [2 2 2 2], []) % Normal mode FM : Checked
        Mode(k)  = 2;
        Mode2(k) = 1;
%     elseif isequal(fliplr(F), [270 280 280 275]) && isequal(fliplr(T), [6 6 6 6]) % Normal mode FM : Checked
    elseif checkFrequenciesAndPulseLength(F, [270 280 280 275], T, [6 6 6 6], []) % Normal mode FM : Checked
        Mode(k)  = 2;
        Mode2(k) = 1;

        %% Single Swath, Single sector
    elseif checkFrequenciesAndPulseLength(F, [300 300], T, [0.035 0.035], [0.051 0.051]) % Normal mode S : Checked
        Mode(k)  = 2;
        Mode2(k) = 2;
    elseif checkFrequenciesAndPulseLength(F, [300 300], T, [0.07 0.07], [0.101 0.101]) % Normal mode M : Checked
        Mode(k)  = 2;
        Mode2(k) = 2;
    elseif checkFrequenciesAndPulseLength(F, [300 300], T, [0.15 0.15], [0.216 0.216]) % Normal mode L : Checked
        Mode(k)  = 2;
        Mode2(k) = 2;
%     elseif isequal(fliplr(F), [300 300]) && isequal(fliplr(T), [1.5 1.5]) % Normal mode FM : Checked
    elseif checkFrequenciesAndPulseLength(F, [300 300], T, [1.5 1.5], []) % Normal mode FM : Checked
        Mode(k)  = 2;
        Mode2(k) = 2;

        %% Single Swath, Normal mode
    elseif checkFrequenciesAndPulseLength(F(1:2), [220 190 220 190], T(1:2), [0.07 0.07 0.07], [0.101 0.101 0.101 0.101]) % Normal mode S : Checked
        Mode(k)  = 1;
        Mode2(k) = 1;



        %% Dual Swath, Normal mode
%     elseif (isequal(fliplr(F), [260 320 320 260]) || isequal(fliplr(F), [290 350 350 290])) && (isequal(fliplr(T), [0.07 0.07 0.07 0.07]) || isNearlyEqual(fliplr(T), [0.101 0.101 0.101 0.101], 0.001)) % Normal mode S : Checked
    elseif checkFrequenciesAndPulseLength(F, [260 320 320 260], T, [0.07 0.07 0.07 0.07], [0.101 0.101 0.101 0.101]) % Normal mode S : Checked
        Mode(k)  = 2;
        Mode2(k) = 1;
    elseif checkFrequenciesAndPulseLength(F, [290 350 350 290], T, [0.07 0.07 0.07 0.07], [0.101 0.101 0.101 0.101]) % Normal mode S : Checked
        Mode(k)  = 2;
        Mode2(k) = 1;
%     elseif (isequal(fliplr(F), [260 300 300 270]) || isequal(fliplr(F), [280 320 320 290]))&& (isequal(fliplr(T), [0.2 0.2 0.2 0.2]) || isNearlyEqual(fliplr(T), [0.288 0.288 0.288 0.288], 0.001)) % Normal mode M : Checked
    elseif checkFrequenciesAndPulseLength(F, [260 300 300 270], T, [0.2 0.2 0.2 0.2], [0.288 0.288 0.288 0.288]) % Normal mode M : Checked
        Mode(k)  = 2;
        Mode2(k) = 1;
    elseif checkFrequenciesAndPulseLength(F, [280 320 320 290], T, [0.2 0.2 0.2 0.2], [0.288 0.288 0.288 0.288]) % Normal mode M : Checked
        Mode(k)  = 2;
        Mode2(k) = 1;
%     elseif (isequal(fliplr(F), [265 285 285 270]) || isequal(fliplr(F), [275 290 290 280])) && (isequal(fliplr(T), [0.6 0.6 0.6 0.6]) || isNearlyEqual(fliplr(T), [0.865 0.865 0.865 0.865], 0.001)) % Normal mode L : Checked
    elseif checkFrequenciesAndPulseLength(F, [265 285 285 270], T, [0.6 0.6 0.6 0.6], [0.865 0.865 0.865 0.865]) % Normal mode L : Checked
        Mode(k)  = 2;
        Mode2(k) = 1;
    elseif checkFrequenciesAndPulseLength(F, [275 290 290 280], T, [0.6 0.6 0.6 0.6], [0.865 0.865 0.865 0.865]) % Normal mode L : Checked
        Mode(k)  = 2;
        Mode2(k) = 1;
%     elseif (isequal(fliplr(F), [265 285 285 270]) || isequal(fliplr(F), [275 290 290 280])) && (isequal(fliplr(T), [2 2 2 2]) || isequal(fliplr(T), [6 6 6 6])) % Normal mode FM : Checked
    elseif checkFrequenciesAndPulseLength(F, [265 285 285 270], T, [2 2 2 2], []) % Normal mode FM : Checked
        Mode(k)  = 2;
        Mode2(k) = 1;
    elseif checkFrequenciesAndPulseLength(F, [275 290 290 280], T, [2 2 2 2], []) % Normal mode FM : Checked
        Mode(k)  = 2;
        Mode2(k) = 1;
    elseif checkFrequenciesAndPulseLength(F, [265 285 285 270], T, [6 6 6 6], []) % Normal mode FM : Checked
        Mode(k)  = 2;
        Mode2(k) = 1;
    elseif checkFrequenciesAndPulseLength(F, [275 290 290 280], T, [6 6 6 6], []) % Normal mode FM : Checked
        Mode(k)  = 2;
        Mode2(k) = 1;

        %% Dual Swath, Single sector
%     elseif (isequal(fliplr(F), [260 260]) || isequal(fliplr(F), [300 300]))&& isequal(fliplr(T), [0.045 0.045]) % Normal mode S : Checked
    elseif checkFrequenciesAndPulseLength(F, [260 260], T, [0.045 0.045], []) % Normal mode S : Checked
        Mode(k)  = 2;
        Mode2(k) = 2;
    elseif checkFrequenciesAndPulseLength(F, [300 300], T, [0.045 0.045], []) % Normal mode S : Checked
        Mode(k)  = 2;
        Mode2(k) = 2;
%     elseif (isequal(fliplr(F), [270 270]) || isequal(fliplr(F), [300 300])) && isequal(fliplr(T), [0.07 0.07]) % Normal mode M : Checked
    elseif checkFrequenciesAndPulseLength(F, [270 270], T, [0.07 0.07], []) % Normal mode M : Checked
        Mode(k)  = 2;
        Mode2(k) = 2;
    elseif checkFrequenciesAndPulseLength(F, [300 300], T, [0.07 0.07], []) % Normal mode M : Checked
        Mode(k)  = 2;
        Mode2(k) = 2;
%     elseif (isequal(fliplr(F), [280 280]) || isequal(fliplr(F), [300 300])) && isequal(fliplr(T), [0.15 0.15]) % Normal mode L : Checked
    elseif checkFrequenciesAndPulseLength(F, [280 280], T, [0.15 0.15], []) % Normal mode L : Checked
        Mode(k)  = 2;
        Mode2(k) = 2;
    elseif checkFrequenciesAndPulseLength(F, [300 300], T, [0.15 0.15], []) % Normal mode L : Checked
        Mode(k)  = 2;
        Mode2(k) = 2;
%     elseif (isequal(fliplr(F), [280 280]) || isequal(fliplr(F), [300 300])) && isequal(fliplr(T), [1.5 1.5]) % Normal mode FM : Checked
    elseif checkFrequenciesAndPulseLength(F, [280 280], T, [1.5 1.5], []) % Normal mode FM : Checked
        Mode(k)  = 2;
        Mode2(k) = 2;
    % elseif checkFrequenciesAndPulseLength(F, [300 300], T, [1.5 1.5], []) % Normal mode FM : Checked
    %     Mode(k)  = 2;
    %     Mode2(k) = 2;

        %% 400 kHz
        %% Single Swath, Normal mode
    elseif checkFrequenciesAndPulseLength(F, [320 380  380 320], T, [0.05 0.05 0.05 0.05], [0.073 0.073 0.073 0.073]) % Normal mode S : Checked
        Mode(k)  = 3;
        Mode2(k) = 1;
    elseif checkFrequenciesAndPulseLength(F, [320 380  380 320], T, [0.1 0.1 0.1 0.1], [0.145 0.145 0.145 0.145]) % Normal mode M : Checked
        Mode(k)  = 3;
        Mode2(k) = 1;
    elseif checkFrequenciesAndPulseLength(F, [320 380  380 320], T, [0.2 0.2 0.2 0.2], [0.288 0.288 0.288 0.288]) % Normal mode L : Checked
        Mode(k)  = 3;
        Mode2(k) = 1;

        %% Single Swath, Single sector
    elseif checkFrequenciesAndPulseLength(F, [380 380], T, [0.025 0.025], [0.037 0.037]) % Normal mode S : Checked
        Mode(k)  = 3;
        Mode2(k) = 2;
    elseif checkFrequenciesAndPulseLength(F, [380 380], T, [0.05 0.05], [0.073 0.073]) % Normal mode M : Checked
        Mode(k)  = 3;
        Mode2(k) = 2;
    elseif checkFrequenciesAndPulseLength(F, [380 380], T, [0.1 0.1], [0.145 0.145]) % Normal mode L : Checked
        Mode(k)  = 3;
        Mode2(k) = 2;
%     elseif isequal(fliplr(F), [380 380]) && isequal(fliplr(T), [1.5 1.5]) % Normal mode FM % TODO : pas dans le tableau !
    elseif checkFrequenciesAndPulseLength(F, [380 380], T, [1.5 1.5], []) % Normal mode FM % TODO : pas dans le tableau !
        my_breakpoint
        Mode(k)  = 3;
        Mode2(k) = 2;


        %% Dual Swath, Normal mode
%     elseif (isequal(fliplr(F), [320 380 380 320]) || isequal(fliplr(F), [290 350 350 290])) && isequal(fliplr(T), [0.05 0.05 0.05 0.05]) % Normal mode S : Checked
    elseif checkFrequenciesAndPulseLength(F, [320 380 380 320], T, [0.05 0.05 0.05 0.05], []) % Normal mode S : Checked
        Mode(k)  = 3;
        Mode2(k) = 1;
    elseif checkFrequenciesAndPulseLength(F, [290 350 350 290], T, [0.05 0.05 0.05 0.05], []) % Normal mode S : Checked
        Mode(k)  = 3;
        Mode2(k) = 1;
%     elseif (isequal(fliplr(F), [320 380 380 320]) || isequal(fliplr(F), [300 360 360 300]))&& isequal(fliplr(T), [0.1 0.1 0.1 0.1]) % Normal mode M : Checked
    elseif checkFrequenciesAndPulseLength(F, [320 380 380 320], T, [0.1 0.1 0.1 0.1], []) % Normal mode M : Checked
        Mode(k)  = 3;
        Mode2(k) = 1;
    elseif checkFrequenciesAndPulseLength(F, [300 360 360 300], T, [0.1 0.1 0.1 0.1], []) % Normal mode M : Checked
        Mode(k)  = 3;
        Mode2(k) = 1;
%     elseif (isequal(fliplr(F), [320 380 380 320]) || isequal(fliplr(F), [310 370 370 310]) ) && isequal(fliplr(T), [0.2 0.2 0.2 0.2]) % Normal mode L : Checked
    elseif checkFrequenciesAndPulseLength(F, [320 380 380 320], T, [0.2 0.2 0.2 0.2], []) % Normal mode L : Checked
        Mode(k)  = 3;
        Mode2(k) = 1;
    elseif checkFrequenciesAndPulseLength(F, [310 370 370 310], T, [0.2 0.2 0.2 0.2], []) % Normal mode L : Checked
        Mode(k)  = 3;
        Mode2(k) = 1;

        %% Dual Swath, Single sector
%     elseif (isequal(fliplr(F), [380 380]) || isequal(fliplr(F), [300 300])) && isequal(fliplr(T), [0.025 0.025]) % Normal mode S : Checked
    elseif checkFrequenciesAndPulseLength(F, [380 380], T, [0.025 0.025], []) % Normal mode S : Checked
        Mode(k)  = 3;
        Mode2(k) = 2;
    elseif checkFrequenciesAndPulseLength(F, [300 300], T, [0.025 0.025], []) % Normal mode S : Checked
        Mode(k)  = 3;
        Mode2(k) = 2;
%     elseif (isequal(fliplr(F), [380 380]) || isequal(fliplr(F), [340 340])) && isequal(fliplr(T), [0.05 0.05]) % Normal mode M : Checked
    elseif checkFrequenciesAndPulseLength(F, [380 380], T, [0.05 0.05], []) % Normal mode M : Checked
        Mode(k)  = 3;
        Mode2(k) = 2;
    elseif checkFrequenciesAndPulseLength(F, [340 340], T, [0.05 0.05], []) % Normal mode M : Checked
        Mode(k)  = 3;
        Mode2(k) = 2;
%     elseif (isequal(fliplr(F), [380 380]) || isequal(fliplr(F), [360 360])) && isequal(fliplr(T), [0.1 0.1]) % Normal mode L : Checked
    elseif checkFrequenciesAndPulseLength(F, [380 380], T, [0.1 0.1], []) % Normal mode L : Checked
        Mode(k)  = 3;
        Mode2(k) = 2;
    elseif checkFrequenciesAndPulseLength(F, [360 360], T, [0.1 0.1], []) % Normal mode L : Checked
        Mode(k)  = 3;
        Mode2(k) = 2;

    else
        my_breakpoint
        message_erreur(nomFic, F, T) % Rencontr� sur \\meskl1\data\JMA-Thalia-EM2040\2013_02\0062_20130203_143759_Thalia.all o� F=200, T=0.101 : C'est le mode qui est faux en fait, il devrait �tre = 2
    end
end

Mode2 = my_interp1_Extrap_PreviousThenNext(DataRawDatetime, Mode2, DataDepthDatetime);
Z.Mode2    = single(Mode2(:)); % TODO : Devrait �tre supprim� (if ~isdeployed pour commencer)
Z.Data     = single(Mode2(:));
Z.Datetime = DataDepthDatetime;
Z.Time     = cl_time('timeMat', datenum(DataDepthDatetime));
Z.Unit     = '';
Z.strData  = {'Normal mode'; 'Single sector'; 'Scanning'};

flag = 1;


function message_erreur(nomFic, F, T, varargin)

F = num2str(F);
T = num2str(T);
if nargin == 3
    str1 = sprintf('Pb rencontr� dans "create_signal_Mode2_EM2040" : Param�tres non attendu pour EM2040, F=[%s], T=[%s]. Envoyez ce message � sonarscope@ifremer.fr SVP\nFichier "%s".', F, T, nomFic);
    str2 = sprintf('Pb in "create_signal_Mode2_EM2040" : Parameters not expected for EM2040, F=[%s], T=[%s]. Report to sonarscope@ifremer.fr please.\nFile "%s".', F, T, nomFic);
else
    Mode = varargin{1};
    str1 = sprintf('Pb rencontr� dans "create_signal_Mode2_EM2040" : Mode=%d non attendu pour EM2040, F=[%s], T=[%s]. Envoyez ce message � sonarscope@ifremer.fr SVP\nFichier "%s".', Mode, F, T, nomFic);
    str2 = sprintf('Pb in "create_signal_Mode2_EM2040" : Mode=%d not expected for EM2040, F=[%s], T=[%s]. Report to sonarscope@ifremer.fr please.\nFichier "%s".', Mode, F, T, nomFic);
end
my_warndlg(Lang(str1,str2), 0, 'Tag', 'ModeNonIdentifie');


function flag = isNearlyEqual(A, B, Precision)
flag = nearlyEqual(A, B, Precision);
flag = all(flag);


function [flag, Z] = create_signal_Mode2_EM2045_SingleSwath(DataDepthDatetime, sizeMode, DataRawDatetime, CentralFrequency) %, SignalLength)

Mode2 = zeros(sizeMode);
[nbPings, nbTx] = size(CentralFrequency); %#ok<ASGLU>
for k=1:nbPings
    Mode2(k) = 1; % Mode + 1; TODO CW / FM su bit 6
end

Mode2X = my_interp1_Extrap_PreviousThenNext(DataRawDatetime, Mode2, DataDepthDatetime);
Mode2X(isnan(Mode2X)) = Mode2(1); % Cas o� l'heure du premier ping est ant�rieure au premier datagramme Runtime (fichier 0004_20181010_172532_Panopee.all de l'ENSTA)

Z.Mode2    = single(Mode2X(:));
Z.Data     = single(Mode2X(:));
Z.Time     = cl_time('timeMat', datenum(DataDepthDatetime));
Z.Datetime = DataDepthDatetime;
Z.Unit     = '';
Z.strData  = {'CW'; 'FM'};

flag = 1;


function flag = checkFrequenciesAndPulseLength(F1, F2, T1, T2, T3)

% isequal(fliplr(F), [190 220]) && (isequal(T, [0.07 0.07]) || isNearlyEqual(fliplr(T), [0.101 0.101], 0.001)) % Normal mode S : Checked
% isequal(fliplr(F), [190 200]) && isequal(fliplr(T), [3 3]) % Normal mode FM : Checked

if isempty(T3)
    if isequal(F1, F2) && isequal(T1, T2)
        flag = true;
    elseif isequal(fliplr(F1), F2) && isequal(fliplr(T1), T2)
        flag = true;
    else
        flag = false;
    end
else
    if isequal(F1, F2) && (isequal(T1, T2) || isNearlyEqual(T1, T3, 0.001))
        flag = true;
    elseif isequal(fliplr(F1), F2) && (isequal(fliplr(T1), T2) || isNearlyEqual(fliplr(T1), T3, 0.001))
        flag = true;
    else
        flag = false;
    end
end
