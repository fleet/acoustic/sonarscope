function [flag, Z] = create_signal_Mode3_EM2040(All, varargin)

[flag, Z] = create_signal_Mode(All, varargin{:});
if ~flag
    return
end
TsubDepth = Z.Datetime(:,1);

[~, signalContainerFromRuntime] = create_signalsFromRuntime(All, TsubDepth, varargin{:});
pulseLengthConfSignal = signalContainerFromRuntime.getSignalByTag('PulseLengthConf');
if isempty(pulseLengthConfSignal)
    flag = 0;
else
    Z.Data    = pulseLengthConfSignal.ySample.data;
    Z.strData = pulseLengthConfSignal.ySample.strIndexList;
end
