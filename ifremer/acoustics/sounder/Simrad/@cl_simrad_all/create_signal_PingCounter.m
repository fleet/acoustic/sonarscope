function [flag, Z] = create_signal_PingCounter(this, varargin)

[varargin, DataDepth] = getPropertyValue(varargin, 'DataDepth', []); %#ok<ASGLU>

if isempty(DataDepth)
    [flag, DataDepth] = read_depth_bin(this, [], 'Memmapfile', -1);
    if ~flag
        Z.Data = [];
        return
    end
end

Z.Time     = DataDepth.Time;
Z.Datetime = DataDepth.Datetime;
Z.Data     = DataDepth.PingCounter;
Z.Unit     = '';

flag = 1;
