function [flag, Z, comment] = create_signal_PresenceRTK(this, varargin)

[varargin, IndNavUnit]   = getPropertyValue(varargin, 'IndNavUnit',   []);
[varargin, DataPosition] = getPropertyValue(varargin, 'DataPosition', []); %#ok<ASGLU>

comment = '0=No RTK available, 1=RTK available, 2=RTK processed';

if isempty(DataPosition)
    [flag, DataPosition] = read_position_bin(this, 'IndNavUnit', IndNavUnit, 'Memmapfile', 1);
    if ~flag
        Z.Data = [];
        return
    end
end

if ~isfield(DataPosition, 'QualityIndicator')
    Z.Data = [];
    return
end

QualityIndicator = DataPosition.QualityIndicator(:);

Z.Time     = DataPosition.Time;
Z.Datetime = DataPosition.Datetime;
infoRTK    = zeros(DataPosition.Dimensions.NbSamples, 1, 'uint8');
infoRTK(:) = (QualityIndicator(:,1) == 4) | (QualityIndicator(:,1) == 5);

% Attention : modifs apportées par JMA le 15/12/2021 après constat par Ridha
% du plot nav+PresenceRTK deffectueux mais je n'ai pas retesté l=des
% fichiers contenant du RTK
if any(infoRTK)
    flag = SSc_ReadDatagrams(this.nomFic, 'Ssc_Height');
    if flag
        [flag, A] = create_signal_Attitude(this, 'Tide', 'Value');
        if flag
            Tide = interp1(A.Time, A.Data(:,:), DataPosition.Time);
            infoRTK(~isnan(Tide)) = 2;
        end
    else
        infoRTK(:) = 0;
    end
end

Z.Data = infoRTK;
Z.Unit = '';
Z.strData = {'No RTK available'; 'RTK available'; 'RTK processed'};
Z.strIndData = [0 1 2];
flag = 1;
