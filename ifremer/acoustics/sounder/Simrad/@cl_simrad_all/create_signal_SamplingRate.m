function [flag, Z] = create_signal_SamplingRate(this, varargin)

% [varargin, DataDepth] = getPropertyValue(varargin, 'DataDepth', []);
%
% if isempty(DataDepth)
%     [flag, DataDepth] = read_depth_bin(this, [], 'Memmapfile', 1);
%     if ~flag
%         Z.Data = [];
%         return
%     end
% end
%
% Z.Time = DataDepth.Time;
% Z.Data = DataDepth.SamplingRate(:,:);
% Z.Unit = 'Hz';
%
% flag = 1;

Version = version_datagram(this);
if isempty(Version)
    Z.Data = [];
    return
end

switch Version
    case 'V1'
        [varargin, DataDepth] = getPropertyValue(varargin, 'DataDepth', []); %#ok<ASGLU>
        
        if isempty(DataDepth)
            [flag, DataDepth] = read_depth_bin(this, 'Memmapfile', 1);
            if ~flag
                Z.Data = [];
                return
            end
        end
        Z.Time     = DataDepth.Time;
        Z.Datetime = DataDepth.Datetime;
        Z.Data     = DataDepth.SamplingRate(:,:);
        Z.Unit     = 'Hz';
        
    case 'V2'
        [varargin, DataSeabed] = getPropertyValue(varargin, 'DataSeabed', []); %#ok<ASGLU>
        if isempty(DataSeabed)
            [flag, DataSeabed] = read_seabedImage_bin(this, 'Memmapfile', -1);
            if ~flag
                Z.Data = [];
                return
            end
        end
        Z.Time     = DataSeabed.Time;
        Z.Datetime = DataSeabed.Datetime;
        Z.Data     = DataSeabed.SamplingRate(:,:);
        Z.Unit     = 'Hz';
end
flag = 1;
