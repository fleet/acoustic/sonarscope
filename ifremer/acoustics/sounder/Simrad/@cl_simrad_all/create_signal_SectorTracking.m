function [flag, Z] = create_signal_SectorTracking(All)

[flag, DataDepth] = read_depth_bin(All, 'Memmapfile', -1);
if ~flag
    Z = [];
    return
end
Z.Time     = DataDepth.Time;
Z.Datetime = DataDepth.Datetime(:,1);
Z.Unit     = '';

[flag, signalContainerFromRuntime] = create_signalsFromRuntime(All, Z.Time.timeMat);
if ~flag
    return
end

sectorTrackingSignal = signalContainerFromRuntime.getSignalByTag('SectorTracking');
Z.Data = sectorTrackingSignal.ySample.data;
Z.strData = {'No'; 'Yes'};
Z.strIndData = [0 1];
