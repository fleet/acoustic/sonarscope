function [flag, Z] = create_signal_SoundSpeed(this, varargin)

[varargin, DataDepth] = getPropertyValue(varargin, 'DataDepth', []); %#ok<ASGLU>

if isempty(DataDepth)
    [flag, DataDepth] = read_depth_bin(this, [], 'Memmapfile', -1);
    if ~flag
        Z.Data = [];
        return
    end
end

Z.Data     = DataDepth.SoundSpeed;
Z.Time     = DataDepth.Time;
Z.Datetime = DataDepth.Datetime;
Z.Unit     = 'm/s';

flag = 1;
