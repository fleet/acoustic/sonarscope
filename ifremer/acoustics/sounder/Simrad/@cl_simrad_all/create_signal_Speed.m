function [flag, Z] = create_signal_Speed(this, varargin)

[varargin, IndNavUnit]   = getPropertyValue(varargin, 'IndNavUnit',   []);
[varargin, DataPosition] = getPropertyValue(varargin, 'DataPosition', []); %#ok<ASGLU>

if isempty(DataPosition)
    [flag, DataPosition] = read_position_bin(this, 'IndNavUnit', IndNavUnit, 'Memmapfile', 1);
    if ~flag
        Z.Data = [];
        return
    end
end

Speed = DataPosition.Speed(:,:);

if all(isnan(Speed))
    [~, Speed] = calCapFromLatLon(DataPosition.Latitude, DataPosition.Longitude, 'Time', DataPosition.Time);
end

Z.Data     = Speed;
Z.Time     = DataPosition.Time;
Z.Datetime = DataPosition.Datetime;
Z.Unit     = 'm/s';

flag = 1;
