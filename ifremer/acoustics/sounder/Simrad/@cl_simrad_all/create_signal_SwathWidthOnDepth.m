function [flag, Z] = create_signal_SwathWidthOnDepth(a, varargin)

[varargin, DataDepth] = getPropertyValue(varargin, 'DataDepth', []); %#ok<ASGLU>

if isempty(DataDepth)
    [flag, DataDepth] = read_depth_bin(a, 'Memmapfile', -1);
    if ~flag
        Z.Data = [];
        return
    end
end

Z.Time     = DataDepth.Time;
Z.Datetime = DataDepth.Datetime;

n1 = size(DataDepth.AcrossDist,1);
Z.Data = NaN(n1, 1);
for k=1:n1
    AcrossDist = DataDepth.AcrossDist(k,:);
    SwathWidth = max(AcrossDist, [], 'omitnan') - min(AcrossDist, [], 'omitnan');
    MeanDepth  = mean(DataDepth.Depth(k,:), 'omitnan');
    Z.Data(k)  = SwathWidth / abs(MeanDepth);
end

Z.Unit = '';
