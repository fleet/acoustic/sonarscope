function [flag, Z] = create_signal_TVGFunctionAppliedWC(a, varargin)

[varargin, DataWC] = getPropertyValue(varargin, 'DataWC', []); %#ok<ASGLU>

if isempty(DataWC)
    [flag, DataWC] = read_WaterColumn_bin(a, 'Memmapfile', -1);
    if ~flag
        Z.Data = [];
        return
    end
end

Z.Data = DataWC.TVGFunctionApplied(:,1);
Z.Datetime = DataWC.Datetime(:,1);
Z.Unit = '';
