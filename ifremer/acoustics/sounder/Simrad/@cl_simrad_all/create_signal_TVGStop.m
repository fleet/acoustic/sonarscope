function [flag, Z] = create_signal_TVGStop(this, varargin)

[varargin, DataSeabed] = getPropertyValue(varargin, 'DataSeabed', []); %#ok<ASGLU>

if isempty(DataSeabed)
    [flag, DataSeabed, SeabedImageFormat] = read_seabedImage_bin(this, 'Memmapfile', -1);
    if ~flag
        Z.Data = [];
        return
    end
end

if SeabedImageFormat == 1
    Z.Time     = DataSeabed.Time;
    Z.Datetime = DataSeabed.Datetime;
    Z.Data     = DataSeabed.TVGStop(:,:);
    Z.Unit     = 'dB';
    flag = 1;
else
    Z.Data = [];
    flag = 0;
    return
end
