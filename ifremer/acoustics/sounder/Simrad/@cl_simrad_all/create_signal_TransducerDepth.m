function [flag, Z] = create_signal_TransducerDepth(this, varargin)

[varargin, DataDepth] = getPropertyValue(varargin, 'DataDepth', []); %#ok<ASGLU>

if isempty(DataDepth)
    [flag, DataDepth] = read_depth_bin(this, [], 'Memmapfile', -1);
    if ~flag
        Z.Data = [];
        return
    end
end

Z.Time     = DataDepth.Time;
Z.Datetime = DataDepth.Datetime;
Z.Data     = DataDepth.TransducerDepth;
Z.Data     = -DataDepth.TransducerDepth; % Modif JMA MAYOBS15 AUV le 20/10/2020
% % Attention : modif faite en m�me temps dans create_signal_vertDepth
Z.Unit     = '';

flag = 1;
