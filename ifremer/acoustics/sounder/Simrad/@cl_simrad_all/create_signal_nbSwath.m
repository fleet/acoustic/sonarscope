function [flag, Z] = create_signal_nbSwath(a, varargin)

[varargin, DataRaw] = getPropertyValue(varargin, 'DataRaw', []);
[varargin, subRaw]  = getPropertyValue(varargin, 'subRaw',  []); %#ok<ASGLU>

if isempty(DataRaw)
    [flag, DataRaw] = read_rawRangeBeamAngle(a, 'Memmapfile', -1);
    if ~flag
        flag = 0;
        Z.Data = [];
        return
    end
end

flag = 1;

if isempty(subRaw)
    subRaw = 1:DataRaw.Dimensions.nbPings;
end

% TODO : il est possible maintenant de lire l'information directement dans
% le runtime parameter, param�tre mode :
% 00xx xxxx - Dual swath = 0ff
% 01xx xxxx - Dual swath = Fixed
% 10xx xxxx - Dual swath = Dynamic
% Ceci-dit, c'est pr�f�rable de d�tecter le r�sultat car l'option "Dynamic"
% ne permet pas de trancher

% if isfield(DataRaw, 'CentralFrequency')
%     Freq = DataRaw.CentralFrequency(:,1);
%     if length(unique(Freq)) == 1
%         PingSequence = ones(DataRaw.Dimensions.nbPings, 1, 'single');
%     else
%         PingSequence = 1 + (Freq(1:end-1) > Freq(2:end));
%         if Freq(end) > Freq(end-1)
%             PingSequence(end+1) = 2;
%         else
%             PingSequence(end+1) = 1;
%         end
%     end
%     PingSequence = DataRaw.PingSequence;
% else
% 	PingSequence = ones(DataRaw.Dimensions.nbPings, 1, 'single');
% end

PingSequence = DataRaw.PingSequence(subRaw);

% DualSwath = my_interp1_Extrap_PreviousThenNext(DataRuntime.Datetime, DataRuntime.DualSwath, DataDepth.Datetime(:,1));


X = single(PingSequence);
for k=1:(length(PingSequence)-1)
    X(k) = max(PingSequence(k:k+1));
end
Z.Data = X;

Z.Datetime = DataRaw.Datetime(subRaw,1);
Z.Unit = '';
