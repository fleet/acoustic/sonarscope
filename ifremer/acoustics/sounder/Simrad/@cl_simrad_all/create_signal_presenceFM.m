function [flag, Z] = create_signal_presenceFM(a, varargin)

[varargin, DataRaw] = getPropertyValue(varargin, 'DataRaw', []);
[varargin, subRaw]  = getPropertyValue(varargin, 'subRaw',  []); %#ok<ASGLU>

if isempty(DataRaw)
    [flag, DataRaw] = read_rawRangeBeamAngle(a, 'Memmapfile', -1);
    if ~flag
        Z.Data = [];
        return
    end
end

flag = 1;

if isempty(subRaw)
    subRaw = 1:DataRaw.Dimensions.nbPings;
end

if isfield(DataRaw, 'SignalWaveformIdentifier')
    
    % Ajout JMA le 05/10/2021 pour donn�es issues de XSF. C'�tait d� � la
    % confusion de nom pulse_form / frequency_mode le temps d'une journ�e
%     DataRaw.SignalWaveformIdentifier(DataRaw.SignalWaveformIdentifier == -1) = 0;
    
    X = max(DataRaw.SignalWaveformIdentifier(subRaw,:), [],  2);
else
    % Reliquat d'une erreur de nommage � la cr�ation de la chaine all2ssc
    if isfield(DataRaw, 'SignamWaveformIdentifier')
        X = max(DataRaw.SignamWaveformIdentifier(subRaw,:), [],  2);
    else
        X = zeros(length(subRaw), 1);
    end
end
X(isnan(X)) = 0; % Ajout JMA le 17/04/2024 pour fichier SPFE EM2040D Simon Stevin 0039_20240208_100346_Simon_Stevin.kmall
Z.Data = X;

Z.Datetime   = DataRaw.Datetime(subRaw,1);
Z.Unit       = '';
Z.strData    = {'No'; 'Yes'};
Z.strIndData = [0 1];
