function [flag, Z] = create_signal_presenceMagAndPhase(a, varargin)

[varargin, DataDepth]    = getPropertyValue(varargin, 'DataDepth',    []);
[varargin, DataMagPhase] = getPropertyValue(varargin, 'DataMagPhase', []);
[varargin, subDepth]     = getPropertyValue(varargin, 'subDepth',     []); %#ok<ASGLU>

if isempty(DataDepth)
    [flag, DataDepth] = read_depth_bin(a, 'Memmapfile', 1);
    if ~flag
        Z.Data = [];
        return
    end
end

if isempty(subDepth)
    subDepth = 1:DataDepth.Dimensions.nbPings;
end

T1 = DataDepth.PingCounter(subDepth,1);

DataDepthDatetime = DataDepth.Datetime(subDepth);

if isempty(DataMagPhase)
    [flag, DataMagPhase] = SSc_ReadDatagrams(a.nomFic, 'Ssc_RawBeamSamples', 'Memmapfile', -1);
    if ~flag
        flag = 1;
        X = zeros(length(T1), 1);
        Z.Data     = X;
        Z.Datetime = DataDepthDatetime;
        Z.Unit     = '';
        return
    end
end

T2 = DataMagPhase.PingCounter(:,1);
[subsubDepth, subWC] = intersect3(T1, T2, T2); %#ok<ASGLU>

X = zeros(length(T1), 1, 'single');
X(subsubDepth) = 1;
Z.Data     = X;
Z.Datetime = DataDepthDatetime;
Z.Unit     = '';

flag = 1;
