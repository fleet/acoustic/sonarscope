function [flag, Z] = create_signal_presenceStaveData(this, varargin)

[varargin, DataDepth] = getPropertyValue(varargin, 'DataDepth', []);
[varargin, DataStave] = getPropertyValue(varargin, 'DataStave', []);
[varargin, subDepth]  = getPropertyValue(varargin, 'subDepth',  []); %#ok<ASGLU>

if isempty(DataDepth)
    [flag, DataDepth] = read_depth_bin(this, 'Memmapfile', 1);
    if ~flag
        Z.Data = [];
        return
    end
end

if isempty(subDepth)
    subDepth = 1:DataDepth.Dimensions.nbPings;
end
T1 = DataDepth.PingCounter(subDepth,1);

DataDepthDatetime = DataDepth.Datetime(subDepth);

if isempty(DataStave)
%     [flag, DataStave] = read_StaveData_bin(this, 'Memmapfile', 1);
    [flag, DataStave] = SSc_ReadDatagrams(this.nomFic, 'Ssc_StaveData', 'Memmapfile', -1); % NON TESTE
    if ~flag
        flag = 1;
        X = zeros(length(T1), 1);
        Z.Data     = X;
        Z.Datetime = DataDepthDatetime;
        Z.Unit     = '';
        return
    end
end

T2 = DataStave.PingCounter(:,1);
[subsubDepth, subWC] = intersect3(T1, T2, T2); %#ok<ASGLU>

X = zeros(length(T1), 1, 'single');
X(subsubDepth) = 1;

Z.Data     = X;
Z.Datetime = DataDepthDatetime;
Z.Unit     = '';

flag = 1;
