function [flag, Z] = create_signal_presenceWC(a, varargin)

[varargin, DataDepth] = getPropertyValue(varargin, 'DataDepth', []);
[varargin, DataWC]    = getPropertyValue(varargin, 'DataWC',    []);
[varargin, subDepth]  = getPropertyValue(varargin, 'subDepth',  []); %#ok<ASGLU>

if isempty(DataDepth)
    [flag, DataDepth] = read_depth_bin(a, 'Memmapfile', -1);
    if ~flag
        Z.Data = [];
        return
    end
end

if isempty(subDepth)
    subDepth = 1:DataDepth.Dimensions.nbPings;
end

DataDepthDatetime = DataDepth.Datetime(subDepth,1);

T1 = DataDepth.PingCounter(subDepth,1);

if isempty(DataWC)
    % Ancien code :
    
%     try
%         [flag, PingCounter] = SScCacheNetcdfUtils.getSignalUnit(a.nomFic, 'Ssc_Depth', 'PingCounter'); % Pourquoi �a ???
%         T2 = PingCounter(:,1);
%     catch ME %#ok<NASGU>
%         [flag, DataWC] = read_WaterColumn_bin(a, 'Memmapfile', -1);
%         if flag
%             T2 = DataWC.PingCounter(:,1);
%         end
%     end
%     if ~flag
%         flag = 1;
%         X = zeros(length(T1), 1);
%         Z.Data       = X;
%         Z.Datetime   = DataDepthDatetime;
%         Z.Unit       = '';
%         Z.strData    = {'No'; 'Yes'};
%         Z.strIndData = [0 1];
%         return
%     end
    
    % Nouveau cosde : Modif JMA le 03/02/2022
    try
        [flag, DataWC] = read_WaterColumn_bin(a, 'Memmapfile', -1);
        if flag
            T2 = DataWC.PingCounter(:,1);
        else
            T2 = [];
        end
    catch ME %#ok<NASGU>
        T2 = [];
    end
else
	T2 = DataWC.PingCounter(:,1);
end

[subsubDepth, subWC] = intersect3(T1, T2, T2); %#ok<ASGLU>

X = zeros(length(T1), 1, 'single');
X(subsubDepth) = 1;

Z.Datetime   = DataDepthDatetime;
Z.Data       = X;
Z.Unit       = '';
Z.strData    = {'No'; 'Yes'};
Z.strIndData = [0 1];

flag = 1;
