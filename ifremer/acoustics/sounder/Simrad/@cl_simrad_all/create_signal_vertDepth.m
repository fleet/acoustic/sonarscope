function [flag, Z] = create_signal_vertDepth(a, varargin)

[varargin, DataDepth]  = getPropertyValue(varargin, 'DataDepth',  []); %#ok<ASGLU>

if isempty(DataDepth)
    [flag, DataDepth] = read_depth_bin(a, 'Memmapfile', -1);
    if ~flag
        Z.Data = [];
        return
    end
end

%             iFaisCentral = floor(size(DataDepth.Depth,2)/2);
%             Z.Data = DataDepth.Depth(:,iFaisCentral);
D = DataDepth.Depth(:,:);
D(DataDepth.Mask(:,:) ~= 0) = NaN;

% Ici on prenait la moyenne de tout mais sur le passage de
% cr�tes c'�tait pas bon (donn�es Healy � Hawai)
%             Z.Data = mean(D, 2, 'omitnan');

% Ici on prend la moyenne des 5 faisceaux valides les plus proche du milieu
% du tableau. Il vaudrait lire le tableau des angles et s�lectionner les 5
% plus proches de la verticale

n1 = size(D,1);
n2 = size(D,2);
sub1 = 1:floor(n2/2);
sub2 = (floor(n2/2)+1):n2;
sub = [fliplr(sub1)  sub2];
sub = sub(:);
Z.Data = NaN(n1, 1);
for k=1:n1
    z = D(k,:);
    sub1 = ~isnan(z(sub));
    sub1 = sub(sub1);
    sub1 = sub1(1:min(5,length(sub1)));
    %     H = mean(z(sub1), 'omitnan');
    H = mean(z(sub1));
    Z.Data(k) = H;
end


Z.Data = -Z.Data; % Modif JMA MAYOBS15 AUV le 20/10/2020
% Attention : modif faite en m�me temps dans create_signal_TransducerDepth

Z.Time     = DataDepth.Time;
Z.Datetime = DataDepth.Datetime;
Z.Unit     = 'm';

flag = 1;
