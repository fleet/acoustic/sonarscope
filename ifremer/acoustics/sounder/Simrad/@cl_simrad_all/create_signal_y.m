function [flag, Z] = create_signal_y(this, varargin)

[varargin, DataDepth] = getPropertyValue(varargin, 'DataDepth', []); %#ok<ASGLU>

if isempty(DataDepth)
    [flag, DataDepth] = read_depth_bin(this, [], 'Memmapfile', -1);
    if ~flag
        Z.Data = [];
        return
    end
end

Z.Data     = (1:DataDepth.Dimensions.nbPings)';
Z.Time     = DataDepth.Time;
Z.Datetime = DataDepth.Datetime;
Z.Unit     = '#';

flag = 1;
