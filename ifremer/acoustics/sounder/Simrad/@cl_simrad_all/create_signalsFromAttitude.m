function [flag, signalContainer] = create_signalsFromAttitude(this, TimeTx, sectorTransmitDelayYSampleList, varargin)

[varargin, DataAttitude] = getPropertyValue(varargin, 'DataAttitude', []); %#ok<ASGLU>

%% Lecture des datagrammes si pas présents

if isempty(DataAttitude)
    [flag, DataAttitude] = read_attitude_bin(this, 'Memmapfile', 1);
    if ~flag
        return
    end
end

%% Préparation des signaux

timeSample = XSample('name', 'time', 'data', TimeTx);

% Creation of signalList
signalList = ClSignal.empty;

%% Calcul des valeurs de Pitch, Pitch, Heave et Heading

[RollTx, PitchTx, HeaveTx, HeadingTx] = get_AttitudeTx(this, DataAttitude, TimeTx, sectorTransmitDelayYSampleList);

%% RollTx

ySampleList = YSample.empty;
X = RollTx;
nbSectors = size(X,2);
tagName = 'RollTx';
if nbSectors == 1
    ySampleList = YSample('data', X, 'unit', 'deg');
    ySampleList.name = tagName;
else
    for k=1:nbSectors
        sampleName = sprintf('%s, Ind=%d', tagName, k);
        ySampleList(end+1) = YSample('data', X(:,k), 'unit', 'deg'); %#ok<AGROW>
        ySampleList(end).name = sampleName;
    end
end
signalList(end+1) = ClSignal('name', tagName, 'xSample', timeSample, 'ySample', ySampleList, 'tag', tagName, 'linkType', 3, 'link', 'TxBeamIndex');

%% PitchTx

ySampleList = YSample.empty;
X = PitchTx;
nbSectors = size(X,2);
tagName = 'PitchTx';
if nbSectors == 1
    ySampleList(end+1) = YSample('data', X, 'unit', 'deg');
    ySampleList(end).name = tagName;
else
    for k=1:nbSectors
        sampleName = sprintf('%s, Ind=%d', tagName, k);
        ySampleList(end+1) = YSample('data', X(:,k), 'unit', 'deg'); %#ok<AGROW>
        ySampleList(end).name = sampleName;
    end
end
signalList(end+1) = ClSignal('name', tagName, 'xSample', timeSample, 'ySample', ySampleList, 'tag', tagName, 'linkType', 3, 'link', 'TxBeamIndex');

%% HeaveTx

ySampleList = YSample.empty;
X = HeaveTx;
nbSectors = size(X,2);
tagName = 'HeaveTx';
if nbSectors == 1
    ySampleList(end+1) = YSample('data', X, 'unit', 'm');
    ySampleList(end).name = tagName;
else
    for k=1:nbSectors
        sampleName = sprintf('%s, Ind=%d', tagName, k);
        ySampleList(end+1) = YSample('data', X(:,k), 'unit', 'm', 'linkType'); %#ok<AGROW>
        ySampleList(end).name = sampleName;
    end
end
signalList(end+1) = ClSignal('name', tagName, 'xSample', timeSample, 'ySample', ySampleList, 'tag', tagName, 'linkType', 3, 'link', 'TxBeamIndex');

%% HeadingTx

ySampleList = YSample.empty;
X = HeadingTx;
nbSectors = size(X,2);
tagName = 'HeadingTx';
if nbSectors == 1
    ySampleList(end+1) = YSample('data', X, 'unit', 'deg');
    ySampleList(end).name = tagName;
else
    clear ySample
    for k=1:nbSectors
        sampleName = sprintf('%s, Ind=%d', tagName, k);
        ySampleList(end+1) = YSample('data', X(:,k), 'unit', 'deg'); %#ok<AGROW>
        ySampleList(end).name = sampleName;
    end
end
signalList(end+1) = ClSignal('name', tagName, 'xSample', timeSample, 'ySample', ySampleList, 'tag', tagName, 'linkType', 3, 'link', 'TxBeamIndex');

%% Compute averaged attitude signals 

ConstanteDeTemps = 1*60 ; % 1 mn
[MeanVal, StdVal] = filtreAttitude(DataAttitude.Datetime, DataAttitude.Roll, ConstanteDeTemps);
RollMeanVal = my_interp1(DataAttitude.Datetime, MeanVal, TimeTx);
RollStdVal  = my_interp1(DataAttitude.Datetime, StdVal,  TimeTx);

[MeanVal, StdVal] = filtreAttitude(DataAttitude.Datetime, DataAttitude.Pitch, ConstanteDeTemps);
PitchMeanVal = my_interp1(DataAttitude.Datetime, MeanVal, TimeTx);
PitchStdVal  = my_interp1(DataAttitude.Datetime, StdVal,  TimeTx);

[MeanVal, StdVal] = filtreAttitude(DataAttitude.Datetime, DataAttitude.Heave, ConstanteDeTemps);
HeaveMeanVal = my_interp1(DataAttitude.Datetime, MeanVal, TimeTx);
HeaveStdVal  = my_interp1(DataAttitude.Datetime, StdVal,  TimeTx);

%% RollMean

ySampleList = YSample.empty;
X = RollMeanVal;
nbSectors = size(X,2);
tagName = 'RollMean';
if nbSectors == 1
    ySampleList = YSample('data', X, 'unit', 'deg');
    ySampleList.name = tagName;
else
    for k=1:nbSectors
        sampleName = sprintf('%s, Ind=%d', tagName, k);
        ySampleList(end+1) = YSample('data', X(:,k), 'unit', 'deg'); %#ok<AGROW>
        ySampleList(end).name = sampleName;
    end
end
signalList(end+1) = ClSignal('name', tagName, 'xSample', timeSample, 'ySample', ySampleList, 'tag', tagName, 'linkType', 3, 'link', 'TxBeamIndex');

%% RollStd

ySampleList = YSample.empty;
X = RollStdVal;
nbSectors = size(X,2);
tagName = 'RollStd';
if nbSectors == 1
    ySampleList = YSample('data', X, 'unit', 'deg');
    ySampleList.name = tagName;
else
    for k=1:nbSectors
        sampleName = sprintf('%s, Ind=%d', tagName, k);
        ySampleList(end+1) = YSample('data', X(:,k), 'unit', 'deg'); %#ok<AGROW>
        ySampleList(end).name = sampleName;
    end
end
signalList(end+1) = ClSignal('name', tagName, 'xSample', timeSample, 'ySample', ySampleList, 'tag', tagName, 'linkType', 3, 'link', 'TxBeamIndex');


%% PitchMean

ySampleList = YSample.empty;
X = PitchMeanVal;
nbSectors = size(X,2);
tagName = 'PitchMean';
if nbSectors == 1
    ySampleList = YSample('data', X, 'unit', 'deg');
    ySampleList.name = tagName;
else
    for k=1:nbSectors
        sampleName = sprintf('%s, Ind=%d', tagName, k);
        ySampleList(end+1) = YSample('data', X(:,k), 'unit', 'deg'); %#ok<AGROW>
        ySampleList(end).name = sampleName;
    end
end
signalList(end+1) = ClSignal('name', tagName, 'xSample', timeSample, 'ySample', ySampleList, 'tag', tagName, 'linkType', 3, 'link', 'TxBeamIndex');

%% PitchStd

ySampleList = YSample.empty;
X = PitchStdVal;
nbSectors = size(X,2);
tagName = 'PitchStd';
if nbSectors == 1
    ySampleList = YSample('data', X, 'unit', 'deg');
    ySampleList.name = tagName;
else
    for k=1:nbSectors
        sampleName = sprintf('%s, Ind=%d', tagName, k);
        ySampleList(end+1) = YSample('data', X(:,k), 'unit', 'deg'); %#ok<AGROW>
        ySampleList(end).name = sampleName;
    end
end
signalList(end+1) = ClSignal('name', tagName, 'xSample', timeSample, 'ySample', ySampleList, 'tag', tagName, 'linkType', 3, 'link', 'TxBeamIndex');

%% HeaveMean

ySampleList = YSample.empty;
X = HeaveMeanVal;
nbSectors = size(X,2);
tagName = 'HeaveMean';
if nbSectors == 1
    ySampleList = YSample('data', X, 'unit', 'deg');
    ySampleList.name = tagName;
else
    for k=1:nbSectors
        sampleName = sprintf('%s, Ind=%d', tagName, k);
        ySampleList(end+1) = YSample('data', X(:,k), 'unit', 'deg'); %#ok<AGROW>
        ySampleList(end).name = sampleName;
    end
end
signalList(end+1) = ClSignal('name', tagName, 'xSample', timeSample, 'ySample', ySampleList, 'tag', tagName, 'linkType', 3, 'link', 'TxBeamIndex');

%% HeaveStd

ySampleList = YSample.empty;
X = HeaveStdVal;
nbSectors = size(X,2);
tagName = 'HeaveStd';
if nbSectors == 1
    ySampleList = YSample('data', X, 'unit', 'deg');
    ySampleList.name = tagName;
else
    for k=1:nbSectors
        sampleName = sprintf('%s, Ind=%d', tagName, k);
        ySampleList(end+1) = YSample('data', X(:,k), 'unit', 'deg'); %#ok<AGROW>
        ySampleList(end).name = sampleName;
    end
end
signalList(end+1) = ClSignal('name', tagName, 'xSample', timeSample, 'ySample', ySampleList, 'tag', tagName, 'linkType', 3, 'link', 'TxBeamIndex');

%% Create signalContainer

signalContainer = SignalContainer('signalList', signalList, 'name', 'signalContainerFromAttitude', 'tag', 'Attitude');
signalContainer.originFilename = this.nomFic;

%% The end

flag = 1;
