% [flag, nomFic] = FtpUtils.getSScDemoFile('0029_20180306_213505_SimonStevin.all', 'OutputDir', 'D:\Temp\ExData');
% if flag
%     aKM = cl_simrad_all('nomFic', nomFic);
%     [flag, DataInstallationParameters, isDual] = read_installationParameters_bin(aKM);
%     if flag
%         [flag, signalContainerList, flagNoSignalLength] = create_signalsFromRawRange(aKM, DataInstallationParameters);
%         if flag
%             signalContainerList.plot();
%         end
%     end
% end

function [flag, signalContainerList, flagNoSignalLength] = create_signalsFromRawRange(this, DataSonarInstallationParameters, varargin)

[varargin, Mode]    = getPropertyValue(varargin, 'Mode',    []);
[varargin, DataRaw] = getPropertyValue(varargin, 'DataRaw', []);
[varargin, subRaw]  = getPropertyValue(varargin, 'subRaw',  []); %#ok<ASGLU>

flagNoSignalLength = 1;

% Cas sp�cial pour vieux fichiers .all (V1, F)
% [varargin, ySampleTransmitPulseLengthFromRuntime] = getPropertyValue(varargin, 'ySampleTransmitPulseLengthFromRuntime', []);

%% Lecture des datagrammes si pas pr�sents

if isempty(DataRaw)
    [flag, DataRaw] = read_rawRangeBeamAngle(this, 'Memmapfile', 1);
    if ~flag
        return
    end
end

if isempty(subRaw)
    subRaw = 1:DataRaw.Dimensions.nbPings;
end

%% Pr�paration des signaux

TsubRaw = DataRaw.Datetime(subRaw, 1);

%% Initialisation de l'instance ClSignal

mBESSignal        = ClSignal.empty();
environmentSignal = ClSignal.empty();
signalSignal      = ClSignal.empty();

%% Cr�ation de l'instance XSample

timeSample = XSample('name', 'time', 'data', TsubRaw);

%% Calcul des longueurs d'impulsion

if isfield(DataRaw, 'SignalBandwith')
    SignalLengthTotal = DataRaw.SignalLength(subRaw,:) * 1e3; % ms
    SignalBandWidth   = DataRaw.SignalBandwith(subRaw,:); % kHz
    if SignalBandWidth == 0
        SignalBandWidth = 1000 ./ SignalLengthTotal;
    end
    PulseLength_Nominal   = 1000 ./ SignalBandWidth;
    PulseLength_Effective = NaN(size(PulseLength_Nominal), 'single');
    SignalWaveformIdentifier = DataRaw.SignalWaveformIdentifier(subRaw,:);
    subCW = (SignalWaveformIdentifier == 0);

    if size(subCW,2) == 1
        subCW = repmat(subCW, 1, size(PulseLength_Effective,2));
    end
    
    if DataSonarInstallationParameters.EmModel == 3020
        PulseLength_Effective(subCW) = SignalLengthTotal(subCW);
    else
        PulseLength_Effective(subCW) = SignalLengthTotal(subCW) * 0.375; % Mail kjell.echholt.nilsen@km.kongsberg.com du 28/09/2017
    end
    PulseLength_Effective(~subCW) = 1000 ./ SignalBandWidth(~subCW); % Mail kjell.echholt.nilsen@km.kongsberg.com du 28/09/2017
    
    % Temporaire pour test Naig et Laurent
    %     PulseLength_Effective = SignalLengthTotal * 0.477; % Mail kjell.echholt.nilsen@km.kongsberg.com du 28/09/2017
    % Temporaire pour test Naig et Laurent
    
    if DataSonarInstallationParameters.EmModel == 850 % ME70
%     	PulseLength_UsedByKMinIA = SignalLengthTotal * 0.7;
        PulseLength_Effective    = SignalLengthTotal * (0.7^2);
%     else
%         if isfield(DataSonarInstallationParameters, 'PSV') && (versionSIS(DataSonarInstallationParameters.PSV(1:5)) < 2.22) % Modif JMA le 08/01/2019 sous couvert Ridha et Laurent
%             PulseLength_UsedByKMinIA = PulseLength_Nominal;
%         else
%             PulseLength_UsedByKMinIA = PulseLength_Effective;
%         end
    end
    flagNoSignalLength = 0;
    
else % Cas du format V1,F (EM1002)
%     sub = (Mode == 1); % Shallow
%     SignalLengthTotal(sub,1) = 0.2; % ms
%     sub = (Mode == 2); % Medium
%     SignalLengthTotal(sub,1) = 0.7; % ms
%     sub = (Mode == 3); % Deep
%     SignalLengthTotal(sub,1) = 2; % ms
    
%     PulseLength_Effective = 0.375 * SignalLengthTotal;
%     SignalBandWidth       = 1 ./ PulseLength_Effective; % kHz
%     PulseLength_Nominal   = PulseLength_Effective; % Pas tr�s s�r de mo : JMA le 14/04/2020
%     PulseLength_UsedByKMinIA = SignalLengthTotal;

    SignalWaveformIdentifier = ones(length(subRaw),1);
    
    PulseLength_Effective = [];
    PulseLength_Nominal   = [];
    SignalLengthTotal     = [];
    SignalBandWidth       = [];
end

%% NbBeams

if isfield(DataRaw, 'NbBeams')
    X = DataRaw.NbBeams(subRaw,1); % TODO : 2 valeurs sont donn�es pour un sondeur dual
elseif isfield(DataRaw, 'NRx')
    X = DataRaw.NRx(subRaw,1); % TODO : 2 valeurs sont donn�es pour un sondeur dual
end
if ~isempty(X)
    tagName = 'NbBeams';
    ySampleList = YSample('data', X);
    mBESSignal(end+1) = ClSignal('name', tagName, 'xSample', timeSample, 'ySample', ySampleList, 'tag', tagName);
else
    my_breakpoint
end

%% SoundSpeed

X = DataRaw.SoundSpeed(subRaw,1); % TODO : 2 valeurs sont donn�es pour un sondeur dual
tagName = 'SoundSpeed';
ySampleList = YSample('data', X, 'unit', 'm/s');
environmentSignal(end+1) = ClSignal('name', tagName, 'xSample', timeSample, 'ySample', ySampleList, 'tag', tagName);

%% SamplingRate

tagName = 'SamplingRate';
if isfield(DataRaw, tagName)
    X = DataRaw.SamplingRate(subRaw,1); % TODO : 2 valeurs sont donn�es pour un sondeur dual
    ySampleList = YSample('data', X, 'unit', 'Hz');
    signalSignal(end+1) = ClSignal('name', tagName, 'xSample', timeSample, 'ySample', ySampleList, 'tag', tagName);
end

%% TiltAngle

tagName = 'TiltAngle';
if isfield(DataRaw, tagName)
    X = DataRaw.TiltAngle(subRaw,:);
    nbSectors = size(X,2);
    if nbSectors == 1
        ySampleList = YSample('data', X, 'unit', 'deg');
    else
        ySampleList = YSample.empty();
        for k=1:nbSectors
            sampleName = sprintf('%s, Ind=%d', tagName, k);
            ySampleList(end+1) = YSample('data', X(:,k), 'unit', 'deg'); %#ok<AGROW>
            ySampleList(end).name = sampleName;
        end
    end
    mBESSignal(end+1) = ClSignal('name', tagName, 'xSample', timeSample, 'ySample', ySampleList, 'tag', tagName, 'linkType', 3, 'link', 'TxBeamIndex');
end

%% FocusRange

tagName = 'FocusRange';
if isfield(DataRaw, tagName)
    X = DataRaw.FocusRange(subRaw,:);
    nbSectors = size(X,2);
    if nbSectors == 1
        ySampleList = YSample('data', X, 'unit', 'm');
    else
        ySampleList = YSample.empty();
        for k=1:nbSectors
            sampleName = sprintf('%s, Ind=%d', tagName, k);
            ySampleList(end+1) = YSample('data', X(:,k), 'unit', 'm'); %#ok<AGROW>
            ySampleList(end).name = sampleName;
        end
    end
    mBESSignal(end+1) = ClSignal('name', tagName, 'xSample', timeSample, 'ySample', ySampleList, 'tag', tagName, 'linkType', 3, 'link', 'TxBeamIndex');
end

%% SectorTransmitDelay

tagName = 'SectorTransmitDelay';
if isfield(DataRaw, tagName)
    X = DataRaw.SectorTransmitDelay(subRaw,:);
    X = X * 1e-3;
    nbSectors = size(X,2);
    if nbSectors == 1
        ySampleList = YSample('data', X, 'unit', 'ms');
    else
        ySampleList = YSample.empty();
        for k=1:nbSectors
            sampleName = sprintf('%s, Ind=%d', tagName, k);
            ySampleList(end+1) = YSample('data', X(:,k), 'unit', 'ms'); %#ok<AGROW>
            ySampleList(end).name = sampleName;
        end
    end
    mBESSignal(end+1) = ClSignal('name', tagName, 'xSample', timeSample, 'ySample', ySampleList, 'tag', tagName, 'linkType', 3, 'link', 'TxBeamIndex');
end

%% CentralFrequency

tagName = 'CentralFrequency';
if isfield(DataRaw, tagName)
    X = DataRaw.CentralFrequency(subRaw,:);
    X = X * 1e-3;
    nbSectors = size(X,2);
    if nbSectors == 1
        ySampleList = YSample('data', X, 'unit', 'kHz');
    else
        ySampleList = YSample.empty();
        for k=1:nbSectors
            sampleName = sprintf('%s, Ind=%d', tagName, k);
            ySampleList(end+1) = YSample('data', X(:,k), 'unit', 'kHz'); %#ok<AGROW>
            ySampleList(end).name = sampleName;
        end
    end
    mBESSignal(end+1) = ClSignal('name', tagName, 'xSample', timeSample, 'ySample', ySampleList, 'tag', tagName, 'linkType', 3, 'link', 'TxBeamIndex');
end

%% ROV_Depth

tagName = 'ROV_Depth';
if isfield(DataRaw, tagName)
    X = DataRaw.ROV_Depth(subRaw,:);
    nbSectors = size(X,2);
    if nbSectors == 1
        ySampleList = YSample('data', X, 'unit', 'm');
    else
        ySampleList = YSample.empty();
        for k=1:nbSectors
            sampleName = sprintf('%s, Ind=%d', tagName, k);
            ySampleList(end+1) = YSample('data', X(:,k), 'unit', ''); %#ok<AGROW>
            ySampleList(end).name = sampleName;
        end
    end
    mBESSignal(end+1) = ClSignal('name', tagName, 'xSample', timeSample, 'ySample', ySampleList, 'tag', tagName, 'linkType', 3, 'link', 'TxBeamIndex');
end

%% SignalWaveformIdentifier

tagName = 'SignalWaveformIdentifier';
if ~isempty(SignalWaveformIdentifier)
    X = SignalWaveformIdentifier;
    nbSectors = size(X,2);
    if nbSectors == 1
        X = X + 1;
        ySampleList = IndexYSample('data', X, 'unit', '');
        ySampleList.strIndexList = {'CW'; 'FM up sweep'; 'FM down sweep'};
    else
        ySampleList = YSample.empty();
        for k=1:nbSectors
            sampleName = sprintf('%s, Ind=%d', tagName, k);
            ySampleList(end+1) = YSample('data', X(:,k), 'unit', ''); %#ok<AGROW>
            ySampleList(end).name = sampleName;
        end
    end
    mBESSignal(end+1) = ClSignal('name', tagName, 'xSample', timeSample, 'ySample', ySampleList, 'tag', tagName, 'linkType', 3, 'link', 'TxBeamIndex');
end

%% SignalBandWidth

tagName = 'SignalBandwith';
if ~isempty(SignalBandWidth)
    X = SignalBandWidth;
    nbSectors = size(X,2);
    if nbSectors == 1
        ySampleList = YSample('data', X, 'unit', 'Hz');
    else
        ySampleList = YSample.empty();
        for k=1:nbSectors
            sampleName = sprintf('%s, Ind=%d', tagName, k);
            ySampleList(end+1) = YSample('data', X(:,k), 'unit', 'Hz'); %#ok<AGROW>
            ySampleList(end).name = sampleName;
        end
    end
    mBESSignal(end+1) = ClSignal('name', tagName, 'xSample', timeSample, 'ySample', ySampleList, 'tag', tagName, 'linkType', 3, 'link', 'TxBeamIndex');
end

%% SignalLengthTotal

tagName = 'PulseLength_Total';
if ~isempty(SignalLengthTotal)
    X = SignalLengthTotal;
    nbSectors = size(X,2);
    if nbSectors == 1
        ySampleList = YSample('data', X, 'unit', 'ms');
    else
        ySampleList = YSample.empty();
        for k=1:size(X,2)
            sampleName = sprintf('%s, Ind=%d', tagName, k);
            ySampleList(end+1) = YSample('data', X(:,k), 'unit', 'ms'); %#ok<AGROW>
            ySampleList(end).name = sampleName;
        end
    end
    mBESSignal(end+1) = ClSignal('name', tagName, 'xSample', timeSample, 'ySample', ySampleList, 'tag', tagName, 'linkType', 3, 'link', 'TxBeamIndex');
end

%% PulseLength_Nominal

tagName = 'PulseLength_Nominal';
if ~isempty(PulseLength_Nominal)
    X = PulseLength_Nominal;
    nbSectors = size(X,2);
    if nbSectors == 1
        ySampleList = YSample('data', X, 'unit', 'ms');
    else
        ySampleList = YSample.empty();
        for k=1:size(X,2)
            sampleName = sprintf('%s, Ind=%d', tagName, k);
            ySampleList(end+1) = YSample('data', X(:,k), 'unit', 'ms'); %#ok<AGROW>
            ySampleList(end).name = sampleName;
        end
    end
    mBESSignal(end+1) = ClSignal('name', tagName, 'xSample', timeSample, 'ySample', ySampleList, 'tag', tagName, 'linkType', 3, 'link', 'TxBeamIndex');
end

%% PulseLength_Effective

tagName = 'PulseLength_Effective';
if ~isempty(PulseLength_Effective)
    X = PulseLength_Effective;
    nbSectors = size(X,2);
    if nbSectors == 1
        ySampleList = YSample('data', X, 'unit', 'ms');
    else
        ySampleList = YSample.empty();
        for k=1:size(X,2)
            sampleName = sprintf('%s, Ind=%d', tagName, k);
            ySampleList(end+1) = YSample('data', X(:,k), 'unit', 'ms', 'linkType'); %#ok<AGROW>
            ySampleList(end).name = sampleName;
        end
    end
    mBESSignal(end+1) = ClSignal('name', tagName, 'xSample', timeSample, 'ySample', ySampleList, 'tag', tagName, 'linkType', 3, 'link', 'TxBeamIndex');
end

%% PulseLength_UsedByKMinIA

%{
tagName = 'PulseLength_UsedByKMinIA';
if ~isempty(PulseLength_UsedByKMinIA)
    X = PulseLength_UsedByKMinIA;
    nbSectors = size(X,2);
    if nbSectors == 1
        ySampleList = YSample('data', X, 'unit', 'ms');
    else
        ySampleList = YSample.empty();
        for k=1:size(X,2)
            sampleName = sprintf('%s, Ind=%d', tagName, k);
            ySampleList(end+1) = YSample('data', X(:,k), 'unit', 'ms'); %#ok<AGROW>
            ySampleList(end).name = sampleName;
        end
    end
    mBESSignal(end+1) = ClSignal('name', tagName, 'xSample', timeSample, 'ySample', ySampleList, 'tag', tagName, 'linkType', 3, 'link', 'TxBeamIndex');
end
%}

%% Create signalContainer

%MBES SignalContainer
signalContainerList(1) = SignalContainer('signalList', mBESSignal, 'name', 'MBES', 'tag', 'MBES');
% Environement SignalContainer
signalContainerList(2) = SignalContainer('signalList', environmentSignal, 'name', 'Environment', 'tag', 'Environement');
% Signal SignalContainer
signalContainerList(3) = SignalContainer('signalList', signalSignal, 'name', 'Signal', 'tag', 'Signal');

%% The end

flag = 1;
