%% TODO : quand Pierre aura r�alis� les am�liorations Signal/SignalIndex avec matlab.mixin.Heterogeneous alors sortir des signaux du genre signalsBDA, signalsXXX, ...

function [flag, signalContainerList] = create_signalsFromRuntime(this, TsubDepth, varargin)

[varargin, DataRuntime]        = getPropertyValue(varargin, 'DataRuntime',       []);
[varargin, CreateSignalLength] = getPropertyValue(varargin, 'CreateSignalLength', 0); %#ok<ASGLU>

TsubDepth = TsubDepth(:,1); % Modif JMA le 04/03/2020 pour lecture cache Netcdf

%% init signal arrays

confSignal          = ClSignal.empty();
signalSignal        = ClSignal.empty();
apertureSignal      = ClSignal.empty();
bDASignal           = ClSignal.empty();
mBESSignal          = ClSignal.empty();
stabilizationSignal = ClSignal.empty();

%% Lecture des datagrammes si pas pr�sents

if isempty(DataRuntime)
    [flag, DataRuntime] = SSc_ReadDatagrams(this.nomFic, 'Ssc_Runtime');
    if ~flag
        return
    end
end

%% Gestion de l'historique

if isfield(DataRuntime, 'ModeDatagram')
    Mode = DataRuntime.ModeDatagram(:);
    flagComputeOtherDataFromMode = 1;
else
    Mode = DataRuntime.Mode(:) - 1; % Pour pouvoir continuer � exploiter les fichiers qui ont �t� convertis avant l'introduction de ModeDatagram
    flagComputeOtherDataFromMode = 0;
end

%% Cr�ation de l'instance XSample

timeSample = XSample('name', 'time', 'data', TsubDepth);

%% Mode1

X = 1 + decodeBits(Mode, [4 3 2 1]);
switch DataRuntime.EmModel
    case 3000
        strX = {'Nearfield (4�)'; 'Normal (1.5�)'; 'Target detect'};
        
    case {3002; 3020}
        strX = {'Wide Tx beamwidth (4�)'; 'Normal Tx beamwidth (1.5�)'};
        
    case {2000; 710; 1002; 300; 302; 120; 122}
        strX = {'Very shallow'; 'Shallow'; 'Medium'; 'Deep'; 'Very deep'; 'Extra deep'};
        
    case 2040
        strX = {'200 kHz'; '300 kHz'; '400 kHz'};
        
    case 2045 % 2040C
        X = 1 + decodeBits(Mode, [5 4 3 2 1]);
        for k=23:-1:1
            strX{k} = sprintf('%d kHz', 180 + (k-1) * 10);
        end
        
    case 304
        X = 1 + decodeBits(Mode, [2 1]);
        strX = {'Very shallow'; 'Shallow'; 'Medium'; 'Deep'; 'Deeper'; 'Very deep'; 'Extra deep'; 'Extreme Deep'};
        
    case 850
        X = 1 + decodeBits(Mode, [2 1]);
%         if DataDepth.Dimensions.nbBeams <= 51
%             Z.strModes = {''; ''; 'Fisheries'; ''; ''};
%         else
            strX = {'xshort'; 'short'; 'long'; 'xlong'; 'FM'};
%         end
        
    otherwise
        X = 1 + decodeBits(Mode, [2 1]);
        X(:) = NaN;
        strX = [];
end

TRuntime = DataRuntime.Datetime;
X = my_interp1_Extrap_PreviousThenNext(TRuntime, X, TsubDepth);
% Mode1 = X;
tagName = 'Mode1';
ySampleList = IndexYSample('data', X, 'strIndexList', strX);
confSignal(end+1) = ClSignal('name', tagName, 'xSample', timeSample, 'ySample', ySampleList, 'tag', tagName);

%% DualSwathModeOrder

if flagComputeOtherDataFromMode
    switch DataRuntime.EmModel
        case {2040; 710; 712; 302; 304; 122; 124}
            X = 1 + decodeBits(Mode, [8 7]);
            strX = {'Off'; 'Fixed'; 'Dynamic'};
            X = my_interp1_Extrap_PreviousThenNext(TRuntime, X, TsubDepth);
            tagName = 'DualSwathModeOrder';
            ySampleList = IndexYSample('data', X, 'strIndexList', strX);
            confSignal(end+1) = ClSignal('name', tagName, 'xSample', timeSample, 'ySample', ySampleList, 'tag', tagName);
    end
    
    %% TxPulseForm
    
    switch DataRuntime.EmModel
        case {2040; 710; 302; 122}
            X = 1 + decodeBits(Mode, [6 5]);
            strX = {'CW'; 'Mixed'; 'FM'};
            X = my_interp1_Extrap_PreviousThenNext(TRuntime, X, TsubDepth);
            tagName = 'TxPulseForm';
            ySampleList = IndexYSample('data', X, 'strIndexList', strX);
            signalSignal(end+1) = ClSignal('name', tagName, 'xSample', timeSample, 'ySample', ySampleList, 'tag', tagName);
            
        case 2045
            X = 1 + decodeBits(Mode, 6);
            strX = {'CW'; 'FM'};
            X = my_interp1_Extrap_PreviousThenNext(TRuntime, X, TsubDepth);
            tagName = 'TxPulseForm';
            ySampleList = IndexYSample('data', X, 'strIndexList', strX);
            signalSignal(end+1) = ClSignal('name', tagName, 'xSample', timeSample, 'ySample', ySampleList, 'tag', tagName);
    end
end

%% TxAlongBeamWidth

X = DataRuntime.TransmitBeamWidth(:,:);
X = my_interp1_Extrap_PreviousThenNext(TRuntime, X, TsubDepth);
% X = changeValueIfEM2040(DataRuntime.EmModel, X, Mode1); % Comment� par JMA le 22/22/2021
tagName = 'TxAlongBeamWidth';
ySampleList = YSample('data', X, 'unit', 'deg');
apertureSignal(end+1) = ClSignal('name', tagName, 'xSample', timeSample, 'ySample', ySampleList, 'tag', tagName);

%% RxAcrossBeamWidth

X = DataRuntime.ReceiveBeamWidth(:,:);
X = my_interp1_Extrap_PreviousThenNext(TRuntime, X, TsubDepth);
% X = changeValueIfEM2040(DataRuntime.EmModel, X, Mode1); % Comment� par JMA le 22/22/2021
tagName = 'RxAcrossBeamWidth';
ySampleList = YSample('data', X, 'unit', 'deg');
apertureSignal(end+1) = ClSignal('name', tagName, 'xSample', timeSample, 'ySample', ySampleList, 'tag', tagName);

%% FilterIdentifier

%{
The filter identifier byte is used as follows:
� xxxx xx00 - Spike filter set to Off
� xxxx xx01 - Spike filter is set to Weak
� xxxx xx10 - Spike filter is set to Medium
� xxxx xx11 - Spike filter is set to Strong
� xxxx x1xx - Slope filter is on
� xxxx 1xxx - Sector tracking or Robust Bottom Detection (EM 3000) is on
� 0xx0 xxxx - Range gates have Normal size
� 0xx1 xxxx - Range gates are Large
� 1xx0 xxxx - Range gates are Small
� xx1x xxxx - Aeration filter is on
� x1xx xxxx - Interference filter is on
%}

FilterIdentifier = DataRuntime.FilterIdentifier(:);
X = 1 + decodeBits(FilterIdentifier, [2 1]);
strX = {'Off'; 'Weak'; 'Medium'; 'Strong'};
X = my_interp1_Extrap_PreviousThenNext(TRuntime, X, TsubDepth);
tagName = 'SpikeFilter';
ySampleList = IndexYSample('data', X, 'strIndexList', strX);
bDASignal(end+1) = ClSignal('name', tagName, 'xSample', timeSample, 'ySample', ySampleList, 'tag', tagName);

X = decodeBits(FilterIdentifier, 3);
X = my_interp1_Extrap_PreviousThenNext(TRuntime, X, TsubDepth);
tagName =  'SlopeFilter';
ySampleList = BooleanYSample('data', X);
bDASignal(end+1) = ClSignal('name', tagName, 'xSample', timeSample, 'ySample', ySampleList, 'tag', tagName);

X = decodeBits(FilterIdentifier, 4);
X = my_interp1_Extrap_PreviousThenNext(TRuntime, X, TsubDepth);
tagName =  'SectorTracking';
ySampleList = BooleanYSample('data', X);
bDASignal(end+1) = ClSignal('name', tagName, 'xSample', timeSample, 'ySample', ySampleList, 'tag', tagName);

X = 1 + decodeBits(FilterIdentifier, [8 5]);
strX = {'Normal size'; 'Large'; 'Small'};
X = my_interp1_Extrap_PreviousThenNext(TRuntime, X, TsubDepth);
tagName =  'RangeGates';
ySampleList = IndexYSample('data', X, 'strIndexList', strX);
bDASignal(end+1) = ClSignal('name', tagName, 'xSample', timeSample, 'ySample', ySampleList, 'tag', tagName);

X = decodeBits(FilterIdentifier, 6);
X = my_interp1_Extrap_PreviousThenNext(TRuntime, X, TsubDepth);
tagName =  'AerationFilter';
ySampleList = BooleanYSample('data', X);
bDASignal(end+1) = ClSignal('name', tagName, 'xSample', timeSample, 'ySample', ySampleList, 'tag', tagName);

X = decodeBits(FilterIdentifier, 7);
X = my_interp1_Extrap_PreviousThenNext(TRuntime, X, TsubDepth);
tagName =  'InterferenceFilter';
ySampleList = BooleanYSample('data', X);
bDASignal(end+1) = ClSignal('name', tagName, 'xSample', timeSample, 'ySample', ySampleList, 'tag', tagName);

%% Min and Max Depth

X = DataRuntime.MinimumDepth(:);
X = my_interp1_Extrap_PreviousThenNext(TRuntime, X, TsubDepth);
tagName =  'MinimumDepth';
ySampleList = YSample('data', X, 'unit', 'm');
bDASignal(end+1) = ClSignal('name', tagName, 'xSample', timeSample, 'ySample', ySampleList, 'tag', tagName);

X = DataRuntime.MaximumDepth(:);
X = my_interp1_Extrap_PreviousThenNext(TRuntime, X, TsubDepth);
tagName =  'MaximumDepth';
ySampleList(end+1) = YSample('data', X, 'unit', 'm');
bDASignal(end+1) = ClSignal('name', tagName, 'xSample', timeSample, 'ySample', ySampleList, 'tag', tagName);

%% TransmitPulseLength

if CreateSignalLength % Pour donn�es anciennes (EM1002 de Marc Roche)
    X = DataRuntime.TransmitPulseLength(:) / 1000;
    X = my_interp1_Extrap_PreviousThenNext(TRuntime, X, TsubDepth);
    
    SignalLengthTotal   = X; % ms
    SignalLengthNominal = X;
%     SignalLengthEffective = SignalLengthTotal * 0.375; % Mail kjell.echholt.nilsen@km.kongsberg.com du 28/09/2017
    SignalLengthEffective = SignalLengthTotal; % Mail kjell.echholt.nilsen@km.kongsberg.com du 28/09/2017
    
    PulseLength_UsedByKMinIA = SignalLengthNominal;
    SignalBandWidth          = 1 ./ SignalLengthEffective; % kHz
    
    %% SignalLengthTotal
    
    fieldName = 'PulseLength_Total';
%     if ~isempty(SignalLengthTotal)
        X = SignalLengthTotal;
        nbSectors = size(X,2);
        tagName =  fieldName;
        if nbSectors == 1
            ySampleList = YSample('data', X, 'unit', 'ms');
        else
            ySampleList = AdvancedYSample.empty();
            for k=1:size(X,2)
                name = sprintf('%s, Ind=%d', fieldName, k);
                ySampleList(end+1) = AdvancedYSample('data', X(:,k), 'unit', 'ms', 'typeLink', 3, 'link', 'TxBeamIndex'); %#ok<AGROW>
                ySampleList(end).tag  = ySampleTag;
                ySampleList(end).name = name;
            end
        end
        mBESSignal(end+1) = ClSignal('name', tagName, 'xSample', timeSample, 'ySample', ySampleList, 'tag', tagName);
%     end
    
    %% SignalLengthNominal
    
    fieldName = 'PulseLength_Nominal';
%     if ~isempty(SignalLengthNominal)
        X = SignalLengthNominal;
        nbSectors = size(X,2);
        tagName =  fieldName;
        if nbSectors == 1
            ySampleList = YSample('data', X, 'unit', 'ms');
        else
            ySampleList = AdvancedYSample.empty();
            for k=1:size(X,2)
                name = sprintf('%s, Ind=%d', fieldName, k);
                ySampleList(end+1) = AdvancedYSample('data', X(:,k), 'unit', 'ms', 'typeLink', 3, 'link', 'TxBeamIndex'); %#ok<AGROW>
                ySampleList(end).tag  = ySampleTag;
                ySampleList(end).name = name;
            end
        end
        mBESSignal(end+1) = ClSignal('name', tagName, 'xSample', timeSample, 'ySample', ySampleList, 'tag', tagName);
%     end
    
    %% SignalLengthEffective
    
    fieldName = 'PulseLength_Effective';
%     if ~isempty(SignalLengthEffective)
        X = SignalLengthEffective;
        nbSectors = size(X,2);
        tagName =  fieldName;
        if nbSectors == 1
            ySampleList = YSample('data', X, 'unit', 'ms');
        else
            ySampleList = AdvancedYSample.empty();
            for k=1:size(X,2)
                name = sprintf('%s, Ind=%d', fieldName, k);
                ySampleList(end+1) = AdvancedYSample('data', X(:,k), 'unit', 'ms', 'typeLink', 3, 'link', 'TxBeamIndex'); %#ok<AGROW>
                ySampleList(end).tag  = ySampleTag;
                ySampleList(end).name = name;
            end
        end
        mBESSignal(end+1) = ClSignal('name', tagName, 'xSample', timeSample, 'ySample', ySampleList, 'tag', tagName);
%     end
    
    %% PulseLength_UsedByKMinIA
    
    fieldName = 'PulseLength_UsedByKMinIA';
%     if ~isempty(PulseLength_UsedByKMinIA)
        X = PulseLength_UsedByKMinIA;
        nbSectors = size(X,2);
        tagName = fieldName;
        if nbSectors == 1
            ySampleList = YSample('data', X, 'unit', 'ms');
        else
            ySampleList = AdvancedYSample.empty();
            for k=1:size(X,2)
                name = sprintf('%s, Ind=%d', fieldName, k);
                ySampleList(end+1) = AdvancedYSample('data', X(:,k), 'unit', 'ms', 'typeLink', 3, 'link', 'TxBeamIndex'); %#ok<AGROW>
                ySampleList(end).tag  = ySampleTag;
                ySampleList(end).name = name;
            end
        end
        mBESSignal(end+1) = ClSignal('name', tagName, 'xSample', timeSample, 'ySample', ySampleList, 'tag', tagName);
%     end
    
    fieldName = 'SignalBandwith';
%     if ~isempty(SignalBandWidth)
        X = SignalBandWidth;
        nbSectors = size(X,2);
        tagName = fieldName;
        if nbSectors == 1
            ySampleList = YSample('data', X, 'unit', 'Hz');
        else
            ySampleList = YSample.empty();
            for k=1:nbSectors
                sampleName = sprintf('%s, Ind=%d', fieldName, k);
                ySampleList(end+1) = YSample('data', X(:,k), 'unit', 'Hz'); %#ok<AGROW>
                ySampleList(end).name = sampleName;
            end
        end
        mBESSignal(end+1) = ClSignal('name', tagName, 'xSample', timeSample, 'ySample', ySampleList, 'tag', tagName, 'linkType', 3, 'link', 'TxBeamIndex');
%     end

else
    X = DataRuntime.TransmitPulseLength(:);
    X = my_interp1_Extrap_PreviousThenNext(TRuntime, X, TsubDepth);
    tagName =  'TransmitPulseLengthFromRuntime';
    ySampleList = YSample('data', X, 'unit', 'ms');
    signalSignal(end+1) = ClSignal('name', tagName, 'xSample', timeSample, 'ySample', ySampleList, 'tag', tagName);
    
    tagName =  'PulseLength_UsedByKMinIA';
    ySampleList = YSample('data', X, 'unit', 'ms');
    signalSignal(end+1) = ClSignal('name', tagName, 'xSample', timeSample, 'ySample', ySampleList, 'tag', tagName);
end

%% TxPowerRefMax

X = DataRuntime.TransmitPowerMax(:);
X = my_interp1_Extrap_PreviousThenNext(TRuntime, X, TsubDepth);
tagName =  'TxPowerRefMax';
ySampleList = YSample('data', X, 'unit', 'dB');
signalSignal(end+1) = ClSignal('name', tagName, 'xSample', timeSample, 'ySample', ySampleList, 'tag', tagName);

%% ReceiveBandWidth

X = DataRuntime.ReceiveBandWidth(:) / 1000;
X = my_interp1_Extrap_PreviousThenNext(TRuntime, X, TsubDepth);
tagName =  'ReceiveBandWidth';
ySampleList = YSample('data', X, 'unit', 'kHz');
ySampleList.comment = 'Value >= 12.7 kHz indicates bandwidth larger than 12.7 kHz';
signalSignal(end+1) = ClSignal('name', tagName, 'xSample', timeSample, 'ySample', ySampleList, 'tag', tagName);

%% Mode2 or RxFixedGain

% TODO : KM appelle �a Mode2 mais Mode2 devrait �tre Normal / Single sector
% / Scaning et Mode3 serait ce qui suit.
Mode2 = uint8(DataRuntime.ReceiverFixGain(:));
switch DataRuntime.EmModel
    case {2000; 1002; 3000; 3002; 300; 120; 3020}
        X = my_interp1_Extrap_PreviousThenNext(TRuntime, Mode2, TsubDepth);
        tagName =  'RxFixedGain';
        ySampleList = YSample('data', X, 'unit', 'dB');
        confSignal(end+1) = ClSignal('name', tagName, 'xSample', timeSample, 'ySample', ySampleList, 'tag', tagName);
        
    case 2040
        X = 1 + decodeBits(Mode2, [2 1]);
        strX = {'Port Off - Starboard Off'; 'Port On - Starboard Off'; 'Port Off - Starboard On'; 'Port On - Starboard On'};
        X = my_interp1_Extrap_PreviousThenNext(TRuntime, X, TsubDepth);
        tagName =  'SideActiveForDual';
        ySampleList = IndexYSample('data', X, 'strIndexList', strX);
        confSignal(end+1) = ClSignal('name', tagName, 'xSample', timeSample, 'ySample', ySampleList, 'tag', tagName);
        
        X = 1 + decodeBits(Mode2, [4 3]);
        strX = {'S'; 'M'; 'L'; 'FM'};
        X = my_interp1_Extrap_PreviousThenNext(TRuntime, X, TsubDepth);
        tagName =  'PulseLengthConf';
        ySampleList = IndexYSample('data', X, 'strIndexList', strX);
        signalSignal(end+1) = ClSignal('name', tagName, 'xSample', timeSample, 'ySample', ySampleList, 'tag', tagName);
        
    case 2045
        X = 1 + decodeBits(Mode2, [2 1]);
        strX = {'Port Off - Starboard Off'; 'Port On - Starboard Off'; 'Port Off - Starboard On'; 'Port On - Starboard On'};
        X = my_interp1_Extrap_PreviousThenNext(TRuntime, X, TsubDepth);
        tagName =  'SideActiveForDual';
        ySampleList = IndexYSample('data', X, 'strIndexList', strX);
        confSignal(end+1) = ClSignal('name', tagName, 'xSample', timeSample, 'ySample', ySampleList, 'tag', tagName);
        
        X = 1 + decodeBits(Mode2, [7 6 5]);
        strX = {'Very Short CW'; 'Short CW'; 'Medium CW'; 'Long CW'; 'Very Long CW'; 'Extra Long CW'; 'Short FM'; 'Long FM'};
        X = my_interp1_Extrap_PreviousThenNext(TRuntime, X, TsubDepth);
        if isnan(X) % Cas o� l'heure du premier ping est ant�rieure � l'heure du premier datagramme Runtime
            X = 1 + decodeBits(Mode2(1), [7 6 5]);
        end
        tagName =  'PulseLengthConf';
        ySampleList = IndexYSample('data', X, 'strIndexList', strX);
        signalSignal(end+1) = ClSignal('name', tagName, 'xSample', timeSample, 'ySample', ySampleList, 'tag', tagName);
        
    case {122; 302; 304; 710; 712; 850}
        % TODO : Apparemment non utilis�
        
    otherwise
        messageForDebugInspection
end
% Mode3 = X;

% TODO : Extra detections enable; Sonar mode enables; Passive mode enabled;
% 3D scanning enabled ???

%% Maximum port and starboard swath width and coverage

X = DataRuntime.MaximumPortSwath(:);
X = my_interp1_Extrap_PreviousThenNext(TRuntime, X, TsubDepth);
tagName =  'MaximumPortSwath';
ySampleList = YSample('data', X, 'unit', 'm');
bDASignal(end+1) = ClSignal('name', tagName, 'xSample', timeSample, 'ySample', ySampleList, 'tag', tagName);

X = DataRuntime.MaximumStarboardSwath(:);
X = my_interp1_Extrap_PreviousThenNext(TRuntime, X, TsubDepth);
tagName =  'MaximumStarboardSwath';
ySampleList = YSample('data', X, 'unit', 'm');
bDASignal(end+1) = ClSignal('name', tagName, 'xSample', timeSample, 'ySample', ySampleList, 'tag', tagName);

X = DataRuntime.MaximumPortCoverage(:);
X = my_interp1_Extrap_PreviousThenNext(TRuntime, X, TsubDepth);
tagName =  'MaximumPortCoverage';
ySampleList = YSample('data', X, 'unit', 'deg');
bDASignal(end+1) = ClSignal('name', tagName, 'xSample', timeSample, 'ySample', ySampleList, 'tag', tagName);

X = DataRuntime.MaximumStarboardCoverage(:);
X = my_interp1_Extrap_PreviousThenNext(TRuntime, X, TsubDepth);
tagName =  'MaximumStarboardCoverage';
ySampleList = YSample('data', X, 'unit', 'deg');
bDASignal(end+1) = ClSignal('name', tagName, 'xSample', timeSample, 'ySample', ySampleList, 'tag', tagName);

%% Source of sound speed at transducer

SurfaceSoundSpeedOrigin = uint8(DataRuntime.SourceSoundSpeed(:));
X = 1 + decodeBits(SurfaceSoundSpeedOrigin, [2 1]);
strX = {'From real time sensor'; 'Manually entered by operator'; 'Interpolated from currently used sound speed profile'; 'Calculated by ME70BO TRU'};
X = my_interp1_Extrap_PreviousThenNext(TRuntime, X, TsubDepth);
tagName =  'SurfaceSoundSpeedOrigin';
ySampleList = IndexYSample('data', X, 'strIndexList', strX);
confSignal(end+1) = ClSignal('name', tagName, 'xSample', timeSample, 'ySample', ySampleList, 'tag', tagName);

%% BeamSpacingIdentifier

BeamSpacingIdentifier = uint8(DataRuntime.BeamSpacing(:));
X = 1 + decodeBits(BeamSpacingIdentifier, [2 1]);
switch DataRuntime.EmModel
    case 3000
        strX = {'Determined by beamwidth'; 'Equidistant'; 'Equiangle'; 'Hish density'};
    case 3002
        strX = {'Not used'; 'Equidistant'; 'Equiangle'; 'High density'};
    case {122; 124; 302; 304; 702; 704}
        strX = {'Not used'; 'Inbetween'; 'Equiangle'; 'High density'};
    case {2000; 120; 300; 1002}
        strX = {'Not used'; 'Equidistant'; 'Equiangle'; 'Inbetween'};
    otherwise
        strX = {'Determined by beamwidth'; 'Equidistant'; 'Equiangle'; 'High density'};
end
X = my_interp1_Extrap_PreviousThenNext(TRuntime, X, TsubDepth);
tagName =  'BeamSpacingIdentifier';
ySampleList = IndexYSample('data', X, 'strIndexList', strX);
confSignal(end+1) = ClSignal('name', tagName, 'xSample', timeSample, 'ySample', ySampleList, 'tag', tagName);

%% Yaw and pitch stabilization

YawStabMode = uint8(DataRuntime.YawStabMode(:));

X = 1 + decodeBits(YawStabMode, [2 1]);
strX = {'Off'; 'Survey line heading (Not used)'; 'Mean vessel heading'; 'Manually entered heading'};
X = my_interp1_Extrap_PreviousThenNext(TRuntime, X, TsubDepth);
tagName =  'YawStabilization';
ySampleList = IndexYSample('data', X, 'strIndexList', strX);
stabilizationSignal(end+1) = ClSignal('name', tagName, 'xSample', timeSample, 'ySample', ySampleList, 'tag', tagName);

X = 1 + decodeBits(YawStabMode, [4 3]);
strX = {'Hard'; 'Medium'; 'Weak'};
X = my_interp1_Extrap_PreviousThenNext(TRuntime, X, TsubDepth);
tagName =  'YawAndHeadingFilter';
ySampleList = IndexYSample('data', X, 'strIndexList', strX);
stabilizationSignal(end+1) = ClSignal('name', tagName, 'xSample', timeSample, 'ySample', ySampleList, 'tag', tagName);

X = decodeBits(YawStabMode, 8);
X = my_interp1_Extrap_PreviousThenNext(TRuntime, X, TsubDepth);
tagName =  'PitchStabilization';
ySampleList = BooleanYSample('data', X);
stabilizationSignal(end+1) = ClSignal('name', tagName, 'xSample', timeSample, 'ySample', ySampleList, 'tag', tagName);

%% Transmit Along TiltOffset

if ~DataRuntime.EmModel == 1002
    % TODO : ATTENTION : valeur apparemment fausse
    X = 10 * single(typecast(DataRuntime.DurotongSpeed(:), 'int16'));
    X = my_interp1_Extrap_PreviousThenNext(TRuntime, X, TsubDepth);
    tagName =  'TxAlongTiltOffset';
    ySampleList = YSample('data', X, 'unit', 'deg');
    stabilizationSignal(end+1) = ClSignal('name', tagName, 'xSample', timeSample, 'ySample', ySampleList, 'tag', tagName);
end

%% Filter identifier 2

FilterIdentifier2 = DataRuntime.HiLoFreqAbsCoefRatio(:); % FilterIdentifier

X = 1 + decodeBits(FilterIdentifier2, [2 1]);
switch DataRuntime.EmModel
    case {2040; 710; 302; 122; 124; 304; 704} % TODO 2045 ?
        strX = {'Off'; 'Weak'; 'Medium'; 'Strong'};
    otherwise
        strX = {};
end
if ~isempty(strX)
    X = my_interp1_Extrap_PreviousThenNext(TRuntime, X, TsubDepth);
    tagName =  'PenetrationFilter';
    ySampleList = IndexYSample('data', X, 'strIndexList', strX);
    bDASignal(end+1) = ClSignal('name', tagName, 'xSample', timeSample, 'ySample', ySampleList, 'tag', tagName);
end

X = 1 + decodeBits(FilterIdentifier2, [4 3]);
switch DataRuntime.EmModel
    case {3002; 2040}
        strX = {'Normal'; 'Waterway'; 'Tracking'; 'Minimum depth'};
    otherwise
        strX = {};
end
if ~isempty(strX)
    X = my_interp1_Extrap_PreviousThenNext(TRuntime, X, TsubDepth);
    tagName =  'DetectionMode';
    ySampleList = IndexYSample('data', X, 'strIndexList', strX);
    bDASignal(end+1) = ClSignal('name', tagName, 'xSample', timeSample, 'ySample', ySampleList, 'tag', tagName);
end

X = 1 + decodeBits(FilterIdentifier2, [6 5]);
switch DataRuntime.EmModel
    case {2040; 3002; 710; 302; 122; 124; 304; 704} % TODO 2045 ?
%         strX = {'Short'; 'Normal'; 'Long'};
        strX = {'Short'; 'Normal'; 'Long'; 'Normal'}; % Rajout� par JMA le 09/01/2019 pour fichier F:\SPFE\Problem_EM2040SimonStevin\0110_20171122_042859_SimonStevin.all
    otherwise
        strX = {};
end
if ~isempty(strX)
    X = my_interp1_Extrap_PreviousThenNext(TRuntime, X, TsubDepth);
    tagName =  'PhaseRamp';
    ySampleList = IndexYSample('data', X, 'strIndexList', strX);
    bDASignal(end+1) = ClSignal('name', tagName, 'xSample', timeSample, 'ySample', ySampleList, 'tag', tagName);
end

X = 1 + decodeBits(FilterIdentifier2, 7);
switch DataRuntime.EmModel
    case {3002; 2040}
        strX = {'Normal'; 'Special'};
    otherwise
        strX = {};
end
if ~isempty(strX)
    X = my_interp1_Extrap_PreviousThenNext(TRuntime, X, TsubDepth);
    tagName =  'TypeTVG';
    ySampleList = IndexYSample('data', X, 'strIndexList', strX);
    bDASignal(end+1) = ClSignal('name', tagName, 'xSample', timeSample, 'ySample', ySampleList, 'tag', tagName);
end

X = 1 + decodeBits(FilterIdentifier2, 8);
strX = {'Normal'; 'Special or soft sediment'};
X = my_interp1_Extrap_PreviousThenNext(TRuntime, X, TsubDepth);
tagName =  'AmpDetect';
ySampleList = IndexYSample('data', X, 'strIndexList', strX);
bDASignal(end+1) = ClSignal('name', tagName, 'xSample', timeSample, 'ySample', ySampleList, 'tag', tagName);


%% Create signalContainers

%Conf SignalContainer
signalContainerList(1) = SignalContainer('signalList', confSignal, 'name', 'Conf', 'tag', 'Conf');
% Signal SignalContainer
signalContainerList(2) = SignalContainer('signalList', signalSignal, 'name', 'Signal', 'tag', 'Signal');
% Aperture SignalContainer
signalContainerList(3) = SignalContainer('signalList', apertureSignal, 'name', 'Aperture', 'tag', 'Aperture');
% BDA SignalContainer
signalContainerList(4) = SignalContainer('signalList', bDASignal, 'name', 'BDA', 'tag', 'BDA');
% MBES SignalContainer
signalContainerList(5) = SignalContainer('signalList', mBESSignal, 'name', 'MBES', 'tag', 'MBES');
% Stabilization SignalContainer
signalContainerList(6) = SignalContainer('signalList', stabilizationSignal, 'name', 'Stabilization', 'tag', 'StabilizationS');

for k=1:numel(signalContainerList)
    signalContainerList(k).originFilename = this.nomFic;
end

%% The end

flag = 1;


function X = changeValueIfEM2040(EmModel, X, Mode1)
switch EmModel
    case 2040
        sub = find(Mode1 == 1); % 200 kHz
        X(sub) = X(sub) * (300 / 200);
        
        sub = find(Mode1 == 3); % 400 kHz
        X(sub) = X(sub) * (300 / 400);
        
    case 2045
        X = X .* (300 ./ (180 + (Mode1-1) * 10));
end
