% [flag, nomFic] = FtpUtils.getSScDemoFile('0029_20180306_213505_SimonStevin.all', 'OutputDir', 'D:\Temp\ExData');
% if flag
%     aKM = cl_simrad_all('nomFic', nomFic);
%     [flag, signalContainer] = create_signalsFromSeabedImage(aKM);
%     if flag
%         signalContainer.plot();
%     end
% end

function [flag, signalContainer] = create_signalsFromSeabedImage(this, varargin)

[varargin, DataSeabed] = getPropertyValue(varargin, 'DataSeabed', []);
[varargin, subSeabed]  = getPropertyValue(varargin, 'subSeabed',  []); %#ok<ASGLU>

%% Lecture des datagrammes si pas pr�sents

if isempty(DataSeabed)
    [flag, DataSeabed] = read_seabedImage_bin(this, 'Memmapfile', 1);
    if isempty(DataSeabed)
        return
    end
end

if isempty(subSeabed)
    subSeabed = 1:DataSeabed.Dimensions.nbPings;
end

%% Pr�paration des signaux

TsubSeabed = DataSeabed.Datetime(subSeabed, 1);

%% Cr�ation de l'instance XSample

timeSample = XSample('name', 'time', 'data', TsubSeabed);

%% Initialisation de l'instance ClSignal
signal = ClSignal.empty();

%% SampleFrequency

% TODO : v�rifier l'unit�

X = DataSeabed.SamplingRate(subSeabed,1);
tagName = 'SampleFrequency';
ySampleList = YSample('data', X, 'unit', 'Hz');
signal(end+1) = ClSignal('name', tagName, 'xSample', timeSample, 'ySample', ySampleList, 'tag', tagName);

%% TVGN

% TODO : g�rer l'EM3002D avec link sur signal HeadIndex

tagName = 'TVGN';
X = DataSeabed.TVGN(subSeabed,:);
nbSectors = size(X,2);
if nbSectors == 1
    ySampleList = YSample('data', X, 'unit', 'sample');
else
    ySampleList = YSample.empty();
    for k=1:nbSectors
        sampleName = sprintf('%s, Ind=%d', tagName, k);
        ySampleList(end+1) = YSample('data', X(:,k), 'unit', 'sample'); %#ok<AGROW>
        ySampleList(end).name = sampleName;
    end
end
signal(end+1) = ClSignal('name', tagName, 'xSample', timeSample, 'ySample', ySampleList, 'tag', tagName, 'linkType', 3, 'link', 'TxBeamIndex');

%% TVGNRecomputed

% TODO : g�rer l'EM3002D avec link sur signal HeadIndex

if isfield(DataSeabed, 'TVGNRecomputed')
    tagName = 'TVGNRecomputed';
    X = DataSeabed.TVGNRecomputed(subSeabed,:);
    nbSectors = size(X,2);
    if nbSectors == 1
        ySampleList = YSample('data', X, 'unit', 'sample');
    else
        ySampleList = YSample.empty();
        for k=1:nbSectors
            sampleName = sprintf('%s, Ind=%d', tagName, k);
            ySampleList(end+1) = YSample('data', X(:,k), 'unit', 'sample'); %#ok<AGROW>
            ySampleList(end).name = sampleName;
        end
    end
    signal(end+1) = ClSignal('name', tagName, 'xSample', timeSample, 'ySample', ySampleList, 'tag', tagName, 'linkType', 3, 'link', 'TxBeamIndex');
end

%% BSN

tagName = 'BSN';
if isfield(DataSeabed, tagName)
    X = DataSeabed.BSN(subSeabed,:);
    nbSectors = size(X,2);
    if nbSectors == 1
        ySampleList = YSample('data', X, 'unit', 'dB');
    else
        ySampleList = YSample.empty();
        for k=1:nbSectors
            sampleName = sprintf('%s, Ind=%d', tagName, k);
            ySampleList(end+1) = YSample('data', X(:,k), 'unit', 'dB', 'linkType', 3, 'link', 'TxBeamIndex'); %#ok<AGROW>
            ySampleList(end).name = sampleName;
        end
    end
    signal(end+1) = ClSignal('name', tagName, 'xSample', timeSample, 'ySample', ySampleList, 'tag', tagName);
end

%% BSO

tagName = 'BSO';
if isfield(DataSeabed, tagName)
    X = DataSeabed.BSO(subSeabed,:);
    nbSectors = size(X,2);
    if nbSectors == 1
        ySampleList = YSample('data', X, 'unit', 'dB');
    else
        ySampleList = YSample.empty();
        for k=1:nbSectors
            sampleName = sprintf('%s, Ind=%d', tagName, k);
            ySampleList(end+1) = YSample('data', X(:,k), 'unit', 'dB'); %#ok<AGROW>
            ySampleList(end).name = sampleName;
        end
    end
    signal(end+1) = ClSignal('name', tagName, 'xSample', timeSample, 'ySample', ySampleList, 'tag', tagName, 'linkType', 3, 'link', 'TxBeamIndex');
end

%% BSN-BSO

tagName = 'BSN-BSO';
if isfield(DataSeabed, 'BSN')
    X = DataSeabed.BSN(subSeabed,:) - DataSeabed.BSO(subSeabed,:);
    nbSectors = size(X,2);
    if nbSectors == 1
        ySampleList = YSample('data', X, 'unit', 'dB');
    else
        ySampleList = YSample.empty();
        for k=1:nbSectors
            sampleName = sprintf('%s, Ind=%d', tagName, k);
            ySampleList(end+1) = YSample('data', X(:,k), 'unit', 'dB'); %#ok<AGROW>
            ySampleList(end).name = sampleName;
        end
    end
    signal(end+1) = ClSignal('name', tagName, 'xSample', timeSample, 'ySample', ySampleList, 'tag', tagName, 'linkType', 3, 'link', 'TxBeamIndex');
end

%% TVGCrossOver

tagName = 'TVGCrossOver';
if isfield(DataSeabed, tagName)
    X = DataSeabed.TVGCrossOver(subSeabed,:);
    nbSectors = size(X,2);
    if nbSectors == 1
        ySampleList = YSample('data', X, 'unit', 'deg');
    else
        ySampleList = YSample.empty();
        for k=1:nbSectors
            sampleName = sprintf('%s, Ind=%d', tagName, k);
            ySampleList(end+1) = YSample('data', X(:,k), 'unit', 'deg'); %#ok<AGROW>
            ySampleList(end).name = sampleName;
        end
    end
    signal(end+1) = ClSignal('name', tagName, 'xSample', timeSample, 'ySample', ySampleList, 'tag', tagName, 'linkType', 3, 'link', 'TxBeamIndex');
end

%% ROV_Depth

tagName = 'TxBeamWidth';
if isfield(DataSeabed, tagName)
    X = DataSeabed.TxBeamWidth(subSeabed,:);
    nbSectors = size(X,2);
    if nbSectors == 1
        ySampleList = YSample('data', X, 'unit', 'deg');
    else
        ySampleList = YSample.empty();
        for k=1:nbSectors
            sampleName = sprintf('%s, Ind=%d', tagName, k);
            ySampleList(end+1) = YSample('data', X(:,k), 'unit', 'deg'); %#ok<AGROW>
            ySampleList(end).name = sampleName;
        end
    end
    signal(end+1) = ClSignal('name', tagName, 'xSample', timeSample, 'ySample', ySampleList, 'tag', tagName, 'linkType', 3, 'link', 'TxBeamIndex');
end

%% Create signalContainer

signalContainer = SignalContainer('signalList', signal, 'name', 'signalContainerFromSeabed', 'tag', 'Seabed');

%% The end

flag = 1;
