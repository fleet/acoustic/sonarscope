% [flag, nomFic] = FtpUtils.getSScDemoFile('0029_20180306_213505_SimonStevin.all', 'OutputDir', 'D:\Temp\ExData');
% if flag
%     aKM = cl_simrad_all('nomFic', nomFic);
%     [flag, signalContainer] = create_signalsFromWaterColumn(aKM);
%     if flag
%         signalContainer.plot();
%     end
% end

function [flag, signalContainer] = create_signalsFromWaterColumn(this, varargin)

[varargin, DataWC] = getPropertyValue(varargin, 'DataWC', []);
[varargin, subWC]  = getPropertyValue(varargin, 'subWC',  []); %#ok<ASGLU>

%% Lecture des datagrammes si pas pr�sents

if isempty(DataWC)
    [flag, DataWC] = read_WaterColumn_bin(this, 'Memmapfile', -1);
    if ~flag %isempty(DataWC)
%         flag = 0;
        signalContainer = SignalContainer.empty();
        return
    end
end

if isempty(subWC)
    subWC = 1:DataWC.Dimensions.nbPings;
end

%% Pr�paration des signaux

TsubWC = DataWC.Datetime(subWC, 1);

%% Cr�ation de l'instance XSample

timeSample = XSample('name', 'time', 'data', TsubWC);

%% Initialisation de l'instance ClSignal
signal = ClSignal.empty();

%% SoundSpeed

X = DataWC.SoundSpeed(subWC,1);
tagName = 'WCSoundSpeed';
ySampleList = YSample('data', X, 'unit', 'm/s');
signal(end+1) = ClSignal('name', tagName, 'xSample', timeSample, 'ySample', ySampleList, 'tag', tagName);

%% SampleFrequency

X = DataWC.SamplingFreq(subWC,1);
tagName = 'WCSamplingFreq';
ySampleList = YSample('data', X, 'unit', 'Hz');
signal(end+1) = ClSignal('name', tagName, 'xSample', timeSample, 'ySample', ySampleList, 'tag', tagName);

%% TVGFunctionApplied

X = DataWC.TVGFunctionApplied(subWC,1);
tagName = 'WCTVGFunctionApplied';
ySampleList = YSample('data', X, 'unit', '');
signal(end+1) = ClSignal('name', tagName, 'xSample', timeSample, 'ySample', ySampleList, 'tag', tagName);

%% TVGFunctionApplied

X = DataWC.TVGOffset(subWC,1);
tagName = 'WCTVGOffset';
ySampleList = YSample('data', X, 'unit', 'dB');
signal(end+1) = ClSignal('name', tagName, 'xSample', timeSample, 'ySample', ySampleList, 'tag', tagName);

%% TVGFunctionApplied

X = DataWC.FlagPings(subWC,1);
tagName = 'WCFlagPings';
ySampleList = YSample('data', X, 'unit', '');
signal(end+1) = ClSignal('name', tagName, 'xSample', timeSample, 'ySample', ySampleList, 'tag', tagName);

%% Create signalContainer

signalContainer = SignalContainer('signalList', signal, 'name', 'signalContainerFromWC', 'tag', 'WC');

%% The end

flag = 1;
