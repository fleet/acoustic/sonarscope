function [flag, signalContainerList] = create_signalsModesEM2040(~, T, Mode2, Mode3)

%% Initialisation de l'instance ClSignal

modesSignal = ClSignal.empty();

%% Cr�ation de l'instance XSample

timeSample = XSample('name', 'time', 'data', T);

%% Mode2

tagName = 'Mode2';
X = Mode2;
ySampleList = IndexYSample('data', X, 'unit', '');
ySampleList.strIndexList = {'Normal mode'; 'Single sector'; 'Scanning'};

modesSignal(end+1) = ClSignal('name', tagName, 'xSample', timeSample, 'ySample', ySampleList, 'tag', tagName);

%% Mode3

tagName = 'Mode3';
X = Mode3;
ySampleList = IndexYSample('data', X, 'unit', '');
ySampleList.strIndexList = {'Short'; 'Medium'; 'Long'; 'CW'};

modesSignal(end+1) = ClSignal('name', tagName, 'xSample', timeSample, 'ySample', ySampleList, 'tag', tagName);

%% Create signalContainer

% TODO MHO : est-ce utile de passer par un container ?
signalContainerList = SignalContainer('signalList', modesSignal, 'name', 'Modes', 'tag', 'Modes');

%% The end

flag = 1;
