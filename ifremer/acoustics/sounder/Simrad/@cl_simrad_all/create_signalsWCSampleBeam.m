% [flag, nomFic] = FtpUtils.getSScDemoFile('0029_20180306_213505_SimonStevin.all', 'OutputDir', 'D:\Temp\ExData');
% if flag
%       nomFic = 'G:\Carla\FUGRO\FG-Mob\FG_20200422_181329_EM122.all';
%     aKM = cl_simrad_all('nomFic', nomFic);
%     b = import_WaterColumn_All(aKM, 'subl', 1);
%     SonarScope(b);
%     [flag, signalContainer] = create_signalsWCSampleBeam(b);
%     if flag
%         signalContainer.plot();
%     end
% end

function [flag, signalContainer] = create_signalsWCSampleBeam(~, DataWC, DataAttitude, nbSamples, subWC, varargin)

% [varargin, subWC]        = getPropertyValue(varargin, 'subWC',        []); %#ok<ASGLU>

%% Pr�paration des signaux

twoWayTravelTime = (1:nbSamples) ./ DataWC.SamplingFreq(subWC, 1);
D = duration(0, 0, twoWayTravelTime);
D.Format='hh:mm:ss.SSS';

%% Cr�ation de l'instance XSample

timeSample = XSample('name', 'time', 'data', D); %twoWayTravelTime);

%% Initialisation de l'instance ClSignal
signal = ClSignal.empty();

%% Roll

T = DataWC.Datetime(subWC) + D;
X  = my_interp1(DataAttitude.Datetime(:), DataAttitude.Roll(:), T, 'linear', 'extrap');
% figure; plot(DataAttitude.Datetime(:), DataAttitude.Roll(:), 'b'); hold on; plot(T, X, 'r'); grid on;
% figure; plot(DataAttitude.Roll(:), 'b'); grid on;
% figure; plot(DataAttitude.Datetime(:), 'b.'); grid on;
tagName = 'RollRx';
ySampleList = YSample('data', X, 'unit', 'deg');
signal(end+1) = ClSignal('name', tagName, 'xSample', timeSample, 'ySample', ySampleList, 'tag', tagName);

%% Pitch

T = DataWC.Datetime(subWC) + D;
X  = my_interp1(DataAttitude.Datetime(:), DataAttitude.Pitch(:), T, 'linear', 'extrap');
% figure; plot(T, X); grid on;
tagName = 'PitchRx';
ySampleList = YSample('data', X, 'unit', 'deg');
signal(end+1) = ClSignal('name', tagName, 'xSample', timeSample, 'ySample', ySampleList, 'tag', tagName);

%% TVGFunctionApplied

% X = DataWC.TVGFunctionApplied(subWC,1);
% tagName = 'WCTVGFunctionApplied';
% ySampleList = YSample('data', X, 'unit', '');
% signal(end+1) = ClSignal('name', tagName, 'xSample', timeSample, 'ySample', ySampleList, 'tag', tagName);

%% Create signalContainer

signalContainer = SignalContainer('signalList', signal, 'name', 'signalContainerFromWCSampleBeam', 'tag', 'WC');

%% The end

flag = 1;
