function flag = disableLayerInNetcdfFile(~, nomFic, grpName, varName)

[flag, ncFileName] = existCacheNetcdf(nomFic);
if ~exist(ncFileName, 'file') || ~NetcdfUtils.existVar(ncFileName, grpName, varName)
    return
end
            
ncID  = netcdf.open(ncFileName, 'WRITE');
grpID = netcdf.inqNcid(ncID, grpName);
varID = netcdf.inqVarID(grpID, varName);
netcdf.putAtt(grpID, varID, 'Populated', 0);
netcdf.close(ncID);
  
% https://fr.mathworks.com/matlabcentral/answers/104412-how-can-delete-a-variable-within-a-netcdf-file
% system('ncks -x -v varname in.nc out.nc')
