% Representation externe d'une instance sur le terminal
%
% Syntax
%   display(this)
%
% Input Arguments 
%   this : une instance de cl_simrad_all
%
% Examples
%   nomFic = getNomFicDatabase('EM1002_SHC_0017_20050602_175237.all')
%   aKM = cl_simrad_all('nomFic', nomFic);
%   display(aKM)
%   aKM
%
% See also cl_simrad_all cl_simrad_all/char Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function display(this)

s = whos('this');
sz = s.size;
fprintf('Name\tSize\tClass\n');
fprintf('%s\t%s\t%s\n', inputname(1), num2strCode(sz), s.class);
disp(char(this));
