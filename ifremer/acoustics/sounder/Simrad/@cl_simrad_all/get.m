% Acces en lecture des proprietes d'une instance
%
% Syntax
%   [...] = get(this, ...)
% 
% Input Arguments 
%   this : Une instance de la classe cl_simrad_all
%
% Name-Value Pair Arguments
%   nomFic : Nom du fichier .all
%
% Output Arguments
%   PropertyValues associees aux PropertyNames passes en argument
% 
% Examples
%   nomFic = getNomFicDatabase('EM1002_0001_20040916_054013_raw.all')
%   aKM = cl_simrad_all('nomFic', nomFic)
%   nomFic = get(aKM, 'nomFic')
%   EmModel = get(aKM, 'EmModel')
%   SystemSerialNumber = get(aKM, 'SystemSerialNumber')
%
% See also cl_simrad_all cl_simrad_all/set Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function varargout = get(this, varargin)

varargout = {};
for k=1:length(varargin)
    switch varargin{k}
        case 'nomFic'
            varargout{end+1} = this.nomFic; %#ok<AGROW>
            
        case 'flagRTK'
            varargout{end+1} = this.flagRTK; %#ok<AGROW> TODO
        
        case 'EmModel'
            varargout{end+1} = this.EmModel; %#ok<AGROW>
            
        case 'SystemSerialNumber'
            varargout{end+1} = this.SystemSerialNumber; %#ok<AGROW>
            
        case 'Version'
            varargout{end+1} = this.Version; %#ok<AGROW>
            
        case 'SousVersion'
            varargout{end+1} = this.SousVersion; %#ok<AGROW>
            
        otherwise
            w = sprintf('cl_simrad_all/get %s non pris en compte', varargin{k});
            my_warndlg(w, 0);
    end
end
