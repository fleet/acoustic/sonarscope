function [flag, SonarDescription] = getSonarDescriptionFromData(this, DataDepth, DataRaw, DataRuntime)

[flag, IdentSonar] = SimradModel2IdentSonar(DataDepth.EmModel);
if ~flag
    return
end
if isfield(DataDepth, 'ListeSystemSerialNumber')
    SystemSerialNumber = DataDepth.ListeSystemSerialNumber(1);
else
    SystemSerialNumber = DataDepth.SystemSerialNumber(:,:);
end
[flag, SonarMode_1, SonarMode_2, SonarMode_3] = get_SonarModes(this, DataDepth, DataRaw, DataRuntime);
if ~flag
    return
end
SonarDescription = cl_sounder('Sonar.Ident', IdentSonar, ...
    'Sonar.Mode_1', SonarMode_1, 'Sonar.Mode_2', SonarMode_2, 'Sonar.Mode_3', SonarMode_3, ...
    'Sonar.SystemSerialNumber', SystemSerialNumber);
