function [RollTx, PitchTx, HeaveTx, HeadingTx] = get_AttitudeTx(this, DataAttitude, TimeTx, sectorTransmitDelayYSampleList)

%{
TODO : SensorStatus: [14377�1 uint16]
%}

DatetimeTx = get_DatetimeTx(this, sectorTransmitDelayYSampleList, TimeTx); % TODO : ClSounderAll.get_DatetimeTx : fct Static

% Attitude signals are filered because data is quantified (0.01 degree)
[B,A] = butter(2, 0.1);

if isfield(DataAttitude, 'TrueHeave') ...
        && ~isempty(DataAttitude.TrueHeave) ...
        && ~all(isnan(DataAttitude.TrueHeave)) ...
        && any(DataAttitude.TrueHeave ~= 0)
    Heave = DataAttitude.TrueHeave(:);
else
    Heave = DataAttitude.Heave(:);
end

if isempty(Heave)
    HeaveTx = zeros(size(TimeTx), 'single');
else
    Heave = filtfilt(B, A, double(Heave));
    HeaveTx = my_interp1_MethodTnenExtrap_PreviousThenNext(DataAttitude.Time.timeMat, Heave, DatetimeTx);
end

X = filtfilt(B, A, double(DataAttitude.Roll(:)));
RollTx = my_interp1_MethodTnenExtrap_PreviousThenNext(DataAttitude.Datetime, X, DatetimeTx);

X = filtfilt(B, A, double(DataAttitude.Pitch(:)));
PitchTx = my_interp1_MethodTnenExtrap_PreviousThenNext(DataAttitude.Datetime, X, DatetimeTx);

X = filtfilt(B, A, double(DataAttitude.Heading(:)));
HeadingTx = my_interp1_MethodTnenExtrap_PreviousThenNext(DataAttitude.Datetime, X, DatetimeTx);
