function DatetimeRx = get_DatetimeRx(~, TwoWayTravelTimeInSeconds, TxDelay_PingBeam_s, DatetimeTx)

TwoWayTravelTimeInSeconds = double(TwoWayTravelTimeInSeconds);
TxDelay_PingBeam_s        = double(TxDelay_PingBeam_s);
if size(DatetimeTx, 1) == 1
    DatetimeTx = DatetimeTx';
end
DatetimeRx = bsxfun(@plus, (TwoWayTravelTimeInSeconds + TxDelay_PingBeam_s) / 86400, datenum(DatetimeTx));
DatetimeRx = datetime(DatetimeRx, 'ConvertFrom', 'datenum');
