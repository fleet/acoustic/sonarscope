function DatetimeTx = get_DatetimeTx(~, sectorTransmitDelayYSampleList, TimeTx)

if isempty(sectorTransmitDelayYSampleList)
    DatetimeTx = TimeTx;
    return
end

for k=length(sectorTransmitDelayYSampleList):-1:1
    TxDelay_PingBeam_s(:,k) = sectorTransmitDelayYSampleList(k).data;
end

TxDelay_PingBeam_s = double(TxDelay_PingBeam_s);
if size(TimeTx, 1) == 1
    TimeTx = TimeTx';
end
DatetimeTx = bsxfun(@plus, TxDelay_PingBeam_s / 86400, datenum(TimeTx));
DatetimeTx = datetime(DatetimeTx, 'ConvertFrom', 'datenum');
