function HeadingRx = get_HeadingRx(this, DataAttitude, DatetimeTx, TwoWayTravelTime_s, TxDelay_PingBeam_s)

if isempty(DataAttitude) || isempty(DataAttitude.Heading)
    HeadingRx = zeros(size(TwoWayTravelTime_s), 'single');
    return
end

% Heading is filered because data is quantified (0.01 degree)
ordre = 2;
if length(DataAttitude.Heading) >= (ordre*3 + 1)
    [B,A] = butter(ordre, 0.1);
    Heading = filtfilt(B, A, double(DataAttitude.Heading(:)));
else
    Heading = DataAttitude.Heading(:);
end

DatetimeRx = get_DatetimeRx(this, TwoWayTravelTime_s, TxDelay_PingBeam_s, DatetimeTx); % TODO : UtilsSonars.get_DatetimeTx : fct Static
HeadingRx = my_interp1_headingDeg(DataAttitude.Datetime, Heading,  DatetimeRx);
