function HeaveRx = get_HeaveRx(this, DataAttitude, DatetimeTx, TwoWayTravelTime_s, TxDelay_PingBeam_s)

if isempty(DataAttitude) || isempty(DataAttitude.Heave)
    HeaveRx = zeros(size(TwoWayTravelTime_s), 'single');
    return
end

% Heave is filered because data is quantified (0.01 degree)

Heave = DataAttitude.Heave(:);
ordre = 2;
if length(DataAttitude.Heave) >= (ordre*3 + 1)
    [B,A] = butter(ordre, 0.1);
    Heave = filtfilt(B, A, double(DataAttitude.Heave(:)));
else
    Heave = DataAttitude.Heave(:);
end

DatetimeRx = get_DatetimeRx(this, TwoWayTravelTime_s, TxDelay_PingBeam_s, DatetimeTx); % TODO : ClSounderAll.get_DatetimeTx : fct Static
HeaveRx = my_interp1_MethodTnenExtrap_PreviousThenNext(DataAttitude.Datetime, Heave, DatetimeRx);
