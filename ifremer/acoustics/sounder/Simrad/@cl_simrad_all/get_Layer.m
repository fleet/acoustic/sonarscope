%{
Carto = [];
nomFicAll = 'D:\Demos-3DV\WreckEM710Dz\Data\0006_20090605_211643.all'
allV2 = cl_simrad_all('nomFic', nomFicAll);

[X, Carto] = get_Layer(allV2, 'RxBeamAngle', 'Carto', Carto);
SonarScope(X)
[X, Carto] = get_Layer(allV2, 'ReflectivityFromSnippets', 'Carto', Carto);
SonarScope(X)
[X, Carto] = get_Layer(allV2, 'Depth', 'Carto', Carto);
SonarScope(X)
[X, Carto] = get_Layer(allV2, 'AcrossDist', 'Carto', Carto);
SonarScope(X)
[X, Carto] = get_Layer(allV2, 'AlongDist', 'Carto', Carto);
SonarScope(X)
[X, Carto] = get_Layer(allV2, 'TwoWayTravelTimeInSeconds', 'Carto', Carto);
SonarScope(X)
[X, Carto] = get_Layer(allV2, 'Reflectivity', 'Carto', Carto);
SonarScope(X)
[X, Carto] = get_Layer(allV2, 'Detection', 'Carto', Carto);
SonarScope(X)
[X, Carto] = get_Layer(allV2, 'RayPathSampleNb', 'Carto', Carto);
SonarScope(X)
[X, Carto] = get_Layer(allV2, 'Roll', 'Carto', Carto);
SonarScope(X)
[X, Carto] = get_Layer(allV2, 'Pitch', 'Carto', Carto);
SonarScope(X)
[X, Carto] = get_Layer(allV2, 'Heave', 'Carto', Carto);
SonarScope(X)
[X, Carto] = get_Layer(allV2, 'Heading', 'Carto', Carto);
SonarScope(X)
[X, Carto] = get_Layer(allV2, 'TxBeamIndex', 'Carto', Carto);
SonarScope(X)
[X, Carto] = get_Layer(allV2, 'Depth+Draught-Heave', 'Carto', Carto);
SonarScope(X)
[X, Carto] = get_Layer(allV2, 'RayPathLength', 'Carto', Carto); % One way length in meters
SonarScope(X)
[X, Carto] = get_Layer(allV2, 'TxBeamIndexSwath', 'Carto', Carto);
SonarScope(X)
[X, Carto] = get_Layer(allV2, 'Mask', 'Carto', Carto);
SonarScope(X)
[X, Carto] = get_Layer(allV2, 'KongsbergLengthDetection', 'Carto', Carto);
SonarScope(X)
[X, Carto] = get_Layer(allV2, 'KongsbergQualityFactor', 'Carto', Carto);
SonarScope(X)
[X, Carto] = get_Layer(allV2, 'KongsbergIBA', 'Carto', Carto);
SonarScope(X)
[X, Carto] = get_Layer(allV2, 'KongsbergDetectionInfo', 'Carto', Carto);
SonarScope(X)
[X, Carto] = get_Layer(allV2, 'KongsbergRealTimeCleaningInfo', 'Carto', Carto);
SonarScope(X)
[X, Carto] = get_Layer(allV2, 'NaNFillMethod', 'Carto', Carto);
SonarScope(X)
[X, Carto] = get_Layer(allV2, 'KongsbergTxTiltAngle', 'Carto', Carto);
SonarScope(X)
[X, Carto] = get_Layer(allV2, 'KongsbergFocusRange', 'Carto', Carto);
SonarScope(X)
[X, Carto] = get_Layer(allV2, 'PulseLength', 'Carto', Carto);
SonarScope(X)
[X, Carto] = get_Layer(allV2, 'KongsbergSectorTransmitDelay', 'Carto', Carto);
SonarScope(X)
[X, Carto] = get_Layer(allV2, 'Frequency', 'Carto', Carto);
SonarScope(X)
[X, Carto] = get_Layer(allV2, 'AbsorptionCoeff', 'Carto', Carto);
SonarScope(X)
[X, Carto] = get_Layer(allV2, 'KongsbergWaveformIdentifier', 'Carto', Carto);
SonarScope(X)
[X, Carto] = get_Layer(allV2, 'SignalBandwith', 'Carto', Carto);
SonarScope(X)
[X, Carto] = get_Layer(allV2, 'RxTime', 'Carto', Carto);
SonarScope(X)

[X, Carto] = get_Layer(allV2, 'TwoWayTTAmpDetect', 'Carto', Carto);
SonarScope(X)
[X, Carto] = get_Layer(allV2, 'TwoWayTTPhaseDetect', 'Carto', Carto);
SonarScope(X)
[X, Carto] = get_Layer(allV2, 'AmpDetectQF', 'Carto', Carto);
SonarScope(X)
[X, Carto] = get_Layer(allV2, 'PhaseDetectQF', 'Carto', Carto);
SonarScope(X)
[X, Carto] = get_Layer(allV2, 'AmpDetectStatus', 'Carto', Carto);
SonarScope(X)
[X, Carto] = get_Layer(allV2, 'PhaseDetectStatus', 'Carto', Carto);
SonarScope(X)
[X, Carto] = get_Layer(allV2, 'RawBeamNumber', 'Carto', Carto);
SonarScope(X)
[X, Carto] = get_Layer(allV2, 'RawBeamAngle', 'Carto', Carto);
SonarScope(X)



Carto = [];
nomFicAll = getNomFicDatabase('EM1002_BELGICA_005053_raw.all')
allV1 = cl_simrad_all('nomFic', nomFicAll);

[X, Carto] = get_Layer(allV1, 'Depth', 'Carto', Carto);
SonarScope(X)
[X, Carto] = get_Layer(allV1, 'AcrossDist', 'Carto', Carto);
SonarScope(X)
[X, Carto] = get_Layer(allV1, 'AlongDist', 'Carto', Carto);
SonarScope(X)
[X, Carto] = get_Layer(allV1, 'BeamDepressionAngle', 'Carto', Carto);
SonarScope(X)
[X, Carto] = get_Layer(allV1, 'BeamAzimuthAngle', 'Carto', Carto);
SonarScope(X)
[X, Carto] = get_Layer(allV1, 'RayPathSampleNb', 'Carto', Carto);
SonarScope(X)
[X, Carto] = get_Layer(allV1, 'QualityFactor', 'Carto', Carto);
SonarScope(X)
[X, Carto] = get_Layer(allV1, 'LengthDetection', 'Carto', Carto);
SonarScope(X)
[X, Carto] = get_Layer(allV1, 'Reflectivity', 'Carto', Carto);
SonarScope(X)
[X, Carto] = get_Layer(allV1, 'Detection', 'Carto', Carto);
SonarScope(X)
[X, Carto] = get_Layer(allV1, 'DetecAmplitudeNbSamples', 'Carto', Carto);
SonarScope(X)
[X, Carto] = get_Layer(allV1, 'DetecPhaseOrdrePoly', 'Carto', Carto);
SonarScope(X)
[X, Carto] = get_Layer(allV1, 'DetecPhaseVariance', 'Carto', Carto);
SonarScope(X)
[X, Carto] = get_Layer(allV1, 'Raw-BeamPointingAngle', 'Carto', Carto);
SonarScope(X)
[X, Carto] = get_Layer(allV1, 'Raw-TransmitTiltAngle', 'Carto', Carto);
SonarScope(X)
[X, Carto] = get_Layer(allV1, 'Raw-Range', 'Carto', Carto);
SonarScope(X)
[X, Carto] = get_Layer(allV1, 'Raw-Reflectivity', 'Carto', Carto);
SonarScope(X)
[X, Carto] = get_Layer(allV1, 'RayPathLength', 'Carto', Carto); % One way length in meters
SonarScope(X)
[X, Carto] = get_Layer(allV1, 'RxTime', 'Carto', Carto);
SonarScope(X)
[X, Carto] = get_Layer(allV1, 'Roll', 'Carto', Carto);
SonarScope(X)
[X, Carto] = get_Layer(allV1, 'Pitch', 'Carto', Carto);
SonarScope(X)
[X, Carto] = get_Layer(allV1, 'Heave', 'Carto', Carto);
SonarScope(X)
[X, Carto] = get_Layer(allV1, 'Heading', 'Carto', Carto);
SonarScope(X)
[X, Carto] = get_Layer(allV1, 'Bathymetry', 'Carto', Carto);
SonarScope(X)
[X, Carto] = get_Layer(allV1, 'RayPathTravelTime', 'Carto', Carto);
SonarScope(X)
[X, Carto] = get_Layer(allV1, 'TxBeamIndex', 'Carto', Carto);
SonarScope(X)
[X, Carto] = get_Layer(allV1, 'ReflectivityFromSnippets', 'Carto', Carto);
SonarScope(X)
[X, Carto] = get_Layer(allV1, 'Mask', 'Carto', Carto);
SonarScope(X)
%}

function [a, Carto, OrigineDepthRaw, sublDepth, nbPings] = get_Layer(this, nomLayer, varargin)

[varargin, flagTideCorrection] = getPropertyValue(varargin, 'TideCorrection',  0);
[varargin, Carto]              = getPropertyValue(varargin, 'Carto',           []);
[varargin, CartoAuto]          = getPropertyValue(varargin, 'CartoAuto',       0);
[varargin, Mute]               = getPropertyValue(varargin, 'Mute',            0);
[varargin, IndNavUnit]         = getPropertyValue(varargin, 'IndNavUnit',      []); 

[varargin, DataDepth]          = getPropertyValue(varargin, 'DataDepth',       []);
[varargin, DataRaw]            = getPropertyValue(varargin, 'DataRaw',         []);
[varargin, DataSeabed]         = getPropertyValue(varargin, 'DataSeabed',      []);
[varargin, DataAttitude]       = getPropertyValue(varargin, 'DataAttitude',    []);
[varargin, DataRuntime]        = getPropertyValue(varargin, 'DataRuntime',     []);
[varargin, DataSSP]            = getPropertyValue(varargin, 'DataSSP',         []);
[varargin, DataInstalParam]    = getPropertyValue(varargin, 'DataInstalParam', []);

[varargin, subl]               = getPropertyValue(varargin, 'subl', []);%#ok<ASGLU>

Version = version_datagram(this);
switch Version
    case 'V1'
        ListeLayers = Simrad_getDefaultLayersV1(nomLayer);
        [a, Carto, sublDepth, nbPings, OrigineDepthRaw] = import_PingBeam_All_V1(this, 'ListeLayers', ListeLayers, 'Carto', Carto, ...
            'TideCorrection', flagTideCorrection, 'CartoAuto', CartoAuto, 'Mute', Mute, 'IndNavUnit', IndNavUnit, 'subl', subl);
    case 'V2'
        ListeLayers = Simrad_getDefaultLayersV2(nomLayer);
        [a, Carto, sublDepth, nbPings] = import_PingBeam_All_V2(this, 'ListeLayers', ListeLayers, 'Carto', Carto, ...
            'TideCorrection', flagTideCorrection, 'CartoAuto', CartoAuto, 'Mute', Mute, 'IndNavUnit', IndNavUnit, ...
            'DataDepth', DataDepth, 'DataRaw', DataRaw, 'DataSeabed', DataSeabed, 'DataInstalParam', DataInstalParam, ...
            'DataRuntime', DataRuntime, 'DataSSP', DataSSP, 'DataAttitude', DataAttitude, 'subl', subl);
        OrigineDepthRaw = []; % Pas utile dans ce cas
end
