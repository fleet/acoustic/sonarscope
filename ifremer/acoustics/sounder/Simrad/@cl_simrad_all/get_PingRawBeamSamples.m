function [flag, BeamDataOut] = get_PingRawBeamSamples(this, DataMagPhase, DataDepth, subPingMagPhase, subPingDepth)

flag = existCacheNetcdf(this.nomFic);
if flag
    [flag, BeamDataOut] = get_PingRawBeamSamplesNetcdf(this, DataMagPhase, DataDepth, subPingMagPhase, subPingDepth);
else
    [flag, BeamDataOut] = get_PingRawBeamSamplesXMLBin(this, DataMagPhase, DataDepth, subPingMagPhase, subPingDepth);
end
