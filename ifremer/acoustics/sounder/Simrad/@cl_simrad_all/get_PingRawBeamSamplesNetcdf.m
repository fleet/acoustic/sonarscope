function [flag, BeamDataOut] = get_PingRawBeamSamplesNetcdf(this, DataMagPhase, DataDepth, subPingMagPhase, subPingDepth)

%% Check the number of elements

flag = checkIfOneElement(length(this));
if ~flag
    BeamDataOut = [];
    return
end

%%

[nomDir, nom] = fileparts(this.nomFic);
nomDir = fullfile(nomDir, 'SonarScope', nom);
if ~exist(nomDir, 'dir')
    BeamDataOut = [];
    return
end

N = length(subPingDepth);
str1 = 'Import de l''amplitude 1/2';
str2 = 'Loading Amplitude : 1/2';
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
    if N > 1
        my_waitbar(k, N, hw)
    end
    
    % TODO TODO : pr�voir le cas o� il n'y a pas tous les pings de DataDepth en RawBeamSamples
    if subPingMagPhase(k) > length(DataMagPhase.PingCounter)
        str1 = sprintf('Il n''y a que %d datagrammes de RawBeamSamples dans ce fichier.', length(DataMagPhase.PingCounter));
        str2 = sprintf('Only %d RawBeamSamples datagrams in this file.', length(DataMagPhase.PingCounter));
        my_warndlg(Lang(str1,str2), 1);
        flag = 0;
        BeamDataOut = [];
        return
    end
    
    BeamData.SamplingRate  = DataMagPhase.SamplingFreq(subPingMagPhase(k),:);
    BeamData.SoundSpeed       = DataMagPhase.SoundSpeed(subPingMagPhase(k),:);
    BeamData.CentralFrequency = DataMagPhase.CentralFrequency(subPingMagPhase(k),:);

    BeamData.AcrossDist = DataDepth.AcrossDist(subPingDepth(k),:);
    BeamData.AlongDist  = DataDepth.AlongDist( subPingDepth(k),:);
    BeamData.Depth      = DataDepth.Depth(     subPingDepth(k),:);

    BeamData.iPing = subPingMagPhase(k);
    BeamData.DetectedRangeInSamples = DataMagPhase.DetectedRangeInSamples(subPingMagPhase(k),:);
    BeamData.TransmitSectorNumber   = DataMagPhase.TransmitSectorNumber(subPingMagPhase(k),:);
    
    nbSamples = max(DataMagPhase.StartRangeNumber(subPingMagPhase(k),:) + DataMagPhase.NumberOfSamples(subPingMagPhase(k),:));
    if isnan(nbSamples)
        str1 = sprintf('Probl�me de d�codage de fichier de Water Column ping %d fichier %s.', subPingMagPhase(k), this.nomFic);
        str2 = sprintf('There is an issue while reading Water Column file ping %d on %s', subPingMagPhase(k), this.nomFic);
        my_warndlg(Lang(str1,str2), 0, 'Tag', 'PbLecturePingRawBeamSamples');
        flag = 0;
        BeamDataOut = [];
        return
    end
    nbBeams = DataMagPhase.Dimensions.nbRx;
    BeamData.Samples = NaN(nbSamples, nbBeams, 'single');
    BeamData.Phase   = NaN(nbSamples, nbBeams, 'single');
    idebAmp = 1 + 2 * sum(sum(double(DataMagPhase.NumberOfSamples(1:subPingMagPhase(k)-1,:)), 'omitnan'));
    n = sum(double(DataMagPhase.NumberOfSamples(subPingMagPhase(k),:)), 'omitnan');
    
    Amplitude = DataMagPhase.Amplitude(idebAmp:(idebAmp+n-1));
    Phase = DataMagPhase.Phase(idebAmp:(idebAmp+n-1));
    
    idebAmp2 = 1;
    PingStartRangeNumber = DataMagPhase.StartRangeNumber(subPingMagPhase(k),:);
    PingNumberOfSamples  = DataMagPhase.NumberOfSamples(subPingMagPhase(k),:);
    for iBeam=1:nbBeams
        if ~isnan(PingStartRangeNumber(iBeam))
            sampleDeb = PingStartRangeNumber(iBeam) + 1;
            sampleFin = PingStartRangeNumber(iBeam) + PingNumberOfSamples(iBeam);
            
            subAmpPhase = idebAmp2:(idebAmp2 + double(PingNumberOfSamples(iBeam)) - 1);
            B = Amplitude(subAmpPhase);
            C = Phase(subAmpPhase);
            
            BeamData.Samples(sampleDeb:sampleFin,iBeam) = B * 0.0001;
            BeamData.Phase(sampleDeb:sampleFin,iBeam)   = C * (0.0001 * 180 / pi);
           
            idebAmp2 = idebAmp2+ double(PingNumberOfSamples(iBeam));
        end
    end
    
    % Ajout JMA le 24/02/2020
    sub = (BeamData.Phase > 180);
    BeamData.Phase(sub) = BeamData.Phase(sub) - 360;
    
    BeamDataOut(k) = BeamData; %#ok<AGROW>
end
my_close(hw, 'MsgEnd')
% fclose(fidAmplitude);

flag = 1;

% BeamDataOut.Frequency = 100;
% MultiPingSequence