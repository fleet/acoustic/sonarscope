function [flag, BeamDataOut] = get_PingRawBeamSamplesXMLBin(this, DataMagPhase, DataDepth, subPingMagPhase, subPingDepth)

%% Check the number of elements

flag = checkIfOneElement(length(this));
if ~flag
    BeamDataOut = [];
    return
end

%%

[nomDir, nom] = fileparts(this.nomFic);
nomDir = fullfile(nomDir, 'SonarScope', nom);
if ~exist(nomDir, 'dir')
    BeamDataOut = [];
    return
end

nomFicXml = fullfile(nomDir, 'ALL_RawBeamSamples.xml');
flag = exist(nomFicXml, 'file');
if ~flag
    BeamDataOut = [];
    return
end

Datagrams = xml_mat_read(nomFicXml);

% TODO GLU : c'est ici qu'on vien chercher les donn�es WC dans les "ALL_"

[nomDirSignal, nomFicSignal] = fileparts(nomFicXml);
nomDirSignal = fullfile(nomDirSignal, nomFicSignal);
iTableSampleAmplitude = findIndVariable(Datagrams.Tables, 'SampleAmplitude');
iTableSamplePhase     = findIndVariable(Datagrams.Tables, 'SamplePhase');

% <Methode1------------------------------------------------------------------------------->
% [flag, Amplitude] = readSignal(nomDirSignal, Datagrams.Tables(iTableSampleAmplitude), sum(sum(double(DataMagPhase.NumberOfSamples))), 'Memmapfile', 1);
% </Methode1------------------------------------------------------------------------------->

% <Methode2------------------------------------------------------------------------------->
nomFicAmp = fullfile(nomDirSignal, Datagrams.Tables(iTableSampleAmplitude).FileName);
fidAmplitude = fopen(nomFicAmp, 'r');
if fidAmplitude == -1
    flag = 0;
    BeamDataOut = [];
    return
end

nomFicPhase = fullfile(nomDirSignal, Datagrams.Tables(iTableSamplePhase).FileName);
fidPhase = fopen(nomFicPhase, 'r');
if fidPhase == -1
    flag = 0;
    BeamDataOut = [];
    return
end
% </Methode2------------------------------------------------------------------------------->


N = length(subPingDepth);
% flagVirtualMemory = (N > 10);
str1 = 'Import de l''amplitude 1/2';
str2 = 'Loading Amplitude : 1/2';
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
    if N > 1
        my_waitbar(k, N, hw)
    end
    
    % TODO TODO : pr�voir le cas o� il n'y a pas tous les pings de DataDepth en RawBeamSamples
    if subPingMagPhase(k) > length(DataMagPhase.PingCounter)
        str1 = sprintf('Il n''y a que %d datagrammes de RawBeamSamples dans ce fichier.', length(DataMagPhase.PingCounter));
        str2 = sprintf('Only %d RawBeamSamples datagrams in this file.', length(DataMagPhase.PingCounter));
        my_warndlg(Lang(str1,str2), 1);
        flag = 0;
        BeamDataOut = [];
        return
    end
    
%     BeamData.SamplingRate  = DataDepth.SamplingRate(subPingDepth(k));
    BeamData.SamplingRate  = DataMagPhase.SamplingFreq(subPingMagPhase(k),:);
%     BeamData.SoundSpeed    = DataDepth.SoundSpeed(subPingDepth(k));
    BeamData.SoundSpeed       = DataMagPhase.SoundSpeed(subPingMagPhase(k),:);
    BeamData.CentralFrequency = DataMagPhase.CentralFrequency(subPingMagPhase(k),:);
%     BeamData.SoundVelocity = DataDepth.SoundSpeed(subPingDepth(k)); % Y'en a un de trop

    BeamData.AcrossDist = DataDepth.AcrossDist(subPingDepth(k),:);
    BeamData.AlongDist  = DataDepth.AlongDist( subPingDepth(k),:);
    BeamData.Depth      = DataDepth.Depth(     subPingDepth(k),:);

    BeamData.iPing = subPingMagPhase(k);
    BeamData.DetectedRangeInSamples = DataMagPhase.DetectedRangeInSamples(subPingMagPhase(k),:);
    BeamData.TransmitSectorNumber   = DataMagPhase.TransmitSectorNumber(subPingMagPhase(k),:);
    
    nbSamples = max(DataMagPhase.StartRangeNumber(subPingMagPhase(k),:) + DataMagPhase.NumberOfSamples(subPingMagPhase(k),:));
    if isnan(nbSamples)
        str1 = sprintf('Probl�me de d�codage de fichier de Water Column ping %d fichier %s.', subPingMagPhase(k), this.nomFic);
        str2 = sprintf('There is an issue while reading Water Column file ping %d on %s', subPingMagPhase(k), this.nomFic);
        my_warndlg(Lang(str1,str2), 0, 'Tag', 'PbLecturePingRawBeamSamples');
        flag = 0;
        BeamDataOut = [];
        return
    end
    nbBeams = DataMagPhase.Dimensions.nbRx;
    BeamData.Samples = NaN(nbSamples, nbBeams, 'single');
    BeamData.Phase   = NaN(nbSamples, nbBeams, 'single');
    idebAmp = 1 + 2 * sum(sum(double(DataMagPhase.NumberOfSamples(1:subPingMagPhase(k)-1,:)), 'omitnan'));
    n = sum(double(DataMagPhase.NumberOfSamples(subPingMagPhase(k),:)), 'omitnan');
    
    % <Methode2------------------------------------------------------------------------------->
    flag = fseek(fidAmplitude, idebAmp-1, 'bof');
    if flag ~= 0
        flag = 0;
        BeamDataOut = [];
        return
    end
    Amplitude = fread(fidAmplitude, n, 'uint16=>single');
    
    flag = fseek(fidPhase, idebAmp-1, 'bof');
    if flag ~= 0
        flag = 0;
        BeamDataOut = [];
        return
    end
    Phase = fread(fidPhase, n, 'int16=>single'); % GLU TODO : int16
    
    idebAmp2 = 1;
    
    % </Methode2------------------------------------------------------------------------------->
    
    PingStartRangeNumber = DataMagPhase.StartRangeNumber(subPingMagPhase(k),:);
    PingNumberOfSamples  = DataMagPhase.NumberOfSamples(subPingMagPhase(k),:);
    for iBeam=1:nbBeams
        if ~isnan(PingStartRangeNumber(iBeam))
            sampleDeb = PingStartRangeNumber(iBeam) + 1;
            sampleFin = PingStartRangeNumber(iBeam) + PingNumberOfSamples(iBeam);
            
            % <Methode1------------------------------------------------------------------------------->
            %{
        subAmp = idebAmp:(idebAmp + double(PingNumberOfSamples(iBeam)) - 1);
        AmplBeam = code2val(Amplitude(subAmp), Datagrams.Tables(iTableSampleAmplitude), 'single');
        BeamData.Samples(sampleDeb:sampleFin,iBeam) = AmplBeam;
        idebAmp = idebAmp + double(PingNumberOfSamples(iBeam));
            %}
            % </Methode1------------------------------------------------------------------------------->
            
            % <Methode2------------------------------------------------------------------------------->
            subAmpPhase = idebAmp2:(idebAmp2 + double(PingNumberOfSamples(iBeam)) - 1);
            B = Amplitude(subAmpPhase);
            C = Phase(subAmpPhase);
            
            BeamData.Samples(sampleDeb:sampleFin,iBeam) = B * 0.0001;
            BeamData.Phase(sampleDeb:sampleFin,iBeam)   = C * (0.0001 * 180 / pi);
            
            idebAmp2 = idebAmp2+ double(PingNumberOfSamples(iBeam));
            % </Methode2------------------------------------------------------------------------------->
        end
    end
    BeamDataOut(k) = BeamData; %#ok<AGROW>
end
my_close(hw, 'MsgEnd')
fclose(fidAmplitude);
flag = 1;

% BeamDataOut.Frequency = 100;
% MultiPingSequence