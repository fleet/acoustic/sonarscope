function [flag, BeamDataOut] = get_PingSeabedImageryInWaterColumn(this, ...
    DataSeabed, DataDepth, DataRaw, SeabedImageFormat, subDepth, subRaw, subSeabed)

flag = existCacheNetcdf(this.nomFic);
if flag
    [flag, BeamDataOut] = get_PingSeabedImageryInWaterColumnNetcdf(this, ...
        DataSeabed, DataDepth, DataRaw, SeabedImageFormat, subDepth, subRaw, subSeabed);
else
    [flag, BeamDataOut] = get_PingSeabedImageryInWaterColumnXMLBin(this, ...
        DataSeabed, DataDepth, DataRaw, SeabedImageFormat, subDepth, subRaw, subSeabed);
end
