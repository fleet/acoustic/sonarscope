function [flag, BeamDataOut] = get_PingSeabedImageryInWaterColumnXMLBin(this, ...
    DataSeabed, DataDepth, DataRaw, SeabedImageFormat, subDepth, subRaw, subSeabed)

%% Check the number of elements

flag = checkIfOneElement(length(this));
if ~flag
    BeamDataOut = [];
    return
end

%%

[nomDir, nom] = fileparts(this.nomFic);
nomDir = fullfile(nomDir, 'SonarScope', nom);
if ~exist(nomDir, 'dir')
    return
end

if SeabedImageFormat == 1
    nomFicXml = fullfile(nomDir, 'ALL_SeabedImage53h.xml');
else
    nomFicXml = fullfile(nomDir, 'ALL_SeabedImage59h.xml');
end

Datagrams = xml_mat_read(nomFicXml);

% TODO GLU : c'est ici qu'on vient lire les donn�es d'imagerie

[nomDirSignal, nomFicSignal] = fileparts(nomFicXml);
nomDirSignal = fullfile(nomDirSignal, nomFicSignal);
iTableSampleAmplitude = findIndVariable(Datagrams.Tables, 'Entries');
nomFicAmp = fullfile(nomDirSignal, Datagrams.Tables(iTableSampleAmplitude).FileName);
fid = fopen(nomFicAmp, 'r');
if fid == -1
    flag = 0;
    return
end

N = length(subDepth);
hw = create_waitbar('Loading Amplitude : 1/2', 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    iPingDepth  = subDepth(k);
    iPingSeabed = subSeabed(k);
    
    if SeabedImageFormat == 1
        Range = floor(DataDepth.Range(iPingDepth,:));
    else
        iPingRaw = subRaw(k);
        Range = floor(DataRaw.TwoWayTravelTimeInSeconds(iPingRaw,:) * DataSeabed.SamplingRate(iPingSeabed));
        
        %{
% Modif � faire pour fichier EM2040 Dual (0011_20110607_134644_ShipName) que Berit m'a transmis
if DataRaw.SamplingRate(iPingRaw) > 32669 %
Range = floor(Range/4);
end
        %}
    end
    nbBeams = DataSeabed.Dimensions.nbBeams;
    
    % Rustine pour corriger le manque d'un beam fichier C:\Temp\UNH\2009data\0012_20090403_030916_ShipName.all
    if size(Range,2) ~= nbBeams
        nbBeams = size(Range,2);
        DataSeabed.InfoBeamNbSamples     = DataSeabed.InfoBeamNbSamples(:,1:nbBeams);
        DataSeabed.InfoBeamSortDirection = DataSeabed.InfoBeamSortDirection(:,1:nbBeams);
        DataSeabed.InfoBeamCentralSample = DataSeabed.InfoBeamCentralSample(:,1:nbBeams);
        DataSeabed.InfoBeamDetectionInfo = DataSeabed.InfoBeamDetectionInfo(:,1:nbBeams);
    end
    
    BeamData.SamplingRate  = DataDepth.SamplingRate(iPingDepth);
    BeamData.SoundSpeed    = DataDepth.SoundSpeed(iPingDepth);
    BeamData.SoundVelocity = DataDepth.SoundSpeed(iPingDepth); % Y'en a un de trop
    BeamData.iPing = iPingDepth;
    BeamData.DetectedRangeInSamples = Range;
    if SeabedImageFormat == 1
        BeamData.TransmitSectorNumber = ones(size(BeamData.DetectedRangeInSamples)); %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% DataSeabed.TransmitSectorNumber(iPingSeabed,:);
    else
        BeamData.TransmitSectorNumber = DataRaw.TransmitSectorNumber(iPingRaw,:);
    end
    
    nbSamples = max(Range + DataSeabed.InfoBeamNbSamples(iPingSeabed,:)); % Pourrait �tre am�lior� avec InfoBeamCentralSample
    
    idebAmp = sum(sum(double(DataSeabed.InfoBeamNbSamples(1:iPingSeabed-1,:)), 'omitnan')); % Modif JMA le 18/03/2021
    n = sum(double(DataSeabed.InfoBeamNbSamples(iPingSeabed,:)), 'omitnan');
    
    if SeabedImageFormat == 1
        flag = fseek(fid, idebAmp, 'bof');
        if flag ~= 0
            flag = 0;
            return
        end
        Entries = fread(fid, n, 'int8=>single') * 0.5;
    else
        flag = fseek(fid, idebAmp*2, 'bof');
        if flag ~= 0
            flag = 0;
            return
        end
        Entries = fread(fid, n, 'int16=>single') * 0.1;
    end
    
    BeamData.Samples           = NaN(nbSamples, nbBeams, 'single');
    BeamData.BeamSortDirection = zeros(nbSamples, nbBeams, 'uint8');
    subEntries = 0;
    for iBeam=1:nbBeams
        nSamples = DataSeabed.InfoBeamNbSamples(iPingSeabed,iBeam);
        if isnan(nSamples)
            continue
        end
        subEntries = subEntries(end) + (1:nSamples);
        if isnan(Range(iBeam))
            continue
        end
        
        if DataSeabed.InfoBeamSortDirection(iPingSeabed,iBeam) == -1
            subSampleBeam = (Range(iBeam) - (DataSeabed.InfoBeamNbSamples(iPingSeabed,iBeam) - DataSeabed.InfoBeamCentralSample(iPingSeabed,iBeam))) + (1:length(subEntries));
        else
            subSampleBeam = (Range(iBeam) - DataSeabed.InfoBeamCentralSample(iPingSeabed,iBeam) + 0) + (1:length(subEntries));
        end
        
        if DataSeabed.InfoBeamSortDirection(iPingSeabed,iBeam) ~= 1
            subSampleBeam = fliplr(subSampleBeam);
        end
        % figure(7777); plot(DataSeabed.InfoBeamSortDirection(iPingSeabed,:), '*');
        BeamData.Samples(subSampleBeam,iBeam)           = Entries(subEntries);
        BeamData.BeamSortDirection(subSampleBeam,iBeam) = 1+(DataSeabed.InfoBeamSortDirection(iPingSeabed,iBeam) == 1);
    end
    % SonarScope(BeamData.Samples)
    BeamDataOut(k) = BeamData; %#ok<AGROW>
end
my_close(hw)
fclose(fid);
flag = 1;
