function [flag, StaveDataOut] = get_PingStaveData(this, DataStave, varargin)

% TODO : A terminer en collaboration avec JMA.

[varargin, subStave] = getPropertyValue(varargin, 'subStave', 1); %#ok<ASGLU>

%% Check the number of elements

flag = checkIfOneElement(length(this));
if ~flag
    StaveDataOut = [];
    return
end

%%

[nomDir, nom] = fileparts(this.nomFic);
nomDir = fullfile(nomDir, 'SonarScope', nom);
if ~exist(nomDir, 'dir')
    StaveDataOut = [];
    return
end
nomFicXml = fullfile(nomDir, 'ALLStaveData.xml');
flag = exist(nomFicXml, 'file');
if ~flag
    StaveDataOut = [];
    return
end

% TODO GLU : c'est ici qu'on vient lire les donn�es d'imagerie

Datagrams = xml_mat_read(nomFicXml);
[nomDirSignal, nomFicSignal] = fileparts(nomFicXml);
nomDirSignal = fullfile(nomDirSignal, nomFicSignal);
iTableStaveBackscatter = findIndVariable(Datagrams.Tables, 'StaveBackscatter');

% <Methode1------------------------------------------------------------------------------->
% [flag, Amplitude] = readSignal(nomDirSignal, Datagrams.Tables(iTableSampleAmplitude), sum(sum(double(DataStave.NumberOfSamples))), 'Memmapfile', 1);
% </Methode1------------------------------------------------------------------------------->

% <Methode2------------------------------------------------------------------------------->
nomFicAmp = fullfile(nomDirSignal, Datagrams.Tables(iTableStaveBackscatter).FileName);
fid = fopen(nomFicAmp, 'r');
if fid == -1
    flag = 0;
    StaveDataOut = [];
    return
end
% </Methode2------------------------------------------------------------------------------->


N = length(subStave);
% flagVirtualMemory = (N > 10);
hw = create_waitbar('Loading Amplitude : 1/2', 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    
    iPingStave    = subStave(k);
    
    % TODO TODO : pr�voir le cas o� il n'y a pas tous les pings de DataDepth en WaterColumn
    if iPingStave > length(DataStave.PingCounter)
        str1 = sprintf('Il n''y a que %d datagrammes de Stave dans ce fichier.', length(DataStave.PingCounter));
        str2 = sprintf('Only %d WaterColumn datagrams in this file.', length(DataStave.PingCounter));
        my_warndlg(Lang(str1,str2), 1);
        flag = 0;
        StaveDataOut = [];
        return
    end
    
    nbSamples = max(DataStave.StartSampleRef1TxPulse(iPingStave,:) + DataStave.NumberOfSamples(iPingStave,:));
    if isnan(nbSamples)
        str1 = sprintf('Probl�me de d�codage de fichier de Water Column ping %d fichier %s.', iPingStave, this.nomFic);
        str2 = sprintf('There is an issue while reading Water Column file ping %d on %s', iPingStave, this.nomFic);
        my_warndlg(Lang(str1,str2), 0, 'Tag', 'PbLecturePingWaterColumn');
        flag = 0;
        StaveDataOut = [];
        return
    end
    nbStaves = DataStave.Dimensions.nbStaves;
    StaveData.Samples = NaN(nbSamples, nbStaves, 'single');
    idebAmp = 1 + sum(sum(double(DataStave.NumberOfSamples(1:iPingStave-1,:)), 'omitnan'));
    n = sum(double(DataStave.NumberOfSamples(iPingStave,:)), 'omitnan');
    
    % <Methode2------------------------------------------------------------------------------->
    flag = fseek(fid, idebAmp-1, 'bof');
    if flag ~= 0
        flag = 0;
        StaveDataOut = [];
        return
    end
    A = fread(fid, n, 'int8=>single');
    idebAmp2 = 1;
    % </Methode2------------------------------------------------------------------------------->
    
    PingStartRangeNumber = DataStave.StartSampleRef1TxPulse(iPingStave,:);
    PingNumberOfSamples  = DataStave.NumberOfSamples(iPingStave,:);
    
    if isfield(DataStave, 'OrdreStave')
        [~, iStaveSort] = sort(DataStave.OrdreStave(iPingStave, :));
    else
        iStaveSort = 1:DataStave.Dimensions.nbRx;
    end
    
    %     for iStave=1:nbStaves
    for iStaveTab=1:nbStaves
        iStave = iStaveSort(iStaveTab);
        if ~isnan(PingStartRangeNumber(iStave))
            sampleDeb = PingStartRangeNumber(iStave) + 1;
            sampleFin = PingStartRangeNumber(iStave) + PingNumberOfSamples(iStave);
            
            
            B = A(idebAmp2:(idebAmp2 + double(PingNumberOfSamples(iStave)) - 1));
            
            StaveData.Samples(sampleDeb:sampleFin,iStave) = B;
            idebAmp2 = idebAmp2+ double(PingNumberOfSamples(iStave));
        end
    end
    StaveDataOut(k) = StaveData; %#ok<AGROW>
    
    %% Sauvegarde de la structure dans un fichier .mat
    
    if ~isdeployed
        iPingStaveDir = floor(iPingStave / 100);
        nomDir2 = fullfile(nomDir, 'SSc_WaterColumn', num2str(iPingStaveDir, '%03d'));
        if ~exist(nomDir2, 'dir')
            status = mkdir(nomDir2);
            if ~status
                messageErreur(nomDir2)
                flag = 0;
                return
            end
        end
        nomFic2 = fullfile(nomDir2, num2str(iPingStave, '%05d.mat'));
        save(nomFic2, 'StaveData')
    end
end
my_close(hw)

fclose(fid);
flag = 1;
