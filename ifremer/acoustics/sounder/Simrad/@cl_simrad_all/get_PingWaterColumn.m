function [flag, BeamDataOut] = get_PingWaterColumn(this, DataWC, DataDepth, DataRaw, DataAttitude, DataInstalParam, isDual, varargin)

flag = existCacheNetcdf(this.nomFic);
if flag
    [flag, BeamDataOut] = get_PingWaterColumnNetcdf(this, DataWC, DataDepth, DataRaw, ...
        DataAttitude, DataInstalParam, isDual, varargin{:});
else
    [flag, BeamDataOut] = get_PingWaterColumnXMLBin(this, DataWC, DataDepth, DataRaw, ...
        DataAttitude, DataInstalParam, isDual, varargin{:});
end
