function [flag, BeamDataOut] = get_PingWaterColumnNetcdf(this, DataWC, DataDepth, DataRaw, DataAttitude, DataInstallationParameters, isDual, varargin)

[varargin, MaskFlaggedPings]    = getPropertyValue(varargin, 'MaskFlaggedPings',    0);
[varargin, subWC]               = getPropertyValue(varargin, 'subWC',               1);
[varargin, subDepth]            = getPropertyValue(varargin, 'subDepth',            1);
[varargin, subRaw]              = getPropertyValue(varargin, 'subRaw',              1);
[varargin, maskOverlapingBeams] = getPropertyValue(varargin, 'maskOverlapingBeams', 0); %#ok<ASGLU>

%% Check the number of elements

flag = checkIfOneElement(length(this));
if ~flag
    BeamDataOut = [];
    return
end
    
%% Loop on pings

N = length(subWC);
hw = create_waitbar('Loading Amplitude : 1/2', 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    
    iPingWC    = subWC(k);
    iPingDepth = subDepth(k);
    iPingRaw   = subRaw(k);
    
    % TODO TODO : pr�voir le cas o� il n'y a pas tous les pings de DataDepth en WaterColumn
    if iPingWC > length(DataWC.PingCounter)
        str1 = sprintf('Il n''y a que %d datagrammes de WaterColumn dans ce fichier.', length(DataWC.PingCounter));
        str2 = sprintf('Only %d WaterColumn datagrams in this file.', length(DataWC.PingCounter));
        my_warndlg(Lang(str1,str2), 1);
        flag = 0;
        BeamDataOut = [];
        return
    end
    
    BeamData.SamplingRate    = DataWC.SamplingFreq(iPingWC);
    BeamData.SoundSpeed      = DataDepth.SoundSpeed(iPingDepth);
    BeamData.SoundVelocity   = DataDepth.SoundSpeed(iPingDepth); % Y'en a un de trop
    BeamData.TransducerDepth = DataDepth.TransducerDepth(iPingDepth,:);
    BeamData.AcrossDist      = DataDepth.AcrossDist(iPingDepth,:);
    BeamData.AlongDist       = DataDepth.AlongDist(iPingDepth,:);
    BeamData.Depth           = DataDepth.Depth(iPingDepth,:);
    
    if isfield(DataDepth, 'WCSignalWidth')
        BeamData.WCSignalWidth = DataDepth.WCSignalWidth(iPingDepth,:);
    else
        BeamData.WCSignalWidth = [];
    end
    
    BeamData.BeamMask = DataDepth.Mask(iPingDepth,:); % Ajout JMA le 17/02/2020 pour prise en compte nettoyage de la bathy
        
    Datetime = DataDepth.Datetime(subDepth);
    if isfield(DataRaw, 'TwoWayTravelTimeInSeconds')
        TwoWayTravelTime_s = DataRaw.TwoWayTravelTimeInSeconds(subRaw,:);
    elseif isfield(DataRaw, 'Range')
        resolution = 1500 / (2 * DataDepth.SamplingRate(iPingDepth)); % TODO : 1500 !!!
        TwoWayTravelTime_s = DataRaw.Range(subRaw,:) .* resolution;
    end
    TxDelay_PingBeam_s = 1e-3 * get_PingBeamMatrixFromPingSignal(DataRaw.SectorTransmitDelay(subRaw,:), DataRaw.TransmitSectorNumber(subRaw,:));

    RollRx = get_RollRx(this, DataAttitude, Datetime, TwoWayTravelTime_s, TxDelay_PingBeam_s);
    I = DataRaw.BeamPointingAngle(subRaw,:);
    [RxAnglesHullBathy, maskOverlap] = Simrad_AnglesAntenna2ShipFloor(I, DataInstallationParameters, isDual);
%     if isdeployed
%         BeamData.BeamPointingAngle = RxAnglesHullBathy(k,:) - RollRx(k,:); % this name should be removed because not enough explicit
%     end
    BeamData.RxAnglesHullBathy  = RxAnglesHullBathy(k,:);
    BeamData.RxAnglesEarthBathy = RxAnglesHullBathy(k,:) - RollRx(k,:);
    
    BeamData.iPing                  = iPingWC;
    BeamData.DetectedRangeInSamples = DataWC.DetectedRangeInSamples(iPingWC,:);
    BeamData.TransmitSectorNumber   = DataWC.TransmitSectorNumber(iPingWC,:);
    BeamData.TVGFunctionApplied     = DataWC.TVGFunctionApplied(iPingWC);
    
    if DataRaw.PingSequence(iPingRaw) == 1
        BeamData.TxBeamIndexSwath = BeamData.TransmitSectorNumber;
    else
        BeamData.TxBeamIndexSwath = BeamData.TransmitSectorNumber + max(BeamData.TransmitSectorNumber); % Peut-�tre pas bon si il manque des secteurs ?
    end
    
    if isfield(DataRaw, 'SectorTransmitDelay')
        TxDelay = DataRaw.SectorTransmitDelay(iPingRaw,:);
        if isscalar(TxDelay) % TODO : faire le point sur ce pb : �a bugue si EM2040 de SimonStevins
            BeamData.SectorTransmitDelay = TxDelay;
        else
            try
                BeamData.SectorTransmitDelay = TxDelay(BeamData.TransmitSectorNumber);
            catch
                BeamData.SectorTransmitDelay = TxDelay(1);
            end
        end
    end
    
    nbSamples = max(DataWC.StartRangeNumber(iPingWC,:) + DataWC.NumberOfSamples(iPingWC,:));
    if isnan(nbSamples)
        str1 = sprintf('Probl�me de d�codage de fichier de Water Column ping %d fichier %s.', iPingWC, this.nomFic);
        str2 = sprintf('There is an issue while reading Water Column file ping %d on %s',               iPingWC, this.nomFic);
        my_warndlg(Lang(str1,str2), 0, 'Tag', 'PbLecturePingWaterColumn');
        flag = 0;
        BeamDataOut = [];
        return
    end
    
    [flag, A] = ALL_getPingWCSampleBeamBufferNetcdf(DataWC, iPingWC);
    if ~flag
        str1 = sprintf('Erreur lecture WC iPing=%s fichier %s', iPingWC, this.nomFic);
        str2 = sprintf('Error reading WC iPing=%d file %s',     iPingWC, this.nomFic);
        my_warndlg(Lang(str1,str2), 0);
        flag = 0;
        BeamDataOut = [];
        return
    end

    [~, ImageWCSampleBeam] = ALL_getPingWCSampleBeam(DataWC, iPingWC, A);
    if maskOverlapingBeams
        ImageWCSampleBeam(:,maskOverlap) = NaN;
    end
    
    BeamData.Samples = ImageWCSampleBeam;
        
%     figure; plot(BeamData.DetectedRangeInSamples); grid on
%     figure; plot(BeamData.TransmitSectorNumber); grid on
%     figure; plot(BeamData.TxBeamIndexSwath); grid on
%     figure; plot(BeamData.SectorTransmitDelay); grid on
    
    BeamDataOut(k) = BeamData; %#ok<AGROW>
end
my_close(hw)

flag = 1;
