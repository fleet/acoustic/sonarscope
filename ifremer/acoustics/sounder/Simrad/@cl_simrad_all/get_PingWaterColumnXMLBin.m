function [flag, BeamDataOut] = get_PingWaterColumnXMLBin(this, DataWC, DataDepth, DataRaw, DataAttitude, DataInstallationParameters, isDual, varargin)

[varargin, MaskFlaggedPings]    = getPropertyValue(varargin, 'MaskFlaggedPings',    0);
[varargin, subWC]               = getPropertyValue(varargin, 'subWC',               1);
[varargin, subDepth]            = getPropertyValue(varargin, 'subDepth',            1);
[varargin, subRaw]              = getPropertyValue(varargin, 'subRaw',              1);
[varargin, maskOverlapingBeams] = getPropertyValue(varargin, 'maskOverlapingBeams', 0);
[varargin, Datagrams]           = getPropertyValue(varargin, 'XML_ALL_WaterColumn', []); %#ok<ASGLU>

TestFichiersSepares = 0;

%% Check the number of elements

flag = checkIfOneElement(length(this));
if ~flag
    BeamDataOut = [];
    return
end

%%

[flag, fid] = ALL_getFidWCSampleBeamBufferXMLBin(this.nomFic, 'XML_ALL_WaterColumn', Datagrams);
if ~flag
    BeamDataOut = [];
    return
end
    
%% Loop on pings

N = length(subWC);
hw = create_waitbar('Loading Amplitude : 1/2', 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    
    iPingWC    = subWC(k);
    iPingDepth = subDepth(k);
    iPingRaw   = subRaw(k);
    
    % TODO TODO : pr�voir le cas o� il n'y a pas tous les pings de DataDepth en WaterColumn
    if iPingWC > length(DataWC.PingCounter)
        str1 = sprintf('Il n''y a que %d datagrammes de WaterColumn dans ce fichier.', length(DataWC.PingCounter));
        str2 = sprintf('Only %d WaterColumn datagrams in this file.', length(DataWC.PingCounter));
        my_warndlg(Lang(str1,str2), 1);
        flag = 0;
        BeamDataOut = [];
        return
    end
    
    BeamData.SamplingRate    = DataWC.SamplingFreq(iPingWC);
    BeamData.SoundSpeed      = DataDepth.SoundSpeed(iPingDepth);
    BeamData.SoundVelocity   = DataDepth.SoundSpeed(iPingDepth); % Y'en a un de trop
    BeamData.TransducerDepth = DataDepth.TransducerDepth(iPingDepth,:);
    BeamData.AcrossDist      = DataDepth.AcrossDist(iPingDepth,:);
    BeamData.AlongDist       = DataDepth.AlongDist(iPingDepth,:);
    BeamData.Depth           = DataDepth.Depth(iPingDepth,:);
    
    BeamData.BeamMask        = DataDepth.Mask(iPingDepth,:); % Ajout JMA le 17/02/2020 pour prise en compte nettoyage de la bathy
        
    Datetime = DataDepth.Datetime(subDepth);
    if isfield(DataRaw, 'TwoWayTravelTimeInSeconds')
        TwoWayTravelTime_s = DataRaw.TwoWayTravelTimeInSeconds(subRaw,:);
    elseif isfield(DataRaw, 'Range')
        resolution = 1500 / (2 * DataDepth.SamplingRate(iPingDepth)); % TODO : 1500 !!!
        TwoWayTravelTime_s = DataRaw.Range(subRaw,:) .* resolution;
    end
    TxDelay_PingBeam_s = 1e-3 * get_PingBeamMatrixFromPingSignal(DataRaw.SectorTransmitDelay(subRaw,:), DataRaw.TransmitSectorNumber(subRaw,:));

    RollRx = get_RollRx(this, DataAttitude, Datetime, TwoWayTravelTime_s, TxDelay_PingBeam_s);
    I = DataRaw.BeamPointingAngle(subRaw,:);
    [RxAnglesHullBathy, maskOverlap] = Simrad_AnglesAntenna2ShipFloor(I, DataInstallationParameters, isDual);
%     if isdeployed
%         BeamData.BeamPointingAngle = RxAnglesHullBathy(k,:) - RollRx(k,:); % this name should be removed because not enough explicit
%     end
    BeamData.RxAnglesHullBathy  = RxAnglesHullBathy(k,:);
    BeamData.RxAnglesEarthBathy = RxAnglesHullBathy(k,:) - RollRx(k,:);
    
    BeamData.iPing                  = iPingWC;
    BeamData.DetectedRangeInSamples = DataWC.DetectedRangeInSamples(iPingWC,:);
    BeamData.TransmitSectorNumber   = DataWC.TransmitSectorNumber(iPingWC,:);
    BeamData.TVGFunctionApplied     = DataWC.TVGFunctionApplied(iPingWC);
    
    if DataRaw.PingSequence(iPingRaw) == 1
        BeamData.TxBeamIndexSwath = BeamData.TransmitSectorNumber;
    else
        BeamData.TxBeamIndexSwath = BeamData.TransmitSectorNumber + max(BeamData.TransmitSectorNumber); % Peut-�tre pas bon si il manque des secteur ?
    end
    
    if isfield(DataRaw, 'SectorTransmitDelay')
        TxDelay = DataRaw.SectorTransmitDelay(iPingRaw,:);
        if isscalar(TxDelay) % TODO : faire le point sur ce pb : �a bugue si EM2040 de SimonStevins
            BeamData.SectorTransmitDelay = TxDelay;
        else
            try
                BeamData.SectorTransmitDelay = TxDelay(BeamData.TransmitSectorNumber);
            catch
                BeamData.SectorTransmitDelay = TxDelay(1);
            end
        end
    end
    
    nbSamples = max(DataWC.StartRangeNumber(iPingWC,:) + DataWC.NumberOfSamples(iPingWC,:));
    if isnan(nbSamples)
        str1 = sprintf('Probl�me de d�codage de fichier de Water Column ping %d fichier %s.', iPingWC, this.nomFic);
        str2 = sprintf('There is an issue while reading Water Column file ping %d on %s', iPingWC, this.nomFic);
        my_warndlg(Lang(str1,str2), 0, 'Tag', 'PbLecturePingWaterColumn');
        flag = 0;
        BeamDataOut = [];
        return
    end
  
    [flag, A] = ALL_getPingWCSampleBeamBufferXMLBin(DataWC, iPingWC, fid);
    if ~flag
        flag = 0;
        BeamDataOut = [];
        return
    end
    [~, ImageWCSampleBeam] = ALL_getPingWCSampleBeam(DataWC, iPingWC, A);
    if maskOverlapingBeams
        ImageWCSampleBeam(:,maskOverlap) = NaN;
    end

    BeamData.Samples = ImageWCSampleBeam;
%     figure; plot(BeamData.DetectedRangeInSamples); grid on
%     figure; plot(BeamData.TransmitSectorNumber); grid on
%     figure; plot(BeamData.TxBeamIndexSwath); grid on
%     figure; plot(BeamData.SectorTransmitDelay); grid on
    
    BeamDataOut(k) = BeamData; %#ok<AGROW>
    
    %% Sauvegarde de la structure dans un fichier .mat
    
    if ~isdeployed && TestFichiersSepares
%         iPingWCMod = mod(iPingWC, 100);
        iPingWCDir = floor(iPingWC / 100);
        nomDir2 = fullfile(nomDir, 'SSc_WaterColumn', num2str(iPingWCDir, '%03d'));
        if ~exist(nomDir2, 'dir')
            status = mkdir(nomDir2);
            if ~status
                messageErreur(nomDir2)
                flag = 0;
                return
            end
        end
        nomFic2 = fullfile(nomDir2, num2str(iPingWC, '%05d.mat'));
        save(nomFic2, 'BeamData')
    end
end
my_close(hw)

fclose(fid);
flag = 1;
