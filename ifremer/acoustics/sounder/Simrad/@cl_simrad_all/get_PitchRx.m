function PitchRx = get_PitchRx(this, DataAttitude, DatetimeTx, TwoWayTravelTime_s, TxDelay_PingBeam_s)

if isempty(DataAttitude) || isempty(DataAttitude.Pitch)
    PitchRx = zeros(size(TwoWayTravelTime_s));
    return
end

% Pitch is filered because data is quantified (0.01 degree)
ordre = 2;
if length(DataAttitude.Pitch) >= (ordre*3 + 1)
    [B,A] = butter(ordre, 0.1);
    Pitch = filtfilt(B, A, double(DataAttitude.Pitch(:)));
else
    Pitch = DataAttitude.Pitch(:);
end

DatetimeRx = get_DatetimeRx(this, TwoWayTravelTime_s, TxDelay_PingBeam_s, DatetimeTx); % TODO : ClSounderAll.get_DatetimeTx : fct Static
PitchRx = my_interp1_MethodTnenExtrap_PreviousThenNext(DataAttitude.Datetime, Pitch, DatetimeRx);
