function RollRx = get_RollRx(this, DataAttitude, DatetimeTx, TwoWayTravelTime_s, TxDelay_PingBeam_s)

if isempty(DataAttitude) || isempty(DataAttitude.Roll)
    RollRx = zeros(size(TwoWayTravelTime_s));
    return
end

% Roll is filered because data is quantified (0.01 degree)
ordre = 2;
if length(DataAttitude.Roll) >= (ordre*3 + 1)
    [B,A] = butter(ordre, 0.1);
    Roll = filtfilt(B, A, double(DataAttitude.Roll(:)));
else
    Roll = DataAttitude.Roll(:);
end

DatetimeRx = get_DatetimeRx(this, TwoWayTravelTime_s, TxDelay_PingBeam_s, DatetimeTx); % TODO : ClSounderAll.get_DatetimeTx : fct Static
RollRx = my_interp1_MethodTnenExtrap_PreviousThenNext(DataAttitude.Datetime, Roll, DatetimeRx);
