function [flag, SonarMode_1, SonarMode_2, SonarMode_3] = get_SonarModes(this, DataDepth, DataRaw, DataRuntime, varargin)

[varargin, isDual] = getPropertyValue(varargin,  'isDual', 0); %#ok<ASGLU>

SonarMode_1 = [];
SonarMode_2 = [];
SonarMode_3 = [];

Mode = my_interp1_Extrap_PreviousThenNext(DataRuntime.Datetime, DataRuntime.Mode, DataDepth.Datetime);
% SounderMode = floor(my_median(Mode(Mode ~= 0)));
SounderMode = mode(Mode(Mode ~= 0)); % Modif JMA le 21/11/2017

[flag, nbSwaths] = create_signal_nbSwath(this, 'DataRaw', DataRaw);
if ~flag
    return
end
nbSwaths = floor(my_median(nbSwaths.Data));

[flag, SignalPresenceFM] = create_signal_presenceFM(this, 'DataRaw', DataRaw);
if ~flag
    return
end

flagPresenceFM = my_interp1_Extrap_PreviousThenNext(SignalPresenceFM.Datetime, SignalPresenceFM.Data, DataDepth.Datetime);

presenceFM = floor(my_median(SignalPresenceFM.Data(~isnan(SignalPresenceFM.Data)))); % Modif GLT : cas de deux pings � NaN
EmModel    = DataDepth.EmModel;
Model      = sprintf('EM%d', EmModel);

if isfield(DataDepth, 'ScanningInfo')
    ScanningInfo = floor(my_median(DataDepth.ScanningInfo(:)));
else
    ScanningInfo = 0; % Fichiers d�cod�s avant le 03/02/2017
end

switch Model
    case {'EM2040'; 'EM2040S'; 'EM2040D'}
        nbSectors = max(DataRaw.TransmitSectorNumberTx(:));
    otherwise
        nbSectors = [];
end

if strcmp(Model, 'EM304')
    subDeeper = (Mode == 4) & (flagPresenceFM == 1);
    Mode(subDeeper) = 5;
    SounderMode = mode(Mode(Mode ~= 0)); % Modif JMA le 21/11/2017
end

% SignalLength = mode(DataRaw.SignalLength(:));
SignalLength = mode(DataRuntime.TransmitPulseLength(:)) * 1e-3; % Modif JMA le 23/01/2019
PulseLengthConfIfEM2040 = mode(1 + decodeBits(uint8(DataRuntime.ReceiverFixGain), [4 3]));
[flag, SonarMode_1, SonarMode_2, SonarMode_3] = getSounderParamsForcl_sounderXML(Model, ...
    'nbSwaths', nbSwaths, 'presenceFM', presenceFM, 'SounderMode', SounderMode, ...
    'ScanningInfo', ScanningInfo, 'nbSectors', nbSectors, ...
    'SignalLength', SignalLength, 'isDual', isDual, ...
    'PulseLengthConfIfEM2040', PulseLengthConfIfEM2040);
