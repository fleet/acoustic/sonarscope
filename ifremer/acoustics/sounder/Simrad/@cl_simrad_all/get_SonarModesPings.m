function [flag, Mode1, Mode2, Mode3] = get_SonarModesPings(this, DataDepth, DataRaw, DataRuntime, sublDepth)
    
nbPings = length(sublDepth);
Mode1 = NaN(nbPings, 1);
Mode2 = NaN(nbPings, 1);
Mode3 = NaN(nbPings, 1);

[subDepth, subRaw] = intersect3(DataDepth.PingCounter(sublDepth,1), DataRaw.PingCounter(:,1), DataRaw.PingCounter(:,1));
subDepth = sublDepth(subDepth);

SounderMode = my_interp1_Extrap_PreviousThenNext(DataRuntime.Time, DataRuntime.Mode, DataDepth.Time);
SounderMode = SounderMode(subDepth);

[flag, nbSwaths] = create_signal_nbSwath(this, 'DataRaw', DataRaw);
if ~flag
    return
end
nbSwaths = nbSwaths.Data(subRaw);

[flag, presenceFM] = create_signal_presenceFM(this, 'DataRaw', DataRaw);
if ~flag
    return
end
presenceFM = presenceFM.Data(subRaw);

if isfield(DataDepth, 'ScanningInfo')
    ScanningInfo = DataDepth.ScanningInfo(subDepth,1);
else
    ScanningInfo = zeros(1,length(subDepth)); % Fichiers d�cod�s avant le 03/02/2017
end

EmModel = DataDepth.EmModel;
Model   = sprintf('EM%d', EmModel);
switch Model
    case {'EM2040'; 'EM2040S'; 'EM2040D'}
%         nbSectors = max(DataRaw.TransmitSectorNumberTx(subRaw,:),2);
        nbSectors = max(DataRaw.TransmitSectorNumber(subRaw,:), [], 2);
    otherwise
        nbSectors = [];
end

for k=1:nbPings
    
% TODO : Remplacer DataRaw.SignalLength par
% SignalLength = mode(DataRuntime.TransmitPulseLength(:)) * 1e-3; % Modif JMA le 23/01/2019
% PulseLengthConfIfEM2040 = mode(1 + decodeBits(uint8(DataRuntime.ReceiverFixGain), [4 3]));

    [flag, Mode1(k), Mode2(k), Mode3(k)] = getSounderParamsForcl_sounderXML(Model, ...
        'nbSwaths', nbSwaths(k), 'presenceFM', presenceFM(k), 'SounderMode', SounderMode(k), ...
        'ScanningInfo', ScanningInfo(k), 'nbSectors', nbSectors(k), ...
        'SignalLength', DataRaw.SignalLength(subRaw(k),:));
end
