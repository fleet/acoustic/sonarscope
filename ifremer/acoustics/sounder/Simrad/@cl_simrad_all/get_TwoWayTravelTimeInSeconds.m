function [TwoWayTravelTime_s, TwoWayTravelTimeInSamples, TxDelay_PingBeam_s] = get_TwoWayTravelTimeInSeconds(~, DatagramVersion, DataRaw, DataDepth, sublRaw, sublDepth)

switch DatagramVersion
    case 1 % Raw range and beam angle (F)
        TwoWayTravelTimeInSamples = double(DataRaw.Range(sublRaw,:)) / 2;
        SamplingRate = double(DataDepth.SamplingRate(sublDepth,:));
        if size(SamplingRate,2) == 1
%             TwoWayTravelTime_s = bsxfun(@rdivide, TwoWayTravelTimeInSamples, SamplingRate);
            TwoWayTravelTime_s = TwoWayTravelTimeInSamples ./ SamplingRate;
        else
            sz = size(TwoWayTravelTimeInSamples);
            N = sz(2) / 2;
            subBab = 1:N;
            subTri = (N+1):size(TwoWayTravelTimeInSamples,2);
%             TwoWayTravelTime_s(:,subTri) = bsxfun(@rdivide, TwoWayTravelTimeInSamples(:,subTri), SamplingRate(:,2));
%             TwoWayTravelTime_s(:,subBab) = bsxfun(@rdivide, TwoWayTravelTimeInSamples(:,subBab), SamplingRate(:,1));
            TwoWayTravelTime_s(:,subTri) = TwoWayTravelTimeInSamples(:,subTri) ./ SamplingRate(:,2);
            TwoWayTravelTime_s(:,subBab) = TwoWayTravelTimeInSamples(:,subBab) ./ SamplingRate(:,1);
        end
        TxDelay_PingBeam_s = zeros(size(TwoWayTravelTime_s));
        
    case 2 % Raw range and beam angle (f)
        TwoWayTravelTimeInSamples = double(DataRaw.Range(sublRaw,:));
        SamplingRate              = double(DataDepth.SamplingRate(sublDepth,:));
        
        if size(SamplingRate,2) == 1
%             TwoWayTravelTime_s = bsxfun(@rdivide, TwoWayTravelTimeInSamples, SamplingRate);
            TwoWayTravelTime_s = TwoWayTravelTimeInSamples ./ SamplingRate;
        else
            sz = size(TwoWayTravelTimeInSamples);
            N = sz(2) / 2;
            subBab = 1:N;
            subTri = (N+1):size(TwoWayTravelTimeInSamples,2);
            TwoWayTravelTime_s(:,subTri) = TwoWayTravelTimeInSamples(:,subTri) ./ SamplingRate(:,2);
            TwoWayTravelTime_s(:,subBab) = TwoWayTravelTimeInSamples(:,subBab) ./ SamplingRate(:,1);
            
            % ----------------------------------------------------------------------------------------------
            offsetTribord_s = 6/SamplingRate(1,2);
            TwoWayTravelTime_s(:,subTri) = TwoWayTravelTime_s(:,subTri) - offsetTribord_s; % Test JMA le 02/05/2020 6 échantillons
%             voir"D:\perso-jma\Documents\SPFE\AnalysisInsonifiedAreaEM3002.docx"
            % ----------------------------------------------------------------------------------------------
            
        end
        TxDelay_PingBeam_s = 1e-3 * get_PingBeamMatrixFromPingSignal(DataRaw.SectorTransmitDelay(sublRaw,:), DataRaw.TransmitSectorNumber(sublRaw,:));
%         TwoWayTravelTime_s = TwoWayTravelTime_s + TxDelay_PingBeam / 1000;
        
    case 3 % Raw range and angle 78
        TwoWayTravelTime_s = DataRaw.TwoWayTravelTimeInSeconds(sublRaw,:);
        SamplingRate = double(DataRaw.SamplingRate(sublRaw,:));
        
        if size(SamplingRate,2) == 1
%             TwoWayTravelTimeInSamples = bsxfun(@times, TwoWayTravelTime_s, SamplingRate);
            TwoWayTravelTimeInSamples = TwoWayTravelTime_s .* SamplingRate;
        else
            sz = size(TwoWayTravelTime_s);
            N = sz(2) / 2;
            subBab = 1:N;
            subTri = (N+1):size(TwoWayTravelTime_s,2);
%             TwoWayTravelTimeInSamples(:,subTri) = bsxfun(@times, TwoWayTravelTime_s(:,subTri), SamplingRate(:,2));
%             TwoWayTravelTimeInSamples(:,subBab) = bsxfun(@times, TwoWayTravelTime_s(:,subBab), SamplingRate(:,1));
            TwoWayTravelTimeInSamples(:,subTri) = TwoWayTravelTime_s(:,subTri) .* SamplingRate(:,2);
            TwoWayTravelTimeInSamples(:,subBab) = TwoWayTravelTime_s(:,subBab) .* SamplingRate(:,1);
        end
        
        TxDelay_PingBeam_s = 1e-3 * get_PingBeamMatrixFromPingSignal(DataRaw.SectorTransmitDelay(sublRaw,:), DataRaw.TransmitSectorNumber(sublRaw,:));
%         TwoWayTravelTime_s = TwoWayTravelTime_s + TxDelay_PingBeam_s;
        
    otherwise
        str = sprintf('DatagramVersion=%d, not expected in get_TwoWayTravelTimeInSeconds.', DatagramVersion);
        my_warndlg(str, 1);
end
