function [flag, c, indLayerAsCurrentOne, Selection, Carto] = get_layers(a, nomsLayers, DataTypeCurrentImage, DataTypes, bilan, ModeDependant, Carto, ...
    Selection, UseModel, varargin)

[varargin, flagTideCorrection]       = getPropertyValue(varargin, 'TideCorrection',           0);
[varargin, PreserveMeanValueIfModel] = getPropertyValue(varargin, 'PreserveMeanValueIfModel', 0);
[varargin, MasqueActif]              = getPropertyValue(varargin, 'MasqueActif',              1);
[varargin, Mute]                     = getPropertyValue(varargin, 'Mute',                     0);
[varargin, CartoAuto]                = getPropertyValue(varargin, 'CartoAuto',                0);
[varargin, flagMarcRoche]            = getPropertyValue(varargin, 'flagMarcRoche',            0);
[varargin, SpecialDiagTxCompensated] = getPropertyValue(varargin, 'SpecialDiagTxCompensated', 0);
[varargin, IndNavUnit]               = getPropertyValue(varargin, 'IndNavUnit',               []); %#ok<ASGLU>

c = [];
indLayerAsCurrentOne = [];

listeLayersSupplementaires = [];
DataTypeConditions         = [];
for k2=1:length(bilan)
    DTC = bilan{k2}{1}(1).DataTypeConditions;
    [listeCor, dataTypeCor, flag] = getListeLayersSupplementaires(DTC); %#ok<ASGLU>
    if ~flag
        return
    end
    listeLayersSupplementaires = [listeLayersSupplementaires listeCor];
    DataTypeConditions = [DataTypeConditions DTC];
end

[listeLayersSupplementaires, ia] = unique(listeLayersSupplementaires);
DataTypeConditions = DataTypeConditions(ia);

Version = version_datagram(a);
if isempty(Version)
    flag = 0;
    return
end
switch Version
    case 'V1'
        ListeLayersV1 = [];
        for k2=1:length(DataTypes)
            X = Simrad_getDefaultLayersV1(cl_image.strDataType{DataTypes(k2)}, 'nomLayer', nomsLayers{k2});
            ListeLayersV1 = [ListeLayersV1 X]; %#ok<*AGROW>
        end
        ListeLayersV1 = unique([ListeLayersV1 listeLayersSupplementaires]); % Rajout� par JMA le 09/11/2018
        ListeLayersV1 = unique(ListeLayersV1); % Comment� par JMA le 02/02/2017
        
        [c, Carto] = import_PingBeam_All_V1(a, 'ListeLayers', ListeLayersV1, 'CartoAuto', CartoAuto, 'Carto', Carto, ...
            'TideCorrection', flagTideCorrection, 'MasqueActif', MasqueActif, 'IndNavUnit', IndNavUnit);
        if isempty(c)
            flag = 0;
            return
        end
        
    case 'V2'
        ListeLayersV1 = [];
        for k2=1:length(DataTypes)
            X = Simrad_getDefaultLayersV1(cl_image.strDataType{DataTypes(k2)}, 'nomLayer', nomsLayers{k2});
            ListeLayersV1 = [ListeLayersV1 X]; %#ok<*AGROW>
        end
        ListeLayersV1 = unique([ListeLayersV1 listeLayersSupplementaires]);
%         nbLayersDemandes = length(ListeLayersV1);
        ListeLayersV2 = SelectionLayers_V1toV2(ListeLayersV1);
        [c, Carto] = import_PingBeam_All_V2(a, 'ListeLayers', ListeLayersV2, 'CartoAuto', CartoAuto, 'Carto', Carto, ...
            'TideCorrection', flagTideCorrection, 'MasqueActif', MasqueActif, 'IndNavUnit', IndNavUnit, 'Mute', Mute);
%         if isempty(c) || (length(c) ~= nbLayersDemandes)
        if isempty(c) % Modif JMA le 07/01/2019 car pose un pb pour layer "IncidenceAngle" quand l'utilisateur veut forcer le calcul en fonction du layer BeamPointingAngle
            [~, nomFicSimple] = fileparts(a.nomFic);
            str1 = sprintf('Sans le fichier "%s.all", certains layers n''ont pu �tre charg�s, je bypasse ce fichier', nomFicSimple);
            str2 = sprintf('In file "%s.all", some required layers could not be loaded, I bypass this file.', nomFicSimple);
            my_warndlg(Lang(str1,str2), 0, 'Tag', 'SomeLayersNotLoaded');
            flag = 0;
            return
        end
end

indLayerAsCurrentOne = findIndLayerSonar(c, 1, 'DataType', DataTypeCurrentImage, 'OnlyOneLayer', 'WithCurrentImage', 1);
for k1=1:length(Selection)
    DT = Selection(k1).DataType;
    if DT == cl_image.indDataType('TxAngle')
        DT(2) = cl_image.indDataType('BeamPointingAngle');
    end
    Selection(k1).indLayer = findIndLayerSonar(c, 1, 'DataType', DT, 'WithCurrentImage', 1);
end

%% Compensation statistique (Ex : courbes de calibration)

for k2=1:length(bilan)
    identLayersConditionCompensation = [];
    for k3=1:length(listeLayersSupplementaires)
        switch DataTypeConditions{k3}
            case {'TxAngle'; 'BeamPointingAngle'} % Pour �viter confusion avec cette version de SSc
                strDT = {'TxAngle'; 'BeamPointingAngle'};
            otherwise
                strDT = DataTypeConditions(k3);
        end
        identDataType = [];
        for k4=1:length(strDT)
            identDataType(k4) = cl_image.indDataType(strDT(k4));
        end
        x = findIndLayerSonar(c, 1, 'DataType', identDataType, 'WithCurrentImage', 1);
        if isempty(x)
            if ~Mute
                str1 = sprintf('Le layer de type "%s" n''a pas �t� trouv� pour le fichier "%s".', DataTypeConditions{k3}, a.nomFic);
                str2 = sprintf('Layer of type "%s" was not found for file "%s".', DataTypeConditions{k3}, a.nomFic);
                switch Mute
                    case -1
                        my_warndlg(Lang(str1,str2), 0, 'displayItEvenIfSSc', 1, 'TimeDelay', 30);
                    case 1
                        my_warndlg(Lang(str1,str2), 0);
                    case 0
                        my_warndlg(Lang(str1,str2), 1);
                end
            end
            flag = 0;
            return
        else
            identLayersConditionCompensation(k3) = x;
        end
    end
    if length(UseModel) == 1
        UseModelHere = UseModel;
        PreserveMeanValueIfModelHere = PreserveMeanValueIfModel;
    else
        UseModelHere = UseModel(k2);
        PreserveMeanValueIfModelHere = PreserveMeanValueIfModel(k2);
    end
    [d, flag] = compensationCourbesStats(c(indLayerAsCurrentOne), c(identLayersConditionCompensation), ...
        bilan{k2}, 'ModeDependant', ModeDependant, 'UseModel', UseModelHere, ...
        'PreserveMeanValueIfModel', PreserveMeanValueIfModelHere, 'flagMarcRoche', flagMarcRoche);
    if ~flag
        return
    end
    if SpecialDiagTxCompensated
        d.Sonar.DiagEmi.etat               = 1;
        d.Sonar.DiagEmi.origine            = 2;
        d.Sonar.DiagEmi.IfremerTypeCompens = 2;
    end
    c(indLayerAsCurrentOne) = d;
end

flag = 1;
