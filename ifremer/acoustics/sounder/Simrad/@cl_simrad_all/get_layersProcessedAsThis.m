function [flag, c, indLayerAsCurrentOne, Selection, Carto] = get_layersProcessedAsThis(a, this, indImage, nomsLayers, DataTypes, bilan, ModeDependant, Carto, ...
    Selection, SameReflectivityStatus, UseModel, varargin)

[varargin, flagTideCorrection]       = getPropertyValue(varargin, 'TideCorrection',           0);
[varargin, PreserveMeanValueIfModel] = getPropertyValue(varargin, 'PreserveMeanValueIfModel', 0);
[varargin, MasqueActif]              = getPropertyValue(varargin, 'MasqueActif',              1);
[varargin, Mute]                     = getPropertyValue(varargin, 'Mute',                     0);
[varargin, flagMarcRoche]            = getPropertyValue(varargin, 'flagMarcRoche',            0);
[varargin, CartoAuto]                = getPropertyValue(varargin, 'CartoAuto',                0); %#ok<ASGLU>

DataTypeCurrentImage = this(indImage).DataType;
[flag, c, indLayerAsCurrentOne, Selection, Carto] = get_layers(a, nomsLayers, DataTypeCurrentImage, DataTypes, ...
            bilan, ModeDependant, Carto, Selection, UseModel, ...
            'PreserveMeanValueIfModel', PreserveMeanValueIfModel, 'TideCorrection', flagTideCorrection, ...
            'MasqueActif', MasqueActif, 'CartoAuto', CartoAuto, 'Mute', Mute, 'flagMarcRoche', flagMarcRoche);
if ~flag
	return
end
        
%% Traitement de la r�flectivit�

if SameReflectivityStatus
    [flag, c] = setReflectivityStatusAsThis(this(indImage), c, indLayerAsCurrentOne, 'Mute', 1);
    if ~flag
        str1 = 'La donn�e de r�flectivit� n''a pas pu �tre rendue conforme � celle de l''image de r�f�rence.';
        str2 = 'The Reflectivity data could not be set as the reference one.';
        my_warndlg(Lang(str1,str2), 1);
        flag = 0;
        return
    end
end

flag = 1;
