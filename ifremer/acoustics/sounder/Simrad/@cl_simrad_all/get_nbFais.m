% Lecture du nombre de faisceaux de "depth" dans un fichier .all
%
% Syntax
%   NbMaxFaisceaux = get_nbFais(aKM)
% 
% Input Arguments
%   aKM : Instance de cl_simrad_all
%
% Output Arguments
%   NbMaxFaisceaux : Nombre max de faisceaux 
%
% Examples
%   nomFic = getNomFicDatabase('EM1002_0001_20040916_054013_raw.all')
%   aKM = cl_simrad_all('nomFic', nomFic);
%   NbMaxFaisceaux = get_nbFais(aKM)
%
% See also cl_simrad_all cl_simrad_all/plot_position cl_simrad_all/plot_nav Authors
% Authors : JMA
% -------------------------------------------------------------------------

function NbMaxFaisceaux = get_nbFais(this)

[nomDir, nomFic] = fileparts(this.nomFic);
nomFicMat = fullfile(nomDir, 'SonarScope', [nomFic '_depth.mat']);
if exist(nomFicMat ,'file')
    NbMaxFaisceaux = loadmat(nomFicMat, 'nomVar', 'NbMaxFais');
    if isempty(NbMaxFaisceaux)
        [flag, DataDepth] = read_depth_bin(this);
        NbMaxFaisceaux = size(DataDepth.Depth, 2);
    end
else
    [flag, DataDepth] = read_depth_bin(this, 'Memmapfile', 1);
    NbMaxFaisceaux = DataDepth.Dimensions.nbBeams;
end
