function noms = get_nomFic(this)

noms = {};
for k=1:length(this)
    noms{k,1} = this(k).nomFic; %#ok<AGROW>
end
