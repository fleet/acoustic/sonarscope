% Histogramme des type de datagrams contenus dans un fichier .all
%
% Syntax
%   histo_index(aKM)
%
% Input Arguments
%   aKM : Instance de cl_simrad_all
%
% Name-Value Pair Arguments
%   fullListe : 1=affichage que des datagrams presents, 0=tous les datagrams
%
% Output Arguments
%   []                : Auto-plot activation
%   texteTypeDatagram : Resume textuel de l'histogramme
%   Code              : Abscisses
%   histoTypeDatagram : Nb de datagrammes (ordonnees)
%
% Examples
%   nomFic = getNomFicDatabase('EM1002_0001_20040916_054013_raw.all')
%   aKM = cl_simrad_all('nomFic', nomFic);
%   histo_index(aKM)
%   [texteTypeDatagram] = histo_index(aKM)
%   [texteTypeDatagram, Code, histoTypeDatagram] = histo_index(aKM)
%   figure; bar(Code, histoTypeDatagram)
%
% See also cl_simrad_all plot_index Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [texteTypeDatagram, Code, histoTypeDatagram] = histo_index(this, varargin)

flagPlot = (nargout == 0);

NbFic = length(this);
hw = create_waitbar('Plot .all Histo Index', 'N', NbFic);
for k=1:NbFic
    [texteTypeDatagram, X1, X2, X3] = histo_index_unitaire(this(k), flagPlot, varargin{:});
    if isempty(texteTypeDatagram)
        Code              = [];
        histoTypeDatagram = [];
        return
    end
    sub = 1:length(X1);
    Code(k,sub)              = X1; %#ok<AGROW>
    histoTypeDatagram(k,sub) = X2; %#ok<AGROW>
    tabNbBytes(k,sub)        = X3; %#ok<AGROW>
end
my_close(hw);

if NbFic > 1
    Code = max(Code);
    histoTypeDatagram = sum(histoTypeDatagram);
    tabNbBytes = sum(tabNbBytes);
    
    flagPlot = (nargout == 0);
    if flagPlot
        sub = (histoTypeDatagram ~= 0);
        plot_histo_index(Code(sub), histoTypeDatagram(sub), texteTypeDatagram, tabNbBytes(sub), 'All files')
    end
end
