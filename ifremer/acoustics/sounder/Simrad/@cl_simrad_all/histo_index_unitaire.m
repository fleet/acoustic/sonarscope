function [texteTypeDatagram, Code, histoTypeDatagram, tabNbBytes] = histo_index_unitaire(this, flagPlot, varargin)

[varargin, fullListe] = getPropertyValue(varargin, 'fullListe', 0); %#ok<ASGLU>

[flag, Data] = read_FileIndex_bin(this);
if ~flag
    texteTypeDatagram = [];
    Code              = [];
    histoTypeDatagram = [];
    tabNbBytes        = [];
    return
end

TypeDatagram   = Data.TypeOfDatagram;
DatagramLength = Data.DatagramLength;

nomFicWC = strrep(this.nomFic, '.all', '.wcd');
if exist(nomFicWC, 'file')
    [flag, DataWC] = read_FileIndex_bin(this, 'nomFic', nomFicWC);
    if flag
        TypeDatagramWC = DataWC.TypeOfDatagram;
        subWC = (TypeDatagramWC == 107); % On oublie les autres datagrammes
        
        DatagramLengthWC = grpstats(double(DataWC.DatagramLength(subWC)), DataWC.PingCounter(subWC), {'sum'});
        
        TypeDatagram   = [TypeDatagram;   zeros(size(DatagramLengthWC)) + 107];
        DatagramLength = [DatagramLength; DatagramLengthWC];
    end
end

TypeDatagram(TypeDatagram >= 254) = []; % Correction bug fichier 0118_20181102_070107_Europe2040_.all
TypeDatagram(TypeDatagram  == 0)  = []; % Correction bug fichier 0118_20181102_070107_Europe2040_.all

listeTypeDatagram = unique(TypeDatagram);
nbTypeDatagrams = length(listeTypeDatagram);
nbBytes = zeros(1,nbTypeDatagrams );
for k=1:nbTypeDatagrams
    sub = (TypeDatagram == listeTypeDatagram(k));
    nbBytes(k) = sum(DatagramLength(sub));
%     fprintf('Datagramm %3d    Nb of Bytes %10d\n', listeTypeDatagram(k), nbBytes(k));
end

%% Lecture du fichier d'index

Code = min(TypeDatagram):max(TypeDatagram);
histoTypeDatagram = my_hist(TypeDatagram, Code);
texteTypeDatagram = codesDatagramsSimrad(Code, histoTypeDatagram, 'fullListe', fullListe);

tabNbBytes = zeros(size(histoTypeDatagram));
tabNbBytes(histoTypeDatagram ~= 0) = nbBytes;

if flagPlot
    sub = (histoTypeDatagram ~= 0);
    plot_histo_index(Code(sub), histoTypeDatagram(sub), texteTypeDatagram, tabNbBytes(sub), this.nomFic);
end
