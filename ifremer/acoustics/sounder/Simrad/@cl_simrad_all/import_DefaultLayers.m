% Import the default layers of a .all file (in PingBeam geometry)
%
% Syntax
%   [flag, a] = import_DefaultLayers(allKM)
%
% Input Arguments
%   allKM : Instance of cl_simrad_all
%
% Output Arguments
%   flag : 1=OK, 0=KO
%   a    : Instance(s) of cl_image
%
% Examples
%     nomFic = getNomFicDatabase('EM1002_BELGICA_005053_raw.all')
%     allKM = cl_simrad_all('nomFic', nomFic);
%   [flag, a] = import_DefaultLayers(allKM);
%     SonarScope(a)
%
%     nomFic = 'D:\Demos-3DV\WreckEM710Dz\Data\0006_20090605_211643.all'
%     allKM = cl_simrad_all('nomFic', nomFic);
%   [flag, a] = import_DefaultLayers(allKM);
%     SonarScope(a)
%
% See also cl_simrad_all.vew_depth_V1 cl_simrad_all.vew_depth_V2 Authors
% Authors  : JMA
%-------------------------------------------------------------------------------

function [flag, a] = import_DefaultLayers(this)

flag = 1;
a = [];

Version = version_datagram(this);
switch Version
    case 'V1'
        ListeLayers = [2 3 4 6 10 24 26 27];
        a = import_PingBeam_All_V1(this, 'ListeLayers', ListeLayers);
        if isempty(a)
            flag = 0;
            return
        end
    case 'V2'
        ListeLayers = Simrad_getDefaultLayersV2;
        ListeLayers.Order = [];
        a = import_PingBeam_All_V2(this, 'ListeLayers', ListeLayers);
end
