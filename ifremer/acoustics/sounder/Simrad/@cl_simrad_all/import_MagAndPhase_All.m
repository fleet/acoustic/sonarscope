% Visualisation du contenu des datagrams "Seabed Image" contenus dans un fichier .all
%
% Syntax
%   [b, S] = import_MagAndPhase_All(aKM, ...);
%
% Input Arguments
%   aKM : Instance de cl_simrad_all
%
% Name-Value Pair Arguments
%   subl : Numeros de lignes � traiter
%
% Name-only Arguments
%   FaisceauxPairs         : Pour cr�er une image uniquement avec les faisceaux pairs
%   FaisceauxImairs        : Pour cr�er une image uniquement avec les faisceaux impairs
%   NoSpecularCompensation : Pour ne pas faire de correction du sp�culaire
%
% Output Arguments
%   b          : Instance de cli_image
%   subl_Image : Numeros de lignes communs avec la bathy (index relatis � seabedImage)
%   subl_Depth : Numeros de lignes communs avec la bathy (index relatis � Depth)
%
% Examples
%   nomFic = getNomFicDatabase('EM1002_0001_20040916_054013_raw.all')
%   aKM = cl_simrad_all('nomFic', nomFic);
%   b = import_MagAndPhase_All(aKM);
%   SonarScope(b);
%
%   b = import_MagAndPhase_All(aKM, 'subl', 1:100);
%   SonarScope(b);
%
% See also cl_simrad_all cl_simrad_all/plot_position cl_simrad_all/plot_nav Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [b, Carto, TransmitSectorNumber, R] = import_MagAndPhase_All(this, varargin)

parameters_BDAgetPlatform;

[varargin, DisplayLevel] = getPropertyValue(varargin, 'DisplayLevel', 1);
[varargin, Carto]        = getPropertyValue(varargin, 'Carto',        []);
[varargin, DepthMin]     = getPropertyValue(varargin, 'DepthMin',     -1);
[varargin, DepthMax]     = getPropertyValue(varargin, 'DepthMax',     -12000);
[varargin, Draught]      = getPropertyValue(varargin, 'Draught',      0);
[varargin, Mute]         = getPropertyValue(varargin, 'Mute',         0);
[varargin, subPingDepth] = getPropertyValue(varargin, 'subPing',      []); %#ok<ASGLU>
% [varargin, RMax1_Precedent] = getPropertyValue(varargin, 'RMax1_Precedent', []);

b = [];
R = [];
TransmitSectorNumber = [];

%% Check the number of elements

flag = checkIfOneElement(length(this));
if ~flag
    return
end

%%

% subPingDepth = 1:50:1000

[flag, DataDepth] = read_depth_bin(this, 'Memmapfile', 1);
if ~flag
    return
end
[flag, DataMagPhase] = SSc_ReadDatagrams(this.nomFic, 'Ssc_RawBeamSamples', 'Memmapfile', -1);
if ~flag
    return
end
[flag, DataRaw] = SSc_ReadDatagrams(this.nomFic, 'Ssc_RawRangeBeamAngle', 'Memmapfile', -1); % Non test� ici
if ~flag
    return
end


if ~Mute
    figure(5555); plot(DataDepth.Datetime, DataDepth.PingCounter(:,1)-DataDepth.PingCounter(1,1), '-*'); grid on
    hold on; plot(DataMagPhase.Datetime, DataMagPhase.PingCounter(:,1)-DataDepth.PingCounter(1,1), '-or');
    xlabel('Time'); ylabel('Ping Counter')
    legend({'Depth datagram'; 'Mag & Phase datagram'})
end

P1 = DataDepth.PingCounter(:,1);
P2 = DataMagPhase.PingCounter(:,1);
P3 = DataRaw.PingCounter(:,1);

if isempty(subPingDepth)
    subPingDepth = 1:length(P1);
end
[subDepth, subPingMagPhase, subRaw] = intersect3(P1(subPingDepth), P2, P3);
if isempty(subPingMagPhase)
    str1 = 'Ce ping n''existe pas en Water-Column';
    str2 = 'This ping does not exist in Water-Column';
    my_warndlg(Lang(str1,str2), 1);
    return
end
%     subPingMagPhase = subPingDepth(subPingMagPhase);

[flag, BeamDataOut] = get_PingRawBeamSamples(this, DataMagPhase, DataDepth, subPingMagPhase, subPingDepth);

for k=1:length(subDepth)
    BeamDataOut(k).SamplingRate = DataDepth.SamplingRate(subDepth(k)); % GLU TODO : redonn� ici
    BeamDataOut(k).Frequency    = DataRaw.CentralFrequency(subRaw(k),1); % GLU TODO
end


if ~flag
    return
end

[flag, IdentSonar] = SimradModel2IdentSonar(DataMagPhase.EmModel, DataDepth.EmModel);
if ~flag
    return
end

SonarDescription = cl_sounder('Sonar.Ident', IdentSonar, 'Sonar.Mode_1', 1, ...
    'Sonar.SystemSerialNumber', DataMagPhase.SystemSerialNumber);

TxPulseWidth = get(SonarDescription, 'Signal.Duration');

if isempty(DataMagPhase) || isempty(BeamDataOut)
    return
end


N = length(subPingMagPhase);
str1 = 'Chargement de l''Amplitude et de la phase : 2/2';
str2 = 'Loading Amplitude & Phase : 2/2';
hw = create_waitbar(Lang(str1,str2), 'N', N);
% TransmitSectorNumber = zeros(N, DataRaw.Dimensions.nbRx, 'single');
TransmitSectorNumber = zeros(N, DataMagPhase.Dimensions.nbRx, 'single');
b = repmat(cl_image, 1, N);
for k=1:N
    my_waitbar(k, N, hw)
    iPing = subPingMagPhase(k);
    
    %     TransmitSectorNumber(k,:) = DataRaw.TransmitSectorNumber(subRaw(k), :);
    TransmitSectorNumber(k,:) = DataMagPhase.TransmitSectorNumber(subPingMagPhase(k), :);
    
    DataPing = Extraction(DataMagPhase, iPing);
    %     DataPing.TxAngle = DataMagPhase.RxArray.BeamPointingAngle(iPing,:);
    DataPing.TxAngle = DataMagPhase.BeamPointingAngle(iPing,:);
    DataPing.TransmitSectorNumber = BeamDataOut(k).TransmitSectorNumber;
    DataPing.TxPulseWidth = TxPulseWidth * 1e-3;
    fe = DataPing.SampleRate;
    
    switch DataPing.EmModel
        case 3020
            RangeMax = 1000; % (m)
        case 710
            RangeMax = 3000; % (m)
        case 302
            RangeMax = 5000; % (m)
        case 122
            RangeMax = 13000; % (m)
        case 2040
            RangeMax = 100; % (m) % TODO
        otherwise
            if ~isdeployed
                my_warndlg('RangeMax � r�gler dans import_MagAndPhase_All', 0, 'Tag', 'RangeMaxWaterColumn');
            end
            RangeMax = 13000; % (m)
    end
    
    RangeMax = min(min(floor(2* fe .* RangeMax / 1500), size(BeamDataOut(k).Samples,1)));
    
    Amplitude = BeamDataOut(k).Samples(1:RangeMax,:);
    if isempty(Amplitude)
        continue
    end
    Phase = BeamDataOut(k).Phase(1:RangeMax,:);
    
    
    R = BeamDataOut(k).DetectedRangeInSamples + 1;
    Angles = DataPing.TxAngle;
    C = DataPing.SoundVelocity(1); % TODO Distinction bab tri (pas tr�s n�cessaire ici mais bon, ce serait quand m�me mieux)
    
    if length(unique(DataPing.SamplingRateMinMax)) == 1
        D = (R/2) * C(1) ./ fe(1); % TODO Distinction bab tri � faire absolument
    else
        feBab = DataPing.SamplingRateMinMax(1);
        feTri = DataPing.SamplingRateMinMax(2);
        subTri = (BeamDataOut(k).TransmitSectorNumber == 2);
        D(~subTri) = (R(~subTri)/2) * C(1) / feBab;
        D( subTri) = (R( subTri)/2) * C(2) / feTri;
    end
    
    %     switch DataMagPhase.SystemSerialNumberMin
    %         case {593; 594}
    %             feBab = fe;
    %             feTri = fe * (14620/13960); % 0010_20090225_001241_ShipName
    %             feTri = fe * (14620/14290); % 0010_20090226_003818_Belgica
    %             subTri = (BeamDataOut(k).TransmitSectorNumber == 2);
    %             D(~subTri) = (R(~subTri)/2) * C / feBab; %#ok<AGROW>
    %             D( subTri) = (R( subTri)/2) * C / feTri; %#ok<AGROW>
    %         otherwise
    %             D = (R/2) * C / fe;
    %     end
    
    %     figure(7000+k);
    if ~Mute
        figure(7000+iPing);
        hc(1) = subplot(4,1,1); PlotUtils.createSScPlot(Angles, '-+'); grid on; title(sprintf('Angles for iPing=%d', iPing))
        hc(2) = subplot(4,1,2); PlotUtils.createSScPlot(R, '-+'); grid on; title(sprintf('Kongsberg Detection for iPing=%d', iPing)); set(gca,'YDir', 'Reverse')
        hc(3) = subplot(4,1,3); PlotUtils.createSScPlot(-D .* cosd(Angles), '-+'); grid on; title(sprintf('Pseudo Z for iPing=%d', iPing))
        linkaxes(hc, 'x')
        subplot(4,1,4);  PlotUtils.createSScPlot(D .* sind(Angles), -D .* cosd(Angles), '-+'); grid on; title(sprintf('Pseudo Z for iPing=%d', iPing))
    end
    
    PulseDuration    = get(SonarDescription, 'Signal.Duration'); % sBandWth
    SampleRate       = DataPing.SampleRate; % Hz
    ReceiveBeamWidth = get(SonarDescription, 'BeamForm.Rx.TransWth'); % deg
    %     PingSequence        = DataPing.PingSequence;
    
    
    %% Data reduction in the Time domain
    
    StepMax = ceil(SampleRate(1) * PulseDuration*1e-3 / 2);
    [AmplitudeReduced, step, yAmplitudeReduced, R0_FirstEstimation] = ...
        SampleBeam_ReduceMatrixAmplitude(Amplitude, 'StepMax', StepMax); %#ok<ASGLU>
    
    %% Preprocessing of Mask on Amplitude
    
    DepthMaxPing = abs(DepthMax)-Draught;
    DepthMinPing = abs(DepthMin)-Draught;
    
    %  SampleRate(1) todo
    IdentAlgo = 1; % TODO
    [R0, RMax1, RMax2, R1SamplesFiltre, iBeamMax0, AmpPingNormHorzMax, MaskWidth, iBeamBeg, iBeamEnd] = ...
        SampleBeam_PreprocAmpMask_7111_V6(...
        AmplitudeReduced, step, DataPing.TxAngle, ReceiveBeamWidth, ...
        SampleRate(1), PulseDuration*1e-3, DepthMinPing, DepthMaxPing, IdentAlgo, DisplayLevel, iPing);
    %         RMax1_Precedent = RMax1;
    %     RMax1_Precedent = RMax1 * (1500 / SampleRate / 2);
    
    
    %{
figure(56557); imagesc(reflec_Enr2dB(Amplitude)); colorbar;
hold on;
plot(repmat(R0 ,1,length(RMax1)), '*c');
plot(RMax2, 'ok')
plot(RMax1, '*k')
hold off
    %}
    DataPing.R0                 = R0;
    DataPing.RMax1              = RMax1;
    DataPing.RMax2              = RMax2;
    DataPing.R1SamplesFiltre    = R1SamplesFiltre;
    DataPing.iBeamMax0          = iBeamMax0;
    DataPing.AmpPingNormHorzMax = AmpPingNormHorzMax;
    DataPing.MaskWidth          = MaskWidth;
    %     DataPing.DataTVG            = DataTVG;
    DataPing.iBeamBeg           = iBeamBeg;
    DataPing.iBeamEnd           = iBeamEnd;
    DataPing.ReceiveBeamWidth   = ReceiveBeamWidth;
    DataPing.AcrossDist         = BeamDataOut(k).AcrossDist;
    DataPing.AlongDist          = BeamDataOut(k).AlongDist;
    DataPing.Depth              = BeamDataOut(k).Depth;
    
    % Cr�ation de l'instance de classe
    
    %     IdentSonar = find_IdentSonar(cl_sounder, num2str(DataPing.SystemSerialNumber), ...
    %         'Frequency', DataPing.Frequency/1000);
    %     SonarDescription = cl_sounder('Sonar.Ident', IdentSonar, 'Sonar.Mode_1', 1, ...
    %     'Sonar.SystemSerialNumber', DataPing.SystemSerialNumber);
    
    [~, TagSynchro] = fileparts(this.nomFic);
    %     NomImage = sprintf('%s_%d', TagSynchro, BeamDataOut(k).iPing);
    TagSynchro = sprintf('%s_%d', TagSynchro, BeamDataOut(k).iPing);
    NomImage = TagSynchro;
    
    b(1,k) = cl_image_I0;
    if isa(Amplitude, 'cl_memmapfile')
        b(1,k) = set_Image(b(1,k), Amplitude);
    else
        if N > 1
            b(1,k) = set_Image(b(1,k), cl_memmapfile('Value', Amplitude, 'LogSilence', 1));
        else
            b(1,k) = set_Image(b(1,k), Amplitude, 'NoMemmapfile');
        end
    end
    b(1,k).Name              = NomImage;
    b(1,k).x                 = 1:size(Amplitude,2);
    b(1,k).y                 = 1:size(Amplitude,1);
    b(1,k).YDir              = 2;
    % b(1,k).InitialImageName = nomLayer;
    b(1,k).Unit              = 'Amp';
    b(1,k).TagSynchroX       = TagSynchro;
    b(1,k).TagSynchroY       = TagSynchro;
    b(1,k).XUnit             = 'Beam #';
    b(1,k).YUnit             = 'Sample #';
    b(1,k).InitialFileName   = this.nomFic;
    b(1,k).InitialFileFormat = 'SimradAll';
    b(1,k).Comments          = sprintf('Reson PingCounter : %d', DataPing.PingCounter);
    b(1,k).GeometryType      = cl_image.indGeometryType('SampleBeam');
    
    b(1,k) = set_SonarDescription(b(1,k), SonarDescription);
    %     b(1,k).Comments    = Comments{ListeLayers(k)};
    %     b(1,k).strMaskBits = strMaskBits;
    
    b(1,k) = set_SampleBeamData(b(1,k), DataPing);
    
    b(1,k).DataType = cl_image.indDataType('Reflectivity');
    b(1,k).ColormapIndex = 3;
    
    %     if ~isempty(DataSoundSpeedProfile)
    %         SoundSpeedProfile.Z = DataSoundSpeedProfile.Depth;
    %         SoundSpeedProfile.T = DataSoundSpeedProfile.WaterTemperature;
    %         SoundSpeedProfile.S = DataSoundSpeedProfile.Salinity;
    %         SoundSpeedProfile.C = DataSoundSpeedProfile.SoundSpeed;
    %         b(1,k) = set_SonarBathyCel(b(1,k), SoundSpeedProfile);
    %     end
    b(1,k) = update_Name(b(1,k));
    b(1,k).Writable = false;
    
    %     b(1,k) = compute_stats(b(1,k));
    %     b(1,k).TagSynchroContrast = num2str(rand(1));
    %     StatValues = b(1,k).StatValues;
    %     CLim = [StatValues.Min StatValues.Max];
    %     b(1,k).CLim = CLim;
    
    %% D�finition d'une carto
    
    while isempty(Carto)
        %         Carto = getDefinitionCarto(b(1,k), 'NoGeodetic', 1);
        Carto = getDefinitionCarto(b(1,k), 'NoGeodetic', 0);
    end
    b(1,k) = set_Carto(b(1,k), Carto);
    
    
    b(2,k) = cl_image;
    if isa(Phase, 'cl_memmapfile')
        b(2,k) = set_Image(b(1,k), Phase);
    else
        if N > 1
            b(2,k) = set_Image(b(2,k), cl_memmapfile('Value', Phase, 'LogSilence', 1));
        else
            b(2,k) = set_Image(b(2,k), Phase, 'NoMemmapfile');
        end
    end
    
    b(2,k).x                  = 1:size(Phase,2);
    b(2,k).y                  = 1:size(Phase,1);
    b(2,k).YDir               = 2;
    b(2,k).Name               = NomImage;
    b(2,k).Unit               = 'deg';
    b(2,k).TagSynchroX        = TagSynchro;
    b(2,k).TagSynchroY        = TagSynchro;
    b(2,k).TagSynchroContrast = num2str(rand(1));
    b(2,k).XUnit              = 'Beam #';
    b(2,k).YUnit              = 'Sample #';
    b(2,k).InitialFileName    = this.nomFic;
    b(2,k).InitialFileFormat  = 'SimradAll';
    b(2,k).GeometryType       = cl_image.indGeometryType('SampleBeam');
    
    b(2,k) = set_SonarDescription(b(2,k), SonarDescription);
    b(2,k) = set_SampleBeamData(b(2,k), DataPing);
    b(2,k).DataType      = cl_image.indDataType('TxAngle');
    b(2,k).ColormapIndex = 3;
    b(2,k) = compute_stats(b(2,k));
    b(2,k).TagSynchroContrast = num2str(rand(1));
    StatValues = b(2,k).StatValues;
    b(2,k).CLim = [StatValues.Min StatValues.Max];
    b(2,k).ColormapIndex = 17;
    
    b(2,k) = update_Name(b(2,k));
    b(2,k).Writable = false;
    
end
b = b(:);
my_close(hw, 'MsgEnd')

%% Edition de l'objet cli_image

if nargout == 0
    SonarScope(b);
end


function DataPing = Extraction(Data, k)

DataPing.PingCounter        = Data.PingCounter(k,:);
DataPing.SystemSerialNumber = Data.SystemSerialNumber(k,:);

DataPing.SampleRate         = Data.SamplingFreq(k,:);
DataPing.EmModel            = Data.EmModel;
if isfield(Data, 'Position') % A conserver tant qu'on peut lire les donn�es ancienne lecture
    DataPing.Position       = Data.Position(k,:);
end

DataPing.SoundVelocity      = Data.SoundSpeed(k,:);

DataPing.SamplingRateMinMax = Data.SamplingFreq(k,:);
DataPing.Frequency          = Data.CentralFrequency(k,:); % Babord uniquement pour le moment
DataPing.MultiPingSequence  = []; % En attente de maintenance
DataPing.ReceiveBeamWidth   = []; % En attente de maintenance

T = Data.Time;
DataPing.Time = T(k);
DataPing.Time     = DataPing.Time.timeMat;
DataPing.Datetime = Data.Datetime(k);
