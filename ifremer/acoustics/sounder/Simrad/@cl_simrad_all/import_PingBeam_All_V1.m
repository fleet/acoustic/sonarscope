% Visualisation du contenu des datagrams "depth" contenus dans un fichier .all
%
% Syntax
%   [b, Carto] = import_PingBeam_All_V1(aKM)
%
% Input Arguments
%   aKM : Instance de cl_simrad_all
%
% Name-Value Pair Arguments
%   subl         : Numeros de lignes � traiter
%   subc         : Numeros de faisceaux � traiter
%   TagSynchroY  : Tag de synchronisation en Y
%   ListeLayers  : Numeros des layers [] par defaut
%   Carto        : Parametres cartographiques ([] par defaut)
%   memeReponses : false=On pose les questions � chaque appel, true=On m�morise les r�ponses
%
% Output Arguments
%   b     : Instance de cli_image
%   Carto : Parametres cartographiques
%
% Examples
%   nomFic = getNomFicDatabase('EM1002_0001_20040916_054013_raw.all')
%   aKM = cl_simrad_all('nomFic', nomFic);
%   b = import_PingBeam_All_V1(aKM);
%   b = editobj(b);
%
% See also cl_simrad_all import_PingBeam_All_V2 plot_position cl_simrad_all/plot_nav Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [c, Carto, sublDepth, nbPings, OrigineDepthRaw] = import_PingBeam_All_V1(this, varargin)

persistent persistent_ListeLayers persistent_Carto

[varargin, subl]                  = getPropertyValue(varargin, 'subl',              []);
[varargin, subc]                  = getPropertyValue(varargin, 'subc',              []);
[varargin, ListeLayers]           = getPropertyValue(varargin, 'ListeLayers',       []);
[varargin, Carto]                 = getPropertyValue(varargin, 'Carto',             []);
[varargin, CartoAuto]             = getPropertyValue(varargin, 'CartoAuto',         0);
[varargin, memeReponses]          = getPropertyValue(varargin, 'memeReponses',      false);
[varargin, MasqueActif]           = getPropertyValue(varargin, 'MasqueActif',       0);
[varargin, flagTideCorrection]    = getPropertyValue(varargin, 'TideCorrection',    0);
[varargin, flagDraughtCorrection] = getPropertyValue(varargin, 'DraughtCorrection', 0);
[varargin, Mute]                  = getPropertyValue(varargin, 'Mute',              0);
[varargin, IndNavUnit]            = getPropertyValue(varargin, 'IndNavUnit',        []);

c = cl_image;
c(:) = [];
OrigineDepthRaw = [];
sublDepth       = [];
nbPings         = [];

A0 = cl_simrad_all([]);

%% Check the number of elements

flag = checkIfOneElement(length(this));
if ~flag
    return
end
[~, SousVersionV1] = version_datagram(this);
switch SousVersionV1
    case 'F'
        DatagramVersion = 1;
    case 'f'
        DatagramVersion = 2;
end

%%

I0 = cl_image_I0;

[nomDir, NomImage] = fileparts(this.nomFic);

[varargin, TagSynchroY] = getPropertyValue(varargin, 'TagSynchroY', NomImage); %#ok<ASGLU>

[ListeNomLayers, Unit, Comments, strSsc] = ALL.getListeLayers;

if isempty(ListeLayers)
    if ~memeReponses
        %         [ListeLayers, flag] = my_listdlg('Layers :', strSsc, 'InitialValue', [2 3 4 6 10 24 26 27]);
        [ListeLayers, flag] = my_listdlg('Layers :', strSsc, 'InitialValue', [2 3 4 10 24 25 26 27]); % 25 au lieu de 6 (InSeconds au lieu de InSamples)
        if ~flag || isempty(ListeLayers)
            c = [];
            return
        end
        persistent_ListeLayers = ListeLayers;
    else
        ListeLayers = persistent_ListeLayers;
    end
end

%% Lecture de la donn�e

[~, DataRuntime] = SSc_ReadDatagrams(this.nomFic, 'Ssc_Runtime');

%% Vraisemblablement plus actif : devrait �tre supprim�

if MasqueActif == 1
    Option = 'MasqueDepth';
else
    Option = [];
end

% [flag, DataDepth] = read_depth_bin(this, Option, 'Memmapfile', 1);
% if ~flag
%     return
% end

[flag, DataDepth, VarNameInFailure] = read_depth_bin(this, Option, 'Memmapfile', -1);
% [flag, DataDepth, VarNameInFailure] = read_depth_bin(this, Option, 'Memmapfile', -3);
if ~flag
    if isempty(VarNameInFailure)
        str = sprintf('Pb reading "Depth" datagrams in file "%s"', this.nomFic);
    else
        str = sprintf('Pb reading variable "%s" in "Depth" datagrams in file "%s"', VarNameInFailure, this.nomFic);
    end
    my_warndlg(str, 0);
    c = [];
    return
end
nbPings = DataDepth.Dimensions.nbPings;

if ~isfield(DataDepth, 'PingCounter')
    c = [];
    return
end

if isempty(DataDepth.PingCounter)
    str = sprintf('No "Depth" datagrams in this file. (%s)', this.nomFic);
    my_warndlg(str, 0);
    c = [];
    return
end
nbPings = length(DataDepth.PingCounter);

[flag, DataRaw] = read_rawRangeBeamAngle(this, 'Memmapfile', -1);
% [flag, DataRaw] = read_rawRangeBeamAngle(this, 'Memmapfile', -3);
if ~flag
    c = [];
    return
end

if isempty(DataRaw) || isempty(DataRaw.PingCounter)
    subVide = find((ListeLayers > 13) & (ListeLayers < 18));
    if ~isempty(subVide)
        str1 = 'Vous avez demand� le chargement de layers issus des datagrams "RawRangeBeamAngle" mais ces datagrams n''existent pas dans votre fichier .all';
        str2 = 'TODO';
        my_warndlg(Lang(str1,str2), 0, 'Tag', 'Y''a pas DataRaw');
        ListeLayers(subVide) = [];
    end
end

%% Lecture et interpolation de la donnee "Attitude"

[flag, DataAttitude] = read_attitude_bin(this, 'Memmapfile', 0); %1);
if ~flag || isempty(DataAttitude) || isempty(DataAttitude.Roll)
    RollTx  = [];
    Pitch   = [];
    HeaveTx = [];
    Tide    = [];
    Draught = [];
    subVide = find((ListeLayers >= 20) & (ListeLayers < 24));
    if ~isempty(subVide)
        str1 = 'Vous avez demand� le chargement de layers issus des datagrams "Attitude" mais ces datagrams n''existent pas dans votre fichier .all';
        str2 = 'TODO';
        my_warndlg(Lang(str1,str2), 0, 'Tag', 'Y''a pas DataAttitude');
        ListeLayers(subVide) = [];
    end
else
    RollTx  = my_interp1(DataAttitude.Datetime, DataAttitude.Roll,   DataDepth.Datetime, 'linear', 'extrap');
    Pitch   = my_interp1(DataAttitude.Datetime, DataAttitude.Pitch,  DataDepth.Datetime, 'linear', 'extrap');
    HeaveTx = my_interp1(DataAttitude.Datetime, DataAttitude.Heave,  DataDepth.Datetime, 'linear', 'extrap');
    
    if isfield(DataAttitude, 'Tide') && ~isempty(DataAttitude.Tide)
%         Tide.Time     = DataAttitude.Time;
        Tide.Datetime = DataAttitude.Datetime;
        Tide.Value    = DataAttitude.Tide;
    else
        Tide = [];
        flagTideCorrection = 0;
    end
    
    if isfield(DataAttitude, 'Draught') && ~isempty(DataAttitude.Draught)
%         Draught.Time     = DataAttitude.Time;
        Draught.Datetime = DataAttitude.Datetime;
        Draught.Value    = DataAttitude.Draught;
    else
        Draught = [];
        flagDraughtCorrection = 0;
    end
end

%% Lecture des datagrams soundSpeedProfile

[~, DataSSP] = SSc_ReadDatagrams(this.nomFic, 'Ssc_SoundSpeedProfile');

%% Lecture de la donnee "InstallationParameters"

[~, DataInstallationParameters, isDual] = read_installationParameters_bin(this, 'Memmapfile', 0);

%% Lecture et interpolation de la donnee "Position"

[flag, DataPosition] = read_position_bin(this, 'IndNavUnit', IndNavUnit, 'Memmapfile', 0); %1);
if ~flag || isempty(DataPosition.Latitude) || isempty(DataPosition.Longitude)
    n = length(DataDepth.Datetime);
    Latitude = NaN(1,n);
    Longitude = NaN(1,n);
elseif length(DataPosition.Latitude) == 1
    Latitude  = repmat(DataPosition.Latitude(1),  length(DataDepth.Datetime), 1);
    Longitude = repmat(DataPosition.Longitude(1), length(DataDepth.Datetime), 1);
else
    Latitude  = my_interp1(          DataPosition.Datetime, DataPosition.Latitude,  DataDepth.Datetime, 'linear', 'extrap');
    Longitude = my_interp1_longitude(DataPosition.Datetime, DataPosition.Longitude, DataDepth.Datetime, 'linear', 'extrap');
end
if isempty(DataPosition)
    Speed =[];
else
    Speed = my_interp1(DataPosition.Datetime, DataPosition.Speed(:), DataDepth.Datetime);
end

%% Lecture et interpolation de la donn�e "SurfaceSoundSpeed"

[~, DataSurfaceSoundSpeed] = SSc_ReadDatagrams(this.nomFic, 'Ssc_SurfaceSoundSpeed');
% if ~flag || isempty(DataSurfaceSoundSpeed) || isempty(DataSurfaceSoundSpeed.Datetime)
%     disp('Message pour JMA : Ici pas trouve de SurfaceSoundSpeed, on prend 1500. C''est pas top de faire ca')
%     SurfaceSoundSpeed = ones(length(DataDepth.Datetime), 1) * 1500;
if isempty(DataSurfaceSoundSpeed) || isempty(DataSurfaceSoundSpeed.Datetime)
    SurfaceSoundSpeed = DataDepth.SoundSpeed;
    %     disp('Message pour JMA : Ici pas trouve de SurfaceSoundSpeed, on prend 1500. C''est pas top de faire ca')
    %     SurfaceSoundSpeed = ones(length(DataDepth.Datetime), 1) * 1500;
else
    %     SurfaceSoundSpeed = my_interp1(DataSurfaceSoundSpeed.Datetime, DataSurfaceSoundSpeed.SurfaceSoundSpeed, DataDepth.Datetime, 'linear', 'extrap');
    SurfaceSoundSpeed = my_interp1(DataSurfaceSoundSpeed.Datetime, DataSurfaceSoundSpeed.SurfaceSoundSpeed, DataDepth.Datetime, 'nearest', 'extrap'); % Modif JMA le 27/01/2016 pour fichier SHOM 0001_20100324_123624_Macareux
    kdeb = find(~isnan(SurfaceSoundSpeed), 1, 'first');
    SurfaceSoundSpeed(1:kdeb-1) = SurfaceSoundSpeed(kdeb);
    kdeb = find(~isnan(SurfaceSoundSpeed), 1, 'last');
    SurfaceSoundSpeed(kdeb+1:end) = SurfaceSoundSpeed(kdeb);
end

% -------------------------------------------------------------------------
% TRAITEMENT PARTICULIER UTILE UNIQUEMENT POUR DES DONNEES RESON AU FORMAT
% SIMRAD : le datagram runtime n'existe pas

if isempty(DataRuntime.PingCounter)
    DataRuntime.Time     = DataDepth.Time;
    DataRuntime.Datetime = DataDepth.Datetime;
    DataRuntime.Mode     = ones(nbPings, 1);
end

%% Lecture et interpolation de la donnee "Mode"

Mode = my_interp1_Extrap_PreviousThenNext(DataRuntime.Datetime, DataRuntime.Mode, DataDepth.Datetime);
% Mode = Mode';
Mode = Mode(:);

% Traitement particulier necessit� par le fichier NIWA/TAN0105/ALL/line-006
indPremierNonNul = find(Mode ~= 0, 1);
if ~isempty(indPremierNonNul)
    Mode(1:indPremierNonNul) = Mode(indPremierNonNul);
end
subMode0 = find(Mode == 0);
if ~isempty(subMode0)
    if ~isdeployed
        my_warndlg(['Attention : Mode=0 trouv� dans le fichier ' NomImage ' '], 0, 'Tag', 'Mode0TrouveDansFichier');
    end
    Mode(subMode0) = 1;
end

%% Creation de l'instance cli_image

TagSynchroX = [NomImage ' - PingBeam'];

%% Selection interactive de la zone

[flag, IdentSonar] = SimradModel2IdentSonar(DataDepth.EmModel, 'isDual', isDual);
if ~flag
    c = [];
    return
end

%% Param�tres du sondeur definis pour le mode majoritaire

if ~any(Mode)
    Mode(:) = 1; % IMPOSE AUTORITAIREMENT (Cas des donn�es du NIWA)
end
ModeMajoritaire = nanmode(double(Mode));

% Important : ici, ce n'est pas la peine de tester si il faut passer le
% mode du sondeur en "Sonar.Mode_1" ou "Sonar.Mode_2" car on est dans
% import_PingBeam_All_V1 c.a.d dans le cas des sondeurs anciens donc il n'y a pas de
% "Single Swath" ou "Double Swath"
SonarDescription = cl_sounder('Sonar.Ident', IdentSonar, 'Sonar.Mode_1', ModeMajoritaire, ...
    'Sonar.SystemSerialNumber', DataDepth.SystemSerialNumber(1));
resolutionMajoritaire = get_RangeResol(SonarDescription, ModeMajoritaire);


InitialFileFormat = 'SimradAll';
GeometryType = cl_image.indGeometryType('PingBeam');
y = 1:nbPings;
YUnit = 'Ping';

%% Cas o� il n'y a pas de datagrammes d'attitude : on estime HeaveTx sur la donn�e

if isempty(HeaveTx)
    tmp = sprintf('HeaveTx is estimated on data because there is no Attitude datagrams found in the file%s', this.nomFic);
    my_warndlg(tmp, 0, 'Tag', 'Y''a pas DataAttitude');
    %              Height = -get(Z, 'Height');
    Height = NaN(1,nbPings);
    for k=1:nbPings
        Height(k) = mean(DataDepth.Depth(k,:), 'omitnan');
    end
    
    [ButterB,ButterA] = butter(2, 0.05);
    subNaN = find(isnan(Height));
    if ~isempty(subNaN)
        subNonNaN = find(~isnan(Height));
        Height(subNaN) = interp1(subNonNaN, Height(subNonNaN), subNaN);
    end
    if length(Height) > 2*length(ButterB)
        Heightfiltree = filtfilt(ButterB, ButterA, double(Height));
        %             figure; plot(Height, 'b'); grid on
        %             hold on; plot(Heightfiltree, 'r'); grid on
    else
        Heightfiltree = Height;
    end
    HeaveTx = Height - Heightfiltree;
end
HeaveTx = HeaveTx(:,:);

%% Lecture des infos concernant l'imagerie

[flag, DataSeabed] = SSc_ReadDatagrams(this.nomFic, 'Ssc_SeabedImage53h', 'Memmapfile', -1);
% [flag, DataSeabed] = SSc_ReadDatagrams(this.nomFic, 'Ssc_SeabedImage53h', 'Memmapfile', -3);
if ~flag
    DataSeabed = [];
    str1 = sprintf('Il n''y a pas de datagramme "SeabedImage" dans le fichier "%s".', this.nomFic);
    str2 = sprintf('There is no "SeabedImage" datagram in the file "%s".', this.nomFic);
    my_warndlg(Lang(str1,str2), 0, 'Tag', 'NoDatagramSeabedImageInThisFile');
    %     c = []; %% Comment� car des fichiers .all n'ont pas de datagrammes sebedimage
    %     return
end

%% Facteur de qualit� Ifremer

[~, DataQualityFactor] = SSc_ReadDatagrams(this.nomFic, 'Ssc_QualityFactorFromIfr', 'Memmapfile', -1);

%% Lecture des differents layers de l'image Caraibes et stockage dans l'instance cli_image

% if ~Mute
%     fprintf('Reading file : %s\n', NomImage);
% end

OrigineDepthRaw = ones(1, length(ListeLayers)); % 1=DataDepth, 2=DataRaw
for k1=1:length(ListeLayers)
    nomLayer = ListeNomLayers{ListeLayers(k1)};

    switch nomLayer
        case {'Detection'
                'DetecAmplitudeNbSamples'
                'DetecPhaseOrdrePoly'
                'DetecPhaseVariance'
                'MasqueDetection'
                'ProbaDetection'}
            nomLayerAll = 'QualityFactor';
        case {'MasqueDepth'; 'ProbaDepth'}
            nomLayerAll = 'Depth';
        case {'LongueurTrajet'; 'Temps universel'; 'Roll'; 'Pitch'; 'Heave'; 'Heading'; 'Depth+Draught-Heave'; 'RayPathTravelTime'; 'Speed'}
            nomLayerAll = 'Range';
        case {'EmissionBeam'; 'RxBeamAngle'; 'TxAngleKjell'}
            nomLayerAll = 'BeamDepressionAngle';
        case 'BeamAngles/Earth'
            nomLayerAll = 'BeamDepressionAngle';
        case 'CentralFrequency'
            nomLayerAll = 'BeamDepressionAngle';
        case 'Raw-TxTiltAngle'
            nomLayerAll = 'Raw-TransmitTiltAngle';
        otherwise
            nomLayerAll = nomLayer;
    end
    
    if isempty(subl)
        if isempty(DataQualityFactor)
            [sublDepth, sublRaw] = intersect3(DataDepth.PingCounter(:,1), DataRaw.PingCounter(:,1), DataRaw.PingCounter(:,1));
        else
            [sublDepth, sublRaw, sublQF] = intersect3(DataDepth.PingCounter(:,1), DataRaw.PingCounter(:,1), DataQualityFactor.PingCounter(:,1));
        end
    else
        if isempty(DataQualityFactor)
            [sublDepth, sublRaw] = intersect3(DataDepth.PingCounter(subl,1), DataRaw.PingCounter(:,1), DataRaw.PingCounter(:,1));
        else
            [sublDepth, sublRaw, sublQF] = intersect3(DataDepth.PingCounter(subl,1), DataRaw.PingCounter(:,1), DataQualityFactor.PingCounter(:,1));
        end
        sublDepth = subl(sublDepth);
    end

    %% Calcul du vecteur Tide, Draught
    
    if isempty(Tide)
        TideValue = [];
    else
        TideValue = my_interp1(Tide.Datetime, Tide.Value, DataDepth.Datetime(sublDepth,1));
    end
    
    if isempty(Draught)
        DraughtValue = [];
    else
        DraughtValue = my_interp1(Draught.Datetime, Draught.Value, DataDepth.Datetime(sublDepth,1));
    end
    
    %%
    
    TsubDepth = DataDepth.Datetime(sublDepth);
    
    %% Cr�ation des signaux provenant des datagrammes RawRangeAndBeamAngle
    
    [~, signalContainerFromRawRange, flagNoSignalLength] = create_signalsFromRawRange(this, DataInstallationParameters, ...
        'DataRaw', DataRaw, 'subRaw', sublRaw, 'Mode', Mode);
    
    %% Cr�ation des signaux provenant des datagrammes runtime
    
    [~, signalContainerFromRuntime] = create_signalsFromRuntime(this, TsubDepth, 'DataRuntime', DataRuntime, ...
        'CreateSignalLength', flagNoSignalLength);
    
    %% Cr�ation des signaux provenant des datagrammes Attitude
    
    sectorTransmitDelaySignal = signalContainerFromRawRange.getSignalByTag('SectorTransmitDelay');
    if isempty(sectorTransmitDelaySignal)
        sectorTransmitDelayYSampleList = [];
    else
        sectorTransmitDelayYSampleList = sectorTransmitDelaySignal.ySample;
    end
    [~, signalContainerFromAttitude] = create_signalsFromAttitude(this, TsubDepth, sectorTransmitDelayYSampleList, 'DataAttitude', DataAttitude);
    
    %%
    
    if isempty(subc)
        if (ListeLayers(k1) > 13) && (ListeLayers(k1) < 18)
            nomLayerRaw = nomLayerAll(5:end);
            subc = 1:size(DataRaw.(nomLayerRaw),2);
        else
            subc = 1:size(DataDepth.Depth,2);
        end
    end
    
    if k1 == 1
        Time            =  DataDepth.Time(sublDepth, 1);
        Datetime        =  DataDepth.Datetime(sublDepth, 1);
        Heading         =  DataDepth.Heading(sublDepth, 1);
        TransducerDepth = -DataDepth.TransducerDepth(:,:);
        % TransducerDepth int�gre le HeaveTx dans les datagrams Simrad. On retire
        % l'influence du HeaveTx. On devrait obtenir une constante.
        
        %     HeaveTx = reshape(HeaveTx,size(TransducerDepth));
        if all(isnan(HeaveTx))
            HeaveTx(:) = 0;
        end
        
        TransducerDepth = bsxfun(@plus, TransducerDepth(sublDepth,:), HeaveTx(sublDepth,:));
        PingCounter     = DataDepth.PingCounter(sublDepth,1);
        SampleFrequency = DataDepth.SamplingRate(sublDepth,:);

        [TwoWayTravelTime_s, TwoWayTravelTimeInSamples, TxDelay_PingBeam_s] = get_TwoWayTravelTimeInSeconds(A0, DatagramVersion, DataRaw, DataDepth, sublRaw, sublDepth);
        HeaveRx = get_HeaveRx(this, DataAttitude, Datetime, TwoWayTravelTime_s(:, subc), TxDelay_PingBeam_s(:,subc));
        
        clear HeaveTx
        heaveTxSignal = signalContainerFromAttitude.getSignalByTag('HeaveTx');
        for k=length(heaveTxSignal.ySample):-1:1
            HeaveTx(:,k) = heaveTxSignal.ySample(k).data;
        end
        if isfield(DataRaw, 'TransmitSectorNumber')
            HeaveTx_PingBeam = get_PingBeamMatrixFromPingSignal(HeaveTx, DataRaw.TransmitSectorNumber(sublRaw,:));
            Heave = (HeaveRx + HeaveTx_PingBeam) / 2;
        else
            Heave = HeaveRx;
        end
    end
    
    %% Lecture de l'image
    
    if ~Mute && (get_LevelQuestion >= 3)
        fprintf('Reading file : %s - Layer : %s\n', NomImage, nomLayer);
    end
    
    BathymetryStatus = get_BathymetryStatus(I0);
    
    switch ListeLayers(k1)
        case 6
            I = TwoWayTravelTimeInSamples;
            
        case 25
            I = TwoWayTravelTime_s(:,subc);
            
        case {1;2;3;7;8;9;10;11;12;13;28;29} % Modif JMA le 13/02/2016
            if ~isfield(DataDepth, nomLayerAll)
                message_LayerNotAvailable(nomLayerAll, 0);
                continue
            end
            I = DataDepth.(nomLayerAll)(sublDepth,subc);
            
        case 4 % Pour obtenir BeamPointingAngleVersusHull
            I = DataRaw.BeamPointingAngle(sublRaw,subc); % New Modif JMA 13/02/2016
            I = Simrad_AnglesAntenna2ShipFloor(I, DataInstallationParameters, isDual);
            
        case 5 % Azimuth % Ajout JMA le 24/03/2016
            if ~isfield(DataDepth, nomLayerAll)
                message_LayerNotAvailable(nomLayerAll, 0);
                continue
            end
            I = DataDepth.(nomLayerAll)(sublDepth,subc);
            sub = (I(:,:) > 180);
            I(sub) = I(sub) - 360;
            
        case 36 % BeamAngles/Earth
            RollRx = get_RollRx(A0, DataAttitude, Datetime, TwoWayTravelTime_s(:, subc), TxDelay_PingBeam_s(:,subc));
            I = DataRaw.BeamPointingAngle(sublRaw,subc); % New Modif JMA 13/02/2016
            I = Simrad_AnglesAntenna2ShipFloor(I, DataInstallationParameters, isDual); % Pas retenu car le calcul de la bathy n'est pas bon
            I = bsxfun(@minus,I, RollRx); % New Modif JMA 13/02/2016
            
        case 48 % TxAngleKjell
            I = DataRaw.BeamPointingAngle(sublRaw,subc); % New Modif JMA 13/02/2016
            if isfield(DataRaw, 'TransmitSectorNumber')
                RollRx_PingBeam = get_RollRx(A0, DataAttitude, Datetime, TwoWayTravelTime_s(:,subc), TxDelay_PingBeam_s(:,subc));
                rollTxSignal = signalContainerFromAttitude.getSignalByTag('RollTx');
                for k=length(rollTxSignal.ySample):-1:1
                    RollTxVal(:,k) = rollTxSignal.ySample(k).data;
                end
                RollTx_PingBeam = get_PingBeamMatrixFromPingSignal(RollTxVal, DataRaw.TransmitSectorNumber(sublRaw,:));
                Correctif = RollRx_PingBeam - RollTx_PingBeam;
                I = I + Correctif;
            end
            
        case 37 % Mean Sound Speed in the Water Column
            I = sonar_depth2soundSpeed(DataDepth.Depth(sublDepth,subc), DataDepth.Datetime(sublDepth), DataSSP);
            
        case 35
            if isempty(DataQualityFactor)
                message_LayerNotAvailable(nomLayerAll, 0);
                continue
            end
            I = DataQualityFactor.(nomLayerAll)(sublQF,subc);
            
        case {27; 50; 51} % Reflectivity from snippets
            if ~isfield(DataDepth, nomLayerAll)
                message_LayerNotAvailable(nomLayerAll, 0);
                continue
            end
            I = DataDepth.(nomLayerAll)(sublDepth,subc);
            
            if isfield(DataDepth, 'MaskPingsBathy') && ~isempty(DataDepth.MaskPingsBathy)
                for k2=1:length(DataDepth.MaskPingsBathy)
                    if DataDepth.MaskPingsBathy(k2) == 1
                        I(kPing,:) = NaN;
                    end
                end
            end
            
            if isfield(DataDepth, 'MaskPingsReflectivity') && ~isempty(DataDepth.MaskPingsReflectivity)
                DataDepth.MaskPingsReflectivity = DataDepth.MaskPingsReflectivity(:,:);
                for k2=1:length(DataDepth.MaskPingsReflectivity)
                    if DataDepth.MaskPingsReflectivity(k2) == 1
                        I(k2,:) = NaN;
                    end
                end
            end
            
        case {14;15;16;17}
            nomLayerRaw = nomLayerAll(5:end);
            
            % Test introduit car bug fichier SHOM EM1002/St-Brieuc : U:\augustin\SHOM\20110420_02_LRB_B5_B6_B10_EOL_trav\0040_20110420_172449_lpo.all
            if DataRaw.Dimensions.nbRx ~= DataDepth.Dimensions.nbBeams
                str1 = sprintf('Le fichier "%s" est probl�matique, le nombre de faisceaux entre "depth datagrams" et "raw-range and beam angle datagrams" est diff�rent.', this.nomFic);
                str2 = sprintf('"%s" is dammaged, the number of beams between "depth datagrams" and "raw-range and beam angle datagrams" are different.', this.nomFic);
                my_warndlg(Lang(str1,str2), 0);
                c = [];
                return
            end
            I = DataRaw.(nomLayerRaw)(sublRaw,subc);
            if ListeLayers(k1) == 14
                I = Simrad_AnglesAntenna2ShipFloor(I, DataInstallationParameters, isDual);
            end
            OrigineDepthRaw(k1) = 2;
            
        case 18
            I = get_RayPathLength(DatagramVersion, DataRaw, DataDepth, sublDepth, sublRaw, subc); % One way length in meters
            
        case 19
            I = get_DatetimeRx(A0, TwoWayTravelTime_s(:, subc), TxDelay_PingBeam_s(:,subc), Datetime);
            I = datenum(I); % TODO : int�grer datetime dans la visu SSc
            
        case 20
            I = get_RollRx(A0, DataAttitude, Datetime, TwoWayTravelTime_s(:, subc), TxDelay_PingBeam_s(:,subc));
            
        case 21
            I = get_PitchRx(A0, DataAttitude, Datetime, TwoWayTravelTime_s(:, subc), TxDelay_PingBeam_s(:,subc));
            
        case 22
            I = get_HeaveRx(A0, DataAttitude, Datetime, TwoWayTravelTime_s(:, subc), TxDelay_PingBeam_s(:,subc));
            
        case 23
            I = get_HeadingRx(A0, DataAttitude, Datetime, TwoWayTravelTime_s(:, subc), TxDelay_PingBeam_s(:,subc));
            
        case 34
            % TODO : faire un get_xxxx
            DatetimeRx = get_DatetimeRx(A0, TwoWayTravelTime_s(:, subc), TxDelay_PingBeam_s(:,subc), Datetime);
            if all(isnan(DataPosition.Speed(:)))
                continue
            else
                I = my_interp1(DataPosition.Datetime, DataPosition.Speed, DatetimeRx);
            end
            
        case 24
            I = DataDepth.Depth(sublDepth,subc);
            
            if flagTideCorrection && all(isnan(TideValue(sublDepth)))
                str1 = sprintf('La mar�e n''est pas d�finie pour %s.\nLa valeur 0 est impos�e ici.', ImageName);
                str2 = sprintf('The tide value is not defined for %s.\nIt is set to 0 here', ImageName);
                my_warndlg(Lang(str1,str2), 0, 'displayItEvenIfSSc', 1, 'TimeDelay', 60)
                TideValue(:) = 0;
            end
            
            if flagDraughtCorrection && all(isnan(DraughtValue(sublDepth)))
                str1 = sprintf('Draught n''est pas d�fini pour %s.\nLa valeur 0 est impos�e ici.', ImageName);
                str2 = sprintf('The Draught value is not defined for %s.\nIt is set to 0 here.', ImageName);
                my_warndlg(Lang(str1,str2), 0, 'displayItEvenIfSSc', 1)
                DraughtValue(:) = 0;
            end
            
            for iPing=1:length(sublDepth)
                % TODO : attention danger ici pour l'EM3002D, il faut
                % distinguer b�bord et tribord TransducerDepth(:,1 et 2)
                I(iPing,:) = I(iPing,:) + (TransducerDepth(iPing,1) - Heave(iPing,:)); % Modifi� par JMA le 20/08/2015 apr�s refonte des imports de la mar�e
                
                if flagTideCorrection && ~isempty(TideValue)
                    I(iPing,:) = I(iPing,:) + TideValue(iPing);
                end
                if flagDraughtCorrection
                    I(iPing,:) = I(iPing,:) + DraughtValue(iPing);
                end
            end
            BathymetryStatus.TideApplied = flagTideCorrection;
            
        case 26
            I = DataDepth.BeamDepressionAngle(sublDepth,subc);
            
        case 39
            I = DataDepth.(nomLayerAll)(sublDepth,subc);
            
        case {30; 31; 32; 33}
            if ~isempty(DataSeabed)
                [subSeabed, sublSeabedImage] = intersect3(DataSeabed.PingCounter(:,1), DataDepth.PingCounter(sublDepth,1), DataDepth.PingCounter(sublDepth,1));
                I = NaN(length(sublDepth), length(subc), 'single');
                I(sublSeabedImage,:) = DataSeabed.(nomLayerAll)(subSeabed,subc);
            end
            
        case 40 % AbsorptionCoefficientRT
            % TODO : isfield pour �tre compatible avec les anciens fichiers
            % (r�alis�s avant introduction des layers d'absorption)
            if ~isfield(DataDepth, nomLayerAll)
                message_LayerNotAvailable(nomLayerAll, 1);
                continue
            end
            I = DataDepth.(nomLayerAll)(sublDepth,subc);
            
        case 41 % AbsorptionCoefficientSSc
            % TODO : isfield pour �tre compatible avec les anciens fichiers
            % (r�alis�s avant introduction des layers d'absorption)
            if ~isfield(DataDepth, nomLayerAll)
                message_LayerNotAvailable(nomLayerAll, 1);
                continue
            end
            I = DataDepth.(nomLayerAll)(sublDepth,subc);
            
        case 42 % AbsorptionCoefficientUser
            % TODO : isfield pour �tre compatible avec les anciens fichiers
            % (r�alis�s avant introduction des layers d'absorption)
            if ~isfield(DataDepth, nomLayerAll)
                message_LayerNotAvailable(nomLayerAll, 1);
                continue
            end
            if isempty(DataDepth.(nomLayerAll))
                message_LayerNotAvailable(nomLayerAll, 0);
                continue
            end
            I = DataDepth.(nomLayerAll)(sublDepth,subc);
            
        case {43; 44; 45; 46; 47; 52} % TODO 47
            if ~isfield(DataDepth, nomLayerAll)
                message_LayerNotAvailable(nomLayerAll, 1);
                continue
            end
            flag = ALL_CacheIsLayerPopulated(this.nomFic, DataDepth.Images, nomLayerAll);
            if ~flag
                continue
            end
            if isempty(DataDepth.(nomLayerAll))
                message_LayerNotAvailable(nomLayerAll, 0);
                continue
            end
            I = DataDepth.(nomLayerAll)(sublDepth,subc);
            
        case 49 % TODO : cr�er plut�t un signal horizontal et cr�er le layer � partir de ce signal
            ns2 = floor(DataDepth.Dimensions.nbBeams / 2);
            subBab = 1:ns2;
            subTri = (ns2+1):DataDepth.Dimensions.nbBeams;
            I(1:length(sublDepth),subBab) = 1;
            I(:,subTri) = 2;
            I = I(:,subc);
            
        case 38
             I = zeros(length(sublDepth), length(subc), 'single') + Mode(sublDepth);
            
        case 53
            pitchTxSignal = signalContainerFromAttitude.getSignalByTag('PitchTx');
            for k2=length(pitchTxSignal.ySample):-1:1
                PitchTxVal(:,k2) = pitchTxSignal.ySample(k2).data;
            end
            PitchTx_PingBeam = get_PingBeamMatrixFromPingSignal(PitchTxVal, DataRaw.TransmitSectorNumber(sublRaw,:));
            
            if isfield(DataInstallationParameters, 'S0P') % DualTX voir note 19 table  "D:\perso-jma\Documents\Kongsberg\160692av_em_datagram_formats.pdf"
                n = DataRaw.Dimensions.nbRx / 2;
                AngleMontageAlong = [repmat(DataInstallationParameters.S1P, n, 1) repmat(DataInstallationParameters.S2P, n, 1)];
                AngleMontageAlong = AngleMontageAlong(subc);
            else
                AngleMontageAlong = DataInstallationParameters.S1P;
            end
            
            I = DataRaw.TransmitSectorNumber(sublRaw,subc);
            K = DataRaw.TransmitSectorNumber(sublRaw,subc);
            for kRaw=1:max(K(:))
                I(K == kRaw) = DataRaw.TiltAngle(kRaw+1);
            end
            I = I + PitchTx_PingBeam + AngleMontageAlong;
            ImageName = NomImage;
           
        otherwise
            messageForDebugInspection
            continue
    end
    
    ValNaN = NaN;
    switch nomLayer
        case 'Detection'
            I = single((I >= 128)) + 1;
            
            % CARAIBES : Verrue en attendant que la lecture du layer rende
            % NaN
            subNaN = isnan(DataDepth.Depth(sublDepth,subc));
            I(subNaN) = NaN;
        case 'DetecAmplitudeNbSamples'
            I(I >= 128) = NaN;
        case 'DetecPhaseOrdrePoly'
            subNan  = (I < 128);
            I = single((I < 192)) + 1;
            I(subNan) = NaN;
        case 'DetecPhaseVariance'
            subNan  = (I < 128);
            I(isnan(I)) = 0;
            I = single(bitand(uint8(I),63));
            I(subNan) = NaN;
        case 'Mask'
            ValNaN = 255;
    end
    
    %% Mask
    
    if isa(I, 'single') || isa(I, 'double')
        Mask = DataDepth.Mask(sublDepth,:);
        Mask = (Mask ~= 0);
        I(Mask) = NaN;
    else
        % TODO
    end
    
    %% Creation de l'instance cl_image
    
    b = cl_image('Image', I);
    b.Name              = NomImage;
    b.InitialImageName  = nomLayer;
    b.Unit              = Unit{ListeLayers(k1)};
    b.TagSynchroX       = TagSynchroX;
    b.TagSynchroY       = TagSynchroY;
    b.x                 = subc;
    b.y                 = y(sublDepth);
    b.XUnit             = 'Beam #';
    b.YUnit             = YUnit;
    b.InitialFileName   = this.nomFic;
    b.InitialFileFormat = InitialFileFormat;
    b.GeometryType      = GeometryType;
    b = set_SonarDescription(b, SonarDescription);
    b.Comments          = Comments{ListeLayers(k1)};
    b.ValNaN            = ValNaN;
    
    [DataType, ColormapIndex] = ALL.identSonarDataType(ListeNomLayers{ListeLayers(k1)});
    b.DataType = DataType;
    b.ColormapIndex = ColormapIndex;
    
    %% D�finition d'une carto
    
    b = set_SonarFishLongitude(b, Longitude(sublDepth));
    b = set_SonarFishLatitude( b, Latitude(sublDepth));
    [b, Carto, persistent_Carto] = define_carto(b, Carto, nomDir, memeReponses, persistent_Carto, 'CartoAuto', CartoAuto);
    b = set_Carto(b, Carto);
        
    %% D�termination du signal "Distance Inter Pings"
    
    [~, DIP] = create_signal_InterPingDistance(this, 'DataPosition', DataPosition, 'DataDepth', DataDepth, ...
        'Carto', Carto);
    DistanceInterPings = DIP.Data;
    
    %% D�termination du signal "Height"
    
    [~, Z] = create_signal_height(this, 'DataDepth', DataDepth);
    SonarHeight = Z.Data;
    X1 = SonarHeight(sublDepth);
    X2 = TransducerDepth(:,1);
    b = set_SonarHeight(b, X1(:) + X2(:)); % TODO : attention danger pour l'EM3002D
    
    b = set_BathymetryStatus(b, BathymetryStatus);
    
    %% Valeur du coefficient d'absorption
    
    [flag, signalContainerAbsorptionCoeff_RT] = create_signal_AbsorptionCoeff_RT(this, ...
        'DataDepth', DataDepth, 'DataSeabed', DataSeabed, ...
        'DataRaw', DataRaw, 'DataRuntime', DataRuntime, 'subDepth', sublDepth);
    if flag
        AbsorptionCoeff_RT = signalContainerAbsorptionCoeff_RT.signalList.ySample.data;
    else
        AbsorptionCoeff_RT = [];
    end
    
    b = set_SonarTime(              b, Time);
    b = set_SonarHeading(           b, Heading);
    b = set_SonarSpeed(             b, Speed);
    %     b = set_SonarImmersion(         b, TransducerDepth(:,1));
    b = set_SonarImmersion(         b, TransducerDepth(:,:)); % Modifi� le 26/01/2016 pour l'EM3002D
    b = set_SonarSurfaceSoundSpeed( b, SurfaceSoundSpeed(sublDepth)); % ATTENTION, donn� en horizontal : faire un point complet pour non regression avant de l'imposer en vertical
    b = set_SonarPortMode_1(        b, Mode(sublDepth)); % Idem SurfaceSoundSpeed
    b = set_SonarSampleFrequency(   b, SampleFrequency(:,:));
    
    %% Suite
    
    b = set_SonarRawDataResol(b, resolutionMajoritaire);
    b = set_SonarResolutionD( b, resolutionMajoritaire);
    b = set(b, 'SonarDistPings', DistanceInterPings);
    
    if ~isempty(RollTx)
        b = set_SonarRoll( b, RollTx(sublDepth));
        b = set_SonarPitch(b, Pitch(sublDepth));
        %     b = set(b, 'SonarYaw',   Yaw);
    end
    
    %     b = set_SonarHeave(b, HeaveTx(sublDepth,1));
    b = set_SonarHeave(b, HeaveTx(:,:)); % Modif JMA le 12/02/2018
    
    if ~isempty(TideValue)
        b = set_SonarTide(b, TideValue);
    end
    if ~isempty(DraughtValue)
        b = set_SonarDraught(b, DraughtValue);
    end
    % TODO if flagTideCorrection : mettre une info du type
    % c(k) = set_SonarTideCorrection(c(k), 1), idem pour draught et TrueHeave
    
    b = set_SonarPingCounter(  b, PingCounter(:,1));
    
    %% Vertical signals from runtime datagrams
    
    b = set_SignalsVert(b, [signalContainerFromRuntime signalContainerFromRawRange signalContainerFromAttitude signalContainerAbsorptionCoeff_RT]);
    
    %% Transfert des parametres d'�tat de la donn�e
    
    switch nomLayer
        case 'Reflectivity'
            EtatSonar = init_BSStatus('EmModel', DataDepth.EmModel, 'Absorption', AbsorptionCoeff_RT, 'SonarBS_etat', 2, ...
                'IdentAlgoSnippets2OneValuePerBeam', 2);
            b = set_BSStatus(b, EtatSonar);
            [subSeabed, sublSeabedImage] = intersect3(DataSeabed.PingCounter(:,1), DataDepth.PingCounter(sublDepth,1), DataDepth.PingCounter(sublDepth,1));
            SonarRn             = NaN(length(sublDepth), DataSeabed.Dimensions.nbSounders, 'single');
            SonarBSN            = NaN(length(sublDepth), DataSeabed.Dimensions.nbSounders, 'single');
            SonarBSO            = NaN(length(sublDepth), DataSeabed.Dimensions.nbSounders, 'single');
            SonarTxBeamWidth    = NaN(length(sublDepth), DataSeabed.Dimensions.nbSounders, 'single');
            SonarTVGCrossOver   = NaN(length(sublDepth), DataSeabed.Dimensions.nbSounders, 'single');
            SonarRn(sublSeabedImage,:)           = DataSeabed.TVGN(subSeabed,:);
            SonarBSN(sublSeabedImage,:)          = DataSeabed.BSN(subSeabed,:);
            SonarBSO(sublSeabedImage,:)          = DataSeabed.BSO(subSeabed,:);
            SonarTxBeamWidth(sublSeabedImage,:)  = DataSeabed.TxBeamWidth(subSeabed,:);
            SonarTVGCrossOver(sublSeabedImage,:) = DataSeabed.TVGCrossOver(subSeabed,:);
            b = set_SonarTVGN(b,         SonarRn);
            b = set_SonarBSN(b,          SonarBSN);
            b = set_SonarBSO(b,          SonarBSO);
            b = set_SonarTxBeamWidth(b,  SonarTxBeamWidth);
            b = set_SonarTVGCrossOver(b, SonarTVGCrossOver);
            
        case {'Raw-Range'; 'TwoWayTravelTimeInSeconds'; 'TwoWayTravelTime_s'}
            if ~isempty(DataSeabed)
                [subSeabed, sublSeabedImage] = intersect3(DataSeabed.PingCounter(:,1), DataDepth.PingCounter(sublDepth,1), DataDepth.PingCounter(sublDepth,1));
                SonarRn = NaN(length(sublDepth), DataSeabed.Dimensions.nbSounders, 'single');
                SonarRn(sublSeabedImage,:) = DataSeabed.TVGN(subSeabed,:);
                b = set_SonarTVGN(b, SonarRn);
            end
            
        case {'ReflectivityFromSnippets'; 'ReflectivityFromSnippetsWithoutSpecularRestablishment'}
            if ~isempty(DataSeabed)
                [subSeabed, sublSeabedImage] = intersect3(DataSeabed.PingCounter(:,1), DataDepth.PingCounter(sublDepth,1), DataDepth.PingCounter(sublDepth,1));
                SonarRn             = NaN(length(sublDepth), DataSeabed.Dimensions.nbSounders, 'single');
                SonarBSN            = NaN(length(sublDepth), DataSeabed.Dimensions.nbSounders, 'single');
                SonarBSO            = NaN(length(sublDepth), DataSeabed.Dimensions.nbSounders, 'single');
                SonarTxBeamWidth    = NaN(length(sublDepth), DataSeabed.Dimensions.nbSounders, 'single');
                SonarTVGCrossOver   = NaN(length(sublDepth), DataSeabed.Dimensions.nbSounders, 'single');
                SonarRn(sublSeabedImage,:)           = DataSeabed.TVGN(subSeabed,:);
                SonarBSN(sublSeabedImage,:)          = DataSeabed.BSN(subSeabed,:);
                SonarBSO(sublSeabedImage,:)          = DataSeabed.BSO(subSeabed,:);
                SonarTxBeamWidth(sublSeabedImage,:)  = DataSeabed.TxBeamWidth(subSeabed,:);
                SonarTVGCrossOver(sublSeabedImage,:) = DataSeabed.TVGCrossOver(subSeabed,:);
                b = set_SonarTVGN(b,         SonarRn);
                b = set_SonarBSN(b,          SonarBSN);
                b = set_SonarBSO(b,          SonarBSO);
                b = set_SonarTxBeamWidth(b,  SonarTxBeamWidth);
                b = set_SonarTVGCrossOver(b, SonarTVGCrossOver);
            end
            
            %% Transfert des parametres d'�tat de la donn�e
            
            InitialImageName = b.InitialImageName;
            switch InitialImageName
                case 'Reflectivity'
                    EtatSonar = init_BSStatus('EmModel', DataDepth.EmModel, ...
                        'Absorption', AbsorptionCoeff_RT, ...
                        'SonarBS_etat', 2, ...
                        'IdentAlgoSnippets2OneValuePerBeam', 2);
                case {'ReflectivityFromSnippets'; 'ReflectivityFromSnippetsWithoutSpecularRestablishment'}
                    if strcmp(InitialImageName, 'ReflectivityFromSnippets')
                        SonarBS_SpecularReestablishment = 1;
                    else
                        SonarBS_SpecularReestablishment = 0;
                    end
                    if isfield(DataDepth, 'IdentAlgoSnippets2OneValuePerBeam')
                        IdentAlgoSnippets2OneValuePerBeam = DataDepth.IdentAlgoSnippets2OneValuePerBeam;
                    else
                        IdentAlgoSnippets2OneValuePerBeam = 3; % Energy : C'est comme cela que �a a �t� calcul� pr�c�demment
                    end
                    EtatSonar = init_BSStatus('EmModel', DataDepth.EmModel, ...
                        'Absorption', AbsorptionCoeff_RT, ...
                        'SonarBS_etat', 1, 'SonarBS_LambertCorrection', 1, 'SonarAireInso_origine', 1, ...
                        'IdentAlgoSnippets2OneValuePerBeam', IdentAlgoSnippets2OneValuePerBeam, ...
                        'SonarBS_SpecularReestablishment', SonarBS_SpecularReestablishment);
                otherwise
                    EtatSonar = init_BSStatus('EmModel', DataDepth.EmModel, 'Absorption', AbsorptionCoeff_RT, 'SonarBS_etat', 2);
            end
            b = set_BSStatus(b, EtatSonar);
        otherwise
            % Devrait �tre supprim� : � tester avec la R2009b
            EtatSonar = init_BSStatus('EmModel', DataDepth.EmModel, 'Absorption', AbsorptionCoeff_RT, 'SonarBS_etat', 1);
            b = set_BSStatus(b, EtatSonar); % Devrait �tre supprim� : � tester avec la R2009b
    end
    
    if ~isempty(DataInstallationParameters) && ~isempty(DataInstallationParameters.Datetime)
        b = set_SonarInstallationParameters(b, DataInstallationParameters);
    end
    
    %% Profil de c�l�rit�
    
    if ~isempty(DataSSP)
        b = set_SonarBathyCel(b, DataSSP);
    end
    
    %% Cas particuliers de layers
    
    switch ListeLayers(k1)
        case 26
            [DataType, ColormapIndex] = ALL.identSonarDataType('BeamDepressionAngle');
            b.DataType = DataType;
            b.ColormapIndex = ColormapIndex;
            b = sonar_secteur_emission(b, []); % Modifi� le 27/02/2010 pour fichier NIWA 0001_20081011_084329_raw.all

        case 39
            [DataType, ColormapIndex] = ALL.identSonarDataType('BeamDepressionAngle');
            b.DataType = DataType;
            b.ColormapIndex = ColormapIndex;
            [b, Frequency] = sonar_secteur_emission(b, []); % Modifi� le 27/02/2010 pour fichier NIWA 0001_20081011_084329_raw.all
            if isfield(DataRaw, 'CentralFrequency')
                Frequency = DataRaw.CentralFrequency(sublRaw,:);
                b = sonar_V1_CentralFrequency(b, Frequency);
            else
                %             b = secteur_frequency(b); % Ajout� le 14/02/2017 pour donn�es EM1002 de Marc Roche
                b = Frequency; % Modifi� par JMA le 30/10/2018 pour donn�es EM300
            end

        case 53
            [DataType, ColormapIndex] = ALL.identSonarDataType('SonarAlongBeamAngle');
            b.DataType = DataType;
            b.ColormapIndex = ColormapIndex;
    end
    
    if strcmp(ListeNomLayers{ListeLayers(k1)}, 'Mask')
        DataType = ALL.identSonarDataType('Mask');
        b.DataType = DataType;
        b = compute_stats(b);
    end
    
    %% D�finition du nom de l'image
    
    if strcmp(ListeNomLayers{ListeLayers(k1)}, 'Depth+Draught-Heave')
        ImageName = [NomImage '_Depth-Heave'];
        if flagTideCorrection
            ImageName = [ImageName '+Tide']; %#ok<AGROW>
        end
        
        if flagDraughtCorrection
            ImageName = [ImageName '+Draught']; %#ok<AGROW>
        end
        b.Name = ImageName;
    end
    
    % TODO : trouver un moyen �l�gant de d�livrer un Tag qui compl�te le
    % DataType (DataTypeTag ?)
    switch ListeNomLayers{ListeLayers(k1)}
        case 'ReflectivityFromSnippets'
            b.Name = [NomImage '_FromSnippets'];
        case 'ReflectivityFromSnippetsWithoutSpecularRestablishment'
            b.Name = [NomImage '_FromSnippetsWSR'];
        case 'AbsorptionCoefficientRT'
            b.Name = [NomImage '_RT'];
        case 'AbsorptionCoefficientSSc'
            b.Name = [NomImage '_SSc'];
        case 'AbsorptionCoefficientUser'
            b.Name = [NomImage '_User'];
        case 'InsonifiedAreaSSc'
            b.Name = [NomImage '_SSc'];
        otherwise
            % On ne change pas le nom
    end
    b = update_Name(b);
    
    %% Ajout de l'image dans l'instance clc_image
    
    c(end+1) = b; %#ok<AGROW>
end

%% Edition de l'objet cli_image

if nargout == 0
    editobj(c);
end


% TODO : pas terrible cette fonction : celerit� please !!!
% Il faudrait tout simplement ne pas utiliser ce layer car il peut engendrer pas mal de probl�mes
function I = get_RayPathLength(DatagramVersion, DataRaw, DataDepth, sublDepth, sublRaw, subc)
A0 = cl_simrad_all([]);
TwoWayTravelTime_s = get_TwoWayTravelTimeInSeconds(A0, DatagramVersion, DataRaw, DataDepth, sublDepth, sublRaw);
I = TwoWayTravelTime_s(:, subc) * (1500 / 2); % TODO : Profil de c�l�rite SVP !
