% Visualisation du contenu des datagrams "depth" contenus dans un fichier .all
%
% Syntax
%   [b, Carto] = import_PingBeam_All_V2(aKM)
%
% Input Arguments
%   aKM : Instance de cl_simrad_all
%
% Name-Value Pair Arguments
%   subl         : Numeros de lignes � traiter
%   subc         : Numeros de faisceaux � traiter
%   TagSynchroY  : Tag de synchronisation en Y
%   ListeLayers  : Numeros des layers [] par defaut
%   Carto        : Parametres cartographiques ([] par defaut)
%   memeReponses : false=On pose les questions � chaque appel, true=On m�morise les r�ponses
%
% Output Arguments
%   b     : Instance de cli_image
%   Carto : Parametres cartographiques
%
% Examples
%   nomFic = getNomFicDatabase('0011_20110307_211804_IKATERE_02.all')
%   aKM = cl_simrad_all('nomFic', nomFic);
%   b = import_PingBeam_All_V2(aKM);
%   b = editobj(b);
%
% See also cl_simrad_all cl_simrad_all/plot_position cl_simrad_all/plot_nav Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [c, Carto, subDepth, nbPingsOut] = import_PingBeam_All_V2(this, varargin)

persistent persistent_ListeLayers persistent_Carto

[varargin, sublDepth]             = getPropertyValue(varargin, 'subl',              []);
[varargin, subc]                  = getPropertyValue(varargin, 'subc',              []);
[varargin, ListeLayers]           = getPropertyValue(varargin, 'ListeLayers',       []);
[varargin, Carto]                 = getPropertyValue(varargin, 'Carto',             []);
[varargin, CartoAuto]             = getPropertyValue(varargin, 'CartoAuto',         0);
[varargin, memeReponses]          = getPropertyValue(varargin, 'memeReponses',      false);
[varargin, MasqueActif]           = getPropertyValue(varargin, 'MasqueActif',       1);
[varargin, flagTideCorrection]    = getPropertyValue(varargin, 'TideCorrection',    0);
[varargin, flagDraughtCorrection] = getPropertyValue(varargin, 'DraughtCorrection', 0);
[varargin, Mute]                  = getPropertyValue(varargin, 'Mute',              0);
[varargin, IndNavUnit]            = getPropertyValue(varargin, 'IndNavUnit',        1);
[varargin, IndAttUnit]            = getPropertyValue(varargin, 'IndAttUnit',        1);
[varargin, DataDepth]             = getPropertyValue(varargin, 'DataDepth',         []);
[varargin, DataRaw]               = getPropertyValue(varargin, 'DataRaw',           []);
[varargin, DataSeabed]            = getPropertyValue(varargin, 'DataSeabed',        []);
[varargin, DataAttitude]          = getPropertyValue(varargin, 'DataAttitude',      []);
[varargin, DataRuntime]           = getPropertyValue(varargin, 'DataRuntime',       []);
[varargin, DataSSP]               = getPropertyValue(varargin, 'DataSSP',           []);
[varargin, DataInstalParam]       = getPropertyValue(varargin, 'DataInstalParam',   []);

subDepth = [];
c = cl_image;
c(:) = [];
nbPingsOut = [];

A0 = cl_simrad_all([]);

%% Check the number of elements

flag = checkIfOneElement(length(this));
if ~flag
    c = [];
    return
end

%%

[nomDir, NomImage] = fileparts(this.nomFic);
[varargin, TagSynchroY] = getPropertyValue(varargin, 'TagSynchroY', NomImage); %#ok<ASGLU>

I0 = cl_image_I0;

if isempty(ListeLayers)
    if memeReponses
        ListeLayers = persistent_ListeLayers;
        flagChoixInteractif = 0;
    else
        ListeLayers = Simrad_getDefaultLayersV2;
        persistent_ListeLayers = ListeLayers;
        flagChoixInteractif = 1;
    end
    
elseif isequal(ListeLayers, 'Default')
    ListeLayers = Simrad_getDefaultLayersV2;
    persistent_ListeLayers = ListeLayers;
    flagChoixInteractif = 0;
    
else
    flagChoixInteractif = 0;
end

if flagChoixInteractif
    titleDataSet{1,1} = 'From Depth datagram - XYZ 88';
    titleDataSet{2,1} = 'From Raw Range Beam Angles datagram';
    titleDataSet{3,1} = 'From Quality Factor datagram'; % OK
    titleDataSet{3,2} = 'From Seabed Image datagram';
    titleDataSet{1,2} = 'Deduced from Depth datagram - XYZ 88';
    titleDataSet{2,2} = 'Deduced from Raw Range Beam Angles datagram';
    
    ListeLayers2.Order = ListeLayers.Order;
    ListeLayers2.SelectionLayers      = ListeLayers.SelectionLayers(1:2,:);
    ListeLayers2.SelectionLayers(3,1) = ListeLayers.SelectionLayers(3,1);
    ListeLayers2.SelectionLayers(3,2) = ListeLayers.SelectionLayers(4,1);
    
    ListeLayers2.nomLayers      = ListeLayers.nomLayers(1:2,:);
    ListeLayers2.nomLayers(3,1) = ListeLayers.nomLayers(3,1);
    ListeLayers2.nomLayers(3,2) = ListeLayers.nomLayers(4,1);
    
    ListeLayers2.Units      = ListeLayers.Units(1:2,:);
    ListeLayers2.Units(3,1) = ListeLayers.Units(3,1);
    ListeLayers2.Units(3,2) = ListeLayers.Units(4,1);
    
    ListeLayers2.Comments      = ListeLayers.Comments(1:2,:);
    ListeLayers2.Comments(3,1) = ListeLayers.Comments(3,1);
    ListeLayers2.Comments(3,2) = ListeLayers.Comments(4,1);
    
    [flag, ListeLayers2] = uiSelectItemsFromTableSheet(ListeLayers2, 'titleDataSet', titleDataSet);
    if ~flag
        c = [];
        return
    end
    
    ListeLayers3.SelectionLayers      = ListeLayers2.SelectionLayers(1:2,:);
    ListeLayers3.SelectionLayers(3,1) = ListeLayers2.SelectionLayers(3,1);
    ListeLayers3.SelectionLayers(4,1) = ListeLayers2.SelectionLayers(3,2);
    
    ListeLayers3.nomLayers      = ListeLayers2.nomLayers(1:2,:);
    ListeLayers3.nomLayers(3,1) = ListeLayers2.nomLayers(3,1);
    ListeLayers3.nomLayers(4,1) = ListeLayers2.nomLayers(3,2);
    
    ListeLayers3.Units      = ListeLayers2.Units(1:2,:);
    ListeLayers3.Units(3,1) = ListeLayers2.Units(3,1);
    ListeLayers3.Units(4,1) = ListeLayers2.Units(3,2);
    
    ListeLayers3.Comments      = ListeLayers2.Comments(1:2,:);
    ListeLayers3.Comments(3,1) = ListeLayers2.Comments(3,1);
    ListeLayers3.Comments(4,1) = ListeLayers2.Comments(3,2);
    
    ListeLayers = ListeLayers3;
    %         save('Y:\private\ifremer\Ridha\ligne_Renard_EM710\ListeLayersCalib.mat', 'ListeLayers')
    %         save('F:\NIWA\TheBrothers2017\Data\ListeLayersCalib.mat', 'ListeLayers')
    %     end
    persistent_ListeLayers = ListeLayers;
end

%% Lecture des datagrams soundSpeedProfile

if isempty(DataSSP)
    [~, DataSSP] = SSc_ReadDatagrams(this.nomFic, 'Ssc_SoundSpeedProfile', 'Memmapfile', 0);
end

%% Lecture de la donnee "InstallationParameters"

if isempty(DataInstalParam)
    [flag, DataInstalParam, isDual] = read_installationParameters_bin(this);
    if ~flag
        c = [];
        return
    end
else
    isDual = DataInstalParam.isDual;
end

%% Lecture de runtime

if isempty(DataRuntime)
    [flag, DataRuntime] = SSc_ReadDatagrams(this.nomFic, 'Ssc_Runtime');
    if ~flag
        str = sprintf('Pb reading "Runtime" datagrams in this file. (%s)', this.nomFic);
        my_warndlg(str, 0);
        c = [];
        return
    end
end
if isempty(DataRuntime.PingCounter)
    str = sprintf('No "Runtime" datagrams in this file. (%s)', this.nomFic);
    my_warndlg(str, 0);
    c = [];
    return
end

%% Lecture de depth

if isempty(DataDepth)
    [flag, DataDepth, VarNameInFailure] = read_depth_bin(this, 'Memmapfile', -1);
    % [flag, DataDepth, VarNameInFailure] = read_depth_bin(this, 'Memmapfile', -3);
    if ~flag
        if isempty(VarNameInFailure)
            str = sprintf('Pb reading "Depth" datagrams in file "%s"', this.nomFic);
        else
            str = sprintf('Pb reading variable "%s" in "Depth" datagrams in file "%s"', VarNameInFailure, this.nomFic);
        end
        my_warndlg(str, 0);
        c = [];
        return
    end
end
if isempty(DataDepth.PingCounter)
    str = sprintf('No "Depth" datagrams in file "%s"', this.nomFic);
    my_warndlg(str, 0);
    c = [];
    return
end
nbPings = length(DataDepth.PingCounter);
if nbPings < 2
    str = sprintf('Two pings are necessary in the file, that is not the case  in this file. (%s)', this.nomFic);
    my_warndlg(str, 0);
    c = [];
    return
end

%% Lecture de Raw

if isempty(DataRaw)
    [flag, DataRaw] = read_rawRangeBeamAngle(this, 'Memmapfile', -1);
    % [flag, DataRaw] = read_rawRangeBeamAngle(this, 'Memmapfile', -3);
    if ~flag
        return
    end
end

%% Read SeabedImage in order to get the correct sample frequency (pb encontered on EM2040
%  file 0008_20141031_084035_Thalia where the sample frequency od Depth
%  datagrams are 4 times higher thant the sample frequency from Range
%  datagrams

if isempty(DataSeabed)
    [flag, DataSeabed] = SSc_ReadDatagrams(this.nomFic, 'Ssc_SeabedImage59h', 'Memmapfile', -1);
    % [flag, DataSeabed] = SSc_ReadDatagrams(this.nomFic, 'Ssc_SeabedImage59h', 'Memmapfile', -3);
    if ~flag
        return
    end
end

%% Lecture et interpolation de la donn�e "Attitude"

if isempty(DataAttitude)
    [flag, DataAttitude] = read_attitude_bin(this, 'IndAttUnit', IndAttUnit, 'Memmapfile', -1); %#ok<ASGLU>
end
if isempty(DataAttitude) || isempty(DataAttitude.Roll)
%     RollTx  = [];
%     PitchTx = [];
%     HeaveTx = [];
%     Tide    = [];
%     Draught = [];
    return
else
    % TODO : RollTx, PitchTx sont maintenant calcul�s sous forme de signaux
    % (signalContainerFromAttitude). Cette partie du code devra �tre supprim�e
    % lorsque les signaux seront pris en compte � la place des signaux
    % manuels "c(k) = set(c(k), 'SonarRoll',  RollTx(subDepth));"
    RollTx  = my_interp1(DataAttitude.Datetime, DataAttitude.Roll,  DataDepth.Datetime(:,1), 'linear', 'extrap');
    PitchTx = my_interp1(DataAttitude.Datetime, DataAttitude.Pitch, DataDepth.Datetime(:,1), 'linear', 'extrap');
    HeaveTx = my_interp1(DataAttitude.Datetime, DataAttitude.Heave, DataDepth.Datetime(:,1), 'linear', 'extrap');
    
    if isfield(DataAttitude, 'Tide') && ~isempty(DataAttitude.Tide)
%       Tide.Time     = DataAttitude.Time;
        Tide.Datetime = DataAttitude.Datetime;
        Tide.Value    = DataAttitude.Tide;
    else
        Tide = [];
        flagTideCorrection = 0;
    end
    
    if isfield(DataAttitude, 'Draught') && ~isempty(DataAttitude.Draught)
%       Draught.Time     = DataAttitude.Time;
        Draught.Datetime = DataAttitude.Datetime;
        Draught.Value    = DataAttitude.Draught;
    else
        Draught = [];
        flagDraughtCorrection = 0;
    end
    
    % TODO isempty(TrueHeave)
    if flagDraughtCorrection && isfield(DataAttitude, 'TrueHeave') && ~isempty(DataAttitude.TrueHeave)
%       TrueHeave.Time     = DataAttitude.Time;
        TrueHeave.Datetime = DataAttitude.Datetime;
        TrueHeave.Value    = DataAttitude.TrueHeave;
        %         'TrueHeave Jamais test�, �a risque de planter'
        HeaveTx = my_interp1(TrueHeave.Datetime, TrueHeave.Value,  DataDepth.Datetime(:,1), 'linear', 'extrap');
    end
end

%% Lecture et interpolation de la donn�e "Position"

[flag, DataPosition] = read_position_bin(this, 'IndNavUnit', IndNavUnit, 'Memmapfile', 0);
if flag
    Longitude = my_interp1_longitude(DataPosition.Datetime, DataPosition.Longitude, DataDepth.Datetime(:,1), 'linear', 'extrap');
    Latitude  = my_interp1(DataPosition.Datetime, DataPosition.Latitude,            DataDepth.Datetime(:,1), 'linear', 'extrap');
    Speed     = my_interp1(DataPosition.Datetime, DataPosition.Speed,               DataDepth.Datetime(:,1), 'linear', 'extrap');
else
    Longitude = NaN(DataDepth.Dimensions.nbPings, 1);
    Latitude  = NaN(DataDepth.Dimensions.nbPings, 1);
    Speed     = NaN(DataDepth.Dimensions.nbPings, 1);
end

%% Lecture et interpolation de la donn�e "SurfaceSoundSpeed"

[flag, DataSurfaceSoundSpeed] = SSc_ReadDatagrams(this.nomFic, 'Ssc_SurfaceSoundSpeed', 'Memmapfile', -1);
if ~flag || isempty(DataSurfaceSoundSpeed) || isempty(DataSurfaceSoundSpeed.Datetime)
    SurfaceSoundSpeed = DataDepth.SoundSpeed;
    %     disp('Message pour JMA : Ici pas trouve de SurfaceSoundSpeed, on prend 1500. C''est pas top de faire ca')
    %     SurfaceSoundSpeed = ones(length(DataDepth.Datetime), 1) * 1500;
else
    subNon0 = find(DataSurfaceSoundSpeed.SurfaceSoundSpeed ~= 0);
    SurfaceSoundSpeed = my_interp1(DataSurfaceSoundSpeed.Datetime(subNon0), DataSurfaceSoundSpeed.SurfaceSoundSpeed(subNon0), DataDepth.Datetime(:,1));
    if all(isnan(SurfaceSoundSpeed))
        SurfaceSoundSpeed = DataDepth.SoundSpeed;
    else
        kdeb = find(~isnan(SurfaceSoundSpeed), 1, 'first');
        SurfaceSoundSpeed(1:kdeb-1) = SurfaceSoundSpeed(kdeb);
        kdeb = find(~isnan(SurfaceSoundSpeed), 1, 'last');
        SurfaceSoundSpeed(kdeb+1:end) = SurfaceSoundSpeed(kdeb);
    end
end
SurfaceSoundSpeed = SurfaceSoundSpeed(:);

% ------------------------------------------------------------------------
% TRAITEMENT PARTICULIER UTILE UNIQUEMENT POUR DES DONNEES RESON AU FORMAT
% SIMRAD : le datagram runtime n'existe pas

if isempty(DataRuntime.PingCounter)
    DataRuntime.Time     = DataDepth.Time;
    DataRuntime.Datetime = DataDepth.Datetime;
    DataRuntime.Mode     = ones(nbPings, 1);
    % DataRaw.MeanAbsorptionCoeff = 0; � valider quand le cas se pr�sentera
end

%% Lecture et interpolation de la donn�e "Mode"

Mode = my_interp1_Extrap_PreviousThenNext(DataRuntime.Datetime, DataRuntime.Mode, DataDepth.Datetime(:,1));
Mode = Mode(:);

% TODO : remplacer ligne pr�c�dente par celles-ci
% [~, Z] = create_signal_Mode(this, 'DataDepth', DataDepth, 'DataRunTime', DataRunTime);
% Mode = Z.Data;

% Traitement particulier necessite par le fichier NIWA/TAN0105/ALL/line-006
indPremierNonNul = find(Mode ~= 0, 1);
if ~isempty(indPremierNonNul)
    Mode(1:indPremierNonNul) = Mode(indPremierNonNul);
end
subMode0 = find(Mode == 0);
if ~isempty(subMode0)
    if ~isdeployed
        my_warndlg(['Attention : Mode=0 trouv� dans le fichier ' NomImage ' '], 0, 'Tag', 'Mode0TrouveDansFichier');
    end
    Mode(subMode0) = 1;
end

%% Cr�ation de l'instance cli_image

TagSynchroX = [NomImage ' - PingBeam'];

%% S�lection interactive de la zone

[flag, IdentSonar] = SimradModel2IdentSonar(DataDepth.EmModel, 'isDual', isDual);
if ~flag
    c = [];
    return
end

%% Param�tres du sondeur d�finis pour le mode majoritaire

% if ~any(Mode)
%     Mode(:) = 1; % IMPOSE AUTORITAIREMENT (Cas des donn�es du NIWA)
% end

if isfield(DataDepth, 'ListeSystemSerialNumber')
    SystemSerialNumber = DataDepth.ListeSystemSerialNumber(1);
else
    SystemSerialNumber = DataDepth.SystemSerialNumber;
end

% TODO : resolutionMajoritaire a �t� d�fini au tout d�but de SonarScope, �
% cette �poque nous n'avions qu'une seule r�solution par fichier. Il
% faudrait supprimer ces attributs globaux et les remplacer par des vecteurs

[flag, SonarMode_1, SonarMode_2, SonarMode_3] = get_SonarModes(this, DataDepth, DataRaw, DataRuntime, 'isDual', isDual);
if ~flag
    c = [];
    return
end
MainFrequency = getMainFrequencyIfEM2040(IdentSonar, DataRaw.CentralFrequency);
SonarDescription = cl_sounder('Sonar.Ident', IdentSonar, ...
    'Sonar.Mode_1', SonarMode_1, 'Sonar.Mode_2', SonarMode_2, 'Sonar.Mode_3', SonarMode_3, ...
    'Sonar.SystemSerialNumber', SystemSerialNumber, 'MainFrequency', MainFrequency);
resolutionMajoritaire = get_RangeResol(SonarDescription, SonarMode_1, SonarMode_2);

InitialFileFormat = 'SimradAll';
GeometryType = cl_image.indGeometryType('PingBeam');
YUnit = 'Ping';

%% Cas o� il n'y a pas de datagrams d'attitude : on estime HeaveTx sur la donn�e

if isempty(HeaveTx)
    tmp = sprintf('HeaveTx is estimated on data because there is no Attitude datagrams found in the file%s', this.nomFic);
    my_warndlg(tmp, 0, 'Tag', 'Y''a pas DataAttitude');
    %              Height = -get(Z, 'Height');
    Height = NaN(1,nbPings);
    for kPing=1:nbPings
        Height(kPing) = mean(DataDepth.Depth(kPing,:), 'omitnan');
    end
    
    [ButterB,ButterA] = butter(2, 0.05);
    subNaN = find(isnan(Height));
    if ~isempty(subNaN)
        subNonNaN = find(~isnan(Height));
        Height(subNaN) = interp1(subNonNaN, Height(subNonNaN), subNaN);
    end
    if length(Height) < (length(ButterB) * 2 + 1)
        HeaveTx = zeros(size(Height));
    else
        Heightfiltree = filtfilt(ButterB, ButterA, double(Height));
        % figure; plot(Height, 'b'); grid on
        % hold on; plot(Heightfiltree, 'r'); grid on
        HeaveTx = Height - Heightfiltree;
    end
end
HeaveTx = HeaveTx(:);


if isempty(sublDepth)
    sublDepth = 1:size(DataDepth.Depth,1);
end

if isempty(subc)
    subc = 1:size(DataDepth.Depth,2);
end

[subDepth, subRaw, subSeabed] = intersect3(DataDepth.PingCounter(sublDepth,1), DataRaw.PingCounter(:,1), DataSeabed.PingCounter(:,1));
subDepth = sublDepth(subDepth);
clear sublDepth % Histoire de ne plus se tromper
nbPings = length(subDepth);
if nbPings < 2
    str = sprintf('Two common pings between XYZ, Raw Range and Seabed Image datagrams are necessary in the file, that is not the case  in this file. (%s)', this.nomFic);
    my_warndlg(str, 0);
    c = [];
    return
end

try
    DualSwath = my_interp1_Extrap_PreviousThenNext(DataRaw.Datetime, DataRaw.DualSwath, DataDepth.Datetime(subDepth,1)); % Modif JMA le 30/03/2023
catch    % Pour les caches anciens
    DualSwath = my_interp1_Extrap_PreviousThenNext(DataRuntime.Datetime, DataRuntime.DualSwath, DataDepth.Datetime(subDepth,1));
end
% Attention, incompatible avec import_PingBeam_All_V1 o� nbPings sorti de la fonction est = DataDepth.Dimensions.nbPings;
nbPingsOut = DataDepth.Dimensions.nbPings;


if isDual && (nbPings == 0)
    [subDepth, subRaw] = intersect3(DataDepth.PingCounter(:,2), DataRaw.PingCounter(:,2), DataRaw.PingCounter(:,2));
    nbPings = length(subDepth);
end
y = 1:nbPings;
Time     = DataDepth.Time(subDepth);
Datetime = DataDepth.Datetime(subDepth, 1);
% Heading = DataDepth.Heading(subDepth,:);
Heading = DataDepth.Heading(subDepth,1); % Modif JMA le 30/06/2017 EM2040 Dual Rx

%% Cr�ation des signaux provenant des datagrammes runtime

TsubDepth = DataDepth.Datetime;
[~, signalContainerFromRuntime] = create_signalsFromRuntime(this, TsubDepth(subDepth,1), 'DataRuntime', DataRuntime);
% signalContainerFromRuntime.plot
% signalContainerFromRuntime.plotAuto
% signalContainerFromRuntime.editProperties

%% Cr�ation des signaux provenant des datagrammes RawRangeAndBeamAngle

[~, signalContainerFromRawRange] = create_signalsFromRawRange(this, DataInstalParam, 'DataRaw', DataRaw, 'subRaw', subRaw);

%% Cr�ation des signaux provenant des datagrammes SeabedImage

[~, signalContainerFromSeabedImage] = create_signalsFromSeabedImage(this, 'DataSeabed', DataSeabed, 'subSeabed', subSeabed);

%% Cr�ation des signaux provenant des datagrammes Attitude

sectorTransmitDelaySignal = signalContainerFromRawRange.getSignalByTag('SectorTransmitDelay');
[~, signalContainerFromAttitude] = create_signalsFromAttitude(this, TsubDepth(subDepth,1), sectorTransmitDelaySignal.ySample, 'DataAttitude', DataAttitude);

%% HeaveRx
        
[TwoWayTravelTime_s, TwoWayTravelTimeInSamples, TxDelay_PingBeam_s] = get_TwoWayTravelTimeInSeconds(A0, 3, DataRaw, DataDepth, subRaw, subDepth);
% HeaveRx = get_HeaveRx(this, DataAttitude, Datetime, TwoWayTravelTime_s(:,subc), TxDelay_PingBeam_s(:,subc));
% heaveTxSignal = signalContainerFromAttitude.getSignalByTag('HeaveTx');
% for k=length(heaveTxSignal.ySample):-1:1
%     HeaveTxVal(:,k) = heaveTxSignal.ySample(k).data;
% end
% HeaveTx_PingBeam = get_PingBeamMatrixFromPingSignal(HeaveTxVal, DataRaw.TransmitSectorNumber(subRaw,:));
% Heave = (HeaveRx + HeaveTx_PingBeam) / 2;

%% Lecture de Immersion

% [flag, Z] = create_signal_Immersion(this, 'DataDepth', DataDepth, 'DataAttitude', DataAttitude);
% if ~flag
%     % TODO
% end
% TransducerDepth = Z.Data(subDepth);
TransducerDepth = -abs(DataDepth.TransducerDepth(subDepth,:)); % Modif JMA le 25/11/2020 (�changes de mail avec Herv�)


PingCounter     = DataDepth.PingCounter(subDepth,1); % Modif JMA le 30/06/2017 EM2040 Dual Rx
SampleFrequency = DataSeabed.SamplingRate(subSeabed,1); % Modif JMA le 30/06/2017 EM2040 Dual Rx

Mask = isnan(DataDepth.Depth(subDepth,:)); % Modifi� par JMA le 07/03/2018
if MasqueActif
    Mask = Mask | DataDepth.Mask(subDepth,:); % Modifi� par JMA le 07/03/2018
end
Mask = (Mask ~= 0);

%% D�termination du ping sequence

PingSequence = DataRaw.PingSequence(subRaw);
SonarName = get(SonarDescription, 'Sonar.Name');

%% Recherche d'un fichier Bscorr.txt �ventuel

SonarDescription = set(SonarDescription, 'Sonar.Ident', IdentSonar, ...
    'Sonar.Mode_1', SonarMode_1, 'Sonar.Mode_2', SonarMode_2, 'Sonar.Mode_3', SonarMode_3);
[~, SonarDescription] = set_KongsbergBsCorr(this, SonarDescription);

%% Recherche des modes suppl�mentaires pour l'EM2040

switch DataDepth.EmModel
    case {2040; 2045}
        isEM2040 = 1;
        
        % Avant le 08/01/2019
        %{
    [flag, ~, Mode2, Mode3] = get_SonarModesPings(this, DataDepth, DataRaw, DataRuntime, subDepth);
    if ~flag
        c = [];
        return
    end
        %}
        
        % Apr�s le 08/01/2019
        % {
        [flag, Z] = create_signal_Mode(this, 'DataDepth', DataDepth, 'DataRuntime', DataRuntime, 'subDepth', subDepth);
        if ~flag
            return
        end
        Mode1 = Z.Data;
        
        [flag, Z] = create_signal_Mode2_EM2040(this, Mode1, 'DataDepth', DataDepth, 'DataRaw', DataRaw, 'subDepth', subDepth, 'subRaw', subRaw);
        if ~flag
            return
        end
        Mode2 = Z.Data;
        
        [flag, Z] = create_signal_Mode3_EM2040(this, 'DataDepth', DataDepth, 'DataRuntime', DataRuntime, 'subDepth', subDepth);
        if ~flag
            return
        end
        Mode3 = Z.Data;
        % }
    otherwise
        isEM2040 = 0;
end

%% Lecture des layers primaires des datagrams "XYZ 88"

for k=1:length(ListeLayers.SelectionLayers{1,1})
    if ~ListeLayers.SelectionLayers{1,1}(k)
        continue
    end
    
    nomLayer = ListeLayers.nomLayers{1,1}{k};
    Unit     = ListeLayers.Units{1,1}{k};
    Comment  = ListeLayers.Comments{1,1}{k};
    
    messageLoadImage(NomImage, nomLayer, Mute)
    if ~isfield(DataDepth, nomLayer)
        message_LayerNotAvailable(nomLayer, 0);
        continue
    end
    if isempty(DataDepth.(nomLayer))
        message_LayerNotAvailable(nomLayer, 0);
        continue
    end

    I = DataDepth.(nomLayer)(subDepth,subc);
    
    if ~strcmp(nomLayer, 'Mask')
        I(Mask) = NaN;
    end
    
    if strcmp(nomLayer, 'ReflectivityFromSnippets') ...
            || strcmp(nomLayer, 'ReflectivityFromSnippetsWithoutSpecularRestablishment') ...
            || strcmp(nomLayer, 'ReflectivityBestBS')
        if isfield(DataDepth, 'MaskPingsReflectivity') && ~isempty(DataDepth.MaskPingsReflectivity)
            for kPing=1:length(subDepth)
                if DataDepth.MaskPingsReflectivity(subDepth(kPing)) == 1
                    I(kPing,:) = NaN;
                end
            end
        end
    end
    
    switch nomLayer
        case 'Mask'
            ValNaN = 255;
        otherwise
            ValNaN = NaN;
    end
        
    if isfield(DataDepth, 'MaskPingsBathy') && ~isempty(DataDepth.MaskPingsBathy)
        for kPing=1:length(subDepth)
            if DataDepth.MaskPingsBathy(subDepth(kPing)) == 1
                I(kPing,:) = NaN;
            end
        end
    end

    %% Cr�ation de l'instance cl_image
    
    b = cl_image('Image', I, 'Name', NomImage, 'InitialImageName', nomLayer, ...
        'Unit', Unit, 'ColormapIndex', 3, ...
        'TagSynchroX', TagSynchroX, 'TagSynchroY', TagSynchroY,...
        'x', subc, 'XUnit', '# Beam', ...
        'y', y, 'YUnit', YUnit, ...
        'ValNaN', ValNaN, ...
        'InitialFileName', this.nomFic, ...
        'InitialFileFormat', InitialFileFormat, ...
        'GeometryType', GeometryType, ...
        'SonarDescription', SonarDescription, ...
        'Comments', Comment);
    
    [DataType, ColormapIndex] = ALL.identSonarDataType(nomLayer);
    b = set(b, 'DataType', DataType, 'ColormapIndex', ColormapIndex);
    
    switch nomLayer
        case 'ReflectivityFromSnippets'
            b.Name = [NomImage '_FromSnippets'];
        case 'ReflectivityFromSnippetsWithoutSpecularRestablishment'
            b.Name = [NomImage '_FromSnippetsWSR'];
        case 'ReflectivityBestBS'
            b.Name = [NomImage '_BestBS'];
        otherwise
            % On ne change pas le nom
    end
    b = update_Name(b);
    c(end+1) = b; %#ok<AGROW>
    ListeLayers.Order{1,1}(k) = length(c);
end

%% Calcul du vecteur Tide, Draught

if isempty(Tide)
    TideValue = [];
else
    TideValue = my_interp1(Tide.Datetime, Tide.Value, DataDepth.Datetime(subDepth));
end

if isempty(Draught)
    DraughtValue = [];
else
    DraughtValue = my_interp1(Draught.Datetime, Draught.Value, DataDepth.Datetime(subDepth));
end

%% Lecture des layers d�duits des datagrams "XYZ 88"

for k=1:length(ListeLayers.SelectionLayers{1,2})
    if ~ListeLayers.SelectionLayers{1,2}(k)
        continue
    end
    
    BathymetryStatus = get_BathymetryStatus(I0);

    nomLayer = ListeLayers.nomLayers{1,2}{k};
    Unit     = ListeLayers.Units{1,2}{k};
    Comment  = ListeLayers.Comments{1,2}{k};
    ColormapIndex = 3;
    CLim          = [];
    ValNaN        = NaN;
    
    messageLoadImage(NomImage, nomLayer, Mute)
    switch k
        case 1
            [I, DataType, ImageName] = getBathy(DataDepth, subDepth, subc, Mask, NomImage, ...
                flagTideCorrection, TideValue, ...
                flagDraughtCorrection, DraughtValue, ...
                TransducerDepth, Mute);
            BathymetryStatus.TideApplied = flagTideCorrection;
            
        case 14
            flagTideCorrection = 1;
            if isempty(TideValue)
                str1 = sprintf('Pas de Tide dans "%s", pas de layer "%s" loaded.', NomImage, nomLayer);
                str2 = sprintf('No tide in "%s", no layer "%s" loaded.', NomImage, nomLayer);
                my_warndlg(Lang(str1,str2), 0);
                ListeLayers.Order{1,2}(k) = 0;
                message_LayerNotAvailable(nomLayer, 0);
                continue
            end
            [I, DataType, ImageName] = getBathy(DataDepth, subDepth, subc, Mask, NomImage, ...
                flagTideCorrection, TideValue, ...
                flagDraughtCorrection, DraughtValue, ...
                TransducerDepth, Mute);
            BathymetryStatus.TideApplied = 1;
            
        case 2 % Detection
            I = DataDepth.DetectionInfo(subDepth,subc);
            subNaN = (I >= 128);
            I = decodeBits(I, 1);
            I = I + 1;
            I(subNaN) = 0;
            DataType = cl_image.indDataType('DetectionType');
            ImageName = NomImage;
            CLim = [0 2];
            ValNaN = 0;
            
        case 3 % NaNDetection % A COMPLETER LORSQUE L'ON AURA DES DONNEES QUI S'Y PRETERONT
            I = DataDepth.DetectionInfo(subDepth,subc);
            %             I(I<128) = NaN;
            %             I = I - 128;
            I(I>=128) = NaN;
            DataType = cl_image.indDataType('Unknown');
            ImageName = [NomImage '-NaNFillMethod'];
            
        case 4 % Sound speed
            I = sonar_depth2soundSpeed(DataDepth.Depth(subDepth,subc), DataDepth.Datetime(subDepth), DataSSP);
            I(Mask) = NaN;
            DataType = cl_image.indDataType('SoundVelocity');
            ImageName = NomImage;
            
        case 5 % AbsorptionCoefficientRT
            % TODO : isfield pour �tre compatible avec les anciens fichiers
            % (r�alis�s avant introduction des layers d'absorption)
            if ~isfield(DataDepth, 'AbsorptionCoefficientRT')
                % TODO : mettre un message d'avertissement
                ListeLayers.Order{1,2}(k) = 0;
                message_LayerNotAvailable(nomLayer, 0);
                continue
            end
            I = DataDepth.AbsorptionCoefficientRT(subDepth,subc);
            I(Mask) = NaN;
            DataType = cl_image.indDataType('AbsorptionCoeff');
            ImageName = NomImage;
            
        case 6 % AbsorptionCoefficientSSc
            % TODO : isfield pour �tre compatible avec les anciens fichiers
            % (r�alis�s avant introduction des layers d'absorption)
            if ~isfield(DataDepth, 'AbsorptionCoefficientSSc')
                ListeLayers.Order{1,2}(k) = 0;
                message_LayerNotAvailable(nomLayer, 0);
                continue
            end
            I = DataDepth.AbsorptionCoefficientSSc(subDepth,subc);
            I(Mask) = NaN;
            DataType = cl_image.indDataType('AbsorptionCoeff');
            ImageName = NomImage;
            
        case 7 % AbsorptionCoefficientUser
            % TODO : isfield pour �tre compatible avec les anciens fichiers
            % (r�alis�s avant introduction des layers d'absorption)
            if ~isfield(DataDepth, 'AbsorptionCoefficientUser')
                ListeLayers.Order{1,2}(k) = 0;
                message_LayerNotAvailable(nomLayer, 0);
                continue
            end
            flag = ALL_CacheIsLayerPopulated(this.nomFic, DataDepth.Images, 'AbsorptionCoefficientUser');
            if ~flag
                ListeLayers.Order{1,2}(k) = 0;
                continue
            end
            if isempty(DataDepth.AbsorptionCoefficientUser)
                ListeLayers.Order{1,2}(k) = 0;
                message_LayerNotAvailable(nomLayer, 0);
                continue
            end
            I = DataDepth.AbsorptionCoefficientUser(subDepth,subc);
            I(Mask) = NaN;
            DataType = cl_image.indDataType('AbsorptionCoeff');
            ImageName = NomImage;
            
        case 8 % IncidenceAngle
            % TODO : isfield pour �tre compatible avec les anciens fichiers
            % (r�alis�s avant introduction des layers d'absorption)
            if ~isfield(DataDepth, 'IncidenceAngle')
                ListeLayers.Order{1,2}(k) = 0;
                message_LayerNotAvailable(nomLayer, 0);
                continue
            end
            flag = ALL_CacheIsLayerPopulated(this.nomFic, DataDepth.Images, 'IncidenceAngle');
            if ~flag
                ListeLayers.Order{1,2}(k) = 0;
                continue
            end
            if isempty(DataDepth.IncidenceAngle)
                ListeLayers.Order{1,2}(k) = 0;
                message_LayerNotAvailable(nomLayer, 0);
                continue
            end
            I = DataDepth.IncidenceAngle(subDepth,subc);
            I(Mask) = NaN;
            DataType = cl_image.indDataType('IncidenceAngle');
            ImageName = NomImage;
            ColormapIndex = 17;
            
        case 9 % SlopeAcross
            % TODO : isfield pour �tre compatible avec les anciens fichiers
            % (r�alis�s avant introduction des layers d'absorption)
            if ~isfield(DataDepth, 'SlopeAcross')
                ListeLayers.Order{1,2}(k) = 0;
                message_LayerNotAvailable(nomLayer, 0);
                continue
            end
            flag = ALL_CacheIsLayerPopulated(this.nomFic, DataDepth.Images, 'SlopeAcross');
            if ~flag
                ListeLayers.Order{1,2}(k) = 0;
                continue
            end
            if isempty(DataDepth.SlopeAcross)
                ListeLayers.Order{1,2}(k) = 0;
                message_LayerNotAvailable(nomLayer, 0);
                continue
            end
            I = DataDepth.SlopeAcross(subDepth,subc);
            I(Mask) = NaN;
            DataType = cl_image.indDataType('SlopeAcross');
            ImageName = NomImage;
            
        case 10 % SlopeAlong
            % TODO : isfield pour �tre compatible avec les anciens fichiers
            % (r�alis�s avant introduction des layers d'absorption)
            if ~isfield(DataDepth, 'SlopeAlong')
                ListeLayers.Order{1,2}(k) = 0;
                message_LayerNotAvailable(nomLayer, 0);
                continue
            end
            flag = ALL_CacheIsLayerPopulated(this.nomFic, DataDepth.Images, 'SlopeAlong');
            if ~flag
                ListeLayers.Order{1,2}(k) = 0;
                continue
            end
            if isempty(DataDepth.SlopeAlong)
                ListeLayers.Order{1,2}(k) = 0;
                message_LayerNotAvailable(nomLayer, 0);
                continue
            end
            I = DataDepth.SlopeAlong(subDepth,subc);
            I(Mask) = NaN;
            DataType = cl_image.indDataType('SlopeAlong');
            ImageName = NomImage;
            
        case 11 % BathymetryFromDTM
            % TODO : isfield pour �tre compatible avec les anciens fichiers
            % (r�alis�s avant introduction des layers d'absorption)
            if ~isfield(DataDepth, 'BathymetryFromDTM')
                ListeLayers.Order{1,2}(k) = 0;
                message_LayerNotAvailable(nomLayer, 0);
                continue
            end
            flag = ALL_CacheIsLayerPopulated(this.nomFic, DataDepth.Images, 'BathymetryFromDTM');
            if ~flag
                ListeLayers.Order{1,2}(k) = 0;
                continue
            end
            if isempty(DataDepth.BathymetryFromDTM)
                ListeLayers.Order{1,2}(k) = 0;
                message_LayerNotAvailable(nomLayer, 0);
                continue
            end
            I = DataDepth.BathymetryFromDTM(subDepth,subc);
            I(Mask) = NaN;
            DataType = cl_image.indDataType('Bathymetry');
            ImageName = NomImage;
            
        case 12 % InsonifiedAreaSSc
            % TODO : isfield pour �tre compatible avec les anciens fichiers
            % (r�alis�s avant introduction des layers d'absorption)
            if ~isfield(DataDepth, 'InsonifiedAreaSSc')
                ListeLayers.Order{1,2}(k) = 0;
                message_LayerNotAvailable(nomLayer, 0);
                continue
            end
            flag = ALL_CacheIsLayerPopulated(this.nomFic, DataDepth.Images, 'InsonifiedAreaSSc');
            if ~flag
                ListeLayers.Order{1,2}(k) = 0;
                continue
            end
            if isempty(DataDepth.InsonifiedAreaSSc)
                ListeLayers.Order{1,2}(k) = 0;
                message_LayerNotAvailable(nomLayer, 0);
                continue
            end
            I = DataDepth.InsonifiedAreaSSc(subDepth,subc);
            I(Mask) = NaN;
            DataType = cl_image.indDataType('InsonifiedAreadB');
            ImageName = NomImage;
            
        case 13 % PortStarboard
            ns2 = floor(DataDepth.Dimensions.nbBeams / 2);
            subBab = 1:ns2;
            subTri = (ns2+1):DataDepth.Dimensions.nbBeams;
            I(:,subTri) = single(2);
            I(1:length(subDepth),subBab) = 1;
            I = I(:,subc);
            I(Mask) = NaN;
            DataType = cl_image.indDataType('PortStarboard');
            ImageName = NomImage;
           
        otherwise
            ListeLayers.Order{1,2}(k) = 0;
            message_LayerNotAvailable(nomLayer, 1);
            continue
    end
    
    % --------
    % Masquage
    
    if isfield(DataDepth, 'MaskPingsBathy') && ~isempty(DataDepth.MaskPingsBathy)
        for kPing=1:length(subDepth)
            if DataDepth.MaskPingsBathy(subDepth(kPing)) == 1
                I(kPing,:) = NaN;
            end
        end
    end
    
    % -------------------------------
    % Cr�ation de l'instance cl_image
    
    b = cl_image('Image', I, 'Name', ImageName, 'InitialImageName', nomLayer, ...
        'Unit', Unit, 'ColormapIndex', 3, 'CLim', CLim, ...
        'TagSynchroX', TagSynchroX, 'TagSynchroY', TagSynchroY,...
        'x', subc, 'XUnit', '# Beam', ...
        'y', y, 'YUnit', YUnit, ...
        'ValNaN', ValNaN, ...
        'InitialFileName', this.nomFic, ...
        'InitialFileFormat', InitialFileFormat, ...
        'GeometryType', GeometryType, ...
        'SonarDescription', SonarDescription, ...
        'Comments', Comment);
    b = set(b, 'DataType', DataType, 'ColormapIndex', ColormapIndex);
    
    b = set_BathymetryStatus(b, BathymetryStatus);
    
    switch nomLayer
        case 'AbsorptionCoefficientRT'
            b.Name = [NomImage '_RT'];
        case 'AbsorptionCoefficientSSc'
            b.Name = [NomImage '_SSc'];
        case 'AbsorptionCoefficientUser'
            b.Name = [NomImage '_User'];
        otherwise
            % On ne change pas le nom
    end
    b = update_Name(b);
    c(end+1) = b; %#ok<AGROW>
    ListeLayers.Order{1,2}(k) = length(c);
end
% clear DataDepth

%% Lecture des layers primaires des datagrams "Raw range and angle 78"

for k=1:length(ListeLayers.SelectionLayers{2,1})
    if ~ListeLayers.SelectionLayers{2,1}(k)
        continue
    end
    
    nomLayer = ListeLayers.nomLayers{2,1}{k};
    Unit     = ListeLayers.Units{2,1}{k};
    Comment  = ListeLayers.Comments{2,1}{k};
    
    messageLoadImage(NomImage, nomLayer, Mute)
    I = DataRaw.(nomLayer)(subRaw,subc);
    
    switch nomLayer
        case 'BeamPointingAngle'
            I = DataRaw.BeamPointingAngle(subRaw,subc);
            I = Simrad_AnglesAntenna2ShipFloor(I, DataInstalParam, isDual);
            
            if DataInstalParam.EmModel == 850 % 850 = ME70
                RollRx = get_RollRx(A0, DataAttitude, Datetime, TwoWayTravelTime_s(:,subc), TxDelay_PingBeam_s(:,subc));
                I = I + RollRx;
            end
            
        case 'TransmitSectorNumber'
            % Bidouille pour EM2040C dual head car la proc�dure automatique
            % ne fonctionne pas
            if isDual
                if (DataInstalParam.EmModel == 2045)
                    m = size(I,2) / 2 + 1;
                    I(:,m:end) = 2;
%                 elseif (DataInstalParam.EmModel == 2040)
%                     m = size(I,2) / 2 + 1;
%                     I(:,m:end) = I(:,m:end) + 1;
                end
            end
    end
    
    % --------
    % Masquage
    
    %     I(Mask) = NaN;
    
    % -------------------------------
    % Creation de l'instance cl_image
    
    b = cl_image('Image', I, 'Name', NomImage, 'InitialImageName', nomLayer, ...
        'Unit', Unit, 'ColormapIndex', 3, ...
        'TagSynchroX', TagSynchroX, 'TagSynchroY', TagSynchroY,...
        'x', subc, 'XUnit', '# Beam', ...
        'y', y, 'YUnit', YUnit, ...
        'InitialFileName', this.nomFic, ...
        'InitialFileFormat', InitialFileFormat, ...
        'GeometryType', GeometryType, ...
        'SonarDescription', SonarDescription, ...
        'Comments', Comment);
    
    [DataType, ColormapIndex] = ALL.identSonarDataType(nomLayer);
    b = set(b, 'DataType', DataType, 'ColormapIndex', ColormapIndex);
    
    b = update_Name(b);
    c(end+1) = b; %#ok<AGROW>
    ListeLayers.Order{2,1}(k) = length(c);
end

%% Lecture des layers d�duits des datagrams "Raw range and angle 78"

for k=1:length(ListeLayers.SelectionLayers{2,2})
    if ~ListeLayers.SelectionLayers{2,2}(k)
        continue
    end
    
    nomLayer = ListeLayers.nomLayers{2,2}{k};
    Unit     = ListeLayers.Units{2,2}{k};
    Comment  = ListeLayers.Comments{2,2}{k};
    ColormapIndex = 3;
    
    messageLoadImage(NomImage, nomLayer, Mute)
    switch k
        case 1 % TiltAngle
            I = get_PingBeamMatrixFromPingSignal(DataRaw.TiltAngle(subRaw,:), DataRaw.TransmitSectorNumber(subRaw,subc));
            DataType = cl_image.indDataType('TxTiltAngle');
            ImageName = NomImage;
            
        case 2 % FocusRange
            I = get_PingBeamMatrixFromPingSignal(DataRaw.FocusRange(subRaw,:), DataRaw.TransmitSectorNumber(subRaw,subc));
            DataType = cl_image.indDataType('KongsbergFocusRange');
            ImageName = NomImage;
            
        case 3 % SignalLength
            I = get_PingBeamMatrixFromPingSignal(DataRaw.SignalLength(subRaw,:), DataRaw.TransmitSectorNumber(subRaw,subc));
            DataType = cl_image.indDataType('PulseLength');
            ImageName = NomImage;
            
        case 4 % SectorTransmitDelay
            I = get_PingBeamMatrixFromPingSignal(DataRaw.SectorTransmitDelay(subRaw,:), DataRaw.TransmitSectorNumber(subRaw,subc));
            DataType = cl_image.indDataType('KongsbergSectorTransmitDelay');
            ImageName = NomImage;
            
        case 5 % CentralFrequency
            I = get_PingBeamMatrixFromPingSignal(DataRaw.CentralFrequency(subRaw,:), DataRaw.TransmitSectorNumber(subRaw,subc));
            I = I / 1000; % Rajout� par JMA le 15/03/2016 pour AbsorptionCoefficientSSc
            DataType = cl_image.indDataType('Frequency');
            ImageName = NomImage;
            
        case 6 % SignalWaveformIdentifier
            I = get_PingBeamMatrixFromPingSignal(DataRaw.SignalWaveformIdentifier(subRaw,:), DataRaw.TransmitSectorNumber(subRaw,subc));
            DataType = cl_image.indDataType('KongsbergWaveformIdentifier');
            ImageName = NomImage;
            
        case 7 % SignalBandwith
            I = get_PingBeamMatrixFromPingSignal(DataRaw.SignalBandwith(subRaw,:), DataRaw.TransmitSectorNumber(subRaw,subc));
            DataType = cl_image.indDataType('SignalBandwith');
            ImageName = NomImage;
            
        case 8 % RayPathLength % One way length in meters TODO : A proscrire, 1500 m/s !!!!!!!!!!!!!!!!!!
            I = TwoWayTravelTime_s(:,subc) * (1500 / 2);
            DataType = cl_image.indDataType('RayPathLength'); % One way length in meters
            ImageName = NomImage;
            
        case 9 % RayPathSampleNb % TODO : supprimer cette option, Utiliser TwoWayTravelTimeInSamples
            I = TwoWayTravelTimeInSamples;
            DataType = cl_image.indDataType('TwoWayTravelTimeInSamples');
            ImageName = NomImage;
            
        case 10 % RxTime
            I = get_DatetimeRx(A0, TwoWayTravelTime_s(:, subc), TxDelay_PingBeam_s(:,subc), Datetime);
            I = datenum(I); % TODO : int�grer datetime dans la visu SSc
            DataType = cl_image.indDataType('RxTime');
            ImageName = NomImage;
            
        case 11
            % DataRaw.SectorTransmitDelay(subRaw,:)
            I = get_RollRx(A0, DataAttitude, Datetime, TwoWayTravelTime_s(:,subc), TxDelay_PingBeam_s(:,subc));
            DataType = cl_image.indDataType('Roll');
            ImageName = NomImage;
            
        case 12
            I = get_PitchRx(A0, DataAttitude, Datetime, TwoWayTravelTime_s(:,subc), TxDelay_PingBeam_s(:,subc));
            DataType = cl_image.indDataType('Pitch');
            ImageName = NomImage;
            
        case 13
            I = get_HeaveRx(A0, DataAttitude, Datetime, TwoWayTravelTime_s(:,subc), TxDelay_PingBeam_s(:,subc));
            DataType = cl_image.indDataType('Heave');
            ImageName = NomImage;
            
        case 14
            I = get_HeadingRx(A0, DataAttitude, Datetime, TwoWayTravelTime_s(:,subc), TxDelay_PingBeam_s(:,subc));
            DataType = cl_image.indDataType('Heading');
            ImageName = NomImage;
            
        case 15 % TxBeamIndexSwath
%             [flag, SonarMode_1, SonarMode_2, SonarMode_3] = get_SonarModes(this, DataDepth, DataRaw, DataRuntime, 'isDual', isDual);
% Pour EM2040 SonarMode_2=1 -> Normal SonarMode_2=2 -> Single sector
            I = Simrad_DataRaw2TxBeamIndexSwath(DataRaw, Mode(subDepth), 'subl', subRaw, 'subc', subc);
            DataType = cl_image.indDataType('TxBeamIndexSwath');
            ImageName = NomImage;
            
            % DataRawTransmitSectorNumber = changeTxBeamIndexForEM2040D(DataRawTransmitSectorNumber, mode2) ;

            % Bidouille pour EM2040C dual head car la proc�dure automatique
            % ne fonctionne pas : ATTENTION : on ne traite pas le cas du
            % dual swath mais existe t'il pour ce soundeur ?
            if (DataInstalParam.EmModel == 2045) && isDual
                m = size(I,2) / 2;
                I(:,1:m) = 1;
                I(:,(m+1):end) = 2;
            end
            
        case 16 % Speed
            DatetimeRx = get_DatetimeRx(A0, TwoWayTravelTime_s(:, subc), TxDelay_PingBeam_s(:,subc), Datetime);
            I = my_interp1(DataPosition.Datetime(:), DataPosition.Speed(:), DatetimeRx, 'linear', 'extrap');
            I = reshape(I, size(DatetimeRx));
            DataType = cl_image.indDataType('Speed');
            ImageName = NomImage;
            
        case 17 % RxAngleEarth
            I = DataRaw.BeamPointingAngle(subRaw,subc);
            I = Simrad_AnglesAntenna2ShipFloor(I, DataInstalParam, isDual);
            
            if DataInstalParam.EmModel ~= 850 % 850 = ME70
                RollRx = get_RollRx(A0, DataAttitude, Datetime, TwoWayTravelTime_s(:,subc), TxDelay_PingBeam_s(:,subc));
                I = I - RollRx; % Tr�s bizarre, on doit refaire de nouveau -RollRx dans sonar_range2depth_EM !!!!!
                % C'est comme si KM faisait l'op�ration dans le mauvais sens
            end
            
            DataType = cl_image.indDataType('RxAngleEarth');
            ImageName = NomImage;
            
        case 18 % Mode
            % Essai pour Mode GLT
            I = repmat(Mode(subDepth), 1, numel(subc));
            DataType = cl_image.indDataType('Mode');
            ImageName = NomImage;
            
        case 19 % TxAngleKjell
            I = DataRaw.BeamPointingAngle(subRaw,subc);
            I = Simrad_AnglesAntenna2ShipFloor(I, DataInstalParam, isDual);
            
            if DataInstalParam.EmModel ~= 850 % 850 = ME70
                RollRx_PingBeam = get_RollRx(A0, DataAttitude, Datetime, TwoWayTravelTime_s(:,subc), TxDelay_PingBeam_s(:,subc));
                
                rollTxSignal = signalContainerFromAttitude.getSignalByTag('RollTx');
                for k2=length(rollTxSignal.ySample):-1:1
                    RollTxVal(:,k2) = rollTxSignal.ySample(k2).data;
                end
                RollTx_PingBeam = get_PingBeamMatrixFromPingSignal(RollTxVal, DataRaw.TransmitSectorNumber(subRaw,:));
                Correctif = RollRx_PingBeam - RollTx_PingBeam;
                I = I + Correctif;
            end
            DataType = cl_image.indDataType('TxAngleKjell');
            ImageName = NomImage;
            
        case 20
            pitchTxSignal = signalContainerFromAttitude.getSignalByTag('PitchTx');
            for k2=length(pitchTxSignal.ySample):-1:1
                PitchTxVal(:,k2) = pitchTxSignal.ySample(k2).data;
            end
            PitchTx_PingBeam = get_PingBeamMatrixFromPingSignal(PitchTxVal, DataRaw.TransmitSectorNumber(subRaw,:));
            
            %{
            if isfield(DataInstalParam, 'S0P') % DualTX voir note 19 table  "D:\perso-jma\Documents\Kongsberg\160692av_em_datagram_formats.pdf"
                n = DataRaw.Dimensions.nbRx / 2;
                AngleMontageAlong = [repmat(DataInstalParam.S1P, n, 1) repmat(DataInstalParam.S2P, n, 1)];
                AngleMontageAlong = AngleMontageAlong(subc);
            else
                AngleMontageAlong = DataInstalParam.S1P;
            end
            %}
            
            I = DataRaw.TransmitSectorNumber(subRaw,subc);
            K = DataRaw.TransmitSectorNumber(subRaw,subc);
            for kRaw=1:max(K(:))
                I(K == kRaw) = DataRaw.TiltAngle(kRaw+1);
            end
%             I = I + PitchTx_PingBeam + AngleMontageAlong; % AngleMontageAlong est la valeur obtenue par le patch-test. Il ne faut pas la r�int�grer ici !
            I = I + PitchTx_PingBeam;
            DataType = cl_image.indDataType('SonarAlongBeamAngle');
            ImageName = NomImage;
            
        otherwise
            ListeLayers.Order{2,2}(k) = 0;
            message_LayerNotAvailable(nomLayer, 1);
            continue
    end
    
    % --------
    % Masquage
    
    %     I(Mask) = NaN;
    
    % -------------------------------
    % Cr�ation de l'instance cl_image
    
    b = cl_image('Image', I, 'Name', ImageName, 'InitialImageName', nomLayer, ...
        'Unit', Unit, 'ColormapIndex', 3, ...
        'TagSynchroX', TagSynchroX, 'TagSynchroY', TagSynchroY,...
        'x', subc, 'XUnit', '# Beam', ...
        'y', y, 'YUnit', YUnit, ...
        'InitialFileName', this.nomFic, ...
        'InitialFileFormat', InitialFileFormat, ...
        'GeometryType', GeometryType, ...
        'SonarDescription', SonarDescription, ...
        'Comments', Comment);
    b = set(b, 'DataType', DataType, 'ColormapIndex', ColormapIndex);
    
    b = update_Name(b);
    c(end+1) = b; %#ok<AGROW>
    ListeLayers.Order{2,2}(k) = length(c);
end

%% Lecture des layers primaires des datagrams "QualityFactor"

if any(ListeLayers.SelectionLayers{3,1}) || any(ListeLayers.SelectionLayers{3,2})
    [flag, DataQualityFactor] = SSc_ReadDatagrams(this.nomFic, 'Ssc_QualityFactorFromIfr', 'Memmapfile', -1);
    if ~flag
        str1 = sprintf('Le fichier "%s" ne contient pas de datagrammes "Quality Factor"', this.nomFic);
        str2 = sprintf('No "Quality Factor" datagrams in file "%s"', this.nomFic);
        my_warndlg(Lang(str1,str2), 0);
        ListeLayers.SelectionLayers{3,1}(:) = 0;
        ListeLayers.SelectionLayers{3,2}(:) = 0;
    end
end
for k=1:length(ListeLayers.SelectionLayers{3,1})
    if ~ListeLayers.SelectionLayers{3,1}(k)
        continue
    end
    
    nomLayer = ListeLayers.nomLayers{3,1}{k};
    Unit     = ListeLayers.Units{3,1}{k};
    Comment  = ListeLayers.Comments{3,1}{k};
    
    messageLoadImage(NomImage, nomLayer, Mute)
    
    I = DataQualityFactor.IfremerQF(subDepth,subc); % TODO : subDepth remplacer par subQualityFactor
    I(I == 0) = NaN;
    
    %{
    PL = get_PingBeamMatrixFromPingSignal(DataRaw.SignalLength(subRaw,:), DataRaw.TransmitSectorNumber(subRaw,subc));
    WI = get_PingBeamMatrixFromPingSignal(DataRaw.SignalWaveformIdentifier(subRaw,:), DataRaw.TransmitSectorNumber(subRaw,subc));
    I = CorrectSupposedErrorQFComputationByKongsberg(I, PL, WI);
    %}
    
    switch k
        case 2
            I = 10.^(-I) * 100;
        case 3
            Z = DataDepth.Depth(subDepth,:);
            I = 10.^(-I) .* (-Z);
    end
    
    % --------
    % Masquage
    
    I(Mask) = NaN;
    
    % -------------------------------
    % Cr�ation de l'instance cl_image
    
    b = cl_image('Image', I, 'Name', NomImage, 'InitialImageName', nomLayer, ...
        'Unit', Unit, 'ColormapIndex', 3, ...
        'TagSynchroX', TagSynchroX, 'TagSynchroY', TagSynchroY,...
        'x', subc, 'XUnit', '# Beam', ...
        'y', y, 'YUnit', YUnit, ...
        'InitialFileName', this.nomFic, ...
        'InitialFileFormat', InitialFileFormat, ...
        'GeometryType', GeometryType, ...
        'SonarDescription', SonarDescription, ...
        'Comments', Comment);
    
    [DataType, ColormapIndex] = ALL.identSonarDataType(nomLayer);
    b = set(b, 'DataType', DataType, 'ColormapIndex', ColormapIndex);
    
    b = update_Name(b);
    c(end+1) = b; %#ok<AGROW>
    ListeLayers.Order{3,1}(k) = length(c);
end

%% Lecture des layers d�duits des datagrams "DataRaw"

for k=1:length(ListeLayers.SelectionLayers{3,2})
    if ~ListeLayers.SelectionLayers{3,2}(k)
        continue
    end
    
    nomLayer = ListeLayers.nomLayers{3,2}{k};
    Unit     = ListeLayers.Units{3,2}{k};
    Comment  = ListeLayers.Comments{3,2}{k};
    ColormapIndex = 3;
    
    messageLoadImage(NomImage, nomLayer, Mute)
    switch k
        case 1 % TiltAngle
            I = DataRaw.TransmitSectorNumber(subRaw,subc);
            K = DataRaw.TransmitSectorNumber(subRaw,subc);
            for kRaw=1:max(K(:))
                I(K == kRaw) = DataRaw.TiltAngle(kRaw+1);
            end
            DataType = cl_image.indDataType('KongsbergTxTiltAngle');
            ImageName = NomImage;
            
        otherwise
            ListeLayers.Order{3,2}(k) = 0;
            message_LayerNotAvailable(nomLayer, 1);
            continue
    end
    
    % --------
    % Masquage
    
    %     I(Mask) = NaN;
    
    % -------------------------------
    % Creation de l'instance cl_image
    
    b = cl_image('Image', I, 'Name', ImageName, 'InitialImageName', nomLayer, ...
        'Unit', Unit, 'ColormapIndex', 3, ...
        'TagSynchroX', TagSynchroX, 'TagSynchroY', TagSynchroY,...
        'x', subc, 'XUnit', '# Beam', ...
        'y', y, 'YUnit', YUnit, ...
        'InitialFileName', this.nomFic, ...
        'InitialFileFormat', InitialFileFormat, ...
        'GeometryType',  GeometryType, ...
        'SonarDescription', SonarDescription, ...
        'Comments', Comment);
    b = set(b, 'DataType', DataType, 'ColormapIndex', ColormapIndex);
    
    b = update_Name(b);
    c(end+1) = b; %#ok<AGROW>
    ListeLayers.Order{3,2}(k) = length(c);
end

%% Lecture des layers primaires des datagrams "SeabedImage"

for k=1:length(ListeLayers.SelectionLayers{4,1})
    if ~ListeLayers.SelectionLayers{4,1}(k)
        continue
    end
    
    nomLayer = ListeLayers.nomLayers{4,1}{k};
    Unit     = ListeLayers.Units{4,1}{k};
    Comment  = ListeLayers.Comments{4,1}{k};
    
    messageLoadImage(NomImage, nomLayer, Mute)
    I = DataSeabed.(nomLayer)(subDepth,subc);
    CLim = [];
    ValNaN = NaN;
    
    switch k
        case 2 % InfoBeamDetectionInfo
            subNaN = (I >= 128);
            I = decodeBits(I, 1);
            I = I + 1;
            I(subNaN) = 0;
%             DataType = cl_image.indDataType('DetectionType');
%             ImageName = NomImage;
            CLim = [0 2];
            ValNaN = 0;
            
        otherwise
            I(I == 0) = NaN;
    end
    
    %% Cr�ation de l'instance cl_image
    
    b = cl_image('Image', I, 'Name', NomImage, 'InitialImageName', nomLayer, ...
        'Unit', Unit, 'ColormapIndex', 3, ...
        'TagSynchroX', TagSynchroX, 'TagSynchroY', TagSynchroY,...
        'x', subc, 'XUnit', '# Beam', ...
        'y', y, 'YUnit', YUnit, ...
        'ValNaN', ValNaN, 'CLim', CLim, ...
        'InitialFileName', this.nomFic, ...
        'InitialFileFormat', InitialFileFormat, ...
        'GeometryType', GeometryType, ...
        'SonarDescription', SonarDescription, ...
        'Comments', Comment);
    
    [DataType, ColormapIndex] = ALL.identSonarDataType(nomLayer);
    b = set(b, 'DataType', DataType, 'ColormapIndex', ColormapIndex);
    
    switch nomLayer
        case 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx'
            str = 'xxxxxxxxxxxxxxxxxxx';
        otherwise
            str = [];
    end
    b = update_Name(b, 'Append', str);
    c(end+1) = b; %#ok<AGROW>
    ListeLayers.Order{4,1}(k) = length(c);
end

%% D�termination du signal "Pr�sence de WaterColumn"

[~, WC] = create_signal_presenceWC(this, 'DataDepth', DataDepth);
PresenceWaterColumn = WC.Data(subDepth);

%% D�termination du signal "Pr�sence de FM"

[~, FM] = create_signal_presenceFM(this, 'DataRaw', DataRaw);
PresenceFM = FM.Data(subRaw);

%% D�termination du signal "Nombre de swaths"

% Avant le 14/11/2018
% [~, nbSwaths] = create_signal_nbSwath(this, 'DataRaw', DataRaw);
% nbSwaths = nbSwaths.Data(subRaw);

% Apr�s le 14/11/2018
nbSwaths = DualSwath;
nbSwaths(nbSwaths == 3) = 2;
PingSequence(nbSwaths == 1) = 1;

%% D�termination du signal "Height"

[~, Z] = create_signal_height(this, 'DataDepth', DataDepth, 'subDepth', subDepth);
% SonarHeight = Z.Data(subDepth);
SonarHeight = Z.Data;

%% Valeur du coefficient d'absorption

[flag, signalContainerAbsorptionCoeff_RT] = create_signal_AbsorptionCoeff_RT(this, 'DataDepth', DataDepth, 'DataSeabed', DataSeabed, ...
    'DataRaw', DataRaw, 'DataRuntime', DataRuntime, 'subDepth', subDepth);
if flag
    AbsorptionCoeff_RT = signalContainerAbsorptionCoeff_RT.signalList.ySample.data;
else
    AbsorptionCoeff_RT = [];
end

%% Transmission des signaux

for k=1:length(c)
    c(k) = set(c(k), 'SonarPresenceWC',        PresenceWaterColumn);
    c(k) = set(c(k), 'SonarPresenceFM',        PresenceFM);
    c(k) = set(c(k), 'SonarNbSwaths',          nbSwaths);
    
    c(k) = set(c(k), 'SonarHeight',            SonarHeight);
    c(k) = set(c(k), 'SonarTime',              Time);
    c(k) = set(c(k), 'SonarHeading',           Heading);
    c(k) = set(c(k), 'SonarImmersion',         TransducerDepth);
    c(k) = set(c(k), 'SonarSurfaceSoundSpeed', SurfaceSoundSpeed(subDepth));
    c(k) = set(c(k), 'SonarPortMode_1',        Mode(subDepth));
    
    if isEM2040
        [flag, signalContainerModes] = create_signalsModesEM2040(this, Datetime, Mode2, Mode3); %#ok<ASGLU>
%         c(k) = set(c(k), 'SonarEM2040Mode2',    Mode2);
%         c(k) = set(c(k), 'SonarEM2040Mode3',    Mode3);
    else
        signalContainerModes = SignalContainer.empty();
    end
    
    c(k) = set(c(k), 'SampleFrequency', SampleFrequency); % TODO supprimer quand signalsFromSeabedImage sera branch�
    
    %% Vertical signals from runtime datagrams
    
    c(k) = set_SignalsVert(c(k), ...
        [signalContainerFromRuntime(:)
        signalContainerFromRawRange(:)
        signalContainerFromAttitude(:)
        signalContainerAbsorptionCoeff_RT(:)
        signalContainerFromSeabedImage(:)
        signalContainerModes(:)]);
  
    %%
    
    c(k) = set(c(k), 'SonarRawDataResol', resolutionMajoritaire, 'SonarResolutionD', resolutionMajoritaire);
    
    if ~isempty(RollTx)
        c(k) = set(c(k), 'SonarRoll',  RollTx(subDepth)); % Supprimer ceci lorsque les nouveaux signaux seront pris en compte partout
        % commenter ces lignes en mode "Dev" et laisser venir les bugs :
        % attention � ne pas supprimer totalement les anciens signaux de cl_image pour
        % compatibilit� sonars lat�raux, geoswath et Reson
        c(k) = set(c(k), 'SonarPitch', PitchTx(subDepth));
    end
    c(k) = set(c(k), 'SonarHeave', HeaveTx(subDepth));
    c(k) = set(c(k), 'SonarSpeed', Speed(subDepth));
    
    if ~isempty(TideValue)
        c(k) = set_SonarTide(c(k), TideValue);
    end
    if ~isempty(DraughtValue)
        c(k) = set_SonarDraught(c(k), DraughtValue);
    end
    
    % TODO : TrueHeave
    
    % TODO if flagTideCorrection : mettre une info du type
    % c(k) = set_SonarTideCorrection(c(k), 1), idem pour draught
    % et TrueHeave
    
    c(k) = set(c(k), 'SonarPingCounter', PingCounter);
    if strcmp(SonarName, 'EM710') || strcmp(SonarName, 'EM122') || strcmp(SonarName, 'EM302') || strcmp(SonarName, 'EM2040') || strcmp(SonarName, 'EM2040S') || strcmp(SonarName, 'EM2040D')
        c(k) = set(c(k), 'SonarInterlacing', PingSequence);
    end
    
    c(k) = set(c(k), 'SonarFishLongitude', Longitude(subDepth));
    c(k) = set(c(k), 'SonarFishLatitude',  Latitude(subDepth));
    
    DT = c(k).DataType;
    if (DT == cl_image.indDataType('Reflectivity')) || (DT == cl_image.indDataType('TwoWayTravelTimeInSeconds')) || (DT == cl_image.indDataType('TwoWayTravelTime_s'))
        [subSeabed, pppp] = intersect3(DataSeabed.PingCounter(:,1), DataDepth.PingCounter(subDepth,1), DataDepth.PingCounter(subDepth,1));
        SonarRn  = NaN(length(subDepth), DataSeabed.Dimensions.nbSounders, 'single');
        SonarBSN = NaN(length(subDepth), DataSeabed.Dimensions.nbSounders, 'single');
        SonarBSO = NaN(length(subDepth), DataSeabed.Dimensions.nbSounders, 'single');
        
        if size(DataSeabed.TxBeamWidth,2) == 1
            SonarTxBeamWidth  = NaN(length(subDepth), 1, 'single');
            SonarTVGCrossOver = NaN(length(subDepth), 1, 'single');
        else
            SonarTxBeamWidth  = NaN(length(subDepth), DataSeabed.Dimensions.nbSounders, 'single');
            SonarTVGCrossOver = NaN(length(subDepth), DataSeabed.Dimensions.nbSounders, 'single');
        end
        
        SonarRn(pppp,:)           = DataSeabed.TVGN(subSeabed,:);
        SonarBSN(pppp,:)          = DataSeabed.BSN(subSeabed,:);
        SonarBSO(pppp,:)          = DataSeabed.BSO(subSeabed,:);
        SonarTxBeamWidth(pppp,:)  = DataSeabed.TxBeamWidth(subSeabed,:);
        SonarTVGCrossOver(pppp,:) = DataSeabed.TVGCrossOver(subSeabed,:);
        
        c(k) = set_SonarTVGN(c(k),         SonarRn);
        c(k) = set_SonarBSN(c(k),          SonarBSN);
        c(k) = set_SonarBSO(c(k),          SonarBSO);
        c(k) = set_SonarTxBeamWidth(c(k),  SonarTxBeamWidth);
        c(k) = set_SonarTVGCrossOver(c(k), SonarTVGCrossOver);
        %             clear DataSeabed
        %         end
    end
    
    %% Transfert des param�tres d'�tat de la donn�e
    
    InitialImageName = c(k).InitialImageName;
    switch InitialImageName
        case 'Reflectivity'
            [~, ~, Ext] = fileparts(c(k).InitialFileName);
            switch Ext
                case '.kmall'
                    InfoLambertCompensation = DataRuntime.UseLambertsLaw;
                    InfoLambertCompensation = mode(InfoLambertCompensation(:));
                otherwise
                    I = DataDepth.DetectionInfo(subDepth,:);
                    InfoLambertCompensation = mode(I(:));
                    InfoLambertCompensation = decodeBits(InfoLambertCompensation, 5);
            end

            if InfoLambertCompensation
                EtatSonar = init_BSStatus('EmModel', DataDepth.EmModel, 'Absorption', AbsorptionCoeff_RT, ...
                    'IdentAlgoSnippets2OneValuePerBeam', 2, ...
                    'SonarBS_etat', 1, 'SonarBS_LambertCorrection', 1, 'SonarBS_SpecularReestablishment', 0);
            else
                EtatSonar = init_BSStatus('EmModel', DataDepth.EmModel, 'Absorption', AbsorptionCoeff_RT, ...
                    'IdentAlgoSnippets2OneValuePerBeam', 2, ...
                    'SonarBS_etat', 2, 'SonarBS_LambertCorrection', 0, 'SonarBS_SpecularReestablishment', 1);
            end
            
        case {'ReflectivityFromSnippets'; 'ReflectivityFromSnippetsWithoutSpecularRestablishment'}
            if strcmp(InitialImageName, 'ReflectivityFromSnippets')
                SonarBS_SpecularReestablishment = 1;
            else
                SonarBS_SpecularReestablishment = 0;
            end
            if isfield(DataDepth, 'IdentAlgoSnippets2OneValuePerBeam')
                IdentAlgoSnippets2OneValuePerBeam = DataDepth.IdentAlgoSnippets2OneValuePerBeam;
            else
                IdentAlgoSnippets2OneValuePerBeam = 3; % Energy : C'est comme cela que �a a �t� calcul� pr�c�demment
            end
            EtatSonar = init_BSStatus('EmModel', DataDepth.EmModel, 'Absorption', AbsorptionCoeff_RT, ...
                'SonarBS_etat', 1, 'SonarBS_LambertCorrection', 1, 'SonarAireInso_origine', 1, ...
                'IdentAlgoSnippets2OneValuePerBeam', IdentAlgoSnippets2OneValuePerBeam, ...
                'SonarBS_SpecularReestablishment', SonarBS_SpecularReestablishment);
            
        case 'ReflectivityBestBS'
            SonarBS_SpecularReestablishment = 1;
            if isfield(DataDepth, 'IdentAlgoSnippets2OneValuePerBeam')
                IdentAlgoSnippets2OneValuePerBeam = DataDepth.IdentAlgoSnippets2OneValuePerBeam;
            else
                IdentAlgoSnippets2OneValuePerBeam = 3; % Energy : C'est comme cela que �a a �t� calcul� pr�c�demment
            end
            EtatSonar = init_BSStatus('EmModel', DataDepth.EmModel, 'Absorption', AbsorptionCoeff_RT, ...
                'SonarTVG_origine', 2, ... 
                'SonarBS_etat', 2, 'SonarBS_LambertCorrection', 3, 'SonarAireInso_origine', 3, ...
                'IdentAlgoSnippets2OneValuePerBeam', IdentAlgoSnippets2OneValuePerBeam, ...
                'SonarBS_SpecularReestablishment', SonarBS_SpecularReestablishment);
%                 'SonarTVG_ConstructTypeCompens', 2, ...
           
        otherwise
            EtatSonar = init_BSStatus('EmModel', DataDepth.EmModel, 'Absorption', AbsorptionCoeff_RT, ...
                'SonarBS_etat', 2);
    end
    c(k) = set_BSStatus(c(k), EtatSonar);
    
    %% D�finition d'une carto
    
    try % Pour que Ridha puisse utiliser la version Matlab de SSc alors qu'il n'a pas la Mapping Toolbox
        [c(k), Carto, persistent_Carto] = define_carto(c(k), Carto, nomDir, memeReponses, persistent_Carto, 'CartoAuto', CartoAuto);
        c(k) = set_Carto(c(k), Carto);
    catch
        Carto = [];
    end
    
    %% D�termination du signal "Distance Inter Pings"
    
    if k == 1
        [flag, DIP] = create_signal_InterPingDistance(this, 'DataDepth', DataDepth, 'DataPosition', DataPosition, ...
            'Carto', Carto);
        if flag || ~isempty(DIP.Data)
            DistanceInterPings = DIP.Data(subDepth);
        else
            DistanceInterPings = NaN(length(subDepth), 1);
        end
    end
    c(k) = set(c(k), 'SonarDistPings', DistanceInterPings);
    
    %% Profil de c�l�rit�
    
    if ~isempty(DataSSP)
        c(k) = set_SonarBathyCel(c(k), DataSSP);
    end
    
    %% Param�tres d'installation
    
    if ~isempty(DataInstalParam) && ~isempty(DataInstalParam.Datetime)
        c(k) = set_SonarInstallationParameters(c(k), DataInstalParam);
    end
end
if isempty(c)
    return
end

%% Classement des images dans l'ordre demand�

if isfield(ListeLayers, 'Order') && ~isempty(ListeLayers.Order)
    k = 1;
    for j1=1:size(ListeLayers.Order, 1)
        for j2=1:size(ListeLayers.Order, 2)
            for i=1:length(ListeLayers.Order{j1,j2})
                if ListeLayers.Order{j1,j2}(i) ~= 0
                    d(ListeLayers.Order{j1,j2}(i)) = c(k); %#ok<AGROW>
                    k = k + 1;
                end
            end
        end
    end
    c = d;
end

%% Edition de l'objet cli_image

if nargout == 0
    SonarScope(c);
end



function [I, DataType, ImageName] = getBathy(DataDepth, subDepth, subc, Mask, NomImage, ...
    flagTideCorrection, TideValue, ...
    flagDraughtCorrection, DraughtValue, ...
    TransducerDepth, Mute)

% TODO : copy de ce qui est au dessus : simplifier cela
I = DataDepth.Depth(subDepth,subc);
I(Mask) = NaN;

ImageName = [NomImage '_Depth-Heave'];
if flagTideCorrection
    ImageName = [ImageName '+Tide'];
end

if flagDraughtCorrection
    ImageName = [ImageName '+Draught'];
end


% if flagTideCorrection && all(isnan(TideValue(subDepth)))
if flagTideCorrection && all(isnan(TideValue)) % Modif JMA le 01/05/2020
    if ~Mute
        str1 = sprintf('La mar�e n''est pas d�finie pour %s.\nLa valeur 0 est impos�e ici.', ImageName);
        str2 = sprintf('The tide value is not defined for %s.\nIt is set to 0 here', ImageName);
        my_warndlg(Lang(str1,str2), 0, 'displayItEvenIfSSc', 1, 'TimeDelay', 60)
    end
    TideValue(:) = 0;
end

% if flagDraughtCorrection && all(isnan(DraughtValue(subDepth)))
if flagDraughtCorrection && all(isnan(DraughtValue)) % Modif JMA le 01/05/2020
    if ~Mute
        str1 = sprintf('Draught n''est pas d�fini pour %s.\nLa valeur 0 est impos�e ici.', ImageName);
        str2 = sprintf('The Draught value is not defined for %s.\nIt is set to 0 here.', ImageName);
        my_warndlg(Lang(str1,str2), 0, 'displayItEvenIfSSc', 1)
    end
    DraughtValue(:) = 0;
end
    
% TODO : re�crire cela en matriciel : Matrice+Vecteur
for iPing=1:length(subDepth)
%     I(iPing,:) = I(iPing,:) + (TransducerDepth(iPing) - Heave(iPing, :));
%     I(iPing,:) = I(iPing,:) - Heave(subDepth(iPing), :);
    I(iPing,:) = I(iPing,:) + TransducerDepth(iPing); % Modif JMA le 25/11/2020 (�changes de mail avec Herv�)
    if flagTideCorrection
        I(iPing,:) = I(iPing,:) + TideValue(iPing);
    end
    if flagDraughtCorrection
        I(iPing,:) = I(iPing,:) + DraughtValue(iPing);
    end
end
DataType = cl_image.indDataType('Bathymetry');

J = DataDepth.DetectionInfo(subDepth,subc);
J(J>=128) = NaN;
I(isnan(J)) = NaN;



function messageLoadImage(NomImage, nomLayer, Mute)
if ~Mute
    disp(['  --> Loading layer : ' NomImage ' - Layer : ' nomLayer])
end
