% Visualisation du contenu des datagrams "Seabed Image" contenus dans un fichier .all
%
% Syntax
%   [b,S] = import_SeabedImage_All(aKM, ...);
%
% Input Arguments
%   aKM : Instance de cl_simrad_all
%
% Name-Value Pair Arguments
%   subl : Numeros de lignes � traiter
%
% Name-only Arguments
%   FaisceauxPairs         : Pour cr�er une image uniquement avec les faisceaux pairs
%   FaisceauxImairs        : Pour cr�er une image uniquement avec les faisceaux impairs
%   NoSpecularCompensation : Pour ne pas faire de correction du sp�culaire
%
% Output Arguments
%   b          : Instance de cli_image
%   subl_Image : Numeros de lignes communs avec la bathy (index relatis � seabedImage)
%   subl_Depth : Numeros de lignes communs avec la bathy (index relatis � Depth)
%
% Examples
%   nomFic = getNomFicDatabase('EM1002_0001_20040916_054013_raw.all')
%   aKM = cl_simrad_all('nomFic', nomFic);
%   b = import_SeabedImage_All(aKM);
%   SonarScope(b);
%
%   b = import_SeabedImage_All(aKM, 'subl', 1:100);
%   SonarScope(b);
%
% See also cl_simrad_all cl_simrad_all/plot_position cl_simrad_all/plot_nav Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function varargout = import_SeabedImage_All(this, varargin)

persistent persistent_ListeLayers persistent_Carto

[varargin, Memmapfile] = getPropertyValue(varargin, 'Memmapfile', 0);
[varargin, IndNavUnit] = getPropertyValue(varargin, 'IndNavUnit', []);

varargout{1} = [];
varargout{2} = [];
varargout{3} = [];
varargout{4} = [];
varargout{5} = [];

%% Check the number of elements

flag = checkIfOneElement(length(this));
if ~flag
    return
end

%%

[varargin, subl]         = getPropertyValue(varargin, 'subl',         []);
[varargin, Carto]        = getPropertyValue(varargin, 'Carto',        []);
[varargin, memeReponses] = getPropertyValue(varargin, 'memeReponses', false);
[varargin, ListeLayers]  = getPropertyValue(varargin, 'ListeLayers',  []); %#ok<ASGLU>

%% Lecture de la donn�e

if isempty(Carto)
    if ~memeReponses && ~isempty(persistent_Carto)
        Carto = persistent_Carto;
    end
end

if isempty(ListeLayers)
    if ~memeReponses
        clear str
        str{1} = 'Reflectivity';
        str{2} = 'AveragedPtsNb';
        str{3} = 'TxAngle';
        str{4} = 'TxBeamIndex';
        str{5} = 'RxBeamIndex ';
        str{6} = 'AlongDist';
        str{7} = 'TwoWayTravelTimeInSamples';
        str{8} = 'TxBeamIndexSwath';
        %         str{9} = 'BeamPointingAngle';
        ListeLayers = my_listdlg('Lay.xml :', str, 'InitialValue', [1 3 4 5 6]);
        if isempty(ListeLayers)
            return
        end
        persistent_ListeLayers = ListeLayers;
    else
        ListeLayers = persistent_ListeLayers;
    end
end
if isempty(ListeLayers)
    str1 = 'Vous devez s�lectionner au moins un layer.';
    str2 = 'You must select at least one layer';
    my_warndlg(Lang(str1,str2), 1);
    return
end

%% Lecture de la bathy

[flag, DataDepth] = read_depth_bin(this, 'Memmapfile', Memmapfile);
if ~flag
    return
end
if isfield(DataDepth, 'MaskPingsReflectivity') && ~isempty(DataDepth.MaskPingsReflectivity)
    MaskPingsReflectivity = DataDepth.MaskPingsReflectivity(:);
else
    MaskPingsReflectivity = false(DataDepth.Dimensions.nbPings,1);
end

%% Lecture de l'imagerie

[flag, b, DataSeabedImage, Carto] = read_PingAcrossSample(this, 'ListeLayers', ListeLayers, 'Carto', Carto, ...
    'MaskPingsReflectivity', MaskPingsReflectivity, 'IndNavUnit', IndNavUnit);
if ~flag
    return
end
if ~memeReponses
    persistent_Carto = Carto;
end

%% Recherche de la zone commune

nbPings = length(DataSeabedImage.PingCounter);
if isempty(subl)
    subl = 1:nbPings;
end
subl((subl < 1) | (subl > nbPings)) = [];

%% Recherche de la zone commune

% [pppp, sub] = int.xmlect(DataSeabedImage.PingCounter(subl,1), DataDepth.PingCounter(:,1));
% subl_Image = subl(sub);
% [pppp, subl_Depth] = interect(DataDepth.PingCounter(subl,1), DataSeabedImage.PingCounter(subl, 1));

% TODO : Avec le fichier 0300_20081024_034958_raw.all du NIWA on a des
% num�ros de pings � NaN  : � corriger

[subl_Image, subl_Depth] = intersect3(DataSeabedImage.PingCounter(subl,1), DataDepth.PingCounter(:,1), DataDepth.PingCounter(:,1));

%{
comment� par JMA le 19/06/2012 car extraction d�j� faite dans read_PingAcrossSample. Donn�e de test : EM1002 Nouvelle-Cal�donie mission CALGON fichier 20040512_015633_raw.all (Charline)
if ~isequal(subl_Image(:), (1:nbPings)')
for i=1:numel(b)
ImageName = b(i).Name;
b(i) = extraction(b(i), 'suby', subl_Image);
b(i) = set(b(i), 'Name', ImageName);
end
end
%}

%% Edition de l'objet cli_image

if nargout == 0
    SonarScope(b);
else
    varargout{1} = b;
    varargout{2} = Carto;
    varargout{3} = subl_Image;
    varargout{4} = subl_Depth;
    varargout{5} = memeReponses;
end
