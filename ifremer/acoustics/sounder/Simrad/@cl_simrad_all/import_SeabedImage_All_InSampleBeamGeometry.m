function [b, Carto] = import_SeabedImage_All_InSampleBeamGeometry(this, varargin)

[varargin, Carto]   = getPropertyValue(varargin, 'Carto',   []);
[varargin, subPing] = getPropertyValue(varargin, 'subPing', []); %#ok<ASGLU>

b = [];

%% Check the number of elements

flag = checkIfOneElement(length(this));
if ~flag
    return
end

%%

% subPing = 1:50:1000

[flag, DataDepth] = read_depth_bin(this, 'Memmapfile', 1);
if ~flag
    return
end

[flag, DataSeabed, SeabedImageFormat] = read_seabedImage_bin(this, 'Memmapfile', 1);
if ~flag
    return
end
if SeabedImageFormat == 1
    DataRaw = [];
else
    [flag, DataRaw] = read_rawRangeBeamAngle(this, 'Memmapfile', 1);
    if ~flag
        return
    end
end

if SeabedImageFormat == 1
    [subl_Depth, subl_Image] = intersect3(DataDepth.PingCounter(subPing,1), DataSeabed.PingCounter(:,1), DataSeabed.PingCounter(:,1));
    subl_Raw = [];
else
    [subl_Depth, subl_Image, subl_Raw] = intersect3(DataDepth.PingCounter(subPing,1), DataSeabed.PingCounter(:,1), DataRaw.PingCounter(:,1));
end
sublDepth = subPing(subl_Depth); % Rajout� le 8/4/2010



% Lecture de InstallationParameters

[flag, DataInstallationParameters, isDual] = read_installationParameters_bin(this);
if ~flag
    return
end

[flag, BeamDataOut] = get_PingSeabedImageryInWaterColumn(this, DataSeabed, DataDepth, DataRaw, SeabedImageFormat, sublDepth, subl_Raw, subl_Image);
if ~flag
    return
end

% [flag, IdentSonar] = SimradModel2IdentSonar(DataSeabed.Model, DataDepth.EmModel);
[flag, IdentSonar] = SimradModel2IdentSonar(DataSeabed.EmModel, DataDepth.EmModel);
if ~flag
    return
end

SystemSerialNumber = DataSeabed.SystemSerialNumber(1); % TODO : indexer correctement ce truc
SonarDescription = cl_sounder('Sonar.Ident', IdentSonar, 'Sonar.Mode_1', 1, ...
    'Sonar.SystemSerialNumber', SystemSerialNumber);

TxPulseWidth = get(SonarDescription, 'Signal.Duration');

if isempty(DataSeabed) || isempty(BeamDataOut)
    return
end

N = length(sublDepth);
str1 = 'Chargement des snippets';
str2 = 'Loading Snippets';
hw = create_waitbar(Lang(str1,str2), 'N', N);
b = repmat(cl_image, 1, N);
for k=1:N
    my_waitbar(k, N, hw)
    iPingDepth  = sublDepth(k);
    iPingSeabed = subl_Image(k);
    
    [~, TagSynchro] = fileparts(this.nomFic);
    TagSynchro = sprintf('%s_%d', TagSynchro, iPingDepth);
    NomImage = TagSynchro;
    
    DataPing = ExtractionDataPing(DataSeabed, DataDepth, iPingSeabed, iPingDepth, SeabedImageFormat, BeamDataOut(k).DetectedRangeInSamples);
    
    if SeabedImageFormat == 1
        DataPing.TxAngle = DataDepth.BeamDepressionAngle(iPingDepth,:);
    else
        iPingRaw = subl_Raw(k);
        DataPing.TxAngle = DataRaw.BeamPointingAngle(iPingRaw,:);
        DataPing.TxAngle = Simrad_AnglesAntenna2ShipFloor(DataPing.TxAngle, DataInstallationParameters, isDual);
    end
    DataPing.TransmitSectorNumber = BeamDataOut(k).TransmitSectorNumber;
    DataPing.TxPulseWidth = TxPulseWidth;
    fe = DataPing.SampleRate;
    
    switch DataPing.EmModel
        case 3020
            RangeMax = 1000; % (m)
        otherwise
            RangeMax = Inf;
    end
    
    RangeMax = min(floor(2* fe * RangeMax / 1500), size(BeamDataOut(k).Samples,1));
    
    Amplitude = BeamDataOut(k).Samples(1:RangeMax,:);
    %     Phase     = BeamDataOut(k).Samples(1:RangeMax,:);
    
    %     R = (BeamDataOut(k).DetectedRangeInSamples + 1); % Modif JMA le 12/09/2017
    R = BeamDataOut(k).DetectedRangeInSamples; % Modif JMA le 12/09/2017
    Angles = DataPing.TxAngle;
    C = DataPing.SoundVelocity;
    
    if length(unique(DataPing.SamplingRateMinMax)) == 1
        D = R * C / (2*fe); % Ajout� le 2* le 11/11/11
    else
        feBab = DataPing.SamplingRateMinMax(1);
        feTri = DataPing.SamplingRateMinMax(2);
        subTri = (BeamDataOut(k).TransmitSectorNumber == 2);
        D(~subTri) = R(~subTri) * C / (2*feBab); % Ajout� le 2* le 11/11/11
        D( subTri) = R( subTri) * C / (2*feTri); % Ajout� le 2* le 11/11/11
    end
    Z = -D .* cosd(Angles);
    % figure; plot(DataDepth.TransducerDepth); grid
    if size(DataDepth.TransducerDepth, 2) == 1
        Z = Z - DataDepth.TransducerDepth(iPingDepth,1);
    else
        nbBeams = size(DataDepth.Depth, 2);
        subBab = 1:floor(nbBeams/2);
        subTri = (1+floor(nbBeams/2)):nbBeams;
        Z(subBab) = Z(subBab) - DataDepth.TransducerDepth(iPingDepth,1);
        Z(subTri) = Z(subTri) - DataDepth.TransducerDepth(iPingDepth,2);
    end
    
    if isfield(DataDepth, 'DetectionInfo')
        subAmp = find(mod(DataDepth.DetectionInfo(iPingDepth,:),2) == 0);
        subPhs = find(mod(DataDepth.DetectionInfo(iPingDepth,:),2) == 1);
    else
        subAmp = find(DataDepth.QualityFactor(iPingDepth,:) < 128);
        subPhs = find(DataDepth.QualityFactor(iPingDepth,:) >= 128);
    end
    figure(7000+k);
    clear hc
    hc(1,1) = subplot(4,2,1);  hold off; PlotUtils.createSScPlot(Angles, '-ok'); grid on; title(sprintf('Angles for iPing=%d', iPingDepth))
    hc(2,1) = subplot(4,2,3);  hold off; PlotUtils.createSScPlot(R, '-ok'); grid on; title(sprintf('Kongsberg Detection for iPing=%d', iPingDepth)); set(gca,'YDir', 'Reverse')
    hc(3,1) = subplot(4,2,5);  hold off; PlotUtils.createSScPlot(Z, '-ok'); grid on; title(sprintf('Pseudo Z for iPing=%d', iPingDepth))
    
    set(hc(2,1), 'Tag', 'BottomDetector', 'UserData', NomImage);
    setappdata(hc(2,1), 'TagSynchroYScale', 1/fe)
    
    hc(4,1) = subplot(4,2,7);
    X = D .* sind(Angles);
    hold off; PlotUtils.createSScPlot(X, Z, '-ok'); grid on; title(sprintf('Pseudo Z for iPing=%d', iPingDepth))
    
    hc(1,2) = subplot(4,2,2);  hold off; PlotUtils.createSScPlot(subAmp, Angles(subAmp), '*r'); grid on; title(sprintf('Angles for iPing=%d', iPingDepth))
    hc(2,2) = subplot(4,2,4);  hold off; PlotUtils.createSScPlot(subAmp, R(subAmp), '*r'); grid on; title(sprintf('Kongsberg Detection for iPing=%d', iPingDepth)); set(gca,'YDir', 'Reverse')
    hc(3,2) = subplot(4,2,6);  hold off; PlotUtils.createSScPlot(subAmp, Z(subAmp), '*r'); grid on; title(sprintf('Pseudo Z for iPing=%d', iPingDepth))
    hc(1,2) = subplot(4,2,2);  hold on; PlotUtils.createSScPlot(subPhs, Angles(subPhs), '*k'); grid on; title(sprintf('Angles for iPing=%d', iPingDepth))
    hc(2,2) = subplot(4,2,4);  hold on; PlotUtils.createSScPlot(subPhs, R(subPhs), '*k'); grid on; title(sprintf('Kongsberg Detection for iPing=%d', iPingDepth)); set(gca,'YDir', 'Reverse')
    hc(3,2) = subplot(4,2,6);  hold on; PlotUtils.createSScPlot(subPhs, Z(subPhs), '*k'); grid on; title(sprintf('Pseudo Z for iPing=%d', iPingDepth))
    hc(4,2) = subplot(4,2,8);
    X = D .* sind(Angles);
    hold off; PlotUtils.createSScPlot(X(subAmp), Z(subAmp), '+r'); grid on; title(sprintf('Pseudo Z for iPing=%d', iPingDepth))
    hold on;  PlotUtils.createSScPlot(X(subPhs), Z(subPhs), '+k'); grid on; title(sprintf('Pseudo Z for iPing=%d', iPingDepth))
    hc1 = hc(1:3,:);
    linkaxes(hc1(:), 'x')
    %     linkaxes(hc(1,:))
    %     linkaxes(hc(2,:))
    %     linkaxes(hc(3,:))
    linkaxes(hc(4,:)); axis tight; drawnow;
    
    %% Cr�ation de l'image de la r�flectivit�
    
    [~, TagSynchro] = fileparts(this.nomFic);
    TagSynchroX = sprintf('%s_%d_%d', TagSynchro, BeamDataOut(k).iPing, DataSeabed.Dimensions.nbBeams);
    TagSynchroY = sprintf('%s_%d',    TagSynchro, BeamDataOut(k).iPing);
    NomImage = TagSynchroY;
    
    b(1,k) = cl_image;
    if isa(Amplitude, 'cl_memmapfile')
        b(1,k) = set_Image(b(1,k), Amplitude);
    else
        if N > 1
            b(1,k) = set_Image(b(1,k), cl_memmapfile('Value', Amplitude, 'LogSilence', 1));
        else
            b(1,k) = set_Image(b(1,k), Amplitude, 'NoMemmapfile');
        end
    end
    b(1,k).YDir              = 2;
    b(1,k).Name              =  NomImage;
    b(1,k).Unit              = 'dB';
    b(1,k).TagSynchroX       = TagSynchroX;
    b(1,k).TagSynchroY       = TagSynchroY;
    b(1,k).TagSynchroYScale  = 1/fe;
    b(1,k).XUnit             = 'Beam #';
    b(1,k).YUnit             = 'Sample #';
    b(1,k).InitialFileName   = this.nomFic;
    b(1,k).InitialFileFormat = 'SimradAll';
    b(1,k).Comments = sprintf('Reson PingCounter : %d', DataPing.PingCounter);
    
    b(1,k).GeometryType = cl_image.indGeometryType('SampleBeam');
    b(1,k) = set_SonarDescription(b(1,k), SonarDescription);
    %     b(1,k).Comments    = Comments{ListeLayers(k)};
    %     b(1,k).strMaskBits = strMaskBits;
    
    b(1,k) = set_SampleBeamData(b(1,k), DataPing);
    
    b(1,k).DataType = cl_image.indDataType('Reflectivity');
    b(1,k).ColormapIndex = 3;
    
    %     if ~isempty(DataSoundSpeedProfile)
    %         SoundSpeedProfile.Z = DataSoundSpeedProfile.Depth;
    %         SoundSpeedProfile.T = DataSoundSpeedProfile.WaterTemperature;
    %         SoundSpeedProfile.S = DataSoundSpeedProfile.Salinity;
    %         SoundSpeedProfile.C = DataSoundSpeedProfile.SoundSpeed;
    %         b(1,k) = set_SonarBathyCel(b(1,k), SoundSpeedProfile);
    %     end
    b(1,k) = update_Name(b(1,k));
    b(1,k).Writable = false;
    
    %     b(1,k) = compute_stats(b(1,k));
    %     b(1,k).TagSynchroContrast = num2str(rand(1));
    %     StatValues = b(1,k).StatValues;
    %     CLim = [StatValues.Min StatValues.Max];
    %     b(1,k).CLim = CLim;
    
    % ----------------------
    % Definition d'une carto
    
    while isempty(Carto)
        %         Carto = getDefinitionCarto(b(1,k), 'NoGeodetic', 1);
        Carto = getDefinitionCarto(b(1,k), 'NoGeodetic', 0);
    end
    b(1,k) = set_Carto(b(1,k), Carto);
    
    
    %% Cr�ation de l'image donnant la direction des samples
    
    BeamSortDirection = BeamDataOut(k).BeamSortDirection(1:RangeMax,:);
    b(2,k) = cl_image;
    if isa(Amplitude, 'cl_memmapfile')
        b(2,k) = set_Image(b(2,k), BeamSortDirection);
    else
        if N > 1
            b(2,k) = set_Image(b(2,k), cl_memmapfile('Value', BeamSortDirection, 'LogSilence', 1));
        else
            b(2,k) = set_Image(b(2,k), BeamSortDirection, 'NoMemmapfile');
        end
    end
    b(2,k).YDir               = 2;
    b(2,k).Name               = NomImage;
    b(2,k).Unit               = '';
    b(2,k).TagSynchroX        = TagSynchroX;
    %  b(2,k).TagSynchroXScale = 1/size(Amplitude,2);
    b(2,k).TagSynchroY        = TagSynchroY;
    b(2,k).TagSynchroYScale   = 1/fe;
    b(2,k).TagSynchroContrast = num2str(rand(1));
    b(2,k).XUnit              = 'Beam #';
    b(2,k).YUnit              = 'Sample #';
    b(2,k).InitialFileName    = this.nomFic;
    b(2,k).InitialFileFormat  = 'SimradAll';
    
    b(2,k).GeometryType       = cl_image.indGeometryType('SampleBeam');
    b(2,k) = set_SonarDescription(b(2,k), SonarDescription);
    b(2,k) = set_SampleBeamData(b(2,k), DataPing);
    b(2,k).DataType           = 1; %cl_image.indDataType('TxAngle'));
    b(2,k).ColormapIndex      = 3;
    b(2,k) = compute_stats(b(2,k));
    b(2,k).TagSynchroContrast = num2str(rand(1));
    b(2,k).CLim               = [0 2];
    
    b(2,k) = update_Name(b(2,k));
    b(2,k).Writable = false;
end
b = b(:);
my_close(hw)

%% Edition de l'objet cli_image

if nargout == 0
    SonarScope(b);
end


function DataPing = ExtractionDataPing(DataSeabed, DataDepth, iPingSeabed, iPingDepth, SeabedImageFormat, DetectedRangeInSamples)

DataPing.PingCounter        = DataSeabed.PingCounter(iPingSeabed);
DataPing.SystemSerialNumber = DataSeabed.SystemSerialNumber(:,:);

if SeabedImageFormat == 1
    DataPing.SampleRate = DataDepth.SamplingRate(iPingDepth,1);
else
    DataPing.SampleRate  = DataSeabed.SamplingRate(iPingSeabed,1);
end
DataPing.EmModel      = DataDepth.EmModel;
if isfield(DataSeabed, 'Position') % A conserver tant qu'on peut lire les donn�es ancienne lecture
    DataPing.Position = DataSeabed.Position(iPingSeabed);
end
DataPing.SoundVelocity      = DataDepth.SoundSpeed(iPingDepth);
DataPing.SamplingRateMinMax = DataDepth.SamplingRate(iPingDepth,:);

DataPing.Depth           = DataDepth.Depth(iPingDepth,:);
DataPing.AcrossDist      = DataDepth.AcrossDist(iPingDepth,:);
DataPing.R1SamplesFiltre = DetectedRangeInSamples;

T = DataSeabed.Time;
DataPing.Time = T(iPingSeabed);
