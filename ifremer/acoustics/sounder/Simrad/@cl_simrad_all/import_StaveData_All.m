% Visualisation du contenu des datagrams "Stave Data" contenus dans un fichier .all
%
% Syntax
%   [b,S] = import_StaveData_All(aKM, DataStave, SonarDescription, ...);
%
% Input Arguments
%   aKM : Instance de cl_simrad_all
%   DataStave, SonarDescription : structure qu'il est pr�f�rable de lire en amont pour �viter de les relires � chaque lecture d'un ping
%
% Name-Value Pair Arguments
%   subl : Numeros de lignes � traiter
%
% Name-only Arguments
%
% Output Arguments
%   b          : Instance de cli_image
%   subl_Image : Numeros de lignes communs avec la bathy (index relatis � seabedImage)
%
% Examples
%   nomFic = getNomFicDatabase('0001_20120511_120513_FK_EM710.all');
%   nomFic = 'L:\FalkorMay2012\EM710\sphere_calibration\0014_20120515_192138_FK_EM710.all';
%   aKM = cl_simrad_all('nomFic', nomFic);
%   b = import_StaveData_All(aKM);
%   SonarScope(b);
%
%   b = import_StaveData_All(aKM, 'subl', 1);
%   SonarScope(b);
%
% See also cl_simrad_all cl_simrad_all/plot_position cl_simrad_all/plot_nav Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [b, Carto, TransmitSectorNumber, R] = import_StaveData_All(this, ...
    DataDepth, DataStave, DataRaw, DataPosition, SonarDescription, varargin)

[varargin, Carto]   = getPropertyValue(varargin, 'Carto',   []);
[varargin, Mute]    = getPropertyValue(varargin, 'Mute',     0);
[varargin, subPing] = getPropertyValue(varargin, 'subPing', []); %#ok<ASGLU>

b                    = [];
TransmitSectorNumber = [];
R                    = [];

%% Check the number of elements

flag = checkIfOneElement(length(this));
if ~flag
    return
end

%%

if ~Mute
    figure(5555); plot(DataDepth.Time, DataDepth.PingCounter(:,1)-DataDepth.PingCounter(1,1), '-*'); grid on
    hold on; plot(DataStave.Time, DataStave.PingCounter(:,1)-DataDepth.PingCounter(1,1), '-or');
    xlabel('Time'); ylabel('Ping Counter')
    legend({'Stave datagram'; 'Stave datagram'})
end

T1 = DataDepth.PingCounter(:,1);
T2 = DataStave.PingCounter(:,1);
T3 = DataRaw.PingCounter(:,1);

if all(subPing < 0)
    subStave = T2;
else
    [subDepth, subStave] = intersect3(T1(subPing), T2, T3);
    subDepth = subPing(subDepth); % Rajout� le 8/4/2010
    if isempty(subStave)
        str1 = 'Ce ping n''existe pas en Stave Data';
        str2 = 'This ping does not exist in Stave Data';
        my_warndlg(Lang(str1,str2), 0, 'Tag', 'PasDePingStaveData');
        return
    end
end

if isempty(DataStave)
    return
end

Longitude = my_interp1_longitude(DataPosition.Time, DataPosition.Longitude, DataStave.Time, 'linear', 'extrap');
Latitude  = my_interp1(          DataPosition.Time, DataPosition.Latitude,  DataStave.Time, 'linear', 'extrap');
Heading   = my_interp1(          DataPosition.Time, DataPosition.Heading,   DataStave.Time, 'linear', 'extrap');

[~, filename] = fileparts(this.nomFic);
N = length(subStave);
hw = create_waitbar('Loading Stave Data : 2/2', 'N', N);
b = repmat(cl_image, 1, N);
for k=1:N
    my_waitbar(k, N, hw)
    iPing = subStave(k);
    
%     TagSynchro = sprintf('%s_%d', TagSynchro, subStave(k));
    TagSynchro = sprintf('%Staves_s_%d', filename, subStave(k));
    NomImage = sprintf('%s-Staves-%d', filename, subStave(k));
    
%     DataPing = Extraction(DataStave, iPing);
    DataPing = Extraction(DataStave, k); % Modif JMA le 16/06/2021
    
%     DataPing.Latitude  = Latitude( subStave(k));
%     DataPing.Longitude = Longitude(subStave(k));
%     DataPing.Heading   = Heading(  subStave(k));
    DataPing.Latitude  = Latitude(k);
    DataPing.Longitude = Longitude(k);
    DataPing.Heading   = Heading(k);
    
%     Amplitude = DataStave.StaveBackscatter(:,:,iPing);
    Amplitude = DataStave.StaveBackscatter(:,:,k);
    
    b(1,k) = cl_image;
    if isa(Amplitude, 'cl_memmapfile')
        b(1,k) = set_Image(b(1,k), Amplitude);
    else
        if N > 1
            b(1,k) = set_Image(b(1,k), cl_memmapfile('Value', Amplitude, 'LogSilence', 1));
        else
            b(1,k) = set_Image(b(1,k), Amplitude, 'NoMemmapfile');
        end
    end
    
    b(1,k).x                 = 1:size(Amplitude,2);
    b(1,k).y                 = 1:size(Amplitude,1);
    b(1,k).YDir              = 2;
    b(1,k).Name              = NomImage;
    b(1,k).Unit              = 'dB';
    b(1,k).TagSynchroX       = TagSynchro;
    b(1,k).TagSynchroY       = TagSynchro;
    b(1,k).XUnit             = 'Beam #';
    b(1,k).YUnit             = 'Sample #';
    b(1,k).InitialFileName   = this.nomFic;
    b(1,k).InitialFileFormat = 'SimradAll';
    b(1,k).Comments          = sprintf('PingCounter : %d', DataPing.PingCounter);
    b(1,k).GeometryType      = cl_image.indGeometryType('SampleStave');
    
    b(1,k) = set_SonarDescription(b(1,k), SonarDescription);
    
    b(1,k) = set_SampleBeamData(b(1,k), DataPing);
    
    b(1,k).DataType      = cl_image.indDataType('Reflectivity');
    b(1,k).ColormapIndex = 3;
    b(1,k) = update_Name(b(1,k));
    b(1,k).Writable      = false;
    
    %% Definition d'une carto
    
    while isempty(Carto)
        Carto = getDefinitionCarto(b(1,k), 'NoGeodetic', 0);
    end
    b(1,k) = set_Carto(b(1,k), Carto);
end
b = b(:);
my_close(hw)


function DataPing = Extraction(DataWC, k)

% TODO : (k,:) pour sondeur dual SVP !!!!

DataPing.PingCounter        = DataWC.PingCounter(k,1);
DataPing.SystemSerialNumber = DataWC.SystemSerialNumber(k,1);
DataPing.EmModel            = DataWC.EmModel;
DataPing.SoundVelocity      = DataWC.SoundSpeed(k,1);

T = DataWC.Time;
DataPing.Time = T(k);
