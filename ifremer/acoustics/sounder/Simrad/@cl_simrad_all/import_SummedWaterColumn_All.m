% [flag, nomFic] = FtpUtils.getSScDemoFile('0029_20180306_213505_SimonStevin.all', 'OutputDir', 'D:\Temp\ExData');
% if flag
%     aKM = cl_simrad_all('nomFic', nomFic);
%     [flag, A] = import_SummedWaterColumn_All(aKM, 'Raw');
%     if flag
%         SonarScope(A)
%     end
% end

function [flag, aKM, subDepth] = import_SummedWaterColumn_All(this, Tag, varargin)

[varargin, subDepth]            = getPropertyValue(varargin, 'subDepth',            []);
[varargin, DataDepth]           = getPropertyValue(varargin, 'DataDepth',           []);
[varargin, DataRaw]             = getPropertyValue(varargin, 'DataRaw',             []);
[varargin, DataWC]              = getPropertyValue(varargin, 'DataWC',              []);
[varargin, DataSeabed]          = getPropertyValue(varargin, 'DataSeabed',          []);
[varargin, DataInstalParam]     = getPropertyValue(varargin, 'DataInstalParam',     []);
[varargin, DataRuntime]         = getPropertyValue(varargin, 'DataRuntime',         []);
[varargin, DataSSP]             = getPropertyValue(varargin, 'DataSSP',             []);
[varargin, DataAttitude]        = getPropertyValue(varargin, 'DataAttitude',        []);
[varargin, TypeMeanUnit]        = getPropertyValue(varargin, 'TypeMeanUnit',        1);
[varargin, CorrectionTVG]       = getPropertyValue(varargin, 'CorrectionTVG',       2);
[varargin, AngleLim]            = getPropertyValue(varargin, 'AngleLim',            []);
[varargin, MaskFlaggedPings]    = getPropertyValue(varargin, 'MaskFlaggedPings',    1);
[varargin, maskOverlapingBeams] = getPropertyValue(varargin, 'maskOverlapingBeams', 0); %#ok<ASGLU>

aKM = [];

%% Contr�le si le cache Netcdf existe

flagNetcdf = existCacheNetcdf(this.nomFic);
if ~flagNetcdf
    [flag, fid] = ALL_getFidWCSampleBeamBufferXMLBin(this.nomFic);
    if ~flag
        return
    end
end

%% Lecture des donn�es

if isempty(DataDepth)
    [flag, DataDepth] = read_depth_bin(this, 'Memmapfile', 0); % -1 quand memmapfile sera adapt� � Netcdf
    if ~flag
        return
    end
end

if isempty(subDepth)
    PCDepth = DataDepth.PingCounter(:,1);
else
    PCDepth = DataDepth.PingCounter(subDepth,1);
end

if isempty(DataRaw)
    [flag, DataRaw] = read_rawRangeBeamAngle(this, 'MemmapFile', 0); % -1 quand memmapfile sera adapt� � Netcdf
    if ~flag
        return
    end
end
PCRaw = DataRaw.PingCounter(:,1);

if isempty(DataWC)
    [flag, DataWC] = read_WaterColumn_bin(this, 'Memmapfile', 0);
    if ~flag
        return
    end
end
PCWC = DataWC.PingCounter(:,1);

[subDepth, subRaw, subWC] = intersect3(PCDepth, PCRaw, PCWC);

if isempty(DataInstalParam)
    [flag, DataInstalParam, isDual] = read_installationParameters_bin(this, 'Memmapfile', 1);
    if ~flag
        return
    end
else
    isDual = DataInstalParam.isDual;
end

I = DataRaw.BeamPointingAngle(subRaw,:);
[~, maskOverlap] = Simrad_AnglesAntenna2ShipFloor(I, DataInstalParam, isDual);
clear I
    
%% Calcul de la moyenne de la r�flectivit� des faisceaux pour chaque ping

N = length(subDepth);
R0Max = 1;
R0 = NaN(N,1);
ImageWCPingSample = NaN(N, 1, 'single');
hw = create_waitbar('Processing', 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    
    iPingWC = subWC(k);

    %% Lecture de la matrice de la colonne d'eau pour le ping
    
    if flagNetcdf
        [flag, S] = ALL_getPingWCSampleBeamBufferNetcdf(DataWC, iPingWC);
    else
        [flag, S] = ALL_getPingWCSampleBeamBufferXMLBin(DataWC, iPingWC, fid);
    end
    if ~flag
        N = k-1;
        break
    end
    [~, ImageWCSampleBeam] = ALL_getPingWCSampleBeam(DataWC, iPingWC, S);
    
    %% Compensation 
    
    if strcmp(Tag, 'Comp')
        if isfield(DataWC, 'TransmitSectorNumber') % TxBeamIndexSwath
            TransmitSectorNumber = DataWC.TransmitSectorNumber(iPingWC, :);
        else
            TransmitSectorNumber = [];
        end
        ImageWCSampleBeam = compensation_WaterColumnSectors(ImageWCSampleBeam, TransmitSectorNumber, TypeMeanUnit);
    end

    %% Masquage ping entier

    if MaskFlaggedPings && DataWC.FlagPings(iPingWC)
        continue
    end
    
    %% Masquage zone angulaire de recouvrement si sondeur dual
    
    if maskOverlapingBeams && ~isempty(maskOverlap)
        maskOverlapPing = maskOverlap(k,:);
        ImageWCSampleBeam(:,maskOverlapPing) = NaN;
    end
    
    %% Masquage en fonction des angles
    
    if ~isempty(AngleLim)
        A = DataWC.BeamPointingAngle(iPingWC,:);
        sub1 = ((A < min(AngleLim)) | (A > max(AngleLim)));
        ImageWCSampleBeam(:,sub1) = NaN;
    end
    
    R = DataWC.DetectedRangeInSamples(iPingWC,:);
%     figure(67788787); subplot(1,2,1); imagesc(ImageWCSampleBeam); colormap(jet(256)); colorbar; drawnow;
    
    R(R <= 1) = [];
    R0(k) = min(R);
    ImageWCSampleBeam((R0(k)+1):end,:) = NaN;
    R0Max = max(R0Max, R0(k));
    %     figure(67788787); subplot(1,2,2); imagesc(ImageWCSampleBeam); colormap(jet(256)); colorbar; drawnow;
    
    %% Correction de la TVG
    
    switch CorrectionTVG
        case 'Sv'
%             subx = 1:size(ImageWCSampleBeam,1);
%             fe = DataWC.SamplingFreq(iPingWC,1);
%             r = subx * (1500 / (2 * fe));
%             C = 1500;
            %{
            % TODO : trouver un moyen d'acc�der � TxPulseWidth
            % Sv = Amp_WC - X.log(r) - C + 20log(r) - 10.log(c.Teff/2)
            Teff = DataPing.TxPulseWidth / 1000; % * 0.375;
            TVG = (20 - DataWC.TVGFunctionApplied(iPingWC, 1)) * log10(r) - 10*log10(C * Teff/2);
            ImageWCSampleBeam = ImageWCSampleBeam + TVG(:);
            %}
            my_warndlg('It is impossible to compensate the TVG using "Sv" in function "import_SummedWaterColumn_All"', 0, 'Tag', 'import_SummedWaterColumn_AllNoSvCorrection');

        case 'Ts'
%             subx = 1:size(ImageWCSampleBeam,1);
%             fe = DataWC.SamplingFreq(iPingWC,1);
%             r = subx * (1500 / (2 * fe));
%             C = 1500;
            %{
            % TODO : trouver un moyen d'acc�der � Teff, ReceiveBeamWidth et TransmitBeamWidth
            % Sv = Amp_WC - X.log(r) - C + 20log(r) - 10.log(c.Teff/2)
            % TS = Sv + 10.log(V)
            
%           BeamWidthAlongDeg = AntenneOuvEmiRec(TransmitBeamWidth(iPingWC), ReceiveBeamWidth(iPingWC)); % TODO : utiliser ?
            psi = ReceiveBeamWidth(iPingWC) * TransmitBeamWidth(iPingWC) * (pi/180) ^ 2;
            V = (r .^ 2) * (psi * C * DataPing.TxPulseWidth/2);
            Teff = DataPing.TxPulseWidth / 1000; % * 0.375;
            TVG = (20 - DataWC.TVGFunctionApplied(iPingWC, 1)) * log10(r) - 10*log10(C * Teff/2);
            ImageWCSampleBeam = ImageWCSampleBeam + TVG(:) + 10*log10(V(:));
            %}
            my_warndlg('It is impossible to compensate the TVG using "Ts" in function "import_SummedWaterColumn_All"', 0, 'Tag', 'import_SummedWaterColumn_AllNoTsCorrection');
            
        otherwise
            if CorrectionTVG ~= DataWC.TVGFunctionApplied(iPingWC, 1)
                subx = 1:size(ImageWCSampleBeam,1);
                fe = DataWC.SamplingFreq(iPingWC,1);
                r = subx * (1500 / (2 * fe));
                TVG = (CorrectionTVG - DataWC.TVGFunctionApplied(iPingWC, 1)) .* log10(r);
                ImageWCSampleBeam = ImageWCSampleBeam + TVG(:);
            end
    end
    
    %% Calcul de la moyenne
    
    switch TypeMeanUnit
        case {2; 'Amp'}
            ImageWCSampleBeam  = reflec_dB2Amp(ImageWCSampleBeam);
        case {3 ; 'Enr'}
            ImageWCSampleBeam  = reflec_dB2Enr(ImageWCSampleBeam);
    end

    X = mean(ImageWCSampleBeam, 2, 'omitnan');
    
    switch TypeMeanUnit
        case {2; 'Amp'}
            X  = reflec_Amp2dB(X);
        case {3 ; 'Enr'}
            X  = reflec_Enr2dB(X);
    end
    
    %% Crop de l'image
    
    n = size(ImageWCPingSample,2);
    ImageWCPingSample(k,1:length(X)) = X';
    ImageWCPingSample(:,(n+1):end) = NaN;
end
my_close(hw);

if N <= 1
    flag = 0;
    return
end

subValid = 1:N;
ImageWCPingSample = ImageWCPingSample(1:N,1:R0Max);
% figure; imagesc(ImageWCPingSample); colormap(jet(256)); colorbar

%% Lecture de la r�flectivit�

[Reflectivity, ~, ~, sublDepth, ~] = get_Layer(this, 'ReflectivityFromSnippets', 'CartoAuto', 1, ...
    'DataDepth', DataDepth, 'DataRaw', DataRaw, 'DataSeabed', DataSeabed, ...
    'DataInstalParam', DataInstalParam, 'DataRuntime', DataRuntime, 'DataSSP', DataSSP, 'DataAttitude', DataAttitude, 'subl', subDepth);

TagSynchroX = Reflectivity.TagSynchroX;
TagSynchroX = strrep(TagSynchroX, 'PingBeam', 'PingSamples');
ImageName = Reflectivity.Name;
ImageName = strrep(ImageName, 'FromSnippets', 'WCAveragedOnBeams');
Comments = 'This image is the mean value of the WC data in the beams direction. This image must be used just for quick control of the data.';

%% Cr�ation de l'instance de l'image de sortie par h�ritage de l'instance de r�flectivit�

% [~, ia, ib] = intersect(subDepth, sublDepth);
% subWC    = subWC(ia); % Modif JMA � l'essai pour fichier "G:\SPFE\DREDGING-PLUMES\FOLLOWING-INTERBALLASTIII\0000_20160914_221918_SimonStevin.all"
% subValid = subValid(ib); % Modif JMA � l'essai pour fichier "G:\SPFE\DREDGING-PLUMES\FOLLOWING-INTERBALLASTIII\0000_20160914_221918_SimonStevin.all"
% subDepth = subDepth(ia);

subWC = subWC(sublDepth);
subValid = sublDepth;
subDepth = sublDepth;

% F = DataWC.SamplingFreq(subWC(subValid), 1);
F = DataWC.SamplingFreq(subWC, 1);
% R0 = R0(subValid);
R0 = R0(subValid);
FMode = mode(F);

ImageWCPingSampleNew = ImageWCPingSample;
subSup = find(F > FMode);
for k=1:length(subSup)
    coef = F(subSup(k)) / FMode;
    v1 = ImageWCPingSample(subSup(k),:);
    v2 = resample(double(v1), 1000, round(1000*double(coef)));
    ImageWCPingSampleNew(subSup(k),:) = NaN;
    ImageWCPingSampleNew(subSup(k),1:length(v2)) = v2;
    R0(subSup(k)) = round(R0(subSup(k))) / coef;
end
F(subSup) = FMode;
ImageWCPingSample = ImageWCPingSampleNew;
nbPixNonNaN = sum(~isnan(ImageWCPingSample));
n = find(nbPixNonNaN == 0, 1, 'first');
ImageWCPingSample(:,n:end) = [];
ImageWCPingSample(ImageWCPingSample == 0) = NaN;

ImageWCPingSampleNew = ImageWCPingSample;
subInf = find(F < FMode);
for k=1:length(subInf)
    coef = F(subInf(k)) / FMode;
    v1 = ImageWCPingSample(subInf(k),:);
    v2 = resample(double(v1), 1000, round(1000*double(coef)));
    ImageWCPingSampleNew(subInf(k),:) = NaN;
    subNonNaN = find(~isnan(v2));
    if ~isempty(subNonNaN)
        ImageWCPingSampleNew(:,(size(ImageWCPingSampleNew, 2)+1):subNonNaN(end)) = NaN;
        ImageWCPingSampleNew(subInf(k),subNonNaN) = v2(subNonNaN);
    end
    R0(subInf(k)) = round(R0(subInf(k))) / coef;
end
F(subInf) = FMode;
ImageWCPingSample = ImageWCPingSampleNew;
nbPixNonNaN = sum(~isnan(ImageWCPingSample));
% n = find(nbPixNonNaN == 0, 1, 'first');
n = find(nbPixNonNaN ~= 0, 1, 'last') + 1; % Modif JMA le 19/11/2020 pour donn�es Ridha FLUME
ImageWCPingSample(:,n:end) = [];
% ImageWCPingSample(ImageWCPingSample == 0) = NaN;

x = 1:size(ImageWCPingSample,2);
% aKM = inherit(Reflectivity, ImageWCPingSample(subValid,:), 'x', x, 'y', 1:length(subValid), ...
aKM = inherit(Reflectivity, ImageWCPingSample(subValid,:), 'x', x, 'y', subValid, ...
    'GeometryType', cl_image.indGeometryType('PingSamples'), 'ColormapIndex', 3, 'YUnit', '# Sample', 'TagSynchroX', TagSynchroX, ...
    'Name', ImageName, 'Comments', Comments);

%% Changement de la fr�quence d'�chantillonnage

aKM = set(aKM, 'SampleFrequency', F);
aKM = set(aKM, 'SonarHeight',     R0);
