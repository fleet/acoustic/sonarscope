% Visualisation du contenu des datagrams "Seabed Image" contenus dans un fichier .all 
%
% Syntax
%   [b,S] = import_WaterColumn_All(aKM, DataDepth, DataWC, DataRaw, DataPosition, DataAttitude, SonarDescription, ...);
%
% Input Arguments 
%   aKM : Instance de cl_simrad_all
%   DataDepth, DataWC, DataRaw, DataPosition, DataAttitude, SonarDescription : structure qu'il est pr�f�rable de lire en amont pour �viter de les relires � chaque lecture d'un ping
%
% Name-Value Pair Arguments
%   subl : Numeros de lignes � traiter
%
% Name-only Arguments
%   FaisceauxPairs         : Pour creer une image uniquement avec les faisceaux pairs
%   FaisceauxImairs        : Pour creer une image uniquement avec les faisceaux impairs
%   NoSpecularCompensation : Pour ne pas faire de correction du sp�culaire
%
% Output Arguments
%   b          : Instance de cli_image
%   subl_Image : Numeros de lignes communs avec la bathy (index relatis � seabedImage)
%   subl_Depth : Numeros de lignes communs avec la bathy (index relatis � Depth)
%
% Examples
%   nomFic = getNomFicDatabase('EM1002_0001_20040916_054013_raw.all')
%   aKM = cl_simrad_all('nomFic', nomFic);
%   b = import_WaterColumn_All(aKM);
%   SonarScope(b);
%
%   b = import_WaterColumn_All(aKM, 'subl', 1:100);
%   SonarScope(b);
%
% See also cl_simrad_all cl_simrad_all/plot_position cl_simrad_all/plot_nav Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [b, Carto, TransmitSectorNumber, R, WCSignalWidthInSeconds] = import_WaterColumn_All(this, ...
    DataDepth, DataWC, DataRaw, DataPosition, DataAttitude, DataRuntime, DataInstalParam, SonarDescription, varargin)

persistent persistent_Carto

% [varargin, DisplayLevel]    = getPropertyValue(varargin, 'DisplayLevel',   1);
% [varargin, DepthMin]        = getPropertyValue(varargin, 'DepthMin',       -1);
% [varargin, DepthMax]        = getPropertyValue(varargin, 'DepthMax',       -12000);
% [varargin, Draught]         = getPropertyValue(varargin, 'Draught',         0);
% [varargin, RMax1_Precedent] = getPropertyValue(varargin, 'RMax1_Precedent', []);
[varargin, Carto]                = getPropertyValue(varargin, 'Carto',                []);
[varargin, Mute]                 = getPropertyValue(varargin, 'Mute',                 0);
[varargin, subPing]              = getPropertyValue(varargin, 'subPing',              []);
[varargin, DataALL_WaterColumn]  = getPropertyValue(varargin, 'XML_ALL_WaterColumn',  []);
[varargin, CorrectionTVG]        = getPropertyValue(varargin, 'CorrectionTVG',        30);
[varargin, maskOverlapingBeams]  = getPropertyValue(varargin, 'maskOverlapingBeams',  0);
[varargin, memeReponses]         = getPropertyValue(varargin, 'memeReponses',         false); %#ok<ASGLU>

b = [];
[nomDir, nomFicSeul] = fileparts(this.nomFic);

%% Interpolation de la donnee "Attitude"

T = DataAttitude.Datetime;
TDepth = DataDepth.Datetime(subPing);

Roll  = my_interp1(T, DataAttitude.Roll(:),  TDepth, 'linear', 'extrap');
Pitch = my_interp1(T, DataAttitude.Pitch(:), TDepth, 'linear', 'extrap');
Heave = my_interp1(T, DataAttitude.Heave(:), TDepth, 'linear', 'extrap');
    
if isfield(DataAttitude, 'Tide') && ~isempty(DataAttitude.Tide)
    TideValue = my_interp1(T, DataAttitude.Tide(:), TDepth, 'linear', 'extrap');
else
    TideValue = NaN;
end

%% Lecture de InstallationParameters

if isempty(DataInstalParam)
    [flag, DataInstalParam] = read_installationParameters_bin(this);
    if ~flag
        %     return % Comment� le 15/09/2014 pour donn�es hydromap�es du NIWA
    end
end

%% 

TransmitSectorNumber = [];
R = [];

%% Check the number of elements

flag = checkIfOneElement(length(this));
if ~flag
    return
end

%%

% if ~Mute
%     Ttemp = DataDepth.Datetime;
%     figure(5555); plot(Ttemp, DataDepth.PingCounter(:,1)-DataDepth.PingCounter(1,1), '-*'); grid on
%     hold on; plot(DataWC.Datetime, DataWC.PingCounter(:,1)-DataDepth.PingCounter(1,1), '-or');
%     xlabel('Time'); ylabel('Ping Counter')
%     legend({'Depth datagram'; 'WaterColumn datagram'})
% end

P1 = DataDepth.PingCounter(:,1);
P2 = DataWC.PingCounter(:,1);
P3 = DataRaw.PingCounter(:,1);

% P2(DataWC.FlagPings(:) == 1) = []; % TODO : voir si
% NbSamples = sum(DataWC.NumberOfSamples(:,:), 2);
% P2(NbSamples == 0) = [];

[subDepth, subWC, subRaw] = intersect3(P1(subPing), P2, P3);
subDepth = subPing(subDepth); % Rajout� le 8/4/2010
if isempty(subWC)
    str1 = sprintf('Le ping %d de %s n''existe pas en Water-Column', subPing, nomFicSeul);
    str2 = sprintf('Ping %d of %s does not exist in Water-Column',   subPing, nomFicSeul);
    my_warndlg(Lang(str1,str2), 0, 'Tag', 'PasDePingWC');
    return
end

isDual = DataInstalParam.isDual;
[flag, BeamDataOut] = get_PingWaterColumn(this, DataWC, DataDepth, DataRaw, DataAttitude, DataInstalParam, isDual, ...
    'subWC', subWC, 'subDepth', subDepth, 'subRaw', subRaw, 'XML_ALL_WaterColumn', DataALL_WaterColumn, 'maskOverlapingBeams', maskOverlapingBeams);
if ~flag
    return
end

% TxPulseWidth  = get(SonarDescription, 'Signal.Duration');
TxPulseWidth = my_interp1_Extrap_PreviousThenNext(DataRuntime.Datetime, DataRuntime.TransmitPulseLength, DataWC.Datetime); % My_interp1 plut�t que interp1 pour la gestion de points uniques
subNaN       = isnan(TxPulseWidth);
subNonNaN    = find(~subNaN);
subNaN       = find(subNaN);
if isempty(subNonNaN)
    return
end
TxPulseWidth(subNaN) = my_interp1(subNonNaN, TxPulseWidth(subNonNaN), subNaN, 'nearest', 'extrap');

TransmitBeamWidth = my_interp1_Extrap_PreviousThenNext(DataRuntime.Datetime, DataRuntime.TransmitBeamWidth, DataWC.Datetime); % My_interp1 plut�t que interp1 pour la gestion de points uniques
subNaN    = isnan(TransmitBeamWidth);
subNonNaN = find(~subNaN);
subNaN    = find(subNaN);
if isempty(subNonNaN)
    return
end
TransmitBeamWidth(subNaN) = my_interp1(subNonNaN, TransmitBeamWidth(subNonNaN), subNaN, 'nearest', 'extrap');

ReceiveBeamWidth = my_interp1_Extrap_PreviousThenNext(DataRuntime.Datetime, DataRuntime.ReceiveBeamWidth, DataWC.Datetime); % My_interp1 plut�t que interp1 pour la gestion de points uniques
subNaN       = isnan(ReceiveBeamWidth);
subNonNaN    = find(~subNaN);
subNaN       = find(subNaN);
if isempty(subNonNaN)
    return
end
ReceiveBeamWidth(subNaN) = my_interp1(subNonNaN, ReceiveBeamWidth(subNonNaN), subNaN, 'nearest', 'extrap');

if isempty(DataWC) || isempty(BeamDataOut)
    return
end

if isempty(DataPosition)
    Longitude = NaN(DataDepth.Dimensions.nbPings, 1);
    Latitude  = NaN(DataDepth.Dimensions.nbPings, 1);
else
    Longitude = my_interp1_longitude(DataPosition.Datetime, DataPosition.Longitude, DataDepth.Datetime, 'linear', 'extrap');
    Latitude  = my_interp1(          DataPosition.Datetime, DataPosition.Latitude,  DataDepth.Datetime, 'linear', 'extrap');
end

N = length(subWC);
str1 = 'Chargement des Water Columns';
str2 = 'Loading WC images';
hw = create_waitbar(Lang(str1,str2), 'N', N);
% TransmitSectorNumber = zeros(N, DataRaw.Dimensions.nbRx, 'single');
TransmitSectorNumber = zeros(N, DataWC.Dimensions.nbRx, 'single');
b = repmat(cl_image, 1, N);
for k=1:N
    my_waitbar(k, N, hw)
    iPingWC  = subWC(k);
%   iPingRaw = subRaw(k);
    
    [~, TagSynchro] = fileparts(this.nomFic);
    TagSynchroX = sprintf('%s_%d_%d', TagSynchro, subDepth(k), DataWC.Dimensions.nbRx);
    TagSynchroY = sprintf('%s_%d', TagSynchro, subDepth(k));
    NomImage = TagSynchroY;

%   TransmitSectorNumber(k,:) = DataRaw.TransmitSectorNumber(subRaw(k), :);
    TransmitSectorNumber(k,:) = DataWC.TransmitSectorNumber(iPingWC, :);

    DataPing = Extraction(DataWC, iPingWC);
%   DataPing.TxAngle = DataWC.RxArray.BeamPointingAngle(iPingWC,:);
    RxAnglesEarth_WC = DataWC.BeamPointingAngle(iPingWC,:);
%     if isdeployed
%         DataPing.TxAngle = RxAnglesEarth_WC;
%     end
    DataPing.RxAnglesEarth_WC = RxAnglesEarth_WC;
    
    RxAnglesEarth_Bathy = BeamDataOut(k).RxAnglesEarthBathy;
    if all(isnan(RxAnglesEarth_Bathy))
%         if N == 1 % Comment� le 27/02/2017
%             b = [];
%             return
%         end
    end
    DataPing.TransmitSectorNumber = BeamDataOut(k).TransmitSectorNumber;
    DataPing.TransducerDepth      = BeamDataOut(k).TransducerDepth;
    DataPing.TxBeamIndexSwath     = BeamDataOut(k).TxBeamIndexSwath;
    DataPing.SectorTransmitDelay  = BeamDataOut(k).SectorTransmitDelay;
    
    if length(RxAnglesEarth_WC) == length(RxAnglesEarth_Bathy)
        DataPing.AcrossDist = BeamDataOut(k).AcrossDist;
        DataPing.AlongDist  = BeamDataOut(k).AlongDist;
        DataPing.Depth      = BeamDataOut(k).Depth;
        DataPing.BeamMask   = BeamDataOut(k).BeamMask;
%         if isdeployed
%             DataPing.BeamPointingAngle = BeamDataOut(k).BeamPointingAngle;
%         end
        if any(~isnan(BeamDataOut(k).RxAnglesEarthBathy))
            DataPing.RxAnglesEarth_WC = BeamDataOut(k).RxAnglesEarthBathy;
        else
            DataPing.RxAnglesEarth_WC = BeamDataOut(k).RxAnglesHullBathy;
        end
%         DataPing.RxAnglesEarth_WC = BeamDataOut(k).RxAnglesEarth_WC;
% TODO : tester le quel
    else
%         DataPing.AcrossDist = my_interp1(RxAnglesEarth_Bathy, BeamDataOut(k).AcrossDist, RxAnglesEarth_WC);
%         DataPing.AlongDist  = my_interp1(RxAnglesEarth_Bathy, BeamDataOut(k).AlongDist,  RxAnglesEarth_WC);
%         DataPing.Depth      = my_interp1(RxAnglesEarth_Bathy, BeamDataOut(k).Depth,      RxAnglesEarth_WC);

% Modif JMA le 02/03/2016 bug fichier EM2040 de l'AUV (r�pertoire ssc Ifremer/Lucie)
%         DataPing.AcrossDist = my_interp1(RxAnglesEarth_Bathy(1,:), BeamDataOut(k).AcrossDist, RxAnglesEarth_WC);
%         DataPing.AlongDist  = my_interp1(RxAnglesEarth_Bathy(1,:), BeamDataOut(k).AlongDist,  RxAnglesEarth_WC);
%         DataPing.Depth      = my_interp1(RxAnglesEarth_Bathy(1,:), BeamDataOut(k).Depth,      RxAnglesEarth_WC);
%         DataPing.AcrossDist = my_interp1(RxAnglesEarth_Bathy(1,:), BeamDataOut(k).AcrossDist, RxAnglesEarth_WC - DataInstalParam.S1R, 'nearest');
%         DataPing.AlongDist  = my_interp1(RxAnglesEarth_Bathy(1,:), BeamDataOut(k).AlongDist,  RxAnglesEarth_WC - DataInstalParam.S1R, 'nearest');
%         DataPing.Depth      = my_interp1(RxAnglesEarth_Bathy(1,:), BeamDataOut(k).Depth,      RxAnglesEarth_WC - DataInstalParam.S1R, 'nearest');
        
        %% Interpolation pour passer des sondes d�livr�es dans les datagrammes "Depth" aux faisceaux WC : cas du mode "High Density"

        DataPing.AcrossDist = my_interp1(RxAnglesEarth_Bathy(1,:), BeamDataOut(k).AcrossDist, RxAnglesEarth_WC, 'nearest');
        DataPing.AlongDist  = my_interp1(RxAnglesEarth_Bathy(1,:), BeamDataOut(k).AlongDist,  RxAnglesEarth_WC, 'nearest');
        DataPing.Depth      = my_interp1(RxAnglesEarth_Bathy(1,:), BeamDataOut(k).Depth,      RxAnglesEarth_WC, 'nearest');
        DataPing.BeamMask   = my_interp1(RxAnglesEarth_Bathy(1,:), BeamDataOut(k).BeamMask,   RxAnglesEarth_WC, 'nearest', 'extrap');
        
        % Trouver pourquoi les flag WC sont NaN pour le premier ping de chaque fichier
        % R�ponse : c'est ici, RxAnglesEarth_Bathy est enti�rement NaN pour certains fichiers
        
%         if isdeployed
%             DataPing.BeamPointingAngle = RxAnglesEarth_WC;
%         end
        DataPing.RxAnglesEarth_WC = RxAnglesEarth_WC;
        %{
        figure; plot(RxAnglesEarth_Bathy, '-+'); grid on; title('RxAnglesEarth_WC')
        figure; plot(RxAnglesEarth_WC, '-+'); grid on; title('RxAnglesEarth_WC')
        figure; plot(DataPing.AcrossDist, DataPing.Depth); grid on
        pppp = my_interp1(RxAnglesEarth_WC , BeamDataOut(k).DetectedRangeInSamples, RxAnglesEarth_Bathy);
        FigUtils.createSScFigure; PlotUtils.createSScPlot(pppp); grid on; set(gca, 'YDir', 'Reverse')
        FigUtils.createSScFigure; PlotUtils.createSScPlot(BeamDataOut(k).AcrossDist, pppp); grid on; set(gca, 'YDir', 'Reverse')
        FigUtils.createSScFigure; PlotUtils.createSScPlot(BeamDataOut(k).DetectedRangeInSamples); grid on; set(gca, 'YDir', 'Reverse')
        %}
        DataPing.HighDensity.AcrossDist         = BeamDataOut(k).AcrossDist;
        DataPing.HighDensity.AlongDist          = BeamDataOut(k).AlongDist;
        DataPing.HighDensity.Depth              = BeamDataOut(k).Depth;
        DataPing.HighDensity.RxAnglesHullBathy  = BeamDataOut(k).RxAnglesHullBathy;
        DataPing.HighDensity.RxAnglesEarthBathy = BeamDataOut(k).RxAnglesEarthBathy;
    end
    DataPing.TxPulseWidth = TxPulseWidth(iPingWC);
    DataPing.Roll         = Roll(k);
    DataPing.Pitch        = Pitch(k);
    DataPing.Heave        = Heave(k);
    
    DataPing.Latitude  = Latitude(subDepth(k));
    DataPing.Longitude = Longitude(subDepth(k));
    DataPing.Heading   = DataDepth.Heading(subDepth(k),1);
    
    fe = DataPing.SampleRate;
 
    %{
    % Comment� le 30/07/2013 : Le Suro�t : TODO : d�finir une r�gle
    switch DataPing.EmModel
        case 3020
            RangeMax = 1000; % (m)
        case 710
            RangeMax = 3000; % (m)
        case 302
            RangeMax = 5000; % (m)
        case 122
            RangeMax = 13000; % (m)
        case 2040
            RangeMax = 1000; % (m)
        case 2045 % EM2040C
            RangeMax = 1000; % (m) % TODO : � d�finir
        otherwise
            if ~isdeployed
                str1 = 'Message pour sonarscope@ifremer.fr : RangeMax � r�gler dans import_WaterColumn_All';
                str2 = 'Message for sonarscope@ifremer.fr : RangeMax must be fixed in import_WaterColumn_All';
                my_warndlg(Lang(str1,str2), 0, 'Tag', 'RangeMaxWaterColumn');
            end
            RangeMax = 13000; % (m)
    end
    %}
    RangeMax = Inf; % Modifi� le 30/07/2013 pour l'EM2040 du Suro�t

    RangeMax = min(floor(2* fe * RangeMax / 1500), size(BeamDataOut(k).Samples,1));    
    
    subx = 1:RangeMax;
    Amplitude = BeamDataOut(k).Samples(subx,:);
%     Phase     = BeamDataOut(k).Samples(1:RangeMax,:);

%{
Mail de Na�g le 02/10/2019
En premi�re intention comme on dit, on peut approcher le Sv (index de r�verb�ration volumique) EM2040 par:
        Sv = Amp_WC - X.log(r) - C + 20log(r) - 10.log(c.Teff/2)
Avec:
- r la distance � l'antenne en m�tres
- X et C : param�tres de TVG WC du datagramme KM
- c: vitesse du son en m/s
- Teff: dur�e de pulse effective en secondes apr�s traitement de r�ception

Je rejoins une copie des slides d'�talonnage de l'EM2040 pour m�moire.
On a par ailleurs Sv = TS - 10.log(V)
V volume insonifi� par la pulse ~psi.r^2 x c.Teff/2
psi ~ouv_3dB_tx_along_1way_rad x ouv_3dB_rx_athwart_1way_rad

%}
    switch CorrectionTVG
        case {20; 30; 40}
            if CorrectionTVG ~= BeamDataOut(k).TVGFunctionApplied
                r = subx * (1500 / (2 * fe));
                TVG = (CorrectionTVG - BeamDataOut(k).TVGFunctionApplied) * log10(r);
                Amplitude = Amplitude + TVG(:);
            end
            
        case 'Ts'
            % Sv = Amp_WC - X.log(r) - C + 20log(r) - 10.log(c.Teff/2)
            % TS = Sv + 10.log(V)
            r = subx * (1500 / (2 * fe));
            C = 1500;
            
%           BeamWidthAlongDeg = AntenneOuvEmiRec(TransmitBeamWidth(iPingWC), ReceiveBeamWidth(iPingWC)); % TODO : utiliser ?
            psi = ReceiveBeamWidth(iPingWC) * TransmitBeamWidth(iPingWC) * (pi/180) ^ 2;
            V = (r .^ 2) * (psi * C * DataPing.TxPulseWidth/2);
            Teff = DataPing.TxPulseWidth / 1000; % * 0.375;
            TVG = (20 - BeamDataOut(k).TVGFunctionApplied) * log10(r) - 10*log10(C * Teff/2);
            Amplitude = Amplitude + TVG(:) + 10*log10(V(:));
            
        case 'Sv'
            % Sv = Amp_WC - X.log(r) - C + 20log(r) - 10.log(c.Teff/2)
            Teff = DataPing.TxPulseWidth / 1000; % * 0.375;
            r = subx * (1500 / (2 * fe));
            C = 1500;
            TVG = (20 - BeamDataOut(k).TVGFunctionApplied) * log10(r) - 10*log10(C * Teff/2);
            Amplitude = Amplitude + TVG(:);
    end

    R = BeamDataOut(k).DetectedRangeInSamples + 1;
    
    if isfield(BeamDataOut, 'WCSignalWidth') && ~isempty(BeamDataOut(k).WCSignalWidth)
        WCSignalWidthInSeconds = BeamDataOut(k).WCSignalWidth;
    else
        WCSignalWidthInSeconds = [];
    end
    
%     Angles = DataPing.BeamPointingAngle; %  - Roll(k);

%     Angles = DataPing.TxAngle; %  - DataPing.Roll;
%     Angles = RxAnglesEarth_WC;
    Angles = DataPing.RxAnglesEarth_WC;
    
    if isfield(DataWC, 'OrdreBeam')
        [~, iBeamSort] = sort(DataWC.OrdreBeam(k, :));
        Angles(iBeamSort) = Angles;
        TransmitSectorNumber(iBeamSort) = TransmitSectorNumber(k,:);
%         RxAnglesEarth_WC(iBeamSort) = RxAnglesEarth_WC;
        DataPing.RxAnglesEarth_WC = Angles;
    end

    C = DataPing.SoundVelocity;
    
    if length(unique(DataPing.SamplingRateMinMax)) == 1
        D = (R/2) * (C / fe);
    else
        feBab = DataPing.SamplingRateMinMax(1);
        feTri = DataPing.SamplingRateMinMax(2);
%         subTri = (BeamDataOut(k).TransmitSectorNumber(k,:) == 2); % Avant modif
        subTri = (BeamDataOut(k).TransmitSectorNumber == 2); % Modif JMA le 04/05/2021
        D(~subTri) = (R(~subTri)/2) * (C / feBab);
        D( subTri) = (R( subTri)/2) * (C / feTri);
    end
    
%     switch DataWC.SystemSerialNumberMin
%         case {593; 594}
%             feBab = fe;
%             feTri = fe * (14620/13960); % 0010_20090225_001241_ShipName
%             feTri = fe * (14620/14290); % 0010_20090226_003818_Belgica
%             subTri = (BeamDataOut.TransmitSectorNumber(k,:) == 2);
%             D(~subTri) = (R(~subTri)/2) * C / feBab; %#ok<AGROW>
%             D( subTri) = (R( subTri)/2) * C / feTri; %#ok<AGROW>
%         otherwise
%             D = (R/2) * C / fe;
%     end
    
%     figure(7000+k);
    if ~Mute
%         ModeDetection = DataDepth.DetectionInfo(subDepth(k),:);
%         subAmp = find(ModeDetection == 0);
%         subPha = find(ModeDetection == 1);
        Fig = figure(9000+iPingWC);
        set(Fig, 'Name', NomImage)
        hc(1) = subplot(4,1,1);
        hp = PlotUtils.createSScPlot(hc(1), Angles, '-+'); grid on; title(sprintf('Angles for iPingWC=%d', iPingWC))
        set(hp, 'Tag', 'BottomDetector', 'UserData', NomImage);

% On ne peut pas utiliser subAmp et subPhas � cause du High density : TODO
%         hc(2) = subplot(4,1,2);  PlotUtils.createSScPlot(subAmp, R(subAmp), '-+r'); grid on; title(sprintf('Kongsberg Detection for iPingWC=%d', iPingWC)); set(gca,'YDir', 'Reverse')
%         hc(2) = subplot(4,1,2);  PlotUtils.createSScPlot(subPha, R(subPha), '-+'); grid on; title(sprintf('Kongsberg Detection for iPingWC=%d', iPingWC)); set(gca,'YDir', 'Reverse')
        hc(2) = subplot(4,1,2);
        hp = PlotUtils.createSScPlot(hc(2), R, '-+'); grid on; title(sprintf('Kongsberg Detection for iPingWC=%d', iPingWC)); set(gca,'YDir', 'Reverse')
        set(hp, 'Tag', 'BottomDetector', 'UserData', NomImage);
        setappdata(hp, 'TagSynchroYScale', 1/fe)
        
        hc(3) = subplot(4,1,3);
        hp = PlotUtils.createSScPlot(hc(3), -D .* cosd(Angles), '-+'); grid on; title(sprintf('Pseudo Z for iPingWC=%d', iPingWC))
        linkaxes(hc, 'x'); axis tight; drawnow;
         set(hp, 'Tag', 'BottomDetector', 'UserData', NomImage);
       
         % TODO : ajouter xAntenne au plot de subplot(4,1,4) idem dans import_MagAndPhase_All
%          xAntenne = this.Sonar.Ship.Arrays.Transmit.X; % Modif JMA le 29/11/2016

        h4 = subplot(4,1,4);
        hp = PlotUtils.createSScPlot(h4, D .* sind(Angles), -D .* cosd(Angles), '-+'); grid on; title(sprintf('Pseudo Z for iPingWC=%d', iPingWC))
        set(hp, 'Tag', 'BottomDetector', 'UserData', NomImage);
    end
    
%     PulseDuration    = get(SonarDescription, 'Signal.Duration'); % sBandWth
%     SampleRate       = DataPing.SampleRate; % Hz
%     ReceiveBeamWidth = get(SonarDescription, 'BeamForm.Rx.TransWth'); % deg
    
    %% DataWC reduction in the Time domain
    
%     [AmplitudeReduced, step, yAmplitudeReduced, R0_FirstEstimation] = ...
%         SampleBeam_ReduceMatrixAmplitude(Amplitude); %#ok<NASGU>
    
    %% Preprocessing of Mask on Amplitude
    
%     DepthMaxPing = abs(DepthMax)-Draught;
%     DepthMinPing = abs(DepthMin)-Draught;
%     
%     [R0, RMax1, RMax2, R1SamplesFiltre, iBeamMax0, AmpPingNormHorzMax, MaskWidth, iBeamBeg, iBeamEnd] = ...
%         SampleBeam_PreprocAmpMask_7111_V6(...
%         AmplitudeReduced, step, DataPing.TxAngle, ReceiveBeamWidth, ...
%         SampleRate, PulseDuration, DepthMinPing, DepthMaxPing, DisplayLevel, iPingWC);
        
    %{
    figure(56557); imagesc(reflec_Enr2dB(Amplitude)); colorbar;
    hold on;
    plot(repmat(R0 ,1,length(RMax1)), '*c');
    plot(RMax2, 'ok')
    plot(RMax1, '*k')
    hold off
%}
%     DataPing.R0                 = R0;
%     DataPing.RMax1              = RMax1;
%     DataPing.RMax2              = RMax2;
    DataPing.R1SamplesFiltre    = R; %R1SamplesFiltre; % Pas de d�tection sur fichier Maskoor 1_0023_20100212_223316_EX ping 119
%     DataPing.iBeamMax0          = iBeamMax0;
%     DataPing.AmpPingNormHorzMax = AmpPingNormHorzMax;
%     DataPing.MaskWidth          = MaskWidth;
%     DataPing.iBeamBeg           = iBeamBeg;
%     DataPing.iBeamEnd           = iBeamEnd;
     
    % Cr�ation de l'instance de classe
    
%     IdentSonar = find_IdentSonar(cl_sounder, num2str(DataPing.SystemSerialNumber), ...
%         'Frequency', DataPing.Frequency/1000);
%     SonarDescription = cl_sounder('Sonar.Ident', IdentSonar, 'Sonar.Mode_1', 1, ...
%     'Sonar.SystemSerialNumber', DataPing.SystemSerialNumber);

    [~, signalsWCSampleBeam] = create_signalsWCSampleBeam(this, DataWC, DataAttitude, size(Amplitude,1), subWC(k));
% signalsWCSampleBeam.plot
% signalsWCSampleBeam.plotAuto
% signalsWCSampleBeam.editProperties

    b(1,k) = cl_image;
    if isa(Amplitude, 'cl_memmapfile')
        b(1,k) = set_Image(b(1,k), Amplitude);
    else
        if N > 1
            b(1,k) = set_Image(b(1,k), cl_memmapfile('Value', Amplitude, 'LogSilence', 1));
        else
            b(1,k) = set_Image(b(1,k), Amplitude, 'NoMemmapfile');
        end
    end
    
    b(1,k).x                 = 1:size(Amplitude,2);
    b(1,k).y                 = 1:size(Amplitude,1);
    b(1,k).YDir              = 2;
    b(1,k).Name              =  NomImage;
    b(1,k).Unit              = 'dB';
    b(1,k).TagSynchroX       = TagSynchroX;
    b(1,k).TagSynchroY       = TagSynchroY;
    b(1,k).TagSynchroYScale  = 1/fe;
    b(1,k).XUnit             = 'Beam #';
    b(1,k).YUnit             = 'Sample #';
    b(1,k).InitialFileName   = this.nomFic;
    b(1,k).InitialFileFormat = 'SimradAll';
    b(1,k).Comments          = sprintf('Reson PingCounter : %d', DataPing.PingCounter);
    b(1,k).GeometryType      = cl_image.indGeometryType('SampleBeam');
    
    b(1,k) = set_SonarDescription(b(1,k), SonarDescription);

    b(1,k) = set_SampleBeamData(b(1,k), DataPing);
    
    %% TODO TODO TODO TODO 24/11/2016 FLOWS : corriger cette rustine (voir sonar_SimradProcessWC.m
    if length(TideValue) == 1
        TideValue = repmat(TideValue, size(Amplitude,1), 1);
    end
    b(1,k) = set_SonarTide(b(1,k), TideValue);

    b(1,k).DataType = cl_image.indDataType('Reflectivity');
    b(1,k).ColormapIndex = 3;
    
    % Param�tres d'installation
    
    if isempty(DataInstalParam) || isempty(DataInstalParam.Datetime)
        InstalParamsRx = [];
    else
        b(1,k) = set_SonarInstallationParameters(b(1,k), DataInstalParam);
        InstalParamsRx = get_SonarInstalParamsRx(b(1,k));
    end
    
    b(1,k) = update_Name(b(1,k));
    b(1,k).Writable = false;

    b(1,k) = set_SignalsVert(b(1,k), signalsWCSampleBeam(:));

    %% D�finition d'une carto

    while isempty(Carto)
        [b(1,k), Carto, persistent_Carto] = define_carto(b(1,k), Carto, nomDir, memeReponses, persistent_Carto);
        b(1,k) = set_Carto(b(1,k), Carto);
    end
    b(1,k) = set_Carto(b(1,k), Carto);
end
b = b(:);
my_close(hw)

%% Edition de l'objet cli_image

if nargout == 0
    SonarScope(b);
end


function DataPing = Extraction(DataWC, k)

% TODO : (k,:) pour sondeur dual SVP !!!!

DataPing.PingCounter           = DataWC.PingCounter(k,1);
DataPing.SystemSerialNumber    = DataWC.SystemSerialNumber(k,1);
% DataPing.SampleRate          = DataWC.SamplingRate(k); % OLD
DataPing.SampleRate            = DataWC.SamplingFreq(k,1);
DataPing.EmModel               = DataWC.EmModel;
if isfield(DataWC, 'Position') % A conserver tant qu'on peut lire les donn�es ancienne lecture
    DataPing.Position          = DataWC.Position(k);
end
% DataPing.SoundVelocity       = DataWC.SoundVelocity(k); % OLD
DataPing.SoundVelocity         = DataWC.SoundSpeed(k,1);
% DataPing.SamplingRateMinMax  = DataWC.SamplingRateMinMax; % OLD
DataPing.SamplingRateMinMax    = DataWC.SamplingFreq(k,:);
DataPing.TiltAngle             = DataWC.TiltAngle(k,:); % TODO : Rend dim(2)=2 &lors que la donn�e r�eelle est de dim 1 !!! Tester sur sondeur dual en r�ception
T                              = DataWC.Time;
DataPing.Time                  = T(k);
DataPing.Datetime              = DataWC.Datetime(k);
