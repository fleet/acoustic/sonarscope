% Fusion des datagrammes WCD et ALL dans un seul fichier ALL.
%
% Syntax
%   Data = merge_ALL(aKM, ...)
%
% Input Arguments
%   aKM : Instance de cl_simrad_all
%
% Name-only Arguments
%
% Output Arguments
%   nomFicOut : liste des fichiers de sortie.
%
% Examples
%   nomFic      = getNomFicDatabase('EM1002_0001_20040916_054013_raw.all')
%   aKM           = cl_simrad_all('nomFic', nomFic);
%   nomFicOut   = merge_ALL(aKM);
%
% See also cl_simrad_all Authors
% Authors : GLU
% ----------------------------------------------------------------------------

function merge_ALL(this, subDatagram)

% Identification de la pr�sence des fichiers WCD par test sur le 1er
% fichier.
[nomDir, nomFic, ~] = fileparts(this(1).nomFic);
nomFicWcd = fullfile(nomDir, [nomFic '.wcd']);
if ~exist(nomFicWcd, 'file')
    nomFicWcd = fullfile(nomDir, [nomFic '.WCD']);
    if ~exist(nomFicWcd, 'file')
        nomFicWcd = [];
    end
end
if ~isempty(nomFicWcd)
    processWcd = true;
else
    processWcd = false;    
end


% Rassemblement des fichiers unitaire de type ALL.
nomFicOut = merge_ALL_unitaire(this, false, subDatagram);
if isempty(nomFicOut)
    return
end

%% Lecture des nouveaux fichiers ALL (et WCD en automatique)

aKM = cl_simrad_all('nomFic', nomFicOut); %#ok<NASGU>

% Rassemblement des fichiers unitaire de type Wcd.
if processWcd
    % On ajoute d'abord le Datagramme WCD m�me si le nombre d'occurence
    % n'est pas connu.
    subDatagram(end+1,:) = {0; 107; 'WaterColumn'; true};
    nomFicWCOut = strrep(nomFicOut, '.all', '.wcd');
    merge_ALL_unitaire(this, true, subDatagram, 'nomFicOut', nomFicWCOut);
end

%% Lecture des nouveaux fichiers ALL (et WCD en automatique)

aKM = cl_simrad_all('nomFic', nomFicOut); %#ok<NASGU>

% plot_position(aKM)

%% Renommage du fichier .all et .wcd

%{
for k=1:numel(this)
    [nomDir, nomFic, ~] = fileparts(this(k).nomFic);
    nomFicNew = fullfile(nomDir, [nomFic '.allMerged']);
    [status, message] = movefile(this(k).nomFic, nomFicNew, 'f');
    if status
        strFR = sprintf('Le fichier "%s" a �t� renomm� en "%s"', this(k).nomFic, nomFicNew);
        strUS = sprintf('File "%s" was renamed as "%s"', this(k).nomFic, nomFicNew);
        my_warndlg(Lang(strFR,strUS), 0, 'Tag', 'ImpossibleDeRenommerCeFichier');
    else
        strFR = sprintf('Echec de renommage de "%s" en "%s", faites le � la main : \nMessage matlab : %s', this(k).nomFic, nomFicNew, message);
        strUS = sprintf('It was impossible to rename "%s" in "%s", do it manualy : \nMatlab message : %s', this(k).nomFic, nomFicNew, message);
        my_warndlg(Lang(strFR,strUS), 0, 'Tag', 'ImpossibleDeRenommerCeFichier');
    end
end
%}

function nomFicOut = merge_ALL_unitaire(this, flagProcessWcd, subDatagram, varargin)

[varargin, nomFicOut] = getPropertyValue(varargin, 'nomFicOut', []); %#ok<ASGLU>

%% Nommage du fichier de sortie.

if isempty(nomFicOut)
    [nomDir, nomFic, ~] = fileparts(this(1).nomFic);
    if ~flagProcessWcd
        nomFicOut = fullfile(nomDir, [nomFic '_merged.all']);
    else
        nomFicOut = fullfile(nomDir, [nomFic '_merged.wcd']);
    end
    str1 = 'Nom du fichier r�sultant de l''agr�gation des fichiers .all';
    str2 = 'Give the name to the file that will merge the .all files';
    [flag, nomFicOut] = my_uiputfile('*.txt', Lang(str1,str2), nomFicOut);
    if ~flag
        nomFicOut = [];
        return
    end
end

fidOut  = fopen(nomFicOut, 'w+');
% Simple filtrage des datagrammes.
[nomDir2, nomFic2] = fileparts(nomFicOut);
nomsSsc = fullfile(nomDir2, 'SonarScope', nomFic2);
if exist(nomsSsc, 'dir')
    try
        rmdir(nomsSsc, 's')
        if exist(nomsSsc, 'dir')
            strFR = sprintf('"%s" n''a pas pu �tre supprim�.', nomsSsc);
            strUS = sprintf('"%s" could not be suppressed.', nomsSsc);
            my_warndlg(Lang(strFR,strUS), 0);
            flag = 0;
        else
            flag = 1;
        end
        
    catch %#ok<CTCH>
        strFR = sprintf('Le r�pertoire"%s" n''a pas pu �tre supprim�, on arr�te tout.', nomsSsc);
        strUS = sprintf('Folder "%s" could not be suppressed. Nothing will be done before, please solve the problem before.', nomsSsc);
        my_warndlg(Lang(strFR,strUS), 0);
        flag = 0;
    end
end
if ~flag
    return
end

Position       = [];
% PingCounter  = [];
Taille         = [];
TypeOfDatagram = [];
t              = [];
indexIn        = [];

% Test et pr�traitement du fichier aLL
for k=1:numel(this)
    dummy = this(k);
    % Traitement des Wcd car les .All ont d�j� �t� trait�s.
    if flagProcessWcd
        [nomDir, nomFic, ~] = fileparts(dummy.nomFic);
        nomFicWcd = fullfile(nomDir, [nomFic '.wcd']);
        if ~exist(nomFicWcd, 'file')
            nomFicWcd = fullfile(nomDir, [nomFic '.WCD']);
            if ~exist(nomFicWcd, 'file')
                nomFicWcd = [];
            end
        end
        if ~isempty(nomFicWcd)
            dummy = cl_simrad_all('nomFic', nomFicWcd);
        end
    end
    
    %% Lecture du fichier d'index du fichier ALL
    
    % {
    [flag, Data] = read_FileIndex_bin(dummy);
    if ~flag
        return
    end
    
    tThis = Data.Time;
    if isa(tThis, 'cl_time')
        tThis = tThis.timeMat;
    end
    
    Position        = [Position; Data.DatagramPosition]; %#ok<AGROW>
%     PingCounter     = [PingCounter;    Data.PingCounter];
    Taille          = [Taille; double(Data.DatagramLength)]; %#ok<AGROW>
    TypeOfDatagram  = [TypeOfDatagram; Data.TypeOfDatagram]; %#ok<AGROW>
    t               = [t; tThis(:)]; %#ok<AGROW>
    % Cr�ation du vecteur Index des fichiers d'entr�e.
    indexIn         = [indexIn; ones(size(tThis,1),1)*k]; %#ok<AGROW>
    % }
    
    %% Decodage du fichier de donn�es
    
    % Ouverture des fichiers ALL d'origine
    [fidIn(k), message] = fopen(dummy.nomFic, 'r'); %#ok<AGROW>
    if fidIn < 0
        my_warndlg(message, 1);
        return
    end
end

% Tri des indices de fichiers selon les arriv�es de Ping.
[~, idx] = sort(t);
clear t
% t            = t(idxFic);
Position       = Position(idx);
% PingCounter  = PingCounter(idx);
Taille         = Taille(idx);
TypeOfDatagram = TypeOfDatagram(idx);
indexIn        = indexIn(idx);

% V�rification pour que des pings d'un sondeur dual ne se retrouvent pas s�par�s dans 2 fichiers s�par�s

sub          = [subDatagram{:,4}];
selectedCode = [subDatagram{sub,2}];
if isempty(selectedCode)
    strFR = sprintf('Aucun datagramme de s�lectionner : pas d''agr�gation de fichiers ALL.');
    strUS = sprintf('None datagram selected : no merging files creation.');
    my_warndlg(Lang(strFR,strUS), 0);
    return;
end

% Filtre du SSP : on ne conserve que la 1�re occurrence.
subDataToFilter  = find(TypeOfDatagram == 85);
Position(subDataToFilter(2:end))       = [];
Taille(subDataToFilter(2:end))         = [];
TypeOfDatagram(subDataToFilter(2:end)) = [];
indexIn(subDataToFilter(2:end))        = [];

% % % 
% Filtre du Install Param Start : on ne conserve que la 1�re occurrence.
subDataToFilter = find(TypeOfDatagram == 73);
Position(subDataToFilter(2:end))       = [];
Taille(subDataToFilter(2:end))         = [];
TypeOfDatagram(subDataToFilter(2:end)) = [];
indexIn(subDataToFilter(2:end))        = [];

% Filtre du Install Param Stop : on ne conserve que la 1�re occurrence.
% D�placement pr�alable du Datagramme Install Parameters en fin de paquet
% de datagramme.
subDataToFilter = find(TypeOfDatagram == 105);
Position(end+1)        = Position(subDataToFilter(1));
Taille(end+1)          = Taille(subDataToFilter(1));
TypeOfDatagram(end+1)  = TypeOfDatagram(subDataToFilter(1));
indexIn(end+1)         = indexIn(subDataToFilter(1));

Position(subDataToFilter(1:end))       = [];
Taille(subDataToFilter(1:end))         = [];
TypeOfDatagram(subDataToFilter(1:end)) = [];
indexIn(subDataToFilter(1:end))        = [];

%% Traitement du fichier

% PU Status	:               49
% Extra Parameters	:       51
% Attitude	:               65
% Clock	:                   67
% DepthV1	:               68
% Raw Range Beam Angles	:	70
% Surface Sound Speed	:	71
% Heading	:               72
% Mecha Transducer Tilt	:	74
% Raw Range Beam Angles (new version)	:	78
% Position	:               80
% Runtime	:               82
% Seabed Image Data	:       83
% SoundSpeedProfile	:       85
% XYZ88	:                   88
% Seabed Image Data 89	:	89
% Raw Range Beam Angles (since 2004)	:	102
% Height	:               104
% Water Column	:           107
% Stave Data	:           109
% Network Attitude	:       110
% Quality Factor	:       112
% Raw Beam Samples	:       114
% InstallParams	:           73 (Start) ou 105 (Stop)ou 112
% TODO: identifier les num�ros et choisir l'action
% 102 78 113 72 69 84 71 87 112 74 48 49 66

str = sprintf('Loading file %s', nomFic);
N = length(Position);
hw = create_waitbar(str, 'N', N);
for k=1:N
    my_waitbar(k, N, hw);
    % On ne sauvegarde que les datagrammes s�lectionn�s.
    pppp = find(selectedCode == TypeOfDatagram(k),1);
    if ~isempty(pppp)
        switch TypeOfDatagram(k)
            
            % Depth, Raw range and beam angle, SeabedImage, Central beams
            % echogram, quality factor,
            case {49 51 68 70 75 78 79 83 88 89 102 104 107 109 113 114 }
                fseek(fidIn(indexIn(k)), Position(k), 'bof');
                X = fread(fidIn(indexIn(k)), Taille(k), 'uchar');
                fwrite(fidOut, X, 'uchar');
                % Attitude, Clock, Position : TODO : il faudrait peut-�tre recopier un de chaque avant et apr�s  : passage par des subAttitude{k} qui serait obtenu par un subAtt(1)-1:subAtt(end)+1
            
            case {65 67 80 110}
                fseek(fidIn(indexIn(k)), Position(k), 'bof');
                X = fread(fidIn(indexIn(k)), Taille(k), 'uchar');
                fwrite(fidOut, X, 'uchar');
                
                % Runtime...
            case {66 69 71 72 73 74 48 82 84 85 87 105 112}
                fseek(fidIn(indexIn(k)), Position(k), 'bof');
                X = fread(fidIn(indexIn(k)), Taille(k), 'uchar');
                fwrite(fidOut, X, 'uchar');
                
                %
            otherwise
                strFR = sprintf('TypeOfDatagram "%d" non identifi� dans "merge_ALL", signalez ce message � sonarscope@ifremer.fr', TypeOfDatagram(k));
                strUS =  sprintf('TypeOfDatagram "%d" not identified in "merge_ALL", please report to sonarscope@ifremer.fr', TypeOfDatagram(k));
                my_warndlg(Lang(strFR,strUS), 0, 'Tag', 'DatagrammeNonIdentifie');
                fseek(fidIn(indexIn(k)), Position(k), 'bof');
                X = fread(fidIn(indexIn(k)), Taille(k), 'uchar');
                fwrite(fidOut, X, 'uchar');
        end
    end
end
my_close(hw)

% Fermeture des fichiers d'entr�e.
for k=1:numel(this)
    fclose(fidIn(k));    
end

% Fermeture du fichier de sortie;
fclose(fidOut);
