function flag = modifyModeDeepFMInModeDeeperForEM304(this, varargin)

[varargin, DataRuntime] = getPropertyValue(varargin, 'DataRuntime', []);
[varargin, DataRaw]     = getPropertyValue(varargin, 'DataRaw',     []); %#ok<ASGLU>

EmModel = get(this, 'EmModel'); %DataRaw.EmModel;
if EmModel ~= 304
    flag = 1;
    return
end

if isempty(DataRuntime)
    [flag, DataRuntime] = SSc_ReadDatagrams(this.nomFic, 'Ssc_Runtime');
    if ~flag
        return
    end
end

if isempty(DataRaw)
    [flag, DataRaw] = read_rawRangeBeamAngle(a, 'Memmapfile', 1);
    if ~flag
        return
    end
end

%% Récupération du signal "PresencesFM"

ModeRuntime = DataRuntime.Mode(:);
[flag, SignalPresenceFM] = create_signal_presenceFM(this, 'DataRaw', DataRaw);
if ~flag
    return
end
flagPresenceFM = my_interp1_Extrap_PreviousThenNext(SignalPresenceFM.Datetime, SignalPresenceFM.Data, DataRuntime.Datetime);

%% Modification des modes

for k=1:length(ModeRuntime)
    switch ModeRuntime(k)
        case 2 % Shallow
            ModeRuntime(k) = 2;
        case 3 % Medium
            ModeRuntime(k) = 3;
        case 4 % Deep
            if mode(flagPresenceFM) == 1
                ModeRuntime(k) = 5;
            else
                ModeRuntime(k) = 4;
            end
        case 5 % Very deep FM and Very deep CW
            ModeRuntime(k) = 6;
        case 6 % Extra deep
            ModeRuntime(k) = 7;
        case 1 % Extreme deep
            ModeRuntime(k) = 8;
        otherwise
            str1 = sprintf('Sondeur "%s" ne connait pas le mode %d (dans cl_simrad_all/modifyModeDeepFMInModeDeeperForEM304)', Model, ModeRuntime(k));
            str2 = sprintf('Sounder "%s" does not know mode=%d (in cl_simrad_all/modifyModeDeepFMInModeDeeperForEM304)', Model, ModeRuntime(k));
            my_warndlg(Lang(str1,str2), 1);
    end
end

%% Recriture du signal Mode dans le cache SSc

flag = write_runtime_bin(this, 'Mode', ModeRuntime);
