function [flag, IndAttUnit] = params_ALL_IndexAttitudeSystem(this, varargin)

[varargin, Filename] = getPropertyValue(varargin, 'Filename', []); %#ok<ASGLU>

flag       = 1;
IndAttUnit = [];

if get_LevelQuestion < 3
    return
end

if ~isempty(Filename)
    this = cl_simrad_all('nomFic', Filename);
end

[flag, ~, nbSystems, indSystemUsed] = read_attitude_bin(this);
if ~flag
    return
end
if isempty(nbSystems) || (nbSystems == 1)
    return
end

if nbSystems == 2
    str1 = sprintf('Il y a %d syst�mes de navigation ici. L''index du syst�me courant est le %d. Voulez-vous en choisir l''autre ?', ...
        nbSystems, indSystemUsed);
    str2 = sprintf('There are %d navigation systems here. The index of the default one is %d. Do you want to select the other ?', ...
        nbSystems, indSystemUsed);
    [ind, flag] = my_questdlg(Lang(str1,str2), 'Init', 2);
    if ~flag || (ind == 2)
        return
    end
    if indSystemUsed == 1
        IndAttUnit = 2;
    else
        IndAttUnit = 1;
    end
else
    str1 = sprintf('Il y a %d syst�mes de navigation ici. L''index du syst�me courant est le %d. Voulez-vous en choisir un autre ?', ...
        nbSystems, indSystemUsed);
    str2 = sprintf('There are %d navigation systems here. The index of the default one is %d. Do you want to select another one ?', ...
        nbSystems, indSystemUsed);
    [ind, flag] = my_questdlg(Lang(str1,str2), 'Init', 2);
    if ~flag || (ind == 2)
        return
    end
    list = 1:3;
    str = {'Navigation system 1'; 'Navigation system 2'; 'Navigation system 3'};
    list(indSystemUsed) = [];
    str(indSystemUsed) = [];
    [ind, flag] = my_listdlg('Index of the system ?', str, 'SelectionMode', 'Single');
    if ~flag
        return
    end
    IndAttUnit = list(ind);
end
    