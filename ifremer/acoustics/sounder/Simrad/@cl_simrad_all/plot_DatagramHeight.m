%{
nomFic = 'D:\Temp\Herve\EM2040-Thalia_05032013\0029_20130305_074519_ShipName.all';
aKM = cl_simrad_all('nomFic', nomFic);
[fig1, fig2] = plot_DatagramHeight(aKM)
%}

function [fig1, fig2] = plot_DatagramHeight(this, varargin)

[varargin, fig1]  = getPropertyValue(varargin, 'fig1',  []);
[varargin, fig2]  = getPropertyValue(varargin, 'fig2',  []);
[varargin, Color] = getPropertyValue(varargin, 'Color', []); %#ok<ASGLU>

[flag, DataHeight] = SSc_ReadDatagrams(this.nomFic, 'Ssc_Height');
if ~flag
    return
end

[flag, DataAttitude] = read_attitude_bin(this);
if ~flag
    return
end

if ~isequal(fig1, 'No')
    if isempty(fig1)
        fig1 = FigUtils.createSScFigure;
    else
        FigUtils.createSScFigure(fig1);
    end
    h1(1) = subplot(3,2,1); PlotUtils.createSScPlot(DataHeight.PingCounter,                         ['*' Color]); grid on; title('PingCounter')
    h1(2) = subplot(3,2,3); PlotUtils.createSScPlot(DataHeight.Height,                              ['-' Color]); grid on; title('Height')
    h1(3) = subplot(3,2,5); PlotUtils.createSScPlot(DataHeight.HeightType,                          ['*' Color]); grid on; title('HeightType')
    h2(1) = subplot(3,2,2); PlotUtils.createSScPlot(DataHeight.PingCounter, DataHeight.PingCounter, ['*' Color]); grid on; hold on; title('PingCounter/PingCounter')
    h2(2) = subplot(3,2,4); PlotUtils.createSScPlot(DataHeight.PingCounter, DataHeight.Height,      ['*' Color]); grid on; hold on; title('Height/PingCounter')
    h2(3) = subplot(3,2,6); PlotUtils.createSScPlot(DataHeight.PingCounter, DataHeight.HeightType,  ['*' Color]); grid on; hold on; title('HeightType/PingCounter')
    linkaxes(h1, 'x'); axis tight; drawnow;
    linkaxes(h2, 'x'); axis tight; drawnow;
end

if isempty(fig2)
    fig2 = FigUtils.createSScFigure;
else
    FigUtils.createSScFigure(fig2);
end

T = datetime(DataHeight.Time.timeMat, 'ConvertFrom', 'datenum');
h3(1) = subplot(4,1,1); PlotUtils.createSScPlot(T, DataHeight.PingCounter, ['*' Color]); grid on; hold on; title('PingCounter / Time')
h3(2) = subplot(4,1,2); PlotUtils.createSScPlot(T, DataHeight.Height,      ['-' Color]); grid on; hold on; title('Height / Time')
h3(3) = subplot(4,1,3); PlotUtils.createSScPlot(T, DataHeight.HeightType,  ['*' Color]); grid on; hold on; title('HeightType / Time')

if isfield(DataAttitude, 'Tide')
    T = datetime(DataAttitude.Time.timeMat, 'ConvertFrom', 'datenum');
    h3(4) = subplot(4,1,4); PlotUtils.createSScPlot(T, DataAttitude.Tide,        ['-' Color]); grid on; hold on; title('Tide / Time')
end
axis(h3(1), 'tight');
axis(h3(2), 'tight');
axis(h3(3), 'tight');
% axis(h3(4), 'tight');
linkaxes(h3, 'x'); axis tight; drawnow;
