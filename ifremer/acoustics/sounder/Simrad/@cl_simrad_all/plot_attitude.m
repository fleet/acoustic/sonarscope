% Affichage des informations contenues dans les datagrams "attitude" d'un fichier .all
%
% Syntax
%   plot_attitude(aKM)
%
% Input Arguments
%   aKM : Instance de cl_simrad_all
%
% Examples
%   nomFic = getNomFicDatabase('EM1002_0001_20040916_054013_raw.all')
%   aKM = cl_simrad_all('nomFic', nomFic);
%   plot_attitude(aKM)
%   plot_attitude(aKM, 'sub', 1:1000)
%
% See also cl_simrad_all Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function Fig = plot_attitude(this, varargin)

[varargin, sub] = getPropertyValue(varargin, 'sub', []);
[varargin, Fig] = getPropertyValue(varargin, 'Fig', []); %#ok<ASGLU>

if isempty(Fig)
    Fig = FigUtils.createSScFigure('Name', 'Attitude datagrams'); % 'Position', centrageFig(1600, 800)
else
    FigUtils.createSScFigure(Fig);
end


NbFic = length(this);
hw = create_waitbar('Plot .all preprocessing Depth', 'N', NbFic);
for k=1:NbFic
    my_waitbar(k, NbFic, hw);
    plot_attitude_unitaire(this(k), sub, Fig);
end
my_close(hw, 'MsgEnd');
    
    
function plot_attitude_unitaire(this, sub, Fig)

%% Lecture des datagrams d'attitude

[flag, DataAttitude] = read_attitude_bin(this);
if ~flag
    return
end

%% Affichage de la navigation

[~, nomFic] = fileparts(this.nomFic);

FigName = sprintf('Attitude datagrams : Sondeur EM%d Serial Number  %d, Ship : %s', ...
    DataAttitude.EmModel, DataAttitude.SystemSerialNumber, ShipName(cl_sounder([]), DataAttitude.SystemSerialNumber));

FigUtils.createSScFigure(Fig);
set(Fig, 'name', FigName)

hc = findobj(gca, 'Type', 'line');
nbCourbes = length(hc);
ColorOrder = get(gca, 'ColorOrder');
nbCoul = size(ColorOrder,1);
iCoul = mod(nbCourbes-1, nbCoul) + 1;

% 'display('0003_20160308_095943_Belgica - 08/03/2016  09:59:43.356  08/03/2016  10:07:48.978  ')'
% 'display('0003_20160308_095943_Belgica - 2016-03-08 09:59:43.357  2016-03-08 10:07:48.978')'

T = DataAttitude.Datetime;
str = sprintf('display(''%s - %s  %s'')', nomFic, char(T(1)), char(T(end)));

fields = {'Roll'; 'Pitch'; 'Heave'; 'Heading'};
units = repmat({''}, 1, length(fields));
units{1} = 'deg';
units{2} = 'deg';
units{3} = 'm';
units{4} = 'deg';
strFields = cell2str(fields);

if isempty(sub)
    sub = 1:length(T);
end

for k=1:length(fields)
    Y = DataAttitude.(fields{k});
    
    haxe(k) = subplot(4, 1, k); %#ok
    h = PlotUtils.createSScPlot(haxe(k), T(sub), Y(sub)); hold on;
    set(h, 'Color', ColorOrder(iCoul,:))
    set(h, 'ButtonDownFcn', str)
    grid on; title(fields{k})
    
    msg = sprintf('%s : %s %s', strFields(k,:), num2strCode(Y(:,:)));
    if ~isempty(units{k})
        msg = sprintf('%s (%s)', msg, units{k});
    end
    msg = sprintf('%s %s', msg, interpretation(DataAttitude.EmModel, fields{k}, unique(Y)));
    fprintf('%s\n', msg);
end
linkaxes(haxe, 'x'); axis tight; drawnow;



function str  = interpretation(EmModel, Layer, Y)

str = '';

strModeOther  = {'Very shallow'; 'Shallow'; 'Medium'; 'Deep'; 'Very deep'};
strModeEM3000 = {'Nearfield'; 'Normal'; 'Targetdetect'};

switch Layer
    case 'SourceSoundSpeed'
        str = sprintf('%s \t | 0=From real time sensor, 1=Manually entered by operator, 3=Interpolated from currently used sound speed profile', str);
        
    case 'BeamSpacing'
        str = sprintf('%s \t | 0=Determined by beamwidth, 1=Equidistant, 2=Equiangle, 3=InBetween', str);
        
    case 'YawStabMode'
        str = sprintf('%s \t | 0=No, 1=Yes to survey line, 2=Yes to mean vessel heading, 3=Yes to manually entered headind', str);
        
    case 'PitchStabMode'
        str = sprintf('%s \t | 0=No, 8=Yes', str);
        
    case {'OperatorStationStatus'; 'ProcessingUniStatus'; 'BSPStatus'; 'SonarHeadStatus'}
        if any(Y)
            str = sprintf('%s \t | ~=0 --> Something wrong', str);
        end
        
    case 'Mode'
        switch EmModel
            case 3000
                strIci = strModeEM3000;
            otherwise
                strIci = strModeOther;
        end
        for k=1:length(Y)
            str = sprintf('%s \t : %d --> %s', str, Y(k), strIci{Y(k)+1});
        end
        
    case 'FilterIdentifier'
        X = bitand(Y, 3);
        switch X
            case 1
                str = sprintf('%s : Spike filter is set to Weak', str);
            case 2
                str = sprintf('%s : Spike filter is set to Medium', str);
            case 3
                str = sprintf('%s : Spike filter is set to Strong', str);
        end
        
        X = bitand(Y, 4);
        if X == 4
            str = sprintf('%s : Slope filter is on', str);
        end
        
        X = bitand(Y, 8);
        if (X == 8) && ~((EmModel == 1002) || (EmModel == 3002))
            str = sprintf('%s : Sector tracking or Robust BottomDetection is on', str);
            my_warndlg('Sector tracking or Robust BottomDetection is on : it is impossible to calibrate the imagery.', 0, 'Tag', 'Sector tracking');
        end
        
        Y = bitshift(Y, -4);
        
        X = bitand(Y, 6);
        switch X
            case 1
                str = sprintf('%s : Range gates are Large', str);
            case 8
                str = sprintf('%s : Range gates are Small', str);
        end
        
        X = bitand(Y, 4);
        if X == 4
            str = sprintf('%s : Interference filter is on', str);
        end
end
