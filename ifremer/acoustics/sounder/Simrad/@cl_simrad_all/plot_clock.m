% Affichage des informations contenues dans les datagrams "clock" d'un fichier .all
%
% Syntax
%   plot_clock(aKM)
%
% Input Arguments
%   aKM : Instance de cl_simrad_all
%
% Examples
%   nomFic = getNomFicDatabase('EM1002_0001_20040916_054013_raw.all')
%   aKM = cl_simrad_all('nomFic', nomFic);
%   plot_clock(aKM)
%
% See also cl_simrad_all Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function plot_clock(this, varargin)

[varargin, sub] = getPropertyValue(varargin, 'sub', []);
[varargin, Fig] = getPropertyValue(varargin, 'Fig', []); %#ok<ASGLU>

if isempty(Fig)
    Fig = FigUtils.createSScFigure('Name', 'Clock datagrams');
else
    FigUtils.createSScFigure(Fig);
end


NbFic = length(this);
hw = create_waitbar('Plot .all Plot Clock', 'N', NbFic);
for i=1:NbFic
    my_waitbar(i, NbFic, hw);
    plot_clock_unitaire(this(i), sub, Fig)
end
my_close(hw, 'MsgEnd');


function plot_clock_unitaire(this, sub, Fig)

%% Lecture des datagrams d'clock

[flag, DataClock] = SSc_ReadDatagrams(this.nomFic, 'Ssc_Clock');
if ~flag
    return
end

if ~isfield(DataClock, 'EmModel') && isfield(DataClock, 'Model') % Il est urgent de déterminer les codes valables pour ts les sondeurs
    DataClock.EmModel = DataClock.Model;
end

if isempty(DataClock) || ~isfield(DataClock, 'EmModel')
    return
end

%% Affichage de la navigation

[~, nomFic] = fileparts(this.nomFic);

FigName = sprintf('Clock datagrams : Sondeur EM%d Serial Number  %d, Ship : %s', ...
    DataClock.EmModel, DataClock.SystemSerialNumber, ShipName(cl_sounder([]), DataClock.SystemSerialNumber));

t = DataClock.Time.timeMat;
t = datetime(t, 'ConvertFrom', 'datenum');
if isempty(t)
    return
end

FigUtils.createSScFigure(Fig);
set(Fig, 'name', FigName)

hc = findobj(gca, 'Type', 'line');
nbCourbes = length(hc);
ColorOrder = get(gca, 'ColorOrder');
nbCoul = size(ColorOrder,1);
iCoul = mod(nbCourbes-1, nbCoul) + 1;

% str = timeMat2str([t(1), t(end)]);
% str = [str repmat(' ', 2, 2)]';
% str = sprintf('display(''%s - %s %s'')', nomFic, str(:));
str = sprintf('display(''%s - %s %s'')', nomFic, char(t(1)), char(t(end)));

T = DataClock.Time;
if isempty(sub)
    sub = 1:length(T);
end

D = T(sub) - DataClock.ExternalTime(sub);
h = PlotUtils.createSScPlot(T(sub), D); hold on;
set(h, 'Color', ColorOrder(iCoul,:))
set(h, 'ButtonDownFcn', str)
grid on; title('Time - ExternalTime (ms)')

if isfield(DataClock, 'Synchronisation') % Ancien Ssc format
    if DataClock.Synchronisation
        msg = sprintf('%s : System clock is synchronized to an external 1 PPS', nomFic);
    else
        msg = sprintf('%s : System clock is NOT synchronized to an external 1 PPS', nomFic);
    end
else
    if DataClock.PPS(1) % Nouveau Ssc format
        msg = sprintf('%s : System clock is synchronized to an external 1 PPS', nomFic);
    else
        msg = sprintf('%s : System clock is NOT synchronized to an external 1 PPS', nomFic);
    end
end
disp(msg)
