% Affichage des informations contenues dans les datagrams "height" d'un fichier .all
%
% Syntax
%   plot_height(aKM)
%
% Input Arguments
%   aKM : Instance de cl_simrad_all
%
% Examples
%   nomFic = getNomFicDatabase('EM1002_0001_20040916_054013_raw.all')
%   aKM = cl_simrad_all('nomFic', nomFic);
%   plot_height(aKM)
%
% See also cl_simrad_all Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function plot_height(this, varargin)

[varargin, Fig] = getPropertyValue(varargin, 'Fig', []);
if isempty(Fig)
    Fig = FigUtils.createSScFigure('Name', 'Height datagrams');
    flagCreation = 1;
else
    FigUtils.createSScFigure(Fig);
    flagCreation = 0;
end


NbFic = length(this);
hw = create_waitbar('Plot .all Runtime Parameters', 'N', NbFic);
for i=1:NbFic
    my_waitbar(i, NbFic, hw);
    status(i) = plot_height_unitaire(this(i), Fig, varargin{:}); %#ok<AGROW>
end
my_close(hw);
if all(~status) && flagCreation
    my_close(Fig)
end

function status = plot_height_unitaire(this, Fig, varargin)

[varargin, sub] = getPropertyValue(varargin, 'sub', []); %#ok<ASGLU>

status = 0;

%% Lecture des datagrams d'height

[flag, DataHeight] = SSc_ReadDatagrams(this.nomFic, 'Ssc_Height');
if ~flag
    return
end

if isempty(DataHeight)
    my_warndlg(Lang('Pas de datagrams "Height" dans ce fichier', 'No "Height" datagrams in this file'), 0, 'Tag', 'Pas de datagrams "Height"');
    return
end

%% Affichage de la navigation

[~, nomFic] = fileparts(this.nomFic);

% TODO TODO TODO
try
    DataModel = DataHeight.Model;
catch
    DataModel = DataHeight.EmModel;
end
FigName = sprintf('Height datagrams : Sondeur %d Serial Number  %d', DataModel, DataHeight.SystemSerialNumber);

FigUtils.createSScFigure(Fig);
set(Fig, 'name', FigName)

hc = findobj(gca, 'Type', 'line');
nbCourbes = length(hc);
ColorOrder = get(gca, 'ColorOrder');
nbCoul = size(ColorOrder,1);
iCoul = mod(nbCourbes-1, nbCoul) + 1;

t = DataHeight.Time.timeMat;
str = timeMat2str([t(1), t(end)]);
str = [str repmat(' ', 2, 2)]';
str = sprintf('display(''%s - %s'')', nomFic, str(:));

T = DataHeight.Time;
if isempty(sub)
    sub = 1:length(T);
end

h = PlotUtils.createSScPlot(T(sub), DataHeight.Height); hold on;
set(h, 'Color', ColorOrder(iCoul,:))
set(h, 'ButtonDownFcn', str)
grid on; title('Height (m)')

status = 1;
