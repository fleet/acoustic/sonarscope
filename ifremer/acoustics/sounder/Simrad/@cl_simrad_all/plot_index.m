% Affichage des informations relatives aux datagrams contenus dans un fichier .all
%
% Syntax
%   plot_index(aKM)
% 
% Input Arguments 
%   aKM : Instance de cl_simrad_all
%
% Examples
%   nomFic = getNomFicDatabase('EM1002_0001_20040916_054013_raw.all')
%   aKM = cl_simrad_all('nomFic', nomFic);
%   plot_index(aKM)
%
% See also cl_simrad_all histo_index Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function plot_index(this)

NbFic = length(this);
hw = create_waitbar('Plot .all Plot Index', 'N', NbFic);
for i=1:NbFic
    my_waitbar(i, NbFic, hw);
    plot_index_unitaire(this(i))
end
my_close(hw, 'MsgEnd');


function plot_index_unitaire(this)

%% Lecture du fichier d'index

[flag, Data] = read_FileIndex_bin(this);
if ~flag
    return
end

T            = Data.Time;             
Position     = Data.DatagramPosition;
Taille       = Data.DatagramLength;
TypeDatagram = Data.TypeOfDatagram;
PingCounter  = Data.PingCounter;
Date  = T.date;
Heure = T.heure;                

[~, nomFic] = fileparts(this.nomFic);
FigUtils.createSScFigure('Name', nomFic);

haxe(1) = subplot(6,1,1);
PlotUtils.createSScPlot(Date); grid on; title('Date');
haxe(2) = subplot(6,1,2);
PlotUtils.createSScPlot(Heure); grid on; title('Heure');
haxe(3) = subplot(6,1,3);
PlotUtils.createSScPlot(Taille, '+'); grid on; title('Taille');
haxe(4) = subplot(6,1,4);
PlotUtils.createSScPlot(Position); grid on; title('Position');
haxe(5) = subplot(6,1,5);
PlotUtils.createSScPlot(TypeDatagram, '+'); grid on; title('TypeDatagram');
haxe(6) = subplot(6,1,6);
PlotUtils.createSScPlot(PingCounter, '+'); grid on; title('PingCounter');

linkaxes(haxe, 'x'); axis tight; drawnow;

% nomSignal{1} = 'Date';
% nomSignal{2} = 'Heure';
% nomSignal{3} = 'Taille';
% nomSignal{4} = 'Position';
% nomSignal{5} = 'TypeDatagram';
% nomSignal{6} = 'PingCounter';
% 
% signal{1} = Date;
% signal{2} = Heure;
% signal{3} = Taille;
% signal{4} = Position;
% signal{5} = TypeDatagram;
% signal{6} = PingCounter;
% 
% ex2 = cli_visu_caraibes;
% set(ex2, 'lstVec', signal);
% set(ex2, 'lstNomsVec', nomSignal);
% editobj(ex2) ;
% 
