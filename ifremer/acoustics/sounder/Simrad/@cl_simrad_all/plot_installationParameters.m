% Affichage des informations contenues dans les datagrams "runtime" d'un fichier .all
%
% Syntax
%   plot_installationParameters(aKM)
% 
% Input Arguments 
%   aKM : Instance de cl_simrad_all
%
% Name-Value Pair Arguments
%   fullListe : 1=affichage que des datagrams presents, 0=tous les datagrams
%
% Examples
%   nomFic = getNomFicDatabase('EM1002_0001_20040916_054013_raw.all')
%   aKM = cl_simrad_all('nomFic', nomFic);
%   plot_installationParameters(aKM)
%
% See also cl_simrad_all plot_index Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function plot_installationParameters(this)

NbFic = length(this);
hw = create_waitbar('Plot .all Runtime Parameters', 'N', NbFic);
for k=1:NbFic
    my_waitbar(k, NbFic, hw);
    plot_installationParameters_unitaire(this(k))
end
my_close(hw, 'MsgEnd');


function plot_installationParameters_unitaire(this)

%% Lecture des datagrams de navigation

[flag, DataInstallationParameters] = read_installationParameters_bin(this);
if ~flag
    return
end

str = {};
names = fieldnames(DataInstallationParameters);
for i=1:length(names)
    if ischar(DataInstallationParameters.(names{i}))
        str{end+1} = [names{i} ' : ' DataInstallationParameters.(names{i})]; %#ok<AGROW>
    elseif isa(DataInstallationParameters.(names{i}), 'cl_time')
    elseif isa(DataInstallationParameters.(names{i}), 'datetime')
        t = DataInstallationParameters.Datetime;
        str{end+1} = [names{i}   'Begin : ' char(t(1))]; %#ok<AGROW>
        str{end+1} = [names{i}   'End   : ' char(t(end))]; %#ok<AGROW>
    elseif strcmp(names{i}, 'Dimensions')
    elseif strcmp(names{i}, 'Signals')
    else
        pppp = DataInstallationParameters.(names{i});
        if isempty(pppp)
            str{end+1} = [names{i} ' : ']; %#ok<AGROW>
        else
            str{end+1} = [names{i} ' : ' num2str(pppp(1))]; %#ok<AGROW>
        end
    end
end

edit_infoPixel(cell2str(str), [], 'PromptString', 'Installation Parameters');
