% Trace de la navigation et des capteurs de vitesse et de cap d'un fichier .all
%
% Syntax
%   plot_position_data(a, Invite)
%
% Input Arguments
%   a      : Instance of cl_simrad_all
%   Invite : Height | Heading | Speed | CourseVessel | SoundSpeed
%
% Examples
%   nomDir = 'D:\NIWA_DATA\TAN0105'
%   listFileNames = listeFicOnDir2(nomDir, '.all')
%   [flag, All] = ALL.checkFiles(listFileNames);
%   plot_position_data(All, 'Heading')
%   plot_position_data(All, 'Speed')
%   plot_position_data(All, 'CourseVessel')
%   plot_position_data(All, 'SoundSpeed')
%   plot_position_data(All, 'Height')
%   plot_position_data(All, 'Mode')
%   plot_position_data(All, 'Time')
%   plot_position_data(All, 'Drift')
%
% See also cl_simrad_all plot_index Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function InfoSignal = plot_position_data(this, Invite, varargin)

[varargin, IndNavUnit]   = getPropertyValue(varargin, 'IndNavUnit',   []);
[varargin, modePlotData] = getPropertyValue(varargin, 'modePlotData', 1);
[varargin, Mute]         = getPropertyValue(varargin, 'Mute',         0);
[varargin, Fig]          = getPropertyValue(varargin, 'Fig',          []); %#ok<ASGLU>
% [varargin, stepPings]  = getPropertyValue(varargin, 'Step',         1);

InfoSignal = [];

Memmapfile = 0;

NbFic = length(this);

nbColors = 64;
Ident = rand(1);

if isempty(Fig)
    Fig = figure('name', 'Position datagrams');
else
    figure(Fig);
end
set(Fig, 'UserData', Ident)
hold on;

%% Recherche des valeurs extr�mes sur l'ensemble des fichiers
% TODO : �a serait bien d'�crire un r�sum� des statistiques d'un signal
% lorsqu'on le cr�e. On acc�derait ainsi aux valeurs min/max sans avoir besoin de
% les recalculer

XLim  = [Inf -Inf];
YLim  = [Inf -Inf];
ZLim  = [Inf -Inf];
ZUnit = [];
str1 = sprintf('Lecture du signal "%s".', Invite);
str2 = sprintf('Reading "%s" signal.', Invite);
hw = create_waitbar(Lang(str1,str2), 'N', NbFic);
for k=1:NbFic
    my_waitbar(k, NbFic, hw);
    
    [flag, Z, DataPosition, Colors, comment] = read_Invite(this(k), Invite, nbColors, IndNavUnit, Memmapfile, 'Histo');
    if ~flag
        continue
    end
    
    ZUnit = Z.Unit;
    Val = Z.Data(:);
    Val(isnan(Val)) = [];
    if isempty(Val)
        continue
    end
    
    Val = double(Val);
    ZLim(1) = min(ZLim(1), min(Val));
    ZLim(2) = max(ZLim(2), max(Val));
            
    [no{k}, xo{k}] = my_hist(Val); %#ok<AGROW>
    
    XLim(1) = min(XLim(1), min(DataPosition.Longitude, [], 'omitnan'));
    XLim(2) = max(XLim(2), max(DataPosition.Longitude, [], 'omitnan'));
    YLim(1) = min(YLim(1), min(DataPosition.Latitude,  [], 'omitnan'));
    YLim(2) = max(YLim(2), max(DataPosition.Latitude,  [], 'omitnan'));
end
my_close(hw)
ZLim = sort(ZLim);

if isinf(ZLim(1))
    str1 = sprintf('La variable "%s" n''est d�finie pour aucun des fichiers ".all".', Invite);
    str2 = sprintf('"%s" is undefined for all ".all" files.', Invite);
    my_warndlg(Lang(str1,str2), 0, 'Tag', 'PlotPositionDataZLimNonDefini', 'TimeDelay', 60);
    return
end

figure(Fig);
if isempty(ZUnit)
    strTitle = Invite;
else
    strTitle = [Invite ' (' ZUnit ')'];
end
if ~isempty(comment)
    strTitle = sprintf('%s (%s)', strTitle, comment);
end
title(strTitle)

%% Saisie des bornes de la table de couleurs

switch Invite
%     case 'Drift' % Comment� par JMA le 05/05/2020 pour donn�es Fugro-Gauss sur Mayobs13
%         ZLim = [-5 5];
    case {'Heading'; 'CourseVessel'}
        ZLim = [0 360];
    case {'WaterColumn'; 'FM-Mode'}
        ZLim = [-1 1];
    case {'Nb Of Swaths'; 'NbSwaths'}
        ZLim = [0 2];
    case 'PresenceWC'
        ZLim = [0 2];
    case 'PresenceRTK'
        ZLim = [0 2];
%     case '1/CurvatureRadius'
%         ZLim = ;
    otherwise
        if Mute
            for k=length(xo):-1:1
                if ~isempty(xo{k})
                    xDeb(k) = xo{k}(1);
                    xFin(k) = xo{k}(end);
                    step(k) = (xFin(k) - xDeb(k)) / (length(xo{k}) - 1);
                end
            end
            xDeb = min(xDeb);
            xFin = max(xFin);
            step = round(mean(step), 1, 'significant');
            bins = centrage_magnetique(xDeb:step:xFin);
            bins = [bins(1) - 2*step, bins(1) - step, bins, bins(end) + step, bins(end) + 2*step];
            nHistTotal = zeros(1,length(bins));
%             figure;
            for k=1:length(xo)
                if ~isempty(xo{k})
                    sub = 3 + floor((xo{k} - bins(3)) / step); % 3=1 + 2 en s�curit�
                    nHistTotal(sub) = nHistTotal(sub) + no{k};
%                     subplot(2,1,1); plot(xo{k}, no{k}, '*'); grid on; hold on;
%                     subplot(2,1,2); plot(bins, nHistTotal, '*'); grid on; hold on;
                end
            end
            nCum = cumsum(nHistTotal) / sum(nHistTotal);
%             figure; plot(bins, nHistTotal, '*'); grid on; 
%             figure; plot(bins, nCum, '*'); grid on; 
            
            ZLimIn = my_interp1(nCum, bins, [0.01 0.99], 'linear', 'extrap');
            for k=2:6
                ZLim = round(ZLimIn, k, 'significant');
                r = range(ZLimIn) / range(ZLim);
                if ~isinf(r) && (r > 1)
                    break
                end
            end
        else
            [flag, ZLim] = saisie_CLim(min(ZLim), max(ZLim), ZUnit, 'SignalName', Invite, 'hFigHisto', 87698);
            if ~flag
                return
            end
        end
end

%% Trac� de la figure

figure(Fig);
W = axis;
x = linspace(XLim(1), XLim(1)+100*nbColors*eps(XLim(1)), nbColors);
y = linspace(YLim(1), YLim(2), nbColors);
h = imagesc(x, y, linspace(ZLim(1), ZLim(2), nbColors)); colormap(Colors), colorbar %#ok<NASGU>
% delete(h)
hold on;
axis(W)

DataUnique = [];
str1 = sprintf('Trac� du signal "%s".', Invite);
str2 = sprintf('Plot "%s" signal.', Invite);
hw = create_waitbar(Lang(str1,str2), 'N', NbFic);
for k=1:NbFic
    my_waitbar(k, NbFic, hw);
    % TODO : , DataUnique et faire un unique des unique
    [EmModel, SystemSerialNumber, DataUniqueFile] = plot_position_data_unitaire(this(k), Invite, ZLim, Fig, ...
        modePlotData, nbColors, Colors, IndNavUnit, Memmapfile);
    DataUnique = unique([DataUnique; DataUniqueFile(:)]);
end
my_close(hw, 'MsgEnd')

if ~isfield(Z, 'Unit') || isempty(Z.Unit)
    FigName = sprintf('Sondeur %d Serial Number  %d - %s', EmModel, SystemSerialNumber, Invite);
else
    FigName = sprintf('Sondeur %d Serial Number  %d - %s (%s)', EmModel, SystemSerialNumber, Invite, Z.Unit);
end
figure(Fig);
set(Fig, 'name', FigName)

if isfield(Z, 'strData')
    InfoSignal.strData = Z.strData;
    if isfield(Z, 'strIndData')
        InfoSignal.strIndData = Z.strIndData;
    else
        InfoSignal.strIndData = 1:length(InfoSignal.strData);
    end
%     InfoSignal.Unit = Z.Unit;
else
    if length(DataUnique) < 20 % MaxNumberDataUnique : d�finit ind�pendemment � deux endroits dans ce .m
        InfoSignal.DataUnique = DataUnique;
    else
        InfoSignal.DataUnique = [];
    end
    InfoSignal.Unit = ZUnit;
end
end


function [EmModel, SystemSerialNumber, DataUnique] = plot_position_data_unitaire(this, Invite, ZLim, Fig, ...
    modePlotData, nbColors, Colors, IndNavUnit, Memmapfile)

EmModel            = [];
SystemSerialNumber = [];
DataUnique         = [];

nomFic = get(this, 'nomFic');
figure(Fig)

%% Lecture des datagrams de navigation

[flag, DataPosition] = read_position_bin(this, 'IndNavUnit', IndNavUnit, 'Memmapfile', Memmapfile);
if ~flag || isempty(DataPosition) || isempty(DataPosition.Latitude)
    return
end

[flag, Z] = read_Invite(this, Invite, nbColors, IndNavUnit, 0);
if ~flag
    return
end

DataUnique = checkDataUnique(Z.Data(:));

nbSounders = size(Z.Data,2);
if nbSounders == 2
%     str1 = 'Ce soundeur est dual, les donn�es peuvent �tre diff�rentes � b�bord et � tribord, pour l''instant je ne trace que les donn�es du sondeur tribord.';
%     str2 = 'This sounder is a dual one, for the moment I plot only data from the port one.';
%     my_warndlg(Lang(str1,str2), 0, 'Tag', 'PlotPositionDataNbSounders=2');
    nbSounders = 1; %#ok<NASGU>
end

% for iCote=1:nbSounders
for iCote=1:1 % Modif JMA le 02/05/2017
    %     figure(Fig(iCote))
    figure(Fig) % Modif JMA le 02/05/2017
    
    ZData = Z.Data(:,iCote);
    subNonNaN = find(~isnan(ZData));
    if length(subNonNaN) <= 1
        return
    end
    subNaN = find(isnan(ZData));
    if ~isa(ZData, 'double')
        ZData = single(ZData);
    end
    ZData(subNaN) = interp1(subNonNaN, ZData(subNonNaN), subNaN, 'linear', 'extrap');
    if ZLim(2) == ZLim(1)
        ZLim(1) = ZLim(1) - 0.5;
        ZLim(2) = ZLim(2) + 0.5;
    end
    
    %% Affichage de la navigation
    
    if isfield(DataPosition, 'EmModel')
        EmModel = DataPosition.EmModel;
    else
        EmModel = DataPosition.Model;
    end
    SystemSerialNumber = DataPosition.SystemSerialNumber;
    
    if modePlotData == 1
%         switch Invite % Comment� par JMA le 09/09/2020 pour fichier "G:\BATHYCOR2\ALL\South\0028_20160403_125421_raw.all"
%             case {'Mode'; 'NbSwaths'; 'EM2040Mode2'; 'PresenceRTK'; 'TVGFunctionAppliedWC'}
                Value = my_interp1_Extrap_PreviousThenNext(Z.Datetime(:,1), ZData, DataPosition.Datetime);
%             otherwise
%                 Value = my_interp1(Z.Datetime(:,1), ZData, DataPosition.Datetime); % Modif JLA le 04/03/2020 pour lecture cache Netcdf TODO : attention dim2=2 si dualHead
%         end
        coef = (nbColors-1) / (ZLim(2) - ZLim(1));
        Value = ceil((Value - ZLim(1)) * coef);
        
        % Ajout le 23/04/2014
        h = findobj(Fig, 'Tag', nomFic);
        if ~isempty(h)
            if ~any(ZData - round(ZData))
                % on a affaire � un entier
                Signal = my_interp1(Z.Datetime(:,1), ZData, DataPosition.Datetime, 'nearest');
            else
                Signal = my_interp1(Z.Datetime(:,1), ZData, DataPosition.Datetime);
            end
            setappdata(h, 'Signal', Signal);
        end
        % Fin Ajout le 23/04/2014
        
        for k=1:nbColors
            sub = find(Value == (k-1));
            if ~isempty(sub)
                h = plot(DataPosition.Longitude(sub), DataPosition.Latitude(sub), '+');
                set(h, 'Color', Colors(k,:), 'Tag', nomFic)
                set_HitTest_Off(h)
            end
        end
        
        sub = find(Value < 0);
        if ~isempty(sub)
            h = plot(DataPosition.Longitude(sub), DataPosition.Latitude(sub), '+');
            set(h, 'Color', Colors(1,:)*0.8, 'Tag', nomFic)
            set_HitTest_Off(h)
        end
        
        sub = find(Value > (nbColors-1));
        if ~isempty(sub)
            h = plot(DataPosition.Longitude(sub), DataPosition.Latitude(sub), '+');
            set(h, 'Color', Colors(nbColors,:)*0.8, 'Tag', nomFic)
        end
    else % TODO : tester scatter sur gros jeu de donn�es
        % % Ajout� le23/04/2014 en vue d'export des valeurs du signal en shape file
        % % Test trac� par scatter : permet de donner les valeurs directement
        % % sans passer par la d�composition en 64 couleurs. Permet aussi de
        % % r�cup�rer les valeurs pour affichage dans le texte du bas de la
        % % figure ainsi que transf�rer la valeur dans les exports de navigation
        Value = my_interp1(Z.Datetime(:,1), ZData, DataPosition.Datetime);
        h = scatter(DataPosition.Longitude(:), DataPosition.Latitude(:), [], Value, '.');
        set(h, 'Tag', nomFic)
        set_HitTest_Off(h)
        setappdata(h, 'Signature1', 'scatter')
        
        hC = colorbar;
        set(gca, 'CLim', ZLim);
        GCA = gca;
        set(hC, 'ButtonDownFcn', @callbackColorbar);
        setappdata(hC, 'Signature1', 'Colorbar')
    end
end

axisGeo;
grid on;
% colorbar % Supprim� le 7/11/2006 car ca remet les infos de colorbar de 0 � 64
drawnow

    function callbackColorbar(varargin)
        try
            [hImcontrast, vSSC] = my_imcontrast(GCA); %#ok<ASGLU>
        catch ME %#ok<NASGU>
%             fprintf('%s\n', getReport(ME));
%             str1 = 'Il est pr�f�rable d''utiliser la version corrig�e de imcontrast. Pour cela, voir sous https://forge.ifremer.fr/frs/?group_id=126';
%             str2 = 'It is more useful to use imcontrast version corrected for adjust data. See https://forge.ifremer.fr/frs/?group_id=126';
%             str1 = sprintf('%s\n\n%s', ME.message, str1);
%             str2 = sprintf('%s\n\n%s', ME.message, str2);
%             my_warndlg(Lang(str1,str2),1);
            hImcontrast = imcontrast(GCA); % Appel sans vSSC
        end
        waitfor(hImcontrast)
        % CLim = get(this.hAxePrincipal, 'CLim');
    end
end


function DataUnique = checkDataUnique(Data)

n = numel(Data);
if n < 100
    DataUnique = unique(Data(:));
else
    sub = floor(linspace(1,n,100));
    DataUnique = unique(Data(sub));
    if length(DataUnique) < 20 % MaxNumberDataUnique : d�finit ind�pendemment � deux endroits dans ce .m
        DataUnique = unique(Data(:));
    end
end
end