% Affichage des informations contenues dans les datagrams "runtime" d'un fichier .all
%
% Syntax
%   plot_runtime(aKM)
%
% Input Arguments
%   aKM : Instance de cl_simrad_all
%
% Name-Value Pair Arguments
%   fullListe : 1=affichage que des datagrams presents, 0=tous les datagrams
%
% Examples
%   nomFic = getNomFicDatabase('EM1002_0001_20040916_054013_raw.all')
%   aKM = cl_simrad_all('nomFic', nomFic);
%   plot_runtime(aKM)
%
% See also cl_simrad_all plot_index Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function plot_runtime(this, varargin)

Fig = [];
NbFic = length(this);
typeAbscisses = 2; % min(NbFic,2);
hw = create_waitbar('Plot .all Runtime Parameters', 'N', NbFic);
for k=1:NbFic
    my_waitbar(k, NbFic, hw);
    Fig = plot_runtime_unitaire(this(k), typeAbscisses, 'Fig', Fig, 'iCoul', k);
end
my_close(hw, 'MsgEnd');


function Fig = plot_runtime_unitaire(this, typeAbscisses, varargin)

[varargin, iCoul] = getPropertyValue(varargin, 'iCoul', 1);
[varargin, Fig]   = getPropertyValue(varargin, 'Fig',   []); %#ok<ASGLU>

if isempty(Fig)
    Fig = FigUtils.createSScFigure('Name', 'Runtime datagrams');
else
    FigUtils.createSScFigure(Fig);
end

%% Lecture des datagrams de navigation

[flag, DataRuntime] = SSc_ReadDatagrams(this.nomFic, 'Ssc_Runtime');
if ~flag
    return
end

%% Affichage de la navigation

[~, nomFic] = fileparts(this.nomFic);

if ~isfield(DataRuntime, 'EmModel') % Vaudrait mieux faire le contraire
    DataRuntime.EmModel = DataRuntime.Model;
end
FigName = sprintf('Runtime datagrams : Sondeur %d Serial Number  %d, Ship : %s', ...
    DataRuntime.EmModel, DataRuntime.SystemSerialNumber, ShipName(cl_sounder([]), DataRuntime.SystemSerialNumber));

FigUtils.createSScFigure(Fig);
set(Fig, 'name', FigName, 'Position', [360    67   560   500])

% hc = findobj(gca, 'Type', 'line');
% nbCourbes = length(hc);
ColorOrder = get(gca, 'ColorOrder');
nbCoul = size(ColorOrder,1);
iCoul = mod(iCoul-1, nbCoul) + 1;

t = DataRuntime.Datetime;
str = sprintf('display(''%s - %s %s'')', nomFic, char(t(1)), char(t(end)));

if isfield(DataRuntime, 'ProcessingUniStatus') % Correctif ancien Ssc format
    DataRuntime.ProcessingUnitStatus = DataRuntime.ProcessingUniStatus;
end

fields = {'PingCounter'; 'OperatorStationStatus'; 'ProcessingUnitStatus'; 'BSPStatus'
    'SonarHeadStatus'; 'Mode'; 'FilterIdentifier'; 'MinimumDepth'; 'MaximumDepth'
    'AbsorptionCoeff'; 'TransmitPulseLength'; 'TransmitBeamWidth'; 'TransmitPowerMax'
    'ReceiveBeamWidth'; 'ReceiveBandWidth'; 'ReceiverFixGain'; 'TVGCrossAngle'; 'SourceSoundSpeed'
    'MaximumPortSwath'; 'BeamSpacing'; 'MaximumPortCoverage'
    'YawStabMode'; 'PitchStabMode'
    'MaximumStarboardCoverage'; 'MaximumStarboardSwath'};

units = repmat({''}, 1, length(fields));
units{8}  = 'm';
units{9}  = 'm';
units{10} = 'dB/km';
units{11} = 'ms';
units{12} = 'deg';
units{13} = 'dB';
units{14} = 'deg';
units{15} = 'Hz';
units{16} = 'dB';
units{17} = 'deg';
units{19} = 'm';
units{21} = 'deg';
units{24} = 'deg';
units{25} = 'm';

strFields = cell2str(fields);

strInfo = {};
t = DataRuntime.Datetime;
for k=1:length(fields)
    if ~isfield(DataRuntime, fields{k})
        continue
    end
    Y = DataRuntime.(fields{k});
    
    hAxe(k) = subplot(7, 4, k); %#ok<AGROW>
    
    if typeAbscisses == 1
        h = PlotUtils.createSScPlot(Y(:,:));
    else
        h = PlotUtils.createSScPlot(t, Y(:,:));
    end
    hold on;
    set(h, 'Color', ColorOrder(iCoul,:))
    set(h, 'ButtonDownFcn', str)
    grid on;
    if isempty(units{k})
        title(fields{k})
    else
        title([fields{k} ' (' units{k} ')'])
    end
    
    msg = sprintf('%s : %s', strFields(k,:), num2strCode(Y(:,:)));
    if ~isempty(units{k})
        msg = sprintf('%s (%s)', msg, units{k});
    end
    msg = sprintf('%s %s', msg, interpretation_runtime(DataRuntime.EmModel, fields{k}, unique(Y)));
    %     disp(msg)
    strInfo{end+1} = msg; %#ok
end

strInfo{end+1} = '-----------------------------------------------------';
strInfo{end+1} = 'Time for the records';
T = DataRuntime.Datetime;
for k=1:length(T)
    strInfo{end+1} = [num2str(k) ' : ' char(T(k))]; %#ok<AGROW>
end
edit_infoPixel(strInfo, [], 'PromptString', ['Runtime datagrams for file ' nomFic]);
drawnow
linkaxes(hAxe(hAxe ~= 0), 'x');
