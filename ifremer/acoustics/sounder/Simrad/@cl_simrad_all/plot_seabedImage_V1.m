% Affichage des informations contenues dans les datagrams "seabedImage" d'un fichier .all
%
% Syntax
%   plot_seabedImage_V1(aKM)
%
% Input Arguments
%   aKM : Instance de cl_simrad_all
%
% Examples
%   nomFic = getNomFicDatabase('EM1002_0001_20040916_054013_raw.all')
%   aKM = cl_simrad_all('nomFic', nomFic);
%   plot_seabedImage_V1(aKM)
%
% See also cl_simrad_all Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function plot_seabedImage_V1(this, varargin)

if isempty(this)
    return
end

[varargin, sub] = getPropertyValue(varargin, 'sub', []);
[varargin, Fig] = getPropertyValue(varargin, 'Fig', []); %#ok<ASGLU>

if isempty(Fig)
    Fig = FigUtils.createSScFigure('Name', 'SeabeImage datagrams');
else
    FigUtils.createSScFigure(Fig);
end


NbFic = length(this);
hw = create_waitbar('Plot .all Plot Seabed Image Parameters', 'N', NbFic);
for k=1:NbFic
    my_waitbar(k, NbFic, hw);
    plot_seabedImage_V1_unitaire(this(k), sub, Fig)
end
my_close(hw, 'MsgEnd');


function plot_seabedImage_V1_unitaire(this, sub, Fig)

%% Lecture des datagrams seabedImage

[flag, DataSeabed] = SSc_ReadDatagrams(this.nomFic, 'Ssc_SeabedImage53h', 'Memmapfile', -1);
if ~flag
    return
end

%% Affichage des courbes

[~, nomFic] = fileparts(this.nomFic);

if isempty(DataSeabed.MeanAbsorpCoeff)
    return
end

if ~isfield(DataSeabed, 'EmModel') && isfield(DataSeabed, 'Model')
    DataSeabed.EmModel = DataSeabed.Model;
end

FigName = sprintf('Seabed Image Datagrams : Sondeur EM%d,  Serial Number %d, Ship : %s', ...
    DataSeabed.EmModel, DataSeabed.SystemSerialNumber(1), ShipName(cl_sounder([]), DataSeabed.SystemSerialNumber(1)));

FigUtils.createSScFigure(Fig);
set(Fig, 'name', FigName)
grid on; hold on;

hc = findobj(gca, 'Type', 'line');
nbCourbes = length(hc);
ColorOrder = get(gca, 'ColorOrder');
nbCoul = size(ColorOrder,1);
iCoul = mod(nbCourbes-1, nbCoul) + 1;

T = DataSeabed.Time.timeMat;
T = datetime(T, 'ConvertFrom', 'datenum');
if isempty(sub)
    sub = 1:length(T);
end

% t = DataSeabed.Time.timeMat;
% str = timeMat2str([t(1), t(end)]);
% str = [str repmat(' ', 2, 2)]';
% str = sprintf('display(''%s - %s'')', nomFic, str(:));
str = sprintf('display(''%s - %s'')', nomFic, char(T(1)), char(T(end)));

haxe(1) = subplot(5,2,1);
h = PlotUtils.createSScPlot(T(sub), DataSeabed.MeanAbsorpCoeff(sub,:)); hold on;
set(h, 'Color', ColorOrder(iCoul,:))
set(h, 'ButtonDownFcn', str)
grid on; title('MeanAbsorpCoeff (dB/km)')

haxe(2) = subplot(5,2,2);
h = PlotUtils.createSScPlot(T(sub), DataSeabed.PulseLength(sub,:)); hold on;
set(h, 'Color', ColorOrder(iCoul,:))
set(h, 'ButtonDownFcn', str)
grid on; title('PulseLength (ms)')

haxe(3) = subplot(5,2,3);
h = PlotUtils.createSScPlot(T(sub), DataSeabed.TVGN(sub,:)); hold on;
set(h, 'Color', ColorOrder(iCoul,:))
set(h, 'ButtonDownFcn', str)
grid on; title('Rn (samples)')

haxe(4) = subplot(5,2,4);
h = PlotUtils.createSScPlot(T(sub), DataSeabed.TVGStart(sub,:)); hold on;
set(h, 'Color', ColorOrder(iCoul,:))
set(h, 'ButtonDownFcn', str)
grid on; title('TVGStart (samples)')

haxe(5) = subplot(5,2,5);
h = PlotUtils.createSScPlot(T(sub), DataSeabed.TVGStop(sub,:)); hold on;
set(h, 'Color', ColorOrder(iCoul,:))
set(h, 'ButtonDownFcn', str)
grid on; title('TVGStop (samples)')

haxe(6) = subplot(5,2,6);
h = PlotUtils.createSScPlot(T(sub), DataSeabed.BSN(sub,:)); hold on;
set(h, 'Color', ColorOrder(iCoul,:))
set(h, 'ButtonDownFcn', str)
grid on; title('BSN (dB)')

haxe(7) = subplot(5,2,7);
h = PlotUtils.createSScPlot(T(sub), DataSeabed.BSO(sub,:)); hold on;
set(h, 'Color', ColorOrder(iCoul,:))
set(h, 'ButtonDownFcn', str)
grid on; title('BSO (dB)')

haxe(8) = subplot(5,2,8);
h = PlotUtils.createSScPlot(T(sub), DataSeabed.TxBeamWidth(sub,:)); hold on;
set(h, 'Color', ColorOrder(iCoul,:))
set(h, 'ButtonDownFcn', str)
grid on; title('TxBeamWidth (deg)')

haxe(9) = subplot(5,2,9);
h = PlotUtils.createSScPlot(T(sub), DataSeabed.TVGCrossOver(sub,:)); hold on;
set(h, 'Color', ColorOrder(iCoul,:))
set(h, 'ButtonDownFcn', str)
grid on; title('TVGCrossOver (deg)')

if isfield(DataSeabed, 'NbFaisceaux') % Anciens Ssc formats
    haxe(10) = subplot(5,2,10);
    h = PlotUtils.createSScPlot(T(sub), DataSeabed.NbFaisceaux(sub,:)); hold on;
    set(h, 'Color', ColorOrder(iCoul,:))
    set(h, 'ButtonDownFcn', str)
    grid on; title('NbFaisceaux')
end
if isfield(DataSeabed, 'NbBeams') % Nouveaux Ssc formats
    haxe(10) = subplot(5,2,10);
    h = PlotUtils.createSScPlot(T(sub), DataSeabed.NbBeams(sub,:)); hold on;
    set(h, 'Color', ColorOrder(iCoul,:))
    set(h, 'ButtonDownFcn', str)
    grid on; title('NbBeams')
end

linkaxes(haxe, 'x');
