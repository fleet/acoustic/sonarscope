% Affichage des informations contenues dans les datagrams "seabedImage" d'un fichier .all
%
% Syntax
%   plot_seabedImage_V2(aKM)
%
% Input Arguments
%   aKM : Instance de cl_simrad_all
%
% Examples
%   nomFic = getNomFicDatabase('EM1002_0001_20040916_054013_raw.all')
%   aKM = cl_simrad_all('nomFic', nomFic);
%   plot_seabedImage_V2(aKM)
%
% See also cl_simrad_all Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function plot_seabedImage_V2(this, varargin)

if isempty(this)
    return
end

[varargin, sub] = getPropertyValue(varargin, 'sub', []);
[varargin, Fig] = getPropertyValue(varargin, 'Fig', []); %#ok<ASGLU>

if isempty(Fig)
    Fig = FigUtils.createSScFigure('Name', 'SeabeImage datagrams');
else
    FigUtils.createSScFigure(Fig);
end

for k=1:length(this)
    plot_seabedImage_V2_unitaire(this(k), sub, Fig)
end


function  plot_seabedImage_V2_unitaire(this, sub, Fig)

%% Lecture des datagrams seabedImage

[flag, Data, SeabedImageFormat] = read_seabedImage_bin(this, 'Memmapfile', 1); %#ok<ASGLU>
if ~flag
    return
end

%% Affichage des courbes

[~, nomFic] = fileparts(this.nomFic);
if isempty(Data.TVGN)
    return
end

if isfield(Data, 'EmModel')
    Model = Data.EmModel;
else
    Model = Data.Model;
end
FigName = sprintf('Seabed Image Datagrams : Sondeur EM%d,  Serial Number %d, Ship : %s', ...
    Model, Data.SystemSerialNumber(1), ShipName(cl_sounder([]), Data.SystemSerialNumber(1)));

FigUtils.createSScFigure(Fig);
set(Fig, 'name', FigName)
grid on; hold on;

hc = findobj(gca, 'Type', 'line');
nbCourbes = length(hc);
ColorOrder = get(gca, 'ColorOrder');
nbCoul = size(ColorOrder,1);
iCoul = mod(nbCourbes-1, nbCoul) + 1;

T = Data.Time.timeMat;
T = datetime(T, 'ConvertFrom', 'datenum');
if isempty(sub)
    sub = 1:length(T);
end

% t = Data.Time.timeMat;
% str = timeMat2str([t(1), t(end)]);
% str = [str repmat(' ', 2, 2)]';
% str = sprintf('display(''%s - %s'')', nomFic, str(:));
str = sprintf('display(''%s - %s'')', nomFic, char(T(1)), char(T(end)));

haxe = preallocateGraphics(0,0);

haxe(end+1) = subplot(4,2,1);
X = Data.PingCounter(sub,:);
h = PlotUtils.createSScPlot(T(sub), double(X)); hold on;
set(h, 'Color', ColorOrder(iCoul,:))
set(h, 'ButtonDownFcn', str)
grid on; title('PingCounter')

haxe(end+1) = subplot(4,2,2);
X = Data.SamplingRate(sub,:);
h = PlotUtils.createSScPlot(T(sub), double(X)); hold on;
set(h, 'Color', ColorOrder(iCoul,:))
set(h, 'ButtonDownFcn', str)
grid on; title('SamplingRate (Hz)')

haxe(end+1) = subplot(4,2,3);
X = Data.BSN(sub,:);
h = PlotUtils.createSScPlot(T(sub), double(X)); hold on;
set(h, 'Color', ColorOrder(iCoul,:))
set(h, 'ButtonDownFcn', str)
grid on; title('BSN (dB)')

haxe(end+1) = subplot(4,2,4);
X = Data.BSO(sub,:);
h = PlotUtils.createSScPlot(T(sub), double(X)); hold on;
set(h, 'Color', ColorOrder(iCoul,:))
set(h, 'ButtonDownFcn', str)
grid on; title('BSO (dB)')

haxe(end+1) = subplot(4,2,5);
X = Data.TVGN(sub,:);
h = PlotUtils.createSScPlot(T(sub), double(X)); hold on;
set(h, 'Color', ColorOrder(iCoul,:))
set(h, 'ButtonDownFcn', str)
grid on; title('Rn (samples)')

haxe(end+1) = subplot(4,2,6);
X = Data.BSN(sub,:)-Data.BSO(sub,:);
h = PlotUtils.createSScPlot(T(sub), double(X)); hold on;
set(h, 'Color', ColorOrder(iCoul,:))
set(h, 'ButtonDownFcn', str)
grid on; title('BSN-BSO (dB)')

haxe(end+1) = subplot(4,2,7);
X = Data.TxBeamWidth(sub,:);
h = PlotUtils.createSScPlot(T(sub), double(X)); hold on;
set(h, 'Color', ColorOrder(iCoul,:))
set(h, 'ButtonDownFcn', str)
grid on; title('TxBeamWidth (deg)')

haxe(end+1) = subplot(4,2,8);
X = Data.TVGCrossOver(sub,:);
h = PlotUtils.createSScPlot(T(sub), double(X)); hold on;
set(h, 'Color', ColorOrder(iCoul,:))
set(h, 'ButtonDownFcn', str)
grid on; title('TVGCrossOver (deg)')

linkaxes(haxe, 'x');
