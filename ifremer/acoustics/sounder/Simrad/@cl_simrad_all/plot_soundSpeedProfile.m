% Affichage des informations contenues dans les datagrams "soundSpeedProfile" d'un fichier .all
%
% Syntax
%   plot_soundSpeedProfile(aKM)
%
% Input Arguments
%   aKM : Instance de cl_simrad_all
%
% Examples
%   nomFic = getNomFicDatabase('EM1002_0001_20040916_054013_raw.all')
%   aKM = cl_simrad_all('nomFic', nomFic);
%   plot_soundSpeedProfile(aKM)
%
% See also cl_simrad_all Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [FigNav, FigSSP_Full, FigSSP_Partial, nbEffectiveSSP] = plot_soundSpeedProfile(this, varargin)

[varargin, IndNavUnit]  = getPropertyValue(varargin, 'IndNavUnit', []);
[varargin, FigSSP_Full] = getPropertyValue(varargin, 'Fig',        []); %#ok<ASGLU>

FigSSP_Full    = [];
FigSSP_Partial = [];
nbEffectiveSSP = [];

%% Trac� de la navigation

FigNav = ALL_PlotNav(this, 'IndNavUnit', IndNavUnit, 'Name', 'Sound Speed Profile', 'Title', 'SSP positions (Red=new SSP, black=repeated SSP)');
 if isempty(FigNav)
    return
end

%% Lecture des profils de c�l�rit�

nSSP = 0;
for k=1:length(this)
    [flag, DataSSP] = SSc_ReadDatagrams(this(k).nomFic, 'Ssc_SoundSpeedProfile');
    if ~flag || isempty(DataSSP)
        continue
    end
    DataSSP.nomFic = this(k).nomFic;
    Data(k) = DataSSP; %#ok<AGROW>
    nSSP(k) = length(DataSSP.DatetimeStartOfUse); %#ok<AGROW>
end
% nbSSP = sum(nSSP);

%% Concat�nation des profils de c�l�rit� au cas o� il y en aurait plusieurs pour certain fichiers

DataNew = [];
for k1=1:length(Data)
    for k2=1:nSSP(k1)
        
        [n1, n2] = size(Data(k1).Depth);
        if n1 > n2 % ALL
            Data(k1).Depth      = Data(k1).Depth';
            Data(k1).SoundSpeed = Data(k1).SoundSpeed';
            if isfield(Data(k1), 'Temperature')
                Data(k1).Temperature = Data(k1).Temperature';
                Data(k1).Salinity    = Data(k1).Salinity';
            end
        end
        
        if isempty(DataNew)
            DataNew = Data(k1);
        else
            DataNew(end+1) = Data(k1); %#ok<AGROW>
        end
        DataNew(end).TimeStartOfUse     = Data(k1).TimeStartOfUse(k2);
        DataNew(end).DatetimeStartOfUse = Data(k1).DatetimeStartOfUse(k2);
        DataNew(end).Depth(k2,:)      = Data(k1).Depth(k2,:);
        DataNew(end).SoundSpeed(k2,:) = Data(k1).SoundSpeed(k2,:);
        if isfield(Data(k1), 'Temperature')
            DataNew(end).Temperature(k2,:) = Data(k1).Temperature(k2,:);
            DataNew(end).Salinity(k2,:)    = Data(k1).Salinity(k2,:);
        end
    end
end
Data = DataNew;

%% Tri des profils de c�l�rit� par date

for k=length(Data):-1:1
    TData(k) = Data(k).DatetimeStartOfUse(1);
end
[~, ind] = sort(TData);
Data = Data(ind);

%% Rep�rage des profils de c�l�rit� qui changent de valeur

flagDataNew = false(1,length(Data));
flagDataNew(1) = true;
for k=length(Data):-1:2
    if ~isequal(Data(k).Depth, Data(k-1).Depth) || ~isequal(Data(k).SoundSpeed, Data(k-1).SoundSpeed)
        flagDataNew(k) = true;
    end
end
nbEffectiveSSP = sum(flagDataNew);

%% Cr�ation de la figure

if isempty(FigSSP_Full)
    Position = centrageFig(1200, 500);
    FigSSP_Full = FigUtils.createSScFigure('Position', Position);
else
    if ~ishandle(FigSSP_Full)
        Position = centrageFig(1200, 500);
        FigSSP_Full = FigUtils.createSScFigure('Position', Position);
    end
end

%% Trac� des profils de c�l�rit�

ZMin = 0;
Legende = {};
for k=1:length(Data)
    if flagDataNew(k)
        [~, nomFic] = fileparts(Data(k).nomFic);
        Legende = plot_soundSpeedProfile_unitaire(Data(k), FigSSP_Full, nomFic, Legende, k);
        
        d = diff(Data(k).Depth);
        seuil = median(d) * 20;
        ind = find(abs(d) > abs(seuil));
        if ~isempty(ind)
            ZMin = min(ZMin, Data(k).Depth(ind(1)));
        end
    end
end
title('Full Sound Speed Profiles')
legend(Legende, 'Interpreter', 'none', 'Location', 'eastoutside')

flagPartial = 0;
Position = centrageFig(1200, 500);
FigSSP_Partial = FigUtils.createSScFigure('Position', Position);
for k=1:length(Data)
    if flagDataNew(k)
        [~, nomFic] = fileparts(Data(k).nomFic);
        kFin = find((abs(Data(k).Depth(1,:)) < abs(ZMin)), 1, 'last');
        if isempty(kFin)
            sub = 1:ceil(Data(k).Dimensions.nbPoints / 2);
            flagPartial = 1;
        else
            sub = 1:(kFin-1);
            flagPartial = 1;
        end
        plot_soundSpeedProfile_unitaire(Data(k), FigSSP_Partial, nomFic, Legende, k, 'sub', sub);
    end
end
if flagPartial
    title('Partial Sound Speed Profiles')
    legend(Legende, 'Interpreter', 'none', 'Location', 'eastoutside')
else
    close(FigSSP_Partial);
    FigSSP_Partial = [];
end

%% Trac� des endroits o� il y a changement de profil de c�l�rit�

tol = 2 / (24*3600); % 2 secondes de tol�rance
for k1=1:length(this)
    [flag, DataPosition] = read_position_bin(this(k1), 'IndNavUnit', IndNavUnit, 'Memmapfile', 0);
    if ~flag
        continue
    end
    TNav = DataPosition.Datetime;
    
    for k2=1:length(Data)
        T2 = Data(k2).DatetimeStartOfUse;
        % figure; plot(TNav - T2, '-*'); grid on;
        if ((T2 + tol) >= TNav(1)) && ((T2 - tol) <= TNav(end))
            LatSSp = interp1(DataPosition.Datetime, DataPosition.Latitude,  T2, 'linear', 'extrap');
            LonSSp = interp1(DataPosition.Datetime, DataPosition.Longitude, T2, 'linear', 'extrap');
            
            if flagDataNew(k2)
                color = '*r';
            else
                color = '*k';
            end
            
            figure(FigNav)
            PlotUtils.createSScPlot(LonSSp, LatSSp, color);
        end
    end
end
    


function Legende = plot_soundSpeedProfile_unitaire(Data, Fig, nomFic, Legende, k, varargin)

[varargin, sub] = getPropertyValue(varargin, 'sub', 1:size(Data.Depth,2)); %#ok<ASGLU>

figure(Fig)

%% Affichage de la navigation

ColorOrder = get(gca, 'ColorOrder');
nbCoul = size(ColorOrder,1);

t = Data.DatetimeStartOfUse;
Z = Data.Depth(1,sub);
C = Data.SoundSpeed(1,sub);
h = PlotUtils.createSScPlot(C, Z);
grid on; hold on;

iCoul = mod(k, nbCoul) + 1;
set(h, 'Color', ColorOrder(iCoul,:), 'Marker', '.')

Legende{end+1} = sprintf('%s - %s - %d', nomFic, char(t), k);
