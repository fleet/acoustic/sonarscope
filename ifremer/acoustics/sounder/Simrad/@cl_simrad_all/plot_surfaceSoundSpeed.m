% Affichage des informations contenues dans les datagrams "surfaceSoundSpeed" d'un fichier .all
%
% Syntax
%   plot_surfaceSoundSpeed(aKM)
%
% Input Arguments
%   aKM : Instance de cl_simrad_all
%
% Examples
%   nomFic = getNomFicDatabase('EM1002_0001_20040916_054013_raw.all')
%   aKM = cl_simrad_all('nomFic', nomFic);
%   plot_surfaceSoundSpeed(aKM)
%
% See also cl_simrad_all Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function plot_surfaceSoundSpeed(this, varargin)

% [varargin, sub] = getPropertyValue(varargin, 'sub', []);
[varargin, Fig] = getPropertyValue(varargin, 'Fig', []); %#ok<ASGLU>

if isempty(Fig)
    Fig = FigUtils.createSScFigure('Name', 'SurfaceSoundSpeed datagrams');
else
    FigUtils.createSScFigure(Fig);
end


NbFic = length(this);
hw = create_waitbar('Plot .all Runtime Parameters', 'N', NbFic);
for i=1:NbFic
    my_waitbar(i, NbFic, hw);
    plot_surfaceSoundSpeed_unitaire(this(i), Fig)
end
my_close(hw, 'MsgEnd');


function plot_surfaceSoundSpeed_unitaire(this, Fig)

%% Lecture des datagrams d'surfaceSoundSpeed

[flag, DataSurfaceSoundSpeed] = SSc_ReadDatagrams(this.nomFic, 'Ssc_SurfaceSoundSpeed');
if ~flag
    return
end

%% Affichage de la navigation

[~, nomFic] = fileparts(this.nomFic);

FigName = sprintf('Sondeur %d Serial Number  %d', DataSurfaceSoundSpeed.EmModel, DataSurfaceSoundSpeed.SystemSerialNumber);

FigUtils.createSScFigure(Fig);
set(Fig, 'name', FigName)

hc = findobj(gca, 'Type', 'line');
nbCourbes = length(hc);
ColorOrder = get(gca, 'ColorOrder');
nbCoul = size(ColorOrder,1);
iCoul = mod(nbCourbes-1, nbCoul) + 1;

t = DataSurfaceSoundSpeed.Time.timeMat;
t = datetime(t, 'ConvertFrom', 'datenum');
str = sprintf('display(''%s - %s %s'')', nomFic, char(t(1)), char(t(end)));


h = PlotUtils.createSScPlot(t, DataSurfaceSoundSpeed.SurfaceSoundSpeed(:,:)); hold on;
set(h, 'Color', ColorOrder(iCoul,:))
set(h, 'ButtonDownFcn', str)
grid on; title('Surface sound speed (m/s)')

