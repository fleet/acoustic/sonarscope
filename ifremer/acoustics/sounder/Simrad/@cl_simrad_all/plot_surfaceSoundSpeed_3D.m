% Affichage des informations contenues dans les datagrams "surfaceSoundSpeed" d'un fichier .all
%
% Syntax
%   plot_surfaceSoundSpeed_3D(aKM)
%
% Input Arguments
%   aKM : Instance de cl_simrad_all
%
% Examples
%   nomFic = getNomFicDatabase('EM1002_0001_20040916_054013_raw.all')
%   aKM = cl_simrad_all('nomFic', nomFic);
%   plot_surfaceSoundSpeed_3D(aKM)
%
% See also cl_simrad_all Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function plot_surfaceSoundSpeed_3D(this, varargin)

[varargin, IndNavUnit] = getPropertyValue(varargin, 'IndNavUnit', []);
% [varargin, sub]      = getPropertyValue(varargin, 'sub',        []);
[varargin, Fig]        = getPropertyValue(varargin, 'Fig',        []); %#ok<ASGLU>

if isempty(Fig)
    Fig = FigUtils.createSScFigure('Name', 'SurfaceSoundSpeed datagrams');
else
    FigUtils.createSScFigure(Fig);
end

NbFic = length(this);
hw = create_waitbar('Plot .all Runtime Parameters', 'N', NbFic);
for i=1:NbFic
    my_waitbar(i, NbFic, hw);
    plot_surfaceSoundSpeed_3D_unitaire(this(i), IndNavUnit, Fig)
end
my_close(hw, 'MsgEnd');


function plot_surfaceSoundSpeed_3D_unitaire(this, IndNavUnit, Fig)

%% Lecture des datagrams d'surfaceSoundSpeed

[flag, DataSurfaceSoundSpeed] = SSc_ReadDatagrams(this.nomFic, 'Ssc_SurfaceSoundSpeed');
if ~flag
    return
end

[flag, DataPosition] = read_position_bin(this, 'IndNavUnit', IndNavUnit);
if ~flag || isempty(DataPosition)
    return
end

Longitude = interp1(DataPosition.Time, DataPosition.Longitude, DataSurfaceSoundSpeed.Time);
Latitude  = interp1(DataPosition.Time, DataPosition.Latitude,  DataSurfaceSoundSpeed.Time);

%% Affichage de la navigation

[~, nomFic] = fileparts(this.nomFic);

FigName = sprintf('Sondeur %d Serial Number  %d', DataSurfaceSoundSpeed.EmModel, DataSurfaceSoundSpeed.SystemSerialNumber);

FigUtils.createSScFigure(Fig);
set(Fig, 'name', FigName)

hc = findobj(gca, 'Type', 'line');
nbCourbes = length(hc);
ColorOrder = get(gca, 'ColorOrder');
nbCoul = size(ColorOrder,1);
iCoul = mod(nbCourbes-1, nbCoul) + 1;

t = DataSurfaceSoundSpeed.Time.timeMat;
t = datetime(t, 'ConvertFrom', 'datenum');
% str = timeMat2str([t(1), t(end)]);
% str = [str repmat(' ', 2, 2)]';
str = sprintf('display(''%s - %s %s'')', nomFic, char(t(1)), char(t(end)));

% T = DataSurfaceSoundSpeed.Time;
% if isempty(sub)
%     sub = 1:length(T);
% end

% h = PlotUtils.createSScPlot3(Longitude(sub), Latitude(sub), DataSurfaceSoundSpeed.SurfaceSoundSpeed(:,:)); hold on;
h = PlotUtils.createSScPlot3(Longitude, Latitude, DataSurfaceSoundSpeed.SurfaceSoundSpeed(:,:)); hold on;
set(h, 'Color', ColorOrder(iCoul,:))
set(h, 'ButtonDownFcn', str)
grid on; title('Surface sound speed (m/s)')
