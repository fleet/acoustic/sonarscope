% Transformation en chaine de caracteres d'une instance
%
% Syntax
%   str = char_instance(a)
%
% Input Arguments
%   a : instance de cl_simrad_all
%
% Name-Value Pair Arguments
%   InfoSonar  : 1=Information de l'image uniquement
%                2=Info de l'information sonar uniquement
%                3=Display de toute l'information disponible (image+sonar) : (3 par defaut)
%
% Output Arguments
%   str : une chaine de caracteres representative de l instance
%
% Examples
%
% See also cl_simrad_all Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function str = char_instance(this, varargin)

str = [];

str{end+1} = sprintf('nomFic             <-> %s', this.nomFic);
str{end+1} = sprintf('nomFicWC           <-- %s', this.nomFicWC);
str{end+1} = sprintf('flagRTK            <-- %d', this.flagRTK);
str{end+1} = sprintf('EmModel            <-- %d', this.EmModel);
str{end+1} = sprintf('SystemSerialNumber <-- %d', this.SystemSerialNumber);
str{end+1} = sprintf('Version            <-- %s', this.Version);
str{end+1} = sprintf('SousVersion        <-- %s', this.SousVersion);

if ~isempty(this.nomFic)
    str{end+1} = sprintf('--- Histogram of datagrams ---');
    strHisto = histo_index(this, 'minimal', 1);
    if isempty(strHisto)
        return
    end
    strHisto = strsplit(strHisto, newline);
    for k=1: length(strHisto)
        str{end+1} = sprintf('                %s', strHisto{k}); %#ok<AGROW>
    end
end

%% Concatenation en une cha�ne de caract�res

str = cell2str(str);
