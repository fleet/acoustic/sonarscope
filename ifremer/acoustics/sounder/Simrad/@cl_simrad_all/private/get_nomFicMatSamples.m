function nomFicMatSamples = get_nomFicMatSamples(nomDir, nomFic, i)
nomFicMatSamples = sprintf('%s%sSonarScope%s%s_seabedImageSamples_%05d.mat', ...
                nomDir, filesep, filesep, nomFic, i);
