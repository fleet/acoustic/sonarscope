function InterpParameter = interpRuntime(RuntimeTime, RuntimeParameter, InterpTime)

RuntimeTime = RuntimeTime.timeMat;
InterpTime  = InterpTime.timeMat;
RuntimeParameter = RuntimeParameter(:,:);
% figure; plot(RuntimeTime, '-xr'); grid; hold on; plot(InterpTime, '-+b')
InterpParameter = NaN(size(InterpTime), class(RuntimeParameter));
for k=length(RuntimeTime):-1:1
    sub = find(InterpTime >= RuntimeTime(k));
    InterpParameter(sub) = RuntimeParameter(k);
    InterpTime(sub) = [];
end
