function str = interpretation_runtime(EmModel, Layer, Y)

str = '';
Y = double(Y); % sinon bitand buggue

strModeOther  = {'Very shallow'; 'Shallow'; 'Medium'; 'Deep'; 'Very deep'; 'Extra deep'};
strModeEM3000 = {'Nearfield'; 'Normal'; 'Targetdetect'};

switch Layer
    case 'SourceSoundSpeed'
        str = sprintf('%s \t | 0=From real time sensor, 1=Manually entered by operator, 3=Interpolated from currently used sound speed profile', str);
        
    case 'BeamSpacing'
        str = sprintf('%s \t | 0=Determined by beamwidth, 1=Equidistant, 2=Equiangle, 3=InBetween', str);
        
    case 'YawStabMode'
        str = sprintf('%s \t | 0=No, 1=Yes to survey line, 2=Yes to mean vessel heading, 3=Yes to manually entered headind', str);

    case 'PitchStabMode'
        str = sprintf('%s \t | 0=No, 8=Yes', str);
        
    case {'OperatorStationStatus'; 'ProcessingUnitStatus'; 'BSPStatus'; 'SonarHeadStatus'}
        if any(Y)
            str = sprintf('%s \t | ~=0 --> Something wrong', str);
        end
        
    case 'Mode'
        switch EmModel
            case 3000
                strIci = strModeEM3000;
            otherwise
                strIci = strModeOther;
        end
        for i=1:length(Y)
            str = sprintf('%s \t : %d --> %s', str, Y(i), strIci{Y(i)});
        end
        
    case 'FilterIdentifier'
        X = bitand(Y, 3);
        for i=1:length(X)
            switch X(i)
                case 1
                    str = sprintf('%s : Spike filter is set to Weak', str);
                case 2
                    str = sprintf('%s : Spike filter is set to Medium', str);
                case 3
                    str = sprintf('%s : Spike filter is set to Strong', str);
            end
        end
        
        X = bitand(Y, 4);
        for i=1:length(X)
            if X(i) == 4
                str = sprintf('%s : Slope filter is on', str);
            end
        end

        X = bitand(Y, 8);
        for i=1:length(X)
            if (X(i) == 8) && ~((EmModel == 1002) || (EmModel == 3002))
                str = sprintf('%s : Sector tracking or Robust BottomDetection is on', str);
                my_warndlg('Sector tracking or Robust BottomDetection is on : it is impossible to calibrate the imagery.', ...
                    0, 'Tag', 'Sector tracking');
            end
        end
        
        Y = bitshift(Y, -4);
        
        X = bitand(Y, 6);
        for i=1:length(X)
            switch X(i)
                case 1
                    str = sprintf('%s : Range gates are Large', str);
                case 8
                    str = sprintf('%s : Range gates are Small', str);
            end
        end
        
        X = bitand(Y, 4);
        for i=1:length(X)
            if X(i) == 4
                str = sprintf('%s : Interference filter is on', str);
            end
        end
end
