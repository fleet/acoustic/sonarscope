function process_SimradAll_SOSBeamPointingAngle(this)


N = length(this);
str1 = 'SOS BeamPointingAngle';
str2 = 'SOS BeamPointingAngle';
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    fprintf('%s\n%d / %d : %s\n', repmat('-',1,100), k, N, this{k});
    
    process_SimradAll_SOSBeamPointingAngle_unitaire(this(k));
end
my_close(hw, 'MsgEnd')



function process_SimradAll_SOSBeamPointingAngle_unitaire(this)

[flag, DataDepth] = read_depth_bin(this, 'Memmapfile', 0);
if ~flag
    return
end

if strcmp(DataDepth.Version, 'V1')
    str1 = 'Ce SOS n''est pas cabl� pour les anciens datagrammes.';
    str2 = 'This SOS is not plugged for ols formats.';
    my_warndlg(Lang(str1,str2), 0, 'Tag', 'SOSBeamPointingAnglePasFaitPourV1');
else
    [flag, DataRaw] = SSc_ReadDatagrams(this.nomFic, 'Ssc_RawRangeBeamAngle', 'Memmapfile', -1); % Non test� ici
    if ~flag
        % TODO : sortir un message
        return
    end
end

isModified = 0;
for kPing=1:size(DataRaw.BeamPointingAngle, 1)
    X = DataRaw.BeamPointingAngle(kPing,:);
    X = X(~isnan(X));
    if length(X) > 2
        isModified = (X(1) > X(end));
        break
    end
end

if  isModified
    flag = save_image(this, 'Ssc_RawRangeBeamAngle', 'BeamPointingAngle', -DataRaw.BeamPointingAngle);
    if ~flag
        str1 = sprintf('Le fichier "%s" n''a pas pu �tre modifi�.', this.nomFic);
        str2 = sprintf('"%s" could not be modified.', this.nomFic);
        my_warndlg(Lang(str1,str2), 0);
    end
end
