function process_SimradAll_SOSLevelArm(this, Ax, Ay, Az)

N = length(this);
str1 = 'Correction des bras de levier';
str2 = 'Level arm correction';
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    fprintf('%s\n%d / %d : %s\n', repmat('-',1,100), k, N, this(k).nomFic);
    
    process_SimradAll_SOSLevelArm_unitaire(this(k), Ax, Ay, Az);
end
my_close(hw, 'MsgEnd')


function process_SimradAll_SOSLevelArm_unitaire(this, Ax, Ay, Az)

[flag, DataDepth] = read_depth_bin(this, 'Memmapfile', 0);
if ~flag
    % TODO : message
end

[flag, DataAttitude] = read_attitude_bin(this);
if ~flag
    % TODO : message
end
Heave = DataAttitude.Heave;
p = DataAttitude.Pitch;
r = DataAttitude.Roll;

HeaveNew = (-Ax *sind(p) + Ay * cosd(p) .* sind(r) + Az * cosd(r) .* cosd(p) - Az);

%{
     FigUtils.createSScFigure;
     h(1) = subplot(5,1,1); PlotUtils.createSScPlot(p, 'b'); grid on; title('Pitch')
     h(2) = subplot(5,1,2); PlotUtils.createSScPlot(r, 'b'); grid on; title('Roll')
     h(3) = subplot(5,1,3); PlotUtils.createSScPlot(Heave, 'b'); grid on; title('Heave Data')
     h(4) = subplot(5,1,4); PlotUtils.createSScPlot(HeaveNew, 'b'); grid on; title('Heave new');
     h(5) = subplot(5,1,5); PlotUtils.createSScPlot(Heave, 'b'); hold on; PlotUtils.createSScPlot(HeaveNew, 'r'); grid on; title('Heave Data et new');
     linkaxes(h, 'x'); axis tight; drawnow;
%}
DataAttitude.Heave = Heave + HeaveNew;
flag = save_signal(this, 'Ssc_Attitude', 'Heave',  DataAttitude.Heave);
if ~flag
    my_close(hw, 'MsgEnd')
    return
end

TD = my_interp1(DataAttitude.Time, HeaveNew, DataDepth.Time, 'linear', 'extrap');
DataDepth.TransducerDepth = DataDepth.TransducerDepth + TD;

flag = save_signal(this, 'Ssc_Depth', 'TransducerDepth',  DataDepth.TransducerDepth);
if ~flag
    my_close(hw, 'MsgEnd')
    return
end
