% nomFic = 'D:\Temp\SHOM\EM710\0001_20140327_134856_LPO.all';
% aKM = cl_simrad_all('nomFic', nomFic);
%
% [flag, Data] = read_ExtraParameters_bin(aKM)

function [flag, Data] = read_ExtraParameters_bin(this, varargin)

%% Lecture du r�pertoire SonarScope

[flag, Data] = SSc_ReadDatagrams(this.nomFic, 'SSc_ExtraParameters', 'XMLOnly', 1);
if ~flag
    return
end

%% Lecture du BSCOrr

[nomDir, nom] = fileparts(this.nomFic);
nomDir = fullfile(nomDir, 'SonarScope', nom);
nomDirData = fullfile(nomDir, 'Ssc_ExtraParameters');

listeFic = listeFicOnDir(nomDirData, '.txt');
if isempty(listeFic) % Ajout JMA le 13/12/2021 pour contourner l'oubli de stockage du bscorr dans le fichier Netcdf
    nomDirData = fileparts(fileparts(nomDirData));
    listeFic = listeFicOnDir(nomDirData, '.txt', 'Filtre', 'BScorr_');
end
for k=1:length(listeFic)
    [flag, BSCorr, BSCorrInfo] = read_BSCorr(listeFic{k});
    if flag
        Data.BsCorr = BSCorr;
        Data.BSCorrInfo = BSCorrInfo;
    else
        str1 = sprintf('Le fichier "%s" n''a pas pu �tre d�cod�.', listeFic{k});
        str2 = sprintf('"%s" could not be read.', listeFic{k});
        my_warndlg(Lang(str1,str2), 0, 'Tag', 'FichierBSCorrPasLisible');
        my_warndlg(Lang(str1,str2), 0, 'Tag', 'FichierExtraParamsPasEncoreDecode');
    end
end
