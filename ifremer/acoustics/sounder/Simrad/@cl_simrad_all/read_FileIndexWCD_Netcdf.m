% nomFic = 'D:\Temp\VLIZ\Lifewatch(19-740)_22242018_EM2040d\0013_20191022_173330_SimonStevin.all';
% aV2c = cl_simrad_all('nomFic', nomFic);
%
% [flag, Data1] = read_FileIndexWCD_Netcdf(aV2c)
% [flag, Data2] = SSc_ReadDatagrams(nomFic, 'WCD_FileIndex')
% [flag, Data3] = SSc_ReadDatagrams(nomFic, 'FileIndexWCD') % 

function [flag, Data] = read_FileIndexWCD_Netcdf(this)

[flag, Data] = SSc_ReadDatagrams(this.nomFic, 'FileIndexWCD');
if ~flag
    return
end
