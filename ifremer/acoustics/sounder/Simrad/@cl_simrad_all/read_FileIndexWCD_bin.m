% nomFic = 'D:\Temp\VLIZ\Lifewatch(19-740)_22242018_EM2040d\0013_20191022_173330_SimonStevin.all';
% aV2c = cl_simrad_all('nomFic', nomFic);
%
% [flag, Data1] = read_FileIndexWCD_bin(aV2c)
% [flag, Data2] = SSc_ReadDatagrams(nomFic, 'WCD_FileIndex')

function [flag, Data] = read_FileIndexWCD_bin(this)

flag = existCacheNetcdf(this.nomFic);
if flag
    [flag, Data] = read_FileIndexWCD_Netcdf(this);
else
    [flag, Data] = read_FileIndexWCD_XMLBin(this);
end
