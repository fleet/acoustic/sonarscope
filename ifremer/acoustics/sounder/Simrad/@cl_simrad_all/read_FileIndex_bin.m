function [flag, Data] = read_FileIndex_bin(this, varargin)

flag = existCacheNetcdf(this.nomFic);
if flag
    [flag, Data] = read_FileIndex_Netcdf(this.nomFic, varargin{:});
else
    [flag, Data] = read_FileIndex_XMLBin(this.nomFic, varargin{:});
end
