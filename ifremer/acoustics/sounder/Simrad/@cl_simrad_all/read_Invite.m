function [flag, Z, DataPosition, Colors, comment] = read_Invite(this, Invite, nbColors, IndNavUnit, Memmapfile, varargin)

[varargin, flagHisto] = getFlag(varargin, 'Histo'); %#ok<ASGLU>

Z       = [];
Colors  = jet(nbColors);
comment = [];

[flag, DataPosition] = read_position_bin(this, 'IndNavUnit', IndNavUnit, 'Memmapfile', Memmapfile);
if ~flag
    return
end

switch Invite
    case 'Time'
        Z.Data = DataPosition.Time.timeMat;
        Z.Time = DataPosition.Time;
        Z.Datetime = DataPosition.Datetime;
        Z.Unit = '';
        
    case 'Heading'
        Colors = hsv(nbColors);
        [flag, Z] = create_signal_Heading(this, 'DataPosition', DataPosition, 'IndNavUnit', IndNavUnit);
        if ~flag
            return
        end
        
    case 'Speed'
        [flag, Z] = create_signal_Speed(this, 'DataPosition', DataPosition, 'IndNavUnit', IndNavUnit);
        if ~flag
            return
        end
        
    case 'CourseVessel'
        Colors = hsv(nbColors);
        [flag, Z] = create_signal_CourseVessel(this, 'DataPosition', DataPosition, 'IndNavUnit', IndNavUnit);
        if ~flag
            return
        end
        
    case 'Drift'
        [flag, Z] = create_signal_Drift(this, 'DataPosition', DataPosition, 'IndNavUnit', IndNavUnit);
        if ~flag
            return
        end
        
    case {'SoundSpeed'; 'SurfaceSoundSpeed'}
        [flag, Z] = create_signal_SoundSpeed(this);
        if ~flag
            return
        end
        
    case 'y'
        [flag, Z] = create_signal_y(this);
        if ~flag
            return
        end
        
    case 'PingCounter'
        [flag, Z] = create_signal_PingCounter(this);
        if ~flag
            return
        end
        
    case {'AUV Immersion'; 'TransducerDepth'; 'Immersion'}
        [flag, Z] = create_signal_TransducerDepth(this);
        if ~flag
            return
        end
        
    case 'Depth ref seafloor' % 'Immersion+VertDepth'
        [flag, Z] = create_signal_ImmersionPlusTransducerDepth(this);
        if ~flag
            return
        end
        
    case 'AUV Altitude' % 'VertDepth'
        [flag, Z] = create_signal_vertDepth(this);
        if ~flag
            return
        end
        
    case 'Height'
        [flag, Z] = create_signal_height(this);
        if ~flag
            return
        end
        
    case {'Swath width'; 'SwathWidth'}
        [flag, Z] = create_signal_SwathWidth(this);
        if ~flag
            return
        end
        
    case 'SwathWidth / Altitude'
        [flag, Z] = create_signal_SwathWidthOnDepth(this);
        if ~flag
            return
        end
        
    case 'MeanQF'
        [flag, Z] = create_signal_MeanQF(this);
        if ~flag
            return
        end
        
    case 'AngularCoverage'
        [flag, Z] = create_signal_AngularCoverage(this);
        if ~flag
            return
        end
        
    case {'Max Angle'; 'MaxAngle'}
        [flag, Z] = create_signal_MaxAngle(this);
        if ~flag
            return
        end
        
    case {'Mean Roll', 'MeanRoll'}
        [flag, Z] = create_signal_Attitude(this, 'Roll', 'Mean');
        if ~flag
            return
        end
        
    case {'Std Roll'; 'StdRoll'}
        [flag, Z] = create_signal_Attitude(this, 'Roll', 'Std');
        if ~flag
            return
        end
        
    case {'Mean Pitch'; 'MeanPitch'}
        [flag, Z] = create_signal_Attitude(this, 'Pitch', 'Mean');
        if ~flag
            return
        end
        
    case {'Std Pitch'; 'StdPitch'}
        [flag, Z] = create_signal_Attitude(this, 'Pitch', 'Std');
        if ~flag
            return
        end
        
    case {'Mean Heave'; 'MeanHeave'}
        [flag, Z] = create_signal_Attitude(this, 'Heave', 'Mean');
        if ~flag
            return
        end
        
    case {'Std Heave', 'StdHeave'}
        [flag, Z] = create_signal_Attitude(this, 'Heave', 'Std');
        if ~flag
            return
        end
        
    case {'Mean True Heave'; 'MeanTrueHeave'}
        [flag, Z] = create_signal_Attitude(this, 'TrueHeave', 'Mean');
        if ~flag
            return
        end
        
    case {'Std True Heave'; 'StdTrueHeave'}
        [flag, Z] = create_signal_Attitude(this, 'TrueHeave', 'Std');
        if ~flag
            return
        end
        
    case 'Tide'
        [flag, Z] = create_signal_Attitude(this, 'Tide', 'Value');
        if ~flag
            return
        end
        
    case 'Draught'
        [flag, Z] = create_signal_Attitude(this, 'Draught', 'Value');
        if ~flag
            return
        end
        
    case 'Mode'
        [flag, Z] = create_signal_Mode(this);
        if ~flag
            return
        end
        
    case 'EM2040Mode2'
        [flag, Z, DataDepth] = create_signal_Mode(this);
        if ~flag
            return
        end
        Mode = Z.Data;
        [flag, Z] = create_signal_Mode2_EM2040(this, Mode, 'DataDepth', DataDepth);
        if ~flag
            return
        end
        
    case 'EM2040Mode3'
        [flag, Z] = create_signal_Mode3_EM2040(this);
        if ~flag
            return
        end
        
    case {'Inter Pings Distance'; 'InterPingsDistance'}
        [flag, Z] = create_signal_InterPingDistance(this, 'DataPosition', DataPosition, 'flagHisto', flagHisto);
        if ~flag
            return
        end
        
    case {'TVGN'; 'Rn'}
        [flag, Z] = create_signal_Rn(this);
        if ~flag
            return
        end
        
    case 'TVGStart'
        [flag, Z] = create_signal_TVGStart(this);
        if ~flag
            return
        end
        
    case 'TVGStop'
        [flag, Z] = create_signal_TVGStop(this);
        if ~flag
            return
        end
        
    case 'BSN'
        [flag, Z] = create_signal_BSN(this);
        if ~flag
            return
        end
        
    case 'BSO'
        [flag, Z] = create_signal_BSO(this);
        if ~flag
            return
        end
        
    case 'TxBeamWidth'
        [flag, Z] = create_signal_TxBeamWidth(this);
        if ~flag
            return
        end
        
    case 'TVGCrossOver'
        [flag, Z] = create_signal_TVGCrossOver(this);
        if ~flag
            return
        end
        
    case 'SamplingRate'
        [flag, Z] = create_signal_SamplingRate(this);
        if ~flag
            return
        end
        
    case 'NbRxBeams'
        [flag, Z] = create_signal_NbRxBeams(this);
        if ~flag
            return
        end
        
    case {'BSN - BSO'; 'BSN-BSO'}
        [flag, Z] = create_signal_BSNMinusBSO(this);
        if ~flag
            return
        end
        
    case 'EmModel'
        [flag, DataDepth] = read_depth_bin(this, 'Memmapfile', -1);
        if ~flag
            return
        end
        Z.Time = DataDepth.Time;
        Z.Datetime = DataDepth.Datetime;
        Z.Data = repmat(DataDepth.EmModel, length(DataDepth.PingCounter), 1);
        Z.Unit = '';
        
    case 'SystemSerialNumber'
        [flag, DataDepth] = read_depth_bin(this, 'Memmapfile', -1);
        if ~flag
            return
        end
        Z.Time = DataDepth.Time;
        Z.Datetime = DataDepth.Datetime;
        Z.Data = DataDepth.SystemSerialNumber(:,:);
        Z.Unit = '';
        
    case 'AbsorptionCoeff'
        [flag, signalContainerAbsorptionCoeff_RT] = create_signal_AbsorptionCoeff_RT(this);
        if ~flag
            return
        end
        pppp = signalContainerAbsorptionCoeff_RT.signalList.xSample.data;
        T = datenum(pppp); % Modif JMA le 04/01/2019 : bugue si datenum(signalContainerAbsorptionCoeff_RT.signalList.xSample.data) ! retester en version R2018b
        Z.Time = cl_time('timeMat', T);
        Z.Datetime = datetime(T, 'ConvertFrom', 'datenum');
        Z.Data = signalContainerAbsorptionCoeff_RT.signalList.ySample.data;
        Z.Unit = signalContainerAbsorptionCoeff_RT.signalList.ySample.unit;
        
    case {'Sector Tracking or Robust BottomDetection'; 'SectorTrackingOrRobustBottomDetection'}
        [flag, Z] = create_signal_SectorTracking(this);
        if ~flag
            return
        end
        
    case {'Nb Of Swaths'; 'NbSwaths'}
        [flag, Z] = create_signal_nbSwath(this);
        if ~flag
            return
        end
        
    case 'FM-Mode'
        [flag, Z] = create_signal_presenceFM(this);
        if ~flag
            return
        end
        
    case {'WaterColumn'; 'PresenceWC'}
        [flag, Z] = create_signal_presenceWC(this);
        if ~flag
            return
        end
        
    case 'TVGOffsetWC'
        [flag, Z] = create_signal_TVGOffsetWC(this);
        if ~flag
            return
        end
        
    case 'TVGFunctionAppliedWC'
        [flag, Z] = create_signal_TVGFunctionAppliedWC(this);
        if ~flag
            return
        end
        
    case 'MinPulseLength'
        [flag, Z] = create_signal_MinPulseLength(this);
        if ~flag
            return
        end
        
    case {'1/curvature radius'; '1/CurvatureRadius'}
        [flag, Z] = create_signal_CurvatureRadius(this, 'DataPosition', DataPosition, 'IndNavUnit', IndNavUnit); %, 'flagHisto', flagHisto);
        if ~flag
            return
        end
        
    case 'PresenceRTK'
        [flag, Z, comment] = create_signal_PresenceRTK(this, 'IndNavUnit', IndNavUnit);
        if ~flag
            return
        end
        
    otherwise
        str = sprintf('%s not yet available in plot_position_data', Invite);
        my_warndlg(str, 1);
end
flag = 1;

if ~isfield(Z, 'Datetime')
    Z.Datetime = datetime(Z.Time.timeMat, 'ConvertFrom', 'datenum');
end
