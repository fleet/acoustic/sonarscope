function [flag, b, DataSeabedImage, Carto] = read_PingAcrossSample(this, varargin)

[varargin, Carto]                 = getPropertyValue(varargin, 'Carto',                 []);
[varargin, ListeLayers]           = getPropertyValue(varargin, 'ListeLayers',           []);
[varargin, IndNavUnit]            = getPropertyValue(varargin, 'IndNavUnit',            []);
[varargin, MaskPingsReflectivity] = getPropertyValue(varargin, 'MaskPingsReflectivity', []); %#ok<ASGLU>

[nomDir, nomFic] = fileparts(this.nomFic);
nomDir = fullfile(nomDir, 'SonarScope', nomFic);
if ~exist(nomDir, 'dir')
    status = mkdir(nomDir);
    if ~status
        messageErreur(nomDir)
        %         flag = 0;
        return
    end
end
nomDirPingAcrossSample = fullfile(nomDir, 'PingAcrossSample');
unzipSonarScopeCache(nomDirPingAcrossSample)
if ~exist(nomDirPingAcrossSample, 'dir')
    status = mkdir(nomDirPingAcrossSample);
    if ~status
        messageErreur(nomDirPingAcrossSample)
        %         flag = 0;
        return
    end
end

flagExist = false(1,8);
nomFicXml = cell(1,8);
nomFicXml{1} = fullfile(nomDirPingAcrossSample, [nomFic '_PingAcrossSample_Reflectivity.xml']);
nomFicXml{2} = fullfile(nomDirPingAcrossSample, [nomFic '_PingAcrossSample_AveragedPtsNb.xml']);
nomFicXml{3} = fullfile(nomDirPingAcrossSample, [nomFic '_PingAcrossSample_TxAngle.xml']);
nomFicXml{4} = fullfile(nomDirPingAcrossSample, [nomFic '_PingAcrossSample_TxBeamIndex.xml']);
nomFicXml{5} = fullfile(nomDirPingAcrossSample, [nomFic '_PingAcrossSample_RxBeamIndex.xml']);
nomFicXml{6} = fullfile(nomDirPingAcrossSample, [nomFic '_PingAcrossSample_AlongDist.xml']);
nomFicXml{7} = fullfile(nomDirPingAcrossSample, [nomFic '_PingAcrossSample_TwoWayTravelTimeInSamples.xml']);
nomFicXml{8} = fullfile(nomDirPingAcrossSample, [nomFic '_PingAcrossSample_TxBeamIndexSwath.xml']);
for k=1:length(nomFicXml)
    flagExist(k) = exist(nomFicXml{k}, 'file');
end
if all(flagExist(1:7)) && ~flagExist(8)
    % Ancien sondeur
    ListeLayers(ListeLayers == 8) = [];
    flagExist = flagExist(1:7);
end

nomFicMask = fullfile(nomDir, 'Ssc_Depth', 'Mask.bin');
if exist(nomFicMask, 'file')
    if checkFilePosterior(nomFicMask, nomFicXml{1})
        flagExist = false;
    end
end

if all(flagExist) % Tous les fichiers sont pr�ts � l'emploi
    [flag, DataSeabedImage, SeabedImageFormat] = read_seabedImage_bin(this, 'Memmapfile', 1); %#ok<ASGLU>
    if ~flag
        return
    end
    
    nbImages = length(ListeLayers);
    if nbImages == 0
        b = cl_image.empty;
    end
    for k=1:nbImages
        [flag, I] = cl_image.import_xml(nomFicXml{ListeLayers(k)}, 'Memmapfile', 1);
        if ~flag
            return
        end
        b(k) = I;
    end
else % On doit cr�er (ou refaire) les fichiers en PingAcrossSample
    [DataSeabedImage, b, Carto, subPingDepth] = Snippets2PingAcrossSample(this, 'Carto', Carto, 'IndNavUnit', IndNavUnit);
    if isempty(b)
        flag = 0;
        return
    end
    
    for k=1:length(b)
        nomFic = b(k).Name;
        nomFic = fullfile(nomDirPingAcrossSample, [nomFic '.xml']);
        flag = export_xml(b(k), nomFic);
    end
    ListeLayers(ListeLayers > length(b)) = [];
    b = b(ListeLayers);
end

if ~isempty(MaskPingsReflectivity) && any(MaskPingsReflectivity) % TODO : attention, cela ne r�soud pas le pb de taille diff�rente fichier G:\FSK\Course\0207_20150409_085822_Nynorderoog.all
    X = NaN(size(MaskPingsReflectivity));
    X(~MaskPingsReflectivity) = 1;
    b(1) = b(1) * X(subPingDepth,:);
end
