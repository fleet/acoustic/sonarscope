% Lecture des datagrams de donn�es du Raw Data Logger dans un fichier .rdl
%
% Syntax
%   Data = read_RawDataParameters(a)
% 
% Input Arguments 
%   a : Instance de cl_simrad_all
%
% Output Arguments 
%   Data : Structure contenant les 
%
% Examples
%   nomFic = 'C:\Temp\SAT_EM122\RDL\EM122_default_survey_23125_090529_124934.rdl'
%   fid = fopen(nomFic, 'r');
%   RawDataParameters = read_RawDataParameters(cl_simrad_all([]), fid)
%   RawDataStructure  = read_RawDataStructure(cl_simrad_all([]), fid)
%   fclose(fid)
%
% See also cl_simrad_all read_RawDataStructure Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function Data = read_RawDataParameters(this, fid) %#ok<INUSL>

NumberOfBytesInDatagram = fread(fid, 1, 'int32'); %#ok
STX                     = fread(fid, 1, 'uchar'); %#ok
TypeOfDatagram          = fread(fid, 1, 'uchar'); %#ok
Data.EmModel            = fread(fid, 1, 'uint16');
Date                    = fread(fid, 1, 'uint32');
nbMilliSec              = fread(fid, 1, 'uint32');

Jour    = mod(Date, 100);
Date    = (Date - Jour) / 100;
Mois    = mod(Date, 100);
Annee   = (Date - Mois) / 100;
Date    = dayJma2Ifr(Jour, Mois, Annee);
Data.Time = cl_time('timeIfr', Date, nbMilliSec);

Data.PingCounter           = fread(fid, 1, 'uint16');
Data.SystemSerialNumber    = fread(fid, 1, 'uint16');
Data.SoundSpeedTransducer  = fread(fid, 1, 'single');     % en m/s
Data.WaterTempAtTransducer = fread(fid, 1, 'single');     % en �c
Data.WaterSalinity         = fread(fid, 1, 'single');     % en %
spare                      = fread(fid, 1, 'single'); %#ok<NASGU>
Data.RxSoftwareVersion     = fread(fid, 1, 'uint16');
Data.TxSoftwareVersion     = fread(fid, 1, 'uint16');
Data.RxOnLineBITSStatus    = fread(fid, 1, 'uint16');
Data.TxOnLineBITSStatus    = fread(fid, 1, 'uint16');
spare                      = fread(fid, 1, 'uint32'); %#ok<NASGU>
Data.PUSoftwareVersion     = fread(fid, 1, 'uint16');
Data.BspSoftwareVersion    = fread(fid, 1, 'uint16');
Data.PUOnLineBITSStatus    = fread(fid, 1, 'uint16');
spare                      = fread(fid, 1, 'uint16'); %#ok<NASGU>
Data.RxSamplingFrequency   = fread(fid, 1, 'single');   % en Hz
Data.SampleNuAtStart       = fread(fid, 1, 'uint32');
Data.NuSamplesInPing       = fread(fid, 1, 'uint32');
Data.NuStavesInArray       = fread(fid, 1, 'uint16');
Data.NuStavesPerModule     = fread(fid, 1, 'uint16');
Data.StaceSpacing          = fread(fid, 1, 'single');   % en m
Data.ExtraGapBetweenModules= fread(fid, 1, 'int32');
Data.FixedGain             = fread(fid, 1, 'int32');    % en dB
Data.RxBandwidth           = fread(fid, 1, 'single');   % en Hz
Data.RxStaveSensibility    = fread(fid, 1, 'int16');    %en dB
Data.RxStaveDirectivity    = fread(fid, 1, 'int16');    %en dB
Data.BytesPerRXStave       = fread(fid, 1, 'uint16');
Data.StartElementNumber    = fread(fid, 1, 'int16');
spare                      = fread(fid, 1, 'uint32'); %#ok<NASGU>
spare                      = fread(fid, 1, 'uint32'); %#ok<NASGU>
Data.TXMode                = fread(fid, 1, 'uint16');
Data.NTx                   = fread(fid, 1, 'uint16');
Data.AlongShipTXOffset     = fread(fid, 1, 'single');   % en m
Data.TxBeamwidthAlong      = fread(fid, 1, 'uint16');
spare                      = fread(fid, 1, 'int16'); %#ok<NASGU>
Data.RollCorrectionAngle   = fread(fid, 1, 'single');   % en rad
spare                      = fread(fid, 1, 'int32'); %#ok<NASGU>

for i=1:Data.NTx
    Data.TilAngleAlong(i)      = fread(fid, 1, 'single');
    Data.SignalLength(i)       = fread(fid, 1, 'single');   % en s
    Data.FocusRange(i)         = fread(fid, 1, 'single');   % en m
    Data.CenterFrequency(i)    = fread(fid, 1, 'single');   % en Hz
    Data.TimeDelayRef(i)       = fread(fid, 1, 'single');   % en s
    Data.Bandwidth(i)          = fread(fid, 1, 'single');   % en Hz
    Data.SL(i)                 = fread(fid, 1, 'single');   % dB
    Data.AbsorptionCoeff(i)    = fread(fid, 1, 'single');   % dB
    Data.BeamAngleAcross(i)    = fread(fid, 1, 'single');   % rad
    Data.BeamWidthAcross(i)    = fread(fid, 1, 'single');   % rad
    Data.WaveformIdentifier(i) = fread(fid, 1, 'uint16');   % rad
    spare                      = fread(fid, 1, 'int16'); %#ok<NASGU>
end
spare                    = fread(fid, 1, 'uint8'); %#ok<NASGU>
ETX                      = fread(fid, 1, 'uint8'); %#ok<NASGU>
Checksum                 = fread(fid, 1, 'uint16'); %#ok<NASGU>

