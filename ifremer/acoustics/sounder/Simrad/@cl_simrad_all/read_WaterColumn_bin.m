% nomFic = 'D:\Temp\VLIZ\Lifewatch(19-740)_22242018_EM2040d\0013_20191022_173330_SimonStevin.all';
% aKM = cl_simrad_all('nomFic', nomFic);
%
% [flag, DataWC1] = read_WaterColumn_bin(aKM, 'Memmapfile', -1)

function [flag, DataWC] = read_WaterColumn_bin(this, varargin)

[varargin, XMLOnly]    = getPropertyValue(varargin, 'XMLOnly',    0);
[varargin, Memmapfile] = getPropertyValue(varargin, 'Memmapfile', 0);
[varargin, ReadOnly]   = getPropertyValue(varargin, 'ReadOnly',   0); %#ok<ASGLU>

%% Lecture du fichier XML d�crivant la donn�e 

[flag, DataWC] = SSc_ReadDatagrams(this.nomFic, 'Ssc_WaterColumn', ...
    'XMLOnly', XMLOnly, 'Memmapfile', Memmapfile, 'ReadOnly', ReadOnly);
if ~flag || XMLOnly
    return
end

%% Tentative de correction donn�es AUV fichier 0012_20120315_114159_raw.all
% Comment� par JMA le 12/09/2017 pour EM2040 Thalia o� cette v�rue mixait
% les faisceaux b�bord et tribord

%{
if DataWC.Dimensions.nbSounders == 1
    for k=1:1:DataWC.Dimensions.nbPings
        [~, ordre] = sort(DataWC.BeamPointingAngle(k,:));
        if ~isequal(ordre, 1:DataWC.Dimensions.nbRx)
            if isa(DataWC.BeamPointingAngle, 'cl_memmapfile')
                DataWC.BeamPointingAngle      = DataWC.BeamPointingAngle(:,:);
                DataWC.StartRangeNumber       = DataWC.StartRangeNumber(:,:);
                DataWC.NumberOfSamples        = DataWC.NumberOfSamples(:,:);
                DataWC.DetectedRangeInSamples = DataWC.DetectedRangeInSamples(:,:);
                DataWC.TransmitSectorNumber   = DataWC.TransmitSectorNumber(:,:);
                DataWC.RxBeamNumber           = DataWC.RxBeamNumber(:,:);
                DataWC.OrdreBeam              = NaN(DataWC.Dimensions.nbPings, DataWC.Dimensions.nbRx, 'single');
            end
            DataWC.BeamPointingAngle(k,:)      = DataWC.BeamPointingAngle(k,ordre);
            DataWC.StartRangeNumber(k,:)       = DataWC.StartRangeNumber(k,ordre);
            DataWC.NumberOfSamples(k,:)        = DataWC.NumberOfSamples(k,ordre);
            DataWC.DetectedRangeInSamples(k,:) = DataWC.DetectedRangeInSamples(k,ordre);
            DataWC.TransmitSectorNumber(k,:)   = DataWC.TransmitSectorNumber(k,ordre);
            DataWC.RxBeamNumber(k,:)           = DataWC.RxBeamNumber(k,ordre);
            DataWC.OrdreBeam(k,:)              = ordre;
        end 
    end
end 
%}

if DataWC.Dimensions.nbTx > 1
    TransmitSectorNumber = DataWC.TransmitSectorNumber(:,:);
    for k=1:1:DataWC.Dimensions.nbPings
        [~, ind] = sort(TransmitSectorNumber(k,:));
        ordre = [ind(1):length(ind) 1:(ind(1)-1)];
        DataWC.OrdreBeam(k,:) = ordre;
%         figure;
%         subplot(3,1,1); plot(TransmitSectorNumber(k,:)); grid on; title('TransmitSectorNumber')
%         subplot(3,1,2); plot(DataWC.BeamPointingAngle(k,:)); grid on; title('BeamPointingAngle')
%         subplot(3,1,3); plot(ordre); grid on; title('ordre')
    end
end

%% Ajout FlagPings si ancien fichier

if ~isfield(DataWC, 'FlagPings')
    [nomDir, nom] = fileparts(this.nomFic);
    nomFicFlagPings = fullfile(nomDir, 'SonarScope', nom, 'Ssc_WaterColumn', 'FlagPings.bin');
    if exist(nomFicFlagPings, 'file')
        fid = fopen(nomFicFlagPings, 'r');
        DataWC.FlagPings = fread(fid, 'uint8');
        fclose(fid);
    else
        DataWC.FlagPings = zeros(DataWC.Dimensions.nbPings, DataWC.Dimensions.nbSounders, 'uint8');
    end
end
