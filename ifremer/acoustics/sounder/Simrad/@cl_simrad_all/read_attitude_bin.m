% nomFic = getNomFicDatabase('0011_20110307_211804_IKATERE_02.all');
% aKM = cl_simrad_all('nomFic', nomFic);
%
% [flag, DataAttitude1] = read_attitude_bin(aKM, 'Memmapfile', -1)

function [flag, DataAttitude, nbSensors, indSystemUsed] = read_attitude_bin(this, varargin)

[varargin, Memmapfile]              = getPropertyValue(varargin, 'Memmapfile',              0);
[varargin, DataInstalParam]         = getPropertyValue(varargin, 'DataInstalParam',         []);
% [varargin, ApplyCalibrationOffsets] = getPropertyValue(varargin, 'ApplyCalibrationOffsets', true);
[varargin, ApplyCalibrationOffsets] = getPropertyValue(varargin, 'ApplyCalibrationOffsets', false);
% [varargin, IndAttUnit]              = getPropertyValue(varargin, 'IndAttUnit',              1); %#ok<ASGLU>
[varargin, IndAttUnit]              = getPropertyValue(varargin, 'IndAttUnit',              []); %#ok<ASGLU> % Modif JMA le 18/04/2023

nbSensors     = [];
indSystemUsed = [];

% if isempty(IndAttUnit) % Comment� par  JMA le 18/04/2023
%     IndAttUnit = 1;
% end

%% Lecture du fichier XML d�crivant la donn�e 

[flag, DataAttitude] = SSc_ReadDatagrams(this.nomFic, 'Ssc_Attitude', 'Memmapfile', Memmapfile);
if ~flag
    return
end

SensorSystemDescriptor = DataAttitude.SensorSystemDescriptor(:);
listSensors = unique(SensorSystemDescriptor);
nbSensors = length(listSensors);
indSystemUsed = 1; % TODO : comment le d�terminer ?
% TODO : voir aussi all2ssc_Attitude

%% Lecture des param�tres d'installation

if isempty(DataInstalParam)
    [flag, DataInstalParam] = read_installationParameters_bin(this);
    if ~flag
        return
    end
end

%% Lecture DataInstallationParameters

if ~isfield(DataInstalParam, 'VSN')
    DataInstalParam.VSN = 1; % Cas rencontr� pour donn�es EM1002
end
switch DataInstalParam.VSN
    case 0 % Oups ! Pas de centrale d'attitude utilis�e, on fait comme si c'�tait la MRU1
        MRU.X = DataInstalParam.MSY;
        MRU.Y = DataInstalParam.MSX;
        MRU.Z = DataInstalParam.MSZ;
        MRU.RollCalibration    = DataInstalParam.MSR;
        MRU.PitchCalibration   = DataInstalParam.MSP;
        MRU.HeadingCalibration = DataInstalParam.MSG;
        MRU.TimeDelay          = DataInstalParam.MSD;
    case 1 % First Motion sensor system
        try
            MRU.X = DataInstalParam.MSY;
            MRU.Y = DataInstalParam.MSX;
            MRU.Z = DataInstalParam.MSZ;
            MRU.RollCalibration  = DataInstalParam.MSR;
            MRU.PitchCalibration = DataInstalParam.MSP;
            try
                MRU.HeadingCalibration = DataInstalParam.MSG;
            catch
                MRU.HeadingCalibration = 0; % Ajout JMA le 28/04/2023 pour fichier 0114_20221212_200247.kmall
            end
            MRU.TimeDelay          = DataInstalParam.MSD;
        catch % Cas des fichiers EM2000 des italiens de Pise
            MRU.X                  = 0;
            MRU.Y                  = 0;
            MRU.Z                  = 0;
            MRU.RollCalibration    = 0;
            MRU.PitchCalibration   = 0;
            MRU.HeadingCalibration = 0;
            MRU.TimeDelay          = 0;
        end
    case 2 % Second Motion sensor system
        MRU.X = DataInstalParam.NSY;
        MRU.Y = DataInstalParam.NSX;
        MRU.Z = DataInstalParam.NSZ;
        MRU.RollCalibration    = DataInstalParam.NSR;
        MRU.PitchCalibration   = DataInstalParam.NSP;
        MRU.HeadingCalibration = DataInstalParam.NSG;
        MRU.TimeDelay          = DataInstalParam.NSD;
    otherwise
        % str1 = sprintf('La variable "ARO=%s" du datagramme Installation parameters datagramme n''est pas interpr�table. Contactez l''assistance.', num2str(DataInstalParam.ARO));
        % str2 = sprintf('"ARO=%s" is unexpected in the Installation parameters datagram. Please report to the support.', num2str(DataInstalParam.ARO));
        % my_warndlg(Lang(str1,str2), 1);
end

if ApplyCalibrationOffsets
    %% D�calage temporel
    
    DataAttitude.Time = DataAttitude.Time + MRU.TimeDelay; % TODO : + en attendant confirmation Herv�
    
    %% Application des offsets
    
    DataAttitude.Roll    = DataAttitude.Roll(:,:)    + MRU.RollCalibration;
    DataAttitude.Pitch   = DataAttitude.Pitch(:,:)   + MRU.PitchCalibration;
    DataAttitude.Heading = DataAttitude.Heading(:,:) + MRU.HeadingCalibration;
end
DataAttitude.ApplyCalibrationOffsets = ApplyCalibrationOffsets;

DataAttitude.Dimensions.NbSamples = length(DataAttitude.Heading); % Ajout� par JMA le 06/04/2017

AttitudeDescriptor = DataAttitude.SensorSystemDescriptor(:);
ADUnique = unique(AttitudeDescriptor);
isFieldPingCounter = isfield(DataAttitude, 'PingCounter');
if length(ADUnique) == 1
    nbSensors     = 1;
    indSystemUsed = 1;
else
    if isempty(IndAttUnit) % Ajout JMA le 18/04/2023
        IndAttUnit = DataInstalParam.VSN;
    end
%     systemUsed = (AttitudeDescriptor == IndAttUnit); % Comment� par JMA le 18/04/2023
%     systemUsed = (DataAttitude.IndexSensorSystemDescriptor == IndAttUnit); % Ajout JMA le 18/04/2023
    
%     indAttitudeSystems = bitand(AttitudeDescriptor, uint8(bin2dec('11')));
%     indSystemUsed  = unique(indAttitudeSystems(systemUsed));
%     listSystemUsed = unique(indAttitudeSystems);
%     nbSensors = length(listSystemUsed);
    
%     if ~isempty(IndAttUnit) && (IndAttUnit ~= indSystemUsed) && (IndAttUnit >= 1) && (IndAttUnit <= nbSensors)
%         systemUsed = (indAttitudeSystems == listSystemUsed(IndAttUnit));
%     end
    
%     subIndexAttUnit = ([DataAttitude.SensorSystemDescriptor] == listSystemUsed(IndAttUnit));

    if isfield(DataAttitude, 'NbCycles')
        subIndexAttUnit = (DataAttitude.IndexSensorSystemDescriptor == IndAttUnit); % Ajout JMA le 18/04/2023
        indFirstSampleInCycle = int64(1);
        systemUsed = logical(size(DataAttitude.NbCycles));
        for k=1:length(DataAttitude.NbCycles)
            systemUsed(k) = subIndexAttUnit(indFirstSampleInCycle);
            indFirstSampleInCycle = indFirstSampleInCycle + int64(DataAttitude.NbCycles(k));
        end
    else
        systemUsed = (DataAttitude.IndexSensorSystemDescriptor == IndAttUnit);
        subIndexAttUnit = systemUsed;
    end
    
    if isFieldPingCounter
        % Champs absent si .kmall
        DataAttitude.PingCounter             = DataAttitude.PingCounter(systemUsed);
        DataAttitude.NbCycles                = DataAttitude.NbCycles(systemUsed);
    else
        DataAttitude.TrueHeave               = DataAttitude.TrueHeave(systemUsed);
        DataAttitude.Draught                 = DataAttitude.Draught(systemUsed);
        DataAttitude.Tide                    = DataAttitude.Tide(systemUsed);
    end
    DataAttitude.SensorSystemDescriptor      = DataAttitude.SensorSystemDescriptor(systemUsed);
    DataAttitude.Time                        = DataAttitude.Time(subIndexAttUnit);
    DataAttitude.Datetime                    = DataAttitude.Datetime(subIndexAttUnit);
    DataAttitude.SensorStatus                = DataAttitude.SensorStatus(subIndexAttUnit);
    DataAttitude.IndexSensorSystemDescriptor = DataAttitude.IndexSensorSystemDescriptor(subIndexAttUnit);

    %{
    figure;
    subplot(4,1,1); plot(DataAttitude.Roll); grid; title('DataAttitude.Roll');
    subplot(4,1,2); plot(DataAttitude.Roll(subIndexAttUnit)); grid; title('DataAttitude.Roll(subIndexAttUnit)');
    subplot(4,1,3); plot(DataAttitude.Roll(~subIndexAttUnit)); grid; title('DataAttitude.Roll(~subIndexAttUnit)');
    subplot(4,1,4); plot(DataAttitude.Roll(subIndexAttUnit)); grid;
    hold on; plot(DataAttitude.Roll(~subIndexAttUnit), 'r'); grid; title('DataAttitude.Roll MR1 and MR2 2 separated');
    %}

    DataAttitude.Roll  = DataAttitude.Roll(subIndexAttUnit);
    DataAttitude.Pitch = DataAttitude.Pitch(subIndexAttUnit);
    DataAttitude.Heave = DataAttitude.Heave(subIndexAttUnit);

    %{
    figure;
    subplot(4,1,1); plot(DataAttitude.Heading); grid; title('DataAttitude.Heading');
    subplot(4,1,2); plot(DataAttitude.Heading(subIndexAttUnit)); grid; title('DataAttitude.Heading(subIndexAttUnit)');
    subplot(4,1,3); plot(DataAttitude.Heading(~subIndexAttUnit)); grid; title('DataAttitude.Heading(~subIndexAttUnit)');
    subplot(4,1,4); plot(DataAttitude.Heading(subIndexAttUnit)); grid;
    hold on; plot(DataAttitude.Heading(~subIndexAttUnit), 'r'); grid; title('DataAttitude.Heading MR1 and MR2 2 separated');
    %}

    if isfield(DataAttitude, 'Tide')
        DataAttitude.TrueHeave = DataAttitude.TrueHeave(subIndexAttUnit);
        DataAttitude.Draught   = DataAttitude.Draught(subIndexAttUnit);
        DataAttitude.Tide      = DataAttitude.Tide(subIndexAttUnit);
    end

    DataAttitude.Heading   = DataAttitude.Heading(subIndexAttUnit);
end
