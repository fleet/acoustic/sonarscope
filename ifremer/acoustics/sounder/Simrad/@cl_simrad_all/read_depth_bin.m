% nomFic = getNomFicDatabase('0034_20070406_081642_raw.all');
% nomFic = getNomFicDatabase('0011_20110307_211804_IKATERE_02.all');
% aKM = cl_simrad_all('nomFic', nomFic);
%
% [flag, Data1] = read_depth_bin(aKM, 'Memmapfile', -1)

function [flag, DataDepth, VarNameInFailure] = read_depth_bin(this, varargin)

[varargin, nomFic]     = getPropertyValue(varargin, 'nomFic',     []);
[varargin, XMLOnly]    = getPropertyValue(varargin, 'XMLOnly',    0);
[varargin, Memmapfile] = getPropertyValue(varargin, 'Memmapfile', 0);
[varargin, ReadOnly]   = getPropertyValue(varargin, 'ReadOnly',   0); %#ok<ASGLU>

%% Lecture du fichier XML d�crivant la donn�e 

if isempty(nomFic)
    nomFic = this.nomFic; % Possibili� d'appel pour pouvoir �viter un appel � cl_simrad_all
end
    
[flag, DataDepth, VarNameInFailure] = SSc_ReadDatagrams(nomFic, 'Ssc_Depth', ...
    'XMLOnly', XMLOnly, 'Memmapfile', Memmapfile, 'ReadOnly', ReadOnly);
if ~flag
    return
end

%% Cas particulier

% TODO : Il ne faudra laisser que TwoWayTravelTimeInSamples

if isfield(DataDepth, 'Range')
    DataDepth.Range = DataDepth.Range(:,:) / 2; % Cas du format V1 (all2ssc_Depth44h)
    % Il faudrait faire le m�nage de toutes les utilisations de
    % DataDepth.Range mais c'est du boulot !!!
    % Ca permettrait de supprimer ce cas particulier et d'appeler
    % directement la fonction g�n�rale
end
