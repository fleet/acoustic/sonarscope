% nomFic = getNomFicDatabase('0011_20110307_211804_IKATERE_02.all');
% aKM = cl_simrad_all('nomFic', nomFic);
%
% [flag, DataInstallationParameters, isDual] = read_installationParameters_bin(aKM)

function [flag, DataInstallationParameters, isDual] = read_installationParameters_bin(this, varargin)

[varargin, Memmapfile] = getPropertyValue(varargin, 'Memmapfile', 0); %#ok<ASGLU>

isDual = [];

[flag, DataInstallationParameters] = SSc_ReadDatagrams(this.nomFic, 'Ssc_InstallationParameters', 'Memmapfile', Memmapfile);
if ~flag
    return
end

%% Suite

% Traitement transitoire (apr�s int�gration des travaux d'Export ALL).
% A supprimer � l'avenir. 
% WHY ? 02/02/2020
if isfield(DataInstallationParameters, 'Line')
    names = fieldnames(DataInstallationParameters.Line);
    for k=1:length(names)
        DataInstallationParameters.(names{k}) = DataInstallationParameters.Line.(names{k});
    end
    DataInstallationParameters = rmfield(DataInstallationParameters, 'Line');
end

%% D�terminanbtion de isDual

isDual = defineIsDual(DataInstallationParameters);
DataInstallationParameters.isDual = isDual;
