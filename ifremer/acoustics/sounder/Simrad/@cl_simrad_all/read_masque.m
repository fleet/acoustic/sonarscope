% Lecture du masques assici� � un un fichier .all
%
% Syntax
%   Data = read_masque(a)
% 
% Input Arguments 
%   a : Instance de cl_simrad_all
%
% Output Arguments 
%   Data : Data.MasqueDetection : masque obtenu par analyse du facteur de qualite
%          Data.Depth           : masque obtenu pa analyse de l'histogramme de la profondeur
%
% Examples
%   Data = read_masque(a);
%   figure; imagesc(Data.MasqueDetection); colorbar
%
% See also cl_simrad_all cl_simrad_all/plot_position cl_simrad_all/plot_nav Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function Data = read_masque(this)

%% Check the number of elements

flag = checkIfOneElement(length(this));
if ~flag
    Data = [];
    return
end

%% On regarde si le fichier .mat existe

[nomDir, nomFic] = fileparts(this.nomFic);
nomFicMat = fullfile(nomDir, 'SonarScope', [nomFic '_masque.mat']);
if exist(nomFicMat, 'file')
    Data = loadmat(nomFicMat, 'nomVar', 'Data');
    return
end

%% On cr�e au besoin le r�pertoire SonarScope

nomDirMat = fullfile(nomDir, 'SonarScope');
if ~exist(nomDirMat, 'dir')
    [status, msg] = mkdir(nomDirMat);
    if status
        special_message('CreationRepertoireReussie', nomDirMat)
    else
        special_message('CreationRepertoireEchec', nomDirMat)
        nomFicMat = [];
    end
end

%% Cr�ation du masque

[flag, DataDepth] = read_depth_bin(this);
if ~flag
    Data = [];
    return
end

Data.MasqueDetection = ones(size(DataDepth.Depth), 'single');
Data.MasqueDepth     = ones(size(DataDepth.Depth), 'single');

%% Sauvegarde de la donnee dans un fichier .mat

if ~isempty(nomFicMat)
    try
        save(nomFicMat, 'Data')
    catch %#ok<CTCH>
        str = sprintf('Erreur ecriture fichier %s', nomFicMat);
        my_warndlf(str, 1)
    end
end
