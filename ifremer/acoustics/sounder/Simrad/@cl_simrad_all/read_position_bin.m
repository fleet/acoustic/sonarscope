% nomFic = getNomFicDatabase('0011_20110307_211804_IKATERE_02.all');
% aKM = cl_simrad_all('nomFic', nomFic);
%
% [flag, DataPosition] = read_position_bin(aKM, 'Memmapfile', -1)

function [flag, DataPosition, nbSensors, indSystemUsed] = read_position_bin(this, varargin)

[varargin, Memmapfile] = getPropertyValue(varargin, 'Memmapfile', 0);
[varargin, LogSilence] = getPropertyValue(varargin, 'LogSilence', 1);
[varargin, IndNavUnit] = getPropertyValue(varargin, 'IndNavUnit', []);
[varargin, nomFicAll]  = getPropertyValue(varargin, 'nomFicAll',  []); %#ok<ASGLU>

nbSensors     = [];
indSystemUsed = [];

if isempty(nomFicAll)
    nomFicAll = this.nomFic;
end

if isempty(IndNavUnit)
    IndNavUnit = 1;
end

%% Lecture du fichier XML d�crivant la donn�e 

[flag, DataPosition] = SSc_ReadDatagrams(nomFicAll, 'Ssc_Position', 'Memmapfile', Memmapfile, 'LogSilence', LogSilence);
if ~flag
    return
end

%% Extraction de la nav active : Ajout JMA le 16/10/2018 pour fichier EM120 Allemand (Provenance Delphine)

isFieldQuality = isfield(DataPosition, 'Quality');

PositionDescriptor = DataPosition.PositionDescriptor(:);
PDUnique = unique(PositionDescriptor);
if length(PDUnique) == 1
    nbSensors     = 1;
    indSystemUsed = 1;
else
    systemUsed = (PositionDescriptor == PDUnique(IndNavUnit)); % (bitget(PositionDescriptor,8) == 1);
    
    indPositionSystems = bitand(PositionDescriptor, uint8(bin2dec('11')));
    indSystemUsed  = unique(indPositionSystems(systemUsed));
    listSystemUsed = unique(indPositionSystems);
    nbSensors = length(listSystemUsed);
    
    if ~isempty(IndNavUnit) && (IndNavUnit ~= indSystemUsed) && (IndNavUnit >= 1) && (IndNavUnit <= nbSensors)
        systemUsed = (indPositionSystems == listSystemUsed(IndNavUnit));
    end
    
    DataPosition.PingCounter                    = DataPosition.PingCounter(systemUsed);
    DataPosition.Latitude                       = DataPosition.Latitude(systemUsed);
    DataPosition.Longitude                      = DataPosition.Longitude(systemUsed);
    if isFieldQuality
        DataPosition.Quality                        = DataPosition.Quality(systemUsed);
    end
    DataPosition.Speed                          = DataPosition.Speed(systemUsed);
    DataPosition.CourseVessel                   = DataPosition.CourseVessel(systemUsed);
    DataPosition.Heading                        = DataPosition.Heading(systemUsed);
    DataPosition.QualityIndicator               = DataPosition.QualityIndicator(systemUsed);
    DataPosition.NumberSatellitesUsed           = DataPosition.NumberSatellitesUsed(systemUsed);
    DataPosition.HorizontalDilutionOfPrecision  = DataPosition.HorizontalDilutionOfPrecision(systemUsed);
    DataPosition.AltitudeOfIMU                  = DataPosition.AltitudeOfIMU(systemUsed);
    DataPosition.GeoidalSeparation              = DataPosition.GeoidalSeparation(systemUsed);
    DataPosition.AgeOfDifferentialCorrections   = DataPosition.AgeOfDifferentialCorrections(systemUsed);
    DataPosition.PositionDescriptor             = DataPosition.PositionDescriptor(systemUsed);
    
    T = DataPosition.Time.timeMat;
    DataPosition.Time = cl_time('timeMat', T(systemUsed));
%     DataPosition.Datetime = datetime(T(systemUsed), 'ConvertFrom', 'datenum');
    DataPosition.Datetime = DataPosition.Datetime(systemUsed);
    
    T = DataPosition.GPSTime.timeMat;
    DataPosition.GPSTime = cl_time('timeMat', T(systemUsed));
%     DataPosition.GPSDatetime = datetime(T(systemUsed), 'ConvertFrom', 'datenum');
    DataPosition.GPSDatetime = DataPosition.GPSDatetime(systemUsed);
    
    try
        DataPosition.DGPSRefStationIdentity = DataPosition.DGPSRefStationIdentity(systemUsed); % Ajout JMA le 02/02/2020
    catch
    end

    DataPosition.Dimensions.NbSamples = length(DataPosition.PingCounter);
end
