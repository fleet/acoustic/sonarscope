% Lecture des datagrams "Raw range and beam angle" dans un fichier .all
%
% Syntax
%   [flag, DataRawRange] = read_rawRangeBeamAngle(aKM)
% 
% Input Arguments 
%   aKM : Instance de cl_simrad_all
%
% Output Arguments 
%   DataRawRange : Structure contenant les 
%       Time               : Heure GMT du ping
%       PingCounter        : Numero du ping
%       SystemSerialNumber : Numero de serie du sondeur
%       NbBeams            : Nombre faisceaux
%       SoundSpeed         : Vitesse du son au niveau dus antennes (m/s)
%       BeamPointingAngle  : Angle de pointage (deg)
%       TransmitTiltAngle  : 
%       Range              : Temps aller simple (samples)
%       Reflectivity       : Reflectivite (dB)
%
% Examples
%   nomFic = getNomFicDatabase('EM1002_0001_20040916_054013_raw.all')
%   aKM = cl_simrad_all('nomFic', nomFic);
%   [flag, DataRawRange] = read_rawRangeBeamAngle(aKM)
%
%   figure; imagesc(DataRawRange.BeamPointingAngle); colorbar
%   figure; imagesc(DataRawRange.TransmitTiltAngle); colorbar
%   figure; imagesc(DataRawRange.Range); axis xy; colorbar
%   figure; imagesc(DataRawRange.Reflectivity); axis xy; colorbar
%
%   b = view_depth(aKM);
%   b = editobj(b);
%
% See also cl_simrad_all cl_simrad_all/plot_position cl_simrad_all/view_depth cl_simrad_all/plot_nav Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [flag, DataRawRange] = read_rawRangeBeamAngle(this, varargin)

[varargin, XMLOnly]    = getPropertyValue(varargin, 'XMLOnly',    0);
[varargin, Memmapfile] = getPropertyValue(varargin, 'Memmapfile', 0);
[varargin, ReadOnly]   = getPropertyValue(varargin, 'ReadOnly',   0); %#ok<ASGLU>

[flag, DataRawRange] = SSc_ReadDatagrams(this.nomFic, 'Ssc_RawRangeBeamAngle', ...
    'XMLOnly', XMLOnly, 'Memmapfile', Memmapfile, 'ReadOnly', ReadOnly);
if ~flag
    return
end

% Traitement transitoire (apr�s int�gration des travaux d'Export ALL).
% A supprimer � l'avenir. 
DataRawRange = renameField(DataRawRange, 'TransmitSectorNumberRx', 'TransmitSectorNumber');
DataRawRange = renameField(DataRawRange, 'TwoWayTravelTime',       'TwoWayTravelTimeInSeconds');

%% D�termination du ping sequence

if isfield(DataRawRange, 'CentralFrequency')
    Freq = DataRawRange.CentralFrequency(:,1);
    listFreq = unique(Freq);
    if length(listFreq) == 1
        PingSequence = ones(DataRawRange.Dimensions.nbPings, 1 ,'single');
    else
%         if length(listFreq) == 2 % Dans le cas o� on a deux seules fr�quences
%             PingSequence = NaN(DataRawRange.Dimensions.nbPings, 1 ,'single');
%             sub = (Freq == min(Freq));
%             PingSequence(sub)  = 1;
%             PingSequence(~sub) = 2;
%         else
            PingSequence = 1 + (Freq(1:end-1) > Freq(2:end));
            if Freq(end) > Freq(end-1)
                PingSequence(end+1) = 2;
            else
                PingSequence(end+1) = 1;
            end
%         end
        
        % L'identification des pings avant et arri�re en mode double swath est incertaine.
        % La diff�rence de temps pourrait �tre employ�e pour rendre cette d�tection plus robute.
        % Il faudrait aussi analyser les along distances pour renforcer encore la proc�dure
        % La proc�dure actuelle ne marche pas si il y a des pings arri�res cons�cutifs qui manquent.
        % De plus, on est pas s�r que la fr�quence inf�rieure correspond au ping arri�re !
        %   difTime = diff(DataRawRange.Time.timeMat);
        %   figure; plot(difTime, '.'); grid on
    end
else
    PingSequence = ones(DataRawRange.Dimensions.nbPings, 1, 'single');
end
DataRawRange.PingSequence = PingSequence;
