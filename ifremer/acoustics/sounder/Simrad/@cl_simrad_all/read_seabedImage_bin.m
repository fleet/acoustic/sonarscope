function [flag, DataSeabed, SeabedImageFormat] = read_seabedImage_bin(this, varargin)

SeabedImageFormat = [];

[flag, DataSeabed] = SSc_ReadDatagrams(this.nomFic, 'Ssc_SeabedImage53h', varargin{:});
if flag
    SeabedImageFormat = 1;
else
    [flag, DataSeabed] = SSc_ReadDatagrams(this.nomFic, 'Ssc_SeabedImage59h', varargin{:});
    if ~flag
        return
    end
    SeabedImageFormat = 2;
end
