function renameBadFile(this, Type, varargin)

switch Type
    case 'ALL'
        renameBadFile(this.nomFic, varargin{:})
    case 'WC'
        renameBadFile(this.nomFicWC, varargin{:})
        
        [nomDir, nom] = fileparts(this.nomFic);
        nomDir = fullfile(nomDir, 'SonarScope', nom);
        if exist(nomDir, 'dir')
            nomFicXml = fullfile(nomDir, 'Ssc_WaterColumn.xml');
            renameBadFile(nomFicXml, varargin{:})
        end
end
