function flag = resetMaskNetcdf(this)

%% Test d'existence du fichier Netcdf

[nomDir, nomFic] = fileparts(this.nomFic);
nomFicNc = fullfile(nomDir, 'SonarScope', [nomFic '.nc']);
flag = exist(nomFicNc, 'file');
if ~flag
    return
end

%% Remise � z�ro du Mask

ncID  = netcdf.open(nomFicNc, 'WRITE');
grpID = netcdf.inqNcid(ncID, 'Depth');

varID = netcdf.inqVarID(grpID, 'Mask');
Mask = netcdf.getVar(grpID, varID);
Mask(:,:) = 0;
NetcdfUtils.putVarVal(grpID, varID, Mask, 1)

varID = netcdf.inqVarID(grpID, 'MaskPingsBathy');
Mask = netcdf.getVar(grpID, varID);
Mask(:) = 0;
NetcdfUtils.putVarVal(grpID, varID, Mask, 1)

netcdf.close(ncID);
