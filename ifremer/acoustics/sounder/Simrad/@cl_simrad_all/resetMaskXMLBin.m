function flag = resetMaskXMLBin(this)

%% Lecture de l'image

[flag, DataDepth] = read_depth_bin(this, 'Memmapfile', 1);
if ~flag
    return
end

%% Lecture du r�pertoire SonarScope

[nomDir, nom] = fileparts(this.nomFic);
nomDir = fullfile(nomDir, 'SonarScope', nom);
if ~exist(nomDir, 'dir')
    return
end

%% Lecture du fichier XML d�crivant la donn�e

nomFicXml = fullfile(nomDir, 'Ssc_Depth.xml');
flag = exist(nomFicXml, 'file');
if ~flag
    return
end

%% Lecture du r�pertoire Depth

nomDirDepth = fullfile(nomDir, 'Ssc_Depth');
if ~exist(nomDirDepth, 'dir')
    flag = 0;
    return
end
k = find(strcmp({DataDepth.Images.Name}, 'Mask'));

%% Ecriture des fichiers binaires des images

Image = DataDepth.(DataDepth.Images(k).Name);
Image = Image(:,:);
Image(:,:) = 0;
Dimensions  = DataDepth.Dimensions;
structImage = DataDepth.Images(k);
clear DataDepth
flag = writeSscSignal(Image(:,:), nomDir, structImage, Dimensions);
if ~flag
    return
end
