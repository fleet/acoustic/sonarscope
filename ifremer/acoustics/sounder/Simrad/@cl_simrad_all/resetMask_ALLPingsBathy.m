function resetMask_ALLPingsBathy(this, nomFic, varargin) %#ok<INUSL>

if ~iscell(nomFic)
    nomFic = {nomFic};
end

str1 = 'Remise � z�ro des masks de bathym�trie des fichiers .all en cours.';
str2 = 'Reset Bathymetry masks from .all files';
N = length(nomFic);
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
    my_waitbar(k, N, hw);
    flag = resetMask_ALLPingsBathy_unitaire(nomFic{k});
    if ~flag
        return
    end
end
my_close(hw, 'MsgEnd')


function flag = resetMask_ALLPingsBathy_unitaire(nomFic)

%% Lecture de l'image

this = cl_simrad_all('nomFic', nomFic);
[flag, DataDepth] = read_depth_bin(this, 'Memmapfile', 0);
if ~flag
    return
end

%% Nettoyage de la r�flectivit�

if isfield(DataDepth, 'MaskPingsBathy') && ~isempty(DataDepth.MaskPingsBathy)
    nbPings = DataDepth.Dimensions.nbPings;
    clear DataDepth
    flag = save_signal(this, 'Ssc_Depth', 'MaskPingsBathy', zeros(nbPings, 1, 'single'));
    if ~flag
        return
    end
else
    if all(DataDepth.MaskPingsBathy(:) ~= 0)
        str1 = 'D�sol� ce fichier a �t� import� avant que les flags de bathym�trie par pings aient �t� introduits dans SonarScope, le nettoyage que vous avez fait est irr�versible. Il ne vous reste plus qu''� supprimer le r�pertoire correspondant � ce fichier dans le r�pertoire SonarScope si vous voulez vraiment annuler le masque que vous avez fait.';
        str2 = 'Sorry, this file has been imported before the introduction of masks on pings for bathymetry, it is impossible to restore the reflectivity. If you really want to restore the original reflectivity you have to delete the sorresponding directory to thr file in the SonarScope directory.';
        str1 = sprintf('%s\n\nNom du fichier : %s', str1, this.nomFic);
        str2 = sprintf('%s\n\nFile name : %s', str2, this.nomFic);
        my_warndlg(Lang(str1,str2), 1);
    end
end
