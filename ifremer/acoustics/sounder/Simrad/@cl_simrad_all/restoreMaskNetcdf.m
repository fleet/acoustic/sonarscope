function flag = restoreMaskNetcdf(this)

%% Test d'existence du fichier Netcdf

[nomDir, nomFic] = fileparts(this.nomFic);
nomFicNc = fullfile(nomDir, 'SonarScope', [nomFic '.nc']);
flag = exist(nomFicNc, 'file');
if ~flag
    return
end

%% Transfert MaskBackup => Mask

ncID  = netcdf.open(nomFicNc, 'WRITE');
grpID = netcdf.inqNcid(ncID, 'Depth');
varID = netcdf.inqVarID(grpID, 'MaskBackup');
Mask = netcdf.getVar(grpID, varID);
varID = netcdf.inqVarID(grpID, 'Mask');
NetcdfUtils.putVarVal(grpID, varID, Mask, 1)
netcdf.close(ncID);
