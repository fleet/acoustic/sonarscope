function flag = restoreMaskXMLBin(this)

%% Lecture de l'image

[flag, DataDepth] = read_depth_bin(this, 'Memmapfile', 1);
if ~flag
    return
end

%% Sauvegarde de la version pr�c�dente

k = strcmp({DataDepth.Images.Name}, 'Mask');
[nomDir, nomFic] = fileparts(this.nomFic);
nomFicMask = fullfile(nomDir, 'SonarScope', nomFic, DataDepth.Images(k).FileName);
if ~exist(nomFicMask,'file')
    flag = 0;
    return
end
clear DataDepth

[nomDirMask, nomFicMaskSave] = fileparts(nomFicMask);
nomFicMaskSave = fullfile(nomDirMask, [nomFicMaskSave '_Save.bin']);
flag = copyfile(nomFicMaskSave, nomFicMask);
if flag
    str1 = sprintf('Le fichier backup� "%s" a �t� r�tabli.', nomFicMask);
    str2 = sprintf('The backup version "%s" has been restored.', nomFicMask);
    my_warndlg(Lang(str1,str2), 0, 'Tag', 'SauvegardeFichierMasque');
else
    str1 = sprintf('Le fichier "%s" n''a pas pu �tre restaur�.', nomFicMask);
    str2 = sprintf('File "%s" could not be restored.', nomFicMask);
    my_warndlg(Lang(str1,str2), 0, 'Tag', 'SauvegardeFichierMasque');
end
