% Saucissonnage d'un fichier .all
%
% Syntax
%   Data = saucissonne_bin(aKM, ...)
%
% Input Arguments
%   aKM : Instance de cl_simrad_all
%
% Name-only Arguments
%   NbPings  : [] ou Nombre max de pings
%   tCoupure : [] ou heures des coupures
%
% Output Arguments
%   Data : Structure contenant Depth, AcrossDist, AlongDist, AlongDist,
%          BeamAzimuthAngle, Range, QualityFactor, LengthDetection et Reflectivity
%
% Examples
%   nomFic = getNomFicDatabase('EM1002_0001_20040916_054013_raw.all')
%   aKM = cl_simrad_all('nomFic', nomFic);
%   [flag, DataDepth] = read_depth_bin(aKM)
%   DataDepth.Time
%
%   t1 = cl_time('timeIfr', dayStr2Ifr('16/09/2004'), hourStr2Ifr('05:59:28'))
%   t2 = cl_time('timeIfr', dayStr2Ifr('16/09/2004'), hourStr2Ifr('06:10:00'))
%   t3 = cl_time('timeIfr', dayStr2Ifr('16/09/2004'), hourStr2Ifr('05:50:00'))
%   t4 = cl_time('timeIfr', dayStr2Ifr('16/09/2004'), hourStr2Ifr('00:50:00'))
%   t5 = cl_time('timeIfr', dayStr2Ifr('16/09/2004'), hourStr2Ifr('23:50:00'))
%   saucissonne_bin(aKM, [t1.timeMat t2.timeMat t3.timeMat t4.timeMat t5.timeMat]);
%
% See also cl_simrad_all Authors
% Authors : GLU
% ----------------------------------------------------------------------------

function nomFicOut = saucissonne_bin(this, NbPings, tCoupure, subDatagram, varargin)

[varargin, TimeOnly] = getPropertyValue(varargin, 'TimeOnly', 0); %#ok<ASGLU>

nomFicOut = {};

%% Check the number of elements

flag = checkIfOneElement(length(this));
if ~flag
    return
end

%% Cr�ation des fichiers unitaire de type ALL
if isempty(NbPings) && isempty(tCoupure)
    accessMode = 'Suppress';
else
    accessMode = 'Split';
end

nomFicAllOut = saucissonne_bin_unitaire(this, NbPings, tCoupure, subDatagram, accessMode, TimeOnly);
[nomDir, nomFic, ~] = fileparts(this.nomFic);
nomFicWcd = fullfile(nomDir, [nomFic, '.wcd']);
if exist(nomFicWcd, 'file')
    wcd = cl_simrad_all('nomFic',  nomFicWcd);
    % Cr�ation des fichiers unitaire de type WCD.
    subDatagram  = [subDatagram; {0, 107, 'Water Column', true}];
    nomFicWcdOut = saucissonne_bin_unitaire(wcd, NbPings, tCoupure, subDatagram, accessMode, TimeOnly);
end

%% Lecture des nouveaux fichiers ALL (et WCD en automatique)

k2 = 0;
for k1=1:length(nomFicAllOut)
    X = cl_simrad_all('nomFic', nomFicAllOut{k1});
    if isempty(X)
        continue
    end
    k2 = k2 + 1;
    aKM(k2) = X; %#ok<NASGU,AGROW>
end

% plot_position(aKM)

%% Renommage du fichier .all et .wcd si le fichier a bien �t� impact�

if ~isempty(nomFicAllOut)
    if strcmp(accessMode, 'Suppress')
        nomFicNew = fullfile(nomDir, [nomFic '.allReduced']);
    else
        nomFicNew = fullfile(nomDir, [nomFic '.allSplitted']);
    end
    
    [status, message] = movefile(this.nomFic, nomFicNew, 'f');
    
    if status
%         strFR = sprintf('Le fichier "%s" a �t� renomm� en "%s"', this.nomFic, nomFicNew);
%         strUS = sprintf('File "%s" was renamed as "%s"', this.nomFic, nomFicNew);
%         my_warndlg(Lang(strFR,strUS), 0, 'Tag', 'ImpossibleDeRenommerCeFichier');
    else
        strFR = sprintf('Echec de renommage de "%s" en "%s", faites le � la main : \nMessage matlab : %s', this.nomFic, nomFicNew, message);
        strUS = sprintf('It was impossible to rename "%s" in "%s", do it manualy : \nMatlab message : %s', this.nomFic, nomFicNew, message);
        my_warndlg(Lang(strFR,strUS), 0, 'Tag', 'ImpossibleDeRenommerCeFichier');
    end
end

if exist(nomFicWcd, 'file')
    if ~isempty(nomFicWcdOut)
        if isempty(NbPings) && isempty(tCoupure)
            nomFicNew = fullfile(nomDir, [nomFic '.wcdReduced']);
        else
            nomFicNew = fullfile(nomDir, [nomFic '.wcdSplitted']);
        end
        [status, message] = movefile(nomFicWcd, nomFicNew, 'f');
        
        if status
%             strFR = sprintf('Le fichier "%s" a �t� renomm� en "%s"', this.nomFic, nomFicNew);
%             strUS = sprintf('File "%s" was renamed as "%s"', this.nomFic, nomFicNew);
%             my_warndlg(Lang(strFR,strUS), 0, 'Tag', 'ImpossibleDeRenommerCeFichier');
        else
            strFR = sprintf('Echec de renommage de "%s" en "%s", faites le � la main : \nMessage matlab : %s', this.nomFic, nomFicNew, message);
            strUS = sprintf('It was impossible to rename "%s" in "%s", do it manualy : \nMatlab message : %s', this.nomFic, nomFicNew, message);
            my_warndlg(Lang(strFR,strUS), 0, 'Tag', 'ImpossibleDeRenommerCeFichier');
        end
    end
end


function nomFicOut = saucissonne_bin_unitaire(this, NbPings, tCoupure, subDatagram, accessMode, TimeOnly)

nomFicOut = {};

if isa(tCoupure, 'cl_time')
    tCoupure = tCoupure.timeMat;
end

[nomDir, nomFic, ext] = fileparts(this.nomFic);
if strcmp(ext, '.wcd')
    typeFile = 'WCD';
else
    typeFile = 'ALL';
end

%% Check the number of elements

flag = checkIfOneElement(length(this));
if ~flag
    return
end

%% Lecture de la bathy (pour r�cup�rer le temps)

[flag, DataDepth] = read_depth_bin(this, [], 'Memmapfile', 1);
if ~flag
    return
end
tDepth = DataDepth.Time.timeMat;
% clear DataDepth

%% Lecture du fichier d'index

[flag, Data] = read_FileIndex_bin(this);
if ~flag
    return
end

Position       = Data.DatagramPosition;
Taille         = double(Data.DatagramLength);
PingCounter    = Data.PingCounter;
TypeOfDatagram = Data.TypeOfDatagram;
try
    t = Data.Time.timeMat; % Ok pour XML-Bin
catch
    t = Data.Time; % Ok pour netcdf
end
% t(t < tDepth(1))   = tDepth(1);
% t(t > tDepth(end)) = tDepth(end);

% On d�coupe sur la base des datagrammes de Bathy ou de WCD.
if ~isempty(NbPings)
    if strcmp(typeFile, 'WCD')
        subDepth = (TypeOfDatagram == 107);
    else % .ALL de base
        subDepth = (TypeOfDatagram == 68) | (TypeOfDatagram == 88);
    end
    T = unique(t(subDepth,1));
    tCoupure = T(NbPings:NbPings:end);
end

%{
T0 = t(1);
figure; plot(t-T0, '-*'); grid on; hold on;
for k=1:length(tCoupure)
plot([1 length(t)], [tCoupure(k) tCoupure(k)]-T0, 'k')
end
%}

% if isempty(NbPings) % Cas d'un fichier de d�coupe par heure
% On supprime les points de d�coupe qui sont en dehors du fichier
sub = (tCoupure < min(t(:,1)));
tCoupure(sub) = [];
sub = (tCoupure >= max(t(:,1)));
tCoupure(sub) = [];

%{
figure; plot(t-T0, '-*'); grid on; hold on;
for k=1:length(tCoupure)
plot([1 length(t)], [tCoupure(k) tCoupure(k)]-T0, '-*k')
end
plot([1 length(t)], [tDepth(1) tDepth(1)]-T0, '-*r')
plot([1 length(t)], [tDepth(end) tDepth(end)]-T0, '-*r')
%}

sub = (tCoupure < min(tDepth(:,1)));
tCoupure(sub) = [];
sub = (tCoupure >= max(tDepth(:,1)));
tCoupure(sub) = [];

%{
T0 = tDepth(1);
figure; plot(tDepth-T0, '-*'); grid on; hold on;
for k=1:length(tCoupure)
plot([1 length(tDepth)], [tCoupure(k) tCoupure(k)]-T0, 'k')
end
hold off
%}

if TimeOnly && isempty(tCoupure)
    return
end

sub          = [subDatagram{:,4}];
selectedCode = [subDatagram{sub,2}];
if isempty(selectedCode)
    strFR = sprintf('Aucun datagramme de s�lectionn� : pas de cr�ation de fichiers splitt�s.');
    strUS = sprintf('None datagram selected : no split files creation.');
    my_warndlg(Lang(strFR,strUS), 0, 'Tag', 'NoDatagramSelected');
    return
end

nbCoupures = length(tCoupure);
tCoupure   = sort(tCoupure);
index      = ones(size(t,1),1) * (nbCoupures+1);
% index = ones(size(PingCounter,1),1) * (nbCoupures+1); % Modif JMA le 20/03/2014

for k=length(tCoupure):-1:1
    sub = (t(:,1) <= tCoupure(k));
    index(sub) = k;
end
%{
figure; plot(index, '*'); grid on
figure; plot(t, '-*'); grid on; hold on;
for k=1:length(tCoupure)
plot([1 length(t)], [tCoupure(k) tCoupure(k)], 'k')
end
%}

%% V�rification pour que des pings d'un sondeur dual ne se retrouvent pas s�par�s dans 2 fichiers s�par�s

% listeDatagramsMBES = [68 88 75 70 102 78 83 89 107 113];
% listeDatagramsMBES = [68 70 75 78 79 83 88 89 102 107 109 113]; % Modifi� le 15/05/2012 � bord du Falkor
% Modif par GLU pour traiter les derniers datagrammes trait�s : le 04/06/2012
listeDatagramsMBES = [49 65 67 68 70 71 72 73 74 78 80 82 83 85 88 89 102 104 107 109 110 112 114];

for k=1:length(listeDatagramsMBES)
    sub           = find((TypeOfDatagram == listeDatagramsMBES(k)));
    PC            = PingCounter(sub);
    PCIndex       = index(sub);
    subChangement = find(diff(PCIndex) == 1);
    for k2=1:length(subChangement)
        if length(unique(PC(subChangement(k2):(subChangement(k2)+1)))) == 1
            PCIndex(subChangement(k2):(subChangement(k2)+1)) = PCIndex(subChangement(k2)+1);
        end
    end
    index(sub) = PCIndex;
end

% plot_position(this)

%% D�codage du fichier de donn�es

typeIndian = getIndianAll(this.nomFic);
[fidIn, message] = fopen(this.nomFic, 'r', typeIndian);
if fidIn < 0
    my_warndlg(message, 1);
    return
end

for k=1:max(index)
    if isempty(tCoupure)
        if strcmp(accessMode, 'Suppress')
            % Simple filtrage des datagrammes.
            nomFicOut{k} = fullfile(nomDir, [nomFic '_reduced' ext]); %#ok<AGROW>
        else
            % On sort car on n'a plus rien � faire avec ce fichier qui sera
            % tout de m�me renomm� en Splitted.
            fclose(fidIn);
            return
        end
    else
        % Splittage des fichiers.
        nomFicOut{k} = fullfile(nomDir, [nomFic '_' num2str(k,'%02d') ext]); %#ok<AGROW>
    end
    fidOut(k) = fopen(nomFicOut{k}, 'w+'); %#ok<AGROW>
    [nomDir2, nomFic2] = fileparts(nomFicOut{k});
    nomsSsc = fullfile(nomDir2, 'SonarScope', nomFic2);
    if exist(nomsSsc, 'dir')
        try
            if ~strcmp(typeFile, 'WCD')
                rmdir(nomsSsc, 's')
                if exist(nomsSsc, 'dir')
                    strFR = sprintf('"%s" n''a pas pu �tre supprim�.', nomsSsc);
                    strUS = sprintf('"%s" could not be suppressed.', nomsSsc);
                    my_warndlg(Lang(strFR,strUS), 0);
                    flag = 0;
                end
            end
        catch %#ok<CTCH>
            strFR = sprintf('Le r�pertoire"%s" n''a pas pu �tre supprim�, on arr�te tout.', nomsSsc);
            strUS = sprintf('Folder "%s" could not be suppressed. Nothing will be done before, please solve the problem before.', nomsSsc);
            my_warndlg(Lang(strFR,strUS), 0);
            flag = 0;
        end
    end
end
if ~flag
    return
end

nomFicAuDela = fullfile(nomDir, [nomFic '_' num2str(k+1,'%02d') ext]);
if exist(nomFicAuDela, 'file')
    strFR = sprintf('ATTENTION : des fichiers existent sur le r�pertoire avec des num�ros supp�rieurs � ceux qui vont �tre g�n�r�s. Cela risque de semer la confusion lors des traitements ult�rieurs. Je vous conseille de les supprimer � partir du fichier "%s".', nomFicAuDela);
    strUS = sprintf('WARNING : some files exist with numbers supeperior to the ones you are going to create. I recommand that you supress them beginning from "%s"', nomFicAuDela);
    my_warndlg(Lang(strFR,strUS), 0);
end

%% Traitement du fichier

% PU Status	:               49
% ExtraParameters	:       51
% Attitude	:               65
% Clock	:                   67
% DepthV1	:               68
% Raw Range Beam Angles	:	70
% Surface Sound Speed	:	71
% Heading	:               72
% Mecha Transducer Tilt	:	74
% Raw Range Beam Angles (new version)	:	78
% Position	:               80
% Runtime	:               82
% Seabed Image Data	:       83
% SoundSpeedProfile	:       85
% XYZ88	:                   88
% Seabed Image Data 89	:	89
% Raw Range Beam Angles (since 2004)	:	102
% Height	:               104
% Water Column	:           107
% Stave Data	:           109
% Network Attitude	:       110
% Quality Factor	:       112
% Raw Beam Samples	:       114
% InstallParams	:           73 (Start) ou 105 (Stop)ou 112
% TODO: identifier les num�ros et choisir l'action
% 102 78 113 72 69 84 71 87 112 74 48 49 66

str = sprintf('Loading file %s', nomFic);
N   = length(Position);
hw  = create_waitbar(str, 'N', N);
for k=1:N
    my_waitbar(k, N, hw);
    % On ne sauvegarde que les datagrammes s�lectionn�s.
    sub = find(selectedCode == TypeOfDatagram(k),1);
    if ~isempty(sub)
        switch TypeOfDatagram(k)
            
            % Depth, Raw range and beam angle, SeabedImage, Central beams
            % echogram, quality factor,
            case {49 68 70 75 78 79 83 88 89 102 104 107 109 113 114 }
                fseek(fidIn, Position(k), 'bof');
                X = fread(fidIn, Taille(k), 'uchar');
                fwrite(fidOut(index(k)), X, 'uchar');
                
                % Attitude, Clock, Position : TODO : il faudrait peut-�tre recopier un de chaque avant et apr�s  : passage par des subAttitude{k} qui serait obtenu par un subAtt(1)-1:subAtt(end)+1
            case {65 67 80 110}
                fseek(fidIn, Position(k), 'bof');
                X = fread(fidIn, Taille(k), 'uchar');
                fwrite(fidOut(index(k)), X, 'uchar');
                
                % Installation Parameter start, Runtime, Sound speed profile, Installation Parameter stop
            case {51 66 69 71 72 73 74 48 82 84 85 87 105 112}
                fseek(fidIn, Position(k), 'bof');
                X = fread(fidIn, Taille(k), 'uchar');
                for k2=1:max(index)
                    fwrite(fidOut(k2), X, 'uchar');
                end
                
                %
            otherwise
                strFR = sprintf('TypeOfDatagram "%d" non identifi� dans "saucissonne_bin", signalez ce message � sonarscope@ifremer.fr', TypeOfDatagram(k));
                strUS =  sprintf('TypeOfDatagram "%d" not identified in "saucissonne_bin", please report to sonarscope@ifremer.fr', TypeOfDatagram(k));
                my_warndlg(Lang(strFR,strUS), 0, 'Tag', 'DatagrammeNonIdentifie');
                fseek(fidIn, Position(k), 'bof');
                X = fread(fidIn, Taille(k), 'uchar');
                for k2=1:max(index)
                    fwrite(fidOut(k2), X, 'uchar');
                end
        end
    end
end
my_close(hw)
fclose(fidIn);
for k=1:max(index)
    fclose(fidOut(k));
end
