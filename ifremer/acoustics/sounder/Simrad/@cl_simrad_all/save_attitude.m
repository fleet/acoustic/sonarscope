function flag = save_attitude(this, NomSignal, SignalTime, SignalValue, varargin)

global logFileId %#ok<GVMIS>

[varargin, Mute]      = getPropertyValue(varargin, 'Mute',      0);
[varargin, logFileId] = getPropertyValue(varargin, 'logFileId', logFileId); %#ok<ASGLU>

[flag, DataAttitude] = read_attitude_bin(this);
if ~flag
    return
end

if isa(SignalTime, 'datetime')
    AttitudeTime = DataAttitude.Datetime;
else
    AttitudeTime = DataAttitude.Time;
end

switch NomSignal
    case 'Tide'
        if ~isempty(SignalValue)
            X = my_interp1(SignalTime, SignalValue, AttitudeTime);
            subNaN = isnan(X);
            DataAttitude.Tide(subNaN)  = NaN;
            DataAttitude.Tide(~subNaN) = X(~subNaN);
        end
    case 'Draught'
        if ~isempty(SignalValue)
            X = my_interp1(SignalTime, SignalValue, AttitudeTime);
            subNaN = isnan(X);
            DataAttitude.Draught(subNaN)  = NaN;
            DataAttitude.Draught(~subNaN) = X(~subNaN);
        end
    case 'TrueHeave'
        if ~isempty(SignalValue)
            X = my_interp1(SignalTime, SignalValue, AttitudeTime);
            subNaN = isnan(X);
            DataAttitude.TrueHeave(subNaN)  = NaN;
            DataAttitude.TrueHeave(~subNaN) = X(~subNaN);
        end
    otherwise
        X = my_interp1(SignalTime, SignalValue, AttitudeTime);
        subNaN = isnan(X);
        DataAttitude.(NomSignal)(subNaN)  = NaN;
        DataAttitude.(NomSignal)(~subNaN) = X(~subNaN);
        %         str1 = sprintf('"%s" pas encore pr�vu dans cl_simrad/save_attitude', NomSignal);
        %         str2 = sprintf('"%s" not yet coded in cl_simrad/save_attitude', NomSignal);
        %         my_warndlg(Lang(str1,str2), 1);
end

if ~Mute
    messageIfNaN(~subNaN, NomSignal, this.nomFic)
end

flag = existCacheNetcdf(this.nomFic);
if flag
    flag = write_attitude_Netcdf(this, DataAttitude);
else
    flag = write_attitude_bin(this, DataAttitude);
end


function messageIfNaN(subNonNaN, NomSignal, nomFic)

n = length(subNonNaN);
s = sum(subNonNaN);
if s == 0
    str = sprintf('\n\t%s\n\t--> 0 "%s" sample impacted !!!!!!!!!!!!\n', nomFic, NomSignal);
else
    str = sprintf('\n\t%s\n\t--> %d / %d "%s" samples impacted\n', nomFic, s, n, NomSignal);
end

logFileId = getLogFileId;
logFileId.info('AllTideImport', str);
