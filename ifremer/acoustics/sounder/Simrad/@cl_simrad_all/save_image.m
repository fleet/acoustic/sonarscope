function flag = save_image(this, NomRep, NomLayer, X)

flag = existCacheNetcdf(this.nomFic);
if flag
    flag = save_signalNetcdf(this, NomRep, NomLayer, X);
else
    flag = save_imageXMLBin(this, NomRep, NomLayer, X);
end

