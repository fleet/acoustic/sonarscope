function flag = save_signal(this, NomRep, NomLayer, X)

flag = existCacheNetcdf(this.nomFic);
if flag
    flag = save_signalNetcdf(this, NomRep, NomLayer, X);
else
    flag = save_signalXMLBin(this, NomRep, NomLayer, X);
end
