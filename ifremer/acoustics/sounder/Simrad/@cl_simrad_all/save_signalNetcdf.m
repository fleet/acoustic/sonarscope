% flag = save_signalNetcdf(this, 'Ssc_Position', 'Latitude', LatitudeImage);

function flag = save_signalNetcdf(this, NomRep, NomLayer, X)

[nomDir, nom] = fileparts(this.nomFic);
nomFicNc = fullfile(nomDir, 'SonarScope', [nom '.nc']);
if ~exist(nomFicNc, 'file')
    flag = 0;
    return
end

try
    grpName = strrep(NomRep, 'Ssc_', '');
    ncID  = netcdf.open(nomFicNc, 'WRITE');
    grpID = netcdf.inqNcid(ncID, grpName);
    varID = netcdf.inqVarID(grpID, NomLayer);
    
    % Gestion des exceptions (mauvaise conception de cl_time au d�part)
    switch class(X)
        case 'cl_time'
            X = X.timeMat;
            Temp = netcdf.getVar(grpID, varID);
            if ~isequal(size(X), size(Temp))
                for k=1:size(Temp, 2)
                    Temp(:,k) = X;
                end
                X = Temp;
            end
        case 'datetime'
            X = datenum(X);
            Temp = netcdf.getVar(grpID, varID);
            if ~isequal(size(X), size(Temp))
                for k=1:size(Temp, 2)
                    Temp(:,k) = X;
                end
                X = Temp;
            end
    end
    NetcdfUtils.putVarVal(grpID, varID, X, 1)
    flag = 1;
    netcdf.close(ncID);
catch
	netcdf.close(ncID);
    flag = 0;
    messageErreurFichier(nomFicNc, 'WriteFailure');
end
