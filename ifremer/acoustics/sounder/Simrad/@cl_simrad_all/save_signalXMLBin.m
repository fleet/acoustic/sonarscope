function flag = save_signalXMLBin(this, NomRep, NomLayer, X)

[nomDir, nom] = fileparts(this.nomFic);

nomFicXml = fullfile(nomDir, 'SonarScope', nom, [NomRep '.xml']);
if ~exist(nomFicXml, 'file')
    flag = 0;
    return
end

Info = xml_mat_read(nomFicXml);
nomDirRacine = fullfile(nomDir, 'SonarScope', nom);

k = find(strcmp({Info.Signals.Name}, NomLayer));
if isempty(k)
    flag = 0;
    return
end

flag = writeSignal(nomDirRacine, Info.Signals(k), X);
