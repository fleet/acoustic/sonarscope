% Sélection des datagrammes pour troncature, séparation, merge ... d'un
% fichier ALL.
%
% Syntax
%   [f, S] = selectDatagramFromFile(aKM, 'DisplayTable', 0)
%
% Input Arguments
%   aKM : Instance de cl_simrad_all
%
% Name-only Arguments
%   DisplayTable : affichage ou non de la table
%
% Output Arguments
%   selectDatagram : datagrammes sélectionnées (Nb, Code, Label, Flag de sélection).
%
% Examples
%   nomFicAll = getNomFicDatabase('0011_20110307_211804_IKATERE_02.all');
%   aKM = cl_simrad_all('nomFic', nomFicAll);
%   selectDatagram = selectDatagramFromFile(aKM);
%   % Création du fichier all (avec tous les datagrammes hors WC si il en contenait).
%   nomFicOut = saucissonne_bin(aKM, NbPings, SplitTime, selectDatagram);
%
% See also saucissonne_bin Authors
% Authors : GLU
% ----------------------------------------------------------------------------

function [flag, selectDatagram] = selectDatagramFromFile(this, varargin)

[varargin, flagDisplayTable] = getPropertyValue(varargin, 'DisplayTable', 1); %#ok<ASGLU>

% Histogramme des types de datagrammes.
[~, Code, histoTypeDatagram] = histo_index(this);

if isempty(Code)
    flag           = 0;
    selectDatagram = [];
    return
end

sub       = ~(histoTypeDatagram == 0);
labels    = labelDatagramsSimrad(Code, histoTypeDatagram);
subCode   = Code(sub);
subLabels = labels(subCode);
subHisto  = histoTypeDatagram(sub);

subLabels{2} = 'Unknown';
listDatagramMandatory.code = [];
listDatagramMandatory.name = {};
listDatagramMandatory.nb   = [];
listDatagramOpt.code       = [];
listDatagramOpt.name       = {};
listDatagramOpt.nb         = [];
u = 0;
v = 0;
% Datagrammes obligatoires : RunTime, Depth (68 et 88) et Installation
% Parameters (73 et 105).
datagramMandatory = [82, 68, 88, 73, 105];
for k = 1:size(subHisto,2)
    if any(datagramMandatory(:) == subCode(k))
        u = u+1;
        listDatagramMandatory.code(u) = subCode(k);
        listDatagramMandatory.name{u} = subLabels{k};
        listDatagramMandatory.nb(u)   = subHisto(k);
    else
        v = v+1;
        listDatagramOpt.code(v) = subCode(k);
        listDatagramOpt.name{v} = subLabels{k};
        listDatagramOpt.nb(v)   = subHisto(k);
    end
end

% Ouverture de l'IHM de sélection.
if flagDisplayTable
    [flag, selectDatagram] = uiTableDatagram(listDatagramMandatory, listDatagramOpt);
    if ~flag
        return
    end
else
    flag = 1;
    % Datagrammes obligatoires
    selectDatagram = [];
    for k=1:size(listDatagramMandatory.name,2)
        selectDatagram = [selectDatagram;{listDatagramMandatory.nb(k), ...
            listDatagramMandatory.code(k), ...
            listDatagramMandatory.name{k}, ...
            true}]; %#ok<AGROW>
    end
    % Datagrammes optionnels
    for k=1:size(listDatagramOpt.name,2)
        selectDatagram = [selectDatagram;  {listDatagramOpt.nb(k), ...
            listDatagramOpt.code(k), ...
            listDatagramOpt.name{k}, ...
            true}]; %#ok<AGROW>
    end
end
% % % Test de présence du RunTime
% % dummy = ([selectDatagram{:,2}] == 82);
% % if selectDatagram{dummy,4} == 0
% %     strFR = sprintf('Veuillez sélectionner le datagramme RunTime');
% %     strUS = sprintf('Please select Datagram RunTime');
% %     my_warndlg(Lang(strFR,strUS), 0, 'Tag', 'SelectionRunTime');
% %     return;
% % end
