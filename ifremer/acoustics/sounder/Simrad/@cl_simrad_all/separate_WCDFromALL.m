% S�paration des datagrammes WCD d'un fichier ALL.
%
% Syntax
%   Data = separate_WCDFromALL(aKM, ...)
%
% Input Arguments
%   aKM : Instance de cl_simrad_all
%
% Name-only Arguments
%
% Output Arguments
%   nomFicOut :
%
% Examples
%   nomFic      = getNomFicDatabase('EM1002_0001_20040916_054013_raw.all')
%   aKM           = cl_simrad_all('nomFic', nomFic);
%   nomFicOut   = separate_WCDFromALL(aKM);
%
% See also cl_simrad_all Authors
% Authors : GLU
% ----------------------------------------------------------------------------

function nomFicOut = separate_WCDFromALL(this, subDatagram)

nomFicOut = {};

%% Check the number of elements

flag = checkIfOneElement(length(this));
if ~flag
    return
end

%%

subAllDatagram = subDatagram;
subWcdDatagram = subDatagram;

% On filtre tout d'abord le Datagramme WCD du fichier .ALL
dummy = ([subDatagram{:,2}] == 107);
if subDatagram{dummy,4} == 1
    subAllDatagram{dummy,4} = false;
end

% Cr�ation des fichiers unitaire de type ALL.
targetFile   = '.all';
nomFicAllOut = separate_WCDFromALL_unitaire(this, targetFile, subAllDatagram);
[nomDir, nomFic, ~] = fileparts(this.nomFic);
% nomFicWcd = fullfile(nomDir, [nomFic, '.wcd']);
% Cr�ation des fichiers unitaire de type WCD.
% On d�signe les Datagrammes WCD du fichier .ALL � conserver.
sub = [73, 85, 105, 107];
for k=1:length(subWcdDatagram)
    dummy = find(sub == [subWcdDatagram{k,2}], 1);
    % For�age du choix si celui-ci �tait faux.
    if ~isempty(dummy)
        if subDatagram{dummy,4} == false
            subWcdDatagram{dummy,4} = true;
        end
    else
        subWcdDatagram{k,4} = false;
    end
end

% Cr�ation du WCD sur labase des datagrammes conserv�s plus haut.
targetFile   = '.wcd';
nomFicWcdOut = separate_WCDFromALL_unitaire(this, targetFile, subWcdDatagram); %#ok<NASGU>


%% Lecture des nouveaux fichiers ALL (et WCD en automatique)

k2 = 0;
for k1=1:length(nomFicAllOut)
    X = cl_simrad_all('nomFic', nomFicAllOut{k1});
    if isempty(X)
        continue
    end
    k2 = k2 + 1;
    aKM(k2) = X; %#ok<AGROW,NASGU>
end

% plot_position(aKM)

%% Renommage du fichier .all et .wcd

nomFicNew = fullfile(nomDir, [nomFic '.allSeparated']);

[status, message] = movefile(this.nomFic, nomFicNew, 'f');

if status
    strFR = sprintf('Le fichier "%s" a �t� renomm� en "%s"', this.nomFic, nomFicNew);
    strUS = sprintf('File "%s" was renamed as "%s"', this.nomFic, nomFicNew);
    my_warndlg(Lang(strFR,strUS), 0, 'Tag', 'ImpossibleDeRenommerCeFichier');
else
    strFR = sprintf('Echec de renommage de "%s" en "%s", faites le � la main : \nMessage matlab : %s', this.nomFic, nomFicNew, message);
    strUS = sprintf('It was impossible to rename "%s" in "%s", do it manualy : \nMatlab message : %s', this.nomFic, nomFicNew, message);
    my_warndlg(Lang(strFR,strUS), 0, 'Tag', 'ImpossibleDeRenommerCeFichier');
end


function nomFicOut = separate_WCDFromALL_unitaire(this, targetFile, subDatagram)

nomFicOut = {};
[nomDir, nomFic, ~] = fileparts(this.nomFic);

%% Check the number of elements

flag = checkIfOneElement(length(this));
if ~flag
    return
end

%% Lecture du fichier d'index

[flag, Data] = read_FileIndex_bin(this);
if ~flag
    return
end

Position       = Data.DatagramPosition;
Taille         = double(Data.DatagramLength);
PingCounter    = Data.PingCounter;
TypeOfDatagram = Data.TypeOfDatagram;
t              = Data.Time.timeMat;

sub          = [subDatagram{:,4}];
selectedCode = [subDatagram{sub,2}];
if isempty(selectedCode)
    strFR = sprintf('Aucun datagramme de s�lectionner : pas de cr�ation de fichiers splitt�s.');
    strUS = sprintf('None datagram selected : no split files creation.');
    my_warndlg(Lang(strFR,strUS), 0);
    return
end

index = ones(size(t,1),1) ;

%% V�rification pour que des pings d'un sondeur dual ne se retrouvent pas s�par�s dans 2 fichiers s�par�s

% listeDatagramsMBES = [68 88 75 70 102 78 83 89 107 113];
% listeDatagramsMBES = [68 70 75 78 79 83 88 89 102 107 109 113]; % Modifi� le 15/05/2012 � bord du Falkor
% Modif par GLU pour traiter les derniers datagrammes trait�s : le
% 04/06/0212
listeDatagramsMBES = [49 51 65 67 68 70 71 72 73 74 78 80 82 83 85 88 89 102 104 107 109 110 112 114];

for k=1:length(listeDatagramsMBES)
    sub = (TypeOfDatagram == listeDatagramsMBES(k));
    PC = PingCounter(sub);
    PCIndex = index(sub);
    subChangement = find(diff(PCIndex) == 1);
    for k2=1:length(subChangement)
        if length(unique(PC(subChangement:subChangement+1))) == 1
            PCIndex(subChangement:subChangement+1) = PCIndex(subChangement+1);
        end
    end
    index(sub) = PCIndex;
end

% plot_position(this)

%% Decodage du fichier de donn�es

typeIndian = getIndianAll(this.nomFic);
[fidIn, message] = fopen(this.nomFic, 'r', typeIndian);
if fidIn < 0
    my_warndlg(message, 1);
    return
end

for k=1:max(index)
    % Simple filtrage des datagrammes.
    nomFicOut{k} = fullfile(nomDir, [nomFic '_createdFromAll' targetFile]); %#ok<AGROW>
    fidOut(k) = fopen(nomFicOut{k}, 'w+'); %#ok<AGROW>
    [nomDir2, nomFic2] = fileparts(nomFicOut{k});
    nomsSsc = fullfile(nomDir2, 'SonarScope', nomFic2);
    if exist(nomsSsc, 'dir')
        try
            rmdir(nomsSsc, 's')
            if exist(nomsSsc, 'dir')
                strFR = sprintf('"%s" n''a pas pu �tre supprim�.', nomsSsc);
                strUS = sprintf('"%s" could not be suppressed.', nomsSsc);
                my_warndlg(Lang(strFR,strUS), 0);
                flag = 0;
            end
            
        catch %#ok<CTCH>
            strFR = sprintf('Le r�pertoire"%s" n''a pas pu �tre supprim�, on arr�te tout.', nomsSsc);
            strUS = sprintf('Folder "%s" could not be suppressed. Nothing will be done before, please solve the problem before.', nomsSsc);
            my_warndlg(Lang(strFR,strUS), 0);
            flag = 0;
        end
    end
end
if ~flag
    return
end

nomFicAuDela = fullfile(nomDir, [nomFic '_' num2str(k+1,'%02d') targetFile]);
if exist(nomFicAuDela, 'file')
    strFR = sprintf('ATTENTION : des fichiers existent sur le r�pertoire avec des num�ros supp�rieurs � ceux qui vont �tre g�n�r�s. Cela risque de semer la confusion lors des traitements ult�rieurs. Je vous conseille de les supprimer � partir du fichier "%s".', nomFicAuDela);
    strUS = sprintf('WARNING : some files exist with numbers supeperior to the ones you are going to create. I recommand that you supress them beginning from "%s"', nomFicAuDela);
    my_warndlg(Lang(strFR,strUS), 0);
end


%% Traitement du fichier

% PU Status	:               49
% ExtraParameters	:       51
% Attitude	:               65
% Clock	:                   67
% DepthV1	:               68
% Raw Range Beam Angles	:	70
% Surface Sound Speed	:	71
% Heading	:               72
% Mecha Transducer Tilt	:	74
% Raw Range Beam Angles (new version)	:	78
% Position	:               80
% Runtime	:               82
% Seabed Image Data	:       83
% SoundSpeedProfile	:       85
% XYZ88	:                   88
% Seabed Image Data 89	:	89
% Raw Range Beam Angles (since 2004)	:	102
% Height	:               104
% Water Column	:           107
% Stave Data	:           109
% Network Attitude	:       110
% Quality Factor	:       112
% Raw Beam Samples	:       114
% InstallParams	:           73 (Start) ou 105 (Stop)ou 112
% TODO: identifier les num�ros et choisir l'action
% 102 78 113 72 69 84 71 87 112 74 48 49 66

str = sprintf('Loading file %s', nomFic);
N   = length(Position);
hw  = create_waitbar(str, 'N', N);
for k=1:N
    my_waitbar(k, N, hw);
    % On ne sauvegarde que les datagrammes s�lectionn�s.
    pppp = find(selectedCode == TypeOfDatagram(k),1);
    if ~isempty(pppp)
        switch TypeOfDatagram(k)
            
            % Depth, Raw range and beam angle, SeabedImage, Central beams
            % echogram, quality factor,
            case {51 49 68 70 75 78 79 83 88 89 102 104 107 109 113 114 }
                fseek(fidIn, Position(k), 'bof');
                X = fread(fidIn, Taille(k), 'uchar');
                fwrite(fidOut(index(k)), X, 'uchar');
                
                % Attitude, Clock, Position : TODO : il faudrait peut-�tre recopier un de chaque avant et apr�s  : passage par des subAttitude{k} qui serait obtenu par un subAtt(1)-1:subAtt(end)+1
            case {65 67 80 110}
                fseek(fidIn, Position(k), 'bof');
                X = fread(fidIn, Taille(k), 'uchar');
                fwrite(fidOut(index(k)), X, 'uchar');
                
                % Installation Parameter start, Runtime, Sound speed profile, Installation Parameter stop
            case {66 69 71 72 73 74 48 82 84 85 87 105 112}
                fseek(fidIn, Position(k), 'bof');
                X = fread(fidIn, Taille(k), 'uchar');
                for k2=1:max(index)
                    fwrite(fidOut(k2), X, 'uchar');
                end
                
                %
            otherwise
                strFR = sprintf('TypeOfDatagram "%d" non identifi� dans "separate_WCDFromALL", signalez ce message � sonarscope@ifremer.fr', TypeOfDatagram(k));
                strUS =  sprintf('TypeOfDatagram "%d" not identified in "separate_WCDFromALL", please report to sonarscope@ifremer.fr', TypeOfDatagram(k));
                my_warndlg(Lang(strFR,strUS), 0, 'Tag', 'DatagrammeNonIdentifie');
                fseek(fidIn, Position(k), 'bof');
                X = fread(fidIn, Taille(k), 'uchar');
                for k2=1:max(index)
                    fwrite(fidOut(k2), X, 'uchar');
                end
        end
    end
end
my_close(hw)
fclose(fidIn);
for k=1:max(index)
    fclose(fidOut(k));
end
