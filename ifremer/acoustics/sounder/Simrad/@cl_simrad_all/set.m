% Acces en ecriture des proprietes d'une instance
%
% Syntax
%   b = set(a, ...)
% 
% Input Arguments 
%   a : Une instance de la classe cl_simrad_all
%
% Name-Value Pair Arguments
%   nomFic : Nom(s) du fichier .all
%
% Output Arguments
%   [] : Sauve l'instance dans a
%   b  : Instance de la classe
% 
% Examples
%   nomFic = getNomFicDatabase('EM1002_0001_20040916_054013_raw.all')
%   a = cl_simrad_all
%   a = set(a, 'nomFic', nomFic)
%
% See also cl_simrad_all cl_simrad_all/get Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function varargout = set(this, varargin)

[varargin, nomFic]          = getPropertyValue(varargin, 'nomFic',          []);
[varargin, ShowProgressBar] = getPropertyValue(varargin, 'ShowProgressBar', true); %#ok<ASGLU>

if ~isempty(nomFic)
    nomFic = windows_replaceVolumeLetter(nomFic);
    this.nomFic = nomFic;
    [~, ~, Ext] = fileparts(nomFic);
    switch Ext
        case {'.all'; '.wcd'}
            [flag, flagRTK, nomFicWc, EmModel, SystemSerialNumber, Version, SousVersion] = all2ssc(nomFic, 'ShowProgressBar', ShowProgressBar); % TODO
            if ~flag
                varargout{1} = [];
                return
            end
        case {'.kmall'; '.kmwcd'}
            [flag, flagRTK, nomFicWc, EmModel, SystemSerialNumber, Version, SousVersion] = KmallXsf2ssc(nomFic, 'ShowProgressBar', ShowProgressBar); % TODO
            if ~flag
                varargout{1} = [];
                return
            end
    end
    
    this.flagRTK = flagRTK;
    if ~isempty(nomFicWc)
        this.nomFicWC = nomFicWc;
    end
    this.EmModel            = EmModel;
    this.SystemSerialNumber = SystemSerialNumber;
    this.Version            = Version;
    this.SousVersion        = SousVersion;
end

%% Sortie

if nargout == 0
    assignin('caller', inputname(1), this);
else
    varargout{1} = this;
end
