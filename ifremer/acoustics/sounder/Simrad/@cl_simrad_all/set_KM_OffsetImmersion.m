function [flag, Z, DataDepth, DataAttitude] = set_KM_OffsetImmersion(a, tOffsetImmersion, OffsetImmersion, varargin)

[varargin, DataDepth]    = getPropertyValue(varargin, 'DataDepth',    []);
[varargin, DataAttitude] = getPropertyValue(varargin, 'DataAttitude', []); %#ok<ASGLU>

if isempty(DataDepth)
    [flag, DataDepth] = read_depth_bin(a, 'Memmapfile', 1);
    if ~flag
        Z.Data = [];
        return
    end
end

if isempty(DataAttitude)
    [flag, DataAttitude] = read_attitude_bin(a, 'Memmapfile', 1);
    if ~flag
        Z.Data = [];
        return
    end
end

try
    TransducerDepth = -DataDepth.TransducerDepth(:,:);
catch % TODO : trouver le bug
    %     TransducerDepth = -DataDepth.TransducerDepth(:,:);
    %     TransducerDepth = TransducerDepth();
    TransducerDepth = -DataDepth.TransducerDepth(:);
end
ImmersionDepth = TransducerDepth;

for k=1:size(OffsetImmersion,2)
%     OffsetImmersionDepth(:,k) = my_interp1(tOffsetImmersion, OffsetImmersion(:,k), DataDepth.Datetime, 'linear', 'extrap'); %#ok<AGROW>
    OffsetImmersionDepth(:,k) = my_interp1(tOffsetImmersion, OffsetImmersion(:,k), DataDepth.Datetime, 'linear'); %#ok<AGROW>
end

if all(isnan(OffsetImmersionDepth))
%     msg = sprintf('Immersion file is not defined for file "%s"', a.nomFic);
%     my_warndlg(msg, 0, 'Tag', 'set_KM_TransducerDepth');
    flag = 0;
    Z.Data = [];
    return
end

% TransducerDepth int�gre le Heave dans les datagrams Simrad. On retire
% l'influence du Heave. On devrait obtenir une constante.
if size(TransducerDepth,2) == 2
    TransducerDepth(:,1) = ImmersionDepth(:,1) + OffsetImmersionDepth;
    TransducerDepth(:,2) = ImmersionDepth(:,2) + OffsetImmersionDepth;
else
    TransducerDepth = ImmersionDepth + OffsetImmersionDepth;
end

Z.Data     = -TransducerDepth;
Z.Time     = DataDepth.Time;
Z.Datetime = DataDepth.Datetime;
Z.Unit     = 'm';

flag = 1;
