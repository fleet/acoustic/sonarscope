function [flag, Z, DataDepth, DataAttitude] = set_KM_TransducerDepth(a, tImmersion, Immersion, varargin)

[varargin, DataDepth]    = getPropertyValue(varargin, 'DataDepth',    []);
[varargin, DataAttitude] = getPropertyValue(varargin, 'DataAttitude', []); %#ok<ASGLU>

if isempty(DataDepth)
    [flag, DataDepth] = read_depth_bin(a, 'Memmapfile', 1);
    if ~flag
        Z.Data = [];
        return
    end
end

if isempty(DataAttitude)
    [flag, DataAttitude] = read_attitude_bin(a, 'Memmapfile', 1);
    if ~flag
        Z.Data = [];
        return
    end
end

try
    TransducerDepth = -DataDepth.TransducerDepth(:,:);
catch % TODO : trouver le bug
    %     TransducerDepth = -DataDepth.TransducerDepth(:,:);
    %     TransducerDepth = TransducerDepth();
    TransducerDepth = -DataDepth.TransducerDepth(:);
end

Heave = my_interp1(DataAttitude.Time, DataAttitude.Heave, DataDepth.Time, 'linear', 'extrap');
for k=1:size(Immersion,2)
%     ImmersionDepth(:,k) = my_interp1(tImmersion, Immersion(:,k), DataDepth.Datetime, 'linear', 'extrap'); %#ok<AGROW>
    ImmersionDepth(:,k) = my_interp1(tImmersion, Immersion(:,k), DataDepth.Datetime, 'linear'); %#ok<AGROW>
end

if all(isnan(ImmersionDepth))
%     msg = sprintf('Immersion file is not defined for file "%s"', a.nomFic);
%     my_warndlg(msg, 0, 'Tag', 'set_KM_TransducerDepth');
    flag = 0;
    Z.Data = [];
    return
end

% TransducerDepth int�gre le Heave dans les datagrams Simrad. On retire
% l'influence du Heave. On devrait obtenir une constante.
if size(TransducerDepth,2) == 2
    TransducerDepth(:,1) = ImmersionDepth(:,1) - Heave;
    TransducerDepth(:,2) = ImmersionDepth(:,2) - Heave;
else
    TransducerDepth = ImmersionDepth - Heave;
end

Z.Data     = TransducerDepth;
Z.Time     = DataDepth.Time;
Z.Datetime = DataDepth.Datetime;
Z.Unit     = 'm';

flag = 1;
