function [flag, SonarDescription] = set_KongsbergBsCorr(this, SonarDescription)

[flag, DataExtraParameters] = read_ExtraParameters_bin(this);
if flag 
    if isfield(DataExtraParameters, 'BSCorrInfo')
        % Format de type EM710.
        switch DataExtraParameters.BSCorrInfo.identFormat 
            case 1 % 712
                [flag, SonarDescription] = set_KongsbergBsCorrSpline(SonarDescription, ...
                                DataExtraParameters.EmModel, DataExtraParameters.SystemSerialNumber, ...
                                DataExtraParameters.BsCorr);
                return
            case 2
                [flag, SonarDescription] = set_KongsbergBsCorrPoly(SonarDescription, ...
                                DataExtraParameters.EmModel, DataExtraParameters.SystemSerialNumber, ...
                                DataExtraParameters.BsCorr);
                return
            case 4
%                 my_breakpoint
                %{
                % TODO
                [flag, SonarDescription] = set_KongsbergBsCorrSplineEM2040(SonarDescription, ...
                                DataExtraParameters.EmModel, DataExtraParameters.SystemSerialNumber, ...
                                DataExtraParameters.BsCorr);
                %}
        end
%     else
%         str1 = sprintf('Donn�es BSCorr absentes du fichier .all : %s', this.nomFic);
%         str2 = sprintf('BSCorr data not enable in .all file : %s', this.nomFic);
%         fprintf('%s\n', Lang(str1,str2));
    end
end

%% A d�faut, de d�codage du BSCorr du sondeur depuis le fichier .ALL, on recherche si il y a un BSCorrSpline correspondant � ce sondeur dans SScTbxResources\BSCorr

EmModel = get(this, 'EmModel');
SystemSerialNumber = get(this, 'SystemSerialNumber');
nomFicSeul   = sprintf('BScorr_%s.txt', num2str(EmModel));
nomFicBSCorr = getNomFicDatabase(nomFicSeul, 'NoMessage');
if ~isempty(nomFicBSCorr)
    [flag, BsCorr, BSCorrInfo] = read_BSCorr(nomFicBSCorr);
    if flag
        [~, nomFicAllSeul] = fileparts(this.nomFic);
        str1 = sprintf('Fichier "%s" : BSCorr a �t� trouv� dans "%s", les mod�les de diagramme de directivit� dits "Calibration" ont �t� forc�s � "AntennePoly".', nomFicAllSeul, nomFicBSCorr);
        str2 = sprintf('File "%s" : BSCorr was found in "%s", the Calibration Tx beam diagrams have been switched to "AntennePoly models".', nomFicAllSeul, nomFicBSCorr);
        switch BSCorrInfo.identFormat 
            case 1 % Nodes
                [flag, SonarDescription] = set_KongsbergBsCorrSpline(SonarDescription, ...
                    EmModel, SystemSerialNumber, BsCorr);
                if flag
                    if ~my_isdeployed
                        %                     my_warndlg(Lang(str1,str2), 0, 'Tag', 'FichierBSCorrLisible');
                        fprintf('%s\n', Lang(str1,str2));
                    end
                end
                return
                
            case 2 % Poly
                [flag, SonarDescription] = set_KongsbergBsCorrPoly(SonarDescription, ...
                    EmModel, SystemSerialNumber, BsCorr);
                if flag
%                     my_warndlg(Lang(str1,str2), 0, 'Tag', 'FichierBSCorrLisible');
                    fprintf('%s\n', Lang(str1,str2));
                end
                return
                
            case 3 % Nodes EM304
                [flag, SonarDescription] = set_KongsbergBsCorrSpline(SonarDescription, ...
                    EmModel, SystemSerialNumber, BsCorr);
                if flag
                    if ~my_isdeployed
                        %                     my_warndlg(Lang(str1,str2), 0, 'Tag', 'FichierBSCorrLisible');
                        fprintf('%s\n', Lang(str1,str2));
                    end
                end
                return
                
            case 4 % Nodes EM2040
                % TODO
                my_breakpoint
                %{
                [flag, SonarDescription] = set_KongsbergBsCorrSplineEM2040(SonarDescription, ...
                    EmModel, SystemSerialNumber, BsCorr);
                if flag
                    if ~my_isdeployed
                        %                     my_warndlg(Lang(str1,str2), 0, 'Tag', 'FichierBSCorrLisible');
                        fprintf('%s\n', Lang(str1,str2));
                    end
                end
                return
                %}
                
            otherwise
                my_breakpoint
        end
        return
    else
        str1 = sprintf('Le fichier "%s" n''a pas pu �tre d�cod�.', nomFicBSCorr);
        str2 = sprintf('"%s" could not be read.', nomFicBSCorr);
        fprintf('%s\n', Lang(str1,str2));
%         my_warndlg(Lang(str1,str2), 0, 'Tag', 'FichierBSCorrPasLisible'); % Comment� car bug si //Tbx
    end
end

%% On recherche si il y a un BSCorr correspondant � ce sondeur dans SScTbxResources\BSCorr

nomFicSeul = sprintf('bscorr_%s.txt', num2str(EmModel));
nomFicBSCorr = getNomFicDatabase(nomFicSeul, 'NoMessage');
if ~isempty(nomFicBSCorr)
    [flag, BsCorr] = read_BSCorr(nomFicBSCorr);
    if flag
        [flag, SonarDescription] = set_KongsbergBsCorrPoly(SonarDescription, ...
            DataExtraParameters.EmModel, DataExtraParameters.SystemSerialNumber, ...
            BsCorr);
        return
    else
        str1 = sprintf('Le fichier "%s" n''a pas pu �tre d�cod�.', nomFicBSCorr);
        str2 = sprintf('"%s" could not be read.', nomFicBSCorr);
        fprintf('%s\n', Lang(str1,str2));
%         my_warndlg(Lang(str1,str2), 0, 'Tag', 'FichierBSCorrPasLisible'); % Comment� car bug si //Tbx
    end
end
