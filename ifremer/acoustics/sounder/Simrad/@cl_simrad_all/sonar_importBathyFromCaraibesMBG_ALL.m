function sonar_importBathyFromCaraibesMBG_ALL(this, nomFicMBG, varargin)

[varargin, ListeFlagsInvalides] = getPropertyValue(varargin, 'ListeFlagsInvalides', []); %#ok<ASGLU>

if ~iscell(nomFicMBG)
    nomFicMBG = {nomFicMBG};
end

%% R�cup�ration du type de sondeur dans le premier .all

this_1 = this(1);
if isempty(this_1)
    return
end
[flag, DataDepth] = read_depth_bin(this_1, 'Memmapfile', 1);
if ~flag
    return
end

[flag, ~, isDual] = read_installationParameters_bin(this_1, 'Memmapfile', 0);
if ~flag
    return
end

[flag, SonarIdent] = SimradModel2IdentSonar(DataDepth.EmModel, 'isDual', isDual);
if ~flag
    return
end

%% Lecture des heures d�but et fin des fichiers .mbg

if isempty(ListeFlagsInvalides)
    [flag, ListeFlagsInvalides] = paramsFlagCaraibes;
    if ~flag
        return
    end
end
I0 = cl_image_I0;

N = length(nomFicMBG);
TimeMatMbg = NaN(N,2);
subOK = true(N,1);
for k=1:N
    MBG = import_CaraibesMbg(I0, nomFicMBG{k}, 'Sonar.Ident', SonarIdent, ...
        'ListeFlagsInvalides', ListeFlagsInvalides, 'indLayerImport', 3);
    if isempty(MBG)
        subOK(k) = false;
        continue
    end
    SonarTimeMbg = get(MBG, 'SonarTime');
    SonarTimeMbg = SonarTimeMbg.timeMat;
%     TimeMatMbg(k,:) = SonarTimeMbg([1 end],1); %#ok<AGROW>
    TimeMatMbg(k,:) = SonarTimeMbg([1 end]);
end
TimeMatMbg = TimeMatMbg(subOK,:);
nomFicMBG = nomFicMBG(subOK);

% tri des heures de d�but croissante
[TimeMatMbg,idx] = sortrows(TimeMatMbg, 1);
nomFicMBG = nomFicMBG(idx);

%% Traitement des fichiers .all

str1 = 'Importation des masques de Caraibes en cours.';
str2 = 'Import masks from Caraibes MBG files.';
N = length(this);
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
    my_waitbar(k, N, hw);
    
    flag = sonar_importBathyFromCaraibesMBG_ALL_unitaire(this(k), nomFicMBG, TimeMatMbg, ...
        SonarIdent, ListeFlagsInvalides);
    
    if ~flag && (k ~= N)
        flag = question_ContinueNextFile();
        if ~flag
            break
        end
    end
end
my_close(hw, 'MsgEnd')


function flag = sonar_importBathyFromCaraibesMBG_ALL_unitaire(this, nomFicMBG, TimeMatMbg, SonarIdent, ListeFlagsInvalides)

I0 = cl_image_I0;

[flag, DataDepth] = read_depth_bin(this, 'Memmapfile', 0);
if ~flag
    return
end

% [flag, DataInstallationParameters, isDual] = read_installationParameters_bin(this, 'Memmapfile', 1);
% if ~flag
%     return
% end

% [flag, IdentSonar] = SimradModel2IdentSonar(DataDepth.EmModel, 'isDual', isDual);
% if ~flag
%     return
% end

logFileId = getLogFileId;

%% Recherche des pings Kongsberg vides

% X = ~isnan(DataDepth.Depth);
SonarTimeAll = DataDepth.Time.timeMat;
% SonarPingAll = DataDepth.PingCounter;

%% Lecture du fichier MBG

flagFichierImpacte = 0;
for k=1:length(nomFicMBG)
    if (SonarTimeAll(end) < TimeMatMbg(k,1)) || (SonarTimeAll(1) > TimeMatMbg(k,2))
        continue
    end
    
    flagFichierImpacte = 1;
    MBG = import_CaraibesMbg(I0, nomFicMBG{k}, 'Sonar.Ident', SonarIdent, ...
        'ListeFlagsInvalides', ListeFlagsInvalides, 'indLayerImport', 3);
    SonarTimeMbg = get(MBG, 'SonarTime');
%     SonarPingMbg = get(MBG, 'SonarPingCounter');
    SonarTimeMbg = SonarTimeMbg.timeMat;    

% Comment� le 09/07/2019 pour donn�es Geosciences-Azur o� le ping counter fait plusieurs fois le tour du cadran
%     [~, subAll_t, subMbg_t] = intersect(SonarTimeAll, SonarTimeMbg(:));
%     subAll = union(subAll_t, subAll_p);
%     subMbg = union(subMbg_t, subMbg_p);

    % Ajout JMA le 20/09/2021
    if size(SonarTimeMbg,1) == 1
        SonarTimeMbg = SonarTimeMbg';
    end
    
    p1 = datetime(round(SonarTimeAll(:,1),7), 'ConvertFrom', 'datenum');
    p2 = datetime(round(SonarTimeMbg(:,1),7), 'ConvertFrom', 'datenum');
    [~, subAll_t, subMbg_t] = intersect(p1, p2);
    if isempty(subAll_t)
        continue
    end
    d1 = diff(subAll_t([1 end]));
    d2 = diff(subMbg_t([1 end]));
    if d1 == d2
        subAll_t = subAll_t(1):subAll_t(end);
        subMbg_t = subMbg_t(1):subMbg_t(end);
    end
    subAll = subAll_t;
    subMbg = subMbg_t;
    
    % Verrue dans le cas o� les deux vecteurs sont de taille diff�rentes
    % !!!! ecart d'un ping
    if numel(subAll) == numel(subMbg)
%         subMbg;
    elseif (numel(subAll) - numel(subMbg)) == 1
        mTop = mean(SonarTimeAll(subAll(2:end))'   - SonarTimeMbg(subMbg));
        mBot = mean(SonarTimeAll(subAll(1:end-1))' - SonarTimeMbg(subMbg));
        if mTop < mBot
            subAll = subAll(2:end);
        else
            subAll = subAll(1:end-1);
        end
    elseif (numel(subMbg) - numel(subAll)) == 1
        mTop = mean(SonarTimeAll(subAll)' - SonarTimeMbg(subMbg(2:end)));
        mBot = mean(SonarTimeAll(subAll)' - SonarTimeMbg(subMbg(1:end-1)));
        if mTop < mBot
            subMbg = subMbg(2:end);
        else
            subMbg = subMbg(1:end-1);
        end
    else % on sait pas g�rer
        str = sprintf('\n\t%s\n\t%s\n\t--> I do not know what to do !\n',  this.nomFic, nomFicMBG{k});
        logFileId.info('SonarAllImportFlagsFromCaraibes', str);
        continue
    end
    
    % Aucun �l�ment commun
    if numel(subAll) == 0 || numel(subMbg) == 0
        str = sprintf('\n\t%s\n\t%s\n\t--> No common part\n',  this.nomFic, nomFicMBG{k});
        logFileId.info('SonarAllImportFlagsFromCaraibes', str);
        continue
    end    
    
    % Sortie �cran
    str = sprintf('\n\t%s\n\t%s\n\t--> %d / %d pings impacted\n',  this.nomFic, nomFicMBG{k}, numel(subAll), length(p1));
    logFileId.info('SonarAllImportFlagsFromCaraibes', str);
    
%     T0 = SonarTimeAll(1);
%     DeltaT = median(diff(SonarTimeAll-T0));
%     T1 = floor((SonarTimeAll-T0)/DeltaT);
%     T2 = floor((SonarTimeMbg(:,1)-T0)/DeltaT);
%     [subAll, subMbg, ~, tdeb, tfin] = intersect3(T1, T2, T2);
%     subNonAll = setdiff(1:length(SonarTimeAll), subAll);
    %{
    FigUtils.createSScFigure; PlotUtils.createSScPlot(SonarTimeAll-T0, 'b+'); grid on; hold on; plot(SonarTimeMbg(:,1)-T0, 'ro'); plot(subNonAll, SonarTimeAll(subNonAll)-T0, 'xk')
    FigUtils.createSScFigure; PlotUtils.createSScPlot(diff(SonarTimeAll-T0), 'b+'); grid on; hold on; plot(diff(SonarTimeMbg(:,1)-T0), 'ro'); title('Differences')
    
    FigUtils.createSScFigure; PlotUtils.createSScPlot(SonarTimeAll-T0, 'b+'); grid on; hold on; plot(SonarTimeMbg(:)-T0, 'ro'); plot(subNonAll, SonarTimeAll(subNonAll)-T0, 'xk')
    FigUtils.createSScFigure; PlotUtils.createSScPlot(diff(SonarTimeAll-T0), 'b+'); grid on; hold on; plot(diff(SonarTimeMbg(:)-T0), 'ro'); title('Differences')
%}

%{
    %% Premi�re tentative d'am�lioration : voir avec Arnaud si meilleure id�e
    T1 = SonarTimeAll - SonarTimeAll(1);
    T2 = SonarTimeMbg(:,1) - SonarTimeAll(1);
    epsilon = max(median(diff(T1)), median(diff(T2)));
    epsilon = epsilon / 2;
    T1 = floor(T1 / epsilon) * epsilon;
    T2 = floor(T2 / epsilon) * epsilon;
    [subAll, subMbg, ~, tdeb, tfin] = intersect3(T1, T2, T2);
    subNonAll = setdiff(1:length(T1), subAll);
%     FigUtils.createSScFigure; PlotUtils.createSScPlot(T1, 'b+'); grid on; hold on; plot(T2, 'ro'); plot(subNonAll, T1(subNonAll), '*k')
%     FigUtils.createSScFigure; PlotUtils.createSScPlot(diff(T1), 'b+'); grid on; hold on; plot(diff(T2), 'ro'); title('Differences')
%}

%{
%% Deuxi�me possibilit� : traiter les heures du .all de la m�me mani�re que
cela est fait lors de l'archivage dans les .mbg
%}
    
    X = get_val_ij(MBG, subMbg, []);
    X = isnan(X);
    
    %% Masquage 
    
    DataDepth.Mask(subAll,:) = DataDepth.Mask(subAll,:) | X;
    flag = write_depth_mask(this, DataDepth);
    if ~flag
        MessageErreurFichier(this.nomFic, 'WriteFailure');
    end
end
if ~flagFichierImpacte
    str = sprintf('\n\t%s\n\t--> 0 ping impacted\n',  this.nomFic);
    logFileId.info('SonarAllImportFlagsFromCaraibes', str);
end
