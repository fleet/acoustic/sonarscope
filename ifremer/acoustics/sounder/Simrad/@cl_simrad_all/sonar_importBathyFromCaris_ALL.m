function sonar_importBathyFromCaris_ALL(this, nomFicTxt, varargin)

useParallel = get_UseParallelTbx;

if ~iscell(nomFicTxt)
    nomFicTxt = {nomFicTxt};
end

messageCarisAsciiFlagFile

NbLigEntete  = [];
Separator    = [];
ValNaN       = [];
nColumns     = [];
GeometryType = 7;
IndexColumns = [];
DataType     = [];
Carto        = cl_carto([]);
TypeAlgo     = [];

str1 = 'Importation des masques de Caris';
str2 = 'Import masks from CARIS';
N = length(this);
if useParallel && (N > 1)
    [hw, DQ] = create_parforWaitbar(N, Lang(str1,str2));
    k = 1;
    [flag, NbLigEntete, Separator, ValNaN, nColumns, GeometryType, IndexColumns, DataType, Carto, TypeAlgo] ...
        = sonar_importBathyFromCaris_ALL_unitaire(this(k), nomFicTxt{k}, NbLigEntete, Separator, ValNaN, nColumns, ...
        GeometryType, IndexColumns, DataType, Carto, TypeAlgo);
    if ~flag
        close(hw)
        return
    end
    send(DQ, k);
    parfor k=2:N
        sonar_importBathyFromCaris_ALL_unitaire(this(k), nomFicTxt{k}, NbLigEntete, Separator, ValNaN, nColumns, ...
            GeometryType, IndexColumns, DataType, Carto, TypeAlgo, 'isUsingParfor', true);
        send(DQ, k);
    end
    my_close(hw, 'MsgEnd')
else
    hw = create_waitbar(Lang(str1,str2), 'N', N);
    for k=1:N
        my_waitbar(k, N, hw);
        [flag, NbLigEntete, Separator, ValNaN, nColumns, GeometryType, IndexColumns, DataType, Carto, TypeAlgo] ...
            = sonar_importBathyFromCaris_ALL_unitaire(this(k), nomFicTxt{k}, NbLigEntete, Separator, ValNaN, nColumns, ...
            GeometryType, IndexColumns, DataType, Carto, TypeAlgo); %#ok<ASGLU>
    end
    my_close(hw, 'MsgEnd')
end

function [flag, NbLigEntete, Separator, ValNaN, nColumns, GeometryType, IndexColumns, DataType, Carto, TypeAlgo] ...
    = sonar_importBathyFromCaris_ALL_unitaire(this, nomFicTxt, NbLigEntete, Separator, ValNaN, nColumns, ...
    GeometryType, IndexColumns, DataType, Carto, TypeAlgo, varargin)

global isUsingParfor %#ok<GVMIS>

[varargin, isUsingParfor]  = getPropertyValue(varargin, 'isUsingParfor',  isUsingParfor); %#ok<ASGLU>

%% Test si il existe un fichier .txt

if isempty(nomFicTxt)
    flag = 1;
    return
end

%% Lecture de l'image

[flag, DataDepth] = read_depth_bin(this, 'Memmapfile', 0);
if ~flag
    return
end

%% Lecture du fichier Caris

nbPings = DataDepth.Dimensions.nbPings;
nbBeams = DataDepth.Dimensions.nbBeams;
[flag, MaskCaris] = read_CarisASCII(nomFicTxt, nbPings, nbBeams);
if ~flag
    return
end

%% Masquage

DataDepth.Mask(:,:) = MaskCaris(1:DataDepth.Dimensions.nbPings,:); % nbPings introduit pour fichier NIWA QUOI 0481_20180714_140635.all
flag = write_depth_mask(this, DataDepth);
