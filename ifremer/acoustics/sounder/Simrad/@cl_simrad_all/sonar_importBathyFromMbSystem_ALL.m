function sonar_importBathyFromMbSystem_ALL(this)

NbLigEntete     = [];
Separator       = [];
ValNaN          = [];
nColumns        = [];
GeometryType    = 7;
IndexColumns    = [];
DataType        = [];
Carto           = cl_carto([]);
TypeAlgo        = [];

str1 = 'Importation des masques de Mb-System en cours';
str2 = 'Import masks from Mb-System';
N = length(this);
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
    my_waitbar(k, N, hw);
    [flag, NbLigEntete, Separator, ValNaN, nColumns, GeometryType, IndexColumns, DataType, Carto, TypeAlgo] ...
        = sonar_importBathyFromMbSystem_ALL_unitaire(this(k), NbLigEntete, Separator, ValNaN, nColumns, ...
        GeometryType, IndexColumns, DataType, Carto, TypeAlgo);
    if ~flag && (k ~= N)
        flag = question_ContinueNextFile();
        if ~flag
            break
        end
    end
end
my_close(hw, 'MsgEnd')


function [flag, NbLigEntete, Separator, ValNaN, nColumns, GeometryType, IndexColumns, DataType, Carto, TypeAlgo] ...
    = sonar_importBathyFromMbSystem_ALL_unitaire(this, NbLigEntete, Separator, ValNaN, nColumns, ...
    GeometryType, IndexColumns, DataType, Carto, TypeAlgo)

%% Recherche du fichier Caris correspondant

[nomDir, nomFic] = fileparts(this.nomFic);
nomFicTxt = fullfile(nomDir, [nomFic '.txt']);
if ~exist(nomFicTxt, 'file')
    liste = listeFicOnDir(nomDir, '.txt', 'Filtre', nomFic);
    switch length(liste)
        case 0
            str1 = sprintf('Aucun fichier ".txt" n''a �t� trouv� pour le fichier %s.', this.nomFic);
            str2 = sprintf('No ".txt" file found for %s.', this.nomFic);
            my_warndlg(Lang(str1,str2), 0);
            flag = 0;
            return
        case 1
            nomFicTxt = liste{1};
        otherwise
            str1 = sprintf('Plusieurs fichiers ".txt" correspondent au fichier "%s". S�lectionner celui qui convient dans la liste svp.', nomFic);
            str2 = sprintf('Several ".txt" files seem to be linked to "%s". Select the good one please.', nomFic);
            [rep, flag] = my_listdlg(Lang(str1,str2), liste, 'InitialValue', 1, 'SelectionMode', 'Single');
            if ~flag
                return
            end
            nomFicTxt = liste{rep};
    end
end

%% Lecture de l'image

[flag, DataDepth] = read_depth_bin(this, 'Memmapfile', 0);
if ~flag
    return
end

%% Recherche des pings Kongsberg vides

% X = ~isnan(DataDepth.Depth);
% Y = sum(X,2);
% subPingsVides = find(Y == 0);
% clear X Y

%% Lecture du fichier Caris

nbPings = DataDepth.Dimensions.nbPings;
nbBeams = DataDepth.Dimensions.nbBeams;
[flag, MaskCaris] = read_MbSystemASCII(nomFicTxt, nbPings, nbBeams, DataDepth.Time.timeMat);
if ~flag
    return
end
% figure; imagesc(MaskCaris); title(nomFicTxt);

%% Masquage

DataDepth.Mask(:,:) = MaskCaris;
flag = write_depth_mask(this, DataDepth);
