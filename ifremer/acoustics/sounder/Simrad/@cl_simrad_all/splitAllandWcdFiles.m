function repExport = splitAllandWcdFiles(this, repExport)

% NbPings   = [];
% SplitTime = [];

nomFicListe = fullfile(repExport, 'ListSeparateALLandWCDFiles.txt');
str1 = 'Nom du fichier donnant la liste des nouveaux fichiers .all sectionn�s';
str2 = 'Give the name to the file that will collect the full names of the splitted .all files';
[flag, nomFicListe] = my_uiputfile('*.txt', Lang(str1,str2), nomFicListe);
if ~flag
    return
end
repExport = fileparts(nomFicListe);

fid = fopen(nomFicListe, 'w+');
for k1=1:length(this)
    [flag, selectDatagram]  = selectDatagramFromFile(this(k1), 'DisplayTable', 0);
    if ~flag
        return
    end
    % Cr�ation du fichier all (avec tous les datagrammes hors WC s'il en contenait).
    nomFicOut = separate_WCDFromALL(this(k1), selectDatagram);    
    for k2=1:length(nomFicOut)
        fprintf(fid, '%s\n', nomFicOut{k2});
    end
end
fclose(fid);

str1 = 'La s�paration des fichiers .all et .wcd est termin�';
str2 = 'Separation .all and .wcd files is over.';
my_warndlg(Lang(str1,str2), 0);
