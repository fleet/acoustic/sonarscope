function summary_Datagrams(this, fid, ListeDatagrams, varargin)

[varargin, IndNavUnit] = getPropertyValue(varargin, 'IndNavUnit', []); %#ok<ASGLU>

fprintf(fid, '\n-------------------------------------------------------------------------------\n');
fprintf(fid, 'File\t\t\t : %s\n', get(this, 'nomFic'));

if any(strcmp('Depth', ListeDatagrams))
    summary_Depth(this, fid)
end
if any(strcmp('RawRangeBeamAngle', ListeDatagrams))
    summary_RawRangeBeamAngle(this, fid)
end
if any(strcmp('SeabedImage', ListeDatagrams))
    summary_SeabedImage(this, fid)
end
if any(strcmp('Runtime', ListeDatagrams))
    summary_Runtime(this, fid)
end
if any(strcmp('SurfaceSoundSpeed', ListeDatagrams))
    summary_SurfaceSoundSpeed(this, fid)
end
if any(strcmp('SoundSpeedProfile', ListeDatagrams))
    summary_SoundSpeedProfile(this, fid)
end
if any(strcmp('WaterColumn', ListeDatagrams))
    summary_WaterColumn(this, fid)
end
if any(strcmp('InstallationParameters', ListeDatagrams))
    summary_InstallationParameters(this, fid)
end
if any(strcmp('Attitude', ListeDatagrams))
    summary_Attitude(this, fid)
end
if any(strcmp('Clock', ListeDatagrams))
    summary_Clock(this, fid)
end
if any(strcmp('Position', ListeDatagrams))
    summary_Position(this, fid, IndNavUnit)
end
if any(strcmp('Heading', ListeDatagrams))
    summary_Heading(this, fid)
end
if any(strcmp('Height', ListeDatagrams))
    summary_Height(this, fid)
end
if any(strcmp('VerticalDepth', ListeDatagrams))
    summary_VerticalDepth(this, fid)
end
if any(strcmp('QualityFactor', ListeDatagrams))
    summary_QualityFactor(this, fid)
end
if any(strcmp('StaveData', ListeDatagrams))
    summary_StaveData(this, fid)
end
if any(strcmp('ExtraParameters', ListeDatagrams))
    summary_ExtraParameters(this, fid)
end


function summary_QualityFactor(this, fid)

[flag, DataQualityFactor] = SSc_ReadDatagrams(this.nomFic, 'Ssc_QualityFactorFromIfr', 'Memmapfile', -1);
if ~flag
    return
end

fprintf(fid, '\n--- Quality Factor datagrams ---\n');

fprintf(fid, '%s\n', formatVar('Nb NbSamples', DataQualityFactor.Dimensions.nbPings));
fprintf(fid, '%s\n', formatVar('Nb Beams',     DataQualityFactor.Dimensions.nbBeams));
fprintf(fid, '%s\n', formatVar('Nb Sonders',   DataQualityFactor.Dimensions.nbSounders));

T = DataQualityFactor.Datetime(:);
strTime = sprintf('%s  -  %s', char(T(1)), char(T(end)));
fprintf(fid, '%s\n', formatVar('Time', strTime));

fprintf(fid, '%s\n', strEntete');

valStats = stats(DataQualityFactor.IfremerQF(:));
fprintf(fid, '%s\n', formatStatsFull('Height', valStats));


function summary_VerticalDepth(this, fid)

% [flag, DataVerticalDepth] = read_verticalDepth_bin(this, 'Memmapfile', 1);
[flag, DataVerticalDepth] = SSc_ReadDatagrams(this.nomFic, 'Height', 'Memmapfile', -1); % Non test�
if ~flag
    return
end

fprintf(fid, '\n--- Vertical Depth datagrams ---\n');

str1 = 'Je n''ai jamais pu tester ce datagramme de "VerticalDepth", si vous pouviez m''envoyer ce fichier ce serait sympa. JMA';
str2 = 'I could not test this "VerticalDepth" datagram, if you can send me this file it would be nice. JMA';
my_warndlg(Lang(str1,str2), 0, 'Tag', 'HeigthDatagramDetectedForTheFirstTime');


function summary_Height(this, fid)

[flag, DataHeight] = SSc_ReadDatagrams(this.nomFic, 'Ssc_Height', 'Memmapfile', -1); % Non test� ici
if ~flag
    return
end

fprintf(fid, '\n--- Height datagrams ---\n');

fprintf(fid, '%s\n', formatVar('Nb NbSamples', DataHeight.Dimensions.NbSamples));

T = DataHeight.Datetime(:);
strTime = sprintf('%s  -  %s', char(T(1)), char(T(end)));
fprintf(fid, '%s\n', formatVar('Time', strTime));

fprintf(fid, '%s\n', strEntete');

valStats = stats(DataHeight.Height(:));
fprintf(fid, '%s\n', formatStatsFull('Height', valStats));



function summary_Heading(this, fid)

[flag, x] = SSc_ReadDatagrams(this.nomFic, 'Ssc_Heading');
if ~flag
    return
end

fprintf(fid, '\n--- Heading datagrams ---\n');

fprintf(fid, '%s\n', formatVar('Nb NbSamples', x.Dimensions.NbSamples));

T = x.Datetime(:);
strTime = sprintf('%s  -  %s', char(T(1)), char(T(end)));
fprintf(fid, '%s\n', formatVar('Time', strTime));

fprintf(fid, '%s\n', strEntete');

valStats = stats(x.Heading(:));
fprintf(fid, '%s\n', formatStatsFull('Heading', valStats));


function summary_Position(this, fid, IndNavUnit)

[flag, DataPosition] = read_position_bin(this, 'IndNavUnit', IndNavUnit, 'Memmapfile', 1);
if ~flag
    return
end

fprintf(fid, '\n--- Position datagrams ---\n');

fprintf(fid, '%s\n', formatVar('Nb NbSamples', DataPosition.Dimensions.NbSamples));

T = DataPosition.Datetime(:);
strTime = sprintf('%s  -  %s', char(T(1)), char(T(end)));
fprintf(fid, '%s\n', formatVar('Time', strTime));

fprintf(fid, '%s\n', strEntete');

valStats = stats(DataPosition.Latitude(:));
fprintf(fid, '%s\n', formatStatsFull('Latitude', valStats));

valStats = stats(DataPosition.Longitude(:));
fprintf(fid, '%s\n', formatStatsFull('Longitude', valStats));

valStats = stats(DataPosition.Heading(:));
fprintf(fid, '%s\n', formatStatsLight('Heading', valStats));

valStats = stats(DataPosition.Speed(:));
fprintf(fid, '%s\n', formatStatsFull('Speed', valStats));

valStats = stats(DataPosition.CourseVessel(:));
fprintf(fid, '%s\n', formatStatsFull('CourseVessel', valStats));

if isfield(DataPosition, 'FixPosition') % Ajout� le 14/12/2012 par JMA
    valStats = stats(DataPosition.FixPosition(:));
    fprintf(fid, '%s\n', formatStatsFull('FixPosition', valStats));
end

valStats = stats(DataPosition.PositionDescriptor(:)); % PositionDecriptor avant, modifi� le 14/12/2012 par JM1
fprintf(fid, '%s\n', formatStatsLight('PositionDescriptor', valStats));
     

function summary_Clock(this, fid)

[flag, DataClock] = SSc_ReadDatagrams(this.nomFic, 'Ssc_Clock');
if ~flag
    return
end

fprintf(fid, '\n--- Clock datagrams ---\n');

fprintf(fid, '%s\n', formatVar('Nb NbSamples', DataClock.Dimensions.NbSamples));

T = DataClock.Datetime(:);
strTime = sprintf('%s  -  %s', char(T(1)), char(T(end)));
fprintf(fid, '%s\n', formatVar('Time', strTime));

T = DataClock.ExternalTime(:);
[date, heure] = timeMat2Ifr(T.timeMat);
strTime = sprintf('%s %s  -  %s % s', ...
    dayIfr2str(date(1)), hourIfr2str(heure(1)), ...
    dayIfr2str(date(end)), hourIfr2str(heure(end)));
fprintf(fid, '%s\n', formatVar('ExternalTime', strTime));

fprintf(fid, '%s\n', strEntete');

valStats = stats(DataClock.PPS(:));
fprintf(fid, '%s\n', formatStatsFull('PPS', valStats));
   

function summary_Attitude(this, fid)

[flag, DataAttitude] = read_attitude_bin(this, 'Memmapfile', 1);
if ~flag
    return
end

fprintf(fid, '\n--- Attitude datagrams ---\n');

fprintf(fid, '%s\n', formatVar('Nb NbSamples', DataAttitude.Dimensions.NbSamples));

T = DataAttitude.Datetime(:);
strTime = sprintf('%s  -  %s', char(T(1)), char(T(end)));
fprintf(fid, '%s\n', formatVar('Time', strTime));

fprintf(fid, '%s\n', strEntete');

valStats = stats(DataAttitude.Roll(:));
fprintf(fid, '%s\n', formatStatsFull('Roll', valStats));

valStats = stats(DataAttitude.Pitch(:));
fprintf(fid, '%s\n', formatStatsFull('Pitch', valStats));

valStats = stats(DataAttitude.Heave(:));
fprintf(fid, '%s\n', formatStatsFull('Heave', valStats));

valStats = stats(DataAttitude.Heading(:));
fprintf(fid, '%s\n', formatStatsLight('Heading', valStats));

valStats = stats(DataAttitude.SensorStatus(:));
fprintf(fid, '%s\n', formatStatsLight('SensorStatus', valStats));


function summary_InstallationParameters(this, fid)

[flag, DataInstallationParameters] = read_installationParameters_bin(this, 'Memmapfile', 0);
if ~flag
    return
end

fprintf(fid, '\n--- Installation Parameters datagrams ---\n');
fprintf(fid, '%s\n', formatVar('Nb Datagrams', DataInstallationParameters.Dimensions.NbDatagrams));

T = DataInstallationParameters.Datetime(:);
strTime = sprintf('%s  -  %s', char(T(1)), char(T(end)));
fprintf(fid, '%s\n', formatVar('Time', strTime));

if isfield(DataInstallationParameters, 'SystemSerialNumberSecondHead') % Ajout du if le 03/02/2022 pour KMALL-XSF
    fprintf(fid, '%s\n', formatVar('SecondHead S/N', DataInstallationParameters.SystemSerialNumberSecondHead));
end

fprintf(fid, '%s\n', strEntete');

if isfield(DataInstallationParameters, 'SurveyLineNumber') % Ajout du if le 03/02/2022 pour KMALL-XSF
    valStats = stats(DataInstallationParameters.SurveyLineNumber(:));
    fprintf(fid, '%s\n', formatStatsLight('SurveyLineNumber', valStats));
end

names = fieldnames(DataInstallationParameters);
for k=1:length(names)
    if isa(DataInstallationParameters.(names{k}), 'cl_time')
    elseif strcmp(names{k}, 'Dimensions')
    elseif strcmp(names{k}, 'Signals')
    elseif strcmp(names{k}, 'Title')
    elseif strcmp(names{k}, 'Constructor')
    elseif strcmp(names{k}, 'EmModel')
    elseif strcmp(names{k}, 'SystemSerialNumberSecondHead')
    elseif strcmp(names{k}, 'TimeOrigin')
    elseif strcmp(names{k}, 'Comments')
    elseif strcmp(names{k}, 'Datetime')
        fprintf(fid, '%s\n', formatVar(names{k}, char(DataInstallationParameters.(names{k})(1))));
    else
        fprintf(fid, '%s\n', formatVar(names{k}, DataInstallationParameters.(names{k})(:)));
    end
end


function summary_SeabedImage(this, fid)
Version = version_datagram(this);
switch Version
    case 'V1'
        summary_SeabedImage53h(this, fid)
    case 'V2'
        summary_SeabedImage59h(this, fid)
end


function summary_SeabedImage59h(this, fid)

[flag, DataSeabed] = SSc_ReadDatagrams(this.nomFic, 'Ssc_SeabedImage59h', 'Memmapfile', -1);
if ~flag
    return
end

fprintf(fid, '\n--- Seabed Image 59h datagrams ---\n');
fprintf(fid, '%s\n', formatVar('Nb Pings',   DataSeabed.Dimensions.nbPings));
fprintf(fid, '%s\n', formatVar('Nb RxBeams', DataSeabed.Dimensions.nbBeams));
fprintf(fid, '%s\n', formatVar('Nb Sonders', DataSeabed.Dimensions.nbSounders));

T = DataSeabed.Datetime(:);
strTime = sprintf('%s  -  %s', char(T(1)), char(T(end)));
fprintf(fid, '%s\n', formatVar('Time', strTime));

strPing = sprintf('%d - %d', DataSeabed.PingCounter(1), DataSeabed.PingCounter(length(DataSeabed.PingCounter)));
fprintf(fid, '%s\n', formatVar('PingCounter', strPing));

fprintf(fid, '%s\n', strEntete');

valStats = stats(DataSeabed.SystemSerialNumber(:));
fprintf(fid, '%s\n', formatStatsLight('SystemSerialNumber', valStats));

valStats = stats(DataSeabed.TVGN(:));
fprintf(fid, '%s\n', formatStatsFull('Rn', valStats));

valStats = stats(DataSeabed.BSN(:));
fprintf(fid, '%s\n', formatStatsFull('BSN', valStats));

valStats = stats(DataSeabed.BSO(:));
fprintf(fid, '%s\n', formatStatsFull('BSO', valStats));

valStats = stats(DataSeabed.TxBeamWidth(:));
fprintf(fid, '%s\n', formatStatsLight('TxBeamWidth', valStats));

valStats = stats(DataSeabed.TVGCrossOver(:));
fprintf(fid, '%s\n', formatStatsLight('TVGCrossOver', valStats));

valStats = stats(DataSeabed.SamplingRate(:));
fprintf(fid, '%s\n', formatStatsLight('SamplingRate', valStats));

valStats = stats(DataSeabed.InfoBeamSortDirection(:));
fprintf(fid, '%s\n', formatStatsLight('InfoBeamSortDirection', valStats));

valStats = stats(DataSeabed.InfoBeamNbSamples(:));
fprintf(fid, '%s\n', formatStatsFull('InfoBeamNbSamples', valStats));

valStats = stats(DataSeabed.InfoBeamCentralSample(:));
fprintf(fid, '%s\n', formatStatsFull('InfoBeamCentralSample', valStats));

valStats = stats(DataSeabed.InfoBeamDetectionInfo(:));
fprintf(fid, '%s\n', formatStatsLight('InfoBeamDetectionInfo', valStats));


function summary_SeabedImage53h(this, fid)

[flag, DataSeabed] = SSc_ReadDatagrams(this.nomFic, 'Ssc_SeabedImage53h', 'Memmapfile', -1);
if ~flag
    return
end

fprintf(fid, '\n--- Seabed Image 59h datagrams ---\n');
fprintf(fid, '%s\n', formatVar('Nb Pings',   DataSeabed.Dimensions.nbPings));
fprintf(fid, '%s\n', formatVar('Nb RxBeams', DataSeabed.Dimensions.nbBeams));
fprintf(fid, '%s\n', formatVar('Nb Sonders', DataSeabed.Dimensions.nbSounders));

T = DataSeabed.Datetime(:);
strTime = sprintf('%s  -  %s', char(T(1)), char(T(end)));
fprintf(fid, '%s\n', formatVar('Time', strTime));

strPing = sprintf('%d - %d', DataSeabed.PingCounter(1), DataSeabed.PingCounter(length(DataSeabed.PingCounter)));
fprintf(fid, '%s\n', formatVar('PingCounter', strPing));

fprintf(fid, '%s\n', strEntete');

valStats = stats(DataSeabed.SystemSerialNumber(:));
fprintf(fid, '%s\n', formatStatsLight('SystemSerialNumber', valStats));

valStats = stats(DataSeabed.MeanAbsorpCoeff(:));
fprintf(fid, '%s\n', formatStatsLight('MeanAbsorpCoeff', valStats));

valStats = stats(DataSeabed.PulseLength(:));
fprintf(fid, '%s\n', formatStatsLight('PulseLength', valStats));

valStats = stats(DataSeabed.TVGN(:));
fprintf(fid, '%s\n', formatStatsFull('Rn', valStats));

valStats = stats(DataSeabed.BSN(:));
fprintf(fid, '%s\n', formatStatsFull('BSN', valStats));

valStats = stats(DataSeabed.BSO(:));
fprintf(fid, '%s\n', formatStatsFull('BSO', valStats));

valStats = stats(DataSeabed.TxBeamWidth(:));
fprintf(fid, '%s\n', formatStatsLight('TxBeamWidth', valStats));

valStats = stats(DataSeabed.TVGCrossOver(:));
fprintf(fid, '%s\n', formatStatsLight('TVGCrossOver', valStats));

valStats = stats(DataSeabed.InfoBeamSortDirection(:));
fprintf(fid, '%s\n', formatStatsLight('InfoBeamSortDirection', valStats));

valStats = stats(DataSeabed.InfoBeamNbSamples(:));
fprintf(fid, '%s\n', formatStatsFull('InfoBeamNbSamples', valStats));

valStats = stats(DataSeabed.InfoBeamCentralSample(:));
fprintf(fid, '%s\n', formatStatsFull('InfoBeamCentralSample', valStats));

valStats = stats(DataSeabed.TVGStart(:));
fprintf(fid, '%s\n', formatStatsLight('TVGStart', valStats));

valStats = stats(DataSeabed.TVGStop(:));
fprintf(fid, '%s\n', formatStatsLight('TVGStop', valStats));

valStats = stats(DataSeabed.NbBeams(:));
fprintf(fid, '%s\n', formatStatsLight('NbBeams', valStats));

valStats = stats(DataSeabed.NbSamplesPerPing(:));
fprintf(fid, '%s\n', formatStatsFull('NbSamplesPerPing', valStats));


function summary_RawRangeBeamAngle(this, fid)

Version = version_datagram(this);
switch Version
    case 'V1'
        summary_RawRangeBeamAngle66h(this, fid)
    case 'V2'
        summary_RawRangeBeamAngle4eh(this, fid)
end


function summary_RawRangeBeamAngle66h(this, fid)

[flag, DataRawRange] = SSc_ReadDatagrams(this.nomFic, 'Ssc_RawRangeBeamAngle', 'Memmapfile', -1);
if ~flag
    return
end

fprintf(fid, '\n--- RawRangeBeamAngle 4eh datagrams ---\n');
fprintf(fid, '%s\n', formatVar('Nb Pings',   DataRawRange.Dimensions.nbPings));
fprintf(fid, '%s\n', formatVar('Nb RxBeams', DataRawRange.Dimensions.nbRx));
fprintf(fid, '%s\n', formatVar('Nb Sonders', DataRawRange.Dimensions.nbSounders));

T = DataRawRange.Datetime(:);
strTime = sprintf('%s  -  %s', char(T(1)), char(T(end)));
fprintf(fid, '%s\n', formatVar('Time', strTime));

strPing = sprintf('%d - %d', DataRawRange.PingCounter(1), DataRawRange.PingCounter(length(DataRawRange.PingCounter)));
fprintf(fid, '%s\n', formatVar('PingCounter', strPing));

fprintf(fid, '%s\n', strEntete');

% valStats = stats(DataRawRange.SystemSerialNumber(:));
% fprintf(fid, '%s\n', formatStatsLight('SystemSerialNumber', valStats));

valStats = stats(DataRawRange.SoundSpeed(:));
fprintf(fid, '%s\n', formatStatsFull('SoundSpeed', valStats));

valStats = stats(DataRawRange.BeamPointingAngle(:));
fprintf(fid, '%s\n', formatStatsFull('BeamPointingAngle', valStats));

valStats = stats(DataRawRange.TransmitTiltAngle(:));
fprintf(fid, '%s\n', formatStatsFull('TransmitTiltAngle', valStats));

valStats = stats(DataRawRange.Range(:));
fprintf(fid, '%s\n', formatStatsFull('Range', valStats));

valStats = stats(DataRawRange.Reflectivity(:));
fprintf(fid, '%s\n', formatStatsFull('Reflectivity', valStats));


function summary_RawRangeBeamAngle4eh(this, fid)
        
[flag, DataRawRange] = SSc_ReadDatagrams(this.nomFic, 'Ssc_RawRangeBeamAngle', 'Memmapfile', -1);
if ~flag
    return
end

fprintf(fid, '\n--- RawRangeBeamAngle 4eh datagrams ---\n');
fprintf(fid, '%s\n', formatVar('Nb Pings',   DataRawRange.Dimensions.nbPings));
fprintf(fid, '%s\n', formatVar('Nb TxBeams', DataRawRange.Dimensions.nbTx));
fprintf(fid, '%s\n', formatVar('Nb RxBeams', DataRawRange.Dimensions.nbRx));
fprintf(fid, '%s\n', formatVar('Nb Sonders', DataRawRange.Dimensions.nbSounders));

T = DataRawRange.Datetime(:);
strTime = sprintf('%s  -  %s', char(T(1)), char(T(end)));
fprintf(fid, '%s\n', formatVar('Time', strTime));

strPing = sprintf('%d - %d', DataRawRange.PingCounter(1), DataRawRange.PingCounter(length(DataRawRange.PingCounter)));
fprintf(fid, '%s\n', formatVar('PingCounter', strPing));

fprintf(fid, '%s\n', strEntete');

valStats = stats(DataRawRange.SystemSerialNumber(:));
fprintf(fid, '%s\n', formatStatsLight('SystemSerialNumber', valStats));

valStats = stats(DataRawRange.SoundSpeed(:));
fprintf(fid, '%s\n', formatStatsFull('SoundSpeed', valStats));

valStats = stats(DataRawRange.SamplingRate(:));
fprintf(fid, '%s\n', formatStatsLight('SamplingRate', valStats));

valStats = stats(DataRawRange.TiltAngle(:));
fprintf(fid, '%s\n', formatStatsFull('TiltAngle', valStats));

valStats = stats(DataRawRange.FocusRange(:));
fprintf(fid, '%s\n', formatStatsFull('FocusRange', valStats));

valStats = stats(DataRawRange.SignalLength(:));
fprintf(fid, '%s\n', formatStatsLight('SignalLength', valStats));

valStats = stats(DataRawRange.SectorTransmitDelay(:));
fprintf(fid, '%s\n', formatStatsLight('SectorTransmitDelay', valStats));

valStats = stats(DataRawRange.CentralFrequency(:));
fprintf(fid, '%s\n', formatStatsLight('CentralFrequency', valStats));

valStats = stats(DataRawRange.MeanAbsorptionCoeff(:));
fprintf(fid, '%s\n', formatStatsLight('MeanAbsorptionCoeff', valStats));

% Pour compenser une erreur initiale de nommage du fichier .bin (format
% V1 ?).
if isfield(DataRawRange, 'SignamWaveformIdentifier')
    valStats = stats(DataRawRange.SignamWaveformIdentifier(:));
else
    valStats = stats(DataRawRange.SignalWaveformIdentifier(:));
end
fprintf(fid, '%s\n', formatStatsLight('SignalWaveformIdentifier', valStats));

valStats = stats(DataRawRange.TransmitSectorNumberTx(:)); % TransmitSectorNumber avant, modifi� le 14/12/2012 par JMA
fprintf(fid, '%s\n', formatStatsLight('TransmitSectorNumberTx', valStats));

valStats = stats(DataRawRange.SignalBandwith(:));
fprintf(fid, '%s\n', formatStatsLight('SignalBandwith', valStats));

valStats = stats(DataRawRange.BeamPointingAngle(:));
fprintf(fid, '%s\n', formatStatsLight('BeamPointingAngle', valStats));

valStats = stats(DataRawRange.TransmitSectorNumberTx(:));
fprintf(fid, '%s\n', formatStatsLight('TransmitSectorNumberTx', valStats));

valStats = stats(DataRawRange.DetectionInfo(:));
fprintf(fid, '%s\n', formatStatsLight('DetectionInfo', valStats));

valStats = stats(DataRawRange.LengthDetection(:));
fprintf(fid, '%s\n', formatStatsLight('LengthDetection', valStats));

valStats = stats(DataRawRange.QualityFactor(:));
fprintf(fid, '%s\n', formatStatsFull('QualityFactor', valStats));

% Pour compenser un changement de nommage du fichier .bin (format V1 ?).
if isfield(DataRawRange, 'TwoWayTravelTimeInSeconds')
    valStats = stats(DataRawRange.TwoWayTravelTimeInSeconds(:)); 
else
    valStats = stats(DataRawRange.TwoWayTravelTime(:)); % TwoWayTravelTimeInSec avant, modifi� le 14/12/2012 par JMA
end
fprintf(fid, '%s\n', formatStatsLight('TwoWayTravelTime', valStats));

valStats = stats(DataRawRange.Reflectivity(:));
fprintf(fid, '%s\n', formatStatsFull('Reflectivity', valStats));

valStats = stats(DataRawRange.RealTimeCleaningInfo(:));
fprintf(fid, '%s\n', formatStatsLight('RealTimeCleaningInfo', valStats));


function summary_WaterColumn(this, fid)

[flag, DataWC] = read_WaterColumn_bin(this, 'Memmapfile', 1);
if ~flag
    return
end

fprintf(fid, '\n--- WaterColumn datagrams ---\n');
fprintf(fid, '%s\n', formatVar('Nb Pings',   DataWC.Dimensions.nbPings));
fprintf(fid, '%s\n', formatVar('Nb RxBeams', DataWC.Dimensions.nbRx));
fprintf(fid, '%s\n', formatVar('Nb Sonders', DataWC.Dimensions.nbSounders));

T = DataWC.Datetime(:);
strTime = sprintf('%s  -  %s', char(T(1)), char(T(end)));
fprintf(fid, '%s\n', formatVar('Time', strTime));

strPing = sprintf('%d - %d',   DataWC.PingCounter(1),   DataWC.PingCounter(length(DataWC.PingCounter)));
fprintf(fid, '%s\n', formatVar('PingCounter', strPing));

fprintf(fid, '%s\n', strEntete');

valStats = stats(DataWC.SoundSpeed(:) / 10); % %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Division par 10
fprintf(fid, '%s\n', formatStatsFull('SoundSpeed', valStats));

valStats = stats(DataWC.SamplingFreq(:));
fprintf(fid, '%s\n', formatStatsLight('SoundSpeed', valStats));

valStats = stats(DataWC.TVGOffset(:));
fprintf(fid, '%s\n', formatStatsLight('TVGOffset', valStats));

valStats = stats(DataWC.TotalOfReceiveBeams(:));
fprintf(fid, '%s\n', formatStatsLight('TotalRxBeams', valStats));

valStats = stats(DataWC.TiltAngle(:));
fprintf(fid, '%s\n', formatStatsFull('TiltAngle', valStats));

valStats = stats(DataWC.TxTimeHeave(:));
fprintf(fid, '%s\n', formatStatsFull('TxTimeHeave', valStats));

if isfield(DataWC, 'TxBeamNumber')
    valStats = stats(DataWC.TxBeamNumber(:));
    fprintf(fid, '%s\n', formatStatsLight('TxBeamNumber', valStats));
end
if isfield(DataWC, 'RxBeamNumber')
    valStats = stats(DataWC.RxBeamNumber(:));
    fprintf(fid, '%s\n', formatStatsLight('RxBeamNumber', valStats));
end

valStats = stats(DataWC.CentralFrequency(:));
fprintf(fid, '%s\n', formatStatsLight('CentralFreq', valStats));

valStats = stats(DataWC.BeamPointingAngle(:));
fprintf(fid, '%s\n', formatStatsLight('BeamAngle', valStats));

valStats = stats(DataWC.StartRangeNumber(:));
fprintf(fid, '%s\n', formatStatsLight('StartRangeNb', valStats));

valStats = stats(DataWC.NumberOfSamples(:));
fprintf(fid, '%s\n', formatStatsLight('NumberOfSamples', valStats));

valStats = stats(DataWC.DetectedRangeInSamples(:));
fprintf(fid, '%s\n', formatStatsLight('RangeInSamples', valStats));

valStats = stats(DataWC.TransmitSectorNumber(:));
fprintf(fid, '%s\n', formatStatsLight('TxSectorNumber', valStats));


function summary_SoundSpeedProfile(this, fid)

[flag, DataSSP] = SSc_ReadDatagrams(this.nomFic, 'Ssc_SoundSpeedProfile');
if ~flag
    return
end
fprintf(fid, '\n--- SoundSpeedProfile datagrams ---\n');
fprintf(fid, '%s\n', formatVar('Nb Profils',  DataSSP.Dimensions.nbProfiles));
fprintf(fid, '%s\n', formatVar('Nb nbPoints', DataSSP.Dimensions.nbPoints));

T = DataSSP.DatetimeProfileMeasurement(:);
strTime = sprintf('%s  -  %s', char(T(1)), char(T(end)));
fprintf(fid, '%s\n', formatVar('TimeProfileMeasurement', strTime));

T = DataSSP.TimeStartOfUse(:);
[date, heure] = timeMat2Ifr(T.timeMat);
strTime = sprintf('%s %s  -  %s % s', ...
    dayIfr2str(date(1)), hourIfr2str(heure(1)), ...
    dayIfr2str(date(end)), hourIfr2str(heure(end)));
fprintf(fid, '%s\n', formatVar('TimeStartOfUse', strTime));

fprintf(fid, '%s\n', strEntete');

valStats = stats(DataSSP.Depth(:));
fprintf(fid, '%s\n', formatStatsFull('Depth', valStats));

valStats = stats(DataSSP.SoundSpeed(:));
fprintf(fid, '%s\n', formatStatsFull('SoundSpeed', valStats));


function summary_Depth(this, fid)
[flag, DataDepth] = read_depth_bin(this, 'Memmapfile', 1);
if ~flag
    return
end

fprintf(fid, '\n--- Depth datagrams ---\n');
fprintf(fid, '%s\n', formatVar('EmModel',       DataDepth.EmModel)); % Model avant, modifi� le 14/12/2012 par JMA
fprintf(fid, '%s\n', formatVar('Serial number', DataDepth.ListeSystemSerialNumber));
fprintf(fid, '%s\n', formatVar('Nb Pings',      DataDepth.Dimensions.nbPings));
fprintf(fid, '%s\n', formatVar('Nb Beams',      DataDepth.Dimensions.nbBeams));
fprintf(fid, '%s\n', formatVar('Nb Sonders',    DataDepth.Dimensions.nbSounders));

T = DataDepth.Datetime(:);
strTime = sprintf('%s  -  %s', char(T(1)), char(T(end)));
fprintf(fid, '%s\n', formatVar('Time', strTime));

strPing = sprintf('%d - %d',   DataDepth.PingCounter(1),   DataDepth.PingCounter(length(DataDepth.PingCounter)));
fprintf(fid, '%s\n', formatVar('PingCounter', strPing));

fprintf(fid, '%s\n', strEntete');

valStats = stats(DataDepth.Heading(:));
fprintf(fid, '%s\n', formatStatsFull('Heading', valStats));

valStats = stats(DataDepth.SoundSpeed(:));
fprintf(fid, '%s\n', formatStatsFull('SoundSpeed', valStats));

valStats = stats(DataDepth.SamplingRate(:));
fprintf(fid, '%s\n', formatStatsLight('SamplingRate', valStats));

valStats = stats(DataDepth.SystemSerialNumber(:));
fprintf(fid, '%s\n', formatStatsLight('SystemSerialNumber', valStats));

valStats = stats(DataDepth.TransducerDepth(:));
fprintf(fid, '%s\n', formatStatsFull('TransducerDepth', valStats));

valStats = stats(DataDepth.Depth(:));
fprintf(fid, '%s\n', formatStatsFull('Depth', valStats));

valStats = stats(DataDepth.AcrossDist(:));
fprintf(fid, '%s\n', formatStatsLight('AcrossDist', valStats));

valStats = stats(DataDepth.AlongDist(:));
fprintf(fid, '%s\n', formatStatsLight('AlongDist', valStats));

valStats = stats(DataDepth.QualityFactor(:));
fprintf(fid, '%s\n', formatStatsFull('QualityFactor', valStats));

if isfield(DataDepth, 'BeamIBA')
    valStats = stats(DataDepth.BeamIBA(:));
    fprintf(fid, '%s\n', formatStatsFull('BeamIBA', valStats));
end

valStats = stats(DataDepth.Reflectivity(:));
fprintf(fid, '%s\n', formatStatsFull('Reflectivity', valStats));

valStats = stats(DataDepth.LengthDetection(:));
fprintf(fid, '%s\n', formatStatsFull('LengthDetection', valStats));

if isfield(DataDepth, 'BeamDepressionAngle')
    valStats = stats(DataDepth.BeamDepressionAngle(:));
    fprintf(fid, '%s\n', formatStatsFull('BeamDepressionAngle', valStats));
end

if isfield(DataDepth, 'BeamAzimuthAngle')
    valStats = stats(DataDepth.BeamAzimuthAngle(:));
    fprintf(fid, '%s\n', formatStatsFull('BeamAzimuthAngle', valStats));
end

if isfield(DataDepth, 'Range')
    valStats = stats(DataDepth.Range(:));
    fprintf(fid, '%s\n', formatStatsFull('Range', valStats));
end


function summary_Runtime(this, fid)

[flag, DataRuntime] = SSc_ReadDatagrams(this.nomFic, 'Ssc_Runtime');
if ~flag
    return
end

fprintf(fid, '\n--- Runtime datagrams ---\n');

fprintf(fid, '%s\n', formatVar('Nb NbSamples', DataRuntime.Dimensions.NbSamples));

T = DataRuntime.Datetime(:);
strTime = sprintf('%s  -  %s', char(T(1)), char(T(end)));
fprintf(fid, '%s\n', formatVar('Time', strTime));

strPing = sprintf('%d - %d', DataRuntime.PingCounter(1), DataRuntime.PingCounter(length(DataRuntime.PingCounter)));
fprintf(fid, '%s\n', formatVar('PingCounter', strPing));

fprintf(fid, '%s\n', strEntete');

valStats = stats(DataRuntime.Mode(:));
fprintf(fid, '%s\n', formatStatsLight('Mode', valStats));

valStats = stats(DataRuntime.DualSwath(:));
fprintf(fid, '%s\n', formatStatsLight('DualSwath', valStats));

valStats = stats(DataRuntime.FilterIdentifier(:));
fprintf(fid, '%s\n', formatStatsLight('FilterIdentifier', valStats));
fprintf(fid, '%s\n', interpretation_runtime(DataRuntime.EmModel, 'FilterIdentifier', unique(DataRuntime.FilterIdentifier(:))));

valStats = stats(DataRuntime.MinimumDepth(:));
fprintf(fid, '%s\n', formatStatsLight('MinimumDepth', valStats));

valStats = stats(DataRuntime.MaximumDepth(:));
fprintf(fid, '%s\n', formatStatsLight('MaximumDepth', valStats));

valStats = stats(DataRuntime.AbsorptionCoeff(:));
fprintf(fid, '%s\n', formatStatsFull('AbsorptionCoeff', valStats));

valStats = stats(DataRuntime.TransmitPulseLength(:));
fprintf(fid, '%s\n', formatStatsLight('TxPulseLength', valStats));

valStats = stats(DataRuntime.TransmitBeamWidth(:));
fprintf(fid, '%s\n', formatStatsLight('TxBeamWidth', valStats));

valStats = stats(DataRuntime.TransmitPowerMax(:));
fprintf(fid, '%s\n', formatStatsLight('TxPowerMax', valStats));

valStats = stats(DataRuntime.ReceiveBeamWidth(:));
fprintf(fid, '%s\n', formatStatsLight('RxBeamWidth', valStats));

valStats = stats(DataRuntime.ReceiveBandWidth(:));
fprintf(fid, '%s\n', formatStatsLight('RxBandWidth', valStats));

valStats = stats(DataRuntime.ReceiverFixGain(:));
fprintf(fid, '%s\n', formatStatsLight('RxFixGain', valStats));

% Stats enti�res ici ?
valStats = stats(DataRuntime.TVGCrossAngle(:));
fprintf(fid, '%s\n', formatStatsLight('TVGCrossAngle', valStats));

valStats = stats(DataRuntime.SourceSoundSpeed(:));
fprintf(fid, '%s\n', formatStatsFull('SourceSoundSpeed', valStats));
fprintf(fid, '%s\n', interpretation_runtime(DataRuntime.EmModel, 'SourceSoundSpeed', unique(DataRuntime.SourceSoundSpeed(:))));

valStats = stats(DataRuntime.MaximumPortSwath(:));
fprintf(fid, '%s\n', formatStatsLight('MaximumPortSwath', valStats));

valStats = stats(DataRuntime.BeamSpacing(:));
fprintf(fid, '%s\n', formatStatsLight('BeamSpacing', valStats));
fprintf(fid, '%s\n', interpretation_runtime(DataRuntime.EmModel, 'BeamSpacing', unique(DataRuntime.BeamSpacing(:))));

valStats = stats(DataRuntime.MaximumPortCoverage(:));
fprintf(fid, '%s\n', formatStatsLight('MaxPortCoverage', valStats));

valStats = stats(DataRuntime.MaximumStarboardCoverage(:));
fprintf(fid, '%s\n', formatStatsLight('MaxStarCoverage', valStats));

valStats = stats(DataRuntime.MaximumStarboardSwath(:));
fprintf(fid, '%s\n', formatStatsLight('MaxStarSwath', valStats));

valStats = stats(DataRuntime.YawStabMode(:));
fprintf(fid, '%s\n', formatStatsLight('YawStabMode', valStats));
fprintf(fid, '%s\n', interpretation_runtime(DataRuntime.EmModel, 'YawStabMode', unique(DataRuntime.YawStabMode(:))));

valStats = stats(DataRuntime.PitchStabMode(:));
fprintf(fid, '%s\n', formatStatsLight('PitchStabMode', valStats));
fprintf(fid, '%s\n', interpretation_runtime(DataRuntime.EmModel, 'PitchStabMode', unique(DataRuntime.PitchStabMode(:))));

valStats = stats(DataRuntime.OperatorStationStatus(:));
fprintf(fid, '%s\n', formatStatsLight('OperatorStationStatus', valStats));
fprintf(fid, '%s\n', interpretation_runtime(DataRuntime.EmModel, 'OperatorStationStatus', unique(DataRuntime.OperatorStationStatus(:))));


%{
        ProcessingUnitStatus: [1003x1 cl_memmapfile]
                   BSPStatus: [1003x1 cl_memmapfile]
             SonarHeadStatus: [1003x1 cl_memmapfile]
%}


function summary_SurfaceSoundSpeed(this, fid)
[flag, DataSurfaceSoundSpeed] = SSc_ReadDatagrams(this.nomFic, 'Ssc_SurfaceSoundSpeed');
if ~flag
    return
end

fprintf(fid, '\n--- SurfaceSoundSpeed datagrams ---\n');

fprintf(fid, '%s\n', formatVar('Nb NbSamples', DataSurfaceSoundSpeed.Dimensions.NbSamples));

T = DataSurfaceSoundSpeed.Datetime(:);
strTime = sprintf('%s  -  %s', char(T(1)), char(T(end)));
fprintf(fid, '%s\n', formatVar('Time', strTime));

fprintf(fid, '%s\n', strEntete');

valStats = stats(DataSurfaceSoundSpeed.SurfaceSoundSpeed(:));
fprintf(fid, '%s\n', formatStatsLight('SurfaceSoundSpeed', valStats));


function strStats = formatStatsFull(Nom, valStats)

strStats = repmat(' ', 1, 110);
strStats(1:length(Nom)) = Nom;
strStats(24) = ':';
str = num2str(valStats.Moyenne);
strStats(26:26+length(str)-1) = str;
str = num2str(valStats.Sigma);
strStats(40:40+length(str)-1) = str;
str = num2str(valStats.Mediane);
strStats(54:54+length(str)-1) = str;
str = num2str(valStats.Min);
strStats(68:68+length(str)-1) = str;
str = num2str(valStats.Max);
strStats(82:82+length(str)-1) = str;
str = num2str(valStats.Range);
strStats(96:96+length(str)-1) = str;


function strStats = formatStatsLight(Nom, valStats)

strStats = repmat(' ', 1, 110);
strStats(1:length(Nom)) = Nom;
strStats(24) = ':';
str = num2str(valStats.Moyenne); % '---------';
strStats(26:26+length(str)-1) = str;
str = num2str(valStats.Sigma); % '---------';
strStats(40:40+length(str)-1) = str;
str = num2str(valStats.Mediane);
strStats(54:54+length(str)-1) = str;
str = num2str(valStats.Min);
strStats(68:68+length(str)-1) = str;
str = num2str(valStats.Max);
strStats(82:82+length(str)-1) = str;
str = num2str(valStats.Range); % '---------';
strStats(96:96+length(str)-1) = str;


function str = strEntete

str = repmat(' ', 1, 100);
str(1:4) = 'Data';
str(24) = ':';
str(26:29) = 'Mean';
str(40:42) = 'Std';
str(54:59) = 'Median';
str(68:70) = 'Min';
str(82:84) = 'Max';
str(96:100) = 'Range';


function str = formatVar(Nom, val)
str = repmat(' ', 1, 110);
str(1:length(Nom)) = Nom;
str(24) = ':';
if ischar(val)
    strVal = val;
else
    val = val(:);
    strVal = num2str(val');
end
str(26:26+length(strVal)-1) = strVal;


function summary_StaveData(this, fid)

% [flag, x] = read_StaveData_bin(this, 'Memmapfile', 1);
[flag, x] = SSc_ReadDatagrams(this.nomFic, 'Ssc_StaveData', 'Memmapfile', -1); % NON TESTE
if ~flag
    return
end

fprintf(fid, '\n--- StaveData datagrams ---\n');
fprintf(fid, '%s\n', formatVar('Nb Pings',     x.Dimensions.nbPings));
fprintf(fid, '%s\n', formatVar('Nb Samples',   x.Dimensions.nbSamples));
fprintf(fid, '%s\n', formatVar('Nb Staves',    x.Dimensions.nbStaves));
fprintf(fid, '%s\n', formatVar('Nb Pings',     x.Dimensions.nbPings));
fprintf(fid, '%s\n', formatVar('Nb Datagrams', x.Dimensions.nbDatagrams));
fprintf(fid, '%s\n', formatVar('Nb Sonders',   x.Dimensions.nbSounders));
    
T = x.Datetime(:);
strTime = sprintf('%s  -  %s', char(T(1)), char(T(end)));
fprintf(fid, '%s\n', formatVar('Time', strTime));

strPing = sprintf('%d - %d',   x.PingCounter(1),   x.PingCounter(length(x.PingCounter)));
fprintf(fid, '%s\n', formatVar('PingCounter', strPing));

fprintf(fid, '%s\n', strEntete');

valStats = stats(x.NumberOfDatagrams(:));
fprintf(fid, '%s\n', formatStatsLight('NumberOfDatagrams', valStats));

valStats = stats(x.DatagramNumbers(:));
fprintf(fid, '%s\n', formatStatsLight('DatagramNumbers', valStats));

valStats = stats(x.RxSamplingFreq(:));
fprintf(fid, '%s\n', formatStatsLight('RxSamplingFreq', valStats));

valStats = stats(x.SoundSpeed(:));
fprintf(fid, '%s\n', formatStatsFull('SoundSpeed', valStats));

valStats = stats(x.StartSampleRef1TxPulse(:));
fprintf(fid, '%s\n', formatStatsFull('StartSampleRef1TxPulse', valStats));

valStats = stats(x.TotalSamplesPerStave(:));
fprintf(fid, '%s\n', formatStatsFull('TotalSamplesPerStave', valStats));

valStats = stats(x.NbSamples(:));
fprintf(fid, '%s\n', formatStatsLight('NbSamples', valStats));

valStats = stats(x.StaveNumber(:));
fprintf(fid, '%s\n', formatStatsLight('StaveNumber', valStats));

valStats = stats(x.NbStavesPerHead(:));
fprintf(fid, '%s\n', formatStatsLight('NbStavesPerHead', valStats));

valStats = stats(x.RangeToNormalIncidence(:));
fprintf(fid, '%s\n', formatStatsLight('RangeToNormalIncidence', valStats));

% valStats = stats(x.SampleNumber(:));
% fprintf(fid, '%s\n', formatStatsLight('SampleNumber', valStats));

valStats = stats(x.TVGGain(:));
fprintf(fid, '%s\n', formatStatsLight('TVGGain', valStats));

% valStats = stats(x.StaveBackscatter(:));
% fprintf(fid, '%s\n', formatStatsLight('StaveBackscatter', valStats));


function summary_ExtraParameters(this, fid)

[flag, x] = read_ExtraParameters_bin(this);
if ~flag
    return
end

fprintf(fid, '\n--- ExtraParameters datagrams ---\n');
if isfield(x, 'BsCorr')
    for k1=1:length(x.BsCorr)
        fprintf(fid, 'BsCorr(%d) ----------\n', k1);
        fprintf(fid, '    pingMode  : %d\n', x.BsCorr(k1).pingMode);
        fprintf(fid, '    swathMode : %d\n', x.BsCorr(k1).swathMode);
        fprintf(fid, '    nbSectors : %d\n', x.BsCorr(k1).nbSectors);
        for k2=1:x.BsCorr(k1).nbSectors
            if isfield(x.BsCorr(k1).Sector(k2), 'nbNodes')
                fprintf(fid, '    Sector(%d) ----------\n', k2);
                fprintf(fid, '        Label   : %s\n', x.BsCorr(k1).Sector(k2).Label);
                fprintf(fid, '        SL      : %f\n', x.BsCorr(k1).Sector(k2).SL);
                fprintf(fid, '        nbNodes : %d\n', x.BsCorr(k1).Sector(k2).nbNodes);
                for k3=1:x.BsCorr(k1).Sector(k2).nbNodes
                    fprintf(fid, '        Node(%d,:)  : %f  %f\n', k3, x.BsCorr(k1).Sector(k2).Node(k3,1), x.BsCorr(k1).Sector(k2).Node(k3,2));
                end
            end
        end
    end
end
