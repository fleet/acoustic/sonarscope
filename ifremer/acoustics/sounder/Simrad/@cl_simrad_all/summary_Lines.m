function [flag, Values, strValues] = summary_Lines(this, varargin)

global isUsingParfor %#ok<GVMIS>
global logFileId %#ok<GVMIS>

[varargin, isUsingParfor] = getPropertyValue(varargin, 'isUsingParfor', 0);
[varargin, logFileId]     = getPropertyValue(varargin, 'logFileId',     logFileId);
[varargin, IndNavUnit]    = getPropertyValue(varargin, 'IndNavUnit',    []); %#ok<ASGLU>

% logFileId.info('summary_Lines', this.nomFic);

strValues = {};
Values    = {};

%% Version des datagrammes

[Version, SousVersion] = version_datagram(this);

%% TODO : lecture globale plus rapide mais pr�voir des flag (pex : pas de lecture de DataWC, ...)

% [flag, All, DataDepth, DataRaw, DataWC, DataSeabed, DataPosition, DataAttitude, DataRuntime, DataInstalParam, DataSSP, ...
%     subDepth, subRaw, subSeabed, subWC, FlagPings, Latitude, Longitude, Heading, tWC] = lectureGlobaleALL(this.nomFic, IndNavUnit);

%% Lecture de runtime

[flag, DataRuntime] = SSc_ReadDatagrams(this.nomFic, 'Ssc_Runtime');
if ~flag
    return
end

%% Lecture de read_rawRangeBeamAngle

[flag, DataRaw] = read_rawRangeBeamAngle(this, 'Memmapfile', -1);
if ~flag
    return
end

%% Lecture Seabed image

[~, DataSeabed] = read_seabedImage_bin(this, 'Memmapfile', -1);

%% Lecture de Height

[~, DataHeight] = SSc_ReadDatagrams(this.nomFic, 'Ssc_Height');

%% Lecture de Attitude

[flag, DataAttitude, nbSensorsAttitude, indSystemUsedAttitude] = read_attitude_bin(this, 'Memmapfile', 0);
if flag
    valStats = stats(DataAttitude.Roll(:));
    RollMean = valStats.Moyenne;
    RollStd  = valStats.Sigma;
    
    valStats  = stats(DataAttitude.Pitch(:));
    PitchMean = valStats.Moyenne;
    PitchStd  = valStats.Sigma;
    
    valStats  = stats(DataAttitude.Heave(:));
    HeaveMean = valStats.Moyenne;
    HeaveStd  = valStats.Sigma;
    
    valStats = stats(DataAttitude.Heading(:));
    HeadingMedian = valStats.Mediane;
    
    typeLine      = ''; % Calcul� plus loin
    strDirection  = '';
else % Cas des fichiers NIWA hydromapes
    RollMean      = NaN;
    RollStd       = NaN;
    PitchMean     = NaN;
    PitchStd      = NaN;
    HeaveMean     = NaN;
    HeaveStd      = NaN;
    HeadingMedian = NaN;
    typeLine      = '';
    strDirection  = '';
end

%% Tide

% flagRTK = this.flagRTK;
if isfield(DataAttitude, 'Tide')
    Tide = mean(DataAttitude.Tide, 'omitnan');
else
    Tide = [];
end

%% Lecture de Depth

Option = [];
[flag, DataDepth] = read_depth_bin(this, Option, 'Memmapfile', -1);
if ~flag
    return
end

%% Recherche de la zone commune

% FigUtils.createSScFigure; PlotUtils.createSScPlot(DataDepth.PingCounter(:,1), '.b'); grid on; hold on; PlotUtils.createSScPlot(DataRaw.PingCounter(:,1), '.r'); PlotUtils.createSScPlot(DataSeabed.PingCounter(:,1), '.k');
[subDepth, subRaw, subSeabed] = intersect3(DataDepth.PingCounter(:,1), DataRaw.PingCounter(:,1), DataSeabed.PingCounter(:,1));
DataDepth_PingCounter = DataDepth.PingCounter(subDepth);

[flag, Z] = create_signal_Immersion(this, 'DataDepth', DataDepth, 'DataAttitude', DataAttitude, 'subDepth', subDepth);
if flag == 1
    MeanImmersion = mean(Z.Data(:), 'omitnan');
else
    MeanImmersion = 0;
end

if isempty(DataSeabed)
    MeanBSO = [];
    MeanBSN = [];
else
    valStats = stats(DataSeabed.BSN(subSeabed));
    MeanBSN  = valStats.Moyenne;
    valStats = stats(DataSeabed.BSO(subSeabed));
    MeanBSO  = valStats.Moyenne;
end

Mask  = DataDepth.Mask(subDepth,:);
Depth = DataDepth.Depth(subDepth,:);

% if isfield(DataDepth, 'DetectionInfo')
if strcmp(Version, 'V2')
    DetectionType = DataDepth.DetectionInfo(subDepth,:);
    DetectionType(DetectionType >=128) = NaN;
    DetectionType = DetectionType + 1; % 1=Amplitude, 2=Phase
else
    DetectionType = DataDepth.QualityFactor(subDepth,:);
    DetectionType = single((DetectionType >= 128)) + 1;
end

flagValidDepth = ~isnan(Depth);
flagValidMask  = (Mask == 0);
clear Mask
% PerCentValidCleanedSoundings  = 100 * sum(flagValidDepth & flagValidMask) / numel(Depth);
PerCentValidCleanedSoundings  = 100 * sum(~flagValidMask(:)) / sum(flagValidDepth(:));
PerCentValidRawSoundings      = 100 * sum(flagValidDepth(:)) / numel(Depth);
PerCentAmplitudeDetections    = 100 * sum(DetectionType(flagValidDepth(:)) == 1) / sum(flagValidDepth(:));

Depth(~flagValidMask | ~flagValidDepth) = NaN;

Depth = Depth(flagValidMask & flagValidDepth);
valStats = stats(Depth);
clear Depth

AltitudetOverSeabed = valStats.Moyenne;
MeanDepth   = AltitudetOverSeabed + MeanImmersion;
MinDepth    = valStats.Min + MeanImmersion;
MaxDepth    = valStats.Max + MeanImmersion;
StdDepth    = valStats.Sigma;
EmModel     = DataDepth.EmModel;
nbSoundings = DataDepth.Dimensions.nbBeams;
nbPings     = length(subDepth);
SamplingFrequencyBathy = mode(DataDepth.SamplingRate(subDepth));

X = DataDepth.AcrossDist(subDepth,:);
XBab = min(X, [], 2);
XTri = max(X, [], 2);
% FigUtils.createSScFigure; PlotUtils.createSScPlot(-XBab, 'r'); hold on; grid on; plot(XTri, 'g');
PingRange = XTri - XBab;
SwathWidth = median(PingRange, 'omitnan');

X = DataDepth.ReflectivityFromSnippets(subDepth,:);
BSMean = mean(X(:), 'omitnan');

X = DataDepth.AcrossDist(subDepth,:);
X = X(flagValidMask & flagValidDepth);
valStats = stats(X);
DistanceInterBeams = (valStats.Max - valStats.Min) / (DataDepth.Dimensions.nbBeams);

%% Lecture de DataRaw

X = DataRaw.BeamPointingAngle(subRaw,:);
AngularRange = median(max(X, [], 2) - min(X, [], 2), 'omitnan');

if strcmp(Version, 'V2')
    X = DataRaw.MeanAbsorptionCoeff(subRaw,:);
    AlphaMedian = median(X(:), 'omitnan');
else
    if isfield(DataDepth, 'AbsorptionCoefficientRT')
        X = DataDepth.AbsorptionCoefficientRT(subDepth,:);
        AlphaMedian = mean(X(:), 'omitnan');
    else
        AlphaMedian = [];
    end
end
SwathOnDepth = SwathWidth / abs(MeanDepth);

SurfaceSoundSpeed = median(DataRaw.SoundSpeed(subRaw), 'omitnan');

%% Lecture de la navigation

[flag, DataPosition, nbSensorsPosition, indSystemUsedPosition] = read_position_bin(this, 'IndNavUnit', IndNavUnit, 'Memmapfile', 0);
if ~flag
    return
end
[flag, DIP, speedMpS, speedNds, LengthLine] = create_signal_InterPingDistance(this, 'DataDepth', DataDepth, ...
    'DataPosition', DataPosition, 'subDepth', subDepth, 'IndNavUnit', IndNavUnit);
if flag
    DistanceInterPings = median(DIP.Data, 'omitnan');
    LineArea = sum(sum(PingRange .* DIP.Data, 'omitnan'));
else
    DistanceInterPings = [];
    LineArea           = NaN;
end

LatMedian = median(DataPosition.Latitude(:), 'omitnan');
LonMedian = median(DataPosition.Longitude(:), 'omitnan');

if isfield(DataPosition, 'QualityIndicator') && ~isempty(DataHeight)
    flagPingsRTK = (DataPosition.QualityIndicator(:) == 4) | (DataPosition.QualityIndicator(:) == 5);
    PourcentagePingsRTK = 100 * sum(flagPingsRTK) / length(flagPingsRTK);
    X = mode(DataPosition.QualityIndicator(:));
    GPSQualityIndicator = X;
    strGPSQualityIndicator = {'GPS fix (SPS)'; 'DGPS fix'; 'PPS fix'; 'Real Time Kinematic'; 'Float RTK'; 'estimated (dead reckoning)'; 'Manual input mod'; 'Simulation mode'};
    %{
http://www.gpsinformation.org/dale/nmea.htm
0 = invalid
1 = GPS fix (SPS)
2 = DGPS fix
3 = PPS fix
4 = Real Time Kinematic
5 = Float RTK
6 = estimated (dead reckoning) (2.3 feature)
7 = Manual input mode
8 = Simulation mode
    %}
else
    GPSQualityIndicator    = [];
    strGPSQualityIndicator = {};
    PourcentagePingsRTK    = 0;
end
if isfield(DataPosition, 'GPSTime')
    diffTime = DataPosition.Datetime - DataPosition.GPSDatetime;
    subOutOfRange = isnan(diffTime) | (abs(diffTime) > (12*3600*1000));
    diffTime(subOutOfRange) = [];
    PU_GGA = milliseconds(mean(diffTime));
else
    PU_GGA = [];
end

 if ~isnan(HeadingMedian)   
    [typeLine, strDirection] = classifyProfileFromHeading(DataAttitude.Heading(:), ...
        'DistanceInterPings', DistanceInterPings, 'DistanceInterBeams', DistanceInterBeams);
 end

%% Lecture de WC

[flag, DataWC] = read_WaterColumn_bin(this, 'Memmapfile', -1);
if flag
    nbBeamsWC = DataWC.Dimensions.nbRx;
    SamplingFrequencyWC = mode(DataWC.SamplingFreq(:));
    TVGFunctionApplied  = mode(DataWC.TVGFunctionApplied(:));
    TVGOffset           = mode(DataWC.TVGOffset(:));
else
    nbBeamsWC = 0;
    SamplingFrequencyWC = [];
    TVGFunctionApplied  = [];
    TVGOffset           = [];
end

%% Lecture des StaveData

% [~, DataStave] = read_StaveData_bin(this, 'Memmapfile', 1);
[~, DataStave] = SSc_ReadDatagrams(this.nomFic, 'Ssc_StaveData', 'Memmapfile', -1); % NON TESTE

%% Lecture des MagAndPhase

[~, DataMagPhase] = SSc_ReadDatagrams(this.nomFic, 'Ssc_RawBeamSamples', 'Memmapfile', -1);

%% Lecture de soundSpeedProfile

[flag, DataSSP] = SSc_ReadDatagrams(this.nomFic, 'Ssc_SoundSpeedProfile');
if flag
    nbSoundSpeedProfile = DataSSP.Dimensions.nbProfiles;
else
    nbSoundSpeedProfile = 0;
end

%% Lecture du facteur de qualit�

[flag, DataQualityFactor] = SSc_ReadDatagrams(this.nomFic, 'Ssc_QualityFactorFromIfr');
if flag
    [subsubDepth, subQF] = intersect3(DataDepth.PingCounter(subDepth,1), DataQualityFactor.PingCounter(:,1), DataQualityFactor.PingCounter(:,1));
    QF = DataQualityFactor.IfremerQF(subQF,:);
    QFAmp   = QF(DetectionType(subsubDepth,:) == 1);
    QFPhase = QF(DetectionType(subsubDepth,:) == 2);
    QFMeanAmp   = mean(QFAmp(:), 'omitnan');
    QFMeanPhase = mean(QFPhase(:), 'omitnan');
else
    QFMeanAmp   = [];
    QFMeanPhase = [];
end

%% Lecture des param�tres d'installation

[flag, DataInstallationParameters] = read_installationParameters_bin(this);
if flag && isfield(DataInstallationParameters, 'OSV')
    OSV = DataInstallationParameters.OSV;
else
    OSV = [];
end
if flag && isfield(DataInstallationParameters, 'PSV')
    PSV = DataInstallationParameters.PSV;
else
    PSV = [];
end
if flag && isfield(DataInstallationParameters, 'SID')
    SID = DataInstallationParameters.SID;
else
    SID = [];
end

%% D�calage temporels entre PU et horloge bord

[flag, DataClock] = SSc_ReadDatagrams(this.nomFic, 'Ssc_Clock');
if flag
    PU_ZBA = milliseconds(mean(DataClock.Datetime - DataClock.ExternalDatetime, 'omitnan'));
else
    PU_ZBA = NaN;
end
clear DataClock

%% Cr�ation des signaux

if strcmp(Version, 'V2')
    [flag, presenceFM] = create_signal_presenceFM(this, 'DataRaw', DataRaw, 'subRaw', subRaw);
    if ~flag
        return
    end
    presenceFM = presenceFM.Data(:,:);
    
    [~, presenceWC] = create_signal_presenceWC(this, 'DataDepth', DataDepth, 'DataWC', DataWC, 'subDepth', subDepth);
    presenceWC = presenceWC.Data(:,:);
    
    [~, nbSwath] = create_signal_nbSwath(this, 'DataRaw', DataRaw, 'subRaw', subRaw);
    nbSwath = nbSwath.Data(:,:);
    
    [~, presenceStaveData] = create_signal_presenceStaveData(this, 'DataDepth', DataDepth, 'DataStave', DataStave, 'subDepth', subDepth);
    presenceStaveData = presenceStaveData.Data(:,:);
    
    % TODO : Le 12/06/2017
    [~, presenceMagAndPhase] = create_signal_presenceMagAndPhase(this, 'DataDepth', DataDepth, 'DataMagPhase', ...
        DataMagPhase, 'subDepth', subDepth);
    presenceMagAndPhase = presenceMagAndPhase.Data(:,:);
else
    presenceFM          = zeros(length(subDepth),1);
    presenceWC          = zeros(length(subDepth),1);
    presenceStaveData   = zeros(length(subDepth),1);
    presenceMagAndPhase = zeros(length(subDepth),1);
    nbSwath             = ones(length(subDepth),1);
end
nbPingsWithWC       = sum(presenceWC == 1);
nbPingsWithoutWC    = sum(presenceWC == 0);
nbPingsWithFM       = sum(presenceFM == 1);
nbPingsWithoutFM    = sum(presenceFM == 0);
nbPingsSingleSwaths = sum(nbSwath == 1);
nbPingsDoubleSwaths = sum(nbSwath == 2);

[flag, Z] = create_signal_Mode(this, 'DataDepth', DataDepth, 'DataRunTime', DataRuntime, 'subDepth', subDepth);
if flag == 1
    Mode      = Z.Data;
    strModes  = Z.strData;
    HistModes = my_hist(Mode, 0:8);
else
    Mode = [];
end

%% RecurrenceInterPings

t = DataDepth.Datetime(subDepth);
subDualSwath = find(nbSwath == 2);
subDualSwath(2:2:end) = [];
t(subDualSwath) = [];
RecurrenceInterPings = t(2:end) - t(1:(end-1));
RecurrenceInterPings = median(RecurrenceInterPings);

%% Pulse length

if isfield(DataRaw, 'SignalLength')
    PulseLengthTotal = mode(min(DataRaw.SignalLength(subRaw,:), [], 2));
    NyquistWC        = SamplingFrequencyWC    * PulseLengthTotal;
    NyquistBathy     = SamplingFrequencyBathy * PulseLengthTotal;
elseif isfield(DataRuntime, 'TransmitPulseLength')
    PulseLengthTotal = mode(DataRuntime.TransmitPulseLength) * 1e-3;
    NyquistWC        = PulseLengthTotal * NaN;
    NyquistBathy     = PulseLengthTotal * SamplingFrequencyBathy;
else
    PulseLengthTotal  = [];
    NyquistWC    = [];
    NyquistBathy = [];
end


%% Lecture de la donnee "InstallationParameters"

[~, DataInstallationParameters, isDual] = read_installationParameters_bin(this, 'Memmapfile', 0);

%% Cr�ation des signaux provenant des datagrammes RawRange

[~, signalContainerFromRawRangle] = create_signalsFromRawRange(this, DataInstallationParameters, 'DataRaw', DataRaw);

% PulseLength_UsedByKMinIA = signalContainerFromRawRangle.getSignalByTag('PulseLength_UsedByKMinIA');
% if isempty(PulseLength_UsedByKMinIA)
%     PulseLength_UsedByKMinIA = NaN;
% else
%     PulseLength_UsedByKMinIA = PulseLength_UsedByKMinIA.ySample.data;
% end

PulseLength_EffectiveSignal = signalContainerFromRawRangle.getSignalByTag('PulseLength_Effective');
if isempty(PulseLength_EffectiveSignal)
    PulseLength_Effective = NaN;
else
%     PulseLength_Effective = PulseLength_EffectiveSignal.ySample.data; % TODO MHO : ceci �tait une erreur
    [~, PulseLength_Effective] = PulseLength_EffectiveSignal.getValueMatrix(); % modif JMA le 29/04/2020
end

%% Cr�ation des signaux provenant des datagrammes runtime

TsubDepth = DataDepth.Datetime(subDepth);
[~, signalContainerFromRuntime] = create_signalsFromRuntime(this, TsubDepth, 'DataRuntime', DataRuntime);

PulseLength_UsedByKMinIASignal = signalContainerFromRuntime.getSignalByTag('PulseLength_UsedByKMinIA');
if isempty(PulseLength_UsedByKMinIASignal)
    PulseLength_UsedByKMinIA = NaN;
else
%     PulseLength_UsedByKMinIA = PulseLength_UsedByKMinIASignal.ySample.data; % c'�tait correct mais modif pour �tre d'�querre avec PulseLength_Effective
    [~, PulseLength_UsedByKMinIA] = PulseLength_UsedByKMinIASignal.getValueMatrix(); % modif JMA le 29/04/2020
end

penetrationFilterSignal  = signalContainerFromRuntime.getSignalByTag('PenetrationFilter');
interferenceFilterSignal = signalContainerFromRuntime.getSignalByTag('InterferenceFilter');
aerationFilterSignal     = signalContainerFromRuntime.getSignalByTag('AerationFilter');
sectorTrackingSignal     = signalContainerFromRuntime.getSignalByTag('SectorTracking');
spikeFilterSignal        = signalContainerFromRuntime.getSignalByTag('SpikeFilter');
rangeGatesSignal         = signalContainerFromRuntime.getSignalByTag('RangeGates');
phaseRampSignal          = signalContainerFromRuntime.getSignalByTag('PhaseRamp');
slopeFilterSignal        = signalContainerFromRuntime.getSignalByTag('SlopeFilter');
detectionModeSignal      = signalContainerFromRuntime.getSignalByTag('DetectionMode');

pulseLengthConfSignal = signalContainerFromRuntime.getSignalByTag('PulseLengthConf');
% [Mode3_EM2040, strMode3] = synthesize(pulseLengthConfSignal);
if isempty(pulseLengthConfSignal)
    Mode3_EM2040 = [];
else
    Mode3_EM2040 = pulseLengthConfSignal.ySample.data;
    strMode3     = pulseLengthConfSignal.ySample.strIndexList;
end

%% Cr�ation Mode2

[flag, Z] = create_signal_Mode2_EM2040(this, Mode, 'DataDepth', DataDepth, 'DataRaw', DataRaw, ...
    'subDepth', subDepth, 'subRaw', subRaw);
if flag == 1
    Mode2_EM2040 = Z.Mode2;
    strMode2     = Z.strData;
else
    Mode2_EM2040 = [];
end

[nomDir, nomFic, Ext] = fileparts(get(this, 'nomFic'));

%% File name

X = nomDir;
Values{end+1}    = X;
strValues{end+1} = X;

X = [nomFic, Ext];
Values{end+1}    = X;
strValues{end+1} = X;

%% EM model

X = EmModel;
Values{end+1}    = X;
strValues{end+1} = num2str(X);

%% Mode

X = mode(Mode);
Values{end+1}    = X;
strValues{end+1} = num2str(X);
if isnan(X) % rencontr� avec fichier G:\DONNEES\LEVE_SMF\0242_20160206_185008_ATL.all
    Values{end+1}    = 'NaN';
    strValues{end+1} = 'NaN';
else
    Values{end+1}    = strModes{X};
    strValues{end+1} = strModes{X};
end

%% Modes suppl�mentaires pour EM2040

if isempty(Mode2_EM2040)
    X = 0;
    Values{end+1}    = X;
    strValues{end+1} = num2str(X);
    Values{end+1}    = '';
    strValues{end+1} = '';
else
    X = mode(Mode2_EM2040);
    if isnan(X)
        X = 0;
        Values{end+1}    = X;
        strValues{end+1} = num2str(X);
        Values{end+1}    = '';
        strValues{end+1} = '';
    else
        Values{end+1}    = X;
        strValues{end+1} = num2str(X);
        Values{end+1}    = strMode2{X};
        strValues{end+1} = strMode2{X};
    end
end

if isempty(Mode3_EM2040)
    X = 0;
    Values{end+1}    = X;
    strValues{end+1} = num2str(X);
    Values{end+1}    = '';
    strValues{end+1} = '';
else
    X = mode(Mode3_EM2040);
    if isnan(X)
        X = 0;
        Values{end+1}    = X;
        strValues{end+1} = num2str(X);
        Values{end+1}    = '';
        strValues{end+1} = '';
    else
        Values{end+1}    = X;
        strValues{end+1} = num2str(X);
        Values{end+1}    = strMode3{X};
        strValues{end+1} = strMode3{X};
    end
end

%% Nb swaths

X = mode(nbSwath);
Values{end+1}    = X;
strValues{end+1} = num2str(X);

%% Presence FM

X = mode(presenceFM);
Values{end+1}    = X;
strValues{end+1} = num2str(X);

%% Presence WC

X = max(presenceWC);
Values{end+1}    = X;
strValues{end+1} = num2str(X);

%% Ping counter

X = nbPings;
Values{end+1}    = X;
strValues{end+1} = num2str(X);

%% Number of soundings (beam number in depth datagram)

X = nbSoundings;
Values{end+1}    = X;
strValues{end+1} = num2str(X);

%% Number of beams (WC datagram)

X = nbBeamsWC;
Values{end+1}    = X;
strValues{end+1} = num2str(X);

%% CentralFrequency

if isempty(DataRaw) % Cas des fichiers NIWA hydromap�s
    X = NaN;
    Values{end+1}    = X;
    strValues{end+1} = num2str(X);
else
    if strcmp(SousVersion, 'F') % DataRuntime
        switch DataRaw.EmModel
            case 1002
                X = 95;
            otherwise
                X = NaN;
        end
        Values{end+1}    = X;
        strValues{end+1} = num2str(X);
    else
        X = DataRaw.CentralFrequency(subRaw,:);
        X = median(X(:), 'omitnan') / 1000;
        Values{end+1}    = X;
        strValues{end+1} = num2str(X);
    end
end

%% Pulse length Total

if isfield(DataRaw, 'SignalLength')
    X = PulseLengthTotal * 1000;
    Values{end+1}    = X;
    strValues{end+1} = num2str(X);
else
    if isempty(PulseLengthTotal)
        X = NaN;
        Values{end+1}    = X;
        strValues{end+1} = num2str(X);
    else
        X = PulseLengthTotal * 1000;
        Values{end+1}    = X;
        strValues{end+1} = num2str(X);
    end
end

%% PulseLength_Effective

if isempty(PulseLength_Effective)
    X = NaN;
    Values{end+1}    = X;
    strValues{end+1} = num2str(X);
else
    X = mode(min(PulseLength_Effective, [], 2));
    Values{end+1}    = X;
    strValues{end+1} = num2str(X);
end

%% PulseLength_UsedByKMinIA

if isempty(PulseLength_UsedByKMinIA)
    X = NaN;
    Values{end+1}    = X;
    strValues{end+1} = num2str(X);
else
    X = mode(min(PulseLength_UsedByKMinIA, [], 2));
    Values{end+1}    = X;
    strValues{end+1} = num2str(X,3);
end

%% Sampling frequency Imagery

X = SamplingFrequencyBathy;
Values{end+1}    = X;
strValues{end+1} = num2str(X);

%% Sampling frequency WC

X = SamplingFrequencyWC;
Values{end+1}    = X;
strValues{end+1} = num2str(X);

%% Nyquist Bathy

X = NyquistBathy;
Values{end+1}    = X;
strValues{end+1} = num2str(X);

%% Nyquist WC

X = NyquistWC;
Values{end+1}    = X;
strValues{end+1} = num2str(X);

%% Number of sound speed profiles

X = max(nbSoundSpeedProfile);
Values{end+1}    = X;
strValues{end+1} = num2str(X);

%% Depth

X = MeanDepth;
Values{end+1}    = X;
strValues{end+1} = num2str(X);

X = StdDepth;
Values{end+1}    = X;
strValues{end+1} = num2str(X);

X = MinDepth;
Values{end+1}    = X;
strValues{end+1} = num2str(X);

X = MaxDepth;
Values{end+1}    = X;
strValues{end+1} = num2str(X);

%% Valid soundings

X = PerCentValidRawSoundings;
Values{end+1}    = X;
strValues{end+1} = num2str(X);

X = PerCentValidCleanedSoundings;
Values{end+1}    = -X;
strValues{end+1} = num2str(X);

X = PerCentAmplitudeDetections;
Values{end+1}    = X;
strValues{end+1} = num2str(X);

%% Immersion

X = MeanImmersion;
Values{end+1}    = X;
strValues{end+1} = num2str(X);

X = AltitudetOverSeabed;
Values{end+1}    = X;
strValues{end+1} = num2str(X);

%% Param�tres seabed image

X = BSMean;
Values{end+1}    = X;
strValues{end+1} = num2str(X, '%5.2f');
if isempty(MeanBSN)
    X = 0;
    for k=1:3
        Values{end+1}    = X; %#ok<AGROW>
        strValues{end+1} = num2str(X); %#ok<AGROW>
    end
else
    Values{end+1}    = MeanBSN;
    strValues{end+1} = num2str(MeanBSN);
    Values{end+1}    = MeanBSO;
    strValues{end+1} = num2str(MeanBSO);
    Values{end+1}    = MeanBSN - MeanBSO;
    strValues{end+1} = num2str(MeanBSN - MeanBSO);
end

%% Swath width

X = SwathWidth;
Values{end+1}    = X;
strValues{end+1} = num2str(X, '%5.2f');

X = AngularRange;
Values{end+1}    = X;
strValues{end+1} = num2str(X, '%5.2f');

X = SwathOnDepth;
Values{end+1}    = X;
strValues{end+1} = num2str(X, '%5.2f');

%% Surface sound speed

X = SurfaceSoundSpeed;
Values{end+1}    = X;
strValues{end+1} = num2str(X, '%5.2f');

%% Absorption coefficient

X = AlphaMedian;
Values{end+1}    = X;
strValues{end+1} = num2str(X, '%5.2f');

%% Speed

X = speedMpS;
Values{end+1}    = X;
strValues{end+1} = num2str(X, '%5.2f');

X = speedNds;
Values{end+1}    = X;
strValues{end+1} = num2str(X, '%5.2f');

%% Inter ping distance

X = DistanceInterPings;
Values{end+1}    = X;
strValues{end+1} = num2str(X,2);

%% Distance max inter beam

Values{end+1}    = DistanceInterBeams;
strValues{end+1} = num2str(DistanceInterBeams, '%5.2f');

%% Ration DistanceInterBeams / DistanceInterPings

ratio = DistanceInterBeams / DistanceInterPings;
Values{end+1}    = ratio;
strValues{end+1} = num2str(ratio, '%5.2f');


%% Inter ping recurrence

Values{end+1}    = seconds(RecurrenceInterPings);
strValues{end+1} = num2str(seconds(RecurrenceInterPings), '%5.2f');

%% Longueur de la ligne

Values{end+1}    = LengthLine;
strValues{end+1} = num2str(LengthLine, '%5.2f');

%% Surface couverte de la ligne (km2)

Values{end+1}    = LineArea * 1e-6;
strValues{end+1} = num2str(LineArea * 1e-6, '%5.2f');

%% Geographic position

X = LatMedian;
Values{end+1}    = X;
strValues{end+1} = lat2str(X);

X = LonMedian;
Values{end+1}    = X;
strValues{end+1} = lon2str(X);

if isempty(GPSQualityIndicator)
    Values{end+1}    = NaN;
    strValues{end+1} = '';
else
    X = GPSQualityIndicator;
    Values{end+1}    = X;
    strValues{end+1} = strGPSQualityIndicator{X};
end

%% QF Amp & Phase

X = QFMeanAmp;
Values{end+1}    = X;
strValues{end+1} = num2str(X, '%5.2f');

X = QFMeanPhase;
Values{end+1}    = X;
strValues{end+1} = num2str(X, '%5.2f');

%% SpikeFilterSignal

[Values{end+1}, strValues{end+1}] = synthesize(spikeFilterSignal);

%% RangeGates

[Values{end+1}, strValues{end+1}] = synthesize(rangeGatesSignal);

%% phaseRamp

[Values{end+1}, strValues{end+1}] = synthesize(phaseRampSignal);

%% slopeFilterSignal

[Values{end+1}, strValues{end+1}] = synthesize(slopeFilterSignal);

%% DetectionMode

[Values{end+1}, strValues{end+1}] = synthesize(detectionModeSignal);

%% SectorTracking

[Values{end+1}, strValues{end+1}] = synthesize(sectorTrackingSignal);

%% AerationFilter

[Values{end+1}, strValues{end+1}] = synthesize(aerationFilterSignal);

%% InterferenceFilter

[Values{end+1}, strValues{end+1}] = synthesize(interferenceFilterSignal);

%% PenetrationFilter

[Values{end+1}, strValues{end+1}] = synthesize(penetrationFilterSignal);

%% Heading

X = median(HeadingMedian, 'omitnan');
Values{end+1}    = X;
strValues{end+1} = num2str(X, '%5.1f');

Values{end+1}    = strDirection;
strValues{end+1} = strDirection;

Values{end+1}    = typeLine;
strValues{end+1} = typeLine;

%% Roll

X = RollMean;
Values{end+1}    = X;
strValues{end+1} = num2str(X, '%5.2f');

X = RollStd;
Values{end+1}    = X;
strValues{end+1} = num2str(RollStd, '%5.2f');

%% Pitch

X = PitchMean;
Values{end+1}    = X;
strValues{end+1} = num2str(X, '%5.2f');

X = PitchStd;
Values{end+1}    = X;
strValues{end+1} = num2str(X,  '%5.2f');

%% Heave

X = HeaveMean;
Values{end+1}    = X;
strValues{end+1} = num2str(X, '%5.2f');

X = HeaveStd;
Values{end+1}    = X;
strValues{end+1} = num2str(X,  '%5.2f');

%% Tide

X = PourcentagePingsRTK;
Values{end+1}    = X;
strValues{end+1} = num2str(X, '%5.1f');

X = Tide;
Values{end+1}    = X;
strValues{end+1} = num2str(X,  '%5.2f');

%% Single / Double Swath

X = nbPingsSingleSwaths;
Values{end+1}    = X;
strValues{end+1} = num2str(X);

X = nbPingsDoubleSwaths;
Values{end+1}    = X;
strValues{end+1} = num2str(X);

%% Pings WC

X = nbPingsWithWC;
Values{end+1}    = X;
strValues{end+1} = num2str(X);

X = nbPingsWithoutWC;
Values{end+1}    = X;
strValues{end+1} = num2str(X);

X = TVGFunctionApplied;
Values{end+1}    = X;
strValues{end+1} = num2str(X);

X = TVGOffset;
Values{end+1}    = X;
strValues{end+1} = num2str(X);

%% Pings FM

X = nbPingsWithFM;
Values{end+1}    = X;
strValues{end+1} = num2str(X);

X = nbPingsWithoutFM;
Values{end+1}    = X;
strValues{end+1} = num2str(X);

%% Pings Modes

for k=1:length(HistModes)
    X = HistModes(k);
    Values{end+1}    = X; %#ok<AGROW>
    strValues{end+1} = num2str(X); %#ok<AGROW>
end

%% File size

X = sizeFic(this.nomFic);
Values{end+1}    = X;
strValues{end+1} = num2strMoney(X);

%% Stave Data

X = max(presenceStaveData);
Values{end+1}    = X;
strValues{end+1} = num2str(X);

%% Mag & Phase Data

X = max(presenceMagAndPhase);
Values{end+1}    = X;
strValues{end+1} = num2str(X);

%% Syst�mes de navigation

if isempty(nbSensorsPosition)
    Values{end+1}    = '';
    strValues{end+1} = '';
else
    Values{end+1}    = nbSensorsPosition;
    strValues{end+1} = num2str(nbSensorsPosition);
    
    Values{end+1}    = indSystemUsedPosition;
    strValues{end+1} = num2str(indSystemUsedPosition);
end

%% Centrales d'attitude

if isempty(nbSensorsAttitude)
    Values{end+1}    = '';
    strValues{end+1} = '';
else
    Values{end+1}    = nbSensorsAttitude;
    strValues{end+1} = num2str(nbSensorsAttitude);
    
    Values{end+1}    = indSystemUsedAttitude;
    strValues{end+1} = num2str(indSystemUsedAttitude);
end

%% Param�tres d'installation

if isempty(OSV)
    Values{end+1}    = '';
    strValues{end+1} = '';
else
    Values{end+1}    = OSV;
    strValues{end+1} = OSV;
end

if isempty(PSV)
    Values{end+1}    = '';
    strValues{end+1} = '';
else
    Values{end+1}    = PSV;
    strValues{end+1} = PSV;
end

if isempty(SID)
    Values{end+1}    = '';
    strValues{end+1} = '';
else
    Values{end+1}    = SID;
    strValues{end+1} = SID;
end

if isDual
    Values{end+1}    = 'Yes';
    strValues{end+1} = 'Yes';
else
    Values{end+1}    = 'No';
    strValues{end+1} = 'No';
end

%% Heure d�but et fin

format longG
t = DataDepth.Datetime(subDepth);

Values{end+1}    = t(1);
strValues{end+1} = char(t(1));

Values{end+1}    = t(end);
strValues{end+1} = char(t(end));

Duration = t(end) - t(1);
Values{end+1}    = minutes(Duration);
strValues{end+1} = char(Duration);

%% KM #Ping beginning et KM #Ping end

X = DataDepth_PingCounter(1);
Values{end+1}    = X;
strValues{end+1} = num2str(X, '%d');

X = DataDepth_PingCounter(end);
Values{end+1}    = X;
strValues{end+1} = num2str(X, '%d');

%% D�calages temporels

Values{end+1}    = PU_ZBA;
strValues{end+1} = num2str(PU_ZBA);

if isempty(PU_GGA)
    Values{end+1}    = NaN;
    strValues{end+1} = '';
else
    Values{end+1}    = PU_GGA;
    strValues{end+1} = num2str(PU_GGA);
end
format long

%% The End

flag = 1;


function [Values, strValues] = synthesize(signal)

if isempty(signal)
    Values    = [];
    strValues = [];
    
elseif isa(signal.ySample, 'BooleanYSample')
    X = signal.ySample.data;
    if mode(logical(X))
        Values    = 1;
        strValues = 'On';
    else
        Values    = 0;
        strValues = 'Off';
    end
    
elseif isa(signal.ySample, 'IndexYSample')
    X = mode(signal.ySample.data);
    if isnan(X)
        Values    = NaN;
        strValues = 'NaN';
    else
        Values    = X;
        strValues = signal.ySample.strIndexList{X};
    end
    
else
    my_breakpoint('FctName', 'cl_simarad_all/summary_Lines/synthesize');
    class(signal.ySample)
end