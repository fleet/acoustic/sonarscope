function repExport = suppressDatagramInAllFile(this, repExport)

nomFicListe = fullfile(repExport, 'ListReducedFiles.txt');
str1 = 'Nom du fichier donnant la liste des nouveaux fichiers .all sectionn�s';
str2 = 'Give the name to the file that will collect the full names of the reduced .all files';
[flag, nomFicListe] = my_uiputfile('*.txt', Lang(str1,str2), nomFicListe);
if ~flag
    return
end
repExport = fileparts(nomFicListe);

fid = fopen(nomFicListe, 'w+');

str1 = 'R�duction des fichiers .all (et .wcd)';
str2 = 'Reducing .all files';
N = length(this);
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
    [flag, selectDatagram] = selectDatagramFromFile(this(k));
    if ~flag
        return
    end
    nomFicOut = saucissonne_bin(aKM, [], [], selectDatagram);
    for k2=1:length(nomFicOut)
        fprintf(fid, '%s\n', nomFicOut{k2});
    end
    my_waitbar(k, N, hw)
end
my_close(hw, 'MsgEnd')
