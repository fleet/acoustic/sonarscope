% Fusion des datagrammes WCD et ALL dans un seul fichier ALL.
%
% Syntax
%   Data = union_WCDandALL(aKM, ...)
%
% Input Arguments
%   aKM : Instance de cl_simrad_all
%
% Name-only Arguments
%
% Output Arguments
%   nomFicOut : liste des fichiers de sortie.
%
% Examples
%   nomFic    = getNomFicDatabase('EM1002_0001_20040916_054013_raw.all')
%   aKM         = cl_simrad_all('nomFic', nomFic);
%   nomFicOut = union_WCDFromALL(aKM);
%
% See also cl_simrad_all Authors
% Authors : GLU
% ----------------------------------------------------------------------------

function nomFicAllOut = union_WCDandALL(this, subDatagram)

nomFicAllOut = {};

%%

[nomDir, nomFic, ~] = fileparts(this.nomFic);
nomFicWcd = fullfile(nomDir, [nomFic '.wcd']);
if ~exist(nomFicWcd, 'file')
    nomFicWcd = fullfile(nomDir, [nomFic '.WCD']);
    if ~exist(nomFicWcd, 'file')
        strFR = 'Le fichier WCD n''existe pas.';
        strUS = 'WCD file doesn''t exist.';
        my_warndlg(Lang(strFR,strUS), 1);
        return
    end
end

% On consid�re les WC synchornis�s sur les WC. Seul le datagramme WC est ins�r� dans les
% datagrammes ALL.

% Cr�ation des fichiers unitaire de type ALL.
nomFicAllOut = union_WCDFromALL_unitaire(this, nomFicWcd, subDatagram);

%% Lecture des nouveaux fichiers ALL (et WCD en automatique)

%{
aKM = cl_simrad_all('nomFic', nomFicAllOut);
if isempty(aKM)
    return
end
% plot_position(aKM)
%}

%% Renommage du fichier .all et .wcd

%{
nomFicNew = fullfile(nomDir, [nomFic '.alluniond']);

[status, message] = movefile(this.nomFic, nomFicNew, 'f');

if status
    strFR = sprintf('Le fichier "%s" a �t� renomm� en "%s"', this.nomFic, nomFicNew);
    strUS = sprintf('File "%s" was renamed as "%s"', this.nomFic, nomFicNew);
    my_warndlg(Lang(strFR,strUS), 0, 'Tag', 'ImpossibleDeRenommerCeFichier');
else
    strFR = sprintf('Echec de renommage de "%s" en "%s", faites le � la main : \nMessage matlab : %s', this.nomFic, nomFicNew, message);
    strUS = sprintf('It was impossible to rename "%s" in "%s", do it manualy : \nMatlab message : %s', this.nomFic, nomFicNew, message);
    my_warndlg(Lang(strFR,strUS), 0, 'Tag', 'ImpossibleDeRenommerCeFichier');
end

nomFicNew = fullfile(nomDir, [nomFic '.wcduniond']);
[status, message] = movefile(nomFicWcd, nomFicNew, 'f');

if status
    strFR = sprintf('Le fichier "%s" a �t� renomm� en "%s"', this.nomFic, nomFicNew);
    strUS = sprintf('File "%s" was renamed as "%s"', this.nomFic, nomFicNew);
    my_warndlg(Lang(strFR,strUS), 0, 'Tag', 'ImpossibleDeRenommerCeFichier');
else
    strFR = sprintf('Echec de renommage de "%s" en "%s", faites le � la main : \nMessage matlab : %s', nomFicWcd, nomFicNew, message);
    strUS = sprintf('It was impossible to rename "%s" in "%s", do it manualy : \nMatlab message : %s', nomFicWcd, nomFicNew, message);
    my_warndlg(Lang(strFR,strUS), 0, 'Tag', 'ImpossibleDeRenommerCeFichier');
end
%}


function nomFicOut = union_WCDFromALL_unitaire(this, nomFicWcd, subDatagram)

[nomDir, nomFic, ~] = fileparts(this.nomFic);
nomFicOut = fullfile(nomDir, [nomFic '_uniondAllAndWcd.all']);
if exist(nomFicOut, 'file')
    str1 = sprintf('Le fichier "%s" existe d�j� : on le bypasse', nomFicOut);
    str2 = sprintf('File "%s" already exists : bypassed', nomFicOut);
    my_warndlg(Lang(str1,str2), 0);
    return
end

%% Lecture du fichier d'index du fichier ALL

[flag, Data] = read_FileIndex_bin(this);
if ~flag
    return
end

PositionInAll       = Data.DatagramPosition;
TailleInAll         = double(Data.DatagramLength);
PingCounterInAll    = Data.PingCounter;
TypeOfDatagramInAll = Data.TypeOfDatagram;
% TimeMatInAll        = Data.Time.timeMat;
% IndexInAll = ones(size(TimeMatInAll,1),1) ; % TODO : distinguer IndexInAll et IndexOut ?

%% Lecture du fichier d'index du fichier WCD

thisWcd = cl_simrad_all('nomFic', nomFicWcd);
[flag, DataWcd] = read_FileIndex_bin(thisWcd);
if ~flag
    return
end

PositionInWcd       = DataWcd.DatagramPosition;
TailleInWcd         = double(DataWcd.DatagramLength);
PingCounterInWcd    = DataWcd.PingCounter;
TypeOfDatagramInWcd = DataWcd.TypeOfDatagram;
% indexInWcd = ones(size(PingCounterInWcd));

%% V�rification pour que des pings d'un sondeur dual ne se retrouvent pas s�par�s dans 2 fichiers s�par�s

sub = [subDatagram{:,4}];
selectedCode = [subDatagram{sub,2}];
if isempty(selectedCode)
    strFR = sprintf('Aucun datagramme s�lectionn� : pas de cr�ation de fichiers splitt�s.');
    strUS = sprintf('None datagram selected : no split files creation.');
    my_warndlg(Lang(strFR,strUS), 0);
    return
end

% listeDatagramsMBES = [68 88 75 70 102 78 83 89 107 113];
% listeDatagramsMBES = [68 70 75 78 79 83 88 89 102 107 109 113]; % Modifi� le 15/05/2012 � bord du Falkor
% Modif par GLU pour traiter les derniers datagrammes trait�s : le 04/06/0212
% listeDatagramsMBES = [49 65 67 68 70 71 72 73 74 78 80 82 83 85 88 89 102 104 107 109 110 112 114];
% listeDatagramsMBES = [48 49 65 67 68 70 71 72 73 74 78 79 80 82 83 85 88 89 102 104 107 109 110 112 114]; % 48 et 79 rajout�s

% for k1=1:length(listeDatagramsMBES)
%     sub = (TypeOfDatagramInAll == listeDatagramsMBES(k1));
%     PC = PingCounterInAll(sub);
%     PCIndex = IndexInAll(sub);
%     subChangement = find(diff(PCIndex) == 1);
%     for k2=1:length(subChangement)
%         if length(unique(PC(subChangement:subChangement+1))) == 1
%             PCIndex(subChangement:subChangement+1) = PCIndex(subChangement+1);
%         end
%     end
%     IndexInAll(sub) = PCIndex;
% end
% % plot_position(this)

%% Decodage du fichier de donn�es

typeIndian = getIndianAll(this.nomFic);
% Ouverture du fichier ALL d'origine
[fidInAll, message] = fopen(this.nomFic, 'r', typeIndian);
if fidInAll < 0
    my_warndlg(message, 1);
    return
end
% Ouverture du fichier WCD d'origine
[fidInWcd, message] = fopen(nomFicWcd, 'r', typeIndian);
if fidInWcd < 0
    my_warndlg(message, 1);
    return
end

    % Simple filtrage des datagrammes.
fidOut = fopen(nomFicOut, 'w+');
[nomDir2, nomFic2] = fileparts(nomFicOut);
nomsSsc = fullfile(nomDir2, 'SonarScope', nomFic2);
if exist(nomsSsc, 'dir')
    try
        rmdir(nomsSsc, 's')
        if exist(nomsSsc, 'dir')
            strFR = sprintf('"%s" n''a pas pu �tre supprim�.', nomsSsc);
            strUS = sprintf('"%s" could not be suppressed.', nomsSsc);
            my_warndlg(Lang(strFR,strUS), 0);
            flag = 0;
        end
        
    catch %#ok<CTCH>
        strFR = sprintf('Le r�pertoire"%s" n''a pas pu �tre supprim�, on arr�te tout.', nomsSsc);
        strUS = sprintf('Folder "%s" could not be suppressed. Nothing will be done before, please solve the problem before.', nomsSsc);
        my_warndlg(Lang(strFR,strUS), 0);
        flag = 0;
    end
end
if ~flag
    return
end

%% Traitement du fichier

% 65 : Attitude
% 67 : Clock
% 68 : Depth
% 70 : Raw Range And BeamAngle
% 71 : Surface Sound Speed
% 72 : Heading
% 73 : Installation Parameter (start)
% 80 : Position
% 82 : Runtime parameter
% 83 : seabed Image
% 85 : SoundSpeed profile
% 105 : Installation Parameter (stop)


% ExtraParameters	:       51
% Attitude	:               65
% Clock	:                   67
% DepthV1	:               68
% Raw Range Beam Angles	:	70
% Surface Sound Speed	:	71
% Heading	:               72
% Mecha Transducer Tilt	:	74
% Raw Range Beam Angles (new version)	:	78
% Position	:               80
% Runtime	:               82
% Seabed Image Data	:       83
% SoundSpeedProfile	:       85
% XYZ88	:                   88
% Seabed Image Data 89	:	89
% Raw Range Beam Angles (since 2004)	:	102
% Height	:               104
% Water Column	:           107
% Stave Data	:           109
% Network Attitude	:       110
% Quality Factor	:       112
% Raw Beam Samples	:       114
% InstallParams	:           73 (Start) ou 105 (Stop)ou 112
% TODO: identifier les num�ros et choisir l'action
% 102 78 113 72 69 84 71 87 112 74 48 49 66

str = sprintf('Loading file %s', nomFic);
N   = length(PositionInAll);
hw  = create_waitbar(str, 'N', N);
iFlagPingWcd = zeros(size(TypeOfDatagramInWcd,1), 1);
for k1=1:N
    my_waitbar(k1, N, hw);
    % On ne sauvegarde que les datagrammes s�lectionn�s.
    pppp = find(selectedCode == TypeOfDatagramInAll(k1),1);
    if ~isempty(pppp)
        switch TypeOfDatagramInAll(k1)
            
            % Depth, Raw range and beam angle, SeabedImage, Central beams
            % echogram, quality factor,
            case {68 70 75 78 79 83 88 89 102 107 109 113 114}
                fseek(fidInAll, PositionInAll(k1), 'bof');
                X = fread(fidInAll, TailleInAll(k1), 'uchar');
                fwrite(fidOut, X, 'uchar');
                % Ajout du datagramme WCD du fichier .wcd au niveau du Ping correspondant.
                % On les consid�re strictement synchronis� avec les Pings
                % de type Depth, Seabed, ...
                
                u = find(PingCounterInWcd == PingCounterInAll(k1));
                v = (TypeOfDatagramInWcd(u) == 107);
                idxWcd = u(v);
                if iFlagPingWcd(idxWcd(1)) == 0
                    for k2=1:length(idxWcd)
                        fseek(fidInWcd, PositionInWcd(idxWcd(k2)), 'bof');
                        X = fread(fidInWcd, TailleInWcd(idxWcd(k2)), 'uchar');
                        fwrite(fidOut, X, 'uchar');
                        iFlagPingWcd(idxWcd(k2)) = 1;
                    end
                end
                
                % Attitude, Clock, Position : TODO : il faudrait peut-�tre recopier un de chaque avant et apr�s : passage par des subAttitude{k1} qui serait obtenu par un subAtt(1)-1:subAtt(end)+1
            case {65 67 80 110}
                fseek(fidInAll, PositionInAll(k1), 'bof');
                X = fread(fidInAll, TailleInAll(k1), 'uchar');
                fwrite(fidOut, X, 'uchar');
                
                % Installation Parameter start, Runtime, Sound speed profile, Installation Parameter stop
            case {49 51 66 69 71 72 73 74 48 82 84 85 87 104 105 112}
                fseek(fidInAll, PositionInAll(k1), 'bof');
                X = fread(fidInAll, TailleInAll(k1), 'uchar');
                fwrite(fidOut, X, 'uchar');
                
                %
            otherwise
                strFR = sprintf('TypeOfDatagram "%d" non identifi� dans "union_WCDandALL", signalez ce message � sonarscope@ifremer.fr', TypeOfDatagramInAll(k1));
                strUS =  sprintf('TypeOfDatagram "%d" not identified in "union_WCDandALL", please report to sonarscope@ifremer.fr', TypeOfDatagramInAll(k1));
                my_warndlg(Lang(strFR,strUS), 0, 'Tag', 'DatagrammeNonIdentifie');
                fseek(fidInAll, PositionInAll(k1), 'bof');
                X = fread(fidInAll, TailleInAll(k1), 'uchar');
                fwrite(fidOut, X, 'uchar');
        end
    end
end
my_close(hw)
fclose(fidInAll);
fclose(fidInWcd);
fclose(fidOut);
