% Recherche du num�ro de version des datagrams un fichier .all
%
% Syntax
%   str = version_datagram(aKM)
% 
% Input Arguments
%   aKM : Instance de cl_simrad_all
%
% Output Arguments
%   str : 
%
% Examples
%   nomFic = getNomFicDatabase('EM1002_0001_20040916_054013_raw.all')
%   aKM = cl_simrad_all('nomFic', nomFic);
%   Data = version_datagram(aKM)
%
% See also cl_simrad_all  Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [Version, SousVersion] = version_datagram(this)

% flag = existCacheNetcdf(this.nomFic);
% if flag
%     [Version, SousVersion] = version_datagramNetcdf(this);
% else
%     [Version, SousVersion] = version_datagramXMLBin(this);
% end

Version     = get(this, 'Version');
SousVersion = get(this, 'SousVersion');
