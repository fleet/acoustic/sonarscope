% Recherche du num�ro de version des datagrams un fichier .all
%
% Syntax
%   str = version_datagram(aKM)
% 
% Input Arguments
%   aKM : Instance de cl_simrad_all
%
% Output Arguments
%   str : 
%
% Examples
%   nomFic = getNomFicDatabase('EM1002_0001_20040916_054013_raw.all')
%   aKM = cl_simrad_all('nomFic', nomFic);
%   Data = version_datagramNetcdf(aKM)
%
% See also cl_simrad_all  Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [Version, SousVersion] = version_datagramNetcdf(this)

Version     = [];
SousVersion = [];

%% Check the number of elements

flag = checkIfOneElement(length(this));
if ~flag
    return
end

%%

% [nomDir, nomFic] = fileparts(this.nomFic);
% ncFileName = fullfile(nomDir, 'SonarScope', [nomFic '.nc']);
% if exist(ncFileName, 'file')
[flag, ncFileName] = existCacheNetcdf(this.nomFic);
if flag
    ncID  = netcdf.open(ncFileName, 'NOWRITE');
    attID = -1; % netcdf.getConstant('GLOBAL');
    
    Version = netcdf.getAtt(ncID, attID, 'DatagramVersion');
    if strcmp(Version, 'V1')
        SousVersion = netcdf.getAtt(ncID, attID, 'DatagramSousVersion');
    end
    netcdf.close(ncID);
end
