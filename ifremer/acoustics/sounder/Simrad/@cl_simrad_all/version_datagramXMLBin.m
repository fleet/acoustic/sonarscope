% Recherche du num�ro de version des datagrams un fichier .all
%
% Syntax
%   str = version_datagram(aKM)
% 
% Input Arguments
%   aKM : Instance de cl_simrad_all
%
% Output Arguments
%   str : 
%
% Examples
%   nomFic = getNomFicDatabase('EM1002_0001_20040916_054013_raw.all')
%   aKM = cl_simrad_all('nomFic', nomFic);
%   Data = version_datagramXMLBin(aKM)
%
% See also cl_simrad_all  Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [Version, SousVersion] = version_datagramXMLBin(this)

Version     = [];
SousVersion = [];

%% Check the number of elements

flag = checkIfOneElement(length(this));
if ~flag
    return
end

%%

[nomDir, nomFicSeul] = fileparts(this.nomFic);
nomDirRacine = fullfile(nomDir, 'SonarScope', nomFicSeul);
if exist(fullfile(nomDirRacine, 'ALL_RawRangeBeamAngle66h'), 'dir')
    Version     = 'V1';
    SousVersion = 'f';
elseif exist(fullfile(nomDirRacine, 'ALL_RawRangeBeamAngle46h'), 'dir')
    Version     = 'V1';
    SousVersion = 'F';
elseif exist(fullfile(nomDirRacine, 'ALL_RawRangeBeamAngle4eh'), 'dir')
    Version     = 'V2';
    SousVersion = '';
else
    
    % On force la version � V1. Cas rencontr� avec fichier NIWA 0005_20080703_224347_raw.all
    Version = 'V1';
    SousVersion = 'F';
end

if ~isdeployed && isempty(Version)
    [~, nomFic] = fileparts(this.nomFicIndex);
    str1 = sprintf('Impossible de d�terminer la version du fichier %s. J''impose V1', nomFic);
    str2 = sprintf('It is impossible de determine the version of file %s. I impose V1', nomFic);
    my_warndlg(Lang(str1,str2), 0, 'Tag', 'OmpossibleDetermineVersionFile');
end
