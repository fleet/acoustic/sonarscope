function flag = write_SeabedImage_bin(this, varargin)

[varargin, TVGNRecomputed] = getPropertyValue(varargin, 'TVGNRecomputed', []); %#ok<ASGLU>

[nomDir, nom] = fileparts(this.nomFic);
nomDir = fullfile(nomDir, 'SonarScope', nom);

flag = 1;

if ~isempty(TVGNRecomputed)
    InfoSignals.Name       = 'TVGNRecomputed';
    InfoSignals.Dimensions = 'nbPings, nbSounders';
    InfoSignals.Storage    = 'single';
    InfoSignals.Unit       = 'deg';
    InfoSignals.FileName   = fullfile('Ssc_SeabedImage59h','TVGNRecomputed.bin');
    InfoSignals.Tag        = verifKeyWord('TODO');
    
    flag = writeSignal(nomDir, InfoSignals, TVGNRecomputed(:));
    if ~flag
        return
    end
end
