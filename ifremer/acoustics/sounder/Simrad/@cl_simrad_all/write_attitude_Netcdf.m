function flag = write_attitude_Netcdf(this, Data)

flag = 0;
if isempty(Data) || isempty(Data.Time) || isempty(Data.Time.timeMat)
    return
end

[nomDirRacine, nomFic] = fileparts(this.nomFic);
nomFicNc = fullfile(nomDirRacine, 'SonarScope', [nomFic '.nc']);
if ~exist(nomFicNc, 'file')
    return
end

ncID  = netcdf.open(nomFicNc, 'WRITE');
grpID = netcdf.inqNcid(ncID, 'Attitude');

varID = netcdf.inqVarID(grpID, 'Time');
tNetcdf = netcdf.getVar(grpID, varID);

if isfield(Data, 'Tide')
    varID = netcdf.inqVarID(grpID, 'Tide');
    X = interp1(Data.Time.timeMat, Data.Tide, tNetcdf);
    NetcdfUtils.putVarVal(grpID, varID, X, 1)
end

if isfield(Data, 'TrueHeave')
    varID = netcdf.inqVarID(grpID, 'TrueHeave');
    X = interp1(Data.Time.timeMat, Data.TrueHeave, tNetcdf);
    NetcdfUtils.putVarVal(grpID, varID, X, 1)
end

if isfield(Data, 'Draught')
    varID = netcdf.inqVarID(grpID, 'Draught');
    X = interp1(Data.Time.timeMat, Data.Draught, tNetcdf);
    NetcdfUtils.putVarVal(grpID, varID, X, 1)
end

netcdf.close(ncID);

flag = 1;
