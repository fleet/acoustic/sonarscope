function flag = write_attitude_bin(this, Data)

if isempty(Data) || isempty(Data.Time) || isempty(Data.Time.timeMat)
    flag = 0;
    return
end
Info.Title              = Data.Title; % Motion Reference Unit
Info.Constructor        = Data.Constructor;
Info.EmModel            = Data.EmModel;
Info.SystemSerialNumber = Data.SystemSerialNumber(:,:);
Info.TimeOrigin         = Data.TimeOrigin;
Info.Comments           = Data.Comments;
Info.FormatVersion      = Data.FormatVersion;

Info.Dimensions         = Data.Dimensions;
Info.Signals            = Data.Signals;

%% Ajout �ventuel de signaux non compris dans le datagramme Kongsberg d'attidude

k = strcmp({Info.Signals.Name}, 'Tide');
if ~any(k) && isfield(Data, 'Tide')
    Info.Signals(end+1).Name      = 'Tide';
    Info.Signals(end).Dimensions  = 'NbSamples, 1';
    Info.Signals(end).Index       = 'NbDatagrams, NbCycles';
    Info.Signals(end).Storage     = 'single';
    Info.Signals(end).Unit        = 'm';
    Info.Signals(end).FileName    = fullfile('Ssc_Attitude','Tide.bin');
    Info.Signals(end).Tag         = verifKeyWord('MotionSensorHeading');
end

k = strcmp({Info.Signals.Name}, 'TrueHeave');
if ~any(k) && isfield(Data, 'TrueHeave')
    Info.Signals(end+1).Name      = 'TrueHeave';
    Info.Signals(end).Dimensions  = 'NbSamples, 1';
    Info.Signals(end).Index       = 'NbDatagrams, NbCycles';
    Info.Signals(end).Storage     = 'single';
    Info.Signals(end).Unit        = 'm';
    Info.Signals(end).FileName    = fullfile('Ssc_Attitude','TrueHeave.bin');
    Info.Signals(end).Tag         = verifKeyWord('MotionSensorHeading');
end

k = strcmp({Info.Signals.Name}, 'Draught');
if ~any(k) && isfield(Data, 'Draught')
    Info.Signals(end+1).Name      = 'Draught';
    Info.Signals(end).Dimensions  = 'NbSamples, 1';
    Info.Signals(end).Index       = 'NbDatagrams, NbCycles';
    Info.Signals(end).Storage     = 'single';
    Info.Signals(end).Unit        = 'm';
    Info.Signals(end).FileName    = fullfile('Ssc_Attitude','Draught.bin');
    Info.Signals(end).Tag         = verifKeyWord('MotionSensorHeading');
end

% Ajout des champs Scale_Factor, Add_Offset et Orig_Storage.
% [flag, Info] = addScaleOffsetStorage(Datagrams, Info);

%% Cr�ation du fichier XML d�crivant la donn�e

[nomDirRacine, nomFic] = fileparts(this.nomFic);
nomDirRacine = fullfile(nomDirRacine, 'SonarScope', nomFic);
nomFicXml = fullfile(nomDirRacine, 'Ssc_Attitude.xml');
xml_write(nomFicXml, Info);
flag = exist(nomFicXml, 'file');
if ~flag
    messageErreur(nomFicXml)
    return
end

%% Cr�ation du r�pertoire Attitude

nomDirAttitude = fullfile(nomDirRacine, 'Ssc_Attitude');
if ~exist(nomDirAttitude, 'dir')
    status = mkdir(nomDirAttitude);
    if ~status
        messageErreur(nomDirAttitude)
        flag = 0;
        return
    end
end

%% Cr�ation des fichiers binaires des signaux

for k=1:length(Info.Signals)
    flag = writeSignal(nomDirRacine, Info.Signals(k), Data.(Info.Signals(k).Name));
    if ~flag
        return
    end
end

flag = 1;
