function flag = write_clock_bin(a)


%% Lecture de la donn�e Clock dans les anciens dormats SonarScope

Data = read_clock(a);

if isempty(Data) || isempty(Data.Time) || isempty(Data.Time.timeMat)
    flag = 0;
    return
end

nbSamples = length(Data.Time.timeMat);

%% Cr�ation de la structure d�crivant la donn�e

Info.Title                  = 'Clock';
Info.EmModel                = Data.EmModel;
Info.SystemSerialNumber     = Data.SystemSerialNumber(:,:);
Info.NbSamples              = nbSamples;
Info.Comments               = 'Sounder ping rate';

Info.Synchronisation        = Data.Synchronisation;

Info.Signals(1).Name        = 'Time';
Info.Signals(1).Storage     = 'double';
Info.Signals(1).Unit        = 'days since JC';
Info.Signals(1).Direction	= 'FirstValue=FirstPing';
Info.Signals(1).FileName    = fullfile('Clock', 'Time.bin');
Info.Signals(1).Tag         = verifKeyWord('SounderTime');

Info.Signals(end+1).Name      = 'ExternalTime';
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = 'days since JC';
Info.Signals(end).Direction	  = 'FirstValue=FirstPing';
Info.Signals(end).FileName    = fullfile('Clock', 'ExternalTime.bin');
Info.Signals(end).Tag         = verifKeyWord('ExternalTime');

%% Cr�ation du r�pertoire SonarScope

[nomDir, nom] = fileparts(a.nomFic);
nomDir = fullfile(nomDir, 'SonarScope', nom);
if ~exist(nomDir, 'dir')
    status = mkdir(nomDir);
    if ~status
        messageErreurFichier(nomDir);
        flag = 0;
        return
    end
end

%% Cr�ation du fichier XML d�crivant la donn�e 

nomFicXml = fullfile(nomDir, 'Clock.xml');
xml_write(nomFicXml, Info);
flag = exist(nomFicXml, 'file');
if ~flag
    messageErreurFichier(nomFicXml);
    return
end

%% Cr�ation du r�pertoire Clock

nomDirClock = fullfile(nomDir, 'Clock');
if ~exist(nomDirClock, 'dir')
    status = mkdir(nomDirClock);
    if ~status
        messageErreurFichier(nomDirClock);
        flag = 0;
        return
    end
end

%% Cr�ation des fichiers binaires des signaux

for i=1:length(Info.Signals)
    flag = writeSignal(nomDir, Info.Signals(i), Data.(Info.Signals(i).Name));
    if ~flag
        return
    end
end
