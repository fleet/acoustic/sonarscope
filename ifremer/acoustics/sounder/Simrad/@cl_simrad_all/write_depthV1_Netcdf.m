function flag = write_depthV1_Netcdf(this, Data, varargin)

[varargin, Memmapfile] = getPropertyValue(varargin, 'Memmapfile', 0); %#ok<ASGLU>

%% Test d'existence du fichier Netcdf

[nomDir, nom] = fileparts(this.nomFic);
nomFicNc = fullfile(nomDir, 'SonarScope', [nom '.nc']);
if ~exist(nomFicNc, 'file')
    return
end

%% Lecture du XML

package = 'Depth';
[flag, XML] = SSc_ReadDatagrams(this.nomFic, ['Ssc_' package], 'XMLOnly', 1);
if ~flag
    return
end
   
%% Ecriture des signaux

for k=1:length(XML.Signals)
    X = Data.(XML.Signals(k).Name);
	flag = save_signalNetcdf(this, ['Ssc_' package], XML.Signals(k).Name, X(:,:,:,:));
    if ~flag
        message(XML.Signals(k).Name, nomFicNc)
    end
end
 
%% Ecriture des images

for k=1:length(XML.Images)
    X = Data.(XML.Images(k).Name);
	flag = save_signalNetcdf(this, ['Ssc_' package], XML.Images(k).Name, X(:,:,:,:));
    if ~flag
        message(XML.Images(k).Name, nomFicNc)
    end
end
 
%% Ecritude des Strings ?

% Pas utile (jusqu'� nouvel ordre)

flag = 1;

function message(Name, nomFicNc)

str1 = sprintf('La donn�e "%s" n''a pas pu �tre sauvegard�e dans le fichier %s', Name, nomFicNc);
str2 = sprintf('Data "%s" could not be exported into the netcdf file %s', Name, nomFicNc);
my_warndlg(Lang(str1,str2), 0, 'Tag', 'ExportSignalsCacheNetcdf');
