function flag = write_depthV1_bin(this, Data, varargin)

[varargin, Memmapfile] = getPropertyValue(varargin, 'Memmapfile', 0); %#ok<ASGLU>

flag = 0;

%% Lecture du r�pertoire SonarScope

[nomDir, nom] = fileparts(this.nomFic);
nomDir = fullfile(nomDir, 'SonarScope', nom);
if ~exist(nomDir, 'dir')
    return
end

%% Lecture du fichier XML d�crivant la donn�e

nomFicXml = fullfile(nomDir, 'Ssc_Depth.xml');
flag = exist(nomFicXml, 'file');
if ~flag
    return
end
% DataPrecedent = xml_read(nomFicXml);

%% Test de comparaison des champs de DataPrecedent et Data
% TODO

% if 0 % Si un champ est diff�rent on ne fait rien
%     return
% end

%% Lecture du r�pertoire Depth

nomDirDepth = fullfile(nomDir, 'Ssc_Depth');
if ~exist(nomDirDepth, 'dir')
    flag = 0;
    return
end

%% Correction temporaire %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Ne pas oublier de la supprimer quand on supprimera RayPathSampleNb
if isfield(Data, 'Range')
    Data.Range = Data.Range(:,:) * 2;
end

%% Ecriture des fichiers binaires des signaux

n = length(Data.Signals);
flagSignals = true(1,n);
for k=1:n
    if XMLBinUtils.isVarPostprocessed(Data.Signals(k))
        continue
    end
    Signal = Data.(Data.Signals(k).Name);
    flag = writeSscSignal(Signal(:,:), nomDir, Data.Signals(k), Data.Dimensions);
    if ~flag
        %         return
        flagSignals(k) = false;
    end
end

%% Ecriture des fichiers binaires des images

for k=1:length(Data.Images)
    if XMLBinUtils.isVarPostprocessed(Data.Images(k))
        continue
    end
    Image = Data.(Data.Images(k).Name);
    flag = writeSscSignal(Image(:,:), nomDir, Data.Images(k), Data.Dimensions);
    if ~flag
        return
    end
end

flag = all(flagSignals);
