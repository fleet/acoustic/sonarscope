function flag = write_depth_bin(this, Data)

flag = existCacheNetcdf(this.nomFic);
if flag
    flag = write_depthV1_Netcdf(this, Data);
else
    flag = write_depthV1_bin(this, Data);
end
