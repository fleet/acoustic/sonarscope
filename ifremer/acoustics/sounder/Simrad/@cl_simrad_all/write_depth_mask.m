function flag = write_depth_mask(this, Data)

flag = existCacheNetcdf(this.nomFic);
if flag
    flag = write_depth_maskNetcdf(this, Data);
else
    flag = write_depth_maskXMLBin(this, Data);
end
