function flag = write_depth_maskNetcdf(this, Data)

%% Ecriture des fichiers binaires des images

Image = Data.Mask(:,:);

if isa(Image, 'single') || isa(Image, 'double')
    Image(isnan(Image)) = 0;
    Image = uint8(Image);
end

flag = save_signalNetcdf(this, 'Ssc_Depth', 'Mask', Image);
if ~flag
    return
end
