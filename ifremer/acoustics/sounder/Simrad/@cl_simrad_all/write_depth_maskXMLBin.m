function flag = write_depth_maskXMLBin(this, Data)

flag = 0;

%% Lecture du r�pertoire SonarScope

[nomDir, nom] = fileparts(this.nomFic);
nomDir = fullfile(nomDir, 'SonarScope', nom);
if ~exist(nomDir, 'dir')
    return
end

%% Lecture du fichier XML d�crivant la donn�e

nomFicXml = fullfile(nomDir, 'Ssc_Depth.xml');
flag = exist(nomFicXml, 'file');
if ~flag
    return
end

%% Lecture du r�pertoire Depth

nomDirDepth = fullfile(nomDir, 'Ssc_Depth');
if ~exist(nomDirDepth, 'dir')
    flag = 0;
    return
end

%% Ecriture des fichiers binaires des images

k = find(strcmp({Data.Images.Name}, 'Mask'));
Image = Data.(Data.Images(k).Name);
switch Data.Images(k).Storage
    case {'uint8'; 'int8'; 'uint6'; 'int16'; 'uint32'; 'int32'}
        Image = Image(:,:);
        if isa(Image, 'single') || isa(Image, 'double')
            Image(isnan(Image)) = 0;
        end
end
flag = writeSscSignal(Image(:,:), nomDir, Data.Images(k), Data.Dimensions);
if ~flag
    return
end
