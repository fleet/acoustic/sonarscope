function flag = write_installationParameters_bin(this)

%% Lecture de la donn�e InstallationParameters dans les anciens dormats SonarScope

Data = read_installationParameters(this);
if isempty(Data) || isempty(Data.Time) || isempty(Data.Time.timeMat)
    flag = 0;
    return
end

% nbSamples = length(Data.Time.timeMat);

%% Cr�ation de la structure d�crivant la donn�e

Info.Title              = 'InstallationParameters';
Info.EmModel            = Data.EmModel;
Info.SystemSerialNumber = Data.SystemSerialNumber(:,:);
Info.Comments           = 'One per file';

Info.SurveyLineNumber              = Data.SurveyLineNumber;
Info.Time                          = t2str(Data.Time);
Info.SerialNumberOfSecondSonarHead = Data.SerialNumberOfSecondSonarHead;

Data = rmfield(Data, {'Time'; 'SurveyLineNumber'; 'SerialNumberOfSecondSonarHead'; 'EmModel'; 'SystemSerialNumber'});

names = fieldnames(Data);
for k=1:length(names)
    Info.(names{k}) = Data.(names{k});
end

%% Cr�ation du r�pertoire SonarScope

[nomDir, nom] = fileparts(this.nomFic);
nomDir = fullfile(nomDir, 'SonarScope', nom);
if ~exist(nomDir, 'dir')
    status = mkdir(nomDir);
    if ~status
        messageErreur(nomDir)
        flag = 0;
        return
    end
end

%% Cr�ation du fichier XML d�crivant la donn�e 

nomFicXml = fullfile(nomDir, 'InstallationParameters.xml');
xml_write(nomFicXml, Info);
flag = exist(nomFicXml, 'file');
if ~flag
    messageErreur(nomFicXml)
    return
end
