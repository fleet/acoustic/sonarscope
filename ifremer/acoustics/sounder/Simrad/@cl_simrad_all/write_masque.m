% Ecriture du masques assici� � un un fichier .all
%
% Syntax
%   write_masque(aKM, ...)
%
% Input Arguments
%   aKM : Instance de cl_simrad_all
%
% Name-Value Pair Arguments
%   MasqueDetection : Suppression de valeurs masqu�es par MasqueDetection
%   MasqueDepth     : Suppression de valeurs masqu�es par Masque sur
%                     l'histogramme des Depth
%
% Examples
%   nomFicIn  = getNomFicDatabase('EM1002_0001_20040916_054013_raw.all')
%   nomFicOut = [my_tempdir filesep 'toto.all']
%   copyfile(nomFicIn, nomFicOut)
%   aKM = cl_simrad_all('nomFic', nomFicOut);
%   [flag, DataDepth] = read_depth_bin(aKM)
%   figure; imagesc(DataDepth.Depth); colorbar
%   histo1D(DataDepth.Depth)
%
%   [MasqueDetection, ProbaDetection] = cleanDetection(DataDepth.QualityFactor);
%   figure; imagesc(MasqueDetection); colorbar
%   figure; imagesc(ProbaDetection); colorbar
%   figure; imagesc(DataDepth.Depth .* MasqueDetection); colorbar
%
%   write_masque(aKM, 'MasqueDetection', MasqueDetection)
%   [flag, DataDepth] = read_depth_bin(aKM, 'MasqueDetection')
%   figure; imagesc(DataDepth.Depth); colorbar
%   histo1D(DataDepth.Depth)
%
%   [MasqueDepth, ProbaHisto] = cleanHisto(DataDepth.Depth);
%   figure; imagesc(ProbaHisto); colorbar
%   figure; imagesc(MasqueDepth); colorbar
%   figure; imagesc(DataDepth.Depth .* MasqueDepth); colorbar
%   figure; imagesc(DataDepth.Depth .* MasqueDepth .* MasqueDetection); colorbar
%
%   write_masque(aKM, 'MasqueDepth', MasqueDepth)
%   [flag, DataDepth] = read_depth_bin(aKM, 'MasqueDepth')
%   figure; imagesc(DataDepth.Depth); colorbar
%   histo1D(DataDepth.Depth)
%
%   [flag, DataDepth] = read_depth_bin(aKM, 'MasqueDetection', 'MasqueDepth')
%   figure; imagesc(DataDepth.Depth); colorbar
%   histo1D(DataDepth.Depth)
%
%   nomFicVerif = [my_tempname '.all']
%   copyfile(nomFicOut, nomFicVerif)
%   bKM = cl_simrad_all('nomFic', nomFicVerif);
%   [flag, X] = read_depth_bin(bKM)
%   figure; imagesc(X.Depth); colorbar
%
% See also cl_simrad_all cl_simrad_all/plot_position cl_simrad_all/plot_nav Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function write_masque(this, varargin)

%% Check the number of elements

flag = checkIfOneElement(length(this));
if ~flag
    return
end

%% Lecture du masque precedent

Data = read_masque(this);

%% Masque sur la D�tection

[varargin, MasqueDetection] = getPropertyValue(varargin, 'MasqueDetection', []);
if ~isempty(MasqueDetection)
    if ~isequal(size(MasqueDetection), size(Data.MasqueDetection))
        str1 = sprintf('La taille des matrices doit �tre la m�me.  MasqueDetection=%s  Data.MasqueDetection=%s', ...
            num2strCode(size(MasqueDetection)), num2strCode(size(Data.MasqueDetection)));
        str2 = sprintf('The size of the matrices are different.  MasqueDetection=%s  Data.MasqueDetection=%s', ...
            num2strCode(size(MasqueDetection)), num2strCode(size(Data.MasqueDetection)));
        my_warndlg(Lang(str1,str2), 1);
        return
    end
    Data.MasqueDetection = MasqueDetection;
end

%% Masque sur la profondeur

[varargin, MasqueDepth] = getPropertyValue(varargin, 'MasqueDepth', []); %#ok<ASGLU>

if ~isempty(MasqueDepth)
    if ~isequal(size(MasqueDepth), size(Data.MasqueDetection))
        str1 = sprintf('La taille des matrices doit �tre la m�me.  MasqueDetection=%s  Data.MasqueDetection=%s', ...
            num2strCode(size(MasqueDepth)), num2strCode(size(Data.MasqueDepth)));
        str2 = sprintf('The size of the matrices are different.  MasqueDetection=%s  Data.MasqueDetection=%s', ...
            num2strCode(size(MasqueDepth)), num2strCode(size(Data.MasqueDepth)));
        my_warndlg(Lang(str1,str2), 1);
        return
    end
    Data.MasqueDepth = MasqueDepth;
end

%% Sauvegarde de la donn�e dans un fichier .mat

[nomDir, nomFic] = fileparts(this.nomFic);
nomFicMat = fullfile(nomDir, 'SonarScope', [nomFic '_masque.mat']);
if ~isempty(nomFicMat)
    try
        save(nomFicMat, 'Data')
    catch %#ok<CTCH>
        str = sprintf('Erreur ecriture fichier %s', nomFicMat);
        my_warndlg(str, 1);
    end
end
