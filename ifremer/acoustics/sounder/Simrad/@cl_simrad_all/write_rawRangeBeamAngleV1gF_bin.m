function flag = write_rawRangeBeamAngleV1gF_bin(a)


%% Lecture de la donn�e RawRangeBeamAngle dans les anciens dormats SonarScope

[flag, Data] = read_rawRangeBeamAngle(a);
if ~flag || isempty(Data.Time) || isempty(Data.Time.timeMat)
    flag = 0;
    return
end

[nbPings, nbBeams] = size(Data.Range);

%% Cr�ation de la structure d�crivant la donn�e

Info.Title                  = 'RawRangeBeamAngle';
Info.EmModel                = Data.EmModel;
Info.SystemSerialNumber     = Data.SystemSerialNumber(:,:);
Info.nbPings                = nbPings;
Info.nbBeams                = nbBeams;
Info.Comments               = 'Sounder ping rate';

Info.Signals(1).Name        = 'Time';
Info.Signals(1).Storage     = 'double';
Info.Signals(1).Unit        = 'days since JC';
Info.Signals(1).Direction	= 'FirstValue=FirstPing';
Info.Signals(1).FileName    = fullfile('RawRangeBeamAngle', 'Time.bin');

Info.Signals(end+1).Name      = 'PingCounter';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).Direction   = 'FirstValue=FirstPing';
Info.Signals(end).FileName    = fullfile('RawRangeBeamAngle', 'PingCounter.bin');

Info.Signals(end+1).Name      = 'SoundSpeed';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'm/s';
Info.Signals(end).Direction   = 'FirstValue=FirstPing';
Info.Signals(end).FileName    = fullfile('RawRangeBeamAngle', 'SoundSpeed.bin');

Info.Signals(end+1).Name      = 'SamplingRate';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'Hz';
Info.Signals(end).Direction   = 'FirstValue=FirstPing';
Info.Signals(end).FileName    = fullfile('RawRangeBeamAngle', 'SamplingRate.bin');


Info.Images(1).Name        = 'Range';
Info.Images(1).Storage     = 'single';
% Devrait �tre chang� car �a n'a pas trop de sens
Info.Images(1).Unit        = 'One way traveltime in samples';
Info.Images(1).Direction	= 'FirstValue=FirstPing';
Info.Images(1).FileName    = fullfile('RawRangeBeamAngle', 'Range.bin');

Info.Images(end+1).Name      = 'BeamPointingAngle';
Info.Images(end).Storage     = 'single';
Info.Images(end).Unit        = 'deg';
Info.Images(end).Direction   = 'FirstValue=FirstPing';
Info.Images(end).FileName    = fullfile('RawRangeBeamAngle', 'BeamPointingAngle.bin');

Info.Images(end+1).Name      = 'TxBeamIndex';
Info.Images(end).Storage     = 'single';
Info.Images(end).Unit        = '';
Info.Images(end).Direction   = 'FirstValue=FirstPing';
Info.Images(end).FileName    = fullfile('RawRangeBeamAngle', 'TxBeamIndex.bin');

Info.Images(end+1).Name      = 'TransmitTiltAngle';
Info.Images(end).Storage     = 'single';
Info.Images(end).Unit        = 'deg';
Info.Images(end).Direction   = 'FirstValue=FirstPing';
Info.Images(end).FileName    = fullfile('RawRangeBeamAngle', 'TransmitTiltAngle.bin');

Info.Images(end+1).Name      = 'Reflectivity';
Info.Images(end).Storage     = 'single';
Info.Images(end).Unit        = 'dB';
Info.Images(end).Direction   = 'FirstValue=FirstPing';
Info.Images(end).FileName    = fullfile('RawRangeBeamAngle', 'Reflectivity.bin');


%% Cr�ation du r�pertoire SonarScope

[nomDir, nom] = fileparts(a.nomFic);
nomDir = fullfile(nomDir, 'SonarScope', nom);
if ~exist(nomDir, 'dir')
    status = mkdir(nomDir);
    if ~status
        messageErreur(nomDir)
        flag = 0;
        return
    end
end

%% Cr�ation du fichier XML d�crivant la donn�e 

nomFicXml = fullfile(nomDir, 'RawRangeBeamAngle.xml');
xml_write(nomFicXml, Info);
flag = exist(nomFicXml, 'file');
if ~flag
    messageErreur(nomFicXml)
    return
end

%% Cr�ation du r�pertoire RawRangeBeamAngle

nomDirRawRangeBeamAngle = fullfile(nomDir, 'RawRangeBeamAngle');
if ~exist(nomDirRawRangeBeamAngle, 'dir')
    status = mkdir(nomDirRawRangeBeamAngle);
    if ~status
        messageErreur(nomDirRawRangeBeamAngle)
        flag = 0;
        return
    end
end

%% Cr�ation des fichiers binaires des signaux

for i=1:length(Info.Signals)
    flag = writeSignal(nomDir, Info.Signals(i), Data.(Info.Signals(i).Name));
    if ~flag
        return
    end
end

%% Cr�ation des fichiers binaires des images

for i=1:length(Info.Images)
    flag = writeImage(nomDir, Info.Images(i), Data.(Info.Images(i).Name));
    if ~flag
        return
    end
end
