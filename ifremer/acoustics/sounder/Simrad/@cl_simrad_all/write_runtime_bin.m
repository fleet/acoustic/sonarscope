function flag = write_runtime_bin(this, varargin)

[varargin, Mode] = getPropertyValue(varargin, 'Mode', []); %#ok<ASGLU>

[nomDir, nom] = fileparts(this.nomFic);
nomDir = fullfile(nomDir, 'SonarScope', nom);

flag = 1;

if ~isempty(Mode)
%     Info.Signals(end+1).Name      = 'Mode';
%     Info.Signals(end).Dimensions  = 'NbSamples, 1';
%     Info.Signals(end).Storage     = 'uint8';
%     Info.Signals(end).Unit        = '';
%     Info.Signals(end).FileName    = fullfile('Ssc_Runtime','Mode.bin');
%     Info.Signals(end).Tag         = verifKeyWord('SounderMode')

    InfoSignals.Name        = 'Mode';
    InfoSignals.Dimensions  = 'NbSamples, 1';
    InfoSignals.Storage     = 'uint8';
    InfoSignals.Unit        = '';
%     InfoSignals.Direction   = 'FirstValue=FirstPing';
    InfoSignals.FileName    = fullfile('SSc_Runtime', 'Mode.bin');
    InfoSignals.Tag         = verifKeyWord('Auxillary');
    
    flag = writeSignal(nomDir, InfoSignals, Mode);
    if ~flag
        return
    end
end
