function flag = write_surfaceSoundSpeed_bin(a)


%% Lecture de la donn�e SurfaceSoundSpeed dans les anciens dormats SonarScope

Data = read_surfaceSoundSpeed(a);
if isempty(Data) || isempty(Data.Time) || isempty(Data.Time.timeMat)
    flag = 0;
    return
end

nbSamples = length(Data.Time.timeMat);

%% Cr�ation de la structure d�crivant la donn�e

Info.Title                  = 'SurfaceSoundSpeed';
Info.EmModel                = Data.EmModel;
Info.SystemSerialNumber     = Data.SystemSerialNumber(:,:);
Info.NbSamples              = nbSamples;
Info.Comments               = 'Sounder sampling rate';

Info.Signals(1).Name        = 'Time';
Info.Signals(1).Storage     = 'double';
Info.Signals(1).Unit        = 'days since JC';
Info.Signals(1).Direction	= 'FirstValue=FirstPing';
Info.Signals(1).FileName    = fullfile('SurfaceSoundSpeed', 'Time.bin');
Info.Signals(1).Tag         = verifKeyWord('SounderTime');

Info.Signals(end+1).Name    = 'SurfaceSoundSpeed';
Info.Signals(end).Storage   = 'single';
Info.Signals(end).Unit      = 'm/s';
Info.Signals(end).Direction = 'FirstValue=FirstPing';
Info.Signals(end).FileName  = fullfile('SurfaceSoundSpeed', 'SurfaceSoundSpeed.bin');
Info.Signals(1).Tag         = verifKeyWord('SounderSoundSpeed');

%% Cr�ation du r�pertoire SonarScope

[nomDir, nom] = fileparts(a.nomFic);
nomDir = fullfile(nomDir, 'SonarScope', nom);
if ~exist(nomDir, 'dir')
    status = mkdir(nomDir);
    if ~status
        messageErreur(nomDir)
        flag = 0;
        return
    end
end

%% Cr�ation du fichier XML d�crivant la donn�e 

nomFicXml = fullfile(nomDir, 'SurfaceSoundSpeed.xml');
xml_write(nomFicXml, Info);
flag = exist(nomFicXml, 'file');
if ~flag
    messageErreur(nomFicXml)
    return
end

%% Cr�ation du r�pertoire SurfaceSoundSpeed

nomDirSurfaceSoundSpeed = fullfile(nomDir, 'SurfaceSoundSpeed');
if ~exist(nomDirSurfaceSoundSpeed, 'dir')
    status = mkdir(nomDirSurfaceSoundSpeed);
    if ~status
        messageErreur(nomDirSurfaceSoundSpeed)
        flag = 0;
        return
    end
end

%% Cr�ation des fichiers binaires des signaux

for i=1:length(Info.Signals)
    flag = writeSignal(nomDir, Info.Signals(i), Data.(Info.Signals(i).Name));
    if ~flag
        return
    end
end
