% [flag, dataFileName] = FtpUtils.getSScDemoFile('0013_20191022_173330_SimonStevin.all', 'OutputDir', 'D:\Temp\ExData');
% flag = ALL_Cache2Netcdf(dataFileName)
%
% dataFileName = getNomFicDatabase('0011_20110307_211804_IKATERE_02.all');
% flag = ALL_Cache2Netcdf(dataFileName)
%
% [flag, dataFileName] = FtpUtils.getSScDemoFile('0000_20130802_152942_Europe.all', 'OutputDir', 'D:\Temp\ExData');    % Datagrammes QF Ifremer
% flag = ALL_Cache2Netcdf(dataFileName)
%
% dataFileName = getNomFicDatabase('0034_20070406_081642_raw.all');   % Datagrammes SeabedImage 53h
% flag = ALL_Cache2Netcdf(dataFileName)
%
% [flag, dataFileName] = FtpUtils.getSScDemoFile('0000_20150309_200643_Belgica.all', 'OutputDir', 'D:\Temp\ExData');
% flag = ALL_Cache2Netcdf(dataFileName)
%
% [flag, dataFileName] = FtpUtils.getSScDemoFile('0056_20150615_234546_ATL_EM710.all', 'OutputDir', 'D:\Temp\ExData'); % Datagrammes Height (Disque dur 7To)
% flag = ALL_Cache2Netcdf(dataFileName)
%
% [flag, dataFileName] = FtpUtils.getSScDemoFile('0056_20150615_234546_ATL_EM710.all', 'OutputDir', 'D:\Temp\ExData'); % Datagrammes Mag&Phase
% flag = ALL_Cache2Netcdf(dataFileName)
%
% [flag, dataFileName] = FtpUtils.getSScDemoFile('0059_20130203_134006_Thalia.all','OutputDir', 'D:\Temp\ExData');     % Datagrammes StaveData
% ALL_Cache2Netcdf(dataFileName)
%
% [flag, dataFileName] = FtpUtils.getSScDemoFile('AbsC1610.txt',                     'OutputDir', 'D:\Temp\ExData');
% [flag, dataFileName] = FtpUtils.getSScDemoFile('0002_20160310_120246_Belgica.all', 'OutputDir', 'D:\Temp\ExData');
% ALL_Cache2Netcdf(dataFileName)
% [flag, dataFileName] = FtpUtils.getSScDemoFile('0001_20160310_115615_Belgica.all', 'OutputDir', 'D:\Temp\ExData');
% ALL_Cache2Netcdf(dataFileName)
% [flag, dataFileName] = FtpUtils.getSScDemoFile('0000_20160310_115008_Belgica.all', 'OutputDir', 'D:\Temp\ExData');
% ALL_Cache2Netcdf(dataFileName)
%
% [flag, dataFileName] = FtpUtils.getSScDemoFile('0029_20180306_213505_SimonStevin.wcd', 'OutputDir', 'D:\Temp\ExData');
% [flag, dataFileName] = FtpUtils.getSScDemoFile('0029_20180306_213505_SimonStevin.all', 'OutputDir', 'D:\Temp\ExData');
% ALL_Cache2Netcdf(dataFileName)
%
% [flag, dataFileName] = FtpUtils.getSScDemoFile('0030_20180306_213905_SimonStevin.wcd', 'OutputDir', 'D:\Temp\ExData');
% [flag, dataFileName] = FtpUtils.getSScDemoFile('0030_20180306_213905_SimonStevin.all', 'OutputDir', 'D:\Temp\ExData');
% ALL_Cache2Netcdf(dataFileName)

function flag = ALL_Cache2Netcdf(listFileNames, varargin)

global useParallel    %#ok<GVMIS>
global useCacheNetcdf %#ok<GVMIS>

[varargin, useParallel]    = getPropertyValue(varargin, 'useParallel',    useParallel);
[varargin, useCacheNetcdf] = getPropertyValue(varargin, 'useCacheNetcdf', useCacheNetcdf);
[varargin, deleteXMLBin]   = getPropertyValue(varargin, 'deleteXMLBin',   0);
[varargin, deflateLevel]   = getPropertyValue(varargin, 'deflateLevel',   1);
[varargin, Redo]           = getPropertyValue(varargin, 'Redo',           true); %#ok<ASGLU>

if ischar(listFileNames)
    listFileNames = {listFileNames};
end

logFileId = getLogFileId;

useCacheNetcdfHere = useCacheNetcdf;
logFileIdHere = logFileId;

if isempty(useParallel)
    useParallel = 0; % Bug trouv� par Herv� le 23/0302021
end

N = length(listFileNames);
str1 = 'TODO';
str2 = sprintf('Cache directory : Conversion from XML-Bin to Netcdf');
if useParallel && (N > 1)
    [hw, DQ] = create_parforWaitbar(N, Lang(str1,str2));
    parfor k=1:N
        ALL_Cache2Netcdf_unitaire(listFileNames{k}, deflateLevel, Redo, ...
            useCacheNetcdfHere, 'isUsingParfor', true, 'logFileId', logFileIdHere);
        if deleteXMLBin
            SScCacheXMLBinUtils.deleteCachDirectory(listFileNames{k});
        end
        send(DQ, k);
    end
else
    hw = create_waitbar(Lang(str1,str2), 'N', N);
    for k=1:N
        flag(k) = ALL_Cache2Netcdf_unitaire(listFileNames{k}, deflateLevel, Redo, ...
            useCacheNetcdfHere); %#ok<AGROW>
        if flag(k) && deleteXMLBin
            SScCacheXMLBinUtils.deleteCachDirectory(listFileNames{k});
        end
        my_waitbar(k, N, hw)
    end
end
my_close(hw, 'MsgEnd')


function flag = ALL_Cache2Netcdf_unitaire(dataFileName, deflateLevel, Redo, useCacheNetcdfHere, varargin)

global isUsingParfor  %#ok<GVMIS>
global useCacheNetcdf %#ok<GVMIS>
global logFileId %#ok<GVMIS>

[varargin, logFileId]     = getPropertyValue(varargin, 'logFileId',     logFileId);
[varargin, isUsingParfor] = getPropertyValue(varargin, 'isUsingParfor', isUsingParfor); %#ok<ASGLU>

useCacheNetcdf = useCacheNetcdfHere;

msg = sprintf(' Processing "%s" is starting', dataFileName);
logFileId.info('ALL_Cache2Netcdf', msg);

%% Test si le fichier Netcdf a d�j� �t� cr��

if ~Redo
    flag = existCacheNetcdf(dataFileName);
    if flag
        return
    end
end

%% Pour �tre s�r que le XMLBin est cr��

% aKM = cl_simrad_all('nomFic', dataFileName);
% if isempty(aKM)
%     flag = 0;
%     return
% end

%% Cr�ation du fichier Netcdf

[flag, ncFileName] = SScCacheNetcdfUtils.createSScNetcdfFile(dataFileName);
if ~flag
    return
end

%% Version des datagrammes

flag = CacheVersionDatagrammes2Netcdf(ncFileName, dataFileName);
if ~flag
    return
end

%% Transfert des donn�es Runtime

flag = SScCacheNetcdfUtils.Cache2Netcdf(ncFileName, dataFileName, 'Runtime', 'deflateLevel', deflateLevel);
if ~flag
    return
end

%% Transfert des donn�es RawRange

flag = SScCacheNetcdfUtils.Cache2Netcdf(ncFileName, dataFileName, 'RawRangeBeamAngle', 'deflateLevel', deflateLevel);
if ~flag
    return
end

%% Transfert des donn�es Ssc_SurfaceSoundSpeed

flag = SScCacheNetcdfUtils.Cache2Netcdf(ncFileName, dataFileName, 'SurfaceSoundSpeed', 'deflateLevel', deflateLevel);
if ~flag
    return
end

%% Transfert des donn�es Ssc_Clock

flag = SScCacheNetcdfUtils.Cache2Netcdf(ncFileName, dataFileName, 'Clock', 'deflateLevel', deflateLevel);
if ~flag
    return
end

%% Transfert des donn�es Ssc_SoundSpeedProfile

flag = SScCacheNetcdfUtils.Cache2Netcdf(ncFileName, dataFileName, 'SoundSpeedProfile', 'deflateLevel', deflateLevel);
if ~flag
    return
end

%% Transfert des donn�es Ssc_SeabedImage53h

flag = SScCacheNetcdfUtils.Cache2Netcdf(ncFileName, dataFileName, 'SeabedImage53h', 'deflateLevel', deflateLevel);
if ~flag
    return
end
flag = CacheSeabedImage2Netcdf(ncFileName, dataFileName, 'SeabedImage53h', 'deflateLevel', deflateLevel);
if ~flag
    return
end

%% Transfert des donn�es Ssc_SeabedImage59h

flag = SScCacheNetcdfUtils.Cache2Netcdf(ncFileName, dataFileName, 'SeabedImage59h', 'deflateLevel', deflateLevel);
if ~flag
    return
end
flag = CacheSeabedImage2Netcdf(ncFileName, dataFileName, 'SeabedImage59h', 'deflateLevel', deflateLevel);
if ~flag
    return
end

%% Transfert des donn�es Ssc_QualityFactorFromIfr

flag = SScCacheNetcdfUtils.Cache2Netcdf(ncFileName, dataFileName, 'QualityFactorFromIfr', 'deflateLevel', deflateLevel);
if ~flag
    return
end

%% Transfert des donn�es Ssc_Heading

flag = SScCacheNetcdfUtils.Cache2Netcdf(ncFileName, dataFileName, 'Heading', 'deflateLevel', deflateLevel);
if ~flag
    return
end

%% Transfert des donn�es Ssc_Height

flag = SScCacheNetcdfUtils.Cache2Netcdf(ncFileName, dataFileName, 'Height', 'deflateLevel', deflateLevel);
if ~flag
    return
end

%% Transfert des donn�es Ssc_Attitude

flag = SScCacheNetcdfUtils.Cache2Netcdf(ncFileName, dataFileName, 'Attitude', 'deflateLevel', deflateLevel);
if ~flag
    return
end
flag = CacheAttitude2Netcdf(ncFileName, dataFileName, 'deflateLevel', deflateLevel);
if ~flag
    return
end

%% Transfert des donn�es Ssc_Position

flag = SScCacheNetcdfUtils.Cache2Netcdf(ncFileName, dataFileName, 'Position', 'deflateLevel', deflateLevel);
if ~flag
    return
end

%% Transfert des donn�es Ssc_Depth

flag = SScCacheNetcdfUtils.Cache2Netcdf(ncFileName, dataFileName, 'Depth', 'deflateLevel', deflateLevel);
if ~flag
    return
end
flag = CacheDepth2Netcdf(ncFileName, 'deflateLevel', deflateLevel);
if ~flag
    return
end

%% Transfert des donn�es Ssc_WaterColumn

flag = SScCacheNetcdfUtils.Cache2Netcdf(ncFileName, dataFileName, 'WaterColumn', 'deflateLevel', deflateLevel);
if ~flag
    return
end
flag = CacheWaterColumn2Netcdf(ncFileName, dataFileName, 'deflateLevel', deflateLevel);
if ~flag
    return
end

%% Transfert des donn�es Ssc_RawBeamSamples (Mag & Phase)

flag = SScCacheNetcdfUtils.Cache2Netcdf(ncFileName, dataFileName, 'RawBeamSamples', 'deflateLevel', deflateLevel);
if ~flag
    return
end
flag = CacheMagPhase2Netcdf(ncFileName, dataFileName, 'deflateLevel', deflateLevel);
if ~flag
    return
end

%% Transfert des donn�es Ssc_StaveData

flag = SScCacheNetcdfUtils.Cache2Netcdf(ncFileName, dataFileName, 'StaveData', 'deflateLevel', deflateLevel);
if ~flag
    return
end

%%  Transfert des donn�es Ssc_InstallationParameters

flag = SScCacheNetcdfUtils.Cache2Netcdf(ncFileName, dataFileName, 'InstallationParameters', 'deflateLevel', deflateLevel);
if ~flag
    return
end

%%  Transfert des donn�es ALL_ExtraParameters

flag = SScCacheNetcdfUtils.Cache2Netcdf(ncFileName, dataFileName, 'ExtraParameters', 'deflateLevel', deflateLevel);
if ~flag
    return
end

%% Fichier d'index du .all

flag = CacheFileIndex2Netcdf(ncFileName, dataFileName, 'FileIndex', 'deflateLevel', deflateLevel);
if ~flag
    return
end

%% Fichier d'index correspondant aux fichier .wcd

flag = CacheFileIndex2Netcdf(ncFileName, dataFileName, 'FileIndexWCD', 'deflateLevel', deflateLevel);
if ~flag
%     return % Returns 0 if the .wcd does not exist
end

%% Renommage de l'extension .NC en .nc pour indiquer � l'utilisateur que le fichier est termin�

my_rename(ncFileName, strrep(ncFileName, '.NC', '.nc'));

%% The End

msg = sprintf(' Processing "%s" is over', dataFileName);
logFileId.info('ALL_Cache2Netcdf', msg);

flag = 1;
