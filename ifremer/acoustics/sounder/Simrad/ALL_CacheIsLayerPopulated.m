function flag = ALL_CacheIsLayerPopulated(dataFileName, Images, nomLayerAll)

flag = existCacheNetcdf(dataFileName);
if flag
    indStructImages = find(strcmp({Images(:).Name}, nomLayerAll), 1);
    if ~isempty(indStructImages)
        if ~Images(indStructImages).Populated % Donn�e provenant de Netcdf
            message_LayerNotAvailable(nomLayerAll, 0, 'FileName', dataFileName);
            flag = 0;
            return
        end
    end
    flag = 1;
else
    flag = 1;
end
