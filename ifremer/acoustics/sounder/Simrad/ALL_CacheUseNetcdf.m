function ALL_CacheUseNetcdf(x)

global useCacheNetcdf %#ok<GVMIS>

if isempty(x)
    x = 3; % Modif JLA le 05/10/2020 : Netcdf and delete XMLBin par d�faut � partir du 05/10/2020
end

useCacheNetcdf = x;

str{1} = 'Create XML-Bin only';
str{2} = 'Create Netcdf and keep XML-Bin';
str{3} = 'Create Netcdf and delete XML-Bin';
msg = sprintf('useCacheNetcdf set to "%s"', str{x});
logFileId = getLogFileId;
logFileId.info('ALL_CacheUseNetcdf', msg);
