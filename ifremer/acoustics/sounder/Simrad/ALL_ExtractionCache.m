% Pas utilis� mais pourrait �tre utile pour split des .all

function DataOut = ALL_ExtractionCache(DataIn, subPings, varargin)

[varargin, Margin] = getPropertyValue(varargin, 'Margin', 0); %#ok<ASGLU>

if Margin > 0
    subPings = max(1,subPings(1)-Margin):min(DataIn.Dimensions.nbPings,subPings(end)+Margin);
end

DataOut = DataIn;

if strcmp(DataIn.Title, 'WaterColumn')
    nbOfSamplesPerBeam = uint64(DataIn.NumberOfSamples(:,:));
    nbOfSamplesPerPing = sum(nbOfSamplesPerBeam,2);
    offsetSamplesPing  = [0; cumsum(nbOfSamplesPerPing)];
    kSampleDeb = (1+offsetSamplesPing(subPings(1)));
    kSampleFin = offsetSamplesPing(subPings(end)+1);
    DataOut.Amplitude = DataIn.Amplitude(kSampleDeb:kSampleFin);
    DataOut.idebAmp(subPings) = 1 + [0; cumsum(nbOfSamplesPerPing(subPings(1:end-1)))];
end


DataOut.Dimensions.nbPings = length(subPings);

SignalNames = {};
for k=1:length(DataOut.Signals)
    if strcmp(DataOut.Signals(k).Dimensions(1).Name, 'nbPings')
        Name = DataOut.Signals(k).Name;
        DataOut.(Name) = DataOut.(Name)(subPings,:);
        SignalNames{end+1} = Name; %#ok<AGROW>
    end
end

DataOut.Datetime = datetime(DataOut.Time.timeMat, 'ConvertFrom', 'datenum');


for k=1:length(DataOut.Images)
    if strcmp(DataOut.Images(k).Dimensions(1).Name, 'nbPings')
        Name = DataOut.Images(k).Name;
        if isfield(DataOut, Name) && isempty(find(strcmp(SignalNames, Name), 1)) % Obligatoire � cause de bidouille datagramme Raw
            DataOut.(Name) = DataOut.(Name)(subPings,:);
        end
    end
end

if strcmp(DataOut.Title, 'RawRangeBeamAngle') % Cas du datagramme Raw
    DataOut.TransmitSectorNumber      = DataOut.TransmitSectorNumber(subPings,:);
    DataOut.TwoWayTravelTimeInSeconds = DataOut.TwoWayTravelTimeInSeconds(subPings,:);
    DataOut.PingSequence              = DataOut.PingSequence(subPings,:);
end

if strcmp(DataOut.Title, 'WaterColumn') % Cas du datagramme Raw
    DataOut.OrdreBeam = DataOut.OrdreBeam(subPings,:);
end
