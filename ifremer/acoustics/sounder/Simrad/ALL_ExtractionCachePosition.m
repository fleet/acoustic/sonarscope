% Pas utilis� mais pourrait �tre utile pour split des .all

function DataOut = ALL_ExtractionCachePosition(DataIn, DatetimeLim)

DataOut = DataIn;

sub = find((DataIn.Datetime >= DatetimeLim(1)) & (DataIn.Datetime <= DatetimeLim(end)));

%{
x = [1 2 2 3 4 5 5 5 6]
subBad = find(diff(x) <= 0);
x(subBad+1) = []
%}

subBad = find(diff(DataIn.Datetime(sub)) <= 0);
sub(subBad+1) = [];

DataOut.Dimensions.nbPings = length(sub);

SignalNames = {};
for k=1:length(DataOut.Signals)
    if strcmp(DataOut.Signals(k).Dimensions(1).Name, 'NbSamples')
        Name = DataOut.Signals(k).Name;
        DataOut.(Name) = DataOut.(Name)(sub,:);
        SignalNames{end+1} = Name; %#ok<AGROW>
    end
end

DataOut.Datetime    = DataIn.Datetime(sub);
DataOut.GPSDatetime = DataIn.GPSDatetime(sub);

DataOut.DGPSRefStationIdentity = DataIn.DGPSRefStationIdentity(sub,:);
