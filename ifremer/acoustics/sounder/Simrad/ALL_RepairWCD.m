% TODO : N'est plus utilis� : conserver quelques temps et supprimer ensuite

function flag = ALL_RepairWCD(~, listFilenames)

% useParallel = get_UseParallelTbx;
% useCacheNetcdfHere = useCacheNetcdf;

if ~iscell(listFilenames)
    listFilenames = {listFilenames};
end

N = length(listFilenames);
str1 = 'R�paration des WCD.';
str2 = 'Repair the WCD';

ALL.checkFiles(listFilenames, 'ShowProgressBar', 1);

% TODO : parall�lisation neutralis�e pour le moment car il y a plein de pb d'I/O Netcdf

% if useParallel && (N > 1)
%     [hw, DQ] = create_parforWaitbar(N, Lang(str1,str2));
%     parfor k=1:N  %, min(useParallel, 8))
%         send(DQ, k);
%         nomFicAll = listFilenames{k};
%         ALL_RepairWCD_unitaire(nomFicAll, 'isUsingParfor', true, ...
%             'useParallel', useParallel, 'useCacheNetcdf', useCacheNetcdfHere);
%     end
%     flag = 1;
%     my_close(hw, 'MsgEnd')
% else
    hw = create_waitbar(Lang(str1,str2), 'N', N);
    for k=1:N
        my_waitbar(k, N, hw);
        flag = ALL_RepairWCD_unitaire(listFilenames{k});
    end
    my_close(hw, 'MsgEnd')
% end


function deleteAllFileAndCache(nomFicAll)

[pathname, filename] = fileparts(nomFicAll);

if exist(nomFicAll, 'file')
    delete(nomFicAll);
else
    nomFicAllBad = strrep(nomFicAll, '.all', '_all.bad');
    if exist(nomFicAllBad, 'file')
        delete(nomFicAllBad);
    end
end

if exist(nomFicAll, 'file')
    delete(nomFicAll);
else
    nomFicAllBad = strrep(nomFicAll, '.all', '_all.bad');
    if exist(nomFicAllBad, 'file')
        delete(nomFicAllBad);
    end
end

nomFicWCD = strrep(nomFicAll, '.all', '.wcd');
if exist(nomFicWCD, 'file')
    delete(nomFicWCD);
else
    nomFicWCDBad = strrep(nomFicWCD, '.all', '_wcd.bad');
    if exist(nomFicWCDBad, 'file')
        delete(nomFicWCDBad);
    end
end

nomFicNc = fullfile(pathname, 'SonarScope', [filename '.nc']);
if exist(nomFicNc, 'file')
    delete(nomFicNc);
end

nomFicXML = fullfile(pathname, 'SonarScope', [filename '.xml']);
if exist(nomFicXML, 'file')
    delete(nomFicXML);
end

nomDirAll = fullfile(pathname, 'SonarScope', [filename '']);
if exist(nomDirAll, 'dir')
    rmdir(nomDirAll, 's');
end

nomFicAllSplitted = [nomFicAll 'Splitted'];
if exist(nomFicAllSplitted, 'file')
    delete(nomFicAllSplitted);
end

nomFicWCDSplitted = [nomFicWCD 'Splitted'];
if exist(nomFicWCDSplitted, 'file')
    delete(nomFicWCDSplitted);
end


function renameAllFileAndCache(nomFicIn, nomFicOut)

my_rename(nomFicIn, nomFicOut)

nomFicWCDIn = strrep(nomFicIn, '.all', '.wcd');
if exist(nomFicWCDIn, 'file')
    nomFicWCDOut = strrep(nomFicOut, '.all', '.wcd');
    my_rename(nomFicWCDIn, nomFicWCDOut)
end

[pathnameIn, filenameIn]   = fileparts(nomFicIn);
[pathnameOut, filenameOut] = fileparts(nomFicOut);

nomFicInNc  = fullfile(pathnameIn,  'SonarScope', [filenameIn  '.nc']);
nomFicOutNc = fullfile(pathnameOut, 'SonarScope', [filenameOut '.nc']);
if exist(nomFicInNc, 'file')
    my_rename(nomFicInNc, nomFicOutNc);
end

nomFicInNc  = fullfile(pathnameIn,  'SonarScope', [filenameIn  '.xml']);
nomFicOutNc = fullfile(pathnameOut, 'SonarScope', [filenameOut '.xml']);
if exist(nomFicInNc, 'file')
    my_rename(nomFicInNc, nomFicOutNc);
end

nomDirInNc  = fullfile(pathnameIn,  'SonarScope', filenameIn);
nomDirOutNc = fullfile(pathnameOut, 'SonarScope', filenameOut);
if exist(nomDirInNc, 'file')
    my_rename(nomDirInNc, nomDirOutNc);
end


function flag = ALL_RepairWCD_unitaire(nomFicAll, varargin)

global isUsingParfor  %#ok<GVMIS>
global useParallel    %#ok<GVMIS>
global useCacheNetcdf %#ok<GVMIS>

% fprintf('Avant : isUsingParfor=%d  useParallel=%d   useCacheNetcdf=%d\n', isUsingParfor, useParallel, useCacheNetcdf);

[varargin, useCacheNetcdf] = getPropertyValue(varargin, 'useCacheNetcdf', useCacheNetcdf);
[varargin, useParallel]    = getPropertyValue(varargin, 'useParallel',    useParallel);
[varargin, isUsingParfor]  = getPropertyValue(varargin, 'isUsingParfor',  isUsingParfor); %#ok<ASGLU>

% fprintf('Apr�s : isUsingParfor=%d   useParallel=%d   useCacheNetcdf=%d\n', isUsingParfor, useParallel, useCacheNetcdf);

fprintf('Begin of "Repair WCD" %s\n', nomFicAll);
        
aKM = cl_simrad_all('nomFic',  nomFicAll);
if isempty(aKM) 
    flag = 0;
    return
end

[flag, DataDepth] = read_depth_bin(aKM, 'Memmapfile', -1);
if ~flag
    fprintf('Error of "Repair WCD" %s\n', nomFicAll);
    return
end

[flag, selectDatagram] = selectDatagramFromFile(aKM, 'DisplayTable', 0, 'TimeOnly', 1);
if ~flag
    fprintf('Error of "Repair WCD" %s\n', nomFicAll);
    return
end

SplitTime = datenum(DataDepth.Datetime(2));
clear DataDepth
saucissonne_bin(aKM, [], SplitTime, selectDatagram, 'TimeOnly', 1);
clear aKM

deleteAllFileAndCache(nomFicAll)

nomFicAll_01 = strrep(nomFicAll, '.all', '_01.all');
deleteAllFileAndCache(nomFicAll_01)

renameAllFileAndCache(strrep(nomFicAll, '.all', '_02.all'), nomFicAll)

fprintf('End of "Repair WCD" %s\n', nomFicAll);
