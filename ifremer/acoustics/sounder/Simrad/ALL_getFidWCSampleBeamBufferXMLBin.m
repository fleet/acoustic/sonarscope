function [flag, fid] = ALL_getFidWCSampleBeamBufferXMLBin(nomFic, varargin)

[varargin, Datagrams] = getPropertyValue(varargin, 'XML_ALL_WaterColumn', []); %#ok<ASGLU>

flag = 0;
fid = [];

[nomDir, nom] = fileparts(nomFic);
nomDir = fullfile(nomDir, 'SonarScope', nom);
if ~exist(nomDir, 'dir')
    return
end

nomFicXml = fullfile(nomDir, 'ALL_WaterColumn.xml');
flag = exist(nomFicXml, 'file');
if ~flag
    nomFicXml = fullfile(nomDir, 'WCD_WaterColumn.xml');
    flag = exist(nomFicXml, 'file');
    if ~flag
        return
    end
end
if isempty(Datagrams)
    Datagrams = xml_mat_read(nomFicXml);
    % TODO GLU : c'est ici qu'on vient lire les donn�es WC
end

[nomDirSignal, nomFicSignal] = fileparts(nomFicXml);
nomDirSignal = fullfile(nomDirSignal, nomFicSignal);
iTableSampleAmplitude = findIndVariable(Datagrams.Tables, 'SampleAmplitude');

nomFicAmp = fullfile(nomDirSignal, Datagrams.Tables(iTableSampleAmplitude).FileName);
fid = fopen(nomFicAmp, 'r');
if fid == -1
    flag = 0;
    return
end
