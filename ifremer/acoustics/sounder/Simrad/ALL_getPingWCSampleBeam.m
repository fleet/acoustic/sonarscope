function [flag, ImageWCSampleBeam] = ALL_getPingWCSampleBeam(DataWC, iPingWC, S)

flag = 1;

idebAmp2 = 1;
PingStartRangeNumber = DataWC.StartRangeNumber(iPingWC,:);
PingNumberOfSamples  = DataWC.NumberOfSamples(iPingWC,:);
if isfield(DataWC, 'OrdreBeam')
    [~, iBeamSort] = sort(DataWC.OrdreBeam(iPingWC, :));
else
    iBeamSort = 1:DataWC.Dimensions.nbRx;
end
nbSamples = max(DataWC.StartRangeNumber(iPingWC,:) + DataWC.NumberOfSamples(iPingWC,:));
nbBeams = DataWC.Dimensions.nbRx;
ImageWCSampleBeam = NaN(nbSamples, nbBeams, 'single');

for iBeam=1:nbBeams
    if ~isnan(PingStartRangeNumber(iBeam))
        sampleDeb = PingStartRangeNumber(iBeam) + 1;
        sampleFin = PingStartRangeNumber(iBeam) + PingNumberOfSamples(iBeam);
        try
            B = S(idebAmp2:(idebAmp2 + double(PingNumberOfSamples(iBeam)) - 1));
        catch
            flag = 0;
            break
        end
        ImageWCSampleBeam(sampleDeb:sampleFin,iBeamSort(iBeam)) = (B * 0.5) - DataWC.TVGOffset(iPingWC);
        idebAmp2 = idebAmp2+ double(PingNumberOfSamples(iBeam));
    end
end
