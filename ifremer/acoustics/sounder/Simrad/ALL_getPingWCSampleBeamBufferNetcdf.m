function [flag, A] = ALL_getPingWCSampleBeamBufferNetcdf(DataWC, iPingWC)

if isfield(DataWC, 'idebAmp')
    idebAmp = DataWC.idebAmp(iPingWC);
else
    X = sum(double(DataWC.NumberOfSamples(1:iPingWC-1,:)), 'omitnan');
    idebAmp = 1 + sum(X);
end
n = sum(double(DataWC.NumberOfSamples(iPingWC,:)), 'omitnan');
try
    A = single(DataWC.Amplitude(idebAmp:(idebAmp+n-1)));
catch
    flag = 0;
    A = [];
    return
end

flag = 1;
