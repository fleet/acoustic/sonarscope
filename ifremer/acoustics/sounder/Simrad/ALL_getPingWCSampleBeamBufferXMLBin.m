function [flag, A] = ALL_getPingWCSampleBeamBufferXMLBin(DataWC, iPingWC, fid)

if isfield(DataWC, 'idebAmp')
    idebAmp = DataWC.idebAmp(iPingWC);
else
    idebAmp = 1 + sum(sum(double(DataWC.NumberOfSamples(1:iPingWC-1,:)), 'omitnan'));
end
n = sum(double(DataWC.NumberOfSamples(iPingWC,:)), 'omitnan');
flag = fseek(fid, idebAmp-1, 'bof');
if flag ~= 0
    flag = 0;
    A = [];
    return
end
A = fread(fid, n, 'int8=>single');

flag = 1;
