function flagRTK = ALL_isRTKAvailable(nomFicAll)

flag = existCacheNetcdf(nomFicAll);
if flag
    flagRTK = ALL_isRTKAvailableNetcdf(nomFicAll);
else
    flagRTK = ALL_isRTKAvailableXMLBin(nomFicAll);
end
