function flagRTK = ALL_isRTKAvailableNetcdf(nomFicAll)

flagRTK = 0;

%% Lecture du r�pertoire SonarScope

[nomDir, nom] = fileparts(nomFicAll);
nomFicNc = fullfile(nomDir, 'SonarScope', [nom '.nc']);
if ~exist(nomFicNc, 'file')
    return
end

%% Test existence QualityIndicator

[flag, DataPosition] = read_position_bin(cl_simrad_all([]), 'nomFicAll', nomFicAll);
if ~flag || ~isfield(DataPosition, 'QualityIndicator')
    return
end

%% Test existence DataHeight

if any(DataPosition.QualityIndicator == 4) || any(DataPosition.QualityIndicator == 5)
    [flag, DataHeight] = SSc_ReadDatagrams(nomFicAll, 'Ssc_Height'); % Memmapfile=0 peut-�tre ?
    if flag && ~isempty(DataHeight) && ~isempty(DataHeight.Height)
        flagRTK = 1;
    end
end
