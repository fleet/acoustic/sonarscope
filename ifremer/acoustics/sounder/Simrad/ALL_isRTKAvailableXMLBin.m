function flagRTK = ALL_isRTKAvailableXMLBin(nomFicAll)

flagRTK = 0;

%% Lecture du r�pertoire SonarScope

[nomDir, nom] = fileparts(nomFicAll);
nomDir = fullfile(nomDir, 'SonarScope', nom);
if ~exist(nomDir, 'dir')
    return
end

%% Test existence fichier RTK

nomFicRTK = fullfile(nomDir, 'presenceRTK.txt');
if exist(nomFicRTK, 'file')
    fid = fopen(nomFicRTK, 'r');
    flagRTK = fread(fid, 1, 'single');
    fclose(fid);
else
    [flag, DataPosition] = read_position_bin(cl_simrad_all([]), 'Memmapfile', 1, 'nomFicAll', nomFicAll);
    if ~flag || ~isfield(DataPosition, 'QualityIndicator')
        return
    end
    if any(DataPosition.QualityIndicator == 4) || any(DataPosition.QualityIndicator == 5)
        [flag, DataHeight] = SSc_ReadDatagrams(nomFicAll, 'Ssc_Height', 'Memmapfile', 1); % Memmapfile=0 peut-�tre ?
        if flag && ~isempty(DataHeight) && ~isempty(DataHeight.Height)
            flagRTK = 1;
        end
    end
    
    fid = fopen(nomFicRTK, 'w');
    fwrite(fid, flagRTK, 'single');
    fclose(fid);
end
