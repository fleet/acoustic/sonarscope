% TODO : ramener les traitements au niveau sup�rieur
%
% Pr�traitement des fichiers .all de Simrad.
% Cette operation n'est pas necessaire mais on gagnera du temps lors des
% premieres lectures de fichier
%
% [Fig1, RepExport] = ALL_plotDatagram(...)
%
% Name-Value Pair Arguments
%   nom : Nom d'un repertoire contenant des .all ou nom d'un fichier .all
%   ou tableau de cellules contenant des noms de fichiers .all
%
% Name-Value Pair Arguments
%   Option : 'Index' | 'Position' | 'Runtime' | 'Attitude' |
%            'Clock' | 'SoundSpeedProfile' | 'SurfaceSoundSpeed' | 'Height'
%
% Examples TODO
%
% See also cl_simrad_all Authors
% Authors : JMA
%------------------------------------------------------------------------------------------

function ALL_plotDatagram(this, Option)

if any(strcmp(Option, 'Index'))
    % Visualisation graphique des types de datagrammes
    if get_LevelQuestion >= 3
        plot_index(this);
    end
    % Histogramme des types de datagrammes
    histo_index(this);
end

if any(strcmp(Option, 'Position'))
    % Affichage des donn�es contenues dans les datagrammes "position"
    ALL_PlotNav(this);
end

if any(strcmp(Option, 'Installation parameters'))
    % Affichage des donn�es contenues dans les datagrammes "Installation parameters"
    plot_installationParameters(this);
end

if any(strcmp(Option, 'Runtime'))
    % Affichage des donn�es contenues dans les datagrammes "runtime"
    plot_runtime(this);
end

if any(strcmp(Option, 'Attitude'))
    % Affichage des donn�es contenues dans les datagrammes "attitude"
    plot_attitude(this);
end

if any(strcmp(Option, 'Clock'))
    % Affichage des donn�es contenues dans les datagrammes "clock"
    plot_clock(this);
end

if any(strcmp(Option, 'SoundSpeedProfile'))
    % Affichage des donn�es vecteurs contenues dans les datagrammes "soundSpeedProfile"
    plot_soundSpeedProfile(this);
end

if any(strcmp(Option, 'SurfaceSoundSpeed'))
    % Affichage des donn�es vecteurs contenues dans les datagrammes "Surface Sound Speed"
    plot_surfaceSoundSpeed(this);
    plot_surfaceSoundSpeed_3D(this);
end

if any(strcmp(Option, 'Height'))
    % Affichage des donn�es vecteurs contenues dans les datagrammes "Height"
    plot_height(this);
end

if any(strcmp(Option, 'SeabedImageParameters'))
    % Affichage des donn�es vecteurs contenues dans les datagrammes "seabedImage"
    subV1 = [];
    subV2 = [];
    NbFic = length(this);
    hw = create_waitbar('Plot .all Runtime Parameters', 'N', NbFic);
    for iFic=1:NbFic
        my_waitbar(iFic, NbFic, hw);
        Version = version_datagram(this(iFic));
        switch Version
            case 'V1'
                subV1(end+1) = iFic; %#ok<AGROW>
            case 'V2'
                subV2(end+1) = iFic; %#ok<AGROW>
        end
    end
    my_close(hw, 'MsgEnd');
    plot_seabedImage_V1(this(subV1));
    plot_seabedImage_V2(this(subV2));
end
