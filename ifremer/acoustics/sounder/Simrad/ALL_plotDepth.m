function ALL_plotDepth(nomFic)

aKM = cl_simrad_all('nomFic', nomFic);
[flag, Data] = read_depth_bin(aKM, 'Memmapfile', -1);
if ~flag
    return
end

FigName = sprintf('Depth Datagrams : Sondeur EM%d,  Serial Number %d, Ship : %s', ...
    Data.EmModel, Data.SystemSerialNumber(1), ShipName(cl_sounder([]), Data.SystemSerialNumber(1)));

Fig = FigUtils.createSScFigure;
set(Fig, 'name', FigName)

T = Data.Datetime;

%% Signaux

N = 8;
M = 3;
k = 1;
haxe(1) = subplot(N,M,k);
PlotUtils.createSScPlot(Data.PingCounter(:,:));
grid on; title('PingCounter')

k = k + 1;
haxe(k) = subplot(N,M,k);
PlotUtils.createSScPlot(Data.Heading(:,:));
grid on; title('Heading (deg)')

k = k + 1;
haxe(k) = subplot(N,M,k);
PlotUtils.createSScPlot(Data.SoundSpeed(:,:));
grid on; title('SoundSpeed (m/s)')

k = k + 1;
haxe(k) = subplot(N,M,k);
PlotUtils.createSScPlot(Data.TransducerDepth(:,:));
grid on; title('TransducerDepth (m)')

k = k + 1;
haxe(k) = subplot(N,M,k);
PlotUtils.createSScPlot(Data.MaxNbBeamsPossible(:,:));
grid on; title('MaxNbBeamsPossible')

k = k + 1;
haxe(k) = subplot(N,M,k);
PlotUtils.createSScPlot(Data.NbBeams(:,:));
grid on; title('NbBeams')

k = k + 1;
haxe(k) = subplot(N,M,k);
PlotUtils.createSScPlot(Data.SamplingRate(:,:));
grid on; title('SamplingRate (Hz)')

k = k + 1;
haxe(k) = subplot(N,M,k);
PlotUtils.createSScPlot(Data.ScanningInfo(:,:));
grid on; title('ScanningInfo')

k = k + 1;
haxe(k) = subplot(N,M,k);
PlotUtils.createSScPlot(Data.MaskPingsReflectivity(:,:));
grid on; title('MaskPingsReflectivity')

%% Images

k = k + 1;
X = Data.Depth(:,:);
s = stats(X);
haxe(k) = subplot(N,M,k);
imagesc(X', s.Quant_01_99);
colormap(jet(256)); colorbar('east'); title('Depth (m)')

k = k + 1;
X = Data.AcrossDist(:,:);
s = stats(X);
haxe(k) = subplot(N,M,k);
imagesc(X', s.Quant_01_99);
colormap(jet(256)); colorbar('east'); title('AcrossDist')

k = k + 1;
X = Data.AlongDist(:,:);
s = stats(X);
haxe(k) = subplot(N,M,k);
imagesc(X', s.Quant_01_99);
colormap(jet(256)); colorbar('east'); title('AlongDist')

k = k + 1;
X = Data.LengthDetection(:,:);
s = stats(X);
haxe(k) = subplot(N,M,k);
imagesc(X', s.Quant_01_99);
colormap(jet(256)); colorbar('east'); title('LengthDetection')

k = k + 1;
X = Data.QualityFactor(:,:);
% s = stats(X);
haxe(k) = subplot(N,M,k);
imagesc(X');
colormap(jet(256)); colorbar('east'); title('QualityFactor')

k = k + 1;
X = Data.BeamIBA(:,:);
% s = stats(X);
haxe(k) = subplot(N,M,k);
imagesc(X');
colormap(jet(256)); colorbar('east'); title('BeamIBA')

k = k + 1;
X = Data.DetectionInfo(:,:);
s = stats(X);
haxe(k) = subplot(N,M,k);
imagesc(X', s.Quant_01_99);
colormap(gray(256)); colorbar('east'); title('DetectionInfo')

k = k + 1;
X = Data.RealTimeCleaningInfo(:,:);
% s = stats(X);
haxe(k) = subplot(N,M,k);
imagesc(X');
colormap(jet(256)); colorbar('east'); title('RealTimeCleaningInfo')

k = k + 1;
X = Data.Reflectivity(:,:);
s = stats(X);
haxe(k) = subplot(N,M,k);
imagesc(X', s.Quant_01_99);
colormap(jet(256)); colorbar('east'); title('Reflectivity (dB)')

k = k + 1;
X = Data.ReflectivityFromSnippets(:,:);
s = stats(X);
haxe(k) = subplot(N,M,k);
imagesc(X', s.Quant_01_99);
colormap(jet(256)); colorbar('east'); title('ReflectivityFromSnippets (dB)')

if isfield(Data, 'ReflectivityFromSnippetsWithoutSpecularRestablishment')
    k = k + 1;
    X = Data.ReflectivityFromSnippetsWithoutSpecularRestablishment(:,:);
    s = stats(X);
    haxe(k) = subplot(N,M,k);
    imagesc(X', s.Quant_01_99);
    colormap(jet(256)); colorbar('east'); title('ReflectivityFromSnippetsWithoutSpecularRestablishment')
end

k = k + 1;
X = Data.AbsorptionCoefficientRT(:,:);
s = stats(X);
haxe(k) = subplot(N,M,k);
imagesc(X', s.Quant_01_99);
colormap(jet(256)); colorbar('east'); title('AbsorptionCoefficientRT')

k = k + 1;
X = Data.AbsorptionCoefficientSSc(:,:);
s = stats(X);
haxe(k) = subplot(N,M,k);
imagesc(X', s.Quant_01_99);
colormap(jet(256)); colorbar('east'); title('AbsorptionCoefficientSSc')

k = k + 1;
X = Data.Mask(:,:);
% s = stats(X);
haxe(k) = subplot(N,M,k);
imagesc(X');
colormap(jet(256)); colorbar('east'); title('Mask')

linkaxes(haxe, 'x'); axis tight; drawnow;
