function ALL_plotRawRange(nomFic)

[flag, Data] = SSc_ReadDatagrams(nomFic, 'Ssc_RawRangeBeamAngle', 'Memmapfile', -1);
if ~flag
    return
end

FigName = sprintf('RawRange Datagrams : Sondeur EM%d,  Serial Number %d, Ship : %s', ...
    Data.EmModel, Data.SystemSerialNumber(1), ShipName(cl_sounder([]), Data.SystemSerialNumber(1)));

Fig = FigUtils.createSScFigure;
set(Fig, 'name', FigName)

T = Data.Datetime;

%% Signaux

N = 7;
M = 3;
k = 1;
haxe(1) = subplot(N,M,k);
PlotUtils.createSScPlot(Data.PingCounter(:,:));
grid on; title('PingCounter')

k = k + 1;
haxe(k) = subplot(N,M,k);
PlotUtils.createSScPlot(Data.SoundSpeed(:,:));
grid on; title('SoundSpeed (m/s)')

k = k + 1;
haxe(k) = subplot(N,M,k);
PlotUtils.createSScPlot(Data.NbBeams(:,:));
grid on; title('NbBeams')

k = k + 1;
haxe(k) = subplot(N,M,k);
PlotUtils.createSScPlot(Data.SamplingRate(:,:));
grid on; title('SamplingRate (Hz)')

k = k + 1;
haxe(k) = subplot(N,M,k);
PlotUtils.createSScPlot(Data.TiltAngle(:,:));
grid on; title('TiltAngle (deg)')

k = k + 1;
haxe(k) = subplot(N,M,k);
PlotUtils.createSScPlot(Data.FocusRange(:,:));
grid on; title('FocusRange (?)')

k = k + 1;
haxe(k) = subplot(N,M,k);
PlotUtils.createSScPlot(Data.SignalLength(:,:));
grid on; title('SignalLength (ms)')

k = k + 1;
haxe(k) = subplot(N,M,k);
PlotUtils.createSScPlot(Data.SectorTransmitDelay(:,:));
grid on; title('SectorTransmitDelay (xxxx)')

k = k + 1;
haxe(k) = subplot(N,M,k);
PlotUtils.createSScPlot(Data.CentralFrequency(:,:));
grid on; title('CentralFrequency (kHz)')

k = k + 1;
haxe(k) = subplot(N,M,k);
PlotUtils.createSScPlot(Data.MeanAbsorptionCoeff(:,:));
grid on; title('MeanAbsorptionCoeff (dB/km)')

k = k + 1;
haxe(k) = subplot(N,M,k);
PlotUtils.createSScPlot(Data.SignalWaveformIdentifier(:,:));
grid on; title('SignalWaveformIdentifier')

k = k + 1;
haxe(k) = subplot(N,M,k);
PlotUtils.createSScPlot(Data.TransmitSectorNumberTx(:,:));
grid on; title('TransmitSectorNumberTx')

k = k + 1;
haxe(k) = subplot(N,M,k);
PlotUtils.createSScPlot(Data.SignalBandwith(:,:));
grid on; title('SignalBandwith (Hz)')

%% Images

k = k + 1;
X = Data.BeamPointingAngle(:,:);
s = stats(X);
haxe(k) = subplot(N,M,k);
imagesc(X', s.Quant_01_99);
colormap(jet(256)); colorbar('east'); title('BeamPointingAngle (deg)')

k = k + 1;
X = Data.TransmitSectorNumberRx(:,:);
s = stats(X);
haxe(k) = subplot(N,M,k);
imagesc(X', s.Quant_01_99);
colormap(jet(256)); colorbar('east'); title('TransmitSectorNumberRx')

k = k + 1;
X = Data.DetectionInfo(:,:);
s = stats(X);
haxe(k) = subplot(N,M,k);
imagesc(X', s.Quant_01_99);
colormap(jet(256)); colorbar('east'); title('DetectionInfo')

k = k + 1;
X = Data.LengthDetection(:,:);
s = stats(X);
haxe(k) = subplot(N,M,k);
imagesc(X', s.Quant_01_99);
colormap(jet(256)); colorbar('east'); title('LengthDetection')

k = k + 1;
X = Data.QualityFactor(:,:);
% s = stats(X);
haxe(k) = subplot(N,M,k);
imagesc(X');
colormap(jet(256)); colorbar('east'); title('QualityFactor')

k = k + 1;
X = Data.TwoWayTravelTime(:,:);
s = stats(X);
haxe(k) = subplot(N,M,k);
imagesc(X', s.Quant_01_99);
colormap(jet(256)); colorbar('east'); title('TwoWayTravelTime (s)')

k = k + 1;
X = Data.Reflectivity(:,:);
s = stats(X);
haxe(k) = subplot(N,M,k);
imagesc(X', s.Quant_01_99);
colormap(gray(256)); colorbar('east'); title('Reflectivity (dB)')

k = k + 1;
X = Data.RealTimeCleaningInfo(:,:);
% s = stats(X);
haxe(k) = subplot(N,M,k);
imagesc(X');
colormap(jet(256)); colorbar('east'); title('RealTimeCleaningInfo')

linkaxes(haxe, 'x'); axis tight; drawnow;
