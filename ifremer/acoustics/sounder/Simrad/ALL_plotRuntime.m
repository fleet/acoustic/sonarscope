function ALL_plotRuntime(nomFic)

[flag, DataRuntime] = SSc_ReadDatagrams(nomFic, 'Ssc_Runtime');
if ~flag
    return
end

fprintf('--- %s ---\n', nomFic);
for k=1:DataRuntime.Dimensions.NbSamples
    fprintf('-------------------------------------------------------\n');
    fprintf('Datagram : %d\n', k);
    fprintf('\t Time                     : %s\n', char(DataRuntime.Datetime(k)));
    fprintf('\t PingCounter              : %d\n', DataRuntime.PingCounter(k));
    fprintf('\t OperatorStationStatus    : %d\n', DataRuntime.OperatorStationStatus(k));
    fprintf('\t ProcessingUnitStatus     : %d\n', DataRuntime.ProcessingUnitStatus(k));
    fprintf('\t BSPStatus                : %d\n', DataRuntime.BSPStatus(k));
    fprintf('\t SonarHeadStatus          : %d\n', DataRuntime.SonarHeadStatus(k));
    fprintf('\t Mode                     : %d\n', DataRuntime.Mode(k));
    if isfield(DataRuntime, 'ModeDatagram')
        fprintf('\t ModeDatagram             : %d\n', DataRuntime.ModeDatagram(k));
    end
    fprintf('\t DualSwath                : %d\n', DataRuntime.DualSwath(k));
    fprintf('\t FilterIdentifier         : %d\n', DataRuntime.FilterIdentifier(k));
    fprintf('\t MinimumDepth             : %f\n', DataRuntime.MinimumDepth(k));
    fprintf('\t MaximumDepth             : %f\n', DataRuntime.MaximumDepth(k));
    fprintf('\t AbsorptionCoeff          : %f\n', DataRuntime.AbsorptionCoeff(k));
    fprintf('\t TransmitPulseLength      : %f\n', DataRuntime.TransmitPulseLength(k));
    fprintf('\t TransmitBeamWidth        : %f\n', DataRuntime.TransmitBeamWidth(k));
    fprintf('\t TransmitPowerMax         : %f\n', DataRuntime.TransmitPowerMax(k));
    fprintf('\t ReceiveBeamWidth         : %f\n', DataRuntime.ReceiveBeamWidth(k));
    fprintf('\t ReceiveBandWidth         : %f\n', DataRuntime.ReceiveBandWidth(k));
    fprintf('\t ReceiverFixGain          : %f\n', DataRuntime.ReceiverFixGain(k));
    fprintf('\t TVGCrossAngle            : %f\n', DataRuntime.TVGCrossAngle(k));
    fprintf('\t SourceSoundSpeed         : %f\n', DataRuntime.SourceSoundSpeed(k));
    fprintf('\t MaximumPortSwath         : %f\n', DataRuntime.MaximumPortSwath(k));
    fprintf('\t BeamSpacing              : %f\n', DataRuntime.BeamSpacing(k));
    fprintf('\t MaximumPortCoverage      : %f\n', DataRuntime.MaximumPortCoverage(k));
    fprintf('\t YawStabMode              : %f\n', DataRuntime.YawStabMode(k));
    fprintf('\t MaximumStarboardCoverage : %f\n', DataRuntime.MaximumStarboardCoverage(k));
    fprintf('\t MaximumStarboardSwath    : %f\n', DataRuntime.MaximumStarboardSwath(k));
    fprintf('\t PitchStabMode            : %f\n', DataRuntime.PitchStabMode(k));
    fprintf('\t DurotongSpeed            : %d\n', DataRuntime.DurotongSpeed(k));
    fprintf('\t HiLoFreqAbsCoefRatio     : %d\n', DataRuntime.HiLoFreqAbsCoefRatio(k));

end
