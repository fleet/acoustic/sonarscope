function ALL_plotSeabed(nomFic)

[flag, Data] = SSc_ReadDatagrams(nomFic, 'Ssc_SeabedImage59h', 'Memmapfile', -1);
if ~flag
    return
end

FigName = sprintf('Seabed Image Datagrams : Sondeur EM%d,  Serial Number %d, Ship : %s', ...
    Data.EmModel, Data.SystemSerialNumber(1), ShipName(cl_sounder([]), Data.SystemSerialNumber(1)));

Fig = FigUtils.createSScFigure;
set(Fig, 'name', FigName)

T = Data.Datetime;

N= 6;
k = 1;
haxe(1) = subplot(N,2,k);
PlotUtils.createSScPlot(Data.PingCounter(:,:));
grid on; title('PingCounter')

k = k + 1;
haxe(k) = subplot(N,2,k);
PlotUtils.createSScPlot(Data.SamplingRate(:,:));
grid on; title('SamplingRate (Hz)')

k = k + 1;
haxe(k) = subplot(N,2,k);
PlotUtils.createSScPlot(Data.TVGN(:,:));
grid on; title('Rn (samples)')

% k = k + 1;
% haxe(k) = subplot(N,2,k);
% h = PlotUtils.createSScPlot(Data.TVGNRecomputed(:,:));
% set(h, 'ButtonDownFcn', str)
% grid on; title('TVGNRecomputed (samples)')

% k = k + 1;
% haxe(k) = subplot(N,2,k);
% % h = PlotUtils.createSScPlot(Data.TVGStop(:,:));
% % set(h, 'ButtonDownFcn', str)
% % grid on; title('TVGStop (samples)')

k = k + 1;
haxe(k) = subplot(N,2,k);
PlotUtils.createSScPlot(Data.BSN(:,:));
grid on; title('BSN (dB)')

k = k + 1;
haxe(k) = subplot(N,2,k);
PlotUtils.createSScPlot(Data.BSO(:,:));
grid on; title('BSO (dB)')

k = k + 1;
haxe(k) = subplot(N,2,k);
PlotUtils.createSScPlot(Data.TxBeamWidth(:,:));
grid on; title('TxBeamWidth (deg)')

k = k + 1;
haxe(k) = subplot(N,2,k);
PlotUtils.createSScPlot(Data.TVGCrossOver(:,:));
grid on; title('TVGCrossOver (deg)')

if isfield(Data, 'NbFaisceaux') % Anciens Ssc formats
    k = k + 1;
    haxe(k) = subplot(N,2,k);
    PlotUtils.createSScPlot(Data.NbFaisceaux(:,:));
    grid on; title('NbFaisceaux')
end
if isfield(Data, 'NbBeams') % Nouveaux Ssc formats
    k = k + 1;
    haxe(k) = subplot(N,2,k);
    PlotUtils.createSScPlot(Data.NbBeams(:,:));
    grid on; title('NbBeams')
end

k = k + 1;
X = Data.InfoBeamSortDirection(:,:);
s = stats(X);
haxe(k) = subplot(N,2,k);
imagesc(X', s.Quant_01_99);
colormap(jet(256)); title('InfoBeamSortDirection')

k = k + 1;
X = Data.InfoBeamDetectionInfo(:,:);
s = stats(X);
haxe(k) = subplot(N,2,k);
imagesc(X', s.Quant_01_99);
colormap(jet(256)); title('InfoBeamDetectionInfo')

k = k + 1;
X = Data.InfoBeamNbSamples(:,:);
s = stats(X);
haxe(k) = subplot(N,2,k);
imagesc(X', s.Quant_01_99);
colormap(jet(256)); title('InfoBeamNbSamples')

k = k + 1;
X = Data.InfoBeamCentralSample(:,:);
s = stats(X);
haxe(k) = subplot(N,2,k);
imagesc(X', s.Quant_01_99);
colormap(jet(256)); title('InfoBeamCentralSample')

linkaxes(haxe, 'x'); axis tight; drawnow;
