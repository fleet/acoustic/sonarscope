% nomFic = 'D:\Temp\SPFE\A\0029_20180306_213505_SimonStevin.all';
% nomFic = 'D:\Temp\SPFE\A\0030_20180306_213905_SimonStevin.all';
% nomFic = 'D:\Temp\SPFE\A\0031_20180306_214305_SimonStevin.all';
% ALL_testDeflateLevel(nomFic)

function ALL_testDeflateLevel(listFileNames)

if ischar(listFileNames)
    listFileNames = {listFileNames};
end

for k=1:length(listFileNames)
    ALL_testDeflateLevel_unitaire(listFileNames{k})
end

figure('Name', 'Synthesis');
h(1) = subplot(3,2,1); grid on; hold on;
xlabel('deflateLevel'); ylabel('Time(s)'); title('Time : Write (s)', 'Interpreter', 'none')
h(2) = subplot(3,2,3); grid on; hold on;
xlabel('deflateLevel'); ylabel('Time(s)'); title('Time : Read (s)', 'Interpreter', 'none')
h(3) = subplot(3,2,5); grid on; hold on;
xlabel('deflateLevel'); ylabel('File size (Mo)'); title('NetcdfSize (Mb)', 'Interpreter', 'none')
hLegend = subplot(3,2,2:2:6); grid on; hold on;
set(hLegend, 'Visible', 'Off');

for k=1:length(listFileNames)
    [~, nomFicSeul] = fileparts(listFileNames{k});
    nomFicTable = fullfile(my_tempdir, ['Compression-' nomFicSeul '.txt']);
    T = readtable(nomFicTable);
    
    fprintf('\n------------------------------------------\n%s\n', nomFicSeul)
    T %#ok<NOPRT>
    
    plot(h(1), 0:9, T.TimeWrite);  drawnow;
    plot(h(2), 0:9, T.TimeRead);   drawnow;
    plot(h(3), 0:9, T.NetcdfSize); drawnow;
    
    plot(hLegend, 0:9, NaN*T.TimeWrite, '*');
    Legend{k} = nomFicSeul; %#ok<AGROW>
end
linkaxes(h, 'x'); axis tight; drawnow;
legend(hLegend, Legend, 'Interpreter', 'None'); drawnow

 
figure('Name', 'Synthesis');
h(1) = subplot(3,2,1); grid on; hold on;
xlabel('deflateLevel'); ylabel('TimeWriteRatio'); title('Time : Write ratio', 'Interpreter', 'none')
h(2) = subplot(3,2,3); grid on; hold on;
xlabel('deflateLevel'); ylabel('TimeReadRatio'); title('Time : Read ratio', 'Interpreter', 'none')
h(3) = subplot(3,2,5); grid on; hold on;
xlabel('deflateLevel'); ylabel('NetcdfSizeRatio'); title('NetcdfSize ratio', 'Interpreter', 'none')
hLegend = subplot(3,2,2:2:6); grid on; hold on;
set(hLegend, 'Visible', 'Off');

for k=1:length(listFileNames)
    [~, nomFicSeul] = fileparts(listFileNames{k});
    nomFicTable = fullfile(my_tempdir, ['Compression-' nomFicSeul '.txt']);
    T = readtable(nomFicTable);
    
    fprintf('\n------------------------------------------\n%s\n', nomFicSeul)
    T %#ok<NOPRT>
    
    plot(h(1), 0:9, T.TimeWriteRatio);  drawnow;
    plot(h(2), 0:9, T.TimeReadRatio);   drawnow;
    plot(h(3), 0:9, T.NetcdfSizeRatio); drawnow;
    
    plot(hLegend, 0:9, NaN*T.TimeWrite, '*');
    Legend{k} = nomFicSeul;
end
linkaxes(h, 'x'); axis tight; drawnow;
legend(hLegend, Legend, 'Interpreter', 'None'); drawnow


function ALL_testDeflateLevel_unitaire(nomFic)

[nomDir, nomFicSeul] = fileparts(nomFic);
nomFicNetcdf = fullfile(nomDir, 'SonarScope', [nomFicSeul '.nc']);

AllSize = sizeFic(nomFic) * 1e-6;
nomFicWcd = fullfile(nomDir, [nomFicSeul '.wcd']);
if exist(nomFicWcd, 'file')
    AllSize = AllSize + sizeFic(nomFicWcd) * 1e-6;
end

figure('Name', nomFicSeul);
h(1) = subplot(2,1,1); grid on; hold on;
xlabel('deflateLevel'); ylabel('Time(s)'); title('Time (s) : Write (red), Read (blue)', 'Interpreter', 'none')
h(2) = subplot(2,1,2); grid on; hold on;
xlabel('deflateLevel'); ylabel('File size (Mo)'); title('NetcdfSize (Mb)', 'Interpreter', 'none')

for k=1:10
    cl_simrad_all('nomFic', nomFic); % Pour s'assurer que le cache XML-Bin est d�j� fait
    
    deflateLevel = k-1;
    
    tic
    ALL_Cache2Netcdf(nomFic, 'deflateLevel', deflateLevel);
    TWrite(k,1) = toc; %#ok<AGROW>
    plot(h(1), deflateLevel, TWrite(k), '*r'); drawnow;
    
    NetcdfSize(k,1) = sizeFic(nomFicNetcdf) * 1e-6; %#ok<AGROW>
    plot(h(2), deflateLevel, NetcdfSize(k), '*b'); drawnow;
    
    tic
    testLectureCacheNetcdf(nomFic)
    TRead(k,1) = toc; %#ok<AGROW>
    plot(h(1), deflateLevel, TRead(k), '*b'); drawnow;
    
    saveTable(nomFicSeul, TWrite, TRead, NetcdfSize, AllSize)
end


function testLectureCacheNetcdf(nomFic)

[flag, Data2] = SScCacheNetcdfUtils.readWholeData(nomFic, 'Ssc_Attitude'); %#ok<ASGLU>
[flag, Data2] = SScCacheNetcdfUtils.readWholeData(nomFic, 'Ssc_Position'); %#ok<ASGLU>
[flag, Data2] = SScCacheNetcdfUtils.readWholeData(nomFic, 'Ssc_Depth');    %#ok<ASGLU>
[flag, Data2] = SScCacheNetcdfUtils.readWholeData(nomFic, 'Ssc_Runtime');  %#ok<ASGLU>
[flag, Data2] = SScCacheNetcdfUtils.readWholeData(nomFic, 'Ssc_SeabedImage59h',    'Memmapfile', -1); %#ok<ASGLU>
[flag, Data2] = SScCacheNetcdfUtils.readWholeData(nomFic, 'Ssc_SeabedImage53h',    'Memmapfile', -1); %#ok<ASGLU>
[flag, Data2] = SScCacheNetcdfUtils.readWholeData(nomFic, 'Ssc_RawRangeBeamAngle', 'Memmapfile', -1); %#ok<ASGLU>
[flag, Data2] = SScCacheNetcdfUtils.readWholeData(nomFic, 'Ssc_SurfaceSoundSpeed', 'Memmapfile', -1); %#ok<ASGLU>
[flag, Data2] = SScCacheNetcdfUtils.readWholeData(nomFic, 'Ssc_Clock'); %#ok<ASGLU>
[flag, Data2] = SScCacheNetcdfUtils.readWholeData(nomFic, 'Ssc_SoundSpeedProfile');     %#ok<ASGLU>
[flag, Data2] = SScCacheNetcdfUtils.readWholeData(nomFic, 'Ssc_InstallationParameters'); %#ok<ASGLU>
[flag, Data2] = SScCacheNetcdfUtils.readWholeData(nomFic, 'Ssc_QualityFactorFromIfr', 'Memmapfile', -1); %#ok<ASGLU>
[flag, Data2] = SScCacheNetcdfUtils.readWholeData(nomFic, 'Ssc_Heading'); %#ok<ASGLU>
[flag, Data2] = SScCacheNetcdfUtils.readWholeData(nomFic, 'Ssc_Height');  %#ok<ASGLU>
[flag, Data2] = SScCacheNetcdfUtils.readWholeData(nomFic, 'Ssc_WaterColumn',   'Memmapfile', -1); %#ok<ASGLU>
[flag, Data2] = SScCacheNetcdfUtils.readWholeData(nomFic, 'Ssc_StaveData',      'Memmapfile', -1); %#ok<ASGLU>
[flag, Data2] = SScCacheNetcdfUtils.readWholeData(nomFic, 'Ssc_RawBeamSamples', 'Memmapfile', -1); %#ok<ASGLU>


function saveTable(nomFicSeul, TimeWrite, TimeRead, NetcdfSize, AllSize)

nomFicTable = fullfile(my_tempdir, ['Compression-' nomFicSeul '.txt']);

TimeWriteRatio  = round(TimeWrite / TimeWrite(1),   2);
TimeReadRatio   = round(TimeRead  / TimeRead(1),    2);
NetcdfSizeRatio = round(NetcdfSize / NetcdfSize(1), 2);
AllSizeRatio    = round(NetcdfSize / AllSize,   2);

Compression = (1:length(TimeWrite))' - 1;
TimeWrite   = round(TimeWrite, 2);
TimeRead    = round(TimeRead, 2);
NetcdfSize  = round(NetcdfSize, 2);

T = table(Compression, TimeWrite, TimeRead, NetcdfSize, TimeWriteRatio, TimeReadRatio, NetcdfSizeRatio, AllSizeRatio);
T.Properties.VariableUnits = {'','s','s','Mb', '', '', '', ''};
T %#ok<NOPRT>
writetable(T, nomFicTable)
