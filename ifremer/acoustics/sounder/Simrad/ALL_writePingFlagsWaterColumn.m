function flag = ALL_writePingFlagsWaterColumn(nomFicAll, subWC, subOKRaw, nbWCPings)

flag = existCacheNetcdf(nomFicAll);
if flag
    flag = ALL_writePingFlagsWaterColumnNetcdf(nomFicAll, subWC, subOKRaw);
else
    flag = ALL_writePingFlagsWaterColumnXMLBin(nomFicAll, subWC, subOKRaw, nbWCPings);
end


function flag = ALL_writePingFlagsWaterColumnXMLBin(nomFicAll, subWC, subOKRaw, nbWCPings)

[nomDir, nomFic2] = fileparts(nomFicAll);
nomFicFlagPings = fullfile(nomDir, 'SonarScope', nomFic2, 'Ssc_WaterColumn', 'FlagPings.bin');
if exist(nomFicFlagPings, 'file')
    fid = fopen(nomFicFlagPings, 'r');
    FlagPingsPrevious = fread(fid, 'uint8');
    fclose(fid);
    %                         FlagPingsNow = zeros(size(FlagPingsPrevious), 'uint8');
    FlagPingsNow = subOKRaw;
    subKO = (FlagPingsPrevious(:) == 1);
    subKO(subWC) = subKO(subWC) | (FlagPingsNow(:) == 0);
else
    FlagPings = zeros(nbWCPings, 1, 'uint8');
    FlagPings(subWC) = subOKRaw;
    subKO = ~FlagPings;
end
fid = fopen(nomFicFlagPings, 'w+');
if fid == -1
    messageErreurFichier(nomFicFlagPings, 'WriteFailure')
    flag = 0;
    return
end
fwrite(fid, subKO, 'uint8'); % TODO : subDepthFic subWCFic
fclose(fid);

flag = 1;


function flag = ALL_writePingFlagsWaterColumnNetcdf(nomFicAll, subWC, subOKRaw)

[flag, DataWC] = SSc_ReadDatagrams(nomFicAll, 'Ssc_WaterColumn', 'Memmapfile', -2); % -2 to avoid reading images since we just want FlagPings
if ~flag
    return
end
DataWC.FlagPings = ~subOKRaw;

%% Ouverture du fichier

[nomDir, nomFic] = fileparts(nomFicAll);
nomFicNc = fullfile(nomDir, 'SonarScope', [nomFic '.nc']);
if exist(nomFicNc, 'file')
    ncID = netcdf.open(nomFicNc, 'WRITE');
    grdID = netcdf.inqNcid(ncID, 'WaterColumn');
    varID = netcdf.inqVarID(grdID, 'FlagPings');
    
    FlagPingsPrevious = netcdf.getVar(grdID, varID);
    FlagPingsNow = subOKRaw;
    subKO = (FlagPingsPrevious(:) == 1);
    subKO(subWC) = subKO(subWC) | (FlagPingsNow(:) == 0);
    
%     netcdf.putVar(grdID, varID, uint8(DataWC.FlagPings(:,:)));
    netcdf.putVar(grdID, varID, uint8(subKO));
    netcdf.close(ncID);
end
