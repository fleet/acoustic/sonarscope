% Prelecture des fichiers .all : Transcodage des datagrams runtime, attitude, clock, 
% height, position, surfaceSoundSpeed, soundSpeedProfile, depth et rawRangeBeamAngle
%
% Syntax
%   AddValOnMode(liste)
% 
% Input Arguments 
%   liste : Nom de fichier ou liste de noms de fichiers ou repertoires
%
% Examples
%   AddValOnMode
%
% See also cl_simrad_all Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function AddValOnMode

[flag, nomDir] = my_uigetdir(pwd, 'Pick a Directory');
if ~flag
    return
end
liste = listeFicOnDir(nomDir, '.all');
sub = my_listdlg('Layers :', liste);
liste = liste(sub);

[flag, Val] = inputOneParametre('Value to add on Mode', 'Value', ...
    'Value', 1, 'Unit', 'dB/km/m', 'MinValue', -5, 'MaxValue', 5, 'Format', '%d');
if ~flag
    return
end

  
[Cor_runtime, Validation] = my_questdlg('Correction des fichiers _runtime.mat ?');
if ~Validation
    return
end

[Cor_reflectivity_sigV, Validation] = my_questdlg('Correction des fichiers reflectivity_sigV ?');
if ~Validation
    return
end

[Cor_beams_sigV, Validation] = my_questdlg('Correction des fichiers beams_sigV ?');
if ~Validation
    return
end

for i=1:length(liste)
    [nomDir, nomFic, ext] = fileparts(liste{i});
    aKM = cl_simrad_all('nomFic', liste{i});
    disp('---------------------------------------------------------------')

    if Cor_runtime == 1
        disp(['read_runtime           ' liste{i}])
        nomFicMat = fullfile(nomDir, 'SonarScope', [nomFic '_runtime.mat']);
        Data = read_runtime(aKM);

        % -----------------------
        % Changement de la valeur

        Data.Mode = Data.Mode + Val;

        % --------------------------------
        % Sauvegarde de la donnee corrigee

        try
            disp(['write_runtime          ' liste{i}])
            save(nomFicMat, 'Data')
        catch
            str = sprintf('Erreur ecriture fichier %s', nomFicMat);
            errordlg(str, 'Erreur')
        end
    end
    
    if Cor_reflectivity_sigV == 1
        [nomDir, nomFic] = fileparts(liste{i});
        nomFic = fullfile(nomDir, 'SonarScope', [nomFic '_FullReflectivity'], [nomFic '_FullReflectivity_sigV']);
        cor_sigV(nomFic, Val)
    end
    
    if Cor_beams_sigV == 1
        [nomDir, nomFic] = fileparts(liste{i});
        nomFic = fullfile(nomDir, 'SonarScope', [nomFic '_FullBeams'], [nomFic '_FullBeams_sigV']);
        cor_sigV(nomFic, Val)
    end

end




function cor_sigV(nomFic, Val)

disp(['read_sigV           ' nomFic])
fid = fopen(nomFic, 'r', 'ieee-le.l64');
if fid == -1
    disp([nomFic ' does not exist.'])
    return
end

X = fread(fid, [25 Inf], 'double');
fclose(fid);

Mode = X(17,:);
Mode = Mode + Val;
X(17,:) = Mode;

fid = fopen(nomFic, 'w+', 'ieee-le.l64');
if fid == -1
    str = sprintf('Erreur d''ecriture fichier %s', nomFic);
    my_warndlg(str, 1);
    return
end

disp(['write_sigV          ' nomFic])
count = fwrite(fid, X, 'double');
if count ~= numel(X)
    str = sprintf('Erreur d''ecriture fichier %s', nomFic);
    my_warndlg(str, 1);
    status = 0;
    return
end
fclose(fid);