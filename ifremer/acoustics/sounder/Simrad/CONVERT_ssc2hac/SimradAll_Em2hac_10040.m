function [flag, NbSLusOut] = SimradAll_Em2hac_10040(file, nomDir, iTuple, NbFaisceaux, Data, NbSamplesLusIn, SeuildB)

flag = 0;

% Tuple 10040 Ping C-16 compressed 16 bytes format pour chaque
% faisceau
TupleType           = 10040;
TimeFraction        = Data.DataSample.time_fraction(iTuple);    %0.0000;
TimeCPU             = Data.DataSample.time_cpu(iTuple);         %InfoPing.startdatatime + InfoPing.datatime;
TransceiverMode     = 0;
PingNumber          = Data.DataSample.Ping(iTuple);
BeamWidth.along     = Data.RunTime.BeamWidthAlong;
BeamWidth.athwart   = Data.RunTime.BeamWidthAthwart;
PulseLength         = Data.RunTime.PulseLength;
nomFicBin           = fullfile(nomDir, 'WCD_WaterColumn', 'SampleAmplitude.bin');

% Ouverture du fichier des �chantillons pour lecture par faisceau par faisceau.
fidSamples = fopen(nomFicBin, 'r');
if fidSamples == -1
    nomFicBin = fullfile(nomDir, 'ALL_WaterColumn', 'SampleAmplitude.bin');
    fidSamples = fopen(nomFicBin, 'r');
    if fidSamples == -1
        messageErreurFichier(nomFicBin);
        flag = 0;
        return
    end
end

NSamplesEcrits = 0;
for k=1:NbFaisceaux   %InfoPing.faisceaux
    
    fwrite(file.fid, zeros(14,1), 'int16');
    
    % Calcul du nombre d'�chantillons � stocker et d�j� stock�s.
    NbSamplesALire  =   double(Data.DataSample.NbSamples(iTuple,k));

    % Traitement du paquets d'�chantillons (Ping, Faisceau), traitement de
    % la compression.
    WCAngleAthwart = Data.DataSample.angles_athwart(iTuple,k);
    
    [flag, N, CompressSamples] = SimradAll_compression10040(fidSamples, NbSamplesLusIn, NbSamplesALire, ...
                                Data.DataSample.sampling(iTuple), Data.DataSample.soundSpeed(iTuple), ...
                                Data.DataSample.TVGFunctionApplied(iTuple), BeamWidth, WCAngleAthwart, ...
                                PulseLength, SeuildB);
    
    % Calcul de compl�ment � deux pour les valeurs d�passant 32767
    % for s=1:numel(CompressSamples)
    %     if CompressSamples(s) > 32767
    %         CompressSamples(s) = twos_comp(double(CompressSamples(s)), 16);
    %         CompressSamples(s) = CompressSamples(s) - 1;
    %     end
    % 
    % end

    
    sub                     = (CompressSamples > 32767);
    pppp                    = CompressSamples(sub);
    pppp                    = -32768+(pppp-32768)-1;
    CompressSamples(sub==1) = pppp;
  
    
    % Ecriture dans le fichier HAC.
    fwrite(file.fid, int16(CompressSamples),'*int16');
    NSamplesEcrits = NSamplesEcrits + N;
    TupleSize       = 26 + 2*N;
    SoftChannelId   = k;
    
    fseek(file.fid, -28-2*N, 'cof');
    
    fwrite(file.fid, TupleSize,'uint32');
    fwrite(file.fid, TupleType,'uint16');
    fwrite(file.fid, TimeFraction,'uint16');
    fwrite(file.fid, TimeCPU,'uint32');
    fwrite(file.fid, SoftChannelId,'uint16');
    fwrite(file.fid, TransceiverMode,'uint16');
    fwrite(file.fid, PingNumber,'uint32');
    if Data.DataSample.rangebottom(iTuple,k)==0
        fwrite(file.fid, 2147483647,'int32');
        %  fwrite(file.fid, ((24/cos(WC.angles_athwart(iTuple,k)*pi/180))*1000),'int32');
    else
        pppp = int32(Data.DataSample.rangebottom(iTuple,k)*Data.DataSample.soundSpeed(iTuple)/(2*Data.DataSample.sampling(iTuple)))*1000;
        fwrite(file.fid, pppp,'int32');
        %  fwrite(file.fid, ((24/cos(WC.angles_athwart(iTuple,k)*pi/180))*1000),'int32');
    end
    
    % Ecriture du nombre d'�chantillons � venir.
    fwrite(file.fid, N,'uint32');
    
    % Avance pour recalage sur la fin du paquet.
    fseek(file.fid, 2*N, 'cof');
    
    % Ecriture de la fin du tuple
    TupleAttribute  = 0;
    Backlink        = TupleSize+10;
    fwrite(file.fid, TupleAttribute,'int32');
    fwrite(file.fid, Backlink,'uint32');
    NbSamplesLusIn = NbSamplesLusIn + NbSamplesALire;
end

NbSLusOut = NbSamplesLusIn;   

fclose(fidSamples);
