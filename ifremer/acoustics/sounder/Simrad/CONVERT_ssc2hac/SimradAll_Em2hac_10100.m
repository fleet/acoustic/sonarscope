function flag = SimradAll_Em2hac_10100(file, iTuple, NbFaisceaux, Data, varargin)

persistent TuplePrev;
if (iTuple == 1)
    TuplePrev = [];
end

flag = 0;


% Allocation de m�moire.
TupleCrt(NbFaisceaux) = struct( 'SoftwareChannelIdentifier', [], ...
    'TVGMaxRange', [], ...
    'TVGMinRange', [], ...
    'TVTEvaluationMode', [], ...
    'TVTEvaluationInterval', [], ...
    'TVTEvaluationNoPings', [], ...
    'TVTEvaluationStartingTVTPingNum', [], ...
    'TVToffsetparameter', [], ...
    'TVTAmplificationParameter', [], ...
    'TupleAttribute', [], ...
    'Backlink', [] );


% Tuple 10100 General threshold
for k=1:NbFaisceaux
    TupleSize                                   = 34;
    TupleType                                   = 10100;
    TimeFraction                                = Data.DataSample.time_fraction(iTuple); %0.0000;
    TimeCPU                                     = Data.DataSample.time_cpu(iTuple);      %InfoPing.startdatatime+InfoPing.datatime; %heure locale en seconde depuis le 1er janvier 1970
    % Enregistrement des informations utiles du Tuple pour comparaison
    % avec le Ping pr�c�dent.
    TupleCrt(k).SoftwareChannelIdentifier       = k;                   % num�ro du faisceau  - � it�rer de 1 � 128
    TupleCrt(k).TVGMaxRange                     = 6553.5*10;                % le max
    TupleCrt(k).TVGMinRange                     = 6553.5*10;
    TupleCrt(k).TVTEvaluationMode               = 0;
    TupleCrt(k).TVTEvaluationInterval           = 65535;
    TupleCrt(k).TVTEvaluationNoPings            = 65535;
    TupleCrt(k).TVTEvaluationStartingTVTPingNum = 4294967295;
    TupleCrt(k).TVToffsetparameter              = -2147.483648*1000000; %Seuil d�archivage = not available
    TupleCrt(k).TVTAmplificationParameter       = 0;
    TupleCrt(k).TupleAttribute                  = 0;
    TupleCrt(k).Backlink                        = 44;
    
    % Ecriture des tuples que si les donn�es diff�rents du datagramme
    % pr�c�dent.
    if isempty(TuplePrev) || ~isequal(TuplePrev(k), TupleCrt(k))
        fwrite(file.fid, TupleSize,'uint32');
        fwrite(file.fid, TupleType,'uint16');
        fwrite(file.fid, TimeFraction,'uint16');
        fwrite(file.fid, TimeCPU,'uint32');
        fwrite(file.fid, TupleCrt(k).SoftwareChannelIdentifier,'uint16');
        fwrite(file.fid, TupleCrt(k).TVGMaxRange,'uint16');
        fwrite(file.fid, TupleCrt(k).TVGMinRange,'uint16');
        fwrite(file.fid, TupleCrt(k).TVTEvaluationMode,'uint16');
        fwrite(file.fid, TupleCrt(k).TVTEvaluationInterval,'uint16');
        fwrite(file.fid, TupleCrt(k).TVTEvaluationNoPings,'uint16');
        fwrite(file.fid, TupleCrt(k).TVTEvaluationStartingTVTPingNum,'uint32');
        fwrite(file.fid, TupleCrt(k).TVToffsetparameter,'int32');
        fwrite(file.fid, TupleCrt(k).TVTAmplificationParameter,'uint32');
        fwrite(file.fid, TupleCrt(k).TupleAttribute,'int32');
        fwrite(file.fid, TupleCrt(k).Backlink,'uint32');
    end
end


% Conservation des donn�es dans une structure
TuplePrev = TupleCrt;

flag = 1;
