function flag = SimradAll_Em2hac_10140(file, iTuple, Data) 

persistent TuplePrev;
if (iTuple == 1)
    TuplePrev = [];
end

flag = 0;

% Tuple 10140 attitude sensor tuple pour chaque ping
TupleSize                   = 22;
TupleType                   = 10140;
TimeFraction                = Data.DataSample.time_fraction(iTuple);                                    %0.0000;
TimeCPU                     = Data.DataSample.time_cpu(iTuple);                                         %InfoPing.startdatatime + InfoPing.datatime;
% Enregistrement des informations utiles du Tuple pour comparaison
% avec le Ping pr�c�dent.
TupleCrt.AttitudeSensorId   = 1;
TupleCrt.Pitch              = Data.Attitude.pitch(iTuple)*10;
TupleCrt.Roll               = Data.Attitude.roll(iTuple)*10;
TupleCrt.Heave              = Data.Attitude.heave(iTuple)*100;
TupleCrt.Yaw                = Data.Attitude.heading(iTuple)*10;
TupleCrt.Space              = 0;
TupleCrt.TupleAttribute     = 0;
TupleCrt.Backlink           = 32;

% Ecriture des tuples que si les donn�es sont diff�rentes du datagramme
% pr�c�dent.
if ~isempty(TuplePrev) || ~isequal(TuplePrev, TupleCrt)
    fwrite(file.fid, TupleSize,'uint32');
    fwrite(file.fid, TupleType,'uint16');
    fwrite(file.fid, TimeFraction,'uint16');
    fwrite(file.fid, TimeCPU,'uint32');
    
    fwrite(file.fid, TupleCrt.AttitudeSensorId,'uint16');
    fwrite(file.fid, TupleCrt.Pitch,'int16');
    fwrite(file.fid, TupleCrt.Roll,'int16');
    fwrite(file.fid, TupleCrt.Heave,'int16');
    fwrite(file.fid, TupleCrt.Yaw,'int16');
    fwrite(file.fid, TupleCrt.Space,'uint16');
    fwrite(file.fid, TupleCrt.TupleAttribute,'int32');
    fwrite(file.fid, TupleCrt.Backlink,'uint32');
end

% Conservation des donn�es dans une structure
TuplePrev = TupleCrt;

flag = 1;