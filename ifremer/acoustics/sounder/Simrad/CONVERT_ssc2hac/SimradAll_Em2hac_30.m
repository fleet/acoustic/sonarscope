function flag = SimradAll_Em2hac_30(file, iTuple, Data)


persistent TuplePrev;
if (iTuple == 1)
    TuplePrev = [];
end

flag = 0;

% Tuple 30 Navigation Tuple (doc � part ....)
TupleSize           = 18;
TupleType           = 30;
TimeFraction        = Data.Position.time_fraction(iTuple);
TimeCPU             = Data.Position.time_cpu(iTuple);                                          % heure locale en seconde depuis le 1er janvier 1970
% Enregistrement des informations utiles du Tuple pour comparaison
% avec le Ping pr�c�dent.
TupleCrt.NavSystem           = 0;
TupleCrt.Heading             = Data.Position.heading(iTuple)*0.1;
TupleCrt.NavigationSpeed     = Data.Position.speed(iTuple)*10;
TupleCrt.Space               = 0;
TupleCrt.TupleAttribute      = 0;
TupleCrt.BackLink            = 28;

% Ecriture des tuples que si les donn�es diff�rents du datagramme
% pr�c�dent.
if ~isempty(TuplePrev) || ~isequal(TuplePrev, TupleCrt)
    fwrite(file.fid, TupleSize,'uint32');
    fwrite(file.fid, TupleType,'uint16');
    fwrite(file.fid, TimeFraction,'uint16');
    fwrite(file.fid, TimeCPU,'uint32');
    fwrite(file.fid, TupleCrt.NavSystem,'uint16');
    fwrite(file.fid, TupleCrt.Heading,'int16');
    fwrite(file.fid, TupleCrt.NavigationSpeed,'int16');
    fwrite(file.fid, TupleCrt.Space,'uint16');
    fwrite(file.fid, TupleCrt.TupleAttribute,'int32');
    fwrite(file.fid, TupleCrt.BackLink,'uint32');
end


% Conservation des donn�es dans une structure 
TuplePrev = TupleCrt;

flag = 1;
