function flag = SimradAll_Em2hac_9001(file, iTuple, NumberSWChannels, SounderName, Data)

persistent TuplePrev;
if (iTuple == 1)
    TuplePrev = [];
end

flag = 0;

% Allocation de m�moire.
TupleCrt(NumberSWChannels) = struct( ...
    'SamplingRate', [], ...
    'SamplingInterval', [], ...
    'AcousticFrequency', [], ...
    'TransceiverChannelNumber', [], ...
    'TypeOfData', [], ...
    'TimeVariedGainMultiplier', [], ...
    'TVGBlankingMode', [], ...
    'TVGMinimumRange', [], ...
    'TVGMaximumRange', [], ...
    'BlankingUpToRange', [], ...
    'SampleRange', [], ...
    'InstallationDepthOfTransducer', [], ...
    'PlatformIdentifier', [], ...
    'Space', [], ...
    'AlongshipOffsetRelToAttSensor', [], ...
    'AthwartshipOffsetRelToAttSensor', [], ...
    'VerticalOffsetRelToAttSensor', [], ...
    'AlongshipAngleOffsetOfTransducer', [], ...
    'AthwartshipAngleOffsetOfTransducer', [], ...
    'RotationAngleOffsetOfTransducer', [], ...
    'AlongshipAngleOffsetToMainAxisOfAcBeam', [], ...
    'AthwartshipAngleOffsetToMainAxisOfAcBeam', [], ...
    'AbsorptionOfSound', [], ...
    'PulseDuration', [], ...
    'PulseShapeMode', [], ...
    'BandWidth', [], ...
    'TransducerShapeMode', [], ...
    'BeamAlongship3dBBeamWidth', [], ...
    'BeamAthwartship3dBBeamWidth', [], ...
    'BeamEquivalentTwoWayBeamAngle', [], ...
    'CalibrationSourceLevel', [], ...
    'CalibrationReceivingSensitivity', [], ...
    'SLPlusVR', [], ...
    'BottomDetectionMinimumLevel', [], ...
    'BottomDetectionMinimumDepth', [], ...
    'BottomDetectionMaximumDepth', [], ...
    'Remarks', [], ...
    'TupleAttribute', [], ...
    'Backlink', []);


for k=1:NumberSWChannels
    TupleSize                                               = 146;
    TupleType                                               = 9001;
    SoftwareChannelIdentifier   = k;
    EchosounderDocIdentifier                                = iMultiPing;        %2; %999; % Identifiant sondeur
    % Enregistrement des informations utiles du Tuple pour comparaison
    % avec le Ping pr�c�dent.
    TupleCrt(k).SamplingRate                                = Data.DataSample.sampling(iTuple);
    TupleCrt(k).SamplingInterval                            = (1/Data.DataSample.sampling(iTuple))*Data.DataSample.soundSpeed(iTuple)/2*1e+6;
    TupleCrt(k).AcousticFrequency                           = Data.DataSample.frequency(iTuple,k)*10;
    TupleCrt(k).TransceiverChannelNumber                    = k;
    TupleCrt(k).TypeOfData                                  = 11;    % 11 = mean Sv (volume backscattering strength in dB)*
    TupleCrt(k).TimeVariedGainMultiplier                    = 20;
    TupleCrt(k).TVGBlankingMode                             = 65535;
    TupleCrt(k).TVGMinimumRange                             = 65535;
    TupleCrt(k).TVGMaximumRange                             = 65535;
    TupleCrt(k).BlankingUpToRange                           = 4294967295;
    TupleCrt(k).SampleRange                                 = 4294967295;
    TupleCrt(k).InstallationDepthOfTransducer               = Data.InstallParams.VerticalOffsetRelToAttSensor*1e4; % Motion sensor 1 vertical location in m
    TupleCrt(k).PlatformIdentifier                          = 2;
    TupleCrt(k).Space                                       = 0;
    TupleCrt(k).AlongshipOffsetRelToAttSensor               = Data.InstallParams.AlongshipOffsetRelToAttSensor*10000; % Motion sensor 1 along location in m
    TupleCrt(k).AthwartshipOffsetRelToAttSensor             = Data.InstallParams.AthwartshipOffsetRelToAttSensor*10000; % Motion sensor 1 athwart location in m
    TupleCrt(k).VerticalOffsetRelToAttSensor                = Data.InstallParams.VerticalOffsetRelToAttSensor*10000; % Motion sensor 1 vertical location in m
    TupleCrt(k).AlongshipAngleOffsetOfTransducer            = Data.InstallParams.AlongshipAngleOffsetOfTransducer*100; % Transducer 1 pitch in 0.01 degrees
    TupleCrt(k).AthwartshipAngleOffsetOfTransducer          = Data.InstallParams.AthwartshipAngleOffsetOfTransducer*100; % Transducer 1 roll in 0.01 degrees
    TupleCrt(k).RotationAngleOffsetOfTransducer             = Data.InstallParams.RotationAngleOffsetOfTransducer*100; % Transducer 1 heading in 0.01 degrees
    TupleCrt(k).AlongshipAngleOffsetToMainAxisOfAcBeam      = Data.DataSample.angles_along(iTuple, k)*100;
    TupleCrt(k).AthwartshipAngleOffsetToMainAxisOfAcBeam    = Data.DataSample.angles_athwart(iTuple, k)*100;
    TupleCrt(k).AbsorptionOfSound                           = Data.RunTime.AbsorptionCoeff(iTuple)*10000;     % en 0.0001 dB/km Absorption of sound in the propagation medium
    TupleCrt(k).PulseDuration                               = Data.RunTime.PulseLength(iTuple)*1000;
    TupleCrt(k).PulseShapeMode                              = 65535;
    TupleCrt(k).BandWidth                                   = Data.RunTime.BandWidth(iTuple)*100;
    TupleCrt(k).TransducerShapeMode                         = 3;
    TupleCrt(k).BeamAlongship3dBBeamWidth                   = Data.RunTime.BeamWidthAlong(iTuple)*10;  % Half power (3dB) beam width of the transducer in the alongship direction.
    TupleCrt(k).BeamAthwartship3dBBeamWidth                 = (Data.RunTime.BeamWidthAthwart(iTuple)/cos(Data.DataSample.angles_athwart(iTuple,k)*pi/180));
    TupleCrt(k).BeamEquivalentTwoWayBeamAngle               = -32768;                % Equivalent two way beam opening solid angle. MacLennan and Simmonds, Fisheries Acoustics1992, section 2.3.
    TupleCrt(k).CalibrationSourceLevel                      = 65535;
    TupleCrt(k).CalibrationReceivingSensitivity             = -32768;
    TupleCrt(k).SLPlusVR                                    = -32768;
    TupleCrt(k).BottomDetectionMinimumLevel                 = -32768;
    TupleCrt(k).BottomDetectionMinimumDepth                 = 4294967295;
    TupleCrt(k).BottomDetectionMaximumDepth                 = 4294967295;
    TupleCrt(k).Remarks                                     = sprintf('%40s', ['Sounder Name : ' SounderName(1:5)]); % sur 100 caract�res
    
    TupleCrt(k).TupleAttribute                              = 0;
    TupleCrt(k).Backlink                                    = 156;
    
    % Ecriture des tuples que si les donn�es diff�rents du datagramme
    % pr�c�dent.
    if ~isequal(TuplePrev, TupleCrt(k))
            fwrite(file.fid, TupleSize,'uint32');
            fwrite(file.fid, TupleType,'uint16');
            fwrite(file.fid, SoftwareChannelIdentifier,'uint16');
            fwrite(file.fid, EchosounderDocIdentifier,'uint32');
            fwrite(file.fid, TupleCrt(k).SamplingRate,'uint32');
            fwrite(file.fid, TupleCrt(k).SamplingInterval,'uint32');
            fwrite(file.fid, TupleCrt(k).AcousticFrequency,'uint32');
            fwrite(file.fid, TupleCrt(k).TransceiverChannelNumber,'uint16');
            fwrite(file.fid, TupleCrt(k).TypeOfData,'uint16');
            fwrite(file.fid, TupleCrt(k).TimeVariedGainMultiplier,'uint16');
            fwrite(file.fid, TupleCrt(k).TVGBlankingMode,'uint16');
            fwrite(file.fid, TupleCrt(k).TVGMinimumRange,'uint16');
            fwrite(file.fid, TupleCrt(k).TVGMaximumRange,'uint16');
            fwrite(file.fid, TupleCrt(k).BlankingUpToRange,'uint32');
            fwrite(file.fid, TupleCrt(k).SampleRange,'uint32');
            fwrite(file.fid, TupleCrt(k).InstallationDepthOfTransducer,'uint32');
            fwrite(file.fid, TupleCrt(k).PlatformIdentifier,'uint16');
            fwrite(file.fid, TupleCrt(k).Space,'uint16');
            fwrite(file.fid, TupleCrt(k).AlongshipOffsetRelToAttSensor,'uint32');
            fwrite(file.fid, TupleCrt(k).AthwartshipOffsetRelToAttSensor,'uint32');
            fwrite(file.fid, TupleCrt(k).VerticalOffsetRelToAttSensor,'uint32');
            fwrite(file.fid, TupleCrt(k).AlongshipAngleOffsetOfTransducer,'int16');
            fwrite(file.fid, TupleCrt(k).AthwartshipAngleOffsetOfTransducer,'int16');
            fwrite(file.fid, TupleCrt(k).RotationAngleOffsetOfTransducer,'int16');
            fwrite(file.fid, TupleCrt(k).AlongshipAngleOffsetToMainAxisOfAcBeam,'int16');
            fwrite(file.fid, TupleCrt(k).AthwartshipAngleOffsetToMainAxisOfAcBeam ,'int16');
            fwrite(file.fid, TupleCrt(k).AbsorptionOfSound,'uint16');
            fwrite(file.fid, TupleCrt(k).PulseDuration,'uint32');
            fwrite(file.fid, TupleCrt(k).PulseShapeMode,'uint16');
            fwrite(file.fid, TupleCrt(k).BandWidth,'uint16');
            fwrite(file.fid, TupleCrt(k).TransducerShapeMode,'uint16');
            fwrite(file.fid, TupleCrt(k).BeamAlongship3dBBeamWidth,'uint16');
            fwrite(file.fid, TupleCrt(k).BeamAthwartship3dBBeamWidth,'uint16');
            fwrite(file.fid, TupleCrt(k).BeamEquivalentTwoWayBeamAngle,'int16');
            fwrite(file.fid, TupleCrt(k).CalibrationSourceLevel,'uint16');
            fwrite(file.fid, TupleCrt(k).CalibrationReceivingSensitivity,'int16');
            fwrite(file.fid, TupleCrt(k).SLPlusVR,'int16');
            fwrite(file.fid, TupleCrt(k).BottomDetectionMinimumLevel,'int16');
            fwrite(file.fid, TupleCrt(k).BottomDetectionMinimumDepth,'uint32');
            fwrite(file.fid, TupleCrt(k).BottomDetectionMaximumDepth,'uint32');
            fwrite(file.fid, TupleCrt(k).Remarks,'char');
            fwrite(file.fid, TupleCrt(k).TupleAttribute,'int32');
            fwrite(file.fid, TupleCrt(k).Backlink,'uint32');  
    end
    
end

% Conservation des donn�es dans une structure
TuplePrev = TupleCrt;

flag = 1;
