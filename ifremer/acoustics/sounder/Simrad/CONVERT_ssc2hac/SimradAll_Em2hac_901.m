function flag = SimradAll_Em2hac_901(file, iTuple, NbFaisceaux, SounderName, Data, varargin)

persistent TuplePrev;
if (iTuple == 1)
    TuplePrev = [];
end


flag = 0; %#ok<NASGU>


% Tuple 220 MBES Echo sounder
TupleSize                           = 118;
TupleType                           = 901;
NumberSWChannels                    = NbFaisceaux;
EchosounderDocIdentifier            = 2;
% Enregistrement des informations utiles du Tuple pour comparaison
% avec le Ping pr�c�dent.
TupleCrt.SoundSpeed                  = Data.DataSample.soundSpeed(iTuple)*10;
TupleCrt.PingInterval                = 0.0;                      % not available
TupleCrt.TriggerMode                 = 65535;                    % not available
TupleCrt.Space                       = 0 ;
TupleCrt.Remarks                     = sprintf('%100s', ['Sounder Name : ' SounderName(1:5)]); % sur 100 caract�res
TupleCrt.TupleAttribute              = 0;
TupleCrt.Backlink                    = 128;


% Ecriture des tuples que si les donn�es diff�rents du datagramme
% pr�c�dent.
if isempty(TuplePrev) || ~isequal(TuplePrev, TupleCrt)
    fwrite(file.fid, TupleSize,                      'uint32');
    fwrite(file.fid, TupleType,                      'uint16');
    fwrite(file.fid, NumberSWChannels,               'uint16');
    fwrite(file.fid, EchosounderDocIdentifier,       'uint32');
    fwrite(file.fid, TupleCrt.SoundSpeed,            'uint16');
    fwrite(file.fid, TupleCrt.PingInterval,          'uint16');
    fwrite(file.fid, TupleCrt.TriggerMode,           'uint16');
    fwrite(file.fid, TupleCrt.Space,                 'uint16');
    fwrite(file.fid, TupleCrt.Remarks,               'char');
    fwrite(file.fid, TupleCrt.TupleAttribute,        'int32');
    fwrite(file.fid, TupleCrt.Backlink,              'uint32');
end

% Conservation des donn�es dans une structure
TuplePrev = TupleCrt;


flag = 1;