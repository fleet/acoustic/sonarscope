% Lecture de l'arborescence sonarscope pour un fichier .all Kongsberg
% Prise en compte des données WC et contextuelles pour création fichier hac
% correspondant
%fichier conversion avec modif tuple 220 et NbDayspos version du
%24/06/2010. Selection de la liste des fichiers a partir des .avi.
%ATTENTION:Créer un répertoire XXXXXXXX_HAC dans le répertoire XXXXXXXX (Exemple:20091110_HAC dans le répertoire 20091110 s'il n'existe
%pas) avant de lancer la conversion.

%%%%%%% clear all;


global WC;
global Position;
global NbFaisceaux;
global Beamwidth;
global PulseLength;
global LS;
global Name;

%% lecture d'une liste de fichiers de données (modifier le chemin ici)
[flag, chemin1]=my_uigetdir('???\Sonar');
% [flag, chemin1]=my_uigetdir('F:\SonarScopeData\From AndreOgor');
% [flag, chemin1]=my_uigetdir('F:\Data_SSC-3DViewer\FromLaurentBerger\Data');
% [flag, chemin1]=my_uigetdir('H:\MARMESONET\Leg1\EM302\20091113');
% [flag, chemin1]=my_uigetdir('Q:\Marmesonet\EM302\20091107');
%mkdir('Q:\Marmesonet\conversion\test','_HAC')
% listage=dir (fullfile(chemin1,'\*.avi'));
% listage=dir (fullfile(chemin1,'0006_20091128_125114_Lesuroit.all'));
listage=dir (fullfile(chemin1,'0307_20091113_163950_Lesuroit.all'));
nblistage=length(listage);

%% En cas de plantage "Out of memory" repérer la valeur de n puis la
% modifier dans la ligne suivante (par exemple n=5:nblistage) si le
% plantage a eu lieu au 5ème fichier.
tic
for n=1:nblistage
    NomFic=listage(n).name;
    %chemin3=[chemin1,'\',NomFic(1,6:13),'_HAC'];
    chemin3=chemin1;
    %nomficcomplet = [chemin1,'\', NomFic];
    chemin = [chemin1,'\SonarScope\',NomFic(1,1:end-4)];
    % chemin = ('Q:\Marmesonet\EM302\20091107\SonarScope\0083_20091107_064701_Lesuroit');
    Name ='EM302';
    
    %     NbPingsTotal=411;
    %     firstPing=1;
    struct=xml_read([chemin,'\Ssc_WaterColumn.xml']);
    NbPingsTot=struct.Dimensions.nbPings;
    NbFaisceaux=struct.Dimensions.nbRx;
    NbPingsTotal=NbPingsTot;

        
    %% test du nombre de pings et traitement par LotNb pings
    LotNb=200;
    % LotNb=5;
    fid = fopen([chemin,'\Ssc_Runtime\TransmitPulseLength.bin']);
    PulseLength = max(fread(fid,'single'));
    fclose (fid);


    fid = fopen([chemin,'\Ssc_Runtime\TransmitBeamWidth.bin']);
    Beamwidth.along = max(fread(fid,'single'));
    fclose (fid);

    fid = fopen([chemin,'\Ssc_Runtime\ReceiveBeamWidth.bin']);
    Beamwidth.athwart = max(fread(fid,'single'));
    fclose (fid);

           
    fid = fopen([chemin,'\Ssc_WaterColumn\TiltAngle.bin']);
    angles_list2 = fread(fid,'single');
    fclose (fid);
    
    
    fid = fopen([chemin,'\Ssc_Position\Time.bin']);
    NbDaysPos = fread(fid,'double');
    fclose (fid);


    Position.time_cpu=floor((NbDaysPos-datenum('19700101','yyyymmdd'))*86400);
    Position.time_fraction=((NbDaysPos-datenum('19700101','yyyymmdd'))*86400-Position.time_cpu)*10000;

    fid = fopen([chemin,'\Ssc_Position\Latitude.bin']);
    Position.latitude = fread(fid,'double');
    fclose (fid);

    fid = fopen([chemin,'\Ssc_Position\Longitude.bin']);
    Position.longitude = fread(fid,'double');
    fclose (fid);

    fid = fopen([chemin,'\Ssc_Position\Speed.bin']);
    Position.speed = fread(fid,'single');
    fclose (fid);

    % Position.speed = 3.6*ones(NbPings);

    fid = fopen([chemin,'\Ssc_Position\Heading.bin']);
    Position.heading = fread(fid,'single');
    fclose (fid);
    
    if NbPingsTotal>=LotNb
        %décompte préliminaire du nombre de boucles (nb de fois 200 pings)
        LotPings_entiersup=ceil(NbPingsTotal/LotNb);
        LotPings_entierinf=floor(NbPingsTotal/LotNb);
        %calcul du nb de pings restant
        RestePings =(NbPingsTotal-LotPings_entierinf*LotNb);
        
        for f=1:LotPings_entiersup
            %cas des lots complets de LotNb pings
            if f<LotPings_entiersup
                firstPing=(f*LotNb-LotNb)+1;
                lastPing=f*LotNb;
                %cas du dernier lot de pings (< à LotNb)
            else
                firstPing=(NbPingsTotal-RestePings)+1;
                lastPing=NbPingsTotal;
            end
            
            stepvalidity=1;
            
            
            
            %struct=xml_read([chemin,'\Ssc_WaterColumn.xml']);
            %NbPingsTot=struct.Dimensions.nbPings;
            %NbFaisceaux=struct.Dimensions.nbRx;
            
            struct = xml_read([chemin, '\Ssc_RawRangeBeamAngle.xml']);
            
            NbSector = struct.Dimensions.nbTx;
            
            NbPings = (lastPing - firstPing) + 1;
            
            fid = fopen([chemin,'\Ssc_WaterColumn\PingCounter.bin']);
            fseek(fid, (firstPing-1) * 4, 'bof');
            WC.ping = fread(fid, NbPings, 'single');
            fclose(fid);
            
            
            fid = fopen([chemin,'\Ssc_WaterColumn\Time.bin']);
            fseek(fid,(firstPing-1)*8, 'bof');
            NbDays = fread(fid,NbPings, 'double');
            fclose (fid);
            
            WC.time_cpu      = floor((NbDays-datenum('19700101','yyyymmdd'))*86400);
            WC.time_fraction = ((NbDays-datenum('19700101','yyyymmdd'))*86400-WC.time_cpu)*10000;
            
            
            WC.angles_athwart = NaN(NbPings, NbFaisceaux);
            fid = fopen([chemin,'\Ssc_WaterColumn\BeamPointingAngle.bin']);
            fseek(fid, NbFaisceaux*(firstPing-1)*4, 'bof');
            for i=1:NbPings
                WC.angles_athwart(i,:) = fread(fid, NbFaisceaux, 'single');
            end
            fclose (fid);
            
            
            
            
            WC.start_range=NaN(NbPings,NbFaisceaux);
            fid = fopen([chemin,'\Ssc_WaterColumn\StartRangeNumber.bin']);
            fseek(fid,NbFaisceaux*(firstPing-1)*4,'bof');
            for i=1:NbPings
                WC.start_range(i,:) = fread(fid,NbFaisceaux,'single');
            end
            fclose (fid);
            
            
            WC.frequency=NaN(NbPings,NbFaisceaux);
            WC.angles_along=NaN(NbPings,NbFaisceaux);
            fid = fopen([chemin,'\Ssc_WaterColumn\TransmitSectorNumber.bin']);
            fseek(fid,NbFaisceaux*(firstPing-1)*4,'bof');
            for i=1:NbPings
                sector(i,:) = fread(fid,NbFaisceaux,'single')-1;
            end
            fclose (fid);
            
            
            fid = fopen([chemin,'\Ssc_WaterColumn\CentralFrequency.bin']);
            fseek(fid,NbSector*(firstPing-1)*4,'bof');
            freq_list = fread(fid,NbSector*NbPings,'single');
            fclose (fid);
            
            fid = fopen([chemin,'\Ssc_WaterColumn\TiltAngle.bin']);
            fseek(fid,(firstPing-1)*NbSector*4,'bof');
            angles_list = fread(fid,NbSector*NbPings,'single');
            fclose (fid);
          
            for i=1:NbPings
                if NbSector==1
                    WC.frequency(i,:)=freq_list*ones(1,NbFaisceaux);
                    WC.angles_along(i,:)=angles_list*ones(1,NbFaisceaux);
                else
                    for j=1:NbSector
                        if find(sector(i,:)==j)
                            WC.frequency(i,find(sector(i,:)==j))=freq_list(NbSector*(i-1)+j);
                            WC.angles_along(i,find(sector(i,:)==j))=angles_list(NbSector*(i-1)+j);
                        end
                    end
                end
            end
            
            fid = fopen([chemin,'\Ssc_WaterColumn\SamplingFreq.bin']);
            fseek(fid,(firstPing-1)*4,'bof');
            WC.sampling = fread(fid,NbPings,'single')*0.1;
            fclose (fid);
            
            fid = fopen([chemin,'\Ssc_WaterColumn\SoundSpeed.bin']);
            fseek(fid,(firstPing-1)*4,'bof');
            WC.soundSpeed = fread(fid,NbPings,'single')*0.1;
            fclose (fid);
            
            
            
            
            WC.rangebottom=NaN(NbPings,NbFaisceaux);
            fid = fopen([chemin,'\Ssc_WaterColumn\DetectedRangeInSamples.bin']);
            fseek(fid,(firstPing-1)*4*NbFaisceaux,'bof');
            for i=1:NbPings
                WC.rangebottom(i,:) = fread(fid,NbFaisceaux,'single');
            end
            
            
            AllSamples=NaN(NbPingsTot,NbFaisceaux);
            
            fid = fopen([chemin,'\Ssc_WaterColumn\NumberOfSamples.bin']);
            for i=1:NbPingsTot
                AllSamples(i,:) = fread(fid,NbFaisceaux,'single');
            end
            WC.samples=AllSamples(firstPing:lastPing,:);
            
            WC.amplitudes=NaN(NbPings,NbFaisceaux,max(max(WC.samples(:,:))),'single');
            fid = fopen([chemin,'\WCD_WaterColumn\SampleAmplitude.bin']);
            fseek(fid,sum(sum(AllSamples(1:firstPing-1,:))),'bof');
            % figure(111111); hold on;
            for i=1:NbPings
                N(i) = 0;
                for j=1:NbFaisceaux
                    N(i) = N(i) + WC.samples(i,j);
                    WC.amplitudes(i,j,1:WC.samples(i,j)) = fread(fid,WC.samples(i,j),'int8')*0.5;
                end
                % bidon = WC.amplitudes(i,:,:);
                % imagesc(squeeze(bidon)');                
            end
            fclose (fid);
            
            %% GLU : Equivalent de la recherche pour un Ping via Memmapfile.
            %{
            nomFicBin = [chemin,'\WCD_WaterColumn\SampleAmplitude.bin'];
            iPing = firstPing;
            [flag, nBytes]  = my_getBytesPerType('uint8');
            if flag ~= 0
                return
            end
            % Calcul du nombre d'échantillons à stocker.    
            NbALire = sum(WC.samples(iPing,:), 'omitnan');
            % Calcul du nombre d'échantillons déjà stockés.
            NbLus   = sum(sum(WC.samples(1:iPing-1,:), 'omitnan'))*nBytes; % Modif JMA le 18/03/2021
            EntriesMemMap   = memmapfile(nomFicBin, ...
                                        'Offset', NbLus, ...
                                        'Format', {'int8' [1 double(NbALire)] 'x'}, ...
                                        'Writable', false, ...
                                        'Repeat', 1);
            %}
            
            %%
            
            fid = fopen([chemin,'\Ssc_WaterColumn\TxTimeHeave.bin']);
            fseek(fid,(firstPing-1)*4,'bof');
            WC.heave = fread(fid,NbPings,'single');
            fclose (fid);
            
            
        
            %ecriture fichier hac
            
            %FileOUT = [chemin,'\',chemin(size(chemin,2)-20:end),'.hac'];
            %FileOUT = [chemin,'\','seq_a_1_647_',chemin(size(chemin,2)-28:end),'.hac'];
            %FileOUT = [chemin3,'\',chemin(size(chemin,2)-28:end),'_',num2str(f),'.hac'];
            FileOUT = [chemin3,'\',NomFic(1,1:end-4),'_',num2str(f),'.hac'];
            
            fid2 = fopen(FileOUT,'w','L');
            
            Em2hac(fid2,65535)
            
            Em2hac(fid2,220,1)
            
            Em2hac(fid2,2200,1)
            
            Em2hac(fid2,41,1)
            
            
            % profile on
            indexpos=find(NbDaysPos>NbDays(1));
            j=indexpos(1);
            Em2hac(fid2,20,j)
            Em2hac(fid2,30,j)
            
            for i=1:stepvalidity:NbPings
                while NbDays(i)>NbDaysPos(j) && j<length(NbDaysPos)
                    Em2hac(fid2,20,j)
                    Em2hac(fid2,30,j)
                    j=j+1;
                end
                Em2hac(fid2,220,i)
                Em2hac(fid2,2200,i)
                Em2hac(fid2,10000,i)
            end
            
            Em2hac(fid2,65534,i)
            % profile report
            
            fclose(fid2);
            
        end % de la boucle for sur le lots pour ecrire plusieres fichiers hac par fichier all
        
        
        
        
        
        %-----------------------------------------------------------
    else %si le fichier a moins de pings que le nombre paramétré par LotNb en principe 200)
        
        firstPing=1;
        lastPing=NbPingsTot;
        stepvalidity=1;
        
        fid = fopen([chemin,'\Ssc_Runtime\TransmitPulseLength.bin']);
        PulseLength = max(fread(fid,'single'));
        fclose (fid);
        
        
        fid = fopen([chemin,'\Ssc_Runtime\TransmitBeamWidth.bin']);
        Beamwidth.along = max(fread(fid,'single'));
        fclose (fid);
        
        fid = fopen([chemin,'\Ssc_Runtime\ReceiveBeamWidth.bin']);
        Beamwidth.athwart = max(fread(fid,'single'));
        fclose (fid);
        
        
        %struct=xml_read([chemin,'\Ssc_WaterColumn.xml']);
        %NbPingsTot=struct.Dimensions.nbPings;
        %NbFaisceaux=struct.Dimensions.nbRx;
        
        struct=xml_read([chemin,'\Ssc_RawRangeBeamAngle.xml']);
        
        NbSector=struct.Dimensions.nbTx;
        
        NbPings=(lastPing-firstPing)+1;
        
        fid = fopen([chemin,'\Ssc_WaterColumn\PingCounter.bin']);
        fseek(fid,(firstPing-1)*4,'bof');
        WC.ping = fread(fid,NbPings,'single');
        fclose (fid);
        
        
        fid = fopen([chemin,'\Ssc_WaterColumn\Time.bin']);
        fseek(fid,(firstPing-1)*8,'bof');
        NbDays = fread(fid,NbPings,'double');
        fclose (fid);
        
        
        WC.time_cpu=floor((NbDays-datenum('19700101','yyyymmdd'))*86400);
        WC.time_fraction=((NbDays-datenum('19700101','yyyymmdd'))*86400-WC.time_cpu)*10000;
        
        
        WC.angles_athwart=NaN(NbPings,NbFaisceaux);
        fid = fopen([chemin,'\Ssc_WaterColumn\BeamPointingAngle.bin']);
        fseek(fid,NbFaisceaux*(firstPing-1)*4,'bof');
        for i=1:NbPings
            WC.angles_athwart(i,:) = fread(fid,NbFaisceaux,'single');
        end
        fclose (fid);
        
        
        
        
        WC.start_range=NaN(NbPings,NbFaisceaux);
        fid = fopen([chemin,'\Ssc_WaterColumn\StartRangeNumber.bin']);
        fseek(fid,NbFaisceaux*(firstPing-1)*4,'bof');
        for i=1:NbPings
            WC.start_range(i,:) = fread(fid,NbFaisceaux,'single');
        end
        fclose (fid);
        
        
        WC.frequency=NaN(NbPings,NbFaisceaux);
        WC.angles_along=NaN(NbPings,NbFaisceaux);
        fid = fopen([chemin,'\Ssc_WaterColumn\TransmitSectorNumber.bin']);
        fseek(fid,NbFaisceaux*(firstPing-1)*4,'bof');
        for i=1:NbPings
            sector(i,:) = fread(fid,NbFaisceaux,'single')-1;
        end
        fclose (fid);
        
        
        fid = fopen([chemin,'\Ssc_WaterColumn\CentralFrequency.bin']);
        fseek(fid,NbSector*(firstPing-1)*4,'bof');
        freq_list = fread(fid,NbSector*NbPings,'single');
        fclose (fid);
        
        fid = fopen([chemin,'\Ssc_WaterColumn\TiltAngle.bin']);
        fseek(fid,(firstPing-1)*NbSector*4,'bof');
        angles_list = fread(fid,NbSector*NbPings,'single');
        fclose (fid);
        
        
        fid = fopen([chemin,'\Ssc_WaterColumn\TiltAngle.bin']);
        angles_list2 = fread(fid,'single');
        fclose (fid);
        
        for i=1:NbPings
            if NbSector==1
                WC.frequency(i,:)=freq_list*ones(1,NbFaisceaux);
                WC.angles_along(i,:)=angles_list*ones(1,NbFaisceaux);
            else
                for j=1:NbSector
                    if find(sector(i,:)==j)
                        WC.frequency(i,find(sector(i,:)==j))=freq_list(NbSector*(i-1)+j);
                        WC.angles_along(i,find(sector(i,:)==j))=angles_list(NbSector*(i-1)+j);
                    end
                end
            end
        end
        
        fid = fopen([chemin,'\Ssc_WaterColumn\SamplingFreq.bin']);
        fseek(fid,(firstPing-1)*4,'bof');
        WC.sampling = fread(fid,NbPings,'single')*0.1;
        fclose (fid);
        
        fid = fopen([chemin,'\Ssc_WaterColumn\SoundSpeed.bin']);
        fseek(fid,(firstPing-1)*4,'bof');
        WC.soundSpeed = fread(fid,NbPings,'single')*0.1;
        fclose (fid);
        
        
        
        
        WC.rangebottom=NaN(NbPings,NbFaisceaux);
        fid = fopen([chemin,'\Ssc_WaterColumn\DetectedRangeInSamples.bin']);
        fseek(fid,(firstPing-1)*4*NbFaisceaux,'bof');
        for i=1:NbPings
            WC.rangebottom(i,:) = fread(fid,NbFaisceaux,'single');
        end
        
        
        AllSamples=NaN(NbPingsTot,NbFaisceaux);
        
        fid = fopen([chemin,'\Ssc_WaterColumn\NumberOfSamples.bin']);
        for i=1:NbPingsTot
            AllSamples(i,:) = fread(fid,NbFaisceaux,'single');
        end
        WC.samples=AllSamples(firstPing:lastPing,:);
        
        WC.amplitudes=NaN(NbPings,NbFaisceaux,max(max(WC.samples(:,:))),'single');
        fid = fopen([chemin,'\WCD_WaterColumn\SampleAmplitude.bin']);
        fseek(fid,sum(sum(AllSamples(1:firstPing-1,:))),'bof');
        for i=1:NbPings
            for j=1:NbFaisceaux
                WC.amplitudes(i,j,1:WC.samples(i,j)) = fread(fid,WC.samples(i,j),'int8')*0.5;
            end
        end
        
        fid = fopen([chemin,'\Ssc_WaterColumn\TxTimeHeave.bin']);
        fseek(fid,(firstPing-1)*4,'bof');
        WC.heave = fread(fid,NbPings,'single');
        fclose (fid);
        
        
        fid = fopen([chemin,'\Ssc_Position\Time.bin']);
        NbDaysPos = fread(fid,'double');
        fclose (fid);
        
        
        Position.time_cpu=floor((NbDaysPos-datenum('19700101','yyyymmdd'))*86400);
        Position.time_fraction=((NbDaysPos-datenum('19700101','yyyymmdd'))*86400-Position.time_cpu)*10000;
        
        fid = fopen([chemin,'\Ssc_Position\Latitude.bin']);
        Position.latitude = fread(fid,'double');
        fclose (fid);
        
        fid = fopen([chemin,'\Ssc_Position\Longitude.bin']);
        Position.longitude = fread(fid,'double');
        fclose (fid);
        
        fid = fopen([chemin,'\Ssc_Position\Speed.bin']);
        Position.speed = fread(fid,'single');
        fclose (fid);
        
        % Position.speed = 3.6*ones(NbPings);
        
        fid = fopen([chemin,'\Ssc_Position\Heading.bin']);
        Position.heading = fread(fid,'single');
        fclose (fid);
        
        %ecriture fichier hac


        %FileOUT = [chemin,'\',chemin(size(chemin,2)-20:end),'.hac'];
        %FileOUT = [chemin,'\','seq_a_1_647_',chemin(size(chemin,2)-28:end),'.hac'];
        %FileOUT = [chemin3,'\',chemin(size(chemin,2)-28:end),'_1.hac'];
        FileOUT = [chemin3,'\',NomFic(1,1:end-4),'_1.hac'];
        fid2 = fopen(FileOUT,'w','L');
        
        Em2hac(fid2,65535)
        
        Em2hac(fid2,220,1)
        
        Em2hac(fid2,2200,1)
        
        Em2hac(fid2,41,1)
        
        
        
        indexpos=find(NbDaysPos>NbDays(1));
        j=indexpos(1);
        Em2hac(fid2,20,j)
        Em2hac(fid2,30,j)
        
        for i=1:stepvalidity:NbPings
            while NbDays(i)>NbDaysPos(j)&& j<length(NbDaysPos)
                Em2hac(fid2,20,j)
                Em2hac(fid2,30,j)
                j=j+1;
            end
            Em2hac(fid2,220,i)
            Em2hac(fid2,2200,i)
            Em2hac(fid2,10000,i)
        end
        
        
        Em2hac(fid2,65534,i)
        
        fclose(fid2);
        
    end %if sur la condition sur le lot nob de pings
end %boucle sur le nb repertoires
toc

