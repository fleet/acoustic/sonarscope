% Recopie par correspondance vers structure de type HAC Tuple depuis les datagrammes
% Simrad.
%
% Syntax
%   AttHAC = loadAttTupleDataFromBinFile(AttALLBinData)
%
% Input Arguments
%   AttBinData : donn�es de datagramms d'attitude depuis les fichiers SIMRAD.
%
% Examples
%   ADU
%
% See also ssc2hac Authors
% Authors : GLU
% VERSION  : $Id: ssc2hac.m,v 1.0 2011/05/19 11:53:30 rgallou Exp $
%-----------------------------------------------------------------------
function Att = loadAttTupleDataFromBinFile(AttDatagrams, AttBinData)


Att.Ping   = AttBinData.PingCounter;
Time       = AttBinData.Time.timeMat;   

identRoll         = findIndVariable(AttDatagrams.Signals, 'Roll');
identPitch        = findIndVariable(AttDatagrams.Signals, 'Pitch');
identHeave        = findIndVariable(AttDatagrams.Signals, 'Heave');
identHeading      = findIndVariable(AttDatagrams.Signals, 'Heading');
for k=1:size(AttBinData.NbCycles,1)
    % On ne conserve que la premi�re d'un cycle de donn�es Attitude.
    % IndexCycles pointe sur chaque d�but de cycle.
    indexCycles         = sum(AttBinData.NbCycles(1:k-1))+1;
    % D�composition du time.
    Att.time_cpu(k)         = floor((Time(indexCycles)-datenum('19700101','yyyymmdd'))*86400);
    Att.time_fraction(k)    =((Time(indexCycles)-datenum('19700101','yyyymmdd'))*86400-Att.time_cpu(k))*10000;

    Att.roll(k)            = AttBinData.Roll(indexCycles)*AttDatagrams.Signals(identRoll).ScaleFactor;
    Att.pitch(k)           = AttBinData.Pitch(indexCycles)*AttDatagrams.Signals(identPitch).ScaleFactor;
    Att.heave(k)           = AttBinData.Heave(indexCycles)*AttDatagrams.Signals(identHeave).ScaleFactor;
    Att.heading(k)         = AttBinData.Heading(indexCycles)*AttDatagrams.Signals(identHeading).ScaleFactor;
end
