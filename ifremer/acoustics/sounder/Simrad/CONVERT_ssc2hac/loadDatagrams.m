function [flag, Datagrams] = loadDatagrams(nomDirSignal, nameDatagram)

flag      = 0;
nomFicXml = fullfile(nomDirSignal, ['SSC_' nameDatagram '.xml']);
if exist(nomFicXml, 'file')
    Datagrams = xml_read(nomFicXml);
else
    flag = -1;
    Datagrams = [];
    % On poursuit l'analyse des datagrams existants.
    return
end
