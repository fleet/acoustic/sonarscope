% Recopie par correspondance vers structure de type HAC Tuple depuis les datagrammes
% Simrad.
%
% Syntax
%   IPHAC = loadIPTupleDataFromBinFile(IPALLBinData)
%
% Input Arguments
%   IPBinData : donn�es de datagramms d'installation Parameters depuis les fichiers SIMRAD.
%
% Examples
%   ADU
%
% See also ssc2hac Authors
% Authors : GLU
%-----------------------------------------------------------------------

function Config = loadIPTupleDataFromBinFile(IPBinData)

Config.AlongshipOffsetRelToAttSensor      = IPBinData.S1X;
Config.AthwartshipOffsetRelToAttSensor    = IPBinData.S1Y;
Config.VerticalOffsetRelToAttSensor       = IPBinData.S1Z;
Config.AlongshipAngleOffsetOfTransducer   = IPBinData.S1H;
Config.AthwartshipAngleOffsetOfTransducer = IPBinData.S1R;
Config.RotationAngleOffsetOfTransducer    = IPBinData.S1P;
