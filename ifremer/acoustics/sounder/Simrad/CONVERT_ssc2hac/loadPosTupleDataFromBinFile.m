function Pos = loadPosTupleDataFromBinFile(PosBinData)

Pos.Ping = PosBinData.PingCounter;
Time     = PosBinData.Time.timeMat;

% Décomposition du time.
Pos.time_cpu=floor((Time-datenum('19700101','yyyymmdd'))*86400);
Pos.time_fraction=((Time-datenum('19700101','yyyymmdd'))*86400-Pos.time_cpu)*10000;

Pos.latitude  = PosBinData.Latitude*0.05;
Pos.longitude = PosBinData.Longitude*0.01;
Pos.speed     = PosBinData.Speed;
Pos.heading   = PosBinData.Heading;

