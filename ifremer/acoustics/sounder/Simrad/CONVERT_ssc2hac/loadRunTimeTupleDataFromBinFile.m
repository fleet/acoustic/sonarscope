% Recopie par correspondance vers structure de type HAC Tuple depuis les datagrammes
% Simrad.
%
% Syntax
%   RTHAC = loadRTTupleDataFromBinFile(RTALLBinData)
%
% Input Arguments
%   RTBinData : donn�es de datagramms de Run Time depuis les fichiers SIMRAD.
%
% Examples
%   ADU
%
% See also ssc2hac Authors
% Authors : GLU
% VERSION  : $Id: ssc2hac.m,v 1.0 2011/05/19 11:53:30 rgallou Exp $
%-----------------------------------------------------------------------
function RT = loadRunTimeTupleDataFromBinFile(RunTimeBinData)

RT.PulseLength         = max(RunTimeBinData.TransmitPulseLength)*1e-6;  % en us dans les datagrammes Simrad;
RT.BeamWidthAlong      = max(RunTimeBinData.TransmitBeamWidth)/10;      % en 0.1 deg dans les datagrammes Simrad;
RT.BeamWidthAthwart    = max(RunTimeBinData.ReceiveBeamWidth)/10;       % en 0.1 deg dans les datagrammes Simrad;
RT.BandWidth           = max(RunTimeBinData.ReceiveBandWidth);          % in 50 Hz resolution dans les datagrammes Simrad;
RT.AbsorptionCoeff     = max(RunTimeBinData.AbsorptionCoeff);           % in 0.01 dB/km dans les datagrammes Simrad;

