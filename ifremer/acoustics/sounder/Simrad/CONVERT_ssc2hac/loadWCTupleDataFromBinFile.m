% Recopie par correspondance vers structure de type HAC Tuple depuis les datagrammes
% Simrad.
%
% Syntax
%   WCHAC = loadWCTupleDataFromBinFile(WCALLBinData)
%
% Input Arguments
%   SampleData : donn�es de datagramms de Water Column depuis les fichiers SIMRAD.
%
% Examples
%   ADU
%
% See also ssc2hac Authors
% Authors : GLU
% VERSION  : $Id: ssc2hac.m,v 1.0 2011/05/19 11:53:30 rgallou Exp $
%-----------------------------------------------------------------------

function [flag, WC] = loadWCTupleDataFromBinFile(WCDatagrams, DataSample)

flag = 0;

% GLU le 13/09/2011 : NbSector    = WCDatagrams.Dimensions.nbTx;
NbPings     = WCDatagrams.Dimensions.nbPings;
NbFaisceaux = WCDatagrams.Dimensions.nbRx;

WC.Ping     = DataSample.PingCounter;
Time        = DataSample.Time.timeMat;   
% % % sub         = ([Tuple.Type] == 10000);
% % % WC.Time   = [Tuple(sub).Date];

% D�composition du time.
WC.time_cpu=floor((Time-datenum('19700101','yyyymmdd'))*86400);
WC.time_fraction=((Time-datenum('19700101','yyyymmdd'))*86400-WC.time_cpu)*10000;

identAthwart                = findIndVariable(WCDatagrams.Images, 'BeamPointingAngle');
pppp                        = DataSample.BeamPointingAngle;          % en 0.01 � dans Simrad.
pppp                        = reshape(pppp,NbFaisceaux,NbPings);
WC.angles_athwart           = pppp' * WCDatagrams.Images(identAthwart).ScaleFactor; % GLU : signe !!!!
WC.start_range              = DataSample.StartRangeNumber;
WC.Sector                   = DataSample.TransmitSectorNumber;
WC.freq_list                = DataSample.CentralFrequency;
WC.angles_list              = DataSample.TiltAngle;
WC.TVGFunctionApplied       = DataSample.TVGFunctionApplied;

pppp                        = DataSample.TransmitSectorNumber;
pppp                        = reshape(pppp,NbFaisceaux,NbPings);
sector                      = pppp';


WC.frequency        = NaN(NbPings,NbFaisceaux);
WC.angles_along     = NaN(NbPings,NbFaisceaux);

identCF          = findIndVariable(WCDatagrams.Images, 'CentralFrequency');
identAlong       = findIndVariable(WCDatagrams.Images, 'TiltAngle');

for i=1:NbPings
    % GLU le 13/09/2011 : prise en compte du nb de secteurs d'�missions
    % r�el par ping pour r�cup�rer les infos utiles. 
    NbSector    = max(sector(i, :)) + 1; % Les secteurs Tx vont de 0 � N.
    if NbSector==1
        WC.frequency(i,:)       = freq_list*ones(1,NbFaisceaux);
        WC.angles_along(i,:)    = angles_list*ones(1,NbFaisceaux);
    else
        for j=1:NbSector
            if find(sector(i,:)==j)
                try
                    WC.frequency(i,sector(i,:)==j)      = WC.freq_list(NbSector*(i-1)+j)*WCDatagrams.Images(identCF).ScaleFactor;  % en 10 Hz dans Simrad. 
                    WC.angles_along(i,sector(i,:)==j)   = WC.angles_list(NbSector*(i-1)+j)*WCDatagrams.Images(identAlong).ScaleFactor;
                catch ME
                    break
                    flag = -1;
                    return
                end
            end
        end
    end
end

identSignal         = findIndVariable(WCDatagrams.Signals, 'SamplingFreq');
WC.sampling         = DataSample.SamplingFreq*WCDatagrams.Signals(identSignal).ScaleFactor;
identSignal         = findIndVariable(WCDatagrams.Signals, 'SoundSpeed');
WC.soundSpeed       = DataSample.SoundSpeed*WCDatagrams.Signals(identSignal).ScaleFactor;

pppp                = DataSample.DetectedRangeInSamples;
pppp                = reshape(pppp,NbFaisceaux,NbPings);
WC.rangebottom      = pppp';

pppp                = DataSample.NumberOfSamples;
pppp                = reshape(pppp,NbFaisceaux,NbPings);
WC.NbSamples        = pppp';

identHeave          = findIndVariable(WCDatagrams.Signals, 'TxTimeHeave');
WC.heave            = DataSample.TxTimeHeave*WCDatagrams.Signals(identHeave).ScaleFactor;    % en cm chez Simrad



