%%
% mcc -mv 'ssc2hac.m' -R -startmsg 'toto' -R -completemsg 'titi' -d 'C:\SonarScopeTbx\ifremer\acoustics\sounder\Hac\CONVERT_ssc2hac'
% mcc -mv 'ssc2hac.m' -d 'C:\SonarScopeTbx\ifremer\extern\mcc'
%% aKM = cl_simrad_all('nomFic', nomFic)
%% Test de lancement de l'exécutable

% nomFic = 'F:\SonarScopeData\From AndreOgor\0812_20091213_205617_Lesuroit_Orig.all';
% nomFic = '???\Sonar\0307_20091113_163950_Lesuroit.all';
% nomFic = 'F:\SonarScopeData\From Carla\question 2\0314_20110129_213139.all';
% nomFic = 'F:\SonarScopeData\From Carla\question 2\0293_20110129_015608.all';
% nomFic = 'I:\Data\IFREMER\Data_SSC\SSC-3DViewer-Data\V2\Echanges\From L Berger\20110912\0603_20091119_200148_Lesuroit.all';
% nomFic = 'I:\Data\IFREMER\Data_SSC\SSC-3DViewer-Data\V2\Echanges\From L Berger\20110912\0605_20091119_210146_Lesuroit.all';
% nomFic = 'I:\Data\IFREMER\Data_SSC\SSC-3DViewer-Data\V2\Echanges\From L Berger\20110912\0037_20091129_042116_Lesuroit.all';
% nomFic = 'J:\Data\IFREMER\Data_SSC\SonarScopeData\From Carla\Futuna\0000_20111106_081640_AtalanteEM122.all';
% nomFic = 'J:\Data\IFREMER\Data_SSC\SonarScopeData\From Carla\Futuna\0001_20111106_091639_AtalanteEM122.all';
nomFic = 'F:\SonarScopeData\From Carla\EM2040\0012_20120608_163602_raw_12.all';

global IfrTbxResources %#ok<GVMIS>

nomExe = 'ssc2hac.exe';
[nomDir, nomFicSeul] = fileparts(nomFic);
nomDirRacine = fullfile(nomDir, 'SonarScope', nomFicSeul);
cmd = sprintf('!"%s" "%s" """%s"""', nomExe, 'NomFicAll', nomFic);

str1 = sprintf('Prétraitement du fichier %s', nomFic);
str2 = sprintf('Processing file %s', nomFic);

fprintf(1,'%s\n', cmd);

hw = my_warndlg(Lang(str1,str2), 0);
% % try
if isdeployed
    nomDir = fileparts(IfrTbxResources);
    nomExe = fullfile(nomDir, 'extern', 'mcc', nomExe);
    [status, msg] = dos(cmd(2:end));
else
    % nomExe = ['C:\SonarScopeTbx' '\ifremer\extern\mcc\' nomExe];
    f = ssc2hac('NomFicAll', nomFic);
end
my_close(hw);
% % catch %#ok<CTCH>
% %     str1 = sprintf('ssc2hac semble avoir rencontré un problème sur le fichier %s', nomFic);
% %     str2 = sprintf('ssc2hac does not seem working for file %s', nomFic);
% %     my_warndlg(Lang(str1,str2), 0);
% % end
%%
nomFicAll = 'F:\SonarScopeData\From Andre Ogor\0812_20091213_205617_Lesuroit_Orig.all';
flag = SimradAll_ssc2hac('NomFicAll', nomFicAll, 'RepDefaut', 'C:\Temp\Test_HAC_Creation');
%%
nomFicAll = 'C:\Temp\Test_HAC_Creation\0024_20100605_114501.all';
flag = SimradAll_ssc2hac('NomFicAll', nomFicAll, 'RepDefaut', 'C:\Temp\Test_HAC_Creation');
%%
nomFicAll = 'F:\SonarScopeData\From Carla\EM2040\0012_20120608_163602_raw_12.all';
flag = SimradAll_ssc2hac('NomFicAll', nomFicAll, 'RepDefaut', 'C:\Temp\Test_HAC_Creation');
