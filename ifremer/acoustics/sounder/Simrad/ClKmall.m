classdef ClKmall < handle
    % %% ClKmall help
    % Description
    %   Class that give acess to all the variables of a Kongsberg .kmall file
    %
    % Syntax
    %   a = ClKmall(...)
    %
    % ClKmall properties :
    %   Filename - Name of the .kmall file
    %
    % Output Arguments
    %   a : One ClKmall instance
    %
    % Examples
    %   %% Instance of ClKmall of file 0039_20180905_222154_raw.kmall
    %   b = ClKmall('Filename', 'D:\XSF\FromKmall\0039_20180905_222154_raw.kmall');        % Format Rev E - Avec Mag&Phase - avec BSCorr
    %   %% Instance of ClKmall of file 0039_20180905_222154_raw.kmall
    %   a = ClKmall('Filename', 'F:\Ridha\dataEM2040P\0038_20210609_105428_200kHz.kmall'); % Format Rev H - Avec Mag&Phase
    %   %%
    %   a
    %   %%
    %   plotInfoDatagrams(a)
    %   %%
    %   methods(a)
    %   %% 
    %   a.Filename
    %   %% 
    %   a.sDataKmAll
    %     %     index: [1×2886 struct]
    %     % dgmIIP: [1×1 struct]    : Installation parameters and sensor setup
    %     % dgmIOP: [1×1 struct]    : Runtime parameters as chosen by operator
    %     % dgmSVP: [1×1 struct]    : Sensor (S) data from sound velocity (V) profile (P) or CTD
    %     % dgmSKM: [1×1036 struct] : Sensor (S) KM binary sensor format : data from attitude and attitude velocity sensors
    %     % dgmSCL: [1×528 struct]  : Sensor (S) data from clock (CL)  : CLock datagram
    %     % dgmSPO: [1×528 struct]  : Struct of position sensor datagram
    %     % dgmCPO: [1×528 struct]  : Compatibility (C) data for position (PO)
    %     % dgmCHE: [1×84 struct]   : Compatibility (C) data for heave (HE)
    %     % dgmMRZ: [1×84 struct]   : Multibeam (M) raw range (R) and depth(Z) datagram
    %     % dgmMWC: [1×84 struct]   : Multibeam (M) water (W) column (C) datagram
    %     % dgmFCF:                 : Backscatter calibration (C) file (F) datagram : sData.dgmFCF.FCommon.bsCalibrationFile
    %     %     dgmSHA TODO GLU ??? : Lecture des datas de caractérisation des sensors
    %
    %   %% Exploration of MRZ datagrams
    %   a.sDataKmAll.dgmMRZ(1)
    %     % Partition    : [1×1 struct]
    %     % Body         : [1×1 struct]
    %     % PingInfo     : [1×1 struct]
    %     % TxSectorInfo : [1×2 struct]
    %     % RxInfo       : [1×1 struct]
    %     % Sounding     : [1×512 struct]
    %     % Samples      : [2475×1 int16]
    %
    %   %% 
    %   a.sDataKmAll.dgmMRZ(1).Sounding(1)
    %     % soundingIndex  : 0
    %     % txSectorNumber : 0
    %     % DetInfo        : [1×1 struct]
    %     % WCParams       : [1×1 struct]
    %     % BSData         : [1×1 struct]
    %     % RangeAngleData : [1×1 struct]
    %     % GeoDepthPts    : [1×1 struct]
    %     % SeabedImage    : [1×1 struct]
    %
    %   %% 
    %   a.sDataKmAll.dgmMRZ(1).Sounding(1).GeoDepthPts
    %     %   Variable name     - getter method
    %     % deltaLatitude_deg   : get_MRZ_DeltaLatitude_deg
    %     % deltaLongitude_deg  : get_MRZ_DeltaLongitude_deg
    %     % z_reRefPoint_m      : get_MRZ_Depth
    %     % y_reRefPoint_m      : get_MRZ_AcrossDist
    %     % x_reRefPoint_m      : get_MRZ_AlongDist
    %     % beamIncAngleAdj_deg : get_MRZ_BeamIBA
    %     % realTimeCleanInfo   : get_MRZ_RealTimeCleanInfo
    %
    %     MRZ_Datetime = get_MRZ_Datetime(a)
    %
    %   %% 
    %   Depth = get_MRZ_Depth(a);
    %   figure; imagesc(Depth); axis xy; colormap(jet(256)); colorbar; title('Depth');
    %
    %   %% 
    %   AcrossDist = get_MRZ_AcrossDist(a);
    %   figure; imagesc(AcrossDist); axis xy; colormap(jet(256)); colorbar; title('AcrossDist');
    %
    %   %% 
    %   AlongDist = get_MRZ_AlongDist(a);
    %   figure; imagesc(AlongDist); axis xy; colormap(jet(256)); colorbar; title('AlongDist');
    %
    %   %% 
    %   BeamIBA = get_MRZ_BeamIBA(a);
    %   figure; imagesc(BeamIBA); axis xy; colormap(jet(256)); colorbar; title('BeamIBA');
    %
    %   %% 
    %   RealTimeCleanInfo = get_MRZ_RealTimeCleanInfo(a);
    %   figure; imagesc(RealTimeCleanInfo); axis xy; colormap(jet(256)); colorbar; title('RealTimeCleanInfo');
    %
    %   %% 
    %   DeltaLatitude = get_MRZ_DeltaLatitude_deg(a);
    %   figure; imagesc(DeltaLatitude); axis xy; colormap(jet(256)); colorbar; title('DeltaLatitude');
    %
    %   %% 
    %   DeltaLongitude = get_MRZ_DeltaLongitude_deg(a);
    %   figure; imagesc(DeltaLongitude); axis xy; colormap(jet(256)); colorbar; title('DeltaLongitude');
    %
    %   %% 
    %   a.sDataKmAll.dgmMRZ(1).Sounding(1).DetInfo
    %     %   Variable name           -   getter method
    %     % detectionType             : get_MRZ_DetectionType
    %     % detectionMethod           : get_MRZ_DetectionMethod
    %     % rejectionInfo1            : get_MRZ_RejectionInfo1
    %     % rejectionInfo2            : get_MRZ_RejectionInfo2
    %     % postProcessingInfo        : get_MRZ_PostProcessingInfo
    %     % detectionClass            : get_MRZ_DetectionClass
    %     % detectionConfidenceLevel  : get_MRZ_DetectionConfidenceLevel
    %     % padding                   :
    %     % rangeFactor               : get_MRZ_RangeFactor
    %     % qualityFactor             : get_MRZ_QualityFactor
    %     % detectionUncertaintyVer_m : get_MRZ_DetectionUncertaintyVer_m
    %     % detectionUncertaintyHor_m : get_MRZ_DetectionUncertaintyHor_m
    %     % detectionWindowLength_sec : get_MRZ_DetectionWindowLength_sec
    %     % echoLength_sec            : get_MRZ_EchoLength_sec
    %
    %   %% 
    %   DetectionType = get_MRZ_DetectionType(a);
    %   figure; imagesc(DetectionType); axis xy; colormap(jet(256)); colorbar; title('DetectionType');
    %
    %   %% 
    %   DetectionMethod = get_MRZ_DetectionMethod(a);
    %   figure; imagesc(DetectionMethod); axis xy; colormap(jet(256)); colorbar; title('DetectionMethod');
    %
    %   %% 
    %   RejectionInfo1 = get_MRZ_RejectionInfo1(a);
    %   figure; imagesc(RejectionInfo1); axis xy; colormap(jet(256)); colorbar; title('RejectionInfo1');
    %
    %   %% 
    %   RejectionInfo2 = get_MRZ_RejectionInfo2(a);
    %   figure; imagesc(RejectionInfo2); axis xy; colormap(jet(256)); colorbar; title('RejectionInfo2');
    %
    %   %% 
    %   PostProcessingInfo = get_MRZ_PostProcessingInfo(a);
    %   figure; imagesc(PostProcessingInfo); axis xy; colormap(jet(256)); colorbar; title('PostProcessingInfo');
    %
    %   %% 
    %   DetectionConfidenceLevel = get_MRZ_DetectionConfidenceLevel(a);
    %   figure; imagesc(DetectionConfidenceLevel); axis xy; colormap(jet(256)); colorbar; title('DetectionConfidenceLevel');
    %
    %   %% 
    %   RangeFactor = get_MRZ_RangeFactor(a);
    %   figure; imagesc(RangeFactor); axis xy; colormap(jet(256)); colorbar; title('RangeFactor');
    %
    %   %% 
    %   QualityFactor = get_MRZ_QualityFactor(a);
    %   figure; imagesc(QualityFactor); axis xy; colormap(jet(256)); colorbar; title('QualityFactor');
    %
    %   %% 
    %   DetectionUncertaintyVer_m = get_MRZ_DetectionUncertaintyVer_m(a);
    %   figure; imagesc(DetectionUncertaintyVer_m); axis xy; colormap(jet(256)); colorbar; title('DetectionUncertaintyVer_m', 'interp', 'None');
    %
    %   %% 
    %   DetectionUncertaintyHor_m = get_MRZ_DetectionUncertaintyHor_m(a);
    %   figure; imagesc(DetectionUncertaintyHor_m); axis xy; colormap(jet(256)); colorbar; title('DetectionUncertaintyHor_m', 'interp', 'None');
    %
    %   %% 
    %   DetectionWindowLength_sec = get_MRZ_DetectionWindowLength_sec(a);
    %   figure; imagesc(DetectionWindowLength_sec); axis xy; colormap(jet(256)); colorbar; title('DetectionWindowLength_sec', 'interp', 'None');
    %
    %   %% 
    %   EchoLength_sec = get_MRZ_EchoLength_sec(a);
    %   figure; imagesc(EchoLength_sec); axis xy; colormap(jet(256)); colorbar; title('EchoLength_sec', 'interp', 'None');
    %
    %   %% 
    %   a.sDataKmAll.dgmMRZ(1).Sounding(1).BSData
    %     %   Variable name     - getter method
    %     % meanAbsCoeff_dBPerkm          : get_MRZ_MeanAbsCoeff_dBPerkm
    %     % reflectivity1_dB              : get_MRZ_Reflectivity1_dB
    %     % reflectivity2_dB              : get_MRZ_Reflectivity2_dB
    %     % receiverSensitivityApplied_dB : get_MRZ_ReceiverSensitivityApplied_dB
    %     % sourceLevelApplied_dB         : get_MRZ_SourceLevelApplied_dB
    %     % BScalibration_dB              : get_MRZ_BScalibration_dB
    %     % TVG_dB                        : get_MRZ_TVG_dB
    %
    %   %% 
    %   MeanAbsCoeff_dBPerk = get_MRZ_MeanAbsCoeff_dBPerkm(a);
    %   figure; imagesc(MeanAbsCoeff_dBPerk); axis xy; colormap(jet(256)); colorbar; title('MeanAbsCoeff_dBPerk', 'interp', 'None');
    %
    %   %% 
    %   Reflectivity1_dB = get_MRZ_Reflectivity1_dB(a);
    %   figure; imagesc(Reflectivity1_dB); axis xy; colormap(gray(256)); colorbar; title('Reflectivity1_dB', 'interp', 'None');
    %
    %   %% 
    %   Reflectivity2_dB = get_MRZ_Reflectivity2_dB(a);
    %   figure; imagesc(Reflectivity2_dB); axis xy; colormap(gray(256)); colorbar; title('Reflectivity2_dB', 'interp', 'None');
    %
    %   %% 
    %   ReceiverSensitivityApplied_dB = get_MRZ_ReceiverSensitivityApplied_dB(a);
    %   figure; imagesc(ReceiverSensitivityApplied_dB); axis xy; colormap(jet(256)); colorbar; title('ReceiverSensitivityApplied_dB', 'interp', 'None');
    %
    %   %% 
    %   SourceLevelApplied_dB = get_MRZ_SourceLevelApplied_dB(a);
    %   figure; imagesc(SourceLevelApplied_dB); axis xy; colormap(jet(256)); colorbar; title('SourceLevelApplied_dB', 'interp', 'None');
    %
    %   %% 
    %   BScalibration_dB = get_MRZ_BScalibration_dB(a);
    %   figure; imagesc(BScalibration_dB); axis xy; colormap(jet(256)); colorbar; title('BScalibration_dB', 'interp', 'None');
    %
    %   %% 
    %   TVG_dB = get_MRZ_TVG_dB(a);
    %   figure; imagesc(TVG_dB); axis xy; colormap(jet(256)); colorbar; title('TVG_dB', 'interp', 'None');
    %
    %   %% 
    %   a.sDataKmAll.dgmMRZ(1).Sounding(1).RangeAngleData
    %     %   Variable name                - getter method
    %     % beamAngleReRx_deg              : get_MRZ_BeamAngleReRx_deg
    %     % beamAngleReRxCorrection_deg    : get_MRZ_BeamAngleReRxCorrection_deg
    %     % twoWayTravelTime_sec           : get_MRZ_TwoWayTravelTime_sec
    %     % twoWayTravelTimeCorrection_sec : get_MRZ_TwoWayTravelTimeCorrection_sec
    %
    %   %% 
    %   BeamAngleReRx_deg = get_MRZ_BeamAngleReRx_deg(a);
    %   figure; imagesc(BeamAngleReRx_deg); axis xy; colormap(jet(256)); colorbar; title('BeamAngleReRx_deg', 'interp', 'None');
    %
    %   %% 
    %   BeamAngleReRxCorrection_deg = get_MRZ_BeamAngleReRxCorrection_deg(a);
    %   figure; imagesc(BeamAngleReRxCorrection_deg); axis xy; colormap(jet(256)); colorbar; title('BeamAngleReRxCorrection_deg', 'interp', 'None');
    %
    %   %% 
    %   TwoWayTravelTime_sec = get_MRZ_TwoWayTravelTime_sec(a);
    %   figure; imagesc(TwoWayTravelTime_sec); axis xy; colormap(jet(256)); colorbar; title('TwoWayTravelTime_sec', 'interp', 'None');
    %
    %   %% 
    %   TwoWayTravelTimeCorrection_sec = get_MRZ_TwoWayTravelTimeCorrection_sec(a);
    %   figure; imagesc(TwoWayTravelTimeCorrection_sec); axis xy; colormap(jet(256)); colorbar; title('TwoWayTravelTimeCorrection_sec', 'interp', 'None');
    %
    %   %% 
    %   a.sDataKmAll.dgmMRZ(1).Sounding(1).SeabedImage
    %     %   Variable name      - getter method
    %     % SIstartRange_samples : get_MRZ_SIstartRange_samples
    %     % SIcentreSample       : get_MRZ_SIcentreSample
    %     % SInumSamples         : get_MRZ_SInumSamples
    %
    %   %% 
    %   SIstartRange_samples = get_MRZ_SIstartRange_samples(a);
    %   figure; imagesc(SIstartRange_samples); axis xy; colormap(jet(256)); colorbar; title('SIstartRange_samples', 'interp', 'None');
    %
    %   %% 
    %   SIcentreSample = get_MRZ_SIcentreSample(a);
    %   figure; imagesc(SIcentreSample); axis xy; colormap(jet(256)); colorbar; title('SIcentreSample');
    %
    %   %% 
    %   SInumSamples = get_MRZ_SInumSamples(a);
    %   figure; imagesc(SInumSamples); axis xy; colormap(jet(256)); colorbar; title('SInumSamples');
    %
    %   %% 
    %   a.sDataKmAll.dgmMRZ(1).Sounding(1).WCParams
    %     %   Variable name      - getter method
    %     % WCBeamNumber         : get_MRZ_WCBeamNumber
    %     % WCRangeSamples       : get_MRZ_WCRangeSamples
    %     % WCNomBeamAngleAcross : get_MRZ_WCNomBeamAngleAcross
    %
    %   %% 
    %   WCBeamNumber = get_MRZ_WCBeamNumber(a);
    %   figure; imagesc(WCBeamNumber); axis xy; colormap(jet(256)); colorbar; title('WCBeamNumber');
    %
    %   %% 
    %   WCRangeSamples = get_MRZ_WCRangeSamples(a);
    %   figure; imagesc(WCRangeSamples); axis xy; colormap(jet(256)); colorbar; title('WCRangeSamples');
    %
    %   %% 
    %   WCNomBeamAngleAcross = get_MRZ_WCNomBeamAngleAcross(a);
    %   figure; imagesc(WCNomBeamAngleAcross); axis xy; colormap(jet(256)); colorbar; title('WCNomBeamAngleAcross');
    %
    %   %% 
    %   a.sDataKmAll.dgmMRZ(1).Sounding(1)
    %     %  Variable name -  getter method
    %     % soundingIndex  : get_MRZ_SoundingIndex
    %     % txSectorNumber : get_MRZ_TxSectorNumber
    %
    %   %% 
    %   SoundingIndex = get_MRZ_SoundingIndex(a);
    %   figure; imagesc(SoundingIndex); axis xy; colormap(jet(256)); colorbar; title('SoundingIndex');
    %
    %   %% 
    %   TxSectorNumber = get_MRZ_TxSectorNumber(a);
    %   figure; imagesc(TxSectorNumber); axis xy; colormap(jet(256)); colorbar; title('TxSectorNumber');
    %
    %
    %   %% 
    %   a.sDataKmAll.dgmMRZ(1).RxInfo
    %     %    Variable name         -   getter method
    %     % numBytesRxInfo           : get_MRZ_NumBytesRxInfo
    %     % numSoundingsMaxMain      : get_MRZ_NumSoundingsMaxMain
    %     % numSoundingsValidMain    : get_MRZ_NumSoundingsValidMain
    %     % numBytesPerSounding      : get_MRZ_NumBytesPerSounding
    %     % WCSampleRate             : get_MRZ_WCSampleRate
    %     % seabedImageSampleRate    : get_MRZ_SeabedImageSampleRate
    %     % BSnormal_dB              : get_MRZ_BSnormal_dB
    %     % BSoblique_dB             : get_MRZ_BSoblique_dB
    %     % extraDetectionAlarmFlag  : get_MRZ_ExtraDetectionAlarmFlag
    %     % numExtraDetections       : get_MRZ_NumExtraDetections
    %     % numExtraDetectionClasses : get_MRZ_NumExtraDetectionClasses
    %     % numBytesPerClass         : get_MRZ_NumBytesPerClass
    %
    %   %% 
    %   NumBytesRxInfo = get_MRZ_NumBytesRxInfo(a);
    %   figure; plot(NumBytesRxInfo); grid on; title('NumBytesRxInfo');
    %
    %   %% 
    %   NumSoundingsMaxMain = get_MRZ_NumSoundingsMaxMain(a);
    %   figure; plot(NumSoundingsMaxMain); grid on; title('NumSoundingsMaxMain');
    %
    %   %% 
    %   NumSoundingsValidMain = get_MRZ_NumSoundingsValidMain(a);
    %   figure; plot(NumSoundingsValidMain); grid on; title('NumSoundingsValidMain');
    %
    %   %% 
    %   NumBytesPerSounding = get_MRZ_NumBytesPerSounding(a);
    %   figure; plot(NumBytesPerSounding); grid on; title('NumBytesPerSounding');
    %
    %   %% 
    %   WCSampleRate = get_MRZ_WCSampleRate(a);
    %   figure; plot(WCSampleRate); grid on; title('WCSampleRate');
    %
    %   %% 
    %   SeabedImageSampleRate = get_MRZ_SeabedImageSampleRate(a);
    %   figure; plot(SeabedImageSampleRate); grid on; title('SeabedImageSampleRate');
    %
    %   %% 
    %   BSnormal_dB = get_MRZ_BSnormal_dB(a);
    %   figure; plot(BSnormal_dB); grid on; title('BSnormal_dB', 'interp', 'None');
    %
    %   %% 
    %   BSoblique_dB = get_MRZ_BSoblique_dB(a);
    %   figure; plot(BSoblique_dB); grid on; title('BSoblique_dB', 'interp', 'None');
    %
    %   %% 
    %   ExtraDetectionAlarmFlag = get_MRZ_ExtraDetectionAlarmFlag(a);
    %   figure; plot(ExtraDetectionAlarmFlag); grid on; title('ExtraDetectionAlarmFlag');
    %
    %   %% 
    %   NumExtraDetections = get_MRZ_NumExtraDetections(a);
    %   figure; plot(NumExtraDetections); grid on; title('NumExtraDetections');
    %
    %   %% 
    %   NumExtraDetectionClasses = get_MRZ_NumExtraDetectionClasses(a);
    %   figure; plot(NumExtraDetectionClasses); grid on; title('NumExtraDetectionClasses');
    %
    %   %% 
    %   NumBytesPerClass = get_MRZ_NumBytesPerClass(a);
    %   figure; plot(NumBytesPerClass); grid on; title('NumBytesPerClass');
    %
    %
    %   %% 
    %   a.sDataKmAll.dgmMRZ(1).PingInfo
    %     %   Variable name  -  getter method
    %     % numBytesInfoData : get_MRZ_NumBytesInfoData
    %     % padding0         :
    %     % General          : [1×1 struct]
    %     % TxSectorInfo     : [1×1 struct]
    %     % TxTime1stPulse   : [1×1 struct]
    %     % dgmParams        : [1×1 struct]
    %
    %   %% 
    %   NumBytesInfoData = get_MRZ_NumBytesInfoData(a);
    %   figure; plot(NumBytesInfoData); grid on; title('NumBytesInfoData');
    %
    %   MRZ_Datetime = get_MRZ_Datetime(a)
    %
    %
    %   %% 
    %   b.sDataKmAll.dgmMRZ(1).PingInfo.General % TODO :: b.sDataKmAll.dgmMRZ(1).PingInfo ne donne pas la même structure !!!
    %     %   Variable name           -  getter method
    %     % pingRate_Hz               : get_MRZ_PingRate_Hz
    %     % beamSpacing               : get_MRZ_BeamSpacing
    %     % depthMode                 : get_MRZ_DepthMode
    %     % subDepthMode              : get_MRZ_SubDepthMode
    %     % distanceBtwSwath          : get_MRZ_DistanceBtwSwath
    %     % detectionMode             : get_MRZ_DetectionMode
    %     % pulseForm                 : get_MRZ_PulseForm
    %     % padding1                  :
    %     % frequencyMode_Hz          : get_MRZ_FrequencyMode_Hz
    %     % freqRangeLowLim_Hz        : get_MRZ_FreqRangeLowLim_Hz
    %     % freqRangeHighLim_Hz       : get_MRZ_FreqRangeHighLim_Hz
    %     % maxTotalTxPulseLength_sec : get_MRZ_MaxTotalTxPulseLength_sec
    %     % maxEffTxPulseLength_sec   : get_MRZ_MaxEffTxPulseLength_sec
    %     % maxEffTxBandWidth_Hz      : get_MRZ_MaxEffTxBandWidth_Hz
    %     % absCoeff_dBPerkm          : get_MRZ_AbsCoeff_dBPerkm
    %     % portSectorEdge_deg        : get_MRZ_PortSectorEdge_deg
    %     % starbSectorEdge_deg       : get_MRZ_StarbSectorEdge_deg
    %     % portMeanCov_deg           : get_MRZ_PortMeanCov_deg
    %     % starbMeanCov_deg          : get_MRZ_StarbMeanCov_deg
    %     % portMeanCov_m             : get_MRZ_PortMeanCov_m
    %     % starbMeanCov_m            : get_MRZ_StarbMeanCov_m
    %     % modeAndStabilisation      : get_MRZ_ModeAndStabilisation
    %     % runtimeFilter1            : get_MRZ_RuntimeFilter1
    %     % runtimeFilter2            : get_MRZ_RuntimeFilter2
    %     % pipeTrackingStatus        : get_MRZ_PipeTrackingStatus
    %     % transmitArraySizeUsed_deg : get_MRZ_TransmitArraySizeUsed_deg
    %     % receiveArraySizeUsed_deg  : get_MRZ_ReceiveArraySizeUsed_deg
    %     % transmitPower_dB          : get_MRZ_TransmitPower_dB
    %     % SLrampUpTimeRemaining     : get_MRZ_SLrampUpTimeRemaining
    %     % padding2                  : 0
    %     % yawAngle_deg              : get_MRZ_YawAngle_deg
    %
    %   %% 
    %   PingRate_Hz = get_MRZ_PingRate_Hz(b);
    %   figure; plot(PingRate_Hz); grid on; title('PingRate_Hz', 'Interpreter', 'None');
    %
    %   %% 
    %   BeamSpacing = get_MRZ_BeamSpacing(b);
    %   figure; plot(BeamSpacing); grid on; title('BeamSpacing');
    %
    %   %% 
    %   DepthMode = get_MRZ_DepthMode(b);
    %   figure; plot(DepthMode); grid on; title('DepthMode');
    %
    %   %% 
    %   SubDepthMode = get_MRZ_SubDepthMode(b);
    %   figure; plot(SubDepthMode); grid on; title('SubDepthMode');
    %
    %   %% 
    %   DistanceBtwSwath = get_MRZ_DistanceBtwSwath(b);
    %   figure; plot(DistanceBtwSwath); grid on; title('DistanceBtwSwath');
    %
    %   %% 
    %   DetectionMode = get_MRZ_DetectionMode(b);
    %   figure; plot(DetectionMode); grid on; title('DetectionMode');
    %
    %   %% 
    %   PulseForm = get_MRZ_PulseForm(b);
    %   figure; plot(PulseForm); grid on; title('PulseForm');
    %
    %   %% 
    %   FrequencyMode_Hz = get_MRZ_FrequencyMode_Hz(b);
    %   figure; plot(FrequencyMode_Hz); grid on; title('FrequencyMode_Hz', 'Interpreter', 'None');
    %
    %   %% 
    %   FreqRangeLowLim_Hz = get_MRZ_FreqRangeLowLim_Hz(b);
    %   figure; plot(FreqRangeLowLim_Hz); grid on; title('FreqRangeLowLim_Hz', 'Interpreter', 'None');
    %
    %   %% 
    %   FreqRangeHighLim_Hz = get_MRZ_FreqRangeHighLim_Hz(b);
    %   figure; plot(FreqRangeHighLim_Hz); grid on; title('FreqRangeHighLim_Hz', 'Interpreter', 'None');
    %
    %   %% 
    %   MaxTotalTxPulseLength_sec = get_MRZ_MaxTotalTxPulseLength_sec(b);
    %   figure; plot(MaxTotalTxPulseLength_sec); grid on; title('MaxTotalTxPulseLength_sec', 'Interpreter', 'None');
    %
    %   %% 
    %   MaxEffTxPulseLength_sec = get_MRZ_MaxEffTxPulseLength_sec(b);
    %   figure; plot(MaxEffTxPulseLength_sec); grid on; title('MaxEffTxPulseLength_sec', 'Interpreter', 'None');
    %
    %   %% 
    %   MaxEffTxBandWidth_Hz = get_MRZ_MaxEffTxBandWidth_Hz(b);
    %   figure; plot(MaxEffTxBandWidth_Hz); grid on; title('MaxEffTxBandWidth_Hz', 'Interpreter', 'None');
    %
    %   %% 
    %   AbsCoeff_dBPerkm = get_MRZ_AbsCoeff_dBPerkm(b);
    %   figure; plot(AbsCoeff_dBPerkm); grid on; title('AbsCoeff_dBPerkm', 'Interpreter', 'None');
    %
    %   %% 
    %   PortSectorEdge_deg = get_MRZ_PortSectorEdge_deg(b);
    %   figure; plot(PortSectorEdge_deg); grid on; title('PortSectorEdge_deg', 'Interpreter', 'None');
    %
    %   %% 
    %   StarbSectorEdge_deg = get_MRZ_StarbSectorEdge_deg(b);
    %   figure; plot(StarbSectorEdge_deg); grid on; title('StarbSectorEdge_deg', 'Interpreter', 'None');
    %
    %   %% 
    %   PortMeanCov_deg = get_MRZ_PortMeanCov_deg(b);
    %   figure; plot(PortMeanCov_deg); grid on; title('PortMeanCov_deg', 'Interpreter', 'None');
    %
    %   %% 
    %   StarbMeanCov_deg = get_MRZ_StarbMeanCov_deg(b);
    %   figure; plot(StarbMeanCov_deg); grid on; title('StarbMeanCov_deg', 'Interpreter', 'None');
    %
    %   %% 
    %   PortMeanCov_m = get_MRZ_PortMeanCov_m(b);
    %   figure; plot(PortMeanCov_m); grid on; title('PortMeanCov_m', 'Interpreter', 'None');
    %
    %   %% 
    %   StarbMeanCov_m = get_MRZ_StarbMeanCov_m(b);
    %   figure; plot(StarbMeanCov_m); grid on; title('StarbMeanCov_m', 'Interpreter', 'None');
    %
    %   %% 
    %   ModeAndStabilisation = get_MRZ_ModeAndStabilisation(b);
    %   figure; plot(ModeAndStabilisation); grid on; title('ModeAndStabilisation');
    %
    %   %% 
    %   RuntimeFilter1 = get_MRZ_RuntimeFilter1(b);
    %   figure; plot(RuntimeFilter1); grid on; title('RuntimeFilter1');
    %
    %   %% 
    %   RuntimeFilter2 = get_MRZ_RuntimeFilter2(b);
    %   figure; plot(RuntimeFilter2); grid on; title('RuntimeFilter2');
    %
    %   %% 
    %   PipeTrackingStatus = get_MRZ_PipeTrackingStatus(b);
    %   figure; plot(PipeTrackingStatus); grid on; title('PipeTrackingStatus');
    %
    %   %% 
    %   TransmitArraySizeUsed_deg = get_MRZ_TransmitArraySizeUsed_deg(b);
    %   figure; plot(TransmitArraySizeUsed_deg); grid on; title('TransmitArraySizeUsed_deg', 'Interpreter', 'None');
    %
    %   %% 
    %   ReceiveArraySizeUsed_deg = get_MRZ_ReceiveArraySizeUsed_deg(b);
    %   figure; plot(ReceiveArraySizeUsed_deg); grid on; title('ReceiveArraySizeUsed_deg', 'Interpreter', 'None');
    %
    %   %% 
    %   TransmitPower_dB = get_MRZ_TransmitPower_dB(b);
    %   figure; plot(TransmitPower_dB); grid on; title('TransmitPower_dB', 'Interpreter', 'None');
    %
    %   %% 
    %   SLrampUpTimeRemaining = get_MRZ_SLrampUpTimeRemaining(b);
    %   figure; plot(SLrampUpTimeRemaining); grid on; title('SLrampUpTimeRemaining');
    %
    %   %% 
    %   YawAngle_deg = get_MRZ_YawAngle_deg(b);
    %   figure; plot(YawAngle_deg); grid on; title('YawAngle_deg', 'Interpreter', 'None');
    %
    %
    %   %% 
    %   a.sDataKmAll.dgmMRZ(1).PingInfo.TxSectorInfo
    %     %   Variable name     -  getter method
    %     % numTxSectors        : get_MRZ_NumTxSectors
    %     % numBytesPerTxSector : get_MRZ_NumBytesPerTxSector
    %
    %   %% 
    %   NumTxSectors = get_MRZ_NumTxSectors(a);
    %   figure; plot(NumTxSectors); grid on; title('NumTxSectors');
    %
    %   %% 
    %   NumBytesPerTxSector = get_MRZ_NumBytesPerTxSector(a);
    %   figure; plot(NumBytesPerTxSector); grid on; title('NumBytesPerTxSector');
    %
    %
    %   %% 
    %   a.sDataKmAll.dgmMRZ(1).PingInfo.TxTime1stPulse
    %     %   Variable name             -  getter method
    %     % headingVessel_deg           : get_MRZ_HeadingVessel_deg
    %     % soundSpeedAtTxDepth_mPerSec : get_MRZ_SoundSpeedAtTxDepth_mPerSec
    %     % txTransducerDepth_m         : get_MRZ_TxTransducerDepth_m
    %     % z_waterLevelReRefPoint_m    : get_MRZ_Z_waterLevelReRefPoint_m
    %     % x_kmallToall_m              : get_MRZ_X_kmallToall_m
    %     % y_kmallToall_m              : get_MRZ_Y_kmallToall_m
    %     % latLongInfo                 : get_MRZ_LatLongInfo
    %     % posSensorStatus             : get_MRZ_PosSensorStatus
    %     % attitudeSensorStatus        : get_MRZ_AttitudeSensorStatus
    %     % padding3                    :
    %     % latitude_deg                : get_MRZ_Latitude_deg
    %     % longitude_deg               : get_MRZ_Longitude_deg
    %     % ellipsoidHeightReRefPoint_m : get_MRZ_EllipsoidHeightReRefPoint_m
    %
    %   %% 
    %   HeadingVessel_deg = get_MRZ_HeadingVessel_deg(a);
    %   figure; plot(HeadingVessel_deg); grid on; title('HeadingVessel_deg', 'Interpreter', 'None');
    %
    %   %% 
    %   SoundSpeedAtTxDepth_mPerSec = get_MRZ_SoundSpeedAtTxDepth_mPerSec(a);
    %   figure; plot(SoundSpeedAtTxDepth_mPerSec); grid on; title('SoundSpeedAtTxDepth_mPerSec', 'Interpreter', 'None');
    %
    %   %% 
    %   TxTransducerDepth_m = get_MRZ_TxTransducerDepth_m(a);
    %   figure; plot(TxTransducerDepth_m); grid on; title('TxTransducerDepth_m', 'Interpreter', 'None');
    %
    %   %% 
    %   Z_waterLevelReRefPoint_m = get_MRZ_Z_waterLevelReRefPoint_m(a);
    %   figure; plot(Z_waterLevelReRefPoint_m); grid on; title('Z_waterLevelReRefPoint_m', 'Interpreter', 'None');
    %
    %   %% 
    %   X_kmallToall_m = get_MRZ_X_kmallToall_m(a);
    %   figure; plot(X_kmallToall_m); grid on; title('X_kmallToall_m', 'Interpreter', 'None');
    %
    %   %% 
    %   Y_kmallToall_m = get_MRZ_Y_kmallToall_m(a);
    %   figure; plot(Y_kmallToall_m); grid on; title('Y_kmallToall_m', 'Interpreter', 'None');
    %
    %   %% 
    %   LatLongInfo = get_MRZ_LatLongInfo(a);
    %   figure; plot(LatLongInfo); grid on; title('LatLongInfo');
    %
    %   %% 
    %   PosSensorStatus = get_MRZ_PosSensorStatus(a);
    %   figure; plot(PosSensorStatus); grid on; title('PosSensorStatus');
    %
    %   %% 
    %   AttitudeSensorStatus = get_MRZ_AttitudeSensorStatus(a);
    %   figure; plot(AttitudeSensorStatus); grid on; title('AttitudeSensorStatus');
    %
    %   %% 
    %   Latitude_deg = get_MRZ_Latitude_deg(a);
    %   figure; plot(Latitude_deg); grid on; title('Latitude_deg', 'Interpreter', 'None');
    %
    %   %% 
    %   Longitude_deg = get_MRZ_Longitude_deg(a);
    %   figure; plot(Longitude_deg); grid on; title('Longitude_deg', 'Interpreter', 'None');
    %
    %   %% 
    %   EllipsoidHeightReRefPoint_m = get_MRZ_EllipsoidHeightReRefPoint_m(a);
    %   figure; plot(EllipsoidHeightReRefPoint_m); grid on; title('EllipsoidHeightReRefPoint_m', 'Interpreter', 'None');
    %
    %   %% 
    % %{
    %   a.sDataKmAll.dgmMRZ(1).PingInfo.dgmParams
    %     %   Variable name           -  getter method
    %     %     bsCorrectionOffset_dB : get_MRZ_BsCorrectionOffset_dB
    %     %     lambertsLawApplied    : get_MRZ_LambertsLawApplied
    %     %     iceWindow             : get_MRZ_IceWindow
    %     %     activeModes           : get_MRZ_ActiveModes
    %
    %   BsCorrectionOffset_dB = get_MRZ_BsCorrectionOffset_dB(a);
    %   figure; plot(BsCorrectionOffset_dB); grid on; title('BsCorrectionOffset_dB', 'Interpreter', 'None');
    %
    %   % TODO GLU : variables non trouvées
    %     %   LambertsLawApplied = get_MRZ_LambertsLawApplied(a);
    %     %   figure; plot(LambertsLawApplied); grid on; title('LambertsLawApplied');
    %     %
    %     %   IceWindow = get_MRZ_IceWindow(a);
    %     %   figure; plot(IceWindow); grid on; title('IceWindow');
    %     %
    %     %   ActiveModes = get_MRZ_ActiveModes(a);
    %     %   figure; plot(ActiveModes); grid on; title('ActiveModes');
    % %}
    %
    %
    %   %% 
    %   a.sDataKmAll.dgmMRZ(1).TxSectorInfo
    %     %   Variable name               -  getter method
    %     %     txSectorNumb              : get_MRZ_TxSectorNumb
    %     %     txArrNumber               : get_MRZ_TxArrNumber
    %     %     txSubArray                : get_MRZ_TxSubArray
    %     %     padding0                  :
    %     %     sectorTransmitDelay_sec   : get_MRZ_SectorTransmitDelay_sec
    %     %     tiltAngleReTx_deg         : get_MRZ_TiltAngleReTx_deg
    %     %     txNominalSourceLevel_dB   :
    %     %     txFocusRange_m            : get_MRZ_TxFocusRange_m
    %     %     centreFreq_Hz             : get_MRZ_CentreFreq_Hz
    %     %     signalBandWidth_Hz        : get_MRZ_TxBandwidth_Hz
    %     %     totalSignalLength_sec     : get_MRZ_TotalSignalLength_sec
    %     %     pulseShading              :
    %     %     signalWaveForm            : get_MRZ_SignalWaveForm
    %     %     padding1                  :
    %     %     highVoltageLevel_dB       :
    %     %     sectorTrackingCorr_dB     :
    %     %     effectiveSignalLength_sec : get_MRZ_EffectiveSignalLength_sec
    %
    %   %% 
    %   TxSectorNumb = get_MRZ_TxSectorNumb(a);
    %   figure; plot(TxSectorNumb); grid on; title('TxSectorNumb');
    %
    %   %% 
    %   TxArrNumber = get_MRZ_TxArrNumber(a);
    %   figure; plot(TxArrNumber); grid on; title('TxArrNumber');
    %
    %   %% 
    %   TxSubArray = get_MRZ_TxSubArray(a);
    %   figure; plot(TxSubArray); grid on; title('TxSubArray');
    %
    %   %% 
    %   SectorTransmitDelay_sec = get_MRZ_SectorTransmitDelay_sec(a);
    %   figure; plot(SectorTransmitDelay_sec); grid on; title('SectorTransmitDelay_sec', 'Interpreter', 'None');
    %
    %   %%
    %   TxFocusRange_m = get_MRZ_TxFocusRange_m(a);
    %   figure; plot(TxFocusRange_m); grid on; title('TxFocusRange_m', 'Interpreter', 'None');
    %
    %   %% 
    %   a.sDataKmAll.dgmMRZ(1).Body
    %     %   Variable name        -  getter method
    %     %     numBytesCmnPart    : get_MRZ_NumBytesCmnPart
    %     %     pingCnt            : get_MRZ_PingCnt
    %     %     rxFansPerPing      : get_MRZ_RxFansPerPing
    %     %     rxFanIndex         : get_MRZ_RxFanIndex
    %     %     swathsPerPing      : get_MRZ_SwathsPerPing
    %     %     swathAlongPosition : get_MRZ_SwathAlongPosition
    %     %     txTransducerInd    : get_MRZ_TxTransducerInd
    %     %     rxTransducerInd    : get_MRZ_RxTransducerInd
    %     %     numRxTransducers   : get_MRZ_NumRxTransducers
    %     %     algorithmType      : get_MRZ_AlgorithmType
    %
    %   %% 
    %   NumBytesCmnPart = get_MRZ_NumBytesCmnPart(a);
    %   figure; plot(NumBytesCmnPart); grid on; title('NumBytesCmnPart');
    %
    %   %% 
    %   PingCnt = get_MRZ_PingCnt(a);
    %   figure; plot(PingCnt); grid on; title('PingCnt');
    %
    %   %% 
    %   RxFansPerPing = get_MRZ_RxFansPerPing(a);
    %   figure; plot(RxFansPerPing); grid on; title('RxFansPerPing');
    %
    %   %% 
    %   RxFanIndex = get_MRZ_RxFanIndex(a);
    %   figure; plot(RxFanIndex); grid on; title('RxFanIndex');
    %
    %   %% 
    %   SwathsPerPing = get_MRZ_SwathsPerPing(a);
    %   figure; plot(SwathsPerPing); grid on; title('SwathsPerPing');
    %
    %   %% 
    %   SwathAlongPosition = get_MRZ_SwathAlongPosition(a);
    %   figure; plot(SwathAlongPosition); grid on; title('SwathAlongPosition');
    %
    %   %% 
    %   TxTransducerInd = get_MRZ_TxTransducerInd(a);
    %   figure; plot(TxTransducerInd); grid on; title('TxTransducerInd');
    %
    %   %% 
    %   RxTransducerInd = get_MRZ_RxTransducerInd(a);
    %   figure; plot(TxTransducerInd); grid on; title('RxTransducerInd');
    %
    %   %% 
    %   NumRxTransducers = get_MRZ_NumRxTransducers(a);
    %   figure; plot(NumRxTransducers); grid on; title('NumRxTransducers');
    %
    %   %% 
    %   AlgorithmType = get_MRZ_AlgorithmType(a);
    %   figure; plot(AlgorithmType); grid on; title('AlgorithmType');
    %
    %
    %   %% 
    %   a.sDataKmAll.dgmMRZ(1).Partition
    %     %   Variable name -  getter method
    %     %     numOfDgms   : get_MRZ_NumOfDgms
    %     %     dgmNum      : get_MRZ_DgmNum
    %
    %   %% 
    %   NumOfDgms = get_MRZ_NumOfDgms(a);
    %   figure; plot(NumOfDgms); grid on; title('NumOfDgms');
    %
    %   %% 
    %   DgmNum = get_MRZ_DgmNum(a);
    %   figure; plot(DgmNum); grid on; title('DgmNum');
    %
    %
    %
    %   %% 
    %  Samples = a.sDataKmAll.dgmMRZ(1).Samples;
    %  figure; plot(Samples); grid on;
    %  [Y, Y2] = get_MRZ_Samples(a);
    %  figure; imagesc(Y2); axis xy; colormap(gray(256)); colorbar; title('Y2');
    %
    %
    %
    %   %% Exploration of IIP datagrams
    % a.sDataKmAll.dgmIIP
    %     %   Variable name -  getter method
    %     %     info        : 0
    %     %     status      : 0
    %     %     install_txt : get_IIP_Install_txt
    %     %     revFormat   : get_IIP_RevFormat
    %
    %   %% 
    %  IIP_Datetime = get_IIP_Datetime(a)
    %  RevFormat   = get_IIP_RevFormat(a)
    %  Install_txt = get_IIP_Install_txt(a)
    %
    %
    %
    %   %% Exploration of IOP datagrams
    % a.sDataKmAll.dgmIOP
    %     %   Variable name -  getter method
    %     %     numBytesDgm : 1019
    %     %     info        : 0
    %     %     status      : 0
    %     %     install_txt : get_IOP_Install_txt
    %     %     revFormat   : get_IOP_getRevFormat
    %
    %   %% 
    %  RevFormat   = get_IOP_getRevFormat(a)
    %  Install_txt = get_IOP_Install_txt(a)
    %
    %
    %
    %   %% Exploration of SVP datagrams
    % a.sDataKmAll.dgmSVP
    %     %   Variable name -  getter method
    %     %     numBytesCmnPart : 28
    %     %     numSamples      : get_SVP_NumSamples
    %     %     sensorFormat    : get_SVP_SensorFormat
    %     %     time_sec        : 0
    %     %     latitude        : 200
    %     %     longitude       : 200
    %     %     profil          : [1×237 struct]
    %
    %   %% 
    %  SVP_NumSamples   = get_SVP_NumSamples(a)
    %  SVP_SensorFormat = get_SVP_SensorFormat(a)
    %
    %
    %
    %   %% 
    % a.sDataKmAll.dgmSVP.profil(1)
    %     %   Variable name           -  getter method
    %     %     depth_m               : get_SVP_Depth
    %     %     soundVelocity_mPerSec : get_SVP_Celerity
    %     %     absCoeff_dBPerkm      : get_SVP_Absorption
    %     %     temp_C                : get_SVP_Temp
    %     %     salinity              : get_SVP_Salinity
    %
    %   %% 
    %  SVP_Depth    = get_SVP_Depth(a)
    %  SVP_Celerity = get_SVP_Celerity(a)
    %  figure; plot(SVP_Celerity, SVP_Depth, '.-'); grid on;
    %
    %   %% 
    %  SVP_Temp       = get_SVP_Temp(a)
    %  SVP_Salinity   = get_SVP_Salinity(a)
    %  SVP_Absorption = get_SVP_Absorption(a)
    %
    %
    %
    %   %% Exploration of SVP datagrams
    % a.sDataKmAll.dgmSKM % [1×2886 struct]  1×1036 struct array with fields:
    %     %     SKMInfo
    %     %     SKMSamples
    %
    %   %% 
    % a.sDataKmAll.dgmSKM(1).SKMInfo
    %     %   Variable name       -  Signification
    %     %     numBytesInfoPart  :
    %     %     sensorSystem      : Attitude system number, as numbered in installation parameters. E.g. system 0 referes to system ATTI_1 in installation datagram #IIP.
    %     %     sensorStatus      :
    %     %       Bit 0 : 0 = Data OK, 1 = Data OK and sensor is chosen as active
    %     %       Bit 1 : 0
    %     %       Bit 2 : 0 = Data OK, 1 = Reduced performance
    %     %       Bit 3 : 0
    %     %       Bit 4 : 0 = Data OK, 1 = Invalid data
    %     %       Bit 5 : 0
    %     %       Bit 6 : 0 = Velocity from sensor, 1 = Velocity calculated by PU
    %     %     sensorInputFormat : Format of raw data from input sensor, given in numerical code according to table below
    %     %       1 : KM binary Sensor Input
    %     %       2 : EM 3000 data
    %     %       3 : Sagem
    %     %       4 : Seapath binary 11
    %     %       5 : Seapath binary 23
    %     %       6 : Seapath binary 26
    %     %       7 : POS M/V GRP 102/103
    %     %       8 : Coda Octopus MCOM
    %     %     numSamplesArray   : Number of KM binary sensor samples added in this datagram.
    %     %     numBytesPerSample : Length in bytes of one whole KM binary sensor sample.
    %     %     padding           :
    %
    %   %% 
    % a.sDataKmAll.dgmSKM(1).SKMInfo
    %     %   Variable name       -  getter method
    %     %     numBytesInfoPart  : 12
    %     %     sensorSystem      : get_SKM_SensorSystem
    %     %     sensorStatus      : get_SKM_SensorStatus
    %     %     sensorInputFormat : get_SKM_SensorInputFormat
    %     %     numSamplesArray   : get_SKM_NumSamplesArray
    %     %     numBytesPerSample : 132
    %     %     padding           : 127
    %
    %   %% 
    %   % Select the sensor data that was used in real time
    %   subSensorStatus = get_SKM_selectionSensorStatus(a, -1)
    %
    %   %% 
    %   SKM_SensorSystem = get_SKM_SensorSystem(a, 'sub', subSensorStatus);
    %   figure; plot(SKM_SensorSystem); grid on; title('SKM_SensorSystem', 'Interpreter', 'None');
    %
    %   %% 
    %   SKM_SensorStatus = get_SKM_SensorStatus(a, 'sub', subSensorStatus);
    %   figure; plot(SKM_SensorStatus); grid on; title('SKM_SensorStatus', 'Interpreter', 'None');
    %
    %   %% 
    %   [SKM_SensorSystem(1:10) SKM_SensorStatus(1:10)]
    %
    %   %% 
    %   SKM_NumSamplesArray = get_SKM_NumSamplesArray(a, 'sub', subSensorStatus);
    %   figure; plot(SKM_NumSamplesArray); grid on; title('SKM_NumSamplesArray', 'Interpreter', 'None');
    %
    %   SKM_SensorInputFormat = get_SKM_SensorInputFormat(a, 'sub', subSensorStatus);
    %   figure; plot(SKM_SensorInputFormat); grid on; title('SKM_SensorInputFormat', 'Interpreter', 'None');
    %
    %
    %
    %   %% 
    % a.sDataKmAll(1).dgmSKM(1).SKMSamples % 1×102 struct array with fields:
    %     %     KMBinary
    %     %     KMdelayedHeave
    %
    %   %% 
    % a.sDataKmAll.dgmSKM(1).SKMSamples(1).KMBinary
    %     %   Variable name -  getter method
    %     %     dgmType      : '#KMB'
    %     %     numBytesDgm  : 132
    %     %     dgmVersion   : 1
    %     %     time_sec     : get_SKM_Datetime
    %     %     time_nanosec : ----------------
    %     %     status       : get_SKM_Status
    %     %     Position     : [1×1 struct]
    %     %     Attitude     : [1×1 struct]
    %     %     Rates        : [1×1 struct]
    %     %     Vel          : [1×1 struct]
    %     %     Err          : [1×1 struct]
    %     %     Acc          : [1×1 struct]
    %
    %   %% 
    %   SKM_Datetime = get_SKM_Datetime(a, 'sub', subSensorStatus)
    %
    %   %% 
    %   SKM_Status = get_SKM_Status(a, 'sub', subSensorStatus);
    %   figure; plot(SKM_Status); grid on; title('SKM_Status', 'Interpreter', 'None');
    %
    %
    %
    %   %% 
    % a.sDataKmAll.dgmSKM(1).SKMSamples(1).KMBinary.Position
    %     %   Variable name       -  getter method
    %     %     latitude_deg      : get_SKM_Latitude_deg
    %     %     longitude_deg     : get_SKM_Longitude_deg
    %     %     ellipsoidHeight_m : get_SKM_EllipsoidHeight_m
    %
    %   %% 
    %   SKM_Latitude_deg = get_SKM_Latitude_deg(a, 'sub', subSensorStatus);
    %   figure; plot(SKM_Latitude_deg); grid on; title('SKM_Latitude_deg', 'Interpreter', 'None');
    %
    %   %% 
    %   SKM_Longitude_deg = get_SKM_Longitude_deg(a, 'sub', subSensorStatus);
    %   figure; plot(SKM_Longitude_deg); grid on; title('SKM_Longitude_deg', 'Interpreter', 'None');
    %
    %   %% 
    %   SKM_EllipsoidHeight_m = get_SKM_EllipsoidHeight_m(a, 'sub', subSensorStatus);
    %   figure; plot(SKM_EllipsoidHeight_m); grid on; title('SKM_EllipsoidHeight_m', 'Interpreter', 'None');
    %
    %
    %
    %   %% 
    % a.sDataKmAll.dgmSKM(1).SKMSamples(1).KMBinary.Attitude
    %     %   Variable name -  getter method
    %     %     roll_deg    : get_SKM_Roll_deg
    %     %     pitch_deg   : get_SKM_Pitch_deg
    %     %     heading_deg : get_SKM_Heading_deg
    %     %     heave_m     : get_SKM_Heave_m
    %
    %   %% 
    %   SKM_Roll_deg = get_SKM_Roll_deg(a, 'sub', subSensorStatus);
    %   figure; plot(SKM_Roll_deg); grid on; title('SKM_Roll_deg', 'Interpreter', 'None');
    %
    %   %% 
    %   SKM_Pitch_deg = get_SKM_Pitch_deg(a, 'sub', subSensorStatus);
    %   figure; plot(SKM_Pitch_deg); grid on; title('SKM_Pitch_deg', 'Interpreter', 'None');
    %
    %   %% 
    %   SKM_Heading_deg = get_SKM_Heading_deg(a, 'sub', subSensorStatus);
    %   figure; plot(SKM_Heading_deg); grid on; title('SKM_Heading_deg', 'Interpreter', 'None');
    %
    %   %% 
    %   SKM_Heave_m = get_SKM_Heave_m(a, 'sub', subSensorStatus);
    %   figure; plot(SKM_Heave_m); grid on; title('SKM_Heave_m', 'Interpreter', 'None');
    %
    %
    %
    %   %% 
    % a.sDataKmAll.dgmSKM(1).SKMSamples(1).KMBinary.Rates
    %     %   Variable name -  getter method
    %     %     rollRate    : get_SKM_RollRate
    %     %     pitchRate   : get_SKM_PitchRate
    %     %     yawRate     : get_SKM_YawRate
    %
    %   %% 
    %   SKM_RollRate = get_SKM_RollRate(a, 'sub', subSensorStatus);
    %   figure; plot(SKM_RollRate); grid on; title('SKM_RollRate', 'Interpreter', 'None');
    %
    %   %% 
    %   SKM_PitchRate = get_SKM_PitchRate(a, 'sub', subSensorStatus);
    %   figure; plot(SKM_PitchRate); grid on; title('SKM_PitchRate', 'Interpreter', 'None');
    %
    %   %% 
    %   SKM_YawRate = get_SKM_YawRate(a, 'sub', subSensorStatus);
    %   figure; plot(SKM_YawRate); grid on; title('SKM_YawRate', 'Interpreter', 'None');
    %
    %
    %
    %   %% 
    % a.sDataKmAll.dgmSKM(1).SKMSamples(1).KMBinary.Vel
    %     %   Variable name -  getter method
    %     %     velNorth    : get_SKM_VelNorth
    %     %     velEast     : get_SKM_VelEast
    %     %     velDown     : get_SKM_VelDown
    %
    %   %% 
    %   SKM_VelNorth = get_SKM_VelNorth(a, 'sub', subSensorStatus);
    %   figure; plot(SKM_VelNorth); grid on; title('SKM_VelNorth', 'Interpreter', 'None');
    %
    %   %% 
    %   SKM_VelEast = get_SKM_VelEast(a, 'sub', subSensorStatus);
    %   figure; plot(SKM_VelEast); grid on; title('SKM_VelEast', 'Interpreter', 'None');
    %
    %   %% 
    %   SKM_VelDown = get_SKM_VelDown(a, 'sub', subSensorStatus);
    %   figure; plot(SKM_VelDown); grid on; title('SKM_VelDown', 'Interpreter', 'None');
    %
    %
    %
    %   %% 
    % a.sDataKmAll.dgmSKM(1).SKMSamples(1).KMBinary.Err
    %     %   Variable name            -  getter method
    %     %     latitudeError_m        : get_SKM_LatitudeError_m
    %     %     ellipsoidHeightError_m : get_SKM_EllipsoidHeightError_m
    %     %     rollError_deg          : get_SKM_RollError_deg
    %     %     pitchError_deg         : get_SKM_PitchError_deg
    %     %     headingError_deg       : get_SKM_HeadingError_deg
    %     %     heaveError_m           : get_SKM_HeaveError_m
    %
    %   %% 
    %   SKM_LatitudeError_m = get_SKM_LatitudeError_m(a, 'sub', subSensorStatus);
    %   figure; plot(SKM_LatitudeError_m); grid on; title('SKM_LatitudeError_m', 'Interpreter', 'None');
    %
    %   %% 
    %   SKM_EllipsoidHeightError_m = get_SKM_EllipsoidHeightError_m(a, 'sub', subSensorStatus);
    %   figure; plot(SKM_EllipsoidHeightError_m); grid on; title('SKM_EllipsoidHeightError_m', 'Interpreter', 'None');
    %
    %   %% 
    %   SKM_RollError_deg = get_SKM_RollError_deg(a, 'sub', subSensorStatus);
    %   figure; plot(SKM_RollError_deg); grid on; title('SKM_RollError_deg', 'Interpreter', 'None');
    %
    %   %% 
    %   SKM_PitchError_deg = get_SKM_PitchError_deg(a, 'sub', subSensorStatus);
    %   figure; plot(SKM_PitchError_deg); grid on; title('SKM_PitchError_deg', 'Interpreter', 'None');
    %
    %   %% 
    %   SKM_HeadingError_deg = get_SKM_HeadingError_deg(a, 'sub', subSensorStatus);
    %   figure; plot(SKM_HeadingError_deg); grid on; title('SKM_HeadingError_deg', 'Interpreter', 'None');
    %
    %   %% 
    %   SKM_HeaveError_m = get_SKM_HeaveError_m(a, 'sub', subSensorStatus);
    %   figure; plot(SKM_HeaveError_m); grid on; title('SKM_HeaveError_m', 'Interpreter', 'None');
    %
    %
    %
    %   %% 
    % a.sDataKmAll.dgmSKM(1).SKMSamples(1).KMBinary.Acc
    %     %   Variable name       -  getter method
    %     %     northAcceleration : get_SKM_NorthAcceleration
    %     %     eastAcceleration  : get_SKM_EastAcceleration
    %     %     downAcceleration  : get_SKM_DownAcceleration
    %
    %   %% 
    %   SKM_NorthAcceleration = get_SKM_NorthAcceleration(a, 'sub', subSensorStatus);
    %   figure; plot(SKM_NorthAcceleration); grid on; title('SKM_NorthAcceleration', 'Interpreter', 'None');
    %
    %   %% 
    %   SKM_EastAcceleration = get_SKM_EastAcceleration(a, 'sub', subSensorStatus);
    %   figure; plot(SKM_EastAcceleration); grid on; title('SKM_EastAcceleration', 'Interpreter', 'None');
    %
    %   %% 
    %   SKM_DownAcceleration = get_SKM_DownAcceleration(a, 'sub', subSensorStatus);
    %   figure; plot(SKM_DownAcceleration); grid on; title('SKM_DownAcceleration', 'Interpreter', 'None');
    %
    %
    %
    %   %% 
    % a.sDataKmAll.dgmSKM(1).SKMSamples(1).KMdelayedHeave
    %     %   Variable name    -  getter method
    %     %     time_sec       : get_SKM_DelayedHeave_Datetime
    %     %     time_nanosec   : ----------------
    %     %     delayedHeave_m : get_SKM_DelayedHeave_m
    %
    %   %% 
    %   SKM_DelayedHeave_Datetime = get_SKM_DelayedHeave_Datetime(a, 'sub', subSensorStatus)
    %
    %   %% 
    %   SKM_DelayedHeave_m = get_SKM_DelayedHeave_m(a, 'sub', subSensorStatus);
    %   figure; plot(SKM_DelayedHeave_m); grid on; title('SKM_DelayedHeave_m', 'Interpreter', 'None');
    %
    %
    %
    %
    %
    %   %% Exploration of SCL datagrams
    % a.sDataKmAll.dgmSCL % 1×528 struct array with fields:
    %     %     Common     : [1×1 struct]
    %     %     DataSensor : [1×1 struct]
    %
    %   %% 
    % a.sDataKmAll.dgmSCL(1).Common
    %     %   Variable name     -  getter method
    %     %     numBytesCmnPart : 8
    %     %     sensorSystem    : 5
    %     %     sensorStatus    : get_SCL_sensorStatus
    %     %     padding         : 3135
    %
    %   %% 
    %   SCL_sensorStatus = get_SCL_sensorStatus(a);
    %   figure; plot(SCL_sensorStatus); grid on; title('SCL_sensorStatus', 'Interpreter', 'None');
    %
    %
    %   %% 
    % a.sDataKmAll.dgmSCL(1).DataSensor
    %     %   Variable name        -  getter method
    %     %     offset_sec         : 0
    %     %     clockdevPU_nanosec : 2365384
    %     %     dataFromSensor     : get_SCL_DataFromSensor
    %
    %   %% 
    %   SCL_Datetime          = get_SCL_Datetime(a)
    %   SCL_Duration          = get_SCL_Duration(a)
    %   SCL_PosDataFromSensor = get_SCL_DataFromSensor(a)
    %
    %
    %
    %   %% Exploration of SPO datagrams
    % a.sDataKmAll.dgmSPO % 1×528 struct array with fields:
    %     %     Common     : [1×1 struct]
    %     %     DataSensor : [1×1 struct]
    %
    %
    %   %% 
    % a.sDataKmAll.dgmSPO(1).Common
    %     %   Variable name     -  getter method
    %     %     numBytesCmnPart : 130
    %     %     sensorSystem    : 0
    %     %     sensorStatus    : get_SPO_sensorStatus
    %     %     padding         : 2437
    %
    %   %% 
    %   SPO_sensorStatus = get_SPO_sensorStatus(a);
    %   figure; plot(SPO_sensorStatus); grid on; title('SPO_sensorStatus', 'Interpreter', 'None');
    %
    %
    %   %% 
    % a.sDataKmAll.dgmSPO(1).DataSensor
    %     %   Variable name                 -  getter method
    %     %     timeFromSensor_sec          : get_SPO_Datetime
    %     %     timeFromSensor_nanosec      : ----------------
    %     %     posFixQuality_m             : get_SPO_PosFixQuality_m
    %     %     correctedLat_deg            : get_SPO_CorrectedLat_deg
    %     %     correctedLon_deg            : get_SPO_CorrectedLon_deg
    %     %     speedOverGround_mPerSec     : get_SPO_SpeedOverGround_mPerSec
    %     %     courseOverGround_deg        : get_SPO_CourseOverGround_deg
    %     %     ellipsoidHeightReRefPoint_m : get_SPO_EllipsoidHeightReRefPoint_m
    %     %     posDataFromSensor           : get_SPO_PosDataFromSensor
    %
    %   %% 
    %   SPO_Datetime = get_SPO_Datetime(a)
    %
    %   %% 
    %   SPO_PosFixQuality_m = get_SPO_PosFixQuality_m(a);
    %   figure; plot(SPO_PosFixQuality_m); grid on; title('SPO_PosFixQuality_m', 'Interpreter', 'None');
    %
    %   %% 
    %   SPO_CorrectedLat_deg = get_SPO_CorrectedLat_deg(a);
    %   figure; plot(SPO_CorrectedLat_deg); grid on; title('SPO_CorrectedLat_deg', 'Interpreter', 'None');
    %
    %   %% 
    %   SPO_CorrectedLon_deg = get_SPO_CorrectedLon_deg(a);
    %   figure; plot(SPO_CorrectedLon_deg); grid on; title('SPO_CorrectedLon_deg', 'Interpreter', 'None');
    %   figure; plot(SPO_CorrectedLon_deg, SPO_CorrectedLat_deg, '.-'); grid on; title('SPO_CorrectedNav', 'Interpreter', 'None');
    %
    %   %% 
    %   SPO_SpeedOverGround_mPerSec = get_SPO_SpeedOverGround_mPerSec(a);
    %   figure; plot(SPO_SpeedOverGround_mPerSec); grid on; title('SPO_SpeedOverGround_mPerSec', 'Interpreter', 'None');
    %
    %   %% 
    %   SPO_CourseOverGround_deg = get_SPO_CourseOverGround_deg(a);
    %   figure; plot(SPO_CourseOverGround_deg); grid on; title('SPO_CourseOverGround_deg', 'Interpreter', 'None');
    %
    %   %% 
    %   SPO_EllipsoidHeightReRefPoint_m = get_SPO_EllipsoidHeightReRefPoint_m(a);
    %   figure; plot(SPO_EllipsoidHeightReRefPoint_m); grid on; title('SPO_EllipsoidHeightReRefPoint_m', 'Interpreter', 'None');
    %
    %   %% 
    %   SPO_PosDataFromSensor = get_SPO_PosDataFromSensor(a)
    %
    %
    %
    %   %% Exploration of SCO datagrams
    % a.sDataKmAll.dgmCPO % structure similaire à a.sDataKmAll.dgmSPO
    %     %     Common     : [1×1 struct]
    %     %     DataSensor : [1×1 struct]
    %
    %
    %   %% 
    % a.sDataKmAll.dgmCPO(1).Common
    %     %   Variable name     -  getter method
    %     %     numBytesCmnPart : 130
    %     %     sensorSystem    : 0
    %     %     sensorStatus    : get_CPO_sensorStatus
    %     %     padding         : 16388
    %
    %   %% 
    %   CPO_sensorStatus = get_CPO_sensorStatus(a);
    %   figure; plot(CPO_sensorStatus); grid on; title('CPO_sensorStatus', 'Interpreter', 'None');
    %
    %
    %   %% 
    % a.sDataKmAll.dgmCPO(1).DataSensor
    %     %   Variable name                 -  getter method
    %     %     timeFromSensor_sec          : get_CPO_Datetime
    %     %     timeFromSensor_nanosec      : ----------------
    %     %     posFixQuality_m             : get_CPO_PosFixQuality_m
    %     %     correctedLat_deg            : get_CPO_CorrectedLat_deg
    %     %     correctedLon_deg            : get_CPO_CorrectedLon_deg
    %     %     speedOverGround_mPerSec     : get_CPO_SpeedOverGround_mPerSec
    %     %     courseOverGround_deg        : get_CPO_CourseOverGround_deg
    %     %     ellipsoidHeightReRefPoint_m : get_CPO_EllipsoidHeightReRefPoint_m
    %     %     posDataFromSensor           : get_CPO_PosDataFromSensor
    %
    %   %% 
    %   CPO_Datetime = get_CPO_Datetime(a)
    %
    %   %% 
    %   CPO_PosFixQuality_m = get_CPO_PosFixQuality_m(a);
    %   figure; plot(CPO_PosFixQuality_m); grid on; title('CPO_PosFixQuality_m', 'Interpreter', 'None');
    %
    %   %% 
    %   CPO_CorrectedLat_deg = get_CPO_CorrectedLat_deg(a);
    %   figure; plot(CPO_CorrectedLat_deg); grid on; title('CPO_CorrectedLat_deg', 'Interpreter', 'None');
    %
    %   %% 
    %   CPO_CorrectedLon_deg = get_CPO_CorrectedLon_deg(a);
    %   figure; plot(CPO_CorrectedLon_deg); grid on; title('CPO_CorrectedLon_deg', 'Interpreter', 'None');
    %
    %   %% 
    %   CPO_SpeedOverGround_mPerSec = get_CPO_SpeedOverGround_mPerSec(a);
    %   figure; plot(CPO_SpeedOverGround_mPerSec); grid on; title('CPO_SpeedOverGround_mPerSec', 'Interpreter', 'None');
    %
    %   %% 
    %   CPO_CourseOverGround_deg = get_CPO_CourseOverGround_deg(a);
    %   figure; plot(CPO_CourseOverGround_deg); grid on; title('CPO_CourseOverGround_deg', 'Interpreter', 'None');
    %
    %   %% 
    %   CPO_EllipsoidHeightReRefPoint_m = get_CPO_EllipsoidHeightReRefPoint_m(a);
    %   figure; plot(CPO_EllipsoidHeightReRefPoint_m); grid on; title('CPO_EllipsoidHeightReRefPoint_m', 'Interpreter', 'None');
    %
    %   %% 
    %   CPO_PosDataFromSensor = get_CPO_PosDataFromSensor(a)
    %
    %
    %
    %   %% Exploration of CHE datagrams
    % b.sDataKmAll.dgmCHE % 1×84 struct array with fields:
    %     %     Body   : [1×1 struct]
    %     %     dgmCHE : [1×1 struct]
    %
    %   %% 
    % b.sDataKmAll.dgmCHE(1).Body
    %     %   Variable name        -  getter method
    %     %     numBytesCmnPart    : get_MRZ_NumBytesCmnPart(   b, 'dgmName', 'CHE')
    %     %     pingCnt            : get_MRZ_PingCnt(           b, 'dgmName', 'CHE')
    %     %     rxFansPerPing      : get_MRZ_RxFansPerPing(     b, 'dgmName', 'CHE')
    %     %     rxFanIndex         : get_MRZ_RxFanIndex(        b, 'dgmName', 'CHE')
    %     %     swathsPerPing      : get_MRZ_SwathsPerPing(     b, 'dgmName', 'CHE')
    %     %     swathAlongPosition : get_MRZ_SwathAlongPosition(b, 'dgmName', 'CHE');
    %     %     txTransducerInd    : get_MRZ_TxTransducerInd(   b, 'dgmName', 'CHE')
    %     %     rxTransducerInd    : get_MRZ_RxTransducerInd(   b, 'dgmName', 'CHE')
    %     %     numRxTransducers   : get_MRZ_NumRxTransducers(  b, 'dgmName', 'CHE')
    %     %     algorithmType      : get_MRZ_AlgorithmType(     b, 'dgmName', 'CHE')
    %
    %   %% 
    %   CHE_NumBytesCmnPart = get_MRZ_NumBytesCmnPart(b, 'dgmName', 'CHE');
    %   figure; plot(CHE_NumBytesCmnPart); grid on; title('CHE_NumBytesCmnPart', 'Interpreter', 'None');
    %
    %   %% 
    %   CHE_PingCnt = get_MRZ_PingCnt(b, 'dgmName', 'MWC');
    %   figure; plot(CHE_PingCnt); grid on; title('CHE_PingCnt', 'Interpreter', 'None');
    %
    %   %% 
    %   CHE_RxFansPerPing = get_MRZ_RxFansPerPing(b, 'dgmName', 'MWC');
    %   figure; plot(CHE_RxFansPerPing); grid on; title('CHE_RxFansPerPing', 'Interpreter', 'None');
    %
    %   %% 
    %   CHE_RxFanIndex = get_MRZ_RxFanIndex(b, 'dgmName', 'MWC');
    %   figure; plot(CHE_RxFanIndex); grid on; title('CHE_RxFanIndex', 'Interpreter', 'None');
    %
    %   %% 
    %   CHE_SwathsPerPing = get_MRZ_SwathsPerPing(b, 'dgmName', 'MWC');
    %   figure; plot(CHE_SwathsPerPing); grid on; title('CHE_SwathsPerPing', 'Interpreter', 'None');
    %
    %   %% 
    %   CHE_SwathAlongPosition = get_MRZ_SwathAlongPosition(b, 'dgmName', 'MWC');
    %   figure; plot(CHE_SwathAlongPosition); grid on; title('CHE_SwathAlongPosition', 'Interpreter', 'None');
    %
    %   %% 
    %   CHE_TxTransducerInd = get_MRZ_TxTransducerInd(b, 'dgmName', 'MWC');
    %   figure; plot(CHE_TxTransducerInd); grid on; title('CHE_TxTransducerInd', 'Interpreter', 'None');
    %
    %   %% 
    %   CHE_RxTransducerInd = get_MRZ_RxTransducerInd(b, 'dgmName', 'MWC');
    %   figure; plot(CHE_RxTransducerInd); grid on; title('CHE_RxTransducerInd', 'Interpreter', 'None');
    %
    %   %% 
    %   CHE_NumRxTransducers = get_MRZ_NumRxTransducers(b, 'dgmName', 'MWC');
    %   figure; plot(CHE_NumRxTransducers); grid on; title('CHE_NumRxTransducers', 'Interpreter', 'None');
    %
    %   %% 
    %   CHE_AlgorithmType = get_MRZ_AlgorithmType(b, 'dgmName', 'MWC');
    %   figure; plot(CHE_AlgorithmType); grid on; title('CHE_AlgorithmType', 'Interpreter', 'None');
    %
    %
    %   %% 
    %  b.sDataKmAll.dgmCHE(1).dgmCHE
    %     %   Variable name -  getter method
    %     %     heave_m     : get_CHE_Heave_m
    %
    %   %% 
    %   CHE_Heave_m = get_CHE_Heave_m(b);
    %   figure; plot(CHE_Heave_m); grid on; title('CHE_Heave_m', 'Interpreter', 'None');
    %
    %
    %
    %   %% Exploration of MWC datagrams
    % b.sDataKmAll.dgmMWC % 1×84 struct array with fields:
    %     %     Partition    : [1×1 struct]
    %     %     Body         : [1×1 struct]
    %     %     TxInfo       : [1×1 struct]
    %     %     TxSectorData : [1×1 struct]
    %     %     RxInfo       : [1×1 struct]
    %     %     RxBeamData   : [1×1 struct]
    %
    %   %% 
    % b.sDataKmAll.dgmMWC(1).Partition
    %     %   Variable name -  getter method
    %     %     numOfDgms   : get_MRZ_NumOfDgms(b, 'dgmName', 'MWC')
    %     %     dgmNum      : get_MRZ_DgmNum(   b, 'dgmName', 'MWC')
    %
    %
    %  %%
    %     MWC_Datetime = get_MWC_Datetime(a)
    %
    %   %% 
    %   MWC_NumOfDgms = get_MRZ_NumOfDgms(b, 'dgmName', 'MWC');
    %   figure; plot(MWC_NumOfDgms); grid on; title('MWC_NumOfDgms', 'Interpreter', 'None');
    %
    %   %% 
    %   MWC_DgmNum = get_MRZ_DgmNum(b, 'dgmName', 'MWC');
    %   figure; plot(MWC_DgmNum); grid on; title('MWC_DgmNum', 'Interpreter', 'None');
    %
    %
    %   %% 
    % b.sDataKmAll.dgmMWC(1).Body
    %     %   Variable name        -  getter method
    %     %     numBytesCmnPart    : get_MRZ_NumBytesCmnPart(   b, 'dgmName', 'MWC')
    %     %     pingCnt            : get_MRZ_PingCnt(           b, 'dgmName', 'MWC')
    %     %     rxFansPerPing      : get_MRZ_RxFansPerPing(     b, 'dgmName', 'MWC')
    %     %     rxFanIndex         : get_MRZ_RxFanIndex(        b, 'dgmName', 'MWC')
    %     %     swathsPerPing      : get_MRZ_SwathsPerPing(     b, 'dgmName', 'MWC')
    %     %     swathAlongPosition : get_MRZ_SwathAlongPosition(b, 'dgmName', 'MWC');
    %     %     txTransducerInd    : get_MRZ_TxTransducerInd(   b, 'dgmName', 'MWC')
    %     %     rxTransducerInd    : get_MRZ_RxTransducerInd(   b, 'dgmName', 'MWC')
    %     %     numRxTransducers   : get_MRZ_NumRxTransducers(  b, 'dgmName', 'MWC')
    %     %     algorithmType      : get_MRZ_AlgorithmType(     b, 'dgmName', 'MWC')
    %
    %   %% 
    %   MWC_NumBytesCmnPart = get_MRZ_NumBytesCmnPart(b, 'dgmName', 'MWC');
    %   figure; plot(MWC_NumBytesCmnPart); grid on; title('MWC_NumBytesCmnPart', 'Interpreter', 'None');
    %
    %   %% 
    %   MWC_PingCnt = get_MRZ_PingCnt(b, 'dgmName', 'MWC');
    %   figure; plot(MWC_PingCnt); grid on; title('MWC_PingCnt', 'Interpreter', 'None');
    %
    %   %% 
    %   MWC_RxFansPerPing = get_MRZ_RxFansPerPing(b, 'dgmName', 'MWC');
    %   figure; plot(MWC_RxFansPerPing); grid on; title('MWC_RxFansPerPing', 'Interpreter', 'None');
    %
    %   %% 
    %   MWC_RxFanIndex = get_MRZ_RxFanIndex(b, 'dgmName', 'MWC');
    %   figure; plot(MWC_RxFanIndex); grid on; title('MWC_RxFanIndex', 'Interpreter', 'None');
    %
    %   %% 
    %   MWC_SwathsPerPing = get_MRZ_SwathsPerPing(b, 'dgmName', 'MWC');
    %   figure; plot(MWC_SwathsPerPing); grid on; title('MWC_SwathsPerPing', 'Interpreter', 'None');
    %
    %   %% 
    %   MWC_SwathAlongPosition = get_MRZ_SwathAlongPosition(b, 'dgmName', 'MWC');
    %   figure; plot(MWC_SwathAlongPosition); grid on; title('MWC_SwathAlongPosition', 'Interpreter', 'None');
    %
    %   %% 
    %   MWC_TxTransducerInd = get_MRZ_TxTransducerInd(b, 'dgmName', 'MWC');
    %   figure; plot(MWC_TxTransducerInd); grid on; title('MWC_TxTransducerInd', 'Interpreter', 'None');
    %
    %   %% 
    %   MWC_RxTransducerInd = get_MRZ_RxTransducerInd(b, 'dgmName', 'MWC');
    %   figure; plot(MWC_RxTransducerInd); grid on; title('MWC_RxTransducerInd', 'Interpreter', 'None');
    %
    %   %% 
    %   MWC_NumRxTransducers = get_MRZ_NumRxTransducers(b, 'dgmName', 'MWC');
    %   figure; plot(MWC_NumRxTransducers); grid on; title('MWC_NumRxTransducers', 'Interpreter', 'None');
    %
    %   %% 
    %   MWC_AlgorithmType = get_MRZ_AlgorithmType(b, 'dgmName', 'MWC');
    %   figure; plot(MWC_AlgorithmType); grid on; title('MWC_AlgorithmType', 'Interpreter', 'None');
    %
    %
    %   %% 
    % b.sDataKmAll.dgmMWC(1).RxInfo
    %     %   Variable name            -  getter method
    %     %     numBytesRxInfo         : 16
    %     %     numBeams               : get_MWC_NumBeams
    %     %     numBytesPerBeamEntry   : 12
    %     %     phaseFlag              : get_MWC_PhaseFlag
    %     %     TVGfunctionApplied     : get_MWC_TVGfunctionApplied
    %     %     TVGoffset_dB           : get_MWC_TVGoffset_dB
    %     %     sampleFreq_Hz          : get_MWC_SampleFreq_Hz
    %     %     soundVelocity_mPersect : get_MWC_SoundVelocity
    %
    %   %% 
    %   MWC_NumBeams = get_MWC_NumBeams(b);
    %   figure; plot(MWC_NumBeams); grid on; title('MWC_NumBeams', 'Interpreter', 'None');
    %
    %   %% 
    %   MWC_PhaseFlag = get_MWC_PhaseFlag(b);
    %   figure; plot(MWC_PhaseFlag); grid on; title('MWC_PhaseFlag', 'Interpreter', 'None');
    %
    %   %% 
    %   MWC_TVGfunctionApplied = get_MWC_TVGfunctionApplied(b);
    %   figure; plot(MWC_TVGfunctionApplied); grid on; title('MWC_TVGfunctionApplied', 'Interpreter', 'None');
    %
    %   %% 
    %   TVGoffset_dB = get_MWC_TVGoffset_dB(b);
    %   figure; plot(TVGoffset_dB); grid on; title('TVGoffset_dB', 'Interpreter', 'None');
    %
    %   %% 
    %   MWC_SampleFreq_Hz = get_MWC_SampleFreq_Hz(b);
    %   figure; plot(MWC_SampleFreq_Hz); grid on; title('MWC_SampleFreq_Hz', 'Interpreter', 'None');
    %
    %   %% 
    %   MWC_SoundVelocity = get_MWC_SoundVelocity(b);
    %   figure; plot(MWC_SoundVelocity); grid on; title('MWC_SoundVelocity', 'Interpreter', 'None');
    %
    %
    %
    %   %% 
    % b.sDataKmAll.dgmMWC(1).TxInfo
    %     %   Variable name         -  getter method
    %     %     numBytesTxInfo      : 12
    %     %     numTxSectors        : get_MWC_NumTxSectors
    %     %     numBytesPerTxSector : 16
    %     %     padding             : 0
    %     %     heave_m             : get_MWC_heave_m
    %
    %   %% 
    %   MWC_NumTxSectors = get_MWC_NumTxSectors(b);
    %   figure; plot(MWC_NumTxSectors); grid on; title('MWC_NumTxSectors', 'Interpreter', 'None');
    %
    %   %% 
    %   MWC_heave_m = get_MWC_heave_m(b);
    %   figure; plot(MWC_heave_m); grid on; title('MWC_heave_m', 'Interpreter', 'None');
    %
    %
    %
    %   %% 
    % b.sDataKmAll.dgmMWC(1).TxSectorData % 1×8 struct array with fields:
    %     %   Variable name          -  getter method
    %     %     tiltAngleReTx_deg    : get_MWC_TiltAngleReTx_deg
    %     %     centreFreq_Hz        : get_MWC_CentreFreq_Hz
    %     %     txBeamWidthAlong_deg : get_MWC_TxBeamWidthAlong_deg
    %     %     txSectorNumb         : get_MWC_TxSectorNumb
    %     %     padding              : 0
    %
    %   %% 
    %   MWC_TiltAngleReTx_deg = get_MWC_TiltAngleReTx_deg(b);
    %   figure; plot(MWC_TiltAngleReTx_deg); grid on; title('MWC_TiltAngleReTx_deg', 'Interpreter', 'None');
    %
    %   %% 
    %   MWC_CentreFreq_Hz = get_MWC_CentreFreq_Hz(b);
    %   figure; plot(MWC_CentreFreq_Hz); grid on; title('MWC_CentreFreq_Hz', 'Interpreter', 'None');
    %
    %   %% 
    %   MWC_TxBeamWidthAlong_deg = get_MWC_TxBeamWidthAlong_deg(b);
    %   figure; plot(MWC_TxBeamWidthAlong_deg); grid on; title('MWC_TxBeamWidthAlong_deg', 'Interpreter', 'None');
    %
    %   %% 
    %   MWC_TxSectorNumb = get_MWC_TxSectorNumb(b);
    %   figure; plot(MWC_TxSectorNumb); grid on; title('MWC_TxSectorNumb', 'Interpreter', 'None');
    %
    %
    %
    %   %% 
    % b.sDataKmAll.dgmMWC(1).RxBeamData(1)
    %     %   Variable name                -  getter method
    %     %     beamPointAngReVertical_deg : get_MWC_BeamPointAngReVertical_deg
    %     %     startRangeSampleNum        : get_MWC_StartRangeSampleNum
    %     %     detectedRangeInSamples     : get_MWC_DetectedRangeInSamples
    %     %     beamTxSectorNum            : get_MWC_BeamTxSectorNum
    %     %     numSampleData              : get_MWC_NumSampleData
    %     %     Amplitude                  : get_MWC_Amplitude
    %     %     Phase                      : get_MWC_Phase
    %
    %   %% 
    %   kPing = 1;
    %   MWC_NumBeamsOnePing = get_MWC_NumBeams_OnePing(b, kPing)
    %
    %   %% 
    %   MWC_NumSampleData = get_MWC_NumSampleData(b, kPing);
    %   figure; plot(MWC_NumSampleData); grid on; title('MWC_NumSampleData', 'Interpreter', 'None');
    %
    %   %% 
    %   MWC_BeamTxSectorNum = get_MWC_BeamTxSectorNum(b, kPing);
    %   figure; plot(MWC_BeamTxSectorNum); grid on; title('MWC_BeamTxSectorNum', 'Interpreter', 'None');
    %
    %   %% 
    %   MWC_DetectedRangeInSamples = get_MWC_DetectedRangeInSamples(b, kPing);
    %   figure; plot(MWC_DetectedRangeInSamples); grid on; title('MWC_DetectedRangeInSamples', 'Interpreter', 'None');
    %
    %   %% 
    %   MWC_StartRangeSampleNum = get_MWC_StartRangeSampleNum(b, kPing)
    %   figure; plot(MWC_StartRangeSampleNum); grid on; title('MWC_StartRangeSampleNum', 'Interpreter', 'None');
    %
    %   %% 
    %   MWC_BeamPointAngReVertical_deg = get_MWC_BeamPointAngReVertical_deg(b, kPing);
    %   figure; plot(MWC_BeamPointAngReVertical_deg); grid on; title('MWC_BeamPointAngReVertical_deg', 'Interpreter', 'None');
    %
    %   %% 
    %   MWC_Amplitude = get_MWC_Amplitude(b, kPing);
    %   figure; imagesc(MWC_Amplitude); axis ij; colormap(jet(256)); colorbar; title(['MWC_Amplitude - Ping ' num2str(kPing)], 'interp', 'None');
    %   hold on; plot(MWC_DetectedRangeInSamples);
    %
    %   %% 
    %   MWC_Phase = get_MWC_Phase(b, kPing);
    %   figure; imagesc(MWC_Phase); axis ij; colormap(JetSym); colorbar; title(['MWC_Phase - Ping ' num2str(kPing)], 'interp', 'None');
    %   hold on; plot(MWC_DetectedRangeInSamples, 'w');
    %
    %
    %
    %   %% Exploration of FCP datagrams
    % b.sDataKmAll.dgmFCF % 1×1 struct array with fields:
    %     %     Partition : [1×1 struct]
    %     %     FCommon   : [1×1 struct]
    %
    %   %% 
    % b.sDataKmAll.dgmFCF.Partition
    %     %   Variable name -  getter method
    %     %     numOfDgms   : get_MRZ_NumOfDgms(b, 'dgmName', 'FCF')
    %     %     dgmNum      : get_MRZ_DgmNum(   b, 'dgmName', 'FCF')
    %
    %   %% 
    %   FCF_NumOfDgms = get_MRZ_NumOfDgms(b, 'dgmName', 'FCF')
    %   FCF_DgmNum    = get_MRZ_DgmNum(   b, 'dgmName', 'FCF')
    %
    %   %% 
    % b.sDataKmAll.dgmFCF.FCommon
    %     %   Variable name -  getter method
    %     %     numBytesCmnPart   : 30
    %     %     fileStatus        : 0
    %     %     padding1          : 0
    %     %     numBytesFile      : 1
    %     %     filename          : [22×1 char]
    %     %     bsCalibrationFile : get_FCF_BsCalibrationFile
    %
    %   %% 
    %   FCF_BsCalibrationFile = get_FCF_BsCalibrationFile(b)
    %   FCF_BsCalibrationFile = get_FCF_BsCalibrationFile(b, 'OutputFile', ...
    %   fullfile(my_tempdir, '0039_20180905_222154_raw_dgmFCF_CalibFile'));
    %
    % More About :  Follow these steps to create a .mlx file :
    %          * Ask for the help of class ClKmall (help ClKmall)
    %          * Copy the example lines 
    %          * Paste these lines in a .m file (ExClKmall.m)
    %          * Export the file in .mlx file (ExClKmall.m)
    %          * Run the .mlx file
    %
    % Authors : JMA
    % See also +KmAll Authors
    % ----------------------------------------------------------------------------
    
    properties (GetAccess = 'public', SetAccess = 'public')
        Filename = '';   % Name of the .kmall file
    end
    
    properties (GetAccess = 'public', SetAccess = 'private')
        sDataKmAll
    end
    
    methods (Access = 'public')
        function this = ClKmall(varargin)
            % Constructor of the class
            
            [varargin, Filename] = getPropertyValue(varargin, 'Filename', []); %#ok<ASGLU>
            
            if exist(Filename, 'file')
                [flag, X] = KmAll.read_DataSimradKmAll(Filename);
                if ~flag
                    return
                end
                this.sDataKmAll = X;
                this.Filename = Filename;
            else
                this = ClKmall.empty();
            end
        end

        function plotInfoDatagrams(this)
            plot_histo_index(1:length(this.sDataKmAll.infoDatagrams.listDatagram), ...
                this.sDataKmAll.infoDatagrams.nbDgm, ...
                this.sDataKmAll.infoDatagrams.texteTypeDatagram, ...
                this.sDataKmAll.infoDatagrams.tabNbBytes, ...
                this.sDataKmAll.infoDatagrams.filename);
        end
        
        %% dgmMRZ.Sounding(:).GeoDepthPts
        
        function Y = get_MRZ_Datetime(this)
            pppp = arrayfun(@(x) (datenum(x.Header.Datetime)), this.sDataKmAll.dgmMRZ);
            Y = datetime(pppp', 'ConvertFrom', 'datenum');
        end
        
        function Y = get_MRZ_Depth(this)
            Y = -get_Dgm_Struct1_Variable_Struct2(this, 'MRZ', 'Sounding', 'GeoDepthPts', 'z_reRefPoint_m');
        end
        
        function Y = get_MRZ_AcrossDist(this)
            Y = get_Dgm_Struct1_Variable_Struct2(this, 'MRZ', 'Sounding', 'GeoDepthPts', 'y_reRefPoint_m');
        end
        
        function Y = get_MRZ_AlongDist(this)
            Y = get_Dgm_Struct1_Variable_Struct2(this, 'MRZ', 'Sounding', 'GeoDepthPts', 'x_reRefPoint_m');
        end
        
        function Y = get_MRZ_BeamIBA(this)
            Y = get_Dgm_Struct1_Variable_Struct2(this, 'MRZ', 'Sounding', 'GeoDepthPts', 'beamIncAngleAdj_deg');
        end
        
        function Y = get_MRZ_RealTimeCleanInfo(this)
            Y = get_Dgm_Struct1_Variable_Struct2(this, 'MRZ', 'Sounding', 'GeoDepthPts', 'realTimeCleanInfo');
        end
        
        function Y = get_MRZ_DeltaLatitude_deg(this)
            Y = get_Dgm_Struct1_Variable_Struct2(this, 'MRZ', 'Sounding', 'GeoDepthPts', 'deltaLatitude_deg');
        end
        
        function Y = get_MRZ_DeltaLongitude_deg(this)
            Y = get_Dgm_Struct1_Variable_Struct2(this, 'MRZ', 'Sounding', 'GeoDepthPts', 'deltaLongitude_deg');
        end
        
        %% dgmMRZ.Sounding(:).DetInfo
        
        function Y = get_MRZ_DetectionType(this)
            Y = get_Dgm_Struct1_Variable_Struct2(this, 'MRZ', 'Sounding', 'DetInfo', 'detectionType');
        end
        
        function Y = get_MRZ_DetectionMethod(this)
            Y = get_Dgm_Struct1_Variable_Struct2(this, 'MRZ', 'Sounding', 'DetInfo', 'detectionMethod');
        end
        
        function Y = get_MRZ_RejectionInfo1(this)
            Y = get_Dgm_Struct1_Variable_Struct2(this, 'MRZ', 'Sounding', 'DetInfo', 'rejectionInfo1');
        end
        
        function Y = get_MRZ_RejectionInfo2(this)
            Y = get_Dgm_Struct1_Variable_Struct2(this, 'MRZ', 'Sounding', 'DetInfo', 'rejectionInfo2');
        end
        
        function Y = get_MRZ_PostProcessingInfo(this)
            Y = get_Dgm_Struct1_Variable_Struct2(this, 'MRZ', 'Sounding', 'DetInfo', 'postProcessingInfo');
        end
        
        function Y = get_MRZ_DetectionClass(this)
            Y = get_Dgm_Struct1_Variable_Struct2(this, 'MRZ', 'Sounding', 'DetInfo', 'detectionClass');
        end
        
        function Y = get_MRZ_DetectionConfidenceLevel(this)
            Y = get_Dgm_Struct1_Variable_Struct2(this, 'MRZ', 'Sounding', 'DetInfo', 'detectionConfidenceLevel');
        end
        
        function Y = get_MRZ_RangeFactor(this)
            Y = get_Dgm_Struct1_Variable_Struct2(this, 'MRZ', 'Sounding', 'DetInfo', 'rangeFactor');
        end
        
        function Y = get_MRZ_QualityFactor(this)
            Y = get_Dgm_Struct1_Variable_Struct2(this, 'MRZ', 'Sounding', 'DetInfo', 'qualityFactor');
        end
        
        function Y = get_MRZ_DetectionUncertaintyVer_m(this)
            Y = get_Dgm_Struct1_Variable_Struct2(this, 'MRZ', 'Sounding', 'DetInfo', 'detectionUncertaintyVer_m');
        end
        
        function Y = get_MRZ_DetectionUncertaintyHor_m(this)
            Y = get_Dgm_Struct1_Variable_Struct2(this, 'MRZ', 'Sounding', 'DetInfo', 'detectionUncertaintyHor_m');
        end
        
        function Y = get_MRZ_DetectionWindowLength_sec(this)
            Y = get_Dgm_Struct1_Variable_Struct2(this, 'MRZ', 'Sounding', 'DetInfo', 'detectionWindowLength_sec');
        end
        
        function Y = get_MRZ_EchoLength_sec(this)
            Y = get_Dgm_Struct1_Variable_Struct2(this, 'MRZ', 'Sounding', 'DetInfo', 'echoLength_sec');
        end
        
        
        %% dgmMRZ.Sounding(:).BSData
        
        function Y = get_MRZ_MeanAbsCoeff_dBPerkm(this)
            Y = get_Dgm_Struct1_Variable_Struct2(this, 'MRZ', 'Sounding', 'BSData', 'meanAbsCoeff_dBPerkm');
        end
        
        function Y = get_MRZ_Reflectivity1_dB(this)
            Y = get_Dgm_Struct1_Variable_Struct2(this, 'MRZ', 'Sounding', 'BSData', 'reflectivity1_dB');
        end
        
        function Y = get_MRZ_Reflectivity2_dB(this)
            Y = get_Dgm_Struct1_Variable_Struct2(this, 'MRZ', 'Sounding', 'BSData', 'reflectivity2_dB');
        end
        
        function Y = get_MRZ_ReceiverSensitivityApplied_dB(this)
            Y = get_Dgm_Struct1_Variable_Struct2(this, 'MRZ', 'Sounding', 'BSData', 'receiverSensitivityApplied_dB');
        end
        
        function Y = get_MRZ_SourceLevelApplied_dB(this)
            Y = get_Dgm_Struct1_Variable_Struct2(this, 'MRZ', 'Sounding', 'BSData', 'sourceLevelApplied_dB');
        end
        
        function Y = get_MRZ_BScalibration_dB(this)
            Y = get_Dgm_Struct1_Variable_Struct2(this, 'MRZ', 'Sounding', 'BSData', 'BScalibration_dB');
        end
        
        function Y = get_MRZ_TVG_dB(this)
            Y = get_Dgm_Struct1_Variable_Struct2(this, 'MRZ', 'Sounding', 'BSData', 'TVG_dB');
        end
        
        
        %% dgmMRZ.Sounding(:).RangeAngleData
        
        function Y = get_MRZ_BeamAngleReRx_deg(this)
            Y = get_Dgm_Struct1_Variable_Struct2(this, 'MRZ', 'Sounding', 'RangeAngleData', 'beamAngleReRx_deg');
        end
        
        function Y = get_MRZ_BeamAngleReRxCorrection_deg(this)
            Y = get_Dgm_Struct1_Variable_Struct2(this, 'MRZ', 'Sounding', 'RangeAngleData', 'beamAngleReRxCorrection_deg');
        end
        
        function Y = get_MRZ_TwoWayTravelTime_sec(this)
            Y = get_Dgm_Struct1_Variable_Struct2(this, 'MRZ', 'Sounding', 'RangeAngleData', 'twoWayTravelTime_sec');
        end
        
        function Y = get_MRZ_TwoWayTravelTimeCorrection_sec(this)
            Y = get_Dgm_Struct1_Variable_Struct2(this, 'MRZ', 'Sounding', 'RangeAngleData', 'twoWayTravelTimeCorrection_sec');
        end
        
        
        %% dgmMRZ.Sounding(:).SeabedImage
        
        function Y = get_MRZ_SIstartRange_samples(this)
            Y = get_Dgm_Struct1_Variable_Struct2(this, 'MRZ', 'Sounding', 'SeabedImage', 'SIstartRange_samples');
        end
        
        function Y = get_MRZ_SIcentreSample(this)
            Y = get_Dgm_Struct1_Variable_Struct2(this, 'MRZ', 'Sounding', 'SeabedImage', 'SIcentreSample');
        end
        
        function Y = get_MRZ_SInumSamples(this)
            Y = get_Dgm_Struct1_Variable_Struct2(this, 'MRZ', 'Sounding', 'SeabedImage', 'SInumSamples');
        end
        
        
        %% dgmMRZ.Sounding(:).WCParams
        
        function Y = get_MRZ_WCBeamNumber(this)
            Y = get_Dgm_Struct1_Variable_Struct2(this, 'MRZ', 'Sounding', 'WCParams', 'WCBeamNumber');
        end
        
        function Y = get_MRZ_WCRangeSamples(this)
            Y = get_Dgm_Struct1_Variable_Struct2(this, 'MRZ', 'Sounding', 'WCParams', 'WCRangeSamples');
        end
        
        function Y = get_MRZ_WCNomBeamAngleAcross(this)
            Y = get_Dgm_Struct1_Variable_Struct2(this, 'MRZ', 'Sounding', 'WCParams', 'WCNomBeamAngleAcross');
        end
        
        
        
        %% dgmMRZ.Sounding(:)
        
        function Y = get_MRZ_TxSectorNumber(this)
            Y = get_Dgm_Struct_Variable(this, 'MRZ', 'Sounding', 'txSectorNumber');
        end
        
        function Y = get_MRZ_SoundingIndex(this)
            Y = get_Dgm_Struct_Variable(this, 'MRZ', 'Sounding', 'soundingIndex');
        end
        
        
        
        %% dgmMRZ.RxInfo
        
        function Y = get_MRZ_NumBytesRxInfo(this)
            Y = get_Dgm_Struct_Variable(this, 'MRZ', 'RxInfo', 'numBytesRxInfo');
        end
        
        function Y = get_MRZ_NumSoundingsMaxMain(this)
            Y = get_Dgm_Struct_Variable(this, 'MRZ', 'RxInfo', 'numSoundingsMaxMain');
        end
        
        function Y = get_MRZ_NumSoundingsValidMain(this)
            Y = get_Dgm_Struct_Variable(this, 'MRZ', 'RxInfo', 'numSoundingsValidMain');
        end
        
        function Y = get_MRZ_NumBytesPerSounding(this)
            Y = get_Dgm_Struct_Variable(this, 'MRZ', 'RxInfo', 'numBytesPerSounding');
        end
        
        function Y = get_MRZ_WCSampleRate(this)
            Y = get_Dgm_Struct_Variable(this, 'MRZ', 'RxInfo', 'WCSampleRate');
        end
        
        function Y = get_MRZ_SeabedImageSampleRate(this)
            Y = get_Dgm_Struct_Variable(this, 'MRZ', 'RxInfo', 'seabedImageSampleRate');
        end
        
        function Y = get_MRZ_BSnormal_dB(this)
            Y = get_Dgm_Struct_Variable(this, 'MRZ', 'RxInfo', 'BSnormal_dB');
        end
        
        function Y = get_MRZ_BSoblique_dB(this)
            Y = get_Dgm_Struct_Variable(this, 'MRZ', 'RxInfo', 'BSoblique_dB');
        end
        
        function Y = get_MRZ_ExtraDetectionAlarmFlag(this)
            Y = get_Dgm_Struct_Variable(this, 'MRZ', 'RxInfo', 'extraDetectionAlarmFlag');
        end
        
        function Y = get_MRZ_NumExtraDetections(this)
            Y = get_Dgm_Struct_Variable(this, 'MRZ', 'RxInfo', 'numExtraDetections');
        end
        
        function Y = get_MRZ_NumExtraDetectionClasses(this)
            Y = get_Dgm_Struct_Variable(this, 'MRZ', 'RxInfo', 'numExtraDetectionClasses');
        end
        
        function Y = get_MRZ_NumBytesPerClass(this)
            Y = get_Dgm_Struct_Variable(this, 'MRZ', 'RxInfo', 'numBytesPerClass');
        end
        
        
        
        %% dgmMRZ.PingInfo
        
        function Y = get_MRZ_NumBytesInfoData(this)
            Y = get_Dgm_Struct_Variable(this, 'MRZ', 'PingInfo', 'numBytesInfoData');
        end
        
        
        
        %% dgmMRZ.PingInfo.General
        
        function Y = get_MRZ_PingRate_Hz(this)
            Y = get_Dgm_Struct1_Struct2_Variable(this, 'MRZ', 'PingInfo', 'General', 'pingRate_Hz');
        end
        
        function Y = get_MRZ_BeamSpacing(this)
            Y = get_Dgm_Struct1_Struct2_Variable(this, 'MRZ', 'PingInfo', 'General', 'beamSpacing');
        end
        
        function Y = get_MRZ_DepthMode(this)
            Y = get_Dgm_Struct1_Struct2_Variable(this, 'MRZ', 'PingInfo', 'General', 'depthMode');
        end
        
        function Y = get_MRZ_SubDepthMode(this)
            Y = get_Dgm_Struct1_Struct2_Variable(this, 'MRZ', 'PingInfo', 'General', 'subDepthMode');
        end
        
        function Y = get_MRZ_DistanceBtwSwath(this)
            Y = get_Dgm_Struct1_Struct2_Variable(this, 'MRZ', 'PingInfo', 'General', 'distanceBtwSwath');
        end
        
        function Y = get_MRZ_DetectionMode(this)
            Y = get_Dgm_Struct1_Struct2_Variable(this, 'MRZ', 'PingInfo', 'General', 'detectionMode');
        end
        
        function Y = get_MRZ_PulseForm(this)
            Y = get_Dgm_Struct1_Struct2_Variable(this, 'MRZ', 'PingInfo', 'General', 'pulseForm');
        end
        
        function Y = get_MRZ_FrequencyMode_Hz(this)
            Y = get_Dgm_Struct1_Struct2_Variable(this, 'MRZ', 'PingInfo', 'General', 'frequencyMode_Hz');
        end
        
        function Y = get_MRZ_FreqRangeLowLim_Hz(this)
            Y = get_Dgm_Struct1_Struct2_Variable(this, 'MRZ', 'PingInfo', 'General', 'freqRangeLowLim_Hz');
        end
        
        function Y = get_MRZ_FreqRangeHighLim_Hz(this)
            Y = get_Dgm_Struct1_Struct2_Variable(this, 'MRZ', 'PingInfo', 'General', 'freqRangeHighLim_Hz');
        end
        
        function Y = get_MRZ_MaxTotalTxPulseLength_sec(this)
            Y = get_Dgm_Struct1_Struct2_Variable(this, 'MRZ', 'PingInfo', 'General', 'freqRangeHighLim_Hz');
        end
        
        function Y = get_MRZ_MaxEffTxPulseLength_sec(this)
            Y = get_Dgm_Struct1_Struct2_Variable(this, 'MRZ', 'PingInfo', 'General', 'maxEffTxPulseLength_sec');
        end
        
        function Y = get_MRZ_MaxEffTxBandWidth_Hz(this)
            Y = get_Dgm_Struct1_Struct2_Variable(this, 'MRZ', 'PingInfo', 'General', 'maxEffTxBandWidth_Hz');
        end
        
        function Y = get_MRZ_AbsCoeff_dBPerkm(this)
            Y = get_Dgm_Struct1_Struct2_Variable(this, 'MRZ', 'PingInfo', 'General', 'absCoeff_dBPerkm');
        end
        
        function Y = get_MRZ_PortSectorEdge_deg(this)
            Y = -get_Dgm_Struct1_Struct2_Variable(this, 'MRZ', 'PingInfo', 'General', 'portSectorEdge_deg');
        end
        
        function Y = get_MRZ_StarbSectorEdge_deg(this)
            Y = -get_Dgm_Struct1_Struct2_Variable(this, 'MRZ', 'PingInfo', 'General', 'starbSectorEdge_deg');
        end
        
        function Y = get_MRZ_PortMeanCov_deg(this)
            Y = -get_Dgm_Struct1_Struct2_Variable(this, 'MRZ', 'PingInfo', 'General', 'portMeanCov_deg');
        end
        
        function Y = get_MRZ_StarbMeanCov_deg(this)
            Y = -get_Dgm_Struct1_Struct2_Variable(this, 'MRZ', 'PingInfo', 'General', 'starbMeanCov_deg');
        end
        
        function Y = get_MRZ_PortMeanCov_m(this)
            Y = get_Dgm_Struct1_Struct2_Variable(this, 'MRZ', 'PingInfo', 'General', 'portMeanCov_m');
        end
        
        function Y = get_MRZ_StarbMeanCov_m(this)
            Y = get_Dgm_Struct1_Struct2_Variable(this, 'MRZ', 'PingInfo', 'General', 'starbMeanCov_m');
        end
        
        function Y = get_MRZ_ModeAndStabilisation(this)
            Y = get_Dgm_Struct1_Struct2_Variable(this, 'MRZ', 'PingInfo', 'General', 'modeAndStabilisation');
        end
        
        function Y = get_MRZ_RuntimeFilter1(this)
            Y = get_Dgm_Struct1_Struct2_Variable(this, 'MRZ', 'PingInfo', 'General', 'runtimeFilter1');
        end
        
        function Y = get_MRZ_RuntimeFilter2(this)
            Y = get_Dgm_Struct1_Struct2_Variable(this, 'MRZ', 'PingInfo', 'General', 'runtimeFilter2');
        end
        
        function Y = get_MRZ_PipeTrackingStatus(this)
            Y = get_Dgm_Struct1_Struct2_Variable(this, 'MRZ', 'PingInfo', 'General', 'pipeTrackingStatus');
        end
        
        function Y = get_MRZ_TransmitArraySizeUsed_deg(this)
            Y = get_Dgm_Struct1_Struct2_Variable(this, 'MRZ', 'PingInfo', 'General', 'transmitArraySizeUsed_deg');
        end
        
        function Y = get_MRZ_ReceiveArraySizeUsed_deg(this)
            Y = get_Dgm_Struct1_Struct2_Variable(this, 'MRZ', 'PingInfo', 'General', 'receiveArraySizeUsed_deg');
        end
        
        function Y = get_MRZ_TransmitPower_dB(this)
            Y = get_Dgm_Struct1_Struct2_Variable(this, 'MRZ', 'PingInfo', 'General', 'transmitPower_dB');
        end
        
        function Y = get_MRZ_SLrampUpTimeRemaining(this)
            Y = get_Dgm_Struct1_Struct2_Variable(this, 'MRZ', 'PingInfo', 'General', 'SLrampUpTimeRemaining');
        end
        
        function Y = get_MRZ_YawAngle_deg(this)
            Y = get_Dgm_Struct1_Struct2_Variable(this, 'MRZ', 'PingInfo', 'General', 'yawAngle_deg');
        end
        
        
        %% dgmMRZ.PingInfo.TxSectorInfo
        
        function Y = get_MRZ_NumTxSectors(this)
            Y = get_Dgm_Struct1_Struct2_Variable(this, 'MRZ', 'PingInfo', 'TxSectorInfo', 'numTxSectors');
        end
        
        function Y = get_MRZ_NumBytesPerTxSector(this)
            Y = get_Dgm_Struct1_Struct2_Variable(this, 'MRZ', 'PingInfo', 'TxSectorInfo', 'numBytesPerTxSector');
        end
        
        
        %% dgmMRZ.PingInfo.TxTime1stPulse
        
        function Y = get_MRZ_HeadingVessel_deg(this)
            Y = get_Dgm_Struct1_Struct2_Variable(this, 'MRZ', 'PingInfo', 'TxTime1stPulse', 'headingVessel_deg');
        end
        
        function Y = get_MRZ_SoundSpeedAtTxDepth_mPerSec(this)
            Y = get_Dgm_Struct1_Struct2_Variable(this, 'MRZ', 'PingInfo', 'TxTime1stPulse', 'soundSpeedAtTxDepth_mPerSec');
        end
        
        function Y = get_MRZ_TxTransducerDepth_m(this)
            Y = get_Dgm_Struct1_Struct2_Variable(this, 'MRZ', 'PingInfo', 'TxTime1stPulse', 'txTransducerDepth_m');
        end
        
        function Y = get_MRZ_Z_waterLevelReRefPoint_m(this)
            Y = get_Dgm_Struct1_Struct2_Variable(this, 'MRZ', 'PingInfo', 'TxTime1stPulse', 'z_waterLevelReRefPoint_m');
        end
        
        function Y = get_MRZ_X_kmallToall_m(this)
            Y = get_Dgm_Struct1_Struct2_Variable(this, 'MRZ', 'PingInfo', 'TxTime1stPulse', 'x_kmallToall_m');
        end
        
        function Y = get_MRZ_Y_kmallToall_m(this)
            Y = get_Dgm_Struct1_Struct2_Variable(this, 'MRZ', 'PingInfo', 'TxTime1stPulse', 'y_kmallToall_m');
        end
        
        function Y = get_MRZ_LatLongInfo(this)
            Y = get_Dgm_Struct1_Struct2_Variable(this, 'MRZ', 'PingInfo', 'TxTime1stPulse', 'latLongInfo');
        end
        
        function Y = get_MRZ_PosSensorStatus(this)
            Y = get_Dgm_Struct1_Struct2_Variable(this, 'MRZ', 'PingInfo', 'TxTime1stPulse', 'posSensorStatus');
        end
        
        function Y = get_MRZ_AttitudeSensorStatus(this)
            Y = get_Dgm_Struct1_Struct2_Variable(this, 'MRZ', 'PingInfo', 'TxTime1stPulse', 'attitudeSensorStatus');
        end
        
        function Y = get_MRZ_Latitude_deg(this)
            Y = get_Dgm_Struct1_Struct2_Variable(this, 'MRZ', 'PingInfo', 'TxTime1stPulse', 'latitude_deg');
        end
        
        function Y = get_MRZ_Longitude_deg(this)
            Y = get_Dgm_Struct1_Struct2_Variable(this, 'MRZ', 'PingInfo', 'TxTime1stPulse', 'longitude_deg');
        end
        
        function Y = get_MRZ_EllipsoidHeightReRefPoint_m(this)
            Y = get_Dgm_Struct1_Struct2_Variable(this, 'MRZ', 'PingInfo', 'TxTime1stPulse', 'ellipsoidHeightReRefPoint_m');
        end
        
        
        %% dgmMRZ.PingInfo.dgmParams
        
        function Y = get_MRZ_BsCorrectionOffset_dB(this) % TODO GLU : bsCorrectionOffset_dB n'est pas présent dans sDataKmAll.dgmMRZ(k).PingInfo.General
            Y = get_Dgm_Struct1_Struct2_Variable(this, 'MRZ', 'PingInfo', 'General', 'bsCorrectionOffset_dB');
        end
        
        function Y = get_MRZ_LambertsLawApplied(this) % TODO GLU
            Y = get_Dgm_Struct1_Struct2_Variable(this, 'MRZ', 'PingInfo', 'General', 'lambertsLawApplied');
        end
        
        function Y = get_MRZ_IceWindow(this) % TODO GLU
            Y = get_Dgm_Struct1_Struct2_Variable(this, 'MRZ', 'PingInfo', 'General', 'iceWindow');
        end
        
        function Y = get_MRZ_ActiveModes(this) % TODO GLU
            Y = get_Dgm_Struct1_Struct2_Variable(this, 'MRZ', 'PingInfo', 'General', 'activeModes');
        end
        
        
        %% dgmMRZ.PingInfo.TxSectorInfo
        
        function Y = get_MRZ_TxSectorNumb(this)
            Y = get_Dgm_Struct_Variable(this, 'MRZ', 'TxSectorInfo', 'txSectorNumb');
        end
        
        function Y = get_MRZ_TxArrNumber(this)
            Y = get_Dgm_Struct_Variable(this, 'MRZ', 'TxSectorInfo', 'txArrNumber');
        end
        
        function Y = get_MRZ_TxSubArray(this)
            Y = get_Dgm_Struct_Variable(this, 'MRZ', 'TxSectorInfo', 'txSubArray');
        end
        
        function Y = get_MRZ_SectorTransmitDelay_sec(this)
            Y = get_Dgm_Struct_Variable(this, 'MRZ', 'TxSectorInfo', 'sectorTransmitDelay_sec');
        end
        
        function Y = get_MRZ_TiltAngleReTx_deg(this)
            Y = get_Dgm_Struct_Variable(this, 'MRZ', 'TxSectorInfo', 'tiltAngleReTx_deg');
        end
        
        function Y = get_MRZ_CentreFreq_Hz(this)
            Y = get_Dgm_Struct_Variable(this, 'MRZ', 'TxSectorInfo', 'centreFreq_Hz');
        end
        
        function Y = get_MRZ_TotalSignalLength_sec(this)
            Y = get_Dgm_Struct_Variable(this, 'MRZ', 'TxSectorInfo', 'totalSignalLength_sec');
        end
        
        function Y = get_MRZ_TxFocusRange_m(this)
            Y = get_Dgm_Struct_Variable(this, 'MRZ', 'TxSectorInfo', 'txFocusRange_m');
        end
        
        function Y = get_MRZ_TxBandwidth_Hz(this)
            Y = get_Dgm_Struct_Variable(this, 'MRZ', 'TxSectorInfo', 'signalBandWidth_Hz');
        end
        
        function Y = get_MRZ_SignalWaveForm(this)
            Y = get_Dgm_Struct_Variable(this, 'MRZ', 'TxSectorInfo', 'signalWaveForm');
        end
        
        function Y = get_MRZ_EffectiveSignalLength_sec(this)
            Y = get_Dgm_Struct_Variable(this, 'MRZ', 'TxSectorInfo', 'effectiveSignalLength_sec');
        end
        
        
        %% xxx.Body - Ex : dgmMRZ.Body
        
        function Y = get_MRZ_NumBytesCmnPart(this, varargin)
            [varargin, dgmName] = getPropertyValue(varargin, 'dgmName', 'MRZ'); %#ok<ASGLU>
            Y = get_Dgm_Struct_Variable(this, dgmName, 'Body', 'numBytesCmnPart');
        end
        
        function Y = get_MRZ_PingCnt(this, varargin)
            [varargin, dgmName] = getPropertyValue(varargin, 'dgmName', 'MRZ'); %#ok<ASGLU>
            Y = get_Dgm_Struct_Variable(this, dgmName, 'Body', 'pingCnt');
            Y = double(Y);
        end
        
        function Y = get_MRZ_RxFansPerPing(this, varargin)
            [varargin, dgmName] = getPropertyValue(varargin, 'dgmName', 'MRZ'); %#ok<ASGLU>
            Y = get_Dgm_Struct_Variable(this, dgmName, 'Body', 'rxFansPerPing');
        end
        
        function Y = get_MRZ_RxFanIndex(this, varargin)
            [varargin, dgmName] = getPropertyValue(varargin, 'dgmName', 'MRZ'); %#ok<ASGLU>
            Y = get_Dgm_Struct_Variable(this, dgmName, 'Body', 'rxFanIndex');
        end
        
        function Y = get_MRZ_SwathsPerPing(this, varargin)
            [varargin, dgmName] = getPropertyValue(varargin, 'dgmName', 'MRZ'); %#ok<ASGLU>
            Y = get_Dgm_Struct_Variable(this, dgmName, 'Body', 'swathsPerPing');
        end
        
        function Y = get_MRZ_SwathAlongPosition(this, varargin)
            [varargin, dgmName] = getPropertyValue(varargin, 'dgmName', 'MRZ'); %#ok<ASGLU>
            Y = get_Dgm_Struct_Variable(this, dgmName, 'Body', 'swathAlongPosition');
        end
        
        function Y = get_MRZ_TxTransducerInd(this, varargin)
            [varargin, dgmName] = getPropertyValue(varargin, 'dgmName', 'MRZ'); %#ok<ASGLU>
            Y = get_Dgm_Struct_Variable(this, dgmName, 'Body', 'txTransducerInd');
        end
        
        function Y = get_MRZ_RxTransducerInd(this, varargin)
            [varargin, dgmName] = getPropertyValue(varargin, 'dgmName', 'MRZ'); %#ok<ASGLU>
            Y = get_Dgm_Struct_Variable(this, dgmName, 'Body', 'rxTransducerInd');
        end
        
        function Y = get_MRZ_NumRxTransducers(this, varargin)
            [varargin, dgmName] = getPropertyValue(varargin, 'dgmName', 'MRZ'); %#ok<ASGLU>
            Y = get_Dgm_Struct_Variable(this, dgmName, 'Body', 'numRxTransducers');
        end
        
        function Y = get_MRZ_AlgorithmType(this, varargin)
            [varargin, dgmName] = getPropertyValue(varargin, 'dgmName', 'MRZ'); %#ok<ASGLU>
            Y = get_Dgm_Struct_Variable(this, dgmName, 'Body', 'algorithmType');
        end
        
        
        
        %% xxx.Partition - Ex : dgmMRZ.Partition
        
        function Y = get_MRZ_NumOfDgms(this, varargin)
            [varargin, dgmName] = getPropertyValue(varargin, 'dgmName', 'MRZ'); %#ok<ASGLU>
            Y = get_Dgm_Struct_Variable(this, dgmName, 'Partition', 'numOfDgms');
        end
        
        function Y = get_MRZ_DgmNum(this, varargin)
            [varargin, dgmName] = getPropertyValue(varargin, 'dgmName', 'MRZ'); %#ok<ASGLU>
            Y = get_Dgm_Struct_Variable(this, dgmName, 'Partition', 'dgmNum');
        end
        
        
        
        
        %% dgmMRZ.Samples
        
        function [Y, Y2] = get_MRZ_Samples(this)
            nbPings = numel(this.sDataKmAll.dgmMRZ);
            for k=nbPings:-1:1
                X = this.sDataKmAll.dgmMRZ(k).Samples;
                nbSamples(k) = length(X);
                Y{k,:} = X;
            end
            nbSampleMax = max(nbSamples);
            Y2 = NaN(nbPings, nbSampleMax, 'single');
            for k=1:nbPings
                Y2(k,1:nbSamples(k)) = Y{k,:};
            end
        end
        
        
        
        
        %% dgmIIP
        
        function Y = get_IIP_Datetime(this)
            pppp = arrayfun(@(x) (datenum(x.Header.Datetime)), this.sDataKmAll.dgmIIP);
            Y = datetime(pppp', 'ConvertFrom', 'datenum');
        end
        
        function Y = get_IIP_RevFormat(this)
            Y = this.sDataKmAll.dgmIIP(1).revFormat;
        end
        
        function Y = get_IIP_Install_txt(this)
            Y = this.sDataKmAll.dgmIIP(1).install_txt;
        end
        
        
        
        
        %% dgmIOP
        
        function Y = get_IOP_Datetime(this)
            pppp = arrayfun(@(x) (datenum(x.Header.Datetime)), this.sDataKmAll.dgmIOP);
            Y = datetime(pppp', 'ConvertFrom', 'datenum');
        end

        
        function Y = get_IOP_getRevFormat(this)
            Y = this.sDataKmAll.dgmIOP(1).revFormat;
        end
        
        function Y = get_IOP_Install_txt(this)
            Y = this.sDataKmAll.dgmIOP(1).runtime_txt;
        end
        
        
        
        
        %% dgmSVP
        
        
        function Y = get_SVP_Datetime(this)
            pppp = arrayfun(@(x) (datenum(x.Header.Datetime)), this.sDataKmAll.dgmSVP);
            Y = datetime(pppp', 'ConvertFrom', 'datenum');
        end
        
        function Y = get_SVP_NumSamples(this)
            Y = get_Dgm_Variable(this, 'SVP', 'numSamples');
        end
        
        function Y = get_SVP_SensorFormat(this)
            Y = this.sDataKmAll.dgmSVP.sensorFormat;
        end
        
        function Y = get_SVP_Depth(this)
            Y = -get_Dgm_Struct_Variable(this, 'SVP', 'profil', 'depth_m');
        end
        
        function Y = get_SVP_Celerity(this)
            Y = get_Dgm_Struct_Variable(this, 'SVP', 'profil', 'soundVelocity_mPerSec');
        end
        
        function Y = get_SVP_Temp(this)
            Y = get_Dgm_Struct_Variable(this, 'SVP', 'profil', 'temp_C');
        end
        
        function Y = get_SVP_Salinity(this)
            Y = get_Dgm_Struct_Variable(this, 'SVP', 'profil', 'salinity');
        end
        
        function Y = get_SVP_Absorption(this)
            Y = get_Dgm_Struct_Variable(this, 'SVP', 'profil', 'absCoeff_dBPerkm');
        end
        
        
        
        
        %% dgmMWC.RxBeamData
        
        function Y = get_MWC_Datetime(this) % TODO : non utilisable pour l'instant, attente réponse Roger
            pppp = arrayfun(@(x) (datenum(x.Header.Datetime)), this.sDataKmAll.dgmMWC);
            Y = datetime(pppp', 'ConvertFrom', 'datenum');
        end
        
        function Y = get_MWC_NumSampleData(this, varargin)
            if nargin == 1
                for k=length(this.sDataKmAll.dgmMWC):-1:1
                    Y(k,:) = get_DgmMWC_RxBeamData(this, k, 'numSampleData');
                end
            else
                iEchogram = varargin{1};
                Y = get_DgmMWC_RxBeamData(this, iEchogram, 'numSampleData');
            end
        end
        
        function Y = get_MWC_BeamTxSectorNum(this, varargin)
            if nargin == 1
                for k=length(this.sDataKmAll.dgmMWC):-1:1
                    Y(k,:) = get_DgmMWC_RxBeamData(this, k, 'beamTxSectorNum');
                end
            else
                iEchogram = varargin{1};
                Y = get_DgmMWC_RxBeamData(this, iEchogram, 'beamTxSectorNum');
            end
        end
        
        function Y = get_MWC_DetectedRangeInSamples(this, varargin)
            if nargin == 1
                for k=length(this.sDataKmAll.dgmMWC):-1:1
                    Y(k,:) = get_DgmMWC_RxBeamData(this, k, 'detectedRangeInSamples');
                end
            else
                iEchogram = varargin{1};
                Y = get_DgmMWC_RxBeamData(this, iEchogram, 'detectedRangeInSamples');
            end
        end
        
        function Y = get_MWC_StartRangeSampleNum(this, varargin)
            if nargin == 1
                for k=length(this.sDataKmAll.dgmMWC):-1:1
                    Y(k,:) = get_DgmMWC_RxBeamData(this, k, 'startRangeSampleNum');
                end
            else
                iEchogram = varargin{1};
                Y = get_DgmMWC_RxBeamData(this, iEchogram, 'startRangeSampleNum');
            end
        end
        
        function Y = get_MWC_BeamPointAngReVertical_deg(this, varargin)
            if nargin == 1
                for k=length(this.sDataKmAll.dgmMWC):-1:1
                    Y(k,:) = -get_DgmMWC_RxBeamData(this, k, 'beamPointAngReVertical_deg');
                end
            else
                iEchogram = varargin{1};
                Y = -get_DgmMWC_RxBeamData(this, iEchogram, 'beamPointAngReVertical_deg');
            end
        end
        
        function Y = get_MWC_NumBeams_OnePing(this, iEchogram)
            Y = this.sDataKmAll.dgmMWC(iEchogram).RxInfo.numBeams;
        end
        
        function Y = get_MWC_Amplitude(this, iEchogram)
            nbSamples = get_DgmMWC_RxBeamData(this, iEchogram, 'numSampleData');
            nbSamplesMax = max(nbSamples);
            numBeams = get_MWC_NumBeams_OnePing(this, iEchogram);
            Y = NaN(nbSamplesMax, numBeams, 'single');
            for iBeam=1:numBeams
                Y(1:nbSamples(iBeam), iBeam) = this.sDataKmAll.dgmMWC(iEchogram).RxBeamData(iBeam).Amplitude;
            end
        end
        
        function Y = get_MWC_Phase(this, iEchogram)
            nbSamples = get_DgmMWC_RxBeamData(this, iEchogram, 'numSampleData');
            nbSamplesMax = max(nbSamples);
            numBeams = get_MWC_NumBeams_OnePing(this, iEchogram);
            Y = NaN(nbSamplesMax, numBeams, 'single');
            for iBeam=1:numBeams
                Y(1:nbSamples(iBeam), iBeam) = this.sDataKmAll.dgmMWC(iEchogram).RxBeamData(iBeam).Phase;
            end
        end
        
        
        
        %% dgmMWC.TxSectorData
        
        function Y = get_MWC_TiltAngleReTx_deg(this)
            Y = get_Dgm_Struct_Variable(this, 'MWC', 'TxSectorData', 'tiltAngleReTx_deg');
        end
        
        function Y = get_MWC_CentreFreq_Hz(this)
            Y = get_Dgm_Struct_Variable(this, 'MWC', 'TxSectorData', 'centreFreq_Hz');
        end
        
        function Y = get_MWC_TxBeamWidthAlong_deg(this)
            Y = get_Dgm_Struct_Variable(this, 'MWC', 'TxSectorData', 'txBeamWidthAlong_deg');
        end
        
        function Y = get_MWC_TxSectorNumb(this)
            Y = get_Dgm_Struct_Variable(this, 'MWC', 'TxSectorData', 'txSectorNumb');
        end
        
        
        
        
        %% dgmMWC.RxInfo
        
        function Y = get_MWC_NumBeams(this)
            Y = get_Dgm_Struct_Variable(this, 'MWC', 'RxInfo', 'numBeams');
        end
        
        function Y = get_MWC_PhaseFlag(this)
            Y = get_Dgm_Struct_Variable(this, 'MWC', 'RxInfo', 'phaseFlag');
        end
        
        function Y = get_MWC_TVGfunctionApplied(this)
            Y = get_Dgm_Struct_Variable(this, 'MWC', 'RxInfo', 'TVGfunctionApplied');
        end
        
        function Y = get_MWC_TVGoffset_dB(this)
            Y = get_Dgm_Struct_Variable(this, 'MWC', 'RxInfo', 'TVGoffset_dB');
        end
        
        function Y = get_MWC_SampleFreq_Hz(this)
            Y = get_Dgm_Struct_Variable(this, 'MWC', 'RxInfo', 'sampleFreq_Hz');
        end
        
        function Y = get_MWC_SoundVelocity(this)
            Y = get_Dgm_Struct_Variable(this, 'MWC', 'RxInfo', 'soundVelocity_mPersect');
        end
        
        
        
        %% dgmMWC.TxInfo
        
        function Y = get_MWC_NumTxSectors(this)
            Y = get_Dgm_Struct_Variable(this, 'MWC', 'TxInfo', 'numTxSectors');
        end
        
        function Y = get_MWC_heave_m(this)
            Y = get_Dgm_Struct_Variable(this, 'MWC', 'TxInfo', 'heave_m');
        end
        
        
        
        
        %% dgmCHE.dgmCHE % TODO GLU : bizarre d'avoir dgmCHE(k).dgmCHE !
        
        function Y = get_CHE_Heave_m(this)
            Y = get_Dgm_Struct_Variable(this, 'CHE', 'dgmCHE', 'heave_m');
        end
        
        
        
        %% dgmSCL
                
        function Y = get_SCL_Datetime(this)
            pppp = arrayfun(@(x) (datenum(x.Header.Datetime)), this.sDataKmAll.dgmSCL);
            Y = datetime(pppp', 'ConvertFrom', 'datenum');
        end
        
        function Y = get_SCL_DataFromSensor(this)
            Y = get_Dgm_Struct_Variable_CellArray(this, 'SCL', 'DataSensor', 'dataFromSensor');
        end
        
        function Y = get_SCL_Duration(this)
            seconds = get_Dgm_Struct_Variable(this, 'SCL', 'DataSensor', 'offset_sec');
            nanosec = get_Dgm_Struct_Variable(this, 'SCL', 'DataSensor', 'clockdevPU_nanosec');
            X  = double(seconds) + (double(nanosec) / 1e9);
            Y = duration(0, 0, X);
            Y.Format = 'hh:mm:ss.SSSSSS';
        end
        
        function Y = get_SCL_sensorStatus(this)
            Y = get_Dgm_Struct_Variable(this, 'SCL', 'Common', 'sensorStatus');
        end
        
        
        %% dgmSPO
        
        function Y = get_SPO_PosFixQuality_m(this)
            Y = get_Dgm_Struct_Variable(this, 'SPO', 'DataSensor', 'posFixQuality_m');
        end
        
        function Y = get_SPO_CorrectedLat_deg(this)
            Y = get_Dgm_Struct_Variable(this, 'SPO', 'DataSensor', 'correctedLat_deg');
        end
        
        function Y = get_SPO_CorrectedLon_deg(this)
            Y = get_Dgm_Struct_Variable(this, 'SPO', 'DataSensor', 'correctedLon_deg');
        end
        
        function Y = get_SPO_SpeedOverGround_mPerSec(this)
            Y = get_Dgm_Struct_Variable(this, 'SPO', 'DataSensor', 'speedOverGround_mPerSec');
        end
        
        function Y = get_SPO_CourseOverGround_deg(this)
            Y = get_Dgm_Struct_Variable(this, 'SPO', 'DataSensor', 'courseOverGround_deg');
        end
        
        function Y = get_SPO_EllipsoidHeightReRefPoint_m(this)
            Y = get_Dgm_Struct_Variable(this, 'SPO', 'DataSensor', 'ellipsoidHeightReRefPoint_m');
        end
        
        function Y = get_SPO_PosDataFromSensor(this)
            for k=numel(this.sDataKmAll.dgmSPO):-1:1
                Y{k,1} = this.sDataKmAll.dgmSPO(k).DataSensor.posDataFromSensor;
            end
        end
        
        function Y = get_SPO_Datetime(this)
            seconds = get_Dgm_Struct_Variable(this, 'SPO', 'DataSensor', 'timeFromSensor_sec');
            nanosec = get_Dgm_Struct_Variable(this, 'SPO', 'DataSensor', 'timeFromSensor_nanosec');
            X  = double(seconds) + (double(nanosec) / 1e9);
            Y = datetime(X, 'ConvertFrom', 'posixtime');
        end
        
        function Y = get_SPO_sensorStatus(this)
            Y = get_Dgm_Struct_Variable(this, 'SPO', 'Common', 'sensorStatus');
        end
        
        
        
        %% dgmCPO
        
        function Y = get_CPO_PosFixQuality_m(this)
            Y = get_Dgm_Struct_Variable(this, 'CPO', 'DataSensor', 'posFixQuality_m');
        end
        
        function Y = get_CPO_CorrectedLat_deg(this)
            Y = get_Dgm_Struct_Variable(this, 'CPO', 'DataSensor', 'correctedLat_deg');
        end
        
        function Y = get_CPO_CorrectedLon_deg(this)
            Y = get_Dgm_Struct_Variable(this, 'CPO', 'DataSensor', 'correctedLon_deg');
        end
        
        function Y = get_CPO_SpeedOverGround_mPerSec(this)
            Y = get_Dgm_Struct_Variable(this, 'CPO', 'DataSensor', 'speedOverGround_mPerSec');
        end
        
        function Y = get_CPO_CourseOverGround_deg(this)
            Y = get_Dgm_Struct_Variable(this, 'CPO', 'DataSensor', 'courseOverGround_deg');
        end
        
        function Y = get_CPO_EllipsoidHeightReRefPoint_m(this)
            Y = get_Dgm_Struct_Variable(this, 'CPO', 'DataSensor', 'ellipsoidHeightReRefPoint_m');
        end
        
        function Y = get_CPO_PosDataFromSensor(this)
            Y = get_Dgm_Struct_Variable_CellArray(this, 'CPO', 'DataSensor', 'posDataFromSensor');
        end
        
        function Y = get_CPO_Datetime(this)
            seconds = get_Dgm_Struct_Variable(this, 'CPO', 'DataSensor', 'timeFromSensor_sec');
            nanosec = get_Dgm_Struct_Variable(this, 'CPO', 'DataSensor', 'timeFromSensor_nanosec');
            X  = double(seconds) + (double(nanosec) / 1e9);
            Y = datetime(X, 'ConvertFrom', 'posixtime');
        end
        
        function Y = get_CPO_sensorStatus(this)
            Y = get_Dgm_Struct_Variable(this, 'CPO', 'Common', 'sensorStatus');
        end
        
        
        
        %% dgmSKM
        
        function Y = get_SKM_DataType(this, structName1, structName2, fieldName, varargin)
            
            [varargin, sub] = getPropertyValue(varargin, 'sub', []); %#ok<ASGLU>
            
            if isempty(sub)
                sub = 1:numel(this.sDataKmAll.dgmSKM);
            end
            
            Y = [];
            for kSub=1:numel(sub)
                k = sub(kSub);
                X = this.sDataKmAll.dgmSKM(k).SKMSamples;
                Temp = arrayfun(@(x) (x.(structName1).(structName2).(fieldName)), X);
                Y = [Y Temp]; %#ok<AGROW>
            end
            Y = Y';
        end
        
        function Y = get_SKM_DelayedHeave_m(this, varargin)
            Y = get_Dgm_Struct1_Struct2_Variable(this, 'SKM', 'SKMSamples', 'KMdelayedHeave', 'delayedHeave_m', varargin{:});
        end
        
        function Y = get_SKM_Datetime(this, varargin)
            [varargin, sub] = getPropertyValue(varargin, 'sub', []); %#ok<ASGLU>

            NumSamplesArray = get_SKM_NumSamplesArray(this, 'sub', sub);
            
            seconds = get_Dgm_Struct1_Struct2_Variable(this, 'SKM', 'SKMSamples', 'KMBinary', 'time_sec');
            nanosec = get_Dgm_Struct1_Struct2_Variable(this, 'SKM', 'SKMSamples', 'KMBinary', 'time_nanosec');
            X  = double(seconds) + (double(nanosec) / 1e9);
            if ~isempty(sub)
                X = X(sub);
            end
            
            Y = NaN(sum(NumSamplesArray), 1);
            kDeb = 1;
            for k=1:(length(X)-1)
                kFin = kDeb + NumSamplesArray(k) - 1;
                Y(kDeb:kFin) = interp1([1 NumSamplesArray(k)+1], X([k k+1]), 1:NumSamplesArray(k));
                kDeb = kDeb + NumSamplesArray(k);
            end
            kFin = kDeb + NumSamplesArray(end) - 1;
            if length(X) == 1 % Ajout JMA le 17/10/2022
                Y(kDeb:kFin) = X;
            else
                deltaX = (X(end)-X(end-1)) / NumSamplesArray(end);
                Y(kDeb:kFin) = interp1([1 NumSamplesArray(k)+1], [X(end) X(end)+NumSamplesArray(k)*deltaX], 1:NumSamplesArray(k));
            end
            Y = datetime(Y, 'ConvertFrom', 'posixtime');
        end
        
        function Y = get_SKM_DelayedHeave_Datetime(this, varargin)
            [varargin, sub] = getPropertyValue(varargin, 'sub', []); %#ok<ASGLU>
            
            seconds = get_Dgm_Struct1_Struct2_Variable(this, 'SKM', 'SKMSamples', 'KMdelayedHeave', 'time_sec');
            nanosec = get_Dgm_Struct1_Struct2_Variable(this, 'SKM', 'SKMSamples', 'KMdelayedHeave', 'time_nanosec');
            X  = double(seconds) + (double(nanosec) / 1e9);
            Y = datetime(X, 'ConvertFrom', 'posixtime');
            %             Y = duration(0, 0, X);
            if ~isempty(sub)
                Y = Y(sub);
            end
        end
        
        function Y = get_SKM_NorthAcceleration(this, varargin)
            Y = get_SKM_DataType(this, 'KMBinary', 'Acc', 'northAcceleration', varargin{:});
        end
        
        function Y = get_SKM_EastAcceleration(this, varargin)
            Y = get_SKM_DataType(this, 'KMBinary', 'Acc', 'eastAcceleration', varargin{:});
        end
        
        function Y = get_SKM_DownAcceleration(this, varargin)
            Y = get_SKM_DataType(this, 'KMBinary', 'Acc', 'downAcceleration', varargin{:});
        end
        
        function Y = get_SKM_LatitudeError_m(this, varargin)
            Y = get_SKM_DataType(this, 'KMBinary', 'Err', 'latitudeError_m', varargin{:});
        end
        
        function Y = get_SKM_EllipsoidHeightError_m(this, varargin)
            Y = get_SKM_DataType(this, 'KMBinary', 'Err', 'ellipsoidHeightError_m', varargin{:});
        end
        
        function Y = get_SKM_RollError_deg(this, varargin)
            Y = get_SKM_DataType(this, 'KMBinary', 'Err', 'rollError_deg', varargin{:});
        end
        
        function Y = get_SKM_PitchError_deg(this, varargin)
            Y = get_SKM_DataType(this, 'KMBinary', 'Err', 'pitchError_deg', varargin{:});
        end
        
        function Y = get_SKM_HeadingError_deg(this, varargin)
            Y = get_SKM_DataType(this, 'KMBinary', 'Err', 'headingError_deg', varargin{:});
        end
        
        function Y = get_SKM_HeaveError_m(this, varargin)
            Y = get_SKM_DataType(this, 'KMBinary', 'Err', 'heaveError_m', varargin{:});
        end
        
        function Y = get_SKM_VelNorth(this, varargin)
            Y = get_SKM_DataType(this, 'KMBinary', 'Vel', 'velNorth', varargin{:});
        end
        
        function Y = get_SKM_VelEast(this, varargin)
            Y = get_SKM_DataType(this, 'KMBinary', 'Vel', 'velEast', varargin{:});
        end
        
        function Y = get_SKM_VelDown(this, varargin)
            Y = get_SKM_DataType(this, 'KMBinary', 'Vel', 'velDown', varargin{:});
        end
        
        function Y = get_SKM_RollRate(this, varargin)
            Y = get_SKM_DataType(this, 'KMBinary', 'Rates', 'rollRate', varargin{:});
        end
        
        function Y = get_SKM_PitchRate(this, varargin)
            Y = get_SKM_DataType(this, 'KMBinary', 'Rates', 'pitchRate', varargin{:});
        end
        
        function Y = get_SKM_YawRate(this, varargin)
            Y = get_SKM_DataType(this, 'KMBinary', 'Rates', 'yawRate', varargin{:});
        end
        
        function Y = get_SKM_Roll_deg(this, varargin)
            Y = get_SKM_DataType(this, 'KMBinary', 'Attitude', 'roll_deg', varargin{:});
        end
        
        function Y = get_SKM_Pitch_deg(this, varargin)
            Y = get_SKM_DataType(this, 'KMBinary', 'Attitude', 'pitch_deg', varargin{:});
        end
        
        function Y = get_SKM_Heading_deg(this, varargin)
            Y = get_SKM_DataType(this, 'KMBinary', 'Attitude', 'heading_deg', varargin{:});
        end
        
        function Y = get_SKM_Heave_m(this, varargin)
            Y = get_SKM_DataType(this, 'KMBinary', 'Attitude', 'heave_m', varargin{:});
        end
        
        function Y = get_SKM_Latitude_deg(this, varargin)
            Y = get_SKM_DataType(this, 'KMBinary', 'Position', 'latitude_deg', varargin{:});
        end
        
        function Y = get_SKM_Longitude_deg(this, varargin)
            Y = get_SKM_DataType(this, 'KMBinary', 'Position', 'longitude_deg', varargin{:});
        end
        
        function Y = get_SKM_EllipsoidHeight_m(this, varargin)
            Y = get_SKM_DataType(this, 'KMBinary', 'Position', 'ellipsoidHeight_m', varargin{:});
        end
        
        
        function Y = get_SKM_Status(this, varargin)
            Y = get_Dgm_Struct1_Struct2_Variable(this, 'SKM', 'SKMSamples', 'KMBinary', 'status', varargin{:});
        end
        
        function Y = get_SKM_NumSamplesArray(this, varargin)
            Y = get_Dgm_Struct_Variable(this, 'SKM', 'SKMInfo', 'numSamplesArray', varargin{:});
            Y = double(Y);
        end
        
        function subSensorStatus = get_SKM_selectionSensorStatus(this, OutputType)
%             Y = get_SKM_SensorStatus(this);
            Y = get_Dgm_Struct_Variable(this, 'SKM', 'SKMInfo', 'sensorStatus');
            switch OutputType
                case -1 %
                    %                     sub = find(mod(Y,2) == 1);
                    subSensorStatus = find(Y == 1);
                case 0
                    subSensorStatus = 1:length(Y);
                otherwise
                    SS = get_Dgm_Struct_Variable(this, 'SKM', 'SKMInfo', 'sensorSystem');
                    subSensorStatus = find(SS == sensorSystem);
            end
        end
        
        function Y = get_SKM_SensorStatus(this, varargin)
            [varargin, sub] = getPropertyValue(varargin, 'sub', []); %#ok<ASGLU>
            
            X = get_Dgm_Struct_Variable(this, 'SKM', 'SKMInfo', 'sensorStatus', 'sub', sub);
            
            NumSamplesArray = get_SKM_NumSamplesArray(this, 'sub', sub);
            
            Y = NaN(sum(NumSamplesArray), 1);
            kDeb = 1;
            for k=1:length(X)-1
                kFin = kDeb + NumSamplesArray(k) - 1;
                Y(kDeb:kFin) = X(k);
                kDeb = kDeb + NumSamplesArray(k);
            end
        end
        
        function Y = get_SKM_SensorSystem(this, varargin)
            Y = get_Dgm_Struct_Variable(this, 'SKM', 'SKMInfo', 'sensorSystem', varargin{:});
        end
        
        function Y = get_SKM_SensorInputFormat(this, varargin)
            Y = get_Dgm_Struct_Variable(this, 'SKM', 'SKMInfo', 'sensorInputFormat', varargin{:});
        end
        
        
        
        %% dgmFCF
        
        function Y = get_FCF_BsCalibrationFile(this, varargin)
            [varargin, OutputDir] = getPropertyValue(varargin, 'OutputDir', []); %#ok<ASGLU>
            
            if ~isfield(this.sDataKmAll, 'dgmFCF')
                Y = [];
                return
            end
            
            Y = (this.sDataKmAll.dgmFCF.FCommon.bsCalibrationFile)';
            
            [nomDir, nomFic] = fileparts(this.Filename);
            if ~isempty(OutputDir)
                nomDir = OutputDir;
            end
            BsCorrFilename = fullfile(nomDir, [nomFic '_dgmFCF_CalibFile.txt']);
            fid = fopen(BsCorrFilename, 'w+t');
            fprintf(fid, '%s\n', Y);
            fclose(fid);
        end
        
        
    end
    
    
    methods  (Access = 'private')
        
        %% Lecture généralisée d'une donnée de la forme this.sDataKmAll.(dgmName)(k).(fieldName)
        
        function Y = get_Dgm_Variable(this, dgmName, fieldName, varargin)
            %  %   Structure of the data : a.sDataKmAll.dgmSVP.numSamples
            %  %   Function call         : Y = get_Dgm_Variable(this, 'SVP', 'numSamples');
            %  %   Size of output        : [1 1]
            
            [varargin, sub] = getPropertyValue(varargin, 'sub', []); %#ok<ASGLU>
            
            dgmName = ['dgm' dgmName];
            if isempty(sub)
                sub = 1:numel(this.sDataKmAll.(dgmName));
            end
            
            for kSub=numel(sub):-1:1
                k = sub(kSub);
                X = this.sDataKmAll.(dgmName)(k);
                Y(kSub,:) = arrayfun(@(x) (x.(fieldName)), X);
            end
        end
        
        
        %% Lecture généralisée d'une donnée de la forme this.sDataKmAll.(dgmName)(k).(structName).(fieldName)
        
        function Y = get_Dgm_Struct_Variable(this, dgmNameDim1, structName, fieldName, varargin)
            %  %   Structure of the data : a.sDataKmAll.dgmMRZ(dim=84).Sounding(dim=800).txSectorNumber
            %  %   Function call         : Y = get_Dgm_Struct_Variable(this, 'MRZ', 'Sounding', 'txSectorNumber');
            %  %   Size of output        : [84 800]
            
            [varargin, sub] = getPropertyValue(varargin, 'sub', []); %#ok<ASGLU>
            
            dgmName = ['dgm' dgmNameDim1];
            if isempty(sub)
                sub = 1:numel(this.sDataKmAll.(dgmName));
            end
            
            for kSub=numel(sub):-1:1
                k = sub(kSub);
                X = this.sDataKmAll.(dgmName)(k).(structName);
                Y(kSub,:) = arrayfun(@(x) (x.(fieldName)), X);
            end
        end
        
        
        
        %% Lecture généralisée d'une donnée de la forme this.sDataKmAll.(dgmName)(k).(structName1).(fieldName).(structName2)
        
        function Y = get_Dgm_Struct1_Variable_Struct2(this, dgmNameDim1, structNameDim2, structName1, fieldName, varargin)
            %  % Example :
            %  %   Structure of the data : a.sDataKmAll.dgmMRZ(dim=84).Sounding(dim=800).GeoDepthPts.z_reRefPoint_m
            %  %   Function call         : Y = get_Dgm_Struct1_Variable_Struct2(this, 'MRZ', 'Sounding', 'GeoDepthPts', 'z_reRefPoint_m');
            %  %   Size of output        : [84 800]
            
            [varargin, sub] = getPropertyValue(varargin, 'sub', []); %#ok<ASGLU>
            
            dgmName = ['dgm' dgmNameDim1];
            if isempty(sub)
                sub = 1:numel(this.sDataKmAll.(dgmName));
            end
            
            for kSub=numel(sub):-1:1
                k = sub(kSub);
                X = this.sDataKmAll.(dgmName)(k).(structNameDim2);
                Y(kSub,:) = arrayfun(@(x) (x.(structName1).(fieldName)), X);
            end
        end
        
        
        %% Lecture généralisée d'une donnée de la forme this.sDataKmAll.(dgmName)(k).(structName1).(structName2).(fieldName)
        
        function Y = get_Dgm_Struct1_Struct2_Variable(this, dgmNameDim1, structName1, structName2, fieldName, varargin)
            %  %   Structure of the data : a.sDataKmAll.dgmMRZ(dim=84).PingInfo.TxSectorInfo.numTxSectors
            %  %   Function call         : Y = get_Dgm_Struct1_Struct2_Variable(this, 'MRZ', 'PingInfo', 'TxSectorInfo', 'numTxSectors');
            %  %   Size of output        : [84 1]
            
            [varargin, sub] = getPropertyValue(varargin, 'sub', []); %#ok<ASGLU>
            
            dgmName = ['dgm' dgmNameDim1];
            if isempty(sub)
                sub = 1:numel(this.sDataKmAll.(dgmName));
            end
            
            for kSub=numel(sub):-1:1
                k = sub(kSub);
                X = this.sDataKmAll.(dgmName)(k).(structName1).(structName2);
                Y(kSub,:) = arrayfun(@(x) (x.(fieldName)), X);
            end
        end
        
        
        %% Lecture généralisée d'un cell Array de la forme this.sDataKmAll.(dgmName)(k).(structName1).(fieldName)
        
        function Y = get_Dgm_Struct_Variable_CellArray(this, dgmNameDim1, structName, fieldName, varargin)
            %  % Example :
            %  %   Structure of the data : a.sDataKmAll.dgmSCL(dim=528).DataSensor.dataFromSensor
            %  %   Function call         : Y = get_Dgm_Struct_Variable_CellArray(this, 'SCL', 'DataSensor', 'dataFromSensor');
            %  %   Size of output        : [528 1];
            
            [varargin, sub] = getPropertyValue(varargin, 'sub', []); %#ok<ASGLU>
            
            dgmName = ['dgm' dgmNameDim1];
            if isempty(sub)
                sub = 1:numel(this.sDataKmAll.(dgmName));
            end
            
            for kSub=numel(sub):-1:1
                k = sub(kSub);
                Y{kSub,1} = this.sDataKmAll.(dgmName)(k).(structName).(fieldName);
            end
        end
        
        
        %% dgmMWC.RxBeamData
        
        function Y = get_DgmMWC_RxBeamData(this, iEchogram, fieldName)
            for k=numel(this.sDataKmAll.dgmMWC(iEchogram).RxBeamData):-1:1
                Y(k,:) = arrayfun(@(x) (x.(fieldName)), this.sDataKmAll.dgmMWC(iEchogram).RxBeamData(k));
            end
        end
    end
end
