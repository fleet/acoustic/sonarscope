function ReflectivityByBeam = CorSpec(ReflectivityByBeam, deltaBS, r, rnKM, Tetac0, flagAlgoJMA)

if flagAlgoJMA % Test JMA le 03/05/2020 pour données EM3002D
%     rn = max(rnKM, min(r(r > 1))); % Ajout JMA le 03/05/2020
    rn = min(r(r > 1)); % Ajout JMA le 03/05/2020
else
    rn = rnKM;
end

if isempty(rn)
    ReflectivityByBeam(:) = NaN;
    return
end

rc0 = rn / cosd(Tetac0);
r(r < rn) = rn;
% FigUtils.createSScFigure; PlotUtils.createSScPlot(r); grid on; hold on; PlotUtils.createSScPlot([1 length(r)], [rn rn], 'k')
X = zeros(size(r));
sub = find((r >= rn) & (r < rc0));


if flagAlgoJMA % Test JMA le 03/05/2020 pour données EM3002D
    alpha = rn ./r;
    threshold = 0.97;
    subBDA = find(alpha > threshold);
    RelecSpeculaire = ReflectivityByBeam(subBDA);
    RelecSpeculaireNew = RelecSpeculaire;
    nbSamples = length(RelecSpeculaire);
    n = 2;
    for k= (n+1):nbSamples-n
        subn = (k-n):(k+n);
        Max = max(RelecSpeculaire(subn));
        if (Max - RelecSpeculaire(k)) > 3
            RelecSpeculaireNew(k) = Max;
        end
    end
    
    %     ReflectivityByBeam(subBDA) = RelecSpeculaireNew;
    alpha = alpha(subBDA);
    alpha = (alpha - threshold) / (1 - threshold);
    
    %{
    figure;
    subplot(2,1,1); plot(RelecSpeculaire, 'k'); grid on; hold on; plot(RelecSpeculaireNew, 'b'); plot(alpha .* RelecSpeculaireNew + (1-alpha) .* ReflectivityByBeam(subBDA), 'r')
    subplot(2,1,2); plot(alpha); grid on
    %}
    
    ReflectivityByBeam(subBDA) = alpha .* RelecSpeculaireNew + (1-alpha) .* ReflectivityByBeam(subBDA);
    % FigUtils.createSScFigure; PlotUtils.createSScPlot(subBDA, RelecSpeculaire, 'b'); grid on; hold on; PlotUtils.createSScPlot(subBDA, RelecSpeculaireNew, 'r')
end


rn = min(r(sub)); % Ajout JMA le 03/05/2020

X(sub) = 1 - sqrt(((r(sub)) - rn) / (rc0 - rn));
% FigUtils.createSScFigure; PlotUtils.createSScPlot(X); grid on;

if size(deltaBS,2) == 1
    Correction = deltaBS * X;
else
    nbBeams = length(ReflectivityByBeam);
    n = nbBeams / 2;
    subBab = 1:n;
    subTri = n+1:nbBeams;
    Correction(subBab) = deltaBS(1) * X(subBab);
    Correction(subTri) = deltaBS(2) * X(subTri);
end
ReflectivityByBeam = ReflectivityByBeam + Correction;
