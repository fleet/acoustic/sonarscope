function QF = CorrectSupposedErrorQFComputationByKongsberg(QF, PL, WI)

CW = (WI == 0);
minPL = min(PL, [], 2);

correction = 0.5 * log10(PL ./ minPL);

% SonarScope(QF + correction*1.5)

QF(CW) = QF(CW) + correction(CW) * 2; %1.5;
