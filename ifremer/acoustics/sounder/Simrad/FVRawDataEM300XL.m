% Formation de voies EM300
%
% Syntax
%   FVRawDataEM300XL(nomFic)
% 
% Input Arguments 
%   nomFic : Nom du fichier de donnees brutes
%
% Examples
%   nomFic = getNomFicDatabase('ST163821.00')
%   FVRawDataEM300XL(nomFic)
%
% See also Authors
% Authors : XL
% VERSION  : $Id: FVRawDataEM300XL.m,v 1.1 2002/04/09 12:37:42 gmenexia Exp gmenexia $
%-----------------------------------------------------------------------

function FVRawDataEM300XL(nomFic)

%**********************************************************************************************
% DEPOUILLEMENT DE DONNEES BRUTES DE l'EM300
%  MISE AU POINT DE TRAITEMENTS D'ANTENNE PANORAMIQUE

% ------------------
% Lecture du fichier

[PingCounter, Nstave, Nech, TXMode, Rawd, Tvgc, Tvgu] = lecFicStaves(nomFic);
% figure
%subplot(2,1,1); plot(Tvgc); grid on; set(gca, 'XLim', [1 Nech]); title('Tvgc');
% subplot(2,1,2); plot(Tvgu); grid on; set(gca, 'XLim', [1 Nech]); title('Tvgu');

%Affichage de l'image
%figure;
RawddB = 20*log10(abs(Rawd));
figure; imagesc((RawddB)); title('RAW DATA dB');colorbar;
drawnow

% ----------------
% Compensation TVG

NombrePt = size(Tvgu);
NombrePoint = NombrePt(2);
Tvgunat = 10.^(Tvgu./2000);
Tvgnewnat = ([1:NombrePoint]*1500/2/4509).^2;
SumRawdc = zeros(1,NombrePoint);
SumRawd = zeros(1,NombrePoint);

for i=1:Nstave
    Rawdc(i,:)    = Rawd(i,:)./Tvgunat;
    SumRawdc(1,:) = SumRawdc(1,:)+Rawdc(i,:);
    SumRawd(1,:)  = SumRawd(1,:)+Rawd(i,:);
end

% SumRawdcdB=20*log10(abs(SumRawdc));
% SumRawddB=20*log10(abs(SumRawd));
%
% figure;plot(SumRawddB);title('Average/staves Raw data dB with TVG');
% figure;plot(SumRawdcdB);title('Average/staves Raw data dB TVG removed');

RawdcdB = 20*log10(abs(Rawdc));
figure;
imagesc(RawdcdB,[-30 20]); title('RAW DATA dB TVG removed');
colorbar;
drawnow


% figure;plot(RawdcdB(1,:));title('Average/staves Raw data dB TVG removed');
% figure;plot(abs(Rawd(1,:)));title('Average/staves Raw data  TVG removed');

MoyStave=zeros(1,64);
for i=1:64
    MoyStave(i) = mean(abs(Rawd(i,1000:2200)));
    %MoyStave(i)= abs(Rawd(i,763));
end

MoyGen = mean(MoyStave);
MoyStave = MoyStave/MoyGen;
hold on; plot(MoyStave,'k');

% MoyAngStave=zeros(1,64);
% for i=1:64
% MoyAngStave(i)=(angle(Rawd(i,783)));
% ExpMoyAng(i)=exp(complex(0,-1)*MoyAngStave(i));
% end;
% hold on;plot(MoyAngStave,'m');

for i=1:64
    Rawdc(i,:)=Rawdc(i,:) ./ MoyStave(i);
end


% -----------------------
% Definition des secteurs

[AngDebSec, FrSec, AngSec, OuvSec] = getParSecEm300( TXMode ); %#ok<ASGLU>

% --------------------------------
% Calcul de la position des staves

Posstv = calPosStavesEm300;
% TabStave = 1:Nstave;						% ordre des staves de l'antenne

% -------------------
% Formation des voies

Angles = 73:-1:-73;
nbFais = length(Angles);
indexAng = ones(1, nbFais);
for i=1:nbFais
    for j=1:length(AngDebSec)
        if Angles(i) < AngDebSec(j)
            indexAng(i) = j+1;
        end
    end
end
FrFais = FrSec(indexAng) * 1000;
SinAng = sin(Angles * pi / 180);
% CosAng = cos(Angles * pi / 180);
Cel = 1510;
% alfa = 6;         % coef. abs. dB/km
coefk = 2 * pi / Cel;
Fech = 4509;

%**********************************************************************************************
% FORMATION DE VOIES PAR DEPHASAGE ET/OU DECALAGE
% Calcul pr�liminaire des dephasages et des exponentielles complexes et des d�calages temporels
%**********************************************************************************************

Phase = zeros(nbFais, Nstave);
expjph = zeros(nbFais, Nstave);
for i = 1 : nbFais
    Phase(i, :) = coefk * FrFais(i) * Posstv * SinAng(i);
    expjph(i, :) = exp( complex(0, 1) * Phase(i,:));
end

%**********************************************************************************************
% S�lection de la portion temporelle utile

Ndeb = 1;
Nfin = Nech-1;
Rawdu = Rawdc(:,Ndeb:Nfin);					% donn�es brutes sur les staves, limit�es aux �chantillons temporels utiles
Nechu = Nfin - Ndeb + 1;
%Rawdust = complex(zeros(1,Ndemiant));		% donn�e brute sur les staves � un instant donn�

LongAnt = Posstv(64) - Posstv(1);

% Tabstave = 1:Nstave;
Pond = sin(pi*(Posstv-Posstv(1))/LongAnt).^2;
Pond(:) = 1;
CoefFV = sum(Pond);

for i=1:Nstave
    Rawdu(i,:) = Rawdu(i,:)*Pond(i);
end

% RawdudB=20*log10(abs(Rawdu));
% figure;
% imagesc(RawdudB); title('RAW DATA compens� en dB');
% colorbar;

%**********************************************************************************************
% Re-modulation des voies form�es en phase
%**********************************************************************************************

for i = 1:Nechu
    ph = 2 * pi * 31500 * i / Fech;
    Rawdu(:,i) = Rawdu(:,i) * exp(-complex(0,1)*ph);
end

%**********************************************************************************************
% 			FV en d�phasage
%**********************************************************************************************

SigVoie = complex(zeros(nbFais, Nechu));
hw = create_waitbar( 'Tu peux aller fumer ta clope Geraldine.', 'N', Nechu);
for i=1:Nechu
    my_waitbar(i, Nechu, hw)
    for j =  nbFais:-1:1
        %    	   SigVoie(j, i) = expjph(j, TabStave(:)) * Rawdu(TabStave(:), i);
        SigVoie(j, i) = expjph(j, :) * Rawdu(:, i);
    end
end
my_close(hw)

SigVoie = SigVoie / CoefFV;

%**********************************************************************************************
% Demodulation des voies form�es en phase
%**********************************************************************************************

SigVoieDemodulee = zeros(nbFais, Nechu);
expjph2 = complex(zeros(nbFais, Nechu));
dfFrFais = FrFais ;
for j = 1:nbFais
    for i = 1:Nechu
        ph = 2 * pi * dfFrFais(j) * i / Fech;
        expjph2(j, i) = exp( complex(0, -1) * ph);
        SigVoieDemodulee(j, i) = expjph2(j, i) * SigVoie(j, i);
    end
end


%**********************************************************************************************
% Filtrage des voies form�es en phase
%**********************************************************************************************

SigVoiePh = zeros( nbFais, Nechu);
for j = 1:nbFais
    SigVoiePh(j, :) = filter(ones(1,6)/6, [1 0 0 0 0 0], SigVoieDemodulee(j, :));
end


SigVoiePhdB = 20*log10(abs(SigVoiePh));

figure;
imagesc(SigVoiePhdB, [-50 0]);
title('Voie formee en phase (dB) non ponderee');colorbar;
drawnow
