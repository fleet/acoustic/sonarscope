% Lecture et fusion des donn�es contenus dans un fichier CSV issu de
% CASINO et summary SSC
%
% Syntax
%   FusionCasinoCSV(fileCasino, fileSSC)
%
% Input Arguments
%   chemins complets des fichiers � lire
%
% Output Arguments
%   
% EXEMPLE :
%   FusionCasinoCSV('X:\FUTUNA11_LEG1\Carla_acoustique\futuna11_casino_dd_20111120.csv',...
%                 'X:\FUTUNA11_LEG1\Carla_acoustique\FUTUNA2011_Summary_20111120.csv');
% Remarks :
%
% Authors : GLT
 %-------------------------------------------------------------------------------
 
function FusionCasinoCSV(fileCasino, fileSSC)

fid = fopen(fileCasino, 'rt');
firstline = fgetl(fid);
[champs{1}, remain] = strtok(firstline, ';');
while ~isempty(champs{end})
    [champs{end+1}, remain] = strtok(remain, ';'); %#ok<AGROW>
end
champs = champs(1:end-1)';
fclose(fid);

%Lecture des dates Casino
fid = fopen(fileCasino, 'rt');
data = textscan(fid, '%s %s %*[^\n]', 'delimiter', ';', 'HeaderLines', 1);
fclose(fid);
date = datenum( strcat(data{1}, data{2}), 'dd/mm/yyyyHH:MM:SS');

% Lecture des dates SSC
fid = fopen(fileSSC, 'rt');
allNames = textscan(fid, '%s %*[^\n]', 'delimiter', ',', 'HeaderLines', 1);
fclose(fid);
for iAll = 1 : length(allNames{1})
    [~,filename] = fileparts(allNames{1}{iAll});
    C = textscan(filename, '%4f_%4f%2f%2f_%2f%2f%2f_%s'); % 0417_20111120_063417_AtalanteEM122.all
    numAll(iAll) = C{1}; %#ok<AGROW>
    dateAll(iAll) = datenum( [C{2:7}] ); %#ok<AGROW>
    [~, iAllCasino(iAll)] = min(abs(date-dateAll(iAll))); %#ok<AGROW>
    if dateAll(iAll) < date(iAllCasino(iAll))
        iAllCasino(iAll) = iAllCasino(iAll) - 1; %#ok<AGROW> % pour obtenir la date juste avant l'acquisition SMF
    end
end

% Est ce qu'il y a intersection ?
if min(dateAll) > min(date) || max(dateAll) < max(date)
          
    %quelles donn�es veux-t-on exporter ?
    str1 = 'S�l�ctionner les variable � exporter :';
    str2 = 'TODO';
    [col, ok] = listdlg('PromptString', Lang(str1,str2),...
        'SelectionMode', 'Multiple',...
        'ListString',    champs,...
        'ListSize',      [300 500],...
        'InitialValue',  [1 2 3 4 5 7 9 11 12 13 39 43 47 48 66 70 71 87 163 164] );
    
    % Cr�er d'autre champs ?
    choice = 'Oui';
    newChamps = {};
    while strcmp(choice, 'Oui')
        str = sprintf('Voulez-vous cr�er d''autres champs ?\n(Ils seront plac�s en d�but)');
        [rep, flag] = my_questdlg(str, 'Init', 2);
        if ~flag
            return
        end
        
        if rep == 1
            prompt    = {'Nom du champ :'};
            dlg_title = 'Entrer le nom du champ';
            num_lines = 1;
            def       = {'mon champs'};
            answer    = inputdlg(prompt,dlg_title,num_lines,def);
            if ~isempty(answer)
                newChamps{end+1} = answer{1}; %#ok<AGROW>
            end
        else
            break
        end
    end
    
    % Filtrer les acquisitions auto --> ok
    % garder les donn�es lorsqu'il y a du SMF --> ok
    % Ajouter le num�ro des .all --> ok
    
    if ok
        % Etablissement du format de lecture
        frmt = repmat({'%*s'}, 1, length(champs));
        % tri des champs � garder
        frmt(col) = {'%s'};        
        frmt = strcat(frmt{:});
    else
        return
    end
    
    [path, name, ext] = fileparts(fileCasino);
    fileOut = fullfile(path, [name '+SSC' ext]);
    fidCasino   = fopen( fileCasino, 'rt' );
    fidSSC      = fopen( fileSSC, 'rt' );
    fido        = fopen( fileOut, 'wt');
    
    % Lecture header casino
    headerCasino = textscan(fidCasino, frmt, 1, 'delimiter', ';', 'CollectOutput', 1);
    fgetl(fidCasino); % pour lire le caract�re \n
    headerCasino = strcat(headerCasino{1}, ';');
    
    % Lecture header SSC
    headerSSC    = fgetl(fidSSC);
    
    % Ajout nouveaux champs
    nNewChamps  = length(newChamps);
    if nNewChamps > 0
        newChamps   = strcat(newChamps, ';');
        newChamps   = [newChamps{:}];
    else
        newChamps = '';
    end
    
    header = [newChamps, [headerCasino{:}], 'num .ALL;', strrep(headerSSC, ',', ';')];
    
    lignevideDeb = repmat(';', 1, nNewChamps);
    lignevideFin = repmat(';', 1, numel(strfind(headerSSC, ',')) + 1 );
    
    fprintf(fido, '%s\n', header);
    
    iline   = 1;
    iAll    = 1;    
    while ~feof(fidCasino)
        iline   = iline + 1;        
        line    = fgetl(fidCasino);
        lineOk = textscan(line, frmt, 1, 'delimiter', ';', 'CollectOutput', 1);
        lineOk = strcat(lineOk{1}, ';');
        
        if any(iline == iAllCasino) % juste apr�s on a un �vennement SMF
            line2add = [ sprintf('%d', numAll(iAll)), ';', strrep(fgetl(fidSSC), ',', ';') ];
            iAll = iAll + 1;
            fprintf(fido, '%s%s%s\n', lignevideDeb, [lineOk{:}], line2add);
            % A-t-on plusieurs fichiers � la m�me date ?
            if sum(iline == iAllCasino) > 1
                iMemeAll = sum(iline == iAllCasino) - 1;
                while iMemeAll > 0
                    line2add = [ sprintf('%d', numAll(iAll)), ';', strrep(fgetl(fidSSC), ',', ';') ];
                    iAll = iAll + 1;
                    fprintf(fido, '%s%s%s\n', lignevideDeb, [lineOk{:}], line2add);   
                    iMemeAll = iMemeAll - 1;
                end
            end
        else
            line2add = lignevideFin;
            if contains(line ,'ACQAUTO')
                continue
            else
                fprintf(fido, '%s%s%s\n', lignevideDeb, [lineOk{:}], line2add);
            end
        end              
    end
else
    warndlg('Pas d''intersection temporelle entre les deux fichiers','Attention')
end

fclose('all');
helpdlg('Termin� !')
