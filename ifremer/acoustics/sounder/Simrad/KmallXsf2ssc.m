function [flag, flagRTK, nomFicWc, EmModel, SystemSerialNumber, Version, SousVersion] = KmallXsf2ssc(nomFic, varargin)

global IfrTbxResources %#ok<GVMIS>
global IfrTbx %#ok<GVMIS>
global isUsingParfor %#ok<GVMIS>
global IdentAlgoSnippets2OneValuePerBeam %#ok<GVMIS>
global useParallel %#ok<GVMIS>
global logFileId %#ok<GVMIS>
 
[varargin, logFileId]   = getPropertyValue(varargin, 'logFileId',   logFileId);
[varargin, useParallel] = getPropertyValue(varargin, 'useParallel', useParallel);

if isempty(IfrTbxResources)
    loadVariablesEnv
end

if isempty(isUsingParfor)
    isUsingParfor = 0;
elseif isUsingParfor == 1
    loadVariablesEnv
    %     fprintf('IfrTbx = "%s"\n', IfrTbx);
    %     fprintf('IfrTbxResources = "%s"\n', IfrTbxResources);
end

if isempty(IdentAlgoSnippets2OneValuePerBeam)
    IdentAlgoSnippets2OneValuePerBeam = 2;
end

% [varargin, RDL]             = getPropertyValue(varargin, 'RDL',             0);
[varargin, isUsingParfor]   = getPropertyValue(varargin, 'isUsingParfor',   isUsingParfor);
[varargin, IfrTbxResources] = getPropertyValue(varargin, 'IfrTbxResources', IfrTbxResources);
[varargin, IfrTbx]          = getPropertyValue(varargin, 'IfrTbx',          IfrTbx); %#ok<ASGLU>

% FormatVersion = 20101118;

flagRTK            = 0;
nomFicWc           = [];
EmModel            = [];
SystemSerialNumber = [];
Version            = [];
SousVersion        = [];

%% Test d'existence du fichier

flag = exist(nomFic, 'file');
if flag ~= 2
    str1 = sprintf('Le fichier "%s" n''existe pas.', nomFic);
    str2 = sprintf('"%s" doesn not exist', nomFic);
    my_warndlg(Lang(str1,str2), 0, 'Tag', 'FichierAllNonTrouve');
    return
end

%% Test si le fichier est vide

if sizeFic(nomFic) == 0
    str1 = sprintf('Le fichier "%s" est vide. Je le renomme en  "xxx_all.bad"', nomFic);
    str2 = sprintf('"%s" is empty, I rename it in "xxx_all.bad" file.', nomFic);
    renameBadFile(nomFic, 'Message', Lang(str1,str2))
    flag = 0;
    return
end

%% Test de la longueur du nom de fichier

lenghtMax = 184;
if length(nomFic) > lenghtMax
    str1 = sprintf('Le nom du fichier "%s" est trop long, raccourcissez-le d''au moins %d caract�res', nomFic, length(nomFic)-lenghtMax);
    str2 = sprintf('The file name "%s" is too long, shorten it (at least %d characters)', nomFic, length(nomFic)-lenghtMax);
    flag = 0;
    my_warndlg(Lang(str1,str2), 1, 'Tag', 'FichierAllNonTrouve');
    return
end

%% Test si le fichier a d�j� �t� d�cod�

[flag, TypeCache] = all2ssc_checkIfAlreadyDecoded(nomFic); %#ok<ASGLU>
if flag
    flagRTK = ALL_isRTKAvailable(nomFic);
    [flag, Data] = SSc_ReadDatagrams(nomFic, 'Depth', 'XMLOnly', 1);
    if flag
        EmModel = Data.EmModel;
        [flag, varVal] = SSc_getSignalUnit(nomFic, 'Depth', 'SystemSerialNumber');
        if flag
            SystemSerialNumber = unique(varVal);
        end
    end
    Version = 'V2';
    return
% else
%     str1 = 'TODO';
%     str2 = sprintf('You must create at first a .xsf.nc file with GLOBE from %s\nPut it at the same place as the .kmall', nomFic);
%     my_warndlg(Lang(str1,str2), 0, 'Tag', 'MessageImportXSF', 'displayItEvenIfSSc', 1, 'TimeDelay', 60);
end

%% Lancement du d�codage

str1 = sprintf('"%s" d�codage.', nomFic);
str2 = sprintf('"%s" decode.', nomFic);
WorkInProgress(Lang(str1,str2))

[nomDirRacine, ~, Ext] = fileparts(nomFic);
nomDirCache = fullfile(nomDirRacine, 'SonarScope');
if ~exist(nomDirCache, 'dir')
    [flag, message] = mkdir(nomDirCache);
    if ~flag
        messageErreurFichier(nomDirCache, 'WriteFailure', 'Message', message);
        return
    end
end

switch Ext
    case '.kmall'
        nomFicXsf = strrep(nomFic, '.kmall', '.xsf.nc');
        if ~exist(nomFicXsf, 'file')

            % Ajout JMA le 18/06/2024 
            nomExe = 'C:\Program Files\sonarnetcdf-converter\sonarnetcdf-converter.exe';
            cmd = sprintf('!"%s" -in "%s" -out "%s"', nomExe, nomDirRacine, nomDirRacine);
            % fprintf('\n%s\n', cmd)
            try
                [~, msg] = dos(cmd(2:end));
                if ~exist(nomFicXsf, 'file')
                    str1 = sprintf('CONVERT_ALL.exe semble avoir rencontr� un probl�me sur le fichier %s\n%s', nomFic, msg);
                    str2 = sprintf('CONVERT_ALL.exe does not seem working for file %s\n%s', nomFic, msg);
                    my_warndlg(Lang(str1,str2), 0);
                    str1 = 'TODO';
                    str2 = sprintf('You must create at first a .xsf.nc file with GLOBE from %s\nPut it at the same place as the .kmall', nomFic);
                    my_warndlg(Lang(str1,str2), 0, 'Tag', 'MessageImportXSF', 'displayItEvenIfSSc', 1, 'TimeDelay', 60);
                    flag = 0;
                    return
                end
            catch %#ok<CTCH>
                str1 = 'TODO';
                str2 = sprintf('You must create at first a .xsf.nc file with GLOBE from %s\nPut it at the same place as the .kmall', nomFic);
                my_warndlg(Lang(str1,str2), 0, 'Tag', 'MessageImportXSF', 'displayItEvenIfSSc', 1, 'TimeDelay', 60);
                flag = 0;
                return
            end
        end
    otherwise
        my_breakpoint
end

%% Lecture int�grale du fichier

% try
%     tic
%     [flag, Hdf5Data] = Hdf5Utils.readGrpData(nomFicXsf, '/');
%     toc
% catch
%     strWrn = sprintf('%s could not be read in memory. I am trying to do it using memmapfile technology', nomFicXsf);
%     my_warndlg(strWrn, 0, 'Tag', 'MessageHDF5');
%     tic
    [flag, Hdf5Data] = Hdf5Utils.readGrpData(nomFicXsf, '/', 'Memmapfile', 1);
%     toc
% end
if ~flag 
    nomFic = nomFicXsf;
    nomFic = strrep(nomFic, '.xsf.nc', '.kmall');
    str1 = sprintf('Le fichier "%s" est vide. Je le renomme en  "xxx_all.bad"', nomFic);
    str2 = sprintf('"%s" is empty, I rename it in "xxx_all.bad" file.', nomFic);
    renameBadFile(nomFic, 'Message', Lang(str1,str2))
    return
end
try
    names1 = {Hdf5Data.Sonar.Beam_group1.Dim(:).name};
    kPing_time = find(contains(names1, 'ping_time'));
    names2 = {Hdf5Data.Sonar.Beam_group1.Bathymetry.Dim(:).name};
    kDetection = find(contains(names2, 'detection'));
catch
    nomFic = Hdf5Data.fileName;
    nomFic = strrep(nomFic, '.xsf.nc', '.kmall');
    str1 = sprintf('Le fichier "%s" est vide. Je le renomme en  "xxx_all.bad"', nomFic);
    str2 = sprintf('"%s" is empty, I rename it in "xxx_all.bad" file.', nomFic);
    renameBadFile(nomFic, 'Message', Lang(str1,str2))
    return
end
if isempty(kPing_time) || isempty(kDetection) ...
        || (Hdf5Data.Sonar.Beam_group1.Dim(kPing_time).value <= 3) ...
        || (Hdf5Data.Sonar.Beam_group1.Bathymetry.Dim(kDetection).value <= 3)
    flag = 0;
    nomFic = Hdf5Data.fileName;
    nomFic = strrep(nomFic, '.xsf.nc', '.kmall');
    str1 = sprintf('Le fichier "%s" est vide. Je le renomme en  "xxx_all.bad"', nomFic);
    str2 = sprintf('"%s" is empty, I rename it in "xxx_all.bad" file.', nomFic);
    renameBadFile(nomFic, 'Message', Lang(str1,str2))
    return
end

%% Cr�ation du fichier Netcdf

[flag, ncFileName] = SScCacheNetcdfUtils.createSScNetcdfFile(nomFic);
if ~flag
    return
end

%% Transfert des donn�es Runtime

[flag, XML, DataRuntimeXSF] = XSF.extractRuntime(Hdf5Data);
if ~flag
    return
end

flag = NetcdfUtils.XMLBin2Netcdf(XML, DataRuntimeXSF, ncFileName, 'Runtime');
if ~flag
    return
end

%% Datagrammes InstallationParameters

[flag, DataInstallParams] = XSF.extractInstallParameters(Hdf5Data);
if ~flag
    return
end

% DataInstallParams s'apparente a une structure XML avec ecriture uniquement de variables globales.
flag = NetcdfUtils.XMLBin2Netcdf(DataInstallParams, [], ncFileName, 'InstallationParameters');
if ~flag
    return
end

% TODO : fa�on tr�s maladroite de g�rer les 2 t�tes. Ceci est valid�
% uniquement pour EM2040 dual de la Thalia et EM2040 Dual Kongsberg
% Il faut absolument g�rer �a proprement : demander � Kongberg le mode
% d'emploi
% if (Head1.SystemSerialNumber == 212) || (Head1.SystemSerialNumberSecondHead == 212)
%     Head1.SystemSerialNumber = 10212;
%     Head1.SystemSerialNumberSecondHead = 212;
% end

% Comment� pour l'instant car SystemSerialNumber n'est pas d�fini
%{
% D�but bidouille pour EM2040 Thalia 
if (DataInstallParams.SystemSerialNumber  == 172) && (DataInstallParams.SystemSerialNumberSecondHead  == 174)
    pppp = Head1.SystemSerialNumber;
    Head1.SystemSerialNumber = Head1.SystemSerialNumberSecondHead;
    Head1.SystemSerialNumberSecondHead = pppp;
end
% Fin bidouille pour Thalia

% D�but bidouille pour nouveau Belgica 
if (DataInstallParams.SystemSerialNumber  == 2004) && (DataInstallParams.SystemSerialNumberSecondHead  == 2031)
    pppp = Head1.SystemSerialNumber;
    Head1.SystemSerialNumber = Head1.SystemSerialNumberSecondHead;
    Head1.SystemSerialNumberSecondHead = pppp;
end
% Fin bidouille pour nouveau Belgica

% D�but bidouille pour EM2040 SimonStevin apr�s changement de t�te
if (DataInstallParams.SystemSerialNumber  == 353) && (DataInstallParams.SystemSerialNumberSecondHead  == 254)
    TSV = DataInstallParams.TSV;
    if datenum(TSV(6:end), 'mmm dd yyyy') >= 737127 % 'Mar 8 2018'
        pppp = Head1.SystemSerialNumber;
        Head1.SystemSerialNumber = Head1.SystemSerialNumberSecondHead;
        Head1.SystemSerialNumberSecondHead = pppp;
    end
end
% Fin bidouille pour EM2040 SimonStevin apr�s changement de t�te
%}
SounderDualOneTxAntenna = DataInstallParams.isDual; 


%% Datagrammes Attitude

if isfield(Hdf5Data.Sonar.Beam_group1.Att','preferred_MRU')
    idMRU  = Hdf5Data.Sonar.Beam_group1.Att.preferred_MRU + 1;
else
    strWrn = sprintf('Problem in HDF5/Sonar/Beam_group1 MRU identification attributes reading:%s', Hdf5Data.fileName);
    my_warndlg(strWrn, 0, 'Tag', 'MessageHDF5');
    return
end
[flag, XML, DataAttitudeXSF] = XSF.extractAttitude(Hdf5Data) ; % , 'SensorId', strMRUId);
if ~flag
    nomFic = Hdf5Data.fileName;
    nomFic = strrep(nomFic, '.xsf.nc', '.kmall');
    str1 = sprintf('Le fichier "%s" est vide. Je le renomme en  "xxx_all.bad"', nomFic);
    str2 = sprintf('"%s" is empty, I rename it in "xxx_all.bad" file.', nomFic);
    renameBadFile(nomFic, 'Message', Lang(str1,str2))
    return
end

flag = NetcdfUtils.XMLBin2Netcdf(XML, DataAttitudeXSF, ncFileName, 'Attitude');
if ~flag
    return
end


%% Datagrammes Clock

[flag, XML, Data] = XSF.extractClock(Hdf5Data);
if ~flag
    return
end

flag = NetcdfUtils.XMLBin2Netcdf(XML, Data, ncFileName, 'Clock');
if ~flag
    return
end

%% Datagrammes Position

if isfield(Hdf5Data.Sonar.Beam_group1.Att', 'preferred_position')
    idPosition    = Hdf5Data.Sonar.Beam_group1.Att.preferred_position + 1;
%     strPositionId = sprintf('Sensor%03d', idPosition);
else
    strWrn = sprintf('Problem in HDF5/Sonar/Beam_group1 Position identification attributes reading:%s', Hdf5Data.fileName);
    my_warndlg(strWrn, 0, 'Tag', 'MessageHDF5');
    return
end
[flag, XML, Data, XMLHeight, DataHeight] = XSF.extractPosition(Hdf5Data); % , 'SensorId', strPositionId);
if ~flag
    return
end

flag = NetcdfUtils.XMLBin2Netcdf(XML, Data, ncFileName, 'Position');
if ~flag
    return
end

% Ajout JMA le 09/02/2024 pour test r�solution pb de mar�e,
% �a a l'air de fonctionner % TODO � contr�ler par Ridha
DataHeight.Height = DataHeight.Height - DataInstallParams.WLZ;

flag = NetcdfUtils.XMLBin2Netcdf(XMLHeight, DataHeight, ncFileName, 'Height');
if ~flag
    return
end

%% Datagrammes SSP

[flag, XML, Data] = XSF.extractSSP(Hdf5Data);
if ~flag
    return
end

flag = NetcdfUtils.XMLBin2Netcdf(XML, Data, ncFileName, 'SoundSpeedProfile');
if ~flag
    return
end

%% Datagrammes Seabed

BypassSeabedImage= 1;
[flag, XML, DataSeabedXSF] = XSF.extractSeabedImage(Hdf5Data, DataRuntimeXSF, 'BypassSeabedImage', BypassSeabedImage);
if ~flag
    return
end

flag = NetcdfUtils.XMLBin2Netcdf(XML, DataSeabedXSF, ncFileName, 'SeabedImage59h');
if ~flag
    return
end

%% Datagrammes Raw

[flag, XML, DataRawXSF] = XSF.extractRawRangeBeamAngle(Hdf5Data, SounderDualOneTxAntenna);
if ~flag
    return
end

flag = NetcdfUtils.XMLBin2Netcdf(XML, DataRawXSF, ncFileName, 'RawRangeBeamAngle');
if ~flag
    return
end

%% Datagrammes Depth

[flag, XML, DataDepthXSF] = XSF.extractDepth(Hdf5Data, DataAttitudeXSF, DataSeabedXSF, DataRawXSF, 'BypassSeabedImage', BypassSeabedImage);
if ~flag
    return
end

flag = NetcdfUtils.XMLBin2Netcdf(XML, DataDepthXSF, ncFileName, 'Depth');
if ~flag
    return
end

%% Datagrammes WaterColumn

[flag, XML, Data] = XSF.extractWaterColumn(Hdf5Data);
if ~flag
    return
end

if ~isempty(XML)
    flag = NetcdfUtils.XMLBin2Netcdf(XML, Data, ncFileName, 'WaterColumn');
    if ~flag
        return
    end
end

%% Cr�ation des attributs globaux

attID = -1; % netcdf.getConstant('GLOBAL');
ncID = netcdf.open(ncFileName, 'WRITE');
netcdf.putAtt(ncID, attID, 'Constructor',         'Kongsberg');
netcdf.putAtt(ncID, attID, 'EmModel',             304);
netcdf.putAtt(ncID, attID, 'SystemSerialNumber',  160);
netcdf.putAtt(ncID, attID, 'DatagramVersion',     'V2');
netcdf.putAtt(ncID, attID, 'DatagramSousVersion', '');
netcdf.putAtt(ncID, attID, 'Software',            'SonarScope');
netcdf.putAtt(ncID, attID, 'Support',             'sonarscope@ifremer.fr');
netcdf.close(ncID);

my_rename(ncFileName, strrep(ncFileName, '.NC', '.nc'));

Version = 'V2';

%% Cr�ation du layer AbsorptionCoeffSSc

aKM = cl_simrad_all('nomFic', nomFic);
if ~isempty(aKM)
    flag = ALL_AbsorpCoeff_SSP(aKM);
end

%{
  // global attributes:
  :Constructor = "Kongsberg";
  :EmModel = 304.0; // double
  :SystemSerialNumber = 160.0; // double
  :DatagramVersion = "V2";
  :DatagramSousVersion = ;
  :Software = "SonarScope";
  :Support = "sonarscope@ifremer.fr";
  %}

%% Datagrammes ExtraParameters

%% Datagrammes QualityFactor

%% Datagrammes QualityFactor - QF From Ifremer

%% Datagrammes %% Datagrammes Raw Beam Samples Datagram

%% Datagrammes Stave Data Datagram

%% Datagrammes SurfaceSoundSpeed

%% Datagrammes Stave Data Datagram

%% Datagrammes Heading

%% Datagrammes NetworkAttitude
