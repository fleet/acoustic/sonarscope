function [Y, indexSounder] = Kongsberg_table2image(X, Name, NbDatagrams, nbMaxBeams, NbBeams, BeamNumber, PingCounter, SystemSerialNumber, Head1)

SSN         = unique(SystemSerialNumber);
PingCounter = PingCounter - (PingCounter(1) -1);
% GLU : Ligne suivante r�activ�e pour �viter le saut des num�ros Pings. Pb
% contact� sur le fichier de Carla : EM122, 0293_20110129_015608.all.
% NbPings     = length(unique(PingCounter)); 

% TODO : Remaniment de cette fonction avec Roger le 02/04/2015 (date du commit)


% GLU : NbPings     = max(PingCounter); 
BeamNumber  = single(BeamNumber);
nbMaxBeams  = single(nbMaxBeams);
NbBeams     = double(NbBeams);

% PingCounter(PingCounter > NbDatagrams) = NaN; % Comment� par JMA le 20/09/2016 (voir all2ssc_Depth58h)
NbPings = max(PingCounter); % TODO : r�tabli par JMA le 01/04/2015 pour fichier FSK/0015_20141023_085935_ShipName

% Condition trouv�e pour C:\Temp\HWS WaterColonn\0004_20081001_085803_ShipName.all
% if PingCounter(end) > NbDatagrams
%     PingCounter = 1:NbDatagrams;
% end

if (length(SSN) == 1) && ~isfield(Head1, 'SystemSerialNumber')
    nbBeams = nbMaxBeams;
    indexSounder = zeros(1, nbBeams, 'single');
else
    
    if Head1.SystemSerialNumber == 0 % Cas rencontr� sur fichier belgica d'exemple : en cours d'analyse par Roger
        nbBeams = nbMaxBeams;
        indexSounder = zeros(1, nbBeams, 'single');
    else
        
        if isempty(Head1.SystemSerialNumber)
            nbBeams = nbMaxBeams;
            indexSounder = zeros(1, nbBeams, 'single');
        else
            if (length(SSN) == 1) && (SSN == Head1.SystemSerialNumber)
                nbBeams = nbMaxBeams;
                indexSounder = zeros(1, nbBeams, 'single');
            else
                nbBeams = single(nbMaxBeams) * 2;
                indexSounder = zeros(1, nbBeams, 'single');
                indexSounder(nbMaxBeams+1:end) = 1;
            end
        end
    end
end


Y = NaN(NbPings, nbBeams, 'single');
subBeams = 0;
for iPing=1:NbDatagrams
    if NbBeams(iPing) == 0
        continue
    end
%     subBeams = subBeams(end) + (1:NbBeams(iPing));
    before = subBeams(end);
    subBeams = (before+1):(before+NbBeams(iPing));
    
    if Head1.SystemSerialNumberSecondHead == 0
        Offset = 0;
    else
        if SystemSerialNumber(iPing) == Head1.SystemSerialNumber
            if Head1.S1R <= 0
                Offset = 0;
            else
                Offset = nbMaxBeams;
            end
        else
            if Head1.S1R <= 0
                Offset = nbMaxBeams;
            else
                Offset = 0;
            end
        end
    end
    
    %{
    % D�plac� apr�s
    if ~isempty(BeamNumber)
        if max(subBeams) <= length(BeamNumber)
            subAnomalie = (BeamNumber(subBeams) > nbMaxBeams);
            if any(subAnomalie)
                fprintf('Something strange Ping=%d : beam numbers (max=%d) > %d\n', iPing, max(BeamNumber(subBeams(subAnomalie))), nbMaxBeams);
                BeamNumber(subBeams(subAnomalie)) = nbMaxBeams;
            end
        else
            'pas bon �a' % Pb rencontr� sur donn�es Mag&Phase 0007_20100125_100643_ShipName
            subBeams(subBeams > length(BeamNumber)) = [];
        end
    end
    %}
    
    % Correctif annul� car destructuration de l'image sur Tan0211  0028_20020820_093811 et autres
%     subValide = find(BeamNumber(subBeams) <= NbBeams(iPing)); % Pb rencontr� sur fichiers TAN0204 0024_20020324_211159_raw.all et 0025_20020324_232556_raw.all du r�pertoire \\rayleigh\home3\augustin\NIWA\CookStrait\tan0204 on trouve n = 255 pour tous les faisceaux du premier ping et on retrouve cette valeur plus tard dans le fichier
%     if ~isempty(subValide)
%         Y(PingCounter(iPing), BeamNumber(subBeams(subValide)) + Offset) = X(subBeams(subValide));
%     end
    
    if isempty(BeamNumber)
        if ~isnan(PingCounter(iPing))
            subx = 1:length(subBeams);
            if isempty(X)
                Y(PingCounter(iPing), subx + Offset) = subx;
            else
                sub = (subBeams <= length(X)); % Bug trouv� sur fichier F:\SAT_EM710\EM710_Magdebourg_110609\SonarScope\0005_20090611_001149\ALL_WaterColumn.xml
                Y(PingCounter(iPing), subx(sub) + Offset) = X(subBeams(sub));
            end
        end
    else
        
        % D�plac�
        if max(subBeams) <= length(BeamNumber)
            subAnomalie = (BeamNumber(subBeams) > nbMaxBeams);
            if any(subAnomalie)
                fprintf('Something strange Ping=%d : beam numbers (max=%d) > %d\n', iPing, max(BeamNumber(subBeams(subAnomalie))), nbMaxBeams);
%                 BeamNumber(subBeams(subAnomalie)) = nbMaxBeams;
                break % Break rajout� le 15/09/2014 par JMA pour fichier Hydro-map� norfanz-406.0 du NIWA
            end
        else
            'pas bon �a' % Pb rencontr� sur donn�es Mag&Phase 0007_20100125_100643_ShipName
            subBeams(subBeams > length(BeamNumber)) = [];
        end

        
        % Condition rajout�e le 02/05/2011 pour donn�es 2040 Odd Arne Mag&Phase sur �pave pour Yoann
        sub = find(subBeams > length(X));
        if ~isempty(sub)
            subBeams(sub) = [];
        end
        if isempty(subBeams)
            break
        end
        
        if ~isnan(PingCounter(iPing))
            BN = BeamNumber(subBeams) + Offset;
            sub = (BN > 0);
            subBeams = subBeams(sub);
            Y(PingCounter(iPing), BeamNumber(subBeams) + Offset) = X(subBeams);
        end
        
    end
end

if strcmp(Name, 'Depth')
    Y = -Y;
end
