function [Y, indexSounder] = Kongsberg_table2image_Additive(X, NbDatagrams, nbMaxBeams, NbBeams, BeamNumber, PingCounter, SystemSerialNumber, Head1)

SSN         = unique(SystemSerialNumber);
PingCounter = PingCounter - (PingCounter(1) -1);
NbPings     = max(PingCounter);
BeamNumber  = single(BeamNumber);
nbMaxBeams  = single(nbMaxBeams);

if isempty(Head1.SystemSerialNumber)
    nbBeams = nbMaxBeams;
    indexSounder = zeros(1, nbBeams, 'single');
else
    if (length(SSN) == 1) && (SSN == Head1.SystemSerialNumber)
        nbBeams = nbMaxBeams;
        indexSounder = zeros(1, nbBeams, 'single');
    else
        nbBeams = single(nbMaxBeams) * 2;
        indexSounder = zeros(1, nbBeams, 'single');
        indexSounder(nbMaxBeams+1:end) = 1;
    end
end

% Condition trouv�e pour C:\Temp\HWS WaterColonn\0004_20081001_085803_ShipName.all
if PingCounter(end) > NbDatagrams
    PingCounter = 1:NbDatagrams;
end

Y = zeros(NbPings, nbBeams, 'single');
subBeams = 0;

for iPing=1:NbDatagrams
    if NbBeams(iPing) == 0
        continue
    end
    subBeams = subBeams(end) + (1:NbBeams(iPing));
    
    if Head1.SystemSerialNumberSecondHead == 0
        Offset = 0;
    else
        if SystemSerialNumber(iPing) == Head1.SystemSerialNumber
            if Head1.S1R <= 0
                Offset = 0;
            else
                Offset = nbMaxBeams;
            end
        else
            if Head1.S1R <= 0
                Offset = nbMaxBeams;
            else
                Offset = 0;
            end
        end
    end
    
    Y(PingCounter(iPing), BeamNumber(subBeams) + Offset) = Y(PingCounter(iPing), BeamNumber(subBeams) + Offset) + X(subBeams)';
end

