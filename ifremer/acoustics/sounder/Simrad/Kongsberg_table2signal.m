function Y = Kongsberg_table2signal(X, NbDatagrams, PingCounter, SystemSerialNumber, Head1)

SSN         = unique(SystemSerialNumber);
PingCounter = PingCounter - (PingCounter(1) -1);
% GLU : Ligne suivante r�activ�e pour �viter le saut des num�ros Pings. Pb
% contact� sur le fichier de Carla : EM122, 0293_20110129_015608.all.
% NbPings     = length(unique(PingCounter)); 
% GLU : NbPings     = max(PingCounter); 
% PingCounter(PingCounter > NbDatagrams) = NaN; % Comment� par JMA le 20/09/2016 (voir all2ssc_Depth58h)
NbPings = max(PingCounter); % TODO : r�tabli par JMA le 01/04/2015 pour fichier FSK/0015_20141023_085935_ShipName

% TODO : Remaniment de cette fonction avec Roger le 02/04/2015 (date du commit)

% [b, m, n] = unique(PingCounter);
% % isequal(b,  PingCounter(m))
% % isequal(PingCounter,  b(n))
% Condition trouv�e pour C:\Temp\HWSWaterColonn\0004_20081001_085803_ShipName.all !!!
% if PingCounter(end) > NbDatagrams
%     PingCounter = 1:NbDatagrams;
% end

Y = NaN(NbPings, length(SSN), class(X));
for iPing=1:NbDatagrams
    if Head1.SystemSerialNumberSecondHead == 0
        Offset = 1;
    else
        if SystemSerialNumber(iPing) == Head1.SystemSerialNumber
            if Head1.S1R <= 0
                Offset = 1;
            else
                Offset = 2;
            end
        else
            if Head1.S1R <= 0
                Offset = 2;
            else
                Offset = 1;
            end
        end
    end
    %     Offset = 1 + (SystemSerialNumber(iPing) ~=  SSN(1));
    if ~isnan(PingCounter(iPing))
        Y(PingCounter(iPing), Offset) = X(iPing);
    end
end
