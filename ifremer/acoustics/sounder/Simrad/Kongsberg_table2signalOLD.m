function Y = Kongsberg_table2signalOLD(X, NbDatagrams, PingCounter, SystemSerialNumber, Head1)

SSN         = unique(SystemSerialNumber);
PingCounter = PingCounter - (PingCounter(1) -1);
% NbPings     = length(unique(PingCounter));
NbPings     = max(PingCounter);

% [b, m, n] = unique(PingCounter);
% % isequal(b,  PingCounter(m))
% % isequal(PingCounter,  b(n))
% Condition trouvée pour C:\Temp\HWS WaterColonn\0004_20081001_085803_ShipName.all
if PingCounter(end) > NbDatagrams
    PingCounter = 1:NbDatagrams;
end


Y = NaN(NbPings, length(SSN), class(X));
for iPing=1:NbDatagrams
    if Head1.SystemSerialNumberSecondHead == 0
        Offset = 1;
    else
        if SystemSerialNumber(iPing) == Head1.SystemSerialNumber
            if Head1.S1R <= 0
                Offset = 1;
            else
                Offset = 2;
            end
        else
            if Head1.S1R <= 0
                Offset = 2;
            else
                Offset = 1;
            end
        end
    end
%     Offset = 1 + (SystemSerialNumber(iPing) ~=  SSN(1));
    Y(PingCounter(iPing), Offset) = X(iPing);
end
