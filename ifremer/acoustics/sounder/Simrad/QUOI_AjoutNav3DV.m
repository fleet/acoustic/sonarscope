% nomDirAll = 'H:\QUOI-SSc\EM302\BayOfPlenty\BoxFOI1\data';
% QUOI_AjoutNav3DV(nomDirAll, 'EM302-BayOfPlenty-BoxFOI1')
%
% nomDirAll = 'H:\QUOI-SSc\EM302\BayOfPlenty\BoxFOI2\data';
% QUOI_AjoutNav3DV(nomDirAll, 'EM302-BayOfPlenty-BoxFOI2')
%
% nomDirAll = 'H:\QUOI-SSc\EM302\BayOfPlenty\BoxSSG\data';
% QUOI_AjoutNav3DV(nomDirAll, 'EM302-BayOfPlenty-BoxSSG')
%
% nomDirAll = 'H:\QUOI-SSc\EM302\BayOfPlenty\SurveySE\data';
% QUOI_AjoutNav3DV(nomDirAll, 'EM302-BayOfPlenty-SurveySE')
%
% nomDirAll = 'H:\QUOI-SSc\EM302\BayOfPlenty\SurveySW\data';
% QUOI_AjoutNav3DV(nomDirAll, 'EM302-BayOfPlenty-SurveySW')
%
% nomDirAll = 'H:\QUOI-SSc\EM302\BayOfPlenty\SurveyN\data';
% QUOI_AjoutNav3DV(nomDirAll, 'EM302-BayOfPlenty-SurveyN')
%
% nomDirAll = 'H:\QUOI-SSc\EM302\PovertyBay\Box\data';
% QUOI_AjoutNav3DV(nomDirAll, 'EM302-PovertyBay-Box')
%
% nomDirAll = 'H:\QUOI-SSc\EM302\PovertyBay\Survey\data';
% QUOI_AjoutNav3DV(nomDirAll, 'EM302-PovertyBay-Survey')
%
%
%
% nomDirAll = 'H:\QUOI-SSc\EM2040\BayOfPlenty\BoxFOI1\data';
% QUOI_AjoutNav3DV(nomDirAll, 'EM2040-BayOfPlenty-BoxFOI1')
%
% nomDirAll = 'H:\QUOI-SSc\EM2040\BayOfPlenty\BoxFOI2\data';
% QUOI_AjoutNav3DV(nomDirAll, 'EM2040-BayOfPlenty-BoxFOI2')
%
% nomDirAll = 'H:\QUOI-SSc\EM2040\BayOfPlenty\BoxSSG\data';
% QUOI_AjoutNav3DV(nomDirAll, 'EM2040-BayOfPlenty-BoxSSG')
%
% nomDirAll = 'H:\QUOI-SSc\EM2040\BayOfPlenty\SurveySE\data';
% QUOI_AjoutNav3DV(nomDirAll, 'EM2040-BayOfPlenty-SurveySE')
%
% nomDirAll = 'H:\QUOI-SSc\EM2040\BayOfPlenty\SurveySW\data';
% QUOI_AjoutNav3DV(nomDirAll, 'EM2040-BayOfPlenty-SurveySW')
%
% nomDirAll = 'H:\QUOI-SSc\EM2040\BayOfPlenty\SurveyN\data';
% QUOI_AjoutNav3DV(nomDirAll, 'EM2040-BayOfPlenty-SurveyN')
%
% nomDirAll = 'H:\QUOI-SSc\EM2040\PovertyBay\Box\data';
% QUOI_AjoutNav3DV(nomDirAll, 'EM2040-PovertyBay-Box')
%
% nomDirAll = 'H:\QUOI-SSc\EM2040\PovertyBay\Survey\data';
% QUOI_AjoutNav3DV(nomDirAll, 'EM2040-PovertyBay-Survey')
%
%
% nomDirAll = 'H:\Calypso-SSc\EM302\BayOfPlenty\Leg1\data';
% QUOI_AjoutNav3DV(nomDirAll, 'EM302-BayOfPlenty-CalypsoLeg1')
%
% nomDirAll = 'H:\Calypso-SSc\EM302\BayOfPlenty\Leg2\data';
% QUOI_AjoutNav3DV(nomDirAll, 'EM302-BayOfPlenty-CalypsoLeg2')
%
% nomDirAll = 'H:\Calypso-SSc\EM302\PovertyBay\Survey\data';
% QUOI_AjoutNav3DV(nomDirAll, 'EM302-PovertyBay-CalypsoSurvey')
%
% nomDirAll = 'H:\Calypso-SSc\EM302\PovertyBay\Star1\data';
% QUOI_AjoutNav3DV(nomDirAll, 'EM302-PovertyBay-CalypsoStar1')
%
% nomDirAll = 'H:\Calypso-SSc\EM302\PovertyBay\Star2\data';
% QUOI_AjoutNav3DV(nomDirAll, 'EM302-PovertyBay-CalypsoStar2')
%
% nomDirAll = 'H:\Calypso-SSc\EM302\PovertyBay\Star3\data';
% QUOI_AjoutNav3DV(nomDirAll, 'EM302-PovertyBay-CalypsoStar3')
%
% nomDirAll = 'H:\Calypso-SSc\EM302\PovertyBay\Star4\data';
% QUOI_AjoutNav3DV(nomDirAll, 'EM302-PovertyBay-CalypsoStar4')

function QUOI_AjoutNav3DV(nomDirAll, MosName)

nomDir1 = fileparts(nomDirAll);

%% Nettoyage du r�pertoire pour �viter des erreurs

nomFicAdoc = fullfile(nomDir1, 'SurveyReport', [MosName '.adoc']);
AdocUtils.cleanDirectories(nomFicAdoc);

%% Test si le r�pertoire 3DV existe d�j�

nomDir3DV = fullfile(nomDir1, 'SurveyReport', MosName, '3DV');
if ~exist(nomDir3DV, 'dir')
    [flag, msg] = mkdir(nomDir3DV);
    if ~flag
        my_warndlg(msg, 1);
        return
    end
end

%% Liste des fichiers .fig du r�pertoire FIG

nomDirFIG = fullfile(nomDir1, 'SurveyReport', MosName, 'FIG');
if ~exist(nomDir3DV, 'dir')
    return
end

listeFicFig = listeFicOnDir(nomDirFIG, '.fig', 'Filtre', '-Navigation-');
for k=length(listeFicFig):-1:1
    L(k) = length(listeFicFig{k});
end
[~, ind] = min(L); % Le fichier qui nous int�resse est celui dont le nom � le mois de caract�res (premier en principe)

nomFicFig = listeFicFig{ind};
nomFicXml3DV = strrep(nomFicFig, '.fig', '.xml');
nomFicXml3DV = strrep(nomFicXml3DV, 'FIG', '3DV');

open(nomFicFig);
plot_navigation_ExportXML('NomFicXml', nomFicXml3DV)

%% The End

fprintf('Ajout Nav for GLOBE for "%s" is over\n\n', nomDirAll);
