% nomDirAll = 'H:\QUOI-SSc\EM302\BayOfPlenty\BoxFOI1\data';
% QUOI_CleanDirectories(nomDirAll, 'EM302-BayOfPlenty-BoxFOI1')
%
% nomDirAll = 'H:\QUOI-SSc\EM302\BayOfPlenty\BoxFOI2\data';
% QUOI_CleanDirectories(nomDirAll, 'EM302-BayOfPlenty-BoxFOI2')
%
% nomDirAll = 'H:\QUOI-SSc\EM302\BayOfPlenty\BoxSSG\data';
% QUOI_CleanDirectories(nomDirAll, 'EM302-BayOfPlenty-BoxSSG')
%
% nomDirAll = 'H:\QUOI-SSc\EM302\BayOfPlenty\SurveySE\data';
% QUOI_CleanDirectories(nomDirAll, 'EM302-BayOfPlenty-SurveySE')
%
% nomDirAll = 'H:\QUOI-SSc\EM302\BayOfPlenty\SurveySW\data';
% QUOI_CleanDirectories(nomDirAll, 'EM302-BayOfPlenty-SurveySW')
%
% nomDirAll = 'H:\QUOI-SSc\EM302\BayOfPlenty\SurveyN\data';
% QUOI_CleanDirectories(nomDirAll, 'EM302-BayOfPlenty-SurveyN')
%
% nomDirAll = 'H:\QUOI-SSc\EM302\PovertyBay\Box\data';
% QUOI_CleanDirectories(nomDirAll, 'EM302-PovertyBay-Box')
%
% nomDirAll = 'H:\QUOI-SSc\EM302\PovertyBay\Survey\data';
% QUOI_CleanDirectories(nomDirAll, 'EM302-PovertyBay-Survey')
%
%
%
% nomDirAll = 'H:\QUOI-SSc\EM2040\BayOfPlenty\BoxFOI1\data';
% QUOI_CleanDirectories(nomDirAll, 'EM2040-BayOfPlenty-BoxFOI1')
%
% nomDirAll = 'H:\QUOI-SSc\EM2040\BayOfPlenty\BoxFOI2\data';
% QUOI_CleanDirectories(nomDirAll, 'EM2040-BayOfPlenty-BoxFOI2')
%
% nomDirAll = 'H:\QUOI-SSc\EM2040\BayOfPlenty\BoxSSG\data';
% QUOI_CleanDirectories(nomDirAll, 'EM2040-BayOfPlenty-BoxSSG')
%
% nomDirAll = 'H:\QUOI-SSc\EM2040\BayOfPlenty\SurveySE\data';
% QUOI_CleanDirectories(nomDirAll, 'EM2040-BayOfPlenty-SurveySE')
%
% nomDirAll = 'H:\QUOI-SSc\EM2040\BayOfPlenty\SurveySW\data';
% QUOI_CleanDirectories(nomDirAll, 'EM2040-BayOfPlenty-SurveySW')
%
% nomDirAll = 'H:\QUOI-SSc\EM2040\BayOfPlenty\SurveyN\data';
% QUOI_CleanDirectories(nomDirAll, 'EM2040-BayOfPlenty-SurveyN')
%
% nomDirAll = 'H:\QUOI-SSc\EM2040\PovertyBay\Box\data';
% QUOI_CleanDirectories(nomDirAll, 'EM2040-PovertyBay-Box')
%
% nomDirAll = 'H:\QUOI-SSc\EM2040\PovertyBay\Survey\data';
% QUOI_CleanDirectories(nomDirAll, 'EM2040-PovertyBay-Survey')
%
%
% nomDirAll = 'H:\Calypso-SSc\EM302\BayOfPlenty\Leg1\data';
% QUOI_CleanDirectories(nomDirAll, 'EM302-BayOfPlenty-CalypsoLeg1')
%
% nomDirAll = 'H:\Calypso-SSc\EM302\BayOfPlenty\Leg2\data';
% QUOI_CleanDirectories(nomDirAll, 'EM302-BayOfPlenty-CalypsoLeg2')
%
% nomDirAll = 'H:\Calypso-SSc\EM302\PovertyBay\Survey\data';
% QUOI_CleanDirectories(nomDirAll, 'EM302-PovertyBay-CalypsoSurvey')
%
% nomDirAll = 'H:\Calypso-SSc\EM302\PovertyBay\Star1\data';
% QUOI_CleanDirectories(nomDirAll, 'EM302-PovertyBay-CalypsoStar1')
%
% nomDirAll = 'H:\Calypso-SSc\EM302\PovertyBay\Star2\data';
% QUOI_CleanDirectories(nomDirAll, 'EM302-PovertyBay-CalypsoStar2')
%
% nomDirAll = 'H:\Calypso-SSc\EM302\PovertyBay\Star3\data';
% QUOI_CleanDirectories(nomDirAll, 'EM302-PovertyBay-CalypsoStar3')
%
% nomDirAll = 'H:\Calypso-SSc\EM302\PovertyBay\Star4\data';
% QUOI_CleanDirectories(nomDirAll, 'EM302-PovertyBay-CalypsoStar4')

function QUOI_CleanDirectories(nomDirAll, MosName)

nomDir1 = fileparts(nomDirAll);
nomFicAdoc = fullfile(nomDir1, 'SurveyReport', [MosName '.adoc']);
if ~exist(nomFicAdoc, 'file')
    msg = sprintf('File not found : %s', nomFicAdoc);
    my_warndlg(msg, 1)
    return
end

AdocUtils.cleanDirectories(nomFicAdoc);
