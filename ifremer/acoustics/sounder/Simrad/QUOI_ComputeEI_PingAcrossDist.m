% nomDirAll = 'H:\QUOI-SSc\EM302\BayOfPlenty\BoxFOI1\data';
% QUOI_ComputeEI_PingAcrossDist(nomDirAll, 3, 'EM302-BayOfPlenty-BoxFOI1')
%
% nomDirAll = 'H:\QUOI-SSc\EM302\BayOfPlenty\BoxFOI2\data';
% QUOI_ComputeEI_PingAcrossDist(nomDirAll, 3, 'EM302-BayOfPlenty-BoxFOI2')
%
% nomDirAll = 'H:\QUOI-SSc\EM302\BayOfPlenty\BoxSSG\data';
% QUOI_ComputeEI_PingAcrossDist(nomDirAll, 1, 'EM302-BayOfPlenty-BoxSSG')
%
% nomDirAll = 'H:\QUOI-SSc\EM302\BayOfPlenty\SurveySE\data';
% QUOI_ComputeEI_PingAcrossDist(nomDirAll, 3.5, 'EM302-BayOfPlenty-SurveySE')
%
% nomDirAll = 'H:\QUOI-SSc\EM302\BayOfPlenty\SurveySW\data';
% QUOI_ComputeEI_PingAcrossDist(nomDirAll, 3.5, 'EM302-BayOfPlenty-SurveySW')
%
% nomDirAll = 'H:\QUOI-SSc\EM302\BayOfPlenty\SurveyN\data';
% QUOI_ComputeEI_PingAcrossDist(nomDirAll, 3.5, 'EM302-BayOfPlenty-SurveyN')
%
% nomDirAll = 'H:\QUOI-SSc\EM302\PovertyBay\Box\data';
% QUOI_ComputeEI_PingAcrossDist(nomDirAll, 3, 'EM302-PovertyBay-Box')
%
% nomDirAll = 'H:\QUOI-SSc\EM302\PovertyBay\Survey\data';
% QUOI_ComputeEI_PingAcrossDist(nomDirAll, 3, 'EM302-PovertyBay-Survey')
%
%
%
% nomDirAll = 'H:\QUOI-SSc\EM2040\BayOfPlenty\BoxFOI1\data';
% QUOI_ComputeEI_PingAcrossDist(nomDirAll, 3, 'EM2040-BayOfPlenty-BoxFOI1')
%
% nomDirAll = 'H:\QUOI-SSc\EM2040\BayOfPlenty\BoxFOI2\data';
% QUOI_ComputeEI_PingAcrossDist(nomDirAll, 3, 'EM2040-BayOfPlenty-BoxFOI2')
%
% nomDirAll = 'H:\QUOI-SSc\EM2040\BayOfPlenty\BoxSSG\data';
% QUOI_ComputeEI_PingAcrossDist(nomDirAll, 1, 'EM2040-BayOfPlenty-BoxSSG')
%
% nomDirAll = 'H:\QUOI-SSc\EM2040\BayOfPlenty\SurveySE\data';
% QUOI_ComputeEI_PingAcrossDist(nomDirAll, 3.5, 'EM2040-BayOfPlenty-SurveySE')
%
% nomDirAll = 'H:\QUOI-SSc\EM2040\BayOfPlenty\SurveySW\data';
% QUOI_ComputeEI_PingAcrossDist(nomDirAll, 3.5, 'EM2040-BayOfPlenty-SurveySW')
%
% nomDirAll = 'H:\QUOI-SSc\EM2040\BayOfPlenty\SurveyN\data';
% QUOI_ComputeEI_PingAcrossDist(nomDirAll, 3.5, 'EM2040-BayOfPlenty-SurveyN')
%
% nomDirAll = 'H:\QUOI-SSc\EM2040\PovertyBay\Box\data';
% QUOI_ComputeEI_PingAcrossDist(nomDirAll, 3, 'EM2040-PovertyBay-Box')
%
% nomDirAll = 'H:\QUOI-SSc\EM2040\PovertyBay\Survey\data';
% QUOI_ComputeEI_PingAcrossDist(nomDirAll, 3, 'EM2040-PovertyBay-Survey')
%
%
% nomDirAll = 'H:\Calypso-SSc\EM302\BayOfPlenty\Leg1\data';
% QUOI_ComputeEI_PingAcrossDist(nomDirAll, 5, 'EM302-BayOfPlenty-CalyLeg1')
%
% nomDirAll = 'H:\Calypso-SSc\EM302\BayOfPlenty\Leg2\data';
% QUOI_ComputeEI_PingAcrossDist(nomDirAll, 2.5, 'EM302-BayOfPlenty-CalyLeg2')
%
% nomDirAll = 'H:\Calypso-SSc\EM302\PovertyBay\Survey\data';
% QUOI_ComputeEI_PingAcrossDist(nomDirAll, 3, 'EM302-PovertyBay-Survey')
%
% nomDirAll = 'H:\Calypso-SSc\EM302\PovertyBay\Star1\data';
% QUOI_ComputeEI_PingAcrossDist(nomDirAll, 1.5, 'EM302-PovertyBay-Star1')
%
% nomDirAll = 'H:\Calypso-SSc\EM302\PovertyBay\Star2\data';
% QUOI_ComputeEI_PingAcrossDist(nomDirAll, 1.5, 'EM302-PovertyBay-Star2')
%
% nomDirAll = 'H:\Calypso-SSc\EM302\PovertyBay\Star3\data';
% QUOI_ComputeEI_PingAcrossDist(nomDirAll, 2.5, 'EM302-PovertyBay-Star3')
%
% nomDirAll = 'H:\Calypso-SSc\EM302\PovertyBay\Star4\data';
% QUOI_ComputeEI_PingAcrossDist(nomDirAll, 2, 'EM302-PovertyBay-Star4')

function QUOI_ComputeEI_PingAcrossDist(nomDirAll, gridSize, MosName)

global useParallel %#ok<GVMIS>

if isempty(useParallel)
    [flag, X] = question_NumberOfWorkers;
    if ~flag
        return
    end
    useParallel = X;
end

listeFicAll = listeFicOnDir(nomDirAll, '.all');

nomDir1 = fileparts(listeFicAll{1});
nomDir2 = fileparts(nomDir1);
nomDir3 = fullfile(nomDir2, 'EI');
if ~exist(nomDir3, 'dir')
    [flag, msg] = mkdir(nomDir3);
    if ~flag
        my_warndlg(msg, 1);
        return
    end
end
nomDirOut_EIOverSpecularPingAcrossDist = fullfile(nomDir3, 'EI-OverSpecular-PingAcrossDist');
if ~exist(nomDirOut_EIOverSpecularPingAcrossDist, 'dir')
    [flag, msg] = mkdir(nomDirOut_EIOverSpecularPingAcrossDist);
    if ~flag
        my_warndlg(msg, 1);
        return
    end
end

nomDirOut_EIOverSeafloorPingAcrossDist = fullfile(nomDir3, 'EI-OverSeafloor-PingAcrossDist');
if ~exist(nomDirOut_EIOverSeafloorPingAcrossDist, 'dir')
    [flag, msg] = mkdir(nomDirOut_EIOverSeafloorPingAcrossDist);
    if ~flag
        my_warndlg(msg, 1);
        return
    end
end

N = length(listeFicAll);
flagListeFicXmlEchogramOverSpecular = false(N, 1);
flagListeFicXmlEchogramOverSeafloor = false(N, 1);
for k=1:N
    [nomDir1, nomFic1] = fileparts(listeFicAll{k});
    nomDir2 = fileparts(nomDir1);
    
    ListeFicXmlEchogramOverSpecular{k} = fullfile(nomDir2, 'WC-DepthAcrossDist', [nomFic1 '_Raw_PolarEchograms.xml']); %#ok<AGROW>
    if exist(ListeFicXmlEchogramOverSpecular{k}, 'file')
        flagListeFicXmlEchogramOverSpecular(k) = true;
    end
    
    ListeFicXmlEchogramOverSeafloor{k} = fullfile(nomDir2, 'WC-DepthAcrossDist', [nomFic1 '_Comp_PolarEchograms.xml']); %#ok<AGROW>
    if exist(ListeFicXmlEchogramOverSeafloor{k}, 'file')
        flagListeFicXmlEchogramOverSeafloor(k) = true;
    end
end
listeFicAllOverSpecular         = listeFicAll(flagListeFicXmlEchogramOverSpecular);
ListeFicXmlEchogramOverSpecular = ListeFicXmlEchogramOverSpecular(flagListeFicXmlEchogramOverSpecular);
listeFicAllOverSeafloor         = listeFicAll(flagListeFicXmlEchogramOverSeafloor);
ListeFicXmlEchogramOverSeafloor = ListeFicXmlEchogramOverSeafloor(flagListeFicXmlEchogramOverSeafloor);

%% Echo int�gration OverSpecular

params_ZoneEI.SeuilBas  = 0.96;
params_ZoneEI.SeuilHaut = 0.85;
TypeEchoIntegration = 1;
flag = ComputeEchoIntegrationAndExport(ListeFicXmlEchogramOverSpecular, listeFicAllOverSpecular, nomDirOut_EIOverSpecularPingAcrossDist, ...
    TypeEchoIntegration, params_ZoneEI, gridSize, MosName, nomDir2);
if ~flag
    return
end

%% Echo int�gration OverSeafloor

params_ZoneEI.SeuilBas  = 0.96;
params_ZoneEI.SeuilHaut = 0.85;
TypeEchoIntegration = 2;
flag = ComputeEchoIntegrationAndExport(ListeFicXmlEchogramOverSeafloor, listeFicAllOverSeafloor, nomDirOut_EIOverSeafloorPingAcrossDist, ...
    TypeEchoIntegration, params_ZoneEI, gridSize, MosName, nomDir2);
if ~flag
    return
end

%% The End

fprintf('Computing echoe integration for "%s" is over\n\n', nomDirAll);



function flag = ComputeEchoIntegrationAndExport(ListeFicXmlEchogram, listeFicAll, nomDirOut_EIPingAcrossDist, ...
    TypeEchoIntegration, params_ZoneEI, gridSize, MosName, nomDir2)

I0 = cl_image_I0;

if TypeEchoIntegration == 1
    MosName = [MosName '-OverSpecular'];
    Tag = 'Raw';
    nomDir3 = 'EI-OverSpecular-LatLong';
else
    MosName = [MosName '-OverSeabed'];
    Tag = 'Comp';
    nomDir3 = 'EI-OverSeafloor-LatLong';
end

%% Calcul des �cho int�grations en g�om�trie PingAcrossDist

typePolarEchogram = 1; % TODO : .xml seulement pour l'instant. Poser la question

fprintf('Creating EI images in PingAcrossDist geometry for %s\n', MosName);
ALL.Process.WaterColumnEchoIntegration(ListeFicXmlEchogram, listeFicAll, nomDirOut_EIPingAcrossDist, typePolarEchogram, ...
    'params_ZoneEI', params_ZoneEI, 'TypeEchoIntegration', TypeEchoIntegration, 'Info', MosName);

%% Cr�ation des mosa�ques individuelles

fprintf('Creating individual EI mosaics for %s\n', MosName);
N = length(listeFicAll);
for k=1:N
    [~, nomFic1] = fileparts(listeFicAll{k});
    listFileNames(k).nomFicReflec    = fullfile(nomDirOut_EIPingAcrossDist, [nomFic1 '_' Tag '_EI_PingAcrossDist_Reflectivity.xml']); %#ok<AGROW>
    listFileNames(k).nomFicAlongDist = fullfile(nomDirOut_EIPingAcrossDist, [nomFic1 '_' Tag '_EI_PingAcrossDist_AlongDist.xml']); %#ok<AGROW>
    listFileNames(k).nomFicTxAngle   = fullfile(nomDirOut_EIPingAcrossDist, [nomFic1 '_' Tag '_EI_PingAcrossDist_TxAngle.xml']); %#ok<AGROW>
end

InfoMos.nomFicErMapperLayer = [];
InfoMos.nomFicErMapperAngle = [];
InfoMos.DirName = fullfile(nomDir2, 'EI', nomDir3);

InfoMos.TypeMos = 2;
PingAcrossDist2LatLong(listFileNames, gridSize, 10, [5 5], InfoMos, 'Info', MosName); %, ...
%     'listeMode', listeMode, 'listeSwath', listeSwath, 'listeFM', listeFM);

%% Lecture des mosa�ques individuelles

fprintf('Reading individual EI mosaics for %s\n', MosName);
listeFicErs = listeFicOnDir(InfoMos.DirName, '.ers', 'Filtre', [Tag '_EI_Reflectivity']);
listeFicAng = listeFicOnDir(InfoMos.DirName, '.ers', 'Filtre', [Tag '_EI_RxBeamAngle']);
[flag, EI]  = cl_image.import_ermapper(listeFicErs);
if ~flag
    return
end
[flag, Ang] = cl_image.import_ermapper(listeFicAng);
if ~flag
    return
end

%% Cr�ation de la mosa�que globale

fprintf('Merging individual EI mosaics into a global mosaic for %s\n', MosName);
[~, M, A] = mergeMosaics(EI, 'Angle', Ang, 'Info', MosName);
M = update_Name(M, 'Name', MosName);
A = update_Name(A, 'Name', MosName);

%% Affichage

fprintf('Display Global mosaic in SonarScope just to have a look for %s\n', MosName);
SonarScope([M, A]);

%% Export mosa�que d'EI et mosa�que des angles en .ers

fprintf('Exporting global EI mosaic in .ers for %s\n', MosName);

nomFicOut = fullfile(InfoMos.DirName, [M.Name '.ers']);
flag = export_ermapper(M, nomFicOut);
if ~flag
    return
end

nomFicOut = fullfile(InfoMos.DirName, [A.Name '.ers']);
flag = export_ermapper(A, nomFicOut);
if ~flag
    return
end

%% Export 3DV de la mosaique globale d'EI

fprintf('Exporting global EI mosaic in geotif 32 bits for %s', MosName);
nomLayer = [MosName '_texture_v2'];
nomFic3D = [MosName '_texture_v2'];
repExport3DV = fullfile(InfoMos.DirName, '3DV');
if ~exist(repExport3DV, 'dir')
    [flag, msg] = mkdir(repExport3DV);
    if ~flag
        my_warndlg(msg, 1);
        return
    end
end
flagExportTexture   = 1;
flagExportElevation = 0;
sizeTiff = 32000; %2500;
flag = export_3DViewer_InGeotifTiles(M, nomLayer, nomFic3D, repExport3DV, ...
    flagExportTexture, flagExportElevation, 'sizeTiff', sizeTiff);
if ~flag
    return
end
fprintf('The geotif 32 bits used for GLOBE can be used in SIG sw, you will find it here :\n%s\n', ...
    fullfile(repExport3DV, nomLayer, [nomLayer '_0_0_texture_v2.tif']));

%% R�alisation multi-image pour GLOBE

fprintf('Creating GLOBE EI Multi Images for %s\n', MosName);
nomDirMultiImageXML = fullfile(InfoMos.DirName, '3DV');
if ~exist(nomDirMultiImageXML, 'dir')
    [flag, msg] = mkdir(nomDirMultiImageXML);
    if ~flag
        my_warndlg(msg, 1);
        return
    end
end
nomFicMultiImageXML = fullfile(nomDirMultiImageXML, [MosName '-ML.xml']);
flag = export_3DVMultiImages(EI, nomFicMultiImageXML, 'sizeTiff', sizeTiff);
if ~flag
    return
end
fprintf('The geotif 32 bits files of all individual EI mosaics can be used in SIG sw, you will find them here :\n%s\n', ...
    fullfile(repExport3DV, [MosName '-ML'], ['Reflectivity_' MosName '-ML'], '3', [MosName '-ML_0_0.tif']));


