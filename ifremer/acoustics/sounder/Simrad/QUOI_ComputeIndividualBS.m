% Tag = 'BS';
% NomFicCurve = 'H:\QUOI-SSc\EM302\BayOfPlenty\BoxSSG\JMA\DiagTx_Mode2_SwathSingle_SignalCW_Wholeimage_Meancurve_Clean1_comp.mat';
%
% TODO : améliorer QUOI_ComputeIndividualBS pour qu'il n'y ait pas besoin
% de donner une layer d'exemple
%
% Inutilisable pour l'instant
%
% nomDirAll = 'H:\QUOI-SSc\EM302\BayOfPlenty\BoxFOI1\data';
% QUOI_ComputeIndividualBS(nomDirAll, 3, Tag, NomFicCurve)
%
% nomDirAll = 'H:\QUOI-SSc\EM302\BayOfPlenty\BoxFOI2\data';
% QUOI_ComputeIndividualBS(nomDirAll, 3, Tag, NomFicCurve)
%
% nomDirAll = 'H:\QUOI-SSc\EM302\BayOfPlenty\BoxSSG\data';
% QUOI_ComputeIndividualBS(nomDirAll, 1, Tag, NomFicCurve)
%
% nomDirAll = 'H:\QUOI-SSc\EM302\BayOfPlenty\SurveySE\data';
% QUOI_ComputeIndividualBS(nomDirAll, 3.5, Tag, NomFicCurve)
%
% nomDirAll = 'H:\QUOI-SSc\EM302\BayOfPlenty\SurveySW\data';
% QUOI_ComputeIndividualBS(nomDirAll, 3.5, Tag, NomFicCurve)
%
% nomDirAll = 'H:\QUOI-SSc\EM302\BayOfPlenty\SurveyN\data';
% QUOI_ComputeIndividualBS(nomDirAll, 3.5, Tag, NomFicCurve)
%
% nomDirAll = 'H:\QUOI-SSc\EM302\PovertyBay\Box\data';
% QUOI_ComputeIndividualBS(nomDirAll, 3, Tag, NomFicCurve)
%
% nomDirAll = 'H:\QUOI-SSc\EM302\PovertyBay\Survey\data';
% QUOI_ComputeIndividualBS(nomDirAll, 3, Tag, NomFicCurve)
%
%
%
% NomFicCurve = 'TODO';
%
% nomDirAll = 'H:\QUOI-SSc\EM2040\BayOfPlenty\BoxFOI1\data';
% QUOI_ComputeIndividualBS(nomDirAll, 3, Tag, NomFicCurve)
%
% nomDirAll = 'H:\QUOI-SSc\EM2040\BayOfPlenty\BoxFOI2\data';
% QUOI_ComputeIndividualBS(nomDirAll, 3, Tag, NomFicCurve)
%
% nomDirAll = 'H:\QUOI-SSc\EM2040\BayOfPlenty\BoxSSG\data';
% QUOI_ComputeIndividualBS(nomDirAll, 1, Tag, NomFicCurve)
%
% nomDirAll = 'H:\QUOI-SSc\EM2040\BayOfPlenty\SurveySE\data';
% QUOI_ComputeIndividualBS(nomDirAll, 3.5, Tag, NomFicCurve)
%
% nomDirAll = 'H:\QUOI-SSc\EM2040\BayOfPlenty\SurveySW\data';
% QUOI_ComputeIndividualBS(nomDirAll, 3.5, Tag, NomFicCurve)
%
% nomDirAll = 'H:\QUOI-SSc\EM2040\BayOfPlenty\SurveyN\data';
% QUOI_ComputeIndividualBS(nomDirAll, 3.5, Tag, NomFicCurve)
%
% nomDirAll = 'H:\QUOI-SSc\EM2040\PovertyBay\Box\data';
% QUOI_ComputeIndividualBS(nomDirAll, 3, Tag, NomFicCurve)
%
% nomDirAll = 'H:\QUOI-SSc\EM2040\PovertyBay\Survey\data';
% QUOI_ComputeIndividualBS(nomDirAll, 3, Tag, NomFicCurve)
%
%
%
% NomFicCurve = 'TODO';
%
% nomDirAll = 'H:\Calypso-SSc\EM302\PovertyBay\Survey\data';
% QUOI_ComputeIndividualBS(nomDirAll, 3, Tag, NomFicCurve)
%
% nomDirAll = 'H:\Calypso-SSc\EM302\PovertyBay\Star1\data';
% QUOI_ComputeIndividualBS(nomDirAll, 1.5, Tag, NomFicCurve)
%
% nomDirAll = 'H:\Calypso-SSc\EM302\PovertyBay\Star2\data';
% QUOI_ComputeIndividualBS(nomDirAll, 1.5, Tag, NomFicCurve)
%
% nomDirAll = 'H:\Calypso-SSc\EM302\PovertyBay\Star3\data';
% QUOI_ComputeIndividualBS(nomDirAll, 2.5, Tag, NomFicCurve)
%
% nomDirAll = 'H:\Calypso-SSc\EM302\PovertyBay\Star4\data';
% QUOI_ComputeIndividualBS(nomDirAll, 2, Tag, NomFicCurve)
%
% nomDirAll = 'H:\Calypso-SSc\EM302\BayOfPlenty\Leg1\data';
% QUOI_ComputeIndividualBS(nomDirAll, 5, Tag, NomFicCurve)
%
% nomDirAll = 'H:\Calypso-SSc\EM302\BayOfPlenty\Leg2\data';
% QUOI_ComputeIndividualBS(nomDirAll, 2.5, Tag, NomFicCurve)

function QUOI_ComputeIndividualBS(this, indImage, nomDirAll, gridSize, Tag, NomFicCurve)

listeFicAll = listeFicOnDir(nomDirAll, '.all');

nomDir = fileparts(listeFicAll{1});
nomDir = fullfile(fileparts(nomDir), ['Lines-Reflectivity-' Tag]);
if ~exist(nomDir, 'dir')
    [flag, msg] = mkdir(nomDir);
    if ~flag
        my_warndlg(msg, 1);
        return
    end
end

InfoMos.TypeMos            = 2; % Mosaiques individuelles
InfoMos.TypeGrid           = 1; % Grid size unique
InfoMos.flagMasquageDual   = 0;
InfoMos.LimitAngles        = [];
InfoMos.gridSize           = gridSize;
InfoMos.skipExistingMosaic = 0;

InfoMos.Import.MasqueActif  = 1; % On tient compte des flags d'invalidité des sondes
InfoMos.Import.SlotFilename = [];
InfoMos.Import.SonarTime    = [];
InfoMos.LimitAngles         = [];

InfoCompensationCurve.FileName                 = NomFicCurve;
InfoCompensationCurve.ModeDependant            = 0;
InfoCompensationCurve.UseModel                 = 0;
InfoCompensationCurve.PreserveMeanValueIfModel = [];

InfoMos.Covering.Priority           = 1; % Priorité au centre
InfoMos.Covering.SpecularLimitAngle = [];

InfoMos.DirName = nomDir;

SimradAllMosaiqueParProfil(this, indImage, listeFicAll, ...
    0, 'ReflectivityFromSnippets', [3 3], 0, InfoCompensationCurve, InfoMos, 'TideCorrection', 1);

fprintf('Le calcul des MNT individuels pour "%s" est terminé\n\n', nomDirAll);
