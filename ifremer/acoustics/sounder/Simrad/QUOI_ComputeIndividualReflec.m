% Tag = 'Comp';
% NomFicCurve = 'H:\QUOI-SSc\EM302\BayOfPlenty\SurveyN\Handcraft\Mode_2_Swath_Single_Signal_CW_Reflectivity_BeamPointingAngle_TxBeamIndexSwath.mat';
%
% nomDirAll = 'H:\QUOI-SSc\EM302\BayOfPlenty\BoxFOI1\data';
% QUOI_ComputeIndividualReflec(nomDirAll, 3, Tag, NomFicCurve)
%
% nomDirAll = 'H:\QUOI-SSc\EM302\BayOfPlenty\BoxFOI2\data';
% QUOI_ComputeIndividualReflec(nomDirAll, 3, Tag, NomFicCurve)
%
% nomDirAll = 'H:\QUOI-SSc\EM302\BayOfPlenty\BoxSSG\data';
% QUOI_ComputeIndividualReflec(nomDirAll, 1, Tag, NomFicCurve)
%
% nomDirAll = 'H:\QUOI-SSc\EM302\BayOfPlenty\SurveySE\data';
% QUOI_ComputeIndividualReflec(nomDirAll, 3.5, Tag, NomFicCurve)
%
% nomDirAll = 'H:\QUOI-SSc\EM302\BayOfPlenty\SurveySW\data';
% QUOI_ComputeIndividualReflec(nomDirAll, 3.5, Tag, NomFicCurve)
%
% nomDirAll = 'H:\QUOI-SSc\EM302\BayOfPlenty\SurveyN\data';
% QUOI_ComputeIndividualReflec(nomDirAll, 3.5, Tag, NomFicCurve)
%
% nomDirAll = 'H:\QUOI-SSc\EM302\PovertyBay\Box\data';
% QUOI_ComputeIndividualReflec(nomDirAll, 3, Tag, NomFicCurve)
%
% nomDirAll = 'H:\QUOI-SSc\EM302\PovertyBay\Survey\data';
% QUOI_ComputeIndividualReflec(nomDirAll, 3, Tag, NomFicCurve)
%
%
%
% NomFicCurve = 'H:\QUOI-SSc\EM2040\BayOfPlenty\SurveyN\Handcraft\EM2040-Compensation_curve_Mean_curve_Clean_1_Reflectivity_BeamPointingAngle.mat';
%
% nomDirAll = 'H:\QUOI-SSc\EM2040\BayOfPlenty\BoxFOI1\data';
% QUOI_ComputeIndividualReflec(nomDirAll, 3, Tag, NomFicCurve)
%
% nomDirAll = 'H:\QUOI-SSc\EM2040\BayOfPlenty\BoxFOI2\data';
% QUOI_ComputeIndividualReflec(nomDirAll, 3, Tag, NomFicCurve)
%
% nomDirAll = 'H:\QUOI-SSc\EM2040\BayOfPlenty\BoxSSG\data';
% QUOI_ComputeIndividualReflec(nomDirAll, 1, Tag, NomFicCurve)
%
% nomDirAll = 'H:\QUOI-SSc\EM2040\BayOfPlenty\SurveySE\data';
% QUOI_ComputeIndividualReflec(nomDirAll, 3.5, Tag, NomFicCurve)
%
% nomDirAll = 'H:\QUOI-SSc\EM2040\BayOfPlenty\SurveySW\data';
% QUOI_ComputeIndividualReflec(nomDirAll, 3.5, Tag, NomFicCurve)
%
% nomDirAll = 'H:\QUOI-SSc\EM2040\BayOfPlenty\SurveyN\data';
% QUOI_ComputeIndividualReflec(nomDirAll, 3.5, Tag, NomFicCurve)
%
% nomDirAll = 'H:\QUOI-SSc\EM2040\PovertyBay\Box\data';
% QUOI_ComputeIndividualReflec(nomDirAll, 3, Tag, NomFicCurve)
%
% nomDirAll = 'H:\QUOI-SSc\EM2040\PovertyBay\Survey\data';
% QUOI_ComputeIndividualReflec(nomDirAll, 3, Tag, NomFicCurve)
%
%
%
% NomFicCurve = 'H:\Calypso-SSc\EM302\PovertyBay\Survey\Handicraft\Calypso-PovertyBay-Survey-CompensationCurve_Reflectivity_BeamPointingAngle_TxBeamIndexSwath.mat';
%
% nomDirAll = 'H:\Calypso-SSc\EM302\PovertyBay\Survey\data';
% QUOI_ComputeIndividualReflec(nomDirAll, 3, Tag, NomFicCurve)
%
% nomDirAll = 'H:\Calypso-SSc\EM302\PovertyBay\Star1\data';
% QUOI_ComputeIndividualReflec(nomDirAll, 1.5, Tag, NomFicCurve)
%
% nomDirAll = 'H:\Calypso-SSc\EM302\PovertyBay\Star2\data';
% QUOI_ComputeIndividualReflec(nomDirAll, 1.5, Tag, NomFicCurve)
%
% nomDirAll = 'H:\Calypso-SSc\EM302\PovertyBay\Star3\data';
% QUOI_ComputeIndividualReflec(nomDirAll, 2.5, Tag, NomFicCurve)
%
% nomDirAll = 'H:\Calypso-SSc\EM302\PovertyBay\Star4\data';
% QUOI_ComputeIndividualReflec(nomDirAll, 2, Tag, NomFicCurve)
%
% nomDirAll = 'H:\Calypso-SSc\EM302\BayOfPlenty\Leg1\data';
% QUOI_ComputeIndividualReflec(nomDirAll, 5, Tag, NomFicCurve)
%
% nomDirAll = 'H:\Calypso-SSc\EM302\BayOfPlenty\Leg2\data';
% QUOI_ComputeIndividualReflec(nomDirAll, 2.5, Tag, NomFicCurve)

function QUOI_ComputeIndividualReflec(nomDirAll, gridSize, Tag, NomFicCurve)

I0 = cl_image_I0;

listeFicAll = listeFicOnDir(nomDirAll, '.all');

nomDir = fileparts(listeFicAll{1});
nomDir = fullfile(fileparts(nomDir), ['Lines-Reflectivity-' Tag]);
if ~exist(nomDir, 'dir')
    [flag, msg] = mkdir(nomDir);
    if ~flag
        my_warndlg(msg, 1);
        return
    end
end

InfoMos.TypeMos            = 2; % Mosaiques individuelles
InfoMos.TypeGrid           = 1; % Grid size unique
InfoMos.flagMasquageDual   = 0;
InfoMos.LimitAngles        = [];
InfoMos.gridSize           = gridSize;
InfoMos.skipExistingMosaic = 0;

InfoMos.Import.MasqueActif  = 1; % On tient compte des flags d'invalidité des sondes
InfoMos.Import.SlotFilename = [];
InfoMos.Import.SonarTime    = [];
InfoMos.LimitAngles         = [];

InfoCompensationCurve.FileName                 = NomFicCurve;
InfoCompensationCurve.ModeDependant            = 0;
InfoCompensationCurve.UseModel                 = 0;
InfoCompensationCurve.PreserveMeanValueIfModel = [];

InfoMos.Covering.Priority           = 1; % Priorité au centre
InfoMos.Covering.SpecularLimitAngle = [];

InfoMos.DirName = nomDir;

SimradAllMosaiqueParProfil(I0, 1, listeFicAll, ...
    0, 'ReflectivityFromSnippets', [3 3], 0, InfoCompensationCurve, InfoMos, 'TideCorrection', 1);

fprintf('Le calcul des MNT individuels pour "%s" est terminé\n\n', nomDirAll);
