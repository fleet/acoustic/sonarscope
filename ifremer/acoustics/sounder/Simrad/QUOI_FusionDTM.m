% nomDirLinesBathy = 'H:\QUOI-SSc\EM302\BayOfPlenty\BoxFOI1\Lines-Bathy';
% QUOI_FusionDTM(nomDirLinesBathy, 'EM302-BayOfPlenty-BoxFOI1');
% nomDirLinesBathy = 'H:\QUOI-SSc\EM302\BayOfPlenty\BoxFOI2\Lines-Bathy';
% QUOI_FusionDTM(nomDirLinesBathy, 'EM302-BayOfPlenty-BoxFOI2');
% nomDirLinesBathy = 'H:\QUOI-SSc\EM302\BayOfPlenty\BoxSSG\Lines-Bathy';
% QUOI_FusionDTM(nomDirLinesBathy, 'EM302-BayOfPlenty-BoxSSG');
% nomDirLinesBathy = 'H:\QUOI-SSc\EM302\BayOfPlenty\SurveySE\Lines-Bathy';
% QUOI_FusionDTM(nomDirLinesBathy, 'EM302-BayOfPlenty-SurveySE');
% nomDirLinesBathy = 'H:\QUOI-SSc\EM302\BayOfPlenty\SurveySW\Lines-Bathy';
% QUOI_FusionDTM(nomDirLinesBathy, 'EM302-BayOfPlenty-SurveySW');
% nomDirLinesBathy = 'H:\QUOI-SSc\EM302\BayOfPlenty\SurveyN\Lines-Bathy';
% QUOI_FusionDTM(nomDirLinesBathy, 'EM302-BayOfPlenty-SurveyN');
% nomDirLinesBathy = 'H:\QUOI-SSc\EM302\PovertyBay\Box\Lines-Bathy';
% QUOI_FusionDTM(nomDirLinesBathy, 'EM302-PovertyBay-Box');
% nomDirLinesBathy = 'H:\QUOI-SSc\EM302\PovertyBay\Survey\Lines-Bathy';
% QUOI_FusionDTM(nomDirLinesBathy, 'EM302-PovertyBay-Survey');
%
% nomDirLinesBathy = 'H:\QUOI-SSc\EM2040\BayOfPlenty\BoxFOI1\Lines-Bathy';
% QUOI_FusionDTM(nomDirLinesBathy, 'EM2040-BayOfPlenty-BoxFOI1');
% nomDirLinesBathy = 'H:\QUOI-SSc\EM2040\BayOfPlenty\BoxFOI2\Lines-Bathy';
% QUOI_FusionDTM(nomDirLinesBathy, 'EM2040-BayOfPlenty-BoxFOI2');
% nomDirLinesBathy = 'H:\QUOI-SSc\EM2040\BayOfPlenty\BoxSSG\Lines-Bathy';
% QUOI_FusionDTM(nomDirLinesBathy, 'EM2040-BayOfPlenty-BoxSSG');
% nomDirLinesBathy = 'H:\QUOI-SSc\EM2040\BayOfPlenty\SurveySE\Lines-;Bathy';
% QUOI_FusionDTM(nomDirLinesBathy, 'EM2040-BayOfPlenty-SurveySE')
% nomDirLinesBathy = 'H:\QUOI-SSc\EM2040\BayOfPlenty\SurveySW\Lines-Bathy';
% QUOI_FusionDTM(nomDirLinesBathy, 'EM2040-BayOfPlenty-SurveySW');
% nomDirLinesBathy = 'H:\QUOI-SSc\EM2040\BayOfPlenty\SurveyN\Lines-Bathy';
% QUOI_FusionDTM(nomDirLinesBathy, 'EM2040-BayOfPlenty-SurveyN');
% nomDirLinesBathy = 'H:\QUOI-SSc\EM2040\PovertyBay\Box\Lines-Bathy';
% QUOI_FusionDTM(nomDirLinesBathy, 'EM2040-PovertyBay-Box');
% nomDirLinesBathy = 'H:\QUOI-SSc\EM2040\PovertyBay\Survey\Lines-Bathy';
% QUOI_FusionDTM(nomDirLinesBathy, 'EM2040-PovertyBay-Survey');
%
% nomDirLinesBathy = 'H:\Calypso-SSc\EM302\BayOfPlenty\Leg1\Lines-Bathy';
% QUOI_FusionDTM(nomDirLinesBathy, 'EM302-BayOfPlenty-CalypsoLeg1');
% nomDirLinesBathy = 'H:\Calypso-SSc\EM302\BayOfPlenty\Leg2\Lines-Bathy';
% QUOI_FusionDTM(nomDirLinesBathy, 'EM302-BayOfPlenty-CalypsoLeg2');
% nomDirLinesBathy = 'H:\Calypso-SSc\EM302\PovertyBay\Survey\Lines-Bathy';
% QUOI_FusionDTM(nomDirLinesBathy, 'EM302-PovertyBay-CalypsoSurvey');
% nomDirLinesBathy = 'H:\Calypso-SSc\EM302\PovertyBay\Star1\Lines-Bathy';
% QUOI_FusionDTM(nomDirLinesBathy, 'EM302-PovertyBay-CalypsoStar1');
% nomDirLinesBathy = 'H:\Calypso-SSc\EM302\PovertyBay\Star2\Lines-Bathy';
% QUOI_FusionDTM(nomDirLinesBathy, 'EM302-PovertyBay-CalypsoStar2');
% nomDirLinesBathy = 'H:\Calypso-SSc\EM302\PovertyBay\Star3\Lines-Bathy';
% QUOI_FusionDTM(nomDirLinesBathy, 'EM302-PovertyBay-CalypsoStar3');
% nomDirLinesBathy = 'H:\Calypso-SSc\EM302\PovertyBay\Star4\Lines-Bathy';
% QUOI_FusionDTM(nomDirLinesBathy, 'EM302-PovertyBay-CalypsoStar4');

function Out = QUOI_FusionDTM(nomDirLinesBathy, prefix)

I0 = cl_image_I0;

%% Recherche des fichiers .ers du r�pertoire

listeFicLinesBathyErs = listeFicOnDir(nomDirLinesBathy, '.ers', 'Filtre', '_Bathymetry_LatLong');

%% Lecture des images

[flag, b] = cl_image.import_ermapper(listeFicLinesBathyErs);
if ~flag
    return
end
nbImages = length(b);

%% Moyennage des images

ImageName = sprintf('%s_Mean%dImages', prefix, nbImages);
[flag, a] = MultiImages_Mean(b, ImageName, 'NoWaitbar', 1);
if ~flag
    return
end

%% Export de l'image moyenn�e et de l'image du nombre de points moyenn�s

nomDirFusion = fullfile(nomDirLinesBathy, 'Fusion');
if ~exist(nomDirFusion, 'dir')
    [flag, msg] = mkdir(nomDirFusion);
    if ~flag
        my_warndlg(msg, 1);
        return
    end
end

ImageName = a(1).Name;
nomFicOut = fullfile(nomDirFusion, [ImageName '.ers']);
flag = export_ermapper(a(1), nomFicOut);
if ~flag
    return
end

ImageName = a(2).Name;
nomFicOut = fullfile(nomDirFusion, [ImageName '.ers']);
flag = export_ermapper(a(2), nomFicOut);
if ~flag
    return
end

%% Calcul de l'image m�diane

ImageName = sprintf('%s_Median%dImages', prefix, nbImages);
[flag, c] = MultiImages_Median(b, ImageName, 'NoWaitbar', 1);
if ~flag
    return
end

%% Export de l'image moyenn�e et de l'image du nombre de points moyenn�s

ImageName = c(1).Name;
nomFicOut = fullfile(nomDirFusion, [ImageName '.ers']);
flag = export_ermapper(c(1), nomFicOut);
if ~flag
    return
end

Out = [c(1), a];
SonarScope(Out)

%% The End

fprintf('La fusion des MNT individuels pour "%s" est termin�e\n\n', nomDirLinesBathy);
