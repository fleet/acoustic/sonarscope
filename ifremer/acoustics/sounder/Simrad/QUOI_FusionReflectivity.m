% nomDirLines = 'H:\QUOI-SSc\EM302\BayOfPlenty\BoxFOI1\Lines-Reflectivity-Comp';
% QUOI_FusionReflectivity(nomDirLines, 'EM302-BayOfPlenty-BoxFOI1')
%
% nomDirLines = 'H:\QUOI-SSc\EM302\BayOfPlenty\BoxFOI2\Lines-Reflectivity-Comp';
% QUOI_FusionReflectivity(nomDirLines, 'EM302-BayOfPlenty-BoxFOI2')
%
% nomDirLines = 'H:\QUOI-SSc\EM302\BayOfPlenty\BoxSSG\Lines-Reflectivity-Comp';
% QUOI_FusionReflectivity(nomDirLines, 'EM302-BayOfPlenty-BoxSSG')
%
% nomDirLines = 'H:\QUOI-SSc\EM302\BayOfPlenty\SurveySE\Lines-Reflectivity-Comp';
% QUOI_FusionReflectivity(nomDirLines, 'EM302-BayOfPlenty-SurveySE')
%
% nomDirLines = 'H:\QUOI-SSc\EM302\BayOfPlenty\SurveySW\Lines-Reflectivity-Comp';
% QUOI_FusionReflectivity(nomDirLines, 'EM302-BayOfPlenty-SurveySW')
%
% nomDirLines = 'H:\QUOI-SSc\EM302\BayOfPlenty\SurveyN\Lines-Reflectivity-Comp';
% QUOI_FusionReflectivity(nomDirLines, 'EM302-BayOfPlenty-SurveyN')
%
% nomDirLines = 'H:\QUOI-SSc\EM302\PovertyBay\Box\Lines-Reflectivity-Comp';
% QUOI_FusionReflectivity(nomDirLines, 'EM302-PovertyBay-Box')
%
% nomDirLines = 'H:\QUOI-SSc\EM302\PovertyBay\Survey\Lines-Reflectivity-Comp';
% QUOI_FusionReflectivity(nomDirLines, 'EM302-PovertyBay-Survey')
%
%
%
% nomDirLines = 'H:\QUOI-SSc\EM2040\BayOfPlenty\BoxFOI1\Lines-Reflectivity-Comp';
% QUOI_FusionReflectivity(nomDirLines, 'EM2040-BayOfPlenty-BoxFOI1')
%
% nomDirLines = 'H:\QUOI-SSc\EM2040\BayOfPlenty\BoxFOI2\Lines-Reflectivity-Comp';
% QUOI_FusionReflectivity(nomDirLines, 'EM2040-BayOfPlenty-BoxFOI2')
%
% nomDirLines = 'H:\QUOI-SSc\EM2040\BayOfPlenty\BoxSSG\Lines-Reflectivity-Comp';
% QUOI_FusionReflectivity(nomDirLines, 'EM2040-BayOfPlenty-BoxSSG')
%
% nomDirLines = 'H:\QUOI-SSc\EM2040\BayOfPlenty\SurveySE\Lines-Reflectivity-Comp';
% QUOI_FusionReflectivity(nomDirLines, 'EM2040-BayOfPlenty-SurveySE')
%
% nomDirLines = 'H:\QUOI-SSc\EM2040\BayOfPlenty\SurveySW\Lines-Reflectivity-Comp';
% QUOI_FusionReflectivity(nomDirLines, 'EM2040-BayOfPlenty-SurveySW')
%
% nomDirLines = 'H:\QUOI-SSc\EM2040\BayOfPlenty\SurveyN\Lines-Reflectivity-Comp';
% QUOI_FusionReflectivity(nomDirLines, 'EM2040-BayOfPlenty-SurveyN')
%
% nomDirLines = 'H:\QUOI-SSc\EM2040\PovertyBay\Box\Lines-Reflectivity-Comp';
% QUOI_FusionReflectivity(nomDirLines, 'EM2040-PovertyBay-Box')
%
% nomDirLines = 'H:\QUOI-SSc\EM2040\PovertyBay\Survey\Lines-Reflectivity-Comp';
% QUOI_FusionReflectivity(nomDirLines, 'EM2040-PovertyBay-Survey')
%
%
%
% nomDirLines = 'H:\Calypso-SSc\EM302\PovertyBay\Survey\Lines-Reflectivity-Comp';
% QUOI_FusionReflectivity(nomDirLines, 'EM302-PovertyBay-CalypsoSurvey')
%
% nomDirLines = 'H:\Calypso-SSc\EM302\PovertyBay\Star1\Lines-Reflectivity-Comp';
% QUOI_FusionReflectivity(nomDirLines, 'EM302-PovertyBay-CalypsoStar1')
%
% nomDirLines = 'H:\Calypso-SSc\EM302\PovertyBay\Star2\Lines-Reflectivity-Comp';
% QUOI_FusionReflectivity(nomDirLines, 'EM302-PovertyBay-CalypsoStar2')
%
% nomDirLines = 'H:\Calypso-SSc\EM302\PovertyBay\Star3\Lines-Reflectivity-Comp';
% QUOI_FusionReflectivity(nomDirLines, 'EM302-PovertyBay-CalypsoStar3')
%
% nomDirLines = 'H:\Calypso-SSc\EM302\PovertyBay\Star4\Lines-Reflectivity-Comp';
% QUOI_FusionReflectivity(nomDirLines, 'EM302-PovertyBay-CalypsoStar4')
%
% nomDirLines = 'H:\Calypso-SSc\EM302\BayOfPlenty\Leg1\Lines-Reflectivity-Comp';
% QUOI_FusionReflectivity(nomDirLines, 'EM302-PBayOfPlenty-CalypsoLeg1')
%
% nomDirLines = 'H:\Calypso-SSc\EM302\BayOfPlenty\Leg2\Lines-Reflectivity-Comp';
% QUOI_FusionReflectivity(nomDirLines, 'EM302-PBayOfPlenty-CalypsoLeg2')

function QUOI_FusionReflectivity(nomDirLines, prefix, varargin)

[varargin, valInterval] = getPropertyValue(varargin, 'valInterval', []); %#ok<ASGLU>

%% Recherche des fichiers .ers du r�pertoire

listeFicLinesBathyErs = listeFicOnDir(nomDirLines, '.ers', 'Filtre', '_Reflectivity_LatLong');
if isempty(listeFicLinesBathyErs)
    return
end

%% Lecture des images

[flag, b] = cl_image.import_ermapper(listeFicLinesBathyErs);
if ~flag
    return
end
nbImages = length(b);

%% Masquage d�termin� � partir des donn�es d'angles

if ~isempty(valInterval)
    listeFicLinesAnglesErs = listeFicOnDir(nomDirLines, '.ers', 'Filtre', '_RxBeamAngle_LatLong');
    if isempty(listeFicLinesAnglesErs)
        return
    end
    [flag, M] = cl_image.import_ermapper(listeFicLinesAnglesErs);
    if ~flag
        return
    end
    M =  ROI_auto(M, 1, 'Mask', [], [], valInterval, 1);
    b = masquage(b, 'LayerMask', M, 'valMask', 1, 'zone', 2, 'NoWaitbar', 1);
end

%% Moyennage des images

ImageName = sprintf('%s_Mean%dImages', prefix, nbImages);
[flag, a] = MultiImages_Mean(b, ImageName, 'NoWaitbar', 1);
if ~flag
    return
end

%% Export de l'image moyenn�e et de l'image du nombre de points moyenn�s

nomDirFusion = fullfile(nomDirLines, 'Fusion');
if ~exist(nomDirFusion, 'dir')
    [flag, msg] = mkdir(nomDirFusion);
    if ~flag
        my_warndlg(msg, 1);
        return
    end
end

%% Export de l'image moyenn�e

ImageName = a(1).Name;
nomFicOut = fullfile(nomDirFusion, [ImageName '.ers']);
flag = export_ermapper(a(1), nomFicOut);
if ~flag
    return
end

%% Export du nombre de points moyenn�s

ImageName = a(2).Name;
nomFicOut = fullfile(nomDirFusion, [ImageName '.ers']);
flag = export_ermapper(a(2), nomFicOut);
if ~flag
    return
end

%% Calcul de l'image m�diane

ImageName = sprintf('%s_Median%dImages', prefix, nbImages);
[flag, c] = MultiImages_Median(b, ImageName, 'NoWaitbar', 1);
if ~flag
    return
end

%% Export de l'image m�diane

ImageName = c(1).Name;
nomFicOut = fullfile(nomDirFusion, [ImageName '.ers']);
flag = export_ermapper(c(1), nomFicOut);
if ~flag
    return
end

Out = [c(1), a];
SonarScope(Out)

%% The End

fprintf('La fusion des MNT individuels pour "%s" est termin�e\n\n', nomDirLines);
