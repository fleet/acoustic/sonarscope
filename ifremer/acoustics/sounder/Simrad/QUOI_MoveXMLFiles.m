% nomDirAll = 'H:\QUOI-SSc\EM302\BayOfPlenty\BoxFOI1\data'; % Done
% MoveXMLFiles(nomDirAll)
% nomDirAll = 'H:\QUOI-SSc\EM302\BayOfPlenty\BoxFOI2\data'; % Done
% MoveXMLFiles(nomDirAll)
% nomDirAll = 'H:\QUOI-SSc\EM302\BayOfPlenty\BoxSSG\data'; % Done
% MoveXMLFiles(nomDirAll)
% nomDirAll = 'H:\QUOI-SSc\EM302\BayOfPlenty\SurveySE\data';
% MoveXMLFiles(nomDirAll)
% nomDirAll = 'H:\QUOI-SSc\EM302\BayOfPlenty\SurveySW\data';
% MoveXMLFiles(nomDirAll)
% nomDirAll = 'H:\QUOI-SSc\EM302\BayOfPlenty\SurveyN\data';
% MoveXMLFiles(nomDirAll)
% nomDirAll = 'H:\QUOI-SSc\EM302\PovertyBay\Box\data'; % Done
% MoveXMLFiles(nomDirAll)
% nomDirAll = 'H:\QUOI-SSc\EM302\PovertyBay\Survey\data'; % Done
% MoveXMLFiles(nomDirAll)
%
% nomDirAll = 'H:\QUOI-SSc\EM2040\BayOfPlenty\BoxFOI1\data'; % Done
% MoveXMLFiles(nomDirAll)
% nomDirAll = 'H:\QUOI-SSc\EM2040\BayOfPlenty\BoxFOI2\data'; % Done
% MoveXMLFiles(nomDirAll)
% nomDirAll = 'H:\QUOI-SSc\EM2040\BayOfPlenty\BoxSSG\data'; % Done
% MoveXMLFiles(nomDirAll)
% nomDirAll = 'H:\QUOI-SSc\EM2040\BayOfPlenty\SurveySE\data'; % Done
% MoveXMLFiles(nomDirAll)
% nomDirAll = 'H:\QUOI-SSc\EM2040\BayOfPlenty\SurveySW\data'; % Done
% MoveXMLFiles(nomDirAll)
% nomDirAll = 'H:\QUOI-SSc\EM2040\BayOfPlenty\SurveyN\data'; % Done
% MoveXMLFiles(nomDirAll)
% nomDirAll = 'H:\QUOI-SSc\EM2040\PovertyBay\Box\data'; % Done
% MoveXMLFiles(nomDirAll)
% nomDirAll = 'H:\QUOI-SSc\EM2040\PovertyBay\Survey\data'; % Done
% MoveXMLFiles(nomDirAll)

function QUOI_MoveXMLFiles(nomDirAll)

listeFicAll = listeFicOnDir(nomDirAll, '.all');
nomDirWcOut = fullfile(fileparts(nomDirAll), 'WC-DepthAcrossDist');
if ~exist(nomDirWcOut, 'dir')
    mkdir(nomDirWcOut)
end

nomDirWcIn = 'H:\NIWA-QUOI\WC-QUOI\EM2040\WC-DepthAcrossDist';
listeFicXml = listeFicOnDir(nomDirWcIn, '.xml');

nomDirWcIn = 'G:\NIWA\QUOI\EM302\WC-DepthAcrossDist';
X = listeFicOnDir(nomDirWcIn, '.xml');
listeFicXml = [listeFicXml;  X];

for k=1:length(listeFicAll)
    [~, nomFic] = fileparts(listeFicAll{k});
    sub = find(contains(listeFicXml, nomFic));
    for k2=1:length(sub)
        nomFic = listeFicXml{sub(k2)};
        [flag, MESSAGE] = movefile(nomFic, nomDirWcOut);
        if flag
            fprintf('Copie de "%s" sur "%s" OK\n', nomFic, nomDirWcOut);
        else
            str = sprintf('"%s" could not be copied in "%s" : %s', nomFic, nomDirWcOut, MESSAGE);
            my_warndlg(str, 1);
        end
        
        [nomDir, nomFicWc] = fileparts(nomFic);
        nomDir = fullfile(nomDir, nomFicWc);
        if exist(nomDir, 'dir')
            [flag, MESSAGE] = movefile(nomDir, nomDirWcOut);
            if flag
                fprintf('Copie de "%s" sur "%s" OK\n', nomDir, nomDirWcOut);
            else
                str = sprintf('"%s" could not be copied in "%s" : %s', nomDir, nomDirWcOut, MESSAGE);
                my_warndlg(str, 1);
            end
        end
    end
end
fprintf('Le transfert des donn�es pour "%s" est termin�e\n\n', nomDirAll);
