% nomDirLines = 'H:\QUOI-SSc\EM302\BayOfPlenty\BoxFOI1\Lines-Reflectivity-BS';
% QUOI_ReflectivityByAngleSectors(nomDirLines, 'EM302-BayOfPlenty-BoxFOI1')
%
% nomDirLines = 'H:\QUOI-SSc\EM302\BayOfPlenty\BoxFOI2\Lines-Reflectivity-BS';
% QUOI_ReflectivityByAngleSectors(nomDirLines, 'EM302-BayOfPlenty-BoxFOI2')
%
% nomDirLines = 'H:\QUOI-SSc\EM302\BayOfPlenty\BoxSSG\Lines-Reflectivity-BS';
% QUOI_ReflectivityByAngleSectors(nomDirLines, 'EM302-BayOfPlenty-BoxSSG')
%
% nomDirLines = 'H:\QUOI-SSc\EM302\BayOfPlenty\SurveySE\Lines-Reflectivity-BS';
% QUOI_ReflectivityByAngleSectors(nomDirLines, 'EM302-BayOfPlenty-SurveySE')
%
% nomDirLines = 'H:\QUOI-SSc\EM302\BayOfPlenty\SurveySW\Lines-Reflectivity-BS';
% QUOI_ReflectivityByAngleSectors(nomDirLines, 'EM302-BayOfPlenty-SurveySW')
%
% nomDirLines = 'H:\QUOI-SSc\EM302\BayOfPlenty\SurveyN\Lines-Reflectivity-BS';
% QUOI_ReflectivityByAngleSectors(nomDirLines, 'EM302-BayOfPlenty-SurveyN')
%
% nomDirLines = 'H:\QUOI-SSc\EM302\PovertyBay\Box\Lines-Reflectivity-BS';
% QUOI_ReflectivityByAngleSectors(nomDirLines, 'EM302-PovertyBay-Box')
%
% nomDirLines = 'H:\QUOI-SSc\EM302\PovertyBay\Survey\Lines-Reflectivity-BS';
% QUOI_ReflectivityByAngleSectors(nomDirLines, 'EM302-PovertyBay-Survey')
%
%
%
% nomDirLines = 'H:\QUOI-SSc\EM2040\BayOfPlenty\BoxFOI1\Lines-Reflectivity-BS';
% QUOI_ReflectivityByAngleSectors(nomDirLines, 'EM2040-BayOfPlenty-BoxFOI1')
%
% nomDirLines = 'H:\QUOI-SSc\EM2040\BayOfPlenty\BoxFOI2\Lines-Reflectivity-BS';
% QUOI_ReflectivityByAngleSectors(nomDirLines, 'EM2040-BayOfPlenty-BoxFOI2')
%
% nomDirLines = 'H:\QUOI-SSc\EM2040\BayOfPlenty\BoxSSG\Lines-Reflectivity-BS';
% QUOI_ReflectivityByAngleSectors(nomDirLines, 'EM2040-BayOfPlenty-BoxSSG')
%
% nomDirLines = 'H:\QUOI-SSc\EM2040\BayOfPlenty\SurveySE\Lines-Reflectivity-BS';
% QUOI_ReflectivityByAngleSectors(nomDirLines, 'EM2040-BayOfPlenty-SurveySE')
%
% nomDirLines = 'H:\QUOI-SSc\EM2040\BayOfPlenty\SurveySW\Lines-Reflectivity-BS';
% QUOI_ReflectivityByAngleSectors(nomDirLines, 'EM2040-BayOfPlenty-SurveySW')
%
% nomDirLines = 'H:\QUOI-SSc\EM2040\BayOfPlenty\SurveyN\Lines-Reflectivity-BS';
% QUOI_ReflectivityByAngleSectors(nomDirLines, 'EM2040-BayOfPlenty-SurveyN')
%
% nomDirLines = 'H:\QUOI-SSc\EM2040\PovertyBay\Box\Lines-Reflectivity-BS';
% QUOI_ReflectivityByAngleSectors(nomDirLines, 'EM2040-PovertyBay-Box')
%
% nomDirLines = 'H:\QUOI-SSc\EM2040\PovertyBay\Survey\Lines-Reflectivity-BS';
% QUOI_ReflectivityByAngleSectors(nomDirLines, 'EM2040-PovertyBay-Survey')
%
%
%
% nomDirLines = 'H:\Calypso-SSc\EM302\PovertyBay\Survey\Lines-Reflectivity-BS';
% QUOI_ReflectivityByAngleSectors(nomDirLines, 'EM302-PovertyBay-CalypsoSurvey')
%
% nomDirLines = 'H:\Calypso-SSc\EM302\PovertyBay\Star1\Lines-Reflectivity-BS';
% QUOI_ReflectivityByAngleSectors(nomDirLines, 'EM302-PovertyBay-CalypsoStar1')
%
% nomDirLines = 'H:\Calypso-SSc\EM302\PovertyBay\Star2\Lines-Reflectivity-BS';
% QUOI_ReflectivityByAngleSectors(nomDirLines, 'EM302-PovertyBay-CalypsoStar2')
%
% nomDirLines = 'H:\Calypso-SSc\EM302\PovertyBay\Star3\Lines-Reflectivity-BS';
% QUOI_ReflectivityByAngleSectors(nomDirLines, 'EM302-PovertyBay-CalypsoStar3')
%
% nomDirLines = 'H:\Calypso-SSc\EM302\PovertyBay\Star4\Lines-Reflectivity-BS';
% QUOI_ReflectivityByAngleSectors(nomDirLines, 'EM302-PovertyBay-CalypsoStar4')
%
% nomDirLines = 'H:\Calypso-SSc\EM302\BayOfPlenty\Leg1\Lines-Reflectivity-BS';
% QUOI_ReflectivityByAngleSectors(nomDirLines, 'EM302-PBayOfPlenty-CalypsoLeg1')
%
% nomDirLines = 'H:\Calypso-SSc\EM302\BayOfPlenty\Leg2\Lines-Reflectivity-BS';
% QUOI_ReflectivityByAngleSectors(nomDirLines, 'EM302-PBayOfPlenty-CalypsoLeg2')

function QUOI_ReflectivityByAngleSectors(nomDirLines, prefix, varargin)

[varargin, AngleMin]  = getPropertyValue(varargin, 'AngleMin',  -75);
[varargin, AngleMax]  = getPropertyValue(varargin, 'AngleMax',  75);
[varargin, AngleStep] = getPropertyValue(varargin, 'AngleStep', 10); %#ok<ASGLU>

Teta = AngleMin:AngleStep:(AngleMax-1000*eps);
AngleLim = [Teta' , Teta'+AngleStep];

%% Recherche des fichiers .ers du r�pertoire

ListeFicErs = listeFicOnDir(nomDirLines, '.ers', 'Filtre', '_Reflectivity_LatLong');
if isempty(ListeFicErs)
    return
end

%% R�alisation des mosa�ques par angles

repExport = fullfile(fileparts(nomDirLines), 'Lines-ReflecAngularSectors');
if ~exist(repExport, 'dir')
    mkdir(repExport)
end
NomGenerique = prefix;
Reflectivity_LatLong = SimradAllAssemblageParAngle(repExport, ListeFicErs, NomGenerique, AngleLim, 'Mute', 1);

%% Export des images en Multidata pour Globe3DV

NomDir3DV = fullfile(repExport, '3DV');
if ~exist(NomDir3DV, 'dir')
    mkdir(NomDir3DV)
end

sizeTiff = 32000; %2500;
nomFicMultiImageXML = fullfile(NomDir3DV, [NomGenerique '-ML.xml']);
flag = export_3DVMultiImages(Reflectivity_LatLong, nomFicMultiImageXML, 'sizeTiff', sizeTiff);
if ~flag
    return
end

str = sprintf('The multi data XML file for Globe is copied here : \n%s', nomFicMultiImageXML);
my_warndlg(str, 1);

%% The End

fprintf('Le calcul des mosa�ques par angle pour "%s" est termin�e\n\n', nomDirLines);
