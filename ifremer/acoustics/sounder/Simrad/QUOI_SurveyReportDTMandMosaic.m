% nomDirAll = 'H:\QUOI-SSc\EM302\BayOfPlenty\BoxFOI1\data';
% QUOI_SurveyReportDTMandMosaic(nomDirAll, 'EM302-BayOfPlenty-BoxFOI1', 3)
% nomDirAll = 'H:\QUOI-SSc\EM302\BayOfPlenty\BoxFOI2\data';
% QUOI_SurveyReportDTMandMosaic(nomDirAll, 'EM302-BayOfPlenty-BoxFOI2', 3)
% nomDirAll = 'H:\QUOI-SSc\EM302\BayOfPlenty\BoxSSG\data';
% QUOI_SurveyReportDTMandMosaic(nomDirAll, 'EM302-BayOfPlenty-BoxSSG', 1)
% nomDirAll = 'H:\QUOI-SSc\EM302\BayOfPlenty\SurveySE\data';
% QUOI_SurveyReportDTMandMosaic(nomDirAll, 'EM302-BayOfPlenty-SurveySE', 3.5)
% nomDirAll = 'H:\QUOI-SSc\EM302\BayOfPlenty\SurveySW\data';
% QUOI_SurveyReportDTMandMosaic(nomDirAll, 'EM302-BayOfPlenty-SurveySW', 3.5)
% nomDirAll = 'H:\QUOI-SSc\EM302\BayOfPlenty\SurveyN\data';
% QUOI_SurveyReportDTMandMosaic(nomDirAll, 'EM302-BayOfPlenty-SurveyN', 3.5)
% nomDirAll = 'H:\QUOI-SSc\EM302\PovertyBay\Box\data';
% QUOI_SurveyReportDTMandMosaic(nomDirAll, 'EM302-PovertyBay-Box', 3)
% nomDirAll = 'H:\QUOI-SSc\EM302\PovertyBay\Survey\data';
% QUOI_SurveyReportDTMandMosaic(nomDirAll, 'EM302-PovertyBay-Survey', 3)
%
% nomDirAll = 'H:\QUOI-SSc\EM2040\BayOfPlenty\BoxFOI1\data';
% QUOI_SurveyReportDTMandMosaic(nomDirAll, 'EM2040-BayOfPlenty-BoxFOI1', 3)
% nomDirAll = 'H:\QUOI-SSc\EM2040\BayOfPlenty\BoxFOI2\data';
% QUOI_SurveyReportDTMandMosaic(nomDirAll, 'EM2040-BayOfPlenty-BoxFOI2', 3)
% nomDirAll = 'H:\QUOI-SSc\EM2040\BayOfPlenty\BoxSSG\data';
% QUOI_SurveyReportDTMandMosaic(nomDirAll, 'EM2040-BayOfPlenty-BoxSSG', 1)
% nomDirAll = 'H:\QUOI-SSc\EM2040\BayOfPlenty\SurveySE\data';
% QUOI_SurveyReportDTMandMosaic(nomDirAll, 'EM2040-BayOfPlenty-SurveySE', 3.5)
% nomDirAll = 'H:\QUOI-SSc\EM2040\BayOfPlenty\SurveySW\data';
% QUOI_SurveyReportDTMandMosaic(nomDirAll, 'EM2040-BayOfPlenty-SurveySW', 3.5)
% nomDirAll = 'H:\QUOI-SSc\EM2040\BayOfPlenty\SurveyN\data';
% QUOI_SurveyReportDTMandMosaic(nomDirAll, 'EM2040-BayOfPlenty-SurveyN', 3.5)
% nomDirAll = 'H:\QUOI-SSc\EM2040\PovertyBay\Box\data';
% QUOI_SurveyReportDTMandMosaic(nomDirAll, 'EM2040-PovertyBay-Box', 3)
% nomDirAll = 'H:\QUOI-SSc\EM2040\PovertyBay\Survey\data';
% QUOI_SurveyReportDTMandMosaic(nomDirAll, 'EM2040-PovertyBay-Survey', 3)

function QUOI_SurveyReportDTMandMosaic(nomDirAll, nomAdoc, gridSize)

listeFicAll = listeFicOnDir(nomDirAll, '.all');
if isempty(listeFicAll)
    return
end

nomDir = fileparts(listeFicAll{1});
nomDir = fileparts(nomDir);
nomFicAdoc = fullfile(nomDir, 'SurveyReport', [nomAdoc '.adoc']); % EM2040-BayOfPlenty-BoxFOI1.adoc
if ~exist(nomFicAdoc, 'file')
    return
end

ALL.Process.SurveyReport_GlobalMosaic(nomFicAdoc, listeFicAll, gridSize)

fprintf('Le calcul du MNT et de la mosa�que pour "%s" est termin�\n\n', nomDirAll);
