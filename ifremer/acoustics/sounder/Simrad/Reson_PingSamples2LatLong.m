% Realisation de la mosaique de donnees des "SeabedImage" d'une campagne
%
% Syntax
%   [flag, Mosa, nomDir] = Reson_PingSamples2LatLong(...)
%
% Name-Value Pair Arguments
%   nomDir : Nom du r�pertoire des donnees
%   resol  : Pas de la grille en m�tres
%   Backup : P�riode de sauvegarde des fichiers de sortie
%
% Output Arguments
%   []     : Auto-plot activation
%   flag   : Description du parametre (unite).
%   a      : Instances de cl_image contenant la mosaique et la mosaique angulaire
%   nomDir : Nom du r�pertoire des images cr��es
%
% Examples
%   Reson_PingSamples2LatLong
%   flag, a] = Reson_PingSamples2LatLong('NomDir', '/home1/jackson/FEMME2005/Donnees', 'Resol', 10);
%   SonarScope(a)
%   [flag, a] = Reson_PingSamples2LatLong('NomDir', 'D:\NIWA_DATA\TAN0105', 'Resol', 10, 'Backup', 5);
%   SonarScope(a)
%
% See also Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function [flag, Mosa, nomDir] = Reson_PingSamples2LatLong(varargin)

Mosa = [];

[varargin, XLim]          = getPropertyValue(varargin, 'XLim',        []);
[varargin, YLim]          = getPropertyValue(varargin, 'YLim',        []);
[varargin, resol]         = getPropertyValue(varargin, 'Resol',       []);
% [varargin, Backup]      = getPropertyValue(varargin, 'Backup',      []);
[varargin, nomDir]        = getPropertyValue(varargin, 'NomDir',      []);
[varargin, window]        = getPropertyValue(varargin, 'window',      []);
% [varargin, MasqueActif] = getPropertyValue(varargin, 'MasqueActif', []);
[varargin, CLimReflec]    = getPropertyValue(varargin, 'CLimReflec',  []); %#ok<ASGLU>

E0 = cl_ermapper([]);

% if isempty(MasqueActif)
%     [MasqueActif, flag] = my_questdlg('Use Mask ?');
%     if ~flag
%         return
%     end
% end

if isempty(nomDir)
    nomDir = pwd;
end

%% Recherche des fichiers .all

[flag, liste] = uiSelectFiles('ExtensionFiles', '.s7k', 'RepDefaut', nomDir);
if ~flag || isempty(liste)
    return
end
nbProfils = length(liste);
nomDir = fileparts(liste{1});


%% Seuil de rejection des valeur de r�flectivit�

if isempty(CLimReflec)
    str1 = 'Bornes de validation de la r�flectivit�.';
    str2 = 'Limits of the valid reflectivity values.';
    p(1) = ClParametre('Name', Lang('Valeur min','Min value'), ...
        'Unit', 'dB',  'MinValue', -100, 'MaxValue', 10, 'Value', -50);
    p(2) = ClParametre('Name', Lang('Valeur max','Max value'), ...
        'Unit', 'dB',  'MinValue', -100, 'MaxValue', 10, 'Value', 0);
    a = StyledParametreDialog('params', p, 'Title', Lang(str1,str2));
    % a.Help = 'http://flotte.ifremer.fr/fleet/Presentation-of-the-fleet/On-board-software/SonarScope';
    a.sizes = [0 -3 -1 -2 -1 -1 -1 0];
    a.openDialog;
    flag = a.okPressedOut;
    if ~flag
        return
    end
    CLimReflec = a.getParamsValue;
end

%% Saisie de la r�solution

[flag, resol] = get_gridSize('Value', -resol);
if ~flag
    return
end

%% Backup frequency

[flag, Backup] = saisie_backupFrequency(nbProfils);
if ~flag
    return
end

%% Across distance interpolation method

[flag, AcrossInterpolation] = question_AcrossDistanceInterpolation('QL', 3);
if ~flag
    return
end

%% Mosaic interpolation window

[flag, window] = question_MosaicInterpolationWindow(window);
if ~flag
    return
end

%% Noms du fichier de sauvegarde de la mosaique de r�flectivit�

DataType = cl_image.indDataType('Reflectivity');
[nomFicErMapperLayer, flag] = initNomFicErs(E0, 'Mosaic', cl_image.indGeometryType('LatLong'), DataType, nomDir, 'resol', resol);
if ~flag
    return
end
[nomDir, TitreDepth] = fileparts(nomFicErMapperLayer);

%% Noms du fichier de sauvegarde de la mosaique des angles d'�mission

DataType = cl_image.indDataType('TxAngle');
[nomFicErMapperAngle, flag] = initNomFicErs(E0, TitreDepth, cl_image.indGeometryType('LatLong'), DataType, nomDir, 'confirm', (get_LevelQuestion >= 3));
if ~flag
    return
end
[pppp, TitreAngle] = fileparts(nomFicErMapperAngle);%#ok

%% R�cup�ration des mosaiques existantes

Carto = [];
if exist(nomFicErMapperLayer, 'file') && exist(nomFicErMapperAngle, 'file')
    [flag, rep] = question_CompleteExistingMosaic;
    if ~flag
        return
    end
    if rep == 1
        [flag, Z_Mosa_All] = cl_image.import_ermapper(nomFicErMapperLayer);
        if ~flag
            return
        end
        Carto = get(Z_Mosa_All, 'Carto');
        
        [flag, A_Mosa_All]  = cl_image.import_ermapper(nomFicErMapperAngle);
        if ~flag
            return
        end
        
    else
        Z_Mosa_All = [];
        A_Mosa_All = [];
    end
else
    Z_Mosa_All = [];
    A_Mosa_All = [];
end

%% Si Mosaique de Reflectivite, on propose de faire une compensation
% statistique ou une calibration

% [flag, TypeCalibration, CorFile, bilan, DataTypeConditions, alpha, ModelTxDiag, CourbeBS] = params_MBESCompensation(nomDir);
[flag, TypeCalibration, CorFile, bilan, DataTypeConditions, alpha, ModelTxDiag, CourbeBS] = params_SonarCompensation(nomDir);
if ~flag
    return
end

%%

[flag, RepSave] = get_infoSauvegardeImages(nomDir);
if ~flag
    return
end

%% Masquage de la bathy

str1 = sprintf('S�lection des bits du facteur de qualit� � utiliser pour masquer la donn�e de bathy');
str2 = sprintf('Quality factors selected for Depth validation.');
strMaskBits = {'Brightness'; 'Colinearity'; 'Amplitude'; 'Phase'; 'Do not Use'; 'Nadir Filter'; 'Mask by Histogram'};
[Bits, flag] = my_listdlgMultiple(Lang(str1,str2), strMaskBits, 'InitialValue', [1 6 7]);
if ~flag
    return
end

% profile on
% tic
for k=1:nbProfils
    str = sprintf('Processing file %d/%d : %s', k, nbProfils, liste{k});
    fprintf('%s\n', str);
    
    %% Lecture des layers "Range", "AcrossDistance", "AlongDistance" et "BeamDepressionAngle"
    
    s7k = cl_reson_s7k('nomFic', liste{k}, 'KeepNcID', true);
    if isempty(s7k)
        continue
    end
    
    [c, Carto] = view_depth(s7k, 'Carto', Carto, 'ListeLayers', 1:12, 'Bits', Bits, 'setMaskToAllLayers', true);
    
    nbRows = c(1).nbRows;
    if nbRows <= 1
        Message = sprintf('The file %s is discarded because it has %d row.', liste{k}, nbRows);
        my_warndlg(Message, 0);
        continue
    end
    
    TxBeamIndex_PingAcrossDist = [];
    
    Range_PingBeam          = c(6);
    AcrossDistance_PingBeam = c(2);
    AlongDistance_PingBeam  = c(3);
    TxAngle_PingBeam        = c(4);
    TxBeamIndex_PingBeam    = [];%c(5);
    clear c
    
    %% Lecture de SeabedImagery
    
    disp('Loading SeabedImage datagrams')
    [b, Carto, subl_Image, subl_Depth, memeReponses] = view_SidescanImageIFR(s7k, 'Carto', Carto, 'ListeLayers', 1:3);%#ok
    if isempty(b)
        return
    end
    
    Reflectivity_PingSamples = b(1);
    RxBeamIndex_PingSamples  = b(2);
    %     TxAngle_PingSamples      = b(3);
    clear b
    
    
    %% Masquage de la r�flectivit�
    
    if ~isempty(CLimReflec)
        Mask = ROI_auto(Reflectivity_PingSamples, 1, 'MaskReflec', Reflectivity_PingSamples, [], CLimReflec, 1);
        Reflectivity_PingSamples = masquage(Reflectivity_PingSamples, 'LayerMask', Mask, 'valMask', 1);
    end
    
    %% Recherche de la zone commune
    
    % Debut Modif JMA le 24/05/2007 donn�es Koen
    DepthPingCounter = get(Range_PingBeam, 'PingCounter');
    ImagePingCounter = get(Reflectivity_PingSamples, 'PingCounter');
    [pppp, subl_Depth, subl_Image] = intersect(DepthPingCounter, ImagePingCounter);%#ok
    % Fin Modif JMA le 24/05/2007 donn�es Koen
    
    %% Recherche des pings dans la zone d'�tude
    
    if isempty(XLim) || isempty(YLim)
        % Modif JMA le 24/05/2007 donn�es Koen
        %         suby = 1:Range_PingBeam.nbRows; % Avant Modif
        suby = sort(subl_Image); % Apres modif
    else
        % ATTENTION : Ici on prend la nav dans Range alors qu'elle a
        % peut-�tre �t� corrig�e sur Depth : ceci d�montre l'absolue
        % n�cessit� de g�rer les signaux en commun entre tous les layers
        SonarFishLatitude  = get(Range_PingBeam, 'SonarFishLatitude');
        SonarFishLongitude = get(Range_PingBeam, 'SonarFishLongitude');
        SonarFishLatitude  = SonarFishLatitude(subl_Depth);
        SonarFishLongitude = SonarFishLongitude(subl_Depth);
        suby = find((SonarFishLongitude >= XLim(1)) & ...
            (SonarFishLongitude <= XLim(2)) & ...
            (SonarFishLatitude >= YLim(1)) & ...
            (SonarFishLatitude <= YLim(2)));
        clear SonarFishLatitude SonarFishLongitude
    end
    
    
    %% Compensation de l'image si la courbe a �t� calcul�e dans la g�om�trie
    % PingSamples
    
    if ~isempty(CorFile) && strcmp(bilan{1}(1).GeometryType, 'PingSamples')
        for iDTC=1:length(DataTypeConditions)
            switch DataTypeConditions{iDTC}
                case 'RxBeamIndex'
                    c(iDTC) = RxBeamIndex_PingSamples;
                case 'TxAngle'
                    [flag, a] = sonar_bathyFais2SonarD(RxBeamIndex_PingSamples, ...
                        Range_PingBeam, TxAngle_PingBeam, 'suby', suby);
                    if ~flag
                        return
                    end
                    c(iDTC) = a;
                    clear a
                case 'TxBeamIndex'
                    [flag, a] = sonar_bathyFais2SonarD(RxBeamIndex_PingSamples, ...
                        Range_PingBeam, TxBeamIndex_PingBeam, 'suby', suby);
                    if ~flag
                        return
                    end
                    c(iDTC) = a;
                    clear a
                otherwise
                    if ~isdeployed
                        str = sprintf('Condition "%s" pas encore rencontr�e. V�rifier si c''est bien pris en compte.', DataTypeConditions{iDTC});
                        my_warndlg(str, 1);
                    end
            end
        end
        %         c(5) =  sonar_secteur_emission(A, []);
        %         indConditionnel = [5];
        Reflectivity_PingSamples = compensationCourbesStats(Reflectivity_PingSamples, c, bilan, 'suby', suby);
        clear c
    end
    
    %% Changement de coordonn�es : PingSamples to PingAcrossDist de l'image de
    % r�flectivit�, du num�ro de faisceau et du nombre de points moyenn�s
    
    [flag, a] = sonar_MBES_projectionX(Reflectivity_PingSamples, ...
        RxBeamIndex_PingSamples, ...
        AcrossDistance_PingBeam, ...
        Range_PingBeam, ...
        [], ...
        'resolutionX', resol, ...
        'createNbp', 1);
    if ~flag
        return
    end
    Reflectivity_PingAcrossDist = a(1);
    RxBeamIndex_PingAcrossDist  = a(2);
    
    if ~isempty(RepSave.AveragePtsNb_PingAcrossDist)
        AveragePtsNb_PingAcrossDist = a(3);
        
        if ~isempty(AveragePtsNb_PingAcrossDist)
            saveInErmapperIfRequired(AveragePtsNb_PingAcrossDist, RepSave.AveragePtsNb_PingAcrossDist)
        end
        clear AveragePtsNb_PingAcrossDist
    end
    clear a
    
    %% Passage en PingAcrossDist de Range si on fait une calibration
    
    if (TypeCalibration == 3)
        [flag, a] = sonar_MBES_projectionX(Range_PingBeam, ...
            TxBeamIndex_PingBeam, ...
            AcrossDistance_PingBeam, ...
            Range_PingBeam, ...
            TxBeamIndex_PingBeam, ...
            'resolutionX', resol, 'createNbp', 0);
        if ~flag
            return
        end
        Range_PingAcrossDist = a(1);
        clear a
    end
    
    if (TypeCalibration == 1) || (TypeCalibration == 2)
        [flag, a] = sonar_MBES_projectionX(TxAngle_PingBeam, ...
            TxBeamIndex_PingBeam, ...
            AcrossDistance_PingBeam, ...
            Range_PingBeam, ...
            TxBeamIndex_PingBeam, ...
            'resolutionX', resol, 'createNbp', 0);
        if ~flag
            return
        end
        TxAngle_PingAcrossDist = a(1);
        TxBeamIndex_PingAcrossDist = a(2);
        clear a
    end
    
    clear RxBeamIndex_PingSamples
    
    %% R�cup�ration de la navigation
    
    Heading = get(AcrossDistance_PingBeam, 'Heading');
    if isempty(Heading)
        my_warndlg('Heading non renseigne dans cette image', 1);
        flag = 0;
        return
    end
    FishLatitude = get(AcrossDistance_PingBeam, 'FishLatitude');
    if isempty(FishLatitude)
        my_warndlg('SonarFishLatitude non renseigne dans cette image', 1);
        flag = 0;
        return
    end
    FishLongitude = get(AcrossDistance_PingBeam, 'FishLongitude');
    if isempty(FishLongitude)
        my_warndlg('SonarFishLongitude non renseigne dans cette image', 1);
        flag = 0;
        return
    end
    
    %% Calcul des coordonn�es g�ographiques dans la g�om�trie PingBeam
    
    [flag, LatLon_PingBeam] = sonar_calcul_coordGeo_BathyFais(AcrossDistance_PingBeam, ...
        AcrossDistance_PingBeam, ...
        AlongDistance_PingBeam, ...
        Heading, FishLatitude, FishLongitude, 'suby', suby);
    if ~flag
        return
    end
    clear AlongDistance_PingBeam
    
    %% Changement de coordonn�es : PingBeam to PingAcrossDist des coordonn�es g�ographiques
    
    isLayerConditionnel = 0;
    if ~isempty(CorFile) && strcmp(bilan{1}(1).GeometryType, 'PingAcrossDist')
        for iDTC=1:length(DataTypeConditions)
            switch DataTypeConditions{iDTC}
                case 'TxBeamIndex'
                    isLayerConditionnel = 1;
            end
        end
    end
    
    if TypeCalibration == 3 % 'TVG only'
        Reflectivity_PingAcrossDist = set(Reflectivity_PingAcrossDist, 'SonarTVG_IfremerAlpha', alpha, 'SonarTVG_origine', 2, ...
            'Range', Range_PingAcrossDist);
    end
    
    if (TypeCalibration == 1) || (TypeCalibration == 2) % 'Directivity compensated'
        Reflectivity_PingAcrossDist = set(Reflectivity_PingAcrossDist, 'SonarTVG_IfremerAlpha', alpha, 'SonarTVG_origine', 2);
        
        SonarDescription = get(Reflectivity_PingAcrossDist, 'SonarDescription');
        strMode_1 = get(SonarDescription, 'Sonar.strMode_1');
        
        for iMode=1:length(strMode_1)
            if (iMode <= length(ModelTxDiag)) && ~isempty(ModelTxDiag{iMode})
                %             EmissionModelsCalibration = get(SonarDescription, 'EmissionModelsCalibration');
                %             plot(EmissionModelsCalibration)
                %             plot(ModelTxDiag)
                SonarDescription = set(SonarDescription, 'Sonar.Mode_1', iMode);
                %                 SonarDescription = set(SonarDescription, 'EmissionModelsCalibration', ModelTxDiag{iMode});
                SonarDescription = set(SonarDescription, 'EmissionModelsCalibration', ModelTxDiag);
            end
        end
        Reflectivity_PingAcrossDist = set(Reflectivity_PingAcrossDist, 'SonarDescription', SonarDescription);
    end
    
    if TypeCalibration == 1 % BS Image
        AllImages = [Reflectivity_PingAcrossDist TxAngle_PingAcrossDist TxBeamIndex_PingAcrossDist];
        % TODO : attention, traitement TVG comment� dans SonarBestBSIfremer
        [flag, Reflectivity_PingAcrossDist] = SonarBestBSIfremer(AllImages, 1);
        if ~flag
            return
        end
    end
    
    if TypeCalibration == 2 % BS compensated Image
        Reflectivity_PingAcrossDist.CourbesStatistiques = CourbeBS;
        AllImages = [Reflectivity_PingAcrossDist TxAngle_PingAcrossDist TxBeamIndex_PingAcrossDist];
        [flag, Reflectivity_PingAcrossDist] = SonarFullCompensation(AllImages, 1);
        if ~flag
            return
        end
    end
    
    if ~isLayerConditionnel
        TxBeamIndex_PingBeam = []; % Si pas demand� comme layer conditionnel
    end
    [flag, b] = sonar_bathyFais2SonarX([LatLon_PingBeam TxAngle_PingBeam TxBeamIndex_PingBeam], AcrossDistance_PingBeam, resol);
    if ~flag
        return
    end
    Lat_PingAcrossDist = b(1);
    Lon_PingAcrossDist = b(2);
    TxAngle_PingAcrossDist = b(3);
    if isLayerConditionnel
        TxBeamIndex_PingAcrossDist = b(4);
    end
    clear LatLon_PingBeam
    clear AcrossDistance_PingBeam
    clear Range_PingBeam
    
    
    %% Compensation de l'image si la courbe a �t� calcul�e dans la g�om�trie
    % PingAcrossDist
    
    if ~isempty(CorFile) && strcmp(bilan{1}(1).GeometryType, 'PingAcrossDist')
        for iDTC=1:length(DataTypeConditions)
            switch DataTypeConditions{iDTC}
                case 'RxBeamIndex'
                    c(iDTC) = RxBeamIndex_PingAcrossDist;
                case 'TxAngle'
                    c(iDTC) = TxAngle_PingAcrossDist;
                case 'TxBeamIndex'
                    c(iDTC) = TxBeamIndex_PingAcrossDist;
                otherwise
                    str = sprintf('Condition "%s" pas encore rencontr�e', DataTypeConditions{iDTC});
                    my_warndlg(str, 1);
            end
        end
        %         c(5) =  sonar_secteur_emission(A, []);
        %         indConditionnel = [5];
        Reflectivity_PingAcrossDist = compensationCourbesStats(Reflectivity_PingAcrossDist, c, bilan);
        clear c
    end
    
    if ~isempty(RxBeamIndex_PingAcrossDist)
        saveInErmapperIfRequired(RxBeamIndex_PingAcrossDist, RepSave.RxBeamIndex_PingAcrossDist)
    end
    clear RxBeamIndex_PingAcrossDist
    
    if ~isempty(TxBeamIndex_PingAcrossDist)
        saveInErmapperIfRequired(TxBeamIndex_PingAcrossDist, RepSave.TxBeamIndex_PingAcrossDist)
    end
    clear TxBeamIndex_PingAcrossDist
    
    %% Changement de coordonn�es : PingAcrossDist to LatLong de la r�flectivit�
    % (montage en mosaique de l'image)
    
    [flag, Z_Mosa, A_Mosa] = sonar_mosaique2(Reflectivity_PingAcrossDist, TxAngle_PingAcrossDist, Lat_PingAcrossDist, Lon_PingAcrossDist, resol, ...
        'AcrossInterpolation', AcrossInterpolation, 'AlongInterpolation',  1, ...
        'PremiereAlerte', 0);
    
    if ~isempty(Reflectivity_PingAcrossDist)
        saveInErmapperIfRequired(Reflectivity_PingAcrossDist, RepSave.Reflectivity_PingAcrossDist)
    end
    clear Reflectivity_PingAcrossDist
    
    if ~isempty(TxAngle_PingAcrossDist)
        saveInErmapperIfRequired(TxAngle_PingAcrossDist,      RepSave.TxAngle_PingAcrossDist)
    end
    clear TxAngle_PingAcrossDist
    if ~flag
        %         return
        continue
    end
    
    if ~isempty(Lat_PingAcrossDist)
        saveInErmapperIfRequired(Lat_PingAcrossDist, RepSave.Lat_PingAcrossDist)
    end
    clear Lat_PingAcrossDist
    
    if ~isempty(Lon_PingAcrossDist)
        saveInErmapperIfRequired(Lon_PingAcrossDist, RepSave.Lon_PingAcrossDist)
    end
    clear Lon_PingAcrossDist
    
    %% Interpolation
    
    if ~isequal(window, [1 1])
        Z_Mosa = WinFillNaN(Z_Mosa, 'window', window, 'NoStats');
        A_Mosa = WinFillNaN(A_Mosa, 'window', window, 'NoStats');
    end
    
    %% Assemblage de la mosaique de l'image avec la mosaique g�n�rale
    
%     Previous_Z_Mosa_All = Z_Mosa_All;
%     Previous_A_Mosa_All = A_Mosa_All;
    
    [Z_Mosa_All, A_Mosa_All] = mosaique([Z_Mosa_All Z_Mosa], ...
        'Angle', [A_Mosa_All A_Mosa], 'FistImageIsCollector', 1);
    Z_Mosa_All.Name = TitreDepth;
    A_Mosa_All.Name = TitreAngle;
    
    clear Mosa
    Mosa(1) = Z_Mosa_All;
    Mosa(2) = A_Mosa_All;
    
    clear Previous_Z_Mosa_All
    clear Previous_A_Mosa_All
    
    %% Sauvegarde des mosaiques dans des fichiers ErMapper
    
    if (mod(k,Backup) == 0) || (k == nbProfils)
        Z_Mosa_All = majCoordonnees(Z_Mosa_All);
        A_Mosa_All = majCoordonnees(A_Mosa_All);
        export_ermapper(Z_Mosa_All, nomFicErMapperLayer);
        export_ermapper(A_Mosa_All, nomFicErMapperAngle);
    end
    
    %% Suppression des fichiers memmapfile
    
    deleteFileOnListe(cl_memmapfile.empty, gcbf)
end
flag = 1;
% toc
% profile report

if nargout == 0
    SonarScope(Mosa)
end




function [flag, RepSave] = get_infoSauvegardeImages(nomDir)

liste = {'Reflectivity_PingAcrossDist'
    'TxBeamIndex_PingAcrossDist'
    'TxAngle_PingAcrossDist'
    'RxBeamIndex_PingAcrossDist'
    'Lat_PingAcrossDist'
    'Lon_PingAcrossDist'
    'AveragePtsNb_PingAcrossDist'};
for k=1:length(liste)
    RepSave.(liste{k}) = [];
end

if get_LevelQuestion >= 3
    [rep, flag] = my_questdlg(Lang('Est-ce que vous voulez sauver les images "PingAcrossDist" interm�diaires ? (pas recommand� si traitement de toute une mosaique).', ...
        'Save temporary PingAcrossDist images ? (not recommanded if processing a global Mosaic).'), ...
        'Init', 2);
    if ~flag
        return
    end
    if rep == 2
        return
    end
else
    flag = 1;
    return
end

[rep, flag] = my_listdlg(Lang('Choisissez l''image que vous voulez conserver', 'Select the images you want to keep'), ...
    liste, 'InitialValue', [1 2 3]);
if ~flag
    return
end

if isempty(rep)
    return
end

[flag, nomDir] = my_uigetdir(nomDir, 'Directory to put PingAcrossDist images');
if ~flag
    return
end

for k=1:length(liste)
    if any(rep == k)
        RepSave.(liste{k}) = nomDir;
    end
end
