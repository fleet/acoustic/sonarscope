function [flag, varVal, Unit] = SSc_getSignalUnit(nomFicAll, grpName, varName)

flag = existCacheNetcdf(nomFicAll);
if flag
    [flag, varVal, Unit] = SScCacheNetcdfUtils.getSignalUnit(nomFicAll, grpName, varName);
else
    [flag, varVal, Unit] = SScCacheXMLBinUtils.getSignalUnit(nomFicAll, grpName, varName);
end
