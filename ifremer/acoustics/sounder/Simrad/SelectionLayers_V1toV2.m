function ListeLayers = SelectionLayers_V1toV2(ListeLayersV1)

ListeLayers = Simrad_getDefaultLayersV2;

for k=1:numel(ListeLayers.SelectionLayers)
    ListeLayers.SelectionLayers{k} = zeros(1,numel(ListeLayers.nomLayers{k}));
end
        
for k=1:length(ListeLayersV1)
    identLayerV1 = ListeLayersV1(k);
    switch identLayerV1
        case 1 % 'Depth'
            ListeLayers.SelectionLayers{1,1}(2) = 1;
            ListeLayers.Order{1,1}(2) = k;
        case 2 % 'AcrossDist'
            ListeLayers.SelectionLayers{1,1}(3) = 1;
            ListeLayers.Order{1,1}(3) = k;
        case 3 % 'AlongDist'
            ListeLayers.SelectionLayers{1,1}(4) = 1;
            ListeLayers.Order{1,1}(4) = k;
        case 4 % 'BeamDepressionAngle'
            ListeLayers.SelectionLayers{2,1}(1) = 1;
            ListeLayers.Order{2,1}(1) = k;
        case 5 % 'BeamAzimuthAngle'
            ListeLayers.SelectionLayers{1,1}(TODO) = 1;
            ListeLayers.Order{1,1}(TODO) = k;
        case 6 % 'Range'
%             ListeLayers.SelectionLayers{2,2}(11) = 1;
%             ListeLayers.Order{2,2}(11) = k;
            ListeLayers.SelectionLayers{2,1}(3) = 1;
            ListeLayers.Order{2,1}(3) = k;

        case 7 % 'QualityFactor'
            ListeLayers.SelectionLayers{1,1}(6) = 1;
            ListeLayers.Order{1,1}(6) = k;
        case 8 % 'LengthDetection'
            ListeLayers.SelectionLayers{1,1}(5) = 1;
            ListeLayers.Order{1,1}(5) = k;
        case 9 % 'Reflectivity'
%             ListeLayers.SelectionLayers{1,1}(9) = 1;
%             ListeLayers.Order{1,1}(9) = k;
            ListeLayers.SelectionLayers{1,1}(10) = 1;
            ListeLayers.Order{1,1}(10) = k;
        case 10 % 'Detection'
            ListeLayers.SelectionLayers{1,2}(2) = 1;
            ListeLayers.Order{1,2}(2) = k;
        case 11 % 'DetecAmplitudeNbSamples'
            ListeLayers.SelectionLayers{1,1}(TODO) = 1;
            ListeLayers.Order{1,1}(TODO) = k;
        case 12 % 'DetecPhaseOrdrePoly'
            ListeLayers.SelectionLayers{1,1}(TODO) = 1;
            ListeLayers.Order{1,1}(TODO) = k;
        case 13 % 'DetecPhaseVariance'
            ListeLayers.SelectionLayers{1,1}(TODO) = 1;
            ListeLayers.Order{1,1}(TODO) = k;
            
            
        case 14 % 'Raw-BeamPointingAngle'
            ListeLayers.SelectionLayers{2,1}(1) = 1;
            ListeLayers.Order{2,1}(1) = k;

        case 15 % 'Raw-TransmitTiltAngle'
            ListeLayers.SelectionLayers{2,1}(TODO) = 1;
            ListeLayers.Order{2,1}(TODO) = k;
        case 16 % 'Raw-Range'
            ListeLayers.SelectionLayers{2,1}(TODO) = 1;
            ListeLayers.Order{2,1}(TODO) = k;
        case 17 % 'Raw-Reflectivity'
            ListeLayers.SelectionLayers{2,1}(TODO) = 1;
            ListeLayers.Order{2,1}(TODO) = k;
            
        case 18 % 'LongueurTrajet'
            ListeLayers.SelectionLayers{2,2}(9) = 1;
            ListeLayers.Order{2,2}(9) = k;
        case 19 % 'Temps universel'
            ListeLayers.SelectionLayers{2,2}(10) = 1;
            ListeLayers.Order{2,2}(10) = k;
        case 20 % 'Roll'
            ListeLayers.SelectionLayers{2,2}(11) = 1;
            ListeLayers.Order{2,2}(11) = k;
        case 21 % 'Pitch'
            ListeLayers.SelectionLayers{2,2}(12) = 1;
            ListeLayers.Order{2,2}(12) = k;
        case 22 % 'Heave'
            ListeLayers.SelectionLayers{2,2}(13) = 1;
            ListeLayers.Order{2,2}(13) = k;
        case 23 % 'Heading'
            ListeLayers.SelectionLayers{2,2}(14) = 1; % Certainement faux
            ListeLayers.Order{2,2}(14) = k;
        case 24 % 'Depth+Draught-Heave'
            ListeLayers.SelectionLayers{1,2}(1) = 1;
            ListeLayers.Order{1,2}(1) = k;
        case 25 % 'TwoWayTravelTimeInSeconds'
            ListeLayers.SelectionLayers{2,1}(3) = 1;
            ListeLayers.Order{2,1}(3) = k;
        case 26 % 'EmissionBeam'
            ListeLayers.SelectionLayers{2,1}(2) = 1;
            ListeLayers.Order{2,1}(2) = k;
        case 27 % 'ReflectivityFromSnippets'
            ListeLayers.SelectionLayers{1,1}(11) = 1;
            ListeLayers.Order{1,1}(11) = k;
        case 28 % 'TxBeamIndexSwath'
            ListeLayers.SelectionLayers{2,2}(15) = 1;
            ListeLayers.Order{2,2}(15) = k;
        case 34 % 'Speed'
            ListeLayers.SelectionLayers{2,2}(16) = 1;
            ListeLayers.Order{2,2}(16) = k;
        case 35 % 'IfremerQF'
            ListeLayers.SelectionLayers{3,1}(1) = 1;
            ListeLayers.Order{3,1}(1) = k;
        case 36 % ''BeamAngles/Earth''
            ListeLayers.SelectionLayers{2,2}(17) = 1;
            ListeLayers.Order{2,2}(17) = k;
        case 38 % 'Mode' %GLT
            ListeLayers.SelectionLayers{2,2}(18) = 1;            
            ListeLayers.Order{2,2}(18) = k;
        case 40 % 'RT_PingBeam_AbsorptionCoeff'
            ListeLayers.SelectionLayers{1,2}(5) = 1;            
            ListeLayers.Order{1,2}(5) = k;
        case 41 % 'SSc_PingBeam_AbsorptionCoeff'
            ListeLayers.SelectionLayers{1,2}(6) = 1;            
            ListeLayers.Order{1,2}(6) = k;
        case 42 % 'User_PingBeam_AbsorptionCoeff'
            ListeLayers.SelectionLayers{1,2}(7) = 1;            
            ListeLayers.Order{1,2}(7) = k;
        case 43 % 'IncidenceAngle'
            ListeLayers.SelectionLayers{1,2}(8) = 1;            
            ListeLayers.Order{1,2}(8) = k;
        case 44 % 'SlopeAcross'
            ListeLayers.SelectionLayers{1,2}(9) = 1;            
            ListeLayers.Order{1,2}(9) = k;
        case 45 % 'SlopeAlong'
            ListeLayers.SelectionLayers{1,2}(10) = 1;            
            ListeLayers.Order{1,2}(10) = k;
        case 46 % 'BathymetryFromDTM'
            ListeLayers.SelectionLayers{1,2}(11) = 1;            
            ListeLayers.Order{1,2}(11) = k;
        case 47 % 'InsonifiedAreaSSc'
            ListeLayers.SelectionLayers{1,2}(12) = 1;            
            ListeLayers.Order{1,2}(12) = k;
        case 48 % 'TxAngleKjell'
            ListeLayers.SelectionLayers{2,2}(19) = 1;            
            ListeLayers.Order{2,2}(19) = k;
        case 49 % 'PortStarboard'
            ListeLayers.SelectionLayers{1,2}(13) = 1;            
            ListeLayers.Order{1,2}(13) = k;
        case 50 % 'ReflectivityFromSnippetsWithoutSpecularRestablishment'
            ListeLayers.SelectionLayers{1,1}(13) = 1;            
            ListeLayers.Order{1,1}(13) = k;
        case 51 % 'ReflectivityBestBS'
            ListeLayers.SelectionLayers{1,1}(13) = 1;            
            ListeLayers.Order{1,1}(13) = k;
        case 52 % 'WCSignalWidth'
            ListeLayers.SelectionLayers{1,1}(14) = 1;            
            ListeLayers.Order{1,1}(14) = k;
        case 53 % 'SonarAlongBeamAngle'
            ListeLayers.SelectionLayers{2,2}(20) = 1;            
            ListeLayers.Order{2,2}(20) = k;
            
        case 29 
            % C'est un masque sans doute calcul� interactivement
 
        otherwise
            str1 = sprintf('Le layer de num�ro "%d" n''est pas branch� dans la fonction "SelectionLayers_V1toV2".\nEnvoyez une copie de ce message � sonarscope@ifremer.fr SVP.', identLayerV1);
            str2 = sprintf('Layer numbered as "%d" is not pluged in function "SelectionLayers_V1toV2".\nPlease report to sonarscope@ifremer.fr SVP.', identLayerV1);
            my_warndlg(Lang(str1,str2), 1);
    end
end
