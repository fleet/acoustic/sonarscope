function [flag, Datagrams] = Signals2VariablesAndTables(Datagrams)
% Eclatement de la structure de signaux en variables (1 dimension = NbDatagrammes)
% et tables (plusieurs dimensions)
t       = 0;
v       = 0;

for i=1:numel(Datagrams.Signals)
    % Si le signal ne comporte pas d'index, il ne dispose qu'une seule
    % dimension = NbDatagrammes.
    if isfield(Datagrams.Signals(i), 'Index')
        if isempty(Datagrams.Signals(i).Index)
            v=v+1;
            Datagrams.Variables(v)  = Datagrams.Signals(i);
            % Immediatement apr�s PingCounter, on ajoute le Serial Number
            % comme variables pour s�quencer automatiquement l'�criture de
            % l'ent�te.
            if strcmp(Datagrams.Signals(i).Name, 'PingCounter') && ~strcmp(Datagrams.Signals(i+1).Name, 'SystemSerialNumber')
                v=v+1;
                Datagrams.Variables(v).Name           = 'SystemSerialNumber';
                Datagrams.Variables(v).Dimensions     = 'NbSamples, 1';
                Datagrams.Variables(v).Storage        = 'uint16';
                Datagrams.Variables(v).Unit           = '';
                Datagrams.Variables(v).FileName       = '';
                Datagrams.Variables(v).Tag            = 'SystemSerialNumber';
                Datagrams.Variables(v).OrigStorage    = 'uint16';
            end
        else
            t=t+1;
            Datagrams.Tables(t)     = Datagrams.Signals(i);
        end
    else
        v=v+1;
        Datagrams.Variables(v) = Datagrams.Signals(i);
        % Immediatement apr�s PingCounter ou SurveyLineNumber, on ajoute
        % le(s) Serial Number comme variables pour s�quencer
        % automatiquement l'�criture de l'ent�te.
        if strcmp(Datagrams.Signals(i).Name, 'PingCounter') && ~strcmp(Datagrams.Signals(i+1).Name, 'SystemSerialNumber')
            v=v+1;
            Datagrams.Variables(v).Name           = 'SystemSerialNumber';
            Datagrams.Variables(v).Dimensions     = 'NbSamples, 1';
            Datagrams.Variables(v).Storage        = 'uint16';
            Datagrams.Variables(v).Unit           = '';
            Datagrams.Variables(v).FileName       = '';
            Datagrams.Variables(v).Tag            = 'SystemSerialNumber';
            Datagrams.Variables(v).OrigStorage    = 'uint16';
        elseif strcmp(Datagrams.Signals(i).Name, 'SurveyLineNumber')
            % Cas du InstallationParameters
            v=v+1;
            Datagrams.Variables(v).Name           = 'SystemSerialNumber';
            Datagrams.Variables(v).Dimensions     = 'NbSamples, 1';
            Datagrams.Variables(v).Storage        = 'uint16';
            Datagrams.Variables(v).Unit           = '';
            Datagrams.Variables(v).FileName       = '';
            Datagrams.Variables(v).Tag            = 'SystemSerialNumber';
            Datagrams.Variables(v).OrigStorage    = 'uint16';
            v=v+1;
            Datagrams.Variables(v).Name           = 'SystemSerialNumberSecondHead';
            Datagrams.Variables(v).Dimensions     = 'NbSamples, 1';
            Datagrams.Variables(v).Storage        = 'uint16';
            Datagrams.Variables(v).Unit           = '';
            Datagrams.Variables(v).FileName       = '';
            Datagrams.Variables(v).Tag            = 'SystemSerialNumberSecondHead';
            Datagrams.Variables(v).OrigStorage    = 'uint16';
        end
    end
end

% Suppression du champ Signals devenu inutile.
Datagrams = rmfield(Datagrams, 'Signals');
if isfield(Datagrams, 'Images')
    for i=1:numel(Datagrams.Images)
        % Si le signal ne comporte pas d'index, il ne dispose qu'une seule
        % dimension = NbDatagrammes.
        if isfield(Datagrams.Images(i), 'Index')
            if isempty(Datagrams.Images(i).Index)
                v=v+1;
                Datagrams.Variables(v)  = Datagrams.Images(i);
            else
                t=t+1;
                Datagrams.Tables(t)     = Datagrams.Images(i);
            end
        else
            v=v+1;
            Datagrams.Variables(v) = Datagrams.Images(i);
            
        end
    end
    % Suppression du champ Images devenu inutile.
    Datagrams = rmfield(Datagrams, 'Images');
end
flag =   0;