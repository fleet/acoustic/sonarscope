% TODO : cette fonction n'a pas lieu d'�tre dans le menu Simrad, elle est
% g�n�rique : .ers ? 
% TODO : faire le pendant en images dans SonarScope 
% [flag, ListeFicErs, lastDir] = uiSelectFiles('ExtensionFiles', '.ers', 'RepDefaut', 'G:\MARMESONET\Leg2', ...
%     'InitGeometryType', cl_image.indGeometryType('LatLong'), 'InitDataType', ...
%     cl_image.indDataType('Reflectivity'))
% Paseta = 5;
% Teta = -55:Paseta:54.999;
% Teta = -15:Paseta:54.999;
% AngleLim = [Teta' , Teta'+Paseta];
% SimradAllAssemblageParAngle(ListeFicErs, AngleLim)
% NomGenerique = 'Assemblage';

function [tabM, tabA] = SimradAllAssemblageParAngle(repExport, ListeFicErs, NomGenerique, AngleLim, varargin)

[varargin, Mute] = getPropertyValue(varargin, 'Mute', 0); %#ok<ASGLU>

tabM = cl_image.empty;
tabA = cl_image.empty;

%% Lecture des images

nbMos = length(ListeFicErs);
str1 = 'Lecture des mosa�ques individuelles.';
str2 = 'Reading individual mosaics.';
hw = create_waitbar(Lang(str1,str2), 'N', nbMos);
for k=1:nbMos
    my_waitbar(k, nbMos, hw)
        
    [flag, Reflectivity_LatLong(k)] = cl_image.import_ermapper(ListeFicErs{k}); %#ok<AGROW>
    if ~flag
        return
    end
    
    %% Recherche si il y a une image d'angle d'incidence
    
    nomFicErs = strrep(ListeFicErs{k}, '_Reflectivity_LatLong', '_IncidenceAngle_LatLong');
    if exist(nomFicErs, 'file')
        [flag, X] = cl_image.import_ermapper(nomFicErs);
        if flag
            Angle_LatLong(k) = X; %#ok<AGROW>
        else
            
            %% Sinon, recherche si il existe une image de "RxBeamAngle_LatLong"
            
            nomFicErs = strrep(ListeFicErs{k}, '_Reflectivity_LatLong', '_RxBeamAngle_LatLong');
            [flag, X] = cl_image.import_ermapper(nomFicErs);
            if ~flag
                return
            end
            Angle_LatLong(k) = X; %#ok<AGROW>
        end
    else
        nomFicErs = strrep(ListeFicErs{k}, '_Reflectivity_LatLong', '_RxBeamAngle_LatLong');
        if exist(nomFicErs, 'file')
            [flag, X] = cl_image.import_ermapper(nomFicErs);
            if ~flag
                return
            end
            Angle_LatLong(k) = X; %#ok<AGROW>
        else
            return
        end
    end
end
my_close(hw)

%% Si combinaison b�bord et tribord

flagAbsAngle = 1;
if flagAbsAngle
    for k=1:nbMos
        Angle_LatLong(k) = abs(Angle_LatLong(k));
    end
end

%% Cr�ation des mosa�ques

% nomDir = fileparts(nomFicErs);
nbAngles = size(AngleLim,1);
str1 = 'G�n�ration des mosa�ques par angles';
str2 = 'Processing angular mosaics.';
hw = create_waitbar(Lang(str1,str2), 'N', nbAngles);
for k=1:nbAngles
    my_waitbar(k, nbAngles, hw)
    
    [flag, M, A] = mergeMosaics(Reflectivity_LatLong, 'Angle', Angle_LatLong, 'AngleLim', AngleLim(k,:), 'NoWaitbar', 1);
    if ~flag
        continue
    end
%     SonarScope([M A])
    
    nomFic = sprintf('%s_%02d_%d_%d_Reflectivity_LatLong.ers', NomGenerique, k, AngleLim(k,1), AngleLim(k,2));
    nomFicErs = fullfile(repExport, nomFic);
    flag = export_ermapper(M, nomFicErs);
    if ~flag
        return
    end

    A.ColormapIndex = 3;
    nomFic = sprintf('%s_%02d_%d_%d_RxBeamAngle_LatLong.ers', NomGenerique, k, AngleLim(k,1), AngleLim(k,2));
    nomFicErs = fullfile(repExport, nomFic);
    flag = export_ermapper(A, nomFicErs);
    if ~flag
        return
    end
    
    switch nargout
        case 0
        case 1
            M = optimiseMemory(M, 'SizeMax', 0);
            tabM(end+1) = M; %#ok<AGROW>
        case 2
            M = optimiseMemory(M, 'SizeMax', 0);
            A = optimiseMemory(A, 'SizeMax', 0);
            tabM(end+1) = M; %#ok<AGROW>
            tabA(end+1) = A; %#ok<AGROW>
    end
end
my_close(hw, 'MsgEnd')

if ~Mute
    str1 = 'Attention, le cadre g�ographique de l''image assembl�e reste identique � celui de l''image de d�part, utilisez le bouton "Vue globale (q)" pour voir l''ensemble de l''image.';
    str2 = 'Warning : the frame of the assembled image remains the same as the former one. Use the "Full extent (q)" button to see the entire image.';
    my_warndlg(Lang(str1,str2), 0, 'Tag', 'AssemblageCadre', 'TimeDelay', 60);
end
