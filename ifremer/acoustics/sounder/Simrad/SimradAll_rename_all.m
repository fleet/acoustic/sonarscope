function SimradAll_rename_all(nomFicAll, NbDatagrams)

[nomDir, nomFic] = fileparts(nomFicAll);
nomFicAllRenamed = fullfile(nomDir, [nomFic '_all.bad']);

str1 = sprintf('Le fichier "%s"\ncontient trop peu de pings (%d pings), je le renomme en\n"%s"\nafin qu''il ne soit plus trait�.', ...
    nomFicAll, NbDatagrams, nomFicAllRenamed);
str2 = sprintf('File "%s"\nis too small (%d pings), I rename it in\n"%s"\nin order it cannot be processed again.', ...
    nomFicAll, NbDatagrams, nomFicAllRenamed);
my_warndlg(Lang(str1,str2), 0);

my_rename(nomFicAll, nomFicAllRenamed)

SScCacheXMLBinUtils.deleteCachDirectory(nomFicAll);

