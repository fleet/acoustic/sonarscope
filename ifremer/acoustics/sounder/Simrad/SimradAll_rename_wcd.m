function SimradAll_rename_wcd(nomFicWcd, NbDatagrams)

[nomDir, nomFic] = fileparts(nomFicWcd);
nomFicWcdRenamed = fullfile(nomDir, [nomFic '_wcd.bad']);

str1 = sprintf('Le fichier "%s"\ncontient trop peu de pings (%d pings), je le renomme en\n"%s"\nafin qu''il ne soit plus trait�.', ...
    nomFicWcd, NbDatagrams, nomFicWcdRenamed);
str2 = sprintf('File "%s"\nis too small (%d pings), I rename it in\n"%s"\nin order it cannot be processed again.', ...
    nomFicWcd, NbDatagrams, nomFicWcdRenamed);
my_warndlg(Lang(str1,str2), 0);

my_rename(nomFicWcd, nomFicWcdRenamed)
