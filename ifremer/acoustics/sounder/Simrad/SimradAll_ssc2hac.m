% Cr�ation d'un fichier HAC � partir d'une arborescence SSC_ en prvoenance
% d'un fichier ALL.
%
% Syntax
%   SimradAll_ssc2hac(...)
%
% Name-Value Pair Arguments
%   nomFic      : Nom d'un fichier ou liste de fichiers .all.
%   nomDir      : Nom du r�pertoire
%   SeuilSample : seuil de compression (-64 dB par d�faut)
%
% Examples
%   nomFic{1} = 'F:\Data_SSC-3DViewer\FromLaurentBerger\Data\0274_20091113_000951_Lesuroit.all';
%   nomFic{2} = 'F:\SonarScopeData\From Carla\question 2\0293_20110129_015608.all';
%   nomFic{3} = 'F:\SonarScopeData\From Carla\question 2\0314_20110129_213139.all';
%   nomFic{4} = 'F:\SonarScopeData\From AndreOgor\0812_20091213_205617_Lesuroit.all';
%   nomFic{5} = 'C:\Temp\Marmesonet\ProfilAvecWC\0306_20091113_160950_Lesuroit.all';
%   nomFic{6} = 'C:\Temp\Marmesonet\ProfilAvecWC\0307_20091113_163950_Lesuroit.all';
%
%   flag = SimradAll_ssc2hac('NomFicAll', nomFic{5});
%   flag = SimradAll_ssc2hac('NomFicAll', nomFic, 'RepDefaut', 'C:\Temp\Test_HAC_Creation');
%
%   nomDir  = 'F:\SonarScopeData\From AndreOgor';
%   flag    = SimradAll_ssc2hac('NomDir', nomDir, 'RepDefaut', 'C:\Temp\Test_HAC_Creation');
%
% See also Authors
% Authors : GLU
%-----------------------------------------------------------------------

function flag = SimradAll_ssc2hac(varargin)

[varargin, nomDir] = getPropertyValue(varargin, 'NomDir',    []);
[varargin, nomFic] = getPropertyValue(varargin, 'NomFicAll', []);
% Seuil = -64 dB pour les EM, passage � 100 dB pour les autres
[varargin, SeuilSample] = getPropertyValue(varargin, 'SeuilSample', -100);
[varargin, RepDefaut]   = getPropertyValue(varargin, 'RepDefaut',   my_tempdir); %#ok<ASGLU>

if isempty(nomDir)
    if isempty(nomFic)
        [flag, nomFic] = uiSelectFiles('ExtensionFiles', '.all', 'RepDefaut', RepDefaut);
    else
        if ischar(nomFic)
            nomFic = {nomFic};
        end
    end
else
    nomFic = listeFicOnDir('C:\IfremerToolboxDataPublic\Level1', '.all');
end

[path, ~, ~] = fileparts(nomFic{1});

if isempty(RepDefaut)
    [flag, pathOut] = my_uigetdir(path);
    if ~flag
        return
    end
else
    pathOut = RepDefaut;
end

N = length(nomFic);
str1 = 'Conversion des fichiers .all en .hac';
str2 = 'Converting .all files into .hac';
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    
    flag = SimradAll_ssc2hac_unitaire(nomFic{k}, SeuilSample, pathOut);
    if ~flag
        % Que fait-on ?
    end
end
my_close(hw, 'MsgEnd')


function flag = SimradAll_ssc2hac_unitaire(nomFic, SeuilSample, DirOut)

global DEBUG %#ok<GVMIS> 

aKM = cl_simrad_all('nomFic', nomFic);

%%
% Lecture des descriptions de datagrammes pour indexation de ceux-ci.
[pathstr, name] = fileparts(nomFic);

nomDir = fullfile(pathstr, 'SonarScope', name);
if ~exist(nomDir, 'dir')
    str1 = 'Le r�pertoire de donn�e au format SonarScope n''existe pas. Il n''est pas possible d''exporter au format Simrad.';
    str2 = 'The data directory with SonarScope format does not exist. It is not possible to export in Hac Format.';
    my_warndlg(Lang(str1,str2), 1);
end

nomFicHac = fullfile(DirOut, [name '_all2hac.hac']);
if exist(nomFicHac, 'file')
    flag = 1;
    return
end

nameDatagramsSSC = {'Position', 'WaterColumn', 'RunTime', 'Attitude', 'InstallationParameters'};


InstallationParameters  = [];
minLp                   = 1;
maxLp                   = numel(nameDatagramsSSC);
Head1                   = [];
flagProcessTime         = 0; % On conserve le temps sous forme de double.


for lp=minLp:maxLp
    
    % Chargement des datagrammes identifi�s.
    [flag, tmpDatagrams] = loadDatagrams(nomDir, nameDatagramsSSC{lp});
    Datagrams.(nameDatagramsSSC{lp}) = tmpDatagrams;
    if flag ~= 0
        strFr = sprintf('Datagramme %s absent pour ce fichier : %s', nameDatagramsSSC{lp}, nomFic);
        strUs = sprintf('Datagramme %s absent pour this file : %s',  nameDatagramsSSC{lp}, nomFic);
        my_warndlg(Lang(strFr,strUs), 0, 'Tag', 'NoWCInThisFile');
        return
    end
    % Conservation du paquet InstallationParameters
    if strcmp(nameDatagramsSSC(lp), 'InstallationParameters')
        Head1.SystemSerialNumber = Datagrams.InstallationParameters.SystemSerialNumber;
        try
            Head1.S1R = -Datagrams.InstallationParameters.Line.S1R; % Attention : signe moins car convention invers�e
        catch %#ok<CTCH>
            Head1.S1R = -Datagrams.InstallationParameters.S1R; % Attention : signe moins car convention invers�e
        end
        Head1.SystemSerialNumberSecondHead  = Datagrams.InstallationParameters.SystemSerialNumberSecondHead;
        % Translation des champs de Line dans la structure InstallationParameters.
        try
            names = fieldnames(Datagrams.InstallationParameters.Line);
        catch %#ok<CTCH>
            names = fieldnames(Datagrams.InstallationParameters);
        end
        for i=1:numel(names)
            try
                InstallationParameters.(names{i}) = Datagrams.InstallationParameters.Line.(names{i});
            catch %#ok<CTCH>
                InstallationParameters.(names{i}) = Datagrams.InstallationParameters.(names{i});
            end
        end
        DataSSC.(nameDatagramsSSC{lp}) = InstallationParameters;
    else
        % Chargement des donn�es pour les datagrammes identifi�s.
        [~, tmpData, NbDatagrams] = loadDataFromBinFile(nomDir, tmpDatagrams, flagProcessTime); %#ok<ASGLU>
        DataSSC.(nameDatagramsSSC{lp}) = tmpData;
    end
    
    
    clear tmpData tmpDatagrams;
end

%% test si il y a bien du WC dans ce fichier

if ~isfield(DataSSC, 'WaterColumn')
    str1 = sprintf('D�sol�, le fichier "%s" ne contient pas de "Water Column".', nomFic);
    str2 = sprintf('Sorry, "%s" do not contain any Water Column data.', nomFic);
    my_warndlg(Lang(str1,str2), 0, 'Tag', 'NoWCInThisFile');
    flag = 1;
    return
end


%% Test si c'est le bon format

if ~isfield(DataSSC.WaterColumn, 'FormatVersion') || (DataSSC.WaterColumn.FormatVersion < 20101118)
    str1 = sprintf('D�sol�, le r�pertoire SonarScope associ� au fichier "%s" est trop ancien, il vous faut le reconstruire pour pouvoir convertir les donn�es de "Water Column" en HAC.', nomFic);
    str2 = sprintf('Sorry, the SonarScope folder corresponding to "%s" is too old, you have to redo it in order to convert "Water Column" data into HAC.', nomFic);
    my_warndlg(Lang(str1,str2), 1);
    flag = 0;
    return
end
DataSSC.WaterColumn.FormatVersion

%% Indexation des paquets
% Tri des paquets par Date.
WCTime              = DataSSC.WaterColumn.Time.timeMat;
WCPingCounter       = DataSSC.WaterColumn.PingCounter;

PositionTime        = DataSSC.Position.Time.timeMat;
PositionPingCounter = DataSSC.Position.PingCounter;

WCTime              = num2cell(WCTime);
WCPingCounter       = num2cell(WCPingCounter);
PositionTime        = num2cell(PositionTime);
PositionPingCounter = num2cell(PositionPingCounter);
IndexPosTuple       = num2cell((1:size(PositionTime,1))');  % Index des ping
IndexWCTuple        = num2cell((1:size(WCTime,1))');        % Index des ping trait�s.


%% Construction de la structure de Pings en agg�gant les Position et les Samples
Data1 = struct( 'Date', PositionTime, 'PingCounter', PositionPingCounter, ...
    'Type', 20, 'Index', IndexPosTuple);
Data2   = struct( 'Date', WCTime, 'PingCounter', WCPingCounter, ...
    'Type',10000, 'Index', IndexWCTuple);

Ping    = struct( 'Date', {Data1.Date Data2.Date}, ...
    'PingCounter',  {Data1.PingCounter Data2.PingCounter}, ...
    'Type', {Data1.Type Data2.Type}, ...
    'Index', {Data1.Index Data2.Index});

[~, order]  = sort([Ping.Date]);
Ping        = Ping(order);

% -----------------------------------------------
% Calcul de l'histogramme des types de datagramme
% Historisation des types de Tuples �crits (cf. le s�quencement de processTuples)
tmpType = [ Ping.Type ones(1,size(PositionTime,1))*30 ones(1,size(WCTime,1))*220 ...
    ones(1,size(WCTime,1))*2200 ...
    ones(1,size(WCTime,1))*41];
bins                = min(tmpType):max(tmpType);
histoTypeDatagram   = my_hist(tmpType,bins);
list                = unique(tmpType);
histoTypeDatagram   = histoTypeDatagram(histoTypeDatagram~=0);

sub = find(histoTypeDatagram ~= 0);

texteTypeDatagram   = codesTuplesHac(list, histoTypeDatagram);

if DEBUG
    hdlFig = figure; %#ok<NASGU>
    hBar    = bar(list(sub), histoTypeDatagram(sub)); %#ok<NASGU>
    set(gca, 'Position', [0.08  0.11 0.50 0.815]);
    hText   = text(13000,1700,max(histoTypeDatagram(sub)), texteTypeDatagram, ...
        'FontSize', 8, 'VerticalAlignment', 'top'); %#ok<NASGU>
end
NMax = numel(Ping);
nbTuples = histoTypeDatagram(list == Ping(1).Type);
fprintf('Packet 1 - %s  %s\n', Ping(1).Date(1), codesTuplesHac(Ping(1).Type, nbTuples));
nbTuples = histoTypeDatagram(list == Ping(end).Type);
fprintf('Packet %d - %s  %s\n', NMax, Ping(end).Date(1), codesTuplesHac(Ping(end).Type, nbTuples));



%% Traitement des paquets.

% Chargement des donn�es utiles au HAC depuis les caches de SonarScope.
DataTuple.Position             = loadPosTupleDataFromBinFile(DataSSC.Position);
[flag, DataTuple.DataSample]   = loadWCTupleDataFromBinFile(Datagrams.WaterColumn, DataSSC.WaterColumn);
if flag ~= 0
    str1 = 'Pb � cause du filtrage des Nan - Read_SSCImage - des donn�es si tous les Pings ne sont pas valables !';
    str2 = 'TODO';
    my_warndlg(Lang(str1,str2), 1);
    return
end
DataTuple.RunTime       = loadRunTimeTupleDataFromBinFile(DataSSC.RunTime);
DataTuple.Attitude      = loadAttTupleDataFromBinFile(Datagrams.Attitude, DataSSC.Attitude);
DataTuple.InstallParams = loadIPTupleDataFromBinFile(DataSSC.InstallationParameters);

nomFicOut = fullfile(pathstr, [name '_all2hac.hac_tmp']);
% Boucle sur l'ensemble des paquets recens�s dans les formats Kongsberg.
if exist(nomFicOut, 'file')
    delete(nomFicOut);
end
% Ouverture du fichier pour enrichissement � chaque paquet.
fid = fopen(nomFicOut, 'w+');

% On se contente de la mise en forme induite par le chargement pr�c�dent.
clear DataSSC;


% Chargement des donn�es pour les datagrammes identifi�s.
NbFaisceaux = Datagrams.WaterColumn.Dimensions.nbRx;
SounderName = ['Em' num2str(Datagrams.Position.EmModel)];

% Traitement des tuples selon le type de fichier en jeu.
file.Class  = class(aKM);
file.fid    = fid;
flag        = processTuples(file, nomDir, Ping, NbFaisceaux, SounderName, DataTuple, SeuilSample);
if ~flag
    return
end

fclose(fid);

try
    movefile(nomFicOut, nomFicHac, 'f')
catch %#ok<CTCH>
    str1 = sprintf('Le fichier "%s" n''a pas pu �tre renomm� en "%s", tentez de le faire manuellement apr�s �tre sorti de SonarScope.', nomFicOut, nomFicHac);
    str2 = sprintf('"%s" could not be moved in "%s", try to do it by hand after having closed SonarScope.', nomFicOut, nomFicHac);
    my_warndlg(Lang(str1,str2), 1);
end


function [flag, Data, NbDatagrams] = loadDataFromBinFile(nomDirSignal, Datagrams, flagProcessTime)

% Recopie des champs de Header dans la structure
Data = [];
f = fieldnames(Datagrams);
for iLoop=1:numel(f)-2
    Data.(f{iLoop}) = Datagrams.(f{iLoop});
end

% A d�faut, le nb de datagrammes = NbSamples.
if isfield(Datagrams.Dimensions, 'NbDatagrams')
    NbDatagrams = Datagrams.Dimensions.NbDatagrams;
elseif isfield(Datagrams.Dimensions, 'nbProfiles') % Cas du SoundSpeedProfile
    NbDatagrams = Datagrams.Dimensions.nbProfiles;
elseif isfield(Datagrams.Dimensions, 'nbPings')
    NbDatagrams = Datagrams.Dimensions.nbPings;
else
    NbDatagrams = Datagrams.Dimensions.NbSamples;
end

nbSignaux   = length(Datagrams.Signals);
if isfield(Datagrams, 'Images')
    nbImages = length(Datagrams.Images);
else
    nbImages = 0;
end

for iLoop=1:nbSignaux
    [flag, Signal] = Read_SSCBinSignalAll('Datagrams', Datagrams, ...,
            'SignalNomDir', nomDirSignal, ...
            'SignalName', Datagrams.Signals(iLoop).Name, ...
            'SignalType', Datagrams.Signals(iLoop).Storage, ...
            'FlagProcessTime', flagProcessTime);
    
    Data.(Datagrams.Signals(iLoop).Name) = Signal;
    
    % Ajout des champs ScaleFactor et Add_Offset pour la redistribution
    % en Variables et Tables ult�rieures.
    if ~isfield(Datagrams.Signals(iLoop).Name, 'ScaleFactor')
        Datagrams.Signals(iLoop).ScaleFactor = [];
    end
    if ~isfield(Datagrams.Signals(iLoop).Name, 'AddOffset')
        Datagrams.Signals(iLoop).AddOffset = [];
    end
    % Immediatement apr�s PingCounter, on ajoute le Serial Number
    % comme variables pour s�quencer automatiquement l'�criture de
    % l'ent�te.
    if strcmp(Datagrams.Signals(iLoop).Name, 'PingCounter')
        if isfield(Data, 'SystemSerialNumber')
            SSN  = uint16(Data.SystemSerialNumber);
            Data = rmfield(Data, 'SystemSerialNumber');
        elseif isfield(Data, 'ListeSystemSerialNumber')
            % Cas des paquets avec sondeurs Dual (paquets :  RawRangeBeamAngle).
            SSN  = uint16(Data.ListeSystemSerialNumber);
            Data = rmfield(Data, 'ListeSystemSerialNumber');
        end
        % Cr�ation d'un vecteur de type Signal
        Data.SystemSerialNumber = repmat(SSN, NbDatagrams, 1);
    elseif strcmp(Datagrams.Signals(iLoop).Name, 'SurveyLineNumber')
        % Pour le datagramme Installations Parameters
        if isfield(Data, 'SystemSerialNumber')
            SSN  = uint16(Data.SystemSerialNumber);
            Data = rmfield(Data, 'SystemSerialNumber');
        end
        if isfield(Data, 'SystemSerialNumberSecondHead')
            SHSN  = uint16(Data.SystemSerialNumberSecondHead);
            Data = rmfield(Data, 'SystemSerialNumberSecondHead');
        end
        Data.SystemSerialNumber             = repmat(SSN, NbDatagrams, 1);
        Data.SystemSerialNumberSecondHead   = repmat(SHSN, NbDatagrams, 1);
    end
    if ~flag
        return
    end
end
% Lecture du paquet d'images (matrices comme dans SoundSpeedProfile,
% Attitude).
for iLoop=1:nbImages
    [flag, Image] = Read_SSCBinImage(Datagrams, nomDirSignal, Datagrams.Images(iLoop).Name, Datagrams.Images(iLoop).Storage);
    Data.(Datagrams.Images(iLoop).Name) = Image;
    % Ajout des champs ScaleFactor et Add_Offset pour la redistribution
    % en Variables et Tables ult�rieures.
    if ~isfield(Datagrams.Images(iLoop).Name, 'ScaleFactor')
        Datagrams.Images(iLoop).ScaleFactor = [];
    end
    if ~isfield(Datagrams.Images(iLoop).Name, 'AddOffset')
        Datagrams.Images(iLoop).AddOffset = [];
    end
    if ~flag
        return
    end
end

% Lecture de fichiers binaires � partir d'une arborescence SSC_.
%
% Syntax
%        [flag, Signal] = Read_SSCBinSignal(...);
%
% Input Arguments
%     Datagrams       : structure des datagrammes d�j� lus.
%     SignalName      : nom du signal dans la structure.
%     SignalType      : type du stockage du signal.
%     FlagProcessTime : flag de traitement ou non des signaux de Time (traitement en cl_time).
%
% Output Arguments
%   flag : statut r�sultat de la proc�dure,
%   Signal : variable de signal lue.
%
% Examples
%     nomDir = 'F:\SonarScopeData\From AndreOgor\SonarScope\0812_20091213_205617_Lesuroit'
%     [flag, tmpDatagrams] = loadDatagrams(nomDir, 'Position');
%     [f, S] = Read_SSCBinSignal('Datagrams', tmpDatagrams, ...,
%                                             'SignalNomDir', nomDir, ...
%                                             'SignalName', 'Time', ...
%                                             'SignalType', 'double', ...
%                                             'FlagProcessTime', 0);
%
% See also ssc2hac.all Authors
% Authors : GLU
%-----------------------------------------------------------------------

function [flag, X] = Read_SSCBinSignalAll(varargin)

[varargin, Datagrams]       = getPropertyValue(varargin, 'Datagrams',       []);
[varargin, signalNomDir]    = getPropertyValue(varargin, 'SignalNomDir',    my_tempdir);
[varargin, signalName]      = getPropertyValue(varargin, 'SignalName',      '');
[varargin, signalType]      = getPropertyValue(varargin, 'SignalType',      'uint8');
[varargin, flagProcessTime] = getPropertyValue(varargin, 'FlagProcessTime', 0); %#ok<ASGLU>

identSignal = findIndVariable(Datagrams.Signals, signalName);
if isempty(identSignal)
    X    = [];
    flag = 0;
    return
end

% Calcul du nombre d'�chantillons du signal pour la lecture dans le
% fichier.
remain      = Datagrams.Signals(identSignal).Dimensions;
dim         = textscan(remain, '%s', 'delimiter', ',');
dimName = char(dim{1}(1));
if Datagrams.Dimensions.(dimName) >= 1
    NbSamples = Datagrams.Dimensions.(dimName);
end
if numel(dim{:}) > 1
    if strcmp(char(dim{1}(2)), '1')
        nbColumns = 1;
    else
        dimName = char(dim{1}(2));
        nbColumns = Datagrams.Dimensions.(dimName);
    end
end

% Lecture pr�alable des valeurs.
if isVariableConstante(Datagrams.Signals(identSignal))
    X = repmat(Datagrams.Signals(identSignal).Constant, NbSamples, 1);
else
    [flag, X] = readSignalAll(signalNomDir, Datagrams.Signals(identSignal),NbSamples, 'nbColumns', nbColumns);
    if ~flag
        return
    end
end

% Inhibition du facteur d'�chelle et de l'offset de valeur.
if ~(isfield(Datagrams.Signals(identSignal), 'Unit') && strcmpi(Datagrams.Signals(identSignal).Unit, 'days since JC'))
    X = val2code(X, Datagrams.Signals(identSignal), signalType);
else
    % D�composition des variables de type cl_time en Date/Millisecs sinon
    % on laisse la variable en timeMat.
    if flagProcessTime
        % Lecture et traitement (peut �tre assez long) d'une donn�e de type Time.
        if size(X.timeMat, 1) > 10000
            flagWaitbar = 1;
        else
            flagWaitbar = 0;
        end
        A = X.timeMat;
        B = dayIfr2str(dayMat2Ifr(A));
        C = char(B);
        
        yyyymmjj    = str2double(str2cell(C(:,end-3:end)))*10000 + str2double(str2cell(C(:,4:5)))*100 + str2double(str2cell(C(:,1:2)));
        heure       = heure2str(X);
        heure0      = repmat(hourStr2Ifr( '00:00:00' ), size(heure, 1),1);
        C           = my_dayStr2Ifr(C, flagWaitbar);
        NbMilliSecs = timeIfr2Duration([ C, my_hourStr2Ifr( heure, flagWaitbar )], [C, heure0]);
        
        clear X;
        X.Date        = yyyymmjj;
        X.NbMilliSecs = NbMilliSecs;
    end
end

flag    = 1;
