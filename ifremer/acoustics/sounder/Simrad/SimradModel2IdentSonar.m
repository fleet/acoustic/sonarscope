function [flag, IdentSonar] = SimradModel2IdentSonar(Model, varargin)

[varargin, isDual] = getPropertyValue(varargin, 'isDual', 0);

switch Model
    case 120 % EM300
        IdentSonar = 11;
    case 300 % EM300
        IdentSonar = 3;
    case 302 % EM302
        IdentSonar = 22;
    case 304 % EM302
        IdentSonar = 32;
    case 1002
        IdentSonar = 10;
    case 3000
        
        %         if size(DataDepth.Depth, 2) == 126
        %             IdentSonar = 7; % EM3000S
        %         else
        %             IdentSonar = 6; % EM3000D
        %         end
        if isDual
            IdentSonar = 6; % EM3000D
        else
            IdentSonar = 7; % EM3000S
        end
    case 3002   % Obtenu sur fichiers EM3000 du NIWA datagrams d'imagerie
        IdentSonar = 6; % EM3000D' | '7=EM3000S'
    case 3008   % Obtenu sur fichiers EM3000 du NIWA datagrams de bathymetrie
        IdentSonar = 6;
    case 3020
        if isDual
            IdentSonar = 19;
        else
            IdentSonar = 20;
        end
    case 2000 % Sondeur AUV de Geosciences-Azur
        IdentSonar = 21;
    case 850
        IdentSonar = 17;
    case 710
        IdentSonar = 18;
    case 712
        IdentSonar = 33;
    case 122
        IdentSonar = 23;
    case 2040
        if isDual
            IdentSonar = 31;
        else
            IdentSonar = 24;
        end
    case 2045
        IdentSonar = 25;
    otherwise
        if nargin == 1
            EmModel = Model;
        else
            EmModel = varargin{1};
        end
        str1 = sprintf('Le sondeur "%d" n''est pas identifiable, transmettez ce message � JMA s''il vous plait.', EmModel);
        str2 = sprintf('Sounder "%d" not set for the moment in SonarScope, please send this message to sonarscope@ifremer.fr.', EmModel);
        my_warndlg(Lang(str1,str2), 1);
        IdentSonar = [];
        flag = 0;
        return
end
flag = 1;
