% Génération automatique des noms de fichiers SonarScope de l'imagerie Simrad
%
% Syntax
%   nomFic = SimradSeabedNames_PingSamples(nomFicAll)
%
% Input Arguments
%   nomFicAll : Nom du fichier .all
%
% Output Arguments
%   nomFicSeabed   : Structure contenant les noms des différents fichiers SonarScope.
%   flagFilesExist : 1=Files exist, 0=files no not exist
%
% Examples
%   nomFicAll = getNomFicDatabase('EM1002_BELGICA_005053_raw.all');
%   [nomFicSeabed, flagFilesExist] = SimradSeabedNames(nomFicAll)
%
% See also Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function [nomFicSeabed, flag] = SimradSeabedNames_PingSamples(nomFicAll, varargin)

[varargin, CreateDirectories] = getPropertyValue(varargin, 'CreateDirectories', 0); %#ok<ASGLU>

[nomDir, nomFicSeul] = fileparts(nomFicAll);
nomDir2 = fullfile(nomDir, 'SonarScope', nomFicSeul, 'PingSamples');

[nomDir, nomFicSeul] = fileparts(nomFicAll);
nomFicSeabed.Reflectivity     = fullfile(nomDir, 'SonarScope', nomFicSeul, 'PingSamples',[nomFicSeul '_R']);
nomFicSeabed.Beams            = fullfile(nomDir, 'SonarScope', nomFicSeul, 'PingSamples',[nomFicSeul '_B']);
nomFicSeabed.ReflectivityErs  = fullfile(nomDir, 'SonarScope', nomFicSeul, 'PingSamples',[nomFicSeul '_R.ers']);
nomFicSeabed.BeamsErs         = fullfile(nomDir, 'SonarScope', nomFicSeul, 'PingSamples',[nomFicSeul '_B.ers']);

flag = [];

flag(end+1) = exist(nomFicSeabed.Reflectivity, 'file');
flag(end+1) = exist(nomFicSeabed.ReflectivityErs , 'file');
flag(end+1) = exist([fullfile(nomFicSeabed.Reflectivity, nomFicSeul) '_R_ers.mat'], 'file');
flag(end+1) = exist([fullfile(nomFicSeabed.Reflectivity, nomFicSeul) '_R_sigV'], 'file');

flag(end+1) = exist(nomFicSeabed.Beams   , 'file');
flag(end+1) = exist(nomFicSeabed.BeamsErs, 'file');
flag(end+1) = exist([fullfile(nomFicSeabed.Beams, nomFicSeul) '_B_ers.mat'], 'file');
flag(end+1) = exist([fullfile(nomFicSeabed.Beams, nomFicSeul) '_B_sigV'], 'file');

flag = all(flag);

if CreateDirectories
    if exist(nomDir2, 'dir')
        rmdir(nomDir2, 's')
    end
    mkdir(nomDir2)
end
