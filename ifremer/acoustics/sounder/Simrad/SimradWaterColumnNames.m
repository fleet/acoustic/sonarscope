% Génération automatique des noms de fichiers SonarScope de l'imagerie Simrad
%
% Syntax
%   [nomFicBeamRawData, flagFilesExist] = SimradWaterColumnNames(nomFicAll)
%
% Input Arguments
%   nomFicAll : Nom du fichier .s7m
%
% Output Arguments
%   nomFicBeamRawData : Structure contenant les noms des différents fichiers SonarScope.
%   flagFilesExist : 1=Files exist, 0=files no not exist
%
% Examples
%   nomFicAll = getNomFicDatabase('xxxxxxxxxxxxxxxx.s7k');
%   nomFicBeamRawData = SimradWaterColumnNames(nomFicAll)
%
% See also read_SnippetData Authors
% Authors : JMA
%-------------------------------------------------------------------------------

 function [nomFicBeamRawData, flagFilesExist] = SimradWaterColumnNames(nomFicAll, iPing)

[nomDir, nomFicSeul] = fileparts(nomFicAll);
nomFicBeamRawData.Mat = fullfile(nomDir, 'SonarScope', ['_' nomFicSeul '_WaterColumnData.mat']);

if isempty(iPing)
    flagFilesExist = exist(nomFicBeamRawData.Mat, 'file');
    return
end    
    
[nomDir, nomFicSeul] = fileparts(nomFicAll);
nomFicBeamRawData.Mat = fullfile(nomDir, 'SonarScope', ['_' nomFicSeul '_WaterColumnData.mat']);

if isempty(iPing)
    flagFilesExist = exist(nomFicBeamRawData.Mat, 'file');
    return
end

nomFicBeamRawData.AP  = fullfile(nomDir, 'SonarScope', ['_' nomFicSeul], [nomFicSeul '_BeamRawDataAP_' num2str(iPing) '.mat']);
flagFilesExist = exist(nomFicBeamRawData.Mat, 'file') && ...
        exist(nomFicBeamRawData.AP, 'file');


