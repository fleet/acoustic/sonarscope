function [Angles, maskOverlap] = Simrad_AnglesAntenna2ShipFloor(Angles, DataInstallationParameters, isDual)

maskOverlap = [];

if isempty(DataInstallationParameters) || isempty(isDual)
    return
end

if isDual
    n = size(Angles,2);
    ns2 = floor(n/2);
    subBab = 1:ns2;
    subTri = (ns2+1):n;
    if isfield(DataInstallationParameters, 'S3R') % TODO : � voir avec Kongsberg comment faire �a proprement
        AngleDepointageBab = DataInstallationParameters.S2R;
        AngleDepointageTri = DataInstallationParameters.S3R;
        Angles(:,subBab) = Angles(:,subBab) - AngleDepointageBab;
        Angles(:,subTri) = Angles(:,subTri) - AngleDepointageTri;
    else
        AngleDepointageBab = DataInstallationParameters.S1R;
        AngleDepointageTri = DataInstallationParameters.S2R;
        Angles(:,subBab) = Angles(:,subBab) - AngleDepointageBab;
        Angles(:,subTri) = Angles(:,subTri) - AngleDepointageTri;
    end
else
%     if isfield(DataInstallationParameters, 'S2R') % TODO : a voir avec Kongsberg comment faire �a proprement
%         AngleDepointage = DataInstallationParameters.S2R;
%     else
        AngleDepointage = DataInstallationParameters.S1R; % Jamais test�
%     end

    if DataInstallationParameters.EmModel ~= 850 % 850 = ME70
        Angles = Angles - AngleDepointage;
    end
end

X = Angles(1,:);
X = X(~isnan(X));
if ~isempty(X) && (X(1) > X(end))
    Angles = -Angles;
end

if isDual
    maskOverlap = false(size(Angles));
    X = (Angles(:,subBab) > 0);
    maskOverlap(:,subBab) = X;
    X = (Angles(:,subTri) < 0);
    maskOverlap(:,subTri) = X;
    maskOverlap(isnan(Angles)) = 1;
end
