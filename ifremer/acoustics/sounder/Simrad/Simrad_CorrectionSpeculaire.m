function [ReflectivityByBeam] = Simrad_CorrectionSpeculaire(ReflectivityByBeam, BSN, BSO, Rn, Range, subBab, subTri, Version, SousVersion, ...
    TVGCrossOver)

deltaBS = BSN - BSO;

if strcmp(Version, 'V1')
    if strcmp(SousVersion, 'F')
        if length(deltaBS) == 1 % Cas d'un sondeur single
            % Checked
%             Correction = deltaBS * CorSpec(Range, Rn, TVGCrossOver);
%             ReflectivityByBeam = ReflectivityByBeam + Correction;
            ReflectivityByBeam = CorSpec(ReflectivityByBeam, deltaBS, Range, Rn, TVGCrossOver, false);

        else % Cas d'un sondeur dual
            % Not Checked
%             Correction = deltaBS(1) * CorSpec(Range(subBab), Rn(1), TVGCrossOver(1));
%             ReflectivityByBeam(subBab) = ReflectivityByBeam(subBab) + Correction;
            ReflectivityByBeam(subBab) = CorSpec(ReflectivityByBeam(subBab), deltaBS(1), Range(subBab), Rn(1), TVGCrossOver(1), false);
            
%             Correction = deltaBS(2) * CorSpec(Range(subTri), Rn(2), TVGCrossOver(2));
%             ReflectivityByBeam(subTri) = ReflectivityByBeam(subTri) + Correction;
            ReflectivityByBeam(subTri) = CorSpec(ReflectivityByBeam(subTri), deltaBS(2), Range(subTri), Rn(2), TVGCrossOver(2), false);
        end
    else % 'f'
        if length(deltaBS) == 1 % Cas d'un sondeur single
            % Checked
%             Correction = deltaBS * CorSpec(Range, Rn, TVGCrossOver);
%             Correction(end+1:length(ReflectivityByBeam)) = 0;
%             ReflectivityByBeam = ReflectivityByBeam + Correction;
            ReflectivityByBeam = CorSpec(ReflectivityByBeam, deltaBS, Range, Rn, TVGCrossOver, false);
            
        else % Cas d'un sondeur dual
            % Checked
            ReflectivityByBeam(subBab) = CorSpec(ReflectivityByBeam(subBab), deltaBS(1), Range(subBab), Rn(1), TVGCrossOver(1), true);
            
            %{
            FigUtils.createSScFigure;
            hc(1) = subplot(2, 1, 1); PlotUtils.createSScPlot(subBab, Correction); grid on; title(sprintf('Bab, BSN=%s  BSO=%s  BSN-BSO=%s', num2str(BSN(1)), num2str(BSO(1)), num2str(deltaBS(1))))
            hc(2) = subplot(2, 1, 2); PlotUtils.createSScPlot(subBab, Range(subBab)); grid on; title(sprintf('TGVN=%s', num2str(Rn(1))));
            hold on; plot(subBab, ones(size(subBab))*Rn(1));
            linkaxes(hc, 'x'); axis tight; drawnow;
            %}
            
            ReflectivityByBeam(subTri) = CorSpec(ReflectivityByBeam(subTri), deltaBS(2), Range(subTri), Rn(2), TVGCrossOver(2), true);
            %{
            FigUtils.createSScFigure;
            hc(1) = subplot(2, 1, 1); PlotUtils.createSScPlot(subTri, Correction); grid on; title(sprintf('Bab, BSN=%s  BSO=%s  BSN-BSO=%s', num2str(BSN(2)), num2str(BSO(2)), num2str(deltaBS(2))))
            hc(2) = subplot(2, 1, 2); PlotUtils.createSScPlot(subTri, Range(subTri)); grid on; title(sprintf('TGVN=%s', num2str(Rn(2))));
            hold on; plot(subTri, ones(size(subTri))*Rn(2));
            linkaxes(hc, 'x'); axis tight; drawnow;
            %}
        end
    end
else
    if length(deltaBS) == 1 % Cas d'un sondeur single
        % Checked
%         Correction = deltaBS * CorSpec(Range, Rn, TVGCrossOver);
%         ReflectivityByBeam = ReflectivityByBeam + Correction;
        ReflectivityByBeam = CorSpec(ReflectivityByBeam, deltaBS, Range, Rn, TVGCrossOver, false);
            
    else % Cas d'un sondeur dual (EM2040)
        % Checked
%         Correction = deltaBS(1) * CorSpec(Range(subBab), Rn(1), TVGCrossOver(1));
%         ReflectivityByBeam(subBab) = ReflectivityByBeam(subBab) + Correction;
        ReflectivityByBeam(subBab) = CorSpec(ReflectivityByBeam(subBab), deltaBS(1), Range(subBab), Rn(1), TVGCrossOver(1), false);

%         Correction = deltaBS(2) * CorSpec(Range(subTri), Rn(2), TVGCrossOver(2));
%         ReflectivityByBeam(subTri) = ReflectivityByBeam(subTri) + Correction;
        ReflectivityByBeam(subTri) = CorSpec(ReflectivityByBeam(subTri), deltaBS(2), Range(subTri), Rn(2), TVGCrossOver(2), false);
    end
end
