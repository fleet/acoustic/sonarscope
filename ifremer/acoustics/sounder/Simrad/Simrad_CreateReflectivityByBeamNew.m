function ReflectivityByBeam = Simrad_CreateReflectivityByBeamNew(ReflectivityByBeam, Entries, InfoBeamNbSamples, ...
    sublDepth, sublImage, maskEntries, IdentAlgoSnippets2OneValuePerBeam) %, Rn, Range, sublRaw)

% numAlgo = 1;

nbPings = min(size(ReflectivityByBeam, 1), size(InfoBeamNbSamples, 1));

idebEntriesPing = ones(1,nbPings); % zeros en C
for iPing=2:nbPings
    n = (InfoBeamNbSamples(iPing-1,:));
    n(isnan(n)) = 0;
    idebEntriesPing(iPing) = idebEntriesPing(iPing-1) + double(sum(n));
end

N = length(sublDepth);
str1 = 'Calcul de la r�flectivit� par faisceau � partir des snippets';
str2 = 'Computing reflectivity by beam from snippets';
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k1=1:N
    my_waitbar(k1, nbPings, hw)
    iPing = sublDepth(k1);
    if sublImage(k1) > nbPings % Test rajout� le 05/06/2013 pour fichier Charline 20040510_142810_raw.all
        continue
    end
    subEntries = idebEntriesPing(sublImage(k1))-1;
    IBNS       = InfoBeamNbSamples(sublImage(k1),:);
    sub = find((IBNS ~= 0) & ~isnan(IBNS));
    
%     iSide = 1; % TODO : 1 Port, 2 starbord if dual
%     ratio = Range(sublRaw(k1),:) / Rn(sublImage(k1),iSide);
    %         figure; plot(ratio); grid on
    
    for k2=1:length(sub)
        iBeam = sub(k2);
        n = double(IBNS(iBeam));
        subEntries = subEntries(end) + (1:n);
        
        sub2 = find(maskEntries(subEntries));
        n2 = length(sub2);
        if n2 == 0
%             'On ne devrait pas passer par l�'
            continue
        end
        
        X = Entries(subEntries(sub2));
        
        switch IdentAlgoSnippets2OneValuePerBeam
            case 1 % Moyenne en dB
                X = sum(X) / n2;
            case 2 % Moyenne en Amplitude
                X = reflec_dB2Amp(X);
                X = sum(X) / n2;
            case 3 % Moyenne en Energy
                X = reflec_dB2Enr(X);
                X = sum(X) / n2;
            case 4 % Median
                X = median(X);
        end

        ReflectivityByBeam(iPing, iBeam) = X;
    end
end
my_close(hw, 'MsgEnd')

switch IdentAlgoSnippets2OneValuePerBeam
    case 1 % Moyenne en dB
        % Nothing to do
    case 2 % Moyenne en Amplitude
        ReflectivityByBeam = reflec_Amp2dB(ReflectivityByBeam(:,:));
    case 3 % Moyenne en Energy
        ReflectivityByBeam = reflec_Enr2dB(ReflectivityByBeam(:,:));
    case 4 % Median
        % Nothing to do
end
