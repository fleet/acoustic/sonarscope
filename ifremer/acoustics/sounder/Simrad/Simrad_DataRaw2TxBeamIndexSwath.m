function I = Simrad_DataRaw2TxBeamIndexSwath(DataRaw, Mode, varargin)

[varargin, subl] = getPropertyValue(varargin, 'subl', 1:DataRaw.Dimensions.nbPings);
[varargin, subc] = getPropertyValue(varargin, 'subc', 1:DataRaw.Dimensions.nbRx); %#ok<ASGLU>

if isfield(DataRaw, 'PingSequence')
    PingSequence = DataRaw.PingSequence(subl);
else
    PingSequence = ones(length(subl),1);
end

MaxSectors = DataRaw.Dimensions.nbTx;
I = NaN(length(subl),length(subc), 'single');

if ~isfield(DataRaw, 'TransmitSectorNumberRx')
    DataRaw.TransmitSectorNumberRx = DataRaw.TransmitSectorNumber;
end

try
    DataRawTransmitSectorNumber = DataRaw.TransmitSectorNumberRx(:,:);
catch %#ok<CTCH>
    DataRawTransmitSectorNumber = DataRaw.TransmitSectorNumberRx;
end

% DataRawTransmitSectorNumber = changeTxBeamIndexForEM2040D(DataRawTransmitSectorNumber, mode2) ;

for iLig=1:length(subl)
    S = DataRawTransmitSectorNumber(subl(iLig),subc);
    sMax = max(DataRawTransmitSectorNumber(subl(iLig),subc));
    if isnan(sMax)
        sMax = 0;
    end
    
    MS = Simrad_getMaxSectors(DataRaw.EmModel, Mode(iLig), MaxSectors); % Modif JMA le 10/03/2018 pour fichier Marc Roche L4_BARTOLOMEUDIASplume07032018-2\0004_20180307_130102_SimonStevin.all
    
    for s=1:sMax
        I(iLig,S==s) = s + (PingSequence(iLig)-1) * MS;
    end
end
