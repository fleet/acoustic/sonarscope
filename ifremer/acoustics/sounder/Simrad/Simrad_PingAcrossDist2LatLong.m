% Cr�ation d'une mosaique � partir d' images PingAcrossDist
%
% Syntax
%   [flag, aOut, nomDir] = Simrad_PingAcrossDist2LatLong(...)
%
% Name-Value Pair Arguments
%   nomDir : Nom du r�pertoire des donnees
%   resol  : Pas de la grille en m�tres
%
% Output Arguments
%   []     : Auto-plot activation
%   flag   : Description du parametre (unite).
%   a      : Instances de cl_image contenant la mosaique et la mosaique angulaire
%   nomDir : Nom du r�pertoire des images cr��es
%
% Examples
%   Simrad_PingAcrossDist2LatLong
%   [flag, a] = Simrad_PingAcrossDist2LatLong('NomDir', '/home1/jackson/FEMME2005/Donnees', 'Resol', 10);
%   [flag, a] = Simrad_PingAcrossDist2LatLong('NomDir', 'D:\NIWA_DATA\TAN0105', 'Resol', 10);
%   SonarScope(a)
%
% See also Authors
% Authors : JMA
%--------------------------------------------------------------------------

function [flag, nomDir] = Simrad_PingAcrossDist2LatLong(varargin)


disp('TOUT EST A FAIRE')


disp('Supprimer l''interpolation de la mosaique de r�ception')

Mosa = [];

[varargin, resol]       = getPropertyValue(varargin, 'Resol',       []);
[varargin, Layers]      = getPropertyValue(varargin, 'Layers',      []);
[varargin, nomDir]      = getPropertyValue(varargin, 'NomDir',      []);
[varargin, MasqueActif] = getPropertyValue(varargin, 'MasqueActif', []); %#ok<ASGLU>
% [varargin, Rayon]     = getPropertyValue(varargin, 'Rayon',       []);
% [varargin, Backup]    = getPropertyValue(varargin, 'Backup',      []);

%% Question for Mask

[flag, MasqueActif] = question_UseMask(MasqueActif);
if ~flag
    return
end

%%

if isempty(nomDir)
    nomDir = pwd;
end

if isempty(Layers)
    str1 = 'Layer � mosa�quer';
    str2 = 'Layer to mosaic';
    listeLayers = ALL.getListeLayers;
    [choixLayer, flag] = my_listdlg(Lang(str1,str2), listeLayers, 'InitialValue', 1, 'SelectionMode', 'Single');
    if ~flag
        return
    end
    LayerName = listeLayers{choixLayer};
end

%% Recherche des fichiers .all

[flag, liste] = uiSelectFiles('ExtensionFiles', '.all', 'RepDefaut', nomDir);
if ~flag
    return
end
nbProfils = length(liste);

%% Pas de grille

[flag, resol] = get_gridSize('Value', -resol);
if ~flag
    return
end

%% Backup frequency

[flag, Backup] = saisie_backupFrequency(nbProfils);
if ~flag
    return
end

%% Noms des fichiers de sauvegarde des mosaiques

nomFicErMapperLayer = sprintf('Mosaic_%s_%sm.ers', LayerName, num2str(resol));
[flag, nomFicErMapperLayer] = my_uiputfile('*.ers', 'Mosaic File', [nomDir filesep nomFicErMapperLayer]);
if ~flag
    return
end
[nomDir, TitreDepth] = fileparts(nomFicErMapperLayer);

% nomFicErMapperAngle = sprintf('Mosaic_%s_%sm.ers', listeLayers{4}, num2str(resol));
% nomFicErMapperAngle = fullfile(nomDir, nomFicErMapperAngle, '_Angle.ers');
nomFicErMapperAngle = fullfile(nomDir, [TitreDepth '_Angle.ers']);
[flag, nomFicErMapperAngle] = my_uiputfile('*.ers', 'Transmission angle mosaic file', [nomDir filesep nomFicErMapperAngle]);
if ~flag
    return
end
[nomDir, TitreAngle] = fileparts(nomFicErMapperAngle);

%% Si Mosaique de Depth, on propose de faire la correction de mar�e

% if any((choixLayer == 1) | (choixLayer == 25))    % Depth ou Depth-Heave
if (choixLayer == 1) || (choixLayer == 25)    % Depth ou Depth-Heave
    [flag, flagTideCorrection] = question_TideCorrection;
    if ~flag
        return
    end
end

%% Si Mosaique de Reflectivite, on propose de faire une compensation

% TODO : remplacer ce qui suit par [flag, CorFile, ModeDependant, UseModel, PreserveMeanValueIfModel] = QuestionCompensationCurve(nomDirImport, varargin)
[rep, flag] = my_questdlg(Lang('Voulez-vous faire une compensation ?', 'Do you want to do a compensation ?'));
if ~flag
    return
end
if rep == 1
    [flag, CorFile] = my_uigetfile('*.mat', 'Give a compensation file (cancel instead)', nomDir);
    if flag
        CourbeConditionnelle = load_courbesStats(CorFile);
        bilan = CourbeConditionnelle.bilan;
        %     d = compensationCourbesStats(b, c, bilan);
    else
        CorFile = [];
    end
else
    CorFile = [];
end

%% R�cup�ration des mosaiques existantes

if exist(nomFicErMapperLayer, 'file') && exist(nomFicErMapperAngle, 'file')
    [flag, rep] = question_CompleteExistingMosaic;
    if ~flag
        return
    end
    if rep == 1
        [flag, Z_Moza_All] = cl_image.import_ermapper(nomFicErMapperLayer);
        if ~flag
            return
        end
        [flag, A_Moza_All]  = cl_image.import_ermapper(nomFicErMapperAngle);
        if ~flag
            return
        end
    else
        Z_Moza_All = [];
        A_Moza_All = [];
    end
else
    Z_Moza_All = [];
    A_Moza_All = [];
end

Carto = [];
for k=1:nbProfils
    fprintf('Processing file %d/%d : %s\n', k, nbProfils, liste{k});
    
    aKM = cl_simrad_all('nomFic', liste{k});
    
    Version = version_datagram(aKM);
    switch Version
        case 'V1'
            [c, Carto] = import_PingBeam_All_V1(aKM, 'ListeLayers', [choixLayer 2 3 4], 'Carto', Carto, ...
                'TideCorrection', flagTideCorrection, 'MasqueActif', MasqueActif);
        case 'V2'
            ListeLayers = SelectionLayers_V1toV2([choixLayer 2 3 4]);
            [c, Carto] = import_PingBeam_All_V2(aKM, 'ListeLayers', ListeLayers, 'Carto', Carto, ...
                'TideCorrection', flagTideCorrection, 'MasqueActif', MasqueActif);
    end
    
    Z = c(1);
    AcrossDistance = c(2);
    AlongDistance  = c(3);
    A              = c(4);
    Heading        = get(Z, 'Heading');
    FishLatitude   = get(Z, 'FishLatitude');
    FishLongitude  = get(Z, 'FishLongitude');
    
    if ~isempty(CorFile)
        c(5) =  sonar_secteur_emission(A, []);
        indConditionnel = 5;
        Z = compensationCourbesStats(c(1), c(indConditionnel), bilan);
    end
    
    %% Compensation de donnees si possible
    
    if choixLayer == 9
        
    end
    
    [flag, LatLon] = sonar_calcul_coordGeo_BathyFais(Z, AcrossDistance, AlongDistance, ...
        Heading, FishLatitude, FishLongitude);
    if ~flag
        return
    end
    
    [flag, Z_Mosa, A_Mosa] = sonar_mosaique2(Z, A, LatLon(1), LatLon(2), resol, 'AlongInterpolation',  1);
    if ~flag
        return
    end
    clear LatLon
    
    % -------------
    % Interpolation
    
    Z_Mosa = WinFillNaN(Z_Mosa, 'window', [5 5]);
    A_Mosa = WinFillNaN(A_Mosa, 'window', [5 5]);
    
    [Z_Moza_All, A_Moza_All] = mosaique([Z_Moza_All Z_Mosa], 'Angle', [A_Moza_All A_Mosa], 'FistImageIsCollector', 1);
    Z_Moza_All.Name = TitreDepth;
    A_Moza_All.Name = TitreAngle;
    Mosa(1) = Z_Moza_All;
    Mosa(2) = A_Moza_All;
    
    clear Z_Mosa A_Mosa c
    
    %% Sauvegarde des mosaiques dans des fichiers ErMapper
    
    if (mod(k,Backup) == 0) || (k == nbProfils)
        export_ermapper(Z_Moza_All, nomFicErMapperLayer);
        export_ermapper(A_Moza_All, nomFicErMapperAngle);
    end
end
flag = 1;
