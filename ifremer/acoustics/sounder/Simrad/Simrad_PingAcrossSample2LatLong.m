function [flag, Mosa, nomDirImport, nomDirExport] = Simrad_PingAcrossSample2LatLong(varargin)

[varargin, XLim]         = getPropertyValue(varargin, 'XLim',         []);
[varargin, YLim]         = getPropertyValue(varargin, 'YLim',         []);
[varargin, resol]        = getPropertyValue(varargin, 'Resol',        []);
% [varargin, Backup]     = getPropertyValue(varargin, 'Backup',       []);
[varargin, nomDirImport] = getPropertyValue(varargin, 'nomDirImport', pwd);
[varargin, nomDirExport] = getPropertyValue(varargin, 'nomDirExport', pwd);
[varargin, window]       = getPropertyValue(varargin, 'window',       []); %#ok<ASGLU>
% [varargin, MasqueActif] = getPropertyValue(varargin, 'MasqueActif', []);

E0 = cl_ermapper([]);

Mosa = [];

%% Recherche des fichiers .all

[flag, liste] = uiSelectFiles('ExtensionFiles', '.all', 'RepDefaut', nomDirImport);
if ~flag || isempty(liste)
    return
end
nbProfils = length(liste);
nomDirImport = fileparts(liste{1});

%% Saisie de la r�solution

[flag, resol] = get_gridSize('Value', -resol);
if ~flag
    return
end

%% Backup frequency

[flag, Backup] = saisie_backupFrequency(nbProfils);
if ~flag
    return
end

%% Across distance interpolation method

[flag, AcrossInterpolation] = question_AcrossDistanceInterpolation('QL', 3);
if ~flag
    return
end

%% Mosaic interpolation window

[flag, window] = question_MosaicInterpolationWindow(window);
if ~flag
    return
end

%% Noms du fichier de sauvegarde de la mosaique de r�flectivit�

DataType = cl_image.indDataType('Reflectivity');
[nomFicErMapperLayer, flag] = initNomFicErs(E0, 'Mosaic', cl_image.indGeometryType('LatLong'), ...
    DataType, nomDirExport, 'resol', resol);
if ~flag
    return
end
[nomDirExport, TitreDepth] = fileparts(nomFicErMapperLayer);

%% Noms du fichier de sauvegarde de la mosaique des angles d'�mission

DataType = cl_image.indDataType('TxAngle');
[nomFicErMapperAngle, flag] = initNomFicErs(E0, TitreDepth, cl_image.indGeometryType('LatLong'), ...
    DataType, nomDirExport, 'confirm', (get_LevelQuestion >= 3));
if ~flag
    return
end
[~, TitreAngle] = fileparts(nomFicErMapperAngle);

%% Recup�ration des mosaiques existantes

Carto = [];
if exist(nomFicErMapperLayer, 'file') && exist(nomFicErMapperAngle, 'file')
    [flag, rep] = question_CompleteExistingMosaic;
    if ~flag
        return
    end
    if rep == 1
        [flag, Z_Mosa_All] = cl_image.import_ermapper(nomFicErMapperLayer);
        if ~flag
            return
        end
        Carto = get(Z_Mosa_All, 'Carto');
        
        [flag, A_Mosa_All]  = cl_image.import_ermapper(nomFicErMapperAngle);
        if ~flag
            return
        end
    else
        Z_Mosa_All = [];
        A_Mosa_All = [];
    end
else
    Z_Mosa_All = [];
    A_Mosa_All = [];
end

%% Si Mosaique de Reflectivit�, on propose de faire une compensation statistique ou une calibration

[flag, ModeDependant, TypeCalibration, CorFile, alpha, ModelTxDiag, CourbeBS] = params_MBESCompensation(nomDirExport);
if ~flag
    return
end

%%

[flag, RepSave] = get_infoSauvegardeImages(nomDirExport);
if ~flag
    return
end

%% Gestion du recouvrement entre profils

[flag, CoveringPriority, TetaLimite] = question_LinesOverlapping('QL', 3);
if ~flag
    return
end

%% Traitement des fichiers

% profile on
% tic
for k=1:nbProfils
    fprintf('\n--------------------------------------------------------------------------------------\n');
    fprintf('Processing file %d/%d : %s\n', k, nbProfils, liste{k});
    
    %% Lecture des layers "Range", "AcrossDistance", "AlongDistance" et "BeamDepressionAngle"
    
    aKM = cl_simrad_all('nomFic', liste{k});
    if isempty(aKM)
        continue
    end
    Version = version_datagram(aKM);
    
    %% Lecture de SeabedImagery
    
    disp('Loading SeabedImage datagrams')
    
    switch Version
        case 'V1'
            ListeLayersImagerie = [1 3:6];
        case 'V2'
            ListeLayersImagerie = [1 3:6 8];
        otherwise
            'beurk'
    end
    b = import_SeabedImage_All(aKM, 'ListeLayers', ListeLayersImagerie, 'Carto', Carto, 'Memmapfile', 1);
    clear aKM
    if isempty(b)
        continue
    end
    
    DataType = cl_image.indDataType('Reflectivity');
    iDT = findIndLayerSonar(b, 1, 'DataType', DataType, 'WithCurrentImage', 1);
    Reflectivity_PingAcrossSample = b(iDT);
    
    DataType = cl_image.indDataType('TxAngle');
    iDT = findIndLayerSonar(b, 1, 'DataType', DataType, 'WithCurrentImage', 1);
    TxAngle_PingAcrossSample = b(iDT);
    
    DataType = cl_image.indDataType('TxBeamIndex');
    iDT = findIndLayerSonar(b, 1, 'DataType', DataType, 'WithCurrentImage', 1);
    TxBeamIndex_PingAcrossSample = b(iDT);
    
    % A v�rifier sur une image format V1
    DataType = cl_image.indDataType('TxBeamIndexSwath');
    iDT = findIndLayerSonar(b, 1, 'DataType', DataType, 'WithCurrentImage', 1);
    TxBeamIndexSwath_PingAcrossSample = b(iDT);
    
    DataType = cl_image.indDataType('RxBeamIndex');
    iDT = findIndLayerSonar(b, 1, 'DataType', DataType, 'WithCurrentImage', 1);
    RxBeamIndex_PingAcrossSample  = b(iDT);
    
    DataType = cl_image.indDataType('AlongDist');
    iDT = findIndLayerSonar(b, 1, 'DataType', DataType, 'WithCurrentImage', 1);
    AlongDist_PingAcrossSample  = b(iDT);
    clear b
    
    %     DepthPingCounter = get(Range_PingBeam, 'PingCounter');
    %     ImagePingCounter = get(Reflectivity_PingAcrossSample, 'PingCounter');
    %     [~, subl_Depth, subl_Image] = intersect(DepthPingCounter, ImagePingCounter);
    
    %% Recherche des pings dans la zone d'�tude
    
    if isempty(XLim) || isempty(YLim)
        % Modif JMA le 24/05/2007 donn�es Koen
        %         suby = 1:Range_PingBeam.nbRows; % Avant Modif
        %         suby = sort(subl_Image); % Apres modif
    else
        % ATTENTION : Ici on prend la nav dans Range alors qu'elle a
        % peut-�tre �t� corrig�e sur Depth : ceci d�montre l'absolue
        % n�cessit� de g�rer les signaux en commun entre tous les layers
        %         SonarFishLatitude  = get(Range_PingBeam, 'SonarFishLatitude');
        %         SonarFishLongitude = get(Range_PingBeam, 'SonarFishLongitude');
        %         SonarFishLatitude  = SonarFishLatitude(subl_Depth);
        %         SonarFishLongitude = SonarFishLongitude(subl_Depth);
        %         suby = find((SonarFishLongitude >= XLim(1)) & ...
        %                     (SonarFishLongitude <= XLim(2)) & ...
        %                     (SonarFishLatitude >= YLim(1)) & ...
        %                     (SonarFishLatitude <= YLim(2)));
        clear SonarFishLatitude SonarFishLongitude
    end
    
    %% Compensation de l'image si la courbe a �t� calcul�e dans la g�om�trie PingAcrossSample
    
    for k2=1:length(CorFile)
        CourbeConditionnelle = load_courbesStats(CorFile{k2});
        bilan = CourbeConditionnelle.bilan;
        %         if strcmp(bilan{1}(1).GeometryType, 'PingAcrossSample')
        %             str1 = 'Vous avez calcul� une courbe de compensation en g�om�trie PingAcrossSample. Je suis d�sol� mais cette g�om�trie ne peut �s �tre urilis�e da';
        %             str2 = 'TODO';
        %             my_warndlg(Lang(str1,str2), 1);
        %         end
        %         if strcmp(bilan{1}(1).GeometryType, 'PingAcrossDist') || strcmp(bilan{1}(1).GeometryType, 'PingAcrossSample')
        if strcmp(bilan{1}(1).GeometryType, 'PingAcrossSample')
            DataTypeConditions = bilan{1}(1).DataTypeConditions;
            
            for iDTC=1:length(DataTypeConditions)
                switch DataTypeConditions{iDTC}
                    case 'RxBeamIndex'
                        c(iDTC) = RxBeamIndex_PingAcrossSample; %#ok<AGROW> % Axe X
                    case {'TxAngle'; 'TxAngle_PingBeam'; 'RxBeamAngle'} % Rajout� le 01/04/2009
                        c(iDTC) = TxAngle_PingAcrossSample; %#ok<AGROW>
                    case 'TxBeamIndex'
                        c(iDTC) = TxBeamIndex_PingAcrossSample; %#ok<AGROW>
                    case 'TxBeamIndexSwath'
                        c(iDTC) = TxBeamIndexSwath_PingAcrossSample; %#ok<AGROW>
                    case 'BeamPointingAngle'
                        %                         c(iDTC) = BeamPointingAngle_PingAcrossSample; %#ok<AGROW>
                        c(iDTC) = TxAngle_PingAcrossSample; %#ok<AGROW> % En attendant
                    otherwise
                        if strcmp(bilan{1}(1).Support{iDTC}, 'SignalVert')
                            %                             bilan{1}(k).Support{iDTC}
                        else
                            str1 = sprintf('Condition "%s" pas encore rencontr�e, contactez sonarscope@ifremer.fr SVP', DataTypeConditions{iDTC});
                            str2 = sprintf('Condition "%s" not yet meet, please report to sonarscope@ifremer.fr', DataTypeConditions{iDTC});
                            my_warndlg(Lang(str1,str2), 1);
                        end
                end
            end
            %         c(5) =  sonar_secteur_emission(A, []);
            %         indConditionnel = [5];
            Reflectivity_PingAcrossSample = compensationCourbesStats(Reflectivity_PingAcrossSample, c, bilan, ...
                'ModeDependant', ModeDependant);
            clear c
        end
    end
    
    %% Changement de coordonn�es : PingAcrossDist to PingAcrossDist de l'image de r�flectivit�, du num�ro de faisceau et du nombre de points moyenn�s
    
    [flag, Reflectivity_PingAcrossDist] = sonar_PingAcrossSample2PingAcrossDist(Reflectivity_PingAcrossSample, resol, 'createNbp', 1);
    if ~flag
        return
    end
    RxBeamIndex_PingAcrossDist = [];
    %     [flag, RxBeamIndex_PingAcrossDist] = sonar_PingAcrossSample2PingAcrossDist(RxBeamIndex_PingAcrossSample, resol, 'createNbp', 1);
    %     if ~flag
    %         return
    %     end
    [flag, AlongDist_PingAcrossDist] = sonar_PingAcrossSample2PingAcrossDist(AlongDist_PingAcrossSample, resol, 'createNbp', 1);
    if ~flag
        return
    end
    [flag, TxAngle_PingAcrossDist] = sonar_PingAcrossSample2PingAcrossDist(TxAngle_PingAcrossSample, resol, 'createNbp', 1);
    if ~flag
        return
    end
    
    %     if ~isempty(BeamPointingAngle_PingAcrossSample)
    %         [flag, BeamPointingAngle_PingAcrossDist] = sonar_PingAcrossSample2PingAcrossDist(BeamPointingAngle_PingAcrossSample, resol, 'createNbp', 1);
    %         if ~flag
    %             return
    %         end
    %     end
    
    TxBeamIndex_PingAcrossDist = [];
    %     [flag, TxBeamIndex_PingAcrossDist] = sonar_PingAcrossSample2PingAcrossDist(TxBeamIndex_PingAcrossSample, resol, 'createNbp', 1);
    %     if ~flag
    %         return
    %     end
    
    %% Passage en PingAcrossDist de Range si on fait une calibration
    
    if (TypeCalibration == 3)
        [flag, Range_PingAcrossDist] = sonar_PingAcrossSample2PingAcrossDist(Range_PingAcrossSample, resol, 'createNbp', 1);
        if ~flag
            return
        end
    end
    
    %% R�cup�ration de la navigation
    
    Heading = get(Reflectivity_PingAcrossDist, 'Heading');
    if isempty(Heading)
        my_warndlg('Heading non renseigne dans cette image', 1);
        flag = 0;
        return
    end
    FishLatitude = get(Reflectivity_PingAcrossDist, 'FishLatitude');
    if isempty(FishLatitude)
        my_warndlg('SonarFishLatitude non renseigne dans cette image', 1);
        flag = 0;
        return
    end
    FishLongitude = get(Reflectivity_PingAcrossDist, 'FishLongitude');
    if isempty(FishLongitude)
        my_warndlg('SonarFishLongitude non renseigne dans cette image', 1);
        flag = 0;
        return
    end
    
    %% Calcul de % ceci �tait un copi�/coll� : coordonn�es g�ographiques dans la g�om�trie PingAcrossDist
    
    [flag, b] = sonarCalculCoordGeo([Reflectivity_PingAcrossDist AlongDist_PingAcrossDist], 1);
    if ~flag
        return
    end
    Lat_PingAcrossDist = b(1);
    Lon_PingAcrossDist = b(2);
    clear b
    
    %% Changement de coordonn�es : PingBeam to PingAcrossDist des coordonn�es g�ographiques
    
    if TypeCalibration == 3 % 'TVG only'
        Reflectivity_PingAcrossDist = set(Reflectivity_PingAcrossDist, 'SonarTVG_IfremerAlpha', alpha, 'SonarTVG_origine', 2, ...
            'Range', Range_PingAcrossDist);
    end
    
    if (TypeCalibration == 1) || (TypeCalibration == 2) % 'Directivity compensated'
        Reflectivity_PingAcrossDist = set(Reflectivity_PingAcrossDist, 'SonarTVG_IfremerAlpha', alpha, 'SonarTVG_origine', 2);
        
        SonarDescription = get(Reflectivity_PingAcrossDist, 'SonarDescription');
        strMode_1 = get(SonarDescription, 'Sonar.strMode_1');
        
        for iMode=1:length(strMode_1)
            if (iMode <= length(ModelTxDiag)) && ~isempty(ModelTxDiag{iMode})
                %             EmissionModelsCalibration = get(SonarDescription, 'EmissionModelsCalibration');
                %             plot(EmissionModelsCalibration)
                %             plot(ModelTxDiag)
                SonarDescription = set(SonarDescription, 'Sonar.Mode_1', iMode);
                %                 SonarDescription = set(SonarDescription, 'EmissionModelsCalibration', ModelTxDiag{iMode});
                SonarDescription = set(SonarDescription, 'EmissionModelsCalibration', ModelTxDiag);
            end
        end
        Reflectivity_PingAcrossDist = set(Reflectivity_PingAcrossDist, 'SonarDescription', SonarDescription);
    end
    
    if TypeCalibration == 1 % BS Image
        if isempty(TxBeamIndex_PingAcrossDist)
            [flag, TxBeamIndex_PingAcrossDist] = sonar_PingAcrossSample2PingAcrossDist(TxBeamIndex_PingAcrossSample, resol, 'createNbp', 1);
            if ~flag
                return
            end
        end
        AllImages = [Reflectivity_PingAcrossDist TxAngle_PingAcrossDist TxBeamIndex_PingAcrossDist];
        % TODO : attention, traitement TVG comment� dans SonarBestBSIfremer
        [flag, Reflectivity_PingAcrossDist] = SonarBestBSIfremer(AllImages, 1);
        if ~flag
            return
        end
    end
    
    if TypeCalibration == 2 % BS compensated Image
        Reflectivity_PingAcrossDist.CourbesStatistiques = CourbeBS;
        if isempty(TxBeamIndex_PingAcrossDist)
            [flag, TxBeamIndex_PingAcrossDist] = sonar_PingAcrossSample2PingAcrossDist(TxBeamIndex_PingAcrossSample, resol, 'createNbp', 1);
            if ~flag
                return
            end
        end
        AllImages = [Reflectivity_PingAcrossDist TxAngle_PingAcrossDist TxBeamIndex_PingAcrossDist];
        [flag, Reflectivity_PingAcrossDist] = SonarFullCompensation(AllImages, 1);
        if ~flag
            return
        end
    end
    
    %% Compensation de l'image si la courbe a �t� calcul�e dans la g�om�trie PingAcrossDist
    
    %     if ~isempty(CorFile) && strcmp(bilan{1}(1).GeometryType, 'PingAcrossDist')
    for k2=1:length(CorFile)
        CourbeConditionnelle = load_courbesStats(CorFile{k2});
        bilan = CourbeConditionnelle.bilan;
        if strcmp(bilan{1}(1).GeometryType, 'PingAcrossDist')
            DataTypeConditions = bilan{1}(1).DataTypeConditions;
            for iDTC=1:length(DataTypeConditions)
                switch DataTypeConditions{iDTC}
                    case 'RxBeamIndex'
                        if isempty(RxBeamIndex_PingAcrossDist)
                            [flag, RxBeamIndex_PingAcrossDist] = sonar_PingAcrossSample2PingAcrossDist(RxBeamIndex_PingAcrossSample, resol, 'createNbp', 1);
                            if ~flag
                                return
                            end
                        end
                        c(iDTC) = RxBeamIndex_PingAcrossDist;
                    case 'TxAngle'
                        c(iDTC) = TxAngle_PingAcrossDist;
                    case 'BeamPointingAngle'
                        c(iDTC) = BeamPointingAngle_PingAcrossDist;
                    case 'RxBeamAngle'
                        c(iDTC) = TxAngle_PingAcrossDist;
                    case 'TxBeamIndex'
                        if isempty(TxBeamIndex_PingAcrossDist)
                            [flag, TxBeamIndex_PingAcrossDist] = sonar_PingAcrossSample2PingAcrossDist(TxBeamIndex_PingAcrossSample, resol, 'createNbp', 1);
                            if ~flag
                                return
                            end
                        end
                        c(iDTC) = TxBeamIndex_PingAcrossDist;
                    case 'TxBeamIndexSwath'
                        if ~exist('TxBeamIndexSwath_PingAcrossDist', 'var') || isempty(TxBeamIndexSwath_PingAcrossDist)
                            [flag, TxBeamIndexSwath_PingAcrossDist] = sonar_PingAcrossSample2PingAcrossDist(TxBeamIndexSwath_PingAcrossSample, resol, 'createNbp', 1);
                            if ~flag
                                return
                            end
                        end
                        c(iDTC) = TxBeamIndex_PingAcrossDist;
                    otherwise
                        str = sprintf('Condition "%s" pas encore rencontr�e', DataTypeConditions{iDTC});
                        my_warndlg(str, 1);
                end
            end
            %         c(5) =  sonar_secteur_emission(A, []);
            %         indConditionnel = [5];
            Reflectivity_PingAcrossDist = compensationCourbesStats(Reflectivity_PingAcrossDist, c, bilan, ...
                'ModeDependant', ModeDependant);
            clear c
        end
    end
    
    if ~isempty(RxBeamIndex_PingAcrossDist)
        saveInErmapperIfRequired(RxBeamIndex_PingAcrossDist, RepSave.RxBeamIndex_PingAcrossDist)
        clear RxBeamIndex_PingAcrossDist
    end
    if ~isempty(TxBeamIndex_PingAcrossDist)
        saveInErmapperIfRequired(TxBeamIndex_PingAcrossDist, RepSave.TxBeamIndex_PingAcrossDist)
        clear TxBeamIndex_PingAcrossDist
    end
    
    %% Changement de coordonn�es : PingAcrossDist to LatLong de la r�flectivit� (montage en mosaique de l'image)
    
    [flag, Z_Mosa, A_Mosa] = sonar_mosaique2(Reflectivity_PingAcrossDist, TxAngle_PingAcrossDist, Lat_PingAcrossDist, Lon_PingAcrossDist, resol, ...
        'AcrossInterpolation', AcrossInterpolation, 'AlongInterpolation',  1, ...
        'PremiereAlerte', 0);
    
    if ~isempty(Reflectivity_PingAcrossDist)
        saveInErmapperIfRequired(Reflectivity_PingAcrossDist, RepSave.Reflectivity_PingAcrossDist)
        clear Reflectivity_PingAcrossDist
    end
    if ~isempty(TxAngle_PingAcrossDist)
        saveInErmapperIfRequired(TxAngle_PingAcrossDist, RepSave.TxAngle_PingAcrossDist)
        clear TxAngle_PingAcrossDist
    end
    if ~flag
        return
    end
    
    if ~isempty(Lat_PingAcrossDist)
        saveInErmapperIfRequired(Lat_PingAcrossDist, RepSave.Lat_PingAcrossDist)
        clear Lat_PingAcrossDist
    end
    if ~isempty(Lon_PingAcrossDist)
        saveInErmapperIfRequired(Lon_PingAcrossDist, RepSave.Lon_PingAcrossDist)
        clear Lon_PingAcrossDist
    end
    
    %% Interpolation
    
    if ~isequal(window, [1 1])
        Z_Mosa = WinFillNaN(Z_Mosa, 'window', window, 'NoStats');
        A_Mosa = WinFillNaN(A_Mosa, 'window', window, 'NoStats');
    end
    
    %% Assemblage de la mosaique de l'image avec la mosaique g�n�rale
    
    [Z_Mosa_All, A_Mosa_All] = mosaique([Z_Mosa_All Z_Mosa], ...
        'Angle', [A_Mosa_All A_Mosa], 'FistImageIsCollector', 1, ...
        'CoveringPriority', CoveringPriority, 'TetaLimite', TetaLimite);
    Z_Mosa_All.Name = TitreDepth;
    A_Mosa_All.Name = TitreAngle;
    
    clear Mosa
    Mosa(1) = Z_Mosa_All;
    Mosa(2) = A_Mosa_All;
    
    %% Sauvegarde des mosaiques dans des fichiers ErMapper
    
    if (mod(k,Backup) == 0) || (k == nbProfils)
        Z_Mosa_All = majCoordonnees(Z_Mosa_All);
        A_Mosa_All = majCoordonnees(A_Mosa_All);
        export_ermapper(Z_Mosa_All, nomFicErMapperLayer);
        export_ermapper(A_Mosa_All, nomFicErMapperAngle);
    end
    
    %% Suppression des fichier memmapfile
    
    deleteFileOnListe(cl_memmapfile.empty, gcbf)
end
flag = 1;
% toc
% profile report

if nargout == 0
    SonarScope(Mosa)
end



function [flag, RepSave] = get_infoSauvegardeImages(nomDir)

liste = {'Reflectivity_PingAcrossDist'
    'TxBeamIndex_PingAcrossDist'
    'TxAngle_PingAcrossDist'
    'RxBeamIndex_PingAcrossDist'
    'Lat_PingAcrossDist'
    'Lon_PingAcrossDist'
    'AveragePtsNb_PingAcrossDist'};
for k=1:length(liste)
    RepSave.(liste{k}) = [];
end

[rep, flag] = my_questdlg(Lang('Est-ce que vous voulez sauver les images "PingAcrossDist" interm�diaires ? (pas recommand� si traitement de toute une mosaique).', ...
    'Save temporary PingAcrossDist images ? (not recommanded if processing a global Mosaic).'), ...
    'Init', 2);
if ~flag
    return
end
if rep == 2
    return
end

[rep, flag] = my_listdlg(Lang('S�lectionnez les images que vous voulez traiter', 'Select the images you want to keep'), ...
    liste, 'InitialValue', [1 2 3]);
if ~flag
    return
end

if isempty(rep)
    return
end

[flag, nomDir] = my_uigetdir(nomDir, 'Directory to put PingAcrossDist images');
if ~flag
    return
end

for k=1:length(liste)
    if any(rep == k)
        RepSave.(liste{k}) = nomDir;
    end
end
