% Cr�ation des images PingAcrossDist � partir des images PingSamples de Simrad
%
% Syntax
%   [flag, aOut, nomDir] = Simrad_PingSamples2PingAcrossDist(...)
%
% Name-Value Pair Arguments
%   nomDir : Nom du r�pertoire des donnees
%   resol  : Pas de la grille en m�tres
%
% Output Arguments
%   []     : Auto-plot activation
%   flag   : Description du parametre (unite).
%   a      : Instances de cl_image contenant la mosaique et la mosaique angulaire
%   nomDir : Nom du r�pertoire des images cr��es
%
% Examples
%   Simrad_PingSamples2PingAcrossDist
%   [flag, a] = Simrad_PingSamples2PingAcrossDist('NomDir', '/home1/jackson/FEMME2005/Donnees', 'Resol', 2);
%   [flag, a] = Simrad_PingSamples2PingAcrossDist('NomDir', 'D:\NIWA_DATA\TAN0105', 'Resol', 2);
%   SonarScope(a)
%
% See also Authors
% Authors : JMA
%--------------------------------------------------------------------------

function [flag, aOut, nomDir] = Simrad_PingSamples2PingAcrossDist(varargin)

[varargin, MasqueActif] = getPropertyValue(varargin, 'MasqueActif', []);
[varargin, resolutionX] = getPropertyValue(varargin, 'Resol',       []);
[varargin, nomDir]      = getPropertyValue(varargin, 'NomDir',      []); %#ok<ASGLU>

aOut = [];

if isempty(nomDir)
    nomDir = pwd;
end

%% Question for Mask

[flag, MasqueActif] = question_UseMask(MasqueActif);
if ~flag
    return
end

%% Recherche des fichiers .all

[flag, liste] = uiSelectFiles('ExtensionFiles', '.all', 'RepDefaut', nomDir);
if ~flag || isempty(liste)
    return
end
nomDir = fileparts(liste{1});

if isempty(resolutionX)
    str1 = 'Cette projection n�cessite de d�finir le pas en distance lat�rale';
    str2 = 'This projection needs to define the across distance grid size';
    str3 = 'Pas en distance lat�rale';
    str4 = 'Across distance step';
    [flag, resolutionX] = inputOneParametre(Lang(str1,str2), Lang(str3,str4), ...
        'Value', 0.5, 'MinValue', 0, 'Unit', 'm');
    if ~flag
        return
    end
end

nomFicErMapper = {};
Carto = [];
for i=1:length(liste)
    [nomDir, nomFic] = fileparts(liste{i});
    aKM = cl_simrad_all('nomFic', liste{i});
    disp('---------------------------------------------------------------')
    
    %% Lecture de Depth
    
    Version = version_datagram(aKM);
    switch Version
        case 'V1'
            [c, Carto] = import_PingBeam_All_V1(aKM, 'ListeLayers', [6 2 3], 'Carto', Carto, 'MasqueActif', MasqueActif);
        case 'V2'
            ListeLayers = SelectionLayers_V1toV2([6 2 3]);
            [c, Carto] = import_PingBeam_All_V2(aKM, 'ListeLayers', ListeLayers, 'Carto', Carto, 'MasqueActif', MasqueActif);
    end
    
    Bathy_Range       = c(1);
    AcrossDistance(i) = c(2); %#ok
    AlongDistance(i)  = c(3); %#ok
    clear c
    
    %% Lecture de SeabedImagery
    
    disp('Loading SeabedImage datagrams')
    b = import_SeabedImage_All(aKM, 'ListeLayers', [1 2], 'Carto', Carto); %, 'NoSpecularCompensation');
    
    SeabedImageReflectivity = b(1);
    SeabedImageBeamNumber   = b(2);
    clear b
    
    [flag, a] = sonar_MBES_projectionX(SeabedImageReflectivity, ...
        SeabedImageBeamNumber, ...
        AcrossDistance(i), ...
        Bathy_Range, ...
        [], ...
        'resolutionX', resolutionX, ...
        'createNbp', 1);
    
    if flag
        %             SonarScope(a)
        for k=1:length(a)
            DataType = a(k).DataType;
            
            [nomFicErMapperAngle, flag] = initNomFicErs(cl_ermapper([]), nomFic, cl_image.indGeometryType('PingAcrossDist'), DataType, ...
                nomDir, 'resol', resolutionX, 'Confirm', 0);
            if ~flag
                return
            end
            nomFicErMapper{end+1} = nomFicErMapperAngle; %#ok
            export_ermapper(a(k), nomFicErMapper{end});
        end
    end
    clear a
    
    deleteFileOnListe(cl_memmapfile.empty, gcbf);
end

[flag, aOut] = cl_image.import_ermapper(nomFicErMapper);
aOut = [AcrossDistance AlongDistance aOut];

flag = 1;
if nargout == 0
    SonarScope(aOut)
end
