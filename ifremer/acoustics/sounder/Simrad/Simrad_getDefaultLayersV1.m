% ListeLayers = Simrad_getDefaultLayersV1
% ListeLayers = Simrad_getDefaultLayersV1('RxBeamAngle')

function ListeLayers = Simrad_getDefaultLayersV1(varargin)

[varargin, nomLayer] = getPropertyValue(varargin, 'nomLayer', []);

switch length(varargin)
    case 0
        ListeLayers = [2 3 4 6 10 24 26 27];
    case 1
        nomLayers = varargin{1};
        if ischar(nomLayers)
            ListeLayers = getDefaultLayersV1(nomLayers, nomLayer);
        else
            for k=1:length(nomLayers)
                ListeLayers(k) = getDefaultLayersV1(nomLayers{k}, nomLayer); %#ok<AGROW>
            end
        end
        
    otherwise
        'beurk'
end

function ListeLayers = getDefaultLayersV1(nomLayers, nomLayer)
switch nomLayers
    case 'Depth'
        ListeLayers = 1;
    case 'AcrossDist'
        ListeLayers = 2;
    case 'AlongDist'
        ListeLayers = 3;
    case {'BeamDepressionAngle'; 'TxAngle'; 'BeamPointingAngle'; 'RxBeamAngle'}
        %         ListeLayers = 4;
        % -----------------------------------------------------------------------
        ListeLayers = 4; % Modif JMA le 22/03/2017
        % -----------------------------------------------------------------------
    case 'BeamAzimuthAngle'
        ListeLayers = 5;
    case {'TwoWayTravelTimeInSamples'; 'RayPathSampleNb'}
        ListeLayers = 6;
    case {'QualityFactor'; 'KongsbergQualityFactor'}
        ListeLayers = 7;
    case {'LengthDetection'; 'KongsbergLengthDetection'}
        ListeLayers = 8;
    case 'Reflectivity'
        if contains(nomLayer, 'Snippets')
            ListeLayers = 27;
        elseif strcmp(nomLayer, 'ReflectivityBestBS')
            ListeLayers = 51;
        else
            if contains(nomLayer, 'BestBS') % Ajout JMA le 16/11/2023
                ListeLayers = 51;
            elseif contains(nomLayers, 'Snippets')
                ListeLayers = 27;
            else
                ListeLayers = 9;
            end
        end
    case {'Detection'; 'DetectionType'}
        ListeLayers = 10;
    case {'DetecAmplitudeNbSamples'; 'KongsbergAmplitudeNbSamples'}
        ListeLayers = 11;
    case {'DetecPhaseOrdrePoly'; 'KongsbergPhaseOrdrePoly'}
        ListeLayers = 12;
    case {'DetecPhaseVariance'; 'KongsbergPhaseVariance'}
        ListeLayers = 13;
        
    case 'Raw-BeamPointingAngle'
        ListeLayers = 14;
    case {'Raw-TransmitTiltAngle'; 'TxTiltAngle'}
        ListeLayers = 15;
    case 'Raw-Range'
        ListeLayers = 16;
    case 'Raw-Reflectivity'
        ListeLayers = 17;
        
    case 'RayPathLength' % One way length in meters
        ListeLayers = 18;
    case 'RxTime'
        ListeLayers = 19;
    case 'Roll'
        ListeLayers = 20;
    case 'Pitch'
        ListeLayers = 21;
    case 'Heave'
        ListeLayers = 22;
    case 'Heading'
        ListeLayers = 23;
    case {'Bathymetry'; 'Depth+Draught-Heave'}
        ListeLayers = 24;
    case 'TwoWayTravelTimeInSeconds'
        ListeLayers = 25;
    case 'TxBeamIndex'
        ListeLayers = 26;
    case 'ReflectivityFromSnippets'
        ListeLayers = 27;
    case 'TxBeamIndexSwath'
        ListeLayers = 28;
    case 'Mask'
        ListeLayers = 29;
        
    case 'IndexNumberBeam'
        ListeLayers = 30;
    case {'InfoBeamSortDirection'; 'KongsbergSnippetSortDirection'}
        ListeLayers = 31;
    case {'InfoBeamNbSamples'; 'KongsbergSnippetNbSamples'}
        ListeLayers = 32;
    case {'InfoBeamCentralSample'; 'KongsbergSnippetCentralSample'}
        ListeLayers = 33;
    case 'Speed'
        ListeLayers = 34;
    case {'IfremerQF'; 'MbesQualityFactor'}
        ListeLayers = 35;
    case {'BeamAngles/Earth'; 'RxAngleEarth'}
        ListeLayers = 36;
    case {'SoundSpeed'; 'SoundVelocity'}
        ListeLayers = 37;
    case 'Mode'
        ListeLayers = 38;
    case {'CentralFrequency'; 'Frequency'}
        ListeLayers = 39;
    case 'AbsorptionCoeff'
%         if ~contains(nomLayer, '_RT_')
        if contains(nomLayer, 'RT')
            ListeLayers = 40;
        else
%             if ~contains(nomLayer, '_SSc_')
            if contains(nomLayer, 'SSc')
                ListeLayers = 41;
            else
                if contains(nomLayer, 'User')
                    ListeLayers = 42;
                else
                    ListeLayers = [];
                end
            end
        end
    case 'IncidenceAngle'
        ListeLayers = 43;
    case 'SlopeAcross'
        ListeLayers = 44;
    case 'SlopeAlong'
        ListeLayers = 45;
    case 'BathymetryFromDTM'
        ListeLayers = 46;
        
        %     case 'InsonifiedAreaRT'
        %         ListeLayers = 47;
    case 'InsonifiedAreaSSc'
        ListeLayers = 47;
        
    case 'InsonifiedAreadB'
        ListeLayers = 47; % TODO
        
    case 'TxBeamPattern'
        ListeLayers = [];
        
    case 'Segmentation'
        ListeLayers = [];
        
    case 'PortStarboard'
        ListeLayers = 49;
        
    case 'ReflectivityFromSnippetsWithoutSpecularRestablishment'
        ListeLayers = 50;
        
    case 'ReflectivityBestBS'
        ListeLayers = 51;
        
    case 'WCSignalWidth'
        ListeLayers = 52;
        
    otherwise
        str1 = sprintf('"%s" non pr�vu dans Simrad_getDefaultLayersV1, contactez sonarscope@ifremer.fr.', nomLayers);
        str2 = sprintf('"%s" not recognized in Simrad_getDefaultLayersV1, please report to sonarscope@ifremer.fr.', nomLayers);
        my_warndlg(Lang(str1,str2), 1);
        ListeLayers = [];
end
