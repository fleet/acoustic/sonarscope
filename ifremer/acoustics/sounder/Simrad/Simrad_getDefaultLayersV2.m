% ListeLayers = Simrad_getDefaultLayersV2
% ListeLayers = Simrad_getDefaultLayersV2('RxBeamAngle')

% TODO : On ne renseigne pas l'ordre des layers tels que demand�s dans
% varargin. Il faudrait utiliser "ListeLayers.Order" et l'utiliser par la suite

function ListeLayers = Simrad_getDefaultLayersV2(varargin)

ListeLayers.SelectionLayers{1,1} = [0 0 1 1 0 0 0 0 0 0 1 0 0 0 0 0];
ListeLayers.SelectionLayers{1,2} = [1 1 0 0 0 0 0 0 0 0 0 0 0 0];
ListeLayers.SelectionLayers{2,1} = [1 1 1];
ListeLayers.SelectionLayers{2,2} = [0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 1 0 0 0];
ListeLayers.SelectionLayers{3,1} = [0 0 0];
ListeLayers.SelectionLayers{3,2} = [];%[0 0];
ListeLayers.SelectionLayers{4,1} = [0 0 0 0 0];
ListeLayers.SelectionLayers{4,2} = [];
switch nargin
    case 0
        
    case 1
        ListeLayers.SelectionLayers{1,1}(:) = 0;
        ListeLayers.SelectionLayers{1,2}(:) = 0;
        ListeLayers.SelectionLayers{2,1}(:) = 0;
        ListeLayers.SelectionLayers{2,2}(:) = 0;
        ListeLayers.SelectionLayers{3,1}(:) = 0;
        ListeLayers.SelectionLayers{3,2}(:) = 0;
        ListeLayers.SelectionLayers{4,1}(:) = 0;
        ListeLayers.SelectionLayers{4,2}(:) = 0;
        
        nomLayers = varargin{1};
        if ischar(nomLayers)
            nomLayers = {nomLayers}; % TODO : Pas tr�s heureus, il faut trouver pourquoi nomLayers est parfois 
        end
        for k=1:length(nomLayers)
            switch nomLayers{k}
                % ListeLayers.SelectionLayers{1,1}
                
                case 'Mask'
                    ListeLayers.SelectionLayers{1,1}(1) = 1;
                case 'Depth'
                    ListeLayers.SelectionLayers{1,1}(2) = 1;
                case 'AcrossDist'
                    ListeLayers.SelectionLayers{1,1}(3) = 1;
                case 'AlongDist'
                    ListeLayers.SelectionLayers{1,1}(4) = 1;
                case 'KongsbergLengthDetection'
                    ListeLayers.SelectionLayers{1,1}(5) = 1;
                case 'KongsbergQualityFactor'
                    ListeLayers.SelectionLayers{1,1}(6) = 1;
                case 'KongsbergIBA'
                    ListeLayers.SelectionLayers{1,1}(7) = 1;
                case 'KongsbergDetectionInfo'
                    ListeLayers.SelectionLayers{1,1}(8) = 1;
                case 'KongsbergRealTimeCleaningInfo'
                    ListeLayers.SelectionLayers{1,1}(9) = 1;
                case 'Reflectivity'
                    ListeLayers.SelectionLayers{1,1}(10) = 1;
                case 'ReflectivityFromSnippets'
                    ListeLayers.SelectionLayers{1,1}(11) = 1;
                case 'ReflectivityFromSnippetsWithoutSpecularRestablishment'
                    ListeLayers.SelectionLayers{1,1}(12) = 1;
                case 'ReflectivityBestBS'
                    ListeLayers.SelectionLayers{1,1}(13) = 1;
                case 'WCSigna�Width'
                    ListeLayers.SelectionLayers{1,1}(14) = 1;
                    
                    % ListeLayers.SelectionLayers{1,2}
                    
                case {'Depth+Draught-Heave'; 'Bathymetry'}
                    ListeLayers.SelectionLayers{1,2}(1) = 1;
                case  {'Detection'; 'DetectionType'}
                    ListeLayers.SelectionLayers{1,2}(2) = 1;
                case 'NaNFillMethod'
                    ListeLayers.SelectionLayers{1,2}(3) = 1;
                case 'SoundSpeed'
                    ListeLayers.SelectionLayers{1,2}(4) = 1;
                case 'AbsorptionCoefficientRT'
                    ListeLayers.SelectionLayers{1,2}(5) = 1;
                case 'AbsorptionCoefficientSSc'
                    ListeLayers.SelectionLayers{1,2}(6) = 1;
                case 'AbsorptionCoefficientUser'
                    ListeLayers.SelectionLayers{1,2}(7) = 1;
                case 'IncidenceAngle'
                    ListeLayers.SelectionLayers{1,2}(8) = 1;
                case 'SlopeAcross'
                    ListeLayers.SelectionLayers{1,2}(9) = 1;
                case 'SlopeAlong'
                    ListeLayers.SelectionLayers{1,2}(10) = 1;
                case 'BathymetryFromDTM'
                    ListeLayers.SelectionLayers{1,2}(11) = 1;
%                 case 'InsonifiedAreaRT'
%                     ListeLayers.SelectionLayers{1,2}(12) = 1;
                case 'InsonifiedAreaSSc'
                    ListeLayers.SelectionLayers{1,2}(12) = 1;
                case 'PortStarboard'
                    ListeLayers.SelectionLayers{1,2}(13) = 1;
                case 'Depth+Draught-Heave+Tide'
                    ListeLayers.SelectionLayers{1,2}(14) = 1;
                    
                    % ListeLayers.SelectionLayers{2,1}
                    
                case {'RxBeamAngle'; 'BeamPointingAngle'}
                    ListeLayers.SelectionLayers{2,1}(1) = 1;
                case 'TxBeamIndex' % 'Detection2'
                    ListeLayers.SelectionLayers{2,1}(2) = 1;
                case 'TwoWayTravelTimeInSeconds'
                    ListeLayers.SelectionLayers{2,1}(3) = 1;
                    
                    % ListeLayers.SelectionLayers{2,2}
                    
                case 'KongsbergTxTiltAngle'
                    ListeLayers.SelectionLayers{2,2}(1) = 1;
                case 'KongsbergFocusRange'
                    ListeLayers.SelectionLayers{2,2}(2) = 1;
                case 'PulseLength'
                    ListeLayers.SelectionLayers{2,2}(3) = 1;
                case 'KongsbergSectorTransmitDelay'
                    ListeLayers.SelectionLayers{2,2}(4) = 1;
                case 'Frequency'
                    ListeLayers.SelectionLayers{2,2}(5) = 1;
                case 'KongsbergWaveformIdentifier'
                    ListeLayers.SelectionLayers{2,2}(6) = 1;
                case 'SignalBandwith'
                    ListeLayers.SelectionLayers{2,2}(7) = 1;
                case 'RayPathLength' % One way length in meters
                    ListeLayers.SelectionLayers{2,2}(8) = 1;
                case 'RayPathSampleNb'
                    ListeLayers.SelectionLayers{2,2}(9) = 1;
                case 'RxTime'
                    ListeLayers.SelectionLayers{2,2}(10) = 1;
                case 'Roll'
                    ListeLayers.SelectionLayers{2,2}(11) = 1;
                case 'Pitch'
                    ListeLayers.SelectionLayers{2,2}(12) = 1;
                case 'Heave'
                    ListeLayers.SelectionLayers{2,2}(13) = 1;
                case 'Heading'
                    ListeLayers.SelectionLayers{2,2}(14) = 1;
                case 'TxBeamIndexSwath'
                    ListeLayers.SelectionLayers{2,2}(15) = 1;
                case 'Speed'
                    ListeLayers.SelectionLayers{2,2}(16) = 1;
                case 'BeamAngles/Earth'
                    ListeLayers.SelectionLayers{2,2}(17) = 1;
                case 'Mode' %GLT
                    ListeLayers.SelectionLayers{2,2}(18) = 1;
                case 'TxAngleKjell'
                    ListeLayers.SelectionLayers{2,2}(19) = 1;
                case 'AlongBeamPointingAngle'
                    ListeLayers.SelectionLayers{2,2}(20) = 1;
                    
                case 'IfremerQF'
                    ListeLayers.SelectionLayers{3,1}(1) = 1;
                    ListeLayers.SelectionLayers{3,2}(1) = 1;
                    ListeLayers.SelectionLayers{3,3}(1) = 1;
                    
                  % TODO : cela a l'air faux : (2) devrait �tre Detection info  
                case 'KongsbergSnippetSortDirection'
                    ListeLayers.SelectionLayers{4,1}(1) = 1;
                case 'KongsbergSnippetNbSamples'
                    ListeLayers.SelectionLayers{4,1}(2) = 1;
                case 'KongsbergSnippetCentralSample'
                    ListeLayers.SelectionLayers{4,1}(3) = 1;
                case 'KongsbergSnippetRxBeamIndex'
                    ListeLayers.SelectionLayers{4,1}(4) = 1;
                case 'KongsbergAppliedTVG'
                    ListeLayers.SelectionLayers{4,1}(5) = 1;
                    
                     %{
                      89 <--> KongsbergSnippetSortDirection                                                                                                                                                                                                                                                            
                      90 <--> KongsbergSnippetNbSamples                                                                                                                                                                                                                                                                
                      91 <--> KongsbergSnippetCentralSample                                                                                                                                                                                                                                                            
                      92 <--> KongsbergSnippetRxBeamIndex 
      InfoBeamSortDirection: [1184x512 cl_memmapfile]
      InfoBeamDetectionInfo: [1184x512 cl_memmapfile]
      InfoBeamNbSamples: [1184x512 cl_memmapfile]
      InfoBeamCentralSample: [1184x512 cl_memmapfile]
                    %}
                   
                    % ListeLayers.SelectionLayers{4,2}
                    
                    
                otherwise
                    str1 = sprintf('"%s" non pr�vu dans Simrad_getDefaultLayersV2, contactez sonarscope@ifremer.fr.', nomLayers{k});
                    str2 = sprintf('"%s" not recognized in Simrad_getDefaultLayersV2, please report to sonarscope@ifremer.fr.', nomLayers{k});
                    my_warndlg(Lang(str1,str2), 1);
            end
        end
        
    otherwise
        str1 = 'Un seul layer � la fois.';
        str2 = 'Only one layer at a time please.';
        my_warndlg(Lang(str1,str2), 1);
end


ListeLayers.nomLayers{1,1} = {'Mask'; 'Depth'; 'AcrossDist'; 'AlongDist'; 'LengthDetection'; 'QualityFactor'; 'BeamIBA'; 'DetectionInfo'; 'RealTimeCleaningInfo'; 'Reflectivity'; 'ReflectivityFromSnippets'; 'ReflectivityFromSnippetsWithoutSpecularRestablishment'; 'ReflectivityBestBS'; 'WCSignalWidth'; 'Reflectivity1'; 'Reflectivity2'};
ListeLayers.nomLayers{1,2} = {'Depth-Heave+Draught'; 'Detection'; 'NaNDetection'; 'SoundSpeedAtSeafloor'; 'AbsorptionCoefficientRT'; 'AbsorptionCoefficientSSc'; 'AbsorptionCoefficientUser'; 'IncidenceAngle'; 'SlopeAcross'; 'SlopeAlong'; 'BathymetryFromDTM'; 'InsonifiedAreaSSc'; 'PortStarboard'; 'Depth-Heave+Draught+Tide'};
ListeLayers.nomLayers{2,1} = {'BeamPointingAngle'; 'TransmitSectorNumber'; 'TwoWayTravelTimeInSeconds'};
ListeLayers.nomLayers{2,2} = {'TiltAngle'; 'FocusRange'; 'SignalLength'; 'SectorTransmitDelay'; 'CentralFrequency'; 'SignalWaveformIdentifier'; 'SignalBandwith'
                    'DoNotUse'; 'TwoWayTravelTimeInSamples'; 'RxTime'; 'Roll'; 'Pitch'; 'Heave'; 'Heading'; 'TxBeamIndexSwath'; 'Speed'; 'BeamAngles/Earth'; 'Mode'; 'TxAngleKjell'; 'AlongBeamPointingAngle'};
ListeLayers.nomLayers{3,1} = {'IfremerQF'; 'UncertaintyPerCent'; 'UncertaintyMeters'};
ListeLayers.nomLayers{3,2} = {};%{'DepthAmp'; 'DepthPhase'};
ListeLayers.nomLayers{4,1} = {'InfoBeamSortDirection'; 'InfoBeamDetectionInfo'; 'InfoBeamNbSamples'; 'InfoBeamCentralSample'; 'detection_time_varying_gain'};
ListeLayers.nomLayers{4,2} = {};

for k1=1:size(ListeLayers.nomLayers,1)
    for k2=1:size(ListeLayers.nomLayers,2)
        ListeLayers.Order{k1,k2} = zeros(1,length(ListeLayers.nomLayers{k1,k2}));
    end
end

ListeLayers.Units{1,1} = {' '; 'm'; 'm'; 'm'; ''; ''; 'deg'; ''; ''; 'dB'; 'dB'; 'dB'; 'dB'; 's'; 'dB'; 'dB'};
ListeLayers.Units{1,2} = {'m'; ''; ''; 'm/s'; 'dB/km'; 'dB/km'; 'dB/km'; 'deg'; 'deg'; 'deg'; 'm'; 'dB'; ''; 'm'};
ListeLayers.Units{2,1} = {'deg'; ''; 's'};
ListeLayers.Units{2,2} = {'deg'; 'm'; 's'; 'ms'; 'kHz'; ''; 'kHz'; 'm'; ''; 'day since JC'; 'deg'; 'deg'; 'm'; 'deg'; 'num'; 'm/s'; 'deg'; ''; 'deg'; 'deg'};
ListeLayers.Units{3,1} = {' '; '%'; 'm'};
ListeLayers.Units{3,2} = {};
ListeLayers.Units{4,1} = {' '; ' '; ' '; ' '; 'dB'};
ListeLayers.Units{4,2} = {};

ListeLayers.Comments{1,1} = {'Mask of soundings (imported from Caris, Globe or defined in SSc)'
    '"Depth" from "xyz 88" datagrams'
    '"Acrosstrack distance" from "xyz 88" datagrams'
    '"Alongtrack Distance" from "xyz 88" datagrams'
    '"Detection window length in samples" from "xyz 88" datagrams'
    '"Quality Factor" from "xyz 88" datagrams : sd/dr , sd=Scaled std of the range detection, dr=range'
    '"Beam incidence angle adjustment (IBA)" from "xyz 88" datagrams'
    '"Detection info" from "xyz 88" datagrams'
    '"Real time cleaning info" from "xyz 88" datagrams'
    '"Reflectivity" from "xyz 88" datagrams'
    '"Reflectivity" from "Snippets'
    'Reflectivity from "Snippets" without specular re-establishment'
    'ReflectivityBestBS'
    'WCSignalWidth'
    'Reflectivity1'
    'Reflectivity2'};
ListeLayers.Comments{1,2} = {'Depth-Heave+Draught deduced from layer "Depth" in depth datagrams'
    'Detection mode (Amplitude/phase) deduced from layer "Detection" in depth datagrams'
    'Type of interpolation if no detection deduced from layer "Detection" in depth datagrams'
    'Sound speed used for every beam'
    '"Mean absorption coeff" deduced from "seabed image" datagrams'
    '"Mean absorption coeff" deduced from the sound speed profiles datagrams'
    '"Mean absorption coeff" defined by user using Survey Processing'
    '"IncidenceAngle" defined by user using Survey Processing'
    '"SlopeAcross" defined by user using Survey Processing'
    '"SlopeAlong" defined by user using Survey Processing'
    '"BathymetryFromDTM" defined by user using Survey Processing'
    '"Insonified Area" computed with slopes'
    '"1=Port, 2=starboard"'
    'Depth-Heave+Draught+Tide deduced from layer "Depth" in depth datagrams'};
% Attention ""Insonified Area" computed with slopes'" est test�e dans paramsFonctionSonarAireInsonifiee + ...

ListeLayers.Comments{2,1} = {'"Beam pointing angle re Rx array" from "Raw range & angle 78" datagrams'
    '"Transmit sector number" + 1 from "Raw range & angle 78" datagrams'
    '"Two way travel time" from "Raw range & angle 78" datagrams'};
ListeLayers.Comments{2,2} = {'"Tilt angle re Tx array" deduced from "Raw range & angle 78" datagrams'
    '"Focus range" deduced from "Raw range & angle 78" datagrams'
    '"Signal length" deduced from "Raw range & angle 78" datagrams'
    '"Sector transmit delay re first Tx pulse" deduced from "Raw range & angle 78" datagrams'
    '"Central frequency" deduced from "Raw range & angle 78" datagrams'
    '"Signal waveform identifier" deduced from "Raw range & angle 78" datagrams'
    '"Signal bandwith" deduced from "Raw range & angle 78" datagrams'
    'RayPathLength deduced from "Raw range & angle 78" datagrams'
    'RayPathSampleNb deduced from "Raw range & angle 78" datagrams'
    'UTM reception Time deduced from "Two way travel time" in "Raw range & angle 78" datagrams'
    'Roll deduced from "Two way travel time" in "Raw range & angle 78" datagrams & Roll in "Attitude" datagrams.'
    'Pitch deduced from "Two way travel time" in "Raw range & angle 78" datagrams & Pitch in "Attitude" datagrams.'
    'Heave deduced from "Two way travel time" in "Raw range & angle 78" datagrams & Heave in "Attitude" datagrams.'
    'Heading deduced from "Two way travel time" in "Raw range & angle 78" datagrams & Heading in "Attitude" datagrams.'
    'TxBeamIndexSwath'
    'Speed'
    'Beam Angles / Earth'
    'Mode'
    'TxAngleKjell'
    'AlongBeamPointingAngle'};

ListeLayers.Comments{3,1} = {'Ifremer Quality Factor'; 'Uncertainty in % of dZ/z'; 'Uncertainty in meters'};
ListeLayers.Comments{3,2} = {};
%     'Depth from Phase detection'};

% TODO : 
ListeLayers.Comments{4,1} = {'"KongsbergSnippetSortDirection'
    'KongsbergSnippetNbSamples'
    'KongsbergSnippetCentralSample'
    'KongsbergSnippetRxBeamIndex'
    'KongsbergAppliedTVG'};
ListeLayers.Comments{4,2} = {};
