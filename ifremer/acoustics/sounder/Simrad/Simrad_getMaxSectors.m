function MS = Simrad_getMaxSectors(EmModel, Mode, MaxSectors)

persistent DejaPasseIci

switch EmModel
    case 122
        switch Mode
            case 2
                MS = 4;
            case 3
                MS = 4;
            case 4
                MS = 8;
            case 5
                MS = 6;
            otherwise
                if ~isdeployed && isempty(DejaPasseIci)
                    message(EmModel, Mode)
                    DejaPasseIci = 1;
                end
                MS = MaxSectors;
        end
        
    case 302
        switch Mode
            case 2
                MS = 4;
            case 3
                MS = 4;
            case 4
                MS = 8;
            case 5
                MS = 6;
            case 6
                MS = 4;
            otherwise
                if ~isdeployed && isempty(DejaPasseIci)
                    message(EmModel, Mode)
                    DejaPasseIci = 1;
                end
                MS = MaxSectors;
        end

    % case 304 TODO

    case 710
        switch Mode
            case 1
                MS = 3;
            case 2
                MS = 3;
            case 3
                MS = 3;
            case 4
                MS = 3;
            case 5
                MS = 3;
            case 6
                MS = 3;
            otherwise
                if ~isdeployed && isempty(DejaPasseIci)
                    message(EmModel, Mode)
                    DejaPasseIci = 1;
                end
                MS = MaxSectors;
        end
        
    case 850
        MS = 1;
        
    case 2040
        switch Mode
            case 2
                MS = 3;
            case 3
                MS = 3; % 4 is Dual ?
            otherwise
                if ~isdeployed && isempty(DejaPasseIci)
                    message(EmModel, Mode)
                    DejaPasseIci = 1;
                end
                MS = MaxSectors;
        end
        
    case 2045
        switch Mode
            case 4
                MS = 1;
            otherwise
                if ~isdeployed && isempty(DejaPasseIci)
                    message(EmModel, Mode)
                    DejaPasseIci = 1;
                end
                MS = MaxSectors;
        end
        
    otherwise
        if ~isdeployed && isempty(DejaPasseIci)
            message(EmModel, Mode)
            DejaPasseIci = 1;
        end
        MS = MaxSectors;
end


function message(EmModel, Mode)
str1 = sprintf('Compléter Simrad_getMaxSectors : Model %d Mode = %d', EmModel, Mode);
str2 = sprintf('Complete Simrad_getMaxSectors : Model %d Mode = %d', EmModel, Mode);
my_warndlg(Lang(str1,str2), 0, 'Tag', 'Simrad_getMaxSectorsACompleter');
