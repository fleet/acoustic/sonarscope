function maskEntries = Simrad_maskEntries(Entries, InfoBeamNbSamples, InfoBeamCentralSample, ...
    sublDepth, sublImage, sublRaw, SeabedImageFormat, isDual, DataDepth, DataRaw, DataSeabed)

maskEntries = false(size(Entries));

nbPings = size(InfoBeamNbSamples, 1);
nbBeams = DataSeabed.Dimensions.nbBeams;

nbBeamsBySide = floor(nbBeams / 2);
SystemSerialNumber = DataSeabed.SystemSerialNumber;
nbSounders = length(unique(SystemSerialNumber(~isnan(SystemSerialNumber))));

idebEntriesPing = ones(1,nbPings); % zeros en C
for iPing=2:nbPings
    n = (InfoBeamNbSamples(iPing-1,:));
    n(isnan(n)) = 0;
    idebEntriesPing(iPing) = idebEntriesPing(iPing-1) + double(sum(n));
end

str1 = 'Gestion des recouvrement des snippets.';
str2 = 'Suppres the overlaps of snippets';
hw = create_waitbar(Lang(str1,str2), 'N', nbPings);
for kPing=1:nbPings
    my_waitbar(kPing, nbPings, hw)
    iPingDepth = sublDepth(kPing);
    iPingImage = sublImage(kPing);
    iPingRaw   = sublRaw(kPing);
    if sublImage(kPing) <= nbPings % Test rajout� le 05/06/2013 pour fichier Charline 20040510_142810_raw.all
        
        if nbSounders == 1
            if isDual
                if SeabedImageFormat == 1
                    isBab = DataDepth.BeamDepressionAngle(iPingDepth,nbBeamsBySide) < 0;
                else
                    isBab = DataRaw.BeamPointingAngle(iPingRaw,nbBeamsBySide) < 0;
                end
                isBab = repmat(isBab, 1, nbBeams);
            else
                isBab = [true(1, nbBeamsBySide) false(1, nbBeamsBySide)];
                isBab(nbBeams) = isBab(nbBeams-1); % 111 = 2*55+1
            end
        else
            isBab = [true(1, nbBeamsBySide) false(1, nbBeamsBySide)];
        end
                
        subEntries = idebEntriesPing(sublImage(kPing))-1; % Beginning of the snippet in the series of samples
        IBNS = InfoBeamNbSamples(sublImage(kPing),:);
        IBCS = InfoBeamCentralSample(sublImage(kPing),:);
        sub = find((IBNS ~= 0) & ~isnan(IBNS)); % subValidBeams
        n2 = length(sub);  % nbValidBeams
        sub2 = cell(1, n2);
        sub3 = cell(1, n2);
        sub4 = cell(1, n2);
        
        if SeabedImageFormat == 1
            Range = floor(DataDepth.Range(iPingDepth,:)); % Range in samples
        else
            % TODO : faire le m�nage ici ainsi que dans computeReflectivityByBeam
           try
                Range = floor(DataRaw.TwoWayTravelTimeInSeconds(iPingRaw,:) * DataSeabed.SamplingRate(iPingImage));
            catch %#ok<CTCH>
                Range = floor(DataRaw.TwoWayTravelTime(iPingRaw,:) * DataSeabed.SamplingRate(iPingImage));
            end
        end
        
        for k2=1:n2 % Loop on valid beams
            iBeam = sub(k2); % Beam number
            n = double(IBNS(iBeam)); % Number of samples of the snippet
            subEntries1 = subEntries;
            subEntries = subEntries(end) + (1:n); % Indices of the samples of the snippet in the samples serie
            sub2{k2} = subEntries; % Store the samples of the snippet into a cell array
            
            IBCSiBeam = IBCS(iBeam);
            if isBab(iBeam)
                sub3{k2} = (Range(iBeam) - n + IBCSiBeam - 1) + (1:n); % Computes the sonar sample numbers in the SampleBeam geometry
                indCentre(k2) = IBCSiBeam;
                color(k2) = 'g';
            else
                sub3{k2} = (Range(iBeam) - IBCSiBeam)+ (1:n);
                indCentre(k2) = n - IBCSiBeam + 1;
                color(k2) = 'r';
            end
            sub4{k2} = sub3{k2};
            maskEntries(subEntries(indCentre(k2))) = 1;
            if k2 > 1
                k1 = k2 - 1;
                s1 = sub4{k1};
                s2 = sub4{k2};
%                 s2(indCentre) = [];
                [c, is1, is2] = intersect(s1, s2);
                if ~isempty(c)
                    % TODO : Dimitrios, that will be a game for you
                    is1;
                    is2;
                    
%                     sub4{k2-1}(is1) = [];
%                     sub4{k1}(setdiff(is1, indCentre(k1))) = [];
%                     sub4{k2}(setdiff(is2, indCentre(k2))) = [];
                    sub4{k1} = sort([sub4{k1} Range(sub(k1))]);
                    sub4{k2}(setdiff(is2, indCentre(k2))) = [];
                    maskEntries(subEntries1(is1)) = 1;
               end
            end
        end
        
        if mod(kPing,floor(nbPings/5)) == 1
            figure(7863)
            for k2=1:n2
                iBeam = sub(k2); % Beam number
                plot(iBeam*ones(1,length(sub3{k2})), sub3{k2}, ['-*' color(k2)]); grid on; hold on;
                plot(iBeam*ones(1,length(sub4{k2})), sub4{k2}, 'ok');
            end
            PlotUtils.createSScPlot(Range, 'k');
            title(['Ping ' num2str(kPing)])
            set(gca, 'YDir', 'reverse')
        end       
    end
end
my_close(hw, 'MsgEnd')
