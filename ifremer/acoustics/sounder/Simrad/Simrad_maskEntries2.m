function maskEntries = Simrad_maskEntries2(Entries, InfoBeamNbSamples, InfoBeamCentralSample, InfoBeamSortDirection, ...
    sublDepth, sublImage, sublRaw, SeabedImageFormat, isDual, DataDepth, DataRaw, DataSeabed)

maskEntries = false(size(Entries));

nbPings = size(InfoBeamNbSamples, 1);
% nbBeams = DataSeabed.Dimensions.nbBeams;

% nbBeamsBySide = floor(nbBeams / 2);
% SystemSerialNumber = DataSeabed.SystemSerialNumber;
% nbSounders = length(unique(SystemSerialNumber(~isnan(SystemSerialNumber))));

idebEntriesPing = ones(1,nbPings); % zeros en C
for iPing=2:nbPings
    n = (InfoBeamNbSamples(iPing-1,:));
    n(isnan(n)) = 0;
    idebEntriesPing(iPing) = idebEntriesPing(iPing-1) + double(sum(n));
end
clear n

str1 = 'Gestion des recouvrement des snippets.';
str2 = 'Suppres the overlaps of snippets';
hw = create_waitbar(Lang(str1,str2), 'N', nbPings);
for kPing=1:nbPings
    my_waitbar(kPing, nbPings, hw)
    iPingDepth = sublDepth(kPing);
    iPingImage = sublImage(kPing);
    iPingRaw   = sublRaw(kPing);
    if sublImage(kPing) > nbPings % Test rajout� le 05/06/2013 pour fichier Charline 20040510_142810_raw.all
        continue
    end
    
    IBNS = InfoBeamNbSamples(sublImage(kPing),:);
    IBCS = InfoBeamCentralSample(sublImage(kPing),:);
    IBSD = InfoBeamSortDirection(sublImage(kPing),:);
    subValidBeams = find((IBNS ~= 0) & ~isnan(IBNS));
    nbValidBeams  = length(subValidBeams);
    %         sub2 = cell(1, nbValidBeams);
    %         sub3 = cell(1, nbValidBeams);
    %         sub4 = cell(1, nbValidBeams);
    
    if SeabedImageFormat == 1
        Range = floor(DataDepth.Range(iPingDepth,:)); % Range in samples
    else
        % TODO : faire le m�nage ici ainsi que dans computeReflectivityByBeam
        try
            Range = floor(DataRaw.TwoWayTravelTimeInSeconds(iPingRaw,:) * DataSeabed.SamplingRate(iPingImage));
        catch %#ok<CTCH>
            Range = floor(DataRaw.TwoWayTravelTime(iPingRaw,:) * DataSeabed.SamplingRate(iPingImage));
        end
    end
    
    %% Initialisation du premier beam
    
    iBeam2 = subValidBeams(1);
    n2 = double(IBNS(iBeam2)); % Number of samples of the snippet
    c2 = IBCS(iBeam2);
    rc2 = Range(iBeam2);
    if IBSD(iBeam2) == -1
        r12 = Range(iBeam2) - (n2-c2) + 1;
        rn2 = r12 + n2 - 1;
    else
        r12 = Range(iBeam2) - c2;
        rn2 = r12 + n2 - 1;
    end
    subEntries = idebEntriesPing(iPingImage)-1 + cumsum([1 IBNS(subValidBeams)]); % Beginning of the snippet in the series of samples
    
    %% Boucle sur les faisceaux suivants
    
    for k2=2:nbValidBeams % Loop on valid beams
        iBeam1 = iBeam2;
        c1 = c2;
        n1  = n2;
        r11 = r12;
        rc1 = rc2;
        rn1 = rn2;
        
        iBeam2 = subValidBeams(k2); % Beam number
        n2 = double(IBNS(iBeam2)); % Number of samples of the snippet
        c2 = IBCS(iBeam2);
        
        rc2 = Range(iBeam2);
        if IBSD(iBeam2) == -1
            r12 = Range(iBeam2) - (n2-c2) + 1;
            rn2 = r12 + n2 - 1;
        else
            r12 = Range(iBeam2) - c2;
            rn2 = r12 + n2 - 1;
        end
        
        m = floor((rc1 + rc2) / 2);
        if  rc2 > rc1
            m - rc1 %#ok<NOPRT>
%             maskEntries(subEntries(xxxx)) = 1;
        else
        end
        
        %{
        subEntries1 = subEntries;
        subEntries = subEntries(end) + (1:n2); % Indices of the samples of the snippet in the samples serie
        sub2{k2} = subEntries; % Store the samples of the snippet into a cell array
        
        IBCSiBeam = IBCS(iBeam2);
        if isBab(iBeam2)
            sub3{k2} = (Range(iBeam2) - n2 + IBCSiBeam - 1) + (1:n2); % Computes the sonar sample numbers in the SampleBeam geometry
            indCentre(k2) = IBCSiBeam;
            color(k2) = 'g';
        else
            sub3{k2} = (Range(iBeam2) - IBCSiBeam)+ (1:n2);
            indCentre(k2) = n2 - IBCSiBeam + 1;
            color(k2) = 'r';
        end
        sub4{k2} = sub3{k2};
       
        if k2 > 1
            k1 = k2 - 1;
            s1 = sub4{k1};
            s2 = sub4{k2};
            %                 s2(indCentre) = [];
            [c, is1, is2] = intersect(s1, s2);
            if ~isempty(c)
                % TODO : Dimitrios, that will be a game for you
                is1;
                is2;
                
                %                     sub4{k2-1}(is1) = [];
                %                     sub4{k1}(setdiff(is1, indCentre(k1))) = [];
                %                     sub4{k2}(setdiff(is2, indCentre(k2))) = [];
                sub4{k1} = sort([sub4{k1} Range(subValidBeams(k1))]);
                sub4{k2}(setdiff(is2, indCentre(k2))) = [];
                maskEntries(subEntries1(is1)) = 1;
            end
        end
        %}
    end
    
    %{
    if mod(kPing,floor(nbPings/5)) == 1
        figure(7863)
        for k2=1:nbValidBeams
            iBeam2 = subValidBeams(k2); % Beam number
            plot(iBeam2*ones(1,length(sub3{k2})), sub3{k2}, ['-*' color(k2)]); grid on; hold on;
            plot(iBeam2*ones(1,length(sub4{k2})), sub4{k2}, 'ok');
        end
        PlotUtils.createSScPlot(Range, 'k');
        title(['Ping ' num2str(kPing)])
        set(gca, 'YDir', 'reverse')
    end
    %}
end
my_close(hw, 'MsgEnd')
