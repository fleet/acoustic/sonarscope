function [flag, Info] = addScaleOffsetStorage(Datagrams, Info)

flag = 1; %#ok<NASGU>

% Propagation des balises ScaleFactor et AddOffset si elles existent +
% conservation de l'information de format d'origine.
for k=1:length(Info.Signals)
    % Identification du signal � conserver et les Datagrammes lus.
    % Vide si le signal est conserv� sous type cl_time.
    identSignal = findIndVariable(Datagrams.Variables, Info.Signals(k).Name);
    if ~isempty(identSignal)
        pppp = Datagrams.Variables(identSignal);
        if (isfield(pppp, 'ScaleFactor') && ~isempty(pppp.ScaleFactor) && (pppp.ScaleFactor ~= 1))
            Info.Signals(k).ScaleFactor = pppp.ScaleFactor;
        end
        if (isfield(pppp, 'AddOffset') && ~isempty(pppp.AddOffset) && (InfoSignal.AddOffset ~= 0))
            Info.Signals(k).AddOffset = pppp.AddOffset;
        end
        Info.Signals(k).OrigStorage = Datagrams.Variables(identSignal).Storage;
    else
        % Traitement particulier des images stock�es en signal (cf.
        % InstallationParameters).
        if isfield(Datagrams, 'Tables')
            identSignal = findIndVariable(Datagrams.Tables, Info.Signals(k).Name);
            if ~isempty(identSignal)
                pppp = Datagrams.Tables(identSignal);
                if (isfield(pppp, 'ScaleFactor') && ~isempty(pppp.ScaleFactor) && (pppp.ScaleFactor ~= 1))
                    Info.Signals(k).ScaleFactor = pppp.ScaleFactor;
                end
                if (isfield(pppp, 'AddOffset') && ~isempty(pppp.AddOffset) && (pppp.AddOffset ~= 0))
                    Info.Signals(k).AddOffset = pppp.AddOffset;
                end
                Info.Signals(k).OrigStorage = Datagrams.Tables(identSignal).Storage;
            end
        end
    end
end

if isfield(Datagrams, 'Tables') && isfield(Info, 'Images')
    for k=1:length(Info.Images)
        % Identification du signal � conserver et les Datagrammes lus.
        % Vide si le signal est conserv� sous type cl_time.
        identImage = findIndVariable(Datagrams.Tables, Info.Images(k).Name);
        if ~isempty(identImage)
            pppp = Datagrams.Tables(identImage);
            Info.Images(k).OrigStorage = Datagrams.Tables(identImage).Storage;
            if (isfield(pppp, 'ScaleFactor') && ~isempty(pppp.ScaleFactor) && (pppp.ScaleFactor ~= 1))
                Info.Images(k).ScaleFactor = pppp.ScaleFactor;
            end
            if (isfield(pppp, 'AddOffset') && ~isempty(pppp.AddOffset) && (pppp.AddOffset ~= 0))
                Info.Images(k).AddOffset = pppp.AddOffset;
            end
        end
    end
end

flag = 0;
