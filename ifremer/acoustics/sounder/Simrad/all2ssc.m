function [flag, flagRTK, nomFicWc, EmModel, SystemSerialNumber, Version, SousVersion] = all2ssc(nomFic, varargin)

global IfrTbxResources %#ok<GVMIS>
global IfrTbx %#ok<GVMIS>
global isUsingParfor %#ok<GVMIS>
global IdentAlgoSnippets2OneValuePerBeam %#ok<GVMIS>
global useCacheNetcdf %#ok<GVMIS>
global useParallel %#ok<GVMIS>
global logFileId %#ok<GVMIS>
 
[varargin, logFileId]       = getPropertyValue(varargin, 'logFileId',       logFileId);
[varargin, useParallel]     = getPropertyValue(varargin, 'useParallel',     useParallel);
[varargin, ShowProgressBar] = getPropertyValue(varargin, 'ShowProgressBar', 1);
[varargin, useCacheNetcdf]  = getPropertyValue(varargin, 'useCacheNetcdf',  useCacheNetcdf);

if isempty(IfrTbxResources)
    loadVariablesEnv
end

if isempty(isUsingParfor)
    isUsingParfor = 0;
elseif isUsingParfor == 1
    loadVariablesEnv
    %     fprintf('IfrTbx = "%s"\n', IfrTbx);
    %     fprintf('IfrTbxResources = "%s"\n', IfrTbxResources);
end

if isempty(IdentAlgoSnippets2OneValuePerBeam)
    IdentAlgoSnippets2OneValuePerBeam = 2;
end

[varargin, RDL]             = getPropertyValue(varargin, 'RDL',             0);
[varargin, isUsingParfor]   = getPropertyValue(varargin, 'isUsingParfor',   isUsingParfor);
[varargin, IfrTbxResources] = getPropertyValue(varargin, 'IfrTbxResources', IfrTbxResources);
[varargin, IfrTbx]          = getPropertyValue(varargin, 'IfrTbx',          IfrTbx); %#ok<ASGLU>

FormatVersion = 20101118;

flagRTK            = 0;
nomFicWc           = [];
EmModel            = [];
SystemSerialNumber = [];
Version            = [];
SousVersion        = [];

%% Test d'existence du fichier

flag = exist(nomFic, 'file');
if flag ~= 2
    str1 = sprintf('Le fichier "%s" n''existe pas.', nomFic);
    str2 = sprintf('"%s" doesn not exist', nomFic);
    my_warndlg(Lang(str1,str2), 0, 'Tag', 'FichierAllNonTrouve');
    return
end

%% Test si le fichier est vide

if sizeFic(nomFic) == 0
    str1 = sprintf('Le fichier "%s" est vide. Je le renomme en  "xxx_all.bad"', nomFic);
    str2 = sprintf('"%s" is empty, I rename it in "xxx_all.bad" file.', nomFic);
    renameBadFile(nomFic, 'Message', Lang(str1,str2))
    flag = 0;
    return
end

%% Test de la longueur du nom de fichier

lenghtMax = 184;
if length(nomFic) > lenghtMax
    str1 = sprintf('Le nom du fichier "%s" est trop long, raccourcissez-le d''au moins %d caract�res', nomFic, length(nomFic)-lenghtMax);
    str2 = sprintf('The file name "%s" is too long, shorten it (at least %d characters)', nomFic, length(nomFic)-lenghtMax);
    flag = 0;
    my_warndlg(Lang(str1,str2), 1, 'Tag', 'FichierAllNonTrouve');
    return
end

%% Localisation de l'ex�cutable "CONVERT_ALL.exe"

nomExe = 'CONVERT_ALL.exe';
if isdeployed
    nomDir = fileparts(IfrTbxResources);
    nomExe = fullfile(nomDir, 'extern', 'C', nomExe);
else
    nomExe = [IfrTbx '\ifremer\extern\C\CONVERT_exe\Release\' nomExe];
end

%% Test si le fichier a d�j� �t� d�cod�

previous_useCacheNetcdf = useCacheNetcdf;
useCacheNetcdf = 1;

[flag, TypeCache] = all2ssc_checkIfAlreadyDecoded(nomFic); %#ok<ASGLU> 
if flag
    flagRTK = ALL_isRTKAvailable(nomFic);
    [flag, Data] = SSc_ReadDatagrams(nomFic, 'Depth', 'XMLOnly', 1);
    if flag
        EmModel = Data.EmModel;
        [flag, varVal] = SSc_getSignalUnit(nomFic, 'Depth', 'SystemSerialNumber');
        if flag
            SystemSerialNumber = unique(varVal);
        end
    end
    [Version, SousVersion] = getDatagramVersion(nomFic);
    useCacheNetcdf = previous_useCacheNetcdf;
    return
end

%% G�n�ration des XML et des binaires

[nomDir, nomFicSeul] = fileparts(nomFic);
nomDirRacine = fullfile(nomDir, 'SonarScope', nomFicSeul);

%% Lancement du d�codage

nomFicTmp = [nomDirRacine '_tmp'];
if exist(nomFicTmp, 'dir')
    message_erreur_convert(nomFicTmp, nomFic, nomDirRacine)
    flag = 0;
    useCacheNetcdf = previous_useCacheNetcdf;
    return
end

nomFicWCD = strrep(nomFic, '.all', '.wcd');
if exist(nomFicWCD, 'file')
    ALL.Process.RepairDatagrasWC(nomFicWCD)
else
    ALL.Process.RepairDatagrasWC(nomFic)
end

str1 = sprintf('"%s" est en cours de lecture.', nomFic);
str2 = sprintf('"%s" is being read.', nomFic);
WorkInProgress(Lang(str1,str2))

cmd = sprintf('!"%s" "%d" "%s"', nomExe, ShowProgressBar, nomFic);
% fprintf('\n%s\n', cmd)
try
    [~, msg] = dos(cmd(2:end));
catch %#ok<CTCH>
    str1 = sprintf('CONVERT_ALL.exe semble avoir rencontr� un probl�me sur le fichier %s', nomFic);
    str2 = sprintf('CONVERT_ALL.exe does not seem working for file %s', nomFic);
    my_warndlg(Lang(str1,str2), 0);
end

pause(1)
if ~exist(nomDirRacine, 'dir') || exist([nomDirRacine '_tmp'], 'dir')
    try
        movefile([nomDirRacine '_tmp'], nomDirRacine, 'f')
        while 1
            if exist(nomDirRacine, 'dir')
                break
            end
            pause(1)
        end
    catch ME
        fprintf('%s\n', getReport(ME));
    end
end

if ~exist(nomDirRacine, 'dir')
    if ~isa('msg', 'var') || isempty(msg)
        str1 = sprintf('CONVERT_ALL.exe semble avoir rencontr� un probl�me sur le fichier %s', nomFic);
        str2 = sprintf('CONVERT_ALL.exe does not seem working for file %s', nomFic);
        msg = Lang(str1,str2);
    end
    my_warndlg(msg, 0, 'Tag', nomFic);
    
    if length(nomFic) > 128
        str1 = sprintf('Le nom du fichier est peut-�tre trop long pour �tre interpr�t� par la commande DOS, essayer de raccourcsa longueur : %s (%d caract�res).', nomFic, length(nomFic));
        str2 = sprintf('The file name is perhaps too long to be interpreted by the DOS command, try to shorten it : %s (%d characters)', nomFic, length(nomFic));
        msg = Lang(str1,str2);
        my_warndlg(msg, 0, 'Tag', 'CONVERT_ALL_Bug');
    end
    
    flag = 0;
    SimradAll_rename_all(nomFic, 0)
    useCacheNetcdf = previous_useCacheNetcdf;
    return
end

%% Datagrammes InstallationParameters

flag = all2ssc_InstallationParameters(nomDirRacine, FormatVersion);
if ~flag
    SimradAll_rename_all(nomFic, 0)
    useCacheNetcdf = previous_useCacheNetcdf;
    return
end

if ~RDL
    nomFicXml = fullfile(nomDirRacine, 'Ssc_InstallationParameters.xml');
    InstallationParameters = xml_read(nomFicXml);
    % Translation des champs de Line dans la structure InstallationParameters.
    names = fieldnames(InstallationParameters.Line);
    for k=1:numel(names)
        InstallationParameters.(char(names(k))) = InstallationParameters.Line.(char(names(k)));
    end
    InstallationParameters = rmfield(InstallationParameters, 'Line');
    Head1.SystemSerialNumber           =  InstallationParameters.SystemSerialNumber;
    Head1.S1R                          = -InstallationParameters.S1R; % Attention : signe moins car convention invers�e
    Head1.SystemSerialNumberSecondHead =  InstallationParameters.SystemSerialNumberSecondHead;
    %         Head1.SystemSerialNumberSecondHead = 0; % Test pour fichier WC Dual head Simon Stevins
    if isfield(InstallationParameters, 'STC') && (InstallationParameters.EmModel == 2045)
        if Head1.SystemSerialNumberSecondHead == 0
            N = 1;
        else
            N = 2;
        end
        for k=1:N
            if k == 1
                nameIPField = 'SystemSerialNumber';
            else
                nameIPField = 'SystemSerialNumberSecondHead';
            end
            Head1.TX(k).SystemSerialNumber = Head1.(nameIPField);
            nameIPField               = ['S' num2str(k) 'X'];
            nameTXField               = 'SX';
            Head1.TX(k).(nameTXField) = InstallationParameters.(nameIPField);
            nameIPField               = ['S' num2str(k) 'Y'];
            nameTXField               = 'SY';
            Head1.TX(k).(nameTXField) = InstallationParameters.(nameIPField);
            nameIPField               = ['S' num2str(k) 'Z'];
            nameTXField               = 'SZ';
            Head1.TX(k).(nameTXField) = InstallationParameters.(nameIPField);
            nameIPField               = ['S' num2str(k) 'P'];
            nameTXField               = 'SP';
            Head1.TX(k).(nameTXField) = InstallationParameters.(nameIPField);
            nameIPField               = ['S' num2str(k) 'R'];
            nameTXField               = 'SR';
            Head1.TX(k).(nameTXField) = InstallationParameters.(nameIPField);
            nameIPField               = ['S' num2str(k) 'H'];
            nameTXField               = 'SH';
            Head1.TX(k).(nameTXField) = InstallationParameters.(nameIPField);
            nameIPField               = ['GO' num2str(k)];
            nameTXField               = 'GO';
            Head1.TX(k).(nameTXField) = InstallationParameters.(nameIPField);
        end
    end
end


% TODO : fa�on tr�s maladroite de g�rer les 2 t�tes. ceci est valid�
% uniquement pour EM2040 dual de la Thalia et EM2040 Dual Kongsberg
% Il faut absolument g�rer �a proprement : demander � Kongberg le mode
% d'emploi
% if (Head1.SystemSerialNumber == 212) || (Head1.SystemSerialNumberSecondHead == 212)
%     Head1.SystemSerialNumber = 10212;
%     Head1.SystemSerialNumberSecondHead = 212;
% end

% D�but bidouille pour EM2040 Thalia 
if (InstallationParameters.SystemSerialNumber == 172) && (InstallationParameters.SystemSerialNumberSecondHead  == 174)
    pppp = Head1.SystemSerialNumber;
    Head1.SystemSerialNumber = Head1.SystemSerialNumberSecondHead;
    Head1.SystemSerialNumberSecondHead = pppp;
end
if (InstallationParameters.SystemSerialNumber == 109) && (InstallationParameters.SystemSerialNumberSecondHead  == 174)
    pppp = Head1.SystemSerialNumber;
    Head1.SystemSerialNumber = Head1.SystemSerialNumberSecondHead;
    Head1.SystemSerialNumberSecondHead = pppp;
end

% Ajout JMA le 22/11/2022
if (InstallationParameters.SystemSerialNumber == 212) && (InstallationParameters.SystemSerialNumberSecondHead  == 10212)
    pppp = Head1.SystemSerialNumber;
    Head1.SystemSerialNumber = Head1.SystemSerialNumberSecondHead;
    Head1.SystemSerialNumberSecondHead = pppp;
end
% Fin bidouille pour Thalia

% D�but bidouille pour nouveau Belgica 
if (InstallationParameters.SystemSerialNumber == 2004) && (InstallationParameters.SystemSerialNumberSecondHead  == 2031)
    pppp = Head1.SystemSerialNumber;
    Head1.SystemSerialNumber = Head1.SystemSerialNumberSecondHead;
    Head1.SystemSerialNumberSecondHead = pppp;
end
% Fin bidouille pour nouveau Belgica

% D�but bidouille pour EM2040 SimonStevin apr�s changement de t�te
if (InstallationParameters.SystemSerialNumber == 353) && (InstallationParameters.SystemSerialNumberSecondHead  == 254)
    TSV = InstallationParameters.TSV;
    if datenum(TSV(6:end), 'mmm dd yyyy') >= 737127 % 'Mar 8 2018'
        pppp = Head1.SystemSerialNumber;
        Head1.SystemSerialNumber = Head1.SystemSerialNumberSecondHead;
        Head1.SystemSerialNumberSecondHead = pppp;
    end
end
% Fin bidouille pour EM2040 SimonStevin apr�s changement de t�te

if ~isempty(InstallationParameters) && isfield(InstallationParameters, 'S3R')
    SounderDualOneTxAntenna = 1;
else
    SounderDualOneTxAntenna = 0;
end

%% Datagrammes Runtime

flag = all2ssc_Runtime(nomDirRacine, FormatVersion, InstallationParameters.EmModel);
if ~flag
%     useCacheNetcdf = previous_useCacheNetcdf;
    %     return % Comment� par JMA le 28/04/2015 � Norderney fichier G:\FSK\Course\0215_20150409_111650_Nynorderoog.all
end

%% Datagrammes Attitude

flag = all2ssc_Attitude(nomDirRacine, FormatVersion);
if ~flag
    SimradAll_rename_all(nomFic, 0)
    useCacheNetcdf = previous_useCacheNetcdf;
    return
end

%% Datagrammes Network Attitude

% Comment� pour l'instant car bug dans le .xml pour fichiers EM304
% convertis � partir des .kmall
% <item>
% 			<Name>Spare</Name>
% 			<Storage>uchar</Storage>
% 			<Constant>?/Constant>
% </item>
%{
nomFicXml = fullfile(nomDirRacine, 'ALL_NetworkAttitude.xml');
if exist(nomFicXml, 'file')
    flag = all2ssc_NetworkAttitude(nomDirRacine, FormatVersion);
    if ~flag
        useCacheNetcdf = previous_useCacheNetcdf;
        return
    end
end
%}

%% Datagrammes Position

flag = all2ssc_Position(nomDirRacine, FormatVersion);
if ~flag
%     useCacheNetcdf = previous_useCacheNetcdf;
    %     return % Mise en commentaire � cause des fichiers de Maskoor : t�te sonar mont�e sur ponton
end

%% Datagrammes Clock

flag = all2ssc_Clock(nomDirRacine, FormatVersion);
if ~flag
    if ~RDL
%         useCacheNetcdf = previous_useCacheNetcdf;
        %         return
    end
end

%% Datagrammes SurfaceSoundSpeed

flag = all2ssc_SurfaceSoundSpeed(nomDirRacine, FormatVersion);
if ~flag
%     useCacheNetcdf = previous_useCacheNetcdf;
    %     return
end

%% Datagrammes SoundSpeedProfile

flag = all2ssc_SoundSpeedProfile(nomDirRacine, FormatVersion);
if ~flag
%     useCacheNetcdf = previous_useCacheNetcdf;
    %     return
end

%% Datagrammes Depth44h

NoDepth = true;
nomFicXml = fullfile(nomDirRacine, 'ALL_Depth44h.xml');
if exist(nomFicXml, 'file')
    [flag, MaxDesMaxNbBeams] = all2ssc_Depth44h(nomDirRacine, FormatVersion, Head1, nomFic);
    if ~flag
        SimradAll_rename_all(nomFic, 0)
        useCacheNetcdf = previous_useCacheNetcdf;
        return
    end
    NoDepth = false;
end

%% Datagrammes Depth58h

nomFicXml = fullfile(nomDirRacine, 'ALL_Depth58h.xml');
if exist(nomFicXml, 'file')
    flag = all2ssc_Depth58h(nomDirRacine, FormatVersion, Head1, nomFic);
    if ~flag
        SimradAll_rename_all(nomFic, 0)
        useCacheNetcdf = previous_useCacheNetcdf;
        return
    end
    NoDepth = false;
end

if NoDepth
    SimradAll_rename_all(nomFic, 0)
    flag = 0;
    useCacheNetcdf = previous_useCacheNetcdf;
    return
end

%% Datagrammes Data_RawRangeBeamAngle

nomFicXml = fullfile(nomDirRacine, 'ALL_RawRangeBeamAngle66h.xml');
if exist(nomFicXml, 'file')
    flag = all2ssc_RawRangeBeamAngle66h(nomDirRacine, FormatVersion, Head1, MaxDesMaxNbBeams);
    if ~flag
        SimradAll_rename_all(nomFic, 0)
        useCacheNetcdf = previous_useCacheNetcdf;
        return
    end
end

nomFicXml = fullfile(nomDirRacine, 'ALL_RawRangeBeamAngle46h.xml');
if exist(nomFicXml, 'file')
    flag = all2ssc_RawRangeBeamAngle46h(nomDirRacine, FormatVersion, Head1);
    if ~flag
        SimradAll_rename_all(nomFic, 0)
        useCacheNetcdf = previous_useCacheNetcdf;
        return
    end
end

nomFicXml = fullfile(nomDirRacine, 'ALL_RawRangeBeamAngle4eh.xml');
if exist(nomFicXml, 'file')
    flag = all2ssc_RawRangeBeamAngle4eh(nomDirRacine, FormatVersion, Head1, SounderDualOneTxAntenna);
    if ~flag
        SimradAll_rename_all(nomFic, 0)
        useCacheNetcdf = previous_useCacheNetcdf;
        return
    end
end

%% Datagrammes SeabedImage53h

nomFicXml = fullfile(nomDirRacine, 'ALL_SeabedImage53h.xml');
if exist(nomFicXml, 'file')
    flag = all2ssc_SeabedImage53h(nomDirRacine, FormatVersion, Head1, MaxDesMaxNbBeams, InstallationParameters);
    if ~flag
        SimradAll_rename_all(nomFic, 0)
        useCacheNetcdf = previous_useCacheNetcdf;
        return
    end
end

%% Datagrammes SeabedImage59h

nomFicXml = fullfile(nomDirRacine, 'ALL_SeabedImage59h.xml');
if exist(nomFicXml, 'file')
    flag = all2ssc_SeabedImage59h(nomDirRacine, FormatVersion, Head1, InstallationParameters);
    if ~flag
        SimradAll_rename_all(nomFic, 0)
        useCacheNetcdf = previous_useCacheNetcdf;
        return
    end
end

%% Datagrammes WaterColumn

nomFicXml = fullfile(nomDirRacine, 'ALL_WaterColumn.xml');
if ~exist(nomFicXml, 'file')
    nomFicXml = fullfile(nomDirRacine, 'WCD_WaterColumn.xml');
    if ~exist(nomFicXml, 'file')
        pppp = dir(fullfile(nomDirRacine, 'WCD_*.xml'));
        % Comptage du nombre de fichiers
        % pppp = pppp(find(~cellfun(@isempty,{pppp(:).date})));
        if isempty(pppp)
            nomFicXml = [];
        else
            nomFicXml = fullfile(nomDirRacine, pppp(1).name);
        end
    end
end
if ~isempty(nomFicXml) && exist(nomFicXml, 'file')
    try
        flag = all2ssc_WaterColumn(nomDirRacine, FormatVersion, Head1);
        if ~flag
            % Forge 492 : pb sur le fichier 0633_20091209_052954_Lesuroit.wcd
            % qui ne contient pas de datagramme WCD.
            [nomDirSignal, ~]       = fileparts(nomFicXml);
            [nomDirSsc, nomFicRoot] = fileparts(nomDirSignal);
            [nomDirWcd, ~]          = fileparts(nomDirSsc);
            nomFicWcd = fullfile(nomDirWcd, [nomFicRoot '.wcd']);
            SimradAll_rename_wcd(nomFicWcd, 0);
        end
    catch
        [nomDirSignal, ~]       = fileparts(nomFicXml);
        [nomDirSsc, nomFicRoot] = fileparts(nomDirSignal);
        [nomDirWcd, ~]          = fileparts(nomDirSsc);
        nomFicWcd = fullfile(nomDirWcd, [nomFicRoot '.wcd']);
        SimradAll_rename_wcd(nomFicWcd, 0);
    end
end

%% Datagrammes Heading

nomFicXml = fullfile(nomDirRacine, 'ALL_Heading.xml');
if exist(nomFicXml, 'file')
    flag = all2ssc_Heading(nomDirRacine, FormatVersion);
    if ~flag
        SimradAll_rename_all(nomFic, 0)
        useCacheNetcdf = previous_useCacheNetcdf;
        return
    end
end

%% Datagrammes Height

nomFicXml = fullfile(nomDirRacine, 'ALL_Height.xml');
if exist(nomFicXml, 'file')
    flag = all2ssc_Height(nomDirRacine, FormatVersion); %#ok<NASGU>
end

%% Datagrammes QualityFactor

nomFicXml = fullfile(nomDirRacine, 'ALL_QualityFactor.xml');
if exist(nomFicXml, 'file')
    % TODO : bug fichier D:\Nautilus\EM302\Data\Backscatter\Controle\0018_20130425_214217_Nautilus.all
    flag = all2ssc_QualityFactor(nomDirRacine, FormatVersion, Head1);
    if ~flag
        SimradAll_rename_all(nomFic, 0)
        useCacheNetcdf = previous_useCacheNetcdf;
        return
    end
end

%% Datagrammes QualityFactor - QF From Ifremer

nomFicXml = fullfile(nomDirRacine, 'ALL_QualityFactorFromIfr.xml');
if exist(nomFicXml, 'file')
    flag = all2ssc_QualityFactorFromIfr(nomDirRacine, FormatVersion, Head1);
    if ~flag
        SimradAll_rename_all(nomFic, 0)
        useCacheNetcdf = previous_useCacheNetcdf;
        return
    end
end

%% Datagrammes Raw Beam Samples Datagram

nomFicXml = fullfile(nomDirRacine, 'ALL_RawBeamSamples.xml');
if exist(nomFicXml, 'file')
    flag = all2ssc_RawBeamSamples(nomDirRacine, FormatVersion, Head1);
    if ~flag
        SimradAll_rename_all(nomFic, 0)
        useCacheNetcdf = previous_useCacheNetcdf;
        return
    end
end

%% Datagrammes Stave Data Datagram

nomFicXml = fullfile(nomDirRacine, 'ALL_StaveData.xml');
if exist(nomFicXml, 'file')
    flag = all2ssc_StaveData(nomDirRacine, FormatVersion, Head1);
    if ~flag
        SimradAll_rename_all(nomFic, 0)
        useCacheNetcdf = previous_useCacheNetcdf;
        return
    end
end

%% Datagrammes ExtraParameters

nomFicXml = fullfile(nomDirRacine, 'ALL_ExtraParameters.xml');
if exist(nomFicXml, 'file')
    flag = all2ssc_ExtraParameters(nomDirRacine, FormatVersion);
    if ~flag
        SimradAll_rename_all(nomFic, 0)
        useCacheNetcdf = previous_useCacheNetcdf;
        return
    end
end

%% Cr�ation du layer de r�flectivit� � partir des snippets

aKM = cl_simrad_all('nomFic', nomFic);

[flag, DataDepth] = read_depth_bin(aKM, 'Memmapfile', 0); % Modif JMA le 15/09/2015 pour permettre l'�criture dans ALL_AbsorpCoeff_RT. Je sais, c'est pas terrible
if ~flag
    SimradAll_rename_all(nomFic, 0)
    useCacheNetcdf = previous_useCacheNetcdf;
    return
end

[~, DataDepth, DataRaw, DataSeabed] = computeReflectivityByBeam(aKM, DataDepth, 'IdentAlgoSnippets2OneValuePerBeam', IdentAlgoSnippets2OneValuePerBeam);

%% Cr�ation du layer AbsorptionCoeffKM

% Cr�ation du layer AbsorptionCoeffRT
flag = ALL_AbsorpCoeff_RT(aKM, 'DataDepth', DataDepth, 'DataRaw', DataRaw, 'DataSeabed', DataSeabed); %#ok<NASGU>

% Cr�ation du layer AbsorptionCoeffSSc
flag = ALL_AbsorpCoeff_SSP(aKM, 'DataDepth', DataDepth); %#ok<NASGU>

%% Reaffectation du mode Deeper de l'EM304

modifyModeDeepFMInModeDeeperForEM304(aKM, 'DataRaw', DataRaw);

%% Ce fichier contient-il des donn�es RTK ?

flagRTK = ALL_isRTKAvailable(nomFic); %, 0);
EmModel = DataDepth.EmModel;
SystemSerialNumber = unique(DataDepth.SystemSerialNumber(:));
[Version, SousVersion] = getDatagramVersion(nomFic);

%% Gestion du cache

useCacheNetcdf = previous_useCacheNetcdf;
switch useCacheNetcdf
    case 1 % 'Create XML-Bin only'
    case 2 % 'Create Netcdf and keep XML-Bin'
        ALL_Cache2Netcdf(nomFic, 'deleteXMLBin', 0, 'useParallel', useParallel);
    case 3 % 'Create Netcdf and delete XML-Bin'
        clear DataDepth DataRaw DataSeabed
        ALL_Cache2Netcdf(nomFic, 'deleteXMLBin', 1, 'useParallel', useParallel);
end

%% The End

flag = 1;



function [Version, SousVersion] = getDatagramVersion(nomFic)

SousVersion = [];

flag = existCacheNetcdf(nomFic);
if flag
    [flag, ncFileName] = existCacheNetcdf(nomFic);
    if flag
        [flag, Version] = NetcdfUtils.readGlobalAtt(ncFileName, 'DatagramVersion');
        if ~flag
            return
        end
        if strcmp(Version, 'V1')
            [flag, SousVersion] = NetcdfUtils.readGlobalAtt(ncFileName, 'DatagramSousVersion');
            if ~flag
                return
            end
        end
    end
else
    [nomDir, nomFicSeul] = fileparts(nomFic);
    nomDirRacine = fullfile(nomDir, 'SonarScope', nomFicSeul);
    if exist(fullfile(nomDirRacine, 'ALL_RawRangeBeamAngle66h'), 'dir')
        Version     = 'V1';
        SousVersion = 'f';
    elseif exist(fullfile(nomDirRacine, 'ALL_RawRangeBeamAngle46h'), 'dir')
        Version     = 'V1';
        SousVersion = 'F';
    elseif exist(fullfile(nomDirRacine, 'ALL_RawRangeBeamAngle4eh'), 'dir')
        Version     = 'V2';
        SousVersion = '';
    else
        % On force la version � V1. Cas rencontr� avec fichier NIWA 0005_20080703_224347_raw.all
        Version = 'V1';
        SousVersion = 'F';
    end
end