function flag = all2ssc_Attitude(nomDirRacine, FormatVersion)

nomFicXml = fullfile(nomDirRacine, 'ALL_Attitude.xml');
SonarFmt2ssc_message(nomFicXml, '.all')

%% Lecture du fichier XML d�crivant la donn�e

flag = exist(nomFicXml, 'file');
if ~flag
    return
end
% edit(nomFicXml)
Datagrams = xml_read(nomFicXml);

[nomDirSignal, nomFicSignal] = fileparts(nomFicXml);
nomDirSignal = fullfile(nomDirSignal, nomFicSignal);

[flag, Data.PingCounter] = Read_BinVariable(Datagrams, nomDirSignal, 'PingCounter', 'double');
if ~flag
    return
end
Data.PingCounter = unwrapPingCounterSimrad(Data.PingCounter);
% figure; plot(Data.PingCounter); grid on; title('PingCounter');

[flag, Data.NbCycles] = Read_BinVariable(Datagrams, nomDirSignal, 'NbCycles', 'double');
if ~flag
    return
end
nbSamples = sum(Data.NbCycles);
% figure; plot(Data.NbCycles); grid on; title('Data.NbCycles');

[flag, Data.SensorSystemDescriptor] = Read_BinVariable(Datagrams, nomDirSignal, 'SensorSystemDescriptor', 'uint8');
if ~flag
    return
end

[flag, Date] = Read_BinVariable(Datagrams, nomDirSignal, 'Date', 'double');
if ~flag
    return
end
% figure; plot(Date); grid on; title('Date');

[flag, NbMilliSecs] = Read_BinVariable(Datagrams, nomDirSignal, 'NbMilliSecs', 'double');
if ~flag
    return
end
% figure; plot(NbMilliSecs); grid on; title('NbMilliSecs');

%% Lecture de TS

[flag, TS] = Read_BinTable(Datagrams, nomDirSignal, 'TS', 'single', nbSamples);
if ~flag
    return
end
% figure; plot(TS); grid on; title('TS')

%% Lecture du Sensor Status

[flag, Data.SensorStatus] = Read_BinTable(Datagrams, nomDirSignal, 'SensorStatus', 'single', nbSamples);
if ~flag
    return
end
% figure; plot(Data.SensorStatus); grid on; title('SensorStatus')

%% Lecture du Roulis

[flag, Data.Roll] = Read_BinTable(Datagrams, nomDirSignal, 'Roll', 'single', nbSamples);
if ~flag
    return
end
% figure; plot(Data.Roll); grid on; title('Roll')

%% Lecture du Pitch

[flag, Data.Pitch] = Read_BinTable(Datagrams, nomDirSignal, 'Pitch', 'single', nbSamples);
if ~flag
    return
end
% figure; plot(Data.Pitch); grid on; title('Pitch')

%% Lecture du Heave

[flag, Data.Heave] = Read_BinTable(Datagrams, nomDirSignal, 'Heave', 'single', nbSamples);
if ~flag
    return
end
% figure; plot(Data.Heave); grid on; title('Heave')

%% Lecture du Heave

[flag, Data.Heading] = Read_BinTable(Datagrams, nomDirSignal, 'Heading', 'single', nbSamples);
if ~flag
    return
end
% figure; plot(Data.Heading); grid on; title('Heading')

SensorSystemDescriptor = Data.SensorSystemDescriptor(:);
listSensors = unique(SensorSystemDescriptor);
subIndexSensor = [];
for k=1:length(Data.PingCounter)
    idxSensor = find(listSensors == SensorSystemDescriptor(k));
    subIndexSensor = [subIndexSensor;repmat(idxSensor, Data.NbCycles(k), 1)]; %#ok<AGROW>
end
Data.IndexSensorSystemDescriptor = subIndexSensor;
% figure; plot(Data.IndexSensorSystemDescriptor); grid on; title('Heading')

%% Extraction des valeurs pour le capteur actif
%{
SensorSystemDescriptor = Data.SensorSystemDescriptor(:);
nbSensors = length(listSensors);
if nbSensors > 1
    indSystemUsed = 1;
    subDatagrams = find(SensorSystemDescriptor == listSensors(indSystemUsed)); % TODO : extraire le sensor utilis�. Comment ???
    % TODO : voir aussi read_attitude_bin
    
    indDeb = 1;
    for k=1:length(Data.PingCounter)
        indFin = indDeb + Data.NbCycles(k) - 1;
        subSamples{k} = indDeb:indFin; %#ok<AGROW>
        indDeb = indFin + 1;
    end
    
    subSamples = subSamples(subDatagrams);
    subSamples = [subSamples{:}];
    nbSamples  = length(subSamples);
    
    Datagrams.NbDatagrams       = length(subDatagrams);
    Data.PingCounter            = Data.PingCounter(subDatagrams);
    Data.NbCycles               = Data.NbCycles(subDatagrams);
    Data.SensorSystemDescriptor = Data.SensorSystemDescriptor(subDatagrams);
    Date                        = Date(subDatagrams);
    NbMilliSecs                 = NbMilliSecs(subDatagrams);
    
    Data.SensorStatus           = Data.SensorStatus(subSamples);
    Data.Roll                   = Data.Roll(subSamples);
    Data.Pitch                  = Data.Pitch(subSamples);
    Data.Heave                  = Data.Heave(subSamples);
    Data.Heading                = Data.Heading(subSamples);
    TS                          = TS(subSamples);
end
%}
%% Calcul des dates et heures

Jour  = mod(Date,100);
Date  = (Date - Jour) / 100;
Mois  = mod(Date,100);
Annee = (Date - Mois) / 100;
Date = dayJma2Ifr(Jour, Mois, Annee);

D = zeros(1,nbSamples);
H = zeros(1,nbSamples);
pos = 0;
for k=1:Datagrams.NbDatagrams
    sub = pos+1:pos+Data.NbCycles(k);
    D(sub) = Date(k);
    H(sub) = NbMilliSecs(k);
    pos = pos + Data.NbCycles(k);
end
Time = cl_time('timeIfr', D, H);
Data.Time = Time + double(TS');

%% G�n�ration du XML SonarScope

Info.Title                   = 'Attitude'; % Motion Reference Unit
Info.Constructor             = 'Kongsberg';
Info.EmModel                 = Datagrams.Model;
Info.SystemSerialNumber      = Datagrams.SystemSerialNumber;
Info.TimeOrigin              = '01/01/-4713';
Info.Comments                = 'External Sensor Attitude';
Info.FormatVersion           = FormatVersion;

Info.Dimensions.NbSamples    = nbSamples;
Info.Dimensions.NbDatagrams  = Datagrams.NbDatagrams;

Info.Signals(1).Name         = 'PingCounter';
Info.Signals(end).Dimensions = 'NbDatagrams, 1';
Info.Signals(end).Storage    = 'uint16';
Info.Signals(end).Unit       = '';
Info.Signals(end).FileName   = fullfile('Ssc_Attitude','PingCounter.bin');
Info.Signals(end).Tag        = verifKeyWord('SounderPingCounter');

Info.Signals(end+1).Name     = 'NbCycles';
Info.Signals(end).Dimensions = 'NbDatagrams, 1';
Info.Signals(end).Storage    = 'uint16';
Info.Signals(end).Unit       = '';
Info.Signals(end).FileName   = fullfile('Ssc_Attitude','NbCycles.bin');
Info.Signals(end).Tag        = verifKeyWord('NbCycles');

Info.Signals(end+1).Name     = 'SensorSystemDescriptor';
Info.Signals(end).Dimensions = 'NbDatagrams, 1';
Info.Signals(end).Storage    = 'uint8';
Info.Signals(end).Unit       = '';
Info.Signals(end).FileName   = fullfile('Ssc_Attitude','SensorSystemDescriptor.bin');
Info.Signals(end).Tag        = verifKeyWord('SensorSystemDecriptor');
Info.Signals(end+1).Name     = 'Time';
Info.Signals(end).Dimensions = 'NbSamples, 1';
Info.Signals(end).Index      = 'NbDatagrams, NbCycles';
Info.Signals(end).Storage    = 'double';
Info.Signals(end).Unit       = 'days since JC';
Info.Signals(end).FileName   = fullfile('Ssc_Attitude','Time.bin');
Info.Signals(end).Tag        = verifKeyWord('MotionSensorTime');

Info.Signals(end+1).Name     = 'SensorStatus';
Info.Signals(end).Dimensions = 'NbSamples, 1';
Info.Signals(end).Index      = 'NbDatagrams, NbCycles';
Info.Signals(end).Storage    = 'uint16';
Info.Signals(end).Unit       = '';
Info.Signals(end).FileName   = fullfile('Ssc_Attitude', 'SensorStatus.bin');
Info.Signals(end).Tag        = verifKeyWord('MotionSensorStatus');

Info.Signals(end+1).Name     = 'IndexSensorSystemDescriptor';
Info.Signals(end).Dimensions = 'NbSamples, 1';
Info.Signals(end).Index      = 'NbDatagrams, NbCycles';
Info.Signals(end).Storage    = 'uint8';
Info.Signals(end).Unit       = '';
Info.Signals(end).FileName   = fullfile('Ssc_Attitude','IndexSensorSystemDescriptor.bin');
Info.Signals(end).Tag        = verifKeyWord('IndexSensorSystemDescriptor');

Info.Signals(end+1).Name     = 'Roll';
Info.Signals(end).Dimensions = 'NbSamples, 1';
Info.Signals(end).Index      = 'NbDatagrams, NbCycles';
Info.Signals(end).Storage    = 'single';
Info.Signals(end).Unit       = 'deg';
Info.Signals(end).FileName   = fullfile('Ssc_Attitude','Roll.bin');
Info.Signals(end).Tag        = verifKeyWord('MotionSensorRoll');

Info.Signals(end+1).Name     = 'Pitch';
Info.Signals(end).Dimensions = 'NbSamples, 1';
Info.Signals(end).Index      = 'NbDatagrams, NbCycles';
Info.Signals(end).Storage    = 'single';
Info.Signals(end).Unit       = 'deg';
Info.Signals(end).FileName   = fullfile('Ssc_Attitude','Pitch.bin');
Info.Signals(end).Tag        = verifKeyWord('MotionSensorPitch');

Info.Signals(end+1).Name     = 'Heave';
Info.Signals(end).Dimensions = 'NbSamples, 1';
Info.Signals(end).Index      = 'NbDatagrams, NbCycles';
Info.Signals(end).Storage    = 'single';
Info.Signals(end).Unit       = 'm';
Info.Signals(end).FileName   = fullfile('Ssc_Attitude','Heave.bin');
Info.Signals(end).Tag        = verifKeyWord('MotionSensorHeave');

Info.Signals(end+1).Name     = 'Heading';
Info.Signals(end).Dimensions = 'NbSamples, 1';
Info.Signals(end).Index      = 'NbDatagrams, NbCycles';
Info.Signals(end).Storage    = 'single';
Info.Signals(end).Unit       = 'deg';
Info.Signals(end).FileName   = fullfile('Ssc_Attitude','Heading.bin');
Info.Signals(end).Tag        = verifKeyWord('MotionSensorHeading');

% Ajout des champs Scale_Factor, Add_Offset et Orig_Storage.
[~, Info] = addScaleOffsetStorage(Datagrams, Info);

%% Cr�ation du fichier XML d�crivant la donn�e

nomFicXml = fullfile(nomDirRacine, 'Ssc_Attitude.xml');
xml_write(nomFicXml, Info);
flag = exist(nomFicXml, 'file');
if ~flag
    messageErreur(nomFicXml)
    return
end

%% Cr�ation du r�pertoire Attitude

nomDirAttitude = fullfile(nomDirRacine, 'Ssc_Attitude');
if ~exist(nomDirAttitude, 'dir')
    status = mkdir(nomDirAttitude);
    if ~status
        messageErreur(nomDirAttitude)
        flag = 0;
        return
    end
end

%% Cr�ation des fichiers binaires des signaux

for k=1:length(Info.Signals)
    flag = writeSignal(nomDirRacine, Info.Signals(k), Data.(Info.Signals(k).Name));
    if ~flag
        return
    end
end

flag = 1;
