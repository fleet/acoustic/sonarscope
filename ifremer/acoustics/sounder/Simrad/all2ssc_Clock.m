function flag = all2ssc_Clock(nomDirRacine, FormatVersion)

%% Lecture du fichier XML d�crivant la donn�e

nomFicXml = fullfile(nomDirRacine, 'ALL_Clock.xml');
flag = exist(nomFicXml, 'file');
if ~flag
    return
end
% edit(nomFicXml)
Datagrams = xml_read(nomFicXml);

[nomDirSignal, nomFicSignal] = fileparts(nomFicXml);
nomDirSignal = fullfile(nomDirSignal, nomFicSignal);

%{
for k=1:length(Datagrams.Variables)
fprintf(1,'%d  %s\n', k, Datagrams.Variables(k).Name)
end
%}

nbSamples = Datagrams.NbDatagrams;

%% Lecture de Date

[flag, Date] = Read_BinVariable(Datagrams, nomDirSignal, 'Date', 'double');
if ~flag
    return
end
% figure; plot(Date); grid on; title('Date');

%% Lecture de NbMilliSecs

[flag, NbMilliSecs] = Read_BinVariable(Datagrams, nomDirSignal, 'NbMilliSecs', 'double');
if ~flag
    return
end
% figure; plot(NbMilliSecs); grid on; title('NbMilliSecs');

%% Lecture du PingCounter

[flag, Data.PingCounter] = Read_BinVariable(Datagrams, nomDirSignal, 'PingCounter', 'single');
if ~flag
    return
end
Data.PingCounter = unwrapPingCounterSimrad(Data.PingCounter);
% figure; plot(Data.PingCounter); grid on; title('PingCounter')

%% Lecture de Date

[flag, DateExternal] = Read_BinVariable(Datagrams, nomDirSignal, 'DateExternal', 'double');
if ~flag
    return
end
% figure; plot(DateExternal); grid on; title('DateExternal');

%% Lecture de NbMilliSecs

[flag, NbMilliSecsExternal] = Read_BinVariable(Datagrams, nomDirSignal, 'NbMilliSecsExternal', 'double');
if ~flag
    return
end
% figure; plot(NbMilliSecsExternal); grid on; title('NbMilliSecsExternal');

%% Lecture de PPS

[flag, Data.PPS] = Read_BinVariable(Datagrams, nomDirSignal, 'PPS', 'uint8');
if ~flag
    return
end
% figure; plot(Data.PPS); grid on; title('PPS');

%% Calcul des dates et heures

Jour  = mod(Date,100);
Date  = (Date - Jour) / 100;
Mois  = mod(Date,100);
Annee = (Date - Mois) / 100;
Date = dayJma2Ifr(Jour, Mois, Annee);
Data.Time = cl_time('timeIfr', Date, NbMilliSecs);

JourExternal  = mod(DateExternal,100);
DateExternal  = (DateExternal - JourExternal) / 100;
MoisExternal  = mod(DateExternal,100);
AnneeExternal = (DateExternal - MoisExternal) / 100;
DateExternal = dayJma2Ifr(JourExternal, MoisExternal, AnneeExternal);
Data.ExternalTime = cl_time('timeIfr', DateExternal, NbMilliSecsExternal);

%% G�n�ration du XML SonarScope

Info.Title                  = 'Clock';
Info.Constructor            = 'Kongsberg';
Info.EmModel                = Datagrams.Model;
Info.SystemSerialNumber     = Datagrams.SystemSerialNumber;
Info.TimeOrigin             = '01/01/-4713';
Info.Comments               = 'Sounder ping rate';
Info.FormatVersion          = FormatVersion;

Info.Dimensions.NbSamples     = nbSamples;

Info.Signals(1).Name          = 'Time';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = 'days since JC';
Info.Signals(end).FileName    = fullfile('Ssc_Clock','Time.bin');
Info.Signals(end).Tag         = verifKeyWord('SounderTime');

Info.Signals(end+1).Name      = 'PingCounter';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_Clock','PingCounter.bin');
Info.Signals(end).Tag         = verifKeyWord('SounderPingCounter');

Info.Signals(end+1).Name      = 'ExternalTime';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = 'days since JC';
Info.Signals(end).FileName    = fullfile('Ssc_Clock','ExternalTime.bin');
Info.Signals(end).Tag         = verifKeyWord('ExternalTime');

Info.Signals(end+1).Name      = 'PPS';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'uint8';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_Clock','PPS.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

% Ajout des champs Scale_Factor, Add_Offset et Orig_Storage.
[flag, Info] = addScaleOffsetStorage(Datagrams, Info);

%% Cr�ation du fichier XML d�crivant la donn�e

nomFicXml = fullfile(nomDirRacine, 'Ssc_Clock.xml');
xml_write(nomFicXml, Info);
flag = exist(nomFicXml, 'file');
if ~flag
    messageErreur(nomFicXml)
    return
end

%% Cr�ation du r�pertoire Clock

nomDirClock = fullfile(nomDirRacine, 'Ssc_Clock');
if ~exist(nomDirClock, 'dir')
    status = mkdir(nomDirClock);
    if ~status
        messageErreur(nomDirClock)
        flag = 0;
        return
    end
end

%% Cr�ation des fichiers binaires des signaux
for k=1:length(Info.Signals)
    flag = writeSignal(nomDirRacine, Info.Signals(k), Data.(Info.Signals(k).Name));
    if ~flag
        return
    end
end

flag = 1;

