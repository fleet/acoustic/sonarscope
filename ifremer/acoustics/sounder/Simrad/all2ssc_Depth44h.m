function [flag, MaxDesMaxNbBeams] = all2ssc_Depth44h(nomDirRacine, FormatVersion, Head1, nomFicAll)
% Traitement du datagramme Multibeam data.

global IdentAlgoSnippets2OneValuePerBeam %#ok<GVMIS>

if isempty(IdentAlgoSnippets2OneValuePerBeam)
    IdentAlgoSnippets2OneValuePerBeam = 2;
end

MaxDesMaxNbBeams = [];

nomFicXml = fullfile(nomDirRacine, 'ALL_Depth44h.xml');
SonarFmt2ssc_message(nomFicXml, '.all')

%% Lecture du fichier XML d�crivant la donn�e 

flag = exist(nomFicXml, 'file');
if ~flag
    special_message('FichierNonExistant', nomFicXml)
    return
end

% edit(nomFicXml)
Datagrams = xml_read(nomFicXml);

%% Controle si il y a assez de pings

if Datagrams.NbDatagrams < 2
    SimradAll_rename_all(nomFicAll, Datagrams.NbDatagrams)
    flag = 0;
    return
end

[nomDirSignal, nomFicSignal] = fileparts(nomFicXml);
nomDirSignal = fullfile(nomDirSignal, nomFicSignal);

%{
for k=1:length(Datagrams.Variables)
    fprintf(1,'%d  %s\n', k, Datagrams.Variables(k).Name)
end
for k=1:length(Datagrams.Tables)
    fprintf(1,'%d  %s\n', k, Datagrams.Tables(k).Name)
end
%}

%% Transcodage de signaux

[flag, Date1] = Read_BinVariable(Datagrams, nomDirSignal, 'Date', 'double');
if ~flag
    return
end
% figure; plot(Date1); grid on; title('Date');

[flag, NbMilliSecs1] = Read_BinVariable(Datagrams, nomDirSignal, 'NbMilliSecs', 'double');
if ~flag
    return
end
% figure; plot(NbMilliSecs1); grid on; title('NbMilliSecs');

[flag, PingCounter] = Read_BinVariable(Datagrams, nomDirSignal, 'PingCounter', 'single');
if ~flag
    return
end
PingCounter = unwrapPingCounterSimrad(PingCounter);
% figure; plot(PingCounter); grid on; title('PingCounter');

[flag, SystemSerialNumber] = Read_BinVariable(Datagrams, nomDirSignal, 'SystemSerialNumber', 'single');
if ~flag
    return
end
% figure; plot(SystemSerialNumber, '+'); grid on; title('SystemSerialNumber');

[flag, Heading] = Read_BinVariable(Datagrams, nomDirSignal, 'Heading', 'single');
if ~flag
    return
end
% figure; plot(Heading); grid on; title('Heading');

[flag, SoundSpeed] = Read_BinVariable(Datagrams, nomDirSignal, 'SoundSpeed', 'single');
if ~flag
    return
end
% figure; plot(SoundSpeed); grid on; title('SoundSpeed');

[flag, TransducerDepth] = Read_BinVariable(Datagrams, nomDirSignal, 'TransducerDepth', 'single');
if ~flag
    return
end


[flag, TransducerDepthOffsetMult] = Read_BinVariable(Datagrams, nomDirSignal, 'TransducerDepthOffsetMult', 'single');
if ~flag
    return
end
% figure; plot(TransducerDepthOffsetMult); grid on; title('TransducerDepthOffsetMult');


% figure; plot(TransducerDepth); grid on; title('TransducerDepth');
TransducerDepth = TransducerDepth + 655.36 * single(TransducerDepthOffsetMult);


[flag, MaxNbBeamsPossible] = Read_BinVariable(Datagrams, nomDirSignal, 'MaxNbBeamsPossible', 'single');
if ~flag
    return
end
% figure; plot(MaxNbBeamsPossible); grid on; title('MaxNbBeamsPossible');

[flag, NbBeams] = Read_BinVariable(Datagrams, nomDirSignal, 'NbBeams', 'single');
if ~flag
    return
end
% figure; plot(NbBeams); grid on; title('NbBeams');

[flag, ZResol] = Read_BinVariable(Datagrams, nomDirSignal, 'ZResol', 'single');
if ~flag
    return
end
% figure; plot(ZResol); grid on; title('ZResol');

[flag, XYResol] = Read_BinVariable(Datagrams, nomDirSignal, 'XYResol', 'single');
if ~flag
    return
end
% figure; plot(XYResol); grid on; title('XYResol');

[flag, SamplingRate] = Read_BinVariable(Datagrams, nomDirSignal, 'SamplingRate', 'single');
if ~flag
    return
end
% figure; plot(SamplingRate, '+'); grid on; title('SamplingRate');


nbSamples = sum(NbBeams);
[flag, BeamNumber] = Read_BinTable(Datagrams, nomDirSignal, 'BeamNumber', 'single', nbSamples);
if ~flag
    return
end
% figure; plot(BeamNumber, '+'); grid on; title('BeamNumber');

Date                    = Kongsberg_table2signal(Date1,              Datagrams.NbDatagrams, PingCounter, SystemSerialNumber, Head1);
NbMilliSecs             = Kongsberg_table2signal(NbMilliSecs1,       Datagrams.NbDatagrams, PingCounter, SystemSerialNumber, Head1);
Data.PingCounter        = Kongsberg_table2signal(PingCounter,        Datagrams.NbDatagrams, PingCounter, SystemSerialNumber, Head1);
Data.SystemSerialNumber = Kongsberg_table2signal(SystemSerialNumber, Datagrams.NbDatagrams, PingCounter, SystemSerialNumber, Head1);
Data.Heading            = Kongsberg_table2signal(Heading,            Datagrams.NbDatagrams, PingCounter, SystemSerialNumber, Head1);
Data.SoundSpeed         = Kongsberg_table2signal(SoundSpeed,         Datagrams.NbDatagrams, PingCounter, SystemSerialNumber, Head1);
Data.TransducerDepth    = Kongsberg_table2signal(TransducerDepth,    Datagrams.NbDatagrams, PingCounter, SystemSerialNumber, Head1);
Data.MaxNbBeamsPossible = Kongsberg_table2signal(MaxNbBeamsPossible, Datagrams.NbDatagrams, PingCounter, SystemSerialNumber, Head1);
Data.NbBeams            = Kongsberg_table2signal(NbBeams,            Datagrams.NbDatagrams, PingCounter, SystemSerialNumber, Head1);
Data.XYResol            = Kongsberg_table2signal(XYResol,            Datagrams.NbDatagrams, PingCounter, SystemSerialNumber, Head1);
Data.ZResol             = Kongsberg_table2signal(ZResol,             Datagrams.NbDatagrams, PingCounter, SystemSerialNumber, Head1);
Data.SamplingRate       = Kongsberg_table2signal(SamplingRate,       Datagrams.NbDatagrams, PingCounter, SystemSerialNumber, Head1);
Data.TransducerDepthOffsetMult = Kongsberg_table2signal(TransducerDepthOffsetMult,    Datagrams.NbDatagrams, PingCounter, SystemSerialNumber, Head1);

% D�but rajout JMA le 20/09/2016 avec mise en commentaire Kongsberg_table2signal
% subNaN = find(isnan(sum(Data.PingCounter,2)));
subNaN = find(all(isnan(Data.PingCounter),2));  % Modif JMA le 14/03/2019 pour fichier 0021_20190306_040731_Belgica.all
Date(subNaN,:)                    = [];
NbMilliSecs(subNaN,:)             = [];
Data.PingCounter(subNaN,:)        = [];
Data.SystemSerialNumber(subNaN,:) = [];
Data.Heading(subNaN,:)            = [];
Data.SoundSpeed(subNaN,:)         = [];
Data.TransducerDepth(subNaN,:)    = [];
Data.MaxNbBeamsPossible(subNaN,:) = [];
Data.NbBeams(subNaN,:)            = [];
Data.XYResol(subNaN,:)            = [];
Data.ZResol(subNaN,:)             = [];
Data.SamplingRate(subNaN,:)       = [];
Data.TransducerDepthOffsetMult(subNaN,:) = [];
% Fin rajout JMA le 20/09/2016 avec mise en commentaire Kongsberg_table2signal


switch Datagrams.Model
    case 3003
        Data.SamplingRate(:,1) = 13956;
%         Data.SamplingRate(:,2) = 14621;
    case 3004
        Data.SamplingRate(:,1) = 14293;
%         Data.SamplingRate(:,2) = 14621;
    case 3005
        Data.SamplingRate(:,1) = 13956;
%         Data.SamplingRate(:,2) = 14293;
    case 3006
        Data.SamplingRate(:,1) = 14621;
%         Data.SamplingRate(:,2) = 14293;
    case 3007
        Data.SamplingRate(:,1) = 14293;
%         Data.SamplingRate(:,2) = 13956;
    case 3008
        Data.SamplingRate(:,1) = 14621;
%         Data.SamplingRate(:,2) = 13956;
end

% Correct PingCounter in case of dual sounder but only the starboard is on
% (correspondances between pings are calculated on indice=1 on second
% dimension
Data.PingCounter     = repairPingCounterInCaseDualSounderButOnlyStarnoardRunning(Data.PingCounter);
Data.TransducerDepth = repairPingCounterInCaseDualSounderButOnlyStarnoardRunning(Data.TransducerDepth);
Data.SamplingRate    = repairPingCounterInCaseDualSounderButOnlyStarnoardRunning(Data.SamplingRate);
Data.Heading         = repairPingCounterInCaseDualSounderButOnlyStarnoardRunning(Data.Heading);
Data.SoundSpeed      = repairPingCounterInCaseDualSounderButOnlyStarnoardRunning(Data.SoundSpeed);

MaxDesMaxNbBeams = max(MaxNbBeamsPossible);

Data.BeamNumber = Kongsberg_table2image(BeamNumber, 'BeamNumber', Datagrams.NbDatagrams, MaxDesMaxNbBeams, NbBeams, BeamNumber, PingCounter, SystemSerialNumber, Head1);
Data.BeamNumber(subNaN,:) = [];

%% Depth

[flag, X] = Read_BinTable(Datagrams, nomDirSignal, 'Depth', 'single', nbSamples);
if ~flag
    return
end
Data.Depth = Kongsberg_table2image(X, 'Depth', Datagrams.NbDatagrams, MaxDesMaxNbBeams, NbBeams, BeamNumber, PingCounter, SystemSerialNumber, Head1);
Data.Depth(subNaN,:) = [];
Data.Depth(Data.Depth == 0) = NaN; % Pour prendre en compte les flags de MB_System

if size(Data.XYResol,2) == 1
    for k=1:length(Data.XYResol)
        Data.Depth(k,:) = Data.Depth(k,:) * Data.ZResol(k);
    end
else
    nbColumns = size(Data.Depth,2);
    ns2 = nbColumns / 2;
    subBab = 1:ns2;
    subTri = ns2+1:nbColumns;
    for k=1:size(Data.XYResol,1)
        Data.Depth(k,subBab) = Data.Depth(k,subBab) * Data.ZResol(k,1);
        Data.Depth(k,subTri) = Data.Depth(k,subTri) * Data.ZResol(k,2) + (Data.TransducerDepth(k,1) - Data.TransducerDepth(k,2));
    end
end
% figure; imagesc(Data.Depth); title('Depth'); axis xy; colorbar;

%% AcrossDist

[flag, X] = Read_BinTable(Datagrams, nomDirSignal, 'AcrossDist', 'single', nbSamples);
if ~flag
    return
end
Data.AcrossDist = Kongsberg_table2image(X, 'AcrossDist', Datagrams.NbDatagrams, MaxDesMaxNbBeams, NbBeams, BeamNumber, PingCounter, SystemSerialNumber, Head1);
Data.AcrossDist(subNaN,:) = [];
if size(Data.XYResol,2) == 1
    for k=1:length(Data.XYResol)
        Data.AcrossDist(k,:) = Data.AcrossDist(k,:) * Data.XYResol(k);
    end
else
    nbColumns = size(Data.AcrossDist,2);
    ns2 = nbColumns / 2;
    subBab = 1:ns2;
    subTri = ns2+1:nbColumns;
    for k=1:size(Data.XYResol,1)
        Data.AcrossDist(k,subBab) = Data.AcrossDist(k,subBab) * Data.XYResol(k,1);
        Data.AcrossDist(k,subTri) = Data.AcrossDist(k,subTri) * Data.XYResol(k,2);
    end
end
% figure; imagesc(Data.AcrossDist); title('AcrossDist'); axis xy; colorbar;

%% AlongDist

[flag, X] = Read_BinTable(Datagrams, nomDirSignal, 'AlongDist', 'single', nbSamples);
if ~flag
    return
end
Data.AlongDist = Kongsberg_table2image(X, 'AlongDist', Datagrams.NbDatagrams, MaxDesMaxNbBeams, NbBeams, BeamNumber, PingCounter, SystemSerialNumber, Head1);
Data.AlongDist(subNaN,:) = [];
if size(Data.XYResol,2) == 1
    for k=1:length(Data.XYResol)
        Data.AlongDist(k,:) = Data.AlongDist(k,:) * Data.XYResol(k);
    end
else
    nbColumns = size(Data.AlongDist,2);
    ns2 = nbColumns / 2;
    subBab = 1:ns2;
    subTri = ns2+1:nbColumns;
    for k=1:size(Data.XYResol,1)
        Data.AlongDist(k,subBab) = Data.AlongDist(k,subBab) * Data.XYResol(k,1);
        Data.AlongDist(k,subTri) = Data.AlongDist(k,subTri) * Data.XYResol(k,2);
    end
end
% figure; imagesc(Data.AlongDist); title('AlongDist'); axis xy; colorbar;

%% BeamDepressionAngle

[flag, X] = Read_BinTable(Datagrams, nomDirSignal, 'BeamDepressionAngle', 'single', nbSamples);
if ~flag
    return
end
Data.BeamDepressionAngle = Kongsberg_table2image(X, 'BeamDepressionAngle', Datagrams.NbDatagrams, MaxDesMaxNbBeams, NbBeams, BeamNumber, PingCounter, SystemSerialNumber, Head1);
Data.BeamDepressionAngle(subNaN,:) = [];
% sub = (Data.AcrossDist <= 0);
% Data.BeamDepressionAngle(sub) = Data.BeamDepressionAngle(sub) - 90;
% Data.BeamDepressionAngle(~sub) = 90 - Data.BeamDepressionAngle(~sub);
% figure; imagesc(Data.BeamDepressionAngle); title('BeamDepressionAngle'); axis xy; colorbar;

%% BeamAzimuthAngle

[flag, X] = Read_BinTable(Datagrams, nomDirSignal, 'BeamAzimuthAngle', 'single', nbSamples);
if ~flag
    return
end
Data.BeamAzimuthAngle = Kongsberg_table2image(X, 'BeamAzimuthAngle', Datagrams.NbDatagrams, MaxDesMaxNbBeams, NbBeams, BeamNumber, PingCounter, SystemSerialNumber, Head1);
Data.BeamAzimuthAngle(subNaN,:) = [];
% figure; imagesc(Data.BeamAzimuthAngle); title('BeamAzimuthAngle'); axis xy; colorbar;

%% Range

[flag, X] = Read_BinTable(Datagrams, nomDirSignal, 'Range', 'single', nbSamples);
if ~flag
    return
end
Data.Range = Kongsberg_table2image(X, 'Range', Datagrams.NbDatagrams, MaxDesMaxNbBeams, NbBeams, BeamNumber, PingCounter, SystemSerialNumber, Head1);
Data.Range(subNaN,:) = [];
% figure; imagesc(Data.Range); title('Range'); axis xy; colorbar;

%% QualityFactor

[flag, X] = Read_BinTable(Datagrams, nomDirSignal, 'QualityFactor', 'single', nbSamples);
if ~flag
    return
end
Data.QualityFactor = Kongsberg_table2image(X, 'QualityFactor', Datagrams.NbDatagrams, MaxDesMaxNbBeams, NbBeams, BeamNumber, PingCounter, SystemSerialNumber, Head1);
Data.QualityFactor(subNaN,:) = [];
% figure; imagesc(Data.QualityFactor); title('QualityFactor'); axis xy; colorbar;

%% LengthDetection

[flag, X] = Read_BinTable(Datagrams, nomDirSignal, 'LengthDetection', 'single', nbSamples);
if ~flag
    return
end
Data.LengthDetection = Kongsberg_table2image(X, 'LengthDetection', Datagrams.NbDatagrams, MaxDesMaxNbBeams, NbBeams, BeamNumber, PingCounter, SystemSerialNumber, Head1);
Data.LengthDetection(subNaN,:) = [];
% figure; imagesc(Data.LengthDetection); title('LengthDetection'); axis xy; colorbar;

%% Reflectivity

[flag, X] = Read_BinTable(Datagrams, nomDirSignal, 'Reflectivity', 'single', nbSamples);
if ~flag
    return
end
Data.Reflectivity = Kongsberg_table2image(X, 'Reflectivity', Datagrams.NbDatagrams, MaxDesMaxNbBeams, NbBeams, BeamNumber, PingCounter, SystemSerialNumber, Head1);
Data.Reflectivity(subNaN,:) = [];
% figure; imagesc(Data.Reflectivity); title('Reflectivity'); axis xy; colorbar;

%% ReflectivityFromSnippets

sz = size(Data.Reflectivity);
Data.ReflectivityFromSnippets = NaN(sz, 'single');
Data.ReflectivityFromSnippetsWithoutSpecularRestablishment = NaN(sz, 'single');

%% Coefficient d'att�nuation Kongsberg et SSc

Data.AbsorptionCoefficientRT   = NaN(sz, 'single');
Data.AbsorptionCoefficientSSc  = NaN(sz, 'single');
Data.AbsorptionCoefficientUser = NaN(sz, 'single');
Data.IncidenceAngle            = NaN(sz, 'single');
Data.SlopeAcross               = NaN(sz, 'single');
Data.SlopeAlong                = NaN(sz, 'single');
Data.BathymetryFromDTM         = NaN(sz, 'single');
Data.InsonifiedAreaSSc         = NaN(sz, 'single');

%% Masque

% Data.Mask = repmat(false, sz);
Data.Mask = false(sz);

%% Modification de BeamDepressionAngle

if isempty(Head1.SystemSerialNumberSecondHead) || isempty(Head1.SystemSerialNumber) || ...
        (Head1.SystemSerialNumberSecondHead == 0) || (Head1.SystemSerialNumber == 0) || ...
        (Head1.SystemSerialNumberSecondHead ==  Head1.SystemSerialNumber)
    sub = (Data.AcrossDist <= 0);
    Data.BeamDepressionAngle(sub) = Data.BeamDepressionAngle(sub) - 90;
    Data.BeamDepressionAngle(~sub) = 90 - Data.BeamDepressionAngle(~sub);
else
    % A VERIFIER POUR DIFFERENTS SONDEURS DUALS
    nbBeams = size(Data.AcrossDist,2);
    nbBeamsSur2 = floor(nbBeams / 2);
    subBab = 1:nbBeamsSur2;
    subTri = (nbBeamsSur2+1):nbBeams;
    for k=1:size(Data.AcrossDist,1)
        sub = (diff(Data.BeamDepressionAngle(k,subBab)) >= 0);
        sub(end+1) = sub(end); %#ok<AGROW>
        subsub = subBab(sub);
        if ~isempty(subsub)
            sub3 = 1:subsub(end);
            Data.BeamDepressionAngle(k,sub3) = Data.BeamDepressionAngle(k,sub3) - 90;
            
            sub3 = (subsub(end)+1):nbBeamsSur2;
            Data.BeamDepressionAngle(k,sub3) = 90 - Data.BeamDepressionAngle(k,sub3);
        end
        
        sub = (diff(Data.BeamDepressionAngle(k,subTri)) <= 0);
        sub(end+1) = sub(end); %#ok<AGROW>
        subsub = subTri(sub);
        if ~isempty(subsub)
            sub3 = subsub(1):nbBeams;
            Data.BeamDepressionAngle(k,sub3) = 90 - Data.BeamDepressionAngle(k,sub3);
            
            sub3 = (nbBeamsSur2+1):(subsub(1)-1);
            Data.BeamDepressionAngle(k,sub3) = Data.BeamDepressionAngle(k,sub3) -90;
        end
    end
end
% figure; imagesc(Data.BeamDepressionAngle); title('BeamDepressionAngle'); axis xy; colorbar;

%% Calcul des dates et heures

Jour  = mod(Date,100);
Date  = (Date - Jour) / 100;
Mois  = mod(Date,100);
Annee = (Date - Mois) / 100;
Date = dayJma2Ifr(Jour, Mois, Annee);
Data.Time = cl_time('timeIfr', Date, NbMilliSecs);

[nbPings, nbBeams] = size(Data.Depth);
Data.MaskPingsReflectivity = zeros(nbPings, 1, 'single');
Data.MaskPingsBathy        = zeros(nbPings, 1, 'single');

%% G�n�ration du XML SonarScope

Info.Title                  = 'Depth';
Info.Constructor            = 'Kongsberg';
Info.EmModel                = Datagrams.Model;
Info.ListeSystemSerialNumber = num2str(unique(Data.SystemSerialNumber(:))');
Info.TimeOrigin             = '01/01/-4713';
Info.Comments               = 'Sounder ping rate';
Info.Version                = 'V1';
Info.FormatVersion          = FormatVersion;
Info.IdentAlgoSnippets2OneValuePerBeam = IdentAlgoSnippets2OneValuePerBeam;

Info.Dimensions.nbPings     = nbPings;
Info.Dimensions.nbBeams     = nbBeams;
Info.Dimensions.nbSounders  = size(Data.PingCounter,2);

Info.Signals(1).Name          = 'Time';
Info.Signals(end).Dimensions  = 'nbPings, nbSounders';
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = 'days since JC';
Info.Signals(end).FileName    = fullfile('Ssc_Depth', 'Time.bin');
Info.Signals(end).Tag         = verifKeyWord('SounderTime');

Info.Signals(end+1).Name      = 'PingCounter';
Info.Signals(end).Dimensions  = 'nbPings, nbSounders';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_Depth', 'PingCounter.bin');
Info.Signals(end).Tag         = verifKeyWord('SounderPingCounter');

Info.Signals(end+1).Name      = 'SystemSerialNumber';
Info.Signals(end).Dimensions  = 'nbPings, nbSounders';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_Depth', 'SystemSerialNumber.bin');
Info.Signals(end).Tag         = verifKeyWord('SounderSerialNumber');

Info.Signals(end+1).Name      = 'Heading';
Info.Signals(end).Dimensions  = 'nbPings, nbSounders';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile('Ssc_Depth', 'Heading.bin');
Info.Signals(end).Tag         = verifKeyWord('SounderHeading');

Info.Signals(end+1).Name      = 'SoundSpeed';
Info.Signals(end).Dimensions  = 'nbPings, nbSounders';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'm/s';
Info.Signals(end).FileName    = fullfile('Ssc_Depth', 'SoundSpeed.bin');
Info.Signals(end).Tag         = verifKeyWord('SounderSoundSpeed');

Info.Signals(end+1).Name      = 'TransducerDepth';
Info.Signals(end).Dimensions  = 'nbPings, nbSounders';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'm';
Info.Signals(end).FileName    = fullfile('Ssc_Depth', 'TransducerDepth.bin');
Info.Signals(end).Tag         = verifKeyWord('SounderTransducerDepth');

Info.Signals(end+1).Name      = 'MaxNbBeamsPossible';
Info.Signals(end).Dimensions  = 'nbPings, nbSounders';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_Depth', 'MaxNbBeamsPossible.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'NbBeams';
Info.Signals(end).Dimensions  = 'nbPings, nbSounders';
Info.Signals(end).Storage     = 'uint8';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_Depth', 'NbBeams.bin');
Info.Signals(end).Tag         = verifKeyWord('SounderNbBeams');
     
Info.Signals(end+1).Name      = 'ZResol';
Info.Signals(end).Dimensions  = 'nbPings, nbSounders';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_Depth', 'ZResol.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'XYResol';
Info.Signals(end).Dimensions  = 'nbPings, nbSounders';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_Depth', 'XYResol.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'SamplingRate';
Info.Signals(end).Dimensions  = 'nbPings, nbSounders';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'Hz';
Info.Signals(end).FileName    = fullfile('Ssc_Depth', 'SamplingRate.bin');
Info.Signals(end).Tag         = verifKeyWord('SounderSamplingRate');

Info.Signals(end+1).Name      = 'TransducerDepthOffsetMult';
Info.Signals(end).Dimensions  = 'nbPings, nbSounders';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_Depth', 'TransducerDepthOffsetMult.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'MaskPingsBathy';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_Depth', 'MaskPingsBathy.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'MaskPingsReflectivity';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_Depth', 'MaskPingsReflectivity.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Images(1).Name           = 'Depth';
Info.Images(end).Dimensions   = 'nbPings, nbBeams';
Info.Images(end).Index        = 'nbPings, nbBeams';
Info.Images(end).Storage      = 'single';
Info.Images(end).Unit         = 'm';
Info.Images(end).FileName     = fullfile('Ssc_Depth', 'Depth.bin');
Info.Images(end).Tag          = verifKeyWord('SounderDepth');

Info.Images(end+1).Name       = 'AcrossDist';
Info.Images(end).Dimensions   = 'nbPings, nbBeams';
Info.Images(end).Index        = 'nbPings, nbBeams';
Info.Images(end).Storage      = 'single';
Info.Images(end).Unit         = 'm';
Info.Images(end).FileName     = fullfile('Ssc_Depth', 'AcrossDist.bin');
Info.Images(end).Tag          = verifKeyWord('SounderAcrossDist');

Info.Images(end+1).Name       = 'AlongDist';
Info.Images(end).Dimensions   = 'nbPings, nbBeams';
Info.Images(end).Index        = 'nbPings, nbBeams';
Info.Images(end).Storage      = 'single';
Info.Images(end).Unit         = 'm';
Info.Images(end).FileName     = fullfile('Ssc_Depth', 'AlongDist.bin');
Info.Images(end).Tag          = verifKeyWord('SounderAlongDist');

Info.Images(end+1).Name       = 'BeamDepressionAngle';
Info.Images(end).Dimensions   = 'nbPings, nbBeams';
Info.Images(end).Index        = 'nbPings, nbBeams';
Info.Images(end).Storage      = 'single';
Info.Images(end).Unit         = 'deg';
Info.Images(end).FileName     = fullfile('Ssc_Depth', 'BeamDepressionAngle.bin');
Info.Images(end).Tag          = verifKeyWord('SounderBeamAngle');

Info.Images(end+1).Name       = 'BeamAzimuthAngle';
Info.Images(end).Dimensions   = 'nbPings, nbBeams';
Info.Images(end).Index        = 'nbPings, nbBeams';
Info.Images(end).Storage      = 'single';
Info.Images(end).Unit         = 'deg';
Info.Images(end).FileName     = fullfile('Ssc_Depth', 'BeamAzimuthAngle.bin');
Info.Images(end).Tag          = verifKeyWord('SounderAzimuthAngle');

Info.Images(end+1).Name       = 'Range';
Info.Images(end).Dimensions   = 'nbPings, nbBeams';
Info.Images(end).Index        = 'nbPings, nbBeams';
Info.Images(end).Storage      = 'single';
Info.Images(end).Unit         = 'samples';
Info.Images(end).FileName     = fullfile('Ssc_Depth', 'Range.bin');
Info.Images(end).Tag          = verifKeyWord('SounderDetectionNbSamples');

Info.Images(end+1).Name       = 'QualityFactor';
Info.Images(end).Dimensions   = 'nbPings, nbBeams';
Info.Images(end).Index        = 'nbPings, nbBeams';
Info.Images(end).Storage      = 'single';
Info.Images(end).Unit         = '';
Info.Images(end).FileName     = fullfile('Ssc_Depth', 'QualityFactor.bin');
Info.Images(end).Tag          = verifKeyWord('SounderSimradQF');

Info.Images(end+1).Name       = 'LengthDetection';
Info.Images(end).Dimensions   = 'nbPings, nbBeams';
Info.Images(end).Index        = 'nbPings, nbBeams';
Info.Images(end).Storage      = 'single';
Info.Images(end).Unit         = 'ms';
Info.Images(end).FileName     = fullfile('Ssc_Depth', 'LengthDetection.bin');
Info.Images(end).Tag          = verifKeyWord('SounderSimradLengthDetection');

Info.Images(end+1).Name       = 'Reflectivity';
Info.Images(end).Dimensions   = 'nbPings, nbBeams';
Info.Images(end).Index        = 'nbPings, nbBeams';
Info.Images(end).Storage      = 'single';
Info.Images(end).Unit         = 'dB';
Info.Images(end).FileName     = fullfile('Ssc_Depth', 'Reflectivity.bin');
Info.Images(end).Tag          = verifKeyWord('SounderReflectivity');

Info.Images(end+1).Name       = 'ReflectivityFromSnippets';
Info.Images(end).Dimensions   = 'nbPings, nbBeams';
Info.Images(end).Index        = 'nbPings, nbBeams';
Info.Images(end).Storage      = 'single';
Info.Images(end).Unit         = 'dB';
Info.Images(end).FileName     = fullfile('Ssc_Depth', 'ReflectivityFromSnippets.bin');
Info.Images(end).Tag          = verifKeyWord('SounderReflectivity');

Info.Images(end+1).Name       = 'ReflectivityFromSnippetsWithoutSpecularRestablishment';
Info.Images(end).Dimensions   = 'nbPings, nbBeams';
Info.Images(end).Index        = 'nbPings, nbBeams';
Info.Images(end).Storage      = 'single';
Info.Images(end).Unit         = 'dB';
Info.Images(end).FileName     = fullfile('Ssc_Depth', 'ReflectivityFromSnippetsWithoutSpecularRestablishment.bin');
Info.Images(end).Tag          = verifKeyWord('SounderReflectivity');

Info.Images(end+1).Name       = 'AbsorptionCoefficientRT';
Info.Images(end).Dimensions   = 'nbPings, nbBeams';
Info.Images(end).Index        = 'nbPings, nbBeams';
Info.Images(end).Storage      = 'single';
Info.Images(end).Unit         = 'dB/km';
Info.Images(end).FileName     = fullfile('Ssc_Depth', 'AbsorptionCoefficientRT.bin');
Info.Images(end).Tag          = verifKeyWord('TODO');

Info.Images(end+1).Name       = 'AbsorptionCoefficientSSc';
Info.Images(end).Dimensions   = 'nbPings, nbBeams';
Info.Images(end).Index        = 'nbPings, nbBeams';
Info.Images(end).Storage      = 'single';
Info.Images(end).Unit         = 'dB/km';
Info.Images(end).FileName     = fullfile('Ssc_Depth', 'AbsorptionCoefficientSSc.bin');
Info.Images(end).Tag          = verifKeyWord('TODO');

Info.Images(end+1).Name       = 'AbsorptionCoefficientUser';
Info.Images(end).Dimensions   = 'nbPings, nbBeams';
Info.Images(end).Index        = 'nbPings, nbBeams';
Info.Images(end).Storage      = 'single';
Info.Images(end).Unit         = 'dB/km';
Info.Images(end).FileName     = fullfile('Ssc_Depth', 'AbsorptionCoefficientUser.bin');
Info.Images(end).Tag          = verifKeyWord('TODO');
Info.Images(end).Postprocess  = 1;

Info.Images(end+1).Name       = 'BeamNumber';
Info.Images(end).Dimensions   = 'nbPings, nbBeams';
Info.Images(end).Index        = 'nbPings, nbBeams';
Info.Images(end).Storage      = 'single';
Info.Images(end).Unit         = '';
Info.Images(end).FileName     = fullfile('Ssc_Depth', 'BeamNumber.bin');
Info.Images(end).Tag          = verifKeyWord('SounderBeamNumber');

Info.Images(end+1).Name       = 'IncidenceAngle';
Info.Images(end).Dimensions   = 'nbPings, nbBeams';
Info.Images(end).Index        = 'nbPings, nbBeams';
Info.Images(end).Storage      = 'single';
Info.Images(end).Unit         = 'deg';
Info.Images(end).FileName     = fullfile('Ssc_Depth', 'IncidenceAngle.bin');
Info.Images(end).Tag          = verifKeyWord('TODO');
Info.Images(end).Postprocess  = 1;

Info.Images(end+1).Name       = 'ReflectivityBestBS';
Info.Images(end).Dimensions   = 'nbPings, nbBeams';
Info.Images(end).Index        = 'nbPings, nbBeams';
Info.Images(end).Storage      = 'single';
Info.Images(end).Unit         = 'dB';
Info.Images(end).FileName     = fullfile('Ssc_Depth', 'ReflectivityBestBS.bin');
Info.Images(end).Tag          = verifKeyWord('TODO');
Info.Images(end).Postprocess  = 1;

Info.Images(end+1).Name       = 'SlopeAcross';
Info.Images(end).Dimensions   = 'nbPings, nbBeams';
Info.Images(end).Index        = 'nbPings, nbBeams';
Info.Images(end).Storage      = 'single';
Info.Images(end).Unit         = 'deg';
Info.Images(end).FileName     = fullfile('Ssc_Depth', 'SlopeAcross.bin');
Info.Images(end).Tag          = verifKeyWord('TODO');
Info.Images(end).Postprocess  = 1;

Info.Images(end+1).Name       = 'SlopeAlong';
Info.Images(end).Dimensions   = 'nbPings, nbBeams';
Info.Images(end).Index        = 'nbPings, nbBeams';
Info.Images(end).Storage      = 'single';
Info.Images(end).Unit         = 'deg';
Info.Images(end).FileName     = fullfile('Ssc_Depth', 'SlopeAlong.bin');
Info.Images(end).Tag          = verifKeyWord('TODO');
Info.Images(end).Postprocess  = 1;

Info.Images(end+1).Name       = 'BathymetryFromDTM';
Info.Images(end).Dimensions   = 'nbPings, nbBeams';
Info.Images(end).Index        = 'nbPings, nbBeams';
Info.Images(end).Storage      = 'single';
Info.Images(end).Unit         = 'm';
Info.Images(end).FileName     = fullfile('Ssc_Depth', 'BathymetryFromDTM.bin');
Info.Images(end).Tag          = verifKeyWord('TODO');
Info.Images(end).Postprocess  = 1;

Info.Images(end+1).Name       = 'InsonifiedAreaSSc';
Info.Images(end).Dimensions   = 'nbPings, nbBeams';
Info.Images(end).Index        = 'nbPings, nbBeams';
Info.Images(end).Storage      = 'single';
Info.Images(end).Unit         = 'dB';
Info.Images(end).FileName     = fullfile('Ssc_Depth', 'InsonifiedAreaSSc.bin');
Info.Images(end).Tag          = verifKeyWord('TODO');
Info.Images(end).Postprocess  = 1;

Info.Images(end+1).Name       = 'WCSignalWidth';
Info.Images(end).Dimensions   = 'nbPings, nbBeams';
Info.Images(end).Index        = 'nbPings, nbBeams';
Info.Images(end).Storage      = 'single';
Info.Images(end).Unit         = 's';
Info.Images(end).FileName     = fullfile('Ssc_Depth', 'WCSignalWidth.bin');
Info.Images(end).Tag          = verifKeyWord('TODO');
Info.Images(end).Postprocess  = 1;

Info.Images(end+1).Name       = 'Mask';
Info.Images(end).Dimensions   = 'nbPings, nbBeams';
Info.Images(end).Index        = 'nbPings, nbBeams';
Info.Images(end).Storage      = 'uint8';
Info.Images(end).Unit         = '';
Info.Images(end).FileName     = fullfile('Ssc_Depth', 'Mask.bin');
Info.Images(end).Tag          = verifKeyWord('Mask');

% Ajout des champs Scale_Factor, Add_Offset et Orig_Storage.
[flag, Info] = addScaleOffsetStorage(Datagrams, Info);

%% Cr�ation du fichier XML d�crivant la donn�e 

nomFicXml = fullfile(nomDirRacine, 'Ssc_Depth.xml');
xml_write(nomFicXml, Info);
flag = exist(nomFicXml, 'file');
if ~flag
    messageErreur(nomFicXml)
    return
end
% edit(nomFicXml)

%% Cr�ation du r�pertoire Depth

nomDirDepth = fullfile(nomDirRacine, 'Ssc_Depth');
if ~exist(nomDirDepth, 'dir')
    status = mkdir(nomDirDepth);
    if ~status
        messageErreur(nomDirDepth)
        flag = 0;
        return
    end
end

%% Cr�ation des fichiers binaires des signaux

for k=1:length(Info.Signals)
    if XMLBinUtils.isVarPostprocessed(Info.Signals(k))
        continue
    end
    flag = writeSignal(nomDirRacine, Info.Signals(k), Data.(Info.Signals(k).Name));
    if ~flag
        return
    end
end

%% Cr�ation des fichiers binaires des images

for k=1:length(Info.Images)
    if XMLBinUtils.isVarPostprocessed(Info.Images(k))
        continue
    end
    flag = writeImage(nomDirRacine, Info.Images(k), Data.(Info.Images(k).Name));
    if ~flag
        return
    end
end

flag = 1;
