function [flag, BSCorr] = all2ssc_ExtraParameters(nomDirRacine, FormatVersion)

BSCorr = [];

%% Lecture du fichier XML d�crivant la donn�e

nomFicXml = fullfile(nomDirRacine, 'ALL_ExtraParameters.xml');
flag = exist(nomFicXml, 'file');
if ~flag
    special_message('FichierNonExistant', nomFicXml)
    return
end

% edit(nomFicXml)
Datagrams = xml_read(nomFicXml);

[nomDirSignal, nomFicSignal] = fileparts(nomFicXml);
nomDirSignal = fullfile(nomDirSignal, nomFicSignal);

%{
for i=1:length(Datagrams.Variables)
    fprintf(1,'%d  %s\n', i, Datagrams.Variables(i).Name)
end
%}

nbSamples = Datagrams.NbDatagrams;

[flag, Date] = Read_BinVariable(Datagrams, nomDirSignal, 'Date', 'double');
if ~flag
    return
end
% figure; plot(Date); grid on; title('Date');

[flag, NbMilliSecs] = Read_BinVariable(Datagrams, nomDirSignal, 'NbMilliSecs', 'double');
if ~flag
    return
end
% figure; plot(NbMilliSecs); grid on; title('NbMilliSecs');

[flag, Data.PingCounter] = Read_BinVariable(Datagrams, nomDirSignal, 'PingCounter', 'double');
if ~flag
    return
end
Data.PingCounter = unwrapPingCounterSimrad(Data.PingCounter);
% figure; plot(Data.PingCounter); grid on; title('PingCounter');

ContentType = {'Calib'
    'Log all heights'
    'Sound velocity at transducer'
    'Sound velocity profile'
    'Multicast RX status'
    'BScorr'};

[flag, Data.ContentIdentifier] = Read_BinVariable(Datagrams, nomDirSignal, 'ContentIdentifier', 'uint16');
if ~flag
    return
end

% figure; plot(Data.ProcessingUnitStatus); grid on; title('ProcessingUnitStatus');

%% Calcul des dates et heures

Jour  = mod(Date,100);
Date  = (Date - Jour) / 100;
Mois  = mod(Date,100);
Annee = (Date - Mois) / 100;
Date = dayJma2Ifr(Jour, Mois, Annee);

Data.Time = cl_time('timeIfr', Date, NbMilliSecs);

%% G�n�ration du XML SonarScope

Info.Title                  = 'ExtraParameters';
Info.Constructor            = 'Kongsberg';
Info.EmModel                = Datagrams.Model;
Info.SystemSerialNumber     = Datagrams.SystemSerialNumber;
Info.TimeOrigin             = '01/01/-4713';
Info.Comments               = 'Sensor sampling rate';
Info.FormatVersion          = FormatVersion;

Info.Dimensions.NbSamples           = nbSamples;
Info.Dimensions.NbContentIdentifier = numel(Data.ContentIdentifier);

Info.Signals(1).Name          = 'Time';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = 'days since JC';
Info.Signals(end).FileName    = fullfile('Ssc_ExtraParameters','Time.bin');
Info.Signals(end).Tag         = verifKeyWord('SounderTime');

Info.Signals(end+1).Name      = 'PingCounter';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_ExtraParameters','PingCounter.bin');
Info.Signals(end).Tag         = verifKeyWord('SounderPingCounter');

Info.Signals(end+1).Name      = 'ContentIdentifier';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_ExtraParameters','ContentIdentifier.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Strings(1).Name          = 'ListExtraParamsFile';
% Info.Strings(end).Dimensions  = sprintf('%d, 1', numel(Data.ContentIdentifier));
% Info.Strings(end).Dimensions  = sprintf('NbContentIdentifier, %d', numel(Data.ContentIdentifier));
Info.Strings(end).Dimensions  = 'NbContentIdentifier, 1';
Info.Strings(end).Storage     = 'char';
Info.Strings(end).Unit        = '';
Info.Strings(end).FileName    = fullfile('Ssc_ExtraParameters','ListExtraParamsFile.bin');
Info.Strings(end).Tag         = verifKeyWord('TODO');

% Ajout des champs Scale_Factor, Add_Offset et Orig_Storage.
[~, Info] = addScaleOffsetStorage(Datagrams, Info);

%% Cr�ation du fichier XML d�crivant la donn�e

nomFicXml = fullfile(nomDirRacine, 'Ssc_ExtraParameters.xml');
xml_write(nomFicXml, Info);
flag = exist(nomFicXml, 'file');
if ~flag
    messageErreur(nomFicXml)
    return
end

%% Cr�ation du r�pertoire ExtraParameters

nomDirExtraParameters = fullfile(nomDirRacine, 'Ssc_ExtraParameters');
if ~exist(nomDirExtraParameters, 'dir')
    status = mkdir(nomDirExtraParameters);
    if ~status
        messageErreur(nomDirExtraParameters)
        flag = 0;
        return
    end
end

%% Cr�ation des fichiers binaires des signaux

for k=1:length(Info.Signals)
    flag = writeSignal(nomDirRacine, Info.Signals(k), Data.(Info.Signals(k).Name));
    if ~flag
        return
    end
end

%% Lecture des donn�es suppl�mentaires.

for k=1:numel(Data.ContentIdentifier)
    FileNameContent = [ContentType{Data.ContentIdentifier(k)} '_' num2str(Datagrams.Model)  '_' num2str(Datagrams.SystemSerialNumber) '.txt'];
    FileNameSsc = fullfile(nomDirRacine, 'Ssc_ExtraParameters', FileNameContent);
    Data.ListExtraParamsFile{k} = FileNameContent;
    switch Data.ContentIdentifier(k)
        case 6
            BScorrFilename = fullfile(nomDirRacine, 'ALL_ExtraParameters', [ContentType{Data.ContentIdentifier(k)} '.txt']);
            flag = copyfile(BScorrFilename, FileNameSsc);
            if ~flag
                str1 = 'Impossible de recopier les fichiers Extra Parameters. SVP, contactez JMA.';
                str2 = 'Extra Parameters files copying denied. Please contact JMA.';
                my_warndlg(Lang(str1,str2), 1);
                return
            end
            [flag, BSCorr] = read_BSCorr(FileNameSsc);
            if ~flag
                return
            end
            
            % Rajout� le 13/12/2021 pour contourner l'oubli de transfert de
            % cette donn�es dans le Netcdf. Id�alement, il faudrait avoir
            % le contenu du bscorr dans le fichier netcdf.
            nomDirRacineNetcdf = fileparts(nomDirRacine);
            FileNameSsc = fullfile(nomDirRacineNetcdf, FileNameContent);
            flag = copyfile(BScorrFilename, FileNameSsc);
            if ~flag
                str1 = 'Impossible de recopier les fichiers Extra Parameters. SVP, contactez JMA.';
                str2 = 'Extra Parameters files copying denied. Please contact JMA.';
                my_warndlg(Lang(str1,str2), 1);
                return
            end
        otherwise
%             if ~isdeployed
%                 str1 = [ContentType{Data.ContentIdentifier(k)} ' : Donn�es Extra Parameters non d�cod�s pour l''instant.'];
%                 str2 = [ContentType{Data.ContentIdentifier(k)} ' : Extra Parameters data non interpreted for the moment.'];
%                 my_warndlg(Lang(str1,str2), 0, 'Tag', 'ExtraParamsPasTraite', 'TimeDelay', 300);
%             end
    end
end

%% Cr�ation des fichiers binaires des cha�nes

for k=1:length(Info.Strings)
    flag = writeStrings(nomDirRacine, Info.Strings(k), Data.(Info.Strings(k).Name));
    if ~flag
        return
    end
end
