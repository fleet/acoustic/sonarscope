function flag = all2ssc_Heading(nomDirRacine, FormatVersion)

nomFicXml = fullfile(nomDirRacine, 'ALL_Heading.xml');
SonarFmt2ssc_message(nomFicXml, '.all')

%% Lecture du fichier XML d�crivant la donn�e 

flag = exist(nomFicXml, 'file');
if ~flag
    special_message('FichierNonExistant', nomFicXml)
    return
end
% edit(nomFicXml)
Datagrams = xml_read(nomFicXml);

[nomDirSignal, nomFicSignal] = fileparts(nomFicXml);
nomDirSignal = fullfile(nomDirSignal, nomFicSignal);

[flag, Data.NbEntries] = Read_BinVariable(Datagrams, nomDirSignal, 'NbEntries', 'double');
if ~flag
    return
end
nbSamples = sum(Data.NbEntries);
% figure; plot(NbEntries); grid on; title('NbEntries');

[flag, Date] = Read_BinVariable(Datagrams, nomDirSignal, 'Date', 'double');
if ~flag
    return
end
% figure; plot(Date); grid on; title('Date');

[flag, NbMilliSecs] = Read_BinVariable(Datagrams, nomDirSignal, 'NbMilliSecs', 'double');
if ~flag
    return
end
% figure; plot(NbMilliSecs); grid on; title('NbMilliSecs');

%% Lecture du Sensor Status

[flag, Data.PingCounter] = Read_BinVariable(Datagrams, nomDirSignal, 'PingCounter', 'single');
if ~flag
    return
end
Data.PingCounter = unwrapPingCounterSimrad(Data.PingCounter);
% figure; plot(Data.PingCounter); grid on; title('PingCounter')

%% Lecture du Sensor Status

[flag, Data.SystemSerialNumber] = Read_BinVariable(Datagrams, nomDirSignal, 'SystemSerialNumber', 'single');
if ~flag
    return
end
% figure; plot(Data.SystemSerialNumber); grid on; title('SystemSerialNumber')

%% Lecture de StartNbMilliSecs

[flag, TS] = Read_BinTable(Datagrams, nomDirSignal, 'StartNbMilliSecs', 'single', nbSamples);
if ~flag
    return
end
% figure; plot(TS); grid on; title('StartNbMilliSecs')

%% Lecture du Heading

[flag, Data.Heading] = Read_BinTable(Datagrams, nomDirSignal, 'Heading', 'single', nbSamples);
if ~flag
    return
end
% figure; plot(Data.Heading); grid on; title('Heading')


%% Calcul des dates et heures

Jour  = mod(Date,100);
Date  = (Date - Jour) / 100;
Mois  = mod(Date,100);
Annee = (Date - Mois) / 100;
Date = dayJma2Ifr(Jour, Mois, Annee);

D = zeros(1,nbSamples);
H = zeros(1,nbSamples);
pos = 0;
for i=1:Datagrams.NbDatagrams
    sub = pos+1:pos+Data.NbEntries(i);
    D(sub) = Date(i);
    H(sub) = NbMilliSecs(i);
    pos = pos + Data.NbEntries(i);
end
Time = cl_time('timeIfr', D, H);
Data.Time = Time + double(TS');

%% G�n�ration du XML SonarScope

Info.Title                  = 'Heading';
Info.Constructor            = 'Kongsberg';
Info.EmModel                  = Datagrams.Model;
Info.SystemSerialNumber     = Datagrams.SystemSerialNumber;
% Info.NbSamples              = nbSamples;
Info.TimeOrigin             = '01/01/-4713';
Info.Comments               = 'Sensor sampling rate';
Info.FormatVersion          = FormatVersion;

Info.Dimensions.NbSamples    = nbSamples;
Info.Dimensions.NbDatagrams   = Datagrams.NbDatagrams;

Info.Signals(1).Name      = 'PingCounter';
Info.Signals(end).Dimensions  = 'NbDatagrams, 1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_Heading','PingCounter.bin');
Info.Signals(end).Tag         = verifKeyWord('SounderPingCounter');

Info.Signals(end+1).Name      = 'NbEntries';
Info.Signals(end).Dimensions  = 'NbDatagrams, 1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_Heading','NbEntries.bin');
Info.Signals(end).Tag         = verifKeyWord('NbCycles');

Info.Signals(end+1).Name      = 'Time';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Index       = 'NbDatagrams, NbEntries';
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = 'days since JC';
Info.Signals(end).FileName    = fullfile('Ssc_Heading','Time.bin');
Info.Signals(end).Tag         = verifKeyWord('MotionSensorTime');

Info.Signals(end+1).Name      = 'Heading';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Index       = 'NbDatagrams, NbEntries';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile('Ssc_Heading','Heading.bin');
Info.Signals(end).Tag         = verifKeyWord('MotionSensorHeading');

% Ajout des champs Scale_Factor, Add_Offset et Orig_Storage.
[flag, Info] = addScaleOffsetStorage(Datagrams, Info);

%% Cr�ation du fichier XML d�crivant la donn�e 

nomFicXml = fullfile(nomDirRacine, 'Ssc_Heading.xml');
xml_write(nomFicXml, Info);
flag = exist(nomFicXml, 'file');
if ~flag
    messageErreur(nomFicXml)
    return
end

%% Cr�ation du r�pertoire Heading

nomDirHeading = fullfile(nomDirRacine, 'Ssc_Heading');
if ~exist(nomDirHeading, 'dir')
    status = mkdir(nomDirHeading);
    if ~status
        messageErreur(nomDirHeading)
        flag = 0;
        return
    end
end

%% Cr�ation des fichiers binaires des signaux

for i=1:length(Info.Signals)
    flag = writeSignal(nomDirRacine, Info.Signals(i), Data.(Info.Signals(i).Name));
    if ~flag
        return
    end
end

flag = 1;
