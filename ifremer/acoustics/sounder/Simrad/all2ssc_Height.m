function flag = all2ssc_Height(nomDirRacine, FormatVersion)

nomFicXml = fullfile(nomDirRacine, 'ALL_Height.xml');
SonarFmt2ssc_message(nomFicXml, '.all')

%% Lecture du fichier XML d�crivant la donn�e 

flag = exist(nomFicXml, 'file');
if ~flag
    special_message('FichierNonExistant', nomFicXml)
    return
end
% edit(nomFicXml)
Datagrams = xml_read(nomFicXml);
% gen_object_display(Datagrams)

[nomDirSignal, nomFicSignal] = fileparts(nomFicXml);
nomDirSignal = fullfile(nomDirSignal, nomFicSignal);

[flag, Date] = Read_BinVariable(Datagrams, nomDirSignal, 'Date', 'double');
if ~flag
    return
end
% figure; plot(Date); grid on; title('Date');

[flag, NbMilliSecs] = Read_BinVariable(Datagrams, nomDirSignal, 'NbMilliSecs', 'double');
if ~flag
    return
end
% figure; plot(NbMilliSecs); grid on; title('NbMilliSecs');

%% Lecture du PingCounter

[flag, Data.PingCounter] = Read_BinVariable(Datagrams, nomDirSignal, 'PingCounter', 'single');
if ~flag
    return
end
Data.PingCounter = unwrapPingCounterSimrad(Data.PingCounter);
% figure; plot(Data.PingCounter); grid on; title('PingCounter')

%% Lecture du SystemSerialNumber

[flag, Data.SystemSerialNumber] = Read_BinVariable(Datagrams, nomDirSignal, 'SystemSerialNumber', 'single');
if ~flag
    return
end
% figure; plot(Data.SystemSerialNumber); grid on; title('SystemSerialNumber')

%% Lecture du Height

[flag, Data.Height] = Read_BinVariable(Datagrams, nomDirSignal, 'Height', 'single');
if ~flag
    return
end
% figure; plot(Data.SystemSerialNumber); grid on; title('SystemSerialNumber')

%% Lecture du HeightType

[flag, Data.HeightType] = Read_BinVariable(Datagrams, nomDirSignal, 'HeightType', 'uint8');
if ~flag
    return
end
% figure; plot(Data.SystemSerialNumber); grid on; title('SystemSerialNumber')

%% Signal Tide

Data.Tide = NaN(size(Data.Height), 'single');

%% Calcul des dates et heures

Jour  = mod(Date,100);
Date  = (Date - Jour) / 100;
Mois  = mod(Date,100);
Annee = (Date - Mois) / 100;
Date = dayJma2Ifr(Jour, Mois, Annee);
Data.Time = cl_time('timeIfr', Date, NbMilliSecs);

nbSamples = Datagrams.NbDatagrams;

%% G�n�ration du XML SonarScope

Info.Title              = 'Height';
Info.Constructor        = 'Kongsberg';
Info.EmModel            = Datagrams.Model;
Info.SystemSerialNumber = Datagrams.SystemSerialNumber;
Info.NbSamples          = nbSamples;
Info.TimeOrigin         = '01/01/-4713';
Info.Comments           = 'Sensor sampling rate';
Info.FormatVersion      = FormatVersion;

Info.Dimensions.NbSamples = nbSamples;

Info.Signals(1).Name          = 'Time';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = 'days since JC';
Info.Signals(end).FileName    = fullfile('Ssc_Height','Time.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'PingCounter';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_Height','PingCounter.bin');
Info.Signals(end).Tag         = verifKeyWord('SounderPingCounter');

Info.Signals(end+1).Name      = 'Height';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile('Ssc_Height','Height.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'HeightType';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'uint8';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_Height','HeightType.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'Tide';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_Height','Tide.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

% Ajout des champs Scale_Factor, Add_Offset et Orig_Storage.
[flag, Info] = addScaleOffsetStorage(Datagrams, Info);

%% Cr�ation du fichier XML d�crivant la donn�e 

nomFicXml = fullfile(nomDirRacine, 'Ssc_Height.xml');
xml_write(nomFicXml, Info);
flag = exist(nomFicXml, 'file');
if ~flag
    messageErreur(nomFicXml)
    return
end

%% Cr�ation du r�pertoire Height

nomDirHeight = fullfile(nomDirRacine, 'Ssc_Height');
if exist(nomDirHeight, 'dir')
    [status, message, messageid] = rmdir(nomDirHeight, 's');
end
status = mkdir(nomDirHeight);
if ~status
    messageErreur(nomDirHeight)
    flag = 0;
    return
end

%% Cr�ation des fichiers binaires des signaux

for k=1:length(Info.Signals)
    flag = writeSignal(nomDirRacine, Info.Signals(k), Data.(Info.Signals(k).Name));
    if ~flag
        return
    end
end

flag = 1;
