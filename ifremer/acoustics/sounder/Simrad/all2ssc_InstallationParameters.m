function flag = all2ssc_InstallationParameters(nomDirRacine, FormatVersion)

% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Pas s�r que ce soit la bonne structuration au cas o� il y aurait plusieurs enregistrements
% Ex fichier C:\Temp\UNH\2009data\SonarScope\0007_20090403_015755_ShipName\ALL_InstallParams.xml
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

nomFicXml = fullfile(nomDirRacine, 'ALL_InstallParams.xml');
SonarFmt2ssc_message(nomFicXml, '.all')

% [~,nomFic] = fileparts(nomDirSignal);
% nomFic = [nomFic '.all'];

%% Lecture du fichier XML d�crivant la donn�e 

flag = exist(nomFicXml, 'file');
if ~flag
% Comment� � cause des fichiers hydromap
%     special_message('FichierNonExistant', nomFicXml)
    return
end
% edit(nomFicXml)
Datagrams = xml_read(nomFicXml);

% if ~isdeployed && (Datagrams.NbDatagrams > 1)
%     nomDir = fileparts(nomFicXml);
%     str2 = sprintf('Please send this .all file to sonarscope@ifremer.fr becaus something that has never been found exists in this file (all2ssc_InstallationParameters) : \n%s', nomDir);
%     my_warndlg(str2, 0);
% end

%{
for i=1:length(Datagrams.Variables)
    fprintf(1,'%d  %s\n', i, Datagrams.Variables(i).Name)
end
for i=1:length(Datagrams.Tables)
    fprintf(1,'%d  %s\n', i, Datagrams.Tables(i).Name)
end
%}

[nomDirSignal, nomFicSignal] = fileparts(nomFicXml);
nomDirSignal = fullfile(nomDirSignal, nomFicSignal);

[flag, NbOfBytesInDatagram] = Read_BinVariable(Datagrams, nomDirSignal, 'NbOfBytesInDatagram', 'single'); %#ok<ASGLU>
if ~flag
    return
end
% figure; plot(NbOfBytesInDatagram); grid on; title('NbOfBytesInDatagram');

[flag, Date] = Read_BinVariable(Datagrams, nomDirSignal, 'Date', 'double');
if ~flag
    return
end
% figure; plot(Date); grid on; title('Date');

[flag, NbMilliSecs] = Read_BinVariable(Datagrams, nomDirSignal, 'NbMilliSecs', 'double');
if ~flag
    return
end
% figure; plot(NbMilliSecs); grid on; title('NbMilliSecs');

%% Lecture de SurveyLineNumber

[flag, Data.SurveyLineNumber] = Read_BinVariable(Datagrams, nomDirSignal, 'SurveyLineNumber', 'single');
if ~flag
    return
end
% figure; plot(Data.SurveyLineNumber); grid on; title('SurveyLineNumber')

%% Lecture de SerialNumberOfSecondSonarHead

[flag, SerialNumberOfSecondSonarHead] = Read_BinVariable(Datagrams, nomDirSignal, 'SerialNumberOfSecondSonarHead', 'single'); 
if ~flag
    return
end
% figure; plot(SerialNumberOfSecondSonarHead); grid on; title('SerialNumberOfSecondSonarHead')

indLine = findIndVariable(Datagrams.Tables, 'Line');
if isfield(Datagrams.Tables(indLine), 'Constant')
    Line = Datagrams.Tables(indLine).Constant;
    S = decodeEnr(Line);
else
%     Datagrams.Tables(indLine).FileName
    nomFicBin = fullfile(nomDirRacine, 'ALL_InstallParams', Datagrams.Tables(indLine).FileName);
    flag = exist(nomFicBin, 'file');
    if ~flag
        special_message('FichierNonExistant', nomFicBin)
        return
    end
    S = [];
    fid = fopen(nomFicBin, 'r');
    while 1
        Line = fgetl(fid);
        if ~ischar(Line)
            break
        end
        if isempty(Line)
            continue
        end
        pppp = decodeEnr(Line);
        % Recopie des �l�ments complets dans la structure S.
        % Permet d'�viter les lignes mal interpr�t�s en fin de fichier
        % Line.Bin (cas des fichiers EVH08_Boite_909_a.all et
        % EVH08_Boite_909_b.all)
        if ~isempty(pppp)
            S = pppp;
            break
        end
%         if ~isempty(pppp)
%             listeFields = fieldnames(pppp);
%             for k=1:length(listeFields)
%                 S.(listeFields{k}) = pppp.(listeFields{k});
%             end
%         end
    end
    fclose(fid);
end
if isempty(S)
    flag = 0;
    return
end

%{
nbChar = Datagrams.Tables(indLine).NbElem;
[flag, Line] = Read_BinTable(Datagrams, nomDirSignal, 'Line', 'char', nbChar);
[flag, Line] = Read_BinTable(Datagrams, nomDirSignal, 'Line', 'char', []);
if ~flag
    return
end
%}

%% Calcul des dates et heures

Jour  = mod(Date,100);
Date  = (Date - Jour) / 100;
Mois  = mod(Date,100);
Annee = (Date - Mois) / 100;
Date = dayJma2Ifr(Jour, Mois, Annee);

Data.Time = cl_time('timeIfr', Date, NbMilliSecs);

%% G�n�ration du XML SonarScope

Info.Title                          = 'InstallationParameters';
Info.Constructor                    = 'Kongsberg';
Info.EmModel                        = Datagrams.Model;
Info.SystemSerialNumber             = Datagrams.SystemSerialNumber;
Info.SystemSerialNumberSecondHead   = SerialNumberOfSecondSonarHead(1);
Info.TimeOrigin                     = '01/01/-4713';
Info.Comments                       = 'One per file';
Info.FormatVersion                  = FormatVersion;

Info.Dimensions.NbDatagrams     = Datagrams.NbDatagrams;

Info.Signals(1).Name          = 'Time';
Info.Signals(end).Dimensions  = 'NbDatagrams, 1';
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = 'days since JC';
Info.Signals(end).FileName    = fullfile('Ssc_InstallationParameters','Time.bin');
Info.Signals(end).Tag         = verifKeyWord('SounderTime');

Info.Signals(end+1).Name      = 'SurveyLineNumber';
Info.Signals(end).Dimensions  = 'NbDatagrams, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_InstallationParameters','SurveyLineNumber.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

% Ajout des champs Scale_Factor, Add_Offset et Orig_Storage.
[~, Info] = addScaleOffsetStorage(Datagrams, Info);

% Recopie de la structure InstallParams dans Info.Line
Info.Line = S;
clear S

%% Cr�ation du fichier XML d�crivant la donn�e 

nomFicXml = fullfile(nomDirRacine, 'Ssc_InstallationParameters.xml');
xml_write(nomFicXml, Info);
flag = exist(nomFicXml, 'file');
if ~flag
    messageErreur(nomFicXml)
    return
end

%% Cr�ation du r�pertoire InstallationParameters

nomDirInstallationParameters = fullfile(nomDirRacine, 'Ssc_InstallationParameters');
if ~exist(nomDirInstallationParameters, 'dir')
    status = mkdir(nomDirInstallationParameters);
    if ~status
        messageErreur(nomDirInstallationParameters)
        flag = 0;
        return
    end
end

%% Cr�ation des fichiers binaires des signaux

for i=1:length(Info.Signals)
    flag = writeSignal(nomDirRacine, Info.Signals(i), Data.(Info.Signals(i).Name));
    if ~flag
        return
    end
end

flag = 1;


function Data = decodeEnr(Line)
% Nv d�codage de la ligne filtrant les �l�ments parasites dans la cha�ne
% Line (ex : Cas des fichiers EVH08_Boite_909_a.all et EVH08_Boite_909_b.all

Data = [];

% S�paration des �l�ments sur la base du sch�ma : "xxx=*"
pppp        = regexp(Line, '\w{3,}=[-a-zA-Z0-9. _]*', 'match');
nameField   = regexp(pppp, '\w{3,}=', 'match');
% Suppression du '=' pour avoir les noms de champs sous forme "xxx"
nameField   = cellfun(@(x) (regexprep(x, '=', '')), nameField);

% S�paration des �l�ments identifi�s sur la base du sch�ma et r�cup�ration de la cha�ne *: "xxx=*"
pppp2       = regexp(pppp, '\w{3,}=', 'split');

for k=1:numel(nameField)
    Value = pppp2{k}{2};
    numVal = str2double(Value);
    if isnan(numVal)
        Data.(nameField{k}) = Value;
    else
        Data.(nameField{k}) = numVal;
    end    
end

