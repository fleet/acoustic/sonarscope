function flag = all2ssc_NetworkAttitude(nomDirRacine, FormatVersion, varargin)

nomFicXml = fullfile(nomDirRacine, 'ALL_NetworkAttitude.xml');
SonarFmt2ssc_message(nomFicXml, '.all')


%% Lecture du fichier XML d�crivant la donn�e 

flag = exist(nomFicXml, 'file');
if ~flag
    return
end
% edit(nomFicXml)
Datagrams = xml_read(nomFicXml);

[nomDirSignal, nomFicSignal] = fileparts(nomFicXml);
nomDirSignal = fullfile(nomDirSignal, nomFicSignal);

[flag, Data.PingCounter] = Read_BinVariable(Datagrams, nomDirSignal, 'PingCounter', 'double');
if ~flag
    return
end
Data.PingCounter = unwrapPingCounterSimrad(Data.PingCounter);
% figure; plot(Data.PingCounter); grid on; title('PingCounter');

[flag, Data.NbEntries] = Read_BinVariable(Datagrams, nomDirSignal, 'NbEntries', 'double');
if ~flag
    return
end
nbSamples = sum(Data.NbEntries);
% figure; plot(Data.NbEntries); grid on; title('Data.NbEntries');

[flag, Data.SensorSystemDescriptor] = Read_BinVariable(Datagrams, nomDirSignal, 'SensorSystemDescriptor', 'uint8');
if ~flag
    return
end

[flag, Date] = Read_BinVariable(Datagrams, nomDirSignal, 'Date', 'double');
if ~flag
    return
end
% figure; plot(Date); grid on; title('Date');

[flag, NbMilliSecs] = Read_BinVariable(Datagrams, nomDirSignal, 'NbMilliSecs', 'double');
if ~flag
    return
end
% figure; plot(NbMilliSecs); grid on; title('NbMilliSecs');

%% Lecture de TS

[flag, TS] = Read_BinTable(Datagrams, nomDirSignal, 'TS', 'single', nbSamples);
if ~flag
    return
end
% figure; plot(TS); grid on; title('TS')

%% Lecture du Roulis

[flag, Data.Roll] = Read_BinTable(Datagrams, nomDirSignal, 'Roll', 'single', nbSamples);
if ~flag
    return
end
% figure; plot(Data.Roll); grid on; title('Roll')

%% Lecture du Pitch

[flag, Data.Pitch] = Read_BinTable(Datagrams, nomDirSignal, 'Pitch', 'single', nbSamples);
if ~flag
    return
end
% figure; plot(Data.Pitch); grid on; title('Pitch')

%% Lecture du Heave

[flag, Data.Heave] = Read_BinTable(Datagrams, nomDirSignal, 'Heave', 'single', nbSamples);
if ~flag
    return
end
% figure; plot(Data.Heave); grid on; title('Heave')

%% Lecture du Heave

[flag, Data.Heading] = Read_BinTable(Datagrams, nomDirSignal, 'Heading', 'single', nbSamples);
if ~flag
    return
end
% figure; plot(Data.Heading); grid on; title('Heading')

%% Lecture du Nb Bytes in input Datagrams

[flag, Data.NbBytes] = Read_BinTable(Datagrams, nomDirSignal, 'NbBytes', 'single', nbSamples);
if ~flag
    return
end
% figure; plot(Data.NbBytes); grid on; title('NbBytes')

% ------------------------
% Lecture du NetWork Attitude = chaine de char de NbBytes car * NbSamples
% % N = sum(Data.NbBytes);
% % [flag, NetworkAttitude] = Read_BinTable(Datagrams, nomDirSignal, 'NetworkAttitude', 'char', N);
% % maxNbBytes = max(Data.NbBytes);
% % Data.NetworkAttitude = reshape(NetworkAttitude, maxNbBytes, Datagrams.NbDatagrams);
% % if ~flag
% %     return
% % end

%% Calcul des dates et heures

Jour  = mod(Date,100);
Date  = (Date - Jour) / 100;
Mois  = mod(Date,100);
Annee = (Date - Mois) / 100;
Date = dayJma2Ifr(Jour, Mois, Annee);

D = zeros(1,nbSamples);
H = zeros(1,nbSamples);
pos = 0;
for i=1:Datagrams.NbDatagrams
    sub = pos+1:pos+Data.NbEntries(i);
    D(sub) = Date(i);
    H(sub) = NbMilliSecs(i);
    pos = pos + Data.NbEntries(i);
end
Time = cl_time('timeIfr', D, H);
Data.Time = Time + double(TS');

%% G�n�ration du XML SonarScope

Info.Title                  = 'NetworkAttitude'; % Motion Reference Unit
Info.Constructor            = 'Kongsberg';
Info.EmModel                = Datagrams.Model;
Info.SystemSerialNumber     = Datagrams.SystemSerialNumber;
Info.TimeOrigin             = '01/01/-4713';
Info.Comments               = 'Network Attitude velocity datagram 110';
Info.FormatVersion          = FormatVersion;

Info.Dimensions.NbSamples     = nbSamples;
Info.Dimensions.NbDatagrams   = Datagrams.NbDatagrams;

Info.Signals(1).Name          = 'PingCounter';
Info.Signals(end).Dimensions  = 'NbDatagrams, 1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_NetworkAttitude','PingCounter.bin');
Info.Signals(end).Tag         = verifKeyWord('SounderPingCounter');

Info.Signals(end+1).Name      = 'NbEntries';
Info.Signals(end).Dimensions  = 'NbDatagrams, 1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_NetworkAttitude','NbEntries.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'SensorSystemDescriptor';
Info.Signals(end).Dimensions  = 'NbDatagrams, 1';
Info.Signals(end).Storage     = 'uint8';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_NetworkAttitude','SensorSystemDescriptor.bin');
Info.Signals(end).Tag         = verifKeyWord('SensorSystemDecriptor');

Info.Signals(end+1).Name      = 'Time';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Index       = 'NbDatagrams, NbEntries';
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = 'days since JC';
Info.Signals(end).FileName    = fullfile('Ssc_NetworkAttitude','Time.bin');
Info.Signals(end).Tag         = verifKeyWord('MotionSensorTime');

Info.Signals(end+1).Name      = 'Roll';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Index       = 'NbDatagrams, NbEntries';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile('Ssc_NetworkAttitude','Roll.bin');
Info.Signals(end).Tag         = verifKeyWord('MotionSensorRoll');

Info.Signals(end+1).Name      = 'Pitch';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Index       = 'NbDatagrams, NbEntries';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile('Ssc_NetworkAttitude','Pitch.bin');
Info.Signals(end).Tag         = verifKeyWord('MotionSensorPitch');

Info.Signals(end+1).Name      = 'Heave';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Index       = 'NbDatagrams, NbEntries';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'm';
Info.Signals(end).FileName    = fullfile('Ssc_NetworkAttitude','Heave.bin');
Info.Signals(end).Tag         = verifKeyWord('MotionSensorHeave');

Info.Signals(end+1).Name      = 'Heading';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Index       = 'NbDatagrams, NbEntries';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile('Ssc_NetworkAttitude','Heading.bin');
Info.Signals(end).Tag         = verifKeyWord('MotionSensorHeading');

Info.Signals(end+1).Name      = 'NbBytes';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Index       = 'NbDatagrams, NbEntries';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile('Ssc_NetworkAttitude','NbBytes.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

% % Info.Image(1).Name      = 'NetworkAttitude';
% % Info.Image(end).Dimensions  = 'NbSamples, 1';
% % Info.Image(end).Index       = 'NbDatagrams, NbEntries, NbBytes';
% % Info.Image(end).Storage     = 'single';
% % Info.Image(end).Unit        = '';
% % Info.Image(end).FileName    = fullfile('Ssc_NetworkAttitude','NetworkAttitude.bin');
% % Info.Image(end).Tag         = verifKeyWord('TODO');

% Ajout des champs Scale_Factor, Add_Offset et Orig_Storage.
[flag, Info] = addScaleOffsetStorage(Datagrams, Info);

%% Cr�ation du fichier XML d�crivant la donn�e

nomFicXml = fullfile(nomDirRacine, 'Ssc_NetworkAttitude.xml');
xml_write(nomFicXml, Info);
flag = exist(nomFicXml, 'file');
if ~flag
    messageErreur(nomFicXml)
    return
end

%% Cr�ation du r�pertoire NetworkAttitude

nomDirNetworkAttitude = fullfile(nomDirRacine, 'Ssc_NetworkAttitude');
if ~exist(nomDirNetworkAttitude, 'dir')
    status = mkdir(nomDirNetworkAttitude);
    if ~status
        messageErreur(nomDirNetworkAttitude)
        flag = 0;
        return
    end
end

%% Cr�ation des fichiers binaires des signaux

for i=1:length(Info.Signals)
    flag = writeSignal(nomDirRacine, Info.Signals(i), Data.(Info.Signals(i).Name));
    if ~flag
        return
    end
end

flag = 1;
