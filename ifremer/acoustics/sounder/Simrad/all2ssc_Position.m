function flag = all2ssc_Position(nomDirRacine, FormatVersion)

nomFicXml = fullfile(nomDirRacine, 'ALL_Position.xml');
SonarFmt2ssc_message(nomFicXml, '.all')

%% Lecture du fichier XML d�crivant la donn�e

flag = exist(nomFicXml, 'file');
if ~flag
    return
end
% edit(nomFicXml)
Datagrams = xml_read(nomFicXml);

[nomDirSignal, nomFicSignal] = fileparts(nomFicXml);
nomDirSignal = fullfile(nomDirSignal, nomFicSignal);

%{
for k=1:length(Datagrams.Variables)
fprintf(1,'%d  %s\n', k, Datagrams.Variables(k).Name)
end
%}

nbSamples = Datagrams.NbDatagrams;

[flag, Date] = Read_BinVariable(Datagrams, nomDirSignal, 'Date', 'double');
if ~flag
    return
end
% figure; plot(Date); grid on; title('Date');

[flag, NbMilliSecs] = Read_BinVariable(Datagrams, nomDirSignal, 'NbMilliSecs', 'double');
if ~flag
    return
end
% figure; plot(NbMilliSecs); grid on; title('NbMilliSecs');

%% Lecture de Latitude

[flag, Data.Latitude] = Read_BinVariable(Datagrams, nomDirSignal, 'Latitude', 'double');
if ~flag
    return
end
% figure; plot(Data.Latitude); grid on; title('Latitude')

%% Lecture du Longitude

[flag, Data.Longitude] = Read_BinVariable(Datagrams, nomDirSignal, 'Longitude', 'double');
if ~flag
    return
end
% figure; plot(Data.Longitude); grid on; title('Longitude')

sub = find((Data.Latitude == 0) | (Data.Longitude == 0));
Data.Latitude(sub)  = NaN;
Data.Longitude(sub) = NaN;

%% Lecture du Speed

[flag, Data.Speed] = Read_BinVariable(Datagrams, nomDirSignal, 'Speed', 'single');
if ~flag
    return
end
Data.Speed(round(Data.Speed*100) == 65535) = NaN;
% figure; plot(Data.Speed); grid on; title('Speed')

%% Lecture du CourseVessel

[flag, Data.CourseVessel] = Read_BinVariable(Datagrams, nomDirSignal, 'CourseVessel', 'single');
if ~flag
    return
end
% figure; plot(Data.CourseVessel); grid on; title('CourseVessel')

%% Lecture du Heading

[flag, Data.Heading] = Read_BinVariable(Datagrams, nomDirSignal, 'Heading', 'single');
if ~flag
    return
end
% figure; plot(Data.Heading); grid on; title('Heading')

%% Lecture du PingCounter

[flag, Data.PingCounter] = Read_BinVariable(Datagrams, nomDirSignal, 'PingCounter', 'single');
if ~flag
    return
end
Data.PingCounter = unwrapPingCounterSimrad(Data.PingCounter);
% figure; plot(Data.PingCounter); grid on; title('PingCounter')

%% Lecture du Quality

[flag, Data.Quality] = Read_BinVariable(Datagrams, nomDirSignal, 'Quality', 'single');
if ~flag
    return
end
% figure; plot(Data.Quality); grid on; title('Quality')

%% Lecture du PositionDecriptor

[flag, Data.PositionDescriptor] = Read_BinVariable(Datagrams, nomDirSignal, 'PositionDescriptor', 'single');
if ~flag
    return
end
% figure; plot(Data.PositionDescriptor); grid on; title('PositionDecriptor')

%% Lecture du Nb de caract�res de la trame GGA

[flag, Data.NumberOfBytesPosInput] = Read_BinVariable(Datagrams, nomDirSignal, 'NumberOfBytesPosInput', 'single');
if ~isempty(Data.NumberOfBytesPosInput)
    if ~flag
        return
    end
    % figure; plot(Data.NumberOfBytesPosInput); grid on; title('NumberOfBytesPosInput')
    
    sumNbBytes = sum(Data.NumberOfBytesPosInput) + Datagrams.NbDatagrams;
    
    %% Lecture du Input Position Datagram
    
    [flag, X] = Read_BinTable(Datagrams, nomDirSignal, 'PositionInput', 'char', sumNbBytes);
    Offset = 1;
    X = X';
    X = X(:);
    X = X';
    Heure = NaN(Datagrams.NbDatagrams, 1);
    Lat   = NaN(Datagrams.NbDatagrams, 1);
    Lon   = NaN(Datagrams.NbDatagrams, 1);
    kGPS = 0;
    for iLoop=1:Datagrams.NbDatagrams
        strNMEA = X(1,Offset:Offset+Data.NumberOfBytesPosInput(iLoop));
        Offset = Offset + Data.NumberOfBytesPosInput(iLoop) + 1;
        try
            [flag, Heure1, Lat1, Lon1, GPS1] = read_Navigation_NMEA(strNMEA);
            if flag
                kGPS = kGPS + 1;
                Heure(kGPS,1) = Heure1;
                Lat(kGPS,1)   = Lat1;
                Lon(kGPS,1)   = Lon1;
                if kGPS == 1
                    GPS(Datagrams.NbDatagrams, 1) = GPS1; %#ok<AGROW>
                end
                GPS(kGPS) = GPS1; %#ok<AGROW>
            end
        catch ME
            ME.getReport()
        end
    end
%     if kGPS == 0
%         flag = 0;
%         return
%     end
    sub = 1:kGPS;
    if kGPS ~= 0
        GPS = GPS(sub, 1);
    end
    Heure = Heure(sub, 1);
    Lat   = Lat(sub, 1);
    Lon   = Lon(sub, 1);
    if flag
        subNonNaN = find(~isnan(Lat));
        nbSamples = length(subNonNaN);
        
        Date        = Date(subNonNaN);
        NbMilliSecs = NbMilliSecs(subNonNaN);
        
        Data.Latitude              = Data.Latitude(subNonNaN);
        Data.Longitude             = Data.Longitude(subNonNaN);
        Data.Speed                 = Data.Speed(subNonNaN);
        Data.CourseVessel          = Data.CourseVessel(subNonNaN);
        Data.Heading               = Data.Heading(subNonNaN);
        Data.PingCounter           = Data.PingCounter(subNonNaN);
        Data.Quality               = Data.Quality(subNonNaN);
        Data.PositionDescriptor    = Data.PositionDescriptor(subNonNaN);
        Data.NumberOfBytesPosInput = Data.NumberOfBytesPosInput(subNonNaN);
        
        Data.Lat     = Lat(subNonNaN);
        Data.Lon     = Lon(subNonNaN);
        Data.GPSTime = Heure(subNonNaN);
        
        if kGPS ~= 0
            Data.QualityIndicator              = [GPS(subNonNaN).QualityIndicator]';
            Data.NumberSatellitesUsed          = [GPS(subNonNaN).NumberSatellitesUsed]';
            Data.HorizontalDilutionOfPrecision = [GPS(subNonNaN).HorizontalDilutionOfPrecision]';
            Data.AltitudeOfIMU                 = [GPS(subNonNaN).AltitudeOfIMU]';
            Data.GeoidalSeparation             = [GPS(subNonNaN).GeoidalSeparation]';
            Data.AgeOfDifferentialCorrections  = [GPS(subNonNaN).AgeOfDifferentialCorrections]';
            Data.DGPSRefStationIdentity        = [GPS(subNonNaN).DGPSRefStationIdentity]';
        end
    end
end

%% Calcul des dates et heures

Jour  = mod(Date,100);
Date  = (Date - Jour) / 100;
Mois  = mod(Date,100);
Annee = (Date - Mois) / 100;
Date = dayJma2Ifr(Jour, Mois, Annee);

Data.Time = cl_time('timeIfr', Date, NbMilliSecs);
if isfield(Data, 'NumberSatellitesUsed')
    Data.GPSTime = cl_time('timeIfr', Date, Data.GPSTime);
end

%% G�n�ration du XML SonarScope

Info.Title              = 'Position';
Info.Constructor        = 'Kongsberg';
Info.EmModel            = Datagrams.Model;
Info.SystemSerialNumber = Datagrams.SystemSerialNumber;
Info.TimeOrigin         = '01/01/-4713';
Info.Comments           = 'Sensor sampling rate';
Info.FormatVersion      = FormatVersion;

Info.Dimensions.NbSamples = nbSamples;

Info.Signals(1).Name         = 'Time';
Info.Signals(end).Dimensions = 'NbSamples, 1';
Info.Signals(end).Storage    = 'double';
Info.Signals(end).Unit       = 'days since JC';
Info.Signals(end).FileName   = fullfile('Ssc_Position','Time.bin');
Info.Signals(end).Tag        = verifKeyWord('GPSTime');

Info.Signals(end+1).Name     = 'PingCounter';
Info.Signals(end).Dimensions = 'NbSamples, 1';
Info.Signals(end).Storage    = 'uint16';
Info.Signals(end).Unit       = '';
Info.Signals(end).FileName   = fullfile('Ssc_Position','PingCounter.bin');
Info.Signals(end).Tag        = verifKeyWord('SounderPingCounter');

Info.Signals(end+1).Name     = 'Latitude';
Info.Signals(end).Dimensions = 'NbSamples, 1';
Info.Signals(end).Storage    = 'double';
Info.Signals(end).Unit       = 'deg';
Info.Signals(end).FileName   = fullfile('Ssc_Position','Latitude.bin');
Info.Signals(end).Tag        = verifKeyWord('GPSLatitude');

Info.Signals(end+1).Name     = 'Longitude';
Info.Signals(end).Dimensions = 'NbSamples, 1';
Info.Signals(end).Storage    = 'double';
Info.Signals(end).Unit       = 'deg';
Info.Signals(end).FileName   = fullfile('Ssc_Position','Longitude.bin');
Info.Signals(end).Tag        = verifKeyWord('GPSLongitude');

Info.Signals(end+1).Name     = 'Quality'; %% Attention, pr�voir un changement de nom : Quality in cm
Info.Signals(end).Dimensions = 'NbSamples, 1';
Info.Signals(end).Storage    = 'single';
Info.Signals(end).Unit       = '';
Info.Signals(end).FileName   = fullfile('Ssc_Position','Quality.bin');
Info.Signals(end).Tag        = verifKeyWord('GPSAccuracy');

Info.Signals(end+1).Name     = 'Speed';
Info.Signals(end).Dimensions = 'NbSamples, 1';
Info.Signals(end).Storage    = 'single';
Info.Signals(end).Unit       = 'm/s';
Info.Signals(end).FileName   = fullfile('Ssc_Position','Speed.bin');
Info.Signals(end).Tag        = verifKeyWord('GPSSpeed');

Info.Signals(end+1).Name     = 'CourseVessel';
Info.Signals(end).Dimensions = 'NbSamples, 1';
Info.Signals(end).Storage    = 'single';
Info.Signals(end).Unit       = 'deg';
Info.Signals(end).FileName   = fullfile('Ssc_Position','CourseVessel.bin');
Info.Signals(end).Tag        = verifKeyWord('GPSHeading');

Info.Signals(end+1).Name     = 'Heading';
Info.Signals(end).Dimensions = 'NbSamples, 1';
Info.Signals(end).Storage    = 'single';
Info.Signals(end).Unit       = 'deg';
Info.Signals(end).FileName   = fullfile('Ssc_Position','Heading.bin');
Info.Signals(end).Tag        = verifKeyWord('GPSHeading');

if isfield(Data, 'NumberSatellitesUsed')
    Info.Signals(end+1).Name     = 'GPSTime';
    Info.Signals(end).Dimensions = 'NbSamples, 1';
    Info.Signals(end).Storage    = 'double';
    Info.Signals(end).Unit       = 'days since JC';
    Info.Signals(end).FileName   = fullfile('Ssc_Position','GPSTime.bin');
    Info.Signals(end).Tag        = verifKeyWord('GPSTime');
    
    Info.Signals(end+1).Name     = 'QualityIndicator';
    Info.Signals(end).Dimensions = 'NbSamples, 1';
    Info.Signals(end).Storage    = 'single';
    Info.Signals(end).Unit       = '';
    Info.Signals(end).FileName   = fullfile('Ssc_Position','QualityIndicator.bin');
    Info.Signals(end).Tag        = verifKeyWord('GPSQualityIndicator');
    
    Info.Signals(end+1).Name     = 'NumberSatellitesUsed';
    Info.Signals(end).Dimensions = 'NbSamples, 1';
    Info.Signals(end).Storage    = 'single';
    Info.Signals(end).Unit       = '';
    Info.Signals(end).FileName   = fullfile('Ssc_Position','NumberSatellitesUsed.bin');
    Info.Signals(end).Tag        = verifKeyWord('GPSNumberSatellitesUsed');
    
    Info.Signals(end+1).Name     = 'HorizontalDilutionOfPrecision';
    Info.Signals(end).Dimensions = 'NbSamples, 1';
    Info.Signals(end).Storage    = 'single';
    Info.Signals(end).Unit       = '';
    Info.Signals(end).FileName   = fullfile('Ssc_Position','HorizontalDilutionOfPrecision.bin');
    Info.Signals(end).Tag        = verifKeyWord('GPSHDOP');
    
    Info.Signals(end+1).Name     = 'AltitudeOfIMU';
    Info.Signals(end).Dimensions = 'NbSamples, 1';
    Info.Signals(end).Storage    = 'single';
    Info.Signals(end).Unit       = 'm';
    Info.Signals(end).FileName   = fullfile('Ssc_Position','AltitudeOfIMU.bin');
    Info.Signals(end).Tag        = verifKeyWord('GPSAltitudeOfMIU');
    
    Info.Signals(end+1).Name     = 'GeoidalSeparation';
    Info.Signals(end).Dimensions = 'NbSamples, 1';
    Info.Signals(end).Storage    = 'single';
    Info.Signals(end).Unit       = 'm';
    Info.Signals(end).FileName   = fullfile('Ssc_Position','GeoidalSeparation.bin');
    Info.Signals(end).Tag        = verifKeyWord('GPSGeoidalSeparation');
    
    Info.Signals(end+1).Name     = 'AgeOfDifferentialCorrections';
    Info.Signals(end).Dimensions = 'NbSamples, 1';
    Info.Signals(end).Storage    = 'single';
    Info.Signals(end).Unit       = 's';
    Info.Signals(end).FileName   = fullfile('Ssc_Position','AgeOfDifferentialCorrections.bin');
    Info.Signals(end).Tag        = verifKeyWord('GPSAgeOfDifferentialCorrections');
end

if isfield(Data, 'PositionDescriptor')
    Info.Signals(end+1).Name     = 'PositionDescriptor';
    Info.Signals(end).Dimensions = 'NbSamples, 1';
    Info.Signals(end).Storage    = 'uint8';
    Info.Signals(end).Unit       = '';
    Info.Signals(end).FileName   = fullfile('Ssc_Position','PositionDescriptor.bin');
    Info.Signals(end).Tag        = verifKeyWord('GPSDescriptor');
end

% Ajout des champs Scale_Factor, Add_Offset et Orig_Storage.
[flag, Info] = addScaleOffsetStorage(Datagrams, Info); %#ok<ASGLU>

if isfield(Data, 'NumberSatellitesUsed')
    Info.Strings(1).Name         = 'DGPSRefStationIdentity';
    Info.Strings(end).Dimensions = 'NbSamples, 1';
    Info.Strings(end).Storage    = 'char';
    Info.Strings(end).Unit       = 'deg';
    Info.Strings(end).FileName   = fullfile('Ssc_Position','DGPSRefStationIdentity.bin');
    Info.Strings(end).Tag        = verifKeyWord('GPSDGPSRefStationIdentity');
end

%% Cr�ation du fichier XML d�crivant la donn�e

nomFicXml = fullfile(nomDirRacine, 'Ssc_Position.xml');
xml_write(nomFicXml, Info);
flag = exist(nomFicXml, 'file');
if ~flag
    messageErreur(nomFicXml)
    return
end

%% Cr�ation du r�pertoire Position

nomDirAttitude = fullfile(nomDirRacine, 'Ssc_Position');
if ~exist(nomDirAttitude, 'dir')
    status = mkdir(nomDirAttitude);
    if ~status
        messageErreur(nomDirAttitude)
        flag = 0;
        return
    end
end

%% Cr�ation des fichiers binaires des signaux

for k=1:length(Info.Signals)
    flag = writeSignal(nomDirRacine, Info.Signals(k), Data.(Info.Signals(k).Name));
    if ~flag
        return
    end
end

%% Cr�ation des fichiers binaires des strings

if isfield(Info, 'Strings')
    for k=1:length(Info.Strings)
        flag = writeString(nomDirRacine, Info.Strings(k), Data.(Info.Strings(k).Name));
        if ~flag
            return
        end
    end
end
flag = 1;
