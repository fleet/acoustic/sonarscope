function flag = all2ssc_QualityFactor(nomDirRacine, FormatVersion, Head1)

nomFicXml = fullfile(nomDirRacine, 'ALL_QualityFactor.xml');
SonarFmt2ssc_message(nomFicXml, '.all')

%% Lecture du fichier XML d�crivant la donn�e 

flag = exist(nomFicXml, 'file');
if ~flag
    special_message('FichierNonExistant', nomFicXml)
    return
end

% edit(nomFicXml)
Datagrams = xml_read(nomFicXml);

[nomDirSignal, nomFicSignal] = fileparts(nomFicXml);
nomDirSignal = fullfile(nomDirSignal, nomFicSignal);


%{
for i=1:length(Datagrams.Variables)
    fprintf(1,'%d  %s\n', i, Datagrams.Variables(i).Name)
end
for i=1:length(Datagrams.Tables)
    fprintf(1,'%d  %s\n', i, Datagrams.Tables(i).Name)
end
%}


%% Transcodage de signaux

[flag, Date] = Read_BinVariable(Datagrams, nomDirSignal, 'Date', 'double');
if ~flag
    return
end
% figure; plot(Date); grid on; title('Date');

[flag, NbMilliSecs] = Read_BinVariable(Datagrams, nomDirSignal, 'NbMilliSecs', 'double');
if ~flag
    return
end
% figure; plot(NbMilliSecs); grid on; title('NbMilliSecs');

[flag, PingCounter] = Read_BinVariable(Datagrams, nomDirSignal, 'PingCounter', 'single');
if ~flag
    return
end
PingCounter = unwrapPingCounterSimrad(PingCounter);
% figure; plot(PingCounter); grid on; title('PingCounter');

[flag, SystemSerialNumber] = Read_BinVariable(Datagrams, nomDirSignal, 'SystemSerialNumber', 'single');
if ~flag
    return
end
% figure; plot(SystemSerialNumber, '+'); grid on; title('SystemSerialNumber');

[flag, NbDetections] = Read_BinVariable(Datagrams, nomDirSignal, 'NbDetections', 'single');
if ~flag
    return
end
% figure; plot(NbDetections); grid on; title('NbDetections');


nbSamples = sum(NbDetections);
MaxDesMaxNbBeams = max(NbDetections);
MaxNbBeamsPossible = repmat(MaxDesMaxNbBeams, length(NbDetections), 1);
BeamNumber = repmat(1:MaxDesMaxNbBeams, Datagrams.NbDatagrams, 1);
BeamNumber = BeamNumber';
BeamNumber = BeamNumber(:);

Data.PingCounter        = Kongsberg_table2signal(PingCounter,        Datagrams.NbDatagrams, PingCounter, SystemSerialNumber, Head1);
Data.SystemSerialNumber = Kongsberg_table2signal(SystemSerialNumber, Datagrams.NbDatagrams, PingCounter, SystemSerialNumber, Head1);
Date                    = Kongsberg_table2signal(Date,               Datagrams.NbDatagrams, PingCounter, SystemSerialNumber, Head1);
NbMilliSecs             = Kongsberg_table2signal(NbMilliSecs,        Datagrams.NbDatagrams, PingCounter, SystemSerialNumber, Head1);


%% TwoWayTTAmpDetect

[flag, X] = Read_BinTable(Datagrams, nomDirSignal, 'TwoWayTTAmpDetect', 'single', nbSamples);
if ~flag
    return
end
Data.TwoWayTTAmpDetect = Kongsberg_table2image(X, 'TwoWayTTAmpDetect', Datagrams.NbDatagrams, MaxDesMaxNbBeams, MaxNbBeamsPossible, BeamNumber, PingCounter, SystemSerialNumber, Head1);
% figure; imagesc(Data.TwoWayTTAmpDetect); title('TwoWayTTAmpDetect'); axis xy; colorbar;

%% TwoWayTTPhaseDetect

[flag, X] = Read_BinTable(Datagrams, nomDirSignal, 'TwoWayTTPhaseDetect', 'single', nbSamples);
if ~flag
    return
end
Data.TwoWayTTPhaseDetect = Kongsberg_table2image(X, 'TwoWayTTPhaseDetect', Datagrams.NbDatagrams, MaxDesMaxNbBeams, MaxNbBeamsPossible, BeamNumber, PingCounter, SystemSerialNumber, Head1);
% figure; imagesc(Data.TwoWayTTPhaseDetect); title('TwoWayTTPhaseDetect'); axis xy; colorbar;

%% AmpDetectQF

[flag, X] = Read_BinTable(Datagrams, nomDirSignal, 'AmpDetectQF', 'single', nbSamples);
if ~flag
    return
end
Data.AmpDetectQF = Kongsberg_table2image(X, 'AmpDetectQF', Datagrams.NbDatagrams, MaxDesMaxNbBeams, MaxNbBeamsPossible, BeamNumber, PingCounter, SystemSerialNumber, Head1);
% figure; imagesc(Data.AmpDetectQF); title('AmpDetectQF'); axis xy; colorbar;

%% PhaseDetectQF

[flag, X] = Read_BinTable(Datagrams, nomDirSignal, 'PhaseDetectQF', 'single', nbSamples);
if ~flag
    return
end
Data.PhaseDetectQF = Kongsberg_table2image(X, 'PhaseDetectQF', Datagrams.NbDatagrams, MaxDesMaxNbBeams, MaxNbBeamsPossible, BeamNumber, PingCounter, SystemSerialNumber, Head1);
% figure; imagesc(Data.PhaseDetectQF); title('PhaseDetectQF'); axis xy; colorbar;

%% AmpDetectStatus

[flag, X] = Read_BinTable(Datagrams, nomDirSignal, 'AmpDetectStatus', 'single', nbSamples);
if ~flag
    return
end
Data.AmpDetectStatus = Kongsberg_table2image(X, 'AmpDetectStatus', Datagrams.NbDatagrams, MaxDesMaxNbBeams, MaxNbBeamsPossible, BeamNumber, PingCounter, SystemSerialNumber, Head1);
% figure; imagesc(Data.AmpDetectStatus); title('AmpDetectStatus'); axis xy; colorbar;

%% PhaseDetectStatus

[flag, X] = Read_BinTable(Datagrams, nomDirSignal, 'PhaseDetectStatus', 'single', nbSamples);
if ~flag
    return
end
Data.PhaseDetectStatus = Kongsberg_table2image(X, 'PhaseDetectStatus', Datagrams.NbDatagrams, MaxDesMaxNbBeams, MaxNbBeamsPossible, BeamNumber, PingCounter, SystemSerialNumber, Head1);
% figure; imagesc(Data.PhaseDetectStatus); title('PhaseDetectStatus'); axis xy; colorbar;

%% RawBeamNumber

[flag, X] = Read_BinTable(Datagrams, nomDirSignal, 'RawBeamNumber', 'single', nbSamples);
if ~flag
    return
end
Data.RawBeamNumber = Kongsberg_table2image(X, 'RawBeamNumber', Datagrams.NbDatagrams, MaxDesMaxNbBeams, MaxNbBeamsPossible, BeamNumber, PingCounter, SystemSerialNumber, Head1);
% figure; imagesc(Data.RawBeamNumber); title('RawBeamNumber'); axis xy; colorbar;

%% RawBeamAngle

[flag, X] = Read_BinTable(Datagrams, nomDirSignal, 'RawBeamAngle', 'single', nbSamples);
if ~flag
    return
end
Data.RawBeamAngle = Kongsberg_table2image(X, 'RawBeamAngle', Datagrams.NbDatagrams, MaxDesMaxNbBeams, MaxNbBeamsPossible, BeamNumber, PingCounter, SystemSerialNumber, Head1);
Data.RawBeamAngle = Data.RawBeamAngle * (180/pi);
% figure; imagesc(Data.RawBeamAngle); title('RawBeamAngle'); axis xy; colorbar;

%% Masque

% Data.Mask = repmat(false, size(Data.Reflectivity));

%% Calcul des dates et heures

Jour  = mod(Date,100);
Date  = (Date - Jour) / 100;
Mois  = mod(Date,100);
Annee = (Date - Mois) / 100;
Date = dayJma2Ifr(Jour, Mois, Annee);
Data.Time = cl_time('timeIfr', Date, NbMilliSecs);

%% G�n�ration du XML SonarScope

Info.Title                    = 'QualityFactor';
Info.Constructor              = 'Kongsberg';
Info.EmModel                  = Datagrams.Model;
Info.ListeSystemSerialNumber  = num2str(unique(Data.SystemSerialNumber(:))');
Info.TimeOrigin               = '01/01/-4713';
Info.Comments                 = 'Sounder ping rate';
Info.Version                  = 'V2';
Info.FormatVersion            = FormatVersion;

Info.Dimensions.nbPings       = size(Data.TwoWayTTAmpDetect, 1);
Info.Dimensions.nbBeams       = size(Data.TwoWayTTAmpDetect, 2);
Info.Dimensions.nbSounders    = size(Data.PingCounter,2);

Info.Signals(1).Name          = 'Time';
Info.Signals(end).Dimensions  = 'nbPings, nbSounders';
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = 'days since JC';
Info.Signals(end).FileName    = fullfile('Ssc_QualityFactor','Time.bin');
Info.Signals(end).Tag         = verifKeyWord('SounderTime');

Info.Signals(end+1).Name      = 'PingCounter';
Info.Signals(end).Dimensions  = 'nbPings, nbSounders';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_QualityFactor','PingCounter.bin');
Info.Signals(end).Tag         = verifKeyWord('SounderPingCounter');

Info.Signals(end+1).Name      = 'SystemSerialNumber';
Info.Signals(end).Dimensions  = 'nbPings, nbSounders';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_QualityFactor','SystemSerialNumber.bin');
Info.Signals(end).Tag         = verifKeyWord('SounderSerialNumber');


Info.Images(1).Name           = 'TwoWayTTAmpDetect';
Info.Images(end).Dimensions   = 'nbPings, nbBeams';
Info.Images(end).Index        = 'nbPings, nbBeams';
Info.Images(end).Storage      = 'single';
Info.Images(end).Unit         = 'm';
Info.Images(end).FileName     = fullfile('Ssc_QualityFactor','TwoWayTTAmpDetect.bin');
Info.Images(end).Tag          = verifKeyWord('TODO');

Info.Images(end+1).Name       = 'TwoWayTTPhaseDetect';
Info.Images(end).Dimensions   = 'nbPings, nbBeams';
Info.Images(end).Index        = 'nbPings, nbBeams';
Info.Images(end).Storage      = 'single';
Info.Images(end).Unit         = 'm';
Info.Images(end).FileName     = fullfile('Ssc_QualityFactor','TwoWayTTPhaseDetect.bin');
Info.Images(end).Tag          = verifKeyWord('TwoWayTravelTimeInSeconds');

Info.Images(end+1).Name       = 'AmpDetectQF';
Info.Images(end).Dimensions   = 'nbPings, nbBeams';
Info.Images(end).Index        = 'nbPings, nbBeams';
Info.Images(end).Storage      = 'single';
Info.Images(end).Unit         = 'm';
Info.Images(end).FileName     = fullfile('Ssc_QualityFactor','AmpDetectQF.bin');
Info.Images(end).Tag          = verifKeyWord('TwoWayTravelTimeInSeconds');

Info.Images(end+1).Name       = 'PhaseDetectQF';
Info.Images(end).Dimensions   = 'nbPings, nbBeams';
Info.Images(end).Index        = 'nbPings, nbBeams';
Info.Images(end).Storage      = 'single';
Info.Images(end).Unit         = 'deg';
Info.Images(end).FileName     = fullfile('Ssc_QualityFactor','PhaseDetectQF.bin');
Info.Images(end).Tag          = verifKeyWord('TODO');

Info.Images(end+1).Name       = 'AmpDetectStatus';
Info.Images(end).Dimensions   = 'nbPings, nbBeams';
Info.Images(end).Index        = 'nbPings, nbBeams';
Info.Images(end).Storage      = 'single';
Info.Images(end).Unit         = 'deg';
Info.Images(end).FileName     = fullfile('Ssc_QualityFactor','AmpDetectStatus.bin');
Info.Images(end).Tag          = verifKeyWord('TODO');

Info.Images(end+1).Name       = 'PhaseDetectStatus';
Info.Images(end).Dimensions   = 'nbPings, nbBeams';
Info.Images(end).Index        = 'nbPings, nbBeams';
Info.Images(end).Storage      = 'single';
Info.Images(end).Unit         = 'samples';
Info.Images(end).FileName     = fullfile('Ssc_QualityFactor','PhaseDetectStatus.bin');
Info.Images(end).Tag          = verifKeyWord('TODO');

Info.Images(end+1).Name       = 'RawBeamNumber';
Info.Images(end).Dimensions   = 'nbPings, nbBeams';
Info.Images(end).Index        = 'nbPings, nbBeams';
Info.Images(end).Storage      = 'single';
Info.Images(end).Unit         = '';
Info.Images(end).FileName     = fullfile('Ssc_QualityFactor','RawBeamNumber.bin');
Info.Images(end).Tag          = verifKeyWord('TODO');

Info.Images(end+1).Name       = 'RawBeamAngle';
Info.Images(end).Dimensions   = 'nbPings, nbBeams';
Info.Images(end).Index        = 'nbPings, nbBeams';
Info.Images(end).Storage      = 'single';
Info.Images(end).Unit         = '';
Info.Images(end).FileName     = fullfile('Ssc_QualityFactor','RawBeamAngle.bin');
Info.Images(end).Tag          = verifKeyWord('TODO');

% Ajout des champs Scale_Factor, Add_Offset et Orig_Storage.
[flag, Info] = addScaleOffsetStorage(Datagrams, Info);
%% Cr�ation du fichier XML d�crivant la donn�e 

nomFicXml = fullfile(nomDirRacine, 'Ssc_QualityFactor.xml');
xml_write(nomFicXml, Info);
flag = exist(nomFicXml, 'file');
if ~flag
    messageErreur(nomFicXml)
    return
end
% edit(nomFicXml)

%% Cr�ation du r�pertoire Depth

nomDirDepth = fullfile(nomDirRacine, 'Ssc_QualityFactor');
if ~exist(nomDirDepth, 'dir')
    status = mkdir(nomDirDepth);
    if ~status
        messageErreur(nomDirDepth)
        flag = 0;
        return
    end
end

%% Cr�ation des fichiers binaires des signaux

for i=1:length(Info.Signals)
    flag = writeSignal(nomDirRacine, Info.Signals(i), Data.(Info.Signals(i).Name));
    if ~flag
        return
    end
end

%% Cr�ation des fichiers binaires des images

for i=1:length(Info.Images)
    flag = writeImage(nomDirRacine, Info.Images(i), Data.(Info.Images(i).Name));
    if ~flag
        return
    end
end

flag = 1;
