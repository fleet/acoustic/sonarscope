function flag = all2ssc_QualityFactorFromIfr(nomDirRacine, FormatVersion, Head1)

nomFicXml = fullfile(nomDirRacine, 'ALL_QualityFactorFromIfr.xml');
SonarFmt2ssc_message(nomFicXml, '.all')

%% Lecture du fichier XML d�crivant la donn�e 

flag = exist(nomFicXml, 'file');
if ~flag
    special_message('FichierNonExistant', nomFicXml)
    return
end

% edit(nomFicXml)
Datagrams = xml_read(nomFicXml);

[nomDirSignal, nomFicSignal] = fileparts(nomFicXml);
nomDirSignal = fullfile(nomDirSignal, nomFicSignal);


%{
for i=1:length(Datagrams.Variables)
    fprintf(1,'%d  %s\n', i, Datagrams.Variables(i).Name)
end
for i=1:length(Datagrams.Tables)
    fprintf(1,'%d  %s\n', i, Datagrams.Tables(i).Name)
end
%}


%% Transcodage de signaux

[flag, Date1] = Read_BinVariable(Datagrams, nomDirSignal, 'Date', 'double');
if ~flag
    return
end
% figure; plot(Date); grid on; title('Date');

[flag, NbMilliSecs1] = Read_BinVariable(Datagrams, nomDirSignal, 'NbMilliSecs', 'double');
if ~flag
    return
end
% figure; plot(NbMilliSecs); grid on; title('NbMilliSecs');

[flag, PingCounter] = Read_BinVariable(Datagrams, nomDirSignal, 'PingCounter', 'single');
if ~flag
    return
end
PingCounter = unwrapPingCounterSimrad(PingCounter);
% figure; plot(PingCounter); grid on; title('PingCounter');

[flag, SystemSerialNumber] = Read_BinVariable(Datagrams, nomDirSignal, 'SystemSerialNumber', 'single');
if ~flag
    return
end
% figure; plot(SystemSerialNumber, '+'); grid on; title('SystemSerialNumber');

[flag, NbRx] = Read_BinVariable(Datagrams, nomDirSignal, 'NbRx', 'single');
if ~flag
    return
end
% figure; plot(NbDetections); grid on; title('NbDetections');


nbSamples          = sum(NbRx);
MaxDesMaxNbBeams   = max(NbRx);
MaxNbBeamsPossible = repmat(MaxDesMaxNbBeams, length(NbRx), 1);
BeamNumber         = repmat(1:MaxDesMaxNbBeams, Datagrams.NbDatagrams, 1);
BeamNumber         = BeamNumber';
BeamNumber         = BeamNumber(:);

Data.PingCounter        = Kongsberg_table2signal(PingCounter,        Datagrams.NbDatagrams, PingCounter, SystemSerialNumber, Head1);
Data.SystemSerialNumber = Kongsberg_table2signal(SystemSerialNumber, Datagrams.NbDatagrams, PingCounter, SystemSerialNumber, Head1);
Date                    = Kongsberg_table2signal(Date1,              Datagrams.NbDatagrams, PingCounter, SystemSerialNumber, Head1);
NbMilliSecs             = Kongsberg_table2signal(NbMilliSecs1,       Datagrams.NbDatagrams, PingCounter, SystemSerialNumber, Head1);

% D�but rajout JMA le 20/09/2016 avec mise en commentaire Kongsberg_table2signal
subNaN = find(isnan(sum(Data.PingCounter,2)));
Data.SystemSerialNumber(subNaN,:) = [];
Date(subNaN,:)                    = [];
NbMilliSecs(subNaN,:)             = [];
% Fin rajout JMA le 20/09/2016 avec mise en commentaire Kongsberg_table2signal

%% IfremerQF

[flag, X] = Read_BinTable(Datagrams, nomDirSignal, 'IfremerQF', 'single', nbSamples);
if ~flag
    return
end
Data.IfremerQF = Kongsberg_table2image(X, 'IfremerQF', Datagrams.NbDatagrams, MaxDesMaxNbBeams, MaxNbBeamsPossible, BeamNumber, PingCounter, SystemSerialNumber, Head1);
Data.IfremerQF(subNaN,:) = [];
% figure; imagesc(Data.IfremerQF); title('IfremerQF'); axis xy; colorbar;

%% Correctif JMA le 20/02/2018 pour fichier 0000_20180502_112500_Belgica.all

nbPings = min([size(Data.PingCounter,1) 
size(Data.SystemSerialNumber,1) 
size(Data.IfremerQF,1)]);
subPings = 1:nbPings;
Data.PingCounter        = Data.PingCounter(subPings,:);
Data.SystemSerialNumber = Data.SystemSerialNumber(subPings,:);
Data.IfremerQF          = Data.IfremerQF(subPings,:);

%% Masque

% Data.Mask = repmat(false, size(Data.Reflectivity));

%% Calcul des dates et heures

Jour  = mod(Date,100);
Date  = (Date - Jour) / 100;
Mois  = mod(Date,100);
Annee = (Date - Mois) / 100;
Date = dayJma2Ifr(Jour, Mois, Annee);
Data.Time = cl_time('timeIfr', Date, NbMilliSecs);

%% G�n�ration du XML SonarScope

Info.Title                    = 'QualityFactorFromIfr';
Info.Constructor              = 'Kongsberg';
Info.EmModel                  = Datagrams.Model;
Info.ListeSystemSerialNumber  = num2str(unique(Data.SystemSerialNumber(:))');
Info.TimeOrigin               = '01/01/-4713';
Info.Comments                 = 'Quality Factor From Ifremer';
Info.Version                  = 'V2';
Info.FormatVersion            = FormatVersion;

Info.Dimensions.nbPings       = size(Data.IfremerQF, 1);
Info.Dimensions.nbBeams       = size(Data.IfremerQF, 2);
Info.Dimensions.nbSounders    = size(Data.PingCounter,2);

Info.Signals(1).Name          = 'Time';
Info.Signals(end).Dimensions  = 'nbPings, nbSounders';
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = 'days since JC';
Info.Signals(end).FileName    = fullfile('Ssc_QualityFactorFromIfr','Time.bin');
Info.Signals(end).Tag         = verifKeyWord('SounderTime');

Info.Signals(end+1).Name      = 'PingCounter';
Info.Signals(end).Dimensions  = 'nbPings, nbSounders';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_QualityFactorFromIfr','PingCounter.bin');
Info.Signals(end).Tag         = verifKeyWord('SounderPingCounter');

Info.Signals(end+1).Name      = 'SystemSerialNumber';
Info.Signals(end).Dimensions  = 'nbPings, nbSounders';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_QualityFactorFromIfr','SystemSerialNumber.bin');
Info.Signals(end).Tag         = verifKeyWord('SounderSerialNumber');

Info.Images(1).Name           = 'IfremerQF';
Info.Images(end).Dimensions   = 'nbPings, nbBeams';
Info.Images(end).Index        = 'nbPings, nbBeams';
Info.Images(end).Storage      = 'single';
Info.Images(end).Unit         = '';
Info.Images(end).FileName     = fullfile('Ssc_QualityFactorFromIfr','IfremerQF.bin');
Info.Images(end).Tag          = verifKeyWord('TODO');


% Ajout des champs Scale_Factor, Add_Offset et Orig_Storage.
[flag, Info] = addScaleOffsetStorage(Datagrams, Info);

%% Cr�ation du fichier XML d�crivant la donn�e 

nomFicXml = fullfile(nomDirRacine, 'Ssc_QualityFactorFromIfr.xml');
xml_write(nomFicXml, Info);
flag = exist(nomFicXml, 'file');
if ~flag
    messageErreur(nomFicXml)
    return
end
% edit(nomFicXml)

%% Cr�ation du r�pertoire Depth

nomDirDepth = fullfile(nomDirRacine, 'Ssc_QualityFactorFromIfr');
if ~exist(nomDirDepth, 'dir')
    status = mkdir(nomDirDepth);
    if ~status
        messageErreur(nomDirDepth)
        flag = 0;
        return
    end
end

%% Cr�ation des fichiers binaires des signaux

for i=1:length(Info.Signals)
    flag = writeSignal(nomDirRacine, Info.Signals(i), Data.(Info.Signals(i).Name));
    if ~flag
        return
    end
end

%% Cr�ation des fichiers binaires des images

for i=1:length(Info.Images)
    flag = writeImage(nomDirRacine, Info.Images(i), Data.(Info.Images(i).Name));
    if ~flag
        return
    end
end

flag = 1;
