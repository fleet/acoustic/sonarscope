function flag = all2ssc_RawBeamSamples(nomDirRacine, FormatVersion, Head1)

nomFicXml = fullfile(nomDirRacine, 'ALL_RawBeamSamples.xml');
SonarFmt2ssc_message(nomFicXml, '.all')

%% Lecture du fichier XML d�crivant la donn�e 

flag = exist(nomFicXml, 'file');
if ~flag
    special_message('FichierNonExistant', nomFicXml)
    return
end
% edit(nomFicXml)
Datagrams = xml_read(nomFicXml);

[nomDirSignal, nomFicSignal] = fileparts(nomFicXml);
nomDirSignal = fullfile(nomDirSignal, nomFicSignal);


%{
for k=1:length(Datagrams.Variables)
    fprintf(1,'%d  %s\n', k, Datagrams.Variables(k).Name)
end
for k=1:length(Datagrams.Tables)
    fprintf(1,'%d  %s\n', k, Datagrams.Tables(k).Name)
end
%}

[flag, NumberOfDatagrams] = Read_BinVariable(Datagrams, nomDirSignal, 'NumberOfDatagrams', 'single');
if ~flag
    return
end
% figure; plot(NumberOfDatagrams); grid on; title('NumberOfDatagrams');

[flag, DatagramNumbers] = Read_BinVariable(Datagrams, nomDirSignal, 'DatagramNumbers', 'single');
if ~flag
    return
end
% figure; plot(DatagramNumbers); grid on; title('DatagramNumbers');

[flag, Date1] = Read_BinVariable(Datagrams, nomDirSignal, 'Date', 'double');
if ~flag
    return
end
% figure; plot(Date); grid on; title('Date');

[flag, NbMilliSecs1] = Read_BinVariable(Datagrams, nomDirSignal, 'NbMilliSecs', 'double');
if ~flag
    return
end
% figure; plot(NbMilliSecs); grid on; title('NbMilliSecs');

[flag, PingCounter] = Read_BinVariable(Datagrams, nomDirSignal, 'PingCounter', 'single');
if ~flag
    return
end
PingCounter = unwrapPingCounterSimrad(PingCounter);
% figure; plot(PingCounter); grid on; title('PingCounter');

[flag, SystemSerialNumber] = Read_BinVariable(Datagrams, nomDirSignal, 'SystemSerialNumber', 'single');
if ~flag
    return
end
% figure; plot(SystemSerialNumber, '+'); grid on; title('SystemSerialNumber');

% GLU TODO : SamplingFreq=NaN
[flag, SamplingFreq] = Read_BinVariable(Datagrams, nomDirSignal, 'SamplingFreq', 'single');
if ~flag
    return
end
% figure; plot(SamplingFreq, '+'); grid on; title('SamplingFreq');

[flag, SoundSpeed] = Read_BinVariable(Datagrams, nomDirSignal, 'SoundSpeed', 'single');
if ~flag
    return
end
% figure; plot(SoundSpeed); grid on; title('SoundSpeed');

[flag, NTx] = Read_BinVariable(Datagrams, nomDirSignal, 'NTx', 'single');
if ~flag
    return
end
% figure; plot(NTx); grid on; title('NTx');
nbTx = sum(NTx);

[flag, TotalOfReceiveBeams] = Read_BinVariable(Datagrams, nomDirSignal, 'TotalOfReceiveBeams', 'single');
if ~flag
    return
end
% figure; plot(TotalOfReceiveBeams); grid on; title('TotalOfReceiveBeams');
[flag, NRx] = Read_BinVariable(Datagrams, nomDirSignal, 'NRx', 'single');

if ~flag
    return
end
% figure; plot(NRx); grid on; title('NRx');
nbRx = sum(NRx);

[flag, TxTimeHeave] = Read_BinVariable(Datagrams, nomDirSignal, 'TxTimeHeave', 'single');
if ~flag
    return
end
% figure; plot(TxTimeHeave); grid on; title('TxTimeHeave');

[flag, xLog] = Read_BinVariable(Datagrams, nomDirSignal, 'xLog', 'single');
if ~flag
    return
end

[flag, cLog] = Read_BinVariable(Datagrams, nomDirSignal, 'cLog', 'single');
if ~flag
    return
end


% GLU : assemblage NRx par fusion des datagrammes du m�me Num�ro de Ping par Antenne.
if Head1.SystemSerialNumberSecondHead ~= 0
    NbSounders = 2;
else
    NbSounders = 1;
end

Data.NRx = [];
pMin     = min(PingCounter);
pMax     = max(PingCounter);
for ns=1:NbSounders
    if ns==1
        NumSerie = Head1.SystemSerialNumber;
    else
        NumSerie = Head1.SystemSerialNumberSecondHead;
    end
    subNumSounder = (SystemSerialNumber==NumSerie);
    for p=pMin:pMax
        subPing = (PingCounter(subNumSounder)==p);
        Data.NRx(p-pMin+1, ns) = sum(NRx(subPing));
    end
end

sub = (DatagramNumbers == 1);
Date         = Date1(sub);
NTx          = NTx(sub);
NbMilliSecs  = NbMilliSecs1(sub);
PingCounter  = PingCounter(sub);
SamplingFreq = SamplingFreq(sub);

% GLU TODO : SamplingFreq=NaN
if all(isnan(SamplingFreq))
    strFR = 'La fr�quence d''�chantillonnage n''est pas correcte. Veuillez v�rifier la donn�e !!!';
    strUS = 'Sampling Frequency is not available. Please, check our data. !!!';
    my_warndlg(Lang(strFR,strUS), 1);
    SamplingFreq(:) = 7.7593e3;
end

SoundSpeed          = SoundSpeed(sub);
SystemSerialNumber  = SystemSerialNumber(sub);
TotalOfReceiveBeams = TotalOfReceiveBeams(sub);
TxTimeHeave         = TxTimeHeave(sub);

[flag, RxBeamNumber] = Read_BinTable(Datagrams, nomDirSignal, 'RxBeamNumber', 'single', nbRx);
if ~flag
    return
end
% RxBeamNumber = RxBeamNumber + 1;
% figure; plot(RxBeamNumber, '+'); grid on; title('SamBeamNumberplingRate');


% Correct PingCounter in case of dual sounder but only the starboard is on
% (correspondances between pings are calculated on indice=1 on second
% dimension
NbDatagrams = length(Date);
Date                     = Kongsberg_table2signal(Date,               NbDatagrams, PingCounter, SystemSerialNumber, Head1);
NbMilliSecs              = Kongsberg_table2signal(NbMilliSecs,        NbDatagrams, PingCounter, SystemSerialNumber, Head1);
Data.PingCounter         = Kongsberg_table2signal(PingCounter,        NbDatagrams, PingCounter, SystemSerialNumber, Head1);
Data.SystemSerialNumber  = Kongsberg_table2signal(SystemSerialNumber, NbDatagrams, PingCounter, SystemSerialNumber, Head1);
Data.NumberOfDatagrams   = Kongsberg_table2signal(NumberOfDatagrams,  NbDatagrams, PingCounter, SystemSerialNumber, Head1);
Data.DatagramNumbers     = Kongsberg_table2signal(DatagramNumbers,    NbDatagrams, PingCounter, SystemSerialNumber, Head1);
Data.NTx                 = Kongsberg_table2signal(NTx,                NbDatagrams, PingCounter, SystemSerialNumber, Head1);
Data.TotalOfReceiveBeams = Kongsberg_table2signal(TotalOfReceiveBeams,NbDatagrams, PingCounter, SystemSerialNumber, Head1);
% GLU : voir plus haut : Data.NRx                    = Kongsberg_table2signal(NRx,                NbDatagrams, PingCounter, SystemSerialNumber, Head1);
Data.SoundSpeed          = Kongsberg_table2signal(SoundSpeed,         NbDatagrams, PingCounter, SystemSerialNumber, Head1);
Data.SamplingFreq        = Kongsberg_table2signal(SamplingFreq,       NbDatagrams, PingCounter, SystemSerialNumber, Head1);
Data.TxTimeHeave         = Kongsberg_table2signal(TxTimeHeave,        NbDatagrams, PingCounter, SystemSerialNumber, Head1);
Data.xLog                = Kongsberg_table2signal(xLog,               NbDatagrams, PingCounter, SystemSerialNumber, Head1);
Data.cLog                = Kongsberg_table2signal(cLog,               NbDatagrams, PingCounter, SystemSerialNumber, Head1);

pppp       = [0; cumsum(double(NRx))];
pppp       = pppp(sub);
NRx        = diff(pppp);
NRx(end+1)= NRx(end);
% for i=1:NbDatagrams
%    k = i - DatagramNumbers(i) + 1;
%    NRx(k) =  NRx(k) + (DatagramNumbers(i)-1)* NRx(i);
% end
% NRx                 = NRx(sub);

[flag, X] = Read_BinTable(Datagrams, nomDirSignal, 'TransmitSectorNumberTx', 'single', nbTx);
if ~flag
    return
end
TxBeamNumber = X + 1;
% Data.TxBeamNumber = Kongsberg_table2image(X, 'TransmitSectorNumberTx', NbDatagrams, max(NTx), ones(size(NTx)), NTx, PingCounter, SystemSerialNumber, Head1);
Data.TransmitSectorNumberTx = Kongsberg_table2image(X, 'TransmitSectorNumberTx', NbDatagrams, max(NTx), NTx, TxBeamNumber, PingCounter, SystemSerialNumber, Head1);
Data.TransmitSectorNumberTx = Data.TransmitSectorNumberTx + 1;
% figure; plot(Data.TxBeamNumber, '*'); title('TxBeamNumber'); grid on;
% NTx2 = Kongsberg_table2image(NTx, 'TransmitSectorNumber', NbDatagrams, max(NTx), ones(size(NTx)), NTx, PingCounter, SystemSerialNumber, Head1);

[flag, X] = Read_BinTable(Datagrams, nomDirSignal, 'TiltAngle', 'single', nbTx);
if ~flag
    return
end
% Data.TiltAngle = Kongsberg_table2image(X, 'TiltAngle', NbDatagrams, max(NTx), TxBeamNumber, NTx, PingCounter, SystemSerialNumber, Head1);
Data.TiltAngle = Kongsberg_table2image(X, 'TiltAngle', NbDatagrams, max(NTx), NTx, TxBeamNumber, PingCounter, SystemSerialNumber, Head1);
% figure; plot(Data.TiltAngle, '*'); title('TiltAngle'); grid on;

[flag, X] = Read_BinTable(Datagrams, nomDirSignal, 'CentralFrequency', 'single', nbTx);
if ~flag
    return
end
Data.CentralFrequency = Kongsberg_table2image(X, 'CentralFrequency', NbDatagrams, max(NTx), NTx, TxBeamNumber, PingCounter, SystemSerialNumber, Head1);
% figure; plot(Data.CentralFrequency, '*'); title('CentralFrequency'); grid on;

%% CORRECTION

% RxBeamNumber = RxBeamNumber + (DatagramNumbers-1) .* TotalOfReceiveBeams;
maxNRx = max(NRx);
switch Datagrams.Model
    case 3020
        RxBeamNumber = 1 + mod(0:length(RxBeamNumber)-1, 160);
    case {710; 122}
        RxBeamNumber = 1 + mod(0:length(RxBeamNumber)-1, maxNRx);
    case 302
        RxBeamNumber = 1 + mod(0:length(RxBeamNumber)-1, maxNRx);
    case 2040
        RxBeamNumber = 1 + mod(0:length(RxBeamNumber)-1, maxNRx);
    otherwise
        my_warndlg('Bug ici', 1);
end

%% BeamPointingAngle

[flag, X] = Read_BinTable(Datagrams, nomDirSignal, 'BeamPointingAngle', 'single', nbRx);
if ~flag
    return
end
Data.BeamPointingAngle = Kongsberg_table2image(X, 'BeamPointingAngle', NbDatagrams, ...
    maxNRx, NRx, RxBeamNumber, PingCounter, SystemSerialNumber, Head1);
% figure; imagesc(Data.BeamPointingAngle); title('BeamPointingAngle'); axis xy; colorbar;

%% StartRangeNumber

[flag, X] = Read_BinTable(Datagrams, nomDirSignal, 'StartRangeNumber', 'single', nbRx);
if ~flag
    return
end
Data.StartRangeNumber = Kongsberg_table2image(X, 'StartRangeNumber', NbDatagrams, ...
    maxNRx, NRx, RxBeamNumber, PingCounter, SystemSerialNumber, Head1);
% figure; imagesc(Data.StartRangeNumber); title('StartRangeNumber'); axis xy; colorbar;

%% NumberOfSamples

[flag, X] = Read_BinTable(Datagrams, nomDirSignal, 'NumberOfSamples', 'single', nbRx);
if ~flag
    return
end
Data.NumberOfSamples = Kongsberg_table2image(X, 'NumberOfSamples', NbDatagrams, ...
    maxNRx, NRx, RxBeamNumber, PingCounter, SystemSerialNumber, Head1);
% figure; imagesc(Data.NumberOfSamples); title('NumberOfSamples'); axis xy; colorbar;

%% DetectedRangeInSamples

[flag, X] = Read_BinTable(Datagrams, nomDirSignal, 'DetectedRangeInSamples', 'single', nbRx);
if ~flag
    return
end
Data.DetectedRangeInSamples = Kongsberg_table2image(X, 'DetectedRangeInSamples', NbDatagrams, ...
    maxNRx, NRx, RxBeamNumber, PingCounter, SystemSerialNumber, Head1);
% figure; imagesc(Data.DetectedRangeInSamples); title('DetectedRangeInSamples'); axis xy; colorbar;

%% TxBeamIndex

[flag, X] = Read_BinTable(Datagrams, nomDirSignal, 'TransmitSectorNumber', 'single', nbRx);
if ~flag
    return
end
[Data.TransmitSectorNumber, indexSounder] = Kongsberg_table2image(X, 'TransmitSectorNumber', NbDatagrams, ...
    maxNRx, NRx, RxBeamNumber, PingCounter, SystemSerialNumber, Head1);
%GLUGLUGLU for k=1:size(Data.TransmitSectorNumber,1)
%GLUGLUGLU %   Data.TransmitSectorNumber(k,:) =  1 + Data.TransmitSectorNumber(k,:) + indexSounder .* NTx2(k,1);
%GLUGLUGLU %   Data.TransmitSectorNumber(k,:) =  1 + Data.TransmitSectorNumber(k,:) + indexSounder .* NTx2(k,end);
%GLUGLUGLU    Data.TransmitSectorNumber(k,:) =  1 + Data.TransmitSectorNumber(k,:) + indexSounder .* NTx(k);
%GLUGLUGLU end
for k=1:min(size(Data.TransmitSectorNumber,1), length(NTx))
    Data.TransmitSectorNumber(k,:) =  1 + Data.TransmitSectorNumber(k,:) + indexSounder .* NTx(k);
end
for k=(min(size(Data.TransmitSectorNumber,1), length(NTx))+1):size(Data.TransmitSectorNumber,1)
    Data.TransmitSectorNumber(k,:) = NaN;
end
% figure; imagesc(Data.TransmitSectorNumber); title('TransmitSectorNumber'); axis xy; colorbar;

%% RxBeamIndex

Data.RxBeamNumber = Kongsberg_table2image(RxBeamNumber, 'RxBeamNumber', NbDatagrams, ...
    maxNRx, NRx, RxBeamNumber, PingCounter, SystemSerialNumber, Head1);
% figure; imagesc(Data.RxBeamNumber); title('RxBeamNumber'); axis xy; colorbar;

%{
% On tente de supprimer des faisceau abh�rents (fichier
% C:\KM_Ssc\MagAndPhase\SonarScope\0005_20100125_100015_ShipName)
Comment� car �a destructure tout, TODO voir avec GLU si pb de donn�es ou Convert_All
sub = (Data.TransmitSectorNumber > 16) | (Data.NumberOfSamples > 8000);
Data.TransmitSectorNumber(sub) = NaN;
Data.BeamPointingAngle(sub) = NaN;
Data.StartRangeNumber(sub) = NaN;
Data.NumberOfSamples(sub) = NaN;
Data.DetectedRangeInSamples(sub) = NaN;
Data.RxBeamNumber(sub) = NaN;
%}

%% SampleAmplitude

% sumNbSample = sum(double(Data.NumberOfSamples(:)));
sumNbSample = sum(double(Data.NumberOfSamples(:)), 'omitnan'); % Modif JMA le 12/06/2017 pour passer le fichier EM2040 Thalia U:\EM2040_Thalia\2013_02_01_Thalia_REM2040\DATA\EM2040\SIS_raw\ESSTECH2013_02 0085_20130204_141812_Thalia.all
[flag, X] = Read_BinTable(Datagrams, nomDirSignal, 'SampleAmplitude', 'single', sumNbSample);
if ~flag
    return
end
Data.SampleAmplitude = Kongsberg_table2image(X, 'SampleAmplitude', NbDatagrams, ...
    maxNRx, NRx, RxBeamNumber, PingCounter, SystemSerialNumber, Head1);
 %   figure; imagesc(Data.SampleAmplitude); title('SampleAmplitude'); axis xy; colorbar;

%% SamplePhase

[flag, X] = Read_BinTable(Datagrams, nomDirSignal, 'SamplePhase', 'single', sumNbSample);
if ~flag
    return
end
Data.SamplePhase = Kongsberg_table2image(X, 'Samplephase', NbDatagrams, ...
    maxNRx, NRx, RxBeamNumber, PingCounter, SystemSerialNumber, Head1);
 %   figure; imagesc(Data.SamplePhase); title('SamplePhase'); axis xy; colorbar;

 %{
for k=1:length(Datagrams.Tables)
    fprintf(1,'%d  %s  %s\n', k, Datagrams.Tables(k).Name, Datagrams.Tables(k).Indexation)
end
%}

%% Calcul des dates et heures

Jour  = mod(Date,100);
Date  = (Date - Jour) / 100;
Mois  = mod(Date,100);
Annee = (Date - Mois) / 100;
Date = dayJma2Ifr(Jour, Mois, Annee);
Data.Time = cl_time('timeIfr', Date, NbMilliSecs);

%% G�n�ration du XML SonarScope

Info.Title                  = 'RawBeamSamples';
Info.Constructor            = 'Kongsberg';
Info.EmModel                = Datagrams.Model;
Info.ListeSystemSerialNumber = num2str(unique(Data.SystemSerialNumber(:))');
Info.TimeOrigin             = '01/01/-4713';
Info.Comments               = 'Sounder ping rate';
Info.FormatVersion          = FormatVersion;

Info.Dimensions.nbPings       = size(Data.BeamPointingAngle, 1);
Info.Dimensions.nbRx          = size(Data.BeamPointingAngle, 2);
Info.Dimensions.nbSounders    = size(Data.PingCounter,2);
Info.Dimensions.nbTx          = max(NTx);

Info.Signals(1).Name          = 'Time';
Info.Signals(end).Dimensions  = 'nbPings, nbSounders';
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = 'days since JC';
Info.Signals(end).FileName    = fullfile('Ssc_RawBeamSamples', 'Time.bin');
Info.Signals(end).Tag         = verifKeyWord('SounderTime');

Info.Signals(end+1).Name      = 'PingCounter';
Info.Signals(end).Dimensions  = 'nbPings, nbSounders';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_RawBeamSamples', 'PingCounter.bin');
Info.Signals(end).Tag         = verifKeyWord('SounderPingCounter');

Info.Signals(end+1).Name      = 'SystemSerialNumber';
Info.Signals(end).Dimensions  = 'nbPings, nbSounders';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_RawBeamSamples', 'SystemSerialNumber.bin');
Info.Signals(end).Tag         = verifKeyWord('SounderSerialNumber');

Info.Signals(end+1).Name      = 'NumberOfDatagrams';
Info.Signals(end).Dimensions  = 'nbPings, nbSounders';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_RawBeamSamples', 'NumberOfDatagrams.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');
 
Info.Signals(end+1).Name      = 'DatagramNumbers';
Info.Signals(end).Dimensions  = 'nbPings, nbSounders';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_RawBeamSamples', 'DatagramNumbers.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');
 
Info.Signals(end+1).Name      = 'NTx';
Info.Signals(end).Dimensions  = 'nbPings, nbSounders';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'm';
Info.Signals(end).FileName    = fullfile('Ssc_RawBeamSamples', 'NTx.bin');
Info.Signals(end).Tag         = verifKeyWord('NTx');

Info.Signals(end+1).Name      = 'TotalOfReceiveBeams';
Info.Signals(end).Dimensions  = 'nbPings, nbSounders';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'm';
Info.Signals(end).FileName    = fullfile('Ssc_RawBeamSamples', 'TotalOfReceiveBeams.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'NRx';
Info.Signals(end).Dimensions  = 'nbPings, nbSounders';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'm';
Info.Signals(end).FileName    = fullfile('Ssc_RawBeamSamples', 'NRx.bin');
Info.Signals(end).Tag         = verifKeyWord('NRx');

Info.Signals(end+1).Name      = 'SoundSpeed';
Info.Signals(end).Dimensions  = 'nbPings, nbSounders';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'm/s';
Info.Signals(end).FileName    = fullfile('Ssc_RawBeamSamples', 'SoundSpeed.bin');
Info.Signals(end).Tag         = verifKeyWord('SounderSoundSpeed');     

Info.Signals(end+1).Name      = 'SamplingFreq';
Info.Signals(end).Dimensions  = 'nbPings, nbSounders';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'Hz';
Info.Signals(end).FileName    = fullfile('Ssc_RawBeamSamples', 'SamplingFreq.bin');
Info.Signals(end).Tag         = verifKeyWord('SounderSamplingRate');

Info.Signals(end+1).Name      = 'TxTimeHeave';
Info.Signals(end).Dimensions  = 'nbPings, nbSounders';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'cm';
Info.Signals(end).FileName    = fullfile('Ssc_RawBeamSamples', 'TxTimeHeave.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'xLog';
Info.Signals(end).Dimensions  = 'nbPings, nbSounders';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_RawBeamSamples', 'xLog.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'cLog';
Info.Signals(end).Dimensions  = 'nbPings, nbSounders';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_RawBeamSamples', 'cLog.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

% + Spare 1 

% Partie Tx

% + Spare ALignment

Info.Images(1).Name          = 'TiltAngle';
Info.Images(end).Dimensions  = 'nbPings, nbSounders';
Info.Images(end).Index       = 'nbPings, nbTx, nbSounders';
Info.Images(end).Storage     = 'single';
Info.Images(end).Unit        = 'deg';
Info.Images(end).FileName    = fullfile('Ssc_RawBeamSamples', 'TiltAngle.bin');
Info.Images(end).Tag         = verifKeyWord('SounderTiltAngle');

Info.Images(end+1).Name      = 'CentralFrequency';
Info.Images(end).Dimensions  = 'nbPings, nbSounders';
Info.Images(end).Index       = 'nbPings, nbTx, nbSounders';
Info.Images(end).Storage     = 'single';
Info.Images(end).Unit        = 'ms';
Info.Images(end).FileName    = fullfile('Ssc_RawBeamSamples', 'CentralFrequency.bin');
Info.Images(end).Tag         = verifKeyWord('TODO');

Info.Images(end+1).Name      = 'TransmitSectorNumberTx';
Info.Images(end).Dimensions  = 'nbPings, nbSounders';
Info.Images(end).Index       = 'nbPings, nbTx, nbSounders';
Info.Images(end).Storage     = 'single';
Info.Images(end).Unit        = '';
Info.Images(end).FileName    = fullfile('Ssc_RawBeamSamples', 'TransmitSectorNumberTx.bin');
Info.Images(end).Tag         = verifKeyWord('SounderTxBeamIndex');

% Partie Rx
Info.Images(end+1).Name       = 'BeamPointingAngle';
Info.Images(end).Dimensions   = 'nbPings, nbRx';
Info.Images(end).Index        = 'nbPings, nbRx, nbSounders';
Info.Images(end).Storage      = 'single';
Info.Images(end).Unit         = 'deg';
Info.Images(end).FileName     = fullfile('Ssc_RawBeamSamples', 'BeamPointingAngle.bin');
Info.Images(end).Tag          = verifKeyWord('SounderBeamAngle');

Info.Images(end+1).Name       = 'StartRangeNumber';
Info.Images(end).Dimensions   = 'nbPings, nbRx';
Info.Images(end).Index        = 'nbPings, nbRx, nbSounders';
Info.Images(end).Storage      = 'single';
Info.Images(end).Unit         = 'm';
Info.Images(end).FileName     = fullfile('Ssc_RawBeamSamples', 'StartRangeNumber.bin');
Info.Images(end).Tag          = verifKeyWord('TODO');

Info.Images(end+1).Name       = 'NumberOfSamples';
Info.Images(end).Dimensions   = 'nbPings, nbRx';
Info.Images(end).Index        = 'nbPings, nbRx, nbSounders';
Info.Images(end).Storage      = 'single';
Info.Images(end).Unit         = 'm';
Info.Images(end).FileName     = fullfile('Ssc_RawBeamSamples', 'NumberOfSamples.bin');
Info.Images(end).Tag          = verifKeyWord('TODO');

Info.Images(end+1).Name       = 'DetectedRangeInSamples';
Info.Images(end).Dimensions   = 'nbPings, nbRx';
Info.Images(end).Index        = 'nbPings, nbRx, nbSounders';
Info.Images(end).Storage      = 'single';
Info.Images(end).Unit         = 'm';
Info.Images(end).FileName     = fullfile('Ssc_RawBeamSamples', 'DetectedRangeInSamples.bin');
Info.Images(end).Tag          = verifKeyWord('TODO');
   
Info.Images(end+1).Name       = 'TransmitSectorNumber';
Info.Images(end).Dimensions   = 'nbPings, nbRx';
Info.Images(end).Index        = 'nbPings, nbRx, nbSounders';
Info.Images(end).Storage      = 'single';
Info.Images(end).Unit         = 'm';
Info.Images(end).FileName     = fullfile('Ssc_RawBeamSamples', 'TransmitSectorNumber.bin');
Info.Images(end).Tag          = verifKeyWord('SounderTxBeamIndex');

Info.Images(end+1).Name       = 'RxBeamNumber';
Info.Images(end).Dimensions   = 'nbPings, nbRx';
Info.Images(end).Index        = 'nbPings, nbRx, nbSounders';
Info.Images(end).Storage      = 'single';
Info.Images(end).Unit         = '';
Info.Images(end).FileName     = fullfile('Ssc_RawBeamSamples', 'RxBeamNumber.bin');
Info.Images(end).Tag          = verifKeyWord('TODO');

% Ajout des champs Scale_Factor, Add_Offset et Orig_Storage.
[flag, Info] = addScaleOffsetStorage(Datagrams, Info); %#ok<ASGLU>

% Signaux Amplitude et Phase
% Info.Images(end+1).Name       = 'SampleAmplitude';
% Info.Images(end).Dimensions   = 'nbPings, nbRx, nbSamples';
% Info.Images(end).Storage      = 'single';
% Info.Images(end).Unit         = '';
% Info.Images(end).FileName     = fullfile('Ssc_RawBeamSamples', 'SampleAmplitude.bin');
% Info.Images(end).Tag          = verifKeyWord('TODO');
% 
% Info.Images(end+1).Name       = 'SamplePhase';
% Info.Images(end).Dimensions   = 'nbPings, nbRx, nbSamples';
% Info.Images(end).Storage      = 'single';
% Info.Images(end).Unit         = '';
% Info.Images(end).FileName     = fullfile('Ssc_RawBeamSamples', 'SamplePhase.bin');
% Info.Images(end).Tag          = verifKeyWord('TODO');


%% Cr�ation du fichier XML d�crivant la donn�e 

nomFicXml = fullfile(nomDirRacine, 'Ssc_RawBeamSamples.xml');
xml_write(nomFicXml, Info);
flag = exist(nomFicXml, 'file');
if ~flag
    messageErreur(nomFicXml)
    return
end
% edit(nomFicXml)

%% Cr�ation du r�pertoire RawBeamSamples

nomDirRawBeamSamples = fullfile(nomDirRacine, 'Ssc_RawBeamSamples');
if ~exist(nomDirRawBeamSamples, 'dir')
    status = mkdir(nomDirRawBeamSamples);
    if ~status
        messageErreur(nomDirRawBeamSamples)
        flag = 0;
        return
    end
end

%% Cr�ation des fichiers binaires des signaux

for k=1:length(Info.Signals)
    flag = writeSignal(nomDirRacine, Info.Signals(k), Data.(Info.Signals(k).Name));
    if ~flag
        return
    end
end

%% Cr�ation des fichiers binaires des images

for k=1:length(Info.Images)
    flag = writeImage(nomDirRacine, Info.Images(k), Data.(Info.Images(k).Name));
    if ~flag
        return
    end
end

flag = 1;
