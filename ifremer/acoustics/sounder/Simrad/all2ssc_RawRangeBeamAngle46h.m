function flag = all2ssc_RawRangeBeamAngle46h(nomDirRacine, FormatVersion, Head1)

nomFicXml = fullfile(nomDirRacine, 'ALL_RawRangeBeamAngle46h.xml');
SonarFmt2ssc_message(nomFicXml, '.all')

%% Lecture du fichier XML d�crivant la donn�e 

flag = exist(nomFicXml, 'file');
if ~flag
    special_message('FichierNonExistant', nomFicXml)
    return
end
% edit(nomFicXml)
Datagrams = xml_read(nomFicXml);

[nomDirSignal, nomFicSignal] = fileparts(nomFicXml);
nomDirSignal = fullfile(nomDirSignal, nomFicSignal);

%{
for i=1:length(Datagrams.Variables)
    fprintf(1,'%d  %s\n', i, Datagrams.Variables(i).Name)
end
for i=1:length(Datagrams.Tables)
    fprintf(1,'%d  %s\n', i, Datagrams.Tables(i).Name)
end
%}

[flag, Date1] = Read_BinVariable(Datagrams, nomDirSignal, 'Date', 'double');
if ~flag
    return
end
% figure; plot(Date1); grid on; title('Date');

[flag, NbMilliSecs1] = Read_BinVariable(Datagrams, nomDirSignal, 'NbMilliSecs', 'double');
if ~flag
    return
end
% figure; plot(NbMilliSecs1); grid on; title('NbMilliSecs');

[flag, PingCounter] = Read_BinVariable(Datagrams, nomDirSignal, 'PingCounter', 'single');
if ~flag
    return
end
PingCounter = unwrapPingCounterSimrad(PingCounter);
% figure; plot(PingCounter); grid on; title('PingCounter');

[flag, SystemSerialNumber] = Read_BinVariable(Datagrams, nomDirSignal, 'SystemSerialNumber', 'single');
if ~flag
    return
end
% figure; plot(SystemSerialNumber, '+'); grid on; title('SystemSerialNumber');

[flag, MaxNbBeamsPossible] = Read_BinVariable(Datagrams, nomDirSignal, 'MaxNbBeamsPossible', 'single');
if ~flag
    return
end
% figure; plot(MaxNbBeamsPossible, '+'); grid on; title('MaxNbBeamsPossible');

[flag, SoundSpeed] = Read_BinVariable(Datagrams, nomDirSignal, 'SoundSpeed', 'single');
if ~flag
    return
end
% figure; plot(SoundSpeed); grid on; title('SoundSpeed');

[flag, NbBeams] = Read_BinVariable(Datagrams, nomDirSignal, 'NbBeams', 'single');
if ~flag
    return
end
% figure; plot(NbBeams); grid on; title('NbBeams');
nbRx = sum(NbBeams);

Date                    = Kongsberg_table2signal(Date1,              Datagrams.NbDatagrams, PingCounter, SystemSerialNumber, Head1);
NbMilliSecs             = Kongsberg_table2signal(NbMilliSecs1,       Datagrams.NbDatagrams, PingCounter, SystemSerialNumber, Head1);
Data.PingCounter        = Kongsberg_table2signal(PingCounter,        Datagrams.NbDatagrams, PingCounter, SystemSerialNumber, Head1);
Data.MaxNbBeamsPossible = Kongsberg_table2signal(MaxNbBeamsPossible, Datagrams.NbDatagrams, PingCounter, SystemSerialNumber, Head1);
Data.NbBeams            = Kongsberg_table2signal(NbBeams,            Datagrams.NbDatagrams, PingCounter, SystemSerialNumber, Head1);
Data.SystemSerialNumber = Kongsberg_table2signal(SystemSerialNumber, Datagrams.NbDatagrams, PingCounter, SystemSerialNumber, Head1);
Data.SoundSpeed         = Kongsberg_table2signal(SoundSpeed,         Datagrams.NbDatagrams, PingCounter, SystemSerialNumber, Head1);

% D�but rajout JMA le 20/09/2016 avec mise en commentaire Kongsberg_table2signal
subNaN = find(isnan(sum(Data.PingCounter,2)));
Date(subNaN,:)                    = [];
NbMilliSecs(subNaN,:)             = [];
Data.PingCounter(subNaN,:)        = [];
Data.MaxNbBeamsPossible(subNaN,:) = [];
Data.NbBeams(subNaN,:)            = [];
Data.SystemSerialNumber(subNaN,:) = [];
Data.SoundSpeed(subNaN,:)         = [];
% Fin rajout JMA le 20/09/2016 avec mise en commentaire Kongsberg_table2signal

MaxDesMaxNbBeams = max(MaxNbBeamsPossible);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% V�rifier quand NTx ~= 1 : EM300
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Lecture des Table
[flag, BeamNumber] = Read_BinTable(Datagrams, nomDirSignal, 'BeamNumber', 'single', nbRx);
if ~flag
    return
end
BeamNumber        = BeamNumber + 1;
% figure; plot(BeamNumber, '+'); grid on; title('SamBeamNumberplingRate');
Data.BeamNumber = Kongsberg_table2image(BeamNumber-1, 'BeamNumber', Datagrams.NbDatagrams, ...
    MaxDesMaxNbBeams, NbBeams, BeamNumber, PingCounter, SystemSerialNumber, Head1);
Data.BeamNumber(subNaN,:) = [];

[flag, X] = Read_BinTable(Datagrams, nomDirSignal, 'BeamPointingAngle', 'single', nbRx);
if ~flag
    return
end
Data.BeamPointingAngle = Kongsberg_table2image(X, 'BeamPointingAngle', Datagrams.NbDatagrams, ...
    MaxDesMaxNbBeams, NbBeams, BeamNumber, PingCounter, SystemSerialNumber, Head1);
Data.BeamPointingAngle(subNaN,:) = [];
% figure; imagesc(Data.BeamPointingAngle); title('BeamPointingAngle'); axis xy; colorbar;

[flag, X] = Read_BinTable(Datagrams, nomDirSignal, 'TransmitTiltAngle', 'single', nbRx);
if ~flag
    return
end
Data.TransmitTiltAngle = Kongsberg_table2image(X, 'TransmitTiltAngle', Datagrams.NbDatagrams, ...
    MaxDesMaxNbBeams, NbBeams, BeamNumber, PingCounter, SystemSerialNumber, Head1);
Data.TransmitTiltAngle(subNaN,:) = [];
% figure; imagesc(Data.TransmitTiltAngle); title('TransmitTiltAngle'); axis xy; colorbar;

[flag, X] = Read_BinTable(Datagrams, nomDirSignal, 'Range', 'single', nbRx);
if ~flag
    return
end
Data.Range = Kongsberg_table2image(X, 'Range', Datagrams.NbDatagrams, ...
    MaxDesMaxNbBeams, NbBeams, BeamNumber, PingCounter, SystemSerialNumber, Head1);
Data.Range(subNaN,:) = [];
% switch Datagrams.Model
%     case 1002
%     otherwise
%         % Ce n'est pas fait dans lecture datagrammes Depth
%         my_warndlg('Message for JMA : all2ssc_RawRangeBeamAngle46h,  Data.Range = Data.Range / 2; Pas pour EM1002, Faut-il le supprimer aussi pour les autres sondeurs ?', 0, 'Tag', 'Data.Range = Data.Range / 2;')
%         Data.Range = Data.Range / 2;
% end
% figure; imagesc(Data.Range); title('Range'); axis xy; colorbar;

%%  Reflectivity

[flag, X] = Read_BinTable(Datagrams, nomDirSignal, 'Reflectivity', 'single', nbRx);
if ~flag
    return
end
Data.Reflectivity = Kongsberg_table2image(X, 'Reflectivity', Datagrams.NbDatagrams, ...
    MaxDesMaxNbBeams, NbBeams, BeamNumber, PingCounter, SystemSerialNumber, Head1);
Data.Reflectivity(subNaN,:) = [];
% figure; imagesc(Data.Reflectivity); title('Reflectivity'); axis xy; colorbar;

%{
for i=1:length(Datagrams.Tables)
    fprintf(1,'%d  %s  %s\n', i, Datagrams.Tables(i).Name, Datagrams.Tables(i).Indexation)
end
%}

%% Calcul des dates et heures

Jour  = mod(Date,100);
Date  = (Date - Jour) / 100;
Mois  = mod(Date,100);
Annee = (Date - Mois) / 100;
Date = dayJma2Ifr(Jour, Mois, Annee);
Data.Time = cl_time('timeIfr', Date, NbMilliSecs);

%% G�n�ration du XML SonarScope

Info.Title                  = 'RawRangeBeamAngle';
Info.Constructor            = 'Kongsberg';
Info.EmModel                = Datagrams.Model;
Info.ListeSystemSerialNumber = num2str(unique(Data.SystemSerialNumber(:))');
% Info.nbPings                = size(Data.Range, 1);
% Info.nbRx                   = size(Data.Range, 2);
% Info.nbSounders             = size(Data.PingCounter,2);
Info.TimeOrigin             = '01/01/-4713';
Info.Comments               = 'Sounder ping rate';
Info.Version                = 'V1';
Info.FormatVersion          = FormatVersion;

Info.Dimensions.nbPings       = size(Data.Range, 1);
Info.Dimensions.nbRx          = size(Data.Range, 2);
Info.Dimensions.nbSounders    = size(Data.PingCounter,2);

Info.Signals(1).Name          = 'Time';
Info.Signals(end).Dimensions  = 'nbPings, nbSounders';
Info.Signals(1).Storage       = 'double';
Info.Signals(1).Unit          = 'days since JC';
Info.Signals(1).FileName      = fullfile('Ssc_RawRangeBeamAngle', 'Time.bin');
Info.Signals(1).Tag           = verifKeyWord('SounderTime');

Info.Signals(end+1).Name      = 'PingCounter';
Info.Signals(end).Dimensions  = 'nbPings, nbSounders';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_RawRangeBeamAngle', 'PingCounter.bin');
Info.Signals(end).Tag         = verifKeyWord('SounderPingCounter');

Info.Signals(end+1).Name      = 'MaxNbBeamsPossible';
Info.Signals(end).Dimensions  = 'nbPings, nbSounders';
Info.Signals(end).Storage     = 'uint8';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_RawRangeBeamAngle', 'MaxNbBeamsPossible.bin');
Info.Signals(end).Tag         = verifKeyWord('SounderNbBeams');
     
Info.Signals(end+1).Name      = 'NbBeams';
Info.Signals(end).Dimensions  = 'nbPings, nbSounders';
Info.Signals(end).Storage     = 'uint8';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_RawRangeBeamAngle', 'NbBeams.bin');
Info.Signals(end).Tag         = verifKeyWord('SounderNbBeams');
     
Info.Signals(end+1).Name      = 'SoundSpeed';
Info.Signals(end).Dimensions  = 'nbPings, nbSounders';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'm/s';
Info.Signals(end).FileName    = fullfile('Ssc_RawRangeBeamAngle', 'SoundSpeed.bin');
Info.Signals(end).Tag         = verifKeyWord('SounderSoundSpeed');
     
Info.Images(1).Name           = 'BeamPointingAngle';
Info.Images(end).Dimensions   = 'nbPings, nbRx';
Info.Images(end).Index   	  = 'nbPings, NbBeams';
Info.Images(end).Storage      = 'single';
Info.Images(end).Unit         = 'deg';
Info.Images(end).FileName     = fullfile('Ssc_RawRangeBeamAngle', 'BeamPointingAngle.bin');
Info.Images(end).Tag          = verifKeyWord('SounderBeamAngle');

Info.Images(end+1).Name       = 'TransmitTiltAngle';
Info.Images(end).Dimensions   = 'nbPings, nbRx';
Info.Images(end).Index   	  = 'nbPings, NbBeams';
Info.Images(end).Storage      = 'single';
Info.Images(end).Unit         = '';
Info.Images(end).FileName     = fullfile('Ssc_RawRangeBeamAngle', 'TransmitTiltAngle.bin');
Info.Images(end).Tag          = verifKeyWord('SounderTxTiltAngle');

Info.Images(end+1).Name       = 'Range';
Info.Images(end).Dimensions   = 'nbPings, nbRx';
Info.Images(end).Index   	  = 'nbPings, NbBeams';
Info.Images(end).Storage      = 'single';
Info.Images(end).Unit         = 'samples';
Info.Images(end).FileName     = fullfile('Ssc_RawRangeBeamAngle', 'Range.bin');
Info.Images(end).Tag          = verifKeyWord('SounderDetectionNbSamples');

Info.Images(end+1).Name       = 'Reflectivity';
Info.Images(end).Dimensions   = 'nbPings, nbRx';
Info.Images(end).Index   	  = 'nbPings, NbBeams';
Info.Images(end).Storage      = 'single';
Info.Images(end).Unit         = 'm';
Info.Images(end).FileName     = fullfile('Ssc_RawRangeBeamAngle', 'Reflectivity.bin');
Info.Images(end).Tag          = verifKeyWord('SounderReflectivity');

Info.Images(end+1).Name       = 'BeamNumber';
Info.Images(end).Dimensions   = 'nbPings, nbRx';
Info.Images(end).Index   	  = 'nbPings, NbBeams';
Info.Images(end).Storage      = 'single';
Info.Images(end).Unit         = '';
Info.Images(end).FileName     = fullfile('Ssc_RawRangeBeamAngle', 'BeamNumber.bin');
Info.Images(end).Tag          = verifKeyWord('SounderBeamNumber');

% Ajout des champs Scale_Factor, Add_Offset et Orig_Storage.
[flag, Info] = addScaleOffsetStorage(Datagrams, Info);

%% Cr�ation du fichier XML d�crivant la donn�e 

nomFicXml = fullfile(nomDirRacine, 'Ssc_RawRangeBeamAngle.xml');
xml_write(nomFicXml, Info);
flag = exist(nomFicXml, 'file');
if ~flag
    messageErreur(nomFicXml)
    return
end
% edit(nomFicXml)

%% Cr�ation du r�pertoire Ssc_RawRangeBeamAngle

nomDirRawRangeBeamAngle = fullfile(nomDirRacine, 'Ssc_RawRangeBeamAngle');
if ~exist(nomDirRawRangeBeamAngle, 'dir')
    status = mkdir(nomDirRawRangeBeamAngle);
    if ~status
        messageErreur(nomDirRawRangeBeamAngle)
        flag = 0;
        return
    end
end

%% Cr�ation des fichiers binaires des signaux

for i=1:length(Info.Signals)
    flag = writeSignal(nomDirRacine, Info.Signals(i), Data.(Info.Signals(i).Name));
    if ~flag
        return
    end
end

%% Cr�ation des fichiers binaires des images

for i=1:length(Info.Images)
    flag = writeImage(nomDirRacine, Info.Images(i), Data.(Info.Images(i).Name));
    if ~flag
        return
    end
end

flag = 1;
