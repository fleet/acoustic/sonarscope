function flag = all2ssc_RawRangeBeamAngle4eh(nomDirRacine, FormatVersion, Head1, SounderDualOneTxAntenna)

nomFicXml = fullfile(nomDirRacine, 'ALL_RawRangeBeamAngle4eh.xml');
SonarFmt2ssc_message(nomFicXml, '.all')

%% Lecture du fichier XML d�crivant la donn�e 

flag = exist(nomFicXml, 'file');
if ~flag
    special_message('FichierNonExistant', nomFicXml)
    return
end
% edit(nomFicXml)
Datagrams = xml_read(nomFicXml);

[nomDirSignal, nomFicSignal] = fileparts(nomFicXml);
nomDirSignal = fullfile(nomDirSignal, nomFicSignal);

%{
for k=1:length(Datagrams.Variables)
    fprintf(1,'%d  %s\n', k, Datagrams.Variables(k).Name)
end
for k=1:length(Datagrams.Tables)
    fprintf(1,'%d  %s\n', k, Datagrams.Tables(k).Name)
end
%}

[flag, Date1] = Read_BinVariable(Datagrams, nomDirSignal, 'Date', 'double');
if ~flag
    return
end
% figure; plot(Date1); grid on; title('Date');

[flag, NbMilliSecs1] = Read_BinVariable(Datagrams, nomDirSignal, 'NbMilliSecs', 'double');
if ~flag
    return
end
% figure; plot(NbMilliSecs1); grid on; title('NbMilliSecs');

[flag, PingCounter] = Read_BinVariable(Datagrams, nomDirSignal, 'PingCounter', 'single');
if ~flag
    return
end
PingCounter = unwrapPingCounterSimrad(PingCounter);
% figure; plot(PingCounter); grid on; title('PingCounter');

[flag, SystemSerialNumber] = Read_BinVariable(Datagrams, nomDirSignal, 'SystemSerialNumber', 'single');
if ~flag
    return
end
% figure; plot(SystemSerialNumber, '+'); grid on; title('SystemSerialNumber');

% [flag, MaxNbBeamsPossible] = Read_BinVariable(Datagrams, nomDirSignal, 'MaxNbBeamsPossible', 'single');
% if ~flag
%     return
% end
% %figure; plot(MaxNbBeamsPossible, '+'); grid on; title('MaxNbBeamsPossible');

[flag, SoundSpeed] = Read_BinVariable(Datagrams, nomDirSignal, 'SoundSpeed', 'single');
if ~flag
    return
end
% figure; plot(SoundSpeed); grid on; title('SoundSpeed');

[flag, SamplingRate] = Read_BinVariable(Datagrams, nomDirSignal, 'SamplingRate', 'single');
if ~flag
    return
end
% figure; plot(SamplingRate); grid on; title('SamplingRate');

[flag, NTx] = Read_BinVariable(Datagrams, nomDirSignal, 'NTx', 'single');
if ~flag
    return
end
% figure; plot(NTx); grid on; title('NTx');
nbTx = sum(NTx);

[flag, NRx] = Read_BinVariable(Datagrams, nomDirSignal, 'NRx', 'single');
if ~flag
    return
end
% figure; plot(NRx); grid on; title('NRx');
nbRx = sum(NRx);

[flag, NbBeams] = Read_BinVariable(Datagrams, nomDirSignal, 'NbBeams', 'single');
if ~flag
    return
end
% figure; plot(NbBeams); grid on; title('NbBeams');

% nbSamples = sum(NRx);
% RxBeamNumber = zeros(1,nbSamples);
% iDeb = 0;
% for k=1:Datagrams.NbDatagrams
%     sub = 1:NRx(k);
%     RxBeamNumber(iDeb + sub) = sub;
%     iDeb = iDeb + NRx(k);
% end
RxBeamNumber = [];

% TxBeamNumber = zeros(1,sum(NTx));
% iDeb = 0;
% for k=1:Datagrams.NbDatagrams
%     sub = 1:NTx(k);
%     TxBeamNumber(iDeb + sub) = sub;
%     iDeb = iDeb + NTx(k);
% end
TxBeamNumber = [];

Date                    = Kongsberg_table2signal(Date1,              Datagrams.NbDatagrams, PingCounter, SystemSerialNumber, Head1);
NbMilliSecs             = Kongsberg_table2signal(NbMilliSecs1,       Datagrams.NbDatagrams, PingCounter, SystemSerialNumber, Head1);
Data.PingCounter        = Kongsberg_table2signal(PingCounter,        Datagrams.NbDatagrams, PingCounter, SystemSerialNumber, Head1);
Data.SystemSerialNumber = Kongsberg_table2signal(SystemSerialNumber, Datagrams.NbDatagrams, PingCounter, SystemSerialNumber, Head1);
Data.SoundSpeed         = Kongsberg_table2signal(SoundSpeed,         Datagrams.NbDatagrams, PingCounter, SystemSerialNumber, Head1);
Data.SamplingRate       = Kongsberg_table2signal(SamplingRate,       Datagrams.NbDatagrams, PingCounter, SystemSerialNumber, Head1);
Data.NbBeams            = Kongsberg_table2signal(NbBeams,            Datagrams.NbDatagrams, PingCounter, SystemSerialNumber, Head1);

% D�but rajout JMA le 20/09/2016 avec mise en commentaire Kongsberg_table2signal
subNaN = find(isnan(sum(Data.PingCounter,2)));
Date(subNaN,:)                    = [];
NbMilliSecs(subNaN,:)             = [];
Data.PingCounter(subNaN,:)        = [];
Data.SystemSerialNumber(subNaN,:) = [];
Data.SoundSpeed(subNaN,:)         = [];
Data.SamplingRate(subNaN,:)       = [];
Data.NbBeams(subNaN,:)            = [];
% Fin rajout JMA le 20/09/2016 avec mise en commentaire Kongsberg_table2signal


%% TiltAngle 

[flag, X] = Read_BinTable(Datagrams, nomDirSignal, 'TiltAngle', 'single', nbTx);
if ~flag
    return
end
Data.TxArray.TiltAngle = Kongsberg_table2image(X, 'TiltAngle', Datagrams.NbDatagrams, ...
    max(NTx), NTx, TxBeamNumber, PingCounter, SystemSerialNumber, Head1);
Data.TxArray.TiltAngle(subNaN,:)            = [];
% figure; imagesc(Data.TxArray.TiltAngle); title('TiltAngle'); axis xy; colorbar;

%% FocusRange 

[flag, X] = Read_BinTable(Datagrams, nomDirSignal, 'FocusRange', 'single', nbTx);
if ~flag
    return
end
Data.TxArray.FocusRange = Kongsberg_table2image(X, 'FocusRange', Datagrams.NbDatagrams, ...
    max(NTx), NTx, TxBeamNumber, PingCounter, SystemSerialNumber, Head1);
Data.TxArray.FocusRange(subNaN,:) = [];
% figure; imagesc(Data.TxArray.FocusRange); title('FocusRange'); axis xy; colorbar;

%% SignalLength 

[flag, X] = Read_BinTable(Datagrams, nomDirSignal, 'SignalLength', 'single', nbTx);
if ~flag
    return
end
Data.TxArray.SignalLength = Kongsberg_table2image(X, 'SignalLength', Datagrams.NbDatagrams, ...
    max(NTx), NTx, TxBeamNumber, PingCounter, SystemSerialNumber, Head1);
Data.TxArray.SignalLength(subNaN,:) = [];
% figure; imagesc(Data.TxArray.SignalLength); title('SignalLength (s)'); axis xy; colorbar;

%% SectorTransmitDelay 

[flag, X] = Read_BinTable(Datagrams, nomDirSignal, 'SectorTransmitDelay', 'single', nbTx);
if ~flag
    return
end
Data.TxArray.SectorTransmitDelay = 1000 * Kongsberg_table2image(X, 'SectorTransmitDelay', Datagrams.NbDatagrams, ...
    max(NTx), NTx, TxBeamNumber, PingCounter, SystemSerialNumber, Head1);
Data.TxArray.SectorTransmitDelay(subNaN,:) = [];
% figure; imagesc(Data.TxArray.SectorTransmitDelay); title('SectorTransmitDelay'); axis xy; colorbar;

%% CentralFrequency 

[flag, X] = Read_BinTable(Datagrams, nomDirSignal, 'CentralFrequency', 'single', nbTx);
if ~flag
    return
end
Data.TxArray.CentralFrequency = Kongsberg_table2image(X, 'CentralFrequency', Datagrams.NbDatagrams, ...
    max(NTx), NTx, TxBeamNumber, PingCounter, SystemSerialNumber, Head1);
Data.TxArray.CentralFrequency(subNaN,:) = [];
% figure; imagesc(Data.TxArray.CentralFrequency); title('CentralFrequency'); axis xy; colorbar;

%% MeanAbsorptionCoeff 

[flag, X] = Read_BinTable(Datagrams, nomDirSignal, 'MeanAbsorptionCoeff', 'single', nbTx);
if ~flag
    return
end
Data.TxArray.MeanAbsorptionCoeff = Kongsberg_table2image(X, 'MeanAbsorptionCoeff', Datagrams.NbDatagrams, ...
    max(NTx), NTx, TxBeamNumber, PingCounter, SystemSerialNumber, Head1);
Data.TxArray.MeanAbsorptionCoeff(subNaN,:) = [];
Data.TxArray.MeanAbsorptionCoeff(Data.TxArray.MeanAbsorptionCoeff == 0) = NaN;
% figure; imagesc(Data.TxArray.MeanAbsorptionCoeff); title('MeanAbsorptionCoeff'); axis xy; colorbar;

%% SignalWaveformIdentifier 

[flag, X] = Read_BinTable(Datagrams, nomDirSignal, 'SignalWaveformIdentifier', 'single', nbTx);
if ~flag
    return
end
Data.TxArray.SignalWaveformIdentifier = Kongsberg_table2image(X, 'SignalWaveformIdentifier', Datagrams.NbDatagrams, ...
    max(NTx), NTx, TxBeamNumber, PingCounter, SystemSerialNumber, Head1);
Data.TxArray.SignalWaveformIdentifier(subNaN,:) = [];
% figure; imagesc(Data.TxArray.SignamWaveformIdentifier); title('SignamWaveformIdentifier'); axis xy; colorbar;

%% TransmitSectorNumberTx

[flag, X] = Read_BinTable(Datagrams, nomDirSignal, 'TransmitSectorNumberTx', 'single', nbTx);
if ~flag
    return
end
Data.TxArray.TransmitSectorNumberTx = Kongsberg_table2image(X, 'TransmitSectorNumberTx', Datagrams.NbDatagrams, ...
    max(NTx), NTx, TxBeamNumber, PingCounter, SystemSerialNumber, Head1);
%Data.TxArray.TransmitSectorNumberTx(:,2)=1;
Data.TxArray.TransmitSectorNumberTx(subNaN,:) = [];
Data.TxArray.TransmitSectorNumberTx = Data.TxArray.TransmitSectorNumberTx + 1;
% figure; imagesc(Data.TxArray.TransmitSectorNumberTx); title('TransmitSectorNumberTx'); axis xy; colorbar;

%% SignalBandwith

[flag, X] = Read_BinTable(Datagrams, nomDirSignal, 'SignalBandwith', 'single', nbTx);
if ~flag
    return
end
Data.TxArray.SignalBandwith = Kongsberg_table2image(X, 'SignalBandwith', Datagrams.NbDatagrams, ...
    max(NTx), NTx, TxBeamNumber, PingCounter, SystemSerialNumber, Head1);
Data.TxArray.SignalBandwith(subNaN,:) = [];
% figure; imagesc(Data.TxArray.SignalBandwith); title('SignalBandwith'); axis xy; colorbar;



% TODO : runtine pour sondeur EM2040 dual avec une seule t�te d'�mission.
% J'ai r�solu ce pb de mani�re plus �l�gante sur Sector number je crois
% mais je ne retrouve pas o� exactement : chercher et remplacer ce code.
if SounderDualOneTxAntenna % TODO GLU : pb CONVERT_ALL.exe ???
    n = size(Data.TxArray.TiltAngle,2);
    sub = floor(1+n/2):n;
    Data.TxArray.TiltAngle(:,sub)                = [];
    Data.TxArray.FocusRange(:,sub)               = [];
    Data.TxArray.SignalLength(:,sub)             = [];
    Data.TxArray.SectorTransmitDelay(:,sub) 	 = [];
    Data.TxArray.CentralFrequency(:,sub)         = [];
    Data.TxArray.MeanAbsorptionCoeff(:,sub)      = [];
    Data.TxArray.SignalWaveformIdentifier(:,sub) = [];
    Data.TxArray.TransmitSectorNumberTx(:,sub)   = [];
    Data.TxArray.SignalBandwith(:,sub)           = [];
    
    %{
    % Rajout JMA le 17/04/2018 pour fichiers EM2040 du Simon Stevins (0004_20180307_130102_SimonStevin.all)
    try
        Data.PingCounter(:,sub) = [];
        Data.SystemSerialNumber(:,sub) = [];
        Data.SoundSpeed(:,sub) = [];
        Data.SamplingRate(:,sub) = [];
        Data.NbBeams(:,sub) = [];
    catch
    end
    %}
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% V�rifier quand NTx ~= 1 : EM300
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% BeamPointingAngle

[flag, X] = Read_BinTable(Datagrams, nomDirSignal, 'BeamPointingAngle', 'single', nbRx);
if ~flag
    return
end
Data.RxArray.BeamPointingAngle = Kongsberg_table2image(X, 'BeamPointingAngle', Datagrams.NbDatagrams, ...
    max(NRx), NRx, RxBeamNumber, PingCounter, SystemSerialNumber, Head1);
Data.RxArray.BeamPointingAngle(subNaN,:) = [];
Data.RxArray.BeamPointingAngle = -Data.RxArray.BeamPointingAngle;
% figure; imagesc(Data.RxArray.BeamPointingAngle); title('BeamPointingAngle'); axis xy; colorbar;

%% TransmitSectorNumberRx

[flag, X] = Read_BinTable(Datagrams, nomDirSignal, 'TransmitSectorNumberRx', 'single', nbRx);
if ~flag
    return
end
Data.RxArray.TransmitSectorNumberRx = Kongsberg_table2image(X, 'TransmitSectorNumberRx', Datagrams.NbDatagrams, ...
    max(NRx), NRx, RxBeamNumber, PingCounter, SystemSerialNumber, Head1);
Data.RxArray.TransmitSectorNumberRx(subNaN,:) = [];
% figure; imagesc(Data.RxArray.TransmitSectorNumberRx(:,:)); title('TransmitSectorNumber'); axis xy; colorbar;

%% DetectionInfo

[flag, X] = Read_BinTable(Datagrams, nomDirSignal, 'DetectionInfo', 'single', nbRx);
if ~flag
    return
end
Data.RxArray.DetectionInfo = Kongsberg_table2image(X, 'DetectionInfo', Datagrams.NbDatagrams, ...
    max(NRx), NRx, RxBeamNumber, PingCounter, SystemSerialNumber, Head1);
Data.RxArray.DetectionInfo(subNaN,:) = [];
% figure; imagesc(Data.DetectionInfo); title('DetectionInfo'); axis xy; colorbar;

%% LengthDetection

[flag, X] = Read_BinTable(Datagrams, nomDirSignal, 'LengthDetection', 'single', nbRx);
if ~flag
    return
end
Data.RxArray.LengthDetection = Kongsberg_table2image(X, 'LengthDetection', Datagrams.NbDatagrams, ...
    max(NRx), NRx, RxBeamNumber, PingCounter, SystemSerialNumber, Head1);
Data.RxArray.LengthDetection(subNaN,:) = [];
% figure; imagesc(Data.LengthDetection); title('LengthDetection'); axis xy; colorbar;

%% QualityFactor

[flag, X] = Read_BinTable(Datagrams, nomDirSignal, 'QualityFactor', 'single', nbRx);
if ~flag
    return
end
Data.RxArray.QualityFactor = Kongsberg_table2image(X, 'QualityFactor', Datagrams.NbDatagrams, ...
    max(NRx), NRx, RxBeamNumber, PingCounter, SystemSerialNumber, Head1);
Data.RxArray.QualityFactor(subNaN,:) = [];
% figure; imagesc(Data.QualityFactor); title('QualityFactor'); axis xy; colorbar;

%% TwoWayTravelTimeInSeconds

[flag, X] = Read_BinTable(Datagrams, nomDirSignal, 'TwoWayTravelTime', 'single', nbRx);
if ~flag
    return
end
Data.RxArray.TwoWayTravelTime = Kongsberg_table2image(X, 'TwoWayTravelTime', Datagrams.NbDatagrams, ...
    max(NRx), NRx, RxBeamNumber, PingCounter, SystemSerialNumber, Head1);
Data.RxArray.TwoWayTravelTime(subNaN,:) = [];
% figure; imagesc(Data.RxArray.TwoWayTravelTime); title('TwoWayTravelTime'); axis xy; colorbar; colormap jet

%% Reflectivity

[flag, X] = Read_BinTable(Datagrams, nomDirSignal, 'Reflectivity', 'single', nbRx);
if ~flag
    return
end
Data.RxArray.Reflectivity = Kongsberg_table2image(X, 'Reflectivity', Datagrams.NbDatagrams, ...
    max(NRx), NRx, RxBeamNumber, PingCounter, SystemSerialNumber, Head1);
Data.RxArray.Reflectivity(subNaN,:) = [];
% figure; imagesc(Data.Reflectivity); title('Reflectivity'); axis xy; colorbar;

%% RealTimeCleaningInfo

[flag, X] = Read_BinTable(Datagrams, nomDirSignal, 'RealTimeCleaningInfo', 'int8', nbRx);
if ~flag
    return
end
Data.RxArray.RealTimeCleaningInfo = Kongsberg_table2image(X, 'RealTimeCleaningInfo', Datagrams.NbDatagrams, ...
    max(NRx), NRx, RxBeamNumber, PingCounter, SystemSerialNumber, Head1);
Data.RxArray.RealTimeCleaningInfo(subNaN,:) = [];
% figure; imagesc(Data.RealTimeCleaningInfo); title('RealTimeCleaningInfo'); axis xy; colorbar;

%% Calcul des dates et heures

Date = Date(:,1);
NbMilliSecs = NbMilliSecs(:,1);

Jour  = mod(Date,100);
Date  = (Date - Jour) / 100;
Mois  = mod(Date,100);
Annee = (Date - Mois) / 100;
Date = dayJma2Ifr(Jour, Mois, Annee);
Data.Time = cl_time('timeIfr', Date, NbMilliSecs);

%% Mise � NaN des sondes non valides

sub = (Data.RxArray.DetectionInfo  > 127);
Data.RxArray.BeamPointingAngle(sub)      = NaN;
Data.RxArray.TransmitSectorNumberRx(sub) = NaN;
Data.RxArray.RealTimeCleaningInfo(sub)   = 0;
Data.RxArray.LengthDetection(sub)        = NaN;
Data.RxArray.QualityFactor(sub)          = NaN;
Data.RxArray.TwoWayTravelTime(sub)       = NaN;
Data.RxArray.Reflectivity(sub)           = NaN;

%% Cre�ation secteurs suppl�mentaires pour EM2040D � cause des bscorr qui distinguent b�bord et tribord

if (size(Data.TxArray.TransmitSectorNumberTx, 2) == 1) && all(Data.TxArray.TransmitSectorNumberTx == 2)
    Data.TxArray.TransmitSectorNumberTx(:) = 1; % Ajout JMA le 16/03/2022 pour fichier BScorrected_0010_20220204_064127_Belgica.all
end

nbBeams = size(Data.RxArray.TransmitSectorNumberRx, 2);
subTri = (nbBeams/2+1):nbBeams;
for k=1:size(Data.RxArray.TwoWayTravelTime, 1)
    [~, kTx] = sort(Data.TxArray.TransmitSectorNumberTx(k,:));
    Data.TxArray.TransmitSectorNumberTx(k,:)   = Data.TxArray.TransmitSectorNumberTx(k,kTx);
    Data.TxArray.TiltAngle(k,:)                = Data.TxArray.TiltAngle(k,kTx);
    Data.TxArray.FocusRange(k,:)               = Data.TxArray.FocusRange(k,kTx);
    Data.TxArray.SignalLength(k,:)             = Data.TxArray.SignalLength(k,kTx);
    Data.TxArray.SectorTransmitDelay(k,:)      = Data.TxArray.SectorTransmitDelay(k,kTx);
    Data.TxArray.CentralFrequency(k,:)         = Data.TxArray.CentralFrequency(k,kTx);
    Data.TxArray.MeanAbsorptionCoeff(k,:)      = Data.TxArray.MeanAbsorptionCoeff(k,kTx);
    Data.TxArray.SignalWaveformIdentifier(k,:) = Data.TxArray.SignalWaveformIdentifier(k,kTx);
    Data.TxArray.SignalBandwith(k,:)           = Data.TxArray.SignalBandwith(k,kTx);
end

if SounderDualOneTxAntenna % SI SONDEUR DUAL HEAD !!!

    % Modif JMA le 14/12/2022 pour fichier Ridha 0135_20120607_081059_ShipName.all o� plousieurs config au sein d'un m�me fichier
    ntx = sum(~isnan(Data.TxArray.CentralFrequency(:,:)) & (Data.TxArray.CentralFrequency(:,:) ~= 0), 2);

    for k=1:size(Data.RxArray.TwoWayTravelTime, 1)
%         switch NTx(k) % Modif JMA le 14/12/2022
        switch ntx(k)
            case 1 % Single sector : 1 secteur dans EM_2040_short_spec_Feb_2012__v2.pdf, on duplique le secteur � b�bord et tribord
                Data.TxArray.TiltAngle(k,2)                = Data.TxArray.TiltAngle(k,1);
                Data.TxArray.FocusRange(k,2)               = Data.TxArray.FocusRange(k,1);
                Data.TxArray.SignalLength(k,2)             = Data.TxArray.SignalLength(k,1);
                Data.TxArray.SectorTransmitDelay(k,2)      = Data.TxArray.SectorTransmitDelay(k,1);
                Data.TxArray.CentralFrequency(k,2)         = Data.TxArray.CentralFrequency(k,1);
                Data.TxArray.MeanAbsorptionCoeff(k,2)      = Data.TxArray.MeanAbsorptionCoeff(k,1);
                Data.TxArray.SignalWaveformIdentifier(k,2) = Data.TxArray.SignalWaveformIdentifier(k,1);
                Data.TxArray.SignalBandwith(k,2)           = Data.TxArray.SignalBandwith(k,1);
                Data.TxArray.TransmitSectorNumberTx(k,2)   = Data.TxArray.TransmitSectorNumberTx(k,1) + 1;

                Data.RxArray.TransmitSectorNumberRx(k,subTri) = Data.RxArray.TransmitSectorNumberRx(k,subTri) + 1;

            case 2 % Normal mode 200 kHz : 2 secteurs dans EM_2040_short_spec_Feb_2012__v2.pdf, 
                Data.TxArray.TiltAngle(k,1:4)                = Data.TxArray.TiltAngle(k,[1 2 1 2]);
                Data.TxArray.FocusRange(k,1:4)               = Data.TxArray.FocusRange(k,[1 2 1 2]);
                Data.TxArray.SignalLength(k,1:4)             = Data.TxArray.SignalLength(k,[1 2 1 2]);
                Data.TxArray.SectorTransmitDelay(k,1:4)      = Data.TxArray.SectorTransmitDelay(k,[1 2 1 2]);
                Data.TxArray.CentralFrequency(k,1:4)         = Data.TxArray.CentralFrequency(k,[1 2 1 2]);
                Data.TxArray.MeanAbsorptionCoeff(k,1:4)      = Data.TxArray.MeanAbsorptionCoeff(k,[1 2 1 2]);
                Data.TxArray.SignalWaveformIdentifier(k,1:4) = Data.TxArray.SignalWaveformIdentifier(k,[1 2 1 2]);
                Data.TxArray.SignalBandwith(k,1:4)           = Data.TxArray.SignalBandwith(k,[1 2 1 2]);
                Data.TxArray.TransmitSectorNumberTx(k,1:4)   = 1:4;

                Data.RxArray.TransmitSectorNumberRx(k,subTri) = Data.RxArray.TransmitSectorNumberRx(k,subTri) + 2; % Modif JMA le 29/02/2022 pour fichier EM2040 DualHead 200 kHz

            case 3 % Normal mode : 3 secteurs dans EM_2040_short_spec_Feb_2012__v2.pdf, on duplique le secteur central � b�bord et tribord
                Data.TxArray.TiltAngle(k,1:4)                = Data.TxArray.TiltAngle(k,[1 2 2 3]);
                Data.TxArray.FocusRange(k,1:4)               = Data.TxArray.FocusRange(k,[1 2 2 3]);
                Data.TxArray.SignalLength(k,1:4)             = Data.TxArray.SignalLength(k,[1 2 2 3]);
                Data.TxArray.SectorTransmitDelay(k,1:4)      = Data.TxArray.SectorTransmitDelay(k,[1 2 2 3]);
                Data.TxArray.CentralFrequency(k,1:4)         = Data.TxArray.CentralFrequency(k,[1 2 2 3]);
                Data.TxArray.MeanAbsorptionCoeff(k,1:4)      = Data.TxArray.MeanAbsorptionCoeff(k,[1 2 2 3]);
                Data.TxArray.SignalWaveformIdentifier(k,1:4) = Data.TxArray.SignalWaveformIdentifier(k,[1 2 2 3]);
                Data.TxArray.SignalBandwith(k,1:4)           = Data.TxArray.SignalBandwith(k,[1 2 2 3]);
                Data.TxArray.TransmitSectorNumberTx(k,3:4)   = Data.TxArray.TransmitSectorNumberTx(k,2:3) + 1;

                Data.RxArray.TransmitSectorNumberRx(k,subTri) = Data.RxArray.TransmitSectorNumberRx(k,subTri) + 1;
            otherwise
                my_breakpoint
        end
    end
end

%% G�n�ration du XML SonarScope

Info.Title                   = 'RawRangeBeamAngle';
Info.Constructor             = 'Kongsberg';
Info.EmModel                 = Datagrams.Model;
Info.ListeSystemSerialNumber = num2str(unique(Data.SystemSerialNumber(:))');
% Info.nbPings                = size(Data.RxArray.TwoWayTravelTime, 1);
% Info.nbTx                   = max(NTx);
% Info.nbRx                   = size(Data.RxArray.TwoWayTravelTime, 2);
% Info.nbSounders             = size(Data.PingCounter,2);
Info.TimeOrigin              = '01/01/-4713';
Info.Comments                = 'Sounder ping rate';
Info.Version                 = 'V1';
Info.FormatVersion           = FormatVersion;

Info.Dimensions.nbPings = size(Data.RxArray.TwoWayTravelTime, 1);
Info.Dimensions.nbTx    = max(Data.TxArray.TransmitSectorNumberTx(:)); % max(NTx);

% % TODO : v�rif de ce qui est fait dans Convert_all.exe demand�e � Roger
% nbTx = size(Data.TxArray.TiltAngle,2); % Modif JMA le 29/03/2022 pour fichier Thalia 200 kHz, Normal mode
% Info.Dimensions.nbTx = nbTx;
% Data.TxArray.TransmitSectorNumberTx(Data.TxArray.TransmitSectorNumberTx > nbTx) = nbTx; % Pour ramener les indices =3 � 2 !!!

% D�but alout JMA le 24/10/2022 pour donn�es EM2040 du NIWA pour fichier "H:\QUOI-SSc\EM2040\BayOfPlenty\BoxFOI1\data\0138_20180708_024547.all"
if Info.Dimensions.nbTx > size(Data.TxArray.TiltAngle,2)
    Data.TxArray.TiltAngle(               :,Info.Dimensions.nbTx) = NaN;
    Data.TxArray.FocusRange(              :,Info.Dimensions.nbTx) = NaN;
    Data.TxArray.SignalLength(            :,Info.Dimensions.nbTx) = NaN;
    Data.TxArray.SectorTransmitDelay(     :,Info.Dimensions.nbTx) = NaN;
    Data.TxArray.CentralFrequency(        :,Info.Dimensions.nbTx) = NaN;
    Data.TxArray.MeanAbsorptionCoeff(     :,Info.Dimensions.nbTx) = NaN;
    Data.TxArray.SignalWaveformIdentifier(:,Info.Dimensions.nbTx) = NaN;
    Data.TxArray.TransmitSectorNumberTx(  :,Info.Dimensions.nbTx) = NaN;
    Data.TxArray.SignalBandwith(          :,Info.Dimensions.nbTx) = NaN;
end
% Fin alout JMA le 24/10/2022 pour donn�es EM2040 du NIWA

Info.Dimensions.nbRx = size(Data.RxArray.TwoWayTravelTime, 2);
if SounderDualOneTxAntenna
    Info.Dimensions.nbSounders = 1;
    Data.PingCounter        = Data.PingCounter(:,1);
    Data.SystemSerialNumber = Data.SystemSerialNumber(:,1);
    Data.SoundSpeed         = Data.SoundSpeed(:,1);
    Data.NbBeams            = sum(Data.NbBeams, 2);
    Data.SamplingRate       = Data.SamplingRate(:,1);
else
    Info.Dimensions.nbSounders = size(Data.PingCounter,2);
end

Info.Signals(1).Name          = 'Time';
Info.Signals(end).Dimensions  = 'nbPings, 1'; %nbSounders';
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = 'days since JC';
Info.Signals(end).FileName    = fullfile('Ssc_RawRangeBeamAngle', 'Time.bin');
Info.Signals(end).Tag         = verifKeyWord('SounderTime');

Info.Signals(end+1).Name      = 'PingCounter';
Info.Signals(end).Dimensions  = 'nbPings, nbSounders';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_RawRangeBeamAngle', 'PingCounter.bin');
Info.Signals(end).Tag         = verifKeyWord('SounderPingCounter');

Info.Signals(end+1).Name      = 'SystemSerialNumber';
Info.Signals(end).Dimensions  = 'nbPings, nbSounders';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_RawRangeBeamAngle', 'SystemSerialNumber.bin');
Info.Signals(end).Tag         = verifKeyWord('SounderSerialNumber');

Info.Signals(end+1).Name      = 'SoundSpeed';
Info.Signals(end).Dimensions  = 'nbPings, nbSounders';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'm/s';
Info.Signals(end).FileName    = fullfile('Ssc_RawRangeBeamAngle', 'SoundSpeed.bin');
Info.Signals(end).Tag         = verifKeyWord('SounderSoundSpeed');

Info.Signals(end+1).Name      = 'NbBeams';
Info.Signals(end).Dimensions  = 'nbPings, nbSounders';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_RawRangeBeamAngle', 'NbBeams.bin');
Info.Signals(end).Tag         = verifKeyWord('SounderNbBeams');

Info.Signals(end+1).Name      = 'SamplingRate';
Info.Signals(end).Dimensions  = 'nbPings, nbSounders';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'Hz';
Info.Signals(end).FileName    = fullfile('Ssc_RawRangeBeamAngle', 'SamplingRate.bin');
Info.Signals(end).Tag         = verifKeyWord('SounderSamplingRate');

Info.Images(1).Name           = 'TiltAngle';
Info.Images(end).Dimensions   = 'nbPings, nbTx';
Info.Images(end).Index        = 'nbPings, nbTx';
Info.Images(end).Family       = 'TxArray';
Info.Images(end).Storage      = 'single';
Info.Images(end).Unit         = 'deg';
Info.Images(end).FileName     = fullfile('Ssc_RawRangeBeamAngle', 'TiltAngle.bin');
Info.Images(end).Tag          = verifKeyWord('TODO');

Info.Images(end+1).Name       = 'FocusRange';
Info.Images(end).Dimensions   = 'nbPings, nbTx';
Info.Images(end).Index        = 'nbPings, nbTx';
Info.Images(end).Family       = 'TxArray';
Info.Images(end).Storage      = 'single';
Info.Images(end).Unit         = 'm';
Info.Images(end).FileName     = fullfile('Ssc_RawRangeBeamAngle', 'FocusRange.bin');
Info.Images(end).Tag          = verifKeyWord('TODO');

Info.Images(end+1).Name       = 'SignalLength';
Info.Images(end).Dimensions   = 'nbPings, nbTx';
Info.Images(end).Index        = 'nbPings, nbTx';
Info.Images(end).Family       = 'TxArray';
Info.Images(end).Storage      = 'single';
Info.Images(end).Unit         = 's';
Info.Images(end).FileName     = fullfile('Ssc_RawRangeBeamAngle', 'SignalLength.bin');
Info.Images(end).Tag          = verifKeyWord('TODO');

Info.Images(end+1).Name       = 'SectorTransmitDelay';
Info.Images(end).Dimensions   = 'nbPings, nbTx';
Info.Images(end).Index        = 'nbPings, nbTx';
Info.Images(end).Family       = 'TxArray';
Info.Images(end).Storage      = 'single';
Info.Images(end).Unit         = 's';
Info.Images(end).FileName     = fullfile('Ssc_RawRangeBeamAngle', 'SectorTransmitDelay.bin');
Info.Images(end).Tag          = verifKeyWord('TODO');

Info.Images(end+1).Name       = 'CentralFrequency';
Info.Images(end).Dimensions   = 'nbPings, nbTx';
Info.Images(end).Index        = 'nbPings, nbTx';
Info.Images(end).Family       = 'TxArray';
Info.Images(end).Storage      = 'single';
Info.Images(end).Unit         = 'Hz';
Info.Images(end).FileName     = fullfile('Ssc_RawRangeBeamAngle', 'CentralFrequency.bin');
Info.Images(end).Tag          = verifKeyWord('TODO');

Info.Images(end+1).Name       = 'MeanAbsorptionCoeff';
Info.Images(end).Dimensions   = 'nbPings, nbTx';
Info.Images(end).Index        = 'nbPings, nbTx';
Info.Images(end).Family       = 'TxArray';
Info.Images(end).Storage      = 'single';
Info.Images(end).Unit         = 'dB/km';
Info.Images(end).FileName     = fullfile('Ssc_RawRangeBeamAngle', 'MeanAbsorptionCoeff.bin');
Info.Images(end).Tag          = verifKeyWord('TODO');

Info.Images(end+1).Name       = 'SignalWaveformIdentifier';
Info.Images(end).Dimensions   = 'nbPings, nbTx';
Info.Images(end).Index        = 'nbPings, nbTx';
Info.Images(end).Family       = 'TxArray';
Info.Images(end).Storage      = 'single';
Info.Images(end).Unit         = '';
Info.Images(end).FileName     = fullfile('Ssc_RawRangeBeamAngle', 'SignalWaveformIdentifier.bin');
Info.Images(end).Tag          = verifKeyWord('TODO');

Info.Images(end+1).Name       = 'TransmitSectorNumberTx';
Info.Images(end).Dimensions   = 'nbPings, nbTx';
Info.Images(end).Index        = 'nbPings, nbTx';
Info.Images(end).Family       = 'TxArray';
Info.Images(end).Storage      = 'single';
Info.Images(end).Unit         = '';
Info.Images(end).FileName     = fullfile('Ssc_RawRangeBeamAngle', 'TransmitSectorNumberTx.bin');
Info.Images(end).Tag          = verifKeyWord('TODO');

Info.Images(end+1).Name       = 'SignalBandwith';
Info.Images(end).Dimensions   = 'nbPings, nbTx';
Info.Images(end).Index        = 'nbPings, nbTx';
Info.Images(end).Family       = 'TxArray';
Info.Images(end).Storage      = 'single';
Info.Images(end).Unit         = 'Hz';
Info.Images(end).FileName     = fullfile('Ssc_RawRangeBeamAngle', 'SignalBandwith.bin');
Info.Images(end).Tag          = verifKeyWord('TODO');

Info.Images(end+1).Name       = 'BeamPointingAngle';
Info.Images(end).Dimensions   = 'nbPings, nbRx';
Info.Images(end).Index        = 'nbPings, nbRx';
Info.Images(end).Family       = 'RxArray';
Info.Images(end).Storage      = 'single';
Info.Images(end).Unit         = 'deg';
Info.Images(end).FileName     = fullfile('Ssc_RawRangeBeamAngle', 'BeamPointingAngle.bin');
Info.Images(end).Tag          = verifKeyWord('SounderBeamAngle');

Info.Images(end+1).Name       = 'TransmitSectorNumberRx';
Info.Images(end).Dimensions   = 'nbPings, nbRx';
Info.Images(end).Index        = 'nbPings, nbRx';
Info.Images(end).Family       = 'RxArray';
Info.Images(end).Storage      = 'single';
Info.Images(end).Unit         = '';
Info.Images(end).FileName     = fullfile('Ssc_RawRangeBeamAngle', 'TransmitSectorNumberRx.bin');
Info.Images(end).Tag          = verifKeyWord('TODO');

Info.Images(end+1).Name       = 'DetectionInfo';
Info.Images(end).Dimensions   = 'nbPings, nbRx';
Info.Images(end).Index        = 'nbPings, nbRx';
Info.Images(end).Family       = 'RxArray';
Info.Images(end).Storage      = 'single';
Info.Images(end).Unit         = '';
Info.Images(end).FileName     = fullfile('Ssc_RawRangeBeamAngle', 'DetectionInfo.bin');
Info.Images(end).Tag          = verifKeyWord('TODO');

Info.Images(end+1).Name       = 'LengthDetection';
Info.Images(end).Dimensions   = 'nbPings, nbRx';
Info.Images(end).Index        = 'nbPings, nbRx';
Info.Images(end).Family       = 'RxArray';
Info.Images(end).Storage      = 'single';
Info.Images(end).Unit         = '';
Info.Images(end).FileName     = fullfile('Ssc_RawRangeBeamAngle', 'LengthDetection.bin');
Info.Images(end).Tag          = verifKeyWord('TODO');

Info.Images(end+1).Name       = 'QualityFactor';
Info.Images(end).Dimensions   = 'nbPings, nbRx';
Info.Images(end).Index        = 'nbPings, nbRx';
Info.Images(end).Family       = 'RxArray';
Info.Images(end).Storage      = 'single';
Info.Images(end).Unit         = '';
Info.Images(end).FileName     = fullfile('Ssc_RawRangeBeamAngle', 'QualityFactor.bin');
Info.Images(end).Tag          = verifKeyWord('TODO');

% + Spare

Info.Images(end+1).Name       = 'TwoWayTravelTime';
Info.Images(end).Dimensions   = 'nbPings, nbRx';
Info.Images(end).Index        = 'nbPings, nbRx';
Info.Images(end).Family       = 'RxArray';
Info.Images(end).Storage      = 'single';
Info.Images(end).Unit         = 'samples';
Info.Images(end).FileName     = fullfile('Ssc_RawRangeBeamAngle', 'TwoWayTravelTime.bin');
Info.Images(end).Tag          = verifKeyWord('SounderDetectionNbSamples');

Info.Images(end+1).Name       = 'Reflectivity';
Info.Images(end).Dimensions   = 'nbPings, nbRx';
Info.Images(end).Index        = 'nbPings, nbRx';
Info.Images(end).Family       = 'RxArray';
Info.Images(end).Storage      = 'single';
Info.Images(end).Unit         = 'dB';
Info.Images(end).FileName     = fullfile('Ssc_RawRangeBeamAngle', 'Reflectivity.bin');
Info.Images(end).Tag          = verifKeyWord('SounderReflectivity');

Info.Images(end+1).Name       = 'RealTimeCleaningInfo';
Info.Images(end).Dimensions   = 'nbPings, nbRx';
Info.Images(end).Index        = 'nbPings, nbRx';
Info.Images(end).Family       = 'RxArray';
Info.Images(end).Storage      = 'single';
Info.Images(end).Unit         = '';
Info.Images(end).FileName     = fullfile('Ssc_RawRangeBeamAngle', 'RealTimeCleaningInfo.bin');
Info.Images(end).Tag          = verifKeyWord('TODO');

% + Spare

% Ajout des champs Scale_Factor, Add_Offset et Orig_Storage.
[flag, Info] = addScaleOffsetStorage(Datagrams, Info);

%% Cr�ation du fichier XML d�crivant la donn�e 

nomFicXml = fullfile(nomDirRacine, 'Ssc_RawRangeBeamAngle.xml');
xml_write(nomFicXml, Info);
flag = exist(nomFicXml, 'file');
if ~flag
    messageErreur(nomFicXml)
    return
end
% edit(nomFicXml)

%% Cr�ation du r�pertoire RawRangeBeamAngle

nomDirRawRangeBeamAngle = fullfile(nomDirRacine, 'Ssc_RawRangeBeamAngle');
if ~exist(nomDirRawRangeBeamAngle, 'dir')
    status = mkdir(nomDirRawRangeBeamAngle);
    if ~status
        messageErreur(nomDirRawRangeBeamAngle)
        flag = 0;
        return
    end
end

%% Cr�ation des fichiers binaires des signaux

for k=1:length(Info.Signals)
    flag = writeSignal(nomDirRacine, Info.Signals(k), Data.(Info.Signals(k).Name));
    if ~flag
        return
    end
end

%% Cr�ation des fichiers binaires des images

for k=1:length(Info.Images)
    flag = writeImage(nomDirRacine, Info.Images(k), Data.(Info.Images(k).Family).(Info.Images(k).Name));
    if ~flag
        return
    end
end

flag = 1;
