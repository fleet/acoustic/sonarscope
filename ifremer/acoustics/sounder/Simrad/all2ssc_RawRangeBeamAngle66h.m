function flag = all2ssc_RawRangeBeamAngle66h(nomDirRacine, FormatVersion, Head1, MaxDesMaxNbBeams)

nomFicXml = fullfile(nomDirRacine, 'ALL_RawRangeBeamAngle66h.xml');
SonarFmt2ssc_message(nomFicXml, '.all')

%% Lecture du fichier XML d�crivant la donn�e 

flag = exist(nomFicXml, 'file');
if ~flag
    special_message('FichierNonExistant', nomFicXml)
    return
end
% edit(nomFicXml)
Datagrams = xml_read(nomFicXml);

[nomDirSignal, nomFicSignal] = fileparts(nomFicXml);
nomDirSignal = fullfile(nomDirSignal, nomFicSignal);


%{
for k=1:length(Datagrams.Variables)
    fprintf(1,'%d  %s\n', k, Datagrams.Variables(k).Name)
end
for k=1:length(Datagrams.Tables)
    fprintf(1,'%d  %s\n', k, Datagrams.Tables(k).Name)
end
%}

[flag, Date1] = Read_BinVariable(Datagrams, nomDirSignal, 'Date', 'double');
if ~flag
    return
end
% figure; plot(Date1); grid on; title('Date');

[flag, NbMilliSecs1] = Read_BinVariable(Datagrams, nomDirSignal, 'NbMilliSecs', 'double');
if ~flag
    return
end
% figure; plot(NbMilliSecs1); grid on; title('NbMilliSecs');

[flag, PingCounter] = Read_BinVariable(Datagrams, nomDirSignal, 'PingCounter', 'single');
if ~flag
    return
end
PingCounter = unwrapPingCounterSimrad(PingCounter);
% figure; plot(PingCounter); grid on; title('PingCounter');

[flag, SystemSerialNumber] = Read_BinVariable(Datagrams, nomDirSignal, 'SystemSerialNumber', 'single');
if ~flag
    return
end
% figure; plot(SystemSerialNumber, '+'); grid on; title('SystemSerialNumber');

[flag, SamplingRate] = Read_BinVariable(Datagrams, nomDirSignal, 'SamplingRate', 'single');
if ~flag
    return
end
% figure; plot(SamplingRate, '+'); grid on; title('SamplingRate');

[flag, SoundSpeed] = Read_BinVariable(Datagrams, nomDirSignal, 'SoundSpeed', 'single');
if ~flag
    return
end
% figure; plot(SoundSpeed); grid on; title('SoundSpeed');

[flag, ROV_Depth] = Read_BinVariable(Datagrams, nomDirSignal, 'ROV_Depth', 'single');
if ~flag
    return
end
% figure; plot(ROV_Depth); grid on; title('ROV_Depth');

[flag, MaxNbBeamsPossible] = Read_BinVariable(Datagrams, nomDirSignal, 'MaxNbBeamsPossible', 'single');
if ~flag
    return
end
% figure; plot(Data.MaxNbBeamsPossible); grid on; title('Data.MaxNbBeamsPossible');

[flag, NTx] = Read_BinVariable(Datagrams, nomDirSignal, 'NTx', 'single');
if ~flag
    return
end
% figure; plot(NTx); grid on; title('NTx');
nbTx = sum(NTx);

[flag, NRx] = Read_BinVariable(Datagrams, nomDirSignal, 'NRx', 'single');
if ~flag
    return
end
% figure; plot(NRx); grid on; title('NRx');
sumNRx = sum(NRx);

Date                    = Kongsberg_table2signal(Date1,              Datagrams.NbDatagrams, PingCounter, SystemSerialNumber, Head1);
NbMilliSecs             = Kongsberg_table2signal(NbMilliSecs1,       Datagrams.NbDatagrams, PingCounter, SystemSerialNumber, Head1);
Data.SamplingRate       = Kongsberg_table2signal(SamplingRate,       Datagrams.NbDatagrams, PingCounter, SystemSerialNumber, Head1);
Data.PingCounter        = Kongsberg_table2signal(PingCounter,        Datagrams.NbDatagrams, PingCounter, SystemSerialNumber, Head1);
Data.SystemSerialNumber = Kongsberg_table2signal(SystemSerialNumber, Datagrams.NbDatagrams, PingCounter, SystemSerialNumber, Head1);
Data.SoundSpeed         = Kongsberg_table2signal(SoundSpeed,         Datagrams.NbDatagrams, PingCounter, SystemSerialNumber, Head1);
Data.ROV_Depth          = Kongsberg_table2signal(ROV_Depth,          Datagrams.NbDatagrams, PingCounter, SystemSerialNumber, Head1);
Data.NRx                = Kongsberg_table2signal(NRx,                Datagrams.NbDatagrams, PingCounter, SystemSerialNumber, Head1);
Data.MaxNbBeamsPossible = Kongsberg_table2signal(MaxNbBeamsPossible, Datagrams.NbDatagrams, PingCounter, SystemSerialNumber, Head1);

% D�but rajout JMA le 20/09/2016 avec mise en commentaire Kongsberg_table2signal
% subNaN = find(isnan(Data.PingCounter));
% subNaN = find(isnan(sum(Data.PingCounter,2)));
subNaN = find(all(isnan(Data.PingCounter),2));  % Modif JMA le 14/03/2019 pour fichier 0021_20190306_040731_Belgica.all
Date(subNaN,:)                    = [];
NbMilliSecs(subNaN,:)             = [];
Data.SamplingRate(subNaN,:)       = [];
Data.PingCounter(subNaN,:)        = [];
Data.SystemSerialNumber(subNaN,:) = [];
Data.SoundSpeed(subNaN,:)         = [];
Data.ROV_Depth(subNaN,:)          = [];
Data.NRx(subNaN,:)                = [];
Data.MaxNbBeamsPossible(subNaN,:) = [];
% Fin rajout JMA le 20/09/2016 avec mise en commentaire Kongsberg_table2signal

% Correct PingCounter in case of dual sounder but only the starboard is on
% (correspondances between pings are calculated on indice=1 on second
% dimension
Data.PingCounter        = repairPingCounterInCaseDualSounderButOnlyStarnoardRunning(Data.PingCounter);
Data.SamplingRate       = repairPingCounterInCaseDualSounderButOnlyStarnoardRunning(Data.SamplingRate);
Data.SoundSpeed         = repairPingCounterInCaseDualSounderButOnlyStarnoardRunning(Data.SoundSpeed);
Data.ROV_Depth          = repairPingCounterInCaseDualSounderButOnlyStarnoardRunning(Data.ROV_Depth);
Data.MaxNbBeamsPossible = repairPingCounterInCaseDualSounderButOnlyStarnoardRunning(Data.MaxNbBeamsPossible);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% V�rifier quand NTx ~= 1 : EM300
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[flag, TxBeamNumber] = Read_BinTable(Datagrams, nomDirSignal, 'TransmitSectorNumberTx', 'single', nbTx);
if ~flag
    return
end
TxBeamNumber = TxBeamNumber + 1;
Data.TxBeamNumber           = Kongsberg_table2image(TxBeamNumber, 'TransmitSectorNumberTx', Datagrams.NbDatagrams, max(NTx), NTx, TxBeamNumber, PingCounter, SystemSerialNumber, Head1);
Data.TxBeamNumber(subNaN,:) = [];

Data.TransmitSectorNumberTx = Data.TxBeamNumber;
% figure; plot(Data.TxBeamNumber, '*'); title('TxBeamNumber'); grid on;
% TxBeamNumber = ones(size(NTx)); %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[flag, X] = Read_BinTable(Datagrams, nomDirSignal, 'TiltAngle', 'single', nbTx);
if ~flag
    return
end
Data.TiltAngle = Kongsberg_table2image(X, 'TiltAngle', Datagrams.NbDatagrams, max(NTx), NTx, TxBeamNumber,  PingCounter, SystemSerialNumber, Head1);
Data.TiltAngle(subNaN,:) = [];
% figure; plot(Data.TiltAngle, '*'); title('TiltAngle'); grid on;

[flag, X] = Read_BinTable(Datagrams, nomDirSignal, 'FocusRange', 'single', nbTx);
if ~flag
    return
end
Data.FocusRange = Kongsberg_table2image(X, 'FocusRange', Datagrams.NbDatagrams, max(NTx), NTx, TxBeamNumber,  PingCounter, SystemSerialNumber, Head1);
Data.FocusRange(subNaN,:) = [];
% figure; plot(Data.FocusRange, '*'); title('FocusRange'); grid on;

[flag, X] = Read_BinTable(Datagrams, nomDirSignal, 'SignalLength', 'single', nbTx);
if ~flag
    return
end
Data.SignalLength = Kongsberg_table2image(X, 'SignalLength', Datagrams.NbDatagrams, max(NTx), NTx, TxBeamNumber,  PingCounter, SystemSerialNumber, Head1);
Data.SignalLength(subNaN,:) = [];
% figure; plot(Data.SignalLength, '*'); title('SignalLength'); grid on;

[flag, X] = Read_BinTable(Datagrams, nomDirSignal, 'SectorTransmitDelay', 'single', nbTx);
if ~flag
    return
end
Data.SectorTransmitDelay = 1000 * Kongsberg_table2image(X, 'SectorTransmitDelay', Datagrams.NbDatagrams, max(NTx), NTx, TxBeamNumber,  PingCounter, SystemSerialNumber, Head1);
Data.SectorTransmitDelay(subNaN,:) = [];
% figure; plot(Data.SectorTransmitDelay, '*'); title('SectorTransmitDelay'); grid on;

[flag, X] = Read_BinTable(Datagrams, nomDirSignal, 'CentralFrequency', 'single', nbTx);
if ~flag
    return
end
Data.CentralFrequency = Kongsberg_table2image(X, 'CentralFrequency', Datagrams.NbDatagrams, max(NTx), NTx, TxBeamNumber,  PingCounter, SystemSerialNumber, Head1);
Data.CentralFrequency(subNaN,:) = [];
% figure; plot(Data.CentralFrequency, '*'); title('CentralFrequency'); grid on;

[flag, X] = Read_BinTable(Datagrams, nomDirSignal, 'SignalBandwith', 'single', nbTx);
if ~flag
    return
end
Data.SignalBandwith = Kongsberg_table2image(X, 'SignalBandwith', Datagrams.NbDatagrams, max(NTx), NTx, TxBeamNumber,  PingCounter, SystemSerialNumber, Head1);
Data.SignalBandwith(subNaN,:) = [];
% figure; plot(Data.SignalBandwith, '*'); title('SignalBandwith'); grid on;

[flag, X] = Read_BinTable(Datagrams, nomDirSignal, 'SignalWaveformIdentifier', 'single', nbTx);
if ~flag
    return
end
Data.SignalWaveformIdentifier = Kongsberg_table2image(X, 'SignalWaveformIdentifier', Datagrams.NbDatagrams, max(NTx), NTx, TxBeamNumber,  PingCounter, SystemSerialNumber, Head1);
Data.SignalWaveformIdentifier(subNaN,:) = [];
% figure; plot(Data.SignalWaveformIdentifier, '*'); title('SignalWaveformIdentifier'); grid on;

%% RxBeamNumber

[flag, X] = Read_BinTable(Datagrams, nomDirSignal, 'RxBeamNumber', 'single', sumNRx);
if ~flag
    return
end
RxBeamNumber = X; 

max_NRx = max(max(NRx), MaxDesMaxNbBeams);
Data.RxBeamNumber = Kongsberg_table2image(X, 'RxBeamNumber', Datagrams.NbDatagrams, ...
    max_NRx, NRx, RxBeamNumber, PingCounter, SystemSerialNumber, Head1);
Data.RxBeamNumber(subNaN,:) = [];
% figure; imagesc(Data.RxBeamNumber); title('RxBeamNumber'); axis xy; colorbar;

%% BeamPointingAngle

[flag, X] = Read_BinTable(Datagrams, nomDirSignal, 'BeamPointingAngle', 'single', sumNRx);
if ~flag
    return
end
Data.BeamPointingAngle = Kongsberg_table2image(X, 'BeamPointingAngle', Datagrams.NbDatagrams, ...
    max_NRx, NRx, RxBeamNumber, PingCounter, SystemSerialNumber, Head1);
Data.BeamPointingAngle(subNaN,:) = [];
% figure; imagesc(Data.BeamPointingAngle); title('BeamPointingAngle'); axis xy; colorbar;

%% TwoWayTravelTime

[flag, X] = Read_BinTable(Datagrams, nomDirSignal, 'Range', 'single', sumNRx);
if ~flag
    return
end
% X est d�j� transform� en quarts de samples (xxxx.00, xxxx.25,xxxx.50, xxxx.75)
Data.Range = Kongsberg_table2image(X, 'Range', Datagrams.NbDatagrams, ...
    max_NRx, NRx, RxBeamNumber, PingCounter, SystemSerialNumber, Head1);
Data.Range(subNaN,:) = [];
% figure; imagesc(Data.Range); title('Range'); axis xy; colorbar;

%% TransmitSectorNumberRx

[flag, X] = Read_BinTable(Datagrams, nomDirSignal, 'TransmitSectorNumberRx', 'single', sumNRx);
if ~flag
    return
end
Data.TransmitSectorNumberRx = Kongsberg_table2image(X, 'TransmitSectorNumberRx', Datagrams.NbDatagrams, ...
    max_NRx, NRx, RxBeamNumber, PingCounter, SystemSerialNumber, Head1);

Data.TransmitSectorNumberRx(subNaN,:) = [];
% Data.TransmitSectorNumberRx = Data.TransmitSectorNumberRx -1;
% figure; imagesc(Data.TransmitSectorNumberRx); title('TransmitSectorNumberRx'); axis xy; colorbar;


%% Reflectivity

[flag, X] = Read_BinTable(Datagrams, nomDirSignal, 'Reflectivity', 'single', sumNRx);
if ~flag
    return
end
Data.Reflectivity = Kongsberg_table2image(X, 'Reflectivity', Datagrams.NbDatagrams, ...
    max_NRx, NRx, RxBeamNumber, PingCounter, SystemSerialNumber, Head1);
Data.Reflectivity(subNaN,:) = [];
% figure; imagesc(Data.Reflectivity); title('Reflectivity'); axis xy; colorbar;

%% QualityFactor

[flag, X] = Read_BinTable(Datagrams, nomDirSignal, 'QualityFactor', 'single', sumNRx);
if ~flag
    return
end
Data.QualityFactor = Kongsberg_table2image(X, 'QualityFactor', Datagrams.NbDatagrams, ...
    max_NRx, NRx, RxBeamNumber, PingCounter, SystemSerialNumber, Head1);
Data.QualityFactor(subNaN,:) = [];
% figure; imagesc(Data.QualityFactor); title('QualityFactor'); axis xy; colorbar;

%% LengthDetection

[flag, X] = Read_BinTable(Datagrams, nomDirSignal, 'LengthDetection', 'single', sumNRx);
if ~flag
    return
end
Data.LengthDetection = Kongsberg_table2image(X, 'LengthDetection', Datagrams.NbDatagrams, ...
    max_NRx, NRx, RxBeamNumber, PingCounter, SystemSerialNumber, Head1);
Data.LengthDetection(subNaN,:) = [];
% figure; imagesc(Data.LengthDetection); title('LengthDetection'); axis xy; colorbar;

Data.TransmitTiltAngle = NaN(size(Data.Range), 'single');
for k=1:size(Data.Range,1)
%     for iBeam=1:size(Data.Range,2)
%         if isnan(Data.TransmitSectorNumberRx(k,iBeam))
%             continue
%         end
%         Data.TransmitTiltAngle(k,iBeam) = Data.TiltAngle(k,Data.TransmitSectorNumberRx(k,iBeam));
%     end
    sub = find(~isnan(Data.TransmitSectorNumberRx(k,:)) & (Data.TransmitSectorNumberRx(k,:) ~= 0));
    sub2 = Data.TransmitSectorNumberRx(k,sub);
    sub3 = find(sub2 <= size(Data.TiltAngle,2));
    Data.TransmitTiltAngle(k,sub(sub3)) = Data.TiltAngle(k,sub2(sub3));
end

%{
for k=1:length(Datagrams.Tables)
    fprintf(1,'%d  %s  %s\n', k, Datagrams.Tables(k).Name, Datagrams.Tables(k).Indexation)
end
%}

nbPings    = size(Data.Range, 1);
nbRx       = size(Data.Range, 2);
nbSounders = size(Data.PingCounter,2);

%% Calcul des dates et heures

Jour  = mod(Date,100);
Date  = (Date - Jour) / 100;
Mois  = mod(Date,100);
Annee = (Date - Mois) / 100;
Date = dayJma2Ifr(Jour, Mois, Annee);
Data.Time = cl_time('timeIfr', Date, NbMilliSecs);

%% G�n�ration du XML SonarScope

Info.Title                  = 'RawRangeBeamAngle';
Info.Constructor            = 'Kongsberg';
Info.EmModel                = Datagrams.Model;
Info.ListeSystemSerialNumber = num2str(unique(Data.SystemSerialNumber(:))');
Info.TimeOrigin             = '01/01/-4713';
Info.Comments               = 'Sounder ping rate';
Info.Version                = 'V1';
Info.FormatVersion          = FormatVersion;

Info.Dimensions.nbPings       = nbPings;
Info.Dimensions.nbRx          = nbRx;
Info.Dimensions.nbTx          = size(Data.TiltAngle, 2); %  Data.TxBeamNumber;
Info.Dimensions.nbSounders    = nbSounders;

Info.Signals(1).Name          = 'Time';
Info.Signals(end).Dimensions  = 'nbPings, nbSounders';
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = 'days since JC';
Info.Signals(end).FileName    = fullfile('Ssc_RawRangeBeamAngle', 'Time.bin');
Info.Signals(end).Tag         = verifKeyWord('SounderTime');

Info.Signals(end+1).Name      = 'PingCounter';
Info.Signals(end).Dimensions  = 'nbPings, nbSounders';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_RawRangeBeamAngle', 'PingCounter.bin');
Info.Signals(end).Tag         = verifKeyWord('SounderPingCounter');

Info.Signals(end+1).Name      = 'SystemSerialNumber';
Info.Signals(end).Dimensions  = 'nbPings, nbSounders';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_RawRangeBeamAngle', 'SystemSerialNumber.bin');
Info.Signals(end).Tag         = verifKeyWord('SounderSerialNumber');

Info.Signals(end+1).Name      = 'NRx';
Info.Signals(end).Dimensions  = 'nbPings, nbSounders';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'm';
Info.Signals(end).FileName    = fullfile('Ssc_RawRangeBeamAngle', 'NRx.bin');
Info.Signals(end).Tag         = verifKeyWord('NRx');

Info.Signals(end+1).Name      = 'SamplingRate';
Info.Signals(end).Dimensions  = 'nbPings, nbSounders';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'Hz';
Info.Signals(end).FileName    = fullfile('Ssc_RawRangeBeamAngle', 'SamplingRate.bin');
Info.Signals(end).Tag         = verifKeyWord('SounderSamplingRate');

Info.Signals(end+1).Name      = 'ROV_Depth';
Info.Signals(end).Dimensions  = 'nbPings, nbSounders';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'm';
Info.Signals(end).FileName    = fullfile('Ssc_RawRangeBeamAngle', 'ROV_Depth.bin');
Info.Signals(end).Tag         = verifKeyWord('SounderROV_Depth');

Info.Signals(end+1).Name      = 'SoundSpeed';
Info.Signals(end).Dimensions  = 'nbPings, nbSounders';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'm/s';
Info.Signals(end).FileName    = fullfile('Ssc_RawRangeBeamAngle', 'SoundSpeed.bin');
Info.Signals(end).Tag         = verifKeyWord('SounderSoundSpeed');

Info.Signals(end+1).Name      = 'MaxNbBeamsPossible';
Info.Signals(end).Dimensions  = 'nbPings, nbSounders';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'm';
Info.Signals(end).FileName    = fullfile('Ssc_RawRangeBeamAngle', 'MaxNbBeamsPossible.bin');
Info.Signals(end).Tag         = verifKeyWord('MaxNbBeamsPossible');


Info.Images(1).Name          = 'TiltAngle';
Info.Images(end).Dimensions  = 'nbPings, nbTx';
Info.Images(end).Index       = 'nbPings, nbTx';
Info.Images(end).Storage     = 'single';
Info.Images(end).Unit        = 'deg';
Info.Images(end).FileName    = fullfile('Ssc_RawRangeBeamAngle', 'TiltAngle.bin');
Info.Images(end).Tag         = verifKeyWord('SounderTiltAngle');

Info.Images(end+1).Name      = 'FocusRange';
Info.Images(end).Dimensions  = 'nbPings, nbTx';
Info.Images(end).Index       = 'nbPings, nbTx';
Info.Images(end).Storage     = 'single';
Info.Images(end).Unit        = '???';
Info.Images(end).FileName    = fullfile('Ssc_RawRangeBeamAngle', 'FocusRange.bin');
Info.Images(end).Tag         = verifKeyWord('SounderFocusRange');

Info.Images(end+1).Name      = 'SignalLength';
Info.Images(end).Dimensions  = 'nbPings, nbTx';
Info.Images(end).Index       = 'nbPings, nbTx';
Info.Images(end).Storage     = 'single';
Info.Images(end).Unit        = 'ms';
Info.Images(end).FileName    = fullfile('Ssc_RawRangeBeamAngle', 'SignalLength.bin');
Info.Images(end).Tag         = verifKeyWord('SounderPulseLength');

Info.Images(end+1).Name      = 'SectorTransmitDelay';
Info.Images(end).Dimensions  = 'nbPings, nbTx';
Info.Images(end).Index       = 'nbPings, nbTx';
Info.Images(end).Storage     = 'single';
Info.Images(end).Unit        = 'ms';
Info.Images(end).FileName    = fullfile('Ssc_RawRangeBeamAngle', 'SectorTransmitDelay.bin');
Info.Images(end).Tag         = verifKeyWord('SounderTxDelay');

Info.Images(end+1).Name      = 'CentralFrequency';
Info.Images(end).Dimensions  = 'nbPings, nbTx';
Info.Images(end).Index       = 'nbPings, nbTx';
Info.Images(end).Storage     = 'single';
Info.Images(end).Unit        = 'Hz';
Info.Images(end).FileName    = fullfile('Ssc_RawRangeBeamAngle', 'CentralFrequency.bin');
Info.Images(end).Tag         = verifKeyWord('SounderTxCentralFrequency');

Info.Images(end+1).Name      = 'SignalBandwith';
Info.Images(end).Dimensions  = 'nbPings, nbTx';
Info.Images(end).Index       = 'nbPings, nbTx';
Info.Images(end).Storage     = 'single';
Info.Images(end).Unit        = 'Hz';
Info.Images(end).FileName    = fullfile('Ssc_RawRangeBeamAngle', 'SignalBandwith.bin');
Info.Images(end).Tag         = verifKeyWord('SounderTxSignalBandwith');

Info.Images(end+1).Name      = 'SignalWaveformIdentifier';
Info.Images(end).Dimensions  = 'nbPings, nbTx';
Info.Images(end).Index       = 'nbPings, nbTx';
Info.Images(end).Storage     = 'single';
Info.Images(end).Unit        = '';
Info.Images(end).FileName    = fullfile('Ssc_RawRangeBeamAngle', 'SignalWaveformIdentifier.bin');
Info.Images(end).Tag         = verifKeyWord('TODO');

Info.Images(end+1).Name      = 'TransmitSectorNumberTx';
Info.Images(end).Dimensions  = 'nbPings, nbTx';
Info.Images(end).Index       = 'nbPings, nbTx';
Info.Images(end).Storage     = 'single';
Info.Images(end).Unit        = '';
Info.Images(end).FileName    = fullfile('Ssc_RawRangeBeamAngle', 'TransmitSectorNumberTx.bin');
Info.Images(end).Tag         = verifKeyWord('TODO');

% Spare 1

% Spare 2

Info.Images(end+1).Name       = 'BeamPointingAngle';
Info.Images(end).Dimensions   = 'nbPings, nbRx';
Info.Images(end).Index        = 'nbPings, NRx';
Info.Images(end).Storage      = 'single';
Info.Images(end).Unit         = 'deg';
Info.Images(end).FileName     = fullfile('Ssc_RawRangeBeamAngle', 'BeamPointingAngle.bin');
Info.Images(end).Tag          = verifKeyWord('SounderBeamAngle');

Info.Images(end+1).Name       = 'Range';
Info.Images(end).Dimensions   = 'nbPings, nbRx';
Info.Images(end).Index        = 'nbPings, NRx';
Info.Images(end).Storage      = 'single';
Info.Images(end).Unit         = 'samples';
Info.Images(end).FileName     = fullfile('Ssc_RawRangeBeamAngle', 'Range.bin');
Info.Images(end).Tag          = verifKeyWord('SounderDetectionNbSamples');

Info.Images(end+1).Name       = 'TransmitSectorNumberRx';
Info.Images(end).Dimensions   = 'nbPings, nbRx';
Info.Images(end).Index        = 'nbPings, NRx';
Info.Images(end).Storage      = 'single';
Info.Images(end).Unit         = '';
Info.Images(end).FileName     = fullfile('Ssc_RawRangeBeamAngle', 'TransmitSectorNumberRx.bin');
Info.Images(end).Tag          = verifKeyWord('SounderTxBeamIndex');

Info.Images(end+1).Name       = 'Reflectivity';
Info.Images(end).Dimensions   = 'nbPings, nbRx';
Info.Images(end).Index        = 'nbPings, NRx';
Info.Images(end).Storage      = 'single';
Info.Images(end).Unit         = 'dB';
Info.Images(end).FileName     = fullfile('Ssc_RawRangeBeamAngle', 'Reflectivity.bin');
Info.Images(end).Tag          = verifKeyWord('SounderReflectivity');

Info.Images(end+1).Name       = 'QualityFactor';
Info.Images(end).Dimensions   = 'nbPings, nbRx';
Info.Images(end).Index        = 'nbPings, NRx';
Info.Images(end).Storage      = 'single';
Info.Images(end).Unit         = '';
Info.Images(end).FileName     = fullfile('Ssc_RawRangeBeamAngle', 'QualityFactor.bin');
Info.Images(end).Tag          = verifKeyWord('SounderSimradQF');

Info.Images(end+1).Name       = 'LengthDetection';
Info.Images(end).Dimensions   = 'nbPings, nbRx';
Info.Images(end).Index        = 'nbPings, NRx';
Info.Images(end).Storage      = 'single';
Info.Images(end).Unit         = '';
Info.Images(end).FileName     = fullfile('Ssc_RawRangeBeamAngle', 'LengthDetection.bin');
Info.Images(end).Tag          = verifKeyWord('SounderSimradLengthDetection');

Info.Images(end+1).Name       = 'RxBeamNumber';
Info.Images(end).Dimensions   = 'nbPings, nbRx';
Info.Images(end).Index        = 'nbPings, NRx';
Info.Images(end).Storage      = 'single';
Info.Images(end).Unit         = '';
Info.Images(end).FileName     = fullfile('Ssc_RawRangeBeamAngle', 'RxBeamNumber.bin');
Info.Images(end).Tag          = verifKeyWord('TODO');

% Ajout� pour compatibilit� avec l'existant
Info.Images(end+1).Name       = 'TransmitTiltAngle';
Info.Images(end).Dimensions   = 'nbPings, nbRx';
Info.Images(end).Index        = 'nbPings, NRx';
Info.Images(end).Storage      = 'single';
Info.Images(end).Unit         = '';
Info.Images(end).FileName     = fullfile('Ssc_RawRangeBeamAngle', 'TransmitTiltAngle.bin');
Info.Images(end).Tag          = verifKeyWord('SounderTxTiltAngle');

% Ajout des champs Scale_Factor, Add_Offset et Orig_Storage.
[~, Info] = addScaleOffsetStorage(Datagrams, Info);
%% Cr�ation du fichier XML d�crivant la donn�e 

nomFicXml = fullfile(nomDirRacine, 'Ssc_RawRangeBeamAngle.xml');
xml_write(nomFicXml, Info);
flag = exist(nomFicXml, 'file');
if ~flag
    messageErreur(nomFicXml)
    return
end
% edit(nomFicXml)

%% Cr�ation du r�pertoire RawRangeBeamAngle

nomDirRawRangeBeamAngle = fullfile(nomDirRacine, 'Ssc_RawRangeBeamAngle');
if ~exist(nomDirRawRangeBeamAngle, 'dir')
    status = mkdir(nomDirRawRangeBeamAngle);
    if ~status
        messageErreur(nomDirRawRangeBeamAngle)
        flag = 0;
        return
    end
end

%% Cr�ation des fichiers binaires des signaux

for k=1:length(Info.Signals)
    flag = writeSignal(nomDirRacine, Info.Signals(k), Data.(Info.Signals(k).Name));
    if ~flag
        return
    end
end

%% Cr�ation des fichiers binaires des images

for k=1:length(Info.Images)
    flag = writeImage(nomDirRacine, Info.Images(k), Data.(Info.Images(k).Name));
    if ~flag
        return
    end
end

flag = 1;
