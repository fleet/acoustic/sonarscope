function flag = all2ssc_Runtime(nomDirRacine, FormatVersion, EmModel)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% CHANGER de tactique, ne pas cr�er les .bin mais donner les variables dans
% le .xml ?????????????????????????????????????????????????????????????????
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Lecture du fichier XML d�crivant la donn�e 

nomFicXml = fullfile(nomDirRacine, 'ALL_Runtime.xml');
flag = exist(nomFicXml, 'file');
if ~flag
    special_message('FichierNonExistant', nomFicXml, 'NoStop')
    return
end

% edit(nomFicXml)
Datagrams = xml_read(nomFicXml);

[nomDirSignal, nomFicSignal] = fileparts(nomFicXml);
nomDirSignal = fullfile(nomDirSignal, nomFicSignal);

%{
for k=1:length(Datagrams.Variables)
    fprintf(1,'%d  %s\n', k, Datagrams.Variables(k).Name)
end
%}

nbSamples = Datagrams.NbDatagrams;

[flag, Date] = Read_BinVariable(Datagrams, nomDirSignal, 'Date', 'double');
if ~flag
    return
end
% figure; plot(Date); grid on; title('Date');

[flag, NbMilliSecs] = Read_BinVariable(Datagrams, nomDirSignal, 'NbMilliSecs', 'double');
if ~flag
    return
end
% figure; plot(NbMilliSecs); grid on; title('NbMilliSecs');

[flag, Data.PingCounter] = Read_BinVariable(Datagrams, nomDirSignal, 'PingCounter', 'double');
if ~flag
    return
end
Data.PingCounter = unwrapPingCounterSimrad(Data.PingCounter);
% figure; plot(Data.PingCounter); grid on; title('PingCounter');

[flag, Data.OperatorStationStatus] = Read_BinVariable(Datagrams, nomDirSignal, 'OperatorStationStatus', 'uint8');
if ~flag
    return
end
% figure; plot(Data.OperatorStationStatus); grid on; title('OperatorStationStatus');

[flag, Data.ProcessingUnitStatus] = Read_BinVariable(Datagrams, nomDirSignal, 'ProcessingUnitStatus', 'uint8');
if ~flag
    return
end
% figure; plot(Data.ProcessingUnitStatus); grid on; title('ProcessingUnitStatus');

[flag, Data.BSPStatus] = Read_BinVariable(Datagrams, nomDirSignal, 'BSPStatus', 'uint8');
if ~flag
    return
end
% figure; plot(Data.BSPStatus); grid on; title('BSPStatus');

[flag, Data.SonarHeadStatus] = Read_BinVariable(Datagrams, nomDirSignal, 'SonarHeadStatus', 'uint8');
if ~flag
    return
end
% figure; plot(Data.SonarHeadStatus); grid on; title('SonarHeadStatus');

[flag, X] = Read_BinVariable(Datagrams, nomDirSignal, 'Mode', 'uint8');
if ~flag
    return
end
Data.ModeDatagram = X;

switch EmModel
    case 2045
        Data.Mode = 1 + bitand(X, 15);
    otherwise
        Data.Mode = 1 + bitand(X, 7);
end

Data.DualSwath = 1 + bitshift(X, -6); % 00 Dualswath off, 01 : Dual swath on, 10 : Dual swath dynamic
% figure; plot(Data.Mode); grid on; title('Mode');
% figure; plot(Data.DualSwath); grid on; title('DualSwath');

[flag, Data.FilterIdentifier] = Read_BinVariable(Datagrams, nomDirSignal, 'FilterIdentifier', 'uint8');
if ~flag
    return
end
% figure; plot(Data.FilterIdentifier); grid on; title('FilterIdentifier');

[flag, Data.MinimumDepth] = Read_BinVariable(Datagrams, nomDirSignal, 'MinimumDepth', 'single');
if ~flag
    return
end
% figure; plot(Data.MinimumDepth); grid on; title('MinimumDepth');

[flag, Data.MaximumDepth] = Read_BinVariable(Datagrams, nomDirSignal, 'MaximumDepth', 'single');
if ~flag
    return
end
% figure; plot(Data.MaximumDepth); grid on; title('MaximumDepth');

[flag, Data.AbsorptionCoeff] = Read_BinVariable(Datagrams, nomDirSignal, 'AbsorptionCoeff', 'single');
if ~flag
    return
end
% figure; plot(Data.AbsorptionCoeff); grid on; title('AbsorptionCoeff');

[flag, Data.TransmitPulseLength] = Read_BinVariable(Datagrams, nomDirSignal, 'TransmitPulseLength', 'single');
if ~flag
    return
end
% figure; plot(Data.TransmitPulseLength); grid on; title('TransmitPulseLength');

[flag, Data.TransmitBeamWidth] = Read_BinVariable(Datagrams, nomDirSignal, 'TransmitBeamWidth', 'single');
if ~flag
    return
end
switch Datagrams.Model
    case 1002
        SonarDescription = cl_sounder('Sonar.Ident', 4, 'Sonar.SystemSerialNumber', Datagrams.SystemSerialNumber);
        TxBeamWidth = get(SonarDescription, 'Sonar.TransBeamWth');
        Data.TransmitBeamWidth(Data.TransmitBeamWidth == 6553.5) = TxBeamWidth;
    otherwise
        Data.TransmitBeamWidth(Data.TransmitBeamWidth == 6553.5) = NaN; % 2^16 - 1 pour No data
end
% figure; plot(Data.TransmitBeamWidth); grid on; title('TransmitBeamWidth');

[flag, Data.TransmitPowerMax] = Read_BinVariable(Datagrams, nomDirSignal, 'TransmitPowerMax', 'single');
if ~flag
    return
end
% figure; plot(Data.TransmitPowerMax); grid on; title('TransmitPowerMax');

[flag, Data.ReceiveBeamWidth] = Read_BinVariable(Datagrams, nomDirSignal, 'ReceiveBeamWidth', 'single');
Data.ReceiveBeamWidth(Data.ReceiveBeamWidth ==  25.5) = NaN; % 2^8 - 1 pour No data
if ~flag
    return
end
% figure; plot(Data.ReceiveBeamWidth); grid on; title('ReceiveBeamWidth');

[flag, Data.ReceiveBandWidth] = Read_BinVariable(Datagrams, nomDirSignal, 'ReceiveBandWidth', 'single');
if ~flag
    return
end
% figure; plot(Data.ReceiveBandWidth); grid on; title('ReceiveBandWidth');

[flag, Data.ReceiverFixGain] = Read_BinVariable(Datagrams, nomDirSignal, 'ReceiverFixGain', 'single');
if ~flag
    return
end
% figure; plot(Data.ReceiverFixGain); grid on; title('ReceiverFixGain');

[flag, Data.TVGCrossAngle] = Read_BinVariable(Datagrams, nomDirSignal, 'TVGCrossAngle', 'single');
if ~flag
    return
end
% figure; plot(Data.TVGCrossAngle); grid on; title('TVGCrossAngle');

[flag, Data.SourceSoundSpeed] = Read_BinVariable(Datagrams, nomDirSignal, 'SourceSoundSpeed', 'single');
if ~flag
    return
end
% figure; plot(Data.SourceSoundSpeed); grid on; title('SourceSoundSpeed');

[flag, Data.MaximumPortSwath] = Read_BinVariable(Datagrams, nomDirSignal, 'MaximumPortSwath', 'single');
if ~flag
    return
end
% figure; plot(Data.MaximumPortSwath); grid on; title('MaximumPortSwath');

[flag, Data.BeamSpacing] = Read_BinVariable(Datagrams, nomDirSignal, 'BeamSpacing', 'single');
if ~flag
    return
end
% figure; plot(Data.BeamSpacing); grid on; title('BeamSpacing');

[flag, Data.MaximumPortCoverage] = Read_BinVariable(Datagrams, nomDirSignal, 'MaximumPortCoverage', 'single');
if ~flag
    return
end
% figure; plot(Data.MaximumPortCoverage); grid on; title('MaximumPortCoverage');

[flag, Data.MaximumStarboardCoverage] = Read_BinVariable(Datagrams, nomDirSignal, 'MaximumStarboardCoverage', 'single');
if ~flag
    return
end
% figure; plot(Data.MaximumStarboardCoverage); grid on; title('MaximumStarboardCoverage');

[flag, Data.MaximumStarboardSwath] = Read_BinVariable(Datagrams, nomDirSignal, 'MaximumStarboardSwath', 'single');
if ~flag
    return
end
% figure; plot(Data.MaximumStarboardSwath); grid on; title('MaximumStarboardSwath');


[flag, YawAndPitchStabMode] = Read_BinVariable(Datagrams, nomDirSignal, 'YawAndPitchStabMode', 'uint8');
if ~flag
    return
end
Data.YawStabMode   = bitand(YawAndPitchStabMode, 3);
Data.PitchStabMode = bitshift(YawAndPitchStabMode, -7);
% figure; plot(Data.YawStabMode); grid on; title('YawStabMode');
% figure; plot(Data.PitchStabMode); grid on; title('YawAndPitchStabMode');

[flag, Data.DurotongSpeed] = Read_BinVariable(Datagrams, nomDirSignal, 'DurotongSpeed', 'uint16');
if ~flag
    return
end
% figure; plot(Data.DurotongSpeed); grid on; title('DurotongSpeed');

[flag, Data.HiLoFreqAbsCoefRatio] = Read_BinVariable(Datagrams, nomDirSignal, 'HiLoFreqAbsCoefRatio', 'uint8');
if ~flag
    return
end
% figure; plot(Data.HiLoFreqAbsCoefRatio); grid on; title('HiLoFreqAbsCoefRatio');

%% Calcul des dates et heures

Jour  = mod(Date,100);
Date  = (Date - Jour) / 100;
Mois  = mod(Date,100);
Annee = (Date - Mois) / 100;
Date = dayJma2Ifr(Jour, Mois, Annee);

Data.Time = cl_time('timeIfr', Date, NbMilliSecs);

%% G�n�ration du XML SonarScope

Info.Title                  = 'Runtime';
Info.Constructor            = 'Kongsberg';
Info.EmModel                = Datagrams.Model;
Info.SystemSerialNumber     = Datagrams.SystemSerialNumber;
Info.TimeOrigin             = '01/01/-4713';
Info.Comments               = 'Sensor sampling rate';
Info.FormatVersion          = FormatVersion;

Info.Dimensions.NbSamples     = nbSamples;

Info.Signals(1).Name          = 'Time';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = 'days since JC';
Info.Signals(end).FileName    = fullfile('Ssc_Runtime','Time.bin');
Info.Signals(end).Tag         = verifKeyWord('SounderTime');

Info.Signals(end+1).Name      = 'PingCounter';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_Runtime','PingCounter.bin');
Info.Signals(end).Tag         = verifKeyWord('SounderPingCounter');
  
Info.Signals(end+1).Name      = 'OperatorStationStatus';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'uint8';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_Runtime','OperatorStationStatus.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'ProcessingUnitStatus';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'uint8';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_Runtime','ProcessingUnitStatus.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'BSPStatus';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'uint8';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_Runtime','BSPStatus.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'SonarHeadStatus';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'uint8';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_Runtime','SonarHeadStatus.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'Mode';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'uint8';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_Runtime','Mode.bin');
Info.Signals(end).Tag         = verifKeyWord('SounderMode');

Info.Signals(end+1).Name      = 'ModeDatagram';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'uint8';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_Runtime','ModeDatagram.bin');
Info.Signals(end).Tag         = verifKeyWord('SounderMode');

Info.Signals(end+1).Name      = 'DualSwath';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'uint8';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_Runtime','DualSwath.bin');
Info.Signals(end).Tag         = verifKeyWord('SounderMode');

Info.Signals(end+1).Name      = 'FilterIdentifier';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'uint8';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_Runtime','FilterIdentifier.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'MinimumDepth';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'm';
Info.Signals(end).FileName    = fullfile('Ssc_Runtime','MinimumDepth.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'MaximumDepth';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'm';
Info.Signals(end).FileName    = fullfile('Ssc_Runtime','MaximumDepth.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'AbsorptionCoeff';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'dB/km';
Info.Signals(end).FileName    = fullfile('Ssc_Runtime','AbsorptionCoeff.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'TransmitPulseLength';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'ms';
Info.Signals(end).FileName    = fullfile('Ssc_Runtime','TransmitPulseLength.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'TransmitBeamWidth';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile('Ssc_Runtime','TransmitBeamWidth.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'TransmitPowerMax';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'dB';
Info.Signals(end).FileName    = fullfile('Ssc_Runtime','TransmitPowerMax.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'ReceiveBeamWidth'; % Attention nom fichier
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile('Ssc_Runtime','ReceiveBeamWidth.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'ReceiveBandWidth'; % Attention nom fichier
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'Hz';
Info.Signals(end).FileName    = fullfile('Ssc_Runtime','ReceiveBandWidth.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'ReceiverFixGain';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'dB';
Info.Signals(end).FileName    = fullfile('Ssc_Runtime','ReceiverFixGain.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'TVGCrossAngle';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile('Ssc_Runtime','TVGCrossAngle.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'SourceSoundSpeed'; 
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_Runtime','SourceSoundSpeed.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'MaximumPortSwath'; 
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'm';
Info.Signals(end).FileName    = fullfile('Ssc_Runtime','MaximumPortSwath.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'BeamSpacing'; 
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_Runtime','BeamSpacing.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'MaximumPortCoverage'; 
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile('Ssc_Runtime','MaximumPortCoverage.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'YawStabMode';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_Runtime','YawStabMode.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'MaximumStarboardCoverage'; 
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile('Ssc_Runtime','MaximumStarboardCoverage.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'MaximumStarboardSwath'; 
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'm';
Info.Signals(end).FileName    = fullfile('Ssc_Runtime','MaximumStarboardSwath.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'PitchStabMode';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_Runtime','PitchStabMode.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');
 
Info.Signals(end+1).Name      = 'DurotongSpeed';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_Runtime','DurotongSpeed.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'HiLoFreqAbsCoefRatio';
Info.Signals(end).Dimensions  = 'NbSamples, 1';
Info.Signals(end).Storage     = 'uint8';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_Runtime','HiLoFreqAbsCoefRatio.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

% Ajout des champs Scale_Factor, Add_Offset et Orig_Storage.
[~, Info] = addScaleOffsetStorage(Datagrams, Info);

%% Cr�ation du fichier XML d�crivant la donn�e 

nomFicXml = fullfile(nomDirRacine, 'Ssc_Runtime.xml');
xml_write(nomFicXml, Info);
flag = exist(nomFicXml, 'file');
if ~flag
    messageErreur(nomFicXml)
    return
end

%% Cr�ation du r�pertoire Runtime

nomDirAttitude = fullfile(nomDirRacine, 'Ssc_Runtime');
if ~exist(nomDirAttitude, 'dir')
    status = mkdir(nomDirAttitude);
    if ~status
        messageErreur(nomDirAttitude)
        flag = 0;
        return
    end
end

%% Cr�ation des fichiers binaires des signaux

for k=1:length(Info.Signals)
    flag = writeSignal(nomDirRacine, Info.Signals(k), Data.(Info.Signals(k).Name));
    if ~flag
        return
    end
end

flag = 1;
