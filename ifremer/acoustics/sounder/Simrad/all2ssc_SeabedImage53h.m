function flag = all2ssc_SeabedImage53h(nomDirRacine, FormatVersion, Head1, MaxDesMaxNbBeams, InstallationParameters)

nomFicXml = fullfile(nomDirRacine, 'ALL_SeabedImage53h.xml');
SonarFmt2ssc_message(nomFicXml, '.all')

%% Lecture du fichier XML d�crivant la donn�e 

flag = exist(nomFicXml, 'file');
if ~flag
    special_message('FichierNonExistant', nomFicXml)
    return
end
% edit(nomFicXml)
Datagrams = xml_read(nomFicXml);

[nomDirSignal, nomFicSignal] = fileparts(nomFicXml);
nomDirSignal = fullfile(nomDirSignal, nomFicSignal);

%{
for k=1:length(Datagrams.Variables)
    fprintf(1,'%d  %s\n', k, Datagrams.Variables(k).Name)
end
for k=1:length(Datagrams.Tables)
    fprintf(1,'%d  %s\n', k, Datagrams.Tables(k).Name)
end
%}

[flag, Date1] = Read_BinVariable(Datagrams, nomDirSignal, 'Date', 'double');
if ~flag
    return
end
% figure; plot(Date1); grid on; title('Date');

[flag, NbMilliSecs1] = Read_BinVariable(Datagrams, nomDirSignal, 'NbMilliSecs', 'double');
if ~flag
    return
end
% figure; plot(NbMilliSecs1); grid on; title('NbMilliSecs');

%% Lecture de PingCounter

[flag, PingCounter] = Read_BinVariable(Datagrams, nomDirSignal, 'PingCounter', 'single');
if ~flag
    return
end
PingCounter = unwrapPingCounterSimrad(PingCounter);
% figure; plot(PingCounter); grid on; title('PingCounter')

%% Lecture du SystemSerialNumber

[flag, SystemSerialNumber] = Read_BinVariable(Datagrams, nomDirSignal, 'SystemSerialNumber', 'single');
if ~flag
    return
end
% figure; plot(SystemSerialNumber); grid on; title('SystemSerialNumber')

%% Lecture du MeanAbsorpCoeff

[flag, MeanAbsorpCoeff] = Read_BinVariable(Datagrams, nomDirSignal, 'MeanAbsorpCoeff', 'single');
if ~flag
    return
end
% figure; plot(MeanAbsorpCoeff); grid on; title('MeanAbsorpCoeff')

%% Lecture du PulseLength

[flag, PulseLength] = Read_BinVariable(Datagrams, nomDirSignal, 'PulseLength', 'single');
if ~flag
    return
end
% figure; plot(PulseLength); grid on; title('PulseLength')

%% Lecture du Rn

[flag, Rn] = Read_BinVariable(Datagrams, nomDirSignal, 'TVGN', 'single');
if ~flag
    return
end
% figure; plot(Rn); grid on; title('Rn')

%% Lecture du TVGStart

[flag, TVGStart] = Read_BinVariable(Datagrams, nomDirSignal, 'TVGStart', 'single');
if ~flag
    return
end
% figure; plot(TVGStart); grid on; title('TVGStart')

%% Lecture du TVGStop

[flag, TVGStop] = Read_BinVariable(Datagrams, nomDirSignal, 'TVGStop', 'single');
if ~flag
    return
end
% figure; plot(TVGStop); grid on; title('TVGStop')

%% Lecture du BSN

[flag, BSN] = Read_BinVariable(Datagrams, nomDirSignal, 'BSN', 'single');
if ~flag
    return
end
% figure; plot(BSN); grid on; title('BSN')

%% Lecture du BSO

[flag, BSO] = Read_BinVariable(Datagrams, nomDirSignal, 'BSO', 'single');
if ~flag
    return
end
% figure; plot(BSO); grid on; title('BSO')
    
%% Lecture du TxBeamWidth

[flag, TxBeamWidth] = Read_BinVariable(Datagrams, nomDirSignal, 'TxBeamWidth', 'single');
if ~flag
    return
end
% figure; plot(TxBeamWidth); grid on; title('TxBeamWidth')

%% Lecture du TVGCrossOver

[flag, TVGCrossOver] = Read_BinVariable(Datagrams, nomDirSignal, 'TVGCrossOver', 'single');
if ~flag
    return
end
% figure; plot(TVGCrossOver); grid on; title('TVGCrossOver')

%% Lecture du NbBeams

[flag, NbBeams] = Read_BinVariable(Datagrams, nomDirSignal, 'NbBeams', 'single');
if ~flag
    return
end
% figure; plot(NbBeams); grid on; title('NbBeams')


nbSamples = sum(NbBeams);
[flag, IndexNumberBeam] = Read_BinTable(Datagrams, nomDirSignal, 'IndexNumberBeam', 'single', nbSamples);
if ~flag
    return
end
% figure; plot(IndexNumberBeam, '+'); grid on; title('IndexNumberBeam');


IndexNumberBeam = IndexNumberBeam + 1;
MaxDesMaxNbBeams = max([max(NbBeams) max(IndexNumberBeam) MaxDesMaxNbBeams]);

Data.IndexNumberBeam = Kongsberg_table2image(IndexNumberBeam-1, 'InfoBeamSortDirection', Datagrams.NbDatagrams, MaxDesMaxNbBeams, NbBeams, IndexNumberBeam, PingCounter, SystemSerialNumber, Head1);




% Debut Bidouille pour contourner bug fichier NIWA
% 0639_20081101_002546_raw_FromSnippets pour le quel SystemSerialNumber a
% une valeur fausse
SystemSerialNumberUnique = unique(SystemSerialNumber);
nSS = length(SystemSerialNumberUnique);
if  nSS > 1
    hi = zeros(1,nSS);
    for k=1:nSS
        hi(k) = sum(SystemSerialNumber == SystemSerialNumberUnique(k));
    end
    if (max(hi) / min(hi)) > 10
        [~, ik] = max(hi);
        SystemSerialNumber(:) = SystemSerialNumberUnique(ik);
    end
end
% Fin Bidouille pour contourner bug fichier NIWA


[TVGCrossOver, BSN, BSO] = check_BSN_BSO(InstallationParameters, TVGCrossOver, BSN, BSO, nomFicXml);

Data.PingCounter        = Kongsberg_table2signal(PingCounter,        Datagrams.NbDatagrams, PingCounter, SystemSerialNumber, Head1);
Data.SystemSerialNumber = Kongsberg_table2signal(SystemSerialNumber, Datagrams.NbDatagrams, PingCounter, SystemSerialNumber, Head1);
Data.MeanAbsorpCoeff    = Kongsberg_table2signal(MeanAbsorpCoeff,    Datagrams.NbDatagrams, PingCounter, SystemSerialNumber, Head1);
Data.PulseLength        = Kongsberg_table2signal(PulseLength,        Datagrams.NbDatagrams, PingCounter, SystemSerialNumber, Head1);
Data.TVGN               = Kongsberg_table2signal(Rn,                 Datagrams.NbDatagrams, PingCounter, SystemSerialNumber, Head1);
Data.TVGStart           = Kongsberg_table2signal(TVGStart,           Datagrams.NbDatagrams, PingCounter, SystemSerialNumber, Head1);
Data.TVGStop            = Kongsberg_table2signal(TVGStop,            Datagrams.NbDatagrams, PingCounter, SystemSerialNumber, Head1);
Data.BSN                = Kongsberg_table2signal(BSN,                Datagrams.NbDatagrams, PingCounter, SystemSerialNumber, Head1);
Data.BSO                = Kongsberg_table2signal(BSO,                Datagrams.NbDatagrams, PingCounter, SystemSerialNumber, Head1);
Data.TxBeamWidth        = Kongsberg_table2signal(TxBeamWidth,        Datagrams.NbDatagrams, PingCounter, SystemSerialNumber, Head1);
Data.TVGCrossOver       = Kongsberg_table2signal(TVGCrossOver,       Datagrams.NbDatagrams, PingCounter, SystemSerialNumber, Head1);
Date                    = Kongsberg_table2signal(Date1,              Datagrams.NbDatagrams, PingCounter, SystemSerialNumber, Head1);
NbMilliSecs             = Kongsberg_table2signal(NbMilliSecs1,       Datagrams.NbDatagrams, PingCounter, SystemSerialNumber, Head1);
Data.NbBeams            = Kongsberg_table2signal(NbBeams,            Datagrams.NbDatagrams, PingCounter, SystemSerialNumber, Head1);

% D�but rajout JMA le 20/09/2016 avec mise en commentaire Kongsberg_table2signal
% subNaN = find(isnan(sum(Data.PingCounter,2)));
subNaN = find(all(isnan(Data.PingCounter),2));  % Modif JMA le 14/03/2019 pour fichier 0021_20190306_040731_Belgica.all
Data.PingCounter(subNaN,:)        = [];
Data.SystemSerialNumber(subNaN,:) = [];
Data.MeanAbsorpCoeff(subNaN,:)    = [];
Data.PulseLength(subNaN,:)        = [];
Data.TVGN(subNaN,:)               = [];
Data.TVGStart(subNaN,:)           = [];
Data.TVGStop(subNaN,:)            = [];
Data.BSN(subNaN,:)                = [];
Data.BSO(subNaN,:)                = [];
Data.TxBeamWidth(subNaN,:)        = [];
Data.TVGCrossOver(subNaN,:)       = [];
Date(subNaN,:)                    = [];
NbMilliSecs(subNaN,:)             = [];
Data.NbBeams(subNaN,:)            = [];
% Fin rajout JMA le 20/09/2016 avec mise en commentaire Kongsberg_table2signal
Data.IndexNumberBeam(subNaN,:,:) = [];

% Correct PingCounter in case of dual sounder but only the starboard is on
% (correspondances between pings are calculated on indice=1 on second
% dimension
Data.PingCounter     = repairPingCounterInCaseDualSounderButOnlyStarnoardRunning(Data.PingCounter);
Data.MeanAbsorpCoeff = repairPingCounterInCaseDualSounderButOnlyStarnoardRunning(Data.MeanAbsorpCoeff);
Data.PulseLength     = repairPingCounterInCaseDualSounderButOnlyStarnoardRunning(Data.PulseLength);
Data.TVGN            = repairPingCounterInCaseDualSounderButOnlyStarnoardRunning(Data.TVGN);
Data.TVGStart        = repairPingCounterInCaseDualSounderButOnlyStarnoardRunning(Data.TVGStart);
Data.TVGStop         = repairPingCounterInCaseDualSounderButOnlyStarnoardRunning(Data.TVGStop);
Data.BSN             = repairPingCounterInCaseDualSounderButOnlyStarnoardRunning(Data.BSN);
Data.BSO             = repairPingCounterInCaseDualSounderButOnlyStarnoardRunning(Data.BSO);
Data.TxBeamWidth     = repairPingCounterInCaseDualSounderButOnlyStarnoardRunning(Data.TxBeamWidth);
Data.TVGCrossOver    = repairPingCounterInCaseDualSounderButOnlyStarnoardRunning(Data.TVGCrossOver);
% Data.NbBeams = repairPingCounterInCaseDualSounderButOnlyStarnoardRunning(Data.NbBeams);

%% InfoBeamNbSamples

[flag, X] = Read_BinTable(Datagrams, nomDirSignal, 'InfoBeamNbSamples', 'single', nbSamples);
if ~flag
    return
end
Y = Kongsberg_table2image_Additive(X, Datagrams.NbDatagrams, MaxDesMaxNbBeams, NbBeams, IndexNumberBeam, PingCounter, SystemSerialNumber, Head1);
Data.NbSamplesPerPing = sum(Y,2);
clear Y
Data.InfoBeamNbSamples = Kongsberg_table2image(X, 'InfoBeamNbSamples', Datagrams.NbDatagrams, MaxDesMaxNbBeams, NbBeams, IndexNumberBeam, PingCounter, SystemSerialNumber, Head1);
Data.InfoBeamNbSamples(subNaN,:) = [];
% figure; imagesc(Data.InfoBeamNbSamples); title('InfoBeamNbSamples'); axis xy; colorbar;

%% InfoBeamSortDirection

[flag, X] = Read_BinTable(Datagrams, nomDirSignal, 'InfoBeamSortDirection', 'single', nbSamples);
if ~flag
    return
end
Data.InfoBeamSortDirection = Kongsberg_table2image(X, 'InfoBeamSortDirection', Datagrams.NbDatagrams, MaxDesMaxNbBeams, NbBeams, IndexNumberBeam, PingCounter, SystemSerialNumber, Head1);
Data.InfoBeamSortDirection(subNaN,:) = [];
% figure; imagesc(Data.InfoBeamSortDirection); title('InfoBeamSortDirection'); axis xy; colorbar;

%% InfoBeamCentralSample

[flag, X] = Read_BinTable(Datagrams, nomDirSignal, 'InfoBeamCentralSample', 'single', nbSamples);
if ~flag
    return
end
Data.InfoBeamCentralSample = Kongsberg_table2image(X, 'InfoBeamCentralSample', Datagrams.NbDatagrams, MaxDesMaxNbBeams, NbBeams, IndexNumberBeam, PingCounter, SystemSerialNumber, Head1);
Data.InfoBeamCentralSample(subNaN,:) = [];
% figure; imagesc(Data.InfoBeamCentralSample); title('InfoBeamCentralSample'); axis xy; colorbar;

%% Entries

% % k = findIndVariable(Datagrams.Tables, 'Entries');
% % [flag, Entries] = readSignal(nomDirSignal, Datagrams.Tables(k), []);
% % Data.Entries = code2val(Entries, Datagrams.Tables(k), 'single');
% % 

%% Correctif JMA le 20/02/2018 pour fichier 0000_20180502_112500_Belgica.all

nbPings = min([size(Data.IndexNumberBeam,1) 
size(Data.PingCounter,1) 
size(Data.SystemSerialNumber,1) 
size(Data.MeanAbsorpCoeff,1) 
size(Data.PulseLength,1) 
size(Data.TVGN,1) 
size(Data.TVGStart,1) 
size(Data.TVGStop,1) 
size(Data.BSN,1) 
size(Data.BSO,1) 
size(Data.TxBeamWidth,1) 
size(Data.TVGCrossOver,1) 
size(Data.NbBeams,1) 
size(Data.NbSamplesPerPing,1)]);
subPings = 1:nbPings;
Data.IndexNumberBeam    = Data.IndexNumberBeam(subPings,:);
Data.PingCounter        = Data.PingCounter(subPings,:);
Data.SystemSerialNumber = Data.SystemSerialNumber(subPings,:);
Data.MeanAbsorpCoeff    = Data.MeanAbsorpCoeff(subPings,:);
Data.PulseLength        = Data.PulseLength(subPings,:);
Data.TVGN               = Data.TVGN(subPings,:);
Data.TVGStart           = Data.TVGStart(subPings,:);
Data.TVGStop            = Data.TVGStop(subPings,:);
Data.BSN                = Data.BSN(subPings,:);
Data.BSO                = Data.BSO(subPings,:);
Data.TxBeamWidth        = Data.TxBeamWidth(subPings,:);
Data.TVGCrossOver       = Data.TVGCrossOver(subPings,:);
Data.NbBeams            = Data.NbBeams(subPings,:);
Data.NbSamplesPerPing   = Data.NbSamplesPerPing(subPings,:);

%% Calcul des dates et heures

Jour  = mod(Date,100);
Date  = (Date - Jour) / 100;
Mois  = mod(Date,100);
Annee = (Date - Mois) / 100;
Date = dayJma2Ifr(Jour, Mois, Annee);

Data.Time = cl_time('timeIfr', Date, NbMilliSecs);

%% G�n�ration du XML SonarScope

Info.Title                   = 'SeabedImage53h';
Info.Constructor             = 'Kongsberg';
Info.EmModel                 = Datagrams.Model;
Info.ListeSystemSerialNumber = num2str(unique(Data.SystemSerialNumber(:))');
Info.SystemSerialNumber      = Datagrams.SystemSerialNumber;
Info.TimeOrigin              = '01/01/-4713';
Info.Comments                = 'Sensor sampling rate';
Info.FormatVersion           = FormatVersion;

Info.Dimensions.nbPings       = size(Data.InfoBeamNbSamples, 1);
Info.Dimensions.nbBeams       = size(Data.InfoBeamNbSamples, 2);
Info.Dimensions.nbSounders    = size(Data.PingCounter,2);
% Info.Dimensions.NbSamples     = length(Data.Entries);

Info.Signals(1).Name          = 'Time';
Info.Signals(end).Dimensions  = 'nbPings, nbSounders';
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = 'days since JC';
Info.Signals(end).FileName    = fullfile('Ssc_SeabedImage53h','Time.bin');
Info.Signals(end).Tag         = verifKeyWord('GPSTime');

Info.Signals(end+1).Name      = 'PingCounter';
Info.Signals(end).Dimensions  = 'nbPings, nbSounders';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile('Ssc_SeabedImage53h','PingCounter.bin');
Info.Signals(end).Tag         = verifKeyWord('SounderPingCounter');

Info.Signals(end+1).Name      = 'SystemSerialNumber';
Info.Signals(end).Dimensions  = 'nbPings, nbSounders';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile('Ssc_SeabedImage53h','SystemSerialNumber.bin');
Info.Signals(end).Tag         = verifKeyWord('SounderSerialNumber');

Info.Signals(end+1).Name      = 'MeanAbsorpCoeff';
Info.Signals(end).Dimensions  = 'nbPings, nbSounders';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile('Ssc_SeabedImage53h','MeanAbsorpCoeff.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'PulseLength';
Info.Signals(end).Dimensions  = 'nbPings, nbSounders';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile('Ssc_SeabedImage53h','PulseLength.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'TVGN';
Info.Signals(end).Dimensions  = 'nbPings, nbSounders';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile('Ssc_SeabedImage53h','TVGN.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'TVGStart';
Info.Signals(end).Dimensions  = 'nbPings, nbSounders';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile('Ssc_SeabedImage53h','TVGStart.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'TVGStop';
Info.Signals(end).Dimensions  = 'nbPings, nbSounders';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile('Ssc_SeabedImage53h','TVGStop.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'BSN';
Info.Signals(end).Dimensions  = 'nbPings, nbSounders';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile('Ssc_SeabedImage53h','BSN.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'BSO';
Info.Signals(end).Dimensions  = 'nbPings, nbSounders';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile('Ssc_SeabedImage53h','BSO.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'TxBeamWidth';
Info.Signals(end).Dimensions  = 'nbPings, nbSounders';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile('Ssc_SeabedImage53h','TxBeamWidth.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'TVGCrossOver';
Info.Signals(end).Dimensions  = 'nbPings, nbSounders';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile('Ssc_SeabedImage53h','TVGCrossOver.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'NbBeams';
Info.Signals(end).Dimensions  = 'nbPings, nbSounders';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_SeabedImage53h','NbBeams.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'NbSamplesPerPing';
Info.Signals(end).Dimensions  = 'nbPings, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_SeabedImage53h','NbSamplesPerPing.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Images(1).Name           = 'IndexNumberBeam';
Info.Images(end).Dimensions   = 'nbPings, nbBeams';
Info.Images(end).Index        = 'nbPings, nbBeams';
Info.Images(end).Storage      = 'single';
Info.Images(end).Unit         = '';
Info.Images(end).FileName     = fullfile('Ssc_SeabedImage53h','IndexNumberBeam.bin');
Info.Images(end).Tag          = verifKeyWord('SounderBeamNumber');

Info.Images(end+1).Name       = 'InfoBeamSortDirection';
Info.Images(end).Dimensions   = 'nbPings, nbBeams';
Info.Images(end).Index        = 'nbPings, nbBeams';
Info.Images(end).Storage      = 'single';
Info.Images(end).Unit         = 'm';
Info.Images(end).FileName     = fullfile('Ssc_SeabedImage53h','InfoBeamSortDirection.bin');
Info.Images(end).Tag          = verifKeyWord('TODO');

Info.Images(end+1).Name       = 'InfoBeamNbSamples';
Info.Images(end).Dimensions   = 'nbPings, nbBeams';
Info.Images(end).Index        = 'nbPings, nbBeams';
Info.Images(end).Storage      = 'single';
Info.Images(end).Unit         = 'm';
Info.Images(end).FileName     = fullfile('Ssc_SeabedImage53h','InfoBeamNbSamples.bin');
Info.Images(end).Tag          = verifKeyWord('TODO');

Info.Images(end+1).Name       = 'InfoBeamCentralSample';
Info.Images(end).Dimensions   = 'nbPings, nbBeams';
Info.Images(end).Index        = 'nbPings, nbBeams';
Info.Images(end).Storage      = 'single';
Info.Images(end).Unit         = 'm';
Info.Images(end).FileName     = fullfile('Ssc_SeabedImage53h','InfoBeamCentralSample.bin');
Info.Images(end).Tag          = verifKeyWord('TODO');

% Ajout des champs Scale_Factor, Add_Offset et Orig_Storage.
[~, Info] = addScaleOffsetStorage(Datagrams, Info);

%% Cr�ation du fichier XML d�crivant la donn�e 

nomFicXml = fullfile(nomDirRacine, 'Ssc_SeabedImage53h.xml');
xml_write(nomFicXml, Info);
flag = exist(nomFicXml, 'file');
if ~flag
    messageErreur(nomFicXml)
    return
end

%% Cr�ation du r�pertoire SeabedImage53h

nomDirAttitude = fullfile(nomDirRacine, 'Ssc_SeabedImage53h');
if ~exist(nomDirAttitude, 'dir')
    status = mkdir(nomDirAttitude);
    if ~status
        messageErreur(nomDirAttitude)
        flag = 0;
        return
    end
end

%% Cr�ation des fichiers binaires des signaux

for k=1:length(Info.Signals)
    flag = writeSignal(nomDirRacine, Info.Signals(k), Data.(Info.Signals(k).Name));
    if ~flag
        return
    end
end

%% Cr�ation des fichiers binaires des images

for k=1:length(Info.Images)
    flag = writeImage(nomDirRacine, Info.Images(k), Data.(Info.Images(k).Name));
    if ~flag
        return
    end
end

flag = 1;



function [TVGCrossOver, BSN, BSO] = check_BSN_BSO(InstallationParameters, TVGCrossOver, BSN, BSO, nomFicXml)

if isempty(InstallationParameters)
    str1 = sprintf('Pas de "InstallationParameters" dans "all2ssc_SeabedImage53h". Pouvez-vous envoyer le fichier.all correspondant � "%s" � JMA ?', ...
        nomFicXml);
    str2 = sprintf('No "InstallationParameters" in "all2ssc_SeabedImage53h". Please send the .all file corresponding to .all "%s" to JMA.', ...
        nomFicXml);
     my_warndlg(Lang(str1,str2), 0, 'Tag', 'InstallationParametersEmModelAVerifier');
else
    if ~isfield(InstallationParameters, 'PSV') % Cas de fichiers EM2000 des italiens de Pise
        return
    end
    
    switch InstallationParameters.EmModel
        case 120
            switch InstallationParameters.PSV(1:5)
                case {'1.0.9'} 
                    % Ca a l'air bon comme c'est
                case {'2.0.1'}
                    % Ca a l'air bon comme c'est
                otherwise
                    afficherMessage(InstallationParameters, nomFicXml)
            end
            
        case 300
            switch InstallationParameters.PSV(1:5)
                case {'2.0.5'}
                    % Ca a l'air bon comme c'est
                otherwise
                    afficherMessage(InstallationParameters, nomFicXml)
            end
            
        case 1002
            switch InstallationParameters.PSV(1:5)
                case {'1.1.8'} % Cas du fichier EM1002 du Belgica LEKEITIO_EM1002/0002_20110618_141326_raw.all
                    % Ca a l'air bon comme c'est
                case {'1.2.0'} % Cas du fichier EM1002 du Belgica LEKEITIO_EM1002/0002_20110618_141326_raw.all
                    % Ca a l'air bon comme c'est
                case {'1.3.3'} % Cas du fichier Univ-Bordeaux 0002_20130905_042733_BBP.all
                    % Ca a l'air bon comme c'est
                otherwise
                    afficherMessage(InstallationParameters, nomFicXml)
            end
            
        case 2000
            switch InstallationParameters.PSV(1:5)
                case {'0.4.0'} % Cas du fichier 0013_20091115_120624_raw.all
                    % Ca a l'air bon comme c'est
                otherwise
                    afficherMessage(InstallationParameters, nomFicXml)
            end
            
        case 3002
            switch InstallationParameters.PSV(1:5)
                case {'3.0.5'} % Cas du fichier EM3000D du NIWA 0001_20081027_184032_raw
                    TVGCrossOver = TVGCrossOver * 10;
                otherwise
                    afficherMessage(InstallationParameters, nomFicXml)
            end
            
        case 3020
            switch InstallationParameters.PSV(1:5)
                case '1.6.4' % Cas du fichier EM3002D du SHOM R:\FromHerveBisquay\GOBETAS\SHOM-EM3002-151004
                    % Ca a l'air bon comme c'est
                case '2.0.1' % Cas du fichier EM3002D du NIWA 0025_20100427_024418_Ikatere
                    % TODO car le fichier n'a pas permis de statuer
                    afficherMessage(InstallationParameters, nomFicXml)
                case '2.0.2' % Cas du fichier EM3002D du Belgica LEKEITIO_EM3002D\0051_20110619_040230_Belgica
                    % Ca a l'air bon comme c'est
                case '2.0.5' % Cas du fichier EM3002D du Belgica LEKEITIO_EM3002D\0051_20110619_040230_Belgica
                    % Ca a l'air bon comme c'est
                case '2.1.1'
                    % Ca a l'air bon comme c'est
                case '2.1.2'
                    % Ca a l'air bon comme c'est
                otherwise
                    afficherMessage(InstallationParameters, nomFicXml)
            end
        otherwise
            afficherMessage(InstallationParameters, nomFicXml)
    end
end

function afficherMessage(InstallationParameters, nomFicXml)
str1 = sprintf('Sondeur EM%d, PSV=%s: il faut v�rifier si la correction du sp�culaire est r�alis�e correctement. Pouvez-vous envoyer le "%s" � JMA ?', ...
    InstallationParameters.EmModel, InstallationParameters.PSV(1:5), nomFicXml);
str2 = sprintf('Sounder EM%d, PSV=%s : the way the specular correction is done must be checked. Please send "%s" to JMA.', ...
    InstallationParameters.EmModel, InstallationParameters.PSV(1:5), nomFicXml);
my_warndlg(Lang(str1,str2), 0, 'Tag', 'InstallationParametersEmModelAVerifier');
