function flag = all2ssc_SeabedImage59h(nomDirRacine,  FormatVersion, Head1, InstallationParameters)

nomFicXml = fullfile(nomDirRacine, 'ALL_SeabedImage59h.xml');
SonarFmt2ssc_message(nomFicXml, '.all')

%% Lecture du fichier XML d�crivant la donn�e 

flag = exist(nomFicXml, 'file');
if ~flag
    special_message('FichierNonExistant', nomFicXml)
    return
end
% edit(nomFicXml)
Datagrams = xml_read(nomFicXml);

[nomDirSignal, nomFicSignal] = fileparts(nomFicXml);
nomDirSignal = fullfile(nomDirSignal, nomFicSignal);

%{
for k=1:length(Datagrams.Variables)
    fprintf(1,'%d  %s\n', k, Datagrams.Variables(k).Name)
end
for k=1:length(Datagrams.Tables)
    fprintf(1,'%d  %s\n', k, Datagrams.Tables(k).Name)
end
%}

[flag, Date1] = Read_BinVariable(Datagrams, nomDirSignal, 'Date', 'double');
if ~flag
    return
end
% figure; plot(Date1); grid on; title('Date');

[flag, NbMilliSecs1] = Read_BinVariable(Datagrams, nomDirSignal, 'NbMilliSecs', 'double');
if ~flag
    return
end
% figure; plot(NbMilliSecs1); grid on; title('NbMilliSecs');

%% Lecture de PingCounter

[flag, PingCounter] = Read_BinVariable(Datagrams, nomDirSignal, 'PingCounter', 'single');
if ~flag
    return
end
PingCounter = unwrapPingCounterSimrad(PingCounter);
% figure; plot(PingCounter); grid on; title('PingCounter')

%% Lecture du SystemSerialNumber

[flag, SystemSerialNumber] = Read_BinVariable(Datagrams, nomDirSignal, 'SystemSerialNumber', 'single');
if ~flag
    return
end
% figure; plot(SystemSerialNumber); grid on; title('SystemSerialNumber')

%% Lecture du SamplingRate

[flag, SamplingRate] = Read_BinVariable(Datagrams, nomDirSignal, 'SamplingRate', 'single');
if ~flag
    return
end
% figure; plot(SamplingRate); grid on; title('SamplingRate')

%% Lecture du Rn

[flag, Rn] = Read_BinVariable(Datagrams, nomDirSignal, 'TVGN', 'single');
if ~flag
    return
end
% figure; plot(Rn); grid on; title('Rn')

% % -------------------
% % Lecture du TVGStart
% 
% [flag, TVGStart] = Read_BinVariable(Datagrams, nomDirSignal, 'TVGStart', 'single');
% if ~flag
%     return
% end
% %    figure; plot(TVGStart); grid on; title('TVGStart')

% % ------------------
% % Lecture du TVGStop
% 
% [flag, TVGStop] = Read_BinVariable(Datagrams, nomDirSignal, 'TVGStop', 'single');
% if ~flag
%     return
% end
% %figure; plot(TVGStop); grid on; title('TVGStop')

%% Lecture du BSN

[flag, BSN] = Read_BinVariable(Datagrams, nomDirSignal, 'BSN', 'single');
if ~flag
    return
end
% figure; plot(BSN); grid on; title('BSN')

%% Lecture du BSO

[flag, BSO] = Read_BinVariable(Datagrams, nomDirSignal, 'BSO', 'single');
if ~flag
    return
end
% figure; plot(BSO); grid on; title('BSO')

%% Lecture du TxBeamWidth

[flag, TxBeamWidth] = Read_BinVariable(Datagrams, nomDirSignal, 'TxBeamWidth', 'single');
if ~flag
    return
end
% figure; plot(TxBeamWidth); grid on; title('TxBeamWidth')

%% Lecture du TVGCrossOver

[flag, TVGCrossOver] = Read_BinVariable(Datagrams, nomDirSignal, 'TVGCrossOver', 'single');
if ~flag
    return
end
% figure; plot(TVGCrossOver); grid on; title('TVGCrossOver')

%% Lecture du NbBeams

[flag, NbBeams] = Read_BinVariable(Datagrams, nomDirSignal, 'NbBeams', 'single');
if ~flag
    return
end
% figure; plot(NbBeams); grid on; title('NbBeams')


nbSamples = sum(NbBeams);
MaxDesMaxNbBeams = max(NbBeams);
% BeamNumber = repmat(1:MaxDesMaxNbBeams, Datagrams.NbDatagrams, 1);
% BeamNumber = BeamNumber';
% BeamNumber = BeamNumber(:);
BeamNumber = [];

% Test

if isempty(InstallationParameters)
    str1 = 'InstallationParametersEstVide est vide dans "all2ssc_SeabedImage59h"';
    str2 = 'InstallationParametersEstVide is empty in "all2ssc_SeabedImage59h"';
    my_warndlg(Lang(str1,str2), 1, 'Tag', 'InstallationParametersEstVide');
else
    switch InstallationParameters.EmModel
        case 122
            switch InstallationParameters.PSV(1:5)
                case {'1.1.1'; '1.1.2'; '1.1.3'; '1.1.4'}
                    BSN = BSN * 10;
                    BSO = BSO * 10;
                case '1.1.5'
                    %  Bug corrig� � partir de cette version
                otherwise
            end
        case 302
            switch InstallationParameters.PSV(1:5)
                case {'1.4.1'; '1.4.2'; '1.4.3'; '1.4.4'; '1.4.5'; '1.4.6'; '1.4.7'}
                    BSN = BSN * 10;
                    BSO = BSO * 10;
%                 case '1.4.8'
                    %  Bug corrig� � partir de cette version
                otherwise
            end
        case 710
            switch InstallationParameters.PSV(1:5)
                case {'1.4.1'; '1.4.2'; '1.4.3'; '1.4.4'; '1.4.5'; '1.4.6'; '1.4.7'; '1.4.8'; '2.0.9'}
                    BSN = BSN * 10;
                    BSO = BSO * 10;
%                 case '2.2.1'
                    %  Bug corrig� (donn�es du SHOM)
                otherwise
            end
        case 850 % C'est le Pr�cieux !!!
            switch InstallationParameters.PSV(1:5)
                case {'1.0.6'}
                    BSN = BSN * 10;
                    BSO = BSO * 10;
                    Rn = Rn * 2; % TODO : communiquer cette erreur � Kongsberg
%                 case '2.2.1'
                    %  Bug corrig� (donn�es du SHOM)
                otherwise
            end
    end
end


Data.PingCounter        = Kongsberg_table2signal(PingCounter,        Datagrams.NbDatagrams, PingCounter, SystemSerialNumber, Head1);
Data.SystemSerialNumber = Kongsberg_table2signal(SystemSerialNumber, Datagrams.NbDatagrams, PingCounter, SystemSerialNumber, Head1);
% Data.MeanAbsorpCoeff  = Kongsberg_table2signal(MeanAbsorpCoeff,    Datagrams.NbDatagrams, PingCounter, SystemSerialNumber, Head1);
% Data.PulseLength      = Kongsberg_table2signal(PulseLength,        Datagrams.NbDatagrams, PingCounter, SystemSerialNumber, Head1);
Data.TVGN               = Kongsberg_table2signal(Rn,                 Datagrams.NbDatagrams, PingCounter, SystemSerialNumber, Head1);
Data.SamplingRate       = Kongsberg_table2signal(SamplingRate,       Datagrams.NbDatagrams, PingCounter, SystemSerialNumber, Head1);
% Data.TVGStart         = Kongsberg_table2signal(TVGStart,           Datagrams.NbDatagrams, PingCounter, SystemSerialNumber, Head1);
% Data.TVGStop          = Kongsberg_table2signal(TVGStop,            Datagrams.NbDatagrams, PingCounter, SystemSerialNumber, Head1);
Data.BSN                = Kongsberg_table2signal(BSN,                Datagrams.NbDatagrams, PingCounter, SystemSerialNumber, Head1);
Data.BSO                = Kongsberg_table2signal(BSO,                Datagrams.NbDatagrams, PingCounter, SystemSerialNumber, Head1);
Data.TxBeamWidth        = Kongsberg_table2signal(TxBeamWidth,        Datagrams.NbDatagrams, PingCounter, SystemSerialNumber, Head1);
Data.TVGCrossOver       = Kongsberg_table2signal(TVGCrossOver,       Datagrams.NbDatagrams, PingCounter, SystemSerialNumber, Head1);
Date                    = Kongsberg_table2signal(Date1,              Datagrams.NbDatagrams, PingCounter, SystemSerialNumber, Head1);
NbMilliSecs             = Kongsberg_table2signal(NbMilliSecs1,       Datagrams.NbDatagrams, PingCounter, SystemSerialNumber, Head1);
Data.NbBeams            = Kongsberg_table2signal(NbBeams,            Datagrams.NbDatagrams, PingCounter, SystemSerialNumber, Head1);

% D�but rajout JMA le 20/09/2016 avec mise en commentaire Kongsberg_table2signal
subNaN = find(isnan(sum(Data.PingCounter,2)));
Data.PingCounter(subNaN,:)        = [];
Data.SystemSerialNumber(subNaN,:) = [];
Data.TVGN(subNaN,:)               = [];
Data.SamplingRate(subNaN,:)       = [];
Data.BSN(subNaN,:)                = [];
Data.BSO(subNaN,:)                = [];
Data.TxBeamWidth(subNaN,:)        = [];
Data.TVGCrossOver(subNaN,:)       = [];
Date(subNaN,:)                    = [];
NbMilliSecs(subNaN,:)             = [];
Data.NbBeams(subNaN,:)            = [];
% Fin rajout JMA le 20/09/2016 avec mise en commentaire Kongsberg_table2signal

Data.TVGNRecomputed = Data.TVGN;

%% InfoBeamSortDirection

[flag, X] = Read_BinTable(Datagrams, nomDirSignal, 'InfoBeamSortDirection', 'single', nbSamples);
if ~flag
    return
end
Data.InfoBeamSortDirection = Kongsberg_table2image(X, 'InfoBeamSortDirection', Datagrams.NbDatagrams, MaxDesMaxNbBeams, NbBeams, BeamNumber, PingCounter, SystemSerialNumber, Head1);
Data.InfoBeamSortDirection(subNaN,:) = [];
% figure; imagesc(Data.InfoBeamSortDirection); title('InfoBeamSortDirection'); axis xy; colorbar;

%% InfoBeamNbSamples

[flag, X] = Read_BinTable(Datagrams, nomDirSignal, 'InfoBeamNbSamples', 'single', nbSamples);
if ~flag
    return
end
Data.InfoBeamNbSamples = Kongsberg_table2image(X, 'InfoBeamNbSamples', Datagrams.NbDatagrams, MaxDesMaxNbBeams, NbBeams, BeamNumber, PingCounter, SystemSerialNumber, Head1);


% D�but ajout JMA le 20/01/2018 pour fichiers Simon Stevin 0110_20171122_042859_SimonStevin.all , ...
if ~isempty(subNaN)
    InfoBeamNbSamples = double(Data.InfoBeamNbSamples(:,:));
    InfoBeamNbSamples(isnan(InfoBeamNbSamples)) = 0;
    nbSamplePings = sum(InfoBeamNbSamples,2);
    cumNbSamplePings = cumsum(nbSamplePings);
    indEntriesPingDeb = [1; cumNbSamplePings(1:end-1)+1];
    indEntriesPingFin = cumNbSamplePings;
    
    k1 = findIndVariable(Datagrams.Tables, 'Entries');
    % [flag, Entries] = readSignal(nomDirSignal, Datagrams.Tables(k1), Datagrams.NbDatagrams);
    [flag, Entries] = readSignal(nomDirSignal, Datagrams.Tables(k1), []);
    if ~flag
        return
    end
    subEntriesToRemove = [];
    for k2=1:length(subNaN)
        subEntriesToRemove = [subEntriesToRemove indEntriesPingDeb(subNaN(k2)):indEntriesPingFin(subNaN(k2))]; %#ok<AGROW>
    end
    Entries(subEntriesToRemove) = [];
    flag = writeSignal(nomDirSignal, Datagrams.Tables(k1), Entries);
    if ~flag
        return
    end
end
% Fin ajout JMA le 20/01/2018 pour fichiers Simon Stevin 0110_20171122_042859_SimonStevin.all , ...



Data.InfoBeamNbSamples(subNaN,:) = [];
% figure; imagesc(Data.InfoBeamNbSamples); title('InfoBeamNbSamples'); axis xy; colorbar;

%% InfoBeamCentralSample

[flag, X] = Read_BinTable(Datagrams, nomDirSignal, 'InfoBeamCentralSample', 'single', nbSamples);
if ~flag
    return
end
Data.InfoBeamCentralSample = Kongsberg_table2image(X, 'InfoBeamCentralSample', Datagrams.NbDatagrams, MaxDesMaxNbBeams, NbBeams, BeamNumber, PingCounter, SystemSerialNumber, Head1);
Data.InfoBeamCentralSample(subNaN,:) = [];
% figure; imagesc(Data.InfoBeamCentralSample); title('InfoBeamCentralSample'); axis xy; colorbar;

%% InfoBeamDetectionInfo

[flag, X] = Read_BinTable(Datagrams, nomDirSignal, 'InfoBeamDetectionInfo', 'single', nbSamples);
if ~flag
    return
end
Data.InfoBeamDetectionInfo = Kongsberg_table2image(X, 'InfoBeamDetectionInfo', Datagrams.NbDatagrams, MaxDesMaxNbBeams, NbBeams, BeamNumber, PingCounter, SystemSerialNumber, Head1);
Data.InfoBeamDetectionInfo(subNaN,:) = [];
% figure; imagesc(Data.InfoBeamDetectionInfo); title('InfoBeamDetectionInfo'); axis xy; colorbar;

%% Entries

% k = findIndVariable(Datagrams.Tables, 'Entries');
% % [flag, Entries] = readSignal(nomDirSignal, Datagrams.Tables(k), Datagrams.NbDatagrams);
% [flag, Entries] = readSignal(nomDirSignal, Datagrams.Tables(k), []);
% Data.Entries = code2val(Entries, Datagrams.Tables(k), 'single');

%% Calcul des dates et heures

Jour  = mod(Date,100);
Date  = (Date - Jour) / 100;
Mois  = mod(Date,100);
Annee = (Date - Mois) / 100;
Date = dayJma2Ifr(Jour, Mois, Annee);

Data.Time = cl_time('timeIfr', Date, NbMilliSecs);

%% G�n�ration du XML SonarScope

Info.Title                  = 'SeabedImage59h';
Info.Constructor            = 'Kongsberg';
Info.EmModel                  = Datagrams.Model;
Info.ListeSystemSerialNumber = num2str(unique(Data.SystemSerialNumber(:))');
Info.SystemSerialNumber     = Datagrams.SystemSerialNumber;
Info.TimeOrigin             = '01/01/-4713';
Info.Comments               = 'Sensor sampling rate';
Info.Version                = 'V2';
Info.FormatVersion          = FormatVersion;

Info.Dimensions.nbPings       = size(Data.InfoBeamNbSamples, 1);
Info.Dimensions.nbBeams       = size(Data.InfoBeamNbSamples, 2);
Info.Dimensions.nbSounders    = size(Data.PingCounter,2);

Info.Signals(1).Name          = 'Time';
Info.Signals(end).Dimensions  = 'nbPings, nbSounders';
Info.Signals(1).Storage       = 'double';
Info.Signals(end).Unit        = 'days since JC';
Info.Signals(end).FileName    = fullfile('Ssc_SeabedImage59h','Time.bin');
Info.Signals(end).Tag         = verifKeyWord('GPSTime');

Info.Signals(end+1).Name      = 'PingCounter';
Info.Signals(end).Dimensions  = 'nbPings, nbSounders';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile('Ssc_SeabedImage59h','PingCounter.bin');
Info.Signals(end).Tag         = verifKeyWord('SounderPingCounter');

Info.Signals(end+1).Name      = 'SystemSerialNumber';
Info.Signals(end).Dimensions  = 'nbPings, nbSounders';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile('Ssc_SeabedImage59h','SystemSerialNumber.bin');
Info.Signals(end).Tag         = verifKeyWord('SounderSerialNumber');

Info.Signals(end+1).Name      = 'SamplingRate';
Info.Signals(end).Dimensions  = 'nbPings, nbSounders';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'Hz';
Info.Signals(end).FileName    = fullfile('Ssc_SeabedImage59h','SamplingRate.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'TVGN';
Info.Signals(end).Dimensions  = 'nbPings, nbSounders';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile('Ssc_SeabedImage59h','TVGN.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'TVGNRecomputed';
Info.Signals(end).Dimensions  = 'nbPings, nbSounders';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile('Ssc_SeabedImage59h','TVGNRecomputed.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'BSN';
Info.Signals(end).Dimensions  = 'nbPings, nbSounders';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile('Ssc_SeabedImage59h','BSN.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'BSO';
Info.Signals(end).Dimensions  = 'nbPings, nbSounders';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile('Ssc_SeabedImage59h','BSO.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'TxBeamWidth';
Info.Signals(end).Dimensions  = 'nbPings, nbSounders';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile('Ssc_SeabedImage59h','TxBeamWidth.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'TVGCrossOver';
Info.Signals(end).Dimensions  = 'nbPings, nbSounders';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile('Ssc_SeabedImage59h','TVGCrossOver.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'NbBeams';
Info.Signals(end).Dimensions  = 'nbPings, nbSounders';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_SeabedImage59h','NbBeams.bin');
Info.Signals(end).Tag         = verifKeyWord('SounderBeamNumber');

Info.Images(1).Name           = 'InfoBeamSortDirection';
Info.Images(end).Dimensions   = 'nbPings, nbBeams';
Info.Images(end).Index        = 'nbPings, nbBeams';
Info.Images(end).Storage      = 'single';
Info.Images(end).Unit         = 'm';
Info.Images(end).FileName     = fullfile('Ssc_SeabedImage59h','InfoBeamSortDirection.bin');
Info.Images(end).Tag          = verifKeyWord('TODO');

Info.Images(end+1).Name       = 'InfoBeamDetectionInfo';
Info.Images(end).Dimensions   = 'nbPings, nbBeams';
Info.Images(end).Index        = 'nbPings, nbBeams';
Info.Images(end).Storage      = 'single';
Info.Images(end).Unit         = 'm';
Info.Images(end).FileName     = fullfile('Ssc_SeabedImage59h','InfoBeamDetectionInfo.bin');
Info.Images(end).Tag          = verifKeyWord('TODO');

Info.Images(end+1).Name       = 'InfoBeamNbSamples';
Info.Images(end).Dimensions   = 'nbPings, nbBeams';
Info.Images(end).Index        = 'nbPings, nbBeams';
Info.Images(end).Storage      = 'single';
Info.Images(end).Unit         = 'm';
Info.Images(end).FileName     = fullfile('Ssc_SeabedImage59h','InfoBeamNbSamples.bin');
Info.Images(end).Tag          = verifKeyWord('TODO');

Info.Images(end+1).Name       = 'InfoBeamCentralSample';
Info.Images(end).Dimensions   = 'nbPings, nbBeams';
Info.Images(end).Index        = 'nbPings, nbBeams';
Info.Images(end).Storage      = 'single';
Info.Images(end).Unit         = 'm';
Info.Images(end).FileName     = fullfile('Ssc_SeabedImage59h','InfoBeamCentralSample.bin');
Info.Images(end).Tag          = verifKeyWord('TODO');

% Ajout des champs Scale_Factor, Add_Offset et Orig_Storage.
[~, Info] = addScaleOffsetStorage(Datagrams, Info);

%% Cr�ation du fichier XML d�crivant la donn�e 

nomFicXml = fullfile(nomDirRacine, 'Ssc_SeabedImage59h.xml');
xml_write(nomFicXml, Info);
flag = exist(nomFicXml, 'file');
if ~flag
    messageErreur(nomFicXml)
    return
end

%% Cr�ation du r�pertoire SeabedImage59h

nomDirAttitude = fullfile(nomDirRacine, 'Ssc_SeabedImage59h');
if ~exist(nomDirAttitude, 'dir')
    status = mkdir(nomDirAttitude);
    if ~status
        messageErreur(nomDirAttitude)
        flag = 0;
        return
    end
end

%% Cr�ation des fichiers binaires des signaux

for k=1:length(Info.Signals)
    flag = writeSignal(nomDirRacine, Info.Signals(k), Data.(Info.Signals(k).Name));
    if ~flag
        return
    end
end

%% Cr�ation des fichiers binaires des images

for k=1:length(Info.Images)
    flag = writeImage(nomDirRacine, Info.Images(k), Data.(Info.Images(k).Name));
    if ~flag
        return
    end
end

flag = 1;

