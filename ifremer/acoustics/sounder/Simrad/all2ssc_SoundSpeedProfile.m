function flag = all2ssc_SoundSpeedProfile(nomDirRacine, FormatVersion)

nomFicXml = fullfile(nomDirRacine, 'ALL_SoundSpeedProfile.xml');
SonarFmt2ssc_message(nomFicXml, '.all')

%% Lecture du fichier XML d�crivant la donn�e

flag = exist(nomFicXml, 'file');
if ~flag
    special_message('FichierNonExistant', nomFicXml)
    return
end
% edit(nomFicXml)
Datagrams = xml_read(nomFicXml);
nbProfiles = Datagrams.NbDatagrams;

% if nbProfiles > 1
%     nomDir = fileparts(nomFicXml);
%     str2 = sprintf('Please send this .all file to sonarscope@ifremer.fr becaus something that has never been found exists in this file (all2ssc_SoundSpeedProfile) : \n%s', nomDir);
%     my_warndlg(str2, 0);
% end

[nomDirSignal, nomFicSignal] = fileparts(nomFicXml);
nomDirSignal = fullfile(nomDirSignal, nomFicSignal);


%{
for i=1:length(Datagrams.Variables)
fprintf(1,'%d  %s\n', i, Datagrams.Variables(i).Name)
end
for i=1:length(Datagrams.Tables)
fprintf(1,'%d  %s\n', i, Datagrams.Tables(i).Name)
end
%}

[flag, Date] = Read_BinVariable(Datagrams, nomDirSignal, 'Date', 'double');
if ~flag
    return
end
% figure; plot(Date); grid on; title('Date');

[flag, NbMilliSecs] = Read_BinVariable(Datagrams, nomDirSignal, 'NbMilliSecs', 'double');
if ~flag
    return
end
% figure; plot(NbMilliSecs); grid on; title('NbMilliSecs');

[flag, Data.PingCounter] = Read_BinVariable(Datagrams, nomDirSignal, 'PingCounter', 'single');
if ~flag
    return
end
% figure; plot(Data.PingCounter); grid on; title('PingCounter');

[flag, DateProfile] = Read_BinVariable(Datagrams, nomDirSignal, 'DateProfile', 'double');
if ~flag
    return
end
% figure; plot(DateProfile); grid on; title('DateProfile');

[flag, NbMilliSecsProfile] = Read_BinVariable(Datagrams, nomDirSignal, 'NbMilliSecsProfile', 'double');
if ~flag
    return
end
% figure; plot(NbMilliSecsProfile); grid on; title('NbMilliSecsProfile');

[flag, Data.DepthResolution] = Read_BinVariable(Datagrams, nomDirSignal, 'DepthResolution', 'single');
if ~flag
    return
end
% figure; plot(DepthResolution); grid on; title('DepthResolution');

[flag, Data.NbEntries] = Read_BinVariable(Datagrams, nomDirSignal, 'NbEntries', 'single');
if ~flag
    return
end
% figure; plot(NbEntries); grid on; title('NbEntries');

nbPoints = max(Data.NbEntries);

BeamNumber = [];
% for i=1:Datagrams.NbDatagrams
%     BeamNumber = [BeamNumber; (1:NbEntries(i))']; %#ok<AGROW>
% end
Head1.SystemSerialNumberSecondHead = 0;


[flag, X] = Read_BinTable(Datagrams, nomDirSignal, 'Depth', 'single', sum(Data.NbEntries));
if ~flag
    return
end
% Data.Depth = Kongsberg_table2image(X, 'Depth', Datagrams.NbDatagrams, max(Data.NbEntries), Data.NbEntries, BeamNumber, Data.PingCounter, 0, Head1);
Data.Depth = Kongsberg_table2image(X, 'Depth', Datagrams.NbDatagrams, max(Data.NbEntries), Data.NbEntries, BeamNumber, 1:nbProfiles, 0, Head1);
% for i=1:length(DepthResolution)
for i=1:size(Data.Depth,1) % Bug trouv� dans C:\Temp\Delphine\20100104_CourbesSMF\EM2000\ALL\0010_20091118_083508_raw.all
    Data.Depth(i,:) = -Data.Depth(i,:) .* Data.DepthResolution(i);
end
% figure; plot(Data.Depth'); grid on; title('Depth'); axis xy; colorbar;



[flag, X] = Read_BinTable(Datagrams, nomDirSignal, 'SoundSpeed', 'single', sum(Data.NbEntries));
if ~flag
    return
end
% Data.SoundSpeed = Kongsberg_table2image(X, 'SoundSpeed', Datagrams.NbDatagrams, max(Data.NbEntries), Data.NbEntries, BeamNumber, Data.PingCounter, 0, Head1);
Data.SoundSpeed = Kongsberg_table2image(X, 'SoundSpeed', Datagrams.NbDatagrams, max(Data.NbEntries), Data.NbEntries, BeamNumber, 1:nbProfiles, 0, Head1);
% figure; plot(Data.SoundSpeed'); grid on; title('SoundSpeed'); axis xy; colorbar;
% figure; plot(Data.SoundSpeed', Data.Depth', '-*'); grid on; title('SoundSpeed'); axis xy; colorbar;


%% Calcul des dates et heures

Jour  = mod(Date,100);
Date  = (Date - Jour) / 100;
Mois  = mod(Date,100);
Annee = (Date - Mois) / 100;
Date = dayJma2Ifr(Jour, Mois, Annee);
Data.TimeStartOfUse = cl_time('timeIfr', Date, NbMilliSecs);

Jour  = mod(DateProfile,100);
DateProfile  = (DateProfile - Jour) / 100;
Mois  = mod(DateProfile,100);
Annee = (DateProfile - Mois) / 100;
DateProfile = dayJma2Ifr(Jour, Mois, Annee);
Data.TimeProfileMeasurement = cl_time('timeIfr', DateProfile, NbMilliSecsProfile);

%% G�n�ration du XML SonarScope

Info.Title                  = 'SoundSpeedProfile';
Info.Constructor            = 'Kongsberg';
Info.EmModel                = Datagrams.Model;
Info.SystemSerialNumber     = Datagrams.SystemSerialNumber;
Info.FormatVersion          = FormatVersion;

% % GLU : pb rencontr� avec fichier C:\Temp\GeoscienceAustralia\Burley_Griffin\SonarScope\0081_20080229_024643_ShipName
% Info.NbEntries               = max(nbProfiles);

Info.Dimensions.nbProfiles   = nbProfiles;
Info.Dimensions.nbPoints     = nbPoints;

Info.TimeOrigin             = '01/01/-4713';
Info.Comments               = '';

Info.Signals(1).Name          = 'TimeStartOfUse';
Info.Signals(end).Dimensions  = 'nbProfiles, 1';
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = 'days since JC';
Info.Signals(end).FileName    = fullfile('Ssc_SoundSpeedProfile','TimeStartOfUse.bin');
Info.Signals(end).Tag         = verifKeyWord('SoundProfileTimeStartOfUse');

Info.Signals(end+1).Name      = 'PingCounter';
Info.Signals(end).Dimensions  = 'nbProfiles, 1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_SoundSpeedProfile','PingCounter.bin');
Info.Signals(end).Tag         = verifKeyWord('SounderPingCounter');

Info.Signals(end+1).Name        = 'TimeProfileMeasurement';
Info.Signals(end).Dimensions    = 'nbProfiles, 1';
Info.Signals(end).Storage       = 'double';
Info.Signals(end).Unit          = 'days since JC';
Info.Signals(end).FileName      = fullfile('Ssc_SoundSpeedProfile','TimeProfileMeasurement.bin');
Info.Signals(end).Tag           = verifKeyWord('SoundProfileTime');

Info.Signals(end+1).Name      = 'NbEntries';
Info.Signals(end).Dimensions  = 'nbProfiles, 1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_SoundSpeedProfile','NbEntries.bin');
Info.Signals(end).Tag         = verifKeyWord('NbCycles');

Info.Signals(end+1).Name      = 'DepthResolution';
Info.Signals(end).Dimensions  = 'nbProfiles, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_SoundSpeedProfile','DepthResolutions.bin');
Info.Signals(end).Tag         = verifKeyWord('NbCycles');

Info.Images(1).Name          = 'Depth';
Info.Images(end).Dimensions  = 'nbProfiles, nbPoints';
Info.Images(end).Index       = 'nbProfiles, nbPoints';
Info.Images(end).Storage     = 'single';
Info.Images(end).Unit        = 'm';
Info.Images(end).FileName    = fullfile('Ssc_SoundSpeedProfile','Depth.bin');
Info.Images(end).Tag         = verifKeyWord('SoundProfileDepth');

Info.Images(end+1).Name      = 'SoundSpeed';
Info.Images(end).Dimensions  = 'nbProfiles, nbPoints';
Info.Images(end).Index  = 'nbProfiles, nbPoints';
Info.Images(end).Storage     = 'single';
Info.Images(end).Unit        = 'm/s';
Info.Images(end).FileName    = fullfile('Ssc_SoundSpeedProfile','SoundSpeed.bin');
Info.Images(end).Tag         = verifKeyWord('SoundProfileSoundSpeed');

% Ajout des champs Scale_Factor, Add_Offset et Orig_Storage.
[flag, Info] = addScaleOffsetStorage(Datagrams, Info);

%% Cr�ation du fichier XML d�crivant la donn�e

nomFicXml = fullfile(nomDirRacine, 'Ssc_SoundSpeedProfile.xml');
xml_write(nomFicXml, Info);
flag = exist(nomFicXml, 'file');
if ~flag
    messageErreur(nomFicXml)
    return
end

%% Cr�ation du r�pertoire SoundSpeedProfile

nomDirSoundSpeedProfile = fullfile(nomDirRacine, 'Ssc_SoundSpeedProfile');
if ~exist(nomDirSoundSpeedProfile, 'dir')
    status = mkdir(nomDirSoundSpeedProfile);
    if ~status
        messageErreur(nomDirSoundSpeedProfile)
        flag = 0;
        return
    end
end

%% Cr�ation des fichiers binaires des signaux

for i=1:length(Info.Signals)
    flag = writeSignal(nomDirRacine, Info.Signals(i), Data.(Info.Signals(i).Name));
    if ~flag
        return
    end
end

%% Cr�ation des fichiers binaires des images

for i=1:length(Info.Images)
    flag = writeImage(nomDirRacine, Info.Images(i), Data.(Info.Images(i).Name));
    if ~flag
        return
    end
end

flag = 1;
