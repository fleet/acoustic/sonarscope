function flag = all2ssc_StaveData(nomDirRacine, FormatVersion, Head1)

global DEBUG %#ok<GVMIS> 

nomFicXml = fullfile(nomDirRacine, 'ALL_StaveData.xml');
SonarFmt2ssc_message(nomFicXml, '.all')

%% Lecture du fichier XML d�crivant la donn�e 

flag = exist(nomFicXml, 'file');
if ~flag
    special_message('FichierNonExistant', nomFicXml)
    return
end
% edit(nomFicXml)
Datagrams = xml_read(nomFicXml);

[nomDirSignal, nomFicSignal] = fileparts(nomFicXml);
nomDirSignal = fullfile(nomDirSignal, nomFicSignal);

%{
for k=1:length(Datagrams.Variables)
    fprintf(1,'%d  %s\n', k, Datagrams.Variables(k).Name)
end
for k=1:length(Datagrams.Tables)
    fprintf(1,'%d  %s\n', k, Datagrams.Tables(k).Name)
end
%}

[flag, NumberOfDatagrams] = Read_BinVariable(Datagrams, nomDirSignal, 'NumberOfDatagrams', 'single');
if ~flag
    return
end
% FigUtils.createSScFigure; PlotUtils.createSScPlot(NumberOfDatagrams); grid on; title('NumberOfDatagrams');

[flag, DatagramNumbers] = Read_BinVariable(Datagrams, nomDirSignal, 'DatagramNumbers', 'single');
if ~flag
    return
end
% FigUtils.createSScFigure; PlotUtils.createSScPlot(DatagramNumbers, '*'); grid on; title('DatagramNumbers');

[flag, Date] = Read_BinVariable(Datagrams, nomDirSignal, 'Date', 'double');
if ~flag
    return
end
% FigUtils.createSScFigure; PlotUtils.createSScPlot(Date); grid on; title('Date');

[flag, NbMilliSecs] = Read_BinVariable(Datagrams, nomDirSignal, 'NbMilliSecs', 'double');
if ~flag
    return
end
% FigUtils.createSScFigure; PlotUtils.createSScPlot(NbMilliSecs); grid on; title('NbMilliSecs');

[flag, PingCounter] = Read_BinVariable(Datagrams, nomDirSignal, 'PingCounter', 'single');
if ~flag
    return
end
PingCounter = unwrapPingCounterSimrad(PingCounter);
% FigUtils.createSScFigure; PlotUtils.createSScPlot(PingCounter); grid on; title('PingCounter');

[flag, SystemSerialNumber] = Read_BinVariable(Datagrams, nomDirSignal, 'SystemSerialNumber', 'single');
if ~flag
    return
end
% FigUtils.createSScFigure; PlotUtils.createSScPlot(SystemSerialNumber, '+'); grid on; title('SystemSerialNumber');

% GLU TODO : SamplingFreq=NaN
[flag, RxSamplingFreq] = Read_BinVariable(Datagrams, nomDirSignal, 'RxSamplingFreq', 'single');
if ~flag
    return
end
% FigUtils.createSScFigure; PlotUtils.createSScPlot(RxSamplingFreq, '+'); grid on; title('RxSamplingFreq');

[flag, SoundSpeed] = Read_BinVariable(Datagrams, nomDirSignal, 'SoundSpeed', 'single');
if ~flag
    return
end
% FigUtils.createSScFigure; PlotUtils.createSScPlot(SoundSpeed); grid on; title('SoundSpeed');

[flag, StartSampleRef1TxPulse] = Read_BinVariable(Datagrams, nomDirSignal, 'StartSampleRef1TxPulse', 'single');
if ~flag
    return
end
% FigUtils.createSScFigure; PlotUtils.createSScPlot(StartSampleRef1TxPulse); grid on; title('StartSampleRef1TxPulse');

[flag, TotalSamplesPerStave] = Read_BinVariable(Datagrams, nomDirSignal, 'TotalSamplesPerStave', 'single');
if ~flag
    return
end
% FigUtils.createSScFigure; PlotUtils.createSScPlot(TotalSamplesPerStave); grid on; title('TotalSamplesPerStave');

[flag, NbSamples] = Read_BinVariable(Datagrams, nomDirSignal, 'NbSamples', 'single');
if ~flag
    return
end
% FigUtils.createSScFigure; PlotUtils.createSScPlot(NbSamples); grid on; title('NbSamples');

[flag, StaveNumber] = Read_BinVariable(Datagrams, nomDirSignal, 'StaveNumber', 'single');
if ~flag
    return
end
% FigUtils.createSScFigure; PlotUtils.createSScPlot(StaveNumber); grid on; title('StaveNumber');

[flag, NbStavesPerHead] = Read_BinVariable(Datagrams, nomDirSignal, 'NbStavesPerHead', 'single');
if ~flag
    return
end
% FigUtils.createSScFigure; PlotUtils.createSScPlot(NbStavesPerHead); grid on; title('NbStavesPerHead');

NbSamples = double(NbSamples);
Ns = sum(NbSamples);
NbStavesPerHead = double(NbStavesPerHead);
N = sum(NbSamples .* NbStavesPerHead);

[flag, RangeToNormalIncidence] = Read_BinVariable(Datagrams, nomDirSignal, 'RangeToNormalIncidence', 'single');
if ~flag
    return
end
% FigUtils.createSScFigure; PlotUtils.createSScPlot(RangeToNormalIncidence); grid on; title('RangeToNormalIncidence');

% GLU : assemblage NRx par fusion des datagrammes du m�me Num�ro de Ping par Antenne.
if Head1.SystemSerialNumberSecondHead ~= 0
    NbSounders = 2;
else
    NbSounders = 1;
end

% Calcul du nombre d'�chantillon par Ping (recomposition des Ping selon les
% datagrammes associ�s.
Data.NbSamples   = [];
Data.PingCounter = [];
[b, m, n] = unique(PingCounter);
nbPings   = size(m, 1);
for ns=1:NbSounders
    if ns == 1
        NumSerie = Head1.SystemSerialNumber;
    else
        NumSerie = Head1.SystemSerialNumberSecondHead;
    end
    subNumSounder = (SystemSerialNumber == NumSerie);
    % On ne retient que les Pings complets.
    for iPing=1:nbPings
        PC = PingCounter(subNumSounder);
        ND = NumberOfDatagrams(subNumSounder);
        NS = NbSamples(subNumSounder);
        [u, v] = find(PC == b(iPing));
        % GLU : le 04/02/2015, patch pour �viter le plantage si les Stave Data ne sont pas enregistr�s pour une deux t�tes de sondeurs.
        % Cas du fichier THALIA (EM20140) : 0057_20130203_130622_Thalia.all et 0059_20130203_134006_Thalia
        if ~isempty(u)
            if sum(v) == ND(u(1))
                Data.NbSamples(iPing, ns)   = sum(NS(u));
                Data.PingCounter(iPing, ns) = b(iPing);
            end
        end
    end
end
% FigUtils.createSScFigure; PlotUtils.createSScPlot(Data.NbSamples); grid on; title('Data.NbSamples');

% sub = Ping complets, ia : indices des Pings complets dans PingCounter
tmpPingCounter = PingCounter;
[~, sub, ~]            = intersect(PingCounter, Data.PingCounter);
Date                   = Date(sub);
NbMilliSecs            = NbMilliSecs(sub);
PingCounter            = PingCounter(sub);
RxSamplingFreq         = RxSamplingFreq(sub);
SoundSpeed             = SoundSpeed(sub);
% SystemSerialNumber     = SystemSerialNumber(sub); % Comment� par JMA le 07/06/2017 !!!!!!!!!!!!!!!!!!!!!!!!!
TotalSamplesPerStave   = TotalSamplesPerStave(sub);
StaveNumber            = StaveNumber(sub);
RangeToNormalIncidence = RangeToNormalIncidence(sub);

% Correct PingCounter in case of dual sounder but only the starboard is on
% (correspondances between pings are calculated on indice=1 on second
% dimension

nbDatagrams = length(Date);
Date                        = Kongsberg_table2signal(Date,                      nbDatagrams, PingCounter, SystemSerialNumber, Head1);
NbMilliSecs                 = Kongsberg_table2signal(NbMilliSecs,               nbDatagrams, PingCounter, SystemSerialNumber, Head1);
Data.PingCounter            = Kongsberg_table2signal(PingCounter,               nbDatagrams, PingCounter, SystemSerialNumber, Head1);
Data.SystemSerialNumber     = Kongsberg_table2signal(SystemSerialNumber,        nbDatagrams, PingCounter, SystemSerialNumber, Head1);
Data.RxSamplingFreq         = Kongsberg_table2signal(RxSamplingFreq,            nbDatagrams, PingCounter, SystemSerialNumber, Head1);
Data.SoundSpeed             = Kongsberg_table2signal(SoundSpeed,                nbDatagrams, PingCounter, SystemSerialNumber, Head1);
Data.TotalSamplesPerStave   = Kongsberg_table2signal(TotalSamplesPerStave,      nbDatagrams, PingCounter, SystemSerialNumber, Head1);
Data.StaveNumber            = Kongsberg_table2signal(StaveNumber,               nbDatagrams, PingCounter, SystemSerialNumber, Head1);
Data.RangeToNormalIncidence = Kongsberg_table2signal(RangeToNormalIncidence,    nbDatagrams, PingCounter, SystemSerialNumber, Head1);

% nbDatagrams = length(DatagramNumbers); % Comment� par JMA le 07/06/2017 !!!!!!!!!!!!!!!!!!!!!!!!!
Data.NumberOfDatagrams      = Kongsberg_table2signal(NumberOfDatagrams,         nbDatagrams, PingCounter, SystemSerialNumber, Head1);
Data.DatagramNumbers        = Kongsberg_table2signal(DatagramNumbers,           nbDatagrams, PingCounter, SystemSerialNumber, Head1);
Data.StartSampleRef1TxPulse = Kongsberg_table2signal(StartSampleRef1TxPulse,    nbDatagrams, PingCounter, SystemSerialNumber, Head1);
Data.NbSamples              = Kongsberg_table2signal(NbSamples,                 nbDatagrams, PingCounter, SystemSerialNumber, Head1);
Data.NbStavesPerHead      = Kongsberg_table2signal(NbStavesPerHead,         nbDatagrams, PingCounter, SystemSerialNumber, Head1);

if NbSounders == 1
    subPingAbsent = isnan(Data.PingCounter);
    
    Data.NbSamples(subPingAbsent,:) = [];
    Data.PingCounter(subPingAbsent,:) = [];
    Data.SystemSerialNumber(subPingAbsent,:) = [];
    Data.RxSamplingFreq(subPingAbsent,:) = [];
    Data.SoundSpeed(subPingAbsent,:) = [];
    Data.TotalSamplesPerStave(subPingAbsent,:) = [];
    Data.StaveNumber(subPingAbsent,:) = [];
    Data.RangeToNormalIncidence(subPingAbsent,:) = [];
    Data.NumberOfDatagrams(subPingAbsent,:) = [];
    Data.DatagramNumbers(subPingAbsent,:) = [];
    Data.StartSampleRef1TxPulse(subPingAbsent,:) = [];
    Data.NbStavesPerHead(subPingAbsent,:) = [];
    
    Date(subPingAbsent,:) = [];
    NbMilliSecs(subPingAbsent,:) = [];
    
else
    subPingAbsent = (sum(isnan(Data.PingCounter), 2) == size(Data.PingCounter,2));
    
    Data.NbSamples(subPingAbsent,:) = [];
    Data.PingCounter(subPingAbsent,:) = [];
    Data.SystemSerialNumber(subPingAbsent,:) = [];
    Data.RxSamplingFreq(subPingAbsent,:) = [];
    Data.SoundSpeed(subPingAbsent,:) = [];
    Data.TotalSamplesPerStave(subPingAbsent,:) = [];
    Data.StaveNumber(subPingAbsent,:) = [];
    Data.RangeToNormalIncidence(subPingAbsent,:) = [];
    Data.NumberOfDatagrams(subPingAbsent,:) = [];
    Data.DatagramNumbers(subPingAbsent,:) = [];
    Data.StartSampleRef1TxPulse(subPingAbsent,:) = [];
    Data.NbStavesPerHead(subPingAbsent,:) = [];
    
    Date(subPingAbsent,:) = [];
    NbMilliSecs(subPingAbsent,:) = [];
    
    sub1 = isnan(Date(:,1));
    Date(sub1,1)        = Date(sub1,2);
    NbMilliSecs(sub1,1) = NbMilliSecs(sub1,2);
    
    sub1 = isnan(Date(:,2));
    Date(sub1,2)        = Date(sub1,1);
    NbMilliSecs(sub1,2) = NbMilliSecs(sub1,1);
end

% MaxDesMaxNbSamples  = max(Data.TotalSamplesPerStave);
MaxDesMaxNbSamples  = max(Data.TotalSamplesPerStave(:)); % Modif JMA le 07/06/2017
Offset              = 1;
IndexNumberSample = NaN(sum(NbSamples), 1);
for k=1:numel(NbSamples)
    IndexNumberSample(Offset:Offset+(NbSamples(k)-1)) = 1:NbSamples(k);
    Offset = Offset + NbSamples(k);
end

% % % % % % [flag, X] = Read_BinTable(Datagrams, nomDirSignal, 'SampleNumber', 'single', Ns);
% % % % % % if ~flag
% % % % % %     return
% % % % % % end
% % % % % % Data.SampleNumber = Kongsberg_table2image(X, 'SampleNumber', length(Data.PingCounter), MaxDesMaxNbSamples, NbSamples, IndexNumberSample, PingCounter, SystemSerialNumber, Head1);
% % % % % % Data.SampleNumber = X + 1;
% % % % % % % FigUtils.createSScFigure; PlotUtils.createSScPlot(Data.SampleNumber, '*'); title('SampleNumber'); grid on;

[flag, TVGGain] = Read_BinTable(Datagrams, nomDirSignal, 'TVGGain', 'single', Ns);
if ~flag
    return
end
Data.TVGGain = NaN(numel(PingCounter), MaxDesMaxNbSamples);
Offset = 1;
for k=1:numel(PingCounter)
    if NbSounders == 1
        TSPS = Data.TotalSamplesPerStave(k);
    else
        TSPS = max(Data.TotalSamplesPerStave(k,:));
    end
    Data.TVGGain(k,1:TSPS) = TVGGain(Offset:Offset+(TSPS-1));
    Offset = Offset + TSPS;
end
% SonarScope(Data.TVGGain)

%% Nouvelle forme bas�e sur l'analyse des TotalSamplesPerStave

% Mise en forme des donn�es de Stave Backscatter.

if NbSounders == 1
    maxNe = max(Data.NbStavesPerHead(:));
else
    maxNe = max(Data.NbStavesPerHead(:)) * 2;
end
maxNs   = max(Data.TotalSamplesPerStave(:));
nbPings = size(Data.PingCounter,1);

Data.StaveBackscatter = NaN(maxNs, maxNe, nbPings);
[flag, X] = Read_BinTable(Datagrams, nomDirSignal, 'StaveBackscatter', 'single', N);
if ~flag
    return
end

% On se base sur tmpPingCounter qui comprend tous les n� de Ping y compris
% ceux non retenu
idx0 = 1;
for p=1:size(tmpPingCounter,1)
    idxN = idx0 + uint64((NbSamples(p) * NbStavesPerHead(p) - 1));
    if any(any(Data.PingCounter == tmpPingCounter(p)))
        subX = X(idx0:idxN);
        % Pour la prochaine it�ration.
        subI = reshape(subX, NbStavesPerHead(p), NbSamples(p));
        if (p > 1) && (tmpPingCounter(p) == tmpPingCounter(p-1))
            I = [subPrevI subI];
        else
            I = subI;
        end
        subPrevI = I;
        if DatagramNumbers(p) == NumberOfDatagrams(p)
            subStaves = 1:NbStavesPerHead(p);
            if NbSounders == 1
                Data.StaveBackscatter(1:size(I,2), subStaves, n(p)) = I';
            else
                if subNumSounder(p) == 1
                    Data.StaveBackscatter(1:size(I,2), subStaves, n(p)) = I';
                else
                    Data.StaveBackscatter(1:size(I,2), subStaves + (NbStavesPerHead(p) - 1), n(p)) = I';
                end
            end
            subPrevI = [];
        end
        % Il peut y avoir 1-N datagrammes par Ping.
        if DEBUG
            if (DatagramNumbers(p) == NumberOfDatagrams(p)) && mod(p,10)
                figure(123456); imagesc(Data.StaveBackscatter(:,:,n(p)), [-130 -30]), title(['Ping = ' num2str(tmpPingCounter(p)) ]);colorbar;
                pause(0.1);
            end
        end
    end
    idx0 = idxN + 1;
end

%% Calcul des dates et heures

Jour  = mod(Date,100);
Date  = (Date - Jour) / 100;
Mois  = mod(Date,100);
Annee = (Date - Mois) / 100;
Date  = dayJma2Ifr(Jour, Mois, Annee);
Data.Time = cl_time('timeIfr', Date, NbMilliSecs);

%% G�n�ration du XML SonarScope

Info.Title                   = 'StaveData';
Info.Constructor             = 'Kongsberg';
Info.EmModel                 = Datagrams.Model;
Info.ListeSystemSerialNumber = num2str(unique(Data.SystemSerialNumber(~isnan(Data.SystemSerialNumber)))');
Info.TimeOrigin              = '01/01/-4713';
Info.Comments                = 'Stave data format';
Info.FormatVersion           = FormatVersion;

Info.Dimensions.nbPings     = nbPings;
Info.Dimensions.nbSamples   = maxNs;
Info.Dimensions.nbSounders  = size(Data.PingCounter,2);
Info.Dimensions.nbStaves    = maxNe;
Info.Dimensions.nbDatagrams = nbDatagrams;

Info.Signals(1).Name          = 'Time';
Info.Signals(end).Dimensions  = 'nbPings, nbSounders';
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = 'days since JC';
Info.Signals(end).FileName    = fullfile('Ssc_StaveData', 'Time.bin');
Info.Signals(end).Tag         = verifKeyWord('SounderTime');

Info.Signals(end+1).Name      = 'PingCounter';
Info.Signals(end).Dimensions  = 'nbPings, nbSounders';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_StaveData', 'PingCounter.bin');
Info.Signals(end).Tag         = verifKeyWord('SounderPingCounter');

Info.Signals(end+1).Name      = 'SystemSerialNumber';
Info.Signals(end).Dimensions  = 'nbPings, nbSounders';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_StaveData', 'SystemSerialNumber.bin');
Info.Signals(end).Tag         = verifKeyWord('SounderSerialNumber');

Info.Signals(end+1).Name      = 'NumberOfDatagrams';
Info.Signals(end).Dimensions  = 'nbDatagrams, nbSounders';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_StaveData', 'NumberOfDatagrams.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');
 
Info.Signals(end+1).Name      = 'DatagramNumbers';
Info.Signals(end).Dimensions  = 'nbDatagrams, nbSounders';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_StaveData', 'DatagramNumbers.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');
 
Info.Signals(end+1).Name      = 'RxSamplingFreq';
Info.Signals(end).Dimensions  = 'nbPings, nbSounders';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'Hz';
Info.Signals(end).FileName    = fullfile('Ssc_StaveData', 'RxSamplingFreq.bin');
Info.Signals(end).Tag         = verifKeyWord('SampleFrequency');

Info.Signals(end+1).Name      = 'SoundSpeed';
Info.Signals(end).Dimensions  = 'nbPings, nbSounders';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'm/s';
Info.Signals(end).FileName    = fullfile('Ssc_StaveData', 'SoundSpeed.bin');
Info.Signals(end).Tag         = verifKeyWord('SounderSoundSpeed');

Info.Signals(end+1).Name      = 'StartSampleRef1TxPulse';
Info.Signals(end).Dimensions  = 'nbDatagrams, nbSounders';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'm';
Info.Signals(end).FileName    = fullfile('Ssc_StaveData', 'StartSampleRef1TxPulse.bin');
Info.Signals(end).Tag         = verifKeyWord('NRx');

Info.Signals(end+1).Name      = 'TotalSamplesPerStave';
Info.Signals(end).Dimensions  = 'nbPings, nbSounders';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_StaveData', 'TotalSamplesPerStave.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');     

Info.Signals(end+1).Name      = 'NbSamples';
Info.Signals(end).Dimensions  = 'nbDatagrams, nbSounders';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_StaveData', 'NbSamples.bin');
Info.Signals(end).Tag         = verifKeyWord('SounderSamplingRate');

Info.Signals(end+1).Name      = 'StaveNumber';
Info.Signals(end).Dimensions  = 'nbPings, nbSounders';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_StaveData', 'StaveNumber.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'NbStavesPerHead';
Info.Signals(end).Dimensions  = 'nbDatagrams, nbSounders';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_StaveData', 'NbStavesPerHead.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'RangeToNormalIncidence';
Info.Signals(end).Dimensions  = 'nbPings, nbSounders';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_StaveData', 'RangeToNormalIncidence.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

% + Spare 1 

% Partie Tx

% + Spare ALignment
% % % % 
% % % % Info.Images(1).Name          = 'SampleNumber';
% % % % Info.Images(end).Dimensions  = 'nbPings, nbSamples, nbSounders';
% % % % Info.Images(end).Index       = 'nbPings, nbSamples, nbSounders';
% % % % Info.Images(end).Storage     = 'single';
% % % % Info.Images(end).Unit        = 's';
% % % % Info.Images(end).FileName    = fullfile('Ssc_StaveData', 'SampleNumber.bin');
% % % % Info.Images(end).Tag         = verifKeyWord('TODO');

Info.Images(1).Name          = 'TVGGain';
% Info.Images(end).Dimensions  = 'nbPings, nbSamples, nbSounders';
% Info.Images(end).Index       = 'nbPings, nbSamples, nbSounders';
Info.Images(end).Dimensions  = 'nbPings, nbSamples';
Info.Images(end).Index       = 'nbPings, nbSamples';
Info.Images(end).Storage     = 'single';
Info.Images(end).Unit        = 'dB';
Info.Images(end).FileName    = fullfile('Ssc_StaveData', 'TVGGain.bin');
Info.Images(end).Tag         = verifKeyWord('TODO');

Info.Images(end+1).Name      = 'StaveBackscatter';
Info.Images(end).Dimensions  = 'nbSamples, nbStaves, nbPings';
Info.Images(end).Index       = 'nbSamples, nbStaves, nbPings';
Info.Images(end).Storage     = 'single';
Info.Images(end).Unit        = 'dB';
Info.Images(end).FileName    = fullfile('Ssc_StaveData', 'StaveBackscatter.bin');
Info.Images(end).Tag         = verifKeyWord('TODO');

% Ajout des champs Scale_Factor, Add_Offset et Orig_Storage.
[flag, Info] = addScaleOffsetStorage(Datagrams, Info); %#ok<ASGLU>

%% Cr�ation du fichier XML d�crivant la donn�e 

nomFicXml = fullfile(nomDirRacine, 'Ssc_StaveData.xml');
xml_write(nomFicXml, Info);
flag = exist(nomFicXml, 'file');
if ~flag
    messageErreur(nomFicXml)
    return
end
% edit(nomFicXml)

%% Cr�ation du r�pertoire StaveData

nomDirStaveData = fullfile(nomDirRacine, 'Ssc_StaveData');
if ~exist(nomDirStaveData, 'dir')
    status = mkdir(nomDirStaveData);
    if ~status
        messageErreur(nomDirStaveData)
        flag = 0;
        return
    end
end

%% Cr�ation des fichiers binaires des signaux

for k=1:length(Info.Signals)
    flag = writeSignal(nomDirRacine, Info.Signals(k), Data.(Info.Signals(k).Name));
    if ~flag
        return
    end
end

%% Cr�ation des fichiers binaires des images

for k=1:length(Info.Images)
    flag = writeImage(nomDirRacine, Info.Images(k), Data.(Info.Images(k).Name));
    if ~flag
        return
    end
end

flag = 1;
