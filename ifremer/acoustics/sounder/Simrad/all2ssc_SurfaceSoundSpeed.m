function flag = all2ssc_SurfaceSoundSpeed(nomDirRacine, FormatVersion)

nomFicXml = fullfile(nomDirRacine, 'ALL_SurfaceSoundSpeed.xml');
SonarFmt2ssc_message(nomFicXml, '.all')

%% Lecture du fichier XML d�crivant la donn�e 

flag = exist(nomFicXml, 'file');
if ~flag
%     special_message('FichierNonExistant', nomFicXml, 'NoWindow')
    return
end
% edit(nomFicXml)
Datagrams = xml_read(nomFicXml);


%{
for i=1:length(Datagrams.Variables)
    fprintf(1,'%d  %s\n', i, Datagrams.Variables(i).Name)
end
for i=1:length(Datagrams.Tables)
    fprintf(1,'%d  %s\n', i, Datagrams.Tables(i).Name)
end
%}


[nomDirSignal, nomFicSignal] = fileparts(nomFicXml);
nomDirSignal = fullfile(nomDirSignal, nomFicSignal);

[flag, Data.NbEntries] = Read_BinVariable(Datagrams, nomDirSignal, 'NbEntries', 'double');
if ~flag
    return
end
nbSamples = sum(Data.NbEntries);
% figure; plot(Data.NbEntries); grid on; title('Data.NbEntries');

[flag, Date] = Read_BinVariable(Datagrams, nomDirSignal, 'Date', 'double');
if ~flag
    return
end
% figure; plot(Date); grid on; title('Date');

[flag, NbMilliSecs] = Read_BinVariable(Datagrams, nomDirSignal, 'NbMilliSecs', 'double');
if ~flag
    return
end
% figure; plot(NbMilliSecs); grid on; title('NbMilliSecs');

%% Lecture de PingCounter

[flag, PingCounter] = Read_BinVariable(Datagrams, nomDirSignal, 'PingCounter', 'single');
if ~flag
    return
end
Data.PingCounter = unwrapPingCounterSimrad(PingCounter);
% figure; plot(PingCounter); grid on; title('PingCounter')

%% Lecture de SurfaceSoundSpeed

[flag, Data.SurfaceSoundSpeed] = Read_BinTable(Datagrams, nomDirSignal, 'SurfaceSoundSpeed', 'single', nbSamples);
if ~flag
    return
end
% figure; plot(Data.SurfaceSoundSpeed); grid on; title('SurfaceSoundSpeed')

%% Lecture du Pitch

[flag, TS] = Read_BinTable(Datagrams, nomDirSignal, 'StartNbMilliSecs', 'single', nbSamples);
if ~flag
    return
end
% figure; plot(TS); grid on; title('TS')

%% Calcul des dates et heures

Jour  = mod(Date,100);
Date  = (Date - Jour) / 100;
Mois  = mod(Date,100);
Annee = (Date - Mois) / 100;
Date = dayJma2Ifr(Jour, Mois, Annee);

D = zeros(1,nbSamples);
H = zeros(1,nbSamples);
pos = 0;
for i=1:Datagrams.NbDatagrams
    sub = pos+1:pos+Data.NbEntries(i);
    D(sub) = Date(i);
    H(sub) = NbMilliSecs(i);
    pos = pos + Data.NbEntries(i);
end
Time = cl_time('timeIfr', D, H);
Data.Time = Time + double(TS');

%% G�n�ration du XML SonarScope

Info.Title                  = 'SurfaceSoundSpeed';
Info.Constructor            = 'Kongsberg';
Info.EmModel                = Datagrams.Model;
Info.SystemSerialNumber     = Datagrams.SystemSerialNumber;
Info.TimeOrigin             = '01/01/-4713';
Info.Comments               = 'Sensor sampling rate';
Info.FormatVersion          = FormatVersion;

Info.Dimensions.NbSamples     = nbSamples;
Info.Dimensions.NbDatagrams   = Datagrams.NbDatagrams;

Info.Signals(1).Name          = 'PingCounter';
Info.Signals(end).Dimensions  = 'NbDatagrams, 1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_SurfaceSoundSpeed','PingCounter.bin');
Info.Signals(end).Tag         = verifKeyWord('SounderPingCounter');

Info.Signals(end+1).Name      = 'NbEntries';
Info.Signals(end).Dimensions  = 'NbDatagrams, 1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_SurfaceSoundSpeed','NbEntries.bin');
Info.Signals(end).Tag         = verifKeyWord('NbCycles');

Info.Signals(end+1).Name        = 'Time';
Info.Signals(end).Dimensions    = 'NbSamples, 1';
Info.Signals(end).Index         = 'NbDatagrams, NbEntries';
Info.Signals(end).Storage       = 'double';
Info.Signals(end).Unit          = 'days since JC';
Info.Signals(end).FileName      = fullfile('Ssc_SurfaceSoundSpeed','Time.bin');
Info.Signals(end).Tag           = verifKeyWord('SounderTime');

Info.Signals(end+1).Name        = 'SurfaceSoundSpeed';
Info.Signals(end).Dimensions    = 'NbSamples, 1';
Info.Signals(end).Index         = 'NbDatagrams, NbEntries';
Info.Signals(end).Storage       = 'single';
Info.Signals(end).Unit          = 'm/s';
Info.Signals(end).FileName      = fullfile('Ssc_SurfaceSoundSpeed','SurfaceSoundSpeed.bin');
Info.Signals(end).Tag           = verifKeyWord('SounderSoundSpeed');

% Ajout des champs Scale_Factor, Add_Offset et Orig_Storage.
[flag, Info] = addScaleOffsetStorage(Datagrams, Info); %#ok<ASGLU>

%% Cr�ation du fichier XML d�crivant la donn�e 

nomFicXml = fullfile(nomDirRacine, 'Ssc_SurfaceSoundSpeed.xml');
xml_write(nomFicXml, Info);
flag = exist(nomFicXml, 'file');
if ~flag
    messageErreur(nomFicXml)
    return
end

%% Cr�ation du r�pertoire SurfaceSoundSpeed

nomDirSurfaceSoundSpeed = fullfile(nomDirRacine, 'Ssc_SurfaceSoundSpeed');
if ~exist(nomDirSurfaceSoundSpeed, 'dir')
    status = mkdir(nomDirSurfaceSoundSpeed);
    if ~status
        messageErreur(nomDirSurfaceSoundSpeed)
        flag = 0;
        return
    end
end

%% Cr�ation des fichiers binaires des signaux

for i=1:length(Info.Signals)
    flag = writeSignal(nomDirRacine, Info.Signals(i), Data.(Info.Signals(i).Name));
    if ~flag
        return
    end
end

flag = 1;
