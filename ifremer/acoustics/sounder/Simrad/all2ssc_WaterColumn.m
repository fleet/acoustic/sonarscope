function flag = all2ssc_WaterColumn(nomDirRacine, FormatVersion, Head1)

nomFicXml = fullfile(nomDirRacine, 'ALL_WaterColumn.xml');
SonarFmt2ssc_message(nomFicXml, '.all')

%% Lecture du fichier XML d�crivant la donn�e 

flag = exist(nomFicXml, 'file');
if ~flag
    nomFicXml = fullfile(nomDirRacine, 'WCD_WaterColumn.xml');
    flag = exist(nomFicXml, 'file');
end
if ~flag
    special_message('FichierNonExistant', nomFicXml)
    return
end
% edit(nomFicXml)
Datagrams = xml_read(nomFicXml);

[nomDirSignal, nomFicSignal] = fileparts(nomFicXml);
nomDirSignal = fullfile(nomDirSignal, nomFicSignal);

%{
for k=1:length(Datagrams.Variables)
    fprintf(1,'%d  %s\n', k, Datagrams.Variables(k).Name)
end
for k=1:length(Datagrams.Tables)
    fprintf(1,'%d  %s\n', k, Datagrams.Tables(k).Name)
end
%}

[flag, NumberOfDatagrams] = Read_BinVariable(Datagrams, nomDirSignal, 'NumberOfDatagrams', 'single');
if ~flag
    return
end
% figure; PlotUtils.createSScPlot(NumberOfDatagrams); grid on; title('NumberOfDatagrams');

[flag, DatagramNumbers] = Read_BinVariable(Datagrams, nomDirSignal, 'DatagramNumbers', 'single');
if ~flag
    return
end
% figure; PlotUtils.createSScPlot(Data.DatagramNumbers); grid on; title('Data.DatagramNumbers');

[flag, Date1] = Read_BinVariable(Datagrams, nomDirSignal, 'Date', 'double');
if ~flag
    return
end
% figure; PlotUtils.createSScPlot(Date1); grid on; title('Date');

[flag, NbMilliSecs1] = Read_BinVariable(Datagrams, nomDirSignal, 'NbMilliSecs', 'double');
if ~flag
    return
end
% figure; PlotUtils.createSScPlot(NbMilliSecs1); grid on; title('NbMilliSecs');

[flag, PingCounter] = Read_BinVariable(Datagrams, nomDirSignal, 'PingCounter', 'single');
if ~flag
    return
end
PingCounter = unwrapPingCounterSimrad(PingCounter);
% figure; PlotUtils.createSScPlot(PingCounter, '*'); grid on; title('PingCounter');

[flag, SystemSerialNumber] = Read_BinVariable(Datagrams, nomDirSignal, 'SystemSerialNumber', 'single');
if ~flag
    return
end
% figure; PlotUtils.createSScPlot(SystemSerialNumber, '+'); grid on; title('SystemSerialNumber');

[flag, SamplingFreq] = Read_BinVariable(Datagrams, nomDirSignal, 'SamplingFreq', 'single');
if ~flag
    return
end
% figure; PlotUtils.createSScPlot(SamplingFreq, '+'); grid on; title('SamplingFreq');

[flag, SoundSpeed] = Read_BinVariable(Datagrams, nomDirSignal, 'SoundSpeed', 'single');
if ~flag
    return
end
% figure; PlotUtils.createSScPlot(SoundSpeed); grid on; title('SoundSpeed');

[flag, NTx] = Read_BinVariable(Datagrams, nomDirSignal, 'NTx', 'single');
if ~flag
    return
end
% figure; PlotUtils.createSScPlot(NTx); grid on; title('NTx');
nbTx = sum(NTx);

[flag, NRx] = Read_BinVariable(Datagrams, nomDirSignal, 'NRx', 'single');
if ~flag
    return
end
Data.NRx = NRx;
% figure; PlotUtils.createSScPlot(NRx); grid on; title('NRx');
nbRx = sum(NRx);

[flag, TotalOfReceiveBeams] = Read_BinVariable(Datagrams, nomDirSignal, 'TotalOfReceiveBeams', 'single');
if ~flag
    return
end
% figure; PlotUtils.createSScPlot(TotalOfReceiveBeams); grid on; title('TotalOfReceiveBeams');

[flag, TxTimeHeave] = Read_BinVariable(Datagrams, nomDirSignal, 'TxTimeHeave', 'single');
if ~flag
    return
end
% figure; PlotUtils.createSScPlot(TxTimeHeave); grid on; title('TxTimeHeave');

[flag, TVGOffset] = Read_BinVariable(Datagrams, nomDirSignal, 'TVGOffset', 'single');
if ~flag
    return
end
% figure; PlotUtils.createSScPlot(TVGOffset); grid on; title('TVGOffset');

[flag, TVGFunctionApplied] = Read_BinVariable(Datagrams, nomDirSignal, 'TVGFunctionApplied', 'single');
if ~flag
    return
end
% figure; PlotUtils.createSScPlot(TVGFunctionApplied); grid on; title('TVGFunctionApplied');

% NbDatagrams = length(Data.DatagramNumbers);
sub = (DatagramNumbers == 1);

% GLU : assemblage NRx par fusion des datagrammes du m�me Num�ro de Ping par Antenne.
if Head1.SystemSerialNumberSecondHead ~= 0
    NbSounders = 2;
else
    NbSounders = 1;
end

pMin = min(PingCounter(sub));
pMax = max(PingCounter(sub));
Data.NRx = zeros(pMax-pMin+1,NbSounders);
for ns=1:NbSounders
    if ns == 1
        NumSerie = Head1.SystemSerialNumber;
    else
        NumSerie = Head1.SystemSerialNumberSecondHead;
    end
    subNumSounder = find(SystemSerialNumber == NumSerie);
    for p=pMin:pMax
        subPing = find(PingCounter(subNumSounder) == p);
        if ~isempty(subPing)
            Data.NRx(p-pMin+1, ns) = sum(NRx(subNumSounder(subPing)));
        end
    end
end
% sum(NRx)
% sum(Data.NRx(:))

Date1               = Date1(sub);
NTx                 = NTx(sub);
NbMilliSecs1        = NbMilliSecs1(sub);
PingCounter         = PingCounter(sub);
SamplingFreq        = SamplingFreq(sub);
SoundSpeed          = SoundSpeed(sub);
SystemSerialNumber  = SystemSerialNumber(sub);
TVGFunctionApplied  = TVGFunctionApplied(sub);
TVGOffset           = TVGOffset(sub);
TotalOfReceiveBeams = TotalOfReceiveBeams(sub);
TxTimeHeave         = TxTimeHeave(sub);
DatagramNumbers     = DatagramNumbers(sub);
NumberOfDatagrams   = NumberOfDatagrams(sub);
% Incorrect GLU : : Data.NRx            =  NRx(sub);

pppp = [0; cumsum(double(NRx))];
pppp = pppp(sub);
NRx = diff(pppp);
NRx(end+1) = NRx(end);
% for k=1:NbDatagrams
%    k = k - Data.DatagramNumbers(k) + 1;
%    NRx(k) =  NRx(k) + (Data.DatagramNumbers(k)-1)* NRx(k);
% end
% NRx                 = NRx(sub);


%{
[flag, RxBeamNumber] = Read_BinTable(Datagrams, nomDirSignal, 'RxBeamNumber', 'single', nbRx);
if ~flag
    return
end
% RxBeamNumber = RxBeamNumber + 1;
% figure; PlotUtils.createSScPlot(RxBeamNumber, '+'); grid on; title('SamBeamNumberplingRate');
%}

NbDatagrams = length(Date1);
Data.PingCounter         = Kongsberg_table2signal(PingCounter,         NbDatagrams, PingCounter, SystemSerialNumber, Head1);
Date                     = Kongsberg_table2signal(Date1,               NbDatagrams, PingCounter, SystemSerialNumber, Head1);
NbMilliSecs              = Kongsberg_table2signal(NbMilliSecs1,        NbDatagrams, PingCounter, SystemSerialNumber, Head1);
Data.SamplingFreq        = Kongsberg_table2signal(SamplingFreq,        NbDatagrams, PingCounter, SystemSerialNumber, Head1);
Data.SystemSerialNumber  = Kongsberg_table2signal(SystemSerialNumber,  NbDatagrams, PingCounter, SystemSerialNumber, Head1);
Data.SoundSpeed          = Kongsberg_table2signal(SoundSpeed,          NbDatagrams, PingCounter, SystemSerialNumber, Head1);
Data.TVGFunctionApplied  = Kongsberg_table2signal(TVGFunctionApplied,  NbDatagrams, PingCounter, SystemSerialNumber, Head1);
Data.TVGOffset           = Kongsberg_table2signal(TVGOffset,           NbDatagrams, PingCounter, SystemSerialNumber, Head1);
Data.TotalOfReceiveBeams = Kongsberg_table2signal(TotalOfReceiveBeams, NbDatagrams, PingCounter, SystemSerialNumber, Head1);
Data.TxTimeHeave         = Kongsberg_table2signal(TxTimeHeave,         NbDatagrams, PingCounter, SystemSerialNumber, Head1);
Data.DatagramNumbers     = Kongsberg_table2signal(DatagramNumbers,     NbDatagrams, PingCounter, SystemSerialNumber, Head1);
Data.NumberOfDatagrams   = Kongsberg_table2signal(NumberOfDatagrams,   NbDatagrams, PingCounter, SystemSerialNumber, Head1);

% D�but rajout JMA le 20/09/2016 avec mise en commentaire Kongsberg_table2signal
subNaN = find(isnan(sum(Data.PingCounter,2)));
Data.NRx(subNaN,:)                 = [];
Data.PingCounter(subNaN,:)         = [];
Date(subNaN,:)                     = [];
NbMilliSecs(subNaN,:)              = [];
Data.SamplingFreq(subNaN,:)        = [];
Data.SystemSerialNumber(subNaN,:)  = [];
Data.SoundSpeed(subNaN,:)          = [];
Data.TVGFunctionApplied(subNaN,:)  = [];
Data.TVGOffset(subNaN,:)           = [];
Data.TotalOfReceiveBeams(subNaN,:) = [];
Data.TxTimeHeave(subNaN,:)         = [];
Data.DatagramNumbers(subNaN,:)     = [];
Data.NumberOfDatagrams(subNaN,:)   = [];
% Fin rajout JMA le 20/09/2016 avec mise en commentaire Kongsberg_table2signal     

% Correct PingCounter in case of dual sounder but only the starboard is on
% (correspondances between pings are calculated on indice=1 on second
% dimension)
Data.PingCounter         = repairPingCounterInCaseDualSounderButOnlyStarnoardRunning(Data.PingCounter);
Data.SamplingFreq        = repairPingCounterInCaseDualSounderButOnlyStarnoardRunning(Data.SamplingFreq);
Data.SoundSpeed          = repairPingCounterInCaseDualSounderButOnlyStarnoardRunning(Data.SoundSpeed);
Data.TVGFunctionApplied  = repairPingCounterInCaseDualSounderButOnlyStarnoardRunning(Data.TVGFunctionApplied);
Data.TVGOffset           = repairPingCounterInCaseDualSounderButOnlyStarnoardRunning(Data.TVGOffset);
Data.TotalOfReceiveBeams = repairPingCounterInCaseDualSounderButOnlyStarnoardRunning(Data.TotalOfReceiveBeams);
Data.TxTimeHeave         = repairPingCounterInCaseDualSounderButOnlyStarnoardRunning(Data.TxTimeHeave);

[flag, X] = Read_BinTable(Datagrams, nomDirSignal, 'TransmitSectorNumberTx', 'single', nbTx);
if ~flag
    return
end
TransmitSectorNumberTx = X + 1;
Data.TransmitSectorNumberTx = Kongsberg_table2image(X, 'TransmitSectorNumberTx', NbDatagrams, max(NTx), NTx, TransmitSectorNumberTx, PingCounter, SystemSerialNumber, Head1);
Data.TransmitSectorNumberTx(subNaN,:) = [];
% figure; PlotUtils.createSScPlot(Data.TransmitSectorNumberTx, '*'); title('TransmitSectorNumberTx'); grid on;
Data.TransmitSectorNumberTx = Data.TransmitSectorNumberTx + 1;

[flag, X] = Read_BinTable(Datagrams, nomDirSignal, 'TiltAngle', 'single', nbTx);
if ~flag
    return
end
Data.TiltAngle = Kongsberg_table2image(X, 'TiltAngle', NbDatagrams, max(NTx), NTx, TransmitSectorNumberTx, PingCounter, SystemSerialNumber, Head1);
Data.TiltAngle(subNaN,:) = [];
% figure; PlotUtils.createSScPlot(Data.TiltAngle, '*'); title('TiltAngle'); grid on;

[flag, X] = Read_BinTable(Datagrams, nomDirSignal, 'CentralFrequency', 'single', nbTx);
if ~flag
    return
end
Data.CentralFrequency = Kongsberg_table2image(X, 'CentralFrequency', NbDatagrams, max(NTx), NTx, TransmitSectorNumberTx, PingCounter, SystemSerialNumber, Head1);
Data.CentralFrequency(subNaN,:) = [];
% figure; PlotUtils.createSScPlot(Data.CentralFrequency, '*'); title('CentralFrequency'); grid on;

%% CORRECTION

% RxBeamNumber = RxBeamNumber + (Data.DatagramNumbers-1) .* TotalOfReceiveBeams;

% maxNRx = max(NRx);
maxNRx = max(Data.NRx); % Modif JMA le 24/11/2016 pour fichiers UNIV-ATHENS du Langseth
subBeams = 1:sum(maxNRx,2); % Modif JMA le 24/02/2017 pour fichier EM2040 DualRx
maxNRx = max(maxNRx); % Modif JMA le 24/02/2017 pour fichier EM2040 DualRx
      
% switch Datagrams.Model
%     case 3020
%         RxBeamNumber = 1 + mod(0:length(RxBeamNumber)-1, 160);
%     case {710; 122}
%         pppp = NaN(length(NRx),maxNRx, 'single');
%         for k=1:length(NRx)
%             subNRx = 1:NRx(k);
%             pppp(k,subNRx) = subNRx;
%         end
%         pppp = pppp';
%         RxBeamNumber = pppp(:);
%         RxBeamNumber(isnan(RxBeamNumber)) = [];
% %         RxBeamNumber = 1 + mod(0:length(RxBeamNumber)-1, maxNRx);
%     case 302
%         RxBeamNumber = 1 + mod(0:length(RxBeamNumber)-1, maxNRx);
%     otherwise
%         my_warndlg('Bug ici', 1);
% end


RxBeamNumber = [];

%% BeamPointingAngle

[flag, X] = Read_BinTable(Datagrams, nomDirSignal, 'BeamPointingAngle', 'single', nbRx);
if ~flag
    return
end
Data.BeamPointingAngle = Kongsberg_table2image(X, 'BeamPointingAngle', NbDatagrams, ...
    maxNRx, NRx, RxBeamNumber, PingCounter, SystemSerialNumber, Head1);
Data.BeamPointingAngle(subNaN,:) = [];
Data.BeamPointingAngle = Data.BeamPointingAngle(:,subBeams);
% figure; imagesc(Data.BeamPointingAngle); title('BeamPointingAngle'); axis xy; colorbar;

%% StartRangeNumber

[flag, X] = Read_BinTable(Datagrams, nomDirSignal, 'StartRangeNumber', 'single', nbRx);
if ~flag
    return
end
Data.StartRangeNumber = Kongsberg_table2image(X, 'StartRangeNumber', NbDatagrams, ...
    maxNRx, NRx, RxBeamNumber, PingCounter, SystemSerialNumber, Head1);
Data.StartRangeNumber(subNaN,:) = [];
Data.StartRangeNumber = Data.StartRangeNumber(:,subBeams);
% figure; imagesc(Data.StartRangeNumber); title('StartRangeNumber'); axis xy; colorbar;

%% NumberOfSamples

[flag, X] = Read_BinTable(Datagrams, nomDirSignal, 'NumberOfSamples', 'single', nbRx);
if ~flag
    return
end
Data.NumberOfSamples = Kongsberg_table2image(X, 'NumberOfSamples', NbDatagrams, ...
    maxNRx, NRx, RxBeamNumber, PingCounter, SystemSerialNumber, Head1);

Data.idebAmp = cumsum(sum(double(Data.NumberOfSamples), 2, 'omitnan'));
Data.idebAmp = 1 + [0; Data.idebAmp(1:end-1)];
Data.idebAmp(subNaN) = [];

Data.NumberOfSamples(subNaN,:) = [];
Data.NumberOfSamples = Data.NumberOfSamples(:,subBeams);
% figure; imagesc(Data.NumberOfSamples); title('NumberOfSamples'); axis xy; colorbar;

%% DetectedRangeInSamples

[flag, X] = Read_BinTable(Datagrams, nomDirSignal, 'DetectedRangeInSamples', 'single', nbRx);
if ~flag
    return
end
Data.DetectedRangeInSamples = Kongsberg_table2image(X, 'DetectedRangeInSamples', NbDatagrams, ...
    maxNRx, NRx, RxBeamNumber, PingCounter, SystemSerialNumber, Head1);
Data.DetectedRangeInSamples(subNaN,:) = [];
Data.DetectedRangeInSamples = Data.DetectedRangeInSamples(:,subBeams);
% figure; imagesc(Data.DetectedRangeInSamples); title('DetectedRangeInSamples'); axis xy; colorbar;

%% TxBeamIndex

[flag, X] = Read_BinTable(Datagrams, nomDirSignal, 'TransmitSectorNumber', 'single', nbRx);
if ~flag
    return
end
[Data.TransmitSectorNumber, indexSounder] = Kongsberg_table2image(X, 'TransmitSectorNumber', NbDatagrams, ...
    maxNRx, NRx, RxBeamNumber, PingCounter, SystemSerialNumber, Head1);
Data.TransmitSectorNumber(subNaN,:) = [];
Data.TransmitSectorNumber = Data.TransmitSectorNumber(:,subBeams);

for k=1:min(size(Data.TransmitSectorNumber,1), length(NTx))
    Data.TransmitSectorNumber(k,:) = Data.TransmitSectorNumber(k,:) + indexSounder .* NTx(k);
end
for k=(min(size(Data.TransmitSectorNumber,1), length(NTx))+1):size(Data.TransmitSectorNumber,1)
    Data.TransmitSectorNumber(k,:) = NaN;
end
% figure; imagesc(Data.TransmitSectorNumber); title('TransmitSectorNumber'); axis xy; colorbar;

%% RxBeamIndex

Data.RxBeamNumber = Kongsberg_table2image(RxBeamNumber, 'RxBeamNumber', NbDatagrams, ...
    maxNRx, NRx, RxBeamNumber, PingCounter, SystemSerialNumber, Head1);
Data.RxBeamNumber(subNaN,:) = [];
Data.RxBeamNumber = Data.RxBeamNumber(:,subBeams);
% figure; imagesc(Data.RxBeamNumber); title('RxBeamNumber'); axis xy; colorbar;

% % ---------------
% % SampleAmplitude
% 
% [flag, X] = Read_BinTable(Datagrams, nomDirSignal, 'SampleAmplitude', 'single', nbRx);
% if ~flag
%     return
% end
% Data.SampleAmplitude = Kongsberg_table2image(X, 'SampleAmplitude', NbDatagrams, ...
%     maxNRx, NRx, RxBeamNumber, PingCounter, SystemSerialNumber, Head1);
%  %   figure; imagesc(Data.SampleAmplitude); title('SampleAmplitude'); axis xy; colorbar;



% 
% Data.TransmitTiltAngle = NaN(size(Data.Range), 'single');
% for k=1:size(Data.Range,1)
%     for iBeam=1:size(Data.Range,2)
%         if isnan(Data.TxBeamIndex(k,iBeam))
%             continue
%         end
%         Data.TransmitTiltAngle(k,iBeam) = Data.TiltAngle(k,Data.TxBeamIndex(k,iBeam));
%     end
% end

%{
for k=1:length(Datagrams.Tables)
    fprintf(1,'%d  %s  %s\n', k, Datagrams.Tables(k).Name, Datagrams.Tables(k).Indexation)
end
%}



% Debut rajout JMA le 24/02/2017 pour EM2040 DualRx car les images sont
% ramen�es en une seule matrice
if Datagrams.Model == 2040
    Data.NRx                    = Data.NRx(:,1);
    Data.PingCounter            = Data.PingCounter(:,1);
    Data.SamplingFreq           = Data.SamplingFreq(:,1);
    Data.SystemSerialNumber     = Data.SystemSerialNumber(:,1);
    Data.SoundSpeed             = Data.SoundSpeed(:,1);
    Data.TVGFunctionApplied     = Data.TVGFunctionApplied(:,1);
    Data.TVGOffset              = Data.TVGOffset(:,1);
    Data.TotalOfReceiveBeams    = Data.TotalOfReceiveBeams(:,1);
    Data.TxTimeHeave            = Data.TxTimeHeave(:,1);
    Data.DatagramNumbers        = Data.DatagramNumbers(:,1);
    Data.NumberOfDatagrams      = Data.NumberOfDatagrams(:,1);
end

% rajout JMA le 24/02/2017 pour EM2040 DualRx
switch Datagrams.Model
    case 2040
        if isfield(Head1, 'SystemSerialNumberSecondHead') && (Head1.SystemSerialNumberSecondHead ~= 0)
            n = size(Data.TransmitSectorNumberTx,2);
            n = n / 2;
         	Data.TransmitSectorNumberTx = Data.TransmitSectorNumberTx(:,1:n);
            Data.TiltAngle              = Data.TiltAngle(:,1:n);
            Data.CentralFrequency       = Data.CentralFrequency(:,1:n);
%         else
%          	Data.TransmitSectorNumberTx = Data.TransmitSectorNumberTx(:,1);
%             Data.TiltAngle              = Data.TiltAngle(:,1);
%             Data.CentralFrequency       = Data.CentralFrequency(:,1);
        end
        % On suppose les dates de ping identiques entre les deux antennes
        % Rx.
        Date        = Date(:,1);
        NbMilliSecs = NbMilliSecs(:,1);
end
% Fin rajout JMA le 24/02/2017 pour EM2040 DualRx car les images sont
% ramen�es en une seule matrice

nbPings    = size(Data.BeamPointingAngle, 1);
nbSounders = size(Data.PingCounter,2);
Data.FlagPings = zeros(nbPings, nbSounders, 'uint8');

%% Remise en ordre des secteurs

Data.DatagramOrder = zeros(size(Data.TransmitSectorNumber), 'single');
for k=1:size(Data.TransmitSectorNumber,1)
    [~, ind] = sort(Data.TransmitSectorNumber(k,:));
    ordre = [ind(1):length(ind) 1:(ind(1)-1)];
    
    Data.TransmitSectorNumber(k,:)   = Data.TransmitSectorNumber(k,ordre);
    Data.DetectedRangeInSamples(k,:) = Data.DetectedRangeInSamples(k,ordre);
    Data.BeamPointingAngle(k,:)      = Data.BeamPointingAngle(k,ordre);
    Data.StartRangeNumber(k,:)       = Data.StartRangeNumber(k,ordre);
    Data.NumberOfSamples(k,:)        = Data.NumberOfSamples(k,ordre);
    Data.RxBeamNumber(k,:)           = Data.RxBeamNumber(k,ordre);
    Data.DatagramOrder(k,ordre)      = 1:length(ordre);
end

%% Calcul des dates et heures

Jour  = mod(Date,100);
Date  = (Date - Jour) / 100;
Mois  = mod(Date,100);
Annee = (Date - Mois) / 100;
Date = dayJma2Ifr(Jour, Mois, Annee);
Data.Time = cl_time('timeIfr', Date, NbMilliSecs);

%% G�n�ration du XML SonarScope

Info.Title                   = 'WaterColumn';
Info.Constructor             = 'Kongsberg';
Info.EmModel                 = Datagrams.Model;
Info.ListeSystemSerialNumber = num2str(unique(Data.SystemSerialNumber(:))');
Info.TimeOrigin              = '01/01/-4713';
Info.Comments                = 'Sounder ping rate';
Info.FormatVersion           = FormatVersion;

Info.Dimensions.nbPings       = nbPings;
Info.Dimensions.nbRx          = size(Data.BeamPointingAngle, 2);
Info.Dimensions.nbSounders    = nbSounders;
Info.Dimensions.nbTx          = max(NTx);

Info.Signals(1).Name          = 'Time';
Info.Signals(end).Dimensions  = 'nbPings, nbSounders';
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = 'days since JC';
Info.Signals(end).FileName    = fullfile('Ssc_WaterColumn', 'Time.bin');
Info.Signals(end).Tag         = verifKeyWord('SounderTime');

Info.Signals(end+1).Name      = 'PingCounter';
Info.Signals(end).Dimensions  = 'nbPings, nbSounders';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_WaterColumn', 'PingCounter.bin');
Info.Signals(end).Tag         = verifKeyWord('SounderPingCounter');

Info.Signals(end+1).Name      = 'SystemSerialNumber';
Info.Signals(end).Dimensions  = 'nbPings, nbSounders';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_WaterColumn', 'SystemSerialNumber.bin');
Info.Signals(end).Tag         = verifKeyWord('SounderSerialNumber');
 
Info.Signals(end+1).Name      = 'NumberOfDatagrams';
Info.Signals(end).Dimensions  = 'nbPings, nbSounders';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_WaterColumn', 'NumberOfDatagrams.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');
 
Info.Signals(end+1).Name      = 'DatagramNumbers';
Info.Signals(end).Dimensions  = 'nbPings, nbSounders';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_WaterColumn', 'DatagramNumbers.bin');
Info.Signals(end).Tag         = verifKeyWord('SounderPingCounter');
 
Info.Signals(end+1).Name      = 'TotalOfReceiveBeams';
Info.Signals(end).Dimensions  = 'nbPings, nbSounders';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'm';
Info.Signals(end).FileName    = fullfile('Ssc_WaterColumn', 'TotalOfReceiveBeams.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'NRx';
Info.Signals(end).Dimensions  = 'nbPings, nbSounders';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_WaterColumn', 'NRx.bin');
Info.Signals(end).Tag         = verifKeyWord('SounderPingCounter');
 
Info.Signals(end+1).Name      = 'SoundSpeed';
Info.Signals(end).Dimensions  = 'nbPings, nbSounders';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'm/s';
Info.Signals(end).FileName    = fullfile('Ssc_WaterColumn', 'SoundSpeed.bin');
Info.Signals(end).Tag         = verifKeyWord('SounderSoundSpeed');
      
Info.Signals(end+1).Name      = 'SamplingFreq';
Info.Signals(end).Dimensions  = 'nbPings, nbSounders';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'Hz';
Info.Signals(end).FileName    = fullfile('Ssc_WaterColumn', 'SamplingFreq.bin');
Info.Signals(end).Tag         = verifKeyWord('SounderSamplingRate');

Info.Signals(end+1).Name      = 'TxTimeHeave';
Info.Signals(end).Dimensions  = 'nbPings, nbSounders';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'cm';
Info.Signals(end).FileName    = fullfile('Ssc_WaterColumn', 'TxTimeHeave.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'TVGFunctionApplied';
Info.Signals(end).Dimensions  = 'nbPings, nbSounders';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile('Ssc_WaterColumn', 'TVGFunctionApplied.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'TVGOffset';
Info.Signals(end).Dimensions  = 'nbPings, nbSounders';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'dB';
Info.Signals(end).FileName    = fullfile('Ssc_WaterColumn', 'TVGOffset.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name       = 'idebAmp';
Info.Signals(end).Dimensions   = 'nbPings, 1';
Info.Signals(end).Storage      = 'double';
Info.Signals(end).Unit         = '';
Info.Signals(end).FileName     = fullfile('Ssc_WaterColumn', 'idebAmp.bin');
Info.Signals(end).Tag          = verifKeyWord('TODO');

Info.Signals(end+1).Name       = 'FlagPings';
Info.Signals(end).Dimensions   = 'nbPings, nbSounders';
Info.Signals(end).Storage      = 'uint8';
Info.Signals(end).Unit         = '';
Info.Signals(end).FileName     = fullfile('Ssc_WaterColumn', 'FlagPings.bin');
Info.Signals(end).Tag          = verifKeyWord('TODO');

Info.Images(1).Name          = 'TiltAngle';
Info.Images(end).Dimensions  = 'nbPings, nbTx, nbSounders';
Info.Images(end).Index       = 'nbPings, nbTx, nbSounders';
Info.Images(end).Storage     = 'single';
Info.Images(end).Unit        = 'deg';
Info.Images(end).FileName    = fullfile('Ssc_WaterColumn', 'TiltAngle.bin');
Info.Images(end).Tag         = verifKeyWord('SounderTiltAngle');

Info.Images(end+1).Name      = 'CentralFrequency';
Info.Images(end).Dimensions  = 'nbPings, nbTx, nbSounders';
Info.Images(end).Index       = 'nbPings, nbTx, nbSounders';
Info.Images(end).Storage     = 'single';
Info.Images(end).Unit        = 'Hz';
Info.Images(end).FileName    = fullfile('Ssc_WaterColumn', 'CentralFrequency.bin');
Info.Images(end).Tag         = verifKeyWord('TODO');

Info.Images(end+1).Name      = 'TransmitSectorNumberTx'; % TransmitSectorNumber for Tx
Info.Images(end).Dimensions  = 'nbPings, nbTx, nbSounders';
Info.Images(end).Index       = 'nbPings, nbTx, nbSounders';
Info.Images(end).Storage     = 'single';
Info.Images(end).Unit        = '';
Info.Images(end).FileName    = fullfile('Ssc_WaterColumn', 'TransmitSectorNumberTx.bin');
Info.Images(end).Tag         = verifKeyWord('TODO');

% + Spare
Info.Images(end+1).Name       = 'BeamPointingAngle';
Info.Images(end).Dimensions   = 'nbPings, nbRx';
Info.Images(end).Index        = 'nbPings, nbRx, nbSounders';
Info.Images(end).Storage      = 'single';
Info.Images(end).Unit         = 'deg';
Info.Images(end).FileName     = fullfile('Ssc_WaterColumn', 'BeamPointingAngle.bin');
Info.Images(end).Tag          = verifKeyWord('SounderBeamAngle');

Info.Images(end+1).Name       = 'StartRangeNumber';
Info.Images(end).Dimensions   = 'nbPings, nbRx';
Info.Images(end).Index        = 'nbPings, nbRx, nbSounders';
Info.Images(end).Storage      = 'single';
Info.Images(end).Unit         = 'm';
Info.Images(end).FileName     = fullfile('Ssc_WaterColumn', 'StartRangeNumber.bin');
Info.Images(end).Tag          = verifKeyWord('TODO');

Info.Images(end+1).Name       = 'NumberOfSamples';
Info.Images(end).Dimensions   = 'nbPings, nbRx';
Info.Images(end).Index        = 'nbPings, nbRx, nbSounders';
Info.Images(end).Storage      = 'single';
Info.Images(end).Unit         = 'm';
Info.Images(end).FileName     = fullfile('Ssc_WaterColumn', 'NumberOfSamples.bin');
Info.Images(end).Tag          = verifKeyWord('TODO');

Info.Images(end+1).Name       = 'DetectedRangeInSamples';
Info.Images(end).Dimensions   = 'nbPings, nbRx';
Info.Images(end).Index        = 'nbPings, nbRx, nbSounders';
Info.Images(end).Storage      = 'single';
Info.Images(end).Unit         = 'm';
Info.Images(end).FileName     = fullfile('Ssc_WaterColumn', 'DetectedRangeInSamples.bin');
Info.Images(end).Tag          = verifKeyWord('TODO');
   
Info.Images(end+1).Name       = 'TransmitSectorNumber';
Info.Images(end).Dimensions   = 'nbPings, nbRx';
Info.Images(end).Index        = 'nbPings, nbRx, nbSounders';
Info.Images(end).Storage      = 'single';
Info.Images(end).Unit         = 'm';
Info.Images(end).FileName     = fullfile('Ssc_WaterColumn', 'TransmitSectorNumber.bin');
Info.Images(end).Tag          = verifKeyWord('SounderTxBeamIndex');

Info.Images(end+1).Name       = 'RxBeamNumber';
Info.Images(end).Dimensions   = 'nbPings, nbRx';
Info.Images(end).Index        = 'nbPings, nbRx, nbSounders';
Info.Images(end).Storage      = 'single';
Info.Images(end).Unit         = '';
Info.Images(end).FileName     = fullfile('Ssc_WaterColumn', 'RxBeamNumber.bin');
Info.Images(end).Tag          = verifKeyWord('SounderRxBeamIndex');

Info.Images(end+1).Name       = 'DatagramOrder';
Info.Images(end).Dimensions   = 'nbPings, nbRx';
Info.Images(end).Index        = 'nbPings, nbRx, nbSounders';
Info.Images(end).Storage      = 'single';
Info.Images(end).Unit         = '';
Info.Images(end).FileName     = fullfile('Ssc_WaterColumn', 'DatagramOrder.bin');
Info.Images(end).Tag          = verifKeyWord('SounderRxBeamIndex');

% Ajout des champs Scale_Factor, Add_Offset et Orig_Storage.
[~, Info] = addScaleOffsetStorage(Datagrams, Info);

%% Cr�ation du fichier XML d�crivant la donn�e 

nomFicXml = fullfile(nomDirRacine, 'Ssc_WaterColumn.xml');
xml_write(nomFicXml, Info);
flag = exist(nomFicXml, 'file');
if ~flag
    messageErreur(nomFicXml)
    return
end
% edit(nomFicXml)

%% Cr�ation du r�pertoire WaterColumn

nomDirWaterColumn = fullfile(nomDirRacine, 'Ssc_WaterColumn');
if ~exist(nomDirWaterColumn, 'dir')
    status = mkdir(nomDirWaterColumn);
    if ~status
        messageErreur(nomDirWaterColumn)
        flag = 0;
        return
    end
end

%% Cr�ation des fichiers binaires des signaux

for k=1:length(Info.Signals)
    flag = writeSignal(nomDirRacine, Info.Signals(k), Data.(Info.Signals(k).Name));
    if ~flag
        return
    end
end

%% Cr�ation des fichiers binaires des images

for k=1:length(Info.Images)
    flag = writeImage(nomDirRacine, Info.Images(k), Data.(Info.Images(k).Name));
    if ~flag
        return
    end
end

flag = 1;
