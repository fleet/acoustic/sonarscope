function [flag, TypeCache] = all2ssc_checkIfAlreadyDecoded(nomFic)

if ~iscell(nomFic)
    nomFic = {nomFic};
end

N = length(nomFic);
flag = false(1,N);
TypeCache = repmat("", 1, N);
str1 = 'V�rification si les fichiers ont d�j� �t� trait�s.';
str2 = 'Check if files have been already converted.';
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    [flag(k), TypeCache(k)] = all2ssc_checkIfAlreadyDecoded_unitaire(nomFic{k});
end
my_close(hw)


function [flag, TypeCache] = all2ssc_checkIfAlreadyDecoded_unitaire(nomFic)

TypeCache = "";

% FormatVersionIncontournable = 20091111;

[nomDir, nomFicSeul, extFic] = fileparts(nomFic);
nomDirRacine = fullfile(nomDir, 'SonarScope', nomFicSeul);
% unzipSonarScopeCache(nomDirRacine);

%% Test si existence fichier Netcdf

flag = existCacheNetcdf(nomFic);
if flag
    TypeCache = "NC";
    return
end

%% Test si la conversion a �t� faite

if strcmp(extFic, '.all')
    typeFic = 'ALL';
elseif strcmp(extFic, '.kmall')
    typeFic = 'ALL';
else
    typeFic = 'WCD';
end

flag = [];
flag(end+1) = exist(nomDirRacine, 'dir');
flag(end+1) = exist(fullfile(nomDirRacine, [typeFic '_FileIndex.xml']), 'file');
flag(end+1) = exist(fullfile(nomDirRacine, [typeFic '_FileIndex']), 'dir');

% flag(end+1) = exist(fullfile(nomDirRacine, 'Ssc_Attitude.xml'), 'file');
% flag(end+1) = exist(fullfile(nomDirRacine, 'Ssc_Attitude'), 'dir');

% if ~RDL
%     flag(end+1) = exist(fullfile(nomDirRacine, 'Ssc_Clock.xml'), 'file');
%     flag(end+1) = exist(fullfile(nomDirRacine, 'Ssc_Clock'), 'dir');
% end

flag(end+1) = exist(fullfile(nomDirRacine, 'Ssc_Depth.xml'), 'file');
flag(end+1) = exist(fullfile(nomDirRacine, 'Ssc_Depth'), 'dir');

% if ~RDL
%     flag(end+1) = exist(fullfile(nomDirRacine, 'Ssc_InstallationParameters.xml'), 'file');
%     flag(end+1) = exist(fullfile(nomDirRacine, 'Ssc_InstallationParameters'), 'dir');
% end

% flag(end+1) = exist(fullfile(nomDirRacine, 'Ssc_InstallationParameters.xml'), 'file');
% flag(end+1) = exist(fullfile(nomDirRacine, 'Ssc_InstallationParameters'), 'dir');

%{
% Mise en commentaire � cause des fichiers de Mashkoor : t�te sonar mont�e
% sur ponton
flag(end+1) = exist(fullfile(nomDirRacine, 'Ssc_Position.xml'), 'file');
flag(end+1) = exist(fullfile(nomDirRacine, 'Ssc_Position'), 'dir');
%}

flag(end+1) = exist(fullfile(nomDirRacine, 'Ssc_Runtime.xml'), 'file');
flag(end+1) = exist(fullfile(nomDirRacine, 'Ssc_Runtime'), 'dir');

% flag(end+1) = exist(fullfile(nomDirRacine, 'Ssc_SoundSpeedProfile.xml'), 'file');
% flag(end+1) = exist(fullfile(nomDirRacine, 'Ssc_SoundSpeedProfile'), 'dir');

%{
% Mise en commentaire � cause du fichier 0005_20080703_224347_raw.all du NIWA car il ne contient pas de donn�es Raw Range & Beam Angle
flag(end+1) = exist(fullfile(nomDirRacine, 'Ssc_RawRangeBeamAngle.xml'), 'file');
flag(end+1) = exist(fullfile(nomDirRacine, 'Ssc_RawRangeBeamAngle'), 'dir');
%}

% flag(end+1) = exist(fullfile(nomDirRacine, 'Ssc_SurfaceSoundSpeed'), 'dir');

% flag = 0

% DEB :  https://forge.ifremer.fr/tracker/index.php?func=detail&aid=255&group_id=126&atid=535
% Traitement du cas o� l'op�rateur recopie le WCD apr�s avoir import�
% le ALL dans SSC.
% Si le Cache WC n'existe pas alors que le fichier wcd est pr�sent �
% c�t� du .all, on provoque la relance du CONVERT_ALL.
if exist(fullfile(nomDir, [nomFicSeul '.wcd']), 'file')
%     nomFicWc = fullfile(nomDir, [nomFicSeul '.wcd']);
    flag(end+1) = exist(fullfile(nomDirRacine, 'Ssc_WaterColumn.xml'), 'file');
end
% FIN : https://forge.ifremer.fr/tracker/index.php?func=detail&aid=255&group_id=126&atid=729

if all(flag)
    flag = 1;
    TypeCache = "XML";
    return
else
    flag = 0;
end

nomFicTmp = [nomDirRacine '_tmp'];
if exist(nomFicTmp, 'dir')
    message_erreur_convert(nomFicTmp, nomFic, nomDirRacine)
    flag = 0;
    return
end
