% Calcul de la position des staves
%
% Syntax
%   [AngDebSec, FrSec, AngSec, OuvSec] = calPosStaves
%
% Name-Value Pair Arguments
%   TabTemps :
%
% Output Arguments
%   AngDebSec :
%   FrSec     :
%   AngSec    : Angle central
%   OuvSec    :
%
% Examples
%   [AngDebSec, FrSec, AngSec, OuvSec] = calPosStaves
%
% See also Authors
% Authors : JMA + XL
%-----------------------------------------------------------------------

function [AngDebSec, FrSec, AngSec, OuvSec] = calPosStaves( TXMode )

if nargin == 0
    help calPosStaves
    return
end

% Calcul de la position des staves

Dstv = 0.02425;
for i=1:4
    for j=1:16
        k = (i-1) * 16 + j;
        Posstv(k) = (j-1) * Dstv + (i-1) * (16 * Dstv + 0.01825);
    end
end
Milieu = (Posstv(32) + Posstv(33)) / 2;
Posstv = Posstv - Milieu;
