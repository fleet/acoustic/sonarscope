% Calcul de la position des staves
%
% Syntax
%   Posstv = calPosStavesEm300
%
% Output Arguments
%   Posstv : Position des staves en m.
%
% Examples
%   Posstv = calPosStavesEm300;
%   plot(Posstv, '+'); grid on;
%
% See also Authors
% Authors : JMA + XL
%-----------------------------------------------------------------------

function Posstv = calPosStavesEm300

Dstv = 0.02425;
for i=1:4
    for j=1:16
        k = (i-1) * 16 + j;
        Posstv(k) = (j-1) * Dstv + (i-1) * (16 * Dstv + 0.01825);
    end
end
Milieu = (Posstv(32) + Posstv(33)) / 2;
Posstv = Posstv - Milieu;
