% Chantier de travail Staves EM300
%
% See also Authors
% Authors : XL + JMA
% VERSION  : $Id: chantierEM300.m,v 1.2 2002/06/20 11:46:39 augustin Exp $
%-----------------------------------------------------------------------

nomFic = getNomFicDatabase('EM300_Staves_st163820.00');

% ------------------
% Lecture du fichier

[PingCounter, Nstave, Nech, TXMode, Rawd, Tvgc, Tvgu] = lecFicStaves( nomFic );
figure
subplot(2,1,1); plot(Tvgc); grid on; set(gca, 'XLim', [1 Nech]); title('Tvgc');
subplot(2,1,2); plot(Tvgu); grid on; set(gca, 'XLim', [1 Nech]); title('Tvgu');
% --------------------
%Affichage de l'image
figure;
imagesc(abs(Rawd)); title('Rawd');      

% -----------------------------------------------------------------------------------
% Essai de filtrage de la donnee brute pour voir si il y a derive de certaines staves

n = 100;
b = ones(1,n) / n;
a = zeros(1,n); a(1) = 1;
RawdFiltree = (filter(b, a, Rawd'))';
%RawdFiltree =  moyenne(Rawd, n);
figure;
imagesc(abs(RawdFiltree)); title('Donnee brute filtree');
RawdFiltree = (filter(b, a, abs(Rawd)'))';
figure;
imagesc(RawdFiltree); title('Donnee brute filtree');
figure
plot(abs(Rawd(60,:)))
hold on;  plot(abs(RawdFiltree(60,:)),'r'); hold off;

% -----------------------
% Definition des secteurs
[AngDebSec, FrSec, AngSec, OuvSec] = getParSecEm300( TXMode );

% --------------------------------
% Calcul de la position des staves

Posstv = calPosStavesEm300;
Lreldemiant=2/3;			% longueur relative de sous-antenne
Ndemiant= round(Nstave*Lreldemiant);	% longueur de sous-antenne en nombre de staves
Tabstave = zeros (2, Ndemiant);
TabStave(1,:)=1:Ndemiant;						% ordre des staves de premi�re demi-antenne
Nmid1=(Ndemiant-1)/2;							% ordre du milieu de la premi�re demi-antenne
TabStave(2,:)=Nstave-Ndemiant+1:Nstave;	% ordre des staves de seconde demi-antenne
Nmid2 = Nstave-Nmid1+1;							% ordre du milieu de la seconde demi-antenne

Posstv2=zeros(2,Nstave);
Posstv2(1,TabStave(1,:))=Posstv(TabStave(1,:))-Posstv(Nmid1);
Posstv2(2,TabStave(2,:))=Posstv(TabStave(2,:))-Posstv(Nmid2);


% -------------------
% Formation des voies
Angles = 73:-1:-73;
nbFais = length(Angles);
for i=1:nbFais
    indexAng(i) = 1;
    for j=1 : length(AngDebSec)
        if (Angles(i) < AngDebSec(j))
            indexAng(i) = j+1;
        end
    end
end
FrFais = FrSec(indexAng) * 1000;
SinAng = sin(Angles * pi / 180);
Cel = 1500;
coefk = 2 * pi / Cel;
Fech = 4509;

% FORMATION DE VOIES PAR DEPHASAGE ET/OU DECALAGE
% Calcul des dephasages et des exponentielles complexes; et des d�calages temporels

Phase = zeros(2, nbFais, Nstave);
expjph = zeros(2, nbFais, Nstave);
NbDecal = zeros(2, nbFais, Nstave);
PhaseRes = zeros(2, nbFais, Nstave);
expjphres = zeros(2, nbFais, Nstave);
for m = 1 : 2
    for i = 1 : nbFais
        Phase(m, i, :) = coefk * FrFais(i) * Posstv2(m, :) * SinAng(i);
        expjph(m, i, :) = exp( complex(0, 1) * Phase(m,i,:));
        NbDecal(m, i, :) = round(Phase(m,i,:)/ 2 / pi / FrFais(i) * Fech);
        PhaseRes(m, i, :) = Phase(m,i, :) - NbDecal(m,i, :) * 2 * pi * FrFais(i) / Fech;
        expjphres(m, i, :) = exp( complex(0, 1) * PhaseRes(m,i,:));
    end
end

% ----------------------------------------
% S�lection de la portion temporelle utile

Ndeb=640;
Nfin=1200;
Rawdu=Rawd(:,Ndeb:Nfin);					% donn�es brutes sur les staves, limit�es aux �chantillons temporels utiles
Nechu=Nfin-Ndeb+1;
Rawdust = complex(zeros(1,Nstave));		% donn�e brute sur les staves � un instant donn�

% ----------------------------------------------------
% Calcul des signaux temporels pour chaque voie formee
% FV en d�calage temporel
tic
SigVoie=complex(zeros(2, nbFais, Nechu));
Ndecalmax = max(max(NbDecal));

for m = 1 : 2
    hw = create_waitbar('Calcul des signaux temporels...', 'N', Nechu);
    for i = 1 + Ndecalmax : Nechu - Ndecalmax
        my_waitbar(i, Nechu, hw);
        for j= nbFais:-1:1
            for n = 1: Ndemiant
                k=TabStave(m, n);
                Rawdust(k)= Rawdu(k, i + NbDecal(j,k));
            end
            SigVoie(m, j, i) = expjphres(j, TabStave(m, :)) * Rawdust(TabStave(m, :))' ; 
        end
    end
    my_close(hw)
end
SigVoie = SigVoie / Ndemiant;
toc


% FV en d�phasage

tic
SigVoie=complex(zeros(2, nbFais, Nechu));
for m = 1 : 2
    hw = create_waitbar('Calcul des signaux temporels...', 'N', Nechu);
    for i=1:Nechu
        my_waitbar(i, Nechu, hw);
        for j =  nbFais:-1:1
            SigVoie(m, j, i) = expjph(j, TabStave(m, :)) * Rawdu(TabStave(m, :), i); 
        end		% for j
    end			% for i
    my_close(hw)
end				% for m
SigVoie = SigVoie / Ndemiant;
toc


% ------------
% Demodulation

tic
SigVoieDemodulee = zeros(2, nbFais, Nechu);
expjph2 = complex(zeros(nbFais, Nechu));
dfFrFais = FrFais - 31.5;
for m=1:2
   create_waitbar('D�modulation ...', 'N', nbFais);
    for j = 1:nbFais
        my_waitbar(j, nbFais, hw);
        for i = 1:Nechu
            ph = 2 * pi * dfFrFais(j) * i / 4.509;
            expjph2(j, i) = exp( complex(0, 1) * ph);
            SigVoieDemodulee(m, j, i) = expjph2(j, i) * SigVoie(m, j, i);
        end
    end
    my_close(hw)
end
toc



% --------
% Filtrage

tic
SigSsVoie1 = zeros( nbFais, Nechu);
SigSsVoie2 = zeros( nbFais, Nechu);
hw = create_waitbar( 'Filtrage ...', 'N', nbFais);
for j=1:nbFais
    my_waitbar(j, nbFais, hw);
    SigSsVoie1(j, :) = filter(ones(1,6)/6, [1 0 0 0 0 0], SigVoieDemodulee(1, j, :));
    SigSsVoie2(j, :) = filter(ones(1,6)/6, [1 0 0 0 0 0], SigVoieDemodulee(2, j, :));
end
my_close(hw)
toc

figure;
imagesc(abs(SigSsVoie1),[0 256]);
title('Sous-voie 1, demodulation et filtrage');

figure;
imagesc(abs(SigSsVoie2),[0 256]);
title('Sous-voie 2, demodulation et filtrage');


%-------------------------------------
% Calcul de d�phasage entre demi-voies

% DdphSsVoie = zeros(nbFais, Nechu);
DdphSsVoie = atan2( imag(SigSsVoie1./ SigSsVoie2) , real(SigSsVoie1./ SigSsVoie2) );
DdphSsVoie = DdphSsVoie / pi * 180;

figure;
imagesc(-abs(DdphSsVoie), [-180 0] );
title('Diff. de phase avec F.V. en phase');










% --------
% Bricoles

[b,a] = butter(4, 1000/4509);
y = filter(b, a, SigVoie);
imagesc(log(abs(y)));



figure
for j=1:nbFais
    subplot(2,1,1);
    plot(abs(SigVoie(j,:))); hold on; plot(abs(SigVoieDemoduleeFiltree(j,:)), 'r'); hold off; grid on;
    title(sprintf('Faisceau %d', j));
    subplot(2,1,2);
    plot(angle(SigVoie(j,:))); hold on; plot(angle(SigVoieDemoduleeFiltree(j,:)), 'r'); hold off; grid on;
    title(sprintf('Faisceau %d', j));
    pause
end
