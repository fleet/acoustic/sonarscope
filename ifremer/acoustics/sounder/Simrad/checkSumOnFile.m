% Calcul du checksum pour le fichier en cours d'�criture, avec un d�but et fin
% d'index
function [flag, cs] = checkSumOnFile(fid, boi, eoi)

flag = 0;
cs   = uint32(0);
try
        
    % Calcul lecture de blocs de N octets.
    fseek(fid, boi, 'bof');
    nbOctets    = eoi - boi;
    sizeBuffer  = 500;
    nLoopRead   = fix(nbOctets/sizeBuffer);
    % Lecture par bloc d'octets.
    for i=1:nLoopRead
        buffer = fread(fid, sizeBuffer, 'uint8');
        cs     = cs + sum(uint32(buffer));
    end
    % Fin de la lecture des ovets restants.
    while ftell(fid) < eoi
        octetLu = fread(fid, 1, 'uint8');
        cs      = cs + uint32(octetLu);
    end
    % Bornage du checkSum en uint16 par modulo.
    cs = uint16(mod(cs,65536));
  
    %%
    % Positionnement apr�s �criture du End of Text.
    fseek(fid, eoi+1, 'bof');
    
catch %#ok<CTCH>
    flag    = 1;
    cs      = [];
end
