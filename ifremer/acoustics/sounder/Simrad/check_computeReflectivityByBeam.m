function check_computeReflectivityByBeam

nomFic = getNomFicDatabase('0011_20110307_211804_IKATERE_02.all');
aKM = cl_simrad_all('nomFic', nomFic);

%% Mean on dB values

tic
[flag, DataDepth] = computeReflectivityByBeam(aKM, 'ForceMatlab', 1, 'MeanCompType', 1);
toc
if ~flag
    return
end
Reflec_Matlab_dB = DataDepth.ReflectivityFromSnippets;

tic
[flag, DataDepth] = computeReflectivityByBeam(aKM, 'ForceMatlab', 0, 'MeanCompType', 1);
toc
if ~flag
    return
end
Reflec_Mex_dB = DataDepth.ReflectivityFromSnippets;

dif_dB = Reflec_Mex_dB - Reflec_Matlab_dB;
stats_dB = stats(dif_dB) %#ok<*NOPRT,*NASGU>
% SonarScope(dif_dB)

%% Mean on Amp values

tic
[flag, DataDepth] = computeReflectivityByBeam(aKM, 'ForceMatlab', 1, 'MeanCompType', 2);
toc
if ~flag
    return
end
Reflec_Matlab_Amp = DataDepth.ReflectivityFromSnippets;

tic
[flag, DataDepth] = computeReflectivityByBeam(aKM, 'ForceMatlab', 0, 'MeanCompType', 2);
toc
if ~flag
    return
end
Reflec_Mex_Amp = DataDepth.ReflectivityFromSnippets;

dif_Amp = Reflec_Mex_Amp - Reflec_Matlab_Amp;
stats_Amp = stats(dif_Amp)
% SonarScope(dif_Amp)

%% Mean on Enr values

tic
[flag, DataDepth] = computeReflectivityByBeam(aKM, 'ForceMatlab', 1, 'MeanCompType', 3);
toc
if ~flag
    return
end
Reflec_Matlab_Enr = DataDepth.ReflectivityFromSnippets;

tic
[flag, DataDepth] = computeReflectivityByBeam(aKM, 'ForceMatlab', 0, 'MeanCompType', 3);
toc
if ~flag
    return
end
Reflec_Mex_Enr = DataDepth.ReflectivityFromSnippets;

dif_Enr = Reflec_Mex_Enr - Reflec_Matlab_Enr;
stats_Enr = stats(dif_Enr);
% SonarScope(dif_Enr)

%% Median values

tic
[flag, DataDepth] = computeReflectivityByBeam(aKM, 'ForceMatlab', 1, 'MeanCompType', 4);
toc
if ~flag
    return
end
Reflec_Matlab_Med = DataDepth.ReflectivityFromSnippets;

tic
[flag, DataDepth] = computeReflectivityByBeam(aKM, 'ForceMatlab', 0, 'MeanCompType', 4);
toc
if ~flag
    return
end
Reflec_Mex_Med = DataDepth.ReflectivityFromSnippets;

dif_Med = Reflec_Mex_Med - Reflec_Matlab_Med;
stats_Med = stats(dif_Med);
% SonarScope(dif_Med)

