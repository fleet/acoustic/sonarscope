% Recherche automatique des sondes douteuses par analyse du facteur de
% qualit�.
%
% Syntax
%   [Masque, Proba] = cleanDetection(QualityFactor, ...)
%
% Input Arguments
%   QualityFactor : Facteur de qualit� d�livr� par Simrad ou
%                   Detection (1 pour amplitude, 2 pour la phase)
%
% Name-Value Pair Arguments
%   TypeLayer  : Type de layer (1=Qualityfactor, 0=Detection) (1 par defaut)
%   SeuilProba : Seuil de decision de mauvaise sonde (0.5 par defaut)
%
% Output Arguments
%   Masque : Masque de validite des sones (1=Bonne sonde, NaN : mauvaise sonde)
%   Proba  : Probabilite de bonne sonde
%
% Examples
%   nomFicIn  = getNomFicDatabase('EM1002_0001_20040916_054013_raw.all')
%   nomFicOut = [my_tempdir filesep 'toto.all']
%   copyfile(nomFicIn, nomFicOut)
%   aKM = cl_simrad_all('nomFic', nomFicOut);
%   [flag, DataDepth] = read_depth_bin(aKM)
%   figure; imagesc(DataDepth.Depth); colorbar
%   histo1D(DataDepth.Depth)
%
%   [MasqueDetection, ProbaDetection] = cleanDetection(DataDepth.QualityFactor);
%   figure; imagesc(MasqueDetection); colorbar
%   figure; imagesc(ProbaDetection); colorbar
%   figure; imagesc(DataDepth.Depth .* MasqueDetection); colorbar
%
%   write_masque(aKM, 'MasqueDetection', MasqueDetection)
%   [flag, DataDepth] = read_depth_bin(aKM, 'MasqueDetection')
%   figure; imagesc(DataDepth.Depth); colorbar
%   histo1D(DataDepth.Depth)
%
%   [MasqueDepth, ProbaHisto] = cleanHisto(DataDepth.Depth);
%   figure; imagesc(ProbaHisto); colorbar
%   figure; imagesc(MasqueDepth); colorbar
%   figure; imagesc(DataDepth.Depth .* MasqueDepth); colorbar
%   figure; imagesc(DataDepth.Depth .* MasqueDepth .* MasqueDetection); colorbar
%
%   write_masque(aKM, 'MasqueDepth', MasqueDepth)
%   [flag, DataDepth] = read_depth_bin(aKM, 'MasqueDepth')
%   figure; imagesc(DataDepth.Depth); colorbar
%   histo1D(DataDepth.Depth)
%
%   [flag, DataDepth] = read_depth_bin(aKM, 'MasqueDetection', 'MasqueDepth')
%   figure; imagesc(DataDepth.Depth); colorbar
%   histo1D(DataDepth.Depth)
%
%   nomFicVerif = [my_tempname '.all']
%   copyfile(nomFicOut, nomFicVerif)
%   bKM = cl_simrad_all('nomFic', nomFicVerif);
%   [flag, X] = read_depth_bin(bKM)
%   figure; imagesc(X.Depth); colorbar
%
% See also cl_simrad_all Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [Masque, ProbaAmplitude] = cleanDetection(Detection, varargin)

% [varargin, TypeLayer] = getPropertyValue(varargin, 'TypeLayer', 1);
[varargin, SeuilProba] = getPropertyValue(varargin, 'SeuilProba', 0.5); %#ok<ASGLU>

% switch TypeLayer
%     case 1
%         Detection = (Detection < 128);
%     case 0
%         Detection = (Detection == 1);
% end
% Detection = single(Detection);
% Detection(isnan(Detection)) = NaN;

M = my_nanmean(Detection);
M = M / sum(M);
MS = cumsum(M);

x = 1:length(MS);
X = repmat(x, size(Detection, 1), 1);

MU    = sum(x .* M);
SIGMA = sqrt(sum((x-MU).^2 .* M));

ProbaAmplitude = normpdf(X,MU, SIGMA/2);
ProbaAmplitude = ProbaAmplitude / max(ProbaAmplitude(:));
% ProbaPhase = normpdf(X,MU, SIGMA/4);
% ProbaPhase = 1 - ProbaPhase / max(ProbaPhase(:));
% figure; imagesc(ProbaAmplitude); colorbar

Proba = ones(size(Detection), 'single');
sub = (Detection == 1);
Proba(sub) = ProbaAmplitude(sub);
% sub = (Detection == 2);
% Proba(sub) = ProbaPhase(sub);

Masque = zeros(size(Proba), 'single');
Masque(Proba < SeuilProba) = 1;
