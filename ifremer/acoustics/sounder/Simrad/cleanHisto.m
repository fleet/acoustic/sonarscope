% Recherche automatique des sondes douteuses par analyse de l'histogramme METHODE A PERFECTIONNER
%
% Syntax
%   [Masque, Proba] = cleanHisto(X)
%
% Input Arguments
%   X : Profondeur
%
% Name-Value Pair Arguments
%   SeuilProba : Seuil de decision de mauvaise sonde (0.5 par defaut)
%   nbLigBlock : Taille des blocks (1000 par defaut)
%
% Output Arguments
%   Masque : Masque de validite des sones (1=Bonne sonde, NaN : mauvaise sonde)
%   Proba  : Probabilite de bonne sonde
%
% Examples
%   nomFicIn  = getNomFicDatabase('EM1002_0001_20040916_054013_raw.all')
%   nomFicOut = [my_tempdir filesep 'toto.all']
%   copyfile(nomFicIn, nomFicOut)
%   aKM = cl_simrad_all('nomFic', nomFicOut);
%   [flag, DataDepth] = read_depth_bin(aKM)
%   figure; imagesc(DataDepth.Depth); colorbar
%   histo1D(DataDepth.Depth)
%
%   [MasqueDetection, ProbaDetection] = cleanDetection(DataDepth.QualityFactor);
%   figure; imagesc(MasqueDetection); colorbar
%   figure; imagesc(ProbaDetection); colorbar
%   figure; imagesc(DataDepth.Depth .* MasqueDetection); colorbar
%
%   write_masque(aKM, 'MasqueDetection', MasqueDetection)
%   [flag, DataDepth] = read_depth_bin(aKM, 'MasqueDetection')
%   figure; imagesc(DataDepth.Depth); colorbar
%   histo1D(DataDepth.Depth)
%
%   [MasqueDepth, ProbaHisto] = cleanHisto(DataDepth.Depth);
%   figure; imagesc(ProbaHisto); colorbar
%   figure; imagesc(MasqueDepth); colorbar
%   figure; imagesc(DataDepth.Depth .* MasqueDepth); colorbar
%   figure; imagesc(DataDepth.Depth .* MasqueDepth .* MasqueDetection); colorbar
%
%   write_masque(aKM, 'MasqueDepth', MasqueDepth)
%   [flag, DataDepth] = read_depth_bin(aKM, 'MasqueDepth')
%   figure; imagesc(DataDepth.Depth); colorbar
%   histo1D(DataDepth.Depth)
%
%   [flag, DataDepth] = read_depth_bin(aKM, 'MasqueDetection', 'MasqueDepth')
%   figure; imagesc(DataDepth.Depth); colorbar
%   histo1D(DataDepth.Depth)
%
%   nomFicVerif = [my_tempname '.all']
%   copyfile(nomFicOut, nomFicVerif)
%   bKM = cl_simrad_all('nomFic', nomFicVerif);
%   [flag, DataDepth] = read_depth_bin(bKM)
%   figure; imagesc(DataDepth.Depth); colorbar
%
% See also cl_simrad_all Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [Masque, Proba] = cleanHisto(X, varargin)

[varargin, SeuilProba] = getPropertyValue(varargin, 'SeuilProba', 0.5);
[varargin, nbLigBlock] = getPropertyValue(varargin, 'nbLigBlock', 1000); %#ok<ASGLU>

Masque = zeros(size(X), 'single');
Proba  = zeros(size(X), 'single');

fig1 = figure;
nbRows = size(Masque, 1);
subc = 1:size(X,2);
for k1=1:nbLigBlock:nbRows
    subl = k1:min(nbRows, k1+nbLigBlock-1);
    M = ones(length(subl), length(subc), 'single');
    
    [N, bins] = histo1D(X(subl,:));
    
    p = N / sum(N);
    MU    = sum(bins .* p);
    SIGMA = sqrt(sum((bins-MU).^2 .* p));
    ProbaBlock = normpdf(X(subl,:) , MU, SIGMA*4);
    ProbaBlock = ProbaBlock / max(ProbaBlock(:));
    
    Proba(subl,:) = ProbaBlock;
    % figure; imagesc(Proba); colorbar
    K = (ProbaBlock > SeuilProba);
    M(K) = 1;
    
    %     p = (N < 10);
    p = cumsum(N);
    p = p / max(p);
    p = (p < 0.0001) | (p > 0.9999);
    
    %% On passe la main � l'op�rateur
    
    Y = X(subl,:) .* M;
    figure(fig1)
    imagesc(subl, subc, Y); axis xy; colorbar
    sub = getFlaggedBinsOfAnHistogram(bins, N, 'Flag', ~p);
    
    pas = bins(2) - bins(1);
    for k2=1:length(sub)
        k = sub(k2);
        [I,J] = find((Y >= (bins(k)-pas/2)) & (Y <= (bins(k)+pas/2)));
        for k3=1:length(I)
            Masque(subl(I(k3)),J(k3)) = 1;
        end
    end
    %     Y = X(subl,:) .* Masque(subl,:);
end
my_close(fig1)
