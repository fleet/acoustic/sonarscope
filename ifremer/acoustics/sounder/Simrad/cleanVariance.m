% Recherche automatique des sondes douteuses par analyse du facteur de
% qualit�.
%
% Syntax
%   [Masque, Proba] = cleanVariance(QualityFactor, ...)
%
% Input Arguments
%   QualityFactor : Facteur de qualit� d�livr� par Simrad ou
%                   Detection (1 pour amplitude, 2 pour la phase)
%
% Name-Value Pair Arguments
%   TypeLayer  : Type de layer (1=Qualityfactor, 0=Detection) (1 par defaut)
%   SeuilProba : Seuil de decision de mauvaise sonde (0.5 par defaut)
%
% Output Arguments
%   Masque : Masque de validite des sones (1=Bonne sonde, NaN : mauvaise sonde)
%   Proba  : Probabilite de bonne sonde
%
% Examples
%
% See also cleanVariance Authors
% Authors : JMA
% VERSION  : $Id: cleanVariance.m,v 1.1 2002/10/21 11:59:51 emenut Exp emenut $
% ----------------------------------------------------------------------------

function [Masque, Proba] = cleanVariance(Variance, varargin)

[varargin, TypeLayer] = getPropertyValue(varargin, 'TypeLayer', 1);
[varargin, SeuilProba] = getPropertyValue(varargin, 'SeuilProba', 0.5);

subNaN = find(isnan(Variance));
switch TypeLayer
    case 1
        Detection = (Variance < 128);
    case 0
        Detection = (Variance == 1);
end
Variance = single(Variance);
Variance(subNaN) = NaN;

M = my_nanmean(Variance);
M = M / sum(M);
MS = cumsum(M);

x = 1:length(MS);
X = repmat(x, size(Variance, 1), 1);

MU    = sum(x .* M);
SIGMA = sqrt(sum((x-MU).^2 .* M));

ProbaVariance = normpdf(X,MU, SIGMA/2);
ProbaVariance = ProbaVariance / max(ProbaVariance(:));


Proba = NaN(size(Variance), 'single');
sub = (Variance == 1);
Proba(sub) = ProbaVariance(sub);


Masque = ones(size(Proba), 'single');
Masque(Proba >= SeuilProba) = NaN;


