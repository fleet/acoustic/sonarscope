% Codes des datagrams de l'EM300
%
% Syntax
%   texteTypeDatagram = codesDatagramsSimrad(...)
%
% Name-Value Pair Arguments
%   tabHistoTypeDatagram : Histogramme des types de datagram
%
% Name-Value Pair Arguments
%   fullListe : 0=affichage que des datagrams presents, 1=tous les datagrams
%
% Output Arguments
%   texteTypeDatagram : Chaine de caracteres
%       48 (30h) = Installation parameter : Stop (0)
%       51 (33h) = Extra parameters
%       65 (41h) = Attitude (0)
%       67 (43h) = Clock (0)
%       68 (44h) = Depth (0)
%       69 (45h) = Single beam echo sounder depth (0)
%       70 (46h) = Raw range and beam angle (0)
%       71 (47h) = Surface sound speed ??? (0)
%       72 (48h) = Heading (0)
%       73 (49h) = Installation parameter : Start (0)
%       74 (4Ah) = Mechanical transducer tilt (0)
%       75 (4Bh) = Central beams echogram (0)
%       80 (50h) = Position (0)
%       82 (52h) = Runtime parameter (0)
%       83 (53h) = Seabed image (0)
%       84 (54h) = Tide (0)
%       85 (55h) = Sound speed profile (0)
%       87 (57h) = SSP Output (0)
%       104 (68h) = Height (0)
%       105 (69h) = Installation parameter : Stop (0)
%       107 (6Bh) = IWater Column
%       112 (70h) = Installation parameter : Remote info (0)
%       114 (72h) = Mag&Phase (0)
%
% Examples
%   codesDatagramsSimrad
%
% See also Authors
% Authors : JMA
%-----------------------------------------------------------------------

function texteTypeDatagram = codesDatagramsSimrad(varargin)

[varargin, fullListe] = getPropertyValue(varargin, 'fullListe', 0);

LabelsTypeDatagram{48}  = 'Installation parameter : Stop';
LabelsTypeDatagram{49}  = 'PU Status';
LabelsTypeDatagram{51}  = 'Extra Parameters';
LabelsTypeDatagram{65}  = 'Attitude';
LabelsTypeDatagram{67}  = 'Clock';
LabelsTypeDatagram{68}  = 'Depth';
LabelsTypeDatagram{69}  = 'Single beam echo sounder depth';
LabelsTypeDatagram{70}  = 'Raw range and beam angle (F)';
LabelsTypeDatagram{71}  = 'Surface sound speed ???';
LabelsTypeDatagram{72}  = 'Heading';
LabelsTypeDatagram{73}  = 'Installation parameter : Start';
LabelsTypeDatagram{74}  = 'Mechanical transducer tilt';
LabelsTypeDatagram{75}  = 'Central beams echogram';
LabelsTypeDatagram{78}  = 'Raw range and angle 78';
LabelsTypeDatagram{79}  = 'Ifremer Quality factor';
LabelsTypeDatagram{80}  = 'Position';
LabelsTypeDatagram{82}  = 'Runtime parameter';
LabelsTypeDatagram{83}  = 'Seabed image';
LabelsTypeDatagram{84}  = 'Tide';
LabelsTypeDatagram{85}  = 'Sound speed profile';
LabelsTypeDatagram{87}  = 'SSP Output';
LabelsTypeDatagram{88}  = 'XYZ 88';
LabelsTypeDatagram{89}  = 'Seabed image data 89';
LabelsTypeDatagram{102} = 'Raw range and beam angle (f)';
LabelsTypeDatagram{104} = 'Height';
LabelsTypeDatagram{105} = 'Installation parameter : Stop';
LabelsTypeDatagram{107} = 'Water Column';
LabelsTypeDatagram{108} = 'Water Column'; % ATTENTION : ca devrait �tre 107. Il y a certainement un +1 quelque part
LabelsTypeDatagram{109} = 'Stave data ';
LabelsTypeDatagram{110} = 'Network attitude velocity';
LabelsTypeDatagram{112} = 'Installation parameter : Remote info';
LabelsTypeDatagram{113} = 'Quality Factor';
LabelsTypeDatagram{114} = 'Mag&Phase';

if isempty(varargin)
    code = [];
    tabHistoTypeDatagram = zeros(size(LabelsTypeDatagram));
else
    code = varargin{1};
    tabHistoTypeDatagram = varargin{2};
end

texteTypeDatagram = '';
for k=1:length(LabelsTypeDatagram)
    if isempty(LabelsTypeDatagram{k})
        sub = find(k == code);
        if ~isempty(sub) && (tabHistoTypeDatagram(sub) ~= 0)
            texteTypeDatagram = sprintf('%s\n%d (%sh) = %s (%d)', ...
                texteTypeDatagram, code(sub), dec2hex(code(sub)), '?????????', tabHistoTypeDatagram(sub));
        end
    else
        sub = find(k == code);
        if isempty(sub)
            if fullListe
                texteTypeDatagram = sprintf('%s\n%d (%sh) = %s (0)', ...
                    texteTypeDatagram, k, dec2hex(k), LabelsTypeDatagram{k});
            end
        else
            if fullListe
                texteTypeDatagram = sprintf('%s\n%d (%sh) = %s (%d)', ...
                    texteTypeDatagram, code(sub), dec2hex(code(sub)), LabelsTypeDatagram{k}, tabHistoTypeDatagram(sub));
            else
                if tabHistoTypeDatagram(sub) ~= 0
                    texteTypeDatagram = sprintf('%s\n%d (%sh) = %s (%d)', ...
                        texteTypeDatagram, code(sub), dec2hex(code(sub)), LabelsTypeDatagram{k}, tabHistoTypeDatagram(sub));
                    %                 texteTypeDatagram
                end
            end
        end
    end
end
