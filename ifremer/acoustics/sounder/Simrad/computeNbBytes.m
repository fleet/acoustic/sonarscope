function [flag, NbBytes, NbEntries] = computeNbBytes(iDatagram, paquet, Datagrams, Data, NumSounder)
% Calcul du nombre d'octet dans le datagram courant.

flag = 0;

NbSamples = zeros(1, 10);
NbEntries = [];
if isfield(Datagrams, 'Tables')
    % Recherche des index de tables pour conna�tre le nb d'�chantillons de
    % chaque s�rie.
    pppp = {Datagrams.Tables.Index};    
    [B, M, N] = unique(pppp, 'first');
    
    for b=1:size(M,2)
        %%%% indexlist   = char(B(N(M(b))));
        indexlist   = B{b};
        index       = textscan(indexlist, '%s', 'delimiter', ',');
        for idx=1:numel(index{1})
            indexName = char(index{1}(idx));
            % Pour le datagramme SoundSpeedProfile.
            if strcmp(indexName, 'nbPoints')
                indexName = 'NbEntries';
            end    
            % NbDatagrams pour la majorit� des datagrammes.
            % nbProfiles pour les paquets du type SoundSpeedProfile 
            if ~strcmp(indexName, 'NbDatagrams') && ~strcmp(indexName, 'nbProfiles') && ...
               ~strcmp(indexName, 'nbPings') && ~strcmp(indexName, 'NumberOfSamples') && ...
               ~strcmp(indexName, 'nbSounders')
                    
                if isfield(Data,indexName)
                    if strcmp(indexName, 'nbBeams')
                        NbSamples(b)  = double(Data.NbBeams(iDatagram, NumSounder));
                    else
                        NbSamples(b)  = double(Data.(indexName)(iDatagram, NumSounder));
                    end
                elseif isfield(Data.Dimensions,indexName)
                    if strcmp(indexName, 'nbBeams') && isfield(Data, 'NbBeams')
                        % Cas du paquet Depth.
                        NbSamples(b)  = double(Data.NbBeams(iDatagram, NumSounder));
                    elseif strcmp(Datagrams.Title, 'RawBeamSamples') && strcmp(indexName, 'nbRx')
                        % Cas du paquet RawRangeBeamAngle.
                        NbSamples(b)  = double(Data.NRx(iDatagram, NumSounder));
                    elseif strcmp(Datagrams.Title, 'WaterColumn') && strcmp(indexName, 'nbRx')
                        NbSamples(b)  = double(Data.NRx(iDatagram, NumSounder));
                    else
                        NbSamples(b)  = double(Data.Dimensions.(indexName));
                    end
                end
            end
        end
    end % Fin de la boucle sur la liste des index diff�rents.
    % Les index sont tri�s par ordre alphanum�rique et peuvent �tre
    % invers�s.
    if size(M,2) > 1
        if M(1) > M(2)
            NbSamples   = flipud(nonzeros(NbSamples));
            M           = fliplr(M);
        end
    end
elseif isfield(Datagrams.Dimensions, 'NbDatagrams')
    NbSamples(1) = double(Datagrams.Dimensions.NbDatagrams);
else
    NbSamples(1) = double(Datagrams.Dimensions.NbSamples);
end


if size(NbSamples,1) > 1
    NbEntries.NbSamples = NbSamples;
    NbEntries.IndexVar  = M;
    [~, m, ~]           = unique(N);
    m                   = fliplr(m);
    NbEntries.NbVar     = [m(1),diff(m)];
    NbBytes = paquet.headersize + paquet.subsize(1) * NbEntries.NbSamples(1) + paquet.subsize(2) * NbEntries.NbSamples(2);
else
    NbEntries = NbSamples;
    NbBytes = paquet.headersize + paquet.subsize(1) * NbSamples(1) + paquet.subsize(2) * NbSamples(2);
end
 

