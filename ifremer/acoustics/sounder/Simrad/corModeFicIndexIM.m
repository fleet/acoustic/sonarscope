% Lecture d'un fichier d'index ancien d'ARCHIVE
%
% Syntax
%   [Date, Heure, Taille, Position, entete] = corModeFicIndexIM(nomFic)
%
% Input Arguments
%   nomFic	  : Nom du fichier d'index
%
% Output Arguments
%  []        : Auto-plot activation
%   Date	 : Date des enregistrements
%   Heure	 : Heure des enregistrements
%   Taille	 : Taille des enregistrements
%   Position : Position des enregistrements
%   entete   : entete du fichier
%
% Examples
%   nomFicIndexIM = 'Pas de fichier en archive pour le moment'
%   [Date, Heure, Taille, Position, entete, Mode, Canal] = lecFicIndexIM(nomFicIndexIM);
%
%   subplot(6,1,1); plot(Date); grid on; title('Date');
%   subplot(6,1,2); plot(Heure); grid on; title('Heure');
%   subplot(6,1,3); plot(Taille); grid on; title('Taille');
%   subplot(6,1,4); plot(Position); grid on; title('Position');
%   subplot(6,1,5); plot(Mode); grid on; title('Mode');
%   subplot(6,1,6); plot(Canal); grid on; title('Canal');
%
%   Mode(:) = 1;
%   corModeFicIndexIM(nomFicIndexIM, Mode);
%
% See also corModeFicIndexIM Authors
% Authors : JMA
%-----------------------------------------------------------------------

function corModeFicIndexIM(nomFic, Mode)

% ---------------------------
% Decodage du fichier d'index

fid = fopen(nomFic, 'r+', 'b');
fseek(fid, 76, 'bof');
pppp = fread(fid, [20 inf], 'uchar');
pppp(18,:) = Mode;
fseek(fid, 76, 'bof');
fwrite(fid, pppp, 'uchar');
fclose(fid);

