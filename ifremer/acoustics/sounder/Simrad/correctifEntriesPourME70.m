function Entries = correctifEntriesPourME70(Entries, DataImage)

if DataImage.EmModel ~= 850
    return
end

nbSamplesEntries = numel(Entries);
nbSamplesReels = sum(DataImage.InfoBeamNbSamples(:));
if nbSamplesEntries == (2*nbSamplesReels)
    Temp = zeros(nbSamplesReels,1, class(Entries));
    kFin1 = 0;
    kFin2 = 0;
    for k=1:DataImage.Dimensions.nbPings
        nbSamplesPing = sum(DataImage.InfoBeamNbSamples(k,:));
        kDeb1 = kFin1 + 1;
        kFin1 = kFin1 + nbSamplesPing;
        kDeb2 = kFin2 + 1;
        kFin2 = kFin2 + nbSamplesPing;
        Temp(kDeb1:kFin1) = Entries(kDeb2:kFin2);
        kFin2 = kFin2 + nbSamplesPing;
    end
    Entries = Temp;
end

