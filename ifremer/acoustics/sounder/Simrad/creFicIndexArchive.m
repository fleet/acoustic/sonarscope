% Creation d'un fichier d'index d'un fichier ARCHIVE
%
% Syntax
%   creFicIndexArchive(nomFic, nomFicIndex, typeDatagram)
%
% Input Arguments
%   nomFic       : Nom du fichier de donnees.
%   nomFicIndex  : Nom du fichier d'index a creer
%   typeDatagram : Type de datagram recherche : 0=tous, 68=Depth, 70=Raw range and beam angle
%                   83=Seabed image, 75=Central beams echogram, 80=Position, 104=Height,
%                   84=Tide, 65=Attitude, 72=Heading, 74=Mechanical transducer tilt, 67=Clock,
%                   85=Sound speed profile, 87=SSP Output, 69=Single beam echo sounder depth
%                   82=Runtime parameter
%
% Examples
%   nomFic = 'Pas de fichier en archive pour le moment';
%   nomFicIndex = '';
%
%   creFicIndexArchive(nomFic, nomFicIndex, 65);
%
%   [Date, Heure, Taille, Position, entete] = lecFicIndex(nomFicIndex);
%   subplot(4,1,1); plot(Date); grid on; title('Date');
%   subplot(4,1,2); plot(Heure); grid on; title('Heure');
%   subplot(4,1,3); plot(Taille); grid on; title('Taille');
%   subplot(4,1,4); plot(Position); grid on; title('Position');
%
% See also lecFicIndexArchive Authors
% Authors : JMA
% VERSION  : $Id: creFicIndexArchive.m,v 1.5 2002/06/20 11:46:39 augustin Exp $
%-----------------------------------------------------------------------

function creFicIndexArchive(nomFic, nomFicIndex, typeDatagram)

% -------------------------------
% Lecture sequentielle du fichier

fid = fopen(nomFic, 'r', 'b');

ienr = 1;
PositionCourante = 0;
while 1
    NumberOfBytesInDatagram = fread(fid, 1, 'int32');
    if feof(fid)
        break;
    end
    
    STX = fread(fid, 1, 'uchar'); %#ok
    TypeOfDatagram = fread(fid, 1, 'uchar');
    EmModel = fread(fid, 1, 'uint16'); %#ok
    
    SimradDate = fread(fid, 1, 'uint32');
    Year = floor(SimradDate / 10000);
    SimradDate = SimradDate - Year * 10000;
    Month = floor(SimradDate / 100);
    Day = SimradDate - Month * 100;
    
    HeureSimrad = fread(fid, 1, 'uint32');
    
    
    if(TypeOfDatagram == typeDatagram || typeDatagram == 0)
        Date(ienr) = dayJma2Ifr(Day, Month, Year);	 %#ok
        Heure(ienr) = HeureSimrad; %#ok
        Taille(ienr) = NumberOfBytesInDatagram; %#ok
        Position(ienr) = PositionCourante; %#ok
        
        %disp(sprintf('Packet %d - %s  %s - Type %d', ...
        %ienr, dayIfr2str(Date(ienr)), hourIfr2str(Heure(ienr)), TypeOfDatagram));
        ienr = ienr+1;
    end
    
    PositionSuivante = PositionCourante + NumberOfBytesInDatagram + 4;
    fseek(fid, PositionSuivante, 'bof');
    PositionCourante = PositionSuivante ;
end
fclose(fid);

n = length(Date);
pppp = zeros(4, n);
pppp(1,:) = Date;
pppp(2,:) = Heure;
pppp(3,:) = Position;
pppp(4,:) = Taille;

entete = repmat(' ', [1 76]);
fid = fopen(nomFicIndex, 'w+', 'b');
fwrite(fid, entete, 'char')
fwrite(fid, pppp, 'uint32');
fclose(fid);

fprintf('Packet   1 - %s  %s\n', dayIfr2str(Date(1)), hourIfr2str(Heure(1)));
