% nomDir = 'I:\MARMESONET\Leg2\SonarScope'
% createEmptyAllFile(nomDir)

function createEmptyAllFile(nomDir)

listeDir = listeDirOnDir(nomDir);
nomDir = fileparts(nomDir);
for k=1:length(listeDir)
    nomFic = fullfile(nomDir, [listeDir(k).name '.all']);
    if ~exist(nomFic, 'file')
        fid = fopen(nomFic, 'w+');
        fprintf(fid, 'SonarScope Repair');
        fclose(fid);
    end
end
