function flag = create_EM304EmptyBSCorrFile

nomFic = getNomFicDatabase('BSCorr_304.txt');
[flag, BSCorr] = read_BSCorr(nomFic);
if ~flag
    return
end

for k1=1:length(BSCorr)
    for k2=1:length(BSCorr(k1).Sector)
        BSCorr(k1).Sector(k2).Node(:,2) = 0;
    end
end

pathname = fileparts(nomFic);
nomFicOut = fullfile(pathname, 'BSCorr_304_EmptyValues.txt');
flag = write_BSCorr_Spline(BSCorr, nomFicOut, 'EM304');


