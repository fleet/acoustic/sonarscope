function isDual = defineIsDual(DataInstallationParameters)

switch DataInstallationParameters.EmModel
    case {300; 710; 712; 302; 122; 1002; 120; 2000; 304}
        isDual = 0;

    case 2040
        if isfield(DataInstallationParameters, 'Line')
            if ~isfield(DataInstallationParameters.Line, 'S3R') || isempty(DataInstallationParameters.Line.S3R)
                isDual = 0;
            else
                isDual = 1;
            end
        else
            if ~isfield(DataInstallationParameters, 'S3R') || isempty(DataInstallationParameters.S3R)
                isDual = 0;
            else
                isDual = 1;
            end
        end
        
    case 2045 % 2040c
        % Test pour connaître l'existence en mode Single ou Dual en se
        % basant sur l'existence de R2S (on pourrait éventuellement se baser sur SystemSerialNumber et
        % SystemSerialNumberSecondHead).
        if isfield(DataInstallationParameters, 'S2R') && ~isempty(DataInstallationParameters.S2R) && (DataInstallationParameters.S2R ~= 0)
            if isfield(DataInstallationParameters, 'S1R') && ~isempty(DataInstallationParameters.S1R)
                if DataInstallationParameters.S2R ~= DataInstallationParameters.S1R
                    isDual = 1;
                end
            elseif isfield(DataInstallationParameters, 'S3R') && ~isempty(DataInstallationParameters.S3R)
                    isDual = 1;
            else
                isDual = 0; % Etrange cas pas encore rencontré.
            end
        else
            isDual = 0;
        end
        
    case 3002
        if isfield(DataInstallationParameters, 'S2R') && ~isempty(DataInstallationParameters.S2R) && (DataInstallationParameters.S2R ~= 0)
            isDual = 1;
        else
            isDual = 0;
        end
    case 3020
        if isfield(DataInstallationParameters, 'S2R') && ~isempty(DataInstallationParameters.S2R) && (DataInstallationParameters.S2R ~= 0)
            isDual = 1;
        else
            isDual = 0;
        end
        
    case 850 % ME70
        isDual = 0;
        
    otherwise
        if ~isdeployed
            str = sprintf('read_installationParameters_bin : Model=%d : completer le switch', DataInstallationParameters.EmModel);
            my_warndlg(str, 1);
        end
        if isfield(DataInstallationParameters, 'S2R') && ~isempty(DataInstallationParameters.S2R) && (DataInstallationParameters.S2R ~= 0)
            isDual = 1;
        else
            isDual = 0;
        end
end
