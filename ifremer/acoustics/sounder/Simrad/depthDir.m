function listeDir = depthDir(nomDir, varargin)

[varargin, isMatlabPath] = getPropertyValue(varargin, 'isMatlabPath', 0);

% TODO : danger pour la maintenance SSc :
%{
p = genpath(folderName) returns a path name that includes folderName and multiple levels of subfolders
below folderName. The path name does not include folders named private, folders that begin with the
@ character (class folders), folders that begin with the + character (package folders), or subfolders
within any of these.
%}

if ~isMatlabPath
    % http://stackoverflow.com/questions/8748976/list-the-subfolders-in-a-folder-matlab-only-subfolders-not-files
    listeDir = regexp(genpath(nomDir), '[^;]*', 'match');
    return
end

X = [];

liste1 = listeDirOnDir(nomDir);
X = [X; liste1(:)];
for k1=1:length(liste1)
    liste2 = listeDirOnDir(fullfile(liste1(k1).folder, liste1(k1).name));
    X = [X; liste2(:)]; %#ok<AGROW>
    for k2=1:length(liste2)
        liste3 = listeDirOnDir(fullfile(liste2(k2).folder, liste2(k2).name));
        X = [X; liste3(:)]; %#ok<AGROW>
        for k3=1:length(liste3)
            liste4 = listeDirOnDir(fullfile(liste3(k3).folder, liste3(k3).name));
            X = [X; liste4(:)]; %#ok<AGROW>
            for k4=1:length(liste4)
                liste5 = listeDirOnDir(fullfile(liste4(k4).folder, liste4(k4).name));
                X = [X; liste5(:)]; %#ok<AGROW>
                for k5=1:length(liste5)
                    liste6 = listeDirOnDir(fullfile(liste5(k5).folder, liste5(k5).name));
                    X = [X; liste6(:)]; %#ok<AGROW>
                    for k6=1:length(liste6)
                        liste7 = listeDirOnDir(fullfile(liste6(k6).folder, liste6(k6).name));
                        X = [X; liste7(:)]; %#ok<AGROW>
                        for k7=1:length(liste7)
                            liste8 = listeDirOnDir(fullfile(liste7(k7).folder, liste7(k7).name));
                            X = [X; liste8(:)]; %#ok<AGROW>
                            for k8=1:length(liste8)
                                liste9 = listeDirOnDir(fullfile(liste8(k8).folder, liste8(k8).name));
                                X = [X; liste9(:)]; %#ok<AGROW>
                            end
                        end
                    end
                end
            end
        end
    end
end

listeDir = {};
for k=1:length(X)
    listeDir{end+1,1} = fullfile(X(k).folder, X(k).name); %#ok<AGROW>
end

% Avant mais on loupait des répertoires !!!

%     noDir = 1;
%     liste = listeDirOnDir(nomDir);
%     for k=1:length(liste)
%         listeDir{k} = fullfile(nomDir, liste(k).name); %#ok<AGROW>
%
%         if strcmp(listeDir{k}, 'D:\SScTbx\ifremer\matlab\graphics\applications')
%             pi
%         end
%
%         if isdir(listeDir{k})
%             listeDir2 = depthDir(listeDir{k}, 'isMatlabPath', 1);
%             listeDir = [listeDir listeDir2]; %#ok<AGROW>
%             noDir = 0;
%         end
%     end
%     if noDir
%         listeDir = [];
%     end



