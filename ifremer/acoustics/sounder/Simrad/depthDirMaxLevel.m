function listeDir = depthDirMaxLevel(MaxLevel, Level, varargin)

[varargin, nomDirFinal] = getPropertyValue(varargin, 'nomDirFinal', []);

listeDir = {};
if isempty(varargin)
    [flag, nomDir] = my_uigetdir(pwd, 'Pick a Directory');
    if ~flag
        return
    end
else
    nomDir = varargin{1};
end

liste = listeDirOnDir(nomDir);
for i=1:length(liste)
    Skip = 0;
    for k=1:length(nomDirFinal)
        switch nomDirFinal{k}
            case 'SonarScope'
                if strcmp(liste(i).name, 'SonarScope')
                    Skip = 1;
                end
            case '.xml'
                nomFicXml = fullfile(nomDir, [liste(i).name '.xml']);
                if exist(nomFicXml, 'file')
                    Skip = 1;
                end
            case '.ers'
                nomFicXml = fullfile(nomDir, [liste(i).name '.ers']);
                if exist(nomFicXml, 'file')
                    Skip = 1;
                end
            otherwise
                'beurk'
        end
    end
    if Skip
        continue
    end
    
    %     if ~isempty(nomDirFinal) && strcmp(liste(i).name, nomDirFinal)
    %         continue
    %     end
    
    CurrentDir = fullfile(nomDir, liste(i).name);
    listeDir{end+1} = CurrentDir; %#ok<AGROW>
    if isfolder(CurrentDir)
        Level = Level + 1;
        if Level <= MaxLevel
            listeDir2 = depthDirMaxLevel(MaxLevel, Level, CurrentDir, 'nomDirFinal', nomDirFinal);
            Level = Level - 1;
            listeDir = [listeDir listeDir2]; %#ok<AGROW>
        end
    end
end
