% Angles des faisceaux de reception d'un sondeur Simrad
%
% Syntax
%   angles = getAnglesIsoDistance( LBEAM, FBEAM, NBEAMS )
%
% Input Arguments
%   LBEAM	: Angle mini (deg)
%   FBEAM	: Angle maxi (deg)
%   NBEAMS	: Nombre de faisceaux
%
% Output Arguments
%   angles : les angles des faisceaux de reception de l'EM300
%
% Examples
%   anglesEM12D_150 = getAnglesIsoDistance( 81, -2.6875, 75 );
%   plot(anglesEM12D_150, '+'); grid on
%
% See also getAnglesRecSimrad Authors
% Authors : JMA + XL
%-----------------------------------------------------------------------

function angles = getAnglesIsoDistance( LBEAM, FBEAM, NBEAMS )

DTOR = pi / 180;

A = sin(FBEAM * DTOR);
B = cos(FBEAM * DTOR);
D1 = A / B;

A = sin(LBEAM * DTOR);
B = cos(LBEAM * DTOR);
D2 = A / B;

D = D1 - D2;
DD = D / (NBEAMS-1);

angles = fliplr(round(16 / DTOR * atan(D1 - DD * (0:(NBEAMS-1)))) / 16);
