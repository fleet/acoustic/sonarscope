% Angles des faisceaux de reception d'un sondeur Simrad
%
% Syntax
%   angles = getAnglesRecSimrad(iModeRecOuv, ouvertureAngulaire, iModeRecSpacing, n)
%
% Input Arguments 
%   iModeRecOuv        : 1='Fixed Coverage', 2= 'Maximum Auto Beam Angle', 3='Maximum Auto Swath Width'
%   ouvertureAngulaire : Angle d'ouverture (deg)
%   iModeRecSpacing    : 1='Equi Distant', 2='Equi Angle', 3='Optimized for Imagery'
%   n	               : Nombre de faisceaux
%
% Output Arguments
%   []     : Auto-plot activation
%   angles : les angles des faisceaux de reception de l'EM300
%
% Examples
%    getAnglesRecSimrad(1, 70, 1, 135);
%    getAnglesRecSimrad(1, 70, 2, 135);
%    getAnglesRecSimrad(1, 70, 3, 135);
%
%   angles = getAnglesRecSimrad(1, 70, 1, 135);
%     figure(1); plot(angles, '+'); grid on;
%     figure(2); plot(gradient(angles), '+'); grid on;
%
% See also GetAnglesFaisceauxMBES Authors
% Authors : JMA + XL
%-----------------------------------------------------------------------

function varargout = getAnglesRecSimrad(iModeRecOuv, ouvertureAngulaire, iModeRecSpacing, n)

switch iModeRecOuv
    case 1 % Fixed Coverage
        switch iModeRecSpacing
            case 1
                % Equi Distant
                angles = equiDistant(-ouvertureAngulaire, ouvertureAngulaire, n);
            case 2
                % Equi Angle
                angles = equiAngle(-ouvertureAngulaire, ouvertureAngulaire, n);
            case 3
                % Optimized for Imagery
                nbFaisRestant = 1 + (n - 41) / 2;
%                 angles = [equiDistant(-ouvertureAngulaire, -30, nbFaisRestant) equiAngle(-28.5, 28.5, 39) equiDistant(30, ouvertureAngulaire, nbFaisRestant)];
                angles = inBetween(ouvertureAngulaire, n);
            otherwise
                disp('Mode d''espacement non reconnu');
                angles = [];
        end

    case 2 % Maximum Auto Beam Angle
        my_warndlg('Mode de couverture non encore programme dans getAnglesRecSimrad', 0);
        angles = [];

    case 3 % Maximum Auto Swath Width
        my_warndlg('Mode de couverture non encore programme dans getAnglesRecSimrad', 0);
        angles = [];

    otherwise
        my_warndlg('Mode de couverture non reconnu', 0);
        angles = [];
end

% -----------------------------------------
% Sortie des parametre ou traces graphiques

if nargout == 0
    figure;
    subplot(2, 1, 1);
    plot(angles, '+'); grid on;
    xlabel('Nu Faisceaux'); ylabel('angles (deg)'); title('Angles des faisceaux');
    subplot(2, 1, 2);
    plot(gradient(angles), '+'); grid on;
    xlabel('Nu Faisceaux'); ylabel('Gradient (deg)'); title('Gradient des angles des faisceaux');
else
    varargout{1} = angles;
end






% ----------------------------------------------------------------------------
% ----------------------------------------------------------------------------
function a = equiDistant(angleDeb, angleFin, n)

RTOD = 180 / pi;
DTOR = pi / 180;

A = sin(angleFin * DTOR);
B = cos(angleFin * DTOR);
D1 = A / B;

A = sin(angleDeb * DTOR);
B = cos(angleDeb * DTOR);
D2 = A / B;

D = D1 - D2;
DD = D / (n-1);

a = fliplr(RTOD * atan(D1 - DD * (0:(n-1))));


% ----------------------------------------------------------------------------
% ----------------------------------------------------------------------------
function a = equiAngle(angleDeb, angleFin, n)

pas = (angleFin - angleDeb) / (n - 1);
a = angleDeb:pas:angleFin;

% ----------------------------------------------------------------------------
% ----------------------------------------------------------------------------
function angles = inBetween(angleFin, n)

n = floor(n / 2);
i = 0:n;
b = 1.5;


a = 2 * ((angleFin / n) - b) / (n - 1);
angles = (i .* (i-1) / 2) * a + i * b;
angles = [fliplr(-angles(2:n+1)) angles];
