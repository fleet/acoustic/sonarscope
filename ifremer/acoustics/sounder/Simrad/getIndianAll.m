function typeIndian = getIndianAll(nomFic)

fid = fopen(nomFic, 'r', 'l');
if fid == -1
    str = ['Impossible d''ouvrir le fichier ' nomFic];
    my_warndlg(str, 1);
    typeIndian = [];
    return
end
NumberOfBytesInDatagram_l = fread(fid, 1, 'int32');
fclose(fid);

fid = fopen(nomFic, 'r', 'b');
if fid == -1
    str = ['Impossible d''ouvrir le fichier ' nomFic];
    my_warndlg(str, 1);
    typeIndian = [];
    return
end
NumberOfBytesInDatagram_b = fread(fid, 1, 'int32');
fclose(fid);

if abs(NumberOfBytesInDatagram_l) < abs(NumberOfBytesInDatagram_b)
    typeIndian = 'l';
else
    typeIndian = 'b';
end
