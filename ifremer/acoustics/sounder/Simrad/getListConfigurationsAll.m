function listeConfigs = getListConfigurationsAll(SurveyEmModel, listeNomFic, nbPingsPerMode1, Mode2IfEM2040, Mode3IfEM2040, nbPingsPerSwath, nbPingsPerWaveForm)

minPingsPerConfiguration = 10;

switch SurveyEmModel{1} % TODO : généraliser : faire deux listes de fichiers et lancer les 2 fonctions et concaténer
    case {2040; 2045}
        listeConfigs =  createFileListsPerConfigEm2040(SurveyEmModel, listeNomFic, ...
            nbPingsPerMode1, nbPingsPerSwath, Mode2IfEM2040, Mode3IfEM2040, ...
            minPingsPerConfiguration);
    otherwise
        listeConfigs = createFileListsPerConfig(listeNomFic, ...
            nbPingsPerMode1, nbPingsPerSwath, nbPingsPerWaveForm, ...
            minPingsPerConfiguration);
end


function listeConfigs = createFileListsPerConfig(listeNomFic, ...
    nbPingsPerMode1, nbPingsPerSwath, nbPingsPerWaveForm, ...
    minPingsPerConfiguration)

ModeUnique = find(any(nbPingsPerMode1,2));

NbSwathsUnique = [];
if any(nbPingsPerSwath(1,:))
    NbSwathsUnique = [NbSwathsUnique 1];
end
if any(nbPingsPerSwath(2,:))
    NbSwathsUnique = [NbSwathsUnique 2];
end

PresenceFMUnique = [];
if any(nbPingsPerWaveForm(1,:))
    PresenceFMUnique = [PresenceFMUnique 1];
end
if any(nbPingsPerWaveForm(2,:))
    PresenceFMUnique = [PresenceFMUnique 2];
end

kConfig = 0;

for k1=1:length(ModeUnique)
    for kSwath=1:length(NbSwathsUnique)
        for k3=1:length(PresenceFMUnique)
            switch PresenceFMUnique(k3)
                case 1 % CW
                    subFilesCWFMOK = (nbPingsPerWaveForm(1,:) > minPingsPerConfiguration);
                case 2 % FM
                    subFilesCWFMOK = (nbPingsPerWaveForm(2,:) > minPingsPerConfiguration);
            end
            
%             subFiles = find((nbPingsPerMode1(ModeUnique(k1,:),:) > minPingsPerConfiguration) ...
%                 & (nbPingsPerSwath(kSwath,:) > minPingsPerConfiguration) ...
%                 & subFilesCWFMOK);
                        
% Modif JMA le 30/01/20120
            subFiles = find((nbPingsPerMode1(ModeUnique(k1,:),:) > minPingsPerConfiguration) ...
                & (nbPingsPerSwath(NbSwathsUnique(kSwath),:) > minPingsPerConfiguration) ...
                & subFilesCWFMOK);

            if ~isempty(subFiles)
                nbPingMaxForThisConfiguration = min(nbPingsPerMode1(ModeUnique(k1,:),:), nbPingsPerSwath(kSwath,:));
                % Remarque : ce nombre n'est pas le vrai, il faudrait pour cela conserver la fonfig pour chacun des pings
                
                if NbSwathsUnique(kSwath) == 1
                    CodeSwath = 'SingleSwath';
                else
                    CodeSwath = 'DoubleSwath';
                end
                if PresenceFMUnique(k3) == 1
                    CodeCWFM = 'CW';
                else
                    CodeCWFM = 'FM';
                end
                nomConf = sprintf('Mode%d_%s_%s', ModeUnique(k1), CodeSwath, CodeCWFM);
                nomConf = strrep(nomConf, ' ', '');

                kConfig = kConfig + 1;
                listeConfigs(kConfig).Mode.Value  = ModeUnique(k1); %#ok<AGROW>
                listeConfigs(kConfig).Mode.str    = 'TODO'; %#ok<AGROW>
                listeConfigs(kConfig).Swath.Value = NbSwathsUnique(kSwath); %#ok<AGROW>
                listeConfigs(kConfig).Swath.str   = CodeSwath; %#ok<AGROW>
                listeConfigs(kConfig).CWFM.Value  = PresenceFMUnique(k3); %#ok<AGROW>
                listeConfigs(kConfig).CWFM.str    = CodeCWFM; %#ok<AGROW>
                listeConfigs(kConfig).Files.Conf  = nomConf; %#ok<AGROW>
                listeConfigs(kConfig).Files.index = subFiles; %#ok<AGROW>
                listeConfigs(kConfig).Files.str   = listeNomFic(subFiles); %#ok<AGROW>
                listeConfigs(kConfig).Files.nbPingsAtBest = nbPingMaxForThisConfiguration(subFiles); %#ok<AGROW>
                listeConfigs(kConfig).Files.isEM2040      = 0; %#ok<AGROW>
            end
        end
    end
end
if kConfig == 0
    listeConfigs = [];
end


function listeConfigs = createFileListsPerConfigEm2040(SurveyEmModel, listeNomFic, ...
    nbPingsPerMode1, nbPingsPerSwath, Mode2IfEM2040, Mode3IfEM2040, ...
    minPingsPerConfiguration)

ModeUnique = find(any(nbPingsPerMode1,2));

Mode2IfEM2040Unique = [];
if any(Mode2IfEM2040)
    Mode2IfEM2040Unique = unique([Mode2IfEM2040Unique Mode2IfEM2040]);
end

NbSwathsUnique = [];
if any(nbPingsPerSwath(1,:))
    NbSwathsUnique = [NbSwathsUnique 1];
end
if any(nbPingsPerSwath(2,:))
    NbSwathsUnique = [NbSwathsUnique 2];
end

Mode3IfEM2040Unique = [];
if any(Mode3IfEM2040)
    Mode3IfEM2040Unique = unique([Mode3IfEM2040Unique Mode3IfEM2040]);
end

switch SurveyEmModel{1} % TODO : généraliser
    case 2040
        strMode1IfEM2040 = {'200kHz'; '300kHz'; '400kHz'};
        strMode2IfEM2040 = {'Normal'; 'SingleSector'; 'Scanning'};
        strMode3IfEM2040 = {'S'; 'M'; 'L'; 'FM'};
    case 2045
        for k=32:-1:1
            strMode1IfEM2040{k} = sprintf('%d kHz', 180 + (k-1) * 10);
        end
        strMode2IfEM2040 = {'CW'; 'FM'};
        strMode3IfEM2040 = {'Very Short CW'; 'Short CW'; 'Medium CW'; 'Long CW'; 'Very Long CW'; 'Extra Long CW'; 'Short FM'; 'Long FM'};
    otherwise
end

kConfig = 0;
for k1=1:length(ModeUnique) % 200 kHz, 300 kHz, 400 kHz
    subFilesModeOK = (nbPingsPerMode1(ModeUnique(k1),:) > minPingsPerConfiguration);
    if all(subFilesModeOK == 0)
        continue
    end
    CodeMode1 = strMode1IfEM2040{ModeUnique(k1)};
    
    for kSwath=1:length(NbSwathsUnique) % Single Swath, Dual Swath
        switch NbSwathsUnique(kSwath)
            case 1 % Single swath
                subFilesSwathOK = (nbPingsPerSwath(1,:) > minPingsPerConfiguration);
            case 2 % Double swath
                subFilesSwathOK = (nbPingsPerSwath(2,:) > minPingsPerConfiguration);
        end
        if NbSwathsUnique(kSwath) == 1
            CodeSwath = 'SingleSwath';
        else
            CodeSwath = 'DoubleSwath';
        end
        
        for k2=1:length(Mode2IfEM2040Unique) % Normal, Single Sector, Scanning
            codeMode2 = strMode2IfEM2040{Mode2IfEM2040Unique(k2)};
            subFilesMode2OK = (Mode2IfEM2040 == Mode2IfEM2040Unique(k2));
            if all(subFilesMode2OK == 0)
                continue
            end
           
            for k3=1:length(Mode3IfEM2040Unique) % S, M, L, FM
                subFilesMode3OK = (Mode3IfEM2040 == Mode3IfEM2040Unique(k3));
                CodeMode3 = strMode3IfEM2040{Mode3IfEM2040Unique(k3)}; % FAUX
                
                subFiles = find(subFilesModeOK & subFilesMode2OK & subFilesSwathOK & subFilesMode3OK);
                if ~isempty(subFiles)
                    nbPingMaxForThisConfiguration = min(nbPingsPerMode1(ModeUnique(k1,:),:), nbPingsPerSwath(kSwath,:));
                    % Remarque : ce nombre n'est pas le vrai, il faudrait pour cela conserver la fonfig pour chacun des pings
                    
                    nomConf = sprintf('%s_%s_%s_%s', CodeMode1, CodeSwath, codeMode2, CodeMode3);
                    nomConf = strrep(nomConf, ' ', '');
                    
                    kConfig = kConfig + 1;
                    listeConfigs(kConfig).Mode.Value  = ModeUnique(k1); %#ok<AGROW>
                    listeConfigs(kConfig).Mode.str    = CodeMode1; %#ok<AGROW>
                    listeConfigs(kConfig).Swath.Value = NbSwathsUnique(kSwath); %#ok<AGROW>
                    listeConfigs(kConfig).Swath.str   = CodeSwath; %#ok<AGROW>
%                     listeConfigs(kConfig).CWFM.Value  = []; %PresenceFMUnique(k3); %#ok<AGROW>
%                     listeConfigs(kConfig).CWFM.str    = []; %CodeCWFM; %#ok<AGROW>
                    listeConfigs(kConfig).Mode3.Value  = Mode2IfEM2040Unique(k2); %#ok<AGROW>
                    listeConfigs(kConfig).Mode2.str    = codeMode2; %#ok<AGROW>
                    listeConfigs(kConfig).Mode2.Value  = Mode3IfEM2040Unique(k3); %#ok<AGROW>
                    listeConfigs(kConfig).Mode3.str    = CodeMode3; %#ok<AGROW>
                    listeConfigs(kConfig).Files.Conf  = nomConf; %#ok<AGROW>
                    listeConfigs(kConfig).Files.index = subFiles; %#ok<AGROW>
                    listeConfigs(kConfig).Files.str   = listeNomFic(subFiles); %#ok<AGROW>
                    listeConfigs(kConfig).Files.nbPingsAtBest = nbPingMaxForThisConfiguration(subFiles); %#ok<AGROW>
                    listeConfigs(kConfig).Files.isEM2040 = 1; %#ok<AGROW>
                end
            end
        end
    end
end
if kConfig == 0
    listeConfigs = [];
end
