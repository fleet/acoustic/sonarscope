% TODO : transformer cette fonction en m�thode de cl_simrad_all
function [listeLayersSupplementaires, dataTypeCor, flag] = getListeLayersSupplementaires(DataTypeConditions, varargin)

[varargin, Names] = getPropertyValue(varargin, 'Names', []); %#ok<ASGLU>

listeLayersSupplementaires = [];
dataTypeCor = [];
flag        = 1;

if isempty(DataTypeConditions)
    return
end

for k=1:length(DataTypeConditions)
    if strcmp(DataTypeConditions{k}, 'X') || strcmp(DataTypeConditions{k}, 'Y')
    else
        dataTypeCor(end+1) = cl_image.indDataType(DataTypeConditions{k}); %#ok<AGROW>
        switch DataTypeConditions{k}
            case 'Reflectivity' % TODO : Possibilit� de confondre ReflectivityFromSnippets et Reflectivity
                listeLayersSupplementaires(end+1) = 27; %#ok<AGROW>
            case 'BeamDepressionAngle'
                %                 listeLayersSupplementaires(end+1) = 4; %#ok<AGROW>
                
                %---------------------------------------------------------------------------------------------
                listeLayersSupplementaires(end+1) = 14; %#ok<AGROW> % Modif JUMA le 22/03/2016
                %---------------------------------------------------------------------------------------------
                
                
                
            case {'TxAngle'; 'Raw-BeamPointingAngle'; 'RxBeamAngle'; 'BeamPointingAngle'}
%                 listeLayersSupplementaires(end+1) = 14; %#ok<AGROW>
                listeLayersSupplementaires(end+1) = 4; %#ok<AGROW> % Modif JMA le 02/01/2019 : attension, peut-�tre effet de vord .all V2 ?
            case 'RxAngleEarth'
                listeLayersSupplementaires(end+1) = 36; %#ok<AGROW>
            case 'TxBeamIndex'
                listeLayersSupplementaires(end+1) = 26; %#ok<AGROW>
            case 'TxBeamIndexSwath'
                listeLayersSupplementaires(end+1) = 28; %#ok<AGROW>
            case 'DetectionType'
                listeLayersSupplementaires(end+1) = 10; %#ok<AGROW>
            case 'IncidenceAngle'
                listeLayersSupplementaires(end+1) = 43; %#ok<AGROW>
                
                % Ajout�s le 27/02/2017
            case 'AcrossDist'
                listeLayersSupplementaires(end+1) = 2; %#ok<AGROW>
            case 'AlongDist'
                listeLayersSupplementaires(end+1) = 3; %#ok<AGROW>
            case {'Bathymetry'; 'Depth+Draught-Heave'}
                listeLayersSupplementaires(end+1) = 24; %#ok<AGROW>
            case 'TwoWayTravelTimeInSeconds'
                listeLayersSupplementaires(end+1) = 25; %#ok<AGROW>
            case 'AbsorptionCoefficientRT'
                listeLayersSupplementaires(end+1) = 40; %#ok<AGROW>
            case 'AbsorptionCoefficientSSc'
                listeLayersSupplementaires(end+1) = 41; %#ok<AGROW>
            case 'AbsorptionCoefficientUser'
                listeLayersSupplementaires(end+1) = 42; %#ok<AGROW>
            case 'AbsorptionCoeff'
                if isempty(Names)
                    str1 = 'getListeLayersSupplementaires : Dev error : "Names" non d�fini pour s�lectionner "AbsorptionCoeff"';
                    str2 = 'getListeLayersSupplementaires : Dev error : "Names" needed to define "AbsorptionCoeff" type.';
                    my_warndlg(Lang(str1,str2), 1);
                    flag = 0;
                    return
                end
                if ~isempty(strfind(Names{k}, '_RT_'))
                    listeLayersSupplementaires(end+1) = 40; %#ok<AGROW>
                elseif ~isempty(strfind(Names{k}, '_SSc_'))
                    listeLayersSupplementaires(end+1) = 41; %#ok<AGROW>
                elseif ~isempty(strfind(Names{k}, '_User_'))
                    listeLayersSupplementaires(end+1) = 42; %#ok<AGROW>
                else
                    str1 = 'getListeLayersSupplementaires : Dev error : "Names" erron� pour s�lectionner "AbsorptionCoeff"';
                    str2 = 'getListeLayersSupplementaires : Dev error : "Names" not correct to define "AbsorptionCoeff" type.';
                    my_warndlg(Lang(str1,str2), 1);
                    flag = 0;
                    return
                end
            case 'SlopeAcross'
                listeLayersSupplementaires(end+1) = 44; %#ok<AGROW>
            case 'SlopeAlong'
                listeLayersSupplementaires(end+1) = 45; %#ok<AGROW>
            case {'InsonifiedAreadB'; 'InsonifiedAreaSSc'}
                listeLayersSupplementaires(end+1) = 47; %#ok<AGROW>
            case 'WCSignalWidth'
                listeLayersSupplementaires(end+1) = 52; %#ok<AGROW>
            case 'BathymetryFromDTM'
                % TODO
            case 'TwoWayTravelTimeInSamples'
                listeLayersSupplementaires(end+1) = 6; %#ok<AGROW>
                
            case 'TxAngleKjell'
                listeLayersSupplementaires(end+1) = 48; %#ok<AGROW>
                
                %{
Columns 1 through 6
'Depth'    'xxxx'    'xxxx'    'xxxx'    'BeamAzimuthAngle'    'xxxx'

Columns 7 through 11
'QualityFactor'    'LengthDetection'    'xxxx'    'xxxx'    'DetecAmplitudeNbSamples'

Columns 12 through 16
'DetecPhaseOrdrePoly'    'DetecPhaseVariance'    'xxxx'    'Raw-TxTiltAngle'    'Raw-Range'

Columns 17 through 23
'xxxx'    'LongueurTrajet'    'Temps universel'    'Roll'    'Pitch'    'Heave'    'Heading'

Columns 24 through 28
'xxxx'    'xxxx'    'xxxx'    'xxxx'    'xxxx'

Columns 29 through 34
'Mask'    'IndexNumberBeam'    'InfoBeamSortDirection'    'InfoBeamNbSamples'    'InfoBeamCentralSample'    'Speed'

Columns 35 through 40
'IfremerQF'    'xxxx'    'SoundSpeed'    'Mode'    'CentralFrequency'    'xxxx'

Columns 41 through 45
'xxxx'    'xxxx'    'xxxx'    'xxxx'    'xxxx'

Columns 46 through 47
'BathymetryFromDTM'    'xxxx'
                %}
                
            case 'SampleFrequency' % TODO : rajout� par JMA le 29/10/2013 pour correction images EM112 belges qui d�pendent de la fr�quence d'�chantillonnage
            case 'PresenceFM' % TODO : rajout� par JMA le 03/04/2014 pour correction images EM302 o� il y a une diff�rence de r�flectivit� entre CW et FM
            case 'SonarFrequency'
            otherwise
                str = sprintf('"%s" not taken into account. Contact sonarscope@ifremer.fr Augustin', DataTypeConditions{k});
                my_warndlg(str, 1);
                flag = 0;
                return
        end
    end
end
