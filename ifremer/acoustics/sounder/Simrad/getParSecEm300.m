% Parametres des faisceaux d'emission de l'EM300.
%
% Syntax
%   [AngDebSec, FrSec, AngSec, OuvSec] = getParSecEm300( TXMode )
%
% INTPUT PARAMETERS :
%   TXMode : Mode du sondeur
%
% Output Arguments
%   AngDebSec : Debuts des secteurs (deg).
%   FrSec     : Frequences des secteurs (kHz).
%   AngSec    : Centres des secteurs (deg).
%   OuvSec    : Ouvertures des secteurs (deg).
%
% Examples
%   [AngDebSec, FrSec, AngSec, OuvSec] = getParSecEm300(1)
%
% See also Authors
% Authors : JMA
%-----------------------------------------------------------------------

function [AngDebSec, FrSec, AngSec, OuvSec] = getParSecEm300( TXMode )

switch TXMode
    case {0 1 2}
        FrSec = [31.5 33 30];
        AngSec = [60 0 -60];
        OuvSec = [25 95 25];
        AngDebSec = [47.5 -47.5];
    otherwise
        FrSec = [];
        AngSec = [];
        OuvSec = [];
        AngDebSec = [];
        disp('Mode non encore renseigne');
end
