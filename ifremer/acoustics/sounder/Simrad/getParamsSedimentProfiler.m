% Lecture des parametres generaux d'un sondeur
%
% Syntax
%   listeSondeurs  = getParamsSedimentProfiler
%   listeModes     = getParamsSedimentProfiler(identSounder)
%   listeSousModes = getParamsSedimentProfiler(identSounder, identMode)
%   params         = getParamsSedimentProfiler(identSounder, identMode, identSousMode)
% 
% Name-Value Pair Arguments 
%   identSounder  : Identificateur du sondeur
%   identMode     : Identificateur du mode du sondeur
%   identSousMode : Identificateur du sous-mode du mode du sondeur
%
% Output Arguments 
%   listeSondeurs  : Si aucun parametre en entree
%   listeModes     : Si 1 parametre en entree
%   listeSousModes : Si 2 parametres en entree
%   params         : Si 3 parametres en entree
% 
% Examples
%   listeSondeurs  = getParamsSedimentProfiler
%   params         = getParamsSedimentProfiler(1)
%
% See also cl_pames Authors
% Authors : JMA
% VERSION  : $Id: getParamsSedimentProfiler.m,v 1.2 2002/06/20 11:46:39 augustin Exp $
% -------------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HISTORIQUE DEVELOPPEMENT
%   16/02/01 - JMA - creation
%   26/03/01 - JMA - Mise en ref.
% ----------------------------------------------------------------------------

function params = getParamsSedimentProfiler(varargin)

if nargin == 0    % Cas ou on demande la liste des sondeurs
    params = {'Suroit BF'; 'Chirp GF'};
    return
end

identSounder = varargin{1};

switch identSounder
case {1, 'Suroit BF'}
    identSounder = 1;
case {2, 'Chirp GF'}
    identSounder = 2;
otherwise
    params = [];
    return
end

params.TxBeamWidtCircle             = [];
params.TxBeamWidtSquare             = [];
params.TxBeamWidtAlonRectangle      = [];
params.TxBeamWidtAcroRectangle      = [];

params.RxBeamWidtCircle             = [];
params.RxBeamWidtSquare             = [];
params.RxBeamWidtAlonRectangle      = [];
params.RxBeamWidtAcroRectangle      = [];

switch identSounder
case 1 % Suroit BF
    params.Frequency                = 3.5;
    params.CoherentProcessing       = 2;
    params.ModulationBandwith       = 3;
    params.TxArrayShape             = 1;    % 1=circular, 2=square, 3=rectangular
    params.TxBeamWidtCircle         = 30;
    params.RxArrayIsEqualTxArray    = 1;
%     params.RxArrayShape             = 1;
%     params.RxBeamWidtCircle         = 30;
    
    params.EmissionLevel            = 212;
    params.EmitedSignalDuration     = 80;
    params.RangeSampling            = 0.25;
    params.MaximumDepth             = 3000;    
case 2 % Chirp GF
    params.Frequency                = 1.425;   % = (2.20 + 0.65 )/2;
    params.CoherentProcessing       = 2;
    params.ModulationBandwith       = 1.55;     % =  2.20 - 0.65
    params.TxArrayShape             = 1;
    params.TxBeamWidtCircle         = 70;
    params.RxArrayIsEqualTxArray    = 1;
%     params.RxArrayShape             = 1;
%     params.RxBeamWidtCircle         = 70;
    
    params.EmissionLevel            = 196;
    params.EmitedSignalDuration     = 50;
    params.RangeSampling            = 0.25;
    params.MaximumDepth             = 3000;    
otherwise
    errordlg('Ce sondeur n''existe pas')
end

