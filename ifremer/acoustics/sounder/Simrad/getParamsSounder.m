% Lecture des parametres generaux d'un sondeur
%
% listeSondeurs  = getParamsSounder
% listeModes     = getParamsSounder(identSounder)
% listeSousModes = getParamsSounder(identSounder, identMode)
% params         = getParamsSounder(identSounder, identMode, identSousMode)
% 
% Name-Value Pair Arguments 
%   identSounder  : Identificateur du sondeur
%   identMode     : Identificateur du mode du sondeur
%   identSousMode : Identificateur du sous-mode du mode du sondeur
%
% Output Arguments 
%   listeSondeurs  : Si aucun parametre en entree
%   listeModes     : Si 1 parametre en entree
%   listeSousModes : Si 2 parametres en entree
%   params         : Si 3 parametres en entree
% 
% Examples
%   listeSondeurs  = getParamsSounder
%   listeModes     = getParamsSounder(1)
%   listeSousModes = getParamsSounder(1, 1)
%   params         = getParamsSounder(1, 1, 1)
%
% See also cl_pames Authors
% Authors : JMA
% -------------------------------------------------------------------------------

function params = getParamsSounder(varargin)

a = cl_sounder;

N = nargin;
if N == 0    % Cas ou on demande la liste des sondeurs
%     params = {'EM12D'; 'EM12S'; 'EM300'; 'EM1000'; 'SAR'; 'EM3000D'; 'EM3000S'; 'DTS1'; 'EM1002'; 'EM120'};
    params = get(a, 'SonarNames');
    return
end

identSounder = varargin{1};
a = set(a, 'Sonar.Ident', identSounder);
if N == 1
    params = get(a, 'strModes_1');
    return
end

SonarMode_1 = varargin{2};
a = set(a, 'Sonar.Mode_1', SonarMode_1);
if N == 2
    params = get(a, 'strModes_2');
    return
end

SonarMode_2 = varargin{3};
a = set(a, 'Sonar.Mode_2', SonarMode_2);

params.Frequency                	= get(a, 'Sonar.Freq');
params.ListeModes                   = get(a, 'strModes_1');
params.CoherentProcessing           = get(a, 'Signal.Type');
params.ModulationBandwith           = get(a, 'Signal.BandWth');
% params.ModulationBandwith           = get_BandWth(a);
params.TxBeamWidthAlongTrack        = get(a, 'BeamForm.Tx.LongWth');
params.RxBeamWidthAlongTrack        = get(a, 'BeamForm.Rx.LongWth');
params.RxBeamWidthAcrossTrack       = get(a, 'BeamForm.Rx.TransWth');
params.RxArrayShape                 = get(a, 'Array.Rx.Type');
params.TxArrayShape                 = get(a, 'Array.Tx.Type');
params.RxArrayTilt                  = get(a, 'Array.Rx.TransTilt');
params.TxArrayTilt                  = get(a, 'Array.Tx.TransTilt');
params.BeamFormRxAngles             = get(a, 'BeamForm.Rx.Angles');

params.ListeSousModes               = get(a, 'strModes_2');
params.EmissionLevel                = get_NE(a);
params.EmitedSignalDuration         = get_Duration(a);
params.TxBeamWidthAcrossTrack       = get(a, 'TxBeamWidthAcrossTrack');
params.ReceptionMaximumAngle        = get(a, 'ApertWth');
Freq = get(a, 'SampFreq');
Freq = min(Freq); % Rajout� le 20/02/2013 pour EM710 DualSwath Deep
params.RangeSampling                = 1500 / (2 * Freq);
params.NbSoundingsPerPing           = get(a, 'RxNbBeams');
params.MaximumDepth                 = get(a, 'MaxDepth');
params.ModeRepartition              = get(a, 'BeamForm.Rx.Repart');
params.Faisceaux.Centres            = get(a, 'BeamForm.Rx.Angles');
params.Faisceaux.Ouvertures         = repmat(get(a, 'BeamForm.Rx.TransWth'), 1, length(params.Faisceaux.Centres));
params.Secteurs.Ordre               = 1:length(params.EmissionLevel);
params.Secteurs.coefs               = getTxParams(a);
params.Secteurs.LimitesAngulaires	= get(a, 'BeamForm.Tx.LimitAng');
params.Secteurs.Frequence           = get(a, 'BeamForm.Tx.Freq');

return

%{
switch identSounder
    case {1, 'EM12D'}
        identSounder = 1;
        ListeModes = {'Deep';'Shallow'};
        SonarType = 2;   % Sondeur multifaisceau
    case {2, 'EM12S'}
        identSounder = 2;
        ListeModes = {'Deep';'Shallow'};
        SonarType = 2;   % Sondeur multifaisceau
    case {3, 'EM300'}
        identSounder = 3;
        ListeModes = {'Very Shallow (135)'; 'Shallow (136)'; 'Medium (137)'; 'Deep (138)'; 'Very Deep (139)'; 'Extra Deep (140)'};
        SonarType = 2;   % Sondeur multifaisceau
    case {4, 'EM1000'}
        identSounder = 4;
        ListeModes = {'Deep';'Medium';'Shallow'};
        SonarType = 2;   % Sondeur multifaisceau
    case {5, 'SAR'}
        identSounder = 5;
        ListeModes = {'Full';'Haff'};
        SonarType = 1;   % Sonar lateral
    case {6, 'EM3000D'}
        identSounder = 6;
        ListeModes = {'Modes?'};
        SonarType = 2;   % Sondeur multifaisceau
    case {7, 'EM3000S'}
        identSounder = 7;
        ListeModes = {'Modes?'};
        SonarType = 2;   % Sondeur multifaisceau
    case {8, 'DTS1'}
        identSounder = 8;
        ListeModes = {'75kHz 14ms';'75kHz 50ms';'410kHz'};
         SonarType = 1;   % Sonar lateral
   case {9, 'EM1002'}
        identSounder = 9;
        ListeModes = {'Shallow'};
        SonarType = 2;   % Sondeur multifaisceau
    case {10, 'EM120'}
        identSounder = 10;
        ListeModes = {'Very Shallow (135)'; 'Shallow (136)'; 'Medium (137)'; 'Deep (138)'; 'Very Deep (139)'};
        SonarType = 2;   % Sondeur multifaisceau
    otherwise
        ListeModes = [];
        SonarType = 2;   % Sondeur multifaisceau
end

if nargin == 1    % Cas ou on demande la liste des modes
    params = ListeModes;
    return
end

identMode = varargin{2};

if nargin == 2
    identSousMode = 1;
else
    identSousMode = varargin{3};
end

if nargin == 4
    version = varargin{4};
end
version = 1; % ON IMPOSE MODE AVANT MARS 2000

params.SonarType = SonarType;
switch identSounder
    case 1 % EM12D
        params.Frequency                                = 13;
        params.ListeModes                               = ListeModes;
        params.CoherentProcessing                       = 1;
        params.ModulationBandwith                       = 0;
        params.TxBeamWidthAlongTrack                    = 1.8;
        params.RxBeamWidthAlongTrack                    = 18;
        params.RxBeamWidthAcrossTrack                   = 3.5;
        params.RxArrayShape                             = 2;
        params.TxArrayShape                             = 2;
        params.RxArrayTilt                              = 40;    
        params.TxArrayTilt                              = 40;    
        
        switch identMode
            case {1, 'Deep'}
                identMode = 1;
                SousModePrefere = 2;
                ListeSousModes                  = {'150';'140';'128';'114';'98';'iso'};
                EmissionLevel                   = repmat(237,  1, 6);
                EmitedSignalDuration            = repmat(10,   1, 6);
                TxBeamWidthAcrossTrack          = [150 140 128 114 98 150];
                ReceptionMaximumAngle           = [ 75  70  64  57 49 75];
                RangeSampling                   = repmat(2.4,  1, 6);
                NbSoundingsPerPing              = repmat(162,  1, 6);
                MaximumDepth                    = repmat(6000, 1, 6);
                
                LBEAM                           = [ -2.6875 -2 -1.5 -1.125 -0.8125 -5];
                FBEAM                           = [ 75      70   64     57      49 75];
                ModeRepartition                 = [1 1 1 1 1 0];
                for k=1:length(ListeSousModes)
                    NBEAMS =  NbSoundingsPerPing(k)/2;
                    if ModeRepartition(k)
                        x = getAnglesIsoDistance( LBEAM(k), FBEAM(k), NBEAMS );
                    else
                        x = round(16 * linspace(LBEAM(k), FBEAM(k), NBEAMS))/16;  
                    end
                    AnglesFaisceaux(k,:) = [fliplr(-x),x]; %#ok<AGROW>
                end
                
            case {2 , 'Shallow'}
                identMode = 2;
                SousModePrefere = 1;
                ListeSousModes                  = {'150';'iso'};
                EmissionLevel                   = [230 230];
                EmitedSignalDuration            = [2 2];
                TxBeamWidthAcrossTrack          = [150 150];
                ReceptionMaximumAngle           = [75 75];
                RangeSampling                   = [0.6 0.6];
                NbSoundingsPerPing              = [162 162];
                MaximumDepth                    = [6000 6000];
                
                LBEAM                           = [-2.6875 -5 ];
                FBEAM                           = [ 75     75];
                ModeRepartition                 = [1 0];
                for k=1:length(ListeSousModes)
                    NBEAMS =  NbSoundingsPerPing(k)/2;
                    if ModeRepartition(k)
                        x = getAnglesIsoDistance( LBEAM(k), FBEAM(k), NBEAMS );
                    else
                        x = round(16 * linspace(LBEAM(k), FBEAM(k), NBEAMS))/16;  
                    end
                    AnglesFaisceaux(k,:) = [fliplr(-x),x]; %#ok<AGROW>
                end
                
            otherwise
                errordlg('Ce mode n''existe pas')
        end
        
    case 2 % EM12S
        params.Frequency = 13;
        params.ListeModes                               = ListeModes;
        params.CoherentProcessing                       = 1;
        params.ModulationBandwith                       = 0;
        params.TxBeamWidthAlongTrack                    = 1.8;
        params.RxBeamWidthAlongTrack                    = 18;
        params.RxBeamWidthAcrossTrack                   = 3.5;    
        params.RxArrayShape                             = 2;
        params.TxArrayShape                             = 2;
        params.RxArrayTilt                              = 0;    
        params.TxArrayTilt                              = 0;    
        params.MaximumDepth                             = 6000;
        
        switch identMode
            case {1, 'Deep'}
                identMode = 1;
                SousModePrefere = 1;
                ListeSousModes                  = {'120'; '105'; '90'; 'iso'};
                EmissionLevel                   = repmat(237,  1, 4);
                EmitedSignalDuration            = repmat(10,   1, 4);
                TxBeamWidthAcrossTrack          = [120 105  90 90];
                ReceptionMaximumAngle           = [60  52.5 45 45];
                RangeSampling                   = repmat(2.4,  1, 4);
                NbSoundingsPerPing              = repmat(81,   1, 4);
                MaximumDepth                    = repmat(5000, 1, 4);
                
                LBEAM                          = [-60  -52.5 -45 -45];
                FBEAM                          = [ 60   52.5  45  45] ;
                ModeRepartition                 = [1 1 1 0];
                for k=1:length(ListeSousModes)
                    NBEAMS =  NbSoundingsPerPing(k);
                    if ModeRepartition(k)
                        x = getAnglesIsoDistance( LBEAM(k), FBEAM(k), NBEAMS );
                    else
                        x = round(16 * linspace(LBEAM(k), FBEAM(k), NBEAMS))/16;  
                    end
                    AnglesFaisceaux(k,:) = x; %#ok<AGROW>
                end
                
            case {2, 'Shallow'}
                identMode = 2;
                SousModePrefere = 1;
                ListeSousModes                  = {'120'; 'iso'};
                EmissionLevel                   = [230 230];
                EmitedSignalDuration            = [2 2];
                TxBeamWidthAcrossTrack          = [120 120];
                ReceptionMaximumAngle           = [60 60];
                RangeSampling                   = [0.6 0.6];
                NbSoundingsPerPing              = [81 81];
                MaximumDepth                    = [5000 5000];
                
                LBEAM                          = [-60  -60];
                FBEAM                          = [ 60   60] ;
                ModeRepartition                 = [1 0];
                for k=1:length(ListeSousModes)
                    NBEAMS =  NbSoundingsPerPing(k);
                    if ModeRepartition(k)
                        x = getAnglesIsoDistance( LBEAM(k), FBEAM(k), NBEAMS );
                    else
                        x = round(16 * linspace(LBEAM(k), FBEAM(k), NBEAMS))/16;  
                    end
                    AnglesFaisceaux(k,:) = x; %#ok<AGROW>
                end
                
                
            otherwise
                errordlg('Ce mode n''existe pas')
        end
        
    case 3 % EM300
        params.Frequency = 30;
        params.ListeModes                               = ListeModes;
        params.CoherentProcessing                       = 1;
        params.ModulationBandwith                       = 0;
        params.RxBeamWidthAlongTrack                    = 15;
        params.RxBeamWidthAcrossTrack                   = 2;    
        params.RxArrayShape                             = 2;
        params.TxArrayShape                             = 2;
        params.RxArrayTilt                              = 0;    
        params.TxArrayTilt                              = 0;    
        SousModePrefere = 2;
        
        switch identMode
            case {6, 'Extra Deep', 'Extra Deep (140)'}  % VALEURS NON CONNUES
                disp('ATTENTION : Parametres vrais non connus. ');
                identMode = 6;
                ListeSousModes                  = {'60'};
                EmissionLevel                   = repmat(240, 1, 1);
                EmitedSignalDuration            = repmat(5,   1, 1);
                params.TxBeamWidthAlongTrack    = 1;
                TxBeamWidthAcrossTrack          = 60;
                ReceptionMaximumAngle           = repmat(50, 1, 1);
                RangeSampling                   = repmat(1500 / (2*563), 1, 1);
                NbSoundingsPerPing              = repmat(135,            1, 1);
                MaximumDepth                    = repmat(4000,           1, 1);
                for k=1:length(ListeSousModes)
                    AnglesFaisceaux(k,:) = getAnglesRecSimrad( 1, TxBeamWidthAcrossTrack(k)/2, 3, NbSoundingsPerPing(k)); %#ok<AGROW>
                end
                ModeRepartition                 = repmat(1, 1, 3);    % Provisoire
            case {5, 'Very Deep', 'Very Deep (139)'}
                identMode = 5;
                ListeSousModes                  = {'98';'80';'64'};
                EmissionLevel                   = repmat(240,           1, 3);
                EmitedSignalDuration            = repmat(5,             1, 3);
                params.TxBeamWidthAlongTrack    = 1;
                TxBeamWidthAcrossTrack          = [ 98   80   64];
                ReceptionMaximumAngle           = repmat(50,             1, 3);
                RangeSampling                   = repmat(1500 / (2*563), 1, 3);
                NbSoundingsPerPing              = repmat(135,            1, 3);
                MaximumDepth                    = repmat(4000,           1, 3);
                for k=1:length(ListeSousModes)
                    AnglesFaisceaux(k,:) = getAnglesRecSimrad( 1, TxBeamWidthAcrossTrack(k)/2, 3, NbSoundingsPerPing(k)); %#ok<AGROW>
                end
                ModeRepartition                 = repmat(1,             1, 3);    % Provisoire
                
            case {4, 'Deep', 'Deep (138)'}
                identMode = 4;
                ListeSousModes                  = {'150';'140';'128';'114';'98';'80';'64'};
                EmissionLevel                   = repmat(238, 1, 7);
                EmitedSignalDuration            = repmat(5,   1, 7);
                params.TxBeamWidthAlongTrack    = 1;
                TxBeamWidthAcrossTrack          = [150 140 128 114 98 80 64];
                ReceptionMaximumAngle           = repmat(75,   1, 7);
                RangeSampling                   = repmat(1500 / (2*1127), 1, 7);
                NbSoundingsPerPing              = repmat(135,  1, 7);
                MaximumDepth                    = repmat(2500, 1, 7);
                for k=1:length(ListeSousModes)
                    AnglesFaisceaux(k,:) = getAnglesRecSimrad( 1, TxBeamWidthAcrossTrack(k)/2, 2, NbSoundingsPerPing(k)); %#ok<AGROW>
                    if(k == 4)
                        % ATTENTION BIDOUILLE PROVIVOIRE
                        AnglesFaisceaux(k,:) = getAnglesRecSimrad(1, [-56.25 54.9], 2, 135); %#ok<AGROW>
                    end
                end
                ModeRepartition                 = repmat(1, 1, 7);    % Provisoire
                
            case {3, 'Medium', 'Medium (137)'}
                identMode = 3;
                ListeSousModes                  = {'150';'140';'128';'114';'98';'80';'64'};
                EmissionLevel                   = repmat(236, 1, 7);
                EmitedSignalDuration            = repmat(2,   1, 7);
                params.TxBeamWidthAlongTrack    = 1;
                TxBeamWidthAcrossTrack          = [150 140 128 114 98 80 64];
                ReceptionMaximumAngle           = repmat(75,   1, 7);
                RangeSampling                   = repmat(1500 / (2*2254), 1, 7);
                NbSoundingsPerPing              = repmat(135,  1, 7);
                MaximumDepth                    = repmat(800, 1, 7);
                for k=1:length(ListeSousModes)
                    AnglesFaisceaux(k,:) = getAnglesRecSimrad( 1, TxBeamWidthAcrossTrack(k)/2, 3, NbSoundingsPerPing(k)); %#ok<AGROW>
                end
                ModeRepartition                 = repmat(1, 1, 7);    % Provisoire
                
            case {2 , 'Shallow', 'Shallow (136)'}
                identMode = 2;
                ListeSousModes                  = {'150';'140';'128';'114';'98';'80';'64'};
                EmissionLevel                   = repmat(230, 1, 7);
                EmitedSignalDuration            = repmat(0.7,   1, 7);
                params.TxBeamWidthAlongTrack    = 2;
                TxBeamWidthAcrossTrack          = [150 140 128 114 98 80 64];
                ReceptionMaximumAngle           = repmat(75,   1, 7);
                RangeSampling                   = repmat(1500 / (2*4509), 1, 7);
                NbSoundingsPerPing              = repmat(111,  1, 7);
                MaximumDepth                    = repmat(250,  1, 7);
                for k=1:length(ListeSousModes)
                    AnglesFaisceaux(k,:) = getAnglesRecSimrad( 1, TxBeamWidthAcrossTrack(k)/2, 3, NbSoundingsPerPing(k)); %#ok<AGROW>
                end
                ModeRepartition                 = repmat(1, 1, 7);    % Provisoire
                
            case {1 , 'Very Shallow', 'Very Shallow (135)'}
                identMode = 1;
                ListeSousModes                  = {'150';'140';'128';'114';'98';'80';'64'};
                EmissionLevel                   = repmat(224, 1, 7);
                EmitedSignalDuration            = repmat(0.7,   1, 7);
                params.TxBeamWidthAlongTrack    = 4;
                TxBeamWidthAcrossTrack          = [150 140 128 114 98 80 64];
                ReceptionMaximumAngle           = repmat(75,   1, 7);
                RangeSampling                   = repmat(1500 / (2*4509), 1, 7);
                NbSoundingsPerPing              = repmat(111,  1, 7);
                MaximumDepth                    = repmat(50,  1, 7);
                for k=1:length(ListeSousModes)
                    AnglesFaisceaux(k,:) = getAnglesRecSimrad( 1, TxBeamWidthAcrossTrack(k)/2, 3, NbSoundingsPerPing(k)); %#ok<AGROW>
                end
                ModeRepartition                 = repmat(1, 1, 7);    % Provisoire
                
            otherwise
                errordlg('Ce mode n''existe pas')
        end
        
    case 4 % EM1000
        params.Frequency = 95;
        params.ListeModes                               = ListeModes;
        params.CoherentProcessing                       = 1;
        params.ModulationBandwith                       = 0;
        params.TxBeamWidthAlongTrack                    = 3.3;
        params.RxBeamWidthAlongTrack                    = 3.3;
        params.RxBeamWidthAcrossTrack                   = 3.3;
        params.RxArrayShape                             = 1;
        params.TxArrayShape                             = 1;
        params.RxArrayTilt                              = 0;    
        params.TxArrayTilt                              = 0;    
        
        switch identMode
            case {1, 'Deep'}
                identMode = 1;
                SousModePrefere = 1;
                ListeSousModes                  = {'70'};
                EmissionLevel                   = 226;
                EmitedSignalDuration            = 2;
                TxBeamWidthAcrossTrack          = 70;
                ReceptionMaximumAngle           = 35;
                RangeSampling                   = 0.3;
                NbSoundingsPerPing              = 48;
                MaximumDepth                    = 1000;
                
                LBEAM                           = [ -35.0000; -34.4375];
                FBEAM                           = [  33.8125;  34.4375];
                ModeRepartition                 = 1;
                for k=1:length(ListeSousModes)
                    NBEAMS =  NbSoundingsPerPing(k);
                    if ModeRepartition(k)
                        x1 = getAnglesIsoDistance( LBEAM(1,k), FBEAM(1,k), NBEAMS );
                        x2 = getAnglesIsoDistance( LBEAM(2,k), FBEAM(2,k), NBEAMS );
                    else
                        x1 = round(16 * linspace(LBEAM(1,k), FBEAM(1,k), NBEAMS))/16;  
                        x2 = round(16 * linspace(LBEAM(2,k), FBEAM(2,k), NBEAMS))/16;  
                    end
                    % ATTENTION ICI LE SONDEUR SEMBLE NE PAS FAIRE DE DEPOINTAGE
                    % Voir _angles_EM1000_70_2_MS_48_FAIS_h
                    AnglesFaisceaux(k,:) = [x1 x2]; %#ok<AGROW>
                end
                
            case {2, 'Medium'}
                identMode = 2;
                SousModePrefere = 1;
                ListeSousModes                  = {'120';'104';'88'};
                EmissionLevel                   = repmat(225,  1,3);
                EmitedSignalDuration            = repmat(0.7,  1,3);
                TxBeamWidthAcrossTrack          = [120 104 88];
                ReceptionMaximumAngle           = [ 60  52 44];
                RangeSampling                   = repmat(0.3,  1,3);
                NbSoundingsPerPing              = repmat(48,   1,3);
                MaximumDepth                    = repmat(500, 1,3);
                
                LBEAM                           = [-60.0000 -52.0000 -44.0000
                    -59.5000 -51.4375 -43.3750];
                FBEAM                           = [ 59.5000  51.4375  43.3750
                    60.0000  52.0000  44.0000] ;
                
                ModeRepartition                 = [1 1 1];
                for k=1:length(ListeSousModes)
                    NBEAMS =  NbSoundingsPerPing(k);
                    if ModeRepartition(k)
                        x1 = getAnglesIsoDistance( LBEAM(1,k), FBEAM(1,k), NBEAMS );
                        x2 = getAnglesIsoDistance( LBEAM(2,k), FBEAM(2,k), NBEAMS );
                    else
                        x1 = round(16 * linspace(LBEAM(1,k), FBEAM(1,k), NBEAMS))/16;  
                        x2 = round(16 * linspace(LBEAM(2,k), FBEAM(2,k), NBEAMS))/16;  
                    end
                    AnglesFaisceaux(k,:) = [x1 x2]; %#ok<AGROW>
                end
                
                
            case {3, 'Shallow'}
                identMode = 3;
                SousModePrefere = 1;
                ListeSousModes                  = {'150';'140';'128'};
                EmissionLevel                   = repmat(224,  1,3);
                EmitedSignalDuration            = repmat(0.2,  1,3);
                TxBeamWidthAcrossTrack          = [150 140 128];
                ReceptionMaximumAngle           = [ 75  70  64];
                RangeSampling                   = repmat(0.15, 1,3);
                NbSoundingsPerPing              = repmat(60,   1,3);
                MaximumDepth                    = repmat(100, 1,3);
                
                LBEAM                           = [-75.0000 -70.0000 -64.0000
                    -74.7500 -69.6875 -63.6250];
                FBEAM                           = [ 74.7500  69.6875  63.6250
                    75.0000  70.0000  64.0000] ;
                ModeRepartition                 = [1 1 1];
                for k=1:length(ListeSousModes)
                    NBEAMS =  NbSoundingsPerPing(k);
                    if ModeRepartition(k)
                        x1 = getAnglesIsoDistance( LBEAM(1,k), FBEAM(1,k), NBEAMS );
                        x2 = getAnglesIsoDistance( LBEAM(2,k), FBEAM(2,k), NBEAMS );
                    else
                        x1 = round(16 * linspace(LBEAM(1,k), FBEAM(1,k), NBEAMS))/16;  
                        x2 = round(16 * linspace(LBEAM(2,k), FBEAM(2,k), NBEAMS))/16;  
                    end
                    AnglesFaisceaux(k,:) = [x1 x2]; %#ok<AGROW>
                    %             AnglesFaisceaux(k,:) = [x2 x1];
                end
                
            otherwise
                errordlg('Ce mode n''existe pas')
        end
        
    case 5 % SAR
        params.Frequency = 180;
        params.ListeModes                               = ListeModes;
        params.CoherentProcessing                       = 2;
        params.ModulationBandwith                       = 2.5;
        params.TxBeamWidthAlongTrack                    = 1.5;
        params.RxBeamWidthAlongTrack                    = 1.5;
        params.RxBeamWidthAcrossTrack                   = 60;    
        params.RxArrayShape                             = 2;
        params.TxArrayShape                             = 2;
        params.RxArrayTilt                              = 60;    
        params.TxArrayTilt                              = 60;    
        
        
        switch identMode
            case {1, 'Full'}
                identMode = 1;
                SousModePrefere = 1;
                ListeSousModes                  = {'85'};
                EmissionLevel                   = 226;
                EmitedSignalDuration            = 20;
                TxBeamWidthAcrossTrack          = 170;
                ReceptionMaximumAngle           = 89;
                RangeSampling                   = 0.25;
                NbSoundingsPerPing              = 1;
                MaximumDepth                    = 100;
                ModeRepartition                 = 1;
                AnglesFaisceaux(1,:) = NaN;
                
            case {2, 'Haff'}
                identMode = 2;
                SousModePrefere = 1;
                ListeSousModes                  = {'85'};
                EmissionLevel                   = 223;
                EmitedSignalDuration            = 20;
                TxBeamWidthAcrossTrack          = 170;
                ReceptionMaximumAngle           = 89;
                RangeSampling                   = 0.25;
                NbSoundingsPerPing              = 1;
                MaximumDepth                    = 100;
                ModeRepartition                 = 1;
                AnglesFaisceaux(1,:) = NaN;
                
            otherwise
                errordlg('Ce mode n''existe pas')
        end
        
    case 6 % EM3000D
        params.Frequency = 300;
        params.ListeModes                               = ListeModes;
        params.CoherentProcessing                       = 1;
        params.ModulationBandwith                       = 0;
        params.TxBeamWidthAlongTrack                    = 1.5;
        params.RxBeamWidthAlongTrack                    = 30;
        params.RxBeamWidthAcrossTrack                   = 1.5;    
        params.RxArrayShape                             = 2;
        params.TxArrayShape                             = 2;
        params.RxArrayTilt                              = 40;    
        params.TxArrayTilt                              = 40;    
        
        switch identMode
            case {1, 'Deep'}
                identMode = 1;
                SousModePrefere = 1;
                ListeSousModes                  = {'120'};
                EmissionLevel                   = 240;
                EmitedSignalDuration            = 0.150;
                TxBeamWidthAcrossTrack          = 120;
                ReceptionMaximumAngle           = 75;
                RangeSampling                   = 1500 * EmitedSignalDuration / 2;
                NbSoundingsPerPing              = 254;
                MaximumDepth                    = 150;
                ModeRepartition                 = 1;
                AnglesFaisceaux(1,:) = getAnglesRecSimrad( 1, ReceptionMaximumAngle, 3, NbSoundingsPerPing);
                
            otherwise
                errordlg('Ce mode n''existe pas')
        end
        
    case 7 % EM3000S
        params.Frequency = 300;
        params.ListeModes                               = ListeModes;
        params.CoherentProcessing                       = 1;
        params.ModulationBandwith                       = 0;
        params.TxBeamWidthAlongTrack                    = 1.5;
        params.RxBeamWidthAlongTrack                    = 30;
        params.RxBeamWidthAcrossTrack                   = 1.5;    
        params.RxArrayShape                             = 2;
        params.TxArrayShape                             = 2;
        params.RxArrayTilt                              = 0;    
        params.TxArrayTilt                              = 0;    
        
        switch identMode
            case {1, 'Deep'}
                identMode = 1;
                SousModePrefere = 1;
                ListeSousModes                  = {'120'};
                EmissionLevel                   = 240;
                EmitedSignalDuration            = 0.150;
                TxBeamWidthAcrossTrack          = 120;
                ReceptionMaximumAngle           = 75;
                RangeSampling                   = 1500 * EmitedSignalDuration * 1e-3 / 2;
                NbSoundingsPerPing              = 127;
                MaximumDepth                    = 150;
                ModeRepartition                 = 1;
                AnglesFaisceaux(1,:) = getAnglesRecSimrad( 1, ReceptionMaximumAngle, 3, NbSoundingsPerPing);
                
            otherwise
                errordlg('Ce mode n''existe pas')
        end
        
    case 8 % DTS1
        params.ListeModes                               = ListeModes;
        params.CoherentProcessing                       = 2;
        params.RxBeamWidthAcrossTrack                   = 70;  
        params.RxArrayShape                             = 2;
        params.TxArrayShape                             = 2;
        params.RxArrayTilt                              = 65;  
        params.TxArrayTilt                              = 65;  
        
        
        switch identMode
            case {1, '75kHz 14ms'}
                params.Frequency                = 75;
                params.ModulationBandwith       = 7.5;
                params.TxBeamWidthAlongTrack                    = 0.8;
                params.RxBeamWidthAlongTrack                    = 0.8;
                identMode = 1;
                SousModePrefere = 1;
                ListeSousModes                  = {'85'};
                EmissionLevel                   = 210;
                EmitedSignalDuration            = 14;
                TxBeamWidthAcrossTrack          = 170;
                ReceptionMaximumAngle           = 89;
                RangeSampling                   = 0.1;
                NbSoundingsPerPing              = 1;
                MaximumDepth                    = 100; % A definir
                ModeRepartition                 = 1;
                AnglesFaisceaux(1,:) = NaN;
                
            case {2, '75kHz 50ms'}
                params.Frequency                = 75;
                params.ModulationBandwith       = 2;
                params.TxBeamWidthAlongTrack                    = 0.8;
                params.RxBeamWidthAlongTrack                    = 0.8;
                identMode = 1;
                SousModePrefere = 1;
                ListeSousModes                  = {'85'};
                EmissionLevel                   = 210;
                EmitedSignalDuration            = 50;
                TxBeamWidthAcrossTrack          = 170;
                ReceptionMaximumAngle           = 89;
                RangeSampling                   = 0.1;
                NbSoundingsPerPing              = 1;
                MaximumDepth                    = 100; % A definir
                ModeRepartition                 = 1;
                AnglesFaisceaux(1,:) = NaN;
                
            case {3, '410 kHz'}
                params.Frequency                = 410;
                params.ModulationBandwith       = 41;
                params.TxBeamWidthAlongTrack                    = 0.5;
                params.RxBeamWidthAlongTrack                    = 0.5;
                identMode = 2;
                SousModePrefere = 1;
                ListeSousModes                  = {'85'};
                EmissionLevel                   = 216;
                EmitedSignalDuration            = 2.4;
                TxBeamWidthAcrossTrack          = 170;
                ReceptionMaximumAngle           = 89;
                RangeSampling                   = 0.018;
                NbSoundingsPerPing              = 1;
                MaximumDepth                    = 100; % A definir
                ModeRepartition                 = 1;
                AnglesFaisceaux(1,:) = NaN;
                
            otherwise
                errordlg('Ce mode n''existe pas')
        end
        
    case 9 % EM1002
        params.Frequency = 95;
        params.ListeModes                               = ListeModes;
        params.CoherentProcessing                       = 1;
        params.ModulationBandwith                       = 0;
        params.RxBeamWidthAlongTrack                    = 15;
        params.RxBeamWidthAcrossTrack                   = 2;    
        params.RxArrayShape                             = 2;
        params.TxArrayShape                             = 2;
        params.RxArrayTilt                              = 0;    
        params.TxArrayTilt                              = 0;    
        SousModePrefere = 2;
        
        switch identMode
            case {1 , 'Shallow', 'Shallow (136)'}
                identMode = 1;
                ListeSousModes                  = {'150';'140';'128';'114';'98';'80';'64'};
                EmissionLevel                   = repmat(230, 1, 7);
                EmitedSignalDuration            = repmat(0.7,   1, 7);
                params.TxBeamWidthAlongTrack    = 2;
                TxBeamWidthAcrossTrack          = [150 140 128 114 98 80 64];
                ReceptionMaximumAngle           = repmat(75,   1, 7);
                RangeSampling                   = repmat(1500 / (2*4509), 1, 7);
                NbSoundingsPerPing              = repmat(111,  1, 7);
                MaximumDepth                    = repmat(250,  1, 7);
                for k=1:length(ListeSousModes)
                    AnglesFaisceaux(k,:) = getAnglesRecSimrad( 1, TxBeamWidthAcrossTrack(k)/2, 3, NbSoundingsPerPing(k)); %#ok<AGROW>
                end
                ModeRepartition                 = repmat(1, 1, 7);    % Provisoire
                
            otherwise
                errordlg('Ce mode n''existe pas')
        end
        
    case 10 % EM120
        params.Frequency = 12;
        params.ListeModes                               = ListeModes;
        params.CoherentProcessing                       = 1;
        params.ModulationBandwith                       = 0;
%         params.RxBeamWidthAlongTrack                    = 33;
        params.RxBeamWidthAcrossTrack                   = 2;    
        params.RxArrayShape                             = 2;
        params.TxArrayShape                             = 2;
        params.RxArrayTilt                              = 0;    
        params.TxArrayTilt                              = 0;    
        SousModePrefere = 1;
        

        params.TxBeamWidthAcrossTrack   = 2; %????????????????????????
        Version = 2;
        switch Version
            case 1 % '1x1'
                params.TxBeamWidthAlongTrack    = 1;
                params.RxBeamWidthAlongTrack   = 1; %????????????????????????
            case 2 % '1x2'
                params.TxBeamWidthAlongTrack    = 1;
                params.RxBeamWidthAlongTrack   = 2; %????????????????????????
            case 3 % '2x2'
                params.TxBeamWidthAlongTrack    = 2;
                params.RxBeamWidthAlongTrack   = 2; %????????????????????????
            case 4 % '2x4'
                params.TxBeamWidthAlongTrack    = 2;
                params.RxBeamWidthAlongTrack   = 4; %????????????????????????
            case 5 % '4x4'
                params.TxBeamWidthAlongTrack    = 4;
                params.RxBeamWidthAlongTrack   = 4; %????????????????????????
        end


        switch identMode
            case {5, 'Very Deep', 'Very Deep'}
                identMode = 5;
                ListeSousModes                  = {'104'};
                EmissionLevel                   = repmat(242.5,           1, 9);
                EmitedSignalDuration            = repmat(15,             1, 9);
                params.TxBeamWidthAlongTrack    = 1;
                TxBeamWidthAcrossTrack          = 104;
                ReceptionMaximumAngle           = repmat(52,             1, 9);
                RangeSampling                   = repmat(1500 / (2*2001.28), 1, 9);
                NbSoundingsPerPing              = repmat(191,            1, 9);
                MaximumDepth                    = repmat(12000,           1, 9);
                for k=1:length(ListeSousModes)
                    AnglesFaisceaux(k,:) = getAnglesRecSimrad(1, TxBeamWidthAcrossTrack(k)/2, 3, NbSoundingsPerPing(k)); %#ok<AGROW>
                end
                ModeRepartition                 = repmat(1,             1, 9);    % Provisoire
                
            case {4, 'Deep', 'Deep (138)'}
                identMode = 4;
                ListeSousModes                  = {'150'};
                EmissionLevel                   = repmat(239, 1, 9);
                EmitedSignalDuration            = repmat(15,   1, 9);
                params.TxBeamWidthAlongTrack    = 1;
                TxBeamWidthAcrossTrack          = 150;
                ReceptionMaximumAngle           = repmat(75,   1, 9);
                RangeSampling                   = repmat(1500 / (2*2001.28), 1, 9);
                NbSoundingsPerPing              = repmat(191,  1, 9);
                MaximumDepth                    = repmat(7000, 1, 9);
                for k=1:length(ListeSousModes)
                    AnglesFaisceaux(k,:) = getAnglesRecSimrad(1, TxBeamWidthAcrossTrack(k)/2, 2, NbSoundingsPerPing(k)); %#ok<AGROW>
%                     if(k == 4)
%                         % ATTENTION BIDOUILLE PROVIVOIRE
%                         AnglesFaisceaux(k,:) = getAnglesRecSimrad(1, [-56.25 54.9], 2, 135);
%                     end
                end
                ModeRepartition                 = repmat(1, 1, 9);    % Provisoire
                
            case {3, 'Medium', 'Medium (137)'}
                identMode = 3;
                ListeSousModes                  = {'150'};
                EmissionLevel                   = repmat(232.5, 1, 3);
                EmitedSignalDuration            = repmat(5,   1, 3);
                params.TxBeamWidthAlongTrack    = 1;
                TxBeamWidthAcrossTrack          = 150;
                ReceptionMaximumAngle           = repmat(75,   1, 3);
                RangeSampling                   = repmat(1500 / (2*2001.28), 1, 3);
                NbSoundingsPerPing              = repmat(191,  1, 3);
                MaximumDepth                    = repmat(1000, 1, 3);
                for k=1:length(ListeSousModes)
                    AnglesFaisceaux(k,:) = getAnglesRecSimrad( 1, TxBeamWidthAcrossTrack(k)/2, 3, NbSoundingsPerPing(k)); %#ok<AGROW>
                end
                ModeRepartition                 = repmat(1, 1, 3);    % Provisoire
                
            case {2 , 'Shallow', 'Shallow (136)'}
                identMode = 2;
                ListeSousModes                  = {'150'};
                EmissionLevel                   = repmat(232.5-6, 1, 3);
                EmitedSignalDuration            = repmat(0.7,   1, 3);
                params.TxBeamWidthAlongTrack    = 2;
                TxBeamWidthAcrossTrack          = 150;
                ReceptionMaximumAngle           = repmat(75,   1, 3);
                RangeSampling                   = repmat(1500 / (2*2001.28), 1, 3);
                NbSoundingsPerPing              = repmat(191,  1, 3);
                MaximumDepth                    = repmat(450,  1, 3);
                for k=1:length(ListeSousModes)
                    AnglesFaisceaux(k,:) = getAnglesRecSimrad( 1, TxBeamWidthAcrossTrack(k)/2, 3, NbSoundingsPerPing(k)); %#ok<AGROW>
                end
                ModeRepartition                 = repmat(1, 1, 3);    % Provisoire
                
            case {1 , 'Very Shallow', 'Very Shallow (135)'}
                identMode = 1;
                ListeSousModes                  = {'150'};
                EmissionLevel                   = repmat(232.5-12, 1, 3);
                EmitedSignalDuration            = repmat(2,   1, 3);
                params.TxBeamWidthAlongTrack    = 2;
                TxBeamWidthAcrossTrack          = 50;
                ReceptionMaximumAngle           = repmat(75,   1, 3);
                RangeSampling                   = repmat(1500 / (2*2001.28), 1, 3);
                NbSoundingsPerPing              = repmat(191,  1, 3);
                MaximumDepth                    = repmat(50,  1, 3);
                for k=1:length(ListeSousModes)
                    AnglesFaisceaux(k,:) = getAnglesRecSimrad( 1, TxBeamWidthAcrossTrack(k)/2, 3, NbSoundingsPerPing(k)); %#ok<AGROW>
                end
                ModeRepartition                 = repmat(1, 1, 3);    % Provisoire
                
            otherwise
                errordlg('Ce mode n''existe pas')
        end        
        
    otherwise
        errordlg('Ce sondeur n''existe pas')
end





if nargin == 2 % Cas ou on demande la liste des sous_modes
    params = ListeSousModes;
else            % Cas ou on demande les parametres
    if ischar(identSousMode)
        identSousMode = find(strcmp(ListeSousModes, identSousMode));
    end
    
    if identSousMode == 0
        % On demande le sous-mode prefere
        params = SousModePrefere;
        return
    end
    
    params.ListeSousModes                  = ListeSousModes{identSousMode};
    params.EmissionLevel                   = EmissionLevel(identSousMode);
    params.EmitedSignalDuration            = EmitedSignalDuration(identSousMode);
    params.TxBeamWidthAcrossTrack          = TxBeamWidthAcrossTrack(identSousMode);
    params.ReceptionMaximumAngle           = ReceptionMaximumAngle(identSousMode);
    params.RangeSampling                   = RangeSampling(identSousMode);
    params.NbSoundingsPerPing              = NbSoundingsPerPing(identSousMode);
    params.MaximumDepth                    = MaximumDepth(identSousMode);
    params.ModeRepartition                 = ModeRepartition(identSousMode);
    params.Faisceaux.Centres               = AnglesFaisceaux(identSousMode, :);
    params.Faisceaux.Ouvertures            = repmat(params.RxBeamWidthAcrossTrack, 1, length(params.Faisceaux.Centres)); 
    % ones(size( AnglesFaisceaux(identSousMode, :))) * TxBeamWidthAcrossTrack(identSousMode);
    
    switch identSounder
        case 1 % EM12D
            params.Secteurs.Ordre = 1:10;
            params.Secteurs.coefs = [	0, -69,  1.7*14
                0, -55,  1.4*14
                0, -40,  1.3*15
                0, -25,  1.4*17
                4,  -6,  1.4*21
                0,   6,  1.4*21
                0,  25,  1.4*17
                0,  40,  1.3*15
                0,  55,  1.4*14
                0,  69,  1.7*14 ];
            params.Secteurs.LimitesAngulaires = [	-75.0 -61.5
                -61.5 -47.0
                -47.0 -32.5
                -32.5 -15.5
                -15.5   5.5
                -5.5  15.5
                15.5  32.5
                32.5  47.0
                47.0  61.5
                61.5  75.0 ];
            params.Secteurs.Frequence = [13 12.7 13 12.7 13.3 13 12.7 13 12.7 13];
        case 2 % EM12S
            params.Secteurs.Ordre = 1:5;
            params.Secteurs.Frequence = [13 12.7 13 12.7 13]; % A verifier (valeur supposees)
            
            switch params.ListeSousModes
                case '120'
                    params.Secteurs.coefs = [	0, -50,  1.55*20
                        0, -32,  1.45*18
                        0,   0,  1.35*50
                        0,  32,  1.45*18
                        4,  50,  1.55*20 ];
                    params.Secteurs.LimitesAngulaires = [	-60.0 -40.0
                        -40.0 -22.5
                        -22.5  22.5
                        22.5  40.0
                        40.0  60.0];
                case '105'
                    params.Secteurs.coefs = [	0, -43,  1.45*20
                        0, -24,  1.45*18
                        0,   0,  1.40*30
                        0,  24,  1.45*18
                        4,  43,  1.45*20 ];
                    params.Secteurs.LimitesAngulaires = [	-52.5 -33.0
                        -33.0 -15.0
                        -15.0  15.0
                        15.0  33.0
                        33.0  52.5];
                case '90'
                    params.Secteurs.coefs = [	0, -36,  1.35*20
                        0, -17,  1.35*17
                        0,   0,  1.35*17
                        0,  17,  1.35*17
                        4,  36,  1.35*20 ];
                    params.Secteurs.LimitesAngulaires = [	-45.0 -25.0
                        -25.0  -8.0
                        -8.0   8.0
                        8.0  25.0
                        25.0  45.0];
                otherwise
                    errordlg('Pas encore programme dans getParamsSounder')
            end
            
        case 3 % EM300
            switch identMode
                case 6 % Extra Deep (140)
                    % params.Secteurs.Ordre = [1 3 2];    % A VERIFIER
                    params.Secteurs.Ordre = 1:3;
                    params.Secteurs.Frequence = [];
                    params.Secteurs.Frequence = [31.5 33 30];
                    
                    switch version
                        case 1 % Valeurs acorrespondant au SUROIT du 22/01/2000? au 24/03/2001
                            params.Secteurs.coefs = [	0 -9.5 16
                                0  0.0 14
                                0 10.5 16];
                            params.Secteurs.LimitesAngulaires = [-30.0 -5.0
                                -5.0  5.0
                                5.0 30.0];
                            
                        case 2 % Valeurs acorrespondant au SUROIT du 25/03/2001 au xx/xx/xxxx
                            params.Secteurs.coefs = [	0 -9.5 16
                                0  0.0 14
                                0 10.5 16];
                            params.Secteurs.LimitesAngulaires = [-30.0 -5.0
                                -5.0  5.0
                                5.0 30.0];
                    end
                    
                case 5 % Very Deep (139)
                    % params.Secteurs.Ordre = [1 3 5 7 9 8 6 4 2];
                    params.Secteurs.Ordre = 1:9;
                    params.Secteurs.Frequence = [31 32.5 34 32 33.5 30.5 33 31.5 30];
                    
                    switch version
                        case 1 % Valeurs acorrespondant au SUROIT du 22/01/2000? au 24/03/2001
                            params.Secteurs.coefs = [	0 -43.3 22
                                0 -31.5 20
                                0 -20.7 17
                                0 -10.6 16
                                0   0.0 15
                                0   9.8 16
                                0  20.9 17
                                0  31.5 20
                                0  44.1 24];
                            params.Secteurs.LimitesAngulaires = [   -75.0 -35.5
                                -35.5 -23.0
                                -23.0 -15.6
                                -15.6  -5.0
                                -5.0   4.3
                                4.3  16.2
                                16.2  25.7
                                25.7  37.5
                                37.5  75.0];
                        case 2 % Valeurs acorrespondant au SUROIT du 25/03/2001 au xx/xx/xxxx
                            params.Secteurs.coefs = [	0 -43.3 22
                                0 -30.5 20
                                0 -20.7 17
                                0  -9.6 17
                                0   0.0 14
                                0   9.8 16
                                0  19.9 16
                                0  31.5 24
                                0  44.1 24];
                            params.Secteurs.LimitesAngulaires = [   -75.0 -36.5
                                -36.5 -23.0
                                -23.0 -18.0
                                -18.0  -5.0
                                -5.0   4.3
                                4.3  16.2
                                16.2  24.0
                                24.0  34.0
                                34.0  75.0];
                    end
                    
                case 4 % Deep (138)
                    % params.Secteurs.Ordre = [1 3 5 7 9 8 6 4 2];
                    params.Secteurs.Ordre = 1:9;
                    params.Secteurs.Frequence = [31 32.5 34 32 33.5 30.5 33 31.5 30];
                    
                    switch version
                        case 1 % Valeurs acorrespondant au SUROIT du 22/01/2000? au 24/03/2001
                            params.Secteurs.coefs = [	0 -63.4 25
                                0 -44.7 20
                                0 -30.5 18
                                0 -18.4 18
                                0   0.0 40
                                0  17.9 18
                                0  31.0 18
                                0  44.7 20
                                0  63.5 25];
                            params.Secteurs.LimitesAngulaires = [  -90.0 -53.0
                                -53.0 -37.5
                                -37.5 -24.8
                                -24.8 -11.5
                                -11.5  11.5
                                11.5  24.8
                                24.8  37.5
                                37.5  54.0
                                54.0  90.0];
                            
                        case 2 % Valeurs acorrespondant au SUROIT du 25/03/2001 au xx/xx/xxxx
                            params.Secteurs.coefs = [	0 -63.4 25
                                0 -44.7 20
                                0 -29.5 18
                                0 -18.4 18
                                0   0.0 40
                                0  17.9 18
                                0  30.0 18
                                0  44.7 20
                                0  63.5 25];
                            params.Secteurs.LimitesAngulaires = [  -90.0 -53.6
                                -53.6 -33.5
                                -33.5 -26.4
                                -26.4  -9.0
                                -9.0    9.0
                                9.0   24.8
                                24.8   35.0
                                35.0   53.6
                                53.6   90.0];
                    end
                    
                case {3;2;1} % Medium; Shallow; Very Shallow (137, 136, 135)
                    % params.Secteurs.Ordre = [1 3 2];    % A VERIFIER
                    params.Secteurs.Ordre = 1:3;
                    params.Secteurs.Frequence = [31.5 33 30];
                    
                    switch version
                        case 1 % Valeurs acorrespondant au SUROIT du 22/01/2000? au 24/03/2001
                            params.Secteurs.coefs = [	0 -60.0 31
                                0   0.0 200
                                0  60.0 31];
                            params.Secteurs.LimitesAngulaires = [-90.0 -47.0
                                -47.0  45.5
                                45.5  90.0];

                        case 2 % Valeurs acorrespondant au SUROIT du 25/03/2001 au xx/xx/xxxx
                            params.Secteurs.coefs = [	0 -60.0 31
                                0   0.0 200
                                0  60.0 31];
                            params.Secteurs.LimitesAngulaires = [-90.0 -47.0
                                -47.0  45.5
                                45.5  90.0];
                    end
                otherwise
                    errordlg('Pas encore programme dans getParamsSounder')
            end
        case 4 % EM1000
            params.Secteurs.coefs = [];
            params.Secteurs.LimitesAngulaires = [];
            params.Secteurs.Frequence = params.Frequency;
        case 5 % SAR
            params.Secteurs.coefs = [];
            params.Secteurs.LimitesAngulaires = [];
            params.Secteurs.Frequence = params.Frequency;
        case 6 % EM3000D
            params.Secteurs.coefs = [];
            params.Secteurs.LimitesAngulaires = [];
            params.Secteurs.Frequence = params.Frequency;
        case 7 % EM3000S
            params.Secteurs.coefs = [];
            params.Secteurs.LimitesAngulaires = [];
            params.Secteurs.Frequence = params.Frequency;
        case 8 % DTS1
            params.Secteurs.coefs = [];
            params.Secteurs.LimitesAngulaires = [];
            params.Secteurs.Frequence = params.Frequency;

        case 9 % EM1002
            params.Secteurs.coefs = [];
            params.Secteurs.LimitesAngulaires = [];
            params.Secteurs.Frequence = params.Frequency;

            switch identMode
                case 1 % Shallow
                    % params.Secteurs.Ordre = [1 3 2];    % A VERIFIER
                    params.Secteurs.Ordre = 1:3;
                    params.Secteurs.Frequence = [93.37 98.328 93.37];

                    params.Secteurs.coefs = [	0 -60.0 25
                        0   0.0 96.9
                        0  60.0 25];
                    params.Secteurs.LimitesAngulaires = [-80.0 -50.0
                        -50.0  50.0
                        50.0  80.0];
                otherwise
                    my_warndlg('Simrad/getParamsSounder : Mode pas encore programme', 1);
            end

        case 10 % EM120
            switch identMode
                case 5 % Very Deep
                    params.Secteurs.Ordre = 1:9;
                    params.Secteurs.Frequence = [31 32.5 34 32 33.5 30.5 33 31.5 30];

                    params.Secteurs.coefs = [	0 -43.3 22
                        0 -31.5 20
                        0 -20.7 17
                        0 -10.6 16
                        0   0.0 15
                        0   9.8 16
                        0  20.9 17
                        0  31.5 20
                        0  44.1 24];
                    params.Secteurs.LimitesAngulaires = [   -75.0 -35.5
                        -35.5 -23.0
                        -23.0 -15.6
                        -15.6  -5.0
                        -5.0   4.3
                        4.3  16.2
                        16.2  25.7
                        25.7  37.5
                        37.5  75.0];

                case 4 % Deep (138)
                    % params.Secteurs.Ordre = [1 3 5 7 9 8 6 4 2];
                    params.Secteurs.Ordre = 1:9;
                    params.Secteurs.Frequence = [31 32.5 34 32 33.5 30.5 33 31.5 30];

                    params.Secteurs.coefs = [	0 -63.4 25
                        0 -44.7 20
                        0 -30.5 18
                        0 -18.4 18
                        0   0.0 40
                        0  17.9 18
                        0  31.0 18
                        0  44.7 20
                        0  63.5 25];
                    params.Secteurs.LimitesAngulaires = [  -90.0 -53.0
                        -53.0 -37.5
                        -37.5 -24.8
                        -24.8 -11.5
                        -11.5  11.5
                        11.5  24.8
                        24.8  37.5
                        37.5  54.0
                        54.0  90.0];


                case {3;2;1} % Medium; Shallow; Very Shallow (137, 136, 135)
                    % params.Secteurs.Ordre = [1 3 2];    % A VERIFIER
                    params.Secteurs.Ordre = 1:3;
                    params.Secteurs.Frequence = [31.5 33 30];

                    params.Secteurs.coefs = [	0 -60.0 31
                        0   0.0 200
                        0  60.0 31];
                    params.Secteurs.LimitesAngulaires = [-90.0 -47.0
                        -47.0  45.5
                        45.5  90.0];

                otherwise
                    errordlg('Pas encore programme dans getParamsSounder')
            end
    end
end
%}
