% Identification du format de BSCorr (EM710 ou EM122) par la recherche de
% mots-cl�s d�signant les secteurs Babord, Centraux, Tribord.
%
% Syntax
%   [flag, identFormat] = identFormat_BSCorr(nomFic)
%
% Input Arguments
%   nomFic : nom du fichier de BSCorr
%
% Output Arguments
%   flag        : indicateur de bon fonctionnement
%   identFormat : 1=EM710, 2=EM122 or EM302, 3=EM304
%
% Examples
%    nomFic = getNomFicDatabase('BScorr_710.txt');
%    [flag, identFormat] = identFormat_BSCorr(nomFic);
%
% See also cl_simrad_all
% Authors : GLU (ATL)
% -------------------------------------------------------------------------

function [flag, identFormat] = identFormat_BSCorr(nomFic)

flag        = 0;
identFormat = 2;
numKeyword  = -1; % Index de recherche d'un des mots-cl�s.
motsCles    = {'Stb', 'Port', 'Starboard', 'Portboard', 'Cent', 'Central', 'Sector'};

if ~exist(nomFic, 'file') % Pour recherche bug fichier 0146 EM122 MAYOBS15
    fprintf('%s does not exist (in identFormat_BSCorr)\n', nomFic)
    return
end

fidIn = fopen(nomFic, 'r');

if fidIn == -1 % Pour recherche bug fichier 0146 EM122 MAYOBS15
    my_breakpoint
    fprintf('Cannot open %s (in identFormat_BSCorr) !!!\n', nomFic)
    return
end

while ~feof(fidIn)
    line = fgetl(fidIn);
    
    if contains(line, '# EM 304')
        identFormat = 3;
        break
    elseif contains(line, '# EM 2040')
        identFormat = 4;
        break
    end
    
    % Recherche de mots-cl�s pour d�terminer le format de fichier de diagramme
    if numKeyword == -1
        [flag, numKeyword] = detectKeywordInLine(motsCles, line);
        if flag && numKeyword > 0
            identFormat = 1; % 1 de type EM710, 2 pour les autres.
        end
    end
end
fclose(fidIn);
flag = 1;
