function Y = insertDataIntoVector(X, row, dataToInsert)

    % Creation d'un tableau de logique � la nouvelle taille.
    pppp       = false(numel(X)+numel(row),1);
    
    % Remplissage du tableau
    Y          = double(pppp);
    pppp(row)= true;
    Y(pppp)    = dataToInsert;
    Y(~pppp)   = X;
    
    clear pppp;

end

