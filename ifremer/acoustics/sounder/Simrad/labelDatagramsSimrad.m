% Labels des datagrams des sondeurs SIMRAD.
%
% Syntax
%   labelTypeDatagram = labelsDatagramsSimrad(...)
%
% Name-Value Pair Arguments
%   tabHistoTypeDatagram : Histogramme des types de datagram
%
% Name-Value Pair Arguments
%   /
%
% Output Arguments
%   texteTypeDatagram : Chaines de caracteres
%       48 (30h) = Installation parameter : Stop (0)
%       65 (41h) = Attitude (0)
%       67 (43h) = Clock (0)
%       68 (44h) = Depth (0)
%       69 (45h) = Single beam echo sounder depth (0)
%       70 (46h) = Raw range and beam angle (0)
%       71 (47h) = Surface sound speed ??? (0)
%       72 (48h) = Heading (0)
%       73 (49h) = Installation parameter : Start (0)
%       74 (4Ah) = Mechanical transducer tilt (0)
%       75 (4Bh) = Central beams echogram (0)
%       80 (50h) = Position (0)
%       82 (52h) = Runtime parameter (0)
%       83 (53h) = Seabed image (0)
%       84 (54h) = Tide (0)
%       85 (55h) = Sound speed profile (0)
%       87 (57h) = SSP Output (0)
%       104 (68h) = Height (0)
%       105 (69h) = Installation parameter : Stop (0)
%       112 (70h) = Installation parameter : Remote info (0)
%
% Examples
%   nomFic = getNomFicDatabase('EM1002_0001_20040916_054013_raw.all')
%   a = cl_simrad_all('nomFic', nomFic);
%   histo_index(a)
%   [texteTypeDatagram, Code, histoTypeDatagram] = histo_index(a)
%   labels = labelDatagramsSimrad(Code, histoTypeDatagram);
%
% See also Authors
% Authors : JMA, GLU
%-----------------------------------------------------------------------

function labelTypeDatagram = labelDatagramsSimrad(varargin)

[varargin, fullListe] = getPropertyValue(varargin, 'fullListe', 0);

LabelsTypeDatagram{49}  = 'PU Status';
LabelsTypeDatagram{68}  = 'Depth';
LabelsTypeDatagram{70}  = 'Raw range and beam angle (F)';
LabelsTypeDatagram{102} = 'Raw range and beam angle (f)';
LabelsTypeDatagram{83}  = 'Seabed image';
LabelsTypeDatagram{75}  = 'Central beams echogram';
LabelsTypeDatagram{80}  = 'Position';
LabelsTypeDatagram{104} = 'Height';
LabelsTypeDatagram{84}  = 'Tide';
LabelsTypeDatagram{65}  = 'Attitude';
LabelsTypeDatagram{72}  = 'Heading';
LabelsTypeDatagram{71}  = 'Surface sound speed ???';
LabelsTypeDatagram{74}  = 'Mechanical transducer tilt';
LabelsTypeDatagram{67}  = 'Clock';
LabelsTypeDatagram{85}  = 'Sound speed profile';
LabelsTypeDatagram{87}  = 'SSP Output';
LabelsTypeDatagram{69}  = 'Single beam echo sounder depth';
LabelsTypeDatagram{82}  = 'Runtime parameter';
LabelsTypeDatagram{73}  = 'Installation parameter : Start';
LabelsTypeDatagram{79}  = 'Ifremer Quality factor';
LabelsTypeDatagram{105} = 'Installation parameter : Stop';
LabelsTypeDatagram{48}  = 'Installation parameter : Stop';
LabelsTypeDatagram{112} = 'Installation parameter : Remote info';
LabelsTypeDatagram{107} = 'Water Column';
LabelsTypeDatagram{109} = 'Stave data ';
LabelsTypeDatagram{113} = 'Quality Factor';
LabelsTypeDatagram{114} = 'Mag&Phase';

% Nouveaux datagrams : version document Simrad 850-160692
LabelsTypeDatagram{78}  = 'Raw range and angle 78';
LabelsTypeDatagram{88}  = 'XYZ 88';
LabelsTypeDatagram{89}  = 'Seabed image data 89';
LabelsTypeDatagram{110} ='Network attitude velocity';

if ~isempty(varargin)
    code = varargin{1};
    tabHistoTypeDatagram = varargin{2};
else
    code = [];
    tabHistoTypeDatagram = zeros(size(LabelsTypeDatagram));
end

labelTypeDatagram = [];
for i=1:length(LabelsTypeDatagram)
    if ~isempty(LabelsTypeDatagram{i})
        sub = find(i == code);
        if ~isempty(sub)
            if tabHistoTypeDatagram(sub) ~= 0
                labelTypeDatagram{i}  = LabelsTypeDatagram{i};
            end
        end
    end
end
