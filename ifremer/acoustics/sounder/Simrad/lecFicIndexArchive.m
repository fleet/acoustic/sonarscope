% Lecture d'un fichier d'index d'ARCHIVE
%
% Syntax
%   [Date, Heure, Taille, Position, entete] = lecFicIndexArchive(nomFic)
%
% Input Arguments
%   nomFic	  : Nom du fichier d'index
%
% Output Arguments
%  []        : Auto-plot activation
%   Date	 : Date des enregistrements
%   Heure	 : Heure des enregistrements
%   Taille	 : Taille des enregistrements
%   Position : Position des enregistrements
%   entete   : entete du fichier
%
% Examples
%   nomFicIndexArchive = getNomFicDatabase('EM300_Archiv_ess300010.at')
%   lecFicIndexArchive(nomFicIndexArchive);
%   [Date, Heure, Taille, Position, entete] = lecFicIndexArchive(nomFicIndexArchive);
%
%   FigUtils.createSScFigure;
%   subplot(4,1,1); PlotUtils.createSScPlot(Date); grid on; title('Date');
%   subplot(4,1,2); PlotUtils.createSScPlot(Heure); grid on; title('Heure');
%   subplot(4,1,3); PlotUtils.createSScPlot(Taille); grid on; title('Taille');
%   subplot(4,1,4); PlotUtils.createSScPlot(Position); grid on; title('Position');
%
% See also creFicIndexArchive Authors
% Authors : JMA
%-----------------------------------------------------------------------

function varargout = lecFicIndexArchive(nomFic)

%% Décodage du fichier d'index

fid = fopen(nomFic, 'r', 'b');
entete = fscanf(fid, '%c', [1 75]);

fseek(fid, 76, 'bof');
pppp = fread(fid, [4 inf], 'uint32');
fclose(fid);

Date = pppp(1,:);
Heure = pppp(2,:);
Position = pppp(3,:);
Taille  = pppp(4,:);

if(nargout == 0)
	fprintf('Fichier : %s\n', nomFic);
	fprintf('Debut : %s  %s\n', dayIfr2str(Date(1)), hourIfr2str(Heure(1)));
	fprintf('Fin   : %s  %s\n', dayIfr2str(Date(end)), hourIfr2str(Heure(end)));
    
    FigUtils.createSScFigure;
    h(1) = subplot(4,1,1); PlotUtils.createSScPlot(Date); grid on; title('Date');
    h(2) = subplot(4,1,2); PlotUtils.createSScPlot(Heure); grid on; title('Heure');
    h(3) = subplot(4,1,3); PlotUtils.createSScPlot(Taille); grid on; title('Taille');
    h(4) = subplot(4,1,4); PlotUtils.createSScPlot(Position); grid on; title('Position');
    
    linkaxes(h, 'x'); axis tight; drawnow;

else
	varargout{1} = Date;
	varargout{2} = Heure;
	varargout{3} = Taille;
	varargout{4} = Position;
	varargout{5} = entete;
end
