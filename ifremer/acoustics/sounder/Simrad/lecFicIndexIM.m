% Lecture d'un fichier d'index ancien d'ARCHIVE
%
% Syntax
%   [Date, Heure, Taille, Position, entete] = lecFicIndexIM(nomFic)
%
% Input Arguments
%   nomFic	  : Nom du fichier d'index
%
% Output Arguments
%  []        : Auto-plot activation
%   Date	 : Date des enregistrements
%   Heure	 : Heure des enregistrements
%   Taille	 : Taille des enregistrements
%   Position : Position des enregistrements
%   entete   : entete du fichier
%
% Examples
%   nomFicIndexIM = 'Pas de fichier en archive pour le moment''
%   lecFicIndexIM(nomFicIndexIM);
%   [Date, Heure, Taille, Position, entete, Mode, Canal] = lecFicIndexIM(nomFicIndexIM);
%
%   subplot(6,1,1); plot(Date); grid on; title('Date');
%   subplot(6,1,2); plot(Heure); grid on; title('Heure');
%   subplot(6,1,3); plot(Taille); grid on; title('Taille');
%   subplot(6,1,4); plot(Position); grid on; title('Position');
%   subplot(6,1,5); plot(Mode); grid on; title('Mode');
%   subplot(6,1,6); plot(Canal); grid on; title('Canal');
%
% See also lecFicIndexIM Authors
% Authors : JMA
%-----------------------------------------------------------------------

function varargout = lecFicIndexIM(nomFic)

% ---------------------------
% Decodage du fichier d'index

fid = fopen(nomFic, 'r', 'b');
entete = fscanf(fid, '%c', [1 75]);

fseek(fid, 76, 'bof');
pppp = fread(fid, [5 inf], 'uint32');
fclose(fid);

Date = pppp(1,:);
Heure = pppp(2,:);
Position = pppp(3,:);
Taille  = pppp(4,:);

fid = fopen(nomFic, 'r', 'b');
fseek(fid, 76, 'bof');
pppp = fread(fid, [20 inf], 'uchar');
fclose(fid);
Canal  = pppp(17,:);
Mode   = pppp(18,:);


if (nargout == 0)
    fprintf('Fichier : %s\n', nomFic);
    fprintf('Debut : %s  %s\n', dayIfr2str(Date(1)), hourIfr2str(Heure(1)));
    fprintf('Fin   : %s  %s\n', dayIfr2str(Date(end)), hourIfr2str(Heure(end)));
else
    varargout{1} = Date;
    varargout{2} = Heure;
    varargout{3} = Taille;
    varargout{4} = Position;
    varargout{5} = entete;
    varargout{6} = Mode;
    varargout{7} = Canal;
end
