% Lecture d'un fichier de donnees brutes em300.
%
% Syntax
%   [PingCounter, Nstave, Nech, TXMode, Rawd, Tvgc, Tvgu, haut_do] = lecFicStaves( nomFic )
%
%
% Input Arguments
%   nomfic : Nom du fichier
%
% Output Arguments
%   Nstave  : Nombre de staves.
%   Nech    : Nombre d'echantillons temporels.
%   TXMode  : Mode du sondeur.
%   Rawd    : Donnees.
%   Tvgc    : Tvg.
%   Tvgu    : Tvg.
%   haut_do : Hauteur d'eau.
%
% Remarks : hauteur d'eau d�duite des pr�c�dents pings
%
% Examples
%   nomFic = getNomFicDatabase('EM300_Staves_st163820.00');
%   [PingCounter, Nstave, Nech, TXMode, Rawd, Tvgc, Tvgu] = lecFicStaves(nomFic);
%
%   figure
%   subplot(3,1,1); plot(Tvgc); grid on; set(gca, 'XLim', [1 Nech]); title('Tvgc');
%   subplot(3,1,2); plot(Tvgu); grid on; set(gca, 'XLim', [1 Nech]); title('Tvgu');
%   subplot(3,1,3); imagesc(abs(Rawd)); title('Rawd');
%
% See also Authors
% Authors : XL JMA
%-------------------------------------------------------------------------------

function [PingCounter, Nstave, Nech, TXMode, Rawd, Tvgc, Tvgu, haut_do] = lecFicStaves( nomFic )

% --------------------
% Ouverture du fichier

fid = fopen(nomFic, 'r', 'l');

% -------------------
% Lecture de l'entete

entete = fread(fid, 42, 'uint16');
Nstave = entete(39);
TXMode = entete(21);	% 0=verryShallow, 1=shallow, 2=medium, 3=deep, 4=veryDeep
PingCounter = entete(3);
haut_do = entete(7);

% ------------------------------------------------------------------------
% Lecture de tout le reste du fichier en une seule fois dans la variable x

[x, count] = fread(fid, inf, 'int16');

% --------------------
% Fermeture du fichier

fclose(fid);

% ----------------------------------------------
% Extraction des informations dans la variable x

% Calcul du nombre d'octets par echantillon sur l'ensemble de staves
NocPerSample = 3 + 2 * Nstave;

% Calcul du nombre d'echantillons.
Nech = floor(count / NocPerSample);

%  Le nombre n'etant pas une valeur entiere, on supprime la fin
x = x(1:Nech*NocPerSample);

% Redimentionnement de la variable x
x = reshape(x, [NocPerSample, Nech]);

% Extraction du vecteur Echcnt
Echcnt = x(1,:);

% La fin du vecteur est foireuse, on detecte la dernier valeur valide
index = find(gradient(Echcnt) ~= 1);

% On supprime les valeurs foireuses dans la variables x
x = x(:, 1:index(1));

% On reextrait les vecteurs Echcnt, Tvgc et Tvgu
Echcnt = x(1,:);
Tvgc = x(2,:);
Tvgu = x(3,:);

% On explicite le nombre d'echantillons
Nech = length(Echcnt);

% On supprime les valeurs de x correspondant aux vecteurs Echcnt, Tvgc et Tvgu
x = x(4:end,:);

% Redimentionnement de la variable x pour pouvoir la transformer facilement en complex
x = reshape(x, [2, Nstave, Nech]);

% Recuuperation des valeurs reelles et imaginaires et stockage en complex
Rawd = complex(x(1,:,:), x(2,:,:));

% Suppression de la premiere dimension qui est = 1
Rawd = reshape(Rawd, [Nstave, Nech]);



