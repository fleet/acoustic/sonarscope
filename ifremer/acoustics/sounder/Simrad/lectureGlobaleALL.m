function [flag, All, DataDepth, DataRaw, DataWC, DataSeabed, DataPosition, DataAttitude, DataRuntime, DataInstalParam, DataSSP, ...
    subDepth, subRaw, subSeabed, subWC, FlagPings, Latitude, Longitude, Heading, tWC] = lectureGlobaleALL(nomFic, IndNavUnit, varargin)

[varargin, subDepth] = getPropertyValue(varargin, 'subDepth', []); %#ok<ASGLU>

flag            = 0;
DataDepth       = [];
DataRaw         = [];
DataWC          = [];
DataSeabed      = [];
DataPosition    = [];
DataAttitude    = [];
DataRuntime     = [];
DataInstalParam = [];
DataSSP         = [];
Latitude        = [];
Longitude       = [];
Heading         = [];
tWC             = [];
FlagPings       = [];
subSeabed       = [];
subRaw          = [];
subWC           = [];

All = cl_simrad_all('nomFic', nomFic);
if isempty(All)
    return
end

%% Lecture des datagrams de WaterColumn

[flag, DataWC] = read_WaterColumn_bin(All, 'Memmapfile', -1);
if ~flag
    return
end
tWC = DataWC.Datetime;

%% Lecture des datagrammes Depth

[flag, DataDepth] = read_depth_bin(All, 'Memmapfile', - 1);
if ~flag
    return
end
% tDepth = DataDepth.Datetime;

%% Lecture des données RawRange

[flag, DataRaw] = read_rawRangeBeamAngle(All, 'Memmapfile', -1);
if ~flag
    return
end
% tRaw = DataDepth.Datetime;

%% DataSeabed

[flag, DataSeabed] = read_seabedImage_bin(All, 'Memmapfile', -1);
if ~flag
    return
end

%% Lecture des datagrams de navigation

[flag, DataPosition] = read_position_bin(All, 'IndNavUnit', IndNavUnit, 'Memmapfile', 0);
if ~flag || isempty(DataPosition) || isempty(DataPosition.Latitude)
    return
end
tNav = DataPosition.Datetime;

%% Lecture de l'attitude

[flag, DataInstalParam] = read_installationParameters_bin(All);
if ~flag
    return
end

%% Lecture de l'attitude

[flag, DataAttitude] = read_attitude_bin(All, 'DataInstalParam', DataInstalParam, 'Memmapfile', 0);
if ~flag || isempty(DataPosition) || isempty(DataPosition.Latitude)
    return
end

%% Lecture des datagrammes Runtime

[~, DataRuntime] = SSc_ReadDatagrams(nomFic, 'Ssc_Runtime', 'LogSilence', 0); % Non testé ici

%%

try
    [~, DataSSP] = SSc_ReadDatagrams(nomFic, 'Ssc_SoundSpeedProfile', 'Memmapfile', 0);
catch
    % Prévoir lecture SSP XML-Bin
end

%% Recherche des pings communs

if isempty(subDepth)
    subDepth = 1:size(DataDepth.PingCounter,1);
end

%     subDepth = 1:20;

P1 = DataDepth.PingCounter(subDepth,1);
P2 = DataRaw.PingCounter(:,1);
P3 = DataSeabed.PingCounter(:,1);
P4 = DataWC.PingCounter(:,1);

% P2 = P2(DataWC.FlagPings(:) == 0);

% [subP1, subWC, subRaw] = intersect3(P1, P2, P3);
[~, ~, subP1, subRaw, subSeabed, subWC] = lin_intersect(P1, P2, P3, P4);
if isempty(subWC)
    return
end
subDepth = subDepth(subP1);
tWC = tWC(subWC);

FlagPings = (DataWC.FlagPings(subWC) == 0);

%% Interpolation de la navigation aux instants des pings de WC

Heading   = my_interp1_longitude(tNav, DataPosition.Heading,   tWC, 'linear', 'extrap');
Longitude = my_interp1_longitude(tNav, DataPosition.Longitude, tWC, 'linear', 'extrap');
Latitude  = my_interp1(          tNav, DataPosition.Latitude,  tWC, 'linear', 'extrap');
if all(isnan(Longitude))
    return
end
