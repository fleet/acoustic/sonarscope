% Transformation du code Caraibes en numero de sondeur et de mode
%
% [SonarIdent, SonarMode_1, SonarMode_2, FactoryMode, SonarFamily] = modeCar2Sim(modeSim)
% 
% Name-Value Pair Arguments 
%   modeSim : Mode simrad
%
% Output Arguments 
%   SonarIdent  : Cf. cl_sounder
%   SonarMode_1 : Cf. cl_sounder
%   SonarMode_2 : Cf. cl_sounder
%   FactoryMode : Mode propre au constructeur pour le sondeur considere
%   SonarFamily : Cf. cl_sounder
% 
% Examples
%   [SonarIdent, SonarMode_1, SonarMode_2, FactoryMode, SonarFamily] = modeCar2Sim(101)
%   [SonarIdent, SonarMode_1, SonarMode_2, FactoryMode, SonarFamily] = modeCar2Sim(101:142)
%
% See also modeSim2Car cl_pames Authors
% Authors : JMA
% VERSION  : $Id
% -------------------------------------------------------------------------------

function [SonarIdent, SonarMode_1, SonarMode_2, FactoryMode, SonarFamily] = modeCar2Sim(modeSim)

PremiereFois = 1;
for i=1:length(modeSim)
    SonarIdent(i)  = 0; %#ok<AGROW>
    SonarMode_1(i) = 0; %#ok<AGROW>
    SonarMode_2(i) = 0; %#ok<AGROW>
    FactoryMode(i) = 0; %#ok<AGROW>
    P = [0 0 0 0];
    SonarFamily(i) = 2; %#ok<AGROW>
    if ~isnan(modeSim(i))
        switch modeSim(i)
            case 0
            case 103  % CIB_DOU_k_CONF_EM12D_SHALLOW_OLD
                P = [1 2 2 1];
            case 104  % CIB_DOU_k_CONF_EM12D_DEEP_OLD
                P = [1 1 6 2];
            case 114 % CIB_DOU_k_CONF_EM12D_ISO_ANG_SHALLOW/home/doppler/tmsias/cberron
                P = [1 2 2 1];
            case 115 % CIB_DOU_k_CONF_EM12D_ISO_ANG_DEEP
                P = [1 1 6 2];
            case 116 % CIB_DOU_k_CONF_EM12D_SHALLOW
                P = [1 2 1 3];
            case 117 % CIB_DOU_k_CONF_EM12D_150
                P = [1 1 1 4];
            case 118 % CIB_DOU_k_CONF_EM12D_140
                P = [1 1 2 5];
            case 119 % CIB_DOU_k_CONF_EM12D_128
                P = [1 1 3 6];
            case 120 % CIB_DOU_k_CONF_EM12D_114
                P = [1 1 4 7];
            case 121 % CIB_DOU_k_CONF_EM12D_98
                P = [1 1 5 8];

            case 101  % CIB_DOU_k_CONF_EM12S_SHALLOW_OLD
                P = [2 2 2 1];
            case 102  % CIB_DOU_k_CONF_EM12S_DEEP_OLD
                P = [2 1 4 2];
            case 108 % CIB_DOU_k_CONF_EM12S_ISO_ANG_SHALLOW
                P = [2 2 2 1];
            case 109 % CIB_DOU_k_CONF_EM12S_ISO_ANG_DEEP
                P = [2 1 4 2];
            case 110 % CIB_DOU_k_CONF_EM12S_SHALLOW
                P = [2 2 1 3];
            case 111 % CIB_DOU_k_CONF_EM12S_120
                P = [2 1 1 4];
            case 112 % CIB_DOU_k_CONF_EM12S_105
                P = [2 1 2 5];
            case 113 % CIB_DOU_k_CONF_EM12S_90
                P = [2 1 3 6];


                % ATTENTION FactoryMode semble changer de definition dans CIB_DOU_SimradSounder.h
                % -pour l'EM300 par rapport aux sutres : numerotation de 0 a 5
            case 135 % CIB_DOU_k_CONF_EM300_VERY_SHALLOW
                P = [3 1 1 1];
            case 136 % CIB_DOU_k_CONF_EM300_SHALLOW
                P = [3 2 1 2];
            case 137 % CIB_DOU_k_CONF_EM300_MEDIUM
                P = [3 3 1 3];
            case 138 % CIB_DOU_k_CONF_EM300_DEEP
                P = [3 4 1 4];
            case 139 % CIB_DOU_k_CONF_EM300_VERY_DEEP
                P = [3 5 1 5];
            case 140 % CIB_DOU_k_CONF_EM300_EXTRA_DEEP
                P = [3 6 1 6];

                % ATTENTION FactoryMode semble changer de definition dans CIB_DOU_SimradSounder.h
                % -pour l'EM1002 par rapport aux sutres : numerotation de 0 a 5
            case 143 % CIB_DOU_k_CONF_EM1002_VERY_SHALLOW
                P = [10 1 1 1];
            case 144 % CIB_DOU_k_CONF_EM1002_SHALLOW
                P = [10 2 1 2];
            case 145 % CIB_DOU_k_CONF_EM1002_MEDIUM
                P = [10 3 1 3];
            case 146 % CIB_DOU_k_CONF_EM1002_DEEP
                P = [10 4 1 4];
            case 147 % CIB_DOU_k_CONF_EM1002_VERY_DEEP
                P = [10 5 1 5];

                % ATTENTION FactoryMode semble changer de definition dans CIB_DOU_SimradSounder.h
                % -pour l'EM1002 par rapport aux sutres : numerotation de 0 a 5
            case 148 % CIB_DOU_k_CONF_EM120_VERY_SHALLOW
                P = [11 1 1 1];
            case 149 % CIB_DOU_k_CONF_EM120_SHALLOW
                P = [11 2 1 2];
            case 150 % CIB_DOU_k_CONF_EM120_MEDIUM
                P = [11 3 1 3];
            case 151 % CIB_DOU_k_CONF_EM120_DEEP
                P = [11 4 1 4];
            case 152 % CIB_DOU_k_CONF_EM120_VERY_DEEP
                P = [11 5 1 5];

            case 105 % CIB_DOU_k_CONF_EM1000_DEEP_OLD
                P = [4 1 1 0];
            case 106 % CIB_DOU_k_CONF_EM1000_MEDIUM_OLD
                P = [4 2 1 0];
            case 107 % CIB_DOU_k_CONF_EM1000_SHALLOW_OLD
                P = [4 3 1 0];
            case 122 % CIB_DOU_k_CONF_EM1000_ISO_ANG_60_2_MS_48_FAIS
                P = [4 3 4 1];
            case 123 % CIB_DOU_k_CONF_EM1000_ISO_ANG_120_07_MS_48_FAIS
                P = [4 3 5 2];
            case 124 % CIB_DOU_k_CONF_EM1000_ISO_ANG_150_02_MS_60_FAIS
                P = [4 3 8 3];
            case 125 % CIB_DOU_k_CONF_EM1000_CHANNEL_02_MS_60_FAIS
                P = [4 3 5 4];
            case 126 % CIB_DOU_k_CONF_EM1000_150_02_MS_60_FAIS
                P = [4 3 1 5];
            case 127 % CIB_DOU_k_CONF_EM1000_140_02_MS_60_FAIS
                P = [4 3 2 6];
            case 128 % CIB_DOU_k_CONF_EM1000_128_02_MS_60_FAIS
                P = [4 3 3 7];
            case 129 % CIB_DOU_k_CONF_EM1000_120_07_MS_48_FAIS
                P = [4 2 1 8];
            case 130 % CIB_DOU_k_CONF_EM1000_104_07_MS_48_FAIS
                P = [4 2 2 9];
            case 131 % CIB_DOU_k_CONF_EM1000_88_07_MS_48_FAIS
                P = [4 2 3 10];
            case 132 % CIB_DOU_k_CONF_EM1000_70_2_MS_48_FAIS
                P = [4 1 1 11];
            case 133 % CIB_DOU_k_CONF_EM1000_BERGE_BABORD_02_MS_60_FAIS
                P = [4 3 6 12];
            case 134 % CIB_DOU_k_CONF_EM1000_BERGE_TRIBORD_02_MS_60_FAIS
                P = [4 3 7 13];

                % ATTENTION FactoryMode semble changer de definition dans CIB_DOU_SimradSounder.h
                % -pour l'EM3000 par rapport aux sutres : numerotation de 0 a 5
            case 141 % CIB_DOU_k_CONF_EM3000_NEARFIELD
                P = [6 1 1 1];
            case 142 % CIB_DOU_k_CONF_EM3000_NORMAL
                P = [6 1 1 2];
                
                
                
            case 156 % CIB_DOU_k_CONF_EM710_VERY_SHALLOW
                P = [18 1 1 1]; % Compléter les modes %%%%%%%%%%%%%%%%%%
                
            case 157 % CIB_DOU_k_CONF_EM710_SHALLOW
                P = [18 2 1 1];
            case 158 % CIB_DOU_k_CONF_EM710_MEDIUM
                P = [18 3 1 1];
            case 159 % CIB_DOU_k_CONF_EM710_DEEP
                P = [18 4 1 1];
            case 160 % CIB_DOU_k_CONF_EM710_VERY_DEEP
                P = [18 5 1 1];
            case 161 % CIB_DOU_k_CONF_EM710_EXTRA_DEEP
                P = [18 6 1 1];
                
            case 162 % CIB_DOU_k_CONF_ME70_VERY_SHALLOW
                P = [17 1 1 1];
            case 163 % CIB_DOU_k_CONF_ME70_SHALLOW 
                P = [17 2 1 1];
            case 164 % CIB_DOU_k_CONF_ME70_MEDIUM 
                P = [17 3 1 1];
            case 165 % CIB_DOU_k_CONF_ME70_DEEP 
                P = [17 4 1 1];
            case 166 % CIB_DOU_k_CONF_ME70_VERY_DEEP
                P = [17 5 1 1];
            case 167 % CIB_DOU_k_CONF_ME70_EXTRA_DEEP
                P = [17 6 1 1];

            otherwise
                if PremiereFois
                    str = sprintf('Mode %d non connu', modeSim(i));
                    my_warndlg(['sounder/Simrad modeCar2Sim' str], 0, 'Tag', 'modeCar2SimNonConnu');
                    PremiereFois = 0;
                end
        end
        SonarIdent(i)  = P(1); %#ok<AGROW>
        SonarMode_1(i) = P(2); %#ok<AGROW>
        SonarMode_2(i) = P(3); %#ok<AGROW>
        FactoryMode(i) = P(4); %#ok<AGROW>
    end
end
SonarIdent  = SonarIdent';
SonarMode_1 = SonarMode_1';
SonarMode_2 = SonarMode_2';
FactoryMode = FactoryMode';
