% Transformation du code Caraibes en numero de sondeur et de mode
%
% [SonarIdent, SonarMode_1, SonarMode_2, FactoryMode, SonarFamily] = modeSim2Car(modeCar1)
% 
% Name-Value Pair Arguments 
%   modeCar1 : Mode simrad
%
% Output Arguments 
%   SonarIdent  : Cf. cl_sounder
%   SonarMode_1 : Cf. cl_sounder
%   SonarMode_2 : Cf. cl_sounder
%   FactoryMode : Mode propre au constructeur pour le sondeur considere
%   SonarFamily : Cf. cl_sounder
% 
% Examples
%   [SonarIdent, SonarMode_1, SonarMode_2, FactoryMode, SonarFamily] = modeSim2Car(101)
%   [SonarIdent, SonarMode_1, SonarMode_2, FactoryMode, SonarFamily] = modeSim2Car(101:142)
%
% See also modeCar2Sim cl_pames Authors
% Authors : JMA
% -------------------------------------------------------------------------------

function modeCar = modeSim2Car(SonarIdent, SonarMode_1, SonarMode_2)

modeCar = zeros(size(SonarMode_1));
for i=1:length(SonarMode_1)
    P = [SonarIdent, SonarMode_1(i), SonarMode_2(i)];
    if isequal(P, [1 2 2])
        modeCar1 = 103;
    elseif isequal(P, [1 1 6])
        modeCar1 = 104;
    % elseif isequal(P, [1 2 2])
    %     modeCar1 = 114;
    % elseif isequal(P, [1 1 6])
    %     modeCar1 = 115;
    elseif isequal(P,[1 2 1] )
        modeCar1 = 116;
    elseif isequal(P, [1 1 1])
        modeCar1 = 117;
    elseif isequal(P, [1 1 2])
        modeCar1 = 118;
    elseif isequal(P, [1 1 3])
        modeCar1 = 119;
    elseif isequal(P, [1 1 4])
        modeCar1 = 120;
    elseif isequal(P,[1 1 5] )
        modeCar1 = 121;
    elseif isequal(P, [2 2 2])
        modeCar1 = 101;
    elseif isequal(P, [2 1 4])
        modeCar1 = 102;
    % elseif isequal(P, [2 2 2])
    %     modeCar1 = 108;
    % elseif isequal(P, [2 1 4])
    %     modeCar1 = 109;
    elseif isequal(P, [2 2 1])
        modeCar1 = 110;
    elseif isequal(P, [2 1 1])
        modeCar1 = 111;
    elseif isequal(P, [2 1 2])
        modeCar1 = 112;
    elseif isequal(P, [2 1 3])
        modeCar1 = 113;
    elseif isequal(P, [3 1 1])
        modeCar1 = 135;
    elseif isequal(P, [3 2 1])
        modeCar1 = 136;
    elseif isequal(P, [3 3 1])
        modeCar1 = 137;
    elseif isequal(P, [3 4 1])
        modeCar1 = 138;
    elseif isequal(P, [3 5 1])
        modeCar1 = 139;
    elseif isequal(P, [3 6 1])
        modeCar1 = [3 6 1 6];
    elseif isequal(P, [10 1 1])
        modeCar1 = 143;
    elseif isequal(P, [10 2 1])
        modeCar1 = 144;
    elseif isequal(P, [10 3 1])
        modeCar1 = 145;
    elseif isequal(P, [10 4 1])
        modeCar1 = 146;
    elseif isequal(P, [10 5 1])
        modeCar1 =147 ;
    elseif isequal(P, [11 1 1])
        modeCar1 = 148;
    elseif isequal(P, [11 2 1])
        modeCar1 = 149;
    elseif isequal(P, [11 3 1])
        modeCar1 = 150;
    elseif isequal(P, [11 4 1])
        modeCar1 = 151;
    elseif isequal(P, [11 5 1])
        modeCar1 = 152;
    elseif isequal(P, [4 1 1])
        modeCar1 = 105;
    elseif isequal(P, [4 2 1])
        modeCar1 = 106;
    elseif isequal(P, [4 3 1])
        modeCar1 = 107;
    elseif isequal(P, [4 3 4])
        modeCar1 = 122;
    elseif isequal(P, [4 3 5])
        modeCar1 = 123;
    elseif isequal(P, [4 3 8])
        modeCar1 = 124;
    % elseif isequal(P, [4 3 5])
    %     modeCar1 = 125;
    % elseif isequal(P, [4 3 1])
    %     modeCar1 = 126;
    elseif isequal(P, [4 3 2])
        modeCar1 = 127;
    elseif isequal(P, [4 3 3])
        modeCar1 = 128;
    % elseif isequal(P, [4 2 1])
    %     modeCar1 = 129;
    elseif isequal(P, [4 2 2])
        modeCar1 = 130;
    elseif isequal(P, [4 2 3])
        modeCar1 = 131;
    % elseif isequal(P, [4 1 1])
    %     modeCar1 = 132;
    elseif isequal(P, [4 3 6])
        modeCar1 = 133;
    elseif isequal(P, [4 3 7])
        modeCar1 = 134;
    elseif isequal(P, [6 1 1])
        modeCar1 = 141;
    % elseif isequal(P, [6 1 1])
    %     modeCar1 = 142;
        
    elseif isequal(P, [18 1 1 1])
        modeCar1 = 156;
    elseif isequal(P, [18 2 1 1])
        modeCar1 = 157;
    elseif isequal(P, [18 3 1 1])
        modeCar1 = 158;
    elseif isequal(P, [18 4 1 1])
        modeCar1 = 159;
    elseif isequal(P, [18 5 1 1])
        modeCar1 = 160;
    elseif isequal(P, [18 6 1 1])
        modeCar1 = 161;

    elseif isequal(P, [17 1 1 1])
        modeCar1 = 162;
    elseif isequal(P, [17 2 1 1])
        modeCar1 = 163;
    elseif isequal(P, [17 3 1 1])
        modeCar1 = 164;
    elseif isequal(P, [17 4 1 1])
        modeCar1 = 165;
    elseif isequal(P, [17 5 1 1])
        modeCar1 = 166;
    elseif isequal(P, [17 6 1 1])
        modeCar1 = 167;

    else
        str = sprintf('Mode %s non reconnu', num2strCode(P));
        my_warndlg(str, 1);
        modeCar1 = 0;
    end
    modeCar(i) = modeCar1;
end
