function R = modifyRForSpecularRestorationForEM3000D(R, EmModel)

switch EmModel
    case 3003
        fBab = 13956;
        ftri = 14621;
    case 3004
        fBab = 14293;
        ftri = 14621;
    case 3005
        fBab = 13956;
        ftri = 14293;
    case 3006
        fBab = 14621;
        ftri = 14293;
    case 3007
        fBab = 14293;
        ftri = 13956;
    case 3008
        fBab = 14621;
        ftri = 13956;
    otherwise
        return
end

subBab= 1:length(R)/2;
coef = fBab/ftri;
if coef > 1
    R(subBab) =  R(subBab) / coef;
else
    % Il faut comparer TVGN � R et d�cider quel c�t� il faut corriger
    my_warndlg('modifyRForSpecularRestorationForEM3000D : case that has never been found, plese send this file to sonarscope@ifremer.fr', 0, 'Tag', 'modifyRForSpecularRestorationForEM3000D');
end
