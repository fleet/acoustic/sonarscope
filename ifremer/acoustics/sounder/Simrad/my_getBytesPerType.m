% D�termination du nombre d'octets d'une type de donn�e scalaire.
%
% Syntax
%   n = my_getBytesPerType(type);
%
% Input Arguments
%   type : Type de la donn�e d'origine.
%
% Name-Value Pair Arguments
%   n : nombre d'octets du type.
%
% Examples
%   numbytes = my_getBytesPerType('int16');
%
%   X = 3;
%   [f, n] = my_getBytesPerType(class(X));
%
% See also Authors
% Authors : GLU
% VERSION  : $Id: my_getBytesPerType.m,v 1.0 2011/02/16 10:00:00 augustin Exp $
%-----------------------------------------------------------------------
function [flag, numbytes] = my_getBytesPerType(type)

flag        = 0;
numbytes    = 0;

switch (type)
    case {'uint8', 'int8'}
        numbytes = 1;
        
    case {'uint16', 'int16'}
        numbytes = 2;
        
    case {'uint32', 'int32', 'single'}
        numbytes = 4;
        
    case {'uint64', 'int64', 'double'}
        numbytes = 8;
        
    otherwise
        
        str = sprintf('%s:invalidInputType  : %s', mfilename, type);
        my_warndlg(str, type);
        flag = -1;
        
end
