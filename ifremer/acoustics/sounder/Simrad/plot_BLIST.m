% nomFicBLIST = 'C:\UNOLS\Healy\hly10tc-sample-1\HLY_noise_3.5_2jun10.txt';
% plot_BLIST(nomFicBLIST)

function plot_BLIST(nomFicBLIST)

fid = fopen(nomFicBLIST);
while 1
    tline = fgetl(fid);
    if ~ischar(tline)
        break
    end
    disp(tline)
end
fclose(fid);