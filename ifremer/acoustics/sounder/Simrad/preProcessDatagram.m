function [flag, DataOut] = preProcessDatagram(nomDatagram, DataIn, Head)


try
    
    flag = 0;
    
    switch nomDatagram
        case 'Depth44h'
            DataOut = DataIn;
            % Transcodage de la profondeur tenant compte la résolution.
            % !!!!!! Traiter le cas des sondeurs Babord et tribord !!!!!!!!!
            if Head.SystemSerialNumberSecondHead == 0
                pppp                    = DataIn.Depth(:,:) / (DataIn.ZResol(1) * 0.01);
                DataOut.Depth(:,:)      = -pppp;
                
                % !!!!! A traiter Ping par Ping
                if size(DataIn.XYResol,2) == 1
                    DataOut.AlongDist(:,:)  = DataIn.AlongDist(:,:) / (DataIn.XYResol(1) * 0.01);
                    DataOut.AcrossDist(:,:) = DataIn.AcrossDist(:,:) / (DataIn.XYResol(1) * 0.01);
                else
                    nbColumns = size(DataIn.AlongDist,2);
                    ns2 = nbColumns / 2;
                    subBab = 1:ns2;
                    subTri = ns2+1:nbColumns;
                    for i=1:size(DataIn.XYResol,1)
                        DataOut.AlongDist(i,subBab) = DataIn.AlongDist(i,subBab) / DataIn.XYResol(i,1);
                        DataOut.AlongDist(i,subTri) = DataIn.AlongDist(i,subTri) / DataIn.XYResol(i,2);
                        DataOut.AcrossDist(i,subBab) = DataIn.AcrossDist(i,subBab) / DataIn.XYResol(i,1);
                        DataOut.AcrossDist(i,subTri) = DataIn.AcrossDist(i,subTri) / DataIn.XYResol(i,2);
                    end
                end
            else
                strFR = 'Traiter - distinguer le cas des sondeurs Babord et Tribord';
                my_warndlg(strFR, 1);
            end
        otherwise
            DataOut = DataIn;
    end
    
    flag = 1;
    
catch %#ok<CTCH>
    flag = 0;
    return
end

