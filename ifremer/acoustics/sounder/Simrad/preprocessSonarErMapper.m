% Pretraitement des fichiers .ers de Simrad.
% Cette operation n'est pas necessaire mais on gagnera du temps lors des
% premieres lectures de fichier
%
% fig = preprocessSonarErMapper(...)
%
% Name-Value Pair Arguments
%   nom : Nom d'un repertoire contenant des .ers ou nom d'un fichier .ers
%   ou tableau de cellules contenant des noms de fichiers .ers
%
% Name-Value Pair Arguments
%   Option        : {'All of them'} | 'Index' | 'Position' | 'Runtime' | 'Attitude'
%                   | 'Clock' | 'SoundSpeedProfile' | 'SurfaceSoundSpeed' | 'Height'
%
% Output Arguments
%   fig : Numero de la figure de la navigation si navigation demandee
%
% Examples
%   preprocessSonarErMapper
%
%   nomDir = '/home2/rayleigh/calimero/EM300'
%   liste = listeFicOnDir2(nomDir, '.ers')
%   preprocessSonarErMapper(liste{1})
%   preprocessSonarErMapper(liste{1:3})
%   preprocessSonarErMapper(liste)
%
%   preprocessSonarErMapper(nomDir)
%
% See also Ex-cl_simrad_all_01 cl_simrad_all Authors
% Authors : JMA
%------------------------------------------------------------------------------------------

function Fig1 = preprocessSonarErMapper(varargin)

I0 = cl_image_I0;

[varargin, Option] = getPropertyValue(varargin, 'Option', 'Allofthem');

if isempty(varargin)
    [flag, listeNomFic] = my_uigetfile('*.ers', 'MultiSelect','on');
    if ~flag
        return
    end
    if ~iscell(listeNomFic)
        listeNomFic = {listeNomFic};
    end
else
    if iscell(varargin{1})
        listeNomFic = varargin{:};
    else
        if isfolder(varargin{1})
            listeNomFic = listeFicOnDir2(varargin{1}, '.ers');
        else
            listeNomFic = {varargin{:}};
        end
    end
end

NbFic = length(listeNomFic);
str1 = 'Inspection des fichiers';
str2 = 'Checking the files';
hw = create_waitbar(Lang(str1,str2), 'N', NbFic);
for k=1:NbFic
    my_waitbar(k, NbFic, hw);
    [~, ~, Ext] = fileparts(listeNomFic{k});
    switch Ext
        case '.ers'
            [flag, b] = cl_image.import_ermapper(listeNomFic{k});
        case '.xml'
            [flag, b] = cl_image.import_xml(listeNomFic{k});
    end
    if ~flag
        return
    end
    b = Ram2Virtualmemory(b);
    a(k) = b; %#ok<AGROW>
    %     a(k) = cl_ermapper_ers('FicIn', listeNomFic{k}); %#ok
end
my_close(hw, 'MsgEnd');

if strcmp(Option, 'All of them') || strcmp(Option, 'Position')
    Fig1 = plot_position(a);
end

