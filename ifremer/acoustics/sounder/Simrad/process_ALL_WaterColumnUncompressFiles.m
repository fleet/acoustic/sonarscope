function process_ALL_WaterColumnUncompressFiles(ListeFic, DeleteFile)

for k=1:length(ListeFic)
    nomDir = fileparts(ListeFic{k});
    unzip(ListeFic{k}, nomDir)
    if DeleteFile
        delete(ListeFic{k})
    end
end

