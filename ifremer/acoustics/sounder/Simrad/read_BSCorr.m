% Lecture des donn�es de gain de diagramme d'antenne pour un diagramme de
% type Spline ou Polynomial (Type EM710 ou EM122).
%
% Syntax
%   [flag, BSCorr] = read_BSCorr(nomFic)
%
% Input Arguments
%   nomFic : nom du fichier de diagramme d'antenne
%
% Name-Value Pair Arguments
%   Sans objet.
%
% Output Arguments
%   flag   : indicateur de bon fonctionnement.
%   BSCorr : structure de r�sultat contenant par Mode les Noeuds de correction.
%
% Examples
%   FileNameSsc    = getNomFicDatabase('BSCorr_710.txt');
%   FileNameSsc    = getNomFicDatabase('BSCorr_710-SHOM.txt');
%   [flag, BSCorr] = read_BSCorr(FileNameSsc);
%   BSCorr
%   BSCorr(1)
%   BSCorr(1).Sector
%   BSCorr(1).Sector(1)
%   BSCorr(1).Sector(1).Node
%
%   FileNameSsc = getNomFicDatabase('BSCorr_304.txt');
%   [flag, BSCorr] = read_BSCorr(FileNameSsc);
%   BSCorr
%   BSCorr(1)
%   BSCorr(1).Sector
%   BSCorr(1).Sector(1)
%   BSCorr(1).Sector(1).Node
%
% See also cl_simrad_all
% Authors : GLU
% ----------------------------------------------------------------------------

function [flag, BSCorr, BSCorrInfo] = read_BSCorr(nomFic, varargin)

[varargin, ImportCompensationCurves] = getPropertyValue(varargin, 'ImportCompensationCurves', 0); %#ok<ASGLU>

flag       = 0; %#ok<NASGU>
BSCorr     = [];
BSCorrInfo = [];

% Lecture du fichier de type BSCorr
% Suppression des lignes vides au pr�alable de tout traitement.
eraseEmptyLinesInTxtFile(nomFic);

% Identification du format de diagramme de BSCorr.
[flag, identFormat] = identFormat_BSCorr(nomFic);
if ~flag
    str1 = 'Probl�me d''identification du format de diagramme BScorr';
    str2 = 'Problem in format identification for BScorr file';
    my_warndlg(Lang(str1,str2), 0);
    return
end

switch identFormat
    case 1 % Cas de l'EM710
%         [flag, BSCorr] = read_BSCorr_Spline(nomFic); %#ok<ASGLU>
        [flag, BSCorr] = read_BSCorr_SplineNEW(nomFic); %#ok<ASGLU> % Modif JMA le 10/03/2022 pour fichiers .all BS corrig�e de Kongsberg
    case 3 % Cas de l'EM304
        [flag, BSCorr] = read_BSCorr_SplineEM304(nomFic); %#ok<ASGLU>
    case 4 % Cas de l'EM2040
        [flag, BSCorr] = read_BSCorr_SplineEM2040(nomFic); %#ok<ASGLU>
    case 2 % Cas des EM122 et EM302
        [flag, BSCorr] = read_BSCorr_Poly(nomFic);
        if ~flag
            return
        end
        if ImportCompensationCurves
            nomFicSIS = my_tempname('.txt');
            flag = BSCorr_extractOldValues(nomFic, nomFicSIS);
            if flag
                [flag, BSCorrSIS] = read_BSCorr_Poly(nomFicSIS);
                if flag && ~isempty(BSCorrSIS)
                    for k=1:numel(BSCorr)
                        for iSector=1:BSCorr(k).Sector.nbNodes
                            if isfield(BSCorrSIS(k).Sector, 'SL')
                                BSCorr(k).Sector.SL(iSector) = BSCorr(k).Sector.SL(iSector) - BSCorrSIS(k).Sector.SL(iSector);
                            end
                        end
                    end
                end
            end
        end
end

% Identification des formats lus. Ceux reconnus sont ceux :
% - de la s�rie des EM 710 ou  2040, conformes � la doc de Kongsberg 160692aq_em_datagram_formats.pdf
% - de la s�rie des EM 122, fichier ASCII (dans d�finition de secteurs).
BSCorrInfo.identFormat   = identFormat;
BSCorrInfo.strFormat     = {'EM710'; 'EM122'; 'EM304'; 'EM2040'};
BSCorrInfo.commentFormat = {Lang('S�rie EM710 - d�finis par PingMode-SwathMode et N Secteurs', ...
    'Series EM710 - defined by PingMode-SwathMode and N-Sectors'), ...
    Lang('S�rie EM122 - Mod�le de diagramme existant', ...
    'EM122 Series - Model existing diagram')};
flag = 1;
