% Lecture des donn�es de gain de diagramme d'antenne pour un diagramme de
% type Polynomial (Type EM122 ou EM302).
%  
% Syntax
%   flag, BSCorr] = read_BSCorr_Poly(nomFic)
% 
% Input Arguments 
%   nomFic : nom du fichier de diagramme d'antenne
%
% Name-Value Pair Arguments
%   Sans objet. 
%
% Output Arguments
%   flag   : indicateur de bon fonctionnement.
%   BSCorr : structure de r�sultat contenant par Mode les Noeuds de correction.
%            Chaque noeud contient un triplet Niveau de Signal-Angle-Gain.
% 
% Examples
%   FileNameSsc = getNomFicDatabase('bscorr_ORIGINAL_BACKUP_DONOT_MODIFY.txt');
%   eraseEmptyLinesInTxtFile(FileNameSsc);
%   [flag, BSCorr] = read_BSCorr_Poly(FileNameSsc);
%
% See also cl_simrad_all
% Authors : GLU
% ----------------------------------------------------------------------------

function [flag, BSCorr] = read_BSCorr_Poly(nomFic)

flag   = 0; %#ok<NASGU>
iMode  = 0;
iNode  = 1;
BSCorr = [];

% Les chaines sont d�finies pour rechercher l'occurrence de fa�on exacte et
% sans tenir compte de la casse.
strPingMode = {'Very shallow', 'Shallow', 'Medium', 'Deep', 'Very Deep', 'Extra Deep', 'Extreme Deep'};
% Equivalent au BSCorr EM710 : Single swath = 0, Dual Swath 1 = 1, Dual
% Swath 2 = 2.
strSwathMode = {'Single swath', 'Dual Swath 1', 'Dual Swath 2'};
numPingModeSave = -1;

fid = fopen(nomFic, 'r');
while ~feof(fid)
    % Gestion du caract�re de fin de bloc (Spare pr�sent ou non).
    line = fgetl(fid);
    if uint8(line(1)) == 0
        break
    end
    if (length(line) > 5) && strcmp(line(1:5), '#SIS ')
        break
    end
    
    % Rercherche du 1er mode
    while true
        line = fgetl(fid);
        if ~ischar(line)
            break
        end
        if (length(line) > 5) && strcmp(line(1:5), '#SIS ')
            break
        end
    
        % Recherche des chaines d'identification des modes.
        [flag, numPingMode] = detectKeywordInLine(strPingMode, line, 'charStart', '# ');
        if numPingMode > 0
            numPingModeSave = numPingMode;
            iMode           = iMode + 1;
            BSCorr(iMode).pingMode   = numPingMode; %#ok<AGROW>
            BSCorr(iMode).swathMode  = []; %#ok<AGROW>
            % Dans ce format, le nb de secteur = 1(???).
            BSCorr(iMode).nbSectors  = 1; %#ok<AGROW>
            iSector                  = 1;
            BSCorr(iMode).Sector(iSector).Label = line; %#ok<AGROW>
            break
        end
    end
    
    % Boucle sur le reste du fichier.
    while ~feof(fid)
        line = fgetl(fid);
        if (length(line) > 5) && strcmp(line(1:5), '#SIS ')
            break
        end
        % D�tection d'un nouveau Ping mode �ventuel.
        [flag, numPingMode] = detectKeywordInLine(strPingMode, line, 'charStart', '# ');
        if numPingMode > 0
            numPingModeSave = numPingMode;
        else
            % D�tection d'un nouveau Swath Mode �ventuel
            [flag, numSwathMode] = detectKeywordInLine(strSwathMode, line);
            if numSwathMode > 0
                iMode = iMode + 1;
                BSCorr(iMode).pingMode   = numPingModeSave;
                BSCorr(iMode).swathMode  = numSwathMode; %-1; %TODO : Pourquoi -1 ??? Attention : repporter l'op�ration dans write_BSCorr_Spline
                % Dans ce format, le nb de secteur = 1(???).
                BSCorr(iMode).nbSectors  = 1;
                BSCorr(iMode).Sector(iSector).Label = line;
                numSwathMode = -1; %#ok<NASGU>
                iNode        = 1;
            else
                % Exploitation de la ligne de valeurs tabul�es.
                C = textscan(line, '%f\t%f\t%f');
                if C{1} ~= '#'
                    BSCorr(iMode).Sector(iSector).SL(iNode) = C{1} / 100;
                    % Lecture en n�gatif de l'angle.
                    BSCorr(iMode).Sector(iSector).Node(iNode,:) = [-C{2} C{3}]/ 100;
                    iNode = iNode + 1;
                end
            end
            
        end
    end
    
end
fclose(fid);

% Remplissage a posteriori (plus confortable) du nb de points.
nbConf = numel(BSCorr);
if nbConf < 2
    flag = 0;
    return
end

for k=1:nbConf
    for iSector=1:BSCorr(k).nbSectors
        if isfield(BSCorr(k).Sector(iSector), 'Node')
            BSCorr(k).Sector(iSector).nbNodes = numel(BSCorr(k).Sector(iSector).SL); %#ok<AGROW>
        end
    end
end

flag = 1;
