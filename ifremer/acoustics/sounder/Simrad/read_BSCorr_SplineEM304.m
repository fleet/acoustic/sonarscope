% Lecture des donn�es de gain de diagramme d'antenne pour un diagramme de
% type Spline (Type EM710).
%  
% Syntax
%   [flag, BSCorr] = read_BSCorr_Spline(nomFic)
% 
% Input Arguments 
%   nomFic : nom du fichier de diagramme d'antenne
%
% Name-Value Pair Arguments
%   Sans objet. 
%
% Output Arguments
%   flag        : indicateur de bon fonctionnement.
%   BSCorr      : structure de r�sultat contenant par Mode les Noeuds de
%                 correction. Chaque noeud contient un couple Angle-Gain.
% 
% Examples
%   FileNameSsc    = getNomFicDatabase('BSCorr_304.txt');
%   [flag, BSCorr] = read_BSCorr(read_BSCorr_SplineEM304);
%   BSCorr
%   BSCorr(1)
%   BSCorr(1).Sector
%   BSCorr(1).Sector(1)
%   BSCorr(1).Sector(1).Node
%
% See also cl_simrad_all
% Authors : GLU
% ----------------------------------------------------------------------------

function [flag, BSCorr] = read_BSCorr_SplineEM304(nomFic)

flag  = 0; %#ok<NASGU>
fid   = fopen(nomFic, 'r');
iMode = 1;
% strPingMode = {'Very shallow', 'Shallow', 'Medium', 'Deep', 'Very Deep', 'Extra Deep', 'Extreme Deep'};

while ~feof(fid)
    line = fgetl(fid);
    
    if isempty(line)
        break
    end
    
    if strcmp(line(1), '#')
        continue
    end
    
    X = sscanf(line, '%f');
    BSCorr(iMode).pingMode   = X(1); %#ok<AGROW>
    BSCorr(iMode).swathMode  = X(2) + 1; %#ok<AGROW> % +1 car Kongsberg num�rote de 0 � 1 et SSc 1 � 2
    BSCorr(iMode).nbSectors  = X(3); %#ok<AGROW>
    
    % Lecture de lignes fictives (??)
%     line = fgetl(fid);

    for iSector=1:BSCorr(iMode).nbSectors
        line = fgetl(fid);
        BSCorr(iMode).Sector(iSector).Label = line;
        
%         line = fgetl(fid);
%         BSCorr(iMode).Sector(iSector).SL = str2double(line);
        
        line = fgetl(fid);
        BSCorr(iMode).Sector(iSector).nbNodes = str2double(line);
        
        for iNode=1:BSCorr(iMode).Sector(iSector).nbNodes
            line = fgetl(fid);
            X = sscanf(line, '%f');
            % Lecture en n�gatif de l'angle.
            BSCorr(iMode).Sector(iSector).Node(iNode,:) = [-X(1) X(2)];
        end
    end
    iMode = iMode + 1;
end
fclose(fid);

flag = 1;
