% nomFic = 'D:\Temp\VLIZ\Lifewatch(19-740)_22242018_EM2040d\0013_20191022_173330_SimonStevin.all';
% aV2c = cl_simrad_all('nomFic', nomFic);
%
% [flag, Data1] = read_FileIndexWCD_XMLBin(nomFic)
% [flag, Data2] = SSc_ReadDatagrams(nomFic, 'WCD_FileIndex')

function [flag, Data] = read_FileIndexWCD_XMLBin(nomFic)

%% Lecture du fichier XML d�crivant la donn�e 

[nomDirRacine, nomFicSeul] = fileparts(nomFic);
nomFicXml = fullfile(nomDirRacine, 'SonarScope', nomFicSeul, 'WCD_FileIndex.xml');
flag = exist(nomFicXml, 'file');
if ~flag
    Data = [];
    return
end
% edit(nomFicXml)
Datagrams = xml_mat_read(nomFicXml);

%% Lecture des signaux

[nomDirSignal, nomFicSignal] = fileparts(nomFicXml);
nomDirSignal = fullfile(nomDirSignal, nomFicSignal);

[flag, Date] = Read_BinVariable(Datagrams, nomDirSignal, 'Date', 'double');
if ~flag
    Data = [];
    return
end
% figure; plot(Date); grid on; title('Date');

[flag, NbMilliSecs] = Read_BinVariable(Datagrams, nomDirSignal, 'NbMilliSecs', 'double');
if ~flag
    Data = [];
    return
end
% figure; plot(NbMilliSecs); grid on; title('NbMilliSecs');


%% TypeOfDatagram

[flag, Data.TypeOfDatagram] = Read_BinVariable(Datagrams, nomDirSignal, 'TypeOfDatagram', 'single');
if ~flag
    return
end
% figure; plot(Data.TypeOfDatagram, '*'); grid on; title('TypeOfDatagram')

%% Lecture de PingCounter

[flag, Data.PingCounter] = Read_BinVariable(Datagrams, nomDirSignal, 'PingCounter', 'single');
if ~flag
    return
end
% figure; plot(Data.PingCounter); grid on; title('PingCounter')

%% Lecture de SystemSerialNumber

[flag, Data.SystemSerialNumber] = Read_BinVariable(Datagrams, nomDirSignal, 'SystemSerialNumber', 'single');
if ~flag
    return
end
% figure; plot(Data.SystemSerialNumber, '*'); grid on; title('SystemSerialNumber')

%% Lecture du DatagramLength

[flag, Data.DatagramLength] = Read_BinVariable(Datagrams, nomDirSignal, 'DatagramLength', 'single');
if ~flag
    return
end
% figure; plot(Data.DatagramLength); grid on; title('DatagramLength')

% % En attendant que DatagramPosition soit corrig�
% 
Data.DatagramLength = Data.DatagramLength + 4;
% DatagramLength = double(Data.DatagramLength);
% DatagramPosition = cumsum(DatagramLength);
% Data.DatagramPosition = [0; DatagramPosition(1:end-1)];


%% Lecture du DatagramPosition

[flag, Data.DatagramPosition] = Read_BinVariable(Datagrams, nomDirSignal, 'DatagramPosition', 'double');
if ~flag
    return
end
% figure; plot(Data.DatagramPosition); grid on; title('DatagramPosition')

% Data.DatagramPosition = [0; Data.DatagramPosition];
% DatagramPosition = [Data.DatagramPosition; sizeFic(nomFic)];
% Data.DatagramLength = diff(DatagramPosition);

%% Calcul des dates et heures

Jour  = mod(Date,100);
Date  = (Date - Jour) / 100;
Mois  = mod(Date,100);
Annee = (Date - Mois) / 100;
Date = dayJma2Ifr(Jour, Mois, Annee);
Data.Time = cl_time('timeIfr', Date, NbMilliSecs);

%% Pr�paration remplacement cl_time par datetime

Data.Datetime = datetime(Data.Time.timeMat, 'ConvertFrom', 'datenum'); % TODO : t = datetime(Annee, Mois, Jour, 0, 0, 0 , NbMilliSecs)
