function [flag, Data] = read_FileIndex_Netcdf(nomFic, varargin)

[~, ~, ext] = fileparts(nomFic);
switch ext
    case {'.all'; '.kmall'}
        [flag, Data] = SSc_ReadDatagrams(nomFic, 'FileIndex', varargin{:});
    case '.wcd'
        [flag, Data] = SSc_ReadDatagrams(nomFic, 'FileIndexWCD', varargin{:});
end

if ~flag
    return
end
