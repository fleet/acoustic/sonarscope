function [flag, Data, XML] = read_FileIndex_XMLBin(nomFic, varargin)
 
XML = [];

%% Lecture du fichier XML d�crivant la donn�e

[nomDirRacine, nomFicSeul, ext] = fileparts(nomFic);
if strcmp(ext, '.all')
    nomFicXml = fullfile(nomDirRacine, 'SonarScope', nomFicSeul, 'ALL_FileIndex.xml');
else
    nomFicXml = fullfile(nomDirRacine, 'SonarScope', nomFicSeul, 'WCD_FileIndex.xml');
end
flag = exist(nomFicXml, 'file');
if ~flag
%     special_message('FichierNonExistant', nomFicXml)
    Data = [];
    return
end
% edit(nomFicXml)
XML = xml_read(nomFicXml);

%% Lecture des signaux

[nomDirSignal, nomFicSignal] = fileparts(nomFicXml);
nomDirSignal = fullfile(nomDirSignal, nomFicSignal);

[flag, Date] = Read_BinVariable(XML, nomDirSignal, 'Date', 'double');
if ~flag
    Data = [];
    return
end
% figure; plot(Date); grid on; title('Date');

[flag, NbMilliSecs] = Read_BinVariable(XML, nomDirSignal, 'NbMilliSecs', 'double');
if ~flag
    Data = [];
    return
end
% figure; plot(NbMilliSecs); grid on; title('NbMilliSecs');

%% TypeOfDatagram

[flag, Data.TypeOfDatagram] = Read_BinVariable(XML, nomDirSignal, 'TypeOfDatagram', 'single');
if ~flag
    Data = [];
    return
end
% figure; plot(Data.TypeOfDatagram, '*'); grid on; title('TypeOfDatagram')

%% Lecture de PingCounter

[flag, Data.PingCounter] = Read_BinVariable(XML, nomDirSignal, 'PingCounter', 'single');
if ~flag
    Data = [];
    return
end
% figure; plot(Data.PingCounter); grid on; title('PingCounter')

%% Lecture de SystemSerialNumber

[flag, Data.SystemSerialNumber] = Read_BinVariable(XML, nomDirSignal, 'SystemSerialNumber', 'single');
if ~flag
    Data = [];
    return
end
% figure; plot(Data.SystemSerialNumber, '*'); grid on; title('SystemSerialNumber')

%% Lecture du DatagramLength

[flag, Data.DatagramLength] = Read_BinVariable(XML, nomDirSignal, 'DatagramLength', 'single');
if ~flag
    Data = [];
    return
end
% figure; plot(Data.DatagramLength); grid on; title('DatagramLength')

% % En attendant que DatagramPosition soit corrig�
%
Data.DatagramLength = Data.DatagramLength + 4;
% DatagramLength = double(Data.DatagramLength);
% DatagramPosition = cumsum(DatagramLength);
% Data.DatagramPosition = [0; DatagramPosition(1:end-1)];


%% Lecture du DatagramPosition

[flag, Data.DatagramPosition] = Read_BinVariable(XML, nomDirSignal, 'DatagramPosition', 'double');
if ~flag
    Data = [];
    return
end
% figure; plot(Data.DatagramPosition); grid on; title('DatagramPosition')

% Data.DatagramPosition = [0; Data.DatagramPosition];
% DatagramPosition = [Data.DatagramPosition; sizeFic(nomFic)];
% Data.DatagramLength = diff(DatagramPosition);

%% Calcul des dates et heures

Jour  = mod(Date,100);
Date  = (Date - Jour) / 100;
Mois  = mod(Date,100);
Annee = (Date - Mois) / 100;
Date = dayJma2Ifr(Jour, Mois, Annee);
Data.Time = cl_time('timeIfr', Date, NbMilliSecs);

%% Pr�paration remplacement cl_time par datetime

Data.Datetime = datetime(Data.Time.timeMat, 'ConvertFrom', 'datenum');
