function [flag, DataPing, b] = read_SimradRawData(filename)

DataPing = [];
flag = 0;

if ~exist(filename, 'file')
    b = [];
    return
end

[nomDir, nom] = fileparts(filename);

data = importdata(filename, ' ');
beamNb   = max(data(:,1)) + 1;
sampleNb = max(data(:,2)) + 1;
minSample = min(data(:,2)) + 1;

Amp(minSample:sampleNb,:) = reshape(data(:,3), sampleNb-minSample+1, beamNb);
Ph(minSample:sampleNb,:)  = reshape(data(:,4), sampleNb-minSample+1, beamNb) * (180/pi);

b(1) = cl_image('Image', Amp, 'Name', 'Amp',   'ColormapIndex',  3, 'TagSynchroX', nom, 'TagSynchroY', nom);
b(2) = cl_image('Image', Ph,  'Name', 'Phase', 'ColormapIndex', 17, 'TagSynchroX', nom, 'TagSynchroY', nom);
% SonarScope(b)

nomFic = fullfile(nomDir, ['Beam0Data' nom(8:end) '.txt']);
if ~exist(nomFic, 'file')
    b = [];
    return
end
data = importdata(nomFic, ' ');
% BeamNumber = data(:,3) + 1;
Angles     = -data(:,4) * (180/pi);
figure(2352); PlotUtils.createSScPlot(Angles, '*'); grid on;
figure(2352); PlotUtils.createSScPlot(data(:,9), 'ro'); grid on; title('KM Detection'); set(gca, 'YDir', 'Reverse');
hold on; PlotUtils.createSScPlot(data(:,19), 'k'); PlotUtils.createSScPlot(data(:,20), 'k'); PlotUtils.createSScPlot(data(:,5), 'k'); PlotUtils.createSScPlot(data(:,6), 'k')

%{
for i=1:size(data, 2)
    figure; PlotUtils.createSScPlot(data(:,i), '-*'); grid on; title(num2str(i))
end
%}

DataPing.TxAngle = Angles';% - (90-33);


flag = 1;