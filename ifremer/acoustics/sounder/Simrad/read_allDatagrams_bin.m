function [flag, aKM, DataDepth, DataRaw, DataWC, DataSeabed, DataPosition, DataAttitude, DataRuntime, DataInstalParam, DataSSP] ...
    = read_allDatagrams_bin(nomFic, IndNavUnit, varargin)

[varargin, flagDepth]       = getPropertyValue(varargin, 'flagDepth',       1);
[varargin, flagRaw]         = getPropertyValue(varargin, 'flagRaw',         1);
[varargin, flagWC]          = getPropertyValue(varargin, 'flagWC',          1);
[varargin, flagSeabed]      = getPropertyValue(varargin, 'flagSeabed',      1);
[varargin, flagPosition]    = getPropertyValue(varargin, 'flagPosition',    1);
[varargin, flagAttitude]    = getPropertyValue(varargin, 'flagAttitude',    1);
[varargin, flagRuntime]     = getPropertyValue(varargin, 'flagRuntime',     1);
[varargin, flagInstalParam] = getPropertyValue(varargin, 'flagInstalParam', 1);
[varargin, flagSSP]         = getPropertyValue(varargin, 'flagSSP',         1); %#ok<ASGLU>

flag            = 0;
DataDepth       = [];
DataRaw         = [];
DataWC          = [];
DataSeabed      = [];
DataPosition    = [];
DataAttitude    = [];
DataRuntime     = [];
DataInstalParam = [];
DataSSP         = [];

aKM = cl_simrad_all('nomFic', nomFic);
if isempty(aKM)
    return
end

%% Lecture des datagrammes Depth

if flagDepth
    [flag, DataDepth] = read_depth_bin(aKM, 'Memmapfile', - 1);
    if ~flag
        return
    end
end

%% Lecture des datagrams de WaterColumn

if flagWC
    [flag, DataWC] = read_WaterColumn_bin(aKM, 'Memmapfile', -1);
    if ~flag
        return
    end
end

%% Lecture des donn�es RawRange

if flagRaw
    [flag, DataRaw] = read_rawRangeBeamAngle(aKM, 'Memmapfile', -1);
    if ~flag
        return
    end
end

%% DataSeabed

if flagSeabed
    [flag, DataSeabed] = read_seabedImage_bin(aKM, 'Memmapfile', -1);
    if ~flag
        return
    end
end

%% Lecture des datagrams de navigation

if flagPosition
    [flag, DataPosition] = read_position_bin(aKM, 'IndNavUnit', IndNavUnit, 'Memmapfile', 0);
    if ~flag || isempty(DataPosition) || isempty(DataPosition.Latitude)
        return
    end
end

%% Lecture de l'attitude

if flagInstalParam
    [flag, DataInstalParam] = read_installationParameters_bin(aKM);
    if ~flag
        return
    end
end

%% Lecture de l'attitude

if flagAttitude
    [flag, DataAttitude] = read_attitude_bin(aKM, 'DataInstalParam', DataInstalParam, 'Memmapfile', 0);
    if ~flag || isempty(DataPosition) || isempty(DataPosition.Latitude)
        return
    end
end

%% Lecture des datagrammes Runtime

if flagRuntime
    [~, DataRuntime] = SSc_ReadDatagrams(nomFic, 'Ssc_Runtime', 'LogSilence', 0); % Non test� ici
end

%%

if flagSSP
    try
        [~, DataSSP] = SSc_ReadDatagrams(nomFic, 'Ssc_SoundSpeedProfile', 'Memmapfile', 0);
    catch
        % Pr�voir lecture SSP XML-Bin
    end
end
