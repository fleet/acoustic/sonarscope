% Correct PingCounter in case of dual sounder but only the starboard is on
% (correspondances between pings are calculated on indice=1 on second
% dimension : found on Mashkoor data : 0088_20070926_152707
function PingCounter = repairPingCounterInCaseDualSounderButOnlyStarnoardRunning(PingCounter)

if (size(PingCounter,2) == 2) && all(isnan(PingCounter(:,1)))
    PingCounter(:,1) = PingCounter(:,2);
end