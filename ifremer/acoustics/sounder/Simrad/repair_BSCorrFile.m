function flag = repair_BSCorrFile(nomFicIn)

if ischar(nomFicIn)
    nomFicIn = {nomFicIn};
end

for k=1:length(nomFicIn)
    flag = repair_BSCorrFile_unitaire(nomFicIn{k});
end
% nomFic = getNomFicDatabase('BSCorr_304.txt');
% [flag, BSCorr] = read_BSCorr(nomFic);
% if ~flag
%     return
% end
% 
% for k1=1:length(BSCorr)
%     for k2=1:length(BSCorr(k1).Sector)
%         BSCorr(k1).Sector(k2).Node(:,2) = 0;
%     end
% end
% 
% pathname = fileparts(nomFic);
% nomFicOut = fullfile(pathname, 'BSCorr_304_EmptyValues.txt');
% flag = write_BSCorr_Spline(BSCorr, nomFicOut, 'EM304');
%


function flag = repair_BSCorrFile_unitaire(nomFicIn)
    
flag = 1;

Tab = sprintf('\t');

fid1 = fopen(nomFicIn, 'rt');
[nomDir, nomFicSeul, ext] = fileparts(nomFicIn);
nomFicOut = fullfile(nomDir, [nomFicSeul '_repaired' ext]);
fid2 = fopen(nomFicOut, 'w+t');
if fid2 == -1
    flag = 0;
    messageErreurFichier(nomFicOut, 'WriteFailure');
    fclose(fid1);
    return
end

while 1
    tline = fgetl(fid1);
    if ~ischar(tline)
        break
    end
    tline = strtrim(tline);
    tline = strrep(tline, Tab, ' ');
    tline = strrep(tline, Tab, ' ');
    tline = strrep(tline, '  ', ' ');
    tline = strrep(tline, '  ', ' ');
    tline = strrep(tline, '  ', ' ');
    fprintf(fid2, '%s\n', tline);
end
fclose(fid1);
fclose(fid2);
