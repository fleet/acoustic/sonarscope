function simrad_saveNavigation(nomFic, Latitude, Longitude)

aKM = cl_simrad_all('nomFic', nomFic);
Data = read_position(aKM);

Data.Longitude = Longitude;
Data.Latitude  = Latitude;

%{
figure; plot(Data.Longitude); grid on;
figure; plot(Data.Latitude); grid on;
figure; plot(Data.Longitude, Data.Latitude, '-*'); grid on;
%}

[nomDir, nomFic] = fileparts(nomFic);
nomFicMat = fullfile(nomDir, 'SonarScope', [nomFic '_position.mat']);
save(nomFicMat, 'Data')
