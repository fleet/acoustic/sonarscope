function [flag, a, Carto] = sonar_SurveySimradDetectionPhaseAndAmplitude(nomFic, DisplayLevel, nomFicAvi, DepthMin, DepthMax, nomDirErs, BeamsToDisplay, varargin)

[varargin, Carto] = getPropertyValue(varargin, 'Carto', []); %#ok<ASGLU>

a = cl_image.empty;

if isempty(nomFicAvi)
    mov = [];
else
	mov  = VideoWriter(nomFicAvi,'Uncompressed AVI'); % FORGE 353
end

if ischar(nomFic)
    nomFic = {nomFic};
end
for kFig=1:length(nomFic)
    [flag, b, mov, Carto] = sonar_SurveySimradDetectionPhaseAndAmplitude_unitaire(nomFic{kFig}, DisplayLevel, mov, Carto, DepthMin, DepthMax, BeamsToDisplay);
    if ~flag
        return
    end
    
    % -----------------------------
    % Send images in virtual memory
    
    b = optimiseMemory(b, 'SizeMax', 0);
    
    % -----------------------------
    % Save images in ErMapper files
    
    a(:,kFig) = b';
    if ~isempty(nomDirErs)
        for k=1:length(b)
            NomImage = b(k).Name;
            NomDir = extract_ImageName(b(k));
            NomDir = NomDir(1:15);
            NomDir = fullfile(nomDirErs, NomDir);
            if k == 1
                if isfolder(NomDir)
                    try
                        rmdir(NomDir, 's');
                    catch %#ok<CTCH>
                        str = sprintf('%s could not be removed, check if any software is using a file of it.', NomDir);
                        my_warndlg(str, 0);
                    end
                end
                try
                    mkdir(NomDir);
                catch %#ok<CTCH>
                end
            end
            nomFicErs = fullfile(NomDir, NomImage);
            flag = export_ermapper(b(k), nomFicErs);
            
            if b(k).DataType == cl_image.indDataType('Bathymetry')
                NomFicAlgo = get_nomFicAlg(cl_ermapper_ers([]), nomFicErs, 'Shading');
                winopen(NomFicAlgo)
            end
        end
    end
end
a = a(:);

%% Close .avi file and display it

if ~isempty(nomFicAvi)
    close(mov);
    winopen(nomFicAvi)
end


function [flag, e, mov, Carto] = sonar_SurveySimradDetectionPhaseAndAmplitude_unitaire(nomFic, DisplayLevel, mov, Carto, DepthMin, DepthMax, BeamsToDisplay)

% Draught = get_DraughtShip;

e = cl_image.empty;

aKM = cl_simrad_all('nomFic', nomFic);

[flag, Data] = SSc_ReadDatagrams(nomFic, 'Ssc_RawBeamSamples', 'Memmapfile', -1);
if ~flag
    return
end

N = length(Data.PingCounter);
if N == 0
    return
end

DataNAV      = []; %read_navigation(aKM, identSondeur);
DataAttitude = []; %read_attitude(aKM, identSondeur);
Data7030     = []; %read_sonarInstallationParameters(aKM, identSondeur);

% RMax1_Precedent = [];

Compteur = [];
str1 = sprintf('D�tection du fond en cours sur "%s"', nomFic);
str2 = sprintf('Processing Bottom Detection on "%s"', nomFic);
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    
    iPing = k;
    
    [Compteur, strTFin] = estimation_temps_traitement(k, N, Compteur);
    fprintf('%5d/%5d : %s - %s\n', k, N, nomFic, strTFin);
    
    % Temporaire
    %     RMax1_Precedent = [];
    
    [b, Carto] = import_MagAndPhase_All(aKM, 'Carto', Carto, 'subPing', k, ...
        'DepthMin', DepthMin, 'DepthMax', DepthMax, 'DisplayLevel', DisplayLevel, 'Mute', 1);
    
    
    %     [b, Carto, RMax1_Precedent] = view_SimradRawData(nomFic{k}, 'subPing', iPing, 'Carto', Carto, ...
    %             'DepthMin',  DepthMin,  'DepthMax', DepthMax, ...
    %             'DisplayLevel', DisplayLevel, 'RMax1_Precedent', RMax1_Precedent, ...
    %             'Draught', Draught);
    if isempty(b)
        N = k - 1;
        break
    end
    
    if k == 1
        X = get(b(1), 'Sonar');
        SampleBeamData = X.SampleBeamData;
        %         if (SampleBeamData.SystemSerialNumber == 7150) && ...
        %                 (SampleBeamData.Frequency < 18000)
        %             [tetav, DifferentielAngles] = VraisAngles_7150_12kHz; %#ok<NASGU>
        %         else
        %             tetav = [];
        %         end
        
        nbBeams   = b(1).nbColumns;
        nbSamples = b(1).nbRows;
        SampleTime = (1:nbSamples) / double(SampleBeamData.SampleRate(1)); % TODO SampleRate(1)
        SampleTime = SampleTime / (3600*24);
        
        BeamsToDisplay = 1:nbBeams;
        
        Range               = NaN(N, nbBeams, 'single');
        AmpImageRange       = NaN(N, nbBeams, 'single');
        PhaseImageRange     = NaN(N, nbBeams, 'single');
        AmpImageQF          = NaN(N, nbBeams, 'single');
        AmpImageNbSamples   = NaN(N, nbBeams, 'single');
        AmpImageMax         = NaN(N, nbBeams, 'single');
        AmpImageQF2         = NaN(N, nbBeams, 'single');
        Angles              = NaN(N, nbBeams, 'single');
        PhaseImageQF        = NaN(N, nbBeams, 'single');
        PhaseImagePente     = NaN(N, nbBeams, 'single');
        PhaseImageNbSamples = NaN(N, nbBeams, 'single');
        PhaseImageEqm       = NaN(N, nbBeams, 'single');
        TypeDetection       = NaN(N, nbBeams, 'single');
        SyntheseQF          = NaN(N, nbBeams, 'single');
        Bathy               = NaN(N, nbBeams, 'single');
        AmpImageQF3         = zeros(N, nbBeams, 'single');
        % AcrossDistance      = NaN(N, nbBeams, 'single');
        
        DataDepth = Reson_BottomDetector_initDataDepth(N);
        Time         = NaN(1, N);
        DatetimePing = NaT(1, N);
        
        InitialFileName = b(1).InitialFileName;
        NomImage = extract_ImageName(b(1));
        [~, TagSynchro] = fileparts(InitialFileName);
        SonarDescription =  get(b(1), 'SonarDescription');
        identSondeur = get(SonarDescription, 'Sonar.SystemSerialNumber');
        TagSynchroX = [num2str(identSondeur(1)) '_' TagSynchro '_' num2str(rand(1))];
        TagSynchroY = TagSynchroX;
        InitialFileFormat = b(1).InitialFileFormat;
        GeometryType = cl_image.indGeometryType('PingBeam');
        DataType = cl_image.indDataType('RayPathSampleNb');
        % nbCol = b(1).nbColumns;
        InitialImageName = b(1).InitialImageName;
        
        drawnow
    end
    Amp   = b(1);
    Phase = b(2);
    clear b
    
    %% BDA sur un ping
    % Modifi� par JMA le 26/08/2014
    % TODO : pas test� depuis que cette modif est faite
    AmpImage   = get(Amp, 'Image');
    PhaseImage = get(Phase, 'Image');
    
    DataPing = get(Amp, 'SampleBeamData');
    SonarDescription = get_SonarDescription(Amp);
    Frequency = get(SonarDescription, 'Sonar.Freq');
    BeamAngles = DataPing.TxAngle;
    
    TxPulseWidth       = DataPing.TxPulseWidth; % s
    SampleRate         = DataPing.SampleRate; % Hz
    ReceiveBeamWidth   = DataPing.ReceiveBeamWidth * (180/pi); % deg
    SystemSerialNumber = DataPing.SystemSerialNumber;
    % SoundVelocity     = DataPing.SoundVelocity;
    % MultiPingSequence = DataPing.MultiPingSequence;
    
    if isfield(DataPing, 'ProjectionBeam_3dB_WidthVertical')
        ProjectionBeam_3dB_WidthVertical = DataPing.ProjectionBeam_3dB_WidthVertical;
    else
        ProjectionBeam_3dB_WidthVertical = [];
    end
    if isfield(DataPing, 'BottomDetectDepthFilterFlag')
        BottomDetectDepthFilterFlag       = DataPing.BottomDetectDepthFilterFlag;
        BottomDetectionFilterInfoMinDepth = DataPing.BottomDetectionFilterInfoMinDepth;
        BottomDetectionFilterInfoMaxDepth = DataPing.BottomDetectionFilterInfoMaxDepth;
    else
        BottomDetectDepthFilterFlag       = 0;
        BottomDetectionFilterInfoMinDepth = [];
        BottomDetectionFilterInfoMaxDepth = [];
    end
    if isfield(DataPing, 'PingSequence')
        PingSequence = DataPing.PingSequence;
    else
        PingSequence = 1;
    end
    
    GateDepth.Flag = 1; % TODO : r�paration
    Draught = 0;
    
    [DepthMinPing, DepthMaxPing] = BDA_gates(GateDepth, BottomDetectDepthFilterFlag, BottomDetectionFilterInfoMinDepth, BottomDetectionFilterInfoMaxDepth, Draught);
    
    % R�parations de fortune le 18/07/2019
    IdentAlgo     = 1;
    DepthPingPrev = [];
    
    [PingRange, TD, QualityFactor, AmpPingRange, PhasePingRange, AmpPingQF, PhasePingQF, ...
        AmpPingSamplesNb, AmpPingQF2, PhasePingNbSamples, SampleRate, BeamAngles, iBeamBeg, iBeamEnd, SystemSerialNumber, ...
        AmpPingNormHorzMax, PhasePingPente, PhasePingEqm] = ...
        sonar_BDA(...
        AmpImage, PhaseImage, DepthMinPing, DepthMaxPing, BeamAngles, ...
        Frequency, SampleRate, TxPulseWidth, ReceiveBeamWidth, SystemSerialNumber, ...
        IdentAlgo, DisplayLevel, iPing, PingSequence, BeamsToDisplay, ...
        ProjectionBeam_3dB_WidthVertical, DepthPingPrev, 'EmModel', DataPing.EmModel);
    % TODO : appeler sonar_BDA_reson
    
    %% Display results
    
    if DisplayLevel == 2
        figure(12321);
        h(1) = subplot(4,1,1); plot(AmpPingRange, 'k'); hold on; grid on;
        PlotUtils.createSScPlot(PhasePingRange, 'r'); set(h(1), 'YDir', 'Reverse');
        set(h(1), 'YLim', [0 nbSamples]); title('Range'); hold off;
        
        h(2) = subplot(4,1,2);
        Coef = 1500 / (2 * SampleRate(1)); % TODO SampleRate(1)
        PlotUtils.createSScPlot(-(AmpPingRange * Coef) .*cosd(BeamAngles), 'k'); hold on; grid on;
        PlotUtils.createSScPlot(-(PhasePingRange * Coef).*cosd(BeamAngles), 'r'); title('Z'); hold off;
        
        h(3) = subplot(4,1,3);
        PlotUtils.createSScPlot(AmpPingQF, 'k'); grid on; title('AmpPingQF', 'Interpreter', 'none')
        hold on
        PlotUtils.createSScPlot(PhasePingQF, 'r'); grid on; title('PhasePingQF', 'Interpreter', 'none')
        set(h(3), 'YLim', [2 max(4, max([AmpPingQF PhasePingQF]))])
        hold off
        
        subplot(4,1,4);
        z_Amp = -(AmpPingRange * Coef).*cosd(BeamAngles);
        z_Phs = -(PhasePingRange * Coef).*cosd(BeamAngles);
        PlotUtils.createSScPlot((AmpPingRange * Coef).*sind(BeamAngles), z_Amp, 'k'); hold on; grid on; %axis equal;
        PlotUtils.createSScPlot((PhasePingRange * Coef) .*sind(BeamAngles), z_Phs, 'r'); title('Bathy'); hold off;
        
        if ~isempty(iBeamBeg)
            for kAxe=1:length(h)
                axes(h(kAxe)); %#ok<LAXES>
                hold on;
                plot([iBeamBeg iBeamBeg], get(h(kAxe), 'YLim'), 'g');
                plot([iBeamEnd iBeamEnd], get(h(kAxe), 'YLim'), 'g');
                hold off
            end
        end
        
        linkaxes(h, 'x'); axis tight; drawnow;
        
        %{
ImageName = extract_ImageName(Phase);
ScreenSize = get(0,'ScreenSize');
Fig = figure(12349);
if ~isempty(mov)
set(Fig, 'Position', floor([10 40 ScreenSize(3)-20 ScreenSize(4)-140]/4)*4);
end

hImage(1) = subplot(1,2,1);
imagesc(Amp_dB); colormap(jet(256)); colorbar;
title(sprintf('Bottom detection on %s', ImageName), 'Interpreter', 'none')
hold on;
PlotUtils.createSScPlot(abs(PhasePingRange), 'r'); PlotUtils.createSScPlot(abs(AmpPingRange), 'k');
subAmp = find(TD == 1);
PlotUtils.createSScPlot(subAmp, abs(PingRange(subAmp)), 'ok');
subPhase = find(TD == 2);
PlotUtils.createSScPlot(subPhase, abs(PingRange(subPhase)), 'or');
hold off;

hImage(2) = subplot(1,2,2);
I = get(Phase, 'Image');
imagesc(-abs(I)); colormap(jet(256)); colorbar;
title(sprintf('Bottom detection on %s', ImageName), 'Interpreter', 'none')
hold on;
PlotUtils.createSScPlot(abs(PhasePingRange), 'b'); PlotUtils.createSScPlot(abs(AmpPingRange), 'k');
PlotUtils.createSScPlot(subAmp, abs(PingRange(subAmp)), 'ok');
PlotUtils.createSScPlot(subPhase, abs(PingRange(subPhase)), 'ob');
hold off;

linkaxes(hImage); axis tight; drawnow;

if ~isempty(mov)
I =  getframe(Fig);
I = I.cdata;
size(I)
% OLD mov = addframe(mov, I);
writeVideo(mov,I); % FORGE 353
end
        %}
        
        drawnow
    end
    clear Phase Amp
    
    Range(k,:)         = PingRange;
    TypeDetection(k,:) = TD;
    SyntheseQF(k,:)    = QualityFactor;
    
    AmpImageRange(k,:)               = AmpPingRange;
    AmpImageQF(k,:)                  = AmpPingQF;
    AmpImageNbSamples(k,:)           = AmpPingSamplesNb;
    AmpImageMax(k,:)                 = AmpPingNormHorzMax;
    AmpImageQF2(k,:)                 = AmpPingQF2;
    AmpImageQF3(k,iBeamBeg:iBeamEnd) = 1;
    subNaN = isnan(TD);
    AmpImageQF3(k,subNaN)            = NaN;
    
    PhaseImageRange(k,:)     = PhasePingRange;
    PhaseImageQF(k,:)        = PhasePingQF;
    PhaseImagePente(k,:)     = PhasePingPente;
    PhaseImageNbSamples(k,:) = PhasePingNbSamples;
    PhaseImageEqm(k,:)       = PhasePingEqm;
    
    %     if isempty(tetav)
    Angles(k,:) = SampleBeamData.TxAngle;
    %     else
    %         Angles(k,:) = SampleBeamData.TxAngle;
    %     end
    
    DataDepth.VehicleDepth(k) = 0;
    Time(k)         = SampleBeamData.Time;
    DatetimePing(k) = SampleBeamData.Datetime;
    Datetime        = DatetimePing(k);
    
    if isempty(Data7030) || ~isfield(Data7030, 'DRF')
        DataDepth.TransducerDepth(k) = 0;
    else
        DataDepth.TransducerDepth(k) = Data7030.DRF(1).ReceiveArrayZ - Data7030.DRF(1).WaterLineVerticalOffset;
    end
    
    % TODO SoundVelocity(1)
    DataDepth.SurfaceSoundSpeed(k) = SampleBeamData.SoundVelocity(1);
    DataDepth.Mode(k)              = 1;
    DataDepth.SampleRate(k)        = SampleBeamData.SampleRate(1); % TODO SampleRate(1)
    DataDepth.Frequency(k)         = SampleBeamData.Frequency(1)/1000; % TODO Frequency(1)
    
    if isempty(DataAttitude) || ~isfield(DataAttitude, 'Roll') || isempty(DataAttitude.Roll)
        DataDepth.Roll(k)    = 0;
        DataDepth.Pitch(k)   = 0;
        DataDepth.Heave(k)   = 0;
        DataDepth.Heading(k) = 0;
        RollTx  = 0;
        PitchTx = 0;
        HeaveTx = 0;
        RollRx  = zeros(1,nbBeams);
        PitchRx = zeros(1,nbBeams);
        HeaveRx = zeros(1,nbBeams);
    else
        DataDepth.Roll(k)    = interp1(DataAttitude.Datetime, DataAttitude.Roll,    Datetime);
        DataDepth.Pitch(k)   = interp1(DataAttitude.Datetime, DataAttitude.Pitch,   Datetime);
        DataDepth.Heave(k)   = interp1(DataAttitude.Datetime, DataAttitude.Heave,   Datetime);
        DataDepth.Heading(k) = interp1(DataAttitude.Datetime, DataAttitude.Heading, Datetime);
        iPingRange = floor(PingRange);
        subNonNan = find(~isnan(iPingRange));
        
        RollTx  = DataDepth.Roll(k);
        PitchTx = DataDepth.Pitch(k);
        HeaveTx = DataDepth.Heave(k);
        
        RollRx  = NaN(1,nbBeams);
        PitchRx = NaN(1,nbBeams);
        HeaveRx = NaN(1,nbBeams);
        
        tRx = datetime(SampleBeamData.Time, 'ConvertFrom', 'datenum') + SampleTime(iPingRange(subNonNan));
        HeaveRx(subNonNan) = interp1(DataAttitude.Datetime, DataAttitude.Heave, tRx);
        PitchRx(subNonNan) = interp1(DataAttitude.Datetime, DataAttitude.Pitch, tRx);
        RollRx(subNonNan)  = interp1(DataAttitude.Datetime, DataAttitude.Roll,  tRx);
    end
    
    DataDepth.Tide(k)        = 0;
    DataDepth.PingCounter(k) = SampleBeamData.PingCounter(1); % TODO PingCounter(1)
    
    if isempty(DataNAV) || ~isfield(DataNAV, 'Latitude') || isempty(DataNAV.Longitude)
        DataDepth.Longitude(k) = 0;
        DataDepth.Latitude(k)  = 0;
    else
        DataDepth.Longitude(k) = interp1(DataNAV.Datetime, DataNAV.Longitude, Datetime);
        DataDepth.Latitude(k)  = interp1(DataNAV.Datetime, DataNAV.Latitude,  Datetime);
    end
    
    if isempty(Data7030) || isempty(Data7030.Datetime)
        %         Bathy(k,:) = calcul_bathy_simple(Range(k,:), SampleRate, Angles(k,:), SystemSerialNumber);
        Bathy(k,:) = calcul_bathy_simple(Range(k,:), SampleRate(1), Angles(k,:), Draught); % TODO  SampleRate(1)
    else
        Bathy(k,:) = calcul_bathy(Range(k,:), SampleRate(1), Angles(k,:), ...
            RollTx, PitchTx, HeaveTx, RollRx, PitchRx, HeaveRx, Data7030, SystemSerialNumber, Draught); %SampleRate(1)
    end
    
    %   AcrossDistance ;
    
    clear b
end
my_close(hw)
if N == 0
    return
end

clear AmpImage PhaseImage

%% Image de la synth�se de d�tection

if isempty(InitialImageName)
    [~, InitialImageName] = fileparts(nomFic);
end

that = cl_image('Image', Range(1:N,:));
that.Name              =  NomImage;
that.InitialImageName  = InitialImageName;
that.Unit              = 'Sample';
that.TagSynchroX       = TagSynchroX;
that.TagSynchroY       = TagSynchroY;
that.XUnit             = 'Beam #';
that.YUnit             = 'Ping';
that.InitialFileName   = InitialFileName;
that.InitialFileFormat = InitialFileFormat;
that.GeometryType      = GeometryType;
that.DataType          = DataType;
that = set_SonarDescription(that, SonarDescription);
that.Comments          = 'Range coming from Phase';
%     that.strMaskBits = strMaskBits;
that.x                 = 1:nbBeams;
% that.y = subPing;
that.y                 = 1:N;
that.YDir              = 1;
that.ColormapIndex     = 3;
that = set_Carto(that, Carto);
that = update_Name(that);

%% Transfert des signaux

DataDepth.Time = cl_time('timeMat', Time(1:N));
subPing = 1:N;
that = set_SonarHeight(that, -abs(DataDepth.VehicleDepth(1:N)));
that = set_SonarTime(that, DataDepth.Time);
that = set_SonarImmersion(that, DataDepth.TransducerDepth(1:N));
that = set_SonarSurfaceSoundSpeed(that, DataDepth.SurfaceSoundSpeed(1:N)');
that = set_SonarPortMode_1(that, DataDepth.Mode(1:N));
that = set_SonarPingCounter(that, subPing);

% that = set_SonarRawDataResol(that, resolutionMajoritaire);
% that = set_SonarResolutionD(that, resolutionMajoritaire);

that = set_SonarSampleFrequency(that, DataDepth.SampleRate(1:N));
that = set_SonarFrequency(that, DataDepth.Frequency(1:N));

that = set_SonarRoll(that, DataDepth.Roll(1:N));
that = set_SonarPitch(that, DataDepth.Pitch(1:N));

that = set_SonarHeave(that, DataDepth.Heave(1:N));
that = set_SonarTide(that, DataDepth.Tide(1:N));
that = set_SonarPingCounter(that, DataDepth.PingCounter(1:N));

that = set_SonarFishLongitude(that, DataDepth.Longitude(1:N));
that = set_SonarFishLatitude(that, DataDepth.Latitude(1:N));
that = set_SonarHeading(that, DataDepth.Heading(1:N));

%% Image de la d�tection en amplitude

n = 2;
that(n) = replace_Image(that(1), AmpImageRange(1:N,:));
that(n) = compute_stats(that(n));
that(n).TagSynchroContrast = num2str(rand(1)); 
StatValues = that(n).StatValues;
that(n).CLim     = [StatValues.Min StatValues.Max]; 
that(n).Unit     = 'Deg/Sample';
that(n).DataType = DataType;
that(n) = update_Name(that(n), 'Append', 'Amp');
clear AmpImageRange

%% Image de l'amplitude max dans le processus de detection du masque

n = n+1;
that(n) = replace_Image(that(1), AmpImageMax(1:N,:));
that(n) = compute_stats(that(n));
that(n).TagSynchroContrast = num2str(rand(1)); 
StatValues = that(n).StatValues;
that(n).CLim     = [StatValues.Min StatValues.Max]; 
that(n).Unit     = '';
that(n).DataType = cl_image.indDataType('Reflectivity');
that(n) = update_Name(that(n), 'Append', 'Amp');
that(n).ColormapIndex = 2;
clear AmpImageMax

%% Image du nombre de points utilis�s dans la d�tection en amplitude

n = n+1;
that(n) = replace_Image(that(1), AmpImageNbSamples(1:N,:));
that(n) = compute_stats(that(n));
that(n).TagSynchroContrast = num2str(rand(1)); 
StatValues = that(n).StatValues;
that(n).CLim = [StatValues.Min StatValues.Max]; 
that(n).Unit = '';
that(n).DataType = cl_image.indDataType('MbesDetectionNbSamples');
that(n) = update_Name(that(n), 'Append', 'Amp');
clear AmpImageNbSamples

%% Image du facteur de qualit� en amplitude

n = n+1;
that(n) = replace_Image(that(1), AmpImageQF(1:N,:));
that(n) = compute_stats(that(n));
that(n).TagSynchroContrast = num2str(rand(1)); 
StatValues       = that(n).StatValues;
that(n).CLim     = [StatValues.Min StatValues.Max]; 
% that(n).CLim = [-max(CLim) max(CLim)];
that(n).Unit     = '';
that(n).DataType = cl_image.indDataType('MbesQualityFactor');
that(n) = update_Name(that(n), 'Append', 'Amp');
% that(n).ColormapIndex = 15; % 15=NIWA1
clear AmpImageQF

%% Image du facteur de qualit� en amplitude

n = n+1;
AmpImageQF2(isinf(AmpImageQF2)) = NaN;
that(n) = replace_Image(that(1), AmpImageQF2(1:N,:));
that(n) = compute_stats(that(n));
that(n).TagSynchroContrast = num2str(rand(1)); 
StatValues = that(n).StatValues;
that(n).CLim     = [StatValues.Min StatValues.Max]; 
that(n).Unit     = '';
that(n).DataType = cl_image.indDataType('MbesQualityFactor');
that(n) = update_Name(that(n), 'Append', 'Amp_QF2');
% that(n).ColormapIndex = 15; % 15=NIWA1
clear AmpImageQF2

%% Image du mask des amplitudes significatives

n = n+1;
AmpImageQF3(isinf(AmpImageQF3)) = NaN;
that(n) = replace_Image(that(1), AmpImageQF3(1:N,:));
that(n) = compute_stats(that(n));
that(n).TagSynchroContrast = num2str(rand(1)); 
that(n).CLim = [-1 1];
that(n).Unit = '';
that(n).DataType = cl_image.indDataType('MbesQualityFactor');
that(n) = update_Name(that(n), 'Append', 'Amp_QF3');
clear AmpImageQF3

%% Image de la d�tection en phase

n = n+1;
that(n) = replace_Image(that(1), PhaseImageRange(1:N,:));
that(n) = compute_stats(that(n));
that(n).TagSynchroContrast = num2str(rand(1)); 
StatValues = that(n).StatValues;
that(n).CLim     = [StatValues.Min StatValues.Max]; 
that(n).Unit     = 'Deg/Sample';
that(n).DataType = DataType;
that(n) = update_Name(that(n), 'Append', 'Phase');
clear PhaseImageRange

%% Image du nombre de points utilis�s dans la d�tection en phase

n = n+1;
that(n) = replace_Image(that(1), PhaseImageNbSamples(1:N,:));
that(n) = compute_stats(that(n));
that(n).TagSynchroContrast = num2str(rand(1)); 
StatValues = that(n).StatValues;
CLim = [StatValues.Min StatValues.Max];
that(n).CLim = CLim; 
that(n).Unit = '';
that(n).DataType = cl_image.indDataType('MbesDetectionNbSamples');
that(n) = update_Name(that(n), 'Append', 'Phase');
clear PhaseImageNbSamples

%% Image du facteur de qualit� en phase

n = n+1;
that(n) = replace_Image(that(1), PhaseImageQF(1:N,:));
that(n) = compute_stats(that(n));
that(n).TagSynchroContrast = num2str(rand(1));
StatValues = that(n).StatValues;
CLim = [StatValues.Min StatValues.Max];
that(n).CLim = CLim; 
% that(n)._CLim = [-max(CLim) max(CLim)];
that(n).Unit = '';
that(n).DataType = cl_image.indDataType('MbesQualityFactor');
that(n) = update_Name(that(n), 'Append', 'Phase');
% that(n).ColormapIndex = 15; % 15=NIWA1
clear PhaseImageQF

%% Image des pentes de phase

n = n+1;
that(n) = replace_Image(that(1), PhaseImagePente(1:N,:));
that(n) = compute_stats(that(n));
that(n).TagSynchroContrast = num2str(rand(1)); 
StatValues = that(n).StatValues;
that(n).CLim     = [StatValues.Min StatValues.Max]; 
that(n).Unit     = 'Deg/Sample';
that(n).DataType = cl_image.indDataType('MbesPhaseSlope');
that(n) = update_Name(that(n), 'Append', 'Phase');
clear PhaseImagePente

%% Image des pentes de PhaseImageEqm

n = n+1;
that(n) = replace_Image(that(1), PhaseImageEqm(1:N,:));
that(n) = compute_stats(that(n));
that(n).TagSynchroContrast = num2str(rand(1)); 
StatValues = that(n).StatValues;
that(n).CLim     = [StatValues.Min StatValues.Max]; 
that(n).Unit     = 'Deg';
that(n).DataType = cl_image.indDataType('MbesPhaseEqm');
that(n) = update_Name(that(n), 'Append', 'Phase');
clear PhaseImageEqm

%% Image des angles

n = n+1;
that(n) = replace_Image(that(1), Angles(1:N,:));
that(n) = compute_stats(that(n));
that(n).TagSynchroContrast = num2str(rand(1)); 
StatValues = that(n).StatValues;
that(n).CLim     = [StatValues.Min StatValues.Max]; 
that(n).Unit     = 'Deg';
that(n).DataType = cl_image.indDataType('TxAngle');
that(n) = update_Name(that(n));

%% Image du type de d�tection

n = n+1;
that(n) = replace_Image(that(1), TypeDetection(1:N,:));
that(n) = compute_stats(that(n));
that(n).TagSynchroContrast = num2str(rand(1)); 
that(n).CLim = [0 2];
that(n).Unit = '';
that(n).DataType = cl_image.indDataType('DetectionType');
that(n) = update_Name(that(n));
clear TypeDetection

%% Image de la synthese du facteur de qualit�

n = n+1;
that(n) = replace_Image(that(1), SyntheseQF(1:N,:));
that(n) = compute_stats(that(n));
that(n).TagSynchroContrast = num2str(rand(1)); 
StatValues = that(n).StatValues;
CLim = [StatValues.Min StatValues.Max];
that(n).CLim = CLim; 
% that(n).CLim = [-max(CLim) max(CLim)];
that(n).Unit = '';
that(n).DataType = cl_image.indDataType('MbesQualityFactor');
that(n) = update_Name(that(n));
% that(n).ColormapIndex = 15; % 15=NIWA1
clear SyntheseQF

%% Image de la pseudo bathym�trie

n = n+1;
that(n) = replace_Image(that(1), Bathy(1:N,:));
that(n) = compute_stats(that(n));
that(n).TagSynchroContrast = num2str(rand(1)); 
StatValues = that(n).StatValues;
CLim = [StatValues.Min StatValues.Max];
that(n).CLim = CLim; 
that(n).Unit = '';
that(n).DataType = cl_image.indDataType('Bathymetry');
that(n) = update_Name(that(n));
clear Bathy

%% Image de la distance transversale

Coef = 1500 / (2 * SampleRate(1)); % TODO SampleRate(1)
X = NaN(N,nbBeams, 'single');
for k=1:N
    Teta = Angles(k,:) - DataDepth.Roll(k);
    D = Range(k,:) * Coef;
    X(k,:) = D .* sind(Teta);
end
n = n+1;
that(n) = replace_Image(that(1), X);
that(n) = compute_stats(that(n));
that(n).TagSynchroContrast = num2str(rand(1)); 
StatValues = that(n).StatValues;
CLim = [StatValues.Min StatValues.Max];
that(n).CLim = CLim; 
that(n).Unit = '';
that(n).DataType = cl_image.indDataType('AcrossDist');
that(n) = update_Name(that(n));
clear Range Angles

%% Image de la distance longitudinale

n = n+1;
that(n) = replace_Image(that(1), X*0);
that(n) = compute_stats(that(n));
that(n).TagSynchroContrast = num2str(rand(1)); 
StatValues = that(n).StatValues;
CLim = [StatValues.Min StatValues.Max];
that(n).CLim = CLim; 
that(n).Unit = '';
that(n).DataType = cl_image.indDataType('AlongDist');
that(n) = update_Name(that(n));
clear X

%% C'est fini

e = that([2:n 1]);
flag = 1;



% function Z = calcul_bathy(Range, SampleRate, Angles, RollTx, PitchTx, HeaveTx, RollRx, PitchRx, HeaveRx, Data7030, SystemSerialNumber)
function Z = calcul_bathy(Range, SampleRate, Angles, RollTx, PitchTx, HeaveTx, RollRx, PitchRx, HeaveRx, Data7030, SystemSerialNumber, Draught)

Coef = -1500 / (2 * SampleRate);
rad2deg = 180/pi;

if SystemSerialNumber == 7111
    if isfield(Data7030, 'DRF') && isfield(Data7030.DRF, 'TransmitArrayRoll')
        Teta = Angles - RollTx + Data7030.DRF(1).TransmitArrayRoll*rad2deg; % A v�rifier
    else
        Teta = Angles - RollTx;
    end
else
    Teta = Angles;
end
D = Range * Coef;
Z = D .* cosd(Teta);

if isempty(Data7030.Time.timeMat)
    %     Z = Z - 5.9; % Pourquoi-Pas?
    Z = Z - Draught; % Pourquoi-Pas?
else
    %     Z = Z + Data7030.DRF.ReceiveArrayZ - Data7030.DRF(1).WaterLineVerticalOffset; % -5.9
    Z = Z - Data7030.DRF(1).WaterLineVerticalOffset; % -5.9
end

% ----------------
% Heave correction

Heave = heave_correction(RollTx, PitchTx, HeaveTx, RollRx, PitchRx, HeaveRx, Data7030.DRF(1));
% figure(6565); plot(Heave); grid on;
Z = Z + Heave;


function Heave = heave_correction(RollTx, PitchTx, HeaveTx, RollRx, PitchRx, HeaveRx, DRF)
rad2deg = 180/pi;
% Heave = HeaveTx + (Pinduit(RollTx+DRF.TransmitArrayRoll*rad2deg, PitchTx+DRF.TransmitArrayPitch*rad2deg, DRF, 1) + HeaveTx + ...
%     Pinduit(RollRx+DRF.ReceiveArrayRoll*rad2deg, PitchRx+DRF.ReceiveArrayPitch*rad2deg, DRF, 2) + HeaveRx) / 2;
Heave = HeaveTx / 2 ...
    + (Pinduit(RollTx+DRF.TransmitArrayRoll*rad2deg, PitchTx+DRF.TransmitArrayPitch*rad2deg, DRF, 1) ...
    + Pinduit(RollRx+DRF.ReceiveArrayRoll*rad2deg, PitchRx+DRF.ReceiveArrayPitch*rad2deg, DRF, 2) + HeaveRx) / 2;
% Heave = HeaveTx + (Pinduit(RollTx, PitchTx, DRF, 1) + HeaveTx + ...
%                    Pinduit(RollRx, PitchRx, DRF, 2) + HeaveRx) / 2;

% Pinduit(0, 0, DRF, 1)

function P = Pinduit(Roll, Pitch, DRF, Mode)
Cx = DRF.MotionSensorX;
Cy = DRF.MotionSensorY;
Cz = DRF.MotionSensorZ;

if Mode == 1 % Tx
    Sx = DRF.TransmitArrayX;
    Sy = DRF.TransmitArrayY;
    Sz = DRF.TransmitArrayZ;
else % 2= Rx
    Sx = DRF.ReceiveArrayX;
    Sy = DRF.ReceiveArrayY;
    Sz = DRF.ReceiveArrayZ;
end

% 1
P = sind(Pitch) * (Sy-Cy) ...
    + cosd(Pitch) .* sind(Roll) * (Sx-Cx) ...
    + cosd(Roll) .* cosd(Pitch) * (Sz-Cz);

% 3
% P = sind(Pitch) * (Sy-Cy) ...
%     - cosd(Pitch) .* sind(Roll) * (Sx-Cx) ...
%     + cosd(Roll) .* cosd(Pitch) * (Sz-Cz);

% Les 2 autres cas ont �t� �limin�s




% function Z = calcul_bathy_simple(Range, SampleRate, Angles, SystemSerialNumber)
function Z = calcul_bathy_simple(Range, SampleRate, Angles, Draught)

Coef = -1500 / (2 * SampleRate);
Teta = Angles;
D = Range * Coef;
Z = D .* cosd(Teta);

% Z = Z - 5.9; % Pourquoi-Pas?
Z = Z - Draught;
