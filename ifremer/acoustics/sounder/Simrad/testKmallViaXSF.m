function flag = testKmallViaXSF

flag = 0;

% D:\Temp\ESSTECH-TL2021-2\EM304\KMALL

% % ??? 
% nomFicKmall = 'D:\XSF\FromKmall\0023_20180628_122559_ShipName.kmall';

% Fichier de test avec .all converti pour comparaison
if contains(getenv('USERPROFILE'), 'augustin')
    nomFicKmall = 'D:\XSF\FromKmall\0039_20180905_222154_raw.kmall';
elseif contains(getenv('USERPROFILE'), 'rgallou')
    nomFicKmall = 'E:\SonarScopeAnnexes\SonarScopeDataArchive\TestsFormatXsf\0039_20180905_222154_raw.kmall';
end

% % Fichier générés par Ridha avec fichier de description modes304.txt
% nomFicKmall = 'D:\XSF\FromKmall\0200_20210604_082709.kmall';
% nomFicKmall = 'D:\XSF\FromKmall\0201_20210604_085709.kmall';
% nomFicKmall = 'D:\XSF\FromKmall\0202_20210604_092709.kmall';

if ~exist(nomFicKmall, 'file')
    msg = sprintf('%s does not exist', nomFicKmall);
    my_warndlg(msg, 1);
    return
end

nomFicXSF = strrep(nomFicKmall, '.kmall', '.xsf.nc');
if ~exist(nomFicXSF, 'file')
    msg = sprintf('%s does not exist', nomFicXSF);
    my_warndlg(msg, 1);
    return
end

a = cl_simrad_all('nomFic', nomFicKmall);
