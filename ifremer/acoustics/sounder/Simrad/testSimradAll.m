% Fichier avec plusieurs Attitude sensor.
nomFicAll   = 'E:\SonarScopeAnnexes\SonarScopeDataArchive\From Marc Roche\0059_20221211_193145_Belgica.all';
a           = cl_simrad_all('NomFic', nomFicAll);

[flag, flagRTK, nomFicWc, EmModel, SystemSerialNumber, Version, SousVersion] = all2ssc(nomFic, 'ShowProgressBar', ShowProgressBar); % TODO


FormatVersion = 20101118;
nomDirRacine = 'E:\SonarScopeAnnexes\SonarScopeDataArchive\From Marc Roche\SonarScope\0059_20221211_193145_Belgica';
flag = all2ssc_Position(nomDirRacine, FormatVersion);

flag = all2ssc_Attitude(nomDirRacine, FormatVersion);

[flag, DataPosition] = SSc_ReadDatagrams(nomFicAll, 'Ssc_Position', 'Memmapfile', 0, 'LogSilence', 1);
[flag, DataAttitude] = SSc_ReadDatagrams(nomFicAll, 'Ssc_Attitude', 'Memmapfile', 0, 'LogSilence', 1);

IndNavUnit = 1;
[flag, DataPosition1] = read_position_bin(a, 'IndNavUnit', 1, 'Memmapfile', 0);
[flag, DataAttitude1] = read_attitude_bin(a, 'IndNavUnit', 1, 'Memmapfile', 0);
IndNavUnit = 2;
[flag, DataPosition2] = read_position_bin(a, 'IndNavUnit', 2, 'Memmapfile', 0);
[flag, DataAttitude2] = read_attitude_bin(a, 'IndNavUnit', 2, 'Memmapfile', 0);


