%%

nomFic = getNomFicDatabase('BScorr_710.txt');
[flag, BSCorr710] = read_BSCorr(nomFic);

%%

nomFic = getNomFicDatabase('BScorr_122.txt');
[flag, BSCorr122] = read_BSCorr(nomFic);

%%

FileNameSsc = 'D:\SounderDemoFiles\WithBscorr\SonarScope\0014_20121105_103947_SimradEcho\ALL_ExtraParameters\BSCorr.txt';
[flag, ExtraArray] = readExtraParams_BSCorr(FileNameSsc);
ExtraArray %#ok<*NOPTS>
ExtraArray.BSCorr
ExtraArray.BSCorr(1)
ExtraArray.BSCorr(1).pingMode
ExtraArray.BSCorr(1).swathMode
ExtraArray.BSCorr(1).Sector
ExtraArray.BSCorr(1).Sector(1)
ExtraArray.BSCorr(1).Sector(1).Node

MaxPingMode = max([ ExtraArray.BSCorr(:).pingMode])
MaxSwathMode = max([ ExtraArray.BSCorr(:).swathMode])+1
Fig = FigUtils.createSScFigure;
for k1=1:length(ExtraArray.BSCorr)
    ExtraArray.BSCorr(k1)
    pingMode  = ExtraArray.BSCorr(k1).pingMode
    swathMode = ExtraArray.BSCorr(k1).swathMode
%     FigUtils.createSScFigure(1000+swathMode*10+pingMode);
    figure(Fig)
    for k2=1:length(ExtraArray.BSCorr(k1).Sector)
        ExtraArray.BSCorr(k1).Sector(k2)
        ExtraArray.BSCorr(k1).Sector(k2).Node
        x = -ExtraArray.BSCorr(k1).Sector(k2).Node(:,1); % TODO signe -
        y = ExtraArray.BSCorr(k1).Sector(k2).Node(:,2);
        
        my_subplot(MaxPingMode, MaxSwathMode, pingMode, swathMode+1)
        PlotUtils.createSScPlot(x, y, '*'); grid on; hold on;
        
        xi = min(x):0.5:max(x);
        yi = spline(x, y, xi);
        PlotUtils.createSScPlot(xi, yi, 'r'); grid on; hold on;
        
%{
        yi = yi + randn(size(yi)) / 2;
        PlotUtils.createSScPlot(xi, yi, '.k'); grid on; hold on;
        P = polyfit(xi, yi, 12);
        yNew = polyval(P, xi);
%         PlotUtils.createSScPlot(xi, yNew, 'm'); grid on; hold on;
        
        xNewNode = x;
        yNewNode = polyval(P, x);
        
        yiNew = spline(xNewNode, yNewNode, xi);
        PlotUtils.createSScPlot(xi, yiNew, 'g'); grid on; hold on;
%}
    end
    hold off
    title(sprintf('swathMode : %d  -   pingMode : %d', swathMode, pingMode))
end
