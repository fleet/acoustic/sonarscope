
function PingCounter = unwrapPingCounter65536(PingCounter)

for i=2:length(PingCounter)
    if PingCounter(i) < PingCounter(i-1)
        PingCounter(i) = PingCounter(i) + 65536;
    end
end

