function word = verifKeyWord(word)

liste = {'MotionSensorTime'
    'MotionSensorRoll'
    'MotionSensorPitch'
    'MotionSensorHeave'
    'MotionSensorHeading'
    'MotionSensorStatus'
    'GPSTime'
    'GPSLatitude'
    'GPSLongitude'
    'GPSHeading'
    'GPSSpeed'
    'GPSHeight'
    'GPSAccuracyHorizontal'
    'GPSAccuracyHeight'
    'GPSAccuracy'
    'GPSDescriptor'
    'GPSQualityIndicator'
    'GPSNumberSatellitesUsed'
    'GPSHDOP'
    'GPSAltitudeOfMIU'
    'GPSGeoidalSeparation'
    'GPSAgeOfDifferentialCorrections'
    'GPSDGPSRefStationIdentity'
    'ExternalTime'
    'SounderAcrossDist'
    'SounderAlongDist'
    'SounderAzimuthAngle'
    'SounderAzimuthPointingAngle'
    'SounderBeamAngle'
    'SounderDetectionNbSamples'
    'SounderDepth'
    'SounderFrequency'
    'SounderHeading'
    'SounderHeight'
    'SounderMeanAbsorpCoeff'
    'SounderNbBeams'
    'SounderPingCounter'
    'SounderPingSequence'
    'SounderPingLatitude'
    'SounderPointingAngle'
    'SounderPulseLength'
    'SounderReflectivity'
    'SounderTxDelay'
    'SounderTxTiltAngle'
    'SounderTxCentralFrequency'
    'SounderTxSignalBandwith'
    'SounderRxBeamIndex'
    'SounderTxBeamIndex'
    'SounderRxBeamWidth'
    'SounderSamplingRate'
    'SounderROV_Depth'
    'SounderTiltAngle'
    'SounderFocusRange'
    'SounderSerialNumber'
    'SounderSimradQF'
    'SounderSimradLengthDetection'
    'SounderSimradTVGN'
    'SounderSimradTVGStart'
    'SounderSimradTVGStop'
    'SounderSimradBSN'
    'SounderSimradBSO'
    'SounderSnippetsNbSamples'
    'SounderSoundSpeed'
    'SounderTime'
    'SounderMode'
    'SounderRoll'
    'SounderPitch'
    'SounderHeave'
    'SounderLatitude'
    'SounderLongitude'
    'SounderTide'
    'SounderVehicleDepth'
    'SounderTransducerDepth'
    'SounderHeightSource'
    'SounderProjectorBeamSteeringAngleVertical'
    'SounderMinFilterInfo'
    'SounderMaxFilterInfo'
    'SounderTxRollCompensationFlag'
    'SounderTxRollCompensationValue'
    'SounderTxPitchCompensationFlag'
    'SounderTxPitchCompensationValue'
    'SounderRxRollCompensationFlag'
    'SounderRxRollCompensationValue'
    'SounderRxPitchCompensationFlag'
    'SounderRxPitchCompensationValue'
    'SounderRxHeaveCompensationFlag'
    'SounderRxHeaveCompensationValue'
    'SounderBeamVerticalDirectionAngle'
    'SounderBeamHorizontalDirectionAngle'
    'Sounders_3dBBeamWithY'
    'Sounders_3dBBeamWithX'
    'SounderMask'
    'SounderTwoWayTravelTimeInSamples'
    'SounderTxBeamWidth'
    'SounderVerticalDepth'
    'SounderPointerOnSonarSettings'
    'SoundProfileTimeStartOfUse'
    'SoundProfileTime'
    'SoundProfileDepth'
    'SoundProfileSoundSpeed'
    'SoundProfileTemperature'
    'SoundProfileSalinity'
    'SoundProfileAbsorption'
    'TwoWayTravelTimeInSeconds'
    'Auxillary'
    'Mask'
    'SounderROV_Depth'
    'SounderTiltAngle'
    'SounderFocusRange'
    'SounderTxDelay'
    'SounderTxCentralFrequency'
    'SounderTxSignalBandwith'
    'SounderTxBeamIndex'
    'SounderSimradLengthDetection'
    'SounderTxTiltAngle'
    'SounderImmersion'
    'SurfaceSoundSpeed'
    'FishLatitude'
    'FishLongitude'
    'ShipLatitude'
    'ShipLongitude'
    'Heave'
    'Mode'
    'SampleFrequency'
    'Interlacing'
    'CableOut'
    'Speed'
    'RollUsedForEmission'
    'Yaw'
    'AbsorptionCoeff'
    'Tide'
    'SonarFrequency'
    'TxAlongAngle'
    'Colormap'
    'ColormapCustom'
    'ContourValues'
    'HistoValues'
    'HistoCentralClasses'
    'X'
    'Y'
    'DistPings'
    'PresenceWC'
    'PresenceFM'
    'NbSwaths'
    'NbCycles'
    'SensorSystemDecriptor'
    'SystemSerialNumber'
    'NRx'
    'NTx'
    'SounderBeamNumber'
    'MaxNbBeamsPossible'
    'TODO'};

rep = strcmp(liste, word);
if all(rep == 0)
    str = sprintf('"%s" ne fait pas partie du dictionnaire.', word);
    my_warndlg(str, 0, 'Tag', 'VeridMotDansDistionnaire');
end
