function V = versionSIS(OSV)

% OSV = replace(OSV, 'SIS', '');
% s = split(OSV, '.');
OSV = regexprep(OSV, 'SIS', '');
s = strsplit(OSV, '.');
V = str2double(s{1});
for k=2:length(s)
    V = V + str2double(s{k}) / 10^(k-1);
end
V = round(V, 3, 'significant');
