% Cr�ation d'une image PingSamples � partir d'un fichier .s7k
%
% Syntax
%   b = view_SimradRawData(aKM, ...);
%
% Input Arguments
%   aKM : Instance de cl_simrad_all
%
% Name-Value Pair Arguments
%   subPing : Numeros de lignes � traiter
%
% Name-only Arguments
%   FaisceauxPairs         : Pour creer une image uniquement avec les faisceaux pairs
%   FaisceauxImairs        : Pour creer une image uniquement avec les faisceaux impairs
%
% Output Arguments
%   b          : Instance de cli_image
%   subl_Image : Numeros de lignes communs avec la bathy (index relatis � seabedImage)
%   subl_Depth : Numeros de lignes communs avec la bathy (index relatis � Depth)
%
% Examples
%   nomFic = getNomFicDatabase('EM1002_0001_20040916_054013_raw.all')
%   aKM = cl_simrad_all('nomFic', nomFic);
%   b = view_SimradRawData(aKM);
%   SonarScope(b);
%
%   b = view_SimradRawData(aKM, 'subPing', 1:100);
%   SonarScope(b);
%
% See also cl_simrad_all cl_simrad_all/plot_position cl_simrad_all/plot_nav Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [b, Carto, RMax1_Precedent] = view_SimradRawData(nomFic, varargin)

b = [];
[varargin, DisplayLevel]    = getPropertyValue(varargin, 'DisplayLevel',    1);
[varargin, Carto]           = getPropertyValue(varargin, 'Carto',           []);
[varargin, DepthMin]        = getPropertyValue(varargin, 'DepthMin',        []);
[varargin, DepthMax]        = getPropertyValue(varargin, 'DepthMax',        []);
[varargin, Draught]         = getPropertyValue(varargin, 'Draught',         0);
[varargin, RMax1_Precedent] = getPropertyValue(varargin, 'RMax1_Precedent', []); %#ok<ASGLU>

subPing = 1;


[flag, DataPing, BeamDataOut] = read_SimradRawData(nomFic);
if ~flag
    return
end

N = length(subPing);
hw = create_waitbar('Loading Amplitude & Phase files : 2/2', 'N', N);
b = repmat(cl_image, 2,N);
for i=1:N
    my_waitbar(i,N,hw)
    iPing = subPing(i);
    
    DataPing = Extraction(DataPing, iPing);
    
    
    % Limitation of Time sample number
    
    fe = DataPing.SampleRate;
    RangeMax = DataPing.RangeMax; % (m)
    
    %     RMin = 2* fe * abs(DepthMax) / 1500;
    
    sampleNb = BeamDataOut(1).nbRows;
    %     beamNb   = BeamDataOut(1).nbColumns;
    RangeMax = min(floor(2* fe * RangeMax / 1500), sampleNb);
    
    Amplitude = get(BeamDataOut(1), 'Image', 'suby', 1:RangeMax);
    Phase     = get(BeamDataOut(2), 'Image', 'suby', 1:RangeMax);
    
    PulseDuration    = DataPing.TxPulseWidth; % s
    SampleRate       = DataPing.SampleRate; % Hz
    ReceiveBeamWidth = DataPing.ReceiveBeamWidth; % deg
    
    % ---------------------------------
    % Data reduction in the Time domain
    
    [AmplitudeReduced, step] = SampleBeam_ReduceMatrixAmplitude(Amplitude);
    
    % ----------------------------------
    % Preprocessing of Mask on Amplitude
    
    DepthMaxPing = abs(DepthMax)-Draught;
    DepthMinPing = abs(DepthMin)-Draught;
    
    [R0, RMax1, RMax2, R1SamplesFiltre, iBeamMax0, AmpPingNormHorzMax, MaskWidth, iBeamBeg, iBeamEnd] = ...
        SampleBeam_PreprocAmpMask_7111_V6(...
        AmplitudeReduced, step, DataPing.TxAngle, ReceiveBeamWidth, ...
        SampleRate, PulseDuration, DepthMinPing, DepthMaxPing, DisplayLevel, iPing);
    
    RMax1_Precedent = RMax1 * (1500 / SampleRate / 2);
    
    
    %{
figure(56557); imagesc(reflec_Enr2dB(Amplitude)); colorbar;
hold on;
plot(repmat(R0 ,1,length(RMax1)), '*c');
plot(RMax2, 'ok')
plot(RMax1, '*k')
hold off
    %}
    DataPing.R0                 = R0;
    DataPing.RMax1              = RMax1;
    DataPing.RMax2              = RMax2;
    DataPing.R1SamplesFiltre    = R1SamplesFiltre;
    DataPing.iBeamMax0          = iBeamMax0;
    DataPing.AmpPingNormHorzMax = AmpPingNormHorzMax;
    DataPing.MaskWidth          = MaskWidth;
    DataPing.DataTVG            = [];
    DataPing.iBeamBeg           = iBeamBeg;
    DataPing.iBeamEnd           = iBeamEnd;
    
    % Cr�ation de l'instance de classe
    
    IdentSonar = DataPing.IdentSonar;
    SonarDescription = cl_sounder('Sonar.Ident', IdentSonar, 'Sonar.Mode_1', 1, ...
        'Sonar.SystemSerialNumber', DataPing.SystemSerialNumber);
    
    [~, TagSynchro] = fileparts(nomFic);
    NomImage = TagSynchro;
    
    b(1,i) = cl_image;
    if isa(Amplitude, 'cl_memmapfile')
        b(1,i) = set_Image(b(1,i), Amplitude);
    else
        if N > 1
            b(1,i) = set_Image(b(1,i), cl_memmapfile('Value', Amplitude, 'LogSilence', 1));
        else
            b(1,i) = set_Image(b(1,i), Amplitude, 'NoMemmapfile');
        end
    end
    
    b(1,i).x                 = 1:size(Amplitude,2);
    b(1,i).y                 = 1:size(Amplitude,1);
    b(1,i).YDir              = 2;
    b(1,i).Name              =  NomImage;
    % b(1,i).InitialImageName = nomLayer;
    b(1,i).Unit              = 'Amp';
    b(1,i).TagSynchroX       = TagSynchro;
    b(1,i).TagSynchroY       = TagSynchro;
    b(1,i).XUnit             = 'Beam #';
    b(1,i).YUnit             = 'Sample #';
    b(1,i).InitialFileName   = nomFic;
    b(1,i).InitialFileFormat = 'SimradAmpPhase';
    b(1,i).Comments          = sprintf('Kongsberg PingCounter : %d', DataPing.PingCounter);
    b(1,i).GeometryType      = cl_image.indGeometryType('SampleBeam');
    b(1,i) = set_SonarDescription(b(1,i), SonarDescription);
    
    b(1,i) = set_SampleBeamData(b(1,i), DataPing);
    
    b(1,i).DataType          = cl_image.indDataType('Reflectivity');
    b(1,i).ColormapIndex     = 3;
    
    b(1,i) = update_Name(b(1,i));
    b(1,i).Writable = false;
    
    % ----------------------
    % Definition d'une carto
    
    while isempty(Carto)
        Carto = getDefinitionCarto(b(1,i), 'NoGeodetic', 0);
    end
    b(1,i) = set_Carto(b(1,i), Carto);
    
    
    if N > 1
        b(2,i) = set_Image(b(1,i), cl_memmapfile('Value', Phase, 'LogSilence', 1));
    else
        b(2,i) = set_Image(b(1,i), Phase, 'NoMemmapfile');
    end
    
    b(2,i).Name               = NomImage;
    b(2,i).Unit               = 'Deg';
    b(2,i).TagSynchroX        = TagSynchro;
    b(2,i).TagSynchroY        = TagSynchro;
    b(2,i).TagSynchroContrast = num2str(rand(1));
    b(2,i).XUnit              = 'Beam #';
    b(2,i).YUnit              = 'Sample #';
    b(2,i).InitialFileName    = nomFic;
    b(2,i).InitialFileFormat  = 'SimradAmpPhase';
    b(2,i).GeometryType       = cl_image.indGeometryType('SampleBeam');
    
    b(2,i) = set_SonarDescription(b(2,i), SonarDescription);
    b(2,i) = set_SampleBeamData(b(2,i), DataPing);
    
    b(2,i).DataType           = cl_image.indDataType('TxAngle');
    b(2,i).ColormapIndex      = 17;
    
    b(2,i) = compute_stats(b(2,i));
    b(2,i).TagSynchroContrast = num2str(rand(1));
    StatValues = b(2,i).StatValues;
    b(2,i).CLim = [StatValues.Min StatValues.Max];
    
    b(2,i) = update_Name(b(2,i));
    b(2,i).Writable = false;
end
b = b(:);
my_close(hw)

%% Edition de l'objet cli_image

if nargout == 0
    SonarScope(b);
end


function DataPing = Extraction(DataPing, i)

str1 = 'ATTENTION : ce format de donn�e ne donne pas acc�s aux param�tres du sondeur ni � la c�l�rit�.';
str2 = 'BEWARE : this format does not deliver sonar parameters ans sound velocity.';
my_warndlg(Lang(str1,str2), 0, 'Tag', 'DonnesBrutesSimradIndeterminees');

% DataPing.TxAngle = atand(linspace(tand(-40), tand(30), 160)); % deg
DataPing.SampleRate         = 7310.5;    % Hz
DataPing.TxPulseWidth       = 150e-6;   % s
DataPing.ReceiveBeamWidth   = 1.5;      % Deg
DataPing.PingCounter        = i;
DataPing.RangeMax           = 100; % (m)
DataPing.IdentSonar         = 19;   % EM3002D
DataPing.SystemSerialNumber = 0;
DataPing.SoundVelocity      = 1477.5;
DataPing.Frequency          = 307; % !!!!!!!!!!!!!!!!!!!!!!!!!!!!!
DataPing.Time               = now;

% Pour compatibility� avec syst�mes Reson
DataPing.MultiPingSequence = 1;
