% Ecriture des donn�es de gain de diagramme d'antenne pour un diagramme de
% type Polynomial (Type EM122 ou EM302).
%
% Syntax
%   flag = write_BSCorr_Poly(nomFic, BSIn, ...)
%
% Input Arguments
%   nomFic : nom du fichier de diagramme d'antenne
%   BSin   : structure contenant par Mode les Noeuds de
%            correction. Chaque noeud contient un triplet SL-Angle-Gain.
%
% Name-Value Pair Arguments
%   BSCorrSIS : BScorr en usage dans SIS au moment de l'acquisition
%
% Output Arguments
%   flag : indicateur de bon fonctionnement.
%
% Examples
%   fileName1 = ['G:\IFREMER\Doc\Constructeurs-Formats\SimRad-ALL\' ...
%                   'ExtraParameters Datagram\bscorr_ORIGINAL_BACKUP_DONOT_MODIFY.txt';];
%
%   [flag, BSCorr] = read_BSCorr_Poly(fileName1);
%   BSIn           = BSCorr;
%   nomFic         = 'C:\Temp\pppp.txt';
%   flag           = write_BSCorr_Poly(nomFic, BSIn);
%
% See also cl_simrad_all
% Authors : GLU
% ----------------------------------------------------------------------------

function [flag, BSCorr] = write_BSCorr_Poly(nomFic, BSCorr, varargin)

[varargin, BSCorrSIS] = getPropertyValue(varargin, 'BSCorrSIS', []); %#ok<ASGLU>

strPingMode  = {'Very shallow', 'Shallow', 'Medium', 'Deep', 'Very Deep', 'Extra Deep', 'Extreme Deep'};
strSwathMode = {'Single swath', 'Dual Swath 1', 'Dual Swath 2'};

fid = fopen(nomFic, 'wt');

PingMode = 0;
% Ecriture de l'ent�te du mode Ping - Swath - Nb Secteurs.
% fprintf(fid, '%s\n', Lang('# Ecriture par SonarScope des valeurs de correction de diagramme d''antenne', ...
%                      '# Writing by SonarScope the values of antenna diagram correction'));
fprintf(fid, '# Modified by SonarScope - %s\n', datestr(now, 'mmmm dd yyyy'));
fprintf(fid, '%s\n', '#');
fprintf(fid, '%s\n', '#  The values in the first three rows will be used instead of the program default values. ');
fprintf(fid, '%s\n', '#');
fprintf(fid, '%s\n', '# Values  	Default Parameters				Comment');
fprintf(fid, '%s\n', '#');
fprintf(fid, '%s\n', '# Source level    lobe angle    lobe width');
fprintf(fid, '%s\n', '#');

%% Sauvegarde du BSCorr courant

for iMode=1:numel(BSCorr)
    if PingMode ~= BSCorr(iMode).pingMode
        fprintf(fid, '# %s\n', strPingMode{BSCorr(iMode).pingMode});
        PingMode = BSCorr(iMode).pingMode;
    end
    if ~isempty(BSCorr(iMode).swathMode)
        fprintf(fid, '# %s\n', strSwathMode{BSCorr(iMode).swathMode});
    end
    % Ecriture des triplets SL - Angle - Gain
    for iNode=1:BSCorr(iMode).Sector.nbNodes
        % Ecriture en n�gatif de l'angle.
        fprintf(fid, '%d\t%d\t%d\n', ...
            int32( BSCorr(iMode).Sector.SL(iNode)     *100), ...
            int32(-BSCorr(iMode).Sector.Node(iNode, 1)*100), ...
            int32( BSCorr(iMode).Sector.Node(iNode, 2)*100));
    end
end

%% Sauvegarde du BSCorrSIS (en usage actuellement dans le sondeur)

for iMode=1:numel(BSCorrSIS)
    if PingMode ~= BSCorrSIS(iMode).pingMode
        fprintf(fid, '##SIS %s\n', strPingMode{BSCorrSIS(iMode).pingMode});
        PingMode = BSCorrSIS(iMode).pingMode;
    end
    if ~isempty(BSCorrSIS(iMode).swathMode)
        fprintf(fid, '##SIS %s\n', strSwathMode{BSCorrSIS(iMode).swathMode});
    end
    % Ecriture des triplets SL - Angle - Gain
    for iNode=1:BSCorrSIS(iMode).Sector.nbNodes
        % Ecriture en n�gatif de l'angle.
        fprintf(fid, '#SIS %d\t%d\t%d\n', ...
            int32( BSCorrSIS(iMode).Sector.SL(iNode)     *100), ...
            int32(-BSCorrSIS(iMode).Sector.Node(iNode, 1)*100), ...
            int32( BSCorrSIS(iMode).Sector.Node(iNode, 2)*100));
    end
end

fclose(fid);
flag = 1;
