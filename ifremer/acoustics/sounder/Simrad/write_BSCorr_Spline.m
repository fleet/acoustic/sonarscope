% Ecriture des donn�es de gain de diagramme d'antenne pour un diagramme de
% type Spline (Type EM710).
%  
% Syntax
%   flag = write_BSCorr_Spline(BSIn, nomFic)
% 
% Input Arguments 
%   nomFic : nom du fichier de diagramme d'antenne
%   BSin   : structure contenant par Mode les Noeuds de
%            correction. Chaque noeud contient un couple Angle-Gain.
%
% Name-Value Pair Arguments
%   Sans objet. 
%
% Output Arguments
%   flag   : indicateur de bon fonctionnement.
%   BSCorr : structure de r�sultat contenant par Mode les Noeuds de
%            correction. Chaque noeud contient un couple Angle-Gain.
% 
% Examples
%   nomFic = getNomFicDatabase('BSCorr_710-SHOM.txt');
%   nomFic = getNomFicDatabase('BScorr_710.txt');
%   [flag, BSCorr] = read_BSCorr_Spline(nomFic);
%   [flag, BSCorr, BSCorrInfo] = read_BSCorr(nomFic);
%   nomFicOut = my_tempname('.txt');
%   flag = write_BSCorr_Spline(BSCorr, nomFicOut, 'EM710');
%   [flag, BSCorr2] = read_BSCorr_Spline(nomFicOut);
%   isequal(BSCorr, BSCorr2)
%
%   nomFic = getNomFicDatabase('BSCorr_304.txt');
%   [flag, BSCorr] = read_BSCorr_SplineEM304(nomFic)
%   [flag, BSCorr, BSCorrInfo] = read_BSCorr(nomFic);
%   nomFicOut = my_tempname('.txt');
%   flag = write_BSCorr_Spline(BSCorr, nomFicOut, 'EM304');
%   [flag, BSCorr2] = read_BSCorr(nomFicOut);
%   isequal(BSCorr, BSCorr2)
%
% See also cl_simrad_all
% Authors : GLU
% ----------------------------------------------------------------------------

function flag = write_BSCorr_Spline(BSCorr, nomFic, SounderName)

flag = 0;

switch SounderName
    case 'EM304'
        strPingMode  = {'Very shallow', 'Shallow', 'Medium', 'Deep', 'Deeper', 'Very Deep', 'Extra Deep', 'Extreme Deep'};
        BSCorr = completeFor_Thalassa_SAT(BSCorr);
    otherwise
        strPingMode  = {'Very shallow', 'Shallow', 'Medium', 'Deep', 'Very Deep', 'Extra Deep', 'Extreme Deep'};
end
strSwathMode = {'Single swath', 'Dual Swath 1', 'Dual Swath 2'};

fid = fopen(nomFic, 'w+t');
if fid == -1
    messageErreurFichier(nomFic, 'WriteFailure');
    return
end

fprintf(fid, '# EM %s', SounderName(3:end)); % '# EM 304

for iMode=1:numel(BSCorr)
    % Ecriture de l'ent�te du mode Ping - Swath - Nb Secteurs.
    
    swathMode = BSCorr(iMode).swathMode;
    
    fprintf(fid, '# %s - %s \n', strPingMode{BSCorr(iMode).pingMode}, strSwathMode{swathMode});
    fprintf(fid, '%d\t%d\t%d\n', BSCorr(iMode).pingMode, BSCorr(iMode).swathMode - 1, BSCorr(iMode).nbSectors);
    
    % Ecriture des secteurs Port-Central-Starboard.
    for iSector=1:BSCorr(iMode).nbSectors
        fprintf(fid, '%s\n',    BSCorr(iMode).Sector(iSector).Label);
        if isfield(BSCorr(iMode).Sector(iSector), 'SL')
            fprintf(fid, '%8.3f\n', BSCorr(iMode).Sector(iSector).SL);
        end
        fprintf(fid, '%d\n',    BSCorr(iMode).Sector(iSector).nbNodes);
        % Ecriture des valeurs Angles-Gain
        for iNode=1:BSCorr(iMode).Sector(iSector).nbNodes
            % Ecriture en n�gatif de l'angle.
            fprintf(fid, '%8.3f\t%8.3f\n', -BSCorr(iMode).Sector(iSector).Node(iNode,1), ...
                                            BSCorr(iMode).Sector(iSector).Node(iNode,2));
        end
    end
end
fclose(fid);
flag = 1;


function BSCorr = completeFor_Thalassa_SAT(BSCorr)

TmpAngularLimits{2,1} = [-80 -35
                         -58   9
                          -8  59
                          40  79]; % Shallow Single swath
TmpAngularLimits{2,2} = [-79 -34
                         -62   9
                          -8  59
                          37  84]; % Shallow Dual swath 1 
TmpAngularLimits{2,3} = [-77 -36
                         -58   9
                          -8  59
                          35  78]; % Shallow Dual swath 2 
                    
TmpAngularLimits{3,1} = [-77 -34
                         -58   9
                          -8  59
                          40  79]; % Medium Single swath
TmpAngularLimits{3,2} = [-78 -35
                         -62   9
                          -8  63
                          39  78]; % Medium Dual swath 1
TmpAngularLimits{3,3} = [-78 -35
                         -58   9
                          -8  59
                          40  79]; % Medium Dual swath 2

TmpAngularLimits{4,1} = [-77 -34
                       -59 -26
                       -42  -9
                       -26   5
                        -4  27
                        10  43
                        27  60
                        39  80]; % Deep Single swath
TmpAngularLimits{4,2} = [-77 -34
                       -59 -26
                       -42  -9
                       -26   5
                        -4  27
                        10  43
                        27  60
                        39  80]; % Deep Double swath 1
TmpAngularLimits{4,3} = [-75 -36
                       -59 -26
                       -42  -9
                       -26   5
                        -4  27
                        10  43
                        27  60
                        39  80]; % Deep Double swath 2
                    
TmpAngularLimits{5,1} = [-77 -34
                         -59 -26
                         -42  -9
                         -26   5
                          -4  27
                          10  43
                          27  60
                          39  80]; % Deeper Single swath
TmpAngularLimits{5,2} = [-77 -34
                         -59 -26
                         -42  -9
                         -26   5
                          -4  27
                          10  43
                          27  60
                          39  80]; % Deeper Dual swath 1
TmpAngularLimits{5,3} = [-75 -36
                         -59 -26
                         -42  -9
                         -26   5
                          -4  27
                          10  43
                          27  60
                          39  80]; % Deeper Dual swath 2

TmpAngularLimits{6,1} = [-60 -25
                         -39 -12
                         -22   3
                          -2  23
                          13  40
                          26  61]; % Very deep Single swath
TmpAngularLimits{6,2} = TmpAngularLimits{6,1}; % Very deep Dual swath 1
TmpAngularLimits{6,3} = TmpAngularLimits{6,2}; % Very deep Dual swath 2

TmpAngularLimits{7,1} = [-38 -11
                         -21   4
                          -3  22
                          12  39]; % Extra deep Single swath
TmpAngularLimits{7,2} = [-40  -9
                         -21   4
                          -3  22
                          10  41]; % Extra deep Dual swath 1
TmpAngularLimits{7,3} = TmpAngularLimits{7,2}; % Extra deep Dual swath 2


TmpAngularLimits{8,1} = [-21   4
                          -3  22]; % Extreme deep Single swath
TmpAngularLimits{8,2} = TmpAngularLimits{8,1}; % Extra deep Dual swath 1
TmpAngularLimits{8,3} = TmpAngularLimits{8,2}; % Extra deep Dual swath 2

for iMode=1:numel(BSCorr)
    pingMode  = BSCorr(iMode).pingMode;
    swathMode = BSCorr(iMode).swathMode;
    
%     TmpAngularLimits{pingMode,swathMode}
    
    for iSector=1:BSCorr(iMode).nbSectors
        LimImposed = TmpAngularLimits{pingMode,swathMode}(iSector,:);
        AnglesImposed = LimImposed(1):LimImposed(2);
%         ValuesImposed = NaN(size(AnglesImposed));
        
        AnglesNodes = BSCorr(iMode).Sector(iSector).Node(:,1)';
        ValNodes    = BSCorr(iMode).Sector(iSector).Node(:,2)';
        
%         [~, ia, ib] = intersect(AnglesImposed, AnglesNodes);
%         ValuesImposed(ia) = ValNodes(ib);
%         ValuesImposed(1:(ia(1)-1))   = ValNodes(1);
%         ValuesImposed(ia(end)+1:end) = ValNodes(end);
        
        ValuesImposed = interp1(AnglesNodes, ValNodes, AnglesImposed);
        k = find(~isnan(ValuesImposed), 1, 'first');
        if ~isempty(k)
            ValuesImposed(1:(k-1))   = ValuesImposed(k);
        end
        k = find(isnan(ValuesImposed), 1, 'first');
        if ~isempty(k)
            ValuesImposed(k:end) = ValuesImposed(k-1);
        end
        
%         figure; plot(AnglesNodes, ValNodes, 'b*'); grid on; hold on;  plot(AnglesImposed, ValuesImposed, 'k')

        BSCorr(iMode).Sector(iSector).Node = [AnglesImposed' ValuesImposed'];
        BSCorr(iMode).Sector(iSector).nbNodes = size(BSCorr(iMode).Sector(iSector).Node,1);
    end
end
