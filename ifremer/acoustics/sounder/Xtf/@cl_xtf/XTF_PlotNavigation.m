function [Fig, Carto] = XTF_PlotNavigation(~, nomFic, varargin)

[varargin, Carto] = getPropertyValue(varargin, 'Carto', []);
[varargin, Fig]   = getPropertyValue(varargin, 'Fig',   []); %#ok<ASGLU>

%% Cr�ation de la fen�tre avec les menu et callbacks sp�cialis�s pour la nav

Fig = plot_navigation_Figure(Fig);

%% Traitement des fichiers

if ischar(nomFic)
    nomFic = {nomFic};
end
NbFic = length(nomFic);
str1 = 'Trac� de la navigation des .xtf';
str2 = 'Plot .xtf navigation';
hw = create_waitbar(Lang(str1,str2), 'N', NbFic);
for k=1:NbFic
    my_waitbar(k, NbFic, hw);
    
    a = cl_xtf('nomFic', nomFic{k});
    if isempty(a)
        %         str1 = sprintf('%s n''a pas pu �tre d�cod�.', nomFic{k});
        %         str2 = sprintf('%s could not be loaded.', nomFic{k});
        %         my_warndlg(Lang(str1,str2), 0);
        continue
    end
    
    [flag, SonarTime, Latitude, Longitude, ~, Heading] = read_navigation(a);
    if ~flag
        continue
    end
    
    plot_navigation(nomFic{k}, Fig, Longitude, Latitude, SonarTime.timeMat, Heading);
end
my_close(hw, 'MsgEnd');
