function Fig1 = XTF_PlotNavigationCoverage(~, nomFic, varargin)

% [Fig1, TypeDatagram, a, selection, SelectionMode] = plot_nav_XTF(this, nomFic, 'SelectionMode', 'Single');

%% Trace de la navigation de tous les fichiers

Fig1 = XTF_PlotNavigation(cl_xtf([]), nomFic);

%% Trac� de la couverture

Carto = [];
ColorOrder = get(gca, 'ColorOrder');
nbCoul = size(ColorOrder,1);
for k=1:length(nomFic)
    iCoul = mod(k-1, nbCoul) + 1;
    [Carto, SonarName] = XTF_PlotNavigationCoverage_unitaire(Carto, Fig1, ColorOrder(iCoul,:), nomFic{k});
end

FigName = sprintf('Sonar Name %s - Coverage', SonarName);
figure(Fig1);
set(Fig1, 'name', FigName)

str1 = 'Le trac� de la couverture des fichiers XTF est termin�e.';
str2 = 'The plot of XTF swaths is over.';
my_warndlg(Lang(str1,str2), 0, 'Tag', 'CalculEchoIntegration terminee', 'TimeDelay', 60);



function [Carto, SonarName] = XTF_PlotNavigationCoverage_unitaire(Carto, Fig1, Color, nomFic)

SonarName = [];

a = cl_xtf('nomFic', nomFic);

[flag, FileHeader] = SSc_ReadDatagrams(nomFic, 'Ssc_FileHeader', 'XMLOnly', 1);
if ~flag
    return
end

if FileHeader.NumberOfBathymetryChannels == 0
    [flag, PingChanHeader] = read_PingChanHeader_bin(a, 1);
    if ~flag
        return
    end
    
    ListSignals{1} = 'SensorPrimaryAltitude';
    [flag, Data] = SSc_ReadDatagrams(nomFic, 'Ssc_SideScan', 'Memmapfile', -1);
    if ~flag
        return
    end
    
    [flag, SonarTime, Latitude, Longitude, SonarSpeed, Heading] = read_navigation(a);
    if ~flag
        return
    end
    
    
    H = Data.SensorPrimaryAltitude(:);
    D = PingChanHeader.SlantRange(:); % TODO : trouver comment r�cup�rer le fr�quence d'�chantillonnage
    X = sqrt(D.^2 - H.^2);
    
    NbPings = length(Latitude);
    step = min(max(1, floor(NbPings/100)), 100);
    
    sub = [1 2:step:NbPings-1 NbPings];
    D_Tri = X(sub);
    D_Bab = - X(sub);
    Latitude = Latitude(sub);
    Longitude = Longitude(sub);
    Heading   = Heading(sub);
else
    % TODO : trouver une solution tr�s simple plut�t quer tout ce qui suit
    
    [flag, SonarTime, Latitude, Longitude, SonarSpeed, Heading] = read_navigation(a);
    if ~flag
        return
    end
    
    [DataBathy, b2, Carto] = read_Bathy(a, 'Carto', Carto); % TODO , 'subSidescan', Selection.subBathymetry);
    
    [c, this, flag] = sonar_range2depth(b2(2), b2(1), [], []);
    if ~flag
        return
    end
    I = get(c(2), 'Image');
    D_Bab = min(I, [], 2, 'omitnan');
    D_Tri = max(I, [], 2, 'omitnan');
end

[Lat, Lon] = get_faucheeInLatLong(Latitude, Longitude, Heading(:,1), D_Bab, D_Tri, zeros(size(D_Bab)), zeros(size(D_Tri)));

subNan = isnan(sum(Lon, 2)) | isnan(sum(Lat,2));
Lon(subNan, :) = [];
Lat(subNan, :) = [];
X = [Lon(:,1); flipud(Lon(:,2)); Lon(1,1)];
Y = [Lat(:,1); flipud(Lat(:,2)); Lat(1,1)];

figure(Fig1)
h = patch(X, Y, Color, 'FaceAlpha', 0.5, 'Tag', nomFic);
set_HitTest_Off(h)

SonarName = 'toto' %#ok<NOPRT>

axisGeo;
drawnow
    