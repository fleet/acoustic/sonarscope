% Trace de la navigation et des capteurs de vitesse et de cap d'un fichier .all
%
% Syntax
%   plot_position_data(nomFic, Invite)
%
% Input Arguments
%   nomFic : Liste de fichiers .all
%   Invite : Height | Heading | Speed | CourseVessel | SoundSpeed
%
% Examples
%   nomDir = '/home3/jackson/calimero/EM1002/ZoneB/B20040916_1'
%   nomDir = '/home1/jackson/FEMME2005'
%   nomDir = 'D:\NIWA_DATA\TAN0105'
%   liste = listeFicOnDir2(nomDir, '.all')
%   plot_position_data(liste, 'Heading')
%   plot_position_data(liste, 'Speed')
%   plot_position_data(liste, 'CourseVessel')
%   plot_position_data(liste, 'SoundSpeed')
%   plot_position_data(liste, 'Height')
%   plot_position_data(liste, 'Mode')
%   plot_position_data(liste, 'Time')
%   plot_position_data(liste, 'Drift')
%
% See also cl_simrad_all plot_index Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function XTF_PlotNavigationSignals(~, nomFic, Invite, varargin)

% [varargin, Carto] = getPropertyValue(varargin, 'Carto', []);

if ~iscell(nomFic)
    nomFic = {nomFic};
end

NbFic = length(nomFic);
% for k=1:NbFic
%     this(k) = cl_xtf('nomFic', nomFic{k}); %#ok
% end

nbColors = 64;
Colors = jet(nbColors);

Ident = rand(1);

Fig = figure;
set(Fig, 'UserData', Ident)
hold on;

%% Boucle sur les fichiers

XLim = [Inf -Inf];
YLim = [Inf -Inf];
ZLim = [Inf -Inf];
this = cl_xtf([]);
this(:) = [];
hw = create_waitbar('XTF files : reading signal', 'N', NbFic);
for k=1:NbFic
    my_waitbar(k, NbFic, hw);
    
    a = cl_xtf('nomFic', nomFic{k});
    if isempty(a)
        %         str1 = sprintf('%s n''a pas pu �tre d�cod�.', nomFic{k});
        %         str2 = sprintf('%s could not be loaded.', nomFic{k});
        %         my_warndlg(Lang(str1,str2), 0);
        continue
    end
    
    this(end+1) = a; %#ok<AGROW>
    
    [flag, SonarTime, Latitude, Longitude] = read_navigation(a); %#ok<ASGLU>
    if ~flag
        continue
    end
    
    [flag, Signal, Unit] = read_signal(a, Invite);
    if ~flag
        continue
    end
    
    switch lower(Invite)
        case {'heading'; 'sensorheading'}
            ZLim = [0 360];
        otherwise
            ZLim(1) = min(ZLim(1), double(min(Signal(:), [], 'omitnan')));
            ZLim(2) = max(ZLim(2), double(max(Signal(:), [], 'omitnan')));
    end
    XLim(1) = min(XLim(1), min(Longitude(:), [], 'omitnan'));
    XLim(2) = max(XLim(2), max(Longitude(:), [], 'omitnan'));
    YLim(1) = min(YLim(1), min(Latitude(:),  [], 'omitnan'));
    YLim(2) = max(YLim(2), max(Latitude(:),  [], 'omitnan'));
end
my_close(hw)

figure(Fig);
% if isempty(Unit)
%     title(Invite)
% else
%     title([Invite ' (' Unit ')']);
% end

%% Saisie des bornes de la table de couleurs

[flag, ZLim] = saisie_CLim(min(ZLim), max(ZLim), Unit, 'SignalName', Invite, 'hFigHisto', 87698);
if ~flag
    return
end

%% Trace de la navigation de tous les fichiers

Fig = XTF_PlotNavigation(cl_xtf([]), nomFic, 'Fig', Fig);
if isempty(Unit)
    title(Invite);
else
    title([Invite ' (' Unit ')'])
end

%% Trac� de l'�chelle des couleurs du signal

% figure(Fig);
W = axis;
x = linspace(XLim(1), XLim(1)+100*nbColors*eps(XLim(1)), nbColors);
y = linspace(YLim(1), YLim(2), nbColors);
h = imagesc(x, y, linspace(ZLim(1), ZLim(2), nbColors)); colormap(Colors), colorbar %#ok<NASGU>
% delete(h)
hold on;
axis(W)

%% Trac� des signaux

N = length(this);
hw = create_waitbar('XTF files : plottiong signal', 'N', N);
for iFic=1:N
    my_waitbar(iFic, N, hw);
    [flag, Signal] = read_signal(this(iFic), Invite);
    if ~flag
        continue
    end
    plot_position_data_unitaire(this(iFic), Signal, ZLim, Fig, nbColors, Colors);
end
my_close(hw, 'MsgEnd')
figure(Fig);


function plot_position_data_unitaire(this, Signal, ZLim, Fig, nbColors, Colors)

nomFic = this.nomFic;
figure(Fig)

%% Lecture de la navigation et du signal

[flag, SonarTime, Latitude, Longitude] = read_navigation(this); %#ok<ASGLU>
if ~flag
    return
end

%% Trac�

% nbSounders = size(Signal,2);
% if nbSounders == 2
%     str1 = 'Ce soundeur est dual, les donn�es peuvent �tre diff�rentes � b�bord et � tribord, pour l''instant je ne trace que les donn�es du sondeur tribord.';
%     str2 = 'This sounder is this dual one, for the moment I plot only data from the port one.';
%     my_warndlg(Lang(str1,str2), 0, 'Tag', 'PlotPositionDataNbSounders=2');
nbSounders = 1;
% end
for iCote=1:nbSounders
    figure(Fig(iCote))
    ZData = Signal(:,iCote);
    subNonNaN = find(~isnan(ZData));
    if length(subNonNaN) <= 1
        return
    end
    subNaN = find(isnan(ZData));
    ZData(subNaN) = interp1(subNonNaN, ZData(subNonNaN), subNaN, 'linear', 'extrap');
    %     Value = interp1(Time, ZData, Time);
    Value = ZData;
    if ZLim(2) == ZLim(1)
        ZLim(1) = ZLim(1) - 0.5;
        ZLim(2) = ZLim(2) + 0.5;
    end
    coef = (nbColors-1) / (ZLim(2) - ZLim(1));
    Value = ceil((Value - ZLim(1)) * coef);
    
    % --------------------------
    % Affichage de la navigation
    
    for k=1:nbColors
        sub = find(Value == (k-1));
        if ~isempty(sub)
            h = plot(Longitude(sub), Latitude(sub), '+');
            set(h, 'Color', Colors(k,:), 'Tag', nomFic)
            set_HitTest_Off(h)
        end
    end
    
    sub = find(Value < 0);
    if ~isempty(sub)
        h = plot(Longitude(sub), Latitude(sub), '+');
        set(h, 'Color', Colors(1,:)*0.8, 'Tag', nomFic)
        set_HitTest_Off(h)
    end
    
    sub = find(Value > (nbColors-1));
    if ~isempty(sub)
        h = plot(Longitude(sub), Latitude(sub), '+');
        set(h, 'Color', Colors(nbColors,:)*0.8, 'Tag', nomFic)
    end
end

axisGeo;
grid on;
% colorbar % Supprim� le 7/11/2006 car ca remet les infos de colorbar de 0
% � 64
drawnow
