function XTF_ReadOnce(this, nomFic, varargin) %#ok<INUSL>

if ~iscell(nomFic)
    nomFic = {nomFic};
end

N = length(nomFic);
str1 = 'Traitement en cours';
str2 = 'XTF files preprocessing';
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    try
        a = cl_xtf('nomFic', nomFic{k}); %#ok<NASGU>
    catch ME
        ErrorReport = getReport(ME) %#ok<NOPRT>
%         SendEmailSupport(ErrorReport)
        renameBadFile(nomFic{k}, 'Message', ErrorReport)
    end
end
my_close(hw, 'MsgEnd');
