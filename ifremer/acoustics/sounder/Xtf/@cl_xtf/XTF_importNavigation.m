function XTF_importNavigation(~, nomFicXtf, nomFicNav)

my_warndlg('This functionality is not available yet. Please send your .xtf and .nvi file to sonarscope@ifremer.fr', 1);
return

if ~iscell(nomFicXtf)
    nomFicXtf = {nomFicXtf};
end

Carto = [];
N = length(nomFicXtf);
hw = create_waitbar('XTF navigation import', 'N', N);
for k=1:N
    my_waitbar(k, N, hw);
    [flag, Carto] = XTF_importNavigation_unitaire(nomFicXtf{k}, nomFicNav, Carto);
    if ~flag
        my_close(hw)
        return
    end
end
my_close(hw, 'MsgEnd')


function [flag, Carto] = XTF_importNavigation_unitaire(nomFicXtf, nomFicNav, Carto)

%% Lecture de l'image

this = cl_xtf('nomFic', nomFicXtf);

[flag, SonarTime, Latitude, Longitude] = read_navigation(this);
if ~flag
    return
end

T = SonarTime.timeMat;

LatitudeImage  = Latitude;
LongitudeImage = Longitude;

for k=1:length(nomFicNav)
	[flag, Latitude, Longitude, tNavBab] = lecFicNav(nomFicNav{k});
    if ~flag
        messageErreurFichier(nomFicNav, 'ReadFailure');
        return
    end
    
    tNavBab = tNavBab.timeMat;
    
    LongitudeXTF(:,1) = my_interp1_longitude(tNavBab, Longitude(:,1), T(:,1));
    LatitudeXTF(:,1)  = my_interp1(          tNavBab, Latitude(:,1),  T(:,1));
    
    subNonNaN = find(~isnan(LongitudeXTF(:,1)));
    if ~isempty(subNonNaN)
        LatitudeImage(subNonNaN, 1)  = LatitudeXTF(subNonNaN, 1);
        LongitudeImage(subNonNaN, 1) = LongitudeXTF(subNonNaN, 1);
    end
end

flag = save_signal_SensorYCoordinate(a, FileHeader.NavUnits, LatitudeImage, LongitudeImage, Carto);
if ~flag
    return
end

flag = save_signal_SensorXCoordinate(a, FileHeader.NavUnits, LongitudeImage, LatitudeImage, Carto);
if ~flag
    return
end


