% Representation externe d'une instance dans une chaine de caracteres
%
% Syntax 
%   s = char(a)
%
% Input Arguments
%   a : instance ou tableau d'instances de cl_xtf
%
% Output Arguments
%   s : chaine de caracteres
%
% Examples
%   nomFic = getNomFicDatabase('REBENT166.XTF')
%   nomFic = getNomFicDatabase('demowreck.XTF')
%   nomFic = getNomFicDatabase('demoplane.XTF')
%   a = cl_xtf('nomFic', nomFic);
%   str = char(a)
%
% See also cl_xtf Authors
% Authors : GLU
% VERSION  : $Id: char.m,v 1.2 2006/11/19 15:41:55 rgallou Exp rgallou $
% ----------------------------------------------------------------------------

% TODO : re�crire tous les char de cl_simrad, cl_hac, ... sous forme de
% clar_unitaire
function str = char(this)

str = [];
[m, n] = size(this);

if (m*n) > 1

    % -------------------
    % Tableau d'instances

    for i=1:m
        for j=1:n
            str{end+1} = sprintf('-----Element (%d,%d) -------', i, j); %#ok<AGROW>
            str1 = char(this(i, j));
            str{end+1} = str1; %#ok<AGROW>
        end
    end
    str = cell2str(str);

else

    % ---------------
    % Instance unique

    str = char_instance(this);
    str = [repmat(' ', size(str,1), 2) str];
end
