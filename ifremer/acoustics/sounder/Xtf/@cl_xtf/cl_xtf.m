% Constructeur de cl_xtf (Container de donn�es contenue dans une .xtf)
%
% Syntax
%   a = cl_xtf(...)
% 
% Name-Value Pair Arguments
%   nomFic : Nom(s) du fichier .xtf
%  
% Output Arguments 
%   a : Instance de cl_xtf
%
% Examples
%   nomFic = getNomFicDatabase('REBENT166.XTF')
%   a = cl_xtf('nomFic', nomFic)
%
% See also plot_index histo_index Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function this = cl_xtf(varargin)

%% D�finition de la structure

this.nomFic      = '';
this.nomFicIndex = '';

%% Cr�ation de l'instance

this = class(this, 'cl_xtf');

%% On regarde si l'objet a �t� cr�� uniquement pour atteindre une m�thode de la classe

if (nargin == 1) && isempty(varargin{1})
    return
end

%% Initialisation des valeurs transmises lors de l'appel au constructeur

this = set(this, varargin{:});
