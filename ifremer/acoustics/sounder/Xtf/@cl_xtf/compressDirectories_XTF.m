function compressDirectories_XTF(this, nomFic, varargin) %#ok<INUSL>

if ~iscell(nomFic)
    nomFic = {nomFic};
end

N = length(nomFic);
hw = create_waitbar('Processing', 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    zipSonarScopeCache(nomFic{k})
end
my_close(hw, 'MsgEnd')
