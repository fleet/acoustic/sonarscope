function flag = create_XmlBin_Bathymetry(this, Data)

FormatVersion = 20120609;

[nomDir, nom] = fileparts(this.nomFic);
nomDirRacine = fullfile(nomDir, 'SonarScope', nom, 'PingBeam');

%% Cr�ation du r�pertoire PingBeam

if ~exist(nomDirRacine, 'dir')
    status = mkdir(nomDirRacine);
    if ~status
        messageErreur(nomDirRacine)
        flag = 0;
        return
    end
end

%% G�n�ration du XML SonarScope

Info.Title                  = 'Depth';
Info.Constructor            = 'XTF-Format';
% Info.TimeOrigin             = '01/01/-4713';
Info.Comments               = 'Bathymetry computed from Rho-Teta';
Info.FormatVersion          = FormatVersion;

Info.Dimensions.nbPings     = size(Data.Depth, 1);
Info.Dimensions.nbBeams     = size(Data.Depth, 2);

Info.Signals = [];
% Info.Signals(1).Name          = 'Time';
% Info.Signals(end).Dimensions  = 'nbPings, nbSounders';
% Info.Signals(end).Storage     = 'double';
% Info.Signals(end).Unit        = 'days since JC';
% Info.Signals(end).FileName    = fullfile('Ssc_Bathymetry','Time.bin');
% Info.Signals(end).Tag         = verifKeyWord('SounderTime');
% 
% Info.Signals(end+1).Name      = 'PingCounter';
% Info.Signals(end).Dimensions  = 'nbPings, nbSounders';
% Info.Signals(end).Storage     = 'single';
% Info.Signals(end).Unit        = '';
% Info.Signals(end).FileName    = fullfile('Ssc_Bathymetry','PingCounter.bin');
% Info.Signals(end).Tag         = verifKeyWord('SounderPingCounter');
% 
% Info.Signals(end+1).Name      = 'SystemSerialNumber';
% Info.Signals(end).Dimensions  = 'nbPings, nbSounders';
% Info.Signals(end).Storage     = 'single';
% Info.Signals(end).Unit        = '';
% Info.Signals(end).FileName    = fullfile('Ssc_Bathymetry','SystemSerialNumber.bin');
% Info.Signals(end).Tag         = verifKeyWord('SounderSerialNumber');
% 
% Info.Signals(end+1).Name      = 'Heading';
% Info.Signals(end).Dimensions  = 'nbPings, nbSounders';
% Info.Signals(end).Storage     = 'single';
% Info.Signals(end).Unit        = 'deg';
% Info.Signals(end).FileName    = fullfile('Ssc_Bathymetry','Heading.bin');
% Info.Signals(end).Tag         = verifKeyWord('SounderHeading');
% 
% Info.Signals(end+1).Name      = 'SoundSpeed';
% Info.Signals(end).Dimensions  = 'nbPings, nbSounders';
% Info.Signals(end).Storage     = 'single';
% Info.Signals(end).Unit        = 'm/s';
% Info.Signals(end).FileName    = fullfile('Ssc_Bathymetry','SoundSpeed.bin');
% Info.Signals(end).Tag         = verifKeyWord('SounderSoundSpeed');
% 
% Info.Signals(end+1).Name      = 'TransducerDepth';
% Info.Signals(end).Dimensions  = 'nbPings, nbSounders';
% Info.Signals(end).Storage     = 'single';
% Info.Signals(end).Unit        = 'm';
% Info.Signals(end).FileName    = fullfile('Ssc_Bathymetry','TransducerDepth.bin');
% Info.Signals(end).Tag         = verifKeyWord('SounderTransducerDepth');
% 
% Info.Signals(end+1).Name      = 'MaxNbBeamsPossible';
% Info.Signals(end).Dimensions  = 'nbPings, nbSounders';
% Info.Signals(end).Storage     = 'single';
% Info.Signals(end).Unit        = '';
% Info.Signals(end).FileName    = fullfile('Ssc_Bathymetry','MaxNbBeamsPossible.bin');
% Info.Signals(end).Tag         = verifKeyWord('TODO');
% 
% Info.Signals(end+1).Name      = 'NbBeams';
% Info.Signals(end).Dimensions  = 'nbPings, nbSounders';
% Info.Signals(end).Storage     = 'uint8';
% Info.Signals(end).Unit        = '';
% Info.Signals(end).FileName    = fullfile('Ssc_Bathymetry', 'NbBeams.bin');
% Info.Signals(end).Tag         = verifKeyWord('SounderNbBeams');
%      
% Info.Signals(end+1).Name      = 'ZResol';
% Info.Signals(end).Dimensions  = 'nbPings, nbSounders';
% Info.Signals(end).Storage     = 'single';
% Info.Signals(end).Unit        = '';
% Info.Signals(end).FileName    = fullfile('Ssc_Bathymetry','ZResol.bin');
% Info.Signals(end).Tag         = verifKeyWord('TODO');
% 
% Info.Signals(end+1).Name      = 'XYResol';
% Info.Signals(end).Dimensions  = 'nbPings, nbSounders';
% Info.Signals(end).Storage     = 'single';
% Info.Signals(end).Unit        = '';
% Info.Signals(end).FileName    = fullfile('Ssc_Bathymetry','XYResol.bin');
% Info.Signals(end).Tag         = verifKeyWord('TODO');
% 
% Info.Signals(end+1).Name      = 'SamplingRate';
% Info.Signals(end).Dimensions  = 'nbPings, nbSounders';
% Info.Signals(end).Storage     = 'single';
% Info.Signals(end).Unit        = 'Hz';
% Info.Signals(end).FileName    = fullfile('Ssc_Bathymetry','SamplingRate.bin');
% Info.Signals(end).Tag         = verifKeyWord('SounderSamplingRate');
% 
% Info.Signals(end+1).Name      = 'TransducerDepthOffsetMult';
% Info.Signals(end).Dimensions  = 'nbPings, nbSounders';
% Info.Signals(end).Storage     = 'single';
% Info.Signals(end).Unit        = '';
% Info.Signals(end).FileName    = fullfile('Ssc_Bathymetry','TransducerDepthOffsetMult.bin');
% Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Images(1).Name           = 'Depth';
Info.Images(end).Dimensions   = 'nbPings, nbBeams';
Info.Images(end).Storage      = 'single';
Info.Images(end).Unit         = 'm';
Info.Images(end).FileName     = fullfile('Ssc_Bathymetry','Depth.bin');
Info.Images(end).Tag          = verifKeyWord('SounderDepth');

Info.Images(end+1).Name       = 'AcrossDist';
Info.Images(end).Dimensions   = 'nbPings, nbBeams';
Info.Images(end).Storage      = 'single';
Info.Images(end).Unit         = 'm';
Info.Images(end).FileName     = fullfile('Ssc_Bathymetry','AcrossDist.bin');
Info.Images(end).Tag          = verifKeyWord('SounderAcrossDist');

Info.Images(end+1).Name       = 'AlongDist';
Info.Images(end).Dimensions   = 'nbPings, nbBeams';
Info.Images(end).Storage      = 'single';
Info.Images(end).Unit         = 'm';
Info.Images(end).FileName     = fullfile('Ssc_Bathymetry','AlongDist.bin');
Info.Images(end).Tag          = verifKeyWord('SounderAlongDist');

Info.Images(end+1).Name       = 'RxBeamAngle';
Info.Images(end).Dimensions   = 'nbPings, nbBeams';
Info.Images(end).Storage      = 'single';
Info.Images(end).Unit         = 'deg';
Info.Images(end).FileName     = fullfile('Ssc_Bathymetry','RxBeamAngle.bin');
Info.Images(end).Tag          = verifKeyWord('SounderBeamAngle');

% Info.Images(end+1).Name       = 'BeamAzimuthAngle';
% Info.Images(end).Dimensions   = 'nbPings, nbBeams';
% Info.Images(end).Storage      = 'single';
% Info.Images(end).Unit         = 'deg';
% Info.Images(end).FileName     = fullfile('Ssc_Bathymetry','BeamAzimuthAngle.bin');
% Info.Images(end).Tag          = verifKeyWord('SounderAzimuthAngle');

Info.Images(end+1).Name       = 'Reflectivity';
Info.Images(end).Dimensions   = 'nbPings, nbBeams';
Info.Images(end).Storage      = 'single';
Info.Images(end).Unit         = '?';
Info.Images(end).FileName     = fullfile('Ssc_Bathymetry','Reflectivity.bin');
Info.Images(end).Tag          = verifKeyWord('SounderDetectionNbSamples');

Info.Images(end+1).Name       = 'TwoWayTravelTime';
Info.Images(end).Dimensions   = 'nbPings, nbBeams';
Info.Images(end).Storage      = 'single';
Info.Images(end).Unit         = 's';
Info.Images(end).FileName     = fullfile('Ssc_Bathymetry','TwoWayTravelTime.bin');
Info.Images(end).Tag          = verifKeyWord('SounderDetectionNbSamples');

Info.Images(end+1).Name       = 'Mask';
Info.Images(end).Dimensions   = 'nbPings, nbBeams';
Info.Images(end).Index        = 'nbPings, nbBeams';
Info.Images(end).Storage      = 'uint8';
Info.Images(end).Unit         = '';
Info.Images(end).FileName     = fullfile('Ssc_Bathymetry','Mask.bin');
Info.Images(end).Tag          = verifKeyWord('Mask');

%% Cr�ation du fichier XML d�crivant la donn�e 

nomFicXml = fullfile(nomDirRacine, 'Ssc_Bathymetry.xml');
xml_write(nomFicXml, Info);
flag = exist(nomFicXml, 'file');
if ~flag
    messageErreur(nomFicXml)
    return
end
% edit(nomFicXml)

%% Cr�ation du r�pertoire Depth

nomDirDepth = fullfile(nomDirRacine, 'Ssc_Bathymetry');
if ~exist(nomDirDepth, 'dir')
    status = mkdir(nomDirDepth);
    if ~status
        messageErreur(nomDirDepth)
        flag = 0;
        return
    end
end

%% Cr�ation des fichiers binaires des signaux

for i=1:length(Info.Signals)
    flag = writeSignal(nomDirRacine, Info.Signals(i), Data.(Info.Signals(i).Name));
    if ~flag
        return
    end
end

%% Cr�ation des fichiers binaires des images

for i=1:length(Info.Images)
    flag = writeImage(nomDirRacine, Info.Images(i), Data.(Info.Images(i).Name));
    if ~flag
        return
    end
end

flag = 1;
