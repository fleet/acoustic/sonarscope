% Representation externe d'une instance sur le terminal
%
% Syntax
%   display(this)
%
% Input Arguments
%   this : une instance de cl_xtf
%
% Examples
%   nomFic = getNomFicDatabase('REBENT166.XTF')
%   a = cl_xtf('nomFic', nomFic);
%   display(a);
%
% See also cl_xtf cl_xtf/char Authors
% Authors : GLU
% ----------------------------------------------------------------------------

function display(this)

s = whos('this');
sz = s.size;
fprintf('Name\tSize\tClass\n');
fprintf('%s\t%s\t%s\n', inputname(1), num2strCode(sz), s.class);
disp(char(this));
