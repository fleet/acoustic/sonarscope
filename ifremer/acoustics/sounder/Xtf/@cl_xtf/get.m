% Acces en lecture des proprietes d'une instance
%
% Syntax
%   [...] = get(this, ...)
% 
% Input Arguments 
%   this : Une instance de la classe cl_xtf
%
% Name-Value Pair Arguments
%   nomFic : Nom du fichier .xtf
%
% Output Arguments
%   PropertyValues associees aux PropertyNames passes en argument
% 
% Examples
%   nomFic = getNomFicDatabase('REBENT166.XTF')
%   a = cl_xtf('nomFic', nomFic)
%   nomFic = get(a, 'nomFic')
%
% See also cl_xtf cl_xtf/char Authors
% Authors : GLU
% ----------------------------------------------------------------------------

function varargout = get(this, varargin)

%% Boucle de parcours des arguments

varargout = {};
for k=1:length(varargin)
    switch varargin{k}
        case 'nomFic'
            varargout{end+1} = this.nomFic; %#ok<AGROW>
        case 'nomFicIndex'
            varargout{end+1} = this.nomFicIndex; %#ok<AGROW>

        otherwise
            w = sprintf('cl_xtf/get %s non pris en compte', varargin{k});
            my_warndlg(w, 0);
    end
end
