function [flag, Time] = get_BathyTime(this, varargin)

[varargin, Bathy] = getPropertyValue(varargin, 'Bathy', []); %#ok<ASGLU>

[flag, Bathy] = SSc_ReadDatagrams(this.nomFic, 'Ssc_Bathy', 'Memmapfile', -1);
if ~flag
    return
end

Time = XTF_YJdHMSHs2Time(Bathy);
