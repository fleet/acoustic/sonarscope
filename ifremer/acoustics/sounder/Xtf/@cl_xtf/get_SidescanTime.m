function [flag, Time] = get_SidescanTime(this, varargin)

[varargin, SideScan] = getPropertyValue(varargin, 'SideScan', []); %#ok<ASGLU>

Time = [];

[flag, SideScan] = SSc_ReadDatagrams(this.nomFic, 'Ssc_SideScan', 'Memmapfile', -1);
if ~flag
    return
end

Time = XTF_YJdHMSHs2Time(SideScan);
