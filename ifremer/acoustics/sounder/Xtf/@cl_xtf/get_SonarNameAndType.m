function [SonarName, SonarType] = get_SonarNameAndType(this)

typeIndian = 'l';
fid = fopen(this.nomFic, 'r', typeIndian);
if fid == -1
    str = ['Impossible d''ouvrir le fichier ' this.nomFic];
    my_warndlg(str, 1);
    return
end

% Lecture du XTFFileHeader
[flag, XTFFileHeader] = fread_FileHeader(fid);
if ~flag
    return
end

SonarName = XTFFileHeader.SonarName';
SonarType = XTFFileHeader.SonarType;

fclose(fid);
