function SerialNumber = get_SonarSerialNumber(this)

Data = read_7kBeamGeometry(this);
SerialNumber = Data.BeamGeometry(1).RTH.SonarId;
