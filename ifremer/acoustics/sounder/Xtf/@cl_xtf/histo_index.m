% Histogramme des type de datagrams contenus dans un fichier .xtf
%
% Syntax
%   histo_index(a)
%
% Input Arguments
%   a : Instance de cl_xtf
%
% Name-Value Pair Arguments
%   fullListe : 1=affichage que des datagrams presents, 0=tous les datagrams
%
% Name-only Arguments
%   Total : Trace du bilan total uniquement
%
% Output Arguments
%   []                : Auto-plot activation
%   texteTypeDatagram : Resume textuel de l'histogramme
%   Code              : Abscisses
%   histoTypeDatagram : Nb de datagrammes (ordonnees)
%
% Examples
%   nomFic = getNomFicDatabase('REBENT166.XTF')
%   a = cl_xtf('nomFic', nomFic);
%   histo_index(a)
%   [texteTypeDatagram] = histo_index(a)
%   [texteTypeDatagram, Code, histoTypeDatagram] = histo_index(a)
%   figure; bar(Code, histoTypeDatagram)
%
% See also cl_simrad_all plot_index Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [texteTypeDatagram, Code, histoTypeDatagram] = histo_index(this, varargin)

flagPlot = (nargout == 0);

NbFic = length(this);
hw = create_waitbar('Plot .all Histo Index', 'N', NbFic);
for i=1:NbFic
    [flag, texteTypeDatagram, c, h, t] = histo_index_unitaire(this(i), flagPlot, varargin{:});
    if flag
        Code(i,:) = c; %#ok<AGROW>
        histoTypeDatagram(i,:) = h; %#ok<AGROW>
        tabNbBytes(i,:) = t; %#ok<AGROW>
    end
end
my_close(hw, 'MsgEnd')

if NbFic > 1
    %     Code = Code(1,:);
    Code = max(Code); % Pour pb fichiers pas d�cod�s, Code � 0 partout
    histoTypeDatagram = sum(histoTypeDatagram);
    tabNbBytes = sum(tabNbBytes);
    
    flagPlot = (nargout == 0);
    if flagPlot
        plot_histo_index(Code, histoTypeDatagram, texteTypeDatagram, tabNbBytes, 'All files')
    end
end


function [flag, texteTypeDatagram, Code, histoTypeDatagram, tabNbBytes] = histo_index_unitaire(this, flagPlot, varargin)

[varargin, fullListe] = getPropertyValue(varargin, 'fullListe', 0); %#ok<ASGLU>

[flag, Data] = read_FileIndex_bin(this);
if ~flag
    texteTypeDatagram = [];
    Code = [];
    histoTypeDatagram = [];
    tabNbBytes = [];
    return
end
TypeDatagram = Data.TypeOfDatagram;

listeTypeDatagram = unique(TypeDatagram);
nbTypeDatagrams = length(listeTypeDatagram);
nbBytes = zeros(1,nbTypeDatagrams );
for i=1:nbTypeDatagrams
    sub = (TypeDatagram == listeTypeDatagram(i));
    nbBytes(i) = sum(Data.NbOfBytesInDatagram(sub));
    fprintf('Datagramm %3d    Nb of Bytes %10d\n', listeTypeDatagram(i), nbBytes(i));
end

% --------------------------
% Lecture du fichier d'index

Code = min(TypeDatagram):max(TypeDatagram);
histoTypeDatagram = my_hist(TypeDatagram, Code);
if isempty(histoTypeDatagram)
    texteTypeDatagram = 'Empty';
    tabNbBytes = 0;
    histoTypeDatagram = 0;
    return
end
% texteTypeDatagram = codesDatagramsSimrad(Code, histoTypeDatagram, 'fullListe', fullListe);
texteTypeDatagram = codesDatagramsXTF(Code, histoTypeDatagram, 'fullListe', fullListe);

tabNbBytes = zeros(size(histoTypeDatagram));
tabNbBytes(histoTypeDatagram ~= 0) = nbBytes;
if flagPlot
    plot_histo_index(Code, histoTypeDatagram, texteTypeDatagram, tabNbBytes, this.nomFic)
end
