% -------------------------------------------------------------------------

function [DataType, ColormapIndex] = identSonarDataType(this, nomLayer) %#ok

ColormapIndex = 3;    % jet par defaut

switch nomLayer
    case {'Depth'; 'Depth-Heave'}
        DataType = cl_image.indDataType('Bathymetry');
    case 'AcrossDist'
        DataType = cl_image.indDataType('AcrossDist');
    case 'AlongDist'
        DataType = cl_image.indDataType('AlongDist');
    case 'BeamDepressionAngle'
        DataType = cl_image.indDataType('TxAngle');
    case 'Raw-BeamPointingAngle'
        DataType = cl_image.indDataType('RxBeamAngle');
    case 'BeamAzimuthAngle'
        DataType = cl_image.indDataType('BeamAzimuthAngle');
    case {'Range'; 'Raw-Range'}
        % EN ATTENDANT LA TRANSFORMATION DANS LECTURE DATAGRAM
%         DataType = cl_image.indDataType('RayPathSampleNb');
        DataType = cl_image.indDataType('RayPathLength'); % One way length in meters
    case 'LongueurTrajet'
        DataType = cl_image.indDataType('RayPathLength'); % One way length in meters
    case 'QualityFactor'
        DataType = cl_image.indDataType('KongsbergQualityFactor');
    case 'DetectionBrightness'
        DataType = cl_image.indDataType('ResonDetectionBrightness');
    case 'DetectionColinearity'
        DataType = cl_image.indDataType('ResonDetectionColinearity');
    case 'LengthDetection'
        DataType = 1;
    case {'Reflectivity'; 'Raw-Reflectivity'}
        DataType = cl_image.indDataType('Reflectivity');
        ColormapIndex = 2;
    case 'Detection'
        DataType = cl_image.indDataType('DetectionType');
    case {'DetecAmplitudeNbSamples'; 'DetecPhaseOrdrePoly'; 'DetecPhaseVariance'}
        DataType = 1;
    case 'Temps universel'
        DataType = cl_image.indDataType('RxTime');
    case 'Raw-TransmitTiltAngle'
%         message = sprintf('Le layer %s ne semble pas etre un layer de Simrad. Il est donc assimile comme "Other"', nomLayer);
%         msgbox(message)
        DataType = 1;
    case 'RayPathTravelTime'
        DataType = cl_image.indDataType('RayPathTravelTime');
    case 'Mask'
        DataType = cl_image.indDataType('Mask');
    case 'Probability'
        DataType = cl_image.indDataType('Proba');
    case 'TxBeamIndex'
        DataType = cl_image.indDataType('TxBeamIndex');
    case 'MbesQualityFactor'
        DataType = cl_image.indDataType('MbesQualityFactor');
        
    otherwise
%         message = sprintf('Le layer %s ne semble pas etre un layer de Reson. Il est donc assimile comme "Other"', nomLayer);
%         my_warndlg(message, 0);
        DataType = 1;
end

