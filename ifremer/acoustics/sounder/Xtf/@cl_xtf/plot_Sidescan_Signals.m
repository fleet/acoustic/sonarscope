function FigOut = plot_Sidescan_Signals(this, varargin)

[varargin, Fig] = getPropertyValue(varargin, 'Fig', []); %#ok<ASGLU>

N = length(this);
ListeFig = NaN(1,N);
for k=1:N
    [flag, FigOut] = plot_Sidescan_Signals_unitaire(this(k), Fig);
    if flag
        ListeFig(k) = FigOut;
    end
end

function [flag, Fig] = plot_Sidescan_Signals_unitaire(this, Fig)

% ListSignals{1} = 'SensorRoll';
% ListSignals{end+1} = 'SensorPitch';
% ListSignals{end+1} = 'Heave';
% ListSignals{end+1} = 'SensorHeading';
% ListSignals{end+1} = 'ShipSpeed';
% ListSignals{end+1} = 'LayBack';
% ListSignals{end+1} = 'PingNumber';
% ListSignals{end+1} = 'SensorDepth';
% ListSignals{end+1} = 'WaterTemperature';
% ListSignals{end+1} = 'SoundVelocity';
% ListSignals{end+1} = 'SensorPrimaryAltitude';
% ListSignals{end+1} = 'SensorYCoordinate';
% ListSignals{end+1} = 'SensorXCoordinate'; %#ok<NASGU>

[flag, FileHeader] = SSc_ReadDatagrams(this.nomFic, 'Ssc_FileHeader', 'XMLOnly', 1);
if ~flag
    return
end

if FileHeader.NumberOfBathymetryChannels ~= 0
    [flag, SideScan] = SSc_ReadDatagrams(this.nomFic, 'Ssc_Bathy', 'Memmapfile', -1);
else
    [flag, SideScan] = SSc_ReadDatagrams(this.nomFic, 'Ssc_SideScan', 'Memmapfile', -1);
end
if ~flag
    return
end

Roll        = SideScan.SensorRoll(:,1);
Pitch       = SideScan.SensorPitch(:,1);
Heave       = SideScan.Heave(:,1);
Heading     = SideScan.SensorHeading(:,1);
Speed       = SideScan.ShipSpeed(:,1); % ShipSpeed ou SensorSpeed
Layback     = SideScan.LayBack(:,1);
PingCounter = SideScan.PingNumber(:,1);
Immersion   = SideScan.SensorDepth(:,1);
Temperature = SideScan.WaterTemperature(:,1);
SoundVelocity = SideScan.SoundVelocity(:,1);
SonarHeight   = SideScan.SensorPrimaryAltitude(:,1);
Ycoordinate   = SideScan.SensorYCoordinate(:,1);
Xcoordinate   = SideScan.SensorXCoordinate(:,1);

if isempty(Fig) || ~ishandle(Fig)
    Fig = figure('Name', ['  -  Sidescan Signals from ' this.nomFic]);
else
    Fig = figure(Fig);
    set(Fig, 'Name', ['  -  Sidescan Signals from ' this.nomFic]);
end
hAxe = preallocateGraphics(0,0); nA = 7; k = 1;
hAxe(1) = subplot(nA,4,k); PlotUtils.createSScPlot(-SonarHeight); grid on; title('SonarHeight (m)'); k = k+1;
hAxe(end+1) = subplot(nA,4,k); PlotUtils.createSScPlot(Roll); grid on; title('Roll (deg)'); k = k+3;

hAxe(end+1) = subplot(nA,4,k); PlotUtils.createSScPlot(-Immersion); grid on; title('Immersion (m)'); k = k+1;
hAxe(end+1) = subplot(nA,4,k); PlotUtils.createSScPlot(Pitch); grid on; title('Pitch (deg)'); k = k+3;

hAxe(end+1) = subplot(nA,4,k); PlotUtils.createSScPlot(-(SonarHeight+Immersion)); grid on; title('SonarHeight+Immersion (m)'); k = k+1;
hAxe(end+1) = subplot(nA,4,k); PlotUtils.createSScPlot(Temperature); grid on; title('Temperature (�c)'); k = k+3;

hAxe(end+1) = subplot(nA,4,k); PlotUtils.createSScPlot(Speed); grid on; title('Speed (m/s)'); k = k+1;
hAxe(end+1) = subplot(nA,4,k); PlotUtils.createSScPlot(PingCounter); grid on; title('PingCounter'); k = k+3;

hAxe(end+1) = subplot(nA,4,k); PlotUtils.createSScPlot(Layback); grid on; title('Layback (m)'); k = k+1;
hAxe(end+1) = subplot(nA,4,k); PlotUtils.createSScPlot(SoundVelocity); grid on; title('SoundVelocity (m/s)'); k = k+3;

hAxe(end+1) = subplot(nA,4,k); PlotUtils.createSScPlot(Heading); grid on; title('Heading (deg)'); k = k+1;
hAxe(end+1) = subplot(nA,4,k); PlotUtils.createSScPlot(Heave); grid on; title('Heave (m)'); k = k+3;

hAxe(end+1) = subplot(nA,4,k); PlotUtils.createSScPlot(Xcoordinate); grid on; title('Xcoordinate'); k = k+1;
hAxe(end+1) = subplot(nA,4,k); PlotUtils.createSScPlot(Ycoordinate); grid on; title('Ycoordinate'); k = k+3;

linkaxes(hAxe, 'x'); axis tight; drawnow;

sub = (0:(nA-1))*4;
sub = [sub+3 sub+4];
sub = sort(sub);
subplot(nA,4, sub); PlotUtils.createSScPlot(Xcoordinate, Ycoordinate); grid on; title('Navigation'); k = k+1; %#ok<NASGU>

% figure; plot(Xcoordinate, Ycoordinate); grid on; title('Navigation');
