% Affichage des informations contenues dans les datagrams "attitude" d'un
% fichier .xtf
%
% Syntax
%   plot_attitude(a)
%
% Input Arguments
%   a : Instance de cl_xtf
%
% Examples
%   nomFic = getNomFicDatabase('REBENT166.XTF')
%   nomFic = getNomFicDatabase('demowreck.XTF')
%   nomFic = getNomFicDatabase('demoplane.XTF')
%   a = cl_xtf('nomFic', nomFic);
%   plot_attitude(a)
%   plot_attitude(a, 'sub', 1:1000)
%
% See also cl_xtf Authors
% Authors : GLU
% ----------------------------------------------------------------------------

function plot_attitude(this, varargin)

[varargin, sub] = getPropertyValue(varargin, 'sub', []);
[varargin, Fig] = getPropertyValue(varargin, 'Fig', []); %#ok<ASGLU>

if isempty(Fig)
    Fig = figure('name', 'Attitude datagrams');
else
    figure(Fig);
end

for k=1:length(this)
    plot_attitude_unitaire(this(k), sub,  Fig)
end


function plot_attitude_unitaire(this, sub, Fig)

%% Lecture des datagrams d'attitude

Data = read_attitude(this);
if isempty(Data)
    my_close(Fig)
    my_warndlg(Lang('Les paquets Attitude n''existent pas dans le fichier !','Attitude Packets don''t exist in the file'),1);
    return
end

%% Affichage de la navigation

[~, nomFic] = fileparts(this.nomFic);

% FigName = sprintf('Sondeur %s Sonar Type  %d', Data.SonarName,  Data.SonarType);

figure(Fig);
% set(Fig, 'name', FigName)

hc = findobj(gca, 'Type', 'line');
nbCourbes = length(hc);
ColorOrder = get(gca, 'ColorOrder');
nbCoul = size(ColorOrder,1);
iCoul = mod(nbCourbes-1, nbCoul) + 1;

t = Data.Time.timeMat;
str = timeMat2str([t(1), t(end)]);
str = [str repmat(' ', 2, 2)]';
str = sprintf('display(''%s - %s'')', nomFic, str(:));

fields = {'Roll'; 'Pitch'; 'Heave'; 'Heading'};
units = repmat({''}, 1, length(fields));
units{1} = 'deg';
units{2} = 'deg';
units{3} = 'm';
units{4} = 'deg';
strFields = cell2str(fields);

T = Data.Time;
if isempty(sub)
    sub = 1:length(T);
end

% t = Data.Time.timeMat;
for k=1:length(fields)
    Y = Data.(fields{k});
    
    hAxe(k) = subplot(4, 1, k); %#ok<AGROW>
    h = plot(T(sub), Y(sub)); hold on;
    set(h, 'Color', ColorOrder(iCoul,:))
    set(h, 'ButtonDownFcn', str)
    grid on; title(fields{k})
    
    msg = sprintf('%s : %s %s', strFields(k,:), num2strCode(Y));
    if ~isempty(units{k})
        msg = sprintf('%s (%s)', msg, units{k});
    end
    disp(msg)
end
linkaxes(hAxe, 'x'); axis tight; drawnow;
drawnow
