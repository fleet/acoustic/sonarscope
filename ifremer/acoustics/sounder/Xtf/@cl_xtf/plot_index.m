% Affichage des informations relatives aux datagrams contenus dans un
% fichier .xtf
%
% Syntax
%   plot_index(a)
% 
% Input Arguments 
%   a : Instance de cl_xtf
%
% Examples
%   nomFic = getNomFicDatabase('REBENT166.XTF')
%   nomFic = getNomFicDatabase('demowreck.XTF')
%   nomFic = getNomFicDatabase('demoplane.XTF')
%   a = cl_xtf('nomFic', nomFic);
%   plot_index(a)
%
% See also cl_xtf histo_index Authors
% Authors : GLU
% VERSION  : $Id: cl_xtf.m,v 1.1 2002/10/21 11:59:51 rgallou Exp rgallou $
% ----------------------------------------------------------------------------

function plot_index(this)

for i=1:length(this)
    plot_index_unitaire(this(i))
end

function plot_index_unitaire(this)

%% Lecture du fichier d'index

[flag, Data] = read_FileIndex_bin(this);
if ~flag
    return
end

Date            = Data.Time.date;
Heure           = Data.Time.heure;
Position        = Data.Position;
Taille          = Data.NbOfBytesInDatagram;
TypeDatagram    = Data.TypeOfDatagram;

[nomDir, nomFic] = fileparts(this.nomFic);
figure('name', nomFic);
h(1) = subplot(5,1,1); PlotUtils.createSScPlot(Date);         grid on; title('Date');
h(2) = subplot(5,1,2); PlotUtils.createSScPlot(Heure);        grid on; title('Heure');
h(3) = subplot(5,1,3); PlotUtils.createSScPlot(Taille);       grid on; title('Taille');
h(4) = subplot(5,1,4); PlotUtils.createSScPlot(Position);     grid on; title('Position');
h(5) = subplot(5,1,5); PlotUtils.createSScPlot(TypeDatagram, '+'); grid on; title('TypeDatagram');
linkaxes(h, 'x'); axis tight; drawnow;

% nomSignal{1} = 'Date';
% nomSignal{2} = 'Heure';
% nomSignal{3} = 'Taille';
% nomSignal{4} = 'Position';
% nomSignal{5} = 'TypeDatagram';
% nomSignal{6} = 'PingCounter';
% 
% signal{1} = Date;
% signal{2} = Heure;
% signal{3} = Taille;
% signal{4} = Position;
% signal{5} = TypeDatagram;
% signal{6} = PingCounter;
% 
% ex2 = cli_visu_caraibes;
% set(ex2, 'lstVec', signal);
% set(ex2, 'lstNomsVec', nomSignal);
% editobj(ex2) ;
% 
