function preprocess_XTFSignals(this, nomFic) %#ok<INUSL>

FiltreNav.Type  = 5;
FiltreNav.Ordre = 2;
FiltreNav.Wc    = 0.02;

FiltreTime.Type  = 5;
FiltreTime.Ordre = 2;
FiltreTime.Wc    = 0.02;

FiltreImmersion.Type  = 5;
FiltreImmersion.Ordre = 2;
FiltreImmersion.Wc    = 0.02;

FiltreCableOut.Type  = 5;
FiltreCableOut.Ordre = 2;
FiltreCableOut.Wc    = 0.02;

FiltreSpeed.Type  = 5;
FiltreSpeed.Ordre = 2;
FiltreSpeed.Wc    = 0.02;

FiltreDelay.Type  = 5;
FiltreDelay.Ordre = 2;
FiltreDelay.Wc    = 0.02;

FiltreHeading.Type  = 5;
FiltreHeading.Ordre = 2;
FiltreHeading.Wc    = 0.02;

FiltreHeave.Type  = 5;
FiltreHeave.Ordre = 2;
FiltreHeave.Wc    = 0.1;


if ~iscell(nomFic)
    nomFic = {nomFic};
end

%% Origine de la position du poisson

str1 = 'Calcul de la navigation du poisson � partir de la longueur fil�e ?';
str2 = 'Compute fish navigation from cableout ? (if no cableout is found, SSc will ask you to define a constant length)';
[OrigineFishNav, flag] = my_questdlg(Lang(str1,str2), 'Init', 2);
if ~flag
    return
end

%% Traitement des fichiers

Format1 = 'V�rification de la %s.\n\nUne fen�tre SignalScope va s''ouvrir, le signal de %s est en noir et une version filtr�e est en rouge. Zoomez sur le signal pour voir si la version filtr�e est meilleure (elle rem�die souvent � des marches d''escalier dues � une quantification de la mesure de positionnemen).\n\nSi vous conservez le filtre et que vous clickez sur Save et Quit c''est lui qui sera sauvegard�.';
Format2 = 'Checking %s.\n\nA SignalScope window is going to be open, %s signal is the black one, the red one is a filtered version. Zoom on the signal to check if the filter is efficient (it often solves problems of stair steps due to signal quantification).\n\nIf you conserve the filter AND you click on Save and Quit il will be the red curve which will be saved.';
% Format3 = 'V�rification de la %s.\n\nUne fen�tre SignalScope va s''ouvrir. Zoomez sur le signal pour voir si un filtrage s''impose.';
% Format4 = 'Checking %s.\n\nA SignalScope window is going to be opened. Zoom on the signal to check if a filter is necessary.';

Fig2 = [];
Carto = [];
selection = [];
Selection = [];
subSidescan = [];
N = length(nomFic);
hw = create_waitbar('XTF Quality Control processing', 'N', N);
for k=1:N
    my_waitbar(k, N, hw);
    
    str1 = sprintf('Traitement du fichier %d/%d : "%s" en cours.', k, N, nomFic{k});
    str2 = sprintf('Processing file %d/%d : "%s".', k, N, nomFic{k});
    fprintf(1,'%s\n', Lang(str1,str2));
    
    %% V�rification de l'existence du fichier Status
    
    [nomDir, nomFicSeul] = fileparts(nomFic{k});
    nomFicXmlStatus = fullfile(nomDir, [nomFicSeul '_Status.xml']);
    if exist(nomFicXmlStatus, 'file')
        str1 = sprintf('Il semble que le fichier "%s" a d�j� �t� trait�. Que voulez-vous faire ?', nomFicSeul);
        str2 = sprintf('It seems file "%s" has been already processed. What do you want to do ?', nomFicSeul);
        str3 = 'Passer au fichier suivant';
        str4 = 'Skip it';
        str5 = 'Le retraiter';
        str6 = 'Reprocess it';
        [rep, flag] = my_questdlg(Lang(str1,str2), Lang(str3,str4), Lang(str5,str6));
        if ~flag
            my_close(hw)
            return
        end
        if rep == 1
            continue
        end
        Info = xml_read(nomFicXmlStatus);
        FiltreTime      = Info.FiltreTime;
        FiltreNav       = Info.FiltreNav;
        FiltreImmersion = Info.FiltreImmersion;
        FiltreCableOut  = Info.FiltreCableOut;
        FiltreSpeed     = Info.FiltreSpeed;
        FiltreDelay     = Info.FiltreDelay;
        FiltreHeading   = Info.FiltreHeading;
    end
    
    a = cl_xtf('nomFic', nomFic{k});
    if isempty(a)
        continue
    end
    
    %     [texteTypeDatagram, Code, histoTypeDatagram] = histo_index(a);
    if isempty(Selection)
        [flag, Selection] = select_channels(a, 'SelectionMode', 'Single');
        if ~flag
            return
        end
        subSidescan = Selection.subSidescan;
    end
    
    Fig2 = plot_Sidescan_Signals(a, 'Fig', Fig2);
    
    [Data, b, Carto, subSidescan] = read_Sidescan(a, 'Carto', Carto, 'subSidescan', subSidescan);
    if isempty(b)
        [Data, b, Carto] = read_Bathy(a, 'Carto', Carto);
    end
    if isempty(Data)
        return
    end
    
    [flag, FileHeader] = SSc_ReadDatagrams(nomFic{k}, 'Ssc_FileHeader', 'XMLOnly', 1);
    if ~flag
        return
    end
        
    %% Calcul de la navigation du poisson 
    
    displayWarning = (k == 1);
    if OrigineFishNav == 1 % use ShipLatitude and LayBack
        [flag, b, selection, FiltreNav, FiltreTime, FiltreImmersion, FiltreCableOut, FiltreSpeed, FiltreDelay, FiltreHeading] = ...
            ComputeNavFishFromCabeout(b, displayWarning, Data, selection, FiltreNav, FiltreTime, FiltreImmersion, FiltreCableOut, FiltreSpeed, FiltreDelay, FiltreHeading);
        if ~flag
            break
        end
    else % Use FishLatitude
        [b, selection, FiltreNav, FiltreHeading] = checkFishLatLon(b, displayWarning, selection, Format1, Format2, 'Fish', FiltreNav, FiltreHeading);
    end
    Latitude  = get(b(1), 'FishLatitude');
    Longitude = get(b(1), 'FishLongitude');
    Heading   = get(b(1), 'Heading');
    
    Heading = mod(Heading + 360, 360);
    
    flag = save_signal_SensorYCoordinate(a, FileHeader.NavUnits, Latitude, Longitude, Carto);
    if ~flag
        return
    end
    
    flag = save_signal_SensorXCoordinate(a, FileHeader.NavUnits, Longitude, Latitude, Carto);
    if ~flag
        return
    end
    
    if FileHeader.NumberOfBathymetryChannels == 0
        flag = save_signal(a, 'Ssc_SideScan', 'SensorHeading', Heading);
    else
        flag = save_signal(a, 'Ssc_Bathy', 'SensorHeading', repmat(Heading,1,2)); % TODO : corriger �a � la source
    end
    if ~flag
        return
    end
    
    if FileHeader.NumberOfBathymetryChannels ~= 0
        Heave   = get(b(1), 'Heave');
        [Heave, FiltreHeave] = controleSignalNEW('Heave', Heave, 'Filtre', FiltreHeave);
        flag = save_signal(a, 'Ssc_Bathy', 'Heave', repmat(Heave, 1, 2));
    end
    
    %% Affichage de l'image dans SonarScope pour v�rifier la hauteur
    
%     if FileHeader.NumberOfBathymetryChannels == 0
        % TODO :que faut-il faire si nbSidescanChannel ~= 0 ? Pb y'a des fichiers
        % qui ont if FileHeader.nbSidescanChannel ~= 0 mais y'a pas de
        % sidescan (INAT) !!!!!!
        if displayWarning
            messageCheckHeight();
        end
        b(1) = SonarScope(b(1), 'visuProfilY', 1, 'profilY.Type', 'Height', 'visuProfilX', 1, 'ButtonSave', 1);
        
        Height  = get(b, 'Height');
        flag = save_signal(a, 'Ssc_SideScan', 'SensorPrimaryAltitude', Height);
        if ~flag
            return
        end
%     end
    clear b
    
        
    %% Cr�ation du fichier de status du fichier
    
    Info.FiltreTime      = FiltreTime;
    Info.FiltreNav       = FiltreNav;
    Info.FiltreImmersion = FiltreImmersion;
    Info.FiltreCableOut  = FiltreCableOut;
    Info.FiltreSpeed     = FiltreSpeed;
    Info.FiltreDelay     = FiltreDelay;
    Info.FiltreHeading   = FiltreHeading;
    Info.FiltreHeave     = FiltreHeave;
    xml_write(nomFicXmlStatus, Info);
    flag = exist(nomFicXmlStatus, 'file');
    if ~flag
        messageErreur(nomFicXmlStatus)
    end
    
    %% On demande si on veut traiter les autres fichiers
    
    if k ~= N
        flag = question_ContinueNextFile();
        if ~flag
            break
        end
    end
    
end
my_close(hw, 'MsgEnd')



function [flag, b, selection, FiltreNav, FiltreTime, FiltreImmersion, FiltreCableOut, FiltreSpeed, FiltreDelay, FiltreHeading, FiltreHeave] = ...
    ComputeNavFishFromCabeout(b, displayWarning, Data, selection, FiltreNav, FiltreTime, FiltreImmersion, FiltreCableOut, FiltreSpeed, FiltreDelay, FiltreHeading, FiltreHeave)

Format1 = 'V�rification de la %s.\n\nUne fen�tre SignalScope va s''ouvrir, le signal de %s est en noir et une version filtr�e est en rouge. Zoomez sur le signal pour voir si la version filtr�e est meilleure (elle rem�die souvent � des marches d''escalier dues � une quantification de la mesure de positionnemen).\n\nSi vous conservez le filtre et que vous clickez sur Save et Quit c''est lui qui sera sauvegard�.';
Format2 = 'Checking %s.\n\nA SignalScope window is going to be open, %s signal is the black one, the red one is a filtered version. Zoom on the signal to check if the filter is efficient (it often solves problems of stair steps due to signal quantification).\n\nIf you conserve the filter AND you click on Save and Quit il will be the red curve which will be saved.';

Format3 = 'V�rification de la %s.\n\nUne fen�tre SignalScope va s''ouvrir. Zoomez sur le signal pour voir si un filtrage s''impose.';
Format4 = 'Checking %s.\n\nA SignalScope window is going to be opened. Zoom on the signal to check if a filter is necessary.';

%% Contr�le de la longitude du bateau

if displayWarning
    str1 = sprintf(Format1, 'Longitude', 'Longitude');
    str2 = sprintf(Format2, 'Longitude', 'Longitude');
    my_warndlg(Lang(str1,str2), 1);
end

Longitude = get_vecteur(b(1), 'ShipLongitude');
[Longitude, FiltreNav] = controleSignalNEW('ShipLongitude', Longitude, 'Filtre', FiltreNav);
b(1) = set(b(1), 'SonarShipLongitude', Longitude);

%% Contr�le de la latitude du bateau

if displayWarning
    str1 = sprintf(Format1, 'Latitude', 'Latitude');
    str2 = sprintf(Format2, 'Latitude', 'Latitude');
    my_warndlg(Lang(str1,str2), 1);
end

Latitude = get_vecteur(b(1), 'ShipLatitude');
[Latitude, FiltreNav] = controleShipLatitude('ShipLatitude', Latitude, 'Filtre', FiltreNav);
b(1) = set(b(1), 'SonarShipLatitude', Latitude);

%% Contr�le de l'heure

if displayWarning
    str1 = sprintf(Format3, 'Time');
    str2 = sprintf(Format4, 'Time');
    my_warndlg(Lang(str1,str2), 1);
end

Time = get_vecteur(b(1), 'Time');
[Time, FiltreTime] = controleTime('Time', Time, FiltreTime);
b(1) = set(b(1), 'SonarTime', Time);

%% Contr�le de l'immersion

if displayWarning
    str1 = sprintf(Format1, 'Immersion', 'Immersion');
    str2 = sprintf(Format2, 'Immersion', 'Immersion');
    my_warndlg(Lang(str1,str2), 1);
end

Immersion = get_vecteur(b(1), 'Immersion');
[Immersion, FiltreImmersion] = controleCableOut('Immersion', Immersion, 'Filtre', FiltreImmersion);
b(1) = set(b(1), 'SonarImmersion', Immersion);

%% Contr�le du la longueur fil�e

CableOut = get_vecteur(b(1), 'CableOut');
if all(CableOut == 0)
    [flag, X] = params_layback;
    if ~flag
        return
    end
    CableOut(:) = X;
else
    if displayWarning
        str1 = sprintf(Format1, 'CableOut', 'CableOut');
        str2 = sprintf(Format2, 'CableOut', 'CableOut');
        my_warndlg(Lang(str1,str2), 1);
    end
    
    [CableOut, FiltreCableOut] = controleCableOut('CableOut', CableOut, 'Filtre', FiltreCableOut);
end
b(1) = set(b(1), 'SonarCableOut', CableOut);

%% Calcul de la distance horizontale bateau-poisson

disp('preprocess_XTFSignals : ATTENTION BIDOUILLE POUR FICHIERS AXEL : H = Immersion + 3')
H = Immersion + 3; 

distanceFish = CableOut .^ 2 - H .^2;
distanceFish(distanceFish < 0) = 0;
distanceFish = sqrt(distanceFish);

%% Calcul de la distance interpings

distanceEntrePings = distLatLon(Latitude, Longitude);

%% Calcul de la vitesse

if displayWarning
    str1 = sprintf(Format1, 'Speed', 'Speed');
    str2 = sprintf(Format2, 'Speed', 'Speed');
    my_warndlg(Lang(str1,str2), 1);
end

T = Time.timeMat;
T = T - T(1);
T = T * (24*3600);
Speed = abs(distanceEntrePings' ./ diff(T));
[Speed, FiltreSpeed] = controleSignalNEW('Speed', Speed, 'Filtre', FiltreSpeed);
Speed(end+1) = Speed(end);
b(1) = set(b(1), 'SonarSpeed', Speed);

% SpeedBefore = get_vecteur(b(1), 'Speed');

%% Calcul du decalage temporel de la navigation

if displayWarning
    str1 = sprintf(Format3, 'Delay', 'Delay');
    str2 = sprintf(Format4, 'Delay', 'Delay');
    my_warndlg(Lang(str1,str2), 1);
end

Delay = distanceFish ./ Speed;
[Delay, FiltreDelay] = controleSignalNEW('Delay', Delay, 'Filtre', FiltreDelay);

%% D�calage temporel  de la navigation

Time = Time-(1000*Delay);
FishLongitude = my_interp1_longitude(Data.Time, Longitude, Time);
FishLatitude  = my_interp1(Data.Time, Latitude,  Time);

b(1) = set(b(1), 'SonarFishLongitude', FishLongitude);
b(1) = set(b(1), 'SonarFishLatitude', FishLatitude);

%% Calcul du cap

% HeadingBefore = get_vecteur(b(1), 'Heading');
Heading  = calCapFromLatLon(FishLatitude, FishLongitude, 'Time', Time);

%% Contr�le du cap calcul�

if displayWarning
    str1 = sprintf(Format3, 'Heading', 'Heading');
    str2 = sprintf(Format4, 'Heading', 'Heading');
    my_warndlg(Lang(str1,str2), 1);
end

[Heading, FiltreHeading] = controleSignalNEW('Heading', Heading, 'Filtre', FiltreHeading);
b(1) = set(b(1), 'SonarHeading', Heading);

% figure; plot(FishLongitude, FishLatitude); grid on
% figure; plot(Heading); grid on; hold on; plot(Signal, '+r');

%% Sauvegarde des signaux

flag = 1;



function [Signal, Filtre] = controleCableOut(NomSignal, Signal, varargin)
subKO = find(Signal <= 0);
if ~isempty(subKO)
    subOK = find(Signal > 0);
    if ~isempty(subOK)
        Signal(subKO) = interp1(subOK, Signal(subOK), subKO, 'linear', 'extrap');
    end
end
[Signal, Filtre] = controleSignalNEW(NomSignal, Signal, varargin{:});


function [Signal, Filtre] = controleTime(NomSignal, Signal, Filtre, varargin)
Signal = Signal.timeMat;
SignalDeb = Signal(1);
Signal = Signal - SignalDeb;
[Signal, Filtre] = controleSignalNEW(NomSignal, Signal, 'Filtre', Filtre);
Signal = Signal + SignalDeb;
Signal = cl_time('timeMat', Signal, varargin{:});




function [b, selection, FiltreNav, FiltreHeading] = checkFishLatLon(b, displayWarning, selection, Format1, Format2, Origine, FiltreNav, FiltreHeading)

% TODO : reprendre ce qui est fait pour SDF

%% Contr�le de la navigation

if displayWarning
    str1 = sprintf(Format1, 'Longitude', 'Longitude');
    str2 = sprintf(Format2, 'Longitude', 'Longitude');
    my_warndlg(Lang(str1,str2), 1);
end

Datetime = get_vecteur(b(1), 'Datetime');
Heading  = get_vecteur(b(1), 'Heading');
switch Origine
    case 'Fish'
        Latitude  = get_vecteur(b(1), 'FishLatitude');
        Longitude = get_vecteur(b(1), 'FishLongitude');
        
%         [Longitude, FiltreNav] = controleSignalNEW('FishLongitude', Longitude, 'Filtre', FiltreNav);
        [flag, Latitude, Longitude, Heading] = CheckNavigation(Datetime, Latitude, Longitude, ...
            'Navigation Fish', 'Heading', Heading);
        if ~flag
            return
        end
        
        b(1) = set(b(1), 'SonarFishLatitude',  Latitude);
        b(1) = set(b(1), 'SonarFishLongitude', Longitude);
        
    case 'Ship'
        Latitude  = get_vecteur(b(1), 'ShipLatitude');
        Longitude = get_vecteur(b(1), 'ShipLongitude');
        
        [Longitude, FiltreNav] = controleSignalNEW('ShipLongitude', Longitude, 'Filtre', FiltreNav);
        [flag, Latitude, Longitude, Heading] = CheckNavigation(Datetime, Latitude, Longitude, ...
            'Navigation Ship', 'Heading', Heading);
        if ~flag
            return
        end
        
        b(1) = set(b(1), 'SonarShipLatitude',  Latitude);
        b(1) = set(b(1), 'SonarShipLongitude', Longitude);
end
b(1) = set(b(1), 'SonarHeading', Heading);

%% Contr�le du cap calcul�

str1 = 'Que voulez-vous faire du cap ?';
str2 = 'What do you want to do with this heading ?';
[rep, flag] = my_questdlg(Lang(str1,str2), ...
    Lang('L''tiliser tel que','Use it as is'), ...
    Lang('Le recalculer � partir de la navigation','Compute from the navigation'));
if ~flag
    return
end

if rep == 2
    Time = get_vecteur(b(1), 'Time');
	Heading = calCapFromLatLon(Latitude, Longitude, 'Time', Time);
    [Heading, FiltreHeading] = controleSignalNEW('Heading', Heading, 'Filtre', FiltreHeading);
end

b(1) = set(b(1), 'SonarHeading', Heading);
