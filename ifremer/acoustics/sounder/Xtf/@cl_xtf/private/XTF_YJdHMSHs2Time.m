function Time = XTF_YJdHMSHs2Time(SideScan)

% TODO : mettre un indice iChannel (:,iChannel) au lie de (:,1)

nbPings =  SideScan.Dimensions.NbPings;
Date  = dayJma2Ifr(ones(nbPings,1), ones(nbPings,1), double(SideScan.Year(:,1))) + double(SideScan.JulianDay(:,1)) - 1;
Heure = double(SideScan.HSeconds(:,1)) * 10 + 1000 * (double(SideScan.Second(:,1)) + double(SideScan.Minute(:,1)) * 60 + double(SideScan.Hour(:,1)) * 3600);
Time = cl_time('timeIfr', Date', Heure');
