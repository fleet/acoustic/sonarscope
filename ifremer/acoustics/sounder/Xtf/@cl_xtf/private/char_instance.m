% Transformation en chaine de caracteres d'une instance
%
% Syntax
%   str = char_instance(a)
%
% Input Arguments
%   a : instance de cl_xtf
%
% Name-Value Pair Arguments
%   InfoSonar  : 1=Information de l'image uniquement
%                2=Info de l'information sonar uniquement
%                3=Display de toute l'information disponible (image+sonar) : (3 par defaut)
%
% Output Arguments
%   str : une chaine de caracteres representative de l instance
%
% Examples
%
% See also cl_xtf Authors
% Authors : GLU
% ----------------------------------------------------------------------------

function str = char_instance(this, varargin)

str = [];

str{end+1} = sprintf('nomFic      <-> %s', this.nomFic);
str{end+1} = sprintf('nomFicIndex <-- %s', this.nomFicIndex);
if ~isempty(this.nomFic)
    strHisto = histo_index(this, 'minimal', 1);
    strHisto = strsplit(strHisto, newline);
    for i=1: length(strHisto)
        str{end+1} = sprintf('                %s', strHisto{i}); %#ok<AGROW>
    end
end

%% Concatenation en une chaine

str = cell2str(str);
