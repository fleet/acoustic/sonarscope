% -----------------------------
% Lecture  du XTFAttitudeHeader

function [flag, XTFAttitudeHeader] = fread_AttitudeHeader(fid)

flag = 0;

BYTE  = 'uint8';
WORD  = 'uint16';
DWORD = 'uint32';
FLOAT = 'single';
% LONG  = 'int32';

XTFAttitudeHeader.MagicNumber = 'FACE';
XTFAttitudeHeader.HeaderType  = 3;

XTFAttitudeHeader.SubChannelNumber = fread(fid, 1, BYTE);
if feof(fid)
    return
end

XTFAttitudeHeader.NumChansToFollow = fread(fid, 1, WORD);
if feof(fid)
    return
end

XTFAttitudeHeader.Reserved1 = fread(fid, 2, WORD);
if feof(fid)
    return
end

XTFAttitudeHeader.NumBytesThisRecord = fread(fid, 1, DWORD);
if feof(fid)
    return
end

XTFAttitudeHeader.Reserved2 = fread(fid, 4, DWORD);
if feof(fid)
    return
end

XTFAttitudeHeader.Pitch = fread(fid, 1, FLOAT);
if feof(fid)
    return
end

XTFAttitudeHeader.Roll = fread(fid, 1, FLOAT);
if feof(fid)
    return
end

XTFAttitudeHeader.Heave = fread(fid, 1, FLOAT);
if feof(fid)
    return
end

XTFAttitudeHeader.Yaw = fread(fid, 1, FLOAT);
if feof(fid)
    return
end

XTFAttitudeHeader.TimeTag = fread(fid, 1, DWORD);
if feof(fid)
    return
end

XTFAttitudeHeader.Heading = fread(fid, 1, FLOAT);
if feof(fid)
    return
end

XTFAttitudeHeader.Year = fread(fid, 1, WORD);
if feof(fid)
    return
end

XTFAttitudeHeader.Month = fread(fid, 1, BYTE);
if feof(fid)
    return
end

XTFAttitudeHeader.Day = fread(fid, 1, BYTE);
if feof(fid)
    return
end

XTFAttitudeHeader.Hour = fread(fid, 1, BYTE);
if feof(fid)
    return
end

XTFAttitudeHeader.Minute = fread(fid, 1, BYTE);
if feof(fid)
    return
end

XTFAttitudeHeader.Second = fread(fid, 1, BYTE);
if feof(fid)
    return
end

XTFAttitudeHeader.MiliSeconds = fread(fid, 1, WORD)*100;
if feof(fid)
    return
end

XTFAttitudeHeader.Reserved3 = fread(fid, 1, BYTE);
if feof(fid)
    return
end
flag = 1;
return