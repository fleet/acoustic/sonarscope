% -----------------------------
% Lecture  du XTFBathySNP0Data, snippet 0 de la bathymétrie.
function [flag, XTFSNP0Data] = fread_BathySNP0Data(fid)

BYTE    = 'uint8';
WORD    = 'uint16'; %#ok<NASGU>
DWORD   = 'uint32'; %#ok<NASGU>
FLOAT   = 'single'; %#ok<NASGU>
LONG    = 'int32'; %#ok<NASGU>
SHORT   = 'int16'; %#ok<NASGU>

flag = 0;

XTFSNP0Data.ID = fread(fid, 1, 'uint32');
if feof(fid)
    return
end

XTFSNP0Data.HeaderSize = fread(fid, 1, 'uint16');
if feof(fid)
    return
end

XTFSNP0Data.DataSize = fread(fid, 1, 'uint16');
if feof(fid)
    return
end

XTFSNP0Data.PingNumber = fread(fid, 1, 'uint32');
if feof(fid)
    return
end

XTFSNP0Data.Seconds = fread(fid, 1, 'unsigned long');
if feof(fid)
    return
end

XTFSNP0Data.MilliSec = fread(fid, 1, 'unsigned long');
if feof(fid)
    return
end

XTFSNP0Data.Latency = fread(fid, 1, 'unsigned short');
if feof(fid)
    return
end

XTFSNP0Data.SonarID = fread(fid, 2, 'unsigned short');
if feof(fid)
    return
end

XTFSNP0Data.SonarModel = fread(fid, 1, 'unsigned short');
if feof(fid)
    return
end

XTFSNP0Data.Frequency = fread(fid, 1, 'unsigned short');
if feof(fid)
    return
end

XTFSNP0Data.SSpeed = fread(fid, 1, 'unsigned short');
if feof(fid)
    return
end

XTFSNP0Data.SampleRate = fread(fid, 1, 'unsigned short');
if feof(fid)
    return
end

XTFSNP0Data.PingRate = fread(fid, 1, 'unsigned short');
if feof(fid)
    return
end

XTFSNP0Data.Range = fread(fid, 1, 'unsigned short');
if feof(fid)
    return
end

XTFSNP0Data.Power = fread(fid, 1, 'unsigned short');
if feof(fid)
    return
end

XTFSNP0Data.Gain = fread(fid, 1, 'unsigned short');
if feof(fid)
    return
end

XTFSNP0Data.PulseWidth = fread(fid, 1, 'unsigned short');
if feof(fid)
    return
end

XTFSNP0Data.Spread = fread(fid, 1, 'unsigned short');
if feof(fid)
    return
end

XTFSNP0Data.Absorb = fread(fid, 1, 'unsigned short');
if feof(fid)
    return
end

XTFSNP0Data.Proj = fread(fid, 1, 'unsigned short');
if feof(fid)
    return
end

XTFSNP0Data.ProjWidth = fread(fid, 1, 'unsigned short');
if feof(fid)
    return
end

XTFSNP0Data.SpacingNum = fread(fid, 1, 'unsigned short');
if feof(fid)
    return
end

XTFSNP0Data.SpacingDen = fread(fid, 1, 'unsigned short');
if feof(fid)
    return
end

XTFSNP0Data.ProjAngle = fread(fid, 1, 'short');
if feof(fid)
    return
end

XTFSNP0Data.MinRange = fread(fid, 1, 'unsigned short');
if feof(fid)
    return
end

XTFSNP0Data.MaxRange = fread(fid, 1, 'unsigned short');
if feof(fid)
    return
end

XTFSNP0Data.MinDepth = fread(fid, 1, 'unsigned short');
if feof(fid)
    return
end

XTFSNP0Data.MaxDepth = fread(fid, 1, 'unsigned short');
if feof(fid)
    return
end

XTFSNP0Data.Filters = fread(fid, 1, 'unsigned short');
if feof(fid)
    return
end

XTFSNP0Data.bFlags = fread(fid, 2, BYTE);
if feof(fid)
    return
end

XTFSNP0Data.HeadTemp = fread(fid, 1, 'short');
if feof(fid)
    return
end

XTFSNP0Data.BeamCnt = fread(fid, 1, 'unsigned short');
if feof(fid)
    return
end

flag = 1;
return