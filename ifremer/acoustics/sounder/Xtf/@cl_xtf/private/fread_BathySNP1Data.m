% -----------------------------
% Lecture  du XTFBathySNP1Data, snippet 1 de la bathymétrie.
function [flag, XTFSNP1Data] = fread_BathySNP1Data(fid)

BYTE    = 'uint8';%#ok
WORD    = 'uint16';%#ok
DWORD   = 'uint32';%#ok
FLOAT   = 'single';%#ok
LONG    = 'int32';%#ok
SHORT   = 'int16';%#ok

flag = 0;

XTFSNP1Data.ID = fread(fid, 1, 'unsigned long');
if feof(fid)
    return
end

XTFSNP1Data.HeaderSize = fread(fid, 1, 'unsigned short');
if feof(fid)
    return
end

XTFSNP1Data.DataSize = fread(fid, 1, 'unsigned short');
if feof(fid)
    return
end

XTFSNP1Data.PingNumber = fread(fid, 1, 'unsigned long');
if feof(fid)
    return
end

XTFSNP1Data.Beam = fread(fid, 1, 'unsigned long');
if feof(fid)
    return
end

XTFSNP1Data.SnipSamples = fread(fid, 1, 'unsigned long');
if feof(fid)
    return
end

XTFSNP1Data.GainStart = fread(fid, 2, 'unsigned short');
if feof(fid)
    return
end

XTFSNP1Data.GainEnd = fread(fid, 1, 'unsigned short');
if feof(fid)
    return
end

XTFSNP1Data.FragOffset = fread(fid, 1, 'unsigned short');
if feof(fid)
    return
end

XTFSNP1Data.FragSamples = fread(fid, 1, 'unsigned short');
if feof(fid)
    return
end


flag = 1;
return