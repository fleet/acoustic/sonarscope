% -----------------------------
% Lecture  du XTFChanInfo
function [flag, XTFChanInfo] = fread_ChanInfo(fid)

flag = 0;

%{

// Channel information structure (contained in the file header).
// One-time information describing each channel.  64 bytes long.
// This is data pertaining to each channel that will not change
// during the course of a run.
///////////////////////////////////////////////////////////////////////////////
typedef struct {
   BYTE TypeOfChannel;     // PORT, STBD, SBOT or BATH
   BYTE SubChannelNumber;
   WORD CorrectionFlags;   // 1=raw, 2=Corrected
   WORD UniPolar;          // 0=data is bipolar, 1=data is unipolar
   WORD BytesPerSample;    // 1 or 2
   DWORD SamplesPerChannel;// Usually a multiple of 1024 unless bathymetry
   char ChannelName[16];   // Text describing channel.  i.e., "Port 500"

   float VoltScale;        // How many volts is represented by max sample value.  Typically 5.0.
   float Frequency;        // Center transmit frequency
   float HorizBeamAngle;   // Typically 1 degree or so
   float TiltAngle;        // Typically 30 degrees
   float BeamWidth;        // 3dB beam width, Typically 50 degrees

                           // Orientation of these offsets:
                           // Positive Y is forward
                           // Positive X is to starboard
                           // Positive Z is down.  Just like depth.
                           // Positive roll is lean to starboard
                           // Positive pitch is nose up
                           // Positive yaw is turn to right

   float OffsetX;          // These offsets are entered in the 
   float OffsetY;          // Multibeam setup dialog box.
   float OffsetZ;

   float OffsetYaw;        // If the multibeam sensor is reverse
                           // mounted (facing backwards), then
                           // OffsetYaw will be around 180 degrees.
   float OffsetPitch;      
   float OffsetRoll;

   char ReservedArea2[56];

} CHANINFO;


%}

BYTE    = 'uint8';
WORD    = 'uint16';
DWORD   = 'uint32';
FLOAT   = 'single';
LONG    = 'int32';%#ok

XTFChanInfo.TypeOfChannel = fread(fid, 1, BYTE);
if feof(fid)
    return
end

XTFChanInfo.SubChannelNumber = fread(fid, 1, BYTE);
if feof(fid)
    return
end

XTFChanInfo.CorrectionFlags = fread(fid, 1, WORD);
if feof(fid)
    return
end

XTFChanInfo.UniPolar = fread(fid, 1, WORD);
if feof(fid)
    return
end

XTFChanInfo.BytesPerSample = fread(fid, 1, WORD);
if feof(fid)
    return
end

XTFChanInfo.SamplesPerChannel = fread(fid, 1, DWORD);
if feof(fid)
    return
end

XTFChanInfo.ChannelName = fread(fid, [1 16], 'uint8=>char');
if feof(fid)
    return
end

XTFChanInfo.VoltScale = fread(fid, 1, FLOAT);
if feof(fid)
    return
end

XTFChanInfo.Frequency = fread(fid, 1, FLOAT);
if feof(fid)
    return
end

XTFChanInfo.HorizBeamAngle = fread(fid, 1, FLOAT);
if feof(fid)
    return
end

XTFChanInfo.TiltAngle = fread(fid, 1, FLOAT);
if feof(fid)
    return
end

XTFChanInfo.BeamWidth = fread(fid, 1, FLOAT);
if feof(fid)
    return
end

XTFChanInfo.OffsetX = fread(fid, 1, FLOAT);
if feof(fid)
    return
end

XTFChanInfo.OffsetY = fread(fid, 1, FLOAT);
if feof(fid)
    return
end

XTFChanInfo.OffsetZ = fread(fid, 1, FLOAT);
if feof(fid)
    return
end

XTFChanInfo.OffsetYaw = fread(fid, 1, FLOAT);
if feof(fid)
    return
end

XTFChanInfo.OffsetPitch = fread(fid, 1, FLOAT);
if feof(fid)
    return
end

XTFChanInfo.OffsetRoll = fread(fid, 1, FLOAT);
if feof(fid)
    return
end

XTFChanInfo.ReservedArea2 = fread(fid, [1 56], 'uint8=>char');
if feof(fid)
    return
end

flag = 1;
