% -----------------------------
% Lecture  du XTFFILEHEADER
function [flag, XTFFileHeader] = fread_FileHeader(fid)

flag = 0;


% Format définis dans XTF.
BYTE    = 'uint8';
WORD    = 'uint16';
DWORD   = 'uint32';%#ok
FLOAT   = 'single';
LONG    = 'int32';



XTFFileHeader.FileFormat = fread(fid, 1, BYTE);
if feof(fid)
    return
end

XTFFileHeader.SystemType = fread(fid, 1, BYTE);
if feof(fid)
    return
end

XTFFileHeader.RecordingProgramName = fread(fid, [1 8], 'uint8=>char');
if feof(fid)
    return
end

XTFFileHeader.RecordingProgramVersion = fread(fid, [1 8], 'uint8=>char');
if feof(fid)
    return
end

XTFFileHeader.SonarName = fread(fid, [1 16], 'uint8=>char');
if feof(fid)
    return
end

XTFFileHeader.SonarType = fread(fid, 1, WORD);
if feof(fid)
    return
end

XTFFileHeader.NoteString = fread(fid, [1 64], 'uint8=>char');
if feof(fid)
    return
end

XTFFileHeader.ThisFileName = fread(fid, [1 64], 'uint8=>char');
if feof(fid)
    return
end

XTFFileHeader.NavUnits = fread(fid, 1, WORD);
if feof(fid)
    return
end

XTFFileHeader.NumberOfSonarChannels = fread(fid, 1, WORD);
if feof(fid)
    return
end

XTFFileHeader.NumberOfBathymetryChannels = fread(fid, 1, WORD);
if feof(fid)
    return
end

XTFFileHeader.NumberOfSnippetChannels = fread(fid, 1, BYTE);
if feof(fid)
    return
end

XTFFileHeader.NumberOfForwardLookArrays = fread(fid, 1, BYTE);
if feof(fid)
    return
end

XTFFileHeader.NumberOfEchoStrenghChannels = fread(fid, 1, WORD);
if feof(fid)
    return
end

XTFFileHeader.NumberOfInterferometryChannels = fread(fid, 1, BYTE);
if feof(fid)
    return
end

XTFFileHeader.Reserved1 = fread(fid, 1, BYTE);
if feof(fid)
    return
end

XTFFileHeader.Reserved2 = fread(fid, 1, WORD);
if feof(fid)
    return
end

XTFFileHeader.ReferencePointHeight = fread(fid, 1, FLOAT);
if feof(fid)
    return
end

%Lecture des paramètres de navigation du système.
XTFFileHeader.ProjectionType = fread(fid, 12, BYTE);
if feof(fid)
    return
end

XTFFileHeader.SpheriodType = fread(fid, 10, BYTE);
if feof(fid)
    return
end

XTFFileHeader.NavigationLatency = fread(fid, 1, LONG);
if feof(fid)
    return
end

XTFFileHeader.OriginY = fread(fid, 1, FLOAT);
if feof(fid)
    return
end

XTFFileHeader.OriginX = fread(fid, 1, FLOAT);
if feof(fid)
    return
end

XTFFileHeader.NavOffsetY = fread(fid, 1, FLOAT);
if feof(fid)
    return
end

XTFFileHeader.NavOffsetX = fread(fid, 1, FLOAT);
if feof(fid)
    return
end

XTFFileHeader.NavOffsetZ = fread(fid, 1, FLOAT);
if feof(fid)
    return
end

XTFFileHeader.NavOffsetYaw = fread(fid, 1, FLOAT);
if feof(fid)
    return
end

XTFFileHeader.MRUOffsetY = fread(fid, 1, FLOAT);
if feof(fid)
    return
end

XTFFileHeader.MRUOffsetX = fread(fid, 1, FLOAT);
if feof(fid)
    return
end

XTFFileHeader.MRUOffsetZ = fread(fid, 1, FLOAT);
if feof(fid)
    return
end

XTFFileHeader.MRUOffsetYaw = fread(fid, 1, FLOAT);
if feof(fid)
    return
end

XTFFileHeader.MRUOffsetPitch = fread(fid, 1, FLOAT);
if feof(fid)
    return
end

XTFFileHeader.MRUOffsetRoll = fread(fid, 1, FLOAT);
if feof(fid)
    return
end

%Lecture du CHANINFO (6 max ou si > 6, augmenté par bloc de 1024)
NbChannelsTotal = XTFFileHeader.NumberOfSonarChannels + XTFFileHeader.NumberOfBathymetryChannels;
for i=1:NbChannelsTotal
    [flag, XTFFileHeader.ChanInfo(i)] = fread_ChanInfo(fid);
    if feof(fid) || flag == 0
        return
    end
end
