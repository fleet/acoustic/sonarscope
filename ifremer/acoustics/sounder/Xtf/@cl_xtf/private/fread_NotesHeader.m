% -----------------------------
% Lecture  du XTFNotesHeader
function [flag, XTFNotesHeader] = fread_NotesHeader(fid)

flag = 0;

BYTE    = 'uint8';
WORD    = 'uint16';
DWORD   = 'uint32';
FLOAT   = 'single'; %#ok<NASGU>
LONG    = 'int32'; %#ok<NASGU>

XTFNotesHeader.MagicNumber = 'FACE';
XTFNotesHeader.HeaderType  = 1;

XTFNotesHeader.SubChannelNumber = fread(fid, 1, BYTE);
if feof(fid)
    return
end

XTFNotesHeader.NumChansToFollow = fread(fid, 1, WORD);
if feof(fid)
    return
end

XTFNotesHeader.Reserved = fread(fid, 2, WORD);
if feof(fid)
    return
end

XTFNotesHeader.NumBytesThisRecord = fread(fid, 1, DWORD);
if feof(fid)
    return
end

XTFNotesHeader.Year = fread(fid, 1, WORD);
if feof(fid)
    return
end

XTFNotesHeader.Month = fread(fid, 1, BYTE);
if feof(fid)
    return
end

XTFNotesHeader.Day = fread(fid, 1, BYTE);
if feof(fid)
    return
end

XTFNotesHeader.Hour = fread(fid, 1, BYTE);
if feof(fid)
    return
end

XTFNotesHeader.Minute = fread(fid, 1, BYTE);
if feof(fid)
    return
end

XTFNotesHeader.Second = fread(fid, 1, BYTE);
if feof(fid)
    return
end

XTFNotesHeader.ReservedBytes = fread(fid, 35, BYTE);
if feof(fid)
    return
end

XTFNotesHeader.NotesText = fread(fid, 200, 'uint8=>char');
if feof(fid)
    return
end

flag = 1;
return