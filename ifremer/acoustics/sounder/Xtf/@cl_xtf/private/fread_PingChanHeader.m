% -----------------------------
% Lecture  du XTFPingChanHeader

function [flag, XTFPingChanHeader] = fread_PingChanHeader(fid)

flag = 0;

BYTE    = 'uint8';
WORD    = 'uint16';
DWORD   = 'uint32';
FLOAT   = 'single';
LONG    = 'int32'; %#ok<NASGU>
SHORT   = 'int16';


XTFPingChanHeader.ChannelNumber = fread(fid, 1, WORD);
if feof(fid)
    return
end

XTFPingChanHeader.DownSampleMethod = fread(fid, 1, WORD);
if feof(fid)
    return
end

XTFPingChanHeader.SlantRange = fread(fid, 1, FLOAT);
if feof(fid)
    return
end

XTFPingChanHeader.GroundRange = fread(fid, 1, FLOAT);
if feof(fid)
    return
end

XTFPingChanHeader.TimeDelay = fread(fid, 1, FLOAT);
if feof(fid)
    return
end

XTFPingChanHeader.TimeDuration = fread(fid, 1, FLOAT);
if feof(fid)
    return
end

XTFPingChanHeader.SecondsPerPing = fread(fid, 1, FLOAT);
if feof(fid)
    return
end

XTFPingChanHeader.ProcessingFlags = fread(fid, 1, WORD);
if feof(fid)
    return
end


XTFPingChanHeader.Frequency = fread(fid, 1, WORD);
if feof(fid)
    return
end

XTFPingChanHeader.InitialGainCode = fread(fid, 1, WORD);
if feof(fid)
    return
end

XTFPingChanHeader.GainCode = fread(fid, 1, WORD);
if feof(fid)
    return
end

XTFPingChanHeader.BandWidth = fread(fid, 1, WORD);
if feof(fid)
    return
end

XTFPingChanHeader.ContactNumber = fread(fid, 1, DWORD);
if feof(fid)
    return
end

XTFPingChanHeader.ContactClassification = fread(fid, 1, WORD);
if feof(fid)
    return
end

XTFPingChanHeader.ContactSubNumber = fread(fid, 1, BYTE);
if feof(fid)
    return
end

XTFPingChanHeader.ContactType = fread(fid, 1, BYTE);
if feof(fid)
    return
end

XTFPingChanHeader.NumSamples = fread(fid, 1, DWORD);
if feof(fid)
    return
end

XTFPingChanHeader.MillivoltScale = fread(fid, 1, WORD);
if feof(fid)
    return
end

XTFPingChanHeader.ContactTimeOffTrack = fread(fid, 1, FLOAT);
if feof(fid)
    return
end

XTFPingChanHeader.ContactCloseNumber = fread(fid, 1, BYTE);
if feof(fid)
    return
end

XTFPingChanHeader.Reserved2 = fread(fid, 1, BYTE);
if feof(fid)
    return
end

XTFPingChanHeader.FixedVSOP = fread(fid, 1, FLOAT);
if feof(fid)
    return
end

XTFPingChanHeader.Weight = fread(fid, 1, SHORT);
if feof(fid)
    return
end

XTFPingChanHeader.ReservedSpace = fread(fid, 4, BYTE);
if feof(fid)
    return
end

flag = 1;
