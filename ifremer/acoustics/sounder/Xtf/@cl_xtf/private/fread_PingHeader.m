% -----------------------------
% Lecture  du XTFPingHeader
function [flag, XTFPingHeader] = fread_PingHeader(fid, FlagSonar)

flag = 0;

BYTE    = 'uint8';
WORD    = 'uint16';
DWORD   = 'uint32';
FLOAT   = 'single';
LONG    = 'int32';%#ok
SHORT   = 'int16';

% D�j� lu au pr�alable.
XTFPingHeader.MagicNumber = 'FACE';
if (FlagSonar == 1)
    XTFPingHeader.HeaderType  = 0;  % XTF_HEADER_SONAR
else
    XTFPingHeader.HeaderType  = 2;  % XTF_HEADER_BATHY
end

XTFPingHeader.SubChannelNumber = fread(fid, 1, BYTE);
if feof(fid)
    return
end

XTFPingHeader.NumChansToFollow = fread(fid, 1, WORD);
if feof(fid)
    return
end

XTFPingHeader.Reserved = fread(fid, 2, WORD);
if feof(fid)
    return
end

XTFPingHeader.NumBytesThisRecord = fread(fid, 1, DWORD);
if feof(fid)
    return
end

XTFPingHeader.Year = fread(fid, 1, WORD);
if feof(fid)
    return
end

XTFPingHeader.Month = fread(fid, 1, BYTE);
if feof(fid)
    return
end

XTFPingHeader.Day = fread(fid, 1, BYTE);
if feof(fid)
    return
end

XTFPingHeader.Hour = fread(fid, 1, BYTE);
if feof(fid)
    return
end

XTFPingHeader.Minute = fread(fid, 1, BYTE);
if feof(fid)
    return
end

XTFPingHeader.Second = fread(fid, 1, BYTE);
if feof(fid)
    return
end

XTFPingHeader.HSeconds = fread(fid, 1, BYTE);
if feof(fid)
    return
end

XTFPingHeader.JulianDay = fread(fid, 1, WORD);
if feof(fid)
    return
end

XTFPingHeader.EventNumber = fread(fid, 1, DWORD);
if feof(fid)
    return
end

XTFPingHeader.PingNumber = fread(fid, 1, DWORD);
if feof(fid)
    return
end

XTFPingHeader.SoundVelocity = fread(fid, 1, FLOAT);
if feof(fid)
    return
end

XTFPingHeader.OceanTide = fread(fid, 1, FLOAT);
if feof(fid)
    return
end

XTFPingHeader.Reserved2 = fread(fid, 1, DWORD);
if feof(fid)
    return
end

XTFPingHeader.ConductivityFreq = fread(fid, 1, FLOAT);
if feof(fid)
    return
end

XTFPingHeader.TemperatureFreq = fread(fid, 1, FLOAT);
if feof(fid)
    return
end

XTFPingHeader.PressureFreq = fread(fid, 1, FLOAT);
if feof(fid)
    return
end

XTFPingHeader.TemperatureTemp = fread(fid, 1, FLOAT);
if feof(fid)
    return
end

XTFPingHeader.Conductivity = fread(fid, 1, FLOAT);
if feof(fid)
    return
end

XTFPingHeader.WaterTemperature = fread(fid, 1, FLOAT);
if feof(fid)
    return
end

XTFPingHeader.Pressure = fread(fid, 1, FLOAT);
if feof(fid)
    return
end

XTFPingHeader.ComputedSoundVelocity = fread(fid, 1, FLOAT);
if feof(fid)
    return
end

XTFPingHeader.MagX = fread(fid, 1, FLOAT);
if feof(fid)
    return
end

XTFPingHeader.MagY = fread(fid, 1, FLOAT);
if feof(fid)
    return
end

XTFPingHeader.MagZ = fread(fid, 1, FLOAT);
if feof(fid)
    return
end

XTFPingHeader.AuxVal1 = fread(fid, 1, FLOAT);
if feof(fid)
    return
end

XTFPingHeader.AuxVal2 = fread(fid, 1, FLOAT);
if feof(fid)
    return
end

XTFPingHeader.AuxVal3 = fread(fid, 1, FLOAT);
if feof(fid)
    return
end

XTFPingHeader.AuxVal4 = fread(fid, 1, FLOAT);
if feof(fid)
    return
end

XTFPingHeader.AuxVal5 = fread(fid, 1, FLOAT);
if feof(fid)
    return
end

XTFPingHeader.AuxVal6 = fread(fid, 1, FLOAT);
if feof(fid)
    return
end

XTFPingHeader.SpeedLog = fread(fid, 1, FLOAT);
if feof(fid)
    return
end

XTFPingHeader.Turbidity = fread(fid, 1, FLOAT);
if feof(fid)
    return
end

XTFPingHeader.ShipSpeed = fread(fid, 1, FLOAT);
if feof(fid)
    return
end

XTFPingHeader.ShipGyro = fread(fid, 1, FLOAT);
if feof(fid)
    return
end

XTFPingHeader.ShipYCoordinate = fread(fid, 1, 'double');
if feof(fid)
    return
end

XTFPingHeader.ShipXCoordinate = fread(fid, 1, 'double');
if feof(fid)
    return
end

XTFPingHeader.ShipAltitude = fread(fid, 1, WORD);
if feof(fid)
    return
end

XTFPingHeader.ShipDepth = fread(fid, 1, WORD);
if feof(fid)
    return
end

XTFPingHeader.FixTimeHour = fread(fid, 1, BYTE);
if feof(fid)
    return
end

XTFPingHeader.FixTimeMinute = fread(fid, 1, BYTE);
if feof(fid)
    return
end

XTFPingHeader.FixTimeSecond = fread(fid, 1, BYTE);
if feof(fid)
    return
end

XTFPingHeader.FixTimeHsecond = fread(fid, 1, BYTE);
if feof(fid)
    return
end

XTFPingHeader.SensorSpeed = fread(fid, 1, FLOAT);
if feof(fid)
    return
end

XTFPingHeader.KP = fread(fid, 1, FLOAT);
if feof(fid)
    return
end

XTFPingHeader.SensorYcoordinate = fread(fid, 1, 'double');
if feof(fid)
    return
end

XTFPingHeader.SensorXcoordinate = fread(fid, 1, 'double');
if feof(fid)
    return
end

XTFPingHeader.SonarStatus = fread(fid, 1, WORD);
if feof(fid)
    return
end

XTFPingHeader.RangeToFish = fread(fid, 1, WORD);
if feof(fid)
    return
end

XTFPingHeader.BearingToFish = fread(fid, 1, WORD);
if feof(fid)
    return
end

XTFPingHeader.CableOut = fread(fid, 1, WORD);
if feof(fid)
    return
end

XTFPingHeader.LayBack = fread(fid, 1, FLOAT);
if feof(fid)
    return
end

XTFPingHeader.CableTension = fread(fid, 1, FLOAT);
if feof(fid)
    return
end

XTFPingHeader.SensorDepth = fread(fid, 1, FLOAT);
if feof(fid)
    return
end

XTFPingHeader.SensorPrimaryAltitude = fread(fid, 1, FLOAT);
if feof(fid)
    return
end

XTFPingHeader.SensorAuxAltitude = fread(fid, 1, FLOAT);
if feof(fid)
    return
end

XTFPingHeader.SensorPitch = fread(fid, 1, FLOAT);
if feof(fid)
    return
end

XTFPingHeader.SensorRoll = fread(fid, 1, FLOAT);
if feof(fid)
    return
end

XTFPingHeader.SensorHeading = fread(fid, 1, FLOAT);
if feof(fid)
    return
end

XTFPingHeader.Heave = fread(fid, 1, FLOAT);
if feof(fid)
    return
end

XTFPingHeader.Yaw = fread(fid, 1, FLOAT);
if feof(fid)
    return
end

XTFPingHeader.AttitudeTimeTag = fread(fid, 1, DWORD);
if feof(fid)
    return
end

XTFPingHeader.DOT = fread(fid, 1, FLOAT);
if feof(fid)
    return
end

XTFPingHeader.NavFixMilliseconds = fread(fid, 1, DWORD);
if feof(fid)
    return
end

XTFPingHeader.ComputerClockHour = fread(fid, 1, BYTE);
if feof(fid)
    return
end

XTFPingHeader.ComputerClockMinute = fread(fid, 1, BYTE);
if feof(fid)
    return
end

XTFPingHeader.ComputerClockSecond = fread(fid, 1, BYTE);
if feof(fid)
    return
end

XTFPingHeader.ComputerClockHSec = fread(fid, 1, BYTE);
if feof(fid)
    return
end

XTFPingHeader.FishPositionDeltaX = fread(fid, 1, SHORT);
if feof(fid)
    return
end

XTFPingHeader.FishPositionDeltaY = fread(fid, 1, SHORT);
if feof(fid)
    return
end

XTFPingHeader.FishPositionErrorCode = fread(fid, 1, 'uint8=>unsigned char');
if feof(fid)
    return
end

XTFPingHeader.ReservedSpace2 = fread(fid, 11, BYTE);
if feof(fid)
    return
end

if (FlagSonar == 1)
    XTFPingHeader.Channel = {};	% XTF_HEADER_SONAR
else
    XTFPingHeader.SNP0 = {};     % XTF_HEADER_BATHY
end

flag = 1;
