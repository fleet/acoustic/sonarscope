% -----------------------------
% Lecture  du XTFRawSerialHeader

function [flag, XTFRawSerialHeader] = fread_RawSerialHeader(fid)

flag = 0;

BYTE    = 'uint8';
WORD    = 'uint16';
DWORD   = 'uint32';
FLOAT   = 'single'; %#ok<NASGU>
LONG    = 'int32'; %#ok<NASGU>

XTFRawSerialHeader.MagicNumber = 'FACE';
XTFRawSerialHeader.HeaderType  = 6;

XTFRawSerialHeader.SubChannelNumber = fread(fid, 1, BYTE);
if feof(fid)
    return
end

XTFRawSerialHeader.NumChansToFollow = fread(fid, 1, WORD);
if feof(fid)
    return
end

XTFRawSerialHeader.Reserved = fread(fid, 2, WORD);
if feof(fid)
    return
end

XTFRawSerialHeader.NumBytesThisRecord = fread(fid, 1, DWORD);
if feof(fid)
    return
end

XTFRawSerialHeader.Year = fread(fid, 1, WORD);
if feof(fid)
    return
end

XTFRawSerialHeader.Month = fread(fid, 1, BYTE);
if feof(fid)
    return
end

XTFRawSerialHeader.Day = fread(fid, 1, BYTE);
if feof(fid)
    return
end

XTFRawSerialHeader.Hour = fread(fid, 1, BYTE);
if feof(fid)
    return
end

XTFRawSerialHeader.Minute = fread(fid, 1, BYTE);
if feof(fid)
    return
end

XTFRawSerialHeader.Second = fread(fid, 1, BYTE);
if feof(fid)
    return
end

XTFRawSerialHeader.HSeconds = fread(fid, 1, BYTE);
if feof(fid)
    return
end

XTFRawSerialHeader.JulianDay = fread(fid, 1, WORD);
if feof(fid)
    return
end

XTFRawSerialHeader.TimeTag = fread(fid, 1, DWORD);
if feof(fid)
    return
end

XTFRawSerialHeader.StringSize = fread(fid, 1, WORD);
if feof(fid)
    return
end

XTFRawSerialHeader.RawAsciiData = fread(fid, XTFRawSerialHeader.StringSize, 'uint8=>char');
if feof(fid)
    return
end


flag = 1;
return