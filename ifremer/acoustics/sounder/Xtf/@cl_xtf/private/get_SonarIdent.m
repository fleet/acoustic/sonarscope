function SonarIdent = get_SonarIdent(SonarType)

% '1=EM12D' | '2=EM12S' | '3=EM300' | '4=EM1000' | '5=SAR' | '6=EM3000D' | '7=EM3000S' | '8=DF1000' | '9=DTS1' | '10=EM1002' | '11=EM120' | '12=Reson715012kHz' | '13=Reson715024kHz' | '14=Reson7111' | '15=Reson7125' | '16=GeoSwath'
switch SonarType
    case 7
        SonarIdent = 8;
    case 15
        my_warndlg('Affectation arbitraire � DF1000', 1);
        SonarIdent = 8;
    otherwise
        my_warndlg('Affectation arbitraire � EM12D', 1);
        SonarIdent = 1;
end
