function [flag, SonarIdent, Mode1, Mode2, XTFSonarType, ChannelName, Frequency] = identification_sonar(FileHeader, DataChannelInfo, PingChanHeader, kS)

flag = 1;

SlantRange = [];
Frequency  = [];
ChannelName = {};
for k=1:2:FileHeader.NumberOfSonarChannels
    if ~isempty(PingChanHeader)
        SlantRange(end+1)  = max(PingChanHeader{k}.SlantRange); %#ok<*AGROW>
        Frequency(end+1)   = PingChanHeader{k}.Frequency(1); 
    else
        Frequency(end+1)   = NaN; 
    end
    ChannelName{end+1} = DataChannelInfo{k}.ChannelName{1}; 
    ChannelName{end}   = strrep(ChannelName{end}, 'PORT_', '');
end
if FileHeader.NumberOfSonarChannels ~= 0
    N = length(Frequency);
else
    N = 1;
end

%{
0 = NONE , default.
1 = JAMSTEC, Jamstec chirp 2-channel subbottom.
2 = ANALOG_C31, PC31 8-channel.
3 = SIS1000, Chirp SIS-1000 sonar.
4 = ANALOG_32CHAN, Spectrum with 32-channel DSPlink card.
5 = KLEIN2000, Klein system 2000 with digital interface.
6 = RWS, Standard PC31 analog with special nav code.
7 = DF1000, EG&G DF1000 digital interface.
8 = SEABAT, Reson SEABAT 900x analog/serial.
9 = KLEIN595, 4-chan Klein 595, same as ANALOG_C31.
10 = EGG260, 2-channel EGG260, same as ANALOG_C31.
11 = SONATECH_DDS, Sonatech Diver Detection System on Spectrum DSP32C.
12 = ECHOSCAN, Odom EchoScanII multibeam (with simultaneous analog sidescan).
13 = ELAC, Elac multibeam system.
14 = KLEIN5000, Klein system 5000 with digital interface.
15 = Reson Seabat 8101.
16 = Imagenex model 858.
17 = USN SILOS with 3-channel analog.
18 = Sonatech Super-high res sidescan sonar.
19 = Delph AU32 Analog input (2 channel)..
20 = Generic sonar using the memory-mapped file interface.
21 = Simrad SM2000 Multibeam Echo Sounder.
22 = Standard multimedia audio.
23 = Edgetech (EG&G) ACI card for 260 sonar through PC31 card.
24 = Edgetech Black Box.
25 = Fugro deeptow.
26 = C&C's Edgetech Chirp conversion program.
27 = DTI SAS Synthetic Aperture processor (memmap file).
28 = Fugro's Osiris AUV Sidescan data.
29 = Fugro's Osiris AUV Multibeam data.
30 = Geoacoustics SLS.
31 = Simrad EM2000/EM3000.
32 = Klein system 3000.
33 = SHRSSS Chirp system
34 = Benthos C3D SARA/CAATI
35 = Edgetech MP-X
36 = CMAX
37 = Benthos sis1624
38 = Edgetech 4200
39 = Benthos SIS1500
40 = Benthos SIS1502
41 = Benthos SIS3000
42 = Benthos SIS7000
43 = DF1000 DCU
44 = NONE_SIDESCAN
45 = NONE_MULTIBEAM
46 = Reson 7125
47 = CODA Echoscope
48 = Kongsberg SAS
49 = QINSY
50 = GeoAcoustics DSSS
51 = CMAX_USB
52 = SwathPlus Bathy
53 = R2Sonic Qnsy
55 = R2Sonic Triton
54 = Converted SwathPlus Bathy
56 = Edgetech 4600
57 = Klein 3500
58 = Klein 5900
%}
    
SonarIdent = NaN(1,N);
Mode1      = NaN(1,N);
Mode2      = NaN(1,N);

XTFSonarType = FileHeader.SonarType;
if XTFSonarType == 0
    if isempty(FileHeader.RecordingProgramName)
        switch FileHeader.SonarName
            case 'Edgetech JSF'
                XTFSonarType = 32;
            otherwise
                str1 = sprintf('SonarType=0 mais SonarName="%s".\n\Envoyez cette information � JMA SVP qu''il am�liore fonction "cl_xtf/identification_sonarNew".', FileHeader.SonarName);
                str2 = sprintf('SonarType=0 but  SonarName="%s".\n\nPlease send this information to JMA in order he can improve function "cl_xtf/identification_sonarNew".', FileHeader.SonarName);
                my_warndlg(Lang(str1,str2), 0, 'Tag', 'XTFSonarTypeEgal0');
        end
    else
        switch FileHeader.RecordingProgramName
            case 'QINSy'
                XTFSonarType = 49;
            case 'Scanlin'
                XTFSonarType = 49;
            otherwise
                switch FileHeader.SonarName
                    case 'Klein 3000'
                        XTFSonarType = 32;
                    otherwise
                        str1 = sprintf('SonarType=0 mais SonarName="%s".\n\Envoyez cette information � JMA SVP qu''il am�liore fonction "cl_xtf/identification_sonarNew".', FileHeader.SonarName);
                        str2 = sprintf('SonarType=0 but  SonarName="%s".\n\nPlease send this information to JMA in order he can improve function "cl_xtf/identification_sonarNew".', FileHeader.SonarName);
                        my_warndlg(Lang(str1,str2), 0, 'Tag', 'XTFSonarTypeEgal0');
                end
        end
    end
end

str1 = sprintf('Le param�tre "SonarType=%d" donn� dans le FileHeader du fichier XTF n''est pas encore trait� dans cl_xtf/identification_sonarNew. Envoyez ce message � sonarscope@ifremer.fr SVP. Ce sonar est pour l''instant consid�r� comme un DF1000.', XTFSonarType);
str2 = sprintf('Parameter "SonarType=%d" given in the XTF FileHeader is not set in cl_xtf/identification_sonarNew yet. Please send this message to sonarscope@ifremer.fr SVP. This sonar is considered as a DF1000 for the moment.', XTFSonarType);
switch XTFSonarType
    case 0 % Fichier Geoacoustics Shallow Survey
        kS = 1;
        for k=1:N
            % '1=EM12D' | '2=EM12S' | '3=EM300' | '4=EM1000' | '5=SAR' | '6=EM3000D' | '7=EM3000S' | '8=DF1000' | '9=DTS1' | '10=EM1002' | '11=EM120' | '12=Reson715012kHz' | '13=Reson715024kHz' | '14=Reson7111' | '15=Reson7125' | '16=GeoSwath'
            SonarIdent(k) = 8;
            if Frequency(k) == 100 % kHz
                Mode1(k) = 1;
            else
                Mode1(k) = 2;
            end
            if SlantRange(k) < 75
                Mode2(k) = 1; % 50 m
            elseif SlantRange(k) < 150
                Mode2(k) = 2; % 100 m
            elseif SlantRange(k) < 500
                Mode2(k) = 3; % 100 m
            else
                Mode2(k) = 4; % 1000 m
            end
        end
        
    case 44 % 'Elite-5Ti' 
        kS = 1;
        for k=1:N
            % '1=EM12D' | '2=EM12S' | '3=EM300' | '4=EM1000' | '5=SAR' | '6=EM3000D' | '7=EM3000S' | '8=DF1000' | '9=DTS1' | '10=EM1002' | '11=EM120' | '12=Reson715012kHz' | '13=Reson715024kHz' | '14=Reson7111' | '15=Reson7125' | '16=GeoSwath'
            SonarIdent(k) = 8;
            if Frequency(k) == 100 % kHz
                Mode1(k) = 1;
            else
                Mode1(k) = 2;
            end
            if SlantRange(k) < 75
                Mode2(k) = 1; % 50 m
            elseif SlantRange(k) < 150
                Mode2(k) = 2; % 100 m
            elseif SlantRange(k) < 500
                Mode2(k) = 3; % 100 m
            else
                Mode2(k) = 4; % 1000 m
            end
        end
        
    case 7
        for k=1:N
            % '1=EM12D' | '2=EM12S' | '3=EM300' | '4=EM1000' | '5=SAR' | '6=EM3000D' | '7=EM3000S' | '8=DF1000' | '9=DTS1' | '10=EM1002' | '11=EM120' | '12=Reson715012kHz' | '13=Reson715024kHz' | '14=Reson7111' | '15=Reson7125' | '16=GeoSwath'
            SonarIdent(k) = 8;
            if Frequency(k) == 100 % kHz
                Mode1(k) = 1;
            else
                Mode1(k) = 2;
            end
            if SlantRange(k) < 75
                Mode2(k) = 1; % 50 m
            elseif SlantRange(k) < 150
                Mode2(k) = 2; % 100 m
            elseif SlantRange(k) < 500
                Mode2(k) = 3; % 100 m
            else
                Mode2(k) = 4; % 1000 m
            end
        end
        
    case 24 % Fichier Suzanna CSIC 
        kS = 1;
        for k=1:N
            % '1=EM12D' | '2=EM12S' | '3=EM300' | '4=EM1000' | '5=SAR' | '6=EM3000D' | '7=EM3000S' | '8=DF1000' | '9=DTS1' | '10=EM1002' | '11=EM120' | '12=Reson715012kHz' | '13=Reson715024kHz' | '14=Reson7111' | '15=Reson7125' | '16=GeoSwath'
            SonarIdent(k) = 8;
            if Frequency(k) == 100 % kHz
                Mode1(k) = 1;
            else
                Mode1(k) = 2;
            end
            if SlantRange(k) < 75
                Mode2(k) = 1; % 50 m
            elseif SlantRange(k) < 150
                Mode2(k) = 2; % 100 m
            elseif SlantRange(k) < 500
                Mode2(k) = 3; % 100 m
            else
                Mode2(k) = 4; % 1000 m
            end
        end
        
    case 8 % SEABAT, Reson SEABAT 900x analog/serial. % TODO : On fait comme si c'�tait un DF1000
        % '1=EM12D' | '2=EM12S' | '3=EM300' | '4=EM1000' | '5=SAR' | '6=EM3000D' | '7=EM3000S' | '8=DF1000' | '9=DTS1' | '10=EM1002' | '11=EM120' | '12=Reson715012kHz' | '13=Reson715024kHz' | '14=Reson7111' | '15=Reson7125' | '16=GeoSwath'
        my_warndlg(Lang(str1,str2), 0, 'Tag', 'AffectationArbitraireADF1000');
        k = 1;
        SonarIdent(k) = 8;
        Mode1(k) = 1;
        Mode2(k) = 1; % 50 m
        kS = 1; % Rajout� par JMA le 22/04/2015
        Frequency(kS) = NaN;
        ChannelName{kS} = 'SEABAT';
        
    case 32 % Klein 3000
        for k=1:N
            SonarIdent(k) = 26;
            % TODO : ce qui suit n'est pas valid� (c'est un copi�-coller du
            % DF1000)
            if Frequency(k) == 100 % kHz
                Mode1(k) = 1;
            else
                Mode1(k) = 2;
            end
            if SlantRange(k) < 75
                Mode2(k) = 1; % 50 m
            elseif SlantRange(k) < 150
                Mode2(k) = 2; % 100 m
            elseif SlantRange(k) < 500
                Mode2(k) = 3; % 100 m
            else
                Mode2(k) = 4; % 1000 m
            end
        end
        
    case {31, 46, 49, 53} % QINSY Qu'est-ce qu'on fait ? rrrhhhh......
        my_warndlg(Lang(str1,str2), 0, 'Tag', 'AffectationArbitraireADF1000');
        % k = 1;
        SonarIdent(kS) = 8;
        Mode1(kS) = 1;
        Mode2(kS) = 1; % 50 m
        Frequency(kS) = NaN;
        ChannelName{kS} = 'QINSY';
        
    otherwise
        my_warndlg(Lang(str1,str2), 0, 'Tag', 'AffectationArbitraireADF1000');
        SonarIdent(k) = 8;
        Mode1(k) = 1;
        Mode2(k) = 1;
end

SonarIdent  = SonarIdent(kS);
Mode1       = Mode1(kS);
Mode2       = Mode2(kS);
Frequency   = Frequency(kS);
ChannelName = ChannelName{kS};
if strcmp(ChannelName, 'NoChannelName')
    ChannelName = '';
end