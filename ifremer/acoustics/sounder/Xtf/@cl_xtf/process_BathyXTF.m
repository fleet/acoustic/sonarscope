function flag = process_BathyXTF(~, nomFic, skipAlreadyProcessedFiles)

Carto       = [];
fig         = [];
FigPosition = [];
N = length(nomFic);
hw = create_waitbar('Processing bathy XTF', 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    FirstTime = (k == 1);
    [flag, Carto, fig, FigPosition] = process_BathyXTF_unitaire(nomFic{k}, Carto, FirstTime, fig, FigPosition, skipAlreadyProcessedFiles);
    if ~flag
        continue
    end
end
my_close(hw, 'MsgEnd');


function [flag, Carto, fig, FigPosition] = process_BathyXTF_unitaire(nomFic, Carto, FirstTime, fig, FigPosition, skipAlreadyProcessedFiles)

persistent persistent_CoefSpikeFilter

OffsetRoll = 0;

a = cl_xtf('nomFic', nomFic);
if isempty(a)
    flag = 0;
    return
end
[flag, FileHeader] = SSc_ReadDatagrams(nomFic, 'Ssc_FileHeader', 'XMLOnly', 1);
if ~flag
    return
end

[nomDir, nom] = fileparts(nomFic);
nomFicXml = fullfile(nomDir, 'SonarScope', nom, 'PingBeam', 'Ssc_Bathymetry.xml');
flag = exist(nomFicXml, 'file');
if flag && skipAlreadyProcessedFiles
    % Cas o� la bathy a d�j� �t� calcul�e
    [flag, Data] = SSc_ReadDatagrams(nomFic, 'Ssc_Bathymetry', 'Memmapfile', -1); % Pas test�
    if ~flag
        'beurk'
        return
    end
    
else
    % Cas o� la bathy n'a pas encore �t� calcul�e
    flag = (FileHeader.NumberOfBathymetryChannels ~= 0);
    if ~flag
        str1 = sprintf('Le fichier "%s" ne comporte pas de datagrammes de bathym�trie.', nomFic{1});
        str2 = sprintf('File "%s" do not contain any Bathymetry data.', nomFic{1});
        my_warndlg(Lang(str1,str2), 1);
        return
    end
    
    % [flag, SonarTime, Latitude, Longitude, SonarSpeed, Heading] = read_navigation(a);
    % if ~flag
    %     return
    % end
    
    [DataBathy, b, Carto] = read_Bathy(a, 'Carto', Carto); % TODO , 'subSidescan', Selection.subBathymetry);
    
    kRxBeamAngle                 = 0;
    kTwoWayTravelTimeInSeconds  = 0;
    kReflectivity                = 0;
    for k=1:length(b)
        DT = b(k).DataType;
        if DT == cl_image.indDataType('RxBeamAngle')
            kRxBeamAngle = k;
        end
        if DT == cl_image.indDataType('TwoWayTravelTimeInSeconds')
            kTwoWayTravelTimeInSeconds = k;
        end
        if DT == cl_image.indDataType('Reflectivity')
            kReflectivity = k;
        end
    end
        
    if kTwoWayTravelTimeInSeconds && kRxBeamAngle
%         [c, ~, flag] = sonar_range2depth(b(kTwoWayTravelTimeInSeconds), b(kRxBeamAngle), [], [], 'OffsetRoll', OffsetRoll);
        [c, ~, status] = sonar_range2depth_R2sonic(b(kTwoWayTravelTimeInSeconds), b(kRxBeamAngle), [], 'OffsetRoll', OffsetRoll);
        if ~flag
            return
        end
        Data.OffsetRoll   = OffsetRoll;
        Data.TwoWayTravelTime = get(b(kTwoWayTravelTimeInSeconds),      'Image');
        Data.Depth        = get(c(1), 'Image');
        Data.RxBeamAngle  = get(b(kRxBeamAngle), 'Image');
        Data.AcrossDist   = get(c(2), 'Image');
        Data.AlongDist    = get(c(3), 'Image');
        Data.Reflectivity = get(b(kReflectivity), 'Image');
        Data.Mask         = zeros(size(Data.TwoWayTravelTime), 'single'); %get(c(2), 'Image');
        
        flag = create_XmlBin_Bathymetry(a, Data);
        if ~flag
            return
        end
    end
end

%% Nettoyage histogrammique de la bathy

[flag, Data, fig, FigPosition] = nettoyageImages(a, Data, nomFic, FirstTime, fig, FigPosition);

%% Filtrage de spike

str1 = 'Voulez-vous faire un filtrage de spikes ?';
str2 = 'Do you want to do a spike filter ?';
[rep, flag] = my_questdlg(Lang(str1,str2));
if ~flag
    return
end
if rep == 1
    
    % Seuil de rejection de l'algorithme anti-spike (N*Sigma)
    
    if isempty(persistent_CoefSpikeFilter)
        CoefSpikeFilter = 3;
    else
        CoefSpikeFilter = persistent_CoefSpikeFilter;
    end
    
    str1 = 'Param�trage de l''algorithme de suppression des valeurs erratiques.';
    str2 = 'Set parameter for outliers rejection.';
    p = ClParametre('Name', Lang('Seuil','Threshold'), ...
        'Unit', 'std',  'MinValue',  1.5, 'MaxValue', 5, 'Value', CoefSpikeFilter);
    a = StyledParametreDialog('params', p, 'Title', Lang(str1,str2));
    % a.Help = 'http://flotte.ifremer.fr/fleet/Presentation-of-the-fleet/On-board-software/SonarScope';
    a.sizes = [0 -2 -1 -2 -1 -1 -1 0];
    a.openDialog;
    flag = a.okPressedOut;
    if ~flag
        return
    end
    CoefSpikeFilter = a.getParamsValue;
    persistent_CoefSpikeFilter = CoefSpikeFilter;

    % Filtrage
    
    d = cl_image('Image', Data.Depth);
    d = filterSpikeBathy(d, CoefSpikeFilter, 'OtherImages', 0);
    Data.Depth = get(d(1), 'Image');
    M = isnan(Data.Depth);
    flag = save_BathyLayer(a, 'Mask', M);
    if ~flag
        'beurk'
    end
    
    [fig, FigPosition] = display_images(fig, nomFic, Data.TwoWayTravelTime, Data.Depth, Data.AcrossDist, FigPosition);
end



function [flag, Data, fig, FigPosition] = nettoyageImages(a, Data, nomFic, FirstTime, fig, FigPosition)

OK = 2;
disp('---------------------------------------------------------------')
while OK == 2
    sub = (Data.Mask == 1);
    Data.Depth(sub) = NaN;
    Data.TwoWayTravelTime(sub) = NaN;
    Data.AcrossDist(sub) = NaN;
    
    [fig, FigPosition] = display_images(fig, nomFic, Data.TwoWayTravelTime, Data.Depth, Data.AcrossDist, FigPosition);
    
    if FirstTime
        str1 = 'Cette op�ration agit uniquement sur un masque que vous pouvez compl�ter ou remettre � z�ro en cliquant sur l''item ci-dessous.';
        str2 = 'This process operates only on a Mask you can append or reinitialize.';
        str3 = 'Compl�ter';
        str4 = 'Append';
        str5 = 'Remise � z�ro';
        str6 = 'Reinitialize';
        [rep, flag] = my_questdlg(Lang(str1,str2), Lang(str3,str4), Lang(str5,str6));
        if ~flag
            return
        end
        if rep == 2
            Data.Mask(:) = false;
            [fig, FigPosition] = display_images(fig, nomFic, Data.TwoWayTravelTime, Data.Depth, Data.AcrossDist, FigPosition);
            %             flag = write_depth_bin(a, DataBathy);
            flag = save_BathyLayer(a, 'Mask', Data.Mask);
            if ~flag
                return
            end
        end
        FirstTime = 0;
    end
    
    [OK, flag] = my_questdlg(Lang('Voyez-vous toutes les couleurs de l''image "Depth" ?', ...
        'Do you see all the colors on the "Depth" image ?'));
    if ~flag
        return
    end
    if OK == 2
        NomLayer = 'Depth';
    else
        [OK, flag] = my_questdlg(Lang('Voyez-vous toutes les couleurs de l''image "RayPathRange" ?', ...
            'Do you see all the colors on the "RayPathRange" image ?'));
        if ~flag
            return
        end
        if OK == 2
            NomLayer = 'Range';
        else
            [OK, flag] = my_questdlg(Lang('Voyez-vous toutes les couleurs de l''image "AcrossDist" ?', ...
                'Do you see all the colors on the "AcrossDist" image ?'));
            if ~flag
                return
            end
            if OK == 2
                NomLayer = 'AcrossDist';
            end
        end
    end
    
    %         figure(fig)
    if OK == 1
        continue
    end
    
    switch NomLayer
        case 'Depth'
            I = Data.Depth;
        case 'Range'
            I = Data.TwoWayTravelTime;
        case 'AcrossDist'
            I = Data.AcrossDist;
    end
    I(Data.Mask == 1) = NaN;
    
    bins = linspace(min(I(:)), max(I(:)), 200);
    [N, bins, ~, SubBins] = histo1D(I, bins, 'Sampling', 1);
	sub = getFlaggedBinsOfAnHistogram(bins, N);
    
    %% On recupere les modifications
    
    if ~isempty(sub)
        for k2=1:length(sub)
            subPoubelle = SubBins{sub(k2)};
            Data.TwoWayTravelTime(subPoubelle)  = NaN;
            Data.Depth(subPoubelle)             = NaN;
            Data.AcrossDist(subPoubelle)        = NaN;
            Data.Mask(subPoubelle)              = 1;
        end
        [fig, FigPosition] = display_images(fig, nomFic, Data.TwoWayTravelTime, Data.Depth, Data.AcrossDist, FigPosition);
        flag = save_BathyLayer(a, 'Mask', Data.Mask);
        if ~flag
            return
        end
    end
end
my_close(fig)





function [fig, FigPosition] = display_images(fig, nomFic, Range, Depth, AcrossDist, FigPosition)

if isempty(fig) || ~ishandle(fig)
    if isempty(FigPosition)
        ScreenSize = get(0, 'ScreenSize');
        fig = figure('Position', centrageFig(950, ScreenSize(4)-150));
    else
        fig = figure('Position', FigPosition);
    end
end

figure(fig)
FigPosition = get(fig, 'Position');
subplot(1,3,1)
imagesc(Depth); colorbar; axis xy;
title([nomFic ' Depth'], 'interpreter', 'none')

subplot(1,3,2)
imagesc(Range); colorbar; axis xy;
title('Range')

subplot(1,3,3)
imagesc(AcrossDist); colorbar; axis xy;
title([nomFic ' AcrossDist'], 'interpreter', 'none')
