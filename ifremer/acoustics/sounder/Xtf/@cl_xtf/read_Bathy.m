function [Data, b, Carto] = read_Bathy(this, varargin)

[varargin, Carto] = getPropertyValue(varargin, 'Carto', []); %#ok<ASGLU>

Data = [];
b    = [];
% [varargin, subBathy] = getPropertyValue(varargin, 'subBathy', []);
% [varargin, SelectionMode] = getPropertyValue(varargin, 'SelectionMode', 'Multiple');

[flag, DataIndex] = read_FileIndex_bin(this);
[flag, Position]  = read_position_bin(this); % Vide
[flag, Attitude]  = read_attitude_bin(this); % Vide

% Changer la valeur de RecordingProgramVersion avant lecture
[flag, FileHeader] = SSc_ReadDatagrams(this.nomFic, 'Ssc_FileHeader', 'XMLOnly', 1);
if ~flag
    return
end

% TODO : pb NumBytesThisRecord � r�soudre
[flag, BathySignals] = SSc_ReadDatagrams(this.nomFic, 'Ssc_Bathy', 'Memmapfile', -1);
if ~flag
    return
end

for k=1:FileHeader.NumberOfBathymetryChannels
    [flag, DataChannelInfo{k}] = read_ChannelInfo_bin(this, k); %#ok<AGROW>
end

% if isempty(subBathy)
%     [flag, Selection] = select_channels(this, 'SelectionMode', SelectionMode);
%     if ~flag
%         return
%     end
%     subBathy = Selection.subBathymetry;
% end

[flag, BathyChannels] = SSc_ReadDatagrams(this.nomFic, 'Ssc_QPSMultiBeamHeader_0', 'Memmapfile', -1); % Non test�
if ~flag
    return
end

[flag, Data, b, Carto] = read_Bathy_unitaire(this, FileHeader, BathySignals, DataChannelInfo, BathyChannels, 'Carto', Carto);
if ~flag
    return
end

function [flag, Data, b, Carto] = read_Bathy_unitaire(this, FileHeader, BathySignals, DataChannelInfo, BathyChannels, varargin)

[varargin, Carto] = getPropertyValue(varargin, 'Carto', []); %#ok<ASGLU>

Data    = [];
b       = cl_image.empty;

[flag, SonarIdent, Mode_1, Mode_2, XTFSonarType, ChannelName, Frequency] = identification_sonar(FileHeader, DataChannelInfo, [], 1);
if ~flag
    return
end

SonarDescription = cl_sounder('Sonar.Ident', SonarIdent, 'Sonar.Mode_1', 1);

Roll        = BathySignals.SensorRoll(:,1);
Pitch       = BathySignals.SensorPitch(:,1);
Heave       = BathySignals.Heave(:,1);
Heading     = BathySignals.SensorHeading(:,1);
Speed       = BathySignals.ShipSpeed(:,1); % ShipSpeed ou SensorSpeed
Layback     = BathySignals.LayBack(:,1);
PingCounter = BathySignals.PingNumber(:,1);
Immersion   = BathySignals.SensorDepth(:,1);
Temperature = BathySignals.WaterTemperature(:,1);
Ycoordinate = BathySignals.SensorYCoordinate(:,1);
Xcoordinate = BathySignals.SensorXCoordinate(:,1);
SoundVelocity = BathySignals.SoundVelocity(:,1);
SonarHeight   = BathySignals.SensorPrimaryAltitude(:,1);

[flag, Data.Time] = get_BathyTime(this, 'Bathy', BathySignals);
if ~flag
    return
end

%% Param�trage de la donn�e

TypeCompensation = 1;
switch TypeCompensation
    case 1 % Image compens�e en NE, GT, SH et TVG
        Sonar_NE_etat = 1;
        Sonar_SH_etat = 1;
        Sonar_GT_etat = 1;
        SonarDiagEmi_etat                   = 2;
        SonarDiagEmi_origine                = 1;
        SonarDiagEmi_ConstructTypeCompens   = 1;
        SonarDiagRec_etat                   = 2;
        SonarDiagRec_origine                = 1;
        SonarDiagRec_ConstructTypeCompens   = 1;
        SonarBS_etat                        = 2;
        
        % Sp�cifique DF1000
        TVGLaw = get(SonarDescription(1), 'Proc.TVGLaw');
        SonarTVG_ConstructAlpha         = TVGLaw.ConstructAlpha;
        SonarTVG_ConstructConstante     = TVGLaw.ConstructConstante;
        SonarTVG_ConstructCoefDiverg    = TVGLaw.ConstructCoefDiverg;
        SonarTVG_ConstructTypeCompens   = TVGLaw.ConstructTypeCompens;
        SonarTVG_ConstructTable         = TVGLaw.ConstructTable;
        
        % ATTENTION FAIRE DEPENDRE DE Frequency
        SonarTVG_IfremerAlpha = 41; % 41 � 112 et 37 � 100;
    otherwise
        my_warndlg('Type of compensation not defined', 1);
        return
end

%% Estimation du temps de retard du poisson par rapport au bateau

subOk = find(Layback > 0);
if isempty(subOk)
    DeltaTFiltre = zeros(size(Layback));
else
    subKO = find(Layback <= 0);
    Layback(subKO) = interp1(subOk, Layback(subOk), subKO, 'linear', 'extrap');
    DeltaT = Layback ./ Speed;
    Wc = 0.05;
    [B,A] = butter(2,Wc);
    DeltaTFiltre = my_filtfilt(B, A, DeltaT, Wc);
    DeltaTFiltre = DeltaTFiltre(:);
end

PingCounter = unwrapPingCounter(PingCounter);
Data.SonarHeight = SonarHeight;

%% Calcul de la latitude et de la longitude

% XTFFileHeader.ProjectionType
% XTFFileHeader.SpheriodType

if FileHeader.NavUnits == 3
    Longitude = Xcoordinate;
    Latitude  = Ycoordinate;
    if my_nanmean(Latitude) >= 0
        Hemisphere = 'N';
    else
        Hemisphere = 'S';
    end
    Carto = cl_carto('Ellipsoide.Type', 11, 'Projection.Type', 3, ...
        'Projection.UTM.Fuseau', floor((my_nanmean(Longitude)+180)/6 + 1), ...
        'Projection.UTM.Hemisphere', Hemisphere);
    % elseif XTFFileHeader.NavUnits == 0
elseif FileHeader.NavUnits == 0 % Modif JMA le 22/04/2015
    Carto = cl_carto('Ellipsoide.Type', 11, 'Projection.Type', 3);
    [flag, Fuseau] = selectProjectionUTM(Carto, 0);
    Carto = set(Carto, 'Projection.UTM.Fuseau', Fuseau);
    if ~flag
        return
    end
    [Latitude, Longitude] = xy2latlon(Carto, Xcoordinate, Ycoordinate);
else
    my_warndlg('NavUnits not defined, coordinates are considered as Lat/long', 0);
    Longitude = Xcoordinate;
    Latitude  = Ycoordinate;
end

FishLongitude = my_interp1_longitude(Data.Time, Longitude', Data.Time+(1000*DeltaTFiltre'));
FishLatitude  = my_interp1(Data.Time, Latitude, Data.Time+(1000*DeltaTFiltre'));
% figure; plot(Longitude, Latitude); grid on;
% figure; plot(FishLongitude, FishLatitude); grid on;

nbPings = BathyChannels.Dimensions.NbPings;
nbBeams = BathyChannels.Dimensions.NbBeams;
if nbBeams <= 1
    flag = 0;
    return
end

y = 1:nbPings;% for k=1:length(nomFicXTF.Reflectivity)
x = 1:nbBeams;
[~, nomFicSeul] = fileparts(this.nomFic);

InitialFileFormat = 'XTF';
GeometryType = cl_image.indGeometryType('PingBeam');
ImageName    = nomFicSeul;

%% Test si la bathy a d�j� �t� calcul�e

[flagBathyProcessed, DataBathymetrie] = SSc_ReadDatagrams(this.nomFic, 'Ssc_Bathymetry', 'Memmapfile', -1); % Pas test�

%% RxBeamAngle

Unit = 'deg';
ColormapIndex = 3;
DataType = cl_image.indDataType('RxBeamAngle');
Comments = FileHeader.NoteString;
if flagBathyProcessed
    X = DataBathymetrie.RxBeamAngle(:,:);
    X(DataBathymetrie.Mask(:,:) ==1) = NaN;
else
    X = BathyChannels.BeamAngle(:,:);
end
b = cl_image('Image', X, 'Name', ImageName, 'Unit', Unit, ...
    'DataType', DataType, 'ColormapIndex', ColormapIndex, ...
    'TagSynchroX', [nomFicSeul ' - PingBeam'], 'TagSynchroY', nomFicSeul,...
    'x', x, 'XUnit', 'Beams', 'y', y, 'YUnit', 'Pings', ...
    'InitialFileName', this.nomFic, ...
    'InitialFileFormat', InitialFileFormat, ...
    'GeometryType', GeometryType,...
    'SonarDescription', SonarDescription, 'Comments', Comments);

%% TwoWayTravelTime

Unit = 's';
ColormapIndex = 3;
DataType = cl_image.indDataType('TwoWayTravelTimeInSeconds');
Comments = FileHeader.NoteString;
if flagBathyProcessed
    X = DataBathymetrie.TwoWayTravelTime(:,:);
    X(DataBathymetrie.Mask(:,:) ==1) = NaN;
else
    X = BathyChannels.TwoWayTravelTime(:,:);
end
b(end+1) = cl_image('Image', X, 'Name', ImageName, 'Unit', Unit, ...
    'DataType', DataType, 'ColormapIndex', ColormapIndex, ...
    'TagSynchroX', [nomFicSeul ' - PingBeam'], 'TagSynchroY', nomFicSeul,...
    'x', x, 'XUnit', 'Beams', 'y', y, 'YUnit', 'Pings', ...
    'InitialFileName', this.nomFic, ...
    'InitialFileFormat', InitialFileFormat, ...
    'GeometryType', GeometryType,...
    'SonarDescription', SonarDescription, 'Comments', Comments);

%% Intensity

Unit = 'dB';
ColormapIndex = 2;
DataType = cl_image.indDataType('Reflectivity');
Comments = FileHeader.NoteString;
if flagBathyProcessed
    X = DataBathymetrie.Reflectivity(:,:);
    X(DataBathymetrie.Mask(:,:) ==1) = NaN;
else
    X = BathyChannels.Intensity(:,:);
end
X = reflec_Amp2dB(X);
b(end+1) = cl_image('Image', X, 'Name', ImageName, 'Unit', Unit, ...
    'DataType', DataType, 'ColormapIndex', ColormapIndex, ...
    'TagSynchroX', [nomFicSeul ' - PingBeam'], 'TagSynchroY', nomFicSeul,...
    'x', x, 'XUnit', 'Beams', 'y', y, 'YUnit', 'Pings', ...
    'InitialFileName', this.nomFic, ...
    'InitialFileFormat', InitialFileFormat, ...
    'GeometryType', GeometryType,...
    'SonarDescription', SonarDescription, 'Comments', Comments);

if flagBathyProcessed
    
    %% AcrossDist
    
    Unit = 'm';
    ColormapIndex = 3;
    DataType = cl_image.indDataType('AcrossDist');
    Comments = FileHeader.NoteString;
    X = DataBathymetrie.AcrossDist(:,:);
    X(DataBathymetrie.Mask(:,:) ==1) = NaN;
    b(end+1) = cl_image('Image', X, 'Name', ImageName, 'Unit', Unit, ...
        'DataType', DataType, 'ColormapIndex', ColormapIndex, ...
        'TagSynchroX', [nomFicSeul ' - PingBeam'], 'TagSynchroY', nomFicSeul,...
        'x', x, 'XUnit', 'Beams', 'y', y, 'YUnit', 'Pings', ...
        'InitialFileName', this.nomFic, ...
        'InitialFileFormat', InitialFileFormat, ...
        'GeometryType', GeometryType,...
        'SonarDescription', SonarDescription, 'Comments', Comments);
    
    %% AlongDist
    
    Unit = 'm';
    ColormapIndex = 3;
    DataType = cl_image.indDataType('AlongDist');
    Comments = FileHeader.NoteString;
    X = DataBathymetrie.AlongDist(:,:);
    X(DataBathymetrie.Mask(:,:) ==1) = NaN;
    b(end+1) = cl_image('Image', X, 'Name', ImageName, 'Unit', Unit, ...
        'DataType', DataType, 'ColormapIndex', ColormapIndex, ...
        'TagSynchroX', [nomFicSeul ' - PingBeam'], 'TagSynchroY', nomFicSeul,...
        'x', x, 'XUnit', 'Beams', 'y', y, 'YUnit', 'Pings', ...
        'InitialFileName', this.nomFic, ...
        'InitialFileFormat', InitialFileFormat, ...
        'GeometryType', GeometryType,...
        'SonarDescription', SonarDescription, 'Comments', Comments);
    
    %% Depth
    
    Unit = 'm';
    ColormapIndex = 3;
    DataType = cl_image.indDataType('Bathymetry');
    Comments = FileHeader.NoteString;
    X = DataBathymetrie.Depth(:,:);
    X(DataBathymetrie.Mask(:,:) ==1) = NaN;
    b(end+1) = cl_image('Image', X, 'Name', ImageName, 'Unit', Unit, ...
        'DataType', DataType, 'ColormapIndex', ColormapIndex, ...
        'TagSynchroX', [nomFicSeul ' - PingBeam'], 'TagSynchroY', nomFicSeul,...
        'x', x, 'XUnit', 'Beams', 'y', y, 'YUnit', 'Pings', ...
        'InitialFileName', this.nomFic, ...
        'InitialFileFormat', InitialFileFormat, ...
        'GeometryType', GeometryType,...
        'SonarDescription', SonarDescription, 'Comments', Comments);
    
end

%% Affectation des signaux verticaux

% TODO : resl(k)
% resolution = resol;
% b = set(b, 'SonarRawDataResol', resolution, 'SonarResolutionD', resolution, 'SonarResolutionX', resolution);

%% Transfert des signaux

for k=1:length(b)
    b(k) = set(b(k), 'SonarHeight',             SonarHeight(:));
    T = Data.Time.timeMat;
    b(k) = set(b(k), 'SonarTime',               timeMat2cl_time(T(:)));
    %     b(k) = set(b(k), 'SonarTime',               timeMat2cl_time(T(:)));% TEST MARDI SOIR
    b(k) = set(b(k), 'SonarHeading',            Heading(:));
    b(k) = set(b(k), 'SonarImmersion',          Immersion(:));
    b(k) = set(b(k), 'SonarSurfaceSoundSpeed',  SoundVelocity(:));
    b(k) = set(b(k), 'SonarCableOut',           Layback(:));
    
    %     b(k) = set(b(k), 'SonarTemperature',  Temperature(:))); % A AJOUTER ULTERIEUREMENT
    
    b(k) = set(b(k), 'SonarPortMode_1',         repmat(Mode_1, nbPings, 1));
    b(k) = set(b(k), 'SonarPortMode_2',         repmat(Mode_2, nbPings, 1));
    
    b(k) = set(b(k), 'SonarRoll',           Roll(:));
    b(k) = set(b(k), 'SonarPitch',          Pitch(:));
    b(k) = set(b(k), 'SonarHeave',          -Heave(:)); % TODO invers� ici car reinvers� dans sonar-range2depth ( Pour sondeurs KM TODO : corriger ce truc � la source)
    b(k) = set(b(k), 'SonarSpeed',          Speed(:));
    b(k) = set(b(k), 'SonarTide',           zeros(nbPings,1));
    b(k) = set(b(k), 'SonarPingCounter',    PingCounter(:));
    
    b(k) = set(b(k), 'SonarShipLongitude', Longitude(:));
    b(k) = set(b(k), 'SonarShipLatitude',  Latitude(:));
    b(k) = set(b(k), 'SonarFishLongitude', FishLongitude(:));
    b(k) = set(b(k), 'SonarFishLatitude',  FishLatitude(:));
    
    SampleFrequency = get(SonarDescription, 'Proc.SampFreq');
    b(k) = set(b(k), 'SonarSampleFrequency',  repmat(SampleFrequency, length(y),1));
    
    %% Transfert des parametres d'etat de la donnee
    
    b(k) = set(b(k),  'Sonar_DefinitionENCours', ...
        'SonarTVG_ConstructTypeCompens',        SonarTVG_ConstructTypeCompens, ...
        'SonarTVG_ConstructTable',              SonarTVG_ConstructTable, ...
        'SonarTVG_ConstructAlpha',              SonarTVG_ConstructAlpha, ...
        'SonarTVG_ConstructConstante',          SonarTVG_ConstructConstante, ...
        'SonarTVG_ConstructCoefDiverg',         SonarTVG_ConstructCoefDiverg, ...
        'SonarTVG_IfremerAlpha',                SonarTVG_IfremerAlpha, ...
        'SonarDiagEmi_etat',                    SonarDiagEmi_etat, ...
        'SonarDiagEmi_origine',                 SonarDiagEmi_origine, ...
        'SonarDiagEmi_ConstructTypeCompens',    SonarDiagEmi_ConstructTypeCompens, ...
        'SonarDiagRec_etat',                    SonarDiagRec_etat, ...
        'SonarDiagRec_origine',                 SonarDiagRec_origine, ...
        'SonarDiagRec_ConstructTypeCompens',    SonarDiagRec_ConstructTypeCompens, ...
        'Sonar_NE_etat',                        Sonar_NE_etat, ...
        'Sonar_SH_etat',                        Sonar_SH_etat, ...
        'Sonar_GT_etat',                        Sonar_GT_etat, ...
        'SonarBS_etat',                         SonarBS_etat);
    
    
    %     EtatSonar.SonarTVG_ConstructAlpha = mean(SonarTVG_ConstructAlpha);
    %     EtatSonar.SonarTVG_IfremerAlpha   = EtatSonar.SonarTVG_ConstructAlpha;
    %     b(k) = set(b(k),  'Sonar_DefinitionENCours', ...
    %          ...
    %         'SonarAireInso_etat',                   EtatSonar.SonarAireInso_etat, ...
    %         'SonarBS_origine',                      EtatSonar.SonarBS_origine, ...
    %         'SonarBS_origineBelleImage',            EtatSonar.SonarBS_origineBelleImage, ...
    %         'SonarBS_origineFullCompens',           EtatSonar.SonarBS_origineFullCompens, ...
    %         'SonarBS_IfremerCompensTable',          EtatSonar.SonarBS_IfremerCompensTable);
    
    %% D�finition d'une carto
    
    if isempty(Carto)
        %         [Carto, flag] = getDefinitionCarto(b(k), 'nomDirSave', nomDirMat, 'NoGeodetic', 1);
        [Carto, flag] = getDefinitionCarto(b(k), 'nomDirSave', nomDirMat, 'NoGeodetic', 0);
        if ~flag
            return
        end
    end
    b(k) = set(b(k), 'Carto', Carto);
    
    %% D�finition du nom de l'image
    
    b(k) = set(b(k), 'Name', ImageName);   % On reimpose le ImageName car il a ete modifie par b(k) = set(b(k),  'S...
    b(k) = update_Name(b(k));
    
    %% On impose un rehaussement de contraste � 0.5% pour pouvoir
    % cont�ler l'image avec ErViewer
    
    StatValues = b(k).StatValues;
    if ~isempty(StatValues)
        b(k).CLim = StatValues.Quant_25_75;
    end
end
flag = 1;
