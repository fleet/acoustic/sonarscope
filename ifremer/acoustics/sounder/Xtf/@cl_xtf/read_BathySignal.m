function [flag, Signal, Unit] = read_BathySignal(this, Invite, varargin)

[varargin, Bathy] = getPropertyValue(varargin, 'Bathy', []); %#ok<ASGLU>

Signal = [];
Unit   = [];

[flag, SonarTime] = get_BathyTime(this, 'Bathy', Bathy);
if ~flag
    return
end

switch Invite
    case 'Immersion'
        NomSignal = 'SensorDepth';
        Unit      = 'm';
    case 'Height'
        NomSignal = 'SensorPrimaryAltitude';
        Unit      = 'm';
    case 'Speed'
        NomSignal = 'ShipSpeed';
        Unit      = 'm/s';
    case 'Heading'
        NomSignal = 'SensorHeading';
        Unit      = 'deg';
    case 'Roll'
        NomSignal = 'SensorRoll';
        Unit      = 'deg';
    case 'Pitch'
        NomSignal = 'SensorPitch';
        Unit      = 'deg';
    case 'Tide'
        NomSignal = 'OceanTide';
        Unit      = 'm';
    case 'PingCounter'
        NomSignal = 'PingNumber';
        Unit      = '';
    case 'Temperature'
        NomSignal = 'WaterTemperature';
        Unit      = '�c';
    otherwise
        NomSignal = Invite;
        Unit      = '';
end

switch NomSignal
    case 'Time'
        Signal = SonarTime.timeMat;
        Unit = 'ms';
        
    otherwise
        [flag, Bathy] = SSc_ReadDatagrams(this.nomFic, 'Ssc_Bathy', 'Memmapfile', -1);
        if ~flag
            return
        end
        Signal = Bathy.(NomSignal)(:,1);
end
