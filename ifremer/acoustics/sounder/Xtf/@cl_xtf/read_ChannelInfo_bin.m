function [flag, Data] = read_ChannelInfo_bin(this, Channel, varargin)

[varargin, Memmapfile] = getPropertyValue(varargin, 'Memmapfile', 0); %#ok<ASGLU>

flag = 0;
Data = [];

%% Lecture du r�pertoire SonarScope

[nomDir, nom] = fileparts(this.nomFic);
nomDir = fullfile(nomDir, 'SonarScope', nom);
if ~exist(nomDir, 'dir')
    return
end

%% Lecture du fichier XML d�crivant la donn�e

nomFicXml = fullfile(nomDir, ['Ssc_ChannelInfo_' num2str(Channel-1) '.xml']);
flag = exist(nomFicXml, 'file');
if ~flag
    return
end
Data = xml_read(nomFicXml);

%% Lecture du r�pertoire ChannelInfo

nomDirPosition = fullfile(nomDir, ['Ssc_ChannelInfo_' num2str(Channel-1)]);
if ~exist(nomDirPosition, 'dir')
    flag = 0;
    return
end

%% Lecture des strings

for i=1:length(Data.Strings)
    [flag, String] = XMLBinUtils.readStrings(nomDir, Data.Strings(i), Data.Dimensions);
    if ~flag
        return
    end
    Data.(Data.Strings(i).Name) = String;
end

%% Lecture des fichiers binaires des signaux

for i=1:length(Data.Signals)
    [flag, Signal] = readSignal(nomDir, Data.Signals(i), Data.Dimensions.NbDatagrams, 'Memmapfile', Memmapfile & ~strcmp(Data.Signals(i).Storage, 'char'));
    if ~flag
        %         return % TODO : Comment� le 28/03/2012 � cause de ChannelName
    end
    Data.(Data.Signals(i).Name) = Signal;
end
% Data = rmfield(Data, {'Images'; 'Comments'});


