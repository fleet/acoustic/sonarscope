function [flag, Data] = read_FileIndex_bin(this)

Data = [];

%% Lecture du fichier XML d�crivant la donn�e 

[nomDirRacine, nomFicSeul] = fileparts(this.nomFic);
nomFicXml = fullfile(nomDirRacine, 'SonarScope', nomFicSeul, 'XTF_FileIndex.xml');
flag = exist(nomFicXml, 'file');
if ~flag
    special_message('FichierNonExistant', nomFicXml)
    return
end
% edit(nomFicXml)
Datagrams = xml_read(nomFicXml);


%{
for i=1:length(Datagrams.Variables)
    fprintf(1,'%d  %s\n', i, Datagrams.Variables(i).Name)
end
%}

%% Lecture des signaux

[nomDirSignal, nomFicSignal] = fileparts(nomFicXml);
nomDirSignal = fullfile(nomDirSignal, nomFicSignal);

% TODO GLU GLU GLU GLU GLU GLU GLU GLU Valeurs fausses
[flag, Date] = Read_BinVariable(Datagrams, nomDirSignal, 'Date', 'double');
if ~flag
    return
end
% figure; plot(Date); grid on; title('Date');

% [flag, NbMilliSecs] = Read_BinVariable(Datagrams, nomDirSignal, 'NbMilliSecs', 'double');
% if ~flag
%     return
% end
% % figure; plot(NbMilliSecs); grid on; title('NbMilliSecs');

%% TypeOfDatagram

clear Data
[flag, Data.TypeOfDatagram] = Read_BinVariable(Datagrams, nomDirSignal, 'TypeOfDatagram', 'single');
if ~flag
    return
end
% figure; plot(Data.TypeOfDatagram, '*'); grid on; title('TypeOfDatagram')

%% Lecture du DatagramLength

[flag, Data.NbOfBytesInDatagram] = Read_BinVariable(Datagrams, nomDirSignal, 'NbOfBytesInDatagram', 'single');
if ~flag
    return
end
% figure; plot(Data.NbOfBytesInDatagram); grid on; title('DatagramLength')

%% Lecture du DatagramPosition

[flag, Data.Position] = Read_BinVariable(Datagrams, nomDirSignal, 'Position', 'double');
if ~flag
    return
end
% figure; plot(Data.Position); grid on; title('DatagramPosition')

% Data.DatagramPosition = [0; Data.DatagramPosition];
% DatagramPosition = [Data.DatagramPosition; sizeFic(this.nomFic)];
% Data.DatagramLength = diff(DatagramPosition);

%% Calcul des dates et heures

% Jour  = mod(Date,100);
% Date  = (Date - Jour) / 100;
% Mois  = mod(Date,100);
% Annee = (Date - Mois) / 100;
% Date = dayJma2Ifr(Jour, Mois, Annee);
% Data.Time = cl_time('timeIfr', Date, NbMilliSecs);
Data.Time = cl_time('timeIfr', Date, 0);
              