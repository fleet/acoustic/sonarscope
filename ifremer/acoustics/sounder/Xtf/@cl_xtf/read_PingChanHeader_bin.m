function [flag, Data] = read_PingChanHeader_bin(this, Channel, varargin)

[varargin, Memmapfile] = getPropertyValue(varargin, 'Memmapfile', 0); %#ok<ASGLU>

flag = 0;
Data = [];

%% Lecture du r�pertoire SonarScope

[nomDir, nom] = fileparts(this.nomFic);
nomDir = fullfile(nomDir, 'SonarScope', nom);
if ~exist(nomDir, 'dir')
    return
end

%% Lecture du fichier XML d�crivant la donn�e

nomFicXml = fullfile(nomDir, ['Ssc_PingChanHeader_' num2str(Channel-1) '.xml']);
flag = exist(nomFicXml, 'file');
if ~flag
    return
end
Data = xml_read(nomFicXml);

%% Lecture du r�pertoire PingChanHeader

nomDirPosition = fullfile(nomDir, ['Ssc_PingChanHeader_' num2str(Channel-1)]);
if ~exist(nomDirPosition, 'dir')
    flag = 0;
    return
end

%% Lecture des fichiers binaires des signaux

for i=1:length(Data.Signals)
    [flag, Signal] = readSignal(nomDir, Data.Signals(i), Data.Dimensions.nbPings, 'Memmapfile', Memmapfile);
    if ~flag
        %         return % TODO : Comment� le 28/03/2012 � cause de ChannelName
    end
    Data.(Data.Signals(i).Name) = Signal;
end

%% Lecture des fichiers binaires des signaux

for i=1:length(Data.Images)
    [flag, Image] = readImage(nomDir, Data.Images(i), Data.Dimensions.nbPings, Data.Dimensions.nbSamples, 'Memmapfile', Memmapfile);
    if ~flag
        %         return % TODO : Comment� le 28/03/2012 � cause de ChannelName
    end
    Data.(Data.Images(i).Name) = Image;
end
% Data = rmfield(Data, {'Images'; 'Comments'});


