% Lecture des donn�es d'un fichier .XTF
%
% Syntax
%   [Data, b, Carto] = read_Sidescan(a, ...)
%
% Input Arguments
%   a : Instance de cl_rdf
%
% Name-Value Pair Arguments
%   Carto : Instance de cl_carto
%
% Output Arguments
%   Data  : Structure contenant :
%   b     : Instances de cl_image contenant la reflectivite et le numero de faisceau
%   Carto : Instance de cl_carto
%
% Examples
%   nomFic = getNomFicDatabase('demowreck.XTF');
%   a = cl_xtf('nomFic', nomFic);
%   [Data, b] = read_Sidescan(a);
%   SonarScope(b)
%
%   figure; plot(Data.Time, Data.PingCounter); grid on; title('PingCounter')
%
% See also cl_rdf cl_rdf/plot_nav Authors
% Authors : JMA
% -------------------------------------------------------------------------

function [Data, b, Carto, subSidescan] = read_Sidescan(this, varargin)

[varargin, Carto]         = getPropertyValue(varargin, 'Carto',         []);
[varargin, subSidescan]   = getPropertyValue(varargin, 'subSidescan',   []);
[varargin, SelectionMode] = getPropertyValue(varargin, 'SelectionMode', 'Multiple'); %#ok<ASGLU>

Data = [];
b = [];

[flag, DataIndex] = read_FileIndex_bin(this);
[flag, Position]  = read_position_bin(this); % Vide
[flag, Attitude]  = read_attitude_bin(this); % Vide

% Changer la valeur de RecordingProgramVersion avant lecture
[flag, FileHeader] = SSc_ReadDatagrams(this.nomFic, 'Ssc_FileHeader', 'XMLOnly', 1);
if ~flag
    return
end

% TODO : pb NumBytesThisRecord � r�soudre
[flag, SideScan] = SSc_ReadDatagrams(this.nomFic, 'Ssc_SideScan', 'Memmapfile', -1);
if ~flag
    FileHeader.NumberOfSonarChannels = 0;
    %     return
end

for k=1:FileHeader.NumberOfSonarChannels
    [flag, DataChannelInfo{k}] = read_ChannelInfo_bin(this, k); %#ok<AGROW>
    [flag, PingChanHeader{k}]  = read_PingChanHeader_bin(this, k); %#ok<AGROW>
end

if isempty(subSidescan)
    [flag, Selection] = select_channels(this, 'SelectionMode', SelectionMode);
    if ~flag
        return
    end
    subSidescan = Selection.subSidescan;
end

if FileHeader.NumberOfSonarChannels ~= 0
    clear b
    for k=1:length(subSidescan)
        [flag, Data, b(k), Carto] = read_Sidescan_unitaire(this, FileHeader, SideScan, DataChannelInfo, PingChanHeader, subSidescan(k), 'Carto', Carto); %#ok<AGROW>
        if ~flag
            return
        end
    end
end


function [flag, Data, b, Carto] = read_Sidescan_unitaire(this, FileHeader, SideScan, DataChannelInfo, PingChanHeader, kS, varargin)

[varargin, Carto] = getPropertyValue(varargin, 'Carto', []); %#ok<ASGLU>

Data = [];
b    = cl_image.empty;

[flag, SonarIdent, Mode_1, Mode_2, XTFSonarType, ChannelName, Frequency] = identification_sonar(FileHeader, DataChannelInfo, PingChanHeader, kS);
if ~flag
    return
end

[nomDir, nomFicSeul] = fileparts(this.nomFic);

SonarDescription = cl_sounder('Sonar.Ident', SonarIdent, 'Sonar.Mode_1', kS);

Roll        = SideScan.SensorRoll(:);
Pitch       = SideScan.SensorPitch(:);
Heave       = SideScan.Heave(:);
Heading     = SideScan.SensorHeading(:);
Speed       = SideScan.ShipSpeed(:); % ShipSpeed ou SensorSpeed
Layback     = SideScan.LayBack(:);
PingCounter = SideScan.PingNumber(:);
Immersion   = SideScan.SensorDepth(:);
Temperature = SideScan.WaterTemperature(:);
Ycoordinate = SideScan.SensorYCoordinate(:);
Xcoordinate = SideScan.SensorXCoordinate(:);
SoundVelocity = SideScan.SoundVelocity(:);
SonarHeight   = SideScan.SensorPrimaryAltitude(:);

nbPings = length(SonarHeight);

[flag, Data.Time] = get_SidescanTime(this, 'SideScan', SideScan);
if ~flag
    return
end

% Date  = dayJma2Ifr(ones(nbPings,1), ones(nbPings,1), double(SideScan.Year(:))) + double(SideScan.JulianDay(:)) - 1;
% Heure = double(SideScan.HSeconds(:)) * 10 + 1000 * (double(SideScan.Second(:)) + double(SideScan.Minute(:)) * 60 + double(SideScan.Hour(:)) * 3600);
% Data.Time = cl_time('timeIfr', Date', Heure');

% NumSamplesMax = 0;
% for k=1:FileHeader.NumberOfSonarChannels
%     SampleNumber = PingChanHeader{k}.NumSamples(:);
%     NumSamplesMax = max(NumSamplesMax, max(SampleNumber));
%     resol = PingChanHeader{k}.SlantRange(:) ./ single(PingChanHeader{k}.NumSamples(:));
%
% %     nbPings = length(SampleNumber);
%
%     if k == 1
%         % TODO : passer en cl_memmapfile ou try catch
%         Reflectivity = NaN(nbPings, 2*NumSamplesMax+1, 'single');
%     end
%
%     Samples = PingChanHeader{k}.Imagery(:,:);
%     if mod(PingChanHeader{k}.ChannelNumber(1), 2) == 0
%         Reflectivity(:,1:SampleNumber(k)) = Samples;
%     else
%         Reflectivity(:,(SampleNumber(k)+2):((2*SampleNumber(k))+1)) = Samples;
%     end
% end

%% Param�trage de la donn�e

TypeCompensation = 1;
switch TypeCompensation
    case 1 % Image compens�e en NE, GT, SH et TVG
        Sonar_NE_etat = 1;
        Sonar_SH_etat = 1;
        Sonar_GT_etat = 1;
        SonarDiagEmi_etat                   = 2;
        SonarDiagEmi_origine                = 1;
        SonarDiagEmi_ConstructTypeCompens   = 1;
        SonarDiagRec_etat                   = 2;
        SonarDiagRec_origine                = 1;
        SonarDiagRec_ConstructTypeCompens   = 1;
        SonarBS_etat                        = 2;
        
        % Sp�cifique DF1000
        TVGLaw = get(SonarDescription(1), 'Proc.TVGLaw');
        SonarTVG_ConstructAlpha         = TVGLaw.ConstructAlpha;
        SonarTVG_ConstructConstante     = TVGLaw.ConstructConstante;
        SonarTVG_ConstructCoefDiverg    = TVGLaw.ConstructCoefDiverg;
        SonarTVG_ConstructTypeCompens   = TVGLaw.ConstructTypeCompens;
        SonarTVG_ConstructTable         = TVGLaw.ConstructTable;
        
        % ATTENTION FAIRE DEPENDRE DE Frequency
        SonarTVG_IfremerAlpha = 41; % 41 � 112 et 37 � 100;
    otherwise
        my_warndlg('Type of compensation not defined', 1);
        return
end

%% Estimation du temps de retard du poisson par rapport au bateau

subOk = find(Layback > 0);
if isempty(subOk)
    DeltaTFiltre = zeros(size(Layback));
else
    subKO = find(Layback <= 0);
    Layback(subKO) = interp1(subOk, Layback(subOk), subKO, 'linear', 'extrap');
    DeltaT = Layback ./ Speed;
    Wc = 0.05;
    [B,A] = butter(2,Wc);
    DeltaTFiltre = my_filtfilt(B, A, DeltaT, Wc);
    DeltaTFiltre = DeltaTFiltre(:);
end

PingCounter = unwrapPingCounter(PingCounter);
Data.SonarHeight = SonarHeight;

% TOTO PingChanHeader{1}
Data.SlantRange  = PingChanHeader{1}.SlantRange(:);

%% Calcul de la latitude et de la longitude

% XTFFileHeader.ProjectionType
% XTFFileHeader.SpheriodType

if FileHeader.NavUnits == 3
    Longitude = Xcoordinate;
    Latitude  = Ycoordinate;
    if my_nanmean(Latitude) >= 0
        Hemisphere = 'N';
    else
        Hemisphere = 'S';
    end
    Carto = cl_carto('Ellipsoide.Type', 11, 'Projection.Type', 3, ...
        'Projection.UTM.Fuseau', floor((my_nanmean(Longitude)+180)/6 + 1), ...
        'Projection.UTM.Hemisphere', Hemisphere);
elseif FileHeader.NavUnits == 0
    if isempty(Carto)
        Carto = cl_carto([]); %#ok<NASGU>
        [flag, Carto] = editParams(cl_carto([]), 0, 0);
        if ~flag
            return
        end
    end
    [Latitude, Longitude] = xy2latlon(Carto, Xcoordinate, Ycoordinate);
else
    my_warndlg('NavUnits not defined, coordinates are considered as Lat/long', 0);
    Longitude = Xcoordinate;
    Latitude  = Ycoordinate;
end

FishLongitude = my_interp1_longitude(Data.Time, Longitude', Data.Time+(1000*DeltaTFiltre'));
FishLatitude  = my_interp1(Data.Time, Latitude, Data.Time+(1000*DeltaTFiltre'));
DistanceInterPings = compute_InterPingDistance(FishLatitude, FishLongitude);

% figure; plot(Longitude, Latitude); grid on;
y = 1:nbPings;% for k=1:length(nomFicXTF.Reflectivity)

k2 = (kS-1)*2;
NumSamplesMax = 0;
for k3=1:2
    if length(PingChanHeader) >= (k2+k3) % Modif JMA le 16/04/2015 Barcelone
        SampleNumber  = PingChanHeader{k2+k3}.NumSamples(:);
        NumSamplesMax = max(NumSamplesMax, max(SampleNumber));
    end
end

[Reflectivity, resol] = get_Reflectivity(kS, PingChanHeader);
Reflectivity = calibration(TypeCompensation, Reflectivity, SonarDescription, XTFSonarType);

Milieu = double(NumSamplesMax);
nbCol = double(2*NumSamplesMax+1);

x = (1:double(nbCol)) - Milieu;
x = double(x) * double(resol(1)); % TODO resol(1)
x = centrage_magnetique(x);

% nomFicSeul = FileHeader.SonarName;
if ~isempty(ChannelName)
    ImageName = [nomFicSeul '_' ChannelName];
end
if Frequency ~= 0
    ImageName = sprintf('%s_%dkHz', nomFicSeul, Frequency);
end

InitialFileFormat = 'XTF';

ImageName = [ImageName '_' num2str(kS)];
Unit = 'dB';
GeometryType = cl_image.indGeometryType('PingRange');
ColormapIndex = 2;
DataType = cl_image.indDataType('Reflectivity');


Comments = FileHeader.NoteString;
b = cl_image('Image', Reflectivity, 'Name', ImageName, 'Unit', Unit, ...
    'DataType', DataType, 'ColormapIndex', ColormapIndex, ...
    'TagSynchroX', [nomFicSeul ' - PingAcrossDist'], 'TagSynchroY', nomFicSeul,...
    'x', x, 'XUnit', 'm', 'y', y, 'YUnit', 'Pings', ...
    'InitialFileName', this.nomFic, ...
    'InitialFileFormat', InitialFileFormat, ...
    'GeometryType', GeometryType,...
    'SonarDescription', SonarDescription, 'Comments', Comments);

% TODO : resl(k)
resolution = resol(kS);
b = set(b, 'SonarRawDataResol', resolution, 'SonarResolutionD', resolution, 'SonarResolutionX', resolution);

b = set(b, 'SonarHeight',             SonarHeight(:));
T = Data.Time.timeMat;
b = set(b, 'SonarTime',               timeMat2cl_time(T(:)));
%     b = set(b, 'SonarTime',               timeMat2cl_time(T(:)));% TEST MARDI SOIR
b = set(b, 'SonarHeading',            Heading(:));
b = set(b, 'SonarImmersion',          Immersion(:));
b = set(b, 'SonarSurfaceSoundSpeed',  SoundVelocity(:));
b = set(b, 'SonarCableOut',           Layback(:));

%     b = set(b, 'SonarTemperature',  Temperature(:))); % A AJOUTER ULTERIEUREMENT

b = set(b, 'SonarPortMode_1',         repmat(Mode_1, nbPings, 1));
b = set(b, 'SonarPortMode_2',         repmat(Mode_2, nbPings, 1));

b = set(b, 'SonarRoll',           Roll(:));
b = set(b, 'SonarPitch',          Pitch(:));
b = set(b, 'SonarHeave',          Heave(:));
b = set(b, 'SonarSpeed',          Speed(:));
b = set(b, 'SonarTide',           zeros(nbPings,1));
b = set(b, 'SonarPingCounter',    PingCounter(:));

b = set(b, 'SonarShipLongitude', Longitude(:));
b = set(b, 'SonarShipLatitude',  Latitude(:));
b = set(b, 'SonarFishLongitude', FishLongitude(:));
b = set(b, 'SonarFishLatitude',  FishLatitude(:));
b = set(b, 'SonarDistPings',     DistanceInterPings(:));

% TEST %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
b = set(b, 'SonarShipLongitude', Longitude(:));
b = set(b, 'SonarShipLatitude',  Latitude(:));
b = set(b, 'SonarFishLongitude', FishLongitude(:));
b = set(b, 'SonarFishLatitude',  FishLatitude(:));
% TEST %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

SampleFrequency = get(SonarDescription, 'Proc.SampFreq');
b = set(b, 'SonarSampleFrequency',  repmat(SampleFrequency, length(y),1));

% --------------------------------------------
% Transfert des param�tres d'�tat de la donn�e

b = set(b,  'Sonar_DefinitionENCours', ...
    'SonarTVG_ConstructTypeCompens',        SonarTVG_ConstructTypeCompens, ...
    'SonarTVG_ConstructTable',              SonarTVG_ConstructTable, ...
    'SonarTVG_ConstructAlpha',              SonarTVG_ConstructAlpha, ...
    'SonarTVG_ConstructConstante',          SonarTVG_ConstructConstante, ...
    'SonarTVG_ConstructCoefDiverg',         SonarTVG_ConstructCoefDiverg, ...
    'SonarTVG_IfremerAlpha',                SonarTVG_IfremerAlpha, ...
    'SonarDiagEmi_etat',                    SonarDiagEmi_etat, ...
    'SonarDiagEmi_origine',                 SonarDiagEmi_origine, ...
    'SonarDiagEmi_ConstructTypeCompens',    SonarDiagEmi_ConstructTypeCompens, ...
    'SonarDiagRec_etat',                    SonarDiagRec_etat, ...
    'SonarDiagRec_origine',                 SonarDiagRec_origine, ...
    'SonarDiagRec_ConstructTypeCompens',    SonarDiagRec_ConstructTypeCompens, ...
    'Sonar_NE_etat',                        Sonar_NE_etat, ...
    'Sonar_SH_etat',                        Sonar_SH_etat, ...
    'Sonar_GT_etat',                        Sonar_GT_etat, ...
    'SonarBS_etat',                         SonarBS_etat);

%     EtatSonar.SonarTVG_ConstructAlpha = mean(SonarTVG_ConstructAlpha);
%     EtatSonar.SonarTVG_IfremerAlpha   = EtatSonar.SonarTVG_ConstructAlpha;
%     b = set(b,  'Sonar_DefinitionENCours', ...
%          ...
%         'SonarAireInso_etat',                   EtatSonar.SonarAireInso_etat, ...
%         'SonarBS_origine',                      EtatSonar.SonarBS_origine, ...
%         'SonarBS_origineBelleImage',            EtatSonar.SonarBS_origineBelleImage, ...
%         'SonarBS_origineFullCompens',           EtatSonar.SonarBS_origineFullCompens, ...
%         'SonarBS_IfremerCompensTable',          EtatSonar.SonarBS_IfremerCompensTable);

%% D�finition d'une carto

if isempty(Carto)
    %         [Carto, flag] = getDefinitionCarto(b, 'nomDirSave', nomDirMat, 'NoGeodetic', 1);
    nomDirMat = fullfile(nomDir, 'SonarScope');
    [Carto, flag] = getDefinitionCarto(b, 'nomDirSave', nomDirMat, 'NoGeodetic', 0);
    if ~flag
        return
    end
end
b = set(b, 'Carto', Carto);

b = set(b, 'Name', ImageName);   % On reimpose le ImageName car il a ete modifie par b = set(b,  'S...
b = update_Name(b);

%% On impose un rehaussement de contraste � 0.5% pour pouvoir cont�ler l'image avec ErViewer

StatValues = b.StatValues;
if ~isempty(StatValues)
    b.CLim = StatValues.Quant_25_75;
end

flag = 1;


function Reflectivity = calibration(TypeCompensation, Reflectivity, SonarDescription, XTFSonarType)

switch TypeCompensation
    case 1 % Image compens�e en NE, GT, SH et TVG
        NE = get_NE(SonarDescription);
        SH = get_SH(SonarDescription);
        GT = get_GT(SonarDescription);
        
        N = floor(size(Reflectivity,2) / 2);
        subBab = 1:N;
        subTri = (N+2):((2*N)+1);
        
        switch XTFSonarType
            case 7 % DF1000
                if ~isempty(NE) && ~isempty(SH) && ~isempty(GT)
                    Reflectivity(:,subBab) = reflec_Amp2dB(Reflectivity(:,subBab)) - (20*log10((2^15 - 1) / 5) + NE(1) + SH(1) + GT(1));
                    Reflectivity(:,subTri) = reflec_Amp2dB(Reflectivity(:,subTri)) - (20*log10((2^15 - 1) / 5) + NE(2) + SH(2) + GT(2));
                else
                    Reflectivity(:,subBab) = reflec_Amp2dB(Reflectivity(:,subBab)) - (20*log10((2^15 - 1) / 5));
                    Reflectivity(:,subTri) = reflec_Amp2dB(Reflectivity(:,subTri)) - (20*log10((2^15 - 1) / 5));
                end
            case 15
                Reflectivity(:,subBab) = reflec_Amp2dB(Reflectivity(:,subBab));
                Reflectivity(:,subTri) = reflec_Amp2dB(Reflectivity(:,subTri));
            otherwise
                Reflectivity(:,subBab) = reflec_Amp2dB(Reflectivity(:,subBab));
                Reflectivity(:,subTri) = reflec_Amp2dB(Reflectivity(:,subTri));
        end
        % Compensation de la TVG
end


function [Reflectivity, resol] = get_Reflectivity(identSidescan, PingChanHeader)

NumSamplesMax = 0;
% for k=1:FileHeader.NumberOfSonarChannels
for k1=1:2
    kChannel = (identSidescan-1)*2 + k1;
    
    if length(PingChanHeader) < kChannel % Modif JMA le 16/04/2015 Barcelone
        break
    end
    SampleNumber = PingChanHeader{kChannel}.NumSamples(:);
    nbPings = length(SampleNumber);
    NumSamplesMax = max(NumSamplesMax, max(SampleNumber));
    resol = PingChanHeader{kChannel}.SlantRange(:) ./ single(PingChanHeader{kChannel}.NumSamples(:));
    
    %     nbPings = length(SampleNumber);
    
    if  mod(kChannel,2) == 1
        % TODO : passer en cl_memmapfile ou try catch
        Reflectivity = NaN(nbPings, 2*NumSamplesMax+1, 'single');
    end
    
    if mod(PingChanHeader{kChannel}.ChannelNumber(1), 2) == 0
        for k=1:nbPings
            Samples = PingChanHeader{kChannel}.Imagery(k,:);
            sub = 1:SampleNumber(k);
            Reflectivity(k,NumSamplesMax-SampleNumber(k)+sub) = Samples(sub);
        end
    else
        for k=1:nbPings
            Samples = PingChanHeader{kChannel}.Imagery(k,:);
            sub = 1:SampleNumber(k);
            %             Reflectivity(k,(SampleNumber(k)+2):((2*SampleNumber(k))+1)) = Samples(sub);
            Reflectivity(k,NumSamplesMax+sub) = Samples(sub);
        end
    end
end
