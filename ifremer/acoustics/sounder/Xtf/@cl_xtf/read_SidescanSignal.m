function [flag, Signal, Unit] = read_SidescanSignal(this, Invite, varargin)

[varargin, SideScan] = getPropertyValue(varargin, 'SideScan', []); %#ok<ASGLU>

Signal = [];
Unit   = [];

[flag, SonarTime] = get_SidescanTime(this, 'SideScan', SideScan);
if ~flag
    return
end

switch Invite
    case 'Immersion'
        NomSignal = 'SensorDepth';
        Unit      = 'm';
    case 'Height'
        NomSignal = 'SensorPrimaryAltitude';
        Unit      = 'm';
    case 'Speed'
        NomSignal = 'ShipSpeed';
        Unit      = 'm/s';
    case 'Heading'
        NomSignal = 'SensorHeading';
        Unit      = 'deg';
    case 'Roll'
        NomSignal = 'SensorRoll';
        Unit      = 'deg';
    case 'Pitch'
        NomSignal = 'SensorPitch';
        Unit      = 'deg';
    case 'Tide'
        NomSignal = 'OceanTide';
        Unit      = 'm';
    case 'PingCounter'
        NomSignal = 'PingNumber';
        Unit      = '';
    case 'Temperature'
        NomSignal = 'WaterTemperature';
        Unit      = '�c';
    otherwise
        NomSignal = Invite;
        Unit      = '';
end

switch NomSignal
    case 'Time'
        Signal = SonarTime.timeMat;
        Unit = 'ms';
        
    case 'DistPings'
        [flag, SonarTimeNav, Latitude, Longitude] = read_navigation(this);
        Signal = compute_InterPingDistance(Latitude, Longitude);
        % TODO : v�rifier si les longueurs sont le m�me sinon faire Signal
        % = my_interp1(SonarTimeNav, Signal, SonarTime)
        Unit = 'm';
        
    otherwise
        [flag, SideScan] = SSc_ReadDatagrams(this.nomFic, 'Ssc_SideScan', 'Memmapfile', -1);
        if ~flag
            return
        end
        Signal = SideScan.(NomSignal);
end
