function [flag, XTFFileHeader] = read_XTFFileHeader(this, nomFic) %#ok<INUSL>

[fid, message] = fopen(nomFic, 'r', 'l');
if fid < 0
    my_warndlg(message, 1);
    return
end

% ------------------------------
% Lecture de l'entete de fichier

[flag, XTFFileHeader] = fread_FileHeader(fid);
if ~flag
    return
end
fclose(fid);
