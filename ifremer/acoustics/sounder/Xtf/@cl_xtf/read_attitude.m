% Lecture des datagrams de attitude dans un fichier .xtf
%
% Syntax
%   Data = read_attitude(a)
% 
% Input Arguments 
%   a : Instance de cl_xtf
%
% Output Arguments 
%   Data : Structure contenant les 
%       Latitude           : Latitude (deg)
%       Longitude          : Longitude (deg)
%       Time               : Temps (cl_time)
%       CourseVessel       : Tengante de la trajectoire (deg)
%       Heading            : Cap du navire (deg)
%       PingCounter        : Numero de Ping
%       EmModel            : Numero du model du dondeur
%       SonarName          : Nom du Sonar
%       SonarType          : Type du Sonar
%
% Examples
%   nomFic = getNomFicDatabase('REBENT166.XTF')
%   nomFic = getNomFicDatabase('demowreck.XTF')
%   nomFic = getNomFicDatabase('demoplane.XTF')
%   a = cl_xtf('nomFic', nomFic);
%   Data = read_attitude(a)
%
%   figure; plot(Data.Longitude, Data.Latitude, '+'); grid on
%
% See also cl_xtf xtf/plot_attitude cl_xtf/plot_nav Authors
% Authors : GLU
% VERSION  : $Id: cl_xtf.m,v 1.1 2006/11/20 11:59:51 rgallou Exp rgallou $
% ----------------------------------------------------------------------------

function Data = read_attitude(this)

% Format d�finis dans XTF.
BYTE    = 'uint8';
WORD    = 'uint16';
DWORD   = 'uint32';%#ok
FLOAT   = 'single';%#ok
LONG    = 'int32';%#ok

XTF_HEADER_ATTITUDE = 3; % TSS or MRU attitude (pitch, roll, heave, yaw)

%% Check the number of elements

flag = checkIfOneElement(length(this));
if ~flag
    Data = [];
    return
end

%% On regarde si le fichier .mat existe

[nomDir, nomFic, ext] = fileparts(this.nomFic);%#ok
nomFicMat = fullfile(nomDir, 'SonarScope', [nomFic '_attitude.mat']);
if exist(nomFicMat, 'file')
    Data = loadmat(nomFicMat, 'nomVar', 'Data');
    return
end

% --------------------------------------
% On cree au besoin le repertoire SonarScope

nomDirMat = fullfile(nomDir, 'SonarScope');
if ~exist(nomDirMat, 'dir')
    [status, msg] = mkdir(nomDirMat);%#ok
    if status
        special_message('CreationRepertoireReussie', nomDirMat)
    else
        special_message('CreationRepertoireEchec', nomDirMat)
        nomFicMat = [];
    end
end

% -------------------------------------------------------------
% On recup�re le nom et type du Sonar car non donn� dans le datagram

[SonarName,SonarType] = get_SonarNameAndType(this);

% --------------------------
% Lecture du fichier d'index

fidIndex = fopen(this.nomFicIndex, 'r', 'l');
if fidIndex == -1
    str = ['Impossible d''ouvrir le fichier ' this.nomFicIndex];
    errordlg(str, 'Erreur')
    return    
end

pppp = fread(fidIndex, [6 Inf], 'uint32');
fclose(fidIndex);

% Date            = pppp(1,:);
% Heure           = pppp(2,:);
Position        = pppp(3,:);
% Taille          = pppp(4,:);
TypeDatagram    = pppp(5,:);
% PingCounter     = pppp(6,:);
clear pppp

sub = find(TypeDatagram == XTF_HEADER_ATTITUDE);
if isempty(sub)
    Data = [];
    return
end
    
Position = Position(sub);
clear TypeDatagram

% ------------------------------
% Decodage du fichier de donnees

typeIndian = 'l';
[fid, message] = fopen(this.nomFic, 'r', typeIndian);
if fid < 0
    disp(message);
    Data = [];
    return
end

N = length(Position);

Data.SonarName          = [];
Data.SonarType          = [];
Data.Roll               = [];
Data.Pitch              = [];
Data.Heave              = [];
Data.Yaw            = [];
Data.Heading            = [];

DateDatagram            = [];
HeureDatagram           = [];

if isempty(nomFicMat)
    [nomDir, nomFic, ext] = fileparts(this.nomFic);%#ok
    str = sprintf('Loading file %s', nomFic);
else
    [nomDir, nomFic, ext] = fileparts(this.nomFic);%#ok
    [nomDir, nomFic2, ext] = fileparts(nomFicMat);%#ok
    str = sprintf('Loading file %s\nCreation file %s', nomFic, nomFic2);
end

hw = create_waitbar(str, 'N', N);
for i=1:N
    my_waitbar(i, N, hw);
    
    fseek(fid, Position(i), 'bof');
    
    % Lecture du mot caract�risant la trame ATTTITUDE � lire.
    MagicNumber = fread(fid, 1, WORD);%#ok
    if feof(fid)
        break; % et non pas return, on peut sortir si Header non tronqu�.
    end
    HeaderType = fread(fid, 1, BYTE);%#ok
    if feof(fid)
        return;
    end

    [flag, AttitudeHeader] = fread_AttitudeHeader(fid);
    if ~flag
        break
    end
    
    Data.SonarName = SonarName;
    Data.SonarType = SonarType;
    date  = dayJma2Ifr(AttitudeHeader.Day, AttitudeHeader.Month, AttitudeHeader.Year);
    % DEBUG ??? date  = dayJma2Ifr(01, 01, 2000);
    heure = AttitudeHeader.MiliSeconds + 1000 * (AttitudeHeader.Second + AttitudeHeader.Minute * 60 + AttitudeHeader.Hour * 3600);
    TagTIME = cl_time('timeIfr', date, heure);
   
    DateDatagram            = [DateDatagram         TagTIME.date]; %#ok<AGROW>
    HeureDatagram           = [HeureDatagram        AttitudeHeader.TimeTag]; %#ok<AGROW>
    % HeureDatagram           = [HeureDatagram        TagTIME.heure];

    Data.Pitch              = [Data.Pitch          AttitudeHeader.Pitch];
    Data.Roll               = [Data.Roll           AttitudeHeader.Roll];
    Data.Heave              = [Data.Heave          AttitudeHeader.Heave];
    Data.Yaw                = [Data.Yaw            AttitudeHeader.Yaw];
    Data.Heading            = [Data.Heading        AttitudeHeader.Heading];
end
my_close(hw)
fclose(fid);

Data.Time = cl_time('timeIfr', DateDatagram, HeureDatagram);

% --------------------------------------------
% Sauvegarde de la donnee dans un fichier .mat

if ~isempty(nomFicMat)
    try
        save(nomFicMat, 'Data')
    catch %#ok<CTCH>
        str = sprintf('Erreur ecriture fichier %s', nomFicMat);
        my_warndlg(str, 1);
    end
end
