function [flag, SonarTime, Latitude, Longitude, SonarSpeed, Heading] = read_navigation(this)

SonarTime   = [];
Latitude    = [];
Longitude   = [];
SonarSpeed  = [];
Heading     = [];

%% Lecture de l'ent�te du fichier

[flag, FileHeader] = SSc_ReadDatagrams(this.nomFic, 'Ssc_FileHeader', 'XMLOnly', 1);
if ~flag
    return
end

%% Lecture de la navigation

% ListSignals{1}     = 'Year';
% ListSignals{end+1} = 'JulianDay';
% ListSignals{end+1} = 'Hour';
% ListSignals{end+1} = 'Minute';
% ListSignals{end+1} = 'Second';
% ListSignals{end+1} = 'HSeconds';
% ListSignals{end+1} = 'SensorXCoordinate';
% ListSignals{end+1} = 'SensorYCoordinate';
% ListSignals{end+1} = 'SensorHeading';
% ListSignals{end+1} = 'ShipSpeed';
[flag, Data] = SSc_ReadDatagrams(this.nomFic, 'Ssc_SideScan', 'Memmapfile', -1);
if flag
    [flag, SonarTime] = get_SidescanTime(this, 'Data', Data);
else
    [flag, Data] = SSc_ReadDatagrams(this.nomFic, 'Ssc_Bathy', 'Memmapfile', -1);
    if ~flag
        return
    end
    [flag, SonarTime] = get_BathyTime(this, 'Data', Data);
end

%% R�cup�ration de la nav du poisson

if FileHeader.NavUnits == 3
    Longitude = Data.SensorXCoordinate(:,1);
    Latitude  = Data.SensorYCoordinate(:,1);
%     if my_nanmean(Latitude) >= 0
%         Hemisphere = 'N';
%     else
%         Hemisphere = 'S';
%     end
%     Carto = cl_carto('Ellipsoide.Type', 11, 'Projection.Type', 3, ...
%         'Projection.UTM.Fuseau', floor((my_nanmean(Longitude)+180)/6 + 1), ...
%         'Projection.UTM.Hemisphere', Hemisphere);

elseif XTFFileHeader.NavUnits == 0
    Carto = cl_carto('Ellipsoide.Type', 11, 'Projection.Type', 3);
    [flag, Fuseau] = selectProjectionUTM(Carto, 0) %#ok
    Carto = set(Carto, 'Projection.UTM.Fuseau', Fuseau);
    if ~flag
        return
    end
    [Latitude, Longitude] = xy2latlon(Carto, Data.SensorXCoordinate(:,1), Data.SensorYCoordinate(:,1));
    
else
    my_warndlg('NavUnits not defined, coordinates are considered as Lat/long', 0);
    Longitude = Data.SensorXCoordinate(:,1);
    Latitude  = Data.SensorYCoordinate(:,1);
end

%% Autre donn�es

SonarSpeed = Data.ShipSpeed;
Heading    = Data.SensorHeading;
    