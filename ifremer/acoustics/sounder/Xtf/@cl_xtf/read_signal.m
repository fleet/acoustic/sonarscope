function [flag, Signal, Unit] = read_signal(this, Invite)

[flag, FileHeader] = SSc_ReadDatagrams(this.nomFic, 'Ssc_FileHeader', 'XMLOnly', 1);
if ~flag
    return
end

if FileHeader.NumberOfBathymetryChannels == 0
    [flag, Signal, Unit] = read_SidescanSignal(this, Invite);
else
    [flag, Signal, Unit] = read_BathySignal(this, Invite);
end
