function flag = save_BathyLayer(this, NomLayer, X)

[nomDir, nom] = fileparts(this.nomFic);
nomDirRacine = fullfile(nomDir, 'SonarScope', nom, 'PingBeam');
nomFicXml = fullfile(nomDirRacine, 'Ssc_Bathymetry.xml');
if ~exist(nomFicXml, 'file')
    flag = 0;
    return
end

Info = xml_read(nomFicXml);

k = strcmp({Info.Images.Name}, NomLayer);
if isempty(k) % TODO : attention, c'est plut�t un ~any(k) qu'il faut tester
    flag = 0;
    return
end

flag = writeSignal(nomDirRacine, Info.Images(k), X);