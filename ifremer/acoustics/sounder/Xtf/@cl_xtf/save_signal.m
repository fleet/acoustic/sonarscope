function flag = save_signal(this, NomRep, NomLayer, X)

% TODO : tester appel � fonction save_signal flag =
% save_signal(this.nomFic, NomLayer, X, )

[nomDir, nom] = fileparts(this.nomFic);
nomFicXml = fullfile(nomDir, 'SonarScope', nom, [NomRep '.xml']);
if ~exist(nomFicXml, 'file')
    str1 = sprintf('Fichier "%s" non trouv�.', nomFicXml);
    str2 = sprintf('File "%s" not found.', nomFicXml);
    my_warndlg(Lang(str1,str2), 1);
    flag = 0;
    return
end

Info = xml_mat_read(nomFicXml);
nomDirRacine = fullfile(nomDir, 'SonarScope', nom);

k = find(strcmp({Info.Signals.Name}, NomLayer));
if length(k) ~= 1
    str1 = sprintf('Variable "%s" non trouv�e dans "%s"', NomLayer, this.nomFic);
    str2 = sprintf('Variable "%s" not found in "%s"', NomLayer, this.nomFic);
    my_warndlg(Lang(str1,str2), 1);
    flag = 0;
    return
end

flag = writeSignal(nomDirRacine, Info.Signals(k), X);