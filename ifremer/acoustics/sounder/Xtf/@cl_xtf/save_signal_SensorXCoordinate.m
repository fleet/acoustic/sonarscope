function flag = save_signal_SensorXCoordinate(this, NavUnits, SignalValues, FishLatitude, Carto)

switch NavUnits
    case 3
        SensorXCoordinate = SignalValues;
    case 0 % TODO : Jamais test�
        nomDir = fileparts(this.nomFic);
        [Carto, flag] = getDefinitionCarto(cl_image, 'nomDirSave', nomDir, 'NoGeodetic', 0);
        if ~flag
            return
        end
%         Carto = cl_carto('Ellipsoide.Type', 11, 'Projection.Type', 3);
%         [flag, Fuseau] = selectProjectionUTM(Carto, 0) %#ok
%         Carto = set(Carto, 'Projection.UTM.Fuseau', Fuseau);
%         if ~flag
%             return
%         end
        [SensorXCoordinate, SensorYCoordinate] = latlon2xy(Carto, FishLatitude, SignalValues);
        flag = save_signal(this, 'Ssc_SideScan', 'SensorYCoordinate', SensorYCoordinate);
        if ~flag
            return
        end
    otherwise
        SensorXCoordinate = SignalValues;
end

[flag, FileHeader] = SSc_ReadDatagrams(this.nomFic, 'Ssc_FileHeader', 'XMLOnly', 1);
if ~flag
    return
end

if FileHeader.NumberOfBathymetryChannels ~= 0
    flag = save_signal(this, 'Ssc_Bathy', 'SensorXCoordinate', repmat(SensorXCoordinate,1,2)); % TODO : corriger ce truc � la source
else
    flag = save_signal(this, 'Ssc_SideScan', 'SensorXCoordinate', SensorXCoordinate);
end
if ~flag
    return
end
