function flag = save_signal_SensorYCoordinate(this, NavUnits, SignalValues, FishLongitude, Carto)

switch NavUnits
    case 3
        SensorYCoordinate = SignalValues;
    case 0
        nomDir = fileparts(this.nomFic);
        [Carto, flag] = getDefinitionCarto(cl_image, 'nomDirSave', nomDir, 'NoGeodetic', 0);
        if ~flag
            return
        end
%         Carto = cl_carto('Ellipsoide.Type', 11, 'Projection.Type', 3);
%         [flag, Fuseau] = selectProjectionUTM(Carto, 0) %#ok
%         if ~flag
%             return
%         end
%         Carto = set(Carto, 'Projection.UTM.Fuseau', Fuseau);
        [SensorXCoordinate, SensorYCoordinate] = latlon2xy(Carto, SignalValues, FishLongitude);
        flag = save_signal(this, 'Ssc_SideScan', 'SensorXCoordinate', SensorXCoordinate);
        if ~flag
            return
        end
    otherwise
        SensorYCoordinate = SignalValues;
end

[flag, FileHeader] = SSc_ReadDatagrams(this.nomFic, 'Ssc_FileHeader', 'XMLOnly', 1);
if ~flag
    return
end

if FileHeader.NumberOfBathymetryChannels ~= 0
    flag = save_signal(this, 'Ssc_Bathy', 'SensorYCoordinate', repmat(SensorYCoordinate,1,2)); % TODO : corriger ce truc � la source
else
    flag = save_signal(this, 'Ssc_SideScan', 'SensorYCoordinate', SensorYCoordinate);
end
if ~flag
    return
end
