function [flag, Selection] = select_channels(this, varargin)

[varargin, SelectionMode]         = getPropertyValue(varargin, 'SelectionMode',         'Multiple');
[varargin, FileHeader]            = getPropertyValue(varargin, 'FileHeader',            []);
[varargin, FileHeaderChannelInfo] = getPropertyValue(varargin, 'FileHeaderChannelInfo', []); %#ok<ASGLU>

Selection = [];

if isempty(FileHeader)
    [flag, FileHeader] = SSc_ReadDatagrams(this.nomFic, 'Ssc_FileHeader', 'XMLOnly', 1);
    if ~flag
        return
    end
end

NumberOfSonarChannels = FileHeader.NumberOfSonarChannels;
if isempty(FileHeaderChannelInfo) && (NumberOfSonarChannels ~= 0)
    for k=1:NumberOfSonarChannels
        [flag, FileHeaderChannelInfo{k}] = read_ChannelInfo_bin(this, k);
        if ~flag
            return
        end
        [flag, PingChanHeader{k}]  = read_PingChanHeader_bin(this, k); %#ok<AGROW>
        if ~flag
            return
        end
        %     [flag, PingChanHeader{k}]        = read_PingChanHeader_bin(this, k);
    end
end

NumberOfBathymetryChannels = FileHeader.NumberOfBathymetryChannels;
for k=1:NumberOfBathymetryChannels
    kB = k + NumberOfSonarChannels;
    [flag, FileHeaderChannelInfo{kB}] = read_ChannelInfo_bin(this, kB); %#ok<ASGLU>
    %     [flag, PingChanHeader{k}]        = read_PingChanHeader_bin(this, k);
end

str = {};
for k=1:2:NumberOfSonarChannels
    % NumberOfSonarChannels = NumberOfSonarChannels / 2; % Modif JMA pour fichier Shallow Survey mais �a va faire boom pour fichiers Ifremer
    % for k=1:NumberOfSonarChannels
    Freq = FileHeaderChannelInfo{k}.Frequency;
    % GLU le 25/01/2013 : les fr�quences sont = 0 depuis le
    % FileHeaderChannelInfo.
    if Freq == 0
        Freq = PingChanHeader{k}.Frequency(1);
    end
    ChannelName = FileHeaderChannelInfo{k}.ChannelName{1};
    ChannelName = strrep(ChannelName, 'Port_', '');
    ChannelName = strrep(ChannelName, 'PORT_', '');
    if strcmp(ChannelName, 'NoChannelName')
        str{end+1,1} = sprintf('Sidescan : Freq# %d : %f (kHz)', 1 + (k-1)/2, Freq); %#ok<AGROW>
    else
        str{end+1,1} = sprintf('Sidescan : %s : %f (kHz)',ChannelName, Freq); %#ok<AGROW>
    end
end
NumberOfSonarChannels = NumberOfSonarChannels / 2;

%{
for k=1:NumberOfBathymetryChannels
%     Freq = FileHeaderChannelInfo{k}.Frequency;
%     str{end+1,1} = sprintf('Bathymetry : Freq# %d : %f (Hz)', 1 + (k-1)/2, Freq); %#ok<AGROW>
if strcmp(FileHeaderChannelInfo{k}.ChannelName{1}, 'NoChannelName')
str{end+1,1} = sprintf('Bathymetry : %d :', k); %#ok<AGROW>
else
str{end+1,1} = sprintf('Bathymetry : %s : %d :', FileHeaderChannelInfo{k}.ChannelName{1}, k); %#ok<AGROW>
end
end
%}

Selection.NumberOfSonarChannels      = NumberOfSonarChannels;
Selection.NumberOfBathymetryChannels = NumberOfBathymetryChannels;
Selection.LayerNames = str;
if length(str) > 1
    str1 = 'TODO';
    str2 = 'Sonar Frequency preprocessing';
    [rep, flag] = my_listdlg(Lang(str1,str2), str, 'InitialValue', 1, 'SelectionMode', SelectionMode);
    if ~flag || isempty(rep)
        return
    end
else
    rep = 1:(NumberOfSonarChannels+NumberOfBathymetryChannels);
end
Selection.subSidescan   = rep(rep <= NumberOfSonarChannels);
Selection.subBathymetry = rep(rep > NumberOfSonarChannels);

flag = 1;

