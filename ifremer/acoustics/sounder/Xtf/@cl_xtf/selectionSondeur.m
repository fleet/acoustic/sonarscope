% Lecture des datagrams "depth" dans un fichier .xtf
%
% Syntax
%   Data = selectionSondeur(a, ...)
% 
% Input Arguments
%   a : Instance de cl_xtf
%
% Examples
%   nomFic = 'F:\SonarScopeData\From CIDCO\20110627_XTF-pour-Altran\0021 - PM-4 - 0001.xtf';
%   a = cl_xtf('nomFic', nomFic);
%   selectionSondeur(a);
%
% See also cl_reson_xtf cl_reson_xtf/plot_position cl_reson_xtf/plot_nav Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function identSondeur = selectionSondeur(this)

%% Check the number of elements

flag = checkIfOneElement(length(this));
if ~flag
    return
end

%%

listeDevice = [7111 7125 7150 8101];
for i=1:length(listeDevice)
    [nomDir, nom] = fileparts(this.nomFicIndex);
    pppp = fullfile(nomDir, nom, ['XTF_' num2str(listeDevice(i)) 'Bathy.xml']);
    flagDevice(i) = exist(pppp, 'file'); %#ok<AGROW>
    strListeDevice{i} = num2str(listeDevice(i)); %#ok<AGROW>
end
subDevice = find(flagDevice == 2);
if isempty(subDevice)
    identSondeur = [];
    return
end
strListeDevice = strListeDevice(flagDevice == 2);
switch length(subDevice)
    case 0
        my_warndlg('No file available', 1);
        identSondeur = [];
    case 1
        identSondeur = listeDevice(subDevice);
    otherwise
        str1 = 'Choix (messag � compl�ter)';
        str2 = 'Select one';
        [rep, flag] = my_listdlg(Lang(str1,str2), strListeDevice, 'SelectionMode', 'Single');
        if ~flag
            identSondeur = [];
            return
        end
        identSondeur = listeDevice(subDevice(rep));
end
