% Acces en ecriture des proprietes d'une instance
%
% Syntax
%   b = set(a, ...)
%
% Input Arguments
%   a : Une instance de la classe cl_xtf
%
% Name-Value Pair Arguments
%   nomFic : Nom(s) du fichier .xtf
%
% Output Arguments
%   [] : Sauve l'instance dans a
%   b  : Instance de la classe
%
% Examples
%   nomFic = getNomFicDatabase('REBENT166.XTF')
%   a = cl_xtf
%   a = set(a, 'nomFic', nomFic)
%
% See also cl_xtf cl_xtf/char Authors
% Authors : GLU
% ----------------------------------------------------------------------------

function varargout = set(this, varargin)

[varargin, nomFic] = getPropertyValue(varargin, 'nomFic', []); %#ok<ASGLU>

if ~isempty(nomFic)
    nomFic = windows_replaceVolumeLetter(nomFic);
    this.nomFic = nomFic;
    [nomDir, nomFicSeul] = fileparts(nomFic);
    this.nomFicIndex = fullfile(nomDir, 'SonarScope', [nomFicSeul '.index']);
    flag = xtf2ssc(nomFic, 'Display', 0);
    if ~flag
        varargout{1} = [];
        return
    end
end

if nargout == 0
    assignin('caller', inputname(1), this);
else
    varargout{1} = this;
end
