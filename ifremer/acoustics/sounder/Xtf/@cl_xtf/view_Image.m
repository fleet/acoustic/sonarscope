% Visualisation du contenu des datagrams "Seabed Image" contenus dans un fichier .rdf
%
% Syntax
%   [b,S] = view_Image(a, ...);
%
% Input Arguments
%   a : Instance de cl_rdf
%
% Name-Value Pair Arguments
%   Carto        :
%   memeReponses :
%   ListeLayers  :
%
% Output Arguments
%   b            : Instance de cli_image
%   Carto        :
%   memeReponses :
%
% Examples
%   nomFic = getNomFicDatabase('REBENT127.XTF')
%   a = cl_xtf('nomFic', nomFic);
%   [flag, b] = view_Image(a);
%   SonarScope(b);
%
% See also cl_rdf cl_rdf/plot_position cl_rdf/plot_nav Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function [flag, b, Carto, memeReponses, selection] = view_Image(this, varargin)

persistent persistent_ListeLayers

[varargin, Carto]        = getPropertyValue(varargin, 'Carto',        []);
[varargin, memeReponses] = getPropertyValue(varargin, 'memeReponses', false);
[varargin, ListeLayers]  = getPropertyValue(varargin, 'ListeLayers',  []);
[varargin, selection]    = getPropertyValue(varargin, 'selection',    []); %#ok<ASGLU>

b = [];

%% Check the number of elements

flag = checkIfOneElement(length(this));
if ~flag
    return
end

%% S�lection des layers � charger

[flag, Selection] = select_channels(this);
if ~flag
    return
end

%% Lecture de la donn�e

b = cl_image.empty;

if ~isempty(Selection.subSidescan)
    [DataSideScan, b1, Carto] = read_Sidescan(this, 'Carto', Carto, 'subSidescan', Selection.subSidescan);
    %     if isempty(DataSideScan)
    %         SlantRange = [];
    %     else
    %         SlantRange = DataSideScan.SlantRange;
    %     end
    b = [b b1];
end

if ~isempty(Selection.subBathymetry)
    [DataBathy, b2, Carto] = read_Bathy(this, 'Carto', Carto); % TODO , 'subSidescan', Selection.subBathymetry);
    b = [b b2];
end
clear b1 b2

if isempty(b)
    str1 = sprintf('Aucune donn�e de type image ou bathy n''a �t� trouv�e dans le fichier "%s".', this.nomFic);
    str2 = sprintf('No sidescan image or bathymetry was found in file "%s".', this.nomFic);
    my_warndlg(Lang(str1,str2), 1);
    return
end

% if isempty(ListeLayers)
%     if ~memeReponses
%         clear str
%         for i=1:length(b)
%             str{i} = b(i).Name; %#ok
%         end
%         ListeLayers = my_listdlg('Layers :', str, 'InitialValue', 1);
%         if isempty(ListeLayers)
%             return
%         end
%         persistent_ListeLayers = ListeLayers;
%     else
%         ListeLayers = persistent_ListeLayers;
%     end
% end
% b = b(ListeLayers);

%% Edition de l'objet cli_image

if nargout == 0
    SonarScope(b);
end

flag = 1;
