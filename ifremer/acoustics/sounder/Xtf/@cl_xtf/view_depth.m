% Visualisation du contenu des datagrams "depth" contenus dans un fichier .xtf
%
% Syntax
%   [b, Carto] = view_depth(a)
%
% Input Arguments
%   a : Instance de cl_xtf
%
% Name-Value Pair Arguments
%   subl         : Numeros de lignes a traiter
%   subc         : Numeros de faisceaux a traiter
%   TagSynchroY  : Tag de synchronisation en Y
%   ListeLayers  : Numeros des layers [] par defaut
%   Carto        : Parametres cartographiques ([] par defaut)
%   memeReponses : true=On pose les questions à chaque appel, false=On mémorise les réponses
%
% Output Arguments
%   b     : Instance de cli_image
%   Carto : Parametres cartographiques
%
% Examples
%   nomFic = 'F:\SonarScopeData\From CIDCO\20110627_XTF-pour-Altran\0021 - PM-4 - 0001.xtf';
%   a = cl_xtf('nomFic', nomFic);
%   b = view_depth(a);
%   b = editobj(b);
%
% See also cl_xtf cl_xtf/plot_position cl_xtf/plot_nav Authors
% Authors : JMA
% ----------------------------------------------------------------------------

function varargout = view_depth(this, varargin)

persistent persistent_ListeLayers persistent_Carto persistent_ListeMasqueBits

[varargin, setMaskToxtfLayers] = getPropertyValue(varargin, 'setMaskToxtfLayers', false);

%% Check the number of elements

flag = checkIfOneElement(length(this));
if ~flag
    varargout{1} = [];
    return
end

[varargin, subl]         = getPropertyValue(varargin, 'subl',         []);
[varargin, subc]         = getPropertyValue(varargin, 'subc',         []);
[varargin, ListeLayers]  = getPropertyValue(varargin, 'ListeLayers',  []);
[varargin, Carto]        = getPropertyValue(varargin, 'Carto',        []);
[varargin, TideFile]     = getPropertyValue(varargin, 'TideFile',     []);
[varargin, memeReponses] = getPropertyValue(varargin, 'memeReponses', false);
[varargin, Bits]         = getPropertyValue(varargin, 'Bits',         []);

identSondeur = selectionSondeur(this);

[nomDir, NomImage] = fileparts(this.nomFic);
NomImage = [num2str(identSondeur) '_' NomImage];

[varargin, TagSynchroY] = getPropertyValue(varargin, 'TagSynchroY', NomImage); %#ok<ASGLU>

if isempty(TideFile)
    Tide = [];
else
    [flag, Tide] = read_Tide(TideFile);
    if flag
        varargout{1} = [];
        varargout{2} = [];
        return
    end
end

[strNomLayers, Unit, Comments] = getListeLayers(this);

if isempty(ListeLayers)
    if ~memeReponses
        ListeLayers = my_listdlg('Layers :', strNomLayers, 'InitialValue', 1:12);
        if isempty(ListeLayers)
            varargout{1} = [];
            varargout{2} = [];
            return
        end
        persistent_ListeLayers = ListeLayers;
    else
        ListeLayers = persistent_ListeLayers;
    end
end

%% Lecture de la donnée

% TODO : Amorce par GLU. à finaliser par JMA.
flagFormat7006 = 1;
if flagFormat7006
    DataDepth = read_depth_bin(this, identSondeur);
    if isempty(DataDepth) || isempty(DataDepth.PingCounter)
        str1 = sprintf('Il n''y a pas de datagramme "7006" dans le fichier %s.xtf : on arrête sa lecture.', NomImage);
        str2 = sprintf('There is no datagram "7006" in file %s.xtf : we stop the import.', NomImage);
        my_warndlg(Lang(str1,str2), 0, 'Tag', 'XTFPasDe70006');
        varargout{1} = [];
        varargout{2} = [];
        return
    end
end

nbPings = length(DataDepth.PingCounter);

%% Lecture des datagrams SonarInstxtfationParameters

DataSonarInstallationParameters = read_sonarInstallationParameters(this, identSondeur);
if isfield(DataSonarInstallationParameters, 'WaterLineVerticalOffset')
    ReceiveArrayZ = DataSonarInstallationParameters.ReceiveArrayZ;
    WaterLineVerticalOffset = DataSonarInstallationParameters.WaterLineVerticalOffset;
    TransducerDepth = ReceiveArrayZ - WaterLineVerticalOffset;
else
    str = sprintf('No 7030 datagram (sonarInstallationParameters) in file %s', this.nomFic);
    my_warndlg(str, 0, 'Tag', 'No7030Datagram');
    
    %     my_warndlg('No 7030 datagrams (Installation parameters).', 1);
    %     varargout{1} = [];
    %     varargout{2} = [];
    %     return
    ReceiveArrayZ = 0;
    WaterLineVerticalOffset = 0;
    TransducerDepth = ReceiveArrayZ - WaterLineVerticalOffset;
end

%% Lecture des datagrams soundSpeedProfile

DataSoundSpeedProfile = read_soundSpeedProfile(this, identSondeur);
if isempty(DataSoundSpeedProfile)
    %     return
end

%% Lecture et interpolation de la donnée "Attitude"

DataAttitude = read_attitude(this, identSondeur);
if isempty(DataAttitude) || isempty(DataAttitude.Roll)
    Roll = DataDepth.Roll;
    Pitch = DataDepth.Pitch;
    Heave = DataDepth.Heave;
    if isempty(Roll) || isempty(Pitch) ||isempty(Heave)
        subVide = find((ListeLayers >= 20) & (ListeLayers < 24));
        if ~isempty(subVide)
            str1 = 'Vous avez demandé le chargement de layers issus des datagrams "Attitude" mais ces datagrams n''existent pas dans votre fichier .xtf';
            str2 = 'TODO';
            my_warndlg(Lang(str1,str2), 0, 'Tag', 'Y''a pas DataAttitude');
            ListeLayers(subVide) = [];
        end
    end
else
    if xtf(isnan(DataDepth.Roll)) % Fichier non complété par PDS
        Roll  = my_interp1(DataAttitude.Time, DataAttitude.Roll,   DataDepth.Time, 'linear', 'extrap');
        Pitch = my_interp1(DataAttitude.Time, DataAttitude.Pitch,  DataDepth.Time, 'linear', 'extrap');
        Heave = my_interp1(DataAttitude.Time, DataAttitude.Heave,  DataDepth.Time, 'linear', 'extrap');
    else % Fichier complété par PDS
        Roll  = DataDepth.Roll;
        Pitch = DataDepth.Pitch;
        Heave = DataDepth.Heave;
    end
    
    % On faisait les plot car les 2 données étaient différentes.
    % L'explication est que l'une est la donnée MRU et l'autre est
    % compensée au niveau du capteur
    %     figure; plot(RollOld);  grid on; hold on; plot(Roll, 'r');  title('Roll'); legend({'Datagram 1016'; 'Datagram 7006'})
    %     figure; plot(PitchOld); grid on; hold on; plot(Pitch, 'r'); title('Pitch'); legend({'Datagram 1016'; 'Datagram 7006'})
    %     figure; plot(HeaveOld); grid on; hold on; plot(Heave, 'r'); title('Heave,'); legend({'Datagram 1016'; 'Datagram 7006'})
end

%% Lecture et interpolation de la donnee "Position"

if xtf(isnan(DataDepth.Roll)) % Fichier non complété par PDS
    DataPosition = read_navigation(this, identSondeur);
    %{
figure; plot(DataPosition.Longitude, DataPosition.Latitude, 'k');
hold on; plot(DataDepth.Longitude, DataDepth.Latitude, 'r'); title('Navigation'); grid on;
legend({'Datagram 7006 (bathy)'; 'Datagram 1015 (navigation)'});
    %}
    
    if isempty(DataPosition) || isempty(DataPosition.Latitude)
        Longitude = NaN(nbPings,1);
        Latitude  = NaN(nbPings,1);
        Heading   = NaN(nbPings,1);
    else
        Longitude = my_interp1(DataPosition.Time, DataPosition.Longitude, DataDepth.Time, 'linear', 'extrap');
        Latitude  = my_interp1(DataPosition.Time, DataPosition.Latitude,  DataDepth.Time, 'linear', 'extrap');
        Heading   = my_interp1(DataPosition.Time, DataPosition.Heading,   DataDepth.Time, 'linear', 'extrap');
        %                          Speed: [1x774 single]
        %                   CourseVessel: [1x774 single]
        %                   VesselHeight: [1x774 single]
    end
else
    Latitude  = DataDepth.Latitude;
    Longitude = DataDepth.Longitude;
    Heading   = DataDepth.Heading;
end

%% Lecture et interpolation de la donnée "SurfaceSoundSpeed"

SurfaceSoundSpeed = DataDepth.SoundSpeed;

%% Lecture et interpolation de la donnee "Mode"

Mode = ones(nbPings,1); %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Creation de l'instance cli_image

c = cli_image;

% disp('Ici, DataVolatileSonarSettings.RTH.Absorption = 1')
DataVolatileSonarSettings.RTH.Absorption = 1; %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
BSStatus = init_BSStatus('Absorption', DataVolatileSonarSettings.RTH.Absorption);
TagSynchroX = [NomImage ' - PingBeam'];

%% Contrôle

[flag, IdentSonar] = DeviceIdentifier2IdentSonar(cl_reson_s7k.empty(), DataDepth.SystemSerialNumber);
if ~flag
    msg = sprintf('Sondeur %d pas inclus dans cl_xtf/view_depth', DataDepth.SystemSerialNumber);
    my_warndlg(msg, 1);
    varargout{1} = [];
    varargout{2} = [];
    return
end

%% Parametres du sondeur definis pour le mode majoritaire


if ~any(Mode)
    Mode(:) = 1; % IMPOSE AUTORITAIREMENT (Cas des données du NIWA)
end
ModeMajoritaire = median(Mode);
SonarDescription = cl_sounder('Sonar.Ident', IdentSonar, 'Sonar.Mode_1', ModeMajoritaire, ...
    'Sonar.SystemSerialNumber', DataDepth.SystemSerialNumber);
resolutionMajoritaire = get_RangeResol(SonarDescription, ModeMajoritaire);
resolution            = get_RangeResol(SonarDescription, Mode);

InitialFileFormat = 'ResonS7k';
% [~, InitialFileName] = fileparts(this.nomFic);
GeometryType = cl_image.indGeometryType('PingBeam');   % BathyFais
y = 1:nbPings; %DataDepth.PingCounter;
YUnit = 'Ping';

%% Cas où il n'y a pas de datagrams d'attitude : on estime Heave sur la donnée

if isempty(Heave)
    tmp = sprintf('Heave is estimated on data because there is no Attitude datagrams found in the file%s', this.nomFic);
    my_warndlg(tmp, 0, 'Tag', 'Y''a pas DataAttitude');
    %              Height = -get(Z, 'Height');
    Height = NaN(1,nbPings);
    for i=1:nbPings
        Height(i) = mean(DataDepth.Depth(i,:), 'omitnan');
    end
    
    [ButterB,ButterA] = butter(2, 0.05);
    subNaN = find(isnan(Height));
    if ~isempty(subNaN)
        subNonNaN = find(~isnan(Height));
        if ~isempty(subNonNaN)
            Height(subNaN) = interp1(subNonNaN, Height(subNonNaN), subNaN);
        end
    end
    if xtf(isnan(Height))
        Heave = zeros(size(Height));
    else
        if length(Height) > 3
            Heightfiltree = filtfilt(ButterB, ButterA, double(Height));
            %             figure; plot(Height, 'b'); grid on
            %             hold on; plot(Heightfiltree, 'r'); grid on
            Heave = Height - Heightfiltree;
        else
            Heave = Height - mean(Height);
        end
    end
end

%% Lecture des differents layers de l'image Caraibes et stockage dans l'instance cli_image

FlagNaN = [];
DepthMask = [];
for i=1:length(ListeLayers)
    nomLayer = strNomLayers{ListeLayers(i)};
    switch nomLayer
        case {'DetectionBrightness'
                'MasqueDetection'
                'ProbaDetection'
                'DetectionColinearity'
                'DetectionNadirFilter'}
            nomLayerxtf = 'QualityFactor';
        case 'Detection'
            if flagFormat7027
                nomLayerxtf = 'Flags';
            else
                nomLayerxtf = 'QualityFactor';
            end
        case {'MasqueDepth'; 'ProbaDepth'; 'DepthMask'}
            nomLayerxtf = 'Depth';
        case {'LongueurTrajet'; 'Temps universel'; 'Roll'; 'Pitch'; 'Heave'; 'Heading'; 'Depth-Heave'; 'RayPathTravelTime'}
            nomLayerxtf = 'Range';
        case 'TxBeamIndex'
            nomLayerxtf = 'BeamDepressionAngle';
        otherwise
            nomLayerxtf = nomLayer;
    end
    %RAJOUTER CA
    % BeamHorizontalDirectionAngle
    if isempty(subl)
        subl = 1:size(DataDepth.(nomLayerxtf),1);
    end
    
    if isempty(subc)
        subc = 1:size(DataDepth.(nomLayerxtf),2);
    end
    
    %% Lecture de l'image
    
    disp(['Reading file : ' NomImage ' - Layer : ' nomLayer])
    
    switch ListeLayers(i)
        case {1;2;3;4;5;6;7;8;9;10;11;12;13;14;15;16;17;27}
            I = DataDepth.(nomLayerxtf)(subl,subc);
            
            if ~isempty(Tide)
                pppp = DataDepth.Time.timeMat;
                TideValue = my_interp1(Tide.Time.timeMat, Tide.Value, pppp(subl));
                
                % Mettre un message si pas d'intersection entre fichier de
                % marée et donnée
                if any(isnan(TideValue))
                    strTide = char(Tide.Time);
                    strFile = char(DataDepth.Time);
                    str1 = sprintf('Pas d''intersection trouvée entre les données de marée fournies et le fichier %s : Je préfère supprimer ces données plutôt que de ne pas faire la correction.\n\nFile time :\n%s\n\nTide time : \n%s', ...
                        this.nomFic, strFile, strTide);
                    str2 =  sprintf('No intersection found between Tide data and file %s  : I prefer to supress this data rather than not to do the correction.\n\nFile time :\n%s\n\nTide time : \n%s', ...
                        this.nomFic, strFile, strTide);
                    my_warndlg(Lang(str1,str2), 0, 'Tag', 'NoIntersectionTide');
                end
                
                TideValue = repmat(TideValue', 1, length(subc));
                I = I + TideValue; % A vérifier
            end
            
            
            %         case {26;27;28;29}
            %             DataMasque = read_masque(this);
            %             I = DataMasque.(nomLayerxtf)(subl,subc);
            
            
            
        case 18
            I = generateDistanceTrajet(DataDepth.Range, resolution, subl, subc);
            
        case 19
            I = genereAbsolutTime(DataDepth.Time, DataDepth.Range, subl, subc);
            
        case 20
            if isempty(DataAttitude) || isempty(DataAttitude.Roll)
                my_warndlg('No Full Attitude available', 1);
                return
            end
            disp('The signal "Roll" is filered because data is quantified (0.01 degree)')
            [B,A] = butter(2, 0.1);
            DataAttitude.Roll = filtfilt(B, A, double(DataAttitude.Roll));
            
            I = genereAbsolutTime(DataDepth.Time, DataDepth.Range, subl, subc);
            I = my_interp1(DataAttitude.Time.timeMat, DataAttitude.Roll, I);
            
        case 21
            disp('On filtre le signal "Pitch" is filered because data is quantified (0.01 degree)')
            [B,A] = butter(2, 0.1);
            DataAttitude.Pitch = filtfilt(B, A, double(DataAttitude.Pitch));
            
            I = genereAbsolutTime(DataDepth.Time, DataDepth.Range, subl, subc);
            I = my_interp1(DataAttitude.Time.timeMat, DataAttitude.Pitch,  I);
            
        case 22
            disp('On filtre le signal "Heave" is filered because data is quantified (0.01 degree)  quantifie a 0.01 m')
            [B,A] = butter(2, 0.1);
            DataAttitude.Heave = filtfilt(B, A, double(DataAttitude.Heave));
            
            I = genereAbsolutTime(DataDepth.Time, DataDepth.Range, subl, subc);
            I = my_interp1(DataAttitude.Time.timeMat, DataAttitude.Heave,  I);
            
        case 23
            disp('On filtre le signal "Heading" is filered because data is quantified (0.01 degree)')
            [B,A] = butter(2, 0.1);
            DataAttitude.Heading = filtfilt(B, A, double(DataAttitude.Heading));
            
            I = genereAbsolutTime(DataDepth.Time, DataDepth.Range, subl, subc);
            I = my_interp1(DataAttitude.Time.timeMat, DataAttitude.Heading,  I);
            
        case 24
            if isempty(DataAttitude)
                H = Heave;
            else
                disp('On filtre le signal "Heave" is filered because data is quantified (0.01 degree)  quantifie a 0.01 m')
                [B,A] = butter(2, 0.1);
                DataAttitude.Heave = filtfilt(B, A, double(DataAttitude.Heave));
                
                %             I = genereAbsolutTime(DataDepth.Time, DataDepth.Range, subl, subc);
                %             J = my_interp1(DataAttitude.Time.timeMat, DataAttitude.Heave,  I);
                
                %             H = my_interp1(DataAttitude.Time.timeMat, DataAttitude.Heave, DataDepth.Time.timeMat(subl));
                pppp = DataDepth.Time.timeMat; % IL Y A UN GROS PROBLEME SI ON FAIT DIRECTEMENT L'INSTRUCTION PRECEDANTE
                H = my_interp1(DataAttitude.Time.timeMat, DataAttitude.Heave, pppp(subl));
            end
            H = repmat(H', 1, length(subc));
            
            if isempty(Tide)
                TideValue = 0;
            else
                TideValue = my_interp1(Tide.Time.timeMat, Tide.Value, pppp(subl));
                TideValue = repmat(TideValue', 1, length(subc));
            end
            
            I = DataDepth.Depth(subl,subc) - H + TideValue;
            
        case 25
            %             I = generateTempsTrajet(DataDepth.Range, subl, subc) / 2;
            I = generateTempsTrajet(DataDepth.Range, subl, subc) * (1000 / 2);
            
        case 26
            I = DataDepth.(nomLayerxtf)(subl,subc);
            %             this = sonar_secteur_emission(this, b, varargin)
    end
    
    strMaskBits = {};
    switch nomLayer
        case 'Detection'
            if flagFormat7027
                subNan = isnan(I);
                W = warning;
                warning('off', 'xtf')
                I = bitand(uint8(I),3);
                warning(W)
                I(subNan) = NaN;
            else
                subNan = isnan(I);
                W = warning;
                warning('off', 'xtf')
                I = bitand(uint8(I),12);
                I = single(bitshift(uint8(I),-2));
                warning(W)
                I(subNan) = NaN;
            end
        case 'DetectionBrightness'
            subNan = isnan(I);
            I(subNan) = 0;
            I = single(bitand(uint8(I),1));
            I(subNan) = NaN;
        case 'DetectionColinearity'
            subNan = isnan(I);
            I(subNan) = 0;
            I = single(bitand(uint8(I),2)) / 2;
            I(subNan) = NaN;
        case 'DetectionNadirFilter'
            subNan = isnan(I);
            I(subNan) = 0;
            I = single(bitand(uint8(I),32)) / 32;
            
            % EN ATTENDANT QUE RESON INVERSE CE BIT
            I = 1 - I; %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % EN ATTENDANT QUE RESON INVERSE CE BIT
            
            I(subNan) = NaN;
            
            
        case 'Mask'
            strMaskBits = {'Brightness'; 'Colinearity'; 'Amplitude'; 'Phase'; 'Do not use'; 'Nadir Filter'; 'Mask by Histogram'};
            
        otherwise
            if isempty(FlagNaN)
                FlagNaN = isnan(I);
            else
                FlagNaN = FlagNaN | isnan(I);
            end
            
            % -------------------
            % Masquage des images
            
            if isempty(Bits)
                if (i == 1)
                    str1 = sprintf('Sélection des bits du facteur de qualité à utiliser pour masquer la donnée "%s"', nomLayer);
                    str2 = sprintf('Quality factors selected for "%s" validation.', nomLayer);
                    strMaskBits = {'Brightness'; 'Colinearity'; 'Amplitude'; 'Phase'; 'Do not Use'; 'Nadir Filter'; 'Mask by Histogram'};
                    if flagFormat7006
                        InitialValue = [1 2 6];
                    else
                        InitialValue = [1 2];
                    end
                    [Bits, flag] = my_listdlgMultiple(Lang(str1,str2), ...
                        strMaskBits, 'InitialValue', InitialValue);
                    if ~flag
                        varargout{1} = [];
                        varargout{2} = [];
                        return
                    end
                    if ~isempty(Bits)
                        setMaskToxtfLayers = true;
                    end
                    persistent_ListeMasqueBits = Bits;
                else
                    Bits = persistent_ListeMasqueBits;
                end
            end
            
            if strcmp(nomLayer, 'DepthMask')
                if isempty(DepthMask)
                    DepthMask = true(length(subl), length(subc));
                end
                for k=1:length(Bits)
                    Val = 2^(Bits(k)-1);
                    K = single(bitand(uint8(DataDepth.Mask),Val)) / Val;
                    subK = K ~= 1;
                    I(subK) = NaN;
                    DepthMask(subK) = false; %#ok<AGROW>
                    clear subK
                end
            end
            
            if flagFormat7006
                %             if strcmp(nomLayer, 'Range')
                TD = DataDepth.QualityFactor(subl,subc);
                subNaN = isnan(TD);
                TD(subNaN) = 0;
                TD = bitand(uint8(TD),12);
                TD = single(bitshift(uint8(TD),-2));
                I(TD == 0) = NaN;
                I(subNaN) = NaN;
                %             end
            else
            end
    end
    
    %% Création de l'instance cl_image
    
    b = cl_image('Image', I);
    b.Name              =  NomImage;
    b.InitialImageName  = nomLayer;
    b.Unit              = Unit{ListeLayers(i)};
    b.TagSynchroX       = TagSynchroX;
    b.TagSynchroY       = TagSynchroY;
    b.XUnit             = 'Beam #';
    b.YUnit             = YUnit;
    b.InitialFileName   = this.nomFic;
    b.InitialFileFormat = InitialFileFormat;
    b.GeometryType      = GeometryType;
    b = set_SonarDescription(b, SonarDescription);
    b.Comments          = Comments{ListeLayers(i)};
    b.strMaskBits       = strMaskBits;
    b.x                 = subc;
    b.y                 = y(subl);
    
    [DataType, ColormapIndex] = identSonarDataType(this, strNomLayers{ListeLayers(i)});
    b.DataType          = DataType;
    b.ColormapIndex     = ColormapIndex;
    
    %% Transfert des signaux
    
    %     b = set_SonarHeight(b, -DataDepth.VehicleDepth);
    %     disp('Ici DataDepth.VehicleDepth remplacé car données non conformes')
    nbFais = size(DataDepth.Depth,2);
    if mod(nbFais,2)
        ifaisMilieu = ceil(nbFais / 2);
        b = set_SonarHeight(b, -abs(DataDepth.Depth(subl,ifaisMilieu)));
    else
        ifaisMilieu = [floor(nbFais / 2) ceil(nbFais / 2)];
        b = set_SonarHeight(b, -abs(mean(DataDepth.Depth(subl,ifaisMilieu),2)));
    end
    
    b = set_SonarTime(b, DataDepth.Time(subl));
    if ~isempty(Heading)
        b = set_SonarHeading(b, Heading(subl));
    end
    
    if isfield(DataDepth, 'ProjectorBeamSteeringAngleVertical')
        X = DataDepth.ProjectorBeamSteeringAngleVertical(subl);
        b = set_SonarTxAlongAngle(b, X(:));
    end
    
    %     b = set_SonarImmersion(b, DataDepth.TransducerDepth);
    X = repmat(TransducerDepth, length(subl), 1);
    b = set_SonarImmersion(b, X);
    
    X = SurfaceSoundSpeed(subl);
    b = set_SonarSurfaceSoundSpeed(b, X(:));
    b = set_SonarPortMode_1(b, Mode(subl));
    
    b = set_SonarRawDataResol(b, resolutionMajoritaire);
    b = set_SonarResolutionD(b, resolutionMajoritaire);
    
    b = set_SonarSampleFrequency(b, DataDepth.SampleRate(:));
    
    if xtf(isnan(DataDepth.Frequency))
        b = set_SonarFrequency(b, DataDepth.MultiPingSequence(:));
    else
        b = set_SonarFrequency(b, DataDepth.Frequency(:));
    end
    
    if ~isempty(Roll)
        X = Roll(subl);
        b = set_SonarRoll(b, X(:));
        X = Pitch(subl);
        b = set_SonarPitch(b, X(:));
        %     b = set(b, 'SonarYaw',   Yaw);
    end
    X = Heave(subl);
    b = set_SonarHeave(b, X(:));
    b = set_SonarTide(b, zeros(length(subl), 1));
    X = DataDepth.PingCounter(subl);
    b = set_SonarPingCounter(b, X(:));
    
    b = set_SonarFishLongitude(b, Longitude(subl));
    b = set_SonarFishLatitude(b, Latitude(subl));
    
    %% Paramètres d'Installation du sondeur
    
    if ~isempty(DataSonarInstallationParameters) && length(DataSonarInstallationParameters.Time) ~= 0 %#ok<ISMT>
        clear X
        X.X       = DataSonarInstallationParameters.TransmitArrayX;
        X.Y       = DataSonarInstallationParameters.TransmitArrayY;
        X.Z       = DataSonarInstallationParameters.TransmitArrayZ;
        X.Roll    = DataSonarInstallationParameters.TransmitArrayRoll;
        X.Pitch   = DataSonarInstallationParameters.TransmitArrayPitch;
        X.Heading = DataSonarInstallationParameters.TransmitArrayHeading;
        b = set_SonarShipArraysTransmit(b, X);
        
        clear X
        X.X       = DataSonarInstallationParameters.ReceiveArrayX;
        X.Y       = DataSonarInstallationParameters.ReceiveArrayY;
        X.Z       = DataSonarInstallationParameters.ReceiveArrayZ;
        X.Roll    = DataSonarInstallationParameters.ReceiveArrayRoll;
        X.Pitch   = DataSonarInstallationParameters.ReceiveArrayPitch;
        X.Heading = DataSonarInstallationParameters.ReceiveArrayHeading;
        b = set_SonarShipArraysReceive(b, X);
        
        clear X
        X.X                  = DataSonarInstallationParameters.MotionSensorX;
        X.Y                  = DataSonarInstallationParameters.MotionSensorY;
        X.Z                  = DataSonarInstallationParameters.MotionSensorZ;
        X.RollCalibration    = DataSonarInstallationParameters.MotionSensorRollCalibration;
        X.PitchCalibration   = DataSonarInstallationParameters.MotionSensorPitchCalibration;
        X.HeadingCalibration = DataSonarInstallationParameters.MotionSensorHeadingCalibration;
        X.TimeDelay          = DataSonarInstallationParameters.MotionSensorTimeDelay;
        b = set_SonarShipMRU(b, X);
        
        clear X
        X.X         = DataSonarInstallationParameters.PositionSensorX;
        X.Y         = DataSonarInstallationParameters.PositionSensorY;
        X.Z         = DataSonarInstallationParameters.PositionSensorZ;
        X.TimeDelay = DataSonarInstallationParameters.PositionSensorTimeDelay;
        b = set_SonarShipGPS(b, X);
        
        clear X
        X.VerticalOffset = DataSonarInstallationParameters.WaterLineVerticalOffset;
        b = set_SonarShipWaterLine(b, X);
    end
    
    %% Transfert des parametres d'état de la donnée
    
    b = set_BSStatus(b, BSStatus);
    
    %% Profil de célérité
    
    if ~isempty(DataSoundSpeedProfile)
        b = set_SonarBathyCel(b, DataSoundSpeedProfile);
    end
    
    %% Definition d'une carto
    
    while isempty(Carto)
        if ~memeReponses
            nomFicCarto = fullfile(nomDir, 'Carto_Directory.def');
            if exist(nomFicCarto, 'file')
                Carto = import_xml(cl_carto([]), nomFicCarto);
                if isempty(Carto)
                    
                    % Au cas où le fichier soit à l'ancienne méthode
                    
                    Carto = import(cl_carto([]), nomFicCarto);
                    flag = export_xml(Carto, nomFicCarto);
                    if flag
                        %                 delete(nomFic)
                    end
                end
            else
                Carto = getDefinitionCarto(b, 'NoGeodetic', 0);
            end
            persistent_Carto = Carto;
        else
            Carto = persistent_Carto;
        end
    end
    b = set_Carto(b, Carto);
    
    
    if ListeLayers(i) == 26
        [DataType, ColormapIndex] = identSonarDataType(this, 'BeamDepressionAngle');
        b.DataType      = DataType;
        b.ColormapIndex = ColormapIndex;
        
        b = sonar_secteur_emission(b, []);
    end
    
    %% Definition du nom de l'image
    
    b = update_Name(b);
    
    %% Ajout de l'image dans l'instance clc_image
    
    c = add_Image(c, b);
end

flagNaN = (sum(FlagNaN(:)) ~= 0);
if flagNaN || setMaskToxtfLayers
    for i=1:length(ListeLayers)
        if ListeLayers(i) == 1
            continue
        end
        b = get_Image(c, i);
        I = get(b, 'Image');
        
        
        %         if flagNaN && (ListeLayers(i) ~= 4) && (ListeLayers(i) ~= 6) % ~strcmp(strNomLayers{ListeLayers(i)}, 'Range)
        if flagNaN && (ListeLayers(i) == 12)
            if isinteger(I)
                I(FlagNaN) = 0;
            else
                I(FlagNaN) = NaN;
            end
        end
        
        switch strNomLayers{ListeLayers(i)}
            case {'Mask'; 'DetectionBrightness'; 'Detection'; 'DetectionColinearity'; 'DetectionNadirFilter'}
            otherwise
                if setMaskToxtfLayers && ~isempty(DepthMask)
                    if isinteger(I)
                        I(~DepthMask) = 0;
                    else
                        I(~DepthMask) = NaN;
                    end
                end
        end
        
        b = set_Image(b, I); % 'PredefinedStats', []
        b = compute_stats(b);
        StatValues = b.StatValues;
        CLim = [StatValues.Min StatValues.Max];
        b.CLim = CLim;
        
        c = set_Image(c, i, b);
    end
end

%% Edition de l'objet cli_image

if nargout == 0
    editobj(c);
else
    varargout{1} = c;
    varargout{2} = Carto;
end


function I = generateDistanceTrajet(Range, resolution, subl, subc)
I = Range(subl,subc) .* repmat(resolution(subl)/2, 1, length(subc));


function I = generateTempsTrajet(Range, subl, subc)
I = Range(subl,subc) / (1500/2);
% I = I * 1000;


function I = genereAbsolutTime(Time, Range, subl, subc)
I = generateTempsTrajet(Range, subl, subc);
Time = Time.timeMat;
Time = Time(subl);
I = repmat(Time(:), 1, length(subc)) + double(I / (24*3600));
% I = I.timeMat;
