function flag = write_attitude_bin(a)


%% Lecture de la donn�e Attitude dans les anciens dormats SonarScope

Carto = [];
selection = [];
SelectionMode = 'Single';
[Data, b, Carto, selection] = read_Sidescan(a, 'Carto', Carto, 'selection', selection, 'SelectionMode', SelectionMode);
        
if isempty(Data) || isempty(Data.Time) || isempty(Data.Time.timeMat)
    flag = 0;
    return
end
nbSamples = length(Data.Time.timeMat);

%% Cr�ation des signaux

Data.Roll    = get(b(1), 'SonarRoll');
Data.Pitch   = get(b(1), 'SonarPitch');
Data.Heave   = get(b(1), 'SonarHeave');
Data.Heading = get(b(1), 'SonarHeading');

%% Cr�ation de la structure d�crivant la donn�e

Info.Title                  = 'Attitude';
Info.Model                  = 'From XTF';
Info.SystemSerialNumber     = 0;
Info.NbSamples              = nbSamples;
Info.Comments               = 'Sounder sampling rate';

Info.Signals(1).Name          = 'Time';
Info.Signals(1).Storage       = 'double';
Info.Signals(1).Unit          = 'days since JC';
Info.Signals(1).Direction	  = 'FirstValue=FirstPing';
Info.Signals(1).FileName      = fullfile('Attitude', 'Time.bin');
Info.Signals(1).Tag           = verifKeyWord('MotionSensorTime');

Info.Signals(end+1).Name      = 'Roll';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).Direction   = 'FirstValue=FirstPing';
Info.Signals(end).FileName    = fullfile('Attitude', 'Roll.bin');
Info.Signals(end).Tag         = verifKeyWord('MotionSensorRoll');

Info.Signals(end+1).Name      = 'Pitch';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).Direction   = 'FirstValue=FirstPing';
Info.Signals(end).FileName    = fullfile('Attitude', 'Pitch.bin');
Info.Signals(end).Tag         = verifKeyWord('MotionSensorPitch');

Info.Signals(end+1).Name      = 'Heave';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'm';
Info.Signals(end).Direction   = 'FirstValue=FirstPing';
Info.Signals(end).FileName    = fullfile('Attitude', 'Heave.bin');
Info.Signals(end).Tag         = verifKeyWord('MotionSensorHeave');

Info.Signals(end+1).Name      = 'Heading';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).Direction   = 'FirstValue=FirstPing';
Info.Signals(end).FileName    = fullfile('Attitude', 'Heading.bin');
Info.Signals(end).Tag         = verifKeyWord('MotionSensorHeading');

%% Cr�ation du r�pertoire SonarScope

[nomDir, nom] = fileparts(a.nomFic);
nomDir = fullfile(nomDir, 'SonarScope', nom);
if ~exist(nomDir, 'dir')
    status = mkdir(nomDir);
    if ~status
        messageErreur(nomDir)
        flag = 0;
        return
    end
end

%% Cr�ation du fichier XML d�crivant la donn�e 

nomFicXml = fullfile(nomDir, 'Attitude.xml');
xml_write(nomFicXml, Info);
flag = exist(nomFicXml, 'file');
if ~flag
    messageErreur(nomFicXml)
    return
end

%% Cr�ation du r�pertoire Attitude

nomDirAttitude = fullfile(nomDir, 'Attitude');
if ~exist(nomDirAttitude, 'dir')
    status = mkdir(nomDirAttitude);
    if ~status
        messageErreur(nomDirAttitude)
        flag = 0;
        return
    end
end

%% Cr�ation des fichiers binaires des signaux

for i=1:length(Info.Signals)
    flag = writeSignal(nomDir, Info.Signals(i), Data.(Info.Signals(i).Name));
    if ~flag
        return
    end
end
