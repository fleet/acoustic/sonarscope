function flag = write_position_bin(a)


%% Lecture de la donn�e Attitude dans les anciens dormats SonarScope

Carto = [];
selection = [];
SelectionMode = 'Single';
[Data, b, Carto, selection] = read_Sidescan(a, 'Carto', Carto, 'selection', selection, 'SelectionMode', SelectionMode);
        
if isempty(Data) || isempty(Data.Time) || isempty(Data.Time.timeMat)
    flag = 0;
    return
end

nbSamples = length(Data.Time.timeMat);


%% Cr�ation des signaux

Data.Latitude       = get(b(1), 'SonarFishLatitude');
Data.Longitude      = get(b(1), 'SonarFishLongitude');
Data.Heading        = get(b(1), 'SonarHeading');
Data.Speed          = get(b(1), 'SonarSpeed');
Data.CourseVessel	= get(b(1), 'SonarFishLatitude');
Data.VesselHeight   = get(b(1), 'SonarHeight');

%% Cr�ation de la structure d�crivant la donn�e

Info.Title                  = 'Navigation';
Info.Model                  = 'From XTF';
Info.SystemSerialNumber     = 0;
Info.NbSamples              = nbSamples;
Info.Comments               = 'Sensor sampling rate';

Info.Signals(1).Name        = 'Time';
Info.Signals(1).Storage     = 'double';
Info.Signals(1).Unit        = 'days since JC';
Info.Signals(1).Direction	= 'FirstValue=FirstPing';
Info.Signals(1).FileName    = fullfile('Position', 'Time.bin');
Info.Signals(1).Tag         = verifKeyWord('GPSTime');

Info.Signals(end+1).Name      = 'Latitude';
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).Direction   = 'FirstValue=FirstPing';
Info.Signals(end).FileName    = fullfile('Position', 'Latitude.bin');
Info.Signals(1).Tag           = verifKeyWord('GPSLatitude');
  
Info.Signals(end+1).Name      = 'Longitude';
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).Direction   = 'FirstValue=FirstPing';
Info.Signals(end).FileName    = fullfile('Position', 'Longitude.bin');
Info.Signals(1).Tag           = verifKeyWord('GPSLongitude');

Info.Signals(end+1).Name      = 'Heading';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).Direction   = 'FirstValue=FirstPing';
Info.Signals(end).FileName    = fullfile('Position', 'Heading.bin');
Info.Signals(1).Tag           = verifKeyWord('GPSHeading???');

Info.Signals(end+1).Name      = 'Speed';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'm/s';
Info.Signals(end).Direction   = 'FirstValue=FirstPing';
Info.Signals(end).FileName    = fullfile('Position', 'Speed.bin');
Info.Signals(1).Tag           = verifKeyWord('GPSSpeed');

Info.Signals(end+1).Name      = 'CourseVessel';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).Direction   = 'FirstValue=FirstPing';
Info.Signals(end).FileName    = fullfile('Position', 'CourseVessel.bin');
Info.Signals(1).Tag           = verifKeyWord('GPSHeading');

Info.Signals(end+1).Name      = 'VesselHeight';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).Direction   = 'FirstValue=FirstPing';
Info.Signals(end).FileName    = fullfile('Position', 'VesselHeight.bin');
Info.Signals(1).Tag           = verifKeyWord('GPSHeight');

%% Cr�ation du r�pertoire SonarScope

[nomDir, nom] = fileparts(a.nomFic);
nomDir = fullfile(nomDir, 'SonarScope', nom);
if ~exist(nomDir, 'dir')
    status = mkdir(nomDir);
    if ~status
        messageErreur(nomDir)
        flag = 0;
        return
    end
end

%% Cr�ation du fichier XML d�crivant la donn�e 

nomFicXml = fullfile(nomDir, 'Position.xml');
xml_write(nomFicXml, Info);
flag = exist(nomFicXml, 'file');
if ~flag
    messageErreur(nomFicXml)
    return
end

%% Cr�ation du r�pertoire Position

nomDirPosition = fullfile(nomDir, 'Position');
if ~exist(nomDirPosition, 'dir')
    status = mkdir(nomDirPosition);
    if ~status
        messageErreur(nomDirPosition)
        flag = 0;
        return
    end
end

%% Cr�ation des fichiers binaires des signaux

for i=1:length(Info.Signals)
    flag = writeSignal(nomDir, Info.Signals(i), Data.(Info.Signals(i).Name));
    if ~flag
        return
    end
end
