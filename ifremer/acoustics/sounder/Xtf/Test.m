nomFic = 'C:\TEMP\2006EE2580837.XTF';
nomFic = 'C:\TEMP\2006EE2631848.XTF';
nomFic = getNomFicDatabase('REBENT127.XTF');
% nomFic = getNomFicDatabase('REBENT127_glu.XTF');
% nomFic = getNomFicDatabase('REBENT166_glu.XTF');
% nomFic = getNomFicDatabase('REBENT166.XTF');
% nomFic = getNomFicDatabase('demowreck.XTF');
% nomFic = getNomFicDatabase('demoplane.XTF');
a = cl_xtf('nomFic', nomFic);

histo_index(a)

Data = read_navigation(a(1)) %#ok<*NOPTS>

X0 = cl_xtf([]);
XTF_PlotNavigation(X0, nomFic);
XTF_PlotNavigation(X0, nomFic, '+');

Data = read_attitude(a(1))
plot_attitude(a)

[Data, b] = read_Sidescan(a);
SonarScope(b)
clear b

[Data, b] = read_Bathy(a);
SonarScope(b)


[flag, b] = view_Image(a);
SonarScope(b);




%Non fonctionnel pour l''instant


Data = read_verticalDepth(a(1))
plot_verticalDepth(a)

Bathy = read_depth(a(1))
figure; imagesc(Bathy.Depth);                colorbar; title('Depth')
figure; imagesc(Bathy.AcrossDist);           colorbar; title('AcrossDist')
figure; imagesc(Bathy.AlongDist);            colorbar; title('AlongDist')
figure; imagesc(Bathy.BeamDepressionAngle);  colorbar; title('BeamDepressionAngle')
figure; imagesc(Bathy.BeamAzimuthAngle);     colorbar; title('BeamAzimuthAngle')
figure; imagesc(Bathy.Range);                colorbar; title('Range')
figure; imagesc(Bathy.QualityFactor);        colorbar; title('QualityFactor')
figure; imagesc(Bathy.Reflectivity);         colorbar; title('Reflectivity'); colormap(gray(256))

view_depth(a(1))


plot_surfaceSoundSpeed(a)
plot_surfaceSoundSpeed_3D(a)

Data = read_soundSpeedProfile(a(1))
plot_soundSpeedProfile(a(1))


000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
Image.Frequency
Image.SampleRate

figure; plot(Image.PingCounter); grid on; title('PingCounter')
figure; plot(Image.DataSampleType); grid on; title('DataSampleType')
figure; plot(Image.NbBeams); grid on; title('NbBeams')
figure; imagesc(Image.BeamDescriptor);        colorbar; title('BeamDescriptor')
figure; imagesc(Image.BeginSampleDescriptor); colorbar; title('BeginSampleDescriptor')
figure; imagesc(Image.EndSampleDescriptor);   colorbar; title('EndSampleDescriptor')
figure; imagesc(Image.CenterSampleDistance);  colorbar; title('CenterSampleDistance')
figure; imagesc(Image.Range);  colorbar; title('Range')

iLig = 600;
iLig = 300;
figure; plot(Image.BeginSampleDescriptor(iLig,:), '+b'); grid on; hold on; plot(Image.EndSampleDescriptor(iLig,:), '+r')

figure; plot(Image.Range(iLig,:), '+'); grid on; title('Range')
hold on; plot(Image.RangeUtil(iLig,:), '+r');

SonarScope(Image.Sonar)

diff(Image.BeginSampleDescriptor(1,1:10))
diff(Image.EndSampleDescriptor(1,1:10))


figure; imagesc(reflec_Enr2dB(Image.Samples)); colormap(gray(256)); colorbar;
Moy = mean(Image.Samples);
figure; plot(Moy); grid on

% for i=1:255
%     subPix = (1+(i-1)*100):i*100;
%     m = reflec_Enr2dB(Moy(subPix))
%     figure; plot(m); grid on
% end

SonarScope(Image.Samples);




Image = read_beamData(a(1))