% [flag, dataFileName] = FtpUtils.getSScDemoFile('Syp_001_180507104600.xtf', 'OutputDir', 'D:\Temp\ExData')
%
% % TODO GLU : gestion Ssc_PingChanHeader_0, ... pas automatique
% XTF_Cache2Netcdf(dataFileName)

function flag = XTF_Cache2Netcdf(dataFileName)

%% Cr�ation du fichier Netcdf

[flag, nomFicNc] = SScCacheNetcdfUtils.createSScNetcdfFile(dataFileName);
if ~flag
    return
end

%% Transfert des donn�es Ssc_FileHeader

flag = SScCacheNetcdfUtils.Cache2Netcdf(nomFicNc, dataFileName, 'FileHeader');
if ~flag
    return
end

%% Transfert des donn�es Ssc_SideScan

flag = SScCacheNetcdfUtils.Cache2Netcdf(nomFicNc, dataFileName, 'SideScan');
if ~flag
    return
end

%% Transfert des donn�es Ssc_Bathy

flag = SScCacheNetcdfUtils.Cache2Netcdf(nomFicNc, dataFileName, 'Bathy');
if ~flag
    return
end

%% Transfert des donn�es Ssc_Bathymetry

flag = SScCacheNetcdfUtils.Cache2Netcdf(nomFicNc, dataFileName, 'Bathymetry');
if ~flag
    return
end

%% Transfert des donn�es Ssc_QPSMultiBeamHeader_0

flag = SScCacheNetcdfUtils.Cache2Netcdf(nomFicNc, dataFileName, 'QPSMultiBeamHeader_0');
if ~flag
    return
end

%% Transfert des donn�es Ssc_PingChanHeader_0

flag = SScCacheNetcdfUtils.Cache2Netcdf(nomFicNc, dataFileName, 'PingChanHeader_0');
if ~flag
    return
end

%% Transfert des donn�es Ssc_PingChanHeader_1

flag = SScCacheNetcdfUtils.Cache2Netcdf(nomFicNc, dataFileName, 'PingChanHeader_1');
if ~flag
    return
end

%% Transfert des donn�es Ssc_PingChanHeader_2

flag = SScCacheNetcdfUtils.Cache2Netcdf(nomFicNc, dataFileName, 'PingChanHeader_2');
if ~flag
    return
end

%% Transfert des donn�es Ssc_PingChanHeader_3

flag = SScCacheNetcdfUtils.Cache2Netcdf(nomFicNc, dataFileName, 'PingChanHeader_3');
if ~flag
    return
end

%% The End

flag = 1;
