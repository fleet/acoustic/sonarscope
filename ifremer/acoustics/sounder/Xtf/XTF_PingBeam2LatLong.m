
function [flag, Mosa, nomDirImport, nomDirExport] = XTF_PingBeam2LatLong(varargin)

[varargin, XLim]         = getPropertyValue(varargin, 'XLim',        []);
[varargin, YLim]         = getPropertyValue(varargin, 'YLim',        []);
[varargin, resol]        = getPropertyValue(varargin, 'Resol',       []);
[varargin, Layers]       = getPropertyValue(varargin, 'Layers',      []);
% [varargin, Backup]      = getPropertyValue(varargin, 'Backup',     []);
[varargin, nomDirImport] = getPropertyValue(varargin, 'nomDirImport', my_tempdir);
[varargin, nomDirExport] = getPropertyValue(varargin, 'nomDirExport', my_tempdir);
[varargin, window]       = getPropertyValue(varargin, 'window',       []);
[varargin, LimitAngles]  = getPropertyValue(varargin, 'LimitAngles',  []); %#ok<ASGLU>

Mosa = [];

E0 = cl_ermapper([]);

%% Type de layer � mosa�quer

if isempty(Layers)
    str1 = 'Layer � mosa�quer';
    str2 = 'Layer to mosaic';
    listeLayers = {'Bathymetry'; 'Reflectivity'};
    [choixLayer, flag] = my_listdlg(Lang(str1,str2), listeLayers, 'InitialValue', 1, 'SelectionMode', 'Single');
    if ~flag
        return
    end
    LayerName = listeLayers{choixLayer};
end

%% Recherche des fichiers .all

[flag, liste] = uiSelectFiles('ExtensionFiles', '.xtf', 'RepDefaut', nomDirImport);
if ~flag || isempty(liste)
    return
end

nbProfils = length(liste);
nomDirImport = fileparts(liste{1});

%% Taille de la grille

[flag, resol] = get_gridSize('Value', -resol, 'Initial', 0.15);
if ~flag
    return
end

%% Backup frequency

[flag, Backup] = saisie_backupFrequency(nbProfils);
if ~flag
    return
end

%% Across distance interpolation method

[flag, AcrossInterpolation] = question_AcrossDistanceInterpolation('QL', 3);
if ~flag
    return
end

%% Mosaic interpolation window

[flag, window] = question_MosaicInterpolationWindow(window);
if ~flag
    return
end

%% Noms des fichiers de sauvegarde des mosaiques

if strcmp(LayerName, 'Bathymetry')
    NomGenerique = 'DTM';
    DataType = cl_image.indDataType('Bathymetry');
else
    NomGenerique = 'Mosaic';
    DataType = cl_image.indDataType('Reflectivity');
end
[nomFicErMapperLayer, flag] = initNomFicErs(E0, NomGenerique, cl_image.indGeometryType('LatLong'), DataType, nomDirExport, 'resol', resol);
if ~flag
    return
end
[nomDirExport, TitreDepth] = fileparts(nomFicErMapperLayer);
DataType = cl_image.indDataType('TxAngle');
[nomFicErMapperAngle, flag] = initNomFicErs(E0, TitreDepth, cl_image.indGeometryType('LatLong'), DataType, nomDirExport, 'confirm', (get_LevelQuestion >= 3));
if ~flag
    return
end
[~, TitreAngle] = fileparts(nomFicErMapperAngle);

%% R�cup�ration des mosaiques existantes

Carto = [];
if exist(nomFicErMapperLayer, 'file') && exist(nomFicErMapperAngle, 'file')
    [flag, rep] = question_CompleteExistingMosaic;
    if ~flag
        return
    end
    if rep == 1
        [flag, Z_Mosa_All] = cl_image.import_ermapper(nomFicErMapperLayer);
        if ~flag
            return
        end
        Carto = get(Z_Mosa_All, 'Carto');
        
        [flag, A_Mosa_All]  = cl_image.import_ermapper(nomFicErMapperAngle);
        if ~flag
            return
        end
    else
        Z_Mosa_All = [];
        A_Mosa_All = [];
    end
else
    Z_Mosa_All = [];
    A_Mosa_All = [];
end

%% Si Mosaique de Depth, on propose de faire la correction de maree

% TODO : utiliser [flag, TideFile] = inputTideFile(isBathy, nomDirImport) � la place de tout ce qui suit
if strcmp(NomGenerique, 'DTM')
    [flag, flagTideCorrection] = question_TideCorrection;
    if ~flag
        return
    end
    if flagTideCorrection && any(choixLayer == 1) || any(choixLayer == 24)    % Depth ou Depth-Heave
        [flag, TideFile] = my_uigetfile({'*.tid*;*.mar','Tide Files (*.tid*,*.mar)';
            '*.tid*',  'Tide file (*.tid*)'; ...
            '*.mar','SHOM tide file (*.mar)'; ...
            '*.*',  'All Files (*.*)'}, 'Give a tide file (cancel instead)', nomDirImport);
        if flag
            read_Tide(TideFile, 'CleanData', 1);
        else
            TideFile = [];
        end
    else
        TideFile = [];
    end
else
    TideFile = [];
end

%% Si Mosaique de Depth, on propose de faire une correction de biais de roulis

if (get_LevelQuestion >= 3) && ((choixLayer == 1) || (choixLayer == 24))    % Depth ou Depth-Heave
    str1 = 'Voulez-vous faire une correction de biais de roulis ?';
    str2 = 'Do you want to do a bias correction for Roll?';
    [rep, flag] = my_questdlg(Lang(str1,str2), 'ColorLevel', 3);
    if ~flag
        return
    end
    if rep == 1
        str1 = 'D�finission de l''angle de correction du roulis';
        str2 = 'Define the angle for the roll correction';
        [flag, BiaisValue] = inputOneParametre(Lang(str1,str2), Lang('Biais en roulis','Roll bias'), ...
            'Value', 0, 'Unit', 'deg');
        if ~flag
            return
        end
    else
        BiaisValue = [];
    end
else
    BiaisValue = [];
end

%% Compensation statistique

[flag, CorFile, ModeDependant, UseModel, PreserveMeanValueIfModel] = QuestionCompensationCurve(nomDirExport);
if ~flag
    return
end

%% Filtrage des spikes si Bathym�trie

if strcmp(NomGenerique, 'DTM')
    str1 = 'Voulez-vous faire une correction de spikes ?';
    str2 = 'Do you want to do a spike removal ?';
    [repSpikeRemoval, flag] = my_questdlg(Lang(str1,str2));
    if ~flag
        return
    end
    if repSpikeRemoval == 1
        str1 = 'D�finission du seuil de rejection des spikes (N fois l''"cart-type dcart-type de la surface locale)';
        str2 = 'Define the threshold for the spike rejection (N time the std of the local surface)';
        [flag, seuilSpike] = inputOneParametre(Lang(str1,str2), Lang('Biais en roulis','Roll bias'), ...
            'Value', 2, 'MinValue', 0.1, 'MaxValue', 50);
        if ~flag
            return
        end
    end
else
    repSpikeRemoval = 2;
end

%% Gestion du recouvrement entre profils

[flag, CoveringPriority, TetaLimite] = question_LinesOverlapping;
if ~flag
    return
end

%% Limitation angulaire de la mosaique

[flag, LimitAngles] = question_AngularLimits('LimitAngles', LimitAngles, 'QL', 2);
if ~flag
    return
end

%% Constitution de la mosaique / MNT

% profile on
% tic
FigResidusNormalisesSpike = [];
for k1=1:nbProfils
    fprintf('\n--------------------------------------------------------------------------------------\n');
    fprintf('Processing file %d/%d : %s\n', k1, nbProfils, liste{k1});
    
    a = cl_xtf('nomFic', liste{k1});
    if isempty(a)
        continue
    end
    
    %     listeLayersSupplementaires = [];
    DataTypeLayersCompensation = [];
    for k2=1:length(CorFile)
        CourbeConditionnelle = load_courbesStats(CorFile{k2});
        DataTypeConditions = CourbeConditionnelle.bilan{1}(1).DataTypeConditions;
        [listeCor, dataTypeCor, flag] = getListeLayersSupplementaires(DataTypeConditions);
        if ~flag
            return
        end
        %         listeLayersSupplementaires = [listeLayersSupplementaires listeCor]; %#ok<AGROW>
        DataTypeLayersCompensation    = [DataTypeLayersCompensation dataTypeCor]; %#ok<AGROW>
    end
    %     listeLayersSupplementaires = unique(listeLayersSupplementaires);
    
    % TODO : g�rer la correction de mar�e
    [DataBathy, c, Carto] = read_Bathy(a, 'Carto', Carto);
    if isempty(c)
        continue
    end
    %     c = get(b, 'Images');
    
    indexLayersCompensation = [];
    for k2=1:length(DataTypeLayersCompensation)
        [flag, indexLayersCompensation(k2)] = findOneLayerDataType(c, 1, DataTypeLayersCompensation(k2)); %#ok<AGROW>
        if ~flag
            return
        end
    end
    
    DT = cl_image.indDataType(LayerName);
    indLayer = findIndLayerSonar(c, 1, 'DataType', DT, 'WithCurrentImage', 1);
    if isempty(indLayer)
        flag = 0;
        str1 = 'Il n''y a pas de bathym�trie dans ce fichier.';
        str2 = 'There is no bathymetry in this file.';
        my_warndlg(Lang(str1,str2), 1);
        return
    end
    Z = c(indLayer);
    
    nbRows = Z.nbRows;
    if nbRows <= 1
        Message = sprintf('The file %s is discarded because it has %d row.', liste{k1}, nbRows);
        my_warndlg(Message, 0);
        continue
    end
    
    DT = cl_image.indDataType('AcrossDist');
    indLayer = findIndLayerSonar(c, 1, 'DataType', DT, 'WithCurrentImage', 1);
    AcrossDistance = c(indLayer);
    
    DT = cl_image.indDataType('AlongDist');
    indLayer = findIndLayerSonar(c, 1, 'DataType', DT, 'WithCurrentImage', 1);
    AlongDistance = c(indLayer);
    
    DT = cl_image.indDataType('RxBeamAngle');
    indLayer = findIndLayerSonar(c, 1, 'DataType', DT, 'WithCurrentImage', 1);
    A = c(indLayer);
    
    %% Gestion de la limite angulaire
    
    if ~isempty(LimitAngles)
        layerMasque = ROI_auto(A, 1, 'MaskReflec', A, [], LimitAngles, 1);
        A = masquage(A, 'LayerMask', layerMasque, 'valMask', 1);
        Z = masquage(Z, 'LayerMask', layerMasque, 'valMask', 1);
    end
    
    %% Gestion du recouvrement entre sondeur dans le cas d'un sondeur dual
    
    Heading        = get(Z, 'Heading');
    FishLatitude   = get(Z, 'FishLatitude');
    FishLongitude  = get(Z, 'FishLongitude');
    
    for k2=1:length(CorFile)
        CourbeConditionnelle = load_courbesStats(CorFile{k2});
        bilan = CourbeConditionnelle.bilan;
        [Z, flag] = compensationCourbesStats(Z, c(indexLayersCompensation), bilan, 'ModeDependant', ModeDependant);
        if ~flag
            return
        end
    end
    
    %% Filtre spike
    
    if repSpikeRemoval == 1
        if isempty(FigResidusNormalisesSpike)
            FigResidusNormalisesSpike = figure;
            FigDiffMNTSpike = figure;
        end
        Z =  filterSpikeBathy(Z, seuilSpike, 'Fig', FigResidusNormalisesSpike, 'Fig2', FigDiffMNTSpike);
        Z = Z(1);
    end
    
    %% Correction de biais de roulis
    
    if ~isempty(BiaisValue) && (BiaisValue ~= 0)
        [Z, AcrossDistance] = sonar_compens_BiaisRoulis(Z, A, AcrossDistance, BiaisValue);
    end
    
    %% Correction de heave si possible
    
    %     if choixLayer == 1
    %         Heave = get(Z, 'Heave');
    %         if isempty(Heave)
    % %              Height = -get(Z, 'Height');
    %             Height = mean_lig(Z);
    %
    %            [ButterB,ButterA] = butter(2, 0.05);
    %             subNaN = find(isnan(Height));
    %             subNonNaN = find(~isnan(Height));
    %             Height(subNaN) = interp1(subNonNaN, Height(subNonNaN), subNaN);
    %             Heightfiltree = filtfilt(ButterB, ButterA, Height);
    % %             figure; plot(Height, 'b'); grid on
    % %             hold on; plot(Heightfiltree, 'r'); grid on
    %             Heave = Height - Heightfiltree;
    %         end
    %         Z = Z - Heave;
    %     end
    
    %% Compensation de donnees si possible
    
    if choixLayer == 9
        
    end
    
    %% Recherche des pings dans la zone d'�tude
    
    if isempty(XLim) || isempty(YLim)
        suby = 1:Z.nbRows;
    else
        SonarFishLatitude  = get(Z, 'SonarFishLatitude');
        SonarFishLongitude = get(Z, 'SonarFishLongitude');
        suby = find((SonarFishLongitude >= XLim(1)) & ...
            (SonarFishLongitude <= XLim(2)) & ...
            (SonarFishLatitude >= YLim(1)) & ...
            (SonarFishLatitude <= YLim(2)));
        clear SonarFishLatitude SonarFishLongitude
        if isempty(suby)
            clear Z AcrossDistance AlongDistance A c
            continue
        end
    end
    
    %% Calcul des coordonn�es g�ographiques
    
    [flag, LatLon] = sonar_calcul_coordGeo_BathyFais(Z, AcrossDistance(1), AlongDistance(1), ...
        Heading, FishLatitude, FishLongitude, 'suby', suby);
    if ~flag
        return
    end
    
    [flag, Z_Mosa, A_Mosa] = sonar_mosaique2(Z, A, LatLon(1), LatLon(2), resol, ...
        'AcrossInterpolation', AcrossInterpolation, 'AlongInterpolation',  1, ...
        'PremiereAlerte', 0);
    if ~flag
        return
    end
    clear LatLon Z A
    
    %% Interpolation
    
    if ~isequal(window, [1 1])
        Z_Mosa = WinFillNaN(Z_Mosa, 'window', window, 'NoStats');
        A_Mosa = WinFillNaN(A_Mosa, 'window', window, 'NoStats');
    end
    
    %% Assemblage de la mosaique de l'image avec la mosaique g�n�rale
    
    [Z_Mosa_All, A_Mosa_All] = mosaique([Z_Mosa_All Z_Mosa], 'Angle', [A_Mosa_All A_Mosa], ...
        'FistImageIsCollector', 1, 'CoveringPriority', CoveringPriority, 'TetaLimite', TetaLimite);
    Z_Mosa_All.Name = TitreDepth;
    A_Mosa_All.Name = TitreAngle;
    clear Mosa
    Mosa(1) = Z_Mosa_All;
    Mosa(2) = A_Mosa_All;
    clear c
    
    %% Sauvegarde des mosaiques dans des fichiers ErMapper
    
    if (mod(k1,Backup) == 0) || (k1 == nbProfils)
        Z_Mosa_All = majCoordonnees(Z_Mosa_All);
        A_Mosa_All = majCoordonnees(A_Mosa_All);
        StatValues = Z_Mosa_All.StatValues;
        CLim = StatValues.Quant_25_75;
        export_ermapper(Z_Mosa_All, nomFicErMapperLayer, 'CLim', CLim);
        export_ermapper(A_Mosa_All, nomFicErMapperAngle);
    end
    
    %% Suppression des fichier memmapfile
    
    deleteFileOnListe(cl_memmapfile.empty, gcbf)
end
flag = 1;
% toc
% profile report

% % InitialFileName = sprintf('Not saved for the moment. Internal code for mosaic asembling : %f', rand(1));
% for k1=1:length(Mosa)
%     Mosa(k1) = majCoordonnees(Mosa(k1));
% %     Mosa(k1) = set(Mosa(k1), 'InitialFileName', InitialFileName);
% end

if nargout == 0
    SonarScope(Mosa)
end
