function flag = XTF_PingSamples2PingAcrossDist(varargin)

persistent persistent_nomDir persistent_nomDirErs

[varargin, nomDir] = getPropertyValue(varargin, 'nomDir', my_tempdir);
[varargin, resol]  = getPropertyValue(varargin, 'Resol', []); %#ok<ASGLU>

if ~isempty(persistent_nomDir)
    nomDir = persistent_nomDir;
end

if isempty(persistent_nomDirErs)
    nomDirErs = nomDir;
else
    nomDirErs = persistent_nomDirErs;
end

%% Recherche des fichiers .sdf

[flag, liste] = uiSelectFiles('ExtensionFiles', '.xtf', 'RepDefaut', nomDir);
if ~flag || isempty(liste)
    return
end
nbProfils = length(liste);
nomDir = fileparts(liste{1});
persistent_nomDir = nomDir;

%% Saisie de la r�solution

[flag, resol] = get_gridSize('Value', -resol, 'Initial', 0.2);
if ~flag
    return
end

%% Nom du r�pertoire de stockage des images

str1 = 'S�lectionner un r�pertoire o� stocker les images en PingAcrossDist';
str2 = 'Please select a folder for the PingAcrossDist images';
[flag, nomDirErs] = my_uigetdir(nomDirErs, Lang(str1,str2));
if ~flag
    return
end
persistent_nomDirErs = nomDirErs;

%% Si Mosaique de R�flectivite, on propose de faire une compensation statistique ou une calibration

% [flag, TypeCalibration, CorFile, bilan, DataTypeConditions, alpha, ModelTxDiag, CourbeBS] = params_SonarCompensation(nomDir);
[flag, TypeCalibration, CorFile, bilan] = params_SonarCompensation(nomDir);
if ~flag
    return
end

%% Traitement des fichiers

% profile on
% tic

Selection = [];
Carto     = [];
for k=1:nbProfils
    fprintf('Processing file %d/%d : %s\n', k, nbProfils, liste{k});
    
    %% Lecture des layers "Range", "AcrossDistance", "AlongDistance" et "BeamDepressionAngle"
    
    a = cl_xtf('nomFic', liste{k});
    if isempty(a)
        continue
    end
    
    %     if ~isNavPreprocessed(a);
    %         continue
    %     end
    
    if isempty(Selection)
        [flag, Selection] = select_channels(a, 'SelectionMode', 'Single');
        if ~flag
            return
        end
    end
    
    if Selection.NumberOfSonarChannels ~= 0
        [Data, c, Carto] = read_Sidescan(a, 'Carto', Carto, 'subSidescan', Selection.subSidescan);
    end
    
    if Selection.NumberOfBathymetryChannels ~= 0
        [Data, c, Carto] = read_Bathy(a, 'Carto', Carto);
    end
    
    clear a
    nbRows = c.nbRows;
    if nbRows <= 1
        Message = sprintf('The file %s is discarded because it has %d row.', liste{k}, nbRows);
        my_warndlg(Message, 0);
        continue
    end
    
    GT_Image = c.GeometryType;
    if TypeCalibration == 1
        GT_Bilan = cl_image.indGeometryType(bilan{1}(1).GeometryType);
        if ~isempty(CorFile) && (GT_Image == GT_Bilan)
            TxAngle_PingSamples = sonar_lateral_emission(c, 'useRoll', 0);
            c = compensationCourbesStats(c, TxAngle_PingSamples, bilan);
            clear TxAngle_PingSamples
        end
    end
    
    Reflectivity_PingAcrossDist = sonarLateral_PingRange2PingAcrossDist(c, 'resolutionX', resol, 'createNbp', 0);
    clear c
    
    %% Compensation de l'image si la courbe a �t� calcul�e dans la g�om�trie
    % PingSamples
    
    TxAngle_PingAcrossDist = sonar_lateral_emission(Reflectivity_PingAcrossDist, 'useRoll', 0);
    GT_Image = Reflectivity_PingAcrossDist.GeometryType;
    if TypeCalibration == 1
        GT_Bilan = cl_image.indGeometryType(bilan{1}(1).GeometryType);
        if ~isempty(CorFile) && (GT_Image == GT_Bilan)
            Reflectivity_PingAcrossDist = compensationCourbesStats(Reflectivity_PingAcrossDist, TxAngle_PingAcrossDist, bilan);
        end
    end
    
    %% Sauvegarde des images
    
    nomImage = Reflectivity_PingAcrossDist.Name;
    nomFicErs = fullfile(nomDirErs, [nomImage '.ers']);

    flag = export_ermapper(Reflectivity_PingAcrossDist, nomFicErs);
    if ~flag
        % Message SVP
    end
    
end
flag = 1;

% toc
% profile report
