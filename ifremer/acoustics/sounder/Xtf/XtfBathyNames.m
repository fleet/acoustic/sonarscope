% Génération automatique des noms de fichiers SonarScope de l'imagerie des
% sondeur Feoacoustic au format RDF
%
% Syntax
%   [nomFic, status] = XtfBathyNames(nomFic)
%
% Input Arguments
%   nomFic : Nom du fichier .XTF
%
% Output Arguments
%   nomFic : Structure contenant les noms des différents fichiers SonarScope.
%   status    : 1= tous les fichiers existent, 0 sinon
%
% Examples
%   nomFic = getNomFicDatabase('REBENT127.XTF');
%   nomFicRDF = XtfBathyNames(nomFic)
%
% See also Authors
% Authors : JMA
%-------------------------------------------------------------------------------

 function [nomFicRDF, status] = XtfBathyNames(nomFicxTF)

[status, XTFFileHeader] = read_XTFFileHeader(cl_xtf([]), nomFicxTF);
if ~status
    return
end

if XTFFileHeader.NumberOfSonarChannels > 2
    nbFiles = 2;
else
    nbFiles = 1;
end

[nomDir, nomFicSeul] = fileparts(nomFicxTF);
nomFicRDF.Data = fullfile(nomDir, 'SonarScope', [nomFicSeul '_Data.mat']);

for i=1:nbFiles
    nomFicRDF.Bathy{i}    = fullfile(nomDir, 'SonarScope', [nomFicSeul '_Bathy' num2str(i)], [nomFicSeul '_Bathy' num2str(i)]);
    nomFicRDF.BathyErs{i} = fullfile(nomDir, 'SonarScope', [nomFicSeul '_Bathy'  num2str(i) '.ers']);
end

status = exist(nomFicRDF.Data, 'file');
for i=1:nbFiles
    status = status && ...
        exist(nomFicRDF.Bathy{i}, 'file') &&  ...
        exist(nomFicRDF.BathyErs{i} , 'file') &&  ...
        exist([nomFicRDF.Bathy{i} '_ers.mat'], 'file') &&  ...
        exist([nomFicRDF.Bathy{i} '_sigV'], 'file');
end
    