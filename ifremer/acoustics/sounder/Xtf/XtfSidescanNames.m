% Génération automatique des noms de fichiers SonarScope de l'imagerie des
% sondeur Feoacoustic au format RDF
%
% Syntax
%   [nomFic, status] = XtfSidescanImageNames(nomFic)
%
% Input Arguments
%   nomFic : Nom du fichier .XTF
%
% Output Arguments
%   nomFic : Structure contenant les noms des différents fichiers SonarScope.
%   status    : 1= tous les fichiers existent, 0 sinon
%
% Examples
%   nomFic = getNomFicDatabase('REBENT127.XTF');
%   nomFicXtf = XtfSidescanImageNames(nomFic)
%
% See also Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function [selection, nomFicXtf, flag] = XtfSidescanNames(nomFicxTF, varargin)

[varargin, selection]     = getPropertyValue(varargin, 'selection',     []);
[varargin, SelectionMode] = getPropertyValue(varargin, 'SelectionMode', 'Multiple'); %#ok<ASGLU>

nomFicXtf = [];

[flag, XTFFileHeader] = read_XTFFileHeader(cl_xtf([]), nomFicxTF);
if ~flag
    return
end

strFrequency = {};
for i=1:2:XTFFileHeader.NumberOfSonarChannels
    Freq = XTFFileHeader.ChanInfo(i).Frequency;
    strFrequency{end+1} = sprintf('Freq# %d : %f (Hz)', (i-1)/2, Freq); %#ok<AGROW>
end


if isempty(selection)
    selection.Frequency = strFrequency;
    if length(strFrequency) > 1
        [rep, flag] = my_listdlg(Lang('TODO', 'Sonar Frequency preprocessing'), ...
            strFrequency, 'InitialValue', 1, 'SelectionMode', SelectionMode);
        if ~flag || isempty(rep)
            return
        end
    else
        rep = 1;
    end
    selection.subChannel = rep;
end

[nomDir, nomFicSeul] = fileparts(nomFicxTF);
nomFicXtf.Data = fullfile(nomDir, 'SonarScope', [nomFicSeul '_Data.mat']);

nbFiles = length(selection.subChannel);
for i=1:nbFiles
    k = selection.subChannel(i);
    nomFicXtf.Reflectivity{i}    = fullfile(nomDir, 'SonarScope', [nomFicSeul '_Reflectivity_' num2str(k)], [nomFicSeul '_Reflectivity_' num2str(k)]);
    nomFicXtf.ReflectivityErs{i} = fullfile(nomDir, 'SonarScope', [nomFicSeul '_Reflectivity_'  num2str(k) '.ers']);
end

flag = exist(nomFicXtf.Data, 'file');
for i=1:nbFiles
    flag = flag && ...
        exist(nomFicXtf.Reflectivity{i}, 'file') &&  ...
        exist(nomFicXtf.ReflectivityErs{i} , 'file') &&  ...
        exist([nomFicXtf.Reflectivity{i} '_ers.mat'], 'file') &&  ...
        exist([nomFicXtf.Reflectivity{i} '_sigV'], 'file');
end

