% Realisation de la mosaique de donnees en g�om�trie PingSamples d'une campagne
%
% Syntax
%   [flag, Mosa, nomDir] = Xtf_PingSamples2LatLong(...)
%
% Name-Value Pair Arguments
%   nomDir : Nom du r�pertoire des donnees
%   resol  : Pas de la grille en m�tres
%   Backup : P�riode de sauvegarde des fichiers de sortie
%
% Output Arguments
%   []     : Auto-plot activation
%   flag   : Description du parametre (unite).
%   a      : Instances de I0 contenant la mosaique et la mosaique angulaire
%   nomDir : Nom du r�pertoire des images cr��es
%
% Examples
%   Xtf_PingSamples2LatLong
%   flag, a] = Xtf_PingSamples2LatLong('NomDir', '/home1/jackson/FEMME2005/Donnees', 'Resol', 10);
%   SonarScope(a)
%   [flag, a] = Xtf_PingSamples2LatLong('NomDir', 'D:\NIWA_DATA\TAN0105', 'Resol', 10, 'Backup', 5);
%   SonarScope(a)
%
% See also Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function [flag, Mosa, nomDir] = Xtf_PingSamples2LatLong(liste, resol, Backup, AcrossInterpolation, LimitAngles, window, ...
    nomFicErMapperLayer, nomFicErMapperAngle, TitreDepth, CompleteExistingMosaic, bilan, varargin)


Mosa = [];

if ischar(liste)
    liste = {liste};
end
nbProfils = length(liste);
nomDir = fileparts(liste{1});

%% R�cup�ration des mosa�ques existantes

Carto = [];
if exist(nomFicErMapperLayer, 'file') && exist(nomFicErMapperAngle, 'file')
    if CompleteExistingMosaic
        [flag, Z_Mosa_All] = cl_image.import_ermapper(nomFicErMapperLayer);
        if ~flag
            return
        end
        Carto = get(Z_Mosa_All, 'Carto');
        
        [flag, A_Mosa_All]  = cl_image.import_ermapper(nomFicErMapperAngle);
        if ~flag
            return
        end
    else
        Z_Mosa_All = [];
        A_Mosa_All = [];
    end
else
    Z_Mosa_All = [];
    A_Mosa_All = [];
end

%% Traitement des fichiers

Selection = [];

for k1=1:nbProfils
    fprintf('Processing file %d/%d : %s\n', k1, nbProfils, liste{k1});
    
    %% Lecture des layers "Range", "AcrossDistance", "AlongDistance" et "BeamDepressionAngle"
    
    a = cl_xtf('nomFic', liste{k1});
    
    if isempty(Selection)
        [flag, Selection] = select_channels(a, 'SelectionMode', 'Single');
        if ~flag
            return
        end
    end
    
    if Selection.NumberOfSonarChannels ~= 0
        [~, c, Carto] = read_Sidescan(a, 'Carto', Carto, 'subSidescan', Selection.subSidescan);
    end
    
    %     if Selection.NumberOfBathymetryChannels ~= 0
    %         [Data, c, Carto] = read_Bathy(a, 'Carto', Carto);
    %     end
    
    nbRows = c(1).nbRows;
    if nbRows <= 1
        Message = sprintf('The file %s is discarded because it has %d row.', liste{k1}, nbRows);
        my_warndlg(Message, 0);
        continue
    end
    
    FishLatitude  = get(c(1), 'FishLatitude');
    FishLongitude = get(c(1), 'FishLongitude');
    Heading       = get(c(1), 'Heading');
    
    Reflectivity_PingAcrossDist = sonarLateral_PingRange2PingAcrossDist(c(1), 'resolutionX', resol, 'createNbp', 0);
    
    [flag, LatLon] = sonar_calcul_coordGeo_SonarX(Reflectivity_PingAcrossDist, Heading, ...
        FishLatitude, FishLongitude);
    if ~flag
        continue
    end
    clear c
    
    %% Compensation de l'image si la courbe a �t� calcul�e dans la g�om�trie PingSamples
    
    TxAngle_PingAcrossDist = sonar_lateral_emission(Reflectivity_PingAcrossDist, 'useRoll', 0);
    
    %% Gestion de la limite angulaire
    
    if ~isempty(LimitAngles)
        layerMasque = ROI_auto(TxAngle_PingAcrossDist, 1, 'MaskReflec', TxAngle_PingAcrossDist, [], LimitAngles, 1);
        TxAngle_PingAcrossDist = masquage(TxAngle_PingAcrossDist, 'LayerMask', layerMasque, 'valMask', 1);
        Reflectivity_PingAcrossDist = masquage(Reflectivity_PingAcrossDist, 'LayerMask', layerMasque, 'valMask', 1);
        LatLon(1) = masquage(LatLon(1), 'LayerMask', layerMasque, 'valMask', 1);
        LatLon(2) = masquage(LatLon(2), 'LayerMask', layerMasque, 'valMask', 1);
    end
    
%     if TypeCalibration == 1
%         GT_Image = Reflectivity_PingAcrossDist.GeometryType;
%         GT_Bilan = cl_image.indGeometryType(bilan{1}(1).GeometryType);
        if ~isempty(bilan) % && (GT_Bilan == GT_Image)
            Reflectivity_PingAcrossDist = compensationCourbesStats(Reflectivity_PingAcrossDist, TxAngle_PingAcrossDist, bilan);
        end
%     end
    
    nbRows = Reflectivity_PingAcrossDist.nbRows;
    nbCol = Reflectivity_PingAcrossDist.nbColumns;
    pas = floor(nbRows / ((nbRows * nbCol * 4) / 1e7)); % SizePhysTotalMemory
    
    NBlocks = ceil(nbRows/pas);
    pas = ceil(nbRows/NBlocks);
    
    for k2=1:pas:nbRows
        subl = max(1,k2-5):min(k2-1+5+pas, nbRows);
        fprintf('Processing pings [%d:%d] Fin=%d\n', subl(1), subl(end), nbRows);
        
        [flag, Z_Mosa, A_Mosa] = sonar_mosaique2(Reflectivity_PingAcrossDist, TxAngle_PingAcrossDist, ...
            LatLon(1), LatLon(2), resol, ...
            'AcrossInterpolation', AcrossInterpolation, 'AlongInterpolation', 2, 'suby', subl);
        if ~flag
            return
        end
        
        %% Interpolation
        
        if ~isequal(window, [1 1])
            Z_Mosa = WinFillNaN(Z_Mosa, 'window', window, 'NoStats');
            A_Mosa = WinFillNaN(A_Mosa, 'window', window, 'NoStats');
        end
        
        %% Assemblage avec la mosa�que globale
        
        Z_Mosa.Writable = false;
        A_Mosa.Writable = false;
        
        if isempty(Z_Mosa_All)
            Z_Mosa_All = Z_Mosa;
            A_Mosa_All = A_Mosa;
        else
            [Z_Mosa_All, A_Mosa_All] = mosaique([Z_Mosa_All Z_Mosa], 'Angle', [A_Mosa_All A_Mosa], ...
                'FistImageIsCollector', 1);
            clear Z_Mosa A_Mosa
        end
        Z_Mosa_All.Name = extract_ImageName(Z_Mosa_All, 'Name', TitreDepth);
        A_Mosa_All.Name = extract_ImageName(A_Mosa_All, 'Name', TitreDepth);
        Z_Mosa_All = update_Name(Z_Mosa_All);
        A_Mosa_All = update_Name(A_Mosa_All);
        Z_Mosa_All.Writable = false;
        A_Mosa_All.Writable = false;
    end
    clear Reflectivity_PingAcrossDist TxAngle_PingAcrossDist LatLon
    
    %% Sauvegarde des mosaiques dans des fichiers ErMapper
    
    if (mod(k1,Backup) == 0) || (k1 == nbProfils)
        Z_Mosa_All = majCoordonnees(Z_Mosa_All);
        A_Mosa_All = majCoordonnees(A_Mosa_All);
        export_ermapper(Z_Mosa_All, nomFicErMapperLayer);
        export_ermapper(A_Mosa_All, nomFicErMapperAngle);
        
        % D�placement Mikael
        deleteFileOnListe(cl_memmapfile.empty, gcbf)
        % D�placement Mikael
    end
end
flag = 1;

clear Mosa
Mosa(1) = Z_Mosa_All;
Mosa(2) = A_Mosa_All;
