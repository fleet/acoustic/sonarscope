% Codes des datagrams des formats XTF (TRITON Imaging Inc.)
%
% Syntax
%   texteTypeDatagram = codesDatagramsXTF(...)
%
% Name-Value Pair Arguments
%   tabHistoTypeDatagram : Histogramme des types de datagram
%
% Name-Value Pair Arguments
%   fullListe : 0=affichage que des datagrams presents, 1=tous les datagrams
%
% Output Arguments
%   texteTypeDatagram : Chaine de caracteres
%       0 = Reference point
%       1 = Sensor offset position
%
% Examples
%   codesDatagramsXTF
%
% See also creFicIndex Authors
% Authors : GLU
%-----------------------------------------------------------------------

function texteTypeDatagram = codesDatagramsXTF(varargin)

[varargin, fullListe] = getPropertyValue(varargin, 'fullListe', 0);

% TODO : continuel la liste, bizarre, �a ne correspond pas � la nouvelle
% liste

XTF_HEADER_SONAR					= 0;		% Sidescan data
XTF_HEADER_NOTES					= 1;		% Notes - text annotation
XTF_HEADER_BATHY					= 2;		% Bathymetry data
XTF_HEADER_ATTITUDE					= 3;		% Attitude packet
XTF_HEADER_FORWARD					= 4;		% Forward look data (Sonatech)
XTF_HEADER_ELAC						= 5;		% Elac raw data packet.
XTF_HEADER_RAW_SERIAL				= 6;		% Raw ASCII serial port data.
XTF_HEADER_EMBED_HEAD				= 7;		% Embedded header record - num samples probably changed.
XTF_HEADER_HIDDEN_SONAR				= 8;		% Redundant (overlapping) ping from Klein 5000.
XTF_HEADER_SEAVIEW_PROCESSED_BATHY	= 9;		% Bathymetry (angles) for Seaview.
XTF_HEADER_SEAVIEW_DEPTHS			= 10;		% Bathymetry from Seaview data (depths).
XTF_HEADER_RSVD_HIGHSPEED_SENSOR	= 11;		% Used by Klein. 0 roll, 1= yaw
XTF_HEADER_ECHOSTRENGTH				= 12;		% Elac EchoStrength (10 values).
XTF_HEADER_GEOREC					= 13;		% Used to store mosaic parameters.
XTF_HEADER_KLEIN_RAW_BATHY			= 14;		% Bathymetry data from the Klein 5000.
XTF_HEADER_HIGHSPEED_SENSOR2		= 15;		% High speed sensor from Klein 5000.
XTF_HEADER_ELAC_XSE					= 16;		% Elac dual-head.
XTF_HEADER_BATHY_XYZA				= 17;		%
XTF_HEADER_K5000_BATHY_IQ			= 18;		% Raw IQ data from Klein 5000 server
XTF_HEADER_BATHY_SNIPPET			= 19;		% 81xx snippet Packet
XTF_HEADER_GPS                      = 20;		% GPS Position.
XTF_HEADER_STAT						= 21;		% GPS statistics.
XTF_HEADER_SINGLEBEAM				= 22;		%
XTF_HEADER_GYRO						= 23;		%Heading/Speed Sensor.
XTF_HEADER_TRACKPOINT				= 24;		%
XTF_HEADER_MULTIBEAM				= 25;		%
XTF_HEADER_Q_SINGLEBEAM				= 26;		%
XTF_HEADER_Q_MULTITX				= 27;		%
XTF_HEADER_Q_MULTIBEAM				= 28;		%
XTF_HEADER_TIME						= 50;		%
XTF_HEADER_BENTHOS_CAATI_SARA		= 60;		% Custom Benthos data.
XTF_HEADER_7125						= 61;		% 7125 Bathy Data
XTF_HEADER_7125_SNIPPET				= 62;		% 7125 Bathy Data Snippets
XTF_HEADER_QINSY_R2SONIC_BATHY		= 65;		% Quincy R2Sonic bathymetry data
XTF_HEADER_QINSY_R2SONIC_FTS		= 66;		% Quincy R2Sonics Foot Print Time Series (snippets)
XTF_HEADER_R2SONIC_BATHY			= 68;		% Triton R2Sonic bathymetry data
XTF_HEADER_R2SONIC_FTS				= 69;		% Triton R2Sonic Footprint Time Series
XTF_HEADER_CODA_ECHOSCOPE_DATA		= 70;		% Custom CODA Echoscope Data
XTF_HEADER_CODA_ECHOSCOPE_CONFIG	= 71;		% Custom CODA Echoscope Data
XTF_HEADER_CODA_ECHOSCOPE_IMAGE		= 72;		% Custom CODA Echoscope Data
XTF_HEADER_EDGETECH_4600			= 73;		%
XTF_HEADER_RESON_7027_BATHY			= 76;		% (Type 7027 RAW Detection Data � see Reson document 7k Data Format, Vol 1, Ver 2.00 para 10.38)
XTF_HEADER_POSITION					= 100;      % Raw position packet - Reserved for use by Reson, Inc. RESON ONLY.
XTF_HEADER_BATHY_PROC				= 102;      %
XTF_HEADER_ATTITUDE_PROC			= 103;      %
XTF_HEADER_SINGLEBEAM_PROC			= 104;      %
XTF_HEADER_AUX_PROC					= 105;      % Aux Channel + AuxAltitude + Magnetometer.
XTF_HEADER_KLEIN3000_DATA_PAGE		= 106;      %
XTF_HEADER_POS_RAW_NAVIGATION		= 107;      %
XTF_HEADER_KLEINV4_DATA_PAGE		= 108;      %
XTF_HEADER_USERDEFINED				= 200;      %

LabelsTypeDatagram{XTF_HEADER_SONAR+1}					=	'Sidescan data';
LabelsTypeDatagram{XTF_HEADER_NOTES+1}					=	'Notes - text annotation';
LabelsTypeDatagram{XTF_HEADER_BATHY+1}					=	'Bathymetry data';
LabelsTypeDatagram{XTF_HEADER_ATTITUDE+1}				=	'Attitude packet';
LabelsTypeDatagram{XTF_HEADER_FORWARD+1}				=	'Forward look data (Sonatech)';
LabelsTypeDatagram{XTF_HEADER_ELAC+1}					=	'Elac raw data packet.';
LabelsTypeDatagram{XTF_HEADER_RAW_SERIAL+1}				=	'Raw ASCII serial port data.';
LabelsTypeDatagram{XTF_HEADER_EMBED_HEAD+1}				=	'Embedded header record - num samples probably changed.';
LabelsTypeDatagram{XTF_HEADER_HIDDEN_SONAR+1}			=	'Redundant (overlapping) ping from Klein 5000.';
LabelsTypeDatagram{XTF_HEADER_SEAVIEW_PROCESSED_BATHY+1}=	'Bathymetry (angles) for Seaview.';
LabelsTypeDatagram{XTF_HEADER_SEAVIEW_DEPTHS+1}			=	'Bathymetry from Seaview data (depths).';
LabelsTypeDatagram{XTF_HEADER_RSVD_HIGHSPEED_SENSOR+1}	=	'Used by Klein. 0 roll, 1=yaw';
LabelsTypeDatagram{XTF_HEADER_ECHOSTRENGTH+1}			=	'Elac EchoStrength (10 values).';
LabelsTypeDatagram{XTF_HEADER_GEOREC+1}					=	'Used to store mosaic parameters.';
LabelsTypeDatagram{XTF_HEADER_KLEIN_RAW_BATHY+1}		=	'Bathymetry data from the Klein 5000.';
LabelsTypeDatagram{XTF_HEADER_HIGHSPEED_SENSOR2+1}		=	'High speed sensor from Klein 5000.';
LabelsTypeDatagram{XTF_HEADER_ELAC_XSE+1}				=	'Elac dual-head.';
LabelsTypeDatagram{XTF_HEADER_BATHY_XYZA+1}				=	'';
LabelsTypeDatagram{XTF_HEADER_K5000_BATHY_IQ+1}			=	'Raw IQ data from Klein 5000 server';
LabelsTypeDatagram{XTF_HEADER_BATHY_SNIPPET+1}			=	'81xx snippet Packet';
LabelsTypeDatagram{XTF_HEADER_GPS+1}                    =	'GPS Position.';
LabelsTypeDatagram{XTF_HEADER_STAT+1}					=	'GPS statistics.';
LabelsTypeDatagram{XTF_HEADER_SINGLEBEAM+1}				=	'';
LabelsTypeDatagram{XTF_HEADER_GYRO+1}					=	'Heading/Speed Sensor.';
LabelsTypeDatagram{XTF_HEADER_TRACKPOINT+1}				=	'';
LabelsTypeDatagram{XTF_HEADER_MULTIBEAM+1}				=	'';
LabelsTypeDatagram{XTF_HEADER_Q_SINGLEBEAM+1}			=	'';
LabelsTypeDatagram{XTF_HEADER_Q_MULTITX+1}				=	'';
LabelsTypeDatagram{XTF_HEADER_Q_MULTIBEAM+1}			=	'';
LabelsTypeDatagram{XTF_HEADER_TIME+1}					=	'';
LabelsTypeDatagram{XTF_HEADER_BENTHOS_CAATI_SARA+1}		=	'Custom Benthos data.';
LabelsTypeDatagram{XTF_HEADER_7125+1}					=	'7125 Bathy Data';
LabelsTypeDatagram{XTF_HEADER_7125_SNIPPET+1}			=	'7125 Bathy Data Snippets';
LabelsTypeDatagram{XTF_HEADER_QINSY_R2SONIC_BATHY+1}	=	'Quincy R2Sonic bathymetry data';
LabelsTypeDatagram{XTF_HEADER_QINSY_R2SONIC_FTS+1}		=	'Quincy R2Sonics Foot Print Time Series (snippets)';
LabelsTypeDatagram{XTF_HEADER_R2SONIC_BATHY+1}			=	'Triton R2Sonic bathymetry data';
LabelsTypeDatagram{XTF_HEADER_R2SONIC_FTS+1}			=	'Triton R2Sonic Footprint Time Series';
LabelsTypeDatagram{XTF_HEADER_CODA_ECHOSCOPE_DATA+1}	=	'Custom CODA Echoscope Data';
LabelsTypeDatagram{XTF_HEADER_CODA_ECHOSCOPE_CONFIG+1}	=	'Custom CODA Echoscope Data';
LabelsTypeDatagram{XTF_HEADER_CODA_ECHOSCOPE_IMAGE+1}	=	'Custom CODA Echoscope Data';
LabelsTypeDatagram{XTF_HEADER_EDGETECH_4600+1}			=	'';
LabelsTypeDatagram{XTF_HEADER_RESON_7027_BATHY+1}		=	'(Type 7027 RAW Detection Data � see Reson document 7k Data Format, Vol 1, Ver 2.00 para 10.38)';
LabelsTypeDatagram{XTF_HEADER_POSITION+1}				=	'; Raw position packet - Reserved for use by Reson, Inc. RESON ONLY.';
LabelsTypeDatagram{XTF_HEADER_BATHY_PROC+1}				=	'';
LabelsTypeDatagram{XTF_HEADER_ATTITUDE_PROC+1}			=	'';
LabelsTypeDatagram{XTF_HEADER_SINGLEBEAM_PROC+1}		=	'';
LabelsTypeDatagram{XTF_HEADER_AUX_PROC+1}				=	'Aux Channel + AuxAltitude + Magnetometer.';
LabelsTypeDatagram{XTF_HEADER_KLEIN3000_DATA_PAGE+1}	=	'';
LabelsTypeDatagram{XTF_HEADER_POS_RAW_NAVIGATION+1}		=	'';
LabelsTypeDatagram{XTF_HEADER_KLEINV4_DATA_PAGE+1}		=	'';
LabelsTypeDatagram{XTF_HEADER_USERDEFINED+1}			=	'';

if ~isempty(varargin)
    code = varargin{1};
    tabHistoTypeDatagram = varargin{2};
else
    code = [];
    tabHistoTypeDatagram = zeros(size(LabelsTypeDatagram));
end

texteTypeDatagram = '';
for i=1:length(LabelsTypeDatagram)
    if ~isempty(LabelsTypeDatagram{i})
        sub = find(i == code+1);
        if isempty(sub)
            if fullListe
                texteTypeDatagram = sprintf('%s\n%d = %s (0)', ...
                    texteTypeDatagram, i, LabelsTypeDatagram{i});
            end
        else
            if fullListe
                texteTypeDatagram = sprintf('%s\n%d = %s (%d)', ...
                    texteTypeDatagram, code(sub), LabelsTypeDatagram{i}, tabHistoTypeDatagram(sub));
            else
                if tabHistoTypeDatagram(sub) ~= 0
                    texteTypeDatagram = sprintf('%s\n%d = %s (%d)', ...
                        texteTypeDatagram, code(sub), LabelsTypeDatagram{i}, tabHistoTypeDatagram(sub));
                end
            end
        end
    end
end
