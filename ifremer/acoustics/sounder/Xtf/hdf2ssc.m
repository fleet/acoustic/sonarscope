function [flag, sHDF] = hdf2ssc(nomFicHDF, varargin)

%% Exploitation du fichier HDF5
info = h5info(nomFicHDF);
% % % h5disp(nomFicHDF)

% TODO : r�gler le subX et le subY pour des extractions par voie.
subX = getVararginValue(varargin, 'SubX', []);
subY = getVararginValue(varargin, 'SubY', []);

clear sHDF
for k=1:numel(info.Attributes)
    sHDF.(info.Attributes(k).Name) = h5readatt(nomFicHDF,'/',info.Attributes(k).Name);
end

%% Lecture des container (groupes) de type et de niveau 0: Sonar, Bathy, Attitude
listGroupTag   = {'Sonar', 'Attitude', 'Navigation', 'Environment'};
listGeoTypeTag = {'PingSamples', 'PingBeams', 'Snippets', 'Geo'};

rootContainer       = info;
% Identification des container pr�sents
idxContainerGrp = identifyContainerFromRoot(rootContainer, listGroupTag);
if isempty(idxContainerGrp)
    % idSystem    = info.Groups(idxGroup(k)).Groups(k).Name;
    rootContainer       = info.Groups(1);
    idxContainerGrp     = identifyContainerFromRoot(rootContainer, listGeoTypeTag);
end


% Nb de groupes de type Sonar, Attitude, Bathy (a priori un syst�matiquement).
for g=1:numel(idxContainerGrp)
    
    rootGroup = info.Groups(g);
    if numel(rootGroup.Groups)
        
        % Identification des g�om�tries pr�sentes
        idxGeoGrp = identifyGeometryFromSonar(rootGroup, listGeoTypeTag);
        if isempty(idxGeoGrp)
%             idSystem    = info.Groups(g).Groups(1).Name;
            rootGroup   = info.Groups(g).Groups(1);
            idxGeoGrp   = identifyGeometryFromSonar(rootGroup, listGeoTypeTag);
        end
        
        %% Chargement des diff�rentes g�om�tries

        for o=1:numel(idxGeoGrp)
            geoTypeTag = listGeoTypeTag{idxGeoGrp(o)};
            
            geometryName         = rootGroup.Groups(o).Name;
            %% Chargement des diff�rentes voies de la g�om�trie : ex, 100Hz, Subbottom2
            for v=1:numel(rootGroup.Groups(o).Groups)
                
                %% Lecture des groupes de niveau 1 (ex : G�om�trie des donn�es Sonar)
                subChannelGroup     = rootGroup.Groups(o).Groups(v);
                subChannelGroupName = rootGroup.Groups(o).Groups(v).Name;
                
                % On ne conserve que le suffixe du nom (dans le nom du group et les '/').
                channelName = regexprep(subChannelGroupName, geometryName, '');
                channelName = regexprep(channelName, '/', '');
                channelName = regexprep(channelName, '-|_', '');
                channelName = ['channel_' num2str(channelName)];
                
                % Lecture des attributs du groupe de niveau 1.
                for kk=1:numel(subChannelGroup.Attributes)
                    attributeName     = subChannelGroup.Attributes(kk).Name;
                    
                    sHDF.(listGroupTag{idxContainerGrp(g)}).(geoTypeTag).(channelName).attribut.(attributeName).value = h5readatt(nomFicHDF,subChannelGroupName,attributeName);
                    % TODO : lecture et application des attributs de chaque variable.
                end
                
                % Lecture des datasets du groupe de niveau 1.
                for kk=1:numel(subChannelGroup.Datasets)
                    dataSetName     = subChannelGroup.Datasets(kk).Name;
                    
                    sHDF.(listGroupTag{idxContainerGrp(g)}).(geoTypeTag).(channelName).dataset.(dataSetName).value = h5read(nomFicHDF,[subChannelGroupName '/' dataSetName]);
                    % TODO : lecture et application des attributs de chaque variable.
                end
                
                %% Lecture des groupes de niveau 2.
                % Etude des sous-groupes �ventuels de la voie �tudi�e : ex, 100 Hz pour du Sonar PingSamples
                nbGrpCh = numel(subChannelGroup.Groups);
                
                for cc=1:nbGrpCh
                    subChannelName = subChannelGroup.Groups(cc).Name;
                    % On ne conserve que le suffixe du nom (dans le nom du group et les '/').
                    subChannelName = regexprep(subChannelName, subChannelGroupName, '');
                    subChannelName = regexprep(subChannelName, '/', '');
                    subChannelName = regexprep(subChannelName, '-|_', '');
                    subChannelName = ['channel_' num2str(subChannelName)];
                    
                    % Attachement du nom de la voie comme attribut.
                    sHDF.(listGroupTag{idxContainerGrp(g)}).(geoTypeTag).(channelName).attribut.Name.value = channelName;
                    
                    % Lecture des attributs du groupe de niveau 2.
                    subGroupSubChName = subChannelGroup.Groups(cc).Name;
                    
                    for aa=1:numel(subChannelGroup.Groups(cc).Attributes)
                        attributeName = subChannelGroup.Groups(cc).Attributes(aa).Name;
                        
                        sHDF.(listGroupTag{idxContainerGrp(g)}).(geoTypeTag).(channelName).(subChannelName).attribut.(attributeName).value = h5readatt(nomFicHDF,subGroupSubChName,attributeName);
                    end
                    
                    % Lecture des datasets du groupe de niveau 2.
                    for dd=1:numel(subChannelGroup.Groups(cc).Datasets)
                        dataSetName = subChannelGroup.Groups(cc).Datasets(dd).Name;
                        
                        if strcmp(dataSetName, 'Data')
                            % Lecture des infos sp�cifiques � la matrice de type Sonar Lat�ral.
                            infoData = h5info(nomFicHDF,[subGroupSubChName '/' dataSetName]);
                            if isempty(subX)
                                subX = 1:infoData.Dataspace.Size(1);
                            else
                                subX = subX(1):min(subX(end), infoData.Dataspace.Size(1));
                            end
                            if isempty(subY)
                                subY = 1:infoData.Dataspace.Size(2);
                            else
                                subY = subY(1):min(subY(end), infoData.Dataspace.Size(2));
                            end
                            % Extraction de tout ou partie de la donn�e.
                            sHDF.(listGroupTag{idxContainerGrp(g)}).(geoTypeTag).(channelName).(subChannelName).dataset.(dataSetName).value = ...
                                h5read(nomFicHDF,[subGroupSubChName '/' dataSetName], [subX(1) subY(1)], [numel(subX) numel(subY)]);
                        else
                            % Extraction de tout ou partie de la donn�e.
                            sHDF.(listGroupTag{idxContainerGrp(g)}).(geoTypeTag).(channelName).(subChannelName).dataset.(dataSetName).value = ...
                                h5read(nomFicHDF,[subGroupSubChName '/' dataSetName]);
                        end
                        % TODO : lecture des attributs de chaque variable.
                    end
                    
                end
            end
        end
    else
        % Chargement des donn�es d'Attitude, de Bathym�trie, ...
        subGroupName    = rootGroup.Name;
        
        % Lecture des donn�es du groupe de type 'xxxxxxx'.
        for kk=1:numel(rootGroup.Attributes)
            attributeName       = rootGroup.Attributes(kk).Name;
            
            sHDF.(listGroupTag{idxContainerGrp(g)}).attribut.(attributeName).value = h5readatt(nomFicHDF,subGroupName,attributeName);
        end
        
        % Lecture des donn�es du groupe de type 'xxxxxxx'.
        for kk=1:numel(rootGroup.Datasets)
            dataSetName     = rootGroup.Datasets(kk).Name;
            sHDF.(listGroupTag{idxContainerGrp(g)}).dataset.(dataSetName).value = h5read(nomFicHDF,[subGroupName '/' dataSetName]);
            % TODO : lecture des attributs de chaque variable.
        end
    end
end
flag = 1;


function idxContainerGrp = identifyContainerFromRoot(rootGroup, listGroupTag)

idxContainerGrp = [];
nbContainerType = arrayfun(@(x) regexp(x.Name, listGroupTag), rootGroup.Groups(:), 'UniformOutput', false);
for i=1:numel(nbContainerType)
    if any(~cellfun(@isempty, nbContainerType{i}))
        idxContainerGrp(i)   = find(cellfun(@isempty, nbContainerType{i}) == 0); %#ok<AGROW>
    end
end


function idxGeoGrp = identifyGeometryFromSonar(rootGroup, listGeoTypeTag)

idxGeoGrp = [];
nbGeoType   = arrayfun(@(x) regexp(x.Name, listGeoTypeTag), rootGroup.Groups(:), 'UniformOutput', false);
for i=1:numel(nbGeoType)
    if any(~cellfun(@isempty, nbGeoType{i}))
        idxGeoGrp(i)   = find(cellfun(@isempty, nbGeoType{i}) == 0); %#ok<AGROW>
    end
end
