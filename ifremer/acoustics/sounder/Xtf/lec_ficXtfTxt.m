
% nomFic = 'C:\TEMP\NavSonarReb14_RDB.txt';
% lec_ficXtfTxt(nomFic)

function lec_ficXtfTxt(nomFic)

Lat = NaN(1,10000);
Lon = NaN(1,10000);

n = 0;
fid=fopen(nomFic);
while 1
    tline = fgetl(fid);
    if ~ischar(tline)
        break
    end
    n = n + 1;
    
%     disp([tline(113:115) '   ' tline(118:123) '   ' tline(126) '   ' tline(96:98) '   ' tline(101:106) '   ' tline(109)])
    
    LonDeg = str2double(tline(113:115));
    LonMinute = str2double(tline(118:123));
    Lon(n) = LonDeg + LonMinute/60;
    if strcmp(tline(126), 'W')
        Lon(n) = -Lon(n);
    end
    
    LatDeg = str2double(tline(96:98)); 
    LatMinute = str2double(tline(101:106));
    Lat(n) = LatDeg + LatMinute/60;
    if strcmp(tline(109), 'S')
        Lat(n) = -Lat(n);
    end
    
end
fclose(fid);
Lat = Lat(1:n);
Lon = Lon(1:n);

figure; plot(Lat) ; grid on;
figure; plot(Lon) ; grid on;
figure; plot(Lon,Lat) ; grid on;

sub = (Lat < 45);
Lon(sub) = [];
Lat(sub) = [];
figure; plot(Lon,Lat) ; grid on;

figure; plot(60*(Lon-floor(Lon(1))),60*(Lat-floor(Lat(1)))) ; grid on;
