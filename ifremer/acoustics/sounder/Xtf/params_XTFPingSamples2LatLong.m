function [flag, resol, Backup, AcrossInterpolation, LimitAngles, window, nomFicErMapperLayer, ...
    nomFicErMapperAngle, TitreDepth, CompleteExistingMosaic, bilan] = params_XTFPingSamples2LatLong(liste, varargin)

Backup                 = [];
AcrossInterpolation    = [];
LimitAngles            = [];
window                 = [];
nomFicErMapperLayer    = [];
nomFicErMapperAngle    = [];
CompleteExistingMosaic = [];
bilan                  = [];
TitreDepth             = [];

E0 = cl_ermapper([]);

if ischar(liste)
    liste = {liste};
end
nbProfils = length(liste);
nomDir = fileparts(liste{1});

%% Saisie de la r�solution

[flag, resol] = get_gridSize('Initial', 0.15);
if ~flag
    return
end

%% Backup frequency

[flag, Backup] = saisie_backupFrequency(nbProfils);
if ~flag
    return
end

%% Across distance interpolation method

[flag, AcrossInterpolation] = question_AcrossDistanceInterpolation('QL', 3);
if ~flag
    return
end

%% Limitation angulaire de la mosa�que

[flag, LimitAngles] = question_AngularLimits('QL', 2);
if ~flag
    return
end

%% Mosaic interpolation window

[flag, window] = question_MosaicInterpolationWindow([], 'InitValue', [3 3]);
if ~flag
    return
end

%% Noms du fichier de sauvegarde de la mosa�que de r�flectivit�

DataType = cl_image.indDataType('Reflectivity');
[nomFicErMapperLayer, flag] = initNomFicErs(E0, 'Mosaic', cl_image.indGeometryType('LatLong'), DataType, nomDir, 'resol', resol);
if ~flag
    return
end
[nomDir, TitreDepth] = fileparts(nomFicErMapperLayer);

%% Noms du fichier de sauvegarde de la mosa�que des angles d'�mission

DataType = cl_image.indDataType('TxAngle');
[nomFicErMapperAngle, flag] = initNomFicErs(E0, TitreDepth, cl_image.indGeometryType('LatLong'), DataType, nomDir, 'confirm', (get_LevelQuestion >= 3));
if ~flag
    return
end
[~, TitreAngle] = fileparts(nomFicErMapperAngle);%#ok

%% R�cup�ration des mosa�ques existantes

if exist(nomFicErMapperLayer, 'file') && exist(nomFicErMapperAngle, 'file')
    [flag, rep] = question_CompleteExistingMosaic;
    if ~flag
        return
    end
    CompleteExistingMosaic = (rep == 1);
end

%% Si Mosa�que de R�flectivit�, on propose de faire une compensation statistique ou une calibration

[flag, ~, ~, bilan] = params_SonarCompensation(nomDir);
if ~flag
    return
end
