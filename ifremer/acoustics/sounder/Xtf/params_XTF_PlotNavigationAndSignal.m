function [flag, nomSignal] = params_XTF_PlotNavigationAndSignal(listFileNames)

nomSignal = [];

%% Choix du signal

str1 = 'Type de liste des signaux.';
str2 = 'Type of signals list.';
[rep, flag] = my_questdlg(Lang(str1,str2), Lang('Signaux usuels', 'Usual signals'), Lang('Liste compl�te', 'Full list'));
if ~flag
    return
end
if rep == 1
    listeSignaux{1}     = 'Time';
    listeSignaux{end+1} = 'Immersion';
    listeSignaux{end+1} = 'Height';
    listeSignaux{end+1} = 'Layback'; % CableOut
    listeSignaux{end+1} = 'Speed';
    listeSignaux{end+1} = 'Heading';
    listeSignaux{end+1} = 'Roll';
    listeSignaux{end+1} = 'Pitch';
    listeSignaux{end+1} = 'SoundVelocity'; % SurfaceSoundSpeed
%     listeSignaux{end+1} = 'Mode_1';
%     listeSignaux{end+1} = 'Mode_2';
    listeSignaux{end+1} = 'Heave';
    listeSignaux{end+1} = 'Tide';
    listeSignaux{end+1} = 'PingCounter';
    listeSignaux{end+1} = 'DistPings';
%     listeSignaux{end+1} = 'SampleFrequency'; % TODO : comment r�cup�rer
%     �a ?
%     listeSignaux{end+1} = 'Temperature';
else
    
    %% R�cup�ration de la liste des signaux
    
    a = cl_xtf('nomFic', listFileNames{1});
    if isempty(a)
        return
    end
    
    [flag, SideScan] = SSc_ReadDatagrams(listFileNames{1}, 'Ssc_SideScan', 'Memmapfile', -1);
    if ~flag
        return
    end
    
    listeSignaux = {SideScan.Signals.Name};
end

%% Choix du signal

str1 = 'S�lectionnez le signal � repr�senter';
str2 = 'Select a signal to plot';
[rep, flag] = my_listdlg(Lang(str1,str2), listeSignaux, 'SelectionMode', 'Single');
if ~flag
    return
end
nomSignal = listeSignaux{rep};
