function [flag, skipAlreadyProcessedFiles] = params_XTF_process_Bathy(listFileNames)

skipAlreadyProcessedFiles = [];

a = cl_xtf('nomFic', listFileNames{1});
if isempty(a)
    flag = 0;
    return
end

[flag, FileHeader] = SSc_ReadDatagrams(listFileNames{1}, 'Ssc_FileHeader', 'XMLOnly', 1);
if ~flag
    return
end

flag = (FileHeader.NumberOfBathymetryChannels ~= 0);
if ~flag
    str1 = sprintf('Le fichier "%s" ne comporte pas de datagrammes de bathym�trie.', listFileNames{1});
    str2 = sprintf('File "%s" do not contain any Bathymetry data.', listFileNames{1});
    my_warndlg(Lang(str1,str2), 1);
end

%% Skip existingResults

str1 = 'Voulez-vous ignorer les fichiers d�j� trait�s ?';
str2 = 'Do you want to skip the already processed files ?';
[rep, flag] = my_questdlg(Lang(str1,str2));
if ~flag
    return
end
skipAlreadyProcessedFiles = (rep == 1);
