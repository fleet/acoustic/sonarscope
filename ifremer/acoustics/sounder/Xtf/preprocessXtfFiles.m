% Pretraitement des fichiers .xtf.
% Cette operation n'est pas necessaire mais on gagnera du temps lors des
% premieres lectures de fichier
%
% fig = preprocessXtfFiles(...)
%
% Name-Value Pair Arguments
%   nom : Nom d'un repertoire contenant des .xtf ou nom d'un fichier .xtf
%   ou tableau de cellules contenant des noms de fichiers .xtf
%
% Name-Value Pair Arguments
%   Option        : {'All of them'} | 'Index' | 'Position' | 'Runtime' | 'Attitude'
%                   | 'Clock' | 'SoundSpeedProfile' | 'SurfaceSoundSpeed' | 'Height'
%
% Output Arguments
%   fig : Numero de la figure de la navigation si navigation demandee
%
% Examples
%   preprocessXtfFiles
%
%   nomDir = '/home2/rayleigh/calimero/EM300'
%   liste = listeFicOnDir2(nomDir, '.xtf')
%   preprocessXtfFiles(liste{1})
%   preprocessXtfFiles(liste{1:3})
%   preprocessXtfFiles(liste)
%
%   preprocessXtfFiles(nomDir)
%
% See also cl_xtf Authors
% Authors : JMA
%------------------------------------------------------------------------------------------

function Fig1 = preprocessXtfFiles(varargin)

[varargin, Option] = getPropertyValue(varargin, 'Option', 'Allofthem');

if isempty(varargin)
    [flag, listeNomFic] = my_uigetfile('*.xtf', 'MultiSelect','on');
    if ~flag
        return
    end
    if ~iscell(listeNomFic)
        listeNomFic = {listeNomFic};
    end
else
    if iscell(varargin{1})
        listeNomFic = varargin{:};
    else
        if isfolder(varargin{1})
            listeNomFic = listeFicOnDir2(varargin{1}, '.xtf');
        else
            listeNomFic = {varargin{:}};
        end
    end
end

NbFic = length(listeNomFic);
for k=1:NbFic
    b = cl_xtf('nomFic', listeNomFic{k});
    if isempty(b)
        continue
    end
    disp(char(b))
    a(k) = b; %#ok
end

if any(strcmp(Option, 'All of them')) || any(strcmp(Option, 'Index'))
    % Visualisation graphique des types de datagrammes
    plot_index(a)
    
    % Histogramme des types de datagrammes
    histo_index(a)
end

if any(strcmp(Option, 'All of them')) || any(strcmp(Option, 'Sidescan signals'))
    % Affichage des donn�es contenues dans les datagrammes "Sidescan"
    plot_Sidescan_Signals(a);
end

if any(strcmp(Option, 'All of them')) || any(strcmp(Option, 'Position'))
    % Affichage des donn�es contenues dans les datagrammes "position"
    X0 = cl_xtf([]);
    Fig1 = XTF_PlotNavigation(X0, get(a, 'nomFic'));
end

if any(strcmp(Option, 'All of them')) || any(strcmp(Option, 'Attitude'))
    % Affichage des donn�es contenues dans les datagrammes "attitude"
    plot_attitude(a)
end
