function flag = visuBathy
% for i=1:NbBathy
%     for k=1:size(XTFBathyHeader(i).Data, 2)
%         pppp(i, k) = XTFBathyHeader(i).Data(k);
%     end
% end
flag = 1;

for i=1:NbPing
    for j=1:XTFPingHeader(i).NumChansToFollow
        for k=1:size(XTFPingHeader(i).Channel(j).Data, 2)
            pppp(i, k) = XTFPingHeader(i).Channel(j).Data(k);
        end
    end
end

