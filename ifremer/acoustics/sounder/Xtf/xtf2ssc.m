%   nomFicOLD = '???\Sonar\demoplane.xtf';
%   aOLD = cl_xtf('nomFic', nomFicOLD);
%
%   nomFicNEW = '???\Sonar\demoplane.xtf';
%   Display = 0;
%   flag = xtf2ssc(nomFicNEW, 'Display', Display)
%
%   nomFicNEW = '???\Sonar\CANA008.xtf';
%   Display = 0;
%   flag = xtf2ssc(nomFicNEW, 'Display', Display)
%
%   aNEW = cl_xtf('nomFic', nomFicNEW);
%
%   DataOLD = read_imagery(aOLD)
%   [flag, DataOLDBin] = read_imagery_bin(aOLD)
%   [flag, DataNEWBin] = read_imagery_bin(aNEW)
%---------------------------------------------------------------------

function flag = xtf2ssc(nomFic, varargin)

global IfrTbxResources %#ok<GVMIS>
global IfrTbx %#ok<GVMIS>

% Barre de progression uniquement visible en version de Dev car semble �tre
% � l'origine de probl�me lors du traitement successif de donn�es
% (27/09/2012).
%{
if isdeployed
    progressBar = 1;
else
    progressBar = 1;
end
%}
progressBar = 0; % test JMA le 05/02/2019

[varargin, Display] = getPropertyValue(varargin, 'Display', 0); %#ok<ASGLU>

if isdeployed
    nomDir = fileparts(IfrTbxResources);
    nomExe = fullfile(nomDir, 'extern', 'C', 'CONVERT_XTF.exe');
else
    nomExe = [IfrTbx '\ifremer\extern\C\CONVERT_exe\Release\CONVERT_XTF.exe'];
end

%% G�n�ration des XML et des binaires

[nomDir, nomFicSeul] = fileparts(nomFic);
nomDirRacine = fullfile(nomDir, 'SonarScope', nomFicSeul);
cmd = sprintf('!"%s" "%d" "%s"', nomExe, progressBar, nomFic);

flag = [];
flag(end+1) = exist(nomDirRacine, 'dir');
flag(end+1) = exist(fullfile(nomDirRacine, 'XTF_FileIndex.xml'), 'file');
flag(end+1) = exist(fullfile(nomDirRacine, 'XTF_FileIndex'), 'dir');

% Si chanInfo 0 existe, l'extraction des signaux a �t� effectu�.
flag(end+1) = exist(fullfile(nomDirRacine, 'XTF_ChanInfo_0.xml'), 'file');
flag(end+1) = exist(fullfile(nomDirRacine, 'Ssc_FileHeader.xml'), 'file');
% flag(end+1) = exist(fullfile(nomDirRacine, 'Ssc_SideScan'), 'dir');

% Pas de filtrage sur la date pour l'instant.
% if all(flag) && checkFileCreation(nomDirRacine, 2009, 11, 01, 00, 30, 0)
if all(flag)
    flag = 1;
    return
end

nomFicTmp = [nomDirRacine '_tmp'];
if exist(nomFicTmp, 'dir')
    message_erreur_convert(nomFicTmp, nomFic, nomDirRacine)
    flag = 0;
    return
end

fprintf(1,'%s\n', cmd);
str1 = sprintf('"%s" est en cours de lecture.', nomFic);
str2 = sprintf('"%s" is being read.', nomFic);
WorkInProgress(Lang(str1,str2))

[~, msg] = dos(cmd(2:end));
if ~exist(nomDirRacine, 'dir')
    if isempty(msg)
        str1 = sprintf('CONVERT_XTF.exe semble avoir rencontr� un probl�me sur le fichier %s', nomFic);
        str2 = sprintf('CONVERT_XTF.exe does not seem working for file %s', nomFic);
        msg = Lang(str1,str2);
    end
    my_warndlg(msg, 1);
    flag = 0;
    return
end

%% Datagrammes FileHeader

nomFicXml = fullfile(nomDirRacine, 'XTF_FileHeader.xml');
if exist(nomFicXml, 'file')
    [flag, DataFileHeader] = xtf2ssc_FileHeader(nomDirRacine, 'Display', Display);
    if ~flag
        return
    end
end

%% Datagrammes NotesHeader

nomFicXml = fullfile(nomDirRacine, 'XTF_NotesHeader.xml');
if exist(nomFicXml, 'file')
    [flag, Data.NotesHeader] = xtf2ssc_NotesHeader(nomDirRacine, 'Display', Display);
    if ~flag
        return
    end
end

%% Datagrammes SNP0Header

nomFicXml = fullfile(nomDirRacine, 'XTF_SNP0Header.xml');
if exist(nomFicXml, 'file')
    [flag, Data.SNP0Header] = xtf2ssc_SNP0Header(nomDirRacine, 'Display', Display);
    if ~flag
        return
    end
end

%% Datagrammes Header 7006 (DRF, RTH, et RD)

nomFicXml = fullfile(nomDirRacine, 'XTF_7125Bathy_DRF.xml');
if exist(nomFicXml, 'file')
    [flag, Data.DRF] = xtf2ssc_DRF(nomDirRacine);
    if ~flag
        return
    end
end
nomFicXml = fullfile(nomDirRacine, 'XTF_7125Bathy_7006.xml');
if exist(nomFicXml, 'file')
    sub = (Data.DRF.RecordTypeIdentifier == 7006);
    ODBytesOffset = Data.DRF.OptionalDataOffset(sub);
    [flag, Data.S7K7006] = xtf2ssc_7006(nomDirRacine, ODBytesOffset, 'Display', Display);
    if ~flag
        return
    end
end

%% Datagrammes SNP1Header

nomFicXml = fullfile(nomDirRacine, 'XTF_SNP1Header.xml');
if exist(nomFicXml, 'file')
    [flag, Data.SNP0Header] = xtf2ssc_SNP0Header(nomDirRacine, 'Display', Display);
    if ~flag
        return
    end
end

%% Datagrammes RawSerialHeader

nomFicXml = fullfile(nomDirRacine, 'XTF_RawSerialHeader.xml');
if exist(nomFicXml, 'file')
    [flag, Data.RawSerialHeader] = xtf2ssc_RawSerialHeader(nomDirRacine, 'Display', Display); %#ok<STRNU>
    if ~flag
        return
    end
end

%% Lecture des infos sur les channels.

for iNumChan=1:DataFileHeader.NumberOfSonarChannels
    flag = xtf2ssc_ChanInfo(nomDirRacine, iNumChan-1, 'Display', Display);
    if ~flag
        return
    end
end
for iNumChan=1:DataFileHeader.NumberOfBathymetryChannels
    flag = xtf2ssc_ChanInfo(nomDirRacine, iNumChan-1, 'Display', Display);
    if ~flag
        return
    end
end

%% Datagrammes SideScan

nomFicXml = fullfile(nomDirRacine, 'XTF_SideScan.xml');
if exist(nomFicXml, 'file')
    [flag, DataSideScan] = xtf2ssc_DataHeader(nomDirRacine, 'SideScan', 1, 'Display', Display); %#ok<ASGLU> % DataFileHeader.NumberOfSonarChannels ?
    if ~flag
        return
    end
    
    %% Datagrammes SideScan
    
    % nomDirRacine = fullfile(nomDirRacine, 'XTF_SideScan');
    for iNumChan=1:DataFileHeader.NumberOfSonarChannels
        flag = xtf2ssc_PingChanHeader(fullfile(nomDirRacine, 'XTF_SideScan'), iNumChan-1, 'Display', Display);
        if ~flag
            return
        end
    end
end

%% Datagrammes Bathy

nomFicXml = fullfile(nomDirRacine, 'XTF_Bathy.xml');
if exist(nomFicXml, 'file')
    
%{
% TODO : ici il faudrait faire un XML-Bin qui est fait dans la fonction "create_XmlBin_Bathymetry"
%        en la compl�tant avec tous les signaux pr�sents dans read_Bathy_bin
%}
    
    % Description des config d'acquisition de la bathy.
    [flag, DataBathyHeader] = xtf2ssc_DataHeader(nomDirRacine, 'Bathy', ...
        DataFileHeader.NumberOfBathymetryChannels, 'Display', Display);
    if ~flag
        return
    end
    
    
    nomDirRacine = fullfile(nomDirRacine, 'XTF_Bathy');
    for iNumChan=1:DataFileHeader.NumberOfBathymetryChannels
        % Calcul du nombre de faisceaux de chaque voie.
        nbBeamsPerChannels = unique(DataBathyHeader.NumChanToFollow(:,iNumChan));
        % Datagrammes Bathy de Quinsy Multibeam
        nomFicXml = fullfile(nomDirRacine, ['XTF_QPSMultiBeamHeader_' num2str(iNumChan-1) '.xml']);
        if exist(nomFicXml, 'file')
            [flag, DataMultiBeamHeader] = xtf2ssc_QPSMultiBeamHeader(nomDirRacine,  iNumChan-1, nbBeamsPerChannels, 'Display', Display); %#ok<ASGLU>
            if ~flag
                return
            end
        end
        % %         N = sum(DataBathyHeader.NumChanToFollow);
        % %         flag = xtf2ssc_DataBathyHeader(nomDirRacine, iNumChan-1, DataBathyHeader.NumChanToFollow, 'Display', Display, 'N', N);
        % %         if ~flag
        % %             return
        % %         end
    end
    
    
end

%{
a = cl_xtf('nomFic', nomFic);
[flag, DataIndex] = read_FileIndex_bin(a);
[flag, Position] = read_position_bin(a); % Vide
[flag, Attitude] = read_attitude_bin(a); % Vide
%}

% ----------------------------------
% Datagrammes FileIndex
% flag = xtf2ssc_FileIndex(nomDirRacine, 'Display', Display);
% if ~flag
%     return
% end

flag = 1;
