function [flag, Data] = xtf2ssc_7006(nomDirRacine, ODBytesOffset, varargin)

[varargin, flagDisplay] = getPropertyValue(varargin, 'Display', 0); %#ok<ASGLU>

%% Lecture du fichier XML d�crivant la donn�e

nomFicXml = fullfile(nomDirRacine, 'XTF_7125Bathy_7006.xml');
flag = exist(nomFicXml, 'file');
if ~flag
    special_message('FichierNonExistant', nomFicXml)
    return
end
% edit(nomFicXml)
Datagrams = xml_read(nomFicXml);

[nomDirSignal, nomFicSignal] = fileparts(nomFicXml);
nomDirSignal = fullfile(nomDirSignal, nomFicSignal);

%% Lecture de SonarId

[flag, Data.SonarId] = Read_BinVariable(Datagrams, nomDirSignal, 'SonarId', 'uint64');
if ~flag
    return
end

%% Lecture de PingNumber

[flag, Data.PingNumber] = Read_BinVariable(Datagrams, nomDirSignal, 'PingNumber', 'uint32');
if ~flag
    return
end

%% Lecture de MultiPingSequence

[flag, Data.MultiPingSequence] = Read_BinVariable(Datagrams, nomDirSignal, 'MultiPingSequence', 'uint16');
if ~flag
    return
end

%% Lecture de N

[flag, Data.N] = Read_BinVariable(Datagrams, nomDirSignal, 'N', 'uint32');
if ~flag
    return
end
N = sum(Data.N);

%% Lecture de LayerCompensationFlag

[flag, Data.LayerCompensationFlag] = Read_BinVariable(Datagrams, nomDirSignal, 'LayerCompensationFlag', 'uint8');
if ~flag
    return
end

%% Lecture de SoundVelocityFlag

[flag, Data.SoundVelocityFlag] = Read_BinVariable(Datagrams, nomDirSignal, 'SoundVelocityFlag', 'uint8');
if ~flag
    return
end

%% Lecture de SoundVelocity

[flag, Data.SoundVelocity] = Read_BinVariable(Datagrams, nomDirSignal, 'SoundVelocity', 'single');
if ~flag
    return
end

%#############
%Paquet RD

%% Lecture de Range
[flag, Data.Range] = Read_BinTable(Datagrams, nomDirSignal, 'Range', 'single', N);
if ~flag
    return
end

%% Lecture de Quality

[flag, Data.Quality] = Read_BinTable(Datagrams, nomDirSignal, 'Quality', 'uint8', N);
if ~flag
    return
end

%% Lecture de IntensityOrBackscatter

[flag, Data.IntensityOrBackscatter] = Read_BinTable(Datagrams, nomDirSignal, 'IntensityOrBackscatter', 'single', N);
if ~flag
    return
end

%% Lecture de MinFilterInfo
NbBytesBeforeOD = 88 + Data.N*9;
if ODBytesOffset ~= NbBytesBeforeOD
    [flag, Data.MinFilterInfo] = Read_BinTable(Datagrams, nomDirSignal, 'MinFilterInfo', 'single', N);
    if ~flag
        return
    end
    
    %% Lecture de MaxFilterInfo
    
    [flag, Data.MaxFilterInfo] = Read_BinTable(Datagrams, nomDirSignal, 'MaxFilterInfo', 'single', N);
    if ~flag
        return
    end
end
%% Paquet OD
identTable = findIndVariable(Datagrams.Tables, 'Frequency');

if ~isempty(identTable)
    %% Lecture de Frequency
    [flag, Data.Frequency] = Read_BinTable(Datagrams, nomDirSignal, 'Frequency', 'single', Datagrams.NbDatagrams);
    if ~flag
        return
    end
    
    %% Lecture de Latitude
    
    [flag, Data.Latitude] = Read_BinTable(Datagrams, nomDirSignal, 'Latitude', 'double', Datagrams.NbDatagrams);
    if ~flag
        return
    end
    
    %% Lecture de Longitude
    
    [flag, Data.Longitude] = Read_BinTable(Datagrams, nomDirSignal, 'Longitude', 'double', Datagrams.NbDatagrams);
    if ~flag
        return
    end
    
    %% Lecture de Heading
    
    [flag, Data.Heading] = Read_BinTable(Datagrams, nomDirSignal, 'Heading', 'single', Datagrams.NbDatagrams);
    if ~flag
        return
    end
    
    %% Lecture de HeightSource
    
    [flag, Data.HeightSource] = Read_BinTable(Datagrams, nomDirSignal, 'HeightSource', 'uint8', Datagrams.NbDatagrams);
    if ~flag
        return
    end
    
    %% Lecture de Tide
    
    [flag, Data.Tide] = Read_BinTable(Datagrams, nomDirSignal, 'Tide', 'single', Datagrams.NbDatagrams);
    if ~flag
        return
    end
    
    %% Lecture de Roll
    
    [flag, Data.Roll] = Read_BinTable(Datagrams, nomDirSignal, 'Roll', 'single', Datagrams.NbDatagrams);
    if ~flag
        return
    end
    
    %% Lecture de Pitch
    
    [flag, Data.Pitch] = Read_BinTable(Datagrams, nomDirSignal, 'Pitch', 'single', Datagrams.NbDatagrams);
    if ~flag
        return
    end
    
    %% Lecture de Heave
    
    [flag, Data.Heave] = Read_BinTable(Datagrams, nomDirSignal, 'Heave', 'single', Datagrams.NbDatagrams);
    if ~flag
        return
    end
    
    %% Lecture de VehicleDepth
    
    [flag, Data.VehicleDepth] = Read_BinTable(Datagrams, nomDirSignal, 'VehicleDepth', 'single', Datagrams.NbDatagrams);
    if ~flag
        return
    end
    
    %% Lecture de BeamDepth
    
    [flag, Data.BeamDepth] = Read_BinTable(Datagrams, nomDirSignal, 'BeamDepth', 'single', N);
    if ~flag
        return
    end
    
    %% Lecture de BeamAlongTrackDistance
    
    [flag, Data.BeamAlongTrackDistance] = Read_BinTable(Datagrams, nomDirSignal, 'BeamAlongTrackDistance', 'single', N);
    if ~flag
        return
    end
    
    %% Lecture de BeamAcrossTrackDistance
    
    [flag, Data.BeamAcrossTrackDistance] = Read_BinTable(Datagrams, nomDirSignal, 'BeamAcrossTrackDistance', 'single', N);
    if ~flag
        return
    end
    
    %% Lecture de BeamPointingAngle
    
    [flag, Data.BeamPointingAngle] = Read_BinTable(Datagrams, nomDirSignal, 'BeamPointingAngle', 'single', N);
    if ~flag
        return
    end
    
    %% Lecture de BeamAzimuthAngle
    
    [flag, Data.BeamAzimuthAngle] = Read_BinTable(Datagrams, nomDirSignal, 'BeamAzimuthAngle', 'single', N);
    if ~flag
        return
    end
end

%% G�n�ration du XML SonarScope

Info.Title                  = Datagrams.Title; % Motion Reference Unit
Info.Constructor            = Datagrams.Constructor;
Info.Model                	= Datagrams.Model;
Info.TimeOrigin             = Datagrams.TimeOrigin;
Info.Comments               = Datagrams.Comments;

Info.Dimensions.NbRecords     = Datagrams.NbDatagrams;
Info.Dimensions.N             = N;

% Paquet RTH
Info.Signals(1).Name          = 'SonarId';
Info.Signals(end).Dimensions  = 'NbRecords,1';
Info.Signals(end).Storage     = 'uint64';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_7006','SonarId.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'PingNumber';
Info.Signals(end).Dimensions  = 'NbRecords,1';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_7006','PingNumber.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'MultiPingSequence';
Info.Signals(end).Dimensions  = 'NbRecords,1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_7006','MultiPingSequence.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'N';
Info.Signals(end).Dimensions  = 'NbRecords,1';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_7006','N.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'LayerCompensationFlag';
Info.Signals(end).Dimensions  = 'NbRecords,1';
Info.Signals(end).Storage     = 'uint8';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_7006','LayerCompensationFlag.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'SoundVelocityFlag';
Info.Signals(end).Dimensions  = 'NbRecords,1';
Info.Signals(end).Storage     = 'uint8';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_7006','SoundVelocityFlag.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'SoundVelocity';
Info.Signals(end).Dimensions  = 'NbRecords,1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'm/s';
Info.Signals(end).FileName    = fullfile('Ssc_7006','SoundVelocity.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

% Paquet RD
Info.Images(1).Name          = 'Range';
Info.Images(end).Dimensions  = 'NbSamples,N';
Info.Images(end).Storage     = 'single';
Info.Images(end).Unit        = 's';
Info.Images(end).FileName    = fullfile('Ssc_7006','Range.bin');
Info.Images(end).Tag         = verifKeyWord('TODO');

Info.Images(end+1).Name      = 'Quality';
Info.Images(end).Dimensions  = 'NbSamples,N';
Info.Images(end).Storage     = 'uint8';
Info.Images(end).Unit        = [];
Info.Images(end).FileName    = fullfile('Ssc_7006','Quality.bin');
Info.Images(end).Tag         = verifKeyWord('TODO');

Info.Images(end+1).Name      = 'IntensityOrBackscatter';
Info.Images(end).Dimensions  = 'NbSamples,N';
Info.Images(end).Storage     = 'single';
Info.Images(end).Unit        = 'dB re 1�Pa|dB/M2';
Info.Images(end).FileName    = fullfile('Ssc_7006','IntensityOrBackscatter.bin');
Info.Images(end).Tag         = verifKeyWord('TODO');

if ODBytesOffset ~= NbBytesBeforeOD
    Info.Images(end+1).Name      = 'MinFilterInfo';
    Info.Images(end).Dimensions  = 'NbSamples,N';
    Info.Images(end).Storage     = 'single';
    Info.Images(end).Unit        = [];
    Info.Images(end).FileName    = fullfile('Ssc_7006','MinFilterInfo.bin');
    Info.Images(end).Tag         = verifKeyWord('TODO');
    
    Info.Images(end+1).Name      = 'MaxFilterInfo';
    Info.Images(end).Dimensions  = 'NbSamples,N';
    Info.Images(end).Storage     = 'single';
    Info.Images(end).Unit        = [];
    Info.Images(end).FileName    = fullfile('Ssc_7006','MaxFilterInfo.bin');
    Info.Images(end).Tag         = verifKeyWord('TODO');
end

if ~isempty(identTable)
    % Paquet OD
    Info.Signals(end+1).Name      = 'Frequency';
    Info.Signals(end).Dimensions  = 'NbRecords,1';
    Info.Signals(end).Storage     = 'single';
    Info.Signals(end).Unit        = 'Hz';
    Info.Signals(end).FileName    = fullfile('Ssc_7006','Frequency.bin');
    Info.Signals(end).Tag         = verifKeyWord('TODO');
    
    Info.Signals(end+1).Name      = 'Latitude';
    Info.Signals(end).Dimensions  = 'NbRecords,1';
    Info.Signals(end).Storage     = 'double';
    Info.Signals(end).Unit        = 'rad';
    Info.Signals(end).FileName    = fullfile('Ssc_7006','Latitude.bin');
    Info.Signals(end).Tag         = verifKeyWord('TODO');
    
    Info.Signals(end+1).Name      = 'Longitude';
    Info.Signals(end).Dimensions  = 'NbRecords,1';
    Info.Signals(end).Storage     = 'double';
    Info.Signals(end).Unit        = 'rad';
    Info.Signals(end).FileName    = fullfile('Ssc_7006','Longitude.bin');
    Info.Signals(end).Tag         = verifKeyWord('TODO');
    
    Info.Signals(end+1).Name      = 'Heading';
    Info.Signals(end).Dimensions  = 'NbRecords,1';
    Info.Signals(end).Storage     = 'single';
    Info.Signals(end).Unit        = 'rad';
    Info.Signals(end).FileName    = fullfile('Ssc_7006','Heading.bin');
    Info.Signals(end).Tag         = verifKeyWord('TODO');
    
    Info.Signals(end+1).Name      = 'HeightSource';
    Info.Signals(end).Dimensions  = 'NbRecords,1';
    Info.Signals(end).Storage     = 'uint8';
    Info.Signals(end).Unit        = [];
    Info.Signals(end).FileName    = fullfile('Ssc_7006','HeightSource.bin');
    Info.Signals(end).Tag         = verifKeyWord('TODO');
    
    Info.Signals(end+1).Name      = 'Tide';
    Info.Signals(end).Dimensions  = 'NbRecords,1';
    Info.Signals(end).Storage     = 'single';
    Info.Signals(end).Unit        = 'm';
    Info.Signals(end).FileName    = fullfile('Ssc_7006','Tide.bin');
    Info.Signals(end).Tag         = verifKeyWord('TODO');
    
    Info.Signals(end+1).Name      = 'Roll';
    Info.Signals(end).Dimensions  = 'NbRecords,1';
    Info.Signals(end).Storage     = 'single';
    Info.Signals(end).Unit        = 'rad';
    Info.Signals(end).FileName    = fullfile('Ssc_7006','Roll.bin');
    Info.Signals(end).Tag         = verifKeyWord('TODO');
    
    Info.Signals(end+1).Name      = 'Pitch';
    Info.Signals(end).Dimensions  = 'NbRecords,1';
    Info.Signals(end).Storage     = 'single';
    Info.Signals(end).Unit        = 'rad';
    Info.Signals(end).FileName    = fullfile('Ssc_7006','Pitch.bin');
    Info.Signals(end).Tag         = verifKeyWord('TODO');
    
    Info.Signals(end+1).Name      = 'Heave';
    Info.Signals(end).Dimensions  = 'NbRecords,1';
    Info.Signals(end).Storage     = 'single';
    Info.Signals(end).Unit        = 'rad';
    Info.Signals(end).FileName    = fullfile('Ssc_7006','Heave.bin');
    Info.Signals(end).Tag         = verifKeyWord('TODO');
    
    Info.Signals(end+1).Name      = 'VehicleDepth';
    Info.Signals(end).Dimensions  = 'NbRecords,1';
    Info.Signals(end).Storage     = 'single';
    Info.Signals(end).Unit        = 'm';
    Info.Signals(end).FileName    = fullfile('Ssc_7006','VehicleDepth.bin');
    Info.Signals(end).Tag         = verifKeyWord('TODO');
    
    Info.Images(end+1).Name      = 'BeamDepth';
    Info.Images(end).Dimensions  = 'NbSamples,N';
    Info.Images(end).Storage     = 'single';
    Info.Images(end).Unit        = [];
    Info.Images(end).FileName    = fullfile('Ssc_7006','BeamDepth.bin');
    Info.Images(end).Tag         = verifKeyWord('TODO');
    
    Info.Images(end+1).Name      = 'BeamAlongTrackDistance';
    Info.Images(end).Dimensions  = 'NbSamples,N';
    Info.Images(end).Storage     = 'single';
    Info.Images(end).Unit        = [];
    Info.Images(end).FileName    = fullfile('Ssc_7006','BeamAlongTrackDistance.bin');
    Info.Images(end).Tag         = verifKeyWord('TODO');
    
    Info.Images(end+1).Name      = 'BeamAcrossTrackDistance';
    Info.Images(end).Dimensions  = 'NbSamples,N';
    Info.Images(end).Storage     = 'single';
    Info.Images(end).Unit        = [];
    Info.Images(end).FileName    = fullfile('Ssc_7006','BeamAcrossTrackDistance.bin');
    Info.Images(end).Tag         = verifKeyWord('TODO');
    
    Info.Images(end+1).Name      = 'BeamPointingAngle';
    Info.Images(end).Dimensions  = 'NbSamples,N';
    Info.Images(end).Storage     = 'single';
    Info.Images(end).Unit        = [];
    Info.Images(end).FileName    = fullfile('Ssc_7006','BeamPointingAngle.bin');
    Info.Images(end).Tag         = verifKeyWord('TODO');
    
    Info.Images(end+1).Name      = 'BeamAzimuthAngle';
    Info.Images(end).Dimensions  = 'NbSamples,N';
    Info.Images(end).Storage     = 'single';
    Info.Images(end).Unit        = [];
    Info.Images(end).FileName    = fullfile('Ssc_7006','BeamAzimuthAngle.bin');
    Info.Images(end).Tag         = verifKeyWord('TODO');
end

%% Cr�ation du fichier XML d�crivant la donn�e

nomFicXml = fullfile(nomDirRacine, 'Ssc_7006.xml');
xml_write(nomFicXml, Info);
flag = exist(nomFicXml, 'file');
if ~flag
    messageErreur(nomFicXml)
    return
end

%% Cr�ation du r�pertoire 7006

nomDir7006 = fullfile(nomDirRacine, 'Ssc_7006');
if ~exist(nomDir7006, 'dir')
    status = mkdir(nomDir7006);
    if ~status
        messageErreur(nomDir7006)
        flag = 0;
        return
    end
end


%% Cr�ation des fichiers binaires des signaux

for i=1:length(Info.Signals)
    flag = writeSignal(nomDirRacine, Info.Signals(i), Data.(Info.Signals(i).Name));
    if ~flag
        return
    end
end

%% Cr�ation des fichiers binaires des images

for i=1:length(Info.Images)
    flag = writeImage(nomDirRacine, Info.Images(i), Data.(Info.Images(i).Name));
    if ~flag
        return
    end
end

flag = 1;
