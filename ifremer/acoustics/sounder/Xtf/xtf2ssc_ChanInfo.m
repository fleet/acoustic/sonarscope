function flag = xtf2ssc_ChanInfo(nomDirRacine, iNumChannel, varargin)

[varargin, flagDisplay] = getPropertyValue(varargin, 'Display', 0); %#ok<ASGLU>

%% Lecture du fichier XML d�crivant la donn�e

nomFicXml = fullfile(nomDirRacine, ['XTF_ChanInfo_' num2str(iNumChannel) '.xml']);
flag = exist(nomFicXml, 'file');
if ~flag
    special_message('FichierNonExistant', nomFicXml)
    return
end
% edit(nomFicXml)
Datagrams = xml_read(nomFicXml);

[nomDirSignal, nomFicSignal] = fileparts(nomFicXml);
nomDirSignal = fullfile(nomDirSignal, nomFicSignal);

%% Lecture de TypeOfChannel

[flag, Data.TypeOfChannel] = Read_BinVariable(Datagrams, nomDirSignal, 'TypeOfChannel', 'uint8');
if ~flag
    return
end

%% Lecture de SubChannelNumber

[flag, Data.SubChannelNumber] = Read_BinVariable(Datagrams, nomDirSignal, 'SubChannelNumber', 'uint8');
if ~flag
    return
end

%% Lecture de CorrectionFlags

[flag, Data.CorrectionFlags] = Read_BinVariable(Datagrams, nomDirSignal, 'CorrectionFlags', 'uint16');
if ~flag
    return
end

%% Lecture de UniPolar

[flag, Data.UniPolar] = Read_BinVariable(Datagrams, nomDirSignal, 'UniPolar', 'uint16');
if ~flag
    return
end

%% Lecture de BytesPerSample

[flag, Data.BytesPerSample] = Read_BinVariable(Datagrams, nomDirSignal, 'BytesPerSample', 'uint16');
if ~flag
    return
end

%% Lecture de Reserved

[flag, Data.Reserved] = Read_BinVariable(Datagrams, nomDirSignal, 'Reserved', 'uint32');
if ~flag
    return
end

%% Lecture de ChannelName

[flag, Data.ChannelName] = Read_BinVariable(Datagrams, nomDirSignal, 'ChannelName', 'char');
if ~flag
    return
end
if isempty(Data.ChannelName)
    Data.ChannelName = 'NoChannelName';
end
Data.ChannelName = {Data.ChannelName};

%% Lecture de VoltScale

[flag, Data.VoltScale] = Read_BinVariable(Datagrams, nomDirSignal, 'VoltScale', 'single');
if ~flag
    return
end

%% Lecture de Frequency

[flag, Data.Frequency] = Read_BinVariable(Datagrams, nomDirSignal, 'Frequency', 'single');
if ~flag
    return
end

%% Lecture de HorizBeamAngle

[flag, Data.HorizBeamAngle] = Read_BinVariable(Datagrams, nomDirSignal, 'HorizBeamAngle', 'single');
if ~flag
    return
end

%% Lecture de TiltAngle

[flag, Data.TiltAngle] = Read_BinVariable(Datagrams, nomDirSignal, 'TiltAngle', 'single');
if ~flag
    return
end

%% Lecture de BeamWidth

[flag, Data.BeamWidth] = Read_BinVariable(Datagrams, nomDirSignal, 'BeamWidth', 'single');
if ~flag
    return
end

%% Lecture de OffsetX

[flag, Data.OffsetX] = Read_BinVariable(Datagrams, nomDirSignal, 'OffsetX', 'single');
if ~flag
    return
end

%% Lecture de OffsetY

[flag, Data.OffsetY] = Read_BinVariable(Datagrams, nomDirSignal, 'OffsetY', 'single');
if ~flag
    return
end

%% Lecture de OffsetZ

[flag, Data.OffsetZ] = Read_BinVariable(Datagrams, nomDirSignal, 'OffsetZ', 'single');
if ~flag
    return
end

%% Lecture de OffsetYaw

[flag, Data.OffsetYaw] = Read_BinVariable(Datagrams, nomDirSignal, 'OffsetYaw', 'single');
if ~flag
    return
end

%% Lecture de OffsetPitch

[flag, Data.OffsetPitch] = Read_BinVariable(Datagrams, nomDirSignal, 'OffsetPitch', 'single');
if ~flag
    return
end

%% Lecture de OffsetYaw

[flag, Data.OffsetYaw] = Read_BinVariable(Datagrams, nomDirSignal, 'OffsetYaw', 'single');
if ~flag
    return
end

% % Lecture de OffsetYaw

[flag, Data.OffsetRoll] = Read_BinVariable(Datagrams, nomDirSignal, 'OffsetRoll', 'single');
if ~flag
    return
end

%% Lecture de BeamsPerArray

[flag, Data.BeamsPerArray] = Read_BinVariable(Datagrams, nomDirSignal, 'BeamsPerArray', 'uint16');
if ~flag
    return
end

%% Lecture de SamplePerFormat

[flag, Data.SamplePerFormat] = Read_BinVariable(Datagrams, nomDirSignal, 'SamplePerFormat', 'uint8');
if ~flag
    return
end

%% Lecture de ReservedAera2

% [flag, Data.ReservedAera2] = Read_BinVariable(Datagrams, nomDirSignal, 'ReservedAera2', 'char');
% if ~flag
%     return
% end

%% G�n�ration du XML SonarScope

Info.Title                  = Datagrams.Title;
Info.Constructor            = Datagrams.Constructor;
Info.EmModel                = Datagrams.Model;
Info.SystemSerialNumber     = Datagrams.SystemSerialNumber;
Info.TimeOrigin             = Datagrams.TimeOrigin;
Info.Comments               = Datagrams.Comments;

Info.Dimensions.NbDatagrams  = Datagrams.NbDatagrams;
Info.Dimensions.One          = 1;

kSignal  = 0;
kStrings = 0;
for i=1:size(Datagrams.Variables)
    if strcmp(Datagrams.Variables(i).Storage, 'char')
        kStrings = kStrings + 1;
        Info.Strings(kStrings).Name        = Datagrams.Variables(i).Name;
        Info.Strings(kStrings).Dimensions  = 'One';
        Info.Strings(kStrings).FileName    = fullfile(['Ssc_ChannelInfo_' num2str(iNumChannel)], [Datagrams.Variables(i).Name '.bin']);
    else
        kSignal = kSignal +1;
        Info.Signals(kSignal).Name        = Datagrams.Variables(i).Name;
        Info.Signals(kSignal).Dimensions  = 'NbDatagrams';
        Info.Signals(kSignal).Storage     = Datagrams.Variables(i).Storage;
        Info.Signals(kSignal).Unit        = [];
        Info.Signals(kSignal).FileName    = fullfile(['Ssc_ChannelInfo_' num2str(iNumChannel)], [Datagrams.Variables(i).Name '.bin']);
        Info.Signals(kSignal).Tag         = verifKeyWord('TODO');
    end
end

%% Cr�ation du fichier XML d�crivant la donn�e

nomFicXml = fullfile(nomDirRacine, ['Ssc_ChannelInfo_' num2str(iNumChannel) '.xml']);
xml_write(nomFicXml, Info);
flag = exist(nomFicXml, 'file');
if ~flag
    messageErreur(nomFicXml)
    return
end

%% Cr�ation du r�pertoire File Channel Info

nomDirSscData = fullfile(nomDirRacine, ['Ssc_ChannelInfo_' num2str(iNumChannel)]);
if ~exist(nomDirSscData, 'dir')
    status = mkdir(nomDirSscData);
    if ~status
        messageErreur(nomDirSscData)
        flag = 0;
        return
    end
end

%% Cr�ation des fichiers binaires des Strings

for i=1:length(Info.Strings)
    flag = writeStrings(nomDirRacine, Info.Strings(i), Data.(Info.Strings(i).Name));
    if ~flag
        return
    end
end

%% Cr�ation des fichiers binaires des signaux

for i=1:length(Info.Signals)
    flag = writeSignal(nomDirRacine, Info.Signals(i), Data.(Info.Signals(i).Name));
    if ~flag
        return
    end
end

flag = 1;
