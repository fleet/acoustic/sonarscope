function [flag, Data] = xtf2ssc_DRF(nomDirRacine)

%% Lecture du fichier XML d�crivant la donn�e 

nomFicXml = fullfile(nomDirRacine, 'XTF_7125Bathy_DRF.xml');
flag = exist(nomFicXml, 'file');
if ~flag
    special_message('FichierNonExistant', nomFicXml)
    return
end
% edit(nomFicXml)
Datagrams = xml_read(nomFicXml);

[nomDirSignal, nomFicSignal] = fileparts(nomFicXml);
nomDirSignal = fullfile(nomDirSignal, nomFicSignal);
   

%% Lecture de ProtocolVersion

[flag, Data.ProtocolVersion] = Read_BinVariable(Datagrams, nomDirSignal, 'ProtocolVersion', 'uint32');
if ~flag
    return
end

%% Lecture de Offset

[flag, Data.Offset] = Read_BinVariable(Datagrams, nomDirSignal, 'Offset', 'uint16');
if ~flag
    return
end

%% Lecture de SyncPattern

[flag, Data.SyncPattern] = Read_BinVariable(Datagrams, nomDirSignal, 'SyncPattern', 'uint32');
if ~flag
    return
end

%% Lecture de Size

[flag, Data.Size] = Read_BinVariable(Datagrams, nomDirSignal, 'Size', 'uint32');
if ~flag
    return
end

%% Lecture de OptionalDataOffset

[flag, Data.OptionalDataOffset] = Read_BinVariable(Datagrams, nomDirSignal, 'OptionalDataOffset', 'uint32');
if ~flag
    return
end

%% Lecture de OptionalDataIdentifier

[flag, Data.OptionalDataIdentifier] = Read_BinVariable(Datagrams, nomDirSignal, 'OptionalDataIdentifier', 'uint32');
if ~flag
    return
end

%% Lecture de Year

[flag, Data.Year] = Read_BinVariable(Datagrams, nomDirSignal, 'Year', 'uint16');
if ~flag
    return
end

%% Lecture de Day

[flag, Data.Day] = Read_BinVariable(Datagrams, nomDirSignal, 'Day', 'uint16');
if ~flag
    return
end

%% Lecture de Seconds

[flag, Data.Seconds] = Read_BinVariable(Datagrams, nomDirSignal, 'Seconds', 'single');
if ~flag
    return
end

%% Lecture de Hours

[flag, Data.Hours] = Read_BinVariable(Datagrams, nomDirSignal, 'Hours', 'uint8');
if ~flag
    return
end

%% Lecture de Minutes

[flag, Data.Minutes] = Read_BinVariable(Datagrams, nomDirSignal, 'Minutes', 'uint8');
if ~flag
    return
end

%Reserved non exploit�.

%% Lecture de RecordTypeIdentifier

[flag, Data.RecordTypeIdentifier] = Read_BinVariable(Datagrams, nomDirSignal, 'RecordTypeIdentifier', 'uint32');
if ~flag
    return
end

%% Lecture de DeviceIdentifier

[flag, Data.DeviceIdentifier] = Read_BinVariable(Datagrams, nomDirSignal, 'DeviceIdentifier', 'uint32');
if ~flag
    return
end

%Reserved2 non exploit�.


%% Lecture de SystemEnumerator

[flag, Data.SystemEnumerator] = Read_BinVariable(Datagrams, nomDirSignal, 'SystemEnumerator', 'uint16');
if ~flag
    return
end

%Reserved3 non exploit�.

%% Lecture de Flags

[flag, Data.Flags] = Read_BinVariable(Datagrams, nomDirSignal, 'Flags', 'uint16');
if ~flag
    return
end

%Reserved4 non exploit�.
%Reserved5 non exploit�.

%% Lecture de TotalRecordsInFragmentedDataRecordSet

[flag, Data.TotalRecordsInFragmentedDataRecordSet] = Read_BinVariable(Datagrams, nomDirSignal, 'TotalRecordsInFragmentedDataRecordSet', 'uint32');
if ~flag
    return
end

%% Lecture de FragmentNumber

[flag, Data.FragmentNumber] = Read_BinVariable(Datagrams, nomDirSignal, 'FragmentNumber', 'uint32');
if ~flag
    return
end

% ----------------------------
% G�n�ration du XML SonarScope
Info.Title                  = Datagrams.Title; % Motion Reference Unit
Info.Constructor            = Datagrams.Constructor;
Info.Model                  = Datagrams.Model;
Info.TimeOrigin             = Datagrams.TimeOrigin;
Info.Comments               = Datagrams.Comments;

Info.Dimensions.NbRecords     = Datagrams.NbDatagrams;

Info.Signals(1).Name          = 'ProtocolVersion';
Info.Signals(end).Dimensions  = 'NbRecords,1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = []';
Info.Signals(end).FileName    = fullfile('Ssc_DRF','ProtocolVersion.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'Offset';
Info.Signals(end).Dimensions  = 'NbRecords,1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_DRF','Offset.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'SyncPattern';
Info.Signals(end).Dimensions  = 'NbRecords,1';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_DRF','SyncPattern.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'Size';
Info.Signals(end).Dimensions  = 'NbRecords,1';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_DRF','Size.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'OptionalDataOffset';
Info.Signals(end).Dimensions  = 'NbRecords,1';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_DRF','OptionalDataOffset.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'OptionalDataIdentifier';
Info.Signals(end).Dimensions  = 'NbRecords,1';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_DRF','OptionalDataIdentifier.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'Year';
Info.Signals(end).Dimensions  = 'NbRecords,1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_DRF','Year.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'Day';
Info.Signals(end).Dimensions  = 'NbRecords,1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_DRF','Day.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'Seconds';
Info.Signals(end).Dimensions  = 'NbRecords,1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'rad';
Info.Signals(end).FileName    = fullfile('Ssc_DRF','Seconds.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'Hours';
Info.Signals(end).Dimensions  = 'NbRecords,1';
Info.Signals(end).Storage     = 'uint8';
Info.Signals(end).Unit        = 'rad';
Info.Signals(end).FileName    = fullfile('Ssc_DRF','Hours.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'Minutes';
Info.Signals(end).Dimensions  = 'NbRecords,1';
Info.Signals(end).Storage     = 'uint8';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_DRF','Minutes.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

%Reserved1 non exploit�.

Info.Signals(end+1).Name      = 'RecordTypeIdentifier';
Info.Signals(end).Dimensions  = 'NbRecords,1';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_DRF','RecordTypeIdentifier.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'DeviceIdentifier';
Info.Signals(end).Dimensions  = 'NbRecords,1';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_DRF','DeviceIdentifier.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

%Reserved2 non exploit�.

Info.Signals(end+1).Name      = 'SystemEnumerator';
Info.Signals(end).Dimensions  = 'NbRecords,1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_DRF','SystemEnumerator.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

%Reserved3 non exploit�.

Info.Signals(end+1).Name      = 'Flags';
Info.Signals(end).Dimensions  = 'NbRecords,1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_DRF','Flags.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

%Reserved4 non exploit�.
%Reserved5 non exploit�.

Info.Signals(end+1).Name      = 'TotalRecordsInFragmentedDataRecordSet';
Info.Signals(end).Dimensions  = 'NbRecords,1';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_DRF','TotalRecordsInFragmentedDataRecordSet.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'FragmentNumber';
Info.Signals(end).Dimensions  = 'NbRecords,1';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_DRF','FragmentNumber.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');


%% Cr�ation du fichier XML d�crivant la donn�e 

nomFicXml = fullfile(nomDirRacine, 'Ssc_DRF.xml');
xml_write(nomFicXml, Info);
flag = exist(nomFicXml, 'file');
if ~flag
    messageErreur(nomFicXml)
    return
end

%% Cr�ation du r�pertoire DRF

nomDirDRF = fullfile(nomDirRacine, 'Ssc_DRF');
if ~exist(nomDirDRF, 'dir')
    status = mkdir(nomDirDRF);
    if ~status
        messageErreur(nomDirDRF)
        flag = 0;
        return
    end
end


%% Cr�ation des fichiers binaires des signaux

for i=1:length(Info.Signals)
    flag = writeSignal(nomDirRacine, Info.Signals(i), Data.(Info.Signals(i).Name));
    if ~flag
        return
    end
end

flag = 1;
