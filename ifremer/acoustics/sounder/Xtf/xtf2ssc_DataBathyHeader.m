function [flag, Data] = xtf2ssc_DataBathyHeader(FullNomDirRacine, iNumChannel, DataNumChannelsToFollow, varargin)

% [varargin, flagDisplay] = getPropertyValue(varargin, 'Display', 0);
[varargin, N] = getPropertyValue(varargin, 'N', 0); %#ok<ASGLU>

NomDirXml = 'Ssc_Bathy';

%% Lecture du fichier XML d�crivant la donn�e

nomFicXml = fullfile(FullNomDirRacine, ['XTF_Bathymetry_' num2str(iNumChannel) '.xml']);
flag = exist(nomFicXml, 'file');
if ~flag
    special_message('FichierNonExistant', nomFicXml)
    return
end
% edit(nomFicXml)
Datagrams = xml_read(nomFicXml);

[FullNomDirSignal, nomFicSignal] = fileparts(nomFicXml);
FullNomDirSignal = fullfile(FullNomDirSignal, nomFicSignal);

%% Lecture de Bathy
[flag, Bathy] = Read_BinVariable(Datagrams, FullNomDirSignal, 'Bathymetry', 'single');
if ~flag
    return
end

Data.Bathy = NaN(Datagrams.NbDatagrams, N, class(Bathy));
iDeb = 0;
for k=1:Datagrams.NbDatagrams
    sub = 1:DataNumChannelsToFollow(k);
    Data.Bathy(k,sub) = Bathy(iDeb+sub);
    iDeb = iDeb + DataNumChannelsToFollow(k);
end
clear Bathy sub


%% G�n�ration du XML SonarScope

Info.Title                  = Datagrams.Title; % Entete du fichier
Info.Constructor            = Datagrams.Constructor;
Info.EmModel                = Datagrams.Model;
Info.SystemSerialNumber     = Datagrams.SystemSerialNumber;
Info.TimeOrigin             = Datagrams.TimeOrigin;
Info.Comments               = Datagrams.Comments;

Info.Dimensions.NbSamples     = Datagrams.NbDatagrams;

Info.Images(1).Name          = 'Data'; % Set to 0xFACE
Info.Images(end).Dimensions  = 'NbSamples,1';
Info.Images(end).Storage     = 'single';
Info.Images(end).Unit        = [];
Info.Images(end).FileName    = fullfile(NomDirXml,'Data.bin');
Info.Images(end).Tag         = verifKeyWord('TODO');

%ReservedSpace non trait�

%% Cr�ation du fichier XML d�crivant la donn�e

nomFicXml = fullfile(FullNomDirRacine, [NomDirXml '.xml']);
xml_write(nomFicXml, Info);
flag = exist(nomFicXml, 'file');
if ~flag
    messageErreur(nomFicXml)
    return
end

%% Cr�ation du r�pertoire File Header

FullNomDir = fullfile(FullNomDirRacine, NomDirXml);
if ~exist(FullNomDir, 'dir')
    status = mkdir(FullNomDir);
    if ~status
        messageErreur(FullNomDir)
        flag = 0;
        return
    end
end

%% Cr�ation des fichiers binaires des signaux

for i=1:length(Info.Signals)
    flag = writeSignal(FullNomDirRacine, Info.Signals(i), Data.(Info.Signals(i).Name));
    if ~flag
        return
    end
end

flag = 1;
