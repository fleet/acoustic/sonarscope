function [flag, Data] = xtf2ssc_DataHeader(FullNomDirRacine, DataName, NbChannels, varargin)

% [varargin, flagDisplay] = getPropertyValue(varargin, 'Display', 0);

FullNomDirXml = ['Ssc_' DataName];

%% Lecture du fichier XML d�crivant la donn�e

nomFicXml = fullfile(FullNomDirRacine, ['XTF_' DataName '.xml']);
flag = exist(nomFicXml, 'file');
if ~flag
    special_message('FichierNonExistant', nomFicXml)
    return
end
% edit(nomFicXml)
Datagrams = xml_read(nomFicXml);

[FullNomDirSignal, nomFicSignal] = fileparts(nomFicXml);
FullNomDirSignal = fullfile(FullNomDirSignal, nomFicSignal);

NbPings = Datagrams.NbDatagrams / NbChannels;

%% Lecture de MagicNumber

[flag, X] = Read_BinVariable(Datagrams, FullNomDirSignal, 'MagicNumber', 'uint16');
if ~flag
    return
end
X = reshape(X, [NbChannels NbPings]);
Data.MagicNumber   = X';

%% Lecture de HeaderType

[flag, X] = Read_BinVariable(Datagrams, FullNomDirSignal, 'HeaderType', 'uint8');
if ~flag
    return
end
X = reshape(X, [NbChannels NbPings]);
Data.HeaderType   = X';

%% Lecture de SubChannelNumber

[flag, X] = Read_BinVariable(Datagrams, FullNomDirSignal, 'SubChannelNumber', 'uint8');
if ~flag
    return
end
X = reshape(X, [NbChannels NbPings]);
Data.SubChannelNumber   = X' + 1;

%% Lecture de NumChanToFollow

[flag, X] = Read_BinVariable(Datagrams, FullNomDirSignal, 'NumChanToFollow', 'uint16');
if ~flag
    return
end
X = reshape(X, [NbChannels NbPings]);
Data.NumChanToFollow = X';

%% Lecture de Reserved1

% [flag, Reserved1] = Read_BinVariable(Datagrams, FullNomDirSignal, 'Reserved1', 'uint16'); %#ok<NASGU>
% if ~flag
%     return
% end

%% Lecture de NumBytesThisRecord

[flag, X] = Read_BinVariable(Datagrams, FullNomDirSignal, 'NumBytesThisRecord', 'uint32');
if ~flag
    return
end
X = reshape(X, [NbChannels NbPings]);
Data.NumBytesThisRecord = X';

%########################
% Data and time of the ping

%% Lecture de Year

[flag, X] = Read_BinVariable(Datagrams, FullNomDirSignal, 'Year', 'uint16');
if ~flag
    return
end
X = reshape(X, [NbChannels NbPings]);
Data.Year = X';

%% Lecture de Month

[flag, X] = Read_BinVariable(Datagrams, FullNomDirSignal, 'Month', 'uint8');
if ~flag
    return
end
X = reshape(X, [NbChannels NbPings]);
Data.Month = X';

%% Lecture de Day

[flag, X] = Read_BinVariable(Datagrams, FullNomDirSignal, 'Day', 'uint8');
if ~flag
    return
end
X = reshape(X, [NbChannels NbPings]);
Data.Day = X';

%% Lecture de Hour

[flag, X] = Read_BinVariable(Datagrams, FullNomDirSignal, 'Hour', 'uint8');
if ~flag
    return
end
X = reshape(X, [NbChannels NbPings]);
Data.Hour = X';
%% Lecture de Minute

[flag, X] = Read_BinVariable(Datagrams, FullNomDirSignal, 'Minute', 'uint8');
if ~flag
    return
end
X = reshape(X, [NbChannels NbPings]);
Data.Minute = X';

%% Lecture de Second

[flag, X] = Read_BinVariable(Datagrams, FullNomDirSignal, 'Second', 'uint16');
if ~flag
    return
end
X = reshape(X, [NbChannels NbPings]);
Data.Second = X';

%% Lecture de HSeconds

[flag, X] = Read_BinVariable(Datagrams, FullNomDirSignal, 'HSeconds', 'uint8');
if ~flag
    return
end
X = reshape(X, [NbChannels NbPings]);
Data.HSeconds = X';

%% Lecture de JulianDay

[flag, X] = Read_BinVariable(Datagrams, FullNomDirSignal, 'JulianDay', 'uint16');
if ~flag
    return
end
X = reshape(X, [NbChannels NbPings]);
Data.JulianDay = X';

%% Lecture de EventNumber

[flag, X] = Read_BinVariable(Datagrams, FullNomDirSignal, 'EventNumber', 'uint32');
if ~flag
    return
end
X = reshape(X, [NbChannels NbPings]);
Data.EventNumber = X';

%% Lecture de PingNumber

[flag, X] = Read_BinVariable(Datagrams, FullNomDirSignal, 'PingNumber', 'uint32');
if ~flag
    return
end
X = reshape(X, [NbChannels NbPings]);
Data.PingNumber = X';

%% Lecture de SoundVelocity

[flag, X] = Read_BinVariable(Datagrams, FullNomDirSignal, 'SoundVelocity', 'single');
if ~flag
    return
end
X = reshape(X, [NbChannels NbPings]);
Data.SoundVelocity = X*2';

%% Lecture de OceanTide

[flag, X] = Read_BinVariable(Datagrams, FullNomDirSignal, 'OceanTide', 'single');
if ~flag
    return
end
X = reshape(X, [NbChannels NbPings]);
Data.OceanTide = X';

%% Lecture de Reserved2

% [flag, Reserved2] = Read_BinVariable(Datagrams, FullNomDirSignal, 'Reserved2', 'uint32'); %#ok<NASGU>
% if ~flag
%     return
% end

%% Lecture de ConductivityFreq

[flag, X] = Read_BinVariable(Datagrams, FullNomDirSignal, 'ConductivityFreq', 'single');
if ~flag
    return
end
X = reshape(X, [NbChannels NbPings]);
Data.ConductivityFreq = X';

%% Lecture de TemperatureFreq

[flag, X] = Read_BinVariable(Datagrams, FullNomDirSignal, 'TemperatureFreq', 'single');
if ~flag
    return
end
X = reshape(X, [NbChannels NbPings]);
Data.TemperatureFreq = X';

%% Lecture de PressureFreq

[flag, X] = Read_BinVariable(Datagrams, FullNomDirSignal, 'PressureFreq', 'single');
if ~flag
    return
end
X = reshape(X, [NbChannels NbPings]);
Data.PressureFreq = X';

%% Lecture de PressureTemp

[flag, X] = Read_BinVariable(Datagrams, FullNomDirSignal, 'PressureTemp', 'single');
if ~flag
    return
end
X = reshape(X, [NbChannels NbPings]);
Data.PressureTemp = X';

%% Lecture de Conductivity

[flag, X] = Read_BinVariable(Datagrams, FullNomDirSignal, 'Conductivity', 'single');
if ~flag
    return
end
X = reshape(X, [NbChannels NbPings]);
Data.Conductivity = X';

%% Lecture de WaterTemperature

[flag, X] = Read_BinVariable(Datagrams, FullNomDirSignal, 'WaterTemperature', 'single');
if ~flag
    return
end
X = reshape(X, [NbChannels NbPings]);
Data.WaterTemperature = X';

%% Lecture de Pressure

[flag, X] = Read_BinVariable(Datagrams, FullNomDirSignal, 'Pressure', 'single');
if ~flag
    return
end
X = reshape(X, [NbChannels NbPings]);
Data.Pressure = X';

%% Lecture de ComputedSoundVelocity

[flag, X] = Read_BinVariable(Datagrams, FullNomDirSignal, 'ComputedSoundVelocity', 'single');
if ~flag
    return
end
X = reshape(X, [NbChannels NbPings]);
Data.ComputedSoundVelocity = X';

%##########################
% Param�tres du capteur

%% Lecture de MagX

[flag, X] = Read_BinVariable(Datagrams, FullNomDirSignal, 'MagX', 'single');
if ~flag
    return
end
X = reshape(X, [NbChannels NbPings]);
Data.MagX = X';

%% Lecture de MagY

[flag, X] = Read_BinVariable(Datagrams, FullNomDirSignal, 'MagY', 'single');
if ~flag
    return
end
X = reshape(X, [NbChannels NbPings]);
Data.MagY = X';

%% Lecture de MagZ

[flag, X] = Read_BinVariable(Datagrams, FullNomDirSignal, 'MagZ', 'single');
if ~flag
    return
end
X = reshape(X, [NbChannels NbPings]);
Data.MagZ = X';

%% Lecture de AuxVal1

[flag, X] = Read_BinVariable(Datagrams, FullNomDirSignal, 'AuxVal1', 'single');
if ~flag
    return
end
X = reshape(X, [NbChannels NbPings]);
Data.AuxVal1 = X';

%% Lecture de AuxVal2

[flag, X] = Read_BinVariable(Datagrams, FullNomDirSignal, 'AuxVal2', 'single');
if ~flag
    return
end
X = reshape(X, [NbChannels NbPings]);
Data.AuxVal2 = X';

%% Lecture de AuxVal3

[flag, X] = Read_BinVariable(Datagrams, FullNomDirSignal, 'AuxVal3', 'single');
if ~flag
    return
end
X = reshape(X, [NbChannels NbPings]);
Data.AuxVal3 = X';

%% Lecture de AuxVal4

[flag, X] = Read_BinVariable(Datagrams, FullNomDirSignal, 'AuxVal4', 'single');
if ~flag
    return
end
X = reshape(X, [NbChannels NbPings]);
Data.AuxVal4 = X';

%% Lecture de AuxVal5

[flag, X] = Read_BinVariable(Datagrams, FullNomDirSignal, 'AuxVal5', 'single');
if ~flag
    return
end
X = reshape(X, [NbChannels NbPings]);
Data.AuxVal5 = X';

%% Lecture de AuxVal6

[flag, X] = Read_BinVariable(Datagrams, FullNomDirSignal, 'AuxVal6', 'single');
if ~flag
    return
end
X = reshape(X, [NbChannels NbPings]);
Data.AuxVal6 = X';

%% Lecture de SpeedLog

[flag, X] = Read_BinVariable(Datagrams, FullNomDirSignal, 'SpeedLog', 'single');
if ~flag
    return
end
X = reshape(X, [NbChannels NbPings]);
Data.SpeedLog = X';

%% Lecture de Turbidity

[flag, X] = Read_BinVariable(Datagrams, FullNomDirSignal, 'Turbidity', 'single');
if ~flag
    return
end
X = reshape(X, [NbChannels NbPings]);
Data.Turbidity = X';

%% Lecture de ShipSpeed

[flag, X] = Read_BinVariable(Datagrams, FullNomDirSignal, 'ShipSpeed', 'single');
if ~flag
    return
end
X = reshape(X, [NbChannels NbPings]);
Data.ShipSpeed = X';

%% Lecture de ShipGyro

[flag, X] = Read_BinVariable(Datagrams, FullNomDirSignal, 'ShipGyro', 'single');
if ~flag
    return
end
X = reshape(X, [NbChannels NbPings]);
Data.ShipGyro = X';

%% Lecture de ShipYcoordinate

[flag, X] = Read_BinVariable(Datagrams, FullNomDirSignal, 'ShipYcoordinate', 'double');
if ~flag
    return
end
X = reshape(X, [NbChannels NbPings]);
Data.ShipYcoordinate = X';
%% Lecture de ShipXcoordinate

[flag, X] = Read_BinVariable(Datagrams, FullNomDirSignal, 'ShipXcoordinate', 'double');
if ~flag
    return
end
X = reshape(X, [NbChannels NbPings]);
Data.ShipXcoordinate = X';

%% Lecture de ShipAltitude

[flag, X] = Read_BinVariable(Datagrams, FullNomDirSignal, 'ShipAltitude', 'int32');
if ~flag
    return
end
X = reshape(X, [NbChannels NbPings]);
Data.ShipAltitude = X';

%% Lecture de ShipDepth

[flag, X] = Read_BinVariable(Datagrams, FullNomDirSignal, 'ShipDepth', 'int32');
if ~flag
    return
end
X = reshape(X, [NbChannels NbPings]);
Data.ShipDepth = X';

%% Lecture de FixTimeHour

[flag, X] = Read_BinVariable(Datagrams, FullNomDirSignal, 'FixTimeHour', 'uint8');
if ~flag
    return
end
X = reshape(X, [NbChannels NbPings]);
Data.FixTimeHour = X';

%% Lecture de FixTimeMinute

[flag, X] = Read_BinVariable(Datagrams, FullNomDirSignal, 'FixTimeMinute', 'uint8');
if ~flag
    return
end
X = reshape(X, [NbChannels NbPings]);
Data.FixTimeMinute = X';

%% Lecture de FixTimeSecond

[flag, X] = Read_BinVariable(Datagrams, FullNomDirSignal, 'FixTimeSecond', 'uint8');
if ~flag
    return
end
X = reshape(X, [NbChannels NbPings]);
Data.FixTimeSecond = X';

%% Lecture de FixTimeHsecond

[flag, X] = Read_BinVariable(Datagrams, FullNomDirSignal, 'FixTimeHsecond', 'uint8');
if ~flag
    return
end
X = reshape(X, [NbChannels NbPings]);
Data.FixTimeHsecond = X';

%% Lecture de SensorSpeed

[flag, X] = Read_BinVariable(Datagrams, FullNomDirSignal, 'SensorSpeed', 'single');
if ~flag
    return
end
X = reshape(X, [NbChannels NbPings]);
Data.SensorSpeed = X';

%% Lecture de KP

[flag, X] = Read_BinVariable(Datagrams, FullNomDirSignal, 'KP', 'single');
if ~flag
    return
end
X = reshape(X, [NbChannels NbPings]);
Data.KP = X';

%% Lecture de SensorYCoordinate

[flag, X] = Read_BinVariable(Datagrams, FullNomDirSignal, 'SensorYCoordinate', 'double');
if ~flag
    return
end
X = reshape(X, [NbChannels NbPings]);
Data.SensorYCoordinate = X';

%% Lecture de SensorXCoordinate

[flag, X] = Read_BinVariable(Datagrams, FullNomDirSignal, 'SensorXCoordinate', 'double');
if ~flag
    return
end
X = reshape(X, [NbChannels NbPings]);
Data.SensorXCoordinate = X';

%##########################
% Tow Cable information

%% Lecture de SonarStatus

[flag, X] = Read_BinVariable(Datagrams, FullNomDirSignal, 'SonarStatus', 'uint16');
if ~flag
    return
end
X = reshape(X, [NbChannels NbPings]);
Data.SonarStatus = X';

%% Lecture de RangeToFish

[flag, X] = Read_BinVariable(Datagrams, FullNomDirSignal, 'RangeToFish', 'uint16');
if ~flag
    return
end
X = reshape(X, [NbChannels NbPings]);
Data.RangeToFish = X';

%% Lecture de BearingToFish

[flag, X] = Read_BinVariable(Datagrams, FullNomDirSignal, 'BearingToFish', 'uint16');
if ~flag
    return
end
X = reshape(X, [NbChannels NbPings]);
Data.BearingToFish = X';

%% Lecture de CableOut

[flag, X] = Read_BinVariable(Datagrams, FullNomDirSignal, 'CableOut', 'uint16');
if ~flag
    return
end
X = reshape(X, [NbChannels NbPings]);
Data.CableOut = X';

%% Lecture de LayBack

[flag, X] = Read_BinVariable(Datagrams, FullNomDirSignal, 'LayBack', 'single');
if ~flag
    return
end
X = reshape(X, [NbChannels NbPings]);
Data.LayBack = X';

%% Lecture de CableTension

[flag, X] = Read_BinVariable(Datagrams, FullNomDirSignal, 'CableTension', 'single');
if ~flag
    return
end
X = reshape(X, [NbChannels NbPings]);
Data.CableTension = X';

%% Lecture de SensorDepth

[flag, X] = Read_BinVariable(Datagrams, FullNomDirSignal, 'SensorDepth', 'single');
if ~flag
    return
end
X = reshape(X, [NbChannels NbPings]);
Data.SensorDepth = X';

%% Lecture de SensorPrimaryAltitude

[flag, X] = Read_BinVariable(Datagrams, FullNomDirSignal, 'SensorPrimaryAltitude', 'single');
if ~flag
    return
end
X = reshape(X, [NbChannels NbPings]);
Data.SensorPrimaryAltitude = X';

%% Lecture de SensorAuxAltitude

[flag, X] = Read_BinVariable(Datagrams, FullNomDirSignal, 'SensorAuxAltitude', 'single');
if ~flag
    return
end
X = reshape(X, [NbChannels NbPings]);
Data.SensorAuxAltitude = X';

%% Lecture de SensorPitch

[flag, X] = Read_BinVariable(Datagrams, FullNomDirSignal, 'SensorPitch', 'single');
if ~flag
    return
end
X = reshape(X, [NbChannels NbPings]);
Data.SensorPitch = X';

%% Lecture de SensorRoll

[flag, X] = Read_BinVariable(Datagrams, FullNomDirSignal, 'SensorRoll', 'single');
if ~flag
    return
end
X = reshape(X, [NbChannels NbPings]);
Data.SensorRoll = X';

%% Lecture de SensorHeading

[flag, X] = Read_BinVariable(Datagrams, FullNomDirSignal, 'SensorHeading', 'single');
if ~flag
    return
end
X = reshape(X, [NbChannels NbPings]);
Data.SensorHeading = X';
%#########################
% Additional attitude data

%% Lecture de Heave

[flag, X] = Read_BinVariable(Datagrams, FullNomDirSignal, 'Heave', 'single');
if ~flag
    return
end
X = reshape(X, [NbChannels NbPings]);
Data.Heave = X';

%% Lecture de Yaw

[flag, X] = Read_BinVariable(Datagrams, FullNomDirSignal, 'Yaw', 'single');
if ~flag
    return
end
X = reshape(X, [NbChannels NbPings]);
Data.Yaw = X';

%% Lecture de AttitudeTimeTag

[flag, X] = Read_BinVariable(Datagrams, FullNomDirSignal, 'AttitudeTimeTag', 'int32');
if ~flag
    return
end
X = reshape(X, [NbChannels NbPings]);
Data.AttitudeTimeTag = X';

%% Lecture de DOT

[flag, X] = Read_BinVariable(Datagrams, FullNomDirSignal, 'DOT', 'single');
if ~flag
    return
end
X = reshape(X, [NbChannels NbPings]);
Data.DOT = X';

%% Lecture de NavFixMilliseconds

[flag, X] = Read_BinVariable(Datagrams, FullNomDirSignal, 'NavFixMilliseconds', 'uint8');
if ~flag
    return
end
X = reshape(X, [NbChannels NbPings]);
Data.NavFixMilliseconds = X';

%% Lecture de ComputerClockHour

[flag, X] = Read_BinVariable(Datagrams, FullNomDirSignal, 'ComputerClockHour', 'uint8');
if ~flag
    return
end
X = reshape(X, [NbChannels NbPings]);
Data.ComputerClockHour = X';

%% Lecture de ComputerClockMinute

[flag, X] = Read_BinVariable(Datagrams, FullNomDirSignal, 'ComputerClockMinute', 'uint8');
if ~flag
    return
end
X = reshape(X, [NbChannels NbPings]);
Data.ComputerClockMinute = X';

%% Lecture de ComputerClockSecond

[flag, X] = Read_BinVariable(Datagrams, FullNomDirSignal, 'ComputerClockSecond', 'uint8');
if ~flag
    return
end
X = reshape(X, [NbChannels NbPings]);
Data.ComputerClockSecond = X';

%% Lecture de ComputerClockHsec

[flag, X] = Read_BinVariable(Datagrams, FullNomDirSignal, 'ComputerClockHsec', 'uint8');
if ~flag
    return
end
X = reshape(X, [NbChannels NbPings]);
Data.ComputerClockHsec = X';

%% Lecture de FishPositionDeltaX

[flag, X] = Read_BinVariable(Datagrams, FullNomDirSignal, 'FishPositionDeltaX', 'int16');
if ~flag
    return
end
X = reshape(X, [NbChannels NbPings]);
Data.FishPositionDeltaX = X';

%% Lecture de FishPositionDeltaY

[flag, X] = Read_BinVariable(Datagrams, FullNomDirSignal, 'FishPositionDeltaY', 'int16');
if ~flag
    return
end
X = reshape(X, [NbChannels NbPings]);
Data.FishPositionDeltaY = X';

%% Lecture de FishPositionErrorCode

[flag, X] = Read_BinVariable(Datagrams, FullNomDirSignal, 'FishPositionErrorCode', 'uint8');
if ~flag
    return
end
X = reshape(X, [NbChannels NbPings]);
Data.FishPositionErrorCode = X';

%% Lecture de ReservedSpace

% [flag, Data.ReservedSpace] = Read_BinVariable(Datagrams, FullNomDirSignal, 'ReservedSpace', 'uint8');
% if ~flag
%     return
% end

%% G�n�ration du XML SonarScope

Info.Title                  = Datagrams.Title; % Entete du fichier
Info.Constructor            = Datagrams.Constructor;
Info.EmModel                = Datagrams.Model;
Info.SystemSerialNumber     = Datagrams.SystemSerialNumber;
Info.TimeOrigin             = Datagrams.TimeOrigin;
Info.Comments               = Datagrams.Comments;

Info.Dimensions.NbPings     = NbPings;
Info.Dimensions.NbChannels  = NbChannels;

Info.Signals(1).Name          = 'MagicNumber'; % Set to 0xFACE
Info.Signals(end).Dimensions  = 'NbPings, NbChannels';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'MagicNumber.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'HeaderType';
Info.Signals(end).Dimensions  = 'NbPings, NbChannels';
Info.Signals(end).Storage     = 'uint8';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'HeaderType.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'SubChannelNumber';
Info.Signals(end).Dimensions  = 'NbPings, NbChannels';
Info.Signals(end).Storage     = 'uint8';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'SubChannelNumber.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'NumChanToFollow';
Info.Signals(end).Dimensions  = 'NbPings, NbChannels';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'NumChanToFollow.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'NumBytesThisRecord';
Info.Signals(end).Dimensions  = 'NbPings, NbChannels';
Info.Signals(end).Storage     = 'char';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'NumBytesThisRecord.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

% Info.Signals(end+1).Name      = 'Reserved1';
% Info.Signals(end).Dimensions  = 'NbSamples,2';
% Info.Signals(end).Storage     = 'uint16';
% Info.Signals(end).Unit        = [];
% Info.Signals(end).FileName    = fullfile(FullNomDirXml,'Reserved1.bin');
% Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'Year';
Info.Signals(end).Dimensions  = 'NbPings, NbChannels';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'Year.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'Month';
Info.Signals(end).Dimensions  = 'NbPings, NbChannels';
Info.Signals(end).Storage     = 'uint8';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'Month.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'Hour';
Info.Signals(end).Dimensions  = 'NbPings, NbChannels';
Info.Signals(end).Storage     = 'uint8';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'Hour.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');


Info.Signals(end+1).Name      = 'Minute';
Info.Signals(end).Dimensions  = 'NbPings, NbChannels';
Info.Signals(end).Storage     = 'uint8';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'Minute.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'Second';
Info.Signals(end).Dimensions  = 'NbPings, NbChannels';
Info.Signals(end).Storage     = 'uint8';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'Second.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');


Info.Signals(end+1).Name      = 'HSeconds';
Info.Signals(end).Dimensions  = 'NbPings, NbChannels';
Info.Signals(end).Storage     = 'uint8';
Info.Signals(end).Unit        = []; % 0-99
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'HSeconds.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'JulianDay';
Info.Signals(end).Dimensions  = 'NbPings, NbChannels';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = []; % Numbers of days since January 1
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'JulianDay.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'EventNumber';
Info.Signals(end).Dimensions  = 'NbPings, NbChannels';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'EventNumber.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'PingNumber';
Info.Signals(end).Dimensions  = 'NbPings, NbChannels';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'PingNumber.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'SoundVelocity';
Info.Signals(end).Dimensions  = 'NbPings, NbChannels';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'm/s';
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'SoundVelocity.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'OceanTide';
Info.Signals(end).Dimensions  = 'NbPings, NbChannels';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'm';
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'OceanTide.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

% Info.Signals(end+1).Name      = 'Reserved2';
% Info.Signals(end).Dimensions  = 'NbPings, NbChannels';
% Info.Signals(end).Storage     = 'uint16';
% Info.Signals(end).Unit        = [];
% Info.Signals(end).FileName    = fullfile(FullNomDirXml,'Reserved2.bin');
% Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'ComputedSoundVelocity';
Info.Signals(end).Dimensions  = 'NbPings, NbChannels';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'Hz';
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'ComputedSoundVelocity.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'TemperatureFreq';
Info.Signals(end).Dimensions  = 'NbPings, NbChannels';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'Hz';
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'TemperatureFreq.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'PressureFreq';
Info.Signals(end).Dimensions  = 'NbPings, NbChannels';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'Hz';
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'PressureFreq.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'PressureTemp';
Info.Signals(end).Dimensions  = 'NbPings, NbChannels';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'C';
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'PressureTemp.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'Conductivity';
Info.Signals(end).Dimensions  = 'NbPings, NbChannels';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'S/m';
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'Conductivity.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'WaterTemperature';
Info.Signals(end).Dimensions  = 'NbPings, NbChannels';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'C';
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'WaterTemperature.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'Pressure';
Info.Signals(end).Dimensions  = 'NbPings, NbChannels';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'psia';
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'Pressure.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'MagX';
Info.Signals(end).Dimensions  = 'NbPings, NbChannels';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'mgauss';
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'MagX.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'MagY';
Info.Signals(end).Dimensions  = 'NbPings, NbChannels';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'mgauss';
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'MagY.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'MagZ';
Info.Signals(end).Dimensions  = 'NbPings, NbChannels';
Info.Signals(end).Storage     = 'uint64';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'MagZ.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'AuxVal1';
Info.Signals(end).Dimensions  = 'NbPings, NbChannels';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'AuxVal1.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'AuxVal2';
Info.Signals(end).Dimensions  = 'NbPings, NbChannels';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'AuxVal2.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'AuxVal3';
Info.Signals(end).Dimensions  = 'NbPings, NbChannels';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'AuxVal3.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'AuxVal4';
Info.Signals(end).Dimensions  = 'NbPings, NbChannels';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'AuxVal4.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'AuxVal5';
Info.Signals(end).Dimensions  = 'NbPings, NbChannels';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'AuxVal5.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'AuxVal6';
Info.Signals(end).Dimensions  = 'NbPings, NbChannels';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'AuxVal6.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'SpeedLog';
Info.Signals(end).Dimensions  = 'NbPings, NbChannels';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'knots';
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'SpeedLog.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'Turbidity';
Info.Signals(end).Dimensions  = 'NbPings, NbChannels';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'V';    % 0 to +5volts
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'Turbidity.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'ShipSpeed';
Info.Signals(end).Dimensions  = 'NbPings, NbChannels';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'knot';
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'ShipSpeed.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'ShipGyro';
Info.Signals(end).Dimensions  = 'NbPings, NbChannels';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'ShipGyro.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'ShipYcoordinate';
Info.Signals(end).Dimensions  = 'NbPings, NbChannels';
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'ShipYcoordinate.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'ShipXcoordinate';
Info.Signals(end).Dimensions  = 'NbPings, NbChannels';
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'ShipXcoordinate.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'ShipAltitude';
Info.Signals(end).Dimensions  = 'NbPings, NbChannels';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'ShipAltitude.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'ShipDepth';
Info.Signals(end).Dimensions  = 'NbPings, NbChannels';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'ShipDepth.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'FixTimeHour';
Info.Signals(end).Dimensions  = 'NbPings, NbChannels';
Info.Signals(end).Storage     = 'uint8';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'FixTimeHour.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'FixTimeMinute';
Info.Signals(end).Dimensions  = 'NbPings, NbChannels';
Info.Signals(end).Storage     = 'uint8';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'FixTimeMinute.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'FixTimeSecond';
Info.Signals(end).Dimensions  = 'NbPings, NbChannels';
Info.Signals(end).Storage     = 'uint8';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'FixTimeSecond.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'FixTimeHsecond';
Info.Signals(end).Dimensions  = 'NbPings, NbChannels';
Info.Signals(end).Storage     = 'uint8';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'FixTimeHsecond.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'SensorSpeed';
Info.Signals(end).Dimensions  = 'NbPings, NbChannels';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'knots';
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'SensorSpeed.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'KP';
Info.Signals(end).Dimensions  = 'NbPings, NbChannels';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'KP.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'SensorYCoordinate';
Info.Signals(end).Dimensions  = 'NbPings, NbChannels';
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'SensorYCoordinate.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'SensorXCoordinate';
Info.Signals(end).Dimensions  = 'NbPings, NbChannels';
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'SensorXCoordinate.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'SonarStatus';
Info.Signals(end).Dimensions  = 'NbPings, NbChannels';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'SonarStatus.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'RangeToFish';
Info.Signals(end).Dimensions  = 'NbPings, NbChannels';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'RangeToFish.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'BearingToFish';
Info.Signals(end).Dimensions  = 'NbPings, NbChannels';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'BearingToFish.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'CableOut';
Info.Signals(end).Dimensions  = 'NbPings, NbChannels';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'CableOut.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'LayBack';
Info.Signals(end).Dimensions  = 'NbPings, NbChannels';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'LayBack.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'CableTension';
Info.Signals(end).Dimensions  = 'NbPings, NbChannels';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'CableTension.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'SensorDepth';
Info.Signals(end).Dimensions  = 'NbPings, NbChannels';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'SensorDepth.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'SensorPrimaryAltitude';
Info.Signals(end).Dimensions  = 'NbPings, NbChannels';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'SensorPrimaryAltitude.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'SensorAuxAltitude';
Info.Signals(end).Dimensions  = 'NbPings, NbChannels';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'SensorAuxAltitude.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'SensorPitch';
Info.Signals(end).Dimensions  = 'NbPings, NbChannels';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'SensorPitch.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'SensorRoll';
Info.Signals(end).Dimensions  = 'NbPings, NbChannels';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'SensorRoll.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'SensorHeading';
Info.Signals(end).Dimensions  = 'NbPings, NbChannels';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'SensorHeading.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'Heave';
Info.Signals(end).Dimensions  = 'NbPings, NbChannels';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'Heave.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'Yaw';
Info.Signals(end).Dimensions  = 'NbPings, NbChannels';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'Yaw.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'AttitudeTimeTag';
Info.Signals(end).Dimensions  = 'NbPings, NbChannels';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'AttitudeTimeTag.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'DOT';
Info.Signals(end).Dimensions  = 'NbPings, NbChannels';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'DOT.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'NavFixMilliseconds';
Info.Signals(end).Dimensions  = 'NbPings, NbChannels';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'NavFixMilliseconds.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'ComputerClockHour';
Info.Signals(end).Dimensions  = 'NbPings, NbChannels';
Info.Signals(end).Storage     = 'uint8';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'ComputerClockHour.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'ComputerClockMinute';
Info.Signals(end).Dimensions  = 'NbPings, NbChannels';
Info.Signals(end).Storage     = 'uint8';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'ComputerClockMinute.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'ComputerClockSecond';
Info.Signals(end).Dimensions  = 'NbPings, NbChannels';
Info.Signals(end).Storage     = 'uint8';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'ComputerClockSecond.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'ComputerClockHsec';
Info.Signals(end).Dimensions  = 'NbPings, NbChannels';
Info.Signals(end).Storage     = 'uint8';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'ComputerClockHsec.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'FishPositionDeltaX';
Info.Signals(end).Dimensions  = 'NbPings, NbChannels';
Info.Signals(end).Storage     = 'int16';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'FishPositionDeltaX.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'FishPositionDeltaY';
Info.Signals(end).Dimensions  = 'NbPings, NbChannels';
Info.Signals(end).Storage     = 'int16';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'FishPositionDeltaY.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'FishPositionErrorCode';
Info.Signals(end).Dimensions  = 'NbPings, NbChannels';
Info.Signals(end).Storage     = 'uint8';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'FishPositionErrorCode.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

%ReservedSpace non trait�

%% Cr�ation du fichier XML d�crivant la donn�e

nomFicXml = fullfile(FullNomDirRacine, [FullNomDirXml '.xml']);
xml_write(nomFicXml, Info);
flag = exist(nomFicXml, 'file');
if ~flag
    messageErreur(nomFicXml)
    return
end

%% Cr�ation du r�pertoire File Header

FullNomDir = fullfile(FullNomDirRacine, FullNomDirXml);
if ~exist(FullNomDir, 'dir')
    status = mkdir(FullNomDir);
    if ~status
        messageErreur(FullNomDir)
        flag = 0;
        return
    end
end

%% Cr�ation des fichiers binaires des signaux

for i=1:length(Info.Signals)
    flag = writeSignal(FullNomDirRacine, Info.Signals(i), Data.(Info.Signals(i).Name));
    if ~flag
        return
    end
end

flag = 1;
