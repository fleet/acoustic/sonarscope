function [flag, Data] = xtf2ssc_DataType(FullNomDirRacine, DataName, varargin)

% [varargin, flagDisplay] = getPropertyValue(varargin, 'Display', 0);

FullNomDirXml = ['Ssc_' DataName];

%% Lecture du fichier XML d�crivant la donn�e

nomFicXml = fullfile(FullNomDirRacine, ['XTF_' DataName '.xml']);
flag = exist(nomFicXml, 'file');
if ~flag
    special_message('FichierNonExistant', nomFicXml)
    return
end
% edit(nomFicXml)
Datagrams = xml_read(nomFicXml);

[FullNomDirSignal, nomFicSignal] = fileparts(nomFicXml);
FullNomDirSignal = fullfile(FullNomDirSignal, nomFicSignal);


%% Lecture de MagicNumber

[flag, Data.MagicNumber] = Read_BinVariable(Datagrams, FullNomDirSignal, 'MagicNumber', 'uint16');
if ~flag
    return
end

%% Lecture de HeaderType

[flag, Data.HeaderType] = Read_BinVariable(Datagrams, FullNomDirSignal, 'HeaderType', 'uint8');
if ~flag
    return
end

%% Lecture de SubChannelNumber

[flag, Data.SubChannelNumber] = Read_BinVariable(Datagrams, FullNomDirSignal, 'SubChannelNumber', 'char');
if ~flag
    return
end

%% Lecture de NumChanToFollow

[flag, Data.NumChanToFollow] = Read_BinVariable(Datagrams, FullNomDirSignal, 'NumChanToFollow', 'uint16');
if ~flag
    return
end

%% Lecture de Reserved1

% [flag, Reserved1] = Read_BinVariable(Datagrams, FullNomDirSignal, 'Reserved1', 'uint16'); %#ok<NASGU>
% if ~flag
%     return
% end

%% Lecture de NumBytesThisRecord

[flag, Data.NumBytesThisRecord] = Read_BinVariable(Datagrams, FullNomDirSignal, 'NumBytesThisRecord', 'uint32');
if ~flag
    return
end

%########################
% Data and time of the ping

%% Lecture de Year

[flag, Data.Year] = Read_BinVariable(Datagrams, FullNomDirSignal, 'Year', 'uint16');
if ~flag
    return
end

%% Lecture de Month

[flag, Data.Month] = Read_BinVariable(Datagrams, FullNomDirSignal, 'Month', 'uint8');
if ~flag
    return
end

%% Lecture de Day

[flag, Data.Day] = Read_BinVariable(Datagrams, FullNomDirSignal, 'Day', 'uint8');
if ~flag
    return
end

%% Lecture de Hour

[flag, Data.Hour] = Read_BinVariable(Datagrams, FullNomDirSignal, 'Hour', 'uint8');
if ~flag
    return
end

%% Lecture de Minute

[flag, Data.Minute] = Read_BinVariable(Datagrams, FullNomDirSignal, 'Minute', 'uint8');
if ~flag
    return
end

%% Lecture de Second

[flag, Data.Second] = Read_BinVariable(Datagrams, FullNomDirSignal, 'Second', 'uint16');
if ~flag
    return
end

%% Lecture de HSeconds

[flag, Data.HSeconds] = Read_BinVariable(Datagrams, FullNomDirSignal, 'HSeconds', 'uint8');
if ~flag
    return
end

%% Lecture de JulianDay

[flag, Data.JulianDay] = Read_BinVariable(Datagrams, FullNomDirSignal, 'JulianDay', 'uint16');
if ~flag
    return
end

%% Lecture de EventNumber

[flag, Data.EventNumber] = Read_BinVariable(Datagrams, FullNomDirSignal, 'EventNumber', 'uint32');
if ~flag
    return
end

%% Lecture de PingNumber

[flag, Data.PingNumber] = Read_BinVariable(Datagrams, FullNomDirSignal, 'PingNumber', 'uint32');
if ~flag
    return
end

%% Lecture de SoundVelocity

[flag, Data.SoundVelocity] = Read_BinVariable(Datagrams, FullNomDirSignal, 'SoundVelocity', 'single');
if ~flag
    return
end

%% Lecture de OceanTide

[flag, Data.OceanTide] = Read_BinVariable(Datagrams, FullNomDirSignal, 'OceanTide', 'single');
if ~flag
    return
end

%% Lecture de Reserved2

% [flag, Reserved2] = Read_BinVariable(Datagrams, FullNomDirSignal, 'Reserved2', 'uint32'); %#ok<NASGU>
% if ~flag
%     return
% end

%% Lecture de ConductivityFreq

[flag, ConductivityFreq] = Read_BinVariable(Datagrams, FullNomDirSignal, 'ConductivityFreq', 'single');
if ~flag
    return
end

%% Lecture de TemperatureFreq

[flag, Data.TemperatureFreq] = Read_BinVariable(Datagrams, FullNomDirSignal, 'TemperatureFreq', 'single');
if ~flag
    return
end

%% Lecture de PressureFreq

[flag, Data.PressureFreq] = Read_BinVariable(Datagrams, FullNomDirSignal, 'PressureFreq', 'single');
if ~flag
    return
end

%% Lecture de PressureTemp

[flag, Data.PressureTemp] = Read_BinVariable(Datagrams, FullNomDirSignal, 'PressureTemp', 'single');
if ~flag
    return
end

%% Lecture de Conductivity

[flag, Data.Conductivity] = Read_BinVariable(Datagrams, FullNomDirSignal, 'Conductivity', 'single');
if ~flag
    return
end

%% Lecture de WaterTemperature

[flag, Data.WaterTemperature] = Read_BinVariable(Datagrams, FullNomDirSignal, 'WaterTemperature', 'single');
if ~flag
    return
end

%% Lecture de Pressure

[flag, Data.Pressure] = Read_BinVariable(Datagrams, FullNomDirSignal, 'Pressure', 'single');
if ~flag
    return
end


%% Lecture de ComputedSoundVelocity

[flag, Data.ComputedSoundVelocity] = Read_BinVariable(Datagrams, FullNomDirSignal, 'ComputedSoundVelocity', 'single');
if ~flag
    return
end


%##########################
% Param�tres du capteur

%% Lecture de MagX

[flag, Data.MagX] = Read_BinVariable(Datagrams, FullNomDirSignal, 'MagX', 'single');
if ~flag
    return
end

%% Lecture de MagY

[flag, Data.MagY] = Read_BinVariable(Datagrams, FullNomDirSignal, 'MagY', 'single');
if ~flag
    return
end

%% Lecture de MagZ

[flag, Data.MagZ] = Read_BinVariable(Datagrams, FullNomDirSignal, 'MagZ', 'single');
if ~flag
    return
end

%% Lecture de AuxVal1

[flag, Data.AuxVal1] = Read_BinVariable(Datagrams, FullNomDirSignal, 'AuxVal1', 'single');
if ~flag
    return
end

%% Lecture de AuxVal2

[flag, Data.AuxVal2] = Read_BinVariable(Datagrams, FullNomDirSignal, 'AuxVal2', 'single');
if ~flag
    return
end

%% Lecture de AuxVal3

[flag, Data.AuxVal3] = Read_BinVariable(Datagrams, FullNomDirSignal, 'AuxVal3', 'single');
if ~flag
    return
end

%% Lecture de AuxVal4

[flag, Data.AuxVal4] = Read_BinVariable(Datagrams, FullNomDirSignal, 'AuxVal4', 'single');
if ~flag
    return
end

%% Lecture de AuxVal5

[flag, Data.AuxVal5] = Read_BinVariable(Datagrams, FullNomDirSignal, 'AuxVal5', 'single');
if ~flag
    return
end

%% Lecture de AuxVal6

[flag, Data.AuxVal6] = Read_BinVariable(Datagrams, FullNomDirSignal, 'AuxVal6', 'single');
if ~flag
    return
end

%% Lecture de SpeedLog

[flag, Data.SpeedLog] = Read_BinVariable(Datagrams, FullNomDirSignal, 'SpeedLog', 'single');
if ~flag
    return
end

%% Lecture de Turbidity

[flag, Data.Turbidity] = Read_BinVariable(Datagrams, FullNomDirSignal, 'Turbidity', 'single');
if ~flag
    return
end

%% Lecture de ShipSpeed

[flag, Data.ShipSpeed] = Read_BinVariable(Datagrams, FullNomDirSignal, 'ShipSpeed', 'single');
if ~flag
    return
end

%% Lecture de ShipGyro

[flag, Data.ShipGyro] = Read_BinVariable(Datagrams, FullNomDirSignal, 'ShipGyro', 'single');
if ~flag
    return
end

%% Lecture de ShipYcoordinate

[flag, Data.ShipYcoordinate] = Read_BinVariable(Datagrams, FullNomDirSignal, 'ShipYcoordinate', 'double');
if ~flag
    return
end

%% Lecture de ShipXcoordinate

[flag, Data.ShipXcoordinate] = Read_BinVariable(Datagrams, FullNomDirSignal, 'ShipXcoordinate', 'double');
if ~flag
    return
end

%% Lecture de ShipAltitude

[flag, Data.ShipAltitude] = Read_BinVariable(Datagrams, FullNomDirSignal, 'ShipAltitude', 'int32');
if ~flag
    return
end

%% Lecture de ShipDepth

[flag, Data.ShipDepth] = Read_BinVariable(Datagrams, FullNomDirSignal, 'ShipDepth', 'int32');
if ~flag
    return
end

%% Lecture de FixTimeHour

[flag, Data.FixTimeHour] = Read_BinVariable(Datagrams, FullNomDirSignal, 'FixTimeHour', 'uint8');
if ~flag
    return
end

%% Lecture de FixTimeMinute

[flag, Data.FixTimeMinute] = Read_BinVariable(Datagrams, FullNomDirSignal, 'FixTimeMinute', 'uint8');
if ~flag
    return
end

%% Lecture de FixTimeSecond

[flag, Data.FixTimeSecond] = Read_BinVariable(Datagrams, FullNomDirSignal, 'FixTimeSecond', 'uint8');
if ~flag
    return
end

%% Lecture de FixTimeHsecond

[flag, Data.FixTimeHsecond] = Read_BinVariable(Datagrams, FullNomDirSignal, 'FixTimeHsecond', 'uint8');
if ~flag
    return
end

%% Lecture de SensorSpeed

[flag, Data.SensorSpeed] = Read_BinVariable(Datagrams, FullNomDirSignal, 'SensorSpeed', 'single');
if ~flag
    return
end

%% Lecture de KP

[flag, Data.KP] = Read_BinVariable(Datagrams, FullNomDirSignal, 'KP', 'single');
if ~flag
    return
end

%% Lecture de SensorYCoordinate

[flag, Data.SensorYCoordinate] = Read_BinVariable(Datagrams, FullNomDirSignal, 'SensorYCoordinate', 'double');
if ~flag
    return
end

%% Lecture de SensorXCoordinate

[flag, Data.SensorXCoordinate] = Read_BinVariable(Datagrams, FullNomDirSignal, 'SensorXCoordinate', 'double');
if ~flag
    return
end

%##########################
% Tow Cable information

%% Lecture de SonarStatus

[flag, Data.SonarStatus] = Read_BinVariable(Datagrams, FullNomDirSignal, 'SonarStatus', 'uint16');
if ~flag
    return
end

%% Lecture de RangeToFish

[flag, Data.RangeToFish] = Read_BinVariable(Datagrams, FullNomDirSignal, 'RangeToFish', 'uint16');
if ~flag
    return
end

%% Lecture de BearingToFish

[flag, Data.BearingToFish] = Read_BinVariable(Datagrams, FullNomDirSignal, 'BearingToFish', 'uint16');
if ~flag
    return
end

%% Lecture de CableOut

[flag, Data.CableOut] = Read_BinVariable(Datagrams, FullNomDirSignal, 'CableOut', 'uint16');
if ~flag
    return
end

%% Lecture de LayBack

[flag, Data.LayBack] = Read_BinVariable(Datagrams, FullNomDirSignal, 'LayBack', 'single');
if ~flag
    return
end

%% Lecture de CableTension

[flag, Data.CableTension] = Read_BinVariable(Datagrams, FullNomDirSignal, 'CableTension', 'single');
if ~flag
    return
end

%% Lecture de SensorDepth

[flag, Data.SensorDepth] = Read_BinVariable(Datagrams, FullNomDirSignal, 'SensorDepth', 'single');
if ~flag
    return
end

%% Lecture de SensorPrimaryAltitude

[flag, Data.SensorPrimaryAltitude] = Read_BinVariable(Datagrams, FullNomDirSignal, 'SensorPrimaryAltitude', 'single');
if ~flag
    return
end

%% Lecture de SensorAuxAltitude

[flag, Data.SensorAuxAltitude] = Read_BinVariable(Datagrams, FullNomDirSignal, 'SensorAuxAltitude', 'single');
if ~flag
    return
end

%% Lecture de SensorPitch

[flag, Data.SensorPitch] = Read_BinVariable(Datagrams, FullNomDirSignal, 'SensorPitch', 'single');
if ~flag
    return
end

%% Lecture de SensorRoll

[flag, Data.SensorRoll] = Read_BinVariable(Datagrams, FullNomDirSignal, 'SensorRoll', 'single');
if ~flag
    return
end

%% Lecture de SensorHeading

[flag, Data.SensorHeading] = Read_BinVariable(Datagrams, FullNomDirSignal, 'SensorHeading', 'single');
if ~flag
    return
end

%#########################
% Additional attitude data

%% Lecture de Heave

[flag, Data.Heave] = Read_BinVariable(Datagrams, FullNomDirSignal, 'Heave', 'single');
if ~flag
    return
end

%% Lecture de Yaw

[flag, Data.Yaw] = Read_BinVariable(Datagrams, FullNomDirSignal, 'Yaw', 'single');
if ~flag
    return
end

%% Lecture de AttitudeTimeTag

[flag, Data.AttitudeTimeTag] = Read_BinVariable(Datagrams, FullNomDirSignal, 'AttitudeTimeTag', 'int32');
if ~flag
    return
end

%% Lecture de DOT

[flag, Data.DOT] = Read_BinVariable(Datagrams, FullNomDirSignal, 'DOT', 'single');
if ~flag
    return
end

%% Lecture de NavFixMilliseconds

[flag, Data.NavFixMilliseconds] = Read_BinVariable(Datagrams, FullNomDirSignal, 'NavFixMilliseconds', 'uint8');
if ~flag
    return
end

%% Lecture de ComputerClockHour

[flag, Data.ComputerClockHour] = Read_BinVariable(Datagrams, FullNomDirSignal, 'ComputerClockHour', 'uint8');
if ~flag
    return
end

%% Lecture de ComputerClockMinute

[flag, Data.ComputerClockMinute] = Read_BinVariable(Datagrams, FullNomDirSignal, 'ComputerClockMinute', 'uint8');
if ~flag
    return
end

%% Lecture de ComputerClockSecond

[flag, Data.ComputerClockSecond] = Read_BinVariable(Datagrams, FullNomDirSignal, 'ComputerClockSecond', 'uint8');
if ~flag
    return
end

%% Lecture de ComputerClockHsec

[flag, Data.ComputerClockHsec] = Read_BinVariable(Datagrams, FullNomDirSignal, 'ComputerClockHsec', 'uint8');
if ~flag
    return
end

%% Lecture de FishPositionDeltaX

[flag, Data.FishPositionDeltaX] = Read_BinVariable(Datagrams, FullNomDirSignal, 'FishPositionDeltaX', 'int16');
if ~flag
    return
end

%% Lecture de FishPositionDeltaY

[flag, Data.FishPositionDeltaY] = Read_BinVariable(Datagrams, FullNomDirSignal, 'FishPositionDeltaY', 'int16');
if ~flag
    return
end

%% Lecture de FishPositionErrorCode

[flag, Data.FishPositionErrorCode] = Read_BinVariable(Datagrams, FullNomDirSignal, 'FishPositionErrorCode', 'uint8');
if ~flag
    return
end

%% Lecture de ReservedSpace

% [flag, Data.ReservedSpace] = Read_BinVariable(Datagrams, FullNomDirSignal, 'ReservedSpace', 'uint8');
% if ~flag
%     return
% end

%% G�n�ration du XML SonarScope

Info.Title                  = Datagrams.Title; % Entete du fichier
Info.Constructor            = Datagrams.Constructor;
Info.EmModel                = Datagrams.Model;
Info.SystemSerialNumber     = Datagrams.SystemSerialNumber;
Info.TimeOrigin            = Datagrams.TimeOrigin;
Info.Comments               = Datagrams.Comments;

Info.Dimensions.NbSamples     = Datagrams.NbDatagrams;

Info.Signals(1).Name          = 'MagicNumber'; % Set to 0xFACE
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'MagicNumber.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'HeaderType';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint8';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'HeaderType.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'SubChannelNumber';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint8';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'SubChannelNumber.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'NumChanToFollow';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'NumChanToFollow.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'NumBytesThisRecord';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'char';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'NumBytesThisRecord.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

% Info.Signals(end+1).Name      = 'Reserved1';
% Info.Signals(end).Dimensions  = 'NbSamples,2';
% Info.Signals(end).Storage     = 'uint16';
% Info.Signals(end).Unit        = [];
% Info.Signals(end).FileName    = fullfile(FullNomDirXml,'Reserved1.bin');
% Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'Year';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'Year.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'Month';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint8';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'Month.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'Hour';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint8';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'Hour.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');


Info.Signals(end+1).Name      = 'Minute';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint8';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'Minute.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'Second';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint8';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'Second.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');


Info.Signals(end+1).Name      = 'HSeconds';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint8';
Info.Signals(end).Unit        = []; % 0-99
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'HSeconds.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'JulianDay';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = []; % Numbers of days since January 1
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'JulianDay.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'EventNumber';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'EventNumber.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'PingNumber';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'PingNumber.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'SoundVelocity';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'm/s';
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'SoundVelocity.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'OceanTide';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'm';
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'OceanTide.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

% Info.Signals(end+1).Name      = 'Reserved2';
% Info.Signals(end).Dimensions  = 'NbSamples,1';
% Info.Signals(end).Storage     = 'uint16';
% Info.Signals(end).Unit        = [];
% Info.Signals(end).FileName    = fullfile(FullNomDirXml,'Reserved2.bin');
% Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'ComputedSoundVelocity';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'Hz';
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'ComputedSoundVelocity.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'TemperatureFreq';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'Hz';
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'TemperatureFreq.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'PressureFreq';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'Hz';
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'PressureFreq.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'PressureFreq';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'Hz';
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'PressureFreq.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'PressureTemp';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'C';
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'PressureTemp.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'PressureTemp';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'Hz';
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'PressureTemp.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'Conductivity';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'S/m';
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'Conductivity.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'WaterTemperature';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'C';
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'WaterTemperature.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'Pressure';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'psia';
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'Pressure.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'ComputedSoundVelocity';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'm/s';
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'ComputedSoundVelocity.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'MagX';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'mgauss';
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'MagX.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'MagY';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'mgauss';
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'MagY.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'MagZ';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint64';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'MagZ.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'AuxVal1';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'AuxVal1.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'AuxVal2';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'AuxVal2.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'AuxVal3';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'AuxVal3.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'AuxVal4';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'AuxVal4.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'AuxVal5';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'AuxVal5.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'AuxVal6';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'AuxVal6.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'SpeedLog';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'knots';
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'SpeedLog.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'Turbidity';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'V';    % 0 to +5volts
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'Turbidity.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'ShipSpeed';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'knot';
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'ShipSpeed.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'ShipGyro';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'ShipGyro.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'ShipYcoordinate';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'ShipYcoordinate.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'ShipXcoordinate';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'ShipXcoordinate.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'ShipAltitude';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'ShipAltitude.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'ShipDepth';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'ShipDepth.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'FixTimeHour';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint8';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'FixTimeHour.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'FixTimeMinute';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint8';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'FixTimeMinute.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'FixTimeSecond';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint8';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'FixTimeSecond.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'FixTimeHsecond';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint8';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'FixTimeHsecond.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'SensorSpeed';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'knots';
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'SensorSpeed.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'KP';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'KP.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'SensorYCoordinate';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'SensorYCoordinate.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'SensorXCoordinate';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'SensorXCoordinate.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'SonarStatus';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'SonarStatus.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'RangeToFish';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'RangeToFish.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'BearingToFish';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'BearingToFish.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'CableOut';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'CableOut.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'LayBack';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'LayBack.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'CableTension';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'CableTension.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'SensorDepth';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'SensorDepth.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'SensorPrimaryAltitude';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'SensorPrimaryAltitude.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'SensorAuxAltitude';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'SensorAuxAltitude.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'SensorPitch';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'SensorPitch.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'SensorRoll';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'SensorRoll.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'SensorHeading';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'SensorHeading.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'Heave';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'Heave.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'Yaw';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'Yaw.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'AttitudeTimeTag';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'AttitudeTimeTag.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'DOT';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'DOT.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'NavFixMilliseconds';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'NavFixMilliseconds.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'ComputerClockHour';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint8';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'ComputerClockHour.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'ComputerClockMinute';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint8';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'ComputerClockMinute.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'ComputerClockSecond';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint8';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'ComputerClockSecond.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'ComputerClockHsec';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint8';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'ComputerClockHsec.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'FishPositionDeltaX';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'int16';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'FishPositionDeltaX.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'FishPositionDeltaY';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'int16';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'FishPositionDeltaY.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'FishPositionErrorCode';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint8';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile(FullNomDirXml,'FishPositionErrorCode.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

%ReservedSpace non trait�

%% Cr�ation du fichier XML d�crivant la donn�e

nomFicXml = fullfile(FullNomDirRacine, [FullNomDirXml '.xml']);
xml_write(nomFicXml, Info);
flag = exist(nomFicXml, 'file');
if ~flag
    messageErreur(nomFicXml)
    return
end

%% Cr�ation du r�pertoire File Header

FullNomDir = fullfile(FullNomDirRacine, FullNomDirXml);
if ~exist(FullNomDir, 'dir')
    status = mkdir(FullNomDir);
    if ~status
        messageErreur(FullNomDir)
        flag = 0;
        return
    end
end

%% Cr�ation des fichiers binaires des signaux

for i=1:length(Info.Signals)
    flag = writeSignal(FullNomDirRacine, Info.Signals(i), Data.(Info.Signals(i).Name));
    if ~flag
        return
    end
end

flag = 1;
