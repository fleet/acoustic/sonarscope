function [flag, Data] = xtf2ssc_FileHeader(nomDirRacine, varargin)

[varargin, flagDisplay] = getPropertyValue(varargin, 'Display', 0); %#ok<ASGLU>

% ------------------------------------------
% Lecture du fichier XML d�crivant la donn�e

nomFicXml = fullfile(nomDirRacine, 'XTF_FileHeader.xml');
flag = exist(nomFicXml, 'file');
if ~flag
    special_message('FichierNonExistant', nomFicXml)
    return
end
% edit(nomFicXml)
Datagrams = xml_read(nomFicXml);

[nomDirSignal, nomFicSignal] = fileparts(nomFicXml);
nomDirSignal = fullfile(nomDirSignal, nomFicSignal);

for i=1:length(Datagrams.Variables)
    if ~strcmpi(Datagrams.Variables(i).Storage, 'float32')
        Type = Datagrams.Variables(i).Storage;
    else
        Type = 'single';
    end
    [flag, Data.(Datagrams.Variables(i).Name)] = Read_BinVariable(Datagrams, nomDirSignal, Datagrams.Variables(i).Name, Type);
    if ~flag
        return
    end
end

% ------------------------
% Lecture de FileFormat
%{
[flag, Data.(Datagrams.Variables(1).Name)] = Read_BinVariable(Datagrams, nomDirSignal, 'FileFormat', 'uint8');
if ~flag
return
end

% ------------------------
% Lecture de SystemType
[flag, Data.Value{end+1}] = Read_BinVariable(Datagrams, nomDirSignal, 'SystemType', 'uint8');
if ~flag
return
end

% ------------------------
% Lecture de RecordingProgramName
[flag, Data.Value{end+1}] = Read_BinVariable(Datagrams, nomDirSignal, 'RecordingProgramName', 'char');
if ~flag
return
end

% ------------------------
% Lecture de RecordingProgramVersion
[flag, Data.Value{end+1}] = Read_BinVariable(Datagrams, nomDirSignal, 'RecordingProgramVersion', 'char');
if ~flag
return
end

% ------------------------
% Lecture de SonarName
[flag, Data.Value{end+1}] = Read_BinVariable(Datagrams, nomDirSignal, 'SonarName', 'char');
if ~flag
return
end

% ------------------------
% Lecture de SonarName
[flag, Data.Value{end+1}] = Read_BinVariable(Datagrams, nomDirSignal, 'SonarName', 'char');
if ~flag
return
end

% ------------------------
% Lecture de SonarType
[flag, Data.Value{end+1}] = Read_BinVariable(Datagrams, nomDirSignal, 'SonarType', 'uint16');
if ~flag
return
end

% ------------------------
% Lecture de NoteString
[flag, Data.Value{end+1}] = Read_BinVariable(Datagrams, nomDirSignal, 'NoteString', 'char');
if ~flag
return
end


% ------------------------
% Lecture de ThisFileName
[flag, Data.Value{end+1}] = Read_BinVariable(Datagrams, nomDirSignal, 'ThisFileName', 'char');
if ~flag
return
end


% ------------------------
% Lecture de NavUnits
[flag, Data.Value{end+1}] = Read_BinVariable(Datagrams, nomDirSignal, 'NavUnits', 'uint16');
if ~flag
return
end

% ------------------------
% Lecture de NumberOfSonarChannels
[flag, Data.Value{end+1}] = Read_BinVariable(Datagrams, nomDirSignal, 'NumberOfSonarChannels', 'uint16');
if ~flag
return
end

% ------------------------
% Lecture de NumberOfBathymetryChannels
[flag, Data.Value{end+1}] = Read_BinVariable(Datagrams, nomDirSignal, 'NumberOfBathymetryChannels', 'uint16');
if ~flag
return
end

% ------------------------
% Lecture de NumberOfSnippetsChannels
[flag, Data.Value{end+1}] = Read_BinVariable(Datagrams, nomDirSignal, 'NumberOfSnippetsChannels', 'uint8');
if ~flag
return
end

% ------------------------
% Lecture de NumberOfForwardLookChannels
[flag, Data.Value{end+1}] = Read_BinVariable(Datagrams, nomDirSignal, 'NumBerOfForwardLookArrays', 'uint8');
if ~flag
return
end

% ------------------------
% Lecture de NumberOfEchoStrengthChannels
[flag, Data.Value{end+1}] = Read_BinVariable(Datagrams, nomDirSignal, 'NumberOfEchoStrengthChannels', 'uint16');
if ~flag
return
end

% ------------------------
% Lecture de NumberOfInterferometryChannels
[flag, Data.Value{end+1}] = Read_BinVariable(Datagrams, nomDirSignal, 'NumberOfInterferometryChannels', 'uint8');
if ~flag
return
end

% ------------------------
% Lecture de Reserved1
[flag, Data.Value{end+1}] = Read_BinVariable(Datagrams, nomDirSignal, 'Reserved1', 'uint8'); %#ok<NASGU>
if ~flag
return
end

% ------------------------
% Lecture de Reserved2
[flag, Data.Value{end+1}] = Read_BinVariable(Datagrams, nomDirSignal, 'Reserved2', 'uint8'); %#ok<NASGU>
if ~flag
return
end

% ------------------------
% Lecture de ReferencePointHeight
[flag, Data.Value{end+1}] = Read_BinVariable(Datagrams, nomDirSignal, 'ReferencePointHeight', 'single');
if ~flag
return
end

%##########################
% Param�tres de navigation
% ------------------------
% Lecture de ProjectionType
[flag, Data.Value{end+1}] = Read_BinVariable(Datagrams, nomDirSignal, 'ProjectionType', 'uint8');
if ~flag
return
end

% ------------------------
% Lecture de SpheroidType
[flag, Data.Value{end+1}] = Read_BinVariable(Datagrams, nomDirSignal, 'SpheroidType', 'uint8');
if ~flag
return
end

% ------------------------
% Lecture de NavigationLatency
[flag, Data.Value{end+1}] = Read_BinVariable(Datagrams, nomDirSignal, 'NavigationLatency', 'int64');
if ~flag
return
end

% ------------------------
% Lecture de OriginY
[flag, Data.Value{end+1}] = Read_BinVariable(Datagrams, nomDirSignal, 'OriginY', 'single');
if ~flag
return
end

% ------------------------
% Lecture de OriginX
[flag, Data.Value{end+1}] = Read_BinVariable(Datagrams, nomDirSignal, 'OriginX', 'single');
if ~flag
return
end

% ------------------------
% Lecture de NavOffsetY
[flag, Data.Value{end+1}] = Read_BinVariable(Datagrams, nomDirSignal, 'NavOffsetY', 'single');
if ~flag
return
end

% ------------------------
% Lecture de NavOffsetX
[flag, Data.Value{end+1}] = Read_BinVariable(Datagrams, nomDirSignal, 'NavOffsetX', 'single');
if ~flag
return
end

% ------------------------
% Lecture de NavOffsetZ
[flag, Data.Value{end+1}] = Read_BinVariable(Datagrams, nomDirSignal, 'NavOffsetZ', 'single');
if ~flag
return
end

% ------------------------
% Lecture de NavOffsetYaw
[flag, Data.Value{end+1}] = Read_BinVariable(Datagrams, nomDirSignal, 'NavOffsetYaw', 'single');
if ~flag
return
end

% ------------------------
% Lecture de MRUOffsetY
[flag, Data.Value{end+1}] = Read_BinVariable(Datagrams, nomDirSignal, 'MRUOffsetY', 'single');
if ~flag
return
end

% ------------------------
% Lecture de MRUOffsetX
[flag, Data.Value{end+1}] = Read_BinVariable(Datagrams, nomDirSignal, 'MRUOffsetX', 'single');
if ~flag
return
end

% ------------------------
% Lecture de MRUOffsetZ
[flag, Data.Value{end+1}] = Read_BinVariable(Datagrams, nomDirSignal, 'MRUOffsetZ', 'single');
if ~flag
return
end

% ------------------------
% Lecture de MRUOffsetYaw
[flag, Data.Value{end+1}] = Read_BinVariable(Datagrams, nomDirSignal, 'MRUOffsetYaw', 'single');
if ~flag
return
end

% ------------------------
% Lecture de MRUOffsetPitch
[flag, Data.Value{end+1}] = Read_BinVariable(Datagrams, nomDirSignal, 'MRUOffsetPitch', 'single');
if ~flag
return
end

% ------------------------
% Lecture de MRUOffsetRoll
[flag, Data.Value{end+1}] = Read_BinVariable(Datagrams, nomDirSignal, 'MRUOffsetRoll', 'single');
if ~flag
return
end
%}


% ----------------------------
% G�n�ration du XML SonarScope

Info.Title                  = Datagrams.Title; % Entete du fichier
Info.Constructor            = Datagrams.Constructor;
Info.EmModel                = Datagrams.Model;
Info.SystemSerialNumber     = Datagrams.SystemSerialNumber;
Info.TimeOrigin             = Datagrams.TimeOrigin;
Info.Comments               = Datagrams.Comments;

for i=1:length(Datagrams.Variables)
    Info.(Datagrams.Variables(i).Name) = Data.(Datagrams.Variables(i).Name);
end

%{
Info.Dimensions.NbSamples     = Datagrams.NbDatagrams;

Info.Signals(1).Name          = 'FileFormat';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint8';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_FileHeader','FileFormat.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');


Info.Signals(end+1).Name      = 'SystemType';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint8';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_FileHeader','SystemType.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'RecordingProgramName';
Info.Signals(end).Dimensions  = '8';
Info.Signals(end).Storage     = 'char';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_FileHeader','RecordingProgramName.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'RecordingProgramVersion';
Info.Signals(end).Dimensions  = '8';
Info.Signals(end).Storage     = 'char';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_FileHeader','RecordingProgramVersion.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'SonarName';
Info.Signals(end).Dimensions  = '16';
Info.Signals(end).Storage     = 'char';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_FileHeader','SonarName.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'SonarType';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_FileHeader','SonarType.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'NoteString';
Info.Signals(end).Dimensions  = '64';
Info.Signals(end).Storage     = 'char';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_FileHeader','NoteString.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'ThisFileName';
Info.Signals(end).Dimensions  = '64';
Info.Signals(end).Storage     = 'char';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_FileHeader','ThisFileName.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');


Info.Signals(end+1).Name      = 'NavUnits';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_FileHeader','NavUnits.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'NumberOfSonarChannels';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_FileHeader','NumberOfSonarChannels.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');


Info.Signals(end+1).Name      = 'NumberOfBathymetryChannels';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_FileHeader','NumberOfBathymetryChannels.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'NumberOfSnippetsChannels';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint8';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_FileHeader','NumberOfSnippetsChannels.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'NumBerOfForwardLookArrays';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint8';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_FileHeader','NumBerOfForwardLookArrays.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'NumberOfEchoStrengthChannels';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_FileHeader','NumberOfEchoStrengthChannels.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'NumberOfInterferometryChannels';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint8';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_FileHeader','NumberOfInterferometryChannels.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'ReferencePointHeight';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'm';
Info.Signals(end).FileName    = fullfile('Ssc_FileHeader','ReferencePointHeight.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'ProjectionType';
Info.Signals(end).Dimensions  = '12';
Info.Signals(end).Storage     = 'uint8';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_FileHeader','ProjectionType.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'SpheroidType';
Info.Signals(end).Dimensions  = '10';
Info.Signals(end).Storage     = 'uint8';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_FileHeader','SpheroidType.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'NavigationLatency';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint64';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_FileHeader','NavigationLatency.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'OriginY';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'float';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_FileHeader','OriginY.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'OriginX';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'float';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_FileHeader','OriginX.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'NavOffsetY';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'float';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_FileHeader','NavOffsetY.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'NavOffsetX';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'float';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_FileHeader','NavOffsetX.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'NavOffsetZ';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'float';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_FileHeader','NavOffsetZ.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'NavOffsetYaw';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'float';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_FileHeader','NavOffsetYaw.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'MRUOffsetY';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'float';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_FileHeader','MRUOffsetY.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'MRUOffsetX';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'float';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_FileHeader','MRUOffsetX.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'MRUOffsetZ';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'float';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_FileHeader','MRUOffsetZ.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'MRUOffsetYaw';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'float';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_FileHeader','MRUOffsetYaw.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'MRUOffsetPitch';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'float';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_FileHeader','MRUOffsetPitch.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'MRUOffsetRoll';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'float';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_FileHeader','MRUOffsetRoll.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');
%}
%% Cr�ation du fichier XML d�crivant la donn�e

nomFicXml = fullfile(nomDirRacine, 'Ssc_FileHeader.xml');
xml_write(nomFicXml, Info);
flag = exist(nomFicXml, 'file');
if ~flag
    messageErreur(nomFicXml)
    return
end

%% Cr�ation du r�pertoire File Header

nomDirFileHeader = fullfile(nomDirRacine, 'Ssc_FileHeader');
if ~exist(nomDirFileHeader, 'dir')
    status = mkdir(nomDirFileHeader);
    if ~status
        messageErreur(nomDirFileHeader)
        flag = 0;
        return
    end
end

%% Cr�ation des fichiers binaires des signaux

% for i=1:length(Info.Signals)
%     flag = writeSignal(nomDirRacine, Info.Signals(i), Data.(Info.Signals(i).Name));
%     if ~flag
%         return
%     end
% end

flag = 1;
