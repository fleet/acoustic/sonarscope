function [flag, Data] = xtf2ssc_NotesHeader(nomDirRacine, varargin)

[varargin, flagDisplay] = getPropertyValue(varargin, 'Display', 0); %#ok<ASGLU>

% ------------------------------------------
% Lecture du fichier XML d�crivant la donn�e

nomFicXml = fullfile(nomDirRacine, 'XTF_NotesHeader.xml');
flag = exist(nomFicXml, 'file');
if ~flag
    special_message('FichierNonExistant', nomFicXml)
    return
end
% edit(nomFicXml)
Datagrams = xml_read(nomFicXml);

[nomDirSignal, nomFicSignal] = fileparts(nomFicXml);
nomDirSignal = fullfile(nomDirSignal, nomFicSignal);


% ------------------------
% Lecture de MagicNumber
[flag, Data.MagicNumber] = Read_BinVariable(Datagrams, nomDirSignal, 'MagicNumber', 'uint16');
if ~flag
    return
end

% ------------------------
% Lecture de HeaderType
[flag, Data.HeaderType] = Read_BinVariable(Datagrams, nomDirSignal, 'HeaderType', 'uint8');
if ~flag
    return
end

% ------------------------
% Lecture de SubChannelNumber
[flag, Data.SubChannelNumber] = Read_BinVariable(Datagrams, nomDirSignal, 'SubChannelNumber', 'char');
if ~flag
    return
end

% ------------------------
% Lecture de NumChanToFollow
[flag, Data.NumChanToFollow] = Read_BinVariable(Datagrams, nomDirSignal, 'NumChanToFollow', 'uint16');
if ~flag
    return
end

% ------------------------
% Lecture de Reserved1
% [flag, Reserved1] = Read_BinVariable(Datagrams, nomDirSignal, 'Reserved1', 'uint16'); %#ok<NASGU>
% if ~flag
%     return
% end

% ------------------------
% Lecture de NumBytesThisRecord
[flag, Data.NumBytesThisRecord] = Read_BinVariable(Datagrams, nomDirSignal, 'NumBytesThisRecord', 'uint32');
if ~flag
    return
end

%########################
% Data and time of the ping
% ------------------------
% Lecture de Year
[flag, Data.Year] = Read_BinVariable(Datagrams, nomDirSignal, 'Year', 'uint16');
if ~flag
    return
end

% ------------------------
% Lecture de Month
[flag, Data.Month] = Read_BinVariable(Datagrams, nomDirSignal, 'Month', 'uint8');
if ~flag
    return
end

% ------------------------
% Lecture de Day
[flag, Data.Day] = Read_BinVariable(Datagrams, nomDirSignal, 'Day', 'uint8');
if ~flag
    return
end

% ------------------------
% Lecture de Hour
[flag, Data.Hour] = Read_BinVariable(Datagrams, nomDirSignal, 'Hour', 'uint8');
if ~flag
    return
end


% ------------------------
% Lecture de Minute
[flag, Data.Minute] = Read_BinVariable(Datagrams, nomDirSignal, 'Minute', 'uint8');
if ~flag
    return
end

% ------------------------
% Lecture de Second
[flag, Data.Second] = Read_BinVariable(Datagrams, nomDirSignal, 'Second', 'uint16');
if ~flag
    return
end

% ------------------------
% Lecture de ReservedBytes
% [flag, Data.ReservedBytes] = Read_BinVariable(Datagrams, nomDirSignal, 'ReservedBytes', 'uint8');
% if ~flag
%     return
% end

% ------------------------
% Lecture de NotesText
[flag, Data.NotesText] = Read_BinVariable(Datagrams, nomDirSignal, 'NotesText', 'uint16');
if ~flag
    return
end

% ------------------------
% Lecture de ReservedSpace
% [flag, Data.ReservedSpace] = Read_BinVariable(Datagrams, nomDirSignal, 'ReservedSpace', 'uint8');
% if ~flag
%     return
% end

% ----------------------------
% G�n�ration du XML SonarScope

Info.Title                  = Datagrams.Title; % Entete du fichier
Info.Constructor            = Datagrams.Constructor;
Info.EmModel                = Datagrams.Model;
Info.SystemSerialNumber     = Datagrams.SystemSerialNumber;
Info.TimeOrigin            = Datagrams.TimeOrigin;
Info.Comments               = Datagrams.Comments;

Info.Dimensions.NbSamples     = Datagrams.NbDatagrams;

Info.Signals(1).Name          = 'MagicNumber'; % Set to 0xFACE
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_NotesHeader','MagicNumber.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'HeaderType';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint8';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_NotesHeader','HeaderType.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'SubChannelNumber';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint8';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_NotesHeader','SubChannelNumber.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'NumChanToFollow';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_NotesHeader','NumChanToFollow.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'NumBytesThisRecord';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'char';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_NotesHeader','NumBytesThisRecord.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

% Info.Signals(end+1).Name      = 'Reserved1';
% Info.Signals(end).Dimensions  = 'NbSamples,2';
% Info.Signals(end).Storage     = 'uint16';
% Info.Signals(end).Unit        = [];
% Info.Signals(end).FileName    = fullfile('Ssc_NotesHeader','Reserved1.bin');
% Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'Year';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_NotesHeader','Year.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'Month';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint8';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_NotesHeader','Month.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'Hour';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint8';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_NotesHeader','Hour.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');


Info.Signals(end+1).Name      = 'Minute';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint8';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_NotesHeader','Minute.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'Second';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint8';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_NotesHeader','Second.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

% Info.Signals(end+1).Name      = 'ReservedBytes';
% Info.Signals(end).Dimensions  = 'NbSamples,35';
% Info.Signals(end).Storage     = 'uint8';
% Info.Signals(end).Unit        = []; % 0-99
% Info.Signals(end).FileName    = fullfile('Ssc_NotesHeader','ReservedBytes.bin');
% Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'NotesText';
Info.Signals(end).Dimensions  = 'NbSamples,256-56';
Info.Signals(end).Storage     = 'char';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_NotesHeader','NotesText.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');


%% Cr�ation du fichier XML d�crivant la donn�e

nomFicXml = fullfile(nomDirRacine, 'Ssc_NotesHeader.xml');
xml_write(nomFicXml, Info);
flag = exist(nomFicXml, 'file');
if ~flag
    messageErreur(nomFicXml)
    return
end

%% Cr�ation du r�pertoire File Header

nomDirNotesHeader = fullfile(nomDirRacine, 'Ssc_NotesHeader');
if ~exist(nomDirNotesHeader, 'dir')
    status = mkdir(nomDirNotesHeader);
    if ~status
        messageErreur(nomDirNotesHeader)
        flag = 0;
        return
    end
end

%% Cr�ation des fichiers binaires des signaux

for i=1:length(Info.Signals)
    flag = writeSignal(nomDirRacine, Info.Signals(i), Data.(Info.Signals(i).Name));
    if ~flag
        return
    end
end

flag = 1;
