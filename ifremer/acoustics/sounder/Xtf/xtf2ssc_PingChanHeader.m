function flag = xtf2ssc_PingChanHeader(nomDirRacine, iNumChannel, varargin)

% [varargin, flagDisplay] = getPropertyValue(varargin, 'Display', 0);

%% Lecture du fichierf XML d�crivant la donn�e

nomFicXml = fullfile(nomDirRacine, ['XTF_PingChanHeader_' num2str(iNumChannel) '.xml']);
flag = exist(nomFicXml, 'file');
if ~flag
    special_message('FichierNonExistant', nomFicXml)
    return
end
% edit(nomFicXml)
Datagrams = xml_read(nomFicXml);

[nomDirSignal, nomFicSignal] = fileparts(nomFicXml);
nomDirSignal = fullfile(nomDirSignal, nomFicSignal);

%% Lecture de ChannelNumber

[flag, Data.ChannelNumber] = Read_BinVariable(Datagrams, nomDirSignal, 'ChannelNumber', 'uint16');
if ~flag
    return
end

%% Lecture de DownSampleModel

[flag, Data.DownSampleModel] = Read_BinVariable(Datagrams, nomDirSignal, 'DownSampleModel', 'uint16');
if ~flag
    return
end

%% Lecture de SlantRange

[flag, Data.SlantRange] = Read_BinVariable(Datagrams, nomDirSignal, 'SlantRange', 'single');
if ~flag
    return
end

%% Lecture de GroundRange

[flag, Data.GroundRange] = Read_BinVariable(Datagrams, nomDirSignal, 'GroundRange', 'single');

if ~flag
    return
end

%% Lecture de TimeDelay

[flag, Data.TimeDelay] = Read_BinVariable(Datagrams, nomDirSignal, 'TimeDelay', 'single');
if ~flag
    return
end

%% Lecture de TimeDuration

[flag, Data.TimeDuration] = Read_BinVariable(Datagrams, nomDirSignal, 'TimeDuration', 'single');

if ~flag
    return
end

%% Lecture de SecondsPerPing

[flag, Data.SecondsPerPing] = Read_BinVariable(Datagrams, nomDirSignal, 'SecondsPerPing', 'single');
if ~flag
    return
end

%% Lecture de ProcessingFlags

[flag, Data.ProcessingFlags] = Read_BinVariable(Datagrams, nomDirSignal, 'ProcessingFlags', 'uint16');
if ~flag
    return
end

%% Lecture de Frequency

[flag, Data.Frequency] = Read_BinVariable(Datagrams, nomDirSignal, 'Frequency', 'uint16');
if ~flag
    return
end

%% Lecture de InitialGainCode

[flag, Data.InitialGainCode] = Read_BinVariable(Datagrams, nomDirSignal, 'InitialGainCode', 'uint16');
if ~flag
    return
end

%% Lecture de GainCode

[flag, Data.GainCode] = Read_BinVariable(Datagrams, nomDirSignal, 'GainCode', 'uint16');
if ~flag
    return
end

%% Lecture de BandWith

[flag, Data.BandWith] = Read_BinVariable(Datagrams, nomDirSignal, 'BandWith', 'uint16');
if ~flag
    return
end

%% Lecture de ContactNumber

[flag, Data.ContactNumber] = Read_BinVariable(Datagrams, nomDirSignal, 'ContactNumber', 'uint32');
if ~flag
    return
end

%% Lecture de ContactClassification

[flag, Data.ContactClassification] = Read_BinVariable(Datagrams, nomDirSignal, 'ContactClassification', 'uint8');
if ~flag
    return
end

%% Lecture de ContactSubNumber

[flag, Data.ContactSubNumber] = Read_BinVariable(Datagrams, nomDirSignal, 'ContactSubNumber', 'uint16');
if ~flag
    return
end

%% Lecture de ContactType

[flag, Data.ContactType] = Read_BinVariable(Datagrams, nomDirSignal, 'ContactType', 'uint8');
if ~flag
    return
end

%% Lecture de NumSamples

[flag, Data.NumSamples] = Read_BinVariable(Datagrams, nomDirSignal, 'NumSamples', 'uint32');
N = sum(Data.NumSamples);
maxNbSamples = max(Data.NumSamples);
if ~flag
    return
end

%% Lecture de MillivoltScale

[flag, Data.MillivoltScale] = Read_BinVariable(Datagrams, nomDirSignal, 'MillivoltScale', 'uint16');
if ~flag
    return
end

%% Lecture de ContactTimeOffTrack

[flag, Data.ContactTimeOffTrack] = Read_BinVariable(Datagrams, nomDirSignal, 'ContactTimeOffTrack', 'single');
if ~flag
    return
end

%% Lecture de ContactCloseNumber

[flag, Data.ContactCloseNumber] = Read_BinVariable(Datagrams, nomDirSignal, 'ContactCloseNumber', 'uint8');
if ~flag
    return
end

%% Lecture de Reserved2

[flag, Data.Reserved2] = Read_BinVariable(Datagrams, nomDirSignal, 'Reserved2', 'uint8');
if ~flag
    return
end

%% Lecture de FixedVSOP

[flag, Data.FixedVSOP] = Read_BinVariable(Datagrams, nomDirSignal, 'FixedVSOP', 'single');
if ~flag
    return
end

%% Lecture de Weight

[flag, Data.Weight] = Read_BinVariable(Datagrams, nomDirSignal, 'Weight', 'single');
if ~flag
    return
end

%% Lecture de ReserveSpace

[flag, Data.ReserveSpace] = Read_BinVariable(Datagrams, nomDirSignal, 'ReserveSpace', 'single');
if ~flag
    return
end

%% Lecture de Imagery

[flag, Imagery] = Read_BinTable(Datagrams, nomDirSignal, 'Imagery', 'single', N);
if ~flag
    return
end

Data.Imagery = NaN(Datagrams.NbDatagrams, maxNbSamples, class(Imagery));
iDeb = 0;
for k=1:Datagrams.NbDatagrams
    sub = 1:Data.NumSamples(k);
    Data.Imagery(k,sub) = Imagery(iDeb+sub);
    iDeb = iDeb + Data.NumSamples(k);
end
% Filtrage de quelques �chantillons incoh�rents (Fichiers RECOSOM2 :
% Charline).
[flag, Data.Imagery] = filterIncoherentSamples(Data.Imagery);
clear Imagery sub

%% G�n�ration du XML SonarScope

Info.Title                  = Datagrams.Title;
Info.Constructor            = Datagrams.Constructor;
Info.EmModel                = Datagrams.Model;
Info.SystemSerialNumber     = Datagrams.SystemSerialNumber;
Info.TimeOrigin             = Datagrams.TimeOrigin;
Info.Comments               = Datagrams.Comments;

Info.Dimensions.nbPings     = Datagrams.NbDatagrams;
Info.Dimensions.nbSamples   = maxNbSamples;

for i=1:length(Datagrams.Variables)
    Info.Signals(i).Name        = Datagrams.Variables(i).Name;
    Info.Signals(i).Dimensions  = 'nbPings,1';
    Info.Signals(i).Storage     = getMatlabDataType(Datagrams.Variables(i).Storage);
    Info.Signals(i).Unit        = [];
    Info.Signals(i).FileName    = fullfile(['Ssc_PingChanHeader_' num2str(iNumChannel)], [Datagrams.Variables(i).Name '.bin']);
    Info.Signals(i).Tag         = verifKeyWord('TODO');
end

for i=1:length(Datagrams.Tables)
    Info.Images(i).Name        = Datagrams.Tables(i).Name;
    Info.Images(i).Dimensions  = 'nbPings,nbSamples';
    Info.Images(i).Storage     = getMatlabDataType(Datagrams.Tables(i).Storage);
    Info.Images(i).Unit        = [];
    Info.Images(i).FileName    = fullfile(['Ssc_PingChanHeader_' num2str(iNumChannel)], [Datagrams.Tables(i).Name '.bin']);
    Info.Images(i).Tag         = verifKeyWord('TODO');
end

%% Cr�ation du fichier XML d�crivant la donn�e

% Les donn�es Ssc sont remont�es � la racine du r�pertoire.
nomDirRacine = strrep(nomDirRacine, '\XTF_SideScan', '');
nomFicXml = fullfile(nomDirRacine, ['Ssc_PingChanHeader_' num2str(iNumChannel) '.xml']);
xml_write(nomFicXml, Info);
flag = exist(nomFicXml, 'file');
if ~flag
    messageErreur(nomFicXml)
    return
end

%% Cr�ation du r�pertoire File Channel Info

nomDirSscData = fullfile(nomDirRacine, ['Ssc_PingChanHeader_' num2str(iNumChannel)]);
if ~exist(nomDirSscData, 'dir')
    status = mkdir(nomDirSscData);
    if ~status
        messageErreur(nomDirSscData)
        flag = 0;
        return
    end
end

%% Cr�ation des fichiers binaires des signaux

for i=1:length(Info.Signals)
    flag = writeSignal(nomDirRacine, Info.Signals(i), Data.(Info.Signals(i).Name));
    if ~flag
        return
    end
end

%% Cr�ation des fichiers binaires des images

for i=1:length(Info.Images)
    flag = writeImage(nomDirRacine, Info.Images(i), Data.(Info.Images(i).Name));
    if ~flag
        return
    end
end

flag = 1;
