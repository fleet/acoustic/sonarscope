function [flag, Data] = xtf2ssc_QPSMultiBeamHeader(nomDirRacine, NumChannel, NbBeams, varargin)

[varargin, flagDisplay] = getPropertyValue(varargin, 'Display', 0); %#ok<ASGLU>

%% Lecture du fichier XML d�crivant la donn�e

nomFicXml = fullfile(nomDirRacine, ['XTF_QPSMultiBeamHeader_' num2str(NumChannel-1) '.xml']);
flag = exist(nomFicXml, 'file');
if ~flag
    special_message('FichierNonExistant', nomFicXml)
    return
end
% edit(nomFicXml)
Datagrams   = xml_read(nomFicXml);
NbPings     = Datagrams.NbDatagrams/NbBeams;

[nomDirSignal, nomFicSignal] = fileparts(nomFicXml);
% 2�me extraction pour filtrer le XTF_Bathy
[nomDirTarget, ~, ~] = fileparts(nomDirSignal);
nomDirTarget = fullfile(nomDirTarget, 'Ssc_Bathy');

nomDirSignal = fullfile(nomDirSignal, nomFicSignal);

nomDirXml = ['Ssc_QPSMultiBeamHeader_' num2str(NumChannel-1)];

%% Lecture de Id �quivalent au n� de faisceau

% [flag, Data.Id] = Read_BinVariable(Datagrams, nomDirSignal, 'Id', 'int32');
% if ~flag
%     return
% end

%% Lecture de Intensity

[flag, Data.Intensity] = Read_BinVariable(Datagrams, nomDirSignal, 'Intensity', 'double');
if ~flag
    return
end

%% Lecture de Quality

[flag, Data.Quality] = Read_BinVariable(Datagrams, nomDirSignal, 'Quality', 'int32');
if ~flag
    return
end

%% Lecture de TwoWayTravelTime

[flag, Data.TwoWayTravelTime] = Read_BinVariable(Datagrams, nomDirSignal, 'TwoWayTravelTime', 'double');
if ~flag
    return
end

%% Lecture de DeltaTime

[flag, Data.DeltaTime] = Read_BinVariable(Datagrams, nomDirSignal, 'DeltaTime', 'double');
if ~flag
    return
end

%% Lecture de BeamAngle

[flag, Data.BeamAngle] = Read_BinVariable(Datagrams, nomDirSignal, 'BeamAngle', 'double');
if ~flag
    return
end

%% Lecture de TiltAngle

[flag, Data.TiltAngle] = Read_BinVariable(Datagrams, nomDirSignal, 'TiltAngle', 'double');
if ~flag
    return
end

%% Lecture de Reserved
% [flag, Data.Reserved] = Read_BinVariable(Datagrams, nomDirSignal, 'Reserved', 'single');
% if ~flag
%     return
% end

%% G�n�ration du XML SonarScope

Info.Title                  = Datagrams.Title; % Entete du fichier
Info.Constructor            = Datagrams.Constructor;
Info.EmModel                = Datagrams.Model;
Info.SystemSerialNumber     = Datagrams.SystemSerialNumber;
Info.TimeOrigin             = Datagrams.TimeOrigin;
Info.Comments               = Datagrams.Comments;

Info.Dimensions.NbPings     = NbPings;
Info.Dimensions.NbBeams     = NbBeams;

% Id non cr�� car �quivalent au n� de faisceau.
% Info.Images(1).Name          = 'Id'; % Set to 0xFACE
% Info.Images(end).Dimensions  = 'NbPings, NbBeams';
% Info.Images(end).Storage     = 'int32';
% Info.Images(end).Unit        = [];
% Info.Images(end).FileName    = fullfile(nomDirXml,'Id.bin');
% Info.Images(end).Tag         = verifKeyWord('TODO');

Info.Images(1).Name          = 'Intensity';
Info.Images(end).Dimensions  = 'NbPings, NbBeams';
Info.Images(end).Storage     = 'double';
Info.Images(end).Unit        = [];
Info.Images(end).FileName    = fullfile(nomDirXml,'Intensity.bin');
Info.Images(end).Tag         = verifKeyWord('TODO');

Info.Images(end+1).Name      = 'Quality';
Info.Images(end).Dimensions  = 'NbPings, NbBeams';
Info.Images(end).Storage     = 'int32';
Info.Images(end).Unit        = [];
Info.Images(end).FileName    = fullfile(nomDirXml,'Quality.bin');
Info.Images(end).Tag         = verifKeyWord('TODO');

Info.Images(end+1).Name      = 'TwoWayTravelTime';
Info.Images(end).Dimensions  = 'NbPings, NbBeams';
Info.Images(end).Storage     = 'double';
Info.Images(end).Unit        = [];
Info.Images(end).FileName    = fullfile(nomDirXml,'TwoWayTravelTime .bin');
Info.Images(end).Tag         = verifKeyWord('TODO');

Info.Images(end+1).Name      = 'DeltaTime';
Info.Images(end).Dimensions  = 'NbPings, NbBeams';
Info.Images(end).Storage     = 'double';
Info.Images(end).Unit        = [];
Info.Images(end).FileName    = fullfile(nomDirXml,'DeltaTime.bin');
Info.Images(end).Tag         = verifKeyWord('TODO');

Info.Images(end+1).Name      = 'BeamAngle';
Info.Images(end).Dimensions  = 'NbPings, NbBeams';
Info.Images(end).Storage     = 'double';
Info.Images(end).Unit        = [];
Info.Images(end).FileName    = fullfile(nomDirXml,'BeamAngle.bin');
Info.Images(end).Tag         = verifKeyWord('TODO');

Info.Images(end+1).Name      = 'TiltAngle';
Info.Images(end).Dimensions  = 'NbPings, NbBeams';
Info.Images(end).Storage     = 'double';
Info.Images(end).Unit        = [];
Info.Images(end).FileName    = fullfile(nomDirXml,'TiltAngle.bin');
Info.Images(end).Tag         = verifKeyWord('TODO');



%% Cr�ation du fichier XML d�crivant la donn�e
nomFicXml = fullfile(nomDirTarget, [nomDirXml '.xml']);
xml_write(nomFicXml, Info);
flag = exist(nomFicXml, 'file');
if ~flag
    messageErreur(nomFicXml)
    return
end

%% Cr�ation du r�pertoire Ssc_QPSMultiBeamHeader_

nomDirQPSMultiBeamHeader = fullfile(nomDirTarget, ['Ssc_QPSMultiBeamHeader_' num2str(NumChannel-1)]);
if ~exist(nomDirQPSMultiBeamHeader, 'dir')
    status = mkdir(nomDirQPSMultiBeamHeader);
    if ~status
        messageErreur(nomDirQPSMultiBeamHeader)
        flag = 0;
        return
    end
end

%% Cr�ation des fichiers binaires des images

for k=1:length(Info.Images)
    flag = writeImage(nomDirTarget, Info.Images(k), Data.(Info.Images(k).Name));
    if ~flag
        return
    end
end

flag = 1;
