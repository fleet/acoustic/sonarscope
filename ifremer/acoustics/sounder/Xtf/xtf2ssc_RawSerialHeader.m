function [flag, Data] = xtf2ssc_RawSerialHeader(nomDirRacine, varargin)

[varargin, flagDisplay] = getPropertyValue(varargin, 'Display', 0); %#ok<ASGLU>

% ------------------------------------------
% Lecture du fichier XML d�crivant la donn�e

nomFicXml = fullfile(nomDirRacine, 'XTF_RawSerialHeader.xml');
flag = exist(nomFicXml, 'file');
if ~flag
    special_message('FichierNonExistant', nomFicXml)
    return
end
% edit(nomFicXml)
Datagrams = xml_read(nomFicXml);

[nomDirSignal, nomFicSignal] = fileparts(nomFicXml);
nomDirSignal = fullfile(nomDirSignal, nomFicSignal);


% ------------------------
% Lecture de MagicNumber
[flag, Data.MagicNumber] = Read_BinVariable(Datagrams, nomDirSignal, 'MagicNumber', 'uint16');
if ~flag
    return
end

% ------------------------
% Lecture de HeaderType
[flag, Data.HeaderType] = Read_BinVariable(Datagrams, nomDirSignal, 'HeaderType', 'uint8');
if ~flag
    return
end

% ------------------------
% Lecture de SerialPort
[flag, Data.SerialPort] = Read_BinVariable(Datagrams, nomDirSignal, 'SerialPort', 'uint8');
if ~flag
    return
end

% ------------------------
% Lecture de NumChanToFollow
[flag, Data.NumChanToFollow] = Read_BinVariable(Datagrams, nomDirSignal, 'NumChanToFollow', 'uint16');
if ~flag
    return
end

% ------------------------
% Lecture de NumBytesThisRecord
[flag, Data.NumBytesThisRecord] = Read_BinVariable(Datagrams, nomDirSignal, 'NumBytesThisRecord', 'uint32');
if ~flag
    return
end

% ------------------------
% Lecture de Year
[flag, Data.Year] = Read_BinVariable(Datagrams, nomDirSignal, 'Year', 'uint16');
if ~flag
    return
end

% ------------------------
% Lecture de Month
[flag, Data.Month] = Read_BinVariable(Datagrams, nomDirSignal, 'Month', 'uint8');
if ~flag
    return
end

% ------------------------
% Lecture de Day
[flag, Data.Day] = Read_BinVariable(Datagrams, nomDirSignal, 'Day', 'uint8');
if ~flag
    return
end

% ------------------------
% Lecture de Hour
[flag, Data.Hour] = Read_BinVariable(Datagrams, nomDirSignal, 'Hour', 'uint8');
if ~flag
    return
end


% ------------------------
% Lecture de Minute
[flag, Data.Minute] = Read_BinVariable(Datagrams, nomDirSignal, 'Minute', 'uint8');
if ~flag
    return
end

% ------------------------
% Lecture de Second
[flag, Data.Second] = Read_BinVariable(Datagrams, nomDirSignal, 'Second', 'uint16');
if ~flag
    return
end

% ------------------------
% Lecture de HSeconds
[flag, Data.HSeconds] = Read_BinVariable(Datagrams, nomDirSignal, 'HSeconds', 'uint8');
if ~flag
    return
end

% ------------------------
% Lecture de JulianDay
[flag, Data.JulianDay] = Read_BinVariable(Datagrams, nomDirSignal, 'JulianDay', 'uint16');
if ~flag
    return
end

% ------------------------
% Lecture de TimeTag
[flag, Data.TimeTag] = Read_BinVariable(Datagrams, nomDirSignal, 'TimeTag', 'uint32');
if ~flag
    return
end

% ------------------------
% Lecture de StringSize
[flag, Data.StringSize] = Read_BinVariable(Datagrams, nomDirSignal, 'StringSize', 'uint32');
if ~flag
    return
end

% ------------------------
% Lecture de RawAsciiData
[flag, Data.RawAsciiData] = Read_BinVariable(Datagrams, nomDirSignal, 'RawAsciiData', 'char');
if ~flag
    return
end

% ----------------------------
% G�n�ration du XML SonarScope

Info.Title                  = Datagrams.Title; % Entete du fichier
Info.Constructor            = Datagrams.Constructor;
Info.EmModel                = Datagrams.Model;
Info.SystemSerialNumber     = Datagrams.SystemSerialNumber;
Info.TimeOrigin            = Datagrams.TimeOrigin;
Info.Comments               = Datagrams.Comments;

Info.Dimensions.NbSamples     = Datagrams.NbDatagrams;

Info.Signals(1).Name          = 'MagicNumber'; % Set to 0xFACE
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_RawSerialHeader','MagicNumber.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'HeaderType';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint8';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_RawSerialHeader','HeaderType.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'SerialPort';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint8';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_RawSerialHeader','SerialPort.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'NumChanToFollow';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_RawSerialHeader','NumChanToFollow.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'NumBytesThisRecord';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_RawSerialHeader','NumBytesThisRecord.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'Year';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_RawSerialHeader','Year.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'Month';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint8';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_RawSerialHeader','Month.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'Hour';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint8';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_RawSerialHeader','Hour.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');


Info.Signals(end+1).Name      = 'Minute';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint8';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_RawSerialHeader','Minute.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'Second';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint8';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_RawSerialHeader','Second.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'HSeconds';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint8';
Info.Signals(end).Unit        = []; % 0-99
Info.Signals(end).FileName    = fullfile('Ssc_RawSerialHeader','HSeconds.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'JulianDay';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = []; % Numbers of days since January 1
Info.Signals(end).FileName    = fullfile('Ssc_RawSerialHeader','JulianDay.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'TimeTag';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint32';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_RawSerialHeader','TimeTag.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'StringSize';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_RawSerialHeader','StringSize.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'RawAsciiData';
Info.Signals(end).Dimensions  = 'NbSamples,64-30';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_RawSerialHeader','RawAsciiData.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');



%% Cr�ation du fichier XML d�crivant la donn�e

nomFicXml = fullfile(nomDirRacine, 'Ssc_RawSerialHeader.xml');
xml_write(nomFicXml, Info);
flag = exist(nomFicXml, 'file');
if ~flag
    messageErreur(nomFicXml)
    return
end

%% Cr�ation du r�pertoire File Header

nomDirRawSerialHeader = fullfile(nomDirRacine, 'Ssc_RawSerialHeader');
if ~exist(nomDirRawSerialHeader, 'dir')
    status = mkdir(nomDirRawSerialHeader);
    if ~status
        messageErreur(nomDirRawSerialHeader)
        flag = 0;
        return
    end
end

%% Cr�ation des fichiers binaires des signaux

for i=1:length(Info.Signals)
    flag = writeSignal(nomDirRacine, Info.Signals(i), Data.(Info.Signals(i).Name));
    if ~flag
        return
    end
end

flag = 1;
