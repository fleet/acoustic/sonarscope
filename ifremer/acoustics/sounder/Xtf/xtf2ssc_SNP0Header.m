function [flag, Data] = xtf2ssc_SNP0Header(nomDirRacine, varargin)

[varargin, flagDisplay] = getPropertyValue(varargin, 'Display', 0); %#ok<ASGLU>

% ------------------------------------------
% Lecture du fichier XML d�crivant la donn�e

nomFicXml = fullfile(nomDirRacine, 'XTF_SNP0Header.xml');
flag = exist(nomFicXml, 'file');
if ~flag
    special_message('FichierNonExistant', nomFicXml)
    return
end
% edit(nomFicXml)
Datagrams = xml_read(nomFicXml);

[nomDirSignal, nomFicSignal] = fileparts(nomFicXml);
nomDirSignal = fullfile(nomDirSignal, nomFicSignal);


% ------------------------
% Lecture de ID
[flag, Data.ID] = Read_BinVariable(Datagrams, nomDirSignal, 'ID', 'uint64');
if ~flag
    return
end

% ------------------------
% Lecture de HeaderSize
[flag, Data.HeaderSize] = Read_BinVariable(Datagrams, nomDirSignal, 'HeaderSize', 'uint16');
if ~flag
    return
end

% ------------------------
% Lecture de DataSize
[flag, Data.DataSize] = Read_BinVariable(Datagrams, nomDirSignal, 'DataSize', 'uint16');
if ~flag
    return
end

% ------------------------
% Lecture de PingNumber
[flag, Data.PingNumber] = Read_BinVariable(Datagrams, nomDirSignal, 'PingNumber', 'uint64');
if ~flag
    return
end

% ------------------------
% Lecture de Seconds
[flag, Data.Seconds] = Read_BinVariable(Datagrams, nomDirSignal, 'Seconds', 'uint64');
if ~flag
    return
end

% ------------------------
% Lecture de Millisecs
[flag, Data.Millisecs] = Read_BinVariable(Datagrams, nomDirSignal, 'Millisecs', 'uint64');
if ~flag
    return
end

% ------------------------
% Lecture de Latency
[flag, Data.Latency] = Read_BinVariable(Datagrams, nomDirSignal, 'Latency', 'uint16');
if ~flag
    return
end

% ------------------------
% Lecture de SonarId
[flag, Data.SonarId] = Read_BinVariable(Datagrams, nomDirSignal, 'SonarId', 'uint16');
if ~flag
    return
end

% ------------------------
% Lecture de SonarModel
[flag, Data.SonarModel] = Read_BinVariable(Datagrams, nomDirSignal, 'SonarModel', 'uint16');
if ~flag
    return
end

% ------------------------
% Lecture de Frequency
[flag, Data.Frequency] = Read_BinVariable(Datagrams, nomDirSignal, 'Frequency', 'uint16');
if ~flag
    return
end

% ------------------------
% Lecture de SSpeed
[flag, Data.SSpeed] = Read_BinVariable(Datagrams, nomDirSignal, 'SSpeed', 'uint16');
if ~flag
    return
end

% ------------------------
% Lecture de SampleRate
[flag, Data.SampleRate] = Read_BinVariable(Datagrams, nomDirSignal, 'SampleRate', 'uint16');
if ~flag
    return
end

% ------------------------
% Lecture de PingRate
[flag, Data.PingRate] = Read_BinVariable(Datagrams, nomDirSignal, 'PingRate', 'uint16');
if ~flag
    return
end

% ------------------------
% Lecture de Range
[flag, Data.Range] = Read_BinVariable(Datagrams, nomDirSignal, 'Range', 'uint16');
if ~flag
    return
end

% ------------------------
% Lecture de Power
[flag, Data.Power] = Read_BinVariable(Datagrams, nomDirSignal, 'Power', 'uint16');
if ~flag
    return
end

% ------------------------
% Lecture de Gain
[flag, Data.Gain] = Read_BinVariable(Datagrams, nomDirSignal, 'Gain', 'uint16');
if ~flag
    return
end

% ------------------------
% Lecture de PulseWidth
[flag, Data.PulseWidth] = Read_BinVariable(Datagrams, nomDirSignal, 'PulseWidth', 'uint16');
if ~flag
    return
end

% ------------------------
% Lecture de Spread
[flag, Data.Spread] = Read_BinVariable(Datagrams, nomDirSignal, 'Spread', 'uint16');
if ~flag
    return
end

% ------------------------
% Lecture de Absorb
[flag, Data.Absorb] = Read_BinVariable(Datagrams, nomDirSignal, 'Absorb', 'uint16');
if ~flag
    return
end

% ------------------------
% Lecture de Proj
[flag, Data.Proj] = Read_BinVariable(Datagrams, nomDirSignal, 'Proj', 'uint16');
if ~flag
    return
end

% ------------------------
% Lecture de ProjWidth
[flag, Data.ProjWidth] = Read_BinVariable(Datagrams, nomDirSignal, 'ProjWidth', 'uint16');
if ~flag
    return
end

% ------------------------
% Lecture de SpacingNum
[flag, Data.SpacingNum] = Read_BinVariable(Datagrams, nomDirSignal, 'SpacingNum', 'uint16');
if ~flag
    return
end

% ------------------------
% Lecture de SpacingDen
[flag, Data.SpacingDen] = Read_BinVariable(Datagrams, nomDirSignal, 'SpacingDen', 'uint16');
if ~flag
    return
end

% ------------------------
% Lecture de ProjAngle
[flag, Data.ProjAngle] = Read_BinVariable(Datagrams, nomDirSignal, 'ProjAngle', 'int16');
if ~flag
    return
end

% ------------------------
% Lecture de MinRange
[flag, Data.MinRange] = Read_BinVariable(Datagrams, nomDirSignal, 'MinRange', 'uint16');
if ~flag
    return
end

% ------------------------
% Lecture de MaxRange
[flag, Data.MaxRange] = Read_BinVariable(Datagrams, nomDirSignal, 'MaxRange', 'uint16');
if ~flag
    return
end

% ------------------------
% Lecture de MinDepth
[flag, Data.MinDepth] = Read_BinVariable(Datagrams, nomDirSignal, 'MinDepth', 'uint16');
if ~flag
    return
end

% ------------------------
% Lecture de MaxDepth
[flag, Data.MaxDepth] = Read_BinVariable(Datagrams, nomDirSignal, 'MaxDepth', 'uint16');
if ~flag
    return
end

% ------------------------
% Lecture de Filters
[flag, Data.Filters] = Read_BinVariable(Datagrams, nomDirSignal, 'Filters', 'uint16');
if ~flag
    return
end

% ------------------------
% Lecture de Flags
[flag, Data.Flags] = Read_BinVariable(Datagrams, nomDirSignal, 'Flags', 'uint8');
if ~flag
    return
end

% ------------------------
% Lecture de HeadTemp
[flag, Data.HeadTemp] = Read_BinVariable(Datagrams, nomDirSignal, 'HeadTemp', 'int16');
if ~flag
    return
end

% ------------------------
% Lecture de BeamCnt
[flag, Data.BeamCnt] = Read_BinVariable(Datagrams, nomDirSignal, 'BeamCnt', 'int16');
if ~flag
    return
end

% ----------------------------
% G�n�ration du XML SonarScope

Info.Title                  = Datagrams.Title; % Entete du fichier
Info.Constructor            = Datagrams.Constructor;
Info.EmModel                = Datagrams.Model;
Info.SystemSerialNumber     = Datagrams.SystemSerialNumber;
Info.TimeOrigin             = Datagrams.TimeOrigin;
Info.Comments               = Datagrams.Comments;

Info.Dimensions.NbSamples     = Datagrams.NbDatagrams;

Info.Signals(1).Name          = 'ID'; % Set to 0xFACE
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint64';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_SNP0Header','ID.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'HeaderSize';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_SNP0Header','HeaderSize.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'DataSize';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_SNP0Header','DataSize.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'PingNumber';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint64';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_SNP0Header','ProjAngle.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'Seconds';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint64';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_SNP0Header','NumBytesThisRecord.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'Millisecs';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint64';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_SNP0Header','Year.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'Latency';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_SNP0Header','Month.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'SonarID';
Info.Signals(end).Dimensions  = 'NbSamples,2';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_SNP0Header','Hour.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'SonarModel';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_SNP0Header','SonarModel.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'Frequency';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_SNP0Header','Frequency.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'SSPeed';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_SNP0Header','SSPeed.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'SampleRate';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_SNP0Header','SampleRate.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'PingRate';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_SNP0Header','PingRate.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'Range';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_SNP0Header','Range.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'Power';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_SNP0Header','Power.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'Gain';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_SNP0Header','Gain.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'PulseWidth';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_SNP0Header','PulseWidth.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'Spread';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_SNP0Header','Spread.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'Absorb';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_SNP0Header','Absorb.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'Proj';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_SNP0Header','Proj.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'ProjWidth';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_SNP0Header','ProjWidth.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'SpacingNum';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_SNP0Header','SpacingNum.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'SpacingDen';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_SNP0Header','SpacingDen.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'ProjAngle';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'int16';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_SNP0Header','ProjAngle.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'MinRange';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_SNP0Header','MinRange.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'MaxRange';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_SNP0Header','MaxRange.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'MinDepth';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_SNP0Header','MinDepth.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'MaxDepth';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_SNP0Header','MaxDepth.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'Filters';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_SNP0Header','Filters.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'Flags';
Info.Signals(end).Dimensions  = 'NbSamples,2';
Info.Signals(end).Storage     = 'uint8';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_SNP0Header','Flags.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'HeadTemp';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'int16';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_SNP0Header','HeadTemp.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'BeamCnt';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_SNP0Header','BeamCnt.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

%% Cr�ation du fichier XML d�crivant la donn�e

nomFicXml = fullfile(nomDirRacine, 'Ssc_SNP0Header.xml');
xml_write(nomFicXml, Info);
flag = exist(nomFicXml, 'file');
if ~flag
    messageErreur(nomFicXml)
    return
end

%% Cr�ation du r�pertoire File Header

nomDirSNP0Header = fullfile(nomDirRacine, 'Ssc_SNP0Header');
if ~exist(nomDirSNP0Header, 'dir')
    status = mkdir(nomDirSNP0Header);
    if ~status
        messageErreur(nomDirSNP0Header)
        flag = 0;
        return
    end
end

%% Cr�ation des fichiers binaires des signaux

for i=1:length(Info.Signals)
    flag = writeSignal(nomDirRacine, Info.Signals(i), Data.(Info.Signals(i).Name));
    if ~flag
        return
    end
end

flag = 1;
