function [flag, Data] = xtf2ssc_SNP1Header(nomDirRacine, varargin)

[varargin, flagDisplay] = getPropertyValue(varargin, 'Display', 0); %#ok<ASGLU>

% ------------------------------------------
% Lecture du fichier XML d�crivant la donn�e

nomFicXml = fullfile(nomDirRacine, 'XTF_SNP1Header.xml');
flag = exist(nomFicXml, 'file');
if ~flag
    special_message('FichierNonExistant', nomFicXml)
    return
end
% edit(nomFicXml)
Datagrams = xml_read(nomFicXml);

[nomDirSignal, nomFicSignal] = fileparts(nomFicXml);
nomDirSignal = fullfile(nomDirSignal, nomFicSignal);


% ------------------------
% Lecture de ID
[flag, Data.ID] = Read_BinVariable(Datagrams, nomDirSignal, 'ID', 'uint64');
if ~flag
    return
end

% ------------------------
% Lecture de HeaderSize
[flag, Data.HeaderSize] = Read_BinVariable(Datagrams, nomDirSignal, 'HeaderSize', 'uint16');
if ~flag
    return
end

% ------------------------
% Lecture de DataSize
[flag, Data.DataSize] = Read_BinVariable(Datagrams, nomDirSignal, 'DataSize', 'uint16');
if ~flag
    return
end

% ------------------------
% Lecture de PingNumber
[flag, Data.PingNumber] = Read_BinVariable(Datagrams, nomDirSignal, 'PingNumber', 'uint64');
if ~flag
    return
end

% ------------------------
% Lecture de Beam
[flag, Data.Beam] = Read_BinVariable(Datagrams, nomDirSignal, 'Beam', 'uint16');
if ~flag
    return
end

% ------------------------
% Lecture de SnipSamples
[flag, Data.SnipSamples] = Read_BinVariable(Datagrams, nomDirSignal, 'SnipSamples', 'uint16');
if ~flag
    return
end

% ------------------------
% Lecture de GainStart
[flag, Data.GainStart] = Read_BinVariable(Datagrams, nomDirSignal, 'GainStart', 'uint16');
if ~flag
    return
end

% ------------------------
% Lecture de GainEnd
[flag, Data.GainEnd] = Read_BinVariable(Datagrams, nomDirSignal, 'GainEnd', 'uint16');
if ~flag
    return
end

% ------------------------
% Lecture de FragOffset
[flag, Data.FragOffset] = Read_BinVariable(Datagrams, nomDirSignal, 'FragOffset', 'uint16');
if ~flag
    return
end

% ------------------------
% Lecture de FragSamples
[flag, Data.FragSamples] = Read_BinVariable(Datagrams, nomDirSignal, 'FragSamples', 'uint16');
if ~flag
    return
end

% ----------------------------
% G�n�ration du XML SonarScope

Info.Title                  = Datagrams.Title; % Entete du fichier
Info.Constructor            = Datagrams.Constructor;
Info.EmModel                = Datagrams.Model;
Info.SystemSerialNumber     = Datagrams.SystemSerialNumber;
Info.TimeOrigin             = Datagrams.TimeOrigin;
Info.Comments               = Datagrams.Comments;

Info.Dimensions.NbSamples     = Datagrams.NbDatagrams;

Info.Signals(1).Name          = 'ID'; % Set to 0xFACE
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint64';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_SNP1Header','ID.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'HeaderSize';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_SNP1Header','HeaderSize.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'DataSize';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_SNP1Header','DataSize.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'PingNumber';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint64';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_SNP1Header','ProjAngle.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'Beam';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_SNP1Header','NumBytesThisRecord.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'SnipSamples';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_SNP1Header','Year.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'GainStart';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_SNP1Header','Month.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'GainEnd';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_SNP1Header','Hour.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'FragOffset';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_SNP1Header','FragOffset.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');

Info.Signals(end+1).Name      = 'FragSamples';
Info.Signals(end).Dimensions  = 'NbSamples,1';
Info.Signals(end).Storage     = 'uint16';
Info.Signals(end).Unit        = [];
Info.Signals(end).FileName    = fullfile('Ssc_SNP1Header','FragSamples.bin');
Info.Signals(end).Tag         = verifKeyWord('TODO');


%% Cr�ation du fichier XML d�crivant la donn�e

nomFicXml = fullfile(nomDirRacine, 'Ssc_SNP1Header.xml');
xml_write(nomFicXml, Info);
flag = exist(nomFicXml, 'file');
if ~flag
    messageErreur(nomFicXml)
    return
end

%% Cr�ation du r�pertoire File Header

nomDirSNP1Header = fullfile(nomDirRacine, 'Ssc_SNP1Header');
if ~exist(nomDirSNP1Header, 'dir')
    status = mkdir(nomDirSNP1Header);
    if ~status
        messageErreur(nomDirSNP1Header)
        flag = 0;
        return
    end
end

%% Cr�ation des fichiers binaires des signaux

for i=1:length(Info.Signals)
    flag = writeSignal(nomDirRacine, Info.Signals(i), Data.(Info.Signals(i).Name));
    if ~flag
        return
    end
end

flag = 1;
