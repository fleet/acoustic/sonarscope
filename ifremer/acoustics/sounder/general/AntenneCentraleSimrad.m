% Diagramme de directivite d'un faisceau central d'emission d'un sondeur Simrad (EM300 et EM120)
%
% Syntax
%   diagr = AntenneCentraleSimrad(angles, Parametres)
%
% Input Arguments
%   angles      : Angles pour lesquels le diagramme doit etre calcule (deg)
%   Parametres  : Tableau contenant les parametres :
%
% Output Arguments 
%   []    :  Auto-plot activation
%   diagr :  Diagramme de directivite de l'antenne (dB)
%
% Examples 
%   ParametresEM300 = [-2.76773 11.1794 -2.03595 -21.4256 0 0];
%   angles = -46:46;
%   AntenneCentraleSimrad(angles, ParametresEM300);
%   diagr = AntenneCentraleSimrad(angles, ParametresEM300);
%   figure; plot(angles, diagr); grid on;
%
%   ParametresEM120 = [-4.76188  23.7955 -30.1585 2.48292 3.09409 -30];
%   angles = -50:50;
%   AntenneCentraleSimrad(angles, ParametresEM120);
%
% See also AntenneCentraleSimradDep AntennePoly AntenneTcheby Authors
% Authors : JMA + XL
%------------------------------------------------------------------------------

function varargout = AntenneCentraleSimrad(angles, p)

angles = angles * (pi/180);

c1 = p(1) * ones(size(angles));
c2 = p(2) * angles .^ 2;
c3 = p(3) * angles .^ 4;
c4 = p(4) * angles .^ 6;
c5 = p(5) * exp(p(6) * angles .^ 2);

diagr = c1 + c2 + c3 + c4 + c5;
composantes = [c1; c2; c3; c4; c5];

if nargout == 0
	figure;
	subplot(1, 2, 1);
    
    A = angles' * 180 / pi;
    plot(A, c1',  A, c2',  A, c3',  A, c4',  A, c5'); hold on;
	plot(A, diagr' , 'k'); grid on; zoom on;
	xlabel('Angles (deg)'); ylabel('Gain (dB)'); title('Diagramme de directivite AntenneCentraleSimrad');
    
	subplot(1, 2, 2);
	polarplot(A*(pi/180), reflec_dB2Enr(diagr'));
	title('Representation polaire');
else
	varargout{1} = diagr;
    varargout{2} = composantes;
end
