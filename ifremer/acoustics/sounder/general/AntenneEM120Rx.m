% Diagramme de directivite d'antenne en polynome pair d'ordre 12
%sp�cifique � l'EM120 de Simrad
% 
% Syntax
%   diag = AntenneEM120Rx( angles )
%   AntenneEM120Rx( angles, [] )
%
% Input Arguments
%   angles      : Angles pour lesquels le diagramme doit etre calcule (deg)
%   Parametres : Tableau contenant les parametres :
%   constantes        :  vecteur des coefficients des degr�s pairs du polynome
%                                  de taille 1 ligne, 6 colonnes
%
% Output Arguments
%   []    :  Auto-plot activation
%   diag  :  Diagramme de directivite de l'antenne (dB)
%
% Examples 
%   angles = -75:0.2:75;
%   AntenneEM120Rx(angles);
%
% See also AntenneSinc AntenneTcheby AntennePoly Authors
% Authors : JMA + XL
%------------------------------------------------------------------------------------------

function varargout = AntenneEM120Rx(angles, Parametres)

% NbAngles = size(angles, 2);

%Une seule pond�ration globale de tte l'antenne en r�ception
% diag = ones(1, NbAngles);

b = angles*pi/180;

c0  = 0;
c2  = -0.23;
c4  = -2.70;
c6  = 2.52;
c8  = -1.4;
c10 = 0;
c12 = 0;

diag = c0+c2*b.^2+c4*b.^4+c6*b.^6+c8*b.^8+c10*b.^10+c12*b.^12;

if nargout == 0
    figure;
    subplot(1, 2, 1);
    plot( angles', diag' ); grid on; zoom on;
    xlabel('Angles (deg)'); ylabel('Gain (dB)'); title('Diagramme de directivite AntenneEM120Rx');
    subplot(1, 2, 2);
    polarplot(angles'*(pi/180), reflec_dB2Enr(diag'));
    title('Representation polaire');
else
    varargout{1} = diag;
end
