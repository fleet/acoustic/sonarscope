% Description
%   Computes the beam diagram of an EM3002D antenna (Method used by kongsberg)
%
% Syntax
%   diag = AntenneEM3002(angles, [gain, angleMilieu, ouverture])
%
% Input Arguments
%   angles      : Angles for which the beam diagram has to be computed (deg)
%   Parametres  : Array containing the parametres :
%   gain        : Max gain (dB)
%   angleMilieu : Centre of the beam(deg)
%   ouverture   : Aperture of the beam at -3dB (deg)
%
% Output Arguments
%   []   : Auto-plot activation
%   diag : Beam diagram values of the antenna (dB)
%
% Examples
%   angles = -75:75;
%   AntenneEM3002(angles, [0, 0, 50]);
%   diag = AntenneEM3002(angles, [0, 0, 50]);
%   AntenneEM3002(angles, [0, -45,  50; 0 45 50]);
%
% See also AntenneSinc AntenneSincDep AntenneTcheby Authors
% Authors : JMA + XL
%
% Reference pages in help browser
%   <a href="matlab:doc AntenneEM3002">Doc AntenneEM3002</a>
%--------------------------------------------------------------------------------

function [diag, composantes] = AntenneEM3002(angles, Parametres, varargin) % varargin pour compatibilit� avec d'autres fonctions appel�es avec feval

NbFaisceaux = size(Parametres, 1);
NbAngles    = size(angles, 2);

gainFaisceau      = Parametres(:,1);
angleFaisceau     = Parametres(:,2);
ouvertureFaisceau = Parametres(:,3);

diag = NaN(NbFaisceaux, NbAngles, 'single');
for k=1:NbFaisceaux
    A = (angles - 0*angleFaisceau) * (pi/180);
    
    Tw = ouvertureFaisceau / 2;
    Tc = -1.9;

    composantes(1,:) = 3.5 * sqrt(abs(tan(0.89 * A)));
    composantes(2,:) = 2.5 * cos((4.2 * Tw / 48) * (abs(A) + (0.17 * Tc / 1.9)));
    y = composantes(1,:) + composantes(2,:); 
    composantes = - composantes;
    
%     y = 3.5 * (tan(0.89 * A) .^ 2) + 2.5 * cos(4.2 * (abs(A) - 0.17));
%     figure; plot(angles(k,:), y); grid on
    
    diag(k,:) = gainFaisceau(k) - y;
end

%% Sortie des param�tre ou trac�s graphiques

if nargout == 0
    angles = repmat(angles, NbFaisceaux, 1);
	figure;
	subplot(1, 2, 1);
	plot(angles', diag'); grid on; zoom on;
	xlabel('Angles (deg)'); ylabel('Gain (dB)'); title('Diagramme de directivite AntenneEM3002');
	subplot(1, 2, 2);
	polarplot(angles' * (pi/180), reflec_dB2Enr(diag'));
	title('Representation polaire');
end
