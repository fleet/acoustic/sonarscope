% Ouverture angulaire d'une antenne lineaire non ponderee
%
% Syntax
%   teta = AntenneLong2Teta(WaveLength, L, ...)
% 
% Input Arguments 
%   WaveLength : Longueur d'onde du signal (m)
%   L          : Longueur de l'antenne (m) 
%
% Name-Value Pair Arguments 
%   shadingFactor : 
% 
% Output Arguments 
%   teta : Angle d'ouverture a -3dB (deg)
%
% Examples
%   WaveLength = 1500 / 13e3
%   teta = AntenneLong2Teta(WaveLength, 5)
%
% See also AntenneTeta2Long Authors
% Authors : JMA + XL
% VERSION  : $Id: AntenneLong2Teta.m,v 1.4 2002/06/20 11:54:41 augustin Exp $
%-------------------------------------------------------------------------------
 
% ----------------------------------------------------------------------------
% HISTORIQUE DEVELOPPEMENT
%   02/11/00 - JMA - creation
%   23/03/01 - JMA - Mise en ref.
% ----------------------------------------------------------------------------

function teta = AntenneLong2Teta(WaveLength, L, varargin)

if nargin == 0
    help AntenneLong2Teta
    return
end

teta = (WaveLength ./ L) * 65;% 65. = 50. * 1.3

if(nargin == 3)
    shadingFactor = varargin{1};
    teta = teta * shadingFactor;
end