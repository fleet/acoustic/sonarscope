% Ouverture en emission/reception d'une antenne
%
% Syntax
%   theta = AntenneOuvEmiRec(ouvertureEmission, ouvertureReception)
% 
% Input Arguments 
%   ouvertureEmission  : Ouverture en emission  (deg)
%   ouvertureReception : ouverture en reception (deg) 
%
% Output Arguments 
%   theta : Angle d'ouverture en emission/reception (deg)
%
% Examples
%   theta = AntenneOuvEmiRec(150, 1.5)
%
% See also AntenneTeta2Long AntenneLong2Teta Authors
% Authors : JMA + XL
%-------------------------------------------------------------------------------

function theta = AntenneOuvEmiRec(ouvertureEmission, ouvertureReception)

theta = (ouvertureEmission .* ouvertureReception) ./ sqrt(ouvertureEmission .^ 2 + ouvertureReception .^ 2);
