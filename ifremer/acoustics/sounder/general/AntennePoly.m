% Diagramme de directivite d'antenne par approximation polynomiale d'un sinus cardinal.
% (Methode utilisee par Simrad)
%
% Syntax
%   diag = AntennePoly( angles, [gain, angleMilieu, ouverture] )
%
% Input Arguments 
%   angles     : Angles pour lesquels le diagramme doit etre calcule (deg)
%   Parametres : Tableau contenant les parametres :
%   gain        : Gain max (dB)
%   angleMilieu : Centre du faisceau (deg)
%   ouverture   : Ouverture angulaire a -3dB (deg)
%
% Output Arguments 
%   []   : Auto-plot activation
%   diag : Diagramme de directivite de l'antenne (dB)
%
% Examples
%   angles = -75:75;
%   AntennePoly( angles, [0, 0, 10] );
%   diag = AntennePoly( angles, [0, 0, 10] );
%   AntennePoly( angles, [0, 0,  10; 0 0 15] );
%
% See also AntenneSinc AntenneSincDep AntenneTcheby Authors
% Authors : JMA + XL
%--------------------------------------------------------------------------------

function diag = AntennePoly(angles, Parametres, varargin) % varargin pour compatibilit� avec d'autres fonctions appel�es avec feval

% Rustine : j'ai pas le temps de chercher le rem�de pour l'instant
if size(angles,2) == 1
    angles = angles';
    Retournement = 1;
else
    Retournement = 0;
end

NbFaisceaux = size(Parametres, 1);
NbAngles    = size(angles, 2);
% diag        = ones(NbFaisceaux, NbAngles);

gainFaisceau      = Parametres(:,1);
angleFaisceau     = Parametres(:,2);
ouvertureFaisceau = Parametres(:,3);

gainFaisceau      = repmat(gainFaisceau,      [1 NbAngles]) ;
angleFaisceau     = repmat(angleFaisceau,     [1 NbAngles]) ;
ouvertureFaisceau = repmat(ouvertureFaisceau, [1 NbAngles]) ;

angles = repmat(angles, [NbFaisceaux 1]) ;

diag = NaN(size(angles), 'single');
for k=1:size(angles, 1)
    x = 2. * 1.39155 * (angles(k,:) - angleFaisceau) ./ (ouvertureFaisceau);
    y = 1. - x.^2 ./ 6; % + x.^4./120;
    
    sub = find(y > 0);
    if ~isempty(sub)
        diag(k,sub) = gainFaisceau(sub) + 2 .* reflec_Enr2dB(y(sub));
    end
    
    sub = find(y <= 0);
    if ~isempty(sub)
        diag(k,sub) = NaN;
    end
end

if Retournement
    diag   = diag';
    angles = angles';
end

%% Sortie des param�tre ou trac�s graphiques

if nargout == 0
	figure;
	subplot(1, 2, 1);
	plot( angles', diag' ); grid on; zoom on;
	xlabel('Angles (deg)'); ylabel('Gain (dB)'); title('Diagramme de directivite AntennePoly');
	subplot(1, 2, 2);
	polarplot(angles'*(pi/180), reflec_dB2Enr(diag'));
	title('Representation polaire');
end
