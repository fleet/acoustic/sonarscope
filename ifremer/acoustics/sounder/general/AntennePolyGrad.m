% Gradient de la fonction AntennePoly.
%
% Syntax
%   [diag, J] = AntennePolyGrad( angles, [gain, angleMilieu, ouverture] )
%
% Remarks : Celle fonction est appelee par l'algorithme d'optimisation
%           NewtonRecSim. 
% 
% Input Arguments 
%   angles     : Angles pour lesquels le diagramme doit etre calcule (deg)
%   Parametres : Tableau contenant les parametres :
%   gain        : Gain max (dB)
%   angleMilieu : Centre du faisceau (deg)
%   ouverture   : Ouverture angulaire a -3dB (deg)
%
% Output Arguments 
%   []   : Auto-plot activation
%   diag : Diagramme de directivite de l'antenne (dB)
%   J    : Diagramme de directivite de l'antenne (dB)
%
% Examples
%   angles = -75:75;
%   AntennePolyGrad( angles, [0, 0,  10] );
%   [diag, J] = AntennePolyGrad( angles, [0, 0,  10] );
%
%   AntennePolyGrad( angles, [0, 0,  10] );
%
% See also AntennePoly NewtonRecSim Authors
% Authors : YHDR + MS
% VERSION  : $Id: AntennePolyGrad.m,v 1.3 2002/06/20 11:54:41 augustin Exp $
%------------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HISTORIQUE DEVELOPPEMENT
%   02/11/00 - JMA - creation
%   23/03/01 - JMA - Mise en ref.
%   05/03/02 - YHDR - correction bug
% ----------------------------------------------------------------------------

function varargout = AntennePolyGrad( angles, Parametres )

if nargin ~= 2
        help AntennePolyGrad
        return
end

%------------------------------------------------------------------------------

n = size(Parametres, 1);
m = size(angles, 2);
diagr = NaN(n, m);

if(n > 1) 
	disp(' On ne peut calculer le gradient que pour un seul jeu de coeffs');
	return
end

gainFaisceau = Parametres(:,1);
angleFaisceau = Parametres(:,2);
ouvertureFaisceau = Parametres(:,3);

cc = 2. * 1.39155 ;
ldb = 20. / log(10) ;
xx = cc * (angles - angleFaisceau) ./ (ouvertureFaisceau);
yy = 1. - xx.^2 ./ 6;
%
sub = find( yy > 0 );
xsub = xx(sub);
ysub = yy(sub) ;

diagr(:,sub) = gainFaisceau + 2 * reflec_Enr2dB(ysub);

%
J = ones(m,3) ;
J(:,1) = ones(m,1);
J(sub,2) = ( ldb ./ ysub .* xsub ./ 3 .* cc ./ ouvertureFaisceau )';
J(sub,3) = ( ldb ./ ysub .* xsub.^2 ./ 3 ./ ouvertureFaisceau )';

% -----------------------------------------
% Sortie des parametre ou traces graphiques

if(nargout == 0)
	figure;
	subplot(2, 1, 1);
	plot( angles', diagr' ); grid on; zoom on;
	xlabel('Angles (deg)'); ylabel('Gain (dB)'); title('Diagramme de directivite AntennePoly');

	subplot(2, 1, 2);
	plot( angles', J ); grid on; zoom on;
	xlabel('Angles (deg)'); title('Jacobien');
else
	varargout{1} = diagr;
	varargout{2} = J;
end
