% Diagramme de directivité d'antenne en sinus cardinal
%
% Syntax
%   diagr = AntenneSinc(angles, [gain, angleMilieu, ouverture])
%
% Input Arguments
%   angles      : Angles pour lesquels le diagramme doit etre calcule (deg)
%   Parametres  : Tableau contenant les parametres :
%     gain        : Gain max (dB)
%     angleMilieu : Centre du faisceau (deg)
%     ouverture   : Ouverture angulaire a -3dB (deg)
%
% Output Arguments 
%   []    :  Auto-plot activation
%   diagr :  Diagramme de directivite de l'antenne (dB)
%
% Examples 
%   angles = -75:75;
%   AntenneSinc( angles, [0, 0,  10] );
%   diagr = AntenneSinc( angles, [0, 0,  10] );
%   AntenneSinc( angles, [0, 0,  10; 2, 5, 8] );
%
% See also AntenneSincDep AntennePoly AntenneTcheby Authors
% Authors : JMA + XL
%------------------------------------------------------------------------------

function varargout = AntenneSinc( angles, Parametres )

seuil = Inf;

NbFaisceaux = size(Parametres, 1);
NbAngles = size(angles, 2);

% gain = ones(NbFaisceaux, NbAngles);

gainFaisceau = Parametres(:,1);
angleFaisceau = Parametres(:,2);
ouvertureFaisceau = Parametres(:,3);

if NbFaisceaux > 1
  gainFaisceau = repmat(gainFaisceau, [1 NbAngles]);
  angleFaisceau = repmat(angleFaisceau, [1 NbAngles]);
  ouvertureFaisceau = repmat(ouvertureFaisceau, [1 NbAngles]);
  angles = repmat(angles, [NbFaisceaux 1]);
end

x = 1.39155 .* (angles - angleFaisceau) ./ (ouvertureFaisceau ./ 2.);
gain = gainFaisceau + 2 .* reflec_Enr2dB(abs(sinc(x./pi)));

% Seuillage
diagr = reshape(gain, [NbFaisceaux*NbAngles 1]);

ind = find(abs(diagr-max(diagr)) > seuil);
diagr(ind) = NaN;

diagr = reshape(diagr, [NbFaisceaux NbAngles]);

if nargout == 0
	figure;
	subplot(1, 2, 1);
	plot( angles', diagr' ); grid on; zoom on;
	xlabel('Angles (deg)'); ylabel('Gain (dB)'); title('Diagramme de directivite AntenneSinc');
	subplot(1, 2, 2);
	polarplot(angles'*(pi/180), reflec_dB2Enr(diagr'));
	title('Representation polaire');
else
	varargout{1} = diagr;
end
