% Diagramme de directivit� d'antenne en sinus cardinal asym�trique 
% 
% Syntax
%   diag = AntenneSincDep( angles, [gain, angleMilieu, ouverture, inclinaisonAntenne] )
%   AntenneSincDep( angles, [gain, angleMilieu, ouverture, inclinaisonAntenne] )
%
% Input Arguments
%   angles      : Angles pour lesquels le diagramme doit etre calcule (deg)
%   Parametres : Tableau contenant les parametres :
%   gain        : Gain max (dB)
%   angleMilieu : Centre du faisceau (deg)
%   ouverture   : Ouverture angulaire a -3dB (deg)
%   inclinaisonAntenne : inclinaison de l'antenne (deg)
%
% Output Arguments
%   []    :  Auto-plot activation
%   diag  :  Diagramme de directivite de l'antenne (dB)
%
% Examples 
%   angles = -75:0.2:75;
%   AntenneSincDep(angles, [0, 70, 10, 40]);
%   diag = AntenneSincDep(angles, [0, 70, 10, 40]);
%   AntenneSincDep(angles, [0, 50, 30, 40; 0, 50, 30, -40]);
%
% See also AntenneSinc AntenneTcheby AntennePoly AntenneSpline Authors
% Authors : JMA + XL
%------------------------------------------------------------------------------------------

function diag = AntenneSincDep(angles, Parametres)

% Rustine : j'ai pas le temps de chercher le rem�de pour l'instant
if size(angles,2) == 1
    angles = angles';
    Retournement = 1;
else
    Retournement = 0;
end

NbFaisceaux = size(Parametres, 1);
NbAngles    = size(angles, 2);

gainFaisceau       = Parametres(:,1);
angleFaisceau      = Parametres(:,2);
ouvertureFaisceau  = Parametres(:,3);
inclinaisonAntenne = Parametres(:,4);

gainFaisceau       = repmat(gainFaisceau,       [1 NbAngles]);
angleFaisceau      = repmat(angleFaisceau,      [1 NbAngles]);
ouvertureFaisceau  = repmat(ouvertureFaisceau,  [1 NbAngles]);
inclinaisonAntenne = repmat(inclinaisonAntenne, [1 NbAngles]);

angles = repmat(angles, [NbFaisceaux 1]);

angleFaisceau_axe = angleFaisceau - inclinaisonAntenne;

% ouvertureFaisceau = Parametres(i,3) * cos(angleFaisceau_axe * pi / 180);	% DEBUG XL

if ouvertureFaisceau == 0
    diag = NaN * size(angles);
    return
end

l_sur_lambda = 50 ./ ouvertureFaisceau;
piXl_sur_lambda = pi * l_sur_lambda;
angle_axe    = angles - inclinaisonAntenne;
dif_sin      = sind(angle_axe) - sind(angleFaisceau_axe);
numerateur   = abs(sin(piXl_sur_lambda .* dif_sin));
denominateur = piXl_sur_lambda .*  abs(dif_sin);

diag = gainFaisceau;
sub = find(denominateur);
if ~isempty(sub)
    diag(sub) = gainFaisceau(sub) + 2 .* reflec_Enr2dB(numerateur(sub) ./ denominateur(sub));
end

if Retournement
    diag = diag';
    angles = angles';
end

if nargout == 0
    figure;
    subplot(1, 2, 1);
    plot( angles', diag' ); grid on; zoom on;
    xlabel('Angles (deg)'); ylabel('Gain (dB)'); title('Diagramme de directivite AntenneSincDep');
    subplot(1, 2, 2);
    polarplot(angles'*(pi/180), reflec_dB2Enr(diag'));
    title('Representation polaire');
else
    varargout{1} = diag; %#ok<NASGU>
end
