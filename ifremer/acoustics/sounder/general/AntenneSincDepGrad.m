% Gradient de la fonction AntenneSincDep. ATTENTION : LA FORMULE A CHANGE MAIS LES RESULTATS SONT EQUIVALENTS
%
% [diagr, J] = AntenneSincDepGrad(angles, [gain, angleMilieu, ouverture, inclinaisonAntenne])
% AntenneSincDepGrad( angles, [gain, angleMilieu, ouverture, inclinaisonAntenne] )
%
% Input Arguments
%   angles     : Angles pour lesquels le diagramme doit etre calcule (deg)
%   Parametres : Tableau contenant les parametres :
%     gain        : Gain max (dB)
%     angleMilieu : Centre du faisceau (deg)
%     ouverture   : Ouverture angulaire a -3dB (deg)
%     inclinaisonAntenne : inclinaison de l'antenne (deg)
%
% Output Arguments
%   []    :  Auto-plot activation
%   diagr  :  Diagramme de directivite de l'antenne (dB)
%   J    :  Gradient de directivite de l'antenne (dB)
%
% Examples 
%   angles = -75:75;
%   AntenneSincDepGrad( angles, [0, 70, 10, 40] );
%   [diagr, J] = AntenneSincDepGrad( angles, [0, 70, 10, 40] );
%
% See also AntenneSinDep NewtonRecSim Authors
% Authors : YHDR + MS 
% VERSION  : $Id: AntenneSincDepGrad.m,v 1.4 2003/03/27 09:00:00 augustin Exp $
%--------------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HISTORIQUE DEVELOPPEMENT
%   02/11/00 - JMA - creation
%   30/11/00 - JMA - normalisation
%   23/03/01 - JMA - Mise en ref.
%   05/03/02 - YHDR - correction de gros bugs (valeur fonction et derivees 2 et 4)
% ----------------------------------------------------------------------------

function [diagr, J] = AntenneSincDepGrad( angles, Parametres )

if nargin ~= 2
        help AntenneSincDepGrad
        return
end

n = size(Parametres, 1);
NbAngles = size(angles, 2);

diagr = ones(n, NbAngles);

if n > 1
   disp('impossible de calculer le gradient avec plusieurs jeux de coeffs');
   return
end
	
gainFaisceau = Parametres(:,1);
angleFaisceau = Parametres(:,2);
ouvertureFaisceau = Parametres(:,3);
inclinaisonAntenne = Parametres(:,4);

%% OuvertureFaisceau = Parametres(i,3) * cos(angleFaisceau_axe * pi / 180);	% DEBUG XL

d2g = pi / 180;
ldb = 20 / log(10);

pi_sur_lambda = pi * 50 / ouvertureFaisceau;

angle_axe = angles - inclinaisonAntenne;
angleFaisceau_axe = angleFaisceau - inclinaisonAntenne;

dif_sin = sin(angle_axe * d2g) - sin(angleFaisceau_axe * d2g);

argnum = pi_sur_lambda * dif_sin;

diagr = gainFaisceau * ones(1,NbAngles);
numerateur = sin(argnum);
sub = find(argnum);
sincsub = numerateur(sub) ./ argnum(sub);
diagr(:,sub) = diagr(:,sub) + ldb * log(abs(sincsub));

% -------------------------------------
% Derivation par rapport a gainFaisceau

J = zeros(NbAngles,4);
J(:,1) = ones(NbAngles,1);

% --------------------------------------
% Derivation par rapport a angleFaisceau

xsub = argnum(sub) ;
ysub = ( xsub.*cos(xsub) - numerateur(sub) ) ./ (xsub.^2) ;
zsub = ysub .* ldb ./ sincsub ;
J(sub,2) = d2g .* pi_sur_lambda .* (-cos(angleFaisceau_axe * d2g)) .* zsub' ;

% ------------------------------------------
% Derivation par rapport a ouvertureFaisceau

J(sub,3) = - pi_sur_lambda ./ ouvertureFaisceau .* dif_sin(sub)' .* zsub' ;

% -------------------------------------------
% Derivation par rapport a inclinaisonAntenne

J(sub,4) = d2g .* pi_sur_lambda .* ...
    (cos(angleFaisceau_axe * d2g) - cos(angle_axe(sub) * d2g))' .* zsub' ; 


if(nargout == 0)
	figure;
	subplot(2, 1, 1);
	plot( angles', diagr' ); grid on; zoom on;
	xlabel('Angles (deg)'); ylabel('Gain (dB)'); title('Diagramme de directivite AntenneSincDep');

	subplot(2, 1, 2);
	plot( angles', J ); grid on; zoom on;
	xlabel('Angles (deg)'); title('Jacobien');
else
	varargout{1} = diagr;
	varargout{2} = J;
end
