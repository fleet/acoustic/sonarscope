% Gradient de la fonction AntenneSinc
%
% diagr = AntenneSincGrad( angles, [gain, angleMilieu, ouverture] )
%
% Remarks : Celle fonction est appelee par l'algorithme d'optimisation
%           NewtonRecSim. 
% 
% Input Arguments
%   angles     : Angles pour lesquels le diagramme doit etre calcule (deg)
%   Parametres : Tableau contenant les parametres :
%     gain        : Gain max (dB)
%     angleMilieu : Centre du faisceau (deg)
%     ouverture   : Ouverture angulaire a -3dB (deg)
%
% Output Arguments
%   []   : Auto-plot activation
%   diagr : gradient de directivite de l'antenne (dB)
%
% Examples 
%   angles = -75:75;
%   AntenneSincGrad( angles, [0, 0,  10] );
%   [diagr, J] = AntenneSincGrad( angles, [0, 0,  10] );
% 	
% See also AntenneSinc NewtonRecSim Authors
% Authors : YHDR + MS
% VERSION  : $Id
%------------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HISTORIQUE DEVELOPPEMENT
%   02/11/00 - JMA - creation
%   23/03/01 - JMA - Mise en ref.
% ----------------------------------------------------------------------------


function [diagr, J] = AntenneSincGrad( angles, Parametres )

if nargin ~= 2
        help AntenneSincGrad
        return
end

seuil = Inf ;

NbFaisceaux = size(Parametres, 1) ;
NbAngles = size(angles, 2);

diagr = ones(NbFaisceaux, NbAngles);

gainFaisceau = Parametres(:,1);
angleFaisceau = Parametres(:,2);
ouvertureFaisceau = Parametres(:,3);

if (NbFaisceaux > 1)
  disp('impossible de calculer le gradient pour plusieurs jeux de coeffs')
  return;
	
  %gainFaisceau = repmat(gainFaisceau, [1 NbAngles]) ;
  %angleFaisceau = repmat(angleFaisceau, [1 NbAngles]) ;
  %ouvertureFaisceau = repmat(ouvertureFaisceau, [1 NbAngles]) ;
  %angles = repmat(angles, [NbFaisceaux 1]) ;

end
difference = angles - angleFaisceau;
x = 2.7831 .* difference ./ (ouvertureFaisceau * pi);
sincardinal = sinc(x);
diagr = gainFaisceau + 2 .* reflec_Enr2dB(abs(sincardinal));


% Seuillage
%diagr = reshape(diagr, [NbFaisceaux*NbAngles 1]) ;

ind = find(abs(diagr-max(diagr)) > seuil) ;
diagr(ind) = NaN ;

% repérage des indices pour lesquels l'argument du sinus cardinal est nul

ind = find(difference == 0) ;
warning off

cos1 = cos(pi * x);
constante = 20. / log(10);

J(:,1) = ones(NbAngles, 1);

J(:,2) = (constante .* ((sincardinal ./ difference) ...
    - (cos1 ./ (difference))) ./ sincardinal)';

J(:,3) = (constante .* ((sincardinal ./ ouvertureFaisceau) ...
    - (cos1 ./ (ouvertureFaisceau))) ./ sincardinal)';


J(ind,2) = 0. ;

J(ind,3) = 0. ;

if(nargout == 0)
  figure;
  subplot(2, 1, 1);
  plot( angles', diagr' ); grid on; zoom on;
  xlabel('Angles (deg)');
  ylabel('Gain (dB)');
  title('Diagramme de directivite AntenneSinc');
  subplot(2, 1, 2);
  plot( angles', J ); grid on; zoom on;
  xlabel('Angles (deg)'); title('Jacobien');
else
  varargout{1} = diagr;
  varargout{2} = J;
end

warning on
      



