% Description
%   Computes the beam diagram of an antenna parametrized by a set of nodes and a spline
%   interpolation between nodes
%
% Syntax
%   AntenneSpline(XData, yNodes, xNodes)
%   YData = AntenneSpline(XData, yNodes, xNodes)
%
% Input Arguments
%   XData  : Angles for which the beam diagram has to be computed (deg)
%   xNodes : Abscissa of the nodes
%   yNodes : Ordinates of the nodes
%
% Output Arguments
%   []    :  Auto-plot activation
%   YData :  Beam diagram values of the antenna (dB)
%
% More About : This function can be used for any kind of problem where one
% wants to reduce a curve with few nodes and the spline interpolator
%
% Examples
%   XData  = -80:0.5:-30;
%   xNodes = -80:10:-30;
%   yNodes = [-9.8 -4.0 -0.7  1.0 -1.1 -8.0];
%   AntenneSpline(XData, yNodes, xNodes);
%   YData = AntenneSpline(XData, yNodes, xNodes);
%
%   FileNameSsc = getNomFicDatabase('BSCorr_EM710.txt');
%   [flag, ExtraArray] = readExtraParams_BSCorr(FileNameSsc);
%   for k1=1:length(ExtraArray.BSCorr)
%       Fig = FigUtils.createSScFigure;
%       pingMode  = ExtraArray.BSCorr(k1).pingMode;
%       swathMode = ExtraArray.BSCorr(k1).swathMode;
%       for k2=1:length(ExtraArray.BSCorr(k1).Sector)
%           xNodes = -ExtraArray.BSCorr(k1).Sector(k2).Node(:,1)'; % TODO : signe -
%           yNodes = ExtraArray.BSCorr(k1).Sector(k2).Node(:,2)';
%           XData = min(xNodes):0.5:max(xNodes);
%           Title = sprintf('swathMode : %d  -   pingMode : %d', swathMode, pingMode);
%           AntenneSpline(XData, yNodes, xNodes, 'Fig', Fig, 'Title', Title);
%       end
%   end
%
% See also  AntenneSplineModel AntenneSinc AntenneSincDep AntenneTcheby AntennePoly Authors
% Authors : JMA
%
% Reference pages in help browser
%   <a href="matlab:doc AntenneSpline">Doc AntenneSpline</a>
%------------------------------------------------------------------------------------------

function YData = AntenneSpline(XData, yNodes, xNodes, varargin)

subNaN = isnan(yNodes);
xNodes(subNaN) = [];
yNodes(subNaN) = [];

warning('Off', 'MATLAB:polyfit:RepeatedPointsOrRescale')
YData = spline(xNodes, yNodes, XData);

%{
% degree = 12; % Avant le 05/05/2015
degree = min(12, floor(length(XData) / 3)); % Apr�s le 05/05/2015 pour ajustement antenne compl�te des EM2040 (Dimitrios)
P = polyfit(XData, YData, degree);
YData = polyval(P, XData);
%}

warning('On', 'MATLAB:polyfit:RepeatedPointsOrRescale')

if nargout == 0
    [varargin, Fig] = getPropertyValue(varargin, 'Fig', []);
    [varargin, Title] = getPropertyValue(varargin, 'Title', 'DiagrammededirectiviteAntenneSpline'); %#ok<ASGLU>
    
    if isempty(Fig)
        FigUtils.createSScFigure;
    else
        FigUtils.createSScFigure(Fig);
    end
    subplot(1, 2, 1);
    plot(XData', YData' ); grid on; zoom on; hold on;
    xlabel('Angles (deg)'); ylabel('Gain (dB)'); title(Title);
    subplot(1, 2, 2);
    polarplot(XData' * (pi/180), reflec_dB2Enr(YData')); hold on;
    title('Representation polaire');
end
