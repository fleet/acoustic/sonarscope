% Diagramme de directivit� d'antenne pond�r�e par un polynome de Tchebycheff
% 
% Syntax
%   diag = AntenneTcheby( angles, [gain, angleMilieu, attenuation, d_sur_l, N] )
%   AntenneTcheby( angles, [gain, angleMilieu, attenuation, d_sur_l, N] )
%
% Input Arguments
%   angles      : Angles pour lesquels le diagramme doit etre calcule (deg)
%   Parametres : Tableau contenant les parametres :
%     gain        : Gain max (dB)
%     angleMilieu : Centre du faisceau (deg)
%     attenuation : Attenuation du secondaire/faisceau central
%     d_sur_l     : Rapport longuer/lambda (exprime l'ouverture conjointement a N)
%     N           : Nombre de ?? (exprime l'ouverture conjointement a N)
%
% Output Arguments
%   []   : Auto-plot activation
%   diag : Diagramme de directivite de l'antenne (dB)
%
% Examples 
%   angles = -75:0.1:75;
%   AntenneTcheby(angles, [0, -20,  10,  0.4333,  4.4]);
%   diag = AntenneTcheby(angles, [0, -20,  10,  0.4333,  4.4]);
%   AntenneTcheby(angles, [0, -20,  10,  0.4333,  4.4; 0, -15,  10,  0.6,  3.5]);
%
% See also AntenneSinc AntenneSincDep AntennePoly Authors
% Authors : JMA + LH
%---------------------------------------------------------------------------------------

function diag = AntenneTcheby( angles, Parametres )

if isnan(sum(Parametres))
    diag = NaN(size(angles));
    return
end

n = size(Parametres, 1);
m = size(angles, 2);
diag = ones(n, m);
for i=1:n
	gainFaisceau = Parametres(i,1);
	angleFaisceau = Parametres(i,2);
	attenuation = Parametres(i,3);
	d_sur_l = Parametres(i,4);
	N = Parametres(i,5);

	diag(i,:) = gainFaisceau * ones(size(angles));

	%% Calcul de R0 et de x0

	R0 = reflec_dB2Amp(attenuation);
	x0 = cosh(1/N * acosh(R0));

	%% Calcul de b : cas ou d_sur_l est < 0.5

	sub1 = (d_sur_l < 0.5);
	b(sub1) = (x0 * cos((2*pi) * d_sur_l) + 1) / (cos(2. * pi * d_sur_l) - 1);

	%% Calcul de b : cas ou d_sur_l est >= 0.5

	sub1 = (d_sur_l >= 0.5);
	b(sub1) = (x0 - 1) / 2;

	%% Calcul de a

	a = x0 - b;

	%% Calcul de x, u et uprime

	x = (angles - angleFaisceau) * (pi / 180);
	u = 2 * pi * d_sur_l * sin(x);
	uprime = a * cos(u) + b;

	%% Calul de y : cas ou |u| < 1
	
	sub1 = find(abs(uprime) < 1);
	y(sub1) = cos(N * acos( uprime(sub1) ) );

	%% Calul de y : cas ou |u| >= 1
	
	sub1 = find(abs(uprime) >= 1);
	y(sub1) = cosh( N * acosh(uprime(sub1)));

	%% Calul du gain en dB

	diag(i,:) = gainFaisceau + reflec_Enr2dB(y .* y);
	diag(i,:) = diag(i,:) - attenuation;
end

if nargout == 0
	figure;
	subplot(1, 2, 1);
	plot( angles', diag' ); grid on; zoom on;
	xlabel('Angles (deg)'); ylabel('Gain (dB)'); title('Diagramme de directivite AntenneTcheby');
	subplot(1, 2, 2);
	NbFaisceaux = size(Parametres, 1);
	angles = repmat(angles, [NbFaisceaux 1]);
	polarplot(angles'*(pi/180), reflec_dB2Enr(diag'));
	title('Representation polaire');
else
	varargout{1} = diag; %#ok<NASGU>
end
