% Gradient de la fonction AntenneTcheby
%
% Syntax
%   [diag, J] = AntenneTchebyGrad( angles, [gain, angleMilieu, attenuation, d_sur_l, N] )
%   AntenneTchebyGrad( angles, [gain, angleMilieu, attenuation, d_sur_l, N] )
%
% Remarks : Celle fonction est appelee par l'algorithme d'optimisation
%           NewtonRecSim. 
% 
% Input Arguments
%   angles      : Angles pour lesquels le diagramme doit etre calcule (deg)
%   Parametres : Tableau contenant les parametres :
%     gain        : Gain max (dB)
%     angleMilieu : Centre du faisceau (deg)
%     attenuation : Attenuation du secondaire/faisceau central
%     d_sur_l     : Rapport longuer/lambda (exprime l'ouverture conjointement a N)
%     N           : Nombre de ?? (exprime l'ouverture conjointement a N)
%
% Output Arguments
%   []   : Auto-plot activation
%   diag : Diagramme de directivite de l'antenne (dB)
%   J    : gradient du directivite de l'antenne
%
% Examples 
%   angles = -75:0.1:75;
%   AntenneTchebyGrad( angles, [0, -20,  10,  0.4333,  4.4] );
%   [diag, J] = AntenneTchebyGrad( angles, [0, -20,  10,  0.4333,  4.4] );
% 	
% See also AntenneTcheby NewtonRecSim Authors
% Authors : YHDR + MS
% VERSION  : $Id: AntenneTchebyGrad.m,v 1.2 2002/06/20 11:54:41 augustin Exp $
%--------------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HISTORIQUE DEVELOPPEMENT
%   02/11/00 - JMA - creation
%   23/03/01 - JMA - Mise en ref.
% ----------------------------------------------------------------------------

function [diag, J] = AntenneTchebyGrad( angles, Parametres )


if nargin ~= 2
        help AntenneTchebyGrad
        return
end

n = size(Parametres, 1);
m = size(angles, 2);
if n > 1
	disp('impossible de calculer le gradient pour des jeux de coefs differents');
	return;
end

% diag = ones(n, m);

gainFaisceau = Parametres(:,1);
angleFaisceau = Parametres(:,2);
attenuation = Parametres(:,3);
d_sur_l = Parametres(:,4);
N = Parametres(:,5);

diag = gainFaisceau * ones(size(angles));

	% Calcul de R0 et de x0
	% ---------------------

R0 = 10. .^ (attenuation / 20.);
T = acosh( R0 );
x0 = cosh( T/N );

	% Calcul de b : cas ou d_sur_l est < 0.5
	% --------------------------------------

if d_sur_l < 0.5

v=cos( 2. * pi * d_sur_l);
b = ( x0 * v + 1.) / ( v - 1. );

else
	% Calcul de b : cas ou d_sur_l est >= 0.5
	% ---------------------------------------
b = (x0 - 1) / 2.;

end

	% Calcul de a
	% -----------

a =  x0 - b;

	% Calcul de x, u et uprime
	% ------------------------

x = (angles - angleFaisceau) * pi / 180;
u = 2. * pi * d_sur_l * sin(x);
uprime = a * cos(u) + b;

	% Calul de y : cas ou |u| < 1
	% ---------------------------
	
sub1 = find(abs(uprime) < 1);
w1 = acos( uprime(sub1) );
y(sub1) = cos( N * w1 ) ;

	% Calul de y : cas ou |u| >= 1
	% -------------------------=--
	
sub2 = find(abs(uprime) >= 1);
w2 = acosh( uprime(sub2));
y(sub2) = cosh( N * w2 ) ;

	% Calul du gain en dB
	% -------------------

diag = gainFaisceau + reflec_Enr2dB(y .* y);
diag = diag(1,:) - attenuation;

V(1,:)=ones(1,m);
V(2,:)=20 ./ (log(10).*y);
V(3,:)=-ones(1,m);

val=V(2,:);
V(4,sub1)=val(sub1).*(-w1.*sin(N.*w1));
V(2,sub1)=val(sub1).*(N.*sin(N.*w1))./sqrt(1-uprime(sub1).^2);

V(4,sub2)=val(sub2).*(w2.*sinh(N.*w2));
V(2,sub2)=val(sub2).*(N.*sinh(N.*w2))./sqrt(uprime(sub2).^2-1);

V(5,:)=V(4,:);
V(6,:)=V(2,:);
V(4,:)=V(2,:).*(-a.*sin(u));
V(2,:)=V(2,:).*cos(u);

V(7,:)=V(4,:).*2.*pi.*d_sur_l.*cos(x);
V(4,:)=V(4,:).*2.*pi.*sin(x);

V(7,:)=-pi./180.*V(7,:);

V(6,:)=V(6,:)-V(2,:);

if d_sur_l < 0.5 
V(2,:)=V(2,:)+v./(v-1).*V(6,:);
V(6,:)=-(x0+1)./(v-1)^2.*V(6,:);
V(4,:)=V(4,:)-2.*pi.*sin(2.*pi.*d_sur_l).*V(6,:);
V(6,:)=V(7,:);

else

V(2,:)=V(2,:)+0.5.*V(6,:);
V(6,:)=V(7,:);

end

V(5,:)=V(5,:)-V(2,:).*T./N^2.*sinh(T/N);
V(2,:)=V(2,:)./N.*sinh(T./N);

V(2,:)=V(2,:)./sqrt(R0^2-1);

V(3,:)=V(3,:)+log(10)./20.*R0.*V(2,:);
V(2,:)=V(6,:);

J = V([1 2 3 4 5],:)';



if(nargout == 0)
	figure;
	subplot(2, 1, 1);
	plot( angles', diag' ); grid on; zoom on;
	xlabel('Angles (deg)'); ylabel('Gain (dB)'); title('Diagramme de directivite AntenneTcheby');

	subplot(2, 1, 2);
	plot( angles', J ); grid on; zoom on;
	xlabel('Angles (deg)'); title('Jacobien');
else
	varargout{1} = diag;
	varargout{2} = J;
end
