% Longueur d'une antenne lineaire non ponderee en fonction de sa longueur
%
% Syntax
%   L = AntenneLong2Teta(WaveLength, teta)
%   L = AntenneLong2Teta(WaveLength, teta, shadingFactor)
% 
% Input Arguments 
%   WaveLength : Longueur d'onde du signal (m)
%   teta      : Angle d'ouverture a -3dB (deg)
%
% Name-Value Pair Arguments 
%   shadingFactor : Coefficient ponderateur
%
% Output Arguments 
%   L          : Longueur de l'antenne (m) 
%
% Examples
%   WaveLength = 1500 / 13e3
%   teta = AntenneTeta2Long(WaveLength, 1.5)
%
% See also AntenneLong2Teta Authors
% Authors : JMA + XL
%-------------------------------------------------------------------------------
 
function L = AntenneTeta2Long(WaveLength, teta, varargin)

L = WaveLength ./ teta * 65;	% 65. = 50. * 1.3

if nargin == 3
    shadingFactor = varargin{1};
    L = L * shadingFactor;
end