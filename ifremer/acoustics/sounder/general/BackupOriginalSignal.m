function flag = BackupOriginalSignal(nomDirRacine, InfoSignal, grpName)

[pathname, filename] = fileparts(nomDirRacine);
pathname = fileparts(pathname);
dataFileName = fullfile(pathname, [filename '.all']);

[flag, ncFileName] = existCacheNetcdf(dataFileName);
if flag
    flag = NetcdfUtils.BackupOriginalSignal(ncFileName, InfoSignal, grpName);
else
%     InfoSignal.FileName = fullfile(grpName, [InfoSignal.Name '.bin']); % Pour debug quand la lecture a �t� faite en Netcdf
    flag = XMLBinUtils.BackupOriginalSignal(nomDirRacine, InfoSignal);
end
