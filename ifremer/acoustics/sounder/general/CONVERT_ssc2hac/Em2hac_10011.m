function [flag, NLus] = Em2hac_10011(file, nomDir, iTuple, NbFaisceaux, Data, NLus, SeuildB, SeuilDeg, varargin)

% Fonction inspir�e du 10040.

if strcmp(file.Class, 'cl_ExRaw')
    %TODO 
elseif strcmp(file.Class, 'cl_simrad_all')
    %TODO
elseif strcmp(file.Class, 'cl_reson_s7k')
    NLus = NaN;
    flag = ResonS7k_Em2hac_10011(file, nomDir, iTuple, NbFaisceaux, Data, SeuildB, SeuilDeg, varargin{:});
else
    flag = 0;
    return
end
