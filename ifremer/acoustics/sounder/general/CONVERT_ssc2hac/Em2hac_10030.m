function [flag, NbSLusOut] = Em2hac_10030(file, ~, iTuple, NbFaisceaux, Data)

flag = 0;

% Tuple 10040 Ping U-16 uncompressed 16 bytes format pour les phases.
TupleType           = 10030;
TimeFraction        = Data.DataSample.time_fraction(iTuple);    %0.0000;
TimeCPU             = Data.DataSample.time_cpu(iTuple);         %InfoPing.startdatatime + InfoPing.datatime;
TransceiverMode     = 0;
PingNumber          = Data.DataSample.Ping(iTuple);


for k = 1:NbFaisceaux
    
    % Traitement du paquets d'échantillons (Ping, Faisceau), traitement de
    % la compression.
    samples                 = squeeze(Data.DataSample.Samples(k, iTuple, :));
    index_samples           = (1:numel(samples))';
    detectedRangeBottom     = numel(samples);
    SoftChannelId           = k;
    
    X                       = [int16(index_samples) int16(samples)]';
    TupleSize               = 22 + numel(X) * 2; 
    %% Réécriture de l'entête et des échantillons.
    
    fwrite(file.fid, TupleSize,         'uint32');
    fwrite(file.fid, TupleType,         'uint16');
    fwrite(file.fid, TimeFraction,      'uint16');
    fwrite(file.fid, TimeCPU,           'uint32');
    fwrite(file.fid, SoftChannelId,     'uint16');
    fwrite(file.fid, TransceiverMode,   'uint16');
    fwrite(file.fid, PingNumber,        'uint32');
    if detectedRangeBottom==0
        fwrite(file.fid, 2147483647,'int32');
    else
        pppp = int32(detectedRangeBottom*Data.DataSample.SoundVelocity(k)/(2*Data.DataSample.SampleFrequency(k)))*1000;
        fwrite(file.fid, pppp,'int32');        
    end
          
    fwrite(file.fid, X, 'int16');
    
    % Ecriture de la fin du tuple
    TupleAttribute  = 0;
    Backlink        = TupleSize+10;
    fwrite(file.fid, TupleAttribute,'int32');
    fwrite(file.fid, Backlink,'uint32');
end

NbSLusOut = 0;

flag = 1;