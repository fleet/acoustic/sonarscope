function [flag, NbSLusOut] = Em2hac_10031(file, ~, iTuple, NbFaisceaux, Data)

flag = 0;

% Tuple 10040 Ping U-16 uncompressed 16 bytes format pour les phases.
TupleType           = 10031;
TimeFraction        = Data.DataSample.time_fraction(iTuple);    %0.0000;
TimeCPU             = Data.DataSample.time_cpu(iTuple);         %InfoPing.startdatatime + InfoPing.datatime;
TransceiverMode     = 0;
PingNumber          = Data.DataSample.Ping(iTuple);


for k = 1:NbFaisceaux
    
    % Traitement du paquets d'�chantillons (Ping, Faisceau), traitement de
    % la compression.
    angles_athwart          = squeeze(Data.DataSample.angles_athwart(k, iTuple, :));
    angles_along            = squeeze(Data.DataSample.angles_along(k, iTuple, :));
    index_angles            = (1:numel(angles_along))';
    detectedRangeBottom     = numel(angles_along);
    SoftChannelId           = k;
    
    % Conversion en int16 des Index_angles au lieu de uint16 pour �viter un
    % Warning 'Concatenation with dominant (left-most) integer class ...'
    X                       = [int16(index_angles) int16(angles_along) int16(angles_athwart)]';
    TupleSize               = 22 + numel(X) * 2; 
    %% R��criture de l'ent�te et des �chantillons.
    
    fwrite(file.fid, TupleSize,         'uint32');
    fwrite(file.fid, TupleType,         'uint16');
    fwrite(file.fid, TimeFraction,      'uint16');
    fwrite(file.fid, TimeCPU,           'uint32');
    fwrite(file.fid, SoftChannelId,     'uint16');
    fwrite(file.fid, TransceiverMode,   'uint16');
    fwrite(file.fid, PingNumber,        'uint32');
    if detectedRangeBottom==0
        fwrite(file.fid, 2147483647,'int32');
        %  fwrite(file.fid, ((24/cos(WC.angles_athwart(iTuple,k)*pi/180))*1000),'int32');
    else
        pppp = int32(detectedRangeBottom*Data.DataSample.SoundVelocity(k)/(2*Data.DataSample.SampleFrequency(k)))*1000;
        fwrite(file.fid, pppp,'int32');
        %  fwrite(file.fid, ((24/cos(WC.angles_athwart(iTuple,k)*pi/180))*1000),'int32');
        
    end
          
    fwrite(file.fid, X, 'int16');
    
    % Ecriture de la fin du tuple
    TupleAttribute  = 0;
    Backlink        = TupleSize+10;
    % Ecriture de la partie Optional pour tomber sur un mutiple de 4 pour
    % le champ suivant : non fonctionnel si on s'en tient au fichier
    % L0001-D20070513-T080745-EK60.raw
    % while (ftell(file.fid)/4 - floor(ftell(file.fid)/4)) ~= 0
    %         fwrite(file.fid, 0, 'int16');
    % end
    fwrite(file.fid, TupleAttribute,'int32');
    fwrite(file.fid, Backlink,'uint32');
end

NbSLusOut = 0;

flag = 1;