function [flag, NLus] = Em2hac_10040(file, nomDir, iTuple, NbFaisceaux, Data, NLus, SeuildB, varargin)


if strcmp(file.Class, 'cl_ExRaw')
    [flag, NLus] = ExRaw_Em2hac_10040(file, nomDir, iTuple, NbFaisceaux, Data, NLus, SeuildB);
elseif strcmp(file.Class, 'cl_simrad_all')
    [flag, NLus] = SimradAll_Em2hac_10040(file, nomDir, iTuple, NbFaisceaux, Data, NLus, SeuildB);
elseif strcmp(file.Class, 'cl_reson_s7k')
    NLus = NaN;
    flag = ResonS7k_Em2hac_10040(file, nomDir, iTuple, NbFaisceaux, Data, SeuildB, varargin{:});
else
    flag = 0;
    return
end
