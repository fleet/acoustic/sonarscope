function flag = Em2hac_10140(file, iTuple, Data) 

if strcmp(file.Class, 'cl_ExRaw') || strcmp(file.Class, 'cl_simrad_all')
    flag = SimradAll_Em2hac_10140(file, iTuple, Data);
elseif strcmp(file.Class, 'cl_reson_s7k')
    flag = ResonS7k_Em2hac_10140(file, iTuple, Data);
else
    flag = 0;
end
