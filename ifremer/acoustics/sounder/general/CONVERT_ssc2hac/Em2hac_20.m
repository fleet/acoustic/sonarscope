function flag = Em2hac_20(file, iTuple, Data)

persistent TuplePrev;
if (iTuple == 1)
    TuplePrev = [];
end

flag = 0;

% Tuple 20 standard position
TupleSize           = 26;
TupleType           = 20;
TimeFraction        = Data.Position.time_fraction(iTuple);
TimeCPU             = Data.Position.time_cpu(iTuple);                                  %heure locale en seconde depuis le 1er janvier 1970
% Enregistrement des informations utiles du Tuple pour comparaison 
% avec le Ping pr�c�dent.
TupleCrt.GPSTime             = Data.Position.time_cpu(iTuple);                             % heure GPS en seconde depuis le 1er janvier 1970
TupleCrt.PositSystem         = 1;
TupleCrt.Space               = 0;
% % % TupleCrt.Latitude            = Data.Position.latitude(iTuple)*0.05; % GLU : Cf Position.XML %-90 à +90
% % % TupleCrt.Longitude           = Data.Position.longitude(iTuple)*0.1;                    % de -180 à +180
TupleCrt.Latitude            = Data.Position.latitude(iTuple); % GLU : Cf Position.XML %-90 à +90
TupleCrt.Longitude           = Data.Position.longitude(iTuple);                    % de -180 à +180
TupleCrt.TupleAttribute      = 0;
TupleCrt.BackLink            = 36;

% Ecriture des tuples que si les donn�es diff�rents du datagramme
% pr�c�dent.
if ~isempty(TuplePrev) || ~isequal(TuplePrev, TupleCrt)
    fwrite(file.fid, TupleSize,'uint32');
    fwrite(file.fid, TupleType,'uint16');
    fwrite(file.fid, TimeFraction,'uint16');
    fwrite(file.fid, TimeCPU,'uint32');
    
    fwrite(file.fid, TupleCrt.GPSTime,'uint32');
    fwrite(file.fid, TupleCrt.PositSystem,'uint16');
    fwrite(file.fid, TupleCrt.Space,'uint16');
    fwrite(file.fid, TupleCrt.Latitude,'int32');
    fwrite(file.fid, TupleCrt.Longitude,'int32');
    fwrite(file.fid, TupleCrt.TupleAttribute,'int32');
    fwrite(file.fid, TupleCrt.BackLink,'uint32');
end

% Conservation des donn�es dans une structure 
TuplePrev = TupleCrt;

flag = 1;