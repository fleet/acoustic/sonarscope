function flag = Em2hac_210(file, iTuple, NbFaisceaux, Data)

persistent TuplePrev;

if (iTuple == 1)
    TuplePrev = [];
end
flag = 0;


% Tuple 210 EK60 MBES Echo sounder
TupleSize                   = 58;
TupleType                   = 210;
% Enregistrement des informations utiles du Tuple pour comparaison
% avec le Ping pr�c�dent.
TupleCrt.NumberSWChannels            = NbFaisceaux;
TupleCrt.EchosounderDocIdentifier    = 2;                        % 2; %999;
TupleCrt.SoundSpeed                  = Data.DataSample.SoundVelocity(1)*10;                % en 0.1 m/s
TupleCrt.TriggerMode                 = 65535;                    % not available
TupleCrt.PingInterval                = 0.0;                      % not available
TupleCrt.Remarks                     = blanks(40);               % 40 characters max
TupleCrt.TupleAttribute              = 0;
TupleCrt.Backlink                    = 68;



% Ecriture des tuples que si les donn�es diff�rents du datagramme
% pr�c�dent.
if ~isempty(TuplePrev) || ~isequal(TuplePrev, TupleCrt)
        fwrite(file.fid, TupleSize,  'uint32');
        fwrite(file.fid, TupleType,  'uint16');
        
        fwrite(file.fid, TupleCrt.NumberSWChannels,          'uint16');
        fwrite(file.fid, TupleCrt.EchosounderDocIdentifier,  'uint32');
        fwrite(file.fid, TupleCrt.SoundSpeed,                'uint16');
        fwrite(file.fid, TupleCrt.TriggerMode,               'uint16');
        fwrite(file.fid, TupleCrt.PingInterval,              'uint16');
        fwrite(file.fid, 0,                                  'uint16'); % Space

        fwrite(file.fid, TupleCrt.Remarks,                   'char');
        fwrite(file.fid, TupleCrt.TupleAttribute,            'int32');
        fwrite(file.fid, TupleCrt.Backlink,                  'uint32');
end

% Conservation des donn�es dans une structure
TuplePrev = TupleCrt;

flag = 1;