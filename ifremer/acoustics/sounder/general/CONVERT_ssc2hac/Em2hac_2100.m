function flag = Em2hac_2100(file, iTuple, NbFaisceaux, Data)

persistent TuplePrev;
if (iTuple == 1)
    TuplePrev = [];
end

flag = 0;


% Allocation de m�moire.
TupleCrt(NbFaisceaux) = struct( 'SoftwareChannelIdentifier', [], ...
    'EchosounderDocIdentifier', [], ...
    'FrequencyChannelName', [], ...
    'TransceiverSoftwareVersion', [], ...
    'TransducerName', [], ...
    'TimeSampleInterval', [], ...
    'DataType', [], ...
    'BeamType', [], ...
    'TransducerInstallationDepth', [], ...
    'AcousticFrequency', [], ...
    'StartSample', [], ...
    'PlatformIdentifier', [], ...
    'TransducerShape', [], ...
    'TransducerAlongShipAngleOffset', [], ...
    'TransducerAthwartShipAngleOffset', [], ...
    'TransducerRotationAngle', [], ...
    'TransducerMainBeamAxisAlongShipAngleOffset', [], ...
    'TransducerMainBeamAxisAthwartShipAngleOffset', [], ...
    'AbsorptionCoefficent', [], ...
    'PulseDuration', [], ...
    'Bandwith', [], ...
    'TransmissionPower', [], ...
    'BeamAlongshipAngleSensitivity', [], ...
    'BeamAthwartshipAngleSensitivity', [], ...
    'BeamAlongship3dBBeamWidth', [], ...
    'BeamAthwartship3dBBeamWidth', [], ...
    'BeamEquivalentTwoWayBeamAngle', [], ...
    'BeamGain', [], ...
    'BeamSACorrection', [], ...
    'BottomDetectionMinimumDepth', [], ...
    'BottomDetectionMaximumDepth', [], ...
    'BottomDetectionMinimumLevel', [], ...
    'Remarks', [], ...
    'TupleAttribute', [], ...
    'Backlink', []);


for k = 1:NbFaisceaux
    TupleSize                                               = 258;
    TupleType                                               = 2100;
    TupleCrt(k).SoftwareChannelIdentifier                   = k;                % num�ro du faisceau  - à it�rer de 1 � 128
    TupleCrt(k).EchosounderDocIdentifier                    = 2;                %999; %2; %999; % Identifiant sondeur
    TupleCrt(k).FrequencyChannelName                        = Data.Config.FrequencyChannelName{k}(1:48);       % 48 characters
    TupleCrt(k).TransceiverSoftwareVersion                  = Data.Config.TransceiverSoftwareVersion{1}(1:30);       % 48 characters
    TupleCrt(k).TransducerName                              = blanks(30);
    TupleCrt(k).TransducerName(1:numel(Data.Config.TransducerName{k}))  = Data.Config.TransducerName{k};       % 48 characters
    % TupleCrt(k).TransducerName                              = blanks(30); % Data.Config.TransducerName;
    TupleCrt(k).TimeSampleInterval                          = Data.DataSample.TimeSampleInterval(k);       % 48 characters
    TupleCrt(k).DataType                                    = 2;                % 1 = Electrical power, 2 = Sv
    TupleCrt(k).BeamType                                    = 0;
    TupleCrt(k).AcousticFrequency                           = Data.DataSample.AcousticFrequency(k);
    TupleCrt(k).StartSample                                 = 0;                % 0 = no offset
    TupleCrt(k).PlatformIdentifier                          = 1;    % pour EK60
    TupleCrt(k).TransducerShape                             = 1;    % Forme ovale pour EK60
    
    TupleCrt(k).TransducerAlongShipAngleOffset              = 0;
    TupleCrt(k).TransducerAthwartShipAngleOffset            = 0;
    TupleCrt(k).TransducerInstallationDepth                 = Data.DataSample.TransducerDepth(k);
    TupleCrt(k).TransducerRotationAngle                     = 0;
    TupleCrt(k).TransducerMainBeamAxisAlongShipAngleOffset  = Data.Config.AngleOffsetAlongship(k);
    TupleCrt(k).TransducerMainBeamAxisAthwartShipAngleOffset = Data.Config.AngleOffsetAthwartship(k);
    TupleCrt(k).AbsorptionCoefficient                       = Data.DataSample.AbsorptionCoefficient(k);        % en 0.0001 dB/km Absorption of sound in the propagation medium                % en 0.0001 dB/km Absorption of sound in the propagation medium
    TupleCrt(k).PulseDuration                               = Data.DataSample.PulseDuration(k);               
    TupleCrt(k).Bandwidth                                   = Data.DataSample.Bandwidth(k);                 
    TupleCrt(k).TransmissionPower                           = Data.DataSample.TransmitPower(k);                                % Transmit power referred to the transducer terminals - W
    TupleCrt(k).BeamAlongshipAngleSensitivity               = Data.Config.AngleSensitivityAlongship(k);            % Electrical phase angle in degrees for one mechanical angle in the alongship (fore-aft) direction.
    TupleCrt(k).BeamAthwartshipAngleSensitivity             = Data.Config.AngleSensitivityAthwartship(k);
    TupleCrt(k).BeamAlongship3dBBeamWidth                   = Data.Config.BeamwidthAlongShip(k);                   % Half power (3dB) beam width of the transducer in the alongship direction.
    TupleCrt(k).BeamAthwartship3dBBeamWidth                 = Data.Config.BeamwidthAthwartShip(k);
    TupleCrt(k).BeamEquivalentTwoWayBeamAngle               = Data.Config.EquivalentBeamAngle(k);                  % Equivalent two way beam opening solid angle. MacLennan and Simmonds, Fisheries Acoustics1992, section 2.3.
    TupleCrt(k).BeamGain                                    = Data.DataSample.Gain(k);                             % Transducer gain used in power budget calculations for calculation of TS.
    TupleCrt(k).BeamSACorrection                            = Data.DataSample.SaCorrection(k);
    TupleCrt(k).BottomDetectionMinimumDepth                 = 2^32-1;           % Unvalaible value
    TupleCrt(k).BottomDetectionMaximumDepth                 = 2^32-1;           % Unvalaible value
    TupleCrt(k).BottomDetectionMinimumLevel                 = -2^31;            % Unvalaible value
    TupleCrt(k).Remarks                                     = blanks(40);       % 40 characters
    TupleCrt(k).TupleAttribute                              = 0;
    TupleCrt(k).Backlink                                    = 268;
    
    
    % Ecriture des tuples que si les donn�es diff�rents du datagramme
    % pr�c�dent.
    if ~isempty(TuplePrev) 
        if ~isequal(TuplePrev(k), TupleCrt(k))
            fwrite(file.fid, TupleSize,'uint32');
            fwrite(file.fid, TupleType,'uint16');
            fwrite(file.fid, TupleCrt(k).SoftwareChannelIdentifier,'uint16');
            fwrite(file.fid, TupleCrt(k).EchosounderDocIdentifier,'uint32');
            fwrite(file.fid, TupleCrt(k).FrequencyChannelName,'char');
            fwrite(file.fid, TupleCrt(k).TransceiverSoftwareVersion,'char');
            fwrite(file.fid, TupleCrt(k).TransducerName,'char');
            fwrite(file.fid, TupleCrt(k).TimeSampleInterval,'uint32');
            fwrite(file.fid, TupleCrt(k).DataType,'uint16');
            fwrite(file.fid, TupleCrt(k).BeamType,'uint16');
            fwrite(file.fid, TupleCrt(k).AcousticFrequency,'uint32');
            fwrite(file.fid, TupleCrt(k).TransducerInstallationDepth,'uint32');
            fwrite(file.fid, TupleCrt(k).StartSample,'uint32');
            fwrite(file.fid, TupleCrt(k).PlatformIdentifier,'uint16');
            fwrite(file.fid, TupleCrt(k).TransducerShape,'uint16');
            fwrite(file.fid, TupleCrt(k).TransducerAlongShipAngleOffset,'int32');
            fwrite(file.fid, TupleCrt(k).TransducerAthwartShipAngleOffset,'int32');
            fwrite(file.fid, TupleCrt(k).TransducerRotationAngle,'int32');
            fwrite(file.fid, TupleCrt(k).TransducerMainBeamAxisAlongShipAngleOffset,'int32');
            fwrite(file.fid, TupleCrt(k).TransducerMainBeamAxisAthwartShipAngleOffset','int32');
            fwrite(file.fid, TupleCrt(k).AbsorptionCoefficient,'uint32');
            fwrite(file.fid, TupleCrt(k).PulseDuration,'uint32');
            fwrite(file.fid, TupleCrt(k).Bandwidth,'uint32');
            fwrite(file.fid, TupleCrt(k).TransmissionPower,'uint32');
            fwrite(file.fid, TupleCrt(k).BeamAlongshipAngleSensitivity,'uint32');
            fwrite(file.fid, TupleCrt(k).BeamAthwartshipAngleSensitivity,'uint32');
            fwrite(file.fid, TupleCrt(k).BeamAlongship3dBBeamWidth,'uint32');
            fwrite(file.fid, TupleCrt(k).BeamAthwartship3dBBeamWidth,'uint32');
            fwrite(file.fid, TupleCrt(k).BeamEquivalentTwoWayBeamAngle,'int32');
            fwrite(file.fid, TupleCrt(k).BeamGain,'uint32');
            fwrite(file.fid, TupleCrt(k).BeamSACorrection,'int32');
            fwrite(file.fid, TupleCrt(k).BottomDetectionMinimumDepth,'uint32');
            fwrite(file.fid, TupleCrt(k).BottomDetectionMaximumDepth,'uint32');
            fwrite(file.fid, TupleCrt(k).BottomDetectionMinimumLevel,'int32');
            fwrite(file.fid, TupleCrt(k).Remarks,'char');
            fwrite(file.fid, TupleCrt(k).TupleAttribute,'int32');
            fwrite(file.fid, TupleCrt(k).Backlink,'uint32');
        end
    else
        fwrite(file.fid, TupleSize,'uint32');
        fwrite(file.fid, TupleType,'uint16');
        fwrite(file.fid, TupleCrt(k).SoftwareChannelIdentifier,'uint16');
        fwrite(file.fid, TupleCrt(k).EchosounderDocIdentifier,'uint32');
        fwrite(file.fid, TupleCrt(k).FrequencyChannelName,'char');
        fwrite(file.fid, TupleCrt(k).TransceiverSoftwareVersion,'char');
        fwrite(file.fid, TupleCrt(k).TransducerName,'char');
        fwrite(file.fid, TupleCrt(k).TimeSampleInterval,'uint32');
        fwrite(file.fid, TupleCrt(k).DataType,'uint16');
        fwrite(file.fid, TupleCrt(k).BeamType,'uint16');
        fwrite(file.fid, TupleCrt(k).AcousticFrequency,'uint32');
        fwrite(file.fid, TupleCrt(k).TransducerInstallationDepth,'uint32');
        fwrite(file.fid, TupleCrt(k).StartSample,'uint32');
        fwrite(file.fid, TupleCrt(k).PlatformIdentifier,'uint16');
        fwrite(file.fid, TupleCrt(k).TransducerShape,'uint16');
        fwrite(file.fid, TupleCrt(k).TransducerAlongShipAngleOffset,'int32');
        fwrite(file.fid, TupleCrt(k).TransducerAthwartShipAngleOffset,'int32');
        fwrite(file.fid, TupleCrt(k).TransducerRotationAngle,'int32');
        fwrite(file.fid, TupleCrt(k).TransducerMainBeamAxisAlongShipAngleOffset,'int32');
        fwrite(file.fid, TupleCrt(k).TransducerMainBeamAxisAthwartShipAngleOffset','int32');
        fwrite(file.fid, TupleCrt(k).AbsorptionCoefficient,'uint32');
        fwrite(file.fid, TupleCrt(k).PulseDuration,'uint32');
        fwrite(file.fid, TupleCrt(k).Bandwidth,'uint32');
        fwrite(file.fid, TupleCrt(k).TransmissionPower,'uint32');
        fwrite(file.fid, TupleCrt(k).BeamAlongshipAngleSensitivity,'uint32');
        fwrite(file.fid, TupleCrt(k).BeamAthwartshipAngleSensitivity,'uint32');
        fwrite(file.fid, TupleCrt(k).BeamAlongship3dBBeamWidth,'uint32');
        fwrite(file.fid, TupleCrt(k).BeamAthwartship3dBBeamWidth,'uint32');
        fwrite(file.fid, TupleCrt(k).BeamEquivalentTwoWayBeamAngle,'int32');
        fwrite(file.fid, TupleCrt(k).BeamGain,'uint32');
        fwrite(file.fid, TupleCrt(k).BeamSACorrection,'int32');
        fwrite(file.fid, TupleCrt(k).BottomDetectionMinimumDepth,'uint32');
        fwrite(file.fid, TupleCrt(k).BottomDetectionMaximumDepth,'uint32');
        fwrite(file.fid, TupleCrt(k).BottomDetectionMinimumLevel,'int32');
        fwrite(file.fid, TupleCrt(k).Remarks,'char');
        fwrite(file.fid, TupleCrt(k).TupleAttribute,'int32');
        fwrite(file.fid, TupleCrt(k).Backlink,'uint32');
    end

end

% Conservation des donn�es dans une structure
TuplePrev = TupleCrt;

flag = 1;
