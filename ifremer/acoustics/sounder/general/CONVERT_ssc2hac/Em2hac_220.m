function flag = Em2hac_220(file, iTuple, NbFaisceaux, Data)

persistent TuplePrev;
flag = 0;


if (iTuple == 1)
    TuplePrev = [];
end



% Tuple 220 MBES Echo sounder
TupleSize                   = 174;
TupleType                   = 220;
% Enregistrement des informations utiles du Tuple pour comparaison
% avec le Ping pr�c�dent.
TupleCrt.NumberSWChannels            = NbFaisceaux;
TupleCrt.EchosounderDocIdentifier    = 2; %999; %2; %999;
TupleCrt.TransducerName              = Name(1:5);                % InfoPing.TransducerName;
TupleCrt.TransceiverSoftVersion      = blanks(30);
TupleCrt.SoundSpeed                  = Data.WaterColumn.soundSpeed(iTuple)*10;
TupleCrt.TriggerMode                 = 65535;                    % not available
TupleCrt.PingInterval                = 0.0;                      % not available
TupleCrt.PulseForm                   = 1;                        % CW
TupleCrt.PulseDuration               = 200;                      % en us
TupleCrt.TimeSampleInterval          = (1/Data.WaterColumn.sampling(iTuple))*1000000;  % time between each sample en us
TupleCrt.FrequencyBeamSpacing        = 65535;                    % not available
TupleCrt.FrequencySpaceShape         = 65535;                    % not available
TupleCrt.TransceiverPowerPercentage  = 1;                        % de 0 à 1
TupleCrt.TransducerInstallationDepth = Data.InstallParams.VerticalOffsetRelToAttSensor*1e4;
TupleCrt.PlatformIdentifier          = 65535;
TupleCrt.TransducerShape             = 1;                        % 1 = circular transducer, 2 = rectangular
TupleCrt.TransducerFaceAlongshipAngleOffset      = 0*10000;      % -180 à +180°
TupleCrt.TransducerFaceAthwartshipAngleOffset    = 0*10000;      %-InfoPing.PanelInclinaison*10000;   % -180 à +180°
TupleCrt.TransducerRotationAngle                 =  0*10000;     % -180 à +180°
TupleCrt.Remarks                     = blanks(40);               % 40 characters max
TupleCrt.TupleAttribute              = 0;
TupleCrt.Backlink                    = 184;



% Ecriture des tuples que si les donn�es diff�rents du datagramme
% pr�c�dent.
if ~isempty(TuplePrev) || ~isequal(TuplePrev, TupleCrt)
        fwrite(file.fid, TupleSize,'uint32');
        fwrite(file.fid, TupleType,'uint16');
        
        fwrite(file.fid, TupleCrt.NumberSWChannels,'uint16');
        fwrite(file.fid, TupleCrt.EchosounderDocIdentifier,'uint32');
        fwrite(file.fid, TupleCrt.TransducerName,'char');
        fwrite(file.fid, 0,'char');
        fwrite(file.fid, blanks(44),'char');
        fwrite(file.fid, TupleCrt.TransceiverSoftVersion,'char');
        fwrite(file.fid, TupleCrt.SoundSpeed,'uint16');
        fwrite(file.fid, TupleCrt.TriggerMode,'uint16');
        fwrite(file.fid, TupleCrt.PingInterval,'uint16');
        fwrite(file.fid, TupleCrt.PulseForm,'uint16');
        fwrite(file.fid, TupleCrt.PulseDuration,'uint32');
        fwrite(file.fid, TupleCrt.TimeSampleInterval,'uint32');
        fwrite(file.fid, TupleCrt.FrequencyBeamSpacing,'uint16');
        fwrite(file.fid, TupleCrt.FrequencySpaceShape,'uint16');
        fwrite(file.fid, TupleCrt.TransceiverPowerPercentage,'uint32');
        fwrite(file.fid, TupleCrt.TransducerInstallationDepth,'uint32');
        fwrite(file.fid, TupleCrt.PlatformIdentifier,'uint16');
        fwrite(file.fid, TupleCrt.TransducerShape,'uint16');
        fwrite(file.fid, TupleCrt.TransducerFaceAlongshipAngleOffset,'int32');
        fwrite(file.fid, TupleCrt.TransducerFaceAthwartshipAngleOffset,'int32');
        fwrite(file.fid, TupleCrt.TransducerRotationAngle,'int32');
        fwrite(file.fid, TupleCrt.Remarks,'char');
        fwrite(file.fid, TupleCrt.TupleAttribute,'int32');
        fwrite(file.fid, TupleCrt.Backlink,'uint32');
end

% Conservation des donn�es dans une structure
TuplePrev = TupleCrt;

flag = 1;