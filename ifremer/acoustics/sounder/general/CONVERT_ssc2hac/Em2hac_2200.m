function flag = Em2hac_2200(file, iTuple, NbFaisceaux, Data)

persistent TuplePrev;

flag = 0;


% Allocation de m�moire.
TupleCrt(NbFaisceaux) = struct( 'SoftwareChannelIdentifier', [], ...
    'EchosounderDocIdentifier', [], ...
    'FrequencyChannelName', [], ...
    'DataType', [], ...
    'BeamType', [], ...
    'AcousticFrequency', [], ...
    'StartSample', [], ...
    'AlongshipSteeringAngle', [], ...
    'AthwartshipSteeringAngle', [], ...
    'AbsorptionCoefficient', [], ...
    'Bandwidth', [], ...
    'TransmissionPower', [], ...
    'BeamAlongshipAngleSensitivity', [], ...
    'BeamAthwartshipAngleSensitivity', [], ...
    'BeamAlongship3dBBeamWidth', [], ...
    'BeamAthwartship3dBBeamWidth', [], ...
    'BeamEquivalentTwoWayBeamAngle', [], ...
    'BeamGain', [], ...
    'BeamSACorrection', [], ...
    'BottomDetectionMinimumDepth', [], ...
    'BottomDetectionMaximumDepth', [], ...
    'BottomDetectionMinimumLevel', [], ...
    'AlongshipTXRXWeightingIdentifier', [], ...
    'AthwartshipTXRXWeightingIdentifier', [], ...
    'SplitbeamAlongshipRXWeightingIdentifier', [], ...
    'SplitbeamAthwartshipRXWeightingIdentifier', [], ...
    'Remarks', [], ...
    'TupleAttribute', [], ...
    'Backlink', []);


for i = 1:NbFaisceaux
    TupleSize                                               = 178;
    TupleType                                               = 2200;
    TupleCrt(i).SoftwareChannelIdentifier                   = i;                % num�ro du faisceau  - à it�rer de 1 � 128
    TupleCrt(i).EchosounderDocIdentifier                    = 2;                %999; %2; %999; % Identifiant sondeur
    TupleCrt(i).FrequencyChannelName                        = blanks(48);       % 48 characters
    TupleCrt(i).DataType                                    = 2;                % 1 = Electrical power, 2 = Sv
    TupleCrt(i).BeamType                                    = 0;
    TupleCrt(i).AcousticFrequency                           = Data.WaterColumn.frequency(iTuple,i);
    TupleCrt(i).StartSample                                 = 0;                % 0 = no offset
    
    % D�finition du d�pointage des faisceaux
    TupleCrt(i).AlongshipSteeringAngle                      = Data.WaterColumn.angles_along(iTuple,i)*1e4;                                                               % (à la vertical du bateau) -180 à +180°
    TupleCrt(i).AthwartshipSteeringAngle                    = Data.WaterColumn.angles_athwart(iTuple,i)*1e4;
    TupleCrt(i).AbsorptionCoefficient                       = Data.RunTime.AbsorptionCoeff*10000;                % en 0.0001 dB/km Absorption of sound in the propagation medium                % en 0.0001 dB/km Absorption of sound in the propagation medium
    TupleCrt(i).Bandwidth                                   = 0;                % InfoPing.SampleRate; % en Hz
    TupleCrt(i).TransmissionPower                           = 000000 ;          % Transmit power referred to the transducer terminals - W
    TupleCrt(i).BeamAlongshipAngleSensitivity               = 000000;           % Electrical phase angle in degrees for one mechanical angle in the alongship (fore-aft) direction.
    TupleCrt(i).BeamAthwartshipAngleSensitivity             = 000000;
    TupleCrt(i).BeamAlongship3dBBeamWidth                   = Data.RunTime.BeamWidthAlong*10000;  % Half power (3dB) beam width of the transducer in the alongship direction.
    TupleCrt(i).BeamAthwartship3dBBeamWidth                 = (Data.RunTime.BeamWidthAthwart/cos(Data.WaterColumn.angles_athwart(iTuple,i)*pi/180))*10000;
    TupleCrt(i).BeamEquivalentTwoWayBeamAngle               = 0;                % Equivalent two way beam opening solid angle. MacLennan and Simmonds, Fisheries Acoustics1992, section 2.3.
    TupleCrt(i).BeamGain                                    = 000000;           % Transducer gain used in power budget calculations for calculation of TS.
    TupleCrt(i).BeamSACorrection                            = 0;
    TupleCrt(i).BottomDetectionMinimumDepth                 = 0.0000;
    TupleCrt(i).BottomDetectionMaximumDepth                 = 0.0000;
    TupleCrt(i).BottomDetectionMinimumLevel                 = 0;
    TupleCrt(i).AlongshipTXRXWeightingIdentifier            = 65535;    %1;
    TupleCrt(i).AthwartshipTXRXWeightingIdentifier          = 65535;    %2;
    TupleCrt(i).SplitbeamAlongshipRXWeightingIdentifier     = 65535;    %7;
    TupleCrt(i).SplitbeamAthwartshipRXWeightingIdentifier   = 65535;    %8;
    TupleCrt(i).Remarks                                     = blanks(40);       % 40 characters
    TupleCrt(i).TupleAttribute                              = 0;
    TupleCrt(i).Backlink                                    = 188;
    
    
    % Ecriture des tuples que si les donn�es diff�rents du datagramme
    % pr�c�dent.
    if ~isempty(TuplePrev) 
        if ~isequal(TuplePrev(i), TupleCrt(i))
            fwrite(file.fid, TupleSize,'uint32');
            fwrite(file.fid, TupleType,'uint16');
            fwrite(file.fid, TupleCrt(i).SoftwareChannelIdentifier,'uint16');
            fwrite(file.fid, TupleCrt(i).EchosounderDocIdentifier,'uint32');
            fwrite(file.fid, TupleCrt(i).FrequencyChannelName,'char');
            fwrite(file.fid, TupleCrt(i).DataType,'uint16');
            fwrite(file.fid, TupleCrt(i).BeamType,'uint16');
            fwrite(file.fid, TupleCrt(i).AcousticFrequency,'uint32');
            fwrite(file.fid, TupleCrt(i).StartSample,'uint32');
            fwrite(file.fid, TupleCrt(i).AlongshipSteeringAngle,'int32');
            fwrite(file.fid, TupleCrt(i).AthwartshipSteeringAngle,'int32');
            fwrite(file.fid, TupleCrt(i).AbsorptionCoefficient,'uint32');
            fwrite(file.fid, TupleCrt(i).Bandwidth,'uint32');
            fwrite(file.fid, TupleCrt(i).TransmissionPower,'uint32');
            fwrite(file.fid, TupleCrt(i).BeamAlongshipAngleSensitivity,'uint32');
            fwrite(file.fid, TupleCrt(i).BeamAthwartshipAngleSensitivity,'uint32');
            fwrite(file.fid, TupleCrt(i).BeamAlongship3dBBeamWidth,'uint32');
            fwrite(file.fid, TupleCrt(i).BeamAthwartship3dBBeamWidth,'uint32');
            fwrite(file.fid, TupleCrt(i).BeamEquivalentTwoWayBeamAngle,'int32');
            fwrite(file.fid, TupleCrt(i).BeamGain,'uint32');
            fwrite(file.fid, TupleCrt(i).BeamSACorrection,'int32');
            fwrite(file.fid, TupleCrt(i).BottomDetectionMinimumDepth,'uint32');
            fwrite(file.fid, TupleCrt(i).BottomDetectionMaximumDepth,'uint32');
            fwrite(file.fid, TupleCrt(i).BottomDetectionMinimumLevel,'int32');
            fwrite(file.fid, TupleCrt(i).AlongshipTXRXWeightingIdentifier,'uint16');
            fwrite(file.fid, TupleCrt(i).AthwartshipTXRXWeightingIdentifier,'uint16');
            fwrite(file.fid, TupleCrt(i).SplitbeamAlongshipRXWeightingIdentifier,'uint16');
            fwrite(file.fid, TupleCrt(i).SplitbeamAthwartshipRXWeightingIdentifier,'uint16');
            fwrite(file.fid, TupleCrt(i).Remarks,'char');
            fwrite(file.fid, TupleCrt(i).TupleAttribute,'int32');
            fwrite(file.fid, TupleCrt(i).Backlink,'uint32');
        end
    else
        fwrite(file.fid, TupleSize,'uint32');
        fwrite(file.fid, TupleType,'uint16');
        fwrite(file.fid, TupleCrt(i).SoftwareChannelIdentifier,'uint16');
        fwrite(file.fid, TupleCrt(i).EchosounderDocIdentifier,'uint32');
        fwrite(file.fid, TupleCrt(i).FrequencyChannelName,'char');
        fwrite(file.fid, TupleCrt(i).DataType,'uint16');
        fwrite(file.fid, TupleCrt(i).BeamType,'uint16');
        fwrite(file.fid, TupleCrt(i).AcousticFrequency,'uint32');
        fwrite(file.fid, TupleCrt(i).StartSample,'uint32');
        fwrite(file.fid, TupleCrt(i).AlongshipSteeringAngle,'int32');
        fwrite(file.fid, TupleCrt(i).AthwartshipSteeringAngle,'int32');
        fwrite(file.fid, TupleCrt(i).AbsorptionCoefficient,'uint32');
        fwrite(file.fid, TupleCrt(i).Bandwidth,'uint32');
        fwrite(file.fid, TupleCrt(i).TransmissionPower,'uint32');
        fwrite(file.fid, TupleCrt(i).BeamAlongshipAngleSensitivity,'uint32');
        fwrite(file.fid, TupleCrt(i).BeamAthwartshipAngleSensitivity,'uint32');
        fwrite(file.fid, TupleCrt(i).BeamAlongship3dBBeamWidth,'uint32');
        fwrite(file.fid, TupleCrt(i).BeamAthwartship3dBBeamWidth,'uint32');
        fwrite(file.fid, TupleCrt(i).BeamEquivalentTwoWayBeamAngle,'int32');
        fwrite(file.fid, TupleCrt(i).BeamGain,'uint32');
        fwrite(file.fid, TupleCrt(i).BeamSACorrection,'int32');
        fwrite(file.fid, TupleCrt(i).BottomDetectionMinimumDepth,'uint32');
        fwrite(file.fid, TupleCrt(i).BottomDetectionMaximumDepth,'uint32');
        fwrite(file.fid, TupleCrt(i).BottomDetectionMinimumLevel,'int32');
        fwrite(file.fid, TupleCrt(i).AlongshipTXRXWeightingIdentifier,'uint16');
        fwrite(file.fid, TupleCrt(i).AthwartshipTXRXWeightingIdentifier,'uint16');
        fwrite(file.fid, TupleCrt(i).SplitbeamAlongshipRXWeightingIdentifier,'uint16');
        fwrite(file.fid, TupleCrt(i).SplitbeamAthwartshipRXWeightingIdentifier,'uint16');
        fwrite(file.fid, TupleCrt(i).Remarks,'char');
        fwrite(file.fid, TupleCrt(i).TupleAttribute,'int32');        
        fwrite(file.fid, TupleCrt(i).Backlink,'uint32');
    end

end

% Conservation des donn�es dans une structure
TuplePrev = TupleCrt;

flag = 1;