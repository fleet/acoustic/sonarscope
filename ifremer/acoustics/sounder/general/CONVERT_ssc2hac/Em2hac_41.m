function flag = Em2hac_41(file, iTuple, NbFaisceaux, Data, varargin)

if strcmp(file.Class, 'cl_ExRaw')
    flag = ExRaw_Em2hac_41(file, iTuple, NbFaisceaux, Data);
elseif strcmp(file.Class, 'cl_simrad_all')
    flag = SimradAll_Em2hac_41(file, iTuple, NbFaisceaux, Data);
elseif strcmp(file.Class, 'cl_reson_s7k')
    flag = ResonS7k_Em2hac_41(file, iTuple, NbFaisceaux, Data, varargin{:});
else
    flag = 0;
end
