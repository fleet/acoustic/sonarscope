function flag = Em2hac_65534(file, iTuple, Data)

% Tuple 65534 End of file
TupleSize       = 12;
TupleType       = 65534;
TimeFraction    = Data.time_fraction(iTuple); %0.0000;
TimeCPU         = Data.time_cpu(iTuple);      %InfoPing.startdatatime + InfoPing.datatime;
ClosingMode     = 1;
TupleAttribure  = 0;
BackLink        = 22;

fwrite(file.fid, TupleSize,'uint32');
fwrite(file.fid, TupleType,'uint16');
fwrite(file.fid, TimeFraction,'uint16');
fwrite(file.fid, TimeCPU,'uint32');
fwrite(file.fid, ClosingMode,'uint16');
fwrite(file.fid, TupleAttribure,'int32');
fwrite(file.fid, BackLink,'uint32');

flag = 1;
