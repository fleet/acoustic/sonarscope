function flag = Em2hac_65535(file)

% tuple 65535 HAC signature
StartCode           = 172;
TupleSize           = 14;
TupleType           = 65535;
HACIdentifier       = 44204;
HACVersion          = 160;
AcqSoftVersion      = 100;
AcqSoftIdentifier   = 4278234284;
TupleAttribute      = 0;
BackLink            = 24;

fwrite(file.fid, StartCode,'uint32');
fwrite(file.fid, TupleSize,'uint32');
fwrite(file.fid, TupleType,'uint16');
fwrite(file.fid, HACIdentifier,'uint16');
fwrite(file.fid, HACVersion,'uint16');
fwrite(file.fid, AcqSoftVersion,'uint16');
fwrite(file.fid, AcqSoftIdentifier,'uint32');
fwrite(file.fid, TupleAttribute,'int32');
fwrite(file.fid, BackLink,'uint32');

flag = 1;
