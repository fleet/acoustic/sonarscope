function flag = Em2hac_901(file, iTuple, NbFaisceaux, SounderName, Data, varargin)

if strcmp(file.Class, 'cl_ExRaw') || strcmp(file.Class, 'cl_simrad_all')
    flag = SimradAll_Em2hac_901(file, iTuple, NbFaisceaux, SounderName, Data, varargin{:});
elseif strcmp(file.Class, 'cl_reson_s7k')
    flag = ResonS7k_Em2hac_901(file, iTuple, NbFaisceaux, SounderName, Data, varargin{:});
else
    flag = 0;
end
