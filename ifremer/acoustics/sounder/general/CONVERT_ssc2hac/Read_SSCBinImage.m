function [flag, X] = Read_SSCBinImage(Datagrams, nomDirImage, Name, Type)


identImage = findIndVariable(Datagrams.Images, Name);
if isempty(identImage)
    X    = [];
    flag = 0;
    return
end

% Calcul du nombre d'�chantillons du signal pour la lecture dans le
% fichier.
remain      = Datagrams.Images(identImage).Dimensions;
dim         = textscan(remain, '%s', 'delimiter', ',');
d = 1;
dimName = dim{1}{d,1};
NbPings = Datagrams.Dimensions.(dimName);

d = 2;
dimName = dim{1}{d,1};
NbBeams = Datagrams.Dimensions.(dimName);



% Lecture pr�alable des valeurs.
if isVariableConstante(Datagrams.Images(identImage))
    X = repmat(Datagrams.Images(identImage).Constant, NbPings, NbBeams);
else
    [flag, X] = readImage(nomDirImage, Datagrams.Images(identImage),NbPings, NbBeams);
    % Suppression des valeurs de Nan.
    X   = my_transpose(X);
    sub = ~isnan(X);
    X   = X(sub);
    if ~flag
        return
    end
end

% Inhibition du facteur d'�chelle et de l'offset de valeur.
if ~(isfield(Datagrams.Images(identImage), 'Unit') && strcmpi(Datagrams.Images(identImage).Unit, 'days since JC'))
    X  = val2code(X, Datagrams.Images(identImage), Type);
end

flag    = 1;
