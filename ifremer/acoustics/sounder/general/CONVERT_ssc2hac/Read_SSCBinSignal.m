% Lecture de fichiers binaires � partir d'une arborescence SSC_.
%
% Syntax
%        [flag, Signal] = Read_SSCBinSignal(...);
%
% Input Arguments
%     Datagrams       : structure des datagrammes d�j� lus.
%     SignalName      : nom du signal dans la structure.
%     SignalType      : type du stockage du signal.
%     FlagProcessTime : flag de traitement ou non des signaux de Time (traitement en cl_time).
%
% Output Arguments
%   flag : statut r�sultat de la proc�dure,
%   Signal : variable de signal lue.
%
% Examples
%     nomDir = 'F:\SonarScopeData\From AndreOgor\SonarScope\0812_20091213_205617_Lesuroit'
%     [flag, tmpDatagrams] = loadDatagrams(nomDir, 'Position');
%     [f, S] = Read_SSCBinSignal('Datagrams', tmpDatagrams, ...,
%                                             'SignalNomDir', nomDir, ...
%                                             'SignalName', 'Time', ...
%                                             'SignalType', 'double', ...
%                                             'FlagProcessTime', 0);
%
% See also ssc2hac.all Authors
% Authors : GLU
%-----------------------------------------------------------------------

function [flag, X] = Read_SSCBinSignal(varargin)
% OLD : function [flag, X] = Read_SSCBinSignal(Datagrams, signalNomDir, signalName, signalType)

[varargin, Datagrams]       = getPropertyValue(varargin, 'Datagrams',      []);
[varargin, signalNomDir]    = getPropertyValue(varargin, 'SignalNomDir',    my_tempdir);
[varargin, signalName]      = getPropertyValue(varargin, 'SignalName',      '');
[varargin, signalType]      = getPropertyValue(varargin, 'SignalType',      'uint8');
[varargin, flagProcessTime] = getPropertyValue(varargin, 'FlagProcessTime', 0); %#ok<ASGLU>

identSignal = findIndVariable(Datagrams.Signals, signalName);
if isempty(identSignal)
    X    = [];
    flag = 0;
    return
end

% Calcul du nombre d'�chantillons du signal pour la lecture dans le
% fichier.
remain      = Datagrams.Signals(identSignal).Dimensions;
dim         = textscan(remain, '%s', 'delimiter', ',');
dimName = char(dim{1}(1));
if Datagrams.Dimensions.(dimName) >= 1
    NbSamples = Datagrams.Dimensions.(dimName);
end
if numel(dim{:}) > 1
    if strcmp(char(dim{1}(2)), '1')
        nbColumns = 1;
    else
        dimName = char(dim{1}(2));
        nbColumns = Datagrams.Dimensions.(dimName);
    end
end

% Lecture pr�alable des valeurs.
if isVariableConstante(Datagrams.Signals(identSignal))
    X = repmat(Datagrams.Signals(identSignal).Constant, NbSamples, 1);
else
    [flag, X] = readSignal(signalNomDir, Datagrams.Signals(identSignal), NbSamples, 'nbColumns', nbColumns);
    if ~flag
        return
    end
end

% Inhibition du facteur d'�chelle et de l'offset de valeur.
if ~(isfield(Datagrams.Signals(identSignal), 'Unit') && strcmpi(Datagrams.Signals(identSignal).Unit, 'days since JC'))
    X  = val2code(X, Datagrams.Signals(identSignal), signalType);
else
    % D�composition des variables de type cl_time en Date/Millisecs sinon
    % on laisse la variable en timeMat.
    if flagProcessTime
        % Lecture et traitement (peut �tre assez long) d'une donn�e de type Time.
        if size(X.timeMat, 1) > 10000
            flagWaitbar = 1;
        else
            flagWaitbar = 0;
        end
        A    = X.timeMat;
        B    = dayIfr2str(dayMat2Ifr(A));
        C    = char(B);
        
        yyyymmjj        = str2double(str2cell(C(:,end-3:end)))*10000 + str2double(str2cell(C(:,4:5)))*100 + str2double(str2cell(C(:,1:2)));
        heure           = heure2str(X);
        heure0          = repmat(hourStr2Ifr( '00:00:00' ), size(heure, 1),1);
        C               = my_dayStr2Ifr(C, flagWaitbar);
        NbMilliSecs     = timeIfr2Duration([ C, my_hourStr2Ifr( heure, flagWaitbar )], [C, heure0]);
        
        clear X;
        X.Date          = yyyymmjj;
        X.NbMilliSecs   = NbMilliSecs;
    end
end

flag    = 1;
