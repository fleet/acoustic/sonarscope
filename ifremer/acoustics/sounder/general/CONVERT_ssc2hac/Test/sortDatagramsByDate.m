% Data1 = struct('Date', {0001 0002 0003 0004 0005 0006 0007 0010}, 'Type', 20);
% Data2 = struct('Date', {0001 0002 0006 0007 0008 0009 0010}, 'Type',10000);

clear Data
Data = struct('Date', {Data1.Date Data2.Date}, 'Type', {Data1.Type Data2.Type});

[unused, order] = sort([Data.Date]);
DataOut = Data(order);

%%
PositionTime= {0001 0002 0003 0004 0005 0006 0007 0010};
WCTime= {0001 0002 0006 0007 0008 0009 0010};
Data1 = struct('Date', PositionTime, 'Type', 20);
Data2 = struct('Date', {WCTime}, 'Type',10000);
clear Data
Data = struct('Date', [PositionTime WCTime]);

[unused, order] = sort([Data.Date]);
DataOut = Data(order);
