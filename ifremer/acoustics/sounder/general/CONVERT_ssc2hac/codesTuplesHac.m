% Codes des tuples de l'EM300
%
% Syntax
%   texteTypetuple = codestuplesHac(...)
%
% Name-Value Pair Arguments
%   tabHistoTypeTuple : Histogramme des types de tuple
%
% Name-Value Pair Arguments
%   fullListe : 0=affichage que des tuples presents, 1=tous les tuples
%
% Output Arguments
%   texteTypetuple : Chaine de caracteres
%     20		Position tuple
%     41		Platform attitude parameters tuple
%     42		Dynamic platform position parameters tuple
%     50		Trawl geometry tuple
%     100		Biosonics Model 102 Echosounder tuple
%     200		Simrad EK500 Echosounder tuple
%     210		Simrad EK 60 Echosounder tuple
%     220       Simrad ME70 Echo sounder tuple
%     901		Generic Echosounder tuple
%     1000		Biosonics Model 102 Channel tuple
%     1001		Biosonics Model 102 Channel tuple
%     2000		Simrad EK500 Channel tuple
%     2001		Simrad EK500 Channel tuple
%     2002		Simrad EK500 Channel patch tuple
%     2100		Simrad EK60 Channel tuple
%     2200      Simrad ME70 Channel tuple
%     2210		Simrad ME70 transducer weighting array
%     4000		Simrad EK500 Split-beam detected single target parameters sub-channel tuple
%     9001		Generic Channel tuple for the generic echosounder.
%     10000		Ping tuple U-32
%     10001		Ping tuple U-32-16-angles
%     10010		Ping tuple C-32
%     10011		Ping tuple C-32-16-angles
%     10030		Ping tuple U-16
%     10031		Ping tuple U-16-angles
%     10040		Ping tuple C-16
%     10090		Table 28. Split-beam detected single-target tuple
%     10100		General Threshold tuple
%                 Event Marker Tuple
%     10140		Attitude sensor tuple
%     10142		Platform position tuple
%     11000		STD profile tuple
%     65397		Private tuple
%     65534		End of file tuple
%     65535		HAC Signature tuple
%
% Examples
%   codesTuplesHac
%   codesTuplesHac('fullListe', 1);
%
% See also Authors
% Authors : JMA
%-----------------------------------------------------------------------

function texteTypetuple = codesTuplesHac(varargin)

[varargin, fullListe] = getPropertyValue(varargin, 'fullListe', 0);

LabelsTypeTuple(1).Code     = 20;
LabelsTypeTuple(end).Name   = 'Position tuple';
LabelsTypeTuple(end+1).Code = 30;
LabelsTypeTuple(end).Name   = 'Navigation tuple';
LabelsTypeTuple(end+1).Code = 41;
LabelsTypeTuple(end).Name   = 'Platform attitude parameters tuple';
LabelsTypeTuple(end+1).Code = 42;
LabelsTypeTuple(end).Name   = 'Dynamic platform position parameters tuple';
LabelsTypeTuple(end+1).Code = 50;
LabelsTypeTuple(end).Name   = 'Trawl geometry tuple';
LabelsTypeTuple(end+1).Code = 100;
LabelsTypeTuple(end).Name   = 'Biosonics Model 102 Echosounder tuple';
LabelsTypeTuple(end+1).Code = 200;
LabelsTypeTuple(end).Name   = 'Simrad EK500 Echosounder tuple';
LabelsTypeTuple(end+1).Code = 210;
LabelsTypeTuple(end).Name   = 'Simrad EK 60 Echosounder tuple';
LabelsTypeTuple(end+1).Code = 220;
LabelsTypeTuple(end).Name   = 'Simrad ME70 Echo sounder tuple 220';
LabelsTypeTuple(end+1).Code = 910;
LabelsTypeTuple(end).Name   = 'Generic Echosounder tuple';
LabelsTypeTuple(end+1).Code = 1000;
LabelsTypeTuple(end).Name   = 'Biosonics Model 102 Channel tuple';
LabelsTypeTuple(end+1).Code = 1001;
LabelsTypeTuple(end).Name   = 'Biosonics Model 102 Channel tuple';
LabelsTypeTuple(end+1).Code = 2000;
LabelsTypeTuple(end).Name   = 'Simrad EK500 Channel tuple';
LabelsTypeTuple(end+1).Code = 2001;
LabelsTypeTuple(end).Name   = 'Simrad EK500 Channel tuple';
LabelsTypeTuple(end+1).Code = 2002;
LabelsTypeTuple(end).Name   = 'Simrad EK500 Channel patch tuple';
LabelsTypeTuple(end+1).Code = 2100;
LabelsTypeTuple(end).Name   = 'Simrad EK60 Channel tuple';
LabelsTypeTuple(end+1).Code = 2200;
LabelsTypeTuple(end).Name   = 'Simrad ME70 Channel tuple';
LabelsTypeTuple(end+1).Code = 4000;
LabelsTypeTuple(end).Name   = 'Simrad EK500 Split-beam detected single target parameters sub-channel tuple';
LabelsTypeTuple(end+1).Code = 2210;
LabelsTypeTuple(end).Name   = 'Simrad ME70 transducer weighting array';
LabelsTypeTuple(end+1).Code = 9001;
LabelsTypeTuple(end).Name   = 'Generic Channel tuple for the generic echosounder.';
LabelsTypeTuple(end+1).Code = 10000;
LabelsTypeTuple(end).Name   = 'Ping tuple U-32';
LabelsTypeTuple(end+1).Code = 10001;
LabelsTypeTuple(end).Name   = 'Ping tuple U-32-16-angles';
LabelsTypeTuple(end+1).Code = 10010;
LabelsTypeTuple(end).Name   = 'Ping tuple C-32';
LabelsTypeTuple(end+1).Code = 10011;
LabelsTypeTuple(end).Name   = 'Ping tuple C-32-16-angles';
% % LabelsTypeTuple(end+1).Code = 10030;
% % LabelsTypeTuple(end).Name   = 'Ping tuple U-16';
% % LabelsTypeTuple(end+1).Code = 10040;
% % LabelsTypeTuple(end).Name   = 'Ping tuple U-16-angles';
LabelsTypeTuple(end+1).Code = 10040;
LabelsTypeTuple(end).Name   = 'Ping tuple C-16';
LabelsTypeTuple(end+1).Code = 10090;
LabelsTypeTuple(end).Name   = 'Split-beam detected single-target tuple';
LabelsTypeTuple(end+1).Code = 10100;
LabelsTypeTuple(end).Name   = 'General Threshold tuple';
LabelsTypeTuple(end+1).Code = 10110;
LabelsTypeTuple(end).Name   = 'Event Marker Tuple';
LabelsTypeTuple(end+1).Code = 10140;
LabelsTypeTuple(end).Name   = 'Attitude sensor tuple';
LabelsTypeTuple(end+1).Code = 10142;
LabelsTypeTuple(end).Name   = 'Platform position tuple';
LabelsTypeTuple(end+1).Code = 11000;
LabelsTypeTuple(end).Name   = 'STD profile tuple';
LabelsTypeTuple(end+1).Code = 65397;
LabelsTypeTuple(end).Name   = 'Private tuple';
LabelsTypeTuple(end+1).Code = 65534;
LabelsTypeTuple(end).Name   = 'End of file tuple';
LabelsTypeTuple(end+1).Code = 65535;
LabelsTypeTuple(end).Name   = 'HAC Signature tuple';




if ~isempty(varargin)
    code = varargin{1};
    tabHistoTypeTuple = varargin{2};
else
    code = [];
    tabHistoTypeTuple = zeros(size(LabelsTypeTuple));
end

texteTypetuple = '';
for i=1:length(LabelsTypeTuple)
    if isempty(LabelsTypeTuple(i))
        sub = find(LabelsTypeTuple(i).Code == code);
        if ~isempty(sub) && (tabHistoTypeTuple(sub) ~= 0)
            texteTypetuple = sprintf('%s\n%d = %s (%d)', ...
                texteTypetuple, code(sub), '?????????', tabHistoTypeTuple(sub));
        end
    else
        sub = find(LabelsTypeTuple(i).Code == code);
        if isempty(sub)
            if fullListe
                texteTypetuple = sprintf('%s\n%d = %s (0)', ...
                    texteTypetuple, i, LabelsTypeTuple(i).Name);
            end
        else
            if fullListe
                texteTypetuple = sprintf('%s\n%d = %s (%d)', ...
                    texteTypetuple, code(sub),LabelsTypeTuple(i).Name, tabHistoTypeTuple(sub));
            else
                if tabHistoTypeTuple(sub) ~= 0
                    texteTypetuple = sprintf('%s\n%d = %s (%d)', ...
                        texteTypetuple, code(sub), LabelsTypeTuple(i).Name, tabHistoTypeTuple(sub));
                    %                 texteTypetuple
                end
            end
        end
    end
end
