% Conversion d'une chaine de caracteres "JJ/MM/AAAA" en date codee "Ifremer"
% avec Waitbar de progression si le calcul s'av�re long.
%
% Syntax
%   dIfr = dayStr2Ifr( s, flag)
%
% Input Arguments
%	s       : chaine de caracteres
%	flag    : indicateur de barre progression ou non.
%
% Output Arguments
%	dIfr : Nombre de jours depuis le 24/11/-4713
%
% Examples
%   my_dayStr2Ifr('13/09/1998', 0)
%   my_dayStr2Ifr(['13/09/1998'; '14/09/1998'], 1)
%
% See also dayIfr2Jma dayJma2Ifr dayIfr2str hourIfr2Hms hourHms2Ifr hourIfr2str hourStr2Ifr Authors
% Authors : JMA
%--------------------------------------------------------------------------------

function d = my_dayStr2Ifr(s, showProgress)

if iscell(s)
    Jour = zeros(size(s));
    Mois = Jour;
    Annee = Jour;
    for i=1:numel(s)
        x = sscanf(s{i}, '%d%c%d%c%d');
        Jour(i) = x(1);
        Mois(i) = x(3);
        Annee(i) = x(5);
        d = dayJma2Ifr(Jour, Mois, Annee);
    end
else
    d = NaN(size(s,1), 1);
    
    N = size(s,1);
    if showProgress
        waitStrFr = sprintf('Traitement-conversion des dates');
        waitStrUs = sprintf('Process and conversion of dates');
        hw = create_waitbar(Lang(waitStrFr, waitStrUs), 'N', N);
    end
    
    for i=1:N
        if showProgress
            my_waitbar(i, size(s,1), hw);
        end
        x = sscanf(s(i,:), '%d%c%d%c%d');
        Jour = x(1);
        Mois = x(3);
        Annee = x(5);
        d(i) = dayJma2Ifr(Jour, Mois, Annee);
    end
    
    if showProgress
        my_close(hw, 'MsgEnd');
    end
    
end
