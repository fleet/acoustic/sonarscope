% Conversion d'une chaine de caracteres "HH:MM:SS.MMM" en heure codee "Ifremer"
% avec Waitbar de progression si le calcul s'av�re long.
%
% Syntax
%   hIfr = my_hourStr2Ifr( s, flag)
%
% Input Arguments
%	s : chaine de caracteres "HH:MM:SS.MMM"
%	flag    : indicateur de barre progression ou non.
%
% Output Arguments
%	hIfr : Nombre de millisecondes depuis minuit
%
% Examples
%   hourStr2Ifr('11:01:55.380', 0)
%   hourStr2Ifr(['11:01:55.380'; '13:11:05.999'], 1)
%
% See also hourIfr2Hms hourHms2Ifr hourIfr2str dayIfr2Jma dayJma2Ifr dayIfr2str dayStr2Ifr Authors
% Authors : JMA
%--------------------------------------------------------------------------------

function h = my_hourStr2Ifr( s, showProgress)

if iscell(s)
    Heure = zeros(size(s));
    Minute = Heure;
    Seconde = Heure;
    for i=1:numel(s)
        x = sscanf(s{i}, '%d%c%d%c%f');
        Heure(i) = x(1);
        Minute(i) = x(3);
        Seconde(i) = x(5);
        h = hourHms2Ifr(Heure, Minute, Seconde);
    end
else
    h = NaN(size(s,1), 1);
    N = size(s,1);
    
    if showProgress
        waitStrFr = sprintf('Traitement-conversion des heures');
        waitStrUs = sprintf('Process and conversion of hours');
        hw = create_waitbar(Lang(waitStrFr, waitStrUs), 'N', N);
    end
    
    for i=1:N
        if showProgress
            my_waitbar(i, N, hw);
        end
        x = sscanf(s(i,:), '%d%c%d%c%f');
        Heure   = x(1);
        Minute  = x(3);
        Seconde = x(5);
        h(i) = hourHms2Ifr(Heure, Minute, Seconde);
    end
    
    if showProgress       
        my_close(hw, 'MsgEnd');
    end
end


