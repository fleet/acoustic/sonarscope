% Traitement de tous les Tuples d'un fichier HAC � partir d'une arborescence SSC_ (extraite
% d'un fichier RAW ou ALL)
%
% Syntax
%   flag = processTuples(...)
%
% Name-Value Pair Arguments
%   file        : Nom d'un fichier ou liste de fichiers .raw.
%   nomDir      : Nom du r�pertoire de sortie
%   SeuildB     : seuil de compression (-100 dB par d�faut)
%   Tuple       : identifiant du Tuple en cours de traitement
%   NbFaisceaux : nb de faisceaux (ou de Channels)
%   SounderName : nom du sondeur
%   Data        : structure de donn�es �chantillon.
%
% Examples
%   nomFic  = getNomFicDatabase('FK-D20120510-T202356.raw')
%   flag    = ExRaw_ssc2hac('NomFicRaw', nomFic, 'RepDefaut', 'C:\Temp\Test_HAC_Creation');
%   % flag    = processTuples(file, nomDir, Ping, NbChannels, SounderName, DataTuple, SeuilSample);
%
% See also ssc2hac pour des fichiers ALL
% Authors  : BRG, GLU
%-----------------------------------------------------------------------

function flag = processTuples(file, nomDir, sDescTuple, NbFaisceaux, SounderName, Data, seuildB, varargin)

% Seuil angulaire pour les tuples compress�s de Phase
[varargin, seuilDeg]   = getPropertyValue(varargin, 'SeuilDeg',         []);
[varargin, dInterfero] = getPropertyValue(varargin, 'DistanceInterfero', 4); %#ok<ASGLU>

flag     = 0;%#ok<NASGU>
nbTuples = numel(sDescTuple);

%% Ecriture des signaux pour les datagrammes du layer en cours.

dummy       = regexp(nomDir, '\');

[flag, ~] = processUnitaireTuple(file, nomDir, 1, 65535, NbFaisceaux, SounderName, Data);

idxTupleWC      = 0;
NbSamplesLus    = 0;

%% Boucle sur le traitement des tuples.

nomFic = [nomDir(dummy(end)+1:end) '_s7k2hac.hac'];
waitStrFr = sprintf('%s\n - Ecriture s�quentielle des datagrammes', nomFic);
waitStrUs = sprintf('%s\n - Sequential writing for datagrams', nomFic);
hw = create_waitbar(Lang(waitStrFr, waitStrUs), 'N', nbTuples);
for k=1:nbTuples
    my_waitbar(k, nbTuples, hw);
    
    if k == 1
        % On �crit une fois pour toute les tuples de type Platform pour les conversion EwRaw et Simrad vers
        % HAC. Ces tuples sont �crits plus loin dans le cas du Reson pour g�rer le Multiping.
        idxTuple = sDescTuple(k).Index;
        if strcmp(file.Class,class(cl_ExRaw([]))) || strcmp(file.Class,class(cl_simrad_all([])))
            %% Ecriture des tuples g�n�riques de Platform Attitude.
            [flag, ~] = processUnitaireTuple(file, nomDir, idxTuple, 41, NbFaisceaux, SounderName, Data);
        end
    end
    
    % DEBUG disp([num2str(Tuple(k).Type) ' et ' num2str(k)]);
    if sDescTuple(k).Type == 20
        idxTuplePos = sDescTuple(k).Index;
        [flag, ~] = processUnitaireTuple(file, nomDir, idxTuplePos, 20, NbFaisceaux, SounderName, Data); %#ok<ASGLU>
        [flag, ~] = processUnitaireTuple(file, nomDir, idxTuplePos, 30, NbFaisceaux, SounderName, Data);
    end
    
    if sDescTuple(k).Type == 10000
        
        idxTupleWC = sDescTuple(k).Index;
        
        
        %% Ecriture des g�n�riques de Generic Echosounder et Channel et des tuples de Ping.
        
        switch file.Class
            case 'cl_simrad_all'
                % Ecriture des tuples g�n�riques de Generic Echosounder et Channel.
                [flag, ~] = processUnitaireTuple(file, nomDir, idxTupleWC, 901, NbFaisceaux, SounderName, Data);
                [flag, ~] = processUnitaireTuple(file, nomDir, idxTupleWC, 9001, NbFaisceaux, SounderName, Data);
                
                % Ecriture des tuples de Ping
                [flag, ~] = processUnitaireTuple(file, nomDir, idxTupleWC, 10100, NbFaisceaux, SounderName, Data.DataSample);%#ok<ASGLU>
                % Limitation de l'�criture du paquet Attitude � la dimension Max
                % du fichier ALL : cas du fichier EM2040 de Carla : 0012_20120608_163602_raw_12.all
                if idxTupleWC < numel(Data.Attitude.Ping) % Datagrams.Attitude.Dimensions.NbDatagrams
                    [flag, ~] = processUnitaireTuple(file, nomDir, idxTupleWC, 10140, NbFaisceaux, SounderName, Data);%#ok<ASGLU>
                end
                % On �crit des tuples compress�s C-16 pour cette conversion.
                % Le champ NbSamplesLus est it�r� au fil du parcours du fichier de WC qui contient tous les pings.
                [flag, NbSamplesLus] = processUnitaireTuple(file, nomDir, idxTupleWC, 10040, NbFaisceaux, SounderName, Data, ...
                    'OffsetNbSamples', NbSamplesLus, ...
                    'SeuildB', seuildB);
                
            case 'cl_reson_s7k'
                
                idxTupleMultiPingSequenceWC = [];
                % Gestion des Hac de type Multi-Ping (introduits par la conversion depuis Reson).
                if isfield(sDescTuple(k), 'WCMultiPingSequence') % S�curit� inutile
                    idxTupleMultiPingSequenceWC  = sDescTuple(k).WCMultiPingSequence;
                end
                if isfield(sDescTuple(k), 'WCPingIndex') % S�curit� inutile
                    % Cet index sert � pointer la matrice de donn�e d'amplitude indic�e par
                    % l'index de Ping WC sauvegard� (peut avoir sauter un ou plusieurs indice de Ping Depth).
                    idxPingWC  = sDescTuple(k).WCPingIndex;
                end
                
                if idxTupleWC >= 1
                    % Ecriture des tuples g�n�riques de Generic Echosounder et Channel.
                    % Pas de r��criture sur les tuples identiques (d�clinaison des matrices de comparaison
                    % avec le multiping).
                    [flag, ~] = processUnitaireTuple(file, nomDir, idxTupleWC, 901, NbFaisceaux, SounderName, Data, ...
                        'MultiPingSequence',    idxTupleMultiPingSequenceWC);
                    [flag, ~, is9001Writed]  = processUnitaireTuple(file, nomDir, idxTupleWC, 9001, NbFaisceaux, SounderName, Data, ...
                        'MultiPingSequence',    idxTupleMultiPingSequenceWC);
                    
                    %% Ecriture des tuples g�n�riques de Platform Attitude.
                    % On d�cline les �critures de type Attitude Platform en autant de sondeurs diff�rents et de faisceaux.
                    % On r�p�te l'�criture du tuple 41 d�s que le 9001 est �crit pour au moins un faisceau.
                    [flag, ~] = processUnitaireTuple(file, nomDir, idxTupleWC, 41, NbFaisceaux, SounderName, Data, ...
                        'MultiPingSequence',    idxTupleMultiPingSequenceWC, ...
                        'FlagForceWriting',     is9001Writed);
                    
                    % Ecriture des tuples de Ping
                    [flag, ~] = processUnitaireTuple(file, nomDir, idxTupleWC, 10100, NbFaisceaux, SounderName, Data, ...
                        'MultiPingSequence',    idxTupleMultiPingSequenceWC);%#ok<ASGLU>
                    
                    % Ecriture dun paquet Attitude
                    [flag, ~] = processUnitaireTuple(file, nomDir, idxTupleWC, 10140, NbFaisceaux, SounderName, Data);%#ok<ASGLU>
                    
                    
                    % On �crit des tuples d'amplitude compress�s pour cette conversion.
                    [flag, ~]   = processUnitaireTuple(file, nomDir, idxTupleWC, 10040, NbFaisceaux, SounderName, Data, ...
                        'SeuildB',              seuildB, ...
                        'WCPingIndex',          idxPingWC, ...
                        'MultiPingSequence',    idxTupleMultiPingSequenceWC);
                    
                    
                    % Ecriture des MagAndPhase si besoin.
                    if ~isempty(sDescTuple(k).MagAndPPingCounter)
                        % Nous sommes dans la m�me s�quence de Ping. Le Ping WC
                        % correspond au Ping MagAndPhase.
                        idxTupleMultiPingSequenceMagAndP = idxTupleMultiPingSequenceWC;
                        % Ecriture des tuples g�n�riques de Generic Echosounder et Channel.
                        % Pas de r��criture sur les tuples identiques (d�clinaison des matrices de comparaison
                        % avec le multiping).
                        [flag, ~] = processUnitaireTuple(file, nomDir, idxTupleWC, 901, NbFaisceaux, SounderName, Data, ...
                            'MultiPingSequence',    idxTupleMultiPingSequenceMagAndP, ...
                            'SubStruct',           'MagAndPhase');
                        
                        [flag, ~, is9001Writed]  = processUnitaireTuple(file, nomDir, idxTupleWC, 9001, NbFaisceaux, SounderName, Data, ...
                            'MultiPingSequence',    idxTupleMultiPingSequenceMagAndP, ...
                            'SubStruct',           'MagAndPhase');
                        
                        % On d�cline les �critures de type Attitude Platform en autant de sondeurs diff�rents et de faisceaux.
                        % On r�p�te l'�criture du tuple 41 d�s que le 9001 est �crit pour au moins un faisceau.
                        [flag, ~] = processUnitaireTuple(file, nomDir, idxTupleWC, 41, NbFaisceaux, SounderName, Data, ...
                            'FlagForceWriting',     is9001Writed,...
                            'MultiPingSequence',    idxTupleMultiPingSequenceMagAndP, ...
                            'SubStruct',           'MagAndPhase');
                        
                        % Ecriture des tuples de Ping
                        [flag, ~] = processUnitaireTuple(file, nomDir, idxTupleWC, 10100, NbFaisceaux, SounderName, Data, ...
                            'MultiPingSequence',    idxTupleMultiPingSequenceMagAndP, ...
                            'SubStruct',           'MagAndPhase');
                        
                        % Cet index sert � pointer la matrice de donn�e d'amplitude indic�e par
                        % l'index de Ping Mag And Phase.
                        % On �crit des tuples de mag et de phase compress�s pour cette conversion.
                        [flag, ~]   = processUnitaireTuple(file, nomDir, idxTupleWC, 10011, NbFaisceaux, SounderName, Data, ...
                            'SeuildB',              seuildB, ...
                            'SeuilDeg',             seuilDeg, ...
                            'WCPingIndex',          idxPingWC, ...
                            'DistanceInterfero',    dInterfero, ...
                            'MultiPingSequence',    idxTupleMultiPingSequenceMagAndP, ...
                            'SubStruct',           'MagAndPhase');
                    end
                end
            case 'cl_ExRaw'
                % Ecriture des tuples g�n�riques de Generic Echosounder et Channel.
                [flag, ~] = processUnitaireTuple(file, nomDir, idxTupleWC, 210, NbFaisceaux, SounderName, Data); %#ok<ASGLU>
                [flag, ~] = processUnitaireTuple(file, nomDir, idxTupleWC, 2100, NbFaisceaux, SounderName, Data);
                
                [flag, ~]   = processUnitaireTuple(file, nomDir, idxTupleWC, 10100, NbFaisceaux, SounderName, Data.Position);%#ok<ASGLU>
                [flag, ~]   = processUnitaireTuple(file, nomDir, idxTupleWC, 10140, NbFaisceaux, SounderName, Data);
                
                % Ecriture des tuples de Ping sous forme de tuples non-compress�s.
                % Echantillons d'amplitudes.
                [flag, ~]   = processUnitaireTuple(file, nomDir, idxTupleWC, 10030, NbFaisceaux, SounderName, Data);
                % Echantillons de phases.
                [flag, ~]   = processUnitaireTuple(file, nomDir, idxTupleWC, 10031, NbFaisceaux, SounderName, Data);
        end
        
        
        
        if ~flag
            fclose(file.fid);
            return
        end
    end
end % Fin de la boucle sur les datagrammes.
my_close(hw, 'MsgEnd');

if ~idxTupleWC
    [flag, ~] = processUnitaireTuple(file, nomDir, idxTuplePos, 65534, NbFaisceaux, SounderName, Data.Position);
else
    if strcmp(file.Class, 'cl_simrad_all') || strcmp(file.Class, 'cl_reson_s7k')
        [flag, ~] = processUnitaireTuple(file, nomDir, idxTupleWC, 65534, NbFaisceaux, SounderName, Data.DataSample);
    elseif strcmp(file.Class, 'cl_ExRaw')
        [flag, ~] = processUnitaireTuple(file, nomDir, idxTupleWC, 65534, NbFaisceaux, SounderName, Data.Position);
    end
end



