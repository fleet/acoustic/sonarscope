function [flag, NLus, isWrited] = processUnitaireTuple(file, nomDir, iTuple, CodeDatagram, ...
    NbFaisceaux, SounderName, Data, varargin)

global DEBUG %#ok<GVMIS> 
flag    = 0;

% Flag d'indication de l'�criture du tuple
isWrited = 0;


% Seuil d'amplitude pour les tuples compress�s de Phase
[varargin, SeuildB]          = getPropertyValue(varargin, 'SeuildB', []);
% Seuil angulaire pour les tuples compress�s de Phase
[varargin, SeuilDeg]         = getPropertyValue(varargin, 'SeuilDeg', []);
[varargin, NLus]             = getPropertyValue(varargin, 'OffsetNbSamples', 0);
[varargin, iMultiPing]       = getPropertyValue(varargin, 'MultipingSequence', 0);
[varargin, idxWCPing]        = getPropertyValue(varargin, 'WCPingIndex', []);
[varargin, flagForceWriting] = getPropertyValue(varargin, 'FlagForceWriting', 0);
[varargin, subStruct]        = getPropertyValue(varargin, 'SubStruct', 'DataSample'); %#ok<ASGLU>

% Ecriture des paquets.
switch CodeDatagram
    case 65535  % HAC Signature tuple
        if DEBUG
            fprintf('Ecriture HAC 65535 --> Signature de d�but de fichier HAC\n');
        end
        flag = Em2hac_65535(file);
        
    case 65534  % End of file tuple
        if DEBUG
            fprintf('Ecriture HAC 65534 --> Signature de fin de fichier HAC\n');
        end
        % !!! Attention, on passe directement la sous-structure Position ou DataSample
        % puisqu'on ne r�cup�re que les dates
        flag = Em2hac_65534(file, iTuple, Data);
        
        
    case 20     % Position
        if DEBUG
            fprintf('Ecriture HAC 20    --> GPS\n');
        end
        flag = Em2hac_20(file, iTuple, Data);
        
    case 30     % Navigation
        if DEBUG
            fprintf('Ecriture HAC 30    --> HEADING\n');
        end
        flag = Em2hac_30(file, iTuple, Data);
        
    case 41    % Platform attitude parameters tuple
        if DEBUG
            fprintf('Ecriture HAC 41    --> Coordonn�es de la plateforme attitudes sondeurs pour chaque faisceau (non d�finis pour SM20)\n');
        end
        flag = Em2hac_41(file, iTuple, NbFaisceaux, Data,   'MultipingSequence',    iMultiPing, ...
            'SubStruct',            subStruct, ...
            'FlagForceWriting',     flagForceWriting);
        
    case 210    % Simrad Ex60 Echo sounder tuple 220
        if DEBUG
            fprintf('Ecriture HAC 210   --> D�finition du sondeur de type Ex60\n');
        end
        flag = Em2hac_210(file, iTuple, NbFaisceaux, Data);
        
    case 2100   % Simrad Ex60 Channel tuple
        if DEBUG
            fprintf('Ecriture HAC 2100  --> Simrad Ex60 Channel tuple\n');
        end
        flag = Em2hac_2100(file, iTuple, NbFaisceaux, Data);
        
    case 220    % Simrad ME70 Echo sounder tuple 220
        if DEBUG
            fprintf('Ecriture HAC 220   --> D�finition du sondeur\n');
        end
        flag = Em2hac_220(file, iTuple, NbFaisceaux, Data);
        
    case 2200   % Simrad ME70 Channel tuple
        if DEBUG
            fprintf('Ecriture HAC 2200  --> Simrad ME70 Channel tuple\n');
        end
        flag = Em2hac_2200(file, iTuple, NbFaisceaux, Data);
        
    case 901    % Generic Echosounder tuple
        if DEBUG
            fprintf('Ecriture HAC 901   --> D�finition du sondeur g�n�rique\NLus');
        end
        flag = Em2hac_901(file, iTuple, NbFaisceaux, SounderName, Data, ...
            'SubStruct',            subStruct, ...
            'MultipingSequence',    iMultiPing);
        
    case 9001   % Generic Channel tuple for the generic echosounder.
        if DEBUG
            fprintf('Ecriture HAC 9001   --> Generic Echo Sounder Channel tuple\n');
        end
        [flag, isWrited] = Em2hac_9001(file, iTuple, NbFaisceaux, SounderName, Data, ...
            'SubStruct',            subStruct, ...
            'MultipingSequence',    iMultiPing);
        
    case 10011  % Ping tuple C-32-16-angles (10011)
        if DEBUG
            fprintf('Ecriture HAC 10011 --> Ping tuple C-32-16\n');
        end
        [flag, NLus] = Em2hac_10011(file, nomDir, iTuple, NbFaisceaux, Data, NLus, SeuildB, SeuilDeg, ...
            'WCPingIndex',          idxWCPing, ...
            'SubStruct',            subStruct, ...
            'MultipingSequence',    iMultiPing);
        
        
    case 10030  % Ping tuple U16-Samples (10030) non-compress�s
        if DEBUG
            fprintf('Ecriture HAC 10030 --> Ping tuple U16\n');
        end
        [flag, NLus] = Em2hac_10030(file, nomDir, iTuple, NbFaisceaux, Data);
        
    case 10031  % Ping tuple U16-angles (10031) non-compress�s
        if DEBUG
            fprintf('Ecriture HAC 10031 --> Ping tuple U16\n');
        end
        [flag, NLus] = Em2hac_10031(file, nomDir, iTuple, NbFaisceaux, Data);
        
    case 10040  % Ping tuple C-16
        if DEBUG
            fprintf('Ecriture HAC 10040 --> Ping tuple C-16\n');
        end
        
        [flag, NLus] = Em2hac_10040(file, nomDir, iTuple, NbFaisceaux, Data, NLus, SeuildB, ...
            'WCPingIndex',          idxWCPing, ...
            'MultipingSequence',    iMultiPing);
        
    case 10100  % Param�tres de seuillage (10100)
        if DEBUG
            fprintf('Ecriture HAC 10100 --> Param�tres du seuillage pour chaque faisceau (non d�finis pour SM20)\n');
        end
        flag = Em2hac_10100(file, iTuple, NbFaisceaux, Data, ...
            'SubStruct',            subStruct, ...
            'MultipingSequence', iMultiPing);
        
    case 10140  % Attitude sensor tuple
        if DEBUG
            fprintf('Ecriture HAC 10140 --> Attitude sensor tuple pour chaque Ping\n');
        end
        flag = Em2hac_10140(file, iTuple, Data);
        
end

if ~flag
    return
end

