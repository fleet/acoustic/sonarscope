function Signal = val2code(Signal, InfoSignal, Type)

if isfield(InfoSignal, 'AddOffset') && ~isempty(InfoSignal.AddOffset) && (InfoSignal.AddOffset ~= 0)
    Signal = Signal - InfoSignal.AddOffset;
end
if isfield(InfoSignal, 'ScaleFactor') && ~isempty(InfoSignal.ScaleFactor) && (InfoSignal.ScaleFactor ~= 1)
    Signal = Signal / InfoSignal.ScaleFactor;
end
Signal = cast(Signal, Type);