function flag = CacheAttitude2Netcdf(nomFicNc, nomFic, varargin)

[varargin, deflateLevel] = getPropertyValue(varargin, 'deflateLevel', 1); %#ok<ASGLU>

flag = 1;

%% Nom du fichier Netcdf

[nomDir, nom] = fileparts(nomFic);
nomFic = fullfile(nomDir, 'SonarScope', [nom '.nc']);
if ~exist(nomFic, 'file')
    return
end

%% Ecriture des variables Tide, Draught et TrueHeave dans le fichier Netcdf car ils ne sont pas cr��s par d�faut dans le XMLBin

ncID = netcdf.open(nomFicNc, 'WRITE');
childGrpID = netcdf.inqGrps(ncID);

finfo = ncinfo(nomFicNc);
for kGroup=1:length(finfo.Groups)
    if strcmp(finfo.Groups(kGroup).Name, 'Attitude')
        grpID = childGrpID(kGroup);
        dimID = netcdf.inqDimID(grpID, 'NbSamples');
        NC_Storage = NetcdfUtils.getNetcdfClassname('single');
        
        varNames = {finfo.Groups(kGroup).Variables(:).Name};
        if all(~strcmp(varNames, 'Tide'))
            varID = netcdf.defVar(grpID, 'Tide', NC_Storage, dimID);
            netcdf.defVarDeflate(grpID, varID, false, true, deflateLevel);
            netcdf.defVarFill(grpID, varID, false, NaN);
            netcdf.putAtt(grpID, varID, 'Unit',      'm');
            netcdf.putAtt(grpID, varID, 'Populated', 0);
        end
        
        if all(~strcmp(varNames, 'Draught'))
            varID = netcdf.defVar(grpID, 'Draught', NC_Storage, dimID);
            netcdf.defVarDeflate(grpID, varID, false, true, deflateLevel);
            netcdf.defVarFill(grpID, varID, false, NaN);
            netcdf.putAtt(grpID, varID, 'Unit',      'm');
            netcdf.putAtt(grpID, varID, 'Populated', 0);
        end
        
        if all(~strcmp(varNames, 'TrueHeave'))
            varID = netcdf.defVar(grpID, 'TrueHeave', NC_Storage, dimID);
            netcdf.defVarDeflate(grpID, varID, false, true, deflateLevel);
            netcdf.defVarFill(grpID, varID, false, NaN);
            netcdf.putAtt(grpID, varID, 'Unit',      'm');
            netcdf.putAtt(grpID, varID, 'Populated', 0);
        end
    end
end

netcdf.close(ncID);
