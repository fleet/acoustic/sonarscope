function flag = CacheDepth2Netcdf(nomFicNc, varargin)

[varargin, deflateLevel] = getPropertyValue(varargin, 'deflateLevel', 1); %#ok<ASGLU>

flag = 1;

%% Ecriture de la variable Entries dans le fichier Netcdf

ncID  = netcdf.open(nomFicNc, 'WRITE');
grpID = netcdf.inqNcid(ncID, 'Depth');
 
varIDMask = netcdf.inqVarID(grpID, 'Mask');
[~, ~, dimids] = netcdf.inqVar(grpID, varIDMask);

NC_Storage = NetcdfUtils.getNetcdfClassname('uint8');
varID2 = netcdf.defVar(grpID, 'MaskBackup', NC_Storage, dimids);
netcdf.defVarDeflate(grpID, varID2, false, true, deflateLevel);
netcdf.defVarFill(grpID, varID2, true, NaN);
netcdf.putAtt(grpID, varID2, 'Unit', '');
netcdf.putAtt(grpID, varID2, 'Populated', 0);

netcdf.close(ncID);
