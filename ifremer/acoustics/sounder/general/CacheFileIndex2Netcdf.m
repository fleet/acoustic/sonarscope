function flag = CacheFileIndex2Netcdf(nomFicNc, nomFic, grpName, varargin)

[varargin, deflateLevel] = getPropertyValue(varargin, 'deflateLevel', 1); %#ok<ASGLU>

%% Lecture du fichier d'index

switch grpName
    case 'FileIndexWCD'
        [flag, Data] = read_FileIndexWCD_XMLBin(nomFic);
    otherwise
        [flag, Data] = read_FileIndex_XMLBin(nomFic);
end
if ~flag
    return
end

%% Ecriture des variables dans le fichier Netcdf

ncID  = netcdf.open(nomFicNc, 'WRITE');
grpID = netcdf.defGrp(ncID, grpName);

dimVal = length(Data.PingCounter);
dimID  = netcdf.defDim(grpID, 'nbDatagrams', dimVal);

fieldNames = fieldnames(Data);
for k=1:length(fieldNames)
    varName = fieldNames{k};
    X = Data.(varName);
    Unit = '';
    switch class(X)
        case 'cl_time'
            X = X.timeMat;
            Unit = 'days since JC';
        case 'datetime'
%             X = datenum(X);
            continue
    end
    
    NC_Storage = NetcdfUtils.getNetcdfClassname(class(X));
    varID = netcdf.defVar(grpID, varName, NC_Storage, dimID);
    netcdf.defVarDeflate(grpID, varID, false, true, deflateLevel);
    
    NetcdfUtils.putVarVal(grpID, varID, X, 1, 'Unit', Unit)
end
netcdf.close(ncID);

flag = 1;
