function flag = CacheMagPhase2Netcdf(nomFicNc, nomFic, varargin)

[varargin, deflateLevel] = getPropertyValue(varargin, 'deflateLevel', 1); %#ok<ASGLU>

flag = 1;

%% Lecture des donn�es SeabedImage dans le datagramme ALL_SeabedImagexxh

%     'D:\Temp\ExData\ALL-WithMagAndPhase\SonarScope\0056_20150615_234546_ATL_EM710\ALL_RawBeamSamples\SampleAmplitude.bin'
%     'D:\Temp\ExData\ALL-WithMagAndPhase\SonarScope\0056_20150615_234546_ATL_EM710\ALL_RawBeamSamples\SamplePhase.bin'

[nomDir, nom] = fileparts(nomFic);

nomFic = fullfile(nomDir, 'SonarScope', nom, 'ALL_RawBeamSamples', 'SampleAmplitude.bin');
if ~exist(nomFic, 'file')
    return
end
fid = fopen(nomFic, 'r');
if fid == -1
    flag = 0;
    return
end
Amplitude = fread(fid, Inf, 'uint16=>single'); % pr�voir peut-�tre la lecture et �criture dans une boucle comme cela est fait dans CacheWaterColumn2Netcdf
fclose(fid);

nomFic = fullfile(nomDir, 'SonarScope', nom, 'ALL_RawBeamSamples', 'SamplePhase.bin');
if ~exist(nomFic, 'file')
    return
end
fid = fopen(nomFic, 'r');
if fid == -1
    flag = 0;
    return
end
Phase = fread(fid, Inf, 'uint16=>single');
fclose(fid);

%% Ecriture des variables Amplitude et Ohase dans le fichier Netcdf

ncID = netcdf.open(nomFicNc, 'WRITE');
childGrpID = netcdf.inqGrps(ncID);

finfo = ncinfo(nomFicNc);
for kGroup=1:length(finfo.Groups)
    if strcmp(finfo.Groups(kGroup).Name, 'RawBeamSamples')
        grpID = childGrpID(kGroup);
        dimID = netcdf.defDim(grpID, 'nbSamplesAmpPhase', length(Amplitude));
        
        % Ecriture de l'Amplitude
        NC_Storage = NetcdfUtils.getNetcdfClassname(class(Amplitude));
        varID = netcdf.defVar(grpID, 'Amplitude', NC_Storage, dimID);
        netcdf.defVarDeflate(grpID, varID, false, true, deflateLevel);
        NetcdfUtils.putVarVal(grpID, varID, Amplitude, 1, 'Unit', 'dB')

        % Ecriture de la phase
        NC_Storage = NetcdfUtils.getNetcdfClassname(class(Phase));
        varID = netcdf.defVar(grpID, 'Phase', NC_Storage, dimID);
        NetcdfUtils.putVarVal(grpID, varID, Phase, 1, 'Unit', 'deg')
    end
end

netcdf.close(ncID);
