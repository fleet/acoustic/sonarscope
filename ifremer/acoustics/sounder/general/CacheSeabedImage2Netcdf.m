function flag = CacheSeabedImage2Netcdf(ncFileName, nomFic, package, varargin)

[varargin, deflateLevel] = getPropertyValue(varargin, 'deflateLevel', 1); %#ok<ASGLU>

flag = 1;

%% Lecture des donn�es SeabedImage dans le datagramme ALL_SeabedImagexxh

[nomDir, nom] = fileparts(nomFic);
nomFic = fullfile(nomDir, 'SonarScope', nom, ['ALL_' package], 'Entries.bin');
if ~exist(nomFic, 'file')
    return
end

fid = fopen(nomFic, 'r');
if fid == -1
    flag = 0;
    return
end

switch package
    case 'SeabedImage53h'
        Entries = fread(fid, Inf, 'int8=>single') * 0.5;
    case 'SeabedImage59h'
        Entries = fread(fid, Inf, 'int16=>single') * 0.1;
end

fclose(fid);

%% Ecriture de la variable Entries dans le fichier Netcdf

for kBug=1:100
    if kBug > 1
        fprintf('>-------------- CacheSeabedImage2Netcdf kBug=%d --------------<\n', kBug);
    end
    try
        ncID = netcdf.open(ncFileName, 'WRITE');
        childGrpID = netcdf.inqGrps(ncID);
        
        finfo = ncinfo(ncFileName);
        for kGroup=1:length(finfo.Groups)
            if strcmp(finfo.Groups(kGroup).Name, package)
                grpID = childGrpID(kGroup);
                dimID = netcdf.defDim(grpID, 'nbSamplesEntries', length(Entries));
                NC_Storage = NetcdfUtils.getNetcdfClassname(class(Entries));
                varID = netcdf.defVar(grpID, 'Entries', NC_Storage, dimID);
                netcdf.defVarDeflate(grpID, varID, false, true, deflateLevel);
                NetcdfUtils.putVarVal(grpID, varID, Entries, 1, 'Unit', 'dB')
            end
        end
        
        netcdf.close(ncID);
        flag = 1;
        break
    catch
        my_breakpoint
        try
           netcdf.close(ncID); 
        catch
        end
        pause(0.2)
        flag = 0;
    end
end
