function flag = CacheVersionDatagrammes2Netcdf(ncFileName, nomFic)

flag = 0;

[nomDir, nom] = fileparts(nomFic);
nomFicXML = fullfile(nomDir, 'SonarScope', nom, 'ALL_RawRangeBeamAngle4eh.xml');
if exist(nomFicXML, 'file')
    Version     = 'V2';
    SousVersion = '';
else
    nomFicXML = fullfile(nomDir, 'SonarScope', nom, 'ALL_RawRangeBeamAngle46h.xml');
    if exist(nomFicXML, 'file')
        Version     = 'V1';
        SousVersion = 'F';
    else
        nomFicXML = fullfile(nomDir, 'SonarScope', nom, 'ALL_RawRangeBeamAngle66h.xml');
        if exist(nomFicXML, 'file')
            Version     = 'V1';
            SousVersion = 'f';
        else
            return
        end
    end
end
[flag, XML] = XMLBinUtils.readGrpData(nomFicXML, 'XMLOnly', 1);

%% Ecriture de la variable Entries dans le fichier Netcdf

for kBug=1:100
    if kBug > 1
        fprintf('>-------------- CacheVersionDatagrammes2Netcdf kBug=%d --------------<\n', kBug);
    end
    try
        ncID = netcdf.open(ncFileName, 'WRITE');
        
        attID = -1; % netcdf.getConstant('GLOBAL');
        netcdf.putAtt(ncID, attID, 'Constructor',         'Kongsberg');
        netcdf.putAtt(ncID, attID, 'EmModel',             XML.Model);
        netcdf.putAtt(ncID, attID, 'SystemSerialNumber',  XML.SystemSerialNumber);
        netcdf.putAtt(ncID, attID, 'DatagramVersion',     Version);
        netcdf.putAtt(ncID, attID, 'DatagramSousVersion', SousVersion);
        netcdf.putAtt(ncID, attID, 'Software',            'SonarScope');
        netcdf.putAtt(ncID, attID, 'Support',             'sonarscope@ifremer.fr');
        
        netcdf.close(ncID);
        flag = 1;
        break
    catch
        my_breakpoint
        try
            netcdf.close(ncID);
        catch
        end
        pause(0.2); % Test correction de bug fichier juste cr�� mais pas ouvrable
        flag = 0;
    end
end
