function flag = CacheWaterColumn2Netcdf(nomFicNc, nomFic, varargin)

[varargin, deflateLevel] = getPropertyValue(varargin, 'deflateLevel', 1); %#ok<ASGLU>

%% Lecture des donn�es Water Column et �criture dans le fichier Netcdf

[nomDir, nom] = fileparts(nomFic);

flag = 1;
nomFic = fullfile(nomDir, 'SonarScope', nom, 'ALL_WaterColumn', 'SampleAmplitude.bin');
if ~exist(nomFic, 'file')
    nomFic = fullfile(nomDir, 'SonarScope', nom, 'WCD_WaterColumn', 'SampleAmplitude.bin');
    if ~exist(nomFic, 'file')
        return
    end
end
nbBytes = sizeFic(nomFic);

fid = fopen(nomFic, 'r');
if fid == -1
    flag = 0;
    return
end

for kBug=1:100
    if kBug > 1
        fprintf('>-------------- CacheWaterColumn2Netcdf kBug=%d --------------<\n', kBug);
    end
    try
        ncID = netcdf.open(nomFicNc, 'WRITE');
        childGrpID = netcdf.inqGrps(ncID);
        
        finfo = ncinfo(nomFicNc);
        for kGroup=1:length(finfo.Groups)
            if strcmp(finfo.Groups(kGroup).Name, 'WaterColumn')
                grpID = childGrpID(kGroup);
                dimEntriesID = netcdf.defDim(grpID, 'nbSamplesWC', nbBytes);
                NC_Storage = NetcdfUtils.getNetcdfClassname('int8');
                varID = netcdf.defVar(grpID, 'Amplitude', NC_Storage, dimEntriesID);
                ChunkSize = 100000;
                netcdf.defVarChunking(grpID, varID, 'CHUNKED', ChunkSize);
                netcdf.defVarDeflate(grpID, varID, false, true, deflateLevel);
                
                % Ecriture de l'Amplitude
                bins = -128:127;
                histo = zeros(size(bins));
                for k=1:ChunkSize:nbBytes
                    sub = k:min((k+ChunkSize-1), nbBytes);
                    Amplitude = fread(fid, length(sub), 'int8=>int8');
                    netcdf.putVar(grpID, varID, k-1, length(sub), Amplitude);
                    histo = histo + my_hist(Amplitude, bins);
                end
                %         figure; bar(bins, histo); grid on;
                Unit = 'dB';
                netcdf.putAtt(grpID, varID, 'Unit', Unit);
                netcdf.putAtt(grpID, varID, 'Populated', 1);
                %         NetcdfUtils.addValStats(grpID, varID, Amplitude, Unit)
                NetcdfUtils.addValStatsfromHisto(grpID, varID, bins, histo, Unit)
            end
        end
        netcdf.close(ncID);
        fclose(fid);
        flag = 1;
        break
    catch
        my_breakpoint
        try
            netcdf.close(ncID);
        catch
        end
        pause(0.2)
        flag = 0;
    end
end
