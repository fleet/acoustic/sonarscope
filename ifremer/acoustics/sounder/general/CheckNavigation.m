function [flag, Latitude, Longitude, Heading] =  CheckNavigation(Time, Latitude, Longitude, Title, varargin)

persistent FiltreNav_persistent

[varargin, Heading] = getPropertyValue(varargin, 'Heading', []); %#ok<ASGLU>

Format1 = 'V�rification de la %s.\n\nUne fen�tre SignalDialog va s''ouvrir, le signal de %s est en bleu et une version filtr�e est gris�e. Zoomez sur le signal pour voir si la version filtr�e est meilleure (elle rem�die souvent � des marches d''escalier dues � une quantification de la mesure de positionnemen).\n\nSi vous conservez le filtre et que vous clickez sur Save et Quit c''est lui qui sera sauvegard�.';
Format2 = 'Checking %s.\n\nA SignalDialog window is going to be open, %s signal is the blue one, the grey one is a filtered version. Zoom on the signal to check if the filter is efficient (it often solves problems of stair steps due to signal quantification).\n\nIf you conserve the filter AND you click on Save and Quit il will be the red curve which will be saved.';
str1 = sprintf(Format1, 'Latitude', 'Latitude');
str2 = sprintf(Format2, 'Latitude', 'Latitude');
my_warndlg(Lang(str1,str2), 1);

if isempty(FiltreNav_persistent)
    Filtre = [];
else
    Filtre = FiltreNav_persistent;
end
if isempty(Filtre)
    filterDataParametre = [];
%     filtreOK = 0;
else
    filterDataParametre = FilterDataParametre('FilterType', FilterDataParametre.FilterNameList(1), ...
        'ButterworthWc', num2str(Filtre.Wc), 'ButterworthOrder', Filtre.Ordre);
%     filtreOK = 1;
end

timeSample = XSample('name', 'time', 'data', Time);

latSample = YSample('data', Latitude, 'unit', 'deg', 'marker', '.');
latSignal = ClSignal('ySample', latSample, 'xSample', timeSample);

lonSample = YSample('data', Longitude, 'unit', 'deg', 'marker', '.');
lonSignal = ClSignal('ySample', lonSample, 'xSample', timeSample);

if isempty(Heading)
    headingSignal = [];
else
    headingSample = YSample('data', Heading, 'unit', 'deg', 'marker', '.');
    headingSignal = ClSignal('ySample', headingSample, 'xSample', timeSample);
end

nav = ClNavigation(latSignal, lonSignal, 'name', 'RDF navigation port side', 'headingVesselSignal', headingSignal);

%% Edition de la navigation

% a = NavigationDialog(nav, 'Title', nomFicSeul, 'CleanInterpolation', true, 'filterParametreValue', Filtre);
a = NavigationDialog(nav, 'Title', Title, 'CleanInterpolation', true, 'filterDataParametre', filterDataParametre);
a.openDialog();
if ~a.okPressedOut
    flag = 0;
    return
end

%% R�cup�ration du filtre

% TODO MHO : bug ici !!!
% FiltreNav_persistent = a.filterParametreValue;
% if filtreOK
if a.filterDataParametre.selectedFilterIndex ~= 1
    % Recup�ration du parametres de filtrage
    filterDataParametre = a.filterDataParametre;
    % Conversion FilterDataParametre en 'Filtre'
    type = 1;
    order = filterDataParametre.butterworthOrderParam.Value;
    wc    = filterDataParametre.butterworthWcParam.Value;
    Filtre = struct('Type', type, 'Ordre', order, 'Wc', wc);
    FiltreNav_persistent = Filtre;
end

%% R�cup�ration de la navigation

Latitude  = nav.getLatitudeSignalSample().data;
Longitude = nav.getLongitudeSignalSample().data;

if ~isempty(Heading)
    Heading = nav.getHeadingVesselSignalSample().data;
end

flag = 1;
