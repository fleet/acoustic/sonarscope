function [xEIImage, EIReflecImage, EINbPointsImage, EIAnglesImage, EIAlongDistImage, EITxBeamIndexSwathImage] = ...
    EI_Compute_Images(EI, varargin)

[varargin, TxBeamIndexSwath] = getPropertyValue(varargin, 'TxBeamIndexSwath', []); %#ok<ASGLU>

nbPingsWC = length(EI.WCReflecPings);
XMin = Inf;
XMax = -Inf;
for k=1:nbPingsWC
    if ~isempty(EI.WCXPings{k})
        XMin = min(XMin, min(EI.WCXPings{k}));
        XMax = max(XMax, max(EI.WCXPings{k}));
    end
end
nbColumns = median(EI.Nb, 'omitnan');
xEIImage = linspace(double(XMin), double(XMax), nbColumns);
xEIImage = centrage_magnetique(xEIImage);

nbColumns = length(xEIImage);

nbPings = max(EI.subDepth);
EIReflecImage    = NaN(nbPings,  nbColumns, 'single');
EINbPointsImage  = NaN(nbPings,  nbColumns, 'single');
EIAnglesImage    = NaN(nbPings,  nbColumns, 'single');
EIAlongDistImage = zeros(nbPings,nbColumns, 'single');

if isempty(TxBeamIndexSwath)
    EITxBeamIndexSwathImage = [];
else
    EITxBeamIndexSwathImage = NaN(nbPings,nbColumns, 'single');
end

for k=1:nbPingsWC
    kDepth = EI.subDepth(k);
    sunNonNaN = ~isnan(EI.WCReflecPings{k});
    x1 = EI.WCXPings{k}(sunNonNaN);
    if length(x1) > 1
        EIReflecImage(kDepth,:)   = interp1(x1, EI.WCReflecPings{k}(sunNonNaN),   xEIImage);
        EINbPointsImage(kDepth,:) = interp1(x1, EI.WCNbPointsPings{k}(sunNonNaN), xEIImage);
        [~,I1] = unique(EI.WCAcrossDistPings{k});
        AnglesThisPing = EI.WCAnglesPings{k}(I1);
        %     figure; plot(AcrossDistThisPing, AnglesThisPing); grid;
        AcrossDistThisPing = EI.WCAcrossDistPings{k}(I1);
        
        sunNonNaN = ~isnan(AcrossDistThisPing);
        EIAnglesImage(kDepth,:) = interp1(AcrossDistThisPing(sunNonNaN), AnglesThisPing(sunNonNaN), xEIImage);
        
        if ~isempty(TxBeamIndexSwath)
            sunNonNaN2 = ~isnan(TxBeamIndexSwath(k,1:length(AnglesThisPing)));
            EITxBeamIndexSwathImage(k,:) = my_interp1(AnglesThisPing(sunNonNaN2), TxBeamIndexSwath(k,sunNonNaN2), EIAnglesImage(kDepth,:), 'nearest'); %#ok<AGROW>
        end
        
        EIAlongDistImage(kDepth,:) = my_interp1(EI.WCAcrossDistPings{k}, EI.WCAlongDistPings{k}, xEIImage);
        
        sunNonNaN = isnan(EIReflecImage(kDepth,:));
        EIAnglesImage(kDepth,sunNonNaN)    = NaN;
        EIAlongDistImage(kDepth,sunNonNaN) = NaN;
        if ~isempty(TxBeamIndexSwath)
            EITxBeamIndexSwathImage(k,sunNonNaN) = NaN; %#ok<AGROW>
        end
    end
end
