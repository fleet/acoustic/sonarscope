function [flag, xBorneInf, zBorneInf, xBorneSup, zBorneSup, Angle] = ...
    EI_Compute_LimitsOverSeabed(DepthPing, AcrossDistPing, params_ZoneEI, Immer, AntennaAcrossDist)

xBorneInf = [];
zBorneInf = [];
xBorneSup = [];
zBorneSup = [];
Angle     = [];

SeuilBas         = params_ZoneEI.SeuilBas;
SeuilHaut        = params_ZoneEI.SeuilHaut;
DistanceSecutity = params_ZoneEI.DistanceSecutity;
Where            = params_ZoneEI.Where;
DistHaut         = params_ZoneEI.DistHaut;
DistBas          = params_ZoneEI.DistBas;

switch Where
    case 'ASF-Height'
        DepthBas = (DepthPing * SeuilBas);
        
        a2 = AcrossDistPing .^ 2;
        d = sqrt(DepthPing .^2 + a2);
        
        z2 = (d * SeuilBas) .^2 - a2;
        sub = (z2 >= 0);
        DepthBas(sub)  = -sqrt(z2(sub));
        
        DepthHaut = DepthBas + DepthPing * (SeuilHaut - SeuilBas);
        
    case 'ASF-Distance'
        DepthBas  = DepthPing + DistBas;
        DepthHaut = DepthPing + DistHaut;
end

% FigUtils.createSScFigure; PlotUtils.createSScPlot(DepthPing); grid on; hold on; PlotUtils.createSScPlot(DepthBas, 'r'); PlotUtils.createSScPlot(DepthHaut, 'g');

sub0 = isnan(DepthPing);
sub1 = find(~sub0);
if length(sub1) < 2
    flag = 0;
    return
end
sub2 = find(sub0);
AcrossDistPing(sub2) = interp1(sub1, AcrossDistPing(sub1)-AntennaAcrossDist, sub2);
% AlongDistPing(sub2)  = interp1(sub1, AlongDistPing(sub1), sub2);
DepthPing(sub2) = interp1(sub1, DepthPing(sub1), sub2);

% TODO : voir si il faut ressortir les vecteurs interpolés

DepthBas(sub2)  = interp1(sub1, DepthBas(sub1),  sub2);
DepthHaut(sub2) = interp1(sub1, DepthHaut(sub1), sub2);

% Angle = atand(AcrossDistPing ./ abs(DepthPing-Immer)); % TODO : bizarre !!!
Angle = atand(AcrossDistPing ./ abs(DepthPing)); % TODO : bizarre !!!
%  figure; plot(Angle); grid

zBorneInf = DepthBas + DistanceSecutity;
xBorneInf = AcrossDistPing;
zBorneSup = DepthHaut + DistanceSecutity;
xBorneSup = AcrossDistPing;

xBorneInf = xBorneInf + AntennaAcrossDist;
xBorneSup = xBorneSup + AntennaAcrossDist;

zBorneInf = zBorneInf + Immer;
zBorneSup = zBorneSup + Immer;

sub0 = isnan(DepthPing);
zBorneInf(sub0) = [];
zBorneSup(sub0) = [];
xBorneInf(sub0) = [];
xBorneSup(sub0) = [];

flag = 1;
