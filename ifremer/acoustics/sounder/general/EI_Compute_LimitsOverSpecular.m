function [flag, xBorneInf, zBorneInf, xBorneSup, zBorneSup, Angle] = ...
    EI_Compute_LimitsOverSpecular(DepthPing, AcrossDistPing, params_ZoneEI, Immer, AntennaAcrossDist)

SeuilBas         = params_ZoneEI.SeuilBas;
SeuilHaut        = params_ZoneEI.SeuilHaut;
DistanceSecutity = params_ZoneEI.DistanceSecutity;

sub0 = isnan(DepthPing);
sub1 = find(~sub0);
if length(sub1) < 2
    flag = 0;
    xBorneInf = [];
    zBorneInf = [];
    xBorneSup = [];
    zBorneSup = [];
    Angle     = [];
    return
end
sub2 = find(sub0);
AcrossDistPing(sub2) = interp1(sub1, AcrossDistPing(sub1), sub2);
DepthPing(sub2)      = interp1(sub1, DepthPing(sub1),      sub2);
AcrossDistCercle = AcrossDistPing - AntennaAcrossDist;

% Angle = atand(AcrossDistCercle ./ abs(DepthPing-Immer));
Angle = atand(AcrossDistCercle ./ abs(DepthPing));
% figure; plot(Angle); grid

% L = sqrt(AcrossDistPing .^2 + (DepthPing - Immer) .^ 2) ;
L = sqrt(AcrossDistCercle .^2 + DepthPing .^ 2) ;
% figure; plot(L); grid

LMin = min(L);
zBorneInf  =  -(LMin - DistanceSecutity) * SeuilBas .* cosd(Angle);
xBorneInf =    (LMin - DistanceSecutity) * SeuilBas .* sind(Angle);
zBorneSup  =  -LMin * SeuilHaut .* cosd(Angle);
xBorneSup =    LMin * SeuilHaut .* sind(Angle);

xBorneInf = xBorneInf + AntennaAcrossDist;
xBorneSup = xBorneSup + AntennaAcrossDist;

zBorneInf = zBorneInf + Immer;
zBorneSup = zBorneSup + Immer;

sub0 = isnan(Angle);
zBorneInf(sub0) = [];
zBorneSup(sub0) = [];
xBorneInf(sub0) = [];
xBorneSup(sub0) = [];

flag = 1;
