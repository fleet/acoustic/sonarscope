function EI = EI_FillStruct(EI, k, Moy, N, xEIImage, Angle, AcrossDistPing, AlongDistPing)

subNaN = (N < median(N(N ~= 0))/2);
% subNaN = (N < max(N)/2) | (abs(Angle) < 45);
N(subNaN)        = NaN;
Moy(subNaN)      = NaN;
xEIImage(subNaN) = NaN;
%         Angle(subNaN) = NaN;
%         figure(1212); plot(xEIImage, Moy); grid; axis tight

EI.WCReflecPings{k}     = Moy;
EI.WCNbPointsPings{k}   = N;
EI.WCXPings{k}          = xEIImage;
EI.Nb(k)                = length(Moy);
EI.WCAnglesPings{k}     = Angle;
EI.WCAcrossDistPings{k} = AcrossDistPing;
EI.WCAlongDistPings{k}  = AlongDistPing;
