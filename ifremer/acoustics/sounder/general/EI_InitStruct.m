function EI = EI_InitStruct(subDepth)

nbPings = length(subDepth);
EI.WCReflecPings     = cell(nbPings,1);
EI.WCNbPointsPings   = cell(nbPings,1);
EI.WCXPings          = cell(nbPings,1);
EI.WCAnglesPings     = cell(nbPings,1);
EI.WCAcrossDistPings = cell(nbPings,1);
EI.WCAlongDistPings  = cell(nbPings,1);
EI.Nb                = NaN(nbPings,1);
EI.subDepth          = subDepth;
