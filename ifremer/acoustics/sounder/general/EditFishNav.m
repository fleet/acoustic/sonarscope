
function [flag, Data] = EditFishNav(Data)

%% Nouvelle formule

%% Création de l'instances ClNavigation

T = datetime(Data.SonarTime, 'ConvertFrom', 'datenum');
timeSample = XSample('name', 'time', 'data', T);

latSample = YSample('data', Data.Latitude, 'marker', '.');
latSignal = ClSignal('ySample', latSample, 'xSample', timeSample);

lonSample = YSample('data', Data.Longitude, 'marker', '.');
lonSignal = ClSignal('ySample', lonSample, 'xSample', timeSample);

fishHeadingSample = YSample('unit', 'deg', 'data', Data.Heading,     'marker', '.');
fishHeadingSignal = ClSignal('name', 'Fish heading', 'ySample', fishHeadingSample, 'xSample', timeSample);

% try
    shipHeadingSample = YSample('unit', 'deg', 'data', Data.shipHeading, 'marker', '.');
% catch
%     try
%         shipHeadingSample = YSample('unit', 'deg', 'data', Data.shipNavHeading, 'marker', '.');
%     catch
%         pi;%JMA
%     end
% end
shipHeadingSample.dataType = SampleType.ANGLE;
otherSignals        = ClSignal('name', 'Ship heading', 'ySample', shipHeadingSample, 'xSample', timeSample);

ImmersionSample     = YSample('unit', 'm', 'data', Data.Immersion,      'marker', '.');
altitudeMeters      = YSample('unit', 'm', 'data', Data.AltitudeMeters, 'marker', '.');

immersionSignal     = ClSignal('name', 'Immersion',      'ySample', ImmersionSample, 'xSample', timeSample);
otherSignals(end+1) = ClSignal('name', 'AltitudeMeters', 'ySample', altitudeMeters,  'xSample', timeSample);

CableOutSample      = YSample('unit', 'm', 'data', Data.CableOut, 'marker', '.');
otherSignals(end+1) = ClSignal('name', 'CableOut', 'ySample', CableOutSample, 'xSample', timeSample);

speedSample         = YSample('unit', 'm/s?', 'data', Data.Speed, 'marker', '.');
otherSignals(end+1) = ClSignal('name', 'speed', 'ySample', speedSample, 'xSample', timeSample);

PitchSample         = YSample('unit', 'deg', 'data', Data.Pitch, 'marker', '.');
otherSignals(end+1) = ClSignal('name', 'Pitch', 'ySample', PitchSample, 'xSample', timeSample);

RollSample          = YSample('unit', 'deg', 'data', Data.Roll, 'marker', '.');
otherSignals(end+1) = ClSignal('name', 'Roll', 'ySample', RollSample, 'xSample', timeSample);

% [fishNavHeading, speed, distance, acceleration] = calCapFromLatLon(Data.Latitude, Data.Longitude, 'Time', T);
Data.fishNavHeading = calCapFromLatLon(Data.Latitude, Data.Longitude);

FishNavHeadingSample = YSample('unit', 'deg', 'data', Data.fishNavHeading, 'marker', '.');
FishNavHeadingSample.dataType = SampleType.ANGLE;
otherSignals(end+1)  = ClSignal('name', 'FishNavHeading', 'ySample', FishNavHeadingSample, 'xSample', timeSample);

nav = ClNavigation(latSignal, lonSignal, ...
    'headingVesselSignal', fishHeadingSignal, ...
    'immersionSignal',     immersionSignal, ...
    'otherSignal',         otherSignals, ...
    'name', 'SDF navigation');

%% Edition de la navigation

a = NavigationDialog(nav, 'Title', 'Navigation', 'CleanInterpolation', true);
% display only lat lon immersion and heading
a.displayedSignalList = [1 1 1 1 1 1 1 0 0 0 1];

pause(0.5)
a.openDialog();
if ~a.okPressedOut
    flag = 0;
    return
end

%% Récupération de la navigation

% figure; plot(Data.Heading, 'k'); grid on; hold on; plot(Data.shipHeading, 'k');

Data.Latitude  = nav.getLatitudeSignalSample().data;
Data.Longitude = nav.getLongitudeSignalSample().data;
Data.Heading   = nav.getHeadingVesselSignalSample().data;

Data.Immersion = nav.getImmersionSignalSample().data;

Data.shipHeading = getOtherSignalSample(nav, 5);
Data.shipHeading = Data.shipHeading.data;

Data.AltitudeMeters = getOtherSignalSample(nav, 6);
Data.AltitudeMeters = Data.AltitudeMeters.data;

Data.CableOut = getOtherSignalSample(nav, 7);
Data.CableOut = Data.CableOut.data;

Data.fishNavHeading = getOtherSignalSample(nav, 11);
Data.fishNavHeading = Data.fishNavHeading.data;

%{
plot(Data.Heading, 'b'); grid on; hold on; plot(Data.shipHeading, 'r'); plot(Data.fishNavHeading, 'g'); 
legend('Fish heading raw', 'Ship heading raw', 'Fish heading filtered', 'Fish heading filtered', 'fishNavHeading filtered');

my_figure
my_plot(Data.Heading, 'b'); grid on; hold on; my_plot(Data.shipHeading, 'r'); my_plot(Data.fishNavHeading, 'g'); 
legend('Fish heading filtered', 'Fish heading filtered', 'fishNavHeading filtered');
%}

flag = 1;
