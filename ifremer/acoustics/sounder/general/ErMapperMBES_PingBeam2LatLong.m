% Realisation de la mosaique de donnees des "DepthDatagrams" d'une campagne
%
% Syntax
%   [flag, Mosa, nomDir] = ErMapperMBES_PingBeam2LatLong(...)
%
% Name-Value Pair Arguments
%   nomDir : Nom du r�pertoire des donnees
%   resol  : Pas de la grille en m�tres
%   Backup : P�riode de sauvegarde des fichiers de sortie
%   Layers : Liste des numeros de layers a mosaiquer (voir cl_simrad_all/view_depth)
%            (Un seul layer pour le moment)
%   Rayon  : Rayon utilis� lors de l'interpolation
%
% Output Arguments
%   []     : Auto-plot activation
%   flag   : Description du parametre (unite).
%   a      : Instances de cl_image contenant la mosaique et la mosaique angulaire
%   nomDir : Nom du r�pertoire des images cr��es
%
% Examples
%   ErMapperMBES_PingBeam2LatLong
%   flag, a] = ErMapperMBES_PingBeam2LatLong('NomDir', '/home1/jackson/FEMME2005/Donnees', 'Resol', 10);
%   SonarScope(a)
%   [flag, a] = ErMapperMBES_PingBeam2LatLong('NomDir', 'D:\NIWA_DATA\TAN0105', 'Resol', 10, 'Backup', 5);
%   SonarScope(a)
%
% See also Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function [flag, Mosa, nomDir] = ErMapperMBES_PingBeam2LatLong(varargin)

E0 = cl_ermapper([]);

disp('Supprimer l''interpolation de la mosaique de reception')

Mosa = [];

% [varargin, resol] = getPropertyValue(varargin, 'Resol', []);
% [varargin, Layers] = getPropertyValue(varargin, 'Layers', []);
% [varargin, Backup] = getPropertyValue(varargin, 'Backup', []);
% [varargin, Rayon] = getPropertyValue(varargin, 'Rayon', []);
[varargin, nomDir] = getPropertyValue(varargin, 'NomDir', []);
[varargin, window] = getPropertyValue(varargin, 'window', []); %#ok<ASGLU>

% Carto = [];

if isempty(nomDir)
    nomDir = pwd;
end

%% Recherche des fichiers .all

[flag, liste] = uiSelectFiles('ExtensionFiles', '.ers', 'InitGeometryType', cl_image.indGeometryType('PingBeam'), 'RepDefaut', pwd);
if ~flag || isempty(liste)
    return
end

nbProfils = length(liste);
nomDir = fileparts(liste{1});

%% Recup�ration de quelques valeurs dans la premi�re image

[flag, a] = cl_image.import_ermapper(liste{1});
if ~flag
    return
end
% Carto = get(a, 'Carto');
resol = get(a, 'SonarResolutionX');
DataType = a.DataType;
GeometryType = a.GeometryType;
flag = testSignature(a, 'GeometryType', cl_image.indGeometryType('PingBeam'));
if ~flag
    return
end
clear a

%% Saisie de la r�solution

[flag, resol] = get_gridSize('value', resol);
if ~flag
    return
end

%% Backup frequency

[flag, Backup] = saisie_backupFrequency(nbProfils);
if ~flag
    return
end

%% Across distance interpolation method

[flag, AcrossInterpolation] = question_AcrossDistanceInterpolation;
if ~flag
    return
end

%% Mosaic interpolation window

[flag, window] = question_MosaicInterpolationWindow(window);
if ~flag
    return
end

%% Noms des fichiers de sauvegarde des mosaiques

[nomFicErMapperLayer, flag] = initNomFicErs(E0, 'Mosaic', cl_image.indGeometryType('LatLong'), DataType, nomDir, 'resol', resol);
if ~flag
    return
end
[nomDirMos, TitreDepth] = fileparts(nomFicErMapperLayer);
DataTypeAngle = ALL.identSonarDataType('BeamDepressionAngle');
[nomFicErMapperAngle, flag] = initNomFicErs(E0, TitreDepth, cl_image.indGeometryType('LatLong'), DataTypeAngle, nomDirMos, 'confirm', (get_LevelQuestion >= 3));
if ~flag
    return
end
[~, TitreAngle] = fileparts(nomFicErMapperAngle);

%% Recuperation des mosaiques existantes

if exist(nomFicErMapperLayer, 'file') && exist(nomFicErMapperAngle, 'file')
    [flag, rep] = question_CompleteExistingMosaic;
    if ~flag
        return
    end
    if rep == 1
        [flag, Z_Mosa_All] = cl_image.import_ermapper(nomFicErMapperLayer);
        if ~flag
            return
        end
        %             Carto = get(Z_Mosa_All, 'Carto');
        
        [flag, A_Mosa_All]  = cl_image.import_ermapper(nomFicErMapperAngle);
        if ~flag
            return
        end
    else
        Z_Mosa_All = [];
        A_Mosa_All = [];
    end
else
    Z_Mosa_All = [];
    A_Mosa_All = [];
end

%% Si Mosaique de Depth, on propose de faire la correction de maree

if DataType == cl_image.indDataType('Bathymetry')
    [flag, flagTideCorrection] = question_TideCorrection;
    if ~flag
        return
    end
    if flagTideCorrection
        [flag, TideFile] = my_uigetfile('*.*', 'Give a tide file (cancel instead)', nomDir);
        if ~flag
            TideFile = [];
        end
    else
        TideFile = [];
    end
else
    TideFile = [];
end
if ~isempty(TideFile)
    [flag, Tide] = read_Tide(TideFile);
    if ~flag
        return
    end
end

%% Filtre median

[repSpikeRemoval, flag] = my_questdlg(Lang('Voulez-vous faire une correction de spikes ?', 'Do you want to do a spike removal ?'));
if ~flag
    return
end
if repSpikeRemoval == 1
    [flag, windowSpikeRemoval] = saisie_window([3 3], 'Titre', 'Window size for median filter');
    if ~flag
        return
    end
end

%% Si Mosaique de Reflectivite, on propose de faire une compensation

% TODO : remplacer ce qui suit par [flag, CorFile, ModeDependant, UseModel, PreserveMeanValueIfModel] = QuestionCompensationCurve(nomDirImport, varargin)
[rep, flag] = my_questdlg(Lang('Voulez-vous faire une compensation ?', 'Do you want to do a compensation ?'));
if ~flag
    return
end
if rep == 1
    [flag, CorFile] = my_uigetfile('*.mat', 'Give a compensation file (cancel instead)', nomDir);
    if flag
        %         CourbeConditionnelle = loadmat(CorFile, 'nomVar', 'CourbeConditionnelle');
        CourbeConditionnelle = load_courbesStats(CorFile);
        
        bilan = CourbeConditionnelle.bilan;
    else
        CorFile = [];
    end
else
    CorFile = [];
end

% profile on
% tic
for k=1:nbProfils
    fprintf('Processing file %d/%d : %s\n', k, nbProfils, liste{k});
    
    [flag, Z] = cl_image.import_ermapper(liste{k});
    if ~flag
        return
    end
    
    if Z.GeometryType ~= GeometryType
        continue
    end
    if Z.DataType ~= DataType
        continue
    end
    
    
    nbRows = Z.nbRows;
    if nbRows <= 1
        Message = sprintf('The file %s is discarded because it has %d row.', liste{k}, nbRows);
        my_warndlg(Message, 0);
        continue
    end
    
    ImageName = extract_ImageName(Z);
    
    [nomLayer, flag] = initNomFicErs(E0, ImageName, GeometryType, ...
        cl_image.indDataType('AcrossDist'), nomDir, 'confirm', 0);
    if ~flag
        return
    end
    [flag, AcrossDistance] = cl_image.import_ermapper(nomLayer);
    if ~flag
        return
    end
    
    [nomLayer, flag] = initNomFicErs(E0, ImageName, GeometryType, ...
        cl_image.indDataType('AlongDist'), nomDir, 'confirm', (get_LevelQuestion >= 3));
    if ~flag
        return
    end
    [flag, AlongDistance] = cl_image.import_ermapper(nomLayer);
    if ~flag
        return
    end
    
    nomLayer = initNomFicErs(E0, ImageName, GeometryType, ...
        cl_image.indDataType('TxAngle'), nomDir, 'confirm', 0);
    if ~exist(nomLayer, 'file')
        % Les donn�es proviennent sans doute de Caraibes, l'angle est perdu
        A = sonar_Caraibes_estimationTxAngle(Z, AcrossDistance);
    else
        [flag, A] = cl_image.import_ermapper(nomLayer);
        if ~flag
            return
        end
    end
    
    Heading        = get(Z, 'Heading');
    FishLatitude   = get(Z, 'FishLatitude');
    FishLongitude  = get(Z, 'FishLongitude');
    
    if ~isempty(TideFile)
        tZ = get(Z, 'SonarTime');
        TideValue = my_interp1(Tide.Time, Tide.Value, tZ);
        if all(isnan(TideValue))
            my_warndlg('No time between image and tide', 0);
        end
        
        Z = set(Z, 'SonarTide', TideValue);
        Z = plus(Z, TideValue, 'NoStats');% A la place de Z = Z + TideValue; pour �viter le calcul des stats
    end
    
    if ~isempty(CorFile)
        %         c(5) =  sonar_secteur_emission(A, []);
        [LayersConditionnels, flag] = getListeLayersSupplementaires_private(ImageName, GeometryType, nomDir, bilan, ...
            Z, AcrossDistance, AlongDistance, A);
        if ~flag
            return
        end
        
        Z = compensationCourbesStats(Z, LayersConditionnels, bilan);
    end
    
    
    if repSpikeRemoval == 1
        [flag, Z] = sonar_filtreBathySpeculaire(Z, 'window', windowSpikeRemoval);
        if ~flag
            return
        end
    end
    
    [flag, LatLon] = sonar_calcul_coordGeo_BathyFais(Z, AcrossDistance, AlongDistance, ...
        Heading, FishLatitude, FishLongitude);
    if ~flag
        return
    end
    clear AcrossDistance AlongDistance
    
    [flag, Z_Mosa, A_Mosa] = sonar_mosaique2(Z, A, LatLon(1), LatLon(2), resol, ...
        'AcrossInterpolation', AcrossInterpolation, 'AlongInterpolation',  1);
    if ~flag
        return
    end
    clear A LatLon
    
    % -------------
    % Interpolation
    
    if ~isequal(window, [1 1])
        Z_Mosa = WinFillNaN(Z_Mosa, 'window', window, 'NoStats');
        A_Mosa = WinFillNaN(A_Mosa, 'window', window, 'NoStats');
    end
    
    [Z_Mosa_All, A_Mosa_All] = mosaique([Z_Mosa_All Z_Mosa], 'Angle', [A_Mosa_All A_Mosa], 'FistImageIsCollector', 1);
    Z_Mosa_All.Name = TitreDepth;
    A_Mosa_All.Name = TitreAngle;
    
    %% Sauvegarde des mosaiques dans des fichiers ErMapper
    
    if (mod(k,Backup) == 0) || (k == nbProfils)
        Z_Mosa_All = majCoordonnees(Z_Mosa_All);
        A_Mosa_All = majCoordonnees(A_Mosa_All);
        export_ermapper(Z_Mosa_All, nomFicErMapperLayer);
        export_ermapper(A_Mosa_All, nomFicErMapperAngle);
    end
    
    %% Suppression des fichier memmapfile
    
    deleteFileOnListe(cl_memmapfile.empty, gcbf)
end
flag = 1;

clear Mosa
Mosa(1) = Z_Mosa_All;
Mosa(2) = A_Mosa_All;

% toc
% profile report

% % InitialFileName = sprintf('Not saved for the moment. Internal code for mosaic asembling : %f', rand(1));
% for k=1:length(Mosa)
%     Mosa(k) = majCoordonnees(Mosa(k));
% %     Mosa(k) = set(Mosa(k), 'InitialFileName', InitialFileName);
% end

if nargout == 0
    SonarScope(Mosa)
end



function [LayersConditionnels, flag] = getListeLayersSupplementaires_private(ImageName, GeometryType, nomDir, bilan, Z, AcrossDistance, AlongDistance, A)

LayersConditionnels = cl_image.empty;

flag = 1;

if isempty(bilan)
    return
end

% listeLayers = ALL.getListeLayers;

DataTypeConditions = bilan{1}(1).DataTypeConditions;
for k=1:length(DataTypeConditions)
    if strcmp(DataTypeConditions{k}, 'X') || strcmp(DataTypeConditions{k}, 'Y')
        continue
    end
    
    switch DataTypeConditions{k}
        case 'Bathymetry'
            LayersConditionnels(end+1) = Z; %#ok
        case 'AcrossDist'
            LayersConditionnels(end+1) = AcrossDistance; %#ok
        case 'AlongDist'
            LayersConditionnels(end+1) = AlongDistance; %#ok
        case 'TxAngle'
            LayersConditionnels(end+1) = A; %#ok
        otherwise
            [nomLayer, flag] = initNomFicErs(E0, ImageName, GeometryType, ...
                cl_image.indDataType(DataTypeConditions{k}), nomDir, 'confirm', (get_LevelQuestion >= 3));
            if ~flag
                return
            end
            
            if exist(nomLayer, 'file')
                [flag, X] = cl_image.import_ermapper(nomLayer);
                if ~flag
                    return
                end
                LayersConditionnels(end+1) = X; %#ok
                continue
            end
            
            str = sprintf('"%s" not taken into account in ErMapperMBES_PingBeam2LatLong. Contact sonarscope@ifremer.fr Augustin', DataTypeConditions{k});
            my_warndlg(str, 1);
            flag = 0;
            return
    end
end
