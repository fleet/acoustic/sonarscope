% R�alisation de la mosa�que de donn�es ErMapper PingAcrossDist
%
% Syntax
%   [flag, Mosa, nomDir] = ErMapperSL_PingAcrossDist2LatLong(...)
%
% Name-Value Pair Arguments
%   nomDir : Nom du r�pertoire des donnees
%   resol  : Pas de la grille en m�tres
%   Backup : P�riode de sauvegarde des fichiers de sortie
%   NomFicCompens : Nom du fichier de compensation
%
% Output Arguments
%   []     : Auto-plot activation
%   flag   : Description du parametre (unite).
%   a      : Instances de cl_image contenant la mosaique et la mosaique angulaire
%   nomDir : Nom du r�pertoire des images cr��es
%
% Examples
%   ErMapperSL_PingAcrossDist2LatLong
%   flag, a] = ErMapperSL_PingAcrossDist2LatLong('NomDir', '/home1/jackson/FEMME2005/Donnees', 'Resol', 10);
%   SonarScope(a)
%   [flag, a] = ErMapperSL_PingAcrossDist2LatLong('NomDir', 'D:\NIWA_DATA\TAN0105', 'Resol', 10, 'Backup', 5);
%   SonarScope(a)
%
% See also Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function [flag, Mosa, nomDir] = ErMapperSL_PingAcrossDist2LatLong(varargin)

E0 = cl_ermapper([]);
Mosa = [];

[varargin, resol]         = getPropertyValue(varargin, 'Resol',         []);
% [varargin, Backup]        = getPropertyValue(varargin, 'Backup',        []);
[varargin, nomDir]        = getPropertyValue(varargin, 'NomDir',        []);
[varargin, NomFicCompens] = getPropertyValue(varargin, 'NomFicCompens', []); %#ok<ASGLU>

if isempty(nomDir)
    nomDir = pwd;
end

%% Recherche des fichiers .all

[flag, listeReflec, nomDir] = uiSelectFiles('ExtensionFiles', '.ers', 'RepDefaut', nomDir, ...
    'InitGeometryType', cl_image.indGeometryType('PingAcrossDist'));
% TODO : modifier tous les import_ermapper en cons�quence
% [flag, listeReflec, nomDir] = uiSelectFiles('ExtensionFiles', {'.ers'; '.xml'}, 'RepDefaut', nomDir, ...
%     'InitGeometryType', cl_image.indGeometryType('PingAcrossDist'), 'ChaineExclue', 'Carto');
if ~flag
    return
end
nomDir = fileparts(listeReflec{1});

listeAngle = cell(size(listeReflec));
listeLat   = cell(size(listeReflec));
listeLon   = cell(size(listeReflec));
nbProfils  = length(listeReflec);

%{
listeAngle{1} = 'C:\Temp\REBENT050_0_5m_PingAcrossDist_TxAngle.ers';
listeLat{1}   = 'C:\Temp\REBENT050_0_5m_PingAcrossDist_Latitude.ers';
listeLon{1}   = 'C:\Temp\REBENT050_0_5m_PingAcrossDist_Longitude.ers';
%}

%% Saisie de la r�solution

[flag, resol] = get_gridSize('Value', -resol, 'Initial', 0.5);
if ~flag
    return
end

%% Backup frequency

[flag, Backup] = saisie_backupFrequency(nbProfils);
if ~flag
    return
end

%% Noms du fichier de sauvegarde de la mosaique de r�flectivit�

DataType = cl_image.indDataType('Reflectivity');
[nomFicErMapperLayer, flag] = initNomFicErs(E0, 'Mosaic', cl_image.indGeometryType('LatLong'), DataType, nomDir, 'resol', resol);
if ~flag
    return
end
[nomDir, TitreDepth] = fileparts(nomFicErMapperLayer);

%% Noms du fichier de sauvegarde de la mosaique des angles d'�mission

DataType = cl_image.indDataType('TxAngle');
[nomFicErMapperAngle, flag] = initNomFicErs(E0, TitreDepth, cl_image.indGeometryType('LatLong'), DataType, nomDir, 'confirm', (get_LevelQuestion >= 3));
if ~flag
    return
end

%% Fichier de compensation

if isempty(NomFicCompens)
    [rep, flag] = my_questdlg(Lang('Voulez-vous faire une compensation ?', 'Enter a compensation file ?'), 'Init', 2);
    if flag && (rep == 1)
        nomFic = fullfile(nomDir, '*.mat');
        [flag, NomFicCompens] = my_uigetfile(nomFic, 'Pick a .mat file');
        if ~flag
            return
        end
        
        CourbesStatistiques = loadmat(NomFicCompens, 'nomVar', 'CourbeConditionnelle');
        bilan = CourbesStatistiques.bilan;
        %         DataTypeConditions = bilan{1}.DataTypeConditions;
    else
        bilan = [];
    end
else
    CourbesStatistiques = loadmat(NomFicCompens, 'nomVar', 'CourbeConditionnelle');
    bilan = CourbesStatistiques.bilan;
    %     DataTypeConditions = bilan{1}.DataTypeConditions;
end

%% R�cuperation des mosaiques existantes

if exist(nomFicErMapperLayer, 'file') && exist(nomFicErMapperAngle, 'file')
    [flag, rep] = question_CompleteExistingMosaic;
    if ~flag
        return
    end
    if rep == 1
        [flag, Z_Mosa_All] = cl_image.import_ermapper(nomFicErMapperLayer);
        if ~flag
            return
        end
        %             Carto = get(Z_Mosa_All, 'Carto');
        
        [flag, A_Mosa_All]  = cl_image.import_ermapper(nomFicErMapperAngle);
        if ~flag
            return
        end
    else
        Z_Mosa_All = [];
        A_Mosa_All = [];
    end
else
    Z_Mosa_All = [];
    A_Mosa_All = [];
end

%     % -------------------------------
%     % Calcul des layers conditionnels
%
%     for k2=1:length(DataTypeConditions)
%         switch DataTypeConditions{k2}
%             case 'TxAngle'
%                 b = sonar_lateral_emission(c, 'useRoll', 0);
%
%                 nomFicOut = creNomFicErs(this, nomDirOut, resolutionX);
%                 export_ermapper(b, nomFicOut);
%
%                 b = compensationCourbesStats(c, b, bilan);
%                 clear c
%             otherwise
%                 str = sprintf('Layer %s pas encore integre a SurveyErMapper_PingRange2PingAcrossDist', DataTypeConditions{k2})
%                 my_warndlg(str, 1);
%                 flag = 0;
%                 return
%         end
%     end

str1 = 'Mosaiquage des images PingAcrossDist';
str2 = 'Mosaic ErMapper PingAcrossDist images';
hw = create_waitbar(Lang(str1,str2), 'N', nbProfils);
for k1=1:nbProfils
    my_waitbar(k1, nbProfils, hw);
    fprintf('%d/%d : %s\n', k1, nbProfils, listeReflec{k1});
    
    [flag, a] = cl_image.import_ermapper(listeReflec{k1});
    if ~flag
        continue
    end
    a.Writable = false;
    if k1 == 1
        Carto = getDefinitionCarto(a, 'BypassCartoFolder', 1);
    end
    a = set_Carto(a, Carto);
    
    if isempty(listeAngle{k1})
        A = sonar_lateral_emission(a, 'useRoll', 0);
    else
        [flag, A] = cl_image.import_ermapper(listeAngle{k1});
        if ~flag
            continue
        end
    end
    A.Writable = false;
    
    if isempty(listeLat{k1}) || isempty(listeLon{k1})
        FishLatitude   = get(a, 'FishLatitude');
        FishLongitude  = get(a, 'FishLongitude');
        Heading        = get(a, 'Heading');
        
        %     [B,AButter] = butter(2, 0.02);
        %     FishLatitudeFiltree  = filtfilt(B, AButter, FishLatitude);
        %     FishLongitudeFiltree = filtfilt(B, AButter, FishLongitude);
        %
        % %     figure; plot(FishLongitude, FishLatitude); grid on
        % %     hold on; plot(FishLongitudeFiltree, FishLatitudeFiltree, 'r');
        %
        %     a = set(a, 'SonarFishLatitude',  FishLatitudeFiltree);
        %     a = set(a, 'SonarFishLongitude', FishLongitudeFiltree);
        %     a = sonar_ComputeHeading(a);
        %     HeadingFiltre = get(a, 'Heading');
        %     [B,AButter] = butter(2, 0.05);
        %     HeadingFiltre2 = filtfilt(B, AButter, HeadingFiltre);
        %
        %     FishLongitude = FishLongitudeFiltree;
        %     FishLatitude = FishLatitudeFiltree;
        %     Heading = HeadingFiltre;
        %{
figure; plot(Heading, 'k'); grid on
hold on; plot(HeadingFiltre, 'r');
hold on; plot(HeadingFiltre2, 'b');
        %}
        
        [flag, LatLon] = sonar_calcul_coordGeo_SonarX(a, Heading, ...
            FishLatitude, FishLongitude);
        if ~flag
            continue
        end
    else
        [flag, LatLon(1)] = cl_image.import_ermapper(listeLat{k1});
        if ~flag
            continue
        end
        [flag, LatLon(2)] = cl_image.import_ermapper(listeLon{k1});
        if ~flag
            continue
        end
    end
    LatLon(1).Writable = false;
    LatLon(2).Writable = false;
    
    if ~isempty(bilan)
        d = compensationCourbesStats(a, A, bilan); %%%%%%%%%%%%%% TxAngle seulement comme condition
        a = d;
        clear d
        %     imagesc(b)
        %     b.CLim = [-40 40];
        %     imagesc(b)
    end
    
    %     pas = 2000;
    %     pas = 5000;
    pas = 1000000000000000;
    nbRows = a.nbRows;
    NBlocks = ceil(nbRows/pas); % TODO ; dproposer un nombre max � l'utilisateur (valeur par d�faut Inf)
    pas = ceil(nbRows/NBlocks);
    
    %     sublBlock = {};
    %     for k2=1:pas:nbRows
    %         sublBlock{end+1} = max(1,k2-5):min(k2-1+5+pas, nbRows);
    %     end
    %     sublBlock = [sublBlock(1) sublBlock(end) sublBlock(2:end-1)];
    %     for k2=1:length(sublBlock)
    %         subl = sublBlock{k2};
    for k2=1:pas:nbRows
        subl = max(1,k2-5):min(k2-1+5+pas, nbRows);
        fprintf('Processing pings [%d:%d] Fin=%d\n', subl(1), subl(end), nbRows);
        
        [flag, Z_Mosa, A_Mosa] = sonar_mosaique2(a, A, LatLon(1), LatLon(2), resol, ...
            'AcrossInterpolation', 2, 'AlongInterpolation',  2, ...
            'suby', subl);
        if ~flag
            return
        end
        
        %         [flag, Z_Mosa] = sonar_mosaique(a, LatLon(1), LatLon(2), resol, ...
        %             'AcrossInterpolation', 2, 'AlongInterpolation',  2, ...
        %             'suby', subl);
        Z_Mosa.Writable = false;
        
        %         [flag, A_Mosa] = sonar_mosaique(A, LatLon(1), LatLon(2), resol, ...
        %             'AcrossInterpolation', 2, 'AlongInterpolation',  2, ...
        %             'suby', subl);
        A_Mosa.Writable = false;
        
        
        if isempty(Z_Mosa_All)
            Z_Mosa_All = Z_Mosa;
            A_Mosa_All = A_Mosa;
        else
            [Z_Mosa_All, A_Mosa_All] = mosaique([Z_Mosa_All Z_Mosa], 'Angle', [A_Mosa_All A_Mosa], 'FistImageIsCollector', 1);
            clear Z_Mosa A_Mosa
        end
        Z_Mosa_All.Name = extract_ImageName(Z_Mosa_All, 'Name', TitreDepth);
        A_Mosa_All.Name = extract_ImageName(A_Mosa_All, 'Name', TitreDepth);
        Z_Mosa_All = update_Name(Z_Mosa_All);
        A_Mosa_All = update_Name(A_Mosa_All);
        Z_Mosa_All.Writable = false;
        A_Mosa_All.Writable = false;
        
        % ----------------------------------
        % Suppression des fichier memmapfile
        
        %         deleteFileOnListe(cl_memmapfile.empty, gcbf)
    end
    clear a A LatLon
    
    %% Sauvegarde des mosaiques dans des fichiers ErMapper
    
    if (mod(k1,Backup) == 0) || (k1 == nbProfils)
        Z_Mosa_All = majCoordonnees(Z_Mosa_All);
        A_Mosa_All = majCoordonnees(A_Mosa_All);
        export_ermapper(Z_Mosa_All, nomFicErMapperLayer);
        export_ermapper(A_Mosa_All, nomFicErMapperAngle);
        
        % D�placement Mikael
        deleteFileOnListe(cl_memmapfile.empty, gcbf)
        % D�placement Mikael
    end
end
my_close(hw, 'MsgEnd')
flag = 1;

clear Mosa
Mosa(1) = Z_Mosa_All;
Mosa(2) = A_Mosa_All;
