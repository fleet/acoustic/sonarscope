% Cr�ation des images PingAcrossDist � partir des images PingSamples de sonar lateraux
%
% Syntax
%   [flag, nomDir] = ErMapperSL_PingRange2PingAcrossDist(...)
%
% Name-Value Pair Arguments
%   nomDir        : Nom du r�pertoire des donnees
%   resol         : Pas de la grille en m�tres
%   NomFicCompens : Nom du fichier de compensation
%   repExport     : R�pertoire de donnees par defaut
%
% Output Arguments
%   []     : Auto-plot activation
%   flag   : Description du parametre (unite).
%   a      : Instances de cl_image contenant la mosaique et la mosaique angulaire
%   nomDir : Nom du r�pertoire des images cr��es
%
% Examples
%   flag = ErMapperSL_PingRange2PingAcrossDist('NomDir', 'G:\REBENT_PROCESSING\PingRange', 'Resol', 0.5);
%
% See also Authors
% Authors : JMA
%--------------------------------------------------------------------------

function [flag, repImport, repExport] = ErMapperSL_PingRange2PingAcrossDist(repImport, repExport, varargin)

[varargin, resolutionX]   = getPropertyValue(varargin, 'Resol',         []);
[varargin, NomFicCompens] = getPropertyValue(varargin, 'NomFicCompens', []); %#ok<ASGLU>

I0 = cl_image_I0;

%% Recherche des fichiers .ers

% [flag, liste, nomDir] = uiSelectFiles('ExtensionFiles', '.ers', 'RepDefaut', nomDir, ...
%     'InitGeometryType', cl_image.indGeometryType('PingRange'));
[flag, liste, nomDir] = uiSelectFiles('ExtensionFiles', {'.ers', '.xml'}, 'RepDefaut', repImport, ...
    'InitGeometryType', cl_image.indGeometryType('PingSamples'));
if ~flag
    return
end
repImport = nomDir;

[flag, repExport] = my_uigetdir(nomDir, 'Pick an output directory');
if ~flag
    return
end

if isempty(resolutionX)
    str1 = 'Cette projection n�cessite de d�finir le pas en distance lat�rale';
    str2 = 'This projection needs to define the across distance grid size';
    str3 = 'Pas en distance lat�rale';
    str4 = 'Across distance step';
    [flag, resolutionX] = inputOneParametre(Lang(str1,str2), Lang(str3,str4), ...
        'Value', 0.05, 'MinValue', 0, 'Unit', 'm');
    if ~flag
        return
    end
end

%% Fichier de compensation

DataTypeConditions = [];
if isempty(NomFicCompens)
    [rep, flag] = my_questdlg(Lang('Voulez-vous faire une compensation ?', 'Enter a compensation file ?'), 'Init', 2);
    if flag && (rep == 1)
        nomFic = fullfile(repExport, '*.mat');
        [flag, NomFicCompens] = my_uigetfile(nomFic, 'Pick a .mat file');
        if ~flag
            return
        end
        
        CourbesStatistiques = loadmat(NomFicCompens, 'nomVar', 'CourbeConditionnelle');
        bilan = CourbesStatistiques.bilan;
        DataTypeConditions = bilan{1}.DataTypeConditions;
    end
else
    CourbesStatistiques = loadmat(NomFicCompens, 'nomVar', 'CourbeConditionnelle');
    bilan = CourbesStatistiques.bilan;
    DataTypeConditions = bilan{1}.DataTypeConditions;
end


nbFiles = length(liste);
str1 = 'Cr�ation des images en g�om�trie PingAcrossDist';
str2 = 'Create images in pingAcrossDist geometry';
hw = create_waitbar(Lang(str1,str2), 'N', nbFiles);
for k=1:nbFiles
    my_waitbar(k, nbFiles, hw);
    
    fprintf('%d/%d : %s\n', k, nbFiles, liste{k});
    
    [~, ~, Ext] = fileparts(liste{k});
    switch Ext
        case '.ers'
            [flag, a] = cl_image.import_ermapper(liste{k});
        case '.xml'
            [flag, a] = cl_image.import_xml(liste{k});
    end
    if ~flag
        str = sprintf('ErMapper importation failure for %s', liste{k});
        my_warndlg(str, 1);
        continue
    end
    
    %% Test si courbe conditionnelle compatible
    
    if ~isempty(NomFicCompens)
        flag = testSignature(a, 'DataType', cl_image.indDataType(bilan{1}.DataTypeValue));
        if ~flag
            return
        end
    end
    
    %% Slant range correction
    
    b = sonarLateral_PingRange2PingAcrossDist(a, 'resolutionX', resolutionX, 'createNbp', 0);
    %     imagesc(b)
    
    %% Calcul des layers conditionnels
    
    for k2=1:length(DataTypeConditions)
        switch DataTypeConditions{k2}
            case 'TxAngle'
                c = sonar_lateral_emission(b, 'useRoll', 0);
                
                nomFicOut = creNomFicErs(c, repExport, resolutionX);
                export_ermapper(c, nomFicOut);
                
                b = compensationCourbesStats(b, c, bilan);
                clear c
            otherwise
                str = sprintf('Layer %s pas encore integr� � ErMapperSL_PingRange2PingAcrossDist', DataTypeConditions{k2});
                my_warndlg(str, 1);
                flag = 0;
                return
        end
    end
    
    %% Exportation au format ErMapper
    
    nomFicOut = creNomFicErs(b, repExport, resolutionX);
    b.InitialFileName = nomFicOut;
    export_ermapper(b, nomFicOut);
    
    clear a b
end
my_close(hw, 'MsgEnd')
flag = 1;



function nomFicReflectivity = creNomFicErs(this, repExport, resolutionX)

ImageName = extract_ImageName(this);
% [~, ImageName] = fileparts(ImageName);
ImageName = [ImageName '_' num2str(resolutionX) 'm'];

% nomFicReflectivity(nomFicReflectivity == '.') = ',';
this = update_Name(this, 'Name', ImageName);
ImageName = this.Name;
% nomFicReflectivity = [nomFicReflectivity '.ers'];

nomFicReflectivity = fullfile(repExport, [ImageName '.ers']);
