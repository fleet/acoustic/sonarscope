% Cr�ation des images PingAcrossDist � partir des images PingSamples de sonar lateraux
%
% Syntax
%   [flag, nomDir] = ErMapper_SignalFiltre(...)
%
% Name-Value Pair Arguments
%   nomDir        : Nom du r�pertoire des donnees
%   resol         : Pas de la grille en m�tres
%   NomFicCompens : Nom du fichier de compensation
%
% Output Arguments
%   []     : Auto-plot activation
%   flag   : Description du parametre (unite).
%   a      : Instances de cl_image contenant la mosaique et la mosaique angulaire
%   nomDir : Nom du r�pertoire des images cr��es
%
% Examples
%   flag = ErMapper_SignalFiltre('NomDir', 'G:\REBENT_PROCESSING\PingRange', 'Resol', 0.5);
%
% See also Authors
% Authors : JMA
%--------------------------------------------------------------------------

function [flag, nomDir] = ErMapper_SignalFiltre(varargin)

[varargin, nomDir] = getPropertyValue(varargin, 'NomDir', []);
[varargin, Ordre]  = getPropertyValue(varargin, 'Ordre',  []);
[varargin, Wc]     = getPropertyValue(varargin, 'Wc',     []); %#ok<ASGLU>

if isempty(nomDir)
    nomDir = pwd;
end

I0 = cl_image_I0;

%% Recherche des fichiers .ers

[flag, liste, nomDir] = uiSelectFiles('ExtensionFiles', '.ers', 'RepDefaut', nomDir, ...
    'InitGeometryType', cl_image.indGeometryType('PingRange'));
if ~flag
    return
end

fig = figure;
for k1=1:length(liste)
    disp('---------------------------------------------------------------')
    %     [nomDir, nomFic] = fileparts(liste{k1});
    [flag, a] = cl_image.import_ermapper(liste{k1});
    if ~flag
        str = sprintf('ErMapper importation failure for %s', liste{k1});
        my_warndlg(str, 1);
        continue
    end
    
    if k1 == 1
        listeSignaux = get_liste_vecteurs(a, 'VerticalOnly');
        if length(listeSignaux) <= 1
            return
        end
        listeSignaux = listeSignaux(2:end);
        [indSignal, flag] = my_listdlg(Lang('Courbe � visualiser', 'Curve to display'), listeSignaux);
        if ~flag
            return
        end
        
        if isempty(Ordre) || isempty(Wc)
            [flag, Ordre, Wc] = SaisieParamsButterworth(2, 0.1);
            if ~flag
                return
            end
            [B,A] = butter(Ordre, Wc);
        end
    end
    
    
    for k2=1:length(indSignal)
        nomSignal = listeSignaux{indSignal(k2)};
        val = get(a, ['Sonar' nomSignal]);
        
        subNaN = find(isnan(val));
        subNonNaN = find(~isnan(val));
        
        if ~isempty(subNaN)
            val(subNaN) = interp1(subNonNaN, val(subNonNaN), subNaN, 'linear', 'extrap');
        end
        
        if strcmp(nomSignal, 'Heading')
            % Filtrage d'un cap en degres
            valFiltree = val * (pi/180);
            valFiltree = unwrap(valFiltree);
            valFiltree1 = filtfilt(B, A, double(valFiltree));
            valFiltree1 = valFiltree1 * (180/pi);
            valFiltree1 = mod(valFiltree1+360, 360);
        else
            valFiltree1 = filtfilt(B, A, double(val));
        end
        
        figure(fig); hold off; plot(val, 'b'); hold on; plot(valFiltree1, 'r'); grid on; title(liste{k1})
        
        [rep, flag] = my_questdlg(Lang('TODO', 'Do you validate the filter ?'));
        if ~flag
            return
        end
        if rep == 1
            a = set(a, ['Sonar' nomSignal], valFiltree1);
        end
    end
    
    % ------------------------------
    % Exportation au format ErMapper
    
    export_ermapper_sigV(a, liste{k1});
end
flag = 1;

