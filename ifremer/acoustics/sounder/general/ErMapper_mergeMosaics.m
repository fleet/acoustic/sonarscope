function [flag, this, repExport] = ErMapper_mergeMosaics(repExport)

this = [];

%% Liste des fichiers contenant un MNT (ou une mosaïque)

[flag, DTM_Filenames, repExport] = uiSelectFiles('ExtensionFiles', {'.ers'}, 'ChaineExclue', 'RxBeamAngle', 'RepDefaut', repExport);
if ~flag
    return
end

%% Recherche des mosaïques d'angles correspondantes

Angle_Filenames = {};
for k=1:length(DTM_Filenames)
    [ImageName, indGeometryType, indexDataType] = cl_image.uncode_ImageName(DTM_Filenames{k}(1:end-4)); %#ok<ASGLU>
    filename = strrep(DTM_Filenames{k}, cl_image.strDataType{indexDataType}, 'RxBeamAngle');
    if exist(filename, 'file')
        Angle_Filenames{end+1} = filename; %#ok<AGROW>
    end
end

%% Lecture des DTM

[flag, IndividualDTM] = cl_image.import_ermapper(DTM_Filenames);
if ~flag
    return
end

%% Question Type de merging

str{1} = 'Overlapping management';
str{2} = 'Mean';
str{3} = 'Median';
str{4} = 'Min';
str{5} = 'Max';
[rep, flag] = my_listdlg('Liste of items', str, 'SelectionMode', 'Single');
if ~flag
    return
end

%% Algo

switch rep
    case 1 % Overlapping management
        
        %% Lecture des images d'angles
        
        [~, IndividualAngles] = cl_image.import_ermapper(Angle_Filenames);
        
        %% Paramètres de merging
        
        [flag, listeprofils, listeprofilsAngle, pasx, pasy, CoveringPriority] = params_MosaiqueAssemblage([IndividualDTM IndividualAngles], 1, 'listeprofils', 1:length(IndividualDTM));
        if ~flag
            return
        end
        
        %% Assemblage des mosaiques individuelles
        
        [flag, M, A] = mergeMosaics(IndividualDTM(listeprofils), 'Angle', IndividualAngles(listeprofilsAngle - length(listeprofils)), ...
            'CoveringPriority', CoveringPriority, 'pasx', pasx, 'pasy', pasy);
        if ~flag
            return
        end
        this = [M, A];
        
    case 2 % mean
        ImageName = 'Mean_Images';
        [flag, listeImages, ~, ~, ~, ComputationUnit] = paramsFctsAritmMultiImages(IndividualDTM, 1, 'Mean', ...
            'flagSelectLayers', 0, 'flagTypeCadre', 0, 'flagImageName', 0);
        if flag
            [flag, this] = MultiImages_Mean(IndividualDTM(listeImages), ImageName, 'ComputationUnit', ComputationUnit);
            if ~flag
                return
            end
        end
        
    case 3 % Median
        ImageName = 'Median_Images';
        [flag, this] = MultiImages_Median(IndividualDTM, ImageName);
        if ~flag
            return
        end
        
    case 4 % Min
        ImageName = 'Min_Images';
        [flag, this] = MultiImages_Min(IndividualDTM, ImageName);
        if ~flag
            return
        end
        
    case 5 % Max
        ImageName = 'Max_Images';
        [flag, this] = MultiImages_Max(IndividualDTM, ImageName);
        if ~flag
            return
        end
end
