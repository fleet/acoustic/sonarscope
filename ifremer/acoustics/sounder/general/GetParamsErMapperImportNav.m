function [flag, NomFicNav, listeFic, FiltreNav, OrdreButter, Wc, CalculHeading, repImport] ...
    = GetParamsErMapperImportNav(repImport)

listeFic      = [];
FiltreNav     = [];
OrdreButter   = [];
Wc            = [];
CalculHeading = [];

% NomFicNav = 'G:\REBENT\imisol\nvi\total_aveclayback.nvi'
[flag, NomFicNav, repImport] = uiSelectFiles('ExtensionFiles', '.nvi', 'RepDefaut', repImport);
if ~flag
    return
end
if length(NomFicNav) > 1
    my_warndlg('Only one file for the moment', 1);
end

% listeFic = listeFicOnDir('C:\TEMP\TESTS\PingRange', '.ers');
[flag, listeFic, repImport] = uiSelectFiles('ExtensionFiles', '.ers', 'RepDefaut', repImport);
if ~flag
    return
end

[FiltreNav, flag] = my_questdlg(Lang('Filtrage de la navigation import�e ?', 'Filter the imported navigation ?'), ...
    'Init', 2);
if ~flag
    return
end

if FiltreNav == 1
    [flag, OrdreButter, Wc] = SaisieParamsButterworth(2, 0.02);
    if ~flag
        return
    end
end
[CalculHeading, flag] = my_questdlg(Lang('Calcul du cap � partir de la navigation', 'Compute the heading from the navigation ?'), ...
    'Init', 2);
if ~flag
    return
end
