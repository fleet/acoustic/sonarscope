% Calcul de l'Index de directivite d'une antenne
%
% Syntax
%   index = IndexDirectivite( ouvertureLongit, ouvertureTransv )
%
% Input Arguments 
%   ouvertureLongit  : Ouverture longitudinale a -3dB (deg)
%   ouvertureTransv  : Ouverture transversale  a -3dB (deg)
%
% Output Arguments 
%   index : index de directivite (dB)
%
% Examples 
%   index = IndexDirectivite( 1.2, 150. )
%
% See also IndexDirectiviteCircle Authors
% Authors : JMA + XL
%--------------------------------------------------------------------------------

function index = IndexDirectivite( ouvertureLongit, ouvertureTransv )

tetaLongit = ouvertureLongit * pi / 180;
tetaTransv = ouvertureTransv * pi / 180;
index = reflec_Enr2dB(4 * pi ./ (tetaLongit .* tetaTransv));
