% Calcul de l'Index de directivite d'une antenne circulaire
%
% Syntax
%   index = IndexDirectiviteCircle( ouverture )
%
% Input Arguments 
%   ouverture  : Ouverture a -3dB (deg)
%
% Output Arguments 
%   index : index de directivite (dB)
%
% Examples 
%   index = IndexDirectiviteCircle( 30 )
%
% See also IndexDirectivite Authors
% Authors : JMA + XL
%--------------------------------------------------------------------------------

function index = IndexDirectiviteCircle( ouverture )
index = 2 * reflec_Enr2dB(pi * 59 ./ ouverture);

