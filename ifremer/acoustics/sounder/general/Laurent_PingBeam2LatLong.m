% Realisation de la mosaique de donnees des "DepthDatagrams" d'une campagne
%
% Syntax
%   [flag, Mosa, nomDirImport, nomDirExport] = Laurent_PingBeam2LatLong(FileList, Resol, Backup, Window, iSlice, nomFicErMapperLayer, nomFicErMapperAngle)
%
 % Input Arguments
%   FileList : Liste des fichiers .all
%   Resol    : Pas de la grille en m�tres
%   Backup   : P�riode de sauvegarde des fichiers de sortie
%   Window   : Fen�tre utilis�e lors de l'interpolation
%   Window   :  Num�ro de la tranche (1=pr�s du fond)
%   nomFicErMapperLayer : nom du fichier ErMapper de sauvegarde de la r�flectivit�
%   nomFicErMapperAngle : nom du fichier ErMapper de sauvegarde de l'angle
%
% Output Arguments
%   flag         : Description du parametre (unite).
%   a            : Instances de cl_image contenant la mosaique et la mosaique angulaire
%   nomDirImport : Nom du r�pertoire des .all
%   nomDirExport : Nom du r�pertoire des images cr��es
%
% Examples
%   [flag, FileList] = uiSelectFiles('ExtensionFiles', '.mat', 'RepDefaut',  'C:\Temp\Marmesonet\EM302_EI');
%   Resol = 5;  Backup = 20; Window = [5 5];
%   iSlice = 1;
%   nomFicErMapperLayer = [];
%   nomFicErMapperAngle = [];
%   [flag, a] = Laurent_PingBeam2LatLong(FileList, Resol, Backup, Window, iSlice, nomFicErMapperLayer, nomFicErMapperAngle);
%   SonarScope(a)
%
% See also WCD.Params.EchoIntegration Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function [flag, Mosa, nomDirImport, nomDirExport] = Laurent_PingBeam2LatLong(FileList, Resol, Backup, Window, iSlice, nomFicErMapperLayer, nomFicErMapperAngle, varargin)

Mosa = [];

nbProfils = length(FileList);
nomDirImport = fileparts(FileList{1});

if isempty(nomFicErMapperLayer)
    nomDirExport = [];
    TitreDepth = sprintf('%d', iSlice);
    TitreAngle = sprintf('%d', iSlice);
else
    [nomDirExport, TitreDepth] = fileparts(nomFicErMapperLayer);
    [~, TitreAngle] = fileparts(nomFicErMapperAngle);
end

%% Recuperation des mosaiques existantes

I0 = cl_image_I0;
if exist(nomFicErMapperLayer, 'file') && exist(nomFicErMapperAngle, 'file')
    [flag, Z_Moza_All] = cl_image.import_ermapper(nomFicErMapperLayer);
    if ~flag
        return
    end
    %             Carto = get(Z_Moza_All, 'Carto');
    
    [flag, A_Moza_All]  = cl_image.import_ermapper(nomFicErMapperAngle);
    if ~flag
        return
    end
else
    Z_Moza_All = [];
    A_Moza_All = [];
end


%% Constitution de la mosaique / MNT

GeometryType = cl_image.indGeometryType('PingBeam');
Carto = cl_carto('Ellipsoide.Type', 11);

    
% profile on
% tic
hw = create_waitbar('Processing Mosaic', 'N', nbProfils);
for iProfil=1:nbProfils
    my_waitbar(iProfil, nbProfils, hw);
    
    fprintf('\n--------------------------------------------------------------------------------------\n')
    fprintf('Processing file %d/%d : %s\n', iProfil, nbProfils, FileList{iProfil});
    
    latME70         = loadmat(FileList{iProfil}, 'nomVar', 'Lat_botME70');
    longME70        = loadmat(FileList{iProfil}, 'nomVar', 'Long_botME70');
    Sa_bottom_ME70  = loadmat(FileList{iProfil}, 'nomVar', 'Sa_botME70');
    time_ME70       = loadmat(FileList{iProfil}, 'nomVar', 'time_ME70');
    
    t  = cl_time('timeMat', time_ME70/86400 + 719529);
    
    Lat = squeeze(latME70(:,iSlice,:));
    Lon = squeeze(longME70(:,iSlice,:));
    Reflec = squeeze(Sa_bottom_ME70(:,iSlice,:));
    
    m = mean(Lat(:), 'omitnan');
    s = std(Lat(:));
    sub = (Lat < (m-5*s)) | (Lat > (m+5*s));
    Lat(sub) = NaN;
    m = mean(Lon(:), 'omitnan');
    s = std(Lon(:));
    sub = (Lon < (m-5*s)) | (Lon > (m+5*s));
    Lon(sub) = NaN;
    Reflec = reflec_Enr2dB(Reflec);
    %{
    figure;
    geoshow(Lat, Lon, Reflec,'DisplayType','surface'); grid on;
    %}
    
    nbPings = size(Reflec, 1);
    nbBeams = size(Reflec, 2);
    A = (1:nbBeams) -  nbBeams/2;
    A = repmat(single(A), nbPings, 1);
    
    PingCounter = 1:size(Reflec, 1);
    
    DataType = cl_image.indDataType('Reflectivity');
    Z   = cl_image('Image', Reflec, 'GeometryType', GeometryType, 'DataType', DataType, ...
        'Carto', Carto, 'SonarPingCounter', PingCounter, 'SonarTime', t);
    DataType = cl_image.indDataType('TxAngle');
    A   = cl_image('Image', A,      'GeometryType', GeometryType, 'DataType', DataType, ...
        'Carto', Carto, 'SonarPingCounter', PingCounter, 'SonarTime', t);
    DataType = cl_image.indDataType('Latitude');
    Lat = cl_image('Image', Lat,    'GeometryType', GeometryType, 'DataType', DataType, ...
        'Carto', Carto, 'SonarPingCounter', PingCounter, 'SonarTime', t);
    DataType = cl_image.indDataType('Longitude');
    Lon = cl_image('Image', Lon,    'GeometryType', GeometryType, 'DataType', DataType, ...
        'Carto', Carto, 'SonarPingCounter', PingCounter, 'SonarTime', t);
    
    
    %% Calcul de la mosaique individuelle
    
    [flag, Z_Mosa, A_Mosa] = sonar_mosaique2(Z, A, Lat, Lon, Resol, ...
        'AcrossInterpolation', 2, 'AlongInterpolation',  1, 'NoWaitbar');
    if ~flag
        return
    end
    clear Lat Lon Z A
    
    %% Interpolation
    
    if ~isequal(Window, [1 1])
        Z_Mosa = WinFillNaN(Z_Mosa, 'window', Window, 'NoStats');
        A_Mosa = WinFillNaN(A_Mosa, 'window', Window, 'NoStats');
    end
    
    [Z_Moza_All, A_Moza_All] = mosaique([Z_Moza_All Z_Mosa], 'Angle', [A_Moza_All A_Mosa], ...
        'FistImageIsCollector', 1);
    Z_Moza_All = set(Z_Moza_All, 'Name', TitreDepth, 'TagSynchroX', 'Longitude', 'TagSynchroY', 'Latitude', 'YDir', 1);
    A_Moza_All = set(A_Moza_All, 'Name', TitreAngle, 'TagSynchroX', 'Longitude', 'TagSynchroY', 'Latitude', 'YDir', 1);
    clear Mosa
    Mosa(1) = Z_Moza_All;
    Mosa(2) = A_Moza_All;
    
    clear c
    
    %% Sauvegarde des mosaiques dans des fichiers ErMapper
    
    if (mod(iProfil,Backup) == 0) || (iProfil == nbProfils)
        Z_Moza_All = majCoordonnees(Z_Moza_All);
        A_Moza_All = majCoordonnees(A_Moza_All);
        if ~isempty(nomFicErMapperLayer)
            export_ermapper(Z_Moza_All, nomFicErMapperLayer);
        end
        if ~isempty(nomFicErMapperAngle)
            export_ermapper(A_Moza_All, nomFicErMapperAngle);
        end
    end
    
    %% Suppression des fichier memmapfile
    
    deleteFileOnListe(cl_memmapfile.empty, gcbf)
    
    %% Garbage Collector de Java
    
    java.lang.System.gc
end
my_close(hw, 'MsgEnd')
