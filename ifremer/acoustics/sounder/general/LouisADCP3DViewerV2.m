% nomDir = 'F:\ALTRAN_DataPourV2\Donnees_ADCP_ASPEX';
% [flag, NomFicMat, nomDir] = uiSelectFiles('ExtensionFiles', '.mat', 'RepDefaut', nomDir)
% nomDirOut = fullfile(nomDir, 'SSc3DV2')
% LouisADCP3DViewerV2(NomFicMat, nomDirOut)

function flag = LouisADCP3DViewerV2(NomFicMat, nomDirOut)

if ~iscell(NomFicMat)
    NomFicMat = {NomFicMat};
end

N = length(NomFicMat);
for k=1:N
    [~, filename] = fileparts(NomFicMat{k});
    if ~contains('Fluo', filename)
        NomFicXML = fullfile(nomDirOut, [filename '_Temp.xml']);
        LouisADCP3DViewerV2_unitaire(NomFicMat{k}, NomFicXML, 'Temp', 'deg');
        NomFicXML = fullfile(nomDirOut, [filename '_Salt.xml']);
        LouisADCP3DViewerV2_unitaire(NomFicMat{k}, NomFicXML, 'Salt', '0/00');
    else
        NomFicXML = fullfile(nomDirOut, [filename '.xml']);
        LouisADCP3DViewerV2_unitaire(NomFicMat{k}, NomFicXML, 'Fluo', 'TODO');
    end
end


function flag = LouisADCP3DViewerV2_unitaire(NomFicMat, NomFicXML, TypeData, Unit)

load(NomFicMat)

LatI = LatI(1,:);
LonI = LonI(1,:);
ZI   = ZI(:,1);

%{
figure; plot(-BathI); grid on; title('BathI')
figure; plot(LonI, LatI); grid on; title('Nav')
figure; plot(ZI); grid on; title('ZI')
figure; imagesc(Salt); colorbar; title('Salt')
figure; imagesc(Temp); colorbar; title('Temp')
%}

nbPings  = length(LatI);

map = jet(256);

switch TypeData
    case 'Temp'
        CLim = [10 20];
        X = Temp;
    case 'Salt'
        CLim = [35.25 35.70];
        X = Salt;
    case 'Fluo'
        CLim = [0 13];
        X = Fluo;
end

%% Cr�ation du r�pertoire

[nomDirXml, nomDirBin] = fileparts(NomFicXML);
nomDirXml = fullfile(nomDirXml, nomDirBin);
nomDirRoot = fileparts(nomDirXml);
if ~exist(nomDirXml, 'dir')
    flag = mkdir(nomDirXml);
    if ~flag
        messageErreurFichier(nomDirXml, 'WriteFailure');
        return
    end
end

%% Cr�ation du r�pertoire

nomDirPNG = fullfile(nomDirXml, '000');
if ~exist(nomDirPNG, 'dir')
    flag = mkdir(nomDirPNG);
    if ~flag
        messageErreurFichier(nomDirPNG, 'WriteFailure');
        return
    end
end

%% Cr�ation de l'image PNG

I = gray2ind(mat2gray(X, double(CLim)), size(map,1));
% I = flipud(I); % A confirmer
subNaN = isnan(X);
I(I == 0) = 1;
I(subNaN) = 0;

nomFic1 = '00000.png';
nomFic2 = fullfile(nomDirPNG, nomFic1);
imwrite(I, map, nomFic2, 'Transparency', 0)

%% Definition of the structure

Info.Signature1      = 'SonarScope';
Info.Signature2      = 'ImagesAlongNavigation';
Info.FormatVersion   = 20100624;
Info.Name            = nomDirBin;
% Info.Description.Content    = TypeData;
% Info.Description.Unit       = Unit;
Info.ExtractedFrom   = 'Louis Mat files';

Info.Survey         = 'Unknown';
Info.Vessel         = 'Unknown';
Info.Sounder        = 'Unknown';
Info.ChiefScientist = 'Unknown';


Info.Dimensions.nbPings  = nbPings;
Info.Dimensions.nbSlices = 1;
Info.Dimensions.nbRows   = 0;

Data.AcrossDistance =  0;
Data.SliceExists    = true;
Data.Date           = NaN(1, nbPings);
Data.Hour           = NaN(1, nbPings);
Data.PingNumber     = 1:nbPings;
Data.LatitudeNadir  = LatI;
Data.LongitudeNadir = LonI;
Data.LatitudeTop    = LatI;
Data.LongitudeTop   = LonI;
Data.DepthTop       = zeros(1, nbPings);
Data.DepthBottom    = ones(1, nbPings) * -max(ZI);


Info.Signals(1).Name          = 'Date';
Info.Signals(end).Dimensions  = '1, nbPings';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'Ifremer';
Info.Signals(end).FileName    = fullfile(nomDirBin,'Date.bin');

Info.Signals(end+1).Name      = 'Hour';
Info.Signals(end).Dimensions  = '1, nbPings';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'nb milliseconds since midnight';
Info.Signals(end).FileName    = fullfile(nomDirBin,'Hour.bin');

Info.Signals(end+1).Name      = 'PingNumber';
Info.Signals(end).Dimensions  = '1, nbPings';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = ' ';
Info.Signals(end).FileName    = fullfile(nomDirBin,'PingNumber.bin');

Info.Signals(end+1).Name      = 'LatitudeNadir';
Info.Signals(end).Dimensions  = '1, nbPings';
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile(nomDirBin, 'LatitudeNadir.bin');

Info.Signals(end+1).Name      = 'LongitudeNadir';
Info.Signals(end).Dimensions  = '1, nbPings';
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile(nomDirBin, 'LongitudeNadir.bin');

Info.Signals(end+1).Name      = 'SliceExists';
Info.Signals(end).Dimensions  = 'nbSlices, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile(nomDirBin,'SliceExists.bin');

Info.Signals(end+1).Name      = 'DepthTop';
Info.Signals(end).Dimensions  = '1, nbPings';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'm';
Info.Signals(end).FileName    = fullfile(nomDirBin, 'DepthTop.bin');

Info.Signals(end+1).Name      = 'DepthBottom';
Info.Signals(end).Dimensions  = '1, nbPings';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'm';
Info.Signals(end).FileName    = fullfile(nomDirBin, 'DepthBottom.bin');

Info.Signals(end+1).Name      = 'AcrossDistance';
Info.Signals(end).Dimensions  = 'nbSlices, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'm';
Info.Signals(end).FileName    = fullfile(nomDirBin, 'AcrossDistance.bin');


Info.Images(1).Name           = 'LatitudeTop';
Info.Images(end).Dimensions   = 'nbSlices, nbPings';
Info.Images(end).Storage      = 'double';
Info.Images(end).Unit         = 'deg';
Info.Images(end).FileName     = fullfile(nomDirBin, 'LatitudeTop.bin');

Info.Images(end+1).Name       = 'LongitudeTop';
Info.Images(end).Dimensions   = 'nbSlices, nbPings';
Info.Images(end).Storage      = 'double';
Info.Images(end).Unit         = 'deg';
Info.Images(end).FileName     = fullfile(nomDirBin,'LongitudeTop.bin');

% Info.Images(end+1).Name       = 'Reflectivity';
% Info.Images(end).Dimensions   = 'nbRows, nbPings, nbSlices';
% Info.Images(end).Storage      = 'single';
% Info.Images(end).Unit         = 'dB';
% Info.Images(end).FileName     = fullfile(nomDirBin,'Reflectivity.bin');


%% Cr�ation du fichier XML d�crivant la donn�e

% NomFicXML = fullfile(nomDirRacine, [TypeDataVoxler '.xml']);
xml_write(NomFicXML, Info);
flag = exist(NomFicXML, 'file');
if ~flag
    messageErreur(NomFicXML)
    return
end

%% Cr�ation du r�pertoire Info

nomDirInfo = nomDirXml;
if ~exist(nomDirInfo, 'dir')
    status = mkdir(nomDirInfo);
    if ~status
        messageErreur(nomDirInfo)
        flag = 0;
        return
    end
end

%% Cr�ation des fichiers binaires des signaux

for i=1:length(Info.Signals)
    flag = writeSignal(nomDirRoot, Info.Signals(i), Data.(Info.Signals(i).Name));
    if ~flag
        return
    end
end

%% Cr�ation des fichiers binaires des signaux

for i=1:length(Info.Images)
    flag = writeImage(nomDirRoot, Info.Images(i), Data.(Info.Images(i).Name));
    if ~flag
        return
    end
end

flag = 1;




