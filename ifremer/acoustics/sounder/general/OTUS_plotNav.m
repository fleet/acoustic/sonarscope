 % Description
 %   Plot a navigation file
 %
 % Syntax
 %   flag = OTUS_plotNav(nomFicNav)
 %
 % Input Arguments
 %   nomFicNav : Name of the navigation file
 %
 % Output Arguments
 %   [] : Auto-plot activation
 %   flag : 0=KO, 1=OK
 %
 % Examples
 %   nomFicNav = 'C:\DemoBICOSE\NavigationAttitudeOTUS.xml';
 %   flag = OTUS_plotNav(nomFicNav)
 %
 %   Authors : JMA
 %   See also OTUS_PlotNavigationAndImages OTUS_plotNavAndImagesInGoogleEarth
 %  -------------------------------------------------------------------------------
  
 function flag = OTUS_plotNav(nomFicNav)
 
 %% Lecture du fichier qui contient les informations de positionnement des images
 
 [flag, LatNav, LonNav, tNav] = lecFicNav(nomFicNav);
 if ~flag
     return
 end
 
 %% Trac�s de la navigation
 
 plot_navigation(nomFicNav, [], LonNav, LatNav, tNav, [], 'NavOTUS', 1);
