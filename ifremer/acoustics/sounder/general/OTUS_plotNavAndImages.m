% Description
%   Plot a navigation file and display the seabed pictures acorting to the
%   position of the mouse over the navigation plot
%
% Syntax
%   flag = OTUS_plotNavAndImages(nomFicNav, listeFicImages, decalageHoraire)
%
% Input Arguments
%   nomFicNav       : Name of the navigation file
%   listeFicImages  : List of tje image files
%   decalageHoraire : Offset of the image times vs the navigation file
%
% Output Arguments
%   flag : 0=KO, 1=OK
%
% Examples
%   nomFicNav = 'C:\DemoBICOSE\NavigationAttitudeOTUS.xml';
%   listeFicImages = listeFicOnDir('C:\DemoBICOSE', '.tif');
%   decalageHoraire = 0;
%   flag = OTUS_plotNavAndImages(nomFicNav, listeFicImages, decalageHoraire)
%
% Authors : JMA
% See also OTUS_plotNav OTUS_plotNavAndImagesInGoogleEarth
%  -------------------------------------------------------------------------------

function flag = OTUS_plotNavAndImages(nomFicNav, listeFicImages, decalageHoraire)

%% Lecture du fichier qui contient les informations de positionnement des images

[flag, LatNav, LonNav, tNav] = lecFicNav(nomFicNav);
if ~flag
    return
end

%% Trac�s de la navigation

% Fig1 = plot_navigation(nomFicNav, [], LonNav, LatNav, tNav, [], 'NavOTUS', 1);

%% Lecture des heures des images

[flag, tImages, nomCam, NomRacine, NavigationFileFormat] = getTimeSubmarineImages(nomFicNav, listeFicImages);
if ~flag
    return
end

%% Interpolation de la navigation aux heures des images

tNav = tNav - hours(decalageHoraire);
LatImages = my_interp1(tNav, LatNav, tImages);
LonImages = my_interp1_longitude(tNav, LonNav, tImages);
if all(isnan(LatImages))
    str1 = 'TODO';
    str2 = 'The navigation file does not correspond to the images.';
    my_warndlg(Lang(str1,str2), 1);
    flag = 0;
    return
end

%% Trac� de la position des images

[~, ~, ext] = fileparts(listeFicImages{1});
plot_navigation_OTUS(listeFicImages{1}, LonImages, LatImages, tImages, NavigationFileFormat, NomRacine, ext, ...
    'FileNames', listeFicImages, 'nomCam', nomCam);

