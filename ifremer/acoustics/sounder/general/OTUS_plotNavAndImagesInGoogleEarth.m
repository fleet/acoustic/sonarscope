% Description
%   Plot a navigation file and create pins on the navigation in Google Earth
%
% Syntax
%   flag = flag = OTUS_plotNavAndImagesInGoogleEarth(nomFicNav, listeFicImages, decalageHoraire, KmzFilename, flagFullFile)
%
% Input Arguments
%   nomFicNav       : Name of the navigation file
%   listeFicImages  : List of tje image files
%   decalageHoraire : Offset of the image times vs the navigation file
%   KmzFilename     : Name of the google KMZ output file
%   flagFullFile    : 1=Display the full image, 0=Display a matlab figure
%
% Output Arguments
%   flag : 0=KO, 1=OK
%
% Examples
%   nomFicNav = 'C:\DemoBICOSE\NavigationAttitudeOTUS.xml';
%   listeFicImages = listeFicOnDir('C:\DemoBICOSE', '.tif');
%   decalageHoraire = 0;
%   KmzFilename  = 'C:\DemoBICOSE\NavigationAttitudeOTUS-TEST.kmz';
%   flagFullFile = 1;
%   flag = OTUS_plotNavAndImagesInGoogleEarth(nomFicNav, listeFicImages(1:30:end), decalageHoraire, KmzFilename, flagFullFile)
%
% Authors : JMA
% See also OTUS_plotNav OTUS_PlotNavigationAndImages
%  -------------------------------------------------------------------------------

function flag = OTUS_plotNavAndImagesInGoogleEarth(nomFicNav, listeFicImages, ...
    decalageHoraire, KmzFilename, flagFullFile)

%% Lecture du fichier qui contient les informations de positionnement des images

[flag, LatNav, LonNav, tNav] = lecFicNav(nomFicNav);
if ~flag
    return
end

%% Trac� de la navigation

plot_navigation(nomFicNav, [], LonNav, LatNav, tNav, [], 'NavOTUS', 1);
plot_navigation_ExportGoogleEarth('NomFicKmz', KmzFilename)

%{
[nomDirSummary, SurveyName] = fileparts(nomFicAdoc);
Fig = ALL_PlotNav(this, 'IndNavUnit', IndNavUnit);
[nomFicFig, nomFicPng] = AdocUtils.exportFigAndPng(Fig, nomDirSummary, SurveyName, Tag, 'Navigation');
nomFicKmz = fullfile(nomDirSummary, SurveyName, 'KMZ', [SurveyName '-Navigation' Tag '.kmz']);
plot_navigation_ExportGoogleEarth('NomFicKmz', nomFicKmz);
nomFicShp = fullfile(nomDirSummary, SurveyName, 'SHP', [SurveyName '-Navigation.shp']);
plot_navigation_ShapeFile('Fig', Fig, 'nomFicShp', nomFicShp, 'geometry', 2);
nomFicXml3DV = fullfile(nomDirSummary, SurveyName, '3DV', [SurveyName '-Navigation' Tag '.xml']);
plot_navigation_ExportXML('NomFicXml', nomFicXml3DV)
my_close(Fig);
AdocUtils.addNavigation(nomFicAdoc, nomFicFig, nomFicPng, 'Navigation', 'SectionName', 'Navigation plots', ...
    'GoogleEarthFilename', nomFicKmz, 'GlobeFilename', nomFicXml3DV, 'listDataFiles', listDataFiles);
%}

%% Lecture des heures des images

[flag, tImages, nomCam, NomRacine] = getTimeSubmarineImages(nomFicNav, listeFicImages);
if ~flag
    return
end

%% Interpolation de la navigation aux heures des images

tNav = tNav - hours(decalageHoraire);
LatImages = my_interp1(tNav, LatNav, tImages);
LonImages = my_interp1_longitude(tNav, LonNav, tImages);
if all(isnan(LatImages))
    str1 = 'TODO';
    str2 = 'The navigation file does not correspond to the images.';
    my_warndlg(Lang(str1,str2), 1);
    flag = 0;
    return
end

%% Trac� de la position des images

FigImage = [];
N = length(listeFicImages);
nomFicImagePng = cell(N, 1);
hw = create_waitbar('Plot images', 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    fprintf('Display image %d / %d\n', k, N);
    
    if flagFullFile
        [~, nomFicSeul, Ext] = fileparts(listeFicImages{k});
        if strcmpi(Ext, '.tif') % Visiblement Google Eath n'aime pas les .tif
            A = imread(listeFicImages{k});
            nomFicImagePng{k} = fullfile(my_tempdir, [nomFicSeul '.jpg']);
            imwrite(A, nomFicImagePng{k});
        else
            nomFicImagePng{k} = listeFicImages{k};
        end
    else
        A = imread(listeFicImages{k});
        [~, nomImage] = fileparts(listeFicImages{k});
        if isempty(FigImage)
            FigImage = FigUtils.createSScFigure;
        end
        if size(A,3) == 3
            hi = image(A);
        else
            S = stats(A);
            CLim = S.Quant_25_75;
            hi = imagesc(A, CLim); axis equal; axis tight; colormap(gray(256));
        end
        axis equal; axis tight;
        hi.Parent.Visible = 'Off';
        ht = title(nomImage, 'Interpreter', 'none');
        ht.Visible = 'On';
        
        nomFicImagePng{k} = fullfile(my_tempdir, [nomImage '.png']);
        fig2fic(FigImage, nomFicImagePng{k})
%         my_close(FigImage);
    end
    
    InfoOneImage.AllFilename = nomFicImagePng{k}; % Pas utilis� par la suite
    InfoOneImage.PingNumber = k; % Pas utilis� par la suite
    InfoOneImage.Datetime   = tImages(k); % Pas utilis� par la suite
    InfoOneImage.Latitude   = LatImages(k);
    InfoOneImage.Longitude  = LonImages(k);
    InfoOneImage.Heading    = 0; % Pas utilis� par la suite
    InfoOneImage.DataName   = nomCam; % Pas utilis� par la suite
    InfoOneImage.MaxValue   = 0; % Pas utilis� par la suite
    InfoOneImage.Tag        = NomRacine; %'PAGURE';
%     setappdata(FigImage(k), 'InfoImage', InfoOneImage)
    InfoImage{k} = InfoOneImage; %#ok<AGROW>
end
my_close(hw, 'MsgEnd')
my_close(FigImage);

%% Cr�ation des punaises dans le KMZ

[nomDir, nomFic, Ext] = fileparts(KmzFilename);
nomFicKMZ = fullfile(nomDir, [nomFic 'Images' Ext]);
export_GoogleEarth_GeolocatedImage(nomFicKMZ, nomFicImagePng, InfoImage);
winopen(nomFicKMZ)

%% Fermeture des fen�tres et suppression des fichiers temporaires

if ~flagFullFile
%     my_close(FigImage)
    for k=1:N
        my_deleteFile(nomFicImagePng{k})
    end
end
