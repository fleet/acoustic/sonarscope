function [flag, Mosa, nomDirImport, nomDirExport] = PingAcrossDist2LatLong(FileList, Resol, Backup, Window, InfoMos, varargin)

[varargin, CorFile]       = getPropertyValue(varargin, 'CorFile',       []);
[varargin, ModeDependant] = getPropertyValue(varargin, 'ModeDependant', 0);
[varargin, LimitAngles]   = getPropertyValue(varargin, 'LimitAngles',   []);
[varargin, listeMode]     = getPropertyValue(varargin, 'listeMode',     []);
[varargin, listeSwath]    = getPropertyValue(varargin, 'listeSwath',    []);
[varargin, MosName]       = getPropertyValue(varargin, 'MosName',       []);
[varargin, listeFM]       = getPropertyValue(varargin, 'listeFM',       []);
[varargin, Info]          = getPropertyValue(varargin, 'Info',          ''); %#ok<ASGLU>

Mosa = [];

nbProfils = length(FileList);
nomDirImport = fileparts(FileList(1).nomFicReflec);

if isempty(InfoMos.nomFicErMapperLayer)
    TitreDepth = MosName;
    TitreAngle = MosName;
else
    [nomDirExport, TitreDepth] = fileparts(InfoMos.nomFicErMapperLayer);
    [~, TitreAngle] = fileparts(InfoMos.nomFicErMapperAngle);
end

%% R�cup�ration des mosa�ques existantes

if exist(InfoMos.nomFicErMapperLayer, 'file') && exist(InfoMos.nomFicErMapperAngle, 'file')
    [flag, Z_Moza_All] = cl_image.import_ermapper(InfoMos.nomFicErMapperLayer);
    if ~flag
        return
    end
    %             Carto = get(Z_Moza_All, 'Carto');
    
    [flag, A_Moza_All]  = cl_image.import_ermapper(InfoMos.nomFicErMapperAngle);
    if ~flag
        return
    end
else
    Z_Moza_All = [];
    A_Moza_All = [];
end

%% Constitution de la mosaique / MNT

% GeometryType = cl_image.indGeometryType('PingBeam');
% Carto = cl_carto('Ellipsoide.Type', 11);

% profile on
% tic
if InfoMos.TypeMos == 1
    str1 = 'R�alisation de la mosa�que d''�cho-int�gration';
    str2 = 'Processing Echo Integration Mosaic';
else
    str1 = 'Calcul des mosa�ques individuelles d''�cho-int�gration';
    str2 = 'Computing individual EchoIntegration mosaics';
end
msg = Lang(str1,str2);
if ~isempty(Info)
    msg = sprintf('%s\n%s', Info, msg);
end
hw = create_waitbar(msg, 'N', nbProfils);
for iProfil=1:nbProfils
    my_waitbar(iProfil, nbProfils, hw);
    
    fprintf('\n--------------------------------------------------------------------------------------\n')
    fprintf('Processing file %d/%d : %s\n', iProfil, nbProfils, FileList(iProfil).nomFicReflec);
    
    %% Lecture rapide des signaux
    
    [flag, Mode, Swath, FM] = XMLBinUtils.getSignals(FileList(iProfil).nomFicReflec, 'PortMode_1', 'NbSwaths', 'PresenceFM', 'Memmapfile', 1);
    if ~flag
        continue
    end
    
    %% Lecture de l'image de r�flectivit�
    
    %     [flag, Reflec] = cl_image.import_xml(FileList(iProfil).nomFicReflec, 'memmapfile', 1);
    %     if ~flag
    %         messageErreurFichier(FileList(iProfil).nomFicReflec, 'ReadFailure');
    %         continue
    %     end
    %     nbPings = Reflec.nbRows;
    nbPings = length(Mode);
    
    %% Identification des pings correspondant au mode demand�
    
    %     Mode = get(Reflec, 'SonarPortMode_1');
    %     if isempty(listeMode)
    if length(listeMode) <= 1
        subMode = true(nbPings,1);
    else
        subMode = false(nbPings,1);
        for k=1:length(listeMode)
            subMode = (Mode(:) == listeMode(k)) | subMode;
        end
    end
    
    %     Swath = get(Reflec, 'SonarNbSwaths');
    if length(listeSwath) <= 1
        subSwath = true(nbPings,1);
    else
        subSwath = false(nbPings,1);
        for k=1:length(listeSwath)
            subSwath = (Swath(:) == listeSwath(k)) | subSwath;
        end
    end
    
    %     FM = get(Reflec, 'PresenceFM');
    if length(listeFM) <= 1
        subFM = true(nbPings,1);
    else
        subFM = false(nbPings,1);
        for k=1:length(listeFM)
            subFM = (FM(:) == listeFM(k)) | subFM;
        end
    end
    
    suby = find(subMode & subSwath & subFM);
    switch length(suby)
        case 0
            continue
        case 1
            str1 = sprintf('Il n''y a qu''un seul ping �ligible dans "%s". Il n''est pas int�gr� � la mosa�que.', FileList(iProfil).nomFicReflec);
            str2 = sprintf('There is only one selected ping in "%s". It is not integrated to the mosaic.', FileList(iProfil).nomFicReflec);
            my_warndlg(Lang(str1,str2), 0, 'Tag', 'PasAssezDePingsIci');
            continue
    end
    
    %% Lecture du layer "Reflec"
    
    [flag, Reflec] = cl_image.import_xml(FileList(iProfil).nomFicReflec, 'memmapfile', 1);
    if ~flag
        messageErreurFichier(FileList(iProfil).nomFicReflec, 'ReadFailure');
        continue
    end
    
    %% Lecture du layer "AlongDist"
    
    [flag, AlongDist] = cl_image.import_xml(FileList(iProfil).nomFicAlongDist, 'memmapfile', 1);
    if ~flag
        messageErreurFichier(FileList(iProfil).nomFicAlongDist, 'ReadFailure');
        continue
    end
    
    %% Lecture de l'image des angles
    
    [flag, TxAngle] = cl_image.import_xml(FileList(iProfil).nomFicTxAngle, 'memmapfile', 1);
    if ~flag
        messageErreurFichier(FileList(iProfil).nomFicTxAngle, 'ReadFailure');
        continue
    end
    
    %% Extraction des pings pour le mode demand�
    
    if ~isequal(sort(suby(:)), (1:nbPings)')
        y = get(Reflec, 'y');
        [Reflec, flag] = extraction(Reflec, 'y', y(suby));
        if ~flag
            str1 = sprintf('Erreur d''extraction dans le fichier "%s"', FileList(iProfil).nomFicReflec);
            str2 = sprintf('Extraction failure in "%s"', FileList(iProfil).nomFicReflec);
            my_warndlg(Lang(str1,str2), 1);
            continue
        end
        [AlongDist, ~] = extraction(AlongDist, 'suby', suby);
        [TxAngle, ~]   = extraction(TxAngle, 'suby', suby);
    end
    
    %% Masquage des angles
    
    if ~isempty(LimitAngles)
        layerMasque = ROI_auto(TxAngle, 1, 'MaskReflec', TxAngle, [], LimitAngles, 1);
        TxAngle = masquage(TxAngle, 'LayerMask', layerMasque, 'valMask', 1);
        Reflec  = masquage(Reflec, 'LayerMask', layerMasque, 'valMask', 1);
    end
    
    %% Calcul des coordonn�es g�ographiques
    
    [flag, a] = sonarCalculCoordGeo([Reflec AlongDist TxAngle], 1, 'NoWaitbar', 1);
    if ~flag
        return
    end
    Lat = a(1);
    Lon = a(2);
    
    %% Compensation de l'image
    
    if ~isempty(CorFile)
        c(1) = TxAngle;
        
        [nomDir, nomFic] = fileparts(FileList(iProfil).nomFicReflec);
        
        [flag, nomLayersSupplementaires, indexLayersCompensation] = getListeImagesSupplementaires(CorFile, nomDir, nomFic);
        if ~flag
            return
        end
        
        for k=1:length(nomLayersSupplementaires)
            [flag, Layer] = cl_image.import_xml(nomLayersSupplementaires{k}, 'memmapfile', 1);
            if ~flag
                return
            end
            c(end+1) = Layer; %#ok<AGROW>
        end
        
        
        for k=1:length(CorFile)
            CourbeConditionnelle = load_courbesStats(CorFile{k});
            bilan = CourbeConditionnelle.bilan;
            [Reflec, flag] = compensationCourbesStats(Reflec, c(indexLayersCompensation), bilan, 'ModeDependant', ModeDependant);
            if ~flag
                return
            end
        end
        clear c
    end
    
    %% Mosaiquage de l'image de r�flectivit�
    
    [flag, R] = sonar_mosaique(Reflec, Lat, Lon, Resol, 'NoWaitbar', 1);
    if ~flag
        continue
    end
    R.InitialImageName = [R.InitialImageName ' - ' num2str(rand(1))];
    
    %% Mosaiquage de l'image des angles
    
    [flag, A] = sonar_mosaique(TxAngle, Lat, Lon, Resol, 'NoWaitbar', 1);
    if ~flag
        continue
    end
    A.InitialImageName = [A.InitialImageName ' - ' num2str(rand(1))];
    
    
    %% Interpolation
    
    if ~isequal(Window, [1 1])
        R = WinFillNaN(R, 'window', Window, 'NoStats');
        A = WinFillNaN(A, 'window', Window, 'NoStats');
    end
    
    if InfoMos.TypeMos == 2 % Mosaique individuelles
        nomFicSeul = extract_ImageName(R);
        nomFicSeul = strrep(nomFicSeul, '_WinFillNaN', '');
    
        nomFicErs = fullfile(InfoMos.DirName, [nomFicSeul '_Reflectivity_LatLong.ers']);
        flag = export_ermapper(R, nomFicErs);
        if ~flag
            return
        end
    
        nomFicGeotif = fullfile(InfoMos.DirName, [nomFicSeul '_Reflectivity_LatLong.tif']);
        flag = export_geoTiff32Bits(R, nomFicGeotif, 'Mute', 1);
        if ~flag
            return
        end

        nomFicErs = fullfile(InfoMos.DirName, [nomFicSeul '_RxBeamAngle_LatLong.ers']);
        flag = export_ermapper(A, nomFicErs);
        if ~flag
            return
        end
    
% % En attente si demande de Carla
%         nomFicGeotif = fullfile(InfoMos.DirName, [nomFicSeul '_RxBeamAngle_LatLong.tif']);
%         flag = export_geoTiff32Bits(A, nomFicGeotif);
%         if ~flag
%             return
%         end
        
    else % Mosaique classique
        
        [Z_Moza_All, A_Moza_All] = mosaique([Z_Moza_All R], 'Angle', [A_Moza_All A], 'FistImageIsCollector', 1);
        Z_Moza_All = set(Z_Moza_All, 'Name', TitreDepth, 'TagSynchroX', 'Longitude', 'TagSynchroY', 'Latitude', 'YDir', 1);
        A_Moza_All = set(A_Moza_All, 'Name', TitreAngle, 'TagSynchroX', 'Longitude', 'TagSynchroY', 'Latitude', 'YDir', 1);
        clear Mosa
        Mosa(1) = Z_Moza_All;
        Mosa(2) = A_Moza_All;
        
        %% Sauvegarde des mosaiques dans des fichiers ErMapper
        
        if (mod(iProfil,Backup) == 0) || (iProfil == nbProfils)
            Z_Moza_All = majCoordonnees(Z_Moza_All);
            A_Moza_All = majCoordonnees(A_Moza_All);
            if ~isempty(InfoMos.nomFicErMapperLayer)
                export_ermapper(Z_Moza_All, InfoMos.nomFicErMapperLayer);
            end
            if ~isempty(InfoMos.nomFicErMapperAngle)
                export_ermapper(A_Moza_All, InfoMos.nomFicErMapperAngle);
            end
        end
    end
    
    %% Suppression des fichiers memmapfile
    
    deleteFileOnListe(cl_memmapfile.empty, gcbf)
    
    %% Garbage Collector de Java
    
    java.lang.System.gc
end
my_close(hw, 'MsgEnd', 'TimeDelay', 60);


function [flag, nomLayersSupplementaires, indexLayersCompensation] = getListeImagesSupplementaires(CorFile, nomDir, nomFic)

nomLayersSupplementaires = [];
indexLayersCompensation = [];
for k1=1:1 % length(CorFile)% TODO : corriger la stupidit� de "indexLayersCompensation(end+1) = length(indexLayersCompensation) + 1" si plusieurs fichiers de correction 
    CourbeConditionnelle = load_courbesStats(CorFile{k1});
    bilan = CourbeConditionnelle.bilan;
    
    DataTypeConditions = bilan{1}(1).DataTypeConditions;
    for k2=1:length(DataTypeConditions)
        if strcmp(DataTypeConditions{k2}, 'X') || strcmp(DataTypeConditions{k2}, 'Y')
        else
            switch DataTypeConditions{k2}
                case 'TxAngle'
                    indexLayersCompensation(end+1) = 1; %#ok<AGROW> % C{1}
                case 'RxBeamAngle'
                    indexLayersCompensation(end+1) = 1; %#ok<AGROW> % C{1}
                case 'TxBeamIndex'
                    nomLayersSupplementaires{end+1} = fullfile(nomDir, [strrep(nomFic, '_Reflectivity', '_TxBeamIndex') '.xml']); %#ok<AGROW>
                    indexLayersCompensation(end+1) = length(indexLayersCompensation) + 1; %#ok<AGROW>
                case 'TxBeamIndexSwath'
                    nomLayersSupplementaires{end+1} = fullfile(nomDir, [strrep(nomFic, '_Reflectivity', '_TxBeamIndexSwath') '.xml']); %#ok<AGROW>
                    indexLayersCompensation(end+1) = length(indexLayersCompensation) + 1; %#ok<AGROW>
                case 'DetectionType'
                    nomLayersSupplementaires{end+1} = fullfile(nomDir, [strrep(nomFic, '_Reflectivity', '_TxBeamIndexSwath') '.xml']); %#ok<AGROW>
                    indexLayersCompensation(end+1) = length(indexLayersCompensation) + 1; %#ok<AGROW>
                otherwise
                    str = sprintf('"%s" not taken into account in PingAcrossDist2LatLong. Contact sonarscope@ifremer.fr', DataTypeConditions{k2});
                    my_warndlg(str, 1);
                    flag = 0;
                    return
            end
        end
    end
end
nomLayersSupplementaires = unique(nomLayersSupplementaires);
indexLayersCompensation  = unique(indexLayersCompensation);
flag = 1;
