function RTK_PlotGeoid(ListFicGeoid)

if ~iscell(ListFicGeoid)
    ListFicGeoid = {ListFicGeoid};
end

%% Nettoyage des donn�es d'Altitude RTK dans les fichiers .all

nbFic = length(ListFicGeoid);
str1 = 'Visualisation des donn�es .geoig';
str2 = 'Check .geoid files';
hw = create_waitbar(Lang(str1,str2), 'N', nbFic);
for k=1:nbFic
    my_waitbar(k, nbFic, hw);
    
    [flag, lonGeoid, latGeoid, fGeoid, gGeoid] = geoidmodel_read(ListFicGeoid{k});
    if flag
        [~, nomFic, Ext] = fileparts(ListFicGeoid{k});
        str = sprintf('%s', [nomFic Ext]);
        figure('Name', str);
        
        h(1) = subplot(1,2,1);
        scatter(h(1), lonGeoid, latGeoid, [], fGeoid,'filled'); grid on; colormap(jet(256)); colorbar; axis tight;
        title('fGeoid');
        
        h(2) = subplot(1,2,2);
        scatter(h(2), lonGeoid, latGeoid, [], gGeoid,'filled'); grid on; colormap(jet(256)); colorbar; axis tight;
        title('gGeoid');
        linkaxes(h)
    end

end
my_close(hw, 'MsgEnd')
