function [flag, X] = Read_BinTable(Datagrams, nomDirSignal, Name, Type, nbSamples)

identTable = findIndVariable(Datagrams.Tables, Name);
if isempty(identTable)
    flag = 0;
    X = [];
    return
end

if isVariableConstante(Datagrams.Tables(identTable))
    %     X = repmat(Datagrams.Variables(identVariable).Constant, Datagrams.NbDatagrams, 1);
    
    % GLU GLU %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if isempty(Datagrams.Tables(identTable).Constant)
        Datagrams.Tables(identTable).Constant = 1;
    end
    % GLU GLU %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    X = repmat(Datagrams.Tables(identTable).Constant, nbSamples, 1); % A v�rifier
    flag = 1;
else
    [flag, X] = readSignal(nomDirSignal, Datagrams.Tables(identTable), nbSamples);
    if ~flag
        return
    end
end
% Eludation du cas du champ DeviceInfo pour le cas du S7K, paquet 7001.
if ~strcmp(Type, 'char')
    X = code2val(X, Datagrams.Tables(identTable), Type);
end
