function [flag, X] = Read_BinVariable(Datagrams, nomDirSignal, Name, Type)

identVariable = findIndVariable(Datagrams.Variables, Name);
if isempty(identVariable)
    X = [];
    flag = 0;
    return
end
if isVariableConstante(Datagrams.Variables(identVariable))
    X = Datagrams.Variables(identVariable).Constant;
    if strcmp(Datagrams.Variables(identVariable).Storage, 'char') && isnumeric(X)
        X = num2str(X);
    end
    X = repmat(X, Datagrams.NbDatagrams, 1);
else
    [flag, X] = readSignal(nomDirSignal, Datagrams.Variables(identVariable), Datagrams.NbDatagrams);
    if ~flag
        return
    end
end
X = code2val(X, Datagrams.Variables(identVariable), Type);
flag = 1;
