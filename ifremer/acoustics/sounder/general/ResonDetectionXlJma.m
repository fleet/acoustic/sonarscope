function [AmPingRange, AmpPingQF, AmpPingSamplesNb] = ResonDetectionXlJma(Amp, R0, BeamAngles, ...
    SystemSerialNumber, SampleRate, TxPulseWidth, DisplayLevel, iPing)

nbBeams   = size(Amp,2);
nbSamples = size(Amp,1);

AmPingRange      = NaN(1,nbBeams);
AmpPingQF        = NaN(1,nbBeams);
AmpPingSamplesNb = NaN(1,nbBeams);

for iBeam=1:nbBeams
    subSample = find(~isnan(Amp(:,iBeam)));
%     ph = Ph(subSample,iBeam);
    amp = Amp(subSample,iBeam);
%     amp = amp / max(amp);
    
    [AmPingRange(iBeam), AmpPingQF(iBeam), AmpPingSamplesNb(iBeam)] = ...
        detectionXlJma(subSample, amp, R0, ...
        iPing, iBeam, BeamAngles(iBeam), DisplayLevel, nbSamples);
end

% Compensation of the PulseLength if Amplitude detection
AmPingRange = AmPingRange - ((TxPulseWidth / 2) * SampleRate); 

if SystemSerialNumber == 7111
    AmPingRange = AmPingRange + ResonCorrectionAntenna7111(BeamAngles, SampleRate);
end



%% D�tection sur le barycentre

function [AmpBeamRange, AmpBeamQF, AmpBeamSamplesNb] = detectionXlJma(x, Amp, R0, iPing, iBeam, ...
    BeamAngles, DisplayLevel, nbSamples)

subSample = find(~isnan(Amp));
if isempty(subSample)
    AmpBeamRange     = NaN;
    AmpBeamQF        = NaN;
    AmpBeamSamplesNb = NaN;
    return
end

x   = x(subSample);
Amp = Amp(subSample);

% -------------------------
% Suppression des multiples

% subMultiple = ((x > (1.98*R0)) & (x < (2.02*R0)));
% x(subMultiple) = [];
% Amp(subMultiple) = [];

% --------------------
% Calcul du barycentre

[AmpBeamRange, Sigma] = calcul_barycentre(x, Amp.^2);
if Sigma == -1
    AmpBeamQF = 0;
    AmpBeamSamplesNb = 1;
    return
end

% ------------------------------------------------
% Restriction du domaine et recalcul du barycentre

sub = find((x >= (AmpBeamRange-2*Sigma)) & (x <= (AmpBeamRange+2*Sigma)));
if isempty(sub)
    % figure; plot(x, Amp, '-*'); grid on
    AmpBeamQF = 0;
    AmpBeamSamplesNb = 1;
    return
end

AmpBeamSamplesNb = length(sub);

[AmpBeamRange, Sigma] = calcul_barycentre(x(sub), Amp(sub).^2);
if Sigma == -1
    AmpBeamQF = 0;
    AmpBeamSamplesNb = 1;
    return
elseif Sigma == 0
    AmpBeamQF = 0;
    AmpBeamSamplesNb = 1;
    return
end

AmpBeamQF = AmpBeamRange ./ (2 * Sigma);
AmpBeamQF = AmpBeamQF / 40;
AmpBeamQF = log10(AmpBeamQF);

% Correctif � confirmer (identifi� sur fichier
% F:\Feb2008\7111\Spikes\20080302_195235.s7k)
AmpBeamRange = AmpBeamRange + 0.5;

if DisplayLevel == 3
    figure(3536);
    hAxe = subplot(1,1,1);
    plot(x, Amp); grid on; hold on;
    title(sprintf('iPing=%d   iBeam=%d   Angle=%f  AmpBeamRange=%f   Sigma=%f', iPing, iBeam, BeamAngles, AmpBeamRange, Sigma))
    h = plot([AmpBeamRange AmpBeamRange], [0 max(Amp)], 'r');
    set(h, 'LineWidth', 2);
    h = plot([AmpBeamRange-2*Sigma AmpBeamRange+2*Sigma], [0 0], 'r');
    set(h, 'LineWidth', 4); hold off;
    hold off;
    set(hAxe, 'XLim', [1 nbSamples])
    drawnow
end

% ---------------------------------------------------------------
% Restriction du domaine et calcul du deuxi�me facteur de qualit�


% sub = find((x >= (AmpBeamRange-3*Sigma)) & (x <= (AmpBeamRange+3*Sigma)));
% if length(sub) < 7
%     sub = 1:length(x);
% end
% 
% y  = cumsum(Amp(sub));
% y  = y / y(end);
% x1  = x(1);
% x2 =  x(1);
% for i=1:length(y)
%     if y(i) < 0.25
%         x1 = x(i);
%     end
%     if y(i) < 0.75
%         x2 = x(i);
%     else
%         break
%     end
% end
% if x2 == x1
%     AmpBeamQF2 = log10(100);
% else
%     AmpBeamQF2 = .5 * length(y) / (x2 - x1);
%     
%     AmpBeamQF2 = log10(AmpBeamQF2);
% end
% 
% if AmpBeamQF2 < log10(1.2)
%     AmpBeamRange = NaN;
% end
