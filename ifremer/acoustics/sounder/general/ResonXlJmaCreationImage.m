function A = ResonXlJmaCreationImage(A, Phase)

A = A / mean(A(:), 'omitnan');
A = A .^2;
Phase = abs(Phase);
Phase(Phase == 0) = 1e-6;
A = A ./ Phase;
