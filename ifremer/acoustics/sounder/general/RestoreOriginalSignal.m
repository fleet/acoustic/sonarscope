function flag = RestoreOriginalSignal(nomDirRacine, InfoSignal, grpName)

ncFileName = [nomDirRacine '.nc'];
flag = existCacheNetcdf(ncFileName);
if flag
    flag = NetcdfUtils.RestoreOriginalSignal(ncFileName, InfoSignal, grpName);
else
%     InfoSignal.FileName = fullfile(grpName, [InfoSignal.Name '.bin']); % Pour debug quand la lecture a �t� faite en Netcdf
    flag = XMLBinUtils.RestoreOriginalSignal(nomDirRacine, InfoSignal);
end
