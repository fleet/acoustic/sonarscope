% % ------------------ ALL Format V2 ------------------
% dataFileName = getNomFicDatabase('0011_20110307_211804_IKATERE_02.all');
% aV2a = cl_simrad_all('nomFic', dataFileName);
%
% [flag, DataRuntime]  = SSc_ReadDatagrams(dataFileName, 'Ssc_Runtime')
% [flag, DataSeabed]   = SSc_ReadDatagrams(dataFileName, 'Ssc_SeabedImage59h',    'Memmapfile', -1)
% [flag, DataRawRange] = SSc_ReadDatagrams(dataFileName, 'Ssc_RawRangeBeamAngle', 'Memmapfile', -1)
% [flag, DataSurfaceC] = SSc_ReadDatagrams(dataFileName, 'Ssc_SurfaceSoundSpeed', 'Memmapfile', -1)
% [flag, DataClock]    = SSc_ReadDatagrams(dataFileName, 'Ssc_Clock')
% [flag, DataSSP]      = SSc_ReadDatagrams(dataFileName, 'Ssc_SoundSpeedProfile')
%
% % Lectures pas encore rempla�ables
% [flag, DataAttitude]   = read_attitude_bin(aV2a, 'Memmapfile', -1)
% [flag, DataDepth]      = read_depth_bin(aV2a, 'Memmapfile', -1)
% [flag, DataPosition]   = read_position_bin(aV2a, 'Memmapfile', -1)
% [flag, DataIP, isDual] = read_installationParameters_bin(aV2a)
%
% % ------------------ ALL Donn�e avec QFIfremer ------------------
% [flag, dataFileName] = FtpUtils.getSScDemoFile('0000_20130802_152942_Europe.all', 'OutputDir', 'D:\Temp\ExData');
% aV2b = cl_simrad_all('nomFic', dataFileName);
%
% [flag, DataQualityFactor] = SSc_ReadDatagrams(dataFileName, 'Ssc_QualityFactorFromIfr', 'Memmapfile', -1)
%
% % ------------------ ALL Format V1-F ------------------
% dataFileName = getNomFicDatabase('0034_20070406_081642_raw.all');
% aV1F = cl_simrad_all('nomFic', dataFileName);
%
% [flag, DataSeabed]   = SSc_ReadDatagrams(dataFileName, 'Ssc_SeabedImage53h',    'Memmapfile', -1)
% [flag, DataRawRange] = SSc_ReadDatagrams(dataFileName, 'Ssc_RawRangeBeamAngle', 'Memmapfile', -1)
%
% % ------------------ ALL Format V1-f ------------------
% [flag, dataFileName] = FtpUtils.getSScDemoFile('0000_20150309_200643_Belgica.all', 'OutputDir', 'D:\Temp\ExData');
% aV1f = cl_simrad_all('nomFic', dataFileName);
%
% [flag, Data] = SSc_ReadDatagrams(dataFileName, 'Ssc_Heading')
%
% % ------------------ ALL Donn�es avec datagramme height ------------------
% [flag, dataFileName] = FtpUtils.getSScDemoFile('0013_20191022_173330_SimonStevin.all', 'OutputDir', 'D:\Temp\ExData');
% aV2c = cl_simrad_all('nomFic', dataFileName);
%
% [flag, DataHeight] = SSc_ReadDatagrams(dataFileName, 'Ssc_Height')
%
% % ------------------ ALL Lectures pas encore rempla�ables ------------------
% [flag, DataWC1] = read_WaterColumn_bin(aV2c, 'Memmapfile', -1)
%
% % ------------------ ALL Donn�es avec datagrammes Stave ------------------
% [flag, dataFileName] = FtpUtils.getSScDemoFile('0059_20130203_134006_Thalia.all', 'OutputDir', 'D:\Temp\ExData'); 
% aKM = cl_simrad_all('nomFic', dataFileName);
%
% [flag, Data] = SSc_ReadDatagrams(dataFileName, 'Ssc_StaveData', 'Memmapfile', -1)
%
% % ------------------ ALL Donn�es avec datagrammes Ssc_Height ------------------
% [flag, dataFileName] = FtpUtils.getSScDemoFile('0056_20150615_234546_ATL_EM710.all', 'OutputDir', 'D:\Temp\ExData'); 
% aKM = cl_simrad_all('nomFic', dataFileName);
%
% [flag, DataHeight] = SSc_ReadDatagrams(dataFileName, 'Ssc_Height')
%
% % ------------------ ALL Donn�es avec datagrammes Mag&Phase ------------------
% [flag, dataFileName] = FtpUtils.getSScDemoFile('0056_20150615_234546_ATL_EM710.wcd', 'OutputDir', 'D:\Temp\ExData'); 
% [flag, dataFileName] = FtpUtils.getSScDemoFile('0056_20150615_234546_ATL_EM710.all', 'OutputDir', 'D:\Temp\ExData'); 
% aKM = cl_simrad_all('nomFic', dataFileName);
%
% [flag, Data2] = SSc_ReadDatagrams(dataFileName, 'Ssc_RawBeamSamples', 'Memmapfile', -1)
%
% % ------------------ RDF Donn�es Geoswath ------------------ 
% [flag, dataFileName] = FtpUtils.getSScDemoFile('ESSTECH19020.rdf', 'OutputDir', 'D:\Temp\ExData'); 
% a = cl_rdf('nomFic', dataFileName);
%
% [flag, Data] = SSc_ReadDatagrams(dataFileName, 'Ssc_Navigation')
% [flag, Data] = SSc_ReadDatagrams(dataFileName, 'Ssc_Heading')
% [flag, Data] = SSc_ReadDatagrams(dataFileName, 'Ssc_Attitude')
% [flag, Data] = SSc_ReadDatagrams(dataFileName, 'Ssc_Aux1String')
% [flag, Data] = SSc_ReadDatagrams(dataFileName, 'Ssc_FileIndex')
% [flag, Data] = SSc_ReadDatagrams(dataFileName, 'Ssc_Header')
% [flag, Data] = SSc_ReadDatagrams(dataFileName, 'Ssc_HeadingString')
% [flag, Data] = SSc_ReadDatagrams(dataFileName, 'Ssc_MiniSVSString')
% [flag, Data] = SSc_ReadDatagrams(dataFileName, 'Ssc_MiniSVS')
% [flag, Data] = SSc_ReadDatagrams(dataFileName, 'Ssc_NavigationString')
% [flag, Data] = SSc_ReadDatagrams(dataFileName, 'Ssc_Height')
% [flag, Data] = SSc_ReadDatagrams(dataFileName, 'Ssc_Aux2String')
% [flag, Data] = SSc_ReadDatagrams(dataFileName, 'Ssc_FileHeader', 'XMLOnly', 1)
% [flag, Data] = SSc_ReadDatagrams(dataFileName, 'Ssc_Sample', 'Memmapfile', -1)
%
% [flag, Data] = SSc_ReadDatagrams(dataFileName, 'Ssc_EchoSounderString') % Non test�
% [flag, Data] = SSc_ReadDatagrams(dataFileName, 'Ssc_EchoSounder')       % Non test�
%
% [flag, Data, Unit] = SScCacheXMLBinUtils.getSignalUnit(dataFileName, 'Ssc_Sample', 'Mask');
%
% % ------------------ IM Donn�es SAR ------------------ 
% [flag, dataFileName] = FtpUtils.getSScDemoFile('0014.04.im', 'OutputDir', 'D:\Temp\ExData');
% flag = Sar2ssc(dataFileName);
%
% [flag, Data] = SSc_ReadDatagrams(dataFileName, 'Ssc_Sar', 'Memmapfile', -1)
%
% % ------------------ SDF Donn�es Klein ------------------ 
% [flag, dataFileName] = FtpUtils.getSScDemoFile('Syp_001_180507104600.sdf', 'OutputDir', 'D:\Temp\ExData')
% a = cl_sdf('nomFic', dataFileName);
%
% [flag, Data] = SSc_ReadDatagrams(dataFileName, 'Ssc_V3001', 'Memmapfile', -1)
% [flag, Data] = SSc_ReadDatagrams(dataFileName, 'Ssc_V3001', 'XMLOnly', 1)
% listeSignals = get_listeSignals(a)
%
% % ------------------ RAW Donn�es EK60 ------------------ 
% [flag, dataFileName] = FtpUtils.getSScDemoFile('tan1505-D20150428-T013331.raw', 'OutputDir', 'D:\Temp\ExData')
% a = cl_ExRaw('nomFic', dataFileName);
%
% [flag, Data] = SSc_ReadDatagrams(dataFileName, 'Ssc_Sample', 'Memmapfile', -1);
% [flag, Data] = SSc_ReadDatagrams(dataFileName, 'Ssc_Height')
% [flag, Data] = SSc_ReadDatagrams(dataFileName, 'Ssc_Position')
%
% [flag, f, Unit] = SScCacheXMLBinUtils.getSignalUnit(dataFileName, 'Ssc_Sample',   'AcousticFrequency')
% [flag, t, Unit] = SScCacheXMLBinUtils.getSignalUnit(dataFileName, 'Ssc_Position', 'Time')
%
% [flag, Signal] = SScCacheXMLBinUtils.getTimeSignalUnit(dataFileName, 'Ssc_Position', 'Heading')
% [flag, Signal] = SScCacheXMLBinUtils.getTimeSignalUnit(dataFileName, 'Ssc_Position', 'Height');
%
% % ------------------ RAW Donn�es EK80 ------------------ 
% [flag, dataFileName] = FtpUtils.getSScDemoFile('D20190625-T100713.raw', 'OutputDir', 'D:\Temp\ExData')
% a = cl_ExRaw('nomFic', dataFileName);
%
% [flag, Data] = SSc_ReadDatagrams(dataFileName, 'Ssc_Sample', 'Memmapfile', -1)
% [flag, Data] = SSc_ReadDatagrams(dataFileName, 'Ssc_Height')
% [flag, Data] = SSc_ReadDatagrams(dataFileName, 'Ssc_Position')
%
% [flag, f, Unit] = SScCacheXMLBinUtils.getSignalUnit(dataFileName, 'Ssc_Sample',   'AcousticFrequency')
% [flag, t, Unit] = SScCacheXMLBinUtils.getSignalUnit(dataFileName, 'Ssc_Position', 'Time')
%
% [flag, Signal] = SScCacheXMLBinUtils.getTimeSignalUnit(dataFileName, 'Ssc_Position', 'Heading')
% [flag, Signal] = SScCacheXMLBinUtils.getTimeSignalUnit(dataFileName, 'Ssc_Position', 'Height')
%
% % ------------------ XTF Donn�es Klein ------------------ 
% [flag, dataFileName] = FtpUtils.getSScDemoFile('Syp_001_180507104600.xtf', 'OutputDir', 'D:\Temp\ExData')
% a = cl_xtf('nomFic', dataFileName);
%
% [flag, Data] = SSc_ReadDatagrams(dataFileName, 'Ssc_FileHeader', 'XMLOnly', 1)
% [flag, Data] = SSc_ReadDatagrams(dataFileName, 'Ssc_SideScan',   'Memmapfile', -1)
% [flag, Data] = SSc_ReadDatagrams(dataFileName, 'Ssc_Bathy',      'Memmapfile', -1); % Non test�
% [flag, Data] = SSc_ReadDatagrams(dataFileName, 'Ssc_Bathymetry', 'Memmapfile', -1) % Pas test�
% [flag, Data] = SSc_ReadDatagrams(dataFileName, 'Ssc_QPSMultiBeamHeader_0', 'Memmapfile', -1); % Non test�
%
% %{
% % ------------------ Test fichiers merdiques ------------------ 
% dataFileName = 'Z:\private\ifremer\Herve\0006_20200206_203241_Thalassa.all';
% [flag, DataRuntime]  = SSc_ReadDatagrams(dataFileName, 'Ssc_Runtime')
% [flag, DataSeabed]   = SSc_ReadDatagrams(dataFileName, 'Ssc_SeabedImage59h',    'Memmapfile', -1)
% [flag, DataRawRange] = SSc_ReadDatagrams(dataFileName, 'Ssc_RawRangeBeamAngle', 'Memmapfile', -1)
% [flag, DataSurfaceC] = SSc_ReadDatagrams(dataFileName, 'Ssc_SurfaceSoundSpeed', 'Memmapfile', -1)
% [flag, DataClock]    = SSc_ReadDatagrams(dataFileName, 'Ssc_Clock')
% [flag, DataSSP]      = SSc_ReadDatagrams(dataFileName, 'Ssc_SoundSpeedProfile')
% %}
%
% % Lectures pas encore rempla�ables
% [flag, DataAttitude]   = read_attitude_bin(aV2a, 'Memmapfile', -1)
% [flag, DataDepth]      = read_depth_bin(aV2a, 'Memmapfile', -1)
% [flag, DataPosition]   = read_position_bin(aV2a, 'Memmapfile', -1)
% [flag, DataIP, isDual] = read_installationParameters_bin(aV2a)
%
% % ------------------ Donn�es ADCP format ODV ------------------ 
% [flag, dataFileName] = FtpUtils.getSScDemoFile('ADCP_38khz_Malaga_Brest__006_allPofil.odv', 'OutputDir', 'D:\Temp\ExData')
%
% [flag, Data] = SSc_ReadDatagrams(dataFileName, 'Ssc_Data', 'Memmapfile', -1)
%
% % ------------------ Trac�s graphiques de donn�es ALL ------------------ 
% dataFileName = getNomFicDatabase('0011_20110307_211804_IKATERE_02.all');
%
% ALL_plotRuntime(dataFileName)
% ALL_plotSeabed(dataFileName)
% ALL_plotRawRange(dataFileName)
% ALL_plotDepth(dataFileName)

function [flag, Data, VarNameInFailure, DataBin] = SSc_ReadDatagrams(dataFileName, nomDatagram, varargin)

flag = existCacheNetcdf(dataFileName);
if flag
    [flag, Data, VarNameInFailure, DataBin] = SScCacheNetcdfUtils.readWholeData(dataFileName, nomDatagram, varargin{:});
else
    [flag, Data, DataBin, VarNameInFailure] = SScCacheXMLBinUtils.readWholeData(dataFileName, nomDatagram, varargin{:});
end
% figure; plot(Data.Datetime(:), Data.Roll(:), 'b'); grid on;
