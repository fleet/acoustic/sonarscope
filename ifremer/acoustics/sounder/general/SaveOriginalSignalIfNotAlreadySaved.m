function flag = SaveOriginalSignalIfNotAlreadySaved(nomDirRacine, InfoSignal)

nomFic = fullfile(nomDirRacine, InfoSignal.FileName);
[nomDir, nomFic, ext] = fileparts(nomFic);
nomFic = fullfile(nomDir, [nomFic 'Origin' ext]);
flag = exist(nomFic, 'file');
if flag
    return
end

[flag, X] = readSignal(nomDirRacine, InfoSignal, []);
if ~flag
    return
end

InfoSignal.FileName = strrep(InfoSignal.FileName, ext, ['Origin' ext]);

[~, nomFichierSeul] = fileparts(nomDirRacine);

flag = writeSignal(nomDirRacine, InfoSignal, X);
if flag
    str1 = sprintf('Le signal original "%s" du fichier "%s" est sauv�. Vous pourrez le r�cup�rer � tout moment en restaurant les signaux originaux.', InfoSignal.Name, nomFichierSeul);
    str2 = sprintf('Source signal "%s" from file "%s" is backuped. You can restore it at any time using "Restore Source signals".', InfoSignal.Name, nomFichierSeul);
else
    str1 = sprintf('Le signal original "%s" du fichier "%s" n''a pas pu �tre sauv�.', InfoSignal.Name, nomFichierSeul);
    str2 = sprintf('Source signal "%s" from file "%s" could not be saved.', InfoSignal.Name, nomFichierSeul);
end
my_warndlg(Lang(str1,str2), 0, 'TimeDelay', 30);
