function SonarFmt2ssc_message(nomFicXml, Ext)

[nomDirSignal, nomFicSignal] = fileparts(nomFicXml);
[~,nomFic] = fileparts(nomDirSignal);
nomFic = [nomFic Ext];
str1 = sprintf('Traitement de "%s" pour "%s"', nomFicSignal, nomFic);
str2 = sprintf('Processing "%s" for "%s"', nomFicSignal, nomFic);
WorkInProgress(Lang(str1,str2))
