% Identification du format d'un fichier
%
% Syntax
%   SonarIdent = SonarIdent_caraibes2sonarScope(mbSounder)
%
% Input Arguments
%   mbSounder  : Identification CARAIBES du sonar
%
% Output Arguments
%   SonarIdent : Identification du sonar si connu (Cf. cl_sounder)
%
% Examples
%   SonarIdent = SonarIdent_caraibes2sonarScope(51)
%
% See also Authors
% Authors : JMA
%--------------------------------------------------------------------------

function SonarIdent = SonarIdent_caraibes2sonarScope(mbSounder)

switch mbSounder
    case 0
        SonarIdent = [];
%     case 41 % EM2000 % !!!!!!!!!!! 59 aussi
%         SonarIdent = 21;
    case 51 % EM1000
        SonarIdent = 4;
    case 52 % EM12S
        SonarIdent = 2;
    case 53 % EM12D
        SonarIdent = 1;
    case 54 % EM3000S
        SonarIdent = 7;
    case 55 % EM3000D
        SonarIdent = 6;
    case 56 % EM300
        SonarIdent = 3;
    case 57 % EM3002 D ou S
        SonarIdent = 19; % 20 pour single
        % % % %             case 57 % EM1002
        % % % %                 SonarIdent = 10;
    case 58 % EM120
        SonarIdent = 11;
    case 59 % EM2000
        SonarIdent = 21;
    case 60 % EM3002 equidistant
        SonarIdent = 19; % ou 20
    case 61 % EM3002 High frequency
        SonarIdent = 19; % ou 20
    case 62 % EM3002 Dual
        SonarIdent = 19; % ou 20
    case 63 % EM710
        SonarIdent = 18;
    case 64 % ME70
        SonarIdent = 17;
    case 65 % EM122
        SonarIdent = 23;
    case 66 % EM302
        SonarIdent = 22;
    case 67 %EM2040 singleRX
        SonarIdent = 24;
    case 68 % EM3002
        SonarIdent = 19; % ou 20
    case 69 %EM2040 DualRX
        SonarIdent = 24;
    case 84 % 7111
        SonarIdent = 14;
    case 85 % 7125
        SonarIdent = 15;
    case 86 % 7150
        SonarIdent = 12; % 13 en 24 kHz
        % % % %             case 85 % RESON7125
        % % % %                 SonarIdent = 15;
    case 90 % Geoswath
        SonarIdent = 16;
    case 94 % EM304
        SonarIdent = 32;
    case 95 % EM2040C=EM2045
        SonarIdent = 25;
    case 96 % EM712
        SonarIdent = 33;
    otherwise
        SonarIdent = [];
        disp('A completer ici') % EM12S EM3000D EM3000S EM1002
end

%{
Sonar.Ident               <-> 
'1=EM12D' | '2=EM12S' | '5=SAR' 
| '8=DF1000' | '9=DTS1' | '10=EM1002'
'26=Subbottom' | '27=Klein3000' | '28=EK60' | '29=HAC_Generic' | 
'30=ADCP' | '31=EM2040D' 
%}