function [flagDisqueReseau, nomFic, nomDirReseau] = SonarScopeCacheLocal(nomFic)

global NetUseWords %#ok<GVMIS> 

nomDirReseau = [];

[nomFicOut, NetUseWords] = windows_replaceVolumeLetter(nomFic, 'NetUseWords', NetUseWords);
flagDisqueReseau = ~strcmp(nomFic, nomFicOut);
if flagDisqueReseau
    flag = copyfile(nomFic, my_tempdir);
    if flag == 1
        [nomDirReseau, nomFicSeul, ext] = fileparts(nomFic);
        nomFic = fullfile(my_tempdir, [nomFicSeul ext]);
    end
end
