function flag = SonarScopeCacheRemote(flagDisqueReseau, nomDirReseau, nomFicSeul)

if flagDisqueReseau
    nomDirLocal = fullfile(my_tempdir, 'SonarScope', nomFicSeul);
    nomDirReseau2 = fullfile(nomDirReseau, 'SonarScope');
    if exist(nomDirLocal, 'dir')
        if ~exist(nomDirReseau2, 'dir')
            mkdir(nomDirReseau2)
        end
        nomDirReseau2 = fullfile(nomDirReseau, 'SonarScope', nomFicSeul);
        if ~exist(nomDirReseau2, 'dir')
            mkdir(nomDirReseau2)
        end
        flag = copyfile(nomDirLocal, nomDirReseau2, 'f');
        if ~flag
            str1 = sprintf('Erreur de copie de "%s" vers "%s"', nomDirLocal, nomDirReseau2);
            str2 = sprintf('Copy error of "%s" to "%s"', nomDirLocal, nomDirReseau2);
            my_warndlg(Lang(str1,str2), 0);
        end
        flag = rmdir(nomDirLocal, 's');
        if ~flag
            str1 = sprintf('Erreur lors de la suppression de "%s"', nomDirLocal);
            str2 = sprintf('Could not remove "%s"', nomDirLocal);
            my_warndlg(Lang(str1,str2), 0);
        end
    end
end