% ------------------------
% Sonars simrad uniquement

modesEM12D  = [103 104 114:121];
modesEM12S  = [101 102 108:113];
modesEM300  = 135:140;
modesEM1000 = [105:107 122:134];
modesEM3000 = 141:142;
modesEM1002 = 143:147;
modesEM120  = 148:152;

modes = [modesEM12D modesEM12S modesEM1000 modesEM300 modesEM1002 modesEM3000 modesEM120];
for i=1:length(modes)
    [SonarIdent, SonarMode_1, SonarMode_2, FactoryMode,SonarFamily] = modeCar2Sim(modes(i)) %#ok<*NOPTS>
    x = cl_sounder('Sonar.Ident', SonarIdent, 'Sonar.Mode_1', SonarMode_1, 'Sonar.Mode_2', SonarMode_2);
    disp(' ')
    disp('--------------------------------------------------------------------------------------------')
    Sonar.Name = get(x, 'Sonar.Name');
    strMode_1 = get(x, 'Sonar.strMode_1');
    Mode_1 = get(x, 'Sonar.Mode_1');
    Sonar.Mode_1 = strMode_1{Mode_1};
    strMode_2 = get(x, 'Sonar.strMode_2');
    Mode_2 = get(x, 'Sonar.Mode_2');
    Sonar.Mode_2 = strMode_2{Mode_2};
    str = sprintf('Mode Caraibes=%d : %s %s %s', modes(i), Sonar.Name, Sonar.Mode_1, Sonar.Mode_2);
    disp(str)
    x
    get(x, 'BeamForm.Rx.Angles')
end

% ---------------------------
% Tous les sonars et sondeurs

a = cl_sounder;
for i=1:get_nbSounders(a)
    a = cl_sounder('Sonar.Ident', i);
    for j=1:get_nbModes_1(a)
        a = set(a, 'Sonar.Mode_1', j);
        for k=1:get_nbModes_2(a)
            disp(' ')
            disp('--------------------------------------------------------------------------------------------')
            a = set(a, 'Sonar.Mode_1', j, 'Sonar.Mode_2', k)
        end
    end
end
