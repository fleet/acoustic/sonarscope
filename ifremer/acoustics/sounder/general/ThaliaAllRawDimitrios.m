% nomFicAll = 'Z:\private\ifremer\dimitrios\ROLL\0056_20150519_162601_Thalia.all';
% nomFicRaw = 'Z:\private\ifremer\dimitrios\ROLL\TH-D20150519-T162606.raw';
% 
% [flag, a, Carto, repImport] = import(cl_image, nomFicAll);
% SonarScope(a)
% 
% [flag, r, Carto, repImport] = import(cl_image, nomFicRaw);
% SonarScope(r)
%
% ThaliaAllRawDimitrios(a(1), r(1))

function ThaliaAllRawDimitrios(a, r)

nomFicAll = a.InitialFileName;
nomFicRaw = r.InitialFileName;

T1    = get(a(1), 'Time'); % EM2040
Lat1  = get(a(1), 'FishLatitude');
Lon1  = get(a(1), 'FishLongitude');
Roll1 = get(a(1), 'Roll');

T2    = get(r(1), 'Time'); % EK60
Lat2  = get(r(1), 'FishLatitude');
Lon2  = get(r(1), 'FishLongitude');
% Roll2 = get(r(1), 'Roll');

t1 = T1.timeMat;
t2 = T2.timeMat;

Fig1 = plot_navigation(nomFicAll, [], Lon1, Lat1, t1, []);
plot_navigation(nomFicRaw, Fig1, Lon2, Lat2, t2, []);

% {
clear mPos indPos mTim indTim
for k2=1:length(Lat2)
    difPos = (Lat1-Lat2(k2)).^2 + (Lon1-Lon2(k2)).^2;
    [mPos(k2), indPos(k2)] = min(difPos); %#ok<AGROW>
    difTim = abs(t1-t2(k2));
    [mTim(k2), indTim(k2)] = min(difTim); %#ok<AGROW>
end
% FigUtils.createSScFigure; PlotUtils.createSScPlot(indPos); grid on; title('indPos');
% FigUtils.createSScFigure; PlotUtils.createSScPlot(indTim); grid on; title('indTim');
% FigUtils.createSScFigure; PlotUtils.createSScPlot(mPos); grid on; title('mPos');
% FigUtils.createSScFigure; PlotUtils.createSScPlot(mTim); grid on; title('mTim');
indFirst = find(diff(indTim), 1, 'first');
fprintf('First common ping of EK60 compared to EM2040 based on time : %d\n', indFirst);
indFirst = find(diff(indPos), 1, 'first');
fprintf('First common ping of EK60 compared to EM2040 based on position : %d\n', indFirst);

clear mPos indPos mTim indTim
for k1=1:length(Lat1)
    difPos = (Lat2-Lat1(k1)).^2 + (Lon2-Lon1(k1)).^2;
    [mPos(k1), indPos(k1)] = min(difPos);
    difTim = abs(t2-t1(k1));
    [mTim(k1), indTim(k1)] = min(difTim);
end
% FigUtils.createSScFigure; PlotUtils.createSScPlot(indPos); grid on; title('indPos');
% FigUtils.createSScFigure; PlotUtils.createSScPlot(indTim); grid on; title('indTim');
% FigUtils.createSScFigure; PlotUtils.createSScPlot(mPos); grid on; title('mPos');
% FigUtils.createSScFigure; PlotUtils.createSScPlot(mTim); grid on; title('mTim');
indFirst = find(diff(indTim), 1, 'first');
fprintf('First common ping of EM2040 compared to EK60 based on time : %d\n', indFirst);
indFirst = find(diff(indPos), 1, 'first');
fprintf('First common ping of EM2040 compared to EK60 based on position : %d\n', indFirst);

% }

fprintf('\nEM2040 : %s\n', char(T1))
fprintf('\nEK60   : %s\n\n', char(T2))

OffsetT2 = 0;
Roll2 = interp1(t1, Roll1, t2+OffsetT2);
r(1) = set_SonarRoll(r(1), Roll2);

SonarScope(r)
export_ExRaw(r, 'SignalNames', 'Roll')

