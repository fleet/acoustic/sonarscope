function flag = WC_AlongPath(nomFicXml, listFicCSV, Tag, nomDirOut)

if ischar(nomFicXml)
    nomFicXml = {nomFicXml};
end

if ischar(listFicCSV)
    listFicCSV = {listFicCSV};
end

N = length(nomFicXml);
str1 = 'Cr�ation des coupes de WC le long d''un chemin';
str2 = 'Processing WC images along a track';
hw  = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:length(nomFicXml)
    my_waitbar(k, N, hw)
    flag = WC_SlicesAlongNavigation_unitaire1(nomFicXml{k}, listFicCSV, Tag, nomDirOut);
end
my_close(hw, 'MsgEnd')


function flag = WC_SlicesAlongNavigation_unitaire1(nomFicXml, listFicCSV, Tag, nomDirOut)

for kCSV=1:length(listFicCSV)
    [nomDirXml, nomFicSeul] = fileparts(nomFicXml);
    nomDirXml = fullfile(nomDirXml, [nomFicSeul '.zip']);
    my_unzip(nomDirXml);
    
    [flag, Data3DMatrix] = WC_readImageCube3D(nomFicXml, 'Memmapfile', 1);
    if ~flag
        return
    end
    
    if isempty(Data3DMatrix.CLim)
        if contains(nomFicXml, 'Raw')
            if isfield(Data3DMatrix, 'CLimRaw')
                CLimRaw = Data3DMatrix.CLimRaw;
            elseif isfield(Data3DMatrix, 'CLim')
                CLimRaw = Data3DMatrix.CLim;
            end
            if isempty(CLimRaw)
                CLimRaw = [-64 10];
            end
            Data3DMatrix.CLim = CLimRaw;
        end
        
        if contains(nomFicXml, 'Comp')
            if isfield(Data3DMatrix, 'CLimComp')
                CLimComp = Data3DMatrix.CLimComp;
            elseif isfield(Data3DMatrix, 'CLim')
                CLimComp = Data3DMatrix.CLim;
            end
            if isempty(CLimComp)
                CLimComp = [-10 25];
            end
            Data3DMatrix.CLim = CLimComp;
        end
    end
    
    flag = WC_SlicesAlongNavigation_unitaire2(nomFicXml, kCSV, listFicCSV{kCSV}, nomDirOut, Data3DMatrix, Tag);
end


function flag = WC_SlicesAlongNavigation_unitaire2(nomFicXml, kCSV, listFicCSV, nomDirOut, Data3DMatrix, Tag)

%% Lecture du fichier .csv

[flag, lat, lon] = read_3DV_path(listFicCSV);
if ~flag
    fileID = fopen(listFicCSV);
    C = textscan(fileID, '%f;%f;');
    if iscell(C) && (length(C) == 2)
        lat = C{1};
        lon = C{2};
    else
        return
    end
    fclose(fileID);
end

%% Cr�ation du r�pertoire

if isempty(Tag)
    Tag = sprintf('_WCPath%d', kCSV);
else
    Tag = sprintf('_%s_WCPath%d', Tag, kCSV);
end
[~, nomFic] = fileparts(nomFicXml);
nomFic = strrep(nomFic, '_PolarEchograms', '');
nomFic = strrep(nomFic, '_WCCube', Tag);
nomFic = strrep(nomFic, '_Cube', Tag);
nomDirPath = nomFic;

nomDirXml = fullfile(nomDirOut, nomFic);
nomFicXml = [nomDirXml '.xml'];
if ~exist(nomDirXml, 'dir')
    flag = mkdir(nomDirXml);
    if ~flag
        messageErreurFichier(nomDirXml, 'WriteFailure');
        return
    end
end

%% Interpolation de la navigation du path au pas de grille de la matrice 3D

Fuseau = floor((mean(lon+180, 'omitnan'))/6 + 1);
LatMean = mean(lat, 'omitnan');
if LatMean >= 0
    Hemi = 'N';
else
    Hemi = 'S';
end
Carto = cl_carto('Ellipsoide.Type', 11, 'Projection.Type', 3, ...
    'Projection.UTM.Fuseau', Fuseau, 'Projection.UTM.Hemisphere', Hemi);
[xPath, yPath] = latlon2xy(Carto, lat, lon);
Step =  double(mean(diff(Data3DMatrix.AcrossDistance(:,:))));
di = sqrt((diff(xPath)) .^2 + diff(yPath) .^ 2);
sub = find(di == 0);
di(sub) = [];
xPath(sub) = [];
yPath(sub) = [];

d = [0; cumsum(di(:))];
dNew = d(1):Step:d(end);
xPath3D = interp1(d, xPath, dNew);
yPath3D = interp1(d, yPath, dNew);
[lat3D, lon3D] = xy2latlon(Carto, xPath3D, yPath3D);
%{
FigUtils.createSScFigure; PlotUtils.createSScPlot(lon, lat, '-o'); grid on; axisGeo
FigUtils.createSScFigure; PlotUtils.createSScPlot(xPath, yPath, '-o'); grid on; axis equal;
FigUtils.createSScFigure; PlotUtils.createSScPlot(di, '-o'); grid on
FigUtils.createSScFigure; PlotUtils.createSScPlot(d, '-o'); grid on
FigUtils.createSScFigure; PlotUtils.createSScPlot(xPath3D, yPath3D, '-o'); grid on
FigUtils.createSScFigure; PlotUtils.createSScPlot(lon3D, lat3D, '-o'); grid on; axisGeo
%}

%% Extraction des coordonn�es ligne et colonne correspondant au chemin

LatitudeTop  = Data3DMatrix.LatitudeTop(:,:);
LongitudeTop = Data3DMatrix.LongitudeTop(:,:);
[n1, n2] = size(LatitudeTop);
n = length(lat3D);
sub = false(1,n);
kx = NaN(1,n);
ky = NaN(1,n);
for k=1:n
    Dist = (LatitudeTop - lat3D(k)) .^ 2 + (LongitudeTop - lon3D(k)) .^ 2;
    [~, ind] = min(Dist(:));
    [k1, k2] = ind2sub([n1, n2] , ind);
    if (k1 > 1) && (k1 < n1) && (k2 > 1) && (k2 < n2)
        sub(k) = true;
        kx(k) = k2;
        ky(k) = k1;
    end
end
lat3D = lat3D(sub);
lon3D = lon3D(sub);
kx    = kx(sub);
ky    = ky(sub);
%{
FigUtils.createSScFigure; PlotUtils.createSScPlot(kx); grid on;
FigUtils.createSScFigure; PlotUtils.createSScPlot(ky); grid on;
FigUtils.createSScFigure; PlotUtils.createSScPlot(kx, ky); grid on; axis equal;
pppp = sub2ind([n1, n2], ky, kx);
ppppLat = LatitudeTop(pppp)
ppppLon = LongitudeTop(pppp)
FigUtils.createSScFigure; PlotUtils.createSScPlot(ppppLon, ppppLat); grid on; axisGeo;
figure; plot(LongitudeTop(:,:), LatitudeTop(:,:), '.'); grid on; axisGeo;
hold on;  PlotUtils.createSScPlot(lon3D, lat3D, 'bo');
PlotUtils.createSScPlot(ppppLon, ppppLat, 'r*');
%}

%% 

% binsCentre = 0;
nbPings = length(kx);
if nbPings <= 1
    flag = 0;
    return
end
nbRows = Data3DMatrix.Dimensions.nbRows;

%% Cr�ation des images

map = jet(256);
CLim = Data3DMatrix.CLim;
try
    Reflectivity = Data3DMatrix.Reflectivity(:,:,:);
catch ME %#ok<NASGU>
%     ME
    Reflectivity = Data3DMatrix.Reflectivity;
end

X = NaN(nbPings, nbRows, 'single');
for k=1:nbPings
    X(k,:) = Reflectivity(:,kx(k),ky(k));
end
% SonarScope(X)
% clear Reflectivity

I = gray2ind(mat2gray(X, double(CLim)), size(map,1));
I = flipud(I'); % A confirmer
I(I == 0) = 1;
I(I == 255) = 0;

nomSousRepertoire = sprintf('%03d', 0);
nomDir3 = fullfile(nomDirXml, nomSousRepertoire);
if ~exist(nomDir3, 'dir')
    flag = mkdir(nomDir3);
    if ~flag
        messageErreurFichier(nomDir3, 'WriteFailure');
        return
    end
end

%% Export .png

nomFic1 = sprintf('%05d.png', 1);
nomFic2 = fullfile(nomDir3, nomFic1);
imwrite(I, map, nomFic2, 'Transparency', 0)
%{
figure(7567); imagesc(1:nbPings, Data3DMatrix.Depth, X, Data3DMatrix.CLim);
axis xy; xlabel('Pings'), ylabel('Depth'); colormap(jet(256)); colorbar;
drawnow
%}

%% Export .tif

nomFic1 = sprintf('%05d.tif', 1);
nomFic2 = fullfile(nomDir3, nomFic1);
export_ImageTif32bits(X', nomFic2, 'Mute', 1);

%% Definition of the structure

[Info, Data] = createData(Data3DMatrix, nbPings, lat3D, lon3D, ky, nomDirPath);

%% Cr�ation du fichier XML d�crivant la donn�e

% nomFicXml = fullfile(nomDirRacine, [TypeDataVoxler '.xml']);
xml_write(nomFicXml, Info);
flag = exist(nomFicXml, 'file');
if ~flag
    messageErreur(nomFicXml)
    return
end

%% Cr�ation du r�pertoire Info

nomDirInfo = nomDirXml;
if ~exist(nomDirInfo, 'dir')
    status = mkdir(nomDirInfo);
    if ~status
        messageErreur(nomDirInfo)
        flag = 0;
        return
    end
end

%% Cr�ation des fichiers binaires des signaux

for k=1:length(Info.Signals)
    flag = writeSignal(nomDirOut, Info.Signals(k), Data.(Info.Signals(k).Name));
    if ~flag
        return
    end
end

%% Cr�ation des fichiers binaires des signaux

for k=1:length(Info.Images)
    flag = writeImage(nomDirOut, Info.Images(k), Data.(Info.Images(k).Name));
    if ~flag
        return
    end
end

flag = 1;


function [Info, Data] = createData(Data3DMatrix, nbPings, lat3D, lon3D, ky, nomDirPath)

binsCentre = 0;

%% Definition of the structure

Info.Signature1      = 'SonarScope';
Info.Signature2      = 'ImagesAlongNavigation';
Info.FormatVersion   = 20100624;
Info.Name            = Data3DMatrix.Name;
Info.ExtractedFrom   = Data3DMatrix.ExtractedFrom;

Info.Survey         = Data3DMatrix.Survey;
Info.Vessel         = Data3DMatrix.Vessel;
Info.Sounder        = Data3DMatrix.Sounder;
Info.ChiefScientist = Data3DMatrix.ChiefScientist;

Info.DepthTop       = 0;
Info.DepthBottom    = Data3DMatrix.Depth(1);
Info.MiddleSlice    = 0;

nbRows = Data3DMatrix.Dimensions.nbRows;
Info.Dimensions.nbPings  = nbPings;
Info.Dimensions.nbSlices = 1;
Info.Dimensions.nbRows   = nbRows;

Data.LatitudeNadir   = NaN(1, nbPings);
Data.LongitudeNadir  = NaN(1, nbPings);
Data.LatitudeTop     = NaN(1, nbPings);
Data.LongitudeTop    = NaN(1, nbPings);
% Data.LatitudeBottom  = NaN(1, nbPings);
% Data.LongitudeBottom = NaN(1, nbPings);
Data.PingNumber      = NaN(1, nbPings, 'single');
Data.AcrossDistance  = NaN(1, 1, 'single');
Data.Date            = NaN(1, nbPings, 'single');
Data.Hour            = NaN(1, nbPings, 'single');
% Info.Depth           = NaN(1, nbPings, 'single');

Data.AcrossDistance(:) = binsCentre;
Data.SliceExists       = true;

% TODO : bricolage abominable : il faut repositionner chaque ping en
% fonction de l'immersion en amont
% ImmersionMoyenne = nanmean(Data3DMatrix.Immersion(:));
% Immersion = Data3DMatrix.Immersion(:);

% Data.DepthTop    = ones(1, nbPings, 'single') * max(Data3DMatrix.Depth) - ImmersionMoyenne;
% Data.DepthBottom = ones(1, nbPings, 'single') * min(Data3DMatrix.Depth) - ImmersionMoyenne;

Immersion = Data3DMatrix.Immersion(:)';
% ImmersionMoyenne = nanmean(Data3DMatrix.Immersion(:));
Immersion = my_interp1(1:length(Immersion), Immersion, ky);
Immersion(isnan(Immersion)) = 0;

if isfield(Data3DMatrix, 'Tide') && ~isempty(Data3DMatrix.Tide) && any(~isnan(Data3DMatrix.Tide))
    Tide = Data3DMatrix.Tide(:)';
else
    Tide = zeros(size(Immersion));
end
Tide = my_interp1(1:length(Tide), Tide, ky);

Heave = Data3DMatrix.Heave(:)';
Heave = my_interp1(1:length(Heave), Heave, ky);

Data.DepthTop    = ones(1, nbPings, 'single') * max(Data3DMatrix.Depth) + Immersion;
Data.DepthBottom = ones(1, nbPings, 'single') * min(Data3DMatrix.Depth(:));

% Avant modif JMA du 21/08/2020
% Data.DepthBottom = Data.DepthBottom - Heave + Tide; % Car TranscucerDepth utilis� avant int�gre d�j� le Heave

% Modifi� par JMA le 21/08/2010 pour donn�es VLIZ (Nora4) : test� sur XML-Bin
Data.DepthBottom = Data.DepthBottom - Heave ; % Car TranscucerDepth utilis� avant int�gre d�j� le Heave

for k=1:nbPings
    %     Info.Depth(k,:)            = Data3DMatrix.Depth % TODO
    Data.Date(k)           = Data3DMatrix.Date(ky(k));
    Data.Hour(k)           = Data3DMatrix.Hour(ky(k));
    Data.PingNumber(k)     = Data3DMatrix.iPing(ky(k));
%     Data.LatitudeNadir(k)  = Data3DMatrix.latCentre(ky(k));
%     Data.LongitudeNadir(k) = Data3DMatrix.lonCentre(ky(k));
%     Data.LatitudeNadir(k)  = lat3D(ky(k));
%     Data.LongitudeNadir(k) = lon3D(ky(k));
%     Data.LatitudeTop(:,k)  = lat3D(ky(k));
%     Data.LongitudeTop(:,k) = lon3D(ky(k));
    Data.LatitudeNadir(k)  = lat3D(k);
    Data.LongitudeNadir(k) = lon3D(k);
    Data.LatitudeTop(:,k)  = lat3D(k);
    Data.LongitudeTop(:,k) = lon3D(k);
end

Info.Signals(1).Name          = 'Date';
Info.Signals(end).Dimensions  = '1, nbPings';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'Ifremer';
Info.Signals(end).FileName    = fullfile(nomDirPath,'Date.bin');

Info.Signals(end+1).Name      = 'Hour';
Info.Signals(end).Dimensions  = '1, nbPings';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'nb milliseconds since midnight';
Info.Signals(end).FileName    = fullfile(nomDirPath,'Hour.bin');

Info.Signals(end+1).Name      = 'PingNumber';
Info.Signals(end).Dimensions  = '1, nbPings';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = ' ';
Info.Signals(end).FileName    = fullfile(nomDirPath,'PingNumber.bin');

Info.Signals(end+1).Name      = 'LatitudeNadir';
Info.Signals(end).Dimensions  = '1, nbPings';
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile(nomDirPath,'LatitudeNadir.bin');

Info.Signals(end+1).Name      = 'LongitudeNadir';
Info.Signals(end).Dimensions  = '1, nbPings';
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile(nomDirPath,'LongitudeNadir.bin');

Info.Signals(end+1).Name      = 'DepthTop';
Info.Signals(end).Dimensions  = '1, nbPings';
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile(nomDirPath,'DepthTop.bin');

Info.Signals(end+1).Name      = 'DepthBottom';
Info.Signals(end).Dimensions  = '1, nbPings';
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile(nomDirPath,'DepthBottom.bin');

Info.Signals(end+1).Name      = 'AcrossDistance';
Info.Signals(end).Dimensions  = 'nbSlices, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'm';
Info.Signals(end).FileName    = fullfile(nomDirPath,'AcrossDistance.bin');

Info.Signals(end+1).Name      = 'SliceExists';
Info.Signals(end).Dimensions  = 'nbSlices, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile(nomDirPath,'SliceExists.bin');

Info.Images(1).Name          = 'LatitudeTop';
Info.Images(end).Dimensions  = 'nbSlices, nbPings';
Info.Images(end).Storage     = 'double';
Info.Images(end).Unit        = 'deg';
Info.Images(end).FileName    = fullfile(nomDirPath,'LatitudeTop.bin');

Info.Images(end+1).Name      = 'LongitudeTop';
Info.Images(end).Dimensions  = 'nbSlices, nbPings';
Info.Images(end).Storage     = 'double';
Info.Images(end).Unit        = 'deg';
Info.Images(end).FileName    = fullfile(nomDirPath,'LongitudeTop.bin');

% Info.Images(end+1).Name      = 'Reflectivity';
% Info.Images(end).Dimensions  = 'nbRows, nbPings, nbSlices';
% Info.Images(end).Storage     = 'single';
% Info.Images(end).Unit        = 'dB';
% Info.Images(end).FileName    = fullfile(nomDirPath,'Reflectivity.bin');