function flag = WC_AlongPathFlyTexture(nomFicXml, listFicCSV, Tag, nomDirOut)

if ischar(nomFicXml)
    nomFicXml = {nomFicXml};
end

if ischar(listFicCSV)
    listFicCSV = {listFicCSV};
end

N = length(nomFicXml);
str1 = 'Cr�ation des coupes de WC le long d''un chemin';
str2 = 'Processing WC images along a track';
hw  = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:length(nomFicXml)
    my_waitbar(k, N, hw)
    flag = WC_SlicesAlongNavigation_unitaire1(nomFicXml{k}, listFicCSV, Tag, nomDirOut);
end
my_close(hw, 'MsgEnd')


function flag = WC_SlicesAlongNavigation_unitaire1(nomFicXml, listFicCSV, Tag, nomDirOut)

for kCSV=1:length(listFicCSV)
    [nomDirXml, nomFicSeul] = fileparts(nomFicXml);
    nomDirXml = fullfile(nomDirXml, [nomFicSeul '.zip']);
    my_unzip(nomDirXml);
    
    [flag, Data3DMatrix] = WC_readImageCube3D(nomFicXml, 'Memmapfile', 1);
    if ~flag
        return
    end
    
%     [~, ~, Ext] = fileparts(nomFicXml);
%     if strcmp(Ext, '.nc')
        flag = WC_SlicesAlongNavigation_unitaire2Netcdf(nomFicXml, kCSV, listFicCSV{kCSV}, nomDirOut, Data3DMatrix, Tag);
%     else
%         flag = WC_SlicesAlongNavigation_unitaire2(nomFicXml, kCSV, listFicCSV{kCSV}, nomDirOut, Data3DMatrix, Tag);
%     end
end


function flag = WC_SlicesAlongNavigation_unitaire2(nomFicXml, kCSV, listFicCSV, nomDirOut, Data3DMatrix, Tag)

%% Lecture du fichier .csv

[flag, lat, lon] = read_3DV_path(listFicCSV);
if ~flag
    fileID = fopen(listFicCSV);
    C = textscan(fileID, '%f;%f;');
    if iscell(C) && (length(C) == 2)
        lat = C{1};
        lon = C{2};
    else
        return
    end
    fclose(fileID);
end

%% Cr�ation du r�pertoire

if isempty(Tag)
    Tag = sprintf('_WCPath%d', kCSV);
else
    Tag = sprintf('_%s_WCPath%d', Tag, kCSV);
end
[~, nomFic] = fileparts(nomFicXml);
nomFic = strrep(nomFic, '_PolarEchograms', '');
nomFic = strrep(nomFic, '_WCCube', Tag);
nomFic = strrep(nomFic, '_Cube', Tag);
nomDirPath = nomFic;

ncFileName = fullfile(nomDirOut, [nomFic '.nc']);

%% Interpolation de la navigation du path au pas de grille de la matrice 3D

Fuseau = floor((mean(lon+180, 'omitnan'))/6 + 1);
LatMean = mean(lat, 'omitnan');
if LatMean >= 0
    Hemi = 'N';
else
    Hemi = 'S';
end
Carto = cl_carto('Ellipsoide.Type', 11, 'Projection.Type', 3, ...
    'Projection.UTM.Fuseau', Fuseau, 'Projection.UTM.Hemisphere', Hemi);
[xPath, yPath] = latlon2xy(Carto, lat, lon);
Step =  double(mean(diff(Data3DMatrix.AcrossDistance)));
di = sqrt((diff(xPath)) .^2 + diff(yPath) .^ 2);
sub = find(di == 0);
di(sub) = [];
xPath(sub) = [];
yPath(sub) = [];

d = [0; cumsum(di(:))];
dNew = d(1):Step:d(end);
xPath3D = interp1(d, xPath, dNew);
yPath3D = interp1(d, yPath, dNew);
[lat3D, lon3D] = xy2latlon(Carto, xPath3D, yPath3D);
%{
FigUtils.createSScFigure; PlotUtils.createSScPlot(lon, lat, '-o'); grid on; axisGeo
FigUtils.createSScFigure; PlotUtils.createSScPlot(xPath, yPath, '-o'); grid on; axis equal;
FigUtils.createSScFigure; PlotUtils.createSScPlot(di, '-o'); grid on
FigUtils.createSScFigure; PlotUtils.createSScPlot(d, '-o'); grid on
FigUtils.createSScFigure; PlotUtils.createSScPlot(xPath3D, yPath3D, '-o'); grid on
FigUtils.createSScFigure; PlotUtils.createSScPlot(lon3D, lat3D, '-o'); grid on; axisGeo
%}

%% Extraction des coordonn�es ligne et colonne correspondant au chemin

LatitudeTop  = Data3DMatrix.LatitudeTop(:,:);
LongitudeTop = Data3DMatrix.LongitudeTop(:,:);
[n1, n2] = size(LatitudeTop);
n = length(lat3D);
sub = false(1,n);
kx = NaN(1,n);
ky = NaN(1,n);
for k=1:n
    Dist = (LatitudeTop - lat3D(k)) .^ 2 + (LongitudeTop - lon3D(k)) .^ 2;
    [~, ind] = min(Dist(:));
    [k1, k2] = ind2sub([n1, n2] , ind);
    if (k1 > 1) && (k1 < n1) && (k2 > 1) && (k2 < n2)
        sub(k) = true;
        kx(k) = k2;
        ky(k) = k1;
    end
end
lat3D = lat3D(sub);
lon3D = lon3D(sub);
kx    = kx(sub);
ky    = ky(sub);
%{
FigUtils.createSScFigure; PlotUtils.createSScPlot(kx); grid on;
FigUtils.createSScFigure; PlotUtils.createSScPlot(ky); grid on;
FigUtils.createSScFigure; PlotUtils.createSScPlot(kx, ky); grid on; axis equal;
pppp = sub2ind([n1, n2], ky, kx);
ppppLat = LatitudeTop(pppp)
ppppLon = LongitudeTop(pppp)
FigUtils.createSScFigure; PlotUtils.createSScPlot(ppppLon, ppppLat); grid on; axisGeo;
figure; plot(LongitudeTop(:,:), LatitudeTop(:,:), '.'); grid on; axisGeo;
hold on;  PlotUtils.createSScPlot(lon3D, lat3D, 'bo');
PlotUtils.createSScPlot(ppppLon, ppppLat, 'r*');
%}

%% 

% binsCentre = 0;
nbPings = length(kx);
if nbPings <= 1
    flag = 0;
    return
end
nbRows = Data3DMatrix.Dimensions.nbRows;

%% Cr�ation des images

try
    Reflectivity = Data3DMatrix.Reflectivity(:,:,:);
catch ME %#ok<NASGU>
%     ME
    Reflectivity = Data3DMatrix.Reflectivity;
end

if isfield(Data3DMatrix, 'Mask')
    try
        Mask = Data3DMatrix.Mask(:,:,:);
    catch
        Mask = Data3DMatrix.Mask;
        my_breakpoint
    end
end

X = NaN(nbPings, nbRows, 'single');
for k=1:nbPings
    X(k,:) = Reflectivity(:,kx(k),ky(k));
end
% SonarScope(X)
% clear Reflectivity

Reflectivity = X';
%     nomFic2 = ncFileName;

if isfield(Data3DMatrix, 'Mask')
    X = NaN(nbPings, nbRows, 'single');
    for k=1:nbPings
        X(k,:) = Mask(:,kx(k),ky(k));
    end
    ReflectivityMask = (X' > 0.5);
end

%% Definition of the structure

[Info, Data] = createData(Data3DMatrix, nbPings, lat3D, lon3D, ky, nomDirPath);

%% Export en XML-Bin ou Netcdf

Data.Dimensions = Info.Dimensions;
Data.Reflectivity = Reflectivity;
Data.Dimensions.nbGroups = Data.Dimensions.nbSlices;
Data.Dimensions.nbSlices = 1;
SliceNames = {'Reflectivity'};
SliceUnits = {'dB'};

if isfield(Data3DMatrix, 'Mask')
    Data.Reflectivity(:,:,:,end+1) = ReflectivityMask;
    Data.Dimensions.nbSlices = Data.Dimensions.nbSlices + 1;
    SliceNames{end+1} = 'Mask';
    SliceUnits{end+1} = '';
    MaskSignification = {'After bottom detection'};
else
    MaskSignification = [];
end

if isfield(Data3DMatrix, 'CLimRaw')
    CLimRaw = Data3DMatrix.CLimRaw;
elseif isfield(Data3DMatrix, 'CLim')
    CLimRaw = Data3DMatrix.CLim;
end
if isempty(CLimRaw)
    CLimRaw = [-64 10];
end

if isfield(Data3DMatrix, 'CLimComp')
    CLimComp = Data3DMatrix.CLimComp;
elseif isfield(Data3DMatrix, 'CLim')
    CLimComp = Data3DMatrix.CLim;
end
if isempty(CLimComp)
    CLimComp = [-10 25];
end

flag = NetcdfGlobe3DUtils.createGlobeFlyTexture(ncFileName, Data, [], SliceNames, SliceUnits, ...
    'MaskSignification', MaskSignification, 'CLimRaw', CLimRaw, 'CLimComp', CLimComp);


function [Info, Data] = createData(Data3DMatrix, nbPings, lat3D, lon3D, ky, nomDirPath)

binsCentre = 0;

%% Definition of the structure

Info.Signature1      = 'SonarScope';
Info.Signature2      = 'ImagesAlongNavigation';
Info.FormatVersion   = 20100624;
Info.Name            = Data3DMatrix.Name;
Info.ExtractedFrom   = Data3DMatrix.ExtractedFrom;

Info.Survey         = Data3DMatrix.Survey;
Info.Vessel         = Data3DMatrix.Vessel;
Info.Sounder        = Data3DMatrix.Sounder;
Info.ChiefScientist = Data3DMatrix.ChiefScientist;

Info.DepthTop       = 0;
Info.DepthBottom    = Data3DMatrix.Depth(1);
Info.MiddleSlice    = 0;

nbRows = Data3DMatrix.Dimensions.nbRows;
Info.Dimensions.nbPings  = nbPings;
Info.Dimensions.nbSlices = 1;
Info.Dimensions.nbRows   = nbRows;

Data.LatitudeNadir   = NaN(1, nbPings);
Data.LongitudeNadir  = NaN(1, nbPings);
Data.LatitudeTop     = NaN(1, nbPings);
Data.LongitudeTop    = NaN(1, nbPings);
% Data.LatitudeBottom  = NaN(1, nbPings);
% Data.LongitudeBottom = NaN(1, nbPings);
Data.PingNumber      = NaN(1, nbPings, 'single');
Data.AcrossDistance  = NaN(1, 1, 'single');
Data.Date            = NaN(1, nbPings, 'single');
Data.Hour            = NaN(1, nbPings, 'single');
% Info.Depth           = NaN(1, nbPings, 'single');

Data.AcrossDistance(:) = binsCentre;
Data.SliceExists       = true;

% TODO : bricolage abominable : il faut repositionner chaque ping en
% fonction de l'immersion en amont
% ImmersionMoyenne = nanmean(Data3DMatrix.Immersion(:));
% Immersion = Data3DMatrix.Immersion(:);

% Data.DepthTop    = ones(1, nbPings, 'single') * max(Data3DMatrix.Depth) - ImmersionMoyenne;
% Data.DepthBottom = ones(1, nbPings, 'single') * min(Data3DMatrix.Depth) - ImmersionMoyenne;

Immersion = Data3DMatrix.Immersion(:)';
Immersion = my_interp1(1:length(Immersion), Immersion, ky);
Immersion(isnan(Immersion)) = 0;

% if isfield(Data3DMatrix, 'Tide') && ~isempty(Data3DMatrix.Tide) && any(~isnan(Data3DMatrix.Tide))
%     Tide = Data3DMatrix.Tide(:)';
% else
%     Tide = zeros(size(Immersion));
% end
% Tide = zeros(size(Immersion)); % Modif JMA le 24/04/2020

Heave = Data3DMatrix.Heave(:)';
Heave = my_interp1(1:length(Heave), Heave, ky);

% Data.DepthTop    = ones(1, nbPings, 'single') * max(Data3DMatrix.Depth) + Immersion;
% Data.DepthBottom = ones(1, nbPings, 'single') * min(Data3DMatrix.Depth) + Immersion; % Modif JMA le 29/09/2020
Data.DepthTop    = zeros(1, nbPings, 'single') + Immersion; % Modif JM1 le 25/01/2021
Data.DepthBottom = ones( 1, nbPings, 'single') * min(Data3DMatrix.Z) + Immersion; % Modif JMA le 29/09/2020


% Avant modif JMA du 21/08/2020
% Data.DepthBottom = Data.DepthBottom - Heave + Tide; % Car TranscucerDepth utilis� avant int�gre d�j� le Heave

% Modifi� par JMA le 21/08/2010 pour donn�es VLIZ (Nora4) : test� sur XML-Bin
% Data.DepthBottom = Data.DepthBottom - Heave ; % Car TranscucerDepth utilis� avant int�gre d�j� le Heave
if isfield(Data3DMatrix, 'Heave') && ~all(isnan(Data3DMatrix.Heave(:)))
    Data.DepthBottom = Data.DepthBottom - Heave(:)'; %  + Tide; % Car TranscucerDepth utilis� avant int�gre d�j� le Heave
else
    Data.DepthBottom = Data.DepthBottom; % + Tide; % Car TranscucerDepth utilis� avant int�gre d�j� le Heave
end

for k=1:nbPings
    %     Info.Depth(k,:)            = Data3DMatrix.Depth % TODO
    Data.Date(k)           = Data3DMatrix.Date(ky(k));
    Data.Hour(k)           = Data3DMatrix.Hour(ky(k));
    Data.PingNumber(k)     = Data3DMatrix.iPing(ky(k));
%     Data.LatitudeNadir(k)  = Data3DMatrix.latCentre(ky(k));
%     Data.LongitudeNadir(k) = Data3DMatrix.lonCentre(ky(k));
%     Data.LatitudeNadir(k)  = lat3D(ky(k));
%     Data.LongitudeNadir(k) = lon3D(ky(k));
%     Data.LatitudeTop(:,k)  = lat3D(ky(k));
%     Data.LongitudeTop(:,k) = lon3D(ky(k));
    Data.LatitudeNadir(k)  = lat3D(k);
    Data.LongitudeNadir(k) = lon3D(k);
    Data.LatitudeTop(:,k)  = lat3D(k);
    Data.LongitudeTop(:,k) = lon3D(k);
end

Info.Signals(1).Name          = 'Date';
Info.Signals(end).Dimensions  = '1, nbPings';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'Ifremer';
Info.Signals(end).FileName    = fullfile(nomDirPath,'Date.bin');

Info.Signals(end+1).Name      = 'Hour';
Info.Signals(end).Dimensions  = '1, nbPings';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'nb milliseconds since midnight';
Info.Signals(end).FileName    = fullfile(nomDirPath,'Hour.bin');

Info.Signals(end+1).Name      = 'PingNumber';
Info.Signals(end).Dimensions  = '1, nbPings';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = ' ';
Info.Signals(end).FileName    = fullfile(nomDirPath,'PingNumber.bin');

Info.Signals(end+1).Name      = 'LatitudeNadir';
Info.Signals(end).Dimensions  = '1, nbPings';
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile(nomDirPath,'LatitudeNadir.bin');

Info.Signals(end+1).Name      = 'LongitudeNadir';
Info.Signals(end).Dimensions  = '1, nbPings';
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile(nomDirPath,'LongitudeNadir.bin');

Info.Signals(end+1).Name      = 'DepthTop';
Info.Signals(end).Dimensions  = '1, nbPings';
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile(nomDirPath,'DepthTop.bin');

Info.Signals(end+1).Name      = 'DepthBottom';
Info.Signals(end).Dimensions  = '1, nbPings';
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile(nomDirPath,'DepthBottom.bin');

Info.Signals(end+1).Name      = 'AcrossDistance';
Info.Signals(end).Dimensions  = 'nbSlices, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'm';
Info.Signals(end).FileName    = fullfile(nomDirPath,'AcrossDistance.bin');

Info.Signals(end+1).Name      = 'SliceExists';
Info.Signals(end).Dimensions  = 'nbSlices, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile(nomDirPath,'SliceExists.bin');

Info.Images(1).Name          = 'LatitudeTop';
Info.Images(end).Dimensions  = 'nbSlices, nbPings';
Info.Images(end).Storage     = 'double';
Info.Images(end).Unit        = 'deg';
Info.Images(end).FileName    = fullfile(nomDirPath,'LatitudeTop.bin');

Info.Images(end+1).Name      = 'LongitudeTop';
Info.Images(end).Dimensions  = 'nbSlices, nbPings';
Info.Images(end).Storage     = 'double';
Info.Images(end).Unit        = 'deg';
Info.Images(end).FileName    = fullfile(nomDirPath,'LongitudeTop.bin');

% Info.Images(end+1).Name      = 'Reflectivity';
% Info.Images(end).Dimensions  = 'nbRows, nbPings, nbSlices';
% Info.Images(end).Storage     = 'single';
% Info.Images(end).Unit        = 'dB';
% Info.Images(end).FileName    = fullfile(nomDirPath,'Reflectivity.bin');


function flag = WC_SlicesAlongNavigation_unitaire2Netcdf(nomFicXml, kCSV, listFicCSV, nomDirOut, Data3DMatrix, Tag)

%% Lecture du fichier .csv

[flag, lat, lon] = read_3DV_path(listFicCSV);
if ~flag
    fileID = fopen(listFicCSV);
    C = textscan(fileID, '%f;%f;');
    if iscell(C) && (length(C) == 2)
        lat = C{1};
        lon = C{2};
    else
        return
    end
    fclose(fileID);
end

%% Cr�ation du r�pertoire

if isempty(Tag)
    Tag = sprintf('_WCPath%d', kCSV);
else
    Tag = sprintf('_%s_WCPath%d', Tag, kCSV);
end
[~, nomFic] = fileparts(nomFicXml);
nomFic = strrep(nomFic, '_PolarEchograms', '');
nomFic = strrep(nomFic, '_WCCube', Tag);
nomFic = strrep(nomFic, '_Cube', Tag);
nomDirPath = nomFic;

ncFileName = fullfile(nomDirOut, [nomFic '.nc']);

%% Interpolation de la navigation du path au pas de grille de la matrice 3D

Fuseau = floor((mean(lon+180, 'omitnan'))/6 + 1);
LatMean = mean(lat, 'omitnan');
if LatMean >= 0
    Hemi = 'N';
else
    Hemi = 'S';
end
Carto = cl_carto('Ellipsoide.Type', 11, 'Projection.Type', 3, ...
    'Projection.UTM.Fuseau', Fuseau, 'Projection.UTM.Hemisphere', Hemi);
[xPath, yPath] = latlon2xy(Carto, lat, lon);
Step =  double(mean(diff(Data3DMatrix.AcrossDistance(:,:))));
di = sqrt((diff(xPath)) .^2 + diff(yPath) .^ 2);
sub = find(di == 0);
di(sub) = [];
xPath(sub) = [];
yPath(sub) = [];

d = [0; cumsum(di(:))];
dNew = d(1):Step:d(end);
xPath3D = interp1(d, xPath, dNew);
yPath3D = interp1(d, yPath, dNew);
[lat3D, lon3D] = xy2latlon(Carto, xPath3D, yPath3D);
%{
FigUtils.createSScFigure; PlotUtils.createSScPlot(lon, lat, '-o'); grid on; axisGeo
FigUtils.createSScFigure; PlotUtils.createSScPlot(xPath, yPath, '-o'); grid on; axis equal;
FigUtils.createSScFigure; PlotUtils.createSScPlot(di, '-o'); grid on
FigUtils.createSScFigure; PlotUtils.createSScPlot(d, '-o'); grid on
FigUtils.createSScFigure; PlotUtils.createSScPlot(xPath3D, yPath3D, '-o'); grid on
FigUtils.createSScFigure; PlotUtils.createSScPlot(lon3D, lat3D, '-o'); grid on; axisGeo
%}

%% Extraction des coordonn�es ligne et colonne correspondant au chemin

LatitudeTop  = Data3DMatrix.LatitudeTop(:,:);
LongitudeTop = Data3DMatrix.LongitudeTop(:,:);
[n1, n2] = size(LatitudeTop);
n = length(lat3D);
sub = false(1,n);
kx = NaN(1,n);
ky = NaN(1,n);
for k=1:n
    Dist = (LatitudeTop - lat3D(k)) .^ 2 + (LongitudeTop - lon3D(k)) .^ 2;
    [~, ind] = min(Dist(:));
    [k1, k2] = ind2sub([n1, n2] , ind);
    if (k1 > 1) && (k1 < n1) && (k2 > 1) && (k2 < n2)
        sub(k) = true;
        kx(k) = k2;
        ky(k) = k1;
    end
end
lat3D = lat3D(sub);
lon3D = lon3D(sub);
kx    = kx(sub);
ky    = ky(sub);
%{
FigUtils.createSScFigure; PlotUtils.createSScPlot(kx); grid on;
FigUtils.createSScFigure; PlotUtils.createSScPlot(ky); grid on;
FigUtils.createSScFigure; PlotUtils.createSScPlot(kx, ky); grid on; axis equal;
pppp = sub2ind([n1, n2], ky, kx);
ppppLat = LatitudeTop(pppp)
ppppLon = LongitudeTop(pppp)
FigUtils.createSScFigure; PlotUtils.createSScPlot(ppppLon, ppppLat); grid on; axisGeo;
figure; plot(LongitudeTop(:,:), LatitudeTop(:,:), '.'); grid on; axisGeo;
hold on;  PlotUtils.createSScPlot(lon3D, lat3D, 'bo');
PlotUtils.createSScPlot(ppppLon, ppppLat, 'r*');
%}

%% 

% binsCentre = 0;
nbPings = length(kx);
if nbPings <= 1
    flag = 0;
    return
end
nbRows = Data3DMatrix.Dimensions.nbRows;

%% Cr�ation des images

if isfield(Data3DMatrix, 'ReflectivityRaw')
    try
        ReflectivityRaw = Data3DMatrix.ReflectivityRaw(:,:,:);
    catch ME %#ok<NASGU>
        %     ME
        ReflectivityRaw = Data3DMatrix.ReflectivityRaw;
    end
elseif isfield(Data3DMatrix, 'Reflectivity')
    try
        ReflectivityRaw = Data3DMatrix.Reflectivity(:,:,:);
    catch ME %#ok<NASGU>
        %     ME
        ReflectivityRaw = Data3DMatrix.Reflectivity;
    end
else
    flag = 0;
    return
end

if isfield(Data3DMatrix, 'ReflectivityComp')
    try
        ReflectivityComp = Data3DMatrix.ReflectivityComp(:,:,:);
    catch ME %#ok<NASGU>
        %     ME
        ReflectivityComp = Data3DMatrix.ReflectivityComp;
    end
elseif isfield(Data3DMatrix, 'Reflectivity')
    try
        ReflectivityComp = Data3DMatrix.Reflectivity(:,:,:);
    catch ME %#ok<NASGU>
        %     ME
        ReflectivityComp = Data3DMatrix.Reflectivity;
    end
else
    flag = 0;
    return
end

try
    Mask = Data3DMatrix.Mask(:,:,:);
catch
    Mask = Data3DMatrix.Mask;
    my_breakpoint
end

X = NaN(nbPings, nbRows, 'single');
for k=1:nbPings
    X(k,:) = ReflectivityRaw(:,kx(k),ky(k));
end
ReflectivityRaw = X';

X = NaN(nbPings, nbRows, 'single');
for k=1:nbPings
    X(k,:) = ReflectivityComp(:,kx(k),ky(k));
end
ReflectivityComp = X';


X = NaN(nbPings, nbRows, 'single');
for k=1:nbPings
    X(k,:) = Mask(:,kx(k),ky(k));
end
ReflectivityMask = (X' > 0.5);

%% Definition of the structure

[Info, Data] = createData(Data3DMatrix, nbPings, lat3D, lon3D, ky, nomDirPath);

%% Export en XML-Bin ou Netcdf

Data.Dimensions = Info.Dimensions;
Data.ReflectivityRaw  = ReflectivityRaw;
Data.ReflectivityComp = ReflectivityComp;
Data.Dimensions.nbGroups = Data.Dimensions.nbSlices;
Data.Dimensions.nbSlices = 3;
SliceNames = {'ReflectivityRaw'; 'ReflectivityComp'; 'Mask'};
SliceUnits = {'dB'; 'dB'; ''};

Data.Reflectivity(:,:,:,1) = ReflectivityRaw;
Data.Reflectivity(:,:,:,2) = ReflectivityComp;
Data.Reflectivity(:,:,:,3) = ReflectivityMask;

Data.Dimensions.nbGroups = 1;
Data.Dimensions.nbSlices = 3;

MaskSignification = {'After bottom detection'};

CLimComp = [-10 25];
if isfield(Data3DMatrix, 'CLimRaw')
    CLimRaw = Data3DMatrix.CLimRaw;
elseif isfield(Data3DMatrix, 'CLim')
    CLimRaw = Data3DMatrix.CLim;
end
if isempty(CLimRaw)
    CLimRaw = [-64 10];
end

if isfield(Data3DMatrix, 'CLimComp')
    CLimComp = Data3DMatrix.CLimComp;
elseif isfield(Data3DMatrix, 'CLim')
    CLimComp = Data3DMatrix.CLim;
end
if isempty(CLimComp)
    CLimComp = [-10 25];
end

flag = NetcdfGlobe3DUtils.createGlobeFlyTexture(ncFileName, Data, [], SliceNames, SliceUnits, ...
    'MaskSignification', MaskSignification, 'CLimRaw', CLimRaw, 'CLimComp', CLimComp);
