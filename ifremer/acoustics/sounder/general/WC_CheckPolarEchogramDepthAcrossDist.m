% TODO : refaire tout �a avec GUILayout et un joli composant "video player"
% Avance normal, arri�re, pas � pas, d�but, fin, esc et une touche delete
% de l'image.

function WC_CheckPolarEchogramDepthAcrossDist(nomFicXml)

if ~iscell(nomFicXml)
    nomFicXml = {nomFicXml};
end

figShortucs = InfoShortcuts;

nbFic = length(nomFicXml);
hw = create_waitbar('WC-DepthAcrossDist cleaning.', 'N', nbFic);
for kFic=1:nbFic
    my_waitbar(kFic, nbFic, hw)
    Fig = WC_CheckPolarEchogramDepthAcrossDist_unitaire(nomFicXml{kFic});
    while ishandle(Fig)
        pause(1)
    end
end
my_close(hw, 'MsgEnd')

    function fig = InfoShortcuts
        str = {};
        str{end+1} = 'Rightarrow : Next Image';
        str{end+1} = 'Leftarrow : Previous Image';
        str{end+1} = 'Pagedown : First Image';
        str{end+1} = 'Pageup : Last Image';
        str{end+1} = 'Downarrow : Image - 10';
        str{end+1} = 'Uparrow : Image + 10';
        str{end+1} = '"n" or "p": Edit a specific ping number';
        str{end+1} = '"r": Rename the Image';
        str{end+1} = '"z": Restablish a renamed Image';
        str{end+1} = '"d" or Suppr : Delete the Image';
        str{end+1} = '"s": Edit the Image in SonarScope';
        str{end+1} = '"q" or "Echap" : Exit the tool for current file';
        fig = edit_infoPixel(str, [], 'Size', [100, 300], ...
            'PromptString', 'SonarScope Shortcuts for Water Column Data cleaning.');
    end

    function Fig = WC_CheckPolarEchogramDepthAcrossDist_unitaire(nomFicXml)
        
        Fig = [];
        
        %% Lecture du fichier XML d�crivant la donn�e
        
        [nomDr1, nomFic1, Ext] = fileparts(nomFicXml);
        if strcmp(Ext, '.xml')
            [flag, Data] = XMLBinUtils.readGrpData(nomFicXml, 'Memmapfile', -1);
            if ~flag
                return
            end
            ncID = [];
        else
            [flag, Data] = NetcdfGlobe3DUtils.readGlobeFlyTexture(nomFicXml, 'Light', 2);
            if ~flag
                return
            end
            ncID = netcdf.open(nomFicXml);
        end
        
        %% Trac�s graphiques
        
        
        %% Visualisation des images
        
        Fig = figure('WindowKeyPressFcn', @Keyboard, 'WindowButtonDownFcn', @ButtonDown, 'CloseRequestFcn',@my_closereq);
        increment = 1;
        k = 0;
        % while 1
        k = k + increment;
        k = max(k, 1);
        
        if strcmp(Ext, '.xml')
            nbPings = Data.Dimensions.nbPings;
            hAxeRaw  = subplot(1,1,1);
            hAxeComp = [];
        else
            nbPings = length(Data.Datetime);
            hAxeRaw  = subplot(2,1,1);
            hAxeComp = subplot(2,1,2);
        end
        k = min(k, nbPings);
        
        hImageRaw  = [];
        hImageComp = [];
        nomFicImage = displayOneEchogram(k, nomDr1, nomFic1, Data, Fig);
        % end
        
        function ButtonDown(hFig, WindowMouseData) %#ok<INUSD>
            nomFicImage = displayOneEchogram(k, nomDr1, nomFic1, Data, Fig);
            SelectionType = get(hFig, 'SelectionType');
            switch SelectionType
                case 'normal' % Try to set in pause mode
                    increment = 1;
                    pause(2)
                case 'alt' % try to exit from a pause mode
                    pause(2)
                    increment = -1;
                otherwise
                    %                 SelectionType;
            end
        end
        
        function Keyboard(hFig, kData, varargin)
            switch kData.Key
                case 'space'
                    % Il faudrait relancer la boucle while
                    
                case {'d'; 'delete'}
                    nomFicImage = displayOneEchogram(k, nomDr1, nomFic1, Data, Fig);
                    [rep, flag] = my_questdlg('Do you really want to delete this image ?');
                    if flag && (rep == 1)
                        delete(nomFicImage)
                        nomFicImage = strrep(nomFicImage, '.tif', '.png');
                        if exist(nomFicImage, 'file')
                            delete(nomFicImage);
                        end
                    end
                    nomFicImage = displayOneEchogram(k, nomDr1, nomFic1, Data, Fig);
                    
                case 'r'
                    nomFicImage = displayOneEchogram(k, nomDr1, nomFic1, Data, Fig);
                    [rep, flag] = my_questdlg('Do you really want to rename this image ?');
                    if flag && (rep == 1)
                        [nomDir2, nomImage2] = fileparts(nomFicImage);
                        
                        NomFicImage2  = fullfile(nomDir2, [nomImage2, '.tif']);
                        if exist(NomFicImage2, 'file')
                            nomFicImageNew = strrep(NomFicImage2, '.tif', '-tif.bad');
                            my_rename(NomFicImage2, nomFicImageNew)
                        end
                        
                        NomFicImage2  = fullfile(nomDir2, [nomImage2, '.png']);
                        if exist(NomFicImage2, 'file')
                            nomFicImageNew = strrep(NomFicImage2, '.png', '-png.bad');
                            my_rename(NomFicImage2, nomFicImageNew)
                        end
                    end
                    nomFicImage = displayOneEchogram(k, nomDr1, nomFic1, Data, Fig);
                    
                case 'leftarrow'
                    k = k - 1;
                    k = max(k, 1);
                    nomFicImage = displayOneEchogram(k, nomDr1, nomFic1, Data, Fig);
                    
                case 'rightarrow'
                    k = k + 1;
                    k = min(k, nbPings);
                    nomFicImage =  displayOneEchogram(k, nomDr1, nomFic1, Data, Fig);
                    
                case 'pagedown'
                    k = nbPings;
                    nomFicImage = displayOneEchogram(k, nomDr1, nomFic1, Data, Fig);
                    
                case 'pageup'
                    k = 1;
                    nomFicImage = displayOneEchogram(k, nomDr1, nomFic1, Data, Fig);
                    
                case {'n'; 'p'}
                    p    = ClParametre('Name', 'Ping number', 'MinValue', 1, 'MaxValue', nbPings, 'Value', k, 'Format', '%d');
                    a = StyledParametreDialog('params', p, 'Title', 'Select a particular ping number');
                    a.sizes = [0    -2   0    -2    0    -1    -1    0];
                    a.openDialog;
                    flag = a.okPressedOut;
                    if ~flag
                        return
                    end
                    k = a.getParamsValue;
                    nomFicImage = displayOneEchogram(k, nomDr1, nomFic1, Data, Fig);
                    
                case {'q'; 'escape'}
                    close(hFig)
                    
                case 's'
                    nomFicImage = editOneEchogram(k, nomDr1, nomFic1, Data, Fig);
                    
                case 'downarrow'
                    k = k + 10;
                    k = min(k, nbPings);
                    nomFicImage =  displayOneEchogram(k, nomDr1, nomFic1, Data, Fig);
                    
                case 'uparrow'
                    k = k - 10;
                    k = max(k, 1);
                    nomFicImage = displayOneEchogram(k, nomDr1, nomFic1, Data, Fig);
                    
                case 'z'
                    nomFicImage = displayOneEchogram(k, nomDr1, nomFic1, Data, Fig);
                    [nomDir2, nomImage2] = fileparts(nomFicImage);
                    NomFicImage2  = fullfile(nomDir2, [nomImage2, '-tif.bad']);
                    NomFicImage3  = fullfile(nomDir2, [nomImage2, '-png.bad']);
                    if exist(NomFicImage2, 'file') || exist(NomFicImage3, 'file')
                        [rep, flag] = my_questdlg('Do you really want to restablish this image ?');
                        if flag && (rep == 1)
                            [nomDir2, nomImage2] = fileparts(nomFicImage);
                            NomFicImage2  = fullfile(nomDir2, [nomImage2, '-tif.bad']);
                            if exist(NomFicImage2, 'file')
                                nomFicImageNew = strrep(NomFicImage2, '-tif.bad', '.tif');
                                my_rename(NomFicImage2, nomFicImageNew)
                            end
                            
                            NomFicImage2  = fullfile(nomDir2, [nomImage2, '-png.bad']);
                            if exist(NomFicImage2, 'file')
                                nomFicImageNew = strrep(NomFicImage2, '-png.bad', '.png');
                                my_rename(NomFicImage2, nomFicImageNew)
                            end
                        end
                        nomFicImage = displayOneEchogram(k, nomDr1, nomFic1, Data, Fig);
                    end
                    
                otherwise
                    kData.Key
            end
        end
        
        function nomFicImage = displayOneEchogram(k, nomDr1, nomFic1, Data, Fig)
            if strcmp(Ext, '.xml')
                [IRaw, x, z, CLim, nomFicImage] = readPingWCDXML(nomDr1, nomFic1, Data.iPing(k));
                
                figure(Fig); axis(hAxeRaw);
%                 Title = sprintf('%s\niPing %d/%d', nomFicImage, k, nbPings);
                if strcmp(Ext, '.xml')
                    Title = sprintf('%s\niPing %d/%d', nomFic1, k, nbPings);
                else
                ind = strfind(nomFicXml, '\');
                    Title = sprintf('%s\niPing %d/%d', nomFicImage((ind(end)+1):end), k, nbPings);
                end
                hImageRaw = imagesc(hAxeRaw, x, z, IRaw); axis xy; title(Title, 'Interpreter', 'none');
                colormap(jet(256)); axis equal; axis tight;
                hcRaw = colorbar;
                set(hcRaw, 'ButtonDownFcn', {@clbkColorbar; hAxeRaw}, 'BusyAction', 'cancel', 'Interruptible', 'Off')
                if ~isempty(CLim')
                    set(get(hImageRaw, 'parent'), 'CLim', CLim)
                end
                drawnow
            else
                [IRaw, IComp, x, z, CLim, CLimComp, nomFicImage] = readPingWCDNetcdf(ncID, k);
                                
                figure(Fig);
                Title = sprintf('%s\niPing %d/%d', nomFic1, k, nbPings);
                
                
                %{
                if isempty(hAxeRaw) || ~ishandle(hAxeRaw) || isempty(hImageRaw) || ~ishandle(hImageRaw) || isempty(get(hImageRaw, 'CData'))
                    axes(hAxeRaw)
                    hImageRaw = imagesc(hAxeRaw, IRaw); axis equal; axis tight; axis xy; colormap(gca, 'jet');
                    hcRaw = colorbar; %('SouthOutside');
                    set(hcRaw, 'ButtonDownFcn', {@clbkColorbar; hAxeRaw}, 'BusyAction', 'cancel', 'Interruptible', 'Off')
                else
                    set(hImageRaw, 'CData', IRaw);
                    %                     set(hAxeRaw, 'CLim', CLim)
                end
                %}
                
                
                
                if isempty(hAxeRaw) || ~ishandle(hAxeRaw) || isempty(hImageRaw) || ~ishandle(hImageRaw) || isempty(get(hImageRaw, 'CData'))
                    axis(hAxeRaw);
                    hImageRaw = imagesc(hAxeRaw, x, z, IRaw); axis xy; axis equal; axis tight;
                    hcRaw = colorbar(hAxeRaw);
                    set(hcRaw, 'ButtonDownFcn', {@clbkColorbar; hAxeRaw}, 'BusyAction', 'cancel', 'Interruptible', 'Off')
                    if ~isempty(CLim')
                        set(get(hImageRaw, 'parent'), 'CLim', CLim)
                    end
                    set(hAxeRaw, 'YDir', 'normal');  % Pourquoi on est oblig� de faire �a  ici ?
                else
                    set(hImageRaw, 'CData', IRaw);
                end
                title(hAxeRaw, Title, 'Interpreter', 'none'); colormap(jet(256));
                
                if isempty(hAxeComp) || ~ishandle(hAxeComp) || isempty(hImageComp) || ~ishandle(hImageComp) || isempty(get(hImageComp, 'CData'))
                    axis(hAxeComp);
                    hImageComp = imagesc(hAxeComp, x, z, IComp); axis xy; axis equal; axis tight;
                    hcComp = colorbar(hAxeComp);
                    set(hcComp, 'ButtonDownFcn', {@clbkColorbar; hAxeComp}, 'BusyAction', 'cancel', 'Interruptible', 'Off')
                    if ~isempty(CLimComp')
                        set(get(hImageComp, 'parent'), 'CLim', CLimComp)
                    end
                else
                    set(hImageComp, 'CData', IComp);
                end
                title(hAxeComp, Title, 'Interpreter', 'none'); colormap(jet(256));
                
                drawnow
            end
        end
        
        
        function nomFicImage = editOneEchogram(k, nomDr1, nomFic1, Data, Fig)
            if strcmp(Ext, '.xml')
                [IRaw, x, z, ~, nomFicImage] = readPingWCDXML(nomDr1, nomFic1, Data.iPing(k));
                a = cl_image('Image', IRaw, 'x', double(x), 'y', double(z), 'Name', nomFicImage);
            else
                [IRaw, IComp, x, z, ~, ~, nomFicImage] = readPingWCDNetcdf(ncID, k);
                a    = cl_image('Image', IRaw,  'x', double(x), 'y', double(z), 'Name', [nomFicImage '-Raw']);
                a(2) = cl_image('Image', IComp, 'x', double(x), 'y', double(z), 'Name', [nomFicImage '-Comp']);
            end
            
            a = SonarScope(a); %#ok<NASGU>
            
%             IRaw = get_Image(a(end));
%             export_ImageTif32bits(IRaw, nomFicImage, 'Mute', 1);
            
            nomFicImage = displayOneEchogram(k, nomDr1, nomFic1, Data, Fig);
        end
        
        
        function my_closereq(src, callbackdata) %#ok<INUSD>
            if ~isempty(ncID)
                netcdf.close(ncID);
            end
            my_close(figShortucs)
            my_close(src)
        end
        
        
        function[IRaw, IComp, x, z, CLim, CLimComp, nomFicImage] = readPingWCDNetcdf(ncID, k)
            
            [flag, Data, groupName] = NetcdfGlobe3DUtils.readOneGroup(ncID, k);
            if flag
                flagImageTrouvee = 1;
            else
                flagImageTrouvee = 0;
            end
            
            nomFicImage = groupName; %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            IRaw  = (Data.raw);
            IComp = (Data.comp);
            
            if ~flagImageTrouvee
                IRaw  = NaN(100, 100, 'single');
                IComp = NaN(100, 100, 'single');
            end
            z = linspace(Data.elevation(2,1), Data.elevation(1,1), size(IRaw,1));
            x = linspace(Data.across_dist_L,  Data.across_dist_R,  size(IRaw,2));
            
            % CLim = [-64 10];
            CLim     = [];
            CLimComp = [-5 30];
        end
        
        
        function [IRaw, x, z, CLim, nomFicImage] = readPingWCDXML(nomDr1, nomFic1, kPing)
            iDir = floor(kPing/100);
            nomDir2 = num2str(iDir, '%03d');
            nomFicImage = fullfile(nomDr1, nomFic1, nomDir2, sprintf('%05d.tif', kPing));
            flagImageTrouvee = 1;
            if ~exist(nomFicImage, 'file')
                nomFicImage = fullfile(nomDr1, nomFic1, nomDir2, sprintf('%05d.png', kPing));
                if ~exist(nomFicImage, 'file')
                    flagImageTrouvee = 0;
                end
            end
            Info = imfinfo(nomFicImage);
            if flagImageTrouvee
                IRaw = imread(nomFicImage);
                IRaw((IRaw < Info.SMinSampleValue) | (IRaw > Info.SMaxSampleValue)) = NaN;
                
                z = linspace(Data.Immersion(k), Data.Depth(k), size(IRaw,1));
                x = linspace(Data.xBab(k),      Data.xTri(k),  size(IRaw,2));
            else
                IRaw = NaN(100, 100, 'single');
                z = linspace(Data.Immersion(k), Data.Depth(k), size(IRaw,1));
                x = linspace(Data.xBab(k),      Data.xTri(k),  size(IRaw,2));
            end
            
            mots = strsplit(nomFicImage, filesep);
            if  contains(mots{end}, '.tif')
                if contains(mots{end-2}, '_Raw_PolarEchograms')
                    %  CLim = [-64 10];
                    CLim = [];
                elseif contains(mots{end-2}, '_Comp_PolarEchograms')
                    CLim = [-5 30];
                else
                    CLim = [];
                end
            else
                CLim = [];
            end
        end
        
        
        function clbkColorbar(~, ~, hAxe, varargin)
            hImcontrast = my_imcontrast(hAxe); % TODO : le Adjust ne fonctionne pas
            waitfor(hImcontrast)
        end
    end
end
