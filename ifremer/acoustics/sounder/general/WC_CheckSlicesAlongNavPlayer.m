% TODO : refaire tout �a avec GUILayout et un joli composant "video player"
% Avance normal, arri�re, pas � pas, d�but, fin, esc et une touche delete
% de l'image.

function WC_CheckSlicesAlongNavPlayer(nomFicXml)

if ~iscell(nomFicXml)
    nomFicXml = {nomFicXml};
end

figShortucs = InfoShortcuts;

nbFic = length(nomFicXml);
hw = create_waitbar('WC-AlongNav cleaning.', 'N', nbFic);
for kFic=1:nbFic
    my_waitbar(kFic, nbFic, hw)
    Fig = WC_CheckSlicesAlongNavPlayer_unitaire(nomFicXml{kFic});
    while ishandle(Fig)
        pause(1)
    end
end
my_close(hw, 'MsgEnd')

    function fig = InfoShortcuts
        str = {};
        str{end+1} = 'Rightarrow : Next Image';
        str{end+1} = 'Leftarrow : Previous Image';
        str{end+1} = 'Pagedown : First Image';
        str{end+1} = 'Pageup : Last Image';
        str{end+1} = 'Downarrow : Image - 10';
        str{end+1} = 'Uparrow : Image + 10';
        str{end+1} = '"n" or "p": Edit a specific ping number';
        str{end+1} = '"r": Rename the Image';
        str{end+1} = '"z": Restablish a renamed Image';
        str{end+1} = '"d" or Suppr : Delete the Image';
        str{end+1} = '"s": Edit the Image in SonarScope';
        str{end+1} = '"q" or "Echap" : Exit the tool for current file';
        fig = edit_infoPixel(str, [], 'Size', [100, 300], ...
            'PromptString', 'SonarScope Shortcuts for Water Column Data cleaning.');
    end

    function Fig = WC_CheckSlicesAlongNavPlayer_unitaire(nomFicXml)
        Fig = [];
        [nomDr1, nomFic1, Ext] = fileparts(nomFicXml);
        if strcmp(Ext, '.xml')
            [flag, Data] = XMLBinUtils.readGrpData(nomFicXml, 'Memmapfile', -1);
            if ~flag
                return
            end
        else
            [flag, Data] = NetcdfGlobe3DUtils.readGlobeFlyTexture(nomFicXml);
            if ~flag
                return
            end
        end
        
        %% Visualisation des images
        
        Fig = figure('WindowKeyPressFcn', @Keyboard, 'WindowButtonDownFcn', @ButtonDown, 'CloseRequestFcn',@my_closereq);
        increment = 1;
        k = 0;
        % while 1
        k = k + increment;
        k = max(k, 1);
        
        if strcmp(Ext, '.xml')
            nbPings = length(Data.AcrossDistance);
            hAxeRaw  = subplot(1,1,1);
            hAxeComp = [];
        else
            nbPings = length(Data.across_distance);
            hAxeRaw  = subplot(2,1,1);
            hAxeComp = subplot(2,1,2);
        end
        k = min(k, nbPings);
        hImageRaw  = [];
        hImageComp = [];
        nomFicImage = displayOneEchogram(k, nomDr1, nomFic1, Data, Fig);
        
        
        function ButtonDown(hFig, WindowMouseData) %#ok<INUSD>
            nomFicImage = displayOneEchogram(k, nomDr1, nomFic1, Data, Fig);
            SelectionType = get(hFig, 'SelectionType');
            switch SelectionType
                case 'normal' % Try to set in pause mode
                    increment = 1;
                    pause(2)
                case 'alt' % try to exit from a pause mode
                    pause(2)
                    increment = -1;
                otherwise
                    %                 SelectionType;
            end
        end
        
        function Keyboard(hFig, kData, varargin)
            switch kData.Key
                case 'space'
                    % Il faudrait relancer la boucle while
                    
                case {'d'; 'delete'}
                    nomFicImage = displayOneEchogram(k, nomDr1, nomFic1, Data, Fig);
                    [rep, flag] = my_questdlg('Do you really want to delete this image ?');
                    if flag && (rep == 1)
                        delete(nomFicImage)
                        nomFicImage = strrep(nomFicImage, '.tif', '.png');
                        if exist(nomFicImage, 'file')
                            delete(nomFicImage);
                        end
                    end
                    nomFicImage = displayOneEchogram(k, nomDr1, nomFic1, Data, Fig);
                    
                case 'r'
                    nomFicImage = displayOneEchogram(k, nomDr1, nomFic1, Data, Fig);
                    [rep, flag] = my_questdlg('Do you really want to rename this image ?');
                    if flag && (rep == 1)
                        [nomDir2, nomImage2] = fileparts(nomFicImage);
                        
                        NomFicImage2  = fullfile(nomDir2, [nomImage2, '.tif']);
                        if exist(NomFicImage2, 'file')
                            nomFicImageNew = strrep(NomFicImage2, '.tif', '-tif.bad');
                            my_rename(NomFicImage2, nomFicImageNew)
                        end
                        
                        NomFicImage2  = fullfile(nomDir2, [nomImage2, '.png']);
                        if exist(NomFicImage2, 'file')
                            nomFicImageNew = strrep(NomFicImage2, '.png', '-png.bad');
                            my_rename(NomFicImage2, nomFicImageNew)
                        end
                    end
                    nomFicImage = displayOneEchogram(k, nomDr1, nomFic1, Data, Fig);
                    
                case 'leftarrow'
                    k = k - 1;
                    k = max(k, 1);
                    nomFicImage = displayOneEchogram(k, nomDr1, nomFic1, Data, Fig);
                    
                case 'rightarrow'
                    k = k + 1;
                    k = min(k, nbPings);
                    nomFicImage =  displayOneEchogram(k, nomDr1, nomFic1, Data, Fig);
                    
                case 'pagedown'
                    k = nbPings;
                    nomFicImage = displayOneEchogram(k, nomDr1, nomFic1, Data, Fig);
                    
                case 'pageup'
                    k = 1;
                    nomFicImage = displayOneEchogram(k, nomDr1, nomFic1, Data, Fig);
                    
                case {'n'; 'p'}
                    p    = ClParametre('Name', 'Ping number', 'MinValue', 1, 'MaxValue', nbPings, 'Value', k, 'Format', '%d');
                    a = StyledParametreDialog('params', p, 'Title', 'Select a particular ping number');
                    a.sizes = [0    -2   0    -2    0    -1    -1    0];
                    a.openDialog;
                    flag = a.okPressedOut;
                    if ~flag
                        return
                    end
                    k = a.getParamsValue;
                    nomFicImage = displayOneEchogram(k, nomDr1, nomFic1, Data, Fig);
                    
                case {'q'; 'escape'}
                    close(hFig)
                    
                case 's'
                    nomFicImage = editOneEchogram(k, nomDr1, nomFic1, Data, Fig);
                    
                case 'downarrow'
                    k = k + 10;
                    k = min(k, nbPings);
                    nomFicImage =  displayOneEchogram(k, nomDr1, nomFic1, Data, Fig);
                    
                case 'uparrow'
                    k = k - 10;
                    k = max(k, 1);
                    nomFicImage = displayOneEchogram(k, nomDr1, nomFic1, Data, Fig);
                    
                case 'z'
                    nomFicImage = displayOneEchogram(k, nomDr1, nomFic1, Data, Fig);
                    [nomDir2, nomImage2] = fileparts(nomFicImage);
                    NomFicImage2  = fullfile(nomDir2, [nomImage2, '-tif.bad']);
                    NomFicImage3  = fullfile(nomDir2, [nomImage2, '-png.bad']);
                    if exist(NomFicImage2, 'file') || exist(NomFicImage3, 'file')
                        [rep, flag] = my_questdlg('Do you really want to restablish this image ?');
                        if flag && (rep == 1)
                            [nomDir2, nomImage2] = fileparts(nomFicImage);
                            NomFicImage2  = fullfile(nomDir2, [nomImage2, '-tif.bad']);
                            if exist(NomFicImage2, 'file')
                                nomFicImageNew = strrep(NomFicImage2, '-tif.bad', '.tif');
                                my_rename(NomFicImage2, nomFicImageNew)
                            end
                            
                            NomFicImage2  = fullfile(nomDir2, [nomImage2, '-png.bad']);
                            if exist(NomFicImage2, 'file')
                                nomFicImageNew = strrep(NomFicImage2, '-png.bad', '.png');
                                my_rename(NomFicImage2, nomFicImageNew)
                            end
                        end
                        nomFicImage = displayOneEchogram(k, nomDr1, nomFic1, Data, Fig);
                    end
                    
                otherwise
                    kData.Key
            end
        end
        
        function nomFicImage = displayOneEchogram(k, nomDr1, nomFic1, Data, Fig)
            nomFicImage = sprintf('%s - Slice %d', nomFic1, k);
            if strcmp(Ext, '.xml')
                [IRaw, x, z, CLim, nomFicImage] = readPingWCDXML(nomDr1, nomFic1, k);
                
                figure(Fig); axis(hAxeRaw);
%                 Title = sprintf('%s\niPing %d/%d', nomFicImage, k, nbPings);
                if strcmp(Ext, '.xml')
                    Title = sprintf('%s\niPing %d/%d', nomFic1, k, nbPings);
                else
                ind = strfind(nomFicXml, '\');
                    Title = sprintf('%s\niPing %d/%d', nomFicImage((ind(end)+1):end), k, nbPings);
                end
                
                if isempty(hAxeRaw) || ~ishandle(hAxeRaw) || isempty(hImageRaw) || ~ishandle(hImageRaw) || isempty(get(hImageRaw, 'CData'))
                    hImageRaw = imagesc(hAxeRaw, x, z, IRaw);
                    axis xy; axis tight;
                    colormap(jet(256));
                    if ~isempty(CLim')
                        set(get(hImageRaw, 'parent'), 'CLim', CLim)
                    end
                    hcRaw = colorbar;
                    set(hcRaw, 'ButtonDownFcn', {@clbkColorbar; hAxeRaw}, 'BusyAction', 'cancel', 'Interruptible', 'Off')
                else
                    set(hImageRaw, 'CData', IRaw);
                end
                title(Title, 'Interpreter', 'none');
                drawnow
            else
                flagRaw  = isfield(Data, 'reflectivityraw');
                flagComp = isfield(Data, 'reflectivitycomp');
                if flagRaw
                    IRaw  = Data.reflectivityraw(:,:,k);
                    x = 1:size(IRaw,2);
                    z = linspace(Data.elevation(1,1,k), Data.elevation(2,1,k), size(IRaw,1));
                    IRaw  = flipud(IRaw);
                end
                if flagComp
                    IComp = Data.reflectivitycomp(:,:,k);
                    x = 1:size(IComp,2);
                    z = linspace(Data.elevation(1,1,k), Data.elevation(2,1,k), size(IComp,1));
                    IComp = flipud(IComp);
                end
                
                figure(Fig);
                Title = sprintf('%s\niPing %d/%d', nomFic1, k, nbPings);
                
                axis(hAxeRaw);
                if flagRaw
                    if isempty(hAxeRaw) || ~ishandle(hAxeRaw) || isempty(hImageRaw) || ~ishandle(hImageRaw) || isempty(get(hImageRaw, 'CData'))
                        hImageRaw = imagesc(hAxeRaw, x, z, IRaw);
                        axis xy; axis tight;
                        colormap(jet(256));
                        set(hAxeRaw, 'YDir', 'normal');  % Pourquoi on est oblig� de faire �a  ici ?
                        hcRaw = colorbar(hAxeRaw);
                        set(hcRaw, 'ButtonDownFcn', {@clbkColorbar; hAxeRaw}, 'BusyAction', 'cancel', 'Interruptible', 'Off')
                    else
                        set(hImageRaw, 'CData', IRaw);
                    end
                    title(hAxeRaw, Title, 'Interpreter', 'none');
                end
                
                if flagComp
                    if isempty(hAxeComp) || ~ishandle(hAxeComp) || isempty(hImageComp) || ~ishandle(hImageComp) || isempty(get(hImageComp, 'CData'))
                        axis(hAxeComp);
                        hImageComp = imagesc(hAxeComp, x, z, IComp);
                        axis xy; axis tight;
                        colormap(jet(256));
                        CLimComp = [-5 25];
                        if ~isempty(CLimComp')
                            set(get(hImageComp, 'parent'), 'CLim', CLimComp)
                        end
                        hcComp = colorbar(hAxeComp);
                        set(hcComp, 'ButtonDownFcn', {@clbkColorbar; hAxeComp}, 'BusyAction', 'cancel', 'Interruptible', 'Off')
                    else
                        set(hImageComp, 'CData', IComp);
                    end
                    title(hAxeComp, Title, 'Interpreter', 'none');
                end
                drawnow
            end
        end
        
        
        function nomFicImage = editOneEchogram(k, nomDr1, nomFic1, Data, Fig)
            if strcmp(Ext, '.xml')
                [IRaw, x, z, ~, nomFicImage] = readPingWCDXML(nomDr1, nomFic1, k);
                a = cl_image('Image', IRaw, 'x', double(x), 'y', double(z), 'Name', nomFicImage);
            else
                IRaw  = Data.reflectivityraw(:,:,k);
                IComp = Data.reflectivitycomp(:,:,k);
                x = 1:size(IRaw,2);
                z = linspace(Data.elevation(1,1,k), Data.elevation(2,1,k), size(IRaw,1));
                
                IRaw  = flipud(IRaw);
                IComp = flipud(IComp);
                
                nomFicImage = sprintf('%s - Slice %d', nomFic1, k);
                a    = cl_image('Image', IRaw,  'x', double(x), 'y', double(z), 'Name', [nomFicImage '-Raw']);
                a(2) = cl_image('Image', IComp, 'x', double(x), 'y', double(z), 'Name', [nomFicImage '-Comp']);
            end
            
            a = SonarScope(a); %#ok<NASGU>
            
%             IRaw = get_Image(a(end));
%             export_ImageTif32bits(IRaw, nomFicImage, 'Mute', 1);
            
            nomFicImage = displayOneEchogram(k, nomDr1, nomFic1, Data, Fig);
        end
        
        
        function my_closereq(src, callbackdata) %#ok<INUSD>
            my_close(figShortucs)
            my_close(src)
        end
        
        
        function [IRaw, x, z, CLim, nomFicImage] = readPingWCDXML(nomDr1, nomFic1, kPing)
            iDir = floor(kPing/100);
            nomDir2 = num2str(iDir, '%03d');
            nomFicImage = fullfile(nomDr1, nomFic1, nomDir2, sprintf('%05d.tif', kPing));
            flagImageTrouvee = 1;
            if ~exist(nomFicImage, 'file')
                nomFicImage = fullfile(nomDr1, nomFic1, nomDir2, sprintf('%05d.png', kPing));
                if ~exist(nomFicImage, 'file')
                    flagImageTrouvee = 0;
                end
            end
            Info = imfinfo(nomFicImage);
            if flagImageTrouvee
                IRaw = imread(nomFicImage);
                IRaw((IRaw < Info.SMinSampleValue) | (IRaw > Info.SMaxSampleValue)) = NaN;
                
                Depth = linspace(Data.DepthBottom(1), Data.DepthTop(1), Data.Dimensions.nbRows);
                z = linspace(Depth(k), Depth(end), size(IRaw,1));
                x = 1:length(Data.Datetime);
            else
                IRaw = NaN(100, 100, 'single');
%                 z = linspace(Data.Immersion(k), Data.Depth(k), size(IRaw,1));
%                 x = linspace(Data.xBab(k),      Data.xTri(k),  size(IRaw,2));
                Depth = linspace(Data.DepthBottom(1), Data.DepthTop(1), Data.Dimensions.nbRows);
                z = linspace(Depth(k), Depth(end), size(IRaw,1));
                x = 1:length(Data.Datetime);
            end
            
            mots = strsplit(nomFicImage, filesep);
            if  contains(mots{end}, '.tif')
                if contains(mots{end-2}, '_Raw_PolarEchograms')
                    %  CLim = [-64 10];
                    CLim = [];
                elseif contains(mots{end-2}, '_Comp_PolarEchograms')
                    CLim = [-5 30];
                else
                    CLim = [];
                end
            else
                CLim = [];
            end
        end
        
        
        function clbkColorbar(~, ~, hAxe, varargin)
            hImcontrast = my_imcontrast(hAxe); % TODO : le Adjust ne fonctionne pas
            waitfor(hImcontrast)
        end
        
    end
end
