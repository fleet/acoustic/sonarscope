function Points = WC_CleanPointCloud(Points)

% figure(6877); plot(Points.AcrossDistance, Points.Z, '*'); grid

N = length(Points.Z);
sub = false(1, N);
for k=1:N
    D = sqrt((Points.AcrossDistance-Points.AcrossDistance(k)).^2 ...
        + (Points.Z-Points.Z(k)).^2);
    D(k) = Inf;
    sub(k) = ~all(D > 10);
end

Points.Angle          = Points.Angle(sub);
Points.RangeInSamples = Points.RangeInSamples(sub);
Points.Energie        = Points.Energie(sub);
Points.AcrossDistance = Points.AcrossDistance(sub);
Points.Z              = Points.Z(sub);
Points.Latitude       = Points.Latitude(sub);
Points.Longitude      = Points.Longitude(sub);
Points.XCoor          = Points.XCoor(sub);
Points.YCoor          = Points.YCoor(sub);
