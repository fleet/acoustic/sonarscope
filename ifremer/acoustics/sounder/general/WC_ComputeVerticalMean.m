function [Moy, N] = WC_ComputeVerticalMean(I, BW, ExtImage, Unit)
    
J = I;
if strcmp(ExtImage, '.png')
    J = single(J);
    J = J .* BW;
    J(J == 0) = NaN;
    Unit = 'Unknown';
else
    J(~BW) = NaN;
    if isempty(Unit)
        Unit = 'dB';
    end
end
N = sum(BW);

switch Unit
    case 'dB'
        Moy = mean(J, 'omitnan');
    case 'Amp'
        Moy = mean(J, 'omitnan'); % TODO : calculer la moyenne en Amplitude ou en �nergie ???
    case 'Enr'
        Moy = reflec_Enr2dB(mean(reflec_dB2Enr(J), 'omitnan'));
    otherwise
        Moy = mean(J, 'omitnan');
end
