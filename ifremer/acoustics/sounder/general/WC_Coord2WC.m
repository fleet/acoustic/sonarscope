function WC = WC_Coord2WC(Coord, WC, kWC, TransducerDepth, flagTideCorrection, varargin)

[varargin, Detection]        = getPropertyValue(varargin, 'Detection',        []);
[varargin, MinRangeInMeters] = getPropertyValue(varargin, 'MinRangeInMeters', []);
[varargin, Heave]            = getPropertyValue(varargin, 'Heave',            []);
[varargin, nbAcross]         = getPropertyValue(varargin, 'nbAcross',         []); %#ok<ASGLU>

WC.lonBab(kWC)    = Coord.lonBab;
WC.latBab(kWC)    = Coord.latBab;
WC.lonCentre(kWC) = Coord.lonCentre;
WC.latCentre(kWC) = Coord.latCentre;
WC.lonTri(kWC)    = Coord.lonTri;
WC.latTri(kWC)    = Coord.latTri;
WC.iPing(kWC)     = Coord.iPing;
WC.xBab(kWC)      = Coord.xBab;
WC.xTri(kWC)      = Coord.xTri;

% WC.Depth(kWC)     = Coord.Depth - TransducerDepth; % TransducerDepth int�gre le Heave !!!
% WC.Immersion(kWC) = -TransducerDepth;

% Modif JMA le 29/09/2020 pour donn�es AUV
if TransducerDepth > 0
    TransducerDepth = -TransducerDepth;
% else
%     TransducerDepth = TransducerDepth;
end
% TransducerDepth   = -abs(TransducerDepth); % Ajout JMA le 02/10/2020 : TODO : trouver pourquoi on TransducerDepth positif certaines fois et n�gatifs d'autres fois (m�me pour l'AUV)

WC.Depth(kWC)     = Coord.Depth + TransducerDepth; % TransducerDepth int�gre le Heave !!!
WC.Immersion(kWC) = TransducerDepth;

if ~isempty(MinRangeInMeters)
    WC.MinRangeInMeters(kWC) = MinRangeInMeters;
end
if ~isempty(Detection)
    n = length(Detection.RangeInMeters);
    WC.SoundingsRangeInMeters(:,end+1:n) = NaN;
    WC.SoundingsAcrossDist(:,end+1:n)    = NaN;
    WC.SoundingsDepth(:,end+1:n)         = NaN;
    subSoundings = 1:n;
    WC.SoundingsRangeInMeters(kWC,subSoundings) = Detection.RangeInMeters;
    WC.SoundingsAcrossDist(kWC,subSoundings)    = Detection.X;
    WC.SoundingsDepth(kWC,subSoundings)         = Detection.Z;
end

if ~isempty(Heave)
    WC.Heave(kWC) = Heave;
end

%% Prise en compte de la mar�e

if isfield(Coord, 'Tide') && ~isempty(Coord.Tide) % OK pour KM, KOP pour Reson
    TideValue = Coord.Tide(1); % j'ai �t� oblig� de cr�er un signal tide qui contient la m�me valeur. Remplacer cela par un simple scalaire
else
    TideValue = 0;
end

if flagTideCorrection && ~isnan(TideValue)
    Immersion = WC.Immersion(kWC);
    WC.Immersion(kWC) = Immersion + TideValue; % Ajout JMA le 16/10/2016 voir import_WaterColumn_All
    WC.Depth(kWC)     = WC.Depth(kWC) + TideValue; % Ajout JMA le 16/10/2016 voir import_WaterColumn_All
end

WC.Tide(kWC) = TideValue;
WC.Date(kWC) = Coord.Date;
WC.Hour(kWC) = Coord.Heure;

WC.lonBabDepointe(kWC) = Coord.lonBabDepointe;
WC.latBabDepointe(kWC) = Coord.latBabDepointe;
WC.lonTriDepointe(kWC) = Coord.lonTriDepointe;
WC.latTriDepointe(kWC) = Coord.latTriDepointe;

%{
figure(8867); plot(WC.lonBab, WC.latBab, '*r');
grid on; hold on;
plot(WC.lonTri, WC.latTri, '*g');
plot(WC.lonCentre, WC.latCentre, 'ok');
%}

if ~isempty(nbAcross) % N�cessaire quand on cr�e un cube3D
    WC.LatitudeTop(kWC,:)  = linspace(WC.latBab(kWC), WC.latTri(kWC), nbAcross);
    WC.LongitudeTop(kWC,:) = linspace(WC.lonBab(kWC), WC.lonTri(kWC), nbAcross);
end
