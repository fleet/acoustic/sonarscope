function WC_ExportAlongTrackInASCII(nomFicXml, nomDirOut)

if ~iscell(nomFicXml)
    nomFicXml = {nomFicXml};
end

for k=1:length(nomFicXml)
    WC_ExportAlongTrackInASCII_unitaire(nomFicXml{k}, nomDirOut)
end


function WC_ExportAlongTrackInASCII_unitaire(nomFicXml, nomDirOut)

%% Lecture du fichier XML d�crivant la donn�e 

[flag, Data] = XMLBinUtils.readGrpData(nomFicXml, 'Memmapfile', 0);
if ~flag
    return
end

%% Comma or not comma ?

sep = getCSVseparator;

%% Cr�ation du fichier ASCII

[nomDr1, nomFic1] = fileparts(nomFicXml);
nomFicCsv = fullfile(nomDirOut, [nomFic1 '.csv']);
fid = fopen(nomFicCsv, 'w+t');
if fid == -1
    return
end
% Ecritude du header
fprintf(fid, 'Time%sTime UTC%sPing%sLatitude (deg)%sLongitude (deg)%sDepth (m)%sValue (dB)\n', sep, sep, sep, sep, sep, sep);

%% 

LongitudeNadir = Data.LongitudeNadir(:);
LatitudeNadir  = Data.LatitudeNadir(:);
PingNumber     = Data.PingNumber(:);
AcrossDistance = Data.AcrossDistance(:);
LatitudeTop    = Data.LatitudeTop(:,:);
LongitudeTop   = Data.LongitudeTop(:,:);
% DepthTop       = Data.DepthTop(:);
% DepthBottom    = Data.DepthBottom(:);
% Date           = Data.Date(:);
% Hour           = Data.Hour(:);
% SliceExists    = Data.SliceExists(:);

figure; plot(LongitudeTop, LatitudeTop, '.'); grid on; axisGeo
hold on; plot(LongitudeNadir, LatitudeNadir, 'k'); grid on; axisGeo

figure; imagesc(PingNumber, AcrossDistance, LatitudeTop);
colormap(jet(256)); colorbar;
axis xy; title('LatitudeTop'); xlabel('Ping number');  ylabel('Across distance (m)');

figure; imagesc(PingNumber, AcrossDistance, LongitudeTop);
colormap(jet(256)); colorbar;
axis xy; title('LongitudeTop'); xlabel('Ping number');  ylabel('Across distance (m)');

% Depth = linspace(-Data.DepthTop, -Data.DepthBottom, Data.Dimensions.nbRows);
Depth = linspace(Data.DepthBottom(1), Data.DepthTop(1), Data.Dimensions.nbRows);

Fig = figure;
str1 = 'Visualisation des slices';
str2 = 'Display of slices';
hw1 = create_waitbar(Lang(str1,str2), 'N', Data.Dimensions.nbSlices);
for k=1:Data.Dimensions.nbSlices
    my_waitbar(k, Data.Dimensions.nbSlices, hw1)
     
    if isfield(Data, 'Reflectivity')
        I = Data.Reflectivity(:,:,k);
        if all(isnan(I(:)))
            continue
        end
    else
        iDir = floor(k/100);
        nomDir2 = num2str(iDir, '%03d');
        nomFicTif = fullfile(nomDr1, nomFic1, nomDir2, sprintf('%05d.tif', k));
        if ~exist(nomFicTif, 'file')
            continue
        end
        fprintf('%s\n', nomFicTif);
        I = imread(nomFicTif);
    end
    
    figure(Fig);
    Title = sprintf('%s\nSlice %d/%d - Distance : %s m', nomFicXml, k, Data.Dimensions.nbSlices, num2str(Data.AcrossDistance(k)));
    imagesc(Data.PingNumber(:), Depth-Depth(end), I); axis xy; title(Title, 'Interpreter', 'none'); colormap(jet(256)); colorbar;
    drawnow
    
%     Ac = AcrossDistance(k);
    str = sprintf('Export ASCII of %s', nomFic1);
    hw2 = create_waitbar(str, 'N', Data.Dimensions.nbPings);
    for k1=1:Data.Dimensions.nbPings
        my_waitbar(k1, Data.Dimensions.nbPings, hw2)
        PingNumber = Data.PingNumber(k1);
        lat = Data.LatitudeNadir(k1);
        lon = Data.LongitudeNadir(k1);
        t = Data.Datetime(k1);
        strTime = char(t);
        strTimeUnix = num2strPrecis(posixtime(t));
        if isnan(lat) || isnan(lon) || isnan(PingNumber)
            continue
        end
        for k2=1:Data.Dimensions.nbRows
            Value = I(k2,k1);
            if ~isnan(Value)
                z = Depth(k2)-Depth(end);
                fprintf(fid, ' %s%s %s%s %d%s %s%s %s%s %f%s %f\n', ...
                    strTime, sep, strTimeUnix, sep, PingNumber, sep, ...
                    num2strPrecis(lat), sep, num2strPrecis(lon), sep, ...
                    z, sep, Value);
            end
        end
    end
    my_close(hw2, 'MsgEnd');
end
my_close(hw1, 'MsgEnd');
fclose(fid);
