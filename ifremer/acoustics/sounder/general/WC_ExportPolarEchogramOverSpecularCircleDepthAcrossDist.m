function WC_ExportPolarEchogramOverSpecularCircleDepthAcrossDist(nomFicXml, params_ZoneEI, ALim, configExport, Display)  

if ~iscell(nomFicXml)
    nomFicXml = {nomFicXml};
end

nbFic = length(nomFicXml);
hw = create_waitbar('WC-DepthAcrossDist export.', 'N', nbFic);
for kFic=1:nbFic
    my_waitbar(kFic, nbFic, hw)
    WC_Export_unitaire(nomFicXml{kFic}, params_ZoneEI, ALim, configExport, Display);
end
my_close(hw, 'MsgEnd')


function WC_Export_unitaire(nomFicXml, params_ZoneEI, ALim, configExport, Display)

%% Lecture du fichier XML d�crivant la donn�e

flag = exist(nomFicXml, 'file');
if ~flag
    return
end

[~, ~, Ext] = fileparts(nomFicXml);
if strcmp(Ext, '.xml')
    [flag, Data] = XMLBinUtils.readGrpData(nomFicXml, 'Memmapfile', -1);
    if ~flag
        return
    end
    nbPings = Data.Dimensions.nbPings;
    ncID = [];
%     Data.Datetime = datetime(Data.Date(:) + Data.Hour(:) / (24*3600000), 'ConvertFrom', 'juliandate');
    Data.Datetime = timeIfr2Datetime(Data.Date(:), Data.Hour(:)); % TODO / attention, la formule ci-dessus ne fonctionne pas
    LatMean = mean(Data.latCentre, 'omitnan');
    LonMean = mean(Data.lonCentre, 'omitnan');
else
    [flag, Data] = NetcdfGlobe3DUtils.readGlobeFlyTexture(nomFicXml, 'Light', 2);
    if ~flag
        return
    end
    nbPings = length(Data.Datetime);
    ncID = netcdf.open(nomFicXml);
    LatMean = mean(Data.latitude_nadir, 'omitnan');
    LonMean = mean(Data.longitude_nadir, 'omitnan');
end
Data.Datetime.Format = 'yyyy-MM-dd''T''HH:mm:ss.SSS''Z''';

%% Carto

if (configExport == 2) || (configExport == 4)
    Fuseau = floor((LonMean+180)/6 + 1);
    if LatMean >= 0
        H = 'N';
    else
        H = 'S';
    end
    Carto = cl_carto('Ellipsoide.Type', 11, 'Projection.Type', 3, 'Projection.UTM.Fuseau', Fuseau, 'Projection.UTM.Hemisphere', H);
else
    Carto = [];
end

%% Cr�ation du fichier ASCII

sep = getCSVseparator;

[nomDr1, nomFic1] = fileparts(nomFicXml);
nomFicASCII = fullfile(nomDr1, [nomFic1 '.csv']);

%% Visualisation des images

if Display
    Fig = figure;
    ha(1) = subplot(2,1,1);
    ha(2) = subplot(2,1,2);
else
    Fig = [];
    ha  = [];
end

flagFirstTime = 1;
str = sprintf('WC-DepthAcrossDist export %s to ASCII.', nomFic1);
hw = create_waitbar(str, 'N', nbPings);
for k=1:nbPings
    my_waitbar(k, nbPings, hw)
    [flag, TPing] = exportOneEchogram(ncID, k, nomDr1, nomFic1, Data, params_ZoneEI, ALim, configExport, Carto, ha, Display);
    if flag == -1
        break
    end
    if ~flag
        continue
    end
    if isempty(TPing)
        continue
    end
    if flagFirstTime
        flagFirstTime = 0;
        writetable(TPing, nomFicASCII, 'Delimiter', sep);
    else
        writetable(TPing, nomFicASCII, 'Delimiter', sep, 'WriteMode', 'append');
    end
end
my_close(hw, 'MsgEnd')
if ~isempty(ncID)
    netcdf.close(ncID);
end
if Display
    my_close(Fig);
end


function [flag, TPing] = exportOneEchogram(ncID, k, nomDr1, nomFic1, Data, params_ZoneEI, ALim, configExport, Carto, ha, Display)

flag  = 0;

SeuilHaut        = params_ZoneEI.SeuilHaut;
SeuilBas         = params_ZoneEI.SeuilBas;
DistanceSecutity = params_ZoneEI.DistanceSecutity;

if isempty(ncID)
    iDir = floor(Data.iPing(k)/100);
    nomDir2 = num2str(iDir, '%03d');
    nomFicImage = fullfile(nomDr1, nomFic1, nomDir2, sprintf('%05d.tif', Data.iPing(k)));
    if ~exist(nomFicImage, 'file')
        return
    end
    ElevationPointHaut = 0;
    I = imread(nomFicImage);
    % z = linspace(Data.Immersion(k), Data.Depth(k), size(I,1));
    z = linspace(0, Data.Depth(k) - Data.Immersion(k), size(I,1));
    x = linspace(Data.xBab(k),      Data.xTri(k),      size(I,2));
    ImmersionPing = Data.Immersion(k);
    DepthPing     = Data.Depth(k);
    xBabPing      = Data.xBab(k);
    xTriPing      = Data.xTri(k);
    latBabPing    = Data.latBab(k);
    latTriPing    = Data.latTri(k);
    lonBabPing    = Data.lonBab(k);
    lonTriPing    = Data.lonTri(k);
    nbPings       = Data.Dimensions.nbPings;
else
    [flag, DataPing, groupName] = NetcdfGlobe3DUtils.readOneGroup(ncID, k, 'flagSubGroups', true);
    if ~flag
        return
    end
    ElevationPointHaut = DataPing.elevation(1,1);
    nomFicImage = sprintf('%s - Ping %s', nomFic1, groupName);
    I  = flipud(DataPing.raw);
    z = linspace(ElevationPointHaut, DataPing.elevation(2,1), size(I,1));
    x = linspace(DataPing.across_dist_L,  DataPing.across_dist_R,  size(I,2));
    ImmersionPing = 0;
    DepthPing     = DataPing.elevation(2,1);
    xBabPing      = DataPing.across_dist_L;
    xTriPing      = DataPing.across_dist_R;
    latBabPing    = DataPing.latitude(1,1);
    latTriPing    = DataPing.latitude(1,2);
    lonBabPing    = DataPing.longitude(1,1);
    lonTriPing    = DataPing.longitude(1,2);
    nbPings       = length(Data.Datetime);
end

if isfield(Data, 'MinRangeInMeters')
    RayonCercleSpeculaire = Data.MinRangeInMeters(k);
else % TODO : il faut renseigner les WC Nc
    if isfield(DataPing, 'SoundingsRange')
        RayonCercleSpeculaire = min(DataPing.SoundingsRange);
    else
        str = sprintf('Sorry, the file "%s" is too old, you have to rebuild it.', nomFic1);
        my_warndlg(str, 0, 'Tag', 'ExportSamplesImpossible', 'displayItEvenIfSSc', 1);
        flag = -1;
        return
    end
end

[X,Z] = meshgrid(x,z-ElevationPointHaut);
Angle = 90 + atan2d(Z, X);
Range = sqrt(X.^2 + Z.^2);
RayonCercleSpeculaireBas  = RayonCercleSpeculaire * SeuilBas;
RayonCercleSpeculaireHaut = RayonCercleSpeculaire * SeuilHaut;
Angles = (ALim(1):1:ALim(2)) - 90;
anneauBas.x  = cosd(Angles) * RayonCercleSpeculaireBas;
anneauBas.y  = sind(Angles) * RayonCercleSpeculaireBas + ImmersionPing;
anneauHaut.x = cosd(Angles) * RayonCercleSpeculaireHaut;
anneauHaut.y = sind(Angles) * RayonCercleSpeculaireHaut + ImmersionPing;
anneau.x = [anneauBas.x fliplr(anneauHaut.x) anneauBas.x(1)];
anneau.y = [anneauBas.y fliplr(anneauHaut.y) anneauBas.y(1)];
Mask = (Range < (RayonCercleSpeculaire - DistanceSecutity)) ...
    & (Range < RayonCercleSpeculaireBas) & (Range > RayonCercleSpeculaireHaut) ...
    & (Angle >= ALim(1)) & (Angle <= ALim(2)) ...
    & ~isnan(I);

if Display && (mod(k-1, Display) == 0)
    displayOneEchogram(I, Mask, k, nbPings, nomFicImage, ha, anneau, ImmersionPing, ElevationPointHaut, DepthPing, xBabPing, xTriPing)
end

xBase   = double([xBabPing   xTriPing]);
latBase = [latBabPing latTriPing];
lonBase = [lonBabPing lonTriPing];

strTime = char(Data.Datetime(k));

% fid = fopen(nomFicASCII, 'a+t');
subMAsk = find(Mask);

Lat = interp1(xBase, latBase, double(X(:)));
Lon = interp1(xBase, lonBase,  double(X(:)));
% figure; plot(Lon, Lat, '.'); grid on;

if (configExport == 2) || (configExport == 4)
    [x, y, flag] = latlon2xy(Carto, Lat, Lon);
    if ~flag
        return
    end
end

nbPoints = length(subMAsk);
[tableVarNames, tableVarTypes] = defineTableVarNames(configExport);
sz = [nbPoints length(tableVarNames)];
TPing = table('Size', sz, 'VariableTypes', tableVarTypes, 'VariableNames', tableVarNames);

switch configExport
    case 1
        TPing.PingNumber     = repmat(k, nbPoints,1);
        TPing.Time           = repmat(strTime, nbPoints,1);
        TPing.AcrossDistance = single(X(subMAsk));
        TPing.Depth          = single(Z(subMAsk));
        TPing.Value          = single(I(subMAsk));
        TPing.Lat            = Lat(subMAsk);
        TPing.Lon            = Lon(subMAsk);
    case 2
        TPing.PingNumber     = repmat(k, nbPoints,1);
        TPing.Time           = repmat(strTime, nbPoints,1);
        TPing.AcrossDistance = single(X(subMAsk));
        TPing.Depth          = single(Z(subMAsk));
        TPing.Value          = single(I(subMAsk));
        TPing.x              = x(subMAsk);
        TPing.y              = y(subMAsk);
    case 3
        TPing.Depth = single(Z(subMAsk));
        TPing.Value = single(I(subMAsk));
        TPing.Lat   = Lat(subMAsk);
        TPing.Lon   = Lon(subMAsk);
    case 4
        TPing.Depth = single(Z(subMAsk));
        TPing.Value = single(I(subMAsk));
        TPing.x     = x(subMAsk);
        TPing.y     = y(subMAsk);
end
% head(TPing)

% I(~Mask) = NaN;
% SonarScope(I)

flag = 1;



function displayOneEchogram(I, Mask, k, nbPings, nomFicImage, ha, anneau, ImmersionPing, ElevationPointHaut, DepthPing, xBabPing, xTriPing)

mots = strsplit(nomFicImage, filesep);
if contains(mots{end}, '.tif')
    if contains(mots{end-2}, '_Raw_PolarEchograms')
        CLim = [];
    elseif contains(mots{end-2}, '_Comp_PolarEchograms')
        CLim = [-5 30];
    else
        CLim = [];
    end
else
    CLim = [];
end

axes(ha(1)); hold off;
Title = sprintf('%s\niPing %d/%d', nomFicImage, k, nbPings);
z = linspace(ImmersionPing+ElevationPointHaut, DepthPing, size(I,1));
x = linspace(xBabPing,      xTriPing,  size(I,2));
hImage = imagesc(x, z, I); axis xy; title(Title, 'Interpreter', 'none'); colormap(jet(256)); colorbar; axis equal; axis tight;
if ~isempty(CLim')
    set(get(hImage, 'parent'), 'CLim', CLim)
end
hold on; plot(anneau.x, anneau.y+ElevationPointHaut, 'r');

axes(ha(2)); hold off;
I(~Mask) = NaN;
imagesc(x, z, I); axis xy; title('Echo Integration ring'); colormap(jet(256)); colorbar; axis equal; axis tight;
hold on; plot(anneau.x, anneau.y+ElevationPointHaut, 'r');

drawnow


function [tableVarNames, tableVarTypes] = defineTableVarNames(configExport)
switch configExport
    case 1
        tableVarTypes = {'single'; 'datetime'; 'single'; 'single'; 'single'; 'double'; 'double'};
        tableVarNames = {'PingNumber'; 'Time'; 'AcrossDistance'; 'Depth'; 'Value'; 'Lat'; 'Lon'};
        
    case 2
        tableVarTypes = {'single'; 'datetime'; 'single'; 'single'; 'single'; 'double'; 'double'};
        tableVarNames = {'PingNumber'; 'Time'; 'AcrossDistance'; 'Depth'; 'Value'; 'x'; 'y'};
        
    case 3
        tableVarTypes = {'single'; 'single'; 'double'; 'double'};
        tableVarNames = {'Depth'; 'Value'; 'Lat'; 'Lon'};
        
    case 4
        tableVarTypes = {'single'; 'single'; 'double'; 'double'};
        tableVarNames = {'Depth'; 'Value'; 'x'; 'y'};
end
