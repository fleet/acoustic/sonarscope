function WC_ExportPolarEchogramOverSpecularCircleDepthAcrossDistR2019b(nomFicXml, params_ZoneEI, ALim, configExport, Display) 

if ~iscell(nomFicXml)
    nomFicXml = {nomFicXml};
end

nbFic = length(nomFicXml);
hw = create_waitbar('WC-DepthAcrossDist export.', 'N', nbFic);
for kFic=1:nbFic
    my_waitbar(kFic, nbFic, hw)
    WC_Export_unitaire(nomFicXml{kFic}, params_ZoneEI, ALim, configExport, Display);
end
my_close(hw, 'MsgEnd')


function WC_Export_unitaire(nomFicXml, params_ZoneEI, ALim, configExport, Display)

%% Lecture du fichier XML d�crivant la donn�e

flag = exist(nomFicXml, 'file');
if ~flag
    return
end

[~, ~, Ext] = fileparts(nomFicXml);
if strcmp(Ext, '.xml')
    [flag, Data] = XMLBinUtils.readGrpData(nomFicXml, 'Memmapfile', -1);
    if ~flag
        return
    end
    nbPings = Data.Dimensions.nbPings;
    ncID = [];
else
    [flag, Data] = NetcdfGlobe3DUtils.readGlobeFlyTexture(nomFicXml, 'Light', 2);
    if ~flag
        return
    end
    nbPings = length(Data.Datetime);
    ncID = netcdf.open(nomFicXml);
end

%% Carto

if (configExport == 2) || (configExport == 4) 
    LatMean = mean(Data.latCentre, 'omitnan');
    LonMean = mean(Data.lonCentre, 'omitnan');
    
    Fuseau = floor((LonMean+180)/6 + 1);
    if LatMean >= 0
        H = 'N';
    else
        H = 'S';
    end
    Carto = cl_carto('Ellipsoide.Type', 11, 'Projection.Type', 3, 'Projection.UTM.Fuseau', Fuseau, 'Projection.UTM.Hemisphere', H);
else
    Carto = [];
end

%% Cr�ation du fichier ASCII

[nomDr1, nomFic1] = fileparts(nomFicXml);
nomFicASCII = fullfile(nomDr1, [nomFic1 '.csv']);
% {
fid = fopen(nomFicASCII, 'w+t');
if fid == -1
    return
end

% Ecritude du header

switch configExport
    case 1
        fprintf(fid, 'Ping Number; Time; Across Distance (m); Depth (m); Value (dB); Lat (deg); Lon (deg)\n');
    case 2
        fprintf(fid, 'Ping Number; Time; Across Distance (m); Depth (m); Value (dB); x (m); y (m)\n');
    case 3
        fprintf(fid, 'Depth (m); Value (dB); Lat (deg); Lon (deg)\n');
    case 4
        fprintf(fid, 'Depth (m); Value (dB); x (m); y (m)\n');
end
fclose(fid);
% }

%% Visualisation des images

if Display
    Fig = figure;
    ha(1) = subplot(2,1,1);
    ha(2) = subplot(2,1,2);
else
    Fig = [];
    ha  = [];
end

str = sprintf('WC-DepthAcrossDist export %s to ASCII.', nomFic1);
hw = create_waitbar(str, 'N', nbPings);
for k=1:nbPings
    my_waitbar(k, nbPings, hw)
    exportOneEchogram(ncID, k, nomDr1, nomFic1, Data, nomFicASCII, params_ZoneEI, ALim, configExport, Carto, ha, Display);
end
my_close(hw, 'MsgEnd')
if ~isempty(ncID)
    netcdf.close(ncID);
end
if Display
    my_close(Fig);
end


function flag = exportOneEchogram(ncID, k, nomDr1, nomFic1, Data, nomFicASCII, params_ZoneEI, ALim, configExport, Carto, ha, Display)

flag = 0;

SeuilHaut        = params_ZoneEI.SeuilHaut;
SeuilBas         = params_ZoneEI.SeuilBas;
DistanceSecutity = params_ZoneEI.DistanceSecutity;

if isempty(ncID)
    iDir = floor(Data.iPing(k)/100);
    nomDir2 = num2str(iDir, '%03d');
    nomFicImage = fullfile(nomDr1, nomFic1, nomDir2, sprintf('%05d.tif', Data.iPing(k)));
    if ~exist(nomFicImage, 'file')
        return
    end
    I = imread(nomFicImage);
    % z = linspace(Data.Immersion(k), Data.Depth(k), size(I,1));
    z = linspace(0, Data.Depth(k) - Data.Immersion(k), size(I,1));
    x = linspace(Data.xBab(k),      Data.xTri(k),  size(I,2));
else
    [flag, DataPing, groupName] = NetcdfGlobe3DUtils.readOneGroup(ncID, k);
    if ~flag
        return
    end
    nomFicImage =  sprintf('%s - Ping %s', nomFic1, groupName);
    I  = (DataPing.raw);
    % z = linspace(Data.Immersion(k), Data.Depth(k), size(I,1));
    my_breakpoint % Poursuivre la mise au point
    z = linspace(0, Data.Depth(k) - Data.Immersion(k), size(I,1));
    x = linspace(Data.xBab(k),      Data.xTri(k),  size(I,2));
end

if isfield(Data, 'MinRangeInMeters')
    RayonCercleSpeculaire = Data.MinRangeInMeters(k);
else
    RayonCercleSpeculaire = Inf;
end

[X,Z] = meshgrid(x,z);
Angle = 90 + atan2d(Z, X);
Range = sqrt(X.^2 + Z.^2);
RayonCercleSpeculaireBas  = RayonCercleSpeculaire * SeuilBas;
RayonCercleSpeculaireHaut = RayonCercleSpeculaire * SeuilHaut;
Angles = (ALim(1):1:ALim(2)) - 90;
anneauBas.x  = cosd(Angles) * RayonCercleSpeculaireBas;
anneauBas.y  = sind(Angles) * RayonCercleSpeculaireBas + Data.Immersion(k);
anneauHaut.x = cosd(Angles) * RayonCercleSpeculaireHaut;
anneauHaut.y = sind(Angles) * RayonCercleSpeculaireHaut + Data.Immersion(k);
anneau.x = [anneauBas.x fliplr(anneauHaut.x) anneauBas.x(1)];
anneau.y = [anneauBas.y fliplr(anneauHaut.y) anneauBas.y(1)];
Mask = (Range < (RayonCercleSpeculaire - DistanceSecutity)) ...
    & (Range < RayonCercleSpeculaireBas) & (Range > RayonCercleSpeculaireHaut) ...
    & (Angle >= ALim(1)) & (Angle <= ALim(2)) ...
    & ~isnan(I);

if Display && (mod(k-1, Display) == 0)
    displayOneEchogram(I, Mask, k, nomFicImage, Data, ha, anneau)
end

xBase   = double([Data.xBab(k)   Data.xTri(k)]);
latBase = [Data.latBab(k) Data.latTri(k)];
lonBase = [Data.lonBab(k) Data.lonTri(k)];

% t = datetime(Data.Date(:) + Data.Hour(:) / (24*3600000), 'ConvertFrom', 'juliandate');
t = timeIfr2Datetime(Data.Date(:), Data.Hour(:)); % TODO / attention, la formule ci-dessus ne fonctionne pas
t.Format='yyyy-MM-dd''T''HH:mm:ss.SSS''Z''';
strTime = char(t(k));

fid = fopen(nomFicASCII, 'a+t');
subMAsk = find(Mask);

Lat = interp1(xBase, latBase, double(X(:)));
Lon = interp1(xBase, lonBase,  double(X(:)));
% figure; plot(Lon, Lat, '.'); grid on;

if (configExport == 2) || (configExport == 4) 
    [x, y, flag] = latlon2xy(Carto, Lat, Lon);
    if ~flag
        fclose(fid);
        return
    end
end


for k1=1:length(subMAsk)
    k2 = subMAsk(k1); % Nore mail du 22/04/2021 : Depth Decibels et Lat Long ou UTM
    
    switch configExport
        case 1
            fprintf(fid, '%d; %s; %f; %f; %f; %s; %s\n', k, strTime, X(k2), Z(k2), I(k2), num2strPrecis(Lat(k2)), num2strPrecis(Lon(k2)));
        case 2
            fprintf(fid, '%d; %s; %f; %f; %f; %0.2f; %0.2f\n', k, strTime, X(k2), Z(k2), I(k2), x(k2), y(k2));
        case 3
            fprintf(fid, '%f; %f; %s; %s\n', Z(k2), I(k2), num2strPrecis(Lat(k2)), num2strPrecis(Lon(k2)));
        case 4
            fprintf(fid, '%f; %f; %0.2f; %0.2f\n', Z(k2), I(k2), x(k2), y(k2));
    end
end
fclose(fid);

% I(~Mask) = NaN;
% SonarScope(I)

flag = 1;



function displayOneEchogram(I, Mask, k, nomFicImage, Data, ha, anneau)

mots = strsplit(nomFicImage, filesep);
if contains(mots{end}, '.tif')
    if contains(mots{end-2}, '_Raw_PolarEchograms')
        CLim = [];
    elseif contains(mots{end-2}, '_Comp_PolarEchograms')
        CLim = [-5 30];
    else
        CLim = [];
    end
else
    CLim = [];
end

axes(ha(1)); hold off;
Title = sprintf('%s\niPing %d/%d', nomFicImage, k, Data.Dimensions.nbPings);
z = linspace(Data.Immersion(k), Data.Depth(k), size(I,1));
x = linspace(Data.xBab(k),      Data.xTri(k),  size(I,2));
hImage = imagesc(x, z, I); axis xy; title(Title, 'Interpreter', 'none'); colormap(jet(256)); colorbar; axis equal; axis tight;
if ~isempty(CLim')
    set(get(hImage, 'parent'), 'CLim', CLim)
end
hold on; plot(anneau.x, anneau.y, 'r');

axes(ha(2)); hold off;
I(~Mask) = NaN;
imagesc(x, z, I); axis xy; title('Echo Integration ring'); colormap(jet(256)); colorbar; axis equal; axis tight;
hold on; plot(anneau.x, anneau.y, 'r');

drawnow
