function [flag, nomFic2, ExtImage] = WC_FindImageName(iPing, nomDirRaw)

nomSousRepertoire = sprintf('%03d', floor(iPing/100));
nomDir3 = fullfile(nomDirRaw, nomSousRepertoire);
ExtImage = '.tif';
nomFic1 = sprintf('%05d%s', iPing, ExtImage);
nomFic2 = fullfile(nomDir3, nomFic1);
if ~exist(nomFic2, 'file')
    ExtImage = '.png';
    nomFic1 = sprintf('%05d%s', iPing, ExtImage);
    nomFic2 = fullfile(nomDir3, nomFic1);
end
if ~exist(nomFic2, 'file')
    flag = 0;
    return
end

flag = 1;
