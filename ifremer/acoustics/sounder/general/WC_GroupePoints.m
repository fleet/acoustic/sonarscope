function PointsOut = WC_GroupePoints(PointsIn)

PointsOut.Longitude = [PointsIn(:).Longitude];
PointsOut.Latitude  = [PointsIn(:).Latitude];
if isfield(PointsIn, 'XCoor')
    PointsOut.XCoor = [PointsIn(:).XCoor];
    PointsOut.YCoor = [PointsIn(:).YCoor];
end
PointsOut.Z              = [PointsIn(:).Z];
PointsOut.Angle          = [PointsIn(:).Angle];
PointsOut.RangeInSamples = [PointsIn(:).RangeInSamples];
PointsOut.Energie        = [PointsIn(:).Energie];
PointsOut.AcrossDistance = [PointsIn(:).AcrossDistance];
PointsOut.Angle          = [PointsIn(:).Angle];

for k=1:length(PointsIn)
    n = length(PointsIn(k).Longitude);
    PointsIn(k).iPingTab         = repmat(PointsIn(k).iPing, 1, n);
    PointsIn(k).EnergieTotaleTab = repmat(PointsIn(k).EnergieTotale, 1, n);
    PointsIn(k).DateTab          = repmat(PointsIn(k).Date, 1, n);
    PointsIn(k).HourTab          = repmat(PointsIn(k).Heure, 1, n);
    PointsIn(k).CeleriteTab      = repmat(PointsIn(k).Celerite, 1, n);
end
PointsOut.iPing         = [PointsIn(:).iPingTab];
PointsOut.EnergieTotale = [PointsIn(:).EnergieTotaleTab];
PointsOut.Date          = [PointsIn(:).DateTab];
PointsOut.Hour          = [PointsIn(:).HourTab];
PointsOut.Celerite      = [PointsIn(:).CeleriteTab];
