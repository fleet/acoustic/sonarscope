function flag = WC_HorizontalSlices(nomFicXml, GridSize, Step, Tag, nomDirOut)

if ischar(nomFicXml)
    nomFicXml = {nomFicXml};
end

N   = length(nomFicXml);
str1 = 'Cr�ation des Slices horizontales';
str2 = 'Processing horizontal WC slices';
hw  = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:length(nomFicXml)
    my_waitbar(k, N, hw)
    flag = WC_HorizontalSlices_unitaire(nomFicXml{k}, GridSize, Step, Tag, nomDirOut);
end
my_close(hw, 'MsgEnd')


function flag = WC_HorizontalSlices_unitaire(nomFicXml, GridSize, Step, Tag, nomDirOut)

%% Lecture du fichier XML d�crivant la donn�e 

[nomDir, nomFic, Ext] = fileparts(nomFicXml);
nomFicNc = fullfile(nomDir, [nomFic '.nc']);
str1 = sprintf('Lecture du fichier "%s"', nomFicNc);
str2 = sprintf('Reading file "%s"', nomFicNc);
WorkInProgress(Lang(str1,str2))
if exist(nomFicNc, 'file')
    %         [flag, Data3DMatrix] = WC_readImageCube3D(nomFicXml, 'Memmapfile', 1); % Retester cet appel
    [flag, Data3DMatrix] = NetcdfUtils.readGrpData(nomFicNc, 'WaterColumnCube3D'); % 'PolarEchograms');
    if ~flag
        return
    end
    ReflectivityRaw  = Data3DMatrix.ReflectivityRaw(:,:,:);
    ReflectivityComp = Data3DMatrix.ReflectivityComp(:,:,:);
else
    [flag, Data3DMatrix] = XMLBinUtils.readGrpData(nomFicXml, 'Memmapfile', 0); % Reflectivity: [446�1437�871 cl_memmapfile]
    if ~flag
        return
    end
    ReflectivityRaw  = Data3DMatrix.Reflectivity(:,:,:);
    ReflectivityComp = [];
end

%{
[nomDirXml, nomFicSeul, Ext] = fileparts(nomFicXml);
nomDirXml = fullfile(nomDirXml, [nomFicSeul '.zip']);
my_unzip(nomDirXml);

if strcmp(Ext, '.nc')
    if contains(nomFicXml, 'Raw')
        nomFicXmlRaw = nomFicXml;
        nomFicXmlComp = strrep(nomFicXml, 'Raw', 'Comp');
    elseif contains(nomFicXml, 'Comp')
        nomFicXmlRaw = strrep(nomFicXml, 'Comp', 'Raw');
        nomFicXmlComp = nomFicXml;
    else
        nomFicXmlRaw  = nomFicXml;
        nomFicXmlComp = [];
    end
    
%         [flag, Data3DMatrix] = WC_readImageCube3D(nomFicXml, 'Memmapfile', 1); % Retester cet appel
    [flag, Data3DMatrix] = NetcdfUtils.readGrpData(nomFicXmlRaw, 'PolarEchograms');
    if ~flag
        return
    end
    Data3DMatrix.ReflectivityRaw = Data3DMatrix.Reflectivity(:,:,:);
    
%         [flag, Data3DMatrix] = WC_readImageCube3D(nomFicXml, 'Memmapfile', 1); % Retester cet appel
    [flag, Data3DMatrixComp] = NetcdfUtils.readGrpData(nomFicXmlComp, 'PolarEchograms');
    if ~flag
        return
    end
    Data3DMatrix.ReflectivityComp = Data3DMatrixComp.Reflectivity(:,:,:);
    clear Data3DMatrixComp
else
    [flag, Data3DMatrix] = WC_readImageCube3D(nomFicXml, 'Memmapfile', 1);
    if ~flag
        return
    end
end
%}

if numel(Data3DMatrix.LatitudeTop) == 0
    str1 = sprintf('Echec de la cr�ation de slices pour le fichier  %s', nomFicXml);
    str2 = sprintf('Slice creation failed for  %s', nomFicXml);
    my_warndlg(Lang(str1,str2), 1);
    flag = 0;
    return
end

% bins = double(Data3DMatrix.Depth(end):-Step:double(Data3DMatrix.Depth(1))); % Reintroduit par JMA le 31/010/2018
% Z = linspace(min(Data3DMatrix.Depth), max(Data3DMatrix.Immersion), size(Data3DMatrix.ReflectivityRaw, 1));
bins = double(min(Data3DMatrix.Depth):Step:double(max(Data3DMatrix.Immersion)));
binsCentre = centrage_magnetique(bins) - Step/2;

binsDeb = binsCentre + Step/2;
binsDeb(end+1) = binsDeb(end) + Step;
binsDeb = fliplr(binsDeb);
nbSlices = length(binsCentre);

%% Cr�ation du r�pertoire

[~, nomFic] = fileparts(nomFicXml);
nomFic = strrep(nomFic, '_PolarEchograms', '');
nomFic = strrep(nomFic, '_WCCube', '_WCHorizontalSlice');
nomFic = strrep(nomFic, '_Cube', '_WCHorizontalSlice');
nomFic = strrep(nomFic, '__', '_');
nomDirXml = fullfile(nomDirOut, nomFic);
if ~exist(nomDirXml, 'dir')
    flag = mkdir(nomDirXml);
    if ~flag
        messageErreurFichier(nomDirXml, 'WriteFailure');
        return
    end
end

if strcmp(Ext, '.nc')
    
    %% Calcul des slices horizontales Raw
    
    Data3DMatrix.Reflectivity = ReflectivityRaw;
    if isempty(Tag)
        nomFicMultiImageXML = fullfile(nomDirOut, [nomFic '-MLRaw.xml']);
    else
        nomFicMultiImageXML = fullfile(nomDirOut, [nomFic '_' Tag '-MLRaw.xml']);
    end
    [flag, Reflectivity_LatLong] = WC_HorizontalSlices_OneReflectivity(nbSlices, Data3DMatrix, binsDeb, GridSize);
    if flag
        % Export des images en Multidata pour Globe3DV
        sizeTiff = 32000; %2500;
        flag = export_3DVMultiImages(Reflectivity_LatLong, nomFicMultiImageXML, 'sizeTiff', sizeTiff);
        if ~flag
            return
        end
    end
    
    %% Calcul des slices horizontales Comp
    
    Data3DMatrix.Reflectivity = ReflectivityComp;
    if isempty(Tag)
        nomFicMultiImageXML = fullfile(nomDirOut, [nomFic '-MLComp.xml']);
    else
        nomFicMultiImageXML = fullfile(nomDirOut, [nomFic '_' Tag '-MLComp.xml']);
    end
    [flag, Reflectivity_LatLong] = WC_HorizontalSlices_OneReflectivity(nbSlices, Data3DMatrix, binsDeb, GridSize);
    if flag
        % Export des images en Multidata pour Globe3DV
        sizeTiff = 32000; %2500;
        flag = export_3DVMultiImages(Reflectivity_LatLong, nomFicMultiImageXML, 'sizeTiff', sizeTiff);
        if ~flag
            return
        end
    end
    
else
    
    nomFicMultiImageXML = fullfile(nomDirOut, [nomFic '-ML.xml']);
    
    %% Calcul des slices horizontales
    
    [flag, Reflectivity_LatLong] = WC_HorizontalSlices_OneReflectivity(nbSlices, Data3DMatrix, binsDeb, GridSize);
    if flag
        % Export des images en Multidata pour Globe3DV
        sizeTiff = 32000; %2500;
        flag = export_3DVMultiImages(Reflectivity_LatLong, nomFicMultiImageXML, 'sizeTiff', sizeTiff);
        if ~flag
            return
        end
    end
end


function S = computeSlice(Reflectivity, sub)

% X = mean(single(Reflectivity(:,sub,:)), 2 , 'omitnan');
% X = squeeze(X);

[~, n2, n3] = size(Reflectivity);
S = zeros(n2, n3, 'single');
N = zeros(n2, n3, 'single');
R = Reflectivity(sub,:,:);
for k=1:length(sub)
    X = squeeze(R(k,:,:));
    subNonNaN = ~isnan(X);
    S(subNonNaN) = S(subNonNaN) + X(subNonNaN);
    N(subNonNaN) = N(subNonNaN) + 1;
end
clear X
S = S ./ N;


function [flag, Reflectivity_LatLong] = WC_HorizontalSlices_OneReflectivity(nbSlices, Data3DMatrix, binsDeb, GridSize)

flag = 1;
Reflectivity_LatLong = cl_image.empty;

%{
pings = 1:Data3DMatrix.Dimensions.nbPings;
figure;
for k=1:size(Data3DMatrix.Reflectivity, 3)
    imagesc(Data3DMatrix.AcrossDistance, Data3DMatrix.Depth, Data3DMatrix.Reflectivity(:,:,k), Data3DMatrix.CLim);
    axis xy; axis equal; axis tight; colormap(jet(256)); title(sprintf('k=%d', k)); drawnow; colorbar
end

figure;
for k=1:size(Data3DMatrix.Reflectivity, 1)
    imagesc(pings, Data3DMatrix.AcrossDistance, squeeze(Data3DMatrix.Reflectivity(k,:,:)), Data3DMatrix.CLim);
    axis xy; axis tight; colormap(jet(256)); title(sprintf('k=%d', k)); drawnow; colorbar
end
%}

try
    X = Data3DMatrix.Reflectivity(:,:,:);
    Data3DMatrix.Reflectivity = X;
    clear X
catch
    my_breakpoint
    flag = 0;
    return
end

SliceExists = true(nbSlices,1);
pings = 1:Data3DMatrix.Dimensions.nbPings;
Z = linspace(min(Data3DMatrix.Depth), max(Data3DMatrix.Immersion), size(Data3DMatrix.Reflectivity, 1));

str1 = 'Cr�ation des tranches';
str2 = 'Creating slices';
hw = create_waitbar(Lang(str1,str2), 'N', nbSlices);
% for k=nbSlices-2:nbSlices
for k=1:nbSlices
    my_waitbar(k, nbSlices, hw)
    
%     sub = find((Data3DMatrix.Depth >= binsDeb(k)) & (Data3DMatrix.Depth <= binsDeb(k+1)));
%     sub = find((Z >= binsDeb(k)) & (Z <= binsDeb(k+1)));
    sub = find((Z <= binsDeb(k)) & (Z >= binsDeb(k+1)));
    sub(sub < 1) = [];
    sub(sub > Data3DMatrix.Dimensions.nbRows) = [];
    if isempty(sub)
        SliceExists(k) = false;
        continue
    end    
    % TODO : faire le moyennage en valeur naturelle please !!!
    Image = computeSlice(Data3DMatrix.Reflectivity, sub);
    if all(Image(:) == 0)
        SliceExists(k) = false;
        continue
    end

%     figure(7567); imagesc(pings, Data3DMatrix.AcrossDistance, Image);
%     axis xy; axis tight; colormap(jet(256)); title(sprintf('k=%d / %d', k, nbSlices)); drawnow; colorbar;
    
    %% Cr�er l'image en g�om�trie PingAcrossDist
    
    % TODO 
    
    lat1 = Data3DMatrix.latBab(:);
    lon1 = Data3DMatrix.lonBab(:);
    lat2 = Data3DMatrix.latTri(:);
    lon2 = Data3DMatrix.lonTri(:);
    Heading = mod(azimuth([lat1 lon1],[lat2 lon2]) + (360 - 90), 360);
    
    Image = Image';
    a = cl_image('Image', Image, 'x', double(Data3DMatrix.AcrossDistance), ...
        'GeometryType', cl_image.indGeometryType('PingAcrossDist'), ...
        'y', pings, 'XUnit', 'm', 'YUnit', 'Ping#', 'Unit', 'dB');
    a = set_SonarTime(a, Data3DMatrix.Datetime);
    a = set_SonarFishLatitude( a, Data3DMatrix.latCentre);
    a = set_SonarFishLongitude(a, Data3DMatrix.lonCentre);
    a = set_SonarHeading(a, Heading);

    [a, flag] = crop(a);
    if isempty(a)
        continue
    end
%     suby = Suby{1};
%     a = set_SonarTime(a, Data3DMatrix.Datetime(suby));
%     a = set_SonarFishLatitude( a, Data3DMatrix.latCentre(suby));
%     a = set_SonarFishLongitude(a, Data3DMatrix.lonCentre(suby));
%     a = set_SonarHeading(a, Heading(suby));
    
    a.GeometryType = cl_image.indGeometryType('PingAcrossDist');
    a.DataType     = cl_image.indDataType('Reflectivity');

    a.InitialFileName = Data3DMatrix.ExtractedFrom;
    
    [Carto, flag] = getDefinitionCarto(a, 'CartoAuto', 1);
    if ~flag
        return
    end
    a = set_Carto(a, Carto);
    
    %% Mosa�quer l'image en LatLong
    
    [flag, b] = sonarMosaique(a, 1, GridSize, 'NoQuestion', 1, 'NoWaitbar', 1);
    if ~flag
        return
    end
%     SonarScope(b)
%     imagesc(b)
    Reflectivity_LatLong(end+1) = b; %#ok<AGROW>
end
my_close(hw, 'MsgEnd')
% my_close(7567)
