function WC = WC_InitStructXML(nbPings, nbAcross)

WC.lonBab           = NaN(1, nbPings);
WC.latBab           = NaN(1, nbPings);
WC.lonCentre        = NaN(1, nbPings);
WC.latCentre        = NaN(1, nbPings);
WC.lonTri           = NaN(1, nbPings);
WC.latTri           = NaN(1, nbPings);
WC.iPing            = NaN(1, nbPings);
WC.xBab             = NaN(1, nbPings);
WC.xTri             = NaN(1, nbPings);
WC.Depth            = NaN(1, nbPings);
WC.Immersion        = NaN(1, nbPings); % NaN pour KM, zeros pour Reson
WC.Tide             = NaN(1, nbPings); % Pas pr�sent pour Reson
WC.Heave            = NaN(1, nbPings);
WC.Date             = NaN(1, nbPings);
WC.Hour             = NaN(1, nbPings);
WC.lonBabDepointe   = NaN(1, nbPings);
WC.latBabDepointe   = NaN(1, nbPings);
WC.lonTriDepointe   = NaN(1, nbPings);
WC.latTriDepointe   = NaN(1, nbPings);
WC.MinRangeInMeters = NaN(1, nbPings);

if nbAcross ~= 0
    WC.LatitudeTop  = NaN(nbPings,nbAcross);
    WC.LongitudeTop = NaN(nbPings,nbAcross);
end

WC.SoundingsRangeInMeters = NaN(nbPings,0, 'single');
WC.SoundingsAcrossDist    = NaN(nbPings,0, 'single');
WC.SoundingsDepth         = NaN(nbPings,0, 'single');
