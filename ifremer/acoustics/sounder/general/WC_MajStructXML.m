function WC = WC_MajStructXML(WC, sub)

WC.lonBab           = WC.lonBab(sub);
WC.latBab           = WC.latBab(sub);
WC.lonCentre        = WC.lonCentre(sub);
WC.latCentre        = WC.latCentre(sub);
WC.lonTri           = WC.lonTri(sub);
WC.latTri           = WC.latTri(sub);
WC.iPing            = WC.iPing(sub);
WC.xBab             = WC.xBab(sub);
WC.xTri             = WC.xTri(sub);
WC.Depth            = WC.Depth(sub);
WC.Immersion        = WC.Immersion(sub); % NaN pour KM, zeros pour Reson
WC.Tide             = WC.Tide(sub); % Pas pr�sent pour Reson
WC.Heave            = WC.Heave(sub);
WC.Date             = WC.Date(sub);
WC.Hour             = WC.Hour(sub);
WC.lonBabDepointe   = WC.lonBabDepointe(sub);
WC.latBabDepointe   = WC.latBabDepointe(sub);
WC.lonTriDepointe   = WC.lonTriDepointe(sub);
WC.latTriDepointe   = WC.latTriDepointe(sub);
WC.MinRangeInMeters = WC.MinRangeInMeters(sub);

if isfield(WC, 'LatitudeTop')
    WC.LatitudeTop  = WC.LatitudeTop(sub,:); % NaN(nbPings,nbAcross);
    WC.LongitudeTop = WC.LongitudeTop(sub,:);
end
