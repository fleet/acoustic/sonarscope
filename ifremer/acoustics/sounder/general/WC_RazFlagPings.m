function WC_RazFlagPings(nomFic) 

if ~iscell(nomFic)
    nomFic = {nomFic};
end

str1 = 'RAZ des flags de pings WC des fichiers .all';
str2 = 'Reset WC ping flags of .all files';
N = length(nomFic);
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
    my_waitbar(k, N, hw);
    WC_RazFlagPings_unitaire(nomFic{k});
end
my_close(hw, 'MsgEnd')


function WC_RazFlagPings_unitaire(nomFic)

flag = existCacheNetcdf(nomFic);
if flag
     WC_RazFlagPings_Netcdf(nomFic);
else
     WC_RazFlagPings_XMLBin(nomFic);
end


function WC_RazFlagPings_Netcdf(nomFic)

[nomDirRacine, nomFic] = fileparts(nomFic);
nomFicNc = fullfile(nomDirRacine, 'SonarScope', [nomFic '.nc']);
if ~exist(nomFicNc, 'file')
    return
end

ncID  = netcdf.open(nomFicNc, 'WRITE');
grpID = netcdf.inqNcid(ncID, 'WaterColumn');
try
    varID = netcdf.inqVarID(grpID, 'FlagPings');
    X = netcdf.getVar(grpID, varID);
    X(:) = 0;
    NetcdfUtils.putVarVal(grpID, varID, X, 1)
    netcdf.close(ncID);
catch
    varID = netcdf.inqVarID(grpID, 'PingCounter');
    [~, ~, dimids] = netcdf.inqVar(grpID, varID);
    NC_Storage = NetcdfUtils.getNetcdfClassname('uint8');
    varID = netcdf.defVar(grpID, 'FlagPings', NC_Storage, dimids);
    netcdf.defVarDeflate(grpID, varID, false, true, 1);
    netcdf.defVarFill(grpID, varID, true, 0);
    netcdf.putAtt(grpID, varID, 'Unit', '');
    netcdf.putAtt(grpID, varID, 'Populated', 1);
    netcdf.close(ncID);
end


function WC_RazFlagPings_XMLBin(nomFic)

[nomDir, nomFic2] = fileparts(nomFic);
nomFicFlagPings = fullfile(nomDir, 'SonarScope', nomFic2, 'Ssc_WaterColumn', 'FlagPings.bin');
if exist(nomFicFlagPings, 'file')
    fid = fopen(nomFicFlagPings, 'r');
    FlagPings = fread(fid, 'uint8');
    fclose(fid);
    if any(FlagPings)
        fid = fopen(nomFicFlagPings, 'w+');
        if fid == -1
            messageErreurFichier(nomFicFlagPings, 'WriteFailure')
            return
        end
        FlagPings(:) = 0;
        fwrite(fid, FlagPings, 'uint8'); % TODO : subDepthFic subWCFic
        fclose(fid);
    end
end
