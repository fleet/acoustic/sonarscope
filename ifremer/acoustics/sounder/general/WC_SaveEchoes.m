% Save Water Column echoes in a XML file for 3DViewer
%
% Syntax
%   flag = WC_SaveEchoes(nomFicXml, Points, ...)
%
% Input Arguments
%   nomFicXml : Name of the XML file for 3DViewer
%   Points    : Structure containing the data
%               -- Mandatory fields --
%               Latitude       : Latitudes of the echoes
%               Longitude      : Longitudes of the echoes
%               Z              : Depths of the echoes
%               -- Optional fiels --
%               BaseForm       : Pastille ou cube : TODo � compl�ter
%               Date           : Date (Matlab reference)
%               Hour           : Nb of milliseconds from midnight
%               iPing          : Ping number
%               Energie        : Energy of the echoes
%               EnergieTotale  : Energy of the echoes of the ping
%               XCoor          : Abscissa in a given projection
%               YCoor          : Ordinates in a given projection
%               Angle          : Beam angles of the echoes
%               RangeInSamples : Range from the antenna to the echoes
%               AcrossDistance : Across distance
%               Celerite       : Celerity
%
% Name-Value Pair Arguments
%   InitialFileName : Name of the corresponfing sonar file
%   LayerName3DV    : Name of the Echoes in 3DViewer
%   Survey          : Structure containing information on the survey
%                     (no need to create the all the fields)
%                     Survey         : Name of the cruise
%                     Vessel         : Name of the vessel
%                     Sounder        : Name of the sounder
%                     ChiefScientist : Name of the boss
%
% Output Arguments
%   flag : 1=OK, 0=KO
%
% Examples
%   Points.Longitude = rand(100, 1);
%   Points.Latitude  = rand(100, 1);
%   Points.Z         = rand(100, 1);
%   WC_plotEchoes(Points)
%
%   nomFicXml = my_tempname('.xml');
%   flag = WC_SaveEchoes(nomFicXml, Points);
%   edit(nomFicXml)
%
%   [flag, Data] = XMLBinUtils.readGrpData(nomFicXml, 'BinInData', 1)
%   WC_plotEchoes(Data)
%
% See also sonar_SimradProcessWC sonar_exportSoundingsToSSc3DV WC_plotEchoes XMLBinUtils.readGrpData Authors
% Authors  : JMA
%-------------------------------------------------------------------------------

function flag = WC_SaveEchoes(nomFicXml, Points, varargin)

[varargin, InitialFileName] = getPropertyValue(varargin, 'InitialFileName', []);
[varargin, LayerName3DV]    = getPropertyValue(varargin, 'LayerName3DV',    []);
[varargin, Survey]          = getPropertyValue(varargin, 'Survey',          []); %#ok<ASGLU>

if isempty(Points.Z)
    flag = 0;
    return
end

nomDirRacine = fileparts(nomFicXml);
[nomDirPolarEchograms, nomFic] = fileparts(nomFicXml);

%% G�n�ration du XML SonarScope

Echoes.Software = 'SonarScope';
Echoes.DataTag  = 'WaterColumnEchoes';
if isempty(LayerName3DV)
    Echoes.Name = nomFic;
else
    Echoes.Name = LayerName3DV;
end

Echoes.nomDirExport = nomDirRacine; % Est-ce toujours utile ??? GLU
Echoes.FormatVersion = 20100201;
if isempty(InitialFileName)
    Echoes.ExtractedFrom = 'Unkown';
else
    Echoes.ExtractedFrom = InitialFileName;
end

if isempty(Survey)
    Echoes.Survey         = 'Unknown';
    Echoes.Vessel         = 'Unknown';
    Echoes.Sounder        = 'Unknown';
    Echoes.ChiefScientist = 'Unknown';
else
    if isfield(Survey, 'Name')
        Echoes.Survey  = Survey.Name;
    else
        Echoes.Survey = 'Unkown';
    end
    if isfield(Survey, 'Vessel')
        Echoes.Vessel = Survey.Vessel;
    else
        Echoes.Vessel = 'Unkown';
    end
    if isfield(Survey, 'Sounder')
        Echoes.Sounder = Survey.Sounder;
    else
        Echoes.Sounder = 'Unkown';
    end
    if isfield(Survey, 'ChiefScientist')
        Echoes.ChiefScientist = Survey.ChiefScientist;
    else
        Echoes.ChiefScientist = 'Unkown';
    end
end

if isfield(Points, 'BaseForm')
    Echoes.BaseForm = Points.BaseForm;
end

Echoes.Dimensions.nbEchoes = length(Points.Longitude);

Echoes.SignalFilters = [];

Echoes.Signals(1).Name          = 'Latitude';
Echoes.Signals(end).Dimensions  = 'nbEchoes, 1';
Echoes.Signals(end).Storage     = 'double';
Echoes.Signals(end).Unit        = 'deg';
Echoes.Signals(end).FileName    = fullfile(nomFic, 'Latitude.bin');

Echoes.Signals(end+1).Name      = 'Longitude';
Echoes.Signals(end).Dimensions  = 'nbEchoes, 1';
Echoes.Signals(end).Storage     = 'double';
Echoes.Signals(end).Unit        = 'deg';
Echoes.Signals(end).FileName    = fullfile(nomFic, 'Longitude.bin');

Echoes.Signals(end+1).Name     = 'Z';
Echoes.Signals(end).Dimensions  = 'nbEchoes, 1';
Echoes.Signals(end).Storage     = 'single';
Echoes.Signals(end).Unit        = 'm';
Echoes.Signals(end).FileName    = fullfile(nomFic, 'Z.bin');

Echoes.SignalFilters(end+1).Signal = 'Z';
Echoes.SignalFilters(end).Label    = 'Profondeur';
Echoes.SignalFilters(end).Type     = 'MinMax';

if isfield(Points, 'Date')
    Echoes.Signals(end+1).Name      = 'Date';
    Echoes.Signals(end).Dimensions  = 'nbEchoes, 1';
    Echoes.Signals(end).Storage     = 'double';
    Echoes.Signals(end).Unit        = 'Julian Day';
    Echoes.Signals(end).FileName    = fullfile(nomFic, 'Date.bin');
end

if isfield(Points, 'Hour')
    Echoes.Signals(end+1).Name      = 'Hour';
    Echoes.Signals(end).Dimensions  = 'nbEchoes, 1';
    Echoes.Signals(end).Storage     = 'double';
    Echoes.Signals(end).Unit        = 'nb millis / midnight';
    Echoes.Signals(end).FileName    = fullfile(nomFic, 'Hour.bin');
end

if isfield(Points, 'iPing')
    Echoes.Signals(end+1).Name      = 'iPing';
    Echoes.Signals(end).Dimensions  = 'nbEchoes, 1';
    Echoes.Signals(end).Storage     = 'single';
    Echoes.Signals(end).Unit        = '';
    Echoes.Signals(end).FileName    = fullfile(nomFic,'iPing.bin');
    
    Echoes.SignalFilters(end+1).Signal = 'iPing';
    Echoes.SignalFilters(end).Label    = 'Ping';
    Echoes.SignalFilters(end).Type     = 'MinMax';
end

if isfield(Points, 'Energie')
    Echoes.Signals(end+1).Name      = 'Energie';
    Echoes.Signals(end).Dimensions  = 'nbEchoes, 1';
    Echoes.Signals(end).Storage     = 'single';
    Echoes.Signals(end).Unit        = 'dB';
    Echoes.Signals(end).FileName    = fullfile(nomFic, 'Energie.bin');
    
    Echoes.SignalFilters(end+1).Signal = 'Energie';
    Echoes.SignalFilters(end).Label    = 'Energie';
    Echoes.SignalFilters(end).Type     = 'MinMax';
end

if isfield(Points, 'EnergieTotale')
    Echoes.Signals(end+1).Name      = 'EnergieTotale';
    Echoes.Signals(end).Dimensions  = 'nbEchoes, 1';
    Echoes.Signals(end).Storage     = 'single';
    Echoes.Signals(end).Unit        = 'dB';
    Echoes.Signals(end).FileName    = fullfile(nomFic, 'EnergieTotale.bin');
    
    Echoes.SignalFilters(end+1).Signal = 'EnergieTotale';
    Echoes.SignalFilters(end).Label    = 'EnergieTotale';
    Echoes.SignalFilters(end).Type     = 'MinMax';
end

if isfield(Points, 'XCoor')
    Echoes.Signals(end+1).Name      = 'XCoor';
    Echoes.Signals(end).Dimensions  = 'nbEchoes, 1';
    Echoes.Signals(end).Storage     = 'single';
    Echoes.Signals(end).Unit        = 'm';
    Echoes.Signals(end).FileName    = fullfile(nomFic, 'XCoor.bin');
    
    %     Echoes.SignalFilters(end+1).Signal = 'XCoor';
    %     Echoes.SignalFilters(end).Label    = 'XCoor';
    %     Echoes.SignalFilters(end).Type     = 'MinMax';
end

if isfield(Points, 'YCoor')
    Echoes.Signals(end+1).Name      = 'YCoor';
    Echoes.Signals(end).Dimensions  = 'nbEchoes, 1';
    Echoes.Signals(end).Storage     = 'single';
    Echoes.Signals(end).Unit        = 'm';
    Echoes.Signals(end).FileName    = fullfile(nomFic, 'YCoor.bin');
    
    %     Echoes.SignalFilters(end+1).Signal = 'YCoor';
    %     Echoes.SignalFilters(end).Label    = 'YCoor';
    %     Echoes.SignalFilters(end).Type     = 'MinMax';
end

if isfield(Points, 'Angle')
    Echoes.Signals(end+1).Name      = 'Angle';
    Echoes.Signals(end).Dimensions  = 'nbEchoes, 1';
    Echoes.Signals(end).Storage     = 'single';
    Echoes.Signals(end).Unit        = 'deg';
    Echoes.Signals(end).FileName    = fullfile(nomFic, 'Angle.bin');
    
    Echoes.SignalFilters(end+1).Signal = 'Angle';
    Echoes.SignalFilters(end).Label    = 'Angle';
    Echoes.SignalFilters(end).Type     = 'MinMax';
end

if isfield(Points, 'RangeInSamples')
    Echoes.Signals(end+1).Name      = 'RangeInSamples';
    Echoes.Signals(end).Dimensions  = 'nbEchoes, 1';
    Echoes.Signals(end).Storage     = 'single';
    Echoes.Signals(end).Unit        = 'sample';
    Echoes.Signals(end).FileName    = fullfile(nomFic, 'RangeInSamples.bin');
    
    Echoes.SignalFilters(end+1).Signal = 'RangeInSamples';
    Echoes.SignalFilters(end).Label    = 'RangeInSamples';
    Echoes.SignalFilters(end).Type     = 'MinMax';
end

if isfield(Points, 'AcrossDistance')
    Echoes.Signals(end+1).Name      = 'AcrossDistance';
    Echoes.Signals(end).Dimensions  = 'nbEchoes, 1';
    Echoes.Signals(end).Storage     = 'single';
    Echoes.Signals(end).Unit        = 'm';
    Echoes.Signals(end).FileName    = fullfile(nomFic, 'AcrossDistance.bin');
    
    Echoes.SignalFilters(end+1).Signal = 'AcrossDistance';
    Echoes.SignalFilters(end).Label    = 'AcrossDistance';
    Echoes.SignalFilters(end).Type     = 'MinMax';
end

if isfield(Points, 'Celerite')
    Echoes.Signals(end+1).Name      = 'Celerite';
    Echoes.Signals(end).Dimensions  = 'nbEchoes, 1';
    Echoes.Signals(end).Storage     = 'single';
    Echoes.Signals(end).Unit        = 'm/s';
    Echoes.Signals(end).FileName    = fullfile(nomFic, 'Celerite.bin');
    
    %     Echoes.SignalFilters(end+1).Signal = 'Celerite';
    %     Echoes.SignalFilters(end).Label    = 'Celerite';
    %     Echoes.SignalFilters(end).Type     = 'MinMax';
end


%% Cr�ation du fichier XML d�crivant la donn�e

xml_write(nomFicXml, Echoes);
flag = exist(nomFicXml, 'file');
if ~flag
    messageErreur(nomFicXml)
    return
end

%% Cr�ation du r�pertoire Echoes

nomDirPolarEchograms = fullfile(nomDirPolarEchograms, nomFic);
if ~exist(nomDirPolarEchograms, 'dir')
    status = mkdir(nomDirPolarEchograms);
    if ~status
        messageErreur(nomDirPolarEchograms)
        flag = 0;
        return
    end
end

%% Cr�ation des fichiers binaires des signaux

for k=1:length(Echoes.Signals)
    flag = writeSignal(nomDirRacine, Echoes.Signals(k), Points.(Echoes.Signals(k).Name));
    if ~flag
        return
    end
end

flag = 1;
