%{
nomFicEchos = 'C:\Temp\TestSonarScopeJM\Events0611.txt';
MultiMarkerInfos.Survey           = Survey.Name;
MultiMarkerInfos.Vessel           = Survey.Vessel;
MultiMarkerInfos.Sounder          = Survey.Sounder;
MultiMarkerInfos.ChiefScientist   = Survey.ChiefScientist;
[nomDirEchoes, nomFic2] = fileparts(nomFicEchos);
nomFicEchoes = fullfile(nomDirEchoes, [nomFic2 '_Footprints.xml']);
Points.Longitude  = X{2};
Points.Latitude   = X{1};
Points.Altitude = zeros(length(X{1}), 'single');
flag = WC_SaveFootprints(nomFicEchoes, Points, Survey);
%}

function flag = WC_SaveFootprints(nomFicXml, Points, Survey)

if isempty(Points.Latitude)
    flag = 0;
    return
end

nomDirRacine = fileparts(nomFicXml);
[nomDirPolarEchograms, nomFic] = fileparts(nomFicXml);

% ----------------------------
% G�n�ration du XML SonarScope

MultiMarkerInfos.Software     = 'SonarScope';
MultiMarkerInfos.DataTag      = 'BathysMarker';
MultiMarkerInfos.Name         = 'PlumeFootprint';

MultiMarkerInfos.nomDirExport = nomDirRacine; % Est-ce toujours utile ??? GLU
MultiMarkerInfos.FormatVersion = 20100201;

MultiMarkerInfos.Survey           = Survey.Name;
MultiMarkerInfos.Vessel           = Survey.Vessel;
MultiMarkerInfos.Sounder          = Survey.Sounder;
MultiMarkerInfos.ChiefScientist   = Survey.ChiefScientist;

MultiMarkerInfos.Dimensions.nbPings = length(Points.Longitude);

MultiMarkerInfos.Signals(1).Name      = 'Altitude';
MultiMarkerInfos.Signals(end).Dimensions  = 'nbPings, 1';
MultiMarkerInfos.Signals(end).Storage     = 'single';
MultiMarkerInfos.Signals(end).Unit        = '';
MultiMarkerInfos.Signals(end).FileName    = fullfile(nomFic,'Altitude.bin');

MultiMarkerInfos.Signals(end+1).Name      = 'Longitude';
MultiMarkerInfos.Signals(end).Dimensions  = 'nbPings, 1';
MultiMarkerInfos.Signals(end).Storage     = 'double';
MultiMarkerInfos.Signals(end).Unit        = 'deg';
MultiMarkerInfos.Signals(end).FileName    = fullfile(nomFic, 'Longitude.bin');

MultiMarkerInfos.Signals(end+1).Name      = 'Latitude';
MultiMarkerInfos.Signals(end).Dimensions  = 'nbPings, 1';
MultiMarkerInfos.Signals(end).Storage     = 'double';
MultiMarkerInfos.Signals(end).Unit        = 'deg';
MultiMarkerInfos.Signals(end).FileName    = fullfile(nomFic, 'Latitude.bin');


%% Cr�ation du fichier XML d�crivant la donn�e 

% nomFicXml = fullfile(nomDirRacine, 'WC_Echoes.xml');
xml_write(nomFicXml, MultiMarkerInfos);
flag = exist(nomFicXml, 'file');
if ~flag
    messageErreur(nomFicXml)
    return
end

%% Cr�ation du r�pertoire MultiMarkerInfos

nomDirPolarEchograms = fullfile(nomDirPolarEchograms, nomFic);
if ~exist(nomDirPolarEchograms, 'dir')
    status = mkdir(nomDirPolarEchograms);
    if ~status
        messageErreur(nomDirPolarEchograms)
        flag = 0;
        return
    end
end

%% Cr�ation des fichiers binaires des signaux

for i=1:length(MultiMarkerInfos.Signals)
    flag = writeSignal(nomDirRacine, MultiMarkerInfos.Signals(i), Points.(MultiMarkerInfos.Signals(i).Name));
    if ~flag
        return
    end
end

flag = 1;
