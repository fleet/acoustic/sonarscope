function flag = WC_SlicesAlongNavFlyTexture(nomFicXml, nomDirOut, varargin)

[varargin, Mute] = getPropertyValue(varargin, 'Mute', 0);

logFileId = getLogFileId;

if ischar(nomFicXml)
    nomFicXml = {nomFicXml};
end

% N = length(nomFicXml);
% for k=1:N
%     if contains(nomFicXml{k}, 'Raw')
%         nomFicXmlRaw{k} = nomFicXml{k}; %#ok<AGROW>
%         nomFicXmlComp{k} = strrep(nomFicXml{k}, 'Raw', 'Comp'); %#ok<AGROW>
%     elseif contains(nomFicXml{k}, 'Comp')
%         nomFicXmlRaw{k} = strrep(nomFicXml{k}, 'Comp', 'Raw'); %#ok<AGROW>
%         nomFicXmlComp{k} = nomFicXml{k}; %#ok<AGROW>
%     end
% end
% [~, ia] = unique(nomFicXmlRaw);
% nomFicXmlRaw  = nomFicXmlRaw(ia);
% nomFicXmlComp = nomFicXmlComp(ia);

nomFicXmlRaw = nomFicXml;
N = length(nomFicXmlRaw);
str1 = 'Cr�ation des Slices le long de la navigation';
str2 = 'Processing WC slices along navigation';
hw  = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    str = sprintf('Processing %s', nomFicXmlRaw{k});
    logFileId.info('WC_SlicesAlongNavFlyTexture', str);
    
%     if isempty(nomFicXmlComp{k})
%         [~, ~, Ext] = fileparts(nomFicXmlRaw{k});
%     else
%         [~, ~, Ext] = fileparts(nomFicXmlComp{k});
%     end
    flagWaitbar = ~Mute && (N == 1);
    [~, ~, ExtIn] = fileparts(nomFicXmlRaw{k});
    if strcmp(ExtIn, '.nc')
        flag = WC_SlicesAlongNavigation_unitaireNetcdf(nomFicXmlRaw{k}, nomDirOut, flagWaitbar, varargin{:});
    else
        flag = WC_SlicesAlongNavigation_unitaireXMLBin(nomFicXmlRaw{k}, nomDirOut, flagWaitbar, varargin{:});
    end
end
my_close(hw, 'MsgEnd')


function flag = WC_SlicesAlongNavigation_unitaireXMLBin(nomFicXmlRaw, nomDirOut, flagWaitbar, varargin)

[varargin, ncFileName] = getPropertyValue(varargin, 'ncFileName', []);
[varargin, Step]       = getPropertyValue(varargin, 'Step',       []); %#ok<ASGLU>

if ~isempty(nomFicXmlRaw)
    [nomDirXml, nomFicSeul] = fileparts(nomFicXmlRaw);
    nomDirXml = fullfile(nomDirXml, [nomFicSeul '.zip']);
    my_unzip(nomDirXml);
end

% if ~isempty(nomFicXmlComp)
%     [nomDirXml, nomFicSeul] = fileparts(nomFicXmlComp);
%     nomDirXml = fullfile(nomDirXml, [nomFicSeul '.zip']);
%     my_unzip(nomDirXml);
% end

nbLayers = 0;
if ~isempty(nomFicXmlRaw)
    fprintf('Reading %s\n', nomFicXmlRaw)
    
    [flag, Data3DMatrixRaw] = WC_readImageCube3D(nomFicXmlRaw, 'Memmapfile', 0);
    if ~flag
        return
    end
    
    if numel(Data3DMatrixRaw.Reflectivity) == 0
        str1 = sprintf('Echec de la cr�ation de slices pour le fichier  %s', nomFicXmlRaw);
        str2 = sprintf('Slice creation failed for  %s', nomFicXmlRaw);
        my_warndlg(Lang(str1,str2), 1);
        flag = 0;
        return
    end
    Data3DMatrix = Data3DMatrixRaw;
    nbLayers = nbLayers + 1;
    [~, nomFic] = fileparts(nomFicXmlRaw);
end

% if ~isempty(nomFicXmlComp)
%     fprintf('Reading %s\n', nomFicXmlComp)
%     [flag, Data3DMatrixComp] = WC_readImageCube3D(nomFicXmlComp, 'Memmapfile', 0);
%     if ~flag
%         return
%     end
%     if numel(Data3DMatrixComp.Reflectivity) == 0
%         str1 = sprintf('Echec de la cr�ation de slices pour le fichier  %s', nomFicXmlComp);
%         str2 = sprintf('Slice creation failed for  %s', nomFicXmlComp);
%         my_warndlg(Lang(str1,str2), 1);
%         flag = 0;
%         return
%     end
%     Data3DMatrix = Data3DMatrixComp;
%     nbLayers = nbLayers + 1;
%     [~, nomFic] = fileparts(nomFicXmlComp);
% end

if isempty(Step)
%     Step = mean(diff(Data3DMatrix.AcrossDistance));
    Step1 = mean(diff(Data3DMatrix.AcrossDistance));
    Step2 = range(Data3DMatrix.AcrossDistance) / 51;
    Step = max(Step1, Step2);
    Step = round(Step, 1, 'significant');
end

%{
figure;
for k=1:size(Data3DMatrixRaw.Reflectivity, 3)
    % TODO Data3DMatrixRaw.CLim
    imagesc(Data3DMatrixRaw.AcrossDistance, Data3DMatrixRaw.Z, Data3DMatrixRaw.Reflectivity(:,:,k), Data3DMatrixRaw.CLim);
    axis xy; axis equal; axis tight; title(sprintf('k=%d', k)); drawnow; colorbar
end
%}

bins = double(Data3DMatrix.AcrossDistance(1)):Step:double(Data3DMatrix.AcrossDistance(end)); % Reintroduit par JMA le 31/010/2018
binsCentre = centrage_magnetique(bins);
binsDeb = binsCentre - Step/2;
binsDeb(end+1) = binsDeb(end) + Step;
nbGroups = length(binsCentre);

%% Cr�ation du r�pertoire

if isempty(ncFileName)
    nomFic = strrep(nomFic, '_PolarEchograms', '');
    nomFic = strrep(nomFic, '_WCCube', '_WCSlice');
    nomFic = strrep(nomFic, '_Cube', '_WCSlice');
    if nbLayers == 2
        nomFic = strrep(nomFic, 'Raw',  '');
        nomFic = strrep(nomFic, 'Comp', '');
    end
    nomFic = strrep(nomFic, '__', '_');
    if strcmp(nomFic(end), '_')
        nomFic(end) = [];
    end
    nomDirSlice = nomFic;
    ncFileName = fullfile(nomDirOut, [nomFic '.nc']);
else
    [~, nomDirSlice] = fileparts(ncFileName);
end

%% Cr�ation des images

%{
sep = getCSVseparator;
str = sprintf('%s%s%s%sStep=%sm', nomDirOut, sep, [nomFic '.xml'], sep, num2str(Step));
fprintf('Creating %s\n', str)
nomFicInfo = fullfile(nomDirOut, 'WC-AlongNav-StepValues.txt');
appendTxtFile(nomFicInfo, str);
%}
if ~isempty(nomFicXmlRaw)
    try
        X = Data3DMatrixRaw.Reflectivity(:,:,:);
        Data3DMatrixRaw.Reflectivity = X;
        clear X
    catch
        my_breakpoint
    end
    ReflectivityRaw = NaN(Data3DMatrix.Dimensions.nbRows, Data3DMatrix.Dimensions.nbPings, nbGroups, 'single');
end
% if ~isempty(nomFicXmlComp)
%     try
%         X = Data3DMatrixComp.Reflectivity(:,:,:);
%         Data3DMatrixComp.Reflectivity = X;
%         clear X
%     catch
%         my_breakpoint
%     end
%     ReflectivityComp = NaN(Data3DMatrix.Dimensions.nbRows, Data3DMatrix.Dimensions.nbPings, nbGroups, 'single');
% end
if isfield(Data3DMatrix, 'Mask')
    try
        X = Data3DMatrix.Mask(:,:,:);
        Data3DMatrix.Mask = X;
        clear X
    catch
        my_breakpoint
    end
    ReflectivityMask = zeros(Data3DMatrix.Dimensions.nbRows, Data3DMatrix.Dimensions.nbPings, nbGroups, 'single');
end

ZLim = [min(Data3DMatrix.Z) max(Data3DMatrix.Z)];
SliceExists = true(nbGroups,1);

if flagWaitbar
    str1 = 'Cr�ation des profils';
    str2 = 'Creating profiles';
    hw = create_waitbar(Lang(str1,str2), 'N', nbGroups);
end
for k=1:nbGroups
    if flagWaitbar
        my_waitbar(k, nbGroups, hw)
    end
    
    sub = find((Data3DMatrix.AcrossDistance >= binsDeb(k)) & (Data3DMatrix.AcrossDistance <= binsDeb(k+1)));
    sub(sub < 1) = [];
    sub(sub > Data3DMatrix.Dimensions.nbColumns) = [];
    
    flagPingDisplayed = false;
        
%     if ~isempty(nomFicXmlComp)
%         X = computeSlice(Data3DMatrixComp.Reflectivity, sub);
%         if all(X(:) == 0)
%             SliceExists(k) = false;
%             continue
%         end
%         ReflectivityComp(:,:,k) = X;
%         nomFic2 = ncFileName;
%         if mod(k, ceil(nbGroups/41)) == 1
%             figure(7567); imagesc(1:Data3DMatrix.Dimensions.nbPings, Data3DMatrix.Z, X, Data3DMatrix.CLim);
%             axis xy; xlabel('Pings'), ylabel('Depth'); colormap(jet(256)); colorbar;
%             title(sprintf('%d/%d : %s', k, nbGroups, nomFic2), 'Interpreter', 'None')
%             drawnow
%             flagPingDisplayed = true;
%         end
%     end
    
    if ~isempty(nomFicXmlRaw)
        % TODO : faire le moyennage en valeur naturelle please !!!
        %     X = computeSlice(Data3DMatrixRaw.Reflectivity, sub);
        X = computeSlice(Data3DMatrixRaw.Reflectivity, sub);
        %     X = nanmean(single(Data3DMatrixRaw.Reflectivity(:,sub,:)),2);
        %     X = squeeze(X);
        
        if all(X(:) == 0)
            SliceExists(k) = false;
            continue
        end
        
        %{
Ym = repmat(Data3DMatrix.Z(:), 1, Data3DMatrix.Dimensions.nbPings);
Ym = Ym - Data3DMatrix.Immersion(:)';
Xm = repmat(1:Data3DMatrix.Dimensions.nbPings, Data3DMatrix.Dimensions.nbRows, 1);
figure(67676); pcolor(Xm, Ym, X); colormap(jet(256)); colorbar; grid on; shading flat;
        %}
        %     figure(67676); imagesc(I); colormap(jet(256)); colorbar; grid on
        
        ReflectivityRaw(:,:,k) = X;
        nomFic2 = ncFileName;
        
        CLimRaw  = [];
        CLimComp = [];
        if contains(nomFic2, '_WCSliceComp')
            try % TODO : r�gler le pb en amont
                CLim = Data3DMatrix.CLimComp;
            catch
                CLim = Data3DMatrix.CLim; % Cas obtenu lors de la r�alisation de WC-AlongNav
            end
            if isempty(CLim)
                CLim = [-10 25];
            end
            Data.CLimComp = CLim;
            Data.CLim     = CLim;
            Data3DMatrixRaw.CLimComp = CLim;
            Data3DMatrixRaw.CLim     = CLim;
            CLimComp = CLim;
        else
            try % TODO : r�gler le pb en amont
                CLim = Data3DMatrix.CLimRaw;
            catch
                CLim = Data3DMatrix.CLim;
            end
            if isempty(CLim)
                CLim = [-64 10];
            end
            Data.CLimRaw = CLim;
            Data.CLim    = CLim;
            Data3DMatrixRaw.CLimRaw = CLim;
            Data3DMatrixRaw.CLim    = CLim;
            CLimRaw = CLim;
        end

        if (mod(k, ceil(nbGroups/41)) == 1) && ~flagPingDisplayed
            figure(7567); imagesc(1:Data3DMatrix.Dimensions.nbPings, ZLim, X, CLim(:)');
            axis xy; xlabel('Pings'), ylabel('Depth'); colormap(jet(256)); colorbar;
            title(sprintf('%d/%d : %s', k, nbGroups, nomFic2), 'Interpreter', 'None')
            drawnow
        end
    end

    
    if isfield(Data3DMatrix, 'Mask')
        X = computeSlice(Data3DMatrix.Mask, sub);
        if all(X(:) == 0)
            SliceExists(k) = false;
            continue
        end
        ReflectivityMask(:,:,k) = (X > 0.5);
    end
end
if flagWaitbar
    my_close(hw, 'MsgEnd')
end
my_close(7567)

%% D�finition of the structure

if ~isempty(nomFicXmlRaw)
    [Info, Data] = createData(Data3DMatrixRaw, binsCentre, SliceExists, nomDirSlice);
elseif ~isempty(nomFicXmlComp)
    [Info, Data] = createData(Data3DMatrixComp, binsCentre, SliceExists, nomDirSlice);
else
    my_breakpoint
end

%% Export en XML-Bin ou Netcdf

Data.Dimensions = Info.Dimensions;
if nbLayers == 1
    if ~isempty(nomFicXmlRaw)
        Data.Reflectivity = ReflectivityRaw;
        SliceNames = {'ReflectivityRaw'};
    end
%     if ~isempty(nomFicXmlComp)
%         Data.Reflectivity = ReflectivityComp;
%         SliceNames = {'ReflectivityComp'};
%     end
    SliceUnits = {'dB'};
else
    Data.Reflectivity(:,:,:,1) = ReflectivityRaw;
    Data.Reflectivity(:,:,:,2) = ReflectivityComp;
    SliceNames = {'ReflectivityRaw'; 'ReflectivityComp'};
    SliceUnits = {'dB'; 'dB'};
end
Data.Dimensions.nbGroups = Data.Dimensions.nbSlices;
Data.Dimensions.nbSlices = nbLayers;

if isfield(Data3DMatrix, 'Mask')
    Data.Reflectivity(:,:,:,end+1) = ReflectivityMask;
    Data.Dimensions.nbSlices = Data.Dimensions.nbSlices + 1;
    SliceNames{end+1} = 'Mask';
    SliceUnits{end+1} = '';
    MaskSignification = {'After bottom detection'};
else
    MaskSignification = [];
end

flag = NetcdfGlobe3DUtils.createGlobeFlyTexture(ncFileName, Data, [], SliceNames, SliceUnits, ...
    'MaskSignification', MaskSignification, 'CLimRaw', CLimRaw, 'CLimComp', CLimComp);


function S = computeSlice(Reflectivity, sub)

% X = nanmean(single(Reflectivity(:,sub,:)),2);
% X = squeeze(X);

[n1, ~, n3] = size(Reflectivity);
S = zeros(n1, n3, 'single');
N = zeros(n1, n3, 'single');
% nsub = length(sub);
R = Reflectivity(:,sub,:);
R = singleUnlessDouble(R, NaN);
for k=1:length(sub)
    X = R(:,k,:);
    subNonNaN = ~isnan(X);
    S(subNonNaN) = S(subNonNaN) + X(subNonNaN);
    N(subNonNaN) = N(subNonNaN) + 1;
end
clear X
S = S ./ N;


function [Info, Data] = createData(Data3DMatrixRaw, binsCentre, SliceExists, nomDirSlice)

[~, MiddleSlice] = min(abs(binsCentre));

nbSlices = length(binsCentre);
nbPings  = Data3DMatrixRaw.Dimensions.nbPings;
nbRows   = Data3DMatrixRaw.Dimensions.nbRows;

Info.Signature1     = 'SonarScope';
Info.Signature2     = 'ImagesAlongNavigation';
Info.FormatVersion  = 20100624;
Info.Name           = Data3DMatrixRaw.Name;
Info.ExtractedFrom  = Data3DMatrixRaw.ExtractedFrom;

Info.Survey         = Data3DMatrixRaw.Survey;
Info.Vessel         = Data3DMatrixRaw.Vessel;
Info.Sounder        = Data3DMatrixRaw.Sounder;
Info.ChiefScientist = Data3DMatrixRaw.ChiefScientist;

Info.DepthTop       = 0; % == Data3DMatrixRaw.Z(end) == max(Data3DMatrixRaw.Z)
Info.DepthBottom    = Data3DMatrixRaw.Z(1); % == min(Data3DMatrixRaw.Z)
Info.MiddleSlice    = MiddleSlice;

Info.Dimensions.nbPings  = nbPings;
Info.Dimensions.nbSlices = nbSlices;
Info.Dimensions.nbRows   = nbRows;

Data.LatitudeNadir   = NaN(1, nbPings);
Data.LongitudeNadir  = NaN(1, nbPings);
Data.LatitudeTop     = NaN(nbSlices, nbPings);
Data.LongitudeTop    = NaN(nbSlices, nbPings);
% Data.LatitudeBottom  = NaN(nbSlices, nbPings);
% Data.LongitudeBottom = NaN(nbSlices, nbPings);
Data.PingNumber      = NaN(1, nbPings, 'single');
Data.AcrossDistance  = NaN(nbSlices, 1, 'single');
Data.Date            = NaN(1, nbPings, 'single');
Data.Hour            = NaN(1, nbPings, 'single');
% Info.Depth           = NaN(nbSlices, nbPings, 'single');

Data.AcrossDistance(:) = binsCentre;
Data.SliceExists       = SliceExists;

% TODO : bricolage abominable : il faut repositionner chaque ping en
% fonction de l'immarsion en amont
Immersion = Data3DMatrixRaw.Immersion(:)';
% ImmersionMoyenne = nanmean(Data3DMatrixRaw.Immersion(:));
Immersion(isnan(Immersion)) = 0;

% if isfield(Data3DMatrixRaw, 'Tide') && ~isempty(Data3DMatrixRaw.Tide) && any(~isnan(Data3DMatrixRaw.Tide))
%     Tide = Data3DMatrixRaw.Tide(:)';
% else
%     Tide = zeros(size(Immersion));
% end
Tide = zeros(size(Immersion)); % Modif JMA le 24/04/2020

Data.DepthTop    = zeros(1, nbPings, 'single') + Immersion; % Modif JM1 le 25/01/2021
Data.DepthBottom = ones( 1, nbPings, 'single') * min(Data3DMatrixRaw.Z) + Immersion; % Modif JMA le 29/09/2020

% Data.DepthTop  = Data.DepthTop    - Data3DMatrixRaw.Heave(:)' + Tide; ???
if isfield(Data3DMatrixRaw, 'Heave') && ~all(isnan(Data3DMatrixRaw.Heave(:)))
    Data.DepthBottom = Data.DepthBottom - Data3DMatrixRaw.Heave(:)' + Tide; % Car TranscucerDepth utilis� avant int�gre d�j� le Heave
else
    Data.DepthBottom = Data.DepthBottom + Tide; % Car TranscucerDepth utilis� avant int�gre d�j� le Heave
end

for k=1:nbPings
    %     Info.Depth(k,:)            = Data3DMatrixRaw.Depth % TODO
    if isfield(Data3DMatrixRaw, 'Datetime')
        T = cl_time('timeMat', datenum(Data3DMatrixRaw.Datetime(k)));
        Data.Date(k) = T.date;
        Data.Hour(k) = T.heure;
    else
        Data.Date(k) = Data3DMatrixRaw.Date(k);
        Data.Hour(k) = Data3DMatrixRaw.Hour(k);
    end
    Data.PingNumber(k)     = Data3DMatrixRaw.iPing(k);
    Data.LatitudeNadir(k)  = Data3DMatrixRaw.latCentre(k);
    Data.LongitudeNadir(k) = Data3DMatrixRaw.lonCentre(k);
    Data.LatitudeTop(:,k)  = interp1(double(Data3DMatrixRaw.AcrossDistance([1 end])), [Data3DMatrixRaw.latBab(k)  Data3DMatrixRaw.latTri(k)], binsCentre, 'linear', 'extrap');
    Data.LongitudeTop(:,k) = interp1(double(Data3DMatrixRaw.AcrossDistance([1 end])), [Data3DMatrixRaw.lonBab(k)  Data3DMatrixRaw.lonTri(k)], binsCentre, 'linear', 'extrap');
end

Info.Signals(1).Name          = 'Date';
Info.Signals(end).Dimensions  = '1, nbPings';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'Ifremer';
Info.Signals(end).FileName    = fullfile(nomDirSlice,'Date.bin');

Info.Signals(end+1).Name      = 'Hour';
Info.Signals(end).Dimensions  = '1, nbPings';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'nb milliseconds since midnight';
Info.Signals(end).FileName    = fullfile(nomDirSlice,'Hour.bin');

Info.Signals(end+1).Name      = 'PingNumber';
Info.Signals(end).Dimensions  = '1, nbPings';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = ' ';
Info.Signals(end).FileName    = fullfile(nomDirSlice,'PingNumber.bin');

Info.Signals(end+1).Name      = 'LatitudeNadir';
Info.Signals(end).Dimensions  = '1, nbPings';
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile(nomDirSlice,'LatitudeNadir.bin');

Info.Signals(end+1).Name      = 'LongitudeNadir';
Info.Signals(end).Dimensions  = '1, nbPings';
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile(nomDirSlice,'LongitudeNadir.bin');

Info.Signals(end+1).Name      = 'DepthTop';
Info.Signals(end).Dimensions  = '1, nbPings';
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile(nomDirSlice,'DepthTop.bin');

Info.Signals(end+1).Name      = 'DepthBottom';
Info.Signals(end).Dimensions  = '1, nbPings';
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile(nomDirSlice,'DepthBottom.bin');

Info.Signals(end+1).Name      = 'AcrossDistance';
Info.Signals(end).Dimensions  = 'nbSlices, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'm';
Info.Signals(end).FileName    = fullfile(nomDirSlice,'AcrossDistance.bin');

Info.Signals(end+1).Name      = 'SliceExists';
Info.Signals(end).Dimensions  = 'nbSlices, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile(nomDirSlice,'SliceExists.bin');

Info.Images(1).Name          = 'LatitudeTop';
Info.Images(end).Dimensions  = 'nbSlices, nbPings';
Info.Images(end).Storage     = 'double';
Info.Images(end).Unit        = 'deg';
Info.Images(end).FileName    = fullfile(nomDirSlice,'LatitudeTop.bin');

Info.Images(end+1).Name      = 'LongitudeTop';
Info.Images(end).Dimensions  = 'nbSlices, nbPings';
Info.Images(end).Storage     = 'double';
Info.Images(end).Unit        = 'deg';
Info.Images(end).FileName    = fullfile(nomDirSlice,'LongitudeTop.bin');

% Info.Images(end+1).Name      = 'Reflectivity';
% Info.Images(end).Dimensions  = 'nbRows, nbPings, nbSlices';
% Info.Images(end).Storage     = 'single';
% Info.Images(end).Unit        = 'dB';
% Info.Images(end).FileName    = fullfile(nomDirSlice,'Reflectivity.bin');


function flag = WC_SlicesAlongNavigation_unitaireNetcdf(nomFicXmlRaw, nomDirOut, flagWaitbar, varargin)

[varargin, Step] = getPropertyValue(varargin, 'Step', []); %#ok<ASGLU>

[~, nomFic] = fileparts(nomFicXmlRaw);

fprintf('Reading %s\n', nomFicXmlRaw)

[flag, Data3DMatrix] = WC_readImageCube3D(nomFicXmlRaw, 'Memmapfile', 1);
if ~flag
    return
end
    
if numel(Data3DMatrix.ReflectivityRaw) == 0
    str1 = sprintf('Echec de la cr�ation de slices pour le fichier  %s', nomFicXmlRaw);
    str2 = sprintf('Slice creation failed for  %s', nomFicXmlRaw);
    my_warndlg(Lang(str1,str2), 1);
    flag = 0;
    return
end
nbLayers = 3;

if isempty(Step)
    Step1 = mean(diff(Data3DMatrix.AcrossDistance));
    Step2 = range(Data3DMatrix.AcrossDistance) / 51;
    Step = max(Step1, Step2);
    Step = round(Step, 1, 'significant');
end

%{
figure;
for k=1:size(Data3DMatrixRaw.Reflectivity, 3)
    % TODO Data3DMatrixRaw.CLim
    imagesc(Data3DMatrixRaw.AcrossDistance, Data3DMatrixRaw.Z, Data3DMatrixRaw.Reflectivity(:,:,k), Data3DMatrixRaw.CLim);
    axis xy; axis equal; axis tight; title(sprintf('k=%d', k)); drawnow; colorbar
end
%}

bins = double(Data3DMatrix.AcrossDistance(1)):Step:double(Data3DMatrix.AcrossDistance(end)); % Reintroduit par JMA le 31/010/2018
binsCentre = centrage_magnetique(bins);
binsDeb = binsCentre - Step/2;
binsDeb(end+1) = binsDeb(end) + Step;
nbGroups = length(binsCentre);

%% Cr�ation du r�pertoire

nomFic = strrep(nomFic, '_PolarEchograms', '');
nomFic = strrep(nomFic, '_WCCube', '_WCSlice');
nomFic = strrep(nomFic, '_Cube',   '_WCSlice');
nomFic = strrep(nomFic, 'Raw',     '');
nomFic = strrep(nomFic, 'Comp',    '');
nomFic = strrep(nomFic, '__', '_');
if strcmp(nomFic(end), '_')
    nomFic(end) = [];
end
nomDirSlice = nomFic;
ncFileName = fullfile(nomDirOut, [nomFic '.g3d.nc']);

sep = getCSVseparator;
str = sprintf('%s%s%s%sStep=%sm', nomDirOut, sep, [nomFic '.nc'], sep, num2str(Step));
fprintf('Creating %s\n', str)
nomFicInfo = fullfile(nomDirOut, 'WC-AlongNav-StepValues.txt');
appendTxtFile(nomFicInfo, str);

%% Cr�ation des images

try
    X = Data3DMatrix.ReflectivityRaw(:,:,:);
    Data3DMatrix.ReflectivityRaw = X;
    clear X
catch
    my_breakpoint
end
ReflectivityRaw = NaN(Data3DMatrix.Dimensions.nbRows, Data3DMatrix.Dimensions.nbPings, nbGroups, 'single');
try
    X = Data3DMatrix.ReflectivityComp(:,:,:);
    Data3DMatrix.ReflectivityComp = X;
    clear X
catch
    my_breakpoint
end
ReflectivityComp = NaN(Data3DMatrix.Dimensions.nbRows, Data3DMatrix.Dimensions.nbPings, nbGroups, 'single');

try
    X = Data3DMatrix.Mask(:,:,:);
    Data3DMatrix.Mask = X;
    clear X
catch
    my_breakpoint
end
ReflectivityMask = zeros(Data3DMatrix.Dimensions.nbRows, Data3DMatrix.Dimensions.nbPings, nbGroups, 'single');

ZLim = [min(Data3DMatrix.Z) max(Data3DMatrix.Z)];

SliceExists = true(nbGroups,1);

str1 = 'Cr�ation des profils';
str2 = 'Creating profiles';
hw = create_waitbar(Lang(str1,str2), 'N', nbGroups, 'NoWaitbar', ~flagWaitbar);
for k=1:nbGroups
    my_waitbar(k, nbGroups, hw)
    
    sub = find((Data3DMatrix.AcrossDistance >= binsDeb(k)) & (Data3DMatrix.AcrossDistance <= binsDeb(k+1)));
    sub(sub < 1) = [];
    sub(sub > Data3DMatrix.Dimensions.nbColumns) = [];
    
    % TODO : faire le moyennage en valeur naturelle please !!!
    %     X = computeSlice(Data3DMatrixRaw.Reflectivity, sub);
    X = computeSlice(Data3DMatrix.ReflectivityRaw, sub);
    %     X = nanmean(single(Data3DMatrixRaw.Reflectivity(:,sub,:)),2);
    %     X = squeeze(X);
    
    if all(X(:) == 0)
        SliceExists(k) = false;
        continue
    end
    
    % TODO : certains pixels sont transparents dans le 3DV. peut-�tre
    % faudrait-il faire une interpolation 3x3 de X ???
    
    %     Data.Reflectivity(:,:,k) = X;
    
    %{
Ym = repmat(Data3DMatrix.Z(:), 1, Data3DMatrix.Dimensions.nbPings);
Ym = Ym - Data3DMatrix.Immersion(:)';
Xm = repmat(1:Data3DMatrix.Dimensions.nbPings, Data3DMatrix.Dimensions.nbRows, 1);
figure(67676); pcolor(Xm, Ym, X); colormap(jet(256)); colorbar; grid on; shading flat;
    %}
    %     figure(67676); imagesc(I); colormap(jet(256)); colorbar; grid on
    
    ReflectivityRaw(:,:,k) = X;
    nomFic2 = ncFileName;
    
    if flagWaitbar && (mod(k, ceil(nbGroups/41)) == 1)
        if isfield(Data3DMatrix, 'CLim')
            figure(7567); imagesc(1:Data3DMatrix.Dimensions.nbPings, ZLim, X, Data3DMatrix.CLim);
        else
            figure(7567); imagesc(1:Data3DMatrix.Dimensions.nbPings, ZLim, X);
        end
        axis xy; xlabel('Pings'), ylabel('Depth'); colormap(jet(256)); colorbar;
        title(sprintf('%d/%d : %s', k, nbGroups, nomFic2), 'Interpreter', 'None')
        drawnow
    end
    
    X = computeSlice(Data3DMatrix.ReflectivityComp, sub);
    if all(X(:) == 0)
        SliceExists(k) = false;
        continue
    end
    ReflectivityComp(:,:,k) = X;
    
    X = computeSlice(Data3DMatrix.Mask, sub);
    if all(X(:) == 0)
        SliceExists(k) = false;
        continue
    end
    ReflectivityMask(:,:,k) = (X > 0.5);
end
my_close(hw, 'MsgEnd')
my_close(7567)

%% D�finition of the structure

[Info, Data] = createData(Data3DMatrix, binsCentre, SliceExists, nomDirSlice);

%% Export en XML-Bin ou Netcdf

Data.Dimensions = Info.Dimensions;

Data.Reflectivity(:,:,:,1) = ReflectivityRaw;
Data.Reflectivity(:,:,:,2) = ReflectivityComp;
Data.Reflectivity(:,:,:,3) = ReflectivityMask;
SliceNames = {'ReflectivityRaw'; 'ReflectivityComp'; 'Mask'};
SliceUnits = {'dB'; 'dB'; ''};

Data.Dimensions.nbGroups = Data.Dimensions.nbSlices;
Data.Dimensions.nbSlices = nbLayers;

try % TODO : r�gler le pb en amont
    Data.CLimRaw = Data3DMatrix.CLimRaw;
catch
    Data.CLimRaw = Data3DMatrix.CLim;
end
try % TODO : r�gler le pb en amont
    Data.CLimComp = Data3DMatrix.CLimComp;
catch
    Data.CLimComp = Data3DMatrix.CLim; % Cas obtenu lors de la r�alisation de WC-AlongNav
end

MaskSignification = {'After bottom detection'};

flag = NetcdfGlobe3DUtils.createGlobeFlyTexture(ncFileName, Data, [], SliceNames, SliceUnits, ...
    'MaskSignification', MaskSignification, 'CLimRaw', Data.CLimRaw, 'CLimComp', Data.CLimComp);
