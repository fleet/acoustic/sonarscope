function flag = WC_SlicesAlongNavigation(nomFicXml, nomDirOut, varargin)

[varargin, Mute] = getPropertyValue(varargin, 'Mute', 0);
[varargin, Step] = getPropertyValue(varargin, 'Step', []); %#ok<ASGLU>

if ischar(nomFicXml)
    nomFicXml = {nomFicXml};
end

N = length(nomFicXml);
str1 = 'Cr�ation des Slices le long de la navigation';
str2 = 'Processing WC slices along navigation';
hw  = create_waitbar(Lang(str1,str2), 'N', N, 'Nowaitbar', Mute);
for k=1:length(nomFicXml)
    my_waitbar(k, N, hw)
    
    [~, ~, ExtIn] = fileparts(nomFicXml{k});
    if strcmp(ExtIn, '.nc')
        if contains(nomFicXml{k}, '_Comp')
            continue
        end
        flag = WC_SlicesAlongNavigation_unitaire(nomFicXml{k}, Step, nomDirOut, Mute, 'Layer', 'Raw'); %#ok<NASGU>
        flag = WC_SlicesAlongNavigation_unitaire(nomFicXml{k}, Step, nomDirOut, Mute, 'Layer', 'Comp');
    else
        if contains(nomFicXml{k}, '_WCCubeComp')
            flag = WC_SlicesAlongNavigation_unitaire(nomFicXml{k}, Step, nomDirOut, Mute, 'Layer', 'Comp');
        else
            flag = WC_SlicesAlongNavigation_unitaire(nomFicXml{k}, Step, nomDirOut, Mute, 'Layer', 'Raw');
        end
    end
end
my_close(hw, 'MsgEnd')


function flag = WC_SlicesAlongNavigation_unitaire(nomFicXml, Step, nomDirOut, Mute, varargin)

[varargin, Layer] = getPropertyValue(varargin, 'Layer', []); %#ok<ASGLU>

[nomDirXml, nomFicSeul] = fileparts(nomFicXml);
nomDirXml = fullfile(nomDirXml, [nomFicSeul '.zip']);
my_unzip(nomDirXml);

[flag, Data3DMatrix] = WC_readImageCube3D(nomFicXml, 'Memmapfile', -1);
if ~flag
    return
end

if isempty(Layer)
    Reflectivity = Data3DMatrix.Reflectivity(:,:,:);
    CLim = Data3DMatrix.CLim;
else
    switch Layer
        case 'Raw'
            try
                Reflectivity = Data3DMatrix.ReflectivityRaw(:,:,:);
                CLim = Data3DMatrix.CLimRaw;
            catch
                if isfield(Data3DMatrix, 'CLimRaw')
                    CLim = Data3DMatrix.CLimRaw;
                else
                    CLim = Data3DMatrix.CLim;
                end
                Reflectivity = Data3DMatrix.Reflectivity(:,:,:);
            end
        case 'Comp'
            try
                Reflectivity = Data3DMatrix.ReflectivityComp(:,:,:);
            catch
                Reflectivity = Data3DMatrix.Reflectivity(:,:,:);
            end
            if ~isfield(Data3DMatrix, 'CLimComp') || isempty(Data3DMatrix.CLimComp)
                Data3DMatrix.CLimComp = [-10 25]; % Data3DMatrix.CLim;
            end
            CLim = Data3DMatrix.CLimComp;
        otherwise
            my_breakpoint
    end
end

if numel(Reflectivity) == 0
    str1 = sprintf('Echec de la cr�ation de slices pour le fichier  %s', nomFicXml);
    str2 = sprintf('Slice creation failed for  %s', nomFicXml);
    my_warndlg(Lang(str1,str2), 1);
    flag = 0;
    return
end
AcrossDistance = Data3DMatrix.AcrossDistance(:,:);
%{
figure;
for k=1:size(Reflectivity, 3)
    imagesc(AcrossDistance, Data3DMatrix.Z, Reflectivity(:,:,k), CLim);
    axis xy; axis equal; axis tight; title(sprintf('k=%d', k)); drawnow; colorbar
end
%}

if isempty(Step)
    Step1 = mean(diff(AcrossDistance));
    Step2 = range(AcrossDistance) / 51;
    Step = max(Step1, Step2);
    Step = round(Step, 1, 'significant');
end

bins = double(AcrossDistance(1)):Step:double(AcrossDistance(end)); % Reintroduit par JMA le 31/010/2018
binsCentre = centrage_magnetique(bins);
binsDeb = binsCentre - Step/2;
binsDeb(end+1) = binsDeb(end) + Step;
nbSlices = length(binsCentre);

%% Cr�ation du r�pertoire

[~, nomFic] = fileparts(nomFicXml);
nomFic = strrep(nomFic, '_PolarEchograms', '');
nomFic = strrep(nomFic, '_WCCube', '_WCSlice');
nomFic = strrep(nomFic, '_Cube', '_WCSlice');
nomFic = strrep(nomFic, '__', '_');
nomFic = [nomFic Layer];
nomDirSlice = nomFic;

nomDirXml = fullfile(nomDirOut, nomFic);
nomFicXml = [nomDirXml '.xml'];
if ~exist(nomDirXml, 'dir')
    flag = mkdir(nomDirXml);
    if ~flag
        messageErreurFichier(nomDirXml, 'WriteFailure');
        return
    end
end

%% Cr�ation des images

sep = getCSVseparator;
str = sprintf('%s%s%s%sStep=%sm', nomDirOut, sep, [nomFic '.xml'], sep, num2str(Step));
fprintf('Creating %s\n', str)
nomFicInfo = fullfile(nomDirOut, 'WC-AlongNav-StepValues.txt');
appendTxtFile(nomFicInfo, str);

map = jet(256);
SliceExists = true(nbSlices,1);

str1 = 'Cr�ation des profils';
str2 = 'Creating profiles';
hw = create_waitbar(Lang(str1,str2), 'N', nbSlices, 'NoWaitbar', Mute);
for k=1:nbSlices
    my_waitbar(k, nbSlices, hw)
    
    sub = find((AcrossDistance >= binsDeb(k)) & (AcrossDistance <= binsDeb(k+1)));
    sub(sub < 1) = [];
    sub(sub > Data3DMatrix.Dimensions.nbColumns) = [];
    
    % TODO : faire le moyennage en valeur naturelle please !!!
    X = computeSlice(Reflectivity, sub);
%     X = nanmean(single(Reflectivity(:,sub,:)),2);
%     X = squeeze(X);
    
    if all(X(:) == 0)
        SliceExists(k) = false;
        continue
    end
    
    % TODO : certains pixels sont transparents dans le 3DVier. peut-�tre
    % faudrait-il faire une interpolation 3x3 de X ???
    
%     Data.Reflectivity(:,:,k) = X;

%{
Ym = repmat(Data3DMatrix.Z(:), 1, Data3DMatrix.Dimensions.nbPings);
Ym = Ym - Data3DMatrix.Immersion(:)';
Xm = repmat(1:Data3DMatrix.Dimensions.nbPings, Data3DMatrix.Dimensions.nbRows, 1);
figure(67676); pcolor(Xm, Ym, X); colormap(jet(256)); colorbar; grid on; shading flat;
%}
%     figure(67676); imagesc(I); colormap(jet(256)); colorbar; grid on

    %% Nom du r�pertoire

    nomSousRepertoire = sprintf('%03d', floor(k/100));
    nomDir3 = fullfile(nomDirXml, nomSousRepertoire);
    if ~exist(nomDir3, 'dir')
        flag = mkdir(nomDir3);
        if ~flag
            messageErreurFichier(nomDir3, 'WriteFailure');
            return
        end
    end
    
    %% Export .png
    
    nomFic1 = sprintf('%05d.png', k);
    nomFic2 = fullfile(nomDir3, nomFic1);
    I = gray2ind(mat2gray(X, double(CLim)), size(map,1));
    I(I == 0) = 1;
    %     I(I == 255) = 254;
    I(isnan(X)) = 0;
    I = flipud(I);
    imwrite(I, map, nomFic2, 'Transparency', 0)
    
    %% Export .tif
    
    nomFic1 = sprintf('%05d.tif', k);
    nomFic2 = fullfile(nomDir3, nomFic1);
    export_ImageTif32bits(X, nomFic2, 'Mute', 1);
    
    figure(7567); imagesc(1:Data3DMatrix.Dimensions.nbPings, Data3DMatrix.Z, X, CLim(:)');
    axis xy; xlabel('Pings'), ylabel('Depth'); colormap(jet(256)); colorbar;
    title(sprintf('%d/%d : %s', k, nbSlices, nomFic2), 'Interpreter', 'None')
    drawnow
end
my_close(hw, 'MsgEnd')
my_close(7567)

%% D�finition of the structure

[Info, Data] = createData(Data3DMatrix, binsCentre, SliceExists, nomDirSlice);

%% Cr�ation du fichier XML d�crivant la donn�e

xml_write(nomFicXml, Info);
flag = exist(nomFicXml, 'file');
if ~flag
    messageErreur(nomFicXml)
    return
end

%% Cr�ation du r�pertoire Info

nomDirInfo = nomDirXml;
if ~exist(nomDirInfo, 'dir')
    status = mkdir(nomDirInfo);
    if ~status
        messageErreur(nomDirInfo)
        flag = 0;
        return
    end
end

%% Cr�ation des fichiers binaires des signaux

for k=1:length(Info.Signals)
    flag = writeSignal(nomDirOut, Info.Signals(k), Data.(Info.Signals(k).Name));
    if ~flag
        return
    end
end

%% Cr�ation des fichiers binaires des signaux

for k=1:length(Info.Images)
    flag = writeImage(nomDirOut, Info.Images(k), Data.(Info.Images(k).Name));
    if ~flag
        return
    end
end
flag = 1;


function S = computeSlice(Reflectivity, sub)

% X = nanmean(single(Reflectivity(:,sub,:)),2);
% X = squeeze(X);

[n1, ~, n3] = size(Reflectivity);
S = zeros(n1, n3, 'single');
N = zeros(n1, n3, 'single');
% nsub = length(sub);
R = Reflectivity(:,sub,:);
for k=1:length(sub)
    X = R(:,k,:);
    subNonNaN = ~isnan(X);
    S(subNonNaN) = S(subNonNaN) + X(subNonNaN);
    N(subNonNaN) = N(subNonNaN) + 1;
end
clear X
S = S ./ N;




function [Info, Data] = createData(Data3DMatrix, binsCentre, SliceExists, nomDirSlice)

[~, MiddleSlice] = min(abs(binsCentre));

nbSlices = length(binsCentre);
nbPings  = Data3DMatrix.Dimensions.nbPings;
nbRows   = Data3DMatrix.Dimensions.nbRows;

Info.Signature1     = 'SonarScope';
Info.Signature2     = 'ImagesAlongNavigation';
Info.FormatVersion  = 20100624;
Info.Name           = Data3DMatrix.Name;
Info.ExtractedFrom  = Data3DMatrix.ExtractedFrom;

Info.Survey         = Data3DMatrix.Survey;
Info.Vessel         = Data3DMatrix.Vessel;
Info.Sounder        = Data3DMatrix.Sounder;
Info.ChiefScientist = Data3DMatrix.ChiefScientist;

Info.DepthTop       = 0;
Info.DepthBottom    = Data3DMatrix.Z(1);
Info.MiddleSlice    = MiddleSlice;

Info.Dimensions.nbPings  = nbPings;
Info.Dimensions.nbSlices = nbSlices;
Info.Dimensions.nbRows   = nbRows;

Data.LatitudeNadir   = NaN(1, nbPings);
Data.LongitudeNadir  = NaN(1, nbPings);
Data.LatitudeTop     = NaN(nbSlices, nbPings);
Data.LongitudeTop    = NaN(nbSlices, nbPings);
% Data.LatitudeBottom  = NaN(nbSlices, nbPings);
% Data.LongitudeBottom = NaN(nbSlices, nbPings);
Data.PingNumber      = NaN(1, nbPings, 'single');
Data.AcrossDistance  = NaN(nbSlices, 1, 'single');
Data.Date            = NaN(1, nbPings, 'single');
Data.Hour            = NaN(1, nbPings, 'single');
% Info.Depth           = NaN(nbSlices, nbPings, 'single');

Data.AcrossDistance(:) = binsCentre;
Data.SliceExists       = SliceExists;

% TODO : bricolage abominable : il faut repositionner chaque ping en
% fonction de l'immarsion en amont
Immersion = Data3DMatrix.Immersion(:)';
% ImmersionMoyenne = nanmean(Data3DMatrix.Immersion(:));
Immersion(isnan(Immersion)) = 0;

% if isfield(Data3DMatrix, 'Tide') && ~isempty(Data3DMatrix.Tide) && any(~isnan(Data3DMatrix.Tide))
%     Tide = Data3DMatrix.Tide(:)';
% else
%     Tide = zeros(size(Immersion));
% end
Tide = zeros(size(Immersion)); % Modif JMA le 24/04/2020

Data.DepthTop    = zeros(1, nbPings, 'single') + Immersion; % Modif JMA le 01/02/2021
Data.DepthBottom = ones( 1, nbPings, 'single') * min(Data3DMatrix.Z) + Immersion; % Modif JMA le 29/09/2020

if isfield(Data3DMatrix, 'Heave') && ~all(isnan(Data3DMatrix.Heave))
    Data.DepthBottom = Data.DepthBottom - Data3DMatrix.Heave(:)' + Tide; % Car TranscucerDepth utilis� avant int�gre d�j� le Heave
else
    Data.DepthBottom = Data.DepthBottom + Tide; % Car TranscucerDepth utilis� avant int�gre d�j� le Heave
end

AcrossDistance = Data3DMatrix.AcrossDistance(:);
for k=1:nbPings
    %     Info.Depth(k,:)  = Data3DMatrix.Depth % TODO
    Data.Date(k)           = Data3DMatrix.Date(k);
    Data.Hour(k)           = Data3DMatrix.Hour(k);
    Data.PingNumber(k)     = Data3DMatrix.iPing(k);
    Data.LatitudeNadir(k)  = Data3DMatrix.latCentre(k);
    Data.LongitudeNadir(k) = Data3DMatrix.lonCentre(k);
    Data.LatitudeTop(:,k)  = my_interp1(double(AcrossDistance([1 end])), [Data3DMatrix.latBab(k)  Data3DMatrix.latTri(k)], binsCentre, 'linear', 'extrap');
    Data.LongitudeTop(:,k) = my_interp1(double(AcrossDistance([1 end])), [Data3DMatrix.lonBab(k)  Data3DMatrix.lonTri(k)], binsCentre, 'linear', 'extrap');
end

Info.Signals(1).Name          = 'Date';
Info.Signals(end).Dimensions  = '1, nbPings';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'Ifremer';
Info.Signals(end).FileName    = fullfile(nomDirSlice,'Date.bin');

Info.Signals(end+1).Name      = 'Hour';
Info.Signals(end).Dimensions  = '1, nbPings';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'nb milliseconds since midnight';
Info.Signals(end).FileName    = fullfile(nomDirSlice,'Hour.bin');

Info.Signals(end+1).Name      = 'PingNumber';
Info.Signals(end).Dimensions  = '1, nbPings';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = ' ';
Info.Signals(end).FileName    = fullfile(nomDirSlice,'PingNumber.bin');

Info.Signals(end+1).Name      = 'LatitudeNadir';
Info.Signals(end).Dimensions  = '1, nbPings';
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile(nomDirSlice,'LatitudeNadir.bin');

Info.Signals(end+1).Name      = 'LongitudeNadir';
Info.Signals(end).Dimensions  = '1, nbPings';
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile(nomDirSlice,'LongitudeNadir.bin');

Info.Signals(end+1).Name      = 'DepthTop';
Info.Signals(end).Dimensions  = '1, nbPings';
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile(nomDirSlice,'DepthTop.bin');

Info.Signals(end+1).Name      = 'DepthBottom';
Info.Signals(end).Dimensions  = '1, nbPings';
Info.Signals(end).Storage     = 'double';
Info.Signals(end).Unit        = 'deg';
Info.Signals(end).FileName    = fullfile(nomDirSlice,'DepthBottom.bin');

Info.Signals(end+1).Name      = 'AcrossDistance';
Info.Signals(end).Dimensions  = 'nbSlices, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = 'm';
Info.Signals(end).FileName    = fullfile(nomDirSlice,'AcrossDistance.bin');

Info.Signals(end+1).Name      = 'SliceExists';
Info.Signals(end).Dimensions  = 'nbSlices, 1';
Info.Signals(end).Storage     = 'single';
Info.Signals(end).Unit        = '';
Info.Signals(end).FileName    = fullfile(nomDirSlice,'SliceExists.bin');

Info.Images(1).Name          = 'LatitudeTop';
Info.Images(end).Dimensions  = 'nbSlices, nbPings';
Info.Images(end).Storage     = 'double';
Info.Images(end).Unit        = 'deg';
Info.Images(end).FileName    = fullfile(nomDirSlice,'LatitudeTop.bin');

Info.Images(end+1).Name      = 'LongitudeTop';
Info.Images(end).Dimensions  = 'nbSlices, nbPings';
Info.Images(end).Storage     = 'double';
Info.Images(end).Unit        = 'deg';
Info.Images(end).FileName    = fullfile(nomDirSlice,'LongitudeTop.bin');

% Info.Images(end+1).Name      = 'Reflectivity';
% Info.Images(end).Dimensions  = 'nbRows, nbPings, nbSlices';
% Info.Images(end).Storage     = 'single';
% Info.Images(end).Unit        = 'dB';
% Info.Images(end).FileName    = fullfile(nomDirSlice,'Reflectivity.bin');
