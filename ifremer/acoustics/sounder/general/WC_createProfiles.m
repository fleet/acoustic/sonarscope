% nomFicXml = 'C:\Temp\Marmesonet\ProfilAvecWC\0307_gluVoxlerRAW.xml'; Step = 20;
% nomFicXml = 'C:\KM_Ssc\EM302_UNH\0009_20090716_064152_EX_VoxlerRAW.xml'; Step = 50;
% nomFicXml = 'C:\KM_Ssc\EM302_UNH\WC_Vox_RAW_0009_20090716_064152_EX\PolarEchograms.xml'; Step = 50;
% nomFicXml =
% 'C:\Temp\Marmesonet\ProfilAvecWC\WC_Vox_RAW_0306_20091113_160950_Lesuroit\PolarEchograms.xml'; Step = 50;
% nomFicXml = 'C:\KM_Ssc\EM302_UNH\0009_cratere_EX_VoxlerRAW.xml'; Step = 50;
% nomFicXml = 'C:\KM_Ssc\EM302_UNH\0009_Middle_VoxlerRAW.xml'; Step = 50;
% nomFicXml = 'C:\Temp\A0009_20090716_064152_EX_Comp_VoxlerBin.xml'; Step = 50;
% WC_createProfiles(nomFicXml, Step)

function WC_createProfiles(nomFicXml, Step)

[flag, Data] = WC_readImageCube3D(nomFicXml, 'Memmapfile', 1);
if ~flag
    return
end

figure; imagesc(Data.AcrossDistance, Data.Depth, Data.Image(:,:,1)'); axis xy; axis equal; axis tight;

bins = Data.AcrossDistance(1):Step:Data.AcrossDistance(end);
bins = centrage_magnetique(bins) - Step/2;
bins = bins(2:end);
N = length(bins)-1;

%% Cr�ation du r�pertoire

[nomDir, nomFic] = fileparts(nomFicXml);
nomFic = strrep(nomFic, '_VoxlerRAW', '');
nomDirXml = fullfile(nomDir, [nomFic '_WC_Profiles']);
nomFicXml = [nomDirXml '.xml'];
if ~exist(nomDirXml, 'dir')
    flag = mkdir(nomDirXml);
    if ~flag
        messageErreurFichier(nomDirXml, 'WriteFailure');
        return
    end
end

for i=0:floor(N/100)
    nomDirImages = fullfile(nomDirXml, sprintf('%03d', i));
    if ~exist(nomDirImages, 'dir')
        flag = mkdir(nomDirImages);
        if ~flag
            messageErreurFichier(nomDirImages, 'WriteFailure');
            return
        end
    end
end

%% Cr�ation des images

map = jet(256);
str1 = 'Cr�ation des profils';
str2 = 'Creating profiles';
hw = create_waitbar(Lang(str1,str2), 'N', N);
for k=1:N
    my_waitbar(k, N, hw)
    nomDirImages = fullfile(nomDirXml, sprintf('%03d', floor(i/100)));
    nomFicProfile = sprintf('%05d.png', k);
    nomFicImage = fullfile(nomDirImages, nomFicProfile);
    
    sub = find((Data.AcrossDistance >= bins(k)) & (Data.AcrossDistance <= bins(k+1)));
    sub(sub < 1) = [];
    sub(sub > Data.Dimensions.nbColumns) = [];

    X = mean(single(Data.Image(sub,:,:)), 1, 'omitnan');
    X = squeeze(X);

    figure(7567); imagesc(1:Data.Dimensions.nbPings, Data.Depth, X); axis xy; xlabel('Pings'), ylabel('Depth'); colorbar;
    drawnow
    
    X = flipud(X);
    I = gray2ind(mat2gray(X, [0 255]), size(map,1));
    try
        imwrite(I, map, nomFicImage, 'Transparency', 0)
    catch %#ok<CTCH>
        messageErreurFichier(nomFicImage, 'WriteFailure');
        return
    end
end
my_close(hw)

% kMilieu = floor(Data.Dimensions.nbPings/2);
for k=1:N
%     WC.lonBab(k)    =  Data.lonCentre(1);
%     WC.latBab(k)    =  Data.latCentre(1);
%     WC.lonCentre(k) =  Data.lonCentre(kMilieu);
%     WC.latCentre(k) =  Data.latCentre(kMilieu);
%     WC.lonTri(k)    =  Data.lonCentre(Data.Dimensions.nbPings);
%     WC.latTri(k)    =  Data.latCentre(Data.Dimensions.nbPings);
    WC.iPing(k)     =  k;
    WC.xBab(k)      =  NaN;
    WC.xTri(k)      =  NaN;
    WC.Depth(k)     =  min(Data.Depth(:));
    WC.Date(k)      =  Data.Date(1);
    WC.Hour(k)      =  Data.Hour(1);
    
    WC.lonBabDepointe(k) =  NaN;
    WC.latBabDepointe(k) =  NaN;
    WC.lonTriDepointe(k) =  NaN;
    WC.latTriDepointe(k) =  NaN;
end

WC.latVert = NaN(Data.Dimensions.nbPings,N);
WC.lonVert = NaN(Data.Dimensions.nbPings,N);
x = (bins(1:N)+bins(2:N+1)) / 2;
for iPing=1:Data.Dimensions.nbPings
    WC.latVert(iPing,:) = interp1(Data.AcrossDistance([1 end]), [Data.latBab(iPing)  Data.latTri(iPing)], x);
    WC.lonVert(iPing,:) = interp1(Data.AcrossDistance([1 end]), [Data.lonBab(iPing)  Data.lonTri(iPing)], x);
end
kMilieu = floor(Data.Dimensions.nbPings/2);
for k=1:N
    WC.lonBab(k)    =  WC.lonVert(1,k);
    WC.latBab(k)    =  WC.latVert(1,k);
    WC.lonCentre(k) =  WC.lonVert(kMilieu,k);
    WC.latCentre(k) =  WC.latVert(kMilieu,k);
    WC.lonTri(k)    =  WC.lonVert(Data.Dimensions.nbPings,k);
    WC.latTri(k)    =  WC.latVert(Data.Dimensions.nbPings,k);  
end
WC.nomFicAll = Data.ExtractedFrom;
WC.nbPings   = N;
WC.Name      = Data.Name;

% nomDirExport = Data.nomDirExport;
Survey.Name           = 'Unkown';
Survey.Vessel         = 'Unkown';
Survey.Sounder        = 'Unkown';
Survey.ChiefScientist = 'Unkown';

% WC.latVert et WC.lonVert a int�grer dans WC_writeXmlPng
flag = WC_writeXmlPng(nomFicXml, WC, CLim, Survey, Data.Unit);

%{
            iPing: [1x40 double]
              xBab: [1x40 double] NaN
              xTri: [1x40 double] NaN
             Depth: [1x40 single]
              Date: [1x40 double]
              Hour: [1x40 double]
    lonBabDepointe: [1x40 double] NAN
    latBabDepointe: [1x40 double] NAN
    lonTriDepointe: [1x40 double] NAN
    latTriDepointe: [1x40 double] NAN
           latVert: [6x40 double]
           lonVert: [6x40 double]
            lonBab: [1x40 double] WC.lonVert(1,k);
            latBab: [1x40 double] WC.latVert(1,k);
         lonCentre: [1x40 double] WC.lonVert(kMilieu,k);
         latCentre: [1x40 double] WC.latVert(kMilieu,k);
            lonTri: [1x40 double] WC.lonVert(Data.Dimensions.nbPings,k);
            latTri: [1x40 double] WC.latVert(Data.Dimensions.nbPings,k);
         nomFicAll: 'C:\Temp\Marmesonet\ProfilAvecWC\0307_20091113_163950_Lesuroit.all'
           nbPings: 40
              Name: '0307_gluVoxlerRAW'
%}

