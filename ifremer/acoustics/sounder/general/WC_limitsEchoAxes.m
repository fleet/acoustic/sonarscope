function Points = WC_limitsEchoAxes(Points)

if isempty(Points.Z)
    return
end

Titre = Lang('Echos d�tect�s', 'Echoes detected');
fig = FigUtils.createSScFigure; PlotUtils.createSScPlot(Points.AcrossDistance, Points.Z, 'o'); grid on; axis equal;
xlabel('Across distance (m)'); ylabel('Depth (m)'); title(Titre);

%% Filtrage sur les coordonn�es

minz = min(Points.Z);
minx = min(Points.AcrossDistance);
maxx = max(Points.AcrossDistance);

str1 = 'Profondeur max';
str2 = 'Max depth';
str3 = 'AcrossDist min';
str4 = 'Min AcrossDist';
str5 = 'AcrossDist min';
str6 = 'Min AcrossDist';
p    = ClParametre('Name', Lang(str1,str2), 'Unit', 'm', 'Value', 0,    'MinValue', minz, 'MaxValue', 0);
p(2) = ClParametre('Name', Lang(str3,str4), 'Unit', 'm', 'Value', maxx, 'MinValue', minx, 'MaxValue', maxx);
p(3) = ClParametre('Name', Lang(str5,str6), 'Unit', 'm', 'Value', minx, 'MinValue', minx, 'MaxValue', maxx);
str1 = 'Limites des Echos';
str2 = 'WC Limits of Echoes';
a = StyledSimpleParametreDialog('params', p, 'Title', Lang(str1,str2));
a.openDialog;
flag = a.okPressedOut;
if ~flag
    return
end
val = a.getParamsValue;

sub = (Points.AcrossDistance >= val(3)) & (Points.AcrossDistance <= val(2)) & (Points.Z <= val(1));
Points.AcrossDistance = Points.AcrossDistance(sub);
Points.Z              = Points.Z(sub);
Points.XCoor          = Points.XCoor(sub);
Points.YCoor          = Points.YCoor(sub);
Points.Longitude      = Points.Longitude(sub);
Points.Latitude       = Points.Latitude(sub);
Points.Angle          = Points.Angle(sub);
Points.RangeInSamples = Points.RangeInSamples(sub);
Points.Energie        = Points.Energie(sub);
Points.iPing          = Points.iPing(sub);
Points.EnergieTotale  = Points.EnergieTotale(sub);
Points.Date           = Points.Date(sub);
Points.Hour           = Points.Hour(sub);
Points.Celerite       = Points.Celerite(sub);
         
FigUtils.createSScFigure(fig); PlotUtils.createSScPlot(Points.AcrossDistance, Points.Z, 'o'); grid on; axis equal;
xlabel('Across distance (m)'); ylabel('Depth (m)'); title(Titre);

%% Filtrage sur les �nergies

histo1D(Points.Energie, 'Titre', Lang('Histogramme des �chos d�tect�s', 'Histogram of detected echos'));
GCF = gcf;
StatValues = stats(Points.Energie);
minx = StatValues.Quant_01_99(1);
maxx = StatValues.Quant_01_99(2);
[flag, val] = saisie_CLim(minx, maxx, 'dB', 'SignalName', 'Echoes level', 'hFigHisto', GCF);
if ~flag
    return
end

sub = (Points.Energie < val(2));
Points.Energie(sub) = val(2);
sub = (Points.Energie > val(1));
Points.Energie(sub) = val(1);

my_close(fig)
my_close(GCF)

