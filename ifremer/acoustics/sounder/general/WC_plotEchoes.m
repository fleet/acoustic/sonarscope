% Plot echoes (or soundings) given in a structure
%
% Syntax
%   WC_plotEchoes(Points, ...)
%
% Input Arguments
%   Points    : Structure containing the data
%               -- Mandatory fields --
%               Latitude       : Latitudes of the echoes
%               Longitude      : Longitudes of the echoes
%               Z              : Depths of the echoes
%               -- Optional fiels --
%               BaseForm       : Pastille ou cube : TODo � compl�ter
%               Date           : Date (Matlab reference)
%               Hour           : Nb of milliseconds from midnight
%               iPing          : Ping number
%               Energie        : Energy of the echoes
%               EnergieTotale  : Energy of the echoes of the ping
%               XCoor          : Abscissa in a given projection
%               YCoor          : Ordinates in a given projection
%               Angle          : Beam angles of the echoes
%               RangeInSamples : Range from the antenna to the echoes
%               AcrossDistance : Across distance
%               Celerite       : Celerity
%
% Name-Value Pair Arguments
%   Name : String to add in the figure name
%
% Examples
%   Points.Longitude = rand(100, 1);
%   Points.Latitude  = rand(100, 1);
%   Points.Z         = rand(100, 1);
%   WC_plotEchoes(Points)
%
% See also WC_SaveEchoes XMLBinUtils.readGrpData sonar_SimradProcessWC sonar_exportSoundingsToSSc3DV WC_SaveEchoes Authors
% Authors  : JMA
%-------------------------------------------------------------------------------

function WC_plotEchoes(Points, varargin)

[varargin, Name] = getPropertyValue(varargin, 'Name', []); %#ok<ASGLU>

if length(Points.Z) <= 1
    return
end

if isfield(Points, 'Energie')
    Titre = Lang('Echos d�tect�s', 'Echoes detected');
    [N, bins, ~, sub] = histo1D(Points.Energie, 'Titre', Lang('Histogramme des �chos d�tect�s', 'Histogram of detected echos'));
    C = jet(length(N));
else
    Titre = Lang('Echos d�tect�s', 'Echoes detected');
    [N, bins, ~, sub] = histo1D(Points.Z, 'Titre', Lang('Histogramme des Z', 'Histogram of Z'));
    C = jet(length(N));
end

%%

FigUtils.createSScFigure;
for i=1:length(N)
    if ~isempty(sub{i})
        h = PlotUtils.createSScPlot(Points.Longitude(sub{i}), Points.Latitude(sub{i}), 'o'); grid on; axisGeo; hold on;
        set(h, 'Color', C(i,:))
    end
end
hold off;
xlabel('Lon (deg)'); ylabel('Lat (deg)'); title('Carto');

%%

if isempty(Name)
    FigUtils.createSScFigure;
else
    FigUtils.createSScFigure('Name', [' - ' Name]);
end
if isfield(Points, 'EnergieTotale')
    ha = subplot(2, 3, 1);
    PlotUtils.createSScPlot(ha, Points.iPing, Points.EnergieTotale, '-*'); grid on; xlabel('Pings'); title('Total Energy')
end

if isfield(Points, 'AcrossDistance')
    ha = subplot(2, 3, 2:3);
    for i=1:length(N)
        if ~isempty(sub{i})
            h = PlotUtils.createSScPlot(ha, Points.AcrossDistance(sub{i}), Points.Z(sub{i}), 'o'); grid on; axis equal; hold on;
            set(h, 'Color', C(i,:))
        end
    end
    hold off;
    xlabel('Across distance (m)'); ylabel('Depth (m)'); title(Titre);
    my_colorbar('ZLim', [bins(1) bins(end)]);
end


ha = subplot(2, 3, 4);
for i=1:length(N)
    if ~isempty(sub{i})
        h = PlotUtils.createSScPlot(ha, Points.Longitude(sub{i}), Points.Latitude(sub{i}), 'o'); grid on; axisGeo; hold on;
        set(h, 'Color', C(i,:))
    end
end
hold off;
xlabel('Lon (deg)'); ylabel('Lat (deg)'); title('Carto');
% my_colorbar('ZLim', [bins(1) bins(end)]);

if isfield(Points, 'XCoor')
    ha = subplot(2, 3, 5:6);
    xmin = min(Points.XCoor);
    ymin = min(Points.YCoor);
    for i=1:length(N)
        if ~isempty(sub{i})
            h = PlotUtils.createSScPlot3(ha, Points.XCoor(sub{i})-xmin, Points.YCoor(sub{i})-ymin, Points.Z(sub{i}), 'o'); grid on; axis equal; hold on;
            set(h, 'Color', C(i,:))
        end
    end
    hold off;
    xlabel('x (m)'); ylabel('y (m)'); title('3D');
    % my_colorbar('ZLim', [bins(1) bins(end)]);
end
