% nomFicXml = 'D:\Temp\SPFE\ATraiter\L21_ForTEST\WC-3DMatrix\0021_20180306_210305_SimonStevin_WCCubeRaw_01.xml';
% [flag, Data] = WC_readImageCube3D(nomFicXml, 'Memmapfile', -1)
% figure; imagesc(Data.AcrossDistance, Data.Depth, Data.Reflectivity(:,:,1)); axis xy
%{
figure;
for k=1:size(Data.Reflectivity, 3)
    imagesc(Data.AcrossDistance, Data.Depth, Data.Reflectivity(:,:,k)); colormap(jet(256)); axis xy; axis equal;axis tight;  drawnow
end
%}

function [flag, Data, nomFicXml] = WC_readImageCube3D(nomFicXml, varargin)

[varargin, Memmapfile] = getPropertyValue(varargin, 'Memmapfile', -1); %#ok<ASGLU>

[~, ~, Ext] = fileparts(nomFicXml);
if strcmp(Ext, '.nc')
    [flag, Data] = NetcdfUtils.readGrpData(nomFicXml, 'WaterColumnCube3D'); %'PolarEchograms');
    if ~flag
        return
    end
else
    [flag, Data] = XMLBinUtils.readGrpData(nomFicXml, 'Memmapfile', Memmapfile);
    if ~flag
        return
    end
end

%% Contr�le des signatures

if strcmp(Ext, '.xml')
    if ~isfield(Data, 'Signature1') || ~strcmp(Data.Signature1, 'SonarScope')
        str1 = '"Signature1" n''est pas "SonarScope"';
        str2 = '"Signature1" is not "SonarScope"';
        my_warndlg(Lang(str1,str2), 1);
        flag = 0;
        return
    end
    if ~isfield(Data, 'Signature2') || ~strcmp(Data.Signature2, 'WaterColumnCube3D')
        str1 = '"Signature2" n''est pas "WaterColumnCube3D"';
        str2 = '"Signature2" is not "WaterColumnCube3D"';
        my_warndlg(Lang(str1,str2), 1);
        flag = 0;
        return
    end
end

%% Coordonn�es

if strcmp(Ext, '.xml')
%     Data.AcrossDistance = linspace(Data.xBab(1), Data.xTri(1), Data.Dimensions.nbColumns);
else % TODO : bidouilles pour essayer de brancher le calcul des slicesAlongNavigation sur le fichier 3DMatrix en entr�e est en Netcdf
    [nbRows, nbColumns, nbPings] = size(Data.ReflectivityRaw);
    Data.Dimensions.nbColumns = nbColumns;
    Data.Dimensions.nbRows    = nbRows;
    Data.Dimensions.nbPings   = nbPings;
end    
Data.Z = linspace(Data.Depth(1)-Data.Immersion(1), 0, Data.Dimensions.nbRows); % Modif JMA le 17/02/2021

%{
for k=1:5
    kSlice = k * floor(size(Data.Reflectivity,3) / 5);
    figure; imagesc(Data.AcrossDistance, Data.Z, Data.Reflectivity(:,:,kSlice)); axis xy
end
for k=1:5
    kSlice = k * floor(size(Data.ReflectivityRaw,3) / 5);
    figure; imagesc(Data.AcrossDistance, Data.Z, Data.ReflectivityRaw(:,:,kSlice)); axis xy
end
%}

% if ~isfield(Data, 'Unit')
%     Data = 'TODO'; % TODO
% end
