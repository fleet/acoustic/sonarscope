function flag = WC_writeXmlCube(nomFicXml, WC, Survey, CLim, Unit, varargin)

[varargin, Layer]    = getPropertyValue(varargin, 'Layer',    []);
[varargin, flagMask] = getPropertyValue(varargin, 'flagMask', 0); %#ok<ASGLU>

[nomDirRacine, TypeData] = fileparts(nomFicXml);

%% G�n�ration du XML SonarScope

PolarEchograms.Signature1    = 'SonarScope';
PolarEchograms.Signature2    = 'WaterColumnCube3D';
PolarEchograms.Name          = WC.Name;
PolarEchograms.ExtractedFrom = WC.nomFicAll;
PolarEchograms.nomDirExport  = nomDirRacine; % EST-CE TOUJOURS UTILE ????
% if isfield(WC, 'nomFicRAW')
%     PolarEchograms.nomFicRAW   = WC.nomFicRAW;
%     PolarEchograms.RawDataType = WC.RawDataType;
% end
PolarEchograms.FormatVersion = 20100928;

PolarEchograms.CLim = CLim;

if ~isempty(Layer)
    switch Layer
        case 'Raw'
            PolarEchograms.CLimRaw = CLim;
        case 'Comp'
            PolarEchograms.CLimComp = CLim;
    end
end

PolarEchograms.Survey         = Survey.Name;
PolarEchograms.Vessel         = Survey.Vessel;
PolarEchograms.Sounder        = Survey.Sounder;
PolarEchograms.ChiefScientist = Survey.ChiefScientist;

% PolarEchograms.ImageUnit = Unit; % Incompatible avec 3DViewer pour le moment
PolarEchograms.ChiefScientist = Unit; % En attendant que tout le monde ait la version 3DV compatible

PolarEchograms.Dimensions.nbPings = WC.nbPings;
if isfield(WC, 'nbColumns')
    PolarEchograms.Dimensions.nbColumns = WC.nbColumns;
    PolarEchograms.Dimensions.nbRows    = WC.nbRows;
end
if isfield(WC, 'latVert')
    PolarEchograms.Dimensions.nbProfiles = size(WC.latVert, 1);
else
    PolarEchograms.Dimensions.nbProfiles = 1;
end
if isfield(WC, 'SoundingsRangeInMeters')
    PolarEchograms.Dimensions.nbSoundings = size(WC.SoundingsRangeInMeters, 2);
end

PolarEchograms.Signals(1).Name          = 'Date';
PolarEchograms.Signals(end).Dimensions  = 'nbPings, 1';
PolarEchograms.Signals(end).Storage     = 'double';
PolarEchograms.Signals(end).Unit        = 'Julian Day';
PolarEchograms.Signals(end).FileName    = fullfile(TypeData,'Date.bin');

PolarEchograms.Signals(end+1).Name      = 'Hour';
PolarEchograms.Signals(end).Dimensions  = 'nbPings, 1';
PolarEchograms.Signals(end).Storage     = 'double';
PolarEchograms.Signals(end).Unit        = 'nb millis / midnight';
PolarEchograms.Signals(end).FileName    = fullfile(TypeData,'Hour.bin');

PolarEchograms.Signals(end+1).Name      = 'iPing';
PolarEchograms.Signals(end).Dimensions  = 'nbPings, 1';
PolarEchograms.Signals(end).Storage     = 'single';
PolarEchograms.Signals(end).Unit        = '';
PolarEchograms.Signals(end).FileName    = fullfile(TypeData,'iPing.bin');

PolarEchograms.Signals(end+1).Name      = 'xBab';
PolarEchograms.Signals(end).Dimensions  = 'nbPings, 1';
PolarEchograms.Signals(end).Storage     = 'single';
PolarEchograms.Signals(end).Unit        = 'm';
PolarEchograms.Signals(end).FileName    = fullfile(TypeData,'xBab.bin');

PolarEchograms.Signals(end+1).Name      = 'xTri';
PolarEchograms.Signals(end).Dimensions  = 'nbPings, 1';
PolarEchograms.Signals(end).Storage     = 'single';
PolarEchograms.Signals(end).Unit        = 'm';
PolarEchograms.Signals(end).FileName    = fullfile(TypeData,'xTri.bin');

PolarEchograms.Signals(end+1).Name      = 'Depth';
PolarEchograms.Signals(end).Dimensions  = 'nbPings, 1';
PolarEchograms.Signals(end).Storage     = 'single';
PolarEchograms.Signals(end).Unit        = 'm';
PolarEchograms.Signals(end).FileName    = fullfile(TypeData,'Depth.bin');

PolarEchograms.Signals(end+1).Name      = 'Immersion';
PolarEchograms.Signals(end).Dimensions  = 'nbPings, 1';
PolarEchograms.Signals(end).Storage     = 'single';
PolarEchograms.Signals(end).Unit        = 'm';
PolarEchograms.Signals(end).FileName    = fullfile(TypeData,'Immersion.bin');

PolarEchograms.Signals(end+1).Name      = 'Heave';
PolarEchograms.Signals(end).Dimensions  = 'nbPings, 1';
PolarEchograms.Signals(end).Storage     = 'single';
PolarEchograms.Signals(end).Unit        = 'm';
PolarEchograms.Signals(end).FileName    = fullfile(TypeData,'Heave.bin');

PolarEchograms.Signals(end+1).Name      = 'lonBab';
PolarEchograms.Signals(end).Dimensions  = 'nbPings, 1';
PolarEchograms.Signals(end).Storage     = 'double';
PolarEchograms.Signals(end).Unit        = 'deg';
PolarEchograms.Signals(end).FileName    = fullfile(TypeData,'lonBab.bin');

PolarEchograms.Signals(end+1).Name      = 'latBab';
PolarEchograms.Signals(end).Dimensions  = 'nbPings, 1';
PolarEchograms.Signals(end).Storage     = 'double';
PolarEchograms.Signals(end).Unit        = 'deg';
PolarEchograms.Signals(end).FileName    = fullfile(TypeData,'latBab.bin');

PolarEchograms.Signals(end+1).Name      = 'lonCentre';
PolarEchograms.Signals(end).Dimensions  = 'nbPings, 1';
PolarEchograms.Signals(end).Storage     = 'double';
PolarEchograms.Signals(end).Unit        = 'deg';
PolarEchograms.Signals(end).FileName    = fullfile(TypeData,'lonCentre.bin');

PolarEchograms.Signals(end+1).Name      = 'latCentre';
PolarEchograms.Signals(end).Dimensions  = 'nbPings, 1';
PolarEchograms.Signals(end).Storage     = 'double';
PolarEchograms.Signals(end).Unit        = 'deg';
PolarEchograms.Signals(end).FileName    = fullfile(TypeData,'latCentre.bin');

PolarEchograms.Signals(end+1).Name      = 'lonTri';
PolarEchograms.Signals(end).Dimensions  = 'nbPings, 1';
PolarEchograms.Signals(end).Storage     = 'double';
PolarEchograms.Signals(end).Unit        = 'deg';
PolarEchograms.Signals(end).FileName    = fullfile(TypeData,'lonTri.bin');

PolarEchograms.Signals(end+1).Name      = 'latTri';
PolarEchograms.Signals(end).Dimensions  = 'nbPings, 1';
PolarEchograms.Signals(end).Storage     = 'double';
PolarEchograms.Signals(end).Unit        = 'deg';
PolarEchograms.Signals(end).FileName    = fullfile(TypeData,'latTri.bin');

PolarEchograms.Signals(end+1).Name      = 'lonBabDepointe';
PolarEchograms.Signals(end).Dimensions  = 'nbPings, 1';
PolarEchograms.Signals(end).Storage     = 'double';
PolarEchograms.Signals(end).Unit        = 'deg';
PolarEchograms.Signals(end).FileName    = fullfile(TypeData,'lonBabDepointe.bin');

PolarEchograms.Signals(end+1).Name      = 'latBabDepointe';
PolarEchograms.Signals(end).Dimensions  = 'nbPings, 1';
PolarEchograms.Signals(end).Storage     = 'double';
PolarEchograms.Signals(end).Unit        = 'deg';
PolarEchograms.Signals(end).FileName    = fullfile(TypeData,'latBabDepointe.bin');

PolarEchograms.Signals(end+1).Name      = 'lonTriDepointe';
PolarEchograms.Signals(end).Dimensions  = 'nbPings, 1';
PolarEchograms.Signals(end).Storage     = 'double';
PolarEchograms.Signals(end).Unit        = 'deg';
PolarEchograms.Signals(end).FileName    = fullfile(TypeData,'lonTriDepointe.bin');

PolarEchograms.Signals(end+1).Name      = 'latTriDepointe';
PolarEchograms.Signals(end).Dimensions  = 'nbPings, 1';
PolarEchograms.Signals(end).Storage     = 'double';
PolarEchograms.Signals(end).Unit        = 'deg';
PolarEchograms.Signals(end).FileName    = fullfile(TypeData,'latTriDepointe.bin');

if isfield(WC, 'Tide')
    PolarEchograms.Signals(end+1).Name     = 'Tide';
    PolarEchograms.Signals(end).Dimensions = 'nbPings, 1';
    PolarEchograms.Signals(end).Storage    = 'single';
    PolarEchograms.Signals(end).Unit       = 'm';
    PolarEchograms.Signals(end).FileName   = fullfile(TypeData,'Tide.bin');
end

if isfield(WC, 'AcrossDistance')
    PolarEchograms.Signals(end+1).Name     = 'AcrossDistance';
    PolarEchograms.Signals(end).Dimensions = '1, nbColumns';
    PolarEchograms.Signals(end).Storage    = 'double';
    PolarEchograms.Signals(end).Unit       = 'm';
    PolarEchograms.Signals(end).FileName   = fullfile(TypeData,'AcrossDistance.bin');
end

kImage = 1;
PolarEchograms.Images(kImage).Name       = 'Reflectivity';
PolarEchograms.Images(kImage).Dimensions = 'nbRows, nbColumns, nbPings';
PolarEchograms.Images(kImage).Storage    = 'single';
PolarEchograms.Images(kImage).Unit       = Unit;
PolarEchograms.Images(kImage).FileName   = fullfile(TypeData,'Reflectivity.bin');

if flagMask
    kImage = kImage + 1;
    PolarEchograms.Images(kImage).Name       = 'Mask';
    PolarEchograms.Images(kImage).Dimensions = 'nbRows, nbColumns, nbPings';
    PolarEchograms.Images(kImage).Storage    = 'uint8';
    PolarEchograms.Images(kImage).Unit       = '';
    PolarEchograms.Images(kImage).FileName   = fullfile(TypeData,'Mask.bin');
end

if isfield(WC, 'latVert')
    kImage = kImage + 1;
    PolarEchograms.Images(kImage).Name       = 'latVert';
    PolarEchograms.Images(kImage).Dimensions = 'nbProfiles, nbPings';
    PolarEchograms.Images(kImage).Storage    = 'double';
    PolarEchograms.Images(kImage).Unit       = 'deg';
    PolarEchograms.Images(kImage).FileName   = fullfile(TypeData,'latVert.bin');

    kImage = kImage + 1;
    PolarEchograms.Images(kImage).Name       = 'lonVert';
    PolarEchograms.Images(kImage).Dimensions = 'nbProfiles, nbPings';
    PolarEchograms.Images(kImage).Storage    = 'double';
    PolarEchograms.Images(kImage).Unit       = 'deg';
    PolarEchograms.Images(kImage).FileName   = fullfile(TypeData,'lonVert.bin');
end

if isfield(WC, 'LatitudeTop')
    kImage = kImage + 1;
    PolarEchograms.Images(kImage).Name       = 'LatitudeTop';
    PolarEchograms.Images(kImage).Dimensions = 'nbPings, nbColumns';
    PolarEchograms.Images(kImage).Storage    = 'double';
    PolarEchograms.Images(kImage).Unit       = 'deg';
    PolarEchograms.Images(kImage).FileName   = fullfile(TypeData,'LatitudeTop.bin');

    kImage = kImage + 1;
    PolarEchograms.Images(kImage).Name       = 'LongitudeTop';
    PolarEchograms.Images(kImage).Dimensions = 'nbPings, nbColumns';
    PolarEchograms.Images(kImage).Storage    = 'double';
    PolarEchograms.Images(kImage).Unit       = 'deg';
    PolarEchograms.Images(kImage).FileName   = fullfile(TypeData,'LongitudeTop.bin');
end

if isfield(WC, 'SoundingsRangeInMeters')
    kImage = kImage + 1;
    PolarEchograms.Images(kImage).Name        = 'SoundingsRangeInMeters';
    PolarEchograms.Images(kImage).Dimensions  = 'nbPings, nbSoundings';
    PolarEchograms.Images(kImage).Storage     = 'double';
    PolarEchograms.Images(kImage).Unit        = 'deg';
    PolarEchograms.Images(kImage).FileName    = fullfile(TypeData, 'SoundingsRangeInMeters.bin');
    
    kImage = kImage + 1;
    PolarEchograms.Images(kImage).Name        = 'SoundingsAcrossDist';
    PolarEchograms.Images(kImage).Dimensions  = 'nbPings, nbSoundings';
    PolarEchograms.Images(kImage).Storage     = 'double';
    PolarEchograms.Images(kImage).Unit        = 'deg';
    PolarEchograms.Images(kImage).FileName    = fullfile(TypeData, 'SoundingsAcrossDist.bin');
    
    kImage = kImage + 1;
    PolarEchograms.Images(kImage).Name        = 'SoundingsDepth';
    PolarEchograms.Images(kImage).Dimensions  = 'nbPings, nbSoundings';
    PolarEchograms.Images(kImage).Storage     = 'double';
    PolarEchograms.Images(kImage).Unit        = 'deg';
    PolarEchograms.Images(kImage).FileName    = fullfile(TypeData, 'SoundingsDepth.bin');
end

%% Cr�ation du fichier XML d�crivant la donn�e 

xml_write(nomFicXml, PolarEchograms);
flag = exist(nomFicXml, 'file');
if ~flag
    messageErreur(nomFicXml)
    return
end

%% Cr�ation du r�pertoire PolarEchograms

nomDirPolarEchograms = fullfile(nomDirRacine, TypeData);
if ~exist(nomDirPolarEchograms, 'dir')
    status = mkdir(nomDirPolarEchograms);
    if ~status
        messageErreur(nomDirPolarEchograms)
        flag = 0;
        return
    end
end

%% Cr�ation des fichiers binaires des signaux

for k=1:length(PolarEchograms.Signals)
    flag = writeSignal(nomDirRacine, PolarEchograms.Signals(k), WC.(PolarEchograms.Signals(k).Name));
    if ~flag
        return
    end
end

%% Cr�ation des fichiers binaires des images

% Attention : le d�part est intentionnellement donn� � 2 car
% Reflectivity.bin a �t� cr�� au fur et � mesure du traitement des pings
if flagMask
    kDeb = 3;
else
    kDeb = 2;
end

N = length(PolarEchograms.Images);
for k=kDeb:N
    flag = writeImage(nomDirRacine, PolarEchograms.Images(k), WC.(PolarEchograms.Images(k).Name));
    if ~flag
        return
    end
end

flag = 1;
