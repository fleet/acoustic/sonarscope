
function flag = WC_writeXmlCubeOLD(nomFicXml, WC, Survey, CLim)

[nomDirRacine, TypeData] = fileparts(nomFicXml);

%% G�n�ration du XML SonarScope

PolarEchograms.Signature1 = 'SonarScope';
PolarEchograms.Signature2 = 'WaterColumnCube3D';

PolarEchograms.Name     = WC.Name;

PolarEchograms.ExtractedFrom  = WC.nomFicAll;
PolarEchograms.nomDirExport = nomDirRacine; % EST-CE TOUJOURS UTILE ????
% if isfield(WC, 'nomFicRAW')
%     PolarEchograms.nomFicRAW   = WC.nomFicRAW;
%     PolarEchograms.RawDataType = WC.RawDataType;
% end
PolarEchograms.FormatVersion = 20100928;

PolarEchograms.CLim = CLim;

PolarEchograms.Survey           = Survey.Name;
PolarEchograms.Vessel           = Survey.Vessel;
PolarEchograms.Sounder          = Survey.Sounder;
PolarEchograms.ChiefScientist   = Survey.ChiefScientist;

PolarEchograms.Dimensions.nbPings = WC.nbPings;
if isfield(WC, 'nbColumns')
    PolarEchograms.Dimensions.nbColumns = WC.nbColumns;
    PolarEchograms.Dimensions.nbRows    = WC.nbRows;
end
if isfield(WC, 'latVert')
    PolarEchograms.Dimensions.nbProfiles = size(WC.latVert, 1);
end

PolarEchograms.Signals(1).Name          = 'Date';
PolarEchograms.Signals(end).Dimensions  = 'nbPings, 1';
PolarEchograms.Signals(end).Storage     = 'double';
PolarEchograms.Signals(end).Unit        = 'Julian Day';
PolarEchograms.Signals(end).FileName    = fullfile(TypeData,'Date.bin');

PolarEchograms.Signals(end+1).Name      = 'Hour';
PolarEchograms.Signals(end).Dimensions  = 'nbPings, 1';
PolarEchograms.Signals(end).Storage     = 'double';
PolarEchograms.Signals(end).Unit        = 'nb millis / midnight';
PolarEchograms.Signals(end).FileName    = fullfile(TypeData,'Hour.bin');

PolarEchograms.Signals(end+1).Name      = 'iPing';
PolarEchograms.Signals(end).Dimensions  = 'nbPings, 1';
PolarEchograms.Signals(end).Storage     = 'single';
PolarEchograms.Signals(end).Unit        = '';
PolarEchograms.Signals(end).FileName    = fullfile(TypeData,'iPing.bin');

PolarEchograms.Signals(end+1).Name      = 'xBab';
PolarEchograms.Signals(end).Dimensions  = 'nbPings, 1';
PolarEchograms.Signals(end).Storage     = 'single';
PolarEchograms.Signals(end).Unit        = 'm';
PolarEchograms.Signals(end).FileName    = fullfile(TypeData,'xBab.bin');

PolarEchograms.Signals(end+1).Name      = 'xTri';
PolarEchograms.Signals(end).Dimensions  = 'nbPings, 1';
PolarEchograms.Signals(end).Storage     = 'single';
PolarEchograms.Signals(end).Unit        = 'm';
PolarEchograms.Signals(end).FileName    = fullfile(TypeData,'xTri.bin');

PolarEchograms.Signals(end+1).Name      = 'Depth';
PolarEchograms.Signals(end).Dimensions  = 'nbPings, 1';
PolarEchograms.Signals(end).Storage     = 'single';
PolarEchograms.Signals(end).Unit        = 'm';
PolarEchograms.Signals(end).FileName    = fullfile(TypeData,'Depth.bin');

PolarEchograms.Signals(end+1).Name     = 'lonBab';
PolarEchograms.Signals(end).Dimensions  = 'nbPings, 1';
PolarEchograms.Signals(end).Storage     = 'double';
PolarEchograms.Signals(end).Unit        = 'deg';
PolarEchograms.Signals(end).FileName    = fullfile(TypeData,'lonBab.bin');

PolarEchograms.Signals(end+1).Name      = 'latBab';
PolarEchograms.Signals(end).Dimensions  = 'nbPings, 1';
PolarEchograms.Signals(end).Storage     = 'double';
PolarEchograms.Signals(end).Unit        = 'deg';
PolarEchograms.Signals(end).FileName    = fullfile(TypeData,'latBab.bin');

PolarEchograms.Signals(end+1).Name      = 'lonCentre';
PolarEchograms.Signals(end).Dimensions  = 'nbPings, 1';
PolarEchograms.Signals(end).Storage     = 'double';
PolarEchograms.Signals(end).Unit        = 'deg';
PolarEchograms.Signals(end).FileName    = fullfile(TypeData,'lonCentre.bin');

PolarEchograms.Signals(end+1).Name      = 'latCentre';
PolarEchograms.Signals(end).Dimensions  = 'nbPings, 1';
PolarEchograms.Signals(end).Storage     = 'double';
PolarEchograms.Signals(end).Unit        = 'deg';
PolarEchograms.Signals(end).FileName    = fullfile(TypeData,'latCentre.bin');

PolarEchograms.Signals(end+1).Name      = 'lonTri';
PolarEchograms.Signals(end).Dimensions  = 'nbPings, 1';
PolarEchograms.Signals(end).Storage     = 'double';
PolarEchograms.Signals(end).Unit        = 'deg';
PolarEchograms.Signals(end).FileName    = fullfile(TypeData,'lonTri.bin');

PolarEchograms.Signals(end+1).Name      = 'latTri';
PolarEchograms.Signals(end).Dimensions  = 'nbPings, 1';
PolarEchograms.Signals(end).Storage     = 'double';
PolarEchograms.Signals(end).Unit        = 'deg';
PolarEchograms.Signals(end).FileName    = fullfile(TypeData,'latTri.bin');

PolarEchograms.Signals(end+1).Name      = 'lonBabDepointe';
PolarEchograms.Signals(end).Dimensions  = 'nbPings, 1';
PolarEchograms.Signals(end).Storage     = 'double';
PolarEchograms.Signals(end).Unit        = 'deg';
PolarEchograms.Signals(end).FileName    = fullfile(TypeData,'lonBabDepointe.bin');

PolarEchograms.Signals(end+1).Name      = 'latBabDepointe';
PolarEchograms.Signals(end).Dimensions  = 'nbPings, 1';
PolarEchograms.Signals(end).Storage     = 'double';
PolarEchograms.Signals(end).Unit        = 'deg';
PolarEchograms.Signals(end).FileName    = fullfile(TypeData,'latBabDepointe.bin');

PolarEchograms.Signals(end+1).Name      = 'lonTriDepointe';
PolarEchograms.Signals(end).Dimensions  = 'nbPings, 1';
PolarEchograms.Signals(end).Storage     = 'double';
PolarEchograms.Signals(end).Unit        = 'deg';
PolarEchograms.Signals(end).FileName    = fullfile(TypeData,'lonTriDepointe.bin');

PolarEchograms.Signals(end+1).Name      = 'latTriDepointe';
PolarEchograms.Signals(end).Dimensions  = 'nbPings, 1';
PolarEchograms.Signals(end).Storage     = 'double';
PolarEchograms.Signals(end).Unit        = 'deg';
PolarEchograms.Signals(end).FileName    = fullfile(TypeData,'latTriDepointe.bin');

if isfield(WC, 'AcrossDistance')
    PolarEchograms.Signals(end+1).Name      = 'AcrossDistance';
    PolarEchograms.Signals(end).Dimensions  = '1, nbColumns';
    PolarEchograms.Signals(end).Storage     = 'double';
    PolarEchograms.Signals(end).Unit        = 'm';
    PolarEchograms.Signals(end).FileName    = fullfile(TypeData,'AcrossDistance.bin');
end

PolarEchograms.Images(1).Name           = 'Reflectivity';
PolarEchograms.Images(end).Dimensions   = 'nbRows, nbColumns, nbPings';
PolarEchograms.Images(end).Storage      = 'single';
PolarEchograms.Images(end).Unit         = 'dB';
PolarEchograms.Images(end).FileName     = fullfile(TypeData,'Reflectivity.bin');

if isfield(WC, 'latVert')
    PolarEchograms.Images(end+1).Name      = 'latVert';
    PolarEchograms.Images(end).Dimensions  = 'nbProfiles, nbPings';
    PolarEchograms.Images(end).Storage     = 'double';
    PolarEchograms.Images(end).Unit        = 'deg';
    PolarEchograms.Images(end).FileName    = fullfile(TypeData,'latVert.bin');

    PolarEchograms.Images(end+1).Name      = 'lonVert';
    PolarEchograms.Images(end).Dimensions  = 'nbProfiles, nbPings';
    PolarEchograms.Images(end).Storage     = 'double';
    PolarEchograms.Images(end).Unit        = 'deg';
    PolarEchograms.Images(end).FileName    = fullfile(TypeData,'lonVert.bin');
end

if isfield(WC, 'LatitudeTop')
    PolarEchograms.Images(end+1).Name      = 'LatitudeTop';
    PolarEchograms.Images(end).Dimensions  = 'nbPings, nbColumns';
    PolarEchograms.Images(end).Storage     = 'double';
    PolarEchograms.Images(end).Unit        = 'deg';
    PolarEchograms.Images(end).FileName    = fullfile(TypeData,'LatitudeTop.bin');

    PolarEchograms.Images(end+1).Name      = 'LongitudeTop';
    PolarEchograms.Images(end).Dimensions  = 'nbPings, nbColumns';
    PolarEchograms.Images(end).Storage     = 'double';
    PolarEchograms.Images(end).Unit        = 'deg';
    PolarEchograms.Images(end).FileName    = fullfile(TypeData,'LongitudeTop.bin');
end

%% Cr�ation du fichier XML d�crivant la donn�e 

xml_write(nomFicXml, PolarEchograms);
flag = exist(nomFicXml, 'file');
if ~flag
    messageErreur(nomFicXml)
    return
end

%% Cr�ation du r�pertoire PolarEchograms

nomDirPolarEchograms = fullfile(nomDirRacine, TypeData);
if ~exist(nomDirPolarEchograms, 'dir')
    status = mkdir(nomDirPolarEchograms);
    if ~status
        messageErreur(nomDirPolarEchograms)
        flag = 0;
        return
    end
end

%% Cr�ation des fichiers binaires des signaux

for i=1:length(PolarEchograms.Signals)
    flag = writeSignal(nomDirRacine, PolarEchograms.Signals(i), WC.(PolarEchograms.Signals(i).Name));
    if ~flag
        return
    end
end

%% Cr�ation des fichiers binaires des images

% Attention : le d�part est intentionnellement donn� � 2 car
% Reflectivity.bin a �t� cr�� au fur et � mesure du traitement des pings
for i=2:length(PolarEchograms.Images)
    flag = writeImage(nomDirRacine, PolarEchograms.Images(i), WC.(PolarEchograms.Images(i).Name));
    if ~flag
        return
    end
end

flag = 1;

