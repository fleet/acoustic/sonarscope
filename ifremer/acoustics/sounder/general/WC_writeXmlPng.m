function flag = WC_writeXmlPng(nomFicXml, WC, Survey, Unit)
% function flag = WC_writeXmlPng(nomFicXml, WC, Survey, codeUnit) % Modif GLU du 24/10/2018

% %% ATTENTION : Correctif en attendant prise en compte dans SSc3DV
% 
% WC.Depth = WC.Depth - WC.Immersion;
% if any(~isnan(WC.Tide))
%     subNonNaN = ~isnan(WC.Tide);
%     WC.Depth(subNonNaN) = WC.Depth(subNonNaN) + WC.Tide(subNonNaN);
% end

[nomDirRacine, TypeData] = fileparts(nomFicXml);

%% G�n�ration du XML SonarScope

PolarEchograms.Software = 'SonarScope';
PolarEchograms.DataTag  = 'WaterColumnImages';
PolarEchograms.Name     = WC.Name;

PolarEchograms.ExtractedFrom = WC.nomFicAll;
PolarEchograms.nomDirExport  = nomDirRacine; % EST-CE TOUJOURS UTILE ????
if isfield(WC, 'nomFicRAW')
    PolarEchograms.nomFicRAW   = WC.nomFicRAW;
    PolarEchograms.RawDataType = WC.RawDataType;
end
PolarEchograms.FormatVersion = 20100201;

if isempty(Survey)
    PolarEchograms.Survey         = 'Unkown';
    PolarEchograms.Vessel         = 'Unkown';
    PolarEchograms.Sounder        = 'Unkown';
    PolarEchograms.ChiefScientist = 'Unkown';
else
    PolarEchograms.Survey         = Survey.Name;
    PolarEchograms.Vessel         = Survey.Vessel;
    PolarEchograms.Sounder        = Survey.Sounder;
    PolarEchograms.ChiefScientist = Survey.ChiefScientist;
end

%{
% Modif GLU du 24/10/2018 (mail du 31/10/2018 "Sapajou")
% Doc SEGY Rev 2, pg 22
listUnit    = [{-1, 'Other'}; ...
            {0, 'Unknown'}; ...
            {1, 'Pa'}; ...  % Pascal
            {2, 'v'}; ...   % Volts
            {3, 'mV'}; ...  % Millivolts (mV)
            {4, 'A'}; ...   % Amperes
            {5, 'm'}];      % meters
idx         = ([listUnit{:,2}] == codeUnit);
labelUnit   = listUnit{idx,2};
PolarEchograms.ImageUnit = labelUnit;
%}
% PolarEchograms.ImageUnit = Unit;
PolarEchograms.ChiefScientist = Unit; % TODO : changer cela lorsque 3DV sera modifi�





% TODO : comment� pour le moment car Globe ne dig�re pas la ligne CLim
%{
if ~isempty(CLim)
% TODO : comment� par JMA le 05/03/2018 car Globe ne dig�re pas le CLim
%     PolarEchograms.CLim = CLim;
end
%}

PolarEchograms.Dimensions.nbPings = WC.nbPings;
if isfield(WC, 'nbColumns')
    PolarEchograms.Dimensions.nbColumns = WC.nbColumns;
    PolarEchograms.Dimensions.nbRows    = WC.nbRows;
end
if isfield(WC, 'latVert')
    PolarEchograms.Dimensions.nbProfiles = size(WC.latVert, 1);
end

%{
% TODO : en attente correction GLOBE ?
if isfield(WC, 'SoundingsRangeInMeters')
    PolarEchograms.Dimensions.nbSoundings = size(WC.SoundingsRangeInMeters, 2);
end
%}

PolarEchograms.Signals(1).Name          = 'Date';
PolarEchograms.Signals(end).Dimensions  = 'nbPings, 1';
PolarEchograms.Signals(end).Storage     = 'double';
PolarEchograms.Signals(end).Unit        = 'Julian Day';
PolarEchograms.Signals(end).FileName    = fullfile(TypeData, 'Date.bin');

PolarEchograms.Signals(end+1).Name      = 'Hour';
PolarEchograms.Signals(end).Dimensions  = 'nbPings, 1';
PolarEchograms.Signals(end).Storage     = 'double';
PolarEchograms.Signals(end).Unit        = 'nb millis / midnight';
PolarEchograms.Signals(end).FileName    = fullfile(TypeData, 'Hour.bin');

PolarEchograms.Signals(end+1).Name      = 'iPing';
PolarEchograms.Signals(end).Dimensions  = 'nbPings, 1';
PolarEchograms.Signals(end).Storage     = 'single';
PolarEchograms.Signals(end).Unit        = '';
PolarEchograms.Signals(end).FileName    = fullfile(TypeData, 'iPing.bin');

PolarEchograms.Signals(end+1).Name      = 'xBab';
PolarEchograms.Signals(end).Dimensions  = 'nbPings, 1';
PolarEchograms.Signals(end).Storage     = 'single';
PolarEchograms.Signals(end).Unit        = 'm';
PolarEchograms.Signals(end).FileName    = fullfile(TypeData, 'xBab.bin');

PolarEchograms.Signals(end+1).Name      = 'xTri';
PolarEchograms.Signals(end).Dimensions  = 'nbPings, 1';
PolarEchograms.Signals(end).Storage     = 'single';
PolarEchograms.Signals(end).Unit        = 'm';
PolarEchograms.Signals(end).FileName    = fullfile(TypeData, 'xTri.bin');

PolarEchograms.Signals(end+1).Name      = 'Depth';
PolarEchograms.Signals(end).Dimensions  = 'nbPings, 1';
PolarEchograms.Signals(end).Storage     = 'single';
PolarEchograms.Signals(end).Unit        = 'm';
PolarEchograms.Signals(end).FileName    = fullfile(TypeData, 'Depth.bin');

PolarEchograms.Signals(end+1).Name      = 'Immersion';
PolarEchograms.Signals(end).Dimensions  = 'nbPings, 1';
PolarEchograms.Signals(end).Storage     = 'single';
PolarEchograms.Signals(end).Unit        = 'm';
PolarEchograms.Signals(end).FileName    = fullfile(TypeData,'Immersion.bin');

PolarEchograms.Signals(end+1).Name     = 'lonBab';
PolarEchograms.Signals(end).Dimensions  = 'nbPings, 1';
PolarEchograms.Signals(end).Storage     = 'double';
PolarEchograms.Signals(end).Unit        = 'deg';
PolarEchograms.Signals(end).FileName    = fullfile(TypeData, 'lonBab.bin');

PolarEchograms.Signals(end+1).Name      = 'latBab';
PolarEchograms.Signals(end).Dimensions  = 'nbPings, 1';
PolarEchograms.Signals(end).Storage     = 'double';
PolarEchograms.Signals(end).Unit        = 'deg';
PolarEchograms.Signals(end).FileName    = fullfile(TypeData, 'latBab.bin');

PolarEchograms.Signals(end+1).Name      = 'lonCentre';
PolarEchograms.Signals(end).Dimensions  = 'nbPings, 1';
PolarEchograms.Signals(end).Storage     = 'double';
PolarEchograms.Signals(end).Unit        = 'deg';
PolarEchograms.Signals(end).FileName    = fullfile(TypeData, 'lonCentre.bin');

PolarEchograms.Signals(end+1).Name      = 'latCentre';
PolarEchograms.Signals(end).Dimensions  = 'nbPings, 1';
PolarEchograms.Signals(end).Storage     = 'double';
PolarEchograms.Signals(end).Unit        = 'deg';
PolarEchograms.Signals(end).FileName    = fullfile(TypeData, 'latCentre.bin');

PolarEchograms.Signals(end+1).Name      = 'lonTri';
PolarEchograms.Signals(end).Dimensions  = 'nbPings, 1';
PolarEchograms.Signals(end).Storage     = 'double';
PolarEchograms.Signals(end).Unit        = 'deg';
PolarEchograms.Signals(end).FileName    = fullfile(TypeData, 'lonTri.bin');

PolarEchograms.Signals(end+1).Name      = 'latTri';
PolarEchograms.Signals(end).Dimensions  = 'nbPings, 1';
PolarEchograms.Signals(end).Storage     = 'double';
PolarEchograms.Signals(end).Unit        = 'deg';
PolarEchograms.Signals(end).FileName    = fullfile(TypeData, 'latTri.bin');

PolarEchograms.Signals(end+1).Name      = 'lonBabDepointe';
PolarEchograms.Signals(end).Dimensions  = 'nbPings, 1';
PolarEchograms.Signals(end).Storage     = 'double';
PolarEchograms.Signals(end).Unit        = 'deg';
PolarEchograms.Signals(end).FileName    = fullfile(TypeData, 'lonBabDepointe.bin');

PolarEchograms.Signals(end+1).Name      = 'latBabDepointe';
PolarEchograms.Signals(end).Dimensions  = 'nbPings, 1';
PolarEchograms.Signals(end).Storage     = 'double';
PolarEchograms.Signals(end).Unit        = 'deg';
PolarEchograms.Signals(end).FileName    = fullfile(TypeData, 'latBabDepointe.bin');

PolarEchograms.Signals(end+1).Name      = 'lonTriDepointe';
PolarEchograms.Signals(end).Dimensions  = 'nbPings, 1';
PolarEchograms.Signals(end).Storage     = 'double';
PolarEchograms.Signals(end).Unit        = 'deg';
PolarEchograms.Signals(end).FileName    = fullfile(TypeData, 'lonTriDepointe.bin');

PolarEchograms.Signals(end+1).Name      = 'latTriDepointe';
PolarEchograms.Signals(end).Dimensions  = 'nbPings, 1';
PolarEchograms.Signals(end).Storage     = 'double';
PolarEchograms.Signals(end).Unit        = 'deg';
PolarEchograms.Signals(end).FileName    = fullfile(TypeData, 'latTriDepointe.bin');

if isfield(WC, 'Tide')
    PolarEchograms.Signals(end+1).Name      = 'Tide';
    PolarEchograms.Signals(end).Dimensions  = 'nbPings, 1';
    PolarEchograms.Signals(end).Storage     = 'single';
    PolarEchograms.Signals(end).Unit        = 'm';
    PolarEchograms.Signals(end).FileName    = fullfile(TypeData, 'Tide.bin');
end

if isfield(WC, 'MinRangeInMeters')
    PolarEchograms.Signals(end+1).Name      = 'MinRangeInMeters';
    PolarEchograms.Signals(end).Dimensions  = 'nbPings, 1';
    PolarEchograms.Signals(end).Storage     = 'single';
    PolarEchograms.Signals(end).Unit        = 'm';
    PolarEchograms.Signals(end).FileName    = fullfile(TypeData, 'MinRangeInMeters.bin');
end

kImage = 0;
if isfield(WC, 'latVert')
    kImage = kImage + 1;
    PolarEchograms.Images(kImage).Name        = 'latVert';
    PolarEchograms.Images(kImage).Dimensions  = 'nbProfiles, nbPings';
    PolarEchograms.Images(kImage).Storage     = 'double';
    PolarEchograms.Images(kImage).Unit        = 'deg';
    PolarEchograms.Images(kImage).FileName    = fullfile(TypeData, 'latVert.bin');

    kImage = kImage + 1;
    PolarEchograms.Images(kImage).Name        = 'lonVert';
    PolarEchograms.Images(kImage).Dimensions  = 'nbProfiles, nbPings';
    PolarEchograms.Images(kImage).Storage     = 'double';
    PolarEchograms.Images(kImage).Unit        = 'deg';
    PolarEchograms.Images(kImage).FileName    = fullfile(TypeData, 'lonVert.bin');
end

%{
% TODO : en attente correction GLOBE
if isfield(WC, 'SoundingsRangeInMeters')
    kImage = kImage + 1;
    PolarEchograms.Images(kImage).Name        = 'SoundingsRangeInMeters';
    PolarEchograms.Images(kImage).Dimensions  = 'nbPings, nbSoundings';
    PolarEchograms.Images(kImage).Storage     = 'double';
    PolarEchograms.Images(kImage).Unit        = 'deg';
    PolarEchograms.Images(kImage).FileName    = fullfile(TypeData, 'SoundingsRangeInMeters.bin');
    
    kImage = kImage + 1;
    PolarEchograms.Images(kImage).Name        = 'SoundingsAcrossDist';
    PolarEchograms.Images(kImage).Dimensions  = 'nbPings, nbSoundings';
    PolarEchograms.Images(kImage).Storage     = 'double';
    PolarEchograms.Images(kImage).Unit        = 'deg';
    PolarEchograms.Images(kImage).FileName    = fullfile(TypeData, 'SoundingsAcrossDist.bin');
    
    kImage = kImage + 1;
    PolarEchograms.Images(kImage).Name        = 'SoundingsDepth';
    PolarEchograms.Images(kImage).Dimensions  = 'nbPings, nbSoundings';
    PolarEchograms.Images(kImage).Storage     = 'double';
    PolarEchograms.Images(kImage).Unit        = 'deg';
    PolarEchograms.Images(kImage).FileName    = fullfile(TypeData, 'SoundingsDepth.bin');
end
%}

%% Cr�ation du fichier XML d�crivant la donn�e 

xml_write(nomFicXml, PolarEchograms);
flag = exist(nomFicXml, 'file');
if ~flag
    messageErreur(nomFicXml)
    return
end

%% Cr�ation du r�pertoire PolarEchograms

nomDirPolarEchograms = fullfile(nomDirRacine, TypeData);
if ~exist(nomDirPolarEchograms, 'dir')
    status = mkdir(nomDirPolarEchograms);
    if ~status
        messageErreur(nomDirPolarEchograms)
        flag = 0;
        return
    end
end

%% Cr�ation des fichiers binaires des signaux

for k=1:length(PolarEchograms.Signals)
%     figure; plot(WC.(PolarEchograms.Signals(k).Name)); grid on; title(PolarEchograms.Signals(k).Name)
    
    % Rajout� le 16/01/2012 pour donn�es Reson
    subNaN = find(isnan(WC.(PolarEchograms.Signals(k).Name)));
    if ~isempty(subNaN)
        subNonNaN = find(~isnan(WC.(PolarEchograms.Signals(k).Name)));
        if length(subNonNaN) > 2
            WC.(PolarEchograms.Signals(k).Name)(subNaN) = interp1(subNonNaN, WC.(PolarEchograms.Signals(k).Name)(subNonNaN), subNaN, 'linear', 'extrap');
        end
    end
    
    flag = writeSignal(nomDirRacine, PolarEchograms.Signals(k), WC.(PolarEchograms.Signals(k).Name));
    if ~flag
        return
    end
end

%% Cr�ation des fichiers binaires des images

if kImage ~= 0
    for k=1:length(PolarEchograms.Images)
        flag = writeImage(nomDirRacine, PolarEchograms.Images(k), WC.(PolarEchograms.Images(k).Name));
        if ~flag
            return
        end
    end
end

flag = 1;
