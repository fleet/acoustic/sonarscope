function flag = WC_write_PolarEchogramsXML(StructXML, nbPings, nomFicAll, TypeWCOutput, ...
    NamesXML, across, depth, Survey, CLimEchograms, Unit)

% nbPings = length(StructWcXmlRaw.Depth)
nbAcross = length(across);

StructXML = WCnbPings(StructXML, nbPings);
StructXML.nomFicAll = nomFicAll;

if TypeWCOutput == 2 % Matrices 3D
    filename = NamesXML;
    [~, nomFicRAWSeul] = fileparts(filename); %nomFicBinRAW nomFicBinComp
    StructXML.nomFicRAW      = nomFicRAWSeul;
    StructXML.nbColumns      = nbAcross;
    StructXML.nbRows         = length(depth);
    StructXML.RawDataType    = 'single';
    StructXML.AcrossDistance = across;
end
StructXML.nbPings = nbPings;

[~, nomFic] = fileparts(nomFicAll);
StructXML.Name = nomFic;

if contains(NamesXML, '_Raw_PolarEchograms')
    StructXML.Name = ['Raw-' StructXML.Name];
elseif contains(NamesXML, '_Comp_PolarEchograms')
    StructXML.Name = ['Comp-' StructXML.Name];
elseif contains(NamesXML, '_Phase_PolarEchograms')
    StructXML.Name = ['Phase-' StructXML.Name];
end

if contains(NamesXML, '_WCCubeRaw') || contains(NamesXML, '_Raw_PolarEchograms')
    Layer = 'Raw';
elseif contains(NamesXML, '_WCCubeComp') || contains(NamesXML, '_Comp_PolarEchograms')
    Layer = 'Comp';
elseif contains(NamesXML, '???')
    my_breakpoint
    Layer = 'Phase';
end

if TypeWCOutput == 1 % Images individuelles
    flag = WC_writeXmlPng(NamesXML, StructXML, Survey, Unit); % Checked
else % Matrices 3D
  flag = WC_writeXmlCube(NamesXML, StructXML, Survey, CLimEchograms, Unit, 'Layer', Layer, 'flagMask', 1);
%     flag = WC_writeXmlCube(NamesXML, StructXML, Survey, CLimEchograms, Unit, 'Layer', Layer);
end


function WC = WCnbPings(WC, nbPings)
sub = 1:nbPings;
WC.lonBab          = WC.lonBab(sub);
WC.latBab          = WC.latBab(sub);
WC.lonCentre       = WC.lonCentre(sub);
WC.latCentre       = WC.latCentre(sub);
WC.lonTri          = WC.lonTri(sub);
WC.latTri          = WC.latTri(sub);
WC.iPing           = WC.iPing(sub);
WC.xBab            = WC.xBab(sub);
WC.xTri            = WC.xTri(sub);
WC.Depth           = WC.Depth(sub);
WC.Immersion       = WC.Immersion(sub);
WC.Tide            = WC.Tide(sub);
WC.Heave           = WC.Heave(sub);
WC.Date            = WC.Date(sub);
WC.Hour            = WC.Hour(sub);
WC.lonBabDepointe  = WC.lonBabDepointe(sub);
WC.latBabDepointe  = WC.latBabDepointe(sub);
WC.lonTriDepointe  = WC.lonTriDepointe(sub);
WC.latTriDepointe  = WC.latTriDepointe(sub);
if isfield(WC, 'LatitudeTop')
    WC.LatitudeTop  = WC.LatitudeTop(sub,:);
    WC.LongitudeTop = WC.LongitudeTop(sub,:);
end
if isfield(WC, 'MinRangeInMeters')
    WC.MinRangeInMeters = WC.MinRangeInMeters(sub);
end
if isfield(WC, 'SoundingsRangeInMeters')
    WC.SoundingsRangeInMeters = WC.SoundingsRangeInMeters(sub,:);
    WC.SoundingsAcrossDist    = WC.SoundingsAcrossDist(sub,:);
    WC.SoundingsDepth         = WC.SoundingsDepth(sub,:);
end
