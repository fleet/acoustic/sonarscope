% Ajustement des noeuds permettant un approximation par fonction spline d'une courbe exp�riementale
%
% Syntax
%   yNodes = ajustement_spline(XData, YData, xNodes, ...)
%
% Input Arguments
%   XData  : Abscisses des donn�es exp�riemntales
%   YData  : Ordon��s des donn�es exp�riemntales
%   xNodes : Abscisses des noeuds
%
% Name-Value Pair Arguments
%   yNodes : Ordonn�es des noeuds (sert � l'initialisation)
%
% Output Arguments
%   yNodes : Ordonn�es des noeuds
%
% Examples
%   FileNameSsc = getNomFicDatabase('BScorr_710.txt');
%   [flag, BSCorr] = read_BSCorr_Spline(FileNameSsc);
%   k1 = 1;
%   for k2=1:length(BSCorr(k1).Sector)
%       xNodes = BSCorr(k1).Sector(k2).Node(:,1)';
%       yNodes = BSCorr(k1).Sector(k2).Node(:,2)';
%       XData = min(xNodes):0.5:max(xNodes);
%       YData = spline(xNodes, yNodes, XData);
%       yNodesNew = ajustement_spline(XData, YData, xNodes, 'yNodes', yNodes)
%   end
%
% See also AntenneSpline Ex_optimSpline Authors
% Authors  : JMA
%-------------------------------------------------------------------------------

function yNodes = ajustement_spline(XData, YData, xNodes, varargin)

[varargin, yNodes] = getPropertyValue(varargin, 'yNodes', []); %#ok<ASGLU>

XData = XData(:)';
YData = YData(:)';

%% Cr�ation de mod�les

for k=1:3
    modelName = sprintf('AntenneSpline - %d', k);
    model_Spline(k) = AntenneSplineModel('Name', modelName, 'XData', XData, 'YData', YData, 'xNodes', xNodes, 'yNodes', yNodes); %#ok<AGROW>
    xNodes = [xNodes; xNodes + ((xNodes(2)-xNodes(1)) / 2)]; %#ok<AGROW>
    xNodes = xNodes(:)';
    yNodes = [yNodes; yNodes]; %#ok<AGROW>
    yNodes = yNodes(:)';
end

%% D�claration d'un instance d'ajustement

optim = ClOptim('XData', XData, 'YData', YData, ...
    'xLabel', 'Angles', ...
    'yLabel', 'TxBeamDiag', ...
    'Name', 'Tx Diagram');
optim.set('models', model_Spline);

%% Test IHM

a = OptimUiDialog(optim, 'Title', 'Optim ExClOptimBS', 'windowStyle', 'normal');
a.openDialog;
% TODO : test si OK ?
a.optim.plot

%% R�cup�ration du resultat

yNodes = a.optim.getParamsValue;
% xNodes = get_valParams(optim);

