% Estimation du premier echo sur un signal sonar
%
% Syntax
%   H = calPremierEcho(S1, FiltreSonar)
%   calPremierEcho(S1, FiltreSonar)
%
% Input Arguments 
%   IRaw : Image brute sonar
%
% Name-Value Pair Arguments 
%   FiltreSonar : Parametres du filtre sur le signal sonar 
%                 [Ordre du filtre , frequence de coupure normaliee]
%                 [1 0.1] par defaut,  [] pour ne pas filtrer
%
% Output Arguments
%   [] : Auto-plot activation
%   H  : Indice de la hauteur
%
% Remarks : Le filtre utilise est un butterworth a phase nulle (help filtfilt).
%           L'ordre du filtre est donc le double de ce que vous transmettez.
% 
% Examples
%   S = atan(-100:100); S = S + 5 * rand(size(S));
%   calPremierEcho(S, [2, 0.2])
%
%   H = calPremierEcho(S, [2, 0.1])
%
% See also calHeight corObliquite cl_car_sonar Authors
% Authors : JMA
% VERSION  : $Id: calPremierEcho.m,v 1.2 2002/02/13 14:44:54 augustin Exp $
%-------------------------------------------------------------------------------
  
% ----------------------------------------------------------------------------
% HISTORIQUE DEVELOPPEMENT
%   13/02/02 - JMA - Creation.
% ----------------------------------------------------------------------------

function H = calPremierEcho(S1, FiltreSonar)

if nargin == 0
    help calPremierEcho
    return
end

subNan = find(isnan(S1));
if ~isempty(subNan)
    subNonNan = find(~isnan(S1));
    S1(subNan) = interp1(subNonNan, S1(subNonNan), subNan);
end

if ~isempty(FiltreSonar)
    [B,A] = butter(FiltreSonar(1), FiltreSonar(2));
    S2 = filtfilt(B, A, double(S1));
else
    S2 = S1;
end

N = length(S2);
lstep = max(1, floor(N / 10));
S2 = S2(1:lstep*10);
ideb = 1;
ifin = length(S2);
while lstep ~= 0
    for i=1:(ifin-ideb)/lstep
        sub = ideb + (0:lstep-1) + (i-1) * lstep;
        moy(i) = mean(S2(sub));
    end
    
    [q, imax] = max(diff(moy));
    H = ideb + imax - 1;  

    ideb = ideb + (imax - 1) * lstep;
    ifin = ideb + (imax + 1) * lstep;
    
    ideb = max(1, ideb);
    ifin = min(ifin, length(S2));
    
    lstep = max(0, floor(lstep / 2));
end

if nargout == 0
    plot(S1, 'k'); grid on; hold on; plot(S2, 'b');
    plot(H, S1(H), 'ro'); plot([H H], get(gca, 'YLim'), 'r'); hold off;
end