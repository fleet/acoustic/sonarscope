function [XLat, XLon] = calculLatLonFromAlongAcross(E2, A, FishLatitude, FishLongitude, Heading, AlongD, AcrossD)

Heading  = double(Heading);
AlongD   = double(AlongD);
AcrossD  = double(AcrossD);

% Calcul direct en LatLong
% Document Cartolib++ / Manuel de r�f�rence avril 2004 Paragraphe 7.1.2

sinLN2 = sind(FishLatitude) .^ 2;
E2MoinssinLN2 = 1 - E2 * sinLN2;
NORM = A ./ sqrt(E2MoinssinLN2);
RAY0 = NORM * (1 - E2) ./ E2MoinssinLN2;
SC = sind(Heading);
CC = cosd(Heading);
DL = double(AlongD);
DT = double(AcrossD);
XLat = FishLatitude  + (DL * CC - DT * SC) .* ((180/pi) ./ RAY0);
XLon = FishLongitude + (DL * SC + DT * CC) .* ((180/pi) ./ NORM ./ cosd(FishLatitude));
