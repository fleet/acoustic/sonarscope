% Calcul de la hauteur du sonar sur une image brute sonar.
%
% Syntax
%   calculer_hauteur(IRaw)
%   [HBab, HTri] = calculer_hauteur(IRaw, ...)
%   HBab = calculer_hauteur(IRaw, 'Babord', ...)
%   HTri = calculer_hauteur(IRaw, 'Tribord', ...)
%
% Input Arguments
%   IRaw : Image brute sonar
%
% Name-Value Pair Arguments
%   HMin           : Indice min de la hauteur
%   HMax           : Indice max de la hauteur
%   FiltreTemporel : Parametres du filtre sur la hauteur detectee
%                    [Ordre du filtre , frequence de coupure normaliee]
%                    [1 0.2] par defaut,  [] pour ne pas filtrer
%   FiltreSonar    : Parametres du filtre sur le signal sonar
%                    [Ordre du filtre , frequence de coupure normaliee]
%                    [1 0.1] par defaut,  [] pour ne pas filtrer
%
% Name-only Arguments
%   Babord  : Detection a babord uniquement
%   Tribord : Detection a tribord uniquement
%
% Output Arguments
%   []   : Auto-plot activation
%   HBab : Indice de la hauteur detectee a babord
%   HTri : Indice de la hauteur detectee a tribord
%
% Remarks : Les filtres utilises sont des butterworth a phase nulle (help filtfilt).
%           L'ordre du filtre est donc le double de ce que vous transmettez.
%
% Examples
%   addpath /home1/doppler/augustin/MatlabToolboxIfremer/sonars
%   nomFic = getNomFicDatabase('DF1000_ExStr.imo')
%   a = cl_car_sonar('nomFic', nomFic);
%   IRaw = get(a, 'grRawImage', 'subl', 1300:1370);
%
%   [HBab, HTri] = calculer_hauteur(IRaw);
%
%   [M, N] = size(IRaw);
%   subplot(1,3,1); imagesc(IRaw); colormap(gray(256));
%   hold on; plot(N/2 - HBab, 1:M, 'r'); plot(N/2 + HTri + 1, 1:M, 'g')
%   ICor = corriger_obliquite(IRaw, HBab);
%   subplot(1,3,2); imagesc(ICor);
%   ICor = corriger_obliquite(IRaw, HTri);
%   subplot(1,3,3); imagesc(ICor);
%
%   calculer_hauteur(IRaw, 'HMin', 155, 'HMax', 175);
%   calculer_hauteur(IRaw, 'FiltreSonar', [], 'FiltreTemporel', [])
%
%   close(a);
%
% See also calculer_hauteur_signal corriger_obliquite cl_car_sonar Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function varargout = calculer_hauteur(IRaw, varargin)

[M, N] = size(IRaw);

[varargin, X]              = getPropertyValue(varargin, 'X',             (1:N) - N/2 - 0.5);
[varargin, HMin]           = getPropertyValue(varargin, 'HMin',           0.5);
[varargin, HMax]           = getPropertyValue(varargin, 'HMax',           Inf);
[varargin, FiltreTemporel] = getPropertyValue(varargin, 'FiltreTemporel', [1 0.2]);
[varargin, FiltreSonar]    = getPropertyValue(varargin, 'FiltreSonar',    [1 0.1]);

[varargin, FlagBabord]  = getFlag(varargin, 'Babord');
[varargin, FlagTribord] = getFlag(varargin, 'Tribord'); %#ok<ASGLU>

if ~FlagBabord && ~FlagTribord
    FlagBabord  = 1;
    FlagTribord = 1;
end

subBab = fliplr(find(X <= -HMin & X >= -HMax));
if isempty(subBab)
    FlagBabord = 0;
end

subTri = find(X >= HMin & X <= HMax);
if isempty(subTri)
    FlagTribord = 0;
end

HTri = NaN(1,M);
HBab = NaN(1,M);
hw = create_waitbar( 'Calcul de la hauteur', 'M', M);
for i=1:M
    my_waitbar(i, M, hw);
    if FlagBabord
        SBab = IRaw(i, subBab);
        HBab(i) = calculer_hauteur_signal(SBab, FiltreSonar) - X(subBab(1)) + 0.5;
    end
    if FlagTribord
        STri = IRaw(i, subTri);
        HTri(i) = calculer_hauteur_signal(STri, FiltreSonar) + X(subTri(1)) + 0.5;
    end
end
my_close(hw);

if nargout ~= 0
    varargout = {};
end

if ~isempty(FiltreTemporel)
    [B, A] = butter(FiltreTemporel(1), FiltreTemporel(2));
end

if FlagBabord
    if ~isempty(FiltreTemporel)
        HBab = filtfilt(B, A, double(HBab));
        HBab = floor(HBab);
    end
    if nargout ~= 0
        varargout{end+1} = HBab;
    end
end

if FlagTribord
    if ~isempty(FiltreTemporel)
        HTri = filtfilt(B, A, double(HTri));
        HTri = floor(HTri);
    end
    if nargout ~= 0
        varargout{end+1} = HTri;
    end
end

if ~all([FlagBabord FlagTribord])
    varargout{2} = varargout{1};
end

if nargout == 0
    Arrays{1} = IRaw;
    Titres{1} = 'Detection de hauteur sur image sonar';
    
    if FlagBabord
        ICorBab = corriger_obliquite(IRaw, HBab);
        Arrays{end+1} = ICorBab;
        Titres{end+1}  = 'Image corrigee a partir de la hauteur babord';
    end
    
    if FlagTribord
        ICorTri = corriger_obliquite(IRaw, HTri);
        Arrays{end+1} = ICorTri;
        Titres{end+1}  = 'Image corrigee a partir de la hauteur tribord';
    end
    
    obj_visu = cli_visu_caraibes;
    obj_visu = set_nb_lig(obj_visu, M) ;
    obj_visu = set_nb_col(obj_visu, N) ;
    obj_visu = set_lst_mat(obj_visu, Arrays) ;
    %     obj_visu = set_lst_ordonnee(obj_visu, X) ;   % TRUC QUI SERAIT BIEN (DONNER LES ORDONNEES PAR NOUS MEME)
    obj_visu = set_lst_noms_mat(obj_visu, Titres) ;
    obj_visu = editobj(obj_visu) ; %#ok<NASGU>
    
    % En attendant de pouvoir faire cela directement dans cli_visu_caraibes, on
    % le fait a la mimine
    GCF = gcf;
    haxes= findobj(GCF,'Type', 'axes');
    for i=1:length(haxes)
        hc = get(haxes(i), 'Title');
        titre = get(hc, 'String');
        if strcmp(titre, 'Detection de hauteur sur image sonar')
            set(GCF, 'CurrentAxes', haxes(i))
            hold on;
            %             Y = 1:M;
            if FlagBabord
                %                 plot(-HBab, 'r'); % Si  set_lst_ordonnee(obj_visu, X) realise
                plot(1023.5-HBab, 'r');             % A defaut
            end
            if FlagTribord
                %                 plot(HTri, 'g');
                plot(1023.5+HTri, 'g');
            end
            hold off;
        end
    end
end
