% Estimation du premier echo sur un signal sonar
%
% Syntax
%   H = calculer_hauteur_signal(S1, FiltreSonar)
%   calculer_hauteur_signal(S1, FiltreSonar)
%
% Input Arguments 
%   IRaw : Image brute sonar
%
% Name-Value Pair Arguments 
%   FiltreSonar : Parametres du filtre sur le signal sonar 
%                 [Ordre du filtre , frequence de coupure normaliee]
%                 [1 0.1] par defaut,  [] pour ne pas filtrer
%
% Output Arguments
%   [] : Auto-plot activation
%   H  : Indice de la hauteur
%
% Remarks : Le filtre utilise est un butterworth a phase nulle (help filtfilt).
%           L'ordre du filtre est donc le double de ce que vous transmettez.
% 
% Examples
%   S = atan(-100:100); S = S + 5 * rand(size(S));
%   calculer_hauteur_signal(S, [2, 0.2])
%
%   H = calculer_hauteur_signal(S, [2, 0.1])
%
% See also calculer_hauteur cl_car_sonar Authors
% Authors : JMA
% VERSION  : $Id: calculer_hauteur_signal.m,v 1.1 2002/03/01 16:27:35 augustin Exp $
%-------------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HISTORIQUE DEVELOPPEMENT
%   13/02/02 - JMA - Creation.
% ----------------------------------------------------------------------------

function H = calculer_hauteur_signal(S1, FiltreSonar)

% Maintenance Auto : try
    if nargin == 0
        help calculer_hauteur_signal
        return
    end
    
    subNan = find(isnan(S1));
    if ~isempty(subNan)
        subNonNan = find(~isnan(S1));
        S1(subNan) = interp1(subNonNan, S1(subNonNan), subNan);
    end
    
    if ~isempty(FiltreSonar)
        [B,A] = butter(FiltreSonar(1), FiltreSonar(2));
        S2 = filtfilt(B, A, double(S1));
    else
        S2 = S1;
    end
    
    N = length(S2);
    lstep = [1000 500 200 100 50 20 10 5 4 2 1];
    n = floor(N ./ lstep);
    sn = find(n >= 5);
    if isempty(sn)
        sn = 1;
    end
    lstep = lstep(sn(1));
    n = n(sn(1));
    S2 = S2(1:min(lstep*n,N));
    ideb = 1;
    ifin = length(S2);
    while lstep ~= 0
        moy = [];
        for i=1:(ifin-ideb)/lstep
            sub = ideb + (0:lstep-1) + (i-1) * lstep;
            moy(i) = mean(S2(sub));
        end
        
        [q, imax] = max(diff(moy));
        H = ideb + imax - 1;  
        
        ideb = ideb + (imax - 1) * lstep;
        ifin = ideb + (imax + 1) * lstep;
        
        ideb = max(1, ideb);
        ifin = min(ifin, length(S2));
        
        lstep = max(0, floor(lstep / 2));
    end
    
    
    % ---------
    % Auto plot
    
    if nargout == 0
        plot(S1, 'k'); grid on; hold on; plot(S2, 'b');
        plot(H, S1(H), 'ro'); plot([H H], get(gca, 'YLim'), 'r'); hold off;
    end
    
% Maintenance Auto : catch
% Maintenance Auto :     err ('general', 'calculer_hauteur_signal', '') ;
% Maintenance Auto : end
