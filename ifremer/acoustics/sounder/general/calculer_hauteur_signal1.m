% Estimation du premier echo sur un signal sonar
%
% Syntax
%   H = calculer_hauteur_signal1(S1, A, B)
%
% Input Arguments 
%   S1 : Ping sonar
%   A : Parametres du filtre sur le signal sonar 
%                 [] pour ne pas filtrer cf. butter
%   B : Parametres du filtre sur le signal sonar 
%                 [] pour ne pas filtrer
%
% Output Arguments
%   [] : Auto-plot activation
%   H  : Indice de la hauteur
%
% Remarks : Le filtre utilise est un butterworth a phase nulle (help filtfilt).
%           L'ordre du filtre est donc le double de ce que vous transmettez.
% 
% Examples
%   S = atan(-100:100);
%   S = S + 5 * rand(size(S));
%
%   calculer_hauteur_signal1(S, [], [], 0)
%
%   Order = 1;
%   Wc = 0.2;
%   [B,A] = butter(Order, Wc);
%   calculer_hauteur_signal1(S, A, B, 0)
%
%   H = calculer_hauteur_signal1(S, A, B, 0)
%
% See also calculer_hauteur cl_car_sonar Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function H = calculer_hauteur_signal1(S1, A, B, Penetration)

% Comment� par JMA le 27/02/2019 car la gestion des NaN se fait dans la fonction appelante sonar_lateral_hauteur

% subNan = find(isnan(S1));
% if ~isempty(subNan)
%     subNonNan = find(~isnan(S1));
%     if isempty(subNonNan)
%         H = NaN;
%         return
%     end
%     S1(subNan) = interp1(subNonNan, S1(subNonNan), subNan);
% end

if ~isempty(A) && ~isempty(B) && (length(S1) > 2*length(A))
    S2 = filtfilt(B, A, double(S1));
else
    S2 = S1;
end

N = length(S2);
lstep = [1000 500 200 100 50 20 10 5 4 2 1];
n = floor(N ./ lstep);
% sn = find(n >= 5);
sn = find(n >= 10);
if isempty(sn)
    sn = length(lstep);
end
lstep = lstep(sn(1));
n = n(sn(1));
S2 = S2(1:min(lstep*n,N));
ideb = 1;
ifin = length(S2);
kLoop = 0;
while lstep ~= 0
    kLoop = kLoop + 1;
    
    N = floor((1+ifin-ideb)/lstep);
    moy = zeros(1,N);
    for k=1:N
        sub = ideb + (0:lstep-1) + (k-1) * lstep;
        moy(k) = mean(S2(sub));
    end

    [~, imax] = max(diff(moy));
    
    H = ideb + imax + kLoop * Penetration;

    ideb = ideb + (imax - 1) * lstep;
    ifin = ideb + (imax + 1) * lstep;

    ideb = max(1, ideb);
    ifin = min(ifin, length(S2));

    lstep = max(0, floor(lstep / 2));
end
H = H + 1;
H = max(H, 1);
H = min(H, length(S1));

%% Auto plot

if nargout == 0
    figure
    plot(S1, 'k'); grid on; hold on; plot(S2, 'b.');
    plot(H, S1(H), 'ro'); plot([H H], get(gca, 'YLim'), 'r'); hold off;
end
