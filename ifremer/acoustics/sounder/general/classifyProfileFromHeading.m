function [typeLine, strDirection, MeanHeading] = classifyProfileFromHeading(Heading, varargin)

[varargin, DistanceInterPings] = getPropertyValue(varargin, 'DistanceInterPings', []);
[varargin, DistanceInterBeams] = getPropertyValue(varargin, 'DistanceInterBeams', []); %#ok<ASGLU>

typeLine     = 'Undefined';
strDirection = '';
MeanHeading  = '';

Heading(isnan(Heading)) = [];
if length(Heading) <= 10
    typeLine = 'Short file';
    return
end

Heading = unwrap(Heading*(pi/180)) / (pi/180);

valStats = stats(Heading, 'Seuil', [10 90]);

% Heading(Heading < valStats.Quant_03_97(1)) = [];
% Heading(Heading > valStats.Quant_03_97(2)) = [];
Heading(Heading < valStats.Quant_x(1)) = [];
Heading(Heading > valStats.Quant_x(2)) = [];

valStats = stats(Heading);

if ~isempty(DistanceInterPings) &&  ~isempty(DistanceInterBeams)
    ratio = DistanceInterBeams / DistanceInterPings;
    if ratio > 50
        typeLine = 'Station';
        return
    end
end

MeanHeading = [];
if valStats.Sigma < 5
    typeLine = 'Line';
    MeanHeading = valStats.Moyenne;
    strDirection = getGlobalDirection(MeanHeading);
    return
end

if abs(valStats.Max - valStats.Min) > 90
    typeLine = 'U-Turn';
    return
end

if abs(valStats.Moyenne - valStats.Mediane) < 5
    MeanHeading = valStats.Moyenne;
    typeLine = 'Line';
    return
end



function strDirection = getGlobalDirection(Heading)

Heading = mod(Heading+360, 360);
str = {'S-N'; 'SW-NE'; 'W-E'; 'NW-SE'; 'N-S'; 'NE-SW'; 'E-W'; 'SE-NW'; 'S-N'};
k = 1 + floor((Heading + 22.5) / 45);
strDirection = str{k};