function Signal = code2val(Signal, InfoSignal, Type)

Signal = cast(Signal, Type);
if isfield(InfoSignal, 'ScaleFactor') && ~isempty(InfoSignal.ScaleFactor) && (InfoSignal.ScaleFactor ~= 1)
    Signal = Signal * InfoSignal.ScaleFactor;
end
if isfield(InfoSignal, 'AddOffset') && ~isempty(InfoSignal.AddOffset) && (InfoSignal.AddOffset ~= 0)
    Signal = Signal + InfoSignal.AddOffset;
end
