function I = compensation_WaterColumnSectors(I, TransmitSectorNumber, Unit)

if isempty(TransmitSectorNumber)
    TransmitSectorNumber = ones(1,size(I, 2));
end

subNaN    = find(isnan(TransmitSectorNumber));
subNonNaN = find(~isnan(TransmitSectorNumber));
if isempty(subNonNaN)
    return
end
if ~isempty(subNaN)
    TransmitSectorNumber(subNaN) = interp1(subNonNaN, TransmitSectorNumber(subNonNaN), subNaN, 'nearest', 'extrap');
end
liste = unique(TransmitSectorNumber(~isnan(TransmitSectorNumber)));
for k=1:length(liste)
    sub = find((TransmitSectorNumber == liste(k)) & ~isnan(TransmitSectorNumber));
    
    switch Unit
        case {1; 'dB'}
            X = I(:,sub);
        case {2; 'Amp'}
            X = reflec_Amp2dB(I(:,sub));
        case {3; 'Enr'}
            X = reflec_Enr2dB(I(:,sub));
        otherwise
            X = I(:,sub);
            str1 = 'L''unit� de l''image n''est pas d�termin�e, je fais comme si c''�tait en "dB"';
            str2 = 'The unit of the image is not determined, I do as if it was "dB"';
            my_warndlg(Lang(str1,str2), 1);
    end
    
    Med = median(X, 2, 'omitnan');
	X = X - Med;

    switch Unit
        case {1; 'dB'}
            I(:,sub) = X;
        case {2; 'Amp'}
            I(:,sub) = reflec_dB2Amp(X);
        case {3; 'Enr'}
            I(:,sub) = reflec_dB2Enr(X);
        otherwise
            I(:,sub) = X;
    end
    %     toc
end