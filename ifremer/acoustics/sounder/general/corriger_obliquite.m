% Correction d'obliquite d'une image sonar.
%
% Syntax
%   ICor = corriger_obliquite(I, H)
%
% Input Arguments 
%   I : Image brute sonar
%   H : Indice de la hauteur
%
% Examples
%   nomFic = getNomFicDatabase('DF1000_ExStr.imo')
%   a = cl_car_sonar(nomFic);
%   IRaw = get(a, 'grRawImage', 'subl', 1300:1370);
%   [HBab, HTri] = calHeight(IRaw);
%
%   ICorBab = corriger_obliquite(IRaw, HBab);
%   ICorTri = corriger_obliquite(IRaw, HTri);
%
%   subplot(1,3,1); imagesc(IRaw); colormap(gray(256));
%   [M, N] = size(IRaw);
%   hold on; plot(N/2 - HBab, 1:M, 'r'); plot(N/2 + HTri + 1, 1:M, 'g')
%   subplot(1,3,2); imagesc(ICorBab);
%   subplot(1,3,3); imagesc(ICorTri);
%
%   H = get(a, 'grRawImage_grSyncPrtUsrHeight', 'subl', 1300:1370);
%   resol   = get(a, 'grStrRawPixelSize');
%   ICor = corriger_obliquite(IRaw, H/resol);
%   imagesc(ICor); colormap(gray(256));
%
%   close(a);
%
% See also calculer_hauteur cl_car_sonar Authors
% Authors : JMA
%-------------------------------------------------------------------------------
  
function ICor = corriger_obliquite(IRaw, H, varargin)

[varargin, Waitbar] = getFlag(varargin, 'Waitbar'); %#ok<ASGLU>

[M, N] = size(IRaw);
ICor = NaN(M,N);

NH = length(H);
if Waitbar
    hw = create_waitbar('Correction geometrique', 'N', NH);
end

N = N / 2;
X  = 1:N;
X2 = X .^ 2;
for i=1:length(H)
    if Waitbar
        my_waitbar(i, NH, hw);
    end
    D = round(sqrt(X2 + (H(i)+1)^2));
    sub = find(D <= N);
    ICor(i, N + X(sub))       = IRaw(i, N + D(sub));
    ICor(i, N - X(sub) + 1)   = IRaw(i, N - D(sub) + 1);
end

if Waitbar
    my_close(hw);
end

if nargout == 0
    subplot(1,2,1); imagesc(IRaw); colormap(gray(256));
    hold on; plot(N - H, 1:M, 'b'); plot(N + H + 1, 1:M, 'b')
    subplot(1,2,2); imagesc(ICor);
end
