function [flag, ncFileName] = existCacheNetcdf(dataFileName)

[filepath, name] = fileparts(dataFileName);
ncFileName = fullfile(filepath, 'SonarScope', [name '.nc']);
flag = exist(ncFileName, 'file');
