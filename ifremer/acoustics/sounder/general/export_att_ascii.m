function flag = export_att_ascii(nomFicAtt, tAtt, Roll, Pitch, Heave, Heading, varargin)

flag = 0;

% Extraction des dateset heures,
nbRec   = numel(Roll);
pppp    = t2strIso8601(tAtt); % TODO � am�liorer
dateAtt = datestr(pppp(:,1:10), 'dd/mm/yyyy');
timeAtt = pppp(:,12:end);
clear pppp

%% Ouverture du fichier

fid = fopen(nomFicAtt, 'w+t');
if fid == -1
     messageErreurFichier(nomFicAtt, 'WriteFailure');
     return
end

%% Ecriture de l'ent�te. : le fichier est du m�me format que d�crit dans la proc�dure lecFicNav_DelphINSqui permet de le relire.

strHeader = 'UTC Date\tTime\tLatitude\tLongitude\tDepth\tHeading\tPitch\tRoll\tHeave';
strFormat = '%s\t%s\t%s\t%s\t%.5f\t%.5f\t%.5f\t%.5f\t%.5f';
fprintf(fid, '%s\n', strHeader);

%% Ecriture des donn�es tabul�es.

hw = create_waitbar('Write Attitude file', 'N', nbRec);
for k=1:nbRec
    my_waitbar(k, nbRec, hw)
    
    fprintf(fid, strFormat, dateAtt(k,:), timeAtt(k,:), '00�00.00', '00�00.00', 0.0, Heading(k), Pitch(k), Roll(k), Heave(k));
    fprintf(fid, '\n');
end
my_close(hw, 'MsgEnd')

%% Fermeture du fichier

fclose(fid);  
flag = 1;
