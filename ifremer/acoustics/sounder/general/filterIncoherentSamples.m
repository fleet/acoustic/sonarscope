% Détection de valeurs incohérentes en début de Ping (HF) ou fin de Ping (BF) pour certains fichiers
% SDF (ex : SSS_091009093200.sdf, SoReco2_010_111012160800.sdf).
% XTF (ex : SoReco2_065_111025153000.xtf).
% -----------------------------------------------------------------------

function [flag, X] = filterIncoherentSamples(X)

flag = 0; %#ok<NASGU>

[nbRows, ~] = size(X);

% Détection des échantillons incohérents et qui se répètent tout au long de
% l'acquisition :
% - On répète la 1ère ligne et on observe l'égalité de la matrice X avec cette 1ère ligne.
% - Détection des échatillons où la somme du résultat == par ligne le nb de lignes. 
dummy = (X == repmat(X(1,:), nbRows,1));
subDummy = (sum(dummy) == nbRows);
X(:,subDummy) = 0;

flag = 1;
