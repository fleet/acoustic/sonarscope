function [MeanRoll, StdRoll] = filtreAttitude(Time, Roll, ConstanteDeTemps)

if isa(Time, 'datetime')
    DeltatEnSec = mean(seconds(diff(Time)));
else
    T = Time.timeMat;
    DeltatEnSec = mean(diff(T)) * 24*3600;
end

nbSlidingSamples = 1 + 2 * floor(ConstanteDeTemps / DeltatEnSec / 2);
if nbSlidingSamples > 1
    MeanRoll = movmean(Roll, nbSlidingSamples);
    StdRoll  = movstd( Roll, nbSlidingSamples);
else
    MeanRoll = NaN(size(Roll), 'single');
    StdRoll  = NaN(size(Roll), 'single');
end

%{
figure;
h(1) = subplot(2,1,1); plot(Roll, 'k'); grid on; hold on;  plot(MeanRoll, 'r'); plot(MeanRoll-StdRoll, 'r-.'); plot(MeanRoll+StdRoll, 'r-.');
h(2) = subplot(2,1,2); plot(StdRoll, 'r'); grid on; title('std seul');
axis(h, 'tight');
linkaxes(h, 'x'); axis tight; drawnow;
%}
