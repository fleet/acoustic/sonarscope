function k = findIndVariable(Tables, Nom)
if iscell(Nom)
    for k=1:length(Tables)
        for kk=1:length(Nom)
            if strcmp(Tables(k).Name, Nom{kk})
                return
            end
        end
    end
else
    for k=1:length(Tables)
        if strcmp(Tables(k).Name, Nom)
            return
        end
    end
end
k = [];
