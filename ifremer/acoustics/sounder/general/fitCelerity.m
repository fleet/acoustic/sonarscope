function [CNew, AnglesNew, alpha, offsetAngles] = fitCelerity(XData, YData, C, Cs, fe, xAntenne, zAntenne, R1SamplesFiltre, DemiTxPulseLength, RxAnglesEarth_WC, OPTIONS)

alpha = 1;

XData            = double(XData);
YData            = double(YData);
C                = double(C);
Cs               = double(Cs);
fe               = double(fe);
xAntenne         = double(xAntenne);
zAntenne         = double(zAntenne);
R1SamplesFiltre  = double(R1SamplesFiltre);
RxAnglesEarth_WC = double(RxAnglesEarth_WC);

% [B, A] = butter(2, 0.2);
% subNonNaN = ~isnan(R1SamplesFiltre);
% R1SamplesFiltre(subNonNaN) = filtfilt(B, A, R1SamplesFiltre(subNonNaN));

%{
AnglesNew = computeNewAngles(RxAnglesEarth_WC, C, Cs, 0);
[XModel, YModel] = projectionLineaire(R1SamplesFiltre, AnglesNew, C, fe, xAntenne, zAntenne, DemiTxPulseLength);
FigUtils.createSScFigure(96757); hold off; PlotUtils.createSScPlot(XData, YData, 'k'); grid on;
hold on; PlotUtils.createSScPlot(XModel, YModel, 'b');
%}

lowerParameterBounds   = [C-20 -0.002 -0.001];
upperParameterBounds   = [C+20  0.002  0.001];
initialParameterValues = [C     0      0];

flagBab = 0;
flagTri = 0;
fittedParameters = lsqnonlin(@myErrorFunction, initialParameterValues, lowerParameterBounds, upperParameterBounds, OPTIONS);

%{
flagBab = 1;
flagTri = 0;
fittedParametersBab = lsqnonlin(@myErrorFunction, initialParameterValues, lowerParameterBounds, upperParameterBounds, OPTIONS);

flagBab = 0;
flagTri = 1;
fittedParametersTri = lsqnonlin(@myErrorFunction, initialParameterValues, lowerParameterBounds, upperParameterBounds, OPTIONS);
%}

% residuals = myErrorFunction(fittedParameters);
% figure(4552); plot(XData,residuals); grid on

CNew         = fittedParameters(1);
offsetAngles = fittedParameters(2);
xAntenneNew  = fittedParameters(3);

AnglesNew = computeNewAngles(RxAnglesEarth_WC, CNew, Cs, offsetAngles);

%{
[XModel, YModel] = projectionLineaire(R1SamplesFiltre, AnglesNew, CNew, fe, xAntenneNew, zAntenne, DemiTxPulseLength);
FigUtils.createSScFigure(96757); hold on; PlotUtils.createSScPlot(XModel, YModel, 'r'); hold off;
my_breakpoint;
%}


%{
CNewBab         = fittedParametersBab(1);
offsetAnglesBab = fittedParametersBab(2);
xAntenneNewBab  = fittedParametersBab(3);
AnglesNew = computeNewAngles(RxAnglesEarth_WC, CNewBab, Cs, offsetAnglesBab);
[XModel, YModel] = projectionLineaire(R1SamplesFiltre, AnglesNew, CNewBab, fe, xAntenneNewBab, zAntenne, DemiTxPulseLength);
FigUtils.createSScFigure(96757); hold on; PlotUtils.createSScPlot(XModel, YModel, 'm'); hold off;

CNewTri         = fittedParametersTri(1);
offsetAnglesTri = fittedParametersTri(2);
xAntenneNewTri  = fittedParametersTri(3);
AnglesNew = computeNewAngles(RxAnglesEarth_WC, CNewTri, Cs, offsetAnglesTri);
[XModel, YModel] = projectionLineaire(R1SamplesFiltre, AnglesNew, CNewTri, fe, xAntenneNewTri, zAntenne, DemiTxPulseLength);
FigUtils.createSScFigure(96757); hold on; PlotUtils.createSScPlot(XModel, YModel, 'g'); hold off;

fprintf('\n')
fprintf('%7.2f : celerity surface\n', Cs);
fprintf('%7.2f  %5.2f  %5.2f : Init\n', C, 0, 0)
fprintf('%7.2f  %5.2f  %5.2f : all beams\n', CNew, offsetAngles, xAntenneNew)
fprintf('%7.2f  %5.2f  %5.2f : bab\n', CNewBab, offsetAnglesBab, xAntenneNewBab)
fprintf('%7.2f  %5.2f  %5.2f : tri\n', CNewTri, offsetAnglesTri, xAntenneNewTri)
my_breakpoint;
%}

    function [XModel, YModel] = projectionLineaire(R1SamplesFiltre, AnglesNew, CNew, fe, xAntenneNew, zAntenne, DemiTxPulseLength)
        rangeMeters = double(R1SamplesFiltre * (CNew / (fe * 2)) - DemiTxPulseLength);
        % rangeMeters = rangeMeters + SectorTransmitDelay * (C/2000);
        XModel =   xAntenneNew + rangeMeters .* sind(AnglesNew);
        YModel = -(zAntenne + rangeMeters .* cosd(AnglesNew));
    end


    function AnglesNew = computeNewAngles(RxAnglesEarth_WC, CNew, Cs, offsetAngles)
        AnglesNew = asind(sind(double(RxAnglesEarth_WC - offsetAngles)) .* (CNew ./ Cs));
    end

    function M = myErrorFunction(x)
        CIteration      = x(1);
        offsetIteration = x(2);
        xAntenneIteration  = x(3);
        AnglesIteration = computeNewAngles(RxAnglesEarth_WC, CIteration, Cs, offsetIteration);
        
        [XModelIteration, YModelIteration] = projectionLineaire(R1SamplesFiltre, AnglesIteration, CIteration, fe, xAntenneIteration, zAntenne, DemiTxPulseLength);
        
        %{
        FigUtils.createSScFigure; PlotUtils.createSScPlot(XData, YData, 'k'); grid on;
        hold on; PlotUtils.createSScPlot(XModelIteration, YModelIteration, 'b');
        %}
        
        %         M = abs(YModelIteration - YData);
        M = sqrt((XModelIteration - XData).^2 + (YModelIteration - YData).^2);
        M(isnan(M)) = 0;
        
        %{
        CIteration
        FigUtils.createSScFigure(54127); PlotUtils.createSScPlot(M, '-'); grid on; hold on; drawnow
        %}
        %         M = nanmean(M.^2) * ones(size(M));
        
        % {
        if flagTri
            subBab = 1:128;
            M(subBab) = 0;
        end
        if flagBab
            subTri = 129:256;
            M(subTri) = 0;
        end
        % }
    end
end