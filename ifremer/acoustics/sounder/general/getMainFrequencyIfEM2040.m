function MainFrequency = getMainFrequencyIfEM2040(IdentSonar, CentralFrequency)

MainFrequency = [];
switch IdentSonar
    case {24; 25; 31} % EM2040S, EM2045, EM2040D
        F = mean(CentralFrequency(1,:), 'omitnan') / 1000;
        MainFrequencies = [200 300 400];
        [~, ind] = min(abs(MainFrequencies - F));
        MainFrequency = MainFrequencies(ind);
    otherwise
        return
end
