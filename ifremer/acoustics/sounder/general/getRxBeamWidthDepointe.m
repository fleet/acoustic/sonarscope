function RxBeamWidthDegDepointe = getRxBeamWidthDepointe(RxBeamWidthDeg, BeamPointingAngleDeg, RxInclinaison, subBab, subTri)

switch length(RxInclinaison)
    case 0
        Angles = BeamPointingAngleDeg;
    case 1 % Ca serait bizarre de renconter ce cas !
        Angles = BeamPointingAngleDeg + RxInclinaison;
    case 2
        AnglesBab = BeamPointingAngleDeg(subBab) + abs(RxInclinaison(1));
        AnglesTri = BeamPointingAngleDeg(subTri) - abs( RxInclinaison(2));
        Angles = [AnglesBab AnglesTri];
end

RxBeamWidthDegDepointe = RxBeamWidthDeg ./ cosd(Angles);
% figure; plot(RxBeamWidthDegDepointe); grid
