% TODO : fonction associ�e : inverse_getSounderParamsForcl_sounderXML

function [flag, SonarMode_1, SonarMode_2, SonarMode_3] = getSounderParamsForcl_sounderXML(Model, varargin)

% TODO :il faudra trouver un moyen de s'affranchir de cette fonction qui
% suppose la connaissance a priori de la structure des fichiers XML
% d�crivant les sondeurs.

[varargin, nbSwaths]     = getPropertyValue(varargin, 'nbSwaths',     []);
[varargin, presenceFM]   = getPropertyValue(varargin, 'presenceFM',   []);
[varargin, SounderMode]  = getPropertyValue(varargin, 'SounderMode',  []);
[varargin, Mute]         = getPropertyValue(varargin, 'Mute',         0);
[varargin, ScanningInfo] = getPropertyValue(varargin, 'ScanningInfo', []);
[varargin, nbSectors]    = getPropertyValue(varargin, 'nbSectors',    []);
[varargin, SignalLength] = getPropertyValue(varargin, 'SignalLength', []);
[varargin, isDual]       = getPropertyValue(varargin, 'isDual',       0);
[varargin, SounderMode2EM2040]      = getPropertyValue(varargin, 'SounderMode2EM2040',      []);
[varargin, PulseLengthConfIfEM2040] = getPropertyValue(varargin, 'PulseLengthConfIfEM2040', []); %#ok<ASGLU>

SonarMode_2 = [];
SonarMode_3 = [];

% TODO : reste � faire ou � v�rifier
% '1=EM12D' | '2=EM12S' | '3=EM300' | '4=EM1000' | '5=SAR' | '6=EM3000D' | '7=EM3000S' | '8=DF1000' | '9=DTS1' | '10=EM1002' | '11=EM120' | '12=Reson7150_12kHz' | '13=Reson7150_24kHz' | '14=Reson7111' | '15=Reson7125_200kHz' | '16=GeoSwath' | '17=ME70' | '18=EM710' | '19=EM3002D' | '20=EM3002S' | '21=EM2000' | '24=EM2040' | '25=Subbottom' | '26=Klein3000' | '27=EK60' | '28=HAC_Generic' | '29=ADCP' | '30=EM2040D'
% D�j� v�rifi�s : EM122, EM302

flag = 1;
switch Model
    case 'EM1000'
        % Jamais v�rifi�
        SonarMode_1 = SounderMode;
        SonarMode_2 = 1;
        
    case 'EM1002'
        % Jamais test� : � faire sur donn�es Marc Roche
        SonarMode_1 = SounderMode;
        SonarMode_2 = 1;
        
    case 'EM300'
        SonarMode_1 = SounderMode;
        SonarMode_2 = 1;
        
    case 'EM122' % TODO : R�introduire le mode very shallow comme pour l'EM302
        SonarMode_1 = nbSwaths; % 1=Single swath, 2=Double swath
        switch SounderMode
            case 2 % Shallow
                SonarMode_2 = 1;
            case 3 % Medium
                SonarMode_2 = 2;
            case 4 % Deep
%                 if presenceFM % Comment� par JMA le 24/01/2022 suite au mail de Ridha
                    SonarMode_2 = 4;
%                 else
%                     SonarMode_2 = 3;
%                 end
            case 5 % Very deep
                SonarMode_2 = 5;
        end
        
    case 'EM302'
        SonarMode_1 = nbSwaths; % 1=Single swath, 2=Double swath
        switch SounderMode
            case 1 % Very Shallow
                SonarMode_2 = 1;
            case 2 % Shallow
                SonarMode_2 = 2;
            case 3 % Medium
                SonarMode_2 = 3;
            case 4 % Deep
                if presenceFM
                    SonarMode_2 = 5;
                else
                    SonarMode_2 = 4;
                end
            case 5 % Very deep
                if presenceFM
                    SonarMode_2 = 7;
                else
                    SonarMode_2 = 6;
                end
            case 6 % Extra deep % TODO : pb � r�soudre Extra Deep2
                if presenceFM
                    SonarMode_2 = 9;
                else
                    SonarMode_2 = 8;
                end
            otherwise
                messageModeNotFound(Model, SounderMode, Mute);
                flag = 0;
        end
        
    case 'EM304' % EM304 rajout� sauvagement mais �a devrait aller
        SonarMode_1 = nbSwaths; % 1=Single swath, 2=Double swath
        switch SounderMode
            case 1 % Very Shallow
                SonarMode_2 = 1;
            case 2 % Shallow
                SonarMode_2 = 2;
            case 3 % Medium
                SonarMode_2 = 3;
            case 4 % Deep
                if presenceFM
                    SonarMode_2 = 5;
                else
                    SonarMode_2 = 4;
                end
            case 5 % Deeper
                if presenceFM
                    SonarMode_2 = 5;
                else
                    SonarMode_2 = 4;
                end
            case 6 % Very deep
                if presenceFM
                    SonarMode_2 = 7;
                else
                    SonarMode_2 = 6;
                end
            case 7 % Extra deep
                if presenceFM
                    SonarMode_2 = 9;
                else
                    SonarMode_2 = 8;
                end
            case 8 % Extreme deep
                if presenceFM
                    SonarMode_2 = 11;
                else
                    SonarMode_2 = 10;
                end
            otherwise
                messageModeNotFound(Model, SounderMode, Mute);
                flag = 0;
        end
        
    case {'EM710'; 'EM712'}
        SonarMode_1 = nbSwaths; % 1=Single swath, 2=Double swath
        switch SounderMode
            case 1 % Very shallow
                SonarMode_2 = 1;
            case 2 % Shallow
                SonarMode_2 = 2;
            case 3 % Medium
                SonarMode_2 = 3;
            case 4 % Deep
                if presenceFM
                    SonarMode_2 = 5;
                else
                    SonarMode_2 = 4;
                end
            case 5 % Very deep
                if presenceFM
                    SonarMode_2 = 7;
                else
                    SonarMode_2 = 6;
                end
            case 6 % Extra deep
                if presenceFM
                    SonarMode_2 = 9;
                else
                    SonarMode_2 = 8;
                end
            otherwise
                messageModeNotFound(Model, SounderMode, Mute);
                flag = 0;
        end
        
    case 20140 % ???????????????????
        % TODO
        SonarMode_1 = 1; % Provisoire
        SonarMode_2 = 1; % Provisoire
        SonarMode_3 = 1; % Provisoire
        flag = 1;
        
    case 'EM2045' % ???????????????????
        % TODO
        SonarMode_1 = 1; % Provisoire
        if isempty(SounderMode2EM2040) || isnan(SounderMode2EM2040)
            SonarMode_2 = 1; % Provisoire
        else
            SonarMode_2 = SounderMode2EM2040;
        end
        SonarMode_3 = 1; % Provisoire
        flag = 1;
        
    case 'EM850' % ???????????????????
        % TODO
        SonarMode_1 = 1; % Provisoire
        SonarMode_2 = 1; % Provisoire
        SonarMode_3 = 1; % Provisoire
        flag = 1;
        
    case {'EM2040'; 'EM2040S'; 'EM2040D'}
        SonarMode_1 = nbSwaths; % 1=Single swath, 2=Double swath
        if isempty(SounderMode2EM2040) || isnan(SounderMode2EM2040)
            if ScanningInfo
                SonarMode_2 = 3; % Scanning
            else
                if isDual % EM2040D
%                     if nbSectors == 2
%                         SonarMode_2 = 2; % Single sector
%                     else
%                         SonarMode_2 = 1; % Normal mode
%                     end
                    % Modif JMA le 15/03/2022
                    if nbSectors == 2
                        SonarMode_2 = 2; % Single sector
                    else
                        SonarMode_2 = 1; % Normal mode
                    end
                else % EM2040S
                    if nbSectors == 1
                        SonarMode_2 = 2; % Single sector
                    else
                        SonarMode_2 = 1; % Normal mode
                    end
                end
            end
        else
            SonarMode_2 = SounderMode2EM2040;
        end
        
        
        % TODO : en principe on devrait pouvoir faire SonarMode_3 = PulseLengthConfIfEM2040;
        % tout le temps. Il est pr�f�rable de mettre un message au cas o� PulseLengthConfIfEM2040 serait vide
        
        if isempty(SignalLength)
            if isempty(PulseLengthConfIfEM2040)
                SonarMode_3 = 1; % TODO : trouver une solution
                messageModeNotFound(Model, SounderMode, Mute);
            else
                SonarMode_3 = PulseLengthConfIfEM2040;
            end
            %             messageModeNotFound(Model, SounderMode, Mute);
        else
            if isempty(PulseLengthConfIfEM2040)
                PulseLengthConfIfEM2040 = 1; % TODO 
            end
            PulseLengthShortSpec = floor(SignalLength(1)*1e6);
            switch SonarMode_1
                case 1 % Single swath
                    switch SounderMode
                        case 1 % 200 kHz
                            switch SonarMode_2
                                case 1 % Normal mode
                                    switch PulseLengthShortSpec
                                        case {70; 38; 37}
                                            SonarMode_3 = 1;
                                        case {200; 108; 107}
                                            SonarMode_3 = 2;
                                        case {600; 324; 320}
                                            SonarMode_3 = 3;
                                        case {3000; 12000}
                                            SonarMode_3 = 4;
                                        otherwise
                                            SonarMode_3 = PulseLengthConfIfEM2040; % PulseLengthShortSpec = 290 !
%                                             messageModeNotFound(Model, SounderMode, Mute);
                                    end
                                case 2 % Single sector
                                    switch PulseLengthShortSpec
                                        case {35; 19}
                                            SonarMode_3 = 1;
                                        case {70; 38; 37}
                                            SonarMode_3 = 2;
                                        case {150; 81; 80}
                                            SonarMode_3 = 3;
                                        case 1500
                                            SonarMode_3 = 4;
                                        otherwise
                                            SonarMode_3 = PulseLengthConfIfEM2040;
                                            messageModeNotFound(Model, SounderMode, Mute);
                                    end
                                case 3 % Scanning
                                    switch PulseLengthShortSpec
                                        case {35; 19}
                                            SonarMode_3 = 1;
                                        case {70; 38}
                                            SonarMode_3 = 2;
                                        case {150; 81}
                                            SonarMode_3 = 3;
                                        case 1500
                                            SonarMode_3 = 4;
                                        otherwise
                                            SonarMode_3 = PulseLengthConfIfEM2040;
                                            messageModeNotFound(Model, SounderMode, Mute);
                                    end
                            end
                            
                        case 2 % 300 kHz
                            switch SonarMode_2
                                case 1 % Normal mode
                                    switch PulseLengthShortSpec
                                        case {70; 38; 37}
                                            SonarMode_3 = 1;
                                        case {200; 108; 107}
                                            SonarMode_3 = 2;
                                        case {600; 324; 320}
                                            SonarMode_3 = 3;
                                        case {2000; 6000}
                                            SonarMode_3 = 4;
                                        otherwise
                                            SonarMode_3 = PulseLengthConfIfEM2040;
%                                             messageModeNotFound(Model, SounderMode, Mute);
                                    end
                                case 2 % Single sector
                                    switch PulseLengthShortSpec
                                        case {35; 19}
                                            SonarMode_3 = 1;
                                        case {70; 38; 37}
                                            SonarMode_3 = 2;
                                        case {150; 81; 80}
                                            SonarMode_3 = 3;
                                        case 1500
                                            SonarMode_3 = 4;
                                        otherwise
                                            SonarMode_3 = PulseLengthConfIfEM2040;
                                            messageModeNotFound(Model, SounderMode, Mute); % Cas trouv� avec "D:\Temp\ExData\ALL-EM2040SimonsStevin\0029_20180306_213505_SimonStevin.all"
                                    end
                                case 3 % Scanning
                                    switch PulseLengthShortSpec
                                        case {35; 19}
                                            SonarMode_3 = 1;
                                        case {70; 38}
                                            SonarMode_3 = 2;
                                        case {150; 81}
                                            SonarMode_3 = 3;
                                        case 1500
                                            SonarMode_3 = 4;
                                        otherwise
                                            SonarMode_3 = PulseLengthConfIfEM2040;
                                            messageModeNotFound(Model, SounderMode, Mute);
                                    end
                            end
                            
                        case 3 % 400 kHz
                            switch SonarMode_2
                                case 1 % Normal mode
                                    switch PulseLengthShortSpec
                                        case {50; 27}
                                            SonarMode_3 = 1;
                                        case {100; 54}
                                            SonarMode_3 = 2;
                                        case {200; 108; 107}
                                            SonarMode_3 = 3;
                                        otherwise
                                            SonarMode_3 = PulseLengthConfIfEM2040;
                                            messageModeNotFound(Model, SounderMode, Mute);
                                    end
                                case 2 % Single sector
                                    switch PulseLengthShortSpec
                                        case {25; 14}
                                            SonarMode_3 = 1;
                                        case {50; 27}
                                            SonarMode_3 = 2;
                                        case {100; 54}
                                            SonarMode_3 = 3;
                                        otherwise
                                            SonarMode_3 = PulseLengthConfIfEM2040;
                                            messageModeNotFound(Model, SounderMode, Mute);
                                    end
                                case 3 % Scanning
                                    switch PulseLengthShortSpec
                                        case {25; 14}
                                            SonarMode_3 = 1;
                                        case {50; 27}
                                            SonarMode_3 = 2;
                                        case {100; 54}
                                            SonarMode_3 = 3;
                                        otherwise
                                            SonarMode_3 = PulseLengthConfIfEM2040;
                                            messageModeNotFound(Model, SounderMode, Mute);
                                    end
                            end
                        otherwise
                            messageModeNotFound(Model, SounderMode, Mute);
                            flag = 0;
                    end
                    
                case 2 % Double swath
                    switch SounderMode
                        case 1 % 200 kHz
                            switch SonarMode_2
                                case 1 % Normal mode
                                    switch PulseLengthShortSpec
                                        case {100; 54}
                                            SonarMode_3 = 1;
                                        case {200; 108}
                                            SonarMode_3 = 2;
                                        case {600; 324}
                                            SonarMode_3 = 3;
                                        case {3000 12000}
                                            SonarMode_3 = 4;
                                        otherwise
                                            SonarMode_3 = PulseLengthConfIfEM2040;
                                            messageModeNotFound(Model, SounderMode, Mute);
                                    end
                                case 2 % Single sector
                                    switch PulseLengthShortSpec
                                        case {35; 19}
                                            SonarMode_3 = 1;
                                        case {70; 38}
                                            SonarMode_3 = 2;
                                        case {150; 108}
                                            SonarMode_3 = 3;
                                        case 1500
                                            SonarMode_3 = 4;
                                        otherwise
                                            SonarMode_3 = PulseLengthConfIfEM2040;
                                            messageModeNotFound(Model, SounderMode, Mute);
                                    end
                            end
                            
                        case 2 % 300 kHz
                            switch SonarMode_2
                                case 1 % Normal mode
                                    switch PulseLengthShortSpec
                                        case {70; 38}
                                            SonarMode_3 = 1;
                                        case{200;108} 
                                            SonarMode_3 = 2;
                                        case {600; 324}
                                            SonarMode_3 = 3;
                                        case {2000; 6000}
                                            SonarMode_3 = 4;
                                        otherwise
                                            SonarMode_3 = PulseLengthConfIfEM2040;
                                            messageModeNotFound(Model, SounderMode, Mute);
                                    end
                                case 2 % Single sector
                                    switch PulseLengthShortSpec
                                        case {45; 28}
                                            SonarMode_3 = 1;
                                        case {70; 38}
                                            SonarMode_3 = 2;
                                        case {150; 81}
                                            SonarMode_3 = 3;
                                        case 1500
                                            SonarMode_3 = 4;
                                        otherwise
                                            SonarMode_3 = PulseLengthConfIfEM2040;
                                            messageModeNotFound(Model, SounderMode, Mute);
                                    end
                            end
                            
                        case 3 % 400 kHz
                            switch SonarMode_2
                                case 1 % Normal mode
                                    switch PulseLengthShortSpec
                                        case {50; 28}
                                            SonarMode_3 = 1;
                                        case {100; 54}
                                            SonarMode_3 = 2;
                                        case {200; 108}
                                            SonarMode_3 = 3;
                                        otherwise
                                            SonarMode_3 = PulseLengthConfIfEM2040;
                                            messageModeNotFound(Model, SounderMode, Mute);
                                    end
                                case 2 % Single sector
                                    switch PulseLengthShortSpec
                                        case {25; 14}
                                            SonarMode_3 = 1;
                                        case {50; 28}
                                            SonarMode_3 = 2;
                                        case {100; 54}
                                            SonarMode_3 = 3;
                                        otherwise
                                            SonarMode_3 = PulseLengthConfIfEM2040;
                                            messageModeNotFound(Model, SounderMode, Mute);
                                    end
                            end
                        otherwise
                            messageModeNotFound(Model, SounderMode, Mute);
                            flag = 0;
                    end
            end
        end
        
    otherwise
        if ~Mute
            str1 = sprintf('Sondeur "%s" pas encore branch� dans cl_simrad_all/getSounderParamsForcl_sounderXML', Model);
            str2 = sprintf('Sounder "%s" not yet plugged in cl_simrad_all/getSounderParamsForcl_sounderXML', Model);
            my_warndlg(Lang(str1,str2), 0, 'Tag', 'PasEncorePluggeg', 'TimeDelay', 60);
        end
        
        SonarMode_1 = 1;
        SonarMode_2 = 1;
        SonarMode_3 = 1;
        flag = 1;
end


function messageModeNotFound(Model, SounderMode, Mute)
if ~Mute && ~my_isdeployed % rencontr� sur donn�es Shallow Survey 2018
    str1 = sprintf('Sondeur "%s" ne connait pas le mode %d (dans cl_simrad_all/getSounderParamsForcl_sounderXML)', Model, SounderMode);
    str2 = sprintf('Sounder "%s" does not know mode=%d (in getSounderParamsForcl_sounderXML)', Model, SounderMode);
    my_warndlg(Lang(str1,str2), 0, 'Tag', 'getSounderParamsForcl_sounderXMLModeNotIdentified');
end
