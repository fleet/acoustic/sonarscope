function [flag, CourbeBS] = get_CourbeBS(nomDir)

str1 = 'S�lectionnez un fichier de compensation de BS';
str2 = 'Give a BS compensation file';
[flag, CorFileBS] = my_uigetfile('*.mat', Lang(str1,str2), nomDir);
if ~flag
    str1 = 'Un fichier de compensation de BS est n�cessaire pour r�aliser la compensation.';
    str2 = 'A BS compensation file is necessary to continue.';
    my_warndlg(Lang(str1,str2), 1);
    flag = 0;
    return
end

%     CourbeBS = loadmat(CorFileBS, 'nomVar', 'CourbeConditionnelle');
CourbeBS = load_courbesStats(CorFileBS);

bilan = CourbeBS.bilan{1};

flag = strcmp(bilan(1).Tag, 'BS');
if ~flag
    str1 = sprintf('Le fichier "%s" ne contient pas de mod�le de "BS".', CorFileBS);
    str2 = sprintf('File "%s" does not contain any "BS" models.', CorFileBS);
    my_warndlg(Lang(str1,str2), 1);
    return
end

%                 ModelBS = bilan(1).model;
%                 plot(ModelBS)
