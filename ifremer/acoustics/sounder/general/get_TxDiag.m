% nomDirCor = 'W:\calimero\EM300\ZoneC\FH_divers'
% [flag, ModelTxDiag] = get_TxDiag(nomDirCor)
% plot(ModelTxDiag{3})

function [flag, ModelTxDiag] = get_TxDiag(nomDirCor)

ModelTxDiag = cell(0);

str1 = 'TODO';
str2 = 'You are going to select DiagTx compensation files, be sure you give files for all modes wich will be found in the sonar data files.';
my_warndlg(Lang(str1,str2), 1);

[flag, ListeFicFic] = uiSelectFiles('ExtensionFiles', '.mat', 'RepDefaut', nomDirCor);
if ~flag
    return
end

Mode = 1;
for iFic=1:length(ListeFicFic)
    CorFileTxDiag = ListeFicFic{iFic};
    CourbeConditionnelle = load_courbesStats(CorFileTxDiag);

    bilan = CourbeConditionnelle.bilan{1};
    
    flag = isfield(bilan(1), 'model');
    if ~flag
        msg = sprintf('File %s does not contain any "model"', CorFileTxDiag);
        my_warndlg(msg, 1);
        return
    end

    flag = strcmp(bilan(1).Tag, 'DiagTx');
    if ~flag
        str1 = sprintf('Le fichier "%s" ne contient pas de mod�le de "DiagTx".', CorFileTxDiag);
        str2 = sprintf('File "%s" does not contain any "DiagTx" models.', CorFileTxDiag);
        my_warndlg(Lang(str1,str2), 1);
        return
    end

    TagSup = bilan(1).TagSup;
    if ~isempty(TagSup) && strcmp(TagSup{1}, 'Sonar.Mode_1')
        Mode = TagSup{2};
    else
        [flag, Mode] = inputOneParametre('Value to add on Mode', 'Value', ...
            'Value', Mode, 'MinValue', 1, 'MaxValue', 6, 'Format', '%d');
        if ~flag
            return
        end
    end

    for k=1:length(bilan)
        if ~isempty(bilan(k).model)
            ModelTxDiag{Mode, 1}(bilan(k).numVarCondition) = bilan(k).model;
        end
    end

    Mode = Mode + 1;
end


