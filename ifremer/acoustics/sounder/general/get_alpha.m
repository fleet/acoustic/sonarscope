function [flag, alpha] = get_alpha(varargin)

[varargin, ValInit] = getPropertyValue(varargin, 'Init', 0); %#ok<ASGLU>

alpha = 0;
while alpha == 0
    [flag, alpha] = inputOneParametre('Absorption coefficient is necessary for reflectivity calibration', 'Absorption coef value', ...
        'Value', ValInit, 'Unit', 'dB/km/m', 'MinValue', 0, 'MaxValue', 200);
    if ~flag
        return
    end
    if alpha == 0
        my_warndlg('Alpha must be different than 0', 1);
    end
end
