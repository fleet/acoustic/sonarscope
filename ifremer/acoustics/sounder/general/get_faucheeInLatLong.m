function [Lat, Lon] = get_faucheeInLatLong(FishLatitude, FishLongitude, Heading, D_Bab, D_Tri, A_Bab, A_Tri)

Heading = double(Heading);

Carto = cl_carto('Ellipsoide.Type', 11);
E2 = get(Carto, 'Ellipsoide.Excentricite') ^ 2;
A  = get(Carto, 'Ellipsoide.DemiGrandAxe');

Lat = NaN(length(FishLatitude), 2);
Lon = NaN(length(FishLatitude), 2);
for k=1:length(FishLatitude)
    sinLN2 = sind(FishLatitude(k)) .^ 2;
    E2MoinssinLN2 = 1 - E2 * sinLN2;
    NORM = A ./ sqrt(E2MoinssinLN2);
    RAY0 = NORM * (1 - E2) ./ E2MoinssinLN2;
    SC = sind(Heading(k));
    CC = cosd(Heading(k));
    DL = double([A_Bab(k) A_Tri(k)]);
    DT = double([D_Bab(k) D_Tri(k)]);
    Lat(k,:) = FishLatitude(k)  + (DL * CC - DT * SC) .* ((180/pi) ./ RAY0);
    Lon(k,:) = FishLongitude(k) + (DL * SC + DT * CC) .* ((180/pi) ./ NORM ./ cosd(FishLatitude(k)));
end
