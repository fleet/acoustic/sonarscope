function [flag, UseRTK] = guiUseRTK(isBathy, hasRTK)

if ~isBathy
    UseRTK = 0;
    flag   = 1;
    return
end

if isempty(hasRTK)
    str1 = 'Dans l''hypoth�se o� il y aurait des donn�es RTK, voulez-vous les prendre en compte ?';
    str2 = 'In case there is RTK data in your RDF file, do you want to take it into account ?';
    [rep, flag] = my_questdlg(Lang(str1,str2));
    if ~flag
        return
    end
    UseRTK = (rep == 1);
    if UseRTK
        messageRTK
    end
    return
end

if any(hasRTK)
    str1 = 'Vos donn�es se pr�tent � une correction mar�graphique bas�e sur les donn�es RTK. Voulez-vous  prendre l''information RTK en compte ?';
    str2 = 'Your dataset can be processed with RTK data. Do you want to proceed with it ?';
    [rep, flag] = my_questdlg(Lang(str1,str2));
    if ~flag
        return
    end
    UseRTK = (rep == 1);
    
    if UseRTK
        messageRTK
    end
else
    UseRTK = 0;
    flag = 1;
end


function messageRTK
str1 = 'ATTENTION : pour pouvoir utiliser la donn�e RTK il est ABSOLUMENT NECESSAIRE de faire une v�rification de la donn�e par le module "Survey processings / .all / Clean data / Check RTK". D''autre part, il est recommand� de fournir � SonarScope un fichier de mar�e pour pouvoir corriger les pings qui sont d�pourvus de donn�e RTK.';
str2 = 'WARNING : in order to use RTK data, you ABSOLUTELY HAVE TO CHECK the data using "Survey processings / .all / Clean data / Check RTK". Nevertheless, I recommand that you give a tide file to SonarScope that will be used to process the pings where the RTK data is off.';
my_warndlg(Lang(str1,str2), 1);
