function BSStatus = init_BSStatus(varargin)

[varargin, EmModel]                           = getPropertyValue(varargin, 'EmModel',                           []);
[varargin, Absorption]                        = getPropertyValue(varargin, 'Absorption',                        []);
[varargin, SonarBS_etat]                      = getPropertyValue(varargin, 'SonarBS_etat',                      2);
[varargin, SonarAireInso_etat]                = getPropertyValue(varargin, 'SonarAireInso_etat',                1);
[varargin, SonarAireInso_origine]             = getPropertyValue(varargin, 'SonarAireInso_origine',             1);
[varargin, SonarBS_LambertCorrection]         = getPropertyValue(varargin, 'SonarBS_LambertCorrection',         1);
[varargin, SonarTVG_origine]                  = getPropertyValue(varargin, 'SonarTVG_origine',                  1);
[varargin, SonarTVG_ConstructTypeCompens]     = getPropertyValue(varargin, 'SonarTVG_ConstructTypeCompens',     1);
[varargin, SonarBS_SpecularReestablishment]   = getPropertyValue(varargin, 'SonarBS_SpecularReestablishment',   1);
[varargin, IdentAlgoSnippets2OneValuePerBeam] = getPropertyValue(varargin, 'IdentAlgoSnippets2OneValuePerBeam', []); %#ok<ASGLU>


BSStatus.SonarNE_etat = 1; % '1=Compensated' | '2=Not compensated'
BSStatus.SonarGT_etat = 1; % '1=Compensated' | '2=Not compensated'
BSStatus.SonarSH_etat = 1; % '1=Compensated' | '2=Not compensated'

BSStatus.SonarTVG_etat                 = 1; % '1=Compensated' | '2=Not compensated'
BSStatus.SonarTVG_origine              = SonarTVG_origine; % '1=Constructeur' | '2=Ifremer'
BSStatus.SonarTVG_ConstructTypeCompens = SonarTVG_ConstructTypeCompens; % '1=Function' | '2=Nodes'

if isempty(Absorption)
    BSStatus.SonarTVG_ConstructAlpha = 0;
else
    BSStatus.SonarTVG_ConstructAlpha = mean(Absorption(:), 'omitnan');
end
BSStatus.SonarTVG_ConstructCoefDiverg = 40;
BSStatus.SonarTVG_ConstructConstante  = 0;
BSStatus.SonarTVG_ConstructTable      = [];
BSStatus.SonarTVG_IfremerAlpha        = BSStatus.SonarTVG_ConstructAlpha;

if isempty(EmModel)
    BSStatus.SonarDiagEmi_etat                 = 2; % '1=Compensated' | '2=Not compensated'
    BSStatus.SonarDiagEmi_origine              = 1; % '1=Constructeur' | '2=Ifremer'
    BSStatus.SonarDiagEmi_ConstructTypeCompens = 1; % '1=Function' | '2=Nodes'
    BSStatus.SonarDiagEmi_IfremerTypeCompens   = 1; % '1=Function' | '2=Nodes'
else
    switch EmModel
        case 1002
            %         disp('view-seabedImage : Est-ce que les diagrammes d''emission sont compenses ?')
            BSStatus.SonarDiagEmi_etat                 = 2; % '1=Compensated' | '2=Not compensated'
            BSStatus.SonarDiagEmi_origine              = 1; % '1=Constructeur' | '2=Ifremer'
            %2 ici et entrer la table
            BSStatus.SonarDiagEmi_ConstructTypeCompens = 1; % '1=Function' | '2=Nodes'
            BSStatus.SonarDiagEmi_IfremerTypeCompens   = 1; % '1=Function' | '2=Nodes'
            
        case 850 % ME70
            BSStatus.SonarDiagEmi_etat                 = 1; % '1=Compensated' | '2=Not compensated'
            BSStatus.SonarDiagEmi_origine              = 2; % '1=Constructeur' | '2=Ifremer'
            BSStatus.SonarDiagEmi_ConstructTypeCompens = 1; % '1=Function' | '2=Nodes'
            BSStatus.SonarDiagEmi_IfremerTypeCompens   = 1; % '1=Function' | '2=Nodes'
            
        case 710 % TODO : question � poser � Ragnar : est-il compens� par le diag spline ?
            BSStatus.SonarDiagEmi_etat                 = 1; % '1=Compensated' | '2=Not compensated'
            BSStatus.SonarDiagEmi_origine              = 1; % '1=Constructeur' | '2=Ifremer'
            BSStatus.SonarDiagEmi_ConstructTypeCompens = 1; % '1=Function' | '2=Nodes'
            BSStatus.SonarDiagEmi_IfremerTypeCompens   = 1; % '1=Function' | '2=Nodes'
        case 2040 % TODO : question � poser � Ragnar : est-il compens� par le diag spline ?
            BSStatus.SonarDiagEmi_etat                 = 2; % '1=Compensated' | '2=Not compensated'
            BSStatus.SonarDiagEmi_origine              = 1; % '1=Constructeur' | '2=Ifremer'
            
            %{
% TODO : Attention : on fait comme si les diagrammes �taient corrig�s pour �viter que les
% modules de calibration se plantent tant que l'on a pas fini de mettre les fichiers XML au
% point
BSStatus.SonarDiagEmi_etat                   = 1; % '1=Compensated' | '2=Not compensated'
BSStatus.SonarDiagEmi_origine                = 2; % '1=Constructeur' | '2=Ifremer'
            %}
            
            BSStatus.SonarDiagEmi_ConstructTypeCompens = 1; % '1=Function' | '2=Nodes'
            BSStatus.SonarDiagEmi_IfremerTypeCompens   = 1; % '1=Function' | '2=Nodes'
            
        otherwise % 300, 302
            BSStatus.SonarDiagEmi_etat                 = 1; % '1=Compensated' | '2=Not compensated'
            BSStatus.SonarDiagEmi_origine              = 1; % '1=Constructeur' | '2=Ifremer'
            BSStatus.SonarDiagEmi_ConstructTypeCompens = 1; % '1=Function' | '2=Nodes'
            BSStatus.SonarDiagEmi_IfremerTypeCompens   = 1; % '1=Function' | '2=Nodes'
    end
end

BSStatus.SonarDiagRec_etat                  = 2; % '1=Compensated' | '2=Not compensated'
BSStatus.SonarDiagRec_origine               = 1; % '1=Constructeur' | '2=Ifremer'
BSStatus.SonarDiagRec_ConstructTypeCompens  = 1; % '1=Function' | '2=Nodes'
BSStatus.SonarDiagRec_IfremerTypeCompens    = 1; % '1=Function' | '2=Nodes'

BSStatus.SonarAireInso_etat    = SonarAireInso_etat; % {'1=Compensated'} | '2=Not compensated'
BSStatus.SonarAireInso_origine = SonarAireInso_origine; % '1=Constructeur' | '2=Ifremer RxBeamAngle' | '3=Ifremer IncidenceAngle'

BSStatus.SonarBS_etat                = SonarBS_etat; % 1; % '1=Compensated' | '2=Not compensated'
BSStatus.SonarBS_origine             = 1; % '1=Belle Image' | '2=Full compensation'
BSStatus.SonarBS_origineBelleImage   = 1; % '1=Lambert' | '2=Lurton'
BSStatus.SonarBS_origineFullCompens  = 1; % '1=Function' | '2=Nodes'
BSStatus.SonarBS_LambertCorrection   = SonarBS_LambertCorrection; % {'R/Rn'; 'BeamPointingAngle'; 'IncidenceAngle'}
BSStatus.SonarBS_IfremerCompensTable = [];

BSStatus.SonarBS_IdentAlgoSnippets2OneValuePerBeam = IdentAlgoSnippets2OneValuePerBeam;
BSStatus.SonarBS_SpecularReestablishment           = SonarBS_SpecularReestablishment;
