function [flag, TideFile] = inputTideFile(isBathy, nomDirImport)

TideFile = [];
flag     = 1;

if isBathy
    [flag, flagTideCorrection] = question_TideCorrection;
    if ~flag
        return
    end
    
    if flagTideCorrection
        [flag, TideFile] = my_uigetfile({'*.tid*;*.ttb;*.mar;*.txt','Tide Files (*.tid*,*.ttb,*.mar,*.txt)';
            '*.tid*',  'Tide file (*.tid*)'; ...
            '*.ttb',  'Caraibes Tide file (*.ttb)'; ...
            '*.mar','SHOM tide file (*.mar)'; ...
            '*.txt',  'Text file (*.txt)'; ...
            '*.*',  'All Files (*.*)'}, 'Give a tide file (cancel instead)', nomDirImport);
        if flag
            read_Tide(TideFile, 'CleanData', 1);
        else
            TideFile = [];
        end
    end
end
