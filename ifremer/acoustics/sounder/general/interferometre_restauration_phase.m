function [pwNew, Z] = interferometre_restauration_phase(phRaw, h, fe, ka, iPing, flagPlot)

pwNew = phRaw;
% return

c = 1500;

%{
figure(2203);
PlotUtils.createSScPlot(phRaw), grid on; title('phRaw')
%}

PhrbrUW = unwrap(phRaw * (pi/180)) * (180/pi); % Dewrappage de la phase bruit�e mesur�e
%{
figure(2204);
ha(1) = subplot(2,1,1); PlotUtils.createSScPlot(phRaw), grid on; title('phRaw')
ha(2) = subplot(2,1,2); PlotUtils.createSScPlot(PhrbrUW,'r'); grid on;  title('Unwrapped interfero phase');
linkaxes(ha, 'x'); axis tight; drawnow;
%}

% Resolution de l'ambiguite de determination de phase UW

%{
MoyPhaseFin = mean(PhrbrUW(end-100:end));
Nambig = round(MoyPhaseFin/2/pi);
PhrbrUWAmbRem = PhrbrUW - Nambig*2*pi;
figure(2204); subplot(2,1,2);
hold on; PlotUtils.createSScPlot(PhrbrUWAmbRem, 'g'); title('Unwrapped interfero phase ambiguity removed'); hold off;
%}

%% Traitement de la phase bruit�e par filtrage du sin et du cos

AngEst = phRaw * (pi/180);          % Angle arrivee estim� sur phase bruit�e

sinDdPh = sin(AngEst);        % Sinus de diff de phase estim�e
cosDdPh = cos(AngEst);        % Sinus de diff de phase estim�e

[B1, A1] = butter(2, 0.1);            % Filtre - Freq 0.2
sinDdPhFilt1 = filtfilt(B1, A1, double(sinDdPh));
sinDdPhFilt1(sinDdPhFilt1 >= 1) = 1;     % ecretage � 1
sinDdPhFilt1(sinDdPhFilt1 <= -1) = -1;     % ecretage � -1
cosDdPhFilt1 = filtfilt(B1, A1, double(cosDdPh));
cosDdPhFilt1(cosDdPhFilt1 >= 1) = 1;     % ecretage � 1
cosDdPhFilt1(cosDdPhFilt1 <= -1) = -1;     % ecretage � -1

[B2, A2] = butter(2, 0.01);            % Filtre - Freq 0.01 
sinDdPhFilt2 = filtfilt(B2, A2, double(sinDdPh));
sinDdPhFilt2(sinDdPhFilt2 >= 1) = 1;     % ecretage � 1
sinDdPhFilt2(sinDdPhFilt2 <= -1) = -1;     % ecretage � -1
cosDdPhFilt2 = filtfilt(B2, A2, double(cosDdPh));
cosDdPhFilt2(cosDdPhFilt2 >= 1) = 1;     % ecretage � 1
cosDdPhFilt2(cosDdPhFilt2 <= -1) = -1;     % ecretage � -1

NbT = length(cosDdPhFilt2);
alfa = linspace(0, 1, NbT);
sinDdPhFilt3 = (1-alfa) .* sinDdPhFilt1 + alfa .* sinDdPhFilt2;
cosDdPhFilt3 = (1-alfa) .* cosDdPhFilt1 + alfa .* cosDdPhFilt2;

DdPhFilt3 = atan2(sinDdPhFilt3, cosDdPhFilt3);
% figure; plot(DdPhFilt3 * (180/pi)); grid on;
DdPhFiltUW3 = (180/pi) * unwrap(DdPhFilt3) / (4*ka);
% figure; plot(DdPhFiltUW3); grid on;
sub = 1:length(DdPhFiltUW3);
d = (-h - 1 + sub) * c / (2*fe);
Z = -d .* cosd(DdPhFiltUW3);

if flagPlot
    figure(2206);
    % subplot(3,1,1); plot(sinPhr); xlabel('Time'); title('Exact phase sine');
    subplot(4,2,1); plot(sinDdPh); xlabel('Time'); title(['Estimated phase sine - ', num2str(iPing)]);
    hold on; plot(cosDdPh,'r'); grid on; hold off;
    subplot(4,2,3); plot(sinDdPhFilt1); xlabel('Time'); title('Filtered est. phase sine');
    hold on; plot(cosDdPhFilt1,'r'); grid on; hold off;
    subplot(4,2,5); plot(sinDdPhFilt2); xlabel('Time'); title('Filtered est. phase sine');
    hold on; plot(cosDdPhFilt2,'r'); grid on; hold off;
    subplot(4,2,7); plot(sinDdPhFilt3); xlabel('Time'); title('Filtered est. phase sine');
    hold on; plot(cosDdPhFilt3,'r'); grid on; hold off;
    subplot(4,2,2); plot(DdPhFiltUW3); grid on; xlabel('Time'); title('Filtered est. phase sine');
    subplot(4,2,4); plot(Z); grid on; xlabel('Time'); title('Filtered est. phase sine');
end

pwNew = DdPhFiltUW3;
