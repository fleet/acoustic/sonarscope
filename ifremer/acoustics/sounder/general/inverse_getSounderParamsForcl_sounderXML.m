% TODO : fonction associ�e : getSounderParamsForcl_sounderXML

function [flag, SounderMode] = inverse_getSounderParamsForcl_sounderXML(Model, SonarMode_1, SonarMode_2, varargin)

% TODO :il faudra trouver un moyen de s'affranchir de cette fonction qui
% suppose la connaissance a priori de la structure des fichiers XML
% d�crivant les sondeurs.

[varargin, Mute] = getPropertyValue(varargin, 'Mute', 0); %#ok<ASGLU>

SounderMode = [];

% TODO : reste � faire ou � v�rifier
% '1=EM12D' | '2=EM12S' | '3=EM300' | '4=EM1000' | '5=SAR' | '6=EM3000D' | '7=EM3000S' | '8=DF1000' | '9=DTS1' | '10=EM1002' | '11=EM120' | '12=Reson7150_12kHz' | '13=Reson7150_24kHz' | '14=Reson7111' | '15=Reson7125_200kHz' | '16=GeoSwath' | '17=ME70' | '18=EM710' | '19=EM3002D' | '20=EM3002S' | '21=EM2000' | '24=EM2040' | '25=Subbottom' | '26=Klein3000' | '27=EK60' | '28=HAC_Generic' | '29=ADCP' | '30=EM2040D'
% D�j� v�rifi�s : EM122, EM302

flag = 1;
switch Model
    case 'EM1000'
        % Jamais v�rifi�
        SounderMode = SonarMode_1;
        
    case 'EM1002'
        % Jamais test� : � faire sur donn�es Marc Roche
        SounderMode = SonarMode_1;
        
    case 'EM300'
        SounderMode = SonarMode_1;
        
    case 'EM122' % TODO : R�introduire le mode very shallow comme pour l'EM302
        switch SonarMode_2
            case 1 % Shallow
                SounderMode = 2;
            case 2 % Medium
                SounderMode = 3;
            case {3; 4} % Deep CW and Deep FM
                SounderMode = 4;
            case 5 % Very deep FM
                SounderMode = 5;
        end
        
    case 'EM302'
        switch SonarMode_2
            case 1 % Very Shallow
                SounderMode = 1;
            case 2 % Shallow
                SounderMode = 2;
            case 3 % Medium
                SounderMode = 3;
            case {4; 5} % Deep FM and Deep CW
                SounderMode = 4;
            case {6; 7} % Very deep FM and Very deep CW
                SounderMode = 5;
            case {8; 9} % Extra deep % TODO : pb � r�soudre Extra Deep2
                SounderMode = 6;
            otherwise
                messageModeInconnu(Mute, Model, SounderMode);
                flag = 0;
        end
        
    case 'EM304'
        switch SonarMode_2
            case 1 % Very Shallow
                SounderMode = 1;
            case 2 % Shallow
                SounderMode = 2;
            case 3 % Medium
                SounderMode = 3;
            case 4 % Deep
                SounderMode = 4;
            case {5; 6} % Deeper FM and Deeper CW
                SounderMode = 5;
            case {7; 8} % Very deep FM and Very deep CW
                SounderMode = 6;
            case {9; 10} % Extra deep % TODO : pb � r�soudre Extra Deep2
                SounderMode = 7;
            case {11; 12} % Extreme deep % TODO : pb � r�soudre Extra Deep2
                SounderMode = 8;
            otherwise
                messageModeInconnu(Mute, Model, SounderMode);
                flag = 0;
        end
        
    case {'EM710'; 'EM712'} % TODO
        switch SonarMode_2
            case 1 % Very shallow
                SounderMode = 1;
            case 2 % Shallow
                SounderMode = 2;
            case 3 % Medium
                SounderMode = 3;
            case {4; 5} % Deep
                SounderMode = 4;
            case {6; 7} % Very deep
                SounderMode = 5;
            case {8; 9} % Extra deep
                SounderMode = 6;
            otherwise
                messageModeInconnu(Mute, Model, SounderMode);
                flag = 0;
        end
        
    case 'EM850' % TODO
        switch SonarMode_2
            case 1 % Very shallow
                SounderMode = 1;
            case 2 % Shallow
                SounderMode = 2;
            case 3 % Medium
                SounderMode = 3;
            otherwise
                messageModeInconnu(Mute, Model, SounderMode);
                flag = 0;
        end
        
    otherwise
        if ~Mute
            str1 = sprintf('Sondeur "%s" pas encore branch� dans cl_simrad_all/getSounderParamsForcl_sounderXML', Model);
            str2 = sprintf('Sounder "%s" not yet plugged in cl_simrad_all/getSounderParamsForcl_sounderXML', Model);
            my_warndlg(Lang(str1,str2), 0, 'Tag', 'PasEncorePluggeg', 'TimeDelay', 60);
        end
        flag = 1;
end


function messageModeInconnu(Mute, Model, SounderMode)

if ~Mute
    str1 = sprintf('Sondeur "%s" ne connait pas le mode %d (dans cl_simrad_all/inverse_getSounderParamsForcl_sounderXML)', Model, SounderMode);
    str2 = sprintf('Sounder "%s" does not know mode=%d (in cl_simrad_all/inverse_getSounderParamsForcl_sounderXML)', Model, SounderMode);
    my_warndlg(Lang(str1,str2), 1);
end
