function flag = isVariableConstante(Variable)

if isempty(Variable)
    flag = -1;
    return
end
if isfield(Variable, 'FileName') && ~isempty(Variable.FileName)
    flag = 0;
elseif isfield(Variable, 'Constant') && (~isempty(Variable.Constant) || strcmpi(Variable.Storage, 'char'))
    flag = 1;
else
    str = sprintf('Message for JMA : isVariableConstante failed on %s.', Variable.Name);
    my_warndlg(str, 0);
    flag = -1;
end
