% function [EBCReelHeader,BinReelHeader,BinTraceHeader,TraceData]=LitSegY(fname,1stTr,LastTr,NbEch);
% Fonction qui lit un fichier de données SEGY, et renvoie les différents morceaux.
% - EBCDIC Reel Header: 3200 bytes (40 lignes de 80 caractères)
%       (infos générales sur l'acquisition)
%       pour données sbp120, le header est en format texte (et pas en EBCDIC)
% - Binary Reel Header: 400 bytes (contient des entiers codés sur 2 ou 4 bytes)
%       (paramètres d'acquisition, format..)
% - Trace Header (binaire): 240 bytes, mots codés sur 2 ou 4 bytes (pour chaque trace)
%       (paramètres acquisition + paramètres trace (n° SP,CDP,coord...))
% pour le moment : data seulement au format IBM, float ou int16

function [EBCReelHeader, BinReelHeader, BinTraceHeader, TraceData] = litsegy(fname, FirstTr, LastTr, NbEch)

% EBCDIC_To_ASCII = [0  ,1  ,2  ,3  ,4  ,5  ,6  ,7  ,8  ,9  ,10 ,11 ,12 ,13 ,14 ,15 ,...
%    					 16 ,17 ,18 ,19 ,20 ,21 ,22 ,23 ,24 ,25 ,26 ,27 ,28 ,29 ,30 ,31 ,...
%                    32 ,33 ,34 ,35 ,36 ,37 ,38 ,39 ,40 ,41 ,42 ,43 ,44 ,45 ,46 ,47 ,...
%                    46 ,46 ,50 ,51 ,52 ,53 ,54 ,55 ,56 ,57 ,58 ,59 ,60 ,61 ,46 ,63 ,...
%                    32 ,46 ,46 ,46 ,46 ,46 ,46 ,46 ,46 ,46 ,46 ,46 ,60 ,40 ,43 ,124,...
%                    38 ,46 ,46 ,46 ,46 ,46 ,46 ,46 ,46 ,46 ,33 ,36 ,42 ,41 ,59 ,94 ,...
%                    45 ,47 ,46 ,46 ,46 ,46 ,46 ,46 ,46 ,46 ,124,44 ,37 ,95 ,62 ,63 ,...
%                    46 ,46 ,46 ,46 ,46 ,46 ,46 ,46 ,46 ,46 ,58 ,35 ,64 ,39 ,61 ,34 ,...
%                    46 ,97 ,98 ,99 ,100,101,102,103,104,105,46 ,46 ,46 ,46 ,46 ,46 ,...
%                    46 ,106,107,108,109,110,111,112,113,114,46 ,46 ,46 ,46 ,46 ,46 ,...
%                    46 ,126,115,116,117,118,119,120,121,122,46 ,46 ,46 ,91 ,46 ,46 ,...
%                    46 ,46 ,46 ,46 ,46 ,46 ,46 ,46 ,46 ,46 ,46 ,46 ,46 ,93 ,46 ,46 ,...
%                    123,65 ,66 ,67 ,68 ,69 ,70 ,71 ,72 ,73 ,46 ,46 ,46 ,46 ,46 ,46 ,...
%                    125,74 ,75 ,76 ,77 ,78 ,79 ,80 ,81 ,82 ,46 ,46 ,46 ,46 ,46 ,46 ,...
%                    92 ,46 ,83 ,84 ,85 ,86 ,87 ,88 ,89 ,90 ,46 ,46 ,46 ,46 ,46 ,46 ,...
%                    48 ,49 ,50 ,51 ,52 ,53 ,54 ,55 ,56 ,57 ,46 ,46 ,46 ,46 ,46 ,46];


fid = fopen(fname,'rb');
if fid == -1
    disp(['Le fichier ' fname ' n''existe pas'])
    EBCReelHeader   = [];
    BinReelHeader   = [];
    BinTraceHeader  = [];
    TraceData       = [];
    return
end

%% On commence par lire le header EBCDIC du fichier.

EBCReelHeader = fread(fid, 3200, 'uint8');              %si header au format texte
% EBCReelHeader=EBCDIC_To_ASCII(EBCReelHeader+1);    %si header EBCDIC
EBCReelHeader = reshape(EBCReelHeader,80,40);
EBCReelHeader(end+1,:) = 10;
EBCReelHeader = char(EBCReelHeader(:)');


%% Maintenant on se lit le header binaire du fichier.

DebutBinReelHeader = ftell(fid);

%D'abord on fait un test pour savoir si c'est big-endian ou little-endian.
fseek(fid, 24, 'cof');			%Les octets 24 et 25 du header doivent valoir 1,2,3,4 (format).
test = fread(fid, 2, 'uint8');	    %si l'octet 24 vaut 0, c'est donc que le fichier est big-endian.
if (test(1) == 0) && (test(2) == 0)
    disp('Format du fichier inconnu ou non indiqué');
    EBCReelHeader   = [];
    BinReelHeader   = [];
    BinTraceHeader  = [];
    TraceData       = [];
    return
elseif test(1) == 0
    Endianness = 0; %big-endian
else
    Endianness = 1; %little-endian
end
fclose(fid);


% on rouvre le fichier pour le lire dans le bon sens (big / little endian)
if Endianness == 0 % Big-Endian
    fid = fopen(fname, 'rb', 'ieee-be');
else
    fid = fopen(fname, 'rb', 'ieee-le');
end

if fid == -1
    disp(['Problème lors de la réouverture du fichier ' fname]);
    EBCReelHeader   = [];
    BinReelHeader   = [];
    BinTraceHeader  = [];
    TraceData       = [];
    return
end

fseek(fid, DebutBinReelHeader, 'bof');
JobId		 			= fread(fid, 1, 'int32');
LineNumber				= fread(fid, 1, 'int32');
ReelNumber				= fread(fid, 1, 'int32');
DataTracesPerRecord	    = fread(fid, 1, 'int16');  %pas toujours bien à jour -> on le recalcule plus bas
AuxTracesPerRecord	    = fread(fid, 1, 'int16');
ReelSampleInterval	    = fread(fid, 1, 'int16');
%ReelSampleInterval	    = 62.5;
FieldSampleInterval	    = fread(fid, 1, 'int16');
ReelSamplesPerTrace	    = fread(fid, 1, 'int16');

%% si on ne lit pas toute le trace

if NbEch > ReelSamplesPerTrace
    NbEch = ReelSamplesPerTrace;
end
disp(sprintf('Nb total d''échantillons = %d',ReelSamplesPerTrace)); %#ok<DSPS>
disp(sprintf('Nb d''échantillons lus   = %d',NbEch)); %#ok<DSPS>
NbEchTotal  = ReelSamplesPerTrace;
ReelSamplesPerTrace = NbEch;

%%

FieldSamplesPerTrace	= fread(fid, 1, 'int16');
DataSampleFormat		= fread(fid, 1, 'int16'); 	%1 = 32-bit IBM floating point
%2 = 32-bit fixed-point (integer)
%3 = 16-bit fixed-point (integer)
%4 = 32-bit fixed-point with gain code (integer)
%5 = 32-bit floating point (IEEE)
%6 = not currently used
%7 = not currently used
%8 = 8-bit fixed-point (integer)
Tailles = [4 4 2 4 4 0 0 1];

%% On calcule le nombre de traces par record

curseur = ftell(fid);
if DataTracesPerRecord < 12
    fseek(fid, 0, 'eof');
    %DataTracesPerRecord=(ftell(fid)-3200-400)/(240+Tailles(DataSampleFormat)*ReelSamplesPerTrace);
    DataTracesPerRecord = (ftell(fid)-3200-400) / (240+Tailles(DataSampleFormat)*NbEchTotal);
    fseek(fid, curseur, 'bof');
end

%% Si on ne veut pas lire toutes les traces

if FirstTr > DataTracesPerRecord
    disp(sprintf('attention : nb total de traces = %d',DataTracesPerRecord)) %#ok<DSPS>
    EBCReelHeader   = [];
    BinReelHeader   = [];
    BinTraceHeader  = [];
    TraceData       = [];
    return
end
if LastTr > DataTracesPerRecord
    LastTr = DataTracesPerRecord;
end
disp(sprintf('nb total traces    = %d',DataTracesPerRecord)) %#ok<DSPS>
disp(sprintf('1ère trace lue     = %d',FirstTr)) %#ok<DSPS>
disp(sprintf('dernière trace lue = %d',LastTr)) %#ok<DSPS>

DataTracesPerRecord = LastTr - FirstTr + 1;

%%

CDPFold					= fread(fid, 1, 'int16');
TraceSorting			= fread(fid, 1, 'int16');
VerticalSumming		    = fread(fid, 1, 'int16');
StartSweepFrq			= fread(fid, 1, 'int16');
%StartSweepFrq			= 1800;
EndSweepFrq				= fread(fid, 1, 'int16');
%EndSweepFrq				= 5200;
SweepLength				= fread(fid, 1, 'int16');
%SweepLength				= 20;
SweepType				= fread(fid, 1, 'int16');  % 1 = linear, 2 = parabolic, 3 = exponential, 4 = other
SweepChannelIndex		= fread(fid, 1, 'int16');
SweepStartTaper		    = fread(fid, 1, 'int16');
SweepEndTaper			= fread(fid, 1, 'int16');
TaperType				= fread(fid, 1, 'int16');  % 1 = linear, 2 = cos square, 3 = other
CorrelatedTraces		= fread(fid, 1, 'int16');  % 1 = no, 2 = oui
BinaryGainRecovered	    = fread(fid, 1, 'int16');  % 1 = oui, 2 = non
AmplitudeRecovery		= fread(fid, 1, 'int16');
MeasurementSystem		= fread(fid, 1, 'int16');
SignalPolarity			= fread(fid, 1, 'int16');
VibratoryPolarity		= fread(fid, 1, 'int16');
%On stocke tous les paramètres dans une structure
BinReelHeader=struct('JobId', JobId,...
    'LineNumber',           LineNumber,...
    'ReelNumber',           ReelNumber,...
    'DataTracesPerRecord',  DataTracesPerRecord,...
    'AuxTracesPerRecord',   AuxTracesPerRecord,...
    'ReelSampleInterval',   ReelSampleInterval,...
    'FieldSampleInterval',  FieldSampleInterval,...
    'ReelSamplesPerTrace',  ReelSamplesPerTrace,...
    'FieldSamplesPerTrace', FieldSamplesPerTrace,...
    'DataSampleFormat',     DataSampleFormat,...
    'CDPFold',              CDPFold,...
    'TraceSorting',         TraceSorting,...
    'VerticalSumming',      VerticalSumming,...
    'StartSweepFrq',        StartSweepFrq,...
    'EndSweepFrq',          EndSweepFrq,...
    'SweepLength',          SweepLength,...
    'SweepType',            SweepType,...
    'SweepChannelIndex',    SweepChannelIndex,...
    'SweepStartTaper',      SweepStartTaper,...
    'SweepEndTaper',        SweepEndTaper,...
    'TaperType',            TaperType,...
    'CorrelatedTraces',     CorrelatedTraces,...
    'BinaryGainRecovered',  BinaryGainRecovered,...
    'AmplitudeRecovery',    AmplitudeRecovery,...
    'MeasurementSystem',    MeasurementSystem,...
    'SignalPolarity',       SignalPolarity,...
    'VibratoryPolarity',    VibratoryPolarity);

%--------------------------------------------------------------------------

disp(sprintf('pas d''échantillonnage : ReelSampleInterval = %d microsec.',ReelSampleInterval)); %#ok<DSPS>
disp(sprintf('chirp émission : StartSweepFrq = %d microsec.',StartSweepFrq)); %#ok<DSPS>
disp(sprintf('chirp émission : EndSweepFrq   = %d microsec.',EndSweepFrq)); %#ok<DSPS>
disp(sprintf('chirp émission : SweepLength   = %d microsec.',SweepLength)); %#ok<DSPS>

%% On lit les traces  (header de trace et data)

fseek(fid, DebutBinReelHeader+400, 'bof');

DebutDonnees = ftell(fid);
TraceData = NaN(BinReelHeader.ReelSamplesPerTrace, BinReelHeader.DataTracesPerRecord, 'single');

nbTraces = BinReelHeader.DataTracesPerRecord;
for u=FirstTr:(FirstTr+BinReelHeader.DataTracesPerRecord-1)
    if (u/100 - round(u/100)) == 0
        disp(sprintf('n° trace : %d',u)); %#ok<DSPS>
    end
    
    fseek(fid, DebutDonnees+(u-1)*(240+Tailles(BinReelHeader.DataSampleFormat)*NbEchTotal), 'bof');

    %% On lit les headers de trace.
    
    TraceSequenceLine			 = fread(fid, 1, 'int32');
    
    if isempty(TraceSequenceLine)
%         BinTraceHeader = BinTraceHeader(1:k-1);
%         TraceData      = TraceData(:,1:k-1);
        break
    end
    
    TraceSequenceReel			 = fread(fid, 1, 'int32');
    FieldRecordNumber			 = fread(fid, 1, 'int32');
    TraceSequenceRecord			 = fread(fid, 1, 'int32');
    ShotPointNumber				 = fread(fid, 1, 'int32');
    CDPNumber					 = fread(fid, 1, 'int32');
    TraceSequenceCDP			 = fread(fid, 1, 'int32');
    TraceIdentificationCode		 = fread(fid, 1, 'int16');
    NumberVerticalSummedTr		 = fread(fid, 1, 'int16');
    NumberHorizontalStackedTr	 = fread(fid, 1, 'int16');
    DataUse						 = fread(fid, 1, 'int16');
    DistanceSPtoReceiver		 = fread(fid, 1, 'int32');
    ReceiverElevation			 = fread(fid, 1, 'int32');
    SourceSurfaceElevation		 = fread(fid, 1, 'int32');
    SourceDepth					 = fread(fid, 1, 'int32');
    DatumElevationAtReceiver	 = fread(fid, 1, 'int32');
    DatumElevationAtSource		 = fread(fid, 1, 'int32');
    WaterDepthAtSource			 = fread(fid, 1, 'int32');
    WaterDepthAtReceiver		 = fread(fid, 1, 'int32');
    ScalarForElevationDepth		 = fread(fid, 1, 'int16');	% + = multiplier, - = divisor
    ScalarForCoordiantes		 = fread(fid, 1, 'int16');	% + = multiplier, - = divisor
    XSourceCoordinate			 = fread(fid, 1, 'int32');
    YSourceCoordinate			 = fread(fid, 1, 'int32');
    XReceiverCoordinate			 = fread(fid, 1, 'int32');
    YReceiverCoordinate			 = fread(fid, 1, 'int32');
    CoordinateUnits				 = fread(fid, 1, 'int16');	%1=length in meters or feet, 2=arc seconds
    WeatheringVelocity			 = fread(fid, 1, 'int16');
    SubWeatheringVelocity		 = fread(fid, 1, 'int16');
    UpholeTimeAtSource			 = fread(fid, 1, 'int16');
    UpholeTimeAtReceiver		 = fread(fid, 1, 'int16');
    SourceStaticCorrection		 = fread(fid, 1, 'int16');
    ReceiverStaticCorrection	 = fread(fid, 1, 'int16');
    TotalStaticCorrection		 = fread(fid, 1, 'int16');
    LagTimeHeaderBreak			 = fread(fid, 1, 'int16');
    LagTimeBreakShot			 = fread(fid, 1, 'int16');
    LagTimeShotRecording		 = fread(fid, 1, 'int16');
    StartMuteTime				 = fread(fid, 1, 'int16');
    EndMuteTime					 = fread(fid, 1, 'int16');
    NumberOfSamples				 = fread(fid, 1, 'int16'); %#ok<NASGU>
    SampleInterval				 = fread(fid, 1, 'int16'); %#ok<NASGU>
    SampleInterval               = BinReelHeader.ReelSampleInterval;
    InstrumentGainCode			 = fread(fid, 1, 'int16');
    InstrumentGainConstant		 = fread(fid, 1, 'int16');
    InstrumentGainDB			 = fread(fid, 1, 'int16');
    Correlated					 = fread(fid, 1, 'int16');	%1=no, 2=yes
    StartSweepFrq				 = fread(fid, 1, 'int16'); %#ok<NASGU>
    StartSweepFrq                = BinReelHeader.StartSweepFrq;
    EndSweepFrq					 = fread(fid, 1, 'int16'); %#ok<NASGU>
    EndSweepFrq                  = BinReelHeader.EndSweepFrq;
    SweepLength					 = fread(fid, 1, 'int16'); %#ok<NASGU>
    SweepLength                  = BinReelHeader.SweepLength;
    SweepType					 = fread(fid, 1, 'int16');
    SweepStartTaper				 = fread(fid, 1, 'int16');
    SweepEndTaper				 = fread(fid, 1, 'int16');
    TaperType					 = fread(fid, 1, 'int16');
    AliasFilterFrq				 = fread(fid, 1, 'int16');
    AliasFilterSlope			 = fread(fid, 1, 'int16');
    NotchFilterFrq				 = fread(fid, 1, 'int16');
    NotchFilterSlope			 = fread(fid, 1, 'int16');
    LowCutFrq					 = fread(fid, 1, 'int16');
    HighCutFrq					 = fread(fid, 1, 'int16');
    LowCutSlope					 = fread(fid, 1, 'int16');
    HighCutSlope				 = fread(fid, 1, 'int16');
    YearDataRecorded			 = fread(fid, 1, 'int16');
    Day							 = fread(fid, 1, 'int16');
    Hour						 = fread(fid, 1, 'int16');
    Minute						 = fread(fid, 1, 'int16');
    Second						 = fread(fid, 1, 'int16');
    TimeBasis					 = fread(fid, 1, 'int16');
    TraceWeightingFactor		 = fread(fid, 1, 'int16');
    GeophoneNumberRollSwitch	 = fread(fid, 1, 'int16');
    GeophoneNumberFirstTrace	 = fread(fid, 1, 'int16');
    GeophoneNumberLastTrace		 = fread(fid, 1, 'int16');
    GapSize						 = fread(fid, 1, 'int16');
    TaperOvertravel				 = fread(fid, 1, 'int16');
    XcdpCoordinate               = fread(fid, 1, 'int32');
    YcdpCoordinate               = fread(fid, 1, 'int32');
    InLineNb                     = fread(fid, 1, 'int32');
    CdpNb                        = fread(fid, 1, 'int32');
    ShotPointNb                  = fread(fid, 1, 'int32');
    ScalarToShotPonitNb          = fread(fid, 1, 'int16');
    TraceValueMesUnit            = fread(fid, 1, 'int16');
    TransductionConstantMantisse = fread(fid,1,'int32'); %peut-etre à revoir : codé sur 6 bytes ?
    TransductionConstantExponent = fread(fid, 1, 'int16'); %peut-etre à revoir : codé sur 6 bytes ?
    TransductionUnit             = fread(fid, 1, 'int16');
    TraceIdentifier              = fread(fid, 1, 'int16');
    ScalarToTime                 = fread(fid, 1, 'int16');
    SourceType                   = fread(fid, 1, 'int16');
    SourceEnergyDirectionInteger = fread(fid,1,'int32'); %peut-etre à revoir : codé sur 6 bytes ?
    SourceEnergyDirectionDecimal = fread(fid, 1, 'int16'); %peut-etre à revoir : codé sur 6 bytes ?
    SourceMeasurementMantisse    = fread(fid, 1, 'int32'); %peut-etre à revoir : codé sur 6 bytes ?
    SourceMeasurementExponent    = fread(fid, 1, 'int16'); %peut-etre à revoir : codé sur 6 bytes ?
    SourceMeasurementUnit        = fread(fid, 1, 'int16');
    
    
    %% On stocke tous les paramètres dans une structure
    
    k = u-FirstTr+1;
    if k == 2 % Pour éviter de réallouer de la mémoire à chaque itération
        BinTraceHeader(nbTraces) = BinTraceHeader; %#ok<AGROW>
    end
    BinTraceHeader(k) = struct('DataTracesPerRecord', DataTracesPerRecord,...
        'ReelSampleInterval',           ReelSampleInterval,...
        'TraceSequenceLine',            TraceSequenceLine,...
        'TraceSequenceReel',            TraceSequenceReel,...
        'FieldRecordNumber',            FieldRecordNumber,...
        'TraceSequenceRecord',          TraceSequenceRecord,...
        'ShotPointNumber',              ShotPointNumber,...
        'CDPNumber',                    CDPNumber,...
        'TraceSequenceCDP',             TraceSequenceCDP,...
        'TraceIdentificationCode',      TraceIdentificationCode,...
        'NumberVerticalSummedTr',       NumberVerticalSummedTr,...
        'NumberHorizontalStackedTr',    NumberHorizontalStackedTr,...
        'DataUse',                      DataUse,...
        'DistanceSPtoReceiver',         DistanceSPtoReceiver,...
        'ReceiverElevation',            ReceiverElevation,...
        'SourceSurfaceElevation',       SourceSurfaceElevation,...
        'SourceDepth',                  SourceDepth,...
        'DatumElevationAtReceiver',     DatumElevationAtReceiver,...
        'DatumElevationAtSource',       DatumElevationAtSource,...
        'WaterDepthAtSource',           WaterDepthAtSource,...
        'WaterDepthAtReceiver',         WaterDepthAtReceiver,...
        'ScalarForElevationDepth',      ScalarForElevationDepth,...
        'ScalarForCoordiantes',         ScalarForCoordiantes,...
        'XSourceCoordinate',            XSourceCoordinate,...
        'YSourceCoordinate',            YSourceCoordinate,...
        'XReceiverCoordinate',          XReceiverCoordinate,...
        'YReceiverCoordinate',          YReceiverCoordinate,...
        'CoordinateUnits',              CoordinateUnits,...
        'WeatheringVelocity',           WeatheringVelocity,...
        'SubWeatheringVelocity',        SubWeatheringVelocity,...
        'UpholeTimeAtSource',           UpholeTimeAtSource,...
        'UpholeTimeAtReceiver',         UpholeTimeAtReceiver,...
        'SourceStaticCorrection',       SourceStaticCorrection,...
        'ReceiverStaticCorrection',     ReceiverStaticCorrection,...
        'TotalStaticCorrection',        TotalStaticCorrection,...
        'LagTimeHeaderBreak',           LagTimeHeaderBreak,...
        'LagTimeBreakShot',             LagTimeBreakShot,...
        'LagTimeShotRecording',         LagTimeShotRecording,...
        'StartMuteTime',                StartMuteTime,...
        'EndMuteTime',                  EndMuteTime,...
        'NumberOfSamples',              ReelSamplesPerTrace,...
        'SampleInterval',               SampleInterval,...
        'InstrumentGainCode',           InstrumentGainCode,...
        'InstrumentGainConstant',       InstrumentGainConstant,...
        'InstrumentGainDB',             InstrumentGainDB,...
        'Correlated',                   Correlated,...
        'StartSweepFrq',                StartSweepFrq,...
        'EndSweepFrq',                  EndSweepFrq,...
        'SweepLength',                  SweepLength,...
        'SweepType',                    SweepType,...
        'SweepStartTaper',              SweepStartTaper,...
        'SweepEndTaper',                SweepEndTaper,...
        'TaperType',                    TaperType,...
        'AliasFilterFrq',               AliasFilterFrq,...
        'AliasFilterSlope',             AliasFilterSlope,...
        'NotchFilterFrq',               NotchFilterFrq,...
        'NotchFilterSlope',             NotchFilterSlope,...
        'LowCutFrq',                    LowCutFrq,...
        'HighCutFrq',                   HighCutFrq,...
        'LowCutSlope',                  LowCutSlope,...
        'HighCutSlope',                 HighCutSlope,...
        'YearDataRecorded',             YearDataRecorded,...
        'Day',                          Day,...
        'Hour',                         Hour,...
        'Minute',                       Minute,...
        'Second',                       Second,...
        'TimeBasis',                    TimeBasis,...
        'TraceWeightingFactor',         TraceWeightingFactor,...
        'GeophoneNumberRollSwitch',     GeophoneNumberRollSwitch,...
        'GeophoneNumberFirstTrace',     GeophoneNumberFirstTrace,...
        'GeophoneNumberLastTrace',      GeophoneNumberLastTrace,...
        'GapSize',                      GapSize,...
        'TaperOvertravel',              TaperOvertravel,...
        'XcdpCoordinate',               XcdpCoordinate,...
        'YcdpCoordinate',               YcdpCoordinate,...
        'InLineNb',                     InLineNb,...
        'CdpNb',                        CdpNb,...
        'ShotPointNb',                  ShotPointNb,...
        'ScalarToShotPonitNb',          ScalarToShotPonitNb,...
        'TraceValueMesUnit',            TraceValueMesUnit,...
        'TransductionConstantMantisse', TransductionConstantMantisse,...
        'TransductionConstantExponent', TransductionConstantExponent,...
        'TransductionUnit',             TransductionUnit,...
        'TraceIdentifier',              TraceIdentifier,...
        'ScalarToTime',                 ScalarToTime,...
        'SourceType',                   SourceType,...
        'SourceEnergyDirectionInteger', SourceEnergyDirectionInteger,...
        'SourceEnergyDirectionDecimal', SourceEnergyDirectionDecimal,...
        'SourceMeasurementMantisse',    SourceMeasurementMantisse,...
        'SourceMeasurementExponent',    SourceMeasurementExponent,...
        'SourceMeasurementUnit',        SourceMeasurementUnit); %#ok<AGROW>
    
    %% on lit le data
    
    fseek(fid, DebutDonnees+(u-1)*(240+Tailles(BinReelHeader.DataSampleFormat)*NbEchTotal)+240, 'bof');
    
    if BinReelHeader.DataSampleFormat == 1	%si les traces sont au format IBM: OK
        octets = fread(fid, Tailles(BinReelHeader.DataSampleFormat)*BinReelHeader.ReelSamplesPerTrace, 'uint8');
        TraceData(:,k) = IBM2double(octets,Endianness);
        if NbEchTotal > BinReelHeader.ReelSamplesPerTrace
            fseek(fid, Tailles(BinReelHeader.DataSampleFormat)*(NbEchTotal-BinReelHeader.ReelSamplesPerTrace), 'cof');
        end
        
    elseif DataSampleFormat == 5		   %si les traces sont au format float: OK
        pppp = fread(fid, BinReelHeader.ReelSamplesPerTrace, 'float32');
        if isempty(pppp)
            BinTraceHeader = BinTraceHeader(1:k-1);
            TraceData      = TraceData(:,1:k-1);
            return
        end
        TraceData(:,k) = pppp;
        if NbEchTotal > BinReelHeader.ReelSamplesPerTrace
            fseek(fid, Tailles(BinReelHeader.DataSampleFormat)*(NbEchTotal-BinReelHeader.ReelSamplesPerTrace), 'cof');
        end
        
    elseif DataSampleFormat == 3			   %si les traces sont au format int16: OK
        TraceData(:,k) = fread(fid,BinReelHeader.ReelSamplesPerTrace,'int16');
        if NbEchTotal > BinReelHeader.ReelSamplesPerTrace
            fseek(fid, Tailles(BinReelHeader.DataSampleFormat)*(NbEchTotal-BinReelHeader.ReelSamplesPerTrace), 'cof');
        end
    else
        disp(['On n''a pas encore codé le format ' num2str(BinReelHeader.DataSampleFormat)]);
        EBCReelHeader   = [];
        BinReelHeader   = [];
        BinTraceHeader  = [];
        TraceData       = [];
        return
    end
end

fclose(fid);


%% fonction qui permet de lire les traces: conversion format IBM -> float (double)

function V = IBM2double(Vin,Endianness)

Vin = double(Vin);
if Endianness == 0												%Big-Endian
    Signe = Vin(1:4:end) > 128; 												%Le bit de signe est allumé->Négatif
    Exposant = bitand(Vin(1:4:end),127);									%On récupère les 7 premiers bits du 1 octet -> C'est l'exposant
    Mantisse = 65536*Vin(2:4:end) + 256*Vin(3:4:end) + Vin(4:4:end);% La mantisse, c'est les 3 derniers octets, ORés comme il faut.
else																%Little-Endian : les octets sont dans l'autre sens.
    Signe = Vin(4:4:end) > 128; 												%Le bit de signe est allumé->Négatif
    Exposant = bitand(Vin(4:4:end),127);									%On récupère les 7 premiers bits du 1 octet -> C'est l'exposant
    Mantisse = 65536*Vin(3:4:end) + 256*Vin(2:4:end) + Vin(1:4:end);% La mantisse, c'est les 3 derniers octets, ORés comme il faut.
end

V = 16.^(Exposant-70) .* Mantisse;
V(Signe) = -V(Signe);


