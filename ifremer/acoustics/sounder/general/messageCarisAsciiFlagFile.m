function messageCarisAsciiFlagFile

str = sprintf('// Lat (DD) Long (DD) Year Day Time Profile Beam Status\n 48.3040355 -004.4034113 2013 283 09:33:04.151 1 1 R\n48.3040355 -004.4034113 2013 283 09:33:04.151 1 2 A\n48.3040355 -004.4034113 2013 283 09:33:04.151 1 3 R');
str1 = sprintf('Le format ASCII Caris ressemble � cela.\nSeuls les champs "Profile", "Beam" et "Status" sont n�cessaires.\nExemple :\n%s', str);
str2 = sprintf('Caris ASCII format looks like this.\nOnly  "Profile", "Beam" and "Status" are mandatory.\n Example :\n%s', str);
my_warndlg(Lang(str1,str2), 0);
