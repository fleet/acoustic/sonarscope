% Preformated message in case of a file access failure
%
% Syntax
%   messageErreurFichier(filename, ...)
%
% Input Arguments
%   filename : Name of the file
%
% Output Arguments
%   mes : Message
%
% Name-Value Pair Arguments
%   Message : Text that you want to add to the message
%
% Name-only Arguments
%   ReadFailure  : The error concerns a read access
%   WriteFailure : The error concerns a write access
%
% Output Arguments
%
% Examples
%   messageErreurFichier('MyFile.xyz', 'ReadFailure')
%   messageErreurFichier('MyFile.xyz', 'WriteFailure')
%   messageErreurFichier('MyFile.xyz', 'ReadFailure', 'Message', 'Where did I put it ?')
%
% See also my_warndlg Authors  %;
% Authors  : JMA
%-------------------------------------------------------------------------------

function mes = messageErreurFichier(nomFic, varargin)

[varargin, Message] = getPropertyValue(varargin, 'Message', '');
[varargin, Modal]   = getPropertyValue(varargin, 'Modal',    0);

[varargin, flagReadFailure]  = getFlag(varargin, 'ReadFailure');
[varargin, flagWriteFailure] = getFlag(varargin, 'WriteFailure'); %#ok<ASGLU>

if isempty(Message)
    if flagReadFailure
        str1 = sprintf('Erreur de lecture de "%s".', nomFic);
        str2 = sprintf('Read failure for "%s".', nomFic);
    elseif flagWriteFailure
        str1 = sprintf('Erreur d''�criture de  : %s.', nomFic);
        str2 = sprintf('Write failure for "%s".', nomFic);
    else
        str1 = sprintf('Erreur de lecture ou d''�criture de "%s".', nomFic);
        str2 = sprintf('Read or write failure for "%s".', nomFic);
    end
else
    if flagReadFailure
        str1 = sprintf('Erreur de lecture de "%s".\n%s', nomFic, Message);
        str2 = sprintf('Read failure for "%s".\n%s', nomFic, Message);
    elseif flagWriteFailure
        str1 = sprintf('Erreur d''�criture de  : %s.\n%s', nomFic, Message);
        str2 = sprintf('Write failure for %s.\n%s', nomFic, Message);
    else
        str1 = sprintf('Erreur de lecture ou d''�criture de "%s".\n%s', nomFic, Message);
        str2 = sprintf('Read or write failure for "%s".\n%s', nomFic, Message);
    end
end
mes = Lang(str1,str2);
my_warndlg(mes, Modal, 'Tag', 'ErreurEcritureFichierSonarScope');
