function message_LayerNotAvailable(nomLayer, typeWarning, varargin)

[varargin, FileName] = getPropertyValue(varargin, 'FileName', []); %#ok<ASGLU> 

if isempty(FileName)
    str1 = sprintf('Le layer "%s" n''est pas disponible.', nomLayer);
    str2 = sprintf('The layer "%s" is not available.', nomLayer);
else
    str1 = sprintf('Le layer "%s" n''est pas disponible dans le cache de %s.', nomLayer, FileName);
    str2 = sprintf('The layer "%s" is not available in the SSc cache of %s', nomLayer, FileName);
end
my_warndlg(Lang(str1,str2), typeWarning);
