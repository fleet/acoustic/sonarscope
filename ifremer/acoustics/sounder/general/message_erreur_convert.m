function message_erreur_convert(nomFicTmp, nomFic, nomDirRacine)

X = dir(nomFicTmp);
if (now - X(1).datenum) > (10 / (24*60))
    str1 = sprintf('\nLe r�pertoire "%s" existe sur le disque.\nSSc tente de le supprimer car il semble ancien', ...
        nomFicTmp);
    str2 = sprintf('\n"%s" directory exists on your disk.\nSSc trys to delete it because it seems to be old.', ...
        nomFicTmp);
    my_warndlg(Lang(str1,str2), 0, 'Tag', 'FichierNonLisible');
    try
        rmdir(nomFicTmp, 's')
    catch ME
        my_warndlg(ME.message, 0, 'Tag', 'FichierNonLisible');
    end
else
    str1 = sprintf('\nLe r�pertoire "%s" existe sur le disque, Il est possible qu''un autre processus SonarScope soit en train de traiter le fichier "%s". C''est possible �galement que je fichier n''ait pas pu �tre converti.\nVous pouvez essayer de supprimer le r�pertoire ainsi que "%s".', ...
        nomFicTmp, nomFic, nomDirRacine);
    str2 = sprintf('\n"%s" directory exists on your disk, it is possible that another SonarScope process is doing the conversion of file "%s". It is also possible that the file could not be converted.\nYou can delete this directory by hand (delete also directory "%s").', ...
        nomFicTmp, nomFic, nomDirRacine);
    my_warndlg(Lang(str1,str2), 0, 'Tag', 'FichierNonLisible');
end

