% Identification des navires par un numero.
%
% Syntax
%   listeNavires = num2navire
%   nomNavire = num2navire( n )
%
% Input Arguments 
%   [] : Rend la liste des navires
%   n  : Numero du navire
%
% Output Arguments 
%   listeNavires : si pas de parametres en entree
%   nomNavire    : Nom du navire si un numero est donne
%
% Examples
%   listeNavires = num2navire
%   nomNavire = num2navire(2)
%
% See also Authors
% Authors : JMA + XL
% VERSION  : $Id: num2navire.m,v 1.3 2002/06/20 11:54:41 augustin Exp $
%--------------------------------------------------------------------------------

% ----------------------------------------------------------------------------
% HISTORIQUE DEVELOPPEMENT
%   02/11/00 - JMA - creation
%   23/03/01 - JMA - Mise en ref.
% ----------------------------------------------------------------------------

function sortie = num2navire(n)

listeNavires = {'Atalante'; 'Suroit'; 'Thalia'; 'Europe'; 'Other'};
switch nargin
case 0
    sortie = listeNavires;
case 1
    sortie = listeNavires{n};
end

