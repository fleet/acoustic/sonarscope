function [flag, GateDepth] = parameters_gateDepthBDA

GateDepth.Flag     = [];
GateDepth.DepthMax = [];
GateDepth.DepthMin = [];

str1 = 'Fen�tre d''observation en profondeur (utile pour l''algorithme de d�tection du fond).';
str2 = 'Gate in depth (useful for bottom detection algorithm).';
str3 = {'Aucune'; 'Celle d�finie dans les datagrammes si elle existe (sinon aucune)'; 'Personelle'};
str4 = {'None'; 'The one defined in the datagrams if it exists (none otherwise)'; 'Your own gate'};
[rep, flag] = my_listdlg(Lang(str1,str2), Lang(str3,str4), 'InitialValue', 2, 'SelectionMode', 'Single');
if ~flag
    return
end
GateDepth.Flag = rep;
switch GateDepth.Flag
    case 1 % Aucune
    case 2 % Celle d�finie dans les datagrammes si elle existe (sinon aucune)
    case 3 % Personelle
        str1 = 'TODO';
        str2 = 'Please give min and max Depth values if you can do it, keep initial values if not.';
        p(1) = ClParametre('Name', Lang('Profondeur Min','Min Depth'), ...
            'Unit', 'm',  'MinValue', 1, 'MaxValue', 12000, 'Value', 1);
        p(2) = ClParametre('Name', Lang('Profondeur Max','Max Depth'), ...
            'Unit', 'm',  'MinValue', 10, 'MaxValue', 12000, 'Value', 12000);
        a = StyledParametreDialog('params', p, 'Title', Lang(str1,str2));
        % a.Help = 'http://flotte.ifremer.fr/fleet/Presentation-of-the-fleet/On-board-software/SonarScope';
        a.sizes = [0 -2 -1 -2 -1 -1 -1 0];
        a.openDialog;
        flag = a.okPressedOut;
        if ~flag
            return
        end
        val = a.getParamsValue;
        GateDepth.DepthMax = -max(val);
        GateDepth.DepthMin = -min(val);
end
