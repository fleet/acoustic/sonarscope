function [flag, nomFicODAS, repImport] = params_AbsorpCoeff_ODAS(repImport)

%% ODAS file name

[flag, nomFicODAS] = my_uigetfile(...
    {'*.tab;*.txt', 'ODAS file (*.tab, *.txt)'; ...
     '*.tab', 'ODAS file (*.tab)'; ...
     '*.txt', 'ASCII file (*.txt)'; ...
     '*.*',   'All Files (*.*)'}, ...
    'Pick a file', repImport);
if ~flag
    return
end
repImport = fileparts(nomFicODAS);
