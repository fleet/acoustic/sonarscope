function [flag, nomFicABS, repImport] = params_AbsorpCoeff_SIS(repImport)

%% ODAS file name

[flag, nomFicABS, repImport] = uiSelectFiles('ExtensionFiles', '.abs' , 'RepDefaut', repImport, ...
    'Entete', Lang(str1,str2));
if ~flag
    return
end
