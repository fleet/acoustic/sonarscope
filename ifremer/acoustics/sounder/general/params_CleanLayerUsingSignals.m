function [flag, layerName, signalNames] = params_CleanLayerUsingSignals(filename)

flag        = 0;
layerName   = [];
signalNames = [];

%% Cr�ation de l'instance

a = cl_simrad_all('nomFic', filename);
if isempty(a)
    return
end

%% S�lection du layer de travail

str = {'Depth+Draught-Heave'; 'ReflectivityFromSnippets'; 'QualityFactor'};
[rep, flag] = my_listdlg('Select the layer you want to display', str, 'SelectionMode', 'Single');
if ~flag
    return
end
layerName = str{rep};

%% Lecture du layer

Layer = get_Layer(a, layerName, 'CartoAuto', 1);

%% S�lection des signaux

str = get_liste_vecteurs(Layer, 'VerticalOnly');
% listeSignaux() = [];
[rep, flag] = my_listdlg('Liste of signals', str);
if ~flag
    return
end
signalNames = str(rep);
