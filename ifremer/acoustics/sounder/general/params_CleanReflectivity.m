function flag = params_CleanReflectivity

str1 = sprintf('Attention, ce module ne doit �tre utilis� que pour des donn�es dont l''imagerie est de mauvaise qualit� pour cause de bullage (bulles d''air passant sous la coque) du � une mer forte de l''avant (des pings entiers ont une r�flectivit� tr�s faible).\n\nSonarScope va calculer la valeur moyenne de la reflectivit� pour chaque ping et va lancer le nettoyeur de courbe automatiquement, il ne vous restera plus qu''� �liminer les valeurs correspondant � de faibles r�flectivit�s moyennes.\n\nVoulez-vous poursuivre ?');
str2 = sprintf('Warning, this module must be used only for when the reflectivity is of very bad quality due to bubbles (air bulles under the hull). This can happen in case of very bad weather (very weak reflectivity for all beams of a ping).\n\nSonarScope is going to compute the averaged value of the reflectivity per ping then will lauch the data cleaner. You will then supress the low values corrsponding to pings in trouble with bubbles.\n\nDo we continue ?');
[rep, flag] = my_questdlg(Lang(str1,str2));
if ~flag || (rep == 2)
    return
end
