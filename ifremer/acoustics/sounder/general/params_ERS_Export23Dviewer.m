function [flag, nomDirOut, Tag, repExport] = params_ERS_Export23Dviewer(repExport)

Tag = [];

%% Select directory

str1 = 'Choisissez ou cr�ez le r�pertoire o� seront stock�s les fichiers ';
str2 = 'Select or create the directory in which the files will be created.';
[flag, nomDirOut] = my_uigetdir(repExport, Lang(str1,str2));
if ~flag
    return
end
repExport = nomDirOut;

%% Tag suppl�mentaire dans le mom des images

[flag, Tag] = question_AddTagInOutputFile;
if ~flag
    return
end
