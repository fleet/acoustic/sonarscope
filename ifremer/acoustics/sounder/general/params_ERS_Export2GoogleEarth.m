function [flag, nomDirOut, TypeOmbrage, Azimuth, VertExag, Tag, skipExistingFile, repExport] ...
     = params_ERS_Export2GoogleEarth(repExport)

TypeOmbrage = 4; % 3 : ombrage N&B avec azimut, 4=ombrage N&B avec greatestSlopes
Azimuth     = [];
VertExag    = 1;
Tag         = [];
skipExistingFile = [];

%% Select directory

str1 = 'Choisissez ou cr�ez le r�pertoire o� seront stock�s les fichiers ';
str2 = 'Select or create the directory in which the files will be created.';
[flag, nomDirOut] = my_uigetdir(repExport, Lang(str1,str2));
if ~flag
    return
end
repExport = nomDirOut;

%% Tag suppl�mentaire dans le mom des images

[flag, Tag] = question_AddTagInOutputFile;
if ~flag
    return
end

%% skipExistingMosaic

str1 = 'Ne pas refaire les fichiers kmZ existants ?';
str2 = 'Skip existing .kms files ?';
[rep, flag] = my_questdlg(Lang(str1,str2), 'Init', 2);
if ~flag
    return
end
skipExistingFile = (rep == 1);

