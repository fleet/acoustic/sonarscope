function [flag, nomFicTxt, listFileNames] = params_ImportBathyFromCaris(listFileNames)

%% Recherche du fichier Caris correspondant

nomFicLog = fullfile(my_tempdir, 'ImportFlagsAssociationFiles.txt');
fid = fopen(nomFicLog, 'w+t');
nbFicAll = length(listFileNames);
nomFicTxt = cell(nbFicAll, 1);
flagAssociation = false(1,nbFicAll);
for k=1:nbFicAll
    [nomDir, nomFic] = fileparts(listFileNames{k});
    nomFicTxtOne = fullfile(nomDir, [nomFic '.txt']);
    if ~exist(nomFicTxtOne, 'file')
        liste = listeFicOnDir(nomDir, '.txt', 'Filtre', nomFic);
        switch length(liste)
            case 0
                nomFicTxt{k} = '';
                str = sprintf('"%s" : No ".txt" file found for it', listFileNames{k});
                my_warndlg(str, 0, 'TimeDelay', 60, 'displayItEvenIfSSc', 1);
                fprintf(fid, '%s\n', str);
                continue
            case 1
                nomFicTxtOne = liste{1};
            otherwise
                str1 = sprintf('Plusieurs fichiers ".txt" correspondent au fichier "%s". S�lectionner celui qui convient dans la liste svp.', nomFic);
                str2 = sprintf('Several ".txt" files seem to be linked to "%s". Select the good one please.', nomFic);
                [rep, flag] = my_listdlg(Lang(str1,str2), liste, 'InitialValue', 1, 'SelectionMode', 'Single');
                if ~flag
                    return
                end
                nomFicTxtOne = liste{rep};
        end
    end
    fprintf('"%s" will use "%s"\n',      listFileNames{k}, nomFicTxtOne);
    fprintf(fid, '"%s" will use "%s"\n', listFileNames{k}, nomFicTxtOne);
    nomFicTxt{k} = nomFicTxtOne;
    flagAssociation(k) = true;
end
fclose(fid);

%% Affichage du fichier

displayTxtFile(nomFicLog);

%% Liste des fichiers qui ont un .txt associ�

listFileNames = listFileNames(flagAssociation);
nomFicTxt     = nomFicTxt(flagAssociation);

%% Message //Tbx

N = length(listFileNames);
flag = messageParallelTbx(N > 2);
if ~flag
    return
end

if isempty(listFileNames)
    flag = 0;
end
