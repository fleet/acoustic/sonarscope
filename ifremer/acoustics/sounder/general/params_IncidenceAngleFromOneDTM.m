function [flag, versionAlgo, TypeAlgoNormales] = params_IncidenceAngleFromOneDTM(this)

versionAlgo      = [];
TypeAlgoNormales = [];

%% Test si l'image visualis�e est bien un MNT de r�f�rence

DataType = cl_image.indDataType('Bathymetry');
flag = testSignature(this, 'DataType', DataType, 'GeometryType', cl_image.indGeometryType('LatLong'));
if ~flag
    return
end

%% Choix de l'algorithme de calsul des angles d'incidence

str{1} = 'Old one';
str{2} = 'Imen from DTM';
str{3} = 'Imen, point cloud';
% [versionAlgo, flag] = my_listdlg('Select the algorithme', str, 'InitialValue', 2, 'SelectionMode', 'Single');

[versionAlgo, flag] = my_listdlg('Select the algorithme', str, 'InitialValue', 1, 'SelectionMode', 'Single'); % Modif JMA le 04/10/2022 suite � discussion avec Ridha

if ~flag
    return
end

%% Choix de l'algorithme de calcul des normales

if versionAlgo == 3
    str{1} = 'knnsearch';
    str{2} = 'KDTreeSearcher';
    str{3} = 'delaunayTriangulation';
    [TypeAlgoNormales, flag] = my_listdlg('Select the algorithm to compute the normals', str, 'InitialValue', 1, 'SelectionMode', 'Single');
    if ~flag
        return
    end
end
