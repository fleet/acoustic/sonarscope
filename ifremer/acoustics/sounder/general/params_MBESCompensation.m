% Si Mosaique de Reflectivite, on propose de faire une compensation statistique ou une calibration
%   nomDir = 'D:\Temp\Charline'
%   [flag, TypeCalibration, CorFile, DataTypeConditions, alpha, ModelTxDiag, CourbeBS] = params_MBESCompensation(nomDir)

function [flag, ModeDependant, TypeCalibration, CorFile, alpha, ModelTxDiag, CourbeBS] = params_MBESCompensation(nomDir)

ModeDependant      = 0;
TypeCalibration    = 0;
CorFile            = [];
alpha              = [];
ModelTxDiag        = [];
CourbeBS           = [];

%% Choix du type de compensation

if nargout == 4
    str{1} = 'None';
    str{2} = 'Statistic compensation';
else
    str{1} = 'None';
    str{2} = 'Statistic compensation';
    str{3} = 'Sonar calibration';
end
str1 = 'Correction de la réflectivité ?';
str2 = 'Reflectivity Image Correction ?';
[rep, flag] = my_listdlg(Lang(str1,str2), str, 'SelectionMode', 'Single');
if ~flag
    return
end
clear str

ModeDependant = 0;
switch rep
    case 1 % Aucune compensation
        CorFile = [];
        TypeCalibration = 0;
        
    case 2 % Compensation statistique
        [flag, CorFile, ModeDependant, UseModel, PreserveMeanValueIfModel] = QuestionCompensationCurve(nomDir, 'AskQuestion', 0);
        if ~flag
            return
        end
        
    case 3 % Calibration
        CorFile = {};
        clear str
        str1 = 'Type de correction de la réflectivité ?';
        str2 = 'Type of Reflectivity Correction ?';
        str{1} = 'TVG + Directivity compensated';
        str{2} = 'TVG + Directivity & average BS compensated';
        str{3} = 'TVG only';
        [TypeCalibration, flag] = my_listdlg(Lang(str1,str2), str, 'SelectionMode', 'Single');
        if ~flag
            return
        end
                    
        [flag, alpha] = get_alpha;
        if ~flag
            return
        end

        if TypeCalibration ~= 3
            [flag, ModelTxDiag] = get_TxDiag(nomDir);
            if ~flag
                return
            end

            if TypeCalibration == 2 % Directivity & average BS compensated
                [flag, CourbeBS] = get_CourbeBS(nomDir);
                if ~flag
                    return
                end
            end
        end
end