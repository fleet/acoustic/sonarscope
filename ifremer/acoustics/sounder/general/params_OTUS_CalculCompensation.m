% Saisie des param�tres de traitement du module de cr�ation des fichiers xml
% de description des images photo OTUS du Rov Victor
%
% Syntax
%   [flag, ListeFic, nomFicCompens, repImport] = params_OTUS_CalculCompensation(repImport)
%
% Input Arguments
%   I0        : Instance de cl_image vide
%   repImport : 
%
% Output Arguments
%   flag          : 1=OK, 0=KO
%   repImport     : 
%   ListeFic      : Liste des fichiers
%   nomFicCompens : Nom du fichier de compensation
%   SeuilStd      : 
%
% See also clc_image/callback_sonar OTUS_CalculCompensation Authors
% Authors  : JMA
%--------------------------------------------------------------------------

function [flag, ListeFic, nomFicCompens, SeuilStd, repImport] = params_OTUS_CalculCompensation(repImport)

nomFicCompens = [];
SeuilStd      = [];

%% Saisie des noms de fichiers image

[flag, ListeFic, this] = uiSelectFiles('ExtensionFiles', {'.tif';'.jpg'},  'AllFilters', 1, 'RepDefaut', repImport, ...
    'Entete', 'IFREMER - SonarScope - Select .jpg or .tiff image files');
if ~flag
    return
end
repImport = fileparts(ListeFic{1});

%% Saisie du nom du fichier de compensation

filename = fullfile(this, 'Untitled.mat');
[flag, nomFicCompens] = my_uiputfile({'*.mat','MAT-files (*.mat)'}, ...
        'IFREMER - SonarScope : Save image compensation model as : ', filename);
if ~flag
    return
end

%% Saisie du seuil sur l'�cart-type

str1 = 'Seuil de rejection des images.';
str2 = 'Threshold to reject images.';
p = ClParametre('Name', Lang('Seuil sur l''�cart-type','Threshold on std'), 'MinValue', 10, 'MaxValue', 256, 'Value', 30);
a = StyledSimpleParametreDialog('params', p, 'Title', Lang(str1,str2));
% a.Help = 'http://flotte.ifremer.fr/fleet/Presentation-of-the-fleet/On-board-software/SonarScope';
a.sizes = [-2 -2 0 0];
a.openDialog;
flag = a.okPressedOut;
if ~flag
    return
end
SeuilStd = a.getParamsValue;
