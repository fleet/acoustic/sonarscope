function [flag, ListeFic, nomFicCompens, repImport] = params_OTUS_CompensMeanImage(repImport)

nomFicCompens  = [];

%% Saisie des noms de fichiers image

[flag, ListeFic, repImport] = uiSelectFiles('ExtensionFiles', {'.tif';'.jpg'},  'AllFilters', 1, 'RepDefaut', repImport, ...
    'Entete', 'IFREMER - SonarScope - Select .jpg or .tiff image files');
if ~flag
    return
end

%% Nom du fichier de compensation

str1 = 'Nom du fichier de l''image moyenne';
str2 = 'Name of the mean image';
[flag, nomFicCompens] = my_uigetfile('*.mat', Lang(str1,str2), repImport);
if ~flag
    return
end
