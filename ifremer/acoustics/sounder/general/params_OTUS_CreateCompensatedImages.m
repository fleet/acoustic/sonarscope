function [flag, ListeFicImage, OrdrePolyCompens, nomDirOut, repImport, repExport] = params_OTUS_CreateCompensatedImages(repImport, repExport)

nomDirOut        = [];
OrdrePolyCompens = [];

%% Saisie des noms de fichiers

[flag, ListeFicImage, repImport] = uiSelectFiles('ExtensionFiles', {'.tif';'.jpg'}, 'AllFilters', 1, 'RepDefaut', repImport, ...
    'Entete', 'IFREMER - SonarScope - Select .jpg or .tiff image files', 'ChaineExclue', '_Comp');
if ~flag
    return
end

%% Ordre du polynome de compensation

str1 = 'Ordre du polynome';
str2 = 'Order of the polynom';
[flag, OrdrePolyCompens] = inputOneParametre('OTUS create compensated images', Lang(str1,str2), 'Value', 3, 'MinValue', 2, 'MaxValue', 10, 'Format', '%d');
if ~flag
    return
end

%% Nom du r�pertoire de sortie

str1 = 'Nom du r�pertoire o� les masques compens�es seront sauvegard�s.';
str2 = 'Name of the directory where the compensated images will be saved.';
[flag, nomDirOut] = my_uigetdir(repExport, Lang(str1,str2));
if ~flag
    return
end
repExport = nomDirOut;

nomDirIn = fileparts(ListeFicImage{1});
if strcmp(nomDirOut, nomDirIn)
    str1 = 'Le r�pertoire de sortie ne peut pas �tre le m�me que celui des images d''origine.';
    str2 = 'The output directory cannot be the same as the input input images one.';
    my_warndlg(Lang(str1,str2), 1);
    flag = 0;
end
