function [flag, ListeFic, nomDirOut, repImport, repExport] = params_OTUS_ExportLatLongImages3DV(repImport, repExport)

%% Liste des fichiers mosaiques � exporter

[flag, ListeFic, lastDir] = uiSelectFiles('ExtensionFiles', {'.tif';'.jpg'},  'AllFilters', 1, ...
    'ChaineExclue', '_Dist', 'RepDefaut', repImport, 'ChaineExclue', '_Dist', ...
    'Entete', 'IFREMER - SonarScope - Select .jpg or .tif image files');
if ~flag
    return
end
repImport = lastDir;

%% Nom du r�pertoire o� seront stoch�s les fichiers GLOBE

str1 = 'Nom du r�pertoire o� seront cr��s les fichiers GLOBE. Utilisez le bouton "Create a new directory" si n�cessaire.';
str2 = 'Name of the directory to save this GLOBE files. Use the "Create a new directory" button if necessary.';
[flag, nomDirOut] = my_uigetdir(repExport, Lang(str1,str2));
if ~flag
    return
end
repExport = nomDirOut;
