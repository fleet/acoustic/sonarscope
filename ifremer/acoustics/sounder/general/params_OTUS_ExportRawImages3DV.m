function [flag, ListeFic, nomFicXML, Carto, nomFicXMLPosition, repImport, repExport] ...
    = params_OTUS_ExportRawImages3DV(repImport, repExport, varargin)

[varargin, Capteur] = getPropertyValue(varargin, 'Capteur', 'OTUS'); %#ok<ASGLU>

nomFicXML         = [];
nomFicXMLPosition = [];

Carto = cl_carto([]);
Carto = Carto([]);

%% Saisie des noms de fichiers image

[flag, ListeFic, repImport] = uiSelectFiles('ExtensionFiles', {'.tif';'.jpg'}, 'AllFilters', 1, 'RepDefaut', repImport, ...
    'Entete', 'IFREMER - SonarScope - Select .jpg or .tiff image files');
if ~flag
    return
end

%% Saisie du nom du fichier XML d�crivant les positions

str1 = 'Fichier XML qui contient les informations des images';
str2 = 'Select the .xml file that describes the positions of the images';
[flag, nomFicXMLPosition] = my_uigetfile(fullfile(repImport, '*.xml'), Lang(str1,str2));
if ~flag
    return
end

%% Nom du fichier XML de sortie (ou .ifr)

filtre = fullfile(repExport, [Capteur '.xml']);
[flag, nomFicXML] = my_uiputfile({'*.xml'}, Lang('Nom du fichier de sortie', 'Give a file name'), filtre);
if ~flag
    return
end
repExport = fileparts(nomFicXML);

%% Saisie des param�tres cartographiques

Carto = define_carto_Folder(Carto, ListeFic{1});
if isempty(Carto)
    [flag, DataPosition] = XMLBinUtils.readGrpData(nomFicXMLPosition);
    if ~flag
        return
    end
    
    Latitude  = floor(mean(DataPosition.Latitude, 'omitnan'));
    Longitude = floor(mean(DataPosition.Longitude, 'omitnan'));
    
    nomDir = fileparts(ListeFic{1});
    [flag, Carto] = define(Carto, nomDir, Latitude, Longitude);
    if ~flag
        return
    end
end
