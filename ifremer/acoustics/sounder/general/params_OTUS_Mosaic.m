% Saisie des param�tres de traitement du module de cr�ation de mosaiques
% retreintes des images photo OTUS du Rov Victor
%
% Syntax
%   [flag, ListeFic, resol, nomRepOut, nomFicCompens, Carto, TypeCompensation, ...] = params_OTUSMosaic(repImport, repExport)
%
% Input Arguments
%   repImport, repExport : 
%
% Output Arguments
%   flag          : 1 = OK, 0=KO
%   this          : Instance de clc_image
%   ListeFic : Liste des fichiers
%   resol         : R�solution des mosaiques individuelles (m)
%   MosaicName    : Mosaic name
%   nomRepOut     : Nom du r�pertoire de stockage des mosaiques
%   nomFicCompens : Nom du fichier de compensation
%   Carto         : Instance de cl_carto
%   TypeCompensation      : 1 no compensation, 2=One single model, 3=auto on every image
%   nbMaxPixelsWidthTile  : Maximum number of pixels in the width of a tile
%   nbMaxPixelsHeightTile : Maximum number of pixels in the height of a tile
%   nomDirImagesCompensee : Name of the directory where the compensated images will be saved, [] either
%
% See also clc_image/callback_sonar OTUS_creationXml Authors
% Authors  : JMA
%--------------------------------------------------------------------------

function [flag, ListeFic, resol, MosaicName, nomRepOutDales, nomFicCompens, Carto, TypeCompensation, ...
    nbMaxPixelsWidthTile, nbMaxPixelsHeightTile, nomFicXMLPosition, nomDirImagesCompensee, Capteur, ...
    HeightMax, NomRacine, ext, repImport, repExport] = params_OTUS_Mosaic(repImport, repExport, varargin)

[varargin, Capteur] = getPropertyValue(varargin, 'Capteur', 'OTUS'); %#ok<ASGLU>

resol                 = [];
nomRepOutDales        = [];
nomFicCompens         = [];
TypeCompensation      = [];
MosaicName            = [];
nbMaxPixelsWidthTile  = [];
nbMaxPixelsHeightTile = [];
nomFicXMLPosition     = [];
nomDirImagesCompensee = [];
HeightMax             = [];
NomRacine             = [];
ext                   = [];

Carto = cl_carto([]);
Carto = Carto([]);

switch Capteur
    case 'OTUS'
        listeExt = {'.mat'; '.jpg'; '.tif'};
    case 'SCAMPI'
        listeExt = {'.mat'; '.tif'; '.jpg'};
    otherwise
        return
end

%% Saisie des noms de fichiers image

[flag, ListeFic, repImport] = uiSelectFiles('ExtensionFiles', listeExt, 'RepDefaut', repImport, ...
    'ChaineExclue', '_Dist', 'Entete', 'IFREMER - SonarScope - Select .jpg, .tif or .mat image files');
if ~flag
    return
end

[~, nomFicSeul, ext] = fileparts(ListeFic{1});
switch Capteur
    case 'OTUS'
        kDeb = strfind(nomFicSeul, '_');
        NomRacine = nomFicSeul(1:kDeb(1));
    case 'SCAMPI'
        kDeb = strfind(nomFicSeul, '-');
        NomRacine = nomFicSeul(1:kDeb(3));
end

%% Saisie du nom du fichier XML d�crivant les positions

str1 = 'Fichier XML qui contient les informations des images';
str2 = 'Select the .xml file that describes the positions of the images';
[flag, nomFicXMLPosition] = my_uigetfile(fullfile(repImport, '*.xml'), Lang(str1,str2));
if ~flag
    return
end

%% Nom g�n�rique de la mosa�que

str1 = 'Nom g�n�rique de la mosa�que';
str2 = 'Mosaic name';
nomVar = {Lang(str1,str2)};
switch Capteur
    case 'OTUS'
        value = {'Mosaic_OTUS'};
    case 'SCAMPI'
        value = {'Mosaic_SCAMPI'};
end
[MosaicName, flag] = my_inputdlg(nomVar, value);
if ~flag
    return
end
MosaicName = MosaicName{1};

%% Saisie du nom de r�pertoire o� seront stock�es les petites mosaiques

[flag, nomRepOut] = my_uigetdir(repExport, 'Name of the directory to save mosaique files. Use the "Create a new directory" button if necessary.');
if ~flag
    return
end
nomRepOutDales = nomRepOut;
repExport = nomRepOutDales;

%% R�solution des images

str1 = 'Saisie de la r�solution des mosaiques et de leur dimensions maximales';
str2 = 'Grid size and maximum size for the mosaicks';
p(1) = ClParametre('Name', Lang('R�solution', 'Grid size'), ...
    'Unit', 'm',  'MinValue', 0.002, 'MaxValue', 1, 'Value', 0.01);
p(2) = ClParametre('Name', Lang('Hauteur max d''une tuile', 'Max hight of tiles'), ...
    'Unit', 'pixels',  'MinValue', 5000, 'MaxValue', 100000, 'Value', 14000, 'Format', '%d'); % 120 * 300/2.54
p(3) = ClParametre('Name', Lang('Largeur max d''une tuile', 'Max width of tiles'), ...
    'Unit', 'pixels',  'MinValue', 5000, 'MaxValue', 100000, 'Value', 8000, 'Format', '%d'); % 80 * 300/2.54
p(4) = ClParametre('Name', Lang('Hauteur max', 'Hight max'), ...
    'Unit', 'm',  'MinValue',0.1, 'MaxValue', 20, 'Value', 15);
a = StyledParametreDialog('params', p, 'Title', Lang(str1,str2));
% a.Help = 'http://flotte.ifremer.fr/fleet/Presentation-of-the-fleet/On-board-software/SonarScope';
a.sizes = [0 -2 -1 -2 -1 -1 -1 0];
a.openDialog;
flag = a.okPressedOut;
if ~flag
    return
end
val = a.getParamsValue;
resol                 = val(1);
nbMaxPixelsHeightTile = val(2);
nbMaxPixelsWidthTile  = val(3);
HeightMax             = val(4);

%% Compensation des images

if get_LevelQuestion >= 3
    str{1} = Lang('Non', 'No');
    str{2} = Lang('Un model utilisateur', 'A given model');
    str{3} = Lang('Compensations individuelles', 'Individual compensations');
    str1 = 'Voulez-vous compenser les images ?';
    str2 = 'Do you want to compensate the images ?';
    [TypeCompensation, flag] = my_listdlg(Lang(str1,str2), str);
    if ~flag
        return
    end
    if TypeCompensation == 2
        [flag, nomFicCompens] = my_uigetfile( ...
            {'*.mat', 'OTUS compensation file (*.mat)'}, ...
            'Pick an OTUS compensation file', repImport);
        if ~flag
            return
        end
    end
else
    TypeCompensation = 1;
end

%% Sauvegarde des fichiers compens�s

if TypeCompensation ~= 1
    str1 = 'Voulez-vous sauvegarder les images comprens�es dans un r�pertoire ?';
    str2 = 'Do you want to save the compensated images in a directory ?';
    [rep, flag] = my_questdlg(Lang(str1,str2), 'Init', 2);
    if ~flag
        return
    end
    if rep == 1
        str1 = 'Nom du r�pertoire o� seront stock�es les images compens�es.';
        str2 = 'Name of the directory to save compensated images. Use the "Create a new directory" button if necessary.';
        [flag, nomRepOut] = my_uigetdir(repExport, Lang(str1,str2));
        if ~flag
            return
        end
        nomDirImagesCompensee = nomRepOut;
        % Message si on donne le m�me r�pertoire que celi d'origine : % TODO
        %         if strcmp(nomRepOut, )
        %
        %         end
    end
end

%% Saisie des param�tres cartographiques

Carto = define_carto_Folder(Carto, ListeFic{1});
if isempty(Carto)
    [flag, DataPosition] = XMLBinUtils.readGrpData(nomFicXMLPosition);
    if ~flag
        return
    end
    
    Latitude  = floor(mean(DataPosition.Latitude,  'omitnan'));
    Longitude = floor(mean(DataPosition.Longitude, 'omitnan'));
    
    nomDir = fileparts(ListeFic{1});
    [flag, Carto] = define(Carto, nomDir, Latitude, Longitude);
    if ~flag
        return
    end
end
