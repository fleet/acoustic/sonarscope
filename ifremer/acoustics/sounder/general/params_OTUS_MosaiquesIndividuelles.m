% Saisie des param�tres de traitement du module de cr�ation des mosaiques
% individuelles des images photo OTUS du Rov Victor
%
% Syntax
%   [flag, ListeFic, resol, nomRepOut, nomFicCompens, Carto, TypeCompensation, nomFicXMLPosition, ...
%          nomDirImagesCompensee, HeightMax, repImport, repExport] = params_OTUS_MosaiquesIndividuelles(repImport, repExport)
%
% Input Arguments
%   repImport, repExport : 
%
% Output Arguments
%   flag          : 1 = OK, 0=KO
%   ListeFic : Liste des fichiers
%   resol         : R�solution des mosaiques individuelles (m)
%   nomRepOut     : Nom du r�pertoire de stockage des mosaiques
%   nomFicCompens : Nom du fichier de compensation
%   Carto         : Instance de cl_carto
%   TypeCompensation      : 
%   nomFicXMLPosition     : 
%   nomDirImagesCompensee : 
%   HeightMax             : 
%   repImport, repExport : 
%
% See also clc_image/callback_sonar OTUS_creationXml Authors
% Authors  : JMA
%--------------------------------------------------------------------------

function [flag, ListeFic, resol, nomRepOut, nomFicCompens, Carto, TypeCompensation, nomFicXMLPosition, ...
    nomDirImagesCompensee, HeightMax, repImport, repExport] = params_OTUS_MosaiquesIndividuelles(repImport, repExport)

resol                 = [];
nomRepOut             = [];
nomFicCompens         = [];
nomFicXMLPosition     = [];
nomDirImagesCompensee = [];
HeightMax             = [];

Carto = cl_carto([]);
Carto = Carto([]);

% CLim = [40 200];
TypeCompensation = [];

%% Saisie des noms de fichiers image

[flag, ListeFic, repImport] = uiSelectFiles('ExtensionFiles', {'.tif';'.jpg'}, 'AllFilters', 1, ...
    'ChaineExclue', '_Dist', 'RepDefaut', repImport, ...
    'Entete', 'IFREMER - SonarScope - Select .jpg or .tiff image files');
if ~flag
    return
end

%% Saisie du nom du fichier XML d�crivant les positions

str1 = 'Fichier XML qui contient les informations des images';
str2 = 'Select the .xml file that describes the positions of the images';
[flag, nomFicXMLPosition] = my_uigetfile(fullfile(repImport, '*.xml'), Lang(str1,str2));
if ~flag
    return
end

%% Saisie du nom de r�pertoire o� seront stock�es les mosaiques individuelles

[flag, nomRepOut] = my_uigetdir(repExport, 'Name of the directory to save this individual files. Use the "Create a new directory" button if necessary.');
if ~flag
    return
end
repExport = nomRepOut;

%% Resolution des images

str1 = 'Saisie de la r�solution des mosaiques individuelles';
str2 = 'Grid size for the individual mosaicks';
p(1) = ClParametre('Name', Lang('R�solution', 'Resolution'), ...
    'Unit', 'm',  'MinValue', 0.0025, 'MaxValue', 1, 'Value', 0.01);
p(2) = ClParametre('Name', Lang('Bloc', 'Bloc'), ...
    'Unit', 'm',  'MinValue', 0.1, 'MaxValue', 20, 'Value', 15); % '%d'
a = StyledParametreDialog('params', p, 'Title', Lang(str1,str2));
% a.Help = 'http://flotte.ifremer.fr/fleet/Presentation-of-the-fleet/On-board-software/SonarScope';
a.sizes = [0 -2 -1 -2 -1 -1 -1 0];
a.openDialog;
flag = a.okPressedOut;
if ~flag
    return
end
val = a.getParamsValue;
resol     = val(1);
HeightMax = val(2);

%% Compensation des images

if get_LevelQuestion >= 3
    str{1} = Lang('Non', 'No');
    str{2} = Lang('Un model utilisateur', 'A given model');
    str{3} = Lang('Compensations individuelles', 'Individual compensations');
    str1 = 'Voulez-vous compenser les images ?';
    str2 = 'Do you want to compensate the images';
    [TypeCompensation, flag] = my_listdlg(Lang(str1,str2), str);
    if ~flag
        return
    end
    if TypeCompensation == 2
        [flag, nomFicCompens] = my_uigetfile( ...
            {'*.mat', 'OTUS compensation file (*.mat)'}, ...
            'Pick an OTUS compensation file', repImport);
        if ~flag
            return
        end
    end
else
    TypeCompensation = 1;
end

%% Sauvegarde des fichiers compens�s

if TypeCompensation ~= 1
    str1 = 'Voulez-vous sauvegarder les images comprens�es dans un r�pertoire ?';
    str2 = 'Do you want to save the compensated images in a directory ?';
    [rep, flag] = my_questdlg(Lang(str1,str2), 'Init', 2);
    if ~flag
        return
    end
    if rep == 1
        str1 = 'Nom du r�pertoire o� seront stock�s les images compens�es.';
        str2 = 'Name of the directory to save mosaique files. Use the "Create a new directory" button if necessary.';
        [flag, nomRepOut] = my_uigetdir(repExport, Lang(str1,str2));
        if ~flag
            return
        end
        nomDirImagesCompensee = nomRepOut;
        % Message si on donne le m�me r�pertoire que celi d'origine : % TODO
%         if strcmp(nomRepOut, )
%             
%         end
    end
end

%% Saisie des param�tres cartographiques

 Carto = define_carto_Folder(Carto, ListeFic{1});
 if isempty(Carto)
     [flag, DataPosition] = XMLBinUtils.readGrpData(nomFicXMLPosition);
     if ~flag
         return
     end
     
    Latitude  = floor(mean(DataPosition.Latitude,  'omitnan'));
    Longitude = floor(mean(DataPosition.Longitude, 'omitnan'));
    
    nomDir = fileparts(ListeFic{1});
    [flag, Carto] = define(Carto, nomDir, Latitude, Longitude);
     if ~flag
        return
    end
end
