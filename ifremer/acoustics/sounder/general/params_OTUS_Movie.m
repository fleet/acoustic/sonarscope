% Saisie des param�tres de cr�ation d'un film (.avi) des images photo OTUS du Rov Victor
%
% Syntax
%   [flag, nomFicAvi, fps, quality, nomFicCompens, CLim, repExport] = params_OTUS_Movie(repExport)
%
% Input Arguments
%   repExport : Default import directory
%
% Output Arguments
%   flag      : 1 = OK, 0=KO
%   repExport : Default import directory
%   fps       : Frequence d'affichage des images
%   quality   : Qualit� de la compression (75 pard�faut)
%   CLim      : Bornes de rehaussement de contraste
%
% See also clc_image/callback_sonar OTUS_Movie Authors
% Authors    : JMA
%--------------------------------------------------------------------------

function [flag, ListeFicImage, nomFicAvi, fps, quality, repImport, repExport] = params_OTUS_Movie(repImport, repExport)

nomFicAvi = [];
fps       = [];
quality   = 0;

%% Saisie de la liste des fichiers

[flag, ListeFicImage, repImport] = uiSelectFiles('ExtensionFiles', {'.tif';'.jpg'},  'AllFilters', 1, ...
    'RepDefaut', repImport,  'Entete', 'IFREMER - SonarScope - Select .jpg or .tiff image files');
if ~flag
    return
end

%% Saisie du nom du fichier .avi

nomFicAvi = fullfile(repExport, 'Movie.avi');
Filtre = {  '*.avi',   'AVI file'; ...
            '*.mp4',   'MPEG-4 file (systems with Windows 7 or Mac OS X 10.7 and later)'; ...
            '*.mj2',   'Motion JPEG 2000 file'; ...
            '*.*',     'All Files (*.*)'};
str1 = 'Nom du fichier ?';
str2 = 'Give a file name';
[flag, nomFicAvi] = my_uiputfile(Filtre, Lang(str1,str2), nomFicAvi);
if ~flag
    return
end
repExport = fileparts(nomFicAvi);

%% Saisie de la fr�quence d'affichage et de la qualit� de la compressiondes
%  et des bornes de rehaussement de contraste

str1 = 'TODO';
str2 = 'Movie maker parametres';
p = ClParametre('Name', Lang('Frames par seconde', 'Frames per second'), ...
    'Unit', 'Hz',  'MinValue', 1, 'MaxValue', 25, 'Value', 2, 'Format', '%d');
a = StyledParametreDialog('params', p, 'Title', Lang(str1,str2));
% a.Help = 'http://flotte.ifremer.fr/fleet/Presentation-of-the-fleet/On-board-software/SonarScope';
a.sizes = [0 -3 0 -2 0 -1 -1 0];
a.openDialog;
flag = a.okPressedOut;
if ~flag
    return
end
fps = a.getParamsValue;
