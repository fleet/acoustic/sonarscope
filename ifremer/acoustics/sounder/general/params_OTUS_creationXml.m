% Saisie des param�tres de traitement du module de cr�ation des fichiers xml
% de description des images photo OTUS du Rov Victor
%
% Syntax
%   [flag, ListeFicImage, nomFicNav, nomFicXls, repImport] = params_OTUS_creationXml(repImport)
%
% Input Arguments
%   this : Instance de clc_image
%
% Output Arguments
%   flag          : 1 = OK, 0=KO
%   ListeFicImage : Liste des fichiers
%   nomFicNav     : Nom du fichier de navigation
%   nomFicXls     : Nom du fichier des param�tres du ROV
%
% See also clc_image/callback_sonar OTUS_creationXml Authors
% Authors    : JMA
%--------------------------------------------------------------------------

function [flag, ListeFicImage, nomFicNav, nomFicXls, repImport] = params_OTUS_creationXml(repImport)

nomFicNav = [];
nomFicXls = [];

%% Saisie des noms de fichiers image

[flag, ListeFicImage, repImport] = uiSelectFiles('ExtensionFiles', {'.tif';'.jpg'}, ...
    'AllFilters', 1, 'RepDefaut', repImport, ...
    'Entete', 'IFREMER - SonarScope - Select .jpg or .tif image files');
if ~flag
    return
end

%% Saisie du nom du fichier de navigation

[flag, nomFicNav] = my_uigetfile(...
       {'*.nvi', 'Caraibe navigation file (*.nvi)'}, ...
        'Pick a Caraibes navigation file');
if ~flag
    return
end
    
%% Saisie du nom du fichier capteur du ROV

[flag, nomFicXls] = my_uigetfile( ...
       {'*.xls', 'Victor Excel file (*.xls)'}, ...
        'Pick a Victor Excel file');
if ~flag
    return
end
