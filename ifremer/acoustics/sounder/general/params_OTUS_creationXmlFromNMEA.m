% Saisie des param�tres de traitement du module de cr�ation des fichiers xml
% de description des images photo OTUS du Rov Victor
%
% Syntax
%   [flag, ListeFicImage, nomFicNav, nomFicXls, repImport, repExport] = params_OTUS_creationXml(repImport, repExport)
%
% Input Arguments
%   repImport, repExport : 
%
% Output Arguments
%   flag          : 1 = OK, 0=KO
%   this          : Instance de clc_image
%   ListeFicImage : Liste des fichiers
%   nomFicNav     : Nom du fichier de navigation
%   nomFicXls     : Nom du fichier des param�tres du ROV
%   repImport, repExport : 
%
% See also clc_image/callback_sonar OTUS_creationXml Authors
% Authors  : JMA
%--------------------------------------------------------------------------

function [flag, ListeFicImage, nomFicNmea, nomFicNavCorrigee, nomFicOut, repImport, repExport] = params_OTUS_creationXmlFromNMEA(repImport, repExport)

nomFicNmea        = [];
nomFicNavCorrigee = [];
nomFicOut         = [];

%% Saisie des noms de fichiers image

[flag, ListeFicImage, repImport] = uiSelectFiles('ExtensionFiles', {'.tif';'.jpg'},  'AllFilters', 1, 'RepDefaut', repImport, ...
    'Entete', 'IFREMER - SonarScope - Select .jpg or .tif image files');
if ~flag
    return
end

%% Saisie du nom du fichier de navigation

[flag, nomFicNmea, repImport] = uiSelectFiles('ExtensionFiles', {'.nme'}, 'RepDefaut', repImport, ...
    'Entete', 'IFREMER - SonarScope - Select NMEA files (.nme)');
if ~flag
    return
end

%% Saisie du nom du fichier de navigation corrig�

str1 = 'Avez-vous un fichier excel (.dbf) de navigation corrig�e ?';
str2 = 'Do you have an excel (.dbf) file of the corrected navigation  ?';
[rep, flag] = my_questdlg(Lang(str1,str2));
if ~flag
    return
end
if rep == 1
    [flag, nomFicNavCorrigee, repImport] = uiSelectFiles('ExtensionFiles', {'.dbf'}, 'RepDefaut', repImport, ...
        'Entete', 'IFREMER - SonarScope - Select a DBF file (.dbf)');
    if ~flag
        return
    end
    nomFicNavCorrigee = nomFicNavCorrigee{1};
end

%% Nom du fichier de sortie

str1 = 'Nom du fichier XML qui va contenir toutes les infos relatives aux fichiers que vous avez s�lection�s.';
str2 = 'Output XML file that will contain all the informations of the selected images.';
[FileName, repExport] = uiputfile('*.xml', Lang(str1,str2), fullfile(repExport, 'NavigationAttitudeOTUS.xml'));
if isequal(FileName, 0) || isequal(repExport, 0)
    flag = 0;
    return
end
nomFicOut = fullfile(repExport, FileName);
