function [flag, nomFic, resol, repImport] = params_PatchTestRoll(Ext, repImport)

resol = [];

%% List of files

str1 = 'S�lectionnez les 2 fichiers';
str2 = 'Select the 2 needed files';
[flag, nomFic, repImport] = uiSelectFiles('ExtensionFiles', Ext, 'RepDefaut', repImport, ...
    'Entete', Lang(str1,str2));
if ~flag
    return
end

if ~iscell(nomFic) || (length(nomFic) ~= 2)
    str1 = 'Il faut 2 fichiers acquis sur la ligne mais en sens oppos�s pour faire cette calibration en roulis.';
    str2 = 'You need 2 lines in opposite directions to perform this roll biais identification.';
    my_warndlg(Lang(str1,str2), 1);
    flag = 0;
    return
end

%% Grid size

str1 = 'D�finition du pas de grille.';
str2 = 'Grid size determination.';
[rep, flag] = my_questdlg(Lang(str1,str2), 'Auto', 'Manual');
if ~flag
    return
end

if rep == 2
    [flag, resol] = get_gridSize();
    if ~flag
        return
    end
end
