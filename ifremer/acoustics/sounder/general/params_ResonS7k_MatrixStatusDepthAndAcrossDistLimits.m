function [flag, statusDepthAndAcrossDistLimits] = params_ResonS7k_MatrixStatusDepthAndAcrossDistLimits()

% TODO : intitul� ResonS7k pour l'instant. Voir si on peut transposer cette
% nouvelle mouture aux KM

statusDepthAndAcrossDistLimits = [];

str1 = 'Voulez-vous imposer les limites min et max de la matrice 3D ?';
str2 = 'Do you want to define by yourself the min and max values of the 3D mtrix ?';
[ind, flag] = my_questdlg(Lang(str1,str2), 'Init', 2);
if ~flag
    return
end

if ind == 1
    minAcrossDist = -6000;
    maxAcrossDist =  6000;
    minDepth      = -7000;
	[flag, minAcrossDist, maxAcrossDist, minDepth] = question_WCLimits3DMatrix(minAcrossDist, maxAcrossDist, 'minDepth', minDepth);
    statusDepthAndAcrossDistLimits.minDepth      = minDepth;
    statusDepthAndAcrossDistLimits.minAcrossDist = minAcrossDist;
    statusDepthAndAcrossDistLimits.maxAcrossDist = maxAcrossDist;
end
