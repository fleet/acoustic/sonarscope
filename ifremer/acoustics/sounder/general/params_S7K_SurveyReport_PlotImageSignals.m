function [flag, nomFicAdoc, indexConfigs, repExport] = params_S7K_SurveyReport_PlotImageSignals(repExport)

indexConfigs = [];

%% Nom du survey

nomFiltre = fullfile(repExport, '*.adoc');
[flag, nomFicAdoc] = my_uigetfile(nomFiltre, 'Survey Report file name');
if ~flag
    return
end
repExport = fileparts(nomFicAdoc);

%% indexConfigs

str{1} = 'Bathy and Attitude';
str{2} = 'Reflectivity and Sonar Settings';
[indexConfigs, flag] = my_listdlgMultiple('Selection', str, 'InitialValue', 1:2);
if ~flag
    return
end
