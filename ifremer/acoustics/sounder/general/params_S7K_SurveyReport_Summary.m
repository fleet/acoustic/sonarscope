function [flag, nomFicAdoc, repImport] = params_S7K_SurveyReport_Summary(repImport)

%% Nom du survey

nomFicCompens = fullfile(repImport, '*.adoc');
[flag, nomFicAdoc] = my_uigetfile(nomFicCompens, 'Survey Report file name');
if ~flag
    return
end
repImport = fileparts(nomFicAdoc);
