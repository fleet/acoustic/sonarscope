% Saisie des param�tres de traitement du module de cr�ation des fichiers xml
% de description des images photo SCAMPI du Rov Victor
%
% Syntax
%   [flag, ListeFicImage, nomFicNav, ...] = params_SCAMPI_creationXml(repImport, repExport)
%
% Input Arguments
%   repImport, repExport : 
%
% Output Arguments
%   flag          : 1 = OK, 0=KO
%   ListeFicImage : Liste des fichiers
%   nomFicNav     : Nom du fichier de navigation
%   nomFicXls     : Nom du fichier des param�tres du ROV
%
% See also clc_image/callback_sonar SCAMPI_creationXml Authors
% Authors  : JMA
%--------------------------------------------------------------------------

function [flag, ListeFicImage, nomFicNavCorrigee, nomFicOut, Focale, repImport, repExport] ...
    = params_SCAMPI_creationXmlFromNMEA(repImport, repExport) 

nomFicNavCorrigee = [];
nomFicOut         = [];
Focale            = [];

%% Saisie des noms de fichiers image

[flag, ListeFicImage, repImport] = uiSelectFiles('ExtensionFiles', {'.tif';'.jpg'},  'AllFilters', 1, 'RepDefaut', repImport, ...
    'Entete', 'IFREMER - SonarScope - Select .jpg or .tif image files');
if ~flag
    return
end

%% Saisie du nom du fichier de navigation corrig�

%{
str1 = 'Avez-vous un fichier (.txt) de navigation ?';
str2 = 'Do you have a .txt file of the  navigation  ?';
[rep, flag] = my_questdlg(Lang(str1,str2));
if ~flag
    return
end
%}
rep = 1;
if rep == 1
    [flag, nomFicNavCorrigee, repImport] = uiSelectFiles('ExtensionFiles', {'.txt'}, 'RepDefaut', repImport, ...
        'Entete', 'IFREMER - SonarScope - Select a file (.txt)');
    if ~flag
        return
    end
    nomFicNavCorrigee = nomFicNavCorrigee{1};
end

%% Nom du fichier de sortie

str1 = 'Nom du fichier XML qui va contenir toutes les infos relatives aux fichiers que vous avez s�lection�s.';
str2 = 'Output XML file that will contain all the informations of the selected images.';
[FileName, repExport] = uiputfile('*.xml', Lang(str1,str2), fullfile(repExport, 'NavigationAttitudeSCAMPI.xml'));
if isequal(FileName, 0) || isequal(repExport, 0)
    flag = 0;
    return
end
nomFicOut = fullfile(repExport, FileName);

%% Focale

str1 = 'Focale de la cam�ra';
str2 = 'Focal of the carema';
[flag, Focale] = inputOneParametre( Lang(str1,str2), 'Value', ...
    'Value', 53, 'Unit', 'deg', 'MinValue', 20, 'MaxValue', 85);
if ~flag
    return
end
