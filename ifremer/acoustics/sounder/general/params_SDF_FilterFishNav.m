% Apparemment plus utilis�

function [flag, FilterLatLon, FilterHeading] = params_SDF_FilterFishNav

persistent persistent_FilterLatLon persistent_FilterHeading

%% Saisie des param�tres de filtrage de la latitude et de la longitude

if isempty(persistent_FilterLatLon)
    Ordre = 2;
    Wc    = 1/20;
else
    Ordre = persistent_FilterLatLon.Ordre;
    Wc    = persistent_FilterLatLon.Wc;
end

[flag, Ordre, Wc] = SaisieParamsButterworth(Ordre, Wc);
if flag
    FilterLatLon.Ordre = Ordre;
    FilterLatLon.Wc = Wc;
    persistent_FilterLatLon = FilterLatLon;
else
    FilterLatLon  = [];
    FilterHeading = [];
    return
end


%% Saisie des param�tres de filtrage du cap

if isempty(persistent_FilterHeading)
    Ordre = 2;
    Wc    = 1/20;
else
    Ordre = persistent_FilterHeading.Ordre;
    Wc    = persistent_FilterHeading.Wc;
end

[flag, Ordre, Wc] = SaisieParamsButterworth(Ordre, Wc);
if flag
    FilterHeading.Ordre = Ordre;
    FilterHeading.Wc = Wc;
    persistent_FilterHeading = FilterHeading;
else
    FilterHeading = [];
    return
end
