function [flag, FileList, Resol, Window, Backup, InfoMos, CorFile, ModeDependant, ...
    LimitAngles, listeMode, listeSwath, listeFM, repImport, repExport] ...
    = params_SimradAll_WC_Mosaic_EI_SSc(repImport, repExport)

persistent persistentResol  persistentBackup
% persistent persistentWindow

E0 = cl_ermapper([]);

Resol  = persistentResol;
% Window = persistentWindow;
Backup = persistentBackup;

FileList            = [];
Window              = [];
CorFile             = [];
ModeDependant       = 0; 
LimitAngles         = [];
listeMode           = [];
listeSwath          = [];
listeFM             = [];
InfoMos.TypeMos             = 1;
InfoMos.nomFicErMapperLayer = [];
InfoMos.nomFicErMapperAngle = [];
InfoMos.DirName             = [];

%% Liste des fichiers .XML

[flag, ListeFicReflec, repImport] = uiSelectFiles('ExtensionFiles', '.xml', repImport, ...
    'InitGeometryType', cl_image.indGeometryType('PingAcrossDist'), 'InitDataType', cl_image.indDataType('Reflectivity'), ...
    'RepDefaut', repImport);
if ~flag
    return
end
if isempty(ListeFicReflec)
    return
end

%% Recherche des modes, nbSwath et FM

Mode_AllLines  = [];
Swath_AllLines = [];
FM_AllLines    = [];
iFic_AllLines  = [];
nbProfils = length(ListeFicReflec);
% LineModes = cell(nbProfils,1);
% LineSwath = cell(nbProfils,1);
% LineFM    = cell(nbProfils,1);
str1 = 'Inspection des fichiers';
str2 = 'Checking the files';
hw = create_waitbar(Lang(str1,str2), 'N', nbProfils);
for iProfil=1:nbProfils
    my_waitbar(iProfil, nbProfils, hw);
    
    %% Lecture rapide des signaux
    
    [flag, Mode, Swath, FM] = XMLBinUtils.getSignals(ListeFicReflec{iProfil}, 'PortMode_1', 'NbSwaths', 'PresenceFM', ...
        'Memmapfile', 1, 'Mute', 1);
    if ~flag
        continue
    end
    
    Mode_AllLines  = [Mode_AllLines;  Mode(:)]; %#ok<AGROW>
    Swath_AllLines = [Swath_AllLines; Swath(:)]; %#ok<AGROW>
    FM_AllLines    = [FM_AllLines;    FM(:)]; %#ok<AGROW>
    iFic_AllLines  = [iFic_AllLines; repmat(iProfil, length(Mode), 1)]; %#ok<AGROW>
end
my_close(hw, 'MsgEnd');

maxModes = max(Mode_AllLines);
histoMode  = zeros(1, maxModes);
histoSwath = zeros(1,2);
histoFM    = zeros(1,2);
histoFic   = zeros(1, nbProfils);
histoGeneral = zeros(nbProfils, maxModes, 2, 2);
for kPing=1:length(Mode_AllLines)
    ModePing  = Mode_AllLines(kPing);
    if isempty(Swath_AllLines)
        SwathPing = 1;
    else
        SwathPing = Swath_AllLines(kPing);
    end
    if isempty(FM_AllLines)
        FMPing = 1;
    else
        FMPing = FM_AllLines(kPing) + 1;
    end
    iFic = iFic_AllLines(kPing);
    histoMode(ModePing)   = histoMode(ModePing) + 1;
    histoSwath(SwathPing) = histoSwath(SwathPing) + 1;
    histoFM(FMPing)       = histoFM(FMPing) + 1;
    histoFic(iFic)        = histoFic(iFic) + 1;
    histoGeneral(iFic, ModePing, SwathPing, FMPing) = histoGeneral(iFic, ModePing, SwathPing, FMPing) + 1;
end

subFiles    = {};
ModeListe   = [];
SwathListe  = [];
FMListe     = [];
strFM       = {'CW';'FM'};
strSwath    = {'Single';'Double'};
str         = {};
nomFicListe = {};
nomFicAllListe = {};
for k_FM=1:2
    for k_Swath=1:2
        for k_Mode=1:maxModes
            sub = histoGeneral(:, k_Mode, k_Swath, k_FM);
            nbPingsImpactes = sum(sub);
            listeFicOK = find(sub ~= 0);
            nbFicImpactes   = length(listeFicOK);
            if nbPingsImpactes ~= 0
                subFiles{end+1}      = listeFicOK'; %#ok<AGROW>
                ModeListe(end+1)     = k_Mode; %#ok<AGROW>
                SwathListe(end+1)    = k_Swath; %#ok<AGROW>
                FMListe(end+1)       = k_FM; %#ok<AGROW>
                str{end+1,1}         = sprintf('%s %s - Mode %d  Pings : %6d  - Files : %d', strFM{k_FM}, strSwath{k_Swath}, k_Mode, nbPingsImpactes, nbFicImpactes);  %#ok<AGROW>
                nomFicListe{end+1,1} = sprintf('%s_%s_Mode%d.txt', strFM{k_FM}, strSwath{k_Swath}, k_Mode);  %#ok<AGROW>
                nomFicAllListe{end+1,1} = sprintf('%s_%s_Mode%d_all.txt', strFM{k_FM}, strSwath{k_Swath}, k_Mode);  %#ok<AGROW>
            end
        end
    end
end

%% Cr�ation des listes des fichiers

for k=1:length(nomFicListe)
    nomDir = repImport;
    nomFic1 = fullfile(nomDir, nomFicListe{k});
    nomFic2 = fullfile(nomDir, nomFicAllListe{k});
    str1 = sprintf('Cr�ation des fichiers "%s" et "%s" dans le r�pertoire "%s"', nomFicListe{k},  nomFicAllListe{k}, nomDir);
    str2 = sprintf('Writing files "%s" et "%s" in folder "%s"', nomFicListe{k},  nomFicAllListe{k}, nomDir);
    my_warndlg(Lang(str1,str2), 0, 'Tag', 'CreationMosaixEI', 'TimeDelay', 10);
    fid1 = fopen(nomFic1, 'w+t');
    if fid1 == -1
         messageErreurFichier(nomFic1, 'WriteFailure');
    else
        fid2 = fopen(nomFic2, 'w+t');
        listeFicThisConf = ListeFicReflec(subFiles{k});
        for k2=1:length(listeFicThisConf)
            fprintf(fid1, '%s\n', listeFicThisConf{k2});
            XML = xml_mat_read(listeFicThisConf{k2});
            fprintf(fid2, '%s\n', XML.InitialFileName);
        end
        fclose(fid1);
        fclose(fid2);
    end
end

%% S�lection des fichiers

str1 = 'S�lectionnez le ou les modes � utiliser.';
str2 = 'Select the sounder Modes to use.';
[rep, flag] = my_listdlg(Lang(str1,str2), str, 'InitialValue', 1:length(str));
if ~flag
    return
end
listeMode  = ModeListe(rep);
listeSwath = SwathListe(rep);
listeFM    = FMListe(rep) - 1;
subFic = unique([subFiles{rep}]);
ListeFicReflec = ListeFicReflec(subFic);

%% R�solution et fr�quence de backup

nbProfils = length(ListeFicReflec);
if isempty(persistentResol)
    Resol = 50;
else
    Resol = persistentResol;
end
if isempty(persistentBackup)
    Backup = nbProfils;
else
    Backup = persistentBackup;
end

str1 = 'Pas de grille et fr�quence de sauvegarde';
str2 = 'Grid size and backup recurrence';
p    = ClParametre('Name', Lang('Pas de grille', 'Grid size'), ...
    'Unit', 'm', 'Value', Resol, 'MinValue', 0.1, 'MaxValue', 200);
p(2) = ClParametre('Name', Lang('Fr�quence de sauvegarde', 'Backup frequency'), ...
    'Unit', 'files', 'Value', Backup, 'MinValue', 1, 'MaxValue', nbProfils);
a = StyledSimpleParametreDialog('params', p, 'Title', Lang(str1,str2));
a.openDialog;
flag = a.okPressedOut;
if ~flag
    return
end
val = a.getParamsValue;

Resol = val(1);
Backup = val(2);
persistentResol  = Resol;
persistentBackup = Backup;

%% Signature des modes

Signature = '_Mode';
for k=1:length(listeMode)
    Signature = [Signature num2str(listeMode(k))]; %#ok<AGROW>
end

Signature = [Signature '_Swath'];
for k=1:length(listeSwath)
    Signature = [Signature num2str(listeSwath(k))]; %#ok<AGROW>
end

Signature = [Signature '_FM'];
for k=1:length(listeFM)
    Signature = [Signature num2str(listeFM(k))]; %#ok<AGROW>
end

%% Cr�ation des noms de fichiers des layers

nbFic = length(ListeFicReflec);
FileList = struct([]);
for k=1:nbFic
    [nomDir, nomFic] = fileparts(ListeFicReflec{k});
    nomFic3 = fullfile(nomDir, [strrep(nomFic, '_Reflectivity', '_AlongDist') '.xml']);
    nomFic4 = fullfile(nomDir, [strrep(nomFic, '_Reflectivity', '_TxAngle') '.xml']);
    if exist(nomFic3, 'file') && exist(nomFic4, 'file')
        FileList(end+1).nomFicReflec = ListeFicReflec{k}; %#ok<AGROW>
%         FileList(end).nomFicAcrossDist = nomFic2;
        FileList(end).nomFicAlongDist = nomFic3;
        FileList(end).nomFicTxAngle   = nomFic4;
    else
        str1 = sprintf('Le fichier "%s" est exclu car je n''ai pas trouv� l''un des fichiers suivants : \n\n%s\n%s\n%s', ListeFicReflec{k}, nomFic3, nomFic4);
        str2 = sprintf('The file "%s" is excluded because I could not found one of the following ones : \n\n%s\n%s\n%s', ListeFicReflec{k}, nomFic3, nomFic4);
        my_warndlg(Lang(str1,str2), 1);
    end
end

if isempty(FileList)
    str1 = 'Aucun fichier compl�mentaire (AcrossDist, AlongDist, TxAngle) n''a pas �t� trouv� pour chacun des fichiers que vous avez s�lectionn�.';
    str2 = 'No complementary file has been found for the files you have selected (AcrossDist, AlongDist, TxAngle).';
    my_warndlg(Lang(str1,str2), 1);
    flag = 0;
    return
end

%% Interpolation Window

Window = [5 5];
% if isempty(Window)
%     Window = [5 5];
% end
% [flag, Window] = saisie_window(Window);
% if ~flag
%     return
% end
% persistentWindow = Window;

%% Compensation

nomDirExport = repExport;
[flag, CorFile, ModeDependant, UseModel, PreserveMeanValueIfModel] = QuestionCompensationCurve(nomDirExport); %#ok<ASGLU>
if ~flag
    return
end

%% Limites angulaires

[flag, LimitAngles] = question_AngularLimits('QL', 2);
if ~flag
    return
end

%% Mosaique classique ou par fichier

str1 = 'Quelle sorte de mosaique voules-vous r�aliser ?';
str2 = 'What sort of mosaic do you want to compute ?';
[rep, flag] = my_questdlg(Lang(str1,str2), 'Classical', 'Individual ones');
if ~flag
    return
end
InfoMos.TypeMos = rep;

%% Nom du r�pertoire des mosaiques individuelles

if InfoMos.TypeMos == 2
    [flag, nomDirIndMosaics] = question_NomDirIndividualMosaics(repExport);
    if ~flag
        return
    end
    repExport = nomDirIndMosaics;
    InfoMos.DirName = nomDirIndMosaics;
    
else
    
    %% Nom des fichiers de sauvegarde
    
    DataType = cl_image.indDataType('Reflectivity');
    % NomGenerique = 'Mosaic_EIVertSSc';
    NomGenerique = ['Mosaic_EIVertSSc' Signature];
    % NomGenerique = ['Mosaic_' strrep(num2str(Resol), '.', ', ') 'm_EIVertSSc'];
    [nomFicErMapperLayer, flag] = initNomFicErs(E0, NomGenerique, cl_image.indGeometryType('LatLong'), DataType, nomDirExport, 'resol', Resol);
    if ~flag
        return
    end
    [nomDirExport, TitreDepth] = fileparts(nomFicErMapperLayer);
    DataType = cl_image.indDataType('TxAngle');
    [nomFicErMapperAngle, flag] = initNomFicErs(E0, TitreDepth, cl_image.indGeometryType('LatLong'), DataType, nomDirExport, 'confirm', (get_LevelQuestion >= 3));
    if ~flag
        return
    end
    repExport = fileparts(nomFicErMapperAngle);
    
    if exist(nomFicErMapperLayer, 'file') && exist(nomFicErMapperAngle, 'file')
        [flag, rep] = question_CompleteExistingMosaic;
        if ~flag
            return
        end
        if rep == 2
            try
                delete(nomFicErMapperLayer)
            catch %#ok<CTCH>
            end
            try
                delete(nomFicErMapperAngle)
            catch %#ok<CTCH>
            end
        end
    end
    InfoMos.nomFicErMapperLayer = nomFicErMapperLayer;
    InfoMos.nomFicErMapperAngle = nomFicErMapperAngle;
end
