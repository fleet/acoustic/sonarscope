% Si Mosaique de Reflectivite, on propose de faire une compensation statistique ou une calibration
%   nomDir = 'D:\Temp\Charline'
%   [flag, TypeCalibration, CorFile, DataTypeConditions, alpha, ModelTxDiag, CourbeBS] = params_SonarCompensation(nomDir)

function [flag, TypeCalibration, CorFile, bilan, DataTypeConditions, alpha, ModelTxDiag, CourbeBS] = params_SonarCompensation(nomDir)

TypeCalibration    = 0;
CorFile            = [];
bilan              = [];
DataTypeConditions = [];
alpha              = [];
ModelTxDiag        = [];
CourbeBS           = [];

%% Choix du type de compensation

if nargout == 4
    str{1} = 'None';
    str{2} = 'Statistic compensation';
else
    str{1} = 'None';
    str{2} = 'Statistic compensation';
    str{3} = 'Sonar calibration';
end
str1 = 'Correction de la réflectivité ?';
str2 = 'Reflectivity Image Correction ?';
[rep, flag] = my_listdlg(Lang(str1,str2), str, 'SelectionMode', 'Single');
if ~flag
    return
end
clear str

switch rep
    case 1 % Aucune compensation
        
    case 2 % Compensation statistique
        str1 = 'Sélectionnez le fichier de compensation (cancel sinon)';
        str2 = 'Give a compensation file (cancel instead)';
        [flag, CorFile] = my_uigetfile('*.mat', Lang(str1,str2), nomDir);
        if ~flag
           return
        end
        %             CourbeConditionnelle = loadmat(CorFile, 'nomVar', 'CourbeConditionnelle');
        CourbeConditionnelle = load_courbesStats(CorFile);
        
        bilan = CourbeConditionnelle.bilan;
        
        DataTypeConditions = bilan{1}(1).DataTypeConditions;
        
        TypeCalibration = 1;
        
    case 3 % Calibration
        str1 = 'Type de correction de la réflectivité :  ';
        str2 = 'Reflectivity Image Correction :';
        str{1} = 'Directivity compensated';
        str{2} = 'Directivity & average BS compensated';
        [TypeCalibration, flag] = my_listdlg(Lang(str1,str2), str,  'SelectionMode', 'Single');
        if ~flag
            return
        end
        clear str
        
        [flag, alpha] = get_alpha;
        if ~flag
            return
        end

        [flag, ModelTxDiag] = get_TxDiag(nomDir);
        if ~flag
            return
        end

        if TypeCalibration == 2 % Directivity & average BS compensated
            [flag, CourbeBS] = get_CourbeBS(nomDir);
            if ~flag
                return
            end
        end
end
