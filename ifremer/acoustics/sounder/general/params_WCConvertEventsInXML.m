function [flag, ListeFic, BathyFile, repImport, repExport] = params_WCConvertEventsInXML(repImport, repExport)

BathyFile = [];

%% Liste des fichiers .mat

str1 = 'IFREMER - SonarScope : S�lectionnez les fichiers .txt ou .csv est point�s de panaches.';
str2 = 'IFREMER - SonarScope : Please select the .txt or .csv files containing the plume footprints.';
[flag, ListeFic, repImport] = uiSelectFiles('ExtensionFiles', {'.txt'; '.csv'}, ...
    'AllFilters', 1, 'RepDefaut', repImport, 'Entete', Lang(str1,str2));
if ~flag
    return
end

%% Liste des fichiers .ers

str1 = 'IFREMER - SonarScope : S�lectionnez un fichier .ers econtenant une bathym�trie de la zone.';
str2 = 'IFREMER - SonarScope : Please select one .ers file  containinga DTM of the area.';
[flag, BathyFile, repImport] = uiSelectFiles('ExtensionFiles', '.ers', ...
    'RepDefaut', repImport, 'Entete', Lang(str1,str2));
if ~flag
    return
end
BathyFile = BathyFile{1};
% BathyFile = 'C:\Demos\Marmara\marmara_50m_LatLong_Bathymetry.ers'

%% Nom du r�pertoire des fichiers de sortie

str1 = 'S�lectionnez le r�pertoire o� seront cr��s les fichiers xxxxxx_Footprints.xml.';
str2 = 'Selec the output directory for the xxxxxx_Footprints.xml files. ';
[flag, nomDir] = my_uigetdir(repExport, Lang(str1,str2));
if ~flag
    return
end
repExport = nomDir;
