function [flag, listFileNames, ListeFicAll, nomDirOut, typePolarEchogram, params_ZoneEI, ...
    Tag, StepDisplay, repImport, repExport] ...
    = params_WaterColumnEchoIntegration(TypeEI, repImport, repExport, varargin)

% [varargin, ChaineIncluse] = getPropertyValue(varargin, 'ChaineIncluse', '_Raw_PolarEchograms'); %#ok<ASGLU>
[varargin, ChaineIncluse] = getPropertyValue(varargin, 'ChaineIncluse', '_PolarEchograms'); %#ok<ASGLU>

listFileNames     = [];
ListeFicAll       = {};
nomDirOut         = [];
typePolarEchogram = 1;
Tag               = '';
StepDisplay       = 0;
params_ZoneEI     = [];

%% Type de couche

str = {'.xml'; '.nc'};
[typeFormatIn, flag] = my_listdlg('What polar echograms do you want to use ?', str, 'SelectionMode', 'Single', 'InitialValue', 2);
if ~flag
    return
end

%% Type de couche

if TypeEI == 1 % Over specular
    InitialValue = 1;
else % Over seabed
    InitialValue = 2;
end
str = {'Raw'; 'Comp'};
[typePolarEchogram, flag] = my_listdlg('What polar echograms do you want to use ?', str, ...
    'SelectionMode', 'Single', 'InitialValue', InitialValue);
if ~flag
    return
end

if typePolarEchogram == 1 % Over specular
    if typeFormatIn == 1 % .xml
        ChaineIncluse = '_Raw_PolarEchograms';
        Ext = '.xml';
    else % .nc
        ChaineIncluse = '_PolarEchograms';
        Ext = '.nc';
    end
else % Over seabed
    if typeFormatIn == 1 % .xml
        ChaineIncluse = '_Comp_PolarEchograms';
        Ext = '.xml';
    else % .nc
        ChaineIncluse = '_PolarEchograms';
        Ext = '.nc';
    end
end

%% Liste des fichiers d'�chogrammes

str1 = 'IFREMER - SonarScope : S�lectionnez les fichiers des �chogrammes';
str2 = 'IFREMER - SonarScope : Select the files of the echograms';
[flag, listFileNames, lastDir] = uiSelectFiles('Entete', Lang(str1,str2), 'ExtensionFiles', Ext, ...
    'RepDefaut', repImport, 'ChaineIncluse', ChaineIncluse);
if ~flag
    return
end
repImport = lastDir;

%% Nom du r�pertoire de sauvegarde des images d'�cho-int�gration

str1 = 'Nom du r�pertoire o� seront cr��s les fichiers d''�cho-int�gration';
str2 = 'Name of the destination folder for Echo Integration files';
[flag, nomDirOut] = my_uigetdir(repExport, Lang(str1,str2));
if ~flag
    return
end
repExport = nomDirOut;

%% Param�tres de l'�cho-int�gration

% OverSpecular = (typePolarEchogram == 1);
OverSpecular = (TypeEI == 1);
[flag, params_ZoneEI, StepDisplay] = WCD.Params.EchoIntegration('OverSpecular', OverSpecular);
if ~flag
    return
end

%% Tag suppl�mentaire dans le mom des images

[flag, Tag] = question_AddTagInOutputFile;
if ~flag
    return
end

%% Nom du fichiers .all

str1 = 'Mise en correspondance des fichiers WC avec les fichiers source .all ou .s7k';
str2 = 'Linking WC-DepthAcrossDist files with .all or .wcd files';
WorkInProgress(Lang(str1,str2));
ListeFicAll = cell(size(listFileNames));
nomDirAll = {};
LasDir = [];
N = length(listFileNames);
for k=1:N
    FichierAllTrouve = false;
    [~, ~, Ext] = fileparts(listFileNames{k});
    if strcmp(Ext, '.nc')
        ncID = netcdf.open(listFileNames{k});
        attGlobal = -1; % netcdf.getConstant('GLOBAL');
        SourceFileName = netcdf.getAtt(ncID, attGlobal, 'SourceFileName');
        netcdf.close(ncID);
    else
        [flag, DataRaw] = XMLBinUtils.readGrpData(listFileNames{k}, 'XMLOnly', 1); % Nouvel appel (pas valid�)
        SourceFileName = DataRaw.ExtractedFrom;
    end
    if ~flag
        continue
    end
    
    while ~FichierAllTrouve
        if exist(SourceFileName, 'file')
            FichierAllTrouve = true;
            ListeFicAll{k} = SourceFileName;
        else
            [~, nomFicAll, ExtAll] = fileparts(SourceFileName);
            for k2=1:length(nomDirAll)
                pppp = fullfile(nomDirAll{k2}, [nomFicAll ExtAll]);
                if exist(pppp, 'file')
                    FichierAllTrouve = true;
                    ListeFicAll{k} = pppp;
                    break
                end
            end
            
            if ~isempty(LasDir)
                pppp = fullfile(LasDir, [nomFicAll ExtAll]);
                if exist(pppp, 'file')
                    % FichierAllTrouve = true;
                    ListeFicAll{k} = pppp;
                    break
                end
            end
            
            if ~FichierAllTrouve
                [~, nomFicAll, ExtAll] = fileparts(SourceFileName);
                nomDir = fileparts(listFileNames{k});
                str1 = sprintf('S�lectionnez le r�pertoire qui contient le fichier .all (ou .s7k) correspondant au fichier "%s"', listFileNames{k});
                str2 = sprintf('Select the folder that contains the .all (or .s7k) file corresponding to "%s"', listFileNames{k});
                [flag, nomDirAll{end+1}] = my_uigetdir(nomDir, Lang(str1,str2)); %#ok<AGROW>
                if ~flag
                    return
                end
                LasDir = nomDirAll{end};
                SourceFileName = fullfile(nomDirAll{end}, [nomFicAll ExtAll]);
            end
        end
    end
end

%% Message //Tbx

if (StepDisplay == 0) && (N > 1)
    flag = messageParallelTbx(1);
    if ~flag
        return
    end
end
