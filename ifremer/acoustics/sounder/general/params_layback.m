function [flag, val] = params_layback

str1 = 'La longueur fil�e n''est pas renseign�e dans ce fichier, indiquez une valeur approximative.';
str2 = 'The layback is missing in this file, please give your own estimation.';
p = ClParametre('Name', Lang('Longueur ', 'Length'), ...
    'Unit', 'm',  'MinValue', 0, 'MaxValue', 100, 'Value', 20);
a = StyledParametreDialog('params', p, 'Title', Lang(str1,str2));
% a.Help = 'http://flotte.ifremer.fr/fleet/Presentation-of-the-fleet/On-board-software/SonarScope';
a.sizes = [0 -2 0 -2 0 -1 0 0];
a.openDialog;
flag = a.okPressedOut;
if ~flag
    val = [];
    return
end
val = a.getParamsValue;
