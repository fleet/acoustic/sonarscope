% Affichage de l'histogramme des types de datagrams des fichiers .all, .s7k, .xtf et .rdf
%
% Syntax
%   plot_histo_index(Code, histoTypeDatagram, texteTypeDatagram, ...)
%
% Input Arguments
%   Code              : Identifiants des datagrams
%   histoTypeDatagram : Nombre d'occurrence des datagrams
%   texteTypeDatagram : Noms des datagrams
%
% Name-Value Pair Arguments
%   nomFic : Nom du fichier
%
% Examples
%   Code = [10 22 55 88];
%   histoTypeDatagram = [44 77 33 88];
%   tabNbBytes = [44 77 33 88] * 10;
%   texteTypeDatagram = {'toto'; 'titi'; 'tutu'; 'tata'};
%   plot_histo_index(Code, histoTypeDatagram, texteTypeDatagram, tabNbBytes, 'Nom du fichier')
%
% See also Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function [h, hp, Fig] = plot_histo_index(Code, histoTypeDatagram, texteTypeDatagram, tabNbBytes, varargin)

Fig = figure('Position', [100 400 1400 500]);

hp = uipanel('Title', 'Histogram of #datagrams', 'FontSize', 12, ...
             'BackgroundColor', 'white',...
             'Position', [.02 .02 .33 .95]);
h(1) = axes('parent', hp);
bar(histoTypeDatagram); grid on;
h(1).XTick = 1:length(Code); 
h(1).XTickLabels = num2cell(Code);
h(1).XTickLabelRotation = 90;

if ~isempty(tabNbBytes)
    hp = uipanel('Title', 'Nb of bytes per datagram', 'FontSize', 12, ...
             'BackgroundColor', 'white',...
             'Position', [.32 .02 .33 .95], ...
             'Tag', 'BytesByType');
    h(2) = axes('parent', hp);
    bar(tabNbBytes); grid on;
    h(2).XTick = 1:length(Code); 
    h(2).XTickLabels = num2cell(Code);
    h(2).XTickLabelRotation = 90;
    linkaxes(h, 'x'); axis tight; drawnow;
end

hp = uipanel('Title', 'Legend', 'FontSize',12,...
             'BackgroundColor', 'white', ...
             'Position', [.65 .02 .33 .95]);

uicontrol(hp, 'Style','text', 'Units', 'normalized',...
        'Position', [.1 .1 .8 .8],...
        'String', texteTypeDatagram, 'HorizontalAlignment', 'left');
if nargin == 5
    [~, nomFic] = fileparts(varargin{1});
    title(h(1), nomFic, 'Interpreter', 'none')
    title(h(2), nomFic, 'Interpreter', 'none')
else
    title(h(1), 'Synthese')
    title(h(2), 'Synthese')
end
