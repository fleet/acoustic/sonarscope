% Affichage de l'histogramme des types de datagrams des fichiers .all, .s7k, .xtf et .rdf
%
% Syntax
%   plot_histo_index_tab(Code, histoTypeDatagram, texteTypeDatagram, ...)
%
% Input Arguments
%   Code              : Identifiants des datagrams
%   histoTypeDatagram : Nombre d'occurrence des datagrams
%   texteTypeDatagram : Noms des datagrams
%
% Name-Value Pair Arguments
%   nomFic : Nom du fichier
%
% Examples
%   Code = [10 22 55 88];
%   histoTypeDatagram = [44 77 33 88];
%   texteTypeDatagram = {'toto'; 'titi'; 'tutu'; 'tata'};
%   tabNbBytes = [44 77 33 88] * 10;
%   plot_histo_index_tab(Code, histoTypeDatagram, texteTypeDatagram, tabNbBytes, 'NomDuFichier' )
%
% See also Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function plot_histo_index_tab(Code, histoTypeDatagram, texteTypeDatagram, tabNbBytes, nomFic, varargin)

[varargin, Fig] = getPropertyValue(varargin, 'Fig', []);

if isempty(Fig)
    figure('Position', [100 400 1400 500]);
else
    figure(Fig)
    Tag = get(Fig, 'Tag');
    if isempty(Tag)
        set(Fig, 'Position', [100 400 1400 500]);
    end
end
set(Fig, 'Name', nomFic, 'Tag', 'Histogramme Tuples HAC')

x = 1:length(Code);

hp = uipanel('Title', 'Histogram', 'FontSize', 12, ...
    'BackgroundColor', 'white',...
    'Position', [.02 .02 .33 .95]);
h(1) = axes('parent', hp);
bar(x, histoTypeDatagram);
set(h(1), 'XTick', x, 'XTickLabel', num2cell(Code(:)))
% TODO : trouver comment on met les cha�nes de caract�re � la verticale

hp = uipanel('Title', 'Nb of bytes', 'FontSize', 12, ...
    'BackgroundColor', 'white',...
    'Position', [.32 .02 .33 .95]);
h(2) = axes('parent', hp);
bar(x, tabNbBytes);
set(h(2), 'XTick', x, 'XTickLabel', num2cell(Code(:)))

if my_verLessThanR2014b % R2014b
    % Rotation des Labels de Ticks.
    flag = rotateTicksLabel(h, 'AngleRotation', 45); %#ok<NASGU>
else
    % Utilisation de la rotation native.
    set(h(:), 'XTickLabelRotation', 45);
end
% linkaxes(h, 'x'); axis tight; drawnow;

hp = uipanel('Title', 'Legend', 'FontSize',12,...
    'BackgroundColor', 'white', ...
    'Position', [.65 .02 .33 .95]);

uicontrol(hp, 'Style','text', 'Units', 'normalized',...
    'Position', [.1 .1 .8 .8],...
    'String', texteTypeDatagram, 'HorizontalAlignment', 'left');

if nargin == 4
    [~, nomFic] = fileparts(varargin{1});
    title(h(1), nomFic, 'Interpreter', 'none')
    title(h(2), nomFic, 'Interpreter', 'none')
else
    title(h(1), 'Synthese')
    title(h(2), 'Synthese')
end

%% Garbage Collector de Java

java.lang.System.gc

