function flag = process_ErMapperImportNav(listeFic, NomFicNav, FiltreNav, OrdreButter, Wc, CalculHeading)

if ischar(NomFicNav)
    NomFicNav = {NomFicNav};
end

% n = cl_car_nav(NomFicNav{1});
% plot(n)

for k=1:length(listeFic)    
%     [flag, b] = import_ermapperNew(cl_image, listeFic{k}); Fonction d�plac�e dans D:\IfremerToolboxFonctionsObsoletes\ifremer\matlab\images\@cl_image
    [flag, b] = cl_image.import_ermapper(listeFic{k});
    if ~flag
        continue
    end
    
    b = sonar_import_nav(b, NomFicNav{1});

    if FiltreNav == 1
        [b, flag] = filtrageSignal(b, 'SonarFishLatitude',  OrdreButter, Wc);
        if ~flag
            return
        end
        [b, flag] = filtrageSignal(b, 'SonarFishLongitude', OrdreButter, Wc);
        if ~flag
            return
        end
    end

    if CalculHeading == 1
        b = sonar_ComputeHeading(b);
    end
    
    [nomDirRacine, nomDir] = fileparts(listeFic{k});
    nomFicXml = fullfile(nomDirRacine, [nomDir '.xml']);
    
    if exist(nomFicXml, 'file')           
        data = {'FishLatitude', 'FishLongitude'};        
        sonar = get(b,'Sonar');
        for ksig=1:length(data)
            Info.Signals(ksig).Name       = data{ksig};
            Info.Signals(ksig).Dimensions = 'nbRows, nbSounders';
            Info.Signals(ksig).Storage    = 'double';
            Info.Signals(ksig).Unit       = 'deg';
            Info.Signals(ksig).FileName   = fullfile(nomDir,[data{ksig}, '.bin']);
            Info.Signals(ksig).Tag        = verifKeyWord(data{ksig});
                        
            flag = writeSignal(nomDirRacine, Info.Signals(ksig), sonar.(Info.Signals(ksig).Name));
            if ~flag
                return
            end
        end
    else        
        flag = export_ermapper_sigV(b, listeFic{k});
        
        clear b
        if ~flag
            return
        end
    end
%     SonarScope(nomFicErs)
end
flag = 1;
