function flag = process_GroupEchograms(listeFic, nomFicOut)

%% Cr�ation du r�pertoire des WC regroup�s

[nomDir, nomFic] = fileparts(nomFicOut);
if ~exist(nomDir, 'dir')
    [flag, mes] = mkdir(nomDir);
    if ~flag
        my_warndlg(mes, 1);
        return
    end
end
nomDirSignaux = fullfile(nomDir, nomFic);

%% Lecture des XML de tous les fichiers d'entr�e

nbFic = length(listeFic);
nbPings = 0;
for kFic=1:nbFic
    XML(kFic) = xml_read(listeFic{kFic}); %#ok<AGROW>
    nbPings = nbPings + XML(kFic).Dimensions.nbPings;
end


%% Cr�ation de la structure XML du fichier de sortie

XMLOut = XML(1);
XMLOut.Name          = nomFic;
XMLOut.ExtractedFrom = 'Groupement of files';
XMLOut.nomDirExport  = nomDir;
XMLOut.Dimensions.nbPings = nbPings;
nbSignals = length(XMLOut.Signals);
for k=1:nbSignals
    XMLOut.Signals(k).FileName = fullfile(nomFic, [XMLOut.Signals(k).Name '.bin']);
end

%% Ecriture du r�pertoire des signaux

if ~exist(nomDirSignaux, 'dir')
    [flag, mes] = mkdir(nomDirSignaux);
    if ~flag
        my_warndlg(mes, 1);
        return
    end
end

%% Ecriture du fichier de sortie

xml_write(nomFicOut, XMLOut, 'PolarEchograms');
flag = exist(nomFicOut, 'file');
if ~flag
    messageErreurFichier(nomFicOut, 'WriteFailure');
    return
end

%% Tri des fichiers en fonction de l'heure

sub = 1:nbFic; % TODO : � d�velopper
XML = XML(sub);

%% Ecriture des fichiers binaires

str1 = 'Concat�nation des signaux en cours';
str2 = 'Appending signals';
hw = create_waitbar(Lang(str1,str2), 'N', nbSignals);
for k=1:nbSignals
    my_waitbar(k, nbSignals, hw)
    XOut = [];
    for kFic=1:nbFic
        nomDirIn = fileparts(listeFic{kFic});
        [flag, X] = readSignal(nomDirIn, XML(kFic).Signals(k), XML(kFic).Dimensions.nbPings);
        if ~flag
            return
        end
        XOut = [XOut; X]; %#ok<AGROW>
    end
    
    if strcmp(XMLOut.Signals(k).Name, 'iPing')
        flag = writeSignal(nomDir, XMLOut.Signals(k), 1:nbPings);
    else
        flag = writeSignal(nomDir, XMLOut.Signals(k), XOut);
    end
    if ~flag
        return
    end
end
my_close(hw, 'MsgEnd')

%% D�placement et renommage des images

kOut = 0;
str1 = 'Cr�ation des fichiers WC en cours';
str2 = 'Creating XC files';
hw = create_waitbar(Lang(str1,str2), 'N', nbFic);
for kFic=1:nbFic
    my_waitbar(kFic, nbFic, hw)
    
    [nomDirIn, nomDirSeul] = fileparts(listeFic{kFic});
    for k=1:XML(kFic).Dimensions.nbPings
        kDir   = floor(k / 100);
        kImage = k;
        
        % Nom du r�pertoire
        nomDirImage = fullfile(nomDirIn, nomDirSeul, num2str(kDir, '%03d'));
        if ~exist(nomDirImage, 'dir')
            messageErreurFichier(nomDirImage, 'ReadFailure');
            break
        end
        
        % Nom de l'image d'entr�e
        nomImageIn  = fullfile(nomDirImage, [num2str(kImage, '%05d') '.png']);
        if ~exist(nomDirImage, 'dir')
            messageErreurFichier(nomImageIn, 'ReadFailure');
            break
        end
        
        % Nom de l'image de sortie
        kOut = kOut + 1;
        kDirOut   = floor(kOut / 100);
        kImageOut = kOut;
        
        % Nom du r�pertoire de sortie
        nomDirImageOut = fullfile(nomDirSignaux, num2str(kDirOut, '%03d'));
        if ~exist(nomDirImageOut, 'dir')
            [flag, mes] = mkdir(nomDirImageOut);
            if ~flag
                my_warndlg(mes, 1);
                return
            end
        end
        
        % Nom de l'image d'entr�e
        nomImageOut  = fullfile(nomDirImageOut, [num2str(kImageOut, '%05d') '.png']);
        [flag, msg] = copyfile(nomImageIn, nomImageOut, 'f');
        if ~flag
            my_warndlg(msg, 1);
            return
        end
    end
end
my_close(hw, 'MsgEnd')

