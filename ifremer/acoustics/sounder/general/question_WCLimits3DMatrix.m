% [flag, minAcrossDist, maxAcrossDist, minDepth] = question_WCLimits3DMatrix(-100, 200)
% [flag, minAcrossDist, maxAcrossDist, minDepth] = question_WCLimits3DMatrix(-100, 200, 'minDepth', -100)

function [flag, minAcrossDist, maxAcrossDist, minDepth] = question_WCLimits3DMatrix(minAcrossDist, maxAcrossDist, varargin)

[varargin, minMinAcrossDist] = getPropertyValue(varargin, 'minMinAcrossDist', -25000);
[varargin, maxMaxAcrossDist] = getPropertyValue(varargin, 'maxMaxAcrossDist',  25000);
[varargin, minDepth]         = getPropertyValue(varargin, 'minDepth',          []); %#ok<ASGLU>

str1 = 'Distance latérale minimale';
str2 = 'Minimum across distance (Port)';
p = ClParametre('Name', Lang(str1,str2), ...
    'Unit', 'm', 'Value', minAcrossDist, 'MinValue', minMinAcrossDist, 'MaxValue', -1);

str1 = 'Distance latérale maximale';
str2 = 'Maximum across distance (Starbord)';
p(2) = ClParametre('Name', Lang(str1,str2), ...
    'Unit', 'm', 'Value', maxAcrossDist, 'MinValue', 1, 'MaxValue', maxMaxAcrossDist);

if ~isempty(minDepth)
    str1 = 'Hauteur maximale';
    str2 = 'Maximum height';
    p(3) = ClParametre('Name', Lang(str1,str2), ...
        'Unit', 'm', 'Value', -minDepth, 'MinValue', 1, 'MaxValue', 12000);
end

str1 = 'Délimitations du Cube en hauteur et en largeur';
str2 = 'Limits of the 3D volume';
a = StyledSimpleParametreDialog('params', p, 'Title', Lang(str1,str2));
a.openDialog;
flag = a.okPressedOut;
if ~flag
    return
end
val = a.getParamsValue;

minAcrossDist = val(1);
maxAcrossDist = val(2);
if ~isempty(minDepth)
    minDepth  = -val(3);
end
