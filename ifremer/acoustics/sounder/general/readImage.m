function [flag, Image] = readImage(nomDir, InfoImage, nbPings, nbBeams, varargin)

[varargin, flagMemmapfile] = getPropertyValue(varargin, 'Memmapfile', 0); %#ok<ASGLU>

nomFic = fullfile(nomDir, InfoImage.FileName);

if flagMemmapfile && ~strcmpi(InfoImage.Unit, 'days since JC')
    Image = cl_memmapfile('FileName', nomFic, 'FileCreation', 0, ...
        'Format', InfoImage.Storage, 'Size', [nbPings nbBeams], ...
        'ErMapper', 1);%, ...
    %                 'ValNaN', NaN);%);
    flag = 1;
    return
end



fid = fopen(nomFic, 'r');
if fid == -1
    messageErreurFichier(nomFic);
    flag = 0;
    return
end

[Image, n] = fread(fid, [InfoImage.Storage '=>' InfoImage.Storage]);
if n ~= (nbPings*nbBeams)
    messageErreurFichier(nomFic);
    flag = 0;
    fclose(fid);
    return
end
fclose(fid);

% if strcmp(InfoImage.Name, 'Time')
if strcmpi(InfoImage.Unit, 'days since JC')
    Image = cl_time('timeMat', Image);
end

Image = reshape(Image, nbBeams, nbPings);

Image = Image';
if isfield(InfoImage, 'Direction') && ~isempty(InfoImage.Direction) && ~strcmp(InfoImage.Direction, 'FirstValue=FirstPing')
    Image = flipud(Image);
end

flag = 1;
