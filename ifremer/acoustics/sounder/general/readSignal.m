function [flag, Signal] = readSignal(nomDir, InfoSignal, NbSamples, varargin)

[varargin, nbColumns]      = getPropertyValue(varargin, 'nbColumns',  1);
[varargin, flagMemmapfile] = getPropertyValue(varargin, 'Memmapfile', 0); %#ok<ASGLU>

nomFic = fullfile(nomDir, InfoSignal.FileName); % Bug si InfoSignal a �t� lu � partir d'un fichier netcdf. En principe on ne devrait pas rencontrer ce cas

InfoSignal.Storage = getMatlabDataType(InfoSignal.Storage);

if flagMemmapfile && ~(isfield(InfoSignal, 'Unit') && strcmpi(InfoSignal.Unit, 'days since JC'))
    %          Signal = cl_memmapfile('FileName', nomFic, 'FileCreation', 0, ...
    %                 'Format', InfoSignal.Storage, 'Size', [nbColumns NbSamples]);%, ...
    Signal = cl_memmapfile('FileName', nomFic, 'FileCreation', 0, ...
        'Format', InfoSignal.Storage, 'Size', [NbSamples nbColumns]);%, ...
    flag = 1;
    return
end

fid = fopen(nomFic, 'r');
if fid == -1
    if ~strcmp(InfoSignal.Name, 'Entries') % Exception pour SebedImage : � r�soudre plus intelligemment
        messageErreurFichier(nomFic);
        Signal = [];
        flag = 0;
        return
    end
end

[Signal, n] = fread(fid, [InfoSignal.Storage '=>' InfoSignal.Storage]);

if isempty(NbSamples)
    fclose(fid);
    flag = 1;
    return
end

% if n ~= NbSamples
if n < (NbSamples*nbColumns)
    messageErreurFichier(nomFic, 'ReadFailure');
    flag = 0;
    fclose(fid);
    return
end
% if n ~= (NbSamples*nbColumns)
%     if ~strcmp(InfoSignal.Name, 'Entries') % Exception pour SebedImage : � r�soudre plus intelligemment
%         messageErreurFichier(nomFic);
%     end
% end
fclose(fid);
flag = 1;

% if isfield(InfoSignal, 'ScaleFactor') && ~isempty(InfoSignal.ScaleFactor) && (InfoSignal.ScaleFactor ~= 1)
%     Signal = Signal * InfoSignal.ScaleFactor;
% end
% if isfield(InfoSignal, 'AddOffset') && ~isempty(InfoSignal.AddOffset) && (InfoSignal.AddOffset ~= 0)
%     Signal = Signal + InfoSignal.AddOffset;
% end

% if strcmp(InfoSignal.Name, 'Time')
if isfield(InfoSignal, 'Unit') && strcmpi(InfoSignal.Unit, 'days since JC')
    if nbColumns ~= 1
        Signal = reshape(Signal, [NbSamples nbColumns]);
    end
    
    % En attendant la r�vision g�n�rale des signaux
    Signal = Signal(:,1);
    
    Signal = cl_time('timeMat', Signal);
else
    try
        Signal = reshape(Signal, NbSamples, nbColumns);
    catch ME
        % TODO : Test rajout� pour fichiers HAC : voir si on peut d�placer
        % ce morceau dans le try
        if strcmp(InfoSignal.Storage, 'char') && isfield(InfoSignal, 'NbElem')
            pppp = reshape(Signal, [InfoSignal.NbElem, NbSamples]);
            Signal = my_transpose(pppp);
        else
            fprintf('\n--------------------------------------------\nOn ne devrait pas passer par l� (readSignal)\nFilename : %s\nVariable : %s\nNbSamples : %d nbColumns : %d  length(signal) : %d\n%s\n', ...
                nomFic, InfoSignal.Name, NbSamples, nbColumns, length(Signal), ME.message);
        end
    end
end
