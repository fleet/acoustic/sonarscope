function [flag, Signal] = readSignalAll(nomDir, InfoSignal, NbSamples, varargin)

[varargin, nbColumns] = getPropertyValue(varargin, 'nbColumns', 1);
% % % if nbColumns ~= 1
% % %     my_warndlg('nbColumns trouv� ici', 1);
% % % end

nomFic = fullfile(nomDir, InfoSignal.FileName);

[varargin, flagMemmapfile] = getPropertyValue(varargin, 'Memmapfile', 0); %#ok<ASGLU>

if flagMemmapfile && ~(isfield(InfoSignal, 'Unit') && strcmpi(InfoSignal.Unit, 'days since JC'))
    % {
    Signal = cl_memmapfile('FileName', nomFic, 'FileCreation', 0, ...
        'Format', InfoSignal.Storage, 'Size', [nbColumns NbSamples]);%, ...
    %                 'ValNaN', NaN);%, 'ErMapper', 1);
    
    flag = 1;
    return
    % }
end

fid = fopen(nomFic, 'r');
if fid == -1
    if ~strcmp(InfoSignal.Name, 'Entries') % Exception pour SebedImage : � r�soudre plus intelligemment
        messageErreurFichier(nomFic);
        flag = 0;
        return
    end
end

[Signal, n] = fread(fid, [InfoSignal.Storage '=>' InfoSignal.Storage]);

if isempty(NbSamples)
    fclose(fid);
    flag = 1;
    return
end

% if n ~= NbSamples
if n < (NbSamples*nbColumns)
    messageErreurFichier(nomFic, 'ReadFailure');
    flag = 0;
    fclose(fid);
    return
end
% if n ~= (NbSamples*nbColumns)
%     if ~strcmp(InfoSignal.Name, 'Entries') % Exception pour SebedImage : � r�soudre plus intelligemment
%         messageErreurFichier(nomFic);
%     end
% end
fclose(fid);
flag = 1;

% if isfield(InfoSignal, 'ScaleFactor') && ~isempty(InfoSignal.ScaleFactor) && (InfoSignal.ScaleFactor ~= 1)
%     Signal = Signal * InfoSignal.ScaleFactor;
% end
% if isfield(InfoSignal, 'AddOffset') && ~isempty(InfoSignal.AddOffset) && (InfoSignal.AddOffset ~= 0)
%     Signal = Signal + InfoSignal.AddOffset;
% end

% if strcmp(InfoSignal.Name, 'Time')
if isfield(InfoSignal, 'Unit') && strcmpi(InfoSignal.Unit, 'days since JC')
    if nbColumns ~= 1
        Signal = reshape(Signal, [NbSamples nbColumns]);
    end
    
    % En attendant la r�vision g�n�rale des signaux
    Signal = Signal(:,1);
    
    Signal = cl_time('timeMat', Signal);
else
    try
        Signal = reshape(Signal, nbColumns, NbSamples);
        Signal = my_transpose(Signal);
    catch %#ok<CTCH>
        % TODO : Test rajout� pour fichiers HAC : voir si on peut d�placer
        % ce morceau dans le try
        if strcmp(InfoSignal.Storage, 'char') && isfield(InfoSignal, 'NbElem')
            pppp = reshape(Signal, [InfoSignal.NbElem, NbSamples]);
            Signal = my_transpose(pppp);
        else
            'On ne devrait pas passer par l� (readSignalAll)'
        end
    end
end
