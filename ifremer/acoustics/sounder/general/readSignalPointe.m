function [flag, Signal] = readSignalPointe(nomDir, Signals, DataCommun, InfoSignalPointe, NbSamples, varargin)

[varargin, flagMemmapfile] = getPropertyValue(varargin, 'Memmapfile', 0); %#ok<ASGLU>

flag   = 0;
Signal = [];

k = [];
for i=1:length(Signals)
    if strcmp(Signals(i).Name, ['PointerOn' InfoSignalPointe.Storage])
        k = i;
        break
    end
end
if isempty(k)
    str = sprintf('Signal %s not found.', ['PointerOn' InfoSignalPointe.Storage]);
    my_warndlg(str, 0, 'Tag', 'PointerOnNotFound');
    return
end
[flag, sub] = readSignal(nomDir, Signals(k), NbSamples);

k = [];
for i=1:length(DataCommun.Signals)
    if strcmp(InfoSignalPointe.Name, DataCommun.Signals(i).Name)
        k = i;
        break
    end
end
if isempty(k)
    str = sprintf('Signal %s not found.', InfoSignalPointe.Name);
    my_warndlg(str, 0, 'Tag', 'PointerOnNotFound');
    return
end

[flag, Signal] = readSignal(nomDir, DataCommun.Signals(k), NbSamples, 'Memmapfile', flagMemmapfile);


Signal = Signal(sub);
