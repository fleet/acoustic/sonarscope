% D�codage d'une trame NMEA de type $AGHDT
%
% Syntax
%    [flag, HeadingVessel] = read_NMEA_AGHDT(str)
%
% Input Arguments
%   str : Trame NMEA
%
% Output Arguments
%   flag          : 0/1
%   HeadingVessel : True vessel track in the vessel frame
%
% Examples
%   str = '$AGHDT,111.7,T'
%    [flag, HeadingVessel] = read_NMEA_AGHDT(str)
%
% See also Authors
% References : http://aprs.gids.nl/nmea/ & http://pages.uoregon.edu/drt/MGL0910_sup/html_ref/formats/posmv.html
% Authors : JMA
%--------------------------------------------------------------------------

function [flag, HeadingVessel] = read_NMEA_AGHDT(str)
    
HeadingVessel = [];

mots = strsplit(str, ',');
flag = (length(mots) >= 3);
if ~flag
    return
end

try
    HeadingVessel = str2double(mots{2});
catch ME %#ok<NASGU>
    flag = 0;
    return
end