% D�codage d'une trame NMEA de type $INHDT
%
% Syntax
%    [flag, HeadingVessel] = read_NMEA_INHDT(str)
%
% Input Arguments
%   str : Trame NMEA
%
% Output Arguments
%   flag         : 0/1
%   HeadingVessel : True vessel track in the vessel frame
%
% Examples
%   str = '$INHDT,274.64,T,272.57,M,0.1,N,0.1,K,D'
%    [flag, HeadingVessel] = read_NMEA_INHDT(str)
%
% See also Authors
% References : http://aprs.gids.nl/nmea/ & http://pages.uoregon.edu/drt/MGL0910_sup/html_ref/formats/posmv.html
% Authors : JMA
%--------------------------------------------------------------------------

function [flag, HeadingVessel] = read_NMEA_INHDT(str)
    
HeadingVessel = [];

mots = strsplit(str, ',');
flag = (length(mots) >= 3);
if ~flag
    return
end

try
    HeadingVessel = str2double(mots{2});
catch ME %#ok<NASGU>
    flag = 0;
    return
end