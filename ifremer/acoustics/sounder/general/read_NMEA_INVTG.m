% D�codage d'une trame NMEA de type $INVTG
%
% Syntax
%    [flag, Heading, SpeedInKnots] = read_NMEA_INVTG(str)
%
% Input Arguments
%   str : Trame NMEA
%
% Output Arguments
%   flag         : 0/1
%   Heading      : True vessel track in the vessel frame
%   SpeedInKnots : Speed in the vessel frame 
%
% Examples
%   str = '$INVTG,274.64,T,272.57,M,0.1,N,0.1,K,D'
%    [flag, Heading, SpeedInKnots] = read_NMEA_INVTG(str)
%
% See also Authors
% References : http://aprs.gids.nl/nmea/ & http://pages.uoregon.edu/drt/MGL0910_sup/html_ref/formats/posmv.html
% Authors : JMA
%--------------------------------------------------------------------------

function [flag, HeadingTrack, SpeedInKnots] = read_NMEA_INVTG(str)
    
mots = strsplit(str, ',');
flag = (length(mots) >= 10);
if ~flag
    HeadingTrack = [];
    SpeedInKnots = [];
    return
end

try
    HeadingTrack = str2double(mots{2});
    SpeedInKnots = str2double(mots{6});
catch ME %#ok<NASGU>
    flag = 0;
    HeadingTrack = [];
    SpeedInKnots = [];
    return
end