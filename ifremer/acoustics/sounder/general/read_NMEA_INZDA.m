% D�codage d'une trame NMEA de type $INZDA
%
% Syntax
%   [flag, Time] = read_NMEA_INZDA(str)
%
% Input Arguments
%   str : Trame NMEA
%
% Output Arguments
%   flag : 0/1
%   Time : Temps (origine Matlab)
%
% Examples
%   str = '$INZDA,154039.53,05,05,2012,,
%   [flag, Time] = read_NMEA_INZDA(str)
%
% See also Authors
% References : http://aprs.gids.nl/nmea/ & http://pages.uoregon.edu/drt/MGL0910_sup/html_ref/formats/posmv.html
% Authors : JMA
%--------------------------------------------------------------------------

function [flag, Time] = read_NMEA_INZDA(str)
    
mots = strsplit(str, ',');
flag = (length(mots) >= 5);
if ~flag
    Time = [];
    return
end

try
    HH = str2double(mots{2}(1:2));
    MM = str2double(mots{2}(3:4));
    SS = str2double(mots{2}(5:end));
    Heure = 1000 * (SS + MM * 60 + HH * 3600);
    
    Jour  = str2double(mots{3});
    Mois  = str2double(mots{4});
    Annee = str2double(mots{5});
    Date = dayJma2Ifr(Jour, Mois, Annee);
    
    t = cl_time('timeIfr', Date, Heure);
    Time = t.timeMat;
    
catch ME %#ok<NASGU>
    flag = 0;
    Time = [];
    return    
end
