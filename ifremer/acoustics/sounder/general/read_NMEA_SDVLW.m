% TODO : pas termin� car pas trouv� ce que �a repr�sente
% D�codage d'une trame NMEA de type $SDVLW
%
% Syntax
%   [flag] = read_NMEA_SDVLW(str)
%
% Input Arguments
%   str : Trame NMEA
%
% Output Arguments
%   flag : 0/1
%
% Examples
%   str = '$SDVLW,9.222,N,9.222,N'
%   [flag] = read_NMEA_SDVLW(str)
%
% See also Authors
% References : http://aprs.gids.nl/nmea/ & http://pages.uoregon.edu/drt/MGL0910_sup/html_ref/formats/posmv.html
% Authors : JMA
%--------------------------------------------------------------------------

function [flag] = read_NMEA_SDVLW(str)
    
mots = strsplit(str, ',');
flag = (length(mots) >= 5);
if ~flag
    return
end

try
    x1 = str2double(mots{2});
    c1 = mots{3};
    x2 = str2double(mots{4});
    c2 = mots{5};
    
catch ME %#ok<NASGU>
    flag = 0;
    return
end