% D�codage d'une trame NMEA de position GPS
%
% Syntax
%   [flag, Heure, Lat, Lon, Height] = read_NMEA_UNH(str)
%
% Input Arguments
%   str : Trame NMEA
%
% Output Arguments
%   Heure  : Nombre de millisecondes depuis minuit
%   Lat    : Latitude (deg)
%   Lon    : Longitude (deg)
%   Height : Hauteur (m).
%
% Examples
%   str = '$GPGGA,035344.00,0114.74433,N,10352.78390,E,5,10,0.9,13.21,M,0.00,M,12,0001*65'
%   [flag, Heure, Lat, Lon, Height] = read_NMEA_UNH(str)
%
% See also JMA Authors
% References : http://aprs.gids.nl/nmea/
% Authors : JMA
%--------------------------------------------------------------------------

function [flag, Heure, Lat, Lon, Height] = read_NMEA_UNH(str)

Heure               = 0;
Height              = 0;
mots = strsplit(str, {'�'; ' '});

% HH = str2double(mots{2}(1:2));
% MM = str2double(mots{2}(3:4));
% SS = str2double(mots{2}(5:end));
% Heure = 1000 * (SS + MM * 60 + HH * 3600);
%
% t = cl_time('timeIfr', 0, Heure);
% t = t.timeMat;

Lat = str2double(mots{2});
Lon = str2double(mots{4});

% Height =  str2double(mots{10});

flag = 1;
