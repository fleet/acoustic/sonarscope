% nomFic = 'Y:\private\SPFE\TEST ABS COEFFICIENT\copco1516.tab';
% [flag, Odas] = read_ODAS(nomFic);
% FigUtils.createSScFigure;
% h(1) = subplot(3,1,1); PlotUtils.createSScPlot(Odas.Heading); grid on; title('Heading')
% h(2) = subplot(3,1,2); PlotUtils.createSScPlot(Odas.Temp); grid on; title('Temp')
% h(3) = subplot(3,1,3); PlotUtils.createSScPlot(Odas.Sal); grid on; title('Sal')
% linkaxes(h, 'x'); axis tight; drawnow;
% [Fig1, hAxes] = plot_navigation_Figure([]);
%  plot_navigation(nomFic, Fig1, Odas.Lon, Odas.Lat, Odas.Time.timeMat, [])
%
% [flag, Odas] = read_ODAS(nomFic, 'CleanData', 1);
% FigUtils.createSScFigure;
% h(1) = subplot(3,1,1); PlotUtils.createSScPlot(Odas.Heading); grid on; title('Heading')
% h(2) = subplot(3,1,2); PlotUtils.createSScPlot(Odas.Temp); grid on; title('Temp')
% h(3) = subplot(3,1,3); PlotUtils.createSScPlot(Odas.Sal); grid on; title('Sal')
% linkaxes(h, 'x'); axis tight; drawnow;
% [Fig1, hAxes] = plot_navigation_Figure([]);
%  plot_navigation(nomFic, Fig1, Odas.Lon, Odas.Lat, Odas.Time.timeMat, [])

function [flag, Odas] = read_ODAS(nomFicOdas, varargin)

persistent persistent_FiltreTemp persistent_FiltreSal

[varargin, CleanData] = getPropertyValue(varargin, 'CleanData', 0); %#ok<ASGLU>

Odas = [];

[nomDir, nomFic, ext] = fileparts(nomFicOdas);
nomFicMat = fullfile(nomDir, [nomFic '_' ext(2:end) '.mat']);
if exist(nomFicMat, 'file') && checkFileCreation(nomFicMat, '24/08/2015') ...
        && checkFilePosterior(nomFicMat, nomFicOdas)
    Odas = loadmat(nomFicMat, 'nomVar', 'Odas');
%     str1 = sprintf('Un fichier\n"%s"\npost�rtieur au fichier\n"%s"\na �t� trouv�, il est lu directement (pas de nettoyage propos�).', nomFicMat, nomFicOdas);
%     str2 = sprintf('"%s"\nolder than\n"%s"\nwas found, it is read automatically (no data cleaning is proposed).', nomFicMat, nomFicOdas);
%     my_warndlg(Lang(str1,str2), 0, 'Tag', 'FichierMareeMatTrouve', 'TimeDelay', 10);
    flag = 1;
    return
else
    
    %% Lecture du fichier
    
    [flag, nbHeaderLines, nbWordsExpected] = estimateNbHeaderLinesInAnASCIIFile(nomFicOdas, 100);
    if ~flag
        return
    end
    Format = '';
    for k=1:nbWordsExpected
        Format = [Format '%s']; %#ok<AGROW>
    end
    
    fid = fopen(nomFicOdas, 'r');
    data = textscan(fid, Format, 'HeaderLines', nbHeaderLines);
    fclose(fid);
    
    %% D�codage de la date et de l'heure
    
    DIfr = dayStr2Ifr(data{1});
    hIfr = hourStr2Ifr(data{2});
    if isempty(DIfr) || isempty(hIfr)
        flag = 0;
        return
    end
    
    %% D�codage de  la navigation
    
    Si = strcmpi(data{3}, 'N');
    De = str2double(data{4});
    Mi = str2double(data{5});
    Lat = De + Mi / 60;
    Lat(~Si) = -Lat(~Si);
    
    Si = strcmpi(data{6}, 'E');
    De = str2double(data{7});
    Mi = str2double(data{8});
    Lon = De + Mi / 60;
    Lon(~Si) = -Lon(~Si);
    
    %% D�codage du cap, de la temp�rature et de la salinit�
    
    Heading = str2double(data{10});
    Temp    = str2double(data{11});
    Sal     = str2double(data{12});
    
    sub = isnan(Heading) | isnan(Temp) | isnan(Sal) | isnan(DIfr) | isnan(hIfr) | isnan(Lat) | isnan(Lon);
    if all(sub)
        flag = 0;
        return
    end
    Heading(sub) = [];
    Temp(sub)    = [];
    Sal(sub)     = [];
    DIfr(sub)    = [];
    hIfr(sub)    = [];
    Lat(sub)     = [];
    Lon(sub)     = [];
    
    Time = cl_time('timeIfr', DIfr, hIfr);
    
    Odas.Time = Time;
    Odas.Lat = Lat;
    Odas.Lon = Lon;
    Odas.Heading = Heading;
    Odas.Temp = Temp;
    Odas.Sal = Sal;
end

%% Nettoyage

if CleanData
    if isempty(persistent_FiltreTemp)
        FiltreTemp.Type  = 1;
        FiltreTemp.Ordre = 2;
        FiltreTemp.Wc    = 0.1;
    else
        FiltreTemp = persistent_FiltreTemp;
    end
    
    %     T = Time.timeMat;
    [Temp, FiltreTemp] = controleSignalNEW('Temp', Temp, 'Filtre', FiltreTemp);
    persistent_FiltreTemp = FiltreTemp;
    
    if isempty(persistent_FiltreSal)
        FiltreSal = persistent_FiltreTemp;
    else
        FiltreSal = persistent_FiltreSal;
    end
    
    [Sal, FiltreSal] = controleSignalNEW('Sal', Sal, 'Filtre', FiltreSal);
    persistent_FiltreSal = FiltreSal;
    
    Odas.Time    = Time;
    Odas.Lat     = Lat;
    Odas.Lon     = Lon;
    Odas.Heading = Heading;
    Odas.Temp    = Temp;
    Odas.Sal     = Sal;
end

save(nomFicMat, 'nomFic', 'Odas')
