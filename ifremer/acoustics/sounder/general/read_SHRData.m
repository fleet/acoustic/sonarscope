%{
listeNomFic = findFicInDir('H:\Common_Data_Set\Kongsberg\Ancillary_Data\SRH_Data', '*.srh');

color = 'brgkymc';
for k=1:length(listeNomFic)
    [flag, T, Heave] = read_SHRData(listeNomFic{k});
    if flag
        c = color(mod(k-1,length(color)) + 1);
        FigUtils.createSScFigure(1937); PlotUtils.createSScPlot(T, Heave, c); grid on; hold on;
    end
end
%}

% filename = 'H:\Common_Data_Set\Kongsberg\Ancillary_Data\SRH_Data\SRH_20140902_1409656653.srh';
% filename = 'H:\Common_Data_Set\Kongsberg\Ancillary_Data\SRH_Data\SRH_20140911_1410418447.srh';
% [flag, T, Heave] = read_SHRData(filename);
% figure; plot(T, Heave); grid on


function [flag, T, Heave] = read_SHRData(filename)

nbOc = sizeFic(filename);

fid = fopen(filename, 'r', 'l');
if fid == -1
    flag = 0;
    return
end
% x = fread(fid, nbOc, 'uint8');
% figure; plot(x(2:15:end)); grid

nbVal = nbOc / 15;
% nbVal = 2000;
Heave = NaN(1, nbVal);
seconds = NaN(1, nbVal);
millis  = NaN(1, nbVal);
Status  = NaN(1, nbVal);
for k=1:nbVal
    fread(fid, 2, 'uint8'); % Not described in the document
    
    Header = fread(fid, 1, 'uint8'); % Should be AA Hex -> 170 Dec
    if Header ~= 170
        nbVal = k - 1;
        break
    end
    Header = fread(fid, 1, 'uint8'); % Should be 52 Hex -> 82 Dec
    if Header ~= 82
        nbVal = k - 1;
        break
    end
    
    b1 = fread(fid, 1, 'uint8');
    b2 = fread(fid, 1, 'uint8');
    b3 = fread(fid, 1, 'uint8');
    b4 = fread(fid, 1, 'uint8');
    seconds(k) = b4*256^3 + b3*256^2 + b2*256 + b1;
    seconds(k) = b1*256^3 + b2*256^2 + b3*256 + b4;
    
    b1 = fread(fid, 1, 'uint8');
    b2 = fread(fid, 1, 'uint8');
    millis(k) = (b1*256 + b2) / 10000;
    
    b3 = fread(fid, 1, 'uint8');
    b4 = fread(fid, 1, 'int8');
    Heave(k) = (b4*256 + b3) / (100 * 100);
    
    Status(k) = fread(fid, 1, 'uint8');
    checksum  = fread(fid, 1, 'uint16'); %#ok<NASGU>
end
fclose(fid);

if nbVal == 0
    flag = 0;
else
    sub = 1:nbVal;
    Heave   = Heave(sub);
    seconds = seconds(sub);
    millis  = millis(sub);
    Status  = Status(sub);
    flag = 1;
end

sub = (Status == 0);
Heave   = Heave(sub);
seconds = seconds(sub);
millis  = millis(sub);
Status  = Status(sub);

% figure; plot(Heave); grid on; title('Heave2')
% figure; plot(seconds, '.'); grid on; title('seconds')
% figure; plot(millis, '.'); grid on; title('millis')
% figure; plot(Status, '.'); grid on; title('Status')
% figure; plot(seconds+millis, '.'); grid on; title('seconds+millis')

d = dayStr2Ifr('01/01/1970');
T = cl_time('timeIfr', d+(seconds+millis)/(24*60*60), 0);

[~, filename] = fileparts(filename);
figure(7659);
subplot(4,1,1);  plot(Heave); grid on; title(sprintf('%d - %s', k, filename))
subplot(4,1,2);  plot(Status, '.'); grid on; title('Status');
subplot(4,1,3);  plot(T, Heave); grid on; title(sprintf('%d - %s', k, filename))
subplot(4,1,4);  plot(T, Status, '.'); grid on; title('Status');
pi

