% Rotation de l'angle des Ticks Label d'une s�rie de Handles d'axes.
% Syntax
%   rotateTicksLabel(hdl, ...)
%
% Input Arguments
%   hdl              : Ensemble de Handles d'axes
%
% Name-Value Pair Arguments
%   AngleRotation   : angle de rotation du tete
%
% Examples
%   TODO
%-------------------------------------------------------------------------------

function flag = rotateTicksLabel(hdl, varargin)

flag = 1;

[varargin, rot] = getPropertyValue(varargin, 'AngleRotation', 90); %#ok<ASGLU>

%% Rotation du code inspir� de :
% http://www.mathworks.com/matlabcentral/fileexchange/8722-rotate-tick-label
for i=1:size(hdl, 2)
    set(gcf, 'CurrentAxes', hdl(i));
    % get current tick labels
    a   = get(hdl(i),'XTickLabel');
    %get tick label positions
    b   = get(hdl(i),'XTick');
    c   = get(hdl(i),'YTick');
    % erase current tick labels from figure
    set(hdl(i),'XTickLabel',[]);
    % R�cup�ration des cha�nes des labels.
    % pppp    = num2str(b');
    if iscell(a)
        pppp = cell2str(a);
    else
        pppp = a;
    end
    if rot<180
        th      = text(b,repmat(c(1)-.1*(c(2)-c(1)),length(b),1),pppp,'HorizontalAlignment','right','rotation',rot);
    else
        th      = text(b,repmat(c(1)-.1*(c(2)-c(1)),length(b),1),pppp,'HorizontalAlignment','left','rotation',rot);
    end
end
