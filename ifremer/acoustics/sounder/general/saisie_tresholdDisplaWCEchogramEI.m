function [flag, thresholdValueRaw, thresholdValueComp] = saisie_tresholdDisplaWCEchogramEI(varargin)

persistent persistent_thresholdValueRaw persistent_thresholdValueComp

[varargin, thresholdValueRaw]  = getPropertyValue(varargin, 'thresholdValueRaw',  []);
[varargin, thresholdValueComp] = getPropertyValue(varargin, 'thresholdValueComp', []); %#ok<ASGLU>

if isempty(thresholdValueRaw)
    if isempty(persistent_thresholdValueRaw)
        thresholdValueRaw  = -Inf;
    else
        thresholdValueRaw  = persistent_thresholdValueRaw;
    end
end
if isempty(thresholdValueComp)
    if isempty(persistent_thresholdValueRaw)
        thresholdValueComp = -Inf;
    else
        thresholdValueComp = persistent_thresholdValueComp;
    end
end

p = ClParametre(   'Name', 'Threshold on Raw EI signal',  'Unit', 'dB', 'Value', thresholdValueRaw);
p(2) = ClParametre('Name', 'Threshold on Comp EI signal', 'Unit', 'dB', 'Value', thresholdValueComp);
a = StyledSimpleParametreDialog('params', p, 'Title', 'Thresholds for object detection in the Water Column');
a.openDialog;
flag = a.okPressedOut;
if ~flag
    return
end
X = a.getParamsValue;
thresholdValueRaw  = X(1);
thresholdValueComp = X(2);

if ~isinf(thresholdValueRaw)
    persistent_thresholdValueRaw  = thresholdValueRaw;
end
if ~isinf(thresholdValueComp)
    persistent_thresholdValueComp = thresholdValueComp;
end
