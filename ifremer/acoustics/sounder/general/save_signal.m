function flag = save_signal(nomFic, NomRep, NomLayer, X)

[nomDir, nom] = fileparts(nomFic);

nomFicXml = fullfile(nomDir, 'SonarScope', nom, [NomRep '.xml']);
if ~exist(nomFicXml, 'file')
    flag = 0;
    return
end

Info = xml_mat_read(nomFicXml);
nomDirRacine = fullfile(nomDir, 'SonarScope', nom);

k = find(strcmp({Info.Signals.Name}, NomLayer));
if isempty(k)
    str1 = sprintf('"%s" non trouv� dans "%s".', NomLayer, nomFicXml);
    str2 = sprintf('"%s" not found in "%s".', NomLayer, nomFicXml);
    my_warndlg(Lang(str1,str2), 1);
    flag = 0;
    return
end

flag = writeSignal(nomDirRacine, Info.Signals(k), X);
