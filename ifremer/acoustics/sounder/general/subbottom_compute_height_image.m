function H = subbottom_compute_height_image(I, TypeSignal, FiltreSonarA, FiltreSonarB, varargin)

[varargin, Titre] = getPropertyValue(varargin, 'Titre', []); %#ok<ASGLU>

M = size(I, 1);
H = ones(M, 1, 'single');

if ~isempty(Titre')
    str = sprintf('%s : Height estimation', Titre);
    hw = create_waitbar(str, 'N', M);
end
for k=1:M
    if ~isempty(Titre)
        my_waitbar(k, M, hw);
    end
    
    signal = I(k,:);
    
    %     signal(isnan(signal)) = 0;
    subNan    = isnan(signal);
    subNonNan = find(~subNan);
    switch length(subNonNan)
        case 0
            %         H(k) = 1;
        case 1
            H(k) = subNonNan;
        otherwise
            %     signal(subNan) = min(signal(subNonNan));
            signal = signal(subNonNan);
            switch TypeSignal
                case 1 % Signal alternatif
                    signal = abs(single(signal));
                    ih = subbottom_compute_height_ping(signal, FiltreSonarA, FiltreSonarB);
                case 2 % Signal montant
                    %             H(k) = subbottom_compute_height_ping(signal, FiltreSonarA, FiltreSonarB);
                    ih = calculer_hauteur_signal1(signal, FiltreSonarA, FiltreSonarB, 0);
            end
            ih = min(ih, length(signal));
            H(k) = subNonNan(ih);
    end
end
if ~isempty(Titre) && ishandle(hw)
    my_close(hw, 'MsgEnd');
end
