% Estimation du premier echo sur un signal de sondeur pénétrateur de sédiments
%
% Syntax
%   H = subbottom_compute_height_ping(S1, A, B)
%
% Input Arguments 
%   S1 : Ping sonar
%   A : Parametres du filtre sur le signal sonar 
%                 [] pour ne pas filtrer cf. butter
%   B : Parametres du filtre sur le signal sonar 
%                 [] pour ne pas filtrer
%
% Output Arguments
%   [] : Auto-plot activation
%   H  : Indice de la hauteur
%
% Remarks : Le filtre utilise est un butterworth a phase nulle (help filtfilt).
%           L'ordre du filtre est donc le double de ce que vous transmettez.
% 
% Examples
%   S = atan(-100:100);
%   S = S + 5 * rand(size(S));
%
%   subbottom_compute_height_ping(S, [], [], 0)
%
%   Order = 1;
%   Wc = 0.2;
%   [B,A] = butter(Order, Wc);
%   subbottom_compute_height_ping(S, A, B, 0)
%
%   H = subbottom_compute_height_ping(S, A, B, 0)
%
% See also calculer_hauteur cl_car_sonar Authors
% Authors : JMA
%-------------------------------------------------------------------------------

function H = subbottom_compute_height_ping(S1, A, B)

subNonNan = find(~(isnan(S1) | (S1 == 0)));
if ~isempty(subNonNan)
    S1 = S1(subNonNan);
end

if ~isempty(A) && ~isempty(B)
    if length(S1) > (2*length(A))
        S2 = filtfilt(B, A, double(S1));
        %     figure; plot(S1, 'b'); grid on; hold on; plot(S2, 'r');
    else
        S2 = S1;
    end
else
    S2 = S1;
end

Max = max(S2);
Seuil = Max / 5;
H = find(S2 > Seuil, 1 , 'first');
if isempty(H)
    H = length(S1);
    return
end

N = length(S2);
H = max(H, 10+1);
[~,n] = max(S2(max(1,H-10):min(N,H+10)));

k = max(H-10-2+n, 1);
H = subNonNan(k);

%% Auto plot

if nargout == 0
    figure
    plot(S1, 'k'); grid on; hold on; plot(S2, 'b');
    plot(H, S1(H), 'ro'); plot([H H], get(gca, 'YLim'), 'r'); hold off;
end
