function [flagOKCancel, selectDatagram] = uiTableDatagram(listMandatory, listOpt)

selectDatagram  = [];
flagOKCancel    = 0;
%% Cr�ation de la figure.
hdlFig   = figure('Position',[300 100 600 800], 'Menubar', 'none',...
    'Units', 'normalized', ...
    'Name', 'Select datagrams you want to save', ...
    'Color', [229/255 229/255 229/255], ...
    'CloseRequestFcn', {@cbCloseFcn, []});

%% D�coupage en deux zones horizontales dont la 1�re est d�coup�e en trois sous-panneaux.
% Cr�ation des containers
hV1Box       = uiextras.VBox('Parent', hdlFig,   'Tag', 'tagV1Box', 'BackgroundColor', [180/255 180/255 180/255]);
hH1V1Box     = uiextras.HBox('Parent', hV1Box,   'Tag', 'tagH1V1Box', 'BackgroundColor', [180/255 180/255 180/255]);
hV2V1Box     = uiextras.VBox('Parent', hV1Box,   'Tag', 'tagH2V1Box');
hH11H2V1Box  = uiextras.HBox('Parent', hV2V1Box, 'Tag', 'tagH11V2V1Box', 'BackgroundColor', [180/255 180/255 180/255]);
hH21H2V1Box  = uiextras.HBox('Parent', hV2V1Box, 'Tag', 'tagH21V2V1Box', 'BackgroundColor', [180/255 180/255 180/255]);
hH3V1Box     = uiextras.HBox('Parent', hV1Box,   'Tag', 'tagH3V1Box', 'BackgroundColor', [180/255 180/255 180/255]);

% Cr�ation des sous-panels
hPnlMand     = uipanel('Parent', hH1V1Box,    'Title', Lang('Datagrammes obligatoires','Mandatory datagrams'),'Tag', 'tagPnlMand');
hPnlOpt      = uipanel('Parent', hH11H2V1Box, 'Title', Lang('Datagrammes optionnels','Optionnal datagrams'), 'Tag', 'tagPnlOpt');
hPnlCheck    = uipanel('Parent', hH21H2V1Box, 'Title', '', 'Tag', 'tagPnlCheck');
hPnlBtn      = uipanel('Parent', hH3V1Box,    'Title', '', 'Tag', 'hPnlBtn');

set(hV1Box,   'Sizes', [-1 -1 40], 'Spacing', 20)
set(hV2V1Box, 'Sizes', [-1 40],    'Spacing', 2)

% Datagrammes obligatoires
data = [];
for k=1:size(listMandatory.name,2)
    data = [data;{listMandatory.nb(k), listMandatory.code(k), listMandatory.name{k}, true}];
end

% Datagrammes optionnels
dataOpt = [];
for k=1:size(listOpt.name,2)
    dataOpt = [dataOpt;{listOpt.nb(k), listOpt.code(k), listOpt.name{k}, true}];
end


colName         = {'Numbers', 'Code', 'Name', 'Available'};
colFormat       = {'numeric', 'numeric', 'char', 'logical'};
colEditOpt      = [false false false true];
colEditMand     = [false false false false];
hTabDatagramMand     = uitable('Units','normalized', ...
                            'Tag', 'tagTabDatagramMand', ...
                            'Parent', hPnlMand, ...
                            'Position',[0.0 0.0 1.0 1.0], ...
                            'Data', data,...
                            'ColumnName', colName,...
                            'ColumnFormat', colFormat,...
                            'ColumnEditable', colEditMand,...
                            'RowName',[], ...
                            'ColumnWidth',{70, 70, 300, 100}); %#ok<NASGU>

hTabDatagramOpt     = uitable('Units','normalized', ...
                            'Tag', 'tagTabDatagramOpt', ...
                            'Parent', hPnlOpt, ...
                            'Position',[0.0 0.0 1.0 1.0], ...
                            'Data', dataOpt,...
                            'ColumnName', [],...
                            'ColumnFormat', colFormat,...
                            'ColumnEditable', colEditOpt,...
                            'RowName',[], ...
                            'ColumnWidth',{70, 70, 300, 100}); %#ok<NASGU>

                        
hBtnCheckAll = uicontrol('parent', hPnlCheck, ...
                    'Tag',      'btnCheckAll', ...
                    'Style',    'pushbutton',...
                    'Units',    'normalized', ...
                    'String',   'Check All Datagrams',...
                    'Position', [0.0 0.0 0.5 1.0], ...
                    'Callback', {@cb_checkAllDatagrams, 1}); %#ok<NASGU>

hBtnUncheckAll = uicontrol('parent', hPnlCheck, ...
                    'Tag',      'btnUnCheckAll', ...
                    'Style',    'pushbutton',...
                    'Units',    'normalized', ...
                    'String',   'Uncheck All Datagrams',...
                    'Position', [0.5 0.0 0.5 1.0], ...
                    'Callback', {@cb_checkAllDatagrams, 0}); %#ok<NASGU>

hBtnOK  =     uicontrol(...
                    'Parent',   hPnlBtn,...
                    'Units',    'normalized',...
                    'Position', [0.0 0.0 0.5 1.0],...
                    'Tag',      'tagBtnOk',...
                    'String',   'OK', ...
                    'Tooltip',  Lang('Valider','Validate'), ...
                    'CallBack', @cbBtnOK); %#ok<NASGU>
    
hBtnCancel =    uicontrol(...
                    'Parent',   hPnlBtn,...
                    'Units',    'normalized',...
                    'Position', [0.5 0.0 0.5 1.0],...
                    'Tag',      'tagBtnCancel',...
                    'String',   Lang('Annuler','Cancel'), ...
                    'Tooltip',  Lang('Annuler','Cancel'), ...
                    'CallBack', @cbBtnCancel); %#ok<NASGU>
  
% Make the GUI blocking
uiwait(hdlFig);

% Reprise des sorties d'IHM
if isappdata(hdlFig,'flagOKCancel')
    flagOKCancel = getappdata(hdlFig,'flagOKCancel');
    rmappdata(hdlFig,'flagOKCancel');
end
if isappdata(hdlFig,'selectDatagrams')
    selectDatagram = getappdata(hdlFig,'selectDatagrams');
    rmappdata(hdlFig,'selectDatagrams');
end

% Fermeture de l'IHM
delete(hdlFig);


% = Callback ===============
function cbBtnOK(hObject, event)

hdlFig              = ancestor(hObject,   'figure', 'toplevel');
hTabDatagramOpt     = findobj(hdlFig, 'tag', 'tagTabDatagramOpt');
hTabDatagramMand    = findobj(hdlFig, 'tag', 'tagTabDatagramMand');
listDatagrams       = [get(hTabDatagramOpt,'Data');get(hTabDatagramMand,'Data')];

% Stimulation de la Callback de fermeture
setappdata(hdlFig,'flagOKCancel', 1);
cbCloseFcn(hdlFig, event, listDatagrams);
% Fin cbBtnOK


    % = Callback ===============
    function cbBtnCancel(hObject, event)

        hdlFig          = ancestor(hObject, 'figure','toplevel');
        setappdata(hdlFig,'flagOKCancel', 0);
 
        % Stimulation de la Callback de fermeture
        listDatagrams      = [];
        cbCloseFcn(hdlFig, event, listDatagrams);
        
% Fin cbBtnCancel

    % = Callback ===============
    function cbCloseFcn(hObject, event, listDatagrams) %#ok<INUSL>

        % disp('Close request function...');
        setappdata(hObject,'selectDatagrams',listDatagrams);
        uiresume(hObject);
    
% Fin cbCloseFcn

    
% = Callback ===============
function cb_checkAllDatagrams(hObject, event, flagCkeckAllDatagrams) %#ok<INUSL>

    % S�lection de tous les datagrammes.
    hTabDatagramOpt = findobj('tag', 'tagTabDatagramOpt');
    pppp            = get(hTabDatagramOpt,'Data');        
    if flagCkeckAllDatagrams
        for i=1:size(pppp,1)
            pppp{i,end} = true;
        end
    else
       for i=1:size(pppp,1)
            pppp{i,end} = false;
       end
    end

    set(hTabDatagramOpt,'Data', pppp);
% Fin cb_checkAllDatagrams
