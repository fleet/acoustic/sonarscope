% q = unwrapPingCounter([65530:65535 0:10])

function PingCounter = unwrapPingCounter(PingCounter)

% Test
PingCounter(isnan(PingCounter)) = 0;

pas = median(single(diff(PingCounter(~isnan(PingCounter)))));
if pas >= 0
    for k=2:length(PingCounter)
        if PingCounter(k) <= PingCounter(k-1)
            PingCounter(k) = PingCounter(k-1) + 1;
        end
    end
else
    for k=2:length(PingCounter)
        if PingCounter(k) >= PingCounter(k-1)
            PingCounter(k) = PingCounter(k-1) - 1;
        end
    end
end
