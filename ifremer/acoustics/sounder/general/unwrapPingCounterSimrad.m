% q = unwrapPingCounterSimrad([65530:65535 0:10])

function PingCounter = unwrapPingCounterSimrad(PingCounter)

for i=2:length(PingCounter)
    if PingCounter(i) < PingCounter(i-1)
%         k = floor((PingCounter(i-1) - PingCounter(i) + 1) / 65536);
        k = round((PingCounter(i-1) - PingCounter(i) + 1) / 65536);
        PingCounter(i) = PingCounter(i) + k*65536;
    end
end

k = floor(PingCounter(1) / 65536);
PingCounter = PingCounter - k*65536;