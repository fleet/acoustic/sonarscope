function flag = writeImage(nomDir, InfoImage, Image, varargin)

% ErMapper sert uniquement dans le cas d'une image RGB o� on doit �crire
% chaque ligne enti�re en Red, Green et blue  ae passer � la ligne suivante
% sinon les matrices 3D sont �crites leas canaux les uns apr�s les autres
[varargin, ErMapper]       = getPropertyValue(varargin, 'ErMapper',       0);
[varargin, AlreadyFlipped] = getPropertyValue(varargin, 'AlreadyFlipped', 0);
[varargin, isRGB]          = getPropertyValue(varargin, 'RGB',            []); %#ok<ASGLU>

if isa(Image, 'cl_time')
    Image = Image.timeMat;
end

nomFic = fullfile(nomDir, InfoImage.FileName);
checkLengthFilename(nomFic)
fid = fopen(nomFic, 'w+');
if fid == -1
    messageErreurFichier(nomFic, 'WriteFailure');
    flag = 0;
    return
end

if isempty(isRGB)
    isRGB = (ndims(Image) == 3) && (size(Image,3) == 3);
end
% isMemmapfile = isa(Image, 'cl_memmapfile');
N1 = size(Image, 1);
N2 = size(Image, 2);
N3 = size(Image, 3);
% N = N1*N2*N3;

% if (N1*N2*N3) > 1e7 % SizePhysTotalMemory
%     str1 = sprintf('  Exportation en cours de\n"%s"', nomFic);
%     str2 = sprintf('  Exporting %s', nomFic);
%     fprintf('%s\n', Lang(str1,str2))
% end

if ErMapper
    if isRGB % ErMapper & RGB
        if ~isdeployed && (N2 == 1)
            my_warndlg('writeImage N2 == 1, ErMapper & RGB : regarder si il faut optimiser', 0, 'Tag', 'writeImageN2==1');
        end
        n = 0;
        for k1=1:N1
            for k3=1:N3
                if AlreadyFlipped
                    nl = fwrite(fid, Image(k1,:,k3), InfoImage.Storage); % V�rif Earth ombr�e Memmapfile
                else
                    nl = fwrite(fid, Image(N1-k1+1,:,k3), InfoImage.Storage);
                end
                n = n + nl;
            end
        end
        
    else % ErMapper & ~RGB
        if ~isdeployed && (N2 == 1)
            my_warndlg('writeImage N2 == 1, ErMapper & ~RGB : regarder si il faut optimiser', 0, 'Tag', 'writeImageN2==1');
        end
        n = 0;
        if N3 == 1
            for k1=1:N1
                if AlreadyFlipped
                    nl = fwrite(fid, Image(k1,:), InfoImage.Storage); % v�rif Earth ombr�e On Ram
                else
                    nl = fwrite(fid, Image(N1-k1+1,:), InfoImage.Storage); % v�rif Earth ombr�e On Ram
                end
                n = n + nl;
            end
        else % A priori on ne devrait jamais passer par ici
            str1 = 'Message pour JMA : A priori on ne devrait jamais passer par ici';
            str2 = 'Message for JMA : A priori on ne devrait jamais passer par ici';
            my_warndlg(Lang(str1,str2), 0, 'Tag', 'writeImage_PartieJamaisTestee');
            for k1=1:N1
                for k3=1:N3
                    if AlreadyFlipped
                        nl = fwrite(fid, Image(k1,:,k3), InfoImage.Storage); % v�rif Earth ombr�e On Ram
                    else
                        nl = fwrite(fid, Image(N1-k1+1,:,k3), InfoImage.Storage); % v�rif Earth ombr�e On Ram
                    end
                    n = n + nl;
                end
            end
        end
    end
else
    if isRGB % ~ErMapper & RGB
        if ~isdeployed && (N2 == 1)
            my_warndlg('writeImage N2 == 1, isRGB== 1 : regarder si il faut optimiser', 0, 'Tag', 'writeImageN2==1');
        end
        n = 0;
        for k3=1:N3
            for k1=1:N1
                nl = fwrite(fid, Image(k1,:,k3), InfoImage.Storage); % V�rif Earth ombr�e Memmapfile
                n = n + nl;
            end
        end
    else % ~ErMapper & ~RGB
        n = 0;
        N3 = size(Image, 3);
        for k3=1:N3
            if N2 == 1
                nl = fwrite(fid, Image(:,:,k3), InfoImage.Storage);
                n = n + nl;
            else
                if N3 == 1
                    try
                        X = Image(:,:);
                        nl = fwrite(fid, X', InfoImage.Storage);
                        n = n + nl;
                    catch %#ok<CTCH>
                        fseek(fid, 0, 'bof');
                        for k1=1:N1
                            nl = fwrite(fid, Image(k1,:), InfoImage.Storage);
                            n = n + nl;
                        end
                    end
                else
                    X = Image(:,:,k3);
                    nl = fwrite(fid, X', InfoImage.Storage);
                    n = n + nl;
                end
                %                 catch ME
                %                     fseek(fid, 0, 'bof');
                %                     for k1=1:N1
                %                         On pourrait optimiser cela en faisant un write general si N3==0 et que l'image n'est pas en memmapfile
                %                         nl = fwrite(fid, Image(k1,:,k3), InfoImage.Storage);
                %                         n = n + nl;
                %                     end
                %                 end
            end
        end
    end
end
    
if n ~= numel(Image)
    messageErreurFichier(nomFic, 'WriteFailure');
    flag = 0;
    fclose(fid);
    return
end
fclose(fid);
        
flag = 1;
