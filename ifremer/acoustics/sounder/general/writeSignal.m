function flag = writeSignal(nomDir, InfoSignal, Signal)

if isfield(InfoSignal, 'Storage') && strcmp(InfoSignal.Storage, 'Pointer')
    flag = 1;
    return
end

if isa(Signal, 'cl_time')
    Signal = Signal.timeMat;
elseif iscell(Signal)
    % TODO : a supprimer quand le write String sera valid�
    Signal = cell2str(Signal); % Rajout� par JMA le 27/09/2011 pour XML Slices along Nav
    Signal(:,end+1) = sprintf('\r');
end

if isempty(InfoSignal.FileName)
    str1 = '"InfoSignal.FileName" est vide dans writeSignal, repportez cette anomalie � sonarscope@ifremer.fr SVP.';
    str2 = '"InfoSignal.FileName" is empty in writeSignal, please report to sonarscope@ifremer.fr.';
    my_warndlg(Lang(str1,str2), 1);
    flag = 0;
    return
end
nomFic = fullfile(nomDir, InfoSignal.FileName);
checkLengthFilename(nomFic)

% Pour contourner pb bizarre rencontr� avec fichier Klein (variable depth)
if exist(nomFic, 'file')
    nomFicTrash = [nomFic '_Trash'];
    flag = movefile(nomFic, nomFicTrash);
    if flag
        s = warning;
        warning off
        delete(nomFicTrash)
        warning(s)
    end
end

%% Ecriture du fichier

fid = fopen(nomFic, 'w+');
if fid == -1
    messageErreurFichier(nomFic, 'WriteFailure');
    flag = 0;
    return
end

if ~isfield(InfoSignal, 'Direction') || isempty(InfoSignal.Direction) || strcmp(InfoSignal.Direction, 'FirstValue=FirstPing')
    sz = size(Signal);
    if (sz(1) ~= 1) && (sz(2) ~= 1) && (prod(sz) > 1e7) % Pour �viter des Out Of Memory SizePhysTotalMemory
        n = 0;
        for k=1:sz(1)
            nk = fwrite(fid, Signal(k,:), InfoSignal.Storage);
            n = n + nk;
        end
    else
        n = fwrite(fid, Signal(:,:)', InfoSignal.Storage);
    end
else
    n = fwrite(fid, flipud(Signal(:,:))', InfoSignal.Storage);
end
if n ~= numel(Signal)
    messageErreurFichier(nomFic, 'WriteFailure');
    flag = 0;
    fclose(fid);
    return
end
fclose(fid);
flag = 1;
