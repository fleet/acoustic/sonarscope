function flag = writeSscSignal(Signal, nomDir, InfoSignal, Dimensions)

mots = strsplit(InfoSignal.Dimensions, ',');
for k=1:length(mots)
    nomDim = strtrim(mots{k});
    if isnan(str2double(nomDim))
        Dims(k) = Dimensions.(nomDim); %#ok<AGROW>
    else
        Dims(k) = str2double(nomDim); %#ok<AGROW>
    end
end

nomFic = fullfile(nomDir, InfoSignal.FileName);
if exist(nomFic, 'file') % cas o� le fichier est d�j� ouvert en memmapfile (trouver un moyen de le r��crire : writable=true ???
    fid = fopen(nomFic, 'w+');
    if fid == -1
        [nomDir2, nomFic2] = fileparts(nomFic);
        nomFic_new = fullfile(nomDir2, [nomFic2 '_new.bin']);
        fid = fopen(nomFic_new, 'w+');
        if fid == -1
            messageErreurFichier(nomFic_new);
            flag = 0;
            return
        end
        str1 = sprintf('Le fichier "%s" semble d�j� ouvert. Une copie "%s" a �t� cr��e.', nomFic, nomFic_new);
        str2 = sprintf('File "%s" seems to be already open. A copy  "%s" was created.', nomFic, nomFic_new);
        my_warndlg(Lang(str1,str2), 0, 'Tag', 'Erreur_writeSscSignal', 'TimeDelay', 60);
    end
else
    fid = fopen(nomFic, 'w+');
    if fid == -1
        messageErreurFichier(nomFic);
        flag = 0;
        return
    end
end

if isa(Signal, 'cl_time')
    Signal = Signal.timeMat;
    
% TODO : a modifier quand on fera le changement des signaux
    Signal = repmat(Signal, 1, Dims(2));

end

% Signal = reshape(Signal, fliplr(Dims))';

n = fwrite(fid, Signal', InfoSignal.Storage);

fclose(fid);


if n < prod(Dims)
    messageErreurFichier(nomFic);
    flag = 0;
    return
end
% if n ~= prod(Dims)
%     if ~strcmp(InfoSignal.Name, 'Entries') % Exception pour SebedImage : � r�soudre plus intelligemment
%         messageErreurFichier(nomFic);
%     end
% end

% if isfield(InfoSignal, 'Unit') && strcmpi(InfoSignal.Unit, 'days since JC')
%     try
%         Signal = reshape(Signal, fliplr(Dims))';
%     catch
%         'On ne devrait pas passer par ici'
%     end
%     Signal = Signal(:,1);
%     Signal = cl_time('timeMat', Signal);
% else
%     try
%         Signal = reshape(Signal, fliplr(Dims))';
%     catch
%         'On ne devrait pas passer par l�'
%     end
% end

flag = 1;
