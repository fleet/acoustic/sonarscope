function flag = writeString(nomDir, InfoString, String)

if isfield(InfoString, 'Storage') && strcmp(InfoString.Storage, 'Pointer')
    flag = 1;
    return
end

if isa(String, 'cl_time')
    String = String.timeMat;
elseif iscell(String)
    % TODO : a supprimer quand le write String sera valid�
    String = cell2str(String); % Rajout� par JMA le 27/09/2011 pour XML Slices along Nav
    String(:,end+1) = sprintf('\r');
end

nomFic = fullfile(nomDir, InfoString.FileName);
checkLengthFilename(nomFic)

% Pour contourner pb bizarre rencontr� avec fichier Klein (variable depth)
if exist(nomFic, 'file')
    nomFicTrash = [nomFic '_Trash'];
    flag = movefile(nomFic, nomFicTrash);
    if flag
        delete(nomFicTrash)
    end
end


fid = fopen(nomFic, 'w+');
if fid == -1
    messageErreurFichier(nomFic, 'WriteFailure');
    flag = 0;
    return
end

sz =  size(String);
if (sz(1) ~= 1) && (sz(2) ~= 1) && (prod(sz) > 1e7) % Pour �viter des Out Of Memory SizePhysTotalMemory
    n = 0;
    for k=1:sz(1)
        nk = fwrite(fid, String(k,:), 'char');
        n = n + nk;
    end
else
    n = fwrite(fid, String', 'char');
end

if n ~= numel(String)
    messageErreurFichier(nomFic, 'WriteFailure');
    flag = 0;
    fclose(fid);
    return
end
fclose(fid);
flag = 1;
