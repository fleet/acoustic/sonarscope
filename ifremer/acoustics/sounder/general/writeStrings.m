function flag = writeStrings(nomDir, InfoSignal, Signal)

nomFic = fullfile(nomDir, InfoSignal.FileName);
checkLengthFilename(nomFic)

fid = fopen(nomFic, 'w+');
if fid == -1
    messageErreurFichier(nomFic, 'WriteFailure');
    flag = 0;
    return
end

for k=1:numel(Signal)
    fprintf(fid, '%s\n', Signal{k});
end

fclose(fid);
flag = 1;
