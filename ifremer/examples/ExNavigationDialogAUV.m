function ExNavigationDialogAUV

%% Import the data
%%

fileName{1} = 'DataForDemo/NavAUV/usblRepeater09Modif.txt';
fileName{2} = 'DataForDemo/NavAUV/usblRepeater10Modif.txt';
fileName{3} = 'DataForDemo/NavAUV/usblRepeater11Modif.txt';
fileName{4} = 'DataForDemo/NavAUV/usblRepeater12Modif.txt';
FileNameOut = FtpUtils.importDataForDemoFromSScFTP(fileName);

%% Create the ClNavigation instances
%%

nav = ClNavigation.empty();
for k=1:length(FileNameOut)
    [flag, Latitude, Longitude, SonarTime, Heading, SonarSpeed, Immersion] = lecFicNav(FileNameOut{k}); %#ok<ASGLU>
    if ~flag
        continue
    end
    
    T = datetime(SonarTime.timeMat, 'ConvertFrom', 'datenum');
    timeSample = XSample('name', 'time', 'data', T);
    
    latSample = YSample('data', Latitude);
    latSignal = ClSignal('ySample', latSample, 'xSample', timeSample);
    
    lonSample = YSample('data', Longitude);
    lonSignal = ClSignal('ySample', lonSample, 'xSample', timeSample);
    
    immersionSample = YSample('name', 'Immersion', 'unit', 'm', 'data', Immersion);
    immersionSignal = ClSignal('ySample', immersionSample, 'xSample', timeSample);
    
    nav(end+1) = ClNavigation(latSignal, lonSignal, 'immersionSignal', immersionSignal, 'name', ['navigation ' num2str(k)]); %#ok<AGROW>
end

%% Display ClNavigation instance
%%

% nav

%% Create and open the dialog window
%%

a = NavigationDialog(nav, 'Title', 'ExNavigationDialogAUV', 'CleanInterpolation', true, 'waitAnswer', false);
a.openDialog();
