function flag = ExNavigationDialogDaurade

flag = 0;

%% Import the data
%%

nomFicMat = fullfile(my_tempdir, '20131010-navSMF-std.mat');
if exist(nomFicMat, 'file')
    [Latitude, Longitude, SonarTime, Heading, Immersion] = loadmat(nomFicMat, ...
        'nomVar', {'Latitude', 'Longitude', 'SonarTime', 'Heading', 'Immersion'});
else
    fileName = 'DataForDemo/Delph/20131010-navSMF-std.txt';
    FileNameOut = FtpUtils.importDataForDemoFromSScFTP(fileName, 'SScFtpDirectory', '/sonarscope/private/ifremer');
    if isempty(FileNameOut)
        return
    end
    [flag, Latitude, Longitude, SonarTime, Heading, SonarSpeed, Immersion] = lecFicNav(FileNameOut); %#ok<ASGLU>
    if ~flag
        return
    end
    save(nomFicMat, 'Latitude', 'Longitude', 'SonarTime', 'Heading', 'Immersion');
end

%% Create the ClNavigation instances
%%

T = datetime(SonarTime.timeMat, 'ConvertFrom', 'datenum');
timeSample = XSample('name', 'time', 'data', T);

latSample = YSample('data', Latitude);
latSignal = ClSignal('ySample', latSample, 'xSample', timeSample);

lonSample = YSample('data', Longitude);
lonSignal = ClSignal('ySample', lonSample, 'xSample', timeSample);

immersionSample = YSample('unit', 'm', 'data', -Immersion);
immersionSignal = ClSignal('ySample', immersionSample, 'xSample', timeSample);

headingSample = YSample('unit', 'deg', 'data', Heading);
headingSignal = ClSignal('ySample', headingSample, 'xSample', timeSample);

nav = ClNavigation(latSignal, lonSignal, 'headingVesselSignal', headingSignal, 'immersionSignal', immersionSignal, ...
    'name', '20131010-navSMF-std.txt');

%% Display ClNavigation instance
%%

% nav

%% Create and open the dialog window
%%

a = NavigationDialog(nav, 'Title', 'ExNavigationDialogDaurade', 'CleanInterpolation', true, 'waitAnswer', false);
a.openDialog();
