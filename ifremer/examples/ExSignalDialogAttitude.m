function ExSignalDialogAttitude

%% Import the data
%%

nomFic = getNomFicDatabase('EM1002_BELGICA_005053_raw.all');
aKM = cl_simrad_all('nomFic', nomFic);
[flag, DataAttitude] = read_attitude_bin(aKM, 'Memmapfile', 1);
if ~flag
    return
end

%% Create the SignalContainer instance
%%

xSample = XSample('name', 'time', 'data', DataAttitude.Datetime);

ySample       = YSample('data', DataAttitude.Roll(:,:), 'unit', 'deg');
signalList(1) = ClSignal('Name', 'Roll', 'ySample', ySample, 'xSample', xSample);

ySample       = YSample('data', DataAttitude.Pitch(:,:), 'unit', 'deg');
signalList(2) = ClSignal('Name', 'Pitch', 'ySample', ySample, 'xSample', xSample);

ySample       = YSample('data', DataAttitude.Heading(:,:), 'unit', 'deg');
signalList(3) = ClSignal('Name', 'Heading', 'ySample', ySample, 'xSample', xSample);

ySample       = YSample('data', DataAttitude.Heave(:,:), 'unit', 'm');
signalList(4) = ClSignal('Name', 'Heave', 'ySample', ySample, 'xSample', xSample);

attitudeSignalContainer = SignalContainer('signalList', signalList, 'Name', 'Attitude', 'originFilename', nomFic);

%% Display SignalContainer instance
%%

attitudeSignalContainer

%% Create and open the dialog window
%%

style1 = ColorAndFontStyle('BackgroundColor', [0.7 0.7 1], 'ForegroundColor', 'k', 'FontWeight', 'bold', 'FontSize', 13, 'FontAngle', 'italic'); %option
a = SignalDialog(attitudeSignalContainer, 'Title', 'ExSignalDialogAttitude', 'titleStyle', style1, 'waitAnswer', false);
a.openDialog();
