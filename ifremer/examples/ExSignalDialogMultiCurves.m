function ExSignalDialogMultiCurves

%% Import the data
%%

nomFicCurve = getNomFicDatabase('MultiCourbes.mat');
CourbesStatistiques = loadmat(nomFicCurve);
if isempty(CourbesStatistiques)
    return
end

%% Create the signals
%%

Colors = 'brkgmc';
nbCourbes = length(CourbesStatistiques);
for iCourbe=1:nbCourbes
    sectors = CourbesStatistiques(iCourbe).bilan{1};
    
    kmod = 1 + mod(iCourbe, length(Colors));
    if isempty(sectors.y)
        continue
    end
    
    xSample  = XSample('name', 'Angles', 'data', sectors.x, 'unit', 'deg');
    
    ySample = YSample('data', sectors.y,  'unit',  sectors.Unit, 'Color', Colors(kmod), 'marker', '.');
    signalReflec = ClSignal('Name', sectors.DataTypeValue, 'xSample', xSample, 'ySample', ySample);
    
    ySample = YSample('data', sectors.nx, 'unit', 'nb', 'Color', Colors(kmod), 'marker', '.');
    signalNx = ClSignal('Name', 'Nb of points', 'xSample', xSample, 'ySample', ySample);
    
    ySample = YSample('data', log10(sectors.nx), 'unit', 'Log10(nb)', 'Color', Colors(kmod), 'marker', '.');
    signalNxLog10 = ClSignal('Name', 'Nb of points', 'xSample', xSample, 'ySample', ySample);
    
    ySample = YSample('data', sectors.std, 'unit', sectors.Unit, 'Color', Colors(kmod), 'marker', '.');
    signalStd = ClSignal('Name', 'std', 'xSample', xSample, 'ySample', ySample);
    
    sigCurves(iCourbe) = SignalContainer('Name', sectors.nom, 'signalList', [signalReflec signalNx signalNxLog10 signalStd]); %#ok<AGROW>
end

%% Display ClSignal instance
%%

% sigCurves

%% Create and open the dialog window
%%

style1 = ColorAndFontStyle('BackgroundColor', [0.7 0.7 1], 'ForegroundColor', 'k', 'FontWeight', 'bold', 'FontSize', 13, 'FontAngle', 'italic'); %option
a = SignalDialog(sigCurves, 'Title', 'ExSignalDialogMultiCurves', 'titleStyle', style1);
% a.editProperties
a.displayedSignalList = [1 1 1 0];
a.openDialog();

%% Edit properties
%%

% a.editProperties
