function ExSignalDialogMultiSectors

%% Import the data

nomFicCurve = getNomFicDatabase('0554_20140726_012633_Amundsen_Reflectivity_BeamPointingAngle_TxBeamIndexSwath.mat');
[CourbeConditionnelle, flag] = load_courbesStats(nomFicCurve);
if ~flag
    return
end

%% Create the signals

Colors = 'brkgmc';
sectors = CourbeConditionnelle.bilan{1};
nbSectors = length(sectors);

for k=1:nbSectors
    kmod = 1 + mod(k, length(Colors));
    nameSector = sprintf('Sector %d', sectors(k).numVarCondition);
    xSample(k)  = XSample('name', 'Angles',   'data', sectors(k).x,   'unit', 'deg'); %#ok<AGROW>
    ySample1(k) = YSample('Name', nameSector, 'data', sectors(k).y,   'unit', sectors(k).Unit,   'Color', Colors(kmod)); %#ok<AGROW>
    ySample2(k) = YSample('Name', nameSector, 'data', sectors(k).nx,  'unit', 'nb',              'Color', Colors(kmod)); %#ok<AGROW>
    ySample3(k) = YSample('Name', nameSector, 'data', sectors(k).std, 'unit', sectors(k).Unit,   'Color', Colors(kmod)); %#ok<AGROW>
    ySample4(k) = YSample('Name', nameSector, 'data', log10(sectors(k).nx), 'unit', 'log10(nb)', 'Color', Colors(kmod)); %#ok<AGROW>
end

sigCurves    = ClSignal('Name', sectors(1).DataTypeValue, 'xSample', xSample, 'ySample', ySample1);
sigCurves(2) = ClSignal('Name', 'Number of points',       'xSample', xSample, 'ySample', ySample2);
sigCurves(3) = ClSignal('Name', 'Std',                    'xSample', xSample, 'ySample', ySample3);
sigCurves(4) = ClSignal('Name', 'Number of points Log10', 'xSample', xSample, 'ySample', ySample4);

%% Display ClSignal instance

% sigCurves %#ok<NOPRT>

%% Display ClSignal instance

% sigCurves.editProperties();

%% Create and open the dialog window

a = SignalDialog(sigCurves, 'Title', 'ExSignalDialogMultiSectors', 'waitAnswer', false);
a.displayedSignalList = [1 1 0 0];
a.openDialog();
