function ExSignalDialogSoundSpeedProfile

LatCentre  = -0.25;
LonCentre  = 45;
Frequency  = 30;
SonarDepth = -5;

isVertical = true;

%% Create the SignalContainer instances
%%

Mois = 1:3:12;
for k=1:length(Mois)
    
    %% Import the data

    Suffix = sprintf(' - %d', Mois(k));
    a = cli_sound_speed('Incoming', 2, 'LevitusAveragingType', 3, ...
        'Latitude', LatCentre, 'Longitude', LonCentre, ...
        'Frequency', Frequency, 'LevitusMonth', Mois(k), 'SonarDepth', SonarDepth);
    depth = get(a, 'Depth');
    if isempty(depth)
        %     return
    end
    
    DepthLevitus       = get(a, 'Depth');
    TemperatureLevitus = get(a, 'Temperature');
    SalinityLevitus    = get(a, 'Salinity');
    SoundSpeed = celeriteLovett(DepthLevitus, TemperatureLevitus, SalinityLevitus);
    [LocalAbsorption, AveragedAbsorption] = AttenuationGarrison(Frequency, DepthLevitus, TemperatureLevitus, SalinityLevitus);
    
    %% Create the signals
    
    xSample = XSample('name', 'Depth', 'data', -DepthLevitus, 'unit', 'm', 'marker', '.');
    
    ySample       = YSample('data', TemperatureLevitus, 'unit', 'deg', 'marker', '.');
    signalList(1) = ClSignal('Name', 'Temperature','ySample', ySample, 'xSample', xSample, 'isVertical', isVertical);
    
    ySample       = YSample('data', SalinityLevitus, 'unit', '1/1000', 'marker', '.');
    signalList(2) = ClSignal('Name', 'Salinity', 'ySample', ySample, 'xSample', xSample, 'isVertical', isVertical);
    
    ySample       = YSample('data', SoundSpeed, 'unit', 'deg', 'marker', '.');
    signalList(3) = ClSignal('Name', 'Sound Speed','ySample', ySample, 'xSample', xSample, 'isVertical', isVertical);
    
    ySample       = YSample('data', LocalAbsorption, 'unit', 'dB/km', 'marker', '.');
    signalList(4) = ClSignal('Name', 'Local Absorption','ySample', ySample, 'xSample', xSample, 'isVertical', isVertical);
    
    ySample       = YSample('data', AveragedAbsorption, 'unit', 'dB/km', 'marker', '.');
    signalList(5) = ClSignal('Name', 'Averaged Absorption','ySample', ySample, 'xSample', xSample, 'isVertical', isVertical);
    
    ssp(k) = SignalContainer('signalList', signalList, 'Name', ['Sound Speed Profile' Suffix]);  %#ok<AGROW>
end

%% Display SignalContainer instance
%%

% ssp

%% Plot du signal
%%

ssp.plot;

%% Create and open the dialog window
%%

a = SignalDialog(ssp, 'Title', 'Sound Speed Profile', 'waitAnswer', false);
a.openDialog();
