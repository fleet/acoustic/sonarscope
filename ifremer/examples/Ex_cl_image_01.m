% Ex. cl_image : Operateurs arithmetiques
% 
% Syntax
%   Ex_cl_image_01
%
% Examples 
%   Ex_cl_image_01
%
% See also Ex_cl_image_02 Ex_cli_image_01 Authors
% Authors : JMA
%------------------------------------------------------------------------------------------

[I, label] = ImageSonar(1);
a = cl_image('Image', single(I), 'Name', label, 'ColormapIndex', 2);
imagesc(a);

[I, label] = ImageSonar(5);
b = cl_image('Image', single(I), 'Name', label, 'ColormapIndex', 2);
imagesc(b);

alpha = linspace(0, 1, 11);
for k=length(alpha):-1:1
    c(k) = alpha(k) * a + (1-alpha(k)) * b;
    imagesc(c(k))
end

nomFicAvi = fullfile(my_tempdir, 'pppp3.mp4');
[flag, nomFicAvi] = movie(c, nomFicAvi);
if flag
    winopen(nomFicAvi)
end
