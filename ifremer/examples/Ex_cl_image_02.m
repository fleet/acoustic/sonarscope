% Ex. cl_image : filtrage de Fourier
% 
% Syntax
%   Ex_cl_image_02
%
% Examples 
%   Ex_cl_image_02
%
% See also Ex_cl_image_03 Authors
% Authors : JMA
%------------------------------------------------------------------------------------------

%% Premiere image : rides de sables horizontales

[I, label] = ImageSonar(1);
a1 = cl_image('Image', double(I), 'Name', label);
imagesc(a1);
A1 = fft(a1);
imagesc(A1)

% Filtrage passe-bande

A1D = fft_filtre_disk(A1, [0.05 0.083 0], 1);
imagesc(A1D)
a1d = ifft(A1D);
imagesc(a1d)


%% Deuxieme image : rides de sables obliques

[I, label] = ImageSonar(2);
a2 = cl_image('Image', double(I), 'Name', label);
imagesc(a2);
A2 = fft(a2);
imagesc(A2)

% Filtrage passe-bande

A2D = fft_filtre_disk(A2, [0.05 0.075 -0.1], 1);
imagesc(A2D)
a2d = ifft(A2D);
imagesc(a2d)


%% Troisieme image : Addition de a1 et a2

b = a1 + a2;
imagesc(b);
B = fft(b);
imagesc(B)

% Filtrage passe-bande

B0 = fft_filtre_disk(B, [0.05 0 0], 1);
imagesc(B0)
b0 = ifft(B0);
imagesc(b0)

B1 = fft_filtre_disk(B, [0.05 0.083 0], 1);
imagesc(B1)
b1 = ifft(B1);
imagesc(b1)

B2 = fft_filtre_disk(B, [0.05 0.075 -0.1], 1);
imagesc(B2)
b2 = ifft(B2);
imagesc(b2)


C = B0 + B1;
imagesc(C)
c = ifft(C);
imagesc(c)

C = B0 + B2;
imagesc(C)
c = ifft(C);
imagesc(c)

C = B0 + B1 + B2;
imagesc(C)
c = ifft(C);
imagesc(c)
