% Ex. cl_image : Compensations d'image
% 
% Syntax
%   Ex_cl_image_03
%
% Examples 
%   Ex_cl_image_03
%
% See also Ex_cl_image_04 Authors
% Authors : JMA
%------------------------------------------------------------------------------------------

[I, label] = ImageSonar(1);
a = cl_image('Image', double(I), 'Name', label);
imagesc(a);
nbColumns = a.nbColumns %#ok<*NOPTS>
nbRows    = a.nbRows

angles = linspace(-75, 75, nbColumns);
BS = BSLurton(angles, [-5, 3, -30, 2., -20, 10] );

% Ajout d'une modulation horizontale
aH = a + BS;
imagesc(aH);

% Ajout d'une derive verticale
rampe = (1:nbRows)';
aV = a - rampe;
imagesc(aV);

%% Compensation brutale

m = mean_col(aH)
bH = aH - m;
imagesc(bH);

m = mean_lig(aV)
bV = aV - m;
imagesc(bV);

%% Compensation elaboree

[flag, cH] = compensation(aH);
imagesc(cH);
mean_col(cH)
