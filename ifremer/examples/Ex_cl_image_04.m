% Ex. cl_image : Images sonar
% 
% Syntax
%   Ex_cl_image_04
%
% Examples 
%   Ex_cl_image_04
%
% See also Authors
% Authors : JMA
%------------------------------------------------------------------------------------------

nomFic = getNomFicDatabase('DF1000_ExStr.imo');
a = cl_car_sonar('nomFic', nomFic);

b = view(a, 'Sonar.Ident', 8, 'subl', 1:500);
editobj(b)

c = get(b, 'Images');
plot(c)

d = sonar_lateral_emission(c);
imagesc(d)

d = sonar_lateral_hauteur(c(1));
plot(d)

c(1) = set(c(1), 'SonarTVG_ConstructAlpha', 1);
d = sonar_TVG(c(1));
imagesc(d)

c = get(b, 'Images');
c(1)
imagesc(c(1))
d = set(c(1), 'SonarTVG_etat', 2);
d = set(c(1), 'SonarTVG_etat', 1, 'SonarTVG_origine', 2, 'SonarTVG_IfremerAlpha', 0);
imagesc(d)
imagesc(d-c(1))

e = set(c(1), 'SonarTVG_etat', 1, 'SonarTVG_origine', 2, 'SonarTVG_IfremerAlpha', 100);
imagesc(e)
imagesc(e-c(1))
