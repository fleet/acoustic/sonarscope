% Ex. ajustement des diagrammes de directivité d'un sondeur EM710  
% 
% Ex_optimSpline
%
% Examples 
%   Ex_optimSpline
%
% See also ajustement_spline ClOptim Authors
% Authors : JMA
%------------------------------------------------------------------------------------------

function Ex_optimSpline

FileNameSsc = getNomFicDatabase('BScorr_710.txt');
SounderName = 'EM710';

[flag, BSCorr] = read_BSCorr_Spline(FileNameSsc);
if ~flag
    return
end

MaxPingMode  = max([BSCorr(:).pingMode]);
MaxSwathMode = max([BSCorr(:).swathMode]) + 1;
Fig = FigUtils.createSScFigure;
for k1=1:length(BSCorr)
    pingMode  = BSCorr(k1).pingMode;
    swathMode = BSCorr(k1).swathMode;
    figure(Fig)
    for k2=1:length(BSCorr(k1).Sector)
%         BSCorr(k1).Sector(k2)
%         BSCorr(k1).Sector(k2).Node
        xNodes = BSCorr(k1).Sector(k2).Node(:,1)';
        yNodes = BSCorr(k1).Sector(k2).Node(:,2)';
        
        my_subplot(MaxPingMode, MaxSwathMode, pingMode, swathMode+1)
        
        XData = min(xNodes):0.5:max(xNodes);
        YData = spline(xNodes, yNodes, XData);
        
        YData = YData + randn(size(YData)) / 2;
        
        PlotUtils.createSScPlot(XData, YData, '.k'); grid on; hold on;
        
        ParametresAjustes = ajustement_spline(XData, YData, xNodes, 'yNodes', yNodes);
       
        yiNew = spline(xNodes, ParametresAjustes, XData);
        PlotUtils.createSScPlot(XData, yiNew, 'm'); grid on; hold on;
        PlotUtils.createSScPlot(xNodes, ParametresAjustes, 'ob'); grid on; hold on;
        
        BSCorr(k1).Sector(k2).Node(:,2) = yNodes;
    end
    hold off
    title(sprintf('swathMode : %d  -   pingMode : %d', swathMode, pingMode))
end

[nomDir, nomFic] = fileparts(FileNameSsc);
FileNameSscModifie = my_tempname('.txt');
flag = write_BSCorr_Spline(BSCorr, FileNameSscModifie, SounderName);
