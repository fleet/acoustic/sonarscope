%% Cr�ation de l'instance
a = cl_pames %#ok<*NOPTS>

%% Espionnage de la structure echoSounder
echoSounder = get(a, 'echoSounder')

%% R�cup�ration de la liste des sondeurs disponibles
ListeSounders = echoSounder.ListeSounders

%% Affectation du num�ro de sondeur � l'EM300
echoSounder.IdentSounder = 3;

%% R�initialisation de l'instance
a = set(a, 'echoSounder', echoSounder);
echoSounder = get(a, 'echoSounder')

%% R�cup�ration de la liste des modes disponibles pour le sondeur EM300
ListeModes = echoSounder.ListeModes

%% Affectation du num�ro de mode au mode shallow
echoSounder.IdentMode = 2; % Mode shallow

%% R�initialisation de l'instance
a = set(a, 'echoSounder', echoSounder);
echoSounder = get(a, 'echoSounder')

%% Calcul de la pr�cision de mesure pour des angles de faisceaux donn�s � une profondeur de 50 m
% Angle est un vecteur horizontal et Depth est un scalaire
Angle = 0:75;
Depth = 50;
X = CalculDz(a, Depth, Angle);
figure;  plot(tand(Angle), X); grid on;

%% Calcul de la pr�cision de mesure pour des angles de faisceaux donn�s pour diff�rentes profondeurs
% Angle est un vecteur horizontal et Depth est un vecteur vertical
Angle = 0:75;
Depth = (30:60)';
X = CalculDz(a, Depth, Angle);
figure;  plot(tand(Angle), X); grid on;

%% Calcul de la pr�cision de mesure pour des couples {angles, depth} donn�s 
% Angle et Depth sont deux vecteurs horizontaux de m�me longueur
Angle = 0:75;
Depth = linspace(30,60, length(Angle));
X = CalculDz(a, Depth, Angle);
figure;  plot(tand(Angle), X); grid on;

%% Calcul de la pr�cision de positionnement lat�rale pour des angles de faisceaux donn�s � une profondeur de 50 m

Angle = 0:75;
Depth = 50;
X = CalculDx(a, Depth, Angle);
figure; plot(tand(Angle), X); grid on;

%% Calcul de la pr�cision de positionnement pour des angles de faisceaux donn�s pour diff�rentes profondeurs
% Angle est un vecteur horizontal et Depth est un vecteur vertical
Angle = 0:75;
Depth = (30:60)';
X = CalculDx(a, Depth, Angle);
figure;  plot(tand(Angle), X); grid on;

%% Calcul de la pr�cision de positionnement pour des couples {angles, depth} donn�s 
% Angle et Depth sont deux vecteurs horizontaux de m�me longueur
Angle = 0:75;
Depth = linspace(30,60, length(Angle));
X = CalculDx(a, Depth, Angle);
figure;  plot(tand(Angle), X); grid on;


%% Calcul de la r�solution transversale de l'imagerie pour des angles de faisceaux donn�s � une profondeur de 50 m

Angle = 0:75;
Depth = 50;
X = ResolTransIma(a, Depth, Angle);
figure; plot(tand(Angle), X); grid on;


%% Calcul de la r�solution transversale de l'imagerie pour des angles de faisceaux donn�s pour diff�rentes profondeurs
% Angle est un vecteur horizontal et Depth est un vecteur vertical
Angle = 0:75;
Depth = (30:60)';
X = ResolTransIma(a, Depth, Angle);
figure; plot(tand(Angle), X); grid on;

%% Calcul de la r�solution transversale de l'imagerie pour des couples {angles, depth} donn�s 
% Angle et Depth sont deux vecteurs horizontaux de m�me longueur
Angle = 0:75;
Depth = linspace(30,60, length(Angle));
X = ResolTransIma(a, Depth, Angle);
figure; plot(tand(Angle), X); grid on;


%% Calcul de la r�solution transversale de l'imagerie pour des angles de faisceaux donn�s � une profondeur de 50 m

Angle = 0:75;
Depth = 50;
X = ResolTransBathy(a, Depth, Angle);
figure; plot(tand(Angle), X); grid on;


%% Calcul de la r�solution transversale de l'imagerie pour des angles de faisceaux donn�s pour diff�rentes profondeurs
% Angle est un vecteur horizontal et Depth est un vecteur vertical

Angle = 0:75;
Depth = (30:60)';
X = ResolTransBathy(a, Depth, Angle);
figure; plot(tand(Angle), X); grid on;

%% Calcul de la r�solution transversale de l'imagerie pour des couples {angles, depth} donn�s 
% Angle et Depth sont deux vecteurs horizontaux de m�me longueur

Angle = 0:75;
Depth = linspace(30,60, length(Angle));
X = ResolTransBathy(a, Depth, Angle);
figure; plot(tand(Angle), X); grid on;


%% Calcul de la r�solution longitudinale pour des angles de faisceaux donn�s � une profondeur de 50 m

Angle = 0:75;
Depth = 50;
X = ResolLong(a, Depth, Angle);
figure; plot(tand(Angle), X); grid on;


%% Calcul de la r�solution longitudinale pour des angles de faisceaux donn�s pour diff�rentes profondeurs
% Angle est un vecteur horizontal et Depth est un vecteur vertical

Angle = 0:75;
Depth = (30:60)';
X = ResolLong(a, Depth, Angle);
figure; plot(tand(Angle), X); grid on;

%% Calcul de la r�solution longitudinale pour des couples {angles, depth} donn�s 
% Angle et Depth sont deux vecteurs horizontaux de m�me longueur

Angle = 0:75;
Depth = linspace(30,60, length(Angle));
X = ResolLong(a, Depth, Angle);
figure; plot(tand(Angle), X); grid on;

%% Calcul de la fauch�e � une profondeur de 50 m pour les 3 types de fond

Depth = 50;
Width = calculFauchee(a, Depth, 1:3)

%% Calcul de la fauch�e � diff�rentes profondeurs pour les 3 types de fond

Depth = (1:50:3000)';
Width = calculFauchee(a, Depth, 1:3);
figure; plot(Width', repmat(-Depth, 1, 3)); grid on;
