function SetCopyrightsFor3DViewer

%% Etopo2

clear Copyright
Copyright.Name   = 'Etopo2';
Copyright.Source = 'NOAA : National Geographical Data Center';
Copyright.URL    = 'http://www.ngdc.noaa.gov/mgg/fliers/01mgg04.html';
Copyright.Status = 'Public';
Copyright.Logo   = 'D:\Demos-3DV\Earth\NOAA.gif';

nomFicXml = 'D:\Demos-3DV\Earth\Earth_Copyright.xml';
xml_write(nomFicXml, Copyright);

nomFicXml = 'D:\Demos-3DV\Earth\France_Copyright.xml';
xml_write(nomFicXml, Copyright);

%% Sextant

clear Copyright
Copyright.Name   = 'Sextant';
Copyright.Source = 'IFREMER';
Copyright.URL    = 'http://sextant.ifremer.fr/fr/;jsessionid=B72D45975C83646C4A39B92018BF2722';
Copyright.Status = 'Private';
Copyright.Logo   = 'D:\Demos-3DV\MNT-SEXTANT\3DV\Sextant.png';

nomFicXml = 'D:\Demos-3DV\MNT-SEXTANT\3DV\MancheGascogne500m_Copyright.xml';
xml_write(nomFicXml, Copyright);

nomFicXml = 'D:\Demos-3DV\MNT-SEXTANT\3DV\SHOM-FacadeOuest_Copyright.xml';
xml_write(nomFicXml, Copyright);

%% TODO

clear Copyright
Copyright.Name   = 'TODO';
Copyright.Source = 'TODO';
Copyright.URL    = 'TODO';
Copyright.email  = 'Massinissa.Benabdellouahed@ifremer.fr';
Copyright.Status = 'Private';
Copyright.Logo   = '';
nomFicXml = 'D:\Demos-3DV\MNT-SEXTANT\3DV\Massi_Copyright.xml';
xml_write(nomFicXml, Copyright);
