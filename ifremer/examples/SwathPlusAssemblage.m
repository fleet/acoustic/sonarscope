% cd /home3/jackson/Cidco

function SwathPlusAssemblage

%% Recherche des fichiers .mat

[flag, liste] = uiSelectFiles('ExtensionFiles', '.mat', 'RepDefaut', pwd);
if ~flag
    return
end

%% Selection des fichiers � traiter dans la liste

str1 = 'Choix des projets � traiter';
str2 = 'TODO';
[rep, validation] = my_listdlg(Lang(str1,str2), liste);
if ~validation
    return
end
liste = liste(rep);
nbProjets = length(liste);

R = cl_image.empty;
A = cl_image.empty;

titreR = {};
titreA = {};
resol = 0;
for i=1:length(liste)
    [nomDir, nomFic] = fileparts(liste{i});
    nomFicObj = fullfile(nomDir, [nomFic '.mat']);
    b = loadmat(nomFicObj, 'nomVar', 'b');
    if isa(b, 'cli_image')
        b = get(b, 'Images');
    end
    
    identLayerReflectivity  = cl_image.indDataType('Reflectivity');
    identLayerEmissionAngle = cl_image.indDataType('TxAngle');
    for k=1:length(b)
        GeometryType = b(k).GeometryType;
        if (GeometryType == cl_image.indGeometryType('GeoYX')) || (GeometryType == cl_image.indGeometryType('LatLong')) % metrique ou Geographique
            DataType = b(k).DataType;
            if DataType == identLayerReflectivity
                R(end+1) = b(k); %#ok<AGROW>
                titreR{end+1} = b(k).Name; %#ok<AGROW>
                resol = max(resol, get(b(k), 'SonarResolutionX'));
            end
            if DataType == identLayerEmissionAngle
                A(end+1) = b(k); %#ok<AGROW>
                titreA{end+1} = b(k).Name; %#ok<AGROW>
                resol = max(resol, get(b(k), 'SonarResolutionX'));
            end
        end
    end
end

if length(R) > nbProjets
    [rep, validation] = my_listdlg('Choix de l''image de reflectivite a mosaiquer', titreR);
    if ~validation
        return
    end
    R = R(rep);
end

if length(A) > nbProjets
    [rep, validation] = my_listdlg('Choix de l''image d''angle a mosaiquer', titreA);
    if ~validation
        return
    end
    A = A(rep);
end

clear b

[R,A] = mosaique(R, 'Angle', A );
SonarScope([R A])
