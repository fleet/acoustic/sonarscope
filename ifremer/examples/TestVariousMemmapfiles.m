%% Read ETOPO2 from .ers file

nomFic = 'D:\SonarScopeData\Level1\Public\Earth_LatLong_Bathymetry.ers';
[flag, b] = cl_image.import_ermapper(nomFic);
Earth = get(b, 'Image');
[nrows, ncols] = size(Earth);

%% Export variable in .mat file

nomFicOutMat = 'D:\Temp\EarthTestMemmapfile.mat';
save(nomFicOutMat, 'Earth', '-v7.3');

%% Export to a cl_memmapfile variable

tic
m = cl_memmapfile('Value', Earth, 'LogSilence', 1);
toc
whos m

%% Export to a HDF5 file format

nomFicOutHDF5 = 'D:\Temp\EarthTestMemmapfile.hdf5';
delete(nomFicOutHDF5)
tic
h5create(nomFicOutHDF5,'/Earth', [nrows, ncols], 'Datatype', class(Earth));
h5write(nomFicOutHDF5, '/Earth', Earth);
toc

%% Use of matfile class

example = matfile(nomFicOutMat);
varlist = who(example) %#ok<*NOPTS>
%%
[nrows, ncols] = size(example, 'Earth')
%%
fig1 = figure; imagesc(example.Earth); colormap(jet(256));
fig2 = figure; imagesc(example.Earth(1000:3000,5000:7000)); colormap(jet(256));
%%
close(fig1)
close(fig2)

%% Test performances on entire matrix

% Reads entire cl_memmapfile
tic;
X = m(:,:); %#ok<*NASGU>
toc
clear X

%%
% Reads entire matfile
tic;
X = example.Earth;
toc
clear X

%%
% Reads entire HDF5 file
tic;
X = h5read(nomFicOutHDF5, '/Earth');
toc
clear X

%% Test performances for loop on first dimenssion
% cl_memmapfile
tic;
for il=1:nrows
    X = m(il,:);
end
toc

%%
% Matfile
tic;
for il=1:10 % nrows
    X = example.Earth(il,:);
end
T = toc;
fprintf('Elapsed time is %f\n', T * 10);

%%
% HDF5
tic;
for il=1:100 % nrows
    X = h5read(nomFicOutHDF5, '/Earth', [il 1], [1 ncols]);
end
T = toc;
fprintf('Elapsed time is %f\n', T * 100);

%% Test performances for loop on second dimenssion
% cl_memmapfile
tic;
for ic=1:ncols
    X = m(:,ic);
end
toc

%%
% Matfile
tic;
for ic=1:ncols
    X = example.Earth(:,ic);
end
toc

%%
% HDF5
tic;
for ic=1:ncols
    X = h5read(nomFicOutHDF5, '/Earth', [1 ic], [nrows 1]);
end
toc

%% Test performances for loop on both dimensions
% cl_memmapfile ncols, nrows
tic;
for ic=1:100:ncols
    for il=1:100:nrows
        X = m(il,ic);
    end
end
T = toc;
fprintf('Elapsed time is %f\n', T * 10000);
%%

% cl_memmapfile nrows, ncols
tic;
for il=1:100:nrows
    for ic=1:100:ncols
        X = m(il,ic);
    end
end
T = toc;
fprintf('Elapsed time is %f\n', T * 10000);

%%
% Matfile nrows, ncols
tic;
for il=1:100:nrows
    for ic=1:100:ncols
        X = example.Earth(il,ic);
    end
end
T = toc;
fprintf('Elapsed time is %f\n', T * 10000);
%%

% Matfile ncols, nrows
tic;
for ic=1:100:ncols
    for il=1:100:nrows
        X = example.Earth(il,ic);
    end
end
T = toc;
fprintf('Elapsed time is %f\n', T * 10000);

%%
% HDF5 nrows, ncols
tic;
for il=1:100:nrows
    for ic=1:100:ncols
        X = h5read(nomFicOutHDF5, '/Earth', [il ic], [1 1]);
    end
end
T = toc;
fprintf('Elapsed time is %f\n', T * 10000);
%%

% HDF5 ncols, nrows
tic;
for il=1:100:nrows
    for ic=1:100:ncols
        X = h5read(nomFicOutHDF5, '/Earth', [il ic], [1 1]);
    end
end
T = toc;
fprintf('Elapsed time is %f\n', T * 10000);

%% Export to a cl_memmapfile file format by lines and columns

% Per line
tic
m1 = cl_memmapfile('Size', [nrows, ncols], 'Format', class(Earth));
for il=1:nrows
    m1(il,:) = Earth(il,:);
end
toc
%%

% Per column
tic
m2 = cl_memmapfile('Size', [nrows, ncols], 'Format', class(Earth));
for ic=1:ncols
    m2(:,ic) = Earth(:,ic);
end
toc
%%

%% Export to a HDF5 file format by lines and columns

% Per line
nomFicOutHDF5Rows = 'D:\Temp\EarthTestMemmapfileRows.hdf5';
delete(nomFicOutHDF5Rows)
tic
h5create(nomFicOutHDF5Rows,'/Earth', [nrows, ncols], 'Datatype', class(Earth));
for il=1:10:nrows
    h5write(nomFicOutHDF5Rows, '/Earth', Earth(il,:), [il 1], [1 ncols])
end
T = toc;
fprintf('Elapsed time is %f\n', T * 10);
%%

% Per column
nomFicOutHDF5Cols = 'D:\Temp\EarthTestMemmapfileRows.hdf5';
delete(nomFicOutHDF5Cols)
tic
h5create(nomFicOutHDF5Cols,'/Earth', [nrows, ncols], 'Datatype', class(Earth));
for ic=1:ncols
    h5write(nomFicOutHDF5Cols, '/Earth', Earth(:,ic), [1 ic], [nrows 1])
end
toc

