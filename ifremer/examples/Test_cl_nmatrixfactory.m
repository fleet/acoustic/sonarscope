%% Test of cl_nmatrixfactory 

% This script tests the cl_nmatrixfactory behaviour with various
% configurations

%% Create and initialize a 2D matrix with default mapped file

X = [1 2 3 4; 5 6 7 8] %#ok<*NOPTS>
nm = cl_nmatrixfactory.createInstance('Value', X)
%%
X(2,3)
nm(2,3)
%%
X(2,:)
nm(2,:)
%%
X(:,3)
nm(:,3)
%%
X(:,:)
nm(:,:)
%%
X(6)
nm(6)
%%
X([2 3 5 6 8])
nm([2 3 5 6 8])
%%
size(X)
size(nm)
%%
size(X,2)
size(nm,2)
%%
numel(X)
numel(nm)
%%
b = false(size(X)); b([2 3 5 6 8]) = true;
X(b)
nm(b)
%%
filename = get(nm, 'Filename')
exist(filename, 'file')
clear nm
exist(filename, 'file')
%%
nm = cl_nmatrixfactory.createInstance('Value', X)
filename = get(nm, 'Filename')
exist(filename, 'file')
nm = cl_nmatrixfactory.createInstance('Value', X)
exist(filename, 'file')
%%
X(:,:,1,1,1)
nm(:,:,1,1,1)

%% Map on an existing file. Writing dimension order : [1 2]

X = [1 2 3 4; 5 6 7 8]
nomFic = 'C:\Temp\TestMemmapfile12.bin';
fid = fopen(nomFic, 'w+');
for i2=1:size(X,2)
    for i1=1:size(X,1) % Dimension 1 first
        fwrite(fid, X(i1,i2), 'double');
    end
end
fclose(fid);

nm = cl_nmatrixfactory.createInstance('FileName', nomFic, 'Format', class(X), 'Size', size(X), 'Dim', [1 2])
%%

X(2,3)
nm(2,3)
%%
X(2,:)
nm(2,:)
%%
X(:,3)
nm(:,3)
%%
X(:,:)
nm(:,:)
%%
X(6)
nm(6)
%%
X([2 3 5 6 8])
nm([2 3 5 6 8])
%%
size(X)
size(nm)
%%
size(X,2)
size(nm,2)
%%
numel(X)
numel(nm)

%% Map on an existing file. Writing dimension order : [2 1]

X = [1 2 3 4; 5 6 7 8]
nomFic = 'C:\Temp\TestMemmapfile21.bin';
fid = fopen(nomFic, 'w+');
for i1=1:size(X,1)
    for i2=1:size(X,2) % Dimension 2 first
        fwrite(fid, X(i1,i2), 'double');
    end
end
fclose(fid);

nm = cl_nmatrixfactory.createInstance('FileName', nomFic, 'Format', class(X), 'Size', size(X), 'Dim', [2 1])
%%
X(2,3)
nm(2,3)
%%
X(2,:)
nm(2,:)
%%
X(:,3)
nm(:,3)
%%
X(:,:)
nm(:,:)
%%
X(6)
nm(6)
%%
X([2 3 5 6 8])
nm([2 3 5 6 8])
%%
size(X)
size(nm)
%%
size(X,2)
size(nm,2)
%%
numel(X)
numel(nm)

%% Create and initialize a 3D matrix with default mapped file

X = [1 2 3 4; 5 6 7 8];
X(:,:,2) = X(:,:,1) + 100;
X(:,:,3) = X(:,:,1) + 1000;
X

nm = cl_nmatrixfactory.createInstance('Value', X)
%%
X(2,3,1)
nm(2,3,1)
%%
X(2,3,2)
nm(2,3,2)
%%
X(2,:,3)
nm(2,:,3)
%%
X(:,3,2)
nm(:,3,2)
%%
X(2,3,:)
nm(2,3,:)
%%
X(:,3,:)
nm(:,3,:)
%%
X(2,:,:)
nm(2,:,:)
%%
X(:,:,:)
nm(:,:,:)
%%
X(6)
nm(6)
%%
X([2 3 5 6 8 ])
nm([2 3 5 6 8])
%%
size(X)
size(nm)
%%
size(X,2)
size(nm,2)
%%
numel(X)
numel(nm)

%% Map on an existing file, matrix 3D. Writing dimension order : [1 2 3]

X = [1 2 3 4; 5 6 7 8];
X(:,:,2) = X(:,:,1) + 100;
X(:,:,3) = X(:,:,1) + 1000;
X

nomFic = 'C:\Temp\TestMemmapfile123.bin';
fid = fopen(nomFic, 'w+');
for i3=1:size(X,3)
    for i2=1:size(X,2)
        for i1=1:size(X,1) % Dimension 1 first
            fwrite(fid, X(i1,i2,i3), 'double');
        end
    end
end
fclose(fid);

nm = cl_nmatrixfactory.createInstance('FileName', nomFic, 'Format', class(X), 'Size', size(X), 'Dim', [1 2 3]) %#ok<*NASGU>

%% Map on an existing file, matrix 3D. Writing dimension order : [2 1 3]

X = [1 2 3 4; 5 6 7 8];
X(:,:,2) = X(:,:,1) + 100;
X(:,:,3) = X(:,:,1) + 1000;
X

nomFic = 'C:\Temp\TestMemmapfile213.bin';
fid = fopen(nomFic, 'w+');
for i3=1:size(X,3)
    for i1=1:size(X,1)
        for i2=1:size(X,2)
            fwrite(fid, X(i1,i2,i3), 'double');
        end
    end
end
fclose(fid);

nm = cl_nmatrixfactory.createInstance('FileName', nomFic, 'Format', class(X), 'Size', size(X), 'Dim', [2 1 3])

%% Map on an existing file, matrix 3D. Writing dimension order : [2 3 1]

X = [1 2 3 4; 5 6 7 8];
X(:,:,2) = X(:,:,1) + 100;
X(:,:,3) = X(:,:,1) + 1000;
X

nomFic = 'C:\Temp\TestMemmapfile231.bin';
fid = fopen(nomFic, 'w+');
for i1=1:size(X,1)
    for i3=1:size(X,3)
        for i2=1:size(X,2)
            fwrite(fid, X(i1,i2,i3), 'double');
        end
    end
end
fclose(fid);

nm = cl_nmatrixfactory.createInstance('FileName', nomFic, 'Format', class(X), 'Size', size(X), 'Dim', [2 3 1])

%% Map on an existing file, matrix 3D. Writing dimension order : [3 2 1]

X = [1 2 3 4; 5 6 7 8];
X(:,:,2) = X(:,:,1) + 100;
X(:,:,3) = X(:,:,1) + 1000;
X

nomFic = 'C:\Temp\TestMemmapfile321.bin';
fid = fopen(nomFic, 'w+');
for i1=1:size(X,1)
    for i2=1:size(X,2)
        for i3=1:size(X,3)
            fwrite(fid, X(i1,i2,i3), 'double');
        end
    end
end
fclose(fid);

nm = cl_nmatrixfactory.createInstance('FileName', nomFic, 'Format', class(X), 'Size', size(X), 'Dim', [3 2 1])

%% Test BIL, BSQ, BIP image formats
%{
Image Dimensions 	Abbreviation 	Alternative Name
M-N-3 	BSQ 	Band-Sequential or Band-Interleaved
3-M-N 	BIP 	Band-Interleaved-by-Pixel or Pixel-Interleaved
M-3-N 	BIL 	Band-Interleaved-by-Line or Row-Interleaved

http://idlcoyote.com/ip_tips/where3.html
http://help.arcgis.com/fr/arcgisdesktop/10.0/help/index.html#//009t00000010000000
%}

%% Test BIL format

% X = etopo; % Matlab function but does not work.
X = Etopo; % Ifremer function, data obtained from http://www.ngdc.noaa.gov/mgg/global/global.html
X = ombrage(X);
figure; imagesc(X); axis equal; axis tight;
%%
% Write BIL file using Matlab function
filenameBil = fullfile(my_tempdir, 'ETOPO_BIL.bin');
multibandwrite(X, filenameBil, 'bil');
exist(filenameBil, 'file')
%%
% Check if BIL file is OK
Y = multibandread(filenameBil, size(X), class(X), 0, 'bil', 'ieee-le');
Y = cast(Y, class(X));
figure; imagesc(Y); axis equal; axis tight;
%%
nm = cl_nmatrixfactory.createInstance('FileName', filenameBil, 'Format', class(X), 'Size', size(X), 'Dim', [2 3 1]);
figure; imagesc(nm(:,:,:)); axis equal; axis tight;

%% Test BSQ format

% X = etopo; % Matlab function but does not work.
X = Etopo; % Ifremer function, data obtained from http://www.ngdc.noaa.gov/mgg/global/global.html
X = ombrage(X);
figure; imagesc(X); axis equal; axis tight;
%%
% Write BSQ file using Matlab function
filenameBsq = fullfile(my_tempdir, 'ETOPO_BSK.bin');
multibandwrite(X, filenameBsq, 'bsq');
exist(filenameBsq, 'file')
%%
% Check if BSK file is OK
Y = multibandread(filenameBsq, size(X), class(X), 0, 'bsq', 'ieee-le');
Y = cast(Y, class(X));
figure; imagesc(Y); axis equal; axis tight;
%%
nm = cl_nmatrixfactory.createInstance('FileName', filenameBsq, 'Format', class(X), 'Size', size(X), 'Dim', [2 1 3]);
figure; imagesc(nm(:,:,:)); axis equal; axis tight;

%% Test BIP format

% X = etopo; % Matlab function but does not work.
X = Etopo; % Ifremer function, data obtained from http://www.ngdc.noaa.gov/mgg/global/global.html
X = ombrage(X);
figure; imagesc(X); axis equal; axis tight;
%%
% Write BIP file using Matlab function
filenameBip = fullfile(my_tempdir, 'ETOPO_BIP.bin');
multibandwrite(X, filenameBip, 'bip');
exist(filenameBip, 'file')
%%
% Check if BIP file is OK
Y = multibandread(filenameBip, size(X), class(X), 0, 'bip', 'ieee-le');
Y = cast(Y, class(X));
figure; imagesc(Y); axis equal; axis tight;
%%
nm = cl_nmatrixfactory.createInstance('FileName', filenameBip, 'Format', class(X), 'Size', size(X), 'Dim', [3 2 1]);
figure; imagesc(nm(:,:,:)); axis equal; axis tight;

%% Create and initialize a 2D matrix with various parameters

nm = cl_nmatrixfactory.createInstance('Value', pi, 'Size', [2 4])
%%
nm = cl_nmatrixfactory.createInstance('Value', pi, 'Size', [2 4], 'Format', 'single')
%%
nm = cl_nmatrixfactory.createInstance('Value', pi, 'Size', [2 4], 'Format', 'uint8')
%%
nm = cl_nmatrixfactory.createInstance('Size', [2 4], 'Format', 'single')
 
%% Map of 2D and 3D matrices on a .mat file

E2D = Etopo; % Ifremer function, data obtained from http://www.ngdc.noaa.gov/mgg/global/global.html
E3D = ombrage(E2D);
filenameMat = fullfile(my_tempdir, 'ETOPO.mat');
save(filenameMat, 'E2D', 'E3D', '-v6')
exist(filenameMat, 'file')
%%
nm2D = cl_nmatrixfactory.createInstance('FileName', filenameMat, 'VarName', '/E2D');
figure; imagesc(nm2D(:,:)); axis equal; axis tight; colormap(jet(256));
%%
nm3D = cl_nmatrixfactory.createInstance('FileName', filenameMat, 'VarName', '/E3D');
figure; imagesc(nm3D(:,:,:)); axis equal; axis tight;
%%
clear nm2D nm3D

%% Loop performances on RAM

nomFic = getNomFicDatabase('Earth_LatLong_Bathymetry.ers');
[flag, b] = cl_image.import_ermapper(nomFic);
Earth = get(b, 'Image');
[nrows, ncols] = size(Earth);
%%
tic;
for ic=1:ncols
    X = Earth(:,ic);
end
toc
tic;
for il=1:nrows
    X = Earth(il,:);
end
toc

%% Loop performances on virtual matrix on HD 7500 

nm1 = cl_nmatrixfactory.createInstance('Value', Earth);
figure; imagesc(nm1(:,:)); colormap(jet(256));
%%
tic;
for il=1:100 %nrows
    X = nm1(il,:);
end
toc

tic;
for ic=1:100 %ncols
    X = nm1(:,ic);
end
toc

%% Loop performances on virtual matrix on SSD 

nm2 = cl_nmatrixfactory.createInstance('FileName', 'C:\Temp\Earth_LatLong_Bathymetry\Earth_LatLong_Bathymetry.bin', 'Format', class(Earth), 'Size', size(Earth), 'Dim', [2 1]);
figure; imagesc(nm2(:,:), [-12000 8000]); colormap(jet(256)); colorbar; axis equal; axis tight;
%%
tic;
for il=1:nrows
    X = nm2(il,:);
end
toc

tic;
for ic=1:ncols
    X = nm1(:,ic);
end
toc

%% Loop performances on Matlab virtual matrix 

% nm = cl_nmatrixfactory.createInstance('Value', Earth, 'Ext', '.mat') % En attente

% En attendant d'avoir la cr�ation incorpor�e � la classe
filenameMat = fullfile(my_tempdir, 'Earth.mat');
save(filenameMat, 'Earth', '-v6')

nm2D = cl_nmatrixfactory.createInstance('FileName', filenameMat, 'VarName', '/Earth');
figure; imagesc(nm2D(:,:)); axis equal; axis tight; colormap(jet(256));
%%
tic;
for il=1:nrows
    X = nm2D(il,:);
end
toc

tic;
for ic=1:ncols
    X = nm2D(:,ic);
end
toc

%% Create and initialize a 1D matrix with default mapped file

X = [1 2 3 4]
nm = cl_nmatrixfactory.createInstance('Value', X)
%%
X(:)
nm(:)
%%
X(2)
nm(2)
%%
X(2:3)
nm(2:3)
%%
nm = cl_nmatrixfactory.createInstance('Value', X', 'Dim', [2 1])

X(:)
nm(:)
%%
X(2)
nm(2)
%%
X(2:3)
nm(2:3)

%% Create and initialize a 2D matrix using a default .mat mapped file

X = [1 2 3 4; 5 6 7 8]
nm = cl_nmatrixfactory.createInstance('Value', X, 'Ext', '.mat')
%%
nm = cl_nmatrixfactory.createInstance('Value', X, 'Ext', '.mat', 'VarName', 'XXL')

%% Test directions

X = Etopo;
nm1 = cl_nmatrixfactory.createInstance('Value', X);
figure; imagesc(nm1(:,:)); colormap(jet(256));
%%
nm2 = cl_nmatrixfactory.createInstance('Value', flipud(X), 'Direction', [-1 1]); % TODO
figure; imagesc(nm2(:,:)); colormap(jet(256)); 
