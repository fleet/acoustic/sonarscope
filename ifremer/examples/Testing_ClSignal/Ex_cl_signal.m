%% Create instances of ClSignal
% Creating instances of ClSignal with or without parameters

%% Without input parameters
% Default values will be given to all of the instance's properties
sg1 = ClSignal %#ok<*NOPTS>

%% With input parameters
% The input values will be given to the parameters. If no input was set for
% a parameter, it will take its default value.
sg2 =  ClSignal('Name', 'Simple sinus function', 'YData', sin(0:0.1*pi:10*pi-0.1*pi), 'XUnit', 'rad', 'YUnit', 'W')

