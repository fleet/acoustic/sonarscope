%% Filters
% Summary of example objective

%% Apply filters to the square signal (works on every type of signal)
% All of these are initially identical
t = 0 : pi/10: 10*pi-pi/10;
squaresg1 = ClSignal('YData', square(t), 'XData', t, 'Color', 'k', 'Name', 'First Square Signal');

%%
squaresg2 = ClSignal('YData', square(t), 'XData', t, 'Color', 'r', 'Name', '2nd Square Signal');

%%
randsg1 = ClSignal('YData', rand(100,1), 'Color', 'k', 'Name', 'Random Signal');


%% * Butterworth filter :
% This function accepts as inputs the object, the order of the filter, the
% normalized cutoff frequency, and the type of filter (default : lowpass)

order = 4;
Wc = 0.3;


plot(squaresg1)
squaresg1 = filter_butter(squaresg1, order, Wc) %#ok<*NOPTS>
squaresg1.Name = 'First signal - Butterworth filter'

plot(squaresg1)



%% * Gaussian Filter :
% It accepts the object and a value for sigma
% Back to the sg1 signal (200 random values)


plot(squaresg2)
sigma = 5;
squaresg2 = filter_gauss(squaresg2, sigma)
squaresg2.Name = '2nd signal - Gaussian filter';

plot(squaresg2)

%% * Rectangle filter :


plot(randsg1)
window  = 1;
randsg1 = filter_rectangle(randsg1, window)
randsg1.Name = '3rd signal - Rectangle filter';

plot(randsg1)
