%% get
% Getting the value of one or more parameters
%%
sg1 = ClSignal('Name', 'Sine Cardinal', 'YData', sinc(0:0.1*pi:10*pi).', 'XUnit', 'rad', 'YUnit', 'dB', 'Color', 'r');

%% Getting one value
%With an output variable
a=get(sg1, 'Name');

%% Getting more than one value
[a,b,c,d] = get(sg1, 'Name', 'Color', 'XData', 'YData') %#ok<*NOPTS>

%% Using the results of the "get" command to use another function

var = 2*get(sg1, 'YData')
%%
plot(get(sg1, 'XData'), get(sg1, 'YData'), 'Color', get(sg1, 'Color'))



