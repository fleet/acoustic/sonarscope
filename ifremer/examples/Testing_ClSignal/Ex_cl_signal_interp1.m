%% interp1
% Interpolating a signal with rough edges

%%
% Description of first code block
sg1 = ClSignal('Name', '50 random values', 'YData', rand(50,1), 'XUnit', 'rad', 'YUnit', 'W', 'Color', 'm');
%%
plot(sg1)
%% Intetrpolating the signal
% The function needs :
% * The input signal
% * The finer abscissa definition
% * Optional : the interpolation method ('spline', 'linear', 'cubic', etc.)
%
finerAbsc = linspace(1,50,1000); %1000 values between 1 and 50

sg1 = interp1(sg1, finerAbsc, 'spline') %#ok<*NOPTS>

%%
sg1.Name = 'The interpolated signal'
plot(sg1)