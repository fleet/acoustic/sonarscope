%% isnan
% Checks if missing values (NaN) exist in an instance of ClSignal

%% Result with no missing values :
% Creating an instance of ClSignal and putting NaNs in it

sgNaN = ClSignal('YData', rand(10, 1), 'Color', 'g', 'Name', '10 random values') %#ok<*NOPTS>
%%
plot(sgNaN)
%%
% There are no missing values for the moment
isnan(sgNaN)
%% Result with missing values :
% Introducing NaNs into the signal :
l = length(sgNaN.YData);
r = randi([1 l], 1, randi([1 5], 1, 1));
sgNaN.YData(r) = NaN
sgNaN.Name = '10 random values with missing data';
plot(sgNaN)

%%
isnan(sgNaN)
