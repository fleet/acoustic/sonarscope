%%SUBTRACTION
%%
sg1 = ClSignal('YData', sin(0:0.1*pi:20*pi-0.1*pi), 'Color', 'r', 'YUnit', 'dB');
plot(sg1)
%%

sg2 = ClSignal('YData', (2*sin(0:0.1*pi:20*pi-0.1*pi))+rand(1,200), 'Color', 'g', 'YUnit', 'dB');
plot(sg2)
%%
sg1 = sg1-sg2;
%sg1.Name = 'sg1-sg2';
plot(sg1)
%%
sg1= 2-sg1;

%sg1.Name = '2-sg1';
plot(sg1)
%%
sg1 = sg1 - 6 %#ok<*NOPTS>
%sg1.Name = 'sg1 - 6';
plot(sg1)
%%
sg1 = 3 - sg1;
%sg1.Name = '3-sg1';
plot(sg1)