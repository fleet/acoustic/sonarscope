%% Multiplication
sg1 = ClSignal('YData', sin(0:0.1*pi:20*pi-0.1*pi), 'Color', 'r', 'YUnit', 'dB') %#ok<*NOPTS>
%%
plot(sg1)
sg2 = ClSignal('YData', (2*sin(0:0.1*pi:20*pi-0.1*pi))+rand(1,200), 'Color', 'g', 'YUnit', 'dB')
plot(sg2)
%%
% % Cannot be done on dB signals
sg1 = sg1/sg2;
%%
sg1.YUnit = 'W'
sg2.YUnit = 'm'
sg1.XUnit = 'm'
sg2.XUnit = 'km'
%%
sg1 = sg1/sg2;
plot(sg1)
%%
sg1 = sg1/4

plot(sg1)
%%
sg1 = 2/sg1
whos sg1
plot(sg1)
%%
sg1 = 2/sg2;