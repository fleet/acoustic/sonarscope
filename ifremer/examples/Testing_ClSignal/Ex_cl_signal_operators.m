%% Operators
% These function are overdefinitions of already existing +,-,*,/ functions.
% They allow the addition/subtraction/multiplication/division between two
% signals, or between a signal and a scalar. Some conditions have to be
% respected in order for the operation to be done.

% For example, addition or subtraction between signals that have different
% units (both abscissa and ordinate) cannot be done :
sg1 = ClSignal('YData', sin(0:0.1*pi:20*pi-0.1*pi), 'Color', 'r', 'YUnit', 'dB') %#ok<*NOPTS>
sg2 = ClSignal('YData', (2*sin(0:0.1*pi:20*pi-0.1*pi))+rand(1,200), 'Color', 'g', 'YUnit', 'dB')
%%
set(sg1, 'YUnit', 'dB')
set(sg2, 'YUnit', 'W')

a=sg1+sg2;
%%
set(sg1, 'YUnit', 'dB');
set(sg2, 'YUnit', 'dB');
plot(sg1)
plot(sg2)
sg1=sg1+sg2;
sg1.Name = 'Sum between sg1 and sg2';
plot(sg1)
%%
sg1 = 2+sg1;
sg1.Name = '2+sg1';
plot(sg1)
%%
sg1 = sg1 + 6;
sg1.Name = 'sg1 + 6';
plot(sg1)




