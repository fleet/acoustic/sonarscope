%% set
%%
sg1 = ClSignal('Name', 'toto') %#ok<*NOPTS>

n=get(sg1, 'Name')
%pwd
%% Setting values using the "set" method

% With output parameters
 sg1 = set(sg1, 'Name', 'Sine Cardinal', 'YData', sinc(0:0.1*pi:10*pi), 'XUnit', 'rad', 'YUnit', 'dB', 'Color', 'r')
 plot(sg1);

%%
% Without output parameters
 set(sg1, 'Name', '2xSine Cardinal', 'YData', 2*sinc(0:0.1*pi:10*pi), 'XUnit', 'rad', 'YUnit', 'dB', 'Color', 'r')
 plot(sg1);