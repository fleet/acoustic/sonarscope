%% subsref
% Getting one single parameter value from an instance of ClSignal, and
% storing it in a variable
%%
sg1 = ClSignal('Name', 'Sine Cardinal', 'YData', sinc(0:0.1*pi:10*pi), 'XUnit', 'rad', 'YUnit', 'dB', 'Color', 'r');
%% Storing the value in a variable
%
a=sg1.Name; %#ok<NASGU>
b=sg1.YData;

%% Using the value in another function
a = 2*sg1.YData %#ok<NOPTS>
plot(sg1.XData, sg1.YData, sg1.XData,a)

