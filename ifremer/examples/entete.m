% % Description
% %   Descriptif g�n�ral de la fonction (texte qui appara�tra dans le "Current Directory" de l'environnement matlab)
% %   Compl�ment de la description (n'appara�tra pas dans le "Current Directory")
% %
% % Syntax
% %   [flag, a, b, c] = entete(x, y, ...)
% %
% % Input Arguments
% %   x : Description du param�tre (unit�)
% %   y : Description du param�tre (unit�)
% %
% % Optional Input Arguments
% %   Param�tres optionels tels qu'utilis�s dans des fonctions matlab comme
% %   interp1. ON EVITE CE TYPE DE PASSAGE DE PARAMETRES dans IfremerToolbox
% %
% % Name-Value Pair Arguments
% %   AgeCapitaine : Valeur du param�tre (unit�) (Default : 77)
% %   Saison       : {'hiver'} | 'printemps' | 'ete' | 'AUTOMNE'
% %   NomFonction  : Nom de la fonction (Default : 'sin')
% %   method       : {'nearest'} | 'linear' | 'spline' | 'pchip' | 'cubic'
% %   DetailsOn    : {0} = Aucun detail |
% %                    1 = d�tails sur Syntax
% %                    2 = d�tails sur Input Arguments
% %                    3 = d�tails sur Optional Input Arguments
% %                    4 = d�tails sur Name-Value Pair Arguments
% %                    5 = d�tails sur Name-only Arguments
% %                    6 = d�tails sur Output Arguments
% %                    7 = d�tails sur Remarks
% %                    8 = d�tails sur Examples
% %                    9 = d�tails sur See also
% %                   10 = d�tails sur References
% %                   11 = d�tails sur Authors
% %                   12 = d�tails sur VERSION
% %
% % Name-only Arguments
% %   extrap : Extrapolation
% %
% % Output Arguments
% %   [] : Auto-plot activation
% %   flag : 0=KO, 1=OK
% %   a    : Description du param�tre (unit�).
% %   b    : Description du param�tre (unit�).
% %   c    : Description du param�tre (unit�).
% %
% % More About : Remarques particuli�res concernant la fonction
% %
% % Examples
% %   x = 0:pi/360:pi; y = 1;
% %   a = entete(x, y)
% %   [a, b, c] = entete(x, y, 'AgeCapitaine', 77, 'Saison', 'ete', 'method', 'linear', 'extrap')
% %   a = entete(x, y, 'DetailsOn', 1);
% %
% % More about : r�f�rences de l'article d'o� est tir�e cette fonction
% % Authors : JMA + YYY
% % See also toto titi
% %
% % Reference pages in Help browser
% %   <a href="matlab:doc entete">entete</a>
% %   You have to edit entete.m to see the syntax for reference help
% %-------------------------------------------------------------------------------

function entete

help entete
