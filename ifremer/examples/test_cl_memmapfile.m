% Test de performances de cl_memmapfile 
% 
% Syntax
%   test_cl_memmapfile
%
% Examples 
%   test_cl_memmapfile
%
% See also cl_memmapfile Authors
% Authors : JMA
%------------------------------------------------------------------------------------------

function test_cl_memmapfile

nomFic = my_tempname('.bin');

%% Matrices Matlab

X1 = [1 2 3 4]' %#ok<*NOPRT>
%%
size(X1)
%%
saveVar(X1, nomFic)
%%
Y1 = spy(nomFic) %#ok<*NASGU>

%%
X2 = [1 2 3 4]
%%
size(X2)
%%
saveVar(X2, nomFic)
%%
Y2 = spy(nomFic)

%%
X12 = [1 2 3 4; 5 6 7 8]'
%%
size(X12)
%%
saveVar(X12, nomFic)
%%
Y12 = spy(nomFic)

%%
X21 = [1 2 3 4; 5 6 7 8]
%%
size(X21)
%%
saveVar(X21, nomFic)
%%
Y21 = spy(nomFic)

%%
X123(:,:,1) = [1 2 3 4; 5 6 7 8]';
X123(:,:,2) = X123(:,:,1) + 8;
X123(:,:,3) = X123(:,:,1) + 16
%%
size(X123)
%%
saveVar(X123, nomFic)
%%
Y123 = spy(nomFic)


%% Avec cl_memmapfile

nomFic = my_tempname('.bin');

M1 = cl_memmapfile('FileName', nomFic, 'Value', X1)
%%
size(M1)
%%
% Y1 = spy(nomFic)
%%
clear M1

%%
M2 = cl_memmapfile('FileName', nomFic, 'Value', X2)
%%
size(M2)
%%
% Y2 = spy(nomFic);
%%
clear M2

%%
M12 = cl_memmapfile('FileName', nomFic, 'Value', X12)
%%
size(M12)
%%
% Y12 = spy(nomFic)
%%
clear M12

%%
M21 = cl_memmapfile('FileName', nomFic, 'Value', X21)
%%
size(M21)
%%
% Y21 = spy(nomFic)
%%
clear M21

%%
M123 = cl_memmapfile('FileName', nomFic, 'Value', X123)
%%
size(X123)
%%
% Y123 = spy(nomFic)
%%
% clear M123

%% Test index

X123(1:2:end)
%%
M123(1:2:24)
%%
X123(1:2,:,2)
%%
M123(1:2,:,2)
%%
X123(:,2,1)
%%
M123(:,2,1)
%%
X123(1,2,:)
%%
M123(1,2,:)

%% Simulation fichier �crit par un logiciel quelconque

nomFic = my_tempname('.bin');
fid = fopen(nomFic, 'w+');
fwrite(fid, 1:24, 'single');
fclose(fid);

m = cl_memmapfile('FileName', nomFic, 'FileCreation', 0, ...
                'Format', 'single', 'Size', [2 4 3]);

m = cl_memmapfile('FileName', nomFic, 'FileCreation', 0, ...
                'Format', 'single', 'Size', [8 3], 'ErMapper', 1);
m(:,:)

function saveVar(X, nomFic)
fid = fopen(nomFic, 'w+');
fwrite(fid, X);
fclose(fid);

function X = spy(nomFic)
fid = fopen(nomFic, 'r');
X = fread(fid);
fclose(fid);
X = X';
