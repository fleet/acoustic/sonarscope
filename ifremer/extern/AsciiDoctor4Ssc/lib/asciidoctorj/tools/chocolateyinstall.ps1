﻿
$ErrorActionPreference = 'Stop';

$adocjVersion = '1.6.2'
$packageName= 'asciidoctorj'
$toolsDir   = "$(Split-Path -parent $MyInvocation.MyCommand.Definition)"
$url        = "https://dl.bintray.com/asciidoctor/maven/org/asciidoctor/asciidoctorj/$($adocjVersion)/asciidoctorj-$($adocjVersion)-bin.zip"

$packageArgs = @{
  packageName   = $packageName
  unzipLocation = $toolsDir
  url           = $url
  checksum      = 'b42f0efc4f61c57c5638b6e3aff746bb4df825c0d1824ad3065763e9aed2945c'
  checksumType  = 'sha256'
}


Install-ChocolateyZipPackage @packageArgs

Install-BinFile -name 'asciidoctorj' -path "$toolsDir\asciidoctorj-$($adocjVersion)\bin\asciidoctorj.bat"
