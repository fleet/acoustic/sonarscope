#ifndef ALL_DATAGRAMS_H_
#define ALL_DATAGRAMS_H_

#include "stdio.h"

#include "Generic_Utilities.h"    // Contenu dans la librairie libCONVERT_utils.a

// Description des contenants de variables �l�mentaire de description XML
// Peut servir � d�crire des attributs dont l'�criture au format XML est
// limit�e.
typedef struct {
	   char				cNom[50],			// Label de la variable
					    cTag[50],			// Tag identifiant de la variable
					    cUnit[50],			// Unit� de la variable
						cType[30],			// Type WORD, String, ...
						cNomPathFileBin[300];	// Nom et chemin du fichier Bin
	   FILE				*ptr_fpBin;			// Pointeur du fichier de signaux
	   BOOL 			bFlagConstant;		// Indicateur de constance de la variable
	   unsigned short	usSize;				// n octets
	   unsigned short	usNbElem;			// Nombre de variables �l�mentaires
	   void				*ptr_vPrevVal;		// Valeur pr�c�dente de lecture
	   float			fScaleFactor;		// yVraie = Val*S_F + A_O
	   float			fAddOffset;			//
} T_ALLVARXML;

typedef struct {
	   T_ALLVARXML	*strTabSignalXML;		// Description des signaux d'un paquet
	   T_ALLVARXML	*strTabImageXML;		// Description des images d'un paquet
	   T_ALLVARXML	*strTabImage2XML;		// Description des 2i�me images d'un paquet (
	   char			*ptr_cNomHeader,		// Nom du Paquet
					*ptr_cLabelHeader,		// Label du paquet
					*ptr_cCommentsHeader,	// Commentaires du paquet
	   				*ptr_cModelHeader,		// Mod�le du syst�me.
	   				*ptr_cSerialNumberHeader,// SN du syst�me.
				    *ptr_cNomIndexTable,	// Nom de la variable d'indexation des
											// variables de type table.
					*ptr_cNomIndexTable2;	// Nom de la variable d'indexation des
	   										// variables de type table (2i�me image).
	   int			iNbDatagram;			// Nombre d'occurrence des donn�es.
	   short		sNbVarSignal;			// Nombre de variables de type signal.	short		sNb;	// Nombre de signaux du RD
	   short		sNbVarImage;			// Nombre de variables de type image.
	   short		sNbVarImage2;			// Nombre de variables de type image (pour la 2i�me image).

} T_ALLRECORDXML;

typedef struct
{
	char 	cType;
	int		iNbTimes;
} T_TYPEOFDATAGRAMNOTTRANSLATE;


#endif
