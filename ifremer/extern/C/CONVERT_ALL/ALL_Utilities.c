/*
 * ALL_utilities.c
 *
 *  Created on: 6 nov. 2008
 *      Author: rgallou
 */
#include<string.h>
#include<stdio.h>
#include<stdlib.h>
#include<errno.h>
#include<io.h>

#include"ALL_utilities.h"
#include"convertFiles_ALL.h"


off64_t All_trouveDatagram(	FILE *fp,
						unsigned char *ucTypeDatagramChercher,
						int iPositionDebDatagram)
{
	int iNbOfBytesInDatagram = 0,
		iVal,
		iNbBlocsLus;

	unsigned char   ucSTX,
		            ucTypeOfDatagram[1];

	off64_t	llPositionDatagram;

	iNbBlocsLus = fread(&iNbOfBytesInDatagram, sizeof(int), 1, fp);  // Int32
	iNbBlocsLus = fread(&ucSTX, sizeof(unsigned char), 1, fp);
	iNbBlocsLus = fread(ucTypeOfDatagram, sizeof(unsigned char), 1, fp);

	while(ucTypeOfDatagram[0] != ucTypeDatagramChercher[0])
	{
		iPositionDebDatagram = iPositionDebDatagram + iNbOfBytesInDatagram + 4;
		// Positionnement en d�but de fichier + offset.
		fseeko64(fp,iPositionDebDatagram,SEEK_SET);

		iNbBlocsLus = fread(&iVal, sizeof(int), 1, fp);  // Int32
		iNbOfBytesInDatagram = iVal;

		iNbBlocsLus = fread(&ucSTX, sizeof(unsigned char), 1, fp);

		iNbBlocsLus = fread(ucTypeOfDatagram, sizeof(unsigned char), 1, fp);

	}

	llPositionDatagram = iPositionDebDatagram;

	printf("Position cur %lld\n",ftello64(fp));

	printf("Lecture du fichier .all: dans trouveDatagram!\n");
	printf("NbOfBytesInDatagram = %d\n",iNbOfBytesInDatagram);
	printf("STX = %c\n",ucSTX);
	printf("TypeOfDatagram = %c\n",ucTypeOfDatagram[0]);
	printf("NbBlocsLus = %d\n",iNbBlocsLus);
	printf("llPositionDatagram = %lld\n",llPositionDatagram);
	printf("\n");

	return llPositionDatagram;
}


///////////////////////////////////////////////////////////////////////////////////////////
//// Fonction :	utilALL_cValVarFromFile
//// Objet 	:	Relit la valeur depuis le fichier Bin.
//// Modification : 12/11/08
//// Auteur 	: GLU
///////////////////////////////////////////////////////////////////////////////////////////
//int utilALL_cValVarFromFile(T_ALLVARXML strALLVarXML,
//							char *cValeur)
//{
//	unsigned short		usTailleEnByte,
//						usNbElem;
//
//	void 				*ptr_vValeur;
//
//	int 				iRet = 0;
//
//
//	usTailleEnByte= util_usNbOctetTypeVar(strALLVarXML.cType);
//	usNbElem = strALLVarXML.usNbElem;
//
//	ptr_vValeur = (void*)malloc(usTailleEnByte*usNbElem*sizeof(char));
//	strALLVarXML.ptr_fpBin = fopen(strALLVarXML.cNomPathFileBin, "r+b");
//	if (!strALLVarXML.ptr_fpBin)
//	{
//	   printf("%s -- Error in retrieve Signal constant value   : %s\n", __FILE__, strALLVarXML.cNomPathFileBin);
//	   iRet = -1;
//	}
//	else
//	{
//		fread(ptr_vValeur, usTailleEnByte, usNbElem, strALLVarXML.ptr_fpBin);
//
//		iRet = util_cValVarConstant(	strALLVarXML.cType,
//										ptr_vValeur,
//										(int)usNbElem,
//										cValeur);
//		fclose(strALLVarXML.ptr_fpBin);
//		//On lib�re uniquement pour des donn�es de type non-char.
//		// sinon plantage (????)
//		//if (strcmp(strALLVarXML.cType, "char"))
//		//{
//			free(ptr_vValeur);
//		//}
//	}
//
//	return iRet;
//
//}	// utilALL_cValVarFromFile

//////////////////////////////////////////////////////////////////////
// Fonction :    utilALL_iSaveAndCloseFiles
// Objet     :    Sauvegarde de la structure XML dans un fichier.
// Modification : 15/10/08
// Auteur     : GLU
//////////////////////////////////////////////////////////////////////
int utilALL_iSaveAndCloseFiles( char *cRepData,
                                T_ALLRECORDXML *strALLRecordXML)
{
    char    *cNomFicXML,
            *ptr_cNomHeader,
            *cTypeMatLab,
            cNomFicBin[200],
            *cValeur = NULL,
            cValOneChar[1];

    int     iSize,
            iVarSignal,
            iVarImage,
            iRet = 0;

    unsigned short     usNbElem,
                    usTailleEnByte;

    FILE        *fpXML = NULL;

    iVarSignal = 3;    // D�signation du EmModel
    strALLRecordXML->ptr_cModelHeader = (char*)malloc(256*sizeof(char));
    fclose(strALLRecordXML->strTabSignalXML[iVarSignal].ptr_fpBin);
//    iRet = utilALL_cValVarFromFile(    strALLRecordXML->strTabSignalXML[iVarSignal],
//                                    strALLRecordXML->ptr_cModelHeader);

	iRet = util_cValVarFromFile(strALLRecordXML->strTabSignalXML[iVarSignal].cNomPathFileBin,
								strALLRecordXML->strTabSignalXML[iVarSignal].cType,
								strALLRecordXML->strTabSignalXML[iVarSignal].usNbElem,
								strALLRecordXML->ptr_cModelHeader);

    iVarSignal = 7;    // D�signation du System Serial Number
    strALLRecordXML->ptr_cSerialNumberHeader = (char*)malloc(256*sizeof(char));
    fclose(strALLRecordXML->strTabSignalXML[iVarSignal].ptr_fpBin);
//    iRet = utilALL_cValVarFromFile(    strALLRecordXML->strTabSignalXML[iVarSignal],
//                                    strALLRecordXML->ptr_cSerialNumberHeader);
	iRet = util_cValVarFromFile(strALLRecordXML->strTabSignalXML[iVarSignal].cNomPathFileBin,
								strALLRecordXML->strTabSignalXML[iVarSignal].cType,
								strALLRecordXML->strTabSignalXML[iVarSignal].usNbElem,
								strALLRecordXML->ptr_cSerialNumberHeader);


    iSize = strlen(strALLRecordXML->ptr_cNomHeader) + 1;
    ptr_cNomHeader = (char *)malloc(iSize*sizeof(char));
    ptr_cNomHeader = strALLRecordXML->ptr_cNomHeader;
    iSize = 5 + strlen(cRepData) + 2 + strlen(ptr_cNomHeader) + 4; // Nom du paquet + '.xml'
    cNomFicXML = calloc(iSize, sizeof(char));
    // Si Extension du fichier d'origine = '.All'
	if (!stricmp(G_cFileExtension, "all"))
	    sprintf(cNomFicXML, "%s\\ALL_%s%s", cRepData, ptr_cNomHeader, ".xml");
	// Si Extension = '.wcd'
	else if (!stricmp(G_cFileExtension, "wcd"))
	    sprintf(cNomFicXML, "%s\\WCD_%s%s", cRepData, ptr_cNomHeader, ".xml");
	else
	{
		printf("L'extension du fichier n'est pas reconnue : %s\n", G_cFileExtension);
		return EXIT_FAILURE;
	}
    printf("-- XML File Creation : %s \n", cNomFicXML);


    // Cr�ation de l'ent�te de doc XML
    fpXML = fopen(cNomFicXML, "w+");
    fprintf(fpXML, "%s", "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r");
    fprintf(fpXML, "%s", "<ROOT>\r");
    fprintf(fpXML, "%s%s%s", "\t<Title>", strALLRecordXML->ptr_cLabelHeader, "</Title>\r");
    fprintf(fpXML, "%s%s%s", "\t<Constructor>", "Simrad", "</Constructor>\r");
    fprintf(fpXML, "%s%s%s", "\t<Model>", strALLRecordXML->ptr_cModelHeader, "</Model>\r");
    fprintf(fpXML, "%s%s%s", "\t<SystemSerialNumber>", strALLRecordXML->ptr_cSerialNumberHeader, "</SystemSerialNumber>\r");
    fprintf(fpXML, "%s%d%s", "\t<NbDatagrams>", strALLRecordXML->iNbDatagram, "</NbDatagrams>\r");
    fprintf(fpXML, "%s%s%s", "\t<TimeOrigin>", "01/01/-4713", "</TimeOrigin>\r");
    fprintf(fpXML, "%s%s%s", "\t<Comments>", strALLRecordXML->ptr_cCommentsHeader, "</Comments>\r");
    if (strALLRecordXML->sNbVarSignal > 0)
    {
        fprintf(fpXML, "%s", "\t<Variables>\r");

        // Traitement sur l'ensemble des valeurs du RTH.
        for (iVarSignal=0; iVarSignal<strALLRecordXML->sNbVarSignal; iVarSignal++)
        {
            cTypeMatLab = (char*)malloc(10*sizeof(char));
            iRet = util_cConvertTypeLabel(    strALLRecordXML->strTabSignalXML[iVarSignal].cType,
                                            cTypeMatLab);

            fprintf(fpXML, "%s", "\t\t<item>\r");
            fprintf(fpXML, "%s%s%s", "\t\t\t<Name>", strALLRecordXML->strTabSignalXML[iVarSignal].cNom, "</Name>\r");
            fprintf(fpXML, "%s%s%s", "\t\t\t<Storage>", cTypeMatLab, "</Storage>\r");
            if (strALLRecordXML->strTabSignalXML[iVarSignal].usNbElem > 1)
            {
                fprintf(fpXML, "%s%d%s", "\t\t\t<NbElem>",  strALLRecordXML->strTabSignalXML[iVarSignal].usNbElem, "</NbElem>\r");
            }
            //Infos report�e dans format SSC-XML-Bin : fprintf(fpXML, "%s%s%s", "\t\t\t<Unit>", strALLRecordXML->strTabSignalXML[iVarSignal].cUnit, "</Unit>\r");
            //Infos report�e dans format SSC-XML-Bin : fprintf(fpXML, "%s%s%s", "\t\t\t<Tag>", strALLRecordXML->strTabSignalXML[iVarSignal].cTag, "</Tag>\r");

            // Fermeture pr�alable du fichier.
            if (strALLRecordXML->strTabSignalXML[iVarSignal].ptr_fpBin != NULL)
            {
            	iRet = fclose(strALLRecordXML->strTabSignalXML[iVarSignal].ptr_fpBin);
            }

            // R�cup�ration de la variable d�tect�e comme constante.
            usNbElem = strALLRecordXML->strTabSignalXML[iVarSignal].usNbElem;
            usTailleEnByte = util_usNbOctetTypeVar(strALLRecordXML->strTabSignalXML[iVarSignal].cType);

            if (strALLRecordXML->strTabSignalXML[iVarSignal].bFlagConstant == TRUE)
            {
                // Pb de lib�ration de m�moire si  usNbElem * usTailleEnByte = 1
            	if (usNbElem != 1 || usTailleEnByte != 1)
                {
					 // Allocation de la valeur � r�cup�rer selon la taille. Si elle est
					 // du type Chaine, elle doit contenir autant d'�l�ments que d�clar�s.
					 if (strcmp(cTypeMatLab, "char"))
					 {
						 cValeur = (char*)malloc(256*sizeof(char));
					 }
					 else
					 {
						 cValeur = (char*)malloc(usNbElem*sizeof(char));
					 }
//                     iRet = utilALL_cValVarFromFile(strALLRecordXML->strTabSignalXML[iVarSignal],
//                                                     cValeur);
                 	 iRet = util_cValVarFromFile(strALLRecordXML->strTabSignalXML[iVarSignal].cNomPathFileBin,
                 								strALLRecordXML->strTabSignalXML[iVarSignal].cType,
                 								strALLRecordXML->strTabSignalXML[iVarSignal].usNbElem,
                 								cValeur);
                     fprintf(fpXML, "%s%s%s", "\t\t\t<Constant>", cValeur, "</Constant>\r");
					 free(cValeur);
                }
                else
                {
//                    iRet = utilALL_cValVarFromFile(strALLRecordXML->strTabSignalXML[iVarSignal],
//                    		&cValOneChar[0]);
                	iRet = util_cValVarFromFile(strALLRecordXML->strTabSignalXML[iVarSignal].cNomPathFileBin,
                								strALLRecordXML->strTabSignalXML[iVarSignal].cType,
                								strALLRecordXML->strTabSignalXML[iVarSignal].usNbElem,
                								&cValOneChar[0]);
                    fprintf(fpXML, "%s%s%s", "\t\t\t<Constant>", &cValOneChar[0], "</Constant>\r");
                }
 				// Effacement du fichier Bin
            	iRet = remove((const char *)strALLRecordXML->strTabSignalXML[iVarSignal].cNomPathFileBin);
				if (iRet !=0)
				{
				   printf("Cannot remove file %s (%s)\n",
							   strALLRecordXML->strTabSignalXML[iVarSignal].cNomPathFileBin, ERRORSTRING);
				}
            }
            else
            {
                sprintf(cNomFicBin, "%s%s",strALLRecordXML->strTabSignalXML[iVarSignal].cNom, ".bin");
                fprintf(fpXML, "%s%s%s", "\t\t\t<FileName>", cNomFicBin, "</FileName>\r");
            }
            if (strALLRecordXML->strTabSignalXML[iVarSignal].fScaleFactor != 1.0)
				fprintf(fpXML, "%s%g%s", "\t\t\t<ScaleFactor>", strALLRecordXML->strTabSignalXML[iVarSignal].fScaleFactor, "</ScaleFactor>\r");
            if (strALLRecordXML->strTabSignalXML[iVarSignal].fAddOffset != 0.0)
				fprintf(fpXML, "%s%f%s", "\t\t\t<AddOffset>", strALLRecordXML->strTabSignalXML[iVarSignal].fAddOffset, "</AddOffset>\r");
            fprintf(fpXML, "%s", "\t\t</item>\r");
            free(cTypeMatLab);

        } // Fin de la boucle sur les donn�es RTH
        fprintf(fpXML, "%s", "\t</Variables>\r");
        free(strALLRecordXML->strTabSignalXML);
    } // Fin de l'�criture des Signaux

    // Ouverture de la balise Tables
    if (strALLRecordXML->sNbVarImage > 0 || strALLRecordXML->sNbVarImage2 >0)
    {
        fprintf(fpXML, "%s", "\t<Tables>\r");
    }
    // Traitement sur l'ensemble des attributs.
    if (strALLRecordXML->sNbVarImage > 0)
    {
         for (iVarImage=0; iVarImage<strALLRecordXML->sNbVarImage; iVarImage++)
        {
            cTypeMatLab = (char*)malloc(10*sizeof(char));
            iRet = util_cConvertTypeLabel(    strALLRecordXML->strTabImageXML[iVarImage].cType,
                                            cTypeMatLab);

            fprintf(fpXML, "%s", "\t\t<item>\r");
            fprintf(fpXML, "%s%s%s", "\t\t\t<Name>", strALLRecordXML->strTabImageXML[iVarImage].cNom, "</Name>\r");
            fprintf(fpXML, "%s%s%s", "\t\t\t<Storage>", cTypeMatLab, "</Storage>\r");
            if (strALLRecordXML->strTabImageXML[iVarImage].usNbElem > 1)
            {
                fprintf(fpXML, "%s%d%s", "\t\t\t<NbElem>", strALLRecordXML->strTabImageXML[iVarImage].usNbElem, "</NbElem>\r");
            }
            //Infos report�e dans format SSC-XML-Bin : fprintf(fpXML, "%s%s%s", "\t\t\t<Unit>", strALLRecordXML->strTabImageXML[iVarImage].cUnit, "</Unit>\r");
            //Infos report�e dans format SSC-XML-Bin : fprintf(fpXML, "%s%s%s", "\t\t\t<Tag>", strALLRecordXML->strTabImageXML[iVarImage].cTag, "</Tag>\r");
            // Fermeture pr�alable du fichier.
            fclose(strALLRecordXML->strTabImageXML[iVarImage].ptr_fpBin);
            usNbElem = strALLRecordXML->strTabImageXML[iVarImage].usNbElem;
            usTailleEnByte = util_usNbOctetTypeVar(strALLRecordXML->strTabImageXML[iVarImage].cType);
            if (strALLRecordXML->ptr_cNomIndexTable != NULL)
            {
				// Traitement particulier sur la variable SampleAmplitude du paquet WaterColumn et
				// 						  sur les variables Amplitude et Phase du paquet Raw Beam Samples
				if ((!strcmp(strALLRecordXML->strTabImageXML[iVarImage].cNom, "StaveBackscatter") &&
					 !strcmp(ptr_cNomHeader, "StaveData")))
				{
					fprintf(fpXML, "%s%s%s", "\t\t\t<Indexation>", "NbSamples, NbStavesPerHead", "</Indexation>\r");
				}
				else
				{
					fprintf(fpXML, "%s%s%s", "\t\t\t<Indexation>", strALLRecordXML->ptr_cNomIndexTable, "</Indexation>\r");
				}
            }

            if (strALLRecordXML->strTabImageXML[iVarImage].bFlagConstant == TRUE)
            {
                // Pb de lib�ration de m�moire si  usNbElem * usTailleEnByte = 1
            	if (usNbElem != 1 || usTailleEnByte != 1)
            	{
                	// Allocation de la valeur � r�cup�rer selon la taille. Si elle est
                	// du type Chaine, elle doit contenir autant d'�l�ments que d�clar�s.
                    // Ca marche mais c'est moyen, moyen !!!! (cf. Traitement des signaux)
                	if (strcmp(cTypeMatLab, "char"))
                    {
                        cValeur = (char*)malloc(256*sizeof(char));
                    }
                    else
                    {
                        cValeur = (char*)malloc(usNbElem*sizeof(char));
                    }
                    //iRet = utilALL_cValVarFromFile(strALLRecordXML->strTabImageXML[iVarImage],
                    //                            cValeur);
                	iRet = util_cValVarFromFile(strALLRecordXML->strTabImageXML[iVarImage].cNomPathFileBin,
                								strALLRecordXML->strTabImageXML[iVarImage].cType,
                								strALLRecordXML->strTabImageXML[iVarImage].usNbElem,
                								cValeur);
                    fprintf(fpXML, "%s%s%s", "\t\t\t<Constant>", cValeur, "</Constant>\r");

     				if (!strcmp(cTypeMatLab, "char"))
    				// ???? : pb de lib�ration de memoire si la variable fait un octet.
    				{
    					free(cValeur);
    				}
            	}
 				else
 				{
                    //iRet = utilALL_cValVarFromFile(strALLRecordXML->strTabImageXML[iVarImage],
                    //		&cValOneChar[0]);
                   	iRet = util_cValVarFromFile(strALLRecordXML->strTabImageXML[iVarImage].cNomPathFileBin,
                    								strALLRecordXML->strTabImageXML[iVarImage].cType,
                    								strALLRecordXML->strTabImageXML[iVarImage].usNbElem,
                    								&cValOneChar[0]);
                    fprintf(fpXML, "%s%s%s", "\t\t\t<Constant>", &cValOneChar[0], "</Constant>\r");
 				}
                // Effacement du fichier
 				if (remove((const char *)strALLRecordXML->strTabImageXML[iVarImage].cNomPathFileBin) !=0)
 				{
 				   printf("Cannot remove file %s (%s)\n",
 							   strALLRecordXML->strTabImageXML[iVarImage].cNomPathFileBin, ERRORSTRING);
 				}
            }
            else
            {
                sprintf(cNomFicBin, "%s%s",strALLRecordXML->strTabImageXML[iVarImage].cNom, ".bin");
                fprintf(fpXML, "%s%s%s", "\t\t\t<FileName>", cNomFicBin, "</FileName>\r");
            }
            if (strALLRecordXML->strTabImageXML[iVarImage].fScaleFactor != 1.0)
            	fprintf(fpXML, "%s%g%s", "\t\t\t<ScaleFactor>", strALLRecordXML->strTabImageXML[iVarImage].fScaleFactor, "</ScaleFactor>\r");
            if (strALLRecordXML->strTabImageXML[iVarImage].fAddOffset != 0.0)
            	fprintf(fpXML, "%s%f%s", "\t\t\t<AddOffset>", strALLRecordXML->strTabImageXML[iVarImage].fAddOffset, "</AddOffset>\r");
            fprintf(fpXML, "%s", "\t\t</item>\r");
            free(cTypeMatLab);
        } // Fin de la boucle sur les donn�es Image
        free(strALLRecordXML->strTabImageXML);
        } //Fin de l'�criture des Images (RD et OD)

    // Traitement d'une 2i�me image (si n�cessaire).
	if (strALLRecordXML->sNbVarImage2 > 0)
	{
		for (iVarImage=0; iVarImage<strALLRecordXML->sNbVarImage2; iVarImage++)
		{
			cTypeMatLab = (char*)malloc(10*sizeof(char));
			iRet = util_cConvertTypeLabel(strALLRecordXML->strTabImage2XML[iVarImage].cType,
											cTypeMatLab);

			fprintf(fpXML, "%s", "\t\t<item>\r");
			fprintf(fpXML, "%s%s%s", "\t\t\t<Name>", strALLRecordXML->strTabImage2XML[iVarImage].cNom, "</Name>\r");
			fprintf(fpXML, "%s%s%s", "\t\t\t<Storage>", cTypeMatLab, "</Storage>\r");
			if (strALLRecordXML->strTabImage2XML[iVarImage].usNbElem > 1)
			{
				fprintf(fpXML, "%s%d%s", "\t\t\t<NbElem>", strALLRecordXML->strTabImage2XML[iVarImage].usNbElem, "</NbElem>\r");
			}
			//Infos report�e dans format SSC-XML-Bin : fprintf(fpXML, "%s%s%s", "\t\t\t<Unit>", strALLRecordXML->strTabImage2XML[iVarImage].cUnit, "</Unit>\r");
			//Infos report�e dans format SSC-XML-Bin : fprintf(fpXML, "%s%s%s", "\t\t\t<Tag>", strALLRecordXML->strTabImage2XML[iVarImage].cTag, "</Tag>\r");
			// Fermeture pr�alable du fichier.
			fclose(strALLRecordXML->strTabImage2XML[iVarImage].ptr_fpBin);
			usNbElem = strALLRecordXML->strTabImage2XML[iVarImage].usNbElem;
			usTailleEnByte = util_usNbOctetTypeVar(strALLRecordXML->strTabImage2XML[iVarImage].cType);
			if (strALLRecordXML->ptr_cNomIndexTable2 != NULL)
			{
				// Traitement particulier sur la variable SampleAmplitude du paquet WaterColumn et
				// 						  sur les variables Amplitude et Phase du paquet Raw Beam Samples
				if ((!strcmp(strALLRecordXML->strTabImage2XML[iVarImage].cNom, "SampleAmplitude") &&
					 !strcmp(ptr_cNomHeader, "WaterColumn")) ||
					((!strcmp(strALLRecordXML->strTabImage2XML[iVarImage].cNom, "SampleAmplitude") ||
					 !strcmp(strALLRecordXML->strTabImage2XML[iVarImage].cNom, "SamplePhase")) &&
					 !strcmp(ptr_cNomHeader, "RawBeamSamples")))
				{
					fprintf(fpXML, "%s%s%s", "\t\t\t<Indexation>", "NRx,NumberOfSamples", "</Indexation>\r");
				}
				else
				{
					fprintf(fpXML, "%s%s%s", "\t\t\t<Indexation>", strALLRecordXML->ptr_cNomIndexTable2, "</Indexation>\r");
				}

			}

			if (strALLRecordXML->strTabImage2XML[iVarImage].bFlagConstant == TRUE)
			{
                // Pb de lib�ration de m�moire si  usNbElem * usTailleEnByte = 1
            	if (usNbElem != 1 || usTailleEnByte != 1)
                {
					// Allocation de la valeur � r�cup�rer selon la taille. Si elle est
					// du type Chaine, elle doit contenir autant d'�l�ments que d�clar�s.
					if (strcmp(cTypeMatLab, "char"))
					{
						cValeur = (char*)malloc(256*sizeof(char));
					}
					else
					{
						cValeur = (char*)malloc(usNbElem*sizeof(char));
					}
//					iRet = utilALL_cValVarFromFile(strALLRecordXML->strTabImage2XML[iVarImage],
//												cValeur);
                   	iRet = util_cValVarFromFile(strALLRecordXML->strTabImage2XML[iVarImage].cNomPathFileBin,
                    								strALLRecordXML->strTabImage2XML[iVarImage].cType,
                    								strALLRecordXML->strTabImage2XML[iVarImage].usNbElem,
                    								cValeur);
					fprintf(fpXML, "%s%s%s", "\t\t\t<Constant>", cValeur, "</Constant>\r");
					if (!strcmp(cTypeMatLab, "char")) // ???? : pb de lib�ration de memoire
					{
						free(cValeur);
					}
                }
                else
                {
//                    iRet = utilALL_cValVarFromFile(strALLRecordXML->strTabImage2XML[iVarImage],
//                    		&cValOneChar[0]);
                   	iRet = util_cValVarFromFile(strALLRecordXML->strTabImage2XML[iVarImage].cNomPathFileBin,
                    								strALLRecordXML->strTabImage2XML[iVarImage].cType,
                    								strALLRecordXML->strTabImage2XML[iVarImage].usNbElem,
                    								&cValOneChar[0]);
                   fprintf(fpXML, "%s%s%s", "\t\t\t<Constant>", &cValOneChar[0], "</Constant>\r");
                }
				// Effacement du fichier
				if (remove((const char *)strALLRecordXML->strTabImage2XML[iVarImage].cNomPathFileBin) !=0)
				{
				   printf("Cannot remove file %s (%s)\n",
							   strALLRecordXML->strTabImage2XML[iVarImage].cNomPathFileBin, ERRORSTRING);
				}
			}
			else
			{
				sprintf(cNomFicBin, "%s%s",strALLRecordXML->strTabImage2XML[iVarImage].cNom, ".bin");
				fprintf(fpXML, "%s%s%s", "\t\t\t<FileName>", cNomFicBin, "</FileName>\r");
			}
            if (strALLRecordXML->strTabImage2XML[iVarImage].fScaleFactor != 1.0)
            	fprintf(fpXML, "%s%g%s", "\t\t\t<ScaleFactor>", strALLRecordXML->strTabImage2XML[iVarImage].fScaleFactor, "</ScaleFactor>\r");
            if (strALLRecordXML->strTabImage2XML[iVarImage].fAddOffset != 0.0)
            	fprintf(fpXML, "%s%f%s", "\t\t\t<AddOffset>", strALLRecordXML->strTabImage2XML[iVarImage].fAddOffset, "</AddOffset>\r");
			fprintf(fpXML, "%s", "\t\t</item>\r");
			free(cTypeMatLab);
		} // Fin de la boucle sur les donn�es Image
		free(strALLRecordXML->strTabImage2XML);
	} //Fin de l'�criture des Images (RD et OD)

	// Fermeture de la balise Tables
	if (strALLRecordXML->sNbVarImage > 0 || strALLRecordXML->sNbVarImage2 >0)
	{
		fprintf(fpXML, "%s", "\t</Tables>\r");
	}

    fprintf(fpXML, "%s", "</ROOT>\r");
    fclose(fpXML);

    free(cNomFicXML);
    free(strALLRecordXML->ptr_cLabelHeader);
    free(strALLRecordXML->ptr_cNomHeader); // ptr_cNomHeader est du coup lib�r�.

    if (iRet != 0)
        return EXIT_FAILURE;
    else
        return EXIT_SUCCESS;


} //utilALL_iSaveAndCloseFiles

//////////////////////////////////////////////////////////////////////
// Fonction		:	util_iGetIndianAll
// Objet		:	Test du "boutisme" des fichiers
// Modification :	11/12/08
// Auteur		:	GLU
//////////////////////////////////////////////////////////////////////
int util_iGetIndianAll(char *cNomFic)
{

FILE *fp;

int		iNumberOfBytesInDatagram_1,
		iNumberOfBytesInDatagram_2,
		iNbOctetsLus,
		iTypeEndian;

	fp = fopen(cNomFic, "rb");

	if(fp  == NULL)
	{
		printf("\nProblem in access Data file : %s\n", cNomFic);
		iTypeEndian = -1;
		return iTypeEndian;
	}

	// Lecture d'un fichier quelconque
	iNbOctetsLus = fread(&iNumberOfBytesInDatagram_1, sizeof(int), 1, fp);
	iNumberOfBytesInDatagram_2 = util_iBswap_32(iNumberOfBytesInDatagram_1);

	fclose(fp);

	if (abs(iNumberOfBytesInDatagram_1) < abs(iNumberOfBytesInDatagram_2))
		iTypeEndian = 1; // Little Endian
	else
		iTypeEndian = 0; // Big Endian

	return iTypeEndian;
} // util_iGetIndianAll

//////////////////////////////////////////////////////////////////////
// Fonction		:	util_iCreateDirDatagram
// Objet		:	Cr�ation du r�pertoire d�di�es aux donn�es d'un
//					Datagrammes
// Modification :	29/09/09
// Auteur		:	GLU
//////////////////////////////////////////////////////////////////////
int util_iCreateDirDatagram(char *cRepData,
							char *cNomDatagram,
							char *cRepertoire)
{

int 	iErr;

	// Si Extension = '.All'
	if (!stricmp(G_cFileExtension, "all"))
		sprintf(cRepertoire, "%s\\ALL_%s", cRepData, cNomDatagram);
	// Si Extension = '.wcd'
	else if (!stricmp(G_cFileExtension, "wcd"))
		sprintf(cRepertoire, "%s\\WCD_%s", cRepData, cNomDatagram);
	else
	{
		printf("L'extension du fichier n'est pas reconnue : %s\n", G_cFileExtension);
		return EXIT_FAILURE;
	}


	if (DEBUG)
	   printf("-- Directory Creation : %s\n",cRepertoire);

	// Cr�ation du r�pertoire
	iErr = mkdir(cRepertoire);
	if (iErr != 0)
	{
	   switch (errno)
	   {
		   //"File exists"
		   case EEXIST:
			   break;
		   default:
			   printf("Error in Directory creation %s\n", strerror(errno));
			   return EXIT_FAILURE;
			   break;
	   }
	}

	return EXIT_SUCCESS;

} // util_iCreateDirDatagram



