/*
 * ALL_utilities.h
 *
 *  Created on: 6 nov. 2008
 *      Author: dwolyniec
 */

#ifndef ALL_UTILITIES_H_
#define ALL_UTILITIES_H_

#include "ALL_Datagrams.h"


off64_t All_trouveDatagram(FILE *,unsigned char *,int);

//int utilALL_cValVarFromFile(T_ALLVARXML strALLVarXML,
//							char *cValeur);

int utilALL_iSaveAndCloseFiles(	char *cRepData,
								T_ALLRECORDXML *strALLRecordXML);

int util_iGetIndianAll(char *nomFic);

int util_iCreateDirDatagram(char *cRepData,
							char *cNomDatagram,
							char *cRepertoire);

#endif /* ALL_UTILITIES_H_ */
