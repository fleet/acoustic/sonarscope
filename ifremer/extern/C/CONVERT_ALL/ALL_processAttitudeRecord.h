/*
 * ALL_processAttitudeRecord.h
 *
 *  Created on: 21 nov. 2008
 *      Author: rgallou
 */
#pragma pack(1)

#ifndef ALL_PROCESSATTITUDERECORD_H_
#define ALL_PROCESSATTITUDERECORD_H_

#include "ALL_DATAGRAMS.H"


typedef struct  {
		int 			NbOfBytesInDatagram;
		unsigned char	STX;
		unsigned char	TypeOfDatagram;
		unsigned short	EmModel;
		unsigned int	Date;
		unsigned int	NbMilliSecs;
		unsigned short	AttitudeCounter;
		unsigned short	SystemSerialNumber;
		unsigned short	NbCycles;
		unsigned char   SensorSystemDescriptor;
} T_ATTITUDE_RECORD_SIGNAL;

typedef struct  {
		unsigned short	*TS;
		unsigned short	*SensorStatus;
		short			*Roll;
		short			*Pitch;
		short			*Heave;
		unsigned short	*Heading;
} T_ATTITUDE_RECORD_IMAGE;

int iProcessAttitudeRecord(FILE *fpFileData,
						int iNbRecord,
						T_ALLRECORDXML *strALLRecordXML,
						T_ATTITUDE_RECORD_SIGNAL *ptrSignal_prevRecord);


int iInitXMLBinAttitudeRecord(	char *cTitleDatagram,
								char *cNomDatagram,
								char *cComments,
								T_ALLRECORDXML *strALLRecordXML);

#endif /* ALL_PROCESSATTITUDERECORD_H_ */

#pragma pack()
