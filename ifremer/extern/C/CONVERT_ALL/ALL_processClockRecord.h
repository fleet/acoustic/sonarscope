/*
 * ALL_processClockRecord.h
 *
 *  Created on: 21 nov. 2008
 *      Author: rgallou
 */
#pragma pack(1)

#ifndef ALL_PROCESSCLOCKRECORD_H_
#define ALL_PROCESSCLOCKRECORD_H_

#include "ALL_DATAGRAMS.H"


typedef struct  {
		int 			NbOfBytesInDatagram;
		unsigned char	STX;
		unsigned char	TypeOfDatagram;
		unsigned short	EmModel;
		unsigned int	Date;
		unsigned int	NbMilliSecs;
		unsigned short	ClockCounter;
		unsigned short	SystemSerialNumber;
		unsigned int	DateExternal;
		unsigned int 	HeureExternal;
		unsigned char	PPS;
} T_CLOCK_RECORD_SIGNAL;


int iProcessClockRecord(FILE *fpFileData,
						int iNbRecord,
						T_ALLRECORDXML *strALLRecordXML,
						T_CLOCK_RECORD_SIGNAL *ptrSignal_prevRecord);


int iInitXMLBinClockRecord(	char *cTitleDatagram,
								char *cNomDatagram, char *cComments,
								T_ALLRECORDXML *strALLRecordXML);

#endif /* ALL_PROCESSCLOCKRECORD_H_ */

#pragma pack()
