/*
 * ALL_processDepthV1Record.h
 *
 *  Created on: 21 nov. 2008
 *      Author: rgallou
 */
#pragma pack(1)

#ifndef ALL_PROCESSDEPTHV1RECORD_H_
#define ALL_PROCESSDEPTHV1RECORD_H_

#include "ALL_DATAGRAMS.H"


typedef struct  {
		int 			NbOfBytesInDatagram;
		unsigned char	STX;
		unsigned char	TypeOfDatagram;
		unsigned short	EmModel;
		unsigned int	Date;
		unsigned int	NbMilliSecs;
		unsigned short	DepthV1Counter;
		unsigned short	SystemSerialNumber;
		unsigned short	Heading;
		unsigned short	SoundSpeed;
		unsigned short	TransducerDepth;
		unsigned char	MaxNbBeamsPossible;
		unsigned char	NbBeams;
		unsigned char	ZResol;
		unsigned char	XYResol;
		unsigned short	SamplingRate;
		char			TransducerDepthOffsetMultiplier;
} T_DEPTHV1_RECORD_SIGNAL;

typedef struct  {
		short			*Depth;
		short			*AcrossDist;
		short			*AlongDist;
		short			*BeamDepressionAngle;
		unsigned short	*BeamAzimuthAngle;
		unsigned short	*Range;
		unsigned char	*QualityFactor;
		unsigned char	*LengthDetection;
		char			*Reflectivity;
		unsigned char	*BeamNumber;
} T_DEPTHV1_RECORD_IMAGE;

int iProcessDepthV1Record(FILE *fpFileData,
						int iNbRecord,
						T_ALLRECORDXML *strALLRecordXML,
						T_DEPTHV1_RECORD_SIGNAL *ptrSignal_prevRecord);


int iInitXMLBinDepthV1Record(	char *cTitleDatagram,
								char *cNomDatagram, char *cComments,
								T_ALLRECORDXML *strALLRecordXML);

#endif /* ALL_PROCESSDEPTHV1RECORD_H_ */

#pragma pack()
