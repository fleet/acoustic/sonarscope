/*
 * ALL_processDepthV2Record.h
 *
 *  Created on: 21 nov. 2008
 *      Author: rgallou
 */
#pragma pack(1)

#ifndef ALL_PROCESSDEPTHV2RECORD_H_
#define ALL_PROCESSDEPTHV2RECORD_H_

#include "ALL_DATAGRAMS.H"


typedef struct  {
		int 			NbOfBytesInDatagram;
		unsigned char	STX;
		unsigned char	TypeOfDatagram;
		unsigned short	EmModel;
		unsigned int	Date;
		unsigned int	NbMilliSecs;
		unsigned short	DepthV2Counter;
		unsigned short	SystemSerialNumber;
		unsigned short	Heading;
		unsigned short	SoundSpeed;
		float			TransducerDepth;
		unsigned short	MaxNbBeamsPossible;
		unsigned short	NbBeams;
		float			SamplingRate;
		unsigned char	Scanning;
		u8				Spare1;
		unsigned short	Spare2;
} T_DEPTHV2_RECORD_SIGNAL;

typedef struct  {
		float			*Depth;
		float			*AcrossDist;
		float			*AlongDist;
		unsigned short	*LengthDetection;
		unsigned char	*QualityFactor;
		char			*BeamIBA;
		unsigned char	*DetectionInfo;
		char			*RealTimeCleaningInfo;
		short			*Reflectivity;
} T_DEPTHV2_RECORD_IMAGE;


int iProcessDepthV2Record(FILE *fpFileData,
						int iNbRecord,
						T_ALLRECORDXML *strALLRecordXML,
						T_DEPTHV2_RECORD_SIGNAL *ptrSignal_prevRecord);


int iInitXMLBinDepthV2Record(	char *cTitleDatagram,
								char *cNomDatagram, char *cComments,
								T_ALLRECORDXML *strALLRecordXML);

#endif /* ALL_PROCESSDEPTHV2RECORD_H_ */

#pragma pack()
