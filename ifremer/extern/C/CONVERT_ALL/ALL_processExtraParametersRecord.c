/*
 * ALL_processExtraParametersRecord.c
 *
 *  Created on: 13 Mars 2013
 *      Author: rgallou
 */
#pragma pack(1)

#include <sys/types.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "ALL_Datagrams.h"
#include "ALL_Utilities.h"
#include "Generic_Utilities.h"
#include "ALL_processExtraParametersRecord.h"
#include "convertFiles_ALL.h"

#define NB_SIGNAL 		9
#define NB_IMAGE 		0
#define NB_IMAGE2 		0
#define NUM_INDEX_TABLE	0	// Identification de l'index Signal des Images (� partir de 0).
//----------------------------------------------------------------
// Fonction :	iProcessExtraParametersRecord
// Objet 	:	Traitement du paquet ExtraParameters
// Modif.	: 	13/03/2013
// Auteur 	:	GLU
//----------------------------------------------------------------
int iProcessExtraParametersRecord(FILE *fpFileData,
						int iNbRecord,
						T_ALLRECORDXML *strALLRecordXML,
						T_EXTRAPARAMETERS_RECORD_SIGNAL *ptrSignal_prevRecord)
{
	T_EXTRAPARAMETERS_RECORD_SIGNAL	*ptrSignal_Record;

	// Pointeur pour avancer sur les variables automatiquement.
	unsigned char		*ptr_cSignalRecord;
	static void 		*ptr_vVarPrevRecord;
	void				*ptr_vVarRecord,
						*ptr_vDummy;

	BOOL		bFlagVarConstant = TRUE,
				bFlagCompare = TRUE;

	int		iFlag,
			iVarSignal,
			iVarImage,
			iRet = 0,
			iLoop,
			iLen,
			iTailleBufferSignal,
			iNbBytesInDatagrams,
			iContentIdentifier;

	char	cValeur[1],
			*cArray,
			*cFileArrayName,
			cNameContentFile[100];

	FILE	*fpContent;

	unsigned short	usTailleEnByte,
					usNbElem;

	unsigned int uiNbElem; // Txt buffer could be greater than 65535 bytes

	// Initialisation de la structure d'identification du paquet
 	if (iNbRecord == 0)
	{
 		iInitXMLBinExtraParametersRecord(	"ExtraParameters",
									"ExtraParameters",
									"Extra Params : supplementary information",
									strALLRecordXML);
	}

    // Lecture des signaux du paquet
 	iTailleBufferSignal = sizeof(T_EXTRAPARAMETERS_RECORD_SIGNAL);

 	ptrSignal_Record = malloc(iTailleBufferSignal);
	iFlag = fread(ptrSignal_Record, iTailleBufferSignal, 1, fpFileData);

	if (iFlag == 0)
	{
		printf("%s -- Error in reading : %s\n", __FILE__, G_cFileData);
		return FALSE;
	}
	ptr_cSignalRecord = (unsigned char*)ptrSignal_Record;

	// Sauvegarde des donn�es des attributs.
	if (iNbRecord > 0)
	{
		ptr_vVarPrevRecord = (void *)ptrSignal_prevRecord;
	}

	for (iVarSignal=0; iVarSignal<(int)strALLRecordXML->sNbVarSignal; iVarSignal++)
	{
		ptr_vVarRecord = (void *)ptr_cSignalRecord;
		if (iVarSignal==0)
		{
			usTailleEnByte = 0;
			usNbElem = 0;
		}
		else
		{
			usTailleEnByte = util_usNbOctetTypeVar(strALLRecordXML->strTabSignalXML[iVarSignal-1].cType);
			usNbElem = strALLRecordXML->strTabSignalXML[iVarSignal-1].usNbElem;
		}
		// Pointage sur la donn�e suivante.
		ptr_vVarPrevRecord = ptr_vVarPrevRecord + usTailleEnByte*usNbElem;
		// On ne fait pas le test de constance au 1er enregistrement ou si le signal
		// est d�j� vue comme variable.
		if (iNbRecord > 0 && strALLRecordXML->strTabSignalXML[iVarSignal].bFlagConstant == TRUE)
		{
			bFlagVarConstant = strALLRecordXML->strTabSignalXML[iVarSignal].bFlagConstant;
			if (G_iFlagEndian == 0) // On inverse en Big Endian.
			{
				// Observation de la taille propre � la variable.
				usTailleEnByte = util_usNbOctetTypeVar(strALLRecordXML->strTabSignalXML[iVarSignal].cType);
				usNbElem = strALLRecordXML->strTabSignalXML[iVarSignal].usNbElem;
				if (usTailleEnByte == 2) // Pour des (u)short, (u)int, (u)long.
					ptr_vVarPrevRecord = (void *)util_cBswap_16(ptr_vVarPrevRecord);
				else if (usTailleEnByte == 4)
					ptr_vVarPrevRecord = (void *)util_cBswap_32(ptr_vVarPrevRecord);
				else if (usTailleEnByte == 8)
					ptr_vVarPrevRecord = (void *)util_cBswap_64(ptr_vVarPrevRecord);
			}
			bFlagCompare = util_bComparePrevAndCurrentVal(strALLRecordXML->strTabSignalXML[iVarSignal].cType,
												ptr_vVarPrevRecord,
												ptr_vVarRecord,
											   strALLRecordXML->strTabSignalXML[iVarSignal].usNbElem);
			bFlagVarConstant = bFlagVarConstant && bFlagCompare;
			strALLRecordXML->strTabSignalXML[iVarSignal].bFlagConstant = bFlagVarConstant;
		}


		if (iVarSignal == 8)
		{
			iRet =	util_cValVarConstant(	strALLRecordXML->strTabSignalXML[iVarSignal].cType,
											ptr_vVarRecord,
											strALLRecordXML->strTabSignalXML[iVarSignal].usNbElem,
											&cValeur[0]);
			iContentIdentifier = atoi(cValeur);
		}
		if (iVarSignal == 0)
		{
			iRet =	util_cValVarConstant(	strALLRecordXML->strTabSignalXML[iVarSignal].cType,
											ptr_vVarRecord,
											strALLRecordXML->strTabSignalXML[iVarSignal].usNbElem,
											&cValeur[0]);
			iNbBytesInDatagrams = atoi(cValeur);
		}
		// Pr�paration de l'�criture de la variable.
		usTailleEnByte = strALLRecordXML->strTabSignalXML[iVarSignal].usSize;
		usNbElem =  strALLRecordXML->strTabSignalXML[iVarSignal].usNbElem;
		if (G_iFlagEndian == 0) // On inverse en Big Endian.
		{
			if (usTailleEnByte == 2) // Pour des (u)short, (u)int, (u)long.
				ptr_cSignalRecord = (unsigned char*)util_cBswap_16((char*)ptr_cSignalRecord);
			else if (usTailleEnByte == 4)
				ptr_cSignalRecord = (unsigned char*)util_cBswap_32((char*)ptr_cSignalRecord);
			else if (usTailleEnByte == 8)
				ptr_cSignalRecord = (unsigned char*)util_cBswap_64((char*)ptr_cSignalRecord);
		}
		fwrite(ptr_cSignalRecord, usTailleEnByte, usNbElem, strALLRecordXML->strTabSignalXML[iVarSignal].ptr_fpBin);
		//		strALLRecordXML->strTabSignalXML[iVarSignal].ptr_vPrevVal = ptr_cSignalRecord;
		strALLRecordXML->strTabSignalXML[iVarSignal].ptr_vPrevVal = ptr_cSignalRecord;
		ptr_cSignalRecord += usTailleEnByte*usNbElem;
	}
	// Recopie en zone tampon des valeurs courantes.
	memmove(ptrSignal_prevRecord, ptrSignal_Record, iTailleBufferSignal);

	// De Type BSCorr
	if (iContentIdentifier == 6)
	{
		// --------------------------------------------
		// Traitement des paquets Array qui sont de taille variable.
		// Le datagramme Extra Parameters b�n�ficie d'un traitement � part pour l'�criture
		// de fichiers txt directement � partir du d�codage en cours.
		iVarImage 		= 0;
		usTailleEnByte 	= 1; // Tableau de char

		// On conserve la donn�e Spare Byte si elle est pr�sente :
		// Le tableau est pr�fix� de sa taille � lire sur deux octets.
		ptr_vDummy 	= malloc(sizeof(unsigned short));
		iFlag		= fread(ptr_vDummy, 1, 2, fpFileData);
		iRet		=	util_cValVarConstant("unsigned short",
											ptr_vDummy,
											1,
											&cValeur[0]);

		usNbElem	= (unsigned short)atoi(cValeur);
		uiNbElem = iNbBytesInDatagrams - 18 - 4;

		free(ptr_vDummy);

	}
	else
	{
		// L'�criture des autres types de donn�es est � finaliser.
		usTailleEnByte 	= 1; // Tableau de char
		usNbElem = iNbBytesInDatagrams - 18 - 4;
		uiNbElem = iNbBytesInDatagrams - 18 - 4;
	}

	cArray		= malloc(uiNbElem*usTailleEnByte);
	iFlag		= fread(cArray, usTailleEnByte, uiNbElem, fpFileData);

	// Terminaison de cha�ne :
	// cArray[usNbElem-1] = '\0';
	if (G_iFlagEndian == 0) // On inverse en Big Endian.
	{
		if (usTailleEnByte == 2) // Pour des (u)short, (u)int, (u)long.
			cArray = (void *)util_cBswap_16(cArray);
		else if (usTailleEnByte == 4)
			cArray = (void *)util_cBswap_32(cArray);
		else if (usTailleEnByte == 8)
			cArray = (void *)util_cBswap_64(cArray);
	}

	// Choix du type de fichier de donn�e Suppl�mentaire.
	switch (iContentIdentifier) {
		case 1:
			sprintf(cNameContentFile, "%s", "Calib.txt");
			break;
		case 2:
			sprintf(cNameContentFile, "%s", "LogAllHeights.bin");
			break;
		case 3:
			sprintf(cNameContentFile, "%s", "SoundVelocityAtTransducer.bin");
			break;
		case 4:
			sprintf(cNameContentFile, "%s", "SoudnVelocityProfile.bin");
			break;
		case 5:
			sprintf(cNameContentFile, "%s", "MulticastRXStatus.bin");
			break;
		case 6:
			sprintf(cNameContentFile, "%s", "BSCorr.txt");
			break;
	}

	// Ecriture de la cha�ne dans le fichier (�criture de char - 1 octet).
	iLen = strlen(G_cRepData) + 50; // 50 caract�res de marge.
	cFileArrayName = malloc(sizeof(char)*iLen);
	sprintf(cFileArrayName, "%s\\%s\\%s", G_cRepData, "ALL_ExtraParameters", cNameContentFile);

	fpContent= fopen(cFileArrayName, "w+b");
	fwrite(cArray, usTailleEnByte, uiNbElem, fpContent);
	fclose(fpContent);

	// D�sallocation (obligatoire !) des matrices r�serv�es.
	free(cFileArrayName);
	free(ptrSignal_Record);
	free(cArray);

	iNbRecord++;

	return iRet;

} //iProcessExtraParametersRecord


//----------------------------------------------------------------
// Fonction :	iInitXMLBinExtraParametersRecord
// Objet 	:	Initialisation de la structure �quivalent au paquet
//				ExtraParameters pour la manipulation des fichiers bin et XML.
// Modification : 15/10/08
// Auteur 	: GLU
//----------------------------------------------------------------
int iInitXMLBinExtraParametersRecord(	char *cTitleDatagram,
								char *cNomDatagram,
								char *cComments,
								T_ALLRECORDXML *strALLRecordXML)
{

	char cTabNomVarSignal[NB_SIGNAL][30] = {
			"NbOfBytesInDatagram",
			"STX",
			"TypeOfDatagram",
			"EmModel",
			"Date",
			"NbMilliSecs",
			"PingCounter",
			"SystemSerialNumber",
			"ContentIdentifier"};

	char cTabNomTypeSignal[NB_SIGNAL][20] = {
			"int",
			"u8",
			"u8",
			"unsigned short",
			"unsigned int",
			"unsigned int",
			"unsigned short",
			"unsigned short",
			"unsigned short"};

	// ScaleFactor
	float 	fTabSFSignal[NB_SIGNAL] = {
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0};

	// AddOffset
	float 	fTabAOSignal[NB_SIGNAL] = {
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0};


	char	*ptr_cRepertoire,
			cNomFicPathBin[300];

    int 	iSizeRep,
			iRet,
			iLen,
			iVarSignal,
			iVarImage;

	unsigned short usTaille;

	// Initialisation des structures de pilotage des fichiers
	// XML et Bin .
	strALLRecordXML->sNbVarSignal 	= NB_SIGNAL;
	strALLRecordXML->sNbVarImage 	= NB_IMAGE;
	strALLRecordXML->sNbVarImage2 	= 0;
    strALLRecordXML->strTabSignalXML= (T_ALLVARXML *)malloc(strALLRecordXML->sNbVarSignal*sizeof(T_ALLVARXML));
    strALLRecordXML->strTabImageXML= (T_ALLVARXML *)malloc(strALLRecordXML->sNbVarImage*sizeof(T_ALLVARXML));

	iLen = strlen(cNomDatagram)+1;
	strALLRecordXML->ptr_cNomHeader 	= malloc(iLen*sizeof(char));
	sprintf(strALLRecordXML->ptr_cNomHeader, "%s", cNomDatagram);

	iLen = strlen(cTitleDatagram)+1;
	strALLRecordXML->ptr_cLabelHeader 	= malloc(iLen*sizeof(char));
	sprintf(strALLRecordXML->ptr_cLabelHeader, "%s", cTitleDatagram);

	iLen = strlen(cComments)+1;
	strALLRecordXML->ptr_cCommentsHeader = malloc(iLen*sizeof(char));
	sprintf(strALLRecordXML->ptr_cCommentsHeader, "%s", cComments);

	// Remplissage du nom de la variable d'indexation des tables.
	iLen = strlen(cTabNomVarSignal[8])+1;
	strALLRecordXML->ptr_cNomIndexTable = malloc(iLen*sizeof(char));
	sprintf(strALLRecordXML->ptr_cNomIndexTable, "%s", cTabNomVarSignal[8]);

	// Affectation et cr�ation du repertoire du Datagramme.
	iSizeRep = ( strlen(G_cRepData) + strlen( cNomDatagram));
	// Ajout d'une marge au cas o� certains caract�res soient doubl�s soient doubl�s.
	ptr_cRepertoire = malloc( (iSizeRep + 1 + 30 + 4)*sizeof(char));
	iRet = util_iCreateDirDatagram(G_cRepData,
								strALLRecordXML->ptr_cNomHeader,
								ptr_cRepertoire);

	// Initialisation g�n�rale des variables de type Signal
	for (iVarSignal=0; iVarSignal<strALLRecordXML->sNbVarSignal; iVarSignal++) {
	   strALLRecordXML->strTabSignalXML[iVarSignal].ptr_fpBin = NULL;
	   strALLRecordXML->strTabSignalXML[iVarSignal].bFlagConstant = TRUE;
	   strALLRecordXML->strTabSignalXML[iVarSignal].usNbElem = 1;
	   sprintf(strALLRecordXML->strTabSignalXML[iVarSignal].cNom, "%s", cTabNomVarSignal[iVarSignal]);
	   sprintf(strALLRecordXML->strTabSignalXML[iVarSignal].cType, "%s", cTabNomTypeSignal[iVarSignal]);
	   sprintf(strALLRecordXML->strTabSignalXML[iVarSignal].cTag, "%s", "TODO");
	   sprintf(strALLRecordXML->strTabSignalXML[iVarSignal].cUnit, "%s", "TODO");
	   strALLRecordXML->strTabSignalXML[iVarSignal].fScaleFactor = fTabSFSignal[iVarSignal];
	   strALLRecordXML->strTabSignalXML[iVarSignal].fAddOffset = fTabAOSignal[iVarSignal];
	   usTaille = util_usNbOctetTypeVar(cTabNomTypeSignal[iVarSignal]);
	   strALLRecordXML->strTabSignalXML[iVarSignal].usSize = usTaille;
	   iLen = strlen(strALLRecordXML->strTabSignalXML[iVarSignal].cNom);
	   // Ajout d'une marge au cas o� certains caract�res soient doubl�s soient doubl�s.
	   sprintf(cNomFicPathBin, "%s\\%s%s", ptr_cRepertoire, strALLRecordXML->strTabSignalXML[iVarSignal].cNom, ".bin");
	   sprintf(strALLRecordXML->strTabSignalXML[iVarSignal].cNomPathFileBin, "%s", cNomFicPathBin);
	   strALLRecordXML->strTabSignalXML[iVarSignal].ptr_fpBin = fopen(cNomFicPathBin, "w+b");
	   if (!strALLRecordXML->strTabSignalXML[iVarSignal].ptr_fpBin)
	   {
		   printf("%s -- Error in Signal creation  : %s\n", __FILE__, cNomFicPathBin);
		   return EXIT_FAILURE;
	   }
	}


	free(ptr_cRepertoire);
	return EXIT_SUCCESS;

} // iInitXMLBinExtraParametersRecord

#pragma pack()
