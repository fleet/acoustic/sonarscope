/*
 * ALL_processExtraParametersRecord.h
 *
 *  Created on: 13 Mars 2013
 *      Author: rgallou
 */
#pragma pack(1)

#ifndef ALL_PROCESSEXTRAPARAMETERSRECORD_H_
#define ALL_PROCESSEXTRAPARAMETERSRECORD_H_

#include "ALL_DATAGRAMS.H"


typedef struct  {
		int 			NbOfBytesInDatagram;
		unsigned char	STX;
		unsigned char	TypeOfDatagram;
		unsigned short	EmModel;
		unsigned int	Date;
		unsigned int	NbMilliSecs;
		unsigned short	ExtraParametersCounter;
		unsigned short	SystemSerialNumber;
		unsigned short	ContentIdentifier;
} T_EXTRAPARAMETERS_RECORD_SIGNAL;


int iProcessExtraParametersRecord(FILE *fpFileData,
						int iNbRecord,
						T_ALLRECORDXML *strALLRecordXML,
						T_EXTRAPARAMETERS_RECORD_SIGNAL *ptrSignal_prevRecord);


int iInitXMLBinExtraParametersRecord(	char *cTitleDatagram,
								char *cNomDatagram,
								char *cComments,
								T_ALLRECORDXML *strALLRecordXML);

#endif /* ALL_PROCESSEXTRAPARAMETERSRECORD_H_ */

#pragma pack()
