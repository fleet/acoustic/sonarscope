/*
 * ALL_processFileIndexRecord.h
 *
 *  Created on: 21 nov. 2008
 *      Author: rgallou
 */
#pragma pack(1)

#ifndef ALL_PROCESSFILEINDEXRECORD_H_
#define ALL_PROCESSFILEINDEXRECORD_H_

#include "ALL_DATAGRAMS.H"
#include <sys/types.h>


typedef struct  {
		unsigned int	NbOfBytesInDatagram;
		unsigned char	STX;
		unsigned char	TypeOfDatagram;
		unsigned short	EmModel;
		unsigned int	Date;
		unsigned int	NbMilliSecs;
		unsigned short	FileIndexCounter;
		unsigned short	SystemSerialNumber;
		off64_t 		DatagramPosition;
		int 			DatagramLength;
} T_FILEINDEX_RECORD_SIGNAL;


int iProcessFileIndexRecord(FILE *fpFileData,
						int iNbRecord,
						T_ALLRECORDXML *strALLRecordXML,
						T_FILEINDEX_RECORD_SIGNAL *ptrSignal_prevRecord);


int iInitXMLBinFileIndexRecord(	char *cTitleDatagram,
								char *cNomDatagram, char *cComments,
								T_ALLRECORDXML *strALLRecordXML);

#endif /* ALL_PROCESSFILEINDEXRECORD_H_ */

#pragma pack()
