/*
 * ALL_processHeadingRecord.h
 *
 *  Created on: 21 nov. 2008
 *      Author: rgallou
 */
#pragma pack(1)

#ifndef ALL_PROCESSHEADINGRECORD_H_
#define ALL_PROCESSHEADINGRECORD_H_

#include "ALL_DATAGRAMS.H"


typedef struct  {
		int 			NbOfBytesInDatagram;
		unsigned char	STX;
		unsigned char	TypeOfDatagram;
		unsigned short	EmModel;
		unsigned int	Date;
		unsigned int	NbMilliSecs;
		unsigned short	HeadingCounter;
		unsigned short	SystemSerialNumber;
		unsigned short	NbEntries;
} T_HEADING_RECORD_SIGNAL;

typedef struct  {
		unsigned short	*StartNbMilliSecs;
		unsigned short	*Heading;
} T_HEADING_RECORD_IMAGE;

int iProcessHeadingRecord(FILE *fpFileData, int iNbRecord,
						T_ALLRECORDXML *strALLRecordXML,
						T_HEADING_RECORD_SIGNAL *ptrSignal_prevRecord);


int iInitXMLBinHeadingRecord(	char *cTitleDatagram,
								char *cNomDatagram, char *cComments,
								T_ALLRECORDXML *strALLRecordXML);

#endif /* ALL_PROCESSHEADINGRECORD_H_ */

#pragma pack()
