/*
 * ALL_processHeightRecord.h
 *
 *  Created on: 21 nov. 2008
 *      Author: rgallou
 */
#pragma pack(1)

#ifndef ALL_PROCESSHeightRECORD_H_
#define ALL_PROCESSHeightRECORD_H_

#include "ALL_DATAGRAMS.H"


typedef struct  {
		int 			NbOfBytesInDatagram;
		unsigned char	STX;
		unsigned char	TypeOfDatagram;
		unsigned short	EmModel;
		unsigned int	Date;
		unsigned int	NbMilliSecs;
		unsigned short	HeightCounter;
		unsigned short	SystemSerialNumber;
		int				Height;
		unsigned char 	HeightType;
} T_HEIGHT_RECORD_SIGNAL;


int iProcessHeightRecord(FILE *fpFileData, int iNbRecord,
						T_ALLRECORDXML *strALLRecordXML,
						T_HEIGHT_RECORD_SIGNAL *ptrSignal_prevRecord);


int iInitXMLBinHeightRecord(	char *cTitleDatagram,
								char *cNomDatagram, char *cComments,
								T_ALLRECORDXML *strALLRecordXML);

#endif /* ALL_PROCESSHeightRECORD_H_ */

#pragma pack()
