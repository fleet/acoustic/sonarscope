/*
 * ALL_processInstallParamsRecord.c
 *
 *  Created on: 13 oct. 2008
 *      Author: rgallou
 */
#pragma pack(1)

#include <sys/types.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "ALL_Datagrams.h"
#include "ALL_Utilities.h"
#include "Generic_Utilities.h"
#include "ALL_processInstallParamsRecord.h"
#include "convertFiles_ALL.h"


//----------------------------------------------------------------
// Fonction :	iProcessInstallParamsRecord
// Objet 	:	Traitement du paquet InstallParams
// Modif.	: 	03/11/08
// Auteur 	:	GLU
//----------------------------------------------------------------
int iProcessInstallParamsRecord(FILE *fpFileData,
						int iNbRecord,
						T_ALLRECORDXML *strALLRecordXML,
						T_INSTALLPARAMS_RECORD_SIGNAL *ptrSignal_prevRecord)
{
	T_INSTALLPARAMS_RECORD_SIGNAL	*ptrSignal_Record;
	T_INSTALLPARAMS_RECORD_IMAGE	ptrImage_Record;

	// Pointeur pour avancer sur les variables automatiquement.
	unsigned char		*ptr_cSignalRecord;
	static void 		*ptr_vVarPrevRecord;
	void				*ptr_vVarRecord;

	BOOL		bFlagVarConstant = TRUE,
				bFlagCompare = TRUE;

	char 		*ptrCh;

	int 		iFlag,
				iVarSignal,
				iVarImage,
				iRetour = 0,
				iLen;

	//static int		iNbRecord = 0;

    unsigned short	usTailleEnByte,
					usNbElem;

    // Initialisation de la structure d'identification du paquet
 	if (iNbRecord == 0)
	{
 		iInitXMLBinInstallParamsRecord("Installation Parameters",
										"InstallParams",
										"Installation Parameters",
										strALLRecordXML);
	}

    // Lecture des signaux du paquet
	ptrSignal_Record = (T_INSTALLPARAMS_RECORD_SIGNAL *)malloc(sizeof(T_INSTALLPARAMS_RECORD_SIGNAL));
	iFlag = fread(ptrSignal_Record, sizeof(T_INSTALLPARAMS_RECORD_SIGNAL), 1, fpFileData);
	if (iFlag < 0)
	{
		printf("%s -- Error in reading : %s\n", __FILE__, G_cFileData);
		return FALSE;
	}
	ptr_cSignalRecord = (unsigned char*)ptrSignal_Record;
	// Sauvegarde des donn�es des attributs.
	if (iNbRecord > 0)
	{
		ptr_vVarPrevRecord = (void *)ptrSignal_prevRecord;
	}
	for (iVarSignal=0; iVarSignal<(int)strALLRecordXML->sNbVarSignal; iVarSignal++)
	{


		ptr_vVarRecord = (void *)ptr_cSignalRecord;
		if (iVarSignal==0)
		{
			usTailleEnByte = 0;
			usNbElem = 0;
		}
		else
		{
			usTailleEnByte = util_usNbOctetTypeVar(strALLRecordXML->strTabSignalXML[iVarSignal-1].cType);
			usNbElem = strALLRecordXML->strTabSignalXML[iVarSignal-1].usNbElem;
		}

		// Pointage sur la donn�e suivante.
		ptr_vVarPrevRecord = ptr_vVarPrevRecord + usTailleEnByte*usNbElem;
		// On ne fait pas le test de constance au 1er enregistrement ou si le signal
		// est d�j� vue comme variable.
		if (iNbRecord > 0 && strALLRecordXML->strTabSignalXML[iVarSignal].bFlagConstant == TRUE)
		{
			bFlagVarConstant = strALLRecordXML->strTabSignalXML[iVarSignal].bFlagConstant;
			if (G_iFlagEndian == 0) // On inverse en Big Endian.
			{
				// Observation de la taille propre � la variable.
				usTailleEnByte = util_usNbOctetTypeVar(strALLRecordXML->strTabSignalXML[iVarSignal].cType);
				usNbElem = strALLRecordXML->strTabSignalXML[iVarSignal].usNbElem;
				if (usTailleEnByte == 2) // Pour des (u)short, (u)int, (u)long.
					ptr_vVarPrevRecord = (void *)util_cBswap_16(ptr_vVarPrevRecord);
				else if (usTailleEnByte == 4)
					ptr_vVarPrevRecord = (void *)util_cBswap_32(ptr_vVarPrevRecord);
				else if (usTailleEnByte == 8)
					ptr_vVarPrevRecord = (void *)util_cBswap_64(ptr_vVarPrevRecord);
			}
			bFlagCompare = util_bComparePrevAndCurrentVal(strALLRecordXML->strTabSignalXML[iVarSignal].cType,
												ptr_vVarPrevRecord,
												ptr_vVarRecord,
											   strALLRecordXML->strTabSignalXML[iVarSignal].usNbElem);
			bFlagVarConstant = bFlagVarConstant && bFlagCompare;
			strALLRecordXML->strTabSignalXML[iVarSignal].bFlagConstant = bFlagVarConstant;
		}


		// Pr�paration de l'�criture de la variable.
		usTailleEnByte = strALLRecordXML->strTabSignalXML[iVarSignal].usSize;
		usNbElem =  strALLRecordXML->strTabSignalXML[iVarSignal].usNbElem;
		if (G_iFlagEndian == 0) // On inverse en Big Endian.
		{
			if (usTailleEnByte == 2) // Pour des (u)short, (u)int, (u)long.
				ptr_cSignalRecord = (unsigned char*)util_cBswap_16((char*)ptr_cSignalRecord);
			else if (usTailleEnByte == 4)
				ptr_cSignalRecord = (unsigned char*)util_cBswap_32((char*)ptr_cSignalRecord);
			else if (usTailleEnByte == 8)
				ptr_cSignalRecord = (unsigned char*)util_cBswap_64((char*)ptr_cSignalRecord);
		}
		fwrite(ptr_cSignalRecord, usTailleEnByte, usNbElem, strALLRecordXML->strTabSignalXML[iVarSignal].ptr_fpBin);
		strALLRecordXML->strTabSignalXML[iVarSignal].ptr_vPrevVal = ptr_cSignalRecord;
		ptr_cSignalRecord += usTailleEnByte*usNbElem;
	}
	// Recopie en zone tampon des valeurs courantes.
	memmove(ptrSignal_prevRecord, ptrSignal_Record, sizeof(T_INSTALLPARAMS_RECORD_SIGNAL));

	// --------------------------------------------
 	// Traitement des paquets de type Image
	iVarImage = 0;
	usTailleEnByte = strALLRecordXML->strTabImageXML[iVarImage].usSize;
	// Taille �quivalente � la proc�dure sous MatLab + caract�re Null.
	// Normalement 22-3(-1 si nombre de valeur impaire).
	// Valeur de 18 empirique, � valider au fil des essais.

	usNbElem = sizeof(char)*ptrSignal_Record->NbOfBytesInDatagram-18-4+1;
	//R�allocation de la taille (dynamique) de description des param�tres.
	strALLRecordXML->strTabImageXML[iVarImage].usNbElem = usNbElem;
	ptrImage_Record.Line = malloc(usNbElem*usTailleEnByte);
	iFlag = fread(ptrImage_Record.Line, usTailleEnByte, usNbElem, fpFileData);

	// Recherche du caract�re ETX (0x03) de fin de text car peut-etre present en fin de chaine :
	//cf . Fichier 0003_20110308_203947
	ptrCh = strchr(ptrImage_Record.Line, 3);
	if (ptrCh!=NULL)
	{
		iLen = strlen(ptrCh);
		usNbElem = strALLRecordXML->strTabImageXML[iVarImage].usNbElem-iLen-1;
		strALLRecordXML->strTabImageXML[iVarImage].usNbElem = usNbElem;
	}
	// Terminaison de cha�ne :
	ptrImage_Record.Line[usNbElem-1] = '\0';

	if (G_iFlagEndian == 0) // On inverse en Big Endian.
	{
		if (usTailleEnByte == 2) // Pour des (u)short, (u)int, (u)long.
			ptrImage_Record.Line = (void *)util_cBswap_16(ptrImage_Record.Line);
		else if (usTailleEnByte == 4)
			ptrImage_Record.Line = (void *)util_cBswap_32(ptrImage_Record.Line);
		else if (usTailleEnByte == 8)
			ptrImage_Record.Line = (void *)util_cBswap_64(ptrImage_Record.Line);
	}
	fwrite(ptrImage_Record.Line, usTailleEnByte, usNbElem, strALLRecordXML->strTabImageXML[iVarImage].ptr_fpBin);

	// 2015/03/24 : On ne regarde pas la constante du champ Line car celui-ci est probablement diff�rent du
	// Datagramme InstallParams Start ou p d�j� lus.
	// Cela cr�e d'ailleurs un pb car il faudrait mettre en place la r�allocation de m�moire pour le
	// cas o� la donn�e � comparer est plus grande que la donn�e lue.

	// D�sallocation (obligatoire !) des matrices r�serv�es.
	free(ptrImage_Record.Line);

	free(ptrSignal_Record);

	iNbRecord++;

	return iRetour;

} //iProcessInstallParamsRecord


//----------------------------------------------------------------
// Fonction :	iInitXMLBinInstallParamsRecord
// Objet 	:	Initialisation de la structure �quivalent au paquet
//				InstallParams pour la manipulation des fichiers bin et XML.
// Modification : 15/10/08
// Auteur 	: GLU
//----------------------------------------------------------------
int iInitXMLBinInstallParamsRecord(	char *cTitleDatagram,
								char *cNomDatagram, char *cComments,
								T_ALLRECORDXML *strALLRecordXML)
{

	char cTabNomVarSignal[9][40] = {
			"NbOfBytesInDatagram",
			"STX",
			"TypeOfDatagram",
			"EmModel",
			"Date",
			"NbMilliSecs",
			"SurveyLineNumber",
			"SystemSerialNumber",
			"SerialNumberOfSecondSonarHead"};

	char cTabNomTypeSignal[9][20] = {
			"int",
			"u8",
			"u8",
			"unsigned short",
			"unsigned int",
			"unsigned int",
			"unsigned short",
			"unsigned short",
			"unsigned short"};

	// ScaleFactor
	float 	fTabSFSignal[9] = {
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0};

	// AddOffset
	float 	fTabAOSignal[9] = {
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0};

	// On ne lit pas le Spare en fin de bloc.
	char cTabNomVarImage[1][30] = {
			"Line"};

	char cTabNomTypeImage[1][20] = {
			"char"};

	// ScaleFactor
	float 	fTabSFImage[1] = {
			1.0};

	// AddOffset
	float 	fTabAOImage[1] = {
			0.0};

	char	*ptr_cRepertoire,
			cNomFicPathBin[300];

    int 	iSizeRep,
			iRet,
			iLen,
			iVarSignal,
			iVarImage;

	unsigned short usTaille;

	// Initialisation des structures de pilotage des fichiers
	// XML et Bin .
	strALLRecordXML->sNbVarSignal = 9;
	strALLRecordXML->sNbVarImage = 1;
	strALLRecordXML->sNbVarImage2 = 0;
    strALLRecordXML->strTabSignalXML= (T_ALLVARXML *)malloc(strALLRecordXML->sNbVarSignal*sizeof(T_ALLVARXML));
    strALLRecordXML->strTabImageXML= (T_ALLVARXML *)malloc(strALLRecordXML->sNbVarImage*sizeof(T_ALLVARXML));

	iLen = strlen(cNomDatagram)+1;
	strALLRecordXML->ptr_cNomHeader = malloc(iLen*sizeof(char));
	sprintf(strALLRecordXML->ptr_cNomHeader, "%s", cNomDatagram);

	iLen = strlen(cTitleDatagram)+1;
	strALLRecordXML->ptr_cLabelHeader = malloc(iLen*sizeof(char));
	sprintf(strALLRecordXML->ptr_cLabelHeader, "%s", cTitleDatagram);

	iLen = strlen(cComments)+1;
	strALLRecordXML->ptr_cCommentsHeader = malloc(iLen*sizeof(char));
	sprintf(strALLRecordXML->ptr_cCommentsHeader, "%s", cComments);

	// Remplissage du nom de la variable d'indexation des tables.
	iLen = strlen(cTabNomVarSignal[0])+1;
	strALLRecordXML->ptr_cNomIndexTable = malloc(iLen*sizeof(char));
	sprintf(strALLRecordXML->ptr_cNomIndexTable, "%s", cTabNomVarSignal[0]);

	// Affectation et cr�ation du repertoire du Datagramme.
	iSizeRep = ( strlen(G_cRepData) + strlen( cNomDatagram));
	// Ajout d'une marge au cas o� certains caract�res soient doubl�s soient doubl�s.
	ptr_cRepertoire = malloc( (iSizeRep + 1 + 30 + 4)*sizeof(char));
	iRet = util_iCreateDirDatagram(G_cRepData,
								strALLRecordXML->ptr_cNomHeader,
								ptr_cRepertoire);

	// Initialisation g�n�rale des variables de type Signal
	for (iVarSignal=0; iVarSignal<strALLRecordXML->sNbVarSignal; iVarSignal++) {
	   strALLRecordXML->strTabSignalXML[iVarSignal].ptr_fpBin = NULL;
	   strALLRecordXML->strTabSignalXML[iVarSignal].bFlagConstant = TRUE;
	   strALLRecordXML->strTabSignalXML[iVarSignal].usNbElem = 1;
	   sprintf(strALLRecordXML->strTabSignalXML[iVarSignal].cNom, "%s", cTabNomVarSignal[iVarSignal]);
	   sprintf(strALLRecordXML->strTabSignalXML[iVarSignal].cType, "%s", cTabNomTypeSignal[iVarSignal]);
	   sprintf(strALLRecordXML->strTabSignalXML[iVarSignal].cTag, "%s", "TODO");
	   sprintf(strALLRecordXML->strTabSignalXML[iVarSignal].cUnit, "%s", "TODO");
	   strALLRecordXML->strTabSignalXML[iVarSignal].fScaleFactor = fTabSFSignal[iVarSignal];
	   strALLRecordXML->strTabSignalXML[iVarSignal].fAddOffset = fTabAOSignal[iVarSignal];
	   usTaille = util_usNbOctetTypeVar(cTabNomTypeSignal[iVarSignal]);
	   strALLRecordXML->strTabSignalXML[iVarSignal].usSize = usTaille;
	   iLen = strlen(strALLRecordXML->strTabSignalXML[iVarSignal].cNom);
	   // Ajout d'une marge au cas o� certains caract�res soient doubl�s soient doubl�s.
	   sprintf(cNomFicPathBin, "%s\\%s%s", ptr_cRepertoire, strALLRecordXML->strTabSignalXML[iVarSignal].cNom, ".bin");
	   sprintf(strALLRecordXML->strTabSignalXML[iVarSignal].cNomPathFileBin, "%s", cNomFicPathBin);
	   strALLRecordXML->strTabSignalXML[iVarSignal].ptr_fpBin = fopen(cNomFicPathBin, "w+b");
	   if (!strALLRecordXML->strTabSignalXML[iVarSignal].ptr_fpBin)
	   {
		   printf("%s -- Error in Signal creation  : %s\n", __FILE__, cNomFicPathBin);
		   return EXIT_FAILURE;
	   }
	}

	// Initialisation g�n�rale des variables de type Image
	for (iVarImage=0; iVarImage<strALLRecordXML->sNbVarImage; iVarImage++) {
	   strALLRecordXML->strTabImageXML[iVarImage].ptr_fpBin = NULL;
	   strALLRecordXML->strTabImageXML[iVarImage].bFlagConstant = FALSE;
	   strALLRecordXML->strTabImageXML[iVarImage].usNbElem = 1;
	   sprintf(strALLRecordXML->strTabImageXML[iVarImage].cNom, "%s", cTabNomVarImage[iVarImage]);
	   sprintf(strALLRecordXML->strTabImageXML[iVarImage].cType, "%s", cTabNomTypeImage[iVarImage]);
	   sprintf(strALLRecordXML->strTabImageXML[iVarImage].cTag, "%s", "TODO");
	   sprintf(strALLRecordXML->strTabImageXML[iVarImage].cUnit, "%s", "TODO");
	   strALLRecordXML->strTabImageXML[iVarImage].fScaleFactor = fTabSFImage[iVarImage];
	   strALLRecordXML->strTabImageXML[iVarImage].fAddOffset = fTabAOImage[iVarImage];
	   usTaille = util_usNbOctetTypeVar(cTabNomTypeImage[iVarImage]);
	   strALLRecordXML->strTabImageXML[iVarImage].usSize = usTaille;
	   iLen = strlen(strALLRecordXML->strTabImageXML[iVarImage].cNom);
	   // Ajout d'une marge au cas o� certains caract�res soient doubl�s soient doubl�s.
	   sprintf(cNomFicPathBin, "%s\\%s%s", ptr_cRepertoire, strALLRecordXML->strTabImageXML[iVarImage].cNom, ".bin");
	   sprintf(strALLRecordXML->strTabImageXML[iVarImage].cNomPathFileBin, "%s", cNomFicPathBin);
	   strALLRecordXML->strTabImageXML[iVarImage].ptr_fpBin = fopen(cNomFicPathBin, "w+b");
	   if (!strALLRecordXML->strTabImageXML[iVarImage].ptr_fpBin)
	   {
		   printf("%s -- Error in Image creation  : %s\n", __FILE__, cNomFicPathBin);
		   return EXIT_FAILURE;
	   }
	}


	free(ptr_cRepertoire);
	return EXIT_SUCCESS;

} // iInitXMLBinInstallParamsRecord

#pragma pack()
