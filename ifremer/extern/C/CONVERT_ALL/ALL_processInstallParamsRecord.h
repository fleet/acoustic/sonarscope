/*
 * ALL_processInstallParamsRecord.h
 *
 *  Created on: 21 nov. 2008
 *      Author: rgallou
 */
#pragma pack(1)

#ifndef ALL_PROCESSINSTALLPARAMSRECORD_H_
#define ALL_PROCESSINSTALLPARAMSRECORD_H_

#include "ALL_DATAGRAMS.H"


typedef struct  {
	int 			NbOfBytesInDatagram;
	unsigned char	STX;
	unsigned char	TypeOfDatagram;
	unsigned short	EmModel;
	unsigned int	Date;
	unsigned int	NbMilliSecs;
	unsigned short	SurveyLineNumber;
	unsigned short	SystemSerialNumber;
	unsigned short	SerialNumberOfSecondSonarHead;
} T_INSTALLPARAMS_RECORD_SIGNAL;

typedef struct  {
		char	*Line;
} T_INSTALLPARAMS_RECORD_IMAGE;

int iProcessInstallParamsRecord(FILE *fpFileData, int iNbRecord,
						T_ALLRECORDXML *strALLRecordXML,
						T_INSTALLPARAMS_RECORD_SIGNAL *ptrSignal_prevRecord);


int iInitXMLBinInstallParamsRecord(	char *cTitleDatagram,
								char *cNomDatagram, char *cComments,
								T_ALLRECORDXML *strALLRecordXML);

#endif /* ALL_PROCESSINSTALLPARAMSRECORD_H_ */

#pragma pack()
