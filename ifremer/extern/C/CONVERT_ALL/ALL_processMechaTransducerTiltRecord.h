/*
 * ALL_processMechaTransducerTiltRecord.h
 *
 *  Created on: 21 nov. 2008
 *      Author: rgallou
 */
#pragma pack(1)

#ifndef ALL_PROCESSMECHATRANSDUCERTILTRECORD_H_
#define ALL_PROCESSMECHATRANSDUCERTILTRECORD_H_

#include "ALL_DATAGRAMS.H"


typedef struct  {
		int 			NbOfBytesInDatagram;
		unsigned char	STX;
		unsigned char	TypeOfDatagram;
		unsigned short	EmModel;
		unsigned int	Date;
		unsigned int	NbMilliSecs;
		unsigned short	TiltCounter;
		unsigned short	SystemSerialNumber;
		unsigned short	NbEntries;
} T_MECHATRANSDUCERTILT_RECORD_SIGNAL;

typedef struct  {
		unsigned short	*Time;
		short			*Tilt;
} T_MECHATRANSDUCERTILT_RECORD_IMAGE;

int iProcessMechaTransducerTiltRecord(FILE *fpFileData, int iNbRecord,
						T_ALLRECORDXML *strALLRecordXML,
						T_MECHATRANSDUCERTILT_RECORD_SIGNAL *ptrSignal_prevRecord);


int iInitXMLBinMechaTransducerTiltRecord(	char *cTitleDatagram,
								char *cNomDatagram,
								char *cComments,
								T_ALLRECORDXML *strALLRecordXML);

#endif /* ALL_PROCESSMECHATRANSDUCERTILTRECORD_H_ */

#pragma pack()
