/*
 * ALL_processNetworkAttitudeRecord.h
 *
 *  Created on: 21 nov. 2008
 *      Author: rgallou
 */
#pragma pack(1)

#ifndef ALL_PROCESSNETWORKATTITUDERECORD_H_
#define ALL_PROCESSNETWORKATTITUDERECORD_H_

#include "ALL_DATAGRAMS.H"


typedef struct  {
		int 			NbOfBytesInDatagram;
		unsigned char	STX;
		unsigned char	TypeOfDatagram;
		unsigned short	EmModel;
		unsigned int	Date;
		unsigned int	NbMilliSecs;
		unsigned short	NetworkAttitudeCounter;
		unsigned short	SystemSerialNumber;
		unsigned short	NbEntries;
		unsigned char	SensorSystemDescriptor;
		unsigned char	Spare;
} T_NETWORKATTTIUDE_RECORD_SIGNAL;

typedef struct  {
		unsigned short	*TS;
		short			*Roll;
		short			*Pitch;
		short			*Heave;
		unsigned short	*Heading;
		unsigned char	*NbBytes;
		unsigned char	*NetworkAttitude;
} T_NETWORKATTTIUDE_RECORD_IMAGE;

int iProcessNetworkAttitudeRecord(FILE *fpFileData,
						int iNbRecord,
						T_ALLRECORDXML *strALLRecordXML,
						T_NETWORKATTTIUDE_RECORD_SIGNAL *ptrSignal_prevRecord);


int iInitXMLBinNetworkAttitudeRecord(	char *cTitleDatagram,
								char *cNomDatagram,
								char *cComments,
								T_ALLRECORDXML *strALLRecordXML);

#endif // ALL_PROCESSNETWORKATTTIUDERECORD_H_

#pragma pack()
