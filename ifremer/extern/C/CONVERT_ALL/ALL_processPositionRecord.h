/*
 * ALL_processPositionRecord.h
 *
 *  Created on: 21 nov. 2008
 *      Author: rgallou
 */
#pragma pack(1)

#ifndef ALL_PROCESSPOSITIONRECORD_H_
#define ALL_PROCESSPOSITIONRECORD_H_

#include "ALL_DATAGRAMS.H"


typedef struct  {
		int 			NbOfBytesInDatagram;
		unsigned char	STX;
		unsigned char	TypeOfDatagram;
		unsigned short	EmModel;
		unsigned int	Date;
		int				NbMilliSecs;
		unsigned short	Counter;
		unsigned short	SystemSerialNumber;
		int				Latitude;
		int				Longitude;
		unsigned short	Quality;
		unsigned short	Speed;
		unsigned short	CourseVessel;
		unsigned short	Heading;
		unsigned char	PositionDescriptor;
		unsigned char	NumberOfBytesPosInput;
} T_POS_RECORD_SIGNAL;

typedef struct  {
		char	*PositionInput;
} T_POS_RECORD_IMAGE;

int iProcessPositionRecord(FILE *fpFileData, int iNbRecord,
						T_ALLRECORDXML *strALLRecordXML,
						T_POS_RECORD_SIGNAL *ptrSignal_prevRecord);

int iInitXMLBinPositionRecord(	char *cTitleDatagram,
								char *cNomDatagram, char *cComments,
								T_ALLRECORDXML *strALLRecordXML);

#endif /* ALL_PROCESSPOSITIONRECORD_H_ */

#pragma pack()
