/*
 * ALL_processQualityFactorFromIfrRecord.h
 *
 *  Created on: 17/12/2012
 *  Facteur Qualit� IFREMER, cf . Doc 160692ao_em_datagram_formats.pdf
 *      Author: rgallou
 */
#pragma pack(1)

#ifndef ALL_PROCESSQUALITYFACTORRECORDFROMIFR_H_
#define ALL_PROCESSQUALITYFACTORRECORDFROMIFR_H_

#include "ALL_DATAGRAMS.H"


typedef struct  {
		int 			NbOfBytesInDatagram;
		unsigned char	STX;
		unsigned char	TypeOfDatagram;
		unsigned short	EmModel;
		unsigned int	Date;
		unsigned int	NbMilliSecs;
		unsigned short	PingCounter;
		unsigned short	SystemSerialNumber;
		unsigned short	NbRx;
		unsigned char	NbParam;
		unsigned char	Spare;
} T_QUALITYFACTORFROMIFR_RECORD_SIGNAL;

typedef struct  {
		float			*IfremerQF;
} T_QUALITYFACTORFROMIFR_RECORD_IMAGE;

int iProcessQualityFactorFromIfrRecord(FILE *fpFileData,
										int iNbRecord,
										T_ALLRECORDXML *strALLRecordXML,
										T_QUALITYFACTORFROMIFR_RECORD_SIGNAL *ptrSignal_prevRecord);


int iInitXMLBinQualityFactorFromIfrRecord(	char *cTitleDatagram,
											char *cNomDatagram,
											char *cComments,
											T_ALLRECORDXML *strALLRecordXML);

#endif /* ALL_PROCESSQUALITYFACTORRECORDFROMIFR_H_ */

#pragma pack()
