/*
 * ALL_processQualityFactorRecord.h
 *
 *  Created on: 16/05/2009
 *      Author: rgallou
 */
#pragma pack(1)

#ifndef ALL_PROCESSQUALITYFACTORRECORD_H_
#define ALL_PROCESSQUALITYFACTORRECORD_H_

#include "ALL_DATAGRAMS.H"


typedef struct  {
		int 			NbOfBytesInDatagram;
		unsigned char	STX;
		unsigned char	TypeOfDatagram;
		unsigned short	EmModel;
		unsigned int	Date;
		unsigned int	NbMilliSecs;
		unsigned short	PingCounter;
		unsigned short	SystemSerialNumber;
		unsigned short	NbDetections;
		unsigned short	Spare;
} T_QUALITYFACTOR_RECORD_SIGNAL;

typedef struct  {
		float			*TwoWayTTAmpDetect;
		float			*TwoWayTTPhaseDetect;
		float			*AmpDetectQF;
		float			*PhaseDetectQF;
		unsigned char	*AmpDetectStatus;
		unsigned char	*PhaseDetectStatus;
		unsigned short	*RawBeamNumber;
		float			*RawBeamAngle;
} T_QUALITYFACTOR_RECORD_IMAGE;

int iProcessQualityFactorRecord(FILE *fpFileData, int iNbRecord,
						T_ALLRECORDXML *strALLRecordXML,
						T_QUALITYFACTOR_RECORD_SIGNAL *ptrSignal_prevRecord);


int iInitXMLBinQualityFactorRecord(	char *cTitleDatagram,
								char *cNomDatagram, char *cComments,
								T_ALLRECORDXML *strALLRecordXML);

#endif /* ALL_PROCESSQUALITYFACTORRECORD_H_ */

#pragma pack()
