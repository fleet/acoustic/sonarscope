/*
 * ALL_processRawBeamSamplesRecord.h
 *
 *  Created on: 27/01/2010
 *      Author: rgallou
 */
#pragma pack(1)

#ifndef ALL_PROCESSRAWBEAMSAMPLESRECORD_H_
#define ALL_PROCESSRAWBEAMSAMPLESRECORD_H_

#include "ALL_DATAGRAMS.H"


typedef struct  {
		unsigned int 	NbOfBytesInDatagram;
		unsigned char	STX;
		unsigned char	TypeOfDatagram;
		unsigned short	EmModel;
		unsigned int	Date;
		unsigned int	NbMilliSecs;
		unsigned short	PingCounter;
		unsigned short	SystemSerialNumber;
		unsigned short	NumberOfDatagrams;
		unsigned short	DatagramNumbers;
		unsigned short	NTx;
		unsigned short	TotalOfReceiveBeams;
		unsigned short	NRx;
		unsigned short	SoundSpeed;
		unsigned int	SamplingFreq;
		short			TxTimeHeave;
		unsigned char	xLog;
		unsigned char	cLog;
		unsigned int	Spare;
} T_RAWBEAMSAMPLES_RECORD_SIGNAL;

typedef struct  {
		short			*TiltAngle;
		unsigned short	*CenterFrequency;
		unsigned char	*TransmitSectorNumber;
		unsigned char	*Spare2;
} T_RAWBEAMSAMPLES_RECORD_IMAGE;


typedef struct  {
		short			*BeamPointingAngle;
		unsigned short	*StartRangeNumber;
		unsigned short	*NumberOfSamples;
		unsigned short	*DetectedRangeInSamples;
		unsigned char	*TransmitSectorNumber;
		unsigned char	*RxBeamNumber;
		unsigned short	*SampleAmplitude;
		short			*SamplePhase;
} T_RAWBEAMSAMPLES_RECORD_IMAGE2;


int iProcessRawBeamSamplesRecord(	FILE *fpFileData,
									int iNbRecord,
									T_ALLRECORDXML *strALLRecordXML,
									T_RAWBEAMSAMPLES_RECORD_SIGNAL *ptrSignal_prevRecord);


int iInitXMLBinRawBeamSamplesRecord(	char *cTitleDatagram,
										char *cNomDatagram, char *cComments,
										T_ALLRECORDXML *strALLRecordXML);

#endif /* ALL_PROCESSRAWBEAMSAMPLESRECORD_H_ */

#pragma pack()
