/*
 * ALL_processRawRangeBeamAngleV0Record.c
 *
 *  Created on: 13 oct. 2008
 *      Author: rgallou
 */
#pragma pack(1)

#include <sys/types.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "ALL_Datagrams.h"
#include "ALL_Utilities.h"
#include "Generic_Utilities.h"
#include "ALL_processRawRangeBeamAngleV0Record.h"
#include "convertFiles_ALL.h"


//----------------------------------------------------------------
// Fonction :	iProcessRawRangeBeamAngleV0Record
// Objet 	:	Traitement du paquet RawRangeBeamAngleV0
// Modif.	: 	03/11/08
// Auteur 	:	GLU
//----------------------------------------------------------------
int iProcessRawRangeBeamAngleV0Record(FILE *fpFileData, int iNbRecord,
						T_ALLRECORDXML *strALLRecordXML,
						T_RAWRANGEBEAMANGLEV0_RECORD_SIGNAL *ptrSignal_prevRecord)
{
	T_RAWRANGEBEAMANGLEV0_RECORD_SIGNAL	*ptrSignal_Record;

	// Pointeur pour avancer sur les variables automatiquement.
	unsigned char		*ptr_cSignalRecord;
	static void 		*ptr_vVarPrevRecord;
	void				*ptr_vVarRecord,
						*ptr_vBidon;

	BOOL		bFlagVarConstant = TRUE,
				bFlagCompare = TRUE;


	int 	iFlag,
			iVarSignal,
			iVarImage,
			iRet = 0,
			iLoop;

	// Les paquets de donn�es peuvent �tre vides, il faut marquer
	// si on a d�j� initialis� les variables.
	static BOOL bFlagPrevAlloc = FALSE;

    unsigned short	usTailleEnByte,
					usNbElem;

    // Initialisation de la structure d'identification du paquet
 	if (iNbRecord == 0)
	{
 		iInitXMLBinRawRangeBeamAngleV0Record(	"RawRangeBeamAngle46h",
												"RawRangeBeamAngle46h",
												"Only used for EM 3000",
												strALLRecordXML);
	}

    // Lecture des signaux du paquet
	ptrSignal_Record = (T_RAWRANGEBEAMANGLEV0_RECORD_SIGNAL *)malloc(sizeof(T_RAWRANGEBEAMANGLEV0_RECORD_SIGNAL));
	iFlag = fread(ptrSignal_Record, sizeof(T_RAWRANGEBEAMANGLEV0_RECORD_SIGNAL), 1, fpFileData);
	if (iFlag == 0)
	{
		printf("%s -- Error in reading : %s\n", __FILE__, G_cFileData);
		return FALSE;
	}
	ptr_cSignalRecord = (unsigned char*)ptrSignal_Record;
	// Sauvegarde des donn�es des attributs.
	if (iNbRecord > 0)
	{
		ptr_vVarPrevRecord = (void *)ptrSignal_prevRecord;
	}

	for (iVarSignal=0; iVarSignal<(int)strALLRecordXML->sNbVarSignal; iVarSignal++)
	{
		ptr_vVarRecord = (void *)ptr_cSignalRecord;
		if (iVarSignal==0)
		{
			usTailleEnByte = 0;
			usNbElem = 0;
		}
		else
		{
			usTailleEnByte = util_usNbOctetTypeVar(strALLRecordXML->strTabSignalXML[iVarSignal-1].cType);
			usNbElem = strALLRecordXML->strTabSignalXML[iVarSignal-1].usNbElem;
		}
		// Pointage sur la donn�e suivante.
		ptr_vVarPrevRecord = ptr_vVarPrevRecord + usTailleEnByte*usNbElem;
		// On ne fait pas le test de constance au 1er enregistrement ou si le signal
		// est d�j� vue comme variable.
		if (iNbRecord > 0 && strALLRecordXML->strTabSignalXML[iVarSignal].bFlagConstant == TRUE)
		{
			bFlagVarConstant = strALLRecordXML->strTabSignalXML[iVarSignal].bFlagConstant;
			if (G_iFlagEndian == 0) // On inverse en Big Endian.
			{
				// Observation de la taille propre � la variable.
				usTailleEnByte 	= util_usNbOctetTypeVar(strALLRecordXML->strTabSignalXML[iVarSignal].cType);
				usNbElem 		= strALLRecordXML->strTabSignalXML[iVarSignal].usNbElem;
				if (usTailleEnByte == 2) // Pour des (u)short, (u)int, (u)long.
					ptr_vVarPrevRecord = (void *)util_cBswap_16(ptr_vVarPrevRecord);
				else if (usTailleEnByte == 4)
					ptr_vVarPrevRecord = (void *)util_cBswap_32(ptr_vVarPrevRecord);
				else if (usTailleEnByte == 8)
					ptr_vVarPrevRecord = (void *)util_cBswap_64(ptr_vVarPrevRecord);
			}
			bFlagCompare 	= util_bComparePrevAndCurrentVal(strALLRecordXML->strTabSignalXML[iVarSignal].cType,
												ptr_vVarPrevRecord,
												ptr_vVarRecord,
											   strALLRecordXML->strTabSignalXML[iVarSignal].usNbElem);
			bFlagVarConstant = bFlagVarConstant && bFlagCompare;
			strALLRecordXML->strTabSignalXML[iVarSignal].bFlagConstant = bFlagVarConstant;
		}


		// Pr�paration de l'�criture de la variable.
		usTailleEnByte = strALLRecordXML->strTabSignalXML[iVarSignal].usSize;
		usNbElem =  strALLRecordXML->strTabSignalXML[iVarSignal].usNbElem;
		if (G_iFlagEndian == 0) // On inverse en Big Endian.
		{
			if (usTailleEnByte == 2) // Pour des (u)short, (u)int, (u)long.
				ptr_cSignalRecord = (unsigned char*)util_cBswap_16((char*)ptr_cSignalRecord);
			else if (usTailleEnByte == 4)
				ptr_cSignalRecord = (unsigned char*)util_cBswap_32((char*)ptr_cSignalRecord);
			else if (usTailleEnByte == 8)
				ptr_cSignalRecord = (unsigned char*)util_cBswap_64((char*)ptr_cSignalRecord);
		}
		fwrite(ptr_cSignalRecord, usTailleEnByte, usNbElem, strALLRecordXML->strTabSignalXML[iVarSignal].ptr_fpBin);
				strALLRecordXML->strTabSignalXML[iVarSignal].ptr_vPrevVal = ptr_cSignalRecord;
		ptr_cSignalRecord += usTailleEnByte*usNbElem;
	}
	// Recopie en zone tampon des valeurs courantes.
	memmove(ptrSignal_prevRecord, ptrSignal_Record, sizeof(T_RAWRANGEBEAMANGLEV0_RECORD_SIGNAL));

	// --------------------------------------------
 	// Lecture unitaire des variables de type Image
	for (iLoop=0; iLoop< ptrSignal_Record->NbBeams; iLoop++)
	{
		// Lecture unitaire des variables.
		for (iVarImage=0; iVarImage< strALLRecordXML->sNbVarImage; iVarImage++)
		{
			usTailleEnByte = strALLRecordXML->strTabImageXML[iVarImage].usSize;
			usNbElem = strALLRecordXML->strTabImageXML[iVarImage].usNbElem;
			ptr_vBidon = (void *)malloc((usTailleEnByte*usNbElem)*sizeof(char));
			iFlag = fread(ptr_vBidon, usTailleEnByte, usNbElem, fpFileData);
			if (G_iFlagEndian == 0) // On inverse en Big Endian.
			{
				if (usTailleEnByte == 2) // Pour des (u)short, (u)int, (u)long.
					ptr_vBidon = (void *)util_cBswap_16(ptr_vBidon);
				else if (usTailleEnByte == 4)
					ptr_vBidon = (void *)util_cBswap_32(ptr_vBidon);
				else if (usTailleEnByte == 8)
					ptr_vBidon = (void *)util_cBswap_64(ptr_vBidon);
			}
			iFlag = fwrite(ptr_vBidon, usTailleEnByte, usNbElem, strALLRecordXML->strTabImageXML[iVarImage].ptr_fpBin);

			// Traitement du caract�re constant de la donn�e.
			if (iNbRecord == 0 || bFlagPrevAlloc == FALSE)
			{
				strALLRecordXML->strTabImageXML[iVarImage].ptr_vPrevVal = malloc(usTailleEnByte*usNbElem*sizeof(char));
			}
			else if (strALLRecordXML->strTabImageXML[iVarImage].bFlagConstant == TRUE)
			{
				bFlagCompare = util_bComparePrevAndCurrentVal(strALLRecordXML->strTabImageXML[iVarImage].cType,
													strALLRecordXML->strTabImageXML[iVarImage].ptr_vPrevVal,
													ptr_vBidon,
													usNbElem);
				strALLRecordXML->strTabImageXML[iVarImage].bFlagConstant = bFlagCompare && strALLRecordXML->strTabImageXML[iVarImage].bFlagConstant;
			}
			memmove(strALLRecordXML->strTabImageXML[iVarImage].ptr_vPrevVal, ptr_vBidon, usTailleEnByte*usNbElem);

			free(ptr_vBidon);

		} // Fin de la boucles sur les variables.

		// Marquage du traitement d'initialisation des variables de type Image
		if (iLoop == 0)
		{
			bFlagPrevAlloc = TRUE;
		}
	} // Fin de la boucle sur le nombre de faisceaux.

	free(ptrSignal_Record);

	iNbRecord++;

	return iRet;

} //iProcessRawRangeBeamAngleV0Record


//----------------------------------------------------------------
// Fonction :	iInitXMLBinRawRangeBeamAngleV0Record
// Objet 	:	Initialisation de la structure �quivalent au paquet
//				RawRangeBeamAngleV0 pour la manipulation des fichiers bin et XML.
// Modification : 15/10/08
// Auteur 	: GLU
//----------------------------------------------------------------
int iInitXMLBinRawRangeBeamAngleV0Record(	char *cTitleDatagram,
								char *cNomDatagram,
								char *cComments,
								T_ALLRECORDXML *strALLRecordXML)
{

	char cTabNomVarSignal[11][30] = {
			"NbOfBytesInDatagram",
			"STX",
			"TypeOfDatagram",
			"EmModel",
			"Date",
			"NbMilliSecs",
			"PingCounter",
			"SystemSerialNumber",
			"MaxNbBeamsPossible",
			"NbBeams",
			"SoundSpeed"};

	char cTabNomTypeSignal[11][20] = {
			"int",
			"u8",
			"u8",
			"unsigned short",
			"unsigned int",
			"unsigned int",
			"unsigned short",
			"unsigned short",
			"u8",
			"u8",
			"unsigned short"};

	// ScaleFactor
	float 	fTabSFSignal[11] = {
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			0.1};	// en m/s

	// AddOffset
	float 	fTabAOSignal[11] = {
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0};

	char cTabNomVarImage[5][30] = {
			"BeamPointingAngle",
			"TransmitTiltAngle",
			"Range",
			"Reflectivity",
			"BeamNumber"};

	char cTabNomTypeImage[5][20] = {
			"short",
			"short",
			"unsigned short",
			"i8",
			"u8"};

	// ScaleFactor
	float 	fTabSFImage[5] = {
			0.01,	// en deg
			0.01,	// en deg
			0.5,	// en samples
			0.5,	// en dB
			1.0};

	// AddOffset
	float 	fTabAOImage[5] = {
			0.0,
			0.0,
			0.0,
			0.0,
			0.0};

	char	*ptr_cNomHeader,
			*ptr_cRepertoire,
			cNomFicPathBin[300];

    int 	iSizeRep,
			iErr,
			iRet,
			iLen,
			iVarSignal,
			iVarImage;

	unsigned short usTaille;

	// Initialisation des structures de pilotage des fichiers
	// XML et Bin .
	strALLRecordXML->sNbVarSignal = 11;
	strALLRecordXML->sNbVarImage = 5;
	strALLRecordXML->sNbVarImage2 = 0;
    strALLRecordXML->strTabSignalXML= (T_ALLVARXML *)malloc(strALLRecordXML->sNbVarSignal*sizeof(T_ALLVARXML));
    strALLRecordXML->strTabImageXML= (T_ALLVARXML *)malloc(strALLRecordXML->sNbVarImage*sizeof(T_ALLVARXML));

	iLen = strlen(cNomDatagram)+1;
	strALLRecordXML->ptr_cNomHeader = malloc(iLen*sizeof(char));
	sprintf(strALLRecordXML->ptr_cNomHeader, "%s", cNomDatagram);

	iLen = strlen(cTitleDatagram)+1;
	strALLRecordXML->ptr_cLabelHeader = malloc(iLen*sizeof(char));
	sprintf(strALLRecordXML->ptr_cLabelHeader, "%s", cTitleDatagram);

	iLen = strlen(cComments)+1;
	strALLRecordXML->ptr_cCommentsHeader = malloc(iLen*sizeof(char));
	sprintf(strALLRecordXML->ptr_cCommentsHeader, "%s", cComments);

	// Remplissage du nom de la variable d'indexation des tables.
	iLen = strlen(cTabNomVarSignal[9])+1;
	strALLRecordXML->ptr_cNomIndexTable = malloc(iLen*sizeof(char));
	sprintf(strALLRecordXML->ptr_cNomIndexTable, "%s", cTabNomVarSignal[9]);

	// Affectation et cr�ation du repertoire du Datagramme.
	iSizeRep = ( strlen(G_cRepData) + strlen( cNomDatagram));
	// Ajout d'une marge au cas o� certains caract�res soient doubl�s soient doubl�s.
	ptr_cRepertoire = malloc( (iSizeRep + 1 + 30 + 4)*sizeof(char));
	iRet = util_iCreateDirDatagram(G_cRepData,
								strALLRecordXML->ptr_cNomHeader,
								ptr_cRepertoire);

	// Initialisation g�n�rale des variables de type Signal
	for (iVarSignal=0; iVarSignal<strALLRecordXML->sNbVarSignal; iVarSignal++) {
	   strALLRecordXML->strTabSignalXML[iVarSignal].ptr_fpBin = NULL;
	   strALLRecordXML->strTabSignalXML[iVarSignal].bFlagConstant = TRUE;
	   strALLRecordXML->strTabSignalXML[iVarSignal].usNbElem = 1;
	   sprintf(strALLRecordXML->strTabSignalXML[iVarSignal].cNom, "%s", cTabNomVarSignal[iVarSignal]);
	   sprintf(strALLRecordXML->strTabSignalXML[iVarSignal].cType, "%s", cTabNomTypeSignal[iVarSignal]);
	   sprintf(strALLRecordXML->strTabSignalXML[iVarSignal].cTag, "%s", "TODO");
	   sprintf(strALLRecordXML->strTabSignalXML[iVarSignal].cUnit, "%s", "TODO");
	   strALLRecordXML->strTabSignalXML[iVarSignal].fScaleFactor = fTabSFSignal[iVarSignal];
	   strALLRecordXML->strTabSignalXML[iVarSignal].fAddOffset = fTabAOSignal[iVarSignal];
	   usTaille = util_usNbOctetTypeVar(cTabNomTypeSignal[iVarSignal]);
	   strALLRecordXML->strTabSignalXML[iVarSignal].usSize = usTaille;
	   iLen = strlen(strALLRecordXML->strTabSignalXML[iVarSignal].cNom);
	   // Ajout d'une marge au cas o� certains caract�res soient doubl�s soient doubl�s.
	   sprintf(cNomFicPathBin, "%s\\%s%s", ptr_cRepertoire, strALLRecordXML->strTabSignalXML[iVarSignal].cNom, ".bin");
	   sprintf(strALLRecordXML->strTabSignalXML[iVarSignal].cNomPathFileBin, "%s", cNomFicPathBin);
	   strALLRecordXML->strTabSignalXML[iVarSignal].ptr_fpBin = fopen(cNomFicPathBin, "w+b");
	   if (!strALLRecordXML->strTabSignalXML[iVarSignal].ptr_fpBin)
	   {
		   printf("%s -- Error in Signal creation  : %s\n", __FILE__, cNomFicPathBin);
		   return EXIT_FAILURE;
	   }
	}

	// Initialisation g�n�rale des variables de type Image
	for (iVarImage=0; iVarImage<strALLRecordXML->sNbVarImage; iVarImage++) {
	   strALLRecordXML->strTabImageXML[iVarImage].ptr_fpBin = NULL;
	   strALLRecordXML->strTabImageXML[iVarImage].bFlagConstant = TRUE;
	   strALLRecordXML->strTabImageXML[iVarImage].usNbElem = 1;
	   sprintf(strALLRecordXML->strTabImageXML[iVarImage].cNom, "%s", cTabNomVarImage[iVarImage]);
	   sprintf(strALLRecordXML->strTabImageXML[iVarImage].cType, "%s", cTabNomTypeImage[iVarImage]);
	   sprintf(strALLRecordXML->strTabImageXML[iVarImage].cTag, "%s", "TODO");
	   sprintf(strALLRecordXML->strTabImageXML[iVarImage].cUnit, "%s", "TODO");
	   strALLRecordXML->strTabImageXML[iVarImage].fScaleFactor = fTabSFImage[iVarImage];
	   strALLRecordXML->strTabImageXML[iVarImage].fAddOffset = fTabAOImage[iVarImage];
	   usTaille = util_usNbOctetTypeVar(cTabNomTypeImage[iVarImage]);
	   strALLRecordXML->strTabImageXML[iVarImage].usSize = usTaille;
	   iLen = strlen(strALLRecordXML->strTabImageXML[iVarImage].cNom);
	   // Ajout d'une marge au cas o� certains caract�res soient doubl�s soient doubl�s.
	   sprintf(cNomFicPathBin, "%s\\%s%s", ptr_cRepertoire, strALLRecordXML->strTabImageXML[iVarImage].cNom, ".bin");
	   sprintf(strALLRecordXML->strTabImageXML[iVarImage].cNomPathFileBin, "%s", cNomFicPathBin);
	   strALLRecordXML->strTabImageXML[iVarImage].ptr_fpBin = fopen(cNomFicPathBin, "w+b");
	   if (!strALLRecordXML->strTabImageXML[iVarImage].ptr_fpBin)
	   {
		   printf("%s -- Error in Image creation  : %s\n", __FILE__, cNomFicPathBin);
		   return EXIT_FAILURE;
	   }
	}


	free(ptr_cRepertoire);
	return EXIT_SUCCESS;

} // iInitXMLBinRawRangeBeamAngleV0Record

#pragma pack()
