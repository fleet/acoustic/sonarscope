/*
 * ALL_processRawRangeBeamAngleV0Record.h
 *
 *  Created on: 21 nov. 2008
 *      Author: rgallou
 */
#pragma pack(1)

#ifndef ALL_PROCESSRAWRANGEBEAMANGLEV0RECORD_H_
#define ALL_PROCESSRAWRANGEBEAMANGLEV0RECORD_H_

#include "ALL_DATAGRAMS.H"


typedef struct  {
		int 			NbOfBytesInDatagram;
		unsigned char	STX;
		unsigned char	TypeOfDatagram;
		unsigned short	EmModel;
		unsigned int	Date;
		unsigned int	NbMilliSecs;
		unsigned short	RawRangeBeamAngleV0Counter;
		unsigned short	SystemSerialNumber;
		unsigned char	MaxNbBeamsPossible;
		unsigned char	NbBeams;
		unsigned short	SoundSpeed;
} T_RAWRANGEBEAMANGLEV0_RECORD_SIGNAL;

typedef struct  {
		short			*BeamPointingAngle;
		unsigned short	*TransmitTiltAngle;
		unsigned short	*Range;
		char			*Reflectivity;
		unsigned char	*BeamNumber;
} T_RAWRANGEBEAMANGLEV0_RECORD_IMAGE;

int iProcessRawRangeBeamAngleV0Record(FILE *fpFileData, int iNbRecord,
						T_ALLRECORDXML *strALLRecordXML,
						T_RAWRANGEBEAMANGLEV0_RECORD_SIGNAL *ptrSignal_prevRecord);


int iInitXMLBinRawRangeBeamAngleV0Record(	char *cTitleDatagram,
								char *cNomDatagram, char *cComments,
								T_ALLRECORDXML *strALLRecordXML);

#endif /* ALL_PROCESSRAWRANGEBEAMANGLEV0RECORD_H_ */

#pragma pack()
