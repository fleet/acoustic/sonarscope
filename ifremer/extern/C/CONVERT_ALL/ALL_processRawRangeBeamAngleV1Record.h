/*
 * ALL_processRawRangeBeamAngleV1Record.h
 *
 *  Created on: 21 nov. 2008
 *      Author: rgallou
 */
#pragma pack(1)

#ifndef ALL_PROCESSRAWRANGEBEAMANGLEV1RECORD_H_
#define ALL_PROCESSRAWRANGEBEAMANGLEV1RECORD_H_

#include "ALL_DATAGRAMS.H"


typedef struct  {
		int 			NbOfBytesInDatagram;
		unsigned char	STX;
		unsigned char	TypeOfDatagram;
		unsigned short	EmModel;
		unsigned int	Date;
		unsigned int	NbMilliSecs;
		unsigned short	RawRangeBeamAngleV1Counter;
		unsigned short	SystemSerialNumber;
		unsigned short	NTx;
		unsigned short	NRx;
		unsigned int	SamplingRate;
		int				ROV_Depth;
		unsigned short	SoundSpeed;
		unsigned short	MaxNbBeamsPossible;
		unsigned short	Spare1;
		unsigned short	Spare2;
} T_RAWRANGEBEAMANGLEV1_RECORD_SIGNAL;

typedef struct  {
		short			*TiltAngle;
		unsigned short	*FocusRange;
		unsigned int	*SignalLength;
		unsigned int	*SectorTransmitDelay;
		unsigned int	*CentralFrequency;
		unsigned short	*SignalBandwith;
		unsigned char	*SignalWaveformIdentifier;
		unsigned char	*TransmitSectorNumberTx;
} T_RAWRANGEBEAMANGLEV1_RECORD_IMAGE;

typedef struct  {
		short			*BeamPointingAngle;
		unsigned short	*Range;
		unsigned char	*TransmitSectorNumberRx;
		char			*Reflectivity;
		unsigned char	*QualityFactor;
		unsigned char	*LengthDetection;
		short			*RxBeamNumber;
		unsigned short	*Spare;
} T_RAWRANGEBEAMANGLEV1_RECORD_IMAGE2;


int iProcessRawRangeBeamAngleV1Record(FILE *fpFileData, int iNbRecord,
						T_ALLRECORDXML *strALLRecordXML,
						T_RAWRANGEBEAMANGLEV1_RECORD_SIGNAL *ptrSignal_prevRecord);


int iInitXMLBinRawRangeBeamAngleV1Record(	char *cTitleDatagram,
								char *cNomDatagram, char *cComments,
								T_ALLRECORDXML *strALLRecordXML);

#endif /* ALL_PROCESSRAWRANGEBEAMANGLEV1RECORD_H_ */

#pragma pack()
