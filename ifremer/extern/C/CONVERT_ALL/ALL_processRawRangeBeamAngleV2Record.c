/*
 * ALL_processRawRangeBeamAngleV2Record.c
 *
 *  Created on: 13 oct. 2008
 *      Author: rgallou
 */
#pragma pack(1)

#include <sys/types.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "ALL_Datagrams.h"
#include "ALL_Utilities.h"
#include "Generic_Utilities.h"
#include "ALL_processRawRangeBeamAngleV2Record.h"
#include "convertFiles_ALL.h"


//----------------------------------------------------------------
// Fonction :	iProcessRawRangeBeamAngleV2Record
// Objet 	:	Traitement du paquet RawRangeBeamAngleV2 (4eh, 78d)
// Modif.	: 	03/11/08
// Auteur 	:	GLU
//----------------------------------------------------------------
int iProcessRawRangeBeamAngleV2Record(FILE *fpFileData, int iNbRecord,
						T_ALLRECORDXML *strALLRecordXML,
						T_RAWRANGEBEAMANGLEV2_RECORD_SIGNAL *ptrSignal_prevRecord)
{
	T_RAWRANGEBEAMANGLEV2_RECORD_SIGNAL	*ptrSignal_Record;

	// Pointeur pour avancer sur les variables automatiquement.
	unsigned char		*ptr_cSignalRecord;
	static void 		*ptr_vVarPrevRecord;
	void				*ptr_vVarRecord,
						*ptr_vBidon;

	BOOL		bFlagVarConstant = TRUE,
				bFlagCompare = TRUE;

    int 	iFlag,
			iVarSignal,
			iVarImage,
			iRet = 0,
			iLoop;

	// Les paquets de donn�es peuvent �tre vides, il faut marquer
	// si on a d�j� initialis� les variables.
	static BOOL bFlagPrevAlloc = FALSE;

    unsigned short	usTailleEnByte,
					usNbElem;

    // Initialisation de la structure d'identification du paquet
 	if (iNbRecord == 0)
	{
 		iInitXMLBinRawRangeBeamAngleV2Record(	"RawRangeBeamAngle4eh",
									"RawRangeBeamAngle4eh",
									"For new MB Models, starting for EM 710",
									strALLRecordXML);
	}

    // Lecture des signaux du paquet
	ptrSignal_Record = (T_RAWRANGEBEAMANGLEV2_RECORD_SIGNAL *)malloc(sizeof(T_RAWRANGEBEAMANGLEV2_RECORD_SIGNAL));
	iFlag = fread(ptrSignal_Record, sizeof(T_RAWRANGEBEAMANGLEV2_RECORD_SIGNAL), 1, fpFileData);
	if (iFlag == 0)
	{
		printf("%s -- Error in reading : %s\n", __FILE__, G_cFileData);
		return FALSE;
	}
	ptr_cSignalRecord = (unsigned char*)ptrSignal_Record;
	// Sauvegarde des donn�es des attributs.
	if (iNbRecord > 0)
	{
		ptr_vVarPrevRecord = (void *)ptrSignal_prevRecord;
	}

	for (iVarSignal=0; iVarSignal<(int)strALLRecordXML->sNbVarSignal; iVarSignal++)
	{
		ptr_vVarRecord = (void *)ptr_cSignalRecord;
		if (iVarSignal==0)
		{
			usTailleEnByte = 0;
			usNbElem = 0;
		}
		else
		{
			usTailleEnByte = util_usNbOctetTypeVar(strALLRecordXML->strTabSignalXML[iVarSignal-1].cType);
			usNbElem = strALLRecordXML->strTabSignalXML[iVarSignal-1].usNbElem;
		}
		// Pointage sur la donn�e suivante.
		ptr_vVarPrevRecord = ptr_vVarPrevRecord + usTailleEnByte*usNbElem;
		// On ne fait pas le test de constance au 1er enregistrement ou si le signal
		// est d�j� vue comme variable.
		if (iNbRecord > 0 && strALLRecordXML->strTabSignalXML[iVarSignal].bFlagConstant == TRUE)
		{
			bFlagVarConstant = strALLRecordXML->strTabSignalXML[iVarSignal].bFlagConstant;
			if (G_iFlagEndian == 0) // On inverse en Big Endian.
			{
				// Observation de la taille propre � la variable.
				usTailleEnByte = util_usNbOctetTypeVar(strALLRecordXML->strTabSignalXML[iVarSignal].cType);
				usNbElem = strALLRecordXML->strTabSignalXML[iVarSignal].usNbElem;
				if (usTailleEnByte == 2) // Pour des (u)short, (u)int, (u)long.
					ptr_vVarPrevRecord = (void *)util_cBswap_16(ptr_vVarPrevRecord);
				else if (usTailleEnByte == 4)
					ptr_vVarPrevRecord = (void *)util_cBswap_32(ptr_vVarPrevRecord);
				else if (usTailleEnByte == 8)
					ptr_vVarPrevRecord = (void *)util_cBswap_64(ptr_vVarPrevRecord);
			}
			bFlagCompare = util_bComparePrevAndCurrentVal(strALLRecordXML->strTabSignalXML[iVarSignal].cType,
												ptr_vVarPrevRecord,
												ptr_vVarRecord,
											   strALLRecordXML->strTabSignalXML[iVarSignal].usNbElem);
			bFlagVarConstant = bFlagVarConstant && bFlagCompare;
			strALLRecordXML->strTabSignalXML[iVarSignal].bFlagConstant = bFlagVarConstant;
		}


		// Pr�paration de l'�criture de la variable.
		usTailleEnByte = strALLRecordXML->strTabSignalXML[iVarSignal].usSize;
		usNbElem =  strALLRecordXML->strTabSignalXML[iVarSignal].usNbElem;
		if (G_iFlagEndian == 0) // On inverse en Big Endian.
		{
			if (usTailleEnByte == 2) // Pour des (u)short, (u)int, (u)long.
				ptr_cSignalRecord = (unsigned char*)util_cBswap_16((char*)ptr_cSignalRecord);
			else if (usTailleEnByte == 4)
				ptr_cSignalRecord = (unsigned char*)util_cBswap_32((char*)ptr_cSignalRecord);
			else if (usTailleEnByte == 8)
				ptr_cSignalRecord = (unsigned char*)util_cBswap_64((char*)ptr_cSignalRecord);
		}
		fwrite(ptr_cSignalRecord, usTailleEnByte, usNbElem, strALLRecordXML->strTabSignalXML[iVarSignal].ptr_fpBin);
		strALLRecordXML->strTabSignalXML[iVarSignal].ptr_vPrevVal = ptr_cSignalRecord;
		ptr_cSignalRecord += usTailleEnByte*usNbElem;
	}
	// Recopie en zone tampon des valeurs courantes.
	memmove(ptrSignal_prevRecord, ptrSignal_Record, sizeof(T_RAWRANGEBEAMANGLEV2_RECORD_SIGNAL));

	// --------------------------------------------
 	// Lecture unitaire des variables de type Image
	for (iLoop=0; iLoop< ptrSignal_Record->NTx; iLoop++)
	{
		// Lecture unitaire des variables.
		for (iVarImage=0; iVarImage< strALLRecordXML->sNbVarImage; iVarImage++)
		{
			usTailleEnByte = strALLRecordXML->strTabImageXML[iVarImage].usSize;
			usNbElem = strALLRecordXML->strTabImageXML[iVarImage].usNbElem;
			ptr_vBidon = (void *)malloc((usTailleEnByte*usNbElem)*sizeof(char));
			iFlag = fread(ptr_vBidon, usTailleEnByte, usNbElem, fpFileData);
			if (G_iFlagEndian == 0) // On inverse en Big Endian.
			{
				if (usTailleEnByte == 2) // Pour des (u)short, (u)int, (u)long.
					ptr_vBidon = (void *)util_cBswap_16(ptr_vBidon);
				else if (usTailleEnByte == 4)
					ptr_vBidon = (void *)util_cBswap_32(ptr_vBidon);
				else if (usTailleEnByte == 8)
					ptr_vBidon = (void *)util_cBswap_64(ptr_vBidon);
			}
			iFlag = fwrite(ptr_vBidon, usTailleEnByte, usNbElem, strALLRecordXML->strTabImageXML[iVarImage].ptr_fpBin);

			// Traitement du caract�re constant de la donn�e - Attention, il peut n'y avoir
			// qu'un record.
			if (iLoop == 0 && iNbRecord == 0)
			{
				strALLRecordXML->strTabImageXML[iVarImage].ptr_vPrevVal = malloc(usTailleEnByte*usNbElem*sizeof(char));
			}
			else if (strALLRecordXML->strTabImageXML[iVarImage].bFlagConstant == TRUE)
			{
				// GLU le 02/11/2011 : correction. Bug observ� sur le 0024_20111018_114606_PingeLine.all
				bFlagCompare = util_bComparePrevAndCurrentVal(strALLRecordXML->strTabImageXML[iVarImage].cType,
													strALLRecordXML->strTabImageXML[iVarImage].ptr_vPrevVal,
													ptr_vBidon,
													usNbElem);
				strALLRecordXML->strTabImageXML[iVarImage].bFlagConstant = bFlagCompare && strALLRecordXML->strTabImageXML[iVarImage].bFlagConstant;
			}
			memmove(strALLRecordXML->strTabImageXML[iVarImage].ptr_vPrevVal, ptr_vBidon, usTailleEnByte*usNbElem);

			free(ptr_vBidon);

		} // Fin de la boucles sur les variables.
	}

	// --------------------------------------------
	// Lecture unitaire des variables de type Image (2i�me image)
	for (iLoop=0; iLoop< ptrSignal_Record->NRx; iLoop++)
	{
		// Lecture unitaire des variables.
		for (iVarImage=0; iVarImage< strALLRecordXML->sNbVarImage2; iVarImage++)
		{
			usTailleEnByte = strALLRecordXML->strTabImage2XML[iVarImage].usSize;
			usNbElem = strALLRecordXML->strTabImage2XML[iVarImage].usNbElem;
			ptr_vBidon = (void *)malloc((usTailleEnByte*usNbElem)*sizeof(char));
			iFlag = fread(ptr_vBidon, usTailleEnByte, usNbElem, fpFileData);
			if (G_iFlagEndian == 0) // On inverse en Big Endian.
			{
				if (usTailleEnByte == 2) // Pour des (u)short, (u)int, (u)long.
					ptr_vBidon = (void *)util_cBswap_16(ptr_vBidon);
				else if (usTailleEnByte == 4)
					ptr_vBidon = (void *)util_cBswap_32(ptr_vBidon);
				else if (usTailleEnByte == 8)
					ptr_vBidon = (void *)util_cBswap_64(ptr_vBidon);
			}
			iFlag = fwrite(ptr_vBidon, usTailleEnByte, usNbElem, strALLRecordXML->strTabImage2XML[iVarImage].ptr_fpBin);
			// Le nombre de nRX peut-�tre = 0 lors du 1er ping et du coup, ne pas autoriser d'allocations.
			if (iNbRecord == 0 || bFlagPrevAlloc == FALSE)
			{
				strALLRecordXML->strTabImage2XML[iVarImage].ptr_vPrevVal = malloc(usTailleEnByte*usNbElem*sizeof(char));
			}
			// Traitement du caract�re Constant.
			else if (strALLRecordXML->strTabImage2XML[iVarImage].bFlagConstant == TRUE)
			{
				bFlagCompare = util_bComparePrevAndCurrentVal(strALLRecordXML->strTabImage2XML[iVarImage].cType,
													strALLRecordXML->strTabImage2XML[iVarImage].ptr_vPrevVal,
													ptr_vBidon,
													usNbElem);
				strALLRecordXML->strTabImage2XML[iVarImage].bFlagConstant = bFlagCompare && strALLRecordXML->strTabImage2XML[iVarImage].bFlagConstant;
			}
			memmove(strALLRecordXML->strTabImage2XML[iVarImage].ptr_vPrevVal, ptr_vBidon, usTailleEnByte*usNbElem);

			free(ptr_vBidon);

		} // Fin de la boucles sur les variables.
		// Marquage du traitement d'initialisation des variables de type Image
		if (iLoop == 0)
		{
			bFlagPrevAlloc = TRUE;
		}
	}

	free(ptrSignal_Record);

	iNbRecord++;

	return iRet;

} //iProcessRawRangeBeamAngleV2Record


//----------------------------------------------------------------
// Fonction :	iInitXMLBinRawRangeBeamAngleV2Record
// Objet 	:	Initialisation de la structure �quivalent au paquet
//				RawRangeBeamAngleV2 pour la manipulation des fichiers bin et XML.
// Modification : 15/10/08
// Auteur 	: GLU
//----------------------------------------------------------------
int iInitXMLBinRawRangeBeamAngleV2Record(	char *cTitleDatagram,
								char *cNomDatagram,
								char *cComments,
								T_ALLRECORDXML *strALLRecordXML)
{

	char cTabNomVarSignal[14][30] = {
			"NbOfBytesInDatagram",
			"STX",
			"TypeOfDatagram",
			"EmModel",
			"Date",
			"NbMilliSecs",
			"PingCounter",
			"SystemSerialNumber",
			"SoundSpeed",
			"NTx",
			"NRx",
			"NbBeams",
			"SamplingRate",
			"Spare"};

	char cTabNomTypeSignal[14][20] = {
			"int",
			"u8",
			"u8",
			"unsigned short",
			"unsigned int",
			"unsigned int",
			"unsigned short",
			"unsigned short",
			"unsigned short",
			"unsigned short",
			"unsigned short",
			"unsigned short",
			"float",
			"unsigned int"};

	// ScaleFactor
	float 	fTabSFSignal[14] = {
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			0.1,	// en m/s
			1.0,
			1.0,
			1.0,
			1.0,
			1.0};

	// AddOffset
	float 	fTabAOSignal[14] = {
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0};

	char cTabNomVarImage[9][30] = {
			"TiltAngle",
			"FocusRange",
			"SignalLength",
			"SectorTransmitDelay",
			"CentralFrequency",
			"MeanAbsorptionCoeff",
			"SignalWaveformIdentifier",
			"TransmitSectorNumberTx",
			"SignalBandwith"};

	char cTabNomTypeImage[9][20] = {
			"short",
			"unsigned short",
			"float",
			"float",
			"float",
			"unsigned short",
			"u8",
			"u8",
			"float"};

	// ScaleFactor
	float 	fTabSFImage[9] = {
			0.01,	// en deg
			0.1,	// en m
			1.0,
			1.0,
			1.0,
			0.01,	// en m/s
			1.0,
			1.0,
			1.0};

	// AddOffset
	float 	fTabAOImage[9] = {
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0};

	char cTabNomVarImage2[10][30] = {
			"BeamPointingAngle",
			"TransmitSectorNumberRx",
			"DetectionInfo",
			"LengthDetection",
			"QualityFactor",
			"Spare1",
			"TwoWayTravelTime",
			"Reflectivity",
			"RealTimeCleaningInfo",
			"Spare2"};

	char cTabNomTypeImage2[10][20] = {
			"short",
			"u8",
			"u8",
			"unsigned short",
			"u8",
			"i8",
			"float",
			"short",
			"i8",
			"u8"};

	// ScaleFactor
	float 	fTabSFImage2[10] = {
			0.01,	// en deg
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			0.1,
			1.0,
			1.0};

	// AddOffset
	float 	fTabAOImage2[10] = {
			0.0,
			1.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0};

	char	*ptr_cNomHeader,
			*ptr_cRepertoire,
			cNomFicPathBin[300];

    int 	iSizeRep,
			iRet,
			iLen,
			iVarSignal,
			iVarImage;

	unsigned short usTaille;

	// Initialisation des structures de pilotage des fichiers
	// XML et Bin .
	strALLRecordXML->sNbVarSignal = 14;
	strALLRecordXML->sNbVarImage = 9;
	strALLRecordXML->sNbVarImage2 = 10;
    strALLRecordXML->strTabSignalXML= (T_ALLVARXML *)malloc(strALLRecordXML->sNbVarSignal*sizeof(T_ALLVARXML));
    strALLRecordXML->strTabImageXML= (T_ALLVARXML *)malloc(strALLRecordXML->sNbVarImage*sizeof(T_ALLVARXML));
    strALLRecordXML->strTabImage2XML= (T_ALLVARXML *)malloc(strALLRecordXML->sNbVarImage2*sizeof(T_ALLVARXML));

	iLen = strlen(cNomDatagram)+1;
	strALLRecordXML->ptr_cNomHeader = malloc(iLen*sizeof(char));
	sprintf(strALLRecordXML->ptr_cNomHeader, "%s", cNomDatagram);

	iLen = strlen(cTitleDatagram)+1;
	strALLRecordXML->ptr_cLabelHeader = malloc(iLen*sizeof(char));
	sprintf(strALLRecordXML->ptr_cLabelHeader, "%s", cTitleDatagram);

	iLen = strlen(cComments)+1;
	strALLRecordXML->ptr_cCommentsHeader = malloc(iLen*sizeof(char));
	sprintf(strALLRecordXML->ptr_cCommentsHeader, "%s", cComments);

	// Remplissage du nom de la variable d'indexation des tables.
	iLen = strlen(cTabNomVarSignal[9])+1;
	strALLRecordXML->ptr_cNomIndexTable = malloc(iLen*sizeof(char));
	sprintf(strALLRecordXML->ptr_cNomIndexTable, "%s", cTabNomVarSignal[9]);

	// Remplissage du nom de la variable d'indexation des tables.
	iLen = strlen(cTabNomVarSignal[10])+1;
	strALLRecordXML->ptr_cNomIndexTable2 = malloc(iLen*sizeof(char));
	sprintf(strALLRecordXML->ptr_cNomIndexTable2, "%s", cTabNomVarSignal[10]);

	// Affectation et cr�ation du repertoire du Datagramme.
	iSizeRep = ( strlen(G_cRepData) + strlen( cNomDatagram));
	// Ajout d'une marge au cas o� certains caract�res soient doubl�s soient doubl�s.
	ptr_cRepertoire = malloc( (iSizeRep + 1 + 30 + 4)*sizeof(char));
	iRet = util_iCreateDirDatagram(G_cRepData,
								strALLRecordXML->ptr_cNomHeader,
								ptr_cRepertoire);

	// Initialisation g�n�rale des variables de type Signal
	for (iVarSignal=0; iVarSignal<strALLRecordXML->sNbVarSignal; iVarSignal++) {
	   strALLRecordXML->strTabSignalXML[iVarSignal].ptr_fpBin = NULL;
	   strALLRecordXML->strTabSignalXML[iVarSignal].bFlagConstant = TRUE;
	   strALLRecordXML->strTabSignalXML[iVarSignal].usNbElem = 1;
	   sprintf(strALLRecordXML->strTabSignalXML[iVarSignal].cNom, "%s", cTabNomVarSignal[iVarSignal]);
	   sprintf(strALLRecordXML->strTabSignalXML[iVarSignal].cType, "%s", cTabNomTypeSignal[iVarSignal]);
	   sprintf(strALLRecordXML->strTabSignalXML[iVarSignal].cTag, "%s", "TODO");
	   sprintf(strALLRecordXML->strTabSignalXML[iVarSignal].cUnit, "%s", "TODO");
	   strALLRecordXML->strTabSignalXML[iVarSignal].fScaleFactor = fTabSFSignal[iVarSignal];
	   strALLRecordXML->strTabSignalXML[iVarSignal].fAddOffset = fTabAOSignal[iVarSignal];
	   usTaille = util_usNbOctetTypeVar(cTabNomTypeSignal[iVarSignal]);
	   strALLRecordXML->strTabSignalXML[iVarSignal].usSize = usTaille;
	   iLen = strlen(strALLRecordXML->strTabSignalXML[iVarSignal].cNom);
	   // Ajout d'une marge au cas o� certains caract�res soient doubl�s soient doubl�s.
	   sprintf(cNomFicPathBin, "%s\\%s%s", ptr_cRepertoire, strALLRecordXML->strTabSignalXML[iVarSignal].cNom, ".bin");
	   sprintf(strALLRecordXML->strTabSignalXML[iVarSignal].cNomPathFileBin, "%s", cNomFicPathBin);
	   strALLRecordXML->strTabSignalXML[iVarSignal].ptr_fpBin = fopen(cNomFicPathBin, "w+b");
	   if (!strALLRecordXML->strTabSignalXML[iVarSignal].ptr_fpBin)
	   {
		   printf("%s -- Error in Signal creation  : %s\n", __FILE__, cNomFicPathBin);
		   return EXIT_FAILURE;
	   }
	}

	// Initialisation g�n�rale des variables de type Image
	for (iVarImage=0; iVarImage<strALLRecordXML->sNbVarImage; iVarImage++) {
	   strALLRecordXML->strTabImageXML[iVarImage].ptr_fpBin = NULL;
	   strALLRecordXML->strTabImageXML[iVarImage].bFlagConstant = TRUE;
	   strALLRecordXML->strTabImageXML[iVarImage].usNbElem = 1;
	   sprintf(strALLRecordXML->strTabImageXML[iVarImage].cNom, "%s", cTabNomVarImage[iVarImage]);
	   sprintf(strALLRecordXML->strTabImageXML[iVarImage].cType, "%s", cTabNomTypeImage[iVarImage]);
	   sprintf(strALLRecordXML->strTabImageXML[iVarImage].cTag, "%s", "TODO");
	   sprintf(strALLRecordXML->strTabImageXML[iVarImage].cUnit, "%s", "TODO");
	   strALLRecordXML->strTabImageXML[iVarImage].fScaleFactor = fTabSFImage[iVarImage];
	   strALLRecordXML->strTabImageXML[iVarImage].fAddOffset = fTabAOImage[iVarImage];
	   usTaille = util_usNbOctetTypeVar(cTabNomTypeImage[iVarImage]);
	   strALLRecordXML->strTabImageXML[iVarImage].usSize = usTaille;
	   iLen = strlen(strALLRecordXML->strTabImageXML[iVarImage].cNom);
	   // Ajout d'une marge au cas o� certains caract�res soient doubl�s soient doubl�s.
	   sprintf(cNomFicPathBin, "%s\\%s%s", ptr_cRepertoire, strALLRecordXML->strTabImageXML[iVarImage].cNom, ".bin");
	   sprintf(strALLRecordXML->strTabImageXML[iVarImage].cNomPathFileBin, "%s", cNomFicPathBin);
	   strALLRecordXML->strTabImageXML[iVarImage].ptr_fpBin = fopen(cNomFicPathBin, "w+b");
	   if (!strALLRecordXML->strTabImageXML[iVarImage].ptr_fpBin)
	   {
		   printf("%s -- Error in Image creation  : %s\n", __FILE__, cNomFicPathBin);
		   return EXIT_FAILURE;
	   }
	}

	// Initialisation g�n�rale des variables de type Image pour la 2i�me image
	for (iVarImage=0; iVarImage<strALLRecordXML->sNbVarImage2; iVarImage++) {
	   strALLRecordXML->strTabImage2XML[iVarImage].ptr_fpBin = NULL;
	   strALLRecordXML->strTabImage2XML[iVarImage].bFlagConstant = TRUE;
	   strALLRecordXML->strTabImage2XML[iVarImage].usNbElem = 1;
	   sprintf(strALLRecordXML->strTabImage2XML[iVarImage].cNom, "%s", cTabNomVarImage2[iVarImage]);
	   sprintf(strALLRecordXML->strTabImage2XML[iVarImage].cType, "%s", cTabNomTypeImage2[iVarImage]);
	   sprintf(strALLRecordXML->strTabImage2XML[iVarImage].cTag, "%s", "TODO");
	   sprintf(strALLRecordXML->strTabImage2XML[iVarImage].cUnit, "%s", "TODO");
	   strALLRecordXML->strTabImage2XML[iVarImage].fScaleFactor = fTabSFImage2[iVarImage];
	   strALLRecordXML->strTabImage2XML[iVarImage].fAddOffset = fTabAOImage2[iVarImage];
	   usTaille = util_usNbOctetTypeVar(cTabNomTypeImage2[iVarImage]);
	   strALLRecordXML->strTabImage2XML[iVarImage].usSize = usTaille;
	   iLen = strlen(strALLRecordXML->strTabImage2XML[iVarImage].cNom);
	   // Ajout d'une marge au cas o� certains caract�res soient doubl�s soient doubl�s.
	   sprintf(cNomFicPathBin, "%s\\%s%s", ptr_cRepertoire, strALLRecordXML->strTabImage2XML[iVarImage].cNom, ".bin");
	   sprintf(strALLRecordXML->strTabImage2XML[iVarImage].cNomPathFileBin, "%s", cNomFicPathBin);
	   strALLRecordXML->strTabImage2XML[iVarImage].ptr_fpBin = fopen(cNomFicPathBin, "w+b");
	   if (!strALLRecordXML->strTabImage2XML[iVarImage].ptr_fpBin)
	   {
		   printf("%s -- Error in Image creation  : %s\n", __FILE__, cNomFicPathBin);
		   return EXIT_FAILURE;
	   }
	}


	free(ptr_cRepertoire);
	return EXIT_SUCCESS;

} // iInitXMLBinRawRangeBeamAngleV2Record

#pragma pack()
