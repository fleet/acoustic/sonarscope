/*
 * ALL_processRawRangeBeamAngleV2Record.h
 *
 *  Created on: 21 nov. 2008
 *      Author: rgallou
 */
#pragma pack(1)

#ifndef ALL_PROCESSRAWRANGEBEAMANGLEV2RECORD_H_
#define ALL_PROCESSRAWRANGEBEAMANGLEV2RECORD_H_

#include "ALL_DATAGRAMS.H"


typedef struct  {
		int 			NbOfBytesInDatagram;
		unsigned char	STX;
		unsigned char	TypeOfDatagram;
		unsigned short	EmModel;
		unsigned int	Date;
		unsigned int	NbMilliSecs;
		unsigned short	RawRangeBeamAngleV2Counter;
		unsigned short	SystemSerialNumber;
		unsigned short	SoundSpeed;
		unsigned short	NTx;
		unsigned short	NRx;
		unsigned short	NbBeams;
		float			SamplingRate;
		unsigned int	Spare;
} T_RAWRANGEBEAMANGLEV2_RECORD_SIGNAL;

typedef struct  {
		short			*TiltAngle;
		unsigned short	*FocusRange;
		float			*SignalLength;
		float			*SectorTransmitDelay;
		float			*CentralFrequency;
		unsigned short	*MeanAbsorptionCoeff;
		unsigned char	*SignalWaveformIdentifier;
		unsigned char	*TransmitSectorNumberTx;
		float			*SignalBandwith;
} T_RAWRANGEBEAMANGLEV2_RECORD_IMAGE;

typedef struct  {
		short			*BeamPointingAngle;
		unsigned char	*TransmitSectorNumberRx;
		unsigned char	*DetectionInfo;
		unsigned short	*LengthDetection;
		unsigned char	*QualityFactor;
		char			*Spare1;
		float			*TwoWayTravelTime;
		short			*Reflectivity;
		char			*RealTimeCleaningInfo;
		unsigned char	*Spare2;
} T_RAWRANGEBEAMANGLEV2_RECORD_IMAGE2;


int iProcessRawRangeBeamAngleV2Record(FILE *fpFileData, int iNbRecord,
						T_ALLRECORDXML *strALLRecordXML,
						T_RAWRANGEBEAMANGLEV2_RECORD_SIGNAL *ptrSignal_prevRecord);


int iInitXMLBinRawRangeBeamAngleV2Record(	char *cTitleDatagram,
								char *cNomDatagram, char *cComments,
								T_ALLRECORDXML *strALLRecordXML);

#endif /* ALL_PROCESSRAWRANGEBEAMANGLEV2RECORD_H_ */

#pragma pack()
