/*
 * ALL_processRuntimeRecord.h
 *
 *  Created on: 21 nov. 2008
 *      Author: rgallou
 */
#pragma pack(1)

#ifndef ALL_PROCESSRuntimeRECORD_H_
#define ALL_PROCESSRuntimeRECORD_H_

#include "ALL_DATAGRAMS.H"

typedef struct  {
		int 			NbOfBytesInDatagram;
		unsigned char	STX;
		unsigned char	TypeOfDatagram;
		unsigned short	EmModel;
		unsigned int	Date;
		unsigned int	NbMilliSecs;
		unsigned short	PingCounter;
		unsigned short	SystemSerialNumber;
		unsigned char	OperatorStationStatus;
		unsigned char	ProcessingUnitStatus;
		unsigned char	BSPStatus;
		unsigned char	SonarHeadStatus;
		unsigned char	Mode;
		unsigned char	FilterIdentifier;
		unsigned short	MinimumDepth;
		unsigned short	MaximumDepth;
		unsigned short	AbsorptionCoeff;
		unsigned short	TransmitPulseLength;
		unsigned short	TransmitBandWidth;
		char			TransmitPowerMax;
		unsigned char	ReceiveBandWidthIn01Deg;
		unsigned char	ReceiveBandWidthIn50Hz;
		unsigned char	ReceiverFixGain;
		unsigned char	TVGCrossAngle;
		unsigned char	SourceSoundSpeed;
		unsigned short	MaximumPortSwath;
		unsigned char	BeamSpacing;
		unsigned char	MaximumPortCoverage;
		unsigned char	YawAndPitchStabMode;
		unsigned char	MaximumStarboardCoverage;
		unsigned short	MaximumStarboardSwath;
		unsigned short	DurotongSpeed;
		unsigned char	HiLoFreqAbsCoefRatio;

} T_RUNTIME_RECORD_SIGNAL;


int iProcessRuntimeRecord(FILE *fpFileData, int iNbRecord,
						T_ALLRECORDXML *strALLRecordXML,
						T_RUNTIME_RECORD_SIGNAL *ptrSignal_prevRecord);


int iInitXMLBinRuntimeRecord(	char *cTitleDatagram,
								char *cNomDatagram,
								char *cComments,
								T_ALLRECORDXML *strALLRecordXML);

#endif /* ALL_PROCESSRUNTIMERECORD_H_ */

#pragma pack(1)
