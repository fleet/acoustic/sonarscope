/*
 * ALL_processSeabedV1Record.h
 *
 *  Created on: 21 nov. 2008
 *      Author: rgallou
 */
#pragma pack(1)

#ifndef ALL_PROCESSSEABEDV1RECORD_H_
#define ALL_PROCESSSEABEDV1RECORD_H_

#include "ALL_DATAGRAMS.H"


typedef struct  {
	int 			NbOfBytesInDatagram;
	unsigned char	STX;
	unsigned char	TypeOfDatagram;
	unsigned short	EmModel;
	unsigned int	Date;
	unsigned int	NbMilliSecs;
	unsigned short	PingCounter;
	unsigned short	SystemSerialNumber;
	unsigned short	MeanAbsorpCoeff;
	unsigned short	PulseLength;
	unsigned short	TVGN;
	unsigned short	TVGStart;
	unsigned short	TVGStop;
	char			BSN;
	char			BSO;
	unsigned short	TxBeamWidth;
	unsigned char	TVGCrossOver;
	unsigned char	NbBeams;
} T_SEABEDV1_RECORD_SIGNAL;


typedef struct  {
		unsigned char	*IndexNumberBeam;
		char			*InfoBeamSortDirection;
		unsigned short	*InfoBeamNbSamples;
		unsigned short	*InfoBeamCentralSample;
		char			*Entries;
} T_SEABEDV1_RECORD_IMAGE;

int iProcessSeabedV1Record(FILE *fpFileData, int iNbRecord,
						T_ALLRECORDXML *strALLRecordXML,
						T_SEABEDV1_RECORD_SIGNAL *ptrSignal_prevRecord);


int iInitXMLBinSeabedV1Record(	char *cTitleDatagram,
								char *cNomDatagram, char *cComments,
								T_ALLRECORDXML *strALLRecordXML);

#endif /* ALL_PROCESSSEABEDV1_V1RECORD_H_ */

#pragma pack()
