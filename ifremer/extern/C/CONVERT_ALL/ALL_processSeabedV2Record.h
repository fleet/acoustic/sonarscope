/*
 * ALL_processSeabedV2Record.h
 *
 *  Created on: 21 nov. 2008
 *      Author: rgallou
 */
#pragma pack(1)

#ifndef ALL_PROCESSSEABEDV2RECORD_H_
#define ALL_PROCESSSEABEDV2RECORD_H_

#include "ALL_DATAGRAMS.H"


typedef struct  {
	int 			NbOfBytesInDatagram;
	unsigned char	STX;
	unsigned char	TypeOfDatagram;
	unsigned short	EmModel;
	unsigned int	Date;
	unsigned int	NbMilliSecs;
	unsigned short	PingCounter;
	unsigned short	SystemSerialNumber;
	float			SamplingRate;
	unsigned short	TVGN;
	short			BSN;
	short			BSO;
	unsigned short	TxBeamWidth;
	unsigned short	TVGCrossOver;
	unsigned short	NbBeams;
} T_SEABEDV2_RECORD_SIGNAL;


typedef struct  {
		char			*InfoBeamSortDirection;
		unsigned char	*InfoBeamDetectionInfo;
		unsigned short	*InfoBeamNbSamples;
		unsigned short	*InfoBeamCentralSample;
		short			*Entries;
} T_SEABEDV2_RECORD_IMAGE;

int iProcessSeabedV2Record(FILE *fpFileData, int iNbRecord,
						T_ALLRECORDXML *strALLRecordXML,
						T_SEABEDV2_RECORD_SIGNAL *ptrSignal_prevRecord);


int iInitXMLBinSeabedV2Record(	char *cTitleDatagram,
								char *cNomDatagram, char *cComments,
								T_ALLRECORDXML *strALLRecordXML);

#endif /* ALL_PROCESSSEABEDV2_V2RECORD_H_ */

#pragma pack()
