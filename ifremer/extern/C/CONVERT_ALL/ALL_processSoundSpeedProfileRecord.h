/*
 * ALL_processSoundSpeedProfileRecord.h
 *
 *  Created on: 21 nov. 2008
 *      Author: rgallou
 */
#pragma pack(1)

#ifndef ALL_PROCESSSOUNDSPEEDPROFILERECORD_H_
#define ALL_PROCESSSOUNDSPEEDPROFILERECORD_H_

#include "ALL_DATAGRAMS.H"


typedef struct  {
		int 			NbOfBytesInDatagram;
		unsigned char	STX;
		unsigned char	TypeOfDatagram;
		unsigned short	EmModel;
		unsigned int	Date;
		unsigned int	NbMilliSecs;
		unsigned short	SoundSpeedProfileCounter;
		unsigned short	SystemSerialNumber;
		unsigned int	DateProfile;
		unsigned int	NbMilliSecsProfile;
		unsigned short	NbEntries;
		unsigned short	DepthResolution;
} T_SOUNDSPEEDPROFILE_RECORD_SIGNAL;

typedef struct  {
		unsigned int	*Depth;
		unsigned int	*SoundSpeed;
} T_SOUNDSPEEDPROFILE_RECORD_IMAGE;

int iProcessSoundSpeedProfileRecord(FILE *fpFileData, int iNbRecord,
						T_ALLRECORDXML *strALLRecordXML,
						T_SOUNDSPEEDPROFILE_RECORD_SIGNAL *ptrSignal_prevRecord);


int iInitXMLBinSoundSpeedProfileRecord(	char *cTitleDatagram,
								char *cNomDatagram, char *cComments,
								T_ALLRECORDXML *strALLRecordXML);

#endif /* ALL_PROCESSSOUNDSPEEDPROFILERECORD_H_ */

#pragma pack()
