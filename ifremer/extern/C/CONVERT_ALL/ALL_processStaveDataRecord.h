/*
 * ALL_processStaveDataRecord.h
 *
 *  Created on: 27/01/2010
 *      Author: rgallou
 */
#pragma pack(1)

#ifndef ALL_PROCESSSTAVEDATARECORD_H_
#define ALL_PROCESSSTAVEDATARECORD_H_

#include "ALL_DATAGRAMS.H"


typedef struct  {
		unsigned int 	NbOfBytesInDatagram;
		unsigned char	STX;
		unsigned char	TypeOfDatagram;
		unsigned short	EmModel;
		unsigned int	Date;
		unsigned int	NbMilliSecs;
		unsigned short	PingCounter;
		unsigned short	SystemSerialNumber;
		unsigned short	NumberOfDatagrams;
		unsigned short	DatagramNumbers;
		unsigned int	RxSamplingFreq;
		unsigned short	SoundSpeed;
		unsigned short	StartSampleRef1TxPulse;
		unsigned short	TotalSamplesPerStave;
		unsigned short	NbSamples;			//Ns
		unsigned short	StaveNumber;
		unsigned short	NbStavesPerHead;	//Ne
		unsigned short	RangeToNormalIncidence;
		unsigned short	Spare;
} T_STAVEDATA_RECORD_SIGNAL;

typedef struct  {
		short			*SampleNumber;
		unsigned short	*TVGGain;
		char	*StaveBackscatter;
} T_STAVEDATA_RECORD_IMAGE;


int iProcessStaveDataRecord(FILE *fpFileData,
							int iNbRecord,
							T_ALLRECORDXML *strALLRecordXML,
							T_STAVEDATA_RECORD_SIGNAL *ptrSignal_prevRecord);


int iInitXMLBinStaveDataRecord(	char *cTitleDatagram,
										char *cNomDatagram, char *cComments,
										T_ALLRECORDXML *strALLRecordXML);

#endif /* ALL_PROCESSSTAVEDATARECORD_H_ */

#pragma pack()
