/*
 * ALL_processSurfaceSoundSpeedRecord.h
 *
 *  Created on: 21 nov. 2008
 *      Author: rgallou
 */
#pragma pack(1)

#ifndef ALL_PROCESSSURFACESOUNDSPEEDRECORD_H_
#define ALL_PROCESSSURFACESOUNDSPEEDRECORD_H_

#include "ALL_DATAGRAMS.H"


typedef struct  {
		int 			NbOfBytesInDatagram;
		unsigned char	STX;
		unsigned char	TypeOfDatagram;
		unsigned short	EmModel;
		unsigned int	Date;
		unsigned int	NbMilliSecs;
		unsigned short	SoundSpeedCounter;
		unsigned short	SystemSerialNumber;
		unsigned short	NbEntries;
} T_SURFACESOUNDSPEED_RECORD_SIGNAL;

typedef struct  {
		unsigned short	*StartNbMilliSecs;
		unsigned short	*SurfaceSoundSpeed;
} T_SURFACESOUNDSPEED_RECORD_IMAGE;

int iProcessSurfaceSoundSpeedRecord(FILE *fpFileData, int iNbRecord,
						T_ALLRECORDXML *strALLRecordXML,
						T_SURFACESOUNDSPEED_RECORD_SIGNAL *ptrSignal_prevRecord);


int iInitXMLBinSurfaceSoundSpeedRecord(	char *cTitleDatagram,
								char *cNomDatagram, char *cComments,
								T_ALLRECORDXML *strALLRecordXML);

#endif /* ALL_PROCESSSURFACESOUNDSPEEDRECORD_H_ */

#pragma pack()
