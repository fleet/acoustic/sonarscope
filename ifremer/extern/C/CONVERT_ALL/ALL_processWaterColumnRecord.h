/*
 * ALL_processWaterColumnRecord.h
 *
 *  Created on: 21 nov. 2008
 *      Author: rgallou
 */
#pragma pack(1)

#ifndef ALL_PROCESSWATERCOLUMNRECORD_H_
#define ALL_PROCESSWATERCOLUMNRECORD_H_

#include "ALL_DATAGRAMS.H"


typedef struct  {
		unsigned int 	NbOfBytesInDatagram;
		unsigned char	STX;
		unsigned char	TypeOfDatagram;
		unsigned short	EmModel;
		unsigned int	Date;
		unsigned int	NbMilliSecs;
		unsigned short	PingCounter;
		unsigned short	SystemSerialNumber;
		unsigned short	NumberOfDatagrams;
		unsigned short	DatagramNumbers;
		unsigned short	NTx;
		unsigned short	TotalOfReceiveBeams;
		unsigned short	NRx;
		unsigned short	SoundSpeed;
		unsigned int	SamplingFreq;
		short			TxTimeHeave;
		unsigned char	TVGFunctionApplied;
		char			TVGOffSet;
		unsigned int	Spare1;
} T_WATERCOLUMN_RECORD_SIGNAL;

typedef struct  {
		short			*TiltAngle;
		unsigned short	*CenterFrequency;
		unsigned char	*TransmitSectorNumber;
		unsigned char	*Spare2;
} T_WATERCOLUMN_RECORD_IMAGE;


typedef struct  {
		short			*BeamPointingAngle;
		unsigned short	*StartRangeNumber;
		unsigned short	*NumberOfSamples;
		unsigned short	*DetectedRangeInSamples;
		unsigned char	*TransmitSectorNumber;
		unsigned char	*RxBeamNumber;
		char			*SampleAmplitude;
} T_WATERCOLUMN_RECORD_IMAGE2;


int iProcessWaterColumnRecord(FILE *fpFileData, int iNbRecord,
						T_ALLRECORDXML *strALLRecordXML,
						T_WATERCOLUMN_RECORD_SIGNAL *ptrSignal_prevRecord);


int iInitXMLBinWaterColumnRecord(	char *cTitleDatagram,
								char *cNomDatagram, char *cComments,
								T_ALLRECORDXML *strALLRecordXML);

#endif /* ALL_PROCESSWATERCOLUMNRECORD_H_ */

#pragma pack()
