// ----------------------------------------------------------------------
// Programme :
//	CONVERT_ALL.C
//
// Objet :
//	Convertir un fichier au format ALL pour d�composer en fichiers
//	XML et fichiers binaires. Les fichiers XML decrivent le contenu des
// 	binaires.
// 	Chaque fichier binaire comporte une donn�e �l�mentaire des paquets par ping
// 	issue des paquets de donn�es du ALL.
//	Il a ete realise sous l'IDE Eclipse 3.4.1 avec compilateur gcc.
//
// Cr�ation :
//	02/10/2008
// Modif. :
//	13/03/2013
//
// Auteur :
//  GLU : (ATL) roger.gallou@altran.com
//		  02 98 05 43 21
//
// Historique des modifications :
// - v.Main
//
// ----------------------------------------------------------------------
#pragma pack(1) // Pour la compilation sous Visual Studio
				// et l'alignement sur 8 octets.

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <errno.h>
#include <libgen.h>
#include <signal.h>

#include "ALL_utilities.h"
#include "ALL_Datagrams.h"
#include "convertFiles_ALL.h"
#include "ALL_Datagrams.h"
#include "ALL_processPositionRecord.h"
#include "ALL_processHeightRecord.h"
#include "ALL_processRuntimeRecord.h"
#include "ALL_processAttitudeRecord.h"
#include "ALL_processInstallParamsRecord.h"
#include "ALL_processSeabedV2Record.h"
#include "ALL_processSeabedV1Record.h"
#include "ALL_processClockRecord.h"
#include "ALL_processSoundSpeedProfileRecord.h"
#include "ALL_processDepthV1Record.h"
#include "ALL_processDepthV2Record.h"
#include "ALL_processRawRangeBeamAngleV0Record.h"
#include "ALL_processRawRangeBeamAngleV1Record.h"
#include "ALL_processRawRangeBeamAngleV2Record.h"
#include "ALL_processHeadingRecord.h"
#include "ALL_processSurfaceSoundSpeedRecord.h"
#include "ALL_processMechaTransducerTiltRecord.h"
#include "ALL_processWaterColumnRecord.h"
#include "ALL_processQualityFactorRecord.h"
#include "ALL_processQualityFactorFromIfrRecord.h"
#include "ALL_processRawBeamSamplesRecord.h"
#include "ALL_processNetworkAttitudeRecord.h"
#include "ALL_processStaveDataRecord.h"
#include "ALL_processExtraParametersRecord.h"

#include "ALL_processFileIndexRecord.h"

#include "Generic_Utilities.h"    // Contenu dans la librairie libCONVERT_utils.a


// Fonction de d�claration de la fonction principale de conversion.
// Param�tres :
// - cFileName : r�pertoire et nom du fichier de donn�es
// - barre de progression optionnelle dans l'appel de fonction.
int iConvertFiles_ALL(const char *cFileName, void(*progress)(int))
{
	int 	iRet = 0,
			iLoop,
			iFlagTrouve = FALSE,
			iNbDatagramNotTranslate = 0;

	// Adressage d'un fichier potentiellement sup. � 2 Go.
	off64_t 	llPosFic,
				llNbOcFic,
				llPositionDebDatagram = 0;

	FILE *fpFileData;


	T_TYPEOFDATAGRAMNOTTRANSLATE tabStrDatagramNotTranslate[30];

	T_POS_RECORD_SIGNAL 					strSignalPositionRecord;
	T_HEIGHT_RECORD_SIGNAL 					strSignalHeightRecord;
	T_RUNTIME_RECORD_SIGNAL 				strSignalRuntimeRecord;
	T_ATTITUDE_RECORD_SIGNAL 				strSignalAttitudeRecord;
	T_INSTALLPARAMS_RECORD_SIGNAL 			strSignalInstallParamsRecord;
	T_SEABEDV1_RECORD_SIGNAL 				strSignalSeabedV1Record;
	T_SEABEDV2_RECORD_SIGNAL 				strSignalSeabedV2Record;
	T_CLOCK_RECORD_SIGNAL 					strSignalClockRecord;
	T_SOUNDSPEEDPROFILE_RECORD_SIGNAL		strSignalSoundSpeedProfileRecord;
	T_DEPTHV1_RECORD_SIGNAL					strSignalDepthV1Record;
	T_DEPTHV2_RECORD_SIGNAL					strSignalDepthV2Record;
	T_RAWRANGEBEAMANGLEV0_RECORD_SIGNAL		strSignalRawRangeBeamAngleV0Record;
	T_RAWRANGEBEAMANGLEV1_RECORD_SIGNAL		strSignalRawRangeBeamAngleV1Record;
	T_RAWRANGEBEAMANGLEV2_RECORD_SIGNAL		strSignalRawRangeBeamAngleV2Record;
	T_HEADING_RECORD_SIGNAL					strSignalHeadingRecord;
	T_SURFACESOUNDSPEED_RECORD_SIGNAL		strSignalSurfaceSoundSpeedRecord;
	T_MECHATRANSDUCERTILT_RECORD_SIGNAL 	strSignalMechaTransducerTiltRecord;
	T_WATERCOLUMN_RECORD_SIGNAL 			strSignalWaterColumnRecord;
	T_QUALITYFACTOR_RECORD_SIGNAL 			strSignalQualityFactorRecord;
	T_QUALITYFACTORFROMIFR_RECORD_SIGNAL 	strSignalQualityFactorFromIfrRecord;
	T_FILEINDEX_RECORD_SIGNAL 				strSignalFileIndexRecord;
	T_RAWBEAMSAMPLES_RECORD_SIGNAL 			strSignalRawBeamSamplesRecord;
	T_NETWORKATTTIUDE_RECORD_SIGNAL 		strSignalNetworkAttitudeRecord;
	T_STAVEDATA_RECORD_SIGNAL 				strSignalStaveDataRecord;
	T_EXTRAPARAMETERS_RECORD_SIGNAL 		strSignalExtraParametersRecord;

	T_ALLRECORDXML	strPositionRecordXML;
	T_ALLRECORDXML 	strHeightRecordXML;
	T_ALLRECORDXML 	strRuntimeRecordXML;
	T_ALLRECORDXML 	strAttitudeRecordXML;
	T_ALLRECORDXML 	strInstallParamsRecordXML;
	T_ALLRECORDXML 	strSeabedV1RecordXML;
	T_ALLRECORDXML 	strSeabedV2RecordXML;
	T_ALLRECORDXML 	strClockRecordXML;
	T_ALLRECORDXML	strSoundSpeedProfileRecordXML;
	T_ALLRECORDXML 	strDepthV1RecordXML;
	T_ALLRECORDXML 	strDepthV2RecordXML;
	T_ALLRECORDXML 	strRawRangeBeamAngleV0RecordXML;
	T_ALLRECORDXML 	strRawRangeBeamAngleV1RecordXML;
	T_ALLRECORDXML 	strRawRangeBeamAngleV2RecordXML;
	T_ALLRECORDXML 	strHeadingRecordXML;
	T_ALLRECORDXML 	strSurfaceSoundSpeedRecordXML;
	T_ALLRECORDXML 	strMechaTransducerTiltRecordXML;
	T_ALLRECORDXML 	strQualityFactorRecordXML;
	T_ALLRECORDXML 	strQualityFactorFromIfrRecordXML;
	T_ALLRECORDXML 	strWaterColumnRecordXML;
	T_ALLRECORDXML 	strRawBeamSamplesRecordXML;
	T_ALLRECORDXML 	strNetworkAttitudeRecordXML;
	T_ALLRECORDXML 	strStaveDataRecordXML;
	T_ALLRECORDXML 	strExtraParametersRecordXML;
	T_ALLRECORDXML 	strFileIndexRecordXML;


	strHeightRecordXML.iNbDatagram					= 0;
	strRuntimeRecordXML.iNbDatagram					= 0;
	strPositionRecordXML.iNbDatagram				= 0;
	strAttitudeRecordXML.iNbDatagram				= 0;
	strInstallParamsRecordXML.iNbDatagram			= 0;
	strSeabedV1RecordXML.iNbDatagram				= 0;
	strSeabedV2RecordXML.iNbDatagram				= 0;
	strClockRecordXML.iNbDatagram					= 0;
	strSoundSpeedProfileRecordXML.iNbDatagram		= 0;
	strDepthV1RecordXML.iNbDatagram					= 0;
	strDepthV2RecordXML.iNbDatagram					= 0;
	strRawRangeBeamAngleV0RecordXML.iNbDatagram		= 0;
	strRawRangeBeamAngleV1RecordXML.iNbDatagram		= 0;
	strRawRangeBeamAngleV2RecordXML.iNbDatagram		= 0;
	strHeadingRecordXML.iNbDatagram					= 0;
	strSurfaceSoundSpeedRecordXML.iNbDatagram		= 0;
	strMechaTransducerTiltRecordXML.iNbDatagram		= 0;
	strWaterColumnRecordXML.iNbDatagram				= 0;
	strQualityFactorRecordXML.iNbDatagram			= 0;
	strQualityFactorFromIfrRecordXML.iNbDatagram	= 0;
	strRawBeamSamplesRecordXML.iNbDatagram			= 0;
	strNetworkAttitudeRecordXML.iNbDatagram			= 0;
	strStaveDataRecordXML.iNbDatagram				= 0;
	strExtraParametersRecordXML.iNbDatagram			= 0;
	strFileIndexRecordXML.iNbDatagram 				= 0;

	// Initialisation des datagrammes d�j� trouv�s.
	for (iLoop=0; iLoop<30; iLoop++)
	{
		tabStrDatagramNotTranslate[iLoop].cType = 0;
		tabStrDatagramNotTranslate[iLoop].iNbTimes = 0;
	}

	// Ouverture du fichier All
	fpFileData = fopen64(cFileName,"rb");
	if(fpFileData  == NULL)
	{
		printf("\nProblem in access Data file : %s\n", cFileName);
		goto ERROR_OUT;

	}

    // Calcul de la taille du fichier.
	fseeko64(fpFileData, 0, SEEK_END); //On se positionne � la fin du fichier
    llNbOcFic=ftello64(fpFileData);
    fseeko64(fpFileData, 0, SEEK_SET);
    llPosFic = 0;

    llPositionDebDatagram = 0;

    // For�age de la sortie des impressions : stdout iunbuffered
    setvbuf (stdout, NULL, _IONBF, 0);

    //Deroulement entier du fichier .ALL
	while(!feof(fpFileData) && llPositionDebDatagram < llNbOcFic)
	{
	    llPosFic = ftello64(fpFileData);

	    // Affichage de la progression dans la fen�tre d'ex�cution
#ifdef GUI
	    if(progress)
	    {
	    	progress(100*(double)llPosFic/(double)llNbOcFic);
	    }
#endif
	    util_vProgressBar(0, llPosFic, llNbOcFic);

//	    if (strFileIndexRecordXML.iNbDatagram > 10000)
//	    {
//	    	raise(SIGINT);
//		}

	    fseeko64(fpFileData,llPositionDebDatagram,SEEK_SET);

		// Lecture du paquet de donn�es commun aux datagrammes.
		iRet = iProcessFileIndexRecord(fpFileData,
								strFileIndexRecordXML.iNbDatagram,
								&strFileIndexRecordXML,
								&strSignalFileIndexRecord);
		strFileIndexRecordXML.iNbDatagram++;

		// Sortie du processus si le d�codage du fichier devient chaotique.
		// Cas des fichiers de l'AUV (2040), rempli de 0 � 1/5 du fichier
		if (strSignalFileIndexRecord.NbOfBytesInDatagram ==0 &&
			strSignalFileIndexRecord.TypeOfDatagram ==0 &&
			strSignalFileIndexRecord.EmModel ==0)
			break;


		// On revient en arri�re de la lecture du paquet commun
		// car celui-ci est incorpor� � chaque paquet.
		llPosFic = ftello64(fpFileData);
		// Datagramme Position et Length ne sont pas lues mais juste int�gr�es �
		// la structure T_FILEINDEX_RECORD_SIGNAL.
		llPosFic = llPosFic - (sizeof(T_FILEINDEX_RECORD_SIGNAL)-4);
		fseeko64(fpFileData,llPosFic,SEEK_SET);
		llPosFic = ftello64(fpFileData);
		T_FILEINDEX_RECORD_SIGNAL strSignalFileIndexRecord0 =
				strSignalFileIndexRecord;
		// Traitement au cas par cas des datagrammes.
		if (strSignalFileIndexRecord.TypeOfDatagram == 'h') // Height
		{

 		    iRet = iProcessHeightRecord(fpFileData,
									strHeightRecordXML.iNbDatagram,
									&strHeightRecordXML,
									&strSignalHeightRecord);
		    strHeightRecordXML.iNbDatagram++;
		}
		else if(strSignalFileIndexRecord.TypeOfDatagram == 'P') // Position
		{
		    iRet = iProcessPositionRecord(fpFileData,
									strPositionRecordXML.iNbDatagram,
									&strPositionRecordXML,
									&strSignalPositionRecord);
		    strPositionRecordXML.iNbDatagram++;
		}
		else if(strSignalFileIndexRecord.TypeOfDatagram == 'R') //RunTime
		{
		    iProcessRuntimeRecord(fpFileData,
									strRuntimeRecordXML.iNbDatagram,
									&strRuntimeRecordXML,
									&strSignalRuntimeRecord);
		    strRuntimeRecordXML.iNbDatagram++;
		}
		else if(strSignalFileIndexRecord.TypeOfDatagram == 'A') // Attitude
		{
  		    iProcessAttitudeRecord(fpFileData,
									strAttitudeRecordXML.iNbDatagram,
									&strAttitudeRecordXML,
									&strSignalAttitudeRecord);
		    strAttitudeRecordXML.iNbDatagram++;
		}
		// Installation Parameters Start/p/Stop
		else if (	strSignalFileIndexRecord.TypeOfDatagram == 'I' ||
					strSignalFileIndexRecord.TypeOfDatagram == 'p' ||
					strSignalFileIndexRecord.TypeOfDatagram == 'i')
		{
		    iProcessInstallParamsRecord(fpFileData,
									strInstallParamsRecordXML.iNbDatagram,
									&strInstallParamsRecordXML,
									&strSignalInstallParamsRecord);
		    strInstallParamsRecordXML.iNbDatagram++;
		}
		else if(strSignalFileIndexRecord.TypeOfDatagram == 'S') // Seabed Image Data
		{
		    iProcessSeabedV1Record(fpFileData,
									strSeabedV1RecordXML.iNbDatagram,
									&strSeabedV1RecordXML,
									&strSignalSeabedV1Record);
		    strSeabedV1RecordXML.iNbDatagram++;
		}
		else if(strSignalFileIndexRecord.TypeOfDatagram == 'Y')	// Seabed Image Data 89
		{
		    iProcessSeabedV2Record(fpFileData,
									strSeabedV2RecordXML.iNbDatagram,
									&strSeabedV2RecordXML,
									&strSignalSeabedV2Record);
		    strSeabedV2RecordXML.iNbDatagram++;
		}
		else if(strSignalFileIndexRecord.TypeOfDatagram == 'C') // Clock
		{
			iProcessClockRecord(fpFileData,
									strClockRecordXML.iNbDatagram,
									&strClockRecordXML,
									&strSignalClockRecord);
			strClockRecordXML.iNbDatagram++;
		}
		else if(strSignalFileIndexRecord.TypeOfDatagram == 'U') // Sound Speed Profile
		{
			iProcessSoundSpeedProfileRecord(fpFileData,
									strSoundSpeedProfileRecordXML.iNbDatagram,
									&strSoundSpeedProfileRecordXML,
									&strSignalSoundSpeedProfileRecord);
			strSoundSpeedProfileRecordXML.iNbDatagram++;
		}
		else if(strSignalFileIndexRecord.TypeOfDatagram == 'D') // Depth
		{

 			iProcessDepthV1Record(fpFileData,
									strDepthV1RecordXML.iNbDatagram,
									&strDepthV1RecordXML,
									&strSignalDepthV1Record);
			strDepthV1RecordXML.iNbDatagram++;

		}
		else if(strSignalFileIndexRecord.TypeOfDatagram == 'X') // XYZ88 (New Depth)
		{
			iProcessDepthV2Record(fpFileData,
									strDepthV2RecordXML.iNbDatagram,
									&strDepthV2RecordXML,
									&strSignalDepthV2Record);
			strDepthV2RecordXML.iNbDatagram++;
		}
		else if(strSignalFileIndexRecord.TypeOfDatagram == 'F') // Raw Range Beam Angles
		{
			iProcessRawRangeBeamAngleV0Record(fpFileData,
									strRawRangeBeamAngleV0RecordXML.iNbDatagram,
									&strRawRangeBeamAngleV0RecordXML,
									&strSignalRawRangeBeamAngleV0Record);
			strRawRangeBeamAngleV0RecordXML.iNbDatagram++;
		}
		else if(strSignalFileIndexRecord.TypeOfDatagram == 'f') // Raw Range Beam Angles (since 2004)
		{
			iProcessRawRangeBeamAngleV1Record(fpFileData,
									strRawRangeBeamAngleV1RecordXML.iNbDatagram,
									&strRawRangeBeamAngleV1RecordXML,
									&strSignalRawRangeBeamAngleV1Record);
			strRawRangeBeamAngleV1RecordXML.iNbDatagram++;
		}
		else if(strSignalFileIndexRecord.TypeOfDatagram == 'N') // Raw Range Beam Angles (replaces previous)
		{
			iProcessRawRangeBeamAngleV2Record(fpFileData,
									strRawRangeBeamAngleV2RecordXML.iNbDatagram,
									&strRawRangeBeamAngleV2RecordXML,
									&strSignalRawRangeBeamAngleV2Record);
			strRawRangeBeamAngleV2RecordXML.iNbDatagram++;
		}
		else if(strSignalFileIndexRecord.TypeOfDatagram == 'H') // Heading
		{
			iProcessHeadingRecord(fpFileData,
									strHeadingRecordXML.iNbDatagram,
									&strHeadingRecordXML,
									&strSignalHeadingRecord);
			strHeadingRecordXML.iNbDatagram++;
		}
		else if(strSignalFileIndexRecord.TypeOfDatagram == 'G') // Surface Sound Speed
		{
			iProcessSurfaceSoundSpeedRecord(fpFileData,
									strSurfaceSoundSpeedRecordXML.iNbDatagram,
									&strSurfaceSoundSpeedRecordXML,
									&strSignalSurfaceSoundSpeedRecord);
			strSurfaceSoundSpeedRecordXML.iNbDatagram++;
		}
		else if(strSignalFileIndexRecord.TypeOfDatagram == 'J') // Mecha Transducer Tilt
		{
			iProcessMechaTransducerTiltRecord(fpFileData,
									strMechaTransducerTiltRecordXML.iNbDatagram,
									&strMechaTransducerTiltRecordXML,
									&strSignalMechaTransducerTiltRecord);
		    strMechaTransducerTiltRecordXML.iNbDatagram++;
		}
		else if(strSignalFileIndexRecord.TypeOfDatagram == 'k') // Water Column
		{
			iProcessWaterColumnRecord(fpFileData,
									strWaterColumnRecordXML.iNbDatagram,
									&strWaterColumnRecordXML,
									&strSignalWaterColumnRecord);
			strWaterColumnRecordXML.iNbDatagram++;
		}
		else if(strSignalFileIndexRecord.TypeOfDatagram == 'q') // Quality Factor
		{
			iProcessQualityFactorRecord(fpFileData,
									strQualityFactorRecordXML.iNbDatagram,
									&strQualityFactorRecordXML,
									&strSignalQualityFactorRecord);
		    strQualityFactorRecordXML.iNbDatagram++;
		}
		else if(strSignalFileIndexRecord.TypeOfDatagram == 'O') // Quality Factor d'IFREMER
		{
		    iProcessQualityFactorFromIfrRecord(fpFileData,
									strQualityFactorFromIfrRecordXML.iNbDatagram,
									&strQualityFactorFromIfrRecordXML,
									&strSignalQualityFactorFromIfrRecord);
		    strQualityFactorFromIfrRecordXML.iNbDatagram++;
		}
		else if(strSignalFileIndexRecord.TypeOfDatagram == 'r') // Raw Beam Samples
		{
			iProcessRawBeamSamplesRecord(fpFileData,
									strRawBeamSamplesRecordXML.iNbDatagram,
									&strRawBeamSamplesRecordXML,
									&strSignalRawBeamSamplesRecord);
			strRawBeamSamplesRecordXML.iNbDatagram++;
		}
		else if(strSignalFileIndexRecord.TypeOfDatagram == 'n') // Network Attitude
		{
			iProcessNetworkAttitudeRecord(fpFileData,
									strNetworkAttitudeRecordXML.iNbDatagram,
									&strNetworkAttitudeRecordXML,
									&strSignalNetworkAttitudeRecord);
			strNetworkAttitudeRecordXML.iNbDatagram++;
		}
		else if(strSignalFileIndexRecord.TypeOfDatagram == 'm') // Stave Data
		{
			iProcessStaveDataRecord(fpFileData,
									strStaveDataRecordXML.iNbDatagram,
									&strStaveDataRecordXML,
									&strSignalStaveDataRecord);
			strStaveDataRecordXML.iNbDatagram++;
		}
		else if(strSignalFileIndexRecord.TypeOfDatagram == '3') // Extra Parameters (0x33 ou 51d)
		{
			iProcessExtraParametersRecord(fpFileData,
									strExtraParametersRecordXML.iNbDatagram,
									&strExtraParametersRecordXML,
									&strSignalExtraParametersRecord);
			strExtraParametersRecordXML.iNbDatagram++;
		}
		else
		{
			// Recherche de datagrammes non d�cod�s d�j� trouv�s.
			for (iLoop=0; iLoop<20; iLoop++)
			{
				if (strSignalFileIndexRecord.TypeOfDatagram == tabStrDatagramNotTranslate[iLoop].cType)
				{
					tabStrDatagramNotTranslate[iLoop].iNbTimes++;
					iFlagTrouve = TRUE;
					break;
				}
			}
			if (iFlagTrouve == FALSE)
			{
				tabStrDatagramNotTranslate[iNbDatagramNotTranslate].cType = strSignalFileIndexRecord.TypeOfDatagram;
				tabStrDatagramNotTranslate[iNbDatagramNotTranslate].iNbTimes++;
				iNbDatagramNotTranslate++;
			}
			iFlagTrouve = FALSE;
		}

		// Calage de l'index de lecture sur le datagramme suivant.
		llPositionDebDatagram = llPositionDebDatagram + strSignalFileIndexRecord.NbOfBytesInDatagram + 4;

	} // Fin de la boucle while de lecture du fichier.

	// End of progress bar
#ifdef GUI
	if(progress)
		progress(100);
#endif

	// Fermeture en lecture du fichier de donn�es.
	fclose(fpFileData);

	printf("Datagrams not translated : %d\n", iNbDatagramNotTranslate);
	// Recherche de datagrammes non d�cod�s d�j� trouv�s.
	for (iLoop=0; iLoop<iNbDatagramNotTranslate; iLoop++)
	{
		printf("Process datagram : Datagram %xh (%dd) found %d times not translated\n",	tabStrDatagramNotTranslate[iLoop].cType, tabStrDatagramNotTranslate[iLoop].cType, tabStrDatagramNotTranslate[iLoop].iNbTimes);
	}
	// Fermeture des fichiers binaires des paquets et �criture des XML.
	utilALL_iSaveAndCloseFiles(G_cRepData, &strFileIndexRecordXML);
	if (strPositionRecordXML.iNbDatagram != 0)
	{
		utilALL_iSaveAndCloseFiles(G_cRepData, &strPositionRecordXML);
	}
	if (strHeightRecordXML.iNbDatagram != 0)
	{
		utilALL_iSaveAndCloseFiles(G_cRepData, &strHeightRecordXML);
	}
	if (strRuntimeRecordXML.iNbDatagram != 0)
	{
		utilALL_iSaveAndCloseFiles(G_cRepData, &strRuntimeRecordXML);
	}
	if (strAttitudeRecordXML.iNbDatagram != 0)
	{
		utilALL_iSaveAndCloseFiles(G_cRepData, &strAttitudeRecordXML);
	}
	if (strInstallParamsRecordXML.iNbDatagram != 0)
	{
		utilALL_iSaveAndCloseFiles(G_cRepData, &strInstallParamsRecordXML);
	}
	if (strSeabedV1RecordXML.iNbDatagram != 0)
	{
		utilALL_iSaveAndCloseFiles(G_cRepData, &strSeabedV1RecordXML);
	}
	if (strSeabedV2RecordXML.iNbDatagram != 0)
	{
		utilALL_iSaveAndCloseFiles(G_cRepData, &strSeabedV2RecordXML);
	}
	if (strClockRecordXML.iNbDatagram != 0)
	{
		utilALL_iSaveAndCloseFiles(G_cRepData, &strClockRecordXML);
	}
	if (strSoundSpeedProfileRecordXML.iNbDatagram != 0)
	{
		utilALL_iSaveAndCloseFiles(G_cRepData, &strSoundSpeedProfileRecordXML);
	}
	if (strDepthV1RecordXML.iNbDatagram != 0)
	{
		utilALL_iSaveAndCloseFiles(G_cRepData, &strDepthV1RecordXML);
	}
	if (strDepthV2RecordXML.iNbDatagram != 0)
	{
		utilALL_iSaveAndCloseFiles(G_cRepData, &strDepthV2RecordXML);
	}
	if (strRawRangeBeamAngleV0RecordXML.iNbDatagram != 0)
	{
		utilALL_iSaveAndCloseFiles(G_cRepData, &strRawRangeBeamAngleV0RecordXML);
	}
	if (strRawRangeBeamAngleV1RecordXML.iNbDatagram != 0)
	{
		utilALL_iSaveAndCloseFiles(G_cRepData, &strRawRangeBeamAngleV1RecordXML);
	}
	if (strRawRangeBeamAngleV2RecordXML.iNbDatagram != 0)
	{
		utilALL_iSaveAndCloseFiles(G_cRepData, &strRawRangeBeamAngleV2RecordXML);
	}
	if (strHeadingRecordXML.iNbDatagram != 0)
	{
		utilALL_iSaveAndCloseFiles(G_cRepData, &strHeadingRecordXML);
	}
	if (strSurfaceSoundSpeedRecordXML.iNbDatagram != 0)
	{
		utilALL_iSaveAndCloseFiles(G_cRepData, &strSurfaceSoundSpeedRecordXML);
	}
	if (strMechaTransducerTiltRecordXML.iNbDatagram != 0)
	{
		utilALL_iSaveAndCloseFiles(G_cRepData, &strMechaTransducerTiltRecordXML);
	}
	if (strWaterColumnRecordXML.iNbDatagram != 0)
	{
		utilALL_iSaveAndCloseFiles(G_cRepData, &strWaterColumnRecordXML);
	}
	if (strQualityFactorRecordXML.iNbDatagram != 0)
	{
		utilALL_iSaveAndCloseFiles(G_cRepData, &strQualityFactorRecordXML);
	}
	if (strQualityFactorFromIfrRecordXML.iNbDatagram != 0)
	{
		utilALL_iSaveAndCloseFiles(G_cRepData, &strQualityFactorFromIfrRecordXML);
	}
	if (strRawBeamSamplesRecordXML.iNbDatagram != 0)
	{
		utilALL_iSaveAndCloseFiles(G_cRepData, &strRawBeamSamplesRecordXML);
	}
	if (strNetworkAttitudeRecordXML.iNbDatagram != 0)
	{
		utilALL_iSaveAndCloseFiles(G_cRepData, &strNetworkAttitudeRecordXML);
	}
	if (strStaveDataRecordXML.iNbDatagram != 0)
	{
		utilALL_iSaveAndCloseFiles(G_cRepData, &strStaveDataRecordXML);
	}
	if (strExtraParametersRecordXML.iNbDatagram != 0)
	{
		utilALL_iSaveAndCloseFiles(G_cRepData, &strExtraParametersRecordXML);
	}


	return EXIT_SUCCESS;

	ERROR_OUT:

	// End of progress bar
#ifdef GUI
	if(progress){
		progress(50);
		progress(100);
	}
#endif

	if (fpFileData != NULL) {
	  fclose(fpFileData);
	  fpFileData = 0;
	}

	return EXIT_FAILURE;
} // iConvertFiles_ALL


#pragma pack()
