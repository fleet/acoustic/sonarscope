// ----------------------------------------------------------------------
// Programme :
//	CONVERT_ALL.C
//
// Objet :
//	Convertir un fichier au format ALL pour d�composer en fichiers
//	XML et fichiers binaires. Les fichiers XML decrivent le contenu des
// 	binaires.
// 	Chaque fichier binaire comporte une donn�e �l�mentaire des paquets par ping
// 	issue des paquets de donn�es du ALL.
//	Il a ete realise sous l'IDE Eclipse 3.4.1 avec compilateur gcc.
//
// Cr�ation :
//	02/10/2008
// Modif. :
//	29/09/2009
//
// Auteur :
//  GLU : (ATL) roger.gallou@altran.com
//		  02 98 05 43 21
//
// Historique des modifications :
// 01/10/2010 :
// - ajout de la d�tection de l'extension (Traitement WCD et ALL automatiques si d�tect�s))
// - passage du compteur de datagramme comme param�tre des fcts de traitements.
// - ajout de Facteur d'�chelles.
// - Renommage de Signals en Variables, de Tables en Images
// - correction du typage de donn�es de type Unsigned Char en u8
// - introduction des donn�es � 2 Dimensions (Image2)
// - restructuration de l'ent�te du fichier
// - cr�ation d'un fichier d'index.
// - traitement de fichiers pour 64 bits.
// - d�placement de fonction utilitaires (Swap et Val Variable from File)
// - inhibition des balises Unit et Tagf, inutilis�es pour l'instant.
//
// 27/01/2010 :
// - ajout du traitement du Datagramme Raw Beam Samples
// 14/05/2012 :
// - ajout du traitement du paquet NetworkAttitude et StaveData
//
// 27/09/2012 : GLU (ALTRAN Ouest)
// - Passage en option de l'affichage de la barre de progression.
// ----------------------------------------------------------------------
#pragma pack(1) // Pour la compilation sous Visual Studio
// et l'alignement sur 8 octets.


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <errno.h>
#include <libgen.h>
#include <signal.h>


#include "ALL_utilities.h"
#include "convertFiles_ALL.h"
#include "ALL_Datagrams.h"

#include "ALL_processFileIndexRecord.h"

#include "Generic_Utilities.h"    // Contenu dans la librairie libCONVERT_utils.a

#ifdef GUI
#include "ProgressInfo.h"
#endif

void
sig_handle(int sig)
{
	fprintf(stderr, "File Process Error : got signal %i\n", sig);
	abort();
}

int main(int argc, char **argv) {

	signal(SIGINT, &sig_handle);

	char 	cFileName[2000],
			cFileNameALL[2000],
			cFileNameWCD[2000],
			cFileNameTmp[2000],
			cDirSonarScope[1500],
			cDirFileData[1500],
			cFlagProgressBar[5];

	int 	iRet = 0,
			iDummy,
			iFlagWCDandALL,
			iErr,
			iProgressBar;

	if (argc < 3) {
		printf("Usage: CONVERT_ALL <input .ALL file>\n");
		printf("       Reads an ALL file and prints some data about it.\n");
		return FALSE;
	}


	printf("File Process : %s\n", argv[2]);
	G_cFileData = calloc(1000, sizeof(char));
	memcpy(G_cFileData, argv[2], strlen(argv[2]));
	G_cRepData = calloc(2000, sizeof(char));
	sprintf(cFileName, "%s", basename(argv[2]));
	G_cFileExtension = (char*) strrchr(cFileName, '.');

	//R�cup�ration du flag de Progress Bar
	memcpy(cFlagProgressBar, argv[1], strlen(argv[1]));
	iProgressBar = (int)atoi(cFlagProgressBar);

	if (G_cFileExtension == NULL) {
		// pas d'extension
		printf("le fichier %s n'a pas d'extension\n", cFileName);
		goto ERROR_OUT;
	} else {
		// sauter le caractere point
		G_cFileExtension++;
	}
	sprintf(cDirFileData, "%s", dirname(argv[2]));
	sprintf(cDirSonarScope, "%s\\%s", cDirFileData, "SonarScope");
	iDummy = access(cDirSonarScope, F_OK);
	if (iDummy != 0) {
		iErr = mkdir(cDirSonarScope);
		if (iErr != 0) {
			switch (errno) {
			// "File exists", on ne fait rien.
			case EEXIST:
				break;
				// Autres cas d'erreurs
			default:
				printf("Error in Directory creating : %s\n", strerror(errno));
				goto ERROR_OUT;
				break;
			}
		}
	}
	cFileName[strlen(cFileName) - 4] = '\0';
	sprintf(cFileNameALL, "%s\\%s%s", cDirFileData, cFileName, ".all");
	sprintf(cFileNameWCD, "%s\\%s%s", cDirFileData, cFileName, ".wcd");
	if (!stricmp(G_cFileExtension, "WCD")) {
		// Test d'existence du fichier compl�mentaire.
		iFlagWCDandALL = access(cFileNameALL, F_OK);
	} else if (!stricmp(G_cFileExtension, "ALL")) {
		// Test d'existence du fichier compl�mentaire.
		iFlagWCDandALL = access(cFileNameWCD, F_OK);
		// Si on ne trouve pas, on recherche dans le r�pertoire \Water_Colmun
		//du niveau sup�rieur.
		if (iFlagWCDandALL == -1)
		{
			sprintf(cDirFileData, "%s\\Water_Column", dirname(argv[2]));
			sprintf(cFileNameWCD, "%s\\%s%s", cDirFileData, cFileName, ".wcd");
			iFlagWCDandALL = access(cFileNameWCD, F_OK);
		}
	}
	sprintf(cFileNameTmp, "%s_tmp", cFileName);
	sprintf(G_cRepData, "%s\\%s", cDirSonarScope, cFileNameTmp);
	if (DEBUG)
		printf("-- Directory Creation : %s\n", G_cRepData);
	iErr = mkdir(G_cRepData);
	if (iErr != 0) {
		switch (errno) {
		// "File exists", on ne fait rien.
		case EEXIST:
			break;
			// Autres cas d'erreurs
		default:
			printf("Error in Directory creating : %s\n", strerror(errno));
			break;
		}
	}
	G_iFlagEndian = util_iGetIndianAll(G_cFileData);
	if (!stricmp(G_cFileExtension, "ALL"))
		#ifndef GUI
			iRet = iConvertFiles_ALL(cFileNameALL, NULL);
		#else
			if (iProgressBar == 1)
				runWithProgress(argc, argv, &iConvertFiles_ALL, cFileNameALL);
			else
				iRet = iConvertFiles_ALL(cFileNameALL, NULL);
		#endif


	// Si on n'a un fichier WCD.
	if (iFlagWCDandALL == 0) {
		sprintf(G_cFileExtension, "%s", "wcd");
		#ifndef GUI
			iRet = iConvertFiles_ALL(cFileNameWCD, NULL);
		#else
			if (iProgressBar == 1)
				runWithProgress(argc, argv, &iConvertFiles_ALL, cFileNameWCD);
			else
				iRet = iConvertFiles_ALL(cFileNameWCD, NULL);
		#endif
	}
	else if (!stricmp(G_cFileExtension, "WCD"))
	{
		#ifndef GUI
			iRet = iConvertFiles_ALL(cFileNameWCD, NULL);
			if (iRet == EXIT_FAILURE)
			{
				goto ERROR_OUT;
			}

			#else
			runWithProgress(argc, argv, &iConvertFiles_ALL, cFileNameWCD);
		#endif
		if (iFlagWCDandALL == 0)
		{
			sprintf(G_cFileExtension, "%s", "all");
			#ifndef GUI
				iRet = iConvertFiles_ALL(cFileNameALL, NULL);
				if (iRet == EXIT_FAILURE)
				{
					goto ERROR_OUT;
				}
			#else
				runWithProgress(argc, argv, &iConvertFiles_ALL, cFileNameALL);
			#endif
		}
	}


	iRet = util_iRenameDir(G_cRepData);
	if (iRet == 0)
		printf("Process Ending : renaming Temporary Directory %s\n", G_cRepData);

	return EXIT_SUCCESS;

	ERROR_OUT:
		return EXIT_FAILURE;

} // Main

#pragma pack()
