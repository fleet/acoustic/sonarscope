/*
 * MAIN_ALL.h
 *
 *  Created on: 13 oct. 2008
 *      Author: rgallou
 */

#ifndef MAIN_ALL_H_
#define MAIN_ALL_H_

#pragma pack(1) // Pour la compilation sous Visual Studio
				// et l'alignement sur 8 octets.
	#define DEBUG 0

	/*** Variables globales ***/

#pragma pack()

#endif /* MAIN_ALL_H_ */
