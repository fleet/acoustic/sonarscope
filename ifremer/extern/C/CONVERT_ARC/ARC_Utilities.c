/*
 * ARC_utilities.c
 *
 *  Created on: 6 nov. 2008
 *      Author: rgallou
 */
#include<string.h>
#include<stdio.h>
#include<stdlib.h>
#include<errno.h>
#include<io.h>

#include "ARC_utilities.h"
#include "convertFiles_ARC.h"




///////////////////////////////////////////////////////////////////////////////////////////
//// Fonction :	utilARC_cValVarFromFile
//// Objet 	:	Relit la valeur depuis le fichier Bin.
//// Modification : 12/11/08
//// Auteur 	: GLU
///////////////////////////////////////////////////////////////////////////////////////////
//int utilARC_cValVarFromFile(T_ARCVARXML strARCVarXML,
//							char *cValeur)
//{
//	unsigned short		usTailleEnByte,
//						usNbElem;
//
//	void 				*ptr_vValeur;
//
//	int 				iRet = 0;
//
//
//	usTailleEnByte= util_usNbOctetTypeVar(strARCVarXML.cType);
//	usNbElem = strARCVarXML.usNbElem;
//
//	ptr_vValeur = (void*)malloc(usTailleEnByte*usNbElem*sizeof(char));
//	strARCVarXML.ptr_fpBin = fopen(strARCVarXML.cNomPathFileBin, "r+b");
//	if (!strARCVarXML.ptr_fpBin)
//	{
//	   printf("%s -- Error in retrieve Signal constant value   : %s\n", __FILE__, strARCVarXML.cNomPathFileBin);
//	   iRet = -1;
//	}
//	else
//	{
//		fread(ptr_vValeur, usTailleEnByte, usNbElem, strARCVarXML.ptr_fpBin);
//
//		iRet = util_cValVarConstant(	strARCVarXML.cType,
//										ptr_vValeur,
//										(int)usNbElem,
//										cValeur);
//		fclose(strARCVarXML.ptr_fpBin);
//		//On lib�re uniquement pour des donn�es de type non-char.
//		// sinon plantage (????)
//		//if (strcmp(strARCVarXML.cType, "char"))
//		//{
//			free(ptr_vValeur);
//		//}
//	}
//
//	return iRet;
//
//}	// utilARC_cValVarFromFile

//////////////////////////////////////////////////////////////////////
// Fonction :    utilARC_iSaveAndCloseFiles
// Objet     :    Sauvegarde de la structure XML dans un fichier.
// Modification : 15/10/08
// Auteur     : GLU
//////////////////////////////////////////////////////////////////////
int utilARC_iSaveAndCloseFiles( char *cRepData,
                                T_ARCRECORDXML *strARCRecordXML)
{
    char    *cNomFicXML,
            *ptr_cNomHeader,
            *cTypeMatLab,
            cNomFicBin[200],
            *cValeur = NULL,
            cValOneChar[1];


    int     iVarSignal,
			iVarImage,
			iRet = 0;

    int		iSize;

    unsigned short     usNbElem,
                    usTailleEnByte;

    FILE        *fpXML = NULL;

/*
 *
    iVarSignal = 3;    // D�signation du EmModel
 	fclose(strARCRecordXML->strTabSignalXML[iVarSignal].ptr_fpBin);
//    iRet = utilARC_cValVarFromFile(    strARCRecordXML->strTabSignalXML[iVarSignal],
//                                    strARCRecordXML->ptr_cModelHeader);

 	iRet = util_cValVarFromFile(strARCRecordXML->strTabSignalXML[iVarSignal].cNomPathFileBin,
								strARCRecordXML->strTabSignalXML[iVarSignal].cType,
								strARCRecordXML->strTabSignalXML[iVarSignal].usNbElem,
								strARCRecordXML->ptr_cModelHeader);

    iVarSignal = 7;    // D�signation du System Serial Number
    strARCRecordXML->ptr_cSerialNumberHeader = (char*)malloc(256*sizeof(char));
    fclose(strARCRecordXML->strTabSignalXML[iVarSignal].ptr_fpBin);
//    iRet = utilARC_cValVarFromFile( strARCRecordXML->strTabSignalXML[iVarSignal],
//                                    strARCRecordXML->ptr_cSerialNumberHeader);
	iRet = util_cValVarFromFile(strARCRecordXML->strTabSignalXML[iVarSignal].cNomPathFileBin,
								strARCRecordXML->strTabSignalXML[iVarSignal].cType,
								strARCRecordXML->strTabSignalXML[iVarSignal].usNbElem,
								strARCRecordXML->ptr_cSerialNumberHeader);
 */
    iSize = strlen(strARCRecordXML->ptr_cNomHeader) + 1;
    ptr_cNomHeader = (char *)malloc(iSize*sizeof(char));
    ptr_cNomHeader = strARCRecordXML->ptr_cNomHeader;
    iSize = 5 + strlen(cRepData) + 2 + strlen(ptr_cNomHeader) + 4; // Nom du paquet + '.xml'
    cNomFicXML = calloc(iSize, sizeof(char));
    // Si Extension du fichier d'origine = '.NA'
	if (!stricmp(G_cFileExtension, "NA")
			|| !stricmp(G_cFileExtension, "SO")
			|| !stricmp(G_cFileExtension, "TI")
			|| !stricmp(G_cFileExtension, "IM")
			|| !stricmp(G_cFileExtension, "CE")
			|| !stricmp(G_cFileExtension, "JO")
			|| !stricmp(G_cFileExtension, "PA"))
	    sprintf(cNomFicXML, "%s\\%s_%s%s", cRepData, G_cFileExtension, ptr_cNomHeader, ".xml");
	else
	{
		printf("L'extension du fichier n'est pas reconnue : %s\n", G_cFileExtension);
		return EXIT_FAILURE;
	}
    printf("-- XML File Creation : %s \n", cNomFicXML);


    // Cr�ation de l'ent�te de doc XML
    fpXML = fopen(cNomFicXML, "w+");
    fprintf(fpXML, "%s", "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r");
    fprintf(fpXML, "%s", "<ROOT>\r");
    fprintf(fpXML, "%s%s%s", "\t<Title>", strARCRecordXML->ptr_cLabelHeader, "</Title>\r");
    fprintf(fpXML, "%s%s%s", "\t<Constructor>", "Simrad", "</Constructor>\r");
    fprintf(fpXML, "%s%s%s", "\t<Model>", strARCRecordXML->ptr_cModelHeader, "</Model>\r");
    // fprintf(fpXML, "%s%s%s", "\t<SystemSerialNumber>", strARCRecordXML->ptr_cSerialNumberHeader, "</SystemSerialNumber>\r");
    fprintf(fpXML, "%s%d%s", "\t<NbDatagrams>", strARCRecordXML->iNbDatagram, "</NbDatagrams>\r");
    fprintf(fpXML, "%s%s%s", "\t<TimeOrigin>", "01/01/-4713", "</TimeOrigin>\r");
    fprintf(fpXML, "%s%s%s", "\t<Comments>", strARCRecordXML->ptr_cCommentsHeader, "</Comments>\r");


    if (strARCRecordXML->sNbVarSignal > 0)
    {
        fprintf(fpXML, "%s", "\t<Variables>\r");

        // Traitement sur l'ensemble des valeurs du RTH.
        for (iVarSignal=0; iVarSignal<strARCRecordXML->sNbVarSignal; iVarSignal++)
        {
            cTypeMatLab = (char*)malloc(10*sizeof(char));
            iRet = util_cConvertTypeLabel(    strARCRecordXML->strTabSignalXML[iVarSignal].cType,
                                            cTypeMatLab);

            fprintf(fpXML, "%s", "\t\t<item>\r");
            fprintf(fpXML, "%s%s%s", "\t\t\t<Name>", strARCRecordXML->strTabSignalXML[iVarSignal].cNom, "</Name>\r");
            fprintf(fpXML, "%s%s%s", "\t\t\t<Storage>", cTypeMatLab, "</Storage>\r");
            if (strARCRecordXML->strTabSignalXML[iVarSignal].usNbElem > 1)
            {
                fprintf(fpXML, "%s%d%s", "\t\t\t<NbElem>",  strARCRecordXML->strTabSignalXML[iVarSignal].usNbElem, "</NbElem>\r");
            }
            //Infos report�e dans format SSC-XML-Bin : fprintf(fpXML, "%s%s%s", "\t\t\t<Unit>", strARCRecordXML->strTabSignalXML[iVarSignal].cUnit, "</Unit>\r");
            //Infos report�e dans format SSC-XML-Bin : fprintf(fpXML, "%s%s%s", "\t\t\t<Tag>", strARCRecordXML->strTabSignalXML[iVarSignal].cTag, "</Tag>\r");

            // Fermeture pr�alable du fichier.
            if (strARCRecordXML->strTabSignalXML[iVarSignal].ptr_fpBin != NULL)
            {
                fclose(strARCRecordXML->strTabSignalXML[iVarSignal].ptr_fpBin);
            }

            // R�cup�ration de la variable d�tect�e comme constante.
            usNbElem = strARCRecordXML->strTabSignalXML[iVarSignal].usNbElem;
            usTailleEnByte = util_usNbOctetTypeVar(strARCRecordXML->strTabSignalXML[iVarSignal].cType);

            if (strARCRecordXML->strTabSignalXML[iVarSignal].bFlagConstant == TRUE)
            {
                // Pb de lib�ration de m�moire si  usNbElem * usTailleEnByte = 1
            	if (usNbElem != 1 || usTailleEnByte != 1)
                {
					 // Allocation de la valeur � r�cup�rer selon la taille. Si elle est
					 // du type Chaine, elle doit contenir autant d'�l�ments que d�clar�s.
					 if (strcmp(cTypeMatLab, "char"))
					 {
						 cValeur = (char*)malloc(256*sizeof(char));
					 }
					 else
					 {
						 cValeur = (char*)malloc(usNbElem*sizeof(char));
					 }
//                     iRet = utilALL_cValVarFromFile(strARCRecordXML->strTabSignalXML[iVarSignal],
//                                                     cValeur);
                 	 iRet = util_cValVarFromFile(strARCRecordXML->strTabSignalXML[iVarSignal].cNomPathFileBin,
                 								strARCRecordXML->strTabSignalXML[iVarSignal].cType,
                 								strARCRecordXML->strTabSignalXML[iVarSignal].usNbElem,
                 								cValeur);
                     fprintf(fpXML, "%s%s%s", "\t\t\t<Constant>", cValeur, "</Constant>\r");
					 free(cValeur);
                }
                else
                {
//                    iRet = utilALL_cValVarFromFile(strARCRecordXML->strTabSignalXML[iVarSignal],
//                    		&cValOneChar[0]);
                	iRet = util_cValVarFromFile(strARCRecordXML->strTabSignalXML[iVarSignal].cNomPathFileBin,
                								strARCRecordXML->strTabSignalXML[iVarSignal].cType,
                								strARCRecordXML->strTabSignalXML[iVarSignal].usNbElem,
                								&cValOneChar[0]);
                    fprintf(fpXML, "%s%s%s", "\t\t\t<Constant>", &cValOneChar[0], "</Constant>\r");
                }
 				// Effacement du fichier Bin
            	iRet = remove((const char *)strARCRecordXML->strTabSignalXML[iVarSignal].cNomPathFileBin);
				if (iRet !=0)
				{
				   printf("Cannot remove file %s (%s)\n",
							   strARCRecordXML->strTabSignalXML[iVarSignal].cNomPathFileBin, ERRORSTRING);
				}
            }
            else
            {
                sprintf(cNomFicBin, "%s%s",strARCRecordXML->strTabSignalXML[iVarSignal].cNom, ".bin");
                fprintf(fpXML, "%s%s%s", "\t\t\t<FileName>", cNomFicBin, "</FileName>\r");
            }
            if (strARCRecordXML->strTabSignalXML[iVarSignal].fScaleFactor != 1.0)
				fprintf(fpXML, "%s%g%s", "\t\t\t<ScaleFactor>", strARCRecordXML->strTabSignalXML[iVarSignal].fScaleFactor, "</ScaleFactor>\r");
            if (strARCRecordXML->strTabSignalXML[iVarSignal].fAddOffset != 0.0)
				fprintf(fpXML, "%s%f%s", "\t\t\t<AddOffset>", strARCRecordXML->strTabSignalXML[iVarSignal].fAddOffset, "</AddOffset>\r");
            fprintf(fpXML, "%s", "\t\t</item>\r");
            free(cTypeMatLab);

        } // Fin de la boucle sur les donn�es RTH
        fprintf(fpXML, "%s", "\t</Variables>\r");
        free(strARCRecordXML->strTabSignalXML);
    } // Fin de l'�criture des Signaux


    // Ouverture de la balise Tables
    if (strARCRecordXML->sNbVarImage > 0 || strARCRecordXML->sNbVarImage2 >0)
    {
        fprintf(fpXML, "%s", "\t<Tables>\r");
    }
    // Traitement sur l'ensemble des attributs.
    if (strARCRecordXML->sNbVarImage > 0)
    {
         for (iVarImage=0; iVarImage<strARCRecordXML->sNbVarImage; iVarImage++)
        {
            cTypeMatLab = (char*)malloc(10*sizeof(char));
            iRet = util_cConvertTypeLabel(  strARCRecordXML->strTabImageXML[iVarImage].cType,
                                            cTypeMatLab);

            fprintf(fpXML, "%s", "\t\t<item>\r");
            fprintf(fpXML, "%s%s%s", "\t\t\t<Name>", strARCRecordXML->strTabImageXML[iVarImage].cNom, "</Name>\r");
            fprintf(fpXML, "%s%s%s", "\t\t\t<Storage>", cTypeMatLab, "</Storage>\r");
            if (strARCRecordXML->strTabImageXML[iVarImage].usNbElem > 1)
            {
                fprintf(fpXML, "%s%d%s", "\t\t\t<NbElem>", strARCRecordXML->strTabImageXML[iVarImage].usNbElem, "</NbElem>\r");
            }
            //Infos report�e dans format SSC-XML-Bin : fprintf(fpXML, "%s%s%s", "\t\t\t<Unit>", strARCRecordXML->strTabImageXML[iVarImage].cUnit, "</Unit>\r");
            //Infos report�e dans format SSC-XML-Bin : fprintf(fpXML, "%s%s%s", "\t\t\t<Tag>", strARCRecordXML->strTabImageXML[iVarImage].cTag, "</Tag>\r");
            // Fermeture pr�alable du fichier.
            fclose(strARCRecordXML->strTabImageXML[iVarImage].ptr_fpBin);
            usNbElem = strARCRecordXML->strTabImageXML[iVarImage].usNbElem;
            usTailleEnByte = util_usNbOctetTypeVar(strARCRecordXML->strTabImageXML[iVarImage].cType);
            if (strARCRecordXML->ptr_cNomIndexTable != NULL)
            {
                fprintf(fpXML, "%s%s%s", "\t\t\t<Indexation>", strARCRecordXML->ptr_cNomIndexTable, "</Indexation>\r");
            }

            if (strARCRecordXML->strTabImageXML[iVarImage].bFlagConstant == TRUE)
            {
                // Pb de lib�ration de m�moire si  usNbElem * usTailleEnByte = 1
            	if (usNbElem != 1 || usTailleEnByte != 1)
            	{
                	// Allocation de la valeur � r�cup�rer selon la taille. Si elle est
                	// du type Chaine, elle doit contenir autant d'�l�ments que d�clar�s.
                    // Ca marche mais c'est moyen, moyen !!!! (cf. Traitement des signaux)
                	if (strcmp(cTypeMatLab, "char"))
                    {
                        cValeur = (char*)malloc(256*sizeof(char));
                    }
                    else
                    {
                        cValeur = (char*)malloc(usNbElem*sizeof(char));
                    }
                    //iRet = utilARC_cValVarFromFile(strARCRecordXML->strTabImageXML[iVarImage],
                    //                            cValeur);
                	iRet = util_cValVarFromFile(strARCRecordXML->strTabImageXML[iVarImage].cNomPathFileBin,
                								strARCRecordXML->strTabImageXML[iVarImage].cType,
                								strARCRecordXML->strTabImageXML[iVarImage].usNbElem,
                								cValeur);
                    fprintf(fpXML, "%s%s%s", "\t\t\t<Constant>", cValeur, "</Constant>\r");

     				if (!strcmp(cTypeMatLab, "char"))
    				// ???? : pb de lib�ration de memoire si la variable fait un octet.
    				{
    					free(cValeur);
    				}
            	}
 				else
 				{
                    //iRet = utilARC_cValVarFromFile(strARCRecordXML->strTabImageXML[iVarImage],
                    //		&cValOneChar[0]);
                   	iRet = util_cValVarFromFile(strARCRecordXML->strTabImageXML[iVarImage].cNomPathFileBin,
                    								strARCRecordXML->strTabImageXML[iVarImage].cType,
                    								strARCRecordXML->strTabImageXML[iVarImage].usNbElem,
                    								&cValOneChar[0]);
                    fprintf(fpXML, "%s%s%s", "\t\t\t<Constant>", &cValOneChar[0], "</Constant>\r");
 				}
                // Effacement du fichier
 				if (remove((const char *)strARCRecordXML->strTabImageXML[iVarImage].cNomPathFileBin) !=0)
 				{
 				   printf("Cannot remove file %s (%s)\n",
 							   strARCRecordXML->strTabImageXML[iVarImage].cNomPathFileBin, ERRORSTRING);
 				}
            }
            else
            {
                sprintf(cNomFicBin, "%s%s",strARCRecordXML->strTabImageXML[iVarImage].cNom, ".bin");
                fprintf(fpXML, "%s%s%s", "\t\t\t<FileName>", cNomFicBin, "</FileName>\r");
            }
            if (strARCRecordXML->strTabImageXML[iVarImage].fScaleFactor != 1.0)
            	fprintf(fpXML, "%s%g%s", "\t\t\t<ScaleFactor>", strARCRecordXML->strTabImageXML[iVarImage].fScaleFactor, "</ScaleFactor>\r");
            if (strARCRecordXML->strTabImageXML[iVarImage].fAddOffset != 0.0)
            	fprintf(fpXML, "%s%f%s", "\t\t\t<AddOffset>", strARCRecordXML->strTabImageXML[iVarImage].fAddOffset, "</AddOffset>\r");
            fprintf(fpXML, "%s", "\t\t</item>\r");
            free(cTypeMatLab);

        } // Fin de la boucle sur les donn�es Image
        free(strARCRecordXML->strTabImageXML);
        } //Fin de l'�criture des Images (RD et OD)

    // Traitement d'une 2i�me image (si n�cessaire).
	if (strARCRecordXML->sNbVarImage2 > 0)
	{
		for (iVarImage=0; iVarImage<strARCRecordXML->sNbVarImage2; iVarImage++)
		{
			cTypeMatLab = (char*)malloc(10*sizeof(char));
			iRet = util_cConvertTypeLabel(strARCRecordXML->strTabImage2XML[iVarImage].cType,
											cTypeMatLab);

			fprintf(fpXML, "%s", "\t\t<item>\r");
			fprintf(fpXML, "%s%s%s", "\t\t\t<Name>", strARCRecordXML->strTabImage2XML[iVarImage].cNom, "</Name>\r");
			fprintf(fpXML, "%s%s%s", "\t\t\t<Storage>", cTypeMatLab, "</Storage>\r");
			if (strARCRecordXML->strTabImage2XML[iVarImage].usNbElem > 1)
			{
				fprintf(fpXML, "%s%d%s", "\t\t\t<NbElem>", strARCRecordXML->strTabImage2XML[iVarImage].usNbElem, "</NbElem>\r");
			}
			//Infos report�e dans format SSC-XML-Bin : fprintf(fpXML, "%s%s%s", "\t\t\t<Unit>", strARCRecordXML->strTabImage2XML[iVarImage].cUnit, "</Unit>\r");
			//Infos report�e dans format SSC-XML-Bin : fprintf(fpXML, "%s%s%s", "\t\t\t<Tag>", strARCRecordXML->strTabImage2XML[iVarImage].cTag, "</Tag>\r");
			// Fermeture pr�alable du fichier.
			fclose(strARCRecordXML->strTabImage2XML[iVarImage].ptr_fpBin);
			usNbElem = strARCRecordXML->strTabImage2XML[iVarImage].usNbElem;
			usTailleEnByte = util_usNbOctetTypeVar(strARCRecordXML->strTabImage2XML[iVarImage].cType);
			if (strARCRecordXML->ptr_cNomIndexTable2 != NULL)
			{
				fprintf(fpXML, "%s%s%s", "\t\t\t<Indexation>", strARCRecordXML->ptr_cNomIndexTable2, "</Indexation>\r");
			}

			if (strARCRecordXML->strTabImage2XML[iVarImage].bFlagConstant == TRUE)
			{
                // Pb de lib�ration de m�moire si  usNbElem * usTailleEnByte = 1
            	if (usNbElem != 1 || usTailleEnByte != 1)
                {
					// Allocation de la valeur � r�cup�rer selon la taille. Si elle est
					// du type Chaine, elle doit contenir autant d'�l�ments que d�clar�s.
					if (strcmp(cTypeMatLab, "char"))
					{
						cValeur = (char*)malloc(256*sizeof(char));
					}
					else
					{
						cValeur = (char*)malloc(usNbElem*sizeof(char));
					}
//					iRet = utilARC_cValVarFromFile(strARCRecordXML->strTabImage2XML[iVarImage],
//												cValeur);
                   	iRet = util_cValVarFromFile(strARCRecordXML->strTabImage2XML[iVarImage].cNomPathFileBin,
                    								strARCRecordXML->strTabImage2XML[iVarImage].cType,
                    								strARCRecordXML->strTabImage2XML[iVarImage].usNbElem,
                    								cValeur);
					fprintf(fpXML, "%s%s%s", "\t\t\t<Constant>", cValeur, "</Constant>\r");
					if (!strcmp(cTypeMatLab, "char")) // ???? : pb de lib�ration de memoire
					{
						free(cValeur);
					}
                }
                else
                {
//                    iRet = utilARC_cValVarFromFile(strARCRecordXML->strTabImage2XML[iVarImage],
//                    		&cValOneChar[0]);
                   	iRet = util_cValVarFromFile(strARCRecordXML->strTabImage2XML[iVarImage].cNomPathFileBin,
                    								strARCRecordXML->strTabImage2XML[iVarImage].cType,
                    								strARCRecordXML->strTabImage2XML[iVarImage].usNbElem,
                    								&cValOneChar[0]);
                   fprintf(fpXML, "%s%s%s", "\t\t\t<Constant>", &cValOneChar[0], "</Constant>\r");
                }
				// Effacement du fichier
				if (remove((const char *)strARCRecordXML->strTabImage2XML[iVarImage].cNomPathFileBin) !=0)
				{
				   printf("Cannot remove file %s (%s)\n",
							   strARCRecordXML->strTabImage2XML[iVarImage].cNomPathFileBin, ERRORSTRING);
				}
			}
			else
			{
				sprintf(cNomFicBin, "%s%s",strARCRecordXML->strTabImage2XML[iVarImage].cNom, ".bin");
				fprintf(fpXML, "%s%s%s", "\t\t\t<FileName>", cNomFicBin, "</FileName>\r");
			}
            if (strARCRecordXML->strTabImage2XML[iVarImage].fScaleFactor != 1.0)
            	fprintf(fpXML, "%s%g%s", "\t\t\t<ScaleFactor>", strARCRecordXML->strTabImage2XML[iVarImage].fScaleFactor, "</ScaleFactor>\r");
            if (strARCRecordXML->strTabImage2XML[iVarImage].fAddOffset != 0.0)
            	fprintf(fpXML, "%s%f%s", "\t\t\t<AddOffset>", strARCRecordXML->strTabImage2XML[iVarImage].fAddOffset, "</AddOffset>\r");
			fprintf(fpXML, "%s", "\t\t</item>\r");
			free(cTypeMatLab);
		} // Fin de la boucle sur les donn�es Image
		free(strARCRecordXML->strTabImage2XML);
	} //Fin de l'�criture des Images (RD et OD)

	// Fermeture de la balise Tables
	if (strARCRecordXML->sNbVarImage > 0 || strARCRecordXML->sNbVarImage2 >0)
	{
		fprintf(fpXML, "%s", "\t</Tables>\r");
	}

    fprintf(fpXML, "%s", "</ROOT>\r");
    fclose(fpXML);

    free(cNomFicXML);
    free(strARCRecordXML->ptr_cLabelHeader);
    free(strARCRecordXML->ptr_cNomHeader); // ptr_cNomHeader est du coup lib�r�.

    if (iRet != 0)
        return EXIT_FAILURE;
    else
        return EXIT_SUCCESS;


} //utilARC_iSaveAndCloseFiles

//////////////////////////////////////////////////////////////////////
// Fonction		:	util_iGetIndianAll
// Objet		:	Test du "boutisme" des fichiers
// Modification :	11/12/08
// Auteur		:	GLU
//////////////////////////////////////////////////////////////////////
int util_iGetIndianAll(char *cNomFic)
{

FILE *fp;

int		iNumberOfBytesInDatagram_1,
		iNumberOfBytesInDatagram_2,
		iNbOctetsLus,
		iTypeEndian;

	fp = fopen(cNomFic, "rb");

	if(fp  == NULL)
	{
		printf("\nProblem in access Data file : %s\n", cNomFic);
		iTypeEndian = -1;
		return iTypeEndian;
	}

	// Lecture d'un fichier quelconque
	iNbOctetsLus = fread(&iNumberOfBytesInDatagram_1, sizeof(int), 1, fp);
	iNumberOfBytesInDatagram_2 = util_iBswap_32(iNumberOfBytesInDatagram_1);

	fclose(fp);

	if (abs(iNumberOfBytesInDatagram_1) < abs(iNumberOfBytesInDatagram_2))
		iTypeEndian = 1; // Little Endian
	else
		iTypeEndian = 0; // Big Endian

	// For�age en litte endian pour les fichiers Archive.
	iTypeEndian = 0; // Little Endian

	return iTypeEndian;
} // util_iGetIndianAll

//////////////////////////////////////////////////////////////////////
// Fonction		:	util_iCreateDirDatagram
// Objet		:	Cr�ation du r�pertoire d�di�es aux donn�es d'un
//					Datagrammes
// Modification :	29/09/09
// Auteur		:	GLU
//////////////////////////////////////////////////////////////////////
int util_iCreateDirDatagram(char *cRepData,
							char *cNomDatagram,
							char *cRepertoire)
{

int 	iErr;

	// Si l'extension est reconnue comme paquet Archive.
	if (!stricmp(G_cFileExtension, "NA")
			|| !stricmp(G_cFileExtension, "SO")
			|| !stricmp(G_cFileExtension, "CE")
			|| !stricmp(G_cFileExtension, "PA")
			|| !stricmp(G_cFileExtension, "IM")
			|| !stricmp(G_cFileExtension, "TI"))
		sprintf(cRepertoire, "%s\\%s_%s", cRepData, G_cFileExtension, cNomDatagram);
	else
	{
		printf("L'extension du fichier n'est pas reconnue : %s\n", G_cFileExtension);
		return EXIT_FAILURE;
	}


	if (DEBUG)
	   printf("-- Directory Creation : %s\n",cRepertoire);

	// Cr�ation du r�pertoire
	iErr = mkdir(cRepertoire);
	if (iErr != 0)
	{
	   switch (errno)
	   {
		   //"File exists"
		   case EEXIST:
			   break;
		   default:
			   printf("Error in Directory creation %s\n", strerror(errno));
			   return EXIT_FAILURE;
			   break;
	   }
	}

	return EXIT_SUCCESS;

} // util_iCreateDirDatagram

