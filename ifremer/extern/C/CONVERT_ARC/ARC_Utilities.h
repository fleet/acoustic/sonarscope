/*
 * ARC_utilities.h
 *
 *  Created on: 6 nov. 2008
 *      Author: dwolyniec
 */

#ifndef ARC_UTILITIES_H_
#define ARC_UTILITIES_H_

#include "ARC_Datagrams.h"


//int utilARC_cValVarFromFile(T_ARCVARXML strARCVarXML,
//							char *cValeur);

int utilARC_iSaveAndCloseFiles(	char *cRepData,
								T_ARCRECORDXML *strARCRecordXML);

int util_iGetIndianAll(char *nomFic);

int util_iCreateDirDatagram(char *cRepData,
							char *cNomDatagram,
							char *cRepertoire);


#endif /* ARC_UTILITIES_H_ */
