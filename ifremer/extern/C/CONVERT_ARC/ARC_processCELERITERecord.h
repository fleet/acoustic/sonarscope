/*
 * ARC_processCELERITERecord.h
 *
 *  Created on: 21 nov. 2008
 *      Author: rgallou
 */
#pragma pack(1)

#ifndef ARC_PROCESSCELERITERECORD_H_
#define ARC_PROCESSCELERITERECORD_H_

#include "ARC_DATAGRAMS.H"


// D�finition de la structure CELERITE
typedef struct  {
	// On d�crit la taille fixe des chaines comprenant la virgule de s�paration.
	char			BeginMsg[7];	// = '$xxxxx'
	char			Date[9];		// JJ/MM/AA
	char			Heure[13];  	// HH:MM:SS.DDD
	char			TypeRecord[6];		// = BATHY
	unsigned char	Reserve1[1];
	unsigned short	NbSamples;
} T_CELERITE_RECORD_SIGNAL;

// En plus, lecture forc�e d'un mot de 3 octets en fin de paquet.

typedef struct  {
	unsigned short	Profondeur;
	unsigned short	SoundSpeed;
} T_CELERITE_RECORD_IMAGE;

int iProcessCELERITERecord(	FILE *fpFileData,
								int iNbRecord,
								T_ARCRECORDXML *strARCRecordXML,
								T_CELERITE_RECORD_SIGNAL *ptrImage_prevRecord);

int iInitXMLBinCELERITERecord(char *cTitleDatagram,
								char *cNomDatagram,
								char *cComments,
								T_ARCRECORDXML *strARCRecordXML);

#endif /* ARC_PROCESSCELERITERECORD_H_ */

#pragma pack()
