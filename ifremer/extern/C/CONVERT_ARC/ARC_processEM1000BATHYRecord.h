/*
 * ARC_processEM1000BATHYRecord.h
 *
 *  Created on: 21 nov. 2008
 *      Author: rgallou
 */
#pragma pack(1)

#ifndef ARC_PROCESSEM1000BATHYRECORD_H_
#define ARC_PROCESSEM1000BATHYRECORD_H_

#include "ARC_DATAGRAMS.H"


// D�finition de la structure EM1000BATHY
typedef struct  {
	// On d�crit la taille fixe des chaines comprenant la virgule de s�paration.
	char			BeginMsg[7];	// = '$xxxxx'
	char			Date[9];		// JJ/MM/AA
	char			Heure[13];  	// HH:MM:SS.DDD
	char			TypeRecord[6];		// = BATHY
	unsigned char	Reserve1[1];
	unsigned short	NumeroPing;
	unsigned char	Mode[1];
	unsigned char	NbBeams[1];		// d�crit comme facteur de qualit�.
	unsigned short	Draught;	// ProfondeurSousQuille
	unsigned short	Cap;
	unsigned short	Roulis;
	unsigned short	Tangage;
	unsigned short	TangageCapteur;
	unsigned short	Pilonnement;
	unsigned short	VitesseSon;
} T_EM1000BATHY_RECORD_SIGNAL;

// En plus, lecture forc�e d'un mot de 3 octets en fin de paquet.

typedef struct  {
	unsigned short	Profondeur;
	unsigned short	AcrossDist;
	unsigned short	AlongDist;
	unsigned short	Duration;
	unsigned short	Reflectivite;
	unsigned short	BeamQualityFactor;
	unsigned short	Reserve;
} T_EM1000BATHY_RECORD_IMAGE;

int iProcessEM1000BATHYRecord(	FILE *fpFileData,
								int iNbRecord,
								T_ARCRECORDXML *strARCRecordXML,
								T_EM1000BATHY_RECORD_SIGNAL *ptrImage_prevRecord);

int iInitXMLBinEM1000BATHYRecord(char *cTitleDatagram,
								char *cNomDatagram,
								char *cComments,
								T_ARCRECORDXML *strARCRecordXML);

#endif /* ARC_PROCESSEM1000BATHYRECORD_H_ */

#pragma pack()
