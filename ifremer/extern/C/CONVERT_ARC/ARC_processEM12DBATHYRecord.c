/*
 * ARC_processEM12DBATHYREcord.c
 *
 *  Created on: 13 oct. 2008
 *      Author: rgallou
 */
#pragma pack(1)

#include <sys/types.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "ARC_Datagrams.h"
#include "ARC_Utilities.h"
#include "Generic_Utilities.h"
#include "ARC_processEM12DBATHYRecord.h"
#include "convertFiles_ARC.h"

#define NBSIGNAL 	14
#define NBIMAGE 	7


//----------------------------------------------------------------
// Fonction :	iProcessEM12DBATHYREcord
// Objet 	:	Traitement du paquet de capteurs de navigation
// Modif.	: 	03/11/08
// Auteur 	:	GLU
//----------------------------------------------------------------
int iProcessEM12DBATHYRecord(	FILE *fpFileData,
								int iNbRecord,
								T_ARCRECORDXML *strARCRecordXML,
								T_EM12DBATHY_RECORD_SIGNAL *ptrSignal_prevRecord)

{
	T_EM12DBATHY_RECORD_SIGNAL	*ptrSignal_Record;

	// Pointeur pour avancer sur les variables automatiquement.
	unsigned char		*ptr_cSignalRecord;
	static void 		*ptr_vVarPrevRecord;
	void				*ptr_vVarRecord,
						*ptr_vBidon;

	BOOL		bFlagVarConstant = TRUE,
				bFlagCompare = TRUE;

    int 		iFlag,
				iVarImage,
				iVarSignal,
				iRet = 0,
				iLoop;

	char 		cPaddingChar = 0;

    unsigned short	usTailleEnByte,
					usNbElem;

    off64_t		llPosCrt;

    // Initialisation de la structure d'identification du paquet
 	if (iNbRecord == 0)
	{
 		iInitXMLBinEM12DBATHYRecord("BATHY",
									"BATHY",
									"Bathymetrie - EM12D",
									strARCRecordXML);
	}

    // Lecture des signaux du paquet
	ptrSignal_Record = (T_EM12DBATHY_RECORD_SIGNAL *)malloc(sizeof(T_EM12DBATHY_RECORD_SIGNAL));
	iFlag = fread(ptrSignal_Record, sizeof(T_EM12DBATHY_RECORD_SIGNAL), 1, fpFileData);
	llPosCrt = ftello64(fpFileData);
	if (iFlag == 0)
	{
		printf("%s -- Error in reading : %s\n", __FILE__, G_cFileData);
		return FALSE;
	}
	ptr_cSignalRecord = (unsigned char*)ptrSignal_Record;
	// Sauvegarde des donn�es des attributs.
	if (iNbRecord > 0)
	{
		ptr_vVarPrevRecord = (void *)ptrSignal_prevRecord;
	}

	for (iVarSignal=0; iVarSignal<(int)strARCRecordXML->sNbVarSignal; iVarSignal++)
	{
		ptr_vVarRecord = (void *)ptr_cSignalRecord;

		if (iVarSignal==0)
		{
			usTailleEnByte = 0;
			usNbElem = 0;
		}
		else
		{
			usTailleEnByte = util_usNbOctetTypeVar(strARCRecordXML->strTabSignalXML[iVarSignal-1].cType);
			usNbElem = strARCRecordXML->strTabSignalXML[iVarSignal-1].usNbElem;
		}
		// Pointage sur la donn�e suivante.
		ptr_vVarPrevRecord = ptr_vVarPrevRecord + usTailleEnByte*usNbElem;
		// Traitement particulier des cha�nes : s�par�es par une virgule superflue.
		if (!strcmp(strARCRecordXML->strTabSignalXML[iVarSignal].cType, "char"))
		{
			// R�cup�ration du nb d'octets courants.
			usNbElem = strARCRecordXML->strTabSignalXML[iVarSignal].usNbElem;
			// Caract�re de fin de cha�ne forc� en remplacement de la virgule de s�paration.
			ptr_cSignalRecord[usNbElem-1] 	= '\0';
		}
		// On ne fait pas le test de constance au 1er enregistrement ou si le signal
		// est d�j� vue comme variable.
		if (iNbRecord > 0 && strARCRecordXML->strTabSignalXML[iVarSignal].bFlagConstant == TRUE)
		{
			bFlagVarConstant = strARCRecordXML->strTabSignalXML[iVarSignal].bFlagConstant;
			if (G_iFlagEndian == 0) // On inverse en Big Endian.
			{
				// Observation de la taille propre � la variable.
				usTailleEnByte = util_usNbOctetTypeVar(strARCRecordXML->strTabSignalXML[iVarSignal].cType);
				usNbElem = strARCRecordXML->strTabSignalXML[iVarSignal].usNbElem;
				if (usTailleEnByte == 2) // Pour des (u)short, (u)int, (u)long.
					ptr_vVarPrevRecord = (void *)util_cBswap_16(ptr_vVarPrevRecord);
				else if (usTailleEnByte == 4)
					ptr_vVarPrevRecord = (void *)util_cBswap_32(ptr_vVarPrevRecord);
				else if (usTailleEnByte == 8)
					ptr_vVarPrevRecord = (void *)util_cBswap_64(ptr_vVarPrevRecord);
			}
			bFlagCompare = util_bComparePrevAndCurrentVal(strARCRecordXML->strTabSignalXML[iVarSignal].cType,
												ptr_vVarPrevRecord,
												ptr_vVarRecord,
											   strARCRecordXML->strTabSignalXML[iVarSignal].usNbElem);
			bFlagVarConstant = bFlagVarConstant && bFlagCompare;
			strARCRecordXML->strTabSignalXML[iVarSignal].bFlagConstant = bFlagVarConstant;
		}


		// Pr�paration de l'�criture de la variable.
		usTailleEnByte = strARCRecordXML->strTabSignalXML[iVarSignal].usSize;
		usNbElem =  strARCRecordXML->strTabSignalXML[iVarSignal].usNbElem;
		if (G_iFlagEndian == 0) // On inverse en Big Endian.
		{
			if (usTailleEnByte == 2) // Pour des (u)short, (u)int, (u)long.
				ptr_cSignalRecord = (unsigned char*)util_cBswap_16((char*)ptr_cSignalRecord);
			else if (usTailleEnByte == 4)
				ptr_cSignalRecord = (unsigned char*)util_cBswap_32((char*)ptr_cSignalRecord);
			else if (usTailleEnByte == 8)
				ptr_cSignalRecord = (unsigned char*)util_cBswap_64((char*)ptr_cSignalRecord);
		}
		fwrite(ptr_cSignalRecord, usTailleEnByte, usNbElem, strARCRecordXML->strTabSignalXML[iVarSignal].ptr_fpBin);
				strARCRecordXML->strTabSignalXML[iVarSignal].ptr_vPrevVal = ptr_cSignalRecord;
		ptr_cSignalRecord += usTailleEnByte*usNbElem;
	}
	// Recopie en zone tampon des valeurs courantes.
	memmove(ptrSignal_prevRecord, ptrSignal_Record, sizeof(T_EM12DBATHY_RECORD_SIGNAL));

	// Avancement de l'index de lecture jusqu'au 1er caract�re non-nul.
    do
    {
    	cPaddingChar = fgetc(fpFileData);
    } while (cPaddingChar == 0);
	llPosCrt = ftello64(fpFileData);

	// Retour en arri�re d'un caract�re.
	fseeko64(fpFileData,llPosCrt-1,SEEK_SET);

	// --------------------------------------------
 	// Lecture unitaire des variables de type Image
	for (iLoop=0; iLoop< *ptrSignal_Record->NbBeams; iLoop++)
	{
		// Lecture unitaire des variables.
		for (iVarImage=0; iVarImage< strARCRecordXML->sNbVarImage; iVarImage++)
		{
			usTailleEnByte = strARCRecordXML->strTabImageXML[iVarImage].usSize;
			usNbElem = strARCRecordXML->strTabImageXML[iVarImage].usNbElem;
			ptr_vBidon = (void *)malloc((usTailleEnByte*usNbElem)*sizeof(char));
			iFlag = fread(ptr_vBidon, usTailleEnByte, usNbElem, fpFileData);
			if (G_iFlagEndian == 0) // On inverse en Big Endian.
			{
				if (usTailleEnByte == 2) // Pour des (u)short, (u)int, (u)long.
					ptr_vBidon = (void *)util_cBswap_16(ptr_vBidon);
				else if (usTailleEnByte == 4)
					ptr_vBidon = (void *)util_cBswap_32(ptr_vBidon);
				else if (usTailleEnByte == 8)
					ptr_vBidon = (void *)util_cBswap_64(ptr_vBidon);
			}
			iFlag = fwrite(ptr_vBidon, usTailleEnByte, usNbElem, strARCRecordXML->strTabImageXML[iVarImage].ptr_fpBin);
			if (iLoop > 0 && strARCRecordXML->strTabImageXML[iVarImage].bFlagConstant == TRUE)
			{
				bFlagCompare = util_bComparePrevAndCurrentVal(strARCRecordXML->strTabImageXML[iVarImage].cType,
													strARCRecordXML->strTabImageXML[iVarImage].ptr_vPrevVal,
													ptr_vBidon,
													usNbElem);
				strARCRecordXML->strTabImageXML[iVarImage].bFlagConstant = bFlagCompare && strARCRecordXML->strTabImageXML[iVarImage].bFlagConstant;
			}
			if (iNbRecord == 0)
			{
				strARCRecordXML->strTabImageXML[iVarImage].ptr_vPrevVal = malloc(usTailleEnByte*usNbElem*sizeof(char));
			}
			memmove(strARCRecordXML->strTabImageXML[iVarImage].ptr_vPrevVal, ptr_vBidon, usTailleEnByte*usNbElem);

			free(ptr_vBidon);

		} // Fin de la boucles sur les variables.
	}

	free(ptrSignal_Record);
	llPosCrt = ftello64(fpFileData);

	iNbRecord++;

	return iRet;

} //iProcessEM12DBATHYREcord


//----------------------------------------------------------------
// Fonction :	iInitXMLBinEM12DBATHYRecord
// Objet 	:	Initialisation de la structure �quivalent au paquet
//				Capteurs de Navigation pour la manipulation des fichiers bin et XML.
// Modification : 15/10/08
// Auteur 	: GLU
//----------------------------------------------------------------
int iInitXMLBinEM12DBATHYRecord(char *cTitleDatagram,
								char *cNomDatagram,
								char *cComments,
								T_ARCRECORDXML *strARCRecordXML)
{

	char cTabNomVarSignal[NBSIGNAL][30] = {
			"BeginMsg",
			"Date",
			"Heure",
			"TypeRecord",
			"Reserve1",
			"PingCounter",
			"Mode",
			"NbBeams",
			"Draught",
			"Heading",
			"Roll",
			"Pitch",
			"Heave",
			"SoundSpeed"};

	char cTabNomTypeSignal[NBSIGNAL][20] = {
			"char",
			"char",
			"char",
			"char",
			"u8",
			"unsigned short",
			"u8",
			"u8",
			"short",
			"short",
			"short",
			"short",
			"short",
			"unsigned short"};


	// Taille des champs (sans compter le caractere de s�paration = ',')
	float 	usTabNbElemSignal[NBSIGNAL] = {
			7,
			9,
			13,
			6,
			1,
			1,
			1,
			1,
			1,
			1,
			1,
			1,
			1,
			1
	};

	// ScaleFactor
	float 	fTabSFSignal[NBSIGNAL] = {
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			0.002,
			0.1,
			0.01,
			0.01,
			0.01,
			0.1,
	};

	// AddOffset
	float 	fTabAOSignal[NBSIGNAL] = {
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
	};

	char cTabNomVarImage[NBIMAGE][30] = {
			"Depth",
			"AcrossDist",
			"AlongDist",
			"Duration",
			"Reflectivity",
			"BeamQualityFactor",
			"Reserve"
	};

	char cTabNomTypeImage[NBIMAGE][20] = {
			"unsigned short",
			"unsigned short",
			"unsigned short",
			"unsigned short",
			"u8",
			"u8",
			"unsigned short"};

	unsigned short usTabNbElemImage[NBIMAGE] = {
			1,
			1,
			1,
			1,
			1,
			1,
			1
	};

	// ScaleFactor
	float 	fTabSFImage[NBIMAGE] = {
			1.0,
			1.0,
			1.0,
			1.0,
			0.5,
			1.0,
			1.0,
	};

	// AddOffset
	float 	fTabAOImage[NBIMAGE] = {
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0
	};

	char	*ptr_cRepertoire,
			cNomFicPathBin[300];

    int 	iSizeRep,
			iRet,
			iLen,
			iVarImage,
			iVarSignal;

    unsigned short 	usTaille;

	// Initialisation des structures de pilotage des fichiers
	// XML et Bin .
	strARCRecordXML->sNbVarSignal 	 = NBSIGNAL;
	strARCRecordXML->sNbVarImage 	 = NBIMAGE;
	strARCRecordXML->sNbVarImage2 	 = 0;	// On r�p�te le bloc de variables.
    strARCRecordXML->strTabSignalXML = (T_ARCVARXML *)malloc(strARCRecordXML->sNbVarSignal*sizeof(T_ARCVARXML));
    strARCRecordXML->strTabImageXML  = (T_ARCVARXML *)malloc(strARCRecordXML->sNbVarImage*sizeof(T_ARCVARXML));
    strARCRecordXML->strTabImage2XML = (T_ARCVARXML *)malloc(strARCRecordXML->sNbVarImage2*sizeof(T_ARCVARXML));

	iLen = strlen(cNomDatagram)+1;
	strARCRecordXML->ptr_cNomHeader = malloc(iLen*sizeof(char));
	sprintf(strARCRecordXML->ptr_cNomHeader, "%s", cNomDatagram);

	iLen = strlen(cTitleDatagram)+1;
	strARCRecordXML->ptr_cLabelHeader = malloc(iLen*sizeof(char));
	sprintf(strARCRecordXML->ptr_cLabelHeader, "%s", cTitleDatagram);

	iLen = strlen(cComments)+1;
	strARCRecordXML->ptr_cCommentsHeader = malloc(iLen*sizeof(char));
	sprintf(strARCRecordXML->ptr_cCommentsHeader, "%s", cComments);

	// Remplissage du nom de la variable d'indexation des tables.
	iLen = strlen(cTabNomVarSignal[7])+1;
	strARCRecordXML->ptr_cNomIndexTable = malloc(iLen*sizeof(char));
	sprintf(strARCRecordXML->ptr_cNomIndexTable, "%s", cTabNomVarSignal[7]);

	// Remplissage du nom de la variable d'indexation des tables.
	strARCRecordXML->ptr_cNomIndexTable2 = NULL;

	// Affectation et cr�ation du repertoire du Datagramme.
	iSizeRep = ( strlen(G_cRepData) + strlen( cNomDatagram));
	// Ajout d'une marge au cas o� certains caract�res soient doubl�s soient doubl�s.
	ptr_cRepertoire = malloc( (iSizeRep + 1 + 30 + 4)*sizeof(char));
	iRet = util_iCreateDirDatagram(G_cRepData,
								strARCRecordXML->ptr_cNomHeader,
								ptr_cRepertoire);

	// Initialisation g�n�rale des variables de type Image
	for (iVarSignal=0; iVarSignal<strARCRecordXML->sNbVarSignal; iVarSignal++) {
	   strARCRecordXML->strTabSignalXML[iVarSignal].ptr_fpBin = NULL;
	   strARCRecordXML->strTabSignalXML[iVarSignal].bFlagConstant = TRUE;
	   strARCRecordXML->strTabSignalXML[iVarSignal].usNbElem = usTabNbElemSignal[iVarSignal];
	   sprintf(strARCRecordXML->strTabSignalXML[iVarSignal].cNom, "%s", cTabNomVarSignal[iVarSignal]);
	   sprintf(strARCRecordXML->strTabSignalXML[iVarSignal].cType, "%s", cTabNomTypeSignal[iVarSignal]);
	   sprintf(strARCRecordXML->strTabSignalXML[iVarSignal].cTag, "%s", "TODO");
	   sprintf(strARCRecordXML->strTabSignalXML[iVarSignal].cUnit, "%s", "TODO");
	   strARCRecordXML->strTabSignalXML[iVarSignal].fScaleFactor = fTabSFSignal[iVarSignal];
	   strARCRecordXML->strTabSignalXML[iVarSignal].fAddOffset = fTabAOSignal[iVarSignal];
	   usTaille = util_usNbOctetTypeVar(cTabNomTypeSignal[iVarSignal]);
	   strARCRecordXML->strTabSignalXML[iVarSignal].usSize = usTaille;
	   iLen = strlen(strARCRecordXML->strTabSignalXML[iVarSignal].cNom);
	   // Ajout d'une marge au cas o� certains caract�res soient doubl�s soient doubl�s.
	   sprintf(cNomFicPathBin, "%s\\%s%s", ptr_cRepertoire, strARCRecordXML->strTabSignalXML[iVarSignal].cNom, ".bin");
	   sprintf(strARCRecordXML->strTabSignalXML[iVarSignal].cNomPathFileBin, "%s", cNomFicPathBin);
	   strARCRecordXML->strTabSignalXML[iVarSignal].ptr_fpBin = fopen(cNomFicPathBin, "w+b");
	   if (!strARCRecordXML->strTabSignalXML[iVarSignal].ptr_fpBin)
	   {
		   printf("%s -- Error in Image creation  : %s\n", __FILE__, cNomFicPathBin);
		   return EXIT_FAILURE;
	   }
	}

	// Initialisation g�n�rale des variables de type Image.
	// Le nb d'appareils de syst�mes suppl�mentaires d�pend de la variable lue au pr�alable dans le fichier.
	// Le nb de variables = nb de variables la structure X nb structures (=syst�mes suppl�mentaires).
	for (iVarImage=0; iVarImage<strARCRecordXML->sNbVarImage; iVarImage++) {
	   strARCRecordXML->strTabImageXML[iVarImage].ptr_fpBin = NULL;
	   strARCRecordXML->strTabImageXML[iVarImage].bFlagConstant = TRUE;
	   strARCRecordXML->strTabImageXML[iVarImage].usNbElem = usTabNbElemImage[iVarImage];
	   sprintf(strARCRecordXML->strTabImageXML[iVarImage].cNom, "%s", cTabNomVarImage[iVarImage]);
	   sprintf(strARCRecordXML->strTabImageXML[iVarImage].cType, "%s", cTabNomTypeImage[iVarImage]);
	   sprintf(strARCRecordXML->strTabImageXML[iVarImage].cTag, "%s", "TODO");
	   sprintf(strARCRecordXML->strTabImageXML[iVarImage].cUnit, "%s", "TODO");
	   strARCRecordXML->strTabImageXML[iVarImage].fScaleFactor = fTabSFImage[iVarImage];
	   strARCRecordXML->strTabImageXML[iVarImage].fAddOffset = fTabAOImage[iVarImage];
	   usTaille = util_usNbOctetTypeVar(cTabNomTypeImage[iVarImage]);
	   strARCRecordXML->strTabImageXML[iVarImage].usSize = usTaille;
	   iLen = strlen(strARCRecordXML->strTabImageXML[iVarImage].cNom);
	   // Ajout d'une marge au cas o� certains caract�res soient doubl�s soient doubl�s.
	   sprintf(cNomFicPathBin, "%s\\%s%s", ptr_cRepertoire, strARCRecordXML->strTabImageXML[iVarImage].cNom, ".bin");
	   sprintf(strARCRecordXML->strTabImageXML[iVarImage].cNomPathFileBin, "%s", cNomFicPathBin);
	   strARCRecordXML->strTabImageXML[iVarImage].ptr_fpBin = fopen(cNomFicPathBin, "w+b");
	   if (!strARCRecordXML->strTabImageXML[iVarImage].ptr_fpBin)
	   {
		   printf("%s -- Error in Image creation  : %s\n", __FILE__, cNomFicPathBin);
		   return EXIT_FAILURE;
	   }
	}

	free(ptr_cRepertoire);
	return EXIT_SUCCESS;

} // iInitXMLBinEM12DBATHYRecord

#pragma pack()
