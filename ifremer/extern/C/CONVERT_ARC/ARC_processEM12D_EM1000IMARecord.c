/*
 * ARC_processEM1000IMAREcord.c
 *
 *  Created on: 13 oct. 2008
 *      Author: rgallou
 */
#pragma pack(1)

#include <sys/types.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "ARC_Datagrams.h"
#include "ARC_Utilities.h"
#include "Generic_Utilities.h"
#include "ARC_processEM12D_EM1000IMARecord.h"
#include "convertFiles_ARC.h"

#define NBSIGNAL 	9
#define NBIMAGE 	4
#define NBIMAGE2 	1

//----------------------------------------------------------------
// Fonction :	iProcessEM1000IMAREcord
// Objet 	:	Traitement du paquet de capteurs de navigation
// Modif.	: 	03/11/08
// Auteur 	:	GLU
//----------------------------------------------------------------
int iProcessEM12D_EM1000IMARecord(	FILE *fpFileData,
								int iNbRecord,
								int *iSizeDatagram,
								T_ARCRECORDXML *strARCRecordXML,
								T_EM12D_EM1000IMA_RECORD_SIGNAL *ptrSignal_prevRecord)

{
	T_EM12D_EM1000IMA_RECORD_SIGNAL	*ptrSignal_Record;

	// Pointeur pour avancer sur les variables automatiquement.
	unsigned char		*ptr_cSignalRecord;
	static void 		*ptr_vVarPrevRecord;
	void				*ptr_vVarRecord,
						*ptr_vBidon;

	BOOL		bFlagVarConstant = TRUE,
				bFlagCompare = TRUE;

    int 		iFlag,
				iVarImage,
				iVarSignal,
				iRet = 0,
				iLoop,
				iNbSamples = 0,
				iChar = 0,
				iPadding;

    unsigned short	usTailleEnByte,
					usNbElem;

    off64_t		llPosCrt;

    char 		*cValeur;

    // Initialisation de la structure d'identification du paquet
 	if (iNbRecord == 0)
	{
 		iInitXMLBinEM12D_EM1000IMARecord("RawBeamSamples",
										"RawBeamSamples",
										"Imagerie - EM1000",
										strARCRecordXML);
	}

    // Lecture des signaux du paquet
	ptrSignal_Record = (T_EM12D_EM1000IMA_RECORD_SIGNAL *)malloc(sizeof(T_EM12D_EM1000IMA_RECORD_SIGNAL));
	iFlag = fread(ptrSignal_Record, sizeof(T_EM12D_EM1000IMA_RECORD_SIGNAL), 1, fpFileData);
	llPosCrt = ftello64(fpFileData);
	if (iFlag == 0)
	{
		printf("%s -- Error in reading : %s\n", __FILE__, G_cFileData);
		return FALSE;
	}
	ptr_cSignalRecord = (unsigned char*)ptrSignal_Record;
	// Sauvegarde des donn�es des attributs.
	if (iNbRecord > 0)
	{
		ptr_vVarPrevRecord = (void *)ptrSignal_prevRecord;
	}

	for (iVarSignal=0; iVarSignal<(int)strARCRecordXML->sNbVarSignal; iVarSignal++)
	{
		ptr_vVarRecord = (void *)ptr_cSignalRecord;

		if (iVarSignal==0)
		{
			usTailleEnByte = 0;
			usNbElem = 0;
		}
		else
		{
			usTailleEnByte = util_usNbOctetTypeVar(strARCRecordXML->strTabSignalXML[iVarSignal-1].cType);
			usNbElem = strARCRecordXML->strTabSignalXML[iVarSignal-1].usNbElem;
		}
		// Pointage sur la donn�e suivante.
		ptr_vVarPrevRecord = ptr_vVarPrevRecord + usTailleEnByte*usNbElem;
		// Traitement particulier des cha�nes : s�par�es par une virgule superflue.
		if (!strcmp(strARCRecordXML->strTabSignalXML[iVarSignal].cType, "char"))
		{
			// R�cup�ration du nb d'octets courants.
			usNbElem = strARCRecordXML->strTabSignalXML[iVarSignal].usNbElem;
			// Caract�re de fin de cha�ne forc� en remplacement de la virgule de s�paration.
			ptr_cSignalRecord[usNbElem-1] 	= '\0';
		}
		// On ne fait pas le test de constance au 1er enregistrement ou si le signal
		// est d�j� vue comme variable.
		if (iNbRecord > 0 && strARCRecordXML->strTabSignalXML[iVarSignal].bFlagConstant == TRUE)
		{
			bFlagVarConstant = strARCRecordXML->strTabSignalXML[iVarSignal].bFlagConstant;
			if (G_iFlagEndian == 0) // On inverse en Big Endian.
			{
				// Observation de la taille propre � la variable.
				usTailleEnByte = util_usNbOctetTypeVar(strARCRecordXML->strTabSignalXML[iVarSignal].cType);
				usNbElem = strARCRecordXML->strTabSignalXML[iVarSignal].usNbElem;
				if (usTailleEnByte == 2) // Pour des (u)short, (u)int, (u)long.
					ptr_vVarPrevRecord = (void *)util_cBswap_16(ptr_vVarPrevRecord);
				else if (usTailleEnByte == 4)
					ptr_vVarPrevRecord = (void *)util_cBswap_32(ptr_vVarPrevRecord);
				else if (usTailleEnByte == 8)
					ptr_vVarPrevRecord = (void *)util_cBswap_64(ptr_vVarPrevRecord);
			}
			bFlagCompare = util_bComparePrevAndCurrentVal(strARCRecordXML->strTabSignalXML[iVarSignal].cType,
												ptr_vVarPrevRecord,
												ptr_vVarRecord,
											   strARCRecordXML->strTabSignalXML[iVarSignal].usNbElem);
			bFlagVarConstant = bFlagVarConstant && bFlagCompare;
			strARCRecordXML->strTabSignalXML[iVarSignal].bFlagConstant = bFlagVarConstant;
		}


		// Pr�paration de l'�criture de la variable.
		usTailleEnByte = strARCRecordXML->strTabSignalXML[iVarSignal].usSize;
		usNbElem =  strARCRecordXML->strTabSignalXML[iVarSignal].usNbElem;
		if (G_iFlagEndian == 0) // On inverse en Big Endian.
		{
			if (usTailleEnByte == 2) // Pour des (u)short, (u)int, (u)long.
				ptr_cSignalRecord = (unsigned char*)util_cBswap_16((char*)ptr_cSignalRecord);
			else if (usTailleEnByte == 4)
				ptr_cSignalRecord = (unsigned char*)util_cBswap_32((char*)ptr_cSignalRecord);
			else if (usTailleEnByte == 8)
				ptr_cSignalRecord = (unsigned char*)util_cBswap_64((char*)ptr_cSignalRecord);
		}
		fwrite(ptr_cSignalRecord, usTailleEnByte, usNbElem, strARCRecordXML->strTabSignalXML[iVarSignal].ptr_fpBin);
				strARCRecordXML->strTabSignalXML[iVarSignal].ptr_vPrevVal = ptr_cSignalRecord;
		ptr_cSignalRecord += usTailleEnByte*usNbElem;
	}
	// Recopie en zone tampon des valeurs courantes.
	memmove(ptrSignal_prevRecord, ptrSignal_Record, sizeof(T_EM12D_EM1000IMA_RECORD_SIGNAL));

	llPosCrt = ftello64(fpFileData);

	// --------------------------------------------
 	// Lecture unitaire des variables de type Image
	for (iLoop=0; iLoop< *ptrSignal_Record->NbBeams; iLoop++)
	{
		// Lecture unitaire des variables.
		for (iVarImage=0; iVarImage< strARCRecordXML->sNbVarImage; iVarImage++)
		{
			usTailleEnByte = strARCRecordXML->strTabImageXML[iVarImage].usSize;
			usNbElem = strARCRecordXML->strTabImageXML[iVarImage].usNbElem;
			ptr_vBidon = (void *)malloc((usTailleEnByte*usNbElem)*sizeof(char));
			iFlag = fread(ptr_vBidon, usTailleEnByte, usNbElem, fpFileData);
			if (G_iFlagEndian == 0) // On inverse en Big Endian.
			{
				if (usTailleEnByte == 2) // Pour des (u)short, (u)int, (u)long.
					ptr_vBidon = (void *)util_cBswap_16(ptr_vBidon);
				else if (usTailleEnByte == 4)
					ptr_vBidon = (void *)util_cBswap_32(ptr_vBidon);
				else if (usTailleEnByte == 8)
					ptr_vBidon = (void *)util_cBswap_64(ptr_vBidon);
			}
			iFlag = fwrite(ptr_vBidon, usTailleEnByte, usNbElem, strARCRecordXML->strTabImageXML[iVarImage].ptr_fpBin);
			if (iLoop > 0 && strARCRecordXML->strTabImageXML[iVarImage].bFlagConstant == TRUE)
			{
				bFlagCompare = util_bComparePrevAndCurrentVal(strARCRecordXML->strTabImageXML[iVarImage].cType,
													strARCRecordXML->strTabImageXML[iVarImage].ptr_vPrevVal,
													ptr_vBidon,
													usNbElem);
				strARCRecordXML->strTabImageXML[iVarImage].bFlagConstant = bFlagCompare && strARCRecordXML->strTabImageXML[iVarImage].bFlagConstant;
			}
			if (iNbRecord == 0)
			{
				strARCRecordXML->strTabImageXML[iVarImage].ptr_vPrevVal = malloc(usTailleEnByte*usNbElem*sizeof(char));
			}
			// R�cup�ration du nombre d'�chantillons dans ce paquet de r�ception.
			if (iVarImage == 2)
			{
				cValeur = malloc(256*sizeof(char));
				iRet = util_cValVarConstant(strARCRecordXML->strTabImageXML[iVarImage].cType,
						ptr_vBidon,
						usNbElem,
						cValeur);
				iNbSamples = iNbSamples + atoi(cValeur);
				free(cValeur);
			}
			memmove(strARCRecordXML->strTabImageXML[iVarImage].ptr_vPrevVal, ptr_vBidon, usTailleEnByte*usNbElem);

			free(ptr_vBidon);

		} // Fin de la boucles sur les variables.

	}

	// --------------------------------------------
	// Lecture en bloc des variables de type Image 2.
	for (iVarImage=0; iVarImage< strARCRecordXML->sNbVarImage2; iVarImage++)
	{
		usTailleEnByte 	= strARCRecordXML->strTabImage2XML[iVarImage].usSize;
		usNbElem 		= iNbSamples * strARCRecordXML->strTabImage2XML[iVarImage].usNbElem ;
		ptr_vBidon 		= (void *)malloc((usTailleEnByte*usNbElem)*sizeof(char));
		iFlag = fread(ptr_vBidon, usTailleEnByte, usNbElem, fpFileData);
		iFlag = fwrite(ptr_vBidon, usTailleEnByte, usNbElem, strARCRecordXML->strTabImage2XML[iVarImage].ptr_fpBin);

		free(ptr_vBidon);

	} // Fin de la boucles sur les variables.

    // Calcul de la taille du datagramme.
    // Recherche du 1er octet non �gal � 0.
    iChar       = 0;
    iPadding    = 0;
    // Recherche de la lecture du Line Feed
	while (iChar != 10 && iChar != EOF)
	{
		iChar = fgetc(fpFileData);
		iPadding++;
	}
    // Suppression des octets [,\r\n] en trop � la lecture.
	iPadding = iPadding - 3;
    // Calcul de la taille du datagramme courant.
	*iSizeDatagram = 45 + ((int)*ptrSignal_Record->NbBeams *6) + iNbSamples + iPadding;

	iNbRecord++;

	free(ptrSignal_Record);

	return iRet;

} //iProcessEM1000IMAREcord


//----------------------------------------------------------------
// Fonction :	iInitXMLBinEM12D_EM1000IMARecord
// Objet 	:	Initialisation de la structure �quivalent au paquet
//				Capteurs de Navigation pour la manipulation des fichiers bin et XML.
// Modification : 15/10/08
// Auteur 	: GLU
//----------------------------------------------------------------
int iInitXMLBinEM12D_EM1000IMARecord(char *cTitleDatagram,
								char *cNomDatagram,
								char *cComments,
								T_ARCRECORDXML *strARCRecordXML)
{


	char cTabNomVarSignal[NBSIGNAL][30] = {
			"BeginMsg",
			"Date",
			"Heure",
			"TypeRecord",
			"Reserve1",
			"PingCounter",
			"SoundSpeed",
			"Mode",
			"NbBeams"
	};

	char cTabNomTypeSignal[NBSIGNAL][20] = {
			"char",
			"char",
			"char",
			"char",
			"u8",
			"unsigned short",
			"unsigned short",
			"u8",
			"u8"
	};


	// Taille des champs (sans compter le caractere de s�paration = ',')
	float 	usTabNbElemSignal[NBSIGNAL] = {
			7,
			9,
			13,
			6,
			1,
			1,
			1,
			1,
			1};

	// ScaleFactor
	float 	fTabSFSignal[NBSIGNAL] = {
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0
	};

	// AddOffset
	float 	fTabAOSignal[NBSIGNAL] = {
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0
	};

	char cTabNomVarImage[NBIMAGE][30] = {
			"BeamNumber",
			"Frequence",
			"NbSamples",
			"StartSampleNumber"
	};

	char cTabNomTypeImage[NBIMAGE][20] = {
			"u8",
			"u8",
			"unsigned short",
			"unsigned short"
	};

	unsigned short usTabNbElemImage[NBIMAGE] = {
			1,
			1,
			1,
			1
	};

	// ScaleFactor
	float 	fTabSFImage[NBIMAGE] = {
			1.0,
			1.0,
			1.0,
			1.0
	};

	// AddOffset
	float 	fTabAOImage[NBIMAGE] = {
			0.0,
			0.0,
			0.0,
			0.0
	};

	// Tableau de variables de type Image 2.
	char cTabNomVarImage2[NBIMAGE2][20] = {
			"Samples"
	};

	char cTabNomTypeImage2[NBIMAGE2][20] = {
			"u8"
	};

	unsigned short usTabNbElemImage2[NBIMAGE2] = {
			1
	};

	// ScaleFactor
	float 	fTabSFImage2[NBIMAGE2] = {
			1.0
	};

	// AddOffset
	float 	fTabAOImage2[NBIMAGE2] = {
			0.0
	};

	char	*ptr_cRepertoire,
			cNomFicPathBin[300];

    int 	iSizeRep,
			iRet,
			iLen,
			iVarImage,
			iVarSignal;

    unsigned short 	usTaille;

	// Initialisation des structures de pilotage des fichiers
	// XML et Bin .
	strARCRecordXML->sNbVarSignal 	 = NBSIGNAL;
	strARCRecordXML->sNbVarImage 	 = NBIMAGE;
	strARCRecordXML->sNbVarImage2 	 = NBIMAGE2;	// On r�p�te le bloc de variables.
    strARCRecordXML->strTabSignalXML = (T_ARCVARXML *)malloc(strARCRecordXML->sNbVarSignal*sizeof(T_ARCVARXML));
    strARCRecordXML->strTabImageXML  = (T_ARCVARXML *)malloc(strARCRecordXML->sNbVarImage*sizeof(T_ARCVARXML));
    strARCRecordXML->strTabImage2XML = (T_ARCVARXML *)malloc(strARCRecordXML->sNbVarImage2*sizeof(T_ARCVARXML));

	iLen = strlen(cNomDatagram)+1;
	strARCRecordXML->ptr_cNomHeader = malloc(iLen*sizeof(char));
	sprintf(strARCRecordXML->ptr_cNomHeader, "%s", cNomDatagram);

	iLen = strlen(cTitleDatagram)+1;
	strARCRecordXML->ptr_cLabelHeader = malloc(iLen*sizeof(char));
	sprintf(strARCRecordXML->ptr_cLabelHeader, "%s", cTitleDatagram);

	iLen = strlen(cComments)+1;
	strARCRecordXML->ptr_cCommentsHeader = malloc(iLen*sizeof(char));
	sprintf(strARCRecordXML->ptr_cCommentsHeader, "%s", cComments);

	// Remplissage du nom de la variable d'indexation des tables.
	iLen = strlen(cTabNomVarSignal[8])+1;
	strARCRecordXML->ptr_cNomIndexTable = malloc(iLen*sizeof(char));
	sprintf(strARCRecordXML->ptr_cNomIndexTable, "%s", cTabNomVarSignal[8]);

	// Remplissage du nom de la variable d'indexation des tables.
	iLen = strlen(cTabNomVarSignal[8])+ strlen(cTabNomVarImage[2]) +1;
	strARCRecordXML->ptr_cNomIndexTable2 = malloc(iLen*sizeof(char));
	sprintf(strARCRecordXML->ptr_cNomIndexTable2, "%s,%s", cTabNomVarSignal[8],cTabNomVarImage[2]);

	// Affectation et cr�ation du repertoire du Datagramme.
	iSizeRep = ( strlen(G_cRepData) + strlen( cNomDatagram));
	// Ajout d'une marge au cas o� certains caract�res soient doubl�s soient doubl�s.
	ptr_cRepertoire = malloc( (iSizeRep + 1 + 30 + 4)*sizeof(char));
	iRet = util_iCreateDirDatagram(G_cRepData,
								strARCRecordXML->ptr_cNomHeader,
								ptr_cRepertoire);

	// Initialisation g�n�rale des variables de type Image
	for (iVarSignal=0; iVarSignal<strARCRecordXML->sNbVarSignal; iVarSignal++) {
	   strARCRecordXML->strTabSignalXML[iVarSignal].ptr_fpBin = NULL;
	   strARCRecordXML->strTabSignalXML[iVarSignal].bFlagConstant = TRUE;
	   strARCRecordXML->strTabSignalXML[iVarSignal].usNbElem = usTabNbElemSignal[iVarSignal];
	   sprintf(strARCRecordXML->strTabSignalXML[iVarSignal].cNom, "%s", cTabNomVarSignal[iVarSignal]);
	   sprintf(strARCRecordXML->strTabSignalXML[iVarSignal].cType, "%s", cTabNomTypeSignal[iVarSignal]);
	   sprintf(strARCRecordXML->strTabSignalXML[iVarSignal].cTag, "%s", "TODO");
	   sprintf(strARCRecordXML->strTabSignalXML[iVarSignal].cUnit, "%s", "TODO");
	   strARCRecordXML->strTabSignalXML[iVarSignal].fScaleFactor = fTabSFSignal[iVarSignal];
	   strARCRecordXML->strTabSignalXML[iVarSignal].fAddOffset = fTabAOSignal[iVarSignal];
	   usTaille = util_usNbOctetTypeVar(cTabNomTypeSignal[iVarSignal]);
	   strARCRecordXML->strTabSignalXML[iVarSignal].usSize = usTaille;
	   iLen = strlen(strARCRecordXML->strTabSignalXML[iVarSignal].cNom);
	   // Ajout d'une marge au cas o� certains caract�res soient doubl�s soient doubl�s.
	   sprintf(cNomFicPathBin, "%s\\%s%s", ptr_cRepertoire, strARCRecordXML->strTabSignalXML[iVarSignal].cNom, ".bin");
	   sprintf(strARCRecordXML->strTabSignalXML[iVarSignal].cNomPathFileBin, "%s", cNomFicPathBin);
	   strARCRecordXML->strTabSignalXML[iVarSignal].ptr_fpBin = fopen(cNomFicPathBin, "w+b");
	   if (!strARCRecordXML->strTabSignalXML[iVarSignal].ptr_fpBin)
	   {
		   printf("%s -- Error in Image creation  : %s\n", __FILE__, cNomFicPathBin);
		   return EXIT_FAILURE;
	   }
	}

	// Initialisation g�n�rale des variables de type Image.
	// Le nb d'appareils de syst�mes suppl�mentaires d�pend de la variable lue au pr�alable dans le fichier.
	// Le nb de variables = nb de variables la structure X nb structures (=syst�mes suppl�mentaires).
	for (iVarImage=0; iVarImage<strARCRecordXML->sNbVarImage; iVarImage++) {
	   strARCRecordXML->strTabImageXML[iVarImage].ptr_fpBin = NULL;
	   strARCRecordXML->strTabImageXML[iVarImage].bFlagConstant = TRUE;
	   strARCRecordXML->strTabImageXML[iVarImage].usNbElem = usTabNbElemImage[iVarImage];
	   sprintf(strARCRecordXML->strTabImageXML[iVarImage].cNom, "%s", cTabNomVarImage[iVarImage]);
	   sprintf(strARCRecordXML->strTabImageXML[iVarImage].cType, "%s", cTabNomTypeImage[iVarImage]);
	   sprintf(strARCRecordXML->strTabImageXML[iVarImage].cTag, "%s", "TODO");
	   sprintf(strARCRecordXML->strTabImageXML[iVarImage].cUnit, "%s", "TODO");
	   strARCRecordXML->strTabImageXML[iVarImage].fScaleFactor = fTabSFImage[iVarImage];
	   strARCRecordXML->strTabImageXML[iVarImage].fAddOffset = fTabAOImage[iVarImage];
	   usTaille = util_usNbOctetTypeVar(cTabNomTypeImage[iVarImage]);
	   strARCRecordXML->strTabImageXML[iVarImage].usSize = usTaille;
	   iLen = strlen(strARCRecordXML->strTabImageXML[iVarImage].cNom);
	   // Ajout d'une marge au cas o� certains caract�res soient doubl�s soient doubl�s.
	   sprintf(cNomFicPathBin, "%s\\%s%s", ptr_cRepertoire, strARCRecordXML->strTabImageXML[iVarImage].cNom, ".bin");
	   sprintf(strARCRecordXML->strTabImageXML[iVarImage].cNomPathFileBin, "%s", cNomFicPathBin);
	   strARCRecordXML->strTabImageXML[iVarImage].ptr_fpBin = fopen(cNomFicPathBin, "w+b");
	   if (!strARCRecordXML->strTabImageXML[iVarImage].ptr_fpBin)
	   {
		   printf("%s -- Error in Image creation  : %s\n", __FILE__, cNomFicPathBin);
		   return EXIT_FAILURE;
	   }
	}

	// Initialisation g�n�rale des variables de type Image pour la 2i�me image
	for (iVarImage=0; iVarImage<strARCRecordXML->sNbVarImage2; iVarImage++) {
	   strARCRecordXML->strTabImage2XML[iVarImage].ptr_fpBin = NULL;
	   strARCRecordXML->strTabImage2XML[iVarImage].bFlagConstant = FALSE;	// Forc� car non test�.
	   strARCRecordXML->strTabImage2XML[iVarImage].usNbElem = usTabNbElemImage2[iVarImage];
	   sprintf(strARCRecordXML->strTabImage2XML[iVarImage].cNom, "%s", cTabNomVarImage2[iVarImage]);
	   sprintf(strARCRecordXML->strTabImage2XML[iVarImage].cType, "%s", cTabNomTypeImage2[iVarImage]);
	   sprintf(strARCRecordXML->strTabImage2XML[iVarImage].cTag, "%s", "TODO");
	   sprintf(strARCRecordXML->strTabImage2XML[iVarImage].cUnit, "%s", "TODO");
	   strARCRecordXML->strTabImage2XML[iVarImage].fScaleFactor = fTabSFImage2[iVarImage];
	   strARCRecordXML->strTabImage2XML[iVarImage].fAddOffset = fTabAOImage2[iVarImage];
	   usTaille = util_usNbOctetTypeVar(cTabNomTypeImage2[iVarImage]);
	   strARCRecordXML->strTabImage2XML[iVarImage].usSize = usTaille;
	   iLen = strlen(strARCRecordXML->strTabImage2XML[iVarImage].cNom);
	   // Ajout d'une marge au cas o� certains caract�res soient doubl�s soient doubl�s.
	   sprintf(cNomFicPathBin, "%s\\%s%s", ptr_cRepertoire, strARCRecordXML->strTabImage2XML[iVarImage].cNom, ".bin");
	   sprintf(strARCRecordXML->strTabImage2XML[iVarImage].cNomPathFileBin, "%s", cNomFicPathBin);
	   strARCRecordXML->strTabImage2XML[iVarImage].ptr_fpBin = fopen(cNomFicPathBin, "w+b");
	   if (!strARCRecordXML->strTabImage2XML[iVarImage].ptr_fpBin)
	   {
		   printf("%s -- Error in Image creation  : %s\n", __FILE__, cNomFicPathBin);
		   return EXIT_FAILURE;
	   }
	}


	free(ptr_cRepertoire);
	return EXIT_SUCCESS;

} // iInitXMLBinEM12D_EM1000IMARecord

#pragma pack()
