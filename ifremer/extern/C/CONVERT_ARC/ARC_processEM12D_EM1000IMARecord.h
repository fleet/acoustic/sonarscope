/*
 * ARC_processEM12D_EM1000IMARecord.h
 *
 *  Created on: 21 nov. 2008
 *      Author: rgallou
 */
#pragma pack(1)

#ifndef ARC_PROCESSEM1000IMARECORD_H_
#define ARC_PROCESSEM1000IMARECORD_H_

#include "ARC_DATAGRAMS.H"


// D�finition de la structure EM1000IMA
typedef struct  {
	// On d�crit la taille fixe des chaines comprenant la virgule de s�paration.
	char			BeginMsg[7];	// = '$xxxxx'
	char			Date[9];		// JJ/MM/AA
	char			Heure[13];  	// HH:MM:SS.DDD
	char			TypeRecord[6];		// = IMA
	unsigned char	Reserve1[1];
	unsigned short	NumeroPing;
	unsigned short	SoundSpeed;
	unsigned char	Mode[1];
	unsigned char	NbBeams[1];		// d�crit comme facteur de qualit�.
} T_EM12D_EM1000IMA_RECORD_SIGNAL;

typedef struct  {
	unsigned char	BeamNumber[1];		// Num�ro du faisceau
	unsigned char	Frequence[1];
	unsigned short	NbSamples;
	unsigned short	StartSampleNumber;	// numero echantillon de pied de faisceau.
} T_EM12D_EM1000IMA_RECORD_IMA;

typedef struct  {
	unsigned char	Samples[1];		// Num�ro du faisceau
} T_EM12D_EM1000IMA_RECORD_IMA2;

int iProcessEM12D_EM1000IMARecord(	FILE *fpFileData,
								int iNbRecord,
								int *iSizeDatagram,
								T_ARCRECORDXML *strARCRecordXML,
								T_EM12D_EM1000IMA_RECORD_SIGNAL *ptrIMA_prevRecord);

int iInitXMLBinEM12D_EM1000IMARecord(char *cTitleDatagram,
								char *cNomDatagram,
								char *cComments,
								T_ARCRECORDXML *strARCRecordXML);

#endif /* ARC_PROCESSEM1000IMARECORD_H_ */

#pragma pack()
