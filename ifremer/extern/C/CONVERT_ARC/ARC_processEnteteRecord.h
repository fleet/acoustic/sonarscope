/*
 * ARC_processEnteteRecord.h
 *
 *  Created on: 21 nov. 2008
 *      Author: rgallou
 */
#pragma pack(1)

#ifndef ARC_PROCESSENETETERECORD_H_
#define ARC_PROCESSENETETERECORD_H_

#include "ARC_DATAGRAMS.H"

// Définition de la structure Entete
typedef struct  {
	unsigned char	*BeginMsg;

	unsigned char	*Date;	// JJ/MM/AA
	unsigned char	*Heure;  // HH:MM:SS.DDD

	unsigned char	*TypeRecord;

} T_ENTETE_RECORD_IMAGE;

int iProcessEnteteRecord(FILE *fpFileData,
							int iNbRecord,
							T_ARCRECORDXML *strARCRecordXML,
							T_ENTETE_RECORD_IMAGE *ptrSignal_prevRecord);


int iInitXMLBinEnteteRecord(char *cTitleDatagram,
							char *cNomDatagram,
							char *cComments,
							T_ARCRECORDXML *strARCRecordXML);

#endif // ARC_PROCESSENETETERECORD_H_

#pragma pack()
