/*
 * ARC_processFileIndexRecord.c
 *
 *  Created on: 13 oct. 2008
 *      Author: rgallou
 */
#pragma pack(1)

#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "ARC_Datagrams.h"
#include "ARC_Utilities.h"
#include "Generic_Utilities.h"
#include "ARC_processFileIndexRecord.h"
#include "convertFiles_ARC.h"

#include "Generic_Utilities.h"

#define NB_IMAGE 6
//----------------------------------------------------------------
// Fonction :	iProcessFileIndexRecord
// Objet 	:	Traitement du paquet FileIndex
// Modif.	: 	03/11/08
// Auteur 	:	GLU
//----------------------------------------------------------------
int iProcessFileIndexRecord(FILE *fpFileData,
						int iNbRecord,
						T_ARCRECORDXML *strARCRecordXML,
						T_FILEINDEX_RECORD_IMAGE *ptrImage_prevRecord)
{
	T_FILEINDEX_RECORD_IMAGE	*ptrImage_Record;

	// Pointeur pour avancer sur les variables automatiquement.
	unsigned char		*ptr_cImageRecord;
	static void 		*ptr_vVarPrevRecord;
	void				*ptr_vVarRecord;

	BOOL		bFlagVarConstant = TRUE,
				bFlagCompare = TRUE,
				bFlagCarriageReturn,
				bFlagLineFeed,
				bFlagDebutDeLigne;

    int 		iFlag,
				iVarImage,
				iRetour = 0,
				iSizeImageRecord,
				iSizeDatagram,
				iTypeSondeur,
				iChar;

	off64_t		llPosCrt,
				llPosDebutDatagram;

	// On r�initialise errno
    errno = 0;

    unsigned short	usTailleEnByte,
					usNbElem;

    //char cBuffer[29];

    // Initialisation de la structure d'identification du paquet
 	if (iNbRecord == 0)
	{
 		iInitXMLBinFileIndexRecord("FileIndex",
									"FileIndex",
									"Index File of common data",
									strARCRecordXML);
	}

    // Lecture des signaux du paquet : taille de l'entete -  longueur et position.
 	iSizeImageRecord 	= sizeof(T_FILEINDEX_RECORD_IMAGE) - sizeof(off64_t) - sizeof(int);
	ptrImage_Record 	= (T_FILEINDEX_RECORD_IMAGE *)malloc(sizeof(T_FILEINDEX_RECORD_IMAGE));

	llPosDebutDatagram = ftello64(fpFileData);
	iFlag = fread(ptrImage_Record, sizeof(unsigned char), iSizeImageRecord, fpFileData);

	// Calcul de la taille du datagramme selon :
	// - le type de sondeur,
	// - le type de datagramme
	// - le type de l'entete.
 	if (!strncmp(ptrImage_Record->BeginMsg, "$PIFM", 5))
 	{
	    iTypeSondeur = 300;
 		// Sondeur EM300
 		llPosCrt =	ftello64(fpFileData);
 	    fseeko64(fpFileData,llPosDebutDatagram+6,SEEK_SET);
 		fread(&iSizeDatagram, sizeof(int), 1, fpFileData);

	    // D�termination du type de sondeur
 	    strARCRecordXML->ptr_cModelHeader = (char*)malloc(256*sizeof(char));
 	    sprintf(strARCRecordXML->ptr_cModelHeader, "%s", "EM300");
 	}
 	else
 	{
 	    // Affectation du type de sondeur
 	    strARCRecordXML->ptr_cModelHeader = (char*)malloc(256*sizeof(char));
 	    if (!strncmp(ptrImage_Record->BeginMsg, "$12", 3))
 	    {
 	    	iTypeSondeur = 122;	// EM12 Dual.
 	    	sprintf(strARCRecordXML->ptr_cModelHeader, "%s", "EM12D");
 	    }
 	    else
 	    {
 	    	if (!strncmp(ptrImage_Record->BeginMsg, "$95", 3))
 	    	{
 	    		iTypeSondeur = 1000;
 	 	    	sprintf(strARCRecordXML->ptr_cModelHeader, "%s", "EM1000");
 	    	}
 	    	else if (!strncmp(ptrImage_Record->BeginMsg, "$SI", 3))
 	    	{
 	    		iTypeSondeur = -1;
 	 	    	sprintf(strARCRecordXML->ptr_cModelHeader, "%s", "Simrad");
 	    	}
 	    	else
 	    	{
 	    		iTypeSondeur = -1;
	 	    	sprintf(strARCRecordXML->ptr_cModelHeader, "%s", "unknown");
 	    	}
  	    }

 	    if (!strncmp(ptrImage_Record->TypeRecord, "BATHY", 5) && (iTypeSondeur == 1000))
 	    {
 	    	iSizeDatagram = 780;
 	    }
 	    else if (!strncmp(ptrImage_Record->TypeRecord, "VOIE", 4) && (iTypeSondeur == 122))
 	    {
 	    	iSizeDatagram = 1032;
 	    }
 	    else if (!strncmp(ptrImage_Record->TypeRecord, "CEL98", 5))
 	    {
 	    	iSizeDatagram = 1044;	// NbSamples = 500;
 	    }
 	    else if (!strncmp(ptrImage_Record->TypeRecord, "CEL99", 5))
 	    {
 	    	iSizeDatagram = 240;	// NbSamples = 50;
 	    }
 	    else if (!strncmp(ptrImage_Record->TypeRecord, "CEL9A", 5))
 	    {
 	    	iSizeDatagram = 444;	// NbSamples = 100;
 	    }
 	    else if (!strncmp(ptrImage_Record->TypeRecord, "IMA", 3))
 	    {
 	    	// Calcul� plus tard dans l'exploitation du datagramme.
 	    	iSizeDatagram = 0;
 	    }
	    else
 	    {
 	 	    // Calcul de la taille du datagramme suivant par recherche de l'occurrence
 	 	    // 'CR' + 'LF' + '$'
 	 	    bFlagCarriageReturn  = FALSE;
 	 	    bFlagLineFeed		= FALSE;
 	 	    bFlagDebutDeLigne	= FALSE;
 	 	    iChar	= 0;
 	 	    while (!bFlagDebutDeLigne && iChar != EOF)
 	 	    {
 	 	    	iChar = fgetc(fpFileData);
 	 	        // On recherche la continuit� des trois caract�res.
 	 	        if (iChar == '\r')
 	 	        {
 	 	        	iChar = fgetc(fpFileData);
 	 	        	if (iChar == '\n' && iChar != EOF)
 	 	        	{
 	 	        		iChar = fgetc(fpFileData);
 	 	        		if (iChar == '$' && iChar != EOF)
 	 	        		{
 	 	        			bFlagDebutDeLigne = TRUE;
 	 	        		}
 	 	        	}
 	 	        }
 	 	    }
 	 	    // Calcul par l'avancement du pointeur de la taille du datagramme suivant.
 	 	    iSizeDatagram = ftello64(fpFileData) - llPosDebutDatagram;
 	 	     // Suppression du caract�re '$' lu en trop.
 	 	    iSizeDatagram = iSizeDatagram -1;

 	    }
 	    ptrImage_Record->DatagramLength = iSizeDatagram;

	}

    // Repositionnement en d�but de datagramme.
    fseeko64(fpFileData,llPosDebutDatagram,SEEK_SET);


	// Traitement exceptionnel pour la donn�e Position du Datagramme :
	// ptrImage_Record->DatagramPosition = ftello64(fpFileData);
	fwrite(	&llPosCrt,
			strARCRecordXML->strTabImageXML[4].usSize,
			strARCRecordXML->strTabImageXML[4].usNbElem,
			strARCRecordXML->strTabImageXML[4].ptr_fpBin);

	fwrite(	&iSizeDatagram,
			strARCRecordXML->strTabImageXML[5].usSize,
			strARCRecordXML->strTabImageXML[5].usNbElem,
			strARCRecordXML->strTabImageXML[5].ptr_fpBin);

	// --------------------------------------------
 	// Traitement des paquets de type Image
	if (iFlag == 0)
	{
		printf("%s -- Error %s in reading at %d Record : %s\n", __FILE__, strerror(errno), iNbRecord, G_cFileData);
 		return FALSE;
	}
	ptr_cImageRecord = (unsigned char*)ptrImage_Record;
	// Sauvegarde des donn�es des attributs.
	if (iNbRecord > 0)
	{
		ptr_vVarPrevRecord = (void *)ptrImage_prevRecord;
	}

	for (iVarImage=0; iVarImage<(int)strARCRecordXML->sNbVarImage-2; iVarImage++)
	{
		ptr_vVarRecord = (void *)ptr_cImageRecord;
		if (iVarImage==0)
		{
			usTailleEnByte = 0;
			usNbElem = 0;
		}
		else
		{
			usTailleEnByte = util_usNbOctetTypeVar(strARCRecordXML->strTabImageXML[iVarImage-1].cType);
			usNbElem = strARCRecordXML->strTabImageXML[iVarImage-1].usNbElem;
		}

		// Pointage sur la donn�e suivante.
		ptr_vVarPrevRecord = ptr_vVarPrevRecord + usTailleEnByte*usNbElem;
		// Traitement particulier des cha�nes : s�par�es par une virgule superflue.
		if (!strcmp(strARCRecordXML->strTabImageXML[iVarImage].cType, "char"))
		{
			// R�cup�ration du nb d'octets courants.
			usNbElem = strARCRecordXML->strTabImageXML[iVarImage].usNbElem;
			// Caract�re de fin de cha�ne forc� en remplacement de la virgule de s�paration.
			ptr_cImageRecord[usNbElem-1] 	= '\0';
		}
		// On ne fait pas le test de constance au 1er enregistrement ou si le signal
		// est d�j� vue comme variable.
		if (iNbRecord > 0 && strARCRecordXML->strTabImageXML[iVarImage].bFlagConstant == TRUE)
		{
			bFlagVarConstant = strARCRecordXML->strTabImageXML[iVarImage].bFlagConstant;
			if (G_iFlagEndian == 0) // On inverse en Big Endian.
			{
				// Observation de la taille propre � la variable.
				usTailleEnByte = util_usNbOctetTypeVar(strARCRecordXML->strTabImageXML[iVarImage].cType);
				usNbElem = strARCRecordXML->strTabImageXML[iVarImage].usNbElem;
				if (usTailleEnByte == 2) // Pour des (u)short, (u)int, (u)long.
					ptr_vVarPrevRecord = (void *)util_cBswap_16(ptr_vVarPrevRecord);
				else if (usTailleEnByte == 4)
					ptr_vVarPrevRecord = (void *)util_cBswap_32(ptr_vVarPrevRecord);
				else if (usTailleEnByte == 8)
					ptr_vVarPrevRecord = (void *)util_cBswap_64(ptr_vVarPrevRecord);
			}
			bFlagCompare = util_bComparePrevAndCurrentVal(strARCRecordXML->strTabImageXML[iVarImage].cType,
												ptr_vVarPrevRecord,
												ptr_vVarRecord,
											   strARCRecordXML->strTabImageXML[iVarImage].usNbElem);
			bFlagVarConstant = bFlagVarConstant && bFlagCompare;
			strARCRecordXML->strTabImageXML[iVarImage].bFlagConstant = bFlagVarConstant;
		}


		// Pr�paration de l'�criture de la variable.
		usTailleEnByte = strARCRecordXML->strTabImageXML[iVarImage].usSize;
		usNbElem =  strARCRecordXML->strTabImageXML[iVarImage].usNbElem;
		if (G_iFlagEndian == 0) // On inverse en Big Endian.
		{
			if (usTailleEnByte == 2) // Pour des (u)short, (u)int, (u)long.
				ptr_cImageRecord = (unsigned char*)util_cBswap_16((char*)ptr_cImageRecord);
			else if (usTailleEnByte == 4)
				ptr_cImageRecord = (unsigned char*)util_cBswap_32((char*)ptr_cImageRecord);
			else if (usTailleEnByte == 8)
				ptr_cImageRecord = (unsigned char*)util_cBswap_64((char*)ptr_cImageRecord);
		}
		fwrite(ptr_cImageRecord, usTailleEnByte, usNbElem, strARCRecordXML->strTabImageXML[iVarImage].ptr_fpBin);
		strARCRecordXML->strTabImageXML[iVarImage].ptr_vPrevVal = ptr_cImageRecord;
		ptr_cImageRecord += usTailleEnByte*usNbElem;
	}



	// Recopie en zone tampon des valeurs courantes.
	memmove(ptrImage_prevRecord, ptrImage_Record, sizeof(T_FILEINDEX_RECORD_IMAGE));

	free(ptrImage_Record);
	iNbRecord++;

	return iRetour;

} //iProcessFileIndexRecord


//----------------------------------------------------------------
// Fonction :	iInitXMLBinFileIndexRecord
// Objet 	:	Initialisation de la structure �quivalent au paquet
//				FileIndex pour la manipulation des fichiers bin et XML.
// Modification : 15/10/08
// Auteur 	: GLU
//----------------------------------------------------------------
int iInitXMLBinFileIndexRecord(	char *cTitleDatagram,
								char *cNomDatagram,
								char *cComments,
								T_ARCRECORDXML *strARCRecordXML)
{

	char cTabNomVarImage[NB_IMAGE][30] = {
			"BeginMsg",
			"Date",
			"Heure",
			"TypeRecord",
			"DatagramPosition",
			"DatagramLength"};

	char cTabNomTypeImage[NB_IMAGE][20] = {
			"char",
			"char",
			"char",
			"char",
			"u64",
			"int"};


	// Nb d'�l�ments.
	unsigned short usTabNbElem[NB_IMAGE] = {
			7,
			9,
			13,
			6,
			1,
			1};

	// ScaleFactor
	/*
	float 	fTabSFImage[NB_IMAGE] = {
			1.0};

	// AddOffset
	float 	fTabAOImage[NB_IMAGE] = {
			0.0};

	 */

	char	*ptr_cRepertoire,
			cNomFicPathBin[300];

    int 	iSizeRep,
			iRet,
			iLen,
			iVarImage;

	unsigned short usTaille;

	// Initialisation des structures de pilotage des fichiers
	// XML et Bin .
	strARCRecordXML->sNbVarSignal = 0;
	strARCRecordXML->sNbVarImage = NB_IMAGE;
	strARCRecordXML->sNbVarImage2 = 0;
    strARCRecordXML->strTabImageXML= (T_ARCVARXML *)malloc(strARCRecordXML->sNbVarImage*sizeof(T_ARCVARXML));

	iLen = strlen(cNomDatagram)+1;
	strARCRecordXML->ptr_cNomHeader = malloc(iLen*sizeof(char));
	sprintf(strARCRecordXML->ptr_cNomHeader, "%s", cNomDatagram);

	iLen = strlen(cTitleDatagram)+1;
	strARCRecordXML->ptr_cLabelHeader = malloc(iLen*sizeof(char));
	sprintf(strARCRecordXML->ptr_cLabelHeader, "%s", cTitleDatagram);

	iLen = strlen(cComments)+1;
	strARCRecordXML->ptr_cCommentsHeader = malloc(iLen*sizeof(char));
	sprintf(strARCRecordXML->ptr_cCommentsHeader, "%s", cComments);

	// Remplissage du nom de la variable d'indexation des tables.
	strARCRecordXML->ptr_cNomIndexTable = NULL;
	strARCRecordXML->ptr_cNomIndexTable2 = NULL;


	// Affectation et cr�ation du repertoire du Datagramme.
	iSizeRep = ( strlen(G_cRepData) + strlen( cNomDatagram));
	// Ajout d'une marge au cas o� certains caract�res soient doubl�s soient doubl�s.
	ptr_cRepertoire = malloc( (iSizeRep + 1 + 30 + 4)*sizeof(char));
	iRet = util_iCreateDirDatagram(G_cRepData,
								strARCRecordXML->ptr_cNomHeader,
								ptr_cRepertoire);

	strARCRecordXML->ptr_cCommentsHeader = malloc(iLen*sizeof(char));
	sprintf(strARCRecordXML->ptr_cCommentsHeader, "%s", cComments);

	// Initialisation g�n�rale des variables de type Image
	for (iVarImage=0; iVarImage<strARCRecordXML->sNbVarImage; iVarImage++)
	{
	   strARCRecordXML->strTabImageXML[iVarImage].ptr_fpBin = NULL;
	   strARCRecordXML->strTabImageXML[iVarImage].bFlagConstant = TRUE;
	   strARCRecordXML->strTabImageXML[iVarImage].usNbElem = usTabNbElem[iVarImage];
	   sprintf(strARCRecordXML->strTabImageXML[iVarImage].cNom, "%s", cTabNomVarImage[iVarImage]);
	   sprintf(strARCRecordXML->strTabImageXML[iVarImage].cType, "%s", cTabNomTypeImage[iVarImage]);
	   sprintf(strARCRecordXML->strTabImageXML[iVarImage].cTag, "%s", "TODO");
	   sprintf(strARCRecordXML->strTabImageXML[iVarImage].cUnit, "%s", "TODO");
	   //strARCRecordXML->strTabImageXML[iVarImage].fScaleFactor = fTabSFImage[iVarImage];
	   //strARCRecordXML->strTabImageXML[iVarImage].fAddOffset = fTabAOImage[iVarImage];
	   strARCRecordXML->strTabImageXML[iVarImage].fScaleFactor = 1.0;
	   strARCRecordXML->strTabImageXML[iVarImage].fAddOffset = 0.0;
	   usTaille = util_usNbOctetTypeVar(cTabNomTypeImage[iVarImage]);
	   strARCRecordXML->strTabImageXML[iVarImage].usSize = usTaille;
	   iLen = strlen(strARCRecordXML->strTabImageXML[iVarImage].cNom);
	   // Ajout d'une marge au cas o� certains caract�res soient doubl�s soient doubl�s.
	   sprintf(cNomFicPathBin, "%s\\%s%s", ptr_cRepertoire, strARCRecordXML->strTabImageXML[iVarImage].cNom, ".bin");
	   sprintf(strARCRecordXML->strTabImageXML[iVarImage].cNomPathFileBin, "%s", cNomFicPathBin);
	   strARCRecordXML->strTabImageXML[iVarImage].ptr_fpBin = fopen(cNomFicPathBin, "w+b");
	   if (!strARCRecordXML->strTabImageXML[iVarImage].ptr_fpBin)
	   {
		   printf("%s -- Error in Image creation  : %s\n", __FILE__, cNomFicPathBin);
		   return EXIT_FAILURE;
	   }
	}

	free(ptr_cRepertoire);
	return EXIT_SUCCESS;

} // iInitXMLBinFileIndexRecord

#pragma pack()
