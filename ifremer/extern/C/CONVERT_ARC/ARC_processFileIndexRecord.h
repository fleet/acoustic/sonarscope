/*
 * ARC_processFileIndexRecord.h
 *
 *  Created on: 21 nov. 2008
 *      Author: rgallou
 */
#pragma pack(1)

#ifndef ARC_PROCESSFILEINDEXRECORD_H_
#define ARC_PROCESSFILEINDEXRECORD_H_

#include "ARC_DATAGRAMS.H"
#include <sys/types.h>


typedef struct  {
	char			BeginMsg[7];
	char			Date[9];	// JJ/MM/AA
	char			Heure[13];  // HH:MM:SS.DDD
	char			TypeRecord[6];
	off64_t 		DatagramPosition;
	int 			DatagramLength;
} T_FILEINDEX_RECORD_IMAGE;


int iProcessFileIndexRecord(FILE *fpFileData,
						int iNbRecord,
						T_ARCRECORDXML *strARCRecordXML,
						T_FILEINDEX_RECORD_IMAGE *ptrImage_prevRecord);


int iInitXMLBinFileIndexRecord(	char *cTitleDatagram,
								char *cNomDatagram, char *cComments,
								T_ARCRECORDXML *strARCRecordXML);

#endif // ARC_PROCESSFILEINDEXRECORD_H_

#pragma pack()
