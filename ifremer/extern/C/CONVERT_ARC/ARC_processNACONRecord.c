/*
 * ARC_processNACONREcord.c
 *
 *  Created on: 13 oct. 2008
 *      Author: rgallou
 */
#pragma pack(1)

#include <sys/types.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "ARC_Datagrams.h"
#include "ARC_Utilities.h"
#include "Generic_Utilities.h"
#include "ARC_processNACONRecord.h"
#include "convertFiles_ARC.h"

#define NBIMAGE 37
#define NBIMAGE2 5

//----------------------------------------------------------------
// Fonction :	iProcessNACONREcord
// Objet 	:	Traitement du paquet de capteurs de navigation
// Modif.	: 	03/11/08
// Auteur 	:	GLU
//----------------------------------------------------------------
int iProcessNACONRecord(FILE *fpFileData,
						int iNbSystSupp,
						int iNbRecord,
						T_ARCRECORDXML *strARCRecordXML)

{
	// Pointeur pour avancer sur les variables.
	char			*ptr_cImageRecord;


	BOOL			bFlagCompare = TRUE;
	//BOOL			bFlagVarConstant = TRUE;

    int 			iFlag,
					iVarImage,
					iRet = 0;

	//static int		iNbRecord = 0;

    unsigned short	usTailleEnByte,
					usNbElem;

    // Initialisation de la structure d'identification du paquet
 	if (iNbRecord == 0)
	{
 		iInitXMLBinNACONRecord(	iNbSystSupp,
								"NACON",
								"NACON",
								"Configuration des syst�mes connect�s",
								strARCRecordXML);
	}

 	// --------------------------------------------
 	// Lecture unitaire des variables de type Image
	for (iVarImage=0; iVarImage< strARCRecordXML->sNbVarImage; iVarImage++)
	{
		usTailleEnByte = strARCRecordXML->strTabImageXML[iVarImage].usSize;
		usNbElem = strARCRecordXML->strTabImageXML[iVarImage].usNbElem;
		ptr_cImageRecord 	= (char*)calloc((usTailleEnByte*usNbElem),sizeof(char));
		iFlag = fread(ptr_cImageRecord, usTailleEnByte, usNbElem, fpFileData);
		// Caract�re de fin de cha�ne forc�.
		ptr_cImageRecord[usNbElem-1] = '\0';

		if (G_iFlagEndian == 0) // On inverse en Big Endian.
		{
			if (usTailleEnByte == 2) // Pour des (u)short, (u)int, (u)long.
				ptr_cImageRecord = (char*)util_cBswap_16((char*)ptr_cImageRecord);
			else if (usTailleEnByte == 4)
				ptr_cImageRecord = (char*)util_cBswap_32((char*)ptr_cImageRecord);
			else if (usTailleEnByte == 8)
				ptr_cImageRecord = (char*)util_cBswap_64((char*)ptr_cImageRecord);
		}
		iFlag = fwrite(ptr_cImageRecord, usTailleEnByte, usNbElem, strARCRecordXML->strTabImageXML[iVarImage].ptr_fpBin);
		if (iNbRecord > 0 && strARCRecordXML->strTabImageXML[iVarImage].bFlagConstant == TRUE)
		{
			bFlagCompare = util_bComparePrevAndCurrentVal(strARCRecordXML->strTabImageXML[iVarImage].cType,
												strARCRecordXML->strTabImageXML[iVarImage].ptr_vPrevVal,
												(void *)ptr_cImageRecord,
												usNbElem);
			strARCRecordXML->strTabImageXML[iVarImage].bFlagConstant = bFlagCompare && strARCRecordXML->strTabImageXML[iVarImage].bFlagConstant;
		}
		if (iNbRecord == 0)
		{
			strARCRecordXML->strTabImageXML[iVarImage].ptr_vPrevVal = malloc(usTailleEnByte*usNbElem*sizeof(char));
		}
		//memmove(strARCRecordXML->strTabImageXML[iVarImage].ptr_vPrevVal, ptr_vBidon, usTailleEnByte*usNbElem);
		memmove(strARCRecordXML->strTabImageXML[iVarImage].ptr_vPrevVal, ptr_cImageRecord, usTailleEnByte*usNbElem);

		free(ptr_cImageRecord);

	} // Fin de la boucles sur les variables.

	// Lecture du nombre de variables �ventuelles.
	usTailleEnByte 		= 1;	// char
	usNbElem 			= 4;	// 4 octets
	ptr_cImageRecord 	= (char*)calloc((usTailleEnByte*usNbElem),sizeof(char));
	iFlag = fread(ptr_cImageRecord, usTailleEnByte, usNbElem, fpFileData);
	// Caract�re de fin de cha�ne forc�.
	ptr_cImageRecord[usNbElem-1] = '\0';
	free(ptr_cImageRecord);

 	// Si on a des syst�mes de navigation suppl�mentaires.
	// ---------------------------------------------------
	if (iNbSystSupp > 0)
	{
		// Lecture unitaire des variables de type Image2
		for (iVarImage=0; iVarImage< strARCRecordXML->sNbVarImage2; iVarImage++)
		{
			usTailleEnByte = strARCRecordXML->strTabImage2XML[iVarImage].usSize;
			usNbElem = strARCRecordXML->strTabImage2XML[iVarImage].usNbElem;
			ptr_cImageRecord 	= (char*)calloc((usTailleEnByte*usNbElem),sizeof(char));
			iFlag = fread(ptr_cImageRecord, usTailleEnByte, usNbElem, fpFileData);
			// Caract�re de fin de cha�ne forc�.
			ptr_cImageRecord[usNbElem-1] = '\0';

			if (G_iFlagEndian == 0) // On inverse en Big Endian.
			{
				if (usTailleEnByte == 2) // Pour des (u)short, (u)int, (u)long.
					ptr_cImageRecord = (char*)util_cBswap_16((char*)ptr_cImageRecord);
				else if (usTailleEnByte == 4)
					ptr_cImageRecord = (char*)util_cBswap_32((char*)ptr_cImageRecord);
				else if (usTailleEnByte == 8)
					ptr_cImageRecord = (char*)util_cBswap_64((char*)ptr_cImageRecord);
			}
			iFlag = fwrite(ptr_cImageRecord, usTailleEnByte, usNbElem, strARCRecordXML->strTabImage2XML[iVarImage].ptr_fpBin);
			if (iNbRecord > 0 && strARCRecordXML->strTabImage2XML[iVarImage].bFlagConstant == TRUE)
			{
				bFlagCompare = util_bComparePrevAndCurrentVal(strARCRecordXML->strTabImage2XML[iVarImage].cType,
													strARCRecordXML->strTabImage2XML[iVarImage].ptr_vPrevVal,
													(void *)ptr_cImageRecord,
													usNbElem);
				strARCRecordXML->strTabImage2XML[iVarImage].bFlagConstant = bFlagCompare && strARCRecordXML->strTabImage2XML[iVarImage].bFlagConstant;
			}
			if (iNbRecord == 0)
			{
				strARCRecordXML->strTabImage2XML[iVarImage].ptr_vPrevVal = malloc(usTailleEnByte*usNbElem*sizeof(char));
			}
			//memmove(strARCRecordXML->strTabImage2XML[iVarImage].ptr_vPrevVal, ptr_vBidon, usTailleEnByte*usNbElem);
			memmove(strARCRecordXML->strTabImage2XML[iVarImage].ptr_vPrevVal, ptr_cImageRecord, usTailleEnByte*usNbElem);

			free(ptr_cImageRecord);

		} // Fin de la boucles sur les variables.
	} // Fin de la boucle sur le nombre de syst�mes supp.

	iNbRecord++;

	return iRet;

} //iProcessNACONREcord


//----------------------------------------------------------------
// Fonction :	iInitXMLBinNACONRecord
// Objet 	:	Initialisation de la structure �quivalent au paquet
//				Capteurs de Navigation pour la manipulation des fichiers bin et XML.
// Modification : 15/10/08
// Auteur 	: GLU
//----------------------------------------------------------------
int iInitXMLBinNACONRecord(		int iNbSystSupp,
								char *cTitleDatagram,
								char *cNomDatagram,
								char *cComments,
								T_ARCRECORDXML *strARCRecordXML)
{



	char cTabNomVarImage[NBIMAGE][30] = {
			"BeginMsg",
			"Date",
			"Heure",
			"TypeRecord",
			"PtReference",
			"DescriptifPtRef",
			"SystNav1",
			"DescriptifApp1",
			"CoordXNav1",
			"CoordYNav1",
			"CoordZNav1",
			"SystNav2",
			"DescriptifApp2",
			"CoordXNav2",
			"CoordYNav2",
			"CoordZNav2",
			"SystNav3",
			"DescriptifApp3",
			"CoordXNav3",
			"CoordYNav3",
			"CoordZNav3",
			"SystNav4",
			"DescriptifApp4",
			"CoordXNav4",
			"CoordYNav4",
			"CoordZNav4",
			"SystNavX",
			"DescriptifAppX",
			"CoordXNavX",
			"CoordYNavX",
			"CoordZNavX",
			"SystBathy",
			"DescriptifAppBathy",
			"CoordXNavBathy",
			"CoordYNavBathy",
			"CoordZNavBathy",
			"ImmersionNormaleBase"};

	char cTabNomTypeImage[NBIMAGE][20] = {
			"char",
			"char",
			"char",
			"char",
			"char",
			"char",
			"char",
			"char",
			"char",
			"char",
			"char",
			"char",
			"char",
			"char",
			"char",
			"char",
			"char",
			"char",
			"char",
			"char",
			"char",
			"char",
			"char",
			"char",
			"char",
			"char",
			"char",
			"char",
			"char",
			"char",
			"char",
			"char",
			"char",
			"char",
			"char",
			"char",
			"char"};

	// ScaleFactor
	/*
	float 	fTabSFImage[NBIMAGE] = {
			1.0,
			};

	// AddOffset
	float 	fTabAOImage[NBIMAGE] = {
			0.0,
			};
	 */

	// Taille des champs (sans compter le caractere de s�paration = ',')
	float 	usTabNbElem[NBIMAGE] = {
			7,
			9,
			13,
			6,
			6,
			31,
			6,
			21,
			7,
			7,
			7,
			6,
			21,
			7,
			7,
			7,
			6,
			21,
			7,
			7,
			7,
			6,
			21,
			7,
			7,
			7,
			6,
			21,
			7,
			7,
			7,
			6,
			21,
			7,
			7,
			7,
			5};

	char cTabNomVarImage2[NBIMAGE2][30] = {
			"SystSuppN",
			"DescAppN",
			"CoordXSystN",
			"CoordYSystN",
			"CoordZSystN"
	};

	char cTabNomTypeImage2[NBIMAGE2][20] = {
			"char",
			"char",
			"char",
			"char",
			"char"
	};

	unsigned short usTabNbElem2[NBIMAGE2] = {
			6,
			21,
			7,
			7,
			7
	};

	char	*ptr_cRepertoire,
			cNomFicPathBin[300];

    int 	iSizeRep,
			iRet,
			iLen,
			iVarImage,
			iVarSysSupp;

    // int		iVarSignal;
    unsigned short 	usTaille;

	// Initialisation des structures de pilotage des fichiers
	// XML et Bin .
	strARCRecordXML->sNbVarSignal 	= 0;
	strARCRecordXML->sNbVarImage 	= NBIMAGE;
	strARCRecordXML->sNbVarImage2 	= iNbSystSupp*NBIMAGE2;	// On r�p�te le bloc de variables.
    strARCRecordXML->strTabSignalXML = (T_ARCVARXML *)malloc(strARCRecordXML->sNbVarSignal*sizeof(T_ARCVARXML));
    strARCRecordXML->strTabImageXML  = (T_ARCVARXML *)malloc(strARCRecordXML->sNbVarImage*sizeof(T_ARCVARXML));
    strARCRecordXML->strTabImage2XML = (T_ARCVARXML *)malloc(strARCRecordXML->sNbVarImage2*sizeof(T_ARCVARXML));

	iLen = strlen(cNomDatagram)+1;
	strARCRecordXML->ptr_cNomHeader = malloc(iLen*sizeof(char));
	sprintf(strARCRecordXML->ptr_cNomHeader, "%s", cNomDatagram);

	iLen = strlen(cTitleDatagram)+1;
	strARCRecordXML->ptr_cLabelHeader = malloc(iLen*sizeof(char));
	sprintf(strARCRecordXML->ptr_cLabelHeader, "%s", cTitleDatagram);

	iLen = strlen(cComments)+1;
	strARCRecordXML->ptr_cCommentsHeader = malloc(iLen*sizeof(char));
	sprintf(strARCRecordXML->ptr_cCommentsHeader, "%s", cComments);

	// Remplissage du nom de la variable d'indexation des tables.
	strARCRecordXML->ptr_cNomIndexTable = NULL;
	strARCRecordXML->ptr_cNomIndexTable2 = NULL;

	// Affectation et cr�ation du repertoire du Datagramme.
	iSizeRep = ( strlen(G_cRepData) + strlen( cNomDatagram));
	// Ajout d'une marge au cas o� certains caract�res soient doubl�s soient doubl�s.
	ptr_cRepertoire = malloc( (iSizeRep + 1 + 30 + 4)*sizeof(char));
	iRet = util_iCreateDirDatagram(G_cRepData,
								strARCRecordXML->ptr_cNomHeader,
								ptr_cRepertoire);

	// Initialisation g�n�rale des variables de type Image
	for (iVarImage=0; iVarImage<strARCRecordXML->sNbVarImage; iVarImage++) {
	   strARCRecordXML->strTabImageXML[iVarImage].ptr_fpBin = NULL;
	   strARCRecordXML->strTabImageXML[iVarImage].bFlagConstant = TRUE;
	   strARCRecordXML->strTabImageXML[iVarImage].usNbElem = usTabNbElem[iVarImage];
	   sprintf(strARCRecordXML->strTabImageXML[iVarImage].cNom, "%s", cTabNomVarImage[iVarImage]);
	   sprintf(strARCRecordXML->strTabImageXML[iVarImage].cType, "%s", cTabNomTypeImage[iVarImage]);
	   sprintf(strARCRecordXML->strTabImageXML[iVarImage].cTag, "%s", "TODO");
	   sprintf(strARCRecordXML->strTabImageXML[iVarImage].cUnit, "%s", "TODO");
	   //strARCRecordXML->strTabImageXML[iVarImage].fScaleFactor = fTabSFImage[iVarImage];
	   //strARCRecordXML->strTabImageXML[iVarImage].fAddOffset = fTabAOImage[iVarImage];
	   strARCRecordXML->strTabImageXML[iVarImage].fScaleFactor = 1.0;
	   strARCRecordXML->strTabImageXML[iVarImage].fAddOffset = 0.0;
	   usTaille = util_usNbOctetTypeVar(cTabNomTypeImage[iVarImage]);
	   strARCRecordXML->strTabImageXML[iVarImage].usSize = usTaille;
	   iLen = strlen(strARCRecordXML->strTabImageXML[iVarImage].cNom);
	   // Ajout d'une marge au cas o� certains caract�res soient doubl�s soient doubl�s.
	   sprintf(cNomFicPathBin, "%s\\%s%s", ptr_cRepertoire, strARCRecordXML->strTabImageXML[iVarImage].cNom, ".bin");
	   sprintf(strARCRecordXML->strTabImageXML[iVarImage].cNomPathFileBin, "%s", cNomFicPathBin);
	   strARCRecordXML->strTabImageXML[iVarImage].ptr_fpBin = fopen(cNomFicPathBin, "w+b");
	   if (!strARCRecordXML->strTabImageXML[iVarImage].ptr_fpBin)
	   {
		   printf("%s -- Error in Image creation  : %s\n", __FILE__, cNomFicPathBin);
		   return EXIT_FAILURE;
	   }
	}

	// Initialisation g�n�rale des variables de type Image2.
	// Le nb d'appareils de syst�mes suppl�mentaires d�pend de la variable lue au pr�alable dans le fichier.
	// Le nb de variables = nb de variables la structure X nb structures (=syst�mes suppl�mentaires).
	for (iVarImage=0; iVarImage<strARCRecordXML->sNbVarImage2; iVarImage++) {
	   strARCRecordXML->strTabImage2XML[iVarImage].ptr_fpBin = NULL;
	   strARCRecordXML->strTabImage2XML[iVarImage].bFlagConstant = TRUE;
	   iVarSysSupp = iVarImage%NBIMAGE2;
	   strARCRecordXML->strTabImage2XML[iVarImage].usNbElem = usTabNbElem2[iVarSysSupp];
	   sprintf(strARCRecordXML->strTabImage2XML[iVarImage].cNom, "%s", cTabNomVarImage2[iVarSysSupp]);
	   sprintf(strARCRecordXML->strTabImage2XML[iVarImage].cType, "%s", cTabNomTypeImage2[iVarSysSupp]);
	   sprintf(strARCRecordXML->strTabImage2XML[iVarImage].cTag, "%s", "TODO");
	   sprintf(strARCRecordXML->strTabImage2XML[iVarImage].cUnit, "%s", "TODO");
	   //strARCRecordXML->strTabImage2XML[iVarImage].fScaleFactor = fTabSFImage2[iVarSysSupp];
	   //strARCRecordXML->strTabImage2XML[iVarImage].fAddOffset = fTabAOImage2[iVarSysSupp];
	   strARCRecordXML->strTabImage2XML[iVarImage].fScaleFactor = 1.0;
	   strARCRecordXML->strTabImage2XML[iVarImage].fAddOffset = 0.0;
	   usTaille = util_usNbOctetTypeVar(cTabNomTypeImage2[iVarSysSupp]);
	   strARCRecordXML->strTabImage2XML[iVarImage].usSize = usTaille;
	   iLen = strlen(strARCRecordXML->strTabImage2XML[iVarImage].cNom);
	   // Ajout d'une marge au cas o� certains caract�res soient doubl�s soient doubl�s.
	   sprintf(cNomFicPathBin, "%s\\%s%s", ptr_cRepertoire, strARCRecordXML->strTabImage2XML[iVarImage].cNom, ".bin");
	   sprintf(strARCRecordXML->strTabImage2XML[iVarImage].cNomPathFileBin, "%s", cNomFicPathBin);
	   strARCRecordXML->strTabImage2XML[iVarImage].ptr_fpBin = fopen(cNomFicPathBin, "w+b");
	   if (!strARCRecordXML->strTabImage2XML[iVarImage].ptr_fpBin)
	   {
		   printf("%s -- Error in Image creation  : %s\n", __FILE__, cNomFicPathBin);
		   return EXIT_FAILURE;
	   }
	}

	free(ptr_cRepertoire);
	return EXIT_SUCCESS;

} // iInitXMLBinNACONRecord

#pragma pack()
