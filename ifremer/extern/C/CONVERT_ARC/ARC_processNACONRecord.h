/*
 * ARC_processNACONRecord.h
 *
 *  Created on: 21 nov. 2008
 *      Author: rgallou
 */
#pragma pack(1)

#ifndef ARC_PROCESSNACONRECORD_H_
#define ARC_PROCESSNACONRECORD_H_

#include "ARC_DATAGRAMS.H"


// D�finition de la structure NACON
typedef struct  {
	// On d�crit la taille fixe des chaines comprenant la virgule de s�paration.
	char	BeginMsg[7];
	char	Date[9];	// JJ/MM/AA
	char	Heure[13];  // HH:MM:SS.DDD
	char	TypeRecord[6];
	char	PtReference[6];
	char	DescriptifPtRef[31];
	char	SystNav1[6];
	char	DescriptifApp1[21];
	char	CoordXNav1[7];
	char	CoordYNav1[7];
	char	CoordZNav1[7];
	char	SystNav2[6];
	char	DescriptifApp2[21];
	char	CoordXNav2[7];
	char	CoordYNav2[7];
	char	CoordZNav2[7];
	char	SystNav3[6];
	char	DescriptifApp3[21];
	char	CoordXNav3[7];
	char	CoordYNav3[7];
	char	CoordZNav3[7];
	char	SystNav4[6];
	char	DescriptifApp4[21];
	char	CoordXNav4[7];
	char	CoordYNav4[7];
	char	CoordZNav4[7];
	char	SystNavX[6];
	char	DescriptifAppX[21];
	char	CoordXNavX[7];
	char	CoordYNavX[7];
	char	CoordZNavX[7];
	char	SystBathy[6];
	char	DescriptifAppBathy[21];
	char	CoordXNavBathy[7];
	char	CoordYNavBathy[7];
	char	CoordZNavBathy[7];
	char	ImmersionNormaleBase[5];
} T_NACON_RECORD_IMAGE;

typedef struct  {
	char	SystSupp[6];
	char	DescriptifSupp[21];
	char	CoordXNavSupp[7];
	char	CoordYNavSupp[7];
	char	CoordZNavSupp[7];
} T_NACON_RECORD_IMAGE2;

int iProcessNACONRecord(FILE *fpFileData,
						int iNbSystNav,
						int iNbRecord,
						T_ARCRECORDXML *strARCRecordXML);


int iInitXMLBinNACONRecord(		int iNbSystSupp,
								char *cTitleDatagram,
								char *cNomDatagram,
								char *cComments,
								T_ARCRECORDXML *strARCRecordXML);

#endif /* ARC_PROCESSNACONRECORD_H_ */

#pragma pack()
