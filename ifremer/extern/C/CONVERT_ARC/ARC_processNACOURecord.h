/*
 * ARC_processNACOURecord.h
 *
 *  Created on: 21 nov. 2008
 *      Author: rgallou
 */
#pragma pack(1)

#ifndef ARC_PROCESSNACOURECORD_H_
#define ARC_PROCESSNACOURECORD_H_

#include "ARC_DATAGRAMS.H"


// D�finition de la structure NACOU
typedef struct  {
	// On d�crit la taille fixe des chaines comprenant la virgule de s�paration.
	char	BeginMsg[7];
	char	Date[9];	// JJ/MM/AA
	char	Heure[13];  // HH:MM:SS.DDD
	char	TypeRecord[6];
	char	Latitude[14];
	char	Longitude[15];
	char	LochDopplerLong[8];
	char	LochDopplerTrans[8];
	char	LochEMLong[8];
	char	LochEMTrans[8];
	char	CapGyro1[7];
	char	CapGyro2[7];
	char	FlagQualite[2];
	char	SystGeodesie[5];
	char	VentVraiVitesse[3];
	char	VentVraiDirection[4];
	char	CapAuxOrigine[4];	// AT1, AT2, INT ...
	char	CapAuxValeur[6];	// On ne compte pas la virgule.

} T_NACOU_RECORD_IMAGE;

int iProcessNACOURecord(FILE *fpFileData,
						int iNbRecord,
						T_ARCRECORDXML *strARCRecordXML);


int iInitXMLBinNACOURecord(	char *cTitleDatagram,
								char *cNomDatagram,
								char *cComments,
								T_ARCRECORDXML *strARCRecordXML);


#endif /* ARC_PROCESSNACOURECORD_H_ */

#pragma pack()
