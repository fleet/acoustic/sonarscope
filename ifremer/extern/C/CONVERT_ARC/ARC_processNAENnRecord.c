/*
 * ARC_processNAENnREcord.c
 *
 *  Created on: 13 oct. 2008
 *      Author: rgallou
 */
#pragma pack(1)

#include <sys/types.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "ARC_Datagrams.h"
#include "ARC_Utilities.h"
#include "Generic_Utilities.h"
#include "ARC_processNAENnRecord.h"
#include "convertFiles_ARC.h"

#define NBIMAGE 20

//----------------------------------------------------------------
// Fonction :	iProcessNAENnREcord
// Objet 	:	Traitement du paquet de navigation
// Modif.	: 	03/11/08
// Auteur 	:	GLU
//----------------------------------------------------------------
int iProcessNAENnRecord(FILE *fpFileData,
						int iNbRecord,
						T_ARCRECORDXML *strARCRecordXML)

{
	// Pointeur pour avancer sur les variables.
	char			*ptr_cImageRecord;


	BOOL			bFlagCompare = TRUE;
	//BOOL			bFlagVarConstant = TRUE;

    int 			iFlag,
					iVarImage,
					iRetour = 0;

	//static int		iNbRecord = 0;

    unsigned short	usTailleEnByte,
					usNbElem;

    // Initialisation de la structure d'identification du paquet
 	if (iNbRecord == 0)
	{
 		iInitXMLBinNAENnRecord(	"NAENn",
								"NAENn",
								"Bloc de nagivation",
								strARCRecordXML);
	}

 	// --------------------------------------------
 	// Lecture unitaire des variables de type Image
	for (iVarImage=0; iVarImage< strARCRecordXML->sNbVarImage; iVarImage++)
	{
		usTailleEnByte = strARCRecordXML->strTabImageXML[iVarImage].usSize;
		usNbElem = strARCRecordXML->strTabImageXML[iVarImage].usNbElem;
		ptr_cImageRecord 	= (char*)calloc((usTailleEnByte*usNbElem),sizeof(char));
		iFlag = fread(ptr_cImageRecord, usTailleEnByte, usNbElem, fpFileData);
		// Caract�re de fin de cha�ne forc�.
		ptr_cImageRecord[usNbElem-1] = '\0';
		char cValeur[20];

		iFlag = util_cValVarConstant(	strARCRecordXML->strTabImageXML[iVarImage].cType,
										(void *)ptr_cImageRecord,
										usNbElem,
										cValeur);

		if (G_iFlagEndian == 0) // On inverse en Big Endian.
		{
			if (usTailleEnByte == 2) // Pour des (u)short, (u)int, (u)long.
				ptr_cImageRecord = (char*)util_cBswap_16((char*)ptr_cImageRecord);
			else if (usTailleEnByte == 4)
				ptr_cImageRecord = (char*)util_cBswap_32((char*)ptr_cImageRecord);
			else if (usTailleEnByte == 8)
				ptr_cImageRecord = (char*)util_cBswap_64((char*)ptr_cImageRecord);
		}
		iFlag = fwrite(ptr_cImageRecord, usTailleEnByte, usNbElem, strARCRecordXML->strTabImageXML[iVarImage].ptr_fpBin);
		if (iNbRecord > 0 && strARCRecordXML->strTabImageXML[iVarImage].bFlagConstant == TRUE)
		{
			bFlagCompare = util_bComparePrevAndCurrentVal(strARCRecordXML->strTabImageXML[iVarImage].cType,
												strARCRecordXML->strTabImageXML[iVarImage].ptr_vPrevVal,
												(void *)ptr_cImageRecord,
												usNbElem);
			strARCRecordXML->strTabImageXML[iVarImage].bFlagConstant = bFlagCompare && strARCRecordXML->strTabImageXML[iVarImage].bFlagConstant;
		}
		if (iNbRecord == 0)
		{
			strARCRecordXML->strTabImageXML[iVarImage].ptr_vPrevVal = malloc(usTailleEnByte*usNbElem*sizeof(char));
		}
		//memmove(strARCRecordXML->strTabImageXML[iVarImage].ptr_vPrevVal, ptr_vBidon, usTailleEnByte*usNbElem);
		memmove(strARCRecordXML->strTabImageXML[iVarImage].ptr_vPrevVal, ptr_cImageRecord, usTailleEnByte*usNbElem);

		free(ptr_cImageRecord);

	} // Fin de la boucles sur les variables.

	iNbRecord++;

	return iRetour;
} //iProcessNAENnREcord


//----------------------------------------------------------------
// Fonction :	iInitXMLBinNAENnRecord
// Objet 	:	Initialisation de la structure �quivalent au paquet
//				Bathy pour la manipulation des fichiers bin et XML.
// Modification : 15/10/08
// Auteur 	: GLU
//----------------------------------------------------------------
int iInitXMLBinNAENnRecord(	char *cTitleDatagram,
								char *cNomDatagram,
								char *cComments,
								T_ARCRECORDXML *strARCRecordXML)
{
	char cTabNomVarImage[NBIMAGE][30] = {
			"BeginMsg",
			"Date",
			"Heure",
			"TypeEngin",
			"Latitude",
			"Longitude",
			"Immersion",
			"CoordX",
			"CoordY",
			"CoordZ",
			"CapEngin",
			"LochEngin",
			"RouteFond",
			"VitesseFond",
			"VitesseZ",
			"DistanceOblique",
			"DistanceHorizontale",
			"SystemPositionnement",
			"OriginePtSurface",
			"SystGeodesie"};

	char cTabNomTypeImage[NBIMAGE][20] = {
			"char",
			"char",
			"char",
			"char",
			"char",
			"char",
			"char",
			"char",
			"char",
			"char",
			"char",
			"char",
			"char",
			"char",
			"char",
			"char",
			"char",
			"char",
			"char",
			"char"};

	// ScaleFactor
	float 	fTabSFImage[NBIMAGE] = {
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0};

	// AddOffset
	float 	fTabAOImage[NBIMAGE] = {
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0};

	// Taille des champs (sans compter le caractere de s�paration = ',')
	float 	usTabNbElem[NBIMAGE] = {
			7,
			9,
			13,
			6,
			14,
			15,
			10,
			10,
			10,
			10,
			7,
			7,
			7,
			7,
			7,
			10,
			10,
			4,
			4,
			5};


	char	*ptr_cRepertoire,
			cNomFicPathBin[300];

    int 	iSizeRep,
			iRet,
			iLen,
			iVarImage;

    // int		iVarSignal;
    unsigned short usTaille;

	// Initialisation des structures de pilotage des fichiers
	// XML et Bin .
	strARCRecordXML->sNbVarSignal = 0;
	strARCRecordXML->sNbVarImage = NBIMAGE;
	strARCRecordXML->sNbVarImage2 = 0;
    strARCRecordXML->strTabSignalXML= (T_ARCVARXML *)malloc(strARCRecordXML->sNbVarSignal*sizeof(T_ARCVARXML));
    strARCRecordXML->strTabImageXML= (T_ARCVARXML *)malloc(strARCRecordXML->sNbVarImage*sizeof(T_ARCVARXML));

	iLen = strlen(cNomDatagram)+1;
	strARCRecordXML->ptr_cNomHeader = malloc(iLen*sizeof(char));
	sprintf(strARCRecordXML->ptr_cNomHeader, "%s", cNomDatagram);

	iLen = strlen(cTitleDatagram)+1;
	strARCRecordXML->ptr_cLabelHeader = malloc(iLen*sizeof(char));
	sprintf(strARCRecordXML->ptr_cLabelHeader, "%s", cTitleDatagram);

	iLen = strlen(cComments)+1;
	strARCRecordXML->ptr_cCommentsHeader = malloc(iLen*sizeof(char));
	sprintf(strARCRecordXML->ptr_cCommentsHeader, "%s", cComments);

	// Remplissage du nom de la variable d'indexation des tables.
	strARCRecordXML->ptr_cNomIndexTable = NULL;
	strARCRecordXML->ptr_cNomIndexTable2 = NULL;

	// Affectation et cr�ation du repertoire du Datagramme.
	iSizeRep = ( strlen(G_cRepData) + strlen( cNomDatagram));
	// Ajout d'une marge au cas o� certains caract�res soient doubl�s soient doubl�s.
	ptr_cRepertoire = malloc( (iSizeRep + 1 + 30 + 4)*sizeof(char));
	iRet = util_iCreateDirDatagram(G_cRepData,
								strARCRecordXML->ptr_cNomHeader,
								ptr_cRepertoire);

	// Initialisation g�n�rale des variables de type Image
	for (iVarImage=0; iVarImage<strARCRecordXML->sNbVarImage; iVarImage++) {
	   strARCRecordXML->strTabImageXML[iVarImage].ptr_fpBin = NULL;
	   strARCRecordXML->strTabImageXML[iVarImage].bFlagConstant = TRUE;
	   strARCRecordXML->strTabImageXML[iVarImage].usNbElem = usTabNbElem[iVarImage];
	   sprintf(strARCRecordXML->strTabImageXML[iVarImage].cNom, "%s", cTabNomVarImage[iVarImage]);
	   sprintf(strARCRecordXML->strTabImageXML[iVarImage].cType, "%s", cTabNomTypeImage[iVarImage]);
	   sprintf(strARCRecordXML->strTabImageXML[iVarImage].cTag, "%s", "TODO");
	   sprintf(strARCRecordXML->strTabImageXML[iVarImage].cUnit, "%s", "TODO");
	   strARCRecordXML->strTabImageXML[iVarImage].fScaleFactor = fTabSFImage[iVarImage];
	   strARCRecordXML->strTabImageXML[iVarImage].fAddOffset = fTabAOImage[iVarImage];
	   usTaille = util_usNbOctetTypeVar(cTabNomTypeImage[iVarImage]);
	   strARCRecordXML->strTabImageXML[iVarImage].usSize = usTaille;
	   iLen = strlen(strARCRecordXML->strTabImageXML[iVarImage].cNom);
	   // Ajout d'une marge au cas o� certains caract�res soient doubl�s soient doubl�s.
	   sprintf(cNomFicPathBin, "%s\\%s%s", ptr_cRepertoire, strARCRecordXML->strTabImageXML[iVarImage].cNom, ".bin");
	   sprintf(strARCRecordXML->strTabImageXML[iVarImage].cNomPathFileBin, "%s", cNomFicPathBin);
	   strARCRecordXML->strTabImageXML[iVarImage].ptr_fpBin = fopen(cNomFicPathBin, "w+b");
	   if (!strARCRecordXML->strTabImageXML[iVarImage].ptr_fpBin)
	   {
		   printf("%s -- Error in Image creation  : %s\n", __FILE__, cNomFicPathBin);
		   return EXIT_FAILURE;
	   }
	}


	free(ptr_cRepertoire);
	return EXIT_SUCCESS;

} // iInitXMLBinNAENnRecord

#pragma pack()
