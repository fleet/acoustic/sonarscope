/*
 * ARC_processNAENnRecord.h
 *
 *  Created on: 21 nov. 2008
 *      Author: rgallou
 */
#pragma pack(1)

#ifndef ARC_PROCESSNAENnRECORD_H_
#define ARC_PROCESSNAENnRECORD_H_

#include "ARC_DATAGRAMS.H"


// D�finition de la structure NAENn
typedef struct  {
	// On d�crit la taille fixe des chaines comprenant la virgule de s�paration.
	char	BeginMsg[7];
	char	Date[9];	// JJ/MM/AA
	char	Heure[13];  // HH:MM:SS.DDD
	char	TypeEngin[6];
	char	Latitude[14];
	char	Longitude[15];
	char	Immersion[8];
	char	CoordX[8];
	char	CoordY[8];
	char	CoordZ[8];
	char	CapEngin[8];
	char	LochEngin[7];
	char	RouteFond[7];
	char	VitesseFond[2];
	char	VitesseZ[5];
	char	DistanceOblique[3];
	char	DistanceHorizontale[4];
} T_NAENn_RECORD_IMAGE;

int iProcessNAENnRecord(FILE *fpFileData,
						int iNbRecord,
						T_ARCRECORDXML *strARCRecordXML);


int iInitXMLBinNAENnRecord(	char *cTitleDatagram,
								char *cNomDatagram,
								char *cComments,
								T_ARCRECORDXML *strARCRecordXML);

#endif /* ARC_PROCESSNAENnRECORD_H_ */

#pragma pack()
