/*
 * ARC_processNASYnRecord.h
 *
 *  Created on: 21 nov. 2008
 *      Author: rgallou
 */
#pragma pack(1)

#ifndef ARC_PROCESSNASYNRECORD_H_
#define ARC_PROCESSNASYNRECORD_H_

#include "ARC_DATAGRAMS.H"

// D�finition de la structure NASYn
typedef struct  {
	// On d�crit la taille fixe des chaines comprenant la virgule de s�paration.
	char	BeginMsg[7];
	char	Date[9];					// JJ/MM/AA
	char	Heure[13];  				// HH:MM:SS.DDD
	char	TypeRecord[6];
	char	Latitude[14];
	char	Longitude[15];
	char	FlagDifferentiel[2];
	char	Hdop[5];
	char	SystGeodesie[5];
	char	DateRecepteur[9];			// JJ/MM/AA
	char	HeureRecepteur[9];			// HH:MM:SS.DDD
	char	AttitudeOrigine[4];			// AT1, AT2, INT ...
	char	CapInstantane[7];
	char	RoulisInstantane[6];
	char	TangageInstantane[6];
	char	PilonnementInstantane[5];	// sans la virgule.
} T_NASYn_RECORD_IMAGE;

int iProcessNASYnRecord(FILE *fpFileData,
						int iNbRecord,
						T_ARCRECORDXML *strARCRecordXML);

int iInitXMLBinNASYnRecord(	char *cTitleDatagram,
							char *cNomDatagram,
							char *cComments,
							T_ARCRECORDXML *strARCRecordXML);

#endif // ARC_PROCESSNASYNRECORD_H_

#pragma pack()
