// ----------------------------------------------------------------------
// Programme :
//	CONVERT_ARC.C
//
// Objet :
//	Convertir un fichier au format ARC pour d�composer en fichiers
//	XML et fichiers binaires. Les fichiers XML decrivent le contenu des
// 	binaires.
//  Le format ARc est compos� de sous-ensembles: Nav, Bathy, Attit, ...
//  Une description est disponible sous :
//	http://www.ifremer.fr/sismer/program/geophys/format_geophy/index_formats.htm
// 	Chaque fichier binaire comporte une donn�e �l�mentaire des paquets par ping
// 	issue des paquets de donn�es du ARC.
//	Il a ete realise sous l'IDE Eclipse 3.4.1 avec compilateur gcc.
//
// Cr�ation :
//	02/10/2008
// Modif. :
//	29/09/2009
//
// Auteur :
//  GLU : (ATL) roger.gallou@altran.com
//		  02 98 05 43 21
//
// Historique des modifications :
// - v.Main
//
// ----------------------------------------------------------------------
#pragma pack(1) // Pour la compilation sous Visual Studio
				// et l'alignement sur 8 octets.

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <errno.h>
#include <libgen.h>
#include <ctype.h>

#include "ARC_utilities.h"
#include "ARC_Datagrams.h"
#include "convertFiles_ARC.h"
#include "ARC_Datagrams.h"
#include "ARC_processNACOURecord.h"
#include "ARC_processNACONRecord.h"
#include "ARC_processNASYnRecord.h"
#include "ARC_processNAENnRecord.h"
#include "ARC_processEM12DBATHYRecord.h"
#include "ARC_processEM1000BATHYRecord.h"
#include "ARC_processEM12D_EM1000IMARecord.h"
#include "ARC_processCELERITERecord.h"
#include "ARC_processFileIndexRecord.h"

#include "Generic_Utilities.h"    // Contenu dans la librairie libCONVERT_utils.a

// Fonction de d�claration de la fonction principale de conversion.
// Param�tres :
// - cFileName : r�pertoire et nom du fichier de donn�es
// - barre de progression optionnelle dans l'appel de fonction.
int iConvertFiles_ARC(const char *cFileName, void(*progress)(int))
{
	int 		iRet = 0,
				iLoop,
				iFlagTrouve = FALSE,
				iNbDatagramNotTranslate = 0,
				iSizeDatagram;

	// Adressage d'un fichier potentiellement sup. � 2 Go.
	off64_t 	llPosFic,
				llNbOcFic,
				llPositionDebDatagram = 0;

	FILE *fpFileData;

	// Variables utiles pour le d�codage de la navigation - paquet NACON.
	char	cSystSupp;
	int		iSystSupp;

	T_TYPEOFDATAGRAMNOTTRANSLATE 		tabStrDatagramNotTranslate[30];

	T_FILEINDEX_RECORD_IMAGE 			strImageFileIndexRecord;

	T_EM1000BATHY_RECORD_SIGNAL 		strSignalEM1000BATHYRecord;
	T_EM12D_EM1000IMA_RECORD_SIGNAL 	strSignalEM12D_EM1000IMARecord;
	T_EM12DBATHY_RECORD_SIGNAL 			strSignalEM12DBATHYRecord;
	T_CELERITE_RECORD_SIGNAL 			strSignalCELERITERecord;

	T_ARCRECORDXML	strNACOURecordXML;
	T_ARCRECORDXML 	strNASYnRecordXML;
	T_ARCRECORDXML 	strNACONRecordXML;
	T_ARCRECORDXML 	strNAENnRecordXML;
	T_ARCRECORDXML 	strEM12DBATHYRecordXML;
	T_ARCRECORDXML 	strEM1000BATHYRecordXML;
	T_ARCRECORDXML 	strEM12D_EM1000IMARecordXML;
	T_ARCRECORDXML 	strEM300IMARecordXML;
	T_ARCRECORDXML 	strCELERITERecordXML;
	T_ARCRECORDXML 	strFileIndexRecordXML;


	strNACOURecordXML.iNbDatagram = 0;
	strNASYnRecordXML.iNbDatagram = 0;
	strNACONRecordXML.iNbDatagram = 0;
	strNAENnRecordXML.iNbDatagram = 0;
	strEM12DBATHYRecordXML.iNbDatagram = 0;
	strEM1000BATHYRecordXML.iNbDatagram = 0;
	strEM12D_EM1000IMARecordXML.iNbDatagram = 0;
	strEM300IMARecordXML.iNbDatagram = 0;
	strCELERITERecordXML.iNbDatagram = 0;
	strFileIndexRecordXML.iNbDatagram = 0;




	// Initialisation des datagrammes d�j� trouv�s.
	for (iLoop=0; iLoop<30; iLoop++)
	{
		memset (tabStrDatagramNotTranslate[iLoop].cType, 0, sizeof (tabStrDatagramNotTranslate[iLoop].cType));
		tabStrDatagramNotTranslate[iLoop].iNbTimes = 0;
	}

	// Ouverture du fichier All
	fpFileData = fopen64(cFileName,"rb");
	if(fpFileData  == NULL)
	{
		printf("\nProblem in access Data file : %s\n", cFileName);
		goto ERROR_OUT;

	}

    // Calcul de la taille du fichier.
	fseeko64(fpFileData, 0, SEEK_END); //On se positionne � la fin du fichier
    llNbOcFic=ftello64(fpFileData);
    fseeko64(fpFileData, 0, SEEK_SET);
    llPosFic = 0;

    llPositionDebDatagram = 0;

    //Deroulement entier du fichier .ARC
	while(!feof(fpFileData) && llPositionDebDatagram < llNbOcFic-1)
	{
	    llPosFic = ftello64(fpFileData);

	    // Affichage de la progression dans la fen�tre d'ex�cution
#ifdef GUI
	    if(progress)
	    	progress(100*(double)llPosFic/(double)llNbOcFic);
#else
	    util_vProgressBar(0, llPosFic, llNbOcFic);
#endif
		fseeko64(fpFileData,llPositionDebDatagram,SEEK_SET);

		// Lecture du paquet de donn�es commun aux datagrammes.
		iRet = iProcessFileIndexRecord(fpFileData,
								strFileIndexRecordXML.iNbDatagram,
								&strFileIndexRecordXML,
								&strImageFileIndexRecord);
		strFileIndexRecordXML.iNbDatagram++;
		// On revient en arri�re de la lecture du paquet commun
		// car celui-ci est incorpor� � chaque paquet.
		llPosFic = ftello64(fpFileData);


		// Traitement au cas par cas des datagrammes
		if (!strncmp(strImageFileIndexRecord.TypeRecord,"IMA",3))
		{
			// Si le type de Sondeur est EM300
			if (!strncmp(strImageFileIndexRecord.BeginMsg,"$PIFM",5))
			{
			}
			else
			{
				iRet = iProcessEM12D_EM1000IMARecord(fpFileData,
												strEM12D_EM1000IMARecordXML.iNbDatagram,
												&iSizeDatagram,
												&strEM12D_EM1000IMARecordXML,
												&strSignalEM12D_EM1000IMARecord);
				strEM12D_EM1000IMARecordXML.iNbDatagram++;
			}

		    // Ecriture de la taille des datagrammes dans la structure associ�e au File Index
			fwrite(	&iSizeDatagram,
					strFileIndexRecordXML.strTabImageXML[5].usSize,
					strFileIndexRecordXML.strTabImageXML[5].usNbElem,
					strFileIndexRecordXML.strTabImageXML[5].ptr_fpBin);

			strImageFileIndexRecord.DatagramLength = iSizeDatagram;
		}
		else if (!strncmp(strImageFileIndexRecord.TypeRecord,"CEL",3))
		{
			iRet = iProcessCELERITERecord(fpFileData,
											strCELERITERecordXML.iNbDatagram,
											&strCELERITERecordXML,
											&strSignalCELERITERecord);
			strCELERITERecordXML.iNbDatagram++;
		}
		else if (!strncmp(strImageFileIndexRecord.TypeRecord,"BATHY",5))
		{
		    iRet = iProcessEM1000BATHYRecord(fpFileData,
										strEM1000BATHYRecordXML.iNbDatagram,
										&strEM1000BATHYRecordXML,
										&strSignalEM1000BATHYRecord);
		    strEM1000BATHYRecordXML.iNbDatagram++;
		}
		else if (!strncmp(strImageFileIndexRecord.TypeRecord,"VOIE",4))
		{
		    iRet = iProcessEM12DBATHYRecord(fpFileData,
											strEM12DBATHYRecordXML.iNbDatagram,
											&strEM12DBATHYRecordXML,
											&strSignalEM12DBATHYRecord);
		    strEM12DBATHYRecordXML.iNbDatagram++;
		}
		else if (!strncmp(strImageFileIndexRecord.TypeRecord,"NASY",4))
		{
		    iRet = iProcessNASYnRecord(	fpFileData,
										strNASYnRecordXML.iNbDatagram,
										&strNASYnRecordXML);
		    strNASYnRecordXML.iNbDatagram++;
		}
		else if (!strncmp(strImageFileIndexRecord.TypeRecord,"NACOU",5))
		{
		    iRet = iProcessNACOURecord(	fpFileData,
										strNACOURecordXML.iNbDatagram,
										&strNACOURecordXML);
		    strNACOURecordXML.iNbDatagram++;
		}
		else if (!strncmp(strImageFileIndexRecord.TypeRecord,"NAEN",4))
		{
		    iRet = iProcessNAENnRecord(	fpFileData,
										strNAENnRecordXML.iNbDatagram,
										&strNAENnRecordXML);
		    strNAENnRecordXML.iNbDatagram++;
		}
		else if (!strncmp(strImageFileIndexRecord.TypeRecord,"NACON",5))
		{
		    // D�termination du nombre de syst�mes suppl�mentaires.
			fseeko64(fpFileData,llPositionDebDatagram+367,SEEK_SET);
			cSystSupp = fgetc(fpFileData);
			// Repositionnement au d�but du datagramme.
			fseeko64(fpFileData,llPositionDebDatagram,SEEK_SET);
			sscanf(&cSystSupp,"%d",&iSystSupp);
			if(isdigit(cSystSupp))
			{
			    iRet = iProcessNACONRecord(	fpFileData,
											iSystSupp,
											strNACONRecordXML.iNbDatagram,
											&strNACONRecordXML);
			}
			else
			{
				printf("The value you gave was %c, which was NOT a number!!\n",cSystSupp);
				goto ERROR_OUT;
			}
		    strNACONRecordXML.iNbDatagram++;
		}
		else
		{
			// Recherche de datagrammes non d�cod�s d�j� trouv�s.
			for (iLoop=0; iLoop<20; iLoop++)
			{
				if (!strcmp((const char *)strImageFileIndexRecord.TypeRecord,(const char *)tabStrDatagramNotTranslate[iLoop].cType))
				{
					tabStrDatagramNotTranslate[iLoop].iNbTimes++;
					iFlagTrouve = TRUE;
					break;
				}
			}
			if (iFlagTrouve == FALSE)
			{
				strcpy((char *)tabStrDatagramNotTranslate[iNbDatagramNotTranslate].cType,(const char *)strImageFileIndexRecord.TypeRecord);
				tabStrDatagramNotTranslate[iNbDatagramNotTranslate].iNbTimes++;
				iNbDatagramNotTranslate++;
			}
			iFlagTrouve = FALSE;
		}

		// Calage de l'index de lecture sur le datagramme suivant.
		llPositionDebDatagram = llPositionDebDatagram + strImageFileIndexRecord.DatagramLength;

	} // Fin de la boucle while de lecture du fichier.

	// End of progress bar
	if(progress)
		progress(100);

	// Fermeture en lecture du fichier de donn�es.
	fclose(fpFileData);

	printf("Datagrams not translated : %d\n", iNbDatagramNotTranslate);
	// Recherche de datagrammes non d�cod�s d�j� trouv�s.
	for (iLoop=0; iLoop<iNbDatagramNotTranslate; iLoop++)
	{
		printf("Process datagram : Datagram %s (%s) found %d times not translated\n",	tabStrDatagramNotTranslate[iLoop].cType, tabStrDatagramNotTranslate[iLoop].cType, tabStrDatagramNotTranslate[iLoop].iNbTimes);
	}
	// Fermeture des fichiers binaires des paquets et �criture des XML.
	utilARC_iSaveAndCloseFiles(G_cRepData, &strFileIndexRecordXML);
	if (strEM12D_EM1000IMARecordXML.iNbDatagram != 0)
	{
		// Recopie du type de sondeur.
		strEM12D_EM1000IMARecordXML.ptr_cModelHeader = (char*)malloc(256*sizeof(char));
		strEM12D_EM1000IMARecordXML.ptr_cModelHeader = strFileIndexRecordXML.ptr_cModelHeader;

		utilARC_iSaveAndCloseFiles(G_cRepData, &strEM12D_EM1000IMARecordXML);
	}
	if (strCELERITERecordXML.iNbDatagram != 0)
	{
		// Recopie du type de sondeur.
		strCELERITERecordXML.ptr_cModelHeader = (char*)malloc(256*sizeof(char));
		strCELERITERecordXML.ptr_cModelHeader = strFileIndexRecordXML.ptr_cModelHeader;

		utilARC_iSaveAndCloseFiles(G_cRepData, &strCELERITERecordXML);
	}
	if (strEM1000BATHYRecordXML.iNbDatagram != 0)
	{
		// Recopie du type de sondeur.
		strEM1000BATHYRecordXML.ptr_cModelHeader = (char*)malloc(256*sizeof(char));
		strEM1000BATHYRecordXML.ptr_cModelHeader = strFileIndexRecordXML.ptr_cModelHeader;

 	    utilARC_iSaveAndCloseFiles(G_cRepData, &strEM1000BATHYRecordXML);
	}
	if (strEM12DBATHYRecordXML.iNbDatagram != 0)
	{
		// Recopie du type de sondeur.
		strEM12DBATHYRecordXML.ptr_cModelHeader = (char*)malloc(256*sizeof(char));
		strEM12DBATHYRecordXML.ptr_cModelHeader = strFileIndexRecordXML.ptr_cModelHeader;

 	    utilARC_iSaveAndCloseFiles(G_cRepData, &strEM12DBATHYRecordXML);
	}
	if (strNASYnRecordXML.iNbDatagram != 0)
	{
		// Recopie du type de sondeur.
		strNASYnRecordXML.ptr_cModelHeader = (char*)malloc(256*sizeof(char));
		strNASYnRecordXML.ptr_cModelHeader = strFileIndexRecordXML.ptr_cModelHeader;

		utilARC_iSaveAndCloseFiles(G_cRepData, &strNASYnRecordXML);
	}
	if (strNACOURecordXML.iNbDatagram != 0)
	{
		// Recopie du type de sondeur.
		strNACOURecordXML.ptr_cModelHeader = (char*)malloc(256*sizeof(char));
		strNACOURecordXML.ptr_cModelHeader = strFileIndexRecordXML.ptr_cModelHeader;

		utilARC_iSaveAndCloseFiles(G_cRepData, &strNACOURecordXML);
	}
	if (strNAENnRecordXML.iNbDatagram != 0)
	{
		// Recopie du type de sondeur.
		strNAENnRecordXML.ptr_cModelHeader = (char*)malloc(256*sizeof(char));
		strNAENnRecordXML.ptr_cModelHeader = strFileIndexRecordXML.ptr_cModelHeader;

		utilARC_iSaveAndCloseFiles(G_cRepData, &strNAENnRecordXML);
	}
	if (strNACONRecordXML.iNbDatagram != 0)
	{
		// Recopie du type de sondeur.
		strNACONRecordXML.ptr_cModelHeader = (char*)malloc(256*sizeof(char));
		strNACONRecordXML.ptr_cModelHeader = strFileIndexRecordXML.ptr_cModelHeader;

		utilARC_iSaveAndCloseFiles(G_cRepData, &strNACONRecordXML);
	}



	return EXIT_SUCCESS;

	ERROR_OUT:

	// End of progress bar
	if(progress){
		progress(50);
		progress(100);
	}

	if (fpFileData != NULL) {
	  fclose(fpFileData);
	  fpFileData = 0;
	}

	return EXIT_FAILURE;
} // iConvertFiles_ARC


#pragma pack()
