// ----------------------------------------------------------------------
// Programme :
//	CONVERT_ARC.C
//
// Objet :
//	Convertir un fichier au format ARC pour d�composer en fichiers
//	XML et fichiers binaires. Les fichiers XML decrivent le contenu des
// 	binaires.
// 	Chaque fichier binaire comporte une donn�e �l�mentaire des paquets par ping
// 	issue des paquets de donn�es du ARC.
//	Il a ete realise sous l'IDE Eclipse Galileo avec compilateur gcc.
//
// Cr�ation :
//	24/08/2010
// Modif. :
//	/
//
// Auteur :
//  GLU : (ATL) roger.gallou@altran.com
//		  02 98 05 43 21
//
// Historique des modifications :
//
// ----------------------------------------------------------------------
#pragma pack(1) // Pour la compilation sous Visual Studio
// et l'alignement sur 8 octets.


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <errno.h>
#include <libgen.h>

#include "ARC_utilities.h"
#include "convertFiles_ARC.h"
#include "ARC_Datagrams.h"


#include "Generic_Utilities.h"    // Contenu dans la librairie libCONVERT_utils.a

#ifdef GUI
#include "ProgressInfo.h"
#endif

int main(int argc, char **argv) {
	char 	cFileName[MAX_PATH],
			cFileNameARC[MAX_PATH],
			cFileNameTmp[MAX_PATH],
			cDirSonarScope[MAX_PATH],
			cDirFileData[MAX_PATH],
			cFileDescription[MAX_PATH],
			cFileDrive[MAX_PATH],
			cFlagProgressBar[5];


	int 	iRet = 0,
			iDummy,
			iErr,
			iProgressBar;


	if (argc < 3) {
		printf("Usage: CONVERT_ARC <input .ARC file>\n");
		printf("       Reads an ARC file and prints some data about it.\n");
		return FALSE;
	}

	G_cRepData 			= calloc(MAX_PATH, sizeof(char));
	G_cFileData 		= calloc(MAX_PATH, sizeof(char));
	G_cFileExtension 	= calloc(MAX_PATH, sizeof(char));

	util_GetFileDrive(argv[2], cFileDrive);
	util_GetFileDirectory(argv[2], cDirFileData);
	util_GetFileName(argv[2], cFileName);
	util_GetFileType(argv[2], G_cFileExtension, cFileDescription);


	//R�cup�ration du flag de Progress Bar
	memcpy(cFlagProgressBar, argv[1], strlen(argv[1]));
	iProgressBar = (int)atoi(cFlagProgressBar);

	printf("File Process : %s\n", argv[2]);
	//memcpy(G_cFileData, argv[2], strlen(argv[2]));
	//sprintf(cFileName, "%s", basename(argv[2]));
	//G_cFileExtension = (char*) strrchr(cFileName, '.');

	if (G_cFileExtension == NULL) {
		// pas d'extension
		printf("le fichier %s n'a pas d'extension\n", cFileName);
		goto ERROR_OUT;
	}

	sprintf(cDirFileData, "%s", dirname(argv[2]));
	sprintf(cDirSonarScope, "%s\\%s", cDirFileData, "SonarScope");
	iDummy = access(cDirSonarScope, F_OK);
	if (iDummy != 0) {
		iErr = mkdir(cDirSonarScope);
		if (iErr != 0) {
			switch (errno) {
			// "File exists", on ne fait rien.
			case EEXIST:
				break;
				// Autres cas d'erreurs
			default:
				printf("Error in Directory creating : %s\n", strerror(errno));
				goto ERROR_OUT;
				break;
			}
		}
	}
	cFileName[strlen(cFileName) - 3] = '\0'; //Extension sur 3 caract�res y compris le '.'
	sprintf(cFileNameARC, "%s\\%s.%s", cDirFileData, cFileName, G_cFileExtension);
	sprintf(cFileNameTmp, "%s_tmp", cFileName);
	sprintf(G_cRepData, "%s\\%s", cDirSonarScope, cFileNameTmp);
	if (DEBUG)
		printf("-- Directory Creation : %s\n", G_cRepData);
	iErr = mkdir(G_cRepData);
	if (iErr != 0) {
		switch (errno) {
		// "File exists", on ne fait rien.
		case EEXIST:
			break;
			// Autres cas d'erreurs
		default:
			printf("Error in Directory creating : %s\n", strerror(errno));
			break;
		}
	}
	G_iFlagEndian = util_iGetIndianAll(cFileNameARC);
	#ifndef GUI
		iRet = iConvertFiles_ARC(cFileNameARC, NULL);
	#else
		if (iProgressBar == 1)
			runWithProgress(argc, argv, &iConvertFiles_ARC, cFileNameARC);
		else
			iRet = iConvertFiles_ARC(cFileNameARC, NULL);
	#endif


	iRet = util_iRenameDir(G_cRepData);
	if (iRet == 0)
		printf("Process Ending : renaming Temporary Directory %s\n", G_cRepData);

	free(G_cRepData);
	free(G_cFileData);
	free(G_cFileExtension);

	return EXIT_SUCCESS;

	ERROR_OUT:
		return EXIT_FAILURE;

} // Main

#pragma pack()
