#ifndef HAC_TUPLES_H_
#define HAC_TUPLES_H_

#include "stdio.h"

#include "Generic_Utilities.h"    // Contenu dans la librairie libCONVERT_utils.a

// Description des contenants de variables �l�mentaire de description XML
// Peut servir � d�crire des attributs dont l'�criture au format XML est
// limit�e.
typedef struct {
	   char				cNom[50],			// Label de la variable
					    cTag[50],			// Tag identifiant de la variable
					    cUnit[50],			// Unit� de la variable
						cType[30],			// Type WORD, String, ...
						cNomPathFileBin[300];	// Nom et chemin du fichier Bin
	   FILE				*ptr_fpBin;			// Pointeur du fichier de signaux
	   BOOL 			bFlagConstant;		// Indicateur de constance de la variable
	   unsigned short	usSize;				// n octets
	   unsigned short	usNbElem;			// Nombre de variables �l�mentaires
	   void				*ptr_vPrevVal;		// Valeur pr�c�dente de lecture
	   float			fScaleFactor;		// yVraie = Val*S_F + A_O
	   float			fAddOffset;			//
} T_HACVARXML;

typedef struct {
	   T_HACVARXML	*strTabSignalXML;		// Description des signaux d'un paquet
	   T_HACVARXML	*strTabImageXML;		// Description des images d'un paquet
	   T_HACVARXML	*strTabImage2XML;		// Description des 2i�me images d'un paquet (
	   char			*ptr_cNomHeader,		// Nom du Paquet
					*ptr_cLabelHeader,		// Label du paquet
					*ptr_cCommentsHeader,	// Commentaires du paquet
	   				*ptr_cModelHeader,		// Mod�le du syst�me.
	   				*ptr_cSerialNumberHeader,// SN du syst�me.
				    *ptr_cNomIndexTable,	// Nom de la variable d'indexation des
											// variables de type table.
					*ptr_cNomIndexTable2;	// Nom de la variable d'indexation des
	   										// variables de type table (2i�me image).
	   int			iNbDatagram;			// Nombre d'occurrence des donn�es.
	   short		sNbVarSignal;			// Nombre de variables de type signal.	short		sNb;	// Nombre de signaux du RD
	   short		sNbVarImage;			// Nombre de variables de type image.
	   short		sNbVarImage2;			// Nombre de variables de type image (pour la 2i�me image).

} T_HACRECORDXML;

typedef struct
{
	unsigned short 	uiType;
	int				iNbTimes;
} T_TYPEOFTUPLESNOTTRANSLATED;

// Description unitaire des signaux
typedef struct  {
	char 			*cTabNomVar,
					*cTabNomType;
	unsigned short	usNbElem;
	float 			fScaleFactor;
	float 			fAddOffset;
} T_DESC_SIGNAL_OR_IMAGE_PB; // Pb de Pack !!!

// Description unitaire des signaux
typedef struct  {
	char 			*cTabNomVar,
					*cTabNomType;
	float 			fScaleFactor;
	float 			fAddOffset;
} T_DESC_SIGNAL_OR_IMAGE;

// Description unitaire des signaux
typedef struct  {
	unsigned short 				usNbVarSignal;
	unsigned short 				usNbVarImage;
	unsigned short 				usNbVarImage2;
	short 						sNumIndexTable;
	T_DESC_SIGNAL_OR_IMAGE  	*strDescSignal;
	T_DESC_SIGNAL_OR_IMAGE  	*strDescImage;
	T_DESC_SIGNAL_OR_IMAGE  	*strDescImage2;
} T_DESC_TAB_SIGANDIMG;

// Description de l'Arborescence des Tuples d'identification d'EchoSounder et de Channel.
typedef struct
{
	// SoftwareChannel stock� obligatoirement en int sous peine de pb dans
	// la transmission des tableaux de structure en param�tres.
	int	 				iIndexTupleRoot;
	int	 				iIndexTupleParent;
	unsigned int	 	SoftwareChannelIdentifier;
	unsigned int	 	EchosounderDocIdentifier;
} T_SOUNDERANDCHANNELID;

// Description en tables de l'Arborescence des Tuples d'identification d'EchoSounder et de Channel.
typedef struct {
	T_SOUNDERANDCHANNELID	tabStrSounderAndChannel[1000]; // Addition des channels de tous les Soundeurs.
	unsigned short	 		usNbSoftwareChannelId;
} T_DESC_TAB_SOUNDERANDCHANNELID;

// Description de l'Arborescence des Tuples d'identification d'EchoSounder-Channel-Attitude Sensor
typedef struct
{
	// SoftwareChannel stock� obligatoirement en int sous peine de pb dans
	// la transmission des tableaux de structure en param�tres.
	int	 				iIndexTupleRoot;
	int	 				iIndexTupleParent;
	unsigned int	 	SoftwareChannelIdentifier;
	unsigned int	 	SensorIdentifier;
} T_CHANNELSENSORID;

// Description en tables de l'Arborescence des Tuples d'identification d'EchoSounder-Channel-Attitude Sensor.
typedef struct {
	T_CHANNELSENSORID	tabStrSensorSounderChannel[1000]; // Addition des channels de tous les Soundeurs.
	unsigned short	 			usNbSoftwareChannelId;
} T_DESC_TAB_CHANNELSENSORID;

#endif
