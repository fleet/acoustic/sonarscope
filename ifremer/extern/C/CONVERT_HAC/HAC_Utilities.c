#pragma pack(1) // Pour la compilation sous Visual Studio
				// et l'alignement sur 8 octets.

/*
 * HAC_utilities.c
 *
 *  Created on: 23 Mai 2010
 *      Author: rgallou
 */
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <io.h>

#include "HAC_utilities.h"
#include "convertFiles_HAC.h"



//////////////////////////////////////////////////////////////////////
// Fonction :    utilHAC_iSaveAndCloseFiles
// Objet     :    Sauvegarde de la structure XML dans un fichier.
// Modification : 15/10/08
// Auteur     : GLU
//////////////////////////////////////////////////////////////////////
int utilHAC_iSaveAndCloseFiles( int iSounderId,
								char *cRepData,
                                T_HACRECORDXML *strHACRecordXML)
{
    char    *cFileNameXML,
    		*ptr_cNomHeader,
            *cTypeMatLab,
            cNomFicBin[200],
            *cValeur = NULL,
            cValOneChar[1];

    int     iSize,
            iVarSignal,
            iVarImage,
            iRet = 0;

    unsigned short     usNbElem,
                    usTailleEnByte;

    FILE        *fpXML = NULL;

    iSize = strlen(strHACRecordXML->ptr_cNomHeader) + 1;
    ptr_cNomHeader 	= (char *)malloc(iSize*sizeof(char));
    ptr_cNomHeader 	= strHACRecordXML->ptr_cNomHeader;
    iSize 			= 100 + strlen(cRepData) + strlen(ptr_cNomHeader) ; // Nom du paquet + '.xml'
    cFileNameXML 	= calloc(iSize, sizeof(char));
    // Si Extension du fichier d'origine = '.Hac'
	if (!stricmp(G_cFileExtension, "hac"))
		// Pour certains typles de tuple, distribution de ceux-ci par Sounder.
		if (iSounderId != -1)
		{
			sprintf(cFileNameXML, "%s\\%s%d\\HAC_%s%s", cRepData, "HAC_Sounder", iSounderId+1, ptr_cNomHeader, ".xml");
		}
		else
		{
			sprintf(cFileNameXML, "%s\\HAC_%s%s", cRepData, ptr_cNomHeader, ".xml");
		}
	else
	{
		printf("L'extension du fichier n'est pas reconnue : %s\n", G_cFileExtension);
		return EXIT_FAILURE;
	}

	// Probl�me de fuite m�moire ???? - Gestion au burin.
	if (iSounderId != 2 && stricmp(ptr_cNomHeader, "AttitudeSensor"))
    {
    	printf("-- XML File Creation : %s \n", &cFileNameXML[0]);
    }
    else
    {
    	printf("-- XML File Creation : %s\\HAC_Sounder3\\HAC_AttitudeSensor.xml \n", cRepData);
    }


    // Cr�ation de l'ent�te de doc XML
    fpXML = fopen(cFileNameXML, "w+");
    fprintf(fpXML, "%s", "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r");
    fprintf(fpXML, "%s", "<ROOT>\r");
    fprintf(fpXML, "%s%s%s", "\t<Title>", strHACRecordXML->ptr_cLabelHeader, "</Title>\r");
    fprintf(fpXML, "%s%d%s", "\t<NbDatagrams>", strHACRecordXML->iNbDatagram, "</NbDatagrams>\r");
    //fprintf(fpXML, "%s%s%s", "\t<TimeOrigin>", "01/01/-4713", "</TimeOrigin>\r");
    fprintf(fpXML, "%s%s%s", "\t<Comments>", strHACRecordXML->ptr_cCommentsHeader, "</Comments>\r");
    if (strHACRecordXML->sNbVarSignal > 0)
    {
        fprintf(fpXML, "%s", "\t<Variables>\r");

        // Traitement sur l'ensemble des valeurs du RTH.
        for (iVarSignal=0; iVarSignal<strHACRecordXML->sNbVarSignal; iVarSignal++)
        {
            cTypeMatLab = (char*)malloc(10*sizeof(char));
            iRet = util_cConvertTypeLabel(    strHACRecordXML->strTabSignalXML[iVarSignal].cType,
                                            cTypeMatLab);

            fprintf(fpXML, "%s", "\t\t<item>\r");
            fprintf(fpXML, "%s%s%s", "\t\t\t<Name>", strHACRecordXML->strTabSignalXML[iVarSignal].cNom, "</Name>\r");
            fprintf(fpXML, "%s%s%s", "\t\t\t<Storage>", cTypeMatLab, "</Storage>\r");
            if (strHACRecordXML->strTabSignalXML[iVarSignal].usNbElem > 1)
            {
                fprintf(fpXML, "%s%d%s", "\t\t\t<NbElem>",  strHACRecordXML->strTabSignalXML[iVarSignal].usNbElem, "</NbElem>\r");
            }
            //Infos report�es dans format SSC-XML-Bin : fprintf(fpXML, "%s%s%s", "\t\t\t<Unit>", strHACRecordXML->strTabSignalXML[iVarSignal].cUnit, "</Unit>\r");
            //Infos report�es dans format SSC-XML-Bin : fprintf(fpXML, "%s%s%s", "\t\t\t<Tag>", strHACRecordXML->strTabSignalXML[iVarSignal].cTag, "</Tag>\r");

            // Fermeture pr�alable du fichier.
            if (strHACRecordXML->strTabSignalXML[iVarSignal].ptr_fpBin != NULL)
            {
                fclose(strHACRecordXML->strTabSignalXML[iVarSignal].ptr_fpBin);
            }

            // R�cup�ration de la variable d�tect�e comme constante.
            usNbElem = strHACRecordXML->strTabSignalXML[iVarSignal].usNbElem;
            usTailleEnByte = util_usNbOctetTypeVar(strHACRecordXML->strTabSignalXML[iVarSignal].cType);

            if (strHACRecordXML->strTabSignalXML[iVarSignal].bFlagConstant == TRUE)
            {
                // Pb de lib�ration de m�moire si  usNbElem * usTailleEnByte = 1
            	if (usNbElem != 1 || usTailleEnByte != 1)
                {
					 // Allocation de la valeur � r�cup�rer selon la taille. Si elle est
					 // du type Chaine, elle doit contenir autant d'�l�ments que d�clar�s.
					 if (strcmp(cTypeMatLab, "char"))
					 {
						 cValeur = (char*)malloc(256*sizeof(char));
						 usNbElem = strHACRecordXML->strTabSignalXML[iVarSignal].usNbElem;
	                 	 iRet = util_cValVarFromFile(strHACRecordXML->strTabSignalXML[iVarSignal].cNomPathFileBin,
	                 								strHACRecordXML->strTabSignalXML[iVarSignal].cType,
	                 								usNbElem,
	                 								cValeur);
	                     fprintf(fpXML, "%s%s%s", "\t\t\t<Constant>", cValeur, "</Constant>\r");
					 }
					 else
					 {
						 usNbElem = strHACRecordXML->strTabSignalXML[iVarSignal].usNbElem + 1;
						 cValeur = (char*)malloc((usNbElem)*sizeof(char));
	                 	 iRet = util_cValVarFromFile(strHACRecordXML->strTabSignalXML[iVarSignal].cNomPathFileBin,
	                 								strHACRecordXML->strTabSignalXML[iVarSignal].cType,
	                 								usNbElem,
	                 								cValeur);
	                     fprintf(fpXML, "%s%s%s", "\t\t\t<Constant>'", cValeur, "'</Constant>\r");
					 }
					 free(cValeur);
                }
                else
                {
//                    iRet = utilHAC_cValVarFromFile(strHACRecordXML->strTabSignalXML[iVarSignal],
//                    		&cValOneChar[0]);
                	iRet = util_cValVarFromFile(strHACRecordXML->strTabSignalXML[iVarSignal].cNomPathFileBin,
                								strHACRecordXML->strTabSignalXML[iVarSignal].cType,
                								strHACRecordXML->strTabSignalXML[iVarSignal].usNbElem,
                								&cValOneChar[0]);
                    fprintf(fpXML, "%s%s%s", "\t\t\t<Constant>", &cValOneChar[0], "</Constant>\r");
                }
 				// Effacement du fichier Bin
            	iRet = remove((const char *)strHACRecordXML->strTabSignalXML[iVarSignal].cNomPathFileBin);
				if (iRet !=0)
				{
				   printf("Cannot remove file %s (%s)\n",
							   strHACRecordXML->strTabSignalXML[iVarSignal].cNomPathFileBin, ERRORSTRING);
				}
            }
            else
            {
                sprintf(cNomFicBin, "%s%s",strHACRecordXML->strTabSignalXML[iVarSignal].cNom, ".bin");
                fprintf(fpXML, "%s%s%s", "\t\t\t<FileName>", cNomFicBin, "</FileName>\r");
            }
            if (strHACRecordXML->strTabSignalXML[iVarSignal].fScaleFactor != 1.0)
				fprintf(fpXML, "%s%g%s", "\t\t\t<ScaleFactor>", strHACRecordXML->strTabSignalXML[iVarSignal].fScaleFactor, "</ScaleFactor>\r");
            if (strHACRecordXML->strTabSignalXML[iVarSignal].fAddOffset != 0.0)
				fprintf(fpXML, "%s%f%s", "\t\t\t<AddOffset>", strHACRecordXML->strTabSignalXML[iVarSignal].fAddOffset, "</AddOffset>\r");
            fprintf(fpXML, "%s", "\t\t</item>\r");
            free(cTypeMatLab);

        } // Fin de la boucle sur les donn�es RTH
        fprintf(fpXML, "%s", "\t</Variables>\r");
        free(strHACRecordXML->strTabSignalXML);
    } // Fin de l'�criture des Signaux

    // Ouverture de la balise Tables
    if (strHACRecordXML->sNbVarImage > 0 || strHACRecordXML->sNbVarImage2 >0)
    {
        fprintf(fpXML, "%s", "\t<Tables>\r");
    }
    // Traitement sur l'ensemble des attributs.
    if (strHACRecordXML->sNbVarImage > 0)
    {
         for (iVarImage=0; iVarImage<strHACRecordXML->sNbVarImage; iVarImage++)
        {
            cTypeMatLab = (char*)malloc(10*sizeof(char));
            iRet = util_cConvertTypeLabel(    strHACRecordXML->strTabImageXML[iVarImage].cType,
                                            cTypeMatLab);

            fprintf(fpXML, "%s", "\t\t<item>\r");
            fprintf(fpXML, "%s%s%s", "\t\t\t<Name>", strHACRecordXML->strTabImageXML[iVarImage].cNom, "</Name>\r");
            fprintf(fpXML, "%s%s%s", "\t\t\t<Storage>", cTypeMatLab, "</Storage>\r");
            if (strHACRecordXML->strTabImageXML[iVarImage].usNbElem > 1)
            {
                fprintf(fpXML, "%s%d%s", "\t\t\t<NbElem>", strHACRecordXML->strTabImageXML[iVarImage].usNbElem, "</NbElem>\r");
            }
            //Infos report�es dans format SSC-XML-Bin : fprintf(fpXML, "%s%s%s", "\t\t\t<Unit>", strHACRecordXML->strTabImageXML[iVarImage].cUnit, "</Unit>\r");
            //Infos report�es dans format SSC-XML-Bin : fprintf(fpXML, "%s%s%s", "\t\t\t<Tag>", strHACRecordXML->strTabImageXML[iVarImage].cTag, "</Tag>\r");

            // Fermeture pr�alable du fichier.
            fclose(strHACRecordXML->strTabImageXML[iVarImage].ptr_fpBin);
            usNbElem = strHACRecordXML->strTabImageXML[iVarImage].usNbElem;
            usTailleEnByte = util_usNbOctetTypeVar(strHACRecordXML->strTabImageXML[iVarImage].cType);
            // Pas d'index reconnu
            if (strcmp(strHACRecordXML->ptr_cNomIndexTable, "NoneIndex") != 0)
            {
                fprintf(fpXML, "%s%s%s", "\t\t\t<Indexation>", strHACRecordXML->ptr_cNomIndexTable, "</Indexation>\r");
            }

            if (strHACRecordXML->strTabImageXML[iVarImage].bFlagConstant == TRUE)
            {
                // Pb de lib�ration de m�moire si  usNbElem * usTailleEnByte = 1
            	if (usNbElem != 1 || usTailleEnByte != 1)
            	{
                	// Allocation de la valeur � r�cup�rer selon la taille. Si elle est
                	// du type Chaine, elle doit contenir autant d'�l�ments que d�clar�s.
                    // Ca marche mais c'est moyen, moyen !!!! (cf. Traitement des signaux)
                	if (strcmp(cTypeMatLab, "char"))
                    {
                        cValeur = (char*)malloc(256*sizeof(char));
                    }
                    else
                    {
                        cValeur = (char*)malloc(usNbElem*sizeof(char));
                    }
                    //iRet = utilHAC_cValVarFromFile(strHACRecordXML->strTabImageXML[iVarImage],
                    //                            cValeur);
                	iRet = util_cValVarFromFile(strHACRecordXML->strTabImageXML[iVarImage].cNomPathFileBin,
                								strHACRecordXML->strTabImageXML[iVarImage].cType,
                								strHACRecordXML->strTabImageXML[iVarImage].usNbElem,
                								cValeur);
                    fprintf(fpXML, "%s%s%s", "\t\t\t<Constant>", cValeur, "</Constant>\r");

     				if (!strcmp(cTypeMatLab, "char"))
    				// ???? : pb de lib�ration de memoire si la variable fait un octet.
    				{
    					free(cValeur);
    				}
            	}
 				else
 				{
                    //iRet = utilHAC_cValVarFromFile(strHACRecordXML->strTabImageXML[iVarImage],
                    //		&cValOneChar[0]);
                   	iRet = util_cValVarFromFile(strHACRecordXML->strTabImageXML[iVarImage].cNomPathFileBin,
                    								strHACRecordXML->strTabImageXML[iVarImage].cType,
                    								strHACRecordXML->strTabImageXML[iVarImage].usNbElem,
                    								&cValOneChar[0]);
                    fprintf(fpXML, "%s%s%s", "\t\t\t<Constant>", &cValOneChar[0], "</Constant>\r");
 				}
                // Effacement du fichier
 				if (remove((const char *)strHACRecordXML->strTabImageXML[iVarImage].cNomPathFileBin) !=0)
 				{
 				   printf("Cannot remove file %s (%s)\n",
 							   strHACRecordXML->strTabImageXML[iVarImage].cNomPathFileBin, ERRORSTRING);
 				}
            }
            else
            {
                sprintf(cNomFicBin, "%s%s",strHACRecordXML->strTabImageXML[iVarImage].cNom, ".bin");
                fprintf(fpXML, "%s%s%s", "\t\t\t<FileName>", cNomFicBin, "</FileName>\r");
            }
            if (strHACRecordXML->strTabImageXML[iVarImage].fScaleFactor != 1.0)
            	fprintf(fpXML, "%s%g%s", "\t\t\t<ScaleFactor>", strHACRecordXML->strTabImageXML[iVarImage].fScaleFactor, "</ScaleFactor>\r");
            if (strHACRecordXML->strTabImageXML[iVarImage].fAddOffset != 0.0)
            	fprintf(fpXML, "%s%f%s", "\t\t\t<AddOffset>", strHACRecordXML->strTabImageXML[iVarImage].fAddOffset, "</AddOffset>\r");
            fprintf(fpXML, "%s", "\t\t</item>\r");
            free(cTypeMatLab);
        } // Fin de la boucle sur les donn�es Image
        free(strHACRecordXML->strTabImageXML);
        } //Fin de l'�criture des Images (RD et OD)

    // Traitement d'une 2i�me image (si n�cessaire).
	if (strHACRecordXML->sNbVarImage2 > 0)
	{
		for (iVarImage=0; iVarImage<strHACRecordXML->sNbVarImage2; iVarImage++)
		{
			cTypeMatLab = (char*)malloc(10*sizeof(char));
			iRet = util_cConvertTypeLabel(strHACRecordXML->strTabImage2XML[iVarImage].cType,
											cTypeMatLab);

			fprintf(fpXML, "%s", "\t\t<item>\r");
			fprintf(fpXML, "%s%s%s", "\t\t\t<Name>", strHACRecordXML->strTabImage2XML[iVarImage].cNom, "</Name>\r");
			fprintf(fpXML, "%s%s%s", "\t\t\t<Storage>", cTypeMatLab, "</Storage>\r");
			if (strHACRecordXML->strTabImage2XML[iVarImage].usNbElem > 1)
			{
				fprintf(fpXML, "%s%d%s", "\t\t\t<NbElem>", strHACRecordXML->strTabImage2XML[iVarImage].usNbElem, "</NbElem>\r");
			}
			//Infos report�es dans format SSC-XML-Bin : fprintf(fpXML, "%s%s%s", "\t\t\t<Unit>", strHACRecordXML->strTabImage2XML[iVarImage].cUnit, "</Unit>\r");
			//Infos report�es dans format SSC-XML-Bin : fprintf(fpXML, "%s%s%s", "\t\t\t<Tag>", strHACRecordXML->strTabImage2XML[iVarImage].cTag, "</Tag>\r");
			// Fermeture pr�alable du fichier.
			fclose(strHACRecordXML->strTabImage2XML[iVarImage].ptr_fpBin);
			usNbElem = strHACRecordXML->strTabImage2XML[iVarImage].usNbElem;
			usTailleEnByte = util_usNbOctetTypeVar(strHACRecordXML->strTabImage2XML[iVarImage].cType);
            if (strcmp(strHACRecordXML->ptr_cNomIndexTable2, "NoneIndex") != 0)
            {
				fprintf(fpXML, "%s%s%s", "\t\t\t<Indexation>", strHACRecordXML->ptr_cNomIndexTable2, "</Indexation>\r");
			}

			if (strHACRecordXML->strTabImage2XML[iVarImage].bFlagConstant == TRUE)
			{
                // Pb de lib�ration de m�moire si  usNbElem * usTailleEnByte = 1
            	if (usNbElem != 1 || usTailleEnByte != 1)
                {
					// Allocation de la valeur � r�cup�rer selon la taille. Si elle est
					// du type Chaine, elle doit contenir autant d'�l�ments que d�clar�s.
					if (strcmp(cTypeMatLab, "char"))
					{
						cValeur = (char*)malloc(256*sizeof(char));
					}
					else
					{
						cValeur = (char*)malloc(usNbElem*sizeof(char));
					}
//					iRet = utilHAC_cValVarFromFile(strHACRecordXML->strTabImage2XML[iVarImage],
//												cValeur);
                   	iRet = util_cValVarFromFile(strHACRecordXML->strTabImage2XML[iVarImage].cNomPathFileBin,
                    								strHACRecordXML->strTabImage2XML[iVarImage].cType,
                    								strHACRecordXML->strTabImage2XML[iVarImage].usNbElem,
                    								cValeur);
					fprintf(fpXML, "%s%s%s", "\t\t\t<Constant>", cValeur, "</Constant>\r");
					if (!strcmp(cTypeMatLab, "char")) // ???? : pb de lib�ration de memoire
					{
						free(cValeur);
					}
                }
                else
                {
//                    iRet = utilHAC_cValVarFromFile(strHACRecordXML->strTabImage2XML[iVarImage],
//                    		&cValOneChar[0]);
                   	iRet = util_cValVarFromFile(strHACRecordXML->strTabImage2XML[iVarImage].cNomPathFileBin,
                    								strHACRecordXML->strTabImage2XML[iVarImage].cType,
                    								strHACRecordXML->strTabImage2XML[iVarImage].usNbElem,
                    								&cValOneChar[0]);
                   fprintf(fpXML, "%s%s%s", "\t\t\t<Constant>", &cValOneChar[0], "</Constant>\r");
                }
				// Effacement du fichier
				if (remove((const char *)strHACRecordXML->strTabImage2XML[iVarImage].cNomPathFileBin) !=0)
				{
				   printf("Cannot remove file %s (%s)\n",
							   strHACRecordXML->strTabImage2XML[iVarImage].cNomPathFileBin, ERRORSTRING);
				}
			}
			else
			{
				sprintf(cNomFicBin, "%s%s",strHACRecordXML->strTabImage2XML[iVarImage].cNom, ".bin");
				fprintf(fpXML, "%s%s%s", "\t\t\t<FileName>", cNomFicBin, "</FileName>\r");
			}
            if (strHACRecordXML->strTabImage2XML[iVarImage].fScaleFactor != 1.0)
            	fprintf(fpXML, "%s%g%s", "\t\t\t<ScaleFactor>", strHACRecordXML->strTabImage2XML[iVarImage].fScaleFactor, "</ScaleFactor>\r");
            if (strHACRecordXML->strTabImage2XML[iVarImage].fAddOffset != 0.0)
            	fprintf(fpXML, "%s%f%s", "\t\t\t<AddOffset>", strHACRecordXML->strTabImage2XML[iVarImage].fAddOffset, "</AddOffset>\r");
			fprintf(fpXML, "%s", "\t\t</item>\r");
			free(cTypeMatLab);
		} // Fin de la boucle sur les donn�es Image
		free(strHACRecordXML->strTabImage2XML);
	} //Fin de l'�criture des Images (RD et OD)

	// Fermeture de la balise Tables
	if (strHACRecordXML->sNbVarImage > 0 || strHACRecordXML->sNbVarImage2 >0)
	{
		fprintf(fpXML, "%s", "\t</Tables>\r");
	}

    fprintf(fpXML, "%s", "</ROOT>\r");
    fclose(fpXML);

    free(cFileNameXML);
    free(strHACRecordXML->ptr_cLabelHeader);
    free(strHACRecordXML->ptr_cNomHeader); // ptr_cNomHeader est du coup lib�r�.

    if (iRet != 0)
        return EXIT_FAILURE;
    else
        return EXIT_SUCCESS;


} //utilHAC_iSaveAndCloseFiles

//////////////////////////////////////////////////////////////////////
// Fonction		:	util_iGetIndian
// Objet		:	Test du "boutisme" des fichiers
// Modification :	11/12/08
// Auteur		:	GLU
//////////////////////////////////////////////////////////////////////
int utilHAC_iGetIndian(char *cNomFic)
{

FILE *fp;

unsigned int		uiStartCode;

int		iNbOctetsLus,
		iTypeEndian;

	fp = fopen(cNomFic, "rb");

	if(fp  == NULL)
	{
		printf("\nProblem in access Data file : %s\n", cNomFic);
		iTypeEndian = -1;
		return iTypeEndian;
	}

	// Lecture d'un fichier quelconque
	iNbOctetsLus = fread(&uiStartCode, sizeof(unsigned int), 1, fp);

	fclose(fp);

	if (uiStartCode == 172)
		iTypeEndian = 1; // Little Endian
	else
		iTypeEndian = 0; // Big Endian

	return iTypeEndian;
} // utilHAC_iGetIndian

//////////////////////////////////////////////////////////////////////
// Fonction		:	utilHAC_iCreateDirDatagram
// Objet		:	Cr�ation du r�pertoire d�di�es aux donn�es d'un
//					Tuple
// Modification :	29/09/09
// Auteur		:	GLU
//////////////////////////////////////////////////////////////////////
int utilHAC_iCreateDirDatagram(	char *cRepData,
								char *cNomDatagram,
								char *cRepertoire)
{

int 	iErr;

	// Si Extension = '.Hac'
	if (!stricmp(G_cFileExtension, "hac"))
		sprintf(cRepertoire, "%s\\HAC_%s", cRepData, cNomDatagram);
	else
	{
		printf("L'extension du fichier n'est pas reconnue : %s\n", G_cFileExtension);
		return EXIT_FAILURE;
	}


	if (DEBUG)
	   printf("-- Directory Creation : %s\n",cRepertoire);

	// Cr�ation du r�pertoire
	iErr = mkdir(cRepertoire);
	if (iErr != 0)
	{
	   switch (errno)
	   {
		   //"File exists"
		   case EEXIST:
			   break;
		   default:
			   printf("Error in Directory creation %s\n", strerror(errno));
			   return EXIT_FAILURE;
			   break;
	   }
	}

	return EXIT_SUCCESS;

} // utilHAC_iCreateDirDatagram

//////////////////////////////////////////////////////////////////////
// Fonction		:	utilHAC_initXMLRecord
// Objet		:	Initialisation de la structure XML des signaux et images
//					du fichier.
// Modification :	13/05/2011
// Auteur		:	GLU
//////////////////////////////////////////////////////////////////////
int utilHAC_initXMLRecord(	unsigned int iSounderId,
							T_DESC_TAB_SIGANDIMG strDescTabSigOrImg,
							char *cTitleDatagram,
							char *cNomDatagram,
							char *cComments,
							T_HACRECORDXML *strHACRecordXML)

{
	char	*ptr_cRepertoire,
			cNomFicPathBin[300],
			cTmpRepData[500],
			cRepSounder[20]; 		// Nom du sous-r�pertoire du Sondeur concern� = "SounderX"

    int 	iSizeRep,
			iRet,
			iLen,
			iVarSignal,
			iVarImage;

	unsigned short 	usTaille,
					usNbVarSignal,
					usNbVarImage,
					usNbVarImage2;

	short			sNumIndex;


	sNumIndex 		= strDescTabSigOrImg.sNumIndexTable;
	usNbVarSignal	= strDescTabSigOrImg.usNbVarSignal;
	usNbVarImage	= strDescTabSigOrImg.usNbVarImage;
	usNbVarImage2	= strDescTabSigOrImg.usNbVarImage2;

	// Initialisation des structures de pilotage des fichiers
	// XML et Bin .
	strHACRecordXML->sNbVarSignal 		= usNbVarSignal;
	strHACRecordXML->sNbVarImage 		= usNbVarImage;
	strHACRecordXML->sNbVarImage2 		= usNbVarImage2;
    strHACRecordXML->strTabSignalXML	= (T_HACVARXML *)malloc(strHACRecordXML->sNbVarSignal*sizeof(T_HACVARXML));
    strHACRecordXML->strTabImageXML		= (T_HACVARXML *)malloc(strHACRecordXML->sNbVarImage*sizeof(T_HACVARXML));

	iLen = strlen(cNomDatagram)+1;
	strHACRecordXML->ptr_cNomHeader = malloc(iLen*sizeof(char));
	sprintf(strHACRecordXML->ptr_cNomHeader, "%s", cNomDatagram);

	iLen = strlen(cTitleDatagram)+1;
	strHACRecordXML->ptr_cLabelHeader = malloc(iLen*sizeof(char));
	sprintf(strHACRecordXML->ptr_cLabelHeader, "%s", cTitleDatagram);

	iLen = strlen(cComments)+1;
	strHACRecordXML->ptr_cCommentsHeader = malloc(iLen*sizeof(char));
	sprintf(strHACRecordXML->ptr_cCommentsHeader, "%s", cComments);

	// Remplissage du nom de la variable d'indexation des tables.
	if (sNumIndex != -1)
	{
		iLen = strlen(strDescTabSigOrImg.strDescSignal[sNumIndex].cTabNomVar)+1;
		strHACRecordXML->ptr_cNomIndexTable = malloc(iLen*sizeof(char));
		sprintf(strHACRecordXML->ptr_cNomIndexTable, "%s", strDescTabSigOrImg.strDescSignal[sNumIndex].cTabNomVar);
	}
	else
	{
		iLen = strlen("NoneIndex")+1;
		strHACRecordXML->ptr_cNomIndexTable = malloc(iLen*sizeof(char));
		sprintf(strHACRecordXML->ptr_cNomIndexTable, "%s", "NoneIndex");
	}

	if (iSounderId != 0)
	{
		sprintf(cRepSounder, "Sounder%d", iSounderId);

		// Affectation et cr�ation du repertoire du Datagramme.
		iSizeRep = ( strlen(G_cRepData) + strlen(cRepSounder));
		// Ajout d'une marge au cas o� certains caract�res soient doubl�s soient doubl�s.
		ptr_cRepertoire = malloc( (iSizeRep + + 1 + 300 + 4)*sizeof(char));
		iRet = utilHAC_iCreateDirDatagram(G_cRepData,
									cRepSounder,
									ptr_cRepertoire);
		sprintf(cTmpRepData, "%s", ptr_cRepertoire);
	}
	else
	{
		sprintf(cTmpRepData, "%s", G_cRepData);
	}

	// Affectation et cr�ation du repertoire du Datagramme.
	iSizeRep = ( strlen(G_cRepData) + strlen( cNomDatagram));
	// Ajout d'une marge au cas o� certains caract�res soient doubl�s soient doubl�s.
	ptr_cRepertoire = malloc( (iSizeRep + 1 + 300 + 4)*sizeof(char));
	iRet = utilHAC_iCreateDirDatagram(cTmpRepData,
								strHACRecordXML->ptr_cNomHeader,
								ptr_cRepertoire);

	// Initialisation g�n�rale des variables de type Signal
	for (iVarSignal=0; iVarSignal<strHACRecordXML->sNbVarSignal; iVarSignal++) {
	   strHACRecordXML->strTabSignalXML[iVarSignal].ptr_fpBin = NULL;
	   strHACRecordXML->strTabSignalXML[iVarSignal].bFlagConstant = TRUE;
	   strHACRecordXML->strTabSignalXML[iVarSignal].usNbElem = 1;
	   sprintf(strHACRecordXML->strTabSignalXML[iVarSignal].cNom, "%s", strDescTabSigOrImg.strDescSignal[iVarSignal].cTabNomVar);
	   sprintf(strHACRecordXML->strTabSignalXML[iVarSignal].cType, "%s", strDescTabSigOrImg.strDescSignal[iVarSignal].cTabNomType);
	   sprintf(strHACRecordXML->strTabSignalXML[iVarSignal].cTag, "%s", "TODO");
	   sprintf(strHACRecordXML->strTabSignalXML[iVarSignal].cUnit, "%s", "TODO");
	   strHACRecordXML->strTabSignalXML[iVarSignal].fScaleFactor = strDescTabSigOrImg.strDescSignal[iVarSignal].fScaleFactor;
	   strHACRecordXML->strTabSignalXML[iVarSignal].fAddOffset = strDescTabSigOrImg.strDescSignal[iVarSignal].fAddOffset;
	   usTaille = util_usNbOctetTypeVar(strDescTabSigOrImg.strDescSignal[iVarSignal].cTabNomType);
	   strHACRecordXML->strTabSignalXML[iVarSignal].usSize = usTaille;
	   iLen = strlen(strHACRecordXML->strTabSignalXML[iVarSignal].cNom);
	   // Ajout d'une marge au cas o� certains caract�res soient doubl�s soient doubl�s.
	   sprintf(cNomFicPathBin, "%s\\%s%s", ptr_cRepertoire, strHACRecordXML->strTabSignalXML[iVarSignal].cNom, ".bin");
	   sprintf(strHACRecordXML->strTabSignalXML[iVarSignal].cNomPathFileBin, "%s", cNomFicPathBin);

	   strHACRecordXML->strTabSignalXML[iVarSignal].ptr_fpBin = fopen(cNomFicPathBin, "w+b");
	   if (!strHACRecordXML->strTabSignalXML[iVarSignal].ptr_fpBin)
	   {
		   printf("%s -- Error in Signal creation  : %s\n", __FILE__, cNomFicPathBin);
		   return EXIT_FAILURE;
	   }
	}

	// Initialisation g�n�rale des variables de type Image
	for (iVarImage=0; iVarImage<strHACRecordXML->sNbVarImage; iVarImage++) {
	   strHACRecordXML->strTabImageXML[iVarImage].ptr_fpBin = NULL;
	   strHACRecordXML->strTabImageXML[iVarImage].bFlagConstant = TRUE;
	   strHACRecordXML->strTabImageXML[iVarImage].usNbElem = 1;
	   sprintf(strHACRecordXML->strTabImageXML[iVarImage].cNom, "%s", strDescTabSigOrImg.strDescImage[iVarImage].cTabNomVar);
	   sprintf(strHACRecordXML->strTabImageXML[iVarImage].cType, "%s", strDescTabSigOrImg.strDescImage[iVarImage].cTabNomType);
	   sprintf(strHACRecordXML->strTabImageXML[iVarImage].cTag, "%s", "TODO");
	   sprintf(strHACRecordXML->strTabImageXML[iVarImage].cUnit, "%s", "TODO");
	   strHACRecordXML->strTabImageXML[iVarImage].fScaleFactor = strDescTabSigOrImg.strDescImage[iVarImage].fScaleFactor;
	   strHACRecordXML->strTabImageXML[iVarImage].fAddOffset = strDescTabSigOrImg.strDescImage[iVarImage].fAddOffset;
	   usTaille = util_usNbOctetTypeVar(strDescTabSigOrImg.strDescImage[iVarImage].cTabNomType);
	   strHACRecordXML->strTabImageXML[iVarImage].usSize = usTaille;
	   iLen = strlen(strHACRecordXML->strTabImageXML[iVarImage].cNom);
	   // Ajout d'une marge au cas o� certains caract�res soient doubl�s soient doubl�s.
	   sprintf(cNomFicPathBin, "%s\\%s%s", ptr_cRepertoire, strHACRecordXML->strTabImageXML[iVarImage].cNom, ".bin");
	   sprintf(strHACRecordXML->strTabImageXML[iVarImage].cNomPathFileBin, "%s", cNomFicPathBin);
	   strHACRecordXML->strTabImageXML[iVarImage].ptr_fpBin = fopen(cNomFicPathBin, "w+b");
	   if (!strHACRecordXML->strTabImageXML[iVarImage].ptr_fpBin)
	   {
		   printf("%s -- Error in Image creation  : %s\n", __FILE__, cNomFicPathBin);
		   return EXIT_FAILURE;
	   }
	}


	free(ptr_cRepertoire);
	return EXIT_SUCCESS;

} // utilHAC_initXMLRecord

/*
 *
//////////////////////////////////////////////////////////////////////
// Fonction		:	utilHAC_readEchoSounder
// Objet		:	Identification du num�ro de soudeur par stockage
//					it�ratif des Channels-Echo sounder.
// Modification :	20/06/2011
// Auteur		:	GLU
//////////////////////////////////////////////////////////////////////
int utilHAC_readEchoSounder(FILE *fpFileData,
							off64_t llPosFic,
							unsigned short *usNbCoupleChannelAndEcho,
							unsigned short *usSoftwareChannelId,
							unsigned int *uiEchoSounderId,
							T_SOUNDERANDCHANNELID *strSounderAndChannel)
{

int 	iLoop = 0,
		iFlagFindSCId = FALSE,
		iRet;

unsigned int 	uiTupleSize;
unsigned short 	usTupleType;


	// Lecture des valeurs d'identification du Software Channel Id et Echo Sounder Id.
	iRet = fread(&uiTupleSize, sizeof(unsigned int), 1, fpFileData);
	iRet = fread(&usTupleType, sizeof(unsigned short), 1, fpFileData);
	iRet = fread(usSoftwareChannelId, sizeof(unsigned short), 1, fpFileData);
	iRet = fread(uiEchoSounderId, sizeof(unsigned int), 1, fpFileData);
	// Repositionnement en d�but de tuple.
	fseeko64(fpFileData,llPosFic,SEEK_SET);

	// Recherche du Sofware Channel Id dans les donn�es d�j� reconnues.
	if (*usNbCoupleChannelAndEcho > 0)
	{
		// Recherche de datagrammes non d�cod�s d�j� trouv�s.
		for (iLoop=0; iLoop<*usNbCoupleChannelAndEcho; iLoop++)
		{
			if (*usSoftwareChannelId == strSounderAndChannel[iLoop].SoftwareChannelIdentifier)
			{
				iFlagFindSCId = TRUE;
				*usSoftwareChannelId = strSounderAndChannel[iLoop].SoftwareChannelIdentifier;
				*uiEchoSounderId = strSounderAndChannel[iLoop].EchosounderDocIdentifier;
				break;
			}
		}
		if (iFlagFindSCId == FALSE)
		{
			strSounderAndChannel[*usNbCoupleChannelAndEcho].SoftwareChannelIdentifier = (int)*usSoftwareChannelId;
			strSounderAndChannel[*usNbCoupleChannelAndEcho].EchosounderDocIdentifier = *uiEchoSounderId;
			*usNbCoupleChannelAndEcho = *usNbCoupleChannelAndEcho + 1;
		}
		iFlagFindSCId = FALSE;
	}
	else
	{
		// Stockage syst�matique pour le 1er Tuple rencontr�.

		strSounderAndChannel[0].SoftwareChannelIdentifier = (int)*usSoftwareChannelId;
		strSounderAndChannel[0].EchosounderDocIdentifier = *uiEchoSounderId;
		*usNbCoupleChannelAndEcho = *usNbCoupleChannelAndEcho + 1;

	}
	iRet = 0;
} //utilHAC_readEchoSounder
*/

//////////////////////////////////////////////////////////////////////
// Fonction		:	utilHAC_saveSounderChannelId
// Objet		:	Identification du num�ro de sondeur par stockage
//					it�ratif des Channels-Echo sounder.
// Modification :	20/06/2011
// Auteur		:	GLU
//////////////////////////////////////////////////////////////////////
int utilHAC_saveSounderChannelId(int *iIndexTupleRoot,
							int *iIndexTupleParent,
							unsigned short *usSoftwareChannelId,
							unsigned int *uiEchoSounderId,
							T_DESC_TAB_SOUNDERANDCHANNELID *strSounderAndChannel)
{

int 	iLoop = 0,
		iFlagFindSCId = FALSE,
		iRet;


	// Recherche du Sofware Channel Id dans les donn�es d�j� reconnues.
	if (strSounderAndChannel->usNbSoftwareChannelId > 0)
	{
		// Recherche de datagrammes non d�cod�s d�j� trouv�s.
		for (iLoop=0; iLoop<strSounderAndChannel->usNbSoftwareChannelId; iLoop++)
		{
			if (*usSoftwareChannelId == strSounderAndChannel->tabStrSounderAndChannel[iLoop].SoftwareChannelIdentifier)
			{
				iFlagFindSCId = TRUE;
				*usSoftwareChannelId = strSounderAndChannel->tabStrSounderAndChannel[iLoop].SoftwareChannelIdentifier;
				*uiEchoSounderId = strSounderAndChannel->tabStrSounderAndChannel[iLoop].EchosounderDocIdentifier;
				if (*iIndexTupleParent  != 0)
				{
					 strSounderAndChannel->tabStrSounderAndChannel[iLoop].iIndexTupleParent = *iIndexTupleParent;
				}
				else
				{
					 *iIndexTupleParent = strSounderAndChannel->tabStrSounderAndChannel[iLoop].iIndexTupleParent;

				}
				if (*iIndexTupleRoot  != 0)
				{
					 strSounderAndChannel->tabStrSounderAndChannel[iLoop].iIndexTupleRoot = *iIndexTupleRoot;
				}
				else
				{
					 *iIndexTupleRoot = strSounderAndChannel->tabStrSounderAndChannel[iLoop].iIndexTupleRoot;

				}
				break;
			}
		}
		if (iFlagFindSCId == FALSE)
		{
			strSounderAndChannel->tabStrSounderAndChannel[strSounderAndChannel->usNbSoftwareChannelId].SoftwareChannelIdentifier = *usSoftwareChannelId;
			strSounderAndChannel->tabStrSounderAndChannel[strSounderAndChannel->usNbSoftwareChannelId].EchosounderDocIdentifier = *uiEchoSounderId;
			if (*iIndexTupleParent  != 0)
			{
				 strSounderAndChannel->tabStrSounderAndChannel[strSounderAndChannel->usNbSoftwareChannelId].iIndexTupleParent = *iIndexTupleParent;
			}
			if (*iIndexTupleRoot  != 0)
			{
				 strSounderAndChannel->tabStrSounderAndChannel[strSounderAndChannel->usNbSoftwareChannelId].iIndexTupleRoot = *iIndexTupleRoot;
			}
			strSounderAndChannel->usNbSoftwareChannelId++;
		}
		iFlagFindSCId = FALSE;
	}
	else
	{
		// Stockage syst�matique pour le 1er Tuple rencontr�.
		strSounderAndChannel->tabStrSounderAndChannel[0].SoftwareChannelIdentifier = (int)*usSoftwareChannelId;
		strSounderAndChannel->tabStrSounderAndChannel[0].EchosounderDocIdentifier = *uiEchoSounderId;
		if (*iIndexTupleParent  != 0)
		{
			 strSounderAndChannel->tabStrSounderAndChannel[0].iIndexTupleParent = *iIndexTupleParent;
		}
		if (*iIndexTupleRoot  != 0)
		{
			 strSounderAndChannel->tabStrSounderAndChannel[0].iIndexTupleRoot = *iIndexTupleRoot;
		}
		strSounderAndChannel->usNbSoftwareChannelId++;
	}
	iRet = 0;

	return iRet;
} //utilHAC_saveSounderChannelId

//////////////////////////////////////////////////////////////////////
// Fonction		:	utilHAC_readEchoSounder
// Objet		:	Lecture du num�ro de Sondeur dans le fichier de donn�e.
// Modification :	20/06/2011
// Auteur		:	GLU
//////////////////////////////////////////////////////////////////////
int utilHAC_readEchoSounder(FILE *fpFileData,
							off64_t llPosFic,
							unsigned short *usSoftwareChannelId,
							unsigned int *uiEchoSounderId)
{

int 	iRet;

unsigned int 	uiTupleSize;
unsigned short 	usTupleType;


	// Lecture des valeurs d'identification du Software Channel Id et Echo Sounder Id.
	iRet = fread(&uiTupleSize, sizeof(unsigned int), 1, fpFileData);
	iRet = fread(&usTupleType, sizeof(unsigned short), 1, fpFileData);
	iRet = fread(usSoftwareChannelId, sizeof(unsigned short), 1, fpFileData);
	iRet = fread(uiEchoSounderId, sizeof(unsigned int), 1, fpFileData);
	// Repositionnement en d�but de tuple.
	fseeko64(fpFileData,llPosFic,SEEK_SET);

	iRet = 0;

	return iRet;

} //utilHAC_readEchoSounder

//////////////////////////////////////////////////////////////////////
// Fonction		:	utilHAC_identEchoSounder
// Objet		:	Identification du num�ro d'apr�s le tableau de stockage.
// Modification :	29/08/2011
// Auteur		:	GLU
//////////////////////////////////////////////////////////////////////
int utilHAC_identEchoSounder(	unsigned int uiTmpId,
								unsigned int *uiNbEchoSounder,
								int *iTabEchoSounderId,
								unsigned int *uiEchoSounderId)
{

int 	iRet,
		iLoop,
		iFlagFindEchoSounderId = FALSE;

	// Recherche du Id Sondeur d�j� identifi� ou cr�ation d'un  nouveau.
	for (iLoop=0; iLoop<NBMAXSOUNDER; iLoop++)
	{
		if (iTabEchoSounderId[iLoop] != -1)
		{
			if (uiTmpId == iTabEchoSounderId[iLoop])
			{
				iFlagFindEchoSounderId = TRUE;
				*uiEchoSounderId = iLoop + 1;
				break;
			}
		}

	}
	if (iFlagFindEchoSounderId == FALSE)
	{
		*uiEchoSounderId = *uiNbEchoSounder + 1;
		iTabEchoSounderId[*uiNbEchoSounder] = uiTmpId;
		*uiNbEchoSounder = *uiNbEchoSounder + 1;
	}

	iRet = 0;

	return iRet;

} //utilHAC_identEchoSounder

//////////////////////////////////////////////////////////////////////
// Fonction		:	utilHAC_saveChannelSensorId
// Objet		:	Identification du num�ro de sondeur par stockage
//					it�ratif des Channels-Sensor.
// Modification :	20/06/2011
// Auteur		:	GLU
//////////////////////////////////////////////////////////////////////
int utilHAC_saveChannelSensorId(int *iIndexTupleRoot,
							int *iIndexTupleParent,
							unsigned short *usSoftwareChannelId,
							unsigned short *usSensorId,
							T_DESC_TAB_CHANNELSENSORID *strChannelSensor)
{

int 	iLoop = 0,
		iFlagFindSCId = FALSE,
		iRet;


	// Recherche du Sofware Channel Id dans les donn�es d�j� reconnues.
	if (strChannelSensor->usNbSoftwareChannelId > 0)
	{
		// Recherche de datagrammes non d�cod�s d�j� trouv�s.
		for (iLoop=0; iLoop<strChannelSensor->usNbSoftwareChannelId; iLoop++)
		{
			if (*usSoftwareChannelId == strChannelSensor->tabStrSensorSounderChannel[iLoop].SoftwareChannelIdentifier)
			{
				iFlagFindSCId = TRUE;
				*usSoftwareChannelId = strChannelSensor->tabStrSensorSounderChannel[iLoop].SoftwareChannelIdentifier;
				*usSensorId = strChannelSensor->tabStrSensorSounderChannel[iLoop].SensorIdentifier;
				if (*iIndexTupleParent  != 0)
				{
					 strChannelSensor->tabStrSensorSounderChannel[iLoop].iIndexTupleParent = *iIndexTupleParent;
				}
				else
				{
					 *iIndexTupleParent = strChannelSensor->tabStrSensorSounderChannel[iLoop].iIndexTupleParent;

				}
				if (*iIndexTupleRoot  != 0)
				{
					 strChannelSensor->tabStrSensorSounderChannel[iLoop].iIndexTupleRoot = *iIndexTupleRoot;
				}
				else
				{
					 *iIndexTupleRoot = strChannelSensor->tabStrSensorSounderChannel[iLoop].iIndexTupleRoot;

				}
				break;
			}
		}
		if (iFlagFindSCId == FALSE)
		{
			strChannelSensor->tabStrSensorSounderChannel[strChannelSensor->usNbSoftwareChannelId].SoftwareChannelIdentifier = *usSoftwareChannelId;
			strChannelSensor->tabStrSensorSounderChannel[strChannelSensor->usNbSoftwareChannelId].SensorIdentifier = *usSensorId;
			if (*iIndexTupleParent  != 0)
			{
				 strChannelSensor->tabStrSensorSounderChannel[strChannelSensor->usNbSoftwareChannelId].iIndexTupleParent = *iIndexTupleParent;
			}
			if (*iIndexTupleRoot  != 0)
			{
				 strChannelSensor->tabStrSensorSounderChannel[strChannelSensor->usNbSoftwareChannelId].iIndexTupleRoot = *iIndexTupleRoot;
			}
			strChannelSensor->usNbSoftwareChannelId++;
		}
		iFlagFindSCId = FALSE;
	}
	else
	{
		// Stockage syst�matique pour le 1er Tuple rencontr�.
		strChannelSensor->tabStrSensorSounderChannel[0].SoftwareChannelIdentifier = (int)*usSoftwareChannelId;
		strChannelSensor->tabStrSensorSounderChannel[0].SensorIdentifier = (int)*usSensorId;
		if (*iIndexTupleParent  != 0)
		{
			 strChannelSensor->tabStrSensorSounderChannel[0].iIndexTupleParent = *iIndexTupleParent;
		}
		if (*iIndexTupleRoot  != 0)
		{
			 strChannelSensor->tabStrSensorSounderChannel[0].iIndexTupleRoot = *iIndexTupleRoot;
		}
		strChannelSensor->usNbSoftwareChannelId++;
	}
	iRet = 0;

	return iRet;
} //utilHAC_saveChannelSensorId

//////////////////////////////////////////////////////////////////////
// Fonction		:	utilHAC_identSensor
// Objet		:	Identification du num�ro de Sensor Id par lecture
//					du num�ro dans le fichier de donn�e - Tuple 41 -
// Modification :	24/06/2011
// Auteur		:	GLU
//////////////////////////////////////////////////////////////////////
int utilHAC_identSensor(FILE *fpFileData,
							off64_t llPosFic,
							unsigned short *usSoftwareChannelId,
							unsigned short *usSensorId)
{

int 	iRet;

unsigned int 	uiTupleSize;
unsigned short 	usTupleType;
unsigned short	usTupleFraction;
unsigned int	uiTupleCPU;


	// Lecture des valeurs d'identification du Software Channel Id et Echo Sounder Id.
	iRet = fread(&uiTupleSize, sizeof(unsigned int), 1, fpFileData);
	iRet = fread(&usTupleType, sizeof(unsigned short), 1, fpFileData);
	iRet = fread(&usTupleFraction, sizeof(unsigned short), 1, fpFileData);
	iRet = fread(&uiTupleCPU, sizeof(unsigned int), 1, fpFileData);
	iRet = fread(usSensorId, sizeof(unsigned short), 1, fpFileData);
	iRet = fread(usSoftwareChannelId, sizeof(unsigned short), 1, fpFileData);

	// Repositionnement en d�but de tuple.
	fseeko64(fpFileData,llPosFic,SEEK_SET);

	iRet = 0;

	return iRet;

} //utilHAC_identSensor

#pragma pack()
