/*
 * HAC_utilities.h
 *
 *  Created on: 23 Mai 2010
 *      Author: rgallou
 */

#ifndef HAC_UTILITIES_H_
#define HAC_UTILITIES_H_

#include "HAC_Tuples.h"

#define NBMAXSOUNDER 5
int utilHAC_initXMLRecord(	unsigned int uiSounderId,
							T_DESC_TAB_SIGANDIMG strDescTabSigOrImg,
							char *cTitleDatagram,
							char *cNomDatagram,
							char *cComments,
							T_HACRECORDXML *strHACRecordXML);

int utilHAC_iSaveAndCloseFiles(	int iSounderId,
								char *cRepData,
								T_HACRECORDXML *strHACRecordXML);

int utilHAC_iGetIndian(char *nomFic);

int utilHAC_iCreateDirDatagram(	char *cRepData,
								char *cNomDatagram,
								char *cRepertoire);

int utilHAC_saveSounderChannelId( 	int *iIndexTupleRoot,
									int *iIndexTupleParent,
									unsigned short *usSoftwareChannelId,
									unsigned int *uiEchoSounderId,
									T_DESC_TAB_SOUNDERANDCHANNELID *strSounderAndChannel);

int utilHAC_readEchoSounder(FILE *fpFileData,
							off64_t llPosFic,
							unsigned short *usSoftwareChannelId,
							unsigned int *uiEchoSounderId);

int utilHAC_identEchoSounder(	unsigned int uiTmpId,
								unsigned int *uiNbEchoSounder,
								int *iTabEchoSounderId,
								unsigned int *uiEchoSounderId);

int utilHAC_identSensor(FILE *fpFileData,
							off64_t llPosFic,
							unsigned short *usSoftwareChannelId,
							unsigned short *usSensorId);

int utilHAC_saveChannelSensorId(int *iIndexTupleRoot,
								int *iIndexTupleParent,
								unsigned short *usSoftwareChannelId,
								unsigned short *usSensorId,
								T_DESC_TAB_CHANNELSENSORID *strChannelSensor);

#endif /* HAC_UTILITIES_H_ */
