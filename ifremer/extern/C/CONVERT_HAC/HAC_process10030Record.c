/*
 * HAC_process10030Record.c
 *
 *  Created on: 18 11 2013
 *      Author: rgallou
 */
#pragma pack(1)

#define NB_SIGNAL 		13
#define NB_IMAGE 		2
#define NB_IMAGE2 		0
#define NUM_INDEX_TABLE	12	// Identification de l'index Signal des Images (� partir de 0).

#include <sys/types.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "HAC_Tuples.h"
#include "HAC_Utilities.h"
#include "Generic_Utilities.h"
#include "HAC_process10030Record.h"
#include "convertFiles_HAC.h"


//----------------------------------------------------------------
// Fonction :	iProcess10030Record
// Objet 	:	Traitement du Tuple 10030
// Modif.	: 	20/05/11
// Auteur 	:	GLU
//----------------------------------------------------------------
int iProcess10030Record(FILE *fpFileData,
						int iNbRecord,
						int iIndexTupleRoot,
						int iIndexTupleParent,
						int uiSounderId,
						T_HACRECORDXML *strHACRecordXML,
						T_TUPLE10030_SIGNAL *ptrSignal_prevRecord)
{
	T_TUPLE10030_SIGNAL	*ptrSignal_Record;

	// Le Tuple 10030 est vu autant de fois qu'il y a de Pings et de faisceaux.
	// Pour un Ping, il y a une succession de N faisceaux.
	// Pointeur pour avancer sur les variables automatiquement.
	unsigned char		*ptr_cSignalRecord;
	static void 		*ptr_vVarPrevRecord;
	void				*ptr_vVarRecord,
						*ptr_vBidon;

	BOOL		bFlagVarConstant = TRUE,
				bFlagCompare = TRUE;

	int 		iFlag,
				iVarSignal,
				iVarImage,
				iRet = 0,
				iLoop,
				iTailleBufferSignal,
				iNbSampleSequences;


    unsigned short	usTailleEnByte,
					usNbElem;

    char		cNomDatagram[100],
				*cValeur;



    // Initialisation de la structure d'identification du paquet
 	if (iNbRecord == 0)
	{
 		sprintf(cNomDatagram, "PingU16Amplitude");
 		iInitXMLBin10030Record(	uiSounderId,
 								"Ping-U16-Amplitude",
								cNomDatagram,
								"Ping U16-Tuple 10030",
								strHACRecordXML);
	}

    // Lecture des signaux du paquet
 	iTailleBufferSignal = 	sizeof(T_TUPLE10030_SIGNAL)	-
							sizeof(ptrSignal_Record->TupleAttribute) -
							sizeof(ptrSignal_Record->BackLink) -
							sizeof(ptrSignal_Record->IndexTupleRoot)-
							sizeof(ptrSignal_Record->IndexTupleParent)-
							sizeof(ptrSignal_Record->NbSamples);



 	ptrSignal_Record = malloc(iTailleBufferSignal);
	iFlag = fread(ptrSignal_Record, iTailleBufferSignal, 1, fpFileData);

	if (iFlag == 0)
	{
		printf("%s -- Error in reading : %s\n", __FILE__, G_cFileData);
		return FALSE;
	}
	ptr_cSignalRecord = (unsigned char*)ptrSignal_Record;

	// Sauvegarde des donn�es des attributs.
	if (iNbRecord > 0)
	{
		ptr_vVarPrevRecord = (void *)ptrSignal_prevRecord;
	}

	// On lit toute la structure 10030 sauf TupleAttribute, Backlink et IndexTupleRoot, IndexTupleParent.
	for (iVarSignal=0; iVarSignal<(int)strHACRecordXML->sNbVarSignal-5; iVarSignal++)
	{
		ptr_vVarRecord = (void *)ptr_cSignalRecord;
		if (iVarSignal==0)
		{
			usTailleEnByte = 0;
			usNbElem = 0;
		}
		else
		{
			usTailleEnByte = util_usNbOctetTypeVar(strHACRecordXML->strTabSignalXML[iVarSignal-1].cType);
			usNbElem = strHACRecordXML->strTabSignalXML[iVarSignal-1].usNbElem;
		}
		// Pointage sur la donn�e suivante.
		ptr_vVarPrevRecord = ptr_vVarPrevRecord + usTailleEnByte*usNbElem;
		// On ne fait pas le test de constance au 1er enregistrement ou si le signal
		// est d�j� vue comme variable.
		if (iNbRecord > 0 && strHACRecordXML->strTabSignalXML[iVarSignal].bFlagConstant == TRUE)
		{
			bFlagVarConstant = strHACRecordXML->strTabSignalXML[iVarSignal].bFlagConstant;
			if (G_iFlagEndian == 0) // On inverse en Big Endian.
			{
				// Observation de la taille propre � la variable.
				usTailleEnByte = util_usNbOctetTypeVar(strHACRecordXML->strTabSignalXML[iVarSignal].cType);
				usNbElem = strHACRecordXML->strTabSignalXML[iVarSignal].usNbElem;
				if (usTailleEnByte == 2) // Pour des (u)short, (u)int, (u)long.
					ptr_vVarPrevRecord = (void *)util_cBswap_16(ptr_vVarPrevRecord);
				else if (usTailleEnByte == 4)
					ptr_vVarPrevRecord = (void *)util_cBswap_32(ptr_vVarPrevRecord);
				else if (usTailleEnByte == 8)
					ptr_vVarPrevRecord = (void *)util_cBswap_64(ptr_vVarPrevRecord);
			}
			bFlagCompare = util_bComparePrevAndCurrentVal(strHACRecordXML->strTabSignalXML[iVarSignal].cType,
												ptr_vVarPrevRecord,
												ptr_vVarRecord,
											   strHACRecordXML->strTabSignalXML[iVarSignal].usNbElem);
			bFlagVarConstant = bFlagVarConstant && bFlagCompare;
			strHACRecordXML->strTabSignalXML[iVarSignal].bFlagConstant = bFlagVarConstant;
		}


		// Pr�paration de l'�criture de la variable.
		usTailleEnByte = strHACRecordXML->strTabSignalXML[iVarSignal].usSize;
		usNbElem =  strHACRecordXML->strTabSignalXML[iVarSignal].usNbElem;
		if (G_iFlagEndian == 0) // On inverse en Big Endian.
		{
			if (usTailleEnByte == 2) // Pour des (u)short, (u)int, (u)long.
				ptr_cSignalRecord = (unsigned char*)util_cBswap_16((char*)ptr_cSignalRecord);
			else if (usTailleEnByte == 4)
				ptr_cSignalRecord = (unsigned char*)util_cBswap_32((char*)ptr_cSignalRecord);
			else if (usTailleEnByte == 8)
				ptr_cSignalRecord = (unsigned char*)util_cBswap_64((char*)ptr_cSignalRecord);
		}
		// D�termination du nombre de s�quences d'�chantillons angulaires en calculant depuis le TupleSize.
		if (iVarSignal == 0)
		{
			cValeur = malloc(256*sizeof(char));
			iRet 				= util_cValVarConstant(strHACRecordXML->strTabSignalXML[iVarSignal].cType,
										ptr_cSignalRecord,
										usNbElem,
										cValeur);
			iNbSampleSequences 	= (atoi(cValeur) - 22)/2; 	// N = size/2 (en short) - 22 (ent�te).
			iNbSampleSequences 	= iNbSampleSequences/2;		// Sequence = Index (U16) + Sample (I16).
			free(cValeur);
		}
		fwrite(ptr_cSignalRecord, usTailleEnByte, usNbElem, strHACRecordXML->strTabSignalXML[iVarSignal].ptr_fpBin);
		strHACRecordXML->strTabSignalXML[iVarSignal].ptr_vPrevVal = ptr_cSignalRecord;
		ptr_cSignalRecord += usTailleEnByte*usNbElem;
	}
	// Recopie en zone tampon des valeurs courantes.
	memmove(ptrSignal_prevRecord, ptrSignal_Record, sizeof(T_TUPLE10030_SIGNAL));



	// Pr�allocation du buffer de conservation sur le 1er record (en dehors de la boucle pour le cas ou le 1er Tuple
	// de Type 10030 comporte NSamples = 0).
	if (iNbRecord == 0)
	{
		for (iVarImage=0; iVarImage< strHACRecordXML->sNbVarImage; iVarImage++)
		{
			usTailleEnByte = strHACRecordXML->strTabImageXML[iVarImage].usSize;
			usNbElem = strHACRecordXML->strTabImageXML[iVarImage].usNbElem;
			strHACRecordXML->strTabImageXML[iVarImage].ptr_vPrevVal = malloc(usTailleEnByte*usNbElem*sizeof(char));
		}
	}

	// --------------------------------------------
 	// Lecture unitaire des variables de type Image
	for (iLoop=0; iLoop< iNbSampleSequences; iLoop++)
	{
		// Lecture unitaire des variables.
		for (iVarImage=0; iVarImage< strHACRecordXML->sNbVarImage; iVarImage++)
		{
			usTailleEnByte = strHACRecordXML->strTabImageXML[iVarImage].usSize;
			usNbElem = strHACRecordXML->strTabImageXML[iVarImage].usNbElem;
			ptr_vBidon = (void *)malloc((usTailleEnByte*usNbElem)*sizeof(char));

			iFlag = fread(ptr_vBidon, usTailleEnByte, usNbElem, fpFileData);
			if (G_iFlagEndian == 0) // On inverse en Big Endian.
			{
				if (usTailleEnByte == 2) // Pour des (u)short, (u)int, (u)long.
					ptr_vBidon = (void *)util_cBswap_16(ptr_vBidon);
				else if (usTailleEnByte == 4)
					ptr_vBidon = (void *)util_cBswap_32(ptr_vBidon);
				else if (usTailleEnByte == 8)
					ptr_vBidon = (void *)util_cBswap_64(ptr_vBidon);
			}
			iFlag = fwrite(ptr_vBidon, usTailleEnByte, usNbElem, strHACRecordXML->strTabImageXML[iVarImage].ptr_fpBin);
			if (iLoop > 0 && strHACRecordXML->strTabImageXML[iVarImage].bFlagConstant == TRUE)
			{
				bFlagCompare = util_bComparePrevAndCurrentVal(strHACRecordXML->strTabImageXML[iVarImage].cType,
													strHACRecordXML->strTabImageXML[iVarImage].ptr_vPrevVal,
													ptr_vBidon,
													usNbElem);
				strHACRecordXML->strTabImageXML[iVarImage].bFlagConstant = bFlagCompare && strHACRecordXML->strTabImageXML[iVarImage].bFlagConstant;
			}
//			if (iNbRecord == 0)
//			{
//				strHACRecordXML->strTabImageXML[iVarImage].ptr_vPrevVal = malloc(usTailleEnByte*usNbElem*sizeof(char));
//			}
			memmove(strHACRecordXML->strTabImageXML[iVarImage].ptr_vPrevVal, ptr_vBidon, usTailleEnByte*usNbElem);

			free(ptr_vBidon);

		} // Fin de la boucles sur les variables.

	}


	// Traitement du signal TupleAttribute et BackLink exploit� apr�s la lecture
	// des �chantillons.
	for (iVarSignal=(int)strHACRecordXML->sNbVarSignal-5; iVarSignal<(int)strHACRecordXML->sNbVarSignal-3; iVarSignal++)
	{
		usTailleEnByte		= util_usNbOctetTypeVar(strHACRecordXML->strTabSignalXML[iVarSignal].cType);
		usNbElem 			= strHACRecordXML->strTabSignalXML[iVarSignal].usNbElem;
		ptr_vBidon			= (void *)malloc((usTailleEnByte*usNbElem)*sizeof(char));
		iFlag 				= fread(ptr_vBidon, usTailleEnByte, usNbElem, fpFileData);


		if (G_iFlagEndian == 0) // On inverse en Big Endian.
		{
			if (usTailleEnByte == 2) // Pour des (u)short, (u)int, (u)long.
				ptr_vBidon = (void *)util_cBswap_16(ptr_vBidon);
			else if (usTailleEnByte == 4)
				ptr_vBidon = (void *)util_cBswap_32(ptr_vBidon);
			else if (usTailleEnByte == 8)
				ptr_vBidon = (void *)util_cBswap_64(ptr_vBidon);
		}
		iFlag = fwrite(ptr_vBidon, usTailleEnByte, usNbElem, strHACRecordXML->strTabSignalXML[iVarSignal].ptr_fpBin);
		if (iNbRecord > 0 && strHACRecordXML->strTabSignalXML[iVarSignal].bFlagConstant == TRUE)
		{
			bFlagCompare = util_bComparePrevAndCurrentVal(strHACRecordXML->strTabSignalXML[iVarSignal].cType,
												strHACRecordXML->strTabSignalXML[iVarSignal].ptr_vPrevVal,
												ptr_vBidon,
												usNbElem);
			strHACRecordXML->strTabSignalXML[iVarSignal].bFlagConstant = bFlagCompare && strHACRecordXML->strTabSignalXML[iVarSignal].bFlagConstant;
		}
		if (iNbRecord == 0)
		{
			strHACRecordXML->strTabSignalXML[iVarSignal].ptr_vPrevVal = malloc(usTailleEnByte*usNbElem*sizeof(char));
		}
		// Recopie en zone tampon des valeurs courantes.
		memmove(strHACRecordXML->strTabSignalXML[iVarSignal].ptr_vPrevVal, ptr_vBidon, usTailleEnByte*usNbElem);
	}


	free(ptrSignal_Record);

	// Traitement exceptionnel pour la donn�e IndexTupleRoot :
	iVarSignal = strHACRecordXML->sNbVarSignal-3;
	fwrite(		&iIndexTupleRoot,
				strHACRecordXML->strTabSignalXML[iVarSignal].usSize,
				strHACRecordXML->strTabSignalXML[iVarSignal].usNbElem,
				strHACRecordXML->strTabSignalXML[iVarSignal].ptr_fpBin);
	strHACRecordXML->strTabSignalXML[iVarSignal].bFlagConstant = FALSE;

	// Traitement exceptionnel pour la donn�e IndexTupleParent :
	iVarSignal = strHACRecordXML->sNbVarSignal-2;
	fwrite(		&iIndexTupleParent,
				strHACRecordXML->strTabSignalXML[iVarSignal].usSize,
				strHACRecordXML->strTabSignalXML[iVarSignal].usNbElem,
				strHACRecordXML->strTabSignalXML[iVarSignal].ptr_fpBin);
	strHACRecordXML->strTabSignalXML[iVarSignal].bFlagConstant = FALSE;

	// Traitement exceptionnel pour la donn�e NbSamples :
	iVarSignal = strHACRecordXML->sNbVarSignal-1;
	fwrite(		&iNbSampleSequences,
				strHACRecordXML->strTabSignalXML[iVarSignal].usSize,
				strHACRecordXML->strTabSignalXML[iVarSignal].usNbElem,
				strHACRecordXML->strTabSignalXML[iVarSignal].ptr_fpBin);
	strHACRecordXML->strTabSignalXML[iVarSignal].bFlagConstant = FALSE;

	iNbRecord++;

	return iRet;

} //iProcess10030Record


//----------------------------------------------------------------
// Fonction :	iInitXMLBin10030Record
// Objet 	:	Initialisation de la structure �quivalent au Tuple
//				10030 pour la manipulation des fichiers bin et XML.
// Modification : 18/11/13
// Auteur 	: GLU
//----------------------------------------------------------------
int iInitXMLBin10030Record(		unsigned int uiSounderID,
								char *cTitleDatagram,
								char *cNomDatagram,
								char *cComments,
								T_HACRECORDXML *strHACRecordXML)
{
	int iRet;

	T_DESC_SIGNAL_OR_IMAGE strDescriptionSignal[NB_SIGNAL] = {
								{"TupleSize","unsigned int", 						1.0, 0.0},
								{"TupleType","unsigned short", 						1.0, 0.0},
								{"TimeFraction","unsigned short", 					0.0001, 0.0},
								{"TimeCPU","unsigned int", 							1.0, 0.0},
								{"SoftwareChannelIdentifier","unsigned short", 		1.0, 0.0},
								{"TransceiverMode","unsigned short", 				1.0, 0.0},
								{"PingNumber","unsigned int", 						1.0, 0.0},
								{"DetectedBottomRange","int", 						0.001, 0.0},
								{"TupleAttribute","int", 							1.0, 0.0},
								{"BackLink","unsigned int", 						1.0, 0.0},
								{"IndexTupleRoot","unsigned int",          			1.0, 0.0},
								{"IndexTupleParent","unsigned int",          		1.0, 0.0},
								{"NbSamples","int", 								1.0, 0.0},
								};

	T_DESC_SIGNAL_OR_IMAGE strDescriptionImage[NB_IMAGE] = {
								{"SequenceNumberSample",	"unsigned short", 		1.0, 0.0},
								{"Amplitude",				"short", 				0.01, 0.0},		// Amplitude = Sv (donn�e Trait�e du TvG)
								};

	T_DESC_SIGNAL_OR_IMAGE strDescriptionImage2[NB_IMAGE2] = {};

	T_DESC_TAB_SIGANDIMG strDescriptionTab = {	NB_SIGNAL,
												NB_IMAGE,
												NB_IMAGE2,
												NUM_INDEX_TABLE,
												strDescriptionSignal,
												strDescriptionImage,
												strDescriptionImage2};


	// Lancement de l'initialisation des structures de signaux : ouvertures des
	// fichiers, copie des structures, ...
	iRet = utilHAC_initXMLRecord(	uiSounderID,
									strDescriptionTab,
									cTitleDatagram,
									cNomDatagram,
									cComments,
									strHACRecordXML);

	return iRet;

} // iInitXMLBin10030Record

#pragma pack()
