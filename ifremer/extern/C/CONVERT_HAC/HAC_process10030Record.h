/*
 * HAC_process10030Record.h
 *
 *  Created on: 18 11 2013
 *      Author: rgallou
 */
#pragma pack(1)

#ifndef HAC_PROCESS10030RECORD_H_
#define HAC_PROCESS10030RECORD_H_

#include "HAC_Tuples.H"


typedef struct  {
	unsigned int 	TupleSize;
	unsigned short 	TupleType;
	unsigned short 	TimeFraction;
	unsigned int	TimeCPU;
	unsigned short 	SoftwareChannelIdentifier;
	unsigned short 	TransceiverMode;
	unsigned int 	PingNumber;
	int 			DetectedBottomRange;
	int 			TupleAttribute;
	unsigned int	BackLink;
	unsigned int	IndexTupleRoot;
	unsigned int	IndexTupleParent;
	unsigned int	NbSamples;
} T_TUPLE10030_SIGNAL;

typedef struct  {
	unsigned short		*SequenceNumberSample;
	short				*SampleValue;
} T_10030_RECORD_IMAGE;

int iProcess10030Record(FILE *fpFileData,
						int iNbRecord,
						int iIndexTupleRoot,
						int iIndexTupleParent,
						int uiSounderId,
						T_HACRECORDXML *strHACRecordXML,
						T_TUPLE10030_SIGNAL *ptrSignal_prevRecord);

int iInitXMLBin10030Record(	unsigned int uiSounderID,
							char *cTitleDatagram,
							char *cNomDatagram,
							char *cComments,
							T_HACRECORDXML *strHACRecordXML);


#endif /* HAC_PROCESS10030RECORD_H_ */

#pragma pack()
