/*
 * HAC_process10031Record.h
 *
 *  Created on: 18 11 2013
 *      Author: rgallou
 */
#pragma pack(1)

#ifndef HAC_PROCESS10031RECORD_H_
#define HAC_PROCESS10031RECORD_H_

#include "HAC_Tuples.H"


typedef struct  {
	unsigned int 	TupleSize;
	unsigned short 	TupleType;
	unsigned short 	TimeFraction;
	unsigned int	TimeCPU;
	unsigned short 	SoftwareChannelIdentifier;
	unsigned short 	TransceiverMode;
	unsigned int 	PingNumber;
	int 			DetectedBottomRange;
	int 			TupleAttribute;
	unsigned int	BackLink;
	unsigned int	IndexTupleRoot;
	unsigned int	IndexTupleParent;
	unsigned int	NbSamples;				// Attribut supplémentaire pour stocker la dimension du Tableau.
} T_TUPLE10031_SIGNAL;

typedef struct  {
	unsigned short		*SequenceNumberAngles;
	short				*AlongShipPhaseAngle;
	short				*AthwartShipPhaseAngle;
} T_10031_RECORD_IMAGE;

int iProcess10031Record(FILE *fpFileData,
						int iNbRecord,
						int iIndexTupleRoot,
						int iIndexTupleParent,
						int uiSounderId,
						T_HACRECORDXML *strHACRecordXML,
						T_TUPLE10031_SIGNAL *ptrSignal_prevRecord);

int iInitXMLBin10031Record(	unsigned int uiSounderID,
							char *cTitleDatagram,
							char *cNomDatagram,
							char *cComments,
							T_HACRECORDXML *strHACRecordXML);


#endif /* HAC_PROCESS10031RECORD_H_ */

#pragma pack()
