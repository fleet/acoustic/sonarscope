/*
 * HAC_process10040Record.h
 *
 *  Created on: 31 05 2011
 *      Author: rgallou
 */
#pragma pack(1)

#ifndef HAC_PROCESS10040RECORD_H_
#define HAC_PROCESS10040RECORD_H_

#include "HAC_Tuples.H"


typedef struct  {
	unsigned int 	TupleSize;
	unsigned short 	TupleType;
	unsigned short 	TimeFraction;
	unsigned int	TimeCPU;
	unsigned short 	SoftwareChannelIdentifier;
	unsigned short 	TransceiverMode;
	unsigned int 	PingNumber;
	int 			DetectedBottomRange;
	unsigned int 	NbSamples;
	int 			TupleAttribute;
	unsigned int	BackLink;
	unsigned int	IndexTupleRoot;
	unsigned int	IndexTupleParent;
} T_TUPLE10040_SIGNAL;

typedef struct  {
		short	*CompressSamples;
} T_10040_RECORD_IMAGE;

int iProcess10040Record(FILE *fpFileData,
						int iNbRecord,
						int iIndexTupleRoot,
						int iIndexTupleParent,
						int uiSounderId,
						T_HACRECORDXML *strHACRecordXML,
						T_TUPLE10040_SIGNAL *ptrSignal_prevRecord);


int iInitXMLBin10040Record(	unsigned int uiSounderID,
							char *cTitleDatagram,
							char *cNomDatagram,
							char *cComments,
							T_HACRECORDXML *strHACRecordXML);


#endif /* HAC_PROCESS10040RECORD_H_ */

#pragma pack()
