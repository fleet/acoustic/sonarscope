/*
 * HAC_process10140Record.h
 *
 *  Created on: 23 mai 2011
 *      Author: rgallou
 */
#pragma pack(1)

#ifndef HAC_PROCESS10140RECORD_H_
#define HAC_PROCESS10140RECORD_H_

#include "HAC_Tuples.H"


typedef struct  {
	unsigned int 	TupleSize;
	unsigned short 	TupleType;
	unsigned short 	TimeFraction;
	unsigned int	TimeCPU;
	unsigned short 	AttitudeSensorId;
	short 			Pitch;
	short 			Roll;
	short 			Heave;
	short 			Yaw;
	short 			Space;
	int 			TupleAttribute;
	unsigned int	BackLink;
	unsigned int	IndexTupleRoot;
	unsigned int	IndexTupleParent;
} T_TUPLE10140_SIGNAL;


int iProcess10140Record(FILE *fpFileData,
						int iNbRecord,
						int iIndexTupleRoot,
						int iIndexTupleParent,
						int uiSounderId,
						T_HACRECORDXML *strHACRecordXML,
						T_TUPLE10140_SIGNAL *ptrSignal_prevRecord);

int iInitXMLBin10140Record(	unsigned int uiSounderID,
							char *cTitleDatagram,
							char *cNomDatagram,
							char *cComments,
							T_HACRECORDXML *strHACRecordXML);

#endif /* HAC_PROCESS10140RECORD_H_ */

#pragma pack()
