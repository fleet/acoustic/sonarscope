/*
 * HAC_process11000Record.c
 *
 *  Created on: 23 mai 2011
 *      Author: rgallou
 */
#pragma pack(1)

#define NB_SIGNAL 		8
#define NB_IMAGE 		7
#define NB_IMAGE2 		0
#define NUM_INDEX_TABLE	5	// Identification de l'index Signal des Images (� partir de 0).

#include <sys/types.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "HAC_Tuples.h"
#include "HAC_Utilities.h"
#include "Generic_Utilities.h"
#include "HAC_process11000Record.h"
#include "convertFiles_HAC.h"


//----------------------------------------------------------------
// Fonction :	iProcess11000Record
// Objet 	:	Traitement du Tuple 11000
// Modif.	: 	20/05/11
// Auteur 	:	GLU
//----------------------------------------------------------------
int iProcess11000Record(FILE *fpFileData,
						int iNbRecord,
						T_HACRECORDXML *strHACRecordXML,
						T_TUPLE11000_SIGNAL *ptrSignal_prevRecord)
{
	T_TUPLE11000_SIGNAL	*ptrSignal_Record;

	// Le Tuple 11000 est vu autant de fois qu'il y a de Pings et de faisceaux.
	// Pour un Ping, il y a une succession de N faisceaux.
	// Pointeur pour avancer sur les variables automatiquement.
	unsigned char		*ptr_cSignalRecord;
	static void 		*ptr_vVarPrevRecord;
	void				*ptr_vVarRecord,
						*ptr_vBidon;

	BOOL		bFlagVarConstant = TRUE,
				bFlagCompare = TRUE;

    int 	iFlag,
			iVarSignal,
			iVarImage,
			iRet = 0,
			iLoop,
			iTailleBufferSignal;

    long llPosFic;

    unsigned short	usTailleEnByte,
					usNbElem;

    // Initialisation de la structure d'identification du paquet
 	if (iNbRecord == 0)
	{
 		iInitXMLBin11000Record(	(unsigned int)(0),
 								"STD Profile",
								"STDProfile",
								"STD Profile-Tuple 11000",
								strHACRecordXML);
	}

    // Lecture des signaux du paquet
 	iTailleBufferSignal = sizeof(T_TUPLE11000_SIGNAL) - sizeof(ptrSignal_Record->TupleAttribute) - sizeof(ptrSignal_Record->BackLink);

 	ptrSignal_Record = malloc(iTailleBufferSignal);
	iFlag = fread(ptrSignal_Record, iTailleBufferSignal, 1, fpFileData);

	if (iFlag == 0)
	{
		printf("%s -- Error in reading : %s\n", __FILE__, G_cFileData);
		return FALSE;
	}
	ptr_cSignalRecord = (unsigned char*)ptrSignal_Record;

	// Sauvegarde des donn�es des attributs.
	if (iNbRecord > 0)
	{
		ptr_vVarPrevRecord = (void *)ptrSignal_prevRecord;
	}

	// On ne lit pas tout de suite TupleAttribute et BackLink.
	for (iVarSignal=0; iVarSignal<(int)strHACRecordXML->sNbVarSignal-2; iVarSignal++)
	{
		ptr_vVarRecord = (void *)ptr_cSignalRecord;
		if (iVarSignal==0)
		{
			usTailleEnByte = 0;
			usNbElem = 0;
		}
		else
		{
			usTailleEnByte = util_usNbOctetTypeVar(strHACRecordXML->strTabSignalXML[iVarSignal-1].cType);
			usNbElem = strHACRecordXML->strTabSignalXML[iVarSignal-1].usNbElem;
		}
		// Pointage sur la donn�e suivante.
		ptr_vVarPrevRecord = ptr_vVarPrevRecord + usTailleEnByte*usNbElem;
		// On ne fait pas le test de constance au 1er enregistrement ou si le signal
		// est d�j� vue comme variable.
		if (iNbRecord > 0 && strHACRecordXML->strTabSignalXML[iVarSignal].bFlagConstant == TRUE)
		{
			bFlagVarConstant = strHACRecordXML->strTabSignalXML[iVarSignal].bFlagConstant;
			if (G_iFlagEndian == 0) // On inverse en Big Endian.
			{
				// Observation de la taille propre � la variable.
				usTailleEnByte = util_usNbOctetTypeVar(strHACRecordXML->strTabSignalXML[iVarSignal].cType);
				usNbElem = strHACRecordXML->strTabSignalXML[iVarSignal].usNbElem;
				if (usTailleEnByte == 2) // Pour des (u)short, (u)int, (u)long.
					ptr_vVarPrevRecord = (void *)util_cBswap_16(ptr_vVarPrevRecord);
				else if (usTailleEnByte == 4)
					ptr_vVarPrevRecord = (void *)util_cBswap_32(ptr_vVarPrevRecord);
				else if (usTailleEnByte == 8)
					ptr_vVarPrevRecord = (void *)util_cBswap_64(ptr_vVarPrevRecord);
			}
			bFlagCompare = util_bComparePrevAndCurrentVal(strHACRecordXML->strTabSignalXML[iVarSignal].cType,
												ptr_vVarPrevRecord,
												ptr_vVarRecord,
											   strHACRecordXML->strTabSignalXML[iVarSignal].usNbElem);
			bFlagVarConstant = bFlagVarConstant && bFlagCompare;
			strHACRecordXML->strTabSignalXML[iVarSignal].bFlagConstant = bFlagVarConstant;
		}


		// Pr�paration de l'�criture de la variable.
		usTailleEnByte = strHACRecordXML->strTabSignalXML[iVarSignal].usSize;
		usNbElem =  strHACRecordXML->strTabSignalXML[iVarSignal].usNbElem;
		if (G_iFlagEndian == 0) // On inverse en Big Endian.
		{
			if (usTailleEnByte == 2) // Pour des (u)short, (u)int, (u)long.
				ptr_cSignalRecord = (unsigned char*)util_cBswap_16((char*)ptr_cSignalRecord);
			else if (usTailleEnByte == 4)
				ptr_cSignalRecord = (unsigned char*)util_cBswap_32((char*)ptr_cSignalRecord);
			else if (usTailleEnByte == 8)
				ptr_cSignalRecord = (unsigned char*)util_cBswap_64((char*)ptr_cSignalRecord);
		}
		fwrite(ptr_cSignalRecord, usTailleEnByte, usNbElem, strHACRecordXML->strTabSignalXML[iVarSignal].ptr_fpBin);
		strHACRecordXML->strTabSignalXML[iVarSignal].ptr_vPrevVal = ptr_cSignalRecord;
		ptr_cSignalRecord += usTailleEnByte*usNbElem;
	}
	// Recopie en zone tampon des valeurs courantes.
	memmove(ptrSignal_prevRecord, ptrSignal_Record, sizeof(T_TUPLE11000_SIGNAL));


	// --------------------------------------------
 	// Lecture unitaire des variables de type Image
	for (iLoop=0; iLoop< ptrSignal_Record->NumberOfMeasurements; iLoop++)
	{
		// Lecture unitaire des variables.
		for (iVarImage=0; iVarImage< strHACRecordXML->sNbVarImage; iVarImage++)
		{
			usTailleEnByte = strHACRecordXML->strTabImageXML[iVarImage].usSize;
			usNbElem = strHACRecordXML->strTabImageXML[iVarImage].usNbElem;
			ptr_vBidon = (void *)malloc((usTailleEnByte*usNbElem)*sizeof(char));
			iFlag = fread(ptr_vBidon, usTailleEnByte, usNbElem, fpFileData);
			if (G_iFlagEndian == 0) // On inverse en Big Endian.
			{
				if (usTailleEnByte == 2) // Pour des (u)short, (u)int, (u)long.
					ptr_vBidon = (void *)util_cBswap_16(ptr_vBidon);
				else if (usTailleEnByte == 4)
					ptr_vBidon = (void *)util_cBswap_32(ptr_vBidon);
				else if (usTailleEnByte == 8)
					ptr_vBidon = (void *)util_cBswap_64(ptr_vBidon);
			}
			iFlag = fwrite(ptr_vBidon, usTailleEnByte, usNbElem, strHACRecordXML->strTabImageXML[iVarImage].ptr_fpBin);
			if (iLoop > 0 && strHACRecordXML->strTabImageXML[iVarImage].bFlagConstant == TRUE)
			{
				bFlagCompare = util_bComparePrevAndCurrentVal(strHACRecordXML->strTabImageXML[iVarImage].cType,
													strHACRecordXML->strTabImageXML[iVarImage].ptr_vPrevVal,
													ptr_vBidon,
													usNbElem);
				strHACRecordXML->strTabImageXML[iVarImage].bFlagConstant = bFlagCompare && strHACRecordXML->strTabImageXML[iVarImage].bFlagConstant;
			}
			if (iNbRecord == 0)
			{
				strHACRecordXML->strTabImageXML[iVarImage].ptr_vPrevVal = malloc(usTailleEnByte*usNbElem*sizeof(char));
			}
			memmove(strHACRecordXML->strTabImageXML[iVarImage].ptr_vPrevVal, ptr_vBidon, usTailleEnByte*usNbElem);

			free(ptr_vBidon);

		} // Fin de la boucles sur les variables.
	}

	// Traitement du signal TupleAttribute et BackLink exploit� apr�s la lecture
	// des �chantillons.
	for (iVarSignal=(int)strHACRecordXML->sNbVarSignal-2; iVarSignal<(int)strHACRecordXML->sNbVarSignal; iVarSignal++)
	{
		usTailleEnByte		= util_usNbOctetTypeVar(strHACRecordXML->strTabSignalXML[iVarSignal].cType);
		usNbElem 			= strHACRecordXML->strTabSignalXML[iVarSignal].usNbElem;
		ptr_vBidon			= (void *)malloc((usTailleEnByte*usNbElem)*sizeof(char));
		iFlag 				= fread(ptr_vBidon, usTailleEnByte, usNbElem, fpFileData);

		if (G_iFlagEndian == 0) // On inverse en Big Endian.
		{
			if (usTailleEnByte == 2) // Pour des (u)short, (u)int, (u)long.
				ptr_vBidon = (void *)util_cBswap_16(ptr_vBidon);
			else if (usTailleEnByte == 4)
				ptr_vBidon = (void *)util_cBswap_32(ptr_vBidon);
			else if (usTailleEnByte == 8)
				ptr_vBidon = (void *)util_cBswap_64(ptr_vBidon);
		}
		iFlag = fwrite(ptr_vBidon, usTailleEnByte, usNbElem, strHACRecordXML->strTabSignalXML[iVarSignal].ptr_fpBin);
		if (iNbRecord > 0 && strHACRecordXML->strTabSignalXML[iVarSignal].bFlagConstant == TRUE)
		{
			bFlagCompare = util_bComparePrevAndCurrentVal(strHACRecordXML->strTabSignalXML[iVarSignal].cType,
												strHACRecordXML->strTabSignalXML[iVarSignal].ptr_vPrevVal,
												ptr_vBidon,
												usNbElem);
			strHACRecordXML->strTabSignalXML[iVarSignal].bFlagConstant = bFlagCompare && strHACRecordXML->strTabSignalXML[iVarSignal].bFlagConstant;
		}
		if (iNbRecord == 0)
		{
			strHACRecordXML->strTabSignalXML[iVarSignal].ptr_vPrevVal = malloc(usTailleEnByte*usNbElem*sizeof(char));
		}
		// Recopie en zone tampon des valeurs courantes.
		memmove(strHACRecordXML->strTabSignalXML[iVarSignal].ptr_vPrevVal, ptr_vBidon, usTailleEnByte*usNbElem);
	}

	free(ptrSignal_Record);

	iNbRecord++;

	return iRet;

} //iProcess11000Record


//----------------------------------------------------------------
// Fonction :	iInitXMLBin11000Record
// Objet 	:	Initialisation de la structure �quivalent au Tuple
//				11000 pour la manipulation des fichiers bin et XML.
// Modification : 15/10/08
// Auteur 	: GLU
//----------------------------------------------------------------
int iInitXMLBin11000Record(	unsigned int uiSounderID,
							char *cTitleDatagram,
							char *cNomDatagram,
							char *cComments,
							T_HACRECORDXML *strHACRecordXML)
{
	int iRet;


	T_DESC_SIGNAL_OR_IMAGE strDescriptionSignal[NB_SIGNAL] = {
								{"TupleSize","unsigned int", 				1.0, 0.0},
								{"TupleType","unsigned short", 				1.0, 0.0},
								{"TimeFraction","unsigned short", 			0.0001, 0.0},
								{"TimeCPU","unsigned int", 					1.0, 0.0},
								{"SensorType","unsigned short", 			1.0, 0.0},
								{"NumberOfMeasurements","unsigned short", 	1.0, 0.0},
								{"TupleAttribute","int", 					1.0, 0.0},
								{"BackLink","unsigned int", 				1.0, 0.0}};

	T_DESC_SIGNAL_OR_IMAGE strDescriptionImage[NB_IMAGE] = {
								{"Pressure","unsigned int", 				0.001, 0.0},
								{"Temperature","int", 						0.0001, 0.0},
								{"Conductivity","unsigned short", 			0.001, 0.0},
								{"SoundVelocity","unsigned short", 			0.1, 0.0},
								{"Depth","unsigned int", 					0.0001, 0.0},
								{"Salinity","unsigned int", 				0.001, 0.0},
								{"Absorption","unsigned int", 				0.0001, 0.0}};

	T_DESC_SIGNAL_OR_IMAGE strDescriptionImage2[NB_IMAGE2] = {};

	T_DESC_TAB_SIGANDIMG strDescriptionTab = {	NB_SIGNAL,
												NB_IMAGE,
												NB_IMAGE2,
												NUM_INDEX_TABLE,
												strDescriptionSignal,
												strDescriptionImage,
												strDescriptionImage2};


	// Lancement de l'initialisation des structures de signaux : ouvertures des
	// fichiers, copie des structures, ...
	iRet = utilHAC_initXMLRecord(	uiSounderID,
									strDescriptionTab,
									cTitleDatagram,
									cNomDatagram,
									cComments,
									strHACRecordXML);

	return iRet;

} // iInitXMLBin11000Record

#pragma pack()
