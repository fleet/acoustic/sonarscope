/*
 * HAC_process11000Record.h
 *
 *  Created on: 31 05 2011
 *      Author: rgallou
 */
#pragma pack(1)

#ifndef HAC_PROCESS11000RECORD_H_
#define HAC_PROCESS11000RECORD_H_

#include "HAC_Tuples.H"


typedef struct  {
	unsigned int 	TupleSize;
	unsigned short 	TupleType;
	unsigned short 	TimeFraction;
	unsigned int	TimeCPU;
	unsigned short 	SensorType;
	unsigned short 	NumberOfMeasurements;
	int 			TupleAttribute;
	unsigned int	BackLink;
} T_TUPLE11000_SIGNAL;

typedef struct  {
	unsigned int	*Pressure;
	int				*Temperature;
	unsigned short	*Conductivity;
	unsigned short	*SoundVelocity;
	unsigned int	*Depth;
	unsigned int	*Salinity;
	unsigned int	*Absorption;
} T_11000_RECORD_IMAGE;

int iProcess11000Record(FILE *fpFileData,
						int iNbRecord,
						T_HACRECORDXML *strHACRecordXML,
						T_TUPLE11000_SIGNAL *ptrSignal_prevRecord);


int iInitXMLBin11000Record(	unsigned int uiSounderID,
							char *cTitleDatagram,
							char *cNomDatagram,
							char *cComments,
							T_HACRECORDXML *strHACRecordXML);


#endif // HAC_PROCESS11000RECORD_H_

#pragma pack()
