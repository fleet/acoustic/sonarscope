/*
 * HAC_process20Record.h
 *
 *  Created on: 23 mai 2011
 *      Author: rgallou
 */
#pragma pack(1)

#ifndef HAC_PROCESS20RECORD_H_
#define HAC_PROCESS20RECORD_H_

#include "HAC_Tuples.h"


typedef struct  {
	unsigned int 	TupleSize;
	unsigned short 	TupleType;
	unsigned short 	TimeFraction;
	unsigned int	TimeCPU;
	unsigned int 	TimeGPS;
	unsigned short 	PositSystem;
	unsigned short 	Space;
	int 			Latitude;
	int 			Longitude;
	int 			TupleAttribute;
	unsigned int	BackLink;
} T_TUPLE20_SIGNAL;


int iProcess20Record(FILE *fpFileData,
						int iNbRecord,
						T_HACRECORDXML *strHACRecordXML,
						T_TUPLE20_SIGNAL *ptrSignal_prevRecord);


int iInitXMLBin20Record(	unsigned int uiSounderID,
							char *cTitleDatagram,
							char *cNomDatagram,
							char *cComments,
							T_HACRECORDXML *strHACRecordXML);

#endif /* HAC_PROCESS20RECORD_H_ */

#pragma pack()
