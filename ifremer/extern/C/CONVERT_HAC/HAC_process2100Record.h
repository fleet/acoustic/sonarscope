/*
 * HAC_process2100Record.h
 *
 *  Created on: 23 mai 2011
 *      Author: rgallou
 */
#pragma pack(1)

#ifndef HAC_PROCESS2100RECORD_H_
#define HAC_PROCESS2100RECORD_H_

#include "HAC_Tuples.h"


typedef struct  {
	unsigned int	 	TupleSize;
	unsigned short	 	TupleType;
	unsigned short	 	SoftwareChannelIdentifier;
	unsigned int	 	EchosounderDocIdentifier;
	char	 			FrequencyChannelName[48];
	char 				TransceiverSoftwareVersion[30];
	char 				TransducerName[30];
	unsigned int	 	TimeSampleInterval;
	unsigned short	 	DataType;
	unsigned short	 	BeamType;
	unsigned int	 	AcousticFrequency;
	unsigned int	 	TransducerInstallationDepth;
	unsigned int	 	StartSample;
	unsigned short	 	PlatformIdentifier;
	unsigned short 		TransducerShape;
	int 				TransducerFaceAlongshipAngleOffset;
	int 				TransducerFaceAthwartshipAngleOffset;
	int 				TransducerRotationAngle;
	int					AlongshipAngleOffsetToMainAxisOfAcBeam;
	int					AthwartshipAngleOffsetToMainAxisOfAcBeam;
	unsigned int	 	AbsorptionCoefficient;
	unsigned int	 	PulseDuration;
	unsigned int	 	Bandwidth;
	unsigned int	 	TransmissionPower;
	unsigned int	 	BeamAlongshipAngleSensitivity;
	unsigned int	 	BeamAthwartshipAngleSensitivity;
	unsigned int	 	BeamAlongship3dBBeamWidth;
	unsigned int	 	BeamAthwartship3dBBeamWidth;
	int	 				BeamEquivalentTwoWayBeamAngle;
	unsigned int	 	BeamGain;
	int	 				BeamSACorrection;
	unsigned int	 	BottomDetectionMinimumDepth;
	unsigned int	 	BottomDetectionMaximumDepth;
	int	 				BottomDetectionMinimumLevel;
	char	 			Remarks[40];
	int	 				TupleAttribute;
	unsigned int	 	BackLink;
	unsigned int		IndexTupleParent;
} T_TUPLE2100_SIGNAL;


int iProcess2100Record(FILE *fpFileData,
						int iNbRecord,
						unsigned int uiEchoSounderId,
						unsigned int uiIndexTupleParent,
						T_HACRECORDXML *strHACRecordXML,
						T_TUPLE2100_SIGNAL *ptrSignal_prevRecord);


int iInitXMLBin2100Record(	unsigned int uiSounderID,
							char *cTitleDatagram,
							char *cNomDatagram,
							char *cComments,
							T_HACRECORDXML *strHACRecordXML);

#endif /* HAC_PROCESS2100RECORD_H_ */

#pragma pack()
