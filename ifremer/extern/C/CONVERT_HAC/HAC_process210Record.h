/*
 * HAC_process210Record.h
 *
 *  Created on: 23 mai 2011
 *      Author: rgallou
 */
#pragma pack(1)

#ifndef HAC_PROCESS210RECORD_H_
#define HAC_PROCESS210RECORD_H_

#include "HAC_Tuples.h"

typedef struct  {
	int 			TupleSize;
	unsigned short 	TupleType;
	unsigned short 	NumberSWChannels;
	unsigned int 	EchosounderDocIdentifier;
	unsigned short 	SoundSpeed;
	unsigned short 	PingMode;
	unsigned short 	PingInterval;
	unsigned short 	Space;
	char 			Remarks[40];
	int 			TupleAttribute;
	unsigned int	BackLink;
} T_TUPLE210_SIGNAL;


int iProcess210Record(FILE *fpFileData,
						int iNbRecord,
						unsigned int uiEchoSounderId,
						T_HACRECORDXML *strHACRecordXML,
						T_TUPLE210_SIGNAL *ptrSignal_prevRecord);


int iInitXMLBin210Record(	unsigned int uiSounderID,
							char *cTitleDatagram,
							char *cNomDatagram,
							char *cComments,
							T_HACRECORDXML *strHACRecordXML);

#endif /* HAC_PROCESS210RECORD_H_ */

#pragma pack()
