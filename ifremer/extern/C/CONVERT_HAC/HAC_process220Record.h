/*
 * HAC_process220Record.h
 *
 *  Created on: 23 mai 2011
 *      Author: rgallou
 */
#pragma pack(1)

#ifndef HAC_PROCESS220RECORD_H_
#define HAC_PROCESS220RECORD_H_

#include "HAC_Tuples.h"


typedef struct  {
	int 			TupleSize;
	unsigned short 	TupleType;
	unsigned short 	NumberSWChannels;
	unsigned int 	EchosounderDocIdentifier;
	char 			TransducerName[50];
	char 			TransceiverSoftwareVersion[30];
	unsigned short 	SoundSpeed;
	unsigned short 	TriggerMode;
	unsigned short 	PingInterval;
	unsigned short 	PulseMode;
	unsigned int 	PulseDuration;
	unsigned int 	TimeSampleInterval;
	unsigned short 	FrequencyBeamSpacing;
	unsigned short 	FrequencySpaceShape;
	unsigned int 	TransceiverPower;
	unsigned int 	TransducerInstallationDepth;
	unsigned short 	PlatformIdentifier;
	unsigned short 	TransducerShape;
	int 			TransducerFaceAlongshipAngleOffset;
	int 			TransducerFaceAthwartshipAngleOffset;
	int 			TransducerRotationAngle;
	char 			Remarks[40];
	int 			TupleAttribute;
	unsigned int	BackLink;
} T_TUPLE220_SIGNAL;


int iProcess220Record(FILE *fpFileData,
						int iNbRecord,
						unsigned int uiEchoSounderId,
						T_HACRECORDXML *strHACRecordXML,
						T_TUPLE220_SIGNAL *ptrSignal_prevRecord);


int iInitXMLBin220Record(	unsigned int uiSounderID,
							char *cTitleDatagram,
							char *cNomDatagram,
							char *cComments,
							T_HACRECORDXML *strHACRecordXML);

#endif /* HAC_PROCESS220RECORD_H_ */

#pragma pack()
