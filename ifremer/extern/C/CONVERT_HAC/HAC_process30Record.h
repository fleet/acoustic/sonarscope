/*
 * HAC_process30Record.h
 *
 *  Created on: 23 mai 2011
 *      Author: rgallou
 */
#pragma pack(1)

#ifndef HAC_PROCESS30RECORD_H_
#define HAC_PROCESS30RECORD_H_

#include "HAC_Tuples.h"


typedef struct  {
	unsigned int 	TupleSize;
	unsigned short 	TupleType;
	unsigned short 	TimeFraction;
	unsigned int	TimeCPU;
	unsigned short 	NavSystem;
	short 			Heading;
	short 			Speed;
	unsigned short 	Space;
	int 			TupleAttribute;
	unsigned int	BackLink;
} T_TUPLE30_SIGNAL;


int iProcess30Record(FILE *fpFileData,
						int iNbRecord,
						T_HACRECORDXML *strHACRecordXML,
						T_TUPLE30_SIGNAL *ptrSignal_prevRecord);


int iInitXMLBin30Record(	unsigned int uiSounderID,
							char *cTitleDatagram,
							char *cNomDatagram,
							char *cComments,
							T_HACRECORDXML *strHACRecordXML);

#endif /* HAC_PROCESS30RECORD_H_ */

#pragma pack()
