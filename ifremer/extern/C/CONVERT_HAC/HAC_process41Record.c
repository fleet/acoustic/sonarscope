/*
 * HAC_process41Record.c
 *
 *  Created on: 23 mai 2011
 *      Author: rgallou
 */
#pragma pack(1)

#define NB_SIGNAL 		16
#define NB_IMAGE 		0
#define NB_IMAGE2 		0
#define NUM_INDEX_TABLE	-1

#include <sys/types.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "HAC_Tuples.h"
#include "HAC_Utilities.h"
#include "Generic_Utilities.h"
#include "HAC_process41Record.h"
#include "convertFiles_HAC.h"


//----------------------------------------------------------------
// Fonction :	iProcess41Record
// Objet 	:	Traitement du Tuple 41
// Modif.	: 	20/05/11
// Auteur 	:	GLU
//----------------------------------------------------------------
int iProcess41Record(FILE *fpFileData,
						int iNbRecord,
						int iIndexTupleRoot,
						int iIndexTupleParent,
						int uiSounderId,
						T_HACRECORDXML *strHACRecordXML,
						T_TUPLE41_SIGNAL *ptrSignal_prevRecord)
{
	T_TUPLE41_SIGNAL	*ptrSignal_Record;

	// Pointeur pour avancer sur les variables automatiquement.
	unsigned char		*ptr_cSignalRecord;
	static void 		*ptr_vVarPrevRecord;
	void				*ptr_vVarRecord;

	BOOL		bFlagVarConstant = TRUE,
				bFlagCompare = TRUE;

    int 	iFlag,
			iVarSignal,
			iVarImage,
			iRet = 0,
			iLoop,
			iTailleBufferSignal;

    long llPosFic;

    unsigned short	usTailleEnByte,
					usNbElem;

    char cNomDatagram[100];


     // Initialisation de la structure d'identification du paquet
  	if (iNbRecord == 0)
 	{
  		sprintf(cNomDatagram, "PlatformAttitudeParameters");
 		iInitXMLBin41Record(uiSounderId,
 							"Platform attitude parameters",
							cNomDatagram,
							"Platform attitude parameters-Tuple 41",
							strHACRecordXML);
	}

    // Lecture des signaux du paquet
 	iTailleBufferSignal = sizeof(T_TUPLE41_SIGNAL)-sizeof(ptrSignal_Record->IndexTupleRoot)-
 						  sizeof(ptrSignal_Record->IndexTupleParent);

 	ptrSignal_Record = malloc(iTailleBufferSignal);
	iFlag = fread(ptrSignal_Record, iTailleBufferSignal, 1, fpFileData);

	if (iFlag == 0)
	{
		printf("%s -- Error in reading : %s\n", __FILE__, G_cFileData);
		return FALSE;
	}
	ptr_cSignalRecord = (unsigned char*)ptrSignal_Record;

	// Sauvegarde des donn�es des attributs.
	if (iNbRecord > 0)
	{
		ptr_vVarPrevRecord = (void *)ptrSignal_prevRecord;
	}

	for (iVarSignal=0; iVarSignal<(int)strHACRecordXML->sNbVarSignal-2; iVarSignal++)
	{
		ptr_vVarRecord = (void *)ptr_cSignalRecord;
		if (iVarSignal==0)
		{
			usTailleEnByte = 0;
			usNbElem = 0;
		}
		else
		{
			usTailleEnByte = util_usNbOctetTypeVar(strHACRecordXML->strTabSignalXML[iVarSignal-1].cType);
			usNbElem = strHACRecordXML->strTabSignalXML[iVarSignal-1].usNbElem;
		}
		// Pointage sur la donn�e suivante.
		ptr_vVarPrevRecord = ptr_vVarPrevRecord + usTailleEnByte*usNbElem;
		// On ne fait pas le test de constance au 1er enregistrement ou si le signal
		// est d�j� vue comme variable.
		if (iNbRecord > 0 && strHACRecordXML->strTabSignalXML[iVarSignal].bFlagConstant == TRUE)
		{
			bFlagVarConstant = strHACRecordXML->strTabSignalXML[iVarSignal].bFlagConstant;
			if (G_iFlagEndian == 0) // On inverse en Big Endian.
			{
				// Observation de la taille propre � la variable.
				usTailleEnByte = util_usNbOctetTypeVar(strHACRecordXML->strTabSignalXML[iVarSignal].cType);
				usNbElem = strHACRecordXML->strTabSignalXML[iVarSignal].usNbElem;
				if (usTailleEnByte == 2) // Pour des (u)short, (u)int, (u)long.
					ptr_vVarPrevRecord = (void *)util_cBswap_16(ptr_vVarPrevRecord);
				else if (usTailleEnByte == 4)
					ptr_vVarPrevRecord = (void *)util_cBswap_32(ptr_vVarPrevRecord);
				else if (usTailleEnByte == 8)
					ptr_vVarPrevRecord = (void *)util_cBswap_64(ptr_vVarPrevRecord);
			}
			bFlagCompare = util_bComparePrevAndCurrentVal(strHACRecordXML->strTabSignalXML[iVarSignal].cType,
												ptr_vVarPrevRecord,
												ptr_vVarRecord,
											   strHACRecordXML->strTabSignalXML[iVarSignal].usNbElem);
			bFlagVarConstant = bFlagVarConstant && bFlagCompare;
			strHACRecordXML->strTabSignalXML[iVarSignal].bFlagConstant = bFlagVarConstant;
		}


		// Pr�paration de l'�criture de la variable.
		usTailleEnByte = strHACRecordXML->strTabSignalXML[iVarSignal].usSize;
		usNbElem =  strHACRecordXML->strTabSignalXML[iVarSignal].usNbElem;
		if (G_iFlagEndian == 0) // On inverse en Big Endian.
		{
			if (usTailleEnByte == 2) // Pour des (u)short, (u)int, (u)long.
				ptr_cSignalRecord = (unsigned char*)util_cBswap_16((char*)ptr_cSignalRecord);
			else if (usTailleEnByte == 4)
				ptr_cSignalRecord = (unsigned char*)util_cBswap_32((char*)ptr_cSignalRecord);
			else if (usTailleEnByte == 8)
				ptr_cSignalRecord = (unsigned char*)util_cBswap_64((char*)ptr_cSignalRecord);
		}
		fwrite(ptr_cSignalRecord, usTailleEnByte, usNbElem, strHACRecordXML->strTabSignalXML[iVarSignal].ptr_fpBin);
		strHACRecordXML->strTabSignalXML[iVarSignal].ptr_vPrevVal = ptr_cSignalRecord;
		ptr_cSignalRecord += usTailleEnByte*usNbElem;
	}
	// Recopie en zone tampon des valeurs courantes.
	memmove(ptrSignal_prevRecord, ptrSignal_Record, sizeof(T_TUPLE41_SIGNAL));


	free(ptrSignal_Record);

	// Traitement exceptionnel pour la donn�e IndexTupleRoot :
	iVarSignal = strHACRecordXML->sNbVarSignal-2;
	fwrite(		&iIndexTupleRoot,
				strHACRecordXML->strTabSignalXML[iVarSignal].usSize,
				strHACRecordXML->strTabSignalXML[iVarSignal].usNbElem,
				strHACRecordXML->strTabSignalXML[iVarSignal].ptr_fpBin);
	strHACRecordXML->strTabSignalXML[iVarSignal].bFlagConstant = FALSE;

	// Traitement exceptionnel pour la donn�e IndexTupleParent :
	iVarSignal = strHACRecordXML->sNbVarSignal-1;
	fwrite(		&iIndexTupleParent,
				strHACRecordXML->strTabSignalXML[iVarSignal].usSize,
				strHACRecordXML->strTabSignalXML[iVarSignal].usNbElem,
				strHACRecordXML->strTabSignalXML[iVarSignal].ptr_fpBin);
	strHACRecordXML->strTabSignalXML[iVarSignal].bFlagConstant = FALSE;


	iNbRecord++;

	return iRet;

} //iProcess41Record


//----------------------------------------------------------------
// Fonction :	iInitXMLBin41Record
// Objet 	:	Initialisation de la structure �quivalent au Tuple
//				41 pour la manipulation des fichiers bin et XML.
// Modification : 15/10/08
// Auteur 	: GLU
//----------------------------------------------------------------
int iInitXMLBin41Record(	unsigned int uiSounderID,
							char *cTitleDatagram,
							char *cNomDatagram,
							char *cComments,
							T_HACRECORDXML *strHACRecordXML)
{
	int iRet;


	T_DESC_SIGNAL_OR_IMAGE strDescriptionSignal[NB_SIGNAL] = {
			{	"TupleSize","int", 									1.0, 0.0},
			{	"TupleType","unsigned short", 						1.0, 0.0},
			{	"TimeFraction","unsigned short", 					0.0001, 0.0},
			{	"TimeCPU","unsigned int", 							1.0, 0.0},
			{	"AttitudeSensorId","unsigned short", 				1.0, 0.0},
			{	"TransceiverChannelNumber","unsigned short",		1.0, 0.0},
			{	"PlatformType","unsigned short", 					1.0, 0.0},
			{	"AlongOffset","unsigned short", 					0.01, 0.0},
			{	"AthwartOffset","unsigned short", 					0.01, 0.0},
			{	"ElevationOffset","unsigned short", 				0.01, 0.0},
			{	"Remarks","char", 								 	1.0, 0.0},
			{	"Space","unsigned short", 							1.0, 0.0},
			{	"TupleAttribute","int", 							1.0, 0.0},
			{	"BackLink","unsigned int", 							1.0, 0.0},
			{"IndexTupleRoot","unsigned int",          				1.0, 0.0},
			{"IndexTupleParent","unsigned int",          			1.0, 0.0}};


	T_DESC_SIGNAL_OR_IMAGE strDescriptionImage[NB_IMAGE] = {};
	T_DESC_SIGNAL_OR_IMAGE strDescriptionImage2[NB_IMAGE2] = {};

	T_DESC_TAB_SIGANDIMG strDescriptionTab = {	NB_SIGNAL,
												NB_IMAGE,
												NB_IMAGE2,
												NUM_INDEX_TABLE,
												strDescriptionSignal,
												strDescriptionImage,
												strDescriptionImage2};

	// Lancement de l'initialisation des structures de signaux : ouvertures des
	// fichiers, copie des structures, ...
	iRet = utilHAC_initXMLRecord(	uiSounderID,
									strDescriptionTab,
									cTitleDatagram,
									cNomDatagram,
									cComments,
									strHACRecordXML);


	int iVarSignal = 10;
	strHACRecordXML->strTabSignalXML[iVarSignal].usNbElem = 30;

	return iRet;
} // iInitXMLBin41Record

#pragma pack()
