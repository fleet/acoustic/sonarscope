/*
 * HAC_process41Record.h
 *
 *  Created on: 23 mai 2011
 *      Author: rgallou
 */
#pragma pack(1)

#ifndef HAC_PROCESS41RECORD_H_
#define HAC_PROCESS41RECORD_H_

#include "HAC_Tuples.h"


typedef struct  {
	int 			TupleSize;
	unsigned short 	TupleType;
	unsigned short 	TimeFraction;
	unsigned int	TimeCPU;
	unsigned short 	AttitudeSensorId;
	unsigned short 	TransceiverChannelNumber;
	unsigned short 	PlatformType;
	unsigned short 	AlongOffset;
	unsigned short 	AthwartOffset;
	unsigned short 	ElevationOffset;
	char 			Remarks[30];
	unsigned short 	Space;
	int 			TupleAttribute;
	unsigned int	BackLink;
	unsigned int	IndexTupleRoot;
	unsigned int	IndexTupleParent;
} T_TUPLE41_SIGNAL;


int iProcess41Record(FILE *fpFileData,
						int iNbRecord,
						int iIndexTupleRoot,
						int iIndexTupleParent,
						int uiSounderId,
						T_HACRECORDXML *strHACRecordXML,
						T_TUPLE41_SIGNAL *ptrSignal_prevRecord);


int iInitXMLBin41Record(	unsigned int uiSounderID,
							char *cTitleDatagram,
							char *cNomDatagram,
							char *cComments,
							T_HACRECORDXML *strHACRecordXML);

#endif /* HAC_PROCESS41RECORD_H_ */

#pragma pack()
