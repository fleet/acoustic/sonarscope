/*
 * HAC_process65534Record.h
 *
 *  Created on: 23 mai 2011
 *      Author: rgallou
 */
#pragma pack(1)

#ifndef HAC_PROCESS65534RECORD_H_
#define HAC_PROCESS65534RECORD_H_

#include "HAC_Tuples.H"


typedef struct  {
	unsigned int 	TupleSize;
	unsigned short 	TupleType;
	unsigned short 	TimeFraction;
	unsigned int	TimeCPU;
	unsigned short 	ClosingMode;
	int 			TupleAttribute;
	unsigned int	BackLink;
} T_TUPLE65534_SIGNAL;


int iProcess65534Record(FILE *fpFileData,
						int iNbRecord,
						T_HACRECORDXML *strHACRecordXML,
						T_TUPLE65534_SIGNAL *ptrSignal_prevRecord);


int iInitXMLBin65534Record(	unsigned int uiSounderID,
							char *cTitleDatagram,
							char *cNomDatagram,
							char *cComments,
							T_HACRECORDXML *strHACRecordXML);

#endif /* HAC_PROCESS65534RECORD_H_ */

#pragma pack()
