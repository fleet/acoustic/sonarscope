/*
 * HAC_process65535Record.h
 *
 *  Created on: 23 mai 2011
 *      Author: rgallou
 */
#pragma pack(1)

#ifndef HAC_PROCESS65535RECORD_H_
#define HAC_PROCESS65535RECORD_H_

#include "HAC_Tuples.H"


typedef struct  {
	int 			TupleSize;
	unsigned short 	TupleType;
	unsigned short 	HACIdentifier;
	unsigned short	HACVersion;
	unsigned short 	AcqSoftVersion;
	unsigned int	AcqSoftIdentifier;
	int 			TupleAttribute;
	unsigned int	BackLink;
} T_TUPLE65535_SIGNAL;


int iProcess65535Record(FILE *fpFileData,
						int iNbRecord,
						T_HACRECORDXML *strHACRecordXML,
						T_TUPLE65535_SIGNAL *ptrSignal_prevRecord);


int iInitXMLBin65535Record(	unsigned int uiSounderID,
							char *cTitleDatagram,
							char *cNomDatagram,
							char *cComments,
							T_HACRECORDXML *strHACRecordXML);

#endif /* HAC_PROCESS65535RECORD_H_ */

#pragma pack()
