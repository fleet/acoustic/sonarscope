/*
 * HAC_process9001Record.c
 *
 *  Created on: 23 mai 2011
 *      Author: rgallou
 */
#pragma pack(1)

#define NB_SIGNAL 		44
#define NB_IMAGE 		0
#define NB_IMAGE2 		0
#define NUM_INDEX_TABLE	-1

#include <sys/types.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "HAC_Tuples.h"
#include "HAC_Utilities.h"
#include "Generic_Utilities.h"
#include "HAC_process9001Record.h"
#include "convertFiles_HAC.h"


//----------------------------------------------------------------
// Fonction :	iProcess9001Record
// Objet 	:	Traitement du Tuple 9001
// Modif.	: 	20/05/11
// Auteur 	:	GLU
//----------------------------------------------------------------
int iProcess9001Record(FILE *fpFileData,
						int iNbRecord,
						unsigned int uiEchoSounderId,
						unsigned int uiIndexTupleParent,
						T_HACRECORDXML *strHACRecordXML,
						T_TUPLE9001_SIGNAL *ptrSignal_prevRecord)
{
	T_TUPLE9001_SIGNAL	*ptrSignal_Record;

	// Le Tuple 9001 est vu autant de fois qu'il y a de Pings et de faisceaux.
	// Pour un Ping, il y a une succession de N faisceaux.
	// Pointeur pour avancer sur les variables automatiquement.
	unsigned char		*ptr_cSignalRecord;
	static void 		*ptr_vVarPrevRecord;
	void				*ptr_vVarRecord;

	BOOL		bFlagVarConstant = TRUE,
				bFlagCompare = TRUE;

    int 	iFlag,
			iVarSignal,
			iVarImage,
			iRet = 0,
			iLoop,
			iTailleBufferSignal;

    long llPosFic;

    unsigned short	usTailleEnByte,
					usNbElem;

    char cNomDatagram[100];

    // Initialisation de la structure d'identification du paquet
 	if (iNbRecord == 0)
	{
 		sprintf(cNomDatagram, "Channel");
 		iInitXMLBin9001Record(	uiEchoSounderId,
 								"GenericChannel",
								cNomDatagram,
								"Generic Channel-Tuple 9001",
								strHACRecordXML);
	}

    // Lecture des signaux du paquet
 	iTailleBufferSignal = sizeof(T_TUPLE9001_SIGNAL)- sizeof(ptrSignal_Record->IndexTupleParent);

 	ptrSignal_Record = malloc(iTailleBufferSignal);
	iFlag = fread(ptrSignal_Record, iTailleBufferSignal, 1, fpFileData);

	if (iFlag == 0)
	{
		printf("%s -- Error in reading : %s\n", __FILE__, G_cFileData);
		return FALSE;
	}
	ptr_cSignalRecord = (unsigned char*)ptrSignal_Record;

	// Sauvegarde des donn�es des attributs.
	if (iNbRecord > 0)
	{
		ptr_vVarPrevRecord = (void *)ptrSignal_prevRecord;
	}

	for (iVarSignal=0; iVarSignal<(int)strHACRecordXML->sNbVarSignal-1; iVarSignal++)
	{
		ptr_vVarRecord = (void *)ptr_cSignalRecord;
		if (iVarSignal==0)
		{
			usTailleEnByte = 0;
			usNbElem = 0;
		}
		else
		{
			usTailleEnByte = util_usNbOctetTypeVar(strHACRecordXML->strTabSignalXML[iVarSignal-1].cType);
			usNbElem = strHACRecordXML->strTabSignalXML[iVarSignal-1].usNbElem;
		}
		// Pointage sur la donn�e suivante.
		ptr_vVarPrevRecord = ptr_vVarPrevRecord + usTailleEnByte*usNbElem;
		// On ne fait pas le test de constance au 1er enregistrement ou si le signal
		// est d�j� vue comme variable.
		if (iNbRecord > 0 && strHACRecordXML->strTabSignalXML[iVarSignal].bFlagConstant == TRUE)
		{
			bFlagVarConstant = strHACRecordXML->strTabSignalXML[iVarSignal].bFlagConstant;
			if (G_iFlagEndian == 0) // On inverse en Big Endian.
			{
				// Observation de la taille propre � la variable.
				usTailleEnByte = util_usNbOctetTypeVar(strHACRecordXML->strTabSignalXML[iVarSignal].cType);
				usNbElem = strHACRecordXML->strTabSignalXML[iVarSignal].usNbElem;
				if (usTailleEnByte == 2) // Pour des (u)short, (u)int, (u)long.
					ptr_vVarPrevRecord = (void *)util_cBswap_16(ptr_vVarPrevRecord);
				else if (usTailleEnByte == 4)
					ptr_vVarPrevRecord = (void *)util_cBswap_32(ptr_vVarPrevRecord);
				else if (usTailleEnByte == 8)
					ptr_vVarPrevRecord = (void *)util_cBswap_64(ptr_vVarPrevRecord);
			}
			bFlagCompare = util_bComparePrevAndCurrentVal(strHACRecordXML->strTabSignalXML[iVarSignal].cType,
												ptr_vVarPrevRecord,
												ptr_vVarRecord,
											   strHACRecordXML->strTabSignalXML[iVarSignal].usNbElem);
			bFlagVarConstant = bFlagVarConstant && bFlagCompare;
			strHACRecordXML->strTabSignalXML[iVarSignal].bFlagConstant = bFlagVarConstant;
		}


		// Pr�paration de l'�criture de la variable.
		usTailleEnByte = strHACRecordXML->strTabSignalXML[iVarSignal].usSize;
		usNbElem =  strHACRecordXML->strTabSignalXML[iVarSignal].usNbElem;
		if (G_iFlagEndian == 0) // On inverse en Big Endian.
		{
			if (usTailleEnByte == 2) // Pour des (u)short, (u)int, (u)long.
				ptr_cSignalRecord = (unsigned char*)util_cBswap_16((char*)ptr_cSignalRecord);
			else if (usTailleEnByte == 4)
				ptr_cSignalRecord = (unsigned char*)util_cBswap_32((char*)ptr_cSignalRecord);
			else if (usTailleEnByte == 8)
				ptr_cSignalRecord = (unsigned char*)util_cBswap_64((char*)ptr_cSignalRecord);
		}
		fwrite(ptr_cSignalRecord, usTailleEnByte, usNbElem, strHACRecordXML->strTabSignalXML[iVarSignal].ptr_fpBin);
		strHACRecordXML->strTabSignalXML[iVarSignal].ptr_vPrevVal = ptr_cSignalRecord;
		ptr_cSignalRecord += usTailleEnByte*usNbElem;
	}
	// Recopie en zone tampon des valeurs courantes.
	memmove(ptrSignal_prevRecord, ptrSignal_Record, sizeof(T_TUPLE9001_SIGNAL));

	free(ptrSignal_Record);

	// Traitement exceptionnel pour la donn�e IndexTupleParent :
	iVarSignal = strHACRecordXML->sNbVarSignal-1;
	fwrite(		&uiIndexTupleParent,
				strHACRecordXML->strTabSignalXML[iVarSignal].usSize,
				strHACRecordXML->strTabSignalXML[iVarSignal].usNbElem,
				strHACRecordXML->strTabSignalXML[iVarSignal].ptr_fpBin);
	strHACRecordXML->strTabSignalXML[iVarSignal].bFlagConstant = FALSE;



	iNbRecord++;

	return iRet;

} //iProcess9001Record


//----------------------------------------------------------------
// Fonction :	iInitXMLBin9001Record
// Objet 	:	Initialisation de la structure �quivalent au Tuple
//				9001 pour la manipulation des fichiers bin et XML.
// Modification : 15/10/08
// Auteur 	: GLU
//----------------------------------------------------------------
int iInitXMLBin9001Record(	unsigned int uiSounderID,
							char *cTitleDatagram,
							char *cNomDatagram,
							char *cComments,
							T_HACRECORDXML *strHACRecordXML)
{
	int iRet;

	T_DESC_SIGNAL_OR_IMAGE strDescriptionSignal[NB_SIGNAL] = {
			{"TupleSize","unsigned int", 							1.0, 0.0},
			{"TupleType","unsigned short", 							1.0, 0.0},
			{"SoftwareChannelIdentifier","unsigned short", 			1.0, 0.0},
			{"EchosounderDocIdentifier","unsigned int", 			1.0, 0.0},
			{"SamplingRate","unsigned int", 						1.0, 0.0},
			{"SamplingInterval","unsigned int", 					0.000001, 0.0},
			{"AcousticFrequency","unsigned int", 					1.0, 0.0},
			{"TransceiverChannelNumber","unsigned short", 			1.0, 0.0},
			{"TypeOfData","unsigned short", 						1.0, 0.0},
			{"TimeVariedGainMultiplier","unsigned short",          	1.0, 0.0},
			{"TVGBlankingMode","unsigned short",          			1.0, 0.0},
			{"TVGMinimumRange","unsigned short",          			0.1, 0.0},
			{"TVGMaximumRange","unsigned short",          			0.1, 0.0},
			{"BlankingUpToRange","unsigned int",          			0.0001, 0.0},
			{"SampleRange","unsigned int",          				0.0001, 0.0},
			{"TransducerInstallationDepth","unsigned int",		    0.0001, 0.0},
			{"PlatformIdentifier","unsigned short",          		1.0, 0.0},
			{"Space","unsigned short",          					1.0, 0.0},
			{"AlongshipOffsetRelToAttSensor","unsigned int",        0.0001, 0.0},
			{"AthwartshipOffsetRelToAttSensor","unsigned int",      0.0001, 0.0},
			{"VerticalOffsetRelToAttSensor","unsigned int",         0.0001, 0.0},
			{"AlongshipAngleOffsetOfTransducer","short",          	0.01, 0.0},
			{"AthwartshipAngleOffsetOfTransducer","short",          0.01, 0.0},
			{"RotationAngleOffsetOfTransducer","short",         	0.01, 0.0},
			{"AlongshipAngleOffsetToMainAxisOfAcBeam","short",		0.01, 0.0},
			{"AthwartshipAngleOffsetToMainAxisOfAcBeam","short",	0.01, 0.0},
			{"AbsorptionOfSound","unsigned short",          		0.01, 0.0},
			{"PulseDuration","unsigned int",          				0.0001, 0.0},
			{"PulseShapeMode","unsigned short",          			1.0, 0.0},
			{"BandWidth","unsigned short",          				0.01, 0.0},
			{"TransducerShapeMode","unsigned short",          		1.0, 0.0},
			{"BeamAlongship3dBBeamWidth","unsigned short",          0.1, 0.0},
			{"BeamAthwartship3dBBeamWidth","unsigned short",        0.1, 0.0},
			{"BeamEquivalentTwoWayBeamAngle","short",          		0.01, 0.0},
			{"CalibrationSourceLevel","unsigned short",          	0.01, 0.0},
			{"CalibrationReceivingSensitivity","short",          	0.01, 0.0},
			{"SLPlusVR","short",          							0.01, 0.0},
			{"BottomDetectionMinimumLevel","short",          		1.0, 0.0},
			{"BottomDetectionMinimumDepth","unsigned int",          0.01, 0.0},
			{"BottomDetectionMaximumDepth","unsigned int",          0.01, 0.0},
			{"Remarks","char",          							1.0, 0.0},
			{"TupleAttribute","int",          						1.0, 0.0},
			{"BackLink","unsigned int",          					1.0, 0.0},
			{"IndexTupleParent","unsigned int",          			1.0, 0.0}};

	T_DESC_SIGNAL_OR_IMAGE strDescriptionImage[NB_IMAGE] = {};

	T_DESC_SIGNAL_OR_IMAGE strDescriptionImage2[NB_IMAGE2] = {};

	T_DESC_TAB_SIGANDIMG strDescriptionTab = {	NB_SIGNAL,
												NB_IMAGE,
												NB_IMAGE2,
												NUM_INDEX_TABLE,
												strDescriptionSignal,
												strDescriptionImage,
												strDescriptionImage2};


	// Lancement de l'initialisation des structures de signaux : ouvertures des
	// fichiers, copie des structures, ...
	iRet = utilHAC_initXMLRecord(	uiSounderID,
									strDescriptionTab,
									cTitleDatagram,
									cNomDatagram,
									cComments,
									strHACRecordXML);


	int iVarSignal = 40;
	strHACRecordXML->strTabSignalXML[iVarSignal].usNbElem = 40;

	return iRet;

} // iInitXMLBin9001Record

#pragma pack()
