/*
 * HAC_process9001Record.h
 *
 *  Created on: 23 mai 2011
 *      Author: rgallou
 */
#pragma pack(1)

#ifndef HAC_PROCESS9001RECORD_H_
#define HAC_PROCESS9001RECORD_H_

#include "HAC_Tuples.h"


typedef struct  {
	unsigned int	TupleSize;
	unsigned short	TupleType;
	unsigned short	SoftwareChannelIdentifier;
	unsigned int	EchosounderDocIdentifier;
	unsigned int	SamplingRate;
	unsigned int	SamplingInterval;
	unsigned int	AcousticFrequency;
	unsigned short	TransceiverChannelNumber;
	unsigned short	TypeOfData;
	unsigned short	TimeVariedGainMultiplier;
	unsigned short	TVGBlankingMode;
	unsigned short	TVGMinimumRange;
	unsigned short	TVGMaximumRange;
	unsigned int	BlankingUpToRange;
	unsigned int	SampleRange;
	unsigned int	TransducerInstallationDepth;
	unsigned short	PlatformIdentifier;
	unsigned short	Space;
	unsigned int	AlongshipOffsetRelToAttSensor;
	unsigned int	AthwartshipOffsetRelToAttSensor;
	unsigned int	VerticalOffsetRelToAttSensor;
	short			AlongshipAngleOffsetOfTransducer;
	short			AthwartshipAngleOffsetOfTransducer;
	short			RotationAngleOffsetOfTransducer;
	short			AlongshipAngleOffsetToMainAxisOfAcBeam;
	short			AthwartshipAngleOffsetToMainAxisOfAcBeam;
	unsigned short	AbsorptionOfSound;
	unsigned int	PulseDuration;
	unsigned short	PulseShapeMode;
	unsigned short	BandWidth;
	unsigned short	TransducerShapeMode;
	unsigned short	BeamAlongship3dBBeamWidth;
	unsigned short	BeamAthwartship3dBBeamWidth;
	short			BeamEquivalentTwoWayBeamAngle;
	unsigned short	CalibrationSourceLevel;
	short			CalibrationReceivingSensitivity;
	short			SLPlusVR;
	short			BottomDetectionMinimumLevel;
	unsigned int	BottomDetectionMinimumDepth;
	unsigned int	BottomDetectionMaximumDepth;
	char           	Remarks[40];
	int				TupleAttribute;
	unsigned int	BackLink;
	unsigned int	IndexTupleParent;
} T_TUPLE9001_SIGNAL;


int iProcess9001Record(FILE *fpFileData,
						int iNbRecord,
						unsigned int uiEchoSounderId,
						unsigned int uiIndexTupleParent,
						T_HACRECORDXML *strHACRecordXML,
						T_TUPLE9001_SIGNAL *ptrSignal_prevRecord);


int iInitXMLBin9001Record(	unsigned int uiSounderID,
							char *cTitleDatagram,
							char *cNomDatagram,
							char *cComments,
							T_HACRECORDXML *strHACRecordXML);

#endif /* HAC_PROCESS9001RECORD_H_ */

#pragma pack()
