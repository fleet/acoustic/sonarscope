/*
 * HAC_processFileIndexRecord.h
 *
 *  Created on: 23 mai 2011
 *      Author: rgallou
 */
#pragma pack(1)

#ifndef HAC_PROCESSFILEINDEXRECORD_H_
#define HAC_PROCESSFILEINDEXRECORD_H_

#include "HAC_Tuples.H"
#include <sys/types.h>


typedef struct  {
		unsigned int	TupleSize;
		unsigned short	TupleType;
		off64_t 		DatagramPosition;
} T_FILEINDEX_RECORD_SIGNAL;


int iProcessFileIndexRecord(FILE *fpFileData,
							int iNbRecord,
							T_HACRECORDXML *strHACRecordXML,
							T_FILEINDEX_RECORD_SIGNAL *ptrSignal_prevRecord);


int iInitXMLBinFileIndexRecord(	unsigned int uiSounderID,
								char *cTitleDatagram,
								char *cNomDatagram,
								char *cComments,
								T_HACRECORDXML *strHACRecordXML);

#endif /* HAC_PROCESSFILEINDEXRECORD_H_ */

#pragma pack()
