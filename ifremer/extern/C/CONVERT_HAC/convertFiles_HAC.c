// ----------------------------------------------------------------------
// Programme :
//	CONVERT_HAC.C
//
// Objet :
//	Convertir un fichier au format HAC pour d�composer en fichiers
//	XML et fichiers binaires. Les fichiers XML decrivent le contenu des
// 	binaires.
// 	Chaque fichier binaire comporte une donn�e �l�mentaire des paquets par ping
// 	issue des paquets de donn�es du HAC.
//	Il a ete realise sous l'IDE Eclipse Galileo avec le compilateur gcc 3.4.1.
//
// Cr�ation :
//	12/11/2011
//
// Auteur :
//  GLU : (ATL) roger.gallou@altran.com
//		  02 98 05 43 21
//
// Historique des modifications :
// - v.Main
//
// ----------------------------------------------------------------------
#pragma pack(1) // Pour la compilation sous Visual Studio
				// et l'alignement sur 8 octets.


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <errno.h>
#include <libgen.h>

#include "HAC_utilities.h"
#include "HAC_Tuples.h"
#include "convertFiles_HAC.h"
#include "HAC_Tuples.h"
#include "HAC_process20Record.h"
#include "HAC_process30Record.h"
#include "HAC_process41Record.h"
#include "HAC_process65535Record.h"
#include "HAC_process65534Record.h"
#include "HAC_process210Record.h"
#include "HAC_process220Record.h"
#include "HAC_process2100Record.h"
#include "HAC_process2200Record.h"
#include "HAC_process901Record.h"
#include "HAC_process9001Record.h"
#include "HAC_process10011Record.h"
#include "HAC_process10030Record.h"
#include "HAC_process10031Record.h"
#include "HAC_process10040Record.h"
#include "HAC_process10100Record.h"
#include "HAC_process10140Record.h"
#include "HAC_process11000Record.h"
#include "HAC_processFileIndexRecord.h"

#include "Generic_Utilities.h"    // Contenu dans la librairie libCONVERT_utils.a


// Fonction de d�claration de la fonction principale de conversion.
// Param�tres :
// - cFileName : r�pertoire et nom du fichier de donn�es
// - barre de progression optionnelle dans l'appel de fonction.
int iConvertFiles_HAC(const char *cFileName, void(*progress)(int))
{
	int 	iRet = 0,
			iLoop,
			iFlagFindTuple = FALSE,
			iNbDatagramNotTranslate = 0,
			iIndexTupleRoot,
			iIndexTuple,
			iIndexTupleRootDummy,
			iIndexTupleDummy,
			iTabEchoSounderId[NBMAXSOUNDER];

	// Adressage d'un fichier potentiellement sup. � 2 Go.
	off64_t 	llPosFic,
				llNbOcFic,
				llPositionDebDatagram = 0;

	unsigned int 	uiStartCode,
					uiEchoSounderId,
					uiTmpId,
					uiTimeCPU,
					uiTupleSize,
					uiNbEchoSounder = 0;

	unsigned short	usSoftwareChannelId,
					usSensorId,
					usNbSoftwareChannels,
					usTupleType,
					usNbCoupleChannelAndSounderId = 0,
					usTimeFraction;

	float			fProgressRate;
	FILE *fpFileData;

	// Description des tuples rencontr�es non traduits.
	T_TYPEOFTUPLESNOTTRANSLATED tabStrTupleNotTranslate[30];

	// Description de l'Arborescence des Tuples.
	T_DESC_TAB_SOUNDERANDCHANNELID 		strSounderAndChannelId;
	T_DESC_TAB_CHANNELSENSORID 			strChannelSensorId;


	T_TUPLE20_SIGNAL 					strSignalTuple20;
	T_TUPLE30_SIGNAL 					strSignalTuple30;
	T_TUPLE41_SIGNAL 					strSignalTuple41[NBMAXSOUNDER];
	T_TUPLE65535_SIGNAL 				strSignalTuple65535;
	T_TUPLE65534_SIGNAL 				strSignalTuple65534;
	T_TUPLE210_SIGNAL 					strSignalTuple210[NBMAXSOUNDER];
	T_TUPLE220_SIGNAL 					strSignalTuple220[NBMAXSOUNDER];
	T_TUPLE2100_SIGNAL 					strSignalTuple2100[NBMAXSOUNDER];
	T_TUPLE2200_SIGNAL 					strSignalTuple2200[NBMAXSOUNDER];
	T_TUPLE901_SIGNAL 					strSignalTuple901[NBMAXSOUNDER];
	T_TUPLE9001_SIGNAL 					strSignalTuple9001[NBMAXSOUNDER];
	T_TUPLE10011_SIGNAL 				strSignalTuple10011[NBMAXSOUNDER];
	T_TUPLE10030_SIGNAL 				strSignalTuple10030[NBMAXSOUNDER];
	T_TUPLE10031_SIGNAL 				strSignalTuple10031[NBMAXSOUNDER];
	T_TUPLE10040_SIGNAL 				strSignalTuple10040[NBMAXSOUNDER];
	T_TUPLE10100_SIGNAL 				strSignalTuple10100[NBMAXSOUNDER];
	T_TUPLE10140_SIGNAL 				strSignalTuple10140[NBMAXSOUNDER];
	T_TUPLE11000_SIGNAL 				strSignalTuple11000;
	T_FILEINDEX_RECORD_SIGNAL			strSignalFileIndexRecord;


	T_HACRECORDXML				strTuple20XML;
	T_HACRECORDXML				strTuple30XML;
	T_HACRECORDXML				strTuple41XML[NBMAXSOUNDER];
	T_HACRECORDXML				strTuple210XML[NBMAXSOUNDER];
	T_HACRECORDXML				strTuple220XML[NBMAXSOUNDER];
	T_HACRECORDXML				strTuple2100XML[NBMAXSOUNDER];
	T_HACRECORDXML				strTuple2200XML[NBMAXSOUNDER];
	T_HACRECORDXML				strTuple901XML[NBMAXSOUNDER];
	T_HACRECORDXML				strTuple9001XML[NBMAXSOUNDER];
	T_HACRECORDXML				strTuple10011XML[NBMAXSOUNDER];
	T_HACRECORDXML				strTuple10030XML[NBMAXSOUNDER];
	T_HACRECORDXML				strTuple10031XML[NBMAXSOUNDER];
	T_HACRECORDXML				strTuple10040XML[NBMAXSOUNDER];
	T_HACRECORDXML				strTuple10100XML[NBMAXSOUNDER];
	T_HACRECORDXML				strTuple10140XML[NBMAXSOUNDER];
	T_HACRECORDXML				strTuple65535XML;
	T_HACRECORDXML				strTuple65534XML;
	T_HACRECORDXML				strTuple11000XML;
	T_HACRECORDXML 				strFileIndexRecordXML;

	// Initialisation du nb d'�chantillons.
	strSounderAndChannelId.usNbSoftwareChannelId = 0;

	// Initialisations des compteurs d'occurences des tuples.
	strTuple20XML.iNbDatagram 			= 0;
	strTuple30XML.iNbDatagram 			= 0;
	for (iLoop=0; iLoop< NBMAXSOUNDER; iLoop++)
	{
		iTabEchoSounderId[iLoop] = -1;
		strTuple41XML[iLoop].iNbDatagram 		= 0;
		strTuple210XML[iLoop].iNbDatagram 		= 0;
		strTuple220XML[iLoop].iNbDatagram 		= 0;
		strTuple2100XML[iLoop].iNbDatagram 		= 0;
		strTuple2200XML[iLoop].iNbDatagram 		= 0;
		strTuple901XML[iLoop].iNbDatagram 		= 0;
		strTuple9001XML[iLoop].iNbDatagram 		= 0;
		strTuple10040XML[iLoop].iNbDatagram 	= 0;
		strTuple10100XML[iLoop].iNbDatagram 	= 0;
		strTuple10011XML[iLoop].iNbDatagram 	= 0;
		strTuple10030XML[iLoop].iNbDatagram 	= 0;
		strTuple10031XML[iLoop].iNbDatagram 	= 0;
		strTuple10140XML[iLoop].iNbDatagram 	= 0;
	}
	strTuple11000XML.iNbDatagram 		= 0;
	strTuple65535XML.iNbDatagram 		= 0;
	strTuple65534XML.iNbDatagram 		= 0;
	strFileIndexRecordXML.iNbDatagram 	= 0;


	// Initialisation des datagrammes d�j� trouv�s.
	for (iLoop=0; iLoop<30; iLoop++)
	{
		tabStrTupleNotTranslate[iLoop].uiType = 0;
		tabStrTupleNotTranslate[iLoop].iNbTimes = 0;
	}

	// Ouverture du fichier All
	fpFileData = fopen64(cFileName,"rb");
	if(fpFileData  == NULL)
	{
		printf("\nProblem in access Data file : %s\n", cFileName);
		goto ERROR_OUT;

	}

    // Calcul de la taille du fichier.
	fseeko64(fpFileData, 0, SEEK_END); //On se positionne � la fin du fichier
    llNbOcFic=ftello64(fpFileData);
    fseeko64(fpFileData, 0, SEEK_SET);


	// Lecture du StartCode du fichier (exploit� dans le test
    // de d�tection Little et Big Endian).
	iRet = fread(&uiStartCode, sizeof(uiStartCode), 1, fpFileData);
    llPositionDebDatagram = ftello64(fpFileData);

    // For�age de la sortie des impressions : stdout iunbuffered
    setvbuf (stdout, NULL, _IONBF, 0);

    //Deroulement entier du fichier .HAC
	while(!feof(fpFileData) && llPositionDebDatagram < llNbOcFic)
	{
		// Pour sortir brutalement du processus .
//		if (kbhit()) if (getch() == 27)
//		{
//			printf("\nUser pressed ESC\n");
//			return -1; // press ESC to quit.
//		}


		llPosFic = ftello64(fpFileData);

	    // Affichage de la progression dans la fen�tre d'ex�cution
		#ifdef GUI
	    	if(progress)
	    		progress(100*(double)llPosFic/(double)llNbOcFic);
		#else
	    	util_vProgressBar(0, llPosFic, llNbOcFic);
		#endif
		fseeko64(fpFileData,llPositionDebDatagram,SEEK_SET);


		// Lecture du paquet de donn�es commun aux datagrammes.
		iRet = iProcessFileIndexRecord(fpFileData,
								strFileIndexRecordXML.iNbDatagram,
								&strFileIndexRecordXML,
								&strSignalFileIndexRecord);

		//printf("Process datagram(iLoop:%d) et strSignalFileIndexRecord.TupleType: %d\n",	iLoop, strSignalFileIndexRecord.TupleType);

		// Sortie provoqu�e en l'�tat de l'analyse d�j� effectu�e.
		// Pb rencontr� sur le fichier PELGAS11_038_20110602_022051.hac (fin de fichier = 0)
		if (strSignalFileIndexRecord.TupleType == 0)
		{
			fProgressRate = 1 - (float)((llNbOcFic - llPosFic) / (float)llNbOcFic);
			printf("Premature exit from the file analysis (%05.2f) %% : problems encountered in identifying the type of tuple or wrong file.\n",	fProgressRate*100);
			break;
		}
		strFileIndexRecordXML.iNbDatagram++;
		// On revient en arri�re de la lecture du paquet commun
		// car celui-ci est incorpor� � chaque paquet.
		llPosFic = ftello64(fpFileData);
		// Datagramme Position n'est pas lu mais juste int�gr�e �
		// la structure T_FILEINDEX_RECORD_SIGNAL.
		llPosFic = llPosFic - (sizeof(T_FILEINDEX_RECORD_SIGNAL)-8);
		fseeko64(fpFileData,llPosFic,SEEK_SET);
		//llPosFic = ftello64(fpFileData);


	    // Traitement au cas par cas des datagrammes.
		if(strSignalFileIndexRecord.TupleType == 65535)
		{
 		    iRet = iProcess65535Record(fpFileData,
									strTuple65535XML.iNbDatagram,
									&strTuple65535XML,
									&strSignalTuple65535);
		    strTuple65535XML.iNbDatagram++;
		}
		else if(strSignalFileIndexRecord.TupleType == 20)
		{
 		    iRet = iProcess20Record(fpFileData,
									strTuple20XML.iNbDatagram,
									&strTuple20XML,
									&strSignalTuple20);
		    strTuple20XML.iNbDatagram++;
		}
		else if(strSignalFileIndexRecord.TupleType == 30)
		{
 		    iRet = iProcess30Record(fpFileData,
									strTuple30XML.iNbDatagram,
									&strTuple30XML,
									&strSignalTuple30);
		    strTuple30XML.iNbDatagram++;
		}
		else if(strSignalFileIndexRecord.TupleType == 41)
		{
		    // On ne commence � traiter les �chos que si un nombre de faisceaux
			// a �t� pr�lablement identifi�.
			// Les tuples type 41 sont �crits successivement autant de fois qu'il y a de faisceaux.
			// Ils sont trait�s comme des signaux.
			if (strSounderAndChannelId.usNbSoftwareChannelId > 0)
			{
				iRet = utilHAC_identSensor(fpFileData,
												llPosFic,
												&usSoftwareChannelId,
												&usSensorId);


				// Conservation de l'index du tuple Racine (N-2)..
				iIndexTupleRoot 	= 0;
				// iIndexTuple d�signe le Tuple courant qui est incr�ment� plus loin.
				iIndexTuple			= 0;
				// Conservation des Id courants de SoftwareChannel, de Sondeurs,
				// du Tuple Racine et du Tuple courant.
				iRet = utilHAC_saveSounderChannelId(&iIndexTupleRoot,
												&iIndexTuple,
												&usSoftwareChannelId,
												&uiEchoSounderId,
												&strSounderAndChannelId);

				// Conservation de l'index du tuple Racine (N-2)..
				iIndexTupleRoot 	= 0;
				// iIndexTuple d�signe le Tuple courant qui est incr�ment� plus loin.
				iIndexTuple			= strTuple41XML[uiEchoSounderId-1].iNbDatagram + 1;
				// Conservation des Id courants de SoftwareChannel, de Sensor du Tuple Racine et du Tuple courant.
				iRet = utilHAC_saveChannelSensorId(&iIndexTupleRoot,
												&iIndexTuple,
												&usSensorId,
												&usSoftwareChannelId,
												&strChannelSensorId);



				// On ne commence � traiter les �chos que si un nombre de faisceaux
				// a �t� pr�lablement identifi�.
				// Les tuples type 41 sont �crits successivement autant de fois qu'il y a de faisceaux.
				// Ils sont trait�s comme des signaux.
				iRet = iProcess41Record(fpFileData,
									strTuple41XML[uiEchoSounderId-1].iNbDatagram,
									iIndexTupleRoot,
									iIndexTuple,
									uiEchoSounderId,
									&strTuple41XML[uiEchoSounderId-1],
									&strSignalTuple41[uiEchoSounderId-1]);
				strTuple41XML[uiEchoSounderId-1].iNbDatagram++;
			}

		}
		else if(strSignalFileIndexRecord.TupleType == 210)
		{
			// Conservation de l'index du tuple Racine (N-2)..
			iRet = utilHAC_readEchoSounder(fpFileData,
											llPosFic,
											&usNbSoftwareChannels,
											&uiTmpId);

			iRet = utilHAC_identEchoSounder(uiTmpId,
											&uiNbEchoSounder,
											iTabEchoSounderId,
											&uiEchoSounderId);

			iRet = iProcess210Record(fpFileData,
									strTuple210XML[uiEchoSounderId-1].iNbDatagram,
									uiEchoSounderId,
									&strTuple210XML[uiEchoSounderId-1],
									&strSignalTuple210[uiEchoSounderId-1]);
		    strTuple210XML[uiEchoSounderId-1].iNbDatagram++;
		}
		else if(strSignalFileIndexRecord.TupleType == 220)
		{
			// Conservation de l'index du tuple Racine (N-2)..
			iRet = utilHAC_readEchoSounder(fpFileData,
											llPosFic,
											&usNbSoftwareChannels,
											&uiTmpId);

			iRet = utilHAC_identEchoSounder(uiTmpId,
											&uiNbEchoSounder,
											iTabEchoSounderId,
											&uiEchoSounderId);

			iRet = iProcess220Record(fpFileData,
									strTuple220XML[uiEchoSounderId-1].iNbDatagram,
									uiEchoSounderId,
									&strTuple220XML[uiEchoSounderId-1],
									&strSignalTuple220[uiEchoSounderId-1]);
		    strTuple220XML[uiEchoSounderId-1].iNbDatagram++;
		}
		else if(strSignalFileIndexRecord.TupleType == 2100)
		{
			iRet = utilHAC_readEchoSounder(fpFileData,
											llPosFic,
											&usSoftwareChannelId,
											&uiTmpId);

			iRet = utilHAC_identEchoSounder(uiTmpId,
											&uiNbEchoSounder,
											iTabEchoSounderId,
											&uiEchoSounderId);

			// Conservation de l'index du tuple Racine (N-2)..
			iIndexTupleRoot 	= strTuple210XML[uiEchoSounderId-1].iNbDatagram;
			// iIndexTuple d�signe le Tuple courant qui est incr�ment� plus loin.
			iIndexTuple			= strTuple2100XML[uiEchoSounderId-1].iNbDatagram + 1;

			// Conservation des Id courants de SoftwareChannel, de Sondeurs,
			// du Tuple Racine et du Tuple courant.
			iRet = utilHAC_saveSounderChannelId(&iIndexTupleRoot,
											&iIndexTuple,
											&usSoftwareChannelId,
											&uiEchoSounderId,
											&strSounderAndChannelId);


			// On ne commence � traiter les �chos que si un nombre de faisceaux
			// a �t� pr�lablement identifi�.
			// Les tuples type 2200 sont �crits successivement autant de fois qu'il y a de faisceaux.
			// Ils sont trait�s comme des signaux.
			if (strSounderAndChannelId.usNbSoftwareChannelId > 0)
 		    {
				iRet = iProcess2100Record(fpFileData,
										strTuple2100XML[uiEchoSounderId-1].iNbDatagram,
 										uiEchoSounderId,
 										strTuple210XML[uiEchoSounderId-1].iNbDatagram,
										&strTuple2100XML[uiEchoSounderId-1],
										&strSignalTuple2100[uiEchoSounderId-1]);
				strTuple2100XML[uiEchoSounderId-1].iNbDatagram++;
			}
		}
		else if(strSignalFileIndexRecord.TupleType == 2200)
		{
			iRet = utilHAC_readEchoSounder(fpFileData,
											llPosFic,
											&usSoftwareChannelId,
											&uiTmpId);

			iRet = utilHAC_identEchoSounder(uiTmpId,
											&uiNbEchoSounder,
											iTabEchoSounderId,
											&uiEchoSounderId);

			// Conservation de l'index du tuple Racine (N-2)..
			iIndexTupleRoot 	= strTuple220XML[uiEchoSounderId-1].iNbDatagram;
			// iIndexTuple d�signe le Tuple courant qui est incr�ment� plus loin.
			iIndexTuple			= strTuple2200XML[uiEchoSounderId-1].iNbDatagram + 1;


			// Conservation des Id courants de SoftwareChannel, de Sondeurs,
			// du Tuple Racine et du Tuple courant.
			iRet = utilHAC_saveSounderChannelId(&iIndexTupleRoot,
											&iIndexTuple,
											&usSoftwareChannelId,
											&uiEchoSounderId,
											&strSounderAndChannelId);

			// On ne commence � traiter les �chos que si un nombre de faisceaux
			// a �t� pr�lablement identifi�.
			// Les tuples type 2200 sont �crits successivement autant de fois qu'il y a de faisceaux.
			// Ils sont trait�s comme des signaux.
			if (strSounderAndChannelId.usNbSoftwareChannelId > 0)
 		    {
 	 		    iRet = iProcess2200Record(fpFileData,
 										strTuple2200XML[uiEchoSounderId-1].iNbDatagram,
 										uiEchoSounderId,
 										strTuple220XML[uiEchoSounderId-1].iNbDatagram,
 										&strTuple2200XML[uiEchoSounderId-1],
 										&strSignalTuple2200[uiEchoSounderId-1]);
 			    strTuple2200XML[uiEchoSounderId-1].iNbDatagram++;
 		    }
		}
		else if(strSignalFileIndexRecord.TupleType == 901)
		{
			// Conservation de l'index du tuple Racine (N-2)..
			iRet = utilHAC_readEchoSounder(fpFileData,
											llPosFic,
											&usNbSoftwareChannels,
											&uiTmpId);

			iRet = utilHAC_identEchoSounder(uiTmpId,
											&uiNbEchoSounder,
											iTabEchoSounderId,
											&uiEchoSounderId);

			iRet = iProcess901Record(fpFileData,
									strTuple901XML[uiEchoSounderId-1].iNbDatagram,
									uiEchoSounderId,
									&strTuple901XML[uiEchoSounderId-1],
									&strSignalTuple901[uiEchoSounderId-1]);
			strTuple901XML[uiEchoSounderId-1].iNbDatagram++;
		}
		else if(strSignalFileIndexRecord.TupleType == 9001)
		{
			iRet = utilHAC_readEchoSounder(fpFileData,
											llPosFic,
											&usSoftwareChannelId,
											&uiTmpId);

			iRet = utilHAC_identEchoSounder(uiTmpId,
											&uiNbEchoSounder,
											iTabEchoSounderId,
											&uiEchoSounderId);

			// Conservation de l'index du tuple Racine (N-2)..
			iIndexTupleRoot 	= strTuple901XML[uiEchoSounderId-1].iNbDatagram;
			// iIndexTuple d�signe le Tuple courant qui est incr�ment� plus loin.
			iIndexTuple			= strTuple9001XML[uiEchoSounderId-1].iNbDatagram + 1;

			// Conservation des Id courants de SoftwareChannel, de Sondeurs,
			// du Tuple Racine et du Tuple courant.
			iRet = utilHAC_saveSounderChannelId(&iIndexTupleRoot,
											&iIndexTuple,
											&usSoftwareChannelId,
											&uiEchoSounderId,
											&strSounderAndChannelId);

 		    // On ne commence � traiter les �chos que si un nombre de faisceaux
			// a �t� pr�lablement identifi�.
			// Les tuples type 9001 sont �crits successivement autant de fois qu'il y a de faisceaux.
			// Ils sont trait�s comme des signaux.
			if (strSounderAndChannelId.usNbSoftwareChannelId > 0)
 		    {
 	 		    iRet = iProcess9001Record(fpFileData,
 										strTuple9001XML[uiEchoSounderId-1].iNbDatagram,
 										uiEchoSounderId,
 										strTuple901XML[uiEchoSounderId-1].iNbDatagram,
 										&strTuple9001XML[uiEchoSounderId-1],
 										&strSignalTuple9001[uiEchoSounderId-1]);
 			    strTuple9001XML[uiEchoSounderId-1].iNbDatagram++;
 		    }
		}
		else if(strSignalFileIndexRecord.TupleType == 10011)
		{
			// On ne commence � traiter les �chos que si un nombre de faisceaux
			// a �t� pr�lablement identifi�.
			// Les tuples type 10011 sont �crits successivement autant de fois qu'il y a de faisceaux.
			// Ils sont trait�s comme des signaux.
			if (strSounderAndChannelId.usNbSoftwareChannelId > 0)
			{
				// Lecture des valeurs d'identification du Software Channel Id et Echo Sounder Id.
				iRet = fread(&uiTupleSize, sizeof(unsigned int), 1, fpFileData);
				iRet = fread(&usTupleType, sizeof(unsigned short), 1, fpFileData);
				iRet = fread(&usTimeFraction, sizeof(unsigned short), 1, fpFileData);
				iRet = fread(&uiTimeCPU, sizeof(unsigned int), 1, fpFileData);
				iRet = fread(&usSoftwareChannelId, sizeof(unsigned short), 1, fpFileData);
				// Repositionnement en d�but de tuple.
				fseeko64(fpFileData,llPosFic,SEEK_SET);

				iIndexTupleRoot = 0;
				iIndexTuple = 0;
				// Conservation des Id courants de SoftwareChannel, de Sondeurs,
				// du Tuple Racine et du Tuple courant.
				iRet = utilHAC_saveSounderChannelId(&iIndexTupleRoot,
												&iIndexTuple,
												&usSoftwareChannelId,
												&uiEchoSounderId,
												&strSounderAndChannelId);

				iRet = iProcess10011Record(fpFileData,
 										strTuple10011XML[uiEchoSounderId-1].iNbDatagram,
										iIndexTupleRoot,
										iIndexTuple,
										uiEchoSounderId,
 										&strTuple10011XML[uiEchoSounderId-1],
 										&strSignalTuple10011[uiEchoSounderId-1]);
 			    strTuple10011XML[uiEchoSounderId-1].iNbDatagram++;
			}
		}
		else if(strSignalFileIndexRecord.TupleType == 10030)
		{

		    // On ne commence � traiter les �chos que si un nombre de faisceaux
			// a �t� pr�lablement identifi�.
			// Les tuples type 10030 sont �crits successivement autant de fois qu'il y a de faisceaux.
			// Ils sont trait�s comme des signaux.
			if (strSounderAndChannelId.usNbSoftwareChannelId > 0)
			{
				// Lecture des valeurs d'identification du Software Channel Id et Echo Sounder Id.
				iRet = fread(&uiTupleSize, sizeof(unsigned int), 1, fpFileData);
				iRet = fread(&usTupleType, sizeof(unsigned short), 1, fpFileData);
				iRet = fread(&usTimeFraction, sizeof(unsigned short), 1, fpFileData);
				iRet = fread(&uiTimeCPU, sizeof(unsigned int), 1, fpFileData);
				iRet = fread(&usSoftwareChannelId, sizeof(unsigned short), 1, fpFileData);
				// Repositionnement en d�but de tuple.
				fseeko64(fpFileData,llPosFic,SEEK_SET);

				iIndexTupleRoot = 0;
				iIndexTuple = 0;
				// Conservation des Id courants de SoftwareChannel, de Sondeurs,
				// du Tuple Racine et du Tuple courant.
				iRet = utilHAC_saveSounderChannelId(&iIndexTupleRoot,
												&iIndexTuple,
												&usSoftwareChannelId,
												&uiEchoSounderId,
												&strSounderAndChannelId);

				iRet = iProcess10030Record(fpFileData,
 										strTuple10030XML[uiEchoSounderId-1].iNbDatagram,
										iIndexTupleRoot,
										iIndexTuple,
										uiEchoSounderId,
 										&strTuple10030XML[uiEchoSounderId-1],
 										&strSignalTuple10030[uiEchoSounderId-1]);
 				strTuple10030XML[uiEchoSounderId-1].iNbDatagram++;
			}
		}
		else if(strSignalFileIndexRecord.TupleType == 10031)
		{

		    // On ne commence � traiter les �chos que si un nombre de faisceaux
			// a �t� pr�lablement identifi�.
			// Les tuples type 10031 sont �crits successivement autant de fois qu'il y a de faisceaux.
			// Ils sont trait�s comme des signaux.
			if (strSounderAndChannelId.usNbSoftwareChannelId > 0)
			{
				// Lecture des valeurs d'identification du Software Channel Id et Echo Sounder Id.
				iRet = fread(&uiTupleSize, sizeof(unsigned int), 1, fpFileData);
				iRet = fread(&usTupleType, sizeof(unsigned short), 1, fpFileData);
				iRet = fread(&usTimeFraction, sizeof(unsigned short), 1, fpFileData);
				iRet = fread(&uiTimeCPU, sizeof(unsigned int), 1, fpFileData);
				iRet = fread(&usSoftwareChannelId, sizeof(unsigned short), 1, fpFileData);
				// Repositionnement en d�but de tuple.
				fseeko64(fpFileData,llPosFic,SEEK_SET);

				iIndexTupleRoot = 0;
				iIndexTuple = 0;
				// Conservation des Id courants de SoftwareChannel, de Sondeurs,
				// du Tuple Racine et du Tuple courant.
				iRet = utilHAC_saveSounderChannelId(&iIndexTupleRoot,
												&iIndexTuple,
												&usSoftwareChannelId,
												&uiEchoSounderId,
												&strSounderAndChannelId);

				iRet = iProcess10031Record(fpFileData,
 										strTuple10031XML[uiEchoSounderId-1].iNbDatagram,
										iIndexTupleRoot,
										iIndexTuple,
										uiEchoSounderId,
 										&strTuple10031XML[uiEchoSounderId-1],
 										&strSignalTuple10031[uiEchoSounderId-1]);
 				strTuple10031XML[uiEchoSounderId-1].iNbDatagram++;
			}
		}
		else if(strSignalFileIndexRecord.TupleType == 10040)
		{

		    // On ne commence � traiter les �chos que si un nombre de faisceaux
			// a �t� pr�lablement identifi�.
			// Les tuples type 10040 sont �crits successivement autant de fois qu'il y a de faisceaux.
			// Ils sont trait�s comme des signaux.
			if (strSounderAndChannelId.usNbSoftwareChannelId > 0)
			{
				// Lecture des valeurs d'identification du Software Channel Id et Echo Sounder Id.
				iRet = fread(&uiTupleSize, sizeof(unsigned int), 1, fpFileData);
				iRet = fread(&usTupleType, sizeof(unsigned short), 1, fpFileData);
				iRet = fread(&usTimeFraction, sizeof(unsigned short), 1, fpFileData);
				iRet = fread(&uiTimeCPU, sizeof(unsigned int), 1, fpFileData);
				iRet = fread(&usSoftwareChannelId, sizeof(unsigned short), 1, fpFileData);
				// Repositionnement en d�but de tuple.
				fseeko64(fpFileData,llPosFic,SEEK_SET);

				iIndexTupleRoot = 0;
				iIndexTuple = 0;
				// Conservation des Id courants de SoftwareChannel, de Sondeurs,
				// du Tuple Racine et du Tuple courant.
				iRet = utilHAC_saveSounderChannelId(&iIndexTupleRoot,
												&iIndexTuple,
												&usSoftwareChannelId,
												&uiEchoSounderId,
												&strSounderAndChannelId);

				iRet = iProcess10040Record(fpFileData,
 										strTuple10040XML[uiEchoSounderId-1].iNbDatagram,
										iIndexTupleRoot,
										iIndexTuple,
										uiEchoSounderId,
 										&strTuple10040XML[uiEchoSounderId-1],
 										&strSignalTuple10040[uiEchoSounderId-1]);
 			    strTuple10040XML[uiEchoSounderId-1].iNbDatagram++;
			}
		}
		else if(strSignalFileIndexRecord.TupleType == 10100)
		{
		    // On ne commence � traiter les �chos que si un nombre de faisceaux
			// a �t� pr�lablement identifi�.
			// Les tuples type 10040 sont �crits successivement autant de fois qu'il y a de faisceaux.
			// Ils sont trait�s comme des signaux.
			if (strSounderAndChannelId.usNbSoftwareChannelId > 0)
			{
				// Lecture des valeurs d'identification du Software Channel Id et Echo Sounder Id.
				iRet = fread(&uiTupleSize, sizeof(unsigned int), 1, fpFileData);
				iRet = fread(&usTupleType, sizeof(unsigned short), 1, fpFileData);
				iRet = fread(&usTimeFraction, sizeof(unsigned short), 1, fpFileData);
				iRet = fread(&uiTimeCPU, sizeof(unsigned int), 1, fpFileData);
				iRet = fread(&usSoftwareChannelId, sizeof(unsigned short), 1, fpFileData);
				// Repositionnement en d�but de tuple.
				fseeko64(fpFileData,llPosFic,SEEK_SET);

				iIndexTupleRoot = 0;
				iIndexTuple = 0;
				// Conservation des Id courants de SoftwareChannel, de Sondeurs,
				// du Tuple Racine et du Tuple courant.
				iRet = utilHAC_saveSounderChannelId(&iIndexTupleRoot,
												&iIndexTuple,
												&usSoftwareChannelId,
												&uiEchoSounderId,
												&strSounderAndChannelId);

				iRet = iProcess10100Record(fpFileData,
										strTuple10100XML[uiEchoSounderId-1].iNbDatagram,
										iIndexTupleRoot,
										iIndexTuple,
										uiEchoSounderId,
										&strTuple10100XML[uiEchoSounderId-1],
										&strSignalTuple10100[uiEchoSounderId-1]);
				strTuple10100XML[uiEchoSounderId-1].iNbDatagram++;
			}
		}
		else if(strSignalFileIndexRecord.TupleType == 10140)
		{
		    // On ne commence � traiter les �chos que si un nombre de faisceaux
			// a �t� pr�lablement identifi�.
			// Les tuples type 10140 sont �crits successivement autant de fois qu'il y a de faisceaux.
			// Ils sont trait�s comme des signaux.
			if (strSounderAndChannelId.usNbSoftwareChannelId > 0)
			{
				// Lecture des valeurs d'identification du Software Channel Id et Echo Sounder Id.
				iRet = fread(&uiTupleSize, sizeof(unsigned int), 1, fpFileData);
				iRet = fread(&usTupleType, sizeof(unsigned short), 1, fpFileData);
				iRet = fread(&usTimeFraction, sizeof(unsigned short), 1, fpFileData);
				iRet = fread(&uiTimeCPU, sizeof(unsigned int), 1, fpFileData);
				iRet = fread(&usSensorId, sizeof(unsigned short), 1, fpFileData);

				// Repositionnement en d�but de tuple.
				fseeko64(fpFileData,llPosFic,SEEK_SET);

				// Conservation de l'index du tuple Racine (N-2)..
				iIndexTupleRoot 	= 0;
				// iIndexTuple d�signe le Tuple courant qui est incr�ment� plus loin.
				iIndexTuple		= 0;
				// Conservation des Id courants de SoftwareChannel, de Sensor
				// du Tuple Racine et du Tuple courant.
				iRet = utilHAC_saveChannelSensorId(&iIndexTupleRoot,
												&iIndexTuple,
												&usSensorId,
												&usSoftwareChannelId,
												&strChannelSensorId);
				iIndexTuple =

				// Assignation de variables temporaires. Les Index de Tuples sont issus de l'analyse des Sensor Id.
				iIndexTupleRootDummy 	= 0;
				iIndexTupleDummy		= 0;
				// Conservation des Id courants de SoftwareChannel, de Sondeurs,
				// du Tuple Racine et du Tuple courant.
				iRet = utilHAC_saveSounderChannelId(&iIndexTupleRootDummy,
												&iIndexTupleDummy,
												&usSoftwareChannelId,
												&uiEchoSounderId,
												&strSounderAndChannelId);



				iRet = iProcess10140Record(fpFileData,
										strTuple10140XML[uiEchoSounderId-1].iNbDatagram,
										iIndexTupleRoot,
										iIndexTuple,
										uiEchoSounderId,
										&strTuple10140XML[uiEchoSounderId-1],
										&strSignalTuple10140[uiEchoSounderId-1]);
				strTuple10140XML[uiEchoSounderId-1].iNbDatagram++;
			}


		}
		else if(strSignalFileIndexRecord.TupleType == 11000)
		{
 		    iRet = iProcess11000Record(fpFileData,
									strTuple11000XML.iNbDatagram,
									&strTuple11000XML,
									&strSignalTuple11000);
		    strTuple11000XML.iNbDatagram++;
		}
		else if(strSignalFileIndexRecord.TupleType == 65534)
		{
 		    iRet = iProcess65534Record(fpFileData,
									strTuple65534XML.iNbDatagram,
									&strTuple65534XML,
									&strSignalTuple65534);
 		    strTuple65534XML.iNbDatagram++;
		}
		else
		{
			// Recherche de datagrammes non d�cod�s d�j� trouv�s.
			for (iLoop=0; iLoop<20; iLoop++)
			{
				if (strSignalFileIndexRecord.TupleType == tabStrTupleNotTranslate[iLoop].uiType)
				{
					tabStrTupleNotTranslate[iLoop].iNbTimes++;
					iFlagFindTuple = TRUE;
					break;
				}
			}
			if (iFlagFindTuple == FALSE)
			{
				tabStrTupleNotTranslate[iNbDatagramNotTranslate].uiType = strSignalFileIndexRecord.TupleType;
				tabStrTupleNotTranslate[iNbDatagramNotTranslate].iNbTimes++;
				iNbDatagramNotTranslate++;
			}
			iFlagFindTuple = FALSE;
		}


		// Calage de l'index de lecture sur le datagramme suivant.
		// Le TupleSize ne concerne que les Data et non les MetaData associ�es qui sont
		// syst�matiques. (TupleSize, BackLink...).
		llPositionDebDatagram = llPositionDebDatagram + (strSignalFileIndexRecord.TupleSize + 10);

	} // Fin de la boucle while de lecture du fichier.

	// End of progress bar
	if(progress)
		progress(100);

	// Fermeture en lecture du fichier de donn�es.
	fclose(fpFileData);

	printf("Datagrams not translated : %d\n", iNbDatagramNotTranslate);
	// Recherche de datagrammes non d�cod�s d�j� trouv�s.
	for (iLoop=0; iLoop<iNbDatagramNotTranslate; iLoop++)
	{
		printf("Process datagram : Datagram %d found %d times not translated\n",	tabStrTupleNotTranslate[iLoop].uiType, tabStrTupleNotTranslate[iLoop].iNbTimes);
	}
	// Fermeture des fichiers binaires des paquets et �criture des XML.
	utilHAC_iSaveAndCloseFiles(-1,G_cRepData, &strFileIndexRecordXML);

	if (strTuple65535XML.iNbDatagram != 0)
	{
		utilHAC_iSaveAndCloseFiles(-1, G_cRepData, &strTuple65535XML);
	}
	if (strTuple20XML.iNbDatagram != 0)
	{
		utilHAC_iSaveAndCloseFiles(-1, G_cRepData, &strTuple20XML);
	}
	if (strTuple30XML.iNbDatagram != 0)
	{
		utilHAC_iSaveAndCloseFiles(-1, G_cRepData, &strTuple30XML);
	}
	if (strTuple11000XML.iNbDatagram != 0)
	{
		utilHAC_iSaveAndCloseFiles(-1, G_cRepData, &strTuple11000XML);
	}
	// Traitement des tuples d�pendant d'une hierachie de d�claration.
	for (iLoop=0; iLoop < NBMAXSOUNDER; iLoop++)
	{
		if (strTuple41XML[iLoop].iNbDatagram != 0)
		{
			utilHAC_iSaveAndCloseFiles(iLoop, G_cRepData, &strTuple41XML[iLoop]);
		}
		if (strTuple210XML[iLoop].iNbDatagram != 0)
		{
			utilHAC_iSaveAndCloseFiles(iLoop, G_cRepData, &strTuple210XML[iLoop]);
		}
		if (strTuple220XML[iLoop].iNbDatagram != 0)
		{
			utilHAC_iSaveAndCloseFiles(iLoop, G_cRepData, &strTuple220XML[iLoop]);
		}
		if (strTuple2100XML[iLoop].iNbDatagram != 0)
		{
			utilHAC_iSaveAndCloseFiles(iLoop, G_cRepData, &strTuple2100XML[iLoop]);
		}
		if (strTuple2200XML[iLoop].iNbDatagram != 0)
		{
			utilHAC_iSaveAndCloseFiles(iLoop,G_cRepData, &strTuple2200XML[iLoop]);
		}
		if (strTuple901XML[iLoop].iNbDatagram != 0)
		{
			utilHAC_iSaveAndCloseFiles(iLoop,G_cRepData, &strTuple901XML[iLoop]);
		}
		if (strTuple9001XML[iLoop].iNbDatagram != 0)
		{
			utilHAC_iSaveAndCloseFiles(iLoop,G_cRepData, &strTuple9001XML[iLoop]);
		}
		if (strTuple10040XML[iLoop].iNbDatagram != 0)
		{
			utilHAC_iSaveAndCloseFiles(iLoop,G_cRepData, &strTuple10040XML[iLoop]);
		}
		if (strTuple10100XML[iLoop].iNbDatagram != 0)
		{
			utilHAC_iSaveAndCloseFiles(iLoop, G_cRepData, &strTuple10100XML[iLoop]);
		}
		if (strTuple10011XML[iLoop].iNbDatagram != 0)
		{
			utilHAC_iSaveAndCloseFiles(iLoop, G_cRepData, &strTuple10011XML[iLoop]);
		}
		if (strTuple10030XML[iLoop].iNbDatagram != 0)
		{
			utilHAC_iSaveAndCloseFiles(iLoop, G_cRepData, &strTuple10030XML[iLoop]);
		}
		if (strTuple10031XML[iLoop].iNbDatagram != 0)
		{
			utilHAC_iSaveAndCloseFiles(iLoop, G_cRepData, &strTuple10031XML[iLoop]);
		}
		if (strTuple10140XML[iLoop].iNbDatagram != 0)
		{
			utilHAC_iSaveAndCloseFiles(iLoop, G_cRepData, &strTuple10140XML[iLoop]);
		}
	}
	if (strTuple65534XML.iNbDatagram != 0)
	{
		utilHAC_iSaveAndCloseFiles(-1, G_cRepData, &strTuple65534XML);
	}

	return EXIT_SUCCESS;

	ERROR_OUT:

	// End of progress bar
	if(progress){
		progress(50);
		progress(100);
	}

	if (fpFileData != NULL) {
	  fclose(fpFileData);
	  fpFileData = 0;
	}

	return EXIT_FAILURE;
} // iConvertFiles_HAC


#pragma pack()
