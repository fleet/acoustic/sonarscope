/*
 * convertFiles_HAC.h
 *
 *  Created on: 12 May 2011
 *      Author: rgallou
 */

#ifndef CONVERTFILES_HAC_H_
#define CONVERTFILES_HAC_H_

#pragma pack(1) // Pour la compilation sous Visual Studio
				// et l'alignement sur 8 octets.
	#define DEBUG 0

	/*** Variables globales ***/

	char			*G_cFileData;			// Fichier avec chemin d'origine
	char			*G_cRepData;			// R�pertoire de destination des donn�es
	int				G_iFlagEndian;			// EndianNess du fichier de donn�e
											// 1 : little, 0: big.
	 char			*G_cFileExtension;		// Extension du fichier de donn�es
											// (.all ou .wcd)

	 /**
	  * Convert files.
	  * @param cFileName file name to convert
	  * @param progress progress function (may be NULL)
	  */
	 int iConvertFiles_HAC(const char *cFileName, void(*progress)(int));

#pragma pack()

#endif /* CONVERTFILES_HAC_H_ */
