// ----------------------------------------------------------------------
// Programme :
//	CONVERT_HAC.C
//
// Objet :
//	Convertir un fichier au format HAC pour d�composer en fichiers
//	XML et fichiers binaires. Les fichiers XML decrivent le contenu des
// 	binaires.
// 	Chaque fichier binaire comporte une donn�e �l�mentaire des paquets par ping
// 	issue des paquets de donn�es du HAC.
//	Il a ete realise sous l'IDE Eclipse Galileo avec compilateur gcc 3.4.1 .
//
// Cr�ation :
//	12/05/2011
//
// Auteur :
//  GLU : (ATL) roger.gallou@altran.com
//		  02 98 05 43 21
//
// Historique des modifications :
//
//
// ----------------------------------------------------------------------
#pragma pack(1) // Pour la compilation sous Visual Studio et l'alignement sur 8 octets.


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <errno.h>
#include <libgen.h>

#include "HAC_utilities.h"
#include "convertFiles_HAC.h"
#include "HAC_Tuples.h"

#include "HAC_processFileIndexRecord.h"

#include "Generic_Utilities.h"    // Contenu dans la librairie libCONVERT_utils.a

#ifdef GUI
#include "ProgressInfo.h"
#endif

int main(int argc, char **argv) {
	char 	cFileName[2000],
			cFileNameHAC[2000],
			cFileNameTmp[2000],
			cDirSonarScope[1500],
			cDirFileData[1500],
			cFlagProgressBar[5];

	int 	iRet = 0,
			iDummy,
			iErr,
			iProgressBar;

	if (argc < 3) {
		printf("Usage: CONVERT_HAC <input .HAC file>\n");
		printf("       Reads an HAC file and prints some data about it.\n");
		return FALSE;
	}


	printf("File Process : %s\n", argv[2]);
	G_cFileData = calloc(150, sizeof(char));
	memcpy(G_cFileData, argv[2], strlen(argv[2]));
	G_cRepData = calloc(200, sizeof(char));
	sprintf(cFileName, "%s", basename(argv[2]));
	G_cFileExtension = (char*) strrchr(cFileName, '.');

	//R�cup�ration du flag de Progress Bar
	memcpy(cFlagProgressBar, argv[1], strlen(argv[1]));
	iProgressBar = (int)atoi(cFlagProgressBar);

	if (G_cFileExtension == NULL) {
		// pas d'extension
		printf("le fichier %s n'a pas d'extension\n", cFileName);
		goto ERROR_OUT;
	} else {
		// sauter le caractere point
		G_cFileExtension++;
	}
	sprintf(cDirFileData, "%s", dirname(argv[2]));
	sprintf(cDirSonarScope, "%s\\%s", cDirFileData, "SonarScope");
	iDummy = access(cDirSonarScope, F_OK);
	if (iDummy != 0) {
		iErr = mkdir(cDirSonarScope);
		if (iErr != 0) {
			switch (errno) {
			// "File exists", on ne fait rien.
			case EEXIST:
				break;
				// Autres cas d'erreurs
			default:
				printf("Error in Directory creating : %s\n", strerror(errno));
				goto ERROR_OUT;
				break;
			}
		}
	}
	cFileName[strlen(cFileName) - 4] = '\0';
	sprintf(cFileNameHAC, "%s\\%s%s", cDirFileData, cFileName, ".HAC");
	sprintf(cFileNameTmp, "%s_tmp", cFileName);
	sprintf(G_cRepData, "%s\\%s", cDirSonarScope, cFileNameTmp);
	if (DEBUG)
		printf("-- Directory Creation : %s\n", G_cRepData);
	iErr = mkdir(G_cRepData);
	if (iErr != 0) {
		switch (errno) {
		// "File exists", on ne fait rien.
		case EEXIST:
			break;
			// Autres cas d'erreurs
		default:
			printf("Error in Directory creating : %s\n", strerror(errno));
			break;
		}
	}
	G_iFlagEndian = utilHAC_iGetIndian(G_cFileData);


	// Appel de la proc�dure de conversion (sans ou avec barre de progressin (mode Release))
	sprintf(G_cFileExtension, "%s", "HAC");
	#ifndef GUI
		iRet = iConvertFiles_HAC(cFileNameHAC, NULL);
	#else
		if (iProgressBar == 1)
			runWithProgress(argc, argv, &iConvertFiles_HAC, cFileNameHAC);
		else
			iRet = iConvertFiles_HAC(cFileNameHAC, NULL);
	#endif


	iRet = util_iRenameDir(G_cRepData);
	if (iRet == 0)
		printf("Process Ending : renaming Temporary Directory %s\n", G_cRepData);

	return EXIT_SUCCESS;

	ERROR_OUT:
		return EXIT_FAILURE;

} // Main

#pragma pack()
