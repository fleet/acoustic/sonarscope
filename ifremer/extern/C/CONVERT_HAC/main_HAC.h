/*
 * MAIN_HAC.h
 *
 *  Created on: 23 mai 2011
 *      Author: rgallou
 */

#ifndef MAIN_HAC_H_
#define MAIN_HAC_H_

#pragma pack(1) // Pour la compilation sous Visual Studio
				// et l'alignement sur 8 octets.
	#define DEBUG 0

	/*** Variables globales ***/

#pragma pack()

#endif /* MAIN_HAC_H_ */
