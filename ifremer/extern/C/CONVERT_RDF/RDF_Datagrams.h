#ifndef RDF_DATAGRAMS_H_
#define RDF_DATAGRAMS_H_

#include "stdio.h"

#include "Generic_Utilities.h"    // Contenu dans la librairie libCONVERT_utils.a

// Description des contenants de variables �l�mentaire de description XML
// Peut servir � d�crire des attributs dont l'�criture au format XML est
// limit�e.
typedef struct {
	   char				cNom[80],			// Label de la variable
					    cTag[50],			// Tag identifiant de la variable
					    cUnit[50],			// Unit� de la variable
						cType[30],			// Type WORD, String, ...
						cNomPathFileBin[300];	// Nom et chemin du fichier Bin
	   FILE				*ptr_fpBin;			// Pointeur du fichier de signaux
	   BOOL 			bFlagConstant;		// Indicateur de constance de la variable
	   unsigned short	usSize;				// n octets
	   unsigned short	usNbElem;			// Nombre de variables �l�mentaires
	   void				*ptr_vPrevVal;		// Valeur pr�c�dente de lecture
	   float			fScaleFactor;		// yVraie = Val*S_F + A_O
	   float			fAddOffset;			//
} T_RDFVARXML;

typedef struct {
	   T_RDFVARXML	*strTabSignalXML;		// Description de l'ent�te d'un paquet
	   T_RDFVARXML	*strTabImageXML;			// Description des data d'un paquet
	   char			*ptr_cNomHeader,		// Nom du Paquet
					*ptr_cLabelHeader,		// Label du paquet
					*ptr_cCommentsHeader,	// Commentaires du paquet
	   				*ptr_cModelHeader,		// Mod�le du syst�me.
	   				*ptr_cSerialNumberHeader,// SN du syst�me.
				    ptr_cNomIndexTable[100];// Nom de la variable d'indexation des
											// variables de type table.
	   int			iNbDatagram;			// Nombre d'occurrence des donn�es.
	   short		sNbVarSignal;			// Nombre de variables de l'ent�te.
	   short		sNbVarImage;			// Nombre de variables de type Image

} T_RDFRECORDXML;


#endif
