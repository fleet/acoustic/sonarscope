/*
 * RDF_utilities.c
 *
 *  Created on: 6 nov. 2008
 *      Author: rgallou
 */
#include<string.h>
#include<stdio.h>
#include<stdlib.h>

#include"RDF_utilities.h"


///////////////////////////////////////////////////////////////////////////////////////////
//// Fonction :	utilRDF_cValVarFromFile
//// Objet 	:	Relit la valeur depuis le fichier Bin.
//// Modification : 12/11/08
//// Auteur 	: GLU
///////////////////////////////////////////////////////////////////////////////////////////
//int utilRDF_cValVarFromFile(T_RDFVARXML strRDFVarXML,
//							char *cValeur)
//{
//unsigned short		usTailleEnByte,
//					usNbElem;
//
//void 				*ptr_vValeur;
//
//int 				iRet = 0;
//
//
//	usTailleEnByte= util_usNbOctetTypeVar(strRDFVarXML.cType);
//	usNbElem = strRDFVarXML.usNbElem;
//
//	ptr_vValeur = (void*)malloc(usTailleEnByte*usNbElem*sizeof(char));
//	strRDFVarXML.ptr_fpBin = fopen(strRDFVarXML.cNomPathFileBin, "r+b");
//	if (!strRDFVarXML.ptr_fpBin)
//	{
//	   printf("%s -- Error in Retrieve Signal constant value  : %s\n", __FILE__, strRDFVarXML.cNomPathFileBin);
//	   iRet = -1;
//	}
//	else
//	{
//		fread(ptr_vValeur, usTailleEnByte, usNbElem, strRDFVarXML.ptr_fpBin);
//		iRet = util_cValVarConstant(	strRDFVarXML.cType,
//										ptr_vValeur,
//										(int)usNbElem,
//										cValeur);
//		fclose(strRDFVarXML.ptr_fpBin);
//		//On lib�re uniquement pour des donn�es de type non-char.
//		// sinon plantage (????)
//		//if (strcmp(strRDFVarXML.cType, "char"))
//		//{
//			free(ptr_vValeur);
//		//}
//	}
//
//	return iRet;
//
//}	// utilRDF_cValVarFromfile

//////////////////////////////////////////////////////////////////////
// Fonction :	utilRDF_iSaveAndCloseFiles
// Objet 	:	Sauvegarde de la structure XML dans un fichier.
// Modification : 15/10/08
// Auteur 	: GLU
//////////////////////////////////////////////////////////////////////
int utilRDF_iSaveAndCloseFiles(	char *cRepData,
								T_RDFRECORDXML *strRDFRecordXML)
{
	char 	*cNomFicXML,
			*ptr_cNomHeader,
			*cTypeMatLab,
			cNomFicBin[500],
			*cValeur = NULL;

    int 	iSize,
			iVarSignal,
			iVarImage,
			iRet = 0;

    unsigned short 	usNbElem,
					usTailleEnByte;

    FILE		*fpXML = NULL;

    iSize = strlen(strRDFRecordXML->ptr_cNomHeader) + 1;
    ptr_cNomHeader = (char *)malloc(iSize*sizeof(char));
    ptr_cNomHeader = strRDFRecordXML->ptr_cNomHeader;
	//iSize = strlen(cRepData) + 2 + strlen(ptr_cNomHeader) + 4; // Nom du paquet + '.xml'
    cNomFicXML = calloc(200, sizeof(char));
	sprintf(cNomFicXML, "%s\\RDF_%s%s", cRepData, ptr_cNomHeader, ".xml");
	printf("-- XML File Creation : %s \n", cNomFicXML);


    // Cr�ation de l'ent�te de doc XML
    fpXML = fopen(cNomFicXML, "w+");

    fprintf(fpXML, "%s", "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r");
    fprintf(fpXML, "%s", "<ROOT>\r");
    fprintf(fpXML, "%s%s%s", "\t<Title>", strRDFRecordXML->ptr_cLabelHeader, "</Title>\r");
    fprintf(fpXML, "%s%s%s", "\t<Constructor>", "GeoAcoustics", "</Constructor>\r");
    //fprintf(fpXML, "%s%s%s", "\t<Model>", "TODO", "</Model>\r");
    //fprintf(fpXML, "%s%s%s", "\t<SystemSerialNumber>", "TODO", "</SystemSerialNumber>\r");
    fprintf(fpXML, "%s%d%s", "\t<NbDatagrams>", strRDFRecordXML->iNbDatagram, "</NbDatagrams>\r");
    fprintf(fpXML, "%s%s%s", "\t<TimeOrigin>", "01/01/-4713", "</TimeOrigin>\r");
    fprintf(fpXML, "%s%s%s", "\t<Comments>", strRDFRecordXML->ptr_cCommentsHeader, "</Comments>\r");
    if (strRDFRecordXML->sNbVarSignal > 0)
    {
    	fprintf(fpXML, "%s", "\t<Variables>\r");

		// Traitement sur l'ensemble des valeurs du RTH.
		for (iVarSignal=0; iVarSignal<strRDFRecordXML->sNbVarSignal; iVarSignal++)
		{
			cTypeMatLab = (char*)malloc(10*sizeof(char));
			iRet = util_cConvertTypeLabel(	strRDFRecordXML->strTabSignalXML[iVarSignal].cType,
											cTypeMatLab);

			fprintf(fpXML, "%s", "\t\t<item>\r");
			fprintf(fpXML, "%s%s%s", "\t\t\t<Name>", strRDFRecordXML->strTabSignalXML[iVarSignal].cNom, "</Name>\r");
			fprintf(fpXML, "%s%s%s", "\t\t\t<Storage>", cTypeMatLab, "</Storage>\r");
			if (strRDFRecordXML->strTabSignalXML[iVarSignal].usNbElem > 1)
			{
				fprintf(fpXML, "%s%d%s", "\t\t\t<NbElem>",  strRDFRecordXML->strTabSignalXML[iVarSignal].usNbElem, "</NbElem>\r");
			}
			//Infos report�e dans format SSC-XML-Bin : fprintf(fpXML, "%s%s%s", "\t\t\t<Unit>", strRDFRecordXML->strTabSignalXML[iVarSignal].cUnit, "</Unit>\r");
			//Infos report�e dans format SSC-XML-Bin : fprintf(fpXML, "%s%s%s", "\t\t\t<Tag>", strRDFRecordXML->strTabSignalXML[iVarSignal].cTag, "</Tag>\r");

			// Fermeture pr�alable du fichier.
			if (strRDFRecordXML->strTabSignalXML[iVarSignal].ptr_fpBin != NULL)
			{
				fclose(strRDFRecordXML->strTabSignalXML[iVarSignal].ptr_fpBin);
			}

			// R�cup�ration de la variable d�tect�e comme constante.
			usNbElem = strRDFRecordXML->strTabSignalXML[iVarSignal].usNbElem;
			usTailleEnByte = util_usNbOctetTypeVar(strRDFRecordXML->strTabSignalXML[iVarSignal].cType);

			if (strRDFRecordXML->strTabSignalXML[iVarSignal].bFlagConstant == TRUE)
			{
				if(cValeur==NULL)
				{
					// Allocation de la valeur � r�cup�rer selon la taille. Si elle est
					// du type Chaine, elle doit contenir autant d'�l�ments que d�clar�s.
					if (strcmp(cTypeMatLab, "char"))
					{
						cValeur = (char*)malloc(256*sizeof(char));
					}
					else
					{
						cValeur = (char*)malloc(usNbElem*sizeof(char));
					}
				}
				iRet = util_cValVarFromFile(strRDFRecordXML->strTabSignalXML[iVarSignal].cNomPathFileBin,
											strRDFRecordXML->strTabSignalXML[iVarSignal].cType,
											strRDFRecordXML->strTabSignalXML[iVarSignal].usNbElem,
											cValeur);
				// D�tection et contournement des caract�res interdits en XML.
				fprintf(fpXML, "%s%s%s", "\t\t\t<Constant>", cValeur, "</Constant>\r");
				// Effacement du fichier Bin
				if (remove((const char *)strRDFRecordXML->strTabSignalXML[iVarSignal].cNomPathFileBin) !=0)
				{
				   printf("Cannot remove file %s (%s)\n",
							   strRDFRecordXML->strTabSignalXML[iVarSignal].cNomPathFileBin, ERRORSTRING);
				}
				free(cValeur);
				cValeur=NULL;
			}
			else
			{
				sprintf(cNomFicBin, "%s%s",strRDFRecordXML->strTabSignalXML[iVarSignal].cNom, ".bin");
				fprintf(fpXML, "%s%s%s", "\t\t\t<FileName>", cNomFicBin, "</FileName>\r");
			}
            if (strRDFRecordXML->strTabSignalXML[iVarSignal].fScaleFactor != 1.0)
				fprintf(fpXML, "%s%g%s", "\t\t\t<ScaleFactor>", strRDFRecordXML->strTabSignalXML[iVarSignal].fScaleFactor, "</ScaleFactor>\r");
            if (strRDFRecordXML->strTabSignalXML[iVarSignal].fAddOffset != 0.0)
				fprintf(fpXML, "%s%f%s", "\t\t\t<AddOffset>", strRDFRecordXML->strTabSignalXML[iVarSignal].fAddOffset, "</AddOffset>\r");
			fprintf(fpXML, "%s", "\t\t</item>\r");
			free(cTypeMatLab);

		} // Fin de la boucle sur les donn�es RTH
    	fprintf(fpXML, "%s", "\t</Variables>\r");
    	free(strRDFRecordXML->strTabSignalXML);
    } // Fin de l'�criture des Signaux

	// Traitement sur l'ensemble des attributs.
    if (strRDFRecordXML->sNbVarImage > 0)
    {
		fprintf(fpXML, "%s", "\t<Tables>\r");
		for (iVarImage=0; iVarImage<strRDFRecordXML->sNbVarImage; iVarImage++)
		{
			cTypeMatLab = (char*)malloc(10*sizeof(char));
			iRet = util_cConvertTypeLabel(	strRDFRecordXML->strTabImageXML[iVarImage].cType,
											cTypeMatLab);

			fprintf(fpXML, "%s", "\t\t<item>\r");
			fprintf(fpXML, "%s%s%s", "\t\t\t<Name>", strRDFRecordXML->strTabImageXML[iVarImage].cNom, "</Name>\r");
			fprintf(fpXML, "%s%s%s", "\t\t\t<Storage>", cTypeMatLab, "</Storage>\r");
			if (strRDFRecordXML->strTabImageXML[iVarImage].usNbElem > 1)
			{
				fprintf(fpXML, "%s%d%s", "\t\t\t<NbElem>", strRDFRecordXML->strTabImageXML[iVarImage].usNbElem, "</NbElem>\r");
			}
			// fprintf(fpXML, "%s%s%s", "\t\t\t<Unit>", strRDFRecordXML->strTabImageXML[iVarImage].cUnit, "</Unit>\r");
			// fprintf(fpXML, "%s%s%s", "\t\t\t<Tag>", strRDFRecordXML->strTabImageXML[iVarImage].cTag, "</Tag>\r");
			// Fermeture pr�alable du fichier.
			fclose(strRDFRecordXML->strTabImageXML[iVarImage].ptr_fpBin);
			usNbElem = strRDFRecordXML->strTabImageXML[iVarImage].usNbElem;
			usTailleEnByte = util_usNbOctetTypeVar(strRDFRecordXML->strTabImageXML[iVarImage].cType);
			if (strRDFRecordXML->ptr_cNomIndexTable != NULL)
			{
				fprintf(fpXML, "%s%s%s", "\t\t\t<Indexation>", strRDFRecordXML->ptr_cNomIndexTable, "</Indexation>\r");
			}

			if (strRDFRecordXML->strTabImageXML[iVarImage].bFlagConstant == TRUE)
			{
				if(cValeur==NULL)
				{
					// Allocation de la valeur � r�cup�rer selon la taille. Si elle est
					// du type Chaine, elle doit contenir autant d'�l�ments que d�clar�s.
					if (strcmp(cTypeMatLab, "char"))
					{
						cValeur = (char*)malloc(256*sizeof(char));
					}
					else
					{
						cValeur = (char*)malloc(usNbElem*sizeof(char));
					}
				}
				//iRet = utilRDF_cValVarFromFile(strRDFRecordXML->strTabImageXML[iVarImage],
				//							cValeur);
				iRet = util_cValVarFromFile(strRDFRecordXML->strTabImageXML[iVarImage].cNomPathFileBin,
											strRDFRecordXML->strTabImageXML[iVarImage].cType,
											strRDFRecordXML->strTabImageXML[iVarImage].usNbElem,
											cValeur);
				fprintf(fpXML, "%s%s%s", "\t\t\t<Constant>", cValeur, "</Constant>\r");
				// Effacement du fichier
				if (remove((const char *)strRDFRecordXML->strTabImageXML[iVarImage].cNomPathFileBin) !=0)
				{
				   printf("Cannot remove file %s (%s)\n",
							   strRDFRecordXML->strTabImageXML[iVarImage].cNomPathFileBin, ERRORSTRING);
				}
				free(cValeur);
				cValeur=NULL;
			}
			else
			{
				sprintf(cNomFicBin, "%s%s",strRDFRecordXML->strTabImageXML[iVarImage].cNom, ".bin");
				fprintf(fpXML, "%s%s%s", "\t\t\t<FileName>", cNomFicBin, "</FileName>\r");
			}
			if (strRDFRecordXML->strTabImageXML[iVarImage].fScaleFactor != 1.0)
				fprintf(fpXML, "%s%g%s", "\t\t\t<ScaleFactor>", strRDFRecordXML->strTabImageXML[iVarImage].fScaleFactor, "</ScaleFactor>\r");
			if (strRDFRecordXML->strTabImageXML[iVarImage].fAddOffset != 0.0)
				fprintf(fpXML, "%s%f%s", "\t\t\t<AddOffset>", strRDFRecordXML->strTabImageXML[iVarImage].fAddOffset, "</AddOffset>\r");
			fprintf(fpXML, "%s", "\t\t</item>\r");
			free(cTypeMatLab);
		} // Fin de la boucle sur les donn�es Data
		free(strRDFRecordXML->strTabImageXML);
		fprintf(fpXML, "%s", "\t</Tables>\r");
    } //Fin de l'�criture des Datas (RD et OD)

	fprintf(fpXML, "%s", "</ROOT>\r");
	fclose(fpXML);

	free(cNomFicXML);
	free(strRDFRecordXML->ptr_cLabelHeader);
	free(strRDFRecordXML->ptr_cNomHeader); // ptr_cNomHeader est du coup lib�r�.

	if (iRet != 0)
		return EXIT_FAILURE;
	else
		return EXIT_SUCCESS;


} //utilRDF_iSaveAndCloseFiles



