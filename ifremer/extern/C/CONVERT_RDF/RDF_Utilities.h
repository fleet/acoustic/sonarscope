/*
 * RDF_utilities.h
 *
 *  Created on: 6 nov. 2008
 *      Author: rgallou
 */
#pragma pack(1)

#ifndef RDF_UTILITIES_H_
#define RDF_UTILITIES_H_

#include "RDF_Datagrams.h"

//int utilRDF_cValVarFromFile(T_RDFVARXML strALLVarXML,
//							char *cValeur);

int utilRDF_iSaveAndCloseFiles(	char *cRepData,
								T_RDFRECORDXML *strALLRecordXML);

#endif /* RDF_UTILITIES_H_ */

#pragma pack()
