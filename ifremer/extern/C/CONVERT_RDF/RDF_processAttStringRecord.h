/*
 * RDF_processAttStringRecord.h
 *
 *  Created on: 21 nov. 2008
 *      Author: rgallou
 */
#pragma pack(1)

#ifndef RDF_PROCESSATTSTRINGRECORD_H_
#define RDF_PROCESSATTSTRINGRECORD_H_

#include "RDF_DATAGRAMS.H"


typedef struct  {
		char 	*AttitudeString;
} T_ATTSTRING_RECORD;


int iProcessAttStringRecord(	FILE *fpData, int iNbRecord,
								int iStringSize,
								T_RDFRECORDXML *strRDFRecordXML,
								T_ATTSTRING_RECORD *ptrSignal_prevRecord);


int iInitXMLBinAttStringRecord(char *cTitleDatagram,
								char *cNomDatagram,
								char *cComments,
								T_RDFRECORDXML *strRDFRecordXML);

#endif /* RDF_PROCESSATTSTRINGECORD_H_ */

#pragma pack()
