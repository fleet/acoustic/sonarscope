/*
 * RDF_processAttitudeRecord.h
 *
 *  Created on: 21 nov. 2008
 *      Author: rgallou
 */
#pragma pack(1)

#ifndef RDF_PROCESSATTITUDERECORD_H_
#define RDF_PROCESSATTITUDERECORD_H_

#include "RDF_DATAGRAMS.H"


typedef struct  {
		float 			roll;
		float			pitch;
		float			heave;
		double			time_stamp;
} T_ATTITUDE_RECORD;


int iProcessAttitudeRecord(	FILE *fpData, int iNbRecord,
							T_RDFRECORDXML *strRDFRecordXML,
							T_ATTITUDE_RECORD *ptrSignal_prevRecord);


int iInitXMLBinAttitudeRecord(char *cTitleDatagram,
								char *cNomDatagram,
								char *cComments,
								T_RDFRECORDXML *strRDFRecordXML);

#endif /* RDF_PROCESSATTITUDERECORD_H_ */

#pragma pack()
