/*
 * RDF_processAux1Record.h
 *
 *  Created on: 21 nov. 2008
 *      Author: rgallou
 */

#ifndef RDF_PROCESSAUX1RECORD_H_
#define RDF_PROCESSAUX1RECORD_H_

#include "RDF_DATAGRAMS.H"


typedef struct  {
		double			time_stamp;
} T_AUX1_RECORD;


int iProcessAux1Record(	FILE *fpData, int iNbRecord,
						T_RDFRECORDXML *strRDFRecordXML,
						T_AUX1_RECORD *ptrSignal_prevRecord);


int iInitXMLBinAux1Record(	char *cTitleDatagram,
							char *cNomDatagram,
							char *cComments,
							T_RDFRECORDXML *strRDFRecordXML);

#endif /* RDF_PROCESSAUX1RECORD_H_ */
