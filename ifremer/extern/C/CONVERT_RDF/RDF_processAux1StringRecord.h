/*
 * RDF_processAux1StringRecord.h
 *
 *  Created on: 21 nov. 2008
 *      Author: rgallou
 */
#pragma pack(1)

#ifndef RDF_PROCESSAUX1STRINGRECORD_H_
#define RDF_PROCESSAUX1STRINGRECORD_H_

#include "RDF_DATAGRAMS.H"


typedef struct  {
		char 	*Aux1String;
} T_AUX1STRING_RECORD;


int iProcessAux1StringRecord(	FILE *fpData, int iNbRecord,
								int iStringSize,
								T_RDFRECORDXML *strRDFRecordXML,
								T_AUX1STRING_RECORD *ptrSignal_prevRecord);


int iInitXMLBinAux1StringRecord(char *cTitleDatagram,
								char *cNomDatagram,
								char *cComments,
								T_RDFRECORDXML *strRDFRecordXML);

#endif /* RDF_PROCESSAUX1STRINGECORD_H_ */

#pragma pack()
