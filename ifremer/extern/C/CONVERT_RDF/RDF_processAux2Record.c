/*
 * RDF_processAux2Record.c
 *
 *  Created on: 13 oct. 2008
 *      Author: rgallou
 */
#pragma pack(1)

#include <sys/types.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "RDF_Datagrams.h"
#include "RDF_Utilities.h"
#include "Generic_Utilities.h"
#include "RDF_processAux2Record.h"
#include "convertFiles_RDF.h"


//----------------------------------------------------------------
// Fonction :	iProcessAux2Record
// Objet 	:	Traitement du paquet Aux2
// Modif.	: 	03/11/08
// Auteur 	:	GLU
//----------------------------------------------------------------
int iProcessAux2Record(FILE *fpData, int iNbRecord,
						T_RDFRECORDXML *strRDFRecordXML,
						T_AUX2_RECORD *ptrAux2_prevRecord)
{
	T_AUX2_RECORD	*ptrAux2_Record;

	// Pointeur pour avancer sur les variables automatiquement.
	unsigned char		*ptr_cAux2Record;
	static void 		*ptr_vVarPrevRecord;
	void				*ptr_vVarRecord;

	BOOL		bFlagVarConstant = TRUE,
				bFlagCompare = TRUE;

    int 	iFlag,
			iVarImage,
			iRetour = 0;

	

    unsigned short	usTailleEnByte,
					usNbElem;

    // Initialisation de la structure d'identification du paquet
 	if (iNbRecord == 0)
	{
 		iInitXMLBinAux2Record("Aux2", "Aux2", "Data MRU", strRDFRecordXML);
	}
 	// Lecture des signaux du paquet
	ptrAux2_Record = (T_AUX2_RECORD *)malloc(sizeof(T_AUX2_RECORD));
	iFlag = fread(ptrAux2_Record, sizeof(T_AUX2_RECORD), 1, G_fpData);
	if (iFlag == 0)
	{
		printf("%s -- Error in reading : %s\n", __FILE__, G_cFileData);
		return FALSE;
	}
	ptr_cAux2Record = (unsigned char*)ptrAux2_Record;
	// Sauvegarde des donn�es des attributs.
	if (iNbRecord > 0)
	{
		ptr_vVarPrevRecord = (void *)ptrAux2_prevRecord;
	}

	for (iVarImage=0; iVarImage<(int)strRDFRecordXML->sNbVarImage; iVarImage++)
	{
		ptr_vVarRecord = (void *)ptr_cAux2Record;
		if (iVarImage==0)
		{
			usTailleEnByte = 0;
			usNbElem = 0;
		}
		else
		{
			usTailleEnByte = util_usNbOctetTypeVar(strRDFRecordXML->strTabImageXML[iVarImage-1].cType);
			usNbElem = strRDFRecordXML->strTabImageXML[iVarImage-1].usNbElem;
		}

		if (iNbRecord > 0)
		{
			bFlagVarConstant = strRDFRecordXML->strTabImageXML[iVarImage].bFlagConstant;
			ptr_vVarPrevRecord = ptr_vVarPrevRecord + usTailleEnByte*usNbElem;
			bFlagCompare = util_bComparePrevAndCurrentVal(strRDFRecordXML->strTabImageXML[iVarImage].cType,
												ptr_vVarPrevRecord,
												ptr_vVarRecord,
											   strRDFRecordXML->strTabImageXML[iVarImage].usNbElem);
			bFlagVarConstant = bFlagVarConstant && bFlagCompare;
			strRDFRecordXML->strTabImageXML[iVarImage].bFlagConstant = bFlagVarConstant;
		}

		// Ecriture sp�ciale car contenant du XML pour la variable Device Info
		// Pr�paration de l'�criture de la variable.
		usTailleEnByte = strRDFRecordXML->strTabImageXML[iVarImage].usSize;
		usNbElem =  strRDFRecordXML->strTabImageXML[iVarImage].usNbElem;
		fwrite(ptr_cAux2Record, usTailleEnByte, usNbElem, strRDFRecordXML->strTabImageXML[iVarImage].ptr_fpBin);
		strRDFRecordXML->strTabImageXML[iVarImage].ptr_vPrevVal = (void *)ptr_cAux2Record;
		ptr_cAux2Record += usTailleEnByte*usNbElem;
	}
	// Recopie en zone tampon des valeurs courantes.
	memmove(ptrAux2_prevRecord, ptrAux2_Record, sizeof(T_AUX2_RECORD));

	free(ptrAux2_Record);
	iNbRecord++;

	return iRetour;

} //iProcessAux2Record


//----------------------------------------------------------------
// Fonction :	iInitXMLBinAux2Record
// Objet 	:	Initialisation de la structure �quivalent au paquet
//				Aux2 pour la manipulation des fichiers bin et XML.
// Modification : 15/10/08
// Auteur 	: GLU
//----------------------------------------------------------------
int iInitXMLBinAux2Record(	char *cTitleDatagram,
								char *cNomDatagram,
								char *cComments,
								T_RDFRECORDXML *strRDFRecordXML)
{

	char cTabNomVarImage[1][40] = {
			"timestamp"};

	char cTabTypeVarImage[1][20] = {
			"double"};

	// ScaleFactor
	float 	fTabSFImage[1] = {
			1.0};

	// AddOffset
	float 	fTabAOImage[1] = {
			0.0};

	char	*ptr_cNomHeader,
			*ptr_cRepertoire,
			cNomFicPathBin[300];

    int 	iSizeRep,
			iErr,
			iLen,
			iVarImage;

	unsigned short usTaille;

	// Initialisation des structures de pilotage des fichiers
	// XML et Bin .
	strRDFRecordXML->sNbVarSignal = 0;
	strRDFRecordXML->sNbVarImage = 1;
    strRDFRecordXML->strTabImageXML= (T_RDFVARXML *)malloc(strRDFRecordXML->sNbVarImage*sizeof(T_RDFVARXML));

	iLen = strlen(cNomDatagram)+1;
	strRDFRecordXML->ptr_cNomHeader = malloc(iLen*sizeof(char));
	sprintf(strRDFRecordXML->ptr_cNomHeader, "%s", cNomDatagram);

	iLen = strlen(cTitleDatagram)+1;
	strRDFRecordXML->ptr_cLabelHeader = malloc(iLen*sizeof(char));
	sprintf(strRDFRecordXML->ptr_cLabelHeader, "%s", cTitleDatagram);

	iLen = strlen(cComments)+1;
	strRDFRecordXML->ptr_cCommentsHeader = malloc(iLen*sizeof(char));
	sprintf(strRDFRecordXML->ptr_cCommentsHeader, "%s", cComments);

	// Remplissage du nom de la variable d'indexation des tables.
	sprintf(strRDFRecordXML->ptr_cNomIndexTable, "%s", "Header\\Aux2Number");


	// Affectation du nom du repertoire.
	ptr_cNomHeader = strRDFRecordXML->ptr_cNomHeader;
	iSizeRep = ( strlen(G_cRepData) + strlen( ptr_cNomHeader));
	// Ajout d'une marge au cas o� certains caract�res soient doubl�s soient doubl�s.
	ptr_cRepertoire = malloc( (iSizeRep + 1 + 50)*sizeof(char));
	sprintf(ptr_cRepertoire, "%s\\RDF_%s", G_cRepData, ptr_cNomHeader);
	if (DEBUG)
	   printf("-- Directory Creation : %s\n", ptr_cRepertoire);

	// Cr�ation du r�pertoire
	iErr = mkdir(ptr_cRepertoire);
	if (iErr != 0)
	{
	   switch (errno)
	   {
		   //"File exists"
		   case EEXIST:
			   break;
		   default:
			   printf("Error in Directory creation %s\n", strerror(errno));
			   return EXIT_FAILURE;
			   break;
	   }
	}

	// Initialisation g�n�rale des variables de type Attitude
	for (iVarImage=0; iVarImage<strRDFRecordXML->sNbVarImage; iVarImage++) {
	   strRDFRecordXML->strTabImageXML[iVarImage].ptr_fpBin = NULL;
	   strRDFRecordXML->strTabImageXML[iVarImage].bFlagConstant = TRUE;
	   strRDFRecordXML->strTabImageXML[iVarImage].usNbElem = 1;
	   sprintf(strRDFRecordXML->strTabImageXML[iVarImage].cNom, "%s", cTabNomVarImage[iVarImage]);
	   sprintf(strRDFRecordXML->strTabImageXML[iVarImage].cType, "%s", cTabTypeVarImage[iVarImage]);
	   sprintf(strRDFRecordXML->strTabImageXML[iVarImage].cTag, "%s", "TODO");
	   sprintf(strRDFRecordXML->strTabImageXML[iVarImage].cUnit, "%s", "TODO");
	   usTaille = util_usNbOctetTypeVar(cTabTypeVarImage[iVarImage]);
	   strRDFRecordXML->strTabImageXML[iVarImage].usSize = usTaille;
	   strRDFRecordXML->strTabImageXML[iVarImage].fScaleFactor = fTabSFImage[iVarImage];
	   strRDFRecordXML->strTabImageXML[iVarImage].fAddOffset = fTabAOImage[iVarImage];
	   iLen = strlen(strRDFRecordXML->strTabImageXML[iVarImage].cNom);
	   // Ajout d'une marge au cas o� certains caract�res soient doubl�s soient doubl�s.
	   sprintf(cNomFicPathBin, "%s\\%s%s", ptr_cRepertoire, strRDFRecordXML->strTabImageXML[iVarImage].cNom, ".bin");
	   sprintf(strRDFRecordXML->strTabImageXML[iVarImage].cNomPathFileBin, "%s", cNomFicPathBin);
	   strRDFRecordXML->strTabImageXML[iVarImage].ptr_fpBin = fopen(cNomFicPathBin, "w+b");
	   if (!strRDFRecordXML->strTabImageXML[iVarImage].ptr_fpBin)
	   {
		   printf("%s -- Error in Data Creation File  : %s\n", __FILE__, cNomFicPathBin);
		   return EXIT_FAILURE;
	   }
	}

	free(ptr_cRepertoire);
	return EXIT_SUCCESS;

} // iInitXMLBinAux2Record

#pragma pack()
