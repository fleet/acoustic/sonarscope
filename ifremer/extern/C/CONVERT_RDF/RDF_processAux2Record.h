/*
 * RDF_processAux2Record.h
 *
 *  Created on: 21 nov. 2008
 *      Author: rgallou
 */

#ifndef RDF_PROCESSAUX2RECORD_H_
#define RDF_PROCESSAUX2RECORD_H_

#include "RDF_DATAGRAMS.H"


typedef struct  {
		double			time_stamp;
} T_AUX2_RECORD;


int iProcessAux2Record(	FILE *fpData, int iNbRecord,
						T_RDFRECORDXML *strRDFRecordXML,
						T_AUX2_RECORD *ptrSignal_prevRecord);


int iInitXMLBinAux2Record(	char *cTitleDatagram,
							char *cNomDatagram,
							char *cComments,
							T_RDFRECORDXML *strRDFRecordXML);

#endif /* RDF_PROCESSAUX2RECORD_H_ */
