/*
 * RDF_processAux2StringRecord.h
 *
 *  Created on: 21 nov. 2008
 *      Author: rgallou
 */
#pragma pack(1)

#ifndef RDF_PROCESSAUX2STRINGRECORD_H_
#define RDF_PROCESSAUX2STRINGRECORD_H_

#include "RDF_DATAGRAMS.H"


typedef struct  {
		char 	*Aux2String;
} T_AUX2STRING_RECORD;


int iProcessAux2StringRecord(	FILE *fpData, int iNbRecord,
								int iStringSize,
								T_RDFRECORDXML *strRDFRecordXML,
								T_AUX2STRING_RECORD *ptrSignal_prevRecord);


int iInitXMLBinAux2StringRecord(char *cTitleDatagram,
								char *cNomDatagram,
								char *cComments,
								T_RDFRECORDXML *strRDFRecordXML);

#endif /* RDF_PROCESSAUX2STRINGECORD_H_ */

#pragma pack()
