/*
 * RDF_processEchoSounderRecord.h
 *
 *  Created on: 21 nov. 2008
 *      Author: rgallou
 */
#pragma pack(1)

#ifndef RDF_PROCESSECHOSOUNDERRECORD_H_
#define RDF_PROCESSECHOSOUNDERRECORD_H_

#include "RDF_DATAGRAMS.H"


typedef struct  {
		float 			depth1;
		float			depth2;
		double			time_stamp;
} T_ECHOSOUNDER_RECORD;


int iProcessEchoSounderRecord(	FILE *fpData, int iNbRecord,
								T_RDFRECORDXML *strRDFRecordXML,
								T_ECHOSOUNDER_RECORD *ptrSignal_prevRecord);


int iInitXMLBinEchoSounderRecord(char *cTitleDatagram,
								char *cNomDatagram,
								char *cComments,
								T_RDFRECORDXML *strRDFRecordXML);

#endif /* RDF_PROCESSECHOSOUNDERRECORD_H_ */
#pragma pack()
