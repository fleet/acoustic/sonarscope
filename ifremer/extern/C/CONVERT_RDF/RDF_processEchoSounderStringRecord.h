/*
 * RDF_processEchoSounderStringRecord.h
 *
 *  Created on: 21 nov. 2008
 *      Author: rgallou
 */
#pragma pack(1)

#ifndef RDF_PROCESSECHOSOUNDERSTRINGRECORD_H_
#define RDF_PROCESSECHOSOUNDERSTRINGRECORD_H_

#include "RDF_DATAGRAMS.H"


typedef struct  {
		char 	*EchoSounderString;
} T_ECHOSOUNDERSTRING_RECORD;


int iProcessEchoSounderStringRecord(FILE *fpData, int iNbRecord,
									int iStringSize,
									T_RDFRECORDXML *strRDFRecordXML,
									T_ECHOSOUNDERSTRING_RECORD *ptrSignal_prevRecord);


int iInitXMLBinEchoSounderStringRecord(	char *cTitleDatagram,
										char *cNomDatagram,
										char *cComments,
										T_RDFRECORDXML *strRDFRecordXML);

#endif /* RDF_PROCESSECHOSOUNDERSTRINGECORD_H_ */

#pragma pack()
