/*
 * RDF_processFileHeaderRecord.c
 *
 *  Created on: 13 oct. 2008
 *      Author: rgallou
 */
#pragma pack(1)

#include <sys/types.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "RDF_Datagrams.h"
#include "RDF_Utilities.h"
#include "Generic_Utilities.h"
#include "RDF_processFileHeaderRecord.h"
#include "convertFiles_RDF.h"


//----------------------------------------------------------------
// Fonction :	iProcessFileHeaderRecord
// Objet 	:	Traitement du paquet FileHeader
// Modif.	: 	03/11/08
// Auteur 	:	GLU
//----------------------------------------------------------------
int iProcessFileHeaderRecord(FILE *fpData, int iNbRecord,
						T_RDFRECORDXML *strRDFRecordXML,
						T_FILEHEADER_RECORD *ptrSignal_prevRecord)
{
	T_FILEHEADER_RECORD	*ptrSignal_Record;

	// Pointeur pour avancer sur les variables automatiquement.
	unsigned char		*ptr_cSignalRecord;
	static void 		*ptr_vVarPrevRecord;
	void				*ptr_vVarRecord;

	BOOL		bFlagVarConstant = TRUE,
				bFlagCompare = TRUE;

    int 	iFlag,
			iVarSignal,
			iRetour = 0;

	

    unsigned short	usTailleEnByte,
					usNbElem;

    // Initialisation de la structure d'identification du paquet
 	if (iNbRecord == 0)
	{
 		iInitXMLBinFileHeaderRecord("FileHeader", "FileHeader", "FileHeader", strRDFRecordXML);
	}

    // Lecture des signaux du paquet
	ptrSignal_Record = (T_FILEHEADER_RECORD *)malloc(sizeof(T_FILEHEADER_RECORD));
	iFlag = fread(ptrSignal_Record, sizeof(T_FILEHEADER_RECORD), 1, G_fpData);
	if (iFlag == 0)
	{
		printf("%s -- Error in reading : %s\n", __FILE__, G_cFileData);
		return FALSE;
	}
	ptr_cSignalRecord = (unsigned char*)ptrSignal_Record;
	// Sauvegarde des donn�es des attributs.
	if (iNbRecord > 0)
	{
		ptr_vVarPrevRecord = (void *)ptrSignal_prevRecord;
	}

	for (iVarSignal=0; iVarSignal<(int)strRDFRecordXML->sNbVarSignal; iVarSignal++)
	{
		ptr_vVarRecord = (void *)ptr_cSignalRecord;
		if (iVarSignal==0)
		{
			usTailleEnByte = 0;
			usNbElem = 0;
		}
		else
		{
			usTailleEnByte = util_usNbOctetTypeVar(strRDFRecordXML->strTabSignalXML[iVarSignal-1].cType);
			usNbElem = strRDFRecordXML->strTabSignalXML[iVarSignal-1].usNbElem;
		}

		if (iNbRecord > 0)
		{
			bFlagVarConstant = strRDFRecordXML->strTabSignalXML[iVarSignal].bFlagConstant;
			ptr_vVarPrevRecord = ptr_vVarPrevRecord + usTailleEnByte*usNbElem;
			bFlagCompare = util_bComparePrevAndCurrentVal(strRDFRecordXML->strTabSignalXML[iVarSignal].cType,
												ptr_vVarPrevRecord,
												ptr_vVarRecord,
											   strRDFRecordXML->strTabSignalXML[iVarSignal].usNbElem);
			bFlagVarConstant = bFlagVarConstant && bFlagCompare;
			strRDFRecordXML->strTabSignalXML[iVarSignal].bFlagConstant = bFlagVarConstant;
		}

		// Ecriture sp�ciale car contenant du XML pour la variable Device Info
		// Pr�paration de l'�criture de la variable.
		usTailleEnByte = strRDFRecordXML->strTabSignalXML[iVarSignal].usSize;
		usNbElem =  strRDFRecordXML->strTabSignalXML[iVarSignal].usNbElem;
		fwrite(ptr_cSignalRecord, usTailleEnByte, usNbElem, strRDFRecordXML->strTabSignalXML[iVarSignal].ptr_fpBin);
		strRDFRecordXML->strTabSignalXML[iVarSignal].ptr_vPrevVal = (void *)ptr_cSignalRecord;
		ptr_cSignalRecord += usTailleEnByte*usNbElem;
	}
	// Recopie en zone tampon des valeurs courantes.
	memmove(ptrSignal_prevRecord, ptrSignal_Record, sizeof(T_FILEHEADER_RECORD));

	free(ptrSignal_Record);
	iNbRecord++;

	return iRetour;

} //iProcessFileHeaderRecord


//----------------------------------------------------------------
// Fonction :	iInitXMLBinSignalRecord
// Objet 	:	Initialisation de la structure �quivalent au paquet
//				Signal pour la manipulation des fichiers bin et XML.
// Modification : 15/10/08
// Auteur 	: GLU
//----------------------------------------------------------------
int iInitXMLBinFileHeaderRecord(char *cTitleDatagram,
								char *cNomDatagram, char *cComments,
								T_RDFRECORDXML *strRDFRecordXML)
{

	char cTabNomVarSignal[7][40] = {
			"DateCreation",
			"RawHeaderSize",
			"RawPingHeaderSize",
			"FileName",
			"Frequency",
			"EchoType",
			"Spare"};

	char cTabNomTypeSignal[7][20] = {
			"unsigned int",
			"unsigned short",
			"unsigned short",
			"char",
			"unsigned int",
			"unsigned short",
			"char"};

	// ScaleFactor
	float 	fTabSFSignal[7] = {
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0};

	// AddOffset
	float 	fTabAOSignal[7] = {
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0};

	char	*ptr_cNomHeader,
			*ptr_cRepertoire,
			cNomFicPathBin[300];

    int 	iSizeRep,
			iErr,
			iLen,
			iVarSignal;

	unsigned short usTaille;

	// Initialisation des structures de pilotage des fichiers
	// XML et Bin .
	strRDFRecordXML->sNbVarSignal = 6; // !!! On n'enregistre pas le Spare
	strRDFRecordXML->sNbVarImage = 0;
    strRDFRecordXML->strTabSignalXML= (T_RDFVARXML *)malloc(strRDFRecordXML->sNbVarSignal*sizeof(T_RDFVARXML));

	iLen = strlen(cNomDatagram)+1;
	strRDFRecordXML->ptr_cNomHeader = malloc(iLen*sizeof(char));
	sprintf(strRDFRecordXML->ptr_cNomHeader, "%s", cNomDatagram);

	iLen = strlen(cTitleDatagram)+1;
	strRDFRecordXML->ptr_cLabelHeader = malloc(iLen*sizeof(char));
	sprintf(strRDFRecordXML->ptr_cLabelHeader, "%s", cTitleDatagram);

	iLen = strlen(cComments)+1;
	strRDFRecordXML->ptr_cCommentsHeader = malloc(iLen*sizeof(char));
	sprintf(strRDFRecordXML->ptr_cCommentsHeader, "%s", cComments);


	// Affectation du nom du repertoire.
	ptr_cNomHeader = strRDFRecordXML->ptr_cNomHeader;
	iSizeRep = ( strlen(G_cRepData) + strlen( ptr_cNomHeader));
	// Ajout d'une marge au cas o� certains caract�res soient doubl�s soient doubl�s.
	ptr_cRepertoire = malloc( (iSizeRep + 1 + 50)*sizeof(char));
	sprintf(ptr_cRepertoire, "%s\\RDF_%s", G_cRepData, ptr_cNomHeader);
	if (DEBUG)
	   printf("-- Directory Creation : %s\n", ptr_cRepertoire);

	// Cr�ation du r�pertoire
	iErr = mkdir(ptr_cRepertoire);
	if (iErr != 0)
	{
	   switch (errno)
	   {
		   //"File exists"
		   case EEXIST:
			   break;
		   default:
			   printf("Error in Directory creation %s\n", strerror(errno));
			   return EXIT_FAILURE;
			   break;
	   }
	}
	// Initialisation g�n�rale des variables de type Signal
	for (iVarSignal=0; iVarSignal<strRDFRecordXML->sNbVarSignal; iVarSignal++) {
	   strRDFRecordXML->strTabSignalXML[iVarSignal].ptr_fpBin = NULL;
	   strRDFRecordXML->strTabSignalXML[iVarSignal].bFlagConstant = TRUE;
	   strRDFRecordXML->strTabSignalXML[iVarSignal].usNbElem = 1;
	   sprintf(strRDFRecordXML->strTabSignalXML[iVarSignal].cNom, "%s", cTabNomVarSignal[iVarSignal]);
	   sprintf(strRDFRecordXML->strTabSignalXML[iVarSignal].cType, "%s", cTabNomTypeSignal[iVarSignal]);
	   sprintf(strRDFRecordXML->strTabSignalXML[iVarSignal].cTag, "%s", "TODO");
	   sprintf(strRDFRecordXML->strTabSignalXML[iVarSignal].cUnit, "%s", "TODO");
	   usTaille = util_usNbOctetTypeVar(cTabNomTypeSignal[iVarSignal]);
	   strRDFRecordXML->strTabSignalXML[iVarSignal].usSize = usTaille;
	   strRDFRecordXML->strTabSignalXML[iVarSignal].fScaleFactor = fTabSFSignal[iVarSignal];
	   strRDFRecordXML->strTabSignalXML[iVarSignal].fAddOffset = fTabAOSignal[iVarSignal];
	   iLen = strlen(strRDFRecordXML->strTabSignalXML[iVarSignal].cNom);
	   // Ajout d'une marge au cas o� certains caract�res soient doubl�s soient doubl�s.
	   sprintf(cNomFicPathBin, "%s\\%s%s", ptr_cRepertoire, strRDFRecordXML->strTabSignalXML[iVarSignal].cNom, ".bin");
	   sprintf(strRDFRecordXML->strTabSignalXML[iVarSignal].cNomPathFileBin, "%s", cNomFicPathBin);
	   strRDFRecordXML->strTabSignalXML[iVarSignal].ptr_fpBin = fopen(cNomFicPathBin, "w+b");
	   if (!strRDFRecordXML->strTabSignalXML[iVarSignal].ptr_fpBin)
	   {
		   printf("%s -- Error in Signal Creation File  : %s\n", __FILE__, cNomFicPathBin);
		   return EXIT_FAILURE;
	   }
	}

	// Cas particulier de la donn�e FileName
	strRDFRecordXML->strTabSignalXML[3].usNbElem = 512;
	// Cas particulier de la donn�e Spare
	strRDFRecordXML->strTabSignalXML[6].usNbElem = 18;

	free(ptr_cRepertoire);
	return EXIT_SUCCESS;

} // iInitXMLBinSignalRecord

#pragma pack()
