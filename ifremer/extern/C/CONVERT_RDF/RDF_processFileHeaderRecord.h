/*
 * RDF_processFileHeaderRecord.h
 *
 *  Created on: 21 nov. 2008
 *      Author: rgallou
 */
#pragma pack(1)

#ifndef RDF_PROCESSFILEHEADERRECORD_H_
#define RDF_PROCESSFILEHEADERRECORD_H_

#include "RDF_DATAGRAMS.H"


typedef struct  {
		unsigned int	DateCreation;
		unsigned short	RawHeaderSize;
		unsigned short	RawPingHeaderSize;
		char			FileName[512];
		unsigned int	Frequency;
		unsigned short	EchoType;
		char 			Spare[18];
} T_FILEHEADER_RECORD;


int iProcessFileHeaderRecord(FILE *fpData, int iNbRecord,
						T_RDFRECORDXML *strRDFRecordXML,
						T_FILEHEADER_RECORD *ptrSignal_prevRecord);


int iInitXMLBinFileHeaderRecord(char *cTitleDatagram,
								char *cNomDatagram,
								char *cComments,
								T_RDFRECORDXML *strRDFRecordXML);

#endif /* RDF_PROCESSHeightRECORD_H_ */

#pragma pack()
