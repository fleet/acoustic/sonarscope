/*
 * RDF_processFileIndexRecord.c
 *
 *  Created on: 13 oct. 2008
 *      Author: rgallou
 */
#pragma pack(1)

#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "RDF_Datagrams.h"
#include "RDF_Utilities.h"
#include "Generic_Utilities.h"
#include "RDF_processFileIndexRecord.h"
#include "convertFiles_RDF.h"


//----------------------------------------------------------------
// Fonction :	iProcessFileIndexRecord
// Objet 	:	Traitement du paquet FileIndex
// Modif.	: 	03/11/08
// Auteur 	:	GLU
//----------------------------------------------------------------
int iProcessFileIndexRecord(FILE *fpFileData,
						int iNbRecord,
						T_RDFRECORDXML *strRDFRecordXML,
						T_FILEINDEX_RECORD *ptrSignal_prevRecord)
{
	T_FILEINDEX_RECORD	*ptrSignal_Record;

	// Pointeur pour avancer sur les variables automatiquement.
	unsigned char		*ptr_cSignalRecord;
	static void 		*ptr_vVarPrevRecord;
	void				*ptr_vVarRecord;

	BOOL		bFlagVarConstant = TRUE,
				bFlagCompare = TRUE;

    int 	iFlag,
			iVarSignal,
			iRetour = 0;

    // On r�initialise errno
    errno = 0;


    unsigned short	usTailleEnByte,
					usNbElem;

	if (iNbRecord == 0)
	{
		iInitXMLBinFileIndexRecord(	"FileIndex",
									"FileIndex",
									"Index File of common data",
									strRDFRecordXML);
	}
	// Ecriture de la donn�e Date.
	ptr_cSignalRecord = (unsigned char *)ptrSignal_prevRecord;
	for (iVarSignal=0; iVarSignal<(int)strRDFRecordXML->sNbVarSignal; iVarSignal++)
	{
		// Pr�paration de l'�criture de la variable.
		usTailleEnByte = strRDFRecordXML->strTabSignalXML[iVarSignal].usSize;
		usNbElem =  strRDFRecordXML->strTabSignalXML[iVarSignal].usNbElem;
		fwrite(ptr_cSignalRecord, usTailleEnByte, usNbElem, strRDFRecordXML->strTabSignalXML[iVarSignal].ptr_fpBin);
		ptr_cSignalRecord += usTailleEnByte*usNbElem;
	}
	iNbRecord++;

	return iRetour;

} //iProcessFileIndexRecord


//----------------------------------------------------------------
// Fonction :	iInitXMLBinFileIndexRecord
// Objet 	:	Initialisation de la structure �quivalent au paquet
//				FileIndex pour la manipulation des fichiers bin et XML.
// Modification : 15/10/08
// Auteur 	: GLU
//----------------------------------------------------------------
int iInitXMLBinFileIndexRecord(	char *cTitleDatagram,
								char *cNomDatagram,
								char *cComments,
								T_RDFRECORDXML *strRDFRecordXML)
{
	char cTabNomVarSignal[5][30] = {
			"Date",
			"Position",
			"NbOfBytesInDatagram",
			"Side",
			"PingCounter"};

	char cTabNomTypeSignal[5][20] = {
			"double",
			"u64",
			"unsigned int",
			"unsigned short",
			"int"};

	// ScaleFactor
	float 	fTabSFSignal[5] = {
			1.0,
			1.0,
			1.0,
			1.0,
			1.0};

	// AddOffset
	float 	fTabAOSignal[5] = {
			0.0,
			0.0,
			0.0,
			0.0,
			0.0};

	char	*ptr_cNomHeader,
			*ptr_cRepertoire,
			cNomFicPathBin[300];

    int 	iSizeRep,
			iRet,
			iErr,
			iLen,
			iVarSignal;

	unsigned short usTaille;

	// Initialisation des structures de pilotage des fichiers
	// XML et Bin .
	strRDFRecordXML->sNbVarSignal = 5;
	strRDFRecordXML->sNbVarImage = 0;
	strRDFRecordXML->strTabSignalXML = (T_RDFVARXML *)malloc(strRDFRecordXML->sNbVarSignal*sizeof(T_RDFVARXML));

	iLen = strlen(cNomDatagram)+1;
	strRDFRecordXML->ptr_cNomHeader = malloc(iLen*sizeof(char));
	sprintf(strRDFRecordXML->ptr_cNomHeader, "%s", cNomDatagram);

	iLen = strlen(cTitleDatagram)+1;
	strRDFRecordXML->ptr_cLabelHeader = malloc(iLen*sizeof(char));
	sprintf(strRDFRecordXML->ptr_cLabelHeader, "%s", cTitleDatagram);

	iLen = strlen(cComments)+1;
	strRDFRecordXML->ptr_cCommentsHeader = malloc(iLen*sizeof(char));
	sprintf(strRDFRecordXML->ptr_cCommentsHeader, "%s", cComments);


	// Affectation et cr�ation du repertoire du Datagramme.
	ptr_cNomHeader = strRDFRecordXML->ptr_cNomHeader;
	iSizeRep = ( strlen(G_cRepData) + strlen( cNomDatagram));
	// Ajout d'une marge au cas o� certains caract�res soient doubl�s soient doubl�s.
	ptr_cRepertoire = malloc( (iSizeRep + 1 + 50)*sizeof(char));
	sprintf(ptr_cRepertoire, "%s\\RDF_%s", G_cRepData, ptr_cNomHeader);
	if (DEBUG)
	   printf("-- Directory Creation : %s\n", ptr_cRepertoire);

	// Cr�ation du r�pertoire
	iErr = mkdir(ptr_cRepertoire);
	if (iErr != 0)
	{
	   switch (errno)
	   {
		   //"File exists"
		   case EEXIST:
			   break;
		   default:
			   printf("Error in Directory creation %s\n", strerror(errno));
			   return EXIT_FAILURE;
			   break;
	   }
	}

	// Initialisation g�n�rale des variables de type Signal
	for (iVarSignal=0; iVarSignal<strRDFRecordXML->sNbVarSignal; iVarSignal++) {
	   strRDFRecordXML->strTabSignalXML[iVarSignal].ptr_fpBin = NULL;
	   // Attention, for�age diff�rent des autres paquets. Les donn�es du FileIndex
	   // ne sont de nature pas constantes.
	   strRDFRecordXML->strTabSignalXML[iVarSignal].bFlagConstant = FALSE;
	   strRDFRecordXML->strTabSignalXML[iVarSignal].usNbElem = 1;
	   sprintf(strRDFRecordXML->strTabSignalXML[iVarSignal].cNom, "%s", cTabNomVarSignal[iVarSignal]);
	   sprintf(strRDFRecordXML->strTabSignalXML[iVarSignal].cType, "%s", cTabNomTypeSignal[iVarSignal]);
	   sprintf(strRDFRecordXML->strTabSignalXML[iVarSignal].cTag, "%s", "TODO");
	   sprintf(strRDFRecordXML->strTabSignalXML[iVarSignal].cUnit, "%s", "TODO");
	   strRDFRecordXML->strTabSignalXML[iVarSignal].fScaleFactor = fTabSFSignal[iVarSignal];
	   strRDFRecordXML->strTabSignalXML[iVarSignal].fAddOffset = fTabAOSignal[iVarSignal];
	   usTaille = util_usNbOctetTypeVar(cTabNomTypeSignal[iVarSignal]);
	   strRDFRecordXML->strTabSignalXML[iVarSignal].usSize = usTaille;
	   iLen = strlen(strRDFRecordXML->strTabSignalXML[iVarSignal].cNom);
	   // Ajout d'une marge au cas o� certains caract�res soient doubl�s soient doubl�s.
	   sprintf(cNomFicPathBin, "%s\\%s%s", ptr_cRepertoire, strRDFRecordXML->strTabSignalXML[iVarSignal].cNom, ".bin");
	   sprintf(strRDFRecordXML->strTabSignalXML[iVarSignal].cNomPathFileBin, "%s", cNomFicPathBin);
	   strRDFRecordXML->strTabSignalXML[iVarSignal].ptr_fpBin = fopen(cNomFicPathBin, "w+b");
	   if (!strRDFRecordXML->strTabSignalXML[iVarSignal].ptr_fpBin)
	   {
		   printf("%s -- Error in Signal creation  : %s\n", __FILE__, cNomFicPathBin);
		   return EXIT_FAILURE;
	   }
	}


	free(ptr_cRepertoire);
	return EXIT_SUCCESS;

} // iInitXMLBinFileIndexRecord

#pragma pack()
