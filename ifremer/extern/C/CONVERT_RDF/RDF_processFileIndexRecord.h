/*
 * RDF_processFileIndexRecord.h
 *
 *  Created on: 21 nov. 2008
 *      Author: rgallou
 */
#pragma pack(1)

#ifndef RDF_PROCESSFILEINDEXRECORD_H_
#define RDF_PROCESSFILEINDEXRECORD_H_

#include "RDF_DATAGRAMS.H"
#include <sys/types.h>


typedef struct  {
		double			Date;
		off64_t 		DatagramPosition;
		int 			NbOfBytesInDatagram;
		unsigned short	Side;
		int				PingCounter;
} T_FILEINDEX_RECORD;


int iProcessFileIndexRecord(FILE *fpFileData,
						int iNbRecord,
						T_RDFRECORDXML *strRDFRecordXML,
						T_FILEINDEX_RECORD *ptrSignal_prevRecord);


int iInitXMLBinFileIndexRecord(	char *cTitleDatagram,
								char *cNomDatagram, char *cComments,
								T_RDFRECORDXML *strRDFRecordXML);

#endif /* RDF_PROCESSFILEINDEXRECORD_H_ */

#pragma pack()
