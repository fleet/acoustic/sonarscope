/*
 * RDF_processHeaderRecord.c
 *
 *  Created on: 13 oct. 2008
 *      Author: rgallou
 */
#pragma pack(1)

#include <sys/types.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "RDF_Datagrams.h"
#include "RDF_Utilities.h"
#include "Generic_Utilities.h"
#include "RDF_processHeaderRecord.h"
#include "convertFiles_RDF.h"


//----------------------------------------------------------------
// Fonction :	iProcessHeaderRecord
// Objet 	:	Traitement du paquet Header
// Modif.	: 	03/11/08
// Auteur 	:	GLU
//----------------------------------------------------------------
int iProcessHeaderRecord(FILE *fpData, int iNbRecord,
						T_RDFRECORDXML *strRDFRecordXML,
						T_HEADER_RECORD *ptrHeader_prevRecord)
{

	T_HEADER_RECORD	*ptrHeader_Record;

	// Pointeur pour avancer sur les variables automatiquement.
	unsigned char		*ptr_cHeaderRecord;
	static void 		*ptr_vVarPrevRecord;
	void				*ptr_vVarRecord;

	BOOL		bFlagVarConstant = TRUE,
				bFlagCompare = TRUE;

    int 	iFlag,
			iVarSignal,
    		iRetour = 0;


    unsigned short	usTailleEnByte,
					usNbElem;

	// Initialisation de la structure d'identification du paquet
 	if (iNbRecord == 0)
	{
 		iInitXMLBinHeaderRecord("Header", "Header", "Header", strRDFRecordXML);
	}
 	// Lecture des signaux du paquet
	ptrHeader_Record = (T_HEADER_RECORD *)malloc(sizeof(T_HEADER_RECORD));
	iFlag = fread(ptrHeader_Record, sizeof(T_HEADER_RECORD), 1, G_fpData);
	if (iFlag == 0)
	{
		printf("%s -- Error in reading : %s\n", __FILE__, G_cFileData);
		return FALSE;
	}
	// Test pour verrouiller le pb de lecture de paquets vides
	// FORGE 293 : Fichier HACHAUMA_037.rdf)
	if (ptrHeader_Record->PingSize > 0)
	{
		ptr_cHeaderRecord = (unsigned char*)ptrHeader_Record;
		// Sauvegarde des donn�es des attributs.
		if (iNbRecord > 0)
		{
			ptr_vVarPrevRecord = (void *)ptrHeader_prevRecord;
		}

		for (iVarSignal=0; iVarSignal<(int)strRDFRecordXML->sNbVarSignal; iVarSignal++)
		{
			ptr_vVarRecord = (void *)ptr_cHeaderRecord;
			if (iVarSignal==0)
			{
				usTailleEnByte = 0;
				usNbElem = 0;
			}
			else
			{
				usTailleEnByte = util_usNbOctetTypeVar(strRDFRecordXML->strTabSignalXML[iVarSignal-1].cType);
				usNbElem = strRDFRecordXML->strTabSignalXML[iVarSignal-1].usNbElem;
			}

			if (iNbRecord > 0)
			{
				bFlagVarConstant = strRDFRecordXML->strTabSignalXML[iVarSignal].bFlagConstant;
				ptr_vVarPrevRecord = ptr_vVarPrevRecord + usTailleEnByte*usNbElem;
				bFlagCompare = util_bComparePrevAndCurrentVal(strRDFRecordXML->strTabSignalXML[iVarSignal].cType,
													ptr_vVarPrevRecord,
													ptr_vVarRecord,
												   strRDFRecordXML->strTabSignalXML[iVarSignal].usNbElem);
				bFlagVarConstant = bFlagVarConstant && bFlagCompare;
				strRDFRecordXML->strTabSignalXML[iVarSignal].bFlagConstant = bFlagVarConstant;
			}

			// Ecriture sp�ciale car contenant du XML pour la variable Device Info
			// Pr�paration de l'�criture de la variable.
			usTailleEnByte = strRDFRecordXML->strTabSignalXML[iVarSignal].usSize;
			usNbElem =  strRDFRecordXML->strTabSignalXML[iVarSignal].usNbElem;
			fwrite(ptr_cHeaderRecord, usTailleEnByte, usNbElem, strRDFRecordXML->strTabSignalXML[iVarSignal].ptr_fpBin);
			strRDFRecordXML->strTabSignalXML[iVarSignal].ptr_vPrevVal = ptr_cHeaderRecord;
			ptr_cHeaderRecord += usTailleEnByte*usNbElem;
		}
	} // Fin du Test sur PingSize.
	// Recopie en zone tampon des valeurs courantes.
	memcpy(ptrHeader_prevRecord, ptrHeader_Record, sizeof(T_HEADER_RECORD));
	iNbRecord++;
	free(ptrHeader_Record);

	return iRetour;

} //iProcessHeaderRecord


//----------------------------------------------------------------
// Fonction :	iInitXMLBinHeaderRecord
// Objet 	:	Initialisation de la structure �quivalent au paquet
//				Header pour la manipulation des fichiers bin et XML.
// Modification : 15/10/08
// Auteur 	: GLU
//----------------------------------------------------------------
int iInitXMLBinHeaderRecord(	char *cTitleDatagram,
								char *cNomDatagram,
								char *cComments,
								T_RDFRECORDXML *strRDFRecordXML)
{
	char cTabNomVarSignal[25][40] = {
			"PingNumber",
			"DateHeure",
			"PreviousPingPosition",
			"PingSize",
			"NavigationNumber",
			"AttitudeNumber",
			"HeadingNumber",
			"EchoSounderNumber",
			"MiniSVSNumber",
			"Aux1Number",
			"Aux2Number",
			"PingLength",
			"PulseLength",
			"Power",
			"SideScanGain",
			"SampleNumber",
			"Side",
			"NavigationStringSize",
			"AttitudeStringSize",
			"HeadingStringSize",
			"EchoSounderStringSize",
			"MiniSVSStringSize",
			"Aux1StringSize",
			"Aux2StringSize",
			"Spare"};

	char cTabNomTypeSignal[25][20] = {
			"i32",
			"double",
			"int",
			"int",
			"u8",
			"u8",
			"u8",
			"u8",
			"u8",
			"u8",
			"u8",
			"unsigned short",
			"u8",
			"u8",
			"u8",
			"int",
			"u8",
			"unsigned short",
			"unsigned short",
			"unsigned short",
			"unsigned short",
			"unsigned short",
			"unsigned short",
			"unsigned short",
			"char"};

	// ScaleFactor
	float 	fTabSFSignal[25] = {
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0};

	// AddOffset
	float 	fTabAOSignal[25] = {
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0};

	char	*ptr_cNomHeader,
			*ptr_cRepertoire,
			cNomFicPathBin[300];

    int 	iSizeRep,
			iErr,
			iLen,
			iVarSignal;

	unsigned short usTaille;

	// Initialisation des structures de pilotage des fichiers
	// XML et Bin .
	strRDFRecordXML->sNbVarSignal = 24;	// Spare n'est pas enregistr�.
	strRDFRecordXML->sNbVarImage = 0;
    strRDFRecordXML->strTabSignalXML= (T_RDFVARXML *)malloc(strRDFRecordXML->sNbVarSignal*sizeof(T_RDFVARXML));

	iLen = strlen(cNomDatagram)+1;
	strRDFRecordXML->ptr_cNomHeader = malloc(iLen*sizeof(char));
	sprintf(strRDFRecordXML->ptr_cNomHeader, "%s", cNomDatagram);

	iLen = strlen(cTitleDatagram)+1;
	strRDFRecordXML->ptr_cLabelHeader = malloc(iLen*sizeof(char));
	sprintf(strRDFRecordXML->ptr_cLabelHeader, "%s", cTitleDatagram);

	iLen = strlen(cComments)+1;
	strRDFRecordXML->ptr_cCommentsHeader = malloc(iLen*sizeof(char));
	sprintf(strRDFRecordXML->ptr_cCommentsHeader, "%s", cComments);


	// Affectation du nom du repertoire.
	ptr_cNomHeader = strRDFRecordXML->ptr_cNomHeader;
	iSizeRep = ( strlen(G_cRepData) + strlen( ptr_cNomHeader));
	// Ajout d'une marge au cas o� certains caract�res soient doubl�s soient doubl�s.
	ptr_cRepertoire = malloc( (iSizeRep + 1 + 50)*sizeof(char));
	sprintf(ptr_cRepertoire, "%s\\RDF_%s", G_cRepData, ptr_cNomHeader);
	if (DEBUG)
	   printf("-- Directory Creation : %s\n", ptr_cRepertoire);

	// Cr�ation du r�pertoire
	iErr = mkdir(ptr_cRepertoire);
	if (iErr != 0)
	{
	   switch (errno)
	   {
		   //"File exists"
		   case EEXIST:
			   break;
		   default:
			   printf("Error in Directory creation %s\n", strerror(errno));
			   return EXIT_FAILURE;
			   break;
	   }
	}
	// Initialisation g�n�rale des variables de type Header
	for (iVarSignal=0; iVarSignal<strRDFRecordXML->sNbVarSignal; iVarSignal++) {
	   strRDFRecordXML->strTabSignalXML[iVarSignal].ptr_fpBin = NULL;
	   strRDFRecordXML->strTabSignalXML[iVarSignal].bFlagConstant = TRUE;
	   strRDFRecordXML->strTabSignalXML[iVarSignal].usNbElem = 1;
	   sprintf(strRDFRecordXML->strTabSignalXML[iVarSignal].cNom, "%s", cTabNomVarSignal[iVarSignal]);
	   sprintf(strRDFRecordXML->strTabSignalXML[iVarSignal].cType, "%s", cTabNomTypeSignal[iVarSignal]);
	   sprintf(strRDFRecordXML->strTabSignalXML[iVarSignal].cTag, "%s", "TODO");
	   sprintf(strRDFRecordXML->strTabSignalXML[iVarSignal].cUnit, "%s", "TODO");
	   usTaille = util_usNbOctetTypeVar(cTabNomTypeSignal[iVarSignal]);
	   strRDFRecordXML->strTabSignalXML[iVarSignal].usSize = usTaille;
	   iLen = strlen(strRDFRecordXML->strTabSignalXML[iVarSignal].cNom);
	   strRDFRecordXML->strTabSignalXML[iVarSignal].fScaleFactor = fTabSFSignal[iVarSignal];
	   strRDFRecordXML->strTabSignalXML[iVarSignal].fAddOffset = fTabAOSignal[iVarSignal];
	   // Ajout d'une marge au cas o� certains caract�res soient doubl�s soient doubl�s.
	   sprintf(cNomFicPathBin, "%s\\%s%s", ptr_cRepertoire, strRDFRecordXML->strTabSignalXML[iVarSignal].cNom, ".bin");
	   sprintf(strRDFRecordXML->strTabSignalXML[iVarSignal].cNomPathFileBin, "%s", cNomFicPathBin);
	   strRDFRecordXML->strTabSignalXML[iVarSignal].ptr_fpBin = fopen(cNomFicPathBin, "w+b");
	   if (!strRDFRecordXML->strTabSignalXML[iVarSignal].ptr_fpBin)
	   {
		   printf("%s -- Error in Signal Creation File  : %s\n", __FILE__, cNomFicPathBin);
		   return EXIT_FAILURE;
	   }
	}
	// Cas particulier du Spare
	// Non enregistr� : strRDFRecordXML->strTabSignalXML[24].usNbElem = 13;

	free(ptr_cRepertoire);
	return EXIT_SUCCESS;

} // iInitXMLBinHeaderRecord

#pragma pack()
