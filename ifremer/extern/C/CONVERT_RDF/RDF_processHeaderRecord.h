/*
 * RDF_processHeaderRecord.h
 *
 *  Created on: 21 nov. 2008
 *      Author: rgallou
 */
#pragma pack(1)

#ifndef RDF_PROCESSHEADERRECORD_H_
#define RDF_PROCESSHEADERRECORD_H_

#include "RDF_DATAGRAMS.H"


typedef struct {
		int 			PingNumber;
		double			DateHeure;
		int				PreviousPingPosition;
		int				PingSize;
		unsigned char	NavigationNumber;
		unsigned char	AttitudeNumber;
		unsigned char	HeadingNumber;
		unsigned char	EchoSounderNumber;
		unsigned char 	MiniSVSNumber;
		unsigned char	Aux1Number;
		unsigned char	Aux2Number;
		unsigned short	PingLength;
		unsigned char	PulseLength;
		unsigned char	Power;
		unsigned char	SideScanGain;
		int				SampleNumber;
		unsigned char	Side;
		unsigned short	NavigationStringSize;
		unsigned short	AttitudeStringSize;
		unsigned short	HeadingStringSize;
		unsigned short	EchoSounderStringSize;
		unsigned short 	MiniSVSStringSize;
		unsigned short	Aux1StringSize;
		unsigned short	Aux2StringSize;
		char			Spare[13];
} T_HEADER_RECORD;


int iProcessHeaderRecord(FILE *fpData, int iNbRecord,
						T_RDFRECORDXML *strRDFRecordXML,
						T_HEADER_RECORD *ptrSignal_prevRecord);


int iInitXMLBinHeaderRecord(	char *cTitleDatagram,
								char *cNomDatagram,
								char *cComments,
								T_RDFRECORDXML *strRDFRecordXML);

#endif /* RDF_PROCESSHEADERRECORD_H_ */

#pragma pack()
