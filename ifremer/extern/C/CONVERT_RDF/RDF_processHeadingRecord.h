/*
 * RDF_processHeadingRecord.h
 *
 *  Created on: 21 nov. 2008
 *      Author: rgallou
 */
#pragma pack(1)

#ifndef RDF_PROCESSHEADINGRECORD_H_
#define RDF_PROCESSHEADINGRECORD_H_

#include "RDF_DATAGRAMS.H"


typedef struct  {
		float			heading;
		double			time_stamp;
} T_HEADING_RECORD;


int iProcessHeadingRecord(	FILE *fpData, int iNbRecord,
							T_RDFRECORDXML *strRDFRecordXML,
							T_HEADING_RECORD *ptrSignal_prevRecord);


int iInitXMLBinHeadingRecord(char *cTitleDatagram,
								char *cNomDatagram,
								char *cComments,
								T_RDFRECORDXML *strRDFRecordXML);

#endif /* RDF_PROCESSHEADINGRECORD_H_ */

#pragma pack()
