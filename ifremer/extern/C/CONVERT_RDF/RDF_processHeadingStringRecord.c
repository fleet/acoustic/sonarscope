/*
 * RDF_processHeadingStringRecord.c
 *
 *  Created on: 13 oct. 2008
 *      Author: rgallou
 */
#pragma pack(1)

#include <sys/types.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "RDF_Datagrams.h"
#include "RDF_Utilities.h"
#include "Generic_Utilities.h"
#include "RDF_processHeadingStringRecord.h"
#include "convertFiles_RDF.h"


//----------------------------------------------------------------
// Fonction :	iProcessHeadingStringRecord
// Objet 	:	Traitement du paquet HeadingString
// Modif.	: 	03/11/08
// Auteur 	:	GLU
//----------------------------------------------------------------
int iProcessHeadingStringRecord(FILE *fpData,
						int iNbRecord,
						int iStringSize,
						T_RDFRECORDXML *strRDFRecordXML,
						T_HEADINGSTRING_RECORD *ptrHeadingString_prevRecord)
{
	T_HEADINGSTRING_RECORD	HeadingString_Record;

	BOOL		bFlagVarConstant = TRUE,
				bFlagCompare = TRUE;

    int 	iFlag,
			iVarImage,
			iRetour = 0;

	

    unsigned short	usTailleEnByte,
					usNbElem;

    // Initialisation de la structure d'identification du paquet
 	if (iNbRecord == 0)
	{
 		iInitXMLBinHeadingStringRecord("HeadingString", "HeadingString", "Gyro String", strRDFRecordXML);
	}
	// Ecriture des donn�es de la partie RD dans les binaires de stockage.
	iVarImage = 0;
	HeadingString_Record.HeadingString = malloc(iStringSize*sizeof(char));
	iFlag = fread(HeadingString_Record.HeadingString, sizeof(char), iStringSize, G_fpData);
	usTailleEnByte = strRDFRecordXML->strTabImageXML[iVarImage].usSize;
	usNbElem = iStringSize; // pour prendre en compte le caract�re NULL
	// Mise a jour de la taille de la chaine lue en dynamique.
	fwrite(HeadingString_Record.HeadingString, usTailleEnByte, usNbElem, strRDFRecordXML->strTabImageXML[iVarImage].ptr_fpBin);
//	if (iNbRecord > 0 && strRDFRecordXML->strTabImageXML[iVarImage].bFlagConstant == TRUE)
//	{
//		bFlagCompare = util_bComparePrevAndCurrentVal(strRDFRecordXML->strTabImageXML[iVarImage].cType,
//											strRDFRecordXML->strTabImageXML[iVarImage].ptr_vPrevVal,
//											HeadingString_Record.HeadingString,
//											usNbElem);
//		strRDFRecordXML->strTabImageXML[iVarImage].bFlagConstant = bFlagCompare && strRDFRecordXML->strTabImageXML[iVarImage].bFlagConstant;
//	}
//	if (iNbRecord == 0)
//	{
//		strRDFRecordXML->strTabImageXML[iVarImage].ptr_vPrevVal = malloc(usNbElem*usTailleEnByte);
//	}
//	memmove(strRDFRecordXML->strTabImageXML[iVarImage].ptr_vPrevVal, HeadingString_Record.HeadingString, iStringSize*usTailleEnByte);

	free(HeadingString_Record.HeadingString);
	iNbRecord++;

	return iRetour;

} //iProcessHeadingStringRecord

//----------------------------------------------------------------
// Fonction :	iInitXMLBinHeadingStringRecord
// Objet 	:	Initialisation de la structure �quivalent au paquet
//				HeadingString pour la manipulation des fichiers bin et XML.
// Modification : 15/10/08
// Auteur 	: GLU
//----------------------------------------------------------------
int iInitXMLBinHeadingStringRecord(	char *cTitleDatagram,
								char *cNomDatagram,
								char *cComments,
								T_RDFRECORDXML *strRDFRecordXML)
{

	char cTabNomVarImage[1][40] = {
			"HeadingString"};

	char cTabNomTypeImage[1][20] = {
			"char"};

	// ScaleFactor
	float 	fTabSFImage[1] = {
			1.0};

	// AddOffset
	float 	fTabAOImage[1] = {
			0.0};

	char	*ptr_cNomHeader,
			*ptr_cRepertoire,
			cNomFicPathBin[300];

    int 	iSizeRep,
			iErr,
			iLen,
			iVarImage;

	unsigned short usTaille;

	// Initialisation des structures de pilotage des fichiers
	// XML et Bin .
	strRDFRecordXML->sNbVarSignal = 0;
	strRDFRecordXML->sNbVarImage = 1;
    strRDFRecordXML->strTabImageXML= (T_RDFVARXML *)malloc(strRDFRecordXML->sNbVarImage*sizeof(T_RDFVARXML));

	iLen = strlen(cNomDatagram)+1;
	strRDFRecordXML->ptr_cNomHeader = malloc(iLen*sizeof(char));
	sprintf(strRDFRecordXML->ptr_cNomHeader, "%s", cNomDatagram);

	iLen = strlen(cTitleDatagram)+1;
	strRDFRecordXML->ptr_cLabelHeader = malloc(iLen*sizeof(char));
	sprintf(strRDFRecordXML->ptr_cLabelHeader, "%s", cTitleDatagram);

	iLen = strlen(cComments)+1;
	strRDFRecordXML->ptr_cCommentsHeader = malloc(iLen*sizeof(char));
	sprintf(strRDFRecordXML->ptr_cCommentsHeader, "%s", cComments);

	// Remplissage du nom de la variable d'indexation des tables.
	sprintf(strRDFRecordXML->ptr_cNomIndexTable, "%s", "Header\\HeadingNumber");


	// Affectation du nom du repertoire.
	ptr_cNomHeader = strRDFRecordXML->ptr_cNomHeader;
	iSizeRep = ( strlen(G_cRepData) + strlen(ptr_cNomHeader));
	// Ajout d'une marge au cas o� certains caract�res soient doubl�s soient doubl�s.
	ptr_cRepertoire = malloc( (iSizeRep + 1 + 50)*sizeof(char));
	sprintf(ptr_cRepertoire, "%s\\RDF_%s", G_cRepData, ptr_cNomHeader);
	if (DEBUG)
	   printf("-- Directory Creation : %s\n", ptr_cRepertoire);

	// Cr�ation du r�pertoire
	iErr = mkdir(ptr_cRepertoire);
	if (iErr != 0)
	{
	   switch (errno)
	   {
		   //"File exists"
		   case EEXIST:
			   break;
		   default:
			   printf("Error in Directory creation %s\n", strerror(errno));
			   return EXIT_FAILURE;
			   break;
	   }
	}
	// Initialisation g�n�rale des variables de type HeadingString
	for (iVarImage=0; iVarImage<strRDFRecordXML->sNbVarImage; iVarImage++) {
	   strRDFRecordXML->strTabImageXML[iVarImage].ptr_fpBin = NULL;
	   strRDFRecordXML->strTabImageXML[iVarImage].bFlagConstant = FALSE;
	   strRDFRecordXML->strTabImageXML[iVarImage].usNbElem = 1;
	   sprintf(strRDFRecordXML->strTabImageXML[iVarImage].cNom, "%s", cTabNomVarImage[iVarImage]);
	   sprintf(strRDFRecordXML->strTabImageXML[iVarImage].cType, "%s", cTabNomTypeImage[iVarImage]);
	   sprintf(strRDFRecordXML->strTabImageXML[iVarImage].cTag, "%s", "TODO");
	   sprintf(strRDFRecordXML->strTabImageXML[iVarImage].cUnit, "%s", "TODO");
	   usTaille = util_usNbOctetTypeVar(cTabNomTypeImage[iVarImage]);
	   strRDFRecordXML->strTabImageXML[iVarImage].usSize = usTaille;
	   strRDFRecordXML->strTabImageXML[iVarImage].fScaleFactor = fTabSFImage[iVarImage];
	   strRDFRecordXML->strTabImageXML[iVarImage].fAddOffset = fTabAOImage[iVarImage];
	   iLen = strlen(strRDFRecordXML->strTabImageXML[iVarImage].cNom);
	   // Ajout d'une marge au cas o� certains caract�res soient doubl�s soient doubl�s.
	   sprintf(cNomFicPathBin, "%s\\%s%s", ptr_cRepertoire, strRDFRecordXML->strTabImageXML[iVarImage].cNom, ".bin");
	   sprintf(strRDFRecordXML->strTabImageXML[iVarImage].cNomPathFileBin, "%s", cNomFicPathBin);
	   strRDFRecordXML->strTabImageXML[iVarImage].ptr_fpBin = fopen(cNomFicPathBin, "w+b");
	   if (!strRDFRecordXML->strTabImageXML[iVarImage].ptr_fpBin)
	   {
		   printf("%s -- Error in Data Creation File  : %s\n", __FILE__, cNomFicPathBin);
		   return EXIT_FAILURE;
	   }
	}


	free(ptr_cRepertoire);
	return EXIT_SUCCESS;

} // iInitXMLBinHeadingStringRecord

#pragma pack()
