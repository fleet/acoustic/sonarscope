/*
 * RDF_processHeadingStringRecord.h
 *
 *  Created on: 21 nov. 2008
 *      Author: rgallou
 */
#pragma pack(1)

#ifndef RDF_PROCESSHEADINGSTRINGRECORD_H_
#define RDF_PROCESSHEADINGSTRINGRECORD_H_

#include "RDF_DATAGRAMS.H"


typedef struct  {
		char 	*HeadingString;
} T_HEADINGSTRING_RECORD;


int iProcessHeadingStringRecord(	FILE *fpData, int iNbRecord,
								int iStringSize,
								T_RDFRECORDXML *strRDFRecordXML,
								T_HEADINGSTRING_RECORD *ptrSignal_prevRecord);


int iInitXMLBinHeadingStringRecord(char *cTitleDatagram,
								char *cNomDatagram,
								char *cComments,
								T_RDFRECORDXML *strRDFRecordXML);

#endif /* RDF_PROCESSHEADINGSTRINGECORD_H_ */

#pragma pack()
