/*
 * RDF_processMiniSVSRecord.h
 *
 *  Created on: 21 nov. 2008
 *      Author: rgallou
 */
#pragma pack(1)

#ifndef RDF_PROCESSMINISVSRECORD_H_
#define RDF_PROCESSMINISVSRECORD_H_

#include "RDF_DATAGRAMS.H"


typedef struct  {
		float 			velocity;
		double			time_stamp;
} T_MINISVS_RECORD;


int iProcessMiniSVSRecord(FILE *fpData, int iNbRecord,
								T_RDFRECORDXML *strRDFRecordXML,
								T_MINISVS_RECORD *ptrSignal_prevRecord);


int iInitXMLBinMiniSVSRecord(	char *cTitleDatagram,
									char *cNomDatagram,
									char *cComments,
									T_RDFRECORDXML *strRDFRecordXML);

#endif /* RDF_PROCESSMINISVSRECORD_H_ */
