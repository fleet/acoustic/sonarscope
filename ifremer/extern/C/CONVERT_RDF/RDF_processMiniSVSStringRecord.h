/*
 * RDF_processMiniSVSStringRecord.h
 *
 *  Created on: 21 nov. 2008
 *      Author: rgallou
 */
#pragma pack(1)

#ifndef RDF_PROCESSMINISVSSTRINGRECORD_H_
#define RDF_PROCESSMINISVSSTRINGRECORD_H_

#include "RDF_DATAGRAMS.H"


typedef struct  {
		char 	*MiniSVSString;
} T_MINISVSSTRING_RECORD;


int iProcessMiniSVSStringRecord(FILE *fpData, int iNbRecord,
								int iStringSize,
								T_RDFRECORDXML *strRDFRecordXML,
								T_MINISVSSTRING_RECORD *ptrSignal_prevRecord);


int iInitXMLBinMiniSVSStringRecord(char *cTitleDatagram,
								char *cNomDatagram,
								char *cComments,
								T_RDFRECORDXML *strRDFRecordXML);

#endif /* RDF_PROCESSMINISVSSTRINGECORD_H_ */

#pragma pack()
