/*
 * RDF_processNavStringRecord.h
 *
 *  Created on: 21 nov. 2008
 *      Author: rgallou
 */
#pragma pack(1)

#ifndef RDF_PROCESSNAVSTRINGRECORD_H_
#define RDF_PROCESSNAVSTRINGRECORD_H_

#include "RDF_DATAGRAMS.H"


typedef struct  {
		char 	*NavigationString;
} T_NAVSTRING_RECORD;


int iProcessNavStringRecord(	FILE *fpData, int iNbRecord,
								int iStringSize,
								T_RDFRECORDXML *strRDFRecordXML,
								T_NAVSTRING_RECORD *ptrSignal_prevRecord);


int iInitXMLBinNavStringRecord(char *cTitleDatagram,
								char *cNomDatagram,
								char *cComments,
								T_RDFRECORDXML *strRDFRecordXML);

#endif /* RDF_PROCESSNAVSTRINGECORD_H_ */

#pragma pack()
