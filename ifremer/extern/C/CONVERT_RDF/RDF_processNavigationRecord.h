/*
 * RDF_processNavigationRecord.h
 *
 *  Created on: 21 nov. 2008
 *      Author: rgallou
 */
#pragma pack(1)

#ifndef RDF_PROCESSNAVIGATIONRECORD_H_
#define RDF_PROCESSNAVIGATIONRECORD_H_

#include "RDF_DATAGRAMS.H"


typedef struct  {
		double 			x;
		double			y;
		float			z;
		double			time;
		double			time_stamp;
		unsigned char	quality;
} T_NAVIGATION_RECORD;


int iProcessNavigationRecord(	FILE *fpData, int iNbRecord,
								T_RDFRECORDXML *strRDFRecordXML,
								T_NAVIGATION_RECORD *ptrSignal_prevRecord);


int iInitXMLBinNavigationRecord(char *cTitleDatagram,
								char *cNomDatagram,
								char *cComments,
								T_RDFRECORDXML *strRDFRecordXML);

#endif /* RDF_PROCESSNAVIGATIONECORD_H_ */

#pragma pack()
