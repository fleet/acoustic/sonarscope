/*
 * RDF_processSampleRecord.h
 *
 *  Created on: 21 nov. 2008
 *      Author: rgallou
 */
#pragma pack(1)

#ifndef RDF_PROCESSSAMPLERECORD_H_
#define RDF_PROCESSSAMPLERECORD_H_

#include "RDF_DATAGRAMS.H"


typedef struct  {
		unsigned short	Time;
		short			Sine;
		unsigned short	Amplitude;
} T_SAMPLE_RECORD;


int iProcessSampleRecord(	FILE *fpData, int iNbRecord,
							T_RDFRECORDXML *strRDFRecordXML,
							T_SAMPLE_RECORD *ptrSignal_prevRecord);


int iInitXMLBinSampleRecord(	char *cTitleDatagram,
								char *cNomDatagram,
								char *cComments,
								T_RDFRECORDXML *strRDFRecordXML);

#endif /* RDF_PROCESSSAMPLERECORD_H_ */

#pragma pack()
