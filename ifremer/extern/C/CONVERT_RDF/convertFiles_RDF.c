// ----------------------------------------------------------------------
// Programme :
//	CONVERT_RDF.C
//
// Objet :
//	Convertir un fichier au format RDF pour d�composer en fichiers
//	XML et fichiers binaires. Les fichiers XML decrivent le contenu des
// 	binaires.
// 	Chaque fichier binaire comporte une donn�e �l�mentaire des paquets par ping
// 	issue des paquets de donn�es du RDF.
//	Il a ete realise sous l'IDE Eclipse 3.4.1 avec compilateur gcc.
//
// Cr�ation :
//	27/11/2008
//
// Auteur :
//  GLU : (ATL) roger.gallou@altran.com
//		  02 98 05 43 21
//
// Historique des modifications :
// v. Main
// ----------------------------------------------------------------------
#pragma pack(1) // Pour la compilation sous Visual Studio
				// et l'alignement sur 8 octets.

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <errno.h>
#include <libgen.h>


#include "RDF_utilities.h"
#include "RDF_Datagrams.h"
#include "convertFiles_RDF.h"
#include "RDF_Datagrams.h"
#include "RDF_processFileHeaderRecord.h"
#include "RDF_processHeaderRecord.h"
#include "RDF_processNavigationRecord.h"
#include "RDF_processAttitudeRecord.h"
#include "RDF_processHeadingRecord.h"
#include "RDF_processEchoSounderRecord.h"
#include "RDF_processMiniSVSRecord.h"
#include "RDF_processAux1Record.h"
#include "RDF_processAux2Record.h"
#include "RDF_processSampleRecord.h"
#include "RDF_processNavStringRecord.h"
#include "RDF_processAttStringRecord.h"
#include "RDF_processHeadingStringRecord.h"
#include "RDF_processEchoSounderStringRecord.h"
#include "RDF_processMiniSVSStringRecord.h"
#include "RDF_processAux1StringRecord.h"
#include "RDF_processAux2StringRecord.h"

#include "RDF_processFileIndexRecord.h"

#include "Generic_Utilities.h"    // Contenu dans la librairie libCONVERT_utils.a

#ifdef GUI
#include "ProgressInfo.h"
#endif


// Fonction de d�claration de la fonction principale de conversion.
// Param�tres :
// - cFileName : r�pertoire et nom du fichier de donn�es
// - barre de progression optionnelle dans l'appel de fonction.
//OLD int iConvertFiles_RDF(char *cFileName, void(*progress)(int))
int iConvertFiles_RDF(const char *cFileName, void(*progress)(int))
{

	char	cDummy;

	int		iRet = 0,
			iDummy,
			iErr,
			iLoop,
			iNbRecord = 0;


	// Adressage d'un fichier potentiellement sup. � 2 Go.
	off64_t 	llPosFic,
				llNbOcFic,
				llPositionDebDatagram = 0;

	T_RDFRECORDXML 			strFileHeaderRecordXML,
							strHeaderRecordXML,
							strSampleRecordXML,
							strNavigationRecordXML,
							strAttitudeRecordXML,
							strHeadingRecordXML,
							strEchoSounderRecordXML,
							strMiniSVSRecordXML,
							strAux1RecordXML,
							strAux2RecordXML,
							strNavStringRecordXML,
							strAttStringRecordXML,
							strHeadingStringRecordXML,
							strEchoSounderStringRecordXML,
							strMiniSVSStringRecordXML,
							strAux1StringRecordXML,
							strAux2StringRecordXML,
							strFileIndexRecordXML;

	T_HEADER_RECORD				strHeaderRecord;
	T_FILEHEADER_RECORD			strFileHeaderRecord;
	T_SAMPLE_RECORD				strSampleRecord;
	T_NAVIGATION_RECORD			strNavigationRecord;
	T_ATTITUDE_RECORD			strAttitudeRecord;
	T_HEADING_RECORD			strHeadingRecord;
	T_ECHOSOUNDER_RECORD		strEchoSounderRecord;
	T_MINISVS_RECORD			strMiniSVSRecord;
	T_AUX1_RECORD				strAux1Record;
	T_AUX2_RECORD				strAux2Record;
	T_NAVSTRING_RECORD			strNavStringRecord;
	T_ATTSTRING_RECORD			strAttStringRecord;
	T_HEADINGSTRING_RECORD		strHeadingStringRecord;
	T_ECHOSOUNDERSTRING_RECORD	strEchoSounderStringRecord;
	T_MINISVSSTRING_RECORD		strMiniSVSStringRecord;
	T_AUX1STRING_RECORD			strAux1StringRecord;
	T_AUX2STRING_RECORD			strAux2StringRecord;
	T_FILEINDEX_RECORD	 		strFileIndexRecord;

    unsigned short	usTailleEnByte,
					usNbElem;

    int 	iVarSignal;

    unsigned char	*ptr_cSignalRecord;

	// Initialisations.
	strFileHeaderRecordXML.iNbDatagram 	= 0;
	strHeaderRecordXML.iNbDatagram = 0;
	strSampleRecordXML.iNbDatagram = 0;
	strNavigationRecordXML.iNbDatagram = 0;
	strAttitudeRecordXML.iNbDatagram = 0;
	strHeadingRecordXML.iNbDatagram = 0;
	strEchoSounderRecordXML.iNbDatagram = 0;
	strMiniSVSRecordXML.iNbDatagram = 0;
	strAux1RecordXML.iNbDatagram = 0;
	strAux2RecordXML.iNbDatagram = 0;
	strNavStringRecordXML.iNbDatagram = 0;
	strAttStringRecordXML.iNbDatagram = 0;
	strHeadingStringRecordXML.iNbDatagram = 0;
	strEchoSounderStringRecordXML.iNbDatagram = 0;
	strMiniSVSStringRecordXML.iNbDatagram = 0;
	strAux1StringRecordXML.iNbDatagram = 0;
	strAux2StringRecordXML.iNbDatagram = 0;
	strFileIndexRecordXML.iNbDatagram = 0;


	// Ouverture du fichier RDF
	G_fpData = fopen64(cFileName,"rb");
	if(G_fpData  == NULL)
	{
		printf("\nProblem in access Data file : %s\n", cFileName);
		goto ERROR_OUT;

	}

    // Calcul de la taille du fichier.
	fseeko64(G_fpData, 0, SEEK_END); //On se positionne � la fin du fichier
    llNbOcFic=ftello64(G_fpData);
    fseeko64(G_fpData, 0, SEEK_SET);
    llPosFic = 0;

    strHeaderRecord.PingNumber = 0;
    strHeaderRecord.DateHeure = 0;

    // Lecture(unique) de l'entete de fichier.
    iRet = iProcessFileHeaderRecord(G_fpData,
									strFileHeaderRecordXML.iNbDatagram,
									&strFileHeaderRecordXML,
									&strFileHeaderRecord);
    strFileHeaderRecordXML.iNbDatagram++;

    //Deroulement entier du fichier .RDF
	while(!feof(G_fpData) && llPosFic < llNbOcFic)
	{
	    llPosFic = ftello64(G_fpData);

	    // Affichage de la progression dans la fen�tre d'ex�cution
#ifdef GUI
	    if(progress)
	    	progress(100*(double)llPosFic/(double)llNbOcFic);
#else
		util_vProgressBar(0, llPosFic, llNbOcFic);
#endif

		iRet = iProcessHeaderRecord(G_fpData,
									strHeaderRecordXML.iNbDatagram,
									&strHeaderRecordXML,
									&strHeaderRecord);

		// Test pour verrouiller le pb de lecture de paquets vides
		// FORGE 293 : Fichier HACHAUMA_037.rdf)
		if (strHeaderRecord.PingSize == 0)
		{
			// On sort carr�ment de la lecture.
			break;
		}
		strHeaderRecordXML.iNbDatagram++;


		// Pour la cr�ation du fichier d'index apr�s lecture du Header.
		// Le paquet FileIndex fait l'objet d'un traitement particulier n'�tant pas un datagramme � part
		// enti�re.
		strFileIndexRecord.Date = strHeaderRecord.DateHeure;
		strFileIndexRecord.DatagramPosition = llPosFic;
		strFileIndexRecord.NbOfBytesInDatagram = strHeaderRecord.PingSize + strFileHeaderRecord.RawPingHeaderSize;
		strFileIndexRecord.Side = strHeaderRecord.Side;
		strFileIndexRecord.PingCounter = strHeaderRecord.PingNumber;

		iRet = iProcessFileIndexRecord(G_fpData,
									strFileIndexRecordXML.iNbDatagram,
									&strFileIndexRecordXML,
									&strFileIndexRecord);
		strFileIndexRecordXML.iNbDatagram++;


		// Lecture des donn�es de signaux de mesures (Amp, Phase, Time).
		for (iLoop=0; iLoop<(int)strHeaderRecord.SampleNumber; iLoop++)
		{
			iRet = iProcessSampleRecord(G_fpData,
										strSampleRecordXML.iNbDatagram,
										&strSampleRecordXML,
										&strSampleRecord);
			if (iLoop == 0)
			{
				strSampleRecordXML.iNbDatagram++;
			}
		}

		// Lecture des donn�es de Navigation
		for (iLoop=0; iLoop<(int)strHeaderRecord.NavigationNumber; iLoop++)
		{
			iRet = iProcessNavigationRecord(G_fpData,
										strNavigationRecordXML.iNbDatagram,
										&strNavigationRecordXML,
										&strNavigationRecord);
			if (iLoop == 0)
			{
				strNavigationRecordXML.iNbDatagram++;
			}
		}

		// Lecture des donn�es de Centrale.
		for (iLoop=0; iLoop<(int)strHeaderRecord.AttitudeNumber; iLoop++)
		{
			iRet = iProcessAttitudeRecord(G_fpData,
										strAttitudeRecordXML.iNbDatagram,
										&strAttitudeRecordXML,
										&strAttitudeRecord);
			if (iLoop == 0)
			{
				strAttitudeRecordXML.iNbDatagram++;
			}
		}

		// Lecture des donn�es de Cap.
		for (iLoop=0; iLoop<(int)strHeaderRecord.HeadingNumber; iLoop++)
		{
			iRet = iProcessHeadingRecord(G_fpData,
										strHeadingRecordXML.iNbDatagram,
										&strHeadingRecordXML,
										&strHeadingRecord);
			if (iLoop == 0)
			{
				strHeadingRecordXML.iNbDatagram++;
			}
		}

		// Lecture des donn�es de Echo Package.
		for (iLoop=0; iLoop<(int)strHeaderRecord.EchoSounderNumber; iLoop++)
		{
			iRet = iProcessEchoSounderRecord(G_fpData,
										strEchoSounderRecordXML.iNbDatagram,
										&strEchoSounderRecordXML,
										&strEchoSounderRecord);
			if (iLoop == 0)
			{
				strEchoSounderRecordXML.iNbDatagram++;
			}
		}

		// Lecture des donn�es de Mini Package.
		for (iLoop=0; iLoop<(int)strHeaderRecord.MiniSVSNumber; iLoop++)
		{
			iRet = iProcessMiniSVSRecord(G_fpData,
										strMiniSVSRecordXML.iNbDatagram,
										&strMiniSVSRecordXML,
										&strMiniSVSRecord);
			if (iLoop == 0)
			{
				strMiniSVSRecordXML.iNbDatagram++;
			}
		}

		// Lecture des donn�es 1er Auxiliaire.
		for (iLoop=0; iLoop<(int)strHeaderRecord.Aux1Number; iLoop++)
		{
			iRet = iProcessAux1Record(G_fpData,
										strAux1RecordXML.iNbDatagram,
										&strAux1RecordXML,
										&strAux1Record);
			if (iLoop == 0)
			{
				strAux1RecordXML.iNbDatagram++;
			}
		}

		// Lecture des donn�es 2eme Auxiliaire.
		for (iLoop=0; iLoop<(int)strHeaderRecord.Aux2Number; iLoop++)
		{
			iRet = iProcessAux2Record(G_fpData,
										strAux2RecordXML.iNbDatagram,
										&strAux2RecordXML,
										&strAux2Record);
			if (iLoop == 0)
			{
				strAux2RecordXML.iNbDatagram++;
			}
		}

		// Lecture d'un caract�re avant les cha�nes d'Acq.
		iDummy = fread(&cDummy, sizeof(char), 1, G_fpData);

		// Lecture des cha�nes d'acq. de Navigation
//		for (iLoop=0; iLoop<(int)strHeaderRecord.NavigationNumber; iLoop++)
//	    {
//			iRet = iProcessNavStringRecord(G_fpData,
//									strNavStringRecordXML.iNbDatagram,
//									strHeaderRecord.NavigationStringSize/strHeaderRecord.NavigationNumber,
//									&strNavStringRecordXML,
//									&strNavStringRecord);
//			if (iLoop == 0)
//			{
//			strNavStringRecordXML.iNbDatagram++;
//			}
//	    }
		if (strHeaderRecord.NavigationNumber != 0) {
			iRet = iProcessNavStringRecord(G_fpData,
										strNavStringRecordXML.iNbDatagram,
										strHeaderRecord.NavigationStringSize,
										&strNavStringRecordXML,
										&strNavStringRecord);
			strNavStringRecordXML.iNbDatagram++;
		}

		// Lecture des cha�nes d'acq. d'Attitude
//		for (iLoop=0; iLoop<(int)strHeaderRecord.AttitudeNumber; iLoop++)
//	    {
//			iRet = iProcessAttStringRecord(G_fpData,
//										strAttStringRecordXML.iNbDatagram,
//										strHeaderRecord.AttitudeStringSize/strHeaderRecord.AttitudeNumber,
//										&strAttStringRecordXML,
//										&strAttStringRecord);
//			if (iLoop == 0)
//			{
//				strAttStringRecordXML.iNbDatagram++;
//			}
//	    }
		if (strHeaderRecord.AttitudeNumber != 0) {
			iRet = iProcessAttStringRecord(G_fpData,
										strAttStringRecordXML.iNbDatagram,
										strHeaderRecord.AttitudeStringSize,
										&strAttStringRecordXML,
										&strAttStringRecord);
			strAttStringRecordXML.iNbDatagram++;
		}
		// Lecture des cha�nes d'acq. de Gyro
//		for (iLoop=0; iLoop<(int)strHeaderRecord.HeadingNumber; iLoop++)
//	    {
//			iRet = iProcessHeadingStringRecord(G_fpData,
//										strHeadingStringRecordXML.iNbDatagram,
//										strHeaderRecord.HeadingStringSize/strHeaderRecord.HeadingNumber,
//										&strHeadingStringRecordXML,
//										&strHeadingStringRecord);
//			if (iLoop == 0)
//			{
//				strHeadingStringRecordXML.iNbDatagram++;
//			}
//	    }
		if (strHeaderRecord.HeadingNumber != 0) {
			iRet = iProcessHeadingStringRecord(G_fpData,
										strHeadingStringRecordXML.iNbDatagram,
										strHeaderRecord.HeadingStringSize,
										&strHeadingStringRecordXML,
										&strHeadingStringRecord);
			strHeadingStringRecordXML.iNbDatagram++;
		}
		// Lecture des cha�nes d'acq. d'Echo Sounder
//		for (iLoop=0; iLoop<(int)strHeaderRecord.EchoSounderNumber; iLoop++)
//	    {
//			iRet = iProcessEchoSounderStringRecord(G_fpData,
//										strEchoSounderStringRecordXML.iNbDatagram,
//										strHeaderRecord.EchoSounderStringSize/strHeaderRecord.EchoSounderNumber,
//										&strEchoSounderStringRecordXML,
//										&strEchoSounderStringRecord);
//			if (iLoop == 0)
//			{
//				strEchoSounderStringRecordXML.iNbDatagram++;
//			}
//	    }
		if (strHeaderRecord.EchoSounderNumber != 0) {
			iRet = iProcessEchoSounderStringRecord(G_fpData,
										strEchoSounderStringRecordXML.iNbDatagram,
										strHeaderRecord.EchoSounderStringSize,
										&strEchoSounderStringRecordXML,
										&strEchoSounderStringRecord);
			strEchoSounderStringRecordXML.iNbDatagram++;
		}

		// Lecture des cha�nes d'acq. de Mini SVS
//		for (iLoop=0; iLoop<(int)strHeaderRecord.MiniSVSNumber; iLoop++)
//	    {
//			iRet = iProcessMiniSVSStringRecord(G_fpData,
//										strMiniSVSStringRecordXML.iNbDatagram,
//										strHeaderRecord.MiniSVSStringSize/strHeaderRecord.MiniSVSNumber,
//										&strMiniSVSStringRecordXML,
//										&strMiniSVSStringRecord);
//			if (iLoop == 0)
//			{
//				strMiniSVSStringRecordXML.iNbDatagram++;
//			}
//	    }
		if (strHeaderRecord.MiniSVSNumber != 0) {
			iRet = iProcessMiniSVSStringRecord(G_fpData,
										strMiniSVSStringRecordXML.iNbDatagram,
										strHeaderRecord.MiniSVSStringSize,
										&strMiniSVSStringRecordXML,
										&strMiniSVSStringRecord);
			strMiniSVSStringRecordXML.iNbDatagram++;
		}

		// Lecture des cha�nes d'acq. de 1er Auxiliaire
//		for (iLoop=0; iLoop<(int)strHeaderRecord.Aux1Number; iLoop++)
//	    {
//			iRet = iProcessAux1StringRecord(G_fpData,
//										strAux1StringRecordXML.iNbDatagram,
//										strHeaderRecord.Aux1StringSize/strHeaderRecord.Aux1Number,
//										&strAux1StringRecordXML,
//										&strAux1StringRecord);
//			if (iLoop == 0)
//			{
//				strAux1StringRecordXML.iNbDatagram++;
//			}
//	    }
		if (strHeaderRecord.Aux1Number != 0) {
			iRet = iProcessAux1StringRecord(G_fpData,
										strAux1StringRecordXML.iNbDatagram,
										strHeaderRecord.Aux1StringSize,
										&strAux1StringRecordXML,
										&strAux1StringRecord);
			strAux1StringRecordXML.iNbDatagram++;
		}
		// Lecture des cha�nes d'acq. de 2nd Auxiliaire
//		for (iLoop=0; iLoop<(int)strHeaderRecord.Aux2Number; iLoop++)
//	    {
//			iRet = iProcessAux2StringRecord(G_fpData,
//										strAux2StringRecordXML.iNbDatagram,
//										strHeaderRecord.Aux2StringSize/strHeaderRecord.Aux2Number,
//										&strAux2StringRecordXML,
//										&strAux2StringRecord);
//			if (iLoop == 0)
//			{
//				strAux2StringRecordXML.iNbDatagram++;
//			}
//	    }
		if (strHeaderRecord.Aux2Number != 0) {
			iRet = iProcessAux2StringRecord(G_fpData,
										strAux2StringRecordXML.iNbDatagram,
										strHeaderRecord.Aux2StringSize,
										&strAux2StringRecordXML,
										&strAux2StringRecord);
			strAux2StringRecordXML.iNbDatagram++;
		}

		// Calage de l'index de lecture sur le datagramme suivant.
		llPosFic = llPosFic + strHeaderRecord.PingSize + strFileHeaderRecord.RawPingHeaderSize;
		fseeko64(G_fpData, llPosFic, SEEK_SET);
		llPosFic = ftello64(G_fpData);
		iNbRecord++;
	} // Fin de la boucle while de lecture du fichier.

	// End of progress bar
	if(progress){
		progress(100);
	}

	// Fermeture en lecture du fichier de donn�es.
	fclose(G_fpData);


	utilRDF_iSaveAndCloseFiles(G_cRepData, &strFileIndexRecordXML);

	if (strFileHeaderRecordXML.iNbDatagram != 0)
	{
		utilRDF_iSaveAndCloseFiles(G_cRepData, &strFileHeaderRecordXML);
	}
	if (strHeaderRecordXML.iNbDatagram != 0)
	{
		utilRDF_iSaveAndCloseFiles(G_cRepData, &strHeaderRecordXML);
	}
	if (strSampleRecordXML.iNbDatagram != 0)
	{
		utilRDF_iSaveAndCloseFiles(G_cRepData, &strSampleRecordXML);
	}
	if (strNavigationRecordXML.iNbDatagram != 0)
	{
		utilRDF_iSaveAndCloseFiles(G_cRepData, &strNavigationRecordXML);
		utilRDF_iSaveAndCloseFiles(G_cRepData, &strNavStringRecordXML);
	}
	if (strAttitudeRecordXML.iNbDatagram != 0)
	{
		utilRDF_iSaveAndCloseFiles(G_cRepData, &strAttitudeRecordXML);
		utilRDF_iSaveAndCloseFiles(G_cRepData, &strAttStringRecordXML);
	}
	if (strHeadingRecordXML.iNbDatagram != 0)
	{
		utilRDF_iSaveAndCloseFiles(G_cRepData, &strHeadingRecordXML);
		utilRDF_iSaveAndCloseFiles(G_cRepData, &strHeadingStringRecordXML);
	}
	if (strEchoSounderRecordXML.iNbDatagram != 0)
	{
		utilRDF_iSaveAndCloseFiles(G_cRepData, &strEchoSounderRecordXML);
		utilRDF_iSaveAndCloseFiles(G_cRepData, &strEchoSounderStringRecordXML);
	}
	if (strMiniSVSRecordXML.iNbDatagram != 0)
	{
		utilRDF_iSaveAndCloseFiles(G_cRepData, &strMiniSVSRecordXML);
		utilRDF_iSaveAndCloseFiles(G_cRepData, &strMiniSVSStringRecordXML);
	}
	if (strAux1RecordXML.iNbDatagram != 0)
	{
		utilRDF_iSaveAndCloseFiles(G_cRepData, &strAux1RecordXML);
		utilRDF_iSaveAndCloseFiles(G_cRepData, &strAux1StringRecordXML);
	}
	if (strAux2RecordXML.iNbDatagram != 0)
	{
		utilRDF_iSaveAndCloseFiles(G_cRepData, &strAux2RecordXML);
		utilRDF_iSaveAndCloseFiles(G_cRepData, &strAux2StringRecordXML);
	}

	return EXIT_SUCCESS;

	ERROR_OUT:
		// End of progress bar
		if(progress){
			progress(50);
			progress(100);
		}

		if (G_fpData != NULL) {
		  fclose(G_fpData);
		  G_fpData = 0;
		}

		return EXIT_FAILURE;
} // iConvertFiles_RDF



#pragma pack()
