/*
 * convertFiles_RDF.h
 *
 *  Created on: 13 oct. 2008
 *      Author: rgallou
 */

#ifndef CONVERTFILES_RDF_H_
#define CONVERTFILES_RDF_H_

#pragma pack(1) // Pour la compilation sous Visual Studio
				// et l'alignement sur 8 octets.
	#define DEBUG 0

	/*** Variables globales ***/

	char			*G_cFileData;			// Fichier avec chemin d'origine
	char			*G_cRepData;			// R�pertoire de destination des donn�es

	FILE			*G_fpData;

	 /**
	  * Convert files.
	  * @param cFileName file name to convert
	  * @param progress progress function (may be NULL)
	  */
	// int iConvertFiles_RDF(char *cFileName, void(*progress)(int));
	int iConvertFiles_RDF(const char *cFileName, void(*progress)(int));

#pragma pack()

#endif /* CONVERTFILES_RDF_H_ */
