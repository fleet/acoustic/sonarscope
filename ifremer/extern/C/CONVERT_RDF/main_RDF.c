// ----------------------------------------------------------------------
// Programme :
//	main_RDF.c
//
// Objet :
//	Convertir un fichier au format RDF pour d�composer en fichiers
//	XML et fichiers binaires. Les fichiers XML decrivent le contenu des
// 	binaires.
// 	Chaque fichier binaire comporte une donn�e �l�mentaire des paquets par ping
// 	issue des paquets de donn�es du RDF.
//	Il a ete realise sous l'IDE Eclipse 3.4.1 avec compilateur gcc.
//
// Cr�ation :
//	27/11/2008
//
// Auteur :
//  GLU : (ATL) roger.gallou@altran.com
//		  02 98 05 43 21
//
// Historique des modifications :
// - passage du compteur de datagramme comme param�tre des fcts de traitements.
// - ajout de Facteur d'�chelles.
// - Renommage de Signals en Variables, de Tables en Images
// - correction du typage de donn�es de type Unsigned Char en u8
// - introduction des donn�es � 2 Dimensions (Image2)
// - restructuration de l'ent�te du fichier
// - cr�ation d'un fichier d'index.
// - traitement de fichiers pour 64 bits.
// - d�placement de fonction utilitaires (Swap et Val Variable from File)
// - inhibition des balises Unit et Tagf, inutilis�es pour l'instant.

// 27/09/2012 : GLU (ALTRAN Ouest)
// - Passage en option de l'affichage de la barre de progression.
//
// ----------------------------------------------------------------------
#pragma pack(1) // Pour la compilation sous Visual Studio
				// et l'alignement sur 8 octets.

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <errno.h>
#include <libgen.h>


#include "RDF_utilities.h"
#include "main_RDF.h"
#include "convertFiles_RDF.h"
#include "Generic_Utilities.h"    // Contenu dans la librairie libCONVERT_utils.a

#ifdef GUI
#include "ProgressInfo.h"
#endif


// D�claration du programme principal
int main(int argc, char **argv) {

char	cFileName[2000],
		cFileNameTmp[2000],
		cDirSonarScope[1500],
		cDummy,
		cFlagProgressBar[5];

int		iRet = 0,
		iDummy,
		iErr,
		iLoop,
		iNbRecord = 0,
		iProgressBar;

	if (argc < 3) {
		printf("Usage: CONVERT_RDF <input .RDF file>\n");
		printf("       Reads an RDF file and prints some data about it.\n");
		return FALSE;
	}

	// Cr�ation du r�pertoire Racine contenant les .bin.
	printf("File Process : %s\n", argv[2]);
	G_cFileData = calloc(150, sizeof(char));
	memcpy(G_cFileData, argv[2], strlen(argv[2]));
	G_cRepData = calloc(200, sizeof(char));
	// dirname pour le chemin et basename pour le nom de fichier simple.
	sprintf(cFileName, "%s", basename(argv[2]));
	// Suppression de l'extension
	cFileName[strlen(cFileName)-4] = '\0';
	sprintf(cFileNameTmp, "%s_tmp", cFileName);

	//R�cup�ration du flag de Progress Bar
	memcpy(cFlagProgressBar, argv[1], strlen(argv[1]));
	iProgressBar = (int)atoi(cFlagProgressBar);

	// V�rification de l'existence du r�petoire SonarScope.
	sprintf(cDirSonarScope, "%s\\%s", dirname(argv[2]), "SonarScope");

	iDummy = access(cDirSonarScope, F_OK);
	if (iDummy != 0)
	{
		iErr = mkdir(cDirSonarScope);
		if (iErr != 0)
		{
		   switch (errno)
		   {
			   // "File exists", on ne fait rien.
			   case EEXIST:
				   break;
			   // Autres cas d'erreurs
			   default:
				   printf("Error in Directory creating : %s\n", strerror(errno));
				   return FALSE;
				   break;
		   }
		}
	}

	// Cr�ation du r�pertoire de donn�es
	sprintf(G_cRepData, "%s\\%s", cDirSonarScope, cFileNameTmp);

	if (DEBUG)
	   printf("-- Directory Creation : %s\n", G_cRepData);

	iErr = mkdir(G_cRepData);
	if (iErr != 0)
	{
	   switch (errno)
	   {
		   // "File exists", on ne fait rien.
		   case EEXIST:
			   break;
		   // Autres cas d'erreurs
		   default:
			   printf("Error in Directory creating : %s\n", strerror(errno));
			   return FALSE;
			   break;
	   }
	}

	// Appel de la proc�dure principale de d�coupage de fichiers.
	#ifndef GUI
		iRet = iConvertFiles_RDF(G_cFileData, NULL);
	#else
		if (iProgressBar == 1)
			runWithProgress(argc, argv, &iConvertFiles_RDF, G_cFileData);
		else
			iRet = iConvertFiles_RDF(G_cFileData, NULL);
	#endif


	// Renommage du r�pertoire puisqu'on arrive au bout !
	iRet = util_iRenameDir(G_cRepData);
	if (iRet == 0)
		printf("Process Ending : renaming Temporary Directory %s\n", G_cRepData);

	return EXIT_SUCCESS;

	ERROR_OUT:
		return EXIT_FAILURE;
} // Main

#pragma pack()
