%% Génération des XML
global IfrTbx %#ok<GVMIS>
str = {};
nomFic = getNomFicDatabase('EW150.rdf');
nomExe = fullfile(IfrTbx, 'ifremer', 'extern', 'C', 'CONVERT_exe', 'Release', 'CONVERT_RDF.exe');
cmd = sprintf('!%s %s', nomExe, nomFic);
eval(cmd);
%%
nomFic = getNomFicDatabase('Cormo001.rdf');
nomExe = fullfile(IfrTbx, 'ifremer', 'extern', 'C', 'CONVERT_exe', 'Release', 'CONVERT_RDF.exe');
cmd = sprintf('!%s %s', nomExe, nomFic);
eval(cmd);
%%
nomFic = getNomFicDatabase('194023.rdf');
nomExe = fullfile(IfrTbx, 'ifremer', 'extern', 'C', 'CONVERT_exe', 'Release', 'CONVERT_RDF.exe');
cmd = sprintf('!%s %s', nomExe, nomFic);
eval(cmd);
%% Génération des XML
str = {};
nomFic = 'erreur.rdf';
nomExe = fullfile(IfrTbx, 'ifremer', 'extern', 'C', 'CONVERT_RDF', 'Release', 'CONVERT_RDF.exe');
cmd = sprintf('!%s %s', nomExe, nomFic);
eval(cmd);

%% Ecritures des bin par JMA
identSondeur = selectionSondeur(a);
flag = write_sonarSettings_bin(a, identSondeur);
%%
nomDir = '???\SonarScope\EW150\Navigation';
Data.Signals.FileName = 'x.bin';
Data.Signals.Storage = 'double';
Data.Signals.Direction = 'FirstValue=FirstPing';
Data.Signals.Unit = 'm';
format long;
[flag, X] = readSignal(nomDir, Data.Signals, 1);
%%
nomDir = '???\SonarScope\EW150\Navigation';
Data.Signals.FileName = 'y.bin';
Data.Signals.Storage = 'double';
Data.Signals.Direction = 'FirstValue=FirstPing';
Data.Signals.Unit = 'm';
format long;
[flag, Y] = readSignal(nomDir, Data.Signals, 1);
%%
h = figure; hp = plot(X, Y);
