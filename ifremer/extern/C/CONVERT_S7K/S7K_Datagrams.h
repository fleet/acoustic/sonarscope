#ifndef S7K_DATAGRAMS_H_
#define S7K_DATAGRAMS_H_

#include "stdio.h"
#include "Generic_Utilities.h"    // Contenu dans la librairie libCONVERT_utils.a

#define S7K_HEADER_SONAR        0 // sidescan and subbottom

// Description des contenants de variables �l�mentaire de description XML
// Peut servir � d�crire des attributs dont l'�criture au format XML est
// limit�e.
typedef struct {
	   char				cNom[50],			// Label de la variable
					    cTag[50],			// Tag identifiant de la variable
					    cUnit[50],			// Unit� de la variable
						cType[30],			// Type WORD, String, ...
						cNomPathFileBin[300];	// Nom et chemin du fichier Bin
	   FILE				*ptr_fpBin;			// Pointeur du fichier de signaux
	   BOOL 			bFlagConstant;		// Indicateur de constance de la variable
	   unsigned short	usSize;				// n octets
	   unsigned short	usNbElem;			// Nombre de variables �l�mentaires
	   void				*ptr_vPrevVal;		// Valeur pr�c�dente de lecture
	   float			fScaleFactor;		// yVraie = Val*S_F + A_O
	   float			fAddOffset;			//
} T_S7KVarXML;

typedef struct {
	   T_S7KVarXML	*strTabRTHXML;				// Description des signaux du RTH du paquet
	   T_S7KVarXML	*strTabRDXML;				// Description des signaux du RD du paquet
	   T_S7KVarXML	*strTabODXML;				// Description des signaux du OD (opional Data) du paquet
	   u32			uiODOffset;					// Offset des paquets Optional Data
	   char			*ptr_cNomHeader,			// Nom du Paquet
					*ptr_cLabelHeader,			// Label du paquet
					*ptr_cCommentsHeader,		// Commentaires du paquet
				    //*ptr_cConstructorHeader,	// Nom du constructeur.
	   				//*ptr_cModelHeader,		// Mod�le du syst�me.
	   				//*ptr_cSerialNumberHeader,	// SN du syst�me.
				    *ptr_cNomIndexTable;		// Nom de la variable d'indexation des
												// variables de type table.
	   int			iNbDatagram;				// Nombre d'occurrence des paquets.
	   short		sNbVarRTH;					// Nombre de signaux du RTH
	   short		sNbVarRD;					// Nombre de signaux du RD
	   short		sNbVarOD;					// Nombre de signaux du OD
} T_S7KRecordXML;

// Description unitaire des signaux
typedef struct  {
	char 			*cTabNomVar,
					*cTabNomType;
	float 			fScaleFactor;
	float 			fAddOffset;
} T_DESC_SIGNAL_OR_IMAGE;

// Description unitaire des signaux
typedef struct  {
	unsigned short 				usNbVarSignal;
	unsigned short 				usNbVarImage;
	unsigned short 				usNbVarImage2;
	short 						sNumIndexTable;
	T_DESC_SIGNAL_OR_IMAGE  	*strDescSignal;
	T_DESC_SIGNAL_OR_IMAGE  	*strDescImage;
	T_DESC_SIGNAL_OR_IMAGE  	*strDescImage2;
} T_DESC_TAB_SIGANDIMG;

// Pour les record qui ne sont pas reconnus.
typedef struct
{
	unsigned short 	uiType;
	int				iNbTimes;
} T_TYPEORECORDNOTTRANSLATED;

#endif
