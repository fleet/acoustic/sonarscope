/*
 * S7K_utilities.c
 *
 *  Created on: 9 oct. 2008
 *      Author: rgallou
 */
#pragma pack(1)

#include "S7K_Utilities.h"
#include "convertFiles_S7K.h"
#include "string.h"
#include "stdlib.h"
#include "math.h"
#include "stdio.h"
#include "S7K_Datagrams.h"
#include <errno.h>


////----------------------------------------------------------------
//// Fonction :	utilS7K_cValVarFromFile
//// Objet 	:	Relit la valeur depuis le fichier Bin.
//// Modification : 12/11/08
//// Auteur 	: GLU
////----------------------------------------------------------------
//int utilS7K_cValVarFromFile(T_S7KVarXML strS7KVarXML,
//							char *cValeur)
//{
//unsigned short		usTailleEnByte,
//					usNbElem;
//
//void 				*ptr_vValeur;
//
//int 				iRet = 0;
//
//
//	usTailleEnByte= util_usNbOctetTypeVar(strS7KVarXML.cType);
//	usNbElem = strS7KVarXML.usNbElem;
//
//	ptr_vValeur = (void*)malloc(usTailleEnByte*usNbElem*sizeof(char));
//	strS7KVarXML.ptr_fpBin = fopen(strS7KVarXML.cNomPathFileBin, "r+b");
//	if (!strS7KVarXML.ptr_fpBin)
//	{
//	   printf("%s -- Erreur dans la relecture du Signal constant  : %s\n", __FILE__, strS7KVarXML.cNomPathFileBin);
//	   iRet = -1;
//	}
//	else
//	{
//		fread(ptr_vValeur, usTailleEnByte, usNbElem, strS7KVarXML.ptr_fpBin);
//		iRet = util_cValVarConstant(	strS7KVarXML.cType,
//										ptr_vValeur,
//										(int)usNbElem,
//										cValeur);
//		fclose(strS7KVarXML.ptr_fpBin);
//
//		free(ptr_vValeur);
//	}
//
//	return iRet;
//
//}	// util_cValVarFromfile


//----------------------------------------------------------------
// Fonction :	utilS7K_iSaveAndCloseFiles
// Objet 	:	Sauvegarde de la structure XML dans un fichier.
// Modification : 15/10/08
// Auteur 	: GLU
//----------------------------------------------------------------
int utilS7K_iSaveAndCloseFiles(char *cRepData, T_S7KRecordXML *strS7KRecordXML)
{
	char 	*cNomFicXML,
			*cNomFicXMLDeviceInfo,
			*ptr_cNomHeader,
			cNbElem[20],
			*cTypeMatLab,
			cNomFicBin[100],
			*cValeur = NULL;

    int 	iSize,
			iVarRTH,
			iVarRD,
			iVarOD,
			iFlag,
			iRet = 0;

    unsigned short 	usNbElem,
					usTailleEnByte;

    off64_t	lDummy;

    FILE		*fpDeviceInfoXML = NULL;
    FILE		*fpXML = NULL;


    iSize = strlen(strS7KRecordXML->ptr_cNomHeader) + 1;
    ptr_cNomHeader = (char *)malloc(iSize*sizeof(char));
    ptr_cNomHeader = strS7KRecordXML->ptr_cNomHeader;
	iSize = strlen(cRepData) + strlen(ptr_cNomHeader) + 20; // Nom du r�pertoire + du paquet + Marge
    cNomFicXML = calloc(iSize, sizeof(char));
	sprintf(cNomFicXML, "%s%s\\S7K_%s%s", cRepData, "_tmp", ptr_cNomHeader, ".xml");
	printf("-- XML File Creation : %s \n", cNomFicXML);


    // Cr�ation de l'ent�te de doc XML
    fpXML = fopen(cNomFicXML, "w+");
    fprintf(fpXML, "%s", "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r");
    fprintf(fpXML, "%s", "<ROOT>\r");
    fprintf(fpXML, "%s%s%s", "\t<Title>", strS7KRecordXML->ptr_cLabelHeader, "</Title>\r");
    fprintf(fpXML, "%s%s%s", "\t<Constructor>", "RESON", "</Constructor>\r");
    fprintf(fpXML, "%s%d%s", "\t<Model>", G_u32SystemSerialNumber, "</Model>\r");
    //fprintf(fpXML, "%s%d%s", "\t<SystemSerialNumber>", G_u32SystemSerialNumber, "</SystemSerialNumber>\r");
    fprintf(fpXML, "%s%d%s", "\t<NbDatagrams>", strS7KRecordXML->iNbDatagram, "</NbDatagrams>\r");
    fprintf(fpXML, "%s%s%s", "\t<TimeOrigin>", "01/01/-4713", "</TimeOrigin>\r");
    fprintf(fpXML, "%s%s%s", "\t<Comments>", strS7KRecordXML->ptr_cCommentsHeader, "</Comments>\r");
    if (strS7KRecordXML->sNbVarRTH > 0)
    {
    	fprintf(fpXML, "%s", "\t<Variables>\r");

		// Traitement sur l'ensemble des variables du RTH.
		for (iVarRTH=0; iVarRTH<strS7KRecordXML->sNbVarRTH; iVarRTH++)
		{
			// Fermeture pr�alable du fichier.
			fclose(strS7KRecordXML->strTabRTHXML[iVarRTH].ptr_fpBin);

			cTypeMatLab = (char*)malloc(10*sizeof(char));
			iRet = util_cConvertTypeLabel(	strS7KRecordXML->strTabRTHXML[iVarRTH].cType,
											cTypeMatLab);

			fprintf(fpXML, "%s", "\t\t<item>\r");
			fprintf(fpXML, "%s%s%s", "\t\t\t<Name>", strS7KRecordXML->strTabRTHXML[iVarRTH].cNom, "</Name>\r");
			fprintf(fpXML, "%s%s%s", "\t\t\t<Storage>", cTypeMatLab, "</Storage>\r");
			if (strS7KRecordXML->strTabRTHXML[iVarRTH].usNbElem > 1)
			{
				sprintf(cNbElem, "%d", strS7KRecordXML->strTabRTHXML[iVarRTH].usNbElem);
				fprintf(fpXML, "%s%s%s", "\t\t\t<NbElem>", cNbElem, "</NbElem>\r");
			}
			// R�cup�ration des caract�ristiques de la variable d�tect�e comme constante.
			usNbElem = strS7KRecordXML->strTabRTHXML[iVarRTH].usNbElem;
			usTailleEnByte = util_usNbOctetTypeVar(strS7KRecordXML->strTabRTHXML[iVarRTH].cType);

			if (strS7KRecordXML->strTabRTHXML[iVarRTH].bFlagConstant == TRUE)
			{
				if(cValeur==NULL)
				{
					// Allocation de la valeur � r�cup�rer selon la taille. Si elle est
					// du type Chaine, elle doit contenir autant d'�l�ments que d�clar�s.
					if (strcmp(cTypeMatLab, "char"))
					{
						cValeur = (char*)malloc(256*sizeof(char));
					}
					else
					{
						cValeur = (char*)malloc(usNbElem*sizeof(char));
					}
				}
//				iRet = utilS7K_cValVarFromFile(	strS7KRecordXML->strTabRTHXML[iVarRTH],
//												cValeur);
				iRet = util_cValVarFromFile(strS7KRecordXML->strTabRTHXML[iVarRTH].cNomPathFileBin,
											strS7KRecordXML->strTabRTHXML[iVarRTH].cType,
											strS7KRecordXML->strTabRTHXML[iVarRTH].usNbElem,
											cValeur);
				fprintf(fpXML, "%s%s%s", "\t\t\t<Constant>", cValeur, "</Constant>\r");
				// Effacement du fichier Bin
				if (remove((const char *)strS7KRecordXML->strTabRTHXML[iVarRTH].cNomPathFileBin) !=0)
				{
				   printf("Cannot remove file %s (%s)\n",
							   strS7KRecordXML->strTabRTHXML[iVarRTH].cNomPathFileBin, ERRORSTRING);
				}
				free(cValeur);
				cValeur=NULL;
			}
			else
			{
				sprintf(cNomFicBin, "%s%s",strS7KRecordXML->strTabRTHXML[iVarRTH].cNom, ".bin");
				fprintf(fpXML, "%s%s%s", "\t\t\t<FileName>", cNomFicBin, "</FileName>\r");
			}
			fprintf(fpXML, "%s", "\t\t</item>\r");
			free(cTypeMatLab);

		} // Fin de la boucle sur les donn�es RTH
    	fprintf(fpXML, "%s", "\t</Variables>\r");
    	free(strS7KRecordXML->strTabRTHXML);
    } // Fin de l'�criture des Signaux

    // Donn�e de Type RD.
    if (strS7KRecordXML->sNbVarRD > 0)
    {
    	fprintf(fpXML, "%s", "\t<Tables>\r");
		// Traitement sur l'ensemble des variables.
		for (iVarRD=0; iVarRD<strS7KRecordXML->sNbVarRD; iVarRD++)
		{
			lDummy = ftello64(strS7KRecordXML->strTabRDXML[iVarRD].ptr_fpBin);

			// Fermeture pr�alable du fichier.
			fclose(strS7KRecordXML->strTabRDXML[iVarRD].ptr_fpBin);

			// Si le fichier est vide : pas de traitement (cas du 7012) avec variables conditionnelles
			// ou le 7057 selon le type de la variable.
			if (lDummy == 0)
			{
				// Effacement du fichier
				if (remove((const char *)strS7KRecordXML->strTabRDXML[iVarRD].cNomPathFileBin) !=0)
				{
				   printf("Cannot remove file %s (%s)\n",
							   strS7KRecordXML->strTabRDXML[iVarRD].cNomPathFileBin, ERRORSTRING);
				}
			}
			else
			{

				cTypeMatLab = (char*)malloc(10*sizeof(char));
				iRet = util_cConvertTypeLabel(	strS7KRecordXML->strTabRDXML[iVarRD].cType,
												cTypeMatLab);
				usNbElem = strS7KRecordXML->strTabRDXML[iVarRD].usNbElem;
				fprintf(fpXML, "%s", "\t\t<item>\r");
				fprintf(fpXML, "%s%s%s", "\t\t\t<Name>", strS7KRecordXML->strTabRDXML[iVarRD].cNom, "</Name>\r");
				fprintf(fpXML, "%s%s%s", "\t\t\t<Storage>", cTypeMatLab, "</Storage>\r");
				if (strS7KRecordXML->strTabRDXML[iVarRD].usNbElem > 1)
				{
					sprintf(cNbElem, "%d", strS7KRecordXML->strTabRDXML[iVarRD].usNbElem);
					fprintf(fpXML, "%s%s%s", "\t\t\t<NbElem>", cNbElem, "</NbElem>\r");
				}
				// fprintf(fpXML, "%s%s%s", "\t\t\t<Unit>", strS7KRecordXML->strTabRDXML[iVarRD].cUnit, "</Unit>\r");
				// fprintf(fpXML, "%s%s%s", "\t\t\t<Tag>", strS7KRecordXML->strTabRDXML[iVarRD].cTag, "</Tag>\r");
				if (strS7KRecordXML->ptr_cNomIndexTable != NULL)
				{
					fprintf(fpXML, "%s%s%s", "\t\t\t<Indexation>", strS7KRecordXML->ptr_cNomIndexTable, "</Indexation>\r");
				}
				if (strcmp(strS7KRecordXML->strTabRDXML[iVarRD].cNom, "DeviceInfo"))
				{
					if (strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant == TRUE)
					{
						if(cValeur==NULL)
						{
							// Allocation de la valeur � r�cup�rer selon la taille. Si elle est
							// du type Chaine, elle doit contenir autant d'�l�ments que d�clar�s.
							if (strcmp(cTypeMatLab, "char"))
							{
								cValeur = (char*)malloc(256*sizeof(char));
							}
							else
							{
								cValeur = (char*)malloc(usNbElem*sizeof(char));
							}
						}
	//					iRet = utilS7K_cValVarFromFile(strS7KRecordXML->strTabRDXML[iVarRD],
	//												cValeur);
						iRet = util_cValVarFromFile(strS7KRecordXML->strTabRDXML[iVarRD].cNomPathFileBin,
													strS7KRecordXML->strTabRDXML[iVarRD].cType,
													strS7KRecordXML->strTabRDXML[iVarRD].usNbElem,
													cValeur);
						fprintf(fpXML, "%s%s%s", "\t\t\t<Constant>", cValeur, "</Constant>\r");
						free(cValeur);
						cValeur=NULL;
						// Effacement du fichier
						if (remove((const char *)strS7KRecordXML->strTabRDXML[iVarRD].cNomPathFileBin) !=0)
						{
						   printf("Cannot remove file %s (%s)\n",
									   strS7KRecordXML->strTabRDXML[iVarRD].cNomPathFileBin, ERRORSTRING);
						}
					}
					else
					{
						sprintf(cNomFicBin, "%s%s",strS7KRecordXML->strTabRDXML[iVarRD].cNom, ".bin");
						fprintf(fpXML, "%s%s%s", "\t\t\t<FileName>", cNomFicBin, "</FileName>\r");
					}
				}
				else // Traitement de la variable DeviceInfo
				{
					iSize = strlen(cRepData) + strlen(strS7KRecordXML->ptr_cNomHeader) + 100;
					cNomFicXMLDeviceInfo = calloc(iSize, sizeof(char));

					// iVarRD-1 d�signe DeviceInfoLength pour le paquet 7001, 7021 et 7028
					// Affectation des attributs de la structure XML
					usNbElem = strS7KRecordXML->strTabRDXML[iVarRD-1].usNbElem;
					if(cValeur==NULL)
					{
						// Allocation de la valeur � r�cup�rer selon la taille. Si elle est
						// du type Chaine, elle doit contenir autant d'�l�ments que d�clar�s.
						if (strcmp(cTypeMatLab, "char"))
						{
							cValeur = (char*)malloc(256*sizeof(char));
						}
						else
						{
							cValeur = (char*)malloc(usNbElem*sizeof(char));
						}
					}

					iRet = util_cValVarConstant(strS7KRecordXML->strTabRDXML[iVarRD-1].cType,
												strS7KRecordXML->strTabRDXML[iVarRD-1].ptr_vPrevVal,
												(int)strS7KRecordXML->strTabRDXML[iVarRD-1].usNbElem,
												cValeur);
					usNbElem = (unsigned short)atoi(cValeur);
					//free(cValeur);
					cValeur=NULL;

					cTypeMatLab = (char*)malloc(10*sizeof(char));
					iRet = util_cConvertTypeLabel(	strS7KRecordXML->strTabRDXML[iVarRD].cType,
													cTypeMatLab);
					usTailleEnByte = 1;
					strS7KRecordXML->strTabRDXML[iVarRD].usNbElem = usNbElem;

					sprintf(cNbElem, "%d", strS7KRecordXML->strTabRDXML[iVarRD].usNbElem);

					// Augmentation du nom de fichier XML de sauvegarde de la variable DeviceInfo.
					sprintf(cNomFicXMLDeviceInfo, "S7K_%s\\%s%s", strS7KRecordXML->ptr_cNomHeader, strS7KRecordXML->strTabRDXML[iVarRD].cNom, ".xml");
					fprintf(fpXML, "%s%s%s", "\t\t\t<FileName> S7K_%s\\%s%s </FileName>\r", strS7KRecordXML->ptr_cNomHeader, strS7KRecordXML->strTabRDXML[iVarRD].cNom, ".xml");
					// Attribution du nom avec le r�pertoire  temporaire
					sprintf(cNomFicXMLDeviceInfo, "%s%s\\S7K_%s\\%s%s", cRepData, "_tmp", strS7KRecordXML->ptr_cNomHeader, strS7KRecordXML->strTabRDXML[iVarRD].cNom, ".xml");

					// Ecriture sp�ciale du contenu de la variable DeviceInfo
					fpDeviceInfoXML = fopen(cNomFicXMLDeviceInfo, "w+");
					if(cValeur == NULL)
					{
						cValeur = (char*)malloc((usNbElem*usTailleEnByte)*sizeof(char));
					}
					iRet = util_cValVarConstant(strS7KRecordXML->strTabRDXML[iVarRD].cType,
												strS7KRecordXML->strTabRDXML[iVarRD].ptr_vPrevVal,
												(int)usNbElem,
												cValeur);
					iFlag = fprintf(fpDeviceInfoXML, "%s", cValeur);
					fclose(fpDeviceInfoXML);
					free(cNomFicXMLDeviceInfo);
					free(cValeur);
					cValeur=NULL;

					// Effacement du fichier
					if (remove((const char *)strS7KRecordXML->strTabRDXML[iVarRD].cNomPathFileBin) !=0)
					{
					   printf("Cannot remove file %s (%s)\n",
								   strS7KRecordXML->strTabRDXML[iVarRD].cNomPathFileBin, ERRORSTRING);
					}
				}
				fprintf(fpXML, "%s", "\t\t</item>\r");
				free(cTypeMatLab);

			} // Fin du test sur fichier vide.


		} // Fin de la boucle sur les donn�es RD

		// Traitement sur l'ensemble des attributs.
		if (strS7KRecordXML->uiODOffset != 0)
		{
			for (iVarOD=0; iVarOD<strS7KRecordXML->sNbVarOD; iVarOD++)
			{
				lDummy = ftello64(strS7KRecordXML->strTabODXML[iVarOD].ptr_fpBin);

				// Fermeture pr�alable du fichier.
				fclose(strS7KRecordXML->strTabODXML[iVarOD].ptr_fpBin);

				// Si le fichier est vide : pas de traitement de la variables (cas de donn�es optionnelles
				// avec NbBeams = 0, fichier 20080713_110618_PP-7111.s7K paquet 7058 .
				if (lDummy == 0)
				{
					// Effacement du fichier
					if (remove((const char *)strS7KRecordXML->strTabODXML[iVarOD].cNomPathFileBin) !=0)
					{
					   printf("Cannot remove file %s (%s)\n",
								   strS7KRecordXML->strTabODXML[iVarOD].cNomPathFileBin, ERRORSTRING);
					}
				}
				else
				{
					cTypeMatLab = (char*)malloc(10*sizeof(char));
					iRet = util_cConvertTypeLabel(	strS7KRecordXML->strTabODXML[iVarOD].cType,
													cTypeMatLab);
					sprintf(cNbElem, "%d", strS7KRecordXML->strTabODXML[iVarOD].usNbElem);

					fprintf(fpXML, "%s", "\t\t<item>\r");
					fprintf(fpXML, "%s%s%s", "\t\t\t<Name>", strS7KRecordXML->strTabODXML[iVarOD].cNom, "</Name>\r");
					fprintf(fpXML, "%s%s%s", "\t\t\t<Storage>", cTypeMatLab, "</Storage>\r");
					usNbElem = strS7KRecordXML->strTabODXML[iVarOD].usNbElem;
					if (strS7KRecordXML->strTabODXML[iVarOD].usNbElem> 1)
					{
						sprintf(cNbElem, "%d", strS7KRecordXML->strTabODXML[iVarOD].usNbElem);
						fprintf(fpXML, "%s%s%s", "\t\t\t<NbElem>", cNbElem, "</NbElem>\r");
					}
					// fprintf(fpXML, "%s%s%s", "\t\t\t<Unit>", strS7KRecordXML->strTabODXML[iVarOD].cUnit, "</Unit>\r");
					// fprintf(fpXML, "%s%s%s", "\t\t\t<Tag>", strS7KRecordXML->strTabODXML[iVarOD].cTag, "</Tag>\r");
					if (strS7KRecordXML->ptr_cNomIndexTable != NULL)
					{
						fprintf(fpXML, "%s%s%s", "\t\t\t<Indexation>", strS7KRecordXML->ptr_cNomIndexTable, "</Indexation>\r");
					}
					usNbElem = strS7KRecordXML->strTabODXML[iVarOD].usNbElem;
					usTailleEnByte = util_usNbOctetTypeVar(strS7KRecordXML->strTabODXML[iVarOD].cType);
					if (strS7KRecordXML->strTabODXML[iVarOD].bFlagConstant == TRUE)
					{
						if(cValeur==NULL)
						{
							// Allocation de la valeur � r�cup�rer selon la taille. Si elle est
							// du type Chaine, elle doit contenir autant d'�l�ments que d�clar�s.
							if (strcmp(cTypeMatLab, "char"))
							{
								cValeur = (char*)malloc(256*sizeof(char));
							}
							else
							{
								cValeur = (char*)malloc(usNbElem*sizeof(char));
							}
						}
	//					iRet = utilS7K_cValVarFromFile(strS7KRecordXML->strTabODXML[iVarOD],
	//												cValeur);
						iRet = util_cValVarFromFile(strS7KRecordXML->strTabODXML[iVarOD].cNomPathFileBin,
													strS7KRecordXML->strTabODXML[iVarOD].cType,
													strS7KRecordXML->strTabODXML[iVarOD].usNbElem,
													cValeur);
						fprintf(fpXML, "%s%s%s", "\t\t\t<Constant>", cValeur, "</Constant>\r");
						// Effacement du fichier
						if (remove((const char *)strS7KRecordXML->strTabODXML[iVarOD].cNomPathFileBin) !=0)
						{
						   printf("Cannot remove file %s (%s)\n",
									   strS7KRecordXML->strTabODXML[iVarOD].cNomPathFileBin, ERRORSTRING);
						}
						free(cValeur);
						cValeur=NULL;
					}
					else
					{
						sprintf(cNomFicBin, "%s%s",strS7KRecordXML->strTabODXML[iVarOD].cNom, ".bin");
						fprintf(fpXML, "%s%s%s", "\t\t\t<FileName>", cNomFicBin, "</FileName>\r");
					}
					fprintf(fpXML, "%s", "\t\t</item>\r");
					free(cTypeMatLab);

				}	// Fin du traitement conditionnel sur le fichier.
			} // Fin de la boucle sur les donn�es OD
			free(strS7KRecordXML->strTabODXML);
		}
		fprintf(fpXML, "%s", "\t</Tables>\r");
		// Lib�ration des Tables de type RD
    	free(strS7KRecordXML->strTabRDXML);

    } // Fin de l'�criture des Tables (RD et OD)

	fprintf(fpXML, "%s", "</ROOT>\r");
	fclose(fpXML);

	free(cNomFicXML);
	free(strS7KRecordXML->ptr_cLabelHeader);
	free(strS7KRecordXML->ptr_cNomHeader); // ptr_cNomHeader est du coup lib�r�.

	if (iRet != 0)
		return EXIT_FAILURE;
	else
		return EXIT_SUCCESS;


} //utilS7K_iSaveAndCloseFiles

//----------------------------------------------------------------
// Fonction :	utilS7K_uiIdentifyDevice
// Objet 	:	Identification du Device dans un tableau de mod�les
//				pr�d�finis.
// Modification : 16/09/11
// Auteur 	: GLU
//----------------------------------------------------------------
int utilS7K_uiIdentifyDevice(	unsigned int uiDeviceIdentifier,
								int *iFlagDeviceIdentify)
{
	unsigned int 	uiTabDeviceToIdentify[3] = {7111, 7125, 7150};
	int 			iLoop;

	if (*iFlagDeviceIdentify == 0)
	{
		// Recherche du type de sondeur par recherche d'occurrence dans le nom
		// du fichier.
		for (iLoop = 0; iLoop < 3; iLoop++)
		{
			  if (uiDeviceIdentifier == uiTabDeviceToIdentify[iLoop])
			  {
				  *iFlagDeviceIdentify = 1;
				  break;
			  }
		}
	}
	return 0;


}

//////////////////////////////////////////////////////////////////////
// Fonction		:	utilS7K_iCreateDirDatagram
// Objet		:	Cr�ation du r�pertoire d�di�es aux donn�es d'un
//					Datagramme
// Modification :	29/09/09
// Auteur		:	GLU
//////////////////////////////////////////////////////////////////////
int utilS7K_iCreateDirDatagram(char *cRepData,
							char *cNomDatagram,
							char *cRepertoire)
{

int 	iErr;


	sprintf(cRepertoire, "%s%s\\S7K_%s", cRepData, "_tmp", cNomDatagram);
	if (DEBUG)
	   printf("-- Directory Creation : %s\n",cRepertoire);

	// Cr�ation du r�pertoire
	iErr = mkdir(cRepertoire);
	if (iErr != 0)
	{
	   switch (errno)
	   {
		   //"File exists"
		   case EEXIST:
			   break;
		   default:
			   printf("Error in Directory creation %s\n", strerror(errno));
			   return EXIT_FAILURE;
			   break;
	   }
	}

	return EXIT_SUCCESS;

} // utilS7K_iCreateDirDatagram

//////////////////////////////////////////////////////////////////////
// Fonction		:	utilS7K_initXMLRecord
// Objet		:	Initialisation de la structure XML des signaux et images
//					du fichier.
// Modification :	13/05/2011
// Auteur		:	GLU
//////////////////////////////////////////////////////////////////////
int utilS7K_initXMLRecord(	T_DESC_TAB_SIGANDIMG strDescTabSigOrImg,
							char *cTitleDatagram,
							char *cNomDatagram,
							char *cComments,
							T_S7KRecordXML *strS7KRecordXML)

{
	char	*ptr_cRepertoire,
			cNomFicPathBin[300];

    int 	iSizeRep,
			iRet,
			iLen,
			iVarRTH,
			iVarRD,
			iVarOD;

	unsigned short 	usTaille,
					usNbVarRTH,
					usNbVarRD,
					usNbVarOD;

	short			sNumIndex;


	sNumIndex 		= strDescTabSigOrImg.sNumIndexTable;
	usNbVarRTH		= strDescTabSigOrImg.usNbVarSignal ;
	usNbVarRD		= strDescTabSigOrImg.usNbVarImage;
	usNbVarOD		= strDescTabSigOrImg.usNbVarImage2;

	// Initialisation des structures de pilotage des fichiers
	// XML et Bin .
	strS7KRecordXML->sNbVarRTH 			= usNbVarRTH;
	strS7KRecordXML->sNbVarRD 			= usNbVarRD;
	strS7KRecordXML->sNbVarOD 			= usNbVarOD;
    strS7KRecordXML->strTabRTHXML		= (T_S7KVarXML *)malloc(strS7KRecordXML->sNbVarRTH*sizeof(T_S7KVarXML));
    strS7KRecordXML->strTabRDXML		= (T_S7KVarXML *)malloc(strS7KRecordXML->sNbVarRD*sizeof(T_S7KVarXML));
    strS7KRecordXML->strTabODXML		= (T_S7KVarXML *)malloc(strS7KRecordXML->sNbVarOD*sizeof(T_S7KVarXML));

	iLen = strlen(cNomDatagram)+1;
	strS7KRecordXML->ptr_cNomHeader = malloc(iLen*sizeof(char));
	sprintf(strS7KRecordXML->ptr_cNomHeader, "%s", cNomDatagram);

	iLen = strlen(cTitleDatagram)+1;
	strS7KRecordXML->ptr_cLabelHeader = malloc(iLen*sizeof(char));
	sprintf(strS7KRecordXML->ptr_cLabelHeader, "%s", cTitleDatagram);

	iLen = strlen(cComments)+1;
	strS7KRecordXML->ptr_cCommentsHeader = malloc(iLen*sizeof(char));
	sprintf(strS7KRecordXML->ptr_cCommentsHeader, "%s", cComments);

	// Remplissage du nom de la variable d'indexation des tables.
	if (sNumIndex != -1)
	{
		iLen = strlen(strDescTabSigOrImg.strDescSignal[sNumIndex].cTabNomVar)+1;
		strS7KRecordXML->ptr_cNomIndexTable = malloc(iLen*sizeof(char));
		sprintf(strS7KRecordXML->ptr_cNomIndexTable, "%s", strDescTabSigOrImg.strDescSignal[sNumIndex].cTabNomVar);
	}
	else
	{
		iLen = strlen("NoneIndex")+1;
		strS7KRecordXML->ptr_cNomIndexTable = malloc(iLen*sizeof(char));
		sprintf(strS7KRecordXML->ptr_cNomIndexTable, "%s", "NoneIndex");
	}
	// Affectation et cr�ation du repertoire du Datagramme.
	iSizeRep = ( strlen(G_cRepData) + strlen("_tmp")+ strlen( cNomDatagram));
	// Ajout d'une marge au cas o� certains caract�res soient doubl�s soient doubl�s.
	ptr_cRepertoire = malloc( (iSizeRep + 1 + 300 + 4)*sizeof(char));
	iRet = utilS7K_iCreateDirDatagram(	G_cRepData,
										strS7KRecordXML->ptr_cNomHeader,
										ptr_cRepertoire);

	// Initialisation g�n�rale des variables de type Signal
	for (iVarRTH=0; iVarRTH<strS7KRecordXML->sNbVarRTH; iVarRTH++) {
	   strS7KRecordXML->strTabRTHXML[iVarRTH].ptr_fpBin = 0;
	   strS7KRecordXML->strTabRTHXML[iVarRTH].bFlagConstant = TRUE;
	   strS7KRecordXML->strTabRTHXML[iVarRTH].usNbElem = 1;
	   sprintf(strS7KRecordXML->strTabRTHXML[iVarRTH].cNom, "%s", strDescTabSigOrImg.strDescSignal[iVarRTH].cTabNomVar);
	   sprintf(strS7KRecordXML->strTabRTHXML[iVarRTH].cType, "%s", strDescTabSigOrImg.strDescSignal[iVarRTH].cTabNomType);
	   sprintf(strS7KRecordXML->strTabRTHXML[iVarRTH].cTag, "%s", "TODO");
	   sprintf(strS7KRecordXML->strTabRTHXML[iVarRTH].cUnit, "%s", "TODO");
	   strS7KRecordXML->strTabRTHXML[iVarRTH].fScaleFactor = strDescTabSigOrImg.strDescSignal[iVarRTH].fScaleFactor;
	   strS7KRecordXML->strTabRTHXML[iVarRTH].fAddOffset = strDescTabSigOrImg.strDescSignal[iVarRTH].fAddOffset;
	   usTaille = util_usNbOctetTypeVar(strDescTabSigOrImg.strDescSignal[iVarRTH].cTabNomType);
	   strS7KRecordXML->strTabRTHXML[iVarRTH].usSize = usTaille;
	   iLen = strlen(strS7KRecordXML->strTabRTHXML[iVarRTH].cNom);
	   // Ajout d'une marge au cas o� certains caract�res soient doubl�s soient doubl�s.
	   sprintf(cNomFicPathBin, "%s\\%s%s", ptr_cRepertoire, strS7KRecordXML->strTabRTHXML[iVarRTH].cNom, ".bin");
	   sprintf(strS7KRecordXML->strTabRTHXML[iVarRTH].cNomPathFileBin, "%s", cNomFicPathBin);
	   strS7KRecordXML->strTabRTHXML[iVarRTH].ptr_fpBin = fopen(cNomFicPathBin, "w+b");
	   if (!strS7KRecordXML->strTabRTHXML[iVarRTH].ptr_fpBin)
	   {
		   printf("%s -- Error in Signal creation  : %s\n", __FILE__, cNomFicPathBin);
		   return EXIT_FAILURE;
	   }
	}

	// Initialisation g�n�rale des variables de type Image
	for (iVarRD=0; iVarRD<strS7KRecordXML->sNbVarRD; iVarRD++)
	{
	   strS7KRecordXML->strTabRDXML[iVarRD].ptr_fpBin = 0;
	   strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant = TRUE;
	   strS7KRecordXML->strTabRDXML[iVarRD].usNbElem = 1;
	   sprintf(strS7KRecordXML->strTabRDXML[iVarRD].cNom, "%s", strDescTabSigOrImg.strDescImage[iVarRD].cTabNomVar);
	   sprintf(strS7KRecordXML->strTabRDXML[iVarRD].cType, "%s", strDescTabSigOrImg.strDescImage[iVarRD].cTabNomType);
	   sprintf(strS7KRecordXML->strTabRDXML[iVarRD].cTag, "%s", "TODO");
	   sprintf(strS7KRecordXML->strTabRDXML[iVarRD].cUnit, "%s", "TODO");
	   strS7KRecordXML->strTabRDXML[iVarRD].fScaleFactor 	= strDescTabSigOrImg.strDescImage[iVarRD].fScaleFactor;
	   strS7KRecordXML->strTabRDXML[iVarRD].fAddOffset 		= strDescTabSigOrImg.strDescImage[iVarRD].fAddOffset;
	   usTaille = util_usNbOctetTypeVar(strDescTabSigOrImg.strDescImage[iVarRD].cTabNomType);
	   strS7KRecordXML->strTabRDXML[iVarRD].usSize = usTaille;
	   iLen = strlen(strS7KRecordXML->strTabRDXML[iVarRD].cNom);
	   // Ajout d'une marge au cas o� certains caract�res soient doubl�s soient doubl�s.
	   sprintf(cNomFicPathBin, "%s\\%s%s", ptr_cRepertoire, strS7KRecordXML->strTabRDXML[iVarRD].cNom, ".bin");
	   sprintf(strS7KRecordXML->strTabRDXML[iVarRD].cNomPathFileBin, "%s", cNomFicPathBin);
	   strS7KRecordXML->strTabRDXML[iVarRD].ptr_fpBin = fopen(cNomFicPathBin, "w+b");
	   if (!strS7KRecordXML->strTabRDXML[iVarRD].ptr_fpBin)
	   {
		   printf("%s -- Error in Image creation  : %s\n", __FILE__, cNomFicPathBin);
		   return EXIT_FAILURE;
	   }
	}

	for (iVarOD=0; iVarOD<strS7KRecordXML->sNbVarOD; iVarOD++)
	{
		strS7KRecordXML->strTabODXML[iVarOD].ptr_fpBin = 0;
		strS7KRecordXML->strTabODXML[iVarOD].bFlagConstant = TRUE;
		strS7KRecordXML->strTabODXML[iVarOD].usNbElem = 1;
		sprintf(strS7KRecordXML->strTabODXML[iVarOD].cNom, "%s", strDescTabSigOrImg.strDescImage2[iVarOD].cTabNomVar);
		sprintf(strS7KRecordXML->strTabODXML[iVarOD].cType, "%s", strDescTabSigOrImg.strDescImage2[iVarOD].cTabNomType);
		sprintf(strS7KRecordXML->strTabODXML[iVarOD].cTag, "%s", "TODO");
		sprintf(strS7KRecordXML->strTabODXML[iVarOD].cUnit, "%s", "TODO");
	    strS7KRecordXML->strTabODXML[iVarOD].fScaleFactor 	= strDescTabSigOrImg.strDescImage2[iVarOD].fScaleFactor;
	    strS7KRecordXML->strTabODXML[iVarOD].fAddOffset 	= strDescTabSigOrImg.strDescImage2[iVarOD].fAddOffset;
		usTaille = util_usNbOctetTypeVar(strDescTabSigOrImg.strDescImage2[iVarOD].cTabNomType);
		strS7KRecordXML->strTabODXML[iVarOD].usSize = usTaille;
		// Ajout d'une marge au cas o� certains caract�res soient doubl�s soient doubl�s.
		sprintf(cNomFicPathBin, "%s\\%s%s", ptr_cRepertoire, strS7KRecordXML->strTabODXML[iVarOD].cNom, ".bin");
		sprintf(strS7KRecordXML->strTabODXML[iVarOD].cNomPathFileBin, "%s", cNomFicPathBin);
		strS7KRecordXML->strTabODXML[iVarOD].ptr_fpBin = fopen(cNomFicPathBin, "w+b");
		if (!strS7KRecordXML->strTabODXML[iVarOD].ptr_fpBin)
		{
		   printf("%s -- Error in Signal Creation File  : %s\n", __FILE__, cNomFicPathBin);
		   return EXIT_FAILURE;
		}
	}
	free(ptr_cRepertoire);
	return EXIT_SUCCESS;

} // utilS7K_initXMLRecord

#pragma pack()

