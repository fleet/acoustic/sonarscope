/*
 * S7K_Utilities.h
 *
 *  Created on: 9 oct. 2008
 *      Author: rgallou
 */
#pragma pack(1)

#ifndef S7K_UTILITIES_H_
#define S7K_UTILITIES_H_

#include "S7K_Datagrams.h"


int utilS7K_iSaveAndCloseFiles(char *cRepData,
							T_S7KRecordXML *strS7KRecordXML);

int utilS7K_uiIdentifyDevice(	unsigned int uiDeviceIdentifier,
								int *iFlagDeviceIdentify);


int utilS7K_initXMLRecord(	T_DESC_TAB_SIGANDIMG strDescTabSigOrImg,
							char *cTitleDatagram,
							char *cNomDatagram,
							char *cComments,
							T_S7KRecordXML *strS7KRecordXML);

int utilS7K_iCreateDirDatagram(char *cRepData,
							char *cNomDatagram,
							char *cRepertoire);
#endif // S7K_UTILITIES_H_

#pragma pack()
