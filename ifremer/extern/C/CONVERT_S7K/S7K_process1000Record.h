/*
 * S7K_process1000Record.h
 *
 *  Created on: 13 oct. 2008
 *      Author: rgallou
 */

#ifndef S7K_PROCESS1000RECORD_H_
#define S7K_PROCESS1000RECORD_H_

#include "S7K_DATAGRAMS.H"

typedef struct {
	f32	VehicleXReferencePoint2COG;
	f32	VehicleYReferencePoint2COG;
	f32	VehicleZReferencePoint2COG;
	f32	WaterLevel2COG;
} T_1000_RECORD_RTH;


int iProcess1000Record(FILE *G_fpData, int iNbRecord,
						u32 uiODOffset,
						T_S7KRecordXML *strS7KRecordXML,
						T_1000_RECORD_RTH *RTH_prev1000Record);

int iInitXMLBin1000Record(	char *cTitleDatagram,
							char *cNomDatagram, char *cComments,
							u32 uiODOffset,
							T_S7KRecordXML *strS7KRecordXML);
#pragma pack()

#endif /* S7K_PROCESS1000RECORD_H_ */
