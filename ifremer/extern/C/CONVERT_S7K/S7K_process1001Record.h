/*
 * S7K_process1001Record.h
 *
 *  Created on: 13 oct. 2008
 *      Author: rgallou
 */

#ifndef S7K_PROCESS1001RECORD_H_
#define S7K_PROCESS1001RECORD_H_
#pragma pack(1)

#include "S7K_DATAGRAMS.H"

typedef struct {
	f32	SensorPositionXOffset;
	f32	SensorPositionYOffset;
	f32	SensorPositionZOffset;
	f32	SensorPositionRollAngleOffset;
	f32	SensorPositionPitchAngleOffset;
	f32	SensorPositionYawAngleOffset;
} T_1001_RECORD_RTH;


int iProcess1001Record(FILE *G_fpData, int iNbRecord,
						u32 uiODOffset,
						T_S7KRecordXML *strS7KRecordXML,
						T_1001_RECORD_RTH *RTH_prev1001Record);

int iInitXMLBin1001Record(	char *cTitleDatagram,
							char *cNomDatagram, char *cComments,
							u32 uiODOffset,
							T_S7KRecordXML *strS7KRecordXML);
#pragma pack()
#endif /* S7K_PROCESS1001RECORD_H_ */
