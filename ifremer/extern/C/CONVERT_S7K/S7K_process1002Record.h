/*
 * S7K_process1002Record.h
 *
 *  Created on: 13 oct. 2008
 *      Author: rgallou
 */

#ifndef S7K_PROCESS1002RECORD_H_
#define S7K_PROCESS1002RECORD_H_
#pragma pack(1)

#include "S7K_DATAGRAMS.H"

typedef struct {
	f32	SensorPositionXOffset;
	f32	SensorPositionYOffset;
	f32	SensorPositionZOffset;
	f32	SensorPositionRollAngleOffset;
	f32	SensorPositionPitchAngleOffset;
	f32	SensorPositionYawAngleOffset;
} T_1002_RECORD_RTH;


int iProcess1002Record(FILE *G_fpData, int iNbRecord,
						u32 uiODOffset,
						T_S7KRecordXML *strS7KRecordXML,
						T_1002_RECORD_RTH *RTH_prev1002Record);

int iInitXMLBin1002Record(	char *cTitleDatagram,
							char *cNomDatagram, char *cComments,
							u32 uiODOffset,
							T_S7KRecordXML *strS7KRecordXML);
#pragma pack()
#endif /* S7K_PROCESS1002RECORD_H_ */
