/*
 * S7K_process1003Record.h
 *
 *  Created on: 13 oct. 2008
 *      Author: rgallou
 */

#ifndef S7K_PROCESS1003RECORD_H_
#define S7K_PROCESS1003RECORD_H_
#pragma pack(1)
#include "S7K_DATAGRAMS.H"

typedef struct {
	u32	DatumIdentifier;
	f32	Latency;
	f64	LatitudeOrNorthing;
	f64	LongitudeOrEasting;
	//f64	HeightRelativeToDatumOrHeight;
	u8	PositionTypeFlag;
	u8	UtmZone;
	u8	QualityFlag;
	u8	PositioningMethod;
} T_1003_RECORD_RTH;


int iProcess1003Record(FILE *G_fpData, int iNbRecord,
						u32 uiODOffset,
						T_S7KRecordXML *strS7KRecordXML,
						T_1003_RECORD_RTH *RTH_prev1003Record);

int iInitXMLBin1003Record(	char *cTitleDatagram,
							char *cNomDatagram, char *cComments,
							u32 uiODOffset,
							T_S7KRecordXML *strS7KRecordXML);
#pragma pack()
#endif /* S7K_PROCESS1003RECORD_H_ */
