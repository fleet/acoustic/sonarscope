/*
 * S7K_process1005Record.h
 *
 *  Created on: 13 oct. 2008
 *      Author: rgallou
 */

#ifndef S7K_PROCESS1005RECORD_H_
#define S7K_PROCESS1005RECORD_H_
#pragma pack(1)

#include "S7K_DATAGRAMS.H"

typedef struct {
	f32		Tide;
	u16		Source;
	u8		Flags;
	u16		GaugeIdentifier;
	u32		DatumIdentifier;
	f32		Latency;
	f64		LatitudeOrNorthing;
	f64		LongitudeOrEasting;
	f64		HeightRelativeToDatumOrHeight;
	u8		PositionTypeFlag;
	u8		UtmZone;
} T_1005_RECORD_RTH;


int iProcess1005Record(FILE *G_fpData, int iNbRecord,
						u32 uiODOffset,
						T_S7KRecordXML *strS7KRecordXML,
						T_1005_RECORD_RTH *RTH_prev1005Record);

int iInitXMLBin1005Record(	char *cTitleDatagram,
							char *cNomDatagram, char *cComments,
							u32 uiODOffset,
							T_S7KRecordXML *strS7KRecordXML);
#pragma pack()
#endif /* S7K_PROCESS1005RECORD_H_ */
