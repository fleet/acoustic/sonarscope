/*
 * S7K_process1006Record.h
 *
 *  Created on: 13 oct. 2008
 *      Author: rgallou
 */

#ifndef S7K_PROCESS1006RECORD_H_
#define S7K_PROCESS1006RECORD_H_
#pragma pack(1)

#include "S7K_DATAGRAMS.H"

typedef struct {
	f32	Distance;
} T_1006_RECORD_RTH;


int iProcess1006Record(FILE *G_fpData, int iNbRecord,
						u32 uiODOffset,
						T_S7KRecordXML *strS7KRecordXML,
						T_1006_RECORD_RTH *RTH_prev1006Record);

int iInitXMLBin1006Record(	char *cTitleDatagram,
							char *cNomDatagram, char *cComments,
							u32 uiODOffset,
							T_S7KRecordXML *strS7KRecordXML);
#pragma pack()
#endif /* S7K_PROCESS1006RECORD_H_ */
