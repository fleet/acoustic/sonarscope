/*
 * S7K_process1008Record.h
 *
 *  Created on: 13 oct. 2008
 *      Author: rgallou
 */

#ifndef S7K_PROCESS1008RECORD_H_
#define S7K_PROCESS1008RECORD_H_
#pragma pack(1)

#include "S7K_DATAGRAMS.H"

typedef struct {
	u8		DepthDescriptor;
	u8		CorrectionFlag;
	u16		Reserved;
	f32		Depth;
} T_1008_RECORD_RTH;


int iProcess1008Record(FILE *G_fpData, int iNbRecord,
						u32 uiODOffset,
						T_S7KRecordXML *strS7KRecordXML,
						T_1008_RECORD_RTH *RTH_prev1008Record);

int iInitXMLBin1008Record(	char *cTitleDatagram,
							char *cNomDatagram,
							char *cComments,
							u32 uiODOffset,
							T_S7KRecordXML *strS7KRecordXML);
#pragma pack()
#endif /* S7K_PROCESS1008RECORD_H_ */
