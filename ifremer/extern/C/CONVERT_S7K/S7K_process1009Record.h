/*
 * S7K_process1009Record.h
 *
 *  Created on: 13 oct. 2008
 *      Author: rgallou
 */

#ifndef S7K_PROCESS1009RECORD_H_
#define S7K_PROCESS1009RECORD_H_
#pragma pack(1)

#include "S7K_DATAGRAMS.H"

//-Paquet 1009 : partie RTH------------------------
typedef struct {
	u8		PositionFlag;
	u8		Reserved1;
	u16		Reserved2;
	f64		Latitude;
	f64		Longitude;
	u32		N;
} T_1009_RECORD_RTH;

// Paquet 1009 : partie RD
typedef struct {
	f32	*Depth;
	f32	*SoundVelocity;
} T_1009_RECORD_RD;


int iProcess1009Record(FILE *G_fpData, int iNbRecord,
						u32 uiODOffset,
						T_S7KRecordXML *strS7KRecordXML,
						T_1009_RECORD_RTH *RTH_prev1009Record);

int iInitXMLBin1009Record(	char *cTitleDatagram,
							char *cNomDatagram, char *cComments,
							u32 uiODOffset,
							T_S7KRecordXML *strS7KRecordXML);

#endif /* S7K_PROCESS1009RECORD_H_ */
