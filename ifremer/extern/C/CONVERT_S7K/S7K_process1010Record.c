/*
 * S7K_process1010Record.c
 *
 *  Created on: 13 oct. 2008
 *      Author: rgallou
 */
#pragma pack(1)

#include <sys/types.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "S7K_Datagrams.h"
#include "S7K_Utilities.h"
#include "S7K_process1010Record.h"
#include "convertFiles_S7K.h"


//----------------------------------------------------------------
// Fonction :	iProcess1010Record
// Objet 	:	Traitement du paquet 1010
// Modif.	: 	03/11/08
// Auteur 	:	GLU
//----------------------------------------------------------------
int iProcess1010Record(FILE *G_fpData, int iNbRecord,
						u32 uiODOffset,
						T_S7KRecordXML *strS7KRecordXML,
						T_1010_RECORD_RTH *RTH_prev1010Record)
{
    T_1010_RECORD_RTH 	*RTH_1010Record;


	// Pointeur pour avancer sur les variables automatiquement.
	unsigned char		*ptr_cRTHRecord;
	static void 		*ptr_vVarPrevRecord;
	void				*ptr_vVarRecord;

	BOOL		bFlagVarConstant = TRUE,
				bFlagCompare = TRUE;

    int 	iFlag,
			iVarRD,
			iVarRTH,
			iLoop,
			iRetour = 0,
			iBit;

	

    unsigned short	usTailleEnByte,
					usNbElem;

    void *ptr_vBidon; //Pointeur d'�chantillon

	// Conservation de l'information de pr�sence de donn�es Optionnal Data.
    strS7KRecordXML->uiODOffset = 0;

    // Initialisation de la structure d'identification du paquet
 	if (iNbRecord == 0)
	{
 		iInitXMLBin1010Record("CTD", "1010", "CTD", uiODOffset, strS7KRecordXML);
	}

    // Lecture du RTH du paquet
	RTH_1010Record = (T_1010_RECORD_RTH *)malloc(sizeof(T_1010_RECORD_RTH));
	iFlag = fread(RTH_1010Record, sizeof(T_1010_RECORD_RTH), 1, G_fpData);
	if (iFlag == 0)
	{
		printf("Error in reading : %s\n", G_cFileData);
		return FALSE;
	}
	ptr_cRTHRecord = (unsigned char*)RTH_1010Record;
	// Sauvegarde des donn�es des attributs.
	if (iNbRecord > 0)
	{
		ptr_vVarPrevRecord = (void *)RTH_prev1010Record;
	}
	for (iVarRTH=0; iVarRTH<(int)strS7KRecordXML->sNbVarRTH; iVarRTH++)
	{
		ptr_vVarRecord = (void *)ptr_cRTHRecord;
		if (iVarRTH==0)
		{
			usTailleEnByte = 0;
			usNbElem = 0;
		}
		else
		{
			usTailleEnByte = util_usNbOctetTypeVar(strS7KRecordXML->strTabRTHXML[iVarRTH-1].cType);
			usNbElem = strS7KRecordXML->strTabRTHXML[iVarRTH-1].usNbElem;
		}


		if (iNbRecord > 0)
		{
			bFlagVarConstant = strS7KRecordXML->strTabRTHXML[iVarRTH].bFlagConstant;
			ptr_vVarPrevRecord = ptr_vVarPrevRecord + usTailleEnByte*usNbElem;
			bFlagCompare = util_bComparePrevAndCurrentVal(strS7KRecordXML->strTabRTHXML[iVarRTH].cType,
												ptr_vVarPrevRecord,
												ptr_vVarRecord,
											   strS7KRecordXML->strTabRTHXML[iVarRTH].usNbElem);
			bFlagVarConstant = bFlagVarConstant && bFlagCompare;
			strS7KRecordXML->strTabRTHXML[iVarRTH].bFlagConstant = bFlagVarConstant;
		}
		// Ecriture sp�ciale car contenant du XML pour la variable Device Info
		// Pr�paration de l'�criture de la variable.
		usTailleEnByte = strS7KRecordXML->strTabRTHXML[iVarRTH].usSize;
		usNbElem =  strS7KRecordXML->strTabRTHXML[iVarRTH].usNbElem;
		fwrite(ptr_cRTHRecord, usTailleEnByte, usNbElem, strS7KRecordXML->strTabRTHXML[iVarRTH].ptr_fpBin);
	    strS7KRecordXML->strTabRTHXML[iVarRTH].ptr_vPrevVal = ptr_cRTHRecord;
		ptr_cRTHRecord += usTailleEnByte*usNbElem;
	}
	RTH_prev1010Record = malloc(sizeof(T_1010_RECORD_RTH));
	// Recopie en zone tampon des valeurs courantes.
	memmove(RTH_prev1010Record, RTH_1010Record, sizeof(T_1010_RECORD_RTH));

	// --------------------------------------------
 	// Traitement des paquets RD
 	// Lecture des datas les uns apr�s les autres
	for (iLoop=0; iLoop< RTH_1010Record->N; iLoop++)
	{
		// Boucle sur les variables
		for (iVarRD=0; iVarRD< strS7KRecordXML->sNbVarRD; iVarRD++)
		{
			// Lecture de la donn�e du paquet 1010 selon
			iBit = util_GetBits(RTH_1010Record->SampleContentValidity,iVarRD, 1);
			if (iBit == 1)
			{
				usTailleEnByte = strS7KRecordXML->strTabRDXML[iVarRD].usSize;
				usNbElem = strS7KRecordXML->strTabRDXML[iVarRD].usNbElem;
				ptr_vBidon = (void *)malloc((usTailleEnByte*usNbElem)*sizeof(char));
				iFlag = fread(ptr_vBidon, usTailleEnByte, usNbElem, G_fpData);
				iFlag = fwrite(ptr_vBidon, usTailleEnByte, usNbElem, strS7KRecordXML->strTabRDXML[iVarRD].ptr_fpBin);
				if (iNbRecord > 0 && strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant == TRUE)
				{
					bFlagCompare = util_bComparePrevAndCurrentVal(strS7KRecordXML->strTabRDXML[iVarRD].cType,
														strS7KRecordXML->strTabRDXML[iVarRD].ptr_vPrevVal,
														ptr_vBidon,
														usNbElem);
					strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant = bFlagCompare && strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant;
				}
				if (iNbRecord == 0)
				{
					strS7KRecordXML->strTabRDXML[iVarRD].ptr_vPrevVal = malloc(usTailleEnByte*usNbElem*sizeof(char));
				}
				memmove(strS7KRecordXML->strTabRDXML[iVarRD].ptr_vPrevVal, ptr_vBidon, usTailleEnByte*usNbElem);

				free(ptr_vBidon);
			}

		} // Fin de la boucles sur les variables.

	} // Fin de la boucles sur les �chantillons.
	iNbRecord++;

	free(RTH_1010Record);

	return iRetour;

} //iProcess1010Record


//----------------------------------------------------------------
// Fonction :	iInitXMLBin1010Record
// Objet 	:	Initialisation de la structure �quivalent au paquet
//				1010 pour la manipulation des fichiers bin et XML.
// Modification : 15/10/08
// Auteur 	: GLU
//----------------------------------------------------------------
int iInitXMLBin1010Record(	char *cTitleDatagram,
							char *cNomDatagram, char *cComments,
							u32 uiODOffset,
							T_S7KRecordXML *strS7KRecordXML)
{
	char cTabNomSignalRTH[12][40] = {
			"Frequency",
			"SoundVelocitySourceFlag",
			"SoundVelocityAlgorithm",
			"ConductivityFlag",
			"PressureFlag",
			"PositionFlag",
			"SampleContentValidity",
			"Reserved",
			"Latitude",
			"Longitude",
			"SampleRate",
			"N"};

	char cTabTypeSignalRTH[12][40] = {
			"f32",
			"u8",
			"u8",
			"u8",
			"u8",
			"u8",
			"u8",
			"u16",
			"f64",
			"f64",
			"f32",
			"u32"};

	// ScaleFactor
	float 	fTabSFSignal[12] = {
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			RAD2DEG,
			RAD2DEG,
			1.0,
			1.0};

	// AddOffset
	float 	fTabAOSignal[12] = {
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0};

	char cTabNomVarRD[5][30] = {
			"ConductivityOrSalinity",
			"WaterTemperature",
			"PressureOrDepth",
			"SoundVelocity",
			"Absorption"};

	char cTabTypeVarRD[5][20] = {
			"f32",
			"f32",
			"f32",
			"f32",
			"f32"};

	// ScaleFactor
	float 	fTabSFImage[5] = {
			1.0,
			1.0,
			1.0,
			1.0,
			1.0};

	// AddOffset
	float 	fTabAOImage[5] = {
			0.0,
			0.0,
			0.0,
			0.0,
			0.0};

	char	*ptr_cRepertoire,
			cNomFicPathBin[300];

    int 	iSizeRep,
			iErr,
			iLen,
			iVarRTH,
			iVarRD;

	unsigned short usTaille;

	// Initialisation des structures de pilotage des fichiers
	// XML et Bin .
	strS7KRecordXML->sNbVarRTH = 12;
    strS7KRecordXML->strTabRTHXML= (T_S7KVarXML *)malloc(strS7KRecordXML->sNbVarRTH*sizeof(T_S7KVarXML));
	strS7KRecordXML->sNbVarRD = 5;
	strS7KRecordXML->strTabRDXML= (T_S7KVarXML *)malloc(strS7KRecordXML->sNbVarRD*sizeof(T_S7KVarXML));
	strS7KRecordXML->sNbVarOD = 0;

	iLen = strlen(cNomDatagram)+1;
	strS7KRecordXML->ptr_cNomHeader = malloc(iLen*sizeof(char));
	sprintf(strS7KRecordXML->ptr_cNomHeader, "%s", cNomDatagram);

	iLen = strlen(cTitleDatagram)+1;
	strS7KRecordXML->ptr_cLabelHeader = malloc(iLen*sizeof(char));
	sprintf(strS7KRecordXML->ptr_cLabelHeader, "%s", cTitleDatagram);

	iLen = strlen(cComments)+1;
	strS7KRecordXML->ptr_cCommentsHeader = malloc(iLen*sizeof(char));
	sprintf(strS7KRecordXML->ptr_cCommentsHeader, "%s", cComments);

	// Remplissage du nom de la variable d'indexation des tables.
	iLen = strlen(cTabNomSignalRTH[11])+1;
	strS7KRecordXML->ptr_cNomIndexTable = malloc(iLen*sizeof(char));
	sprintf(strS7KRecordXML->ptr_cNomIndexTable, "%s", cTabNomSignalRTH[11]);

	// Affectation du nom du repertoire.
	//ptr_cNomHeader = strS7KRecordXML->ptr_cNomHeader;
	iSizeRep = ( strlen(G_cRepData) + strlen( strS7KRecordXML->ptr_cNomHeader));
	// Ajout d'une marge au cas o� certains caract�res soient doubl�s soient doubl�s.
	ptr_cRepertoire = malloc( (iSizeRep + 100)*sizeof(char));
	sprintf(ptr_cRepertoire, "%s%s\\S7K_%s", G_cRepData, "_tmp", strS7KRecordXML->ptr_cNomHeader);
	if (DEBUG)
	   printf("-- Directory Creation : %s\n", ptr_cRepertoire);
	// Cr�ation du r�pertoire
	iErr = mkdir(ptr_cRepertoire);
	if (iErr != 0)
	{
	   switch (errno)
	   {
		   //"File exists"
		   case EEXIST:
			   break;
		   default:
			   printf("Error in Directory creation %s\n", strerror(errno));
			   return EXIT_FAILURE;
			   break;
	   }
	}
	// Initialisation g�n�rale des variables du paquet RTH
	for (iVarRTH=0; iVarRTH<strS7KRecordXML->sNbVarRTH; iVarRTH++) {
	   strS7KRecordXML->strTabRTHXML[iVarRTH].bFlagConstant = TRUE;
	   strS7KRecordXML->strTabRTHXML[iVarRTH].usNbElem = 1;
	   sprintf(strS7KRecordXML->strTabRTHXML[iVarRTH].cNom, "%s", cTabNomSignalRTH[iVarRTH]);
	   sprintf(strS7KRecordXML->strTabRTHXML[iVarRTH].cType, "%s", cTabTypeSignalRTH[iVarRTH]);
	   sprintf(strS7KRecordXML->strTabRTHXML[iVarRTH].cTag, "%s", "TODO");
	   sprintf(strS7KRecordXML->strTabRTHXML[iVarRTH].cUnit, "%s", "TODO");
	   strS7KRecordXML->strTabRTHXML[iVarRTH].fScaleFactor = fTabSFSignal[iVarRTH];
	   strS7KRecordXML->strTabRTHXML[iVarRTH].fAddOffset = fTabAOSignal[iVarRTH];
	   usTaille = util_usNbOctetTypeVar(cTabTypeSignalRTH[iVarRTH]);
	   strS7KRecordXML->strTabRTHXML[iVarRTH].usNbElem = 1;
	   strS7KRecordXML->strTabRTHXML[iVarRTH].usSize = usTaille;
	   sprintf(cNomFicPathBin, "%s\\%s%s", ptr_cRepertoire, strS7KRecordXML->strTabRTHXML[iVarRTH].cNom, ".bin");
	   sprintf(strS7KRecordXML->strTabRTHXML[iVarRTH].cNomPathFileBin, "%s", cNomFicPathBin);
	   strS7KRecordXML->strTabRTHXML[iVarRTH].ptr_fpBin = fopen(cNomFicPathBin, "w+b");
	   if (!strS7KRecordXML->strTabRTHXML[iVarRTH].ptr_fpBin)
	   {
		   printf("%s -- Error in Signal Creation File  : %s\n", __FILE__, cNomFicPathBin);
		   return EXIT_FAILURE;
	   }

	}

	for (iVarRD=0; iVarRD<strS7KRecordXML->sNbVarRD; iVarRD++) {
	   strS7KRecordXML->strTabRDXML[iVarRD].ptr_fpBin = 0;
	   strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant = TRUE;
	   strS7KRecordXML->strTabRDXML[iVarRD].usNbElem = 1;
	   sprintf(strS7KRecordXML->strTabRDXML[iVarRD].cNom, "%s", cTabNomVarRD[iVarRD]);
	   sprintf(strS7KRecordXML->strTabRDXML[iVarRD].cType, "%s", cTabTypeVarRD[iVarRD]);
	   sprintf(strS7KRecordXML->strTabRDXML[iVarRD].cTag, "%s", "TODO");
	   sprintf(strS7KRecordXML->strTabRDXML[iVarRD].cUnit, "%s", "TODO");
	   strS7KRecordXML->strTabRDXML[iVarRD].fScaleFactor = fTabSFImage[iVarRD];
	   strS7KRecordXML->strTabRDXML[iVarRD].fAddOffset = fTabAOImage[iVarRD];
	   usTaille = util_usNbOctetTypeVar(cTabTypeVarRD[iVarRD]);
	   strS7KRecordXML->strTabRDXML[iVarRD].usSize = usTaille;

	   // Ajout d'une marge au cas o� certains caract�res soient doubl�s soient doubl�s.
	   sprintf(cNomFicPathBin, "%s\\%s%s", ptr_cRepertoire, strS7KRecordXML->strTabRDXML[iVarRD].cNom, ".bin");
	   strS7KRecordXML->strTabRDXML[iVarRD].ptr_fpBin = fopen(cNomFicPathBin, "w+b");
	   sprintf(strS7KRecordXML->strTabRDXML[iVarRD].cNomPathFileBin, "%s", cNomFicPathBin);
	   if (!strS7KRecordXML->strTabRDXML[iVarRD].ptr_fpBin)
	   {
		   printf("%s -- Error in Signal Creation File  : %s\n", __FILE__, cNomFicPathBin);
		   return EXIT_FAILURE;
	   }
	}

	free(ptr_cRepertoire);

	return EXIT_SUCCESS;

} // iInitXMLBin1010Record

#pragma pack()
