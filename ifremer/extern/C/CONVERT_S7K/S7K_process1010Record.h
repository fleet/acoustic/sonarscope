/*
 * S7K_process1010Record.h
 *
 *  Created on: 13 oct. 2008
 *      Author: rgallou
 */

#ifndef S7K_PROCESS1010RECORD_H_
#define S7K_PROCESS1010RECORD_H_
#pragma pack(1)

#include "S7K_DATAGRAMS.H"

//-Paquet 1010 : partie RTH------------------------
typedef struct {
	f32		Frequency;
	u8		SoundVelocitySourceFlag;
	u8		SoundVelocityAlgorithm;
	u8		ConductivityFlag;
	u8		PressureFlag;
	u8		PositionFlag;
	u8		SampleContentValidity;
	u16		Reserved;
	f64		Latitude;
	f64		Longitude;
	f32		SampleRate;
	u32		N;
} T_1010_RECORD_RTH;

// Paquet 1010 : partie RD
typedef struct {
	f32		*ConductivityOrSalinity;
	f32		*WaterTemperature;
	f32		*PressureOrDepth;
	f32		*SoundVelocity;
	f32		*Absorption;
} T_1010_RECORD_RD;


int iProcess1010Record(FILE *G_fpData, int iNbRecord,
						u32 uiODOffset,
						T_S7KRecordXML *strS7KRecordXML,
						T_1010_RECORD_RTH *RTH_prev1010Record);

int iInitXMLBin1010Record(	char *cTitleDatagram,
							char *cNomDatagram, char *cComments,
							u32 uiODOffset,
							T_S7KRecordXML *strS7KRecordXML);
#pragma pack()
#endif /* S7K_PROCESS1010RECORD_H_ */
