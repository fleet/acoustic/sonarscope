/*
 * S7K_process1011Record.h
 *
 *  Created on: 13 oct. 2008
 *      Author: rgallou
 */

#ifndef S7K_PROCESS1011RECORD_H_
#define S7K_PROCESS1011RECORD_H_
#pragma pack(1)

#include "S7K_DATAGRAMS.H"

typedef struct {
	char	SpheroidName[32];
	f64		SemiMajorAxis;
	f64		InverseFlattening;
	char	Reserved1[16];
	char	DatumName[32];
	u32		DataCalculationMethod;
	u8		NumberOfParameters;
	f64		Dx;
	f64		Dy;
	f64		Dz;
	f64		Rx;
	f64		Ry;
	f64		Rz;
	f64		Scale;
	char	Reserved2[35];
	char	GridName[32];
	u8		GridDistanceUnits;
	u8		GridAngularUnits;
	f64		LatitudeOfOrigin;
	f64		CentralMeridian;
	f64		FalseEasting;
	f64		FalseNorthing;
	f64		CentralScaleFactor;
	i32		CustomIdentifier;
	char	Reserved3[50];
} T_1011_RECORD_RTH;


int iProcess1011Record(FILE *G_fpData, int iNbRecord,
						u32 uiODOffset,
						T_S7KRecordXML *strS7KRecordXML,
						T_1011_RECORD_RTH *RTH_prev1011Record);

int iInitXMLBin1011Record(	char *cTitleDatagram,
							char *cNomDatagram, char *cComments,
							u32 uiODOffset,
							T_S7KRecordXML *strS7KRecordXML);
#pragma pack()
#endif /* S7K_PROCESS1011RECORD_H_ */
