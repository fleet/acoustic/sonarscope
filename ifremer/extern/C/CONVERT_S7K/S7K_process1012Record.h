/*
 * S7K_process1012Record.h
 *
 *  Created on: 13 oct. 2008
 *      Author: rgallou
 */

#ifndef S7K_PROCESS1012RECORD_H_
#define S7K_PROCESS1012RECORD_H_
#pragma pack(1)

#include "S7K_DATAGRAMS.H"

typedef struct {
	f32		Roll;
	f32		Pitch;
	f32		Heave;
} T_1012_RECORD_RTH;


int iProcess1012Record(FILE *G_fpData, int iNbRecord,
						u32 uiODOffset,
						T_S7KRecordXML *strS7KRecordXML,
						T_1012_RECORD_RTH *RTH_prev1012Record);

int iInitXMLBin1012Record(	char *cTitleDatagram,
							char *cNomDatagram, char *cComments,
							u32 uiODOffset,
							T_S7KRecordXML *strS7KRecordXML);
#pragma pack()
#endif /* S7K_PROCESS1012RECORD_H_ */
