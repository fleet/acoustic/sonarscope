/*
 * S7K_process1013Record.h
 *
 *  Created on: 13 oct. 2008
 *      Author: rgallou
 */

#ifndef S7K_PROCESS1013RECORD_H_
#define S7K_PROCESS1013RECORD_H_
#pragma pack(1)

#include "S7K_DATAGRAMS.H"

typedef struct {
	f32		Heading;
} T_1013_RECORD_RTH;


int iProcess1013Record(FILE *G_fpData, int iNbRecord,
						u32 uiODOffset,
						T_S7KRecordXML *strS7KRecordXML,
						T_1013_RECORD_RTH *RTH_prev1013Record);


int iInitXMLBin1013Record(	char *cTitleDatagram,
							char *cNomDatagram, char *cComments,
							u32 uiODOffset,
							T_S7KRecordXML *strS7KRecordXML);
#pragma pack()
#endif /* S7K_PROCESS1013RECORD_H_ */
