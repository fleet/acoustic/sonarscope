/*
 * S7K_process1014Record.h
 *
 *  Created on: 13 oct. 2008
 *      Author: rgallou
 */

#ifndef S7K_PROCESS1014RECORD_H_
#define S7K_PROCESS1014RECORD_H_
#pragma pack(1)

#include "S7K_DATAGRAMS.H"

//-Paquet 1014 : partie RTH------------------------
typedef struct {
	u16		WayPointcount;
	u16		PositionType;
	f32		Radius;
	char	LineName[64];
} T_1014_RECORD_RTH;

// Paquet 1014 : partie RD
typedef struct {
	f32	*LatitudeOrNorthing;
	f32	*LongitudeOrEasting;
} T_1014_RECORD_RD;


int iProcess1014Record(FILE *G_fpData, int iNbRecord,
						u32 uiODOffset,
						T_S7KRecordXML *strS7KRecordXML,
						T_1014_RECORD_RTH *RTH_prev1014Record);

int iInitXMLBin1014Record(	char *cTitleDatagram,
							char *cNomDatagram, char *cComments,
							u32 uiODOffset,
							T_S7KRecordXML *strS7KRecordXML);
#pragma pack()
#endif /* S7K_PROCESS1014RECORD_H_ */
