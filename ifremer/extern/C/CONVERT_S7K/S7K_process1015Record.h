/*
 * S7K_process1015Record.h
 *
 *  Created on: 13 oct. 2008
 *      Author: rgallou
 */

#ifndef S7K_PROCESS1015RECORD_H_
#define S7K_PROCESS1015RECORD_H_
#pragma pack(1)

#include "S7K_DATAGRAMS.H"

typedef struct {
	u8		VerticalReference;
	f64		Latitude;
	f64		Longitude;
	f32		HorizontalPositionAccuracy;
	f32		VesselHeight;
	f32		HeightAccuracy;
	f32		SpeedOverGround;
	f32		CourseOverGround;
	f32		Heading;
} T_1015_RECORD_RTH;


int iProcess1015Record(FILE *G_fpData, int iNbRecord,
						u32 uiODOffset,
						T_S7KRecordXML *strS7KRecordXML,
						T_1015_RECORD_RTH *RTH_prev1015Record);

int iInitXMLBin1015Record(	char *cTitleDatagram,
							char *cNomDatagram, char *cComments,
							u32 uiODOffset,
							T_S7KRecordXML *strS7KRecordXML);
#pragma pack()
#endif /* S7K_PROCESS1015RECORD_H_ */
