/*
 * S7K_process1016Record.h
 *
 *  Created on: 13 oct. 2008
 *      Author: rgallou
 */

#ifndef S7K_PROCESS1016RECORD_H_
#define S7K_PROCESS1016RECORD_H_
#pragma pack(1)
#include "S7K_DATAGRAMS.H"

//-Paquet 1016 : partie RTH------------------------
typedef struct {
	u8		NumberOfAttitudeDataSets;
} T_1016_RECORD_RTH;

// Paquet 1016 : partie RD
typedef struct {
	u16		TimeDifferenceFromRecordTimestamp;
	f32		Roll;
	f32		Pitch;
	f32		Heave;
	f32		Heading;
} T_1016_RECORD_RD;


int iProcess1016Record(FILE *G_fpData, int iNbRecord,
						u32 uiODOffset,
						T_S7KRecordXML *strS7KRecordXML,
						T_1016_RECORD_RTH *RTH_prev1016Record);

int iInitXMLBin1016Record(	char *cTitleDatagram,
							char *cNomDatagram, char *cComments,
							u32 uiODOffset,
							T_S7KRecordXML *strS7KRecordXML);
#pragma pack()
#endif /* S7K_PROCESS1016RECORD_H_ */
