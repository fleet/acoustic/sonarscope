/*
 * S7K_process1200Record.h
 *
 *  Created on: 13 oct. 2008
 *      Author: rgallou
 */

#ifndef S7K_PROCESS1200RECORD_H_
#define S7K_PROCESS1200RECORD_H_
#pragma pack(1)

#include "S7K_DATAGRAMS.H"

//-Paquet 1200 : partie RTH------------------------
typedef struct {
	u16		PingCounter;
	u16		NumberOfChannels;
	f32		TotalBytesOfChannelDatatoFollow;
	u32		DataType;
	char	ChannelNumber;
	char	ChannelType;
	char	DataType2;	// TODO : � valider ?????
	char	Polarity;
	char	BytesPerSamples;
	u32		Reserved1;
	u32		NumberOfSamples;
	u32		StartTime;
	u32		SampleInterval;
	f32		Range;
	f32		Voltage;
	char	Name[16];
	u16		CustomDataDescriptor;
	char	Reserved2[18];
} T_1200_RECORD_RTH;

int iProcess1200Record(FILE *G_fpData, int iNbRecord,
						u32 uiODOffset,
						T_S7KRecordXML *strS7KRecordXML,
						T_1200_RECORD_RTH *RTH_prev1200Record);

int iInitXMLBin1200Record(	char *cTitleDatagram,
							char *cNomDatagram, char *cComments,
							u32 uiODOffset,
							T_S7KRecordXML *strS7KRecordXML);
#pragma pack()
#endif /* S7K_PROCESS1200RECORD_H_ */
