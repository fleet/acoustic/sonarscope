/*
 * S7K_process2000Record.h
 *
 *  Created on: 13 oct. 2008
 *      Author: rgallou
 */

#ifndef S7K_PROCESS2000RECORD_H_
#define S7K_PROCESS2000RECORD_H_
#pragma pack(1)

#include "S7K_DATAGRAMS.H"

//-Paquet 2000 : partie RTH------------------------
typedef struct {
	f32		Heading;
	u32		FrameInDataRecords;
} T_2000_RECORD_RTH;

// Paquet 2000 : partie RD
typedef struct {
	char	*s7KTime[10];
	f64		*X;
	f64		*Y;
	f64		*Z;
	u32		*Tide;
	f32		*Height;
	f32		*Heave;
	char	*Reserved[4];
} T_2000_RECORD_RD;


int iProcess2000Record(FILE *G_fpData, int iNbRecord,
						u32 uiODOffset,
						T_S7KRecordXML *strS7KRecordXML,
						T_2000_RECORD_RTH *RTH_prev2000Record);

int iInitXMLBin2000Record(	char *cTitleDatagram,
							char *cNomDatagram, char *cComments,
							u32 uiODOffset,
							T_S7KRecordXML *strS7KRecordXML);
#pragma pack()
#endif /* S7K_PROCESS2000RECORD_H_ */
