/*
 * S7K_process7001Record.h
 *
 *  Created on: 13 oct. 2008
 *      Author: rgallou
 */

#ifndef S7K_PROCESS7001RECORD_H_
#define S7K_PROCESS7001RECORD_H_
#pragma pack(1)

//-Paquet 7001 : partie RTH------------------------
typedef struct {
	u64		SonarId;
	u32		N;
} T_7001_RECORD_RTH;

// Paquet 7001 : partie RD
typedef struct {
	u32		DeviceIdentifier;
	char	DeviceDescription[64];
	u64		DeviceSerialNumber;
	u32		DeviceInfoLength;
	char	*DeviceInfo;
} T_7001_RECORD_RD;


int iProcess7001Record(FILE *G_fpData, int iNbRecord,
						u32 uiODOffset,
						T_S7KRecordXML *strS7KRecordXML,
						T_7001_RECORD_RTH *RTH_prev7001Record);

int iInitXMLBin7001Record(	char *cTitleDatagram,
							char *cNomDatagram, char *cComments,
							T_S7KRecordXML *strS7KRecordXML);
#pragma pack()
#endif /* S7K_PROCESS7001RECORD_H_ */
