/*
 * S7K_process7002Record.h
 *
 *  Created on: 13 oct. 2008
 *      Author: rgallou
 */

#ifndef S7K_PROCESS7002RECORD_H_
#define S7K_PROCESS7002RECORD_H_
#pragma pack(1)

#include "S7K_DATAGRAMS.H"

typedef struct {
	u64		SonarId;
	u32		PingNumber;
	u32		Compression;
	f32		StartFrequency;
	f32		StopFrequency;
} T_7002_RECORD_RTH;


int iProcess7002Record(FILE *G_fpData, int iNbRecord,
						u32 uiODOffset,
						T_S7KRecordXML *strS7KRecordXML,
						T_7002_RECORD_RTH *RTH_prev7002Record);

int iInitXMLBin7002Record(	char *cTitleDatagram,
							char *cNomDatagram, char *cComments,
							u32 uiODOffset,
							T_S7KRecordXML *strS7KRecordXML);
#pragma pack()
#endif /* S7K_PROCESS7002RECORD_H_ */
