/*
 * S7K_process7004Record.h
 *
 *  Created on: 13 oct. 2008
 *      Author: rgallou
 */

#ifndef S7K_PROCESS7004RECORD_H_
#define S7K_PROCESS7004RECORD_H_
#pragma pack(1)

#include "S7K_DATAGRAMS.H"

//-Paquet 7004 : partie RTH------------------------
typedef struct {
	u64		SonarId;
	u32		N;
} T_7004_RECORD_RTH;

// Paquet 7004 : partie RD
typedef struct {
	f32		*BeamVerticalDirectionAngle;
	f32		*BeamHorizontalDirectionAngle;
	f32		*BeamWidthY_3dB;
	f32		*BeamWidthX_3dB;
} T_7004_RECORD_RD;

int iProcess7004Record(FILE *G_fpData, int iNbRecord,
						u32 uiODOffset,
						T_S7KRecordXML *strS7KRecordXML,
						T_7004_RECORD_RTH *RTH_prev7004Record);

int iInitXMLBin7004Record(	char *cTitleDatagram,
							char *cNomDatagram, char *cComments,
							u32 uiODOffset,
							T_S7KRecordXML *strS7KRecordXML);
#pragma pack()
#endif /* S7K_PROCESS7004RECORD_H_ */
