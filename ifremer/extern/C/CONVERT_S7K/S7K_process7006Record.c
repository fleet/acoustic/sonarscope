/*
 * S7K_process7006Record.c
 *
 *  Created on: 13 oct. 2008
 *      Author: rgallou
 */
#pragma pack(1)

#include <sys/types.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "S7K_Datagrams.h"
#include "S7K_Utilities.h"
#include "S7K_process7006Record.h"
#include "convertFiles_S7K.h"


//----------------------------------------------------------------
// Fonction :	iProcess7006Record
// Objet 	:	Traitement du paquet 7006
// Modif.	: 	03/11/08
// Auteur 	:	GLU
//----------------------------------------------------------------
int iProcess7006Record(FILE *G_fpData, int iNbRecord,
						u32 uiODOffset,
						T_S7KRecordXML *strS7KRecordXML,
						T_7006_RECORD_RTH *RTH_prev7006Record)
{
    T_7006_RECORD_RTH 	*RTH_7006Record;
    T_7006_RECORD_RD 	RD_7006Record;
    T_7006_RECORD_OD 	OD_7006Record;

	// Pointeur pour avancer sur les variables automatiquement.
	unsigned char		*ptr_cRTHRecord;
	static void 		*ptr_vVarPrevRecord;
	void				*ptr_vVarRecord;

	BOOL		bFlagVarConstant = TRUE,
				bFlagCompare = TRUE;

    int 	iFlag,
			iVarRD,
			iVarRTH,
			iVarOD,
			iLoop,
			iRetour = 0,
			iNbBytesBeforeOD;



    unsigned short	usTailleEnByte,
					usNbElem;

    // Conservation de l'information de pr�sence de donn�es Optionnal Data.
    strS7KRecordXML->uiODOffset = uiODOffset;

    // Initialisation de la structure d'identification du paquet
 	if (iNbRecord == 0)
	{
 		iInitXMLBin7006Record("Bathymetric Data", "7006", "7k Bathymetric Data", uiODOffset, strS7KRecordXML);
	}

    // Lecture du RTH du paquet
	RTH_7006Record = (T_7006_RECORD_RTH *)malloc(sizeof(T_7006_RECORD_RTH));
	iFlag = fread(RTH_7006Record, sizeof(T_7006_RECORD_RTH), 1, G_fpData);
	if (iFlag == 0)
	{
		printf("Error in reading : %s\n", G_cFileData);
		return FALSE;
	}

	ptr_cRTHRecord = (unsigned char*)RTH_7006Record;
	// Sauvegarde des donn�es des attributs.
	if (iNbRecord > 0)
	{
		ptr_vVarPrevRecord = (void *)RTH_prev7006Record;
	}
	for (iVarRTH=0; iVarRTH<(int)strS7KRecordXML->sNbVarRTH; iVarRTH++)
	{
		ptr_vVarRecord = (void *)ptr_cRTHRecord;
		if (iVarRTH==0)
		{
			usTailleEnByte = 0;
			usNbElem = 0;
		}
		else
		{
			usTailleEnByte = util_usNbOctetTypeVar(strS7KRecordXML->strTabRTHXML[iVarRTH-1].cType);
			usNbElem = strS7KRecordXML->strTabRTHXML[iVarRTH-1].usNbElem;
		}


		if (iNbRecord > 0)
		{
			bFlagVarConstant = strS7KRecordXML->strTabRTHXML[iVarRTH].bFlagConstant;
			ptr_vVarPrevRecord = ptr_vVarPrevRecord + usTailleEnByte*usNbElem;
			bFlagCompare = util_bComparePrevAndCurrentVal(strS7KRecordXML->strTabRTHXML[iVarRTH].cType,
												ptr_vVarPrevRecord,
												ptr_vVarRecord,
											   strS7KRecordXML->strTabRTHXML[iVarRTH].usNbElem);
			bFlagVarConstant = bFlagVarConstant && bFlagCompare;
			strS7KRecordXML->strTabRTHXML[iVarRTH].bFlagConstant = bFlagVarConstant;
		}
		// Ecriture sp�ciale car contenant du XML pour la variable Device Info
		// Pr�paration de l'�criture de la variable.
		usTailleEnByte = strS7KRecordXML->strTabRTHXML[iVarRTH].usSize;
		usNbElem =  strS7KRecordXML->strTabRTHXML[iVarRTH].usNbElem;
		fwrite(ptr_cRTHRecord, usTailleEnByte, usNbElem, strS7KRecordXML->strTabRTHXML[iVarRTH].ptr_fpBin);
		strS7KRecordXML->strTabRTHXML[iVarRTH].ptr_vPrevVal = ptr_cRTHRecord;
		ptr_cRTHRecord += usTailleEnByte*usNbElem;
	}
	// Recopie en zone tampon des valeurs courantes.
	memmove(RTH_prev7006Record, RTH_7006Record, sizeof(T_7006_RECORD_RTH));

	// --------------------------------------------
 	// Traitement des paquets RD
 	// Lecture des datas les uns apr�s les autres
	//RD_7006Record = (T_7006_RECORD_RD *)malloc(sizeof(T_7006_RECORD_RD));
	RD_7006Record.Range = malloc(RTH_7006Record->N*sizeof(f32));
	iFlag = fread(RD_7006Record.Range, sizeof(f32), RTH_7006Record->N, G_fpData);
	RD_7006Record.Quality = malloc(RTH_7006Record->N*sizeof(u8));
	iFlag = fread(RD_7006Record.Quality, sizeof(u8), RTH_7006Record->N, G_fpData);
	RD_7006Record.IntensityOrBackscatter = malloc(RTH_7006Record->N*sizeof(f32));
	iFlag = fread(RD_7006Record.IntensityOrBackscatter, sizeof(f32), RTH_7006Record->N, G_fpData);

	// Calcul de la taille th�orique de DRF + RTH+ RD (88 ????, voir JMA) pour d�tecter la pr�sence des
	// variables Min Filter Info et Max Filter Info.
	iNbBytesBeforeOD = 88 + RTH_7006Record->N*9;
	if (uiODOffset != iNbBytesBeforeOD)
	{
		RD_7006Record.MinFilterInfo = malloc(RTH_7006Record->N*sizeof(f32));
		RD_7006Record.MaxFilterInfo = malloc(RTH_7006Record->N*sizeof(f32));
		// Lecture des Min et Max Filters que si l'Optionnel Data est d�sactiv�e.
		iFlag = fread(RD_7006Record.MinFilterInfo, sizeof(f32), RTH_7006Record->N, G_fpData);
		iFlag = fread(RD_7006Record.MaxFilterInfo, sizeof(f32), RTH_7006Record->N, G_fpData);
	}

	// Ecriture des donn�es de la partie RD dans les binaires de stockage.
	iVarRD = 0;
	usTailleEnByte = strS7KRecordXML->strTabRDXML[iVarRD].usSize;
	usNbElem = strS7KRecordXML->strTabRDXML[iVarRD].usNbElem*RTH_7006Record->N;
	fwrite(RD_7006Record.Range, usTailleEnByte, usNbElem, strS7KRecordXML->strTabRDXML[iVarRD].ptr_fpBin);
	if (iNbRecord > 0 && strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant == TRUE)
	{
		bFlagCompare = util_bComparePrevAndCurrentVal(strS7KRecordXML->strTabRDXML[iVarRD].cType,
											strS7KRecordXML->strTabRDXML[iVarRD].ptr_vPrevVal,
											RD_7006Record.Range,
											usNbElem);
		strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant = bFlagCompare && strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant;
	}
	if (iNbRecord == 0)
	{
		strS7KRecordXML->strTabRDXML[iVarRD].ptr_vPrevVal = malloc(usNbElem*usTailleEnByte);
	}
	memmove(strS7KRecordXML->strTabRDXML[iVarRD].ptr_vPrevVal, RD_7006Record.Range, RTH_7006Record->N*usTailleEnByte);

	iVarRD++;
	usTailleEnByte = strS7KRecordXML->strTabRDXML[iVarRD].usSize;
	usNbElem = strS7KRecordXML->strTabRDXML[iVarRD].usNbElem*RTH_7006Record->N;
	fwrite(RD_7006Record.Quality, usTailleEnByte, usNbElem, strS7KRecordXML->strTabRDXML[iVarRD].ptr_fpBin);
	if (iNbRecord > 0 && strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant == TRUE)
	{
		bFlagCompare = util_bComparePrevAndCurrentVal(strS7KRecordXML->strTabRDXML[iVarRD].cType,
											strS7KRecordXML->strTabRDXML[iVarRD].ptr_vPrevVal,
											RD_7006Record.Quality,
											usNbElem);
		strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant = bFlagCompare && strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant;
	}
	if (iNbRecord == 0)
	{
		strS7KRecordXML->strTabRDXML[iVarRD].ptr_vPrevVal = malloc(usNbElem*usTailleEnByte);
	}
	memmove(strS7KRecordXML->strTabRDXML[iVarRD].ptr_vPrevVal, RD_7006Record.Quality, RTH_7006Record->N*usTailleEnByte);

	iVarRD++;
	usTailleEnByte = strS7KRecordXML->strTabRDXML[iVarRD].usSize;
	usNbElem = strS7KRecordXML->strTabRDXML[iVarRD].usNbElem*RTH_7006Record->N;
	fwrite(RD_7006Record.IntensityOrBackscatter, usTailleEnByte, usNbElem, strS7KRecordXML->strTabRDXML[iVarRD].ptr_fpBin);
	if (iNbRecord > 0 && strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant == TRUE)
	{
		bFlagCompare = util_bComparePrevAndCurrentVal(strS7KRecordXML->strTabRDXML[iVarRD].cType,
											strS7KRecordXML->strTabRDXML[iVarRD].ptr_vPrevVal,
											RD_7006Record.IntensityOrBackscatter,
											usNbElem);
		strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant = bFlagCompare && strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant;
	}
	if (iNbRecord == 0)
	{
		strS7KRecordXML->strTabRDXML[iVarRD].ptr_vPrevVal = malloc(usNbElem*usTailleEnByte);
	}
	memmove(strS7KRecordXML->strTabRDXML[iVarRD].ptr_vPrevVal, RD_7006Record.IntensityOrBackscatter, RTH_7006Record->N*usTailleEnByte);


	// Traitement des Min et Max Filters que si l'Optionnel Data est d�sactiv�e.
	// Les Min et Max Filters sont � 0 dans ce cas.
	if (uiODOffset != iNbBytesBeforeOD)
	{
		iVarRD++;
		usTailleEnByte = strS7KRecordXML->strTabRDXML[iVarRD].usSize;
		usNbElem = strS7KRecordXML->strTabRDXML[iVarRD].usNbElem*RTH_7006Record->N;
		fwrite(RD_7006Record.MinFilterInfo, usTailleEnByte, usNbElem, strS7KRecordXML->strTabRDXML[iVarRD].ptr_fpBin);
		if (iNbRecord > 0 && strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant == TRUE)
		{
			bFlagCompare = util_bComparePrevAndCurrentVal(strS7KRecordXML->strTabRDXML[iVarRD].cType,
												strS7KRecordXML->strTabRDXML[iVarRD].ptr_vPrevVal,
												RD_7006Record.MinFilterInfo,
												usNbElem);
			strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant = bFlagCompare && strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant;
		}
		if (iNbRecord == 0)
		{
			strS7KRecordXML->strTabRDXML[iVarRD].ptr_vPrevVal = malloc(usNbElem*usTailleEnByte);
		}
		memmove(strS7KRecordXML->strTabRDXML[iVarRD].ptr_vPrevVal, RD_7006Record.MinFilterInfo, RTH_7006Record->N*usTailleEnByte);

		iVarRD++;
		usTailleEnByte = strS7KRecordXML->strTabRDXML[iVarRD].usSize;
		usNbElem = strS7KRecordXML->strTabRDXML[iVarRD].usNbElem*RTH_7006Record->N;
		fwrite(RD_7006Record.MaxFilterInfo, usTailleEnByte, usNbElem, strS7KRecordXML->strTabRDXML[iVarRD].ptr_fpBin);
		if (iNbRecord > 0 && strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant == TRUE)
		{
			bFlagCompare = util_bComparePrevAndCurrentVal(strS7KRecordXML->strTabRDXML[iVarRD].cType,
												strS7KRecordXML->strTabRDXML[iVarRD].ptr_vPrevVal,
												RD_7006Record.MaxFilterInfo,
												usNbElem);
			strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant = bFlagCompare && strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant;
		}
		if (iNbRecord == 0)
		{
			strS7KRecordXML->strTabRDXML[iVarRD].ptr_vPrevVal = malloc(usNbElem*usTailleEnByte);
		}
		memmove(strS7KRecordXML->strTabRDXML[iVarRD].ptr_vPrevVal, RD_7006Record.MaxFilterInfo, RTH_7006Record->N*usTailleEnByte);
	}

	// Traitement des donn�es Optionnelles du Paquet 7006
	if (uiODOffset != 0)
	{
		//OD_7006Record = (T_7006_RECORD_OD *)malloc(sizeof(T_7006_RECORD_OD));
		// Ecriture des donn�es dans les binaires de stockage.
		iVarOD = 0;
		usTailleEnByte = strS7KRecordXML->strTabODXML[iVarOD].usSize;

		iFlag = fread(&OD_7006Record.Frequency, usTailleEnByte, 1, G_fpData);
		iFlag = fwrite(&OD_7006Record.Frequency, usTailleEnByte, 1, strS7KRecordXML->strTabODXML[iVarOD].ptr_fpBin);
		if (iNbRecord > 0 && strS7KRecordXML->strTabODXML[iVarOD].bFlagConstant == TRUE)
		{
			bFlagCompare = util_bComparePrevAndCurrentVal(strS7KRecordXML->strTabRTHXML[iVarOD].cType,
												strS7KRecordXML->strTabODXML[iVarOD].ptr_vPrevVal,
												&OD_7006Record.Frequency,
												strS7KRecordXML->strTabODXML[iVarOD].usNbElem);
			strS7KRecordXML->strTabODXML[iVarOD].bFlagConstant = bFlagCompare && strS7KRecordXML->strTabODXML[iVarOD].bFlagConstant;
		}
		if (iNbRecord == 0)
		{
			strS7KRecordXML->strTabODXML[iVarOD].ptr_vPrevVal = (void *)malloc(usTailleEnByte);
		}
		memmove(strS7KRecordXML->strTabODXML[iVarOD].ptr_vPrevVal, &OD_7006Record.Frequency, usTailleEnByte);


		iVarOD++;
		usTailleEnByte = strS7KRecordXML->strTabODXML[iVarOD].usSize;
		iFlag = fread(&OD_7006Record.Latitude, usTailleEnByte, 1, G_fpData);
		iFlag = fwrite(&OD_7006Record.Latitude, usTailleEnByte, 1, strS7KRecordXML->strTabODXML[iVarOD].ptr_fpBin);
		if (iNbRecord > 0 && strS7KRecordXML->strTabODXML[iVarOD].bFlagConstant == TRUE)
		{
			bFlagCompare = util_bComparePrevAndCurrentVal(strS7KRecordXML->strTabRTHXML[iVarOD].cType,
												strS7KRecordXML->strTabODXML[iVarOD].ptr_vPrevVal,
												&OD_7006Record.Latitude,
												strS7KRecordXML->strTabODXML[iVarOD].usNbElem);
			strS7KRecordXML->strTabODXML[iVarOD].bFlagConstant = bFlagCompare && strS7KRecordXML->strTabODXML[iVarOD].bFlagConstant;
		}
		if (iNbRecord == 0)
		{
			strS7KRecordXML->strTabODXML[iVarOD].ptr_vPrevVal = (void *)malloc(usTailleEnByte);
		}
		memmove(strS7KRecordXML->strTabODXML[iVarOD].ptr_vPrevVal, &OD_7006Record.Latitude, usTailleEnByte);

		iVarOD++;
		usTailleEnByte = strS7KRecordXML->strTabODXML[iVarOD].usSize;
		iFlag = fread(&OD_7006Record.Longitude,usTailleEnByte, 1, G_fpData);
		iFlag = fwrite(&OD_7006Record.Longitude, usTailleEnByte, 1, strS7KRecordXML->strTabODXML[iVarOD].ptr_fpBin);
		if (iNbRecord > 0 && strS7KRecordXML->strTabODXML[iVarOD].bFlagConstant == TRUE)
		{
			bFlagCompare = util_bComparePrevAndCurrentVal(strS7KRecordXML->strTabRTHXML[iVarOD].cType,
												strS7KRecordXML->strTabODXML[iVarOD].ptr_vPrevVal,
												&OD_7006Record.Longitude,
												strS7KRecordXML->strTabODXML[iVarOD].usNbElem);
			strS7KRecordXML->strTabODXML[iVarOD].bFlagConstant = bFlagCompare && strS7KRecordXML->strTabODXML[iVarOD].bFlagConstant;
		}
		if (iNbRecord == 0)
		{
			strS7KRecordXML->strTabODXML[iVarOD].ptr_vPrevVal = (void *)malloc(usTailleEnByte);
		}
		memmove(strS7KRecordXML->strTabODXML[iVarOD].ptr_vPrevVal, &OD_7006Record.Longitude, usTailleEnByte);

		iVarOD++;
		usTailleEnByte = strS7KRecordXML->strTabODXML[iVarOD].usSize;
		iFlag = fread(&OD_7006Record.Heading, usTailleEnByte, 1, G_fpData);
		iFlag = fwrite(&OD_7006Record.Heading, usTailleEnByte, 1, strS7KRecordXML->strTabODXML[iVarOD].ptr_fpBin);
		if (iNbRecord > 0 && strS7KRecordXML->strTabODXML[iVarOD].bFlagConstant == TRUE)
		{
			bFlagCompare = util_bComparePrevAndCurrentVal(strS7KRecordXML->strTabRTHXML[iVarOD].cType,
												strS7KRecordXML->strTabODXML[iVarOD].ptr_vPrevVal,
												&OD_7006Record.Heading,
												strS7KRecordXML->strTabODXML[iVarOD].usNbElem);
			strS7KRecordXML->strTabODXML[iVarOD].bFlagConstant = bFlagCompare && strS7KRecordXML->strTabODXML[iVarOD].bFlagConstant;
		}
		if (iNbRecord == 0)
		{
			strS7KRecordXML->strTabODXML[iVarOD].ptr_vPrevVal = (void *)malloc(usTailleEnByte);
		}
		memmove(strS7KRecordXML->strTabODXML[iVarOD].ptr_vPrevVal, &OD_7006Record.Heading, usTailleEnByte);

		iVarOD++;
		usTailleEnByte = strS7KRecordXML->strTabODXML[iVarOD].usSize;
		iFlag = fread(&OD_7006Record.HeightSource, usTailleEnByte, 1, G_fpData);
		iFlag = fwrite(&OD_7006Record.HeightSource, usTailleEnByte, 1, strS7KRecordXML->strTabODXML[iVarOD].ptr_fpBin);
		if (iNbRecord > 0 && strS7KRecordXML->strTabODXML[iVarOD].bFlagConstant == TRUE)
		{
			bFlagCompare = util_bComparePrevAndCurrentVal(strS7KRecordXML->strTabRTHXML[iVarOD].cType,
												strS7KRecordXML->strTabODXML[iVarOD].ptr_vPrevVal,
												&OD_7006Record.HeightSource,
												strS7KRecordXML->strTabODXML[iVarOD].usNbElem);
			strS7KRecordXML->strTabODXML[iVarOD].bFlagConstant = bFlagCompare && strS7KRecordXML->strTabODXML[iVarOD].bFlagConstant;
		}
		if (iNbRecord == 0)
		{
			strS7KRecordXML->strTabODXML[iVarOD].ptr_vPrevVal = (void *)malloc(usTailleEnByte);
		}
		memmove(strS7KRecordXML->strTabODXML[iVarOD].ptr_vPrevVal, &OD_7006Record.HeightSource, usTailleEnByte);

		iVarOD++;
		usTailleEnByte = strS7KRecordXML->strTabODXML[iVarOD].usSize;
		iFlag = fread(&OD_7006Record.Tide, usTailleEnByte, 1, G_fpData);
		iFlag = fwrite(&OD_7006Record.Tide, usTailleEnByte, 1, strS7KRecordXML->strTabODXML[iVarOD].ptr_fpBin);
		if (iNbRecord > 0 && strS7KRecordXML->strTabODXML[iVarOD].bFlagConstant == TRUE)
		{
			bFlagCompare = util_bComparePrevAndCurrentVal(strS7KRecordXML->strTabRTHXML[iVarOD].cType,
												strS7KRecordXML->strTabODXML[iVarOD].ptr_vPrevVal,
												&OD_7006Record.Tide,
												strS7KRecordXML->strTabODXML[iVarOD].usNbElem);
			strS7KRecordXML->strTabODXML[iVarOD].bFlagConstant = bFlagCompare && strS7KRecordXML->strTabODXML[iVarOD].bFlagConstant;
		}
		if (iNbRecord == 0)
		{
			strS7KRecordXML->strTabODXML[iVarOD].ptr_vPrevVal = (void *)malloc(usTailleEnByte);
		}
		memmove(strS7KRecordXML->strTabODXML[iVarOD].ptr_vPrevVal, &OD_7006Record.Tide, usTailleEnByte);

		iVarOD++;
		usTailleEnByte = strS7KRecordXML->strTabODXML[iVarOD].usSize;
		iFlag = fread(&OD_7006Record.Roll, usTailleEnByte, 1, G_fpData);
		iFlag = fwrite(&OD_7006Record.Roll, usTailleEnByte, 1, strS7KRecordXML->strTabODXML[iVarOD].ptr_fpBin);
		if (iNbRecord > 0 && strS7KRecordXML->strTabODXML[iVarOD].bFlagConstant == TRUE)
		{
			bFlagCompare = util_bComparePrevAndCurrentVal(strS7KRecordXML->strTabRTHXML[iVarOD].cType,
												strS7KRecordXML->strTabODXML[iVarOD].ptr_vPrevVal,
												&OD_7006Record.Roll,
												strS7KRecordXML->strTabODXML[iVarOD].usNbElem);
			strS7KRecordXML->strTabODXML[iVarOD].bFlagConstant = bFlagCompare && strS7KRecordXML->strTabODXML[iVarOD].bFlagConstant;
		}
		if (iNbRecord == 0)
		{
			strS7KRecordXML->strTabODXML[iVarOD].ptr_vPrevVal = (void *)malloc(usTailleEnByte);
		}
		memmove(strS7KRecordXML->strTabODXML[iVarOD].ptr_vPrevVal, &OD_7006Record.Roll, usTailleEnByte);

		iVarOD++;
		usTailleEnByte = strS7KRecordXML->strTabODXML[iVarOD].usSize;
		iFlag = fread(&OD_7006Record.Pitch, usTailleEnByte, 1, G_fpData);
		iFlag = fwrite(&OD_7006Record.Pitch, usTailleEnByte, 1, strS7KRecordXML->strTabODXML[iVarOD].ptr_fpBin);
		if (iNbRecord > 0 && strS7KRecordXML->strTabODXML[iVarOD].bFlagConstant == TRUE)
		{
			bFlagCompare = util_bComparePrevAndCurrentVal(strS7KRecordXML->strTabRTHXML[iVarOD].cType,
												strS7KRecordXML->strTabODXML[iVarOD].ptr_vPrevVal,
												&OD_7006Record.Pitch,
												strS7KRecordXML->strTabODXML[iVarOD].usNbElem);
			strS7KRecordXML->strTabODXML[iVarOD].bFlagConstant = bFlagCompare && strS7KRecordXML->strTabODXML[iVarOD].bFlagConstant;
		}
		if (iNbRecord == 0)
		{
			strS7KRecordXML->strTabODXML[iVarOD].ptr_vPrevVal = (void *)malloc(usTailleEnByte);
		}
		memmove(strS7KRecordXML->strTabODXML[iVarOD].ptr_vPrevVal, &OD_7006Record.Pitch, usTailleEnByte);

		iVarOD++;
		usTailleEnByte = strS7KRecordXML->strTabODXML[iVarOD].usSize;
		iFlag = fread(&OD_7006Record.Heave, usTailleEnByte, 1, G_fpData);
		iFlag = fwrite(&OD_7006Record.Heave, usTailleEnByte, 1, strS7KRecordXML->strTabODXML[iVarOD].ptr_fpBin);
		if (iNbRecord > 0 && strS7KRecordXML->strTabODXML[iVarOD].bFlagConstant == TRUE)
		{
			bFlagCompare = util_bComparePrevAndCurrentVal(strS7KRecordXML->strTabRTHXML[iVarOD].cType,
												strS7KRecordXML->strTabODXML[iVarOD].ptr_vPrevVal,
												&OD_7006Record.Heave,
												strS7KRecordXML->strTabODXML[iVarOD].usNbElem);
			strS7KRecordXML->strTabODXML[iVarOD].bFlagConstant = bFlagCompare && strS7KRecordXML->strTabODXML[iVarOD].bFlagConstant;
		}
		if (iNbRecord == 0)
		{
			strS7KRecordXML->strTabODXML[iVarOD].ptr_vPrevVal = (void *)malloc(usTailleEnByte);
		}
		memmove(strS7KRecordXML->strTabODXML[iVarOD].ptr_vPrevVal, &OD_7006Record.Heave, usTailleEnByte);

		iVarOD++;
		usTailleEnByte = strS7KRecordXML->strTabODXML[iVarOD].usSize;
		iFlag = fread(&OD_7006Record.VehicleDepth, usTailleEnByte, 1, G_fpData);
		iFlag = fwrite(&OD_7006Record.VehicleDepth, usTailleEnByte, 1, strS7KRecordXML->strTabODXML[iVarOD].ptr_fpBin);
		if (iNbRecord > 0 && strS7KRecordXML->strTabODXML[iVarOD].bFlagConstant == TRUE)
		{
			bFlagCompare = util_bComparePrevAndCurrentVal(strS7KRecordXML->strTabRTHXML[iVarOD].cType,
												strS7KRecordXML->strTabODXML[iVarOD].ptr_vPrevVal,
												&OD_7006Record.VehicleDepth,
												strS7KRecordXML->strTabODXML[iVarOD].usNbElem);
			strS7KRecordXML->strTabODXML[iVarOD].bFlagConstant = bFlagCompare && strS7KRecordXML->strTabODXML[iVarOD].bFlagConstant;
		}
		if (iNbRecord == 0)
		{
			strS7KRecordXML->strTabODXML[iVarOD].ptr_vPrevVal = (void *)malloc(usTailleEnByte);
		}
		memmove(strS7KRecordXML->strTabODXML[iVarOD].ptr_vPrevVal, &OD_7006Record.VehicleDepth, usTailleEnByte);

		OD_7006Record.BeamDepth = malloc(RTH_7006Record->N*sizeof(f32));
		OD_7006Record.BeamAlongTrackDistance = malloc(RTH_7006Record->N*sizeof(f32));
		OD_7006Record.BeamAcrossTrackDistance = malloc(RTH_7006Record->N*sizeof(f32));
		OD_7006Record.BeamPointingAngle = malloc(RTH_7006Record->N*sizeof(f32));
		OD_7006Record.BeamAzimuthAngle = malloc(RTH_7006Record->N*sizeof(f32));

		for (iLoop=0; iLoop< RTH_7006Record->N; iLoop++)
		{
			iVarOD = 10;
			usTailleEnByte = strS7KRecordXML->strTabODXML[iVarOD].usSize;
			iFlag = fread(OD_7006Record.BeamDepth, usTailleEnByte, 1, G_fpData);
			iFlag = fwrite(OD_7006Record.BeamDepth, usTailleEnByte, 1, strS7KRecordXML->strTabODXML[iVarOD].ptr_fpBin);
			if (iNbRecord > 0 && strS7KRecordXML->strTabODXML[iVarOD].bFlagConstant == TRUE)
			{
				bFlagCompare = util_bComparePrevAndCurrentVal(strS7KRecordXML->strTabRTHXML[iVarOD].cType,
													strS7KRecordXML->strTabODXML[iVarOD].ptr_vPrevVal,
													(void *)OD_7006Record.BeamDepth,
													usNbElem);
				strS7KRecordXML->strTabODXML[iVarOD].bFlagConstant = bFlagCompare && strS7KRecordXML->strTabODXML[iVarOD].bFlagConstant;
			}
			if (iNbRecord == 0)
			{
				strS7KRecordXML->strTabODXML[iVarOD].ptr_vPrevVal = (void *)malloc(usTailleEnByte);
			}
			memmove(strS7KRecordXML->strTabODXML[iVarOD].ptr_vPrevVal, OD_7006Record.BeamDepth, usTailleEnByte);


			iVarOD++;
			usTailleEnByte = strS7KRecordXML->strTabODXML[iVarOD].usSize;
			iFlag = fread(OD_7006Record.BeamAlongTrackDistance, usTailleEnByte, 1, G_fpData);
			iFlag = fwrite(OD_7006Record.BeamAlongTrackDistance, usTailleEnByte, 1, strS7KRecordXML->strTabODXML[iVarOD].ptr_fpBin);
			if (iNbRecord > 0 && strS7KRecordXML->strTabODXML[iVarOD].bFlagConstant == TRUE)
			{
				bFlagCompare = util_bComparePrevAndCurrentVal(strS7KRecordXML->strTabRTHXML[iVarOD].cType,
													strS7KRecordXML->strTabODXML[iVarOD].ptr_vPrevVal,
													(void *)OD_7006Record.BeamAlongTrackDistance,
													usNbElem);
				strS7KRecordXML->strTabODXML[iVarOD].bFlagConstant = bFlagCompare && strS7KRecordXML->strTabODXML[iVarOD].bFlagConstant;
			}
			if (iNbRecord == 0)
			{
				strS7KRecordXML->strTabODXML[iVarOD].ptr_vPrevVal = (void *)malloc(usTailleEnByte);
			}
			memmove(strS7KRecordXML->strTabODXML[iVarOD].ptr_vPrevVal, OD_7006Record.BeamAlongTrackDistance, usTailleEnByte);


			iVarOD++;
			usTailleEnByte = strS7KRecordXML->strTabODXML[iVarOD].usSize;
			iFlag = fread(OD_7006Record.BeamAcrossTrackDistance, usTailleEnByte, 1, G_fpData);
			iFlag = fwrite(OD_7006Record.BeamAcrossTrackDistance, usTailleEnByte, 1, strS7KRecordXML->strTabODXML[iVarOD].ptr_fpBin);
			if (iNbRecord > 0 && strS7KRecordXML->strTabODXML[iVarOD].bFlagConstant == TRUE)
			{
				bFlagCompare = util_bComparePrevAndCurrentVal(strS7KRecordXML->strTabRTHXML[iVarOD].cType,
													strS7KRecordXML->strTabODXML[iVarOD].ptr_vPrevVal,
													(void *)OD_7006Record.BeamAcrossTrackDistance,
													usNbElem);
				strS7KRecordXML->strTabODXML[iVarOD].bFlagConstant = bFlagCompare && strS7KRecordXML->strTabODXML[iVarOD].bFlagConstant;
			}
			if (iNbRecord == 0)
			{
				strS7KRecordXML->strTabODXML[iVarOD].ptr_vPrevVal = (void *)malloc(usTailleEnByte);
			}
			memmove(strS7KRecordXML->strTabODXML[iVarOD].ptr_vPrevVal, OD_7006Record.BeamAcrossTrackDistance, usTailleEnByte);


			iVarOD++;
			usTailleEnByte = strS7KRecordXML->strTabODXML[iVarOD].usSize;
			iFlag = fread(OD_7006Record.BeamPointingAngle, usTailleEnByte, 1, G_fpData);
			iFlag = fwrite(OD_7006Record.BeamPointingAngle, usTailleEnByte, 1, strS7KRecordXML->strTabODXML[iVarOD].ptr_fpBin);
			if (iNbRecord > 0 && strS7KRecordXML->strTabODXML[iVarOD].bFlagConstant == TRUE)
			{
				bFlagCompare = util_bComparePrevAndCurrentVal(strS7KRecordXML->strTabRTHXML[iVarOD].cType,
													strS7KRecordXML->strTabODXML[iVarOD].ptr_vPrevVal,
													(void *)OD_7006Record.BeamPointingAngle,
													usNbElem);
				strS7KRecordXML->strTabODXML[iVarOD].bFlagConstant = bFlagCompare && strS7KRecordXML->strTabODXML[iVarOD].bFlagConstant;
			}
			if (iNbRecord == 0)
			{
				strS7KRecordXML->strTabODXML[iVarOD].ptr_vPrevVal = (void *)malloc(usTailleEnByte);
			}
			memmove(strS7KRecordXML->strTabODXML[iVarOD].ptr_vPrevVal, OD_7006Record.BeamPointingAngle, usTailleEnByte);

			iVarOD++;
			usTailleEnByte = strS7KRecordXML->strTabODXML[iVarOD].usSize;
			iFlag = fread(OD_7006Record.BeamAzimuthAngle, usTailleEnByte, 1, G_fpData);
			iFlag = fwrite(OD_7006Record.BeamAzimuthAngle, usTailleEnByte, 1, strS7KRecordXML->strTabODXML[iVarOD].ptr_fpBin);
			if (iNbRecord > 0 && strS7KRecordXML->strTabODXML[iVarOD].bFlagConstant == TRUE)
			{
				bFlagCompare = util_bComparePrevAndCurrentVal(strS7KRecordXML->strTabRTHXML[iVarOD].cType,
													strS7KRecordXML->strTabODXML[iVarOD].ptr_vPrevVal,
													(void *)OD_7006Record.BeamAzimuthAngle,
													usNbElem);
				strS7KRecordXML->strTabODXML[iVarOD].bFlagConstant = bFlagCompare && strS7KRecordXML->strTabODXML[iVarOD].bFlagConstant;
			}
			if (iNbRecord == 0)
			{
				strS7KRecordXML->strTabODXML[iVarOD].ptr_vPrevVal = (void *)malloc(usTailleEnByte);
			}
			memmove(strS7KRecordXML->strTabODXML[iVarOD].ptr_vPrevVal, OD_7006Record.BeamAzimuthAngle, usTailleEnByte);

			iVarOD++;
		}

	}

	// D�sallocation (obligatoire !) des matrices r�serv�es.
	free(RD_7006Record.Range);
	free(RD_7006Record.Quality);
	free(RD_7006Record.IntensityOrBackscatter);
	if (uiODOffset != iNbBytesBeforeOD)
	{
		free(RD_7006Record.MinFilterInfo);
		free(RD_7006Record.MaxFilterInfo);
	}
	//free(RD_7006Record);
	if (uiODOffset != 0)
	{
		free(OD_7006Record.BeamDepth);
		free(OD_7006Record.BeamAlongTrackDistance);
		free(OD_7006Record.BeamAcrossTrackDistance);
		free(OD_7006Record.BeamPointingAngle);
		free(OD_7006Record.BeamAzimuthAngle);
		//free(OD_7006Record);
	}

	free(RTH_7006Record);
	iNbRecord++;

	return iRetour;

} //iProcess7006Record


//----------------------------------------------------------------
// Fonction :	iInitXMLBin7006Record
// Objet 	:	Initialisation de la structure �quivalent au paquet
//				7006 pour la manipulation des fichiers bin et XML.
// Modification : 15/10/08
// Auteur 	: GLU
//----------------------------------------------------------------
int iInitXMLBin7006Record(	char *cTitleDatagram,
							char *cNomDatagram, char *cComments,
							u32 uiODOffset,
							T_S7KRecordXML *strS7KRecordXML)
{
	char cTabNomSignalRTH[7][30] = {
			"SonarId",
			"PingNumber",
			"MultiPingSequence",
			"N",
			"LayerCompensationFlag",
			"SoundVelocityFlag",
			"SoundVelocity"};


	char cTabTypeSignalRTH[7][20] = {
			"u64",
			"u32",
			"u16",
			"u32",
			"u8",
			"u8",
			"f32"};

	// ScaleFactor
	float 	fTabSFSignal[7] = {
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0};

	// AddOffset
	float 	fTabAOSignal[7] = {
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0};

	char cTabNomVarRD[5][30] = {
			"Range",
			"Quality",
			"IntensityOrBackscatter",
			"MinFilterInfo",
			"MaxFilterInfo"};

	char cTabTypeVarRD[5][20] = {
			"f32",
			"u8",
			"f32",
			"f32",
			"f32"};

	// ScaleFactor
	float 	fTabSFImage[5] = {
			1.0,
			1.0,
			1.0,
			1.0,
			1.0};

	// AddOffset
	float 	fTabAOImage[5] = {
			0.0,
			0.0,
			0.0,
			0.0,
			0.0};

	char cTabNomVarOD[15][30] = {
			"Frequency",
			"Latitude",
			"Longitude",
			"Heading",
			"HeightSource",
			"Tide",
			"Roll",
			"Pitch",
			"Heave",
			"VehicleDepth",
			"BeamDepth",
			"BeamAlongTrackDistance",
			"BeamAcrossTrackDistance",
			"BeamPointingAngle",
			"BeamAzimuthAngle"};

	char cTabTypeVarOD[15][20] = {
			"f32",
			"f64",
			"f64",
			"f32",
			"u8",
			"f32",
			"f32",
			"f32",
			"f32",
			"f32",
			"f32",
			"f32",
			"f32",
			"f32",
			"f32"};

	// ScaleFactor
	float 	fTabSFImage2[15] = {
			0.001,
			RAD2DEG,
			RAD2DEG,
			RAD2DEG,
			1.0,
			1.0,
			RAD2DEG,
			RAD2DEG,
			1.0,
			-1.0,
			1.0,
			1.0,
			1.0,
			RAD2DEG,
			RAD2DEG};

	// AddOffset
	float 	fTabAOImage2[15] = {
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0};


	char	*ptr_cNomHeader,
			*ptr_cRepertoire,
			cNomFicPathBin[300];

    int 	iSizeRep,
			iErr,
			iLen,
			iVarRTH,
			iVarRD,
			iVarOD;

	unsigned short usTaille;

	// Initialisation des structures de pilotage des fichiers
	// XML et Bin .
	strS7KRecordXML->sNbVarRTH = 7;
    strS7KRecordXML->strTabRTHXML= (T_S7KVarXML *)malloc(strS7KRecordXML->sNbVarRTH*sizeof(T_S7KVarXML));
	strS7KRecordXML->sNbVarRD = 5;
	strS7KRecordXML->strTabRDXML= (T_S7KVarXML *)malloc(strS7KRecordXML->sNbVarRD*sizeof(T_S7KVarXML));
	strS7KRecordXML->sNbVarOD = 15;
	strS7KRecordXML->strTabODXML= (T_S7KVarXML *)malloc(strS7KRecordXML->sNbVarOD*sizeof(T_S7KVarXML));

	iLen = strlen(cNomDatagram)+1;
	strS7KRecordXML->ptr_cNomHeader = malloc(iLen*sizeof(char));
	sprintf(strS7KRecordXML->ptr_cNomHeader, "%s", cNomDatagram);

	iLen = strlen(cTitleDatagram)+1;
	strS7KRecordXML->ptr_cLabelHeader = malloc(iLen*sizeof(char));
	sprintf(strS7KRecordXML->ptr_cLabelHeader, "%s", cTitleDatagram);

	iLen = strlen(cComments)+1;
	strS7KRecordXML->ptr_cCommentsHeader = malloc(iLen*sizeof(char));
	sprintf(strS7KRecordXML->ptr_cCommentsHeader, "%s", cComments);

	// Remplissage du nom de la variable d'indexation des tables.
	iLen = strlen(cTabNomSignalRTH[3])+1;
	strS7KRecordXML->ptr_cNomIndexTable = malloc(iLen*sizeof(char));
	sprintf(strS7KRecordXML->ptr_cNomIndexTable, "%s", cTabNomSignalRTH[3]);

	// Affectation du nom du repertoire.
	ptr_cNomHeader = strS7KRecordXML->ptr_cNomHeader;
	iSizeRep = ( strlen(G_cRepData) + strlen("_tmp") +  strlen( ptr_cNomHeader));
	// Ajout d'une marge au cas o� certains caract�res soient doubl�s soient doubl�s.
	ptr_cRepertoire = malloc( (iSizeRep + 100)*sizeof(char));
	sprintf(ptr_cRepertoire, "%s%s\\S7K_%s", G_cRepData, "_tmp", ptr_cNomHeader);
	if (DEBUG)
	   printf("-- Directory Creation : %s\n", ptr_cRepertoire);

	// Cr�ation du r�pertoire
	iErr = mkdir(ptr_cRepertoire);
	if (iErr != 0)
	{
	   switch (errno)
	   {
		   //"File exists"
		   case EEXIST:
			   break;
		   default:
			   printf("Error in Directory creation %s\n", strerror(errno));
			   return EXIT_FAILURE;
			   break;
	   }
	}
	// Initialisation g�n�rale des variables du paquet OD
	for (iVarRTH=0; iVarRTH<strS7KRecordXML->sNbVarRTH; iVarRTH++) {
	   strS7KRecordXML->strTabRTHXML[iVarRTH].ptr_fpBin = 0;
	   strS7KRecordXML->strTabRTHXML[iVarRTH].bFlagConstant = TRUE;
	   strS7KRecordXML->strTabRTHXML[iVarRTH].usNbElem = 1;
	   sprintf(strS7KRecordXML->strTabRTHXML[iVarRTH].cNom, "%s", cTabNomSignalRTH[iVarRTH]);
	   sprintf(strS7KRecordXML->strTabRTHXML[iVarRTH].cType, "%s", cTabTypeSignalRTH[iVarRTH]);
	   sprintf(strS7KRecordXML->strTabRTHXML[iVarRTH].cTag, "%s", "TODO");
	   sprintf(strS7KRecordXML->strTabRTHXML[iVarRTH].cUnit, "%s", "TODO");
	   strS7KRecordXML->strTabRTHXML[iVarRTH].fScaleFactor = fTabSFSignal[iVarRTH];
	   strS7KRecordXML->strTabRTHXML[iVarRTH].fAddOffset = fTabAOSignal[iVarRTH];
	   usTaille = util_usNbOctetTypeVar(cTabTypeSignalRTH[iVarRTH]);
	   strS7KRecordXML->strTabRTHXML[iVarRTH].usSize = usTaille;
	   iLen = strlen(strS7KRecordXML->strTabRTHXML[iVarRTH].cNom);
	   // Ajout d'une marge au cas o� certains caract�res soient doubl�s soient doubl�s.
	   sprintf(cNomFicPathBin, "%s\\%s%s", ptr_cRepertoire, strS7KRecordXML->strTabRTHXML[iVarRTH].cNom, ".bin");
	   sprintf(strS7KRecordXML->strTabRTHXML[iVarRTH].cNomPathFileBin, "%s", cNomFicPathBin);
	   strS7KRecordXML->strTabRTHXML[iVarRTH].ptr_fpBin = fopen(cNomFicPathBin, "w+b");
	   if (!strS7KRecordXML->strTabRTHXML[iVarRTH].ptr_fpBin)
	   {
		   printf("%s -- Error in Signal Creation File  : %s\n", __FILE__, cNomFicPathBin);
		   return EXIT_FAILURE;
	   }
	}


	for (iVarRD=0; iVarRD<strS7KRecordXML->sNbVarRD; iVarRD++)
	{
	   strS7KRecordXML->strTabRDXML[iVarRD].ptr_fpBin = 0;
	   strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant = TRUE;
	   strS7KRecordXML->strTabRDXML[iVarRD].usNbElem = 1;
	   sprintf(strS7KRecordXML->strTabRDXML[iVarRD].cNom, "%s", cTabNomVarRD[iVarRD]);
	   sprintf(strS7KRecordXML->strTabRDXML[iVarRD].cType, "%s", cTabTypeVarRD[iVarRD]);
	   sprintf(strS7KRecordXML->strTabRDXML[iVarRD].cTag, "%s", "TODO");
	   sprintf(strS7KRecordXML->strTabRDXML[iVarRD].cUnit, "%s", "TODO");
	   strS7KRecordXML->strTabRDXML[iVarRD].fScaleFactor = fTabSFImage[iVarRD];
	   strS7KRecordXML->strTabRDXML[iVarRD].fAddOffset = fTabAOImage[iVarRD];
	   usTaille = util_usNbOctetTypeVar(cTabTypeVarRD[iVarRD]);
	   strS7KRecordXML->strTabRDXML[iVarRD].usSize = usTaille;
	   // Ajout d'une marge au cas o� certains caract�res soient doubl�s soient doubl�s.
	   sprintf(cNomFicPathBin, "%s\\%s%s", ptr_cRepertoire, strS7KRecordXML->strTabRDXML[iVarRD].cNom, ".bin");
	   strS7KRecordXML->strTabRDXML[iVarRD].ptr_fpBin = fopen(cNomFicPathBin, "w+b");
	   sprintf(strS7KRecordXML->strTabRDXML[iVarRD].cNomPathFileBin, "%s", cNomFicPathBin);
	   if (!strS7KRecordXML->strTabRDXML[iVarRD].ptr_fpBin)
	   {
		   printf("%s -- Error in Signal Creation File  : %s\n", __FILE__, cNomFicPathBin);
		   return EXIT_FAILURE;
	   }
	}

	// Traitement optionnel des OD.
	if (uiODOffset != 0)
	{
		for (iVarOD=0; iVarOD<strS7KRecordXML->sNbVarOD; iVarOD++)
		{
			strS7KRecordXML->strTabODXML[iVarOD].ptr_fpBin = 0;
			strS7KRecordXML->strTabODXML[iVarOD].bFlagConstant = TRUE;
			strS7KRecordXML->strTabODXML[iVarOD].usNbElem = 1;
			sprintf(strS7KRecordXML->strTabODXML[iVarOD].cNom, "%s", cTabNomVarOD[iVarOD]);
			sprintf(strS7KRecordXML->strTabODXML[iVarOD].cType, "%s", cTabTypeVarOD[iVarOD]);
			sprintf(strS7KRecordXML->strTabODXML[iVarOD].cTag, "%s", "TODO");
			sprintf(strS7KRecordXML->strTabODXML[iVarOD].cUnit, "%s", "TODO");
		    strS7KRecordXML->strTabODXML[iVarOD].fScaleFactor = fTabSFImage2[iVarOD];
		    strS7KRecordXML->strTabODXML[iVarOD].fAddOffset = fTabAOImage2[iVarOD];
			usTaille = util_usNbOctetTypeVar(cTabTypeVarOD[iVarOD]);
			strS7KRecordXML->strTabODXML[iVarOD].usSize = usTaille;
			// Ajout d'une marge au cas o� certains caract�res soient doubl�s soient doubl�s.
			sprintf(cNomFicPathBin, "%s\\%s%s", ptr_cRepertoire, strS7KRecordXML->strTabODXML[iVarOD].cNom, ".bin");
			sprintf(strS7KRecordXML->strTabODXML[iVarOD].cNomPathFileBin, "%s", cNomFicPathBin);
			strS7KRecordXML->strTabODXML[iVarOD].ptr_fpBin = fopen(cNomFicPathBin, "w+b");
			if (!strS7KRecordXML->strTabODXML[iVarOD].ptr_fpBin)
			{
			   printf("%s -- Error in Signal Creation File  : %s\n", __FILE__, cNomFicPathBin);
			   return EXIT_FAILURE;
			}
		}
	}

	return EXIT_SUCCESS;

} // iInitXMLBin7006Record

#pragma pack()
