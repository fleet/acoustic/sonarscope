/*
 * S7K_process7006Record.h
 *
 *  Created on: 13 oct. 2008
 *      Author: rgallou
 */

#ifndef S7K_PROCESS7006RECORD_H_
#define S7K_PROCESS7006RECORD_H_
#pragma pack(1)

#include "S7K_DATAGRAMS.H"

typedef struct {
	u64	SonarId;
	u32	PingNumber;
	u16	MultiPingSequence;
	u32	N;
	u8	LayerCompensationFlag;
	u8	SoundVelocityFlag;
	f32	SoundVelocity;
} T_7006_RECORD_RTH;

// Paquet 7006 : partie RD
typedef struct {
	f32	*Range;
	u8	*Quality;
	f32	*IntensityOrBackscatter;
	f32	*MinFilterInfo;
	f32 *MaxFilterInfo;
} T_7006_RECORD_RD;


// Paquet 7006 : partie OD
typedef struct {
	f32	Frequency;
	f64	Latitude;
	f64	Longitude;
	f32	Heading;
	u8	HeightSource;
	f32	Tide;
	f32	Roll;
	f32	Pitch;
	f32	Heave;
	f32	VehicleDepth;
	f32	*BeamDepth;
	f32	*BeamAlongTrackDistance;
	f32	*BeamAcrossTrackDistance;
	f32	*BeamPointingAngle;
	f32	*BeamAzimuthAngle;
} T_7006_RECORD_OD;


int iProcess7006Record(FILE *G_fpData, int iNbRecord,
						u32 uiODOffset,
						T_S7KRecordXML *strS7KRecordXML,
						T_7006_RECORD_RTH *RTH_prev7006Record);

int iInitXMLBin7006Record(	char *cTitleDatagram,
							char *cNomDatagram, char *cComments,
							u32 uiODOffset,
							T_S7KRecordXML *strS7KRecordXML);
#pragma pack()
#endif /* S7K_PROCESS7006RECORD_H_ */
