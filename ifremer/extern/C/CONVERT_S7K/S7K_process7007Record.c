/*
 * S7K_process7007Record.c
 *
 *  Created on: 13 oct. 2008
 *      Author: rgallou
 */
#pragma pack(1)

#include <sys/types.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "S7K_Datagrams.h"
#include "S7K_Utilities.h"
#include "S7K_process7007Record.h"
#include "convertFiles_S7K.h"


//----------------------------------------------------------------
// Fonction :	iProcess7007Record
// Objet 	:	Traitement du paquet 7007
// Modif.	: 	03/11/08
// Auteur 	:	GLU
//----------------------------------------------------------------
int iProcess7007Record(FILE *G_fpData, int iNbRecord,
						u32 uiODOffset,
						T_S7KRecordXML *strS7KRecordXML,
						T_7007_RECORD_RTH *RTH_prev7007Record)
{
    T_7007_RECORD_RTH 	*RTH_7007Record;
    T_7007_RECORD_RD 	*RD_7007Record;
    T_7007_RECORD_OD 	*OD_7007Record = NULL;


	// Pointeur pour avancer sur les variables automatiquement.
	unsigned char		*ptr_cRTHRecord;
	static void 		*ptr_vVarPrevRecord;
	void				*ptr_vVarRecord;

	BOOL		bFlagVarConstant = TRUE,
				bFlagCompare = TRUE;

    int 	iFlag,
			iVarRD,
			iVarRTH,
			iVarOD,
			iRetour = 0;

	

    unsigned short	usTailleEnByte,
					usNbElem;

    // Conservation de l'information de pr�sence de donn�es Optionnal Data.
    strS7KRecordXML->uiODOffset = uiODOffset;

    // Initialisation de la structure d'identification du paquet
 	if (iNbRecord == 0)
	{
 		iInitXMLBin7007Record("BackScatter Imagery Data", "7007", "7k Backscatter Imagery Data", uiODOffset, strS7KRecordXML);
	}

    // Lecture du RTH du paquet
	RTH_7007Record = (T_7007_RECORD_RTH *)malloc(sizeof(T_7007_RECORD_RTH));
	iFlag = fread(RTH_7007Record, sizeof(T_7007_RECORD_RTH), 1, G_fpData);
	if (iFlag == 0)
	{
		printf("Error in reading : %s\n", G_cFileData);
		return FALSE;
	}

	//ptr_vVarRecord = (void*)malloc(sizeof(T_7007_RECORD_RTH));
	//ptr_cRTHRecord = (unsigned char*)malloc(sizeof(T_7007_RECORD_RTH));
	ptr_cRTHRecord = (unsigned char*)RTH_7007Record;
	// Sauvegarde des donn�es des attributs.
	if (iNbRecord > 0)
	{
		//ptr_vVarPrevRecord = (void*)malloc(sizeof(T_7007_RECORD_RTH));
		ptr_vVarPrevRecord = (void *)RTH_prev7007Record;
	}
	for (iVarRTH=0; iVarRTH<(int)strS7KRecordXML->sNbVarRTH; iVarRTH++)
	{
		ptr_vVarRecord = (void *)ptr_cRTHRecord;
		if (iVarRTH==0)
		{
			usTailleEnByte = 0;
			usNbElem = 0;
		}
		else
		{
			usTailleEnByte = util_usNbOctetTypeVar(strS7KRecordXML->strTabRTHXML[iVarRTH-1].cType);
			usNbElem = strS7KRecordXML->strTabRTHXML[iVarRTH-1].usNbElem;
		}


		if (iNbRecord > 0)
		{
			bFlagVarConstant = strS7KRecordXML->strTabRTHXML[iVarRTH].bFlagConstant;
			ptr_vVarPrevRecord = ptr_vVarPrevRecord + usTailleEnByte*usNbElem;
			bFlagCompare = util_bComparePrevAndCurrentVal(strS7KRecordXML->strTabRTHXML[iVarRTH].cType,
												ptr_vVarPrevRecord,
												ptr_vVarRecord,
											   strS7KRecordXML->strTabRTHXML[iVarRTH].usNbElem);
			bFlagVarConstant = bFlagVarConstant && bFlagCompare;
			strS7KRecordXML->strTabRTHXML[iVarRTH].bFlagConstant = bFlagVarConstant;
		}
		// Ecriture sp�ciale car contenant du XML pour la variable Device Info
		// Pr�paration de l'�criture de la variable.
		usTailleEnByte = strS7KRecordXML->strTabRTHXML[iVarRTH].usSize;
		usNbElem =  strS7KRecordXML->strTabRTHXML[iVarRTH].usNbElem;
		fwrite(ptr_cRTHRecord, usTailleEnByte, usNbElem, strS7KRecordXML->strTabRTHXML[iVarRTH].ptr_fpBin);
		strS7KRecordXML->strTabRTHXML[iVarRTH].ptr_vPrevVal = ptr_cRTHRecord;
		ptr_cRTHRecord += usTailleEnByte*usNbElem;
	}

	// Recopie en zone tampon des valeurs courantes.
	memmove(RTH_prev7007Record, RTH_7007Record, sizeof(T_7007_RECORD_RTH));

	// --------------------------------------------
 	// Traitement des paquets RD
 	// Lecture des datas les uns apr�s les autres

	RD_7007Record = (T_7007_RECORD_RD *)malloc(sizeof(T_7007_RECORD_RD));
	RD_7007Record->PortBeamsAmpOrPhase = malloc(RTH_7007Record->S*strS7KRecordXML->strTabRDXML[0].usSize);
	iFlag = fread(RD_7007Record->PortBeamsAmpOrPhase, sizeof(u16), RTH_7007Record->S, G_fpData);
	RD_7007Record->StartboardsBeamAmpOrPhase = malloc(RTH_7007Record->S*sizeof(u16));
	iFlag = fread(RD_7007Record->StartboardsBeamAmpOrPhase, sizeof(u16), RTH_7007Record->S, G_fpData);

	// Ecriture des donn�es de la partie RD dans les binaires de stockage.
	iVarRD = 0;
	usTailleEnByte = strS7KRecordXML->strTabRDXML[iVarRD].usSize;
	usNbElem = strS7KRecordXML->strTabRDXML[iVarRD].usNbElem*RTH_7007Record->S;
	iFlag = fwrite(RD_7007Record->PortBeamsAmpOrPhase, usTailleEnByte, RTH_7007Record->S, strS7KRecordXML->strTabRDXML[iVarRD].ptr_fpBin);
	if (iNbRecord == 0)
	{
		strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant = FALSE;
	}
	// Mise en commentaire : GLU le 05/10/2011, impact sur le Datagramme pour le fichier 2010M_1582232.s7k
//	if (iNbRecord > 0 && strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant == TRUE)
//	{
//		bFlagCompare = util_bComparePrevAndCurrentVal(strS7KRecordXML->strTabRDXML[iVarRD].cType,
//											strS7KRecordXML->strTabRDXML[iVarRD].ptr_vPrevVal,
//											RD_7007Record->PortBeamsAmpOrPhase,
//											usNbElem);
//		strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant = bFlagCompare && strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant;
//	}
//	if (iNbRecord == 0)
//	{
//		strS7KRecordXML->strTabRDXML[iVarRD].ptr_vPrevVal = malloc((RTH_7007Record->S*usTailleEnByte) + 10);
//	}
//	memmove(strS7KRecordXML->strTabRDXML[iVarRD].ptr_vPrevVal, RD_7007Record->PortBeamsAmpOrPhase, RTH_7007Record->S*usTailleEnByte);

	iVarRD++;
	usTailleEnByte = strS7KRecordXML->strTabRDXML[iVarRD].usSize;
	usNbElem = strS7KRecordXML->strTabRDXML[iVarRD].usNbElem*RTH_7007Record->S;
	fwrite(RD_7007Record->StartboardsBeamAmpOrPhase, usTailleEnByte, usNbElem, strS7KRecordXML->strTabRDXML[iVarRD].ptr_fpBin);
	if (iNbRecord == 0)
	{
		strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant = FALSE;
	}
	// Mise en commentaire : GLU le 05/10/2011, impact sur le Datagramme pour le fichier 2010M_1582232.s7k
//	if (iNbRecord > 0 && strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant == TRUE)
//	{
//		bFlagCompare = util_bComparePrevAndCurrentVal(strS7KRecordXML->strTabRDXML[iVarRD].cType,
//											strS7KRecordXML->strTabRDXML[iVarRD].ptr_vPrevVal,
//											RD_7007Record->StartboardsBeamAmpOrPhase,
//											usNbElem);
//		strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant = bFlagCompare && strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant;
//	}
//	if (iNbRecord == 0)
//	{
//		strS7KRecordXML->strTabRDXML[iVarRD].ptr_vPrevVal = malloc(usNbElem*usTailleEnByte);
//	}
//	memmove(strS7KRecordXML->strTabRDXML[iVarRD].ptr_vPrevVal, RD_7007Record->StartboardsBeamAmpOrPhase, RTH_7007Record->S*usTailleEnByte);



	// Traitement des donn�es Optinnelles du Paquet 7007
	if (uiODOffset != 0)
	{
		OD_7007Record = (T_7007_RECORD_OD *)malloc(sizeof(T_7007_RECORD_OD));
		OD_7007Record->Frequency = malloc(sizeof(float));
		OD_7007Record->Latitude = malloc(sizeof(double));
		OD_7007Record->Longitude = malloc(sizeof(double));

		// Ecriture des donn�es dans les binaires de stockage.
		iVarOD = 0;
		usTailleEnByte = strS7KRecordXML->strTabODXML[iVarOD].usSize;
		iFlag = fread(&OD_7007Record->Frequency, usTailleEnByte, 1, G_fpData);
		iFlag = fwrite(&OD_7007Record->Frequency, usTailleEnByte, 1, strS7KRecordXML->strTabODXML[iVarOD].ptr_fpBin);
		if (iNbRecord > 0 && strS7KRecordXML->strTabODXML[iVarOD].bFlagConstant == TRUE)
		{
			bFlagCompare = util_bComparePrevAndCurrentVal(strS7KRecordXML->strTabRTHXML[iVarOD].cType,
												strS7KRecordXML->strTabODXML[iVarOD].ptr_vPrevVal,
												&OD_7007Record->Frequency,
												strS7KRecordXML->strTabODXML[iVarOD].usNbElem);
			strS7KRecordXML->strTabODXML[iVarOD].bFlagConstant = bFlagCompare && strS7KRecordXML->strTabODXML[iVarOD].bFlagConstant;
		}
		if (iNbRecord == 0)
		{
			strS7KRecordXML->strTabODXML[iVarOD].ptr_vPrevVal = (void *)malloc(usTailleEnByte);
		}
		memmove(strS7KRecordXML->strTabODXML[iVarOD].ptr_vPrevVal, &OD_7007Record->Frequency, usTailleEnByte);


		iVarOD++;
		usTailleEnByte = strS7KRecordXML->strTabODXML[iVarOD].usSize;
		iFlag = fread(&OD_7007Record->Latitude, usTailleEnByte, 1, G_fpData);
		iFlag = fwrite(&OD_7007Record->Latitude, usTailleEnByte, 1, strS7KRecordXML->strTabODXML[iVarOD].ptr_fpBin);
		if (iNbRecord > 0 && strS7KRecordXML->strTabODXML[iVarOD].bFlagConstant == TRUE)
		{
			bFlagCompare = util_bComparePrevAndCurrentVal(strS7KRecordXML->strTabRTHXML[iVarOD].cType,
												strS7KRecordXML->strTabODXML[iVarOD].ptr_vPrevVal,
												&OD_7007Record->Latitude,
												strS7KRecordXML->strTabODXML[iVarOD].usNbElem);
			strS7KRecordXML->strTabODXML[iVarOD].bFlagConstant = bFlagCompare && strS7KRecordXML->strTabODXML[iVarOD].bFlagConstant;
		}
		if (iNbRecord == 0)
		{
			strS7KRecordXML->strTabODXML[iVarOD].ptr_vPrevVal = (void *)malloc((usTailleEnByte));
		}
		memmove(strS7KRecordXML->strTabODXML[iVarOD].ptr_vPrevVal, &OD_7007Record->Latitude, usTailleEnByte);

		iVarOD++;
		usTailleEnByte = strS7KRecordXML->strTabODXML[iVarOD].usSize;
		iFlag = fread(&OD_7007Record->Longitude,usTailleEnByte, 1, G_fpData);
		iFlag = fwrite(&OD_7007Record->Longitude, usTailleEnByte, 1, strS7KRecordXML->strTabODXML[iVarOD].ptr_fpBin);
		if (iNbRecord > 0 && strS7KRecordXML->strTabODXML[iVarOD].bFlagConstant == TRUE)
		{
			bFlagCompare = util_bComparePrevAndCurrentVal(strS7KRecordXML->strTabRTHXML[iVarOD].cType,
												strS7KRecordXML->strTabODXML[iVarOD].ptr_vPrevVal,
												&OD_7007Record->Longitude,
												strS7KRecordXML->strTabODXML[iVarOD].usNbElem);
			strS7KRecordXML->strTabODXML[iVarOD].bFlagConstant = bFlagCompare && strS7KRecordXML->strTabODXML[iVarOD].bFlagConstant;
		}
		if (iNbRecord == 0)
		{
			strS7KRecordXML->strTabODXML[iVarOD].ptr_vPrevVal = (void *)malloc((usTailleEnByte));
		}
		memmove(strS7KRecordXML->strTabODXML[iVarOD].ptr_vPrevVal, &OD_7007Record->Longitude, usTailleEnByte);

	}

	// D�sallocation (obligatoire !) des matrices r�serv�es.
	if (uiODOffset != 0)
	{
		free(OD_7007Record->Frequency);
		free(OD_7007Record->Latitude);
		free(OD_7007Record->Longitude);
	}
	free(RD_7007Record->PortBeamsAmpOrPhase);
	free(RD_7007Record->StartboardsBeamAmpOrPhase);
	free(RD_7007Record);
	free(RTH_7007Record);

	iNbRecord++;


	return iRetour;

} //iProcess7007Record


//----------------------------------------------------------------
// Fonction :	iInitXMLBin7007Record
// Objet 	:	Initialisation de la structure �quivalent au paquet
//				7007 pour la manipulation des fichiers bin et XML.
// Modification : 15/10/08
// Auteur 	: GLU
//----------------------------------------------------------------
int iInitXMLBin7007Record(	char *cTitleDatagram,
							char *cNomDatagram, char *cComments,
							u32 uiODOffset,
							T_S7KRecordXML *strS7KRecordXML)
{
	char cTabNomSignalRTH[17][30] = {
			"SonarId",
			"PingNumber",
			"MultiPingSequence"
			"BeamPosition",
			"ControlFlags",
			"S",
			"Port_3dBBeamWithY",
			"Port_3dBBeamWithZ",
			"Startboard_3dBBeamWithY",
			"Startboard_3dBBeamWithZ",
			"PortBeamSteeringWithY",
			"PortBeamSteeringWithZ",
			"StartboardBeamSteeringAngleY",
			"StartboardBeamSteeringAngleZ",
			"N",
			"W",
			"DataTypes"};

	char cTabTypeSignalRTH[17][20] = {
			"u64",
			"u32",
			"u16",
			"f32",
			"u32",
			"u32",
			"f32",
			"f32",
			"f32",
			"f32",
			"f32",
			"f32",
			"f32",
			"f32",
			"u16",
			"char",
			"char"};

	// ScaleFactor
	float 	fTabSFSignal[17] = {
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0};

	// AddOffset
	float 	fTabAOSignal[17] = {
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0};

	char cTabNomVarRD[2][30] = {
			"PortBeamsAmpOrPhase",
			"StartboardsBeamAmpOrPhase"};

	char cTabTypeVarRD[2][20] = {
			"u16",
			"u16"};

	// ScaleFactor
	float 	fTabSFImage[2] = {
			1.0,
			1.0};

	// AddOffset
	float 	fTabAOImage[2] = {
			0.0,
			0.0};

	char cTabNomVarOD[3][30] = {
			"Frequency",
			"Latitude",
			"Longitude"};

	char cTabTypeVarOD[3][20] = {
			"f32",
			"f64",
			"f64"};

	// ScaleFactor
	float 	fTabSFImage2[3] = {
			0.001,
			RAD2DEG,
			RAD2DEG};

	// AddOffset
	float 	fTabAOImage2[3] = {
			0.0,
			0.0,
			0.0};

	char	*ptr_cNomHeader,
			*ptr_cRepertoire,
			cNomFicPathBin[300];

    int 	iSizeRep,
			iErr,
			iLen,
			iVarRTH,
			iVarRD,
			iVarOD;

	unsigned short usTaille;

	// Initialisation des structures de pilotage des fichiers
	// XML et Bin .
	strS7KRecordXML->sNbVarRTH = 17;
    strS7KRecordXML->strTabRTHXML= (T_S7KVarXML *)malloc(strS7KRecordXML->sNbVarRTH*sizeof(T_S7KVarXML));
	strS7KRecordXML->sNbVarRD = 2;
	strS7KRecordXML->strTabRDXML= (T_S7KVarXML *)malloc(strS7KRecordXML->sNbVarRD*sizeof(T_S7KVarXML));
	strS7KRecordXML->sNbVarOD = 3;
	strS7KRecordXML->strTabODXML= (T_S7KVarXML *)malloc(strS7KRecordXML->sNbVarOD*sizeof(T_S7KVarXML));

	iLen = strlen(cNomDatagram)+1;
	strS7KRecordXML->ptr_cNomHeader = malloc(iLen*sizeof(char));
	sprintf(strS7KRecordXML->ptr_cNomHeader, "%s", cNomDatagram);

	iLen = strlen(cTitleDatagram)+1;
	strS7KRecordXML->ptr_cLabelHeader = malloc(iLen*sizeof(char));
	sprintf(strS7KRecordXML->ptr_cLabelHeader, "%s", cTitleDatagram);

	iLen = strlen(cComments)+1;
	strS7KRecordXML->ptr_cCommentsHeader = malloc(iLen*sizeof(char));
	sprintf(strS7KRecordXML->ptr_cCommentsHeader, "%s", cComments);

	// Remplissage du nom de la variable d'indexation des tables.
	iLen = strlen(cTabNomSignalRTH[5])+1;
	strS7KRecordXML->ptr_cNomIndexTable = malloc(iLen*sizeof(char));
	sprintf(strS7KRecordXML->ptr_cNomIndexTable, "%s", cTabNomSignalRTH[5]);

	// Affectation du nom du repertoire.
	ptr_cNomHeader = strS7KRecordXML->ptr_cNomHeader;
	iSizeRep = ( strlen(G_cRepData) + strlen("_tmp") +  strlen( ptr_cNomHeader));
	// Ajout d'une marge au cas o� certains caract�res soient doubl�s soient doubl�s.
	ptr_cRepertoire = malloc( (iSizeRep + 100)*sizeof(char));
	sprintf(ptr_cRepertoire, "%s%s\\S7K_%s", G_cRepData, "_tmp", ptr_cNomHeader);
	if (DEBUG)
	   printf("-- Directory Creation : %s\n", ptr_cRepertoire);

	// Cr�ation du r�pertoire
	iErr = mkdir(ptr_cRepertoire);
	if (iErr != 0)
	{
	   switch (errno)
	   {
		   //"File exists"
		   case EEXIST:
			   break;
		   default:
			   printf("Error in Directory creation %s\n", strerror(errno));
			   return EXIT_FAILURE;
			   break;
	   }
	}
	// Initialisation g�n�rale des variables du paquet RTH
	for (iVarRTH=0; iVarRTH<strS7KRecordXML->sNbVarRTH; iVarRTH++) {
	   strS7KRecordXML->strTabRTHXML[iVarRTH].ptr_fpBin = 0;
	   strS7KRecordXML->strTabRTHXML[iVarRTH].bFlagConstant = TRUE;
	   strS7KRecordXML->strTabRTHXML[iVarRTH].usNbElem = 1;
	   sprintf(strS7KRecordXML->strTabRTHXML[iVarRTH].cNom, "%s", cTabNomSignalRTH[iVarRTH]);
	   sprintf(strS7KRecordXML->strTabRTHXML[iVarRTH].cType, "%s", cTabTypeSignalRTH[iVarRTH]);
	   sprintf(strS7KRecordXML->strTabRTHXML[iVarRTH].cTag, "%s", "TODO");
	   sprintf(strS7KRecordXML->strTabRTHXML[iVarRTH].cUnit, "%s", "TODO");
	   strS7KRecordXML->strTabRTHXML[iVarRTH].fScaleFactor = fTabSFSignal[iVarRTH];
	   strS7KRecordXML->strTabRTHXML[iVarRTH].fAddOffset = fTabAOSignal[iVarRTH];
	   usTaille = util_usNbOctetTypeVar(cTabTypeSignalRTH[iVarRTH]);
	   strS7KRecordXML->strTabRTHXML[iVarRTH].usSize = usTaille;
	   iLen = strlen(strS7KRecordXML->strTabRTHXML[iVarRTH].cNom);
	   // Ajout d'une marge au cas o� certains caract�res soient doubl�s soient doubl�s.
	   sprintf(cNomFicPathBin, "%s\\%s%s", ptr_cRepertoire, strS7KRecordXML->strTabRTHXML[iVarRTH].cNom, ".bin");
	   sprintf(strS7KRecordXML->strTabRTHXML[iVarRTH].cNomPathFileBin, "%s", cNomFicPathBin);
	   strS7KRecordXML->strTabRTHXML[iVarRTH].ptr_fpBin = fopen(cNomFicPathBin, "w+b");
	   if (!strS7KRecordXML->strTabRTHXML[iVarRTH].ptr_fpBin)
	   {
		   printf("%s -- Error in Signal Creation File  : %s\n", __FILE__, cNomFicPathBin);
		   return EXIT_FAILURE;
	   }
	}


	for (iVarRD=0; iVarRD<strS7KRecordXML->sNbVarRD; iVarRD++) {
	   strS7KRecordXML->strTabRDXML[iVarRD].ptr_fpBin = 0;
	   strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant = TRUE;
	   strS7KRecordXML->strTabRDXML[iVarRD].usNbElem = 1;
	   sprintf(strS7KRecordXML->strTabRDXML[iVarRD].cNom, "%s", cTabNomVarRD[iVarRD]);
	   sprintf(strS7KRecordXML->strTabRDXML[iVarRD].cType, "%s", cTabTypeVarRD[iVarRD]);
	   sprintf(strS7KRecordXML->strTabRDXML[iVarRD].cTag, "%s", "TODO");
	   sprintf(strS7KRecordXML->strTabRDXML[iVarRD].cUnit, "%s", "TODO");
	   strS7KRecordXML->strTabRDXML[iVarRD].fScaleFactor = fTabSFImage[iVarRD];
	   strS7KRecordXML->strTabRDXML[iVarRD].fAddOffset = fTabAOImage[iVarRD];
	   usTaille = util_usNbOctetTypeVar(cTabTypeVarRD[iVarRD]);
	   strS7KRecordXML->strTabRDXML[iVarRD].usSize = usTaille;
	   // Ajout d'une marge au cas o� certains caract�res soient doubl�s soient doubl�s.
	   sprintf(cNomFicPathBin, "%s\\%s%s", ptr_cRepertoire, strS7KRecordXML->strTabRDXML[iVarRD].cNom, ".bin");
	   strS7KRecordXML->strTabRDXML[iVarRD].ptr_fpBin = fopen(cNomFicPathBin, "w+b");
	   sprintf(strS7KRecordXML->strTabRDXML[iVarRD].cNomPathFileBin, "%s", cNomFicPathBin);
	   if (!strS7KRecordXML->strTabRDXML[iVarRD].ptr_fpBin)
	   {
		   printf("%s -- Error in Signal Creation File  : %s\n", __FILE__, cNomFicPathBin);
		   return EXIT_FAILURE;
	   }
	}

	// Traitement optionnel des OD.
	if (uiODOffset != 0)
	{
		for (iVarOD=0; iVarOD<strS7KRecordXML->sNbVarOD; iVarOD++)
		{
			strS7KRecordXML->strTabODXML[iVarOD].ptr_fpBin = 0;
			strS7KRecordXML->strTabODXML[iVarOD].bFlagConstant = TRUE;
			strS7KRecordXML->strTabODXML[iVarOD].usNbElem = 1;
			sprintf(strS7KRecordXML->strTabODXML[iVarOD].cNom, "%s", cTabNomVarOD[iVarOD]);
			sprintf(strS7KRecordXML->strTabODXML[iVarOD].cType, "%s", cTabTypeVarOD[iVarOD]);
			sprintf(strS7KRecordXML->strTabODXML[iVarOD].cTag, "%s", "TODO");
			sprintf(strS7KRecordXML->strTabODXML[iVarOD].cUnit, "%s", "TODO");
			strS7KRecordXML->strTabODXML[iVarOD].fScaleFactor = fTabSFImage2[iVarOD];
			strS7KRecordXML->strTabODXML[iVarOD].fAddOffset = fTabAOImage2[iVarOD];
			usTaille = util_usNbOctetTypeVar(cTabTypeVarOD[iVarOD]);
			strS7KRecordXML->strTabODXML[iVarOD].usSize = usTaille;
			// Ajout d'une marge au cas o� certains caract�res soient doubl�s soient doubl�s.
			sprintf(cNomFicPathBin, "%s\\%s%s", ptr_cRepertoire, strS7KRecordXML->strTabODXML[iVarOD].cNom, ".bin");
			sprintf(strS7KRecordXML->strTabODXML[iVarOD].cNomPathFileBin, "%s", cNomFicPathBin);
			strS7KRecordXML->strTabODXML[iVarOD].ptr_fpBin = fopen(cNomFicPathBin, "w+b");
			if (!strS7KRecordXML->strTabODXML[iVarOD].ptr_fpBin)
			{
			   printf("%s -- Error in Signal Creation File  : %s\n", __FILE__, cNomFicPathBin);
			   return EXIT_FAILURE;
			}
		}
	}

	free(ptr_cRepertoire);

	return EXIT_SUCCESS;

} // iInitXMLBin7007Record

#pragma pack()
