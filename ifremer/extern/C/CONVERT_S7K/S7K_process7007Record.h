/*
 * S7K_process7007Record.h
 *
 *  Created on: 13 oct. 2008
 *      Author: rgallou
 */

#ifndef S7K_PROCESS7007RECORD_H_
#define S7K_PROCESS7007RECORD_H_
#pragma pack(1)

#include "S7K_DATAGRAMS.H"

typedef struct {
	u64		SonarId;
	u32		PingNumber;
	u16		MultiPingSequence;
	f32		BeamPosition;
	u32		ControlFlags;
	u32		S;
	f32		Port_3dBBeamWithY;
	f32		Port_3dBBeamWithZ;
	f32		Startboard_3dBBeamWithY;
	f32		Startboard_3dBBeamWithZ;
	f32		PortBeamSteeringWithY;
	f32		PortBeamSteeringWithZ;
	f32		StartboardBeamSteeringAngleY;
	f32		StartboardBeamSteeringAngleZ;
	u16		N;
	char	W;
	char	DataTypes;
	//u8		Reserved[32];
} T_7007_RECORD_RTH;

// Paquet 7007 : partie RD
typedef struct {
	u16	*PortBeamsAmpOrPhase;
	u16	*StartboardsBeamAmpOrPhase;
} T_7007_RECORD_RD;


// Paquet 7007 : partie OD
typedef struct {
	f32	*Frequency;
	f64	*Latitude;
	f64	*Longitude;
} T_7007_RECORD_OD;


int iProcess7007Record(FILE *G_fpData, int iNbRecord,
						u32 uiODOffset,
						T_S7KRecordXML *strS7KRecordXML,
						T_7007_RECORD_RTH *RTH_prev7007Record);

int iInitXMLBin7007Record(	char *cTitleDatagram,
							char *cNomDatagram, char *cComments,
							u32 uiODOffset,
							T_S7KRecordXML *strS7KRecordXML);
#pragma pack()
#endif /* S7K_PROCESS7007RECORD_H_ */
