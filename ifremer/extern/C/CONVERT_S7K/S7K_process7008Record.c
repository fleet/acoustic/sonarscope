/*
 * S7K_process7008Record.c
 *
 *  Created on: 13 oct. 2008
 *      Author: rgallou
 */
#pragma pack(1)

#include <sys/types.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "S7K_Datagrams.h"
#include "S7K_Utilities.h"
#include "S7K_process7008Record.h"
#include "convertFiles_S7K.h"


//----------------------------------------------------------------
// Fonction :	iProcess7008Record
// Objet 	:	Traitement du paquet 7008
// Modif.	: 	03/11/08
// Auteur 	:	GLU
//----------------------------------------------------------------
int iProcess7008Record(FILE *G_fpData, int iNbRecord,
						u32 uiODOffset,
						T_S7KRecordXML *strS7KRecordXML,
						T_7008_RECORD_RTH *RTH_prev7008Record)
{
    T_7008_RECORD_RTH 	*RTH_7008Record;
    T_7008_RECORD_RD 	*RD_7008Record;
    T_7008_RECORD_OD 	*OD_7008Record;

	// Pointeur pour avancer sur les variables automatiquement.
	unsigned char		*ptr_cRTHRecord;
	static void 		*ptr_vVarPrevRecord;
	void				*ptr_vVarRecord;

	BOOL		bFlagVarConstant = TRUE,
				bFlagCompare = TRUE;

    int 	iFlag,
			iVarRD,
			iVarRTH,
			iVarOD,
			iLoop,
			iRetour = 0,
			iTypeAmp,
			iTypePhase,
			iDummy;

	

    unsigned short	usTailleEnByte,
					usNbElem;

    // Conservation de l'information de pr�sence de donn�es Optionnal Data.
    strS7KRecordXML->uiODOffset = uiODOffset;

    // Initialisation de la structure d'identification du paquet
 	if (iNbRecord == 0)
	{
 		iInitXMLBin7008Record("Generic Data", "7008", "7k Generic Data", uiODOffset, strS7KRecordXML);
	}

    // Lecture du RTH du paquet
	RTH_7008Record = (T_7008_RECORD_RTH *)malloc(sizeof(T_7008_RECORD_RTH));
	iFlag = fread(RTH_7008Record, sizeof(T_7008_RECORD_RTH), 1, G_fpData);
	if (iFlag == 0)
	{
		printf("Error in reading : %s\n", G_cFileData);
		return FALSE;
	}

	ptr_cRTHRecord = (unsigned char*)RTH_7008Record;
	// Sauvegarde des donn�es des attributs.
	if (iNbRecord > 0)
	{
		ptr_vVarPrevRecord = (void *)RTH_prev7008Record;
	}
	for (iVarRTH=0; iVarRTH<(int)strS7KRecordXML->sNbVarRTH; iVarRTH++)
	{
		ptr_vVarRecord = (void *)ptr_cRTHRecord;
		if (iVarRTH==0)
		{
			usTailleEnByte = 0;
			usNbElem = 0;
		}
		else
		{
			usTailleEnByte = util_usNbOctetTypeVar(strS7KRecordXML->strTabRTHXML[iVarRTH-1].cType);
			usNbElem = strS7KRecordXML->strTabRTHXML[iVarRTH-1].usNbElem;
		}


		if (iNbRecord > 0)
		{
			bFlagVarConstant = strS7KRecordXML->strTabRTHXML[iVarRTH].bFlagConstant;
			ptr_vVarPrevRecord = ptr_vVarPrevRecord + usTailleEnByte*usNbElem;
			bFlagCompare = util_bComparePrevAndCurrentVal(strS7KRecordXML->strTabRTHXML[iVarRTH].cType,
												ptr_vVarPrevRecord,
												ptr_vVarRecord,
											   strS7KRecordXML->strTabRTHXML[iVarRTH].usNbElem);
			bFlagVarConstant = bFlagVarConstant && bFlagCompare;
			strS7KRecordXML->strTabRTHXML[iVarRTH].bFlagConstant = bFlagVarConstant;
		}
		// Ecriture sp�ciale car contenant du XML pour la variable Device Info
		// Pr�paration de l'�criture de la variable.
		usTailleEnByte = strS7KRecordXML->strTabRTHXML[iVarRTH].usSize;
		usNbElem =  strS7KRecordXML->strTabRTHXML[iVarRTH].usNbElem;
		fwrite(ptr_cRTHRecord, usTailleEnByte, usNbElem, strS7KRecordXML->strTabRTHXML[iVarRTH].ptr_fpBin);
		// R�cup�ration des valeurs de type d'Amplitude et de Phase.
		// La donn�e stock�e en fichier binaire ne distingue pas le type s�par�ment.
		if (!strcmp(strS7KRecordXML->strTabRTHXML[iVarRTH].cNom, "TypeAmpPhase"))
		{
			iDummy = (int)ptr_cRTHRecord[0];
			iTypeAmp = util_GetBits(iDummy, 3, 4);
			iTypePhase = util_GetBits(iDummy, 0, 4);
		}
		strS7KRecordXML->strTabRTHXML[iVarRTH].ptr_vPrevVal = ptr_cRTHRecord;
		ptr_cRTHRecord += usTailleEnByte*usNbElem;
	}
	// Recopie en zone tampon des valeurs courantes.
	memmove(RTH_prev7008Record, RTH_7008Record, sizeof(T_7008_RECORD_RTH));

	// --------------------------------------------
 	// Traitement des paquets RD
 	// Lecture des datas les uns apr�s les autres
	RD_7008Record = (T_7008_RECORD_RD *)malloc(sizeof(T_7008_RECORD_RD));
	RD_7008Record->BeamDescriptor = malloc(RTH_7008Record->N*sizeof(u16));
	iFlag = fread(RD_7008Record->BeamDescriptor, sizeof(u16), RTH_7008Record->N, G_fpData);
	RD_7008Record->BeginSampleDescriptor = malloc(RTH_7008Record->N*sizeof(u32));
	iFlag = fread(RD_7008Record->BeginSampleDescriptor, sizeof(u32), RTH_7008Record->N, G_fpData);
	RD_7008Record->EndSampleDescriptor = malloc(RTH_7008Record->N*sizeof(u32));
	iFlag = fread(RD_7008Record->EndSampleDescriptor, sizeof(u32), RTH_7008Record->N, G_fpData);
	// Traitement des �chantillons selon les cas de figures. Autres cas � coder.
	// TODO
	if (iTypeAmp == 2 && iTypePhase == 2)
	{
		RD_7008Record->Samples = malloc(RTH_7008Record->N*RTH_7008Record->S*2*sizeof(i16));
		iFlag = fread(RD_7008Record->Samples, sizeof(i16), RTH_7008Record->N*RTH_7008Record->S*2, G_fpData);
	}
	// Ecriture des donn�es de la partie RD dans les binaires de stockage.
	iVarRD = 0;
	usTailleEnByte = strS7KRecordXML->strTabRDXML[iVarRD].usSize;
	usNbElem = strS7KRecordXML->strTabRDXML[iVarRD].usNbElem*RTH_7008Record->N;
	fwrite(RD_7008Record->BeamDescriptor, usTailleEnByte, usNbElem, strS7KRecordXML->strTabRDXML[iVarRD].ptr_fpBin);
	if (iNbRecord > 0 && strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant == TRUE)
	{
		bFlagCompare = util_bComparePrevAndCurrentVal(strS7KRecordXML->strTabRDXML[iVarRD].cType,
											strS7KRecordXML->strTabRDXML[iVarRD].ptr_vPrevVal,
											RD_7008Record->BeamDescriptor,
											usNbElem);
		strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant = bFlagCompare && strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant;
	}
	if (iNbRecord == 0)
	{
		strS7KRecordXML->strTabRDXML[iVarRD].ptr_vPrevVal = malloc(usNbElem*usTailleEnByte);
	}
	memmove(strS7KRecordXML->strTabRDXML[iVarRD].ptr_vPrevVal, RD_7008Record->BeamDescriptor, usNbElem*usTailleEnByte);

	iVarRD++;
	usTailleEnByte = strS7KRecordXML->strTabRDXML[iVarRD].usSize;
	usNbElem = strS7KRecordXML->strTabRDXML[iVarRD].usNbElem*RTH_7008Record->N;
	fwrite(RD_7008Record->BeginSampleDescriptor, usTailleEnByte, usNbElem, strS7KRecordXML->strTabRDXML[iVarRD].ptr_fpBin);
	if (iNbRecord > 0 && strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant == TRUE)
	{
		bFlagCompare = util_bComparePrevAndCurrentVal(strS7KRecordXML->strTabRDXML[iVarRD].cType,
											strS7KRecordXML->strTabRDXML[iVarRD].ptr_vPrevVal,
											RD_7008Record->BeginSampleDescriptor,
											usNbElem);
		strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant = bFlagCompare && strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant;
	}
	if (iNbRecord == 0)
	{
		strS7KRecordXML->strTabRDXML[iVarRD].ptr_vPrevVal = malloc(usNbElem*usTailleEnByte);
	}
	memmove(strS7KRecordXML->strTabRDXML[iVarRD].ptr_vPrevVal, RD_7008Record->BeginSampleDescriptor, RTH_7008Record->N*usTailleEnByte);

	iVarRD++;
	usTailleEnByte = strS7KRecordXML->strTabRDXML[iVarRD].usSize;
	usNbElem = strS7KRecordXML->strTabRDXML[iVarRD].usNbElem*RTH_7008Record->N;
	fwrite(RD_7008Record->EndSampleDescriptor, usTailleEnByte, RTH_7008Record->N, strS7KRecordXML->strTabRDXML[iVarRD].ptr_fpBin);
	if (iNbRecord > 0 && strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant == TRUE)
	{
		bFlagCompare = util_bComparePrevAndCurrentVal(strS7KRecordXML->strTabRDXML[iVarRD].cType,
											strS7KRecordXML->strTabRDXML[iVarRD].ptr_vPrevVal,
											RD_7008Record->EndSampleDescriptor,
											usNbElem);
		strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant = bFlagCompare && strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant;
	}
	strS7KRecordXML->strTabRDXML[iVarRD].ptr_vPrevVal = malloc(usNbElem*usTailleEnByte);
	memmove(strS7KRecordXML->strTabRDXML[iVarRD].ptr_vPrevVal, RD_7008Record->EndSampleDescriptor, RTH_7008Record->N*usTailleEnByte);

	// Traitement des �chantillons selon les cas de figures. Autres cas � coder.
	// TODO
	if (iTypeAmp == 2 && iTypePhase == 2)
	{
		iVarRD++;
		usTailleEnByte = strS7KRecordXML->strTabRDXML[iVarRD].usSize;
		usNbElem = strS7KRecordXML->strTabRDXML[iVarRD].usNbElem*RTH_7008Record->N*RTH_7008Record->S*2;
	    fwrite(RD_7008Record->Samples, usTailleEnByte, usNbElem, strS7KRecordXML->strTabRDXML[iVarRD].ptr_fpBin);
		if (iNbRecord > 0 && strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant == TRUE)
		{
			bFlagCompare = util_bComparePrevAndCurrentVal(strS7KRecordXML->strTabRDXML[iVarRD].cType,
												strS7KRecordXML->strTabRDXML[iVarRD].ptr_vPrevVal,
												RD_7008Record->Samples,
												usNbElem);
			strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant = bFlagCompare && strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant;
		}
		strS7KRecordXML->strTabRDXML[iVarRD].ptr_vPrevVal = malloc(usNbElem*usTailleEnByte);
		memmove(strS7KRecordXML->strTabRDXML[iVarRD].ptr_vPrevVal, RD_7008Record->Samples, usNbElem*usTailleEnByte);
	}

	// Traitement des donn�es Optinnelles du Paquet 7008
	if (uiODOffset != 0)
	{
		OD_7008Record = (T_7008_RECORD_OD *)malloc(sizeof(T_7008_RECORD_OD));
		OD_7008Record->BeamAlongTrackDistance = malloc(RTH_7008Record->N*sizeof(f32));
		OD_7008Record->BeamAcrossTrackDistance = malloc(RTH_7008Record->N*sizeof(f32));
		OD_7008Record->CenterSampleDistance = malloc(RTH_7008Record->N*sizeof(f32));

		// Ecriture des donn�es dans les binaires de stockage.
		iVarOD = 0;
		usTailleEnByte = strS7KRecordXML->strTabODXML[iVarOD].usSize;
		usNbElem = 1;
		iFlag = fread(&OD_7008Record->Frequency, usTailleEnByte, usNbElem, G_fpData);
		iFlag = fwrite(&OD_7008Record->Frequency, usTailleEnByte, usNbElem, strS7KRecordXML->strTabODXML[iVarOD].ptr_fpBin);
		if (iNbRecord > 0 && strS7KRecordXML->strTabODXML[iVarOD].bFlagConstant == TRUE)
		{
			bFlagCompare = util_bComparePrevAndCurrentVal(strS7KRecordXML->strTabRTHXML[iVarOD].cType,
												strS7KRecordXML->strTabODXML[iVarOD].ptr_vPrevVal,
												&OD_7008Record->Frequency,
												strS7KRecordXML->strTabODXML[iVarOD].usNbElem);
			strS7KRecordXML->strTabODXML[iVarOD].bFlagConstant = bFlagCompare && strS7KRecordXML->strTabODXML[iVarOD].bFlagConstant;
		}
		if (iNbRecord == 0)
		{
			strS7KRecordXML->strTabODXML[iVarOD].ptr_vPrevVal = (void *)malloc(usNbElem*usTailleEnByte);
		}
		memmove(strS7KRecordXML->strTabODXML[iVarOD].ptr_vPrevVal, &OD_7008Record->Frequency, RTH_7008Record->N*usTailleEnByte);


		iVarOD++;
		usTailleEnByte = strS7KRecordXML->strTabODXML[iVarOD].usSize;
		usNbElem = 1;
		iFlag = fread(&OD_7008Record->Latitude, usTailleEnByte, usNbElem, G_fpData);
		iFlag = fwrite(&OD_7008Record->Latitude, usTailleEnByte, usNbElem, strS7KRecordXML->strTabODXML[iVarOD].ptr_fpBin);
		if (iNbRecord > 0 && strS7KRecordXML->strTabODXML[iVarOD].bFlagConstant == TRUE)
		{
			bFlagCompare = util_bComparePrevAndCurrentVal(strS7KRecordXML->strTabRTHXML[iVarOD].cType,
												strS7KRecordXML->strTabODXML[iVarOD].ptr_vPrevVal,
												&OD_7008Record->Latitude,
												strS7KRecordXML->strTabODXML[iVarOD].usNbElem);
			strS7KRecordXML->strTabODXML[iVarOD].bFlagConstant = bFlagCompare && strS7KRecordXML->strTabODXML[iVarOD].bFlagConstant;
		}
		if (iNbRecord == 0)
		{
			strS7KRecordXML->strTabODXML[iVarOD].ptr_vPrevVal = (void *)malloc(usNbElem*usTailleEnByte);
		}
		memmove(strS7KRecordXML->strTabODXML[iVarOD].ptr_vPrevVal, &OD_7008Record->Latitude, RTH_7008Record->N*usTailleEnByte);

		iVarOD++;
		usTailleEnByte = strS7KRecordXML->strTabODXML[iVarOD].usSize;
		usNbElem = 1;
		iFlag = fread(&OD_7008Record->Longitude,usTailleEnByte, usNbElem, G_fpData);
		iFlag = fwrite(&OD_7008Record->Longitude, usTailleEnByte, usNbElem, strS7KRecordXML->strTabODXML[iVarOD].ptr_fpBin);
		if (iNbRecord > 0 && strS7KRecordXML->strTabODXML[iVarOD].bFlagConstant == TRUE)
		{
			bFlagCompare = util_bComparePrevAndCurrentVal(strS7KRecordXML->strTabRTHXML[iVarOD].cType,
												strS7KRecordXML->strTabODXML[iVarOD].ptr_vPrevVal,
												&OD_7008Record->Longitude,
												strS7KRecordXML->strTabODXML[iVarOD].usNbElem);
			strS7KRecordXML->strTabODXML[iVarOD].bFlagConstant = bFlagCompare && strS7KRecordXML->strTabODXML[iVarOD].bFlagConstant;
		}
		if (iNbRecord == 0)
		{
			strS7KRecordXML->strTabODXML[iVarOD].ptr_vPrevVal = (void *)malloc(usNbElem*usTailleEnByte);
		}
		memmove(strS7KRecordXML->strTabODXML[iVarOD].ptr_vPrevVal, &OD_7008Record->Longitude, RTH_7008Record->N*usTailleEnByte);

		iVarOD++;
		usTailleEnByte = strS7KRecordXML->strTabODXML[iVarOD].usSize;
		usNbElem = 1;
		iFlag = fread(&OD_7008Record->Heading, usTailleEnByte, usNbElem, G_fpData);
		iFlag = fwrite(&OD_7008Record->Heading, usTailleEnByte, 1, strS7KRecordXML->strTabODXML[iVarOD].ptr_fpBin);
		if (iNbRecord > 0 && strS7KRecordXML->strTabODXML[iVarOD].bFlagConstant == TRUE)
		{
			bFlagCompare = util_bComparePrevAndCurrentVal(strS7KRecordXML->strTabRTHXML[iVarOD].cType,
												strS7KRecordXML->strTabODXML[iVarOD].ptr_vPrevVal,
												&OD_7008Record->Heading,
												strS7KRecordXML->strTabODXML[iVarOD].usNbElem);
			strS7KRecordXML->strTabODXML[iVarOD].bFlagConstant = bFlagCompare && strS7KRecordXML->strTabODXML[iVarOD].bFlagConstant;
		}
		if (iNbRecord == 0)
		{
			strS7KRecordXML->strTabODXML[iVarOD].ptr_vPrevVal = (void *)malloc(usNbElem*usTailleEnByte);
		}
		memmove(strS7KRecordXML->strTabODXML[iVarOD].ptr_vPrevVal, &OD_7008Record->Heading, usNbElem*usTailleEnByte);

		for (iLoop=0; iLoop< RTH_7008Record->N; iLoop++)
		{
			iVarOD = 4;
			usTailleEnByte = strS7KRecordXML->strTabODXML[iVarOD].usSize;
			iFlag = fread(OD_7008Record->BeamAlongTrackDistance, usTailleEnByte, 1, G_fpData);
			iFlag = fwrite(OD_7008Record->BeamAlongTrackDistance, usTailleEnByte, 1, strS7KRecordXML->strTabODXML[iVarOD].ptr_fpBin);
			iVarOD++;
			usTailleEnByte = strS7KRecordXML->strTabODXML[iVarOD].usSize;
			iFlag = fread(OD_7008Record->BeamAcrossTrackDistance, usTailleEnByte, 1, G_fpData);
			iFlag = fwrite(OD_7008Record->BeamAcrossTrackDistance, usTailleEnByte, 1, strS7KRecordXML->strTabODXML[iVarOD].ptr_fpBin);
			iVarOD++;
			usTailleEnByte = strS7KRecordXML->strTabODXML[iVarOD].usSize;
			iFlag = fread(OD_7008Record->CenterSampleDistance, usTailleEnByte, 1, G_fpData);
			iFlag = fwrite(OD_7008Record->CenterSampleDistance, usTailleEnByte, 1, strS7KRecordXML->strTabODXML[iVarOD].ptr_fpBin);
			iVarOD++;
		}

	}

	// D�sallocation (obligatoire !) des matrices r�serv�es.
	free(RD_7008Record->BeamDescriptor);
	free(RD_7008Record->BeginSampleDescriptor);
	free(RD_7008Record->EndSampleDescriptor);
	free(RD_7008Record->Samples);
	free(RD_7008Record);
	if (uiODOffset != 0)
	{
		free(OD_7008Record->BeamAlongTrackDistance);
		free(OD_7008Record->BeamAcrossTrackDistance);
		free(OD_7008Record->CenterSampleDistance);
	}
	free(RTH_7008Record);

	iNbRecord++;


	return iRetour;

} //iProcess7008Record


//----------------------------------------------------------------
// Fonction :	iInitXMLBin7008Record
// Objet 	:	Initialisation de la structure �quivalent au paquet
//				7008 pour la manipulation des fichiers bin et XML.
// Modification : 15/10/08
// Auteur 	: GLU
//----------------------------------------------------------------
int iInitXMLBin7008Record(	char *cTitleDatagram,
							char *cNomDatagram, char *cComments,
							u32 uiODOffset,
							T_S7KRecordXML *strS7KRecordXML)
{

	char cTabNomSignalRTH[10][30] = {
			"SonarId",
			"PingNumber",
			"MultiPingSequence",
			"N",
			"Reserved1",
			"S",
			"RecordSubsetFlag",
			"RowColumnFlag",
			"Reserved2",
			"TypeAmpPhase"};


	char cTabTypeSignalRTH[10][20] = {
			"u64",
			"u32",
			"u16",
			"u16",
			"u16",
			"u32",
			"u8",
			"u8",
			"u16",
			"u8"};

	char cTabNomVarRD[4][30] = {
			"BeamDescriptor",
			"BeginSampleDescriptor",
			"EndSampleDescriptor",
			"Samples"};

	char cTabTypeVarRD[4][20] = {
			"u16",
			"u32",
			"u32",
			"short"};

	// ScaleFactor
	float 	fTabSFSignal[4] = {
			1.0,
			1.0,
			1.0,
			1.0};

	// AddOffset
	float 	fTabAOSignal[4] = {
			0.0,
			0.0,
			0.0,
			0.0};

	char cTabNomVarOD[7][30] = {
			"Frequency",
			"Latitude",
			"Longitude",
			"Heading",
			"BeamAlongTrackDistance",
			"BeamAcrossTackDistance",
			"CenterSampleDistance"};

	char cTabTypeVarOD[7][20] = {
			"f32",
			"f64",
			"f64",
			"f32",
			"f32",
			"f32",
			"u32"};

	// ScaleFactor
	float 	fTabSFImage[7] = {
			1.0,
			RAD2DEG,
			RAD2DEG,
			RAD2DEG,
			1.0,
			1.0,
			1.0};

	// AddOffset
	float 	fTabAOImage[7] = {
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0};


	char	*ptr_cNomHeader,
			*ptr_cRepertoire,
			cNomFicPathBin[300];

    int 	iSizeRep,
			iErr,
			iLen,
			iVarRTH,
			iVarRD,
			iVarOD;

	unsigned short usTaille;

	// Initialisation des structures de pilotage des fichiers
	// XML et Bin .
	strS7KRecordXML->sNbVarRTH = 10;
    strS7KRecordXML->strTabRTHXML= (T_S7KVarXML *)malloc(strS7KRecordXML->sNbVarRTH*sizeof(T_S7KVarXML));
	strS7KRecordXML->sNbVarRD = 4;
	strS7KRecordXML->strTabRDXML= (T_S7KVarXML *)malloc(strS7KRecordXML->sNbVarRD*sizeof(T_S7KVarXML));
	strS7KRecordXML->sNbVarOD = 7;
	strS7KRecordXML->strTabODXML= (T_S7KVarXML *)malloc(strS7KRecordXML->sNbVarOD*sizeof(T_S7KVarXML));

	iLen = strlen(cNomDatagram)+1;
	strS7KRecordXML->ptr_cNomHeader = malloc(iLen*sizeof(char));
	sprintf(strS7KRecordXML->ptr_cNomHeader, "%s", cNomDatagram);

	iLen = strlen(cTitleDatagram)+1;
	strS7KRecordXML->ptr_cLabelHeader = malloc(iLen*sizeof(char));
	sprintf(strS7KRecordXML->ptr_cLabelHeader, "%s", cTitleDatagram);

	iLen = strlen(cComments)+1;
	strS7KRecordXML->ptr_cCommentsHeader = malloc(iLen*sizeof(char));
	sprintf(strS7KRecordXML->ptr_cCommentsHeader, "%s", cComments);

	// Remplissage du nom de la variable d'indexation des tables.
	iLen = strlen(cTabNomSignalRTH[3])+1;
	strS7KRecordXML->ptr_cNomIndexTable = malloc(iLen*sizeof(char));
	sprintf(strS7KRecordXML->ptr_cNomIndexTable, "%s", cTabNomSignalRTH[3]);

	// Affectation du nom du repertoire.
	ptr_cNomHeader = strS7KRecordXML->ptr_cNomHeader;
	iSizeRep = ( strlen(G_cRepData) + strlen("_tmp") +  strlen( ptr_cNomHeader));
	// Ajout d'une marge au cas o� certains caract�res soient doubl�s soient doubl�s.
	ptr_cRepertoire = malloc( (iSizeRep + 100)*sizeof(char));
	sprintf(ptr_cRepertoire, "%s%s\\S7K_%s", G_cRepData, "_tmp", ptr_cNomHeader);
	if (DEBUG)
	   printf("-- Directory Creation : %s\n", ptr_cRepertoire);

	// Cr�ation du r�pertoire
	iErr = mkdir(ptr_cRepertoire);
	if (iErr != 0)
	{
	   switch (errno)
	   {
		   //"File exists"
		   case EEXIST:
			   break;
		   default:
			   printf("Error in Directory creation %s\n", strerror(errno));
			   return EXIT_FAILURE;
			   break;
	   }
	}
	// Initialisation g�n�rale des variables du paquet RTH
	for (iVarRTH=0; iVarRTH<strS7KRecordXML->sNbVarRTH; iVarRTH++) {
	   strS7KRecordXML->strTabRTHXML[iVarRTH].ptr_fpBin = 0;
	   strS7KRecordXML->strTabRTHXML[iVarRTH].bFlagConstant = TRUE;
	   strS7KRecordXML->strTabRTHXML[iVarRTH].usNbElem = 1;
	   sprintf(strS7KRecordXML->strTabRTHXML[iVarRTH].cNom, "%s", cTabNomSignalRTH[iVarRTH]);
	   sprintf(strS7KRecordXML->strTabRTHXML[iVarRTH].cType, "%s", cTabTypeSignalRTH[iVarRTH]);
	   sprintf(strS7KRecordXML->strTabRTHXML[iVarRTH].cTag, "%s", "TODO");
	   sprintf(strS7KRecordXML->strTabRTHXML[iVarRTH].cUnit, "%s", "TODO");
	   strS7KRecordXML->strTabRTHXML[iVarRTH].fScaleFactor = fTabSFSignal[iVarRTH];
	   strS7KRecordXML->strTabRTHXML[iVarRTH].fAddOffset = fTabAOSignal[iVarRTH];
	   usTaille = util_usNbOctetTypeVar(cTabTypeSignalRTH[iVarRTH]);
	   strS7KRecordXML->strTabRTHXML[iVarRTH].usSize = usTaille;
	   iLen = strlen(strS7KRecordXML->strTabRTHXML[iVarRTH].cNom);
	   // Ajout d'une marge au cas o� certains caract�res soient doubl�s soient doubl�s.
	   sprintf(cNomFicPathBin, "%s\\%s%s", ptr_cRepertoire, strS7KRecordXML->strTabRTHXML[iVarRTH].cNom, ".bin");
	   sprintf(strS7KRecordXML->strTabRTHXML[iVarRTH].cNomPathFileBin, "%s", cNomFicPathBin);
	   strS7KRecordXML->strTabRTHXML[iVarRTH].ptr_fpBin = fopen(cNomFicPathBin, "w+b");
	   if (!strS7KRecordXML->strTabRTHXML[iVarRTH].ptr_fpBin)
	   {
		   printf("%s -- Error in Signal Creation File  : %s\n", __FILE__, cNomFicPathBin);
		   return EXIT_FAILURE;
	   }
	}
	// Cas particulier pour la variable TypeAmpPhase.
	strS7KRecordXML->strTabRTHXML[9].usNbElem = 4;

	for (iVarRD=0; iVarRD<strS7KRecordXML->sNbVarRD; iVarRD++)
	{
	   strS7KRecordXML->strTabRDXML[iVarRD].ptr_fpBin = 0;
	   strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant = TRUE;
	   strS7KRecordXML->strTabRDXML[iVarRD].usNbElem = 1;
	   sprintf(strS7KRecordXML->strTabRDXML[iVarRD].cNom, "%s", cTabNomVarRD[iVarRD]);
	   sprintf(strS7KRecordXML->strTabRDXML[iVarRD].cType, "%s", cTabTypeVarRD[iVarRD]);
	   sprintf(strS7KRecordXML->strTabRDXML[iVarRD].cTag, "%s", "TODO");
	   sprintf(strS7KRecordXML->strTabRDXML[iVarRD].cUnit, "%s", "TODO");
	   strS7KRecordXML->strTabRDXML[iVarRD].fScaleFactor = fTabSFImage[iVarRD];
	   strS7KRecordXML->strTabRDXML[iVarRD].fAddOffset = fTabAOImage[iVarRD];
	   usTaille = util_usNbOctetTypeVar(cTabTypeVarRD[iVarRD]);
	   strS7KRecordXML->strTabRDXML[iVarRD].usSize = usTaille;
	   // Ajout d'une marge au cas o� certains caract�res soient doubl�s soient doubl�s.
	   sprintf(cNomFicPathBin, "%s\\%s%s", ptr_cRepertoire, strS7KRecordXML->strTabRDXML[iVarRD].cNom, ".bin");
	   strS7KRecordXML->strTabRDXML[iVarRD].ptr_fpBin = fopen(cNomFicPathBin, "w+b");
	   sprintf(strS7KRecordXML->strTabRDXML[iVarRD].cNomPathFileBin, "%s", cNomFicPathBin);
	   if (!strS7KRecordXML->strTabRDXML[iVarRD].ptr_fpBin)
	   {
		   printf("%s -- Error in Signal Creation File  : %s\n", __FILE__, cNomFicPathBin);
		   return EXIT_FAILURE;
	   }
	}

	// Traitement optionnel des OD.
	if (uiODOffset != 0)
	{
		for (iVarOD=0; iVarOD<strS7KRecordXML->sNbVarOD; iVarOD++)
		{
			strS7KRecordXML->strTabODXML[iVarOD].ptr_fpBin = 0;
			strS7KRecordXML->strTabODXML[iVarOD].bFlagConstant = TRUE;
			strS7KRecordXML->strTabODXML[iVarOD].usNbElem = 1;
			sprintf(strS7KRecordXML->strTabODXML[iVarOD].cNom, "%s", cTabNomVarOD[iVarOD]);
			sprintf(strS7KRecordXML->strTabODXML[iVarOD].cType, "%s", cTabTypeVarOD[iVarOD]);
			sprintf(strS7KRecordXML->strTabODXML[iVarOD].cTag, "%s", "TODO");
			sprintf(strS7KRecordXML->strTabODXML[iVarOD].cUnit, "%s", "TODO");
			usTaille = util_usNbOctetTypeVar(cTabTypeVarOD[iVarOD]);
			strS7KRecordXML->strTabODXML[iVarOD].usSize = usTaille;
			// Ajout d'une marge au cas o� certains caract�res soient doubl�s soient doubl�s.
			sprintf(cNomFicPathBin, "%s\\%s%s", ptr_cRepertoire, strS7KRecordXML->strTabODXML[iVarOD].cNom, ".bin");
			sprintf(strS7KRecordXML->strTabODXML[iVarOD].cNomPathFileBin, "%s", cNomFicPathBin);
			strS7KRecordXML->strTabODXML[iVarOD].ptr_fpBin = fopen(cNomFicPathBin, "w+b");
			if (!strS7KRecordXML->strTabODXML[iVarOD].ptr_fpBin)
			{
			   printf("%s -- Error in Signal Creation File  : %s\n", __FILE__, cNomFicPathBin);
			   return EXIT_FAILURE;
			}
		}
	}

	free(ptr_cRepertoire);

	return EXIT_SUCCESS;

} // iInitXMLBin7008Record

#pragma pack()
