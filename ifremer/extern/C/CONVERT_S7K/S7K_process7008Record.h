/*
 * S7K_process7008Record.h
 *
 *  Created on: 13 oct. 2008
 *      Author: rgallou
 */

#ifndef S7K_PROCESS7008RECORD_H_
#define S7K_PROCESS7008RECORD_H_
#pragma pack(1)

#include "S7K_DATAGRAMS.H"

typedef struct {
	u64		SonarId;
	u32		PingNumber;
	u16		MultiPingSequence;
	u16		N;
	u16		Reserved;
	u32		S;
	u8		RecordSubsetFlag;
	u8		RowColumnFlag;
	u16		Reserved2;
	u8		TypeAmpPhase[4];
} T_7008_RECORD_RTH;

// Paquet 7008 : partie RD
typedef struct {
	u16		*BeamDescriptor;
	u32		*BeginSampleDescriptor;
	u32		*EndSampleDescriptor;
	i16		*Samples;
} T_7008_RECORD_RD;


// Paquet 7008 : partie OD
typedef struct {
	f32	Frequency;
	f64	Latitude;
	f64	Longitude;
	f32	Heading;
	f32	*BeamAlongTrackDistance;
	f32	*BeamAcrossTrackDistance;
	u32	*CenterSampleDistance;
} T_7008_RECORD_OD;


int iProcess7008Record(FILE *G_fpData, int iNbRecord,
						u32 uiODOffset,
						T_S7KRecordXML *strS7KRecordXML,
						T_7008_RECORD_RTH *RTH_prev7008Record);

int iInitXMLBin7008Record(	char *cTitleDatagram,
							char *cNomDatagram, char *cComments,
							u32 uiODOffset,
							T_S7KRecordXML *strS7KRecordXML);
#pragma pack()
#endif /* S7K_PROCESS7008RECORD_H_ */
