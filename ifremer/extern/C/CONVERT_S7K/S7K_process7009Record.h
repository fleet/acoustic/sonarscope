/*
 * S7K_process7009Record.h
 *
 *  Created on: 13 oct. 2008
 *      Author: rgallou
 */

#ifndef S7K_PROCESS7009RECORD_H_
#define S7K_PROCESS7009RECORD_H_
#pragma pack(1)

#include "S7K_DATAGRAMS.H"

typedef struct {
	f32		Frequency;
	u32		PingNumber;
	u16		MultiPingSequence;
	f64		Latitude;
	f64		Longitude;
	f32		Heading;
	f32		AlongTrackDistance;
	f32		AcrossTrackDistance;
	f32		VerticalDepth;
} T_7009_RECORD_RTH;


int iProcess7009Record(FILE *G_fpData, int iNbRecord,
						u32 uiODOffset,
						T_S7KRecordXML *strS7KRecordXML,
						T_7009_RECORD_RTH *RTH_prev7009Record);

int iInitXMLBin7009Record(	char *cTitleDatagram,
							char *cNomDatagram, char *cComments,
							u32 uiODOffset,
							T_S7KRecordXML *strS7KRecordXML);
#pragma pack()
#endif /* S7K_PROCESS7009RECORD_H_ */
