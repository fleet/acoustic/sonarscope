/*
 * S7K_process7010Record.h
 *
 *  Created on: 13 oct. 2008
 *      Author: rgallou
 */

#ifndef S7K_PROCESS7010RECORD_H_
#define S7K_PROCESS7010RECORD_H_
#pragma pack(1)

#include "S7K_DATAGRAMS.H"

//-Paquet 7010 : partie RTH------------------------
typedef struct {
	u64		SonarId;
	u32		PingNumber;
	u16		MultiPingSequence;
	u32		N;
	u32		Reserved;
} T_7010_RECORD_RTH;

// Paquet 7010 : partie RD
typedef struct {
	f32		GainValue;
} T_7010_RECORD_RD;


int iProcess7010Record(FILE *G_fpData, int iNbRecord,
						u32 uiODOffset,
						T_S7KRecordXML *strS7KRecordXML,
						T_7010_RECORD_RTH *RTH_prev7010Record);


int iInitXMLBin7010Record(	char *cTitleDatagram,
							char *cNomDatagram, char *cComments,
							u32 uiODOffset,
							T_S7KRecordXML *strS7KRecordXML);
#pragma pack()
#endif /* S7K_PROCESS7010RECORD_H_ */
