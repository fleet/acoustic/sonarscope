/*
 * S7K_process7012Record.c
 *
 *  Created on: 13 oct. 2008
 *      Author: rgallou
 */
#pragma pack(1)

#include <sys/types.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#define NB_SIGNAL 			7
#define NB_IMAGE 			4
#define NB_IMAGE2 			0
#define NUM_INDEX_TABLE		3	// Identification de l'index Signal des Images (� partir de 0).
#define NUM_INDEX_TABLE2	0	// Identification de l'index Signal des Images (� partir de 0).

#include "S7K_Datagrams.h"
#include "S7K_Utilities.h"
#include "S7K_process7012Record.h"
#include "convertFiles_S7K.h"


//----------------------------------------------------------------
// Fonction :	iProcess7012Record
// Objet 	:	Traitement du paquet 7012
// Modif.	: 	03/11/08
// Auteur 	:	GLU
//----------------------------------------------------------------
int iProcess7012Record(FILE *G_fpData,
						int iNbRecord,
						u32 uiODOffset,
						T_S7KRecordXML *strS7KRecordXML,
						T_7012_RECORD_RTH *RTH_prev7012Record)
{
    T_7012_RECORD_RTH 	*RTH_7012Record;
    T_7012_RECORD_RD 	*RD_7012Record;


	// Pointeur pour avancer sur les variables automatiquement.
	unsigned char		*ptr_cRTHRecord;
	static void 		*ptr_vVarPrevRecord;
	void				*ptr_vVarRecord,
						*ptr_vBidon;

	BOOL		bFlagVarConstant = FALSE,
				bFlagCompare = FALSE;

    int 	iRet,
			iFlag,
			iVarRD,
			iVarRTH,
			iLoop,
			iRetour = 0,
			iFlagPitch, iFlagRoll, iFlagHeading, iFlagHeave;

    char *cValeur;

	

    unsigned short	usTailleEnByte,
					usNbElem;


    // Initialisation de la structure d'identification du paquet
 	if (iNbRecord == 0)
	{
 		iInitXMLBin7012Record("Ping Motion Data", "7012", "7k Ping Motion Data", strS7KRecordXML);
	}
    // Lecture du RTH du paquet
 	RTH_7012Record = (T_7012_RECORD_RTH *)malloc(sizeof(T_7012_RECORD_RTH));
 	iFlag = fread(RTH_7012Record, sizeof(T_7012_RECORD_RTH), 1, G_fpData);
 	if (iFlag == 0)
 	{
 		printf("Error in reading : %s\n", G_cFileData);
 		return FALSE;
 	}
 	ptr_cRTHRecord = (unsigned char*)RTH_7012Record;
 	// Sauvegarde des donn�es des attributs.
 	if (iNbRecord > 0)
 	{
 		ptr_vVarPrevRecord = (void *)RTH_prev7012Record;
 	}
 	for (iVarRTH=0; iVarRTH<(int)strS7KRecordXML->sNbVarRTH; iVarRTH++)
 	{
 		ptr_vVarRecord = (void *)ptr_cRTHRecord;
 		if (iVarRTH==0)
 		{
 			usTailleEnByte = 0;
 			usNbElem = 0;
 		}
 		else
 		{
 			usTailleEnByte = util_usNbOctetTypeVar(strS7KRecordXML->strTabRTHXML[iVarRTH-1].cType);
 			usNbElem = strS7KRecordXML->strTabRTHXML[iVarRTH-1].usNbElem;
 		}
 		if (iNbRecord > 0)
 		{
 			bFlagVarConstant = strS7KRecordXML->strTabRTHXML[iVarRTH].bFlagConstant;
 			ptr_vVarPrevRecord = ptr_vVarPrevRecord + usTailleEnByte*usNbElem;
 			bFlagCompare = util_bComparePrevAndCurrentVal(strS7KRecordXML->strTabRTHXML[iVarRTH].cType,
 												ptr_vVarPrevRecord,
 												ptr_vVarRecord,
 											   strS7KRecordXML->strTabRTHXML[iVarRTH].usNbElem);
 			bFlagVarConstant = bFlagVarConstant && bFlagCompare;
 			strS7KRecordXML->strTabRTHXML[iVarRTH].bFlagConstant = bFlagVarConstant;
 		}
 		// Ecriture de la variable.
 		usTailleEnByte 	= strS7KRecordXML->strTabRTHXML[iVarRTH].usSize;
 		usNbElem 		=  strS7KRecordXML->strTabRTHXML[iVarRTH].usNbElem;
 		fwrite(ptr_cRTHRecord, usTailleEnByte, usNbElem, strS7KRecordXML->strTabRTHXML[iVarRTH].ptr_fpBin);
 	    strS7KRecordXML->strTabRTHXML[iVarRTH].ptr_vPrevVal = ptr_cRTHRecord;
 		ptr_cRTHRecord += usTailleEnByte*usNbElem;

 		iFlagPitch 		= util_GetBits((unsigned short)*ptr_cRTHRecord, 0, 1);
 		iFlagRoll 		= util_GetBits((unsigned short)*ptr_cRTHRecord, 1, 1);
 		iFlagHeading 	= util_GetBits((unsigned short)*ptr_cRTHRecord, 2, 1);
 		iFlagHeave 		= util_GetBits((unsigned short)*ptr_cRTHRecord, 3, 1);

 	}

 	// Recopie en zone tampon des valeurs courantes.
 	memmove(RTH_prev7012Record, RTH_7012Record, sizeof(T_7012_RECORD_RTH));

	// --------------------------------------------
 	// Traitement des paquets RD
 	RD_7012Record = (T_7012_RECORD_RD *)malloc(sizeof(T_7012_RECORD_RD));

	// Lecture unique de la donn�e Pitch.
	iVarRD = 0;
	if (iFlagPitch == 1)
	{
		usTailleEnByte 	= strS7KRecordXML->strTabRDXML[iVarRD].usSize;
		usNbElem 		= strS7KRecordXML->strTabRDXML[iVarRD].usNbElem;
		iFlag 			= fread(&RD_7012Record->Pitch, usTailleEnByte, usNbElem, G_fpData);
		fwrite(&RD_7012Record->Pitch, usTailleEnByte, usNbElem, strS7KRecordXML->strTabRDXML[iVarRD].ptr_fpBin);
		if (iNbRecord > 0 && strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant == TRUE)
		{
			bFlagCompare = util_bComparePrevAndCurrentVal(strS7KRecordXML->strTabRDXML[iVarRD].cType,
												strS7KRecordXML->strTabRDXML[iVarRD].ptr_vPrevVal,
												(void *)&RD_7012Record->Pitch,
												usNbElem);
			strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant = bFlagCompare && strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant;
		}
		if (iNbRecord == 0)
		{
			strS7KRecordXML->strTabRDXML[iVarRD].ptr_vPrevVal = malloc(usNbElem*usTailleEnByte);
		}
		memmove(strS7KRecordXML->strTabRDXML[iVarRD].ptr_vPrevVal, &RD_7012Record->Pitch, usNbElem*usTailleEnByte);
	}

	// Lecture sur N Samples de la donn�e.
	iVarRD++;
	if (iFlagPitch == 1)
	{
		if (iNbRecord == 0)
		{
			RD_7012Record->Roll 	= (float *)malloc(RTH_7012Record->N*sizeof(float));
		}
		usTailleEnByte 			= strS7KRecordXML->strTabRDXML[iVarRD].usSize;
		usNbElem 				= RTH_7012Record->N*strS7KRecordXML->strTabRDXML[iVarRD].usNbElem;

		iFlag 	= fread(&RD_7012Record->Roll, usTailleEnByte, usNbElem, G_fpData);
		iFlag 	= fwrite(&RD_7012Record->Roll, usTailleEnByte, usNbElem, strS7KRecordXML->strTabRDXML[iVarRD].ptr_fpBin);
		if (iNbRecord > 0 && strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant == TRUE)
		{
			bFlagCompare = util_bComparePrevAndCurrentVal(strS7KRecordXML->strTabRDXML[iVarRD].cType,
												strS7KRecordXML->strTabRDXML[iVarRD].ptr_vPrevVal,
												(void *)&RD_7012Record->Roll,
												usNbElem);
			strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant = bFlagCompare && strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant;
		}
		if (iNbRecord == 0)
		{
			strS7KRecordXML->strTabRDXML[iVarRD].ptr_vPrevVal = malloc(usNbElem*usTailleEnByte);
		}
		memmove(strS7KRecordXML->strTabRDXML[iVarRD].ptr_vPrevVal, &RD_7012Record->Roll, usNbElem*usTailleEnByte);
	}

	iVarRD++;
	if (iFlagHeading == 1)
	{
		if (iNbRecord == 0)
		{
			RD_7012Record->Heading 	= (float *)malloc(RTH_7012Record->N*sizeof(float));
		}
		usTailleEnByte 			= strS7KRecordXML->strTabRDXML[iVarRD].usSize;
		usNbElem 				= RTH_7012Record->N*strS7KRecordXML->strTabRDXML[iVarRD].usNbElem;

		iFlag 	= fread(&RD_7012Record->Heading, usTailleEnByte, usNbElem, G_fpData);
		iFlag 	= fwrite(&RD_7012Record->Heading, usTailleEnByte, usNbElem, strS7KRecordXML->strTabRDXML[iVarRD].ptr_fpBin);
		if (iNbRecord > 0 && strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant == TRUE)
		{
			bFlagCompare = util_bComparePrevAndCurrentVal(strS7KRecordXML->strTabRDXML[iVarRD].cType,
												strS7KRecordXML->strTabRDXML[iVarRD].ptr_vPrevVal,
												(void *)&RD_7012Record->Heading,
												usNbElem);
			strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant = bFlagCompare && strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant;
		}
		if (iNbRecord == 0)
		{
			strS7KRecordXML->strTabRDXML[iVarRD].ptr_vPrevVal = malloc(usNbElem*usTailleEnByte);
		}
		memmove(strS7KRecordXML->strTabRDXML[iVarRD].ptr_vPrevVal, &RD_7012Record->Heading, usNbElem*usTailleEnByte);
	}

	iVarRD++;
	if (iFlagHeave == 1)
	{
		if (iNbRecord == 0)
		{
			RD_7012Record->Heave 	= (float *)malloc(RTH_7012Record->N*sizeof(float));
		}
		usTailleEnByte 			= strS7KRecordXML->strTabRDXML[iVarRD].usSize;
		usNbElem 				= RTH_7012Record->N*strS7KRecordXML->strTabRDXML[iVarRD].usNbElem;

		iFlag 	= fread(&RD_7012Record->Heave, usTailleEnByte, usNbElem, G_fpData);
		iFlag 	= fwrite(&RD_7012Record->Heave, usTailleEnByte, usNbElem, strS7KRecordXML->strTabRDXML[iVarRD].ptr_fpBin);
		if (iNbRecord > 0 && strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant == TRUE)
		{
			bFlagCompare = util_bComparePrevAndCurrentVal(strS7KRecordXML->strTabRDXML[iVarRD].cType,
												strS7KRecordXML->strTabRDXML[iVarRD].ptr_vPrevVal,
												(void *)&RD_7012Record->Heave,
												usNbElem);
			strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant = bFlagCompare && strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant;
		}
		if (iNbRecord == 0)
		{
			strS7KRecordXML->strTabRDXML[iVarRD].ptr_vPrevVal = malloc(usNbElem*usTailleEnByte);
		}
		memmove(strS7KRecordXML->strTabRDXML[iVarRD].ptr_vPrevVal, &RD_7012Record->Heave, usNbElem*usTailleEnByte);
	}

 	iNbRecord++;

 	free(RTH_7012Record);

	return iRetour;

} //iProcess7012Record


//----------------------------------------------------------------
// Fonction :	iInitXMLBin7012Record
// Objet 	:	Initialisation de la structure �quivalent au paquet
//				7012 pour la manipulation des fichiers bin et XML.
// Modification : 15/10/08
// Auteur 	: GLU
//----------------------------------------------------------------
int iInitXMLBin7012Record(	char *cTitleDatagram,
							char *cNomDatagram,
							char *cComments,
							T_S7KRecordXML *strS7KRecordXML)

{
	int iRet = 0;

	T_DESC_SIGNAL_OR_IMAGE strDescriptionSignal[NB_SIGNAL] = {
			{	"SonarId","u64",				1.0, 0.0},
			{	"PingNumber","u32", 			1.0, 0.0},
			{	"MultiPingSequence","u16", 		1.0, 0.0},
			{	"N","u32",						1.0, 0.0},
			{	"Flags","u16", 					1.0, 0.0},
			{	"ErrorFlags","u32", 			1.0, 0.0},
			{	"SamplingRate","f32", 			1.0, 0.0}};

	T_DESC_SIGNAL_OR_IMAGE strDescriptionImage[NB_IMAGE] = {
			{	"Pitch","u16",					1.0, 0.0},
			{	"Roll","u32",					1.0, 0.0},
			{	"Heading","u32",				1.0, 0.0},
			{	"Heave","u32",					1.0, 0.0}};


	T_DESC_SIGNAL_OR_IMAGE strDescriptionImage2[NB_IMAGE2] = {};

	T_DESC_TAB_SIGANDIMG strDescriptionTab = {	NB_SIGNAL,
												NB_IMAGE,
												NB_IMAGE2,
												NUM_INDEX_TABLE,
												strDescriptionSignal,
												strDescriptionImage,
												strDescriptionImage2};

	// Lancement de l'initialisation des structures de signaux : ouvertures des
	// fichiers, copie des structures, ...
	iRet = utilS7K_initXMLRecord(	strDescriptionTab,
									cTitleDatagram,
									cNomDatagram,
									cComments,
									strS7KRecordXML);


	return EXIT_SUCCESS;

} // iInitXMLBin7012Record

#pragma pack()
