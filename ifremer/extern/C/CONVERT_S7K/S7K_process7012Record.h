/*
 * S7K_process7012Record.h
 *
 *  Created on: 19 09 2011
 *      Author: rgallou
 */

#ifndef S7K_PROCESS7012RECORD_H_
#define S7K_PROCESS7012RECORD_H_
#pragma pack(1)

#include "S7K_DATAGRAMS.H"

//-Paquet 7012 : partie RTH------------------------
typedef struct {
	u64		SonarId;
	u32		PingNumber;
	u16		MultiPingSequence;
	u32		N;
	u16		Flags;
	u32		ErrorFlags;
	f32		SamplingRate;
} T_7012_RECORD_RTH;

// Paquet 7012 : partie RD
typedef struct {
	float		Pitch;
	float		*Roll;
	float		*Heading;
	float		*Heave;
	// u16		Reserved;
} T_7012_RECORD_RD;


int iProcess7012Record(FILE *G_fpData,
						int iNbRecord,
						u32 uiODOffset,
						T_S7KRecordXML *strS7KRecordXML,
						T_7012_RECORD_RTH *RTH_prev7012Record);

int iInitXMLBin7012Record(	char *cTitleDatagram,
							char *cNomDatagram,
							char *cComments,
							T_S7KRecordXML *strS7KRecordXML);

#pragma pack()

#endif /* S7K_PROCESS7012RECORD_H_ */
