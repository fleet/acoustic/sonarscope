/*
 * S7K_process7018Record.c
 *
 *  Created on: 13 oct. 2008
 *      Author: rgallou
 */
#pragma pack(1)

#define NB_SIGNAL 			6
#define NB_IMAGE 			2
#define NB_IMAGE2 			0
#define NUM_INDEX_TABLE		3	// Identification de l'index Signal des Images (� partir de 0).
#define NUM_INDEX_TABLE2	4	// Identification de l'index Signal des Images (� partir de 0).


#include <sys/types.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "S7K_Datagrams.h"
#include "S7K_Utilities.h"
#include "S7K_process7018Record.h"
#include "convertFiles_S7K.h"


//----------------------------------------------------------------
// Fonction :	iProcess7018Record
// Objet 	:	Traitement du paquet 7018
// Modif.	: 	03/11/08
// Auteur 	:	GLU
//----------------------------------------------------------------
int iProcess7018Record(FILE *G_fpData, int iNbRecord,
						u32 uiODOffset,
						T_S7KRecordXML *strS7KRecordXML,
						T_7018_RECORD_RTH *RTH_prev7018Record)
{
    T_7018_RECORD_RTH 	*RTH_7018Record;
    T_7018_RECORD_RD 	*RD_7018Record;

	// Pointeur pour avancer sur les variables automatiquement.
	unsigned char		*ptr_cRTHRecord,
						*ptr_cRDRecord;
	static void 		*ptr_vVarPrevRecord;
	void				*ptr_vVarRecord,
						*ptr_vBidon;

	BOOL		bFlagVarConstant = TRUE,
				bFlagCompare = TRUE;

    int 	iFlag,
			iVarRD,
			iVarRTH,
			iRetour = 0,
			iSample,
			iBeam,
			iFormatVersion,
			iTailleBuffer;

    unsigned int	uiNbSamples;

    unsigned short	usTailleEnByte,
					usNbElem,
					usNbBeams;

    unsigned char 	cDummy[5];

    // Conservation de l'information de pr�sence de donn�es Optionnal Data.
    strS7KRecordXML->uiODOffset = uiODOffset;

    // Initialisation de la structure d'identification du paquet
 	if (iNbRecord == 0)
	{
 		iInitXMLBin7018Record("7k Beamformed Data", "7018", "7k Beamformed Data", uiODOffset, strS7KRecordXML);
	}

    // Lecture du RTH du paquet
	RTH_7018Record 	= (T_7018_RECORD_RTH *)malloc(sizeof(T_7018_RECORD_RTH));
	iFlag 			= fread(RTH_7018Record, sizeof(T_7018_RECORD_RTH), 1, G_fpData);

	if (iFlag == 0)
	{
		printf("Error in reading : %s\n", G_cFileData);
		return FALSE;
	}

	ptr_cRTHRecord = (unsigned char*)RTH_7018Record;
	// Sauvegarde des donn�es des attributs.
	if (iNbRecord > 0)
	{
		ptr_vVarPrevRecord = (void *)RTH_prev7018Record;
	}
	// On n'exploite pas Reserved mais on le lit pour traiter le cas de l'ancien format.
	for (iVarRTH=0; iVarRTH<(int)strS7KRecordXML->sNbVarRTH; iVarRTH++)
	{
		ptr_vVarRecord = (void *)ptr_cRTHRecord;
		if (iVarRTH==0)
		{
			usTailleEnByte = 0;
			usNbElem = 0;
		}
		else
		{
			usTailleEnByte = util_usNbOctetTypeVar(strS7KRecordXML->strTabRTHXML[iVarRTH-1].cType);
			usNbElem = strS7KRecordXML->strTabRTHXML[iVarRTH-1].usNbElem;
		}

		if (iNbRecord > 0)
		{
			bFlagVarConstant = strS7KRecordXML->strTabRTHXML[iVarRTH].bFlagConstant;
			ptr_vVarPrevRecord = ptr_vVarPrevRecord + usTailleEnByte*usNbElem;
			bFlagCompare = util_bComparePrevAndCurrentVal(strS7KRecordXML->strTabRTHXML[iVarRTH].cType,
												ptr_vVarPrevRecord,
												ptr_vVarRecord,
											   strS7KRecordXML->strTabRTHXML[iVarRTH].usNbElem);
			bFlagVarConstant = bFlagVarConstant && bFlagCompare;
			strS7KRecordXML->strTabRTHXML[iVarRTH].bFlagConstant = bFlagVarConstant;
		}
		// Pr�paration de l'�criture de la variable.
		usTailleEnByte 	= strS7KRecordXML->strTabRTHXML[iVarRTH].usSize;
		usNbElem 		= strS7KRecordXML->strTabRTHXML[iVarRTH].usNbElem;
		// Si on lit le nb de Beams, traitement de deux types de formats (cf. code MatLab)
		if (iVarRTH == 3)
		{
			if (RTH_7018Record->NBeams == 0)
			{
				iFormatVersion = 1;

				// On avance de deux octects dans le buffer de lecture et on recopie dans la structure les
				// valeurs r�cup�r�es.
				ptr_cRTHRecord += 2;
				memcpy(&usNbBeams, ptr_cRTHRecord, usTailleEnByte);
			}
			else
			{
				iFormatVersion = 2;
			}
		}
		else if (iVarRTH == 4 && iFormatVersion == 1)
		{
			memcpy(&uiNbSamples, ptr_cRTHRecord, usTailleEnByte);
		}

		fwrite(ptr_cRTHRecord, usTailleEnByte, usNbElem, strS7KRecordXML->strTabRTHXML[iVarRTH].ptr_fpBin);
		strS7KRecordXML->strTabRTHXML[iVarRTH].ptr_vPrevVal = ptr_cRTHRecord;
		ptr_cRTHRecord += usTailleEnByte*usNbElem;
	}
	if (iFormatVersion == 1)
	{
		// Lecture de 3 + 2 (FirstBeam) bytes en plus de Reserved1 dans le cas de l'ancien format.
		iFlag = fread(&cDummy, sizeof(char), 5, G_fpData);
		memcpy(&RTH_7018Record->NBeams, &usNbBeams, sizeof(unsigned short));
		memcpy(&RTH_7018Record->NSamples, &uiNbSamples, sizeof(unsigned int));
	}

	// Recopie en zone tampon des valeurs courantes.
	memmove(RTH_prev7018Record, RTH_7018Record, sizeof(T_7018_RECORD_RTH));


	iTailleBuffer = RTH_prev7018Record->NSamples*RTH_prev7018Record->NBeams*sizeof(short)*2;
	RD_7018Record = malloc(iTailleBuffer);

	iFlag = fread(RD_7018Record, iTailleBuffer, 1, G_fpData);
	ptr_cRDRecord = (unsigned char*)RD_7018Record;
	for (iSample=0; iSample< RTH_prev7018Record->NSamples; iSample++)
	{
		// Lecture unitaire des variables.
		for (iBeam=0; iBeam< RTH_prev7018Record->NBeams; iBeam++)
		{
			for (iVarRD=0; iVarRD<strS7KRecordXML->sNbVarRD; iVarRD++)
			{
				if (iNbRecord == 0 && strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant == TRUE)
				{
					strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant = FALSE;
				}

				usTailleEnByte 	= util_usNbOctetTypeVar(strS7KRecordXML->strTabRDXML[iVarRD].cType);
				usNbElem 		= strS7KRecordXML->strTabRDXML[iVarRD].usNbElem;

				/*
				unsigned short usAmp;
				short sPhs;
				if (iVarRD == 0)
					memcpy(&usAmp, ptr_cRDRecord, sizeof(unsigned short));
				else
					memcpy(&sPhs, ptr_cRDRecord, sizeof(short));
				}
				 */

				// Conservation de la variable dans le fichier binaire (en attendant de le supprimer).
				iFlag = fwrite(ptr_cRDRecord, usTailleEnByte, usNbElem, strS7KRecordXML->strTabRDXML[iVarRD].ptr_fpBin);
				ptr_cRDRecord += usTailleEnByte*usNbElem;


			} // Fin de la boucles sur les images.
		}
	}

	// D�sallocation (obligatoire !) des matrices r�serv�es.
	free(RD_7018Record);
	free(RTH_7018Record);

	iNbRecord++;


	return iRetour;

} //iProcess7018Record


//----------------------------------------------------------------
// Fonction :	iInitXMLBin7018Record
// Objet 	:	Initialisation de la structure �quivalent au paquet
//				7018 pour la manipulation des fichiers bin et XML.
// Modification : 15/10/08
// Auteur 	: GLU
//----------------------------------------------------------------
int iInitXMLBin7018Record(	char *cTitleDatagram,
							char *cNomDatagram,
							char *cComments,
							u32 iFlagOD,
							T_S7KRecordXML *strS7KRecordXML)
{

	int iRet;

	T_DESC_SIGNAL_OR_IMAGE strDescriptionSignal[NB_SIGNAL] = {
			{	"SonarId","u64",				1.0, 0.0},
			{	"PingNumber","u32", 			1.0, 0.0},
			{	"MultiPingSequence","u16", 		1.0, 0.0},
			{	"NBeams","u16",					1.0, 0.0},
			{	"NSamples","u32", 				1.0, 0.0},
			{	"Reserved1","u32", 				1.0, 0.0}};

	T_DESC_SIGNAL_OR_IMAGE strDescriptionImage[NB_IMAGE] = {
			{	"Amp","u16",					1.0, 0.0},
			{	"Phs","short", 					1.0, 0.0}};


	T_DESC_SIGNAL_OR_IMAGE strDescriptionImage2[NB_IMAGE2] = {};

	T_DESC_TAB_SIGANDIMG strDescriptionTab = {	NB_SIGNAL,
												NB_IMAGE,
												NB_IMAGE2,
												NUM_INDEX_TABLE,
												strDescriptionSignal,
												strDescriptionImage,
												strDescriptionImage2};

	// Lancement de l'initialisation des structures de signaux : ouvertures des
	// fichiers, copie des structures, ...
	iRet = utilS7K_initXMLRecord(	strDescriptionTab,
									cTitleDatagram,
									cNomDatagram,
									cComments,
									strS7KRecordXML);



	// Cas particulier pour la variable Reserved1.
	strS7KRecordXML->strTabRTHXML[5].usNbElem = 8;

	return EXIT_SUCCESS;

} // iInitXMLBin7018Record

#pragma pack()
