/*
 * S7K_process7018Record.h
 *
 *  Created on: 13 oct. 2008
 *      Author: rgallou
 */

#ifndef S7K_PROCESS7018RECORD_H_
#define S7K_PROCESS7018RECORD_H_
#pragma pack(1)

#include "S7K_DATAGRAMS.H"


typedef struct {
	u64		SonarId;
	u32		PingNumber;
	u16		MultiPingSequence;
	u16		NBeams;
	u32		NSamples;
	u32		Reserved1[8];
} T_7018_RECORD_RTH;


// Paquet 7018 : partie RD
typedef struct {
	u16		*Amp;
	i16		*Phs;
} T_7018_RECORD_RD;


int iProcess7018Record(FILE *G_fpData, int iNbRecord,
						u32 uiODOffset,
						T_S7KRecordXML *strS7KRecordXML,
						T_7018_RECORD_RTH *RTH_prev7018Record);

int iInitXMLBin7018Record(	char *cTitleDatagram,
							char *cNomDatagram, char *cComments,
							u32 uiODOffset,
							T_S7KRecordXML *strS7KRecordXML);
#pragma pack()
#endif /* S7K_PROCESS7018RECORD_H_ */
