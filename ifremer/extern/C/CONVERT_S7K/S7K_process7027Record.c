/*
 * S7K_process7027Record.c
 *
 *  Created on: 13 oct. 2008
 *      Author: rgallou
 */
#pragma pack(1)

#include <sys/types.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#define NB_SIGNAL 			10
#define NB_IMAGE 			6
#define NB_IMAGE2 			0
#define NUM_INDEX_TABLE		3	// Identification de l'index Signal des Images (� partir de 0).
#define NUM_INDEX_TABLE2	0	// Identification de l'index Signal des Images (� partir de 0).

#include "S7K_Datagrams.h"
#include "S7K_Utilities.h"
#include "S7K_process7027Record.h"
#include "convertFiles_S7K.h"


//----------------------------------------------------------------
// Fonction :	iProcess7027Record
// Objet 	:	Traitement du paquet 7027
// Modif.	: 	03/11/08
// Auteur 	:	GLU
//----------------------------------------------------------------
int iProcess7027Record(FILE *G_fpData,
						int iNbRecord,
						u32 uiODOffset,
						T_S7KRecordXML *strS7KRecordXML,
						T_7027_RECORD_RTH *RTH_prev7027Record)
{
    T_7027_RECORD_RTH 	*RTH_7027Record;
    T_7027_RECORD_RD 	*RD_7027Record;


	// Pointeur pour avancer sur les variables automatiquement.
	unsigned char		*ptr_cRTHRecord;
	static void 		*ptr_vVarPrevRecord;
	void				*ptr_vVarRecord,
						*ptr_vBidon;

	BOOL		bFlagVarConstant = FALSE,
				bFlagCompare = FALSE;

    int 	iRet,
			iFlag,
			iVarRD,
			iVarRTH,
			iLoop,
			iRetour = 0;

    char *cValeur;

	

    unsigned short	usTailleEnByte,
					usNbElem;


    // Initialisation de la structure d'identification du paquet
 	if (iNbRecord == 0)
	{
 		iInitXMLBin7027Record("RAW Detection Data", "7027", "7k RAW Detection Data", strS7KRecordXML);
	}
    // Lecture du RTH du paquet
 	RTH_7027Record = (T_7027_RECORD_RTH *)malloc(sizeof(T_7027_RECORD_RTH));
 	iFlag = fread(RTH_7027Record, sizeof(T_7027_RECORD_RTH), 1, G_fpData);
 	if (iFlag == 0)
 	{
 		printf("Error in reading : %s\n", G_cFileData);
 		return FALSE;
 	}
 	ptr_cRTHRecord = (unsigned char*)RTH_7027Record;
 	// Sauvegarde des donn�es des attributs.
 	if (iNbRecord > 0)
 	{
 		ptr_vVarPrevRecord = (void *)RTH_prev7027Record;
 	}
 	for (iVarRTH=0; iVarRTH<(int)strS7KRecordXML->sNbVarRTH; iVarRTH++)
 	{
 		ptr_vVarRecord = (void *)ptr_cRTHRecord;
 		if (iVarRTH==0)
 		{
 			usTailleEnByte = 0;
 			usNbElem = 0;
 		}
 		else
 		{
 			usTailleEnByte = util_usNbOctetTypeVar(strS7KRecordXML->strTabRTHXML[iVarRTH-1].cType);
 			usNbElem = strS7KRecordXML->strTabRTHXML[iVarRTH-1].usNbElem;
 		}
 		if (iNbRecord > 0)
 		{
 			bFlagVarConstant = strS7KRecordXML->strTabRTHXML[iVarRTH].bFlagConstant;
 			ptr_vVarPrevRecord = ptr_vVarPrevRecord + usTailleEnByte*usNbElem;
 			bFlagCompare = util_bComparePrevAndCurrentVal(strS7KRecordXML->strTabRTHXML[iVarRTH].cType,
 												ptr_vVarPrevRecord,
 												ptr_vVarRecord,
 											   strS7KRecordXML->strTabRTHXML[iVarRTH].usNbElem);
 			bFlagVarConstant = bFlagVarConstant && bFlagCompare;
 			strS7KRecordXML->strTabRTHXML[iVarRTH].bFlagConstant = bFlagVarConstant;
 		}
 		// Ecriture de la variable.
 		usTailleEnByte = strS7KRecordXML->strTabRTHXML[iVarRTH].usSize;
 		usNbElem =  strS7KRecordXML->strTabRTHXML[iVarRTH].usNbElem;
 		fwrite(ptr_cRTHRecord, usTailleEnByte, usNbElem, strS7KRecordXML->strTabRTHXML[iVarRTH].ptr_fpBin);
 	    strS7KRecordXML->strTabRTHXML[iVarRTH].ptr_vPrevVal = ptr_cRTHRecord;
 		ptr_cRTHRecord += usTailleEnByte*usNbElem;
 	}

 	// Recopie en zone tampon des valeurs courantes.
 	memmove(RTH_prev7027Record, RTH_7027Record, sizeof(T_7027_RECORD_RTH));

	// --------------------------------------------
 	// Traitement des paquets RD
 	RD_7027Record = (T_7027_RECORD_RD *)malloc(sizeof(T_7027_RECORD_RD));
 	// Lecture des datas de fa�on adapt�e � lecture des Samples sur N Detection Point.
 	for (iLoop = 0; iLoop < RTH_7027Record->N; iLoop++)
    {
    	// Ecriture des donn�es de la partie RD dans les binaires de stockage.
    	iVarRD = 0;
    	usTailleEnByte 	= strS7KRecordXML->strTabRDXML[iVarRD].usSize;
    	usNbElem 		= strS7KRecordXML->strTabRDXML[iVarRD].usNbElem;
    	iFlag 			= fread(&RD_7027Record->BeamDescriptor, usTailleEnByte, usNbElem, G_fpData);
    	fwrite(&RD_7027Record->BeamDescriptor, usTailleEnByte, usNbElem, strS7KRecordXML->strTabRDXML[iVarRD].ptr_fpBin);
    	if (iNbRecord > 0 && strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant == TRUE)
    	{
    		bFlagCompare = util_bComparePrevAndCurrentVal(strS7KRecordXML->strTabRDXML[iVarRD].cType,
    											strS7KRecordXML->strTabRDXML[iVarRD].ptr_vPrevVal,
    											(void *)&RD_7027Record->BeamDescriptor,
    											usNbElem);
    		strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant = bFlagCompare && strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant;
    	}
    	if (iNbRecord == 0)
    	{
    		strS7KRecordXML->strTabRDXML[iVarRD].ptr_vPrevVal = malloc(usNbElem*usTailleEnByte);
    	}
    	memmove(strS7KRecordXML->strTabRDXML[iVarRD].ptr_vPrevVal, &RD_7027Record->BeamDescriptor, usNbElem*usTailleEnByte);

    	iVarRD++;
    	usTailleEnByte 	= strS7KRecordXML->strTabRDXML[iVarRD].usSize;
    	usNbElem 		= strS7KRecordXML->strTabRDXML[iVarRD].usNbElem;
    	iFlag 			= fread(&RD_7027Record->DetectionPoint, usTailleEnByte, usNbElem, G_fpData);
    	fwrite(&RD_7027Record->DetectionPoint, usTailleEnByte, usNbElem, strS7KRecordXML->strTabRDXML[iVarRD].ptr_fpBin);
    	if (iNbRecord > 0 && strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant == TRUE)
    	{
    		bFlagCompare = util_bComparePrevAndCurrentVal(strS7KRecordXML->strTabRDXML[iVarRD].cType,
    											strS7KRecordXML->strTabRDXML[iVarRD].ptr_vPrevVal,
    											(void *)&RD_7027Record->DetectionPoint,
    											usNbElem);
    		strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant = bFlagCompare && strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant;
    	}
    	if (iNbRecord == 0)
    	{
    		strS7KRecordXML->strTabRDXML[iVarRD].ptr_vPrevVal = malloc(usNbElem*usTailleEnByte);
    	}
    	memmove(strS7KRecordXML->strTabRDXML[iVarRD].ptr_vPrevVal, &RD_7027Record->DetectionPoint, usNbElem*usTailleEnByte);


    	iVarRD++;
    	usTailleEnByte 	= strS7KRecordXML->strTabRDXML[iVarRD].usSize;
    	usNbElem 		= strS7KRecordXML->strTabRDXML[iVarRD].usNbElem;
    	iFlag 			= fread(&RD_7027Record->RxAngle, usTailleEnByte, usNbElem, G_fpData);
    	fwrite(&RD_7027Record->RxAngle, usTailleEnByte, usNbElem, strS7KRecordXML->strTabRDXML[iVarRD].ptr_fpBin);
    	if (iNbRecord > 0 && strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant == TRUE)
    	{
    		bFlagCompare = util_bComparePrevAndCurrentVal(strS7KRecordXML->strTabRDXML[iVarRD].cType,
    											strS7KRecordXML->strTabRDXML[iVarRD].ptr_vPrevVal,
    											(void *)&RD_7027Record->RxAngle,
    											usNbElem);
    		strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant = bFlagCompare && strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant;
    	}
    	strS7KRecordXML->strTabRDXML[iVarRD].ptr_vPrevVal = malloc(usNbElem*usTailleEnByte);
    	memmove(strS7KRecordXML->strTabRDXML[iVarRD].ptr_vPrevVal, &RD_7027Record->RxAngle, usNbElem*usTailleEnByte);


    	iVarRD++;
    	usTailleEnByte 		= strS7KRecordXML->strTabRDXML[iVarRD].usSize;
    	usNbElem 			= strS7KRecordXML->strTabRDXML[iVarRD].usNbElem;
    	iFlag 				= fread(&RD_7027Record->Flags, usTailleEnByte, usNbElem, G_fpData);
    	fwrite(&RD_7027Record->Flags, usTailleEnByte, usNbElem, strS7KRecordXML->strTabRDXML[iVarRD].ptr_fpBin);
    	if (iNbRecord > 0 && strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant == TRUE)
    	{
    		bFlagCompare = util_bComparePrevAndCurrentVal(strS7KRecordXML->strTabRDXML[iVarRD].cType,
    											strS7KRecordXML->strTabRDXML[iVarRD].ptr_vPrevVal,
    											(void *)&RD_7027Record->Flags,
    											usNbElem);
    		strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant = bFlagCompare && strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant;
    	}
    	if (iNbRecord == 0)
    	{
    		strS7KRecordXML->strTabRDXML[iVarRD].ptr_vPrevVal = malloc(usNbElem*usTailleEnByte);
    	}
    	memmove(strS7KRecordXML->strTabRDXML[iVarRD].ptr_vPrevVal, &RD_7027Record->Flags, usNbElem*usTailleEnByte);


    	iVarRD++;
    	usTailleEnByte 		= strS7KRecordXML->strTabRDXML[iVarRD].usSize;
    	usNbElem 			= strS7KRecordXML->strTabRDXML[iVarRD].usNbElem;
    	iFlag 				= fread(&RD_7027Record->Quality, usTailleEnByte, usNbElem, G_fpData);
    	fwrite(&RD_7027Record->Quality, usTailleEnByte, usNbElem, strS7KRecordXML->strTabRDXML[iVarRD].ptr_fpBin);
    	if (iNbRecord > 0 && strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant == TRUE)
    	{
    		bFlagCompare = util_bComparePrevAndCurrentVal(strS7KRecordXML->strTabRDXML[iVarRD].cType,
    											strS7KRecordXML->strTabRDXML[iVarRD].ptr_vPrevVal,
    											(void *)&RD_7027Record->Quality,
    											usNbElem);
    		strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant = bFlagCompare && strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant;
    	}
    	if (iNbRecord == 0)
    	{
    		strS7KRecordXML->strTabRDXML[iVarRD].ptr_vPrevVal = malloc(usNbElem*usTailleEnByte);
    	}
    	memmove(strS7KRecordXML->strTabRDXML[iVarRD].ptr_vPrevVal, &RD_7027Record->Quality, usNbElem*usTailleEnByte);


    	iVarRD++;
    	usTailleEnByte 		= strS7KRecordXML->strTabRDXML[iVarRD].usSize;
    	usNbElem 			= strS7KRecordXML->strTabRDXML[iVarRD].usNbElem;
    	iFlag 				= fread(&RD_7027Record->Uncertainty, usTailleEnByte, usNbElem, G_fpData);
    	fwrite(&RD_7027Record->Uncertainty, usTailleEnByte, usNbElem, strS7KRecordXML->strTabRDXML[iVarRD].ptr_fpBin);
    	if (iNbRecord > 0 && strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant == TRUE)
    	{
    		bFlagCompare = util_bComparePrevAndCurrentVal(strS7KRecordXML->strTabRDXML[iVarRD].cType,
    											strS7KRecordXML->strTabRDXML[iVarRD].ptr_vPrevVal,
    											(void *)&RD_7027Record->Uncertainty,
    											usNbElem);
    		strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant = bFlagCompare && strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant;
    	}
    	if (iNbRecord == 0)
    	{
    		strS7KRecordXML->strTabRDXML[iVarRD].ptr_vPrevVal = malloc(usNbElem*usTailleEnByte);
    	}
    	memmove(strS7KRecordXML->strTabRDXML[iVarRD].ptr_vPrevVal, &RD_7027Record->Uncertainty, usNbElem*usTailleEnByte);


    } // Fin de la boucle de lecture des donn�es RD


 	iNbRecord++;

 	free(RTH_7027Record);

	return iRetour;

} //iProcess7027Record


//----------------------------------------------------------------
// Fonction :	iInitXMLBin7027Record
// Objet 	:	Initialisation de la structure �quivalent au paquet
//				7027 pour la manipulation des fichiers bin et XML.
// Modification : 15/10/08
// Auteur 	: GLU
//----------------------------------------------------------------
int iInitXMLBin7027Record(	char *cTitleDatagram,
							char *cNomDatagram,
							char *cComments,
							T_S7KRecordXML *strS7KRecordXML)

{
	int iRet = 0;

	T_DESC_SIGNAL_OR_IMAGE strDescriptionSignal[NB_SIGNAL] = {
			{	"SonarId","u64",				1.0, 0.0},
			{	"PingNumber","u32", 			1.0, 0.0},
			{	"MultiPingSequence","u16", 		1.0, 0.0},
			{	"N","u32",						1.0, 0.0},
			{	"DataFieldSize","u32",			1.0, 0.0},
			{	"DetectionAlgorithm","u8", 		1.0, 0.0},
			{	"Flags","u32", 					1.0, 0.0},
			{	"SamplingRate","f32", 			1.0, 0.0},
			{	"TxAngle","f32", 				1.0, 0.0},
			{	"Reserved","u32", 				1.0, 0.0}};

	T_DESC_SIGNAL_OR_IMAGE strDescriptionImage[NB_IMAGE] = {
			{	"BeamDescriptor","u16",			1.0, 0.0},
			{	"DetectionPoint","u32",			1.0, 0.0},
			{	"RxAngle","u32",				1.0, 0.0},
			{	"Flags","u32",					1.0, 0.0},
			{	"Quality","u32",				1.0, 0.0},
			{	"Uncertainty","u16",			1.0, 0.0}};


	T_DESC_SIGNAL_OR_IMAGE strDescriptionImage2[NB_IMAGE2] = {};

	T_DESC_TAB_SIGANDIMG strDescriptionTab = {	NB_SIGNAL,
												NB_IMAGE,
												NB_IMAGE2,
												NUM_INDEX_TABLE,
												strDescriptionSignal,
												strDescriptionImage,
												strDescriptionImage2};

	// Lancement de l'initialisation des structures de signaux : ouvertures des
	// fichiers, copie des structures, ...
	iRet = utilS7K_initXMLRecord(	strDescriptionTab,
									cTitleDatagram,
									cNomDatagram,
									cComments,
									strS7KRecordXML);


	return EXIT_SUCCESS;

} // iInitXMLBin7027Record

#pragma pack()
