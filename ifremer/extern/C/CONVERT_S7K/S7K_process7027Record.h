/*
 * S7K_process7027Record.h
 *
 *  Created on: 19 09 2011
 *      Author: rgallou
 */

#ifndef S7K_PROCESS7027RECORD_H_
#define S7K_PROCESS7027RECORD_H_
#pragma pack(1)

#include "S7K_DATAGRAMS.H"

//-Paquet 7027 : partie RTH------------------------
typedef struct {
	u64		SonarId;
	u32		PingNumber;
	u16		MultiPingSequence;
	u32		N;
	u32		DataFieldSize;
	u8		DetectionAlgorithm;
	u32		Flags;
	f32		SamplingRate;
	f32		TxAngle;
	u32		Reserved[16];
} T_7027_RECORD_RTH;

// Paquet 7027 : partie RD
typedef struct {
	u16		BeamDescriptor;
	f32		DetectionPoint;
	f32		RxAngle;
	u32		Flags;
	u32		Quality;
	f32		Uncertainty;
	// u16		Reserved;
} T_7027_RECORD_RD;


int iProcess7027Record(FILE *G_fpData,
						int iNbRecord,
						u32 uiODOffset,
						T_S7KRecordXML *strS7KRecordXML,
						T_7027_RECORD_RTH *RTH_prev7027Record);

int iInitXMLBin7027Record(	char *cTitleDatagram,
							char *cNomDatagram,
							char *cComments,
							T_S7KRecordXML *strS7KRecordXML);

#pragma pack()

#endif /* S7K_PROCESS7027RECORD_H_ */
