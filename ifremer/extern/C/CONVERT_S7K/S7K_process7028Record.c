/*
 * S7K_process7028Record.c
 *
 *  Created on: 13 oct. 2008
 *      Author: rgallou
 */
#pragma pack(1)

#include <sys/types.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#define NB_SIGNAL 			7
#define NB_IMAGE 			5
#define NB_IMAGE2 			0
#define NUM_INDEX_TABLE		3	// Identification de l'index Signal des Images (� partir de 0).
#define NUM_INDEX_TABLE2	0	// Identification de l'index Signal des Images (� partir de 0).

#include "S7K_Datagrams.h"
#include "S7K_Utilities.h"
#include "S7K_process7028Record.h"
#include "convertFiles_S7K.h"


//----------------------------------------------------------------
// Fonction :	iProcess7028Record
// Objet 	:	Traitement du paquet 7028
// Modif.	: 	03/11/08
// Auteur 	:	GLU
//----------------------------------------------------------------
int iProcess7028Record(FILE *G_fpData,
						int iNbRecord,
						u32 uiODOffset,
						T_S7KRecordXML *strS7KRecordXML,
						T_7028_RECORD_RTH *RTH_prev7028Record)
{
    T_7028_RECORD_RTH 	*RTH_7028Record;
    T_7028_RECORD_RD 	*RD_7028Record;


	// Pointeur pour avancer sur les variables automatiquement.
	unsigned char		*ptr_cRTHRecord;
	static void 		*ptr_vVarPrevRecord;
	void				*ptr_vVarRecord,
						*ptr_vBidon;

	BOOL		bFlagVarConstant = FALSE,
				bFlagCompare = FALSE;

    int 	iRet,
			iFlag,
			iVarRD,
			iVarRTH,
			iLoop,
			iRetour = 0;

    char *cValeur;

	

    unsigned short	usTailleEnByte,
					usNbElem;


    // Initialisation de la structure d'identification du paquet
 	if (iNbRecord == 0)
	{
 		iInitXMLBin7028Record("Snippet Data", "7028", "Snippet Imagery Data", strS7KRecordXML);
	}
    // Lecture du RTH du paquet
 	RTH_7028Record = (T_7028_RECORD_RTH *)malloc(sizeof(T_7028_RECORD_RTH));
 	iFlag = fread(RTH_7028Record, sizeof(T_7028_RECORD_RTH), 1, G_fpData);
 	if (iFlag == 0)
 	{
 		printf("Error in reading : %s\n", G_cFileData);
 		return FALSE;
 	}
 	ptr_cRTHRecord = (unsigned char*)RTH_7028Record;
 	// Sauvegarde des donn�es des attributs.
 	if (iNbRecord > 0)
 	{
 		ptr_vVarPrevRecord = (void *)RTH_prev7028Record;
 	}
 	for (iVarRTH=0; iVarRTH<(int)strS7KRecordXML->sNbVarRTH; iVarRTH++)
 	{
 		ptr_vVarRecord = (void *)ptr_cRTHRecord;
 		if (iVarRTH==0)
 		{
 			usTailleEnByte = 0;
 			usNbElem = 0;
 		}
 		else
 		{
 			usTailleEnByte = util_usNbOctetTypeVar(strS7KRecordXML->strTabRTHXML[iVarRTH-1].cType);
 			usNbElem = strS7KRecordXML->strTabRTHXML[iVarRTH-1].usNbElem;
 		}
 		if (iNbRecord > 0)
 		{
 			bFlagVarConstant = strS7KRecordXML->strTabRTHXML[iVarRTH].bFlagConstant;
 			ptr_vVarPrevRecord = ptr_vVarPrevRecord + usTailleEnByte*usNbElem;
 			bFlagCompare = util_bComparePrevAndCurrentVal(strS7KRecordXML->strTabRTHXML[iVarRTH].cType,
 												ptr_vVarPrevRecord,
 												ptr_vVarRecord,
 											   strS7KRecordXML->strTabRTHXML[iVarRTH].usNbElem);
 			bFlagVarConstant = bFlagVarConstant && bFlagCompare;
 			strS7KRecordXML->strTabRTHXML[iVarRTH].bFlagConstant = bFlagVarConstant;
 		}
 		// Ecriture de la variable.
 		usTailleEnByte = strS7KRecordXML->strTabRTHXML[iVarRTH].usSize;
 		usNbElem =  strS7KRecordXML->strTabRTHXML[iVarRTH].usNbElem;
 		fwrite(ptr_cRTHRecord, usTailleEnByte, usNbElem, strS7KRecordXML->strTabRTHXML[iVarRTH].ptr_fpBin);
 	    strS7KRecordXML->strTabRTHXML[iVarRTH].ptr_vPrevVal = ptr_cRTHRecord;
 		ptr_cRTHRecord += usTailleEnByte*usNbElem;
 	}

 	// Recopie en zone tampon des valeurs courantes.
 	memmove(RTH_prev7028Record, RTH_7028Record, sizeof(T_7028_RECORD_RTH));

	// --------------------------------------------
 	// Traitement des paquets RD
 	RD_7028Record = (T_7028_RECORD_RD *)malloc(sizeof(T_7028_RECORD_RD));
 	// Lecture des datas de fa�on adapt�e � lecture des Samples qui
 	// d�pend des valeurs de Begin et End Sample Descriptor.
 	for (iLoop = 0; iLoop < RTH_7028Record->N; iLoop++)
    {
    	// Ecriture des donn�es de la partie RD dans les binaires de stockage.
    	iVarRD = 0;
    	usTailleEnByte = strS7KRecordXML->strTabRDXML[iVarRD].usSize;
    	usNbElem = strS7KRecordXML->strTabRDXML[iVarRD].usNbElem;
    	iFlag = fread(&RD_7028Record->BeamDescriptor, usTailleEnByte, usNbElem, G_fpData);
    	fwrite(&RD_7028Record->BeamDescriptor, usTailleEnByte, usNbElem, strS7KRecordXML->strTabRDXML[iVarRD].ptr_fpBin);
    	if (iNbRecord > 0 && strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant == TRUE)
    	{
    		bFlagCompare = util_bComparePrevAndCurrentVal(strS7KRecordXML->strTabRDXML[iVarRD].cType,
    											strS7KRecordXML->strTabRDXML[iVarRD].ptr_vPrevVal,
    											(void *)&RD_7028Record->BeamDescriptor,
    											usNbElem);
    		strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant = bFlagCompare && strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant;
    	}
    	if (iNbRecord == 0)
    	{
    		strS7KRecordXML->strTabRDXML[iVarRD].ptr_vPrevVal = malloc(usNbElem*usTailleEnByte);
    	}
    	memmove(strS7KRecordXML->strTabRDXML[iVarRD].ptr_vPrevVal, &RD_7028Record->BeamDescriptor, usNbElem*usTailleEnByte);

    	iVarRD++;
    	usTailleEnByte = strS7KRecordXML->strTabRDXML[iVarRD].usSize;
    	usNbElem = strS7KRecordXML->strTabRDXML[iVarRD].usNbElem;
    	iFlag = fread(&RD_7028Record->SnippetStart, usTailleEnByte, usNbElem, G_fpData);
    	fwrite(&RD_7028Record->SnippetStart, usTailleEnByte, usNbElem, strS7KRecordXML->strTabRDXML[iVarRD].ptr_fpBin);
    	if (iNbRecord > 0 && strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant == TRUE)
    	{
    		bFlagCompare = util_bComparePrevAndCurrentVal(strS7KRecordXML->strTabRDXML[iVarRD].cType,
    											strS7KRecordXML->strTabRDXML[iVarRD].ptr_vPrevVal,
    											(void *)&RD_7028Record->SnippetStart,
    											usNbElem);
    		strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant = bFlagCompare && strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant;
    	}
    	if (iNbRecord == 0)
    	{
    		strS7KRecordXML->strTabRDXML[iVarRD].ptr_vPrevVal = malloc(usNbElem*usTailleEnByte);
    	}
    	memmove(strS7KRecordXML->strTabRDXML[iVarRD].ptr_vPrevVal, &RD_7028Record->SnippetStart, usNbElem*usTailleEnByte);


    	iVarRD++;
    	usTailleEnByte = strS7KRecordXML->strTabRDXML[iVarRD].usSize;
    	usNbElem = strS7KRecordXML->strTabRDXML[iVarRD].usNbElem;
    	iFlag = fread(&RD_7028Record->DetectionSample, usTailleEnByte, usNbElem, G_fpData);
    	fwrite(&RD_7028Record->DetectionSample, usTailleEnByte, usNbElem, strS7KRecordXML->strTabRDXML[iVarRD].ptr_fpBin);
    	if (iNbRecord > 0 && strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant == TRUE)
    	{
    		bFlagCompare = util_bComparePrevAndCurrentVal(strS7KRecordXML->strTabRDXML[iVarRD].cType,
    											strS7KRecordXML->strTabRDXML[iVarRD].ptr_vPrevVal,
    											(void *)&RD_7028Record->DetectionSample,
    											usNbElem);
    		strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant = bFlagCompare && strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant;
    	}
    	strS7KRecordXML->strTabRDXML[iVarRD].ptr_vPrevVal = malloc(usNbElem*usTailleEnByte);
    	memmove(strS7KRecordXML->strTabRDXML[iVarRD].ptr_vPrevVal, &RD_7028Record->DetectionSample, usNbElem*usTailleEnByte);


    	iVarRD++;
    	usTailleEnByte = strS7KRecordXML->strTabRDXML[iVarRD].usSize;
    	usNbElem = strS7KRecordXML->strTabRDXML[iVarRD].usNbElem;
    	iFlag = fread(&RD_7028Record->SnippetEnd, usTailleEnByte, usNbElem, G_fpData);
    	fwrite(&RD_7028Record->SnippetEnd, usTailleEnByte, usNbElem, strS7KRecordXML->strTabRDXML[iVarRD].ptr_fpBin);
    	if (iNbRecord > 0 && strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant == TRUE)
    	{
    		bFlagCompare = util_bComparePrevAndCurrentVal(strS7KRecordXML->strTabRDXML[iVarRD].cType,
    											strS7KRecordXML->strTabRDXML[iVarRD].ptr_vPrevVal,
    											(void *)&RD_7028Record->SnippetEnd,
    											usNbElem);
    		strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant = bFlagCompare && strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant;
    	}
    	if (iNbRecord == 0)
    	{
    		strS7KRecordXML->strTabRDXML[iVarRD].ptr_vPrevVal = malloc(usNbElem*usTailleEnByte);
    	}
    	memmove(strS7KRecordXML->strTabRDXML[iVarRD].ptr_vPrevVal, &RD_7028Record->SnippetEnd, usNbElem*usTailleEnByte);

    	// Lecture des �chantillons selon les indices de D�but et de Fin.
    	iVarRD++;
    	usTailleEnByte = strS7KRecordXML->strTabRDXML[iVarRD].usSize;
    	// Le nb d'�chantillon est calcul� selon les indices lus pr�c�demment.
        usNbElem = strS7KRecordXML->strTabRDXML[iVarRD].usNbElem*(RD_7028Record->SnippetEnd-RD_7028Record->SnippetStart+1);
    	RD_7028Record->Samples = malloc(usNbElem*usTailleEnByte);
    	fwrite(RD_7028Record->Samples, usTailleEnByte, usNbElem, strS7KRecordXML->strTabRDXML[iVarRD].ptr_fpBin);
    	if (iNbRecord > 0 && strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant == TRUE)
    	{
    		bFlagCompare = util_bComparePrevAndCurrentVal(strS7KRecordXML->strTabRDXML[iVarRD].cType,
    											strS7KRecordXML->strTabRDXML[iVarRD].ptr_vPrevVal,
    											RD_7028Record->Samples,
    											usNbElem);
    		strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant = bFlagCompare && strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant;
    	}
    	strS7KRecordXML->strTabRDXML[iVarRD].ptr_vPrevVal = malloc(usNbElem*usTailleEnByte);
    	memmove(strS7KRecordXML->strTabRDXML[iVarRD].ptr_vPrevVal, RD_7028Record->Samples, usNbElem*usTailleEnByte);
    	free(RD_7028Record->Samples);
    } // Fin de la boucle de lecture des donn�es RD


 	iNbRecord++;

 	free(RTH_7028Record);

	return iRetour;

} //iProcess7028Record


//----------------------------------------------------------------
// Fonction :	iInitXMLBin7028Record
// Objet 	:	Initialisation de la structure �quivalent au paquet
//				7028 pour la manipulation des fichiers bin et XML.
// Modification : 15/10/08
// Auteur 	: GLU
//----------------------------------------------------------------
int iInitXMLBin7028Record(	char *cTitleDatagram,
							char *cNomDatagram,
							char *cComments,
							T_S7KRecordXML *strS7KRecordXML)

{
	int iRet = 0;

	T_DESC_SIGNAL_OR_IMAGE strDescriptionSignal[NB_SIGNAL] = {
			{	"SonarId","u64",				1.0, 0.0},
			{	"PingNumber","u32", 			1.0, 0.0},
			{	"MultiPingSequence","u16", 		1.0, 0.0},
			{	"NBeams","u16",					1.0, 0.0},
			{	"ErrorFlags","u8", 				1.0, 0.0},
			{	"ControlFlags","u8", 			1.0, 0.0},
			{	"Reserved1","u32", 				1.0, 0.0}};

	T_DESC_SIGNAL_OR_IMAGE strDescriptionImage[NB_IMAGE] = {
			{	"BeamDescriptor","u16",			1.0, 0.0},
			{	"SnippetStart","u32",			1.0, 0.0},
			{	"DetectionSample","u32",		1.0, 0.0},
			{	"SnippetEnd","u32",				1.0, 0.0},
			{	"Samples","u16",				1.0, 0.0}};


	T_DESC_SIGNAL_OR_IMAGE strDescriptionImage2[NB_IMAGE2] = {};

	T_DESC_TAB_SIGANDIMG strDescriptionTab = {	NB_SIGNAL,
												NB_IMAGE,
												NB_IMAGE2,
												NUM_INDEX_TABLE,
												strDescriptionSignal,
												strDescriptionImage,
												strDescriptionImage2};

	// Lancement de l'initialisation des structures de signaux : ouvertures des
	// fichiers, copie des structures, ...
	iRet = utilS7K_initXMLRecord(	strDescriptionTab,
									cTitleDatagram,
									cNomDatagram,
									cComments,
									strS7KRecordXML);


	return EXIT_SUCCESS;

} // iInitXMLBin7028Record

#pragma pack()
