/*
 * S7K_process7028Record.h
 *
 *  Created on: 13 oct. 2008
 *      Author: rgallou
 */

#ifndef S7K_PROCESS7028RECORD_H_
#define S7K_PROCESS7028RECORD_H_
#pragma pack(1)

#include "S7K_DATAGRAMS.H"

//-Paquet 7028 : partie RTH------------------------
typedef struct {
	u64		SonarId;
	u32		PingNumber;
	u16		MultiPingSequence;
	u16		N;
	u8		ErrorFlags;
	u8		ControlFlags;
	u32		Reserved[7];
} T_7028_RECORD_RTH;

// Paquet 7028 : partie RD
typedef struct {
	u16		BeamDescriptor;
	u32		SnippetStart;
	u32		DetectionSample;
	u32		SnippetEnd;
	u16		*Samples;
} T_7028_RECORD_RD;


int iProcess7028Record(FILE *G_fpData, int iNbRecord,
						u32 uiODOffset,
						T_S7KRecordXML *strS7KRecordXML,
						T_7028_RECORD_RTH *RTH_prev7028Record);

int iInitXMLBin7028Record(	char *cTitleDatagram,
							char *cNomDatagram, char *cComments,
							T_S7KRecordXML *strS7KRecordXML);
#pragma pack()
#endif /* S7K_PROCESS7028RECORD_H_ */
