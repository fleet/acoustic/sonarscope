/*
 * S7K_process7030Record.c
 *
 *  Created on: 13 oct. 2008
 *      Author: rgallou
 */
#pragma pack(1)

#include <sys/types.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "S7K_Datagrams.h"
#include "S7K_Utilities.h"
#include "S7K_process7030Record.h"
#include "convertFiles_S7K.h"

#define NB_SIGNAL 			33
#define NB_IMAGE 			0
#define NB_IMAGE2 			0
#define NUM_INDEX_TABLE		0	// Identification de l'index Signal des Images (� partir de 0).
#define NUM_INDEX_TABLE2	0	// Identification de l'index Signal des Images (� partir de 0).

//----------------------------------------------------------------
// Fonction :	iProcess7030Record
// Objet 	:	Traitement du paquet 7030
// Modif.	: 	03/11/08
// Auteur 	:	GLU
//----------------------------------------------------------------
int iProcess7030Record(FILE *G_fpData, int iNbRecord,
						u32 uiODOffset,
						T_S7KRecordXML *strS7KRecordXML,
						T_7030_RECORD_RTH *RTH_prev7030Record)
{
    T_7030_RECORD_RTH 	*RTH_7030Record;

	// Pointeur pour avancer sur les variables automatiquement.
	unsigned char		*ptr_cRTHRecord;
	static void 		*ptr_vVarPrevRecord;
	void				*ptr_vVarRecord;

	BOOL		bFlagVarConstant = TRUE,
				bFlagCompare = TRUE;

    int 	iFlag,
			iVarRTH,
			iRetour = 0;

	

    unsigned short	usTailleEnByte,
					usNbElem;

	// Conservation de l'information de pr�sence de donn�es Optionnal Data.
    strS7KRecordXML->uiODOffset = 0;

    // Initialisation de la structure d'identification du paquet
 	if (iNbRecord == 0)
	{
 		iInitXMLBin7030Record("Sonar Installation Parameters", "7030", "Sonar Installation Parameters", uiODOffset, strS7KRecordXML);
	}

    // Lecture du RTH du paquet
	RTH_7030Record = (T_7030_RECORD_RTH *)malloc(sizeof(T_7030_RECORD_RTH));
	iFlag = fread(RTH_7030Record, sizeof(T_7030_RECORD_RTH), 1, G_fpData);
	if (iFlag == 0)
	{
		printf("Error in reading : %s\n", G_cFileData);
		return FALSE;
	}
	ptr_cRTHRecord = (unsigned char*)RTH_7030Record;
	// Sauvegarde des donn�es des attributs.
	if (iNbRecord > 0)
	{
		ptr_vVarPrevRecord = (void *)RTH_prev7030Record;
	}
	for (iVarRTH=0; iVarRTH<(int)strS7KRecordXML->sNbVarRTH; iVarRTH++)
	{
		ptr_vVarRecord = (void *)ptr_cRTHRecord;
		if (iVarRTH==0)
		{
			usTailleEnByte = 0;
			usNbElem = 0;
		}
		else
		{
			usTailleEnByte = util_usNbOctetTypeVar(strS7KRecordXML->strTabRTHXML[iVarRTH-1].cType);
			usNbElem = strS7KRecordXML->strTabRTHXML[iVarRTH-1].usNbElem;
		}

		if (iNbRecord > 0)
		{
			bFlagVarConstant = strS7KRecordXML->strTabRTHXML[iVarRTH].bFlagConstant;
			ptr_vVarPrevRecord = ptr_vVarPrevRecord + usTailleEnByte*usNbElem;
			bFlagCompare = util_bComparePrevAndCurrentVal(strS7KRecordXML->strTabRTHXML[iVarRTH].cType,
												ptr_vVarPrevRecord,
												ptr_vVarRecord,
											   strS7KRecordXML->strTabRTHXML[iVarRTH].usNbElem);
			bFlagVarConstant = bFlagVarConstant && bFlagCompare;
			strS7KRecordXML->strTabRTHXML[iVarRTH].bFlagConstant = bFlagVarConstant;
		}
		// Ecriture sp�ciale car contenant du XML pour la variable Device Info
		// Pr�paration de l'�criture de la variable.
		usTailleEnByte = strS7KRecordXML->strTabRTHXML[iVarRTH].usSize;
		usNbElem =  strS7KRecordXML->strTabRTHXML[iVarRTH].usNbElem;
		fwrite(ptr_cRTHRecord, usTailleEnByte, usNbElem, strS7KRecordXML->strTabRTHXML[iVarRTH].ptr_fpBin);
	    strS7KRecordXML->strTabRTHXML[iVarRTH].ptr_vPrevVal = ptr_cRTHRecord;
		ptr_cRTHRecord += usTailleEnByte*usNbElem;
	}

	// Recopie en zone tampon des valeurs courantes.
	memmove(RTH_prev7030Record, RTH_7030Record, sizeof(T_7030_RECORD_RTH));

	iNbRecord++;

	return iRetour;

} //iProcess7030Record


//----------------------------------------------------------------
// Fonction :	iInitXMLBin7030Record
// Objet 	:	Initialisation de la structure �quivalent au paquet
//				7030 pour la manipulation des fichiers bin et XML.
// Modification : 15/10/08
// Auteur 	: GLU
//----------------------------------------------------------------
int iInitXMLBin7030Record(	char *cTitleDatagram,
							char *cNomDatagram, char *cComments,
							u32 uiODOffset,
							T_S7KRecordXML *strS7KRecordXML)
{
	int iRet = 0;

	T_DESC_SIGNAL_OR_IMAGE strDescriptionSignal[NB_SIGNAL] = {
			{"Frequency","f32",						1.0,	0.0},
			{"LengthOfFirmwareVersionInfo","u16",	1.0,	0.0},
			{"FirmwareVersionInfo","char",			1.0,	0.0},
			{"LengthOfSoftwareVersionInfo","u16",	1.0,	0.0},
			{"SoftwareVersionInfo","char",			1.0,	0.0},
			{"LengthOf7KSoftwareVersionInfo","u16",	1.0,	0.0},
			{"SevenKSoftwareVersionInfo","char",	1.0,	0.0},
			{"LengthOfRecordProtocolInfo","u16",	1.0,	0.0},
			{"RecordProtocolVersionInfo","char",	1.0,	0.0},
			{"TransmitArrayX","f32",				1.0,	0.0},
			{"TransmitArrayY","f32",				1.0,	0.0},
			{"TransmitArrayZ","f32",				1.0,	0.0},
			{"TransmitArrayRoll","f32",				1.0,	0.0},
			{"TransmitArrayPitch","f32",			1.0,	0.0},
			{"TransmitArrayHeading","f32",			1.0,	0.0},
			{"ReceiveArrayX","f32",					1.0,	0.0},
			{"ReceiveArrayY","f32",					1.0,	0.0},
			{"ReceiveArrayZ","f32",					1.0,	0.0},
			{"ReceiveArrayRoll","f32",				1.0,	0.0},
			{"ReceiveArrayPitch","f32",				1.0,	0.0},
			{"ReceiveArrayHeading","f32",			1.0,	0.0},
			{"MotionSensorX","f32",					1.0,	0.0},
			{"MotionSensorY","f32",					1.0,	0.0},
			{"MotionSensorZ","f32",					1.0,	0.0},
			{"MotionSensorRollCalibration","f32",	1.0,	0.0},
			{"MotionSensorPitchCalibration","f32",	1.0,	0.0},
			{"MotionSensorHeadingCalibration","f32",1.0,	0.0},
			{"MotionSensorTimeDelay","u16",			1.0,	0.0},
			{"PositionSensorX","f32",				1.0,	0.0},
			{"PositionSensorY","f32",				1.0,	0.0},
			{"PositionSensorZ","f32",				1.0,	0.0},
			{"PositionSensorTimeDelay","u16",		1.0,	0.0},
			{"WaterLineVerticalOffset","f32",		1.0,	0.0}};

	T_DESC_SIGNAL_OR_IMAGE strDescriptionImage[NB_IMAGE] = {};


	T_DESC_SIGNAL_OR_IMAGE strDescriptionImage2[NB_IMAGE2] = {};

	T_DESC_TAB_SIGANDIMG strDescriptionTab = {	NB_SIGNAL,
												NB_IMAGE,
												NB_IMAGE2,
												NUM_INDEX_TABLE,
												strDescriptionSignal,
												strDescriptionImage,
												strDescriptionImage2};


	// Lancement de l'initialisation des structures de signaux : ouvertures des
	// fichiers, copie des structures, ...
	iRet = utilS7K_initXMLRecord(	strDescriptionTab,
									cTitleDatagram,
									cNomDatagram,
									cComments,
									strS7KRecordXML);


	// Cas particuliers de quelques champs pour leur taille.
	strS7KRecordXML->strTabRTHXML[2].usNbElem = 128; // FirmwareVersionInfo
	strS7KRecordXML->strTabRTHXML[4].usNbElem = 128; // SofwareVersionInfo
	strS7KRecordXML->strTabRTHXML[6].usNbElem = 128; // 7kSoftwareVersionInfo
	strS7KRecordXML->strTabRTHXML[8].usNbElem = 128; // RecordProtocolVersionInfo


	return EXIT_SUCCESS;

} // iInitXMLBin7030Record

#pragma pack()
