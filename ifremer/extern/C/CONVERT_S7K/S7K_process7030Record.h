/*
 * S7K_process7030Record.h
 *
 *  Created on: 13 oct. 2008
 *      Author: rgallou
 */

#ifndef S7K_PROCESS7030RECORD_H_
#define S7K_PROCESS7030RECORD_H_
#pragma pack(1)

#include "S7K_DATAGRAMS.H"

//-Paquet 7030 : partie RTH------------------------
typedef struct {
	f32		Frequency;
	u16		LengthOfFirmwareVersionInfo;
	char	FirmwareVersionInfo[128];
	u16		LengthOfSoftwareVersionInfo;
	char	SoftwareVersionInfo[128];
	u16		LengthOf7KSoftwareVersionInfo;
	char	SevenKSoftwareVersionInfo[128];
	u16		LengthOfRecordProtocolInfo;
	char	RecordProtocolVersionInfo[128];
	f32		TransmitArrayX;
	f32		TransmitArrayY;
	f32		TransmitArrayZ;
	f32		TransmitArrayRoll;
	f32		TransmitArrayPitch;
	f32		TransmitArrayHeading;
	f32		ReceiveArrayX;
	f32		ReceiveArrayY;
	f32		ReceiveArrayZ;
	f32		ReceiveArrayRoll;
	f32		ReceiveArrayPitch;
	f32		ReceiveArrayHeading;
	f32		MotionSensorX;
	f32		MotionSensorY;
	f32		MotionSensorZ;
	f32		MotionSensorRollCalibration;
	f32		MotionSensorPitchCalibration;
	f32		MotionSensorHeadingCalibration;
	u16		MotionSensorTimeDelay;
	f32		PositionSensorX;
	f32		PositionSensorY;
	f32		PositionSensorZ;
	u16		PositionSensorTimeDelay;
	f32		WaterLineVerticalOffset;
} T_7030_RECORD_RTH;


int iProcess7030Record(FILE *G_fpData, int iNbRecord,
						u32 uiODOffset,
						T_S7KRecordXML *strS7KRecordXML,
						T_7030_RECORD_RTH *RTH_prev7030Record);

int iInitXMLBin7030Record(	char *cTitleDatagram,
							char *cNomDatagram, char *cComments,
							u32 uiODOffset,
							T_S7KRecordXML *strS7KRecordXML);
#pragma pack()
#endif /* S7K_PROCESS7030RECORD_H_ */
