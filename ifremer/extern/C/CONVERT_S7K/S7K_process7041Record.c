/*
 * S7K_process7041Record.c
 *
 *  Created on: 29 Sept. 2011
 *      Author: rgallou
 */
#pragma pack(1)

#include <sys/types.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "S7K_Datagrams.h"
#include "S7K_Utilities.h"
#include "S7K_process7041Record.h"
#include "convertFiles_S7K.h"

#define NB_SIGNAL 			7
#define NB_IMAGE 			5
#define NB_IMAGE2 			0
#define NUM_INDEX_TABLE		3	// Identification de l'index Signal des Images (� partir de 0).
#define NUM_INDEX_TABLE2	0	// Identification de l'index Signal des Images (� partir de 0).

//----------------------------------------------------------------
// Fonction :	iProcess7041Record
// Objet 	:	Traitement du paquet 7041
// Modif.	: 	03/11/08
// Auteur 	:	GLU
//----------------------------------------------------------------
int iProcess7041Record(FILE *G_fpData, int iNbRecord,
						u32 uiODOffset,
						T_S7KRecordXML *strS7KRecordXML,
						T_7041_RECORD_RTH *RTH_prev7041Record)
{
    T_7041_RECORD_RTH 	*RTH_7041Record;
    T_7041_RECORD_RD 	*RD_7041Record;

	// Pointeur pour avancer sur les variables automatiquement.
	unsigned char		*ptr_cRTHRecord;
	static void 		*ptr_vVarPrevRecord;
	void				*ptr_vVarRecord;

	BOOL		bFlagVarConstant 	= TRUE,
				bFlagCompare 		= TRUE;

    int 	iFlag,
			iVarRTH,
			iVarRD = 0,
			iVarOD = 0,
			iRetour = 0,
			iLoop;

	

    unsigned short	usTailleEnByte,
					usNbElem,
					usDataSize,
					usBeamIdMethod;

	// Conservation de l'information de pr�sence de donn�es Optionnal Data.
    strS7KRecordXML->uiODOffset = uiODOffset;

    // Initialisation de la structure d'identification du paquet
 	if (iNbRecord == 0)
	{
 		iInitXMLBin7041Record("Compressed Beamformed Magnitude Data", "7041", "7k Compressed Beamformed Magnitude Data", uiODOffset, strS7KRecordXML);
	}

    // Lecture du RTH du paquet
	RTH_7041Record = (T_7041_RECORD_RTH *)malloc(sizeof(T_7041_RECORD_RTH));
	iFlag = fread(RTH_7041Record, sizeof(T_7041_RECORD_RTH), 1, G_fpData);
	if (iFlag == 0)
	{
		printf("Error in reading : %s\n", G_cFileData);
		return FALSE;
	}
	ptr_cRTHRecord = (unsigned char*)RTH_7041Record;
	// Sauvegarde des donn�es des attributs.
	if (iNbRecord > 0)
	{
		ptr_vVarPrevRecord = (void *)RTH_prev7041Record;
	}
	for (iVarRTH=0; iVarRTH<(int)strS7KRecordXML->sNbVarRTH; iVarRTH++)
	{
		ptr_vVarRecord = (void *)ptr_cRTHRecord;
		if (iVarRTH==0)
		{
			usTailleEnByte = 0;
			usNbElem = 0;
		}
		else
		{
			usTailleEnByte = util_usNbOctetTypeVar(strS7KRecordXML->strTabRTHXML[iVarRTH-1].cType);
			usNbElem = strS7KRecordXML->strTabRTHXML[iVarRTH-1].usNbElem;
		}

		if (iNbRecord > 0)
		{
			bFlagVarConstant = strS7KRecordXML->strTabRTHXML[iVarRTH].bFlagConstant;
			ptr_vVarPrevRecord = ptr_vVarPrevRecord + usTailleEnByte*usNbElem;
			bFlagCompare = util_bComparePrevAndCurrentVal(strS7KRecordXML->strTabRTHXML[iVarRTH].cType,
												ptr_vVarPrevRecord,
												ptr_vVarRecord,
											   strS7KRecordXML->strTabRTHXML[iVarRTH].usNbElem);
			bFlagVarConstant = bFlagVarConstant && bFlagCompare;
			strS7KRecordXML->strTabRTHXML[iVarRTH].bFlagConstant = bFlagVarConstant;
		}
		// Assignation du type de donn�es et de la m�thode d'identification des faisceaux.
		if (iVarRTH == 4)
		{
			usBeamIdMethod = (unsigned short)util_GetBits(ptr_cRTHRecord, 8, 1);
			usDataSize = (unsigned short)util_GetBits(ptr_cRTHRecord, 0, 2);
		}
		// Ecriture sp�ciale car contenant du XML pour la variable Device Info
		// Pr�paration de l'�criture de la variable.
		usTailleEnByte = strS7KRecordXML->strTabRTHXML[iVarRTH].usSize;
		usNbElem =  strS7KRecordXML->strTabRTHXML[iVarRTH].usNbElem;
		fwrite(ptr_cRTHRecord, usTailleEnByte, usNbElem, strS7KRecordXML->strTabRTHXML[iVarRTH].ptr_fpBin);
	    strS7KRecordXML->strTabRTHXML[iVarRTH].ptr_vPrevVal = ptr_cRTHRecord;
		ptr_cRTHRecord += usTailleEnByte*usNbElem;
	}

	// Recopie en zone tampon des valeurs courantes.
	memmove(RTH_prev7041Record, RTH_7041Record, sizeof(T_7041_RECORD_RTH));

	// --------------------------------------------
 	// Traitement des paquets RD
 	// Lecture des datas les uns apr�s les autres
	RD_7041Record = (T_7041_RECORD_RD *)malloc(sizeof(T_7041_RECORD_RD));
	for (iLoop=0; iLoop< RTH_7041Record->NBeams; iLoop++)
	{
		// Lecture de la donn�e Beam (Beam Number ou Beam Angle)
		if (usBeamIdMethod == 0)
		{
			iVarRD = 0;
			usNbElem 		= strS7KRecordXML->strTabRDXML[iVarRD].usNbElem;
			usTailleEnByte 	= strS7KRecordXML->strTabRDXML[iVarRD].usSize;
			iFlag = fread(&RD_7041Record->u16BeamNumber, usTailleEnByte, usNbElem, G_fpData);
			// Lecture des sur 1 ou deux octets
			fwrite(&RD_7041Record->u16BeamNumber, usTailleEnByte, usNbElem, strS7KRecordXML->strTabRDXML[iVarRD].ptr_fpBin);
		}
		else
		{
			iVarRD = 1;
			usNbElem 		= strS7KRecordXML->strTabRDXML[iVarRD].usNbElem;
			usTailleEnByte 	= strS7KRecordXML->strTabRDXML[iVarRD].usSize;
			iFlag = fread(&RD_7041Record->f32BeamAngle, usTailleEnByte, usNbElem, G_fpData);
			// Lecture des sur 1 ou deux octets
			fwrite(&RD_7041Record->f32BeamAngle, usTailleEnByte, usNbElem, strS7KRecordXML->strTabRDXML[iVarRD].ptr_fpBin);
		}

		// Lecture du nombre d'�chantillons.
		iVarRD = 2;
		usNbElem 		= strS7KRecordXML->strTabRDXML[iVarRD].usNbElem;
		usTailleEnByte 	= strS7KRecordXML->strTabRDXML[iVarRD].usSize;
		iFlag = fread(&RD_7041Record->S, usTailleEnByte, usNbElem, G_fpData);
		// Lecture des sur 1 ou deux octets
		fwrite(&RD_7041Record->S, usTailleEnByte, usNbElem, strS7KRecordXML->strTabRDXML[iVarRD].ptr_fpBin);

		// Lecture des donn�es.
		if (usDataSize == 0)
		{
			// Lecture des donn�es sur 1 octet.
			iVarRD = 3;
			usNbElem 		= strS7KRecordXML->strTabRDXML[iVarRD].usNbElem*RD_7041Record->S;
			usTailleEnByte 	= strS7KRecordXML->strTabRDXML[iVarRD].usSize;
			RD_7041Record->u8Data 		= malloc(RD_7041Record->S*usTailleEnByte);
			iFlag = fread(RD_7041Record->u8Data, usTailleEnByte, usNbElem, G_fpData);
			// Lecture des sur 1 ou deux octets
			fwrite(RD_7041Record->u8Data, usTailleEnByte, usNbElem, strS7KRecordXML->strTabRDXML[iVarRD].ptr_fpBin);
			free(RD_7041Record->u8Data);
		}
		else
		{
			// Lecture des donn�es sur 2 octets.
			iVarRD = 4;
			usNbElem 		= strS7KRecordXML->strTabRDXML[iVarRD].usNbElem*RD_7041Record->S;
			usTailleEnByte 	= strS7KRecordXML->strTabRDXML[iVarRD].usSize;
			RD_7041Record->u16Data 		= malloc(RD_7041Record->S*usTailleEnByte);
			iFlag = fread(RD_7041Record->u16Data, usTailleEnByte, usNbElem, G_fpData);
			// Lecture des sur 1 ou deux octets
			fwrite(RD_7041Record->u16Data, usTailleEnByte, usNbElem, strS7KRecordXML->strTabRDXML[iVarRD].ptr_fpBin);
			free(RD_7041Record->u16Data);
		}
	}


	// D�sallocation (obligatoire !) des matrices r�serv�es.
	free(RD_7041Record);
	free(RTH_7041Record);


	iNbRecord++;

	return iRetour;

} //iProcess7041Record


//----------------------------------------------------------------
// Fonction :	iInitXMLBin7041Record
// Objet 	:	Initialisation de la structure �quivalent au paquet
//				7041 pour la manipulation des fichiers bin et XML.
// Modification : 15/10/08
// Auteur 	: GLU
//----------------------------------------------------------------
int iInitXMLBin7041Record(	char *cTitleDatagram,
							char *cNomDatagram,
							char *cComments,
							u32 uiODOffset,
							T_S7KRecordXML *strS7KRecordXML)
{

	int iRet = 0;

	T_DESC_SIGNAL_OR_IMAGE strDescriptionSignal[NB_SIGNAL] = {
			{"SonarId", 				"u64", 	1.0, 0.0},
			{"PingNumber", 				"u32", 	1.0, 0.0},
			{"MultiPingSequence", 		"u16", 	1.0, 0.0},
			{"N", 						"u16", 	1.0, 0.0},
			{"Flags", 					"u16", 	1.0, 0.0},
			{"SampleRate", 				"f32", 	1.0, 0.0},
			{"Reserved", 				"u32", 	1.0, 0.0}};


	// D�finition des paquets RD.
	T_DESC_SIGNAL_OR_IMAGE strDescriptionImage[NB_IMAGE] = {
			{"u16BeamNumber",			"u16",	1.0,	0.0},
			{"f32BeamNumber",			"f32",	1.0,	0.0},
			{"S",						"u32",	1.0,	0.0},
			{"u8Data",					"u8",	1.0,	0.0},
			{"u16Data",					"u16",	1.0,	0.0}};

	// D�finition des paquets OD.
	T_DESC_SIGNAL_OR_IMAGE strDescriptionImage2[NB_IMAGE2] = {};


	T_DESC_TAB_SIGANDIMG strDescriptionTab = {	NB_SIGNAL,
												NB_IMAGE,
												NB_IMAGE2,
												NUM_INDEX_TABLE,
												strDescriptionSignal,
												strDescriptionImage,
												strDescriptionImage2};


	// Lancement de l'initialisation des structures de signaux : ouvertures des
	// fichiers, copie des structures, ...
	iRet = utilS7K_initXMLRecord(	strDescriptionTab,
									cTitleDatagram,
									cNomDatagram,
									cComments,
									strS7KRecordXML);

	// Cas particuliers de quelques champs pour leur taille.
	strS7KRecordXML->strTabRTHXML[6].usNbElem = 4; // Reserved

	return EXIT_SUCCESS;

} // iInitXMLBin7041Record

#pragma pack()
