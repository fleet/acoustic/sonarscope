/*
 * S7K_process7041Record.h
 *
 *  Created on: 29 Sept. 2011
 *      Author: rgallou
 */

#ifndef S7K_PROCESS7041RECORD_H_
#define S7K_PROCESS7041RECORD_H_
#pragma pack(1)

#include "S7K_DATAGRAMS.H"

//-Paquet 7041 : partie RTH------------------------
typedef struct {
	u64	SonarId;
	u32	PingNumber;
	u16	MultiPingSequence;
	u16	NBeams;
	u16	Flags;
	f32	SampleRate;
	u32	Reserved[4];
} T_7041_RECORD_RTH;

// Paquet 7041 : partie RD
typedef struct {
	u16	u16BeamNumber;
	f32	f32BeamAngle;
	u32 S;
	u8	*u8Data;
	u16	*u16Data;
} T_7041_RECORD_RD;


int iProcess7041Record(FILE *G_fpData, int iNbRecord,
						u32 uiODOffset,
						T_S7KRecordXML *strS7KRecordXML,
						T_7041_RECORD_RTH *RTH_prev7041Record);

int iInitXMLBin7041Record(	char *cTitleDatagram,
							char *cNomDatagram, char *cComments,
							u32 uiODOffset,
							T_S7KRecordXML *strS7KRecordXML);
#pragma pack()
#endif /* S7K_PROCESS7041RECORD_H_ */
