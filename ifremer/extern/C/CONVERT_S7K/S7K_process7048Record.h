/*
 * S7K_process7048Record.h
 *
 *  Created on: 13 oct. 2008
 *      Author: rgallou
 */

#ifndef S7K_PROCESS7048RECORD_H_
#define S7K_PROCESS7048RECORD_H_
#pragma pack(1)

#include "S7K_DATAGRAMS.H"

//-Paquet 7048 : partie RTH------------------------
typedef struct {
	u64	SonarId;
	u32	PingNumber;
	u16	MultiPingSequence;
	u16	FirstBeam;
	u16	N;
	u32	S;
	u8	Reserved;
	u8	ErrorFlag;
	u32	Reserved1;
} T_7048_RECORD_RTH;

// Paquet 7048 : partie RD
typedef struct {
	f32	*BeamSample;
} T_7048_RECORD_RD;


int iProcess7048Record(FILE *G_fpData, int iNbRecord,
						u32 uiODOffset,
						T_S7KRecordXML *strS7KRecordXML,
						T_7048_RECORD_RTH *RTH_prev7048Record);

int iInitXMLBin7048Record(	char *cTitleDatagram,
							char *cNomDatagram, char *cComments,
							u32 uiODOffset,
							T_S7KRecordXML *strS7KRecordXML);
#pragma pack()
#endif /* S7K_PROCESS7048RECORD_H_ */
