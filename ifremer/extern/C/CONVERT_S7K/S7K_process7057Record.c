/*
 * S7K_process7057Record.c
 *
 *  Created on: 13 oct. 2008
 *      Author: rgallou
 */
#pragma pack(1)

#include <sys/types.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "S7K_Datagrams.h"
#include "S7K_Utilities.h"
#include "S7K_process7057Record.h"
#include "convertFiles_S7K.h"

#define NB_SIGNAL 			12
#define NB_IMAGE 			6
#define NB_IMAGE2 			5
#define NUM_INDEX_TABLE		6	// Identification de l'index Signal des Images (� partir de 0).
#define NUM_INDEX_TABLE2	0	// Identification de l'index Signal des Images (� partir de 0).

//----------------------------------------------------------------
// Fonction :	iProcess7057Record
// Objet 	:	Traitement du paquet 7057
// Modif.	: 	03/11/08
// Auteur 	:	GLU
//----------------------------------------------------------------
int iProcess7057Record(FILE *G_fpData, int iNbRecord,
						u32 uiODOffset,
						T_S7KRecordXML *strS7KRecordXML,
						T_7057_RECORD_RTH *RTH_prev7057Record)
{
    T_7057_RECORD_RTH 	*RTH_7057Record;
    T_7057_RECORD_RD 	*RD_7057Record;
    T_7057_RECORD_OD 	OD_7057Record;

	// Pointeur pour avancer sur les variables automatiquement.
	unsigned char		*ptr_cRTHRecord;
	static void 		*ptr_vVarPrevRecord;
	void				*ptr_vVarRecord;

	BOOL		bFlagVarConstant = TRUE,
				bFlagCompare = TRUE;

    int 	iFlag,
			iVarRTH,
			iVarRD = 0,
			iVarOD = 0,
			iRetour = 0;

	

    unsigned short	usTailleEnByte,
					usNbElem;

	// Conservation de l'information de pr�sence de donn�es Optionnal Data.
    strS7KRecordXML->uiODOffset = uiODOffset;

    // Initialisation de la structure d'identification du paquet
 	if (iNbRecord == 0)
	{
 		iInitXMLBin7057Record("Calibrated Backscatter Imagery Data", "7057", "7k Calibrated Sidescan Data", uiODOffset, strS7KRecordXML);
	}

    // Lecture du RTH du paquet
	RTH_7057Record = (T_7057_RECORD_RTH *)malloc(sizeof(T_7057_RECORD_RTH));
	iFlag = fread(RTH_7057Record, sizeof(T_7057_RECORD_RTH), 1, G_fpData);
	if (iFlag == 0)
	{
		printf("Error in reading : %s\n", G_cFileData);
		return FALSE;
	}
	ptr_cRTHRecord = (unsigned char*)RTH_7057Record;
	// Sauvegarde des donn�es des attributs.
	if (iNbRecord > 0)
	{
		ptr_vVarPrevRecord = (void *)RTH_prev7057Record;
	}
	for (iVarRTH=0; iVarRTH<(int)strS7KRecordXML->sNbVarRTH; iVarRTH++)
	{
		ptr_vVarRecord = (void *)ptr_cRTHRecord;
		if (iVarRTH==0)
		{
			usTailleEnByte = 0;
			usNbElem = 0;
		}
		else
		{
			usTailleEnByte = util_usNbOctetTypeVar(strS7KRecordXML->strTabRTHXML[iVarRTH-1].cType);
			usNbElem = strS7KRecordXML->strTabRTHXML[iVarRTH-1].usNbElem;
		}

		if (iNbRecord > 0)
		{
			bFlagVarConstant = strS7KRecordXML->strTabRTHXML[iVarRTH].bFlagConstant;
			ptr_vVarPrevRecord = ptr_vVarPrevRecord + usTailleEnByte*usNbElem;
			bFlagCompare = util_bComparePrevAndCurrentVal(strS7KRecordXML->strTabRTHXML[iVarRTH].cType,
												ptr_vVarPrevRecord,
												ptr_vVarRecord,
											   strS7KRecordXML->strTabRTHXML[iVarRTH].usNbElem);
			bFlagVarConstant = bFlagVarConstant && bFlagCompare;
			strS7KRecordXML->strTabRTHXML[iVarRTH].bFlagConstant = bFlagVarConstant;
		}
		// Ecriture sp�ciale car contenant du XML pour la variable Device Info
		// Pr�paration de l'�criture de la variable.
		usTailleEnByte = strS7KRecordXML->strTabRTHXML[iVarRTH].usSize;
		usNbElem =  strS7KRecordXML->strTabRTHXML[iVarRTH].usNbElem;
		fwrite(ptr_cRTHRecord, usTailleEnByte, usNbElem, strS7KRecordXML->strTabRTHXML[iVarRTH].ptr_fpBin);
	    strS7KRecordXML->strTabRTHXML[iVarRTH].ptr_vPrevVal = ptr_cRTHRecord;
		ptr_cRTHRecord += usTailleEnByte*usNbElem;
	}

	// Recopie en zone tampon des valeurs courantes.
	memmove(RTH_prev7057Record, RTH_7057Record, sizeof(T_7057_RECORD_RTH));

	// --------------------------------------------
 	// Traitement des paquets RD
 	// Lecture des datas les uns apr�s les autres
	RD_7057Record = (T_7057_RECORD_RD *)malloc(sizeof(T_7057_RECORD_RD));
	// Lecture des sur 4 ou 8 octets
	if (RTH_7057Record->W == 4)
	{
		RD_7057Record->u32PortBeamSample 		= malloc(RTH_7057Record->S*sizeof(u32));
		RD_7057Record->u32StarboardBeamSample 	= malloc(RTH_7057Record->S*sizeof(u32));
		iFlag = fread(RD_7057Record->u32PortBeamSample, sizeof(u32), RTH_7057Record->S, G_fpData);
		iFlag = fread(RD_7057Record->u32StarboardBeamSample, sizeof(u32), RTH_7057Record->S, G_fpData);

		// Les donn�es sont sur 32 Bits.
		iVarRD = 0;
		usNbElem 		= strS7KRecordXML->strTabRDXML[iVarRD].usNbElem*RTH_7057Record->S;
		usTailleEnByte 	= strS7KRecordXML->strTabRDXML[iVarRD].usSize;
		fwrite(RD_7057Record->u32PortBeamSample, usTailleEnByte, usNbElem, strS7KRecordXML->strTabRDXML[iVarRD].ptr_fpBin);
		if (iNbRecord > 0 && strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant == TRUE)
		{
			bFlagCompare = util_bComparePrevAndCurrentVal(strS7KRecordXML->strTabRDXML[iVarRD].cType,
												strS7KRecordXML->strTabRDXML[iVarRD].ptr_vPrevVal,
												RD_7057Record->u32PortBeamSample,
												usNbElem);
			strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant = bFlagCompare && strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant;
		}
		if (iNbRecord == 0)
		{
			strS7KRecordXML->strTabRDXML[iVarRD].ptr_vPrevVal = malloc(usNbElem*usTailleEnByte);
		}
		memmove(strS7KRecordXML->strTabRDXML[iVarRD].ptr_vPrevVal, RD_7057Record->u32PortBeamSample, usNbElem*usTailleEnByte);

		iVarRD++;
		usNbElem 		= strS7KRecordXML->strTabRDXML[iVarRD].usNbElem*RTH_7057Record->S;
		usTailleEnByte 	= strS7KRecordXML->strTabRDXML[iVarRD].usSize;
		fwrite(RD_7057Record->u32StarboardBeamSample, usTailleEnByte, usNbElem, strS7KRecordXML->strTabRDXML[iVarRD].ptr_fpBin);
		if (iNbRecord > 0 && strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant == TRUE)
		{
			bFlagCompare = util_bComparePrevAndCurrentVal(strS7KRecordXML->strTabRDXML[iVarRD].cType,
												strS7KRecordXML->strTabRDXML[iVarRD].ptr_vPrevVal,
												RD_7057Record->u32StarboardBeamSample,
												usNbElem);
			strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant = bFlagCompare && strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant;
		}
		if (iNbRecord == 0)
		{
			strS7KRecordXML->strTabRDXML[iVarRD].ptr_vPrevVal = malloc(usNbElem*usTailleEnByte);
		}
		memmove(strS7KRecordXML->strTabRDXML[iVarRD].ptr_vPrevVal, RD_7057Record->u32StarboardBeamSample, usNbElem*usTailleEnByte);


		free(RD_7057Record->u32PortBeamSample);
		free(RD_7057Record->u32StarboardBeamSample);
	}

	else if (RTH_7057Record->W == 8)
	{
		RD_7057Record->u64PortBeamSample 		= malloc(RTH_7057Record->S*sizeof(u64));
		RD_7057Record->u64StarboardBeamSample 	= malloc(RTH_7057Record->S*sizeof(u64));
		iFlag = fread(RD_7057Record->u64PortBeamSample, sizeof(u64), RTH_7057Record->S, G_fpData);
		iFlag = fread(RD_7057Record->u64StarboardBeamSample, sizeof(u64), RTH_7057Record->S, G_fpData);

		iVarRD = 2;
		usNbElem 		= strS7KRecordXML->strTabRDXML[iVarRD].usNbElem*RTH_7057Record->S;
		usTailleEnByte 	= strS7KRecordXML->strTabRDXML[iVarRD].usSize;
		fwrite(RD_7057Record->u64PortBeamSample, usTailleEnByte, usNbElem, strS7KRecordXML->strTabRDXML[iVarRD].ptr_fpBin);
		if (iNbRecord > 0 && strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant == TRUE)
		{
			bFlagCompare = util_bComparePrevAndCurrentVal(strS7KRecordXML->strTabRDXML[iVarRD].cType,
												strS7KRecordXML->strTabRDXML[iVarRD].ptr_vPrevVal,
												RD_7057Record->u64PortBeamSample,
												usNbElem);
			strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant = bFlagCompare && strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant;
		}
		if (iNbRecord == 0)
		{
			strS7KRecordXML->strTabRDXML[iVarRD].ptr_vPrevVal = malloc(usNbElem*usTailleEnByte);
		}
		memmove(strS7KRecordXML->strTabRDXML[iVarRD].ptr_vPrevVal, RD_7057Record->u64PortBeamSample, usNbElem*usTailleEnByte);

		iVarRD++;
		usNbElem 		= strS7KRecordXML->strTabRDXML[iVarRD].usNbElem*RTH_7057Record->S;
		usTailleEnByte	= strS7KRecordXML->strTabRDXML[iVarRD].usSize;
		fwrite(RD_7057Record->u64StarboardBeamSample, usTailleEnByte, usNbElem, strS7KRecordXML->strTabRDXML[iVarRD].ptr_fpBin);
		if (iNbRecord > 0 && strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant == TRUE)
		{
			bFlagCompare = util_bComparePrevAndCurrentVal(strS7KRecordXML->strTabRDXML[iVarRD].cType,
												strS7KRecordXML->strTabRDXML[iVarRD].ptr_vPrevVal,
												RD_7057Record->u64StarboardBeamSample,
												usNbElem);
			strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant = bFlagCompare && strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant;
		}
		if (iNbRecord == 0)
		{
			strS7KRecordXML->strTabRDXML[iVarRD].ptr_vPrevVal = malloc(usNbElem*usTailleEnByte);
		}
		memmove(strS7KRecordXML->strTabRDXML[iVarRD].ptr_vPrevVal, RD_7057Record->u32StarboardBeamSample, usNbElem*usTailleEnByte);

		free(RD_7057Record->u64PortBeamSample);
		free(RD_7057Record->u64StarboardBeamSample);
	}

	// Lecture des Beams Index
	RD_7057Record->PortBeamNumber 		= (u16 *)malloc(RTH_7057Record->S*sizeof(u16));
	iFlag = fread(RD_7057Record->PortBeamNumber, sizeof(u16), RTH_7057Record->S, G_fpData);
	RD_7057Record->StarboardBeamNumber 	= (u16 *)malloc(RTH_7057Record->S*sizeof(u16));
	iFlag = fread(RD_7057Record->StarboardBeamNumber, sizeof(u16), RTH_7057Record->S, G_fpData);

	iVarRD = 4;
	usNbElem 		= strS7KRecordXML->strTabRDXML[iVarRD].usNbElem*RTH_7057Record->S;
	usTailleEnByte 	= strS7KRecordXML->strTabRDXML[iVarRD].usSize;
	fwrite(RD_7057Record->PortBeamNumber, usTailleEnByte, usNbElem, strS7KRecordXML->strTabRDXML[iVarRD].ptr_fpBin);
	if (iNbRecord > 0 && strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant == TRUE)
	{
		bFlagCompare = util_bComparePrevAndCurrentVal(strS7KRecordXML->strTabRDXML[iVarRD].cType,
											strS7KRecordXML->strTabRDXML[iVarRD].ptr_vPrevVal,
											RD_7057Record->PortBeamNumber,
											usNbElem);
		strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant = bFlagCompare && strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant;
	}
	if (iNbRecord == 0)
	{
		strS7KRecordXML->strTabRDXML[iVarRD].ptr_vPrevVal = malloc(usNbElem*usTailleEnByte);
	}
	memmove(strS7KRecordXML->strTabRDXML[iVarRD].ptr_vPrevVal, RD_7057Record->PortBeamNumber, usNbElem*usTailleEnByte);

	iVarRD++;
	usNbElem 		= strS7KRecordXML->strTabRDXML[iVarRD].usNbElem*RTH_7057Record->S;
	usTailleEnByte 	= strS7KRecordXML->strTabRDXML[iVarRD].usSize;
	fwrite(RD_7057Record->StarboardBeamNumber, usTailleEnByte, usNbElem, strS7KRecordXML->strTabRDXML[iVarRD].ptr_fpBin);
	if (iNbRecord > 0 && strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant == TRUE)
	{
		bFlagCompare = util_bComparePrevAndCurrentVal(strS7KRecordXML->strTabRDXML[iVarRD].cType,
											strS7KRecordXML->strTabRDXML[iVarRD].ptr_vPrevVal,
											RD_7057Record->StarboardBeamNumber,
											usNbElem);
		strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant = bFlagCompare && strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant;
	}
	if (iNbRecord == 0)
	{
		strS7KRecordXML->strTabRDXML[iVarRD].ptr_vPrevVal = malloc(usNbElem*usTailleEnByte);
	}
	memmove(strS7KRecordXML->strTabRDXML[iVarRD].ptr_vPrevVal, RD_7057Record->StarboardBeamNumber, usNbElem*usTailleEnByte);

	// Traitement des donn�es Optionnelles du Paquet 7057
	if (uiODOffset != 0)
	{
		//OD_7057Record = (T_7057_RECORD_OD *)malloc(sizeof(T_7057_RECORD_OD));
		// Ecriture des donn�es dans les binaires de stockage.
		iVarOD = 0;
		usTailleEnByte = strS7KRecordXML->strTabODXML[iVarOD].usSize;

		iFlag = fread(&OD_7057Record.Frequency, usTailleEnByte, 1, G_fpData);
		iFlag = fwrite(&OD_7057Record.Frequency, usTailleEnByte, 1, strS7KRecordXML->strTabODXML[iVarOD].ptr_fpBin);
		if (iNbRecord > 0 && strS7KRecordXML->strTabODXML[iVarOD].bFlagConstant == TRUE)
		{
			bFlagCompare = util_bComparePrevAndCurrentVal(strS7KRecordXML->strTabRTHXML[iVarOD].cType,
												strS7KRecordXML->strTabODXML[iVarOD].ptr_vPrevVal,
												&OD_7057Record.Frequency,
												strS7KRecordXML->strTabODXML[iVarOD].usNbElem);
			strS7KRecordXML->strTabODXML[iVarOD].bFlagConstant = bFlagCompare && strS7KRecordXML->strTabODXML[iVarOD].bFlagConstant;
		}
		if (iNbRecord == 0)
		{
			strS7KRecordXML->strTabODXML[iVarOD].ptr_vPrevVal = (void *)malloc(usTailleEnByte);
		}
		memmove(strS7KRecordXML->strTabODXML[iVarOD].ptr_vPrevVal, &OD_7057Record.Frequency, usTailleEnByte);


		iVarOD++;
		usTailleEnByte = strS7KRecordXML->strTabODXML[iVarOD].usSize;
		iFlag = fread(&OD_7057Record.Latitude, usTailleEnByte, 1, G_fpData);
		iFlag = fwrite(&OD_7057Record.Latitude, usTailleEnByte, 1, strS7KRecordXML->strTabODXML[iVarOD].ptr_fpBin);
		if (iNbRecord > 0 && strS7KRecordXML->strTabODXML[iVarOD].bFlagConstant == TRUE)
		{
			bFlagCompare = util_bComparePrevAndCurrentVal(strS7KRecordXML->strTabRTHXML[iVarOD].cType,
												strS7KRecordXML->strTabODXML[iVarOD].ptr_vPrevVal,
												&OD_7057Record.Latitude,
												strS7KRecordXML->strTabODXML[iVarOD].usNbElem);
			strS7KRecordXML->strTabODXML[iVarOD].bFlagConstant = bFlagCompare && strS7KRecordXML->strTabODXML[iVarOD].bFlagConstant;
		}
		if (iNbRecord == 0)
		{
			strS7KRecordXML->strTabODXML[iVarOD].ptr_vPrevVal = (void *)malloc(usTailleEnByte);
		}
		memmove(strS7KRecordXML->strTabODXML[iVarOD].ptr_vPrevVal, &OD_7057Record.Latitude, usTailleEnByte);

		iVarOD++;
		usTailleEnByte = strS7KRecordXML->strTabODXML[iVarOD].usSize;
		iFlag = fread(&OD_7057Record.Longitude,usTailleEnByte, 1, G_fpData);
		iFlag = fwrite(&OD_7057Record.Longitude, usTailleEnByte, 1, strS7KRecordXML->strTabODXML[iVarOD].ptr_fpBin);
		if (iNbRecord > 0 && strS7KRecordXML->strTabODXML[iVarOD].bFlagConstant == TRUE)
		{
			bFlagCompare = util_bComparePrevAndCurrentVal(strS7KRecordXML->strTabRTHXML[iVarOD].cType,
												strS7KRecordXML->strTabODXML[iVarOD].ptr_vPrevVal,
												&OD_7057Record.Longitude,
												strS7KRecordXML->strTabODXML[iVarOD].usNbElem);
			strS7KRecordXML->strTabODXML[iVarOD].bFlagConstant = bFlagCompare && strS7KRecordXML->strTabODXML[iVarOD].bFlagConstant;
		}
		if (iNbRecord == 0)
		{
			strS7KRecordXML->strTabODXML[iVarOD].ptr_vPrevVal = (void *)malloc(usTailleEnByte);
		}
		memmove(strS7KRecordXML->strTabODXML[iVarOD].ptr_vPrevVal, &OD_7057Record.Longitude, usTailleEnByte);

		iVarOD++;
		usTailleEnByte = strS7KRecordXML->strTabODXML[iVarOD].usSize;
		iFlag = fread(&OD_7057Record.Heading, usTailleEnByte, 1, G_fpData);
		iFlag = fwrite(&OD_7057Record.Heading, usTailleEnByte, 1, strS7KRecordXML->strTabODXML[iVarOD].ptr_fpBin);
		if (iNbRecord > 0 && strS7KRecordXML->strTabODXML[iVarOD].bFlagConstant == TRUE)
		{
			bFlagCompare = util_bComparePrevAndCurrentVal(strS7KRecordXML->strTabRTHXML[iVarOD].cType,
												strS7KRecordXML->strTabODXML[iVarOD].ptr_vPrevVal,
												&OD_7057Record.Heading,
												strS7KRecordXML->strTabODXML[iVarOD].usNbElem);
			strS7KRecordXML->strTabODXML[iVarOD].bFlagConstant = bFlagCompare && strS7KRecordXML->strTabODXML[iVarOD].bFlagConstant;
		}
		if (iNbRecord == 0)
		{
			strS7KRecordXML->strTabODXML[iVarOD].ptr_vPrevVal = (void *)malloc(usTailleEnByte);
		}
		memmove(strS7KRecordXML->strTabODXML[iVarOD].ptr_vPrevVal, &OD_7057Record.Heading, usTailleEnByte);

		iVarOD++;
		usTailleEnByte = strS7KRecordXML->strTabODXML[iVarOD].usSize;
		iFlag = fread(&OD_7057Record.Depth, usTailleEnByte, 1, G_fpData);
		iFlag = fwrite(&OD_7057Record.Depth, usTailleEnByte, 1, strS7KRecordXML->strTabODXML[iVarOD].ptr_fpBin);
		if (iNbRecord > 0 && strS7KRecordXML->strTabODXML[iVarOD].bFlagConstant == TRUE)
		{
			bFlagCompare = util_bComparePrevAndCurrentVal(strS7KRecordXML->strTabRTHXML[iVarOD].cType,
												strS7KRecordXML->strTabODXML[iVarOD].ptr_vPrevVal,
												&OD_7057Record.Depth,
												strS7KRecordXML->strTabODXML[iVarOD].usNbElem);
			strS7KRecordXML->strTabODXML[iVarOD].bFlagConstant = bFlagCompare && strS7KRecordXML->strTabODXML[iVarOD].bFlagConstant;
		}
		if (iNbRecord == 0)
		{
			strS7KRecordXML->strTabODXML[iVarOD].ptr_vPrevVal = (void *)malloc(usTailleEnByte);
		}
		memmove(strS7KRecordXML->strTabODXML[iVarOD].ptr_vPrevVal, &OD_7057Record.Depth, usTailleEnByte);

	} // Fin du traitement du paquet OD.

	// D�sallocation (obligatoire !) des matrices r�serv�es.
	free(RD_7057Record->PortBeamNumber);
	free(RD_7057Record->StarboardBeamNumber);
	free(RD_7057Record);
	free(RTH_7057Record);


	iNbRecord++;

	return iRetour;

} //iProcess7057Record


//----------------------------------------------------------------
// Fonction :	iInitXMLBin7057Record
// Objet 	:	Initialisation de la structure �quivalent au paquet
//				7057 pour la manipulation des fichiers bin et XML.
// Modification : 15/10/08
// Auteur 	: GLU
//----------------------------------------------------------------
int iInitXMLBin7057Record(	char *cTitleDatagram,
							char *cNomDatagram,
							char *cComments,
							u32 uiODOffset,
							T_S7KRecordXML *strS7KRecordXML)
{

	int iRet = 0;

	T_DESC_SIGNAL_OR_IMAGE strDescriptionSignal[NB_SIGNAL] = {
			{"SonarId", "u64", 				1.0, 0.0},
			{"PingNumber", "u32", 			1.0, 0.0},
			{"MultiPingSequence", "u16", 	1.0, 0.0},
			{"BeamPosition", "f32", 		1.0, 0.0},
			{"ControlFlags", "u32", 		1.0, 0.0},
			{"S", "u32", 					1.0, 0.0},
			{"Reserved", "f32", 			1.0, 0.0},
			{"N", "u16", 					1.0, 0.0},
			{"CurrentBeamNumber", "u16", 	1.0, 0.0},
			{"W", "u8", 					1.0, 0.0},
			{"DataTypes", "u8", 			1.0, 0.0},
			{"ErrorFlag", "u8", 			1.0, 0.0}};


	// D�finition des paquets RD.
	T_DESC_SIGNAL_OR_IMAGE strDescriptionImage[NB_IMAGE] = {
			{"u32PortBeamSample",		"u32",	1.0,	0.0},
			{"u32StarboardBeamSample",	"u32",	1.0,	0.0},
			{"u64PortBeamSample",		"u64",	1.0,	0.0},
			{"u64StarboardBeamSample",	"u64",	1.0,	0.0},
			{"PortBeamNumber",			"u16",	1.0,	0.0},
			{"StarboardBeamNumber",		"u16",	1.0,	0.0}};

	// D�finition des paquets OD.
	T_DESC_SIGNAL_OR_IMAGE strDescriptionImage2[NB_IMAGE2] = {
			{"Frequency",	"f32",	1.0,	0.0},
			{"Latitude",	"f64",	1.0,	0.0},
			{"Longitude",	"f64",	1.0,	0.0},
			{"Heading",		"f32",	1.0,	0.0},
			{"Depth",		"f32",	1.0,	0.0}};


	T_DESC_TAB_SIGANDIMG strDescriptionTab = {	NB_SIGNAL,
												NB_IMAGE,
												NB_IMAGE2,
												NUM_INDEX_TABLE,
												strDescriptionSignal,
												strDescriptionImage,
												strDescriptionImage2};


	// Lancement de l'initialisation des structures de signaux : ouvertures des
	// fichiers, copie des structures, ...
	iRet = utilS7K_initXMLRecord(	strDescriptionTab,
									cTitleDatagram,
									cNomDatagram,
									cComments,
									strS7KRecordXML);

	// Cas particuliers de quelques champs pour leur taille.
	strS7KRecordXML->strTabRTHXML[6].usNbElem = 8; // Reserved

	return EXIT_SUCCESS;

} // iInitXMLBin7057Record

#pragma pack()
