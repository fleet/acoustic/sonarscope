/*
 * S7K_process7057Record.h
 *
 *  Created on: 13 oct. 2008
 *      Author: rgallou
 */

#ifndef S7K_PROCESS7057RECORD_H_
#define S7K_PROCESS7057RECORD_H_
#pragma pack(1)

#include "S7K_DATAGRAMS.H"

//-Paquet 7057 : partie RTH------------------------
typedef struct {
	u64	SonarId;
	u32	PingNumber;
	u16	MultiPingSequence;
	f32	BeamPosition;
	u32	ControlFlags;
	u32	S;
	f32	Reserved[8];
	u16	N;
	u16	CurrentBeamNumber;
	u8	W;
	u8	DataTypes;
	u8	ErrorFlag;
} T_7057_RECORD_RTH;

// A la place de Reserved
//f32 Port3dBBeamWithY;
//f32 Port3dBBeamWithZ;
//f32 Startboard_3dBBeamWithY;
//f32 Startboard_3dBBeamWithZ;
//f32 PortBeamSteeringWithY;
//f32 PortBeamSteeringWithZ;
//f32 StartboardBeamSteeringAngleY;
//f32 StartboardBeamSteeringAngleZ;

// Paquet 7057 : partie RD
typedef struct {
	u32	*u32PortBeamSample;
	u32 *u32StarboardBeamSample;
	u64	*u64PortBeamSample; 		// Amp ou phase lue.
	u64	*u64StarboardBeamSample;
	u16	*PortBeamNumber;
	u16	*StarboardBeamNumber;
} T_7057_RECORD_RD;

// Paquet 7057 : partie OD
typedef struct {
	f32	Frequency;
	f64	Latitude;
	f64	Longitude;
	f32	Heading;
	f32	Depth;
} T_7057_RECORD_OD;

int iProcess7057Record(FILE *G_fpData, int iNbRecord,
						u32 uiODOffset,
						T_S7KRecordXML *strS7KRecordXML,
						T_7057_RECORD_RTH *RTH_prev7057Record);

int iInitXMLBin7057Record(	char *cTitleDatagram,
							char *cNomDatagram, char *cComments,
							u32 uiODOffset,
							T_S7KRecordXML *strS7KRecordXML);
#pragma pack()
#endif /* S7K_PROCESS7057RECORD_H_ */
