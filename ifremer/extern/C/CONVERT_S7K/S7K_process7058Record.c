/*
 * S7K_process7058Record.c
 *
 *  Created on: 13 oct. 2008
 *      Author: rgallou
 */
#pragma pack(1)

#include <sys/types.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "S7K_Datagrams.h"
#include "S7K_Utilities.h"
#include "S7K_process7058Record.h"
#include "convertFiles_S7K.h"

#define NB_SIGNAL 			7
#define NB_IMAGE 			6
#define NB_IMAGE2 			7
#define NUM_INDEX_TABLE		3	// Identification de l'index Signal des Images (� partir de 0).
#define NUM_INDEX_TABLE2	0	// Identification de l'index Signal des Images (� partir de 0).

//----------------------------------------------------------------
// Fonction :	iProcess7058Record
// Objet 	:	Traitement du paquet 7058
// Modif.	: 	03/11/08
// Auteur 	:	GLU
//----------------------------------------------------------------
int iProcess7058Record(FILE *G_fpData, int iNbRecord,
						u32 uiODOffset,
						T_S7KRecordXML *strS7KRecordXML,
						T_7058_RECORD_RTH *RTH_prev7058Record)
{
    T_7058_RECORD_RTH 	*RTH_7058Record;
    T_7058_RECORD_RD 	*RD_7058Record;
    T_7058_RECORD_OD 	*OD_7058Record;

	// Pointeur pour avancer sur les variables automatiquement.
	unsigned char		*ptr_cRTHRecord;
	static void 		*ptr_vVarPrevRecord;
	void				*ptr_vVarRecord;

	BOOL		bFlagVarConstant = TRUE,
				bFlagCompare = TRUE;

    int 	iFlag,
			iVarRD,
			iVarRTH,
			iVarOD,
			iLoop,
			iRetour = 0;

	

    unsigned short	usTailleEnByte,
					usNbElem;

    // Conservation de l'information de pr�sence de donn�es Optionnal Data.
    strS7KRecordXML->uiODOffset = uiODOffset;

    // Initialisation de la structure d'identification du paquet
 	if (iNbRecord == 0)
	{
 		iInitXMLBin7058Record("Calibrated Snippet Data", "7058", "7k Calibrated Snippets Data", uiODOffset, strS7KRecordXML);
	}

    // Lecture du RTH du paquet
	RTH_7058Record = (T_7058_RECORD_RTH *)malloc(sizeof(T_7058_RECORD_RTH));
	iFlag = fread(RTH_7058Record, sizeof(T_7058_RECORD_RTH), 1, G_fpData);
	if (iFlag == 0)
	{
		printf("Error in reading : %s\n", G_cFileData);
		return FALSE;
	}

	ptr_cRTHRecord = (unsigned char*)RTH_7058Record;
	// Sauvegarde des donn�es des attributs.
	if (iNbRecord > 0)
	{
		ptr_vVarPrevRecord = (void *)RTH_prev7058Record;
	}
	for (iVarRTH=0; iVarRTH<(int)strS7KRecordXML->sNbVarRTH; iVarRTH++)
	{
		ptr_vVarRecord = (void *)ptr_cRTHRecord;
		if (iVarRTH==0)
		{
			usTailleEnByte = 0;
			usNbElem = 0;
		}
		else
		{
			usTailleEnByte = util_usNbOctetTypeVar(strS7KRecordXML->strTabRTHXML[iVarRTH-1].cType);
			usNbElem = strS7KRecordXML->strTabRTHXML[iVarRTH-1].usNbElem;
		}


		if (iNbRecord > 0)
		{
			bFlagVarConstant = strS7KRecordXML->strTabRTHXML[iVarRTH].bFlagConstant;
			ptr_vVarPrevRecord = ptr_vVarPrevRecord + usTailleEnByte*usNbElem;
			bFlagCompare = util_bComparePrevAndCurrentVal(strS7KRecordXML->strTabRTHXML[iVarRTH].cType,
												ptr_vVarPrevRecord,
												ptr_vVarRecord,
											   strS7KRecordXML->strTabRTHXML[iVarRTH].usNbElem);
			bFlagVarConstant = bFlagVarConstant && bFlagCompare;
			strS7KRecordXML->strTabRTHXML[iVarRTH].bFlagConstant = bFlagVarConstant;
		}
		// Ecriture sp�ciale car contenant du XML pour la variable Device Info
		// Pr�paration de l'�criture de la variable.
		usTailleEnByte = strS7KRecordXML->strTabRTHXML[iVarRTH].usSize;
		usNbElem =  strS7KRecordXML->strTabRTHXML[iVarRTH].usNbElem;
		fwrite(ptr_cRTHRecord, usTailleEnByte, usNbElem, strS7KRecordXML->strTabRTHXML[iVarRTH].ptr_fpBin);
		strS7KRecordXML->strTabRTHXML[iVarRTH].ptr_vPrevVal = ptr_cRTHRecord;
		ptr_cRTHRecord += usTailleEnByte*usNbElem;
	}

	// Recopie en zone tampon des valeurs courantes.
	memmove(RTH_prev7058Record, RTH_7058Record, sizeof(T_7058_RECORD_RTH));

	// --------------------------------------------
 	// Traitement des paquets RD
 	RD_7058Record = (T_7058_RECORD_RD *)malloc(sizeof(T_7058_RECORD_RD));
 	// Lecture des datas de fa�on adapt�e � lecture des Samples qui
 	// d�pend des valeurs de Begin et End Sample Descriptor.
 	for (iLoop = 0; iLoop < RTH_7058Record->N; iLoop++)
    {
    	// Ecriture des donn�es de la partie RD dans les binaires de stockage.
    	iVarRD = 0;
    	usTailleEnByte = strS7KRecordXML->strTabRDXML[iVarRD].usSize;
    	usNbElem = strS7KRecordXML->strTabRDXML[iVarRD].usNbElem;
    	iFlag = fread(&RD_7058Record->BeamDescriptor, usTailleEnByte, usNbElem, G_fpData);
    	fwrite(&RD_7058Record->BeamDescriptor, usTailleEnByte, usNbElem, strS7KRecordXML->strTabRDXML[iVarRD].ptr_fpBin);
    	if (iNbRecord > 0 && strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant == TRUE)
    	{
    		bFlagCompare = util_bComparePrevAndCurrentVal(strS7KRecordXML->strTabRDXML[iVarRD].cType,
    											strS7KRecordXML->strTabRDXML[iVarRD].ptr_vPrevVal,
    											(void *)&RD_7058Record->BeamDescriptor,
    											usNbElem);
    		strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant = bFlagCompare && strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant;
    	}
    	if (iNbRecord == 0)
    	{
    		strS7KRecordXML->strTabRDXML[iVarRD].ptr_vPrevVal = malloc(usNbElem*usTailleEnByte);
    	}
    	memmove(strS7KRecordXML->strTabRDXML[iVarRD].ptr_vPrevVal, &RD_7058Record->BeamDescriptor, usNbElem*usTailleEnByte);

    	iVarRD++;
    	usTailleEnByte = strS7KRecordXML->strTabRDXML[iVarRD].usSize;
    	usNbElem = strS7KRecordXML->strTabRDXML[iVarRD].usNbElem;
    	iFlag = fread(&RD_7058Record->BeginSampleDescriptor, usTailleEnByte, usNbElem, G_fpData);
    	fwrite(&RD_7058Record->BeginSampleDescriptor, usTailleEnByte, usNbElem, strS7KRecordXML->strTabRDXML[iVarRD].ptr_fpBin);
    	if (iNbRecord > 0 && strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant == TRUE)
    	{
    		bFlagCompare = util_bComparePrevAndCurrentVal(strS7KRecordXML->strTabRDXML[iVarRD].cType,
    											strS7KRecordXML->strTabRDXML[iVarRD].ptr_vPrevVal,
    											(void *)&RD_7058Record->BeginSampleDescriptor,
    											usNbElem);
    		strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant = bFlagCompare && strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant;
    	}
    	if (iNbRecord == 0)
    	{
    		strS7KRecordXML->strTabRDXML[iVarRD].ptr_vPrevVal = malloc(usNbElem*usTailleEnByte);
    	}
    	memmove(strS7KRecordXML->strTabRDXML[iVarRD].ptr_vPrevVal, &RD_7058Record->BeginSampleDescriptor, usNbElem*usTailleEnByte);


    	iVarRD++;
    	usTailleEnByte = strS7KRecordXML->strTabRDXML[iVarRD].usSize;
    	usNbElem = strS7KRecordXML->strTabRDXML[iVarRD].usNbElem;
    	iFlag = fread(&RD_7058Record->BottomDetectSample, usTailleEnByte, usNbElem, G_fpData);
    	fwrite(&RD_7058Record->BottomDetectSample, usTailleEnByte, usNbElem, strS7KRecordXML->strTabRDXML[iVarRD].ptr_fpBin);
    	if (iNbRecord > 0 && strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant == TRUE)
    	{
    		bFlagCompare = util_bComparePrevAndCurrentVal(strS7KRecordXML->strTabRDXML[iVarRD].cType,
    											strS7KRecordXML->strTabRDXML[iVarRD].ptr_vPrevVal,
    											(void *)&RD_7058Record->BottomDetectSample,
    											usNbElem);
    		strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant = bFlagCompare && strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant;
    	}
    	strS7KRecordXML->strTabRDXML[iVarRD].ptr_vPrevVal = malloc(usNbElem*usTailleEnByte);
    	memmove(strS7KRecordXML->strTabRDXML[iVarRD].ptr_vPrevVal, &RD_7058Record->BottomDetectSample, usNbElem*usTailleEnByte);


    	iVarRD++;
    	usTailleEnByte = strS7KRecordXML->strTabRDXML[iVarRD].usSize;
    	usNbElem = strS7KRecordXML->strTabRDXML[iVarRD].usNbElem;
    	iFlag = fread(&RD_7058Record->EndSampleDescriptor, usTailleEnByte, usNbElem, G_fpData);
    	fwrite(&RD_7058Record->EndSampleDescriptor, usTailleEnByte, usNbElem, strS7KRecordXML->strTabRDXML[iVarRD].ptr_fpBin);
    	if (iNbRecord > 0 && strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant == TRUE)
    	{
    		bFlagCompare = util_bComparePrevAndCurrentVal(strS7KRecordXML->strTabRDXML[iVarRD].cType,
    											strS7KRecordXML->strTabRDXML[iVarRD].ptr_vPrevVal,
    											(void *)&RD_7058Record->EndSampleDescriptor,
    											usNbElem);
    		strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant = bFlagCompare && strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant;
    	}
    	if (iNbRecord == 0)
    	{
    		strS7KRecordXML->strTabRDXML[iVarRD].ptr_vPrevVal = malloc(usNbElem*usTailleEnByte);
    	}
    	memmove(strS7KRecordXML->strTabRDXML[iVarRD].ptr_vPrevVal, &RD_7058Record->EndSampleDescriptor, usNbElem*usTailleEnByte);

    	// Lecture des �chantillons selon les indices de D�but et de Fin.
    	iVarRD++;
    	usTailleEnByte = strS7KRecordXML->strTabRDXML[iVarRD].usSize;
    	// Le nb d'�chantillon est calcul� selon les indices lus pr�c�demment.
        usNbElem = strS7KRecordXML->strTabRDXML[iVarRD].usNbElem*(RD_7058Record->EndSampleDescriptor-RD_7058Record->BeginSampleDescriptor+1);
    	RD_7058Record->Samples = malloc(usNbElem*usTailleEnByte);
    	fwrite(RD_7058Record->Samples, usTailleEnByte, usNbElem, strS7KRecordXML->strTabRDXML[iVarRD].ptr_fpBin);
    	if (iNbRecord > 0 && strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant == TRUE)
    	{
    		bFlagCompare = util_bComparePrevAndCurrentVal(strS7KRecordXML->strTabRDXML[iVarRD].cType,
    											strS7KRecordXML->strTabRDXML[iVarRD].ptr_vPrevVal,
    											RD_7058Record->Samples,
    											usNbElem);
    		strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant = bFlagCompare && strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant;
    	}
    	strS7KRecordXML->strTabRDXML[iVarRD].ptr_vPrevVal = malloc(usNbElem*usTailleEnByte);
    	memmove(strS7KRecordXML->strTabRDXML[iVarRD].ptr_vPrevVal, RD_7058Record->Samples, usNbElem*usTailleEnByte);
    	free(RD_7058Record->Samples);
    } // Fin de la boucle de lecture des donn�es RD



	// Traitement des donn�es Optinnelles du Paquet 7058
	if (uiODOffset != 0 && RTH_7058Record->N > 0)
	{
		OD_7058Record = (T_7058_RECORD_OD *)malloc(sizeof(T_7058_RECORD_OD));
		OD_7058Record->BeamAlongTrackDistance = malloc(RTH_7058Record->N*sizeof(f32));
		OD_7058Record->BeamAcrossTrackDistance = malloc(RTH_7058Record->N*sizeof(f32));
		OD_7058Record->CenterSampleDistance = malloc(RTH_7058Record->N*sizeof(f32));

		// Ecriture des donn�es dans les binaires de stockage.
		iVarOD = 0;
		usTailleEnByte = strS7KRecordXML->strTabODXML[iVarOD].usSize;
		iFlag = fread(&OD_7058Record->Frequency, usTailleEnByte, 1, G_fpData);
		iFlag = fwrite(&OD_7058Record->Frequency, usTailleEnByte, 1, strS7KRecordXML->strTabODXML[iVarOD].ptr_fpBin);
		if (iNbRecord > 0 && strS7KRecordXML->strTabODXML[iVarOD].bFlagConstant == TRUE)
		{
			bFlagCompare = util_bComparePrevAndCurrentVal(strS7KRecordXML->strTabRTHXML[iVarOD].cType,
												strS7KRecordXML->strTabODXML[iVarOD].ptr_vPrevVal,
												&OD_7058Record->Frequency,
												strS7KRecordXML->strTabODXML[iVarOD].usNbElem);
			strS7KRecordXML->strTabODXML[iVarOD].bFlagConstant = bFlagCompare && strS7KRecordXML->strTabODXML[iVarOD].bFlagConstant;
		}
		if (iNbRecord == 0)
		{
			strS7KRecordXML->strTabODXML[iVarOD].ptr_vPrevVal = (void *)malloc((RTH_7058Record->N*usTailleEnByte));
		}
		memmove(strS7KRecordXML->strTabODXML[iVarOD].ptr_vPrevVal, &OD_7058Record->Frequency, RTH_7058Record->N*usTailleEnByte);


		iVarOD++;
		usTailleEnByte = strS7KRecordXML->strTabODXML[iVarOD].usSize;
		iFlag = fread(&OD_7058Record->Latitude, usTailleEnByte, 1, G_fpData);
		iFlag = fwrite(&OD_7058Record->Latitude, usTailleEnByte, 1, strS7KRecordXML->strTabODXML[iVarOD].ptr_fpBin);
		if (iNbRecord > 0 && strS7KRecordXML->strTabODXML[iVarOD].bFlagConstant == TRUE)
		{
			bFlagCompare = util_bComparePrevAndCurrentVal(strS7KRecordXML->strTabRTHXML[iVarOD].cType,
												strS7KRecordXML->strTabODXML[iVarOD].ptr_vPrevVal,
												&OD_7058Record->Latitude,
												strS7KRecordXML->strTabODXML[iVarOD].usNbElem);
			strS7KRecordXML->strTabODXML[iVarOD].bFlagConstant = bFlagCompare && strS7KRecordXML->strTabODXML[iVarOD].bFlagConstant;
		}
		if (iNbRecord == 0)
		{
			strS7KRecordXML->strTabODXML[iVarOD].ptr_vPrevVal = (void *)malloc((RTH_7058Record->N*usTailleEnByte));
		}
		memmove(strS7KRecordXML->strTabODXML[iVarOD].ptr_vPrevVal, &OD_7058Record->Latitude, RTH_7058Record->N*usTailleEnByte);

		iVarOD++;
		usTailleEnByte = strS7KRecordXML->strTabODXML[iVarOD].usSize;
		iFlag = fread(&OD_7058Record->Longitude,usTailleEnByte, 1, G_fpData);
		iFlag = fwrite(&OD_7058Record->Longitude, usTailleEnByte, 1, strS7KRecordXML->strTabODXML[iVarOD].ptr_fpBin);
		if (iNbRecord > 0 && strS7KRecordXML->strTabODXML[iVarOD].bFlagConstant == TRUE)
		{
			bFlagCompare = util_bComparePrevAndCurrentVal(strS7KRecordXML->strTabRTHXML[iVarOD].cType,
												strS7KRecordXML->strTabODXML[iVarOD].ptr_vPrevVal,
												&OD_7058Record->Longitude,
												strS7KRecordXML->strTabODXML[iVarOD].usNbElem);
			strS7KRecordXML->strTabODXML[iVarOD].bFlagConstant = bFlagCompare && strS7KRecordXML->strTabODXML[iVarOD].bFlagConstant;
		}
		if (iNbRecord == 0)
		{
			strS7KRecordXML->strTabODXML[iVarOD].ptr_vPrevVal = (void *)malloc((RTH_7058Record->N*usTailleEnByte));
		}
		memmove(strS7KRecordXML->strTabODXML[iVarOD].ptr_vPrevVal, &OD_7058Record->Longitude, RTH_7058Record->N*usTailleEnByte);

		iVarOD++;
		usTailleEnByte = strS7KRecordXML->strTabODXML[iVarOD].usSize;
		iFlag = fread(&OD_7058Record->Heading, usTailleEnByte, 1, G_fpData);
		iFlag = fwrite(&OD_7058Record->Heading, usTailleEnByte, 1, strS7KRecordXML->strTabODXML[iVarOD].ptr_fpBin);
		if (iNbRecord > 0 && strS7KRecordXML->strTabODXML[iVarOD].bFlagConstant == TRUE)
		{
			bFlagCompare = util_bComparePrevAndCurrentVal(strS7KRecordXML->strTabRTHXML[iVarOD].cType,
												strS7KRecordXML->strTabODXML[iVarOD].ptr_vPrevVal,
												&OD_7058Record->Heading,
												strS7KRecordXML->strTabODXML[iVarOD].usNbElem);
			strS7KRecordXML->strTabODXML[iVarOD].bFlagConstant = bFlagCompare && strS7KRecordXML->strTabODXML[iVarOD].bFlagConstant;
		}
		if (iNbRecord == 0)
		{
			strS7KRecordXML->strTabODXML[iVarOD].ptr_vPrevVal = (void *)malloc((RTH_7058Record->N*usTailleEnByte));
		}
		memmove(strS7KRecordXML->strTabODXML[iVarOD].ptr_vPrevVal, &OD_7058Record->Heading, RTH_7058Record->N*usTailleEnByte);

		for (iLoop=0; iLoop< RTH_7058Record->N; iLoop++)
		{
			iVarOD = 4;
			usTailleEnByte = strS7KRecordXML->strTabODXML[iVarOD].usSize;
			iFlag = fread(OD_7058Record->BeamAlongTrackDistance, usTailleEnByte, 1, G_fpData);
			iFlag = fwrite(OD_7058Record->BeamAlongTrackDistance, usTailleEnByte, 1, strS7KRecordXML->strTabODXML[iVarOD].ptr_fpBin);
			iVarOD++;
			usTailleEnByte = strS7KRecordXML->strTabODXML[iVarOD].usSize;
			iFlag = fread(OD_7058Record->BeamAcrossTrackDistance, usTailleEnByte, 1, G_fpData);
			iFlag = fwrite(OD_7058Record->BeamAcrossTrackDistance, usTailleEnByte, 1, strS7KRecordXML->strTabODXML[iVarOD].ptr_fpBin);
			iVarOD++;
			usTailleEnByte = strS7KRecordXML->strTabODXML[iVarOD].usSize;
			iFlag = fread(OD_7058Record->CenterSampleDistance, usTailleEnByte, 1, G_fpData);
			iFlag = fwrite(OD_7058Record->CenterSampleDistance, usTailleEnByte, 1, strS7KRecordXML->strTabODXML[iVarOD].ptr_fpBin);
			iVarOD++;
		}

		free(OD_7058Record->BeamAlongTrackDistance);
		free(OD_7058Record->BeamAcrossTrackDistance);
		free(OD_7058Record->CenterSampleDistance);


	} // Fin des traitements sur paquet OD.

	// D�sallocation (obligatoire !) des matrices r�serv�es.
	free(RD_7058Record);
	free(RTH_7058Record);


	iNbRecord++;


	return iRetour;

} //iProcess7058Record


//----------------------------------------------------------------
// Fonction :	iInitXMLBin7058Record
// Objet 	:	Initialisation de la structure �quivalent au paquet
//				7058 pour la manipulation des fichiers bin et XML.
// Modification : 15/10/08
// Auteur 	: GLU
//----------------------------------------------------------------
int iInitXMLBin7058Record(	char *cTitleDatagram,
							char *cNomDatagram,
							char *cComments,
							u32 uiODOffset,
							T_S7KRecordXML *strS7KRecordXML)
{

	int iRet = 0;

	T_DESC_SIGNAL_OR_IMAGE strDescriptionSignal[NB_SIGNAL] = {
			{"SonarId", 				"u64", 	1.0, 0.0},
			{"PingNumber", 				"u32", 	1.0, 0.0},
			{"MultiPingSequence", 		"u16", 	1.0, 0.0},
			{"N", 						"u16", 	1.0, 0.0},
			{"ErrorFlag", 				"u8", 	1.0, 0.0},
			{"ControlFlags", 			"u32", 	1.0, 0.0},
			{"Reserved", 				"f32", 	1.0, 0.0}};

	// D�finition des paquets RD.
	T_DESC_SIGNAL_OR_IMAGE strDescriptionImage[NB_IMAGE] = {
			{"BeamDescriptor",			"u16",	1.0,	0.0},
			{"BeginSampleDescriptor",	"u32",	1.0,	0.0},
			{"BottomDetectSample",		"u32",	1.0,	0.0},
			{"EndSampleDescriptor",		"u32",	1.0,	0.0},
			{"Samples",					"f32",	1.0,	0.0},
			{"StarboardBeamNumber",		"u16",	1.0,	0.0}};

	T_DESC_SIGNAL_OR_IMAGE strDescriptionImage2[NB_IMAGE2] = {
			{"Frequency",				"f32",	1.0,	0.0},
			{"Latitude",				"f64",	RAD2DEG,0.0},
			{"Longitude",				"f64",	RAD2DEG,0.0},
			{"Heading",					"f32",	RAD2DEG,0.0},
			{"BeamAlongTrackDistance",	"f32",	1.0,	0.0},
			{"BeamAcrossTrackDistance",	"f32",	1.0,	0.0},
			{"CenterSampleDistance",	"f32",	1.0,	0.0}};


	T_DESC_TAB_SIGANDIMG strDescriptionTab = {	NB_SIGNAL,
												NB_IMAGE,
												NB_IMAGE2,
												NUM_INDEX_TABLE,
												strDescriptionSignal,
												strDescriptionImage,
												strDescriptionImage2};


	// Lancement de l'initialisation des structures de signaux : ouvertures des
	// fichiers, copie des structures, ...
	iRet = utilS7K_initXMLRecord(	strDescriptionTab,
									cTitleDatagram,
									cNomDatagram,
									cComments,
									strS7KRecordXML);

	// Cas particuliers de quelques champs pour leur taille.
	strS7KRecordXML->strTabRTHXML[6].usNbElem = 8; // Reserved

	return EXIT_SUCCESS;

} // iInitXMLBin7058Record

#pragma pack()
