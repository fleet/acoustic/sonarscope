/*
 * S7K_process7058Record.h
 *
 *  Created on: 13 oct. 2008
 *      Author: rgallou
 */

#ifndef S7K_PROCESS7058RECORD_H_
#define S7K_PROCESS7058RECORD_H_
#pragma pack(1)

#include "S7K_DATAGRAMS.H"

typedef struct {
	u64		SonarId;
	u32		PingNumber;
	u16		MultiPingSequence;
	u16		N;
	u8		ErrorFlag;
	u32		ControlFlags;
	u32		Reserved[7];
} T_7058_RECORD_RTH;

// Paquet 7058 : partie RD
// Traitement particulier du � la lecture des Samples qui d�pendent
// des valeurs Begin et End.
typedef struct {
	u16		BeamDescriptor;
	u32		BeginSampleDescriptor;
	u32		BottomDetectSample;
	u32		EndSampleDescriptor;
	f32		*Samples;
} T_7058_RECORD_RD;


// Paquet 7058 : partie OD
typedef struct {
	f32	Frequency;
	f64	Latitude;
	f64	Longitude;
	f32	Heading;
	f32	*BeamAlongTrackDistance;
	f32	*BeamAcrossTrackDistance;
	u32	*CenterSampleDistance;
} T_7058_RECORD_OD;


int iProcess7058Record(FILE *G_fpData, int iNbRecord,
						u32 uiODOffset,
						T_S7KRecordXML *strS7KRecordXML,
						T_7058_RECORD_RTH *RTH_prev7058Record);

int iInitXMLBin7058Record(	char *cTitleDatagram,
							char *cNomDatagram, char *cComments,
							u32 uiODOffset,
							T_S7KRecordXML *strS7KRecordXML);
#pragma pack()
#endif /* S7K_PROCESS7058RECORD_H_ */
