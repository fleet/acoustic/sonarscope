/*
 * S7K_process7200Record.c
 *
 *  Created on: 13 oct. 2008
 *      Author: rgallou
 */
#pragma pack(1)

#include <sys/types.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "S7K_Datagrams.h"
#include "S7K_Utilities.h"
#include "S7K_process7200Record.h"
#include "convertFiles_S7K.h"

#define NB_SIGNAL 			10
#define NB_IMAGE 			2
#define NB_IMAGE2 			0
#define NUM_INDEX_TABLE		5	// Identification de l'index Signal des Images (� partir de 0).
#define NUM_INDEX_TABLE2	0	// Identification de l'index Signal des Images (� partir de 0).

//----------------------------------------------------------------
// Fonction :	iProcess7200Record
// Objet 	:	Traitement du paquet 7200
// Modif.	: 	03/11/08
// Auteur 	:	GLU
//----------------------------------------------------------------
int iProcess7200Record(FILE *G_fpData, int iNbRecord,
						u32 uiODOffset,
						T_S7KRecordXML *strS7KRecordXML,
						T_7200_RECORD_RTH *RTH_prev7200Record)
{
    T_7200_RECORD_RTH 	*RTH_7200Record;


	// Pointeur pour avancer sur les variables automatiquement.
	unsigned char		*ptr_cRTHRecord;
	static void 		*ptr_vVarPrevRecord;
	void				*ptr_vVarRecord,
						*ptr_vBidon;

	BOOL		bFlagVarConstant = TRUE,
				bFlagCompare = TRUE;

    int 	iFlag,
			iVarRD,
			iVarRTH,
			iLoop,
			iRetour = 0;

	

    unsigned short	usTailleEnByte,
					usNbElem;

	// Conservation de l'information de pr�sence de donn�es Optionnal Data.
    strS7KRecordXML->uiODOffset = 0;

    // Initialisation de la structure d'identification du paquet
 	if (iNbRecord == 0)
	{
 		iInitXMLBin7200Record("File Header", "7200", "7k File Header", uiODOffset, strS7KRecordXML);
	}

    // Lecture du RTH du paquet
	RTH_7200Record = (T_7200_RECORD_RTH *)malloc(sizeof(T_7200_RECORD_RTH));
	iFlag = fread(RTH_7200Record, sizeof(T_7200_RECORD_RTH), 1, G_fpData);
	if (iFlag == 0)
	{
		printf("Error in reading : %s\n", G_cFileData);
		return FALSE;
	}
	//ptr_vVarRecord = (void*)malloc(sizeof(T_7200_RECORD_RTH));
	//ptr_cRTHRecord = (unsigned char*)malloc(sizeof(T_7200_RECORD_RTH));
	ptr_cRTHRecord = (unsigned char*)RTH_7200Record;
	// Sauvegarde des donn�es des attributs.
	if (iNbRecord > 0)
	{
		ptr_vVarPrevRecord = (void *)RTH_prev7200Record;
	}
	for (iVarRTH=0; iVarRTH<(int)strS7KRecordXML->sNbVarRTH; iVarRTH++)
	{
		ptr_vVarRecord = (void *)ptr_cRTHRecord;
		if (iVarRTH==0)
		{
			usTailleEnByte = 0;
			usNbElem = 0;
		}
		else
		{
			usTailleEnByte = util_usNbOctetTypeVar(strS7KRecordXML->strTabRTHXML[iVarRTH-1].cType);
			usNbElem = strS7KRecordXML->strTabRTHXML[iVarRTH-1].usNbElem;
		}

		if (iNbRecord > 0)
		{
			bFlagVarConstant = strS7KRecordXML->strTabRTHXML[iVarRTH].bFlagConstant;
			ptr_vVarPrevRecord = ptr_vVarPrevRecord + usTailleEnByte*usNbElem;
			bFlagCompare = util_bComparePrevAndCurrentVal(strS7KRecordXML->strTabRTHXML[iVarRTH].cType,
												ptr_vVarPrevRecord,
												ptr_vVarRecord,
											   strS7KRecordXML->strTabRTHXML[iVarRTH].usNbElem);
			bFlagVarConstant = bFlagVarConstant && bFlagCompare;
			strS7KRecordXML->strTabRTHXML[iVarRTH].bFlagConstant = bFlagVarConstant;
		}
		// Ecriture sp�ciale car contenant du XML pour la variable Device Info
		// Pr�paration de l'�criture de la variable.
		usTailleEnByte = strS7KRecordXML->strTabRTHXML[iVarRTH].usSize;
		usNbElem =  strS7KRecordXML->strTabRTHXML[iVarRTH].usNbElem;
		fwrite(ptr_cRTHRecord, usTailleEnByte, usNbElem, strS7KRecordXML->strTabRTHXML[iVarRTH].ptr_fpBin);

		strS7KRecordXML->strTabRTHXML[iVarRTH].ptr_vPrevVal = ptr_cRTHRecord;
		ptr_cRTHRecord += usTailleEnByte*usNbElem;
	}
	// Recopie en zone tampon des valeurs courantes.
	memmove(RTH_prev7200Record, RTH_7200Record, sizeof(T_7200_RECORD_RTH));

	// --------------------------------------------
 	// Traitement des paquets RD
 	// Lecture des datas les uns apr�s les autres

	for (iLoop=0; iLoop< RTH_7200Record->N; iLoop++)
	{
		iVarRD = 0;

		// Boucle sur les variables.
		for (iVarRD=0; iVarRD< strS7KRecordXML->sNbVarRD; iVarRD++)
		{
			usTailleEnByte = strS7KRecordXML->strTabRDXML[iVarRD].usSize;
			usNbElem = strS7KRecordXML->strTabRDXML[iVarRD].usNbElem;
			ptr_vBidon = (void *)malloc((usTailleEnByte*usNbElem)*sizeof(char));
			iFlag = fread(ptr_vBidon, usTailleEnByte, usNbElem, G_fpData);
			iFlag = fwrite(ptr_vBidon, usTailleEnByte, usNbElem, strS7KRecordXML->strTabRDXML[iVarRD].ptr_fpBin);
			if (iNbRecord > 0 && strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant == TRUE)
			{
				bFlagCompare = util_bComparePrevAndCurrentVal(strS7KRecordXML->strTabRDXML[iVarRD].cType,
													strS7KRecordXML->strTabRDXML[iVarRD].ptr_vPrevVal,
													ptr_vBidon,
													usNbElem);
				strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant = bFlagCompare && strS7KRecordXML->strTabRDXML[iVarRD].bFlagConstant;
			}
			if (iNbRecord == 0)
			{
				strS7KRecordXML->strTabRDXML[iVarRD].ptr_vPrevVal = malloc(usTailleEnByte*usNbElem*sizeof(char));
			}
			memmove(strS7KRecordXML->strTabRDXML[iVarRD].ptr_vPrevVal, ptr_vBidon, usTailleEnByte*usNbElem);

			free(ptr_vBidon);
		} // Fin de la boucles sur les variables.
	}

	free(RTH_7200Record);
	iNbRecord++;

	return iRetour;

} //iProcess7200Record


//----------------------------------------------------------------
// Fonction :	iInitXMLBin7200Record
// Objet 	:	Initialisation de la structure �quivalent au paquet
//				7200 pour la manipulation des fichiers bin et XML.
// Modification : 15/10/08
// Auteur 	: GLU
//----------------------------------------------------------------
int iInitXMLBin7200Record(	char *cTitleDatagram,
							char *cNomDatagram, char *cComments,
							u32 uiODOffset,
							T_S7KRecordXML *strS7KRecordXML)
{

	int iRet = 0;

	T_DESC_SIGNAL_OR_IMAGE strDescriptionSignal[NB_SIGNAL] = {
			{"FileIdentifier", 		"u8", 				1.0, 0.0},
			{"VersionNumber", 		"u16", 				1.0, 0.0},
			{"Reserved", 			"u16", 				1.0, 0.0},
			{"SessionIdentifier", 	"u8",	 			1.0, 0.0},
			{"RecordDataSize", 		"u32", 				1.0, 0.0},
			{"N", 					"u32", 				1.0, 0.0},
			{"RecordingName", 		"char", 			1.0, 0.0},
			{"RecordingProgramVersionNumber", "char", 	1.0, 0.0},
			{"UserDefinedName", 	"char", 			1.0, 0.0},
			{"Notes", 				"char", 			1.0, 0.0}};


	// D�finition des paquets RD.
	T_DESC_SIGNAL_OR_IMAGE strDescriptionImage[NB_IMAGE] = {
			{"DeviceIdentifier",		"u32",	1.0,	0.0},
			{"SystemEnumerator",		"u16",	1.0,	0.0}};

	// D�finition des paquets OD.
	T_DESC_SIGNAL_OR_IMAGE strDescriptionImage2[NB_IMAGE2] = {};


	T_DESC_TAB_SIGANDIMG strDescriptionTab = {	NB_SIGNAL,
												NB_IMAGE,
												NB_IMAGE2,
												NUM_INDEX_TABLE,
												strDescriptionSignal,
												strDescriptionImage,
												strDescriptionImage2};


	// Lancement de l'initialisation des structures de signaux : ouvertures des
	// fichiers, copie des structures, ...
	iRet = utilS7K_initXMLRecord(	strDescriptionTab,
									cTitleDatagram,
									cNomDatagram,
									cComments,
									strS7KRecordXML);

	// Cas particuliers de quelques champs pour leur taille.
	strS7KRecordXML->strTabRTHXML[0].usNbElem = 16; 	// FileIdentifier
	strS7KRecordXML->strTabRTHXML[3].usNbElem = 16; 	// SessionIdentifier
	strS7KRecordXML->strTabRTHXML[6].usNbElem = 64; 	// RecordingName
	strS7KRecordXML->strTabRTHXML[7].usNbElem = 16; 	// RecordingProgramVersionName
	strS7KRecordXML->strTabRTHXML[8].usNbElem = 64; 	// UserDefinedName
	strS7KRecordXML->strTabRTHXML[9].usNbElem = 128; 	// Notes



	return EXIT_SUCCESS;

} // iInitXMLBin7200Record

#pragma pack()
