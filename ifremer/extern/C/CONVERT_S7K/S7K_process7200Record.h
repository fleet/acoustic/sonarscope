/*
 * S7K_process7200Record.h
 *
 *  Created on: 13 oct. 2008
 *      Author: rgallou
 */

#ifndef S7K_PROCESS7200RECORD_H_
#define S7K_PROCESS7200RECORD_H_
#pragma pack(1)

#include "S7K_DATAGRAMS.H"

//-Paquet 7200 : partie RTH------------------------
typedef struct {
	u8		FileIdentifier[16];
	u16		VersionNumber;
	u16		Reserved;
	u8		SessionIdentifier[16];
	u32		RecordDataSize;
	u32		N;
	char	RecordingName[64];
	char	RecordingProgramVersionNumber[16];
	char	UserDefinedName[64];
	char	Notes[128];
} T_7200_RECORD_RTH;

// Paquet 7200 : partie RD
typedef struct {
	u32	*DeviceIdentifier;
	u16	*SystemEnumerator;
} T_7200_RECORD_RD;


int iProcess7200Record(FILE *G_fpData, int iNbRecord,
						u32 uiODOffset,
						T_S7KRecordXML *strS7KRecordXML,
						T_7200_RECORD_RTH *RTH_prev7200Record);

int iInitXMLBin7200Record(	char *cTitleDatagram,
							char *cNomDatagram, char *cComments,
							u32 uiODOffset,
							T_S7KRecordXML *strS7KRecordXML);
#pragma pack()
#endif /* S7K_PROCESS7200RECORD_H_ */
