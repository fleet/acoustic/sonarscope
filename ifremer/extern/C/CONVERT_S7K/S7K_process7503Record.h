/*
 * S7K_process7503Record.h
 *
 *  Created on: 13 oct. 2008
 *      Author: rgallou
 */

#ifndef S7K_PROCESS7503RECORD_H_
#define S7K_PROCESS7503RECORD_H_
#pragma pack(1)

#include "S7K_DATAGRAMS.H"

typedef struct {
	u64		SonarId;
	u32		PingNumber;
	u16		MultiPingSequence;
	f32		Frequency;
	f32		SampleRate;
	f32		ReceiverBandwidth;
	f32		TxPulseWidth;
	u32		TxPulseTypeIdentifier;
	u32		TxPulseEnvelopeIdentifier;
	f32		TxPulseEnvelopeParameter;
	u32		TxPulseReserved;
	f32		MaxPingRate;
	f32		PingPeriod;
	f32		RangeSelection;
	f32		PowerSelection;
	f32		GainSelection;
	u32		ControlFlags;
	u32		ProjectorIdentifier;
	f32		ProjectorBeamSteeringAngleVertical;
	f32		ProjectorBeamSteeringAngleHorizontal;
	f32		ProjectorBeam_3dBBeamWidthVertical;
	f32		ProjectorBeam_3dBBeamWidthHorizontal;
	f32		ProjectorBeamFocalPoint;
	u32		ProjectorBeamWeightingWindowType;
	f32		ProjectorBeamWeightingWindowParameter;
	u32		TransmitFlags;
	u32		HydrophoneIdentifier;
	u32		ReceiveBeamWeightingWindow;
	f32		ReceiveBeamWeightingParameter;
	u32		ReceiveFlags;
	f32		ReceiveBeamWidth;
	f32		BottomDetectionFilterInfoMinRange;
	f32		BottomDetectionFilterInfoMaxrange;
	f32		BottomDetectionFilterInfoMinDepth;
	f32		BottomDetectionFilterInfoMaxDepth;
	f32		Absorption;
	f32		SoundVelocity;
	f32		Spreading;
	u16		Reserved;
	f32		TxArrayPositionOffsetX;
	f32		TxArrayPositionOffsetY;
	f32		TxArrayPositionOffsetZ;
	f32		HeadTiltX;
	f32		HeadTiltY;
	f32		HeadTiltZ;
	u32		PingOnOff;
} T_7503_RECORD_RTH;


int iProcess7503Record(FILE *G_fpData, int iNbRecord,
						u32 uiODOffset,
						T_S7KRecordXML *strS7KRecordXML,
						T_7503_RECORD_RTH *RTH_prev7503Record);

int iInitXMLBin7503Record(	char *cTitleDatagram,
							char *cNomDatagram, char *cComments,
							u32 uiODOffset,
							T_S7KRecordXML *strS7KRecordXML);
#pragma pack()
#endif /* S7K_PROCESS7503RECORD_H_ */
