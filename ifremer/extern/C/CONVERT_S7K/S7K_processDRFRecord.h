/*
 * S7K_processDRFRecord.h
 *
 *  Created on: 13 oct. 2008
 *      Author: rgallou
 */

#ifndef S7K_PROCESSDRFRECORD_H_
#define S7K_PROCESSDRFRECORD_H_
#pragma pack(1)

#include "S7K_DATAGRAMS.H"

// Description des formats de paquets d'un fichier S7K.
// ----------------------------------------------------
// Paquet DRF (partiel, sans la DATA Section ni checksum).
typedef struct {
	u16	ProtocolVersion;
	u16	Offset;
	u32	SyncPattern;
	u32	Size;
	u32	OptionalDataOffset;
	u32	OptionalDataIdentifier;
	u16	Year;		// Constitue jusqu'a Minutes la donn�e 7KTime.
	u16	Day;
	f32	Seconds;
	u8	Hours;
	u8	Minutes;
	u16	Reserved1;
	u32	RecordTypeIdentifier;
	u32	DeviceIdentifier;
	u16	Reserved2;
	u16	SystemEnumerator;
	u32	Reserved3;
	u16	Flags;
	u16	Reserved4;
	u32	Reserved5;
	u32	TotalRecordsInFragmentedDataRecordSet;
	u32	FragmentNumber;
} T_DRF_RECORD;


int iProcessDRFRecord(FILE *G_fpData, int iNbRecord,
						T_S7KRecordXML *strS7KRecordXML,
						T_DRF_RECORD *RTH_prevDRFRecord);

int iInitXMLBinDRFRecord(	char *cTitleDatagram,
							char *cNomDatagram, char *cComments,
							T_S7KRecordXML *strS7KRecordXML);
#pragma pack()
#endif /* S7K_PROCESSDRFRECORD_H_ */
