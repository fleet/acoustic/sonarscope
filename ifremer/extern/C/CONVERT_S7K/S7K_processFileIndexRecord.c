/*
 * S7K_processFileIndexRecord.c
 *
 *  Created on: 13 oct. 2008
 *      Author: rgallou
 */
#pragma pack(1)

#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "S7K_Datagrams.h"
#include "S7K_Utilities.h"
#include "Generic_Utilities.h"
#include "S7K_processFileIndexRecord.h"
#include "convertFiles_S7K.h"


//----------------------------------------------------------------
// Fonction :	iProcessFileIndexRecord
// Objet 	:	Traitement du paquet FileIndex
// Modif.	: 	03/11/08
// Auteur 	:	GLU
//----------------------------------------------------------------
int iProcessFileIndexRecord(FILE *fpFileData,
						int iNbRecord,
						T_S7KRecordXML *strS7KRecordXML,
						T_FILEINDEX_RECORD *ptrSignal_prevRecord)
{
	T_FILEINDEX_RECORD	*ptrSignal_Record;

	// Pointeur pour avancer sur les variables automatiquement.
	unsigned char		*ptr_cSignalRecord;
	static void 		*ptr_vVarPrevRecord;
	void				*ptr_vVarRecord;

	BOOL		bFlagVarConstant = TRUE,
				bFlagCompare = TRUE;

    int 	iFlag,
			iVarRTH,
			iRetour = 0;

    // On r�initialise errno
    errno = 0;


    unsigned short	usTailleEnByte,
					usNbElem;

	if (iNbRecord == 0)
	{
		iInitXMLBinFileIndexRecord(	"FileIndex",
									"FileIndex",
									"Index File of common data",
									strS7KRecordXML);
	}
	// Ecriture de la donn�e Date.
	ptr_cSignalRecord = (unsigned char *)&strS7KRecordXML;
	for (iVarRTH=0; iVarRTH<(int)strS7KRecordXML->sNbVarRTH; iVarRTH++)
	{
		// Pr�paration de l'�criture de la variable.
		usTailleEnByte = strS7KRecordXML->strTabRTHXML[iVarRTH].usSize;
		usNbElem =  strS7KRecordXML->strTabRTHXML[iVarRTH].usNbElem;
		fwrite(ptr_cSignalRecord, usTailleEnByte, usNbElem, strS7KRecordXML->strTabRTHXML[iVarRTH].ptr_fpBin);
		ptr_cSignalRecord += usTailleEnByte*usNbElem;
	}
	iNbRecord++;

	return iRetour;

} //iProcessFileIndexRecord


//----------------------------------------------------------------
// Fonction :	iInitXMLBinFileIndexRecord
// Objet 	:	Initialisation de la structure �quivalent au paquet
//				FileIndex pour la manipulation des fichiers bin et XML.
// Modification : 15/10/08
// Auteur 	: GLU
//----------------------------------------------------------------
int iInitXMLBinFileIndexRecord(	char *cTitleDatagram,
								char *cNomDatagram,
								char *cComments,
								T_S7KRecordXML *strS7KRecordXML)
{
	typedef struct  {
			unsigned int	Date;
			unsigned short	Heure;
			off64_t 		DatagramPosition;
			int 			NbOfBytesInDatagram;
			int 			TypeOfDatagram;
			int				PingCounter;
			unsigned short	PingSequence;
			unsigned int	DeviceIdentifier;
	} T_FILEINDEX_RECORD;

	char cTabNomVarSignal[8][30] = {
			"Date",
			"Heure",
			"Position",
			"NbOfBytesInDatagram",
			"TypeOfDatagram",
			"PingCounter",
			"PingSequence",
			"DeviceIdentifier"};

	char cTabNomTypeSignal[8][20] = {
			"unsigned int",
			"unsigned short",
			"u64",
			"u32",
			"u32",
			"u32",
			"u16",
			"u32"};

	// ScaleFactor
	float 	fTabSFSignal[8] = {
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0};

	// AddOffset
	float 	fTabAOSignal[8] = {
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0};

	char	*ptr_cNomHeader,
			*ptr_cRepertoire,
			cNomFicPathBin[300];

    int 	iSizeRep,
			iRet,
			iErr,
			iLen,
			iVarRTH;

	unsigned short usTaille;

	// Initialisation des structures de pilotage des fichiers
	// XML et Bin .
	strS7KRecordXML->sNbVarRTH = 8;
	strS7KRecordXML->sNbVarRD = 0;
	strS7KRecordXML->strTabRTHXML = (T_S7KVarXML *)malloc(strS7KRecordXML->sNbVarRTH*sizeof(T_S7KVarXML));

	iLen = strlen(cNomDatagram)+1;
	strS7KRecordXML->ptr_cNomHeader = malloc(iLen*sizeof(char));
	sprintf(strS7KRecordXML->ptr_cNomHeader, "%s", cNomDatagram);

	iLen = strlen(cTitleDatagram)+1;
	strS7KRecordXML->ptr_cLabelHeader = malloc(iLen*sizeof(char));
	sprintf(strS7KRecordXML->ptr_cLabelHeader, "%s", cTitleDatagram);

	iLen = strlen(cComments)+1;
	strS7KRecordXML->ptr_cCommentsHeader = malloc(iLen*sizeof(char));
	sprintf(strS7KRecordXML->ptr_cCommentsHeader, "%s", cComments);


	// Affectation et cr�ation du repertoire du Datagramme.
	ptr_cNomHeader = strS7KRecordXML->ptr_cNomHeader;
	iSizeRep = ( strlen(G_cRepData) + strlen( cNomDatagram));
	// Ajout d'une marge au cas o� certains caract�res soient doubl�s soient doubl�s.
	ptr_cRepertoire = malloc( (iSizeRep + 1 + 50)*sizeof(char));
	sprintf(ptr_cRepertoire, "%s%s\\S7K_%s", G_cRepData, "_tmp", ptr_cNomHeader);
	if (DEBUG)
	   printf("-- Directory Creation : %s\n", ptr_cRepertoire);

	// Cr�ation du r�pertoire
	iErr = mkdir(ptr_cRepertoire);
	if (iErr != 0)
	{
	   switch (errno)
	   {
		   //"File exists"
		   case EEXIST:
			   break;
		   default:
			   printf("Error in Directory creation %s\n", strerror(errno));
			   return EXIT_FAILURE;
			   break;
	   }
	}

	// Initialisation g�n�rale des variables de type Signal
	for (iVarRTH=0; iVarRTH<strS7KRecordXML->sNbVarRTH; iVarRTH++) {
	   strS7KRecordXML->strTabRTHXML[iVarRTH].ptr_fpBin = 0;
	   // Attention, for�age diff�rent des autres paquets. Les donn�es du FileIndex
	   // ne sont de nature pas constantes.
	   strS7KRecordXML->strTabRTHXML[iVarRTH].bFlagConstant = FALSE;
	   strS7KRecordXML->strTabRTHXML[iVarRTH].usNbElem = 1;
	   sprintf(strS7KRecordXML->strTabRTHXML[iVarRTH].cNom, "%s", cTabNomVarSignal[iVarRTH]);
	   sprintf(strS7KRecordXML->strTabRTHXML[iVarRTH].cType, "%s", cTabNomTypeSignal[iVarRTH]);
	   sprintf(strS7KRecordXML->strTabRTHXML[iVarRTH].cTag, "%s", "TODO");
	   sprintf(strS7KRecordXML->strTabRTHXML[iVarRTH].cUnit, "%s", "TODO");
	   strS7KRecordXML->strTabRTHXML[iVarRTH].fScaleFactor = fTabSFSignal[iVarRTH];
	   strS7KRecordXML->strTabRTHXML[iVarRTH].fAddOffset = fTabAOSignal[iVarRTH];
	   usTaille = util_usNbOctetTypeVar(cTabNomTypeSignal[iVarRTH]);
	   strS7KRecordXML->strTabRTHXML[iVarRTH].usSize = usTaille;
	   iLen = strlen(strS7KRecordXML->strTabRTHXML[iVarRTH].cNom);
	   // Ajout d'une marge au cas o� certains caract�res soient doubl�s soient doubl�s.
	   sprintf(cNomFicPathBin, "%s\\%s%s", ptr_cRepertoire, strS7KRecordXML->strTabRTHXML[iVarRTH].cNom, ".bin");
	   sprintf(strS7KRecordXML->strTabRTHXML[iVarRTH].cNomPathFileBin, "%s", cNomFicPathBin);
	   strS7KRecordXML->strTabRTHXML[iVarRTH].ptr_fpBin = fopen(cNomFicPathBin, "w+b");
	   if (!strS7KRecordXML->strTabRTHXML[iVarRTH].ptr_fpBin)
	   {
		   printf("%s -- Error in Signal creation  : %s\n", __FILE__, cNomFicPathBin);
		   return EXIT_FAILURE;
	   }
	}


	free(ptr_cRepertoire);
	return EXIT_SUCCESS;

} // iInitXMLBinFileIndexRecord

#pragma pack()
