/*
 * S7K_processFileIndexRecord.h
 *
 *  Created on: 21 nov. 2008
 *      Author: rgallou
 */
#pragma pack(1)

#ifndef S7K_PROCESSFILEINDEXRECORD_H_
#define S7K_PROCESSFILEINDEXRECORD_H_

#include "S7K_Datagrams.h"
#include <sys/types.h>


typedef struct  {
		unsigned int	Date;
		unsigned short	Heure;
		off64_t 		DatagramPosition;
		int 			NbOfBytesInDatagram;
		int 			TypeOfDatagram;
		int				PingCounter;
		unsigned short	PingSequence;
		unsigned int	DeviceIdentifier;
} T_FILEINDEX_RECORD;


int iInitXMLBinFileIndexRecord(	char *cTitleDatagram,
								char *cNomDatagram,
								char *cComments,
								T_S7KRecordXML *strS7KRecordXML);

int iProcessFileIndexRecord(FILE *fpFileData,
						int iNbRecord,
						T_S7KRecordXML *strS7KRecordXML,
						T_FILEINDEX_RECORD *ptrSignal_prevRecord);


#endif /* S7K_PROCESSFILEINDEXRECORD_H_ */

#pragma pack()
