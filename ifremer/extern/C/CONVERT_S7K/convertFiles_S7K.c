// ----------------------------------------------------------------------
// Programme :
//	CONVERT_S7K.C
//
// Objet :
//	Convertir un fichier au format S7K pour d�composer en fichiers
//	XML et fichiers binaires. Les fichiers XML decrivent le contenu des
// 	binaires.
// 	Chaque fichier binaire comporte une donn�e �l�mentaire des paquets par ping
// 	issue des paquets de donn�es du S7K.
//	Il a �t� r�alise sous l'IDE Eclipse 3.4.1 avec compilateur gcc.
//
// Cr�ation :
//	02/10/2008
//
// Auteur :
//  GLU : (ATL) roger.gallou@altran.com
//		  02 98 05 43 21
//
// Historique des modifications :
//
// ----------------------------------------------------------------------
#pragma pack(1) // Pour la compilation sous Visual Studio
				// et l'alignement sur 8 octets.

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <conio.h>
#include <io.h>
#include <string.h>
#include <errno.h>
#include <libgen.h>

#include "S7K_Datagrams.h"
#include "convertFiles_S7K.h"
#include "S7K_processDRFRecord.h"
#include "S7K_process1000Record.h"
#include "S7K_process1001Record.h"
#include "S7K_process1002Record.h"
#include "S7K_process1003Record.h"
//TODO #include "S7K_process1004Record.h"
#include "S7K_process1005Record.h"
#include "S7K_process1006Record.h"
#include "S7K_process1008Record.h"
#include "S7K_process1009Record.h"
#include "S7K_process1010Record.h"
#include "S7K_process1011Record.h"
#include "S7K_process1012Record.h"
#include "S7K_process1013Record.h"
#include "S7K_process1014Record.h"
#include "S7K_process1015Record.h"
#include "S7K_process1016Record.h"
#include "S7K_process1200Record.h"
#include "S7K_process2000Record.h"
#include "S7K_process7000Record.h"
#include "S7K_process7001Record.h"
#include "S7K_process7002Record.h"
#include "S7K_process7004Record.h"
#include "S7K_process7006Record.h"
#include "S7K_process7007Record.h"
#include "S7K_process7008Record.h"
#include "S7K_process7009Record.h"
#include "S7K_process7010Record.h"
#include "S7K_process7012Record.h"
#include "S7K_process7018Record.h"
#include "S7K_process7021Record.h"
#include "S7K_process7027Record.h"
#include "S7K_process7028Record.h"
#include "S7K_process7030Record.h"
#include "S7K_process7041Record.h"
#include "S7K_process7048Record.h"
#include "S7K_process7057Record.h"
#include "S7K_process7058Record.h"
#include "S7K_process7200Record.h"
#include "S7K_process7503Record.h"

#include "S7K_processFileIndexRecord.h"

#include "S7K_Utilities.h"
#include "Generic_Utilities.h"    // Contenu dans la librairie libCONVERT_utils.a

#ifdef GUI
#include "ProgressInfo.h"
#endif


//----------------------------------------------------------------
// Fonction de d�claration de la fonction de conversion.
// Modification : 02/10/08
// Auteur 		: GLU
// Param�tres :
// - cFileName : r�pertoire et nom du fichier de donn�es
// - barre de progression optionnelle dans l'appel de fonction.

//----------------------------------------------------------------
int iConvertFiles_S7K(const char *cFileName, void(*progress)(int))
{
	char	*ptrResult = NULL;

	int 	iRet = 0,
			iDummy,
			iErr,
			iNbDatagramNotTranslate = 0,
			iFlagFindTuple = FALSE,
			iLoop,
			iNbRecord = 0,
			iDate,
			iHeure,
			iFlagDeviceIdentify = 0;

	unsigned int 	uiPingCounter = 0;

	unsigned short usSequence = 0 ;


	// Adressage d'un fichier potentiellement sup. � 2 Go.
	off64_t 	llPosFic,
				llNbOcFic;

	// Description des tuples rencontr�es non traduits.
	T_TYPEORECORDNOTTRANSLATED tabStrRecordNotTranslate[30];

	T_S7KRecordXML 		str1000RecordXML, str1001RecordXML, str1002RecordXML,
						str1003RecordXML,
						//str1004RecordXML,
						str1005RecordXML, str1006RecordXML, str1008RecordXML,
						str1009RecordXML, str1010RecordXML, str1011RecordXML,
						str1012RecordXML, str1013RecordXML, str1014RecordXML,
						str1015RecordXML, str1016RecordXML, str1200RecordXML,
						str2000RecordXML, str7000RecordXML, str7001RecordXML,
						str7002RecordXML, str7004RecordXML, str7006RecordXML,
						str7007RecordXML, str7008RecordXML, str7009RecordXML,
						str7010RecordXML, str7012RecordXML, str7018RecordXML,
						str7021RecordXML, str7027RecordXML, str7028RecordXML,
						str7030RecordXML, str7041RecordXML,
						str7048RecordXML, str7057RecordXML, str7058RecordXML,
						str7200RecordXML, str7503RecordXML, strDRFRecordXML,  strFileIndexRecordXML;

    u32					uiRecordTypeIdentifier;

	// D�clarations des paquets courants lus.
	T_DRF_RECORD 		DRF_Record;
    T_1000_RECORD_RTH 	RTH_1000Record;
    T_1001_RECORD_RTH 	RTH_1001Record;
    T_1002_RECORD_RTH 	RTH_1002Record;
    T_1003_RECORD_RTH 	RTH_1003Record;
//TODO    T_1004_RECORD_RTH 	RTH_1004Record;
    T_1005_RECORD_RTH 	RTH_1005Record;
    T_1006_RECORD_RTH 	RTH_1006Record;
    T_1008_RECORD_RTH 	RTH_1008Record;
    T_1009_RECORD_RTH 	RTH_1009Record;
    T_1010_RECORD_RTH 	RTH_1010Record;
    T_1011_RECORD_RTH 	RTH_1011Record;
    T_1012_RECORD_RTH 	RTH_1012Record;
    T_1013_RECORD_RTH 	RTH_1013Record;
    T_1014_RECORD_RTH 	RTH_1014Record;
    T_1015_RECORD_RTH 	RTH_1015Record;
    T_1016_RECORD_RTH 	RTH_1016Record;
    T_1200_RECORD_RTH 	RTH_1200Record;
    T_2000_RECORD_RTH 	RTH_2000Record;
    T_7000_RECORD_RTH 	RTH_7000Record;
    T_7001_RECORD_RTH 	RTH_7001Record;
    T_7002_RECORD_RTH 	RTH_7002Record;
    T_7004_RECORD_RTH 	RTH_7004Record;
    T_7006_RECORD_RTH 	RTH_7006Record;
    T_7007_RECORD_RTH 	RTH_7007Record;
    T_7008_RECORD_RTH 	RTH_7008Record;
    T_7009_RECORD_RTH 	RTH_7009Record;
    T_7010_RECORD_RTH 	RTH_7010Record;
    T_7012_RECORD_RTH 	RTH_7012Record;
    T_7018_RECORD_RTH 	RTH_7018Record;
    T_7021_RECORD_RTH 	RTH_7021Record;
    T_7027_RECORD_RTH 	RTH_7027Record;
    T_7028_RECORD_RTH 	RTH_7028Record;
    T_7030_RECORD_RTH 	RTH_7030Record;
    T_7041_RECORD_RTH 	RTH_7041Record;
    T_7048_RECORD_RTH 	RTH_7048Record;
    T_7057_RECORD_RTH 	RTH_7057Record;
    T_7058_RECORD_RTH 	RTH_7058Record;
    T_7200_RECORD_RTH 	RTH_7200Record;
    T_7503_RECORD_RTH 	RTH_7503Record;
	T_FILEINDEX_RECORD	RTH_FileIndexRecord;


    // Initialisations
    str1000RecordXML.iNbDatagram = 0;
    str1001RecordXML.iNbDatagram = 0;
    str1002RecordXML.iNbDatagram = 0;
    str1003RecordXML.iNbDatagram = 0;
    str1005RecordXML.iNbDatagram = 0;
    str1006RecordXML.iNbDatagram = 0;
    str1008RecordXML.iNbDatagram = 0;
    str1009RecordXML.iNbDatagram = 0;
    str1010RecordXML.iNbDatagram = 0;
    str1011RecordXML.iNbDatagram = 0;
    str1012RecordXML.iNbDatagram = 0;
    str1013RecordXML.iNbDatagram = 0;
    str1014RecordXML.iNbDatagram = 0;
    str1015RecordXML.iNbDatagram = 0;
    str1016RecordXML.iNbDatagram = 0;
    str1200RecordXML.iNbDatagram = 0;
    str2000RecordXML.iNbDatagram = 0;
    str7000RecordXML.iNbDatagram = 0;
    str7001RecordXML.iNbDatagram = 0;
    str7002RecordXML.iNbDatagram = 0;
    str7004RecordXML.iNbDatagram = 0;
    str7006RecordXML.iNbDatagram = 0;
    str7007RecordXML.iNbDatagram = 0;
    str7008RecordXML.iNbDatagram = 0;
    str7009RecordXML.iNbDatagram = 0;
    str7010RecordXML.iNbDatagram = 0;
    str7012RecordXML.iNbDatagram = 0;
    str7018RecordXML.iNbDatagram = 0;
    str7021RecordXML.iNbDatagram = 0;
    str7027RecordXML.iNbDatagram = 0;
    str7028RecordXML.iNbDatagram = 0;
    str7030RecordXML.iNbDatagram = 0;
    str7041RecordXML.iNbDatagram = 0;
    str7048RecordXML.iNbDatagram = 0;
    str7057RecordXML.iNbDatagram = 0;
    str7058RecordXML.iNbDatagram = 0;
    str7200RecordXML.iNbDatagram = 0;
    str7503RecordXML.iNbDatagram = 0;
    strDRFRecordXML.iNbDatagram = 0;
    strFileIndexRecordXML.iNbDatagram = 0;


	// Ouverture du fichier de donn�es.
	G_fpData = fopen64(cFileName, "rb");
	if (G_fpData==NULL)
	{
		printf("\nProblem in access Data file : %s\n", cFileName);
		goto ERROR_OUT;
	}

	// Initialisations des variables globales.
	ptrBuffer = NULL;
	// Allocate memory for reading file data
	ptrBuffer = (unsigned char *) malloc((unsigned short)32768);
	if (ptrBuffer == NULL)
	{
	  printf("Can't allocate memory!\n");
	  goto ERROR_OUT;
	}

    // Calcul de la taille du fichier.
	fseeko64(G_fpData, 0, SEEK_END); //On se positionne � la fin du fichier
    llNbOcFic=ftello64(G_fpData);
    fseeko64(G_fpData, 0, SEEK_SET);
    llPosFic = 0;


	// Boucle sur la lecture du fichier de donn�es
	while(!feof(G_fpData) && llPosFic < llNbOcFic)
	{
	    llPosFic = ftello64(G_fpData);

	    // Affichage de la progression dans la fen�tre d'ex�cution
#ifdef GUI
	    if(progress)
	    	progress(100*(double)llPosFic/(double)llNbOcFic);
#else
		util_vProgressBar(0, llPosFic, llNbOcFic);
#endif


		// Lecture du Header de paquet.
	    iRet = iProcessDRFRecord(G_fpData, iNbRecord, &strDRFRecordXML, &DRF_Record);
		if (iRet != 0)
		{
			printf("Error in reading : %s\n", cFileName);
			goto ERROR_OUT;
		}
	    iNbRecord++;
	    // Identification du mod�le de RESON parmis une liste de mod�les identifi�s.
	    if (iFlagDeviceIdentify == 0)
	    {
	    	iRet = utilS7K_uiIdentifyDevice(DRF_Record.DeviceIdentifier,
	    								&iFlagDeviceIdentify);
	    	if (iFlagDeviceIdentify == 1)
	    	{
	    		G_u32SystemSerialNumber =  DRF_Record.DeviceIdentifier;
	    	}
	    }

	    uiRecordTypeIdentifier = DRF_Record.RecordTypeIdentifier;

		// Pour la cr�ation du fichier d'index apr�s lecture du Header.
		// Le paquet FileIndex fait l'objet d'un traitement particulier n'�tant pas un datagramme � part
		// enti�re.
	    iDate  = util_iDayJma2Ifr(1, 1, DRF_Record.Year) + DRF_Record.Day - 1;
	    iHeure = 1000 * (DRF_Record.Seconds + DRF_Record.Minutes * 60 + DRF_Record.Hours * 3600);

	    RTH_FileIndexRecord.Date 				= iDate;
	    RTH_FileIndexRecord.Heure 				= iHeure;
	    RTH_FileIndexRecord.DatagramPosition 	= llPosFic;
	    RTH_FileIndexRecord.NbOfBytesInDatagram = DRF_Record.Size;
	    RTH_FileIndexRecord.TypeOfDatagram 		= DRF_Record.RecordTypeIdentifier;
	    RTH_FileIndexRecord.PingCounter 		= uiPingCounter;
	    RTH_FileIndexRecord.PingSequence 		= usSequence;
	    RTH_FileIndexRecord.DeviceIdentifier 	= DRF_Record.DeviceIdentifier;

		iRet = iProcessFileIndexRecord(G_fpData,
									strFileIndexRecordXML.iNbDatagram,
									&strFileIndexRecordXML,
									&RTH_FileIndexRecord);
		strFileIndexRecordXML.iNbDatagram++;

		if (DEBUG)
		{
			printf("Datagram processing %d\n", uiRecordTypeIdentifier);
		}

		switch (uiRecordTypeIdentifier)
		{
		   case 1000:
			    iRet = iProcess1000Record(G_fpData, str1000RecordXML.iNbDatagram, DRF_Record.OptionalDataOffset, &str1000RecordXML, &RTH_1000Record);
			    str1000RecordXML.iNbDatagram++;
				break;

		   case 1001:
			    iRet = iProcess1001Record(G_fpData, str1001RecordXML.iNbDatagram, DRF_Record.OptionalDataOffset, &str1001RecordXML, &RTH_1001Record);
			    str1001RecordXML.iNbDatagram++;
				break;

		   case 1002:
			    iRet = iProcess1002Record(G_fpData, str1002RecordXML.iNbDatagram, DRF_Record.OptionalDataOffset, &str1002RecordXML, &RTH_1002Record);
				str1002RecordXML.iNbDatagram++;
				break;

		   case 1003:
			    iRet = iProcess1003Record(G_fpData, str1003RecordXML.iNbDatagram, DRF_Record.OptionalDataOffset, &str1003RecordXML, &RTH_1003Record);
			    str1003RecordXML.iNbDatagram++;
				break;

		   case 1005:
			    iRet = iProcess1005Record(G_fpData, str1005RecordXML.iNbDatagram, DRF_Record.OptionalDataOffset, &str1005RecordXML, &RTH_1005Record);
			    str1005RecordXML.iNbDatagram++;
				break;

		   case 1006:
			    iRet = iProcess1006Record(G_fpData, str1006RecordXML.iNbDatagram, DRF_Record.OptionalDataOffset, &str1006RecordXML, &RTH_1006Record);
			    str1006RecordXML.iNbDatagram++;
				break;

		   case 1008:
			    iRet = iProcess1008Record(G_fpData, str1008RecordXML.iNbDatagram, DRF_Record.OptionalDataOffset, &str1008RecordXML, &RTH_1008Record);
			    str1008RecordXML.iNbDatagram++;
				break;

		   case 1009:
			    iRet = iProcess1009Record(G_fpData, str1009RecordXML.iNbDatagram, DRF_Record.OptionalDataOffset, &str1009RecordXML, &RTH_1009Record);
				str1009RecordXML.iNbDatagram++;
				break;

		   case 1010:
			    iRet = iProcess1010Record(G_fpData, str1010RecordXML.iNbDatagram, DRF_Record.OptionalDataOffset, &str1010RecordXML, &RTH_1010Record);
				str1010RecordXML.iNbDatagram++;
				break;

		   case 1011:
			    iRet = iProcess1011Record(G_fpData, str1011RecordXML.iNbDatagram, DRF_Record.OptionalDataOffset, &str1011RecordXML, &RTH_1011Record);
				str1011RecordXML.iNbDatagram++;
				break;

		   case 1012:
			    iRet = iProcess1012Record(G_fpData, str1012RecordXML.iNbDatagram, DRF_Record.OptionalDataOffset, &str1012RecordXML, &RTH_1012Record);
			    str1012RecordXML.iNbDatagram++;
				break;

		   case 1013:
			    iRet = iProcess1013Record(G_fpData, str1013RecordXML.iNbDatagram, DRF_Record.OptionalDataOffset, &str1013RecordXML, &RTH_1013Record);
			    str1013RecordXML.iNbDatagram++;
				break;

		   case 1014:
			    iRet = iProcess1014Record(G_fpData, str1014RecordXML.iNbDatagram, DRF_Record.OptionalDataOffset, &str1014RecordXML, &RTH_1014Record);
			    str1014RecordXML.iNbDatagram++;
				break;

		   case 1015:
			    iRet = iProcess1015Record(G_fpData, str1015RecordXML.iNbDatagram, DRF_Record.OptionalDataOffset, &str1015RecordXML, &RTH_1015Record);
			    str1015RecordXML.iNbDatagram++;
				break;

		   case 1016:
			    iRet = iProcess1016Record(G_fpData, str1016RecordXML.iNbDatagram, DRF_Record.OptionalDataOffset, &str1016RecordXML, &RTH_1016Record);
			    str1016RecordXML.iNbDatagram++;
				break;

		   case 1200:
			    iRet = iProcess1200Record(G_fpData, str1200RecordXML.iNbDatagram, DRF_Record.OptionalDataOffset, &str1200RecordXML, &RTH_1200Record);
			    str1200RecordXML.iNbDatagram++;
				break;

		   case 2000:
			    iRet = iProcess2000Record(G_fpData, str2000RecordXML.iNbDatagram, DRF_Record.OptionalDataOffset, &str2000RecordXML, &RTH_2000Record);
			    str2000RecordXML.iNbDatagram++;
				break;

		   case 7000:
			    iRet = iProcess7000Record(G_fpData, str7000RecordXML.iNbDatagram, DRF_Record.OptionalDataOffset, &str7000RecordXML, &RTH_7000Record);
			    str7000RecordXML.iNbDatagram++;
			    uiPingCounter = RTH_7000Record.PingNumber;
			    usSequence    = RTH_7000Record.MultiPingSequence;

			    break;

		   case 7001 : // 7k Configuration
			    iRet = iProcess7001Record(G_fpData, str7001RecordXML.iNbDatagram, DRF_Record.OptionalDataOffset, &str7001RecordXML, &RTH_7001Record);
			    str7001RecordXML.iNbDatagram++;
			   break;

		   case 7002:
			    iRet = iProcess7002Record(G_fpData, str7002RecordXML.iNbDatagram, DRF_Record.OptionalDataOffset, &str7002RecordXML, &RTH_7002Record);
			    str7002RecordXML.iNbDatagram++;
			    uiPingCounter = RTH_7002Record.PingNumber;
			    break;

		   case 7004:
			    iRet = iProcess7004Record(G_fpData, str7004RecordXML.iNbDatagram, DRF_Record.OptionalDataOffset, &str7004RecordXML, &RTH_7004Record);
			    str7004RecordXML.iNbDatagram++;
			    break;

		   case 7006:
			    iRet = iProcess7006Record(G_fpData, str7006RecordXML.iNbDatagram, DRF_Record.OptionalDataOffset, &str7006RecordXML, &RTH_7006Record);
			    str7006RecordXML.iNbDatagram++;
			    uiPingCounter = RTH_7006Record.PingNumber;
			    usSequence    = RTH_7006Record.MultiPingSequence;
				break;

		   case 7007:
			    iRet = iProcess7007Record(G_fpData, str7007RecordXML.iNbDatagram, DRF_Record.OptionalDataOffset, &str7007RecordXML, &RTH_7007Record);
			    str7007RecordXML.iNbDatagram++;
			    uiPingCounter = RTH_7007Record.PingNumber;
			    usSequence    = RTH_7007Record.MultiPingSequence;
				break;

		   case 7008:
			    iRet = iProcess7008Record(G_fpData, str7008RecordXML.iNbDatagram, DRF_Record.OptionalDataOffset, &str7008RecordXML, &RTH_7008Record);
			    str7008RecordXML.iNbDatagram++;
			    uiPingCounter = RTH_7008Record.PingNumber;
			    usSequence    = RTH_7008Record.MultiPingSequence;
				break;

		   case 7009:
			    iRet = iProcess7009Record(G_fpData, str7009RecordXML.iNbDatagram, DRF_Record.OptionalDataOffset, &str7009RecordXML, &RTH_7009Record);
			    str7009RecordXML.iNbDatagram++;
			    uiPingCounter = RTH_7009Record.PingNumber;
			    usSequence    = RTH_7009Record.MultiPingSequence;
				break;

		   case 7010:
			    iRet = iProcess7010Record(G_fpData, str7010RecordXML.iNbDatagram, DRF_Record.OptionalDataOffset, &str7010RecordXML, &RTH_7010Record);
			    uiPingCounter = RTH_7010Record.PingNumber;
			    usSequence    = RTH_7010Record.MultiPingSequence;
			    str7010RecordXML.iNbDatagram++;
				break;

		   case 7012:
			    iRet = iProcess7012Record(G_fpData, str7012RecordXML.iNbDatagram, DRF_Record.OptionalDataOffset, &str7012RecordXML, &RTH_7012Record);
			    uiPingCounter = RTH_7012Record.PingNumber;
			    usSequence    = RTH_7012Record.MultiPingSequence;
			    str7012RecordXML.iNbDatagram++;
				break;
			    break;

		   case 7018:
			    iRet = iProcess7018Record(G_fpData, str7018RecordXML.iNbDatagram, DRF_Record.OptionalDataOffset, &str7018RecordXML, &RTH_7018Record);
			    str7018RecordXML.iNbDatagram++;
			    uiPingCounter = RTH_7018Record.PingNumber;
			    usSequence    = RTH_7018Record.MultiPingSequence;
			    break;

		   case 7021:
			    iRet = iProcess7021Record(G_fpData, str7021RecordXML.iNbDatagram, DRF_Record.OptionalDataOffset, &str7021RecordXML, &RTH_7021Record);
			    str7021RecordXML.iNbDatagram++;
			    break;

		   case 7027:
			    iRet = iProcess7027Record(G_fpData, str7027RecordXML.iNbDatagram, DRF_Record.OptionalDataOffset, &str7027RecordXML, &RTH_7027Record);
			    str7027RecordXML.iNbDatagram++;
			    uiPingCounter = RTH_7027Record.PingNumber;
			    usSequence    = RTH_7027Record.MultiPingSequence;
			    break;

		   case 7028:
			    iRet = iProcess7028Record(G_fpData, str7028RecordXML.iNbDatagram, DRF_Record.OptionalDataOffset, &str7028RecordXML, &RTH_7028Record);
			    str7028RecordXML.iNbDatagram++;
			    uiPingCounter = RTH_7028Record.PingNumber;
			    usSequence    = RTH_7028Record.MultiPingSequence;
			    break;

		   case 7030:
			    iRet = iProcess7030Record(G_fpData, str7030RecordXML.iNbDatagram, DRF_Record.OptionalDataOffset, &str7030RecordXML, &RTH_7030Record);
			    str7030RecordXML.iNbDatagram++;
			    break;

		   case 7038:
				// Pas de traitement sous MatLab.
			    break;

		   case 7041:
			    iRet = iProcess7041Record(G_fpData, str7041RecordXML.iNbDatagram, DRF_Record.OptionalDataOffset, &str7041RecordXML, &RTH_7041Record);
			    str7041RecordXML.iNbDatagram++;
			    uiPingCounter = RTH_7041Record.PingNumber;
			    usSequence    = RTH_7041Record.MultiPingSequence;
			    break;

		   case 7048:
			    iRet = iProcess7048Record(G_fpData, str7048RecordXML.iNbDatagram, DRF_Record.OptionalDataOffset, &str7048RecordXML, &RTH_7048Record);
			    str7048RecordXML.iNbDatagram++;
			    uiPingCounter = RTH_7048Record.PingNumber;
			    usSequence    = RTH_7048Record.MultiPingSequence;
			    break;

		   case 7057:
			    iRet = iProcess7057Record(G_fpData, str7057RecordXML.iNbDatagram, DRF_Record.OptionalDataOffset, &str7057RecordXML, &RTH_7057Record);
			    str7057RecordXML.iNbDatagram++;
			    uiPingCounter = RTH_7057Record.PingNumber;
			    usSequence    = RTH_7057Record.MultiPingSequence;
			    break;

		   case 7058:
			    iRet = iProcess7058Record(G_fpData, str7058RecordXML.iNbDatagram, DRF_Record.OptionalDataOffset, &str7058RecordXML, &RTH_7058Record);
			    str7058RecordXML.iNbDatagram++;
			    uiPingCounter = RTH_7058Record.PingNumber;
			    usSequence    = RTH_7058Record.MultiPingSequence;
			    break;


		   case 7200:
			    iRet = iProcess7200Record(G_fpData, str7200RecordXML.iNbDatagram, DRF_Record.OptionalDataOffset, &str7200RecordXML, &RTH_7200Record);
				str7200RecordXML.iNbDatagram++;
				break;

		   case 7503:
			    iRet = iProcess7503Record(G_fpData, str7503RecordXML.iNbDatagram, DRF_Record.OptionalDataOffset, &str7503RecordXML, &RTH_7503Record);
				str7503RecordXML.iNbDatagram++;
			    uiPingCounter = RTH_7503Record.PingNumber;
			    usSequence    = RTH_7503Record.MultiPingSequence;
				break;


		   default:
				// Recherche de datagrammes non d�cod�s d�j� trouv�s.
				for (iLoop=0; iLoop<20; iLoop++)
				{
					if (uiRecordTypeIdentifier == tabStrRecordNotTranslate[iLoop].uiType)
					{
						tabStrRecordNotTranslate[iLoop].iNbTimes++;
						iFlagFindTuple = TRUE;
						break;
					}
				}
				if (iFlagFindTuple == FALSE)
				{
					tabStrRecordNotTranslate[iNbDatagramNotTranslate].uiType =uiRecordTypeIdentifier;
					tabStrRecordNotTranslate[iNbDatagramNotTranslate].iNbTimes++;
					iNbDatagramNotTranslate++;
				}
				iFlagFindTuple = FALSE;


			   if (DEBUG)
				   printf("Datagram not recognize %d\n", uiRecordTypeIdentifier);
			   break;
		}

		// Traitement d'erreur
		if (iRet != 0)
		{
		   printf("%s : Error in Process %d Data Process : %s\n", __FILE__, uiRecordTypeIdentifier, cFileName);
		   goto ERROR_OUT;
		}
		// Identation en r�f�rence avec la taille reconnue dans le paquet d'ent�te.
		llPosFic = llPosFic + DRF_Record.Size;
		fseeko64(G_fpData, llPosFic, SEEK_SET);

	} // Fin de la boucle While de lecture

	// End of progress bar
	if(progress){
		progress(100);
	}

	// Fermeture du fichier de donn�e.
	fclose(G_fpData);


	printf("Datagrams not translated : %d\n", iNbDatagramNotTranslate);
	// Recherche de datagrammes non d�cod�s d�j� trouv�s.
	for (iLoop=0; iLoop<iNbDatagramNotTranslate; iLoop++)
	{
		printf("Process datagram : Datagram %d found %d times not translated\n",	tabStrRecordNotTranslate[iLoop].uiType, tabStrRecordNotTranslate[iLoop].iNbTimes);
	}


	// ----------------------------------------------
	// Fermeture des fichiers Binaires
	// + Sauvegarde des fichiers XML cr��s
	utilS7K_iSaveAndCloseFiles(G_cRepData, &strFileIndexRecordXML);
	if (iNbRecord != 0)
	{
		strDRFRecordXML.iNbDatagram = iNbRecord;
		utilS7K_iSaveAndCloseFiles(G_cRepData, &strDRFRecordXML);
	}
	if (str1000RecordXML.iNbDatagram != 0)
	{
		utilS7K_iSaveAndCloseFiles(G_cRepData, &str1000RecordXML);
	}
	if (str1001RecordXML.iNbDatagram != 0)
	{
		utilS7K_iSaveAndCloseFiles(G_cRepData, &str1001RecordXML);
	}
	if (str1002RecordXML.iNbDatagram != 0)
	{
		utilS7K_iSaveAndCloseFiles(G_cRepData, &str1002RecordXML);
	}
	if (str1003RecordXML.iNbDatagram != 0)
	{
		utilS7K_iSaveAndCloseFiles(G_cRepData, &str1003RecordXML);
	}
	if (str1005RecordXML.iNbDatagram != 0)
	{
		utilS7K_iSaveAndCloseFiles(G_cRepData, &str1005RecordXML);
	}
	if (str1006RecordXML.iNbDatagram != 0)
	{
		utilS7K_iSaveAndCloseFiles(G_cRepData, &str1006RecordXML);
	}
	if (str1008RecordXML.iNbDatagram != 0)
	{
		utilS7K_iSaveAndCloseFiles(G_cRepData, &str1008RecordXML);
	}
	if (str1009RecordXML.iNbDatagram != 0)
	{
		utilS7K_iSaveAndCloseFiles(G_cRepData, &str1009RecordXML);
	}
	if (str1010RecordXML.iNbDatagram != 0)
	{
		utilS7K_iSaveAndCloseFiles(G_cRepData, &str1010RecordXML);
	}
	if (str1011RecordXML.iNbDatagram != 0)
	{
		utilS7K_iSaveAndCloseFiles(G_cRepData, &str1011RecordXML);
	}
	if (str1012RecordXML.iNbDatagram != 0)
	{
		utilS7K_iSaveAndCloseFiles(G_cRepData, &str1012RecordXML);
	}
	if (str1013RecordXML.iNbDatagram != 0)
	{
		utilS7K_iSaveAndCloseFiles(G_cRepData, &str1013RecordXML);
	}
	if (str1014RecordXML.iNbDatagram != 0)
	{
		utilS7K_iSaveAndCloseFiles(G_cRepData, &str1014RecordXML);
	}
	if (str1015RecordXML.iNbDatagram != 0)
	{
		utilS7K_iSaveAndCloseFiles(G_cRepData, &str1015RecordXML);
	}
	if (str1016RecordXML.iNbDatagram != 0)
	{
		utilS7K_iSaveAndCloseFiles(G_cRepData, &str1016RecordXML);
	}
	if (str1200RecordXML.iNbDatagram != 0)
	{
		utilS7K_iSaveAndCloseFiles(G_cRepData, &str1200RecordXML);
	}
	if (str2000RecordXML.iNbDatagram != 0)
	{
		utilS7K_iSaveAndCloseFiles(G_cRepData, &str2000RecordXML);
	}
	if (str7000RecordXML.iNbDatagram != 0)
	{
		utilS7K_iSaveAndCloseFiles(G_cRepData, &str7000RecordXML);
	}
	if (str7001RecordXML.iNbDatagram != 0)
	{
		utilS7K_iSaveAndCloseFiles(G_cRepData, &str7001RecordXML);
	}
	if (str7002RecordXML.iNbDatagram != 0)
	{
		utilS7K_iSaveAndCloseFiles(G_cRepData, &str7002RecordXML);
	}
	if (str7004RecordXML.iNbDatagram != 0)
	{
		utilS7K_iSaveAndCloseFiles(G_cRepData, &str7004RecordXML);
	}
	if (str7006RecordXML.iNbDatagram != 0)
	{
		utilS7K_iSaveAndCloseFiles(G_cRepData, &str7006RecordXML);
	}
	if (str7007RecordXML.iNbDatagram != 0)
	{
		utilS7K_iSaveAndCloseFiles(G_cRepData, &str7007RecordXML);
	}
	if (str7008RecordXML.iNbDatagram != 0)
	{
		utilS7K_iSaveAndCloseFiles(G_cRepData, &str7008RecordXML);
	}
	if (str7009RecordXML.iNbDatagram != 0)
	{
		utilS7K_iSaveAndCloseFiles(G_cRepData, &str7009RecordXML);
	}
	if (str7010RecordXML.iNbDatagram != 0)
	{
		utilS7K_iSaveAndCloseFiles(G_cRepData, &str7010RecordXML);
	}
	if (str7012RecordXML.iNbDatagram != 0)
	{
		utilS7K_iSaveAndCloseFiles(G_cRepData, &str7012RecordXML);
	}
	if (str7018RecordXML.iNbDatagram != 0)
	{
		utilS7K_iSaveAndCloseFiles(G_cRepData, &str7018RecordXML);
	}
	if (str7021RecordXML.iNbDatagram != 0)
	{
		utilS7K_iSaveAndCloseFiles(G_cRepData, &str7021RecordXML);
	}
	if (str7027RecordXML.iNbDatagram != 0)
	{
		utilS7K_iSaveAndCloseFiles(G_cRepData, &str7027RecordXML);
	}
	if (str7028RecordXML.iNbDatagram != 0)
	{
		utilS7K_iSaveAndCloseFiles(G_cRepData, &str7028RecordXML);
	}
	if (str7030RecordXML.iNbDatagram != 0)
	{
		utilS7K_iSaveAndCloseFiles(G_cRepData, &str7030RecordXML);
	}
	if (str7041RecordXML.iNbDatagram != 0)
	{
		utilS7K_iSaveAndCloseFiles(G_cRepData, &str7041RecordXML);
	}
	if (str7048RecordXML.iNbDatagram != 0)
	{
		utilS7K_iSaveAndCloseFiles(G_cRepData, &str7048RecordXML);
	}
	if (str7057RecordXML.iNbDatagram != 0)
	{
		utilS7K_iSaveAndCloseFiles(G_cRepData, &str7057RecordXML);
	}
	if (str7058RecordXML.iNbDatagram != 0)
	{
		utilS7K_iSaveAndCloseFiles(G_cRepData, &str7058RecordXML);
	}
	if (str7200RecordXML.iNbDatagram != 0)
	{
		utilS7K_iSaveAndCloseFiles(G_cRepData, &str7200RecordXML);
	}
	if (str7503RecordXML.iNbDatagram != 0)
	{
		utilS7K_iSaveAndCloseFiles(G_cRepData, &str7503RecordXML);
	}

	return EXIT_SUCCESS;

	ERROR_OUT:
	// End of progress bar
	if(progress){
		progress(50);
		progress(100);
	}

	if (G_fpData != NULL) {
	  fclose(G_fpData);
	  G_fpData = 0;
	}

	return EXIT_FAILURE;

} // iConvertFiles_S7K


#pragma pack()
