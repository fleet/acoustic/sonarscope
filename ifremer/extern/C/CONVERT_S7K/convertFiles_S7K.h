/*
 * convertFiles_S7K.h
 *
 *  Created on: 13 oct. 2008
 *      Author: rgallou
 */

#ifndef CONVERTFILES_S7K_H_
#define CONVERTFILES_S7K_H_

#pragma pack(1) // Pour la compilation sous Visual Studio
				// et l'alignement sur 8 octets.

	#define DEBUG 0

	char			*G_cFileData;			// Fichier avec chemin d'origine
	char			*G_cRepData;			// R�pertoire de destination des donn�es
	u32				G_u32SystemSerialNumber;// Eq. au DRF.DeviceIdentifier.
	FILE			*G_fpData;

	unsigned char 	*ptrBuffer;			// Buffer de lecture

	int iConvertFiles_S7K(const char *cFileName,void(*progress)(int));
#pragma pack()

#endif /* CONVERTFILES_S7K_H_ */
