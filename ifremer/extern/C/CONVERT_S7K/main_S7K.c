// ----------------------------------------------------------------------
// Programme :
//	CONVERT_S7K.C
//
// Objet :
//	Convertir un fichier au format S7K pour d�composer en fichiers
//	XML et fichiers binaires. Les fichiers XML decrivent le contenu des
// 	binaires.
// 	Chaque fichier binaire comporte une donn�e �l�mentaire des paquets par ping
// 	issue des paquets de donn�es du S7K.
//	Il a �t� r�alise sous l'IDE Eclipse 3.4.1 avec compilateur gcc.
//
// Cr�ation :
//	02/10/2008
//
// Auteur :
//  GLU : (ATL) roger.gallou@altran.com
//		  02 98 05 43 21
//
// Historique des modifications :
// - passage du compteur de datagramme comme param�tre des fcts de traitements.
// - ajout de Facteur d'�chelles.
// - Renommage de Signals en Variables, de Tables en Images
// - correction du typage de donn�es de type Unsigned Char en u8
// - introduction des donn�es � 2 Dimensions (Image2)
// - restructuration de l'ent�te du fichier
// - cr�ation d'un fichier d'index.
// - traitement de fichiers pour 64 bits.
// - d�placement de fonction utilitaires (Swap et Val Variable from File)
// - inhibition des balises Unit et Tagf, inutilis�es pour l'instant.
//
// 27/09/2012 : GLU (ALTRAN Ouest)
// - Passage en option de l'affichage de la barre de progression.
// ----------------------------------------------------------------------
#pragma pack(1) // Pour la compilation sous Visual Studio
				// et l'alignement sur 8 octets.

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <conio.h>
#include <io.h>
#include <string.h>
#include <errno.h>
#include <libgen.h>

#include "S7K_Datagrams.h"
#include "convertFiles_S7K.h"


#include "S7K_processFileIndexRecord.h"

#include "S7K_Utilities.h"
#include "Generic_Utilities.h"    // Contenu dans la librairie libCONVERT_utils.a


#ifdef GUI
#include "ProgressInfo.h"
#endif

//----------------------------------------------------------------
// Fonction 	: Main
// Objet 		: Programme principal
// Modification : 02/10/08
// Auteur 		: GLU
// Param�tres	: Nom du fichier (avec chemin)
//
//----------------------------------------------------------------
int main(int argc, char *argv[])
{
	char 	cFileName[2000],
			cFileNameTmp[2000],
			cDirSonarScope[1500],
			cStrToFind[3][10]= {"7111", "7125", "7150"},
			*ptrResult = NULL,
			cRepDataTmp[1500],
			cFlagProgressBar[5];

	int 	iRet = 0,
			iErr,
			iLoop = 0,
			iDummy,
			iProgressBar;

	unsigned int uiPingCounter = 0;

	unsigned short usSequence = 0 ;



	if (argc < 3) {
	  printf("Usage: CONVERT_S7K <input .S7K file>\n");
	  printf("       Reads an S7K file and prints some data about it.\n");
	  return FALSE;
	}

	// Cr�ation du r�pertoire Racine contenant les .bin.
	printf("File Process : %s\n", argv[2]);
	G_cFileData = calloc(150, sizeof(char));
	memcpy(G_cFileData, argv[2], strlen(argv[2]));
	G_cRepData = calloc(200, sizeof(char));
	// dirname pour le chemin et basename pour le nom de fichier simple.
	sprintf(cFileName, "%s", basename(argv[2]));
	// Suppression de l'extension
	cFileName[strlen(cFileName)-4] = '\0';
	sprintf(cFileNameTmp, "%s_tmp", cFileName);

	//R�cup�ration du flag de Progress Bar
	memcpy(cFlagProgressBar, argv[1], strlen(argv[1]));
	iProgressBar = (int)atoi(cFlagProgressBar);

	// V�rification de l'existence du r�petoire SonarScope.
	sprintf(cDirSonarScope, "%s\\%s", dirname(argv[2]), "SonarScope");

	iDummy = access(cDirSonarScope, F_OK);
	if (iDummy != 0)
	{
		iErr = mkdir(cDirSonarScope);
		if (iErr != 0)
		{
		   switch (errno)
		   {
			   // "File exists", on ne fait rien.
			   case EEXIST:
				   break;
			   // Autres cas d'erreurs
			   default:
				   printf("Error in Directory creating : %s\n", strerror(errno));
				   return FALSE;
				   break;
		   }
		}
	}

	// Cr�ation du r�pertoire de donn�es
	sprintf(G_cRepData, "%s\\%s", cDirSonarScope, cFileName);
	sprintf(cRepDataTmp, "%s\\%s%s", cDirSonarScope, cFileName, "_tmp");

	if (DEBUG)
	{
		 printf("-- Directory Creation : %s\n", cRepDataTmp);

	}

	iErr = mkdir(cRepDataTmp);
	if (iErr != 0)
	{
	   switch (errno)
	   {
		   // "File exists", on ne fait rien.
		   case EEXIST:
			   break;
		   // Autres cas d'erreurs
		   default:
			   printf("Error in Directory creating : %s\n", strerror(errno));
			   return FALSE;
			   break;
	   }
	}

	#ifndef GUI
		iRet = iConvertFiles_S7K(G_cFileData, NULL);
	#else
		if (iProgressBar == 1)
			runWithProgress(argc, argv, &iConvertFiles_S7K, G_cFileData);
		else
			iRet = iConvertFiles_S7K(G_cFileData, NULL);
	#endif

	// Renommage du r�pertoire puisqu'on arrive au bout !
	iRet = util_iRenameDir(cRepDataTmp);
	if (iRet == 0)
	{
		printf("Process Ending : renaming Temporary Directory %s\n", cRepDataTmp);
	}

	return EXIT_SUCCESS;

	ERROR_OUT:

	if (G_fpData != 0) {
	  fclose(G_fpData);
	  G_fpData = 0;
	}

	return EXIT_FAILURE;


} // Main


#pragma pack()
