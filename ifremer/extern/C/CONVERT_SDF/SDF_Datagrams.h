#ifndef SDF_DATAGRAMS_H_
#define SDF_DATAGRAMS_H_

#include "stdio.h"

#include "Generic_Utilities.h"    // Contenu dans la librairie libCONVERT_utils.a

#define U32 unsigned int
#define F32 float
// Description des contenants de variables �l�mentaire de description XML
// Peut servir � d�crire des attributs dont l'�criture au format XML est
// limit�e.

// Description des contenants de variables �l�mentaire de description XML
// Peut servir � d�crire des attributs dont l'�criture au format XML est
// limit�e.
typedef struct {
	   char				cNom[50],			// Label de la variable
					    cTag[50],			// Tag identifiant de la variable
					    cUnit[50],			// Unit� de la variable
						cType[30],			// Type WORD, String, ...
						cNomPathFileBin[300];	// Nom et chemin du fichier Bin
	   FILE				*ptr_fpBin;			// Pointeur du fichier de signaux
	   BOOL 			bFlagConstant;		// Indicateur de constance de la variable
	   unsigned short	usSize;				// n octets
	   unsigned short	usNbElem;			// Nombre de variables �l�mentaires
	   void				*ptr_vPrevVal;		// Valeur pr�c�dente de lecture
	   float			fScaleFactor;		// yVraie = Val*S_F + A_O
	   float			fAddOffset;			//
} T_SDFVARXML;

typedef struct {
	   T_SDFVARXML	*strTabSignalXML;		// Description des signaux d'un paquet
	   T_SDFVARXML	*strTabImageXML;		// Description des images d'un paquet
	   char			*ptr_cNomHeader,		// Nom du Paquet
					*ptr_cLabelHeader,		// Label du paquet
					*ptr_cCommentsHeader,	// Commentaires du paquet
	   				*ptr_cModelHeader,		// Mod�le du syst�me.
	   				*ptr_cSerialNumberHeader,// SN du syst�me.
				    *ptr_cNomIndexTable;	// Nom de la variable d'indexation des
											// variables de type table.
	   int			iNbDatagram;			// Nombre d'occurrence des donn�es.
	   short		sNbVarSignal;			// Nombre de variables de type signal.	short		sNb;	// Nombre de signaux du RD
	   short		sNbVarImage;			// Nombre de variables de type image.

} T_SDFRECORDXML;

//Header structure version 3
typedef struct
{
U32 numberBytes; 		/* total number of bytes in page */
U32 pageVersion;
U32 configuration;
U32 pingNumber;
U32 numSamples; 		/* count of samples in processed side-scan data,
						 * if processed data exists in page, or count
						 * of samples in rawdata channels if no
						 * processed data exists. The difference is
						 * stored in numSamplesExtra (type3 hdr).
						*/
U32 beamsToDisplay;
U32 errorFlags;
U32 range;
U32 speedFish;
U32 speedSound;
U32 resMode;
U32 txWaveform;
U32 respDiv;
U32 respFreq;
U32 manualSpeedSwitch;
U32 despeckleSwitch;
U32 speedFilterSwitch;
U32 year;
U32 month;
U32 day;
U32 hour;
U32 minute;
U32 second;
U32 hSecond;
U32 fixTimeHour;
U32 fixTimeMinute;
float fixTimeSecond;
float heading; 			/* heading from towfish compass */
float pitch; 			/* pitch from compass */
float roll; 			/* roll from compass */
float depth; 			/* from towfish */
float altitude; 		/* from towfish */
float temperature; 		/* from towfish */
float speed; 			/* from GPS, updated on GPS update,m/s */
float shipHeading; 		/* from GPS � Course Over Ground*/
float magneticVariation;/* from GPS */
double shipLat; 		/* from GPS, radians */
double shipLon; 		/* from GPS, radians */
double fishLat; 		/* rads */
double fishLon; 		/* rads */


/* Added at version 3 */
U32 tvgPage;
U32 headerSize; 		/* number of bytes in header */
U32 fixTimeYear;
U32 fixTimeMonth;
U32 fixTimeDay;
float auxPitch; 		/* aux data from AUV or other sensors */
float auxRoll;
float auxDepth;
float auxAlt;
float cableOut;
float fseconds; 		/* fractional seconds */
U32 altimeter; 			/* altimeter off|on */
U32 sampleFreq;
U32 depressorType;
U32 cableType;
F32 shieveXoff;
F32 shieveYoff;
F32 shieveZoff;
F32 GPSheight;
U32 rawDataConfig; 		/* upper word = stbd config, lower = port */

} SDFV3HEADER;


//header structure version 4
typedef struct
	{
	/* Added at version 4 */
	/* Size of this header addition should stay 256 bytes (64 U32s) */
	U32 header3ExtensionSize; 	/* Size of only this header
								 * extension. Must be equal to 256
								 * bytes. */
	U32	sbpTxWaveform; 			/* Tx waveform for Sub Bottom
								 * Profiler (SBP) */
	U32	sbpPreAmpGain; 			/* 0 = low 1 = high */
	U32	sbpDataRaw; 			/* 0 = Processed SBP data, 1 = Raw
								 * SBP data */
	U32	sbpNumSamples; 			/* Number of SBP samples in data
								 * page. May be different from
								 * Side Scan */
	U32	sbpSampleFreq; 			/* Sample frequency of SBP channel */
	U32	sbpTxWaveformVersion; 	/* Tx waveform version of the SBP
								 * Tx board. */
	float wingAngle; 			/* Angle of actuated wing in
								 * degrees */
	U32	emergencySwitchState; 	/* State of system emergency switch
								 * - 1 = on, 0 = off */
								/* Layback position parameters set by SonarPro. Set to zero by
								TPU. */
	U32	laybackMethod; 			/* Method used to calculate
								layback. 0 = Pythagorean
								theorem. */
	double laybackFishLat; /* fish latitude as determined by
	layback calculation (in rads) */
	double laybackFishLon; /* fish longitude as determined by
	layback calculation (in rads) */
	float fishHeadingOffset; /* Magnetic heading offset applied
	to towfish heading, in degrees*/
	float pressureSensorOffset; /* psi */
	/* Added at TPU s/w version 6.13 */
	U32	tpuSwVersion; /* Version of the TPU s/w,
						0xVVNNMMDD where
						VV = Major Version Number
						NN = Minor Version Number
						MM = Month
						DD = Day*/
	/* Added at vxWorks version 6.17.*/
	U32	capabilityMask; /* Bit mask defining various system
						*capabilities.
						* Bit 0 = Configured for raw data (System
						* 5000 only)
						* Bit 1 = Configured for actuated wing
						* Bit 2 = Configured with Sub Bottom
						- 7 -
						* Profiler option
						* Bit 3 = Configured with header ver. 4
						* Bit 4 = Configured to allow a single
						* oversampled frequency (3000 only)
						* Bit 5 = Configured to allow dual
						* frequency operation (3000 only)
						* Added at tpuSwVersion 6.17.
						* Bit 6 = 5000 (V1 or V2) system with
						ver. 2 Demux
						* Bit 7 = 5000 V2 towfish
						*/
	/* Added at vxWorks version V6.19 */
	U32	txVersion; 		/* TPU Transmitter Version 0
						 * = 5000, 1 = 5000 V2
						 */
	/* Added at vxWorks version V6.22 */
	U32	numSamplesExtra; /* The extra number of samples included
						  * in each data channel to account for
						  * chirp Tx waveforms. This value is zero
						  * for other waveforms.
						  */
						 /* Reserved data portion. Array size set to keep this header addition
						  * at 256 bytes. */
	/*Added at vxWorks version V7.00 */
	U32	postProcessVersion;
	u16 motionSensorType;
	u16 pingTimeRefCount;
	u16 extTrigTimeRefCount;
	u16 onePpsTimeRefCount;
	U32	timeRefCountWeight;
	F32 altitudeBathy;
	F32 pingInterval;
	/* Added at vxWorks version V8.00 */
	U32	sdfExtensionSize;
	double secondsOfWeek;
	U32	speedSoundSource;
	F32 pressureSensorMax;
	F32 pressureSensorVoltageMin;
	F32 pressureSensorVoltageMax;
	U32	processedPingNumber;
	U32	reserved3[30];
	} SDFENDV4HEADER;

typedef struct
{
	SDFV3HEADER sdfV3header;
	SDFENDV4HEADER sdfendV4header;
} SDFV4HEADER;

// Description unitaire des signaux
typedef struct  {
	char 			*cTabNomVar,
					*cTabNomType;
	float 			fScaleFactor;
	float 			fAddOffset;
} T_DESC_SIGNAL_OR_IMAGE;

// Description unitaire des signaux
typedef struct  {
	unsigned short 				usNbVarSignal;
	unsigned short 				usNbVarImage;
	unsigned short 				usNbVarImage2;	// Inusit� dans le cas du SDF.
	short 						sNumIndexTable;
	T_DESC_SIGNAL_OR_IMAGE  	*strDescSignal;
	T_DESC_SIGNAL_OR_IMAGE  	*strDescImage;
	T_DESC_SIGNAL_OR_IMAGE  	*strDescImage2; // Inusit� dans le cas du SDF.
} T_DESC_TAB_SIGANDIMG;

#endif
