/*
 * SDF_utilities.c
 *
 *  Created on: 16 juin 2010
 *      Author: mmorvan
 */
#include<string.h>
#include<stdio.h>
#include<stdlib.h>
#include<errno.h>
#include<io.h>

#include"SDF_utilities.h"
#include"convertFiles_SDF.h"


off64_t SDF_trouveDatagram(	FILE *fp,
						unsigned char *ucTypeDatagramChercher,
						int iPositionDebDatagram)
{
	int iNbOfBytesInDatagram = 0,
		iVal,
		iNbBlocsLus;

	unsigned char   ucSTX,
		            ucTypeOfDatagram[1];

	off64_t	llPositionDatagram;

	iNbBlocsLus = fread(&iNbOfBytesInDatagram, sizeof(int), 1, fp);  // Int32
	iNbBlocsLus = fread(&ucSTX, sizeof(unsigned char), 1, fp);
	iNbBlocsLus = fread(ucTypeOfDatagram, sizeof(unsigned char), 1, fp);

	while(ucTypeOfDatagram[0] != ucTypeDatagramChercher[0])
	{
		iPositionDebDatagram = iPositionDebDatagram + iNbOfBytesInDatagram + 4;
		// Positionnement en d�but de fichier + offset.
		fseeko64(fp,iPositionDebDatagram,SEEK_SET);

		iNbBlocsLus = fread(&iVal, sizeof(int), 1, fp);  // Int32
		iNbOfBytesInDatagram = iVal;

		iNbBlocsLus = fread(&ucSTX, sizeof(unsigned char), 1, fp);

		iNbBlocsLus = fread(ucTypeOfDatagram, sizeof(unsigned char), 1, fp);

	}

	llPositionDatagram = iPositionDebDatagram;

	printf("Position cur %lld\n",ftello64(fp));

	printf("Lecture du fichier .sdf: dans trouveDatagram!\n");
	printf("NbOfBytesInDatagram = %d\n",iNbOfBytesInDatagram);
	printf("STX = %c\n",ucSTX);
	printf("TypeOfDatagram = %c\n",ucTypeOfDatagram[0]);
	printf("NbBlocsLus = %d\n",iNbBlocsLus);
	printf("llPositionDatagram = %lld\n",llPositionDatagram);
	printf("\n");

	return llPositionDatagram;
}

//////////////////////////////////////////////////////////////////////
// Fonction :    utilSDF_iSaveAndCloseFiles
// Objet     :    Sauvegarde de la structure XML dans un fichier.
// Modification : 16/06/10
// Auteur     : MVN
//////////////////////////////////////////////////////////////////////
int utilSDF_iSaveAndCloseFiles( char *cRepData,
                                T_SDFRECORDXML *strSDFRecordXML,
                                unsigned int uiConfig)
{
    char    *cNomFicXML,
            *ptr_cNomHeader,
            *cTypeMatLab,
            cNomFicBin[200],
            *cValeur = NULL,
            cValOneChar[1];

    int     iSize,
            iVarSignal,
            iVarImage,
            iRet = 0;

    unsigned short     usNbElem,
                    usTailleEnByte;

    FILE        *fpXML = NULL;


    iSize = strlen(strSDFRecordXML->ptr_cNomHeader) + 1;
    ptr_cNomHeader = (char *)malloc(iSize*sizeof(char));
    ptr_cNomHeader = strSDFRecordXML->ptr_cNomHeader;
    iSize = 5 + strlen(cRepData) + 2 + strlen(ptr_cNomHeader) + 4; // Nom du paquet + '.xml'
    cNomFicXML = calloc(iSize, sizeof(char));
    // Si Extension du fichier d'origine = '.sdf'
	if (!stricmp(G_cFileExtension, "sdf"))
	    sprintf(cNomFicXML, "%s\\SDF_%s%s", cRepData, ptr_cNomHeader, ".xml");
	else
	{
		printf("L'extension du fichier n'est pas reconnue : %s\n", G_cFileExtension);
		return EXIT_FAILURE;
	}
    printf("-- XML File Creation : %s \n", cNomFicXML);

    // Cr�ation de l'ent�te de doc XML
    fpXML = fopen(cNomFicXML, "w+");
    fprintf(fpXML, "%s", "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r");
    fprintf(fpXML, "%s", "<ROOT>\r");
    fprintf(fpXML, "%s%s%s", "\t<Title>", strSDFRecordXML->ptr_cLabelHeader, "</Title>\r");
    fprintf(fpXML, "%s%s%s", "\t<Constructor>", "Klein", "</Constructor>\r");
    fprintf(fpXML, "%s%s%s", "\t<Model>", strSDFRecordXML->ptr_cCommentsHeader, "</Model>\r");
    fprintf(fpXML, "%s%s%s", "\t<SystemSerialNumber>", strSDFRecordXML->ptr_cCommentsHeader, "</SystemSerialNumber>\r");
    fprintf(fpXML, "%s%d%s", "\t<NbDatagrams>", strSDFRecordXML->iNbDatagram, "</NbDatagrams>\r");
    fprintf(fpXML, "%s%s%s", "\t<TimeOrigin>", "01/01/-4713", "</TimeOrigin>\r");
    fprintf(fpXML, "%s%s%s", "\t<Comments>", strSDFRecordXML->ptr_cCommentsHeader, "</Comments>\r");

    if (strSDFRecordXML->sNbVarSignal > 0)
    {
        fprintf(fpXML, "%s", "\t<Variables>\r");

        // Traitement sur l'ensemble des valeurs du RTH.
        for (iVarSignal=0; iVarSignal<strSDFRecordXML->sNbVarSignal; iVarSignal++)
        {
            cTypeMatLab = (char*)malloc(10*sizeof(char));
            iRet = util_cConvertTypeLabel(    strSDFRecordXML->strTabSignalXML[iVarSignal].cType,
                                            cTypeMatLab);

            fprintf(fpXML, "%s", "\t\t<item>\r");
            fprintf(fpXML, "%s%s%s", "\t\t\t<Name>", strSDFRecordXML->strTabSignalXML[iVarSignal].cNom, "</Name>\r");
            fprintf(fpXML, "%s%s%s", "\t\t\t<Storage>", cTypeMatLab, "</Storage>\r");
            if (strSDFRecordXML->strTabSignalXML[iVarSignal].usNbElem > 1)
            {
                fprintf(fpXML, "%s%d%s", "\t\t\t<NbElem>",  strSDFRecordXML->strTabSignalXML[iVarSignal].usNbElem, "</NbElem>\r");
            }

            // Fermeture pr�alable du fichier.
            if (strSDFRecordXML->strTabSignalXML[iVarSignal].ptr_fpBin != NULL)
            {
                fclose(strSDFRecordXML->strTabSignalXML[iVarSignal].ptr_fpBin);
            }

            // R�cup�ration de la variable d�tect�e comme constante.
            usNbElem = strSDFRecordXML->strTabSignalXML[iVarSignal].usNbElem;
            usTailleEnByte = util_usNbOctetTypeVar(strSDFRecordXML->strTabSignalXML[iVarSignal].cType);

            if (strSDFRecordXML->strTabSignalXML[iVarSignal].bFlagConstant == TRUE)
            {
                // Pb de lib�ration de m�moire si  usNbElem * usTailleEnByte = 1
            	if (usNbElem != 1 || usTailleEnByte != 1)
                {
					 // Allocation de la valeur � r�cup�rer selon la taille. Si elle est
					 // du type Chaine, elle doit contenir autant d'�l�ments que d�clar�s.
					 if (strcmp(cTypeMatLab, "char"))
					 {
						 cValeur = (char*)malloc(256*sizeof(char));
					 }
					 else
					 {
						 cValeur = (char*)malloc(usNbElem*sizeof(char));
					 }
                 	 iRet = util_cValVarFromFile(strSDFRecordXML->strTabSignalXML[iVarSignal].cNomPathFileBin,
                 								strSDFRecordXML->strTabSignalXML[iVarSignal].cType,
                 								strSDFRecordXML->strTabSignalXML[iVarSignal].usNbElem,
                 								cValeur);
                     fprintf(fpXML, "%s%s%s", "\t\t\t<Constant>", cValeur, "</Constant>\r");
					 free(cValeur);
                }
                else
                {
                	iRet = util_cValVarFromFile(strSDFRecordXML->strTabSignalXML[iVarSignal].cNomPathFileBin,
                								strSDFRecordXML->strTabSignalXML[iVarSignal].cType,
                								strSDFRecordXML->strTabSignalXML[iVarSignal].usNbElem,
                								&cValOneChar[0]);
                    fprintf(fpXML, "%s%s%s", "\t\t\t<Constant>", &cValOneChar[0], "</Constant>\r");
                }
 				// Effacement du fichier Bin
            	iRet = remove((const char *)strSDFRecordXML->strTabSignalXML[iVarSignal].cNomPathFileBin);
				if (iRet !=0)
				{
				   printf("Cannot remove file %s (%s)\n",
							   strSDFRecordXML->strTabSignalXML[iVarSignal].cNomPathFileBin, ERRORSTRING);
				}
            }
            else
            {
                sprintf(cNomFicBin, "%s%s",strSDFRecordXML->strTabSignalXML[iVarSignal].cNom, ".bin");
                fprintf(fpXML, "%s%s%s", "\t\t\t<FileName>", cNomFicBin, "</FileName>\r");
            }
            if (strSDFRecordXML->strTabSignalXML[iVarSignal].fScaleFactor != 1.0)
				fprintf(fpXML, "%s%g%s", "\t\t\t<ScaleFactor>", strSDFRecordXML->strTabSignalXML[iVarSignal].fScaleFactor, "</ScaleFactor>\r");
            if (strSDFRecordXML->strTabSignalXML[iVarSignal].fAddOffset != 0.0)
				fprintf(fpXML, "%s%f%s", "\t\t\t<AddOffset>", strSDFRecordXML->strTabSignalXML[iVarSignal].fAddOffset, "</AddOffset>\r");
            fprintf(fpXML, "%s", "\t\t</item>\r");
            free(cTypeMatLab);

        } // Fin de la boucle sur les donn�es RTH
        fprintf(fpXML, "%s", "\t</Variables>\r");
        free(strSDFRecordXML->strTabSignalXML);
    } // Fin de l'�criture des Signaux

    // Ouverture de la balise Tables
    if (strSDFRecordXML->sNbVarImage > 0)
    {
        fprintf(fpXML, "%s", "\t<Tables>\r");
        for (iVarImage=0; iVarImage<strSDFRecordXML->sNbVarImage; iVarImage++)
        {
            cTypeMatLab = (char*)malloc(10*sizeof(char));
            iRet = util_cConvertTypeLabel(    strSDFRecordXML->strTabImageXML[iVarImage].cType,
                                            cTypeMatLab);

            // Selon la configuration : fermeture et effacement du fichier. Non �criture de la donn�e dans les fichiers XML.
            if(		((iVarImage==0 || iVarImage==1) && (uiConfig&0x03)!=0x03 && (strncmp(strSDFRecordXML->ptr_cLabelHeader,"V3000",5)==0 || strncmp(strSDFRecordXML->ptr_cLabelHeader,"V3001",5)==0)) ||
            		((iVarImage==2 || iVarImage==3) && (uiConfig&0x0C)!=0x0C && (strncmp(strSDFRecordXML->ptr_cLabelHeader,"V3000",5)==0 || strncmp(strSDFRecordXML->ptr_cLabelHeader,"V3001",5)==0)) ||
            		((iVarImage==4) && (uiConfig&0x10)!=0x10 && (strncmp(strSDFRecordXML->ptr_cLabelHeader,"V3000",5)==0 || strncmp(strSDFRecordXML->ptr_cLabelHeader,"V3001",5)==0)))
            {
            	//low frequency non pr�sent
            	fclose(strSDFRecordXML->strTabImageXML[iVarImage].ptr_fpBin);
            	// Effacement du fichier Bin
				iRet = remove((const char *)strSDFRecordXML->strTabImageXML[iVarImage].cNomPathFileBin);
				if (iRet !=0)
				{
				   printf("Cannot remove file %s (%s)\n",
							   strSDFRecordXML->strTabImageXML[iVarImage].cNomPathFileBin, ERRORSTRING);
				}
            }
            else
            {
				fprintf(fpXML, "%s", "\t\t<item>\r");
				fprintf(fpXML, "%s%s%s", "\t\t\t<Name>", strSDFRecordXML->strTabImageXML[iVarImage].cNom, "</Name>\r");
				fprintf(fpXML, "%s%s%s", "\t\t\t<Storage>", cTypeMatLab, "</Storage>\r");
				if (strSDFRecordXML->strTabImageXML[iVarImage].usNbElem > 1)
				{
					fprintf(fpXML, "%s%d%s", "\t\t\t<NbElem>", strSDFRecordXML->strTabImageXML[iVarImage].usNbElem, "</NbElem>\r");
				}
				// Fermeture pr�alable du fichier.
				fclose(strSDFRecordXML->strTabImageXML[iVarImage].ptr_fpBin);
				usNbElem = strSDFRecordXML->strTabImageXML[iVarImage].usNbElem;
				usTailleEnByte = util_usNbOctetTypeVar(strSDFRecordXML->strTabImageXML[iVarImage].cType);
				if (strSDFRecordXML->ptr_cNomIndexTable != NULL)
				{
					fprintf(fpXML, "%s%s%s", "\t\t\t<Indexation>", strSDFRecordXML->ptr_cNomIndexTable, "</Indexation>\r");
				}

				if (strSDFRecordXML->strTabImageXML[iVarImage].bFlagConstant == TRUE)
				{
					// Pb de lib�ration de m�moire si  usNbElem * usTailleEnByte = 1
					if (usNbElem != 1 || usTailleEnByte != 1)
					{
						// Allocation de la valeur � r�cup�rer selon la taille. Si elle est
						// du type Chaine, elle doit contenir autant d'�l�ments que d�clar�s.
						// Ca marche mais c'est moyen, moyen !!!! (cf. Traitement des signaux)
						if (strcmp(cTypeMatLab, "char"))
						{
							cValeur = (char*)malloc(256*sizeof(char));
						}
						else
						{
							cValeur = (char*)malloc(usNbElem*sizeof(char));
						}
						iRet = util_cValVarFromFile(strSDFRecordXML->strTabImageXML[iVarImage].cNomPathFileBin,
													strSDFRecordXML->strTabImageXML[iVarImage].cType,
													strSDFRecordXML->strTabImageXML[iVarImage].usNbElem,
													cValeur);
						fprintf(fpXML, "%s%s%s", "\t\t\t<Constant>", cValeur, "</Constant>\r");

						if (!strcmp(cTypeMatLab, "char"))
						{
							free(cValeur);
						}
					}
					else
					{
						iRet = util_cValVarFromFile(strSDFRecordXML->strTabImageXML[iVarImage].cNomPathFileBin,
														strSDFRecordXML->strTabImageXML[iVarImage].cType,
														strSDFRecordXML->strTabImageXML[iVarImage].usNbElem,
														&cValOneChar[0]);
						fprintf(fpXML, "%s%s%s", "\t\t\t<Constant>", &cValOneChar[0], "</Constant>\r");
					}
					// Effacement du fichier
					if (remove((const char *)strSDFRecordXML->strTabImageXML[iVarImage].cNomPathFileBin) !=0)
					{
					   printf("Cannot remove file %s (%s)\n",
								   strSDFRecordXML->strTabImageXML[iVarImage].cNomPathFileBin, ERRORSTRING);
					}
				}
				else
				{
					sprintf(cNomFicBin, "%s%s",strSDFRecordXML->strTabImageXML[iVarImage].cNom, ".bin");
					fprintf(fpXML, "%s%s%s", "\t\t\t<FileName>", cNomFicBin, "</FileName>\r");
				}
				if (strSDFRecordXML->strTabImageXML[iVarImage].fScaleFactor != 1.0)
					fprintf(fpXML, "%s%g%s", "\t\t\t<ScaleFactor>", strSDFRecordXML->strTabImageXML[iVarImage].fScaleFactor, "</ScaleFactor>\r");
				if (strSDFRecordXML->strTabImageXML[iVarImage].fAddOffset != 0.0)
					fprintf(fpXML, "%s%f%s", "\t\t\t<AddOffset>", strSDFRecordXML->strTabImageXML[iVarImage].fAddOffset, "</AddOffset>\r");
				fprintf(fpXML, "%s", "\t\t</item>\r");
            }
            free(cTypeMatLab);
        } // Fin de la boucle sur les donn�es Image
        free(strSDFRecordXML->strTabImageXML);
 		fprintf(fpXML, "%s", "\t</Tables>\r");
	}

    fprintf(fpXML, "%s", "</ROOT>\r");
    fclose(fpXML);

    free(cNomFicXML);
    free(strSDFRecordXML->ptr_cLabelHeader);
    free(strSDFRecordXML->ptr_cNomHeader); // ptr_cNomHeader est du coup lib�r�.

    if (iRet != 0)
        return EXIT_FAILURE;
    else
        return EXIT_SUCCESS;


} //utilSDF_iSaveAndCloseFiles

//////////////////////////////////////////////////////////////////////
// Fonction		:	util_iGetIndianSDF
// Objet		:	Test du "boutisme" des fichiers
// Modification : 16/06/10
// Auteur       : MVN
//////////////////////////////////////////////////////////////////////
int util_iGetIndianSDF(char *cNomFic)
{

FILE *fp;

unsigned int	iNumberOfBytesInDatagram_1,
				iNumberOfBytesInDatagram_2;
int				iNbOctetsLus,
				iTypeEndian;

	fp = fopen(cNomFic, "rb");

	if(fp  == NULL)
	{
		printf("\nProblem in access Data file : %s\n", cNomFic);
		iTypeEndian = -1;
		return iTypeEndian;
	}

	//skip pingmaker
	fseeko64(fp,sizeof(int),SEEK_SET);
	// Lecture d'un fichier quelconque
	iNbOctetsLus = fread(&iNumberOfBytesInDatagram_1, sizeof(unsigned int), 1, fp);
	iNumberOfBytesInDatagram_2 = util_iBswap_32(iNumberOfBytesInDatagram_1);

	fclose(fp);

	if (abs(iNumberOfBytesInDatagram_1) < abs(iNumberOfBytesInDatagram_2))
		iTypeEndian = 1; // Little Endian
	else
		iTypeEndian = 0; // Big Endian

	return iTypeEndian;
} // util_iGetIndianSDF

//////////////////////////////////////////////////////////////////////
// Fonction		:	util_iCreateDirDatagram
// Objet		:	Cr�ation du r�pertoire d�di�es aux donn�es d'un
//					Datagrammes
// Modification : 16/06/10
// Auteur       : MVN
//////////////////////////////////////////////////////////////////////
int util_iCreateDirDatagram(char *cRepData,
							char *cNomDatagram,
							char *cRepertoire)
{

int 	iErr;

	// Si Extension = '.sdf'
	if (!stricmp(G_cFileExtension, "sdf"))
		sprintf(cRepertoire, "%s\\SDF_%s", cRepData, cNomDatagram);
	else
	{
		printf("L'extension du fichier n'est pas reconnue : %s\n", G_cFileExtension);
		return EXIT_FAILURE;
	}


	if (DEBUG)
	   printf("-- Directory Creation : %s\n",cRepertoire);

	// Cr�ation du r�pertoire
	iErr = mkdir(cRepertoire);
	if (iErr != 0)
	{
	   switch (errno)
	   {
		   //"File exists"
		   case EEXIST:
			   break;
		   default:
			   printf("Error in Directory creation %s\n", strerror(errno));
			   return EXIT_FAILURE;
			   break;
	   }
	}

	return EXIT_SUCCESS;

} // util_iCreateDirDatagram

//////////////////////////////////////////////////////////////////////
// Fonction		:	utilSDF_initXMLRecord
// Objet		:	Initialisation de la structure XML des signaux et images
//					du fichier.
// Modification :	13/05/2011
// Auteur		:	GLU
//////////////////////////////////////////////////////////////////////
int utilSDF_initXMLRecord(	T_DESC_TAB_SIGANDIMG strDescTabSigOrImg,
							char *cTitleDatagram,
							char *cNomDatagram,
							char *cComments,
							T_SDFRECORDXML *strSDFRecordXML)

{
	char	*ptr_cRepertoire,
			cNomFicPathBin[300];

    int 	iSizeRep,
			iRet,
			iLen,
			iVarSignal,
			iVarImage;

	unsigned short 	usTaille,
					usNbVarSignal,
					usNbVarImage,
					usNbVarImage2;

	short			sNumIndex;


	sNumIndex 		= strDescTabSigOrImg.sNumIndexTable;
	usNbVarSignal	= strDescTabSigOrImg.usNbVarSignal;
	usNbVarImage	= strDescTabSigOrImg.usNbVarImage;
	usNbVarImage2	= strDescTabSigOrImg.usNbVarImage2;

	// Initialisation des structures de pilotage des fichiers
	// XML et Bin .
	strSDFRecordXML->sNbVarSignal 		= usNbVarSignal;
	strSDFRecordXML->sNbVarImage 		= usNbVarImage;
    strSDFRecordXML->strTabSignalXML	= (T_SDFVARXML *)malloc(strSDFRecordXML->sNbVarSignal*sizeof(T_SDFVARXML));
    strSDFRecordXML->strTabImageXML		= (T_SDFVARXML *)malloc(strSDFRecordXML->sNbVarImage*sizeof(T_SDFVARXML));

	iLen = strlen(cNomDatagram)+1;
	strSDFRecordXML->ptr_cNomHeader = malloc(iLen*sizeof(char));
	sprintf(strSDFRecordXML->ptr_cNomHeader, "%s", cNomDatagram);

	iLen = strlen(cTitleDatagram)+1;
	strSDFRecordXML->ptr_cLabelHeader = malloc(iLen*sizeof(char));
	sprintf(strSDFRecordXML->ptr_cLabelHeader, "%s", cTitleDatagram);

	iLen = strlen(cComments)+1;
	strSDFRecordXML->ptr_cCommentsHeader = malloc(iLen*sizeof(char));
	sprintf(strSDFRecordXML->ptr_cCommentsHeader, "%s", cComments);

	// Remplissage du nom de la variable d'indexation des tables.
	if (sNumIndex != -1)
	{
		iLen = strlen(strDescTabSigOrImg.strDescSignal[sNumIndex].cTabNomVar)+1;
		strSDFRecordXML->ptr_cNomIndexTable = malloc(iLen*sizeof(char));
		sprintf(strSDFRecordXML->ptr_cNomIndexTable, "%s", strDescTabSigOrImg.strDescSignal[sNumIndex].cTabNomVar);
	}
	else
	{
		iLen = strlen("NoneIndex")+1;
		strSDFRecordXML->ptr_cNomIndexTable = malloc(iLen*sizeof(char));
		sprintf(strSDFRecordXML->ptr_cNomIndexTable, "%s", "NoneIndex");
	}
	// Affectation et cr�ation du repertoire du Datagramme.
	iSizeRep = ( strlen(G_cRepData) + strlen( cNomDatagram));
	// Ajout d'une marge au cas o� certains caract�res soient doubl�s soient doubl�s.
	ptr_cRepertoire = malloc( (iSizeRep + 1 + 300 + 4)*sizeof(char));
	iRet = util_iCreateDirDatagram(G_cRepData,
								strSDFRecordXML->ptr_cNomHeader,
								ptr_cRepertoire);

	// Initialisation g�n�rale des variables de type Signal
	for (iVarSignal=0; iVarSignal<strSDFRecordXML->sNbVarSignal; iVarSignal++) {
	   strSDFRecordXML->strTabSignalXML[iVarSignal].ptr_fpBin = NULL;
	   strSDFRecordXML->strTabSignalXML[iVarSignal].bFlagConstant = TRUE;
	   strSDFRecordXML->strTabSignalXML[iVarSignal].usNbElem = 1;
	   sprintf(strSDFRecordXML->strTabSignalXML[iVarSignal].cNom, "%s", strDescTabSigOrImg.strDescSignal[iVarSignal].cTabNomVar);
	   sprintf(strSDFRecordXML->strTabSignalXML[iVarSignal].cType, "%s", strDescTabSigOrImg.strDescSignal[iVarSignal].cTabNomType);
	   sprintf(strSDFRecordXML->strTabSignalXML[iVarSignal].cTag, "%s", "TODO");
	   sprintf(strSDFRecordXML->strTabSignalXML[iVarSignal].cUnit, "%s", "TODO");
	   strSDFRecordXML->strTabSignalXML[iVarSignal].fScaleFactor = strDescTabSigOrImg.strDescSignal[iVarSignal].fScaleFactor;
	   strSDFRecordXML->strTabSignalXML[iVarSignal].fAddOffset = strDescTabSigOrImg.strDescSignal[iVarSignal].fAddOffset;
	   usTaille = util_usNbOctetTypeVar(strDescTabSigOrImg.strDescSignal[iVarSignal].cTabNomType);
	   strSDFRecordXML->strTabSignalXML[iVarSignal].usSize = usTaille;
	   iLen = strlen(strSDFRecordXML->strTabSignalXML[iVarSignal].cNom);
	   // Ajout d'une marge au cas o� certains caract�res soient doubl�s soient doubl�s.
	   sprintf(cNomFicPathBin, "%s\\%s%s", ptr_cRepertoire, strSDFRecordXML->strTabSignalXML[iVarSignal].cNom, ".bin");
	   sprintf(strSDFRecordXML->strTabSignalXML[iVarSignal].cNomPathFileBin, "%s", cNomFicPathBin);
	   strSDFRecordXML->strTabSignalXML[iVarSignal].ptr_fpBin = fopen(cNomFicPathBin, "w+b");
	   if (!strSDFRecordXML->strTabSignalXML[iVarSignal].ptr_fpBin)
	   {
		   printf("%s -- Error in Signal creation  : %s\n", __FILE__, cNomFicPathBin);
		   return EXIT_FAILURE;
	   }
	}

	// Initialisation g�n�rale des variables de type Image
	for (iVarImage=0; iVarImage<strSDFRecordXML->sNbVarImage; iVarImage++) {
	   strSDFRecordXML->strTabImageXML[iVarImage].ptr_fpBin = NULL;
	   strSDFRecordXML->strTabImageXML[iVarImage].bFlagConstant = TRUE;
	   strSDFRecordXML->strTabImageXML[iVarImage].usNbElem = 1;
	   sprintf(strSDFRecordXML->strTabImageXML[iVarImage].cNom, "%s", strDescTabSigOrImg.strDescImage[iVarImage].cTabNomVar);
	   sprintf(strSDFRecordXML->strTabImageXML[iVarImage].cType, "%s", strDescTabSigOrImg.strDescImage[iVarImage].cTabNomType);
	   sprintf(strSDFRecordXML->strTabImageXML[iVarImage].cTag, "%s", "TODO");
	   sprintf(strSDFRecordXML->strTabImageXML[iVarImage].cUnit, "%s", "TODO");
	   strSDFRecordXML->strTabImageXML[iVarImage].fScaleFactor = strDescTabSigOrImg.strDescImage[iVarImage].fScaleFactor;
	   strSDFRecordXML->strTabImageXML[iVarImage].fAddOffset = strDescTabSigOrImg.strDescImage[iVarImage].fAddOffset;
	   usTaille = util_usNbOctetTypeVar(strDescTabSigOrImg.strDescImage[iVarImage].cTabNomType);
	   strSDFRecordXML->strTabImageXML[iVarImage].usSize = usTaille;
	   iLen = strlen(strSDFRecordXML->strTabImageXML[iVarImage].cNom);
	   // Ajout d'une marge au cas o� certains caract�res soient doubl�s soient doubl�s.
	   sprintf(cNomFicPathBin, "%s\\%s%s", ptr_cRepertoire, strSDFRecordXML->strTabImageXML[iVarImage].cNom, ".bin");
	   sprintf(strSDFRecordXML->strTabImageXML[iVarImage].cNomPathFileBin, "%s", cNomFicPathBin);
	   strSDFRecordXML->strTabImageXML[iVarImage].ptr_fpBin = fopen(cNomFicPathBin, "w+b");
	   if (!strSDFRecordXML->strTabImageXML[iVarImage].ptr_fpBin)
	   {
		   printf("%s -- Error in Image creation  : %s\n", __FILE__, cNomFicPathBin);
		   return EXIT_FAILURE;
	   }
	}


	free(ptr_cRepertoire);
	return EXIT_SUCCESS;

} // utilSDF_initXMLRecord



