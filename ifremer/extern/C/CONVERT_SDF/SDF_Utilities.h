/*
 * SDF_utilities.h
 *
 *  Created on: 16 juin 2010
 *      Author: mmorvan
 */

#ifndef SDF_UTILITIES_H_
#define SDF_UTILITIES_H_

#include "SDF_Datagrams.h"


off64_t SDF_trouveDatagram(FILE *,unsigned char *,int);

int utilSDF_initXMLRecord(	T_DESC_TAB_SIGANDIMG strDescTabSigOrImg,
							char *cTitleDatagram,
							char *cNomDatagram,
							char *cComments,
							T_SDFRECORDXML *strSDFRecordXML);

int utilSDF_iCreateDirDatagram(char *cRepData,
								char *cNomDatagram,
								char *cRepertoire);


//int utilSDF_cValVarFromFile(T_SDFVARXML strSDFVarXML,
//							char *cValeur);

int utilSDF_iSaveAndCloseFiles(	char *cRepData,
								T_SDFRECORDXML *strSDFRecordXML,
								unsigned int uiConfig);

int util_iGetIndianSDF(char *nomFic);

int util_iCreateDirDatagram(char *cRepData,
							char *cNomDatagram,
							char *cRepertoire);

#endif /* SDF_UTILITIES_H_ */
