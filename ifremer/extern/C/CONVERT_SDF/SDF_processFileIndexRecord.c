/*
 * SDF_processFileIndexRecord.c
 *
 *  Created on: 22 Feb 2013
 *      Author: rgallou
 */
#pragma pack(1)

#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "SDF_Datagrams.h"
#include "SDF_Utilities.h"
#include "Generic_Utilities.h"
#include "SDF_processFileIndexRecord.h"
#include "convertFiles_SDF.h"

#include "Generic_Utilities.h"

#define NB_SIGNAL		12
#define NB_IMAGE 		0
#define NB_IMAGE2 		0
#define NUM_INDEX_TABLE	-1

//----------------------------------------------------------------
// Fonction :	iProcessFileIndexRecord
// Objet 	:	Traitement du paquet FileIndex
// Modif.	: 	03/11/08
// Auteur 	:	GLU
//----------------------------------------------------------------
int iProcessFileIndexRecord(	FILE *fpFileData,
								int iNbRecord,
								T_FILEINDEX_RECORD_SIGNAL ptrSignalToSave,
								T_SDFRECORDXML *strSDFRecordXML)
{

	int 	iFlag,
			iVarSignal,
			iRetour = 0;

	// On réinitialise errno
	errno = 0;


	// Initialisation de la structure d'identification du paquet
	if (iNbRecord == 0)
	{
		iInitXMLBinFileIndexRecord("FileIndex",
									"FileIndex",
									"Index File of common data", strSDFRecordXML);
	}

	// Sauvegarde des différentes variables du paquet d'index.
	for (iVarSignal=0; iVarSignal<(int)strSDFRecordXML->sNbVarSignal; iVarSignal++)
	{
		if (iNbRecord) {
			strSDFRecordXML->strTabSignalXML[iVarSignal].bFlagConstant = FALSE;
		}
		switch (iVarSignal) {
			case 0 :
					fwrite(	&ptrSignalToSave.NbOfBytesInPage,
								strSDFRecordXML->strTabSignalXML[iVarSignal].usSize,
								strSDFRecordXML->strTabSignalXML[iVarSignal].usNbElem,
								strSDFRecordXML->strTabSignalXML[iVarSignal].ptr_fpBin);
					break;
			case 1 :
					fwrite(	&ptrSignalToSave.PageVersion,
								strSDFRecordXML->strTabSignalXML[iVarSignal].usSize,
								strSDFRecordXML->strTabSignalXML[iVarSignal].usNbElem,
								strSDFRecordXML->strTabSignalXML[iVarSignal].ptr_fpBin);
					break;
			case 2 :
					fwrite(	&ptrSignalToSave.year,
								strSDFRecordXML->strTabSignalXML[iVarSignal].usSize,
								strSDFRecordXML->strTabSignalXML[iVarSignal].usNbElem,
								strSDFRecordXML->strTabSignalXML[iVarSignal].ptr_fpBin);
					break;
			case 3 :
					fwrite(	&ptrSignalToSave.month,
								strSDFRecordXML->strTabSignalXML[iVarSignal].usSize,
								strSDFRecordXML->strTabSignalXML[iVarSignal].usNbElem,
								strSDFRecordXML->strTabSignalXML[iVarSignal].ptr_fpBin);
					break;
			case 4 :
					fwrite(	&ptrSignalToSave.day,
								strSDFRecordXML->strTabSignalXML[iVarSignal].usSize,
								strSDFRecordXML->strTabSignalXML[iVarSignal].usNbElem,
								strSDFRecordXML->strTabSignalXML[iVarSignal].ptr_fpBin);
					break;
			case 5 :
					fwrite(	&ptrSignalToSave.hour,
								strSDFRecordXML->strTabSignalXML[iVarSignal].usSize,
								strSDFRecordXML->strTabSignalXML[iVarSignal].usNbElem,
								strSDFRecordXML->strTabSignalXML[iVarSignal].ptr_fpBin);
					break;
			case 6 :
					fwrite(	&ptrSignalToSave.minute,
								strSDFRecordXML->strTabSignalXML[iVarSignal].usSize,
								strSDFRecordXML->strTabSignalXML[iVarSignal].usNbElem,
								strSDFRecordXML->strTabSignalXML[iVarSignal].ptr_fpBin);
					break;
			case 7 :
					fwrite(	&ptrSignalToSave.second,
								strSDFRecordXML->strTabSignalXML[iVarSignal].usSize,
								strSDFRecordXML->strTabSignalXML[iVarSignal].usNbElem,
								strSDFRecordXML->strTabSignalXML[iVarSignal].ptr_fpBin);
					break;
			case 8 :
					fwrite(	&ptrSignalToSave.hSecond,
								strSDFRecordXML->strTabSignalXML[iVarSignal].usSize,
								strSDFRecordXML->strTabSignalXML[iVarSignal].usNbElem,
								strSDFRecordXML->strTabSignalXML[iVarSignal].ptr_fpBin);
					break;
			case 9 :
					fwrite(	&ptrSignalToSave.FileIndexCounter,
								strSDFRecordXML->strTabSignalXML[iVarSignal].usSize,
								strSDFRecordXML->strTabSignalXML[iVarSignal].usNbElem,
								strSDFRecordXML->strTabSignalXML[iVarSignal].ptr_fpBin);
					break;
			case 10 :
					fwrite(	&ptrSignalToSave.DatagramPosition,
								strSDFRecordXML->strTabSignalXML[iVarSignal].usSize,
								strSDFRecordXML->strTabSignalXML[iVarSignal].usNbElem,
								strSDFRecordXML->strTabSignalXML[iVarSignal].ptr_fpBin);
					break;
			case 11 :
					fwrite(	&ptrSignalToSave.DatagramLength,
								strSDFRecordXML->strTabSignalXML[iVarSignal].usSize,
								strSDFRecordXML->strTabSignalXML[iVarSignal].usNbElem,
								strSDFRecordXML->strTabSignalXML[iVarSignal].ptr_fpBin);
					break;
		}

	}

	return iRetour;

} //iProcessFileIndexRecord


//----------------------------------------------------------------
// Fonction :		iInitXMLBinFileIndexRecord
// Objet 	:		Initialisation de la structure équivalent au paquet
//					FileIndex pour la manipulation des fichiers bin et XML.
// Modification : 	22/02/2013
// Auteur 	: 		GLU
//----------------------------------------------------------------
int iInitXMLBinFileIndexRecord(	char *cTitleDatagram,
								char *cNomDatagram, char *cComments,
								T_SDFRECORDXML *strSDFRecordXML)
{

	int iRet;

	T_DESC_SIGNAL_OR_IMAGE strDescriptionSignal[NB_SIGNAL] = {
								{"NbOfBytesInPage",		"unsigned int",		1.0, 0.0},
								{"TypeOfPage",			"unsigned int",		1.0, 0.0},
								{"year",				"unsigned int", 	1.0, 0.0},
								{"month",				"unsigned int",		1.0, 0.0},
								{"day",					"unsigned int", 	1.0, 0.0},
								{"hour",				"unsigned int", 	1.0, 0.0},
								{"minute",				"unsigned int",		1.0, 0.0},
								{"second",				"unsigned int",		1.0, 0.0},
								{"hSecond",				"unsigned int",		1.0, 0.0},
								{"FileIndexCounter",	"unsigned short",	1.0, 0.0},
								{"DatagramPosition",	"u64",				1.0, 0.0},
								{"DatagramLength",		"int",				1.0, 0.0}};


	T_DESC_SIGNAL_OR_IMAGE strDescriptionImage[NB_IMAGE] 	= {};
	T_DESC_SIGNAL_OR_IMAGE strDescriptionImage2[NB_IMAGE2] 	= {};

	T_DESC_TAB_SIGANDIMG strDescriptionTab = {	NB_SIGNAL,
												NB_IMAGE,
												NB_IMAGE2,
												NUM_INDEX_TABLE,
												strDescriptionSignal,
												strDescriptionImage,
												strDescriptionImage2};

	// Lancement de l'initialisation des structures de signaux : ouvertures des
	// fichiers, copie des structures, ...
	iRet = utilSDF_initXMLRecord(	strDescriptionTab,
									cTitleDatagram,
									cNomDatagram,
									cComments,
									strSDFRecordXML);

	return EXIT_SUCCESS;

} // iInitXMLBinFileIndexRecord

#pragma pack()
