/*
 * SDF_processFileIndexRecord.h
 *
 *  Created on: 21 nov. 2008
 *      Author: rgSDFou
 */
#pragma pack(1)

#ifndef SDF_PROCESSFILEINDEXRECORD_H_
#define SDF_PROCESSFILEINDEXRECORD_H_

#include "SDF_DATAGRAMS.H"
#include <sys/types.h>


typedef struct  {
		unsigned int	NbOfBytesInPage;
		unsigned int	PageVersion;
		unsigned int	year;
		unsigned int	month;
		unsigned int	day;
		unsigned int	hour;
		unsigned int	minute;
		unsigned int	second;
		unsigned int	hSecond;
		unsigned short	FileIndexCounter;
		off64_t 		DatagramPosition;
		int 			DatagramLength;
} T_FILEINDEX_RECORD_SIGNAL;


int iProcessFileIndexRecord(	FILE *fpFileData,
								int iNbRecord,
								T_FILEINDEX_RECORD_SIGNAL ptrSignalToSave,
								T_SDFRECORDXML *strSDFRecordXML);


int iInitXMLBinFileIndexRecord(	char *cTitleDatagram,
								char *cNomDatagram,
								char *cComments,
								T_SDFRECORDXML *strSDFRecordXML);

#endif /* SDF_PROCESSFILEINDEXRECORD_H_ */

#pragma pack()
