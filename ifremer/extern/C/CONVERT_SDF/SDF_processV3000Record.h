/*
 * SDF_processV3000Record.h
 *
 *  Created on: 17 juin 2010
 *      Author: mmorvan
 */
#pragma pack(1)

#ifndef SDF_PROCESSV3000RECORD_H_
#define SDF_PROCESSV3000RECORD_H_

#include "SDF_DATAGRAMS.H"

typedef struct  {
	unsigned short *portlf; 	/* Port low-freq */
	unsigned short *stbdlf;	/* Stbd low-freq */
	unsigned short *porthf; 	/* Port high-freq */
	unsigned short *stbdhf; 	/* Stbd high-freq */
	short *sbp; 				/* Sub-bottom profiler */
} T_V3000_RECORD_IMAGE;

int iProcessV3000Record(FILE *fpFileData, int iNbRecord,
						T_SDFRECORDXML *strSDFRecordXML,
						SDFV3HEADER *ptrSignal_Record,
						SDFV3HEADER *ptrSignal_prevRecord);


int iInitXMLBinV3000Record(	char *cTitleDatagram,
								char *cNomDatagram,
								char *cComments,
								T_SDFRECORDXML *strSDFRecordXML);

#endif /* SDF_PROCESSV3000RECORD_H_ */

#pragma pack()
