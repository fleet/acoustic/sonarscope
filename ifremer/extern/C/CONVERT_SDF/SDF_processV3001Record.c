/*
 * SDF_processV3001Record.c
 *
 *  Created on: 17 juin 2010
 *  Modif on : 05/04/2013
 *      Author: rgallou
 */
#pragma pack(1)

#define NB_SIGNAL 		91
#define NB_IMAGE 		5
#define NUM_INDEX_TABLE	8	// Identification de l'index Signal des Images (� partir de 0).


#include <sys/types.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "SDF_Datagrams.h"
#include "SDF_Utilities.h"
#include "Generic_Utilities.h"
#include "SDF_processV3001Record.h"
#include "convertFiles_SDF.h"


//----------------------------------------------------------------
// Fonction :	iProcessV3001Record
// Objet 	:	Traitement du paquet V3001
// Modif.	: 	17/06/10
// Auteur 	:	MVN
//----------------------------------------------------------------
int iProcessV3001Record(FILE *fpFileData, int iNbRecord,
						T_SDFRECORDXML *strSDFRecordXML,
						SDFV4HEADER *ptrSignal_Record,
						SDFV4HEADER *ptrSignal_prevRecord)
{
	// Pointeur pour avancer sur les variables automatiquement.
	unsigned char		*ptr_cSignalRecord;
	static void 		*ptr_vVarPrevRecord;
	void				*ptr_vVarRecord,
						*ptr_vBidon;

	BOOL		bFlagVarConstant = TRUE,
				bFlagCompare = TRUE;

    int 	iFlag,
			iVarSignal,
			iVarImage,
			iRet = 0,
			iLoop;

	//static int		iNbRecord = 0;

    unsigned short	usTailleEnByte;
    unsigned int	uiNbElem;
    unsigned int 	uiConfig = ptrSignal_Record->sdfV3header.configuration;

    // Initialisation de la structure d'identification du paquet
 	if (iNbRecord == 0)
	{
 		iInitXMLBinV3001Record(	"V3001",
								"V3001",
								"V3001",
								strSDFRecordXML);
	}

    // Lecture des signaux du paquet
	ptr_cSignalRecord = (unsigned char*)ptrSignal_Record;

	// Sauvegarde des donn�es des attributs.
	if (iNbRecord > 0)
	{
		ptr_vVarPrevRecord = (void *)ptrSignal_prevRecord;
	}

	for (iVarSignal=0; iVarSignal<(int)strSDFRecordXML->sNbVarSignal; iVarSignal++)
	{
		if(iVarSignal == 1)
			ptr_cSignalRecord += sizeof(ptrSignal_prevRecord->sdfV3header.pageVersion) + sizeof(ptrSignal_prevRecord->sdfV3header.configuration);//pageversion et configuration ne sont pas sauvegard�es;

		ptr_vVarRecord = (void *)ptr_cSignalRecord;
		if (iVarSignal==0)
		{
			usTailleEnByte = 0;
			uiNbElem = 0;
		}
		else
		{
			usTailleEnByte = util_usNbOctetTypeVar(strSDFRecordXML->strTabSignalXML[iVarSignal-1].cType);
			uiNbElem = strSDFRecordXML->strTabSignalXML[iVarSignal-1].usNbElem;
		}
		if(iVarSignal == 1)
			ptr_vVarPrevRecord = ptr_vVarPrevRecord + sizeof(ptrSignal_prevRecord->sdfV3header.pageVersion) + sizeof(ptrSignal_prevRecord->sdfV3header.configuration);//pageversion et configuration ne sont pas sauvegard�es
		// Pointage sur la donn�e suivante.
		ptr_vVarPrevRecord = ptr_vVarPrevRecord + usTailleEnByte*uiNbElem;
		// On ne fait pas le test de constance au 1er enregistrement ou si le signal
		// est d�j� vue comme variable.
		if (iNbRecord > 0 && strSDFRecordXML->strTabSignalXML[iVarSignal].bFlagConstant == TRUE)
		{
			bFlagVarConstant = strSDFRecordXML->strTabSignalXML[iVarSignal].bFlagConstant;
			if (G_iFlagEndian == 0) // On inverse en Big Endian.
			{
				// Observation de la taille propre � la variable.
				usTailleEnByte = util_usNbOctetTypeVar(strSDFRecordXML->strTabSignalXML[iVarSignal].cType);
				uiNbElem = strSDFRecordXML->strTabSignalXML[iVarSignal].usNbElem;
				if (usTailleEnByte == 2) // Pour des (u)short, (u)int, (u)long.
					ptr_vVarPrevRecord = (void *)util_cBswap_16(ptr_vVarPrevRecord);
				else if (usTailleEnByte == 4)
					ptr_vVarPrevRecord = (void *)util_cBswap_32(ptr_vVarPrevRecord);
				else if (usTailleEnByte == 8)
					ptr_vVarPrevRecord = (void *)util_cBswap_64(ptr_vVarPrevRecord);
			}
			bFlagCompare = util_bComparePrevAndCurrentVal(strSDFRecordXML->strTabSignalXML[iVarSignal].cType,
												ptr_vVarPrevRecord,
												ptr_vVarRecord,
											   strSDFRecordXML->strTabSignalXML[iVarSignal].usNbElem);
			bFlagVarConstant = bFlagVarConstant && bFlagCompare;
			strSDFRecordXML->strTabSignalXML[iVarSignal].bFlagConstant = bFlagVarConstant;
		}


		// Pr�paration de l'�criture de la variable.
		usTailleEnByte = strSDFRecordXML->strTabSignalXML[iVarSignal].usSize;
		uiNbElem =  strSDFRecordXML->strTabSignalXML[iVarSignal].usNbElem;
		if (G_iFlagEndian == 0) // On inverse en Big Endian.
		{
			if (usTailleEnByte == 2) // Pour des (u)short, (u)int, (u)long.
				ptr_cSignalRecord = (unsigned char*)util_cBswap_16((char*)ptr_cSignalRecord);
			else if (usTailleEnByte == 4)
				ptr_cSignalRecord = (unsigned char*)util_cBswap_32((char*)ptr_cSignalRecord);
			else if (usTailleEnByte == 8)
				ptr_cSignalRecord = (unsigned char*)util_cBswap_64((char*)ptr_cSignalRecord);
		}
		fwrite(ptr_cSignalRecord, usTailleEnByte, uiNbElem, strSDFRecordXML->strTabSignalXML[iVarSignal].ptr_fpBin);
		strSDFRecordXML->strTabSignalXML[iVarSignal].ptr_vPrevVal = ptr_cSignalRecord;
		ptr_cSignalRecord += usTailleEnByte*uiNbElem;
	}
	// Recopie en zone tampon des valeurs courantes.
	memmove(ptrSignal_prevRecord, ptrSignal_Record, sizeof(SDFV3HEADER));

	// --------------------------------------------
 	// Lecture unitaire des variables de type Image
	// Lecture unitaire des variables.
	for (iVarImage=0; iVarImage< strSDFRecordXML->sNbVarImage; iVarImage++)
	{
		usTailleEnByte = strSDFRecordXML->strTabImageXML[iVarImage].usSize;

		// Analyse des configs pour observer la pr�sence ou l'absence de l'image.
		if(iVarImage==0 || iVarImage==1)
			if((uiConfig&0x03)!=0x03)
				break;

		if(iVarImage==2 || iVarImage==3)
			if((uiConfig&0x0C)!=0x0C)
				break;

		if(iVarImage==4)
			if((uiConfig&0x10)!=0x10)
				break;

		if(iVarImage < strSDFRecordXML->sNbVarImage-1)
			//lecture des 2 bytes contenant le nb de samples de type unsigned short
			fread(&uiNbElem, usTailleEnByte, 1, fpFileData);
		else
			//lecture des 4 bytes contenant le nb de samples de type Long
			fread(&uiNbElem, usTailleEnByte, 1, fpFileData);


		ptr_vBidon = (void *)malloc((usTailleEnByte*uiNbElem)*sizeof(char));
		iFlag = fread(ptr_vBidon, usTailleEnByte, uiNbElem, fpFileData);


		if (G_iFlagEndian == 0) // On inverse en Big Endian.
		{
			ptr_vVarRecord = ptr_vBidon;
			for (iLoop=0; iLoop<uiNbElem; iLoop++)
			{
				// Pour des (u)short, (u)int, (u)long.
				if (usTailleEnByte == 2)
					ptr_vVarRecord = (void *)util_cBswap_16(ptr_vVarRecord);
				else if (usTailleEnByte == 4)
					ptr_vVarRecord = (void *)util_cBswap_16(ptr_vVarRecord);
				else if (usTailleEnByte == 8)
					ptr_vVarRecord = (void *)util_cBswap_16(ptr_vVarRecord);

				ptr_vVarRecord += usTailleEnByte;
			}
		}
		iFlag = fwrite(ptr_vBidon, usTailleEnByte, uiNbElem, strSDFRecordXML->strTabImageXML[iVarImage].ptr_fpBin);
		if (iNbRecord == 0)
		{
			strSDFRecordXML->strTabImageXML[iVarImage].bFlagConstant = FALSE;
		}

		free(ptr_vBidon);

	} // Fin de la boucles sur les variables.

	iNbRecord++;

	return iRet;

} //iProcessV3001Record


//----------------------------------------------------------------
// Fonction :	iInitXMLBinV3001Record
// Objet 	:	Initialisation de la structure �quivalent au paquet
//				V3001 pour la manipulation des fichiers bin et XML.
// Modification : 19/10/2011
// Auteur 	: GLU
//----------------------------------------------------------------
int iInitXMLBinV3001Record(	char *cTitleDatagram,
								char *cNomDatagram,
								char *cComments,
								T_SDFRECORDXML *strSDFRecordXML)
{
	int iRet;

	T_DESC_SIGNAL_OR_IMAGE strDescriptionSignal[NB_SIGNAL] = {
			{"numberBytes"			,"unsigned int",	1.0,	0.0	},
			{"pingNumber"			,"unsigned int",	1.0,	0.0	},
			{"numSamples"			,"unsigned int",	1.0,	0.0	},
			{"beamsToDisplay"		,"unsigned int",	1.0,	0.0	},
			{"errorFlags"			,"unsigned int",	1.0,	0.0	},
			{"range"				,"unsigned int",	1.0,	0.0	},
			{"speedFish"			,"unsigned int",	1.0,	0.0	},
			{"speedSound"			,"unsigned int",	1.0,	0.0	},
			{"resMode"				,"unsigned int",	1.0,	0.0	},
			{"txWaveform"			,"unsigned int",	1.0,	0.0	},
			{"respDiv"				,"unsigned int",	1.0,	0.0	},
			{"respFreq"				,"unsigned int",	1.0,	0.0	},
			{"manualSpeedSwitch"	,"unsigned int",	1.0,	0.0	},
			{"despeckleSwitch"		,"unsigned int",	1.0,	0.0	},
			{"speedFilterSwitch"	,"unsigned int",	1.0,	0.0	},
			{"year"					,"unsigned int",	1.0,	0.0	},
			{"month"				,"unsigned int",	1.0,	0.0	},
			{"day"					,"unsigned int",	1.0,	0.0	},
			{"hour"					,"unsigned int",	1.0,	0.0	},
			{"minute"				,"unsigned int",	1.0,	0.0	},
			{"second"				,"unsigned int",	1.0,	0.0	},
			{"hSecond"				,"unsigned int",	1.0,	0.0	},
			{"fixTimeHour"			,"unsigned int",	1.0,	0.0	},
			{"fixTimeMinute"		,"unsigned int",	1.0,	0.0	},
			{"fixTimeSecond"		,"float",			1.0,	0.0	},
			{"heading"				,"float",			1.0,	0.0	},
			{"pitch"				,"float",			1.0,	0.0	},
			{"roll"					,"float",			1.0,	0.0	},
			{"depth"				,"float",			1.0,	0.0	},
			{"altitude"				,"float",			1.0,	0.0	},
			{"temperature"			,"float",			1.0,	0.0	},
			{"speed"				,"float",			1.0,	0.0	},
			{"shipHeading"			,"float",			1.0,	0.0	},
			{"magneticVariation"	,"float",			1.0,	0.0	},
			{"shipLat"				,"f64",				1.0,	0.0	},
			{"shipLon"				,"f64",				1.0,	0.0	},
			{"fishLat"				,"f64",				1.0,	0.0	},
			{"fishLon"				,"f64",				1.0,	0.0	},
			{"tvgPage"				,"unsigned int",	1.0,	0.0	},
			{"headerSize"			,"unsigned int",	1.0,	0.0	},
			{"fixTimeYear"			,"unsigned int",	1.0,	0.0	},
			{"fixTimeMonth"			,"unsigned int",	1.0,	0.0	},
			{"fixTimeDay"			,"unsigned int",	1.0,	0.0	},
			{"auxPitch"				,"float",			1.0,	0.0	},
			{"auxRoll"				,"float",			1.0,	0.0	},
			{"auxDepth"				,"float",			1.0,	0.0	},
			{"auxAlt"				,"float",			1.0,	0.0	},
			{"cableOut"				,"float",			1.0,	0.0	},
			{"fseconds"				,"float",			1.0,	0.0	},
			{"altimeter"			,"unsigned int",	1.0,	0.0	},
			{"sampleFreq"			,"unsigned int",	1.0,	0.0	},
			{"depressorType"		,"unsigned int",	1.0,	0.0	},
			{"cableType"			,"unsigned int",	1.0,	0.0	},
			{"shieveXoff"			,"float",			1.0,	0.0	},
			{"shieveYoff"			,"float",			1.0,	0.0	},
			{"shieveZoff"			,"float",			1.0,	0.0	},
			{"GPSheight"			,"float",			1.0,	0.0	},
			{"rawDataConfig"		,"unsigned int",	1.0,	0.0	},
			{"header3ExtensionSize","unsigned int",		1.0,	0.0	},
			{"sbpTxWaveform"		,"unsigned int",	1.0,	0.0	},
			{"sbpPreAmpGain"		,"unsigned int",	1.0,	0.0	},
			{"sbpDataRaw"			,"unsigned int",	1.0,	0.0	},
			{"sbpNumSamples"		,"unsigned int",	1.0,	0.0	},
			{"sbpSampleFreq"		,"unsigned int",	1.0,	0.0	},
			{"sbpTxWaveformVersion"	,"unsigned int",	1.0,	0.0	},
			{"wingAngle"			,"float",			1.0,	0.0	},
			{"emergencySwitchState"	,"unsigned int",	1.0,	0.0	},
			{"laybackMethod"		,"unsigned int",	1.0,	0.0	},
			{"laybackFishLat"		,"double",			1.0,	0.0	},
			{"laybackFishLon"		,"double",			1.0,	0.0	},
			{"fishHeadingOffset"	,"float",			1.0,	0.0	},
			{"pressureSensorOffset"	,"float",			1.0,	0.0	},
			{"tpuSwVersion"			,"unsigned int",	1.0,	0.0	},
			{"capabilityMask"		,"unsigned int",	1.0,	0.0	},
			{"txVersion"			,"unsigned int",	1.0,	0.0	},
			{"numSamplesExtra"		,"unsigned int",	1.0,	0.0	},
			{"postProcessVersion"	,"unsigned int",	1.0,	0.0	},
			{"motionSensorType"		,"unsigned short",	1.0,	0.0	},
			{"pingTimeRefCount"		,"unsigned short",	1.0,	0.0	},
			{"extTrigTimeRefCount"	,"unsigned short",	1.0,	0.0	},
			{"onePpsTimeRefCount"	,"unsigned short",	1.0,	0.0	},
			{"timeRefCountWeight"	,"unsigned int",	1.0,	0.0	},
			{"altitudeBathy "		,"float",			1.0,	0.0	},
			{"pingInterval "		,"float",			1.0,	0.0	},
			{"sdfExtensionSize"		,"unsigned int",	1.0,	0.0	},
			{"secondsOfWeek"		,"double",			1.0,	0.0	},
			{"speedSoundSource"		,"unsigned int",	1.0,	0.0	},
			{"pressureSensorMax"	,"float",			1.0,	0.0	},
			{"pressureSensorVoltageMin"	,"float",		1.0,	0.0	},
			{"pressureSensorVoltageMax"	,"float",		1.0,	0.0	},
			{"processedPingNumber"	,"unsigned int",	1.0,	0.0	}};

	T_DESC_SIGNAL_OR_IMAGE strDescriptionImage[NB_IMAGE] = {
			{"portlf",	"unsigned short", 	1.0, 0.0},
			{"stbdlf",	"unsigned short", 	1.0, 0.0},
			{"porthf",	"unsigned short", 	1.0, 0.0},
			{"stbdhf",	"unsigned short", 	1.0, 0.0},
			{"sbp",		"long", 			1.0, 0.0}};


	T_DESC_SIGNAL_OR_IMAGE strDescriptionImage2[0] = {};

	T_DESC_TAB_SIGANDIMG strDescriptionTab = {	NB_SIGNAL,
												NB_IMAGE,
												0,
												NUM_INDEX_TABLE,
												strDescriptionSignal,
												strDescriptionImage,
												strDescriptionImage2};


	// Lancement de l'initialisation des structures de signaux : ouvertures des
	// fichiers, copie des structures, ...
	iRet = utilSDF_initXMLRecord(	strDescriptionTab,
									cTitleDatagram,
									cNomDatagram,
									cComments,
									strSDFRecordXML);

	return iRet;

} // iInitXMLBinV3001Record

#pragma pack()
