/*
 * SDF_processV3001Record.h
 *
 *  Created on: 17 juin 2010
 *      Author: mmorvan
 */
#pragma pack(1)

#ifndef SDF_PROCESSV3001RECORD_H_
#define SDF_PROCESSV3001RECORD_H_

#include "SDF_DATAGRAMS.H"

typedef struct  {
	unsigned short *portlf; 	// Port low-freq
	unsigned short *stbdlf;		// Stbd low-freq
	unsigned short *porthf; 	// Port high-freq
	unsigned short *stbdhf; 	// Stbd high-freq
	long *sbp; 					// Sub-bottom profiler
} T_V3001_RECORD_IMAGE;

int iProcessV3001Record(FILE *fpFileData, int iNbRecord,
						T_SDFRECORDXML *strSDFRecordXML,
						SDFV4HEADER *ptrSignal_Record,
						SDFV4HEADER *ptrSignal_prevRecord);


int iInitXMLBinV3001Record(	char *cTitleDatagram,
								char *cNomDatagram,
								char *cComments,
								T_SDFRECORDXML *strSDFRecordXML);

#endif // SDF_PROCESSV3001RECORD_H_

#pragma pack()
