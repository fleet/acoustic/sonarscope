/*
 * SDF_processV5000Record.h
 *
 *  Created on: 17 juin 2010
 *      Author: mmorvan
 */
#pragma pack(1)

#ifndef SDF_PROCESSV5000RECORD_H_
#define SDF_PROCESSV5000RECORD_H_

#include "SDF_DATAGRAMS.H"

typedef struct  {
	unsigned short *chan1Data; /* processed side-scan data */
	unsigned short *chan2Data;
	unsigned short *chan3Data;
	unsigned short *chan4Data;
	unsigned short *chan5Data;
	unsigned short *chan6Data;
	unsigned short *chan7Data;
	unsigned short *chan8Data;
	unsigned short *chan9Data;
	unsigned short *chan10Data;
	short *bathyPort1i; /* Bathy 1 synthesized from */
	short *bathyPort1q; /* element data */
	short *bathyPort2i; /* Bathy 2,3 raw from arrays */
	short *bathyPort2q;
	short *bathyPort3i;
	short *bathyPort3q;
	short *bathyStbd1i;
	short *bathyStbd1q;
	short *bathyStbd2i;
	short *bathyStbd2q;
	short *bathyStbd3i;
	short *bathyStbd3q;
	short *echo1;
	short *echo2;
	short *subBottom1;
	short *subBottom2;
	short *rollSensor;
	short *yawRate;
	short *rawdataPort1i; /* 1-12 = port raw side-scan */
	short *rawdataPort1q;
	short *rawdataPort2i;
	short *rawdataPort2q;
	short *rawdataPort3i;
	short *rawdataPort3q;
	short *rawdataPort4i;
	short *rawdataPort4q;
	short *rawdataPort5i;
	short *rawdataPort5q;
	short *rawdataPort6i;
	short *rawdataPort6q;
	short *rawdataPort7i;
	short *rawdataPort7q;
	short *rawdataPort8i;
	short *rawdataPort8q;
	short *rawdataPort9i;
	short *rawdataPort9q;
	short *rawdataPort10i;
	short *rawdataPort10q;
	short *rawdataPort11i;
	short *rawdataPort11q;
	short *rawdataPort12i;
	short *rawdataPort12q;
	short *rawdataPort13i; /* bathy string #2 */
	short *rawdataPort13q;
	short *rawdataPort14i;
	short *rawdataPort14q; /* bathy string #3 */
	short *rawdataStbd1i; /* 1-12 = starboard raw side-scan */
	short *rawdataStbd1q;
	short *rawdataStbd2i;
	short *rawdataStbd2q;
	short *rawdataStbd3i;
	short *rawdataStbd3q;
	short *rawdataStbd4i;
	short *rawdataStbd4q;
	short *rawdataStbd5i;
	short *rawdataStbd5q;
	short *rawdataStbd6i;
	short *rawdataStbd6q;
	short *rawdataStbd7i;
	short *rawdataStbd7q;
	short *rawdataStbd8i;
	short *rawdataStbd8q;
	short *rawdataStbd9i;
	short *rawdataStbd9q;
	short *rawdataStbd10i;
	short *rawdataStbd10q;
	short *rawdataStbd11i;
	short *rawdataStbd11q;
	short *rawdataStbd12i;
	short *rawdataStbd12q;
	short *rawdataStbd13i;
	short *rawdataStbd13q;
	short *rawdataStbd14i;
	short *rawdataStbd14q;
} T_V5000_RECORD_IMAGE;


int iProcessV5000Record(FILE *fpFileData, int iNbRecord,
						T_SDFRECORDXML *strSDFRecordXML,
						SDFV3HEADER *ptrSignal_Record,
						SDFV3HEADER *ptrSignal_prevRecord);


int iInitXMLBinV5000Record(	char *cTitleDatagram,
								char *cNomDatagram,
								char *cComments,
								T_SDFRECORDXML *strSDFRecordXML);

#endif /* SDF_PROCESSV5000RECORD_H_ */

#pragma pack()
