/*
 * SDF_processV5001Record.c
 *
 *  Created on: 17 juin 2010
 *      Author: mmorvan
 */
#pragma pack(1)

#include <sys/types.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "SDF_Datagrams.h"
#include "SDF_Utilities.h"
#include "Generic_Utilities.h"
#include "SDF_processV5001Record.h"
#include "convertFiles_SDF.h"


//----------------------------------------------------------------
// Fonction :	iProcessV5001Record
// Objet 	:	Traitement du paquet V5001
// Modif.	: 	17/06/10
// Auteur 	:	MVN
//----------------------------------------------------------------
int iProcessV5001Record(FILE *fpFileData, int iNbRecord,
						T_SDFRECORDXML *strSDFRecordXML,
						SDFV4HEADER *ptrSignal_Record,
						SDFV4HEADER *ptrSignal_prevRecord)
{
	// Pointeur pour avancer sur les variables automatiquement.
	unsigned char		*ptr_cSignalRecord;
	static void 		*ptr_vVarPrevRecord;
	void				*ptr_vVarRecord,
						*ptr_vBidon;

	BOOL		bFlagVarConstant = TRUE,
				bFlagCompare = TRUE;

    int 	iFlag,
			iVarSignal,
			iVarImage,
			iRet = 0,
			iLoop;

    unsigned short	usTailleEnByte,
					usNbElem;

    // Initialisation de la structure d'identification du paquet
 	if (iNbRecord == 0)
	{
 		iInitXMLBinV5001Record(	"V5001",
								"V5001",
								"V5001",
								strSDFRecordXML);
	}

    // Lecture des signaux du paquet
	ptr_cSignalRecord = (unsigned char*)ptrSignal_Record;

	// Sauvegarde des donn�es des attributs.
	if (iNbRecord > 0)
	{
		ptr_vVarPrevRecord = (void *)ptrSignal_prevRecord;
	}

	for (iVarSignal=0; iVarSignal<(int)strSDFRecordXML->sNbVarSignal; iVarSignal++)
	{
		if(iVarSignal == 1)
			ptr_cSignalRecord += sizeof(ptrSignal_prevRecord->sdfV3header.pageVersion) + sizeof(ptrSignal_prevRecord->sdfV3header.configuration);//pageversion et configuration ne sont pas sauvegard�es;

		ptr_vVarRecord = (void *)ptr_cSignalRecord;
		if (iVarSignal==0)
		{
			usTailleEnByte = 0;
			usNbElem = 0;
		}
		else
		{
			usTailleEnByte = util_usNbOctetTypeVar(strSDFRecordXML->strTabSignalXML[iVarSignal-1].cType);
			usNbElem = strSDFRecordXML->strTabSignalXML[iVarSignal-1].usNbElem;
		}
		if(iVarSignal == 1)
			ptr_vVarPrevRecord = ptr_vVarPrevRecord + sizeof(ptrSignal_prevRecord->sdfV3header.pageVersion) + sizeof(ptrSignal_prevRecord->sdfV3header.configuration);//pageversion et configuration ne sont pas sauvegard�es
		// Pointage sur la donn�e suivante.
		ptr_vVarPrevRecord = ptr_vVarPrevRecord + usTailleEnByte*usNbElem;
		// On ne fait pas le test de constance au 1er enregistrement ou si le signal
		// est d�j� vue comme variable.
		if (iNbRecord > 0 && strSDFRecordXML->strTabSignalXML[iVarSignal].bFlagConstant == TRUE)
		{
			bFlagVarConstant = strSDFRecordXML->strTabSignalXML[iVarSignal].bFlagConstant;
			if (G_iFlagEndian == 0) // On inverse en Big Endian.
			{
				// Observation de la taille propre � la variable.
				usTailleEnByte = util_usNbOctetTypeVar(strSDFRecordXML->strTabSignalXML[iVarSignal].cType);
				usNbElem = strSDFRecordXML->strTabSignalXML[iVarSignal].usNbElem;
				if (usTailleEnByte == 2) // Pour des (u)short, (u)int, (u)long.
					ptr_vVarPrevRecord = (void *)util_cBswap_16(ptr_vVarPrevRecord);
				else if (usTailleEnByte == 4)
					ptr_vVarPrevRecord = (void *)util_cBswap_32(ptr_vVarPrevRecord);
				else if (usTailleEnByte == 8)
					ptr_vVarPrevRecord = (void *)util_cBswap_64(ptr_vVarPrevRecord);
			}
			bFlagCompare = util_bComparePrevAndCurrentVal(strSDFRecordXML->strTabSignalXML[iVarSignal].cType,
												ptr_vVarPrevRecord,
												ptr_vVarRecord,
											   strSDFRecordXML->strTabSignalXML[iVarSignal].usNbElem);
			bFlagVarConstant = bFlagVarConstant && bFlagCompare;
			strSDFRecordXML->strTabSignalXML[iVarSignal].bFlagConstant = bFlagVarConstant;
		}


		// Pr�paration de l'�criture de la variable.
		usTailleEnByte = strSDFRecordXML->strTabSignalXML[iVarSignal].usSize;
		usNbElem =  strSDFRecordXML->strTabSignalXML[iVarSignal].usNbElem;
		if (G_iFlagEndian == 0) // On inverse en Big Endian.
		{
			if (usTailleEnByte == 2) // Pour des (u)short, (u)int, (u)long.
				ptr_cSignalRecord = (unsigned char*)util_cBswap_16((char*)ptr_cSignalRecord);
			else if (usTailleEnByte == 4)
				ptr_cSignalRecord = (unsigned char*)util_cBswap_32((char*)ptr_cSignalRecord);
			else if (usTailleEnByte == 8)
				ptr_cSignalRecord = (unsigned char*)util_cBswap_64((char*)ptr_cSignalRecord);
		}
		fwrite(ptr_cSignalRecord, usTailleEnByte, usNbElem, strSDFRecordXML->strTabSignalXML[iVarSignal].ptr_fpBin);
		//		strSDFRecordXML->strTabSignalXML[iVarSignal].ptr_vPrevVal = ptr_cSignalRecord;
		strSDFRecordXML->strTabSignalXML[iVarSignal].ptr_vPrevVal = ptr_cSignalRecord;
		ptr_cSignalRecord += usTailleEnByte*usNbElem;
	}
	// Recopie en zone tampon des valeurs courantes.
	memmove(ptrSignal_prevRecord, ptrSignal_Record, sizeof(SDFV3HEADER));

	// --------------------------------------------
 	// Lecture unitaire des variables de type Image
	// Lecture unitaire des variables.
	for (iVarImage=0; iVarImage< strSDFRecordXML->sNbVarImage; iVarImage++)
	{
		usTailleEnByte = strSDFRecordXML->strTabImageXML[iVarImage].usSize;
		//usNbElem = strSDFRecordXML->strTabImageXML[iVarImage].usNbElem;
		fread(&usNbElem, usTailleEnByte, 1, fpFileData);//lecture des 2 bytes contenant le nb de samples
		ptr_vBidon = (void *)malloc((usTailleEnByte*usNbElem)*sizeof(char));
		iFlag = fread(ptr_vBidon, usTailleEnByte, usNbElem, fpFileData);
		if (G_iFlagEndian == 0) // On inverse en Big Endian.
		{
			ptr_vVarRecord = ptr_vBidon;
			for (iLoop=0; iLoop<usNbElem; iLoop++)
			{
				// Pour des (u)short, (u)int, (u)long.
				if (usTailleEnByte == 2)
					ptr_vVarRecord = (void *)util_cBswap_16(ptr_vVarRecord);
				else if (usTailleEnByte == 4)
					ptr_vVarRecord = (void *)util_cBswap_16(ptr_vVarRecord);
				else if (usTailleEnByte == 8)
					ptr_vVarRecord = (void *)util_cBswap_16(ptr_vVarRecord);

				ptr_vVarRecord += usTailleEnByte;
			}
		}
		iFlag = fwrite(ptr_vBidon, usTailleEnByte, usNbElem, strSDFRecordXML->strTabImageXML[iVarImage].ptr_fpBin);
		if (iNbRecord == 0)
		{
			strSDFRecordXML->strTabImageXML[iVarImage].bFlagConstant = FALSE;
		}
		memmove(strSDFRecordXML->strTabImageXML[iVarImage].ptr_vPrevVal, ptr_vBidon, usTailleEnByte*usNbElem);

		free(ptr_vBidon);

	} // Fin de la boucles sur les variables.

	iNbRecord++;

	return iRet;

} //iProcessV5001Record


//----------------------------------------------------------------
// Fonction :	iInitXMLBinV5001Record
// Objet 	:	Initialisation de la structure �quivalent au paquet
//				V5001 pour la manipulation des fichiers bin et XML.
// Modification : 17/06/10
// Auteur 	: MVN
//----------------------------------------------------------------
int iInitXMLBinV5001Record(	char *cTitleDatagram,
								char *cNomDatagram,
								char *cComments,
								T_SDFRECORDXML *strSDFRecordXML)
{

	// Initialisation des structures de pilotage des fichiers
	// XML et Bin .
	strSDFRecordXML->sNbVarSignal = 58;
	strSDFRecordXML->sNbVarImage  = 84;

	char cTabNomVarSignal[58][30] = {
			"numberBytes",
			"pingNumber",
			"numSamples",
			"beamsToDisplay",
			"errorFlags",
			"range",
			"speedFish",
			"speedSound",
			"resMode",
			"txWaveform",
			"respDiv",
			"respFreq",
			"manualSpeedSwitch",
			"despeckleSwitch",
			"speedFilterSwitch",
			"year",
			"month",
			"day",
			"hour",
			"minute",
			"second",
			"hSecond",
			"fixTimeHour",
			"fixTimeMinute",
			"fixTimeSecond",
			"heading",
			"pitch",
			"roll",
			"depth",
			"altitude",
			"temperature",
			"speed",
			"shipHeading",
			"magneticVariation",
			"shipLat",
			"shipLon",
			"fishLat",
			"fishLon",
			"tvgPage",
			"headerSize",
			"fixTimeYear",
			"fixTimeMonth",
			"fixTimeDay",
			"auxPitch",
			"auxRoll",
			"auxDepth",
			"auxAlt",
			"cableOut",
			"fseconds",
			"altimeter",
			"sampleFreq",
			"depressorType",
			"cableType",
			"shieveXoff",
			"shieveYoff",
			"shieveZoff",
			"GPSheight",
			"rawDataConfig"};

	char cTabNomTypeSignal[58][20] = {
			"unsigned int",
			"unsigned int",
			"unsigned int",
			"unsigned int",
			"unsigned int",
			"unsigned int",
			"unsigned int",
			"unsigned int",
			"unsigned int",
			"unsigned int",
			"unsigned int",
			"unsigned int",
			"unsigned int",
			"unsigned int",
			"unsigned int",
			"unsigned int",
			"unsigned int",
			"unsigned int",
			"unsigned int",
			"unsigned int",
			"unsigned int",
			"unsigned int",
			"unsigned int",
			"unsigned int",
			"float",
			"float",
			"float",
			"float",
			"float",
			"float",
			"float",
			"float",
			"float",
			"float",
			"f64",
			"f64",
			"f64",
			"f64",
			"unsigned int",
			"unsigned int",
			"unsigned int",
			"unsigned int",
			"unsigned int",
			"float",
			"float",
			"float",
			"float",
			"float",
			"float",
			"unsigned int",
			"unsigned int",
			"unsigned int",
			"unsigned int",
			"float",
			"float",
			"float",
			"float",
			"unsigned int"};

	// ScaleFactor
	float 	fTabSFSignal[58] = {
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0};

	// AddOffset
	float 	fTabAOSignal[58] = {
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0};

	char cTabNomVarImage[84][30] = {
			"chan1Data",
			"chan2Data",
			"chan3Data",
			"chan4Data",
			"chan5Data",
			"chan6Data",
			"chan7Data",
			"chan8Data",
			"chan9Data",
			"chan10Data",
			"bathyPort1i",
			"bathyPort1q",
			"bathyPort2i",
			"bathyPort2q",
			"bathyPort3i",
			"bathyPort3q",
			"bathyStbd1i",
			"bathyStbd1q",
			"bathyStbd2i",
			"bathyStbd2q",
			"bathyStbd3i",
			"bathyStbd3q",
			"echo1",
			"echo2",
			"subBottom1",
			"subBottom2",
			"rollSensor",
			"yawRate",
			"rawdataPort1i",
			"rawdataPort1q",
			"rawdataPort2i",
			"rawdataPort2q",
			"rawdataPort3i",
			"rawdataPort3q",
			"rawdataPort4i",
			"rawdataPort4q",
			"rawdataPort5i",
			"rawdataPort5q",
			"rawdataPort6i",
			"rawdataPort6q",
			"rawdataPort7i",
			"rawdataPort7q",
			"rawdataPort8i",
			"rawdataPort8q",
			"rawdataPort9i",
			"rawdataPort9q",
			"rawdataPort10i",
			"rawdataPort10q",
			"rawdataPort11i",
			"rawdataPort11q",
			"rawdataPort12i",
			"rawdataPort12q",
			"rawdataPort13i",
			"rawdataPort13q",
			"rawdataPort14i",
			"rawdataPort14q",
			"rawdataStbd1i",
			"rawdataStbd1q",
			"rawdataStbd2i",
			"rawdataStbd2q",
			"rawdataStbd3i",
			"rawdataStbd3q",
			"rawdataStbd4i",
			"rawdataStbd4q",
			"rawdataStbd5i",
			"rawdataStbd5q",
			"rawdataStbd6i",
			"rawdataStbd6q",
			"rawdataStbd7i",
			"rawdataStbd7q",
			"rawdataStbd8i",
			"rawdataStbd8q",
			"rawdataStbd9i",
			"rawdataStbd9q",
			"rawdataStbd10i",
			"rawdataStbd10q",
			"rawdataStbd11i",
			"rawdataStbd11q",
			"rawdataStbd12i",
			"rawdataStbd12q",
			"rawdataStbd13i",
			"rawdataStbd13q",
			"rawdataStbd14i",
			"rawdataStbd14q"};

	char cTabNomTypeImage[84][20] = {
			"unsigned short",
			"unsigned short",
			"unsigned short",
			"unsigned short",
			"unsigned short",
			"unsigned short",
			"unsigned short",
			"unsigned short",
			"unsigned short",
			"unsigned short",
			"short",
			"short",
			"short",
			"short",
			"short",
			"short",
			"short",
			"short",
			"short",
			"short",
			"short",
			"short",
			"short",
			"short",
			"short",
			"short",
			"short",
			"short",
			"short",
			"short",
			"short",
			"short",
			"short",
			"short",
			"short",
			"short",
			"short",
			"short",
			"short",
			"short",
			"short",
			"short",
			"short",
			"short",
			"short",
			"short",
			"short",
			"short",
			"short",
			"short",
			"short",
			"short",
			"short",
			"short",
			"short",
			"short",
			"short",
			"short",
			"short",
			"short",
			"short",
			"short",
			"short",
			"short",
			"short",
			"short",
			"short",
			"short",
			"short",
			"short",
			"short",
			"short",
			"short",
			"short",
			"short",
			"short",
			"short",
			"short",
			"short",
			"short",
			"short",
			"short",
			"short",
			"short"};

	// ScaleFactor
	float 	fTabSFImage[84] = {
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0};

	// AddOffset
	float 	fTabAOImage[84] = {
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0};


	char	*ptr_cRepertoire,
			cNomFicPathBin[300];

    int 	iSizeRep,
			iRet,
			iLen,
			iVarSignal,
			iVarImage;

	unsigned short usTaille;

	// Initialisation des structures de pilotage des fichiers
	// XML et Bin .
    strSDFRecordXML->strTabSignalXML= (T_SDFVARXML *)malloc(strSDFRecordXML->sNbVarSignal*sizeof(T_SDFVARXML));
    strSDFRecordXML->strTabImageXML= (T_SDFVARXML *)malloc(strSDFRecordXML->sNbVarImage*sizeof(T_SDFVARXML));

	iLen = strlen(cNomDatagram)+1;
	strSDFRecordXML->ptr_cNomHeader = malloc(iLen*sizeof(char));
	sprintf(strSDFRecordXML->ptr_cNomHeader, "%s", cNomDatagram);

	iLen = strlen(cTitleDatagram)+1;
	strSDFRecordXML->ptr_cLabelHeader = malloc(iLen*sizeof(char));
	sprintf(strSDFRecordXML->ptr_cLabelHeader, "%s", cTitleDatagram);

	iLen = strlen(cComments)+1;
	strSDFRecordXML->ptr_cCommentsHeader = malloc(iLen*sizeof(char));
	sprintf(strSDFRecordXML->ptr_cCommentsHeader, "%s", cComments);

	// Remplissage du nom de la variable d'indexation des tables.
	iLen = strlen(cTabNomVarSignal[8])+1;
	strSDFRecordXML->ptr_cNomIndexTable = malloc(iLen*sizeof(char));
	sprintf(strSDFRecordXML->ptr_cNomIndexTable, "%s", cTabNomVarSignal[8]);

	// Affectation et cr�ation du repertoire du Datagramme.
	iSizeRep = ( strlen(G_cRepData) + strlen( cNomDatagram));
	// Ajout d'une marge au cas o� certains caract�res soient doubl�s soient doubl�s.
	ptr_cRepertoire = malloc( (iSizeRep + 1 + 30 + 4)*sizeof(char));
	iRet = util_iCreateDirDatagram(G_cRepData,
								strSDFRecordXML->ptr_cNomHeader,
								ptr_cRepertoire);

	// Initialisation g�n�rale des variables de type Signal
	for (iVarSignal=0; iVarSignal<strSDFRecordXML->sNbVarSignal; iVarSignal++) {
	   strSDFRecordXML->strTabSignalXML[iVarSignal].ptr_fpBin = NULL;
	   strSDFRecordXML->strTabSignalXML[iVarSignal].bFlagConstant = TRUE;
	   strSDFRecordXML->strTabSignalXML[iVarSignal].usNbElem = 1;
	   sprintf(strSDFRecordXML->strTabSignalXML[iVarSignal].cNom, "%s", cTabNomVarSignal[iVarSignal]);
	   sprintf(strSDFRecordXML->strTabSignalXML[iVarSignal].cType, "%s", cTabNomTypeSignal[iVarSignal]);
	   sprintf(strSDFRecordXML->strTabSignalXML[iVarSignal].cTag, "%s", "TODO");
	   sprintf(strSDFRecordXML->strTabSignalXML[iVarSignal].cUnit, "%s", "TODO");
	   strSDFRecordXML->strTabSignalXML[iVarSignal].fScaleFactor = fTabSFSignal[iVarSignal];
	   strSDFRecordXML->strTabSignalXML[iVarSignal].fAddOffset = fTabAOSignal[iVarSignal];
	   usTaille = util_usNbOctetTypeVar(cTabNomTypeSignal[iVarSignal]);
	   strSDFRecordXML->strTabSignalXML[iVarSignal].usSize = usTaille;
	   iLen = strlen(strSDFRecordXML->strTabSignalXML[iVarSignal].cNom);
	   // Ajout d'une marge au cas o� certains caract�res soient doubl�s soient doubl�s.
	   sprintf(cNomFicPathBin, "%s\\%s%s", ptr_cRepertoire, strSDFRecordXML->strTabSignalXML[iVarSignal].cNom, ".bin");
	   sprintf(strSDFRecordXML->strTabSignalXML[iVarSignal].cNomPathFileBin, "%s", cNomFicPathBin);
	   strSDFRecordXML->strTabSignalXML[iVarSignal].ptr_fpBin = fopen(cNomFicPathBin, "w+b");
	   if (!strSDFRecordXML->strTabSignalXML[iVarSignal].ptr_fpBin)
	   {
		   printf("%s -- Error in Signal creation  : %s\n", __FILE__, cNomFicPathBin);
		   return EXIT_FAILURE;
	   }
	}

	// Initialisation g�n�rale des variables de type Image
	for (iVarImage=0; iVarImage<strSDFRecordXML->sNbVarImage; iVarImage++) {
	   strSDFRecordXML->strTabImageXML[iVarImage].ptr_fpBin = NULL;
	   strSDFRecordXML->strTabImageXML[iVarImage].bFlagConstant = TRUE;
	   strSDFRecordXML->strTabImageXML[iVarImage].usNbElem = 1;
	   sprintf(strSDFRecordXML->strTabImageXML[iVarImage].cNom, "%s", cTabNomVarImage[iVarImage]);
	   sprintf(strSDFRecordXML->strTabImageXML[iVarImage].cType, "%s", cTabNomTypeImage[iVarImage]);
	   sprintf(strSDFRecordXML->strTabImageXML[iVarImage].cTag, "%s", "TODO");
	   sprintf(strSDFRecordXML->strTabImageXML[iVarImage].cUnit, "%s", "TODO");
	   strSDFRecordXML->strTabImageXML[iVarImage].fScaleFactor = fTabSFImage[iVarImage];
	   strSDFRecordXML->strTabImageXML[iVarImage].fAddOffset = fTabAOImage[iVarImage];
	   usTaille = util_usNbOctetTypeVar(cTabNomTypeImage[iVarImage]);
	   strSDFRecordXML->strTabImageXML[iVarImage].usSize = usTaille;
	   iLen = strlen(strSDFRecordXML->strTabImageXML[iVarImage].cNom);
	   // Ajout d'une marge au cas o� certains caract�res soient doubl�s soient doubl�s.
	   sprintf(cNomFicPathBin, "%s\\%s%s", ptr_cRepertoire, strSDFRecordXML->strTabImageXML[iVarImage].cNom, ".bin");
	   sprintf(strSDFRecordXML->strTabImageXML[iVarImage].cNomPathFileBin, "%s", cNomFicPathBin);
	   strSDFRecordXML->strTabImageXML[iVarImage].ptr_fpBin = fopen(cNomFicPathBin, "w+b");
	   if (!strSDFRecordXML->strTabImageXML[iVarImage].ptr_fpBin)
	   {
		   printf("%s -- Error in Image creation  : %s\n", __FILE__, cNomFicPathBin);
		   return EXIT_FAILURE;
	   }
	}


	free(ptr_cRepertoire);
	return EXIT_SUCCESS;

} // iInitXMLBinV5001Record

#pragma pack()
