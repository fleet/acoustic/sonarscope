/*
 * SDF_processV7001Record.h
 *
 *  Created on: 17 juin 2010
 *      Author: mmorvan
 */
#pragma pack(1)

#ifndef SDF_PROCESSV7001RECORD_H_
#define SDF_PROCESSV7001RECORD_H_

#include "SDF_DATAGRAMS.H"

typedef struct  {
	unsigned short *chan1Data;
	unsigned short *chan2Data;
	unsigned short *chan3Data;
	unsigned short *chan4Data;
	unsigned short *chan5Data;
	unsigned short *chan6Data;
	unsigned short *chan7Data;
	unsigned short *chan8Data;
	unsigned short *chan9Data;
	unsigned short *chan10Data;
	unsigned short *chan11Data;
	unsigned short *chan12Data;
	unsigned short *chan13Data;
	unsigned short *chan14Data;
	unsigned short *chan15Data;
	unsigned short *chan16Data;
	unsigned short *chan17Data;
	unsigned short *chan18Data;
	unsigned short *chan19Data;
	unsigned short *chan20Data;
	unsigned short *sector1Data;
	unsigned short *sector2Data;
	unsigned short *sector3Data;
	unsigned short *sector4Data;
	unsigned short *sector5Data;
	unsigned short *sector6Data;
	unsigned short *sector7Data;
	unsigned short *sector8Data;
	short *bathy1;
	short *bathy2;
	short *bathy3;
	short *bathy4;
	short *bathy5;
	short *bathy6;
	short *bathy7;
	short *bathy8;
	short *bathy9;
	short *bathy10;
	short *bathy11;
	short *bathy12;
	short *bathy13;
	short *bathy14;
	short *bathy15;
	short *bathy16;
	short *bathy17;
	short *bathy18;
	short *bathy19;
	short *bathy20;
	short *bathy21;
	short *bathy22;
	short *bathy23;
	short *bathy24;
	unsigned short *echo1;
	unsigned short *echo2;
	unsigned short *echo3;
	unsigned short *echo4;
	unsigned short *echo5;
	unsigned short *echo6;
	unsigned short *echo7;
	unsigned short *echo8;
	short *sensor1;
	short *sensor2;
	short *sensor3;
	short *sensor4;
} T_V7001_RECORD_IMAGE;

int iProcessV7001Record(FILE *fpFileData, int iNbRecord,
						T_SDFRECORDXML *strSDFRecordXML,
						SDFV4HEADER *ptrSignal_Record,
						SDFV4HEADER *ptrSignal_prevRecord);


int iInitXMLBinV7001Record(	char *cTitleDatagram,
								char *cNomDatagram,
								char *cComments,
								T_SDFRECORDXML *strSDFRecordXML);

#endif /* SDF_PROCESSV7001RECORD_H_ */

#pragma pack()
