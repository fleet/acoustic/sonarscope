// ----------------------------------------------------------------------
// Programme :
//	CONVERT_SDF.C
//
// Objet :
//	Convertir un fichier au format SDF pour d�composer en fichiers
//	XML et fichiers binaires. Les fichiers XML decrivent le contenu des
// 	binaires.
// 	Chaque fichier binaire comporte une donn�e �l�mentaire des paquets par ping
// 	issue des paquets de donn�es du SDF.
//	Il a ete realise sous l'IDE Eclipse 3.4.1 avec compilateur gcc.
//
// Cr�ation :
//	16/06/2010
// Modif. :
//	16/06/2010
//
// Auteur :
//  MVN : (ATL) mikael.morvan@altran.com
//		  02 98 05 43 21
//
// Historique des modifications :
//
// ----------------------------------------------------------------------
#pragma pack(1) // Pour la compilation sous Visual Studio
				// et l'alignement sur 8 octets.

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <errno.h>
#include <libgen.h>


#include "SDF_utilities.h"
#include "SDF_Datagrams.h"
#include "convertFiles_SDF.h"
#include "SDF_Datagrams.h"

#include "SDF_processV3000Record.h"
#include "SDF_processV3001Record.h"
#include "SDF_processV5000Record.h"
#include "SDF_processV5001Record.h"
#include "SDF_processV7000Record.h"
#include "SDF_processV7001Record.h"
#include "SDF_processFileIndexRecord.h"

#include "Generic_Utilities.h"    // Contenu dans la librairie libCONVERT_utils.a

// Fonction de d�claration de la fonction principale de conversion.
// Param�tres :
// - cFileName : r�pertoire et nom du fichier de donn�es
// - barre de progression optionnelle dans l'appel de fonction.
int iConvertFiles_SDF(const char *cFileName, void(*progress)(int))
{
	int 	iRet = 0;

	// Adressage d'un fichier potentiellement sup. � 2 Go.
	off64_t 	llPosFic,
				llNbOcFic,
				llPositionDebDatagram = 0;

	FILE *fpFileData;


	//T_TYPEOFDATAGRAMNOTTRANSLATE tabStrDatagramNotTranslate[30];


	SDFV3HEADER 	sdfV3Header,sdfV3PrevHeader;
	SDFENDV4HEADER  sdfEndV4Header, sdfEndV4PrevHeader;
	SDFV4HEADER     sdfV4Header, sdfV4PrevHeader;

	//Listes des records
	T_SDFRECORDXML 				strV3000RecordXML;
	T_SDFRECORDXML 				strV3001RecordXML;
	T_SDFRECORDXML 				strV5000RecordXML;
	T_SDFRECORDXML 				strV5001RecordXML;
	T_SDFRECORDXML 				strV7000RecordXML;
	T_SDFRECORDXML 				strV7001RecordXML;
	T_SDFRECORDXML 				strFileIndexRecordXML;
	T_FILEINDEX_RECORD_SIGNAL	strSignalFileIndexToSave;

	strV3000RecordXML.iNbDatagram 		= 0;
	strV3001RecordXML.iNbDatagram 		= 0;
	strV5000RecordXML.iNbDatagram 		= 0;
	strV5001RecordXML.iNbDatagram 		= 0;
	strV7000RecordXML.iNbDatagram 		= 0;
	strV7001RecordXML.iNbDatagram 		= 0;
	strFileIndexRecordXML.iNbDatagram 	= 0;

	// Initialisation des datagrammes d�j� trouv�s.
	/*for (iLoop=0; iLoop<30; iLoop++)
	{
		tabStrDatagramNotTranslate[iLoop].cType = 0;
		tabStrDatagramNotTranslate[iLoop].iNbTimes = 0;
	}*/

	// Ouverture du fichier SDF
	fpFileData = fopen64(cFileName,"rb");
	if(fpFileData  == NULL)
	{
		printf("\nProblem in access Data file : %s\n", cFileName);
		goto ERROR_OUT;

	}

    // Calcul de la taille du fichier.
	fseeko64(fpFileData, 0, SEEK_END); //On se positionne � la fin du fichier
    llNbOcFic=ftello64(fpFileData);
    fseeko64(fpFileData, 0, SEEK_SET);
    llPosFic = 0;

    llPositionDebDatagram = 4;

    //Deroulement entier du fichier .SDF
	while(!feof(fpFileData) && llPositionDebDatagram < llNbOcFic)
	{
	    llPosFic = ftello64(fpFileData);

	    // Affichage de la progression dans la fen�tre d'ex�cution
#ifdef GUI
	    if(progress)
	    	progress(100*(double)llPosFic/(double)llNbOcFic);
#else
	    util_vProgressBar(0, llPosFic, llNbOcFic);
#endif
		fseeko64(fpFileData,llPositionDebDatagram,SEEK_SET);



		// Lecture du pingMarker
		// fread(&pingMarker,1,sizeof(pingMarker),fpFileData);//on skip le pingMarker
		// Lecture du header v3 : 3000 3001 5000 5001 7000 7001
		fread(&sdfV3Header,1,sizeof(sdfV3Header),fpFileData);

		// Affectation des donn�es d'index pour chaque Page lue : pas aussi subtil que dans CONVERT_ALL car
		// les donn�es sont r�parties dans les paquets � lire.
		strSignalFileIndexToSave.NbOfBytesInPage 	= sdfV3Header.numberBytes;
		strSignalFileIndexToSave.PageVersion 		= sdfV3Header.pageVersion;
		strSignalFileIndexToSave.year 				= sdfV3Header.year;
		strSignalFileIndexToSave.month 				= sdfV3Header.month;
		strSignalFileIndexToSave.day 				= sdfV3Header.day;
		strSignalFileIndexToSave.hour 				= sdfV3Header.hour;
		strSignalFileIndexToSave.minute 			= sdfV3Header.minute;
		strSignalFileIndexToSave.second 			= sdfV3Header.second;
		strSignalFileIndexToSave.hSecond 			= sdfV3Header.hSecond;
		// D�calage de 1 du num�ro pour test d'initialisation dans la proc�dure.
		strSignalFileIndexToSave.FileIndexCounter 	= (unsigned short)strFileIndexRecordXML.iNbDatagram+1;
		strSignalFileIndexToSave.DatagramPosition 	= llPositionDebDatagram - 4;
		strSignalFileIndexToSave.DatagramLength 	= sdfV3Header.numberBytes + 4 ; // Ping Marker - � localiser ???

		iRet = iProcessFileIndexRecord(fpFileData,
								strFileIndexRecordXML.iNbDatagram,
								strSignalFileIndexToSave,
								&strFileIndexRecordXML);
		strFileIndexRecordXML.iNbDatagram++;


		// si header v4, lecture de la fin du header
		if(sdfV3Header.pageVersion&0x00000001)//3001 5001 7001
		{
			fread(&sdfEndV4Header,1,sizeof(sdfEndV4Header),fpFileData);

			sdfV4Header.sdfV3header = sdfV3Header;
			sdfV4Header.sdfendV4header = sdfEndV4Header;
		}

		// ATTENTION :	seule la conversion V3001 a �t� test� d fa�on plus approfondie. Le header est plus sp�cifi� que pour les autres
		// 				types de format de sonar.
		// Lecture du paquet de donn�es commun aux datagrammes.
		if(sdfV3Header.pageVersion == 3000)
		{
			iRet = iProcessV3000Record(fpFileData, strV3000RecordXML.iNbDatagram, &strV3000RecordXML, &sdfV3Header, &sdfV3PrevHeader);
			strV3000RecordXML.iNbDatagram++;
		}
		else if(sdfV3Header.pageVersion == 3001)
		{
			iRet = iProcessV3001Record(fpFileData, strV3001RecordXML.iNbDatagram, &strV3001RecordXML, &sdfV4Header, &sdfV4PrevHeader);
			strV3001RecordXML.iNbDatagram++;
		}
		else if(sdfV3Header.pageVersion == 5000)
		{
			iRet = iProcessV5000Record(fpFileData, strV5000RecordXML.iNbDatagram, &strV5000RecordXML, &sdfV3Header, &sdfV3PrevHeader);
			strV5000RecordXML.iNbDatagram++;
		}
		else if(sdfV3Header.pageVersion == 5001)
		{
			iRet = iProcessV5001Record(fpFileData, strV5001RecordXML.iNbDatagram, &strV5001RecordXML, &sdfV4Header, &sdfV4PrevHeader);
			strV5001RecordXML.iNbDatagram++;
		}
		else if(sdfV3Header.pageVersion == 7000)
		{
			iRet = iProcessV7000Record(fpFileData, strV7000RecordXML.iNbDatagram, &strV7000RecordXML, &sdfV3Header, &sdfV3PrevHeader);
			strV7000RecordXML.iNbDatagram++;
		}
		else if(sdfV3Header.pageVersion == 7001)
		{
			iRet = iProcessV7001Record(fpFileData, strV7001RecordXML.iNbDatagram, &strV7001RecordXML, &sdfV4Header, &sdfV4PrevHeader);
			strV7001RecordXML.iNbDatagram++;
		}

		// Calage de l'index de lecture sur le datagramme suivant.
		llPositionDebDatagram = llPositionDebDatagram + sdfV3Header.numberBytes + 4;//+4 skip pingMarker

	} // Fin de la boucle while de lecture du fichier.

	// End of progress bar
	if(progress)
		progress(100);

	// Fermeture en lecture du fichier de donn�es.
	fclose(fpFileData);

	// Fermeture des fichiers binaires des paquets et �criture des XML.
	utilSDF_iSaveAndCloseFiles(G_cRepData, &strFileIndexRecordXML, sdfV4Header.sdfV3header.configuration);
	if (strV3000RecordXML.iNbDatagram != 0)
	{
		utilSDF_iSaveAndCloseFiles(G_cRepData, &strV3000RecordXML, sdfV4Header.sdfV3header.configuration);
	}
	if (strV3001RecordXML.iNbDatagram != 0)
	{
		utilSDF_iSaveAndCloseFiles(G_cRepData, &strV3001RecordXML, sdfV4Header.sdfV3header.configuration);
	}
	if (strV5000RecordXML.iNbDatagram != 0)
	{
		utilSDF_iSaveAndCloseFiles(G_cRepData, &strV5000RecordXML, sdfV4Header.sdfV3header.configuration);
	}
	if (strV5001RecordXML.iNbDatagram != 0)
	{
		utilSDF_iSaveAndCloseFiles(G_cRepData, &strV5001RecordXML, sdfV4Header.sdfV3header.configuration);
	}
	if (strV7000RecordXML.iNbDatagram != 0)
	{
		utilSDF_iSaveAndCloseFiles(G_cRepData, &strV7000RecordXML, sdfV4Header.sdfV3header.configuration);
	}
	if (strV7001RecordXML.iNbDatagram != 0)
	{
		utilSDF_iSaveAndCloseFiles(G_cRepData, &strV7001RecordXML, sdfV4Header.sdfV3header.configuration);
	}

	return EXIT_SUCCESS;

	ERROR_OUT:

	// End of progress bar
	if(progress){
		progress(50);
		progress(100);
	}

	if (fpFileData != NULL) {
	  fclose(fpFileData);
	  fpFileData = 0;
	}

	return EXIT_FAILURE;
} // iConvertFiles_SDF


/*int iSharedDatagrammV3000_SDF(FILE *fpFileData, U32 configuration)
{
	int    iRet = 0;
	unsigned short size=0;
	if(configuration&0x00000003 == 0x00000003)
	{
		unsigned short portlf[];
		unsigned short stbdlf [];
	}
	if(configuration&0x0000000C == 0x0000000C)
	{
		unsigned short porthf[];
		unsigned short stbdhf[];
	}

	return iRet;
}*/

#pragma pack()
