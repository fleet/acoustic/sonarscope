/*
 * MAIN_SDF.h
 *
 *  Created on: 16 juin 2010
 *      Author: mmorvan
 */

#ifndef MAIN_SDF_H_
#define MAIN_SDF_H_

#pragma pack(1) // Pour la compilation sous Visual Studio
				// et l'alignement sur 8 octets.
	#define DEBUG 0

	/*** Variables globales ***/

#pragma pack()

#endif /* MAIN_SDF_H_ */
