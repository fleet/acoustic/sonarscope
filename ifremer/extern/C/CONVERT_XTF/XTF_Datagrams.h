#ifndef XTF_DATAGRAMS_H_
#define XTF_DATAGRAMS_H_
#pragma pack(1) // peut etre remplace par l'option de compil -fpack-struct pour gcc

// Include-file for reading .XTF files.  This file defines the
//   structures currently defined in the .XTF file format.

#include "stdio.h"

#include "Generic_Utilities.h"    // Contenu dans la librairie libCONVERT_utils.a

#define ERRORSTRING (errno == 0 ? "unknown reason" : strerror (errno))
#define NB_MAX_CHANNELS 10

#define XTF_HEADER_SONAR 					0	 // Sidescan data
#define XTF_HEADER_NOTES					1	 // Notes - text annotation
#define XTF_HEADER_BATHY 					2	 // Bathymetry data
#define XTF_HEADER_ATTITUDE 				3	 // Attitude packet
#define XTF_HEADER_FORWARD 					4	 // Forward look data (Sonatech)
#define XTF_HEADER_ELAC 					5	 // Elac raw data packet.
#define XTF_HEADER_RAW_SERIAL 				6	 // Raw ASCII serial port data.
#define XTF_HEADER_EMBED_HEAD 				7	 // Embedded header record - num samples probably changed.
#define XTF_HEADER_HIDDEN_SONAR 			8	 // Redundant (overlapping) ping from Klein 5000.
#define XTF_HEADER_SEAVIEW_PROCESSED_BATHY 	9	 // Bathymetry (angles) for Seaview.
#define XTF_HEADER_SEAVIEW_DEPTHS 			10	 // Bathymetry from Seaview data (depths).
#define XTF_HEADER_RSVD_HIGHSPEED_SENSOR 	11	 // Used by Klein. 0 roll, 1=yaw
#define XTF_HEADER_ECHOSTRENGTH 			12	 // Elac EchoStrength (10 values).
#define XTF_HEADER_GEOREC 					13	 // Used to store mosaic parameters.
#define XTF_HEADER_KLEIN_RAW_BATHY 			14	 // Bathymetry data from the Klein 5000.
#define XTF_HEADER_HIGHSPEED_SENSOR2 		15	 //  High speed sensor from Klein 5000.
#define XTF_HEADER_ELAC_XSE 				16	 //  Elac dual-head.
#define XTF_HEADER_BATHY_XYZA 				17	 //
#define XTF_HEADER_K5000_BATHY_IQ 			18	 //  Raw IQ data from Klein 5000 server
#define XTF_HEADER_BATHY_SNIPPET			19	 // 81xx snippet Packet
#define XTF_HEADER_GPS GPS Position.		20	 //
#define XTF_HEADER_STAT 					21	 //  GPS statistics.
#define XTF_HEADER_SINGLEBEAM				22	 //
#define XTF_HEADER_GYRO 					23	 // Heading/Speed Sensor.
#define XTF_HEADER_TRACKPOINT				24	 //
#define XTF_HEADER_MULTIBEAM				25	 //
#define XTF_HEADER_Q_SINGLEBEAM				26	 //
#define XTF_HEADER_Q_MULTITX				27	 //
#define XTF_HEADER_Q_MULTIBEAM				28	 //
#define XTF_HEADER_TIME						50	 //
#define XTF_HEADER_BENTHOS_CAATI_SARA 		60	 // Custom Benthos data.
#define XTF_HEADER_7125 					61	 //  7125 Bathy Data
#define XTF_HEADER_7125_SNIPPET 			62	 //  7125 Bathy Data Snippets
#define XTF_HEADER_QINSY_R2SONIC_BATHY 		65	 //  Quincy R2Sonic bathymetry data
#define XTF_HEADER_QINSY_R2SONIC_FTS 		66	 //  Quincy R2Sonics Foot Print Time Series (snippets)
#define XTF_HEADER_R2SONIC_BATHY 			68	 //  Triton R2Sonic bathymetry data
#define XTF_HEADER_R2SONIC_FTS 				69	 //  Triton R2Sonic Footprint Time Series
#define XTF_HEADER_CODA_ECHOSCOPE_DATA 		70	 //  Custom CODA Echoscope Data
#define XTF_HEADER_CODA_ECHOSCOPE_CONFIG 	71	 //  Custom CODA Echoscope Data
#define XTF_HEADER_CODA_ECHOSCOPE_IMAGE 	72	 //  Custom CODA Echoscope Data
#define XTF_HEADER_EDGETECH_4600			73	 //
#define XTF_HEADER_RESON_7027_BATHY 		76	 //  (Type 7027 RAW Detection Data � see Reson document 7k Data Format, Vol 1, Ver 2.00 para 10.38)
#define XTF_HEADER_POSITION 				100	 //  Raw position packet - Reserved for use by Reson, Inc. RESON ONLY.
#define XTF_HEADER_BATHY_PROC				102	 //
#define XTF_HEADER_ATTITUDE_PROC			103	 //
#define XTF_HEADER_SINGLEBEAM_PROC			104	 //
#define XTF_HEADER_AUX_PROC 				105	 //  Aux Channel + AuxAltitude + Magnetometer.
#define XTF_HEADER_KLEIN3000_DATA_PAGE		106	 //
#define XTF_HEADER_POS_RAW_NAVIGATION		107	 //
#define XTF_HEADER_KLEINV4_DATA_PAGE		108	 //
#define XTF_HEADER_USERDEFINED				200	 //

#define FMT_XTF 123 // unique ID for all XTF files.

// Channel information structure (contained in the file header).
// One-time information describing each channel.  64 bytes long.
// This is data pertaining to each channel that will not change
// during the course of a run.
///////////////////////////////////////////////////////////////////////////////
typedef struct {
   BYTE TypeOfChannel;     // PORT, STBD, SBOT or BATH
   BYTE SubChannelNumber;
   WORD CorrectionFlags;   // 1=raw, 2=Corrected
   WORD UniPolar;          // 0=data is bipolar, 1=data is unipolar
   WORD BytesPerSample;    // 1 or 2
   // Release 25 : DWORD SamplesPerChannel;// Usually a multiple of 1024 unless bathymetry
   DWORD Reserved;
   char ChannelName[16];   // Text describing channel.  i.e., "Port 500"

   float VoltScale;        // How many volts is represented by max sample value.  Typically 5.0.
   float Frequency;        // Center transmit frequency
   float HorizBeamAngle;   // Typically 1 degree or so
   float TiltAngle;        // Typically 30 degrees
   float BeamWidth;        // 3dB beam width, Typically 50 degrees

                           // Orientation of these offsets:
                           // Positive Y is forward
                           // Positive X is to starboard
                           // Positive Z is down.  Just like depth.
                           // Positive roll is lean to starboard
                           // Positive pitch is nose up
                           // Positive yaw is turn to right

   float OffsetX;          // These offsets are entered in the
   float OffsetY;          // Multibeam setup dialog box.
   float OffsetZ;

   float OffsetYaw;        // If the multibeam sensor is reverse
                           // mounted (facing backwards), then
                           // OffsetYaw will be around 180 degrees.
   float OffsetPitch;
   float OffsetRoll;
   // Deb Release 25
   WORD BeamsPerArray;
   BYTE SamplePerFormat;
   //char ReservedArea2[56];
   char ReservedArea2[53];
   // Fin Release 25

} CHANINFO;



// XTF File header.
// Total of 1024 bytes.
///////////////////////////////////////////////////////////////////////////////
typedef struct {
   BYTE FileFormat;        // 50 for Q-MIPS file format, 51 for Isis format
   BYTE SystemType;        // Type of system used to record this file.  202=Isis
   char RecordingProgramName[8];    // Example: "Isis"
   char RecordingProgramVersion[8]; // Example: "1.72"
   char SonarName[16];     // Name of server used to access sonar.  Example: "C31_SERV.EXE"
   WORD SonarType;         // K2000=5, DF1000=7, SEABAT=8
   char NoteString[64];    // Notes as entered in the Sonar Setup dialog box
   char ThisFileName[64];  // Name of this file. Example: "LINE12-B.SNR"

   WORD NavUnits;          // 0=METERS or 3=DEGREES

   WORD NumberOfSonarChannels;  // if > 60, header goes to 8K in size
   WORD NumberOfBathymetryChannels;
   // Deb Release 25
   BYTE NumberOfSnippetsChannels;
   BYTE NumBerOfForwardLookArrays;
   WORD NumberOfEchoStrengthChannels;
   BYTE NumberOfInterferometryChannels;
   BYTE Reserved1;
   WORD Reserved2;
   float ReferencePointHeight;

//   WORD Reserved1;
//   WORD Reserved2;
//   WORD Reserved3;
//   WORD Reserved4;
//   WORD Reserved5;
//   WORD Reserved6;
   // Fin Release 25
   // nav system parameters
   ///////////////////////////
   BYTE     ProjectionType[12];       // Not currently used
   BYTE     SpheroidType[10];         // Not currently used
   long     NavigationLatency;        // milliseconds, latency of nav system (usually GPS)
                                      // This value is entered on the
                                      // Serial port setup dialog box.
                                      // When computing a position, Isis will
                                      // take the time of the navigation
                                      // and subtract this value.

   float    OriginY;                  // Not currently used
   float    OriginX;                  // Not currently used

                                      // Orientation of these offsets:
                                      // Positive Y is forward
                                      // Positive X is to starboard
                                      // Positive Z is down.  Just like depth.
                                      // Positive roll is lean to starboard
                                      // Positive pitch is nose up
                                      // Positive yaw is turn to right

   float    NavOffsetY;               // These offsets are entered in
   float    NavOffsetX;               // the multibeam setup dialog box.
   float    NavOffsetZ;
   float    NavOffsetYaw;

   float    MRUOffsetY;               // These offsets are entered in
   float    MRUOffsetX;               // the multibeam setup dialog box
   float    MRUOffsetZ;

   float    MRUOffsetYaw;
   float    MRUOffsetPitch;
   float    MRUOffsetRoll;

                           // note: even 128-byte boundary to here

   CHANINFO ChanInfo[6];  // Each CHANINFO struct is 128 bytes.
                          // If more than 6 channels needed, header record
                          // grows 1K in size for each additional 8 channels.
} XTFFILEHEADER;


// Embedded header record -- occurs mid-file
///////////////////////////////////////////////////////////////////////////////
typedef struct {
   WORD MagicNumber;      // Set to 0xFACE
   BYTE HeaderType;       // 7=embedded header record
   BYTE SubChannelNumber; //
   WORD NumChansToFollow; //
   WORD Reserved1[2];

   DWORD NumBytesThisRecord; // Total byte count for this embedded record including the 64 front-pad bytes
   BYTE Reserved[50]; // make even to 64 bytes so far
   XTFFILEHEADER NewHeader;

} XTFEMBEDDEDFILEHEADER;


// The XTFATTITUDEDATA structure used to store information from a TSS or
// MRU motion sensor device.  This is usually high-resolution data
// (updating 20 times per second or more) and is needed when processing
// multibeam bathymetric data.  When TSS or MRU is selected as a serial device,
// the data is received and decoded.  As the attitude information is decoded,
// the values are filled into the following structure and then saved to the
// XTF file.
//
// Attitude data packet, 64 bytes in length.
///////////////////////////////////////////////////////////////////////////////
typedef struct {
   //
   // Type of header
   //
   WORD MagicNumber;      // Set to 0xFACE
   BYTE HeaderType;       // XTF_HEADER_ATTITUDE (3)
   BYTE SubChannelNumber; // When HeaderType is Bathy, indicates which head
   WORD NumChansToFollow; // If Sonar Ping, Number of channels to follow
   WORD Reserved1[2];

   DWORD NumBytesThisRecord; // Total byte count for this ping including this ping header
   DWORD Reverved2[4];

   // will be followed by attitude data even to 64 bytes
   float Pitch;   // positive value is nose up
   float Roll;    // positive value is roll to starboard
   float Heave;   // positive value is sensor up
                  // Note: The TSS sends heave positive up.  The MRU
                  // sends heave positive down.  In order to make the
                  // data logging consistent, the sign of the MRU's
                  // heave is reversed before being stored in this field.

   float Yaw;     // positive value is turn right
   DWORD TimeTag; // time ref. given in milliseconds

   float Heading; // In degrees, as reported by MRU.
                  // TSS doesn't report heading, so when using a TSS
                  // this value will be the most recent ship gyro value
                  // as received from GPS or from any serial port using
                  // 'G' in the template.

   // Deb Release 25 :
   //BYTE  Reserved3[10];
   WORD Year;
   BYTE Month;
   BYTE Day;
   BYTE Hour;
   BYTE Seconds;
   BYTE Minutes;
   WORD MicroSeconds;
   BYTE Reserved3;


} XTFATTITUDEDATA;


// XTFPOSITIONDATA
///////////////////////////////////////////////////////////////////////////////
typedef struct {
   //
   // Type of header
   //
   WORD MagicNumber;      // Set to 0xFACE
   BYTE HeaderType;       // XTF_HEADER_ATTITUDE (3)
   BYTE SubChannelNumber; // When HeaderType is Bathy, indicates which head
   WORD NumChansToFollow; // If Sonar Ping, Number of channels to follow
   WORD Reserved1[2];

   DWORD NumBytesThisRecord; // Total byte count for this ping including this ping header
   //DWORD Reverved2[4];

   float XPosition;
   float YPosition;
   float ZPosigtion;
} XTFPOSITIONDATA;


// Sonar or Bathy Ping header
// The data here can change from ping to ping but will pertain to all
// channels that are at the same time as this ping.  256 bytes in length.
///////////////////////////////////////////////////////////////////////////////
typedef struct {

   //
   // Type of header
   //
   WORD MagicNumber;      // Set to 0xFACE
   BYTE HeaderType;       // XTF_HEADER_SONAR (0), XTF_HEADER_BATHY (2),
                          //   XTF_HEADER_FORWARD or XTF_HEADER_ELAC (5)
   BYTE SubChannelNumber; // When HeaderType is Bathy, indicates which head
                          // When sonar, which ping of a batch (Klein 5000: 0..4)

   WORD NumChansToFollow; // If Sonar Ping, Number of channels to follow
   WORD Reserved1[2];

   DWORD NumBytesThisRecord; // Total byte count for this ping including this ping header

   //
   // Date and time of the ping
   //
   WORD  Year;          // Computer date when this record was saved
   BYTE  Month;
   BYTE  Day;
   BYTE  Hour;          // Computer time when this record was saved
   BYTE  Minute;
   BYTE  Second;
   BYTE  HSeconds;      // hundredths of seconds (0-99)
   WORD  JulianDay;     // Number of days since January 1

   //
   // General information
   //
   // Deb Release 25 :
   //WORD CurrentLineID;     // [i] Current line ID from serial port
   //WORD EventNumber;       // [O] Last logged event number
   DWORD EventNumber;       // [O] Last logged event number
   // Fin Release 25
   DWORD PingNumber;       // Counts consecutively from 0 and increments
                           //   for each update.  Note that the
                           //   counters are different between sonar
                           //   and bathymetery updates.

   float SoundVelocity;    // m/s, Round trip, defaults to 750.
                           //   Can be changed on Isis menu.  This
                           //   value is never computed and can only be
                           //   changed manually by the user. Also see
                           //   ComputedSoundVelocity below.

   float OceanTide;        // [{t}] Ocean tide in meters.  Can be
                           // changed by the user on the Configure
                           // menu in Isis.
   DWORD Reserved2;        // Reserved for future use

   //
   // Raw CTD information.  The Freq values are those sent up by the
   // SeaBird CTD.  The Falmouth Scientific CTD sends up computed data.
   float ConductivityFreq; // [Q] Conductivity frequency in Hz
   float TemperatureFreq;  // [b] Temperature frequency in Hz
   float PressureFreq;     // [0] Pressure frequency in Hz
   float PressureTemp;     // [;] Pressure Temperature (Degrees C)

   //
   // Computed CTD information.  When using a SeaBird CTD, these
   // values are computed from the raw Freq values (above).
   //
   float Conductivity;     // [{c}] Conductivity in S/m can be computed from [Q]
   float WaterTemperature; // [{w}] Water temperature in C, can be computed from [b]
   float Pressure;         // [{p}] Water pressure in psia, can be computed from [0]
   float ComputedSoundVelocity;  // Meters per second, computed from
                                 // Conductivity, WaterTemperature and
                                 // Pressure using the Chen Millero
                                 // formula (1977) formula (JASA,62,1129-1135).


   //
   // Sensors information
   //
   float MagX;             // [e] X-axis magnetometer data, mgauss
   float MagY;             // [w] Y-axis magnetometer data, mgauss
   float MagZ;             // [z] Z-axis magnetometer data, mgauss

                           // Auxillary values can be used to store
                           // and display any value at the user's
                           // discretion.  The are not used in
                           // any calculation in Isis, Target or Vista.

   float AuxVal1;          // [1] Auxillary value.  Displayed in the
   float AuxVal2;          // [2] Auxillary value   "Sensors" window
   float AuxVal3;          // [3] Auxillary value   available by selecting
   float AuxVal4;          // [4] Auxillary value   Window->Text->Sensors.
   float AuxVal5;          // [5] Auxillary value
   float AuxVal6;          // [6] Auxillary value

   float SpeedLog;         // [s] Speed log sensor on towfish - knots. This isn't fish speed!
   float Turbidity;        // [|] turbidity sensor (0 to +5 volts) stored times 10000

   //
   // Ship Navigation information.  These values are stored only
   // and are not part of any equation or computation in Isis.
   //
   float ShipSpeed;        // [v] Speed of ship in knots.  Stored
   float ShipGyro;         // [G] Ship gyro in degrees
   double ShipYcoordinate; // [y] Ship latitude or northing
   double ShipXcoordinate; // [x] Ship longitude or easting
   // Release 25 : DWORD Reserved3;
   WORD ShipAltitude;
   WORD ShipDepth;

   //
   // Sensor Navigation information
   //
   BYTE FixTimeHour;       // [H] Hour of most recent nav update
   BYTE FixTimeMinute;     // [I] Minute of most recent nav update
   BYTE FixTimeSecond;     // [S] Second of most recent nav update
                           // Note that the time of the nav is
                           // adjusted by the NavLatency stored in
                           // the XTF file header.
   BYTE	FixTimeHsecond;
   // Release 25 : BYTE Reserved4;
   float SensorSpeed;      // [V] Speed of the in knots.  Used for
                           //   speed correction and position calculation.
   float KP;               // [{K}] Kilometers Pipe
   double SensorYcoordinate; // [E] Sensor latitude or northing
   double SensorXcoordinate; // [N] Sensor longitude or easting
                             // Note: when NavUnits in the file header
                             // is 0, values are in meters (northings
                             // and eastings).  When NavUnits is 3,
                             // values are in Lat/Long.  Also see
                             // the Layback value, below.

   //
   // Tow Cable information
   //
   // Deb Release 25 : WORD Reserved6;
   WORD SonarStatus;
   // Fin Release 25

   WORD RangeToFish;       // [?] Slant range to fish * 10.
                           //    Not currently used.
   WORD BearingToFish;     // [>] Bearing to towfish from ship * 100.
                           //    Not currently used.
   WORD CableOut;          // [o] Amount of cable payed out in meters
                           //    Not currently used in Isis.
   float Layback;          // [l] Distance over ground from ship to fish.
                           //    When this value is non-zero, Isis
                           //    assumes that SensorYcoordinate and
                           //    SensorXcoordinate need to be
                           //    adjusted with the Layback.  The sensor
                           //    position is then computed using the
                           //    current sensor heading and this layback
                           //    value.  The result is displayed when a
                           //    position is computed in Isis.

   float CableTension;     // [P] Cable tension from serial port. Stored only.

   //
   // Sensor Attitude information
   //
   float SensorDepth;        // [0] Distance from sea surface to
                             //   sensor.  The deeper the sensor goes,
                             //   the bigger (positive) this value becomes.

   float SensorPrimaryAltitude;
                             // [7] Distance from towfish to the sea
                             //   floor.  This is the primary altitude as
                             //   tracked by the Isis bottom tracker or
                             //   entered manually by the user.
                             //   Although not recommended, the user can
                             //   override the Isis bottom tracker by
                             //   sending the primary altitude over the
                             //   serial port.  The user should turn the
                             //   Isis bottom tracker Off when this is done.

   float SensorAuxAltitude;  // [a] Auxillary altitude.  This is an
                             //   auxillary altitude as transmitted by an
                             //   altimeter and received over a serial port.
                             //   The user can switch betwen the Primary and
                             //   Aux altitudes via the "options" button in
                             //   the Isis bottom track window.

   float SensorPitch;        // [8] Pitch in degrees (positive=nose up)
   float SensorRoll;         // [9] Roll in degrees (positive=roll to stbd)
   float SensorHeading;      // [h] Fish heading in degrees

   // These Pitch, Roll, Heading, Heave and Yaw values are those received
   // closest in time to this sonar or bathymetry update.  If a TSS or MRU
   // is being used with a multibeam/bathymetry sensor, the user should
   // use the higher-resolution attitude data found in the XTFATTITUDEDATA
   // structures.


   //
   // additional attitude data
   //
   float Heave;            // Sensor heave at start of ping.
                           // Positive value means sensor moved up.
   float Yaw;              // Sensor yaw.  Positive means turn to right.
   DWORD AttitudeTimeTag;

   float DOT;              // Distance Off Track
   // Debut Nouveau Format Release 25
   DWORD NavFixMilliseconds;
   BYTE	 ComputerClockHour,
		 ComputerClockMinute,
		 ComputerClockSecond;

   BYTE	 ComputerClockHsec;

   short FishPositionDeltaX,
		 FishPositionDeltaY;

   unsigned char FishPositionErrorCode;
   // Pad to make an even 256 bytes
   BYTE ReservedSpace[11];

   // Fin Nouveau Format R25

   //
   // Pad to make an even 256 bytes
   //
   //BYTE ReservedSpace[24];

} XTFPINGHEADER, XTFBATHHEADER;



// Annotation record
// An annotation record is a line of text which can be saved to the
// file and is displayed in the "Notes" field on the Isis display.
// This text is displayed during playback.  Additionally, this text
// may be printed in realtime or in playback.  This can be activated
// in the Print Annotation dialog box.
///////////////////////////////////////////////////////////////////////////////
typedef struct {

   WORD MagicNumber;      // Set to 0xFACE
   BYTE HeaderType;       // XTF_HEADER_NOTES (1)
   BYTE SubChannelNumber;
   WORD NumChansToFollow;
   WORD Reserved[2];
   DWORD NumBytesThisRecord; // Total byte count for this update

   //
   // Date and time of the annotation
   //
   WORD  Year;
   BYTE  Month;
   BYTE  Day;
   BYTE  Hour;
   BYTE  Minute;
   BYTE  Second;
   BYTE  ReservedBytes[35];

   char  NotesText[256-56];

} XTFNOTESHEADER;


// RAW ASCII data received over serial port
// These packets are stored in the XTF file on a per-serial-port
// basis.  To store the raw ASCII data for a given serial port, add
// the token "{SAVEALL}" to the serial port template.  Use of this
// option is not generally recommended, since Isis already parses the
// data for all usefull information.

///////////////////////////////////////////////////////////////////////////////
typedef struct {

   WORD MagicNumber;      // Set to 0xFACE
   BYTE HeaderType;       // will be XTF_HEADER_RAW_SERIAL (7)
   BYTE SerialPort;
   //Deb Release 25
   //WORD Reserved2[3];
   WORD NumChansToFollow;
   WORD Reserved2[2];
   //Fin Release 25
   DWORD NumBytesThisRecord; // Total byte count for this update

   //
   // Date and time raw ASCII data was posted to disk
   //
   WORD  Year;
   BYTE  Month;
   BYTE  Day;
   BYTE  Hour;
   BYTE  Minute;
   BYTE  Second;
   BYTE  HSeconds;      // hundredth of seconds (0-99)
   WORD  JulianDay;     // days since Jan 1.

   DWORD TimeTag;       // millisecond timer value
   WORD  StringSize;    // Number of valid chars in RawAsciiData string
   char  RawAsciiData[64-30]; // will be padded in 64-byte increments to make
                              // structure an even multiple of 64 bytes

} XTFRAWSERIALHEADER;


// Ping Channel header
// This is data that can be unique to each channel from ping to ping.
// Is is stored at the front of each channel of sonar data.
///////////////////////////////////////////////////////////////////////////////
typedef struct {

   WORD ChannelNumber;     // Typically,
                           // 0=port (low frequency)
                           // 1=stbd (low frequency)
                           // 2=port (high frequency)
                           // 3=stbd (high frequency)

   WORD DownsampleMethod;  // 2=MAX, 4=Rms
   float SlantRange;       // Slant range of the data in meters
   float GroundRange;      // Ground range of the data in meters
                           //   (SlantRange^2 - Altitude^2)
   float TimeDelay;        // Amount of time (in seconds) to the start of recorded data
                           //   almost always 0.0
   float TimeDuration;     // Amount of time (in seconds) recorded
   float SecondsPerPing;   // Amount of time (in seconds) from ping to ping

   //Deb Release 25 de la psec.
   //DWORD ProcessingFlags;  // 4=TVG, 8=BAC and GAC, 16=Filter, etc...
   //                        //   almost always 0
   WORD ProcessingFlags;  // 4=TVG, 8=BAC and GAC, 16=Filter, etc...
                           //   almost always 0
   WORD	Frequency;
   WORD InitialGainCode;   // Settings as transmitted by sonar
   WORD GainCode;
   WORD BandWidth;


   DWORD ContactNumber;
   WORD	WordClassification;
   BYTE ContactSubNumber;
   BYTE ContactType;

   DWORD NumSamples;
   WORD MillivoltScale;
   float ContactTimeOffTrack;
   BYTE ContactCloseNumber;
   BYTE Reserved2;
   float FixedVSOP;
   short Weight;

   BYTE ReservedSpace[4];

   //Release 25 BYTE ReservedSpace[30]; // reserved for future expansion

} XTFPINGCHANHEADER;

// Paquet de type QPS_Multibeam
// XTF QPS_Multibeam
///////////////////////////////////////////////////////////////////////////////
typedef struct {
	int		Id;
	double	Intensity;
	int		Quality;
	double	TwoWayTravelTime;
	double	DeltaTime;
	double	BeamAngle;
	double	TiltAngle;
	float	ReservedSpace[4];
} XTFQPSMULTIBEAMHEADER;

// XTF Bathy Snippet SNP0 Data (generated by RESON Seabat)
///////////////////////////////////////////////////////////////////////////////
typedef struct {

	unsigned long	ID;
	WORD  			HeaderSize,
					DataSize;

	unsigned long   PingNumber,
					Seconds,
					Millisecs;


	WORD			Latency,
					SonarID[2],
					SonarModel,
					Frequency,
					sSSpeed,
					SampleRate,
					PingRate,
					Range,
					Power,
					Gain,
					PulseWidth,
					Spread,
					Absorb,
					Proj,
					ProjWidth,
					SpacingNum,
					SpacingDen;

	short			ProjAngle;

	WORD 			MinRange,
					MaxRange,
					MinDepth,
					MaxDepth,
					Filters;

	BYTE			Flags[2];

	short			HeadTemp;

	WORD			BeamCnt;


} XTFBATHYSNP0SNIPPET;

// XTF Bathy Snippet SNP1 Data
///////////////////////////////////////////////////////////////////////////////
typedef struct {

	unsigned long	ID;
	WORD  			HeaderSize,
					DataSize;

	unsigned long   PingNumber;


	WORD			Beam,
					SnipSamples,
					GainStart,
					GainEnd,
					FragOffset,
					FragSamples;

} XTFBATHYSNP1SNIPPET;

typedef struct {
   BYTE 		HeaderType;
   WORD 		NumChansToFollow;
   double 		dSensorLatitude;
   double 		dSensorLongitude;
} XTF7125BATHYDATA;

typedef struct {
   BYTE 		HeaderType;
   DWORD 		NumBytesThisRecord;
   double 		dSensorLatitude;
   double 		dSensorLongitude;
   double 		dPitch;
   double 		dRoll;
   double 		dHeading;
   double 		dHeave;
} XTF7125BATHYDATASNIPPETS;

typedef struct
{
	unsigned short 	uiType;
	int				iNbTimes;
} T_TYPEOFDATAGRAMNOTTRANSLATED;

typedef struct {
	   char				cNom[50],			// Label de la variable
					    cTag[50],			// Tag identifiant de la variable
					    cUnit[50],			// Unit� de la variable
						cType[30],			// Type WORD, String, ...
						cNomPathFileBin[300];	// Nom et chemin du fichier Bin
	   FILE				*ptr_fpBin;			// Pointeur du fichier de signaux
	   BOOL 			bFlagConstant;	// Indicateur de constance de la variable
	   unsigned short	usSize;				// n octets
	   unsigned short	usNbElem;			// Nombre de variables �l�mentaires
	   void				*ptr_vPrevVal;	// Valeur pr�c�dente de lecture
	   float			fScaleFactor;		// yVraie = Val*S_F + A_O
	   float			fAddOffset;			//
} T_XTFVARXML;

typedef struct {
	   char			*ptr_cNomHeader,			// Nom du Paquet
					*ptr_cCommentsHeader,		// Commentaires du paquet
					*ptr_cLabelHeader,			// Label du paquet
					//*ptr_cConstructorHeader,	// Nom du constructeur.
					//*ptr_cModelHeader,		// Mod�le du syst�me.
					//*ptr_cSerialNumberHeader,	// SN du syst�me.
					*ptr_cNomIndexTable;		// Nom de la variable d'indexation des
	   short		sNbVarSignal;				// Nombre de variables de type Signal
	   short		sNbVarImage;				// Nombre de variables de type Image
	   int			iNbDatagram;				// Nombre d'occurrence des paquets.
	   T_XTFVARXML	*strTabSignalXML;				// Description des variables du Header
	   T_XTFVARXML	*strTabImageXML;				// Description des images du Header
} T_XTFRECORDXML;

// Description unitaire des signaux
typedef struct  {
	char 			*cTabNomVar,
					*cTabNomType;
	float 			fScaleFactor;
	float 			fAddOffset;
} T_DESC_SIGNAL_OR_IMAGE;

// Description unitaire des signaux
typedef struct  {
	unsigned short 				usNbVarSignal;
	unsigned short 				usNbVarImage;
	short 						sNumIndexTable;
	T_DESC_SIGNAL_OR_IMAGE  	*strDescSignal;
	T_DESC_SIGNAL_OR_IMAGE  	*strDescImage;
} T_DESC_TAB_SIGANDIMG;



//-Paquet 7004 : partie RTH------------------------
typedef struct {
	u64		SonarId;	// Sonar serial number
	u32		N;			// Number of receiver beams
} T_XTF_7004_RECORD_RTH;

// Paquet 7004 : partie RD
typedef struct {
	f32		*BeamVerticalDirectionAngle;
	f32		*BeamHorizontalDirectionAngle;
	f32		*BeamWidthY_3dB;
	f32		*BeamWidthX_3dB;
} T_XTF_7004_RECORD_RD;

#pragma pack() // peut etre remplace par l'option de compil -fpack-struct



#endif

