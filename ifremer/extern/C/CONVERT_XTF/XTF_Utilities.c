/*
 * XTF_utilities.c
 *
 *  Created on: 9 oct. 2008
 *      Author: rgallou
 */
#pragma pack(1) // peut etre remplace par l'option de compil -fpack-struct pour gcc

#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include "string.h"
#include "stdlib.h"
#include "math.h"
#include <errno.h>
#include "stdio.h"
#include "Generic_Utilities.h"    // Contenu dans la librairie libCONVERT_utils.a
#include "XTF_processDatagrams.h"
#include "XTF_Datagrams.h"
#include "main_XTF.h"
#include "XTF_Utilities.h"


//----------------------------------------------------------------
// Fonction :	utilXTF_iSaveXMLSignalFile
// Objet 	:	Sauvegarde de la structure XML DOM dans un fichier
//				ad hoc.
// Modification : 15/10/08
// Auteur 	: GLU
//----------------------------------------------------------------
int utilXTF_iSaveXMLSignalFile(T_XTFRECORDXML *strXTFHeaderXML)
{
	char 	*cNomFicXML,
			*ptr_cNomHeader,
			cNbElem[20],
			cNomFicBin[50],
			*cValConstant = NULL,
			*cTypeMatLab;

    int 	iSize,
			iLoop,
			iRet = 0,
			iSizeRep,
			iLenVarName,
			iSizeNameHeader;

    char *ptr_cNomFicPathBin;

    unsigned short 	usNbElem,
					usTailleEnByte;

    FILE	*fpXML;

    extern BOOL	G_bFlagSortIncPing;


    // Fermeture syst�matique des
    utilXTF_iCloseBinFile(strXTFHeaderXML);

    ptr_cNomHeader = strXTFHeaderXML->ptr_cNomHeader;
	iSize = strlen(G_cRepData) + 2 + strlen(ptr_cNomHeader) + 20;
    cNomFicXML = calloc(iSize, sizeof(char));
	sprintf(cNomFicXML, "%s\\XTF_%s%s", G_cRepData, ptr_cNomHeader, ".xml");
	printf("-- Creation du fichier xml : %s \n", cNomFicXML);

	// Cr�ation de l'ent�te de doc XML
    fpXML = fopen(cNomFicXML, "w+");
    fprintf(fpXML, "%s", "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r");
    fprintf(fpXML, "%s", "<ROOT>\r");
    fprintf(fpXML, "%s%s%s", "\t<Title>", strXTFHeaderXML->ptr_cNomHeader, "</Title>\r");
    fprintf(fpXML, "%s%s%s", "\t<Constructor>", "Triton", "</Constructor>\r");
    fprintf(fpXML, "%s%s%s", "\t<Model>", G_cSonarName, "</Model>\r");
    fprintf(fpXML, "%s%d%s", "\t<SystemSerialNumber>", G_u32SonarType, "</SystemSerialNumber>\r");
    fprintf(fpXML, "%s%d%s", "\t<NbDatagrams>", strXTFHeaderXML->iNbDatagram, "</NbDatagrams>\r");
    fprintf(fpXML, "%s%s%s", "\t<TimeOrigin>", "01/01/-4713", "</TimeOrigin>\r");
    fprintf(fpXML, "%s%s%s", "\t<Comments>", strXTFHeaderXML->ptr_cCommentsHeader, "</Comments>\r");


	if (strXTFHeaderXML->sNbVarSignal > 0)
	{
		fprintf(fpXML, "%s", "\t<Variables>\r");

		// Traitement sur l'ensemble des variables.
		for (iLoop=0; iLoop<strXTFHeaderXML->sNbVarSignal; iLoop++)
		{
			if (strXTFHeaderXML->strTabSignalXML[iLoop].ptr_fpBin != NULL)
			{
			  iRet = fclose(strXTFHeaderXML->strTabSignalXML[iLoop].ptr_fpBin);
			}

			cTypeMatLab = (char*)malloc(10*sizeof(char));
			iRet = util_cConvertTypeLabel(	strXTFHeaderXML->strTabSignalXML[iLoop].cType,
											cTypeMatLab);
			fprintf(fpXML, "%s", "\t\t<item>\r");
			fprintf(fpXML, "%s%s%s", "\t\t\t<Name>", strXTFHeaderXML->strTabSignalXML[iLoop].cNom, "</Name>\r");
			fprintf(fpXML, "%s%s%s", "\t\t\t<Storage>", cTypeMatLab, "</Storage>\r");
			if (strXTFHeaderXML->strTabSignalXML[iLoop].usNbElem > 1)
			{
				sprintf(cNbElem, "%d", strXTFHeaderXML->strTabSignalXML[iLoop].usNbElem);
				fprintf(fpXML, "%s%s%s", "\t\t\t<NbElem>", cNbElem, "</NbElem>\r");
			}
			//fprintf(fpXML, "%s%s%s", "\t\t\t<Unit>", strXTFHeaderXML->strTabSignalXML[iLoop].cUnit, "</Unit>\r");
			//fprintf(fpXML, "%s%s%s", "\t\t\t<Tag>", strXTFHeaderXML->strTabSignalXML[iLoop].cTag, "</Tag>\r");

			if (strXTFHeaderXML->strTabSignalXML[iLoop].bFlagConstant == TRUE)
			{
				// R�cup�ration de la variable d�tect�e comme constante.
				usNbElem = strXTFHeaderXML->strTabSignalXML[iLoop].usNbElem;
				usTailleEnByte = util_usNbOctetTypeVar(strXTFHeaderXML->strTabSignalXML[iLoop].cType);
				// Allocation de la valeur � r�cup�rer selon la taille. Si elle est
				// du type Chaine, elle doit contenir autant d'�l�ments que d�clar�s.
				if (strcmp(cTypeMatLab, "char"))
				{
					cValConstant = (char*)malloc(256*sizeof(char));
				}
				else
				{
					cValConstant = (char*)malloc(usNbElem*sizeof(char));
				}
				iRet = util_cValVarConstant(strXTFHeaderXML->strTabSignalXML[iLoop].cType,
											strXTFHeaderXML->strTabSignalXML[iLoop].ptr_vPrevVal,
											(int)strXTFHeaderXML->strTabSignalXML[iLoop].usNbElem,
											cValConstant);
				fprintf(fpXML, "%s%s%s", "\t\t\t<Constant>", cValConstant, "</Constant>\r");
				free(cValConstant);
			}
			else
			{
				sprintf(cNomFicBin, "%s%s",strXTFHeaderXML->strTabSignalXML[iLoop].cNom, ".bin");
				fprintf(fpXML, "%s%s%s", "\t\t\t<FileName>", cNomFicBin, "</FileName>\r");
			}
			if (strXTFHeaderXML->strTabSignalXML[iLoop].bFlagConstant == TRUE)
			{
			   // Effacement du fichier et enregistrement de la balise Constant
			   // dans l'arbre XML
			   iLenVarName = strlen(strXTFHeaderXML->strTabSignalXML[iLoop].cNom);
			   iSizeRep = strlen(G_cRepData);
			   iSizeNameHeader = strlen(strXTFHeaderXML->ptr_cNomHeader);
			   // Ajout d'une marge au cas o� certains caract�res soient doubl�s soient doubl�s.
			   ptr_cNomFicPathBin = malloc( (iSizeRep + 1 + iSizeNameHeader + 1 + iLenVarName + 50)*sizeof(char));
			   sprintf(ptr_cNomFicPathBin, "%s\\XTF_%s\\%s%s", G_cRepData, strXTFHeaderXML->ptr_cNomHeader, strXTFHeaderXML->strTabSignalXML[iLoop].cNom, ".bin");
			   if (DEBUG)
			   {
				   printf("Effacement du fichier : %s\n", ptr_cNomFicPathBin);
			   }
			   iRet = remove((const char *)ptr_cNomFicPathBin);
			   if (ptr_cNomFicPathBin != NULL) {
				   free(ptr_cNomFicPathBin);
				   ptr_cNomFicPathBin = NULL;
			   }
			}

			if (strXTFHeaderXML->strTabSignalXML[iLoop].fScaleFactor != 1.0)
				fprintf(fpXML, "%s%g%s", "\t\t\t<ScaleFactor>", strXTFHeaderXML->strTabSignalXML[iLoop].fScaleFactor, "</ScaleFactor>\r");
			if (strXTFHeaderXML->strTabSignalXML[iLoop].fAddOffset != 0.0)
				fprintf(fpXML, "%s%f%s", "\t\t\t<AddOffset>", strXTFHeaderXML->strTabSignalXML[iLoop].fAddOffset, "</AddOffset>\r");

			// On n'�crit la variable que si elle n'est pas conforme.
			if (G_bFlagSortIncPing == FALSE)
			{
				fprintf(fpXML, "%s%s%s", "\t\t\t<Direction>",  "FirstValue=LastPing", "</FileName>\r");
			}
			fprintf(fpXML, "%s", "\t\t</item>\r");
			free(cTypeMatLab);

		} // Fin de la boucle sur les variables
		fprintf(fpXML, "%s", "\t</Variables>\r");
    } // Fin de l'�criture des variables


	// Boucle sur les images
	if (strXTFHeaderXML->sNbVarImage > 0)
	{
		fprintf(fpXML, "%s", "\t<Tables>\r");

		// Traitement sur l'ensemble des variables.
		for (iLoop=0; iLoop<strXTFHeaderXML->sNbVarImage; iLoop++)
		{
			if (strXTFHeaderXML->strTabImageXML[iLoop].ptr_fpBin != NULL)
			{
			  iRet = fclose(strXTFHeaderXML->strTabImageXML[iLoop].ptr_fpBin);
			}

			cTypeMatLab = (char*)malloc(10*sizeof(char));
			iRet = util_cConvertTypeLabel(	strXTFHeaderXML->strTabImageXML[iLoop].cType,
											cTypeMatLab);

			fprintf(fpXML, "%s", "\t\t<item>\r");
			fprintf(fpXML, "%s%s%s", "\t\t\t<Name>", strXTFHeaderXML->strTabImageXML[iLoop].cNom, "</Name>\r");
			fprintf(fpXML, "%s%s%s", "\t\t\t<Storage>", cTypeMatLab, "</Storage>\r");
			if (strXTFHeaderXML->strTabImageXML[iLoop].usNbElem > 1)
			{
				sprintf(cNbElem, "%d", strXTFHeaderXML->strTabImageXML[iLoop].usNbElem);
				fprintf(fpXML, "%s%s%s", "\t\t\t<NbElem>", cNbElem, "</NbElem>\r");
			}
			//fprintf(fpXML, "%s%s%s", "\t\t\t<Unit>", strXTFHeaderXML->strTabImageXML[iLoop].cUnit, "</Unit>\r");
			//fprintf(fpXML, "%s%s%s", "\t\t\t<Tag>", strXTFHeaderXML->strTabImageXML[iLoop].cTag, "</Tag>\r");

			if (strXTFHeaderXML->strTabImageXML[iLoop].bFlagConstant == TRUE)
			{
				// R�cup�ration de la variable d�tect�e comme constante.
				usNbElem 		= strXTFHeaderXML->strTabImageXML[iLoop].usNbElem;
				usTailleEnByte 	= util_usNbOctetTypeVar(strXTFHeaderXML->strTabImageXML[iLoop].cType);
				// Allocation de la valeur � r�cup�rer selon la taille. Si elle est
				// du type Chaine, elle doit contenir autant d'�l�ments que d�clar�s.
				if (strcmp(cTypeMatLab, "char"))
				{
					cValConstant = (char*)malloc(256*sizeof(char));
				}
				else
				{
					cValConstant = (char*)malloc(usNbElem*sizeof(char));
				}
				iRet = util_cValVarConstant(strXTFHeaderXML->strTabImageXML[iLoop].cType,
											strXTFHeaderXML->strTabImageXML[iLoop].ptr_vPrevVal,
											(int)strXTFHeaderXML->strTabImageXML[iLoop].usNbElem,
											cValConstant);
				fprintf(fpXML, "%s%s%s", "\t\t\t<Constant>", cValConstant, "</Constant>\r");
				free(cValConstant);
			}
			else
			{
				sprintf(cNomFicBin, "%s%s",strXTFHeaderXML->strTabImageXML[iLoop].cNom, ".bin");
				fprintf(fpXML, "%s%s%s", "\t\t\t<FileName>", cNomFicBin, "</FileName>\r");
			}
			if (strXTFHeaderXML->strTabImageXML[iLoop].bFlagConstant == TRUE)
			{
			   // Effacement du fichier et enregistrement de la balise Constant
			   // dans l'arbre XML
			   iLenVarName = strlen(strXTFHeaderXML->strTabImageXML[iLoop].cNom);
			   iSizeRep = strlen(G_cRepData);
			   iSizeNameHeader = strlen(strXTFHeaderXML->ptr_cNomHeader);
			   // Ajout d'une marge au cas o� certains caract�res soient doubl�s soient doubl�s.
			   ptr_cNomFicPathBin = malloc( (iSizeRep + 1 + iSizeNameHeader + 1 + iLenVarName + 50)*sizeof(char));
			   sprintf(ptr_cNomFicPathBin, "%s\\XTF_%s\\%s%s", G_cRepData, strXTFHeaderXML->ptr_cNomHeader, strXTFHeaderXML->strTabImageXML[iLoop].cNom, ".bin");
			   if (DEBUG)
			   {
				   printf("Effacement du fichier : %s\n", ptr_cNomFicPathBin);
			   }
			   iRet = remove((const char *)ptr_cNomFicPathBin);
			   if (ptr_cNomFicPathBin != NULL) {
				   free(ptr_cNomFicPathBin);
				   ptr_cNomFicPathBin = NULL;
			   }
			}

			if (strXTFHeaderXML->strTabImageXML[iLoop].fScaleFactor != 1.0)
				fprintf(fpXML, "%s%g%s", "\t\t\t<ScaleFactor>", strXTFHeaderXML->strTabImageXML[iLoop].fScaleFactor, "</ScaleFactor>\r");
			if (strXTFHeaderXML->strTabImageXML[iLoop].fAddOffset != 0.0)
				fprintf(fpXML, "%s%f%s", "\t\t\t<AddOffset>", strXTFHeaderXML->strTabImageXML[iLoop].fAddOffset, "</AddOffset>\r");

			// On n'�crit la variable que si elle n'est pas conforme.
			if (G_bFlagSortIncPing == FALSE)
			{
				fprintf(fpXML, "%s%s%s", "\t\t\t<Direction>",  "FirstValue=LastPing", "</FileName>\r");
			}
			fprintf(fpXML, "%s", "\t\t</item>\r");
			free(cTypeMatLab);

		} // Fin de la boucle sur les images
		fprintf(fpXML, "%s", "\t</Tables>\r");
	}


	fprintf(fpXML, "%s", "</ROOT>\r");
	fclose(fpXML);

	free(cNomFicXML);


	return iRet;


} //utilXTF_iSaveXMLSignalFile

//----------------------------------------------------------------
// Fonction :	utilXTF_iCloseBinFile
// Objet 	:	Fermeture (et effacement) des fichiers des signaux
//				de n'importe quel paquet.
// Modification : 15/10/08
// Auteur 	: GLU
//----------------------------------------------------------------
int utilXTF_iCloseBinFile(T_XTFRECORDXML *strXTFHeaderXML)
{
	int iLoop,
		iSizeRep,
		iLenVarName,
		iSizeNameHeader,
		iRet;

	char *ptr_cNomFicPathBin;

	// Traitement des variables de type Signal.
	// Fermeture, effacement, �criture XML les paquets de donn�es
	for (iLoop=0; iLoop<strXTFHeaderXML->sNbVarSignal; iLoop++)
	{
		   if (strXTFHeaderXML->strTabSignalXML[iLoop].ptr_fpBin != NULL)
		   {
			  iRet = fclose(strXTFHeaderXML->strTabSignalXML[iLoop].ptr_fpBin);
			  strXTFHeaderXML->strTabSignalXML[iLoop].ptr_fpBin = NULL;
		   }
		   if (strXTFHeaderXML->strTabSignalXML[iLoop].bFlagConstant == TRUE)
		   {
			   // Effacement du fichier et enregistrement de la balise Constant
			   // dans l'arbre XML
			   iLenVarName = strlen(strXTFHeaderXML->strTabSignalXML[iLoop].cNom);
			   iSizeRep = strlen(G_cRepData);
			   iSizeNameHeader = strlen(strXTFHeaderXML->ptr_cNomHeader);
			   // Ajout d'une marge au cas o� certains caract�res soient doubl�s soient doubl�s.
			   ptr_cNomFicPathBin = malloc( (iSizeRep + 1 + iSizeNameHeader + 1 + iLenVarName + 50)*sizeof(char));
			   sprintf(ptr_cNomFicPathBin, "%s\\XTF_%s\\%s%s", G_cRepData, strXTFHeaderXML->ptr_cNomHeader, strXTFHeaderXML->strTabSignalXML[iLoop].cNom, ".bin");
			   if (DEBUG)
			   {
				   printf("Effacement du fichier : %s\n", ptr_cNomFicPathBin);
			   }
			   iRet = remove((const char *)ptr_cNomFicPathBin);
			   if (ptr_cNomFicPathBin != NULL) {
				   free(ptr_cNomFicPathBin);
				   ptr_cNomFicPathBin = NULL;
			   }
		   }
	} // Fin de la boucle sur les variables du paquet Bin

	// Traitement des variables de type Image.
	// Fermeture, effacement, �criture XML les paquets de donn�es
	for (iLoop=0; iLoop<strXTFHeaderXML->sNbVarImage; iLoop++)
	{
		   if (strXTFHeaderXML->strTabImageXML[iLoop].ptr_fpBin != NULL)
		   {
			  iRet = fclose(strXTFHeaderXML->strTabImageXML[iLoop].ptr_fpBin);
		   }
		   if (strXTFHeaderXML->strTabImageXML[iLoop].bFlagConstant == TRUE)
		   {
			   // Effacement du fichier et enregistrement de la balise Constant
			   // dans l'arbre XML
			   iLenVarName = strlen(strXTFHeaderXML->strTabImageXML[iLoop].cNom);
			   iSizeRep = strlen(G_cRepData);
			   iSizeNameHeader = strlen(strXTFHeaderXML->ptr_cNomHeader);
			   // Ajout d'une marge au cas o� certains caract�res soient doubl�s soient doubl�s.
			   ptr_cNomFicPathBin = malloc( (iSizeRep + 1 + iSizeNameHeader + 1 + iLenVarName + 50)*sizeof(char));
			   sprintf(ptr_cNomFicPathBin, "%s\\XTF_%s\\%s%s", G_cRepData, strXTFHeaderXML->ptr_cNomHeader, strXTFHeaderXML->strTabImageXML[iLoop].cNom, ".bin");
			   if (DEBUG)
			   {
				   printf("Effacement du fichier : %s\n", ptr_cNomFicPathBin);
			   }
			   iRet = remove((const char *)ptr_cNomFicPathBin);
			   if (ptr_cNomFicPathBin != NULL) {
				   free(ptr_cNomFicPathBin);
				   ptr_cNomFicPathBin = NULL;
			   }
		   }
	} // Fin de la boucle sur les variables du paquet Bin

	return EXIT_SUCCESS;

} // utilXTF_iCloseBinFile

//////////////////////////////////////////////////////////////////////
// Fonction		:	utilXTF_iCreateDirDatagram
// Objet		:	Cr�ation du r�pertoire d�di�es aux donn�es d'un
//					Tuple
// Modification :	29/09/09
// Auteur		:	GLU
//////////////////////////////////////////////////////////////////////
int utilXTF_iCreateDirDatagram(char *cRepData,
							char *cNomDatagram,
							char *cRepertoire)
{

int 	iErr;

	// Si Extension = '.XTF'
//	if (!stricmp(G_cFileExtension, "xtf"))
//		sprintf(cRepertoire, "%s\\XTF_%s", cRepData, cNomDatagram);
//	else
//	{
//		printf("L'extension du fichier n'est pas reconnue : %s\n", G_cFileExtension);
//		return EXIT_FAILURE;
//	}


	if (DEBUG)
	   printf("-- Directory Creation : %s\n",cRepertoire);

	// Cr�ation du r�pertoire
	iErr = mkdir(cRepertoire);
	if (iErr != 0)
	{
	   switch (errno)
	   {
		   //"File exists"
		   case EEXIST:
			   break;
		   default:
			   printf("Error in Directory creation %s\n", strerror(errno));
			   return EXIT_FAILURE;
			   break;
	   }
	}

	return EXIT_SUCCESS;

} // utilXTF_iCreateDirDatagram

//////////////////////////////////////////////////////////////////////
// Fonction		:	utilXTF_initXMLRecord
// Objet		:	Initialisation de la structure XML des signaux et images
//					du fichier.
// Modification :	13/05/2011
// Auteur		:	GLU
//////////////////////////////////////////////////////////////////////
int utilXTF_initXMLRecord(	T_DESC_TAB_SIGANDIMG strDescTabSigOrImg,
							char *cTitleDatagram,
							char *cNomDatagram,
							char *cComments,
							T_XTFRECORDXML *strXTFRecordXML)

{
	char	*ptr_cRepertoire,
			cNomFicPathBin[300];

    int 	iSizeRep,
			iRet,
			iLen,
			iVarSignal,
			iVarImage;

	unsigned short 	usTaille,
					usNbVarSignal,
					usNbVarImage,
					usNbVarImage2;

	short			sNumIndex;


	sNumIndex 		= strDescTabSigOrImg.sNumIndexTable;
	usNbVarSignal	= strDescTabSigOrImg.usNbVarSignal;
	usNbVarImage	= strDescTabSigOrImg.usNbVarImage;

	strXTFRecordXML->sNbVarImage 		= usNbVarImage;


	// Initialisation des structures de pilotage des fichiers
	// XML et Bin .
	strXTFRecordXML->sNbVarSignal 		= usNbVarSignal;
	strXTFRecordXML->sNbVarImage 		= usNbVarImage;
    strXTFRecordXML->strTabSignalXML	= (T_XTFVARXML *)malloc(strXTFRecordXML->sNbVarSignal*sizeof(T_XTFVARXML));
    strXTFRecordXML->strTabImageXML		= (T_XTFVARXML *)malloc(strXTFRecordXML->sNbVarImage*sizeof(T_XTFVARXML));

	iLen = strlen(cNomDatagram)+1;
	strXTFRecordXML->ptr_cNomHeader = malloc(iLen*sizeof(char));
	sprintf(strXTFRecordXML->ptr_cNomHeader, "%s", cNomDatagram);

	iLen = strlen(cTitleDatagram)+1;
	strXTFRecordXML->ptr_cLabelHeader = malloc(iLen*sizeof(char));
	sprintf(strXTFRecordXML->ptr_cLabelHeader, "%s", cTitleDatagram);

	iLen = strlen(cComments)+1;
	strXTFRecordXML->ptr_cCommentsHeader = malloc(iLen*sizeof(char));
	sprintf(strXTFRecordXML->ptr_cCommentsHeader, "%s", cComments);

	// Remplissage du nom de la variable d'indexation des tables.
	if (sNumIndex != -1)
	{
		iLen = strlen(strDescTabSigOrImg.strDescSignal[sNumIndex].cTabNomVar)+1;
		strXTFRecordXML->ptr_cNomIndexTable = malloc(iLen*sizeof(char));
		sprintf(strXTFRecordXML->ptr_cNomIndexTable, "%s", strDescTabSigOrImg.strDescSignal[sNumIndex].cTabNomVar);
	}
	else
	{
		iLen = strlen("NoneIndex")+1;
		strXTFRecordXML->ptr_cNomIndexTable = malloc(iLen*sizeof(char));
		sprintf(strXTFRecordXML->ptr_cNomIndexTable, "%s", "NoneIndex");
	}
	// Affectation et cr�ation du repertoire du Datagramme.
	iSizeRep = ( strlen(G_cRepData) + strlen( cNomDatagram));
	// Ajout d'une marge au cas o� certains caract�res soient doubl�s soient doubl�s.
	ptr_cRepertoire = malloc( (iSizeRep + 1 + 300 + 4)*sizeof(char));
	sprintf(ptr_cRepertoire, "%s\\XTF_%s", G_cRepData, cNomDatagram);
	iRet = utilXTF_iCreateDirDatagram(	G_cRepData,
										strXTFRecordXML->ptr_cNomHeader,
										ptr_cRepertoire);

	// Initialisation g�n�rale des variables de type Signal
	for (iVarSignal=0; iVarSignal<strXTFRecordXML->sNbVarSignal; iVarSignal++)
	{
	   strXTFRecordXML->strTabSignalXML[iVarSignal].ptr_fpBin = NULL;
	   strXTFRecordXML->strTabSignalXML[iVarSignal].bFlagConstant = TRUE;
	   strXTFRecordXML->strTabSignalXML[iVarSignal].usNbElem = 1;
	   sprintf(strXTFRecordXML->strTabSignalXML[iVarSignal].cNom, "%s", strDescTabSigOrImg.strDescSignal[iVarSignal].cTabNomVar);
	   sprintf(strXTFRecordXML->strTabSignalXML[iVarSignal].cType, "%s", strDescTabSigOrImg.strDescSignal[iVarSignal].cTabNomType);
	   sprintf(strXTFRecordXML->strTabSignalXML[iVarSignal].cTag, "%s", "TODO");
	   sprintf(strXTFRecordXML->strTabSignalXML[iVarSignal].cUnit, "%s", "TODO");
	   strXTFRecordXML->strTabSignalXML[iVarSignal].fScaleFactor = strDescTabSigOrImg.strDescSignal[iVarSignal].fScaleFactor;
	   strXTFRecordXML->strTabSignalXML[iVarSignal].fAddOffset = strDescTabSigOrImg.strDescSignal[iVarSignal].fAddOffset;
	   usTaille = util_usNbOctetTypeVar(strDescTabSigOrImg.strDescSignal[iVarSignal].cTabNomType);
	   strXTFRecordXML->strTabSignalXML[iVarSignal].usSize = usTaille;
	   iLen = strlen(strXTFRecordXML->strTabSignalXML[iVarSignal].cNom);
	   // Ajout d'une marge au cas o� certains caract�res soient doubl�s soient doubl�s.
	   sprintf(cNomFicPathBin, "%s\\%s%s", ptr_cRepertoire, strXTFRecordXML->strTabSignalXML[iVarSignal].cNom, ".bin");
	   sprintf(strXTFRecordXML->strTabSignalXML[iVarSignal].cNomPathFileBin, "%s", cNomFicPathBin);
	   strXTFRecordXML->strTabSignalXML[iVarSignal].ptr_fpBin = fopen(cNomFicPathBin, "w+b");
	   if (!strXTFRecordXML->strTabSignalXML[iVarSignal].ptr_fpBin)
	   {
		   printf("%s -- Error in Signal creation  : %s\n", __FILE__, cNomFicPathBin);
		   return EXIT_FAILURE;
	   }
	}

	// Initialisation g�n�rale des variables de type Image
	for (iVarImage=0; iVarImage<strXTFRecordXML->sNbVarImage; iVarImage++)
	{
	   strXTFRecordXML->strTabImageXML[iVarImage].ptr_fpBin = 0;
	   strXTFRecordXML->strTabImageXML[iVarImage].bFlagConstant = TRUE;
	   strXTFRecordXML->strTabImageXML[iVarImage].usNbElem = 1;
	   sprintf(strXTFRecordXML->strTabImageXML[iVarImage].cNom, "%s", strDescTabSigOrImg.strDescImage[iVarImage].cTabNomVar);
	   sprintf(strXTFRecordXML->strTabImageXML[iVarImage].cType, "%s", strDescTabSigOrImg.strDescImage[iVarImage].cTabNomType);
	   sprintf(strXTFRecordXML->strTabImageXML[iVarImage].cTag, "%s", "TODO");
	   sprintf(strXTFRecordXML->strTabImageXML[iVarImage].cUnit, "%s", "TODO");
	   strXTFRecordXML->strTabImageXML[iVarImage].fScaleFactor 	= strDescTabSigOrImg.strDescImage[iVarImage].fScaleFactor;
	   strXTFRecordXML->strTabImageXML[iVarImage].fAddOffset 		= strDescTabSigOrImg.strDescImage[iVarImage].fAddOffset;
	   usTaille = util_usNbOctetTypeVar(strDescTabSigOrImg.strDescImage[iVarImage].cTabNomType);
	   strXTFRecordXML->strTabImageXML[iVarImage].usSize = usTaille;
	   iLen = strlen(strXTFRecordXML->strTabImageXML[iVarImage].cNom);
	   // Ajout d'une marge au cas o� certains caract�res soient doubl�s soient doubl�s.
	   sprintf(cNomFicPathBin, "%s\\%s%s", ptr_cRepertoire, strXTFRecordXML->strTabImageXML[iVarImage].cNom, ".bin");
	   sprintf(strXTFRecordXML->strTabImageXML[iVarImage].cNomPathFileBin, "%s", cNomFicPathBin);
	   strXTFRecordXML->strTabImageXML[iVarImage].ptr_fpBin = fopen(cNomFicPathBin, "w+b");
	   if (!strXTFRecordXML->strTabImageXML[iVarImage].ptr_fpBin)
	   {
		   printf("%s -- Error in Image creation  : %s\n", __FILE__, cNomFicPathBin);
		   return EXIT_FAILURE;
	   }
	}

//	for (iVarImage2=0; iVarImage2<strXTFRecordXML->sNbVarImage2; iVarImage2++)
//	{
//		strXTFRecordXML->strTabImage2XML[iVarImage2].ptr_fpBin = 0;
//		strXTFRecordXML->strTabImage2XML[iVarImage2].bFlagConstant = TRUE;
//		strXTFRecordXML->strTabImage2XML[iVarImage2].usNbElem = 1;
//		sprintf(strXTFRecordXML->strTabImage2XML[iVarImage2].cNom, "%s", strDescTabSigOrImg.strDescImage2[iVarImage2].cTabNomVar);
//		sprintf(strXTFRecordXML->strTabImage2XML[iVarImage2].cType, "%s", strDescTabSigOrImg.strDescImage2[iVarImage2].cTabNomType);
//		sprintf(strXTFRecordXML->strTabImage2XML[iVarImage2].cTag, "%s", "TODO");
//		sprintf(strXTFRecordXML->strTabImage2XML[iVarImage2].cUnit, "%s", "TODO");
//	    strXTFRecordXML->strTabImage2XML[iVarImage2].fScaleFactor 	= strDescTabSigOrImg.strDescImage2[iVarImage2].fScaleFactor;
//	    strXTFRecordXML->strTabImage2XML[iVarImage2].fAddOffset 	= strDescTabSigOrImg.strDescImage2[iVarImage2].fAddOffset;
//		usTaille = util_usNbOctetTypeVar(strDescTabSigOrImg.strDescImage2[iVarImage2].cTabNomType);
//		strXTFRecordXML->strTabImage2XML[iVarImage2].usSize = usTaille;
//		// Ajout d'une marge au cas o� certains caract�res soient doubl�s soient doubl�s.
//		sprintf(cNomFicPathBin, "%s\\%s%s", ptr_cRepertoire, strXTFRecordXML->strTabImage2XML[iVarImage2].cNom, ".bin");
//		sprintf(strXTFRecordXML->strTabImage2XML[iVarImage2].cNomPathFileBin, "%s", cNomFicPathBin);
//		strXTFRecordXML->strTabImage2XML[iVarImage2].ptr_fpBin = fopen(cNomFicPathBin, "w+b");
//		if (!strXTFRecordXML->strTabImage2XML[iVarImage2].ptr_fpBin)
//		{
//		   printf("%s -- Error in Signal Creation File  : %s\n", __FILE__, cNomFicPathBin);
//		   return EXIT_FAILURE;
//		}
//	}
	free(ptr_cRepertoire);
	return EXIT_SUCCESS;

} // utilXTF_initXMLRecord



#pragma pack() // peut etre remplace par l'option de compil -fpack-struct

