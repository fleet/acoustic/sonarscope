/*
 * XTF_utilities.h
 *
 *  Created on: 6 nov. 2008
 *      Author: dwolyniec
 */

#ifndef XTF_UTILITIES_H_
#define XTF_UTILITIES_H_

#include "XTF_Datagrams.h"


off64_t All_trouveDatagram(FILE *,unsigned char *,int);

//int utilXTF_cValVarFromFile(T_XTFVARXML strXTFVarXML,
//							char *cValeur);

int utilXTF_iSaveAndCloseFiles(	char *cRepData,
								T_XTFRECORDXML *strXTFRecordXML);

int util_iGetIndianAll(char *nomFic);

int util_iCreateDirDatagram(char *cRepData,
							char *cNomDatagram,
							char *cRepertoire);

#endif /* XTF_UTILITIES_H_ */
