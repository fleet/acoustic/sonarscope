/*
 * XTF_processBathyHeader.c
 *
 *  Created on: 13 oct. 2008
 *      Author: rgallou
 */
#pragma pack(1) // peut etre remplace par l'option de compil -fpack-struct pour gcc

#include <sys/types.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "XTF_process7125BathyHeader.h"
#include "XTF_processBathyHeader.h"
#include "XTF_processBathymetry.h"
#include "XTF_Datagrams.h"
#include "XTF_Utilities.h"
#include "main_XTF.h"

//#define NB_SIGNAL 		1
//#define NB_IMAGE 		0
//#define NB_IMAGE2 		0
//#define NUM_INDEX_TABLE	-1
//
////----------------------------------------------------------------
//// Fonction :	iInitXMLBin7125Bathymetry
//// Objet 	:	Initialisation de la structure �quivalent aux donn�es
////				Bathymetry pour la manipulation des fichiers bin et XML.
////				Les donn�es d'Imagerie sont incluses dans le paquet
////				Ping Chan Info.
//// Modification : 15/10/08
//// Auteur 	: GLU
////----------------------------------------------------------------
//int iInitXMLBin7125Bathymetry(	char *cTitleDatagram,
//								char *cNomDatagram,
//								char *cComments,
//								T_XTFRECORDXML *strXTFRecordXML)
//{
//	int iRet;
//
//	T_DESC_SIGNAL_OR_IMAGE strDescriptionSignal[NB_SIGNAL] = {
//			{	"Bathymetry","Byte",			1.0, 0.0}};
//
//
//	T_DESC_SIGNAL_OR_IMAGE strDescriptionImage[NB_IMAGE] = {};
//	T_DESC_SIGNAL_OR_IMAGE strDescriptionImage2[NB_IMAGE2] = {};
//
//	T_DESC_TAB_SIGANDIMG strDescriptionTab = {NB_SIGNAL,
//												NB_IMAGE,
//												NUM_INDEX_TABLE,
//												strDescriptionSignal,
//												strDescriptionImage};
//
//	// Lancement de l'initialisation des structures de signaux : ouvertures des
//	// fichiers, copie des structures, ...
//	iRet = utilXTF_initXMLRecord(	strDescriptionTab,
//									cTitleDatagram,
//									cNomDatagram,
//									cComments,
//									strXTFRecordXML);
//
//	return iRet;
//} // iInitXMLBin7125Bathymetry
//
//
//
////----------------------------------------------------------------
//// Fonction :	iWriteBin7125BathyHeader
//// Objet 	:	Ecriture des signaux dans les fichiers bin.
//// Modification : 15/10/08
//// Auteur 	: GLU
////----------------------------------------------------------------
//int 	iWriteBin7125BathyHeader(	int iNbHeader,
//									T_XTFRECORDXML *strXTFRecordXML,
//									XTFBATHHEADER *BathyHeader)
//{
//	char		*ptr_cRepertoire,
//				*ptr_cNomHeader,
//				*ptr_cNomFicPathBin;
//
//	int 		iLenVarName,
//				iSizeRep,
//				iErr,
//				iLoop;
//
//	// Pointeur pour avancer sur les variables automatiquement.
//	unsigned char			*ptrBathyHeader;
//	static unsigned char	*ptrPrevBathyHeader;
//	unsigned short 			usTailleVar,
//							usNbElem;
//
//	BOOL		bFlagConstant,
//				bFlagCompare;
//
//	static void 	*ptr_vValVariable,
//					*ptr_vPrevVal;
//
//	static DWORD 	iPrevPingNumber = 0;
//	DWORD		 	iPingNumber;
//
//	ptr_cNomHeader = strXTFRecordXML->ptr_cNomHeader;
////	if (iNbHeader == 0)
////	{
////		// Affectation et cr�ation du repertoire du Datagramme.
////		iSizeRep 		= ( strlen(G_cRepData) + strlen( ptr_cNomHeader));
////		// Ajout d'une marge au cas o� certains caract�res "soient doubl�s - soient doubl�s".
////		ptr_cRepertoire = malloc( (iSizeRep + 1 + 300 + 4)*sizeof(char));
////		iErr 			= utilXTF_iCreateDirDatagram(	G_cRepData,
////														strXTFRecordXML->ptr_cNomHeader,
////														ptr_cRepertoire);
////		if (DEBUG)
////		{
////		   printf("-- Creation du repertoire : %s\n", ptr_cRepertoire);
////		}
////
////
////		// Affectation des variables.
////		for (iLoop=0; iLoop<(int)strXTFRecordXML->sNbVarSignal; iLoop++)
////		{
////		   iLenVarName = strlen(strXTFRecordXML->strTabSignalXML[iLoop].cNom);
////		   // Ajout d'une marge au cas o� certains caract�res soient doubl�s soient doubl�s.
////		   ptr_cNomFicPathBin = malloc((iSizeRep + 1 + iLenVarName +1 + 4)*sizeof(char));
////		   sprintf(ptr_cNomFicPathBin, "%s\\%s%s", ptr_cRepertoire, strXTFRecordXML->strTabSignalXML[iLoop].cNom, ".bin");
////		   sprintf(ptr_cNomFicPathBin, "%s\\%s%s", ptr_cRepertoire, strXTFRecordXML->strTabSignalXML[iLoop].cNom, ".bin");
////		   strXTFRecordXML->strTabSignalXML[iLoop].ptr_fpBin = fopen(ptr_cNomFicPathBin, "w+b");
////		   if (!strXTFRecordXML->strTabSignalXML[iLoop].ptr_fpBin)
////		   {
////			   printf("%s -- Error in Signal Creation File  : %s\n", __FILE__, ptr_cNomFicPathBin);
////			   return EXIT_FAILURE;
////		   }
////		   free(ptr_cNomFicPathBin);
////		}
////
////		#ifndef DEBUG
////		   free(ptr_cRepertoire);
////		#endif
////	} // iNbHeader ==0
//
//	// Ecriture syst�matique dans le fichier binaire.
//	ptrBathyHeader = (unsigned char *)BathyHeader;
//	ptr_vValVariable = (void *)ptrBathyHeader;
//	ptr_vPrevVal = (void *)ptrPrevBathyHeader;
//	// Affectation des variables.
//	for (iLoop=0; iLoop<(int)strXTFRecordXML->sNbVarSignal; iLoop++) {
//	   if (iLoop > 0)
//	   {
//		   usTailleVar 	= strXTFRecordXML->strTabSignalXML[iLoop-1].usSize;
//		   usNbElem 	= strXTFRecordXML->strTabSignalXML[iLoop-1].usNbElem;
//	   }
//	   else
//	   {
//		   usTailleVar 	= 0; // Pour le premier paquet
//		   usNbElem 	= 0;
//	   }
//	   bFlagConstant = strXTFRecordXML->strTabSignalXML[iLoop].bFlagConstant;
//	   // Pointage de la variable (par sa taille et son occurrence)
//	   ptr_vValVariable = ptr_vValVariable + usTailleVar*usNbElem;
//	   if (iNbHeader > 0)
//	   {
//		   ptr_vPrevVal = ptr_vPrevVal + usTailleVar*usNbElem;
//		   bFlagCompare = util_bComparePrevAndCurrentVal(strXTFRecordXML->strTabSignalXML[iLoop].cType,
//											   ptr_vPrevVal,
//											   ptr_vValVariable,
//											   strXTFRecordXML->strTabSignalXML[iLoop].usNbElem);
//		   bFlagConstant = bFlagConstant && bFlagCompare;
//		   strXTFRecordXML->strTabSignalXML[iLoop].ptr_vPrevVal = ptr_vPrevVal;
//		   // Observation de l'ordre des Ping pour l'attribut Direction.
//		   if (!strcmp(strXTFRecordXML->strTabSignalXML[iLoop].cNom, "PingNumber"))
//		   {
//			   iPrevPingNumber = *((DWORD *)ptr_vPrevVal);
//			   if (iPrevPingNumber != 0)
//			   {
//				   iPingNumber = *((DWORD *)ptr_vValVariable);
//				   if (iPrevPingNumber > iPingNumber)
//				   {
//					   G_bFlagSortIncPing = FALSE;
//				   }
//				   else
//				   {
//					   G_bFlagSortIncPing = TRUE;
//				   }
//			   }
//		   }
//	   }
//	   strXTFRecordXML->strTabSignalXML[iLoop].bFlagConstant = bFlagConstant;
//	   // Pr�paration de l'�criture de la variable.
//	   usTailleVar 		= strXTFRecordXML->strTabSignalXML[iLoop].usSize;
//	   usNbElem 		= strXTFRecordXML->strTabSignalXML[iLoop].usNbElem;
//	   fwrite(ptr_vValVariable, usTailleVar, usNbElem, strXTFRecordXML->strTabSignalXML[iLoop].ptr_fpBin);
//
//	}
//	ptrPrevBathyHeader = malloc(sizeof(XTFBATHHEADER));
//	// Recopie en zone tampon des valeurs courantes.
//	memcpy(ptrPrevBathyHeader, ptrBathyHeader, sizeof(XTFBATHHEADER));
//
//	return EXIT_SUCCESS;
//
//} // iWriteBin7125BathyHeader


////////////////////////////////////////////////////////////////////////////////
// Function : vProcessMultibeamPing
// Put multibeam processing here.
// BathyHeader points to a 256-byte bathyheader structure.  That structure
// is followed by the bathy data itself.  When Isis saves bathy data,
// only one packet is saved at a time.  If there are two SEABAT heads,
// the data from each one will be stored with in different records and
// with their own XTFBATHHEADER structures.
////////////////////////////////////////////////////////////////////////////////

void vProcess7125MultibeamPing(	T_XTFRECORDXML *strXTFRecordXML,
								XTFBATHHEADER *BathyHeader)
{
	unsigned char *Ptr = (unsigned char *)BathyHeader;
	unsigned char *BathyPacket;
	static DWORD LastTimeTag = 0L, BathyTimeTag;
	static DWORD AvgN=0, AvgD=0;

	static int iNbHeader = 0;

	// Ecriture des paquets de Bathy Header
	iWriteBinBathyHeader(iNbHeader, strXTFRecordXML, BathyHeader);

//	// Allocation de la taille m�moire selon le nombre de channels d'un ping
//	if (iNbHeader == 0)
//	{
//		// Initialisation des valeurs des donn�es d'imagerie
//		iInitXMLBin7125Bathymetry(	"Bathymetry",
//									"Bathymetry",
//									"TODO",
//									strXTFRecordXML);
//	}

	// skip past the ping header
	Ptr += sizeof(XTFBATHHEADER);

	BathyPacket = Ptr;
	// Ecriture de la bathymetrie directement sous le Bathy Header
	// Attention : GLUGLUGLU : Confirmer la taille d'un �chantillon.
//	iWriteBin7125BathyHeader(	iNbHeader,
//								strXTFRecordXML,
//								BathyHeader);

	iNbHeader++;


	// BathyPacket now points to the raw data sent up by the
	// SEABAT (or Odom ECHOSCAN).  To determine what kind of
	// raw data, we must look at the data directly.
	//
	// See the Reson or Odom documentation for details on their
	// binary data format.

	// BathyPacket points to single binary data packet.  The length
	// of that packet can be determined by the Seabat or Echoscan.
	// Currently, there are only three packets recognized:
	// 1. Seabat R-Theta (140 bytes)
	// 2. Seabat RI-Theta (210 bytes)
	// 3. Echoscan R-Theta (80 bytes)

	// Do whatever Bathymetry processing here.  BathyPacket
	// points to the binary multibeam data.  The relative time
	// for this ping (in milliseconds) is given by
	//
	BathyTimeTag = BathyHeader->AttitudeTimeTag;

	if (LastTimeTag) {
	  if (BathyTimeTag < LastTimeTag) {
		 // printf("\n*****BATHY TIME WENT BACKWARDS by %ld milliseconds!\n",LastTimeTag-BathyTimeTag);
	  }
	  else {
		 AvgN += (BathyTimeTag - LastTimeTag);
		 AvgD ++;

		 // printf("Bathy time diff: %ld (avg=%ld)\n", BathyTimeTag - LastTimeTag, AvgN/ AvgD);
	  }
	}
	LastTimeTag = BathyTimeTag;
	//
	// Use this time tag to correlate the Attitude (TSS or MRU) data
	// with this multibeam data.

	} //vProcessMultibeamPing


#pragma pack() // peut etre remplace par l'option de compil -fpack-struct
