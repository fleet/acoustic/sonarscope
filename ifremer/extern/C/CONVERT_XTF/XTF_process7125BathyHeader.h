/*
 * XTF_process7125BathyHeader.h
 *
 *  Created on: 20/10/2011
 *      Author: rgallou
 */

#ifndef XTF_PROCESS7125BATHYHEADER_H_
#define XTF_PROCESS7125BATHYHEADER_H_
#pragma pack(1) // peut etre remplace par l'option de compil -fpack-struct pour gcc

#include "XTF_processDatagrams.h"


void 	vProcess7125MultibeamPing(	T_XTFRECORDXML *strXTFRecordXML,
									XTFBATHHEADER *BathyHeader);

#pragma pack() // peut etre remplace par l'option de compil -fpack-struct
#endif // XTF_PROCESS7125BATHYHEADER_H_
