/*
 * XTF_process7125DRFBathyRecord.c
 *
 *  Created on: 03 Nov. 2011
 *      Author: rgallou
 */
#pragma pack(1) // peut etre remplace par l'option de compil -fpack-struct pour gcc

#include <sys/types.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "XTF_Datagrams.h"
#include "XTF_Utilities.h"
#include "XTF_process7125DRFBathyHeader.h"

#define NB_SIGNAL 		22
#define NB_IMAGE 		0
#define NB_IMAGE2 		0
#define NUM_INDEX_TABLE	-1


//----------------------------------------------------------------
// Fonction :	iProcess7125DRFBathyHeader
// Objet 	:	Traitements des paquets de type 7125DRFBathy si on rencontre une bathy 7125.
// Modification : 03/11/2011
// Auteur 	: GLU
//----------------------------------------------------------------
int iProcess7125DRFBathyHeader(	int iNbHeader,
							T_XTFRECORDXML 				*strXTFRecordXML,
							T_XTF_7125DRFBATHY_RECORD 	*XTF7125DRFBathyHeader)
{
	char		*ptr_cRepertoire,
				*ptr_cNomHeader,
				*ptr_cNomFicPathBin;

	int 		iLenVarName,
				iSizeRep,
				iErr,
				iLoop;

	// Pointeur pour avancer sur les variables automatiquement.
	unsigned char			*ptr7125DRFBathyHeader;
	static unsigned char	*ptrPrev7125DRFBathyHeader;
	unsigned short 			usTailleVar,
							usNbElem;

	BOOL					bFlagConstant,
							bFlagCompare;

	static void 			*ptr_vValVariable,
							*ptr_vPrevVal;

	static DWORD 			iPrevPingNumber = 0;
	DWORD		 			iPingNumber;

	extern BOOL 	G_bFlagSortIncPing;

	// Ecriture syst�matique dans le fichier binaire.
	ptr7125DRFBathyHeader 				= (unsigned char *)XTF7125DRFBathyHeader;
	ptr_vValVariable 				= (void *)ptr7125DRFBathyHeader;
	ptr_vPrevVal 					= (void *)ptrPrev7125DRFBathyHeader;
	strXTFRecordXML->iNbDatagram 	= iNbHeader + 1;
	// Affectation des variables.
	for (iLoop=0; iLoop<(int)strXTFRecordXML->sNbVarSignal; iLoop++) {
	   if (iLoop > 0)
	   {
		   usTailleVar = strXTFRecordXML->strTabSignalXML[iLoop-1].usSize;
		   usNbElem = strXTFRecordXML->strTabSignalXML[iLoop-1].usNbElem;
	   }
	   else
	   {
		   usTailleVar = 0; // Pour le premier paquet
		   usNbElem = 0;
	   }
	   bFlagConstant = strXTFRecordXML->strTabSignalXML[iLoop].bFlagConstant;
	   // Pointage de la variable (par sa taille et son occurrence)
	   ptr_vValVariable = ptr_vValVariable + usTailleVar*usNbElem;
	   if (iNbHeader > 0)
	   {
		   ptr_vPrevVal = ptr_vPrevVal + usTailleVar*usNbElem;
		   bFlagCompare = util_bComparePrevAndCurrentVal(strXTFRecordXML->strTabSignalXML[iLoop].cType,
											   ptr_vPrevVal,
											   ptr_vValVariable,
											   strXTFRecordXML->strTabSignalXML[iLoop].usNbElem);
		   bFlagConstant = bFlagConstant && bFlagCompare;
		   strXTFRecordXML->strTabSignalXML[iLoop].ptr_vPrevVal = ptr_vPrevVal;
		   // Observation de l'ordre des Ping pour l'attribut Direction.
		   if (!strcmp(strXTFRecordXML->strTabSignalXML[iLoop].cNom, "PingNumber"))
		   {
			   iPrevPingNumber = *((DWORD *)ptr_vPrevVal);
			   if (iPrevPingNumber != 0)
			   {
				   iPingNumber = *((DWORD *)ptr_vValVariable);
				   if (iPrevPingNumber > iPingNumber)
				   {
					   G_bFlagSortIncPing = FALSE;
				   }
				   else
				   {
					   G_bFlagSortIncPing = TRUE;
				   }
			   }
		   }
	   }
	   strXTFRecordXML->strTabSignalXML[iLoop].bFlagConstant = bFlagConstant;
	   // Pr�paration de l'�criture de la variable.
	   usTailleVar = strXTFRecordXML->strTabSignalXML[iLoop].usSize;
	   usNbElem =  strXTFRecordXML->strTabSignalXML[iLoop].usNbElem;
	   fwrite(ptr_vValVariable, usTailleVar, usNbElem, strXTFRecordXML->strTabSignalXML[iLoop].ptr_fpBin);

	}
	if (iNbHeader == 0)
	{
		ptrPrev7125DRFBathyHeader = malloc(sizeof(T_XTF_7125DRFBATHY_RECORD));
	}
	// Recopie en zone tampon des valeurs courantes.
	memcpy(ptrPrev7125DRFBathyHeader, ptr7125DRFBathyHeader, sizeof(T_XTF_7125DRFBATHY_RECORD));

	return EXIT_SUCCESS;

} // iProcess7125DRFBathyHeader

//----------------------------------------------------------------
// Fonction :	iInitXMLBin7125DRFBathyHeader
// Objet 	:	Initialisation de la structure �quivalent aux donn�es
//				7125DRFBathy du paquet 7125 Bathy pour la manipulation des fichiers
//				bin et XML.
// Modification : 03 Nov. 2011
// Auteur 	: GLU
//----------------------------------------------------------------
int iInitXMLBin7125DRFBathyHeader(	char *cTitleDatagram,
								char *cNomDatagram,
								char *cComments,
								T_XTFRECORDXML *strXTFRecordXML)
{
	int iRet;


	T_DESC_SIGNAL_OR_IMAGE strDescriptionSignal[NB_SIGNAL] = {
			{"ProtocolVersion",			"u16",		1.0,	0.0	},
			{"Offset",					"u16",		1.0,	0.0	},
			{"SyncPattern",				"u32",		1.0,	0.0	},
			{"Size",					"u32",		1.0,	0.0	},
			{"OptionalDataOffset",		"u32",		1.0,	0.0	},
			{"OptionalDataIdentifier",	"u32",		1.0,	0.0	},
			{"Year",					"u16",		1.0,	0.0	},
			{"Day",						"u16",		1.0,	0.0	},
			{"Seconds",					"f32",		1.0,	0.0	},
			{"Hours",					"u8",		1.0,	0.0	},
			{"Minutes",					"u8",		1.0,	0.0	},
			{"Reserved1",				"u16",		1.0,	0.0	},
			{"RecordTypeIdentifier",	"u32",		1.0,	0.0	},
			{"DeviceIdentifier",		"u32",		1.0,	0.0	},
			{"Reserved2",				"u16",		1.0,	0.0	},
			{"SystemEnumerator",		"u16",		1.0,	0.0	},
			{"Reserved3",				"u32",		1.0,	0.0	},
			{"Flags",					"u16",		1.0,	0.0	},
			{"Reserved4",				"u16",		1.0,	0.0	},
			{"Reserved5",				"u32",		1.0,	0.0	},
			{"TotalRecordsInFragmentedDataRecordSet",	"u32",	1.0,	0.0	},
			{"FragmentNumber",			"u32",		1.0,	0.0	},};


	T_DESC_SIGNAL_OR_IMAGE strDescriptionImage[NB_IMAGE] = {};
	T_DESC_SIGNAL_OR_IMAGE strDescriptionImage2[NB_IMAGE2] = {};

	T_DESC_TAB_SIGANDIMG strDescriptionTab = {NB_SIGNAL,
												NB_IMAGE,
												NUM_INDEX_TABLE,
												strDescriptionSignal,
												strDescriptionImage};


	// Lancement de l'initialisation des structures de signaux : ouvertures des
	// fichiers, copie des structures, ...
	iRet = utilXTF_initXMLRecord(	strDescriptionTab,
									cTitleDatagram,
									cNomDatagram,
									cComments,
									strXTFRecordXML);

	return iRet;
} // iInitXMLBin7125DRFBathyHeader


#pragma pack() // peut etre remplace par l'option de compil -fpack-struct
