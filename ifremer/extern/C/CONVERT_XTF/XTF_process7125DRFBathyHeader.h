/*
 * XTF_process7125DRFBathyHeader.h
 *
 *  Created on: 03 Nov. 2011
 *      Author: rgallou
 */

#ifndef XTF_PROCESS7125DRFBATHYHEADER_H_
#define XTF_PROCESS7125DRFBATHYHEADER_H_
#pragma pack(1) // peut etre remplace par l'option de compil -fpack-struct pour gcc

#include "XTF_processDatagrams.h"
#include "XTF_Datagrams.h"

// Description des formats de paquets d'un fichier XTF.
// ----------------------------------------------------
// Paquet DRF (partiel, sans la DATA Section ni checksum).
typedef struct {
	u16	ProtocolVersion;
	u16	Offset;
	u32	SyncPattern;
	u32	Size;
	u32	OptionalDataOffset;
	u32	OptionalDataIdentifier;
	u16	Year;
	u16	Day;
	f32	Seconds;
	u8	Hours;
	u8	Minutes;
	u16	Reserved1;
	u32	RecordTypeIdentifier;
	u32	DeviceIdentifier;
	u16	Reserved2;
	u16	SystemEnumerator;
	u32	Reserved3;
	u16	Flags;
	u16	Reserved4;
	u32	Reserved5;
	u32	TotalRecordsInFragmentedDataRecordSet;
	u32	FragmentNumber;
} T_XTF_7125DRFBATHY_RECORD;


int iProcess7125DRFBathyHeader(	int iNbHeader,
							T_XTFRECORDXML 				*strXTFRecordXML,
							T_XTF_7125DRFBATHY_RECORD 	*XTF7125DRFBathyHeader);

int iInitXMLBin7125DRFBathyHeader(	char *cTitleDatagram,
								char *cNomDatagram,
								char *cComments,
								T_XTFRECORDXML *strXTFRecordXML);

#pragma pack() // peut etre remplace par l'option de compil -fpack-struct
#endif // XTF_PROCESS7125DRFBATHYHEADER_H_
