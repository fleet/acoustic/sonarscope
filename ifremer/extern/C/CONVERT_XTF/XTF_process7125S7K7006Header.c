/*
* XTF_processDRFRecord.c
*
*  Created on: 03 Nov. 2011
*      Author: rgallou
*/
#pragma pack(1) // peut etre remplace par l'option de compil -fpack-struct pour gcc

#include <sys/types.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "XTF_Datagrams.h"
#include "XTF_Utilities.h"
#include "XTF_process7125S7K7006Header.h"

#define NB_SIGNAL 		7
#define NB_IMAGE 		5
#define NB_IMAGE2 		0
#define NUM_INDEX_TABLE	-1


//----------------------------------------------------------------
// Fonction :	iProcess7125S7K7006Header
// Objet 	:	Traitements des paquets de type DRF si on rencontre une bathy 7125.
// Modification : 03/11/2011
// Auteur 	: GLU
//----------------------------------------------------------------
int iProcess7125S7K7006Header(	int iNbHeader,
								T_XTFRECORDXML 	*strXTFRecordXML,
								unsigned char 	*ptr7125S7K7006)
{
	char	*ptr_cRepertoire,
			*ptr_cNomHeader,
			*ptr_cNomFicPathBin;

	int 	iRet,
			iLoop,
			iFlag,
			iSizeRD;

	static unsigned char	*ptrPrev7125S7K7006;

	char					*cValeur=NULL;

	unsigned short 			usTailleVar,
							usNbElem,
							usNbSamples;

	BOOL					bFlagConstant,
							bFlagCompare;

	static void 			*ptr_vValVariable,
							*ptr_vValImage,
							*ptr_vPrevVal;

	static DWORD 			iPrevPingNumber = 0;
	DWORD		 			iPingNumber;

	extern BOOL 			G_bFlagSortIncPing;

	// Ecriture syst�matique dans le fichier binaire.
	ptr_vValVariable 	= (void *)ptr7125S7K7006;
	ptr_vPrevVal 		= (void *)ptrPrev7125S7K7006;
	strXTFRecordXML->iNbDatagram = iNbHeader+1;
	// Affectation des variables.
	for (iLoop=0; iLoop<(int)strXTFRecordXML->sNbVarSignal; iLoop++)
	{
		if (iLoop > 0)
		{
		   usTailleVar = strXTFRecordXML->strTabSignalXML[iLoop-1].usSize;
		   usNbElem = strXTFRecordXML->strTabSignalXML[iLoop-1].usNbElem;
		}
		else
		{
		   usTailleVar = 0; // Pour le premier paquet
		   usNbElem = 0;
		}
		bFlagConstant = strXTFRecordXML->strTabSignalXML[iLoop].bFlagConstant;
		// Pointage de la variable (par sa taille et son occurrence)
		ptr_vValVariable = ptr_vValVariable + usTailleVar*usNbElem;
		if (iNbHeader > 0)
		{
		   ptr_vPrevVal = ptr_vPrevVal + usTailleVar*usNbElem;
		   bFlagCompare = util_bComparePrevAndCurrentVal(strXTFRecordXML->strTabSignalXML[iLoop].cType,
											   ptr_vPrevVal,
											   ptr_vValVariable,
											   strXTFRecordXML->strTabSignalXML[iLoop].usNbElem);
		   bFlagConstant = bFlagConstant && bFlagCompare;
		   strXTFRecordXML->strTabSignalXML[iLoop].ptr_vPrevVal = ptr_vPrevVal;
		   // Observation de l'ordre des Ping pour l'attribut Direction.
		   if (!strcmp(strXTFRecordXML->strTabSignalXML[iLoop].cNom, "PingNumber"))
		   {
			   iPrevPingNumber = *((DWORD *)ptr_vPrevVal);
			   if (iPrevPingNumber != 0)
			   {
				   iPingNumber = *((DWORD *)ptr_vValVariable);
				   if (iPrevPingNumber > iPingNumber)
				   {
					   G_bFlagSortIncPing = FALSE;
				   }
				   else
				   {
					   G_bFlagSortIncPing = TRUE;
				   }
			   }
		   }
		}
		// Conservation du Nb de Samples.
		if (iLoop == 3)
		{
			// Traitement particulier de la variable DeviceInfo
			if(cValeur==NULL)
			{
				cValeur = (char*)malloc((256)*sizeof(char));
			}
			iRet = util_cValVarConstant(strXTFRecordXML->strTabSignalXML[iLoop].cType,
										ptr_vValVariable,
										(int)usNbElem,
										cValeur);
			usNbSamples = (unsigned short)atoi(cValeur);
			free(cValeur);
		}
		strXTFRecordXML->strTabSignalXML[iLoop].bFlagConstant = bFlagConstant;
		// Pr�paration de l'�criture de la variable.
		usTailleVar = strXTFRecordXML->strTabSignalXML[iLoop].usSize;
		usNbElem =  strXTFRecordXML->strTabSignalXML[iLoop].usNbElem;
		fwrite(ptr_vValVariable, usTailleVar, usNbElem, strXTFRecordXML->strTabSignalXML[iLoop].ptr_fpBin);
	}
	if (iNbHeader == 0)
	{
		ptrPrev7125S7K7006 = malloc(sizeof(T_XTF_7006_RECORD_RTH));
	}
	// Recopie en zone tampon des valeurs courantes.
	memcpy(ptrPrev7125S7K7006, ptr7125S7K7006, sizeof(T_XTF_7006_RECORD_RTH));


	// Traitement des paquets RD
	// Avance du pointeur de donn�es sur le paquet de Variables de type Image.
	ptr_vValImage	= ptr_vValVariable + usTailleVar*usNbElem;
	ptr_vPrevVal 	= ptr_vPrevVal + usTailleVar*usNbElem;

	for (iLoop=0; iLoop<(int)strXTFRecordXML->sNbVarImage; iLoop++)
	{
		if (iLoop > 0)
		{
		    usTailleVar = strXTFRecordXML->strTabImageXML[iLoop-1].usSize;
		    usNbElem = strXTFRecordXML->strTabImageXML[iLoop-1].usNbElem*usNbSamples;
		}
		else
		{
			// Conservation de la taille du pointage du dernier signal.
		    usTailleVar = 0; // Pour le premier paquet
		    usNbElem = 0;
		}
		// Pointage de la variable (par sa taille et son occurrence)
		ptr_vValImage = ptr_vValImage + usTailleVar*usNbElem;

		// Pr�paration de l'�criture de la variable.
		usTailleVar = strXTFRecordXML->strTabImageXML[iLoop].usSize;
		usNbElem 	= strXTFRecordXML->strTabImageXML[iLoop].usNbElem*usNbSamples;
		fwrite(ptr_vValImage, usTailleVar, usNbElem, strXTFRecordXML->strTabImageXML[iLoop].ptr_fpBin);

	}


	return EXIT_SUCCESS;

	} // iProcess7125S7K7006Header

//----------------------------------------------------------------
// Fonction :	iInitXMLBin7125S7K7006Header
// Objet 	:	Initialisation de la structure �quivalent aux donn�es
//				RTH du paquet 7006 du 7125 pour la manipulation des fichiers
//				bin et XML.
// Modification : 03 Nov. 2011
// Auteur 	: GLU
//----------------------------------------------------------------
int iInitXMLBin7125S7K7006Header(	char *cTitleDatagram,
						char *cNomDatagram,
						char *cComments,
						T_XTFRECORDXML *strXTFRecordXML)
{

	int iRet,
		iLoop;



	T_DESC_SIGNAL_OR_IMAGE strDescriptionSignal[NB_SIGNAL] = {
		{"SonarId",					"u64",		1.0,	0.0	},
		{"PingNumber",				"u32",		1.0,	0.0	},
		{"MultiPingSequence",		"u16",		1.0,	0.0	},
		{"N",						"u32",		1.0,	0.0	},
		{"LayerCompensationFlag",	"u8",		1.0,	0.0	},
		{"SoundVelocityFlag",		"u8",		1.0,	0.0	},
		{"SoundVelocity",			"f32",		1.0,	0.0	}};

	T_DESC_SIGNAL_OR_IMAGE strDescriptionImage[NB_IMAGE] = {
		{"Range",					"f32",		1.0,	0.0	},
		{"Quality",					"u8",		1.0,	0.0	},
		{"IntensityOrBackscatter",	"f32",		1.0,	0.0	},
		{"MinFilterInfo",			"f32",		1.0,	0.0	},
		{"MaxFilterInfo",			"f32",		1.0,	0.0	}};


	T_DESC_SIGNAL_OR_IMAGE strDescriptionImage2[NB_IMAGE2] = {};

	T_DESC_TAB_SIGANDIMG strDescriptionTab = {NB_SIGNAL,
											NB_IMAGE,
											NUM_INDEX_TABLE,
											strDescriptionSignal,
											strDescriptionImage};

	// Lancement de l'initialisation des structures de signaux : ouvertures des
	// fichiers, copie des structures, ...
	iRet = utilXTF_initXMLRecord(	strDescriptionTab,
									cTitleDatagram,
									cNomDatagram,
									cComments,
									strXTFRecordXML);

	// For�age du statut CONSTANT � FALSE pour les images.
	for (iLoop=0; iLoop<strXTFRecordXML->sNbVarImage; iLoop++)
	{
		strXTFRecordXML->strTabImageXML[iLoop].bFlagConstant = FALSE;
	}

	return iRet;
	} // iInitXMLBin7125S7K7006Header


#pragma pack() // peut etre remplace par l'option de compil -fpack-struct
