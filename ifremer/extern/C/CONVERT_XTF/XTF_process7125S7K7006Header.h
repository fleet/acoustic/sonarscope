/*
 * XTF_process7125S7K7006Header.h
 *
 *  Created on: 03 Nov. 2011
 *      Author: rgallou
 */

#ifndef XTF_PROCESS7125S7K7006HEADER_H_
#define XTF_PROCESS7125S7K7006HEADER_H_
#pragma pack(1) // peut etre remplace par l'option de compil -fpack-struct pour gcc

#include "XTF_processDatagrams.h"
#include "XTF_Datagrams.h"

// Paquet 7006 : partie RTH

//typedef struct __attribute__ ((__packed__)) {
typedef struct {
	u64	SonarId;				// Sonar serial number
	u32	PingNumber;				// Sequential number
	u16	MultiPingSequence;		// Flag to indicate multi-ping sequence.
	u32	N;						// Number of receiver beams
	u8	LayerCompensationFlag; 	// BIT FIELD;
	u8	SoundVelocityFlag; 		// Flag indicating if sound velocity is measured or manually entered
	f32	SoundVelocity;			// Sound velocity at the sonar in meters/second
} T_XTF_7006_RECORD_RTH;


// Paquet 7006 : partie RD
typedef struct {
	f32	*Range;
	u8	*Quality;
	f32	*IntensityOrBackscatter;
	f32	*MinFilterInfo;
	f32 *MaxFilterInfo;
} T_XTF_7006_RECORD_RD;

int iProcess7125S7K7006Header(	int iNbHeader,
									T_XTFRECORDXML 			*strXTFRecordXML,
									unsigned char 	*XTF7125S7K7006RTHHeader);

int iInitXMLBin7125S7K7006Header(	char *cTitleDatagram,
										char *cNomDatagram,
										char *cComments,
										T_XTFRECORDXML *strXTFRecordXML);

#pragma pack() // peut etre remplace par l'option de compil -fpack-struct
#endif /* XTF_PROCESS7125S7K7006HEADER_H_ */
