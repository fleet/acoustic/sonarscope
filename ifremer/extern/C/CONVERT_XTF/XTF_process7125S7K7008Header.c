/*
* XTF_processDRFRecord.c
*
*  Created on: 23 Nov.. 2011
*      Author: rgallou
*/
#pragma pack(1) // peut etre remplace par l'option de compil -fpack-struct pour gcc

#include <sys/types.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "XTF_Datagrams.h"
#include "XTF_Utilities.h"
#include "XTF_process7125S7K7008Header.h"

#define NB_SIGNAL 		10
#define NB_IMAGE 		4
#define NB_IMAGE2 		0
#define NUM_INDEX_TABLE	-1


//----------------------------------------------------------------
// Fonction :	iProcess7125S7K7008Header
// Objet 	:	Traitements des paquets de type DRF si on rencontre une bathy 7125.
// Modification : 03/11/2011
// Auteur 	: GLU
//----------------------------------------------------------------
int iProcess7125S7K7008Header(	int iNbHeader,
								T_XTFRECORDXML 	*strXTFRecordXML,
								unsigned char 	*ptr7125XTF7008)
{
	char	*ptr_cRepertoire,
			*ptr_cNomHeader,
			*ptr_cNomFicPathBin;

	int 	iRet,
			iLoop,
			iFlag,
			iSizeRD,
			iTypeAmp,
			iTypePhase,
			iDummy;

	static unsigned char	*ptrPrev7125XTF7008;

	char					*cValeur=NULL;

	unsigned short 			usTailleVar,
							usNbElem,
							usNbBeams,
							usNbSamples;

	BOOL					bFlagConstant,
							bFlagCompare;

	static void 			*ptr_vValVariable,
							*ptr_vValImage,
							*ptr_vPrevVal;

	static DWORD 			iPrevPingNumber = 0;
	DWORD		 			iPingNumber;

	extern BOOL 			G_bFlagSortIncPing;

	// Ecriture syst�matique dans le fichier binaire.
	ptr_vValVariable 	= (void *)ptr7125XTF7008;
	ptr_vPrevVal 		= (void *)ptrPrev7125XTF7008;
	strXTFRecordXML->iNbDatagram = iNbHeader+1;
	// Affectation des variables.
	for (iLoop=0; iLoop<(int)strXTFRecordXML->sNbVarSignal; iLoop++)
	{
		if (iLoop > 0)
		{
		   usTailleVar = strXTFRecordXML->strTabSignalXML[iLoop-1].usSize;
		   usNbElem = strXTFRecordXML->strTabSignalXML[iLoop-1].usNbElem;
		}
		else
		{
		   usTailleVar = 0; // Pour le premier paquet
		   usNbElem = 0;
		}
		bFlagConstant = strXTFRecordXML->strTabSignalXML[iLoop].bFlagConstant;
		// Pointage de la variable (par sa taille et son occurrence)
		ptr_vValVariable = ptr_vValVariable + usTailleVar*usNbElem;
		if (iNbHeader > 0)
		{
		   ptr_vPrevVal = ptr_vPrevVal + usTailleVar*usNbElem;
		   bFlagCompare = util_bComparePrevAndCurrentVal(strXTFRecordXML->strTabSignalXML[iLoop].cType,
											   ptr_vPrevVal,
											   ptr_vValVariable,
											   strXTFRecordXML->strTabSignalXML[iLoop].usNbElem);
		   bFlagConstant = bFlagConstant && bFlagCompare;
		   strXTFRecordXML->strTabSignalXML[iLoop].ptr_vPrevVal = ptr_vPrevVal;
		   // Observation de l'ordre des Ping pour l'attribut Direction.
		   if (!strcmp(strXTFRecordXML->strTabSignalXML[iLoop].cNom, "PingNumber"))
		   {
			   iPrevPingNumber = *((DWORD *)ptr_vPrevVal);
			   if (iPrevPingNumber != 0)
			   {
				   iPingNumber = *((DWORD *)ptr_vValVariable);
				   if (iPrevPingNumber > iPingNumber)
				   {
					   G_bFlagSortIncPing = FALSE;
				   }
				   else
				   {
					   G_bFlagSortIncPing = TRUE;
				   }
			   }
		   }
		}
		// Conservation du Nb de Beams.
		if (!strcmp(strXTFRecordXML->strTabSignalXML[iLoop].cNom, "N"))
		{
			// Traitement particulier de la variable DeviceInfo
			if(cValeur==NULL)
			{
				cValeur = (char*)malloc((256)*sizeof(char));
			}
			iRet = util_cValVarConstant(strXTFRecordXML->strTabSignalXML[iLoop].cType,
										ptr_vValVariable,
										(int)usNbElem,
										cValeur);
			usNbBeams = (unsigned short)atoi(cValeur);
			free(cValeur);
		}
		// Conservation du Nb de Samples.
		if (!strcmp(strXTFRecordXML->strTabSignalXML[iLoop].cNom, "S"))
		{
			// Traitement particulier de la variable DeviceInfo
			if(cValeur==NULL)
			{
				cValeur = (char*)malloc((256)*sizeof(char));
			}
			iRet = util_cValVarConstant(strXTFRecordXML->strTabSignalXML[iLoop].cType,
										ptr_vValVariable,
										(int)usNbElem,
										cValeur);
			usNbSamples = (unsigned short)atoi(cValeur);
			free(cValeur);
		}
		strXTFRecordXML->strTabSignalXML[iLoop].bFlagConstant = bFlagConstant;
		// Pr�paration de l'�criture de la variable.
		usTailleVar = strXTFRecordXML->strTabSignalXML[iLoop].usSize;
		usNbElem 	=  strXTFRecordXML->strTabSignalXML[iLoop].usNbElem;
		fwrite(ptr_vValVariable, usTailleVar, usNbElem, strXTFRecordXML->strTabSignalXML[iLoop].ptr_fpBin);
		// R�cup�ration des valeurs de type d'Amplitude et de Phase.
		// La donn�e stock�e en fichier binaire ne distingue pas le type s�par�ment.
		if (!strcmp(strXTFRecordXML->strTabSignalXML[iLoop].cNom, "TypeAmpPhase"))
		{
			iDummy 		= (int)ptr_vValVariable;
			iTypeAmp 	= util_GetBits(iDummy, 3, 4);
			iTypePhase 	= util_GetBits(iDummy, 0, 4);
		}
	}
	if (iNbHeader == 0)
	{
		ptrPrev7125XTF7008 = malloc(sizeof(T_XTF_7008_RECORD_RTH));
	}
	// Recopie en zone tampon des valeurs courantes.
	memcpy(ptrPrev7125XTF7008, ptr7125XTF7008, sizeof(T_XTF_7008_RECORD_RTH));


	// Traitement des paquets RD
	// Avance du pointeur de donn�es sur le paquet de Variables de type Image.
	ptr_vValImage	= ptr_vValVariable + usTailleVar*usNbElem;

	// Ecriture des donn�es de la partie RD dans les binaires de stockage.
	//-------------------------------------------------------------------
	// Avance du pointeur de donn�es sur le paquet de Variables de type Image.
	ptr_vValImage	= ptr_vValVariable + usTailleVar*usNbElem;
	iLoop 			= 0;	// BeamDescriptor;
	usTailleVar 	= strXTFRecordXML->strTabImageXML[iLoop].usSize;
	usNbElem 		= strXTFRecordXML->strTabImageXML[iLoop].usNbElem*usNbBeams;
	fwrite(ptr_vValImage, usTailleVar, usNbElem, strXTFRecordXML->strTabImageXML[iLoop].ptr_fpBin);

	ptr_vValImage	= ptr_vValVariable + usTailleVar*usNbElem;
	iLoop++;		// BeginSampleDescriptor;
	usTailleVar 	= strXTFRecordXML->strTabImageXML[iLoop].usSize;
	usNbElem 		= strXTFRecordXML->strTabImageXML[iLoop].usNbElem*usNbBeams;
	fwrite(ptr_vValImage, usTailleVar, usNbElem, strXTFRecordXML->strTabImageXML[iLoop].ptr_fpBin);

	ptr_vValImage	= ptr_vValVariable + usTailleVar*usNbElem;
	iLoop++;		// EndSampleDescriptor;
	usTailleVar 	= strXTFRecordXML->strTabImageXML[iLoop].usSize;
	usNbElem 		= strXTFRecordXML->strTabImageXML[iLoop].usNbElem*usNbBeams;
	fwrite(ptr_vValImage, usTailleVar, usNbBeams, strXTFRecordXML->strTabImageXML[iLoop].ptr_fpBin);

	// Traitement des �chantillons selon les cas de figures. Autres cas � coder.
	// TODO
	ptr_vValImage	= ptr_vValVariable + usTailleVar*usNbElem;
	iLoop++;
	if (iTypeAmp == 2 && iTypePhase == 2)
	{
		iLoop++;
		usTailleVar = strXTFRecordXML->strTabImageXML[iLoop].usSize;
		usNbElem 	= strXTFRecordXML->strTabImageXML[iLoop].usNbElem*usNbBeams*usNbSamples*2;
	    fwrite(ptr_vValImage, usTailleVar, usNbElem, strXTFRecordXML->strTabImageXML[iLoop].ptr_fpBin);
	}

	return EXIT_SUCCESS;

	} // iProcess7125S7K7008Header

//----------------------------------------------------------------
// Fonction :	iInitXMLBin7125S7K7008Header
// Objet 	:	Initialisation de la structure �quivalent aux donn�es
//				RTH du paquet 7008 du 7125 pour la manipulation des fichiers
//				bin et XML.
// Modification : 23 Nov. 2011
// Auteur 	: GLU
//----------------------------------------------------------------
int iInitXMLBin7125S7K7008Header(	char *cTitleDatagram,
									char *cNomDatagram,
									char *cComments,
										T_XTFRECORDXML *strXTFRecordXML)
{

	int iRet,
		iLoop;



	T_DESC_SIGNAL_OR_IMAGE strDescriptionSignal[NB_SIGNAL] = {
		{"SonarId",					"u64",		1.0,	0.0	},
		{"PingNumber",				"u32",		1.0,	0.0	},
		{"MultiPingSequence",		"u16",		1.0,	0.0	},
		{"N",						"u32",		1.0,	0.0	},
		{"Reserved1",				"u16",		1.0,	0.0	},
		{"S",						"u32",		1.0,	0.0	},
		{"RecordSubsetFlag",		"u8",		1.0,	0.0	},
		{"RowColumnFlag",			"u8",		1.0,	0.0	},
		{"Reserved2",				"u16",		1.0,	0.0	},
		{"TypeAmpPhase",			"u8",		1.0,	0.0	}};

	T_DESC_SIGNAL_OR_IMAGE strDescriptionImage[NB_IMAGE] = {
		{"BeamDescriptor",			"u16",		1.0,	0.0	},
		{"BeginSampleDescriptor",	"u32",		1.0,	0.0	},
		{"EndSampleDescriptor",		"u32",		1.0,	0.0	},
		{"Samples",					"i16",		1.0,	0.0	}};


	T_DESC_SIGNAL_OR_IMAGE strDescriptionImage2[NB_IMAGE2] = {};

	T_DESC_TAB_SIGANDIMG strDescriptionTab = {NB_SIGNAL,
											NB_IMAGE,
											NUM_INDEX_TABLE,
											strDescriptionSignal,
											strDescriptionImage};

	// Lancement de l'initialisation des structures de signaux : ouvertures des
	// fichiers, copie des structures, ...
	iRet = utilXTF_initXMLRecord(	strDescriptionTab,
									cTitleDatagram,
									cNomDatagram,
									cComments,
									strXTFRecordXML);

	// Taille de 4 octets pour d�finir le type d'Amplitude/Phase
	strXTFRecordXML->strTabSignalXML[9].usNbElem = 4;

	// For�age du statut CONSTANT � FALSE pour les images.
	for (iLoop=0; iLoop<strXTFRecordXML->sNbVarImage; iLoop++)
	{
		strXTFRecordXML->strTabImageXML[iLoop].bFlagConstant = FALSE;
	}

	return iRet;
	} // iInitXMLBin7125S7K7008Header


#pragma pack() // peut etre remplace par l'option de compil -fpack-struct
