/*
 * XTF_process7125S7K7008Header.h
 *
 *  Created on: 03 Nov. 2011
 *      Author: rgallou
 */

#ifndef XTF_PROCESS7125S7K7008HEADER_H_
#define XTF_PROCESS7125S7K7008HEADER_H_
#pragma pack(1) // peut etre remplace par l'option de compil -fpack-struct pour gcc

#include "XTF_processDatagrams.h"
#include "XTF_Datagrams.h"

// Paquet 7008 : partie RTH

//typedef struct __attribute__ ((__packed__)) {
typedef struct {
	u64		SonarId;
	u32		PingNumber;
	u16		MultiPingSequence;
	u16		N;
	u16		Reserved;
	u32		S;
	u8		RecordSubsetFlag;
	u8		RowColumnFlag;
	u16		Reserved2;
	u8		TypeAmpPhase[4];
} T_XTF_7008_RECORD_RTH;

// Paquet 7008 : partie RD
typedef struct {
	u16		*BeamDescriptor;
	u32		*BeginSampleDescriptor;
	u32		*EndSampleDescriptor;
	i16		*Samples;
} T_XTF_7008_RECORD_RD;


int iProcess7125S7K7008Header(	int iNbHeader,
								T_XTFRECORDXML 	*strXTFRecordXML,
								unsigned char 	*XTF7125S7K7008RTHHeader);

int iInitXMLBin7125S7K7008Header(	char 	*cTitleDatagram,
									char 	*cNomDatagram,
									char 	*cComments,
									T_XTFRECORDXML *strXTFRecordXML);

#pragma pack() // peut etre remplace par l'option de compil -fpack-struct
#endif /* XTF_PROCESS7125S7K7008HEADER_H_ */
