/*
 * XTF_processAttitudeHeader.c
 *
 *  Created on: 13 oct. 2008
 *      Author: rgallou
 */
#pragma pack(1) // peut etre remplace par l'option de compil -fpack-struct pour gcc

#include <sys/types.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "XTF_processAttitudeHeader.h"
#include "XTF_Utilities.h"
#include "XTF_Datagrams.h"
#include "main_XTF.h"


T_XTFRECORDXML G_strXTFAttitudeHeaderXML;

#define NB_SIGNAL 		21
#define NB_IMAGE 		0
#define NB_IMAGE2 		0
#define NUM_INDEX_TABLE	-1


//----------------------------------------------------------------
// Fonction :	iInitXMLBinAttitudeHeader
// Objet 	:	Initialisation de la structure équivalent au paquet
//				AttitudeHeader pour la manipulaiton des fichiers bin et XML.
// Modification : 15/10/08
// Auteur 	: GLU
//----------------------------------------------------------------
int iInitXMLBinAttitudeHeader(	char *cTitleDatagram,
								char *cNomDatagram,
								char *cComments,
								T_XTFRECORDXML *strXTFRecordXML)
{
	int iRet;

	T_DESC_SIGNAL_OR_IMAGE strDescriptionSignal[NB_SIGNAL] = {
			{"MagicNumber",			"WORD",	1.0,	0.0},
			{"HeaderType",			"BYTE",	1.0,	0.0},
			{"SubChannelNumber",	"BYTE",	1.0,	0.0},
			{"NumChanToFollow",		"WORD",	1.0,	0.0},
			{"Reserved1",			"WORD",	1.0,	0.0},
			{"NumBytesThisRecord",	"DWORD",1.0,	0.0},
			{"Reserved2",			"DWORD",1.0,	0.0},
			{"Pitch",				"float",1.0,	0.0},
			{"Roll",				"float",1.0,	0.0},
			{"Heave",				"float",1.0,	0.0},
			{"Yaw",					"float",1.0,	0.0},
			{"TimeTag",				"DWORD",1.0,	0.0},
			{"Heading",				"float",1.0,	0.0},
			{"Year",				"WORD",	1.0,	0.0},
			{"Month",				"BYTE",	1.0,	0.0},
			{"Day",					"BYTE",	1.0,	0.0},
			{"Hour",				"BYTE",	1.0,	0.0},
			{"Minutes",				"BYTE",	1.0,	0.0},
			{"Seconds",				"BYTE",	1.0,	0.0},
			{"MicrosSeconds",		"WORD",	1.0,	0.0},
			{"Reserved3",			"BYTE",	1.0,	0.0}};


	T_DESC_SIGNAL_OR_IMAGE strDescriptionImage[NB_IMAGE] = {};
	T_DESC_SIGNAL_OR_IMAGE strDescriptionImage2[NB_IMAGE2] = {};

	T_DESC_TAB_SIGANDIMG strDescriptionTab = {NB_SIGNAL,
												NB_IMAGE,
												NUM_INDEX_TABLE,
												strDescriptionSignal,
												strDescriptionImage};

	// Lancement de l'initialisation des structures de signaux : ouvertures des
	// fichiers, copie des structures, ...
	iRet = utilXTF_initXMLRecord(	strDescriptionTab,
									cTitleDatagram,
									cNomDatagram,
									cComments,
									strXTFRecordXML);

	// Initialisations spécialisées des variables
	// Attention : Reserved1 est un tab de variables élémentaires.
	strXTFRecordXML->strTabSignalXML[4].usNbElem = 2;
	// Attention : Reserved2 est un tab de variables élémentaires.
	strXTFRecordXML->strTabSignalXML[6].usNbElem = 4;

	return iRet;


}	//iInitXMLBinAttitudeHeader

//----------------------------------------------------------------
// Fonction :	iWriteBinAttitudeHeader
// Objet 	:	Ecriture des signaux dans les fichiers bin.
// Modification : 15/10/08
// Auteur 	: GLU
//----------------------------------------------------------------
int iWriteBinAttitudeHeader(int iNbHeader, XTFATTITUDEDATA *AttitudeHeader)
{
	char		*ptr_cRepertoire,
				*ptr_cNomHeader,
				*ptr_cNomFicPathBin;

	int 		iLenVarName,
				iSizeRep,
				iErr,
				iLoop;

	// Pointeur pour avancer sur les variables automatiquement.
	unsigned char			*ptrAttitudeHeader;
	static unsigned char	*ptrPrevAttitudeHeader;
	unsigned short 			usTailleVar,
							usNbElem;

	BOOL		bFlagConstant,
				bFlagCompare;

	static void 	*ptr_vValVariable,
					*ptr_vPrevVal;

	static DWORD 	iPrevPingNumber = 0;
	DWORD		 	iPingNumber;

	extern BOOL 	G_bFlagSortIncPing;

   ptr_cNomHeader = G_strXTFAttitudeHeaderXML.ptr_cNomHeader;
   // Ecriture systématique dans le fichier binaire.
   ptrAttitudeHeader = (unsigned char *)AttitudeHeader;
   ptr_vValVariable = (void *)ptrAttitudeHeader;
   ptr_vPrevVal = (void *)ptrPrevAttitudeHeader;
   // Affectation des variables.
   for (iLoop=0; iLoop<(int)G_strXTFAttitudeHeaderXML.sNbVarSignal; iLoop++) {
	   if (iLoop > 0)
	   {
		   usTailleVar = G_strXTFAttitudeHeaderXML.strTabSignalXML[iLoop-1].usSize;
		   usNbElem = G_strXTFAttitudeHeaderXML.strTabSignalXML[iLoop-1].usNbElem;
	   }
	   else
	   {
		   usTailleVar = 0; // Pour le premier paquet
		   usNbElem = 0;
	   }
	   bFlagConstant = G_strXTFAttitudeHeaderXML.strTabSignalXML[iLoop].bFlagConstant;
	   // Pointage de la variable (par sa taille et son occurrence)
	   ptr_vValVariable = ptr_vValVariable + usTailleVar*usNbElem;
	   if (iNbHeader > 0)
	   {
		   ptr_vPrevVal = ptr_vPrevVal + usTailleVar*usNbElem;
		   bFlagCompare = util_bComparePrevAndCurrentVal(G_strXTFAttitudeHeaderXML.strTabSignalXML[iLoop].cType,
											   ptr_vPrevVal,
											   ptr_vValVariable,
											   G_strXTFAttitudeHeaderXML.strTabSignalXML[iLoop].usNbElem);
		   bFlagConstant = bFlagConstant && bFlagCompare;
		   G_strXTFAttitudeHeaderXML.strTabSignalXML[iLoop].ptr_vPrevVal = ptr_vPrevVal;
		   // Observation de l'ordre des Ping pour l'attribut Direction.
		   if (!strcmp(G_strXTFAttitudeHeaderXML.strTabSignalXML[iLoop].cNom, "PingNumber"))
		   {
			   iPrevPingNumber = *((DWORD *)ptr_vPrevVal);
			   if (iPrevPingNumber != 0)
			   {
				   iPingNumber = *((DWORD *)ptr_vValVariable);
				   if (iPrevPingNumber > iPingNumber)
				   {
					   G_bFlagSortIncPing = FALSE;
				   }
				   else
				   {
					   G_bFlagSortIncPing = TRUE;
				   }
			   }
		   }
	   }
	   G_strXTFAttitudeHeaderXML.strTabSignalXML[iLoop].bFlagConstant = bFlagConstant;
	   // Préparation de l'écriture de la variable.
	   usTailleVar = G_strXTFAttitudeHeaderXML.strTabSignalXML[iLoop].usSize;
	   usNbElem =  G_strXTFAttitudeHeaderXML.strTabSignalXML[iLoop].usNbElem;
	   fwrite(ptr_vValVariable, usTailleVar, usNbElem, G_strXTFAttitudeHeaderXML.strTabSignalXML[iLoop].ptr_fpBin);

   }
   ptrPrevAttitudeHeader = malloc(sizeof(XTFATTITUDEDATA));
   // Recopie en zone tampon des valeurs courantes.
   memcpy(ptrPrevAttitudeHeader, ptrAttitudeHeader, sizeof(XTFATTITUDEDATA));

   return EXIT_SUCCESS;

} // vWriteBinAttitudeHeader

void vProcessAttitudeUpdate(XTFATTITUDEDATA *AttitudeData) {
/****************************************************************************/
// AttitudeData points to a single 64-byte structure that holds a
// TSS or MRU update.

   DWORD AttitudeTimeTag = AttitudeData->TimeTag;
//   float Pitch           = AttitudeData->Pitch;
//   float Roll            = AttitudeData->Roll;
//   float Heave           = AttitudeData->Heave;
//   float Yaw             = AttitudeData->Yaw;
   static DWORD LastTimeTag;
   static DWORD AvgN=0, AvgD=0;

   static int iNbHeader = 0;

   //
   // Use AttitudeTimeTag to correlate this attitude update with
   // the multibeam data.  Consider putting the data received by
   // this function in a circular buffer.  Then, use BathyTimeTag
   // as a value to perform interpolation on the attitude data to
   // closely approximate the attitude at BathyTimeTag.
   //

   // Traitement pour écriture dans le fichier Binaire
   iWriteBinAttitudeHeader(iNbHeader, AttitudeData);

   if (LastTimeTag) {
      if (AttitudeTimeTag < LastTimeTag) {
         // printf("\n******* ATTITUDE TIME JITTERED by %ld milliseconds\n", LastTimeTag - AttitudeTimeTag);
      }
      else {
         AvgN += (AttitudeTimeTag - LastTimeTag);
         AvgD++;
         // printf("Attitude Time dif: %ld (avg=%ld)\n", AttitudeTimeTag - LastTimeTag, AvgN/AvgD);
      }

   }
   LastTimeTag = AttitudeTimeTag;

   iNbHeader++;
} //vProcessAttitudeUpdate

#pragma pack() // peut etre remplace par l'option de compil -fpack-struct
