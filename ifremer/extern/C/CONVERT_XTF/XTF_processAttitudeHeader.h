/*
 * XTF_processAttitudeHeader.h
 *
 *  Created on: 13 oct. 2008
 *      Author: rgallou
 */

#ifndef XTF_PROCESSATTITUDEHEADER_H_
#define XTF_PROCESSATTITUDEHEADER_H_
#pragma pack(1) // peut etre remplace par l'option de compil -fpack-struct pour gcc

#include "XTF_processDatagrams.h"

int iInitXMLBinAttitudeHeader(	char *cTitleDatagram,
								char *cNomDatagram,
								char *cComments,
								T_XTFRECORDXML *strXTFRecordXML);

int iWriteBinAttitudeHeader(	int iNbHeader,
								XTFATTITUDEDATA *AttitudeHeader);

void vProcessAttitudeUpdate(XTFATTITUDEDATA *AttitudeData);
#pragma pack() // peut etre remplace par l'option de compil -fpack-struct
#endif /* XTF_PROCESSAttitudeHeader_H_ */
