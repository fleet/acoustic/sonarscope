/*
 * XTF_processBathyHeader.c
 *
 *  Created on: 13 oct. 2008
 *      Author: rgallou
 */
#pragma pack(1) // peut etre remplace par l'option de compil -fpack-struct pour gcc

#include <sys/types.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "XTF_processBathyHeader.h"
#include "XTF_processBathymetry.h"
#include "XTF_processQPSMultiBeamHeader.h"
#include "XTF_Datagrams.h"
#include "XTF_Utilities.h"
#include "main_XTF.h"

T_XTFRECORDXML G_strXTFQPSMultiBeamXML[NB_MAX_CHANNELS];
T_XTFRECORDXML G_strXTFBathymetryXML[NB_MAX_CHANNELS];

//----------------------------------------------------------------
// Fonction :	iWriteBinBathyHeader
// Objet 	:	Ecriture des signaux dans les fichiers bin.
// Modification : 15/10/08
// Auteur 	: GLU
//----------------------------------------------------------------
int 	iWriteBinBathyHeader(	int iNbHeader,
								T_XTFRECORDXML *strXTFRecordXML,
								XTFBATHHEADER *BathyHeader)
{
	int		iLoop;

	// Pointeur pour avancer sur les variables automatiquement.
	unsigned char			*ptrBathyHeader;
	static unsigned char	*ptrPrevBathyHeader;
	unsigned short 			usTailleVar,
							usNbElem;

	BOOL		bFlagConstant,
				bFlagCompare;

	static void 	*ptr_vValVariable,
					*ptr_vPrevVal;

	static DWORD 	iPrevPingNumber = 0;
	DWORD		 	iPingNumber;

	// Ecriture syst�matique dans le fichier binaire.
	ptrBathyHeader = (unsigned char *)BathyHeader;
	ptr_vValVariable = (void *)ptrBathyHeader;
	ptr_vPrevVal = (void *)ptrPrevBathyHeader;
	// Affectation des variables.
	for (iLoop=0; iLoop<(int)strXTFRecordXML->sNbVarSignal; iLoop++) {
	   if (iLoop > 0)
	   {
		   usTailleVar = strXTFRecordXML->strTabSignalXML[iLoop-1].usSize;
		   usNbElem = strXTFRecordXML->strTabSignalXML[iLoop-1].usNbElem;
	   }
	   else
	   {
		   usTailleVar = 0; // Pour le premier paquet
		   usNbElem = 0;
	   }
	   bFlagConstant = strXTFRecordXML->strTabSignalXML[iLoop].bFlagConstant;
	   // Pointage de la variable (par sa taille et son occurrence)
	   ptr_vValVariable = ptr_vValVariable + usTailleVar*usNbElem;
	   if (iNbHeader > 0)
	   {
		   ptr_vPrevVal = ptr_vPrevVal + usTailleVar*usNbElem;
		   bFlagCompare = util_bComparePrevAndCurrentVal(strXTFRecordXML->strTabSignalXML[iLoop].cType,
											   ptr_vPrevVal,
											   ptr_vValVariable,
											   strXTFRecordXML->strTabSignalXML[iLoop].usNbElem);
		   bFlagConstant = bFlagConstant && bFlagCompare;
		   strXTFRecordXML->strTabSignalXML[iLoop].ptr_vPrevVal = ptr_vPrevVal;
		   // Observation de l'ordre des Ping pour l'attribut Direction.
		   if (!strcmp(strXTFRecordXML->strTabSignalXML[iLoop].cNom, "PingNumber"))
		   {
			   iPrevPingNumber = *((DWORD *)ptr_vPrevVal);
			   if (iPrevPingNumber != 0)
			   {
				   iPingNumber = *((DWORD *)ptr_vValVariable);
				   if (iPrevPingNumber > iPingNumber)
				   {
					   G_bFlagSortIncPing = FALSE;
				   }
				   else
				   {
					   G_bFlagSortIncPing = TRUE;
				   }
			   }
		   }
	   }
	   strXTFRecordXML->strTabSignalXML[iLoop].bFlagConstant = bFlagConstant;
	   // Pr�paration de l'�criture de la variable.
	   usTailleVar 		= strXTFRecordXML->strTabSignalXML[iLoop].usSize;
	   usNbElem 		= strXTFRecordXML->strTabSignalXML[iLoop].usNbElem;
	   fwrite(ptr_vValVariable, usTailleVar, usNbElem, strXTFRecordXML->strTabSignalXML[iLoop].ptr_fpBin);

	}
	ptrPrevBathyHeader = malloc(sizeof(XTFBATHHEADER));
	// Recopie en zone tampon des valeurs courantes.
	memcpy(ptrPrevBathyHeader, ptrBathyHeader, sizeof(XTFBATHHEADER));

	return EXIT_SUCCESS;

} // vWriteBinBathyHeader


////////////////////////////////////////////////////////////////////////////////
// Function : vProcessMultibeamPing
// Put multibeam processing here.
// BathyHeader points to a 256-byte bathyheader structure.  That structure
// is followed by the bathy data itself.  When Isis saves bathy data,
// only one packet is saved at a time.  If there are two SEABAT heads,
// the data from each one will be stored with in different records and
// with their own XTFBATHHEADER structures.
////////////////////////////////////////////////////////////////////////////////

void vProcessMultibeamPing(	T_XTFRECORDXML *strXTFRecordXML,
							XTFBATHHEADER *BathyHeader)
{
	unsigned char	*Ptr = (unsigned char *)BathyHeader,
					*BathyPacket;

	static DWORD	LastTimeTag = 0L,
					BathyTimeTag,
					AvgN=0,
					AvgD=0;

	static int		iNbHeader = 0;

	int 			iBeam;

	BOOL			bFlagQinsy;

	WORD 			ChannelNumber,
					BytesPerSample;



	// Identification du Channel dans le cas d'un paquet de type Bathy.
	ChannelNumber = BathyHeader->SubChannelNumber;

	bFlagQinsy = 0;
	// GLU le 21/01/2015 : Pb � g�rer les fichiers issus de QINsy (de Charline) pour lesquels la Bathy attend une seule voie
	// et les paquets r�c�lent des n� de voies diff�rents.
	if (	(ChannelNumber == 0 && !strcmpi(XTFFileHeader->RecordingProgramName, "QINsy")) ||
			(strcmpi(XTFFileHeader->RecordingProgramName, "QINsy") != 0 ))
	{
		bFlagQinsy = 1;
	}

	if (bFlagQinsy)
	{
		// Compute the number of bytes of imagery for this channel
		//
		BytesPerSample	= XTFFileHeader->ChanInfo[ChannelNumber].BytesPerSample;

		// Ecriture des paquets de Bathy Header
		iWriteBinBathyHeader(iNbHeader, strXTFRecordXML, BathyHeader);

		// Initialisation de l'�criture de la bathymetrie pour le N� de Channel concern�.
		if (G_strXTFBathymetryXML[ChannelNumber].iNbDatagram == 0)
		{
			// Initialisation des valeurs des donn�es de bathym�trie
			iInitXMLBinBathymetry("Bathymetry", "TODO", ChannelNumber, BytesPerSample);
		}

		// skip past the ping header
		Ptr += sizeof(XTFBATHHEADER);

		BathyPacket = Ptr;

		// R�cup�ration �ventuelle des donn�es faisceaux de type XTFQPSMBEENTRY
		for (iBeam = 0; iBeam < BathyHeader->NumChansToFollow; iBeam++)
		{
			if (G_strXTFQPSMultiBeamXML[ChannelNumber].iNbDatagram == 0)
			{
				// Initialisation des valeurs du paquet QPSMultiBeamHeader
				iInitXMLBinQPSMultiBeamHeader("QPSMultiBeamHeader", "QPSMultiBeamHeader", "TODO", ChannelNumber, &G_strXTFQPSMultiBeamXML[ChannelNumber]);
				// Comptage des channels d�crivant les datagrammes BATHY_Q_MULTIBEAM
				G_NumChannelQPSMB++;
			}
			G_strXTFQPSMultiBeamXML[ChannelNumber].iNbDatagram++;
			vProcessQPSMultiBeam((XTFQPSMULTIBEAMHEADER *) BathyPacket, &G_strXTFQPSMultiBeamXML[ChannelNumber]);
			// Mouvement du BathyPacket de la taille du QPS_MultieamHeader
			BathyPacket += sizeof(XTFQPSMULTIBEAMHEADER);
		}

		// Ecriture de la bathymetrie directement sous le Bathy Header
		// Attention : GLUGLUGLU : Confirmer la taille du paquet � lire
		unsigned short usDummy = BathyHeader->NumBytesThisRecord
					- BathyHeader->NumChansToFollow*sizeof(XTFQPSMULTIBEAMHEADER)
					- sizeof(XTFBATHHEADER);
		iWriteBinBathymetry(	iNbHeader,
								BathyPacket,
								BytesPerSample,
								usDummy,
								ChannelNumber);

		iNbHeader++;
	}

	// BathyPacket now points to the raw data sent up by the
	// SEABAT (or Odom ECHOSCAN).  To determine what kind of
	// raw data, we must look at the data directly.
	//
	// See the Reson or Odom documentation for details on their
	// binary data format.

	// BathyPacket points to single binary data packet.  The length
	// of that packet can be determined by the Seabat or Echoscan.
	// Currently, there are only three packets recognized:
	// 1. Seabat R-Theta (140 bytes)
	// 2. Seabat RI-Theta (210 bytes)
	// 3. Echoscan R-Theta (80 bytes)

	// Do whatever Bathymetry processing here.  BathyPacket
	// points to the binary multibeam data.  The relative time
	// for this ping (in milliseconds) is given by
	//
	BathyTimeTag = BathyHeader->AttitudeTimeTag;

	if (LastTimeTag) {
	  if (BathyTimeTag < LastTimeTag) {
		 // printf("\n*****BATHY TIME WENT BACKWARDS by %ld milliseconds!\n",LastTimeTag-BathyTimeTag);
	  }
	  else {
		 AvgN += (BathyTimeTag - LastTimeTag);
		 AvgD ++;

		 // printf("Bathy time diff: %ld (avg=%ld)\n", BathyTimeTag - LastTimeTag, AvgN/ AvgD);
	  }
	}
	LastTimeTag = BathyTimeTag;
	//
	// Use this time tag to correlate the Attitude (TSS or MRU) data
	// with this multibeam data.

	} //vProcessMultibeamPing


#pragma pack() // peut etre remplace par l'option de compil -fpack-struct
