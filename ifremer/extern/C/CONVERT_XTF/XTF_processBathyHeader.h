/*
 * XTF_processBathyHeader.h
 *
 *  Created on: 13 oct. 2008
 *      Author: rgallou
 */

#ifndef XTF_PROCESSBATHYHEADER_H_
#define XTF_PROCESSBATHYHEADER_H_
#pragma pack(1) // peut etre remplace par l'option de compil -fpack-struct pour gcc

#include "XTF_processDatagrams.h"

int 	iWriteBinBathyHeader(	int iNbHeader,
								T_XTFRECORDXML *strXTFRecordXML,
								XTFBATHHEADER *BathyHeader);

void 	vProcessMultibeamPing(	T_XTFRECORDXML *strXTFRecordXML,
								XTFBATHHEADER *BathyHeader);

#pragma pack() // peut etre remplace par l'option de compil -fpack-struct
#endif // XTF_PROCESSBATHYHEADER_H_
