/*
 * XTF_processBathymetry.c
 *
 *  Created on: 22 oct. 2008
 *      Author: rgallou
 */
#pragma pack(1) // peut etre remplace par l'option de compil -fpack-struct pour gcc

#include <sys/types.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "XTF_processBathymetry.h"
#include "XTF_Utilities.h"
#include "main_XTF.h"


T_XTFRECORDXML 			G_strXTFBathymetryXML[NB_MAX_CHANNELS];
extern T_XTFRECORDXML 	G_strXTFBathyHeaderXML;

//----------------------------------------------------------------
// Fonction :	iInitXMLBinBathymetry
// Objet 	:	Initialisation de la structure �quivalent aux donn�es
//				Bathymetry pour la manipulation des fichiers bin et XML.
//				Les donn�es d'Imagerie sont incluses dans le paquet
//				Ping Chan Info.
// Modification : 15/10/08
// Auteur 	: GLU
//----------------------------------------------------------------
int iInitXMLBinBathymetry(	char *cNomDatagram,
							char *cComments,
							WORD usNumChannel,
							DWORD BytesPerSamples)
{
	char		*ptr_cRepertoire,
				*ptr_cNomHeader,
				*ptr_cNomBathyHeader,
				*ptr_cNomFicPathBin;

	int 		iLen,
				iSizeRep,
				iLoop,
				iErr;

	unsigned short usTailleVar;

	char cTabNomSignal[1][30] = {
			"Bathymetry"};


	char cTabTypeSignal[1][20] = {
			"BYTE"};

	// ScaleFactor
	float 	fTabSFSignal[1] = {
			1.0};

	// AddOffset
	float 	fTabAOSignal[1] = {
			0.0};

	// Initialisation des structures de pilotage des fichiers
	// XML et Bin pour le paquet Bathymetry
	G_strXTFBathymetryXML[usNumChannel].sNbVarSignal = 1;
	G_strXTFBathymetryXML[usNumChannel].strTabSignalXML= (T_XTFVARXML *)malloc(G_strXTFBathymetryXML[usNumChannel].sNbVarSignal*sizeof(T_XTFVARXML));

	iLen = strlen(G_strXTFBathyHeaderXML.ptr_cNomHeader)+1;
	G_strXTFBathymetryXML[usNumChannel].ptr_cNomHeader = malloc(iLen*sizeof(char));
	sprintf(G_strXTFBathymetryXML[usNumChannel].ptr_cNomHeader, "%s_%d", cNomDatagram, usNumChannel);

	iLen = strlen(cComments)+1;
	G_strXTFBathymetryXML[usNumChannel].ptr_cCommentsHeader = malloc(iLen*sizeof(char));
	sprintf(G_strXTFBathymetryXML[usNumChannel].ptr_cCommentsHeader, "%s", cComments);

	// Affectation du nom du repertoire.
	ptr_cNomHeader 			= cNomDatagram;
	ptr_cNomBathyHeader 	= G_strXTFBathyHeaderXML.ptr_cNomHeader;
	// Taille = R�pertoire + S�p�rateur + ("BathyHeader" +  S�parateur) + "PingChanHeader" + "_" + N� Channel

	iSizeRep = ( strlen(G_cRepData) + strlen(ptr_cNomBathyHeader) + 1 + strlen( ptr_cNomHeader) + 50);
	// Ajout d'une marge au cas o� certains caract�res soient doubl�s soient doubl�s.
	ptr_cRepertoire = malloc(iSizeRep*sizeof(char));
	sprintf(ptr_cRepertoire, "%s\\XTF_%s\\XTF_%s_%d", G_cRepData, ptr_cNomBathyHeader, ptr_cNomHeader, usNumChannel);

	// Cr�ation du r�pertoire
	iErr = mkdir(ptr_cRepertoire);
	if (iErr != 0)
	{
		switch (errno)
		{
			//"File exists"
			case EEXIST:
				break;
			default:
				printf("Erreur dans la creation du repertoire %s\n", strerror(errno));
				return EXIT_FAILURE;
				break;
		}
	}


	if (DEBUG)
		printf("Init Bathymetry -- Adr : %s et ptr_cNomHeader : %s\n", G_strXTFBathymetryXML[usNumChannel].ptr_cNomHeader,  G_strXTFBathymetryXML[usNumChannel].ptr_cNomHeader);

	// Initialisation g�n�rale des tableaux de variables (signaux)
	for (iLoop=0; iLoop<G_strXTFBathymetryXML[usNumChannel].sNbVarSignal; iLoop++)
	{
		G_strXTFBathymetryXML[usNumChannel].strTabSignalXML[iLoop].bFlagConstant = TRUE;
		G_strXTFBathymetryXML[usNumChannel].strTabSignalXML[iLoop].usNbElem = 1;
		G_strXTFBathymetryXML[usNumChannel].strTabSignalXML[iLoop].fScaleFactor = fTabSFSignal[iLoop];
		G_strXTFBathymetryXML[usNumChannel].strTabSignalXML[iLoop].fAddOffset = fTabAOSignal[iLoop];
		sprintf(G_strXTFBathymetryXML[usNumChannel].strTabSignalXML[iLoop].cNom, "%s", cTabNomSignal[iLoop]);
		sprintf(G_strXTFBathymetryXML[usNumChannel].strTabSignalXML[iLoop].cType, "%s", cTabTypeSignal[iLoop]);
		sprintf(G_strXTFBathymetryXML[usNumChannel].strTabSignalXML[iLoop].cTag, "%s", "TODO");
		sprintf(G_strXTFBathymetryXML[usNumChannel].strTabSignalXML[iLoop].cUnit, "%s", "TODO");
		usTailleVar = util_usNbOctetTypeVar(cTabTypeSignal[iLoop]);
		G_strXTFBathymetryXML[usNumChannel].strTabSignalXML[iLoop].usSize = usTailleVar;

		// Ouverture des fichiers.
		iLen = strlen(G_strXTFBathymetryXML[usNumChannel].strTabSignalXML[iLoop].cNom);
		// Ajout d'une marge au cas o� certains caract�res soient doubl�s soient doubl�s.
		ptr_cNomFicPathBin = malloc( (iSizeRep + 1 + iLen + 50)*sizeof(char));
		sprintf(ptr_cNomFicPathBin, "%s\\%s%s",
									ptr_cRepertoire,
									G_strXTFBathymetryXML[usNumChannel].strTabSignalXML[iLoop].cNom, ".bin");
		G_strXTFBathymetryXML[usNumChannel].strTabSignalXML[iLoop].ptr_fpBin = fopen(ptr_cNomFicPathBin, "w+b");
		if (!G_strXTFBathymetryXML[usNumChannel].strTabSignalXML[iLoop].ptr_fpBin)
		{
			printf("%s -- Error in Signal Creation File  : %s\n", __FILE__, ptr_cNomFicPathBin);
			return EXIT_FAILURE;
		}
		free(ptr_cNomFicPathBin);
	}

	// Traitement particulier de l'imagerie stock�e en BYTE ou en WORD
	if (BytesPerSamples == 1)
	{
		sprintf(G_strXTFBathymetryXML[usNumChannel].strTabSignalXML[0].cType, "%s", "BYTE");
	}
	else
	{
		sprintf(G_strXTFBathymetryXML[usNumChannel].strTabSignalXML[0].cType, "%s", "WORD");
	}

	return EXIT_SUCCESS;

} // iInitXMLBinBathymetry


//----------------------------------------------------------------
// Fonction :	iWriteBinBathymetry
// Objet 	:	Ecriture des signaux dans les fichiers bin.
// Modification : 15/10/08
// Auteur 	: GLU
//----------------------------------------------------------------
int iWriteBinBathymetry(int iNbHeader,
						unsigned char *ptrBathymetry,
						DWORD ulBytesPerSample,
						DWORD ulNumBytes,
						WORD usNumChannel)
{
	char		*ptr_cNomBathyHeader;

	int 		iLoop;

	// Pointeur pour avancer sur les variables automatiquement.
	// Prev pour les zones de donn�es pr�c�dentes.
	DWORD 			ulTailleVar,
					ulNbElemVar;


	ptr_cNomBathyHeader = G_strXTFBathyHeaderXML.ptr_cNomHeader;

	// Ecriture syst�matique dans le fichier binaire.

	// Affectation des variables.
	for (iLoop=0; iLoop<(int)G_strXTFBathymetryXML[usNumChannel].sNbVarSignal; iLoop++)
	{
		if (iLoop > 0)
		{
			ulTailleVar = (DWORD)G_strXTFBathymetryXML[usNumChannel].strTabSignalXML[iLoop-1].usSize;
			ulNbElemVar = (DWORD)G_strXTFBathymetryXML[usNumChannel].strTabSignalXML[iLoop-1].usNbElem;
		}
		else
		{
			ulTailleVar = ulBytesPerSample; // Pour les donn�es d'imageries
			ulNbElemVar = ulNumBytes;
		}
		// On suppose les donn�es d'imageries variables d'un Ping sur l'autre.
		G_strXTFBathymetryXML[usNumChannel].strTabSignalXML[iLoop].bFlagConstant = FALSE;
		// Pr�paration de l'�criture de la variable.
		fwrite(ptrBathymetry, ulTailleVar, ulNbElemVar, G_strXTFBathymetryXML[usNumChannel].strTabSignalXML[iLoop].ptr_fpBin);

	}

	return EXIT_SUCCESS;

} // vWriteBinBathymetry

//----------------------------------------------------------------
// Fonction :	iCloseBinFileBathymetry
// Objet 	:	Fermeture (et effacement) des fichiers des signaux du paquet Bathymetry.
// Modification : 15/10/08
// Auteur 	: GLU
//----------------------------------------------------------------
int iCloseBinFileBathymetry(WORD usNumChannel)
{
	int iLoop,
		iSizeRep,
		iLenVarName,
		iSizeNameHeader;

	char *ptr_cNomFicPathBin;

	// Traitement de fin : fermeture, effacement, �criture XML pour le
	// paquet Bathymetry
	for (iLoop=0; iLoop<G_strXTFBathymetryXML[usNumChannel].sNbVarSignal; iLoop++)
	{
		// Fermeture du fichier si pointeur != NULL
		if (G_strXTFBathymetryXML[usNumChannel].strTabSignalXML[iLoop].ptr_fpBin != NULL)
		{
			fclose(G_strXTFBathymetryXML[usNumChannel].strTabSignalXML[iLoop].ptr_fpBin);
		}
		// Effacement si la valeur est constante.
		if (G_strXTFBathymetryXML[usNumChannel].strTabSignalXML[iLoop].bFlagConstant == TRUE)
		{
			// Effacement du fichier et enregistrement de la balise Constant
			// dans l'arbre XML
			iLenVarName = strlen(G_strXTFBathymetryXML[usNumChannel].strTabSignalXML[iLoop].cNom);
			iSizeRep = strlen(G_cRepData);
			iSizeNameHeader = strlen(G_strXTFBathymetryXML[usNumChannel].ptr_cNomHeader);
			// Taille = Rep + S�parateur + ("Bathymetry"+S�parateur) + Nom du Paquet + S�parateur + Nom du fichier
			ptr_cNomFicPathBin = malloc( (iSizeRep + 1 + 11 + iSizeNameHeader + 1 + iLenVarName + 50)*sizeof(char));
			sprintf(ptr_cNomFicPathBin, "%s\\XTF_%s\\XTF_%s\\%s%s", G_cRepData, G_strXTFBathyHeaderXML.ptr_cNomHeader, G_strXTFBathymetryXML[usNumChannel].ptr_cNomHeader,
									G_strXTFBathymetryXML[usNumChannel].strTabSignalXML[iLoop].cNom, ".bin");
			if (DEBUG)
			{
				printf("Effacement du fichier : %s\n", ptr_cNomFicPathBin);
			}
			remove((const char *)ptr_cNomFicPathBin);
			if (ptr_cNomFicPathBin != NULL) {
				free(ptr_cNomFicPathBin);
				ptr_cNomFicPathBin = NULL;
			}
		}
	} // Fin de la boucle sur les variables du paquet Bin
	return EXIT_SUCCESS;

} // iCloseBinFileBathymetry

#pragma pack() // peut etre remplace par l'option de compil -fpack-struct
