/*
 * XTF_processBathymetry.h
 *
 *  Created on: 13 oct. 2008
 *      Author: rgallou
 */

#ifndef XTF_PROCESSBATHYMETRY_H_
#define XTF_PROCESSBATHYMETRY_H_
#pragma pack(1) // peut etre remplace par l'option de compil -fpack-struct pour gcc
#include "XTF_processDatagrams.h"

int iInitXMLBinBathymetry(	char *cNomDatagram,
							char *cComments,
							WORD usNumChannel,
							DWORD BytesPerSamples);

int iWriteBinBathymetry(int iNbHeader,
						unsigned char *ptrBathymetry,
						DWORD ulBytesPerSample,
						DWORD ulNumBytes,
						WORD usNumChannel);

int iCloseBinFileBathymetry(WORD usNumChannel);

#pragma pack() // peut etre remplace par l'option de compil -fpack-struct
#endif /* XTF_PROCESSBATHYMETRY_H */
