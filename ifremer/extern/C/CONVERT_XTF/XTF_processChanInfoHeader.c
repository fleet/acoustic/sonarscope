/*
 * XTF_processChanInfoHeader.c
 *
 *  Created on: 13 oct. 2008
 *      Author: rgallou
 */
#pragma pack(1) // peut etre remplace par l'option de compil -fpack-struct pour gcc

#include <sys/types.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "XTF_processChanInfoHeader.h"
#include "XTF_Datagrams.h"
#include "XTF_Utilities.h"

extern T_XTFRECORDXML G_strXTFChanInfoHeaderXML[NB_MAX_CHANNELS];

//----------------------------------------------------------------
// Fonction :	iInitXMLBinChanInfoHeader
// Objet 	:	Initialisation de la structure équivalent au paquet
//				ChanInfoHeader pour la manipulation des fichiers bin et XML.
// Modification : 15/10/08
// Auteur 	: GLU
//----------------------------------------------------------------
int iInitXMLBinChanInfoHeader(	char *cNomDatagram,
								char *cComments,
								T_XTFRECORDXML *strXTFChanInfoHeaderXML)
{
	int iLen,
		iLoop;

	unsigned short usTailleVar;

	char cTabNomSignal[21][30] = {
			   "TypeOfChannel",
			   "SubChannelNumber",
			   "CorrectionFlags",
			   "UniPolar",
			   "BytesPerSample",
			   "Reserved",
			   "ChannelName",
			   "VoltScale",
			   "Frequency",
			   "HorizBeamAngle",
			   "TiltAngle",
			   "BeamWidth",
			   "OffsetX",
			   "OffsetY",
			   "OffsetZ",
			   "OffsetYaw",
			   "OffsetPitch",
			   "OffsetRoll",
			   "BeamsPerArray",
			   "SamplePerFormat",
			   "ReservedArea2"};

	char cTabTypeSignal[21][20] = {
			   "BYTE",
			   "BYTE",
			   "WORD",
			   "WORD",
			   "WORD",
			   "DWORD",
			   "char",
			   "float",
			   "float",
			   "float",
			   "float",
			   "float",
			   "float",
			   "float",
			   "float",
			   "float",
			   "float",
			   "float",
			   "WORD",
			   "BYTE",
			   "char"};

	// ScaleFactor
	float 	fTabSFSignal[21] = {
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0};

	// AddOffset
	float 	fTabAOSignal[21] = {
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0};

	char cTabTagVariable[21][20]; 		//TODO
	char cTabUnitVariable[21][20]; 		//TODO

	// Initialisation des structures de pilotage des fichiers
	// XML et Bin pour le Ping Header
	// On ne stocke pas ReservedAera2
	strXTFChanInfoHeaderXML->sNbVarSignal = 20;
	//strXTFChanInfoHeaderXML->sNbVarSignal = 21;
	strXTFChanInfoHeaderXML->sNbVarImage = 0;
	strXTFChanInfoHeaderXML->strTabSignalXML= (T_XTFVARXML *)malloc(strXTFChanInfoHeaderXML->sNbVarSignal*sizeof(T_XTFVARXML));
	strXTFChanInfoHeaderXML->strTabImageXML= (T_XTFVARXML *)malloc(strXTFChanInfoHeaderXML->sNbVarImage*sizeof(T_XTFVARXML));

	iLen = strlen(cNomDatagram)+1;
	strXTFChanInfoHeaderXML->ptr_cNomHeader = malloc(iLen*sizeof(char));
	sprintf(strXTFChanInfoHeaderXML->ptr_cNomHeader, "%s", cNomDatagram);

	iLen = strlen(cNomDatagram)+1;
	strXTFChanInfoHeaderXML->ptr_cLabelHeader = malloc(iLen*sizeof(char));
	sprintf(strXTFChanInfoHeaderXML->ptr_cLabelHeader, "%s", cNomDatagram);

	iLen = strlen(cComments)+1;
	strXTFChanInfoHeaderXML->ptr_cCommentsHeader = malloc(iLen*sizeof(char));
	sprintf(strXTFChanInfoHeaderXML->ptr_cCommentsHeader, "%s", cComments);


	// Initialisation générale des variables (signaux)
	for (iLoop=0; iLoop<strXTFChanInfoHeaderXML->sNbVarSignal; iLoop++) {
	   strXTFChanInfoHeaderXML->strTabSignalXML[iLoop].ptr_fpBin = NULL;
	   strXTFChanInfoHeaderXML->strTabSignalXML[iLoop].bFlagConstant = TRUE;
	   strXTFChanInfoHeaderXML->strTabSignalXML[iLoop].usNbElem = 1;
	   strXTFChanInfoHeaderXML->strTabSignalXML[iLoop].fScaleFactor = fTabSFSignal[iLoop];
	   strXTFChanInfoHeaderXML->strTabSignalXML[iLoop].fAddOffset = fTabAOSignal[iLoop];
	   sprintf(strXTFChanInfoHeaderXML->strTabSignalXML[iLoop].cNom, "%s", cTabNomSignal[iLoop]);
	   sprintf(strXTFChanInfoHeaderXML->strTabSignalXML[iLoop].cType, "%s", cTabTypeSignal[iLoop]);
	   sprintf(strXTFChanInfoHeaderXML->strTabSignalXML[iLoop].cTag, "%s", "TODO");
	   sprintf(strXTFChanInfoHeaderXML->strTabSignalXML[iLoop].cUnit, "%s", "TODO");
	   usTailleVar = util_usNbOctetTypeVar(cTabTypeSignal[iLoop]);
	   strXTFChanInfoHeaderXML->strTabSignalXML[iLoop].usSize = usTailleVar;
	}
	// Initialisations spécialisées des variables
	// Attention : ChannelName est un tab de variables élémentaires.
	strXTFChanInfoHeaderXML->strTabSignalXML[6].usNbElem = 16;
	// Attention : ReservedArea est un tab de variables élémentaires.
	//strXTFChanInfoHeaderXML->strTabSignalXML[20].usNbElem = 53;

	return EXIT_SUCCESS;

} // iInitXMLBinChanInfoHeader


//----------------------------------------------------------------
// Fonction :	iProcessValChanInfo
// Objet 	:	Ecriture des signaux dans les fichiers bin.
// Modification : 15/10/08
// Auteur 	: GLU
//----------------------------------------------------------------
int iProcessValChanInfo(int iNumChan, CHANINFO *ChanInfoHeader)
{
	int 		iLoop;

	// Pointeur pour avancer sur les variables automatiquement.
	unsigned char			*ptrChanInfoHeader;
	unsigned short 			usTailleVar,
							usNbElem;

	static void 	*ptr_vValVariable;

   // Ecriture systématique dans le fichier binaire.
   ptrChanInfoHeader = (unsigned char *)ChanInfoHeader;
   ptr_vValVariable = (void *)ptrChanInfoHeader;
   // Affectation des variables.
   for (iLoop=0; iLoop<(int)G_strXTFChanInfoHeaderXML[iNumChan].sNbVarSignal; iLoop++) {
	   G_strXTFChanInfoHeaderXML[iNumChan].strTabSignalXML[iLoop].ptr_fpBin = NULL;
	   if (iLoop > 0)
	   {
		   usTailleVar = G_strXTFChanInfoHeaderXML[iNumChan].strTabSignalXML[iLoop-1].usSize;
		   usNbElem = G_strXTFChanInfoHeaderXML[iNumChan].strTabSignalXML[iLoop-1].usNbElem;
	   }
	   else
	   {
		   usTailleVar = 0; // Pour le premier paquet
		   usNbElem = 0;
	   }
	   // Pointage de la variable (par sa taille et son occurrence)
	   ptr_vValVariable = ptr_vValVariable + usTailleVar*usNbElem;
	   G_strXTFChanInfoHeaderXML[iNumChan].strTabSignalXML[iLoop].ptr_vPrevVal = (void *)ptr_vValVariable;
   }
   return EXIT_SUCCESS;

} // iProcessValChanInfoHeader

#pragma pack() // peut etre remplace par l'option de compil -fpack-struct
