/*
 * XTF_processChanInfo.h
 *
 *  Created on: 13 oct. 2008
 *      Author: rgallou
 */

#ifndef XTF_PROCESSCHANINFO_H_
#define XTF_PROCESSCHANINFO_H_
#pragma pack(1) // peut etre remplace par l'option de compil -fpack-struct pour gcc

#include "XTF_processDatagrams.h"

int iInitXMLBinChanInfoHeader(char *cNomDatagram, char *cComments,
						T_XTFRECORDXML *strXTFFileHeaderXML);

int iProcessValChanInfo(int iNumChan, CHANINFO *ptr_ChanInfo);
#pragma pack() // peut etre remplace par l'option de compil -fpack-struct
#endif /* XTF_PROCESSCHANINFO_H_ */
