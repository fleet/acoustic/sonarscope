	/*
	Lecture et transformation des fichiers XTF dans le nouveau format
	SYNTAX :
	convert_EdgeTech_XTF

	PARAMETERS NAMES / PROPERTY VALUES :
	ADU
	PROPERTY NAMES ONLY :

	OUTPUT PARAMETERS :
	 flag : OK ou KO

	EXAMPLES :
	nomFic = F:\SonarScopeData\Sonar\Level1\0034_20070406_081642_raw.all
	convert_EdgeTech_XTF <nomfid> <nomDatagramme>

	COMPILATION :
	gcc -o convert_EdgeTech_XTF convert_EdgeTech_XTF.c

	SEE ALSO   : Nv format Sounder Authors
	AUTHORS    : GLU
	VERSION    : $Id: testOptimSSC.m,v 1.2 2008/09/30 18:00:00 augustin Exp $
	*/
	#pragma pack(1) // peut etre remplace par l'option de compil -fpack-struct pour gcc

	#include <sys/types.h>
	#include <errno.h>
	#include <fcntl.h>
	#include <string.h>
	#include <conio.h>
	#include <stdlib.h>

	#include "XTF_Datagrams.h"
	#include "XTF_processDatagrams.h"
	#include "XTF_processPingHeader.h"
	#include "XTF_processSideScanPing.h"
	#include "XTF_processPingChanHeader.h"
	#include "XTF_processImagery.h"
	#include "XTF_processFileHeader.h"
	#include "XTF_processBathyHeader.h"
	#include "XTF_processSNP0Header.h"
	#include "XTF_processSNP1Header.h"
	#include "XTF_processFragSamples.h"
	#include "XTF_processAttitudeHeader.h"
	#include "XTF_processNotesHeader.h"
	#include "XTF_processQPSMultiBeamHeader.h"
	#include "XTF_processRawSerialHeader.h"
	#include "XTF_processFileIndex.h"
	#include "XTF_process7125DRFBathyHeader.h"
	#include "XTF_process7125DRFSnippetHeader.h"
	#include "XTF_process7125S7K7006Header.h"
	#include "XTF_process7125S7K7008Header.h"
	#include "main_XTF.h"
	#include "XTF_Utilities.h"

	T_XTFRECORDXML G_strXTFSideScanPingXML;
	T_XTFRECORDXML G_strXTFFileHeaderXML;
	T_XTFRECORDXML G_strXTFNotesHeaderXML;
	T_XTFRECORDXML G_strXTFRawSerialHeaderXML;
	T_XTFRECORDXML G_strXTFAttitudeHeaderXML;
	T_XTFRECORDXML G_strXTFPingHeaderSnippetXML;	// Pr�c�de notamment les paquets SNP0
	T_XTFRECORDXML G_strXTFSNP0XML;
	T_XTFRECORDXML G_strXTFFileIndexHeaderXML;
	T_XTFRECORDXML G_strXTF7125BathyHeaderXML;
	T_XTFRECORDXML G_strXTF7125SnippetHeaderXML;
	T_XTFRECORDXML G_strXTFBathyHeaderXML;
	T_XTFRECORDXML G_strXTFBathymetryXML[NB_MAX_CHANNELS];
	T_XTFRECORDXML G_strXTFQPSMultiBeamXML[NB_MAX_CHANNELS];

	T_FILEINDEX_RECORD strFileIndexRecord;

	extern T_XTFRECORDXML	*G_strXTFImageryXML;
	extern T_XTFRECORDXML	*G_strXTFSNP1HeaderXML;
	extern T_XTFRECORDXML	*G_strXTFFragSamplesXML;



/////////////////////////////////////////////////////////////////////////////
// Function : iReadXTFFile
// Proc�dure principale de conversion du XTF en XML-Binaire.
// Read through the file and print out some data about it.
/////////////////////////////////////////////////////////////////////////////
int iReadXTFFile(	const char *cFileData,
					void(*progress)(int))
{

	T_XTFRECORDXML strXTF7125DRFBathyHeaderXML;
	T_XTFRECORDXML strXTF7125DRFSnippetHeaderXML;
	T_XTFRECORDXML strXTFS7K7006HeaderXML;
	T_XTFRECORDXML strXTFS7K7008HeaderXML;

	unsigned int numCompteur;

	char 	*ptr;

	WORD 	usNumChan,
			usNbChan,
			usNumBeam,
			iStartPing = 0;

	int		iPosFic,
			iLoop,
			iNbDatagramNotTranslate = 0,
			iFlagFindDatagram = FALSE,
			iTailleTampon = 0,
			iSizeRD,
			iErr;

	unsigned char 	*ptr7006Record,
					*ptr7008Record;

	char	cDummyFrom[500],
			cDummyTo[500];


	// Description des Datagrams rencontr�es non traduits.
	T_TYPEOFDATAGRAMNOTTRANSLATED tabStrDatagramNotTranslate[30];

	// Initialisation des datagrammes d�j� trouv�s.
	for (iLoop=0; iLoop<30; iLoop++)
	{
		tabStrDatagramNotTranslate[iLoop].uiType = 0;
		tabStrDatagramNotTranslate[iLoop].iNbTimes = 0;
	}

	iIndexFile=0;

	//time ( &G_rawtime );
	//G_timeinfo = localtime ( &G_rawtime );
	iIndexFile = open(cFileData, O_RDONLY | O_BINARY, 0000200);
	if (iIndexFile <= 0) {
		printf("Error: Can't open %s for reading!\n", cFileData);
		goto ERROR_OUT;
	}


	iPosFic = tell(iIndexFile);

	// Read the XTF file header
	if (bReadXTFHeader(iIndexFile, XTFFileHeader) == FALSE)
	{
		return -1;
	}

	iPosFic = tell(iIndexFile);

	// R�cup�ration des infos g�n�riques du Sonar.
	G_strXTFFileHeaderXML.iNbDatagram++;
	G_u32SonarType = XTFFileHeader->SonarType;
	sprintf(G_cSonarName, "%s", XTFFileHeader->SonarName);


	// Traitement du paquet d'ent�te du fichier
	vProcessXTFHeader(XTFFileHeader);

	NumSonar			= 0;
	NumHidden			= 0;
	NumBathy			= 0;
	NumAnnotation		= 0;
	NumAttitude			= 0;
	NumSerial			= 0;
	NumSnippet			= 0;
	NumBathy7125		= 0;
	NumSnippet7125		= 0;

	G_NumChannelQPSMB							= 0;
	G_strXTFSideScanPingXML.iNbDatagram 		= 0;
	G_strXTFNotesHeaderXML.iNbDatagram 			= 0;
	G_strXTFRawSerialHeaderXML.iNbDatagram 		= 0;
	G_strXTFAttitudeHeaderXML.iNbDatagram 		= 0;
	G_strXTFPingHeaderSnippetXML.iNbDatagram 	= 0;	// Pr�c�de notamment les paquets SNP0
	G_strXTFFileIndexHeaderXML.iNbDatagram 		= 0;

	strXTF7125DRFBathyHeaderXML.iNbDatagram 	= 0;
	strXTF7125DRFSnippetHeaderXML.iNbDatagram 	= 0;
	strXTFS7K7006HeaderXML.iNbDatagram 			= 0;
	strXTFS7K7008HeaderXML.iNbDatagram 			= 0;

	//
	// Read the XTF file one packet at a time
	//
	int iNbOcFic = filelength(iIndexFile);

	iPosFic = tell(iIndexFile);

	amt = iReadXTFFormatFileData(iIndexFile, buffer);

	while (amt != 0xFFFF) {

		// Affichage de la progression dans la fen�tre d'ex�cution
		iPosFic = tell(iIndexFile);
		#ifdef GUI
			if(progress)
				progress(100*(double)iPosFic/(double)iNbOcFic);
		#else
			util_vProgressBar(0, iPosFic, iNbOcFic);
		#endif

		//
		// Buffer now holds a single record which can be processed
		// here.  The return value from ReadXTFFormatFileData()
		// holds byte length of the data record (i.e., sidescan ping)
		//

		XTFPINGHEADER *PingHeader = (XTFPINGHEADER *) buffer;

		printf("Type de datagramme : %d\n", PingHeader->HeaderType);
		switch (PingHeader->HeaderType)
		{
			case XTF_HEADER_ATTITUDE :
				ptr			= "TSS   "; // TSS or MRU update
				G_strXTFAttitudeHeaderXML.iNbDatagram++;
				if (NumAttitude == 0)
				{
					// Initialisation des valeurs du paquet AttitudeHeader
					iInitXMLBinAttitudeHeader("AttitudeHeader", "AttitudeHeader", "TODO", &G_strXTFAttitudeHeaderXML);
				}
				NumAttitude++;
				vProcessAttitudeUpdate((XTFATTITUDEDATA *) PingHeader);
				numCompteur	= NumAttitude;
				break;


			case XTF_HEADER_NOTES    :
				ptr			= "NOTES "; // just some user-entered annotation
				G_strXTFNotesHeaderXML.iNbDatagram++;
				if (NumAnnotation == 0)
				{
					// Initialisation des valeurs du paquet NotesHeader
					iInitXMLBinNotesHeader("NotesHeader", "NotesHeader", "TODO", &G_strXTFNotesHeaderXML);
				}
				NumAnnotation++;
				vProcessNotes((XTFNOTESHEADER *) PingHeader);
				numCompteur = NumAnnotation;
				break;

			case XTF_HEADER_ELAC     :
				ptr			= "ELAC  "; // just some user-entered annotation
				NumBathy++;
				numCompteur = NumBathy;
				break;

			case XTF_HEADER_RAW_SERIAL :
				ptr			= "SERIAL";
				G_strXTFRawSerialHeaderXML.iNbDatagram++;
				if (NumSerial == 0)
				{
					// Initialisation des valeurs du paquet RawSerial
					iInitXMLBinRawSerialHeader("RawSerialHeader", "RawSerialHeader", "TODO", &G_strXTFRawSerialHeaderXML);

				}
				NumSerial++;
				vProcessRawSerial((XTFRAWSERIALHEADER *) PingHeader);
				numCompteur = NumSerial;
				break;

			case XTF_HEADER_BATHY_SNIPPET    :
				ptr			= "SNIPPET";
				// Bathy Snippet pour le Reson Seabat
				G_strXTFPingHeaderSnippetXML.iNbDatagram++;	// Pr�c�de notamment les paquets SNP0
				if (NumSnippet == 0)
				{
					// Initialisation des valeurs du paquet PingHeader
					iInitXMLBinPingHeader("BathySnippet", "BathySnippet", "TODO", &G_strXTFPingHeaderSnippetXML);
					// Initialisation des valeurs du paquet PingHeader
					iInitXMLBinSNP0Header("SNP0", "SNP0", "TODO", &G_strXTFSNP0XML);
				}

				NumSnippet++;
				vProcessSNP0((XTFPINGHEADER *) PingHeader);
				XTFBATHYSNP0SNIPPET *SNP0Header = (XTFBATHYSNP0SNIPPET *) buffer+sizeof(XTFPINGHEADER);
				usNumBeam = SNP0Header->BeamCnt;
				numCompteur = NumSnippet;
				break;

			case XTF_HEADER_POSITION    :
				ptr			= "POSITION";
				// GLU le 20/10/2011
				// Non trait� pour l'instant � d�faut de description du paquet POSITION.
				//XTFPOSITIONDATA *Dummy = (XTFPOSITIONDATA*)PingHeader;
				break;


			case XTF_HEADER_SONAR    :
				ptr			= "SONAR "; // sidescan ping
				G_strXTFSideScanPingXML.iNbDatagram++;
				if (NumSonar == 0)
				{
					// Initialisation des valeurs du paquet PingHeader
					iInitXMLBinPingHeader("SideScan", "SideScan", "TODO", &G_strXTFSideScanPingXML);
				}

				NumSonar++;
				vProcessSidescanPing((XTFPINGHEADER *) PingHeader);
				usNbChan 	= PingHeader->NumChansToFollow;
				numCompteur = NumSonar;
				break;

			case XTF_HEADER_HIDDEN_SONAR  :
				ptr			= "HIDDEN"; // sidescan ping
				NumHidden++;
				vProcessSidescanPing((XTFPINGHEADER *) PingHeader);
				numCompteur = NumHidden;
				break;


			// Les donn�es de Bathy sont pr�c�d�es (dans certains cas ?) des donn�es de faisceaux.
			case XTF_HEADER_QINSY_R2SONIC_BATHY:
			case XTF_HEADER_Q_MULTIBEAM : 				// Bathy issue de Quinsy

			case XTF_HEADER_BATHY   :
				ptr			= "BATHY "; // multibeam ping
				usNbChan	= PingHeader->NumChansToFollow;
				usNumChan	= PingHeader->SubChannelNumber;

				if (G_strXTFBathyHeaderXML.iNbDatagram == 0)
				{
					// Initialisation des valeurs du paquet PingHeader
					iInitXMLBinPingHeader("Bathy", "Bathy", "TODO",&G_strXTFBathyHeaderXML);
				}

				// Ecriture des variables d'entete et des donn�es de Bathy successivement.
				vProcessMultibeamPing(&G_strXTFBathyHeaderXML,(XTFBATHHEADER *) PingHeader);
				NumBathy++;
				G_strXTFBathyHeaderXML.iNbDatagram++;
				G_strXTFBathymetryXML[usNumChan].iNbDatagram++;
				numCompteur = NumBathy;
				break;


			case XTF_HEADER_7125 :
				ptr = "7125_BATHY";

				// Traitement du PingHeader de ce paquet BATHY.
				XTFBATHHEADER 	*ptrDummyBathy = (XTFBATHHEADER*)PingHeader;

				if (G_strXTF7125BathyHeaderXML.iNbDatagram == 0)
				{
					// Initialisation des valeurs du paquet PingHeader de la bathy des datagrammes 7125
					iInitXMLBinPingHeader("7125Bathy", "7125Bathy", "TODO", &G_strXTF7125BathyHeaderXML);
					// Initialisation des valeurs du paquet DRF
					iInitXMLBin7125DRFBathyHeader("7125Bathy_DRF", "7125Bathy_DRF", "TODO", &strXTF7125DRFBathyHeaderXML);
					// Initialisation des valeurs du paquet 7006
					iInitXMLBin7125S7K7006Header("7125Bathy_7006", "7125Bathy_7006", "TODO", &strXTFS7K7006HeaderXML);
				}

				G_strXTF7125BathyHeaderXML.iNbDatagram++;
				vProcess7125MultibeamPing(&G_strXTF7125BathyHeaderXML, (XTFBATHHEADER *) ptrDummyBathy);

				iTailleTampon 			= sizeof(XTFBATHHEADER);
				T_XTF_7125DRFBATHY_RECORD 	*ptr7125DRFBathyRecord 	= (T_XTF_7125DRFBATHY_RECORD *) (buffer+iTailleTampon);
			    iProcess7125DRFBathyHeader(NumBathy7125, &strXTF7125DRFBathyHeaderXML, ptr7125DRFBathyRecord);

				iTailleTampon 			= iTailleTampon + sizeof(T_XTF_7125DRFBATHY_RECORD);
				T_XTF_7006_RECORD_RTH 	*ptrDummyXTF_7006RTH 	= (T_XTF_7006_RECORD_RTH *) (buffer + iTailleTampon);
				// Calcul de la taille du buffer de lecture (17 = taille attendue des donn�es 7006 RD).
				iSizeRD = ptrDummyXTF_7006RTH->N*17 + sizeof(T_XTF_7006_RECORD_RTH);

				ptr7006Record			= malloc(sizeof(char)*iSizeRD ) ;
				memcpy(ptr7006Record, ptrDummyXTF_7006RTH, iSizeRD);
				iProcess7125S7K7006Header(NumBathy7125, &strXTFS7K7006HeaderXML, ptr7006Record);
				free(ptr7006Record);
				iTailleTampon 			= iTailleTampon + sizeof(T_XTF_7006_RECORD_RTH);
				T_XTF_7006_RECORD_RD 	*ptrDummyXTF_7006RD 	= (T_XTF_7006_RECORD_RD *) (buffer + iTailleTampon);

				iTailleTampon 			= iTailleTampon + sizeof(T_XTF_7006_RECORD_RD) * ptrDummyXTF_7006RTH->N;
				//unsigned int 			*checksum 				= (T_XTF_7006_RECORD_RD *) (buffer + iTailleTampon);

//				iTailleTampon 			= iTailleTampon + sizeof(unsigned int);
//				T_XTF_7004_RECORD_RTH 	*ptrDummyXTF_7004RTH 	= (T_XTF_7004_RECORD_RTH *) (buffer + iTailleTampon);
//
//				iTailleTampon 			= iTailleTampon + sizeof(T_XTF_7004_RECORD_RTH);
//				T_XTF_7004_RECORD_RD 	*ptrDummyXTF_7004RD 	= (T_XTF_7004_RECORD_RD *) (buffer + iTailleTampon);

				NumBathy7125++;
				numCompteur = NumBathy7125;
				break;

			case XTF_HEADER_7125_SNIPPET    :
				ptr = "7125_SNIPPET";

				// Traitement de l'imagerie Snippet
				XTFPINGHEADER 	*ptrDummySnippet = (XTFPINGHEADER*)PingHeader;
//				if (G_strXTF7125SnippetHeaderXML.iNbDatagram == 0)
//				{
//					// Initialisation des valeurs du paquet PingHeader de la bathy des datagrammes 7125
//					iInitXMLBinPingHeader("7125Snippet", "7125Snippet", "TODO", &G_strXTF7125SnippetHeaderXML);
//					// Initialisation des valeurs du paquet DRF
//					iInitXMLBin7125DRFSnippetHeader("7125Snippet_DRF", "7125B7125Snippet_DRFathy_DRF", "TODO", &strXTF7125DRFSnippetHeaderXML);
//					// Initialisation des valeurs du paquet 7008
//					iInitXMLBin7125S7K7008Header("7125Snippet_7008", "7125Snippet_7008", "TODO", &strXTFS7K7008HeaderXML);
//				}
//				G_strXTF7125SnippetHeaderXML.iNbDatagram++;
//				vProcess7125MultibeamPing(&G_strXTF7125SnippetHeaderXML, (XTFPINGHEADER *) ptrDummySnippet);
//
//				iTailleTampon 			= sizeof(XTFPINGHEADER);
//				T_XTF_7125DRFSNIPPET_RECORD 	*ptr7125DRFSnippetRecord 	= (T_XTF_7125DRFSNIPPET_RECORD *) (buffer+iTailleTampon);
//			    iProcess7125DRFSnippetHeader(NumSnippet7125, &strXTF7125DRFSnippetHeaderXML, ptr7125DRFSnippetRecord);

//				iTailleTampon 			= iTailleTampon + sizeof(T_XTF_7125DRFSNIPPET_RECORD);
//				T_XTF_7008_RECORD_RTH 	*ptrDummyXTF_7008RTH 	= (T_XTF_7008_RECORD_RTH *) (buffer + iTailleTampon);
//				// Calcul de la taille du buffer de lecture :
//				// 10 = taille attendue des donn�es 7008 RD - Part1 Cf Doc 7K Data Format Definition for Ifremer_Rev C_Draft 2.pdf).
//				// + N*S*2 octets
//				iSizeRD = ptrDummyXTF_7008RTH->N*10 + ptrDummyXTF_7008RTH->N*ptrDummyXTF_7008RTH->S*2 + sizeof(T_XTF_7008_RECORD_RTH);
//
//				ptr7008Record = malloc(sizeof(char)*iSizeRD ) ;
//				memcpy(ptr7008Record, ptrDummyXTF_7008RTH, iSizeRD);
//				iProcess7125S7K7008Header(NumSnippet7125, &strXTFS7K7008HeaderXML, ptr7008Record);
//				free(ptr7008Record);
//				iTailleTampon 			= iTailleTampon + sizeof(T_XTF_7008_RECORD_RTH);
//				T_XTF_7008_RECORD_RD 	*ptrDummyXTF_7008RD 	= (T_XTF_7008_RECORD_RD *) (buffer + iTailleTampon);
//
//				iTailleTampon 			= iTailleTampon + sizeof(T_XTF_7008_RECORD_RD) * ptrDummyXTF_7008RTH->N;
//				//unsigned int 			*checksum 				= (T_XTF_7008_RECORD_RD *) (buffer + iTailleTampon);

//				iTailleTampon 			= iTailleTampon + sizeof(unsigned int);
//				T_XTF_7004_RECORD_RTH 	*ptrDummyXTF_7004RTH 	= (T_XTF_7004_RECORD_RTH *) (buffer + iTailleTampon);
//
//				iTailleTampon 			= iTailleTampon + sizeof(T_XTF_7004_RECORD_RTH);
//				T_XTF_7004_RECORD_RD 	*ptrDummyXTF_7004RD 	= (T_XTF_7004_RECORD_RD *) (buffer + iTailleTampon);

				NumSnippet7125++;
				numCompteur = NumSnippet7125;
				break;



			default :
				// Recherche de datagrammes non d�cod�s d�j� trouv�s.
				// Recherche de datagrammes non d�cod�s d�j� trouv�s.
				for (iLoop=0; iLoop<20; iLoop++)
				{
					if (PingHeader->HeaderType == tabStrDatagramNotTranslate[iLoop].uiType)
					{
						tabStrDatagramNotTranslate[iLoop].iNbTimes++;
						iFlagFindDatagram = TRUE;
						break;
					}
				}
				if (iFlagFindDatagram == FALSE)
				{
					tabStrDatagramNotTranslate[iNbDatagramNotTranslate].uiType = PingHeader->HeaderType;
					tabStrDatagramNotTranslate[iNbDatagramNotTranslate].iNbTimes++;
					iNbDatagramNotTranslate++;
				}
				iFlagFindDatagram = FALSE;

				// ptr = "OTHER";
				// printf("%s  \r", ptr);
				break;
		}

		if(DEBUG)
		{
			fprintf(stdout, "%s %02u-%02u/%02u/%02u %02u:%02u:%02u Y=%.6lf X=%.6lf pitch=%.1f roll=%.1f\r",
					ptr,
					numCompteur,
					PingHeader->Month,
					PingHeader->Day,
					PingHeader->Year,
					PingHeader->Hour,
					PingHeader->Minute,
					PingHeader->Second,
					PingHeader->SensorYcoordinate,
					PingHeader->SensorXcoordinate,
					PingHeader->SensorPitch,
					PingHeader->SensorRoll);

		}



		// Pour la cr�ation du fichier d'index apr�s lecture du Header.
		// Le paquet FileIndex fait l'objet d'un traitement particulier n'�tant pas un datagramme � part
		// enti�re.

		int iDate  = util_iDayJma2Ifr(PingHeader->Day, PingHeader->Month, PingHeader->Year);
		int iHeure = 1000 * (PingHeader->Second + PingHeader->Minute * 60 + PingHeader->Hour * 3600);

		double dDateHeure 							= util_clTime(iDate, iHeure);
		strFileIndexRecord.Date 					= dDateHeure;
		strFileIndexRecord.DatagramPosition 		= (unsigned int)iPosFic;
		strFileIndexRecord.NbOfBytesInDatagram 		= (unsigned int)PingHeader->NumBytesThisRecord;
		strFileIndexRecord.TypeOfDatagram 			= PingHeader->HeaderType;

		iProcessFileIndexRecord(G_strXTFFileIndexHeaderXML.iNbDatagram,
				&G_strXTFFileIndexHeaderXML,
				&strFileIndexRecord);
		G_strXTFFileIndexHeaderXML.iNbDatagram++;

		if (kbhit()) if (getch() == 27)
		{
			printf("\nUser pressed ESC\n");
			break; // press ESC to quit.
		}

		// Lecture du nv paquet.
		long lDummy;
		lDummy = tell(iIndexFile);
		amt = iReadXTFFormatFileData(iIndexFile, buffer);

	} // Boucle de lecture

	// End of progress bar
	if(progress)
	{
		progress(100);
	}

	printf("Datagrams not translated : %d\n", iNbDatagramNotTranslate);
	// Recherche de datagrammes non d�cod�s d�j� trouv�s.
	for (iLoop=0; iLoop<iNbDatagramNotTranslate; iLoop++)
	{
		printf("Process datagram : Datagram %d found %d times not translated\n",	tabStrDatagramNotTranslate[iLoop].uiType, tabStrDatagramNotTranslate[iLoop].iNbTimes);
	}

	// Fermeture des fichiers XML et Binaires de le paquet d'index.
	if (G_strXTFFileIndexHeaderXML.iNbDatagram != 0)
	{
		// Paquet Ping Header -- Fermeture des fichiers Bin et Sauvegarde des fichiers XML cr��s
		utilXTF_iSaveXMLSignalFile(&G_strXTFFileIndexHeaderXML);
	}

	// Fermeture des fichiers XML et Binaires de l'ensemble des paquets.
	if (NumSonar != 0)
	{
		// Paquet Ping Header -- Fermeture des fichiers Bin et Sauvegarde des fichiers XML cr��s
		utilXTF_iSaveXMLSignalFile(&G_strXTFSideScanPingXML);
		for (usNumChan=0; usNumChan< G_NumSonarChans; usNumChan++)
		{
			// Paquet Ping Header -- Fermeture des fichiers Bin
			iCloseBinFilePingChanHeader(usNumChan);
			// Paquet Ping Header -- Fermeture des fichiers Bin
			iCloseBinFileImagery(usNumChan);
			G_strXTFImageryXML[usNumChan].iNbDatagram = G_strXTFSideScanPingXML.iNbDatagram;
			// Paquet Ping Header -- Sauvegarde des fichiers XML cr��s
			iSaveXMLFileImagery(usNumChan);
		}
		#ifndef _DEBUG
			//GLUGLUGLU free(G_strXTFPingChanHeaderXML);
			free(G_strXTFImageryXML);
		#endif
	}
	if (NumAttitude != 0)
	{
		// Paquet Attitude Header -- Fermeture des fichiers Bin
		// Paquet Attitude Header -- Sauvegarde des fichiers XML cr��s
		utilXTF_iSaveXMLSignalFile(&G_strXTFAttitudeHeaderXML);
	}

	if (NumAnnotation != 0)
	{
		// Paquet Notes Header -- Fermeture des fichiers Bin et Sauvegarde des fichiers XML cr��s
		utilXTF_iSaveXMLSignalFile(&G_strXTFNotesHeaderXML);
	}

	if (NumSerial != 0)
	{
		// Paquet Raw Serial Header --
		// Paquet Raw Serial Header -- Fermeture des fichiers Bin et Sauvegarde des fichiers XML cr��s
		utilXTF_iSaveXMLSignalFile(&G_strXTFRawSerialHeaderXML);

	}

	if (NumBathy != 0)
	{
		// Paquet Bathy Header -- Fermeture des fichiers Bin et Sauvegarde des fichiers XML cr��s
		utilXTF_iSaveXMLSignalFile(&G_strXTFBathyHeaderXML);
		for (usNumChan=0; usNumChan< G_NumBathyChans; usNumChan++)
		{

			// Paquet Bathymetry -- Fermeture des fichiers Bin et Sauvegarde des fichiers XML cr��s
			utilXTF_iSaveXMLSignalFile(&G_strXTFBathymetryXML[usNumChan]);

			// Artifice pour relocaliser les fichiers de description de la bathy � la mani�re de ce qui est fait
			// pour l'imagerie.
			sprintf(cDummyFrom, "%s\\XTF_%s%s", G_cRepData, G_strXTFBathymetryXML[usNumChan].ptr_cNomHeader, ".xml");
			sprintf(cDummyTo, "%s\\XTF_%s\\XTF_%s%s", G_cRepData, G_strXTFBathyHeaderXML.ptr_cNomHeader, G_strXTFBathymetryXML[usNumChan].ptr_cNomHeader, ".xml");
			iErr = rename(cDummyFrom, cDummyTo);

		}

		// Fermeture pour le nombre de voies concernant par les datagrammes BATHY_Q_MULTIBEAM
		for (usNumChan=0; usNumChan< G_NumChannelQPSMB; usNumChan++)
		{
			if (G_strXTFQPSMultiBeamXML[usNumChan].iNbDatagram > 0)
			{
				// Paquet QPS Multibeam Header -- Fermeture des fichiers Bin et Sauvegarde des fichiers XML cr��s
				utilXTF_iSaveXMLSignalFile(&G_strXTFQPSMultiBeamXML[usNumChan]);
				// Artifice pour relocaliser les fichiers de description de la bathy � la mani�re de ce qui est fait
				// pour l'imagerie.
				sprintf(cDummyFrom, "%s\\XTF_%s%s", G_cRepData, G_strXTFQPSMultiBeamXML[usNumChan].ptr_cNomHeader, ".xml");
				sprintf(cDummyTo, "%s\\XTF_%s\\XTF_%s%s", G_cRepData, G_strXTFBathyHeaderXML.ptr_cNomHeader, G_strXTFQPSMultiBeamXML[usNumChan].ptr_cNomHeader, ".xml");
				iErr = rename(cDummyFrom, cDummyTo);
			}
		}

	}

	if (NumBathy7125 != 0)
	{

		if (G_strXTF7125BathyHeaderXML.iNbDatagram != 0)
		{
			// Paquet Bathymetry -- Fermeture des fichiers Bin et Sauvegarde des fichiers XML cr��s
			utilXTF_iSaveXMLSignalFile(&G_strXTF7125BathyHeaderXML);

			// Paquet Bathymetry de type 7006 DRF -- Fermeture des fichiers Bin et Sauvegarde des fichiers XML cr��s
			utilXTF_iSaveXMLSignalFile(&strXTF7125DRFBathyHeaderXML);

			// Paquet Bathymetry de type 7006 RTH -- Fermeture des fichiers Bin et Sauvegarde des fichiers XML cr��s
			utilXTF_iSaveXMLSignalFile(&strXTFS7K7006HeaderXML);
		}
	}

	if (NumSnippet7125 != 0)
	{
		if (G_strXTF7125SnippetHeaderXML.iNbDatagram != 0)
		{
			// Paquet Bathymetry -- Fermeture des fichiers Bin et Sauvegarde des fichiers XML cr��s
			utilXTF_iSaveXMLSignalFile(&G_strXTF7125SnippetHeaderXML);

			// Paquet Bathymetry de type 7008 DRF -- Fermeture des fichiers Bin et Sauvegarde des fichiers XML cr��s
			utilXTF_iSaveXMLSignalFile(&strXTF7125DRFSnippetHeaderXML);

			// Paquet Bathymetry de type 7008 RTH -- Fermeture des fichiers Bin et Sauvegarde des fichiers XML cr��s
			utilXTF_iSaveXMLSignalFile(&strXTFS7K7008HeaderXML);
		}
	}
	// Fermeture des fichiers XML et Binaires de l'ensemble des paquets.
	if (NumSnippet != 0)
	{
		// Paquet Ping Header -- Fermeture des fichiers Bin et Sauvegarde des fichiers XML cr��s
		utilXTF_iSaveXMLSignalFile(&G_strXTFPingHeaderSnippetXML);
		// Paquet Snippet 0 -- Fermeture des fichiers Bin et Sauvegarde des fichiers XML cr��s
		utilXTF_iSaveXMLSignalFile(&G_strXTFSNP0XML);
		XTFBATHYSNP0SNIPPET *SNP0Header = (XTFBATHYSNP0SNIPPET *) buffer+sizeof(XTFPINGHEADER);
		for (usNumBeam=0; usNumBeam< SNP0Header->BeamCnt; usNumBeam++)
		{
			// Paquet Ping Header -- Fermeture des fichiers Bin
			iCloseBinFileSNP1Header(usNumBeam);
			// Paquet Ping Header -- Fermeture des fichiers Bin
			iCloseBinFileFragSamples(usNumBeam);
			G_strXTFSNP1HeaderXML[usNumBeam].iNbDatagram = G_strXTFSNP0XML.iNbDatagram;
			// Paquet Ping Header -- Sauvegarde des fichiers XML cr��s
			iSaveXMLFileFragSamples(usNumBeam);
		}
		#ifndef _DEBUG
			//free(G_strXTFSNP1HeaderXML);
			free(G_strXTFFragSamplesXML);
		#endif
	}

	if (amt == 0xFFFF) {
		printf("\nStopped - read -1 bytes\n");
	}
	else {
		printf("\nDon't know why loop stopped.\n");
	}

	//printf("\n\nPacket count: %u sonar, %u hidden, %u bathy, %u annotation, %u attitude, %d raw serial\n",
	//   NumSonar, NumHidden, NumBathy, NumAnnotation, NumAttitude, NumSerial);

	printf("\nDone!\n");

	return EXIT_SUCCESS;

	ERROR_OUT:
	// End of progress bar
	if(progress){
		progress(50);
		progress(100);
	}

	if (iIndexFile > 0) {
		close(iIndexFile);
		iIndexFile = 0;
	}

	return EXIT_FAILURE;

	} //iReadXTFFile


	/////////////////////////////////////////////////////////////////////////////
	// Function : iReadXTFFormatFileData
	// This function will read data from an XTF file and return buffer with
	// a ping of sonar data in it.  This function is limited to pings with a byte
	// count of less than 64K.
	// Returns the number of bytes in *buffer*.  These bytes will be a
	// Sonar data packet.  The calling program can use this data (which will
	// be side scan and/or subbottom data) for anything.
	/////////////////////////////////////////////////////////////////////////////
	unsigned int iReadXTFFormatFileData(int iIndexFile,
										unsigned char *buffer) {

	int		iLoop,
	iFlagFindDatagram = FALSE;


	XTFPINGHEADER 	*PacketHeader;

	unsigned char 	*SrcPtr = buffer;

	unsigned long 	BytesScanned=0L;

	long 			AmountRemaining,
					AmountNeeded,
					RecordLength,
					AmountRead,
					tmp,
					len;

	long 			lPosFic,
					lDummy;

	// Read in 64 bytes just to get going
	AmountRead = read(iIndexFile, SrcPtr, 64);
	if (AmountRead != 64) {
		printf("\nCan't read 64 bytes\n");
		return 0xFFFF;
	}

	AmountRemaining = AmountRead;

	while (((long)BytesScanned < AmountRead) && AmountRemaining) {

		PacketHeader = (XTFPINGHEADER *) SrcPtr;

		RecordLength = PacketHeader->NumBytesThisRecord;

		switch (PacketHeader->HeaderType) {

		case XTF_HEADER_SONAR        :
		case XTF_HEADER_BATHY        :
		case XTF_HEADER_ATTITUDE     :
		case XTF_HEADER_NOTES        :
		case XTF_HEADER_ELAC         :
		case XTF_HEADER_RAW_SERIAL   :
		case XTF_HEADER_EMBED_HEAD   :
		case XTF_HEADER_HIDDEN_SONAR :
		case XTF_HEADER_ECHOSTRENGTH :
		case XTF_HEADER_7125		  :
		case XTF_HEADER_7125_SNIPPET :

			// Make sure enough data sits in memory to have
			// a complete packet
			AmountRemaining = AmountRead - BytesScanned;
			if (AmountRemaining && AmountRemaining < RecordLength) {
				AmountNeeded	= RecordLength - AmountRemaining;
				lPosFic = tell(iIndexFile);
				len 			= read(iIndexFile, buffer+(WORD)AmountRead, (WORD)AmountNeeded);
				AmountRead  	+= len;
				if (len != AmountNeeded)
				{
					return 0;
				}
				AmountRemaining = AmountRead - BytesScanned;
			}

			SrcPtr       += RecordLength;
			BytesScanned += RecordLength;
			AmountRemaining = AmountRead - BytesScanned;

			break;


		default : // Unrecognized header type.  Skip past it.

			// printf("\nUnknown data packet: 0x%X\n", (int) (PacketHeader->HeaderType));

			AmountRemaining = AmountRead - BytesScanned;

			lPosFic = tell(iIndexFile);
			// Make sure enough data sits in memory to have a complete note record block
			if (AmountRemaining < RecordLength)
			{
				AmountNeeded    = RecordLength - AmountRemaining;
				tmp 			= AmountRead;
				lDummy 			=  read(iIndexFile, buffer+(WORD)AmountRead, (WORD)AmountNeeded);
				AmountRead  	+= lDummy;
				lPosFic 		= tell(iIndexFile);
				AmountRemaining = AmountRead - BytesScanned;

			}

			BytesScanned 	+= RecordLength;
			lPosFic 		= tell(iIndexFile);
			break;
		}
	}

	return (WORD)AmountRead;
	} //iReadXTFFormatFileData


	#pragma pack() // peut etre remplace par l'option de compil -fpack-struct
