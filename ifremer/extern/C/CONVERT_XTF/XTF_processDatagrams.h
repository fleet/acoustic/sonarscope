/*
 * XTF_processDatagrams.h
 *
 *  Created on: 10 oct. 2008
 *      Author: rgallou
 */

#ifndef XTF_PROCESSDATAGRAMS_H_
#define XTF_PROCESSDATAGRAMS_H_
#pragma pack(1) // peut etre remplace par l'option de compil -fpack-struct pour gcc

#include <stdio.h>
#include "XTF_Datagrams.h"
#include "XTF_Utilities.h"

/*** D�clarations des globales li�es aux fonctions ***/
int 	NumSonar,
		NumHidden,
		NumBathy,
		NumAnnotation,
		NumAttitude,
		NumSerial,
		NumSnippet,
		NumBathy7125,
		NumSnippet7125,
		G_NumChannelQPSMB;

// Possible types of data when multiple channels are involved.
#define NUM_DATA_TYPES 7
char *ChannelTypes[NUM_DATA_TYPES];

// For now, assume that the input file will contain 6 or fewer
// channels of data.  This means that the file header will always
// be 1024 bytes.  In the event that a file has more than 6 channels,
// this program will need to be changed.
#define FILEHEADERSIZE (sizeof(XTFFILEHEADER))

int 			iIndexFile,
				iStartPing,
				G_NumSonarChans,
				G_NumBathyChans;

unsigned int 	amt;
unsigned char 	*buffer;
XTFFILEHEADER 	*XTFFileHeader;



/*** D�clarations des prototypes ***/
int iReadXTFFile(	const char *cFileData,
					void(*progress)(int));

unsigned int iReadXTFFormatFileData(int infl,
									unsigned char *buffer);

#pragma pack() // peut etre remplace par l'option de compil -fpack-struct
#endif // XTF_PROCESSDATAGRAMS_H_
