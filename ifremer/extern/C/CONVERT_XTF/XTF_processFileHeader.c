/*
 * XTF_processFileHeader.c
 *
 *  Created on: 13 oct. 2008
 *      Author: rgallou
 */
#pragma pack(1) // peut etre remplace par l'option de compil -fpack-struct pour gcc

#include <sys/types.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "XTF_processFileHeader.h"
#include "XTF_processChanInfoHeader.h"
#include "XTF_processFmtIsis.h"
#include "XTF_Datagrams.h"
#include "XTF_Utilities.h"
#include "main_XTF.h"


#define NB_SIGNAL 		33
#define NB_IMAGE 		0
#define NB_IMAGE2 		0
#define NUM_INDEX_TABLE	-1

extern T_XTFRECORDXML G_strXTFFileHeaderXML;

T_XTFRECORDXML G_strXTFChanInfoHeaderXML[NB_MAX_CHANNELS]; 		// NB_MAX_CHANNELS = NB_BATHY + NBSONAR
T_XTFRECORDXML G_strXTFQPSMultiBeamHeaderXML[NB_MAX_CHANNELS]; 	// Uniquement Bathy Channels
T_XTFRECORDXML G_strXTFBathyHeaderXML; 							// Uniquement Bathy Channel
T_XTFRECORDXML G_strXTFBathymetryXML[NB_MAX_CHANNELS]; 			// Uniquement Bathy Channel

//----------------------------------------------------------------
// Fonction :	iInitXMLBinFileHeader
// Objet 	:	Initialisation de la structure �quivalent au paquet
//				FileHeader pour la manipulation des fichiers bin et XML.
// Modification : 15/10/08
// Auteur 	: GLU
//----------------------------------------------------------------
int iInitXMLBinFileHeader(char *cNomDatagram, char *cComments,
							T_XTFRECORDXML *strXTFFileHeaderXML)
{
	int iLen,
		iLoop;

	unsigned short usTailleVar;

	char cTabNomSignal[NB_SIGNAL][40] = {
			"FileFormat",
			"SystemType",
			"RecordingProgramName",
			"RecordingProgramVersion",
			"SonarName",
			"SonarType",
			"NoteString",
			"ThisFileName",
			"NavUnits",
			"NumberOfSonarChannels",
			"NumberOfBathymetryChannels",
			"NumberOfSnippetsChannels",
			"NumBerOfForwardLookArrays",
			"NumberOfEchoStrengthChannels",
			"NumberOfInterferometryChannels",
			"Reserved1",
			"Reserved2",
			"ReferencePointHeight",
			"ProjectionType",
			"SpheroidType",
			"NavigationLatency",
			"OriginY",
			"OriginX",
			"NavOffsetY",
			"NavOffsetX",
			"NavOffsetZ",
			"NavOffsetYaw",
			"MRUOffsetY",
			"MRUOffsetX",
			"MRUOffsetZ",
			"MRUOffsetYaw",
			"MRUOffsetPitch",
			"MRUOffsetRoll"};

	char cTabTypeSignal[NB_SIGNAL][20] = {
			   "BYTE",
			   "BYTE",
			   "char",
			   "char",
			   "char",
			   "WORD",
			   "char",
			   "char",
			   "WORD",
			   "WORD",
			   "WORD",
			   "BYTE",
			   "BYTE",
			   "WORD",
			   "BYTE",
			   "BYTE",
			   "WORD",
			   "float",
			   "BYTE",
			   "BYTE",
			   "long",
			   "float",
			   "float",
			   "float",
			   "float",
			   "float",
			   "float",
			   "float",
			   "float",
			   "float",
			   "float",
			   "float",
			   "float"};


	// ScaleFactor
	float 	fTabSFSignal[NB_SIGNAL] = {
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0};

	// AddOffset
	float 	fTabAOSignal[NB_SIGNAL] = {
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0};

	char cTabTagVariable[NB_SIGNAL][20]; 		//TODO
	char cTabUnitVariable[NB_SIGNAL][20]; 		//TODO

	// Initialisation des structures de pilotage des fichiers
	// XML et Bin pour le Ping Header
	strXTFFileHeaderXML->sNbVarSignal = NB_SIGNAL;
	strXTFFileHeaderXML->sNbVarImage = NB_IMAGE;
	strXTFFileHeaderXML->strTabSignalXML= (T_XTFVARXML *)malloc(strXTFFileHeaderXML->sNbVarSignal*sizeof(T_XTFVARXML));

	// Ajout d'une marge au cas o� certains caract�res soient doubl�s soient doubl�s.
	iLen = strlen(cNomDatagram) + 30;
	strXTFFileHeaderXML->ptr_cNomHeader = malloc(iLen*sizeof(char));
	sprintf(strXTFFileHeaderXML->ptr_cNomHeader, "%s", cNomDatagram);

	iLen = strlen(cComments)+1;
	strXTFFileHeaderXML->ptr_cCommentsHeader = malloc(iLen*sizeof(char));
	sprintf(strXTFFileHeaderXML->ptr_cCommentsHeader, "%s", cComments);


	// Initialisation g�n�rale des variables (signaux)
	for (iLoop=0; iLoop<strXTFFileHeaderXML->sNbVarSignal; iLoop++) {
	   strXTFFileHeaderXML->strTabSignalXML[iLoop].ptr_fpBin = NULL;
	   strXTFFileHeaderXML->strTabSignalXML[iLoop].bFlagConstant = TRUE;
	   strXTFFileHeaderXML->strTabSignalXML[iLoop].usNbElem = 1;
	   strXTFFileHeaderXML->strTabSignalXML[iLoop].fScaleFactor = fTabSFSignal[iLoop];
	   strXTFFileHeaderXML->strTabSignalXML[iLoop].fAddOffset = fTabAOSignal[iLoop];
	   sprintf(strXTFFileHeaderXML->strTabSignalXML[iLoop].cNom, "%s", cTabNomSignal[iLoop]);
	   sprintf(strXTFFileHeaderXML->strTabSignalXML[iLoop].cType, "%s", cTabTypeSignal[iLoop]);
	   sprintf(strXTFFileHeaderXML->strTabSignalXML[iLoop].cTag, "%s", "TODO");
	   sprintf(strXTFFileHeaderXML->strTabSignalXML[iLoop].cUnit, "%s", "TODO");
	   usTailleVar = util_usNbOctetTypeVar(cTabTypeSignal[iLoop]);
	   strXTFFileHeaderXML->strTabSignalXML[iLoop].usSize = usTailleVar;
	}
	// Initialisations sp�cialis�es des variables
	// Attention : RecordingProgramName est un tab de variables �l�mentaires.
	strXTFFileHeaderXML->strTabSignalXML[2].usNbElem = 8;
	// Attention : RecordingProgramVersion est un tab de variables �l�mentaires.
	strXTFFileHeaderXML->strTabSignalXML[3].usNbElem = 8;
	// Attention : SonarName est un tab de variables �l�mentaires.
	strXTFFileHeaderXML->strTabSignalXML[4].usNbElem = 16;
	// Attention : NoteString est un tab de variables �l�mentaires.
	strXTFFileHeaderXML->strTabSignalXML[6].usNbElem = 64;
	// Attention : ThisFileName est un tab de variables �l�mentaires.
	strXTFFileHeaderXML->strTabSignalXML[7].usNbElem = 64;
	// Attention : ProjectionType est un tab de variables �l�mentaires.
	strXTFFileHeaderXML->strTabSignalXML[18].usNbElem = 12;
	// Attention : SpheroidType est un tab de variables �l�mentaires.
	strXTFFileHeaderXML->strTabSignalXML[19].usNbElem = 10;


	return EXIT_SUCCESS;

} // iInitXMLBinFileHeader


//----------------------------------------------------------------
// Fonction :	iProcessValFileHeader
// Objet 	:	Ecriture des signaux dans les fichiers bin.
// Modification : 15/10/08
// Auteur 	: GLU
//----------------------------------------------------------------
int iProcessValFileHeader(XTFFILEHEADER *FileHeader)
{
	int 		iLoop;

	// Pointeur pour avancer sur les variables automatiquement.
	unsigned char			*ptrFileHeader;
	unsigned short 			usTailleVar,
							usNbElem;

	static void 	*ptr_vValVariable;

   // Ecriture syst�matique dans le fichier binaire.
   ptrFileHeader = (unsigned char *)FileHeader;
   ptr_vValVariable = (void *)ptrFileHeader;
   // Affectation des variables.
   for (iLoop=0; iLoop<(int)G_strXTFFileHeaderXML.sNbVarSignal; iLoop++) {
	   G_strXTFFileHeaderXML.strTabSignalXML[iLoop].ptr_fpBin = NULL;
	   if (iLoop > 0)
	   {
		   usTailleVar = G_strXTFFileHeaderXML.strTabSignalXML[iLoop-1].usSize;
		   usNbElem = G_strXTFFileHeaderXML.strTabSignalXML[iLoop-1].usNbElem;
	   }
	   else
	   {
		   usTailleVar = 0; // Pour le premier paquet
		   usNbElem = 0;
	   }
	   // Pointage de la variable (par sa taille et son occurrence)
	   ptr_vValVariable = ptr_vValVariable + usTailleVar*usNbElem;
	   G_strXTFFileHeaderXML.strTabSignalXML[iLoop].ptr_vPrevVal = (void *)ptr_vValVariable;
   }
   return EXIT_SUCCESS;

} // iProcessValFileHeader




void vProcessXTFHeader(XTFFILEHEADER *ptr_FileHeader) {
/*****************************************************************************/

   int chan = 0;
   char cNomChannelInfo[15];
   CHANINFO *ptr_ChanInfo;
   unsigned char *Ptr = (unsigned char *)ptr_FileHeader;

   // Ini. de la structure du FileHeader pour le traitement des
   // .bin et .XML
   iInitXMLBinFileHeader("FileHeader", "Content of File Header",&G_strXTFFileHeaderXML);

   iProcessValFileHeader(ptr_FileHeader);

   utilXTF_iSaveXMLSignalFile(&G_strXTFFileHeaderXML);

   printf("\nXTF File header information:\n");

   G_NumSonarChans = ptr_FileHeader->NumberOfSonarChannels;
   G_NumBathyChans = ptr_FileHeader->NumberOfBathymetryChannels;

   // Allocation de la m�moire pour les structures ChanInfo qui
   // le FileHeader
   //G_strXTFChanInfoHeaderXML = malloc((NumBathyChans+G_NumSonarChans)*sizeof(T_XTFRECORDXML));

   // How many 1024-byte blocks do we need to read in order
   // get all of the channel info structures?  For now,
   // don't bother looking into the structures.  Just get
   // past them so we can start reading the data in the file.
   {
      unsigned int Cnt = (G_NumSonarChans+G_NumBathyChans);
      if (Cnt > 6) {
         Cnt -= 6;
         Cnt *= sizeof(CHANINFO);
         Cnt += 1023; // bring to next even 1024-byte increment.
         Cnt /= 1024; // use integer math to truncate fraction.
         read(iIndexFile, buffer+1024, Cnt*1024);
      }
   }

   printf("Recording program version: %s\n",ptr_FileHeader->RecordingProgramVersion);

   printf("Number of Sonar channels: %d\n", G_NumSonarChans);



	// On se positionne sur le d�but de description des Informations
	// de Channel.
	Ptr += sizeof(XTFFILEHEADER) - 6*sizeof(CHANINFO);

	for (chan=0; chan<G_NumSonarChans; chan++)
	{
		ptr_ChanInfo = (CHANINFO *)Ptr;
		sprintf(cNomChannelInfo, "ChanInfo_%1d", chan);

		int chtype = ptr_FileHeader->ChanInfo[chan].TypeOfChannel;
		if (chtype >= NUM_DATA_TYPES)
			chtype = NUM_DATA_TYPES-1;

		printf("   Sonar channel %d is %s, %d byte(s) per sample\n",
				 chan,
				 ChannelTypes[chtype],
				 ptr_FileHeader->ChanInfo[chan].BytesPerSample);

		// Initialisation des valeurs du paquet Ping Channel Header
		G_strXTFChanInfoHeaderXML[chan].iNbDatagram = 0;
		iInitXMLBinChanInfoHeader(cNomChannelInfo, "Data for Each Sonar Channel" ,&G_strXTFChanInfoHeaderXML[chan]);
		iProcessValChanInfo(chan, ptr_ChanInfo);
		G_strXTFChanInfoHeaderXML[chan].iNbDatagram++;
		utilXTF_iSaveXMLSignalFile(&G_strXTFChanInfoHeaderXML[chan]);
		// On passe au Channel suivant;
		Ptr += sizeof(CHANINFO);
	}

	printf("Number of Bathymetry channels: %d\n", G_NumBathyChans);
	for (chan=0; chan<G_NumBathyChans; chan++)
	{
		ptr_ChanInfo = (CHANINFO *)Ptr;
		sprintf(cNomChannelInfo, "ChanInfo_%d", chan+G_NumSonarChans);
		printf("   Bathy channel %d is %s, mounted %.1f degrees\n",
			 chan,
			 ChannelTypes[ptr_FileHeader->ChanInfo[chan+G_NumSonarChans].TypeOfChannel],
			 ptr_FileHeader->ChanInfo[chan+G_NumSonarChans].OffsetRoll);


		G_strXTFChanInfoHeaderXML[chan+G_NumSonarChans].iNbDatagram 	= 0;
		G_strXTFBathyHeaderXML.iNbDatagram 								= 0;
		G_strXTFBathymetryXML[chan].iNbDatagram 						= 0;
		G_strXTFQPSMultiBeamHeaderXML[chan].iNbDatagram 				= 0;
		iInitXMLBinChanInfoHeader(cNomChannelInfo, "Data for Each Bathy Channel" ,&G_strXTFChanInfoHeaderXML[chan+G_NumSonarChans]);
		iProcessValChanInfo(chan+G_NumSonarChans, ptr_ChanInfo);
		G_strXTFChanInfoHeaderXML[chan+G_NumSonarChans].iNbDatagram++;
		utilXTF_iSaveXMLSignalFile(&G_strXTFChanInfoHeaderXML[chan+G_NumSonarChans]);
		// On passe au Channel suivant;
		Ptr += sizeof(CHANINFO);
	}

   printf("\n");
} //vProcessXTFHeader

BOOL bReadXTFHeader(int iIndexFile, XTFFILEHEADER *ptr_FileHeader) {
/****************************************************************************/
// Read the header out of the XTF file and get ready to read the data that
// follows in the file.  Returns TRUE if all wend OK.


   //
   // Read file header
   //
   if (read(iIndexFile, ptr_FileHeader, FILEHEADERSIZE) != FILEHEADERSIZE) {
      printf("Error reading file header!\n");
      return FALSE;
   }

   if (ptr_FileHeader->FileFormat != FMT_XTF) {
      printf("Bad header ID (%d) -- this file is not an XTF format file!\n", ptr_FileHeader->FileFormat);
      return FALSE;
   }

   printf("This file contains %ld sonar pings and %ld bathymetry pings\n\n",
      lXTFFmtLastPingNumberInFile(iIndexFile, XTF_HEADER_SONAR, buffer),
      lXTFFmtLastPingNumberInFile(iIndexFile, XTF_HEADER_BATHY, buffer) +
      lXTFFmtLastPingNumberInFile(iIndexFile, XTF_HEADER_ELAC, buffer));

   // align back to start of data.  0xFF matches any kind of packet.
   // GLU le 03/04/2012 : mis en commentaire car provoque le traitement que de la moiti� du fichier.
   // bGoToIsisFmtPing(iIndexFile, iStartPing, 0xFF, buffer);

   // GLU le 03/04/2012 : repositionnement apr�s lecture du FileHeader.
   lseek(iIndexFile, FILEHEADERSIZE, 0);

   return TRUE;

} //bReadXTFHeader

#pragma pack() // peut etre remplace par l'option de compil -fpack-struct
