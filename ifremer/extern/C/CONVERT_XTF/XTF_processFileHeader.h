/*
 * XTF_processFileHeader.h
 *
 *  Created on: 13 oct. 2008
 *      Author: rgallou
 */

#ifndef XTF_PROCESSFILEHEADER_H_
#define XTF_PROCESSFILEHEADER_H_
#pragma pack(1) // peut etre remplace par l'option de compil -fpack-struct pour gcc

#include "XTF_processDatagrams.h"

int iInitXMLBinFileHeader(char *cNomDatagram, char *cComments,
							T_XTFRECORDXML *strXTFFileHeaderXML);

int iProcessValFileHeader(XTFFILEHEADER *FileHeader);

void vProcessXTFHeader(XTFFILEHEADER *XTFFileHeader);
BOOL bReadXTFHeader(int iIndexFile, XTFFILEHEADER *FileHeader);

#pragma pack() // peut etre remplace par l'option de compil -fpack-struct
#endif /* XTF_PROCESSFILEHEADER_H_ */
