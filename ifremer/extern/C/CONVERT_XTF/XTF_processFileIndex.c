/*
 * XTF_processFileIndex.c
 *
 *  Created on: 05 nov. 2009
 *      Author: rgallou
 */
#pragma pack(1) // peut etre remplace par l'option de compil -fpack-struct pour gcc

#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "XTF_Datagrams.h"
#include "XTF_Utilities.h"
#include "Generic_Utilities.h"
#include "XTF_processFileIndex.h"
#include "main_XTF.h"


//----------------------------------------------------------------
// Fonction :	iProcessFileIndexRecord
// Objet 	:	Traitement du paquet FileIndex
// Modif.	: 	03/11/08
// Auteur 	:	GLU
//----------------------------------------------------------------
int iProcessFileIndexRecord(int iNbRecord,
						T_XTFRECORDXML *strXTFRecordXML,
						T_FILEINDEX_RECORD *ptrSignal_prevRecord)
{

	// Pointeur pour avancer sur les variables automatiquement.
	unsigned char		*ptr_cSignalRecord;

    int 	iVarSignal,
			iRetour = 0;

    // On r�initialise errno
    errno = 0;


    unsigned short	usTailleEnByte,
					usNbElem;

	if (iNbRecord == 0)
	{
		iInitXMLBinFileIndexRecord(	"FileIndex",
				"FileIndex",
				"Index File of common data",
				strXTFRecordXML);
	}
	// Ecriture de la donn�e Date.
	ptr_cSignalRecord = (unsigned char *)ptrSignal_prevRecord;
	for (iVarSignal=0; iVarSignal<(int)strXTFRecordXML->sNbVarSignal; iVarSignal++)
	{
		// Pr�paration de l'�criture de la variable.
		usTailleEnByte = strXTFRecordXML->strTabSignalXML[iVarSignal].usSize;
		usNbElem =  strXTFRecordXML->strTabSignalXML[iVarSignal].usNbElem;
		fwrite(ptr_cSignalRecord, usTailleEnByte, usNbElem, strXTFRecordXML->strTabSignalXML[iVarSignal].ptr_fpBin);
		ptr_cSignalRecord += usTailleEnByte*usNbElem;
	}
	iNbRecord++;

	return iRetour;

} //iProcessFileIndexRecord


//----------------------------------------------------------------
// Fonction :	iInitXMLBinFileIndexRecord
// Objet 	:	Initialisation de la structure �quivalent au paquet
//				FileIndex pour la manipulation des fichiers bin et XML.
// Modification : 15/10/08
// Auteur 	: GLU
//----------------------------------------------------------------
int iInitXMLBinFileIndexRecord(	char *cTitleDatagram,
								char *cNomDatagram,
								char *cComments,
								T_XTFRECORDXML *strXTFRecordXML)
{
	char cTabNomVarSignal[4][30] = {
			"Date",
			"Position",
			"NbOfBytesInDatagram",
			"TypeOfDatagram"};

	char cTabNomTypeSignal[4][20] = {
			"double",
			"DWORD",
			"DWORD",
			"BYTE"};

	// ScaleFactor
	float 	fTabSFSignal[4] = {
			1.0,
			1.0,
			1.0,
			1.0};

	// AddOffset
	float 	fTabAOSignal[4] = {
			0.0,
			0.0,
			0.0,
			0.0};

	char	*ptr_cNomHeader,
			*ptr_cRepertoire,
			*ptr_cNomFicPathBin,
			cNomFicPathBin[300];

    int 	iSizeRep,
			iErr,
			iLen,
			iVarSignal;

	unsigned short usTaille;

	// Initialisation des structures de pilotage des fichiers
	// XML et Bin .
	strXTFRecordXML->sNbVarSignal = 4;
	strXTFRecordXML->strTabSignalXML = (T_XTFVARXML *)malloc(strXTFRecordXML->sNbVarSignal*sizeof(T_XTFVARXML));

	iLen = strlen(cNomDatagram)+1;
	strXTFRecordXML->ptr_cNomHeader = malloc(iLen*sizeof(char));
	sprintf(strXTFRecordXML->ptr_cNomHeader, "%s", cNomDatagram);

	iLen = strlen(cTitleDatagram)+1;
	strXTFRecordXML->ptr_cNomHeader = malloc(iLen*sizeof(char));
	sprintf(strXTFRecordXML->ptr_cNomHeader, "%s", cTitleDatagram);

	iLen = strlen(cComments)+1;
	strXTFRecordXML->ptr_cCommentsHeader = malloc(iLen*sizeof(char));
	sprintf(strXTFRecordXML->ptr_cCommentsHeader, "%s", cComments);


	// Affectation et cr�ation du repertoire du Datagramme.
	ptr_cNomHeader = strXTFRecordXML->ptr_cNomHeader;
	iSizeRep = ( strlen(G_cRepData) + strlen( cNomDatagram));
	// Ajout d'une marge au cas o� certains caract�res soient doubl�s soient doubl�s.
	ptr_cRepertoire = malloc( (iSizeRep + 1 + 50)*sizeof(char));
	sprintf(ptr_cRepertoire, "%s\\XTF_%s", G_cRepData, ptr_cNomHeader);
	if (DEBUG)
	   printf("-- Directory Creation : %s\n", ptr_cRepertoire);

	// Cr�ation du r�pertoire
	iErr = mkdir(ptr_cRepertoire);
	if (iErr != 0)
	{
	   switch (errno)
	   {
		   //"File exists"
		   case EEXIST:
			   break;
		   default:
			   printf("Error in Directory creation %s\n", strerror(errno));
			   return EXIT_FAILURE;
			   break;
	   }
	}

	// Initialisation g�n�rale des variables de type Signal
	for (iVarSignal=0; iVarSignal<strXTFRecordXML->sNbVarSignal; iVarSignal++) {
	   strXTFRecordXML->strTabSignalXML[iVarSignal].ptr_fpBin = NULL;
	   // Attention, for�age diff�rent des autres paquets. Les donn�es du FileIndex
	   // ne sont de nature pas constantes.
	   strXTFRecordXML->strTabSignalXML[iVarSignal].bFlagConstant = FALSE;
	   strXTFRecordXML->strTabSignalXML[iVarSignal].usNbElem = 1;
	   sprintf(strXTFRecordXML->strTabSignalXML[iVarSignal].cNom, "%s", cTabNomVarSignal[iVarSignal]);
	   sprintf(strXTFRecordXML->strTabSignalXML[iVarSignal].cType, "%s", cTabNomTypeSignal[iVarSignal]);
	   sprintf(strXTFRecordXML->strTabSignalXML[iVarSignal].cTag, "%s", "TODO");
	   sprintf(strXTFRecordXML->strTabSignalXML[iVarSignal].cUnit, "%s", "TODO");
	   strXTFRecordXML->strTabSignalXML[iVarSignal].fScaleFactor = fTabSFSignal[iVarSignal];
	   strXTFRecordXML->strTabSignalXML[iVarSignal].fAddOffset = fTabAOSignal[iVarSignal];
	   usTaille = util_usNbOctetTypeVar(cTabNomTypeSignal[iVarSignal]);
	   strXTFRecordXML->strTabSignalXML[iVarSignal].usSize = usTaille;
	   iLen = strlen(strXTFRecordXML->strTabSignalXML[iVarSignal].cNom);
	   // Ajout d'une marge au cas o� certains caract�res soient doubl�s soient doubl�s.
	   iLen = strlen(strXTFRecordXML->strTabSignalXML[iVarSignal].cNom);
	   ptr_cNomFicPathBin = malloc( (iSizeRep + iLen + 30)*sizeof(char));
	   sprintf(cNomFicPathBin, "%s\\%s%s", ptr_cRepertoire, strXTFRecordXML->strTabSignalXML[iVarSignal].cNom, ".bin");
	   sprintf(ptr_cNomFicPathBin, "%s\\%s%s", ptr_cRepertoire, strXTFRecordXML->strTabSignalXML[iVarSignal].cNom, ".bin");
	   strXTFRecordXML->strTabSignalXML[iVarSignal].ptr_fpBin = fopen(ptr_cNomFicPathBin, "w+b");
	   if (!strXTFRecordXML->strTabSignalXML[iVarSignal].ptr_fpBin)
	   {
		   printf("%s -- Error in Signal creation  : %s\n", __FILE__, cNomFicPathBin);
		   return EXIT_FAILURE;
	   }
	   free(ptr_cNomFicPathBin);
	}


	free(ptr_cRepertoire);
	return EXIT_SUCCESS;

} // iInitXMLBinFileIndexRecord

#pragma pack() // peut etre remplace par l'option de compil -fpack-struct
