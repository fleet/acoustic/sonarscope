/*
 * XTF_processFileIndex.h
 *
 *  Created on: 21 nov. 2008
 *      Author: rgallou
 */
#pragma pack(1) // peut etre remplace par l'option de compil -fpack-struct pour gcc

#ifndef XTF_PROCESSFILEINDEX_H_
#define XTF_PROCESSFILEINDEX_H_

#include "XTF_DATAGRAMS.H"
#include <sys/types.h>


typedef struct  {
		double	Date;
		DWORD	DatagramPosition;
		DWORD	NbOfBytesInDatagram;
		BYTE	TypeOfDatagram;
} T_FILEINDEX_RECORD;


int iProcessFileIndexRecord(int iNbRecord,
						T_XTFRECORDXML *strXTFRecordXML,
						T_FILEINDEX_RECORD *ptrSignal_prevRecord);


int iInitXMLBinFileIndexRecord(	char *cTitleDatagram,
								char *cNomDatagram, char *cComments,
								T_XTFRECORDXML *strXTFRecordXML);

#endif /* XTF_PROCESSFILEINDEXRECORD_H_ */

#pragma pack() // peut etre remplace par l'option de compil -fpack-struct
