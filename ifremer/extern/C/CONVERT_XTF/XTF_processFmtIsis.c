/*
 * XTF_processFmtIsis.c
 *
 *  Created on: 5 oct. 2011
 *      Author: rgallou
 */

#include "XTF_processFmtIsis.h"
#include "XTF_Datagrams.h"
#include "XTF_processDatagrams.h"


long lFindIsisFmtHeader(unsigned char *buf, long cnt, unsigned char RecordType, int Dir) {
///////////////////////////////////////////////////////////////////////////////////////////
// Somewhere in buf is (probably) an ping header of type RecordType.
// Find it, and return the offset to it.  If Dir!=0, find backwards (from end)
// Returns offset to the start of the record, or -1 if not found.

   long i;

   long start, end, inc;

   if (Dir == 0) {
      start = 0;
      end = cnt-20+1;
      inc = 1;
   }
   else {
      start = cnt-20-1;
      end = -1;
      inc = -1;
   }

      //
      // Sanity check to see if this is actually
      // the start of a data packet.
      //
   for (i=start; i != end; i+=inc) {
      if (
         *((WORD *)&buf[i]) == 0xFACE && // magic number
         (RecordType == 0xFF || buf[i+2] == RecordType) &&

         buf[i+6] == 0    &&
         buf[i+7] == 0    &&

         buf[i+16] < 13   && // month
         buf[i+17] < 33   && // day
         buf[i+18] < 24   && // hour
         buf[i+19] < 60   && // minute
         buf[i+20] < 60) {   // sec

         return i;
      }
   }
   return -1L;
} //lFindIsisFmtHeader


BOOL bAlignIsisFmtFile(int iIndexFile, unsigned char RecordType, unsigned char *TempBuffer) {
///////////////////////////////////////////////////////////////////////////////////////////
// Align an Isis-format file to the beginning of a ping with type RecordType
// Returns TRUE if successful, FALSE if failure.

   long Skip;
   int tries;

   for (tries=0; tries < 10; tries++) {
      int amt = read(iIndexFile, TempBuffer, 16384);
      if (amt <= 0) break;

      Skip = lFindIsisFmtHeader(TempBuffer, amt, RecordType, 0);
      if (Skip != -1) {
         lseek(iIndexFile, Skip-amt, 1);
         return TRUE;
      }
   }
   // printf("\nNever found alignment!\n");
   return FALSE;
} //bAlignIsisFmtFile

long lGetPingNumberFromIsisFmtFile(	int fl,
									unsigned char RecordType,
									unsigned char *TempBuffer) {
///////////////////////////////////////////////////////////////////////////////////////////
// Given the current file alignment, find the next ping of type RecordType
// and return the ping number of that ping.
// Returns Ping Number if successful, -1 if failure.

   XTFPINGHEADER PingHeader;

   if (bAlignIsisFmtFile(fl, RecordType, TempBuffer))
   {
      if (read(fl, &PingHeader, sizeof(XTFPINGHEADER)) != sizeof(XTFPINGHEADER)) {
         return -1L;
      }
      return PingHeader.PingNumber;
   }
   return -1L;

} //lGetPingNumberFromIsisFmtFile

BOOL bGoToIsisFmtPing(	int iIndexFile,
						long DestPingNumber,
						unsigned char RecordType,
						unsigned char *TempBuffer) {
///////////////////////////////////////////////////////////////////////////////////////////
// Given a ping number, this function will position a file to the indicated point in
// a Isis format file (.XTF).  Alignment will be done on sonar pings if
// RecordType==XTF_HEADER_SONAR or Bathy pings if RecordType==XTF_HEADER_BATHY.
// If RecordType == 0xFF, aligns to any packet type.
// Returns TRUE if successful, FALSE otherwise.
//
/* This function used a modified binary search to skip around the file
   and look for packet headers.  This modified binary search looks like
   this pseudo-code:

   ptr = strt of file
   offset = size of file / 2

   do
      check = ping number at (ptr+offset)
      if (check > SonarPingNumber) offset /= 2;
      else ptr += offset;
   until found

*/
   unsigned long BytesInFile, Offset;
   unsigned long ptr = 0L;
   long check;

   BytesInFile = lseek(iIndexFile, 0L, 2);
   if (BytesInFile <= FILEHEADERSIZE) return FALSE;

   BytesInFile -= FILEHEADERSIZE;

   Offset = (BytesInFile/2);

   do {
      lseek(iIndexFile, ptr + Offset + FILEHEADERSIZE, 0);
      check = lGetPingNumberFromIsisFmtFile(iIndexFile, RecordType, TempBuffer);
      if (check == DestPingNumber) break;
      if (check > DestPingNumber || check == -1L) {
         Offset /= 2L;
         if (Offset < 256L) break;
      }
      else {
         ptr += Offset;
      }
   } while (1);

	lseek(iIndexFile, ptr+Offset+FILEHEADERSIZE, 0);
	bAlignIsisFmtFile(iIndexFile, RecordType, TempBuffer);

	return TRUE;
} //


long lXTFFmtLastPingNumberInFile(	int fl,
									unsigned char RecordType,
									unsigned char *TempBuffer) {
//////////////////////////////////////////////////////////////////////////////////////////
// Align an Isis-format file to the beginning of a ping with type RecordType
// Returns Ping Number if successful, -1 otherwise.

   long Skip;
   int amt;
   int tries;
   XTFPINGHEADER *PingHeaderPtr;

      // Try to read from the file backwards 10 times to
      // see if we encounter a ping with this record type.

   for (tries=0; tries < 10; tries++) {

	   lseek(fl, -(16384L * (tries+1)), 2);

      amt = read(fl, TempBuffer, 16384);
      if (amt <= 0) break;

         // Find backwards
      Skip = lFindIsisFmtHeader(TempBuffer, amt, RecordType, 1);
      if (Skip != -1) {
         lseek(fl, Skip-amt, 1);
         read(fl, TempBuffer, 256);
         PingHeaderPtr = (XTFPINGHEADER *) TempBuffer;
         return PingHeaderPtr->PingNumber;
      }
   }

   return 0;
} //lXTFFmtLastPingNumberInFile



