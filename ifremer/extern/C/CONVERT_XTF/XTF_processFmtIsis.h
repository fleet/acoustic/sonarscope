/*
 * XTF_processFmtIsis.h
 *
 *  Created on: 10 oct. 2008
 *      Author: rgallou
 */

#ifndef XTF_PROCESSFMTISIS_H_
#define XTF_PROCESSFMTISIS_H_
#pragma pack(1) // peut etre remplace par l'option de compil -fpack-struct pour gcc

#include "Generic_Utilities.h"

long lFindIsisFmtHeader(unsigned char *buf,
						long cnt,
						unsigned char RecordType,
						int Dir);

BOOL bAlignIsisFmtFile(int infl,
						unsigned char RecordType,
						unsigned char *TempBuffer);

long lGetPingNumberFromIsisFmtFile(	int fl,
									unsigned char RecordType,
									unsigned char *TempBuffer);
BOOL bGoToIsisFmtPing(	int infl,
						long DestPingNumber,
						unsigned char RecordType,
						unsigned char *TempBuffer);

long lXTFFmtLastPingNumberInFile(	int fl,
									unsigned char RecordType,
									unsigned char *TempBuffer);

#pragma pack() // peut etre remplace par l'option de compil -fpack-struct


#endif // XTF_PROCESSFMTISIS_H_
