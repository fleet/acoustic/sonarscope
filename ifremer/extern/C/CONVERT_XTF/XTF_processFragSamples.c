/*
 * XTF_processFragSamples.c
 *
 *  Created on: 22 oct. 2008
 *      Author: rgallou
 */
#pragma pack(1) // peut etre remplace par l'option de compil -fpack-struct pour gcc

#include <sys/types.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "XTF_Utilities.h"
#include "main_XTF.h"
#include "Generic_Utilities.h"    // Contenu dans la librairie libCONVERT_utils.a
#include "XTF_processFragSamples.h"

extern T_XTFRECORDXML *G_strXTFFragSamplesXML;
extern T_XTFRECORDXML *G_strXTFSNP1HeaderXML;
extern T_XTFRECORDXML G_strXTFSideScanPingXML;


//----------------------------------------------------------------
// Fonction :	iInitXMLBinFragSamplesXMLBin
// Objet 	:	Initialisation de la structure �quivalent aux donn�es
//				FragSamples pour la manipulation des fichiers bin et XML.
//				Les donn�es Frag Samples sont incluses dans le paquet
//				SNP1 Snippet.
// Modification : 15/10/08
// Auteur 	: GLU
//----------------------------------------------------------------
int iInitXMLBinFragSamples(char *cNomDatagram, char *cComments, WORD usNumBeam)
{
	int iLoop,
		iLen;

	unsigned short usTailleVar;

	char cTabNomSignal[1][30] = {
			"FragSamples"};


	char cTabTypeSignal[1][20] = {
			"BYTE"};

	// ScaleFactor
	float 	fTabSFSignal[1] = {
			1.0};

	// AddOffset
	float 	fTabAOSignal[1] = {
			0.0};

	char cTabTagVariable[1][20]; 		//TODO
	char cTabUnitVariable[1][20]; 		//TODO


	// Initialisation des structures de pilotage des fichiers
	// XML et Bin pour le paquet FragSamples
	G_strXTFFragSamplesXML[usNumBeam].sNbVarSignal = 1;
	G_strXTFFragSamplesXML[usNumBeam].strTabSignalXML= (T_XTFVARXML *)malloc(G_strXTFFragSamplesXML[usNumBeam].sNbVarSignal*sizeof(T_XTFVARXML));

	iLen = strlen(G_strXTFSNP1HeaderXML[usNumBeam].ptr_cNomHeader)+1;
	G_strXTFFragSamplesXML[usNumBeam].ptr_cNomHeader = malloc(iLen*sizeof(char));
	G_strXTFFragSamplesXML[usNumBeam].ptr_cNomHeader = G_strXTFSNP1HeaderXML[usNumBeam].ptr_cNomHeader;

	iLen = strlen(cComments)+1;
	G_strXTFFragSamplesXML[usNumBeam].ptr_cCommentsHeader = malloc(iLen*sizeof(char));
	sprintf(G_strXTFFragSamplesXML[usNumBeam].ptr_cCommentsHeader, "%s", cComments);

	// Remplissage du nom de la variable d'indexation des tables.
	iLen = strlen(G_strXTFSNP1HeaderXML[usNumBeam].strTabSignalXML[9].cNom)+1;
	G_strXTFFragSamplesXML[usNumBeam].ptr_cNomIndexTable = malloc(iLen*sizeof(char));
	sprintf(G_strXTFFragSamplesXML[usNumBeam].ptr_cNomIndexTable, "%s", G_strXTFSNP1HeaderXML[usNumBeam].strTabSignalXML[9].cNom);


	// Initialisation g�n�rale des tableaux de variables (signaux)
	for (iLoop=0; iLoop<G_strXTFFragSamplesXML[usNumBeam].sNbVarSignal; iLoop++) {
	   G_strXTFFragSamplesXML[usNumBeam].strTabSignalXML[iLoop].bFlagConstant = TRUE;
	   G_strXTFFragSamplesXML[usNumBeam].strTabSignalXML[iLoop].usNbElem = 1;
	   G_strXTFFragSamplesXML[usNumBeam].strTabSignalXML[iLoop].fScaleFactor = fTabSFSignal[iLoop];
	   G_strXTFFragSamplesXML[usNumBeam].strTabSignalXML[iLoop].fAddOffset = fTabAOSignal[iLoop];
	   sprintf(G_strXTFFragSamplesXML[usNumBeam].strTabSignalXML[iLoop].cNom, "%s", cTabNomSignal[iLoop]);
	   sprintf(G_strXTFFragSamplesXML[usNumBeam].strTabSignalXML[iLoop].cType, "%s", cTabTypeSignal[iLoop]);
	   sprintf(G_strXTFFragSamplesXML[usNumBeam].strTabSignalXML[iLoop].cTag, "%s", "TODO");
	   sprintf(G_strXTFFragSamplesXML[usNumBeam].strTabSignalXML[iLoop].cUnit, "%s", "TODO");
	   usTailleVar = util_usNbOctetTypeVar(cTabTypeSignal[iLoop]);
	   G_strXTFFragSamplesXML[usNumBeam].strTabSignalXML[iLoop].usSize = usTailleVar;
	}
	return EXIT_SUCCESS;

} // iInitXMLBinFragSamplesXMLBin

//----------------------------------------------------------------
// Fonction :	iWriteBinFragSamples
// Objet 	:	Ecriture des signaux dans les fichiers bin.
// Modification : 15/10/08
// Auteur 	: GLU
//----------------------------------------------------------------
int iWriteBinFragSamples(int iNbHeader, WORD usNumBeam,
					unsigned char *ptrFragSamples, DWORD BytesPerSamples,
					DWORD SamplesPerChan)
{
	char		*ptr_cRepertoire,
				*ptr_cNomHeader,
				*ptr_cNomPingHeader,
				*ptr_cNomFicPathBin;

	int 		iLenVarName,
				iSizeRep,
				iLoop;

	// Pointeur pour avancer sur les variables automatiquement.
	// Prev pour les zones de donn�es pr�c�dentes.
	DWORD 			ulTailleVar,
					ulNbElemVar;


   ptr_cNomHeader = G_strXTFFragSamplesXML[usNumBeam].ptr_cNomHeader;
   ptr_cNomPingHeader = G_strXTFSideScanPingXML.ptr_cNomHeader;

   if (iNbHeader == 0 )
   {
	   // Affectation du nom du repertoire.
	   // Taille = R�pertoire + S�p�rateur + ("PingHeader" +  S�parateur) + "PingChanHeader" + "_" + N� Channel
	   // Ajout d'une marge au cas o� certains caract�res soient doubl�s soient doubl�s.
	   iSizeRep = ( strlen(G_cRepData) + strlen(ptr_cNomPingHeader) + 1 + strlen( ptr_cNomHeader) + 50);
	   ptr_cRepertoire = malloc(iSizeRep*sizeof(char));
	   sprintf(ptr_cRepertoire, "%s\\XTF_%s\\XTF_%s_%d", G_cRepData, ptr_cNomPingHeader, ptr_cNomHeader, usNumBeam);
	   // Le r�pertoire est d�j� cr�� par les paquets Ping Chan Header.


	   // Affectation des variables.
	   for (iLoop=0; iLoop<(int)G_strXTFFragSamplesXML[usNumBeam].sNbVarSignal; iLoop++) {
		   iLenVarName = strlen(G_strXTFFragSamplesXML[usNumBeam].strTabSignalXML[iLoop].cNom);
		   // Ajout d'une marge au cas o� certains caract�res soient doubl�s soient doubl�s.
		   ptr_cNomFicPathBin = malloc( (iSizeRep + 1 + iLenVarName + 50)*sizeof(char));
		   sprintf(ptr_cNomFicPathBin, "%s\\%s%s", ptr_cRepertoire, G_strXTFFragSamplesXML[usNumBeam].strTabSignalXML[iLoop].cNom, ".bin");
		   G_strXTFFragSamplesXML[usNumBeam].strTabSignalXML[iLoop].ptr_fpBin = fopen(ptr_cNomFicPathBin, "w+b");
		   if (!G_strXTFFragSamplesXML[usNumBeam].strTabSignalXML[iLoop].ptr_fpBin)
		   {
			   printf("%s -- Error in Signal Creation File  : %s\n", __FILE__, ptr_cNomFicPathBin);
			   return EXIT_FAILURE;
		   }
		   free(ptr_cNomFicPathBin);
	   }

#ifndef DEBUG
	   free(ptr_cRepertoire);
#endif
   } // iNbHeader ==0

   // Ecriture syst�matique dans le fichier binaire.

   // Affectation des variables.
   for (iLoop=0; iLoop<(int)G_strXTFFragSamplesXML[usNumBeam].sNbVarSignal; iLoop++) {
	   if (iLoop > 0)
	   {
		   ulTailleVar = (DWORD)G_strXTFFragSamplesXML[usNumBeam].strTabSignalXML[iLoop-1].usSize;
		   ulNbElemVar = (DWORD)G_strXTFFragSamplesXML[usNumBeam].strTabSignalXML[iLoop-1].usNbElem;
	   }
	   else
	   {
		   ulTailleVar = BytesPerSamples; // Pour les donn�es d'imageries
		   ulNbElemVar = SamplesPerChan;
	   }
	   // On suppose les donn�es d'imageries variables d'un Ping sur l'autre.
	   G_strXTFFragSamplesXML[usNumBeam].strTabSignalXML[iLoop].bFlagConstant = FALSE;
	   // Pr�paration de l'�criture de la variable.
	   fwrite(ptrFragSamples, ulTailleVar, ulNbElemVar, G_strXTFFragSamplesXML[usNumBeam].strTabSignalXML[iLoop].ptr_fpBin);

   }
   return EXIT_SUCCESS;

} // vWriteBinFragSamples

//----------------------------------------------------------------
// Fonction :	iSaveXMLFileFragSamples
// Objet 	:	Sauvegarde de la structure XML DOM dans un fichier
//				ad hoc.
// Modification : 15/10/08
// Auteur 	: GLU
//----------------------------------------------------------------
int iSaveXMLFileFragSamples(WORD usNumBeam)
{
	char 	*cNomFicXML,
			*ptr_cNomHeader,
			*ptr_cNomPingHeader,
			cNomFicBin[50],
			*cTypeMatLab,
			cNbElem[4],
			*cValConstant = NULL;

    int 	iSize,
			iLoop,
			iRet = 0;

    unsigned short 	usNbElem,
					usTailleEnByte;

    FILE *fpXML;

    extern BOOL				G_bFlagSortIncPing;
    extern T_XTFRECORDXML 	*G_strXTFFragSamplesXML;

    ptr_cNomPingHeader = G_strXTFSideScanPingXML.ptr_cNomHeader;

    // Ajout d'une marge au cas o� certains caract�res soient doubl�s soient doubl�s.
    iSize = strlen(G_strXTFFragSamplesXML[usNumBeam].ptr_cNomHeader) + 30;
    ptr_cNomHeader = calloc(iSize, sizeof(char));
    ptr_cNomHeader = G_strXTFFragSamplesXML[usNumBeam].ptr_cNomHeader;
    iSize = strlen(G_strXTFSideScanPingXML.ptr_cNomHeader);
    // Ajout d'une marge au cas o� certains caract�res soient doubl�s soient doubl�s.
    ptr_cNomPingHeader = calloc(iSize + 30, sizeof(char));
    ptr_cNomPingHeader = G_strXTFSideScanPingXML.ptr_cNomHeader;
    // Taille = R�pertoire + S�p�rateur + ("PingHeader" + S�p.) + "ChanInfo" + "_" + N� Channel + ".xml"
    // Ajout d'une marge au cas o� certains caract�res soient doubl�s soient doubl�s.
    iSize = strlen(G_cRepData) + 1 + strlen(ptr_cNomPingHeader) + 1 + strlen(ptr_cNomHeader) + 30;
	cNomFicXML = calloc(iSize, sizeof(char));
	sprintf(cNomFicXML, "%s\\XTF_%s\\XTF_%s_%d%s", G_cRepData, ptr_cNomPingHeader, ptr_cNomHeader, usNumBeam, ".xml");
	printf("-- Creation du fichier xml : %s \n", cNomFicXML);

	// Cr�ation de l'ent�te de doc XML
    fpXML = fopen(cNomFicXML, "w+");
    fprintf(fpXML, "%s", "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r");
    fprintf(fpXML, "%s", "<ROOT>\r");
    fprintf(fpXML, "%s%s%s", "\t<Title>", G_strXTFSNP1HeaderXML[usNumBeam].ptr_cNomHeader, "</Title>\r");
    fprintf(fpXML, "%s%s%s", "\t<Constructor>", "Triton", "</Constructor>\r");
    fprintf(fpXML, "%s%s%s", "\t<Model>", G_cSonarName, "</Model>\r");
    fprintf(fpXML, "%s%d%s", "\t<SystemSerialNumber>", G_u32SonarType, "</SystemSerialNumber>\r");
    fprintf(fpXML, "%s%d%s", "\t<NbDatagrams>", G_strXTFSNP1HeaderXML[usNumBeam].iNbDatagram, "</NbDatagrams>\r");
    fprintf(fpXML, "%s%s%s", "\t<TimeOrigin>", "01/01/-4713", "</TimeOrigin>\r");
    fprintf(fpXML, "%s%s%s", "\t<Comments>", G_strXTFSNP1HeaderXML[usNumBeam].ptr_cCommentsHeader, "</Comments>\r");
    if (G_strXTFSNP1HeaderXML[usNumBeam].sNbVarSignal > 0)
    {
    	fprintf(fpXML, "%s", "\t<Variables>\r");


		// Traitement sur l'ensemble des variables.
		for (iLoop=0; iLoop<G_strXTFSNP1HeaderXML[usNumBeam].sNbVarSignal; iLoop++)
		{
			iSize = strlen(G_strXTFSNP1HeaderXML[usNumBeam].strTabSignalXML[iLoop].cType) + 5;
			cTypeMatLab = (char*)malloc(iSize*sizeof(char));
			iRet = util_cConvertTypeLabel(	G_strXTFSNP1HeaderXML[usNumBeam].strTabSignalXML[iLoop].cType,
											cTypeMatLab);

			fprintf(fpXML, "%s", "\t\t<item>\r");
			fprintf(fpXML, "%s%s%s", "\t\t\t<Name>", G_strXTFSNP1HeaderXML[usNumBeam].strTabSignalXML[iLoop].cNom, "</Name>\r");
			fprintf(fpXML, "%s%s%s", "\t\t\t<Storage>", cTypeMatLab, "</Storage>\r");

			if (G_strXTFSNP1HeaderXML[usNumBeam].strTabSignalXML[iLoop].usNbElem > 1)
			{
				sprintf(cNbElem, "%d",G_strXTFSNP1HeaderXML[usNumBeam].strTabSignalXML[iLoop].usNbElem);
				fprintf(fpXML, "%s%s%s", "\t\t\t<NbElem>", cNbElem, "</NbElem>\r");
			}
			//fprintf(fpXML, "%s%s%s", "\t\t\t<Unit>", G_strXTFSNP1HeaderXML[usNumBeam].strTabSignalXML[iLoop].cUnit, "</Unit>\r");
			//fprintf(fpXML, "%s%s%s", "\t\t\t<Tag>", G_strXTFSNP1HeaderXML[usNumBeam].strTabSignalXML[iLoop].cTag, "</Tag>\r");
			if (G_strXTFSNP1HeaderXML[usNumBeam].strTabSignalXML[iLoop].bFlagConstant == TRUE)
			{
				// R�cup�ration des caract�ristiques de la variable d�tect�e comme constante.
				usNbElem = G_strXTFSNP1HeaderXML[usNumBeam].strTabSignalXML[iLoop].usNbElem;
				usTailleEnByte = util_usNbOctetTypeVar(G_strXTFSNP1HeaderXML[usNumBeam].strTabSignalXML[iLoop].cType);
				// Allocation de la valeur � r�cup�rer selon la taille. Si elle est
				// du type Chaine, elle doit contenir autant d'�l�ments que d�clar�s.
				if (strcmp(cTypeMatLab, "char"))
				{
					cValConstant = (char*)malloc(256*sizeof(char));
				}
				else
				{
					cValConstant = (char*)malloc(usNbElem*sizeof(char));
				}
				iRet = util_cValVarConstant(G_strXTFSNP1HeaderXML[usNumBeam].strTabSignalXML[iLoop].cType,
											G_strXTFSNP1HeaderXML[usNumBeam].strTabSignalXML[iLoop].ptr_vPrevVal,
											(int)G_strXTFSNP1HeaderXML[usNumBeam].strTabSignalXML[iLoop].usNbElem,
											cValConstant);
				fprintf(fpXML, "%s%s%s", "\t\t\t<Constant>", cValConstant, "</Constant>\r");
				free(cValConstant);
			}
			else
			{
				sprintf(cNomFicBin, "%s%s",G_strXTFSNP1HeaderXML[usNumBeam].strTabSignalXML[iLoop].cNom, ".bin");
				fprintf(fpXML, "%s%s%s", "\t\t\t<FileName>", cNomFicBin, "</FileName>\r");
			}
			// On n'�crit la variable que si elle n'est pas conforme.
			if (G_bFlagSortIncPing == FALSE)
			{
				fprintf(fpXML, "%s%s%s", "\t\t\t<Direction>",  "FirstValue=LastPing", "</FileName>\r");
			}
			fprintf(fpXML, "%s", "\t\t</item>\r");
			free(cTypeMatLab);
		}
    	fprintf(fpXML, "%s", "\t</Variables>\r");
    	fprintf(fpXML, "%s", "\t<Tables>\r");

		// Traitement sur l'ensemble des variables.
		for (iLoop=0; iLoop<G_strXTFFragSamplesXML[usNumBeam].sNbVarSignal; iLoop++)
		{
			iSize = strlen(G_strXTFFragSamplesXML[usNumBeam].strTabSignalXML[iLoop].cType) + 5;
			cTypeMatLab = (char*)malloc(iSize*sizeof(char));
			iRet = util_cConvertTypeLabel(	cTypeMatLab,
													G_strXTFFragSamplesXML[usNumBeam].strTabSignalXML[iLoop].cType);

			fprintf(fpXML, "%s", "\t\t<item>\r");
			fprintf(fpXML, "%s%s%s", "\t\t\t<Name>", G_strXTFFragSamplesXML[usNumBeam].strTabSignalXML[iLoop].cNom, "</Name>\r");
			fprintf(fpXML, "%s%s%s", "\t\t\t<Storage>", cTypeMatLab, "</Storage>\r");

			if (G_strXTFFragSamplesXML[usNumBeam].strTabSignalXML[iLoop].usNbElem > 1)
			{
				sprintf(cNbElem, "%d",G_strXTFFragSamplesXML[usNumBeam].strTabSignalXML[iLoop].usNbElem);
				fprintf(fpXML, "%s%s%s", "\t\t\t<NbElem>", cNbElem, "</NbElem>\r");
			}
			//fprintf(fpXML, "%s%s%s", "\t\t\t<Unit>", G_strXTFFragSamplesXML[usNumBeam].strTabSignalXML[iLoop].cUnit, "</Unit>\r");
			//fprintf(fpXML, "%s%s%s", "\t\t\t<Tag>", G_strXTFFragSamplesXML[usNumBeam].strTabSignalXML[iLoop].cTag, "</Tag>\r");
			if (G_strXTFFragSamplesXML[usNumBeam].ptr_cNomIndexTable != NULL)
			{
				fprintf(fpXML, "%s%s%s", "\t\t\t<Indexation>", G_strXTFFragSamplesXML[usNumBeam].ptr_cNomIndexTable, "</Indexation>\r");
			}


			// Enregistrement syst�matique dans un fichier Bin
			sprintf(cNomFicBin, "%s%s",G_strXTFFragSamplesXML[usNumBeam].strTabSignalXML[iLoop].cNom, ".bin");
			fprintf(fpXML, "%s%s%s", "\t\t\t<FileName>", cNomFicBin, "</FileName>\r");

			if (G_strXTFFragSamplesXML[usNumBeam].strTabSignalXML[iLoop].fScaleFactor != 1.0)
				fprintf(fpXML, "%s%g%s", "\t\t\t<ScaleFactor>", G_strXTFFragSamplesXML[usNumBeam].strTabSignalXML[iLoop].fScaleFactor, "</ScaleFactor>\r");
			if (G_strXTFFragSamplesXML[usNumBeam].strTabSignalXML[iLoop].fAddOffset != 0.0)
				fprintf(fpXML, "%s%f%s", "\t\t\t<AddOffset>", G_strXTFFragSamplesXML[usNumBeam].strTabSignalXML[iLoop].fAddOffset, "</AddOffset>\r");

	        // On n'�crit la variable que si elle n'est pas conforme.
			if (G_bFlagSortIncPing == FALSE)
			{
				fprintf(fpXML, "%s%s%s", "\t\t\t<Direction>",  "FirstValue=LastPing", "</FileName>\r");
			}
			fprintf(fpXML, "%s", "\t\t</item>\r");
			free(cTypeMatLab);
		} // Fin de la boucle sur les variables

		fprintf(fpXML, "%s", "\t</Tables>\r");

    }
	fprintf(fpXML, "%s", "</ROOT>\r");
	fclose(fpXML);
	free(cNomFicXML);


	return EXIT_SUCCESS;

} //iSaveXMLFileFragSamples

//----------------------------------------------------------------
// Fonction :	iCloseBinFileFragSamples
// Objet 	:	Fermeture (et effacement) des fichiers des signaux du paquet FragSamples.
// Modification : 15/10/08
// Auteur 	: GLU
//----------------------------------------------------------------
int iCloseBinFileFragSamples(WORD usNumBeam)
{
	int iLoop,
		iSizeRep,
		iLenVarName,
		iSizeNameHeader;

	char *ptr_cNomFicPathBin;

	// Traitement de fin : fermeture, effacement, �criture XML pour le
	// paquet FragSamples
	for (iLoop=0; iLoop<G_strXTFFragSamplesXML[usNumBeam].sNbVarSignal; iLoop++)
	{
	   // Fermeture du fichier si pointeur != NULL
	   if (G_strXTFFragSamplesXML[usNumBeam].strTabSignalXML[iLoop].ptr_fpBin != NULL)
	   {
		   fclose(G_strXTFFragSamplesXML[usNumBeam].strTabSignalXML[iLoop].ptr_fpBin);
	   }
	   // Effacement si la valeur est constante.
	   if (G_strXTFFragSamplesXML[usNumBeam].strTabSignalXML[iLoop].bFlagConstant == TRUE)
	   {
		   // Effacement du fichier et enregistrement de la balise Constant
		   // dans l'arbre XML
		   iLenVarName = strlen(G_strXTFFragSamplesXML[usNumBeam].strTabSignalXML[iLoop].cNom);
		   iSizeRep = strlen(G_cRepData);
		   iSizeNameHeader = strlen(G_strXTFFragSamplesXML[usNumBeam].ptr_cNomHeader);
		   // Taille = Rep + S�parateur + ("FragSamples"+S�parateur) + Nom du Paquet + S�parateur + Nom du fichier
		   // Ajout d'une marge au cas o� certains caract�res soient doubl�s soient doubl�s.
		   ptr_cNomFicPathBin = malloc( (iSizeRep + 1 + 11 + iSizeNameHeader + 1 + iLenVarName + 50)*sizeof(char));
		   sprintf(ptr_cNomFicPathBin, "%s\\XTF_%s\\XTF_%s\\%s%s", G_cRepData, G_strXTFSideScanPingXML.ptr_cNomHeader, G_strXTFFragSamplesXML[usNumBeam].ptr_cNomHeader,
								   G_strXTFFragSamplesXML[usNumBeam].strTabSignalXML[iLoop].cNom, ".bin");
		   if (DEBUG)
		   {
			   printf("Effacement du fichier : %s\n", ptr_cNomFicPathBin);
		   }
		   remove((const char *)ptr_cNomFicPathBin);
		   if (ptr_cNomFicPathBin != NULL) {
			   free(ptr_cNomFicPathBin);
			   ptr_cNomFicPathBin = NULL;
		   }
	   }
	} // Fin de la boucle sur les variables du paquet Bin
	return EXIT_SUCCESS;

} // iCloseBinFileFragSamples

#pragma pack() // peut etre remplace par l'option de compil -fpack-struct
