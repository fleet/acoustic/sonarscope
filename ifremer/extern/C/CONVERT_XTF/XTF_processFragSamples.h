/*
 * XTF_processFragSamples.h
 *
 *  Created on: 13 oct. 2008
 *      Author: rgallou
 */

#ifndef XTF_PROCESSFRAGSAMPLES_H_
#define XTF_PROCESSFRAGSAMPLES_H_
#pragma pack(1) // peut etre remplace par l'option de compil -fpack-struct pour gcc

#include "XTF_processDatagrams.h"

int iInitXMLBinFragSamples(	char *cNomDatagram, char *cComments,
							WORD usNumBeam);

int iWriteBinFragSamples(	int iNbHeader,
							WORD usNumBeam,
							unsigned char *ptrFragSamples,
							DWORD BytesPerSample,
							DWORD SamplesPerBeam);

int iSaveXMLFileFragSamples(WORD usNumBeam);

int iCloseBinFileFragSamples(WORD usNumBeam);

#pragma pack() // peut etre remplace par l'option de compil -fpack-struct
#endif /* XTF_PROCESSFRAGSAMPLES_H_ */
