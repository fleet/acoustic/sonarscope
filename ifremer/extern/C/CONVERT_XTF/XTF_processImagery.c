/*
 * XTF_processImagery.c
 *
 *  Created on: 22 oct. 2008
 *      Author: rgallou
 */
#pragma pack(1) // peut etre remplace par l'option de compil -fpack-struct pour gcc

#include <sys/types.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "XTF_processImagery.h"
#include "XTF_Utilities.h"
#include "main_XTF.h"
//#include "Generic_Utilities.h"    // Contenu dans la librairie libCONVERT_utils.a


extern T_XTFRECORDXML *G_strXTFImageryXML;
extern T_XTFRECORDXML G_strXTFPingChanHeaderXML[NB_MAX_CHANNELS];
extern T_XTFRECORDXML G_strXTFSideScanPingXML;


//----------------------------------------------------------------
// Fonction :	iInitXMLBinImageryXMLBin
// Objet 	:	Initialisation de la structure �quivalent aux donn�es
//				Imagery pour la manipulation des fichiers bin et XML.
//				Les donn�es d'Imagerie sont incluses dans le paquet
//				Ping Chan Info.
// Modification : 15/10/08
// Auteur 	: GLU
//----------------------------------------------------------------
int iInitXMLBinImagery(	char *cNomDatagram,
						char *cComments,
						WORD usNumChannel,
						DWORD BytesPerSamples)
{
	int iLoop,
		iLen,
		iNumIndex;

	unsigned short usTailleVar;

	char cTabNomSignal[1][30] = {
			"Imagery"};


	char cTabTypeSignal[1][20] = {
			"BYTE"};

	// ScaleFactor
	float 	fTabSFSignal[1] = {
			1.0};

	// AddOffset
	float 	fTabAOSignal[1] = {
			0.0};

	char cTabTagVariable[1][20]; 		//TODO
	char cTabUnitVariable[1][20]; 		//TODO

	G_strXTFImageryXML[usNumChannel].iNbDatagram = 0;

	// Initialisation des structures de pilotage des fichiers
	// XML et Bin pour le paquet Imagery
	G_strXTFImageryXML[usNumChannel].sNbVarSignal = 1;
	G_strXTFImageryXML[usNumChannel].strTabSignalXML= (T_XTFVARXML *)malloc(G_strXTFImageryXML[usNumChannel].sNbVarSignal*sizeof(T_XTFVARXML));

	iLen = strlen(G_strXTFPingChanHeaderXML[usNumChannel].ptr_cNomHeader)+1;
	G_strXTFImageryXML[usNumChannel].ptr_cNomHeader = malloc(iLen*sizeof(char));
	G_strXTFImageryXML[usNumChannel].ptr_cNomHeader = G_strXTFPingChanHeaderXML[usNumChannel].ptr_cNomHeader;

	iLen = strlen(cComments)+1;
	G_strXTFImageryXML[usNumChannel].ptr_cCommentsHeader = malloc(iLen*sizeof(char));
	sprintf(G_strXTFImageryXML[usNumChannel].ptr_cCommentsHeader, "%s", cComments);

	// Remplissage du nom de la variable d'indexation des tables.
	iNumIndex = 16;	//NumSamples de PingChanHeader
	iLen = strlen(G_strXTFPingChanHeaderXML->strTabSignalXML[iNumIndex].cNom)+1;
	G_strXTFImageryXML[usNumChannel].ptr_cNomIndexTable = malloc(iLen*sizeof(char));
	sprintf(G_strXTFImageryXML[usNumChannel].ptr_cNomIndexTable, "%s", G_strXTFPingChanHeaderXML->strTabSignalXML[iNumIndex].cNom);

	// Initialisation g�n�rale des tableaux de variables (signaux)
	for (iLoop=0; iLoop<G_strXTFImageryXML[usNumChannel].sNbVarSignal; iLoop++) {
	   G_strXTFImageryXML[usNumChannel].strTabSignalXML[iLoop].ptr_fpBin = G_strXTFPingChanHeaderXML[usNumChannel].strTabSignalXML[iLoop].ptr_fpBin;
	   G_strXTFImageryXML[usNumChannel].strTabSignalXML[iLoop].bFlagConstant = TRUE;
	   G_strXTFImageryXML[usNumChannel].strTabSignalXML[iLoop].usNbElem = 1;
	   G_strXTFImageryXML[usNumChannel].strTabSignalXML[iLoop].fScaleFactor = fTabSFSignal[iLoop];
	   G_strXTFImageryXML[usNumChannel].strTabSignalXML[iLoop].fAddOffset = fTabAOSignal[iLoop];
	   sprintf(G_strXTFImageryXML[usNumChannel].strTabSignalXML[iLoop].cNom, "%s", cTabNomSignal[iLoop]);
	   sprintf(G_strXTFImageryXML[usNumChannel].strTabSignalXML[iLoop].cType, "%s", cTabTypeSignal[iLoop]);
	   sprintf(G_strXTFImageryXML[usNumChannel].strTabSignalXML[iLoop].cTag, "%s", "TODO");
	   sprintf(G_strXTFImageryXML[usNumChannel].strTabSignalXML[iLoop].cUnit, "%s", "TODO");
	   usTailleVar = util_usNbOctetTypeVar(cTabTypeSignal[iLoop]);
	   G_strXTFImageryXML[usNumChannel].strTabSignalXML[iLoop].usSize = usTailleVar;
	}

	// Traitement particulier de l'imagerie stock�e en BYTE ou en WORD
	if (BytesPerSamples == 1)
	{
		sprintf(G_strXTFImageryXML[usNumChannel].strTabSignalXML[0].cType, "%s", "BYTE");
	}
	else
	{
		sprintf(G_strXTFImageryXML[usNumChannel].strTabSignalXML[0].cType, "%s", "WORD");
	}
	return EXIT_SUCCESS;

} // iInitXMLBinImageryXMLBin

//----------------------------------------------------------------
// Fonction :	iWriteBinImagery
// Objet 	:	Ecriture des signaux dans les fichiers bin.
// Modification : 15/10/08
// Auteur 	: GLU
//----------------------------------------------------------------
int iWriteBinImagery(int iNbHeader, WORD usNumChannel,
					unsigned char *ptrImagery, DWORD BytesPerSamples,
					DWORD SamplesPerChan)
{
	char		*ptr_cRepertoire,
				*ptr_cNomHeader,
				*ptr_cNomPingHeader,
				*ptr_cNomFicPathBin;

	int 		iLenVarName,
				iSizeRep,
				iLoop,
				iErr;

	// Pointeur pour avancer sur les variables automatiquement.
	// Prev pour les zones de donn�es pr�c�dentes.
	DWORD 			ulTailleVar,
					ulNbElemVar;


   ptr_cNomHeader = G_strXTFImageryXML[usNumChannel].ptr_cNomHeader;
   ptr_cNomPingHeader = G_strXTFSideScanPingXML.ptr_cNomHeader;

   if (iNbHeader == 0 )
   {
	   // Affectation du nom du repertoire.
	   // Taille = R�pertoire + S�p�rateur + ("PingHeader" +  S�parateur) + "PingChanHeader" + "_" + N� Channel
	   iSizeRep = ( strlen(G_cRepData) + strlen(ptr_cNomPingHeader) + 1 + strlen( ptr_cNomHeader) + 50);
	   // Ajout d'une marge au cas o� certains caract�res soient doubl�s soient doubl�s.
	   ptr_cRepertoire = malloc(iSizeRep*sizeof(char));
	   sprintf(ptr_cRepertoire, "%s\\XTF_%s\\XTF_%s_%d", G_cRepData, ptr_cNomPingHeader, ptr_cNomHeader, usNumChannel);
	   // Le r�pertoire est d�j� cr�� par les paquets Ping Chan Header.

	   // Cr�ation du r�pertoire
	   iErr = mkdir(ptr_cRepertoire);
	   if (iErr != 0)
	   {
		   switch (errno)
		   {
			   //"File exists"
			   case EEXIST:
				   break;
			   default:
				   printf("Erreur dans la creation du repertoire %s\n", strerror(errno));
				   return EXIT_FAILURE;
				   break;
		   }
		}
	   // Affectation des variables.
	   for (iLoop=0; iLoop<(int)G_strXTFImageryXML[usNumChannel].sNbVarSignal; iLoop++) {
		   iLenVarName = strlen(G_strXTFImageryXML[usNumChannel].strTabSignalXML[iLoop].cNom);
		   // Ajout d'une marge au cas o� certains caract�res soient doubl�s soient doubl�s.
		   ptr_cNomFicPathBin = malloc( (iSizeRep + 1 + iLenVarName + 50)*sizeof(char));
		   sprintf(ptr_cNomFicPathBin, "%s\\%s%s", ptr_cRepertoire, G_strXTFImageryXML[usNumChannel].strTabSignalXML[iLoop].cNom, ".bin");
		   G_strXTFImageryXML[usNumChannel].strTabSignalXML[iLoop].ptr_fpBin = fopen(ptr_cNomFicPathBin, "w+b");
		   if (!G_strXTFImageryXML[usNumChannel].strTabSignalXML[iLoop].ptr_fpBin)
		   {
			   printf("%s -- Error in Signal Creation File  : %s\n", __FILE__, ptr_cNomFicPathBin);
			   return EXIT_FAILURE;
		   }
		   free(ptr_cNomFicPathBin);
	   }

#ifndef DEBUG
	   free(ptr_cRepertoire);
#endif
   } // iNbHeader ==0

   // Ecriture syst�matique dans le fichier binaire.

   // Affectation des variables.
   for (iLoop=0; iLoop<(int)G_strXTFImageryXML[usNumChannel].sNbVarSignal; iLoop++) {
	   if (iLoop > 0)
	   {
		   ulTailleVar = (DWORD)G_strXTFImageryXML[usNumChannel].strTabSignalXML[iLoop-1].usSize;
		   ulNbElemVar = (DWORD)G_strXTFImageryXML[usNumChannel].strTabSignalXML[iLoop-1].usNbElem;
	   }
	   else
	   {
		   ulTailleVar = BytesPerSamples; // Pour les donn�es d'imageries
		   ulNbElemVar = SamplesPerChan;
	   }
	   // On suppose les donn�es d'imageries variables d'un Ping sur l'autre.
	   G_strXTFImageryXML[usNumChannel].strTabSignalXML[iLoop].bFlagConstant = FALSE;
	   // Pr�paration de l'�criture de la variable.
	   fwrite(ptrImagery, ulTailleVar, ulNbElemVar, G_strXTFImageryXML[usNumChannel].strTabSignalXML[iLoop].ptr_fpBin);

   }
   return EXIT_SUCCESS;

} // vWriteBinImagery

//----------------------------------------------------------------
// Fonction :	iSaveXMLFileImagery
// Objet 	:	Sauvegarde de la structure XML dans un fichier
//				ad hoc. Sauvegarde �galement la description des donn�es
//				d'imagerie que constituent le paquet PingChanHeader_xxx.
// Modification : 15/10/08
// Auteur 	: GLU
//----------------------------------------------------------------
int iSaveXMLFileImagery(WORD usNumChannel)
{
	char 	*cNomFicXML,
			*ptr_cNomHeader,
			*ptr_cNomPingHeader,
			cNomFicBin[50],
			*cTypeMatLab,
			cNbElem[4],
			*cValConstant;

    int 	iSize,
			iLoop,
			iRet = 0;

    extern BOOL				G_bFlagSortIncPing;
    extern T_XTFRECORDXML 	*G_strXTFImageryXML;

    FILE	*fpXML;

    unsigned short 	usNbElem,
					usTailleEnByte;

    ptr_cNomPingHeader = G_strXTFSideScanPingXML.ptr_cNomHeader;

    iSize = strlen(G_strXTFImageryXML[usNumChannel].ptr_cNomHeader);
    ptr_cNomHeader = calloc(iSize, sizeof(char));
    ptr_cNomHeader = G_strXTFImageryXML[usNumChannel].ptr_cNomHeader;
    iSize = strlen(G_strXTFSideScanPingXML.ptr_cNomHeader);
    ptr_cNomPingHeader = calloc(iSize, sizeof(char));
    ptr_cNomPingHeader = G_strXTFSideScanPingXML.ptr_cNomHeader;
    // Taille = R�pertoire + S�p�rateur + ("PingHeader" + S�p.) + "ChanInfo" + "_" + N� Channel + ".xml"
	iSize = strlen(G_cRepData) + 1 + strlen(ptr_cNomPingHeader) + 1 + strlen(ptr_cNomHeader) + 30;
	cNomFicXML = calloc(iSize, sizeof(char));
	sprintf(cNomFicXML, "%s\\XTF_%s\\XTF_%s_%d%s", G_cRepData, ptr_cNomPingHeader, ptr_cNomHeader, usNumChannel, ".xml");
	printf("-- Creation du fichier xml : %s \n", cNomFicXML);

	// Cr�ation de l'ent�te de doc XML
    fpXML = fopen(cNomFicXML, "w+");
    fprintf(fpXML, "%s", "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r");
    fprintf(fpXML, "%s", "<ROOT>\r");
    fprintf(fpXML, "%s%s%s", "\t<Title>", G_strXTFPingChanHeaderXML[usNumChannel].ptr_cNomHeader, "</Title>\r");
    fprintf(fpXML, "%s%s%s", "\t<Constructor>", "Triton", "</Constructor>\r");
    fprintf(fpXML, "%s%s%s", "\t<Model>", G_cSonarName, "</Model>\r");
    fprintf(fpXML, "%s%d%s", "\t<SystemSerialNumber>", G_u32SonarType, "</SystemSerialNumber>\r");
    fprintf(fpXML, "%s%d%s", "\t<NbDatagrams>", G_strXTFPingChanHeaderXML[usNumChannel].iNbDatagram, "</NbDatagrams>\r");
    fprintf(fpXML, "%s%s%s", "\t<TimeOrigin>", "01/01/-4713", "</TimeOrigin>\r");
    fprintf(fpXML, "%s%s%s", "\t<Comments>", G_strXTFPingChanHeaderXML[usNumChannel].ptr_cCommentsHeader, "</Comments>\r");
    if (G_strXTFPingChanHeaderXML[usNumChannel].sNbVarSignal > 0)
    {
    	fprintf(fpXML, "%s", "\t<Variables>\r");

		// Traitement sur l'ensemble des variables.
		for (iLoop=0; iLoop<G_strXTFPingChanHeaderXML[usNumChannel].sNbVarSignal; iLoop++)
		{
			iSize = strlen(G_strXTFPingChanHeaderXML[usNumChannel].strTabSignalXML[iLoop].cType) + 5;
			cTypeMatLab = (char*)malloc(iSize*sizeof(char));
			iRet = util_cConvertTypeLabel(	G_strXTFPingChanHeaderXML[usNumChannel].strTabSignalXML[iLoop].cType,
											cTypeMatLab);

			fprintf(fpXML, "%s", "\t\t<item>\r");
			fprintf(fpXML, "%s%s%s", "\t\t\t<Name>", G_strXTFPingChanHeaderXML[usNumChannel].strTabSignalXML[iLoop].cNom, "</Name>\r");
			fprintf(fpXML, "%s%s%s", "\t\t\t<Storage>", cTypeMatLab, "</Storage>\r");
			//fprintf(fpXML, "%s%s%s", "\t\t\t<Unit>", G_strXTFPingChanHeaderXML[usNumChannel].strTabSignalXML[iLoop].cUnit, "</Unit>\r");
			//fprintf(fpXML, "%s%s%s", "\t\t\t<Tag>", G_strXTFPingChanHeaderXML[usNumChannel].strTabSignalXML[iLoop].cTag, "</Tag>\r");

			if (G_strXTFPingChanHeaderXML[usNumChannel].strTabSignalXML[iLoop].usNbElem > 1)
			{
				sprintf(cNbElem, "%d", G_strXTFPingChanHeaderXML[usNumChannel].strTabSignalXML[iLoop].usNbElem);
				fprintf(fpXML, "%s%s%s", "\t\t\t<NbElem>", cNbElem, "</NbElem>\r");
			}
			if (G_strXTFPingChanHeaderXML[usNumChannel].strTabSignalXML[iLoop].bFlagConstant == TRUE)
			{
				// R�cup�ration des caract�ristiques de la variable d�tect�e comme constante.
				usNbElem = G_strXTFPingChanHeaderXML[usNumChannel].strTabSignalXML[iLoop].usNbElem;
				usTailleEnByte = util_usNbOctetTypeVar(G_strXTFPingChanHeaderXML[usNumChannel].strTabSignalXML[iLoop].cType);
				// Allocation de la valeur � r�cup�rer selon la taille. Si elle est
				// du type Chaine, elle doit contenir autant d'�l�ments que d�clar�s.
				if (strcmp(cTypeMatLab, "char"))
				{
					cValConstant = (char*)malloc(256*sizeof(char));
				}
				else
				{
					cValConstant = (char*)malloc(usNbElem*sizeof(char));
				}
				iRet = util_cValVarConstant(G_strXTFPingChanHeaderXML[usNumChannel].strTabSignalXML[iLoop].cType,
											G_strXTFPingChanHeaderXML[usNumChannel].strTabSignalXML[iLoop].ptr_vPrevVal,
											(int)G_strXTFPingChanHeaderXML[usNumChannel].strTabSignalXML[iLoop].usNbElem,
											cValConstant);
				fprintf(fpXML, "%s%s%s", "\t\t\t<Constant>", cValConstant, "</Constant>\r");
				free(cValConstant);
			}
			else
			{
				sprintf(cNomFicBin, "%s%s",G_strXTFPingChanHeaderXML[usNumChannel].strTabSignalXML[iLoop].cNom, ".bin");
				fprintf(fpXML, "%s%s%s", "\t\t\t<FileName>", cNomFicBin, "</FileName>\r");
			}
			// On n'�crit la variable que si elle n'est pas conforme.
			if (G_bFlagSortIncPing == FALSE)
			{
				fprintf(fpXML, "%s%s%s", "\t\t\t<Direction>",  "FirstValue=LastPing", "</FileName>\r");
			}
			fprintf(fpXML, "%s", "\t\t</item>\r");
			free(cTypeMatLab);

		} //Traitement sur l'ensemble des variables du Ping channel Header
    	fprintf(fpXML, "%s", "\t</Variables>\r");

    	fprintf(fpXML, "%s", "\t<Tables>\r");
		// Traitement sur l'ensemble des variables.
		for (iLoop=0; iLoop<G_strXTFImageryXML[usNumChannel].sNbVarSignal; iLoop++)
		{
			iSize = strlen(G_strXTFImageryXML[usNumChannel].strTabSignalXML[iLoop].cType) + 5;
			cTypeMatLab = (char*)malloc(iSize*sizeof(char));
			iRet = util_cConvertTypeLabel(G_strXTFImageryXML[usNumChannel].strTabSignalXML[iLoop].cType,
												cTypeMatLab);
			fprintf(fpXML, "%s", "\t\t<item>\r");
			fprintf(fpXML, "%s%s%s", "\t\t\t<Name>", G_strXTFImageryXML[usNumChannel].strTabSignalXML[iLoop].cNom, "</Name>\r");
			fprintf(fpXML, "%s%s%s", "\t\t\t<Storage>", cTypeMatLab, "</Storage>\r");
			//fprintf(fpXML, "%s%s%s", "\t\t\t<Unit>", G_strXTFImageryXML[usNumChannel].strTabSignalXML[iLoop].cUnit, "</Unit>\r");
			//fprintf(fpXML, "%s%s%s", "\t\t\t<Tag>", G_strXTFImageryXML[usNumChannel].strTabSignalXML[iLoop].cTag, "</Tag>\r");
			if (G_strXTFImageryXML[usNumChannel].strTabSignalXML[iLoop].usNbElem > 1)
			{
				sprintf(cNbElem, "%d", G_strXTFImageryXML[usNumChannel].strTabSignalXML[iLoop].usNbElem);
				fprintf(fpXML, "%s%s%s", "\t\t\t<NbElem>", cNbElem, "</NbElem>\r");
			}
			if (G_strXTFImageryXML[usNumChannel].ptr_cNomIndexTable != NULL)
			{
				fprintf(fpXML, "%s%s%s", "\t\t\t<Indexation>", G_strXTFImageryXML[usNumChannel].ptr_cNomIndexTable, "</Indexation>\r");
			}

			// Enregistrement syst�matique dans un fichier Bin
			sprintf(cNomFicBin, "%s%s",G_strXTFImageryXML[usNumChannel].strTabSignalXML[iLoop].cNom, ".bin");
			fprintf(fpXML, "%s%s%s", "\t\t\t<FileName>", cNomFicBin, "</FileName>\r");

			if (G_strXTFImageryXML[usNumChannel].strTabSignalXML[iLoop].fScaleFactor != 1.0)
				fprintf(fpXML, "%s%g%s", "\t\t\t<ScaleFactor>", G_strXTFImageryXML[usNumChannel].strTabSignalXML[iLoop].fScaleFactor, "</ScaleFactor>\r");
			if (G_strXTFImageryXML[usNumChannel].strTabSignalXML[iLoop].fAddOffset != 0.0)
				fprintf(fpXML, "%s%f%s", "\t\t\t<AddOffset>", G_strXTFImageryXML[usNumChannel].strTabSignalXML[iLoop].fAddOffset, "</AddOffset>\r");

			// On n'�crit la variable que si elle n'est pas conforme.
			if (G_bFlagSortIncPing == FALSE)
			{
				fprintf(fpXML, "%s%s%s", "\t\t\t<Direction>",  "FirstValue=LastPing", "</FileName>\r");
			}
			fprintf(fpXML, "%s", "\t\t</item>\r");
			free(cTypeMatLab);

		} // Fin de la boucle sur les variables de l'imagerie
		fprintf(fpXML, "%s", "\t</Tables>\r");

    }
	fprintf(fpXML, "%s", "</ROOT>\r");
	fclose(fpXML);

	free(cNomFicXML);


	return iRet;

} //iSaveXMLFileImagery

//----------------------------------------------------------------
// Fonction :	iCloseBinFileImagery
// Objet 	:	Fermeture (et effacement) des fichiers des signaux du paquet Imagery.
// Modification : 15/10/08
// Auteur 	: GLU
//----------------------------------------------------------------
int iCloseBinFileImagery(WORD usNumChannel)
{
	int iLoop,
		iSizeRep,
		iLenVarName,
		iSizeNameHeader;

	char *ptr_cNomFicPathBin;

	// Traitement de fin : fermeture, effacement, �criture XML pour le
	// paquet Imagery
	for (iLoop=0; iLoop<G_strXTFImageryXML[usNumChannel].sNbVarSignal; iLoop++)
	{
	   // Fermeture du fichier si pointeur != NULL
	   if (G_strXTFImageryXML[usNumChannel].strTabSignalXML[iLoop].ptr_fpBin != NULL)
	   {
		   fclose(G_strXTFImageryXML[usNumChannel].strTabSignalXML[iLoop].ptr_fpBin);
	   }
	   // Effacement si la valeur est constante.
	   if (G_strXTFImageryXML[usNumChannel].strTabSignalXML[iLoop].bFlagConstant == TRUE)
	   {
		   // Effacement du fichier et enregistrement de la balise Constant
		   // dans l'arbre XML
		   iLenVarName = strlen(G_strXTFImageryXML[usNumChannel].strTabSignalXML[iLoop].cNom);
		   iSizeRep = strlen(G_cRepData);
		   iSizeNameHeader = strlen(G_strXTFImageryXML[usNumChannel].ptr_cNomHeader);
		   // Taille = Rep + S�parateur + ("Imagery"+S�parateur) + Nom du Paquet + S�parateur + Nom du fichier
		   // Ajout d'une marge au cas o� certains caract�res soient doubl�s soient doubl�s.
		   ptr_cNomFicPathBin = malloc( (iSizeRep + 1 + 11 + iSizeNameHeader + 1 + iLenVarName + 50)*sizeof(char));
		   sprintf(ptr_cNomFicPathBin, "%s\\XTF_%s\\XTF_%s\\%s%s", G_cRepData, G_strXTFSideScanPingXML.ptr_cNomHeader, G_strXTFImageryXML[usNumChannel].ptr_cNomHeader,
								   G_strXTFImageryXML[usNumChannel].strTabSignalXML[iLoop].cNom, ".bin");
		   if (DEBUG)
		   {
			   printf("Effacement du fichier : %s\n", ptr_cNomFicPathBin);
		   }
		   remove((const char *)ptr_cNomFicPathBin);
		   if (ptr_cNomFicPathBin != NULL) {
			   free(ptr_cNomFicPathBin);
			   ptr_cNomFicPathBin = NULL;
		   }
	   }
	} // Fin de la boucle sur les variables du paquet Bin
	return EXIT_SUCCESS;

} // iCloseBinFileImagery

#pragma pack() // peut etre remplace par l'option de compil -fpack-struct
