/*
 * XTF_processImagery.h
 *
 *  Created on: 13 oct. 2008
 *      Author: rgallou
 */

#ifndef XTF_PROCESSIMAGERY_H_
#define XTF_PROCESSIMAGERY_H_
#pragma pack(1) // peut etre remplace par l'option de compil -fpack-struct pour gcc

#include "XTF_processDatagrams.h"

int iInitXMLBinImagery(	char *cNomDatagram, char *cComments,
						WORD usNumChannel,
						DWORD BytesPerSamples);
int iWriteBinImagery(int iNbHeader, WORD usNumChannel, unsigned char *ptrImagery,
						DWORD BytesPerSample, DWORD SamplesPerChan);

int iSaveXMLFileImagery(WORD usNumChannel);

int iCloseBinFileImagery(WORD usNumChannel);

#pragma pack() // peut etre remplace par l'option de compil -fpack-struct
#endif /* XTF_PROCESSIMAGERY_H_ */
