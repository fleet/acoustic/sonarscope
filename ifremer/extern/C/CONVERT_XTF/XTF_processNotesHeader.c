/*
 * XTF_processNotesHeader.c
 *
 *  Created on: 13 oct. 2008
 *      Author: rgallou
 */
#pragma pack(1) // peut etre remplace par l'option de compil -fpack-struct pour gcc

#include <sys/types.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "XTF_processNotesHeader.h"
#include "XTF_Utilities.h"
#include "XTF_Datagrams.h"
#include "main_XTF.h"


T_XTFRECORDXML G_strXTFNotesHeaderXML;

#define NB_SIGNAL 		14
#define NB_IMAGE 		0
#define NB_IMAGE2 		0
#define NUM_INDEX_TABLE	-1


//----------------------------------------------------------------
// Fonction :	iInitXMLBinNotesHeader
// Objet 	:	Initialisation de la structure équivalent au paquet
//				NotsHeader pour la manipulaiton des fichiers bin et XML.
// Modification : 15/10/08
// Auteur 	: GLU
//----------------------------------------------------------------
int iInitXMLBinNotesHeader(		char *cTitleDatagram,
								char *cNomDatagram,
								char *cComments,
								T_XTFRECORDXML *strXTFRecordXML)
{
	int iRet;

	T_DESC_SIGNAL_OR_IMAGE strDescriptionSignal[NB_SIGNAL] = {
			{"MagicNumber",			"WORD",	1.0,	0.0},
			{"HeaderType",			"BYTE",	1.0,	0.0},
			{"SubChannelNumber",	"BYTE",	1.0,	0.0},
			{"NumChanToFollow",		"WORD",	1.0,	0.0},
			{"Reserved",			"WORD",	1.0,	0.0},
			{"NumBytesThisRecord",	"DWORD",1.0,	0.0},
			{"Year",				"WORD",	1.0,	0.0},
			{"Month",				"BYTE",	1.0,	0.0},
			{"Day",					"BYTE",	1.0,	0.0},
			{"Hour",				"BYTE",	1.0,	0.0},
			{"Minute",				"BYTE",	1.0,	0.0},
			{"Second",				"BYTE",	1.0,	0.0},
			{"ReservedBytes",		"WORD",	1.0,	0.0},
			{"NotesText",			"BYTE",	1.0,	0.0}};


	T_DESC_SIGNAL_OR_IMAGE strDescriptionImage[NB_IMAGE] = {};
	T_DESC_SIGNAL_OR_IMAGE strDescriptionImage2[NB_IMAGE2] = {};

	T_DESC_TAB_SIGANDIMG strDescriptionTab = {NB_SIGNAL,
												NB_IMAGE,
												NUM_INDEX_TABLE,
												strDescriptionSignal,
												strDescriptionImage};


	// Lancement de l'initialisation des structures de signaux : ouvertures des
	// fichiers, copie des structures, ...
	iRet = utilXTF_initXMLRecord(	strDescriptionTab,
									cTitleDatagram,
									cNomDatagram,
									cComments,
									strXTFRecordXML);

	// Initialisations spécialisées des variables
	// Attention : Reserved est un tab de variables élémentaires.
	strXTFRecordXML->strTabSignalXML[4].usNbElem = 2;
	// Attention : ReservedBytes est un tab de variables élémentaires.
	strXTFRecordXML->strTabSignalXML[12].usNbElem = 35;
	// Attention : NotesText est un tab de variables élémentaires.
	strXTFRecordXML->strTabSignalXML[13].usNbElem = 200;

	return iRet;


}	//iInitXMLBinNotesHeader


//----------------------------------------------------------------
// Fonction :	iWriteBinNotesHeader
// Objet 	:	Ecriture des signaux dans les fichiers bin.
// Modification : 15/10/08
// Auteur 	: GLU
//----------------------------------------------------------------
int iWriteBinNotesHeader(int iNbHeader, XTFNOTESHEADER *NotesHeader)
{
	char		*ptr_cRepertoire,
				*ptr_cNomHeader,
				*ptr_cNomFicPathBin;

	int 		iLenVarName,
				iSizeRep,
				iErr,
				iLoop;

	// Pointeur pour avancer sur les variables automatiquement.
	unsigned char			*ptrNotesHeader;
	static unsigned char	*ptrPrevNotesHeader;
	unsigned short 			usTailleVar,
							usNbElem;

	BOOL		bFlagConstant,
				bFlagCompare;

	static void 	*ptr_vValVariable,
					*ptr_vPrevVal;

	static DWORD 	iPrevPingNumber = 0;
	DWORD		 	iPingNumber;

	extern BOOL 	G_bFlagSortIncPing;

   ptr_cNomHeader = G_strXTFNotesHeaderXML.ptr_cNomHeader;

   // Ecriture systématique dans le fichier binaire.
   ptrNotesHeader = (unsigned char *)NotesHeader;
   ptr_vValVariable = (void *)ptrNotesHeader;
   ptr_vPrevVal = (void *)ptrPrevNotesHeader;
   // Affectation des variables.
   for (iLoop=0; iLoop<(int)G_strXTFNotesHeaderXML.sNbVarSignal; iLoop++) {
	   if (iLoop > 0)
	   {
		   usTailleVar = G_strXTFNotesHeaderXML.strTabSignalXML[iLoop-1].usSize;
		   usNbElem = G_strXTFNotesHeaderXML.strTabSignalXML[iLoop-1].usNbElem;
	   }
	   else
	   {
		   usTailleVar = 0; // Pour le premier paquet
		   usNbElem = 0;
	   }
	   bFlagConstant = G_strXTFNotesHeaderXML.strTabSignalXML[iLoop].bFlagConstant;
	   // Pointage de la variable (par sa taille et son occurrence)
	   ptr_vValVariable = ptr_vValVariable + usTailleVar*usNbElem;
	   if (iNbHeader > 0)
	   {
		   ptr_vPrevVal = ptr_vPrevVal + usTailleVar*usNbElem;
		   bFlagCompare = util_bComparePrevAndCurrentVal(G_strXTFNotesHeaderXML.strTabSignalXML[iLoop].cType,
											   ptr_vPrevVal,
											   ptr_vValVariable,
											   G_strXTFNotesHeaderXML.strTabSignalXML[iLoop].usNbElem);
		   bFlagConstant = bFlagConstant && bFlagCompare;
		   G_strXTFNotesHeaderXML.strTabSignalXML[iLoop].ptr_vPrevVal = ptr_vPrevVal;
		   // Observation de l'ordre des Ping pour l'attribut Direction.
		   if (!strcmp(G_strXTFNotesHeaderXML.strTabSignalXML[iLoop].cNom, "PingNumber"))
		   {
			   iPrevPingNumber = *((DWORD *)ptr_vPrevVal);
			   if (iPrevPingNumber != 0)
			   {
				   iPingNumber = *((DWORD *)ptr_vValVariable);
				   if (iPrevPingNumber > iPingNumber)
				   {
					   G_bFlagSortIncPing = FALSE;
				   }
				   else
				   {
					   G_bFlagSortIncPing = TRUE;
				   }
			   }
		   }
	   }
	   else
	   {
		   G_strXTFNotesHeaderXML.strTabSignalXML[iLoop].ptr_vPrevVal = ptr_vValVariable;
	   }
	   G_strXTFNotesHeaderXML.strTabSignalXML[iLoop].bFlagConstant = bFlagConstant;
	   // Préparation de l'écriture de la variable.
	   usTailleVar = G_strXTFNotesHeaderXML.strTabSignalXML[iLoop].usSize;
	   usNbElem =  G_strXTFNotesHeaderXML.strTabSignalXML[iLoop].usNbElem;
	   fwrite(ptr_vValVariable, usTailleVar, usNbElem, G_strXTFNotesHeaderXML.strTabSignalXML[iLoop].ptr_fpBin);

   }
   ptrPrevNotesHeader = malloc(sizeof(XTFNOTESHEADER));
   // Recopie en zone tampon des valeurs courantes.
   memcpy(ptrPrevNotesHeader, ptrNotesHeader, sizeof(XTFNOTESHEADER));

   return EXIT_SUCCESS;

} // vWriteBinNotesHeader

void vProcessNotes(XTFNOTESHEADER *NoteHeader) {
/****************************************************************************/
// Stored in the XTF file whenever the user type in notes during data
// collection.
//
	static int iNbHeader = 0;

	//printf("\nNote: %s\n", NoteHeader->NotesText);
   iWriteBinNotesHeader(iNbHeader, NoteHeader);

   iNbHeader++;

} //vProcessNotes


#pragma pack() // peut etre remplace par l'option de compil -fpack-struct
