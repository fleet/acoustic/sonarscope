/*
 * XTF_processNotesHeader.h
 *
 *  Created on: 13 oct. 2008
 *      Author: rgallou
 */

#ifndef XTF_PROCESSNOTESHEADER_H_
#define XTF_PROCESSNOTESHEADER_H_
#pragma pack(1) // peut etre remplace par l'option de compil -fpack-struct pour gcc

#include "XTF_processDatagrams.h"

int iInitXMLBinNotesHeader(	char *cTitleDatagram,
								char *cNomDatagram,
								char *cComments,
								T_XTFRECORDXML *strXTFRecordXML);

int iWriteBinNotesHeader(int iNbHeader, XTFNOTESHEADER *NotesHeader);

void vProcessNotes(XTFNOTESHEADER *NoteHeader);

#pragma pack() // peut etre remplace par l'option de compil -fpack-struct
#endif /* XTF_PROCESSNotesHeader_H_ */
