/*
 * XTF_processPingChanHeader.c
 *
 *  Created on: 13 oct. 2008
 *      Author: rgallou
 */
#pragma pack(1) // peut etre remplace par l'option de compil -fpack-struct pour gcc

#include <sys/types.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "XTF_processPingChanHeader.h"
#include "XTF_Utilities.h"
#include "main_XTF.h"


//extern T_XTFRECORDXML *G_strXTFPingChanHeaderXML;
extern T_XTFRECORDXML G_strXTFPingChanHeaderXML[NB_MAX_CHANNELS];
extern T_XTFRECORDXML G_strXTFSideScanPingXML;
//----------------------------------------------------------------
// Fonction :	iInitXMLBinPingChanHeader
// Objet 	:	Initialisation de la structure �quivalent au paquet
//				PingChanHeader pour la manipulation des fichiers bin et XML.
// Modification : 15/10/08
// Auteur 	: GLU
//----------------------------------------------------------------
int iInitXMLBinPingChanHeader(	char *cNomDatagram,
								char *cComments,
								WORD usNumChannel)
{
	char		*ptr_cRepertoire,
				*ptr_cNomHeader,
				*ptr_cNomPingHeader,
				*ptr_cNomFicPathBin;

	int 		iLen,
				iSizeRep,
				iLoop,
				iErr;

	unsigned short usTailleVar;

	char cTabNomSignal[24][30] = {
			"ChannelNumber",
			"DownSampleModel",
			"SlantRange",
			"GroundRange",
			"TimeDelay",
			"TimeDuration",
			"SecondsPerPing",
			"ProcessingFlags",
			"Frequency",
			"InitialGainCode",
			"GainCode",
			"BandWith",
			"ContactNumber",
			"ContactClassification",
			"ContactSubNumber",
			"ContactType",
			"NumSamples",
			"MillivoltScale",
			"ContactTimeOffTrack",
			"ContactCloseNumber",
			"Reserved2",
			"FixedVSOP",
			"Weight",
			"ReserveSpace"};

	char cTabTypeSignal[24][20] = {
			"WORD",
			"WORD",
			"float",
			"float",
			"float",
			"float",
			"float",
			"WORD",
			"WORD",
			"WORD",
			"WORD",
			"WORD",
			"DWORD",
			"WORD",
			"BYTE",
			"BYTE",
			"DWORD",
			"WORD",
			"float",
			"BYTE",
			"BYTE",
			"float",
			"short",
			"BYTE"};

	// ScaleFactor
	float 	fTabSFSignal[24] = {
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0};

	// AddOffset
	float 	fTabAOSignal[24] = {
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0};

   ptr_cNomHeader 		= cNomDatagram;
   ptr_cNomPingHeader 	= G_strXTFSideScanPingXML.ptr_cNomHeader;

   // Affectation du nom du repertoire.
   // Taille = R�pertoire + S�p�rateur + ("PingHeader" +  S�parateur) + "PingChanHeader" + "_" + N� Channel
   iSizeRep = ( strlen(G_cRepData) + strlen(ptr_cNomPingHeader) + 1 + strlen( ptr_cNomHeader) + 50);
   // Ajout d'une marge au cas o� certains caract�res soient doubl�s soient doubl�s.
   ptr_cRepertoire = malloc(iSizeRep*sizeof(char));
   sprintf(ptr_cRepertoire, "%s\\XTF_%s\\XTF_%s_%d", G_cRepData, ptr_cNomPingHeader, ptr_cNomHeader, usNumChannel);
   // Le r�pertoire est d�j� cr�� par les paquets Ping Chan Header.

   // Cr�ation du r�pertoire
   iErr = mkdir(ptr_cRepertoire);
   if (iErr != 0)
   {
	   switch (errno)
	   {
		   //"File exists"
		   case EEXIST:
			   break;
		   default:
			   printf("Erreur dans la creation du repertoire %s\n", strerror(errno));
			   return EXIT_FAILURE;
			   break;
	   }
	}



	G_strXTFPingChanHeaderXML[usNumChannel].iNbDatagram = 0;

	// Initialisation des structures de pilotage des fichiers
	// XML et Bin pour le paquet Ping Channel Header
	G_strXTFPingChanHeaderXML[usNumChannel].sNbVarSignal = 24;
	G_strXTFPingChanHeaderXML[usNumChannel].strTabSignalXML= (T_XTFVARXML *)malloc(G_strXTFPingChanHeaderXML[usNumChannel].sNbVarSignal*sizeof(T_XTFVARXML));

	iLen = strlen(cNomDatagram)+1;
	G_strXTFPingChanHeaderXML[usNumChannel].ptr_cNomHeader = malloc(iLen*sizeof(char));
	sprintf(G_strXTFPingChanHeaderXML[usNumChannel].ptr_cNomHeader, "%s", cNomDatagram);

	iLen = strlen(cComments)+1;
	G_strXTFPingChanHeaderXML[usNumChannel].ptr_cCommentsHeader = malloc(iLen*sizeof(char));
	sprintf(G_strXTFPingChanHeaderXML[usNumChannel].ptr_cCommentsHeader, "%s", cComments);

	// Initialisation g�n�rale des tableaux de variables (signaux)
	for (iLoop=0; iLoop<G_strXTFPingChanHeaderXML[usNumChannel].sNbVarSignal; iLoop++) {
	   G_strXTFPingChanHeaderXML[usNumChannel].strTabSignalXML[iLoop].ptr_fpBin = NULL;
	   G_strXTFPingChanHeaderXML[usNumChannel].strTabSignalXML[iLoop].bFlagConstant = TRUE;
	   G_strXTFPingChanHeaderXML[usNumChannel].strTabSignalXML[iLoop].usNbElem = 1;
	   G_strXTFPingChanHeaderXML[usNumChannel].strTabSignalXML[iLoop].fScaleFactor = fTabSFSignal[iLoop];
	   G_strXTFPingChanHeaderXML[usNumChannel].strTabSignalXML[iLoop].fAddOffset = fTabAOSignal[iLoop];
	   sprintf(G_strXTFPingChanHeaderXML[usNumChannel].strTabSignalXML[iLoop].cNom, "%s", cTabNomSignal[iLoop]);
	   sprintf(G_strXTFPingChanHeaderXML[usNumChannel].strTabSignalXML[iLoop].cType, "%s", cTabTypeSignal[iLoop]);
	   sprintf(G_strXTFPingChanHeaderXML[usNumChannel].strTabSignalXML[iLoop].cTag, "%s", "TODO");
	   sprintf(G_strXTFPingChanHeaderXML[usNumChannel].strTabSignalXML[iLoop].cUnit, "%s", "TODO");
	   usTailleVar = util_usNbOctetTypeVar(cTabTypeSignal[iLoop]);
	   G_strXTFPingChanHeaderXML[usNumChannel].strTabSignalXML[iLoop].usSize = usTailleVar;

	   // Ouverture des fichiers.
	   iLen = strlen(G_strXTFPingChanHeaderXML[usNumChannel].strTabSignalXML[iLoop].cNom);
	   // Ajout d'une marge au cas o� certains caract�res soient doubl�s soient doubl�s.
	   ptr_cNomFicPathBin = malloc( (iSizeRep + 1 + iLen + 50)*sizeof(char));
	   sprintf(ptr_cNomFicPathBin, "%s\\%s%s", ptr_cRepertoire, G_strXTFPingChanHeaderXML[usNumChannel].strTabSignalXML[iLoop].cNom, ".bin");
	   G_strXTFPingChanHeaderXML[usNumChannel].strTabSignalXML[iLoop].ptr_fpBin = fopen(ptr_cNomFicPathBin, "w+b");
	   if (!G_strXTFPingChanHeaderXML[usNumChannel].strTabSignalXML[iLoop].ptr_fpBin)
	   {
		   printf("%s -- Error in Signal Creation File  : %s\n", __FILE__, ptr_cNomFicPathBin);
		   return EXIT_FAILURE;
	   }
	   free(ptr_cNomFicPathBin);
	}

	free(ptr_cRepertoire);

	// Initialisations sp�cialis�es des variables
	// Attention : ReservedSpace2 est un tab de variables �l�mentaires.
	G_strXTFPingChanHeaderXML[usNumChannel].strTabSignalXML[23].usNbElem = 4;


	return EXIT_SUCCESS;

} // iInitXMLBinPingChanHeader

//----------------------------------------------------------------
// Fonction :	iWriteBinPingChanHeader
// Objet 	:	Ecriture des signaux dans les fichiers bin.
// Modification : 15/10/08
// Auteur 	: GLU
//----------------------------------------------------------------
int iWriteBinPingChanHeader(int iNbHeader, WORD usNumChannel, XTFPINGCHANHEADER *ChanHeader)
{
	char		*ptr_cRepertoire,
				*ptr_cNomHeader,
				*ptr_cNomFicPathBin;

	int 		iLenVarName,
				iSizeRep,
				iErr,
				iLoop;

	// Pointeur pour avancer sur les variables automatiquement.
	// Prev pour les zones de donn�es pr�c�dentes.
	unsigned char			*ptrChanHeader;

	static unsigned char	*ptrPrevChanHeader[4];

	unsigned short 			usTailleVar,
							usNbElem;

	static void 			*ptr_vValVariable[4],
							*ptr_vPrevVal[4];

	BOOL	   		bFlagConstant,
					bFlagCompare;

	//GLUGLUGLU extern T_XTFRECORDXML *G_strXTFPingChanHeaderXML;
	extern T_XTFRECORDXML G_strXTFPingChanHeaderXML[NB_MAX_CHANNELS];

   ptr_cNomHeader = G_strXTFPingChanHeaderXML[usNumChannel].ptr_cNomHeader;


   // Ecriture syst�matique dans le fichier binaire.
   ptrChanHeader = (unsigned char *)ChanHeader;
   ptr_vValVariable[usNumChannel] = (void *)ptrChanHeader;
   ptr_vPrevVal[usNumChannel] = (void *)ptrPrevChanHeader[usNumChannel];

   // Affectation des variables.
   for (iLoop=0; iLoop<(int)G_strXTFPingChanHeaderXML[usNumChannel].sNbVarSignal; iLoop++) {
	   if (iLoop > 0)
	   {
		   usTailleVar = G_strXTFPingChanHeaderXML[usNumChannel].strTabSignalXML[iLoop-1].usSize;
		   usNbElem = G_strXTFPingChanHeaderXML[usNumChannel].strTabSignalXML[iLoop-1].usNbElem;
	   }
	   else
	   {
		   usTailleVar = 0; // Pour le premier paquet
		   usNbElem = 0;
	   }
	   bFlagConstant = G_strXTFPingChanHeaderXML[usNumChannel].strTabSignalXML[iLoop].bFlagConstant;
	   // Pointage de la variable (par sa taille et son occurrence)
	   ptr_vValVariable[usNumChannel] = ptr_vValVariable[usNumChannel] + usTailleVar*usNbElem;
	   if (iNbHeader > 0)
	   {
		   ptr_vPrevVal[usNumChannel] = ptr_vPrevVal[usNumChannel] + usTailleVar*usNbElem;
		   bFlagCompare = util_bComparePrevAndCurrentVal(G_strXTFPingChanHeaderXML[usNumChannel].strTabSignalXML[iLoop].cType,
											   ptr_vPrevVal[usNumChannel],
											   ptr_vValVariable[usNumChannel],
											   G_strXTFPingChanHeaderXML[usNumChannel].strTabSignalXML[iLoop].usNbElem);
		   bFlagConstant = bFlagConstant && bFlagCompare;
		   G_strXTFPingChanHeaderXML[usNumChannel].strTabSignalXML[iLoop].ptr_vPrevVal = ptr_vPrevVal[usNumChannel];
	   }
	   G_strXTFPingChanHeaderXML[usNumChannel].strTabSignalXML[iLoop].bFlagConstant = bFlagConstant;
	   // Pr�paration de l'�criture de la variable.
	   usTailleVar = G_strXTFPingChanHeaderXML[usNumChannel].strTabSignalXML[iLoop].usSize;
	   usNbElem =  G_strXTFPingChanHeaderXML[usNumChannel].strTabSignalXML[iLoop].usNbElem;
	   fwrite(ptr_vValVariable[usNumChannel], usTailleVar, usNbElem, G_strXTFPingChanHeaderXML[usNumChannel].strTabSignalXML[iLoop].ptr_fpBin);

   }
   ptrPrevChanHeader[usNumChannel] = (unsigned char*)malloc(sizeof(XTFPINGCHANHEADER));
   // Recopie en zone tampon des valeurs courantes.
   memcpy(ptrPrevChanHeader[usNumChannel], ptrChanHeader, sizeof(XTFPINGCHANHEADER));

   return EXIT_SUCCESS;

} // vWriteBinPingChanHeader

//----------------------------------------------------------------
// Fonction :	iCloseBinFilePingChanHeader
// Objet 	:	Fermeture (et effacement) des fichiers des signaux du paquet PingChanHeader.
// Modification : 15/10/08
// Auteur 	: GLU
//----------------------------------------------------------------
int iCloseBinFilePingChanHeader(WORD usNumChannel)
{
	int iLoop,
		iSizeRep,
		iLenVarName,
		iSizeNameHeader;

	char *ptr_cNomFicPathBin;

	// Traitement de fin : fermeture, effacement, �criture XML pour le
	// paquet PingChanHeader
	for (iLoop=0; iLoop<G_strXTFPingChanHeaderXML[usNumChannel].sNbVarSignal; iLoop++)
	{
	   // Fermeture du fichier si pointeur != NULL
	   if (G_strXTFPingChanHeaderXML[usNumChannel].strTabSignalXML[iLoop].ptr_fpBin != NULL)
	   {
		   fclose(G_strXTFPingChanHeaderXML[usNumChannel].strTabSignalXML[iLoop].ptr_fpBin);
	   }
	   // Effacement si la valeur est constante.
	   if (G_strXTFPingChanHeaderXML[usNumChannel].strTabSignalXML[iLoop].bFlagConstant == TRUE)
	   {
		   // Effacement du fichier et enregistrement de la balise Constant
		   // dans l'arbre XML
		   iLenVarName = strlen(G_strXTFPingChanHeaderXML[usNumChannel].strTabSignalXML[iLoop].cNom);
		   iSizeRep = strlen(G_cRepData);
		   iSizeNameHeader = strlen(G_strXTFPingChanHeaderXML[usNumChannel].ptr_cNomHeader);
		   // Taille = Rep + S�parateur + ("PingChanHeader"+"_"+N�Channel+S�parateur) + (Nom du Paquet + "_"+ N�Channel+ S�parateur) + Nom du fichier + ".bin"
		   // Ajout d'une marge au cas o� certains caract�res soient doubl�s soient doubl�s.
		   ptr_cNomFicPathBin = malloc( (iSizeRep + 1 + strlen(G_strXTFSideScanPingXML.ptr_cNomHeader) + 3 + 1 + iSizeNameHeader + 3 + 1 + iLenVarName + 50)*sizeof(char));
		   sprintf(ptr_cNomFicPathBin, "%s\\XTF_%s\\XTF_%s_%d\\%s%s",
										G_cRepData,
										G_strXTFSideScanPingXML.ptr_cNomHeader,
										G_strXTFPingChanHeaderXML[usNumChannel].ptr_cNomHeader,
										usNumChannel,
										G_strXTFPingChanHeaderXML[usNumChannel].strTabSignalXML[iLoop].cNom,
										".bin");
		   if (DEBUG)
		   {
			   printf("Effacement du fichier : %s\n", ptr_cNomFicPathBin);
		   }
		   remove((const char *)ptr_cNomFicPathBin);
		   if (ptr_cNomFicPathBin != NULL) {
			   free(ptr_cNomFicPathBin);
			   ptr_cNomFicPathBin = NULL;
		   }
	   }
	} // Fin de la boucle sur les variables du paquet Bin
	return EXIT_SUCCESS;

} // iCloseBinFilePingChanHeader

#pragma pack() // peut etre remplace par l'option de compil -fpack-struct
