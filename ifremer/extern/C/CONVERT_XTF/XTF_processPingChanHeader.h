/*
 * XTF_processPingChanHeader.h
 *
 *  Created on: 13 oct. 2008
 *      Author: rgallou
 */

#ifndef XTF_PROCESSPINGCHANHEADER_H_
#define XTF_PROCESSPINGCHANHEADER_H_
#pragma pack(1) // peut etre remplace par l'option de compil -fpack-struct pour gcc

#include "XTF_processDatagrams.h"

int iInitXMLBinPingChanHeader(	char *cNomDatagram, char *cComments,
								WORD usNumChannel);

int iWriteBinPingChanHeader(	int iNbHeader,
								WORD usNumChannel,
								XTFPINGCHANHEADER *PingChanHeader);

int iCloseBinFilePingChanHeader(WORD usNumChannel);

#pragma pack() // peut etre remplace par l'option de compil -fpack-struct
#endif /* XTF_PROCESSPingChanHeader_H_ */
