/*
 * XTF_processPingHeader.c
 *
 *  Created on: 13 oct. 2008
 *      Author: rgallou
 */
#pragma pack(1) // peut etre remplace par l'option de compil -fpack-struct pour gcc

#include <sys/types.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "XTF_processPingHeader.h"
#include "XTF_processPingChanHeader.h"
#include "XTF_processImagery.h"
#include "XTF_Datagrams.h"
#include "XTF_Utilities.h"
#include "main_XTF.h"

#define NB_SIGNAL 		77
#define NB_IMAGE 		0
#define NB_IMAGE2 		0
#define NUM_INDEX_TABLE	-1



//----------------------------------------------------------------
// Fonction :	iInitXMLBinPingHeader
// Objet 	:	Initialisation de la structure équivalent au paquet
//				PingHeader pour la manipulation des fichiers bin et XML.
//				Précède les paquets SideScan, Bathy et Snippet.
// Modification : 15/10/08
// Auteur 	: GLU
//----------------------------------------------------------------
int iInitXMLBinPingHeader(	char *cTitleDatagram,
							char *cNomDatagram,
							char *cComments,
							T_XTFRECORDXML *strXTFPingHeaderXML)

{
	int iRet;

	T_DESC_SIGNAL_OR_IMAGE strDescriptionSignal[NB_SIGNAL] = {
			{"MagicNumber",				"WORD",		1.0	,	0.0},
			{"HeaderType",				"BYTE",		1.0	,	0.0},
			{"SubChannelNumber",		"BYTE",		1.0	,	0.0},
			{"NumChanToFollow",			"WORD",		1.0	,	0.0},
			{"Reserved1",				"WORD",		1.0	,	0.0},
			{"NumBytesThisRecord",		"DWORD",	1.0	,	0.0},
			{"Year",					"WORD",		1.0	,	0.0},
			{"Month",					"BYTE",		1.0	,	0.0},
			{"Day",						"BYTE",		1.0	,	0.0},
			{"Hour",					"BYTE",		1.0	,	0.0},
			{"Minute",					"BYTE",		1.0	,	0.0},
			{"Second",					"BYTE",		1.0	,	0.0},
			{"HSeconds",				"BYTE",		1.0	,	0.0},
			{"JulianDay",				"WORD",		1.0	,	0.0},
			{"EventNumber",				"DWORD",	1.0	,	0.0},
			{"PingNumber",				"DWORD",	1.0	,	0.0},
			{"SoundVelocity",			"float",	1.0	,	0.0},
			{"OceanTide",				"float",	1.0	,	0.0},
			{"Reserved2",				"DWORD",	1.0	,	0.0},
			{"ConductivityFreq",		"float",	1.0	,	0.0},
			{"TemperatureFreq",			"float",	1.0	,	0.0},
			{"PressureFreq",			"float",	1.0	,	0.0},
			{"PressureTemp",			"float",	1.0	,	0.0},
			{"Conductivity",			"float",	1.0	,	0.0},
			{"WaterTemperature",		"float",	1.0	,	0.0},
			{"Pressure",				"float",	1.0	,	0.0},
			{"ComputedSoundVelocity",	"float",	1.0	,	0.0},
			{"MagX",					"float",	1.0	,	0.0},
			{"MagY",					"float",	1.0	,	0.0},
			{"MagZ",					"float",	1.0	,	0.0},
			{"AuxVal1",					"float",	1.0	,	0.0},
			{"AuxVal2",					"float",	1.0	,	0.0},
			{"AuxVal3",					"float",	1.0	,	0.0},
			{"AuxVal4",					"float",	1.0	,	0.0},
			{"AuxVal5",					"float",	1.0	,	0.0},
			{"AuxVal6",					"float",	1.0	,	0.0},
			{"SpeedLog",				"float",	1.0	,	0.0},
			{"Turbidity",				"float",	1.0	,	0.0},
			{"ShipSpeed",				"float",	1.0	,	0.0},
			{"ShipGyro",				"float",	1.0	,	0.0},
			{"ShipYcoordinate",			"double",	1.0	,	0.0},
			{"ShipXcoordinate",			"double",	1.0	,	0.0},
			{"ShipAltitude",			"WORD",		1.0	,	0.0},
			{"ShipDepth",				"WORD",		1.0	,	0.0},
			{"FixTimeHour",				"BYTE",		1.0	,	0.0},
			{"FixTimeMinute",			"BYTE",		1.0	,	0.0},
			{"FixTimeSecond",			"BYTE",		1.0	,	0.0},
			{"FixTimeHsecond",			"BYTE",		1.0	,	0.0},
			{"SensorSpeed",				"float",	1.0	,	0.0},
			{"KP",						"float",	1.0	,	0.0},
			{"SensorYCoordinate",		"double",	1.0	,	0.0},
			{"SensorXCoordinate",		"double",	1.0	,	0.0},
			{"SonarStatus",				"WORD",		1.0	,	0.0},
			{"RangeToFish",				"WORD",		1.0	,	0.0},
			{"BearingToFish",			"WORD",		1.0	,	0.0},
			{"CableOut",				"WORD",		1.0	,	0.0},
			{"LayBack",					"float",	1.0	,	0.0},
			{"CableTension",			"float",	1.0	,	0.0},
			{"SensorDepth",				"float",	1.0	,	0.0},
			{"SensorPrimaryAltitude",	"float",	1.0	,	0.0},
			{"SensorAuxAltitude",		"float",	1.0	,	0.0},
			{"SensorPitch",				"float",	1.0	,	0.0},
			{"SensorRoll",				"float",	1.0	,	0.0},
			{"SensorHeading",			"float",	1.0	,	0.0},
			{"Heave",					"float",	1.0	,	0.0},
			{"Yaw",						"float",	1.0	,	0.0},
			{"AttitudeTimeTag",			"DWORD",	1.0	,	0.0},
			{"DOT",						"float",	1.0	,	0.0},
			{"NavFixMilliseconds",		"DWORD",	1.0	,	0.0},
			{"ComputerClockHour",		"BYTE",		1.0	,	0.0},
			{"ComputerClockMinute",		"BYTE",		1.0	,	0.0},
			{"ComputerClockSecond",		"BYTE",		1.0	,	0.0},
			{"ComputerClockHsec",		"BYTE",		1.0	,	0.0},
			{"FishPositionDeltaX",		"short",	1.0	,	0.0},
			{"FishPositionDeltaY",		"short",	1.0	,	0.0},
			{"FishPositionErrorCode",	"BYTE",		1.0	,	0.0},
			{"ReservedSpace2",			"BYTE",		1.0	,	0.0} };


	T_DESC_SIGNAL_OR_IMAGE strDescriptionImage[NB_IMAGE] = {};
	T_DESC_SIGNAL_OR_IMAGE strDescriptionImage2[NB_IMAGE2] = {};

	T_DESC_TAB_SIGANDIMG strDescriptionTab = {NB_SIGNAL,
												NB_IMAGE,
												NUM_INDEX_TABLE,
												strDescriptionSignal,
												strDescriptionImage};

	// Lancement de l'initialisation des structures de signaux : ouvertures des
	// fichiers, copie des structures, ...
	iRet = utilXTF_initXMLRecord(	strDescriptionTab,
									cTitleDatagram,
									cNomDatagram,
									cComments,
									strXTFPingHeaderXML);


	// Initialisations spécialisées des variables
	// Attention : Reserved1 est un tab de variables élémentaires.
	strXTFPingHeaderXML->strTabSignalXML[4].usNbElem = 2;
	// Attention : ReservedSpace2 est un tab de variables élémentaires.
	strXTFPingHeaderXML->strTabSignalXML[76].usNbElem = 11;


	return EXIT_SUCCESS;

} // iInitXMLBinPingHeader


#pragma pack() // peut etre remplace par l'option de compil -fpack-struct
