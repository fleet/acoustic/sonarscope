/*
 * XTF_processPingHeader.h
 *
 *  Created on: 13 oct. 2008
 *      Author: rgallou
 */

#ifndef XTF_PROCESSPINGHEADER_H_
#define XTF_PROCESSPINGHEADER_H_
#pragma pack(1) // peut etre remplace par l'option de compil -fpack-struct pour gcc

#include "XTF_processDatagrams.h"

int iInitXMLBinPingHeader(	char *cTitleDatagram,
							char *cNomDatagram,
							char *cComments,
							T_XTFRECORDXML *strXTFPingHeaderXML);

#pragma pack() // peut etre remplace par l'option de compil -fpack-struct
#endif /* XTF_PROCESSPINGHEADER_H_ */
