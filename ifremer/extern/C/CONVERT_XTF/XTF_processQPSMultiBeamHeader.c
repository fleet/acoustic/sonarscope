/*
 * XTF_processQPSMultiBeamHeader.c
 *
 *  Created on: April 24  2012
 *      Author: rgallou
 */
#pragma pack(1) // peut etre remplace par l'option de compil -fpack-struct pour gcc

#include <sys/types.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "XTF_processQPSMultiBeamHeader.h"
#include "XTF_Utilities.h"
#include "XTF_Datagrams.h"
#include "main_XTF.h"


//T_XTFRECORDXML strXTFRecordXML;

#define NB_SIGNAL 			8
#define NB_IMAGE 			0
#define NB_IMAGE2 			0
#define NUM_INDEX_TABLE		-1

extern T_XTFRECORDXML G_strXTFBathyHeaderXML;

//----------------------------------------------------------------
// Fonction :	iInitXMLBinQPSMultiBeamHeader
// Objet 	:	Initialisation de la structure �quivalent au paquet
//				NotsHeader pour la manipulaiton des fichiers bin et XML.
// Modification : 15/10/08
// Auteur 	: GLU
//----------------------------------------------------------------
int iInitXMLBinQPSMultiBeamHeader(	char *cTitleDatagram,
									char *cNomDatagram,
									char *cComments,
									WORD usNumChannel,
									T_XTFRECORDXML *strXTFRecordXML)
{
	/*
	 * int iRet;

	T_DESC_SIGNAL_OR_IMAGE strDescriptionSignal[NB_SIGNAL] = {
			{"Id",					"int",		1.0,	0.0},
			{"Intensity",			"double",	1.0,	0.0},
			{"Quality",				"int",		1.0,	0.0},
			{"TwoWayTravelTime",	"double",	1.0,	0.0},
			{"DeltaTime",			"double",	1.0,	0.0},
			{"BeamAngle",			"double",	1.0,	0.0},
			{"TiltAngle",			"double",	1.0,	0.0},
			{"Reserved",			"float",	1.0,	0.0}};


	T_DESC_SIGNAL_OR_IMAGE strDescriptionImage[NB_IMAGE] = {};
	T_DESC_SIGNAL_OR_IMAGE strDescriptionImage2[NB_IMAGE2] = {};

	T_DESC_TAB_SIGANDIMG strDescriptionTab = {NB_SIGNAL,
												NB_IMAGE,
												NUM_INDEX_TABLE,
												strDescriptionSignal,
												strDescriptionImage};


	// Lancement de l'initialisation des structures de signaux : ouvertures des
	// fichiers, copie des structures, ...
	iRet = utilXTF_initXMLRecord(	strDescriptionTab,
									cTitleDatagram,
									cNomDatagram,
									cComments,
									strXTFRecordXML);

	*/
	char		*ptr_cRepertoire,
				*ptr_cNomHeader,
				*ptr_cNomBathyHeader,
				*ptr_cNomFicPathBin;

	int 		iLen,
				iSizeRep,
				iLoop,
				iErr,
				iRet;

	unsigned short usTailleVar;

	char cTabNomSignal[NB_SIGNAL][30] = {
			"Id",
			"Intensity",
			"Quality",
			"TwoWayTravelTime",
			"DeltaTime",
			"BeamAngle",
			"TiltAngle",
			"Reserved"};


	char cTabTypeSignal[NB_SIGNAL][20] = {
			"int",
			"double",
			"int",
			"double",
			"double",
			"double",
			"double",
			"float"};

	// ScaleFactor
	float 	fTabSFSignal[NB_SIGNAL] = {
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0};

	// AddOffset
	float 	fTabAOSignal[NB_SIGNAL] = {
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0};

	// Initialisation des structures de pilotage des fichiers
	// XML et Bin pour le paquet Bathymetry
	strXTFRecordXML->sNbVarSignal = NB_SIGNAL;
	strXTFRecordXML->strTabSignalXML= (T_XTFVARXML *)malloc(strXTFRecordXML->sNbVarSignal*sizeof(T_XTFVARXML));

	iLen = strlen(G_strXTFBathyHeaderXML.ptr_cNomHeader)+1;
	strXTFRecordXML->ptr_cNomHeader = malloc(iLen*sizeof(char));
	//GLUGLUGLU G_strXTFBathymetryXML.ptr_cNomHeader = G_strXTFBathyHeaderXML.ptr_cNomHeader;
	sprintf(strXTFRecordXML->ptr_cNomHeader, "%s_%d", cNomDatagram, usNumChannel);

	iLen = strlen(cComments)+1;
	strXTFRecordXML->ptr_cCommentsHeader = malloc(iLen*sizeof(char));
	sprintf(strXTFRecordXML->ptr_cCommentsHeader, "%s", cComments);

	// Affectation du nom du repertoire.
	ptr_cNomHeader 			= cNomDatagram;
	ptr_cNomBathyHeader 	= G_strXTFBathyHeaderXML.ptr_cNomHeader;
	// Taille = R�pertoire + S�p�rateur + ("BathyHeader" +  S�parateur) + "PingChanHeader" + "_" + N� Channel

	iSizeRep = ( strlen(G_cRepData) + strlen(ptr_cNomBathyHeader) + 1 + strlen( ptr_cNomHeader) + 50);
	// Ajout d'une marge au cas o� certains caract�res soient doubl�s soient doubl�s.
	ptr_cRepertoire = malloc(iSizeRep*sizeof(char));
	sprintf(ptr_cRepertoire, "%s\\XTF_%s\\XTF_%s_%d", G_cRepData, ptr_cNomBathyHeader, ptr_cNomHeader, usNumChannel);

	// Cr�ation du r�pertoire
	iErr = mkdir(ptr_cRepertoire);
	if (iErr != 0)
	{
		switch (errno)
		{
			//"File exists"
			case EEXIST:
				break;
			default:
				printf("Erreur dans la creation du repertoire %s\n", strerror(errno));
				return EXIT_FAILURE;
				break;
		}
	}


	if (DEBUG)
		printf("Init Bathymetry -- Adr : %s et ptr_cNomHeader : %s\n", strXTFRecordXML->ptr_cNomHeader,  strXTFRecordXML->ptr_cNomHeader);

	// Initialisation g�n�rale des tableaux de variables (signaux)
	for (iLoop=0; iLoop<strXTFRecordXML->sNbVarSignal; iLoop++)
	{
		strXTFRecordXML->strTabSignalXML[iLoop].bFlagConstant = TRUE;
		strXTFRecordXML->strTabSignalXML[iLoop].usNbElem = 1;
		strXTFRecordXML->strTabSignalXML[iLoop].fScaleFactor = fTabSFSignal[iLoop];
		strXTFRecordXML->strTabSignalXML[iLoop].fAddOffset = fTabAOSignal[iLoop];
		sprintf(strXTFRecordXML->strTabSignalXML[iLoop].cNom, "%s", cTabNomSignal[iLoop]);
		sprintf(strXTFRecordXML->strTabSignalXML[iLoop].cType, "%s", cTabTypeSignal[iLoop]);
		sprintf(strXTFRecordXML->strTabSignalXML[iLoop].cTag, "%s", "TODO");
		sprintf(strXTFRecordXML->strTabSignalXML[iLoop].cUnit, "%s", "TODO");
		usTailleVar = util_usNbOctetTypeVar(cTabTypeSignal[iLoop]);
		strXTFRecordXML->strTabSignalXML[iLoop].usSize = usTailleVar;

		// Ouverture des fichiers.
		iLen = strlen(strXTFRecordXML->strTabSignalXML[iLoop].cNom);
		// Ajout d'une marge au cas o� certains caract�res soient doubl�s soient doubl�s.
		ptr_cNomFicPathBin = malloc( (iSizeRep + 1 + iLen + 50)*sizeof(char));
		sprintf(ptr_cNomFicPathBin, "%s\\%s%s",
									ptr_cRepertoire,
									strXTFRecordXML->strTabSignalXML[iLoop].cNom, ".bin");
		strXTFRecordXML->strTabSignalXML[iLoop].ptr_fpBin = fopen(ptr_cNomFicPathBin, "w+b");
		if (!strXTFRecordXML->strTabSignalXML[iLoop].ptr_fpBin)
		{
			printf("%s -- Error in Signal Creation File  : %s\n", __FILE__, ptr_cNomFicPathBin);
			return EXIT_FAILURE;
		}
		free(ptr_cNomFicPathBin);
	}

	// Initialisations sp�cialis�es des variables
	// Attention : Reserved est un tab de variables �l�mentaires.
	strXTFRecordXML->strTabSignalXML[7].usNbElem = 4;

	return iRet;


}	//iInitXMLBinQPSMultiBeamHeader


//----------------------------------------------------------------
// Fonction :	iWriteBinQPSMultiBeamHeader
// Objet 	:	Ecriture des signaux dans les fichiers bin.
// Modification : 15/10/08
// Auteur 	: GLU
//----------------------------------------------------------------
int iWriteBinQPSMultiBeamHeader(int iNbHeader,
								XTFQPSMULTIBEAMHEADER *QPSMultiBeamHeader,
								T_XTFRECORDXML *strXTFRecordXML)
{
	char		*ptr_cNomHeader;

	int 		iLoop;

	// Pointeur pour avancer sur les variables automatiquement.
	unsigned char			*ptrQPSMultiBeamHeader;
	static unsigned char	*ptrPrevQPSMultiBeamHeader;
	unsigned short 			usTailleVar,
							usNbElem;

	BOOL		bFlagConstant,
				bFlagCompare;

	static void 	*ptr_vValVariable,
					*ptr_vPrevVal;

	static DWORD 	iPrevPingNumber = 0;
	DWORD		 	iPingNumber;

	extern BOOL 	G_bFlagSortIncPing;

	ptr_cNomHeader = strXTFRecordXML->ptr_cNomHeader;

	// Ecriture syst�matique dans le fichier binaire.
	ptrQPSMultiBeamHeader = (unsigned char *)QPSMultiBeamHeader;
	ptr_vValVariable = (void *)ptrQPSMultiBeamHeader;
	ptr_vPrevVal = (void *)ptrPrevQPSMultiBeamHeader;
	// Affectation des variables.
	for (iLoop=0; iLoop<(int)strXTFRecordXML->sNbVarSignal; iLoop++)
	{
			if (iLoop > 0)
			{
				usTailleVar = strXTFRecordXML->strTabSignalXML[iLoop-1].usSize;
				usNbElem = strXTFRecordXML->strTabSignalXML[iLoop-1].usNbElem;
			}
			else
			{
				usTailleVar = 0; // Pour le premier paquet
				usNbElem = 0;
		}
		bFlagConstant = strXTFRecordXML->strTabSignalXML[iLoop].bFlagConstant;
		// Pointage de la variable (par sa taille et son occurrence)
		ptr_vValVariable = ptr_vValVariable + usTailleVar*usNbElem;
		if (iNbHeader > 0)
		{
			ptr_vPrevVal = ptr_vPrevVal + usTailleVar*usNbElem;
			bFlagCompare = util_bComparePrevAndCurrentVal(strXTFRecordXML->strTabSignalXML[iLoop].cType,
													ptr_vPrevVal,
													ptr_vValVariable,
													strXTFRecordXML->strTabSignalXML[iLoop].usNbElem);
			bFlagConstant = bFlagConstant && bFlagCompare;
			strXTFRecordXML->strTabSignalXML[iLoop].ptr_vPrevVal = ptr_vPrevVal;
			// Observation de l'ordre des Ping pour l'attribut Direction.
			if (!strcmp(strXTFRecordXML->strTabSignalXML[iLoop].cNom, "PingNumber"))
			{
				iPrevPingNumber = *((DWORD *)ptr_vPrevVal);
				if (iPrevPingNumber != 0)
				{
					iPingNumber = *((DWORD *)ptr_vValVariable);
					if (iPrevPingNumber > iPingNumber)
					{
						G_bFlagSortIncPing = FALSE;
					}
					else
					{
						G_bFlagSortIncPing = TRUE;
					}
				}
			}
		}
		strXTFRecordXML->strTabSignalXML[iLoop].bFlagConstant = bFlagConstant;
		// Pr�paration de l'�criture de la variable.
		usTailleVar = strXTFRecordXML->strTabSignalXML[iLoop].usSize;
		usNbElem =  strXTFRecordXML->strTabSignalXML[iLoop].usNbElem;
		fwrite(ptr_vValVariable, usTailleVar, usNbElem, strXTFRecordXML->strTabSignalXML[iLoop].ptr_fpBin);

	}
	ptrPrevQPSMultiBeamHeader = malloc(sizeof(XTFQPSMULTIBEAMHEADER));
	// Recopie en zone tampon des valeurs courantes.
	memcpy(ptrPrevQPSMultiBeamHeader, ptrQPSMultiBeamHeader, sizeof(XTFQPSMULTIBEAMHEADER));

	return EXIT_SUCCESS;

} // vWriteBinQPSMultiBeamHeader

void vProcessQPSMultiBeam(XTFQPSMULTIBEAMHEADER *QPSMultiBeamHeader, T_XTFRECORDXML *strXTFRecordXML) {
/****************************************************************************/

	static int iNbHeader = 0;

	iWriteBinQPSMultiBeamHeader(iNbHeader,
					QPSMultiBeamHeader,
					strXTFRecordXML);

	iNbHeader++;

}	//vProcessQPSMultiBeam


#pragma pack() // peut etre remplace par l'option de compil -fpack-struct
