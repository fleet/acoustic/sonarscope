/*
 * XTF_processQPSMultiBeamHeader.h
 *
 *  Created on: 13 oct. 2008
 *      Author: rgallou
 */

#ifndef XTF_PROCESSQPSMULTIBEAMHEADER_H_
#define XTF_PROCESSQPSMULTIBEAMHEADER_H_
#pragma pack(1) // peut etre remplace par l'option de compil -fpack-struct pour gcc

#include "XTF_processDatagrams.h"

int iInitXMLBinQPSMultiBeamHeader(	char *cTitleDatagram,
									char *cNomDatagram,
									char *cComments,
									WORD usNumChannel,
									T_XTFRECORDXML *strXTFRecordXML);

int iWriteBinQPSMultiBeamHeader(int iNbHeader,
								XTFQPSMULTIBEAMHEADER *QPSMultiBeamHeader,
								T_XTFRECORDXML *strXTFRecordXML);

void vProcessQPSMultiBeam(	XTFQPSMULTIBEAMHEADER *QPSMBHeader,
							T_XTFRECORDXML *strXTFRecordXML);

#pragma pack() // peut etre remplace par l'option de compil -fpack-struct
#endif /* XTF_PROCESSQPSMultiBeamHeader_H_ */
