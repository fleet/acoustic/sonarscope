/*
 * XTF_processRawSerialHeader.c
 *
 *  Created on: 13 oct. 2008
 *      Author: rgallou
 */
#pragma pack(1) // peut etre remplace par l'option de compil -fpack-struct pour gcc

#include <sys/types.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "XTF_processRawSerialHeader.h"
#include "XTF_Utilities.h"
#include "XTF_Datagrams.h"
#include "main_XTF.h"

T_XTFRECORDXML G_strXTFRawSerialHeaderXML;

#define NB_SIGNAL 		17
#define NB_IMAGE 		0
#define NB_IMAGE2 		0
#define NUM_INDEX_TABLE	-1


//----------------------------------------------------------------
// Fonction :	iInitXMLBinRawSerialHeader
// Objet 	:	Initialisation de la structure équivalent au paquet
//				RawSerialHeader pour la manipulaiton des fichiers bin et XML.
// Modification : 15/10/08
// Auteur 	: GLU
//----------------------------------------------------------------
int iInitXMLBinRawSerialHeader(	char *cTitleDatagram,
								char *cNomDatagram,
								char *cComments,
								T_XTFRECORDXML *strXTFRecordXML)
{
	int iRet;

	T_DESC_SIGNAL_OR_IMAGE strDescriptionSignal[NB_SIGNAL] = {
			{"MagicNumber",			"WORD",	1.0,	0.0},
			{"HeaderType",			"BYTE",	1.0,	0.0},
			{"SubChannelNumber",	"BYTE",	1.0,	0.0},
			{"NumChanToFollow",		"WORD",	1.0,	0.0},
			{"Reserved",			"WORD",	1.0,	0.0},
			{"NumBytesThisRecord",	"DWORD",1.0,	0.0},
			{"Year",				"WORD",	1.0,	0.0},
			{"Month",				"BYTE",	1.0,	0.0},
			{"Day",					"BYTE",	1.0,	0.0},
			{"Hour",				"BYTE",	1.0,	0.0},
			{"Minute",				"BYTE",	1.0,	0.0},
			{"Second",				"BYTE",	1.0,	0.0},
			{"HSeconds",			"BYTE",	1.0,	0.0},
			{"JulianDay",			"WORD",	1.0,	0.0},
			{"TimeTag",				"DWORD",1.0,	0.0},
			{"StringSize",			"WORD",	1.0,	0.0},
			{"RawAsciiData",		"char",	1.0,	0.0}};


	T_DESC_SIGNAL_OR_IMAGE strDescriptionImage[NB_IMAGE] = {};
	T_DESC_SIGNAL_OR_IMAGE strDescriptionImage2[NB_IMAGE2] = {};

	T_DESC_TAB_SIGANDIMG strDescriptionTab = {NB_SIGNAL,
												NB_IMAGE,
												NUM_INDEX_TABLE,
												strDescriptionSignal,
												strDescriptionImage};


	// Lancement de l'initialisation des structures de signaux : ouvertures des
	// fichiers, copie des structures, ...
	iRet = utilXTF_initXMLRecord(	strDescriptionTab,
									cTitleDatagram,
									cNomDatagram,
									cComments,
									strXTFRecordXML);

	// Initialisations spécialisées des variables
	// Attention : Reserved est un tab de variables élémentaires.
	strXTFRecordXML->strTabSignalXML[4].usNbElem = 2;
	// Attention : ReservedBytes est un tab de variables élémentaires.
	strXTFRecordXML->strTabSignalXML[16].usNbElem = 64-30;

	return iRet;


}	//iInitXMLBinRawSerialHeader


//----------------------------------------------------------------
// Fonction :	iWriteBinRawSerialHeader
// Objet 	:	Ecriture des signaux dans les fichiers bin.
// Modification : 15/10/08
// Auteur 	: GLU
//----------------------------------------------------------------
int iWriteBinRawSerialHeader(int iNbHeader, XTFRAWSERIALHEADER *RawSerialHeader)
{
	char		*ptr_cRepertoire,
				*ptr_cNomHeader,
				*ptr_cNomFicPathBin;

	int 		iLenVarName,
				iSizeRep,
				iErr,
				iLoop;

	// Pointeur pour avancer sur les variables automatiquement.
	unsigned char			*ptrRawSerialHeader;
	static unsigned char	*ptrPrevRawSerialHeader;
	unsigned short 			usTailleVar,
							usNbElem;

	BOOL		bFlagConstant,
				bFlagCompare;

	static void 	*ptr_vValVariable,
					*ptr_vPrevVal;

	static DWORD 	iPrevPingNumber = 0;
	DWORD		 	iPingNumber;

	extern BOOL 	G_bFlagSortIncPing;

   ptr_cNomHeader = G_strXTFRawSerialHeaderXML.ptr_cNomHeader;

   // Ecriture systématique dans le fichier binaire.
   ptrRawSerialHeader = (unsigned char *)RawSerialHeader;
   ptr_vValVariable = (void *)ptrRawSerialHeader;
   ptr_vPrevVal = (void *)ptrPrevRawSerialHeader;
   // Affectation des variables.
   for (iLoop=0; iLoop<(int)G_strXTFRawSerialHeaderXML.sNbVarSignal; iLoop++) {
	   if (iLoop > 0)
	   {
		   usTailleVar = G_strXTFRawSerialHeaderXML.strTabSignalXML[iLoop-1].usSize;
		   usNbElem = G_strXTFRawSerialHeaderXML.strTabSignalXML[iLoop-1].usNbElem;
	   }
	   else
	   {
		   usTailleVar = 0; // Pour le premier paquet
		   usNbElem = 0;
	   }
	   bFlagConstant = G_strXTFRawSerialHeaderXML.strTabSignalXML[iLoop].bFlagConstant;
	   // Pointage de la variable (par sa taille et son occurrence)
	   ptr_vValVariable = ptr_vValVariable + usTailleVar*usNbElem;
	   if (iNbHeader > 0)
	   {
		   ptr_vPrevVal = ptr_vPrevVal + usTailleVar*usNbElem;
		   bFlagCompare = util_bComparePrevAndCurrentVal(G_strXTFRawSerialHeaderXML.strTabSignalXML[iLoop].cType,
											   ptr_vPrevVal,
											   ptr_vValVariable,
											   G_strXTFRawSerialHeaderXML.strTabSignalXML[iLoop].usNbElem);
		   bFlagConstant = bFlagConstant && bFlagCompare;
		   G_strXTFRawSerialHeaderXML.strTabSignalXML[iLoop].ptr_vPrevVal = ptr_vPrevVal;
		   // Observation de l'ordre des Ping pour l'attribut Direction.
		   if (!strcmp(G_strXTFRawSerialHeaderXML.strTabSignalXML[iLoop].cNom, "PingNumber"))
		   {
			   iPrevPingNumber = *((DWORD *)ptr_vPrevVal);
			   if (iPrevPingNumber != 0)
			   {
				   iPingNumber = *((DWORD *)ptr_vValVariable);
				   if (iPrevPingNumber > iPingNumber)
				   {
					   G_bFlagSortIncPing = FALSE;
				   }
				   else
				   {
					   G_bFlagSortIncPing = TRUE;
				   }
			   }
		   }
	   }
	   G_strXTFRawSerialHeaderXML.strTabSignalXML[iLoop].bFlagConstant = bFlagConstant;
	   // Préparation de l'écriture de la variable.
	   usTailleVar = G_strXTFRawSerialHeaderXML.strTabSignalXML[iLoop].usSize;
	   usNbElem =  G_strXTFRawSerialHeaderXML.strTabSignalXML[iLoop].usNbElem;
	   fwrite(ptr_vValVariable, usTailleVar, usNbElem, G_strXTFRawSerialHeaderXML.strTabSignalXML[iLoop].ptr_fpBin);

   }
   ptrPrevRawSerialHeader = malloc(sizeof(XTFRAWSERIALHEADER));
   // Recopie en zone tampon des valeurs courantes.
   memcpy(ptrPrevRawSerialHeader, ptrRawSerialHeader, sizeof(XTFRAWSERIALHEADER));

   return EXIT_SUCCESS;

} // vWriteBinRawSerialHeader

void vProcessRawSerial(XTFRAWSERIALHEADER *SerialHeader) {
/****************************************************************************/
// Raw ASCII serial data as received over a serial port.  Present in file
// only when "SAVERAW" present on Isis' serial template for a specific port.

   static int iNbHeader = 0;

   printf("\nPort %d: %s\n",
      SerialHeader->SerialPort,
      SerialHeader->RawAsciiData);

   iWriteBinRawSerialHeader(iNbHeader, SerialHeader);

  iNbHeader++;

} //vProcessRawSerial


#pragma pack() // peut etre remplace par l'option de compil -fpack-struct
