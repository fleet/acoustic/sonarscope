/*
 * XTF_processRawSerialHeader.h
 *
 *  Created on: 13 oct. 2008
 *      Author: rgallou
 */

#ifndef XTF_PROCESSRAWSERIALHEADER_H_
#define XTF_PROCESSRAWSERIALHEADER_H_
#pragma pack(1) // peut etre remplace par l'option de compil -fpack-struct pour gcc

#include "XTF_processDatagrams.h"

int iInitXMLBinRawSerialHeader(	char *cTitleDatagram,
								char *cNomDatagram,
								char *cComments,
								T_XTFRECORDXML *strXTFRecordXML);

int iWriteBinRawSerialHeader(int iNbHeader, XTFRAWSERIALHEADER *RawSerialHeader);

void vProcessRawSerial(XTFRAWSERIALHEADER *RawSerialHeader);

#pragma pack() // peut etre remplace par l'option de compil -fpack-struct
#endif /* XTF_PROCESSRawSerialHeader_H_ */
