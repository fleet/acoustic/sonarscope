/*
 * XTF_processSNP0Header.c
 *
 *  Created on: 13 oct. 2008
 *      Author: rgallou
 */
#pragma pack(1) // peut etre remplace par l'option de compil -fpack-struct pour gcc

#include <sys/types.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "XTF_processSNP0Header.h"
#include "XTF_processSNP1Header.h"
#include "XTF_processFragSamples.h"
#include "XTF_Datagrams.h"
#include "XTF_Utilities.h"
#include "main_XTF.h"

extern T_XTFRECORDXML G_strXTFPingHeaderSnippetXML;
T_XTFRECORDXML G_strXTFSNP0HeaderXML;
T_XTFRECORDXML *G_strXTFSNP1HeaderXML;
T_XTFRECORDXML *G_strXTFFragSamplesXML;

#define NB_SIGNAL 		32
#define NB_IMAGE 		0
#define NB_IMAGE2 		0
#define NUM_INDEX_TABLE	-1


//----------------------------------------------------------------
// Fonction :	iInitXMLBinSNP0Header
// Objet 	:	Initialisation de la structure �quivalent au paquet
//				NotsHeader pour la manipulaiton des fichiers bin et XML.
// Modification : 15/10/08
// Auteur 	: GLU
//----------------------------------------------------------------
int iInitXMLBinSNP0Header(		char *cTitleDatagram,
								char *cNomDatagram,
								char *cComments,
								T_XTFRECORDXML *strXTFRecordXML)
{
	int iRet;

	T_DESC_SIGNAL_OR_IMAGE strDescriptionSignal[NB_SIGNAL] = {
			{"ID",			"unsigned long",		1.0,	0.0	},
			{"HeaderSize",	"WORD",					1.0,	0.0	},
			{"DataSize",	"WORD",					1.0,	0.0	},
			{"PingNumber",	"unsigned long",		1.0,	0.0	},
			{"Seconds",		"unsigned long",		1.0,	0.0	},
			{"MilliSec",	"unsigned long",		1.0,	0.0	},
			{"Latency",		"WORD",					1.0,	0.0	},
			{"SonarID",		"WORD",					1.0,	0.0	},
			{"SonarModel",	"WORD",					1.0,	0.0	},
			{"Frequency",	"WORD",					1.0,	0.0	},
			{"SSpeed",		"WORD",					1.0,	0.0	},
			{"SampleRate",	"WORD",					1.0,	0.0	},
			{"PingRate",	"WORD",					1.0,	0.0	},
			{"Range",		"WORD",					1.0,	0.0	},
			{"Power",		"WORD",					1.0,	0.0	},
			{"Gain",		"WORD",					1.0,	0.0	},
			{"PulseWidth",	"WORD",					1.0,	0.0	},
			{"Spread",		"WORD",					1.0,	0.0	},
			{"Absorb",		"WORD",					1.0,	0.0	},
			{"Proj",		"WORD",					1.0,	0.0	},
			{"ProjWidth",	"WORD",					1.0,	0.0	},
			{"SpacingNum",	"WORD",					1.0,	0.0	},
			{"SpacingDen",	"WORD",					1.0,	0.0	},
			{"ProjAngle",	"short",				1.0,	0.0	},
			{"MinRange",	"WORD",					1.0,	0.0	},
			{"MaxRange",	"WORD",					1.0,	0.0	},
			{"MinDepth",	"WORD",					1.0,	0.0	},
			{"MaxDepth",	"WORD",					1.0,	0.0	},
			{"Filters",		"WORD",					1.0,	0.0	},
			{"bFlags",		"BYTE",					1.0,	0.0	},
			{"HeadTemp",	"short",				1.0,	0.0	},
			{"BeamCnt",		"WORD",					1.0,	0.0	}};


	T_DESC_SIGNAL_OR_IMAGE strDescriptionImage[NB_IMAGE] = {};
	T_DESC_SIGNAL_OR_IMAGE strDescriptionImage2[NB_IMAGE2] = {};

	T_DESC_TAB_SIGANDIMG strDescriptionTab = {NB_SIGNAL,
												NB_IMAGE,
												NUM_INDEX_TABLE,
												strDescriptionSignal,
												strDescriptionImage};

	// Lancement de l'initialisation des structures de signaux : ouvertures des
	// fichiers, copie des structures, ...
	iRet = utilXTF_initXMLRecord(	strDescriptionTab,
									cTitleDatagram,
									cNomDatagram,
									cComments,
									strXTFRecordXML);

	// Initialisations sp�cialis�es des variables
	// Attention : SonarID est un tab de variables �l�mentaires.
	strXTFRecordXML->strTabSignalXML[7].usNbElem = 2;
	// Attention : bFlags est un tab de variables �l�mentaires.
	strXTFRecordXML->strTabSignalXML[29].usNbElem = 2;

	return iRet;


}	//iInitXMLBinSNP0Header



//----------------------------------------------------------------
// Fonction :	iWriteBinSNP0Header
// Objet 	:	Ecriture des signaux dans les fichiers bin.
// Modification : 15/10/08
// Auteur 	: GLU
//----------------------------------------------------------------
int iWriteBinPingHeaderSnippet(int iNbHeader, XTFPINGHEADER *PingHeader)
{
	char		*ptr_cRepertoire,
				*ptr_cNomHeader,
				*ptr_cNomFicPathBin;

	int 		iLenVarName,
				iSizeRep,
				iErr,
				iLoop;

	// Pointeur pour avancer sur les variables automatiquement.
	unsigned char			*ptrPingHeader;
	static unsigned char	*ptrPrevPingHeader;
	unsigned short 			usTailleVar,
							usNbElem;

	BOOL		bFlagConstant,
				bFlagCompare;

	static void 	*ptr_vValVariable,
					*ptr_vPrevVal;

	static DWORD 	iPrevPingNumber = 0;
	DWORD		 	iPingNumber;


   // Ecriture syst�matique dans le fichier binaire.
   ptrPingHeader = (unsigned char *)PingHeader;
   ptr_vValVariable = (void *)ptrPingHeader;
   ptr_vPrevVal = (void *)ptrPrevPingHeader;
   // Affectation des variables.
   for (iLoop=0; iLoop<(int)G_strXTFPingHeaderSnippetXML.sNbVarSignal; iLoop++) {
	   if (iLoop > 0)
	   {
		   usTailleVar = G_strXTFPingHeaderSnippetXML.strTabSignalXML[iLoop-1].usSize;
		   usNbElem = G_strXTFPingHeaderSnippetXML.strTabSignalXML[iLoop-1].usNbElem;
	   }
	   else
	   {
		   usTailleVar = 0; // Pour le premier paquet
		   usNbElem = 0;
	   }
	   bFlagConstant = G_strXTFPingHeaderSnippetXML.strTabSignalXML[iLoop].bFlagConstant;
	   // Pointage de la variable (par sa taille et son occurrence)
	   ptr_vValVariable = ptr_vValVariable + usTailleVar*usNbElem;
	   if (iNbHeader > 0)
	   {
		   ptr_vPrevVal = ptr_vPrevVal + usTailleVar*usNbElem;
		   bFlagCompare = util_bComparePrevAndCurrentVal(G_strXTFPingHeaderSnippetXML.strTabSignalXML[iLoop].cType,
											   ptr_vPrevVal,
											   ptr_vValVariable,
											   G_strXTFPingHeaderSnippetXML.strTabSignalXML[iLoop].usNbElem);
		   bFlagConstant = bFlagConstant && bFlagCompare;
		   G_strXTFPingHeaderSnippetXML.strTabSignalXML[iLoop].ptr_vPrevVal = ptr_vPrevVal;
		   // Observation de l'ordre des Ping pour l'attribut Direction.
		   if (!strcmp(G_strXTFPingHeaderSnippetXML.strTabSignalXML[iLoop].cNom, "PingNumber"))
		   {
			   iPrevPingNumber = *((DWORD *)ptr_vPrevVal);
			   if (iPrevPingNumber != 0)
			   {
				   iPingNumber = *((DWORD *)ptr_vValVariable);
				   if (iPrevPingNumber > iPingNumber)
				   {
					   G_bFlagSortIncPing = FALSE;
				   }
				   else
				   {
					   G_bFlagSortIncPing = TRUE;
				   }
			   }
		   }
	   }
	   G_strXTFPingHeaderSnippetXML.strTabSignalXML[iLoop].bFlagConstant = bFlagConstant;
	   // Pr�paration de l'�criture de la variable.
	   usTailleVar = G_strXTFPingHeaderSnippetXML.strTabSignalXML[iLoop].usSize;
	   usNbElem =  G_strXTFPingHeaderSnippetXML.strTabSignalXML[iLoop].usNbElem;
	   fwrite(ptr_vValVariable, usTailleVar, usNbElem, G_strXTFPingHeaderSnippetXML.strTabSignalXML[iLoop].ptr_fpBin);

   }
   ptrPrevPingHeader = malloc(sizeof(XTFPINGHEADER));
   // Recopie en zone tampon des valeurs courantes.
   memcpy(ptrPrevPingHeader, ptrPingHeader, sizeof(XTFPINGHEADER));

   return EXIT_SUCCESS;

} // iWriteBinPingHeaderSnippet

//----------------------------------------------------------------
// Fonction :	iWriteBinSNP0Header
// Objet 	:	Ecriture des signaux dans les fichiers bin.
// Modification : 15/10/08
// Auteur 	: GLU
//----------------------------------------------------------------
int iWriteBinSNP0Header(int iNbHeader, XTFBATHYSNP0SNIPPET *SNP0Header)
{
	char		*ptr_cRepertoire,
				*ptr_cNomHeader,
				*ptr_cNomFicPathBin;

	int 		iLenVarName,
				iSizeRep,
				iErr,
				iLoop;

	// Pointeur pour avancer sur les variables automatiquement.
	unsigned char			*ptrSNP0Header;
	static unsigned char	*ptrPrevSNP0Header;
	WORD 			usTailleVar,
					usNbElem;

	BOOL		bFlagConstant,
				bFlagCompare;

	static void 	*ptr_vValVariable,
					*ptr_vPrevVal;

	static DWORD 	iPrevPingNumber = 0;
	DWORD		 	iPingNumber;

   ptr_cNomHeader = G_strXTFSNP0HeaderXML.ptr_cNomHeader;

   // Ecriture syst�matique dans le fichier binaire.
   ptrSNP0Header = (unsigned char *)SNP0Header;
   ptr_vValVariable = (void *)ptrSNP0Header;
   ptr_vPrevVal = (void *)ptrPrevSNP0Header;
   // Affectation des variables.
   for (iLoop=0; iLoop<(int)G_strXTFSNP0HeaderXML.sNbVarSignal; iLoop++) {
	   if (iLoop > 0)
	   {
		   usTailleVar = G_strXTFSNP0HeaderXML.strTabSignalXML[iLoop-1].usSize;
		   usNbElem = G_strXTFSNP0HeaderXML.strTabSignalXML[iLoop-1].usNbElem;
	   }
	   else
	   {
		   usTailleVar = 0; // Pour le premier paquet
		   usNbElem = 0;
	   }
	   bFlagConstant = G_strXTFSNP0HeaderXML.strTabSignalXML[iLoop].bFlagConstant;
	   // Pointage de la variable (par sa taille et son occurrence)
	   ptr_vValVariable = ptr_vValVariable + usTailleVar*usNbElem;
	   if (iNbHeader > 0)
	   {
		   ptr_vPrevVal = ptr_vPrevVal + usTailleVar*usNbElem;
		   bFlagCompare = util_bComparePrevAndCurrentVal(G_strXTFSNP0HeaderXML.strTabSignalXML[iLoop].cType,
											   ptr_vPrevVal,
											   ptr_vValVariable,
											   G_strXTFSNP0HeaderXML.strTabSignalXML[iLoop].usNbElem);
		   bFlagConstant = bFlagConstant && bFlagCompare;
		   G_strXTFSNP0HeaderXML.strTabSignalXML[iLoop].ptr_vPrevVal = ptr_vPrevVal;
		   // Observation de l'ordre des Ping pour l'attribut Direction.
		   if (!strcmp(G_strXTFSNP0HeaderXML.strTabSignalXML[iLoop].cNom, "PingNumber"))
		   {
			   iPrevPingNumber = *((DWORD *)ptr_vPrevVal);
			   if (iPrevPingNumber != 0)
			   {
				   iPingNumber = *((DWORD *)ptr_vValVariable);
				   if (iPrevPingNumber > iPingNumber)
				   {
					   G_bFlagSortIncPing = FALSE;
				   }
				   else
				   {
					   G_bFlagSortIncPing = TRUE;
				   }
			   }
		   }
	   }
	   G_strXTFSNP0HeaderXML.strTabSignalXML[iLoop].bFlagConstant = bFlagConstant;
	   // Pr�paration de l'�criture de la variable.
	   usTailleVar = G_strXTFSNP0HeaderXML.strTabSignalXML[iLoop].usSize;
	   usNbElem =  G_strXTFSNP0HeaderXML.strTabSignalXML[iLoop].usNbElem;
	   fwrite(ptr_vValVariable, usTailleVar, usNbElem, G_strXTFSNP0HeaderXML.strTabSignalXML[iLoop].ptr_fpBin);

   }
   ptrPrevSNP0Header = malloc(sizeof(XTFBATHYSNP0SNIPPET));
   // Recopie en zone tampon des valeurs courantes.
   memcpy(ptrPrevSNP0Header, ptrSNP0Header, sizeof(XTFBATHYSNP0SNIPPET));

   return EXIT_SUCCESS;

} // vWriteBinSNP0


//----------------------------------------------------------------
// Fonction :	vProcessSNP0
// Objet 	:	Traitement des signaux Snippets pr�c�d�s d'un paquet
//				Ping Header
// Modification : 28/10/08
// Auteur 	: GLU
//----------------------------------------------------------------
void vProcessSNP0(XTFPINGHEADER *PingHeader)
{
WORD usBeam;
unsigned char *Ptr = (unsigned char *)PingHeader;
XTFBATHYSNP0SNIPPET *SNP0Header;

   static int iNbHeader = 0;

   // For backwards-compatibility.  The samples per channel used to
   // be stored in the file header.
   // Traitement pour �criture dans le fichier Binaire
   iWriteBinPingHeaderSnippet(iNbHeader, PingHeader);

   // skip past the ping header
   Ptr += sizeof(XTFPINGHEADER);

   SNP0Header = (XTFBATHYSNP0SNIPPET*)Ptr;
   // Traitement pour �criture dans le fichier Binaire
   iWriteBinSNP0Header(iNbHeader, SNP0Header);

	// Allocation de la taille m�moire selon le nombre de faisceaux d'un ping
	if (iNbHeader == 0)
	{
		G_strXTFSNP1HeaderXML = malloc(SNP0Header->BeamCnt*sizeof(T_XTFRECORDXML));
		G_strXTFFragSamplesXML = malloc(SNP0Header->BeamCnt*sizeof(T_XTFRECORDXML));
	}

   for (usBeam=0; usBeam<SNP0Header->BeamCnt; usBeam++) {

      XTFBATHYSNP1SNIPPET *SNP1Header = NULL;
      unsigned char *FragSamples;
      WORD BytesPerSample;
      DWORD BytesThisBeam;
      DWORD SamplesPerBeam;

	  if (iNbHeader == 0)
	  {
		// Initialisation des valeurs du paquet SNP1
		iInitXMLBinSNP1Header("SNP1Header", "TODO", usBeam);
		// Initialisation des valeurs des donn�es de FragSamples
		iInitXMLBinFragSamples("FragSamples", "TODO", usBeam);
      }

	  G_strXTFSNP0HeaderXML.iNbDatagram++;

      // Traitement pour �criture dans les fichiers Binaires les paquets
      // Channel info
      // skip past the channel header
      Ptr += sizeof(XTFBATHYSNP0SNIPPET);
	  // point to the channel header
      //
      SNP1Header = (XTFBATHYSNP1SNIPPET *) Ptr;
      // Traitement pour �criture dans les fichiers Binaires les paquets
      // Channel info
      G_strXTFSNP1HeaderXML[usBeam].iNbDatagram++;
      iWriteBinSNP1Header(iNbHeader, usBeam, SNP1Header);
      // skip past the channel header
      Ptr += sizeof(XTFBATHYSNP1SNIPPET);

      // Compute the number of bytes of FragSamples for this channel
      //
      BytesPerSample = SNP1Header->DataSize;
      SamplesPerBeam = SNP1Header->FragSamples;

      BytesThisBeam = BytesPerSample * SamplesPerBeam;
      // Point to the FragSamples.  If BytesPerSample is 2, then
      // FragSamples should be a pointer to a signed 16-bit value.
      // If BytesPerSample is 1, then FragSamples should point to
      // a unsigned 8-bit value.
      FragSamples = Ptr;
      G_strXTFFragSamplesXML[usBeam].iNbDatagram++;
      iWriteBinFragSamples(iNbHeader, usBeam, FragSamples,
      BytesPerSample, SamplesPerBeam);

      // skip past the FragSamples;
      Ptr += BytesThisBeam;
   }

   iNbHeader++;

} //vProcessSNP0

#pragma pack() // peut etre remplace par l'option de compil -fpack-struct
