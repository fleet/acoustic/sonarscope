/*
 * XTF_processSNP0Header.h
 *
 *  Created on: 13 oct. 2008
 *      Author: rgallou
 */

#ifndef XTF_PROCESSSNP0HEADER_H_
#define XTF_PROCESSSNP0HEADER_H_
#pragma pack(1) // peut etre remplace par l'option de compil -fpack-struct pour gcc

#include "XTF_processDatagrams.h"

int 	iInitXMLBinSNP0Header(	char *cTitleDatagram,
								char *cNomDatagram,
								char *cComments,
								T_XTFRECORDXML *strXTFRecordXML);

int 	iWriteBinPingHeaderSnippet(	int iNbHeader,
									XTFPINGHEADER *PingHeader);

int 	iWriteBinSNP0Header(	int iNbHeader,
								XTFBATHYSNP0SNIPPET *SNP0Header);

void 	vProcessSNP0(XTFPINGHEADER *PingHeader);


#pragma pack() // peut etre remplace par l'option de compil -fpack-struct
#endif /* XTF_PROCESSSNP0HEADER_H_ */
