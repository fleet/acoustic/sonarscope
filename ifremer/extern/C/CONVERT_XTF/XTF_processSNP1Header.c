/*
 * XTF_processSNP1Header.c
 *
 *  Created on: 13 oct. 2008
 *      Author: rgallou
 */
#pragma pack(1) // peut etre remplace par l'option de compil -fpack-struct pour gcc

#include <sys/types.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "XTF_processSNP1Header.h"
#include "XTF_Utilities.h"
#include "main_XTF.h"

extern T_XTFRECORDXML *G_strXTFSNP1HeaderXML;
extern T_XTFRECORDXML G_strXTFPingHeaderSnippetXML;

//----------------------------------------------------------------
// Fonction :	iInitXMLBinSNP1Header
// Objet 	:	Initialisation de la structure �quivalent au paquet
//				SNP1Header pour la manipulation des fichiers bin et XML.
// Modification : 15/10/08
// Auteur 	: GLU
//----------------------------------------------------------------
int iInitXMLBinSNP1Header(char *cNomDatagram, char *cComments, WORD usNumBeam)
{
	int iLen,
		iLoop;

	unsigned short usTailleVar;

	char cTabNomSignal[10][30] = {
			"ID",
			"HeaderSize",
			"DataSize",
			"PingNumber",
			"Beam",
			"SnipSamples",
			"GainStart",
			"GainEnd",
			"FragOffset",
			"FragSamples"};


	char cTabTypeSignal[10][20] = {
			"unsigned long",
			"WORD",
			"WORD",
			"unsigned long",
			"WORD",
			"WORD",
			"WORD",
			"WORD",
			"WORD",
			"WORD"};

	// ScaleFactor
	float 	fTabSFSignal[10] = {
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0,
			1.0};

	// AddOffset
	float 	fTabAOSignal[10] = {
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0,
			0.0};

	char cTabTagVariable[10][20]; 		//TODO
	char cTabUnitVariable[10][20]; 		//TODO

	G_strXTFSNP1HeaderXML[usNumBeam].iNbDatagram = 0;

	// Initialisation des structures de pilotage des fichiers
	// XML et Bin pour le paquet Ping Channel Header
	G_strXTFSNP1HeaderXML[usNumBeam].sNbVarSignal = 10;
	G_strXTFSNP1HeaderXML[usNumBeam].strTabSignalXML= (T_XTFVARXML *)malloc(G_strXTFSNP1HeaderXML[usNumBeam].sNbVarSignal*sizeof(T_XTFVARXML));

	iLen = strlen(cNomDatagram)+1;
	G_strXTFSNP1HeaderXML[usNumBeam].ptr_cNomHeader = malloc(iLen*sizeof(char));
	sprintf(G_strXTFSNP1HeaderXML[usNumBeam].ptr_cNomHeader, "%s", cNomDatagram);

	iLen = strlen(cComments)+1;
	G_strXTFSNP1HeaderXML[usNumBeam].ptr_cCommentsHeader = malloc(iLen*sizeof(char));
	sprintf(G_strXTFSNP1HeaderXML[usNumBeam].ptr_cCommentsHeader, "%s", cComments);

	// Initialisation g�n�rale des tableaux de variables (signaux)
	for (iLoop=0; iLoop<G_strXTFSNP1HeaderXML[usNumBeam].sNbVarSignal; iLoop++) {
	   G_strXTFSNP1HeaderXML[usNumBeam].strTabSignalXML[iLoop].ptr_fpBin = NULL;
	   G_strXTFSNP1HeaderXML[usNumBeam].strTabSignalXML[iLoop].bFlagConstant = TRUE;
	   G_strXTFSNP1HeaderXML[usNumBeam].strTabSignalXML[iLoop].usNbElem = 1;
	   G_strXTFSNP1HeaderXML[usNumBeam].strTabSignalXML[iLoop].fScaleFactor = fTabSFSignal[iLoop];
	   G_strXTFSNP1HeaderXML[usNumBeam].strTabSignalXML[iLoop].fAddOffset = fTabAOSignal[iLoop];
	   sprintf(G_strXTFSNP1HeaderXML[usNumBeam].strTabSignalXML[iLoop].cNom, "%s", cTabNomSignal[iLoop]);
	   sprintf(G_strXTFSNP1HeaderXML[usNumBeam].strTabSignalXML[iLoop].cType, "%s", cTabTypeSignal[iLoop]);
	   sprintf(G_strXTFSNP1HeaderXML[usNumBeam].strTabSignalXML[iLoop].cTag, "%s", "TODO");
	   sprintf(G_strXTFSNP1HeaderXML[usNumBeam].strTabSignalXML[iLoop].cUnit, "%s", "TODO");
	   usTailleVar = util_usNbOctetTypeVar(cTabTypeSignal[iLoop]);
	   G_strXTFSNP1HeaderXML[usNumBeam].strTabSignalXML[iLoop].usSize = usTailleVar;
	}


	return EXIT_SUCCESS;

} // iInitXMLBinSNP1Header

//----------------------------------------------------------------
// Fonction :	iWriteBinSNP1Header
// Objet 	:	Ecriture des signaux dans les fichiers bin.
// Modification : 15/10/08
// Auteur 	: GLU
//----------------------------------------------------------------
int iWriteBinSNP1Header(int iNbHeader, WORD usNumBeam, XTFBATHYSNP1SNIPPET *ChanHeader)
{
	char		*ptr_cRepertoire,
				*ptr_cNomHeader,
				*ptr_cNomFicPathBin;

	int 		iLenVarName,
				iSizeRep,
				iErr,
				iLoop;

	// Pointeur pour avancer sur les variables automatiquement.
	// Prev pour les zones de donn�es pr�c�dentes.
	unsigned char			*ptrChanHeader;

	static unsigned char	*ptrPrevChanHeader[4];

	unsigned short 			usTailleVar,
							usNbElem;

	static void 			*ptr_vValVariable[4],
							*ptr_vPrevVal[4];

	BOOL	   		bFlagConstant,
					bFlagCompare;

	extern T_XTFRECORDXML *G_strXTFSNP1HeaderXML;

   ptr_cNomHeader = G_strXTFSNP1HeaderXML[usNumBeam].ptr_cNomHeader;

   if (iNbHeader == 0 )
   {
	   // Affectation du nom du repertoire.
	   // Taille = R�pertoire + S�p�rateur + ("PingHeader" +  S�parateur) + "ChanInfo" + "_" + N� Channel
	   iSizeRep = ( strlen(G_cRepData) + 11 + strlen(ptr_cNomHeader) + 50);
	   // Ajout d'une marge au cas o� certains caract�res soient doubl�s soient doubl�s.
	   ptr_cRepertoire = malloc(iSizeRep*sizeof(char));
	   sprintf(ptr_cRepertoire, "%s\\XTF_%s\\%s_%d", G_cRepData, G_strXTFPingHeaderSnippetXML.ptr_cNomHeader, ptr_cNomHeader, usNumBeam);
	   if (DEBUG)
	   	   printf("-- Creation du repertoire : %s\n", ptr_cRepertoire);

	   // Cr�ation du r�pertoire
	   iErr = mkdir(ptr_cRepertoire);
	   if (iErr != 0)
	   {
		   switch (errno)
		   {
			   //"File exists"
			   case EEXIST:
				   break;
			   default:
				   printf("Erreur dans la creation du repertoire %s\n", strerror(errno));
				   return EXIT_FAILURE;
				   break;
		   }
		}

	   // Affectation des variables.
	   for (iLoop=0; iLoop<(int)G_strXTFSNP1HeaderXML[usNumBeam].sNbVarSignal; iLoop++) {
		   iLenVarName = strlen(G_strXTFSNP1HeaderXML[usNumBeam].strTabSignalXML[iLoop].cNom);
		   ptr_cNomFicPathBin = malloc( (iSizeRep + 1 + iLenVarName + 50)*sizeof(char));
		   sprintf(ptr_cNomFicPathBin, "%s\\%s%s", ptr_cRepertoire, G_strXTFSNP1HeaderXML[usNumBeam].strTabSignalXML[iLoop].cNom, ".bin");
		   G_strXTFSNP1HeaderXML[usNumBeam].strTabSignalXML[iLoop].ptr_fpBin = fopen(ptr_cNomFicPathBin, "w+b");
		   if (!G_strXTFSNP1HeaderXML[usNumBeam].strTabSignalXML[iLoop].ptr_fpBin)
		   {
			   printf("%s -- Error in Signal Creation File  : %s\n", __FILE__, ptr_cNomFicPathBin);
			   return EXIT_FAILURE;
		   }
		   free(ptr_cNomFicPathBin);
	   }

#ifndef DEBUG
	   free(ptr_cRepertoire);
#endif
   } // iNbHeader ==0

   // Ecriture syst�matique dans le fichier binaire.
   ptrChanHeader = (unsigned char *)ChanHeader;
   ptr_vValVariable[usNumBeam] = (void *)ptrChanHeader;
   ptr_vPrevVal[usNumBeam] = (void *)ptrPrevChanHeader[usNumBeam];

   // Affectation des variables.
   for (iLoop=0; iLoop<(int)G_strXTFSNP1HeaderXML[usNumBeam].sNbVarSignal; iLoop++) {
	   if (iLoop > 0)
	   {
		   usTailleVar = G_strXTFSNP1HeaderXML[usNumBeam].strTabSignalXML[iLoop-1].usSize;
		   usNbElem = G_strXTFSNP1HeaderXML[usNumBeam].strTabSignalXML[iLoop-1].usNbElem;
	   }
	   else
	   {
		   usTailleVar = 0; // Pour le premier paquet
		   usNbElem = 0;
	   }
	   bFlagConstant = G_strXTFSNP1HeaderXML[usNumBeam].strTabSignalXML[iLoop].bFlagConstant;
	   // Pointage de la variable (par sa taille et son occurrence)
	   ptr_vValVariable[usNumBeam] = ptr_vValVariable[usNumBeam] + usTailleVar*usNbElem;
	   if (iNbHeader > 0)
	   {
		   ptr_vPrevVal[usNumBeam] = ptr_vPrevVal[usNumBeam] + usTailleVar*usNbElem;
		   bFlagCompare = util_bComparePrevAndCurrentVal(G_strXTFSNP1HeaderXML[usNumBeam].strTabSignalXML[iLoop].cType,
											   ptr_vPrevVal[usNumBeam],
											   ptr_vValVariable[usNumBeam],
											   G_strXTFSNP1HeaderXML[usNumBeam].strTabSignalXML[iLoop].usNbElem);
		   bFlagConstant = bFlagConstant && bFlagCompare;
		   G_strXTFSNP1HeaderXML[usNumBeam].strTabSignalXML[iLoop].ptr_vPrevVal = ptr_vPrevVal[usNumBeam];
	   }
	   else
	   {
		   G_strXTFSNP1HeaderXML[usNumBeam].strTabSignalXML[iLoop].ptr_vPrevVal = ptr_vValVariable[usNumBeam];
	   }
	   G_strXTFSNP1HeaderXML[usNumBeam].strTabSignalXML[iLoop].bFlagConstant = bFlagConstant;
	   // Pr�paration de l'�criture de la variable.
	   usTailleVar = G_strXTFSNP1HeaderXML[usNumBeam].strTabSignalXML[iLoop].usSize;
	   usNbElem =  G_strXTFSNP1HeaderXML[usNumBeam].strTabSignalXML[iLoop].usNbElem;
	   fwrite(ptr_vValVariable[usNumBeam], usTailleVar, usNbElem, G_strXTFSNP1HeaderXML[usNumBeam].strTabSignalXML[iLoop].ptr_fpBin);

   }
   ptrPrevChanHeader[usNumBeam] = (unsigned char*)malloc(sizeof(XTFBATHYSNP1SNIPPET));
   // Recopie en zone tampon des valeurs courantes.
   memcpy(ptrPrevChanHeader[usNumBeam], ptrChanHeader, sizeof(XTFBATHYSNP1SNIPPET));

   return EXIT_SUCCESS;

} // vWriteBinSNP1Header


//----------------------------------------------------------------
// Fonction :	iCloseBinFileSNP1Header
// Objet 	:	Fermeture (et effacement) des fichiers des signaux du paquet SNP1Header.
// Modification : 15/10/08
// Auteur 	: GLU
//----------------------------------------------------------------
int iCloseBinFileSNP1Header(WORD usNumBeam)
{
	int iLoop,
		iSizeRep,
		iLenVarName,
		iSizeNameHeader;

	char *ptr_cNomFicPathBin;

	// Traitement de fin : fermeture, effacement, �criture XML pour le
	// paquet SNP1Header
	for (iLoop=0; iLoop<G_strXTFSNP1HeaderXML[usNumBeam].sNbVarSignal; iLoop++)
	{
	   // Fermeture du fichier si pointeur != NULL
	   if (G_strXTFSNP1HeaderXML[usNumBeam].strTabSignalXML[iLoop].ptr_fpBin != NULL)
	   {
		   fclose(G_strXTFSNP1HeaderXML[usNumBeam].strTabSignalXML[iLoop].ptr_fpBin);
	   }
	   // Effacement si la valeur est constante.
	   if (G_strXTFSNP1HeaderXML[usNumBeam].strTabSignalXML[iLoop].bFlagConstant == TRUE)
	   {
		   // Effacement du fichier et enregistrement de la balise Constant
		   // dans l'arbre XML
		   iLenVarName = strlen(G_strXTFSNP1HeaderXML[usNumBeam].strTabSignalXML[iLoop].cNom);
		   iSizeRep = strlen(G_cRepData);
		   iSizeNameHeader = strlen(G_strXTFSNP1HeaderXML[usNumBeam].ptr_cNomHeader);
		   // Taille = Rep + S�parateur + ("SNP1Header"+S�parateur) + Nom du Paquet + Sp�rateur + Nom du fichier
		   // Ajout d'une marge au cas o� certains caract�res soient doubl�s soient doubl�s.
		   ptr_cNomFicPathBin = malloc( (iSizeRep + 1 + strlen(G_strXTFPingHeaderSnippetXML.ptr_cNomHeader) + 1 + iSizeNameHeader + 1 + iLenVarName + 50)*sizeof(char));
		   sprintf(ptr_cNomFicPathBin, "%s\\XTF_%s\\XTF_%s\\%s%s", G_cRepData, G_strXTFPingHeaderSnippetXML.ptr_cNomHeader, G_strXTFSNP1HeaderXML[usNumBeam].ptr_cNomHeader,
								   G_strXTFSNP1HeaderXML[usNumBeam].strTabSignalXML[iLoop].cNom, ".bin");
		   if (DEBUG)
		   {
			   printf("Effacement du fichier : %s\n", ptr_cNomFicPathBin);
		   }
		   remove((const char *)ptr_cNomFicPathBin);
		   if (ptr_cNomFicPathBin != NULL) {
			   free(ptr_cNomFicPathBin);
			   ptr_cNomFicPathBin = NULL;
		   }
	   }
	} // Fin de la boucle sur les variables du paquet Bin
	return EXIT_SUCCESS;

} // iCloseBinFileSNP1Header

#pragma pack() // peut etre remplace par l'option de compil -fpack-struct
