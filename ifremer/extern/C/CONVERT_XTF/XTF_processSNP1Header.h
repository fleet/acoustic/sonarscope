/*
 * XTF_processPingChanHeader.h
 *
 *  Created on: 13 oct. 2008
 *      Author: rgallou
 */

#ifndef XTF_PROCESSSNP1HEADER_H_
#define XTF_PROCESSSNP1HEADER_H_
#pragma pack(1) // peut etre remplace par l'option de compil -fpack-struct pour gcc

#include "XTF_processDatagrams.h"

int iInitXMLBinSNP1Header(	char *cNomDatagram,
							char *cComments,
							WORD usNumBeam);

int iWriteBinSNP1Header(int iNbHeader,
						WORD usNumBeam,
						XTFBATHYSNP1SNIPPET *SNP1Header);

int iCloseBinFileSNP1Header(WORD usNumBeam);

#pragma pack() // peut etre remplace par l'option de compil -fpack-struct
#endif /* XTF_PROCESSSNP1HEADER_H_ */
