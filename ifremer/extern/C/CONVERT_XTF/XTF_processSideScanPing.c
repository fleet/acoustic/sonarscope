/*
 * XTF_processPingHeader.c
 *
 *  Created on: 13 oct. 2008
 *      Author: rgallou
 */
#pragma pack(1) // peut etre remplace par l'option de compil -fpack-struct pour gcc

#include <sys/types.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "XTF_processPingHeader.h"
#include "XTF_processPingChanHeader.h"
#include "XTF_processImagery.h"
#include "XTF_Datagrams.h"
#include "XTF_Utilities.h"
#include "main_XTF.h"


extern T_XTFRECORDXML G_strXTFSideScanPingXML;
//GLUGLUGLU T_XTFRECORDXML *G_strXTFPingChanHeaderXML;
T_XTFRECORDXML G_strXTFPingChanHeaderXML[NB_MAX_CHANNELS];
T_XTFRECORDXML *G_strXTFImageryXML;


//----------------------------------------------------------------
// Fonction :	iWriteBinSideScanPing
// Objet 	:	Ecriture des signaux dans les fichiers bin.
// Modification : 15/10/08
// Auteur 	: GLU
//----------------------------------------------------------------
int iWriteBinSideScanPing(int iNbHeader, XTFPINGHEADER *PingHeader)
{
	char		*ptr_cRepertoire,
				*ptr_cNomHeader,
				*ptr_cNomFicPathBin;

	int 		iLenVarName,
				iSizeRep,
				iErr,
				iLoop;

	// Pointeur pour avancer sur les variables automatiquement.
	unsigned char			*ptrPingHeader;
	static unsigned char	*ptrPrevPingHeader;
	unsigned short 			usTailleVar,
							usNbElem;

	BOOL		bFlagConstant,
				bFlagCompare;

	static void 	*ptr_vValVariable,
					*ptr_vPrevVal;

	static DWORD 	iPrevPingNumber = 0;
	DWORD		 	iPingNumber;

   ptr_cNomHeader = G_strXTFSideScanPingXML.ptr_cNomHeader;

   // Ecriture syst�matique dans le fichier binaire.
   ptrPingHeader = (unsigned char *)PingHeader;
   ptr_vValVariable = (void *)ptrPingHeader;
   ptr_vPrevVal = (void *)ptrPrevPingHeader;
   // Affectation des variables.
   for (iLoop=0; iLoop<(int)G_strXTFSideScanPingXML.sNbVarSignal; iLoop++) {
	   if (iLoop > 0)
	   {
		   usTailleVar = G_strXTFSideScanPingXML.strTabSignalXML[iLoop-1].usSize;
		   usNbElem = G_strXTFSideScanPingXML.strTabSignalXML[iLoop-1].usNbElem;
	   }
	   else
	   {
		   usTailleVar = 0; // Pour le premier paquet
		   usNbElem = 0;
	   }
	   bFlagConstant = G_strXTFSideScanPingXML.strTabSignalXML[iLoop].bFlagConstant;
	   // Pointage de la variable (par sa taille et son occurrence)
	   ptr_vValVariable = ptr_vValVariable + usTailleVar*usNbElem;
	   if (iNbHeader > 0)
	   {
		   ptr_vPrevVal = ptr_vPrevVal + usTailleVar*usNbElem;
		   bFlagCompare = util_bComparePrevAndCurrentVal(G_strXTFSideScanPingXML.strTabSignalXML[iLoop].cType,
											   ptr_vPrevVal,
											   ptr_vValVariable,
											   G_strXTFSideScanPingXML.strTabSignalXML[iLoop].usNbElem);
		   bFlagConstant = bFlagConstant && bFlagCompare;
		   G_strXTFSideScanPingXML.strTabSignalXML[iLoop].ptr_vPrevVal = ptr_vPrevVal;
		   // Observation de l'ordre des Ping pour l'attribut Direction.
		   if (!strcmp(G_strXTFSideScanPingXML.strTabSignalXML[iLoop].cNom, "PingNumber"))
		   {
			   iPrevPingNumber = *((DWORD *)ptr_vPrevVal);
			   if (iPrevPingNumber != 0)
			   {
				   iPingNumber = *((DWORD *)ptr_vValVariable);
				   if (iPrevPingNumber > iPingNumber)
				   {
					   G_bFlagSortIncPing = FALSE;
				   }
				   else
				   {
					   G_bFlagSortIncPing = TRUE;
				   }
			   }
		   }
	   }
	   G_strXTFSideScanPingXML.strTabSignalXML[iLoop].bFlagConstant = bFlagConstant;
	   // Pr�paration de l'�criture de la variable.
	   usTailleVar = G_strXTFSideScanPingXML.strTabSignalXML[iLoop].usSize;
	   usNbElem =  G_strXTFSideScanPingXML.strTabSignalXML[iLoop].usNbElem;
	   fwrite(ptr_vValVariable, usTailleVar, usNbElem, G_strXTFSideScanPingXML.strTabSignalXML[iLoop].ptr_fpBin);

   }
   ptrPrevPingHeader = malloc(sizeof(XTFPINGHEADER));
   // Recopie en zone tampon des valeurs courantes.
   memcpy(ptrPrevPingHeader, ptrPingHeader, sizeof(XTFPINGHEADER));

   return EXIT_SUCCESS;

} // vWriteBinSideScanPing


void vProcessSidescanPing(XTFPINGHEADER *PingHeader) {
/****************************************************************************/
// Put whatever processing here to be performed on SIDESCAN data.
// PingHeader points to the 256-byte ping header structure.  That structure
// identifies how many channels of sidescan follow.  The structure is followed
// by the sidescan data itself.
//
// For example: assume there are two channels of sidescan, stored 1024
// 8-bit samples per channel.  The data pointed to by PingHeader looks like:
//
// 256 bytes   - XTFPINGHEADER structure holds data about this ping
// 64 bytes    - XTFPINGCHANHEADER structure holds data about channel 1 (port)
// 1024 bytes  - channel 1 imagery
// 64 bytes    - XTFPINGCHANHEADER structure holds data about channel 2 (stbd)
// 1024 bytes  - channel 2 imagery
//
   WORD usChan;
   int tmp;
   unsigned char *Ptr = (unsigned char *)PingHeader;

   static int iNbHeader = 0;

   // For backwards-compatibility.  The samples per channel used to
   // be stored in the file header.

   // skip past the ping header
   Ptr += sizeof(XTFPINGHEADER);

	// Traitement pour �criture dans le fichier Binaire
    iWriteBinSideScanPing(iNbHeader, PingHeader);
	// Allocation de la taille m�moire selon le nombre de channels d'un ping
	if (iNbHeader == 0)
	{
		//GLUGLUGLU OLD G_strXTFPingChanHeaderXML = malloc(PingHeader->NumChansToFollow*sizeof(T_XTFRECORDXML));
		G_strXTFImageryXML = malloc(PingHeader->NumChansToFollow*sizeof(T_XTFRECORDXML));
	}

	for (usChan=0; usChan<PingHeader->NumChansToFollow; usChan++)
	{
		XTFPINGCHANHEADER *ChanHeader;
		unsigned char *Imagery;
		WORD ChannelNumber;
		WORD BytesPerSample;
		DWORD SamplesPerChan;
		DWORD BytesThisChannel;

		// point to the channel header
		ChanHeader		=	(XTFPINGCHANHEADER *) Ptr;

		// See which channel this is (0=port, 1=stbd, etc...)
		ChannelNumber	=	ChanHeader->ChannelNumber;

		// Compute the number of bytes of imagery for this channel
		//
		BytesPerSample	=	XTFFileHeader->ChanInfo[ChannelNumber].BytesPerSample;

		if (iNbHeader == 0)
		{
			// Initialisation des valeurs du paquet Ping Channel Header
			iInitXMLBinPingChanHeader("PingChanHeader", "TODO", usChan);
			// Initialisation des valeurs des donn�es d'imagerie
			iInitXMLBinImagery("Imagery", "TODO", usChan, BytesPerSample);
		}

		// If the NumSamples value in the channel header is zero, then this
		// must be an old-style XTF File where the samples per channel was
		// stored in this file header.
		// Release 25 SamplesPerChan = XTFFileHeader->ChanInfo[ChannelNumber].Reserved; // for backwards compatibility only!
		SamplesPerChan = (DWORD)*XTFFileHeader->ChanInfo[ChannelNumber].ReservedArea2; // for backwards compatibility only!
		tmp = ChanHeader->NumSamples;

		if (tmp != 0) SamplesPerChan = tmp;

		BytesThisChannel = BytesPerSample * SamplesPerChan;

		// Traitement pour �criture dans les fichiers Binaires les paquets
		// Channel info
		G_strXTFPingChanHeaderXML[usChan].iNbDatagram++;
		iWriteBinPingChanHeader(iNbHeader, usChan, ChanHeader);
		// skip past the channel header
		Ptr += sizeof(XTFPINGCHANHEADER);

		// Point to the imagery.  If BytesPerSample is 2, then
		// Imagery should be a pointer to a signed 16-bit value.
		// If BytesPerSample is 1, then Imagery should point to
		// a unsigned 8-bit value.
		Imagery = Ptr;
		G_strXTFImageryXML[usChan].iNbDatagram++;
		iWriteBinImagery(iNbHeader, usChan, Imagery, BytesPerSample, SamplesPerChan);

		// skip past the imagery;
		Ptr += BytesThisChannel;
   }

   iNbHeader++;

} //vProcessSidescanPing

#pragma pack() // peut etre remplace par l'option de compil -fpack-struct
