/*
 * XTF_processPingHeader.h
 *
 *  Created on: 13 oct. 2008
 *      Author: rgallou
 */

#ifndef XTF_PROCESSSIDESCANPING_H_
#define XTF_PROCESSSIDESCANPING_H_
#pragma pack(1) // peut etre remplace par l'option de compil -fpack-struct pour gcc

#include "XTF_processDatagrams.h"

int iWriteBinSideScanPing(int iNbHeader, XTFPINGHEADER *PingHeader);

void vProcessSidescanPing(XTFPINGHEADER *PingHeader);

#pragma pack() // peut etre remplace par l'option de compil -fpack-struct
#endif /* XTF_PROCESSSIDESCANPING_H_ */
