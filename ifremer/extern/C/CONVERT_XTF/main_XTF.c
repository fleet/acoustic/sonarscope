// ----------------------------------------------------------------------
// Programme :
//	CONVERT_XTF.C
//
// Objet :
//	Convertir un fichier au format XTF pour d�composer en fichiers
//	XML et en fichiers binaires. Les fichiers XML decrivent le contenu des
// 	binaires.
// 	Chaque fichier bianire comporte une donn�e �l�mentaire des paquets par ping
// 	issue des paquets HEADER du format XTF.
//
//	Il a �t� r�alise sous l'IDE Eclipse 3.4.1 avec compilateur gcc.
//
// Cr�ation :
//	02/10/2008
//
// Auteur :
//  GLU : (ATL) roger.gallou@altran.com
//		  02 98 05 43 21
//
// Historique des modifications :
// - passage du compteur de datagramme comme param�tre des fcts de traitements.
// - ajout de Facteur d'�chelles.
// - Renommage de Signals en Variables, de Tables en Images
// - correction du typage de donn�es de type Unsigned Char en u8
// - introduction des donn�es � 2 Dimensions (Image2)
// - restructuration de l'ent�te du fichier
// - cr�ation d'un fichier d'index.
// - traitement de fichiers pour 64 bits.
// - inhibition des balises Unit et Tag, inutilis�es pour l'instant.
//
// 27/09/2012 : GLU (ALTRAN Ouest)
// - Passage en option de l'affichage de la barre de progression.
//
// ----------------------------------------------------------------------
#pragma pack(1) // peut etre remplace par l'option de compil -fpack-struct pour gcc	// Pour la compilation sous Visual Studio
				// et l'alignement sur 8 octets.

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <conio.h>
#include <io.h>
#include <string.h>
#include <errno.h>
#include <libgen.h>
#include <time.h>
#include <unistd.h>

#include "XTF_ProcessDatagrams.h"
#include "XTF_Datagrams.h"
#include "XTF_Utilities.h"
#include "main_XTF.h"


#ifdef GUI
#include "ProgressInfo.h"
#endif

extern T_XTFRECORDXML G_strXTFFileHeaderXML;
//----------------------------------------------------------------
// Fonction 	: Main
// Objet 		: Programme principal
// Modification : 02/10/08
// Auteur 		: GLU
// Param�tres	: Nom du fichier (avec chemin)
//
//----------------------------------------------------------------
int main(int argc, char *argv[])
{
	char	cFileName[80],
			cFileNameTmp[80],
			cDirSonarScope[200],
			cFlagProgressBar[5];

	int 	iDummy,
			iErr,
			iProgressBar,
			iLoop;



	#ifdef TRACE_DEBUG
		time ( &G_rawtime );

		G_fpLog = fopen("C:\\Temp\\Toto.log", "w+");
		time ( &G_rawtime );
		G_timeinfo = localtime ( &G_rawtime );
		fprintf (G_fpLog,"1-Current local time and date: %s", asctime (G_timeinfo) );
	#endif

	// Initialisations des variables globales.
	buffer 				= NULL;
	XTFFileHeader 		= NULL;
	G_bFlagSortIncPing 	= TRUE;

	ChannelTypes[0] = "SUBBOTTOM";
	ChannelTypes[1] = "PORT";
	ChannelTypes[2] = "STBD";
	ChannelTypes[3] = "BATHY";
	ChannelTypes[4] = "ELAC";
	ChannelTypes[5] = "SERIAL";
	ChannelTypes[6] = "UNRECOGNIZED";


	// quick sanity check to make sure the compiler didn't
	// screw up the data structure sizes by changing alignment.
	if (
	  sizeof(XTFFILEHEADER)      	!= 1024 ||
	  sizeof(CHANINFO)           	!= 128  ||
	  sizeof(XTFPINGHEADER)      	!= 256  ||
	  sizeof(XTFNOTESHEADER)     	!= 256  ||
	  sizeof(XTFBATHHEADER)      	!= 256  ||
	  sizeof(XTFRAWSERIALHEADER) 	!= 64   ||
	  sizeof(XTFPINGCHANHEADER)  	!= 64	 ||
	  sizeof(XTFBATHYSNP0SNIPPET) 	!= 74   ||
	  sizeof(XTFBATHYSNP1SNIPPET)  	!= 24
	  ) {
		fprintf(stdout,"Error: Bad structure size! %d,%d,%d,%d,%d,%d,%d,%d,%d\n",
			sizeof(XTFFILEHEADER)     ,
			sizeof(CHANINFO)          ,
			sizeof(XTFPINGHEADER)     ,
			sizeof(XTFNOTESHEADER)    ,
			sizeof(XTFBATHHEADER)     ,
			sizeof(XTFRAWSERIALHEADER),
			sizeof(XTFPINGCHANHEADER),
			sizeof(XTFBATHYSNP0SNIPPET),
			sizeof(XTFBATHYSNP1SNIPPET));
		 getch();
		 return TRUE;
	}

	if (argc < 3) {
	  fprintf(stdout,"Usage: CONVERT_XTF <input .XTF file>\n");
	  fprintf(stdout,"\t\tReads an XTF file and prints some data about it.\n");
	  return FALSE;
	}

	// Mise en majuscule du r�pertoire et nom de fichier:
	// strupr(argv[2]);

	// Cr�ation du r�pertoire Racine contenant les .bin.
	fprintf(stdout,"File Process : %s\n", argv[2]);
	G_cFileData = calloc(150, sizeof(char));
	memcpy(G_cFileData, argv[2], strlen(argv[2]));
	G_cRepData = calloc(200, sizeof(char));
	// dirname pour le chemin et basename pour le nom de fichier simple.
	sprintf(cFileName, "%s", basename(argv[2]));
	// Suppression de l'extension
	cFileName[strlen(cFileName)-4] = '\0';
	sprintf(cFileNameTmp, "%s_tmp", cFileName);

	//R�cup�ration du flag de Progress Bar
	memcpy(cFlagProgressBar, argv[1], strlen(argv[1]));
	iProgressBar = (int)atoi(cFlagProgressBar);

	// V�rification de l'existence du r�petoire SonarScope.
	sprintf(cDirSonarScope, "%s\\%s", dirname(argv[2]), "SonarScope");
	iDummy = access(cDirSonarScope, F_OK);
	if (iDummy != 0)
	{
		iErr = mkdir(cDirSonarScope);
		if (iErr != 0)
		{
		   switch (errno)
		   {
			   // "File exists", on ne fait rien.
			   case EEXIST:
				   break;
			   // Autres cas d'erreurs
			   default:
				   fprintf(stdout,"Error in Directory creating : %s\n", strerror(errno));
				   return FALSE;
				   break;
		   }
		}
	}

	// Cr�ation du r�pertoire de donn�es
	sprintf(G_cRepData, "%s\\%s", cDirSonarScope, cFileNameTmp);

	if (DEBUG)
		fprintf(stdout,"-- Creation du repertoire : %s\n", G_cRepData);

	iErr = mkdir(G_cRepData);
	if (iErr != 0)
	{
	   switch (errno)
	   {
		   // "File exists", on ne fait rien.
		   case EEXIST:
			   break;
		   // Autres cas d'erreurs
		   default:
			   fprintf(stdout,"Error in Directory creating : %s\n", strerror(errno));
			   return FALSE;
			   break;
	   }
	}
//OLD
//	iIndexFile=0;
//
//	iIndexFile = open(G_cFileData, O_RDONLY | O_BINARY, 0000200);
//	if (iIndexFile <= 0) {
//	  printf("Error: Can't open %s for reading!\n", argv[2]);
//	  goto ERROR_OUT;
//	}
//OLD

    // Allocate memory for reading file data
	buffer = (unsigned char *) malloc((WORD)65535);
	if (buffer == NULL) {
	  fprintf(stdout,"Can't allocate memory!\n");
	  goto ERROR_OUT;
	}


	// Allocate memory for storing XTF header
	XTFFileHeader = (XTFFILEHEADER *) malloc((WORD)sizeof(XTFFILEHEADER));
	if (XTFFileHeader == NULL) {
	  fprintf(stdout,"\nCan't allocate memory for XTF header\n");
	  goto ERROR_OUT;
	}


	G_strXTFFileHeaderXML.iNbDatagram = 0;


	// Appel de la proc�dure principale de lecture/d�coupage de fichiers.
	#ifndef GUI
		iReadXTFFile(G_cFileData, NULL);
	#else
		if (iProgressBar == 1)
			runWithProgress(argc, argv, &iReadXTFFile, G_cFileData);
		else
			iReadXTFFile(G_cFileData, NULL);
	#endif


	// Renommage du r�pertoire puisqu'on arrive au bout !
	iDummy = util_iRenameDir(G_cRepData);
	if (iDummy == 0)
		printf("Process Ending : renaming Temporary Directory %s\n", G_cRepData);
	else
	{
		// On retente 10 Fois le renommage avec Pause.
		for (iLoop=0; (iLoop < 10 && iDummy !=0); iLoop++)
		{
			printf("Process Ending : new try for renaming Temporary Directory %s\n", G_cRepData);
			iDummy = util_iRenameDir(G_cRepData);
			// Pause 1sec.
			usleep(1000);
		}
		if (iDummy == 0)
			printf("Process Ending : renaming Temporary Directory %s\n", G_cRepData);
	}

	return TRUE;

	ERROR_OUT:

	fflush(stdout);
	if (iIndexFile > 0) {
	  close(iIndexFile);
	  iIndexFile = 0;
	}
	if (buffer != NULL) {
	  free(buffer);
	}
	if (XTFFileHeader != NULL) {
	  free(XTFFileHeader);
	}

	return FALSE;
} // Main


#pragma pack() // peut etre remplace par l'option de compil -fpack-struct
