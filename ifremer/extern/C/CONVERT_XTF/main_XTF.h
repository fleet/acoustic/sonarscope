/*
 * convertFiles_xtf.h
 *
 *  Created on: 13 oct. 2008
 *      Author: rgallou
 */

#ifndef CONVERTFILES_XTF_H_
#define CONVERTFILES_XTF_H_

#pragma pack(1) // peut etre remplace par l'option de compil -fpack-struct pour gcc // Pour la compilation sous Visual Studio
				// et l'alignement sur 8 octets.

   char			*G_cFileData;			// Fichier avec chemin d'origine
   char			*G_cRepData;			// R�pertoire de destination des donn�es
   BOOL 		G_bFlagSortIncPing;		// Tri croissant sur les Pings Number
   char			G_cSonarName[50];		// Eq. au DRF.DeviceIdentifier.
   u32			G_u32SonarType;			// Eq. au DRF.DeviceIdentifier.

#define DEBUG 0

#pragma pack() // peut etre remplace par l'option de compil -fpack-struct

#endif /* CONVERTFILES_XTF_H_ */
