/*
 * S7K_utilities.c
 *
 *  Created on: 9 oct. 2008
 *      Author: rgallou
 */
#pragma pack(1)
#include "string.h"
#include "ctype.h"
#include "stdlib.h"
#include "stdio.h"
#include <errno.h>
#include <windows.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include "Generic_Utilities.h"    // Contenu dans la librairie libCONVERT_utils.a

// Utilis� dans les fonctions de traitements de chaines.
# define NB_EXT 18

// D�finitions des extensions
char Extension [NB_EXT][8] = {	"ARC", "ALL", "WCD", "S7K", "RDF",
								"XTF", "SDF", "HAC", "HYP",
								"NA", "SO", "CE", "AT",
								"TI", "IM", "PA", "RT",
								"HAC"};

// D�finitions des correspondances
char Description[NB_EXT][64] = {"Archive", "Simrad", "Simrad-Water Column", "RESON", "GeoSwath",
								"Triton", "Klein", "IFR-Halieutique", "HYPACK",
								"Archive-Navigation", "Archive-Bathymetrie", "Archive-Celerite", "Archive-Attitude",
								"Archive-Time", "Archive-Imagerie", "Archive-Parametres", "Archive-RT",
								"FORMAT of PG HAC"};


/////////////////////////////////////////////////////////////////////////////////
// Fonction :	util_iDayJma2Ifr
// Objet 	:	Conversion en date Ifr d'une date.
// Modification : 12/11/08
// Auteur 	: GLU
/////////////////////////////////////////////////////////////////////////////////
int util_iDayJma2Ifr(	int Jour,
						int Mois,
						int Annee)
{
	int iDate,
		ks;

	if(Mois > 2)
	{
		Mois = Mois - 3;
	}
	else
	{
		Mois = Mois + 9;
		Annee = Annee - 1;
	}


	ks   = (int)(Annee / 100);
	Annee  = Annee - 100 * ks;

	iDate = (int)(146097 * ks / 4) + (int)(1461 * Annee / 4) + (int)((153 * Mois + 2) / 5) + Jour + 1721119;

	return iDate;

} //util_iDayJma2Ifr

/////////////////////////////////////////////////////////////////////////////////
// Fonction :	util_clTime
// Objet 	:	Conversion en �quivalent cl_time � partir d'une datet et d'une
//				heure.
// Modification : 12/11/08
// Auteur 	: GLU
/////////////////////////////////////////////////////////////////////////////////
double util_clTime(	int DateDatagram,
					int HeureDatagram)
{
	int		iNbMilisUnJour = 24*3600*1000,
			J;

	double	dTime,
			dDate,
			dHeure;


	dDate  = (double)DateDatagram;
	dHeure = (double)HeureDatagram;

	J = (int)(dHeure / iNbMilisUnJour);
	dHeure = dHeure - J * iNbMilisUnJour;
	dDate  = dDate + J;

	dTime  = ((double)dDate - (double)1721059.0) + ((double)dHeure / (double)86400000.0);

	return dTime;

} //util_clTime

/////////////////////////////////////////////////////////////////////////////////
// Fonction :	util_dEcritTime
// Objet 	:	Calcul num�rique de temps selon une date et une heure.
// Modification : 12/11/08
// Auteur 	: GLU
/////////////////////////////////////////////////////////////////////////////////
double util_dEcritTime(	int iDate,
						int iHeure)
{
	int	iJour,
		iMois,
		iAnnee;

	double	dResultTime;

	iAnnee = (int)(iDate / 10000);
	iMois = (int)((iDate - iAnnee*10000) / 100);
	iJour = (int)(iDate - (int)(iDate / 100)*100);

	dResultTime = util_clTime(util_iDayJma2Ifr(iJour,iMois,iAnnee),iHeure);

	return dResultTime;

} //util_dEcritTime


/////////////////////////////////////////////////////////////////////////////////
// Fonction :	util_vProgressBar
// Objet 	:	Calcul num�rique de progression (ratios).
// Modification : 12/11/08
// Auteur 	: GLU
/////////////////////////////////////////////////////////////////////////////////
void util_vProgressBar(int iLus, int iPosFic, int iTotal)
{
    static int iCurrent=0;
    static int ihOld;
    int ih;

	if (iLus != 0)
		iCurrent += iLus;
	else
		iCurrent = iPosFic;
	// Conversion en int du pourcentage de progression
	ih = (100*(double)iCurrent/(double)iTotal);
	if ((ih - ihOld) == 5)
    {
    	printf("%6.1f %% Complete \r",(float)ih);
        ihOld = ih;
	}

} //util_vProgressBar


/////////////////////////////////////////////////////////////////////////////////
// Fonction :	util_usNbOctetTypeVar
// Objet 	:	Convertit un type en Nombre d'octets.
// Modification : 12/11/08
// Auteur 	: GLU
/////////////////////////////////////////////////////////////////////////////////
unsigned short util_usNbOctetTypeVar(char *cType)
{
	unsigned short usTaille = 1;

	if (!strcmp(cType, "u8") || !strcmp(cType, "BYTE") || !strcmp(cType, "i8"))
		usTaille = 1;
	else if (!strcmp(cType, "u16") || !strcmp(cType, "WORD"))
		usTaille = 2;
	else if (!strcmp(cType, "i32") || !strcmp(cType, "u32") ||
			 !strcmp(cType, "f32") || !strcmp(cType, "DWORD"))
		usTaille = 4;
	else if (!strcmp(cType, "u64") || !strcmp(cType, "long"))
		usTaille = 8;
	else if (!strcmp(cType, "char") ||!strcmp(cType, "unsigned char") ||
			 !strcmp(cType, "signed char"))
		usTaille = 1;
	else if (!strcmp(cType, "short") || !strcmp(cType, "unsigned short") ||
			!strcmp(cType, "signed short"))
		usTaille = 2;
	else if (!strcmp(cType, "int") || !strcmp(cType, "unsigned int")  ||
			!strcmp(cType, "signed int"))
		usTaille = 4;
	else if (!strcmp(cType, "float"))
		usTaille = 4;
	else if (!strcmp(cType, "double") || !strcmp(cType, "f64"))
		usTaille = 8;

	return usTaille;

} //util_usNbOctetTypeVar


/////////////////////////////////////////////////////////////////////////////////
// Fonction :	util_bComparePrevAndCurrentVal
// Objet 	:	Comparaison en m�moire des valeurs.
// Modification : 12/11/08
// Auteur 	: GLU
/////////////////////////////////////////////////////////////////////////////////
BOOL util_bComparePrevAndCurrentVal(char *cType,
							   void *ptr_vPrevValVar,
							   void *ptr_vCurValVar,
							   int iNbElem)
{

BOOL bFlag = TRUE;


int iFlag;
unsigned short usTailleEnByte;

	bFlag = FALSE;
	usTailleEnByte = util_usNbOctetTypeVar(cType);
	iFlag = memcmp(ptr_vCurValVar, ptr_vPrevValVar, iNbElem*usTailleEnByte);
	// iFlag = 0 si les 2 parties sont �gales.
	if (iFlag != 0)
		bFlag = FALSE;
	else
		bFlag = TRUE;

	return bFlag;

} //util_bComparePrevAndCurrentVal

/////////////////////////////////////////////////////////////////////////////////
// Fonction :	util_cConvertTypeLabel
// Objet 	:	Etablit a correspondance entre les types des paquets
//				et les types de donn�es MatLab.
// Modification : 15/10/08
// Auteur 	: GLU
/////////////////////////////////////////////////////////////////////////////////
int util_cConvertTypeLabel(	char *cTypeDatagram,
							char *cTypeMatLab)
{
int iRet = 0;

	if (!strcmp(cTypeDatagram, "u8") || !strcmp(cTypeDatagram, "BYTE"))
		sprintf(cTypeMatLab, "%s", "uint8");
	if (!strcmp(cTypeDatagram, "i8"))
		sprintf(cTypeMatLab, "%s", "int8");
	else if (!strcmp(cTypeDatagram, "u16") || !strcmp(cTypeDatagram, "WORD"))
		sprintf(cTypeMatLab, "%s", "uint16");
	else if (!strcmp(cTypeDatagram, "u32") || !strcmp(cTypeDatagram, "DWORD"))
		sprintf(cTypeMatLab, "%s", "uint32");
	else if (!strcmp(cTypeDatagram, "u64"))
		sprintf(cTypeMatLab, "%s", "uint64");
	else if (!strcmp(cTypeDatagram, "i32"))
		sprintf(cTypeMatLab, "%s", "int32");
	else if (!strcmp(cTypeDatagram, "f32"))
		sprintf(cTypeMatLab, "%s", "float32");
	else if (!strcmp(cTypeDatagram, "f64"))
		sprintf(cTypeMatLab, "%s", "float64");
	else if (!strcmp(cTypeDatagram, "char"))
		sprintf(cTypeMatLab, "%s", "char");
	else if (!strcmp(cTypeDatagram, "unsigned char"))
		sprintf(cTypeMatLab, "%s", "uchar");
	else if (!strcmp(cTypeDatagram, "short"))
		sprintf(cTypeMatLab, "%s", "int16");
	else if (!strcmp(cTypeDatagram, "unsigned short"))
		sprintf(cTypeMatLab, "%s", "uint16");
	else if (!strcmp(cTypeDatagram, "int"))
		sprintf(cTypeMatLab, "%s", "int32");
	else if (!strcmp(cTypeDatagram, "unsigned int"))
		sprintf(cTypeMatLab, "%s", "uint32");
	else if (!strcmp(cTypeDatagram, "float"))
		sprintf(cTypeMatLab, "%s", "float32");
	else if(!strcmp(cTypeDatagram, "long"))
		sprintf(cTypeMatLab, "%s", "int64");
	else if (!strcmp(cTypeDatagram, "double"))
		sprintf(cTypeMatLab, "%s", "double");

	return iRet;
} //util_cConvertTypeLabel

/////////////////////////////////////////////////////////////////////////////////////////
// Fonction :	util_cValVarFromFile
// Objet 	:	Relit la valeur depuis le fichier Bin.
// Modification : 12/11/08
// Auteur 	: GLU
/////////////////////////////////////////////////////////////////////////////////////////
int util_cValVarFromFile(	char *cFileNameBin,
							char *cType,
							unsigned  usNbElem,
							char *cValeur)
{
unsigned short		usTailleEnByte;

void 				*ptr_vValeur = NULL;
FILE				*ptr_fpBin;

int 				iRet = 0;


	usTailleEnByte= util_usNbOctetTypeVar(cType);
	ptr_vValeur = (void*)malloc(usTailleEnByte*usNbElem*sizeof(char));
	ptr_fpBin = fopen(cFileNameBin, "r+b");
	if (!ptr_fpBin)
	{
	   printf("%s -- Error in Retrieve Signal constant value  : %s\n", __FILE__, cFileNameBin);
	   iRet = -1;
	}
	else
	{
		fread(ptr_vValeur, usTailleEnByte, usNbElem,ptr_fpBin);
		iRet = util_cValVarConstant(	cType,
										ptr_vValeur,
										(int)usNbElem,
										cValeur);

		fclose(ptr_fpBin);
		// D�tection et contournement des caract�res interdits en XML.
		if (strstr(cValeur, "&") != NULL || strstr(cValeur, ">") != NULL)
		{
			sprintf(cValeur, "%s", "XML forbidden characters for MATLAB detected");
		}

		//On lib�re uniquement pour des donn�es de type non-char.
		// sinon plantage (????)
		//if (strcmp(cType, "char"))
		//{
			free(ptr_vValeur);
		//}
	}

	return iRet;

}	// util_cValVarFromfile

/////////////////////////////////////////////////////////////////////////////////
// Fonction :	util_cValVarConstant
// Objet 	:	Traduit en char une valeur constante point�e.
// Modification : 12/11/08
// Auteur 	: GLU
/////////////////////////////////////////////////////////////////////////////////
int util_cValVarConstant(	char *cType,
							void *ptr_vValVar,
							int iNbElem,
							char *cValeur)
{
int iRet = 0;

char					*cValVar = NULL;
unsigned char 			*ucValVar = NULL;
unsigned short 			*usValVar = 0;
unsigned int			*uiValVar = 0;
short					*sValVar = 0;
int						*iValVar = 0;
long 					*lValVar = 0;
unsigned long 			*ulValVar = 0;
unsigned long long int	*ullValVar = 0;
float 					*fValVar;
double					*dValVar;


	if (!strcmp(cType, "BYTE") || !strcmp(cType, "u8"))
	{
		ucValVar = malloc(iNbElem*sizeof(BYTE));
		memcpy(ucValVar, ptr_vValVar, iNbElem*sizeof(BYTE));
		sprintf(cValeur, "%d", *ucValVar);
		free(ucValVar);
	}
	else if (!strcmp(cType, "WORD") || !strcmp(cType, "u16"))
	{
		usValVar = malloc(iNbElem*sizeof(short));
		memcpy(usValVar, ptr_vValVar, iNbElem*sizeof(WORD));
		sprintf(cValeur, "%u", *usValVar);
		free(usValVar);
	}
	else if (!strcmp(cType, "unsigned short"))
	{
		usValVar = malloc(iNbElem*sizeof(unsigned short));
		memcpy(usValVar, ptr_vValVar, iNbElem*sizeof(unsigned short));
		sprintf(cValeur, "%d", *usValVar);
		free(usValVar);
	}
	else if (!strcmp(cType, "DWORD") || !strcmp(cType, "u32"))
	{
		ulValVar = malloc(iNbElem*sizeof(DWORD));
		memcpy(ulValVar, ptr_vValVar, iNbElem*sizeof(DWORD));
		sprintf(cValeur, "%ld", *ulValVar);
		free(ulValVar);
	}
	else if (!strcmp(cType, "u64"))
	{
		ullValVar = (u64 *)malloc(iNbElem*sizeof(u64));
		memcpy(ullValVar, ptr_vValVar, iNbElem*sizeof(u64));
		/*
		#ifdef __WIN32__
			sprintf(cValeur, "%I64i\n",*ullValVar);
		#else
			sprintf(cValeur, "%llu\n",*ullValVar);
		#endif
		 */
		#ifdef __WIN32__
			sprintf(cValeur, "%I64i",*ullValVar);
		#else
			sprintf(cValeur, "%llu",*ullValVar);
		#endif
		//sprintf(cValeur, "%lld", *ullValVar);
		free(ullValVar);
	}
	else if (!strcmp(cType, "char"))
	{
		cValVar = (char *)malloc((iNbElem)*sizeof(char));
		memcpy(cValVar, ptr_vValVar, (iNbElem)*sizeof(char));
		cValVar[iNbElem-1] = '\0';
		if (iNbElem > 1)
		{
			sprintf(cValeur, "%s", cValVar);
		}
		else
		{
			sprintf(cValeur, "%c", cValVar[0]);
		}

		free(cValVar);
	}
	else if (!strcmp(cType, "i8"))
	{
		cValVar = (char *)malloc(iNbElem*sizeof(char));
		memcpy(cValVar, ptr_vValVar, iNbElem*sizeof(char));
		sprintf(cValeur, "%d", *cValVar);
		free(cValVar);
	}
	else if (!strcmp(cType, "unsigned char"))
	{
		ucValVar = malloc(iNbElem*sizeof(unsigned char));
		memcpy(ucValVar, ptr_vValVar, iNbElem*sizeof(unsigned char));
		sprintf(cValeur, "%c", *ucValVar);
		free(ucValVar);
	}
	else if (!strcmp(cType, "short"))
	{
		sValVar = malloc(iNbElem*sizeof(short));
		memcpy(sValVar, ptr_vValVar, iNbElem*sizeof(short));
		sprintf(cValeur, "%d", *sValVar);
		free(sValVar);
	}
	else if (!strcmp(cType, "int") || !strcmp(cType, "i32"))
	{
		iValVar = malloc(iNbElem*sizeof(int));
		memcpy(iValVar, ptr_vValVar, iNbElem*sizeof(int));
		sprintf(cValeur, "%d", *iValVar);
		free(iValVar);
	}
	else if (!strcmp(cType, "unsigned int") || !strcmp(cType, "ui32"))
	{
		uiValVar = (unsigned int*)malloc(iNbElem*sizeof(unsigned int));
		memcpy(uiValVar, ptr_vValVar, iNbElem*sizeof(unsigned int));
		sprintf(cValeur, "%u", *uiValVar);
		free(uiValVar);
	}
	else if (!strcmp(cType, "float"))
	{
		fValVar = (float *)malloc(iNbElem*sizeof(float));
		memcpy(fValVar, ptr_vValVar, iNbElem*sizeof(float));
		sprintf(cValeur, "%20.10f", *fValVar);
		free(fValVar);
	}
	else if (!strcmp(cType, "f32"))
	{
		fValVar = (f32 *)malloc(iNbElem*sizeof(f32));
		memcpy(fValVar, ptr_vValVar, iNbElem*sizeof(f32));
		sprintf(cValeur, "%20.10f", *fValVar);
		free(fValVar);
	}
	else if (!strcmp(cType, "long"))
	{
		lValVar = malloc(iNbElem*sizeof(long));
		memcpy(lValVar, ptr_vValVar, iNbElem*sizeof(long));
		sprintf(cValeur, "%ld", *lValVar);
		free(lValVar);
	}
	else if (!strcmp(cType, "double") || !strcmp(cType, "f64"))
	{
		dValVar = malloc(iNbElem*sizeof(double));
		memcpy(dValVar, ptr_vValVar, iNbElem*sizeof(double));
		sprintf(cValeur, "%20.10lf", *dValVar);
		free(dValVar);
	}

	return iRet;

}	// util_cValVarConstant

/////////////////////////////////////////////////////////////////////////////////
// Procedure	: 	util_GetBits
// Objectif  	: 	Proc�dure qui retourne la valeur de iNbBit � partir iNumBit.
//					(cf. K&R Ch. Op�rateur Bit � Bit).
/////////////////////////////////////////////////////////////////////////////////
int util_GetBits(	unsigned int iData,
					unsigned int iNumBit,
					unsigned int iNbBit)
{
	return((iData >> (iNumBit+1-iNbBit)) & ~(~0<<iNbBit));

} // util_GetBits;

/////////////////////////////////////////////////////////////////////////////////
// Procedure	: 	util_vStrrep
// Objectif  	: 	Proc�dure de remplacement d'une chaine par une autre.
/////////////////////////////////////////////////////////////////////////////////
/* C string replacement routine - sorely needed in libc
 * v1 written Nov 26th, 2004
 * initial version by Christian Lavoie
 * some improvements by Kacper Wysocki
 * This file is in the public domain. Yours to do with as you please.
 * */
void util_vStrrep(	char* in,
					char** out,
					char* old,
					char* newString)
{
	char* temp;
	char* found = strstr(in, old);
	if(!found) {
		*out = malloc(strlen(in) + 1);
		strcpy(*out, in);
		return;
	}

	int idx = found - in;

	*out = realloc(*out, strlen(in) - strlen(old) + strlen(newString) + 1);
	strncpy(*out, in, idx);
	strcpy(*out + idx, newString);
	strcpy(*out + idx + strlen(newString), in + idx + strlen(old));


	temp = malloc(idx+strlen(newString)+1);
	strncpy(temp,*out,idx+strlen(newString));
	temp[idx + strlen(newString)] = '\0';

	util_vStrrep(found + strlen(old), out, old, newString);
	temp = realloc(temp, strlen(temp) + strlen(*out) + 1);
	strcat(temp,*out);
	free(*out);
	*out = temp;

}	//util_cStrrep

/////////////////////////////////////////////////////////////////////////////////
// Procedure	: 	util_iRemoveDir
// Objectif  	: 	Pour forcer la suppression d'un repertoire m�me si il contient
//					des fichiers ou des r�pertoires
// Liens		:	http://www.siteduzero.com/forum-83-154142-supprimer-un-dossier-et-tout-son-contenu.html
/////////////////////////////////////////////////////////////////////////////////
int util_iRemoveDir(char const *cDirName)
{
    DIR *directory;           /* pointeur de r�pertoire */
    struct dirent *entry;     /* repr�sente une entr�e dans un r�pertoire. */
    struct stat file_stat;    /* informations sur un fichier. */

    /* Ce tableau servira � stocker le chemin d'acc�s complet
     * des fichiers et dossiers. Pour simplifier l'exemple,
     * je le d�finis comme un tableau statique (avec une taille
     * a priori suffisante pour la majorit� des situations),
     * mais on pourrait l'allouer dynamiquement pour pouvoir
     * le redimensionner si jamais on tombait sur un chemin
     * d'acc�s d�mesur�ment long. */
    char buffer[1024] = {0};

    /* On ouvre le dossier. */
    directory = opendir(cDirName);
    if ( directory == NULL ) {
        fprintf(stderr, "Cannot open directory %s\n", cDirName);
        return 0;
    }

    /* On boucle sur les entr�es du dossier. */
    while ( (entry = readdir(directory)) != NULL ) {

        // On "saute" les r�pertoires "." et "..".
        if ( strcmp(entry->d_name, ".") == 0 ||
             strcmp(entry->d_name, "..") == 0 ) {
        	continue;
        }

        /* On construit le chemin d'acc�s du fichier en
         * concat�nant son nom avec le nom du dossier
         * parent. On intercale "/" entre les deux.
         * NB: '/' est aussi utilisable sous Windows
         * comme s�parateur de dossier. */
        snprintf(buffer, 1024, "%s/%s", cDirName, entry->d_name);

        // On r�cup�re des infos sur le fichier.
        stat(buffer, &file_stat);
        // J'ai la flemme de tester la valeur de retour, mais
        // dans un vrai programme il faudrait le faire :D

        if ( S_ISREG(file_stat.st_mode) ) {
            // On est sur un fichier, on le supprime.
            remove(buffer);
        }
        else if ( S_ISDIR(file_stat.st_mode) ) {
            // On est sur un dossier, on appelle cette fonction.
        	util_iRemoveDir(buffer);
        }
    }

    // On ferme le dossier.
    closedir(directory);

    // Maintenant le dossier doit �tre vide, on le supprime.
    // remove(cDirName); // Ne foncionne pas pour les r�pertoires !
    rmdir(cDirName);

    return 0;
} //util_iRemoveDir


/////////////////////////////////////////////////////////////////////////////////
// Procedure	: 	util_iRenameDir
// Objectif  	: 	Renommage du r�pertoire  avec effacement d'un existant.
/////////////////////////////////////////////////////////////////////////////////
int util_iRenameDir(char const *cDirName)
{
	char	*cRepDataFinal;
	int		iRet = -1,
			iLoop,
			iDummy;

	cRepDataFinal = (char *)malloc(1);
	// Suppression de la cha�ne _tmp du nom initial.
	util_vStrrep((char*)cDirName, &cRepDataFinal, "_tmp", "");

	// V�rification de l'existence d'un r�pertoire pour l'effacer pr�alablement.
	iRet = access(cRepDataFinal, F_OK);
	if (iRet == 0)
	{
		//util_vForce_rmdir(cRepDataFinal);
		//OLD iRet = util_iRemoveDir(cRepDataFinal);
		iRet = util_iRemoveDirectory(cRepDataFinal);
	}

	if ((iRet = rename(cDirName, cRepDataFinal)) !=0)
	{
	   printf("Cannot rename Directory %s (%s)\n",  cDirName, ERRORSTRING);
	}
	// Test d'existence (10 it�rations) du r�pertoire avant de rendre la main.
	// Tentative de r�solution du pb entre l'appel des CONVERT_XXXX et MatLab : trop rapide.
	for (iLoop = 0; iLoop == 10; iLoop++)
	{
		iDummy = access(cRepDataFinal, F_OK);
		if (iDummy != 0)
		{
			// Temps d'attente avant de rendre la main.
			Sleep(0.5);
		}
		else
		{
			// Sortie de la boucle d'attente.
			break;
		}
	}
	//

	free(cRepDataFinal);

	return iRet;

} //util_iRenameDir


/////////////////////////////////////////////////////////////////////////////////
// Procedure	: 	util_iIsDirectory
// Objectif  	: 	D�termination si r�pertoire ou non.
/////////////////////////////////////////////////////////////////////////////////
int util_iIsDirectory(char path[])
{
    int i = strlen(path) - 1;
    if (path[strlen(path)] == '.') {return TRUE;} // exception for directories
    // such as \. and \..
    for (i=strlen(path) - 1; i >= 0; i--)
    {
        if (path[i] == '.') return FALSE; // if we first encounter a . then it's a file
        else if (path[i] == '\\' || path[i] == '/') return TRUE; // if we first encounter a \ it's a dir
    }
    return TRUE;
} // util_iIsDirectory

/////////////////////////////////////////////////////////////////////////////////
// Procedure	: 	util_iRemoveDirectory
// Objectif  	: 	Effacement du r�pertoire avec effacement d'un contenu.
/////////////////////////////////////////////////////////////////////////////////
int util_iRemoveDirectory(char * path)
{

int i;
    if (path[strlen(path)-1] != '\\')
    {
    	strcat(path, "\\");
    }
    // first off, we need to create a pointer to a directory
    DIR *pdir = NULL; // remember, it's good practice to initialise a pointer to NULL!
    pdir = opendir (path);
    struct dirent *pent = NULL;
    if (pdir == NULL) { // if pdir wasn't initialised correctly
        return FALSE; // return false to say "we couldn't do it"
    } // end if
    char file[256];

    int counter = 1; // use this to skip the first TWO which cause an infinite loop (and eventually, stack overflow)
    while ((pent = readdir (pdir)))
    {
    	// while there is still something in the directory to list
        if (counter > 2) {
            for (i = 0; i < 256; i++)
            {
            	file[i] = '\0';
            }
            strcat(file, path);
            if (pent == NULL) { // if pent has not been initialised correctly
                return FALSE; // we couldn't do it
            } // otherwise, it was initialised correctly, so let's delete the file~
            strcat(file, pent->d_name); // concatenate the strings to get the complete path
            if (util_iIsDirectory(file) == TRUE) {
                util_iRemoveDirectory(file);
            } else { // it's a file, we can use remove
                remove(file);
            }
        } counter++;
    }

    // finally, let's clean up
    closedir (pdir); // close the directory
    if (!rmdir(path)) return FALSE; // delete the directory
    return TRUE;

} // util_iRemove Directory

unsigned short util_sBswap_16(unsigned short x)
{
  return (x>>8) | (x<<8);
} //

unsigned int util_uiBswap_32(unsigned int x)
{
  return (util_sBswap_16(x&0xffff)<<16) | (util_sBswap_16(x>>16));
} // util_uiBswap_32

int util_iBswap_32(int x)
{
  return (util_sBswap_16(x&0xffff)<<16) | (util_sBswap_16(x>>16));
} // util_iBswap_32

unsigned long long util_ullBswap_64(unsigned long long x)
{
  return (((unsigned long long)util_uiBswap_32(x&0xffffffffull))<<32) | (util_uiBswap_32(x>>32));
} // util_llBswap_64

char * util_cBswap_16(char *x)
{
	char cBidon;

	cBidon = x[0];
	x[0] = x[1];
	x[1] = cBidon;
	return x;
} // util_cBswap_16

char * util_cBswap_32(char *x)
{
	char *cBidon;

	cBidon = malloc(4*sizeof(char));
	memcpy(cBidon, x, 4*sizeof(char));

	x[3] = cBidon[0];
	x[2] = cBidon[1];
	x[1] = cBidon[2];
	x[0] = cBidon[3];

	free(cBidon);
	return x;
} // util_cBswap_32

char * util_cBswap_64(char *x)
{
	char *cBidon;

	cBidon = malloc(8*sizeof(char));
	memcpy(cBidon, x, 8*sizeof(char));

	x[7] = cBidon[0];
	x[6] = cBidon[1];
	x[5] = cBidon[2];
	x[4] = cBidon[3];
	x[3] = cBidon[4];
	x[2] = cBidon[5];
	x[1] = cBidon[6];
	x[0] = cBidon[7];

	free(cBidon);
	return x;
} // util_cBswap_64



// Converti une chaine de texte en minuscule
char * util_strtoupper(char *dest, const char *src)
{

	size_t i;

	i = strlen(src);
	dest[i] = '\0';

	while(i>0)
	{
		dest[i - 1] = toupper(src[i - 1]);
		i--;
	}

	return dest;
} // util_strtoupper


// Retourne la lettre du lecteur
int util_GetFileDrive(char FilePath[MAX_PATH], char *tFileDrive)
{
size_t i;

	i = 0;

	tFileDrive[0] = '\0';

	while(FilePath[i] != '\\')
	{
		i++;
	}

	strncat(tFileDrive, FilePath, i);
	return 0;
} // util_GetFileDrive


// Retourne le chemin du fichier
int util_GetFileDirectory(char FilePath[MAX_PATH], char *tFileDirectory)
{
size_t pos1, pos2;

pos1 = 0;
pos2 = 0;

tFileDirectory[0] = '\0';


	// Recherche de la premi�re occurence du backslash
	while(FilePath[pos1] != '\\' && pos1 < MAX_PATH)
	{
		pos1++;
	}

	pos2 = strlen(FilePath);

	// Recherche de la derni�re occurence du backslash
	while(FilePath[pos2] != '\\' && pos2 > 0)
	{
		pos2--;
	}

	strncat(tFileDirectory, FilePath + pos1, pos2 - pos1 + 1);

	return 0;

} //util_GetFileDirectory


// Retourne le nom du fichier
int util_GetFileName(char FilePath[MAX_PATH], char *tFileName)
{

size_t i;

	i = strlen(FilePath);

	tFileName[0] = '\0';

	while(FilePath[i-1] != '\\')
	{
	i--;
	}

	strcpy(tFileName,FilePath + i);

	return 0;
} // util_GetFileName


// Retourne le type du fichier
int util_GetFileType(char FilePath[MAX_PATH], char *FileExt, char *FileType)
{
	size_t i;
	char ext[8];


	i = strlen(FilePath);

	// Recherche de l'extension du fichier
	while(FilePath[i] != '.' && i>0)
	{
		i--;
	}

	if(i <= 0) return 1; // Code erreur

	util_strtoupper(ext,FilePath + ++i); // On incr�mente de un pour ne pas avoir le '.' dans l'extension

	// On compare les extensions
	i = 0;

	while(strcmp(Extension[i],ext) != 0 && i < NB_EXT)
	{
		i++;
	}

	if(i==NB_EXT)
	{
		FileExt = NULL;
		strcpy(FileType,"Extension non connu!");
	}
	else
	{
		strcpy(FileExt,Extension[i]);
		strcpy(FileType,Description[i]);
	}

	return 0;

} //util_GetFileType

// Extrait de http://www.gdargaud.net/Hack/SourceCode.html.
void *util_SwapEndian(void* Addr, const int Nb)
{
	static char Swapped[16];
	switch (Nb) {
		case 2:	Swapped[0]=*((char*)Addr+1);
				Swapped[1]=*((char*)Addr  );
				break;
		case 3:	// As far as I know, 3 is used only with RGB images
				Swapped[0]=*((char*)Addr+2);
				Swapped[1]=*((char*)Addr+1);
				Swapped[2]=*((char*)Addr  );
				break;
		case 4:	Swapped[0]=*((char*)Addr+3);
				Swapped[1]=*((char*)Addr+2);
				Swapped[2]=*((char*)Addr+1);
				Swapped[3]=*((char*)Addr  );
				break;
		case 8:	Swapped[0]=*((char*)Addr+7);
				Swapped[1]=*((char*)Addr+6);
				Swapped[2]=*((char*)Addr+5);
				Swapped[3]=*((char*)Addr+4);
				Swapped[4]=*((char*)Addr+3);
				Swapped[5]=*((char*)Addr+2);
				Swapped[6]=*((char*)Addr+1);
				Swapped[7]=*((char*)Addr  );
				break;
		case 16:Swapped[0]=*((char*)Addr+15);
				Swapped[1]=*((char*)Addr+14);
				Swapped[2]=*((char*)Addr+13);
				Swapped[3]=*((char*)Addr+12);
				Swapped[4]=*((char*)Addr+11);
				Swapped[5]=*((char*)Addr+10);
				Swapped[6]=*((char*)Addr+9);
				Swapped[7]=*((char*)Addr+8);
				Swapped[8]=*((char*)Addr+7);
				Swapped[9]=*((char*)Addr+6);
				Swapped[10]=*((char*)Addr+5);
				Swapped[11]=*((char*)Addr+4);
				Swapped[12]=*((char*)Addr+3);
				Swapped[13]=*((char*)Addr+2);
				Swapped[14]=*((char*)Addr+1);
				Swapped[15]=*((char*)Addr  );
				break;
	}
	return (void*)Swapped;
} // util_SwapEndian

/////////////////////////////////////////////////////////////////////////////////
// Procedure	: 	util_GetFileSize
// Objectif  	: 	Retourne la taille du fichier (sous 32/64bits).
// Cette fonction retourne 0 en cas de succes, une valeur differente dans le cas
// contraire.
// La taille du fichier, si elle a pu etre calculee, est retournee dans *ptr.
//
// Lien : http://c.developpez.com/faq/?page=fichiers
/////////////////////////////////////////////////////////////////////////////////
int util_GetFileSize(const char * fname, long * ptr)
{

    FILE * f;
    int ret = 0;

    f = fopen(fname, "rb");
    if (f != NULL)
    {
        fseeko64(f, 0, SEEK_END); 	// aller a la fin du fichier.
        *ptr = ftello64(f); 		// lire l'offset de la position courante par rapport au debut du fichier.
        fclose(f);
    }
    else
        ret = 1;

    return ret;
} // util_GetFileSize


#pragma pack()

