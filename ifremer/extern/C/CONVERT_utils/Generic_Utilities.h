/*
 * S7K_Utilities.h
 *
 *  Created on: 9 oct. 2008
 *      Author: rgallou
 */

#ifndef GENERIC_UTILITIES_H_
#define GENERIC_UTILITIES_H_

#include "stdlib.h"

typedef int		    	BOOL;
#ifndef FALSE
	#define FALSE		    0
#endif
#ifndef TRUE
	#define TRUE		    (!FALSE) // D�j� d�finie dans windef.h de MingW
#endif
#define PI 3.1416		// pr�cision �quivalente � la fct sous matlab
#define RAD2DEG			180/PI

#define MIN(a,b) ((a)>(b)?(b):(a))
#define MAX(a,b) ((a)<(b)?(b):(a))


// D�finition des types de d�clarations des variables S7K
typedef unsigned char			u8;
typedef unsigned int 			u32;
typedef unsigned short			u16;
typedef short					i16;
typedef	unsigned long long int	u64;
typedef float					f32;
typedef double					f64;
typedef	int						i32;

// D�finition des types de d�clarations des variables XTF
typedef unsigned char		BYTE;
typedef unsigned short		WORD;
typedef unsigned long		DWORD;
typedef unsigned int		UINT;
typedef signed long			LONG;


#define ERRORSTRING (errno == 0 ? "unknown reason" : strerror (errno))

int 		util_iDayJma2Ifr(	int Jour,
								int Mois,
								int Annee);

double 		util_clTime(	int iDate,
							int iHeure);

double		util_dEcritTime(int iDate,
							int iHeure);


void 		util_vProgressBar(	int iLus,
							int iElapsed,
							int iTotal);

unsigned 	short util_usNbOctetTypeVar(char *cType);

BOOL 		util_bComparePrevAndCurrentVal(char *cType,
									   void *ptr_vPrevValVar,
									   void *ptr_vCurValVar,
									   int iNbElem);

int 		util_cConvertTypeLabel(	char *cTypeDatagram,
								char *cTypeMatLab);

int 		util_cValVarFromFile(	char *cFileNameBin,
								char *cType,
								unsigned  usNbElem,
								char *cValeur);

int			util_cValVarConstant(	char *cType,
								void *ptr_vValVar,
								int iNbOctets,
								char *cValeur);

int			util_GetBits(	unsigned int iData,
						unsigned int iNumBit,
						unsigned int iNbBit);

// Remplacement de cha�nes de caract�res.
void 		util_vStrrep(	char* in,
					char** out,
					char* old,
					char* newString);

// Traitement des r�pertoires.
int 		util_iRemoveDir(char const *cDirName);
int 		util_iRenameDir(char const *cDirName);
int 		util_iIsDirectory(char path[]);
int 		util_iRemoveDirectory(char * path);

// Traitement du Swap.
unsigned short 		util_sBswap_16(unsigned short x);
unsigned int 		util_uiBswap_32(unsigned int x);
int					util_iBswap_32(int x);
unsigned long long	util_ullBswap_64(unsigned long long x);
char * 				util_cBswap_16(char *x);
char * 				util_cBswap_32(char *x);
char * 				util_cBswap_64(char *x);

void *util_SwapEndian(	void* Addr,
						const int Nb);

// Traitement des chemins et noms de fichiers.
char *  util_strtoupper(char *dest, const char *src);
int		util_GetFileDrive(char FilePath[MAX_PATH], char *tFileDrive);
int 	util_GetFileDirectory(char FilePath[MAX_PATH], char *tFileDirectory);
int		util_GetFileName(char FilePath[MAX_PATH], char *tFileName);
int 	util_GetFileType(char FilePath[MAX_PATH], char *FileExt, char *FileType);
int 	util_GetFileSize(const char * fname, long * ptr);
#endif // GENERIC_UTILITIES_H_
