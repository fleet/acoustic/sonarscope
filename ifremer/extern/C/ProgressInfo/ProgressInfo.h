/*
 * ProgressInfo.h
 *
 *  Created on: 16 oct. 2009
 *      Author: BRL, <guillaume.bourel@altran.com>
 */

#ifndef PROGRESSINFO_H_
#define PROGRESSINFO_H_

/**
 * Typedef for progress callback function.
 */
//typedef void(*progressFn)(int);

/**
 * Run a function with a progress bar.
 * @param argc main function arguments count
 * @param argv main function arguments values
 * @param computeFn computation function to execute
 * @param param1 parameter for computation method
 */
#ifdef __cplusplus
extern "C" void runWithProgress(int argc, char **argv,
		int(*computeFn)(const char*, void(*progressFn)(int)), const char* param1);
#else
	void runWithProgress(int argc, char **argv,
		int(*computeFn)(const char*, void(*progressFn)(int)), const char* param1);
#endif

#endif /* PROGRESSINFO_H_ */
