/*
 * ProgressWatcher.cpp
 *
 *  Created on: 16 oct. 2009
 *      Author: BRL, <guillaume.bourel@altran.com>
 */

#include "ProgressWatcher.h"
#include <QtCore/QtCore>

ProgressWatcher* ProgressWatcher::progressInstance = NULL;

ProgressWatcher::ProgressWatcher() {

}

ProgressWatcher::~ProgressWatcher() {

}

void ProgressWatcher::progressChangeWrapper(int progressValue) {
	// qDebug() << "Progress value (wrapper) : " << progressValue;
	if (progressInstance)
		progressInstance->progressValueChanged(progressValue);
}

