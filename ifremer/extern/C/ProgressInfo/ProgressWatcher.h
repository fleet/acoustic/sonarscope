/*
 * ProgressWatcher.h
 *
 *  Created on: 16 oct. 2009
 *      Author: BRL, <guillaume.bourel@altran.com>
 */

#ifndef PROGRESSWATCHER_H_
#define PROGRESSWATCHER_H_

#include <QtCore/QObject>

/**
 * This class provides signals and wrapper to build a C compatible function pointer.
 * ! WARNING ! Due to the static progress instance this class isn't reentrant !
 */
class ProgressWatcher: public QObject {
Q_OBJECT

public:
	ProgressWatcher();
	virtual ~ProgressWatcher();

	/// Wrapper to use as a C compatible pointer.
	static void progressChangeWrapper(int progressValue);

	/// This instance is use by #progressChangeWrapper(int)
	static ProgressWatcher *progressInstance;

signals:
	/// Signal to update progression
	void progressValueChanged(int progressValue);

};

#endif /* PROGRESSWATCHER_H_ */
