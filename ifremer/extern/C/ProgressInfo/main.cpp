/****************************************************************************
 **
 ** Copyright (C) 2009 Nokia Corporation and/or its subsidiary(-ies).
 ** Contact: Nokia Corporation (qt-info@nokia.com)
 **
 ** This file is part of the examples of the Qt Toolkit.
 **
 ** $QT_BEGIN_LICENSE:LGPL$
 ** No Commercial Usage
 ** This file contains pre-release code and may not be distributed.
 ** You may use this file in accordance with the terms and conditions
 ** contained in the Technology Preview License Agreement accompanying
 ** this package.
 **
 ** GNU Lesser General Public License Usage
 ** Alternatively, this file may be used under the terms of the GNU Lesser
 ** General Public License version 2.1 as published by the Free Software
 ** Foundation and appearing in the file LICENSE.LGPL included in the
 ** packaging of this file.  Please review the following information to
 ** ensure the GNU Lesser General Public License version 2.1 requirements
 ** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
 **
 ** In addition, as a special exception, Nokia gives you certain
 ** additional rights.  These rights are described in the Nokia Qt LGPL
 ** Exception version 1.1, included in the file LGPL_EXCEPTION.txt in this
 ** package.
 **
 ** If you have questions regarding the use of this file, please contact
 ** Nokia at qt-info@nokia.com.
 **
 **
 ** $QT_END_LICENSE$
 **
 ****************************************************************************/

#include <QtGui>
#include "ProgressInfo.h"
#include "ProgressWatcher.h"

#ifndef QT_NO_CONCURRENT

using namespace QtConcurrent;


/**
 * Run a function with a progress bar.
 * @param argc main function arguments count
 * @param argv main function arguments values
 * @param computeFn computation function to execute
 * @param param1 parameter for computation method
 */
void runWithProgress(int argc, char **argv,
			int(*computeFn)(const char*, void(*progressFn)(int)), const char* param1) {
	QApplication app(argc, argv);

	QProgressDialog dialog;
	// Create a progress dialog.
	dialog.setLabelText(QString("Progressing file %1 ...").arg(param1));
	// Create a ProgressWatcher and connect signals and slots.
	ProgressWatcher pw;
	QObject::connect(&pw, SIGNAL(progressValueChanged(int)), &dialog,
			SLOT(setValue(int)));
	dialog.setRange(0, 100);

	// define progress instance
	ProgressWatcher::progressInstance = &pw;

	void (*ptProgress)(int) = &(ProgressWatcher::progressChangeWrapper);

	QtConcurrent::run(computeFn, param1, ptProgress);

	// Display the dialog and start the event loop.
	dialog.setCancelButton(NULL); // Pas de bouton.
	dialog.exec();
	qDebug() << "Canceled ? " << dialog.wasCanceled();
} //runWithProgress


// Symbole � d�finir pour passer en mode Executable et produire une application de d�monstration
// de la barre de progression.
// si STATIC est d�finie : compilation en librairie statique
// si Static n'est pas d�finie : compilation en ex�cutable
//#define STATIC

#ifndef STATIC
// Test method.
int test(const char* name, void(*progressFn)(int)) {
	const int work = 1000 * 1000 * 40;
	volatile int v = 0;
	for (int i = 0; i <= 100; i += 10) {
		for (int j = 0; j < work; ++j)
			++v;
		if (progressFn)
			progressFn(i);
	}

	qDebug() << "test done for " << name;
	return 0;
}


// Main for tests.
int main(int argc, char **argv) {
	runWithProgress(argc, argv, &test, (const char*)"a test string");
	return 0;
}
#endif

#else

#ifndef STATIC
int main()
{
	qDebug() << "Qt Concurrent is not yet supported on this platform";
}
#endif

#endif


