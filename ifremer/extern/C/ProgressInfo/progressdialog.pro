TARGET += 
TEMPLATE = lib
DEPENDPATH += .
INCLUDEPATH += .

# Input
SOURCES += ProgressWatcher.cpp \
    main.cpp

# windows (using app template) or staticlib (using lib template)
CONFIG += ...
staticlib:DEFINES += STATIC

# install
target.path = $$[QT_INSTALL_EXAMPLES]/qtconcurrent/progressdialog
sources.files = $$SOURCES \
    $$HEADERS \
    $$FORMS \
    $$RESOURCES \
    *.pro \
    *.png
sources.path = $$[QT_INSTALL_EXAMPLES]/qtconcurrent/progressdialog
INSTALLS += target \
    sources
symbian:
HEADERS = ProgressInfo.h \
    ProgressWatcher.h \
    ProgressInfo.h \
    ProgressWatcher.h
