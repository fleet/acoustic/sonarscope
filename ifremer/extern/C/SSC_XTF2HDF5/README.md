ConvertNG
=========

ConvertNG est un prototype de successeur à la suite de convertisseurs de
fichiers de sonar vers le cache de SonarScope. Cette version change le format de
cache pour utiliser « [HDF5](https://www.hdfgroup.org/HDF5/) » comme format
support commun à toutes les données sonar.

Pré-requis
==========

Compilateur c++14
-----------------

ConvertNG est développé en c++14. Il faut donc disposer d’un compilateur
supportant cette norme (n’importe quelle version récente de gcc ou clang).

Sous Windows, nous conseillons d’utiliser Mingw-w64
([mingw-w64.org](http://mingw-64.org)). Il faudra s’assurer que les différentes
bibliothèques utilisées par la suite (i.e. hsf5, boost et Qt) sont compilées
avec la même version de compilateur que celle qui sera utilisée pour compiler
ConvertNG (de moins avec les même options d’implantation du mécanisme
d’exceptions et de support de concurence). À l’heure actuelle (2016/01/25), 
la librairie HDF5 ne peut être compilée qu'avec mingw - version 64 bits 
(et non pas 32 bits).

Préconisations:
lors de l'installation, choisir:
- Architecture : x86_64
- Threads : win32
Ne pas omettre de rajouter (ou vérifier la présence) dans le Path, le chemin vers l'installation du bin, ex : 
C:\Program Files\mingw-w64\x86_64-5.2.0-win32-seh-rt_v4-rev0\mingw64\bin

CMake
-----

ConvertNG utilise `cmake`([cmake.org](https://cmake.org)) pour gérer sa chaine
de compilation. `cmake` est disponible sur toutes les principales plate-formes
(Windows, Linux, Mac, BSD…) et permet d’avoir une seule procédure unifiée de
construction.

Boost
-----

Boost est une librairie générale c++ dont certains modules sont utilisés par
ConvertNG (actuellement « system », « filesystem » et « program_options »). Pour
les plateformes Linux, cette librairie est installable via le gestionnaire de
packets.

Pour Windows, il est nécessaire de reconstruire la librairie manuellement. Une
fois les sources téléchargées et extraites, se rendre dans le dossier de sources
avec un invité de commandes (opération qui peut se révèler longue) :

```
bootstrap.bat
b2 toolset=gcc address-model=64 install
```

HDF5
----

La librairie HDF5 est utilisée pour gérer le format HDF sur disque vers lequel les
fichiers sonars sont convertis. Pour les systèmes Linux, il est possible de
l’installer via le gestionnaire de packets.

Sous Windows, la librairie doit être compilée manuellement. Il est recommandé de
travailler à partir de package spécialisé pour
[CMake](https://www.hdfgroup.org/HDF5/release/cmakebuild.html).

Qt
--

Qt est un framework principalement utilisé afin de générer les fenetres de
progression de conversion. Ce support est optionnel. Qt est généralement
disponnible sur les distributions GNU/Linux.

Sous Windows, la librairie doit être recompilée en utilisant le compilateur
allant servir à compiler ConvertNG.  Pour compiler les Qt, récupérer les sources
sur le site http://qt.io, les extraire, et se rendre dans le répertoir dans un
invité de commande et lancer :

```
$ configure -opensource -platform win32-g++ -no-opengl
$ mingw32-make intsall
```
