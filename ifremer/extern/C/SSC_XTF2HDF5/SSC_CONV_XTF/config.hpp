/*
 * config.hpp
 *
 *  Created on: 24 nov. 2015
 *      Author: rgallou
 */

#ifndef CONFIG_HPP_
#define CONFIG_HPP_

#define PROJECT_VERSION 	"0.1"
#define PROJECT_DATE 	"2015 December"
#define PROJECT_NAME 	"SSC_CONV_XTF"
#define PROJECT_AUTHOR 	"L.SIX - R.GALLOU - Quiet-Oceans"

#define USE_QT	0

#endif /* CONFIG_HPP_ */
