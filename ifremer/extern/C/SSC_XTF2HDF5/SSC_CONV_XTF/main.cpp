#include "config.hpp"

#include <memory>
#include <iostream>
#include <iomanip>

#if HAVE_IOCTL_H
#include <sys/ioctl.h>
#endif

#if USE_QT
#include "ProgressInfo.h"
#endif

#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>

#include "xtf_decoder.hpp"
#include "ssc_abstract_decoder.hpp"

void
no_progress(int)
{
}

void text_progress (int cpct) {
    static int pct {0};
    static int term_size {0};
    static int init {0};

    if (init == 0) {
#if HAVE_IOCTL_H
        struct winsize w;
        if (ioctl(1, TIOCGWINSZ, &w) != -1)
            term_size = w.ws_col;
#endif
        init++;
    }
    // term_size == 0 indicates that we do not have a valid underlying TTY
    if (term_size == 0)
        return;

    int i {0};
    if (cpct > pct) {
        pct = cpct;

        // we save space for "[] xxx% "
        int progr_len {term_size - 8};
        int done = (static_cast<float>(progr_len) * (static_cast<float>(pct) / 100.0));
        std::cout << "\r[";
        if (done > 0)
            for (i = 0; i < done ; i++)
                std::cout << "=";
        if (pct == 100)
            std::cout << "=";
        else
            std::cout << ">";
        for (i = i + 1; i < progr_len; i++)
            std::cout << " ";
        std::cout << "] " << std::setw(3) << pct << "%";
        if (pct == 100)
            std::cout << std::endl;
        std::cout.flush ();
    }
};

#if USE_QT
ssc::xtf::XTFDecoder * __UNSAFE__decoder;
bool __UNSAFE_decode_success { false };
int __UNSAFE__decode (const char *, void(*progressFn)(int))
{
    __UNSAFE_decode_success = __UNSAFE__decoder->decode_file (progressFn);
    return __UNSAFE_decode_success;
}
#endif

int
main(int argc, char** argv)
{
    // Handle command-line argument
    boost::program_options::options_description desc ("Allowed arguments");
    desc.add_options ()
        ("help,h", "print help message")
        ("version,v", "print program version")
        ("xtf_file,i", boost::program_options::value< std::string >()->required (),
         "XTF file to decode" )
        ("out_file,o", boost::program_options::value< std::string >(),
         "Name of the HDF5 file to produce")
        ("force,F", "Force owerwriting of HDF5 file if already exist")
        ("progress,p", "Use progress bar");

    boost::program_options::positional_options_description pos;
    pos.add ("xtf_file", 1).
        add ("out_file", 1);


    boost::program_options::variables_map vm;

    boost::program_options::store (boost::program_options::command_line_parser (argc, argv).
                                                             options (desc).
                                                             positional (pos).
                                                             run (),
                                   vm);


    if (argc < 2 || vm.count ("help")) {
        std::cout << argv[0] << " <input_xtf> [output_hdf5]" << std::endl << std::endl;
        std::cout << PROJECT_NAME << "-" << PROJECT_VERSION << std::endl << std::endl;
        std::cout << desc << std::endl;
        return EXIT_FAILURE;
    };

    if (vm.count ("version")) {
        std::cout << PROJECT_NAME << "-" << PROJECT_VERSION << std::endl;
        return EXIT_SUCCESS;
    }

    boost::program_options::notify (vm);


    // We need the input file to be obtained from command lin
    boost::filesystem::path input_file (vm["xtf_file"].as< std::string >());
    // We can forge a default output path
    boost::filesystem::path output_file {input_file.parent_path () / (input_file.stem ().string () + ".hdf5")};
    // but prefer the argument if provided.
    if (vm.count("out_file")) {
        output_file = vm["out_file"].as< std::string >();
    }

    // Overwrite HDF5 option process : exit if option is absent and file exist.
    if (boost::filesystem::exists (output_file)) {
        if (vm.count("force"))
        {
            boost::filesystem::remove (output_file);
        }
        else
        {
            std::cerr << "HDF 5 file already exist. Please force overwrite in command : "<< std::endl;
            std::cerr << argv[0] << " --force <input_xtf> [output_hdf5]" << std::endl;
            return EXIT_FAILURE;
        }
    }

    std::cout << "Converting '" << input_file.make_preferred ().string ()
              << "' into ' " << output_file.make_preferred ().string ()
              << "'." << std::endl;

    // Do the actual decoding of the XTF file
    ssc::xtf::XTFDecoder dec (input_file.string (),
                              output_file.string ());
    bool decode_success;
    if (dec) {
        if (vm.count("progress")) {
#if USE_QT
            __UNSAFE__decoder = &dec;
            runWithProgress (argc, argv, __UNSAFE__decode, input_file.filename ().string ().c_str ());
            decode_success = __UNSAFE_decode_success;
#else
            decode_success = dec.decode_file (text_progress);
#endif
        } else {
            decode_success = dec.decode_file(no_progress);
        }
    } else {
        decode_success = false;
    }

    std::cout << "End processing '" << std::endl;
    return decode_success ? EXIT_SUCCESS : EXIT_FAILURE;
}
