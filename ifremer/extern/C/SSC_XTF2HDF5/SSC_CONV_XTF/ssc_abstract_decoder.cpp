#include "ssc_abstract_decoder.hpp"

#include <iostream>

ssc::AbstractDecoder::AbstractDecoder() {}
ssc::AbstractDecoder::~AbstractDecoder() {}

void
ssc::AbstractDecoder::_skip_current (std::fstream & stream)
{
    XTFUNKNOWNHEADER head;
    _read (stream,
           reinterpret_cast<char *>(&head),
           sizeof(XTFUNKNOWNHEADER));

    // Go forward in the file by the size of the current record,
    // minus what have already been read (14 bytes)
    _seek (stream, head.rec_size - sizeof(XTFUNKNOWNHEADER) - sizeof(XTFCHUNKPRELUDE));
}

void
ssc::AbstractDecoder::_read (std::fstream & st,
                                  char * const buff,
                                  size_t size)
{
    if (st) {
        st.read (buff, size);
    } else {
        throw StreamError ();
    }
}

void
ssc::AbstractDecoder::_seek (std::fstream & st,
                                  size_t size)
{
    if (st) {
        st.seekg (size, st.cur);
    } else {
        throw StreamError ();
    }
}

void
ssc::AbstractDecoder::preprocess (std::fstream & st, BYTE header_type) {
    _count[header_type]++;
    this->_preprocess(st, header_type);
}

void
ssc::AbstractDecoder::preprocess_done () {
    this->_preprocess_done ();
    for (auto count : _count)
        std::cout << "Datagram '" << static_cast<int>(count.first)
                  << "' met " << count.second << " times" << std::endl;
}

void
ssc::AbstractDecoder::process (std::fstream & st, BYTE header_type) {
    this->_process (st, header_type);
}

void
ssc::AbstractDecoder::terminate () {
    this->_terminate ();
}
