#ifndef SSC_ABSTRACT_DECODER_HPP
#define SSC_ABSTRACT_DECODER_HPP

#include <fstream>
#include <exception>
#include "xtf_types.hpp"
#include "ssc_hdf5_utils.hpp"

#include <H5Cpp.h>

namespace ssc {
    class StreamError : public std::exception {};

    class AbstractDecoder {
    public:
        void preprocess (std::fstream & st, BYTE header_type);
        void preprocess_done ();
        void process (std::fstream & st, BYTE header_type);
        void terminate ();
    protected:
        /** \brief Initialize the decoder with a given output
         */
        explicit AbstractDecoder ();

        /** \brief run preprocessing over the stream (acquire data).
         *
         * It is assumed that after a call to this methos, the stream st is
         * placed at the begining of the next chunk of data.
         *
         * \pre this->accept (header_type)
         */
        virtual void _preprocess (std::fstream & st, BYTE header_type) = 0;

        /** \brief let the decoder know that pre-processing is finished
         *
         * After this procedure have been called, preprocess will not be
         * called again, the stream is rewinded and process will be called.
         *
         */
        virtual void _preprocess_done () = 0;

        /** \brief Decode a datagram from the stream where the 3 first bytes have been consumed
         *
         * It is assumed that after a call to this methos, the stream st is
         * placed at the begining of the next chunk of data.
         *
         * \pre this->accept (header_type)
         */
        virtual void _process (std::fstream & st, BYTE header_type) = 0;

        /** \brief Indicates that no more datagrames will be processed
         *
         * A call to this method should flush all remaining output.
         */
        virtual void _terminate () = 0;

        /** \brief Frees all resourses
         */
        virtual ~AbstractDecoder ();

    protected:
        // utility function that goes at the end of the current chunk
        // (given that it only the 3 first bytes have been consumed)
        void _skip_current (std::fstream &);
        void _read (std::fstream &, char * const, size_t);
        void _seek (std::fstream &, size_t);

    private:
        std::map<BYTE, size_t> _count;
    };
} // ssc

#endif
