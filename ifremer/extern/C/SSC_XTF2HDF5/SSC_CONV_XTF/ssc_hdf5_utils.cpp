#include "ssc_hdf5_utils.hpp"

#include <array>

ssc::HDF5::HDFProducer::HDFProducer (H5::H5File & f) :
    _out_file {f}
{
}

void 
ssc::HDF5::add_str_attribute (H5::H5Location & loc,
                              std::string attr_name,
                              std::string val)
{
    // Create new dataspace for attribute
    H5::DataSpace attr_dataspace (H5S_SCALAR);
    // Create new string datatype for attribute
    H5::StrType strdatatype(H5::PredType::C_S1, val.length () + 1); // 1 for final null char
    loc.createAttribute (attr_name.c_str (),
                         strdatatype,
                         attr_dataspace).write (strdatatype, H5std_string (val.c_str ()));
}

H5::Group
ssc::HDF5::create_or_open_group (H5::CommonFG & parent,
                                 char const * const name)
{
    for (hsize_t i = 0; i < parent.getNumObjs (); i++) {
        if (parent.childObjType (i) == H5O_TYPE_GROUP && parent.getObjnameByIdx (i) == std::string(name))
            return parent.openGroup (name);
    }
    return parent.createGroup (name);
}

