#ifndef SSC_HDF5_UTILS_HPP
#define SSC_HDF5_UTILS_HPP

#include "H5Cpp.h"

#include <algorithm>
#include <array>
#include <functional>
#include <iterator>
#include <vector>

template <class T, class M> M get_member_type(M T:: *);
#define GET_TYPE_OF(mem) decltype(get_member_type(mem))

#define CREATE_STRUCT_MEM_VECTOR(g,s,memb,vec,unit,comment,scaleFactor,addOffset) \
    {\
        std::vector< GET_TYPE_OF(&s::memb) > __values;\
        __values.reserve (vec.size ());\
        GET_TYPE_OF(&s::memb) const __ref_val = vec.begin ()->memb;\
        bool all_identical {true};\
        std::for_each (\
            vec.begin (), vec.end (),\
            [&__values,&__ref_val,&all_identical](s const & elem) \
            {\
                all_identical = all_identical && (elem.memb == __ref_val);\
                __values.push_back (elem.memb);\
            });\
        if (all_identical) {\
            std::vector< GET_TYPE_OF(&s::memb) > __tmp;\
            __tmp.push_back (__ref_val);\
            ::ssc::HDF5::create_vector (g, #memb, __tmp, unit, comment, scaleFactor, addOffset);\
        }\
        else\
            ::ssc::HDF5::create_vector (g, #memb, __values, unit, comment, scaleFactor, addOffset);\
    }

#define ADD_STRUCT_ATTRIBUTE(g,val,memb)\
    ::ssc::HDF5::add_attribute (g, #memb, val.memb)

#define ADD_STR_STRUCT_ATTRIBUTE(g,val,memb)\
    ::ssc::HDF5::add_str_attribute (g, #memb, val.memb)

namespace ssc {
    namespace HDF5 {

        /**
         * The TypeMapper is used to map between a native type to a HDF5
         * predefined type.
         *
         * \see HDFProducer::_ins_in_vec for usage
         */
        template<typename T> struct TypeMapper { };
        template<> struct TypeMapper<unsigned char> {
            static H5::PredType const & memtype () { return H5::PredType::NATIVE_UCHAR; }
            static H5::PredType const & filetype () { return memtype (); }
        };
        template<> struct TypeMapper<const unsigned char> {
            static H5::PredType const & memtype () { return H5::PredType::NATIVE_UCHAR; }
            static H5::PredType const & filetype () { return memtype (); }
        };
        template<> struct TypeMapper<unsigned short> {
            static H5::PredType const & memtype () { return H5::PredType::NATIVE_USHORT; }
            static H5::PredType const & filetype () { return memtype (); }
        };
        template<> struct TypeMapper<const unsigned short> {
            static H5::PredType const & memtype () { return H5::PredType::NATIVE_USHORT; }
            static H5::PredType const & filetype () { return memtype (); }
        };
        template<> struct TypeMapper<short> {
            static H5::PredType const & memtype () { return H5::PredType::NATIVE_SHORT; }
            static H5::PredType const & filetype () { return memtype (); }
        };
        template<> struct TypeMapper<const short> {
            static H5::PredType const & memtype () { return H5::PredType::NATIVE_SHORT; }
            static H5::PredType const & filetype () { return memtype (); }
        };
        template<> struct TypeMapper<unsigned int> {
            static H5::PredType const & memtype () { return H5::PredType::NATIVE_UINT; }
            static H5::PredType const & filetype () { return memtype (); }
        };
        template<> struct TypeMapper<const unsigned int> {
            static H5::PredType const & memtype () { return H5::PredType::NATIVE_UINT; }
            static H5::PredType const & filetype () { return memtype (); }
        };
        template<> struct TypeMapper<signed int> {
            static H5::PredType const & memtype () { return H5::PredType::NATIVE_INT; }
            static H5::PredType const & filetype () { return memtype (); }
        };
        template<> struct TypeMapper<const signed int> {
            static H5::PredType const & memtype () { return H5::PredType::NATIVE_INT; }
            static H5::PredType const & filetype () { return memtype (); }
        };
        template<> struct TypeMapper<float> {
            static H5::PredType const & memtype () { return H5::PredType::NATIVE_FLOAT; }
            static H5::PredType const & filetype () { return memtype (); }
        };
        template<> struct TypeMapper<const float> {
            static H5::PredType const & memtype () { return H5::PredType::NATIVE_FLOAT; }
            static H5::PredType const & filetype () { return memtype (); }
        };
        template<> struct TypeMapper<double> {
            static H5::PredType const & memtype () { return H5::PredType::NATIVE_DOUBLE; }
            static H5::PredType const & filetype () { return memtype (); }
        };
        template<> struct TypeMapper<const double> {
            static H5::PredType const & memtype () { return H5::PredType::NATIVE_DOUBLE; }
            static H5::PredType const & filetype () { return memtype (); }
        };

        /** \brief Helper class to insert data un a HDF5 file
         */
        class HDFProducer {
          protected:
            H5::H5File & _out_file;
            explicit HDFProducer (H5::H5File &);
        };

        /** \brief Add a text attribute to a location (Group/DataSet)
         */
        void add_str_attribute (H5::H5Location & loc,
                                std::string attr_name,
                                std::string val);

        template <typename T>
        void add_attribute (H5::H5Location & loc, std::string const & attr_name, T& v)
        {
            std::array<hsize_t, 1> geom { 1 };
            H5::DataSpace dataspace (geom.size (), &geom[0]);
            loc.createAttribute (
                    attr_name.c_str (),
                    ssc::HDF5::TypeMapper<T>::filetype (),
                    dataspace).write (ssc::HDF5::TypeMapper<T>::memtype (), &v);
        }

        // Elementary function for writing values
        template<typename T>
        void
        create_vector (H5::Group & g, std::string const & nme, std::vector< T > & values,
                       std::string const & unit, std::string const & comment,
                       float const scaleFactor,float const addOffset)
        {
            std::array<hsize_t, 1> const dims {values.size ()};
            H5::DataSpace dspace (dims.size (), &dims[0]);
            H5::DataSet dset {g.createDataSet (nme, ssc::HDF5::TypeMapper< T >::filetype (), dspace)};

            dset.setComment (comment);
            add_str_attribute (dset, "unit", unit);
            add_attribute (dset, "scaleFactor", scaleFactor);
            add_attribute (dset, "addOffset", addOffset);

            dset.write (&values[0], ssc::HDF5::TypeMapper<T>::memtype ());
        }


        /** T vector extraction from U vector with generic function f.
            (Call elementary create_vector above). */
        template<typename T, typename U>
        void
        create_vector (H5::Group & g, std::string const & nme, std::vector< U > & values,
                       std::function<T(U const &)> f,
                       std::string const unit, std::string const comment,
                       float const scaleFactor,float const addOffset)
        {
            std::vector< T > extracted_vals;
            extracted_vals.reserve (values.size ());
            std::transform (values.begin (), values.end (), std::back_inserter (extracted_vals),
                            [f](U const & v) -> T { return f(v); });
            create_vector (g, nme, extracted_vals, unit, comment, scaleFactor, addOffset);
        }

        /** \brief Open a group,and create it if it do not exist yet.
         */
        H5::Group
        create_or_open_group (H5::CommonFG & parent,char const * const name);
    } // HDF5
} // SSC

#endif // SSC_HDF5_UTILS_HPP
