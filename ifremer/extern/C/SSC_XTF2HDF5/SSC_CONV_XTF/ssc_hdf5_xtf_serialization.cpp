#include "ssc_hdf5_xtf_serialization.hpp"

#include "ssc_hdf5_utils.hpp"

void
ssc::HDF5::XTF::add_to_group_property (H5::Group & g, XTFFILEHEADER const & head)
{
   ADD_STR_STRUCT_ATTRIBUTE(g, head, RecordingProgramName);
   ADD_STR_STRUCT_ATTRIBUTE(g, head, RecordingProgramVersion);
   ADD_STR_STRUCT_ATTRIBUTE(g, head, SonarName);
   ADD_STR_STRUCT_ATTRIBUTE(g, head, NoteString);
   ADD_STR_STRUCT_ATTRIBUTE(g, head, ThisFileName);

   ADD_STRUCT_ATTRIBUTE(g, head, SystemType);
   ADD_STRUCT_ATTRIBUTE(g, head, SonarType);
   ADD_STRUCT_ATTRIBUTE(g, head, NavUnits);
   ADD_STRUCT_ATTRIBUTE(g, head, NumberOfSonarChannels);
   ADD_STRUCT_ATTRIBUTE(g, head, NumberOfBathymetryChannels);
   ADD_STRUCT_ATTRIBUTE(g, head, NumberOfSnippetsChannels);
   ADD_STRUCT_ATTRIBUTE(g, head, NumBerOfForwardLookArrays);
   ADD_STRUCT_ATTRIBUTE(g, head, NumberOfEchoStrengthChannels);
   ADD_STRUCT_ATTRIBUTE(g, head, NumberOfInterferometryChannels);
   ADD_STRUCT_ATTRIBUTE(g, head, ReferencePointHeight);
   ADD_STRUCT_ATTRIBUTE(g, head, NavigationLatency);
   ADD_STRUCT_ATTRIBUTE(g, head, OriginY);
   ADD_STRUCT_ATTRIBUTE(g, head, OriginX);
   ADD_STRUCT_ATTRIBUTE(g, head, NavOffsetY);
   ADD_STRUCT_ATTRIBUTE(g, head, NavOffsetX);
   ADD_STRUCT_ATTRIBUTE(g, head, NavOffsetZ);
   ADD_STRUCT_ATTRIBUTE(g, head, NavOffsetYaw);
   ADD_STRUCT_ATTRIBUTE(g, head, MRUOffsetY);
   ADD_STRUCT_ATTRIBUTE(g, head, MRUOffsetX);
   ADD_STRUCT_ATTRIBUTE(g, head, MRUOffsetZ);
   ADD_STRUCT_ATTRIBUTE(g, head, MRUOffsetYaw);
   ADD_STRUCT_ATTRIBUTE(g, head, MRUOffsetPitch);
   ADD_STRUCT_ATTRIBUTE(g, head, MRUOffsetRoll);
}

void
ssc::HDF5::XTF::add_to_group_property (H5::Group & g, CHANINFO const & ch)
{
    add_attribute (g, "CorrectionFlags", ch.CorrectionFlags);
    add_attribute (g, "UniPolar", ch.UniPolar);
    add_str_attribute (g, "ChannelName",  ch.ChannelName);
    add_attribute (g, "VoltScale",  ch.VoltScale);
    add_attribute (g, "Frequency", ch.Frequency);
    add_attribute (g, "HorizBeamAngle", ch.HorizBeamAngle);
    add_attribute (g, "TiltAngle", ch.TiltAngle);
    add_attribute (g, "BeamWidth", ch.BeamWidth);
    add_attribute (g, "OffsetX", ch.OffsetX);
    add_attribute (g, "OffsetY", ch.OffsetY);
    add_attribute (g, "OffsetZ", ch.OffsetZ);
    add_attribute (g, "OffsetYaw", ch.OffsetYaw);
    add_attribute (g, "OffsetPitch", ch.OffsetPitch);
    add_attribute (g, "OffsetRoll", ch.OffsetRoll);
    add_attribute (g, "BeamsPerArray", ch.BeamsPerArray);
    add_attribute (g, "SamplePerFormat", ch.SamplePerFormat);
}

void
ssc::HDF5::XTF::create_vector (H5::Group & g, std::vector< XTFATTITUDEDATA > & values)
{
    CREATE_STRUCT_MEM_VECTOR(g, XTFATTITUDEDATA, MagicNumber, values, "", "", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFATTITUDEDATA, HeaderType, values, "", "", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFATTITUDEDATA, SubChannelNumber, values, "", "", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFATTITUDEDATA, NumChansToFollow, values, "", "", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFATTITUDEDATA, EpochMicroseconds, values, "", "In (0 -999999)", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFATTITUDEDATA, SourceEpoch, values, "", "Epoch Seconds since 1/1/1970,", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFATTITUDEDATA, Pitch, values, "", "", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFATTITUDEDATA, Roll, values, "", "", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFATTITUDEDATA, Heave, values, "", "", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFATTITUDEDATA, Yaw, values, "", "", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFATTITUDEDATA, TimeTag, values, "milliseconds", "", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFATTITUDEDATA, Heading, values, "degrees", "", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFATTITUDEDATA, Year, values, "", "Fix year", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFATTITUDEDATA, Month, values, "", "Fix month", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFATTITUDEDATA, Day, values, "", "Fix day", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFATTITUDEDATA, Hour, values, "", "Fix hour", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFATTITUDEDATA, Minutes, values, "", "Fix minutes", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFATTITUDEDATA, Seconds, values, "", "Fix seconds", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFATTITUDEDATA, Milliseconds, values, "", "Fix milliseconds, in (0-999)", 1, 0);
}

void
ssc::HDF5::XTF::create_vector (H5::Group & g, std::vector< XTFPINGHEADER > & values)
{
    CREATE_STRUCT_MEM_VECTOR(g, XTFPINGHEADER, SubChannelNumber, values, "", "", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFPINGHEADER, NumChansToFollow, values, "", "", 1, 0);
    // CREATE_STRUCT_MEM_VECTOR(g, XTFPINGHEADER, Reserved2, values, "", "", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFPINGHEADER, NumBytesThisRecord, values, "", "", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFPINGHEADER, Year, values, "", "Ping Year", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFPINGHEADER, Month, values, "", "Ping Month", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFPINGHEADER, Day, values, "", "Ping Day", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFPINGHEADER, Hour, values, "", "Ping Hour", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFPINGHEADER, Minute, values, "", "Ping Minute", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFPINGHEADER, Second, values, "", "Ping Second", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFPINGHEADER, HSeconds, values, "", "Ping HSeconds", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFPINGHEADER, JulianDay, values, "", "Ping JulianDay", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFPINGHEADER, EventNumber, values, "", "Ping Year", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFPINGHEADER, PingNumber, values, "", "", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFPINGHEADER, SoundVelocity, values, "m/s", "", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFPINGHEADER, OceanTide, values, "m", "Altitude above Geoide or Ocean tide", 1, 0);
    // CREATE_STRUCT_MEM_VECTOR(g, XTFPINGHEADER, Reserved2, values, "", "", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFPINGHEADER, ConductivityFreq, values, "Hz", "", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFPINGHEADER, TemperatureFreq, values, "Hz", "", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFPINGHEADER, PressureFreq, values, "Hz", "", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFPINGHEADER, PressureTemp, values, "Degrees C", "", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFPINGHEADER, Conductivity, values, "Siemens/m", "", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFPINGHEADER, WaterTemperature, values, "Celsius", "", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFPINGHEADER, Pressure, values, "psia", "", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFPINGHEADER, ComputedSoundVelocity, values, "m/s", "", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFPINGHEADER, MagX, values, "mgauss", "", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFPINGHEADER, MagY, values, "mgauss", "", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFPINGHEADER, MagZ, values, "mgauss", "", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFPINGHEADER, AuxVal1, values, "", "", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFPINGHEADER, AuxVal2, values, "", "", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFPINGHEADER, AuxVal3, values, "", "", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFPINGHEADER, AuxVal4, values, "", "", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFPINGHEADER, AuxVal5, values, "", "", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFPINGHEADER, AuxVal6, values, "", "", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFPINGHEADER, SpeedLog, values, "knots", "", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFPINGHEADER, Turbidity, values, "", "", 10000, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFPINGHEADER, ShipSpeed, values, "knots", "", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFPINGHEADER, ShipGyro, values, "degrees", "", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFPINGHEADER, ShipYcoordinate, values, "degrees", "Ship latitude or northing", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFPINGHEADER, ShipXcoordinate, values, "degrees", "Ship longitude or easting", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFPINGHEADER, ShipAltitude, values, "dm", "in decimeters ", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFPINGHEADER, ShipDepth, values, "dm", "in decimeters", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFPINGHEADER, FixTimeHour, values, "", "Hour of most recent nav update", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFPINGHEADER, FixTimeMinute, values, "", "Minute of most recent nav update", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFPINGHEADER, FixTimeSecond, values, "", "Second of most recent nav update", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFPINGHEADER, FixTimeHsecond, values, "", "HSecond of most recent nav update", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFPINGHEADER, SensorSpeed, values, "knots", "Speed of towfish", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFPINGHEADER, KP, values, "km", "Kilometers Pipe", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFPINGHEADER, SensorYcoordinate, values, "", "Sonar latitude or northing. Units depends from NavUnit", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFPINGHEADER, SensorXcoordinate, values, "", "sonar longitude or easting. Units depends from NavUnit", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFPINGHEADER, SonarStatus, values, "", "", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFPINGHEADER, RangeToFish, values, "dm", "in decimeters", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFPINGHEADER, BearingToFish, values, "degrees", "", 100, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFPINGHEADER, CableOut, values, "", "", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFPINGHEADER, Layback, values, "", "Tow Cable information. Distance over ground"
                                                                    "from ship to fish.;", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFPINGHEADER, CableTension, values, "", "Tow Cable information. Cable tension"
                                                                         "from ship to fish.;", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFPINGHEADER, SensorDepth, values, "m", "Distance from sea surface to sensor", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFPINGHEADER, SensorPrimaryAltitude, values, "", "", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFPINGHEADER, SensorAuxAltitude, values, "", "", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFPINGHEADER, SensorPitch, values, "degrees", "Pitch (positive=nose up);", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFPINGHEADER, SensorRoll, values, "degrees", "Roll  (positive=roll to starboard);", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFPINGHEADER, SensorHeading, values, "degrees;", "", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFPINGHEADER, Heave, values, "degrees", "Sensors heave at start of ping.", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFPINGHEADER, Yaw, values, "", "", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFPINGHEADER, AttitudeTimeTag, values, "", "", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFPINGHEADER, DOT, values, "", "Distance Off Track", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFPINGHEADER, NavFixMilliseconds, values, "milliseconds", "", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFPINGHEADER, ComputerClockHour, values, "", "", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFPINGHEADER, ComputerClockMinute, values, "", "", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFPINGHEADER, ComputerClockSecond, values, "", "", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFPINGHEADER, ComputerClockHsec, values, "", "", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFPINGHEADER, FishPositionDeltaX, values, "m", "Additional Tow Cable and Fish information"
                                                                                "from Trackpoint.", 3.0, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFPINGHEADER, FishPositionDeltaY, values, "", "Additional Tow Cable and Fish information"
                                                                                "from Trackpoint.", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFPINGHEADER, FishPositionErrorCode, values, "", "Additional Tow Cable and Fish information"
                                                                                  " from Trackpoint.", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFPINGHEADER, OptionalOffsey, values, "", "(Triton 7125 only)", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFPINGHEADER, CableOutHundredths, values, "", "Hundredths of a meter of cable out, to be added"
                                                                                " to the CableOut field.", 1, 0);
}

void
ssc::HDF5::XTF::create_vector (H5::Group & g, std::vector< XTFPINGCHANHEADER > & values)
{
    CREATE_STRUCT_MEM_VECTOR(g, XTFPINGCHANHEADER, ChannelNumber, values, "", "0=port (LF), 1=stbd (lf), 2=port (HF), 3=stbd (Hf)", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFPINGCHANHEADER, DownsampleMethod, values, "", "2 = MAX; 4 = RMS", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFPINGCHANHEADER, SlantRange, values, "m", "", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFPINGCHANHEADER, GroundRange, values, "m", "", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFPINGCHANHEADER, TimeDelay, values, "s", "Amount of time to the start of recorded data.", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFPINGCHANHEADER, TimeDuration, values, "s", "(typically SlantRange/750)", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFPINGCHANHEADER, SecondsPerPing, values, "s", "Amount of time from ping to ping.", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFPINGCHANHEADER, ProcessingFlags, values, "", "0, 4 = TVG; 8 = BAC&GAC; 16 = filter", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFPINGCHANHEADER, Frequency, values, "", "Center transmit frequency", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFPINGCHANHEADER, InitialGainCode, values, "", "Settings as transmitted by sonar", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFPINGCHANHEADER, GainCode, values, "", "Settings as transmitted by sonar", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFPINGCHANHEADER, BandWidth, values, "", "Settings as transmitted by sonar", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFPINGCHANHEADER, ContactNumber, values, "", "", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFPINGCHANHEADER, ContactClassification, values, "", "", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFPINGCHANHEADER, ContactSubNumber, values, "", "", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFPINGCHANHEADER, ContactType, values, "", "", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFPINGCHANHEADER, NumSamples, values, "", "Nb Bytes= Nb Samples X NbBytesPerSample", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFPINGCHANHEADER, MillivoltScale, values, "mv", "Maximum voltage,", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFPINGCHANHEADER, ContactTimeOffTrack, values, "ms", "", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFPINGCHANHEADER, ContactCloseNumber, values, "", "", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFPINGCHANHEADER, FixedVSOP, values, "cm", "This is the fixed, along-track size of each "
                                                                            "ping stored in centimeters", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFPINGCHANHEADER, Weight, values, "", "Weighting factor passed by some sonars", 1, 0);
    //CREATE_STRUCT_MEM_VECTOR(g, XTFPINGCHANHEADER, Reserver4, values, "", "", 1, 0);
}

void
ssc::HDF5::XTF::create_vector (H5::Group & g, std::vector< XTFPOSRAWDATAHEADER > & values)
{
    CREATE_STRUCT_MEM_VECTOR(g, XTFPOSRAWDATAHEADER, MagicNumber, values, "", "", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFPOSRAWDATAHEADER, HeaderType, values, "", "", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFPOSRAWDATAHEADER, SubChannelNumber, values, "", "", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFPOSRAWDATAHEADER, NumChansToFollow, values, "", "", 1, 0);
    //CREATE_STRUCT_MEM_VECTOR(g, XTFHEADERPOSRAWDATA, Reserved1[2], values, "", "", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFPOSRAWDATAHEADER, NumBytesThisRecord, values, "", "", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFPOSRAWDATAHEADER, Year, values, "", "Fix year", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFPOSRAWDATAHEADER, Month, values, "", "Fix month", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFPOSRAWDATAHEADER, Day, values, "", "Fix day", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFPOSRAWDATAHEADER, Hour, values, "", "Fix hour", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFPOSRAWDATAHEADER, Minutes, values, "", "Fix minutes", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFPOSRAWDATAHEADER, Seconds, values, "", "Fix seconds", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFPOSRAWDATAHEADER, MicroSeconds, values, "ms", "fix tenth of milliseconds", 10, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFPOSRAWDATAHEADER, RawYcoordinate, values, "", "Raw position from POSRAW or other time stamped nav source.", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFPOSRAWDATAHEADER, RawXcoordinate, values, "", "Raw position from POSRAW or other time stamped nav source.", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFPOSRAWDATAHEADER, RawAltitude, values, "", "Positive value is nose up", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFPOSRAWDATAHEADER, Pitch, values, "degrees", "Positive value is roll to starboard", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFPOSRAWDATAHEADER, Roll, values, "degrees", "Positive value is sensor up", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFPOSRAWDATAHEADER, Heave, values, "degrees", "", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFPOSRAWDATAHEADER, Heading, values, "degrees", "reported by MRU.", 1, 0);
    // CREATE_STRUCT_MEM_VECTOR(g, XTFHEADERPOSRAWDATA, Reserved2, values, "", "", 1, 0);
}

void
ssc::HDF5::XTF::create_vector (H5::Group & g, std::vector< XTFRAWSERIALHEADER > & values)
{
    CREATE_STRUCT_MEM_VECTOR(g, XTFRAWSERIALHEADER, MagicNumber, values, "", "", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFRAWSERIALHEADER, HeaderType, values, "", "", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFRAWSERIALHEADER, SerialPort, values, "", "Serial port used to receive this data."
                                                                             "COM1=1, COM2=2, 0 if memomry-mapping.", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFRAWSERIALHEADER, NumChansToFollow, values, "", "", 1, 0);
    //CREATE_STRUCT_MEM_VECTOR(g, XTFHEADERPOSRAWDATA, Reserved1[2], values, "", "", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFRAWSERIALHEADER, NumBytesThisRecord, values, "", "", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFRAWSERIALHEADER, Year, values, "", "Fix year", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFRAWSERIALHEADER, Month, values, "", "Fix month", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFRAWSERIALHEADER, Day, values, "", "Fix day", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFRAWSERIALHEADER, Hour, values, "", "Fix hour", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFRAWSERIALHEADER, Minute, values, "", "Fix minutes", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFRAWSERIALHEADER, Second, values, "", "Fix seconds", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFRAWSERIALHEADER, HSeconds, values, "ms", "Hundredth of seconds (0_99)", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFRAWSERIALHEADER, JulianDay, values, "", "Day since Jan 1.", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFRAWSERIALHEADER, TimeTag, values, "ms", "Millisecond timer value.", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFRAWSERIALHEADER, StringSize, values, "", "Number of valid chars in RawAsciiData string", 1, 0);
    // Voir avec L. SIX
    //CREATE_STRUCT_MEM_VECTOR(g, XTFRAWSERIALHEADER, RawAsciiData[StringSize], values, "degrees", "Characters of Raw ASCII data", 1, 0);
}

void
ssc::HDF5::XTF::create_vector (H5::Group & g, std::vector< XTFNAVIGATIONHEADER > & values)
{
    CREATE_STRUCT_MEM_VECTOR(g, XTFNAVIGATIONHEADER, MagicNumber, values, "", "", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFNAVIGATIONHEADER, HeaderType, values, "", "", 1, 0);
    //CREATE_STRUCT_MEM_VECTOR(g, XTFNAVIGATIONHEADER, Reserved [7], values, "", "", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFNAVIGATIONHEADER, NumBytesThisRecord, values, "", "", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFNAVIGATIONHEADER, Year, values, "", "", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFNAVIGATIONHEADER, Month, values, "", "", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFNAVIGATIONHEADER, Day, values, "", "", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFNAVIGATIONHEADER, Hour, values, "", "", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFNAVIGATIONHEADER, Minute, values, "", "", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFNAVIGATIONHEADER, Second, values, "", "", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFNAVIGATIONHEADER, Microseconds, values, "", "", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFNAVIGATIONHEADER, SourceEpoch, values, "", "", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFNAVIGATIONHEADER, TimeTag, values, "", "", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFNAVIGATIONHEADER, RawYCoordinate, values, "", "", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFNAVIGATIONHEADER, RawYCoordinate, values, "", "", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFNAVIGATIONHEADER, RawAltitude, values, "", "", 1, 0);
    CREATE_STRUCT_MEM_VECTOR(g, XTFNAVIGATIONHEADER, TimeFlag, values, "", "", 1, 0);
    //CREATE_STRUCT_MEM_VECTOR(g, XTFNAVIGATIONHEADER, Reserved1, values, "", "", 1, 0);}
}
