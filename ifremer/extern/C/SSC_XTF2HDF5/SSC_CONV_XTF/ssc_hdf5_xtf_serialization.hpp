#ifndef SSC_HDF5_XTF_SERIALIZATION_HPP
#define SSC_HDF5_XTF_SERIALIZATION_HPP

#include "ssc_hdf5_utils.hpp"

#include <H5Cpp.h>
#include <algorithm>
#include "xtf_types.hpp"

namespace ssc {
    namespace HDF5 {
        namespace XTF {
            void add_to_group_property (H5::Group &, XTFFILEHEADER const &);
            void add_to_group_property (H5::Group &, CHANINFO const &);

            void
            create_vector (H5::Group & g, std::vector< XTFATTITUDEDATA > & values);
            void
            create_vector (H5::Group & g, std::vector< XTFPINGCHANHEADER > & values);
            void
            create_vector (H5::Group & g, std::vector< XTFPINGHEADER > & values);
            void
            create_vector (H5::Group & g, std::vector< XTFPOSRAWDATAHEADER > & values);
            void
            create_vector (H5::Group & g, std::vector< XTFRAWSERIALHEADER > & values);
            void
            create_vector (H5::Group & g, std::vector< XTFNAVIGATIONHEADER > & values);
        }
    }
}

#endif
