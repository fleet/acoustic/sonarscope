#include "ssc_time.hpp"

#include <boost/date_time/gregorian/gregorian_types.hpp>

ssc::time::datetime
ssc::time::norm_date (int year,
                      int month,
                      int day,
                      int hour,
                      int minutes,
                      int secondes,
                      int mil_sec)
{
    const boost::gregorian::date d(year, month, day);
    static const boost::gregorian::date ref(1970, 1, 1);
    static const long millisec_per_day {24 * 60 * 60 * 1000};

    const boost::gregorian::date_duration dur {d - ref};
    const long milli_since_midnight {hour * 60 * 60 * 1000 +
                                     minutes * 60 * 1000 +
                                     secondes * 1000 +
                                     mil_sec};
    const double day_portion {static_cast<double> (milli_since_midnight) /
                              static_cast<double> (millisec_per_day)};

    return (static_cast<double>(dur.days ())) + day_portion;
}
