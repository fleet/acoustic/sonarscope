#ifndef SSC_TIME_HPP
#define SSC_TIME_HPP

namespace ssc {
    namespace time {
        using datetime = double;
         datetime norm_date (int year,
                             int month,
                             int day,
                             int hour,
                             int minutes,
                             int secondes,
                             int mil_sec);
    }
}

#endif // SSC_TIME_HPP
