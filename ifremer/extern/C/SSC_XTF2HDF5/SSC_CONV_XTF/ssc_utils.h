#include <vector>
#include <stdlib.h>
#include <exception>

namespace ssc {
namespace utils {

    class swap_error : public std::exception {};

    /** Modify a vector of T by performing a swap as it was a vector of U
     */
    template <typename T, typename U>
    void
    swap_vector (std::vector<T> & vec) {
        U * fst = reinterpret_cast<U*>(&vec[0]);
        U * lst = reinterpret_cast<U*>(&vec[vec.size () - 1] + sizeof (T) - sizeof (U));
        while (fst < lst)
            std::swap(*(fst++), *(lst--));
    }

    /** \brief Swaps elements in a vector on a arbitrary byte-group basis
     *
     * Given a vector of arbitrary elements, swap its data as if elements had
     * size elem_size.
     *
     * For exemple, [ 0x001 0x002 0x003 0x004 ] can be swapped as if elements
     * had size 2 to obtain the [ 0x003 0x004 0x001 0x002 ] by calling
     * swap_vector (v, 2)
     */
    template <typename T>
    void
    swap_vector (std::vector<T> & vec, size_t elem_size)
    {
        switch (elem_size) {
            case 1:
                static_assert (sizeof (char) == 1, "Portability error");
                swap_vector<T, char> (vec);
                break;
            case 2:
                static_assert (sizeof (short) == 2, "Portability error");
                swap_vector<T, short> (vec);
                break;
            case 4:
                static_assert (sizeof (int) == 4, "Portability error");
                swap_vector<T, int> (vec);
                break;
            case 8:
                static_assert (sizeof (long) == 8, "Portability error");
                swap_vector<T, long> (vec);
                break;
            default:
                char tmp [elem_size];

                if (vec.size () * sizeof (T) % elem_size != 0)
                    throw swap_error();

                char * fst = reinterpret_cast<char *>(&vec[0]);
                char * lst = reinterpret_cast<char *>(&vec[vec.size () - 1] + sizeof (T) - elem_size);

                while (fst < lst) {
                    for (size_t i = 0; i < elem_size; i++) {
                        tmp[i] = *(fst + i);
                        *(fst + i) = *(lst + i);
                        *(lst + i) = tmp[i];
                    }

                    fst += elem_size;
                    lst -= elem_size;
                }
        }
    }

}
} // ssc
