#include "config.hpp"

#include <iostream>

#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>

#include "xtf_decoder.hpp"

int
main (int argc, char** argv)
{
    boost::program_options::options_description desc ("Allowed arguments");
    desc.add_options ()
        ("help,h", "print help message")
        ("version,v", "print program version")
        ("xtf_file,i", boost::program_options::value< std::string >()->required (),
         "XTF file to decompose" )
        ("out,o", boost::program_options::value< std::string >(),
         "Folders where inidvidual packets art stored")
        ("force,F", "Force overriding of output folder if already present");

    boost::program_options::positional_options_description pos;
    pos.add ("xtf_file", 1).
        add ("out", 1);

    boost::program_options::variables_map vm;
    boost::program_options::store (
        boost::program_options::command_line_parser (argc, argv).
            options (desc).
            positional (pos).
            run (),
            vm
    );

    if (argc < 2 || vm.count ("help")) {
        std::cout << argv[0] << " <input_xtf> <outut>" << std::endl << std::endl;
        std::cout << PROJECT_NAME << "-" << PROJECT_VERSION << std::endl << std::endl;
        std::cout << desc << std::endl;
        return 1;
    };

    if (vm.count ("version")) {
        std::cout << PROJECT_NAME << "-" << PROJECT_VERSION << std::endl;
        return 0;
    }

    boost::program_options::notify (vm);

    boost::filesystem::path input_file (vm["xtf_file"].as< std::string >());

    boost::filesystem::path out_folder {input_file.parent_path () / (input_file.stem ().string ())};
    if (vm.count ("out")) {
        out_folder = vm["out"].as< std::string >();
    }

    if (vm.count ("force") && boost::filesystem::exists (out_folder)) {
        boost::filesystem::remove_all (out_folder);
    }

    if (boost::filesystem::create_directory (out_folder)) {
        ssc::xtf::tools::XTFBinaryDecomposer decomposer (input_file.string (),
                                                         out_folder.string ());
        return decomposer && decomposer.decode_file (nullptr);
    }
}
