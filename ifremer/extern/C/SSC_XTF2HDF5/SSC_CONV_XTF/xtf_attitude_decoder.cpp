#include "xtf_attitude_decoder.hpp"

#include <iostream>
#include <array>
#include <vector>
#include <algorithm>
#include <cassert>

#include "ssc_hdf5_xtf_serialization.hpp"

ssc::xtf::AttitudeChunkDecoder::AttitudeChunkDecoder (H5::H5File & f) :
    ::ssc::HDF5::HDFProducer(f),
    _att_count {0},
    _data {}
{
}

void
ssc::xtf::AttitudeChunkDecoder::_preprocess (std::fstream & stream,
                                                   BYTE header_type)
{
    _att_count++;
    _skip_current (stream);
}

void
ssc::xtf::AttitudeChunkDecoder::_preprocess_done ()
{
    _data.reserve (_att_count);
}

void
ssc::xtf::AttitudeChunkDecoder::_process (std::fstream & stream,
                                                BYTE header_type)
{
    using namespace ssc::xtf;
    XTFATTITUDEDATA head_data {0xFACE, header_type, 0};
    _read(stream, reinterpret_cast<char *>(&head_data.SubChannelNumber),
          sizeof(XTFATTITUDEDATA) - sizeof(XTFCHUNKPRELUDE));
    _data.push_back (head_data);
}

void
ssc::xtf::AttitudeChunkDecoder::_terminate ()
{
    // No attitude data. Leaving
    if (_data.size () < 1)
        return;

    H5::Group att_grp (_out_file.createGroup ("/Attitude"));
    ssc::HDF5::XTF::create_vector (att_grp, _data);
}

ssc::xtf::AttitudeChunkDecoder::~AttitudeChunkDecoder ()
{
}
