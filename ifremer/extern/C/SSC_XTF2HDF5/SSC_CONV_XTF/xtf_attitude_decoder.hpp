#ifndef XTF_ATTITUDE_DECODER_HPP
#define XTF_ATTITUDE_DECODER_HPP

#include "ssc_hdf5_utils.hpp"
#include "xtf_types.hpp"
#include "ssc_abstract_decoder.hpp"

#include <vector>

namespace ssc {
    namespace xtf {
        class AttitudeChunkDecoder : public AbstractDecoder,
                                     public ::ssc::HDF5::HDFProducer {
          private:
            size_t _att_count;
            std::vector<XTFATTITUDEDATA> _data;
          public:
            explicit AttitudeChunkDecoder (H5::H5File & f);
            ~AttitudeChunkDecoder ();

          protected:
            void _preprocess (std::fstream &, BYTE);
            void _preprocess_done ();
            void _process (std::fstream &, BYTE);
            void _terminate ();

        };
    } // xtf
} // ssc

#endif // XTF_ATTITUDE_DECODER_HPP
