#include "xtf_bathy_decoder.hpp"
#include "ssc_hdf5_xtf_serialization.hpp"

#include <iostream>
#include <algorithm>
#include <sstream>

ssc::xtf::BathyChunkDecoder::BathyChunkDecoder(H5::H5File & f,
                                               XTFFILEHEADER const & xtf_head,
                                               std::vector< CHANINFO > const & chs) :
    AbstractDecoder (),
    ::ssc::HDF5::HDFProducer (f),
    _system_type {xtf_head.SystemType},
    _stat ()
{
    // Get the subset of CHANINFO that refer to bathy data
    std::copy_if(chs.begin (), chs.end (), std::back_inserter (_bathy_chan_infos),
                 [](CHANINFO const & c) -> bool { return c.TypeOfChannel == CHANNEL_TYPE_BATHY; });
}


void
ssc::xtf::BathyChunkDecoder::_preprocess(std::fstream & st, BYTE header_type)
{
    // D�claration et initialisation des deux 1ers champs + reste � 0
    XTFBATHHEADER head_data {0xFACE, header_type, 0};

    // Lecture du fichier � partir du 3�me champ.
    _read(st,
          reinterpret_cast<char *>(&head_data.SubChannelNumber),
          sizeof (XTFBATHHEADER) - sizeof (XTFCHUNKPRELUDE));

    if (head_data.SubChannelNumber >= _bathy_chan_infos.size ()) {
        std::cerr << "Found BATHY channel " << static_cast< int > (head_data.SubChannelNumber)
                  << " not declared in the CHANINFO structure. Skipping " ;
        std::cerr << "headerType" << static_cast< int > (header_type) << std::endl;
    } else {
        // Get the stat or insert a new one if not already present
        auto it = _stat.insert (
            std::pair<BYTE const, BathyChunkStat> (
                head_data.SubChannelNumber,
                BathyChunkStat (_bathy_chan_infos.at (head_data.SubChannelNumber), header_type)
            )
        ).first;
        BathyChunkStat & curr_stat {it->second};
        curr_stat.count++;
        // TODO check if there is some 0-padding added at the end of the
        // data
        curr_stat.max_dim_byte = std::max (
            curr_stat.max_dim_byte,
            head_data.NumBytesThisRecord - sizeof (XTFBATHHEADER)
        );
    }

    _seek (st, head_data.NumBytesThisRecord - sizeof (XTFBATHHEADER));
}

void
ssc::xtf::BathyChunkDecoder::_preprocess_done ()
{
    if (_stat.size () < 1)
        return;

    H5::Group sonar_group {ssc::HDF5::create_or_open_group(_out_file, "Sonar")};
    std::stringstream system_group_name;
    system_group_name << static_cast< int > (_system_type);
    H5::Group system_group {ssc::HDF5::create_or_open_group (sonar_group, system_group_name.str ().c_str ())};
    H5::Group bathy_group {ssc::HDF5::create_or_open_group(system_group, "PingBeams")};
    // Initialize data in the outout HDF5 file
    std::cout << "Found " << _stat.size () << " bathy channels" << std::endl;

    for (std::pair<BYTE const, BathyChunkStat> & stat : _stat) {
        std::stringstream group_name;
        group_name << "SonarHeaderType" << static_cast< int > (stat.second.header_type)
                   << "ChanNum" << static_cast< int > (stat.first);
        stat.second.group = bathy_group.createGroup (group_name.str ());

        ssc::HDF5::XTF::add_to_group_property (stat.second.group, stat.second.chan_info);

        stat.second.headers.reserve (stat.second.count);

        stat.second.max_dim = stat.second.max_dim_byte / stat.second.chan_info.BytesPerSample;
        // Create the 2D bathy matrice
        std::array<hsize_t, 2> mat_dims {stat.second.count,
                                         stat.second.max_dim};
        H5::DataSpace bathyDspace (mat_dims.size (), &mat_dims[0]);
        stat.second.dataset = stat.second.group.createDataSet ("Bathy",
                                                               stat.second.tpe,
                                                               bathyDspace);
        stat.second.count = 0;
    }
}

void
ssc::xtf::BathyChunkDecoder::_process(std::fstream & st, BYTE header_type)
{

    // D�claration et initialisation des deux 1ers champs + reste � 0
    XTFBATHHEADER head_data {0xFACE, header_type, 0};

    // Lecture du fichier � partir du 3�me champ.
    _read(st,
          reinterpret_cast<char *>(&head_data.SubChannelNumber),
          sizeof (XTFBATHHEADER) - sizeof (XTFCHUNKPRELUDE));

    auto it = _stat.find (head_data.SubChannelNumber);
    if (it != _stat.end ()) {
        BathyChunkStat & stat {it->second};
        std::array<hsize_t, 2> memsdims {1, stat.max_dim};
        std::array<hsize_t, 2> offset {stat.count, 0};
        std::array<hsize_t, 2> count {1, stat.max_dim};
        H5::DataSpace memspce (2, &memsdims[0]);

        // TODO we assume that the endianness is the same between the file
        // and the processing workstation
        std::vector< BYTE > ping (stat.max_dim_byte, 0.0);

        _read(st,
              reinterpret_cast<char *>(&ping[0]),
              head_data.NumBytesThisRecord - sizeof (XTFBATHHEADER));

        std::stringstream group_name;
        H5::DataSpace spce = stat.dataset.getSpace ();
        spce.selectHyperslab(H5S_SELECT_SET, &count[0], &offset[0], nullptr, nullptr);
        stat.dataset.write (&ping[0],
                            stat.tpe,
                            memspce, spce);

        stat.headers.push_back (head_data);
        stat.count++;
    } else {
        _seek (st, head_data.NumBytesThisRecord - sizeof (XTFBATHHEADER));
    }
}

void
ssc::xtf::BathyChunkDecoder::_terminate ()
{
    if (_stat.size () < 1)
        return;

    for (std::pair<BYTE const, BathyChunkStat> & stat : _stat) {
        ssc::HDF5::XTF::create_vector (stat.second.group, stat.second.headers);
        stat.second.dataset.close ();
        stat.second.group.close ();
    }
}

ssc::xtf::BathyChunkDecoder::~BathyChunkDecoder ()
{
}
