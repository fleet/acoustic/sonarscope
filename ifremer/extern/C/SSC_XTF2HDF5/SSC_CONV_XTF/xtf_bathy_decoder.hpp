#ifndef XTF_BATHY_DECODER_HPP
#define XTF_BATHY_DECODER_HPP

#include "ssc_abstract_decoder.hpp"
#include "ssc_hdf5_utils.hpp"

#include <map>
#include <stdexcept>

namespace ssc {
    namespace xtf {
        class BathyChunkDecoder : public AbstractDecoder,
                                  public ::ssc::HDF5::HDFProducer {
          private:
            struct BathyChunkStat {
                size_t count;
                size_t max_dim;
                size_t max_dim_byte;
                CHANINFO const & chan_info;
                H5::DataType tpe;
                H5::Group group;
                H5::DataSet dataset;
                BYTE const header_type;
                std::vector< XTFBATHHEADER > headers;
                BathyChunkStat(CHANINFO const & c,
                               BYTE const header_type) : count {0},
                                                         max_dim {0},
                                                         max_dim_byte {0},
                                                         chan_info {c},
                                                         header_type{header_type}
                {
                    switch (c.BytesPerSample) {
                        case 1:
                            tpe = H5::PredType::STD_U8LE;
                            break;
                        case 2:
                            tpe = H5::PredType::STD_U16LE;
                            break;
                        case 4:
                            tpe = H5::PredType::STD_U32LE;
                            break;
                        default:
                            throw std::runtime_error ("Invalid BytesPerSample for bathy channel");
                    }
                };
            };
            BYTE const _system_type;
            std::vector< CHANINFO > _bathy_chan_infos;
            std::map<BYTE, BathyChunkStat> _stat;
          public:
            explicit BathyChunkDecoder (H5::H5File &, XTFFILEHEADER const &, std::vector< CHANINFO > const &);
            ~BathyChunkDecoder ();
          protected:
            void _preprocess (std::fstream & st, BYTE header_type);
            void _preprocess_done ();
            void _process (std::fstream & st, BYTE header_type);
            void _terminate ();
        };

    }; // xtf
}; // ssc

#endif // XTF_BATHY_DECODER_HPP
