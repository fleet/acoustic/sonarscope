#include "config.hpp"
#include "xtf_decoder.hpp"

#include <iostream>
#include <algorithm>
#include <map>
#include <array>
#include <functional>
#include <sstream>
#include <iomanip>


#include "xtf_attitude_decoder.hpp"
#include "xtf_rawposdata_decoder.hpp"
#include "xtf_rawserialdata_decoder.hpp"
#include "xtf_navdata_decoder.hpp"
#include "xtf_bathy_decoder.hpp"
#include "xtf_unknown_chunk_decoder.hpp"
#include "xtf_sonar_decoder.hpp"
#include "ssc_hdf5_utils.hpp"
#include "ssc_hdf5_xtf_serialization.hpp"


ssc::xtf::XTFBaseDecoder::XTFBaseDecoder (boost::filesystem::path const xtf_filename)
    : _file (xtf_filename.string (),
             std::fstream::in | std::fstream::binary)
{
}

ssc::xtf::XTFBaseDecoder::operator bool()
{
    return bool(_file);
}

void
ssc::xtf::XTFBaseDecoder::_parse_file_header ()
{
    _file.read (reinterpret_cast<char *>(&_f_header),
                sizeof(XTFFILEHEADER));
    
    size_t num_of_chaninfo {static_cast<size_t>(_f_header.NumberOfSonarChannels +
                                                _f_header.NumberOfBathymetryChannels)};

    if (num_of_chaninfo <= 6) {
        _chaninfos.assign(_f_header.ChanInfo,
                          _f_header.ChanInfo + num_of_chaninfo);
    } else {
        // Copy the chaninfo already read in the vector
        _chaninfos.assign(_f_header.ChanInfo,
                          _f_header.ChanInfo + 6);

        int remaining_chaninfo {static_cast<int>(num_of_chaninfo) - 6};
        while (remaining_chaninfo > 0) {
            // Read by blocks of 1026 bytes
            constexpr int chaninfo_per_block = 8;
            constexpr size_t extra_chaninfo_seg_size = chaninfo_per_block * sizeof(CHANINFO);
            static_assert(extra_chaninfo_seg_size == 1024,
                          "Invalid number of block per extra-chaninfo segment");
            std::array<CHANINFO, chaninfo_per_block> readbuf;
            static_assert(sizeof(readbuf) == extra_chaninfo_seg_size,
                          "Invalid buffer size for extra chan-info decoding");

            _file.read(reinterpret_cast<char *>(&readbuf[0]), extra_chaninfo_seg_size);
            std::copy(readbuf.begin (), readbuf.begin () + remaining_chaninfo + 1,
                      std::back_inserter(_chaninfos));
            remaining_chaninfo -= chaninfo_per_block;
        }
    }

    std::cout << "Header summary:" << std::endl
              << "\tRocorded by " << _f_header.RecordingProgramName
              << "-" << _f_header.RecordingProgramVersion << std::endl
              << "\tSonar name: " << _f_header.SonarName
              << " (type=" << _f_header.SonarType << ")" << std::endl
              << "\tThisFileName: " << _f_header.ThisFileName << std::endl
              << "\tNote: " << _f_header.NoteString << std::endl
              << "\tNumber of sonar channels: " << static_cast<int>(_f_header.NumberOfSonarChannels)
              << std::endl
              << "\tNumber of bathy channels: " << static_cast<int>(_f_header.NumberOfBathymetryChannels)
              << std::endl
              << "\tNumber of snippet channels: " << static_cast<int>(_f_header.NumberOfSnippetsChannels)
              << std::endl
              << "\tNumber of forward looking arrays: " << static_cast<int>(_f_header.NumBerOfForwardLookArrays)
              << std::endl
              << "\tNumber of echo strength channels: " << static_cast<int>(_f_header.NumberOfEchoStrengthChannels)
              << std::endl
              << "\tNumber of interferometry channels: " << static_cast<int>(_f_header.NumberOfInterferometryChannels)
              << std::endl;
    for (auto chaninf : _chaninfos) {
        std::cout << "\tChanInfo: " << chaninf.ChannelName << std::endl;
        std::cout << "\t\tTypeOfChannel: " << (int) chaninf.TypeOfChannel << std::endl;
        std::cout << "\t\tSubChannelNumber: " << (int) chaninf.SubChannelNumber << std::endl;
        std::cout << "\t\tFrequency: " << (int) chaninf.Frequency << std::endl;
        std::cout << "\t\tCorrectionFlags: " << (int) chaninf.CorrectionFlags << std::endl;
        std::cout << "\t\tUniPolar: " << (int) chaninf.UniPolar << std::endl;
        std::cout << "\t\tBytesPerSample: " << (int) chaninf.BytesPerSample << std::endl;
    }
}

ssc::xtf::XTFDecoder::XTFDecoder (boost::filesystem::path const xtf_filename,
                                  boost::filesystem::path const dest_hdf)
    : XTFBaseDecoder (xtf_filename),
      _out_file (dest_hdf.string (), H5F_ACC_EXCL)
{
}

ssc::xtf::XTFDecoder::~XTFDecoder ()
{
}

bool
ssc::xtf::XTFDecoder::decode_file (void(*progress_cb)(int))
{
    // Parse the header
    _parse_file_header ();
    _add_metadata_to_hdf ();
    size_t file_size {0};
    auto start_of_chunks = _file.tellg ();

    // Declare here the set of decoders we will use. They will be initialized
    // with the already parsed header, and will be released by the end of this
    // procedure;
    std::map<BYTE const, std::shared_ptr< AbstractDecoder > const > const decoders {
        {0, std::make_shared< SonarChunkDecoder > (_out_file, _f_header, _chaninfos)},
        {2, std::make_shared< BathyChunkDecoder > (_out_file, _f_header, _chaninfos)},
        {3, std::make_shared< AttitudeChunkDecoder > (_out_file)},
        {25, std::make_shared< BathyChunkDecoder > (_out_file, _f_header, _chaninfos)},
        {28, std::make_shared< BathyChunkDecoder > (_out_file, _f_header, _chaninfos)},
        {42, std::make_shared< NavDataChunkDecoder > (_out_file)},
        {65, std::make_shared< BathyChunkDecoder > (_out_file, _f_header, _chaninfos)},
        {66, std::make_shared< BathyChunkDecoder > (_out_file, _f_header, _chaninfos)},
        {67, std::make_shared< BathyChunkDecoder > (_out_file, _f_header, _chaninfos)},
        {107, std::make_shared< RawPosDataChunkDecoder > (_out_file)},
    };
    std::shared_ptr< AbstractDecoder > const default_decoder {new UnknownChunkDecoder ()};

    auto parse_file = [&decoders,&default_decoder,this]
                      (std::function<void(std::shared_ptr<AbstractDecoder> const &,
                                          std::fstream &,
                                          BYTE)> op,
                       std::function<void(size_t)> progress) {
        while (_file) {
            XTFCHUNKPRELUDE prelude;

            // Read migic number and HeaderType
            _file.read(reinterpret_cast<char *>(&prelude), sizeof(XTFCHUNKPRELUDE));
            if (prelude.magic_number != 0xFACE) {
                std::cerr << "Invalid synchronisation bits. Aborting file processing";
                return false;
            }

            // Find the decoder to use de process the current datagram
            auto it = decoders.find (prelude.header_type);
            if (it != decoders.end ()) {
                op(it->second, _file, prelude.header_type);
            } else {
                op(default_decoder, _file, prelude.header_type);
            }
            progress (_file.tellg ());
        }
        return true;
    };


    std::cout << "Pre-analyze step..." << std::endl;
    try {    
        // First pass.
        //
        // Each decoder will have the opportunity to access all the datagrams
        // in order to fetch some statistitcs. This is mainly done in order to
        // determine the dimensions of the variables that will be produced.
        parse_file(
            [](std::shared_ptr<AbstractDecoder> const & dec,
               std::fstream & stream,
               BYTE head) {
                dec->preprocess (stream, head);
            },
            [&file_size](size_t c) { file_size = std::max (c, file_size); }
        );
    } catch (StreamError const &) {
        std::cerr << "Got error while pre-processing the stream. Continuing anyway..." << std::endl;
    }

    std::for_each (decoders.begin (), decoders.end (),
                   [](std::pair<BYTE const, std::shared_ptr< AbstractDecoder > const> const & dec) {
        dec.second->preprocess_done ();
    });
    default_decoder->preprocess_done ();

    // Go back to the begining of the file
    _file.clear ();
    _file.seekg (start_of_chunks, _file.beg);

    std::cout << "Parsing step..." << std::endl;
    try {    
        // Second pass
        //
        // Actually do the decoding of all the datagrams and store the resurt
        // on dist as the stream goes.
        parse_file(
            [](std::shared_ptr<AbstractDecoder> const & dec,
               std::fstream & stream,
               BYTE head) {
                dec->process (stream, head);
            },
            [file_size,progress_cb](size_t c) { progress_cb (static_cast<float>(c) / static_cast<float>(file_size) * 100); }
        );
    } catch (StreamError const &) {}

    std::cout << "Ending step..." << std::endl;
    std::for_each (decoders.begin (), decoders.end (),
                   [](std::pair<BYTE const, std::shared_ptr< AbstractDecoder > const> const & dec) {
        dec.second->terminate ();
    });
    default_decoder->terminate ();

    return true;
}

void
ssc::xtf::XTFDecoder::_add_metadata_to_hdf ()
{
    Group rtGrp {_out_file.openGroup ("/")};
    rtGrp.setComment ("Created by " PROJECT_NAME "-" PROJECT_VERSION);
    ::ssc::HDF5::add_str_attribute (rtGrp, "FormatVersion", "0.1");

    ssc::HDF5::XTF::add_to_group_property (rtGrp, _f_header);
}

/****************************************
 *                                      *
 * The ssc::xtf::tools implementations  *
 *                                      *
 ****************************************/

ssc::xtf::tools::XTFBinaryDecomposer::XTFBinaryDecomposer (boost::filesystem::path const xtf_filename,
                                                           boost::filesystem::path const out_folder)
    : XTFBaseDecoder (xtf_filename),
      _out_folder {out_folder}
{
    std::cout << "Decoding " << xtf_filename.string () << " in folder " << _out_folder.string () << std::endl;
}

bool
ssc::xtf::tools::XTFBinaryDecomposer::decode_file (void(*)(int))
{
    _parse_file_header ();

    {
        std::ofstream head_dump ((_out_folder / "header.bin").string (),
                                 std::fstream::out | std::fstream::binary);
        head_dump.write (reinterpret_cast<char *>(&_f_header),
                         sizeof(XTFFILEHEADER));
    }

    //TODO check file size to implement progress bar

    for (size_t i = 0; i < _chaninfos.size (); i++) {
        std::ostringstream chan_name;
        chan_name << "Chan_" << std::setfill ('0') << std::setw (3)
                  << static_cast<int> (i) << ".bin";
        std::ofstream chan_dump ((_out_folder / chan_name.str ()).string (),
                                 std::fstream::out | std::fstream::binary);
        chan_dump.write (reinterpret_cast<char *>(&_chaninfos[i]),
                         sizeof(CHANINFO));

    }

    size_t current_chunk {0};
    while (_file) {
        // Read the datagram
        XTFEXTENTEDCHUNKPRELUDE prelude;
        _file.read (reinterpret_cast<char *>(&prelude), sizeof(XTFEXTENTEDCHUNKPRELUDE));
        std::vector<char> content;
        content.resize (prelude.rec_size);
        _file.read (&content[sizeof(XTFEXTENTEDCHUNKPRELUDE)], prelude.rec_size - sizeof(XTFEXTENTEDCHUNKPRELUDE));
        *reinterpret_cast<XTFEXTENTEDCHUNKPRELUDE *> (&content[0]) = prelude;

        // find where to save it
        std::ostringstream type_name, chunk_name;
        type_name << std::setfill ('0') << std::setw (3)
                  << static_cast<int> (prelude.header_type);
        boost::filesystem::path type_path = _out_folder / type_name.str ();
        if (!boost::filesystem::exists (type_path))
            boost::filesystem::create_directory (type_path);
        chunk_name << std::setfill ('0') << std::setw (7)
                   << ++current_chunk << ".bin";
        std::ofstream chan_dump ((type_path / chunk_name.str ()).string (),
                                 std::fstream::out | std::fstream::binary);
        chan_dump.write (&content[0], prelude.rec_size);
    }
    return true;
}
