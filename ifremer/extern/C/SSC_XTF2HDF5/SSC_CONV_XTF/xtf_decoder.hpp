#ifndef XTF_DECODER_HPP
#define XTF_DECODER_HPP

#include <fstream>
#include <vector>
#include <memory>
#include <functional>

#include <boost/filesystem.hpp>
#include <H5Cpp.h>

#include "xtf_types.hpp"

#ifndef H5_NO_NAMESPACE
    using namespace H5;
#endif

namespace ssc {
    namespace xtf {

        /** \brief The XTFBaseDecoder class provides minimum structure to read
         *  XTF file
         */
        class XTFBaseDecoder {
          protected:
            std::fstream _file;
            XTFFILEHEADER _f_header;
            std::vector< CHANINFO > _chaninfos;

            void _parse_file_header ();

          protected:
            explicit XTFBaseDecoder (boost::filesystem::path const xtf_filename);

          public:
            /** \brief Check if the Decoder is in a good state for decoding
             */
            operator bool();
            virtual bool decode_file (void(*)(int)) = 0;
        };

        /** \brief The XTFDecoder can transform a XTF file into a HDF5 based file
         */
        class XTFDecoder : public XTFBaseDecoder {
          protected:
            H5File _out_file;

          public:
            /** \brief Initialize file decoding and dest folder layout
             */
            explicit XTFDecoder (boost::filesystem::path const xtf_filename,
                                 boost::filesystem::path const dest_hdf);

            /** \brief Releases associaded resources
             */
            ~XTFDecoder ();

            /** \brief Read the entire XTF file and decode it qo disk
             */
            bool decode_file (void(*)(int));
        
          protected:
            void _add_metadata_to_hdf ();
        };

        /** \brief ssc::xtf::tools provides class to implement developper oriented toos
         */
        namespace tools {
            /** \brief The XTFBinaryDecoder decomposes a XTF file into small binary
             * files, one file per datagram */
            class XTFBinaryDecomposer : public XTFBaseDecoder {
              private:
                  boost::filesystem::path const _out_folder;

              public:
                explicit XTFBinaryDecomposer (boost::filesystem::path const xtf_filename,
                                              boost::filesystem::path const out_folder);

                bool decode_file (void(*)(int));
            };
        }
    }
}

#endif
