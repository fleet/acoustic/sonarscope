#include "xtf_rawposdata_decoder.hpp"

#include <iostream>
#include <array>
#include <vector>
#include <algorithm>
#include <cassert>

#include "ssc_hdf5_xtf_serialization.hpp"

ssc::xtf::RawPosDataChunkDecoder::RawPosDataChunkDecoder (H5::H5File & f) :
    ::ssc::HDF5::HDFProducer(f),
    _rawpos_count {0},
    _data {}
{
}

void
ssc::xtf::RawPosDataChunkDecoder::_preprocess (std::fstream & stream,
                                                   BYTE header_type)
{
    _rawpos_count++;
    _skip_current (stream);
}

void
ssc::xtf::RawPosDataChunkDecoder::_preprocess_done ()
{
    _data.reserve (_rawpos_count);
}

void
ssc::xtf::RawPosDataChunkDecoder::_process (std::fstream & stream,
                                                BYTE header_type)
{
    // D�claration et initialisation des deux 1ers champs + reste � 0
    XTFPOSRAWDATAHEADER head_data {0xFACE, header_type, 0};

    // Lecture du fichier � partir du 3�me champ.
    _read(stream, reinterpret_cast<char *>(&head_data.SubChannelNumber),
          sizeof(XTFPOSRAWDATAHEADER) - sizeof(XTFCHUNKPRELUDE));
    _data.push_back (head_data);
}

void
ssc::xtf::RawPosDataChunkDecoder::_terminate ()
{
    // No attitude data. Leaving
    if (_data.size () < 1)
        return;

    H5::Group rawposdata_grp (_out_file.createGroup ("Navigation"));
    ssc::HDF5::XTF::create_vector (rawposdata_grp, _data);
}

ssc::xtf::RawPosDataChunkDecoder::~RawPosDataChunkDecoder ()
{
}
