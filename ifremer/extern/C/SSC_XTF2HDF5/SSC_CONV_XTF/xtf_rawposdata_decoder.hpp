#ifndef XTF_RAWPOSDATA_DECODER_HPP
#define XTF_RAWPOSDATA_DECODER_HPP

#include "ssc_hdf5_utils.hpp"
#include "xtf_types.hpp"
#include "ssc_abstract_decoder.hpp"

#include <vector>

namespace ssc {
    namespace xtf {
        class RawPosDataChunkDecoder : public AbstractDecoder,
                                     public ::ssc::HDF5::HDFProducer {
          private:
            size_t _rawpos_count;
            std::vector<XTFPOSRAWDATAHEADER> _data;
          public:
            explicit RawPosDataChunkDecoder (H5::H5File & f);
            ~RawPosDataChunkDecoder ();

          protected:
            void _preprocess (std::fstream &, BYTE);
            void _preprocess_done ();
            void _process (std::fstream &, BYTE);
            void _terminate ();

        };
    } // xtf
} // ssc

#endif // XTF_RAWPOSDATA_DECODER_HPP
