#include "xtf_rawserialdata_decoder.hpp"

#include <iostream>
#include <array>
#include <vector>
#include <algorithm>
#include <cassert>

#include "ssc_hdf5_xtf_serialization.hpp"

ssc::xtf::RawSerialDataChunkDecoder::RawSerialDataChunkDecoder (H5::H5File & f) :
    ::ssc::HDF5::HDFProducer(f),
    _rawserial_count {0},
    _data {}
{
}

void
ssc::xtf::RawSerialDataChunkDecoder::_preprocess (std::fstream & stream,
                                                   BYTE header_type)
{
    _rawserial_count++;
    _skip_current (stream);
}

void
ssc::xtf::RawSerialDataChunkDecoder::_preprocess_done ()
{
    _data.reserve (_rawserial_count);
}

void
ssc::xtf::RawSerialDataChunkDecoder::_process (std::fstream & stream,
                                                BYTE header_type)
{
    //using namespace ssc::xtf;
    // D�claration et initialisation des deux 1ers champs + reste � 0
    XTFRAWSERIALHEADER head_data {0xFACE, header_type, 0};

    // Lecture du fichier � partir du 3�me champ.
    _read(stream, reinterpret_cast<char *>(&head_data.SerialPort),
          sizeof(XTFRAWSERIALHEADER) - sizeof(XTFCHUNKPRELUDE));
    _data.push_back (head_data);
}

void
ssc::xtf::RawSerialDataChunkDecoder::_terminate ()
{
    // No attitude data. Leaving
    if (_data.size () < 1)
        return;

    H5::Group rawserialdata_grp (_out_file.createGroup ("/Navigation"));
    ssc::HDF5::XTF::create_vector (rawserialdata_grp, _data);
}

ssc::xtf::RawSerialDataChunkDecoder::~RawSerialDataChunkDecoder ()
{
}
