#include "xtf_sonar_decoder.hpp"

#include <exception>
#include <cassert>
#include <iostream>
#include <algorithm>
#include <sstream>

#include <H5Cpp.h>

#include "ssc_hdf5_xtf_serialization.hpp"

ssc::xtf::SonarChunkDecoder::SonarChunkDecoder (H5::H5File & out_f,
                                                XTFFILEHEADER const & xtfhead,
                                                std::vector< CHANINFO > const & chaninfos)
    : AbstractDecoder (),
      ::ssc::HDF5::HDFProducer (out_f),
      _system_type {xtfhead.SystemType},
      _ping_count {0},
      _stats {},
      _headers {},
      _chaninfos {chaninfos}
{
}

void
ssc::xtf::SonarChunkDecoder::_preprocess (std::fstream & st,
                                          BYTE header_type)
{
    XTFPINGHEADER head;
    size_t already_read {0};
    size_t i;

    _read (st,
           reinterpret_cast<char *>(&head.SubChannelNumber),
           sizeof (XTFPINGHEADER) - sizeof (XTFCHUNKPRELUDE));
    already_read += sizeof (XTFPINGHEADER);
    _ping_count++;

    for (i = 0; i < head.NumChansToFollow ; i++) {
        XTFPINGCHANHEADER chanhead;
        size_t data_size;
        this->_read (st, reinterpret_cast<char *>(&chanhead), sizeof (XTFPINGCHANHEADER));

        auto it = _stats.find (chanhead.ChannelNumber);
        if (it == _stats.end ()) {
            auto chIt = _chaninfos.begin ();
            // Ajout du chaninfo a notre structure de fichier
            for (; chIt < _chaninfos.end (); chIt ++) {
                if (chIt->SubChannelNumber == chanhead.ChannelNumber) {
                    _stats.insert (std::pair<BYTE, SonarStat> (chIt->SubChannelNumber,
                                                               SonarStat (*chIt)));
                    break;
                }
            }
            if (chIt == _chaninfos.end ())
            {
                std::cerr << "Met sonar channel number " << static_cast< int > (chanhead.ChannelNumber)
                          << " which have not been declares in the CHANINFO structures. Aborting." << std::endl;
                // Pas de correspondance du channel avec ceux definis dans l'entete de fichier.
                throw std::runtime_error("Pas de correspondance du channel avec ceux definis "
                        "dans l'entete de fichier");
            }
        }
        it = _stats.find (chanhead.ChannelNumber);
        SonarStat & cstat {it->second};
        data_size = chanhead.NumSamples * cstat.chan_info.BytesPerSample;
        this->_seek (st, data_size);
        already_read += sizeof (XTFPINGCHANHEADER) + data_size;

        cstat.num_samples = std::max (cstat.num_samples,
                                      static_cast<size_t>(chanhead.NumSamples));
    }

    _seek (st, head.NumBytesThisRecord - already_read);
}

void
ssc::xtf::SonarChunkDecoder::_preprocess_done ()
{
    if (_stats.size () == 0)
        return;

    // Find the the channels that should be joined together
    for (std::pair<BYTE const, SonarStat> & chanstat : _stats) {
        ChannelComposition & chan_comp = _sonar_channels[chanstat.second.chan_info.Frequency];

        switch (chanstat.second.chan_info.TypeOfChannel) {
            case CHANNEL_TYPE_PORT:
                if (chan_comp.port != nullptr) {
                    std::cerr << "Port channel (n " << static_cast< int > (chanstat.first)
                              <<")at " << static_cast< int > (chanstat.second.chan_info.Frequency)
                              << "Hz overlaps with channel " << static_cast< int > (chan_comp.port->chan_info.SubChannelNumber)
                              << std::endl;
                    throw std::runtime_error ("Found overlapping sonar channels.");
                }
                chan_comp.port = &(chanstat.second);
                chanstat.second.channel_comp = &chan_comp;
                break;
            case CHANNEL_TYPE_STBD:
                if (chan_comp.starboard != nullptr) {
                    std::cerr << "Starport channel (n " << static_cast< int > (chanstat.first)
                              <<")at " << static_cast< int > (chanstat.second.chan_info.Frequency)
                              << "Hz overlaps with channel " << static_cast< int > (chan_comp.port->chan_info.SubChannelNumber)
                              << std::endl;
                    throw std::runtime_error ("Found overlapping sonar channels.");
                }
                chan_comp.starboard = &(chanstat.second);
                chanstat.second.channel_comp = &chan_comp;
                break;
            case CHANNEL_TYPE_SUBBOTTOM:
                break;
            default:
                std::cerr << "Found Sonar channel number " << static_cast< int > (chanstat.first)
                          << " of type " << static_cast< int > (chanstat.second.chan_info.TypeOfChannel)
                          << ". Only PORT and STBD channels supported in Sonar data. Aborting." << std::endl;
                throw std::runtime_error ("Found invalid sonar channel.");
        }
    }

    H5::Group root_group {ssc::HDF5::create_or_open_group(_out_file, "Sonar")};
    std::stringstream system_group_name;
    system_group_name << static_cast< int > (_system_type);
    H5::Group system_group = ssc::HDF5::create_or_open_group (root_group, system_group_name.str ().c_str ());
    _sonar_group = ssc::HDF5::create_or_open_group (system_group, "PingSamples");
    _headers.reserve (_ping_count);

    for (std::pair<WORD const, ChannelComposition> & channel : _sonar_channels ) {
        std::stringstream group_name;
        group_name << static_cast<int> (channel.first) << "Hz";
        channel.second.group = _sonar_group.createGroup (group_name.str ());

        if (channel.second.starboard == nullptr || channel.second.port == nullptr)
            throw std::runtime_error ("A Sonar channel should have both PORT ant STARBOARD channels");
        if (channel.second.starboard->chan_info.BytesPerSample != channel.second.port->chan_info.BytesPerSample)
            throw std::runtime_error ("Uncoherent port and starboard data types");

        std::array<hsize_t, 2> mat_dims {_ping_count,
                                         channel.second.port->num_samples + channel.second.starboard->num_samples};
        H5::DataSpace ping_dspace (mat_dims.size (), &mat_dims[0]);
        channel.second.dataset = channel.second.group.createDataSet ("Data", channel.second.starboard->tpe, ping_dspace);
    }

    for (std::pair<BYTE const, SonarStat> & stat : _stats) {
        if (stat.second.channel_comp == nullptr) {
            std::stringstream group_name;
            group_name << "SubBottom" << static_cast<int> (stat.first);
            stat.second.group = _sonar_group.createGroup (group_name.str ());

            std::array<hsize_t, 2> mat_dims {_ping_count, stat.second.num_samples};
            H5::DataSpace ping_dspace (mat_dims.size (), &mat_dims[0]);
            stat.second.dset = stat.second.group.createDataSet ("Data", stat.second.tpe, ping_dspace);
        } else {
            stat.second.group = stat.second.channel_comp->group.createGroup (
                stat.second.chan_info.TypeOfChannel == CHANNEL_TYPE_PORT ? "PortChannel" : "StarbordChannel");

                // Log the channel info for the current subchannel
            ssc::HDF5::XTF::add_to_group_property (stat.second.group, stat.second.chan_info);
        }

            // Prepare the vectors to hord the ping chan header
        stat.second.headers.reserve (_ping_count);
    }

    // Reset the counter
    _ping_count = 0;
}

void
ssc::xtf::SonarChunkDecoder::_process (std::fstream & st, BYTE header_type)
{
    XTFPINGHEADER head;
    size_t already_read {0};
    size_t i;

    _read (st,
           reinterpret_cast<char *>(&head.SubChannelNumber),
           sizeof (XTFPINGHEADER) - sizeof (XTFCHUNKPRELUDE));
    already_read += sizeof (XTFPINGHEADER);

    _headers.push_back (head);

    for (i = 0; i < head.NumChansToFollow ; i++) {
        XTFPINGCHANHEADER chanhead;
        size_t data_size;
        // read the current header
        this->_read (st, reinterpret_cast<char *>(&chanhead), sizeof (XTFPINGCHANHEADER));
        already_read += sizeof (XTFPINGCHANHEADER);

        auto it = _stats.find (chanhead.ChannelNumber);
        if (it != _stats.end ()) {
            SonarStat & cstat {it->second};
            data_size = chanhead.NumSamples * cstat.chan_info.BytesPerSample;

            // read the actual ping data
            std::vector< BYTE > ping (data_size, 0.);
            _read (st, reinterpret_cast<char *> (&ping[0]), data_size);
            already_read += data_size;

            // flush the ping chan header to file
            cstat.headers.push_back (chanhead);

            if (cstat.channel_comp == nullptr) {
                H5::DataSpace spce {cstat.dset.getSpace ()};
                std::array<hsize_t, 2> const memsdims {1, chanhead.NumSamples};
                std::array<hsize_t, 2> const offset {_ping_count, 0};
                std::array<hsize_t, 2> const count {1, chanhead.NumSamples};
                H5::DataSpace memspce (memsdims.size (), &memsdims[0]);
                spce.selectHyperslab (H5S_SELECT_SET, &count[0], &offset[0], nullptr, nullptr);
                cstat.dset.write (&ping[0], cstat.tpe, memspce, spce);
            } else {
                // Not necessary for this example, but here is how it would work
                //if (cstat.chan_info.TypeOfChannel == CHANNEL_TYPE_STBD)
                //    ::ssc::utils::swap_vector (ping, cstat.chan_info.BytesPerSample);

                // flush the actual ping data to file
                H5::DataSpace spce {cstat.channel_comp->dataset.getSpace ()};
                std::array<hsize_t, 2> const memsdims {1, chanhead.NumSamples};
                std::array<hsize_t, 2> const offset {
                    _ping_count,
                    cstat.chan_info.TypeOfChannel == CHANNEL_TYPE_PORT
                      ? cstat.num_samples - chanhead.NumSamples
                      : cstat.channel_comp->port->num_samples
                };
                std::array<hsize_t, 2> const count {1, chanhead.NumSamples};
                H5::DataSpace memspce (memsdims.size (), &memsdims[0]);
                spce.selectHyperslab (H5S_SELECT_SET, &count[0], &offset[0], nullptr, nullptr);
                cstat.channel_comp->dataset.write (&ping[0], cstat.tpe, memspce, spce);
            }
        } else {
            std::cerr << "Unknown sonar subchannel '" << chanhead.ChannelNumber << std::endl;
            throw std::runtime_error ("Fond in second pass a sonar channel unknown in first pass (impossible)");
        }
    }

    _ping_count++;
    _seek (st, head.NumBytesThisRecord - already_read);
}

void
ssc::xtf::SonarChunkDecoder::_terminate ()
{
    if (_stats.size () == 0)
        return;

    for (auto chan_comp : _sonar_channels) {
        chan_comp.second.dataset.close ();
    }

    ssc::HDF5::XTF::create_vector (_sonar_group, _headers);

    for (auto stat : _stats) {
        if (stat.second.channel_comp == nullptr) {
            stat.second.dset.close ();
        }
        if (stat.second.headers.size () != _ping_count) {
            std::cerr << "The number of pings for channel " << static_cast<int> (stat.first)
                      << " do not match the total number of pings (which is assumed to never"
                      << " happen). Please take care and double check" << std::endl;
            }
        if (stat.second.headers.size () > 0) {
            ssc::HDF5::XTF::create_vector (stat.second.group, stat.second.headers);
        }
}
}

ssc::xtf::SonarChunkDecoder::~SonarChunkDecoder ()
{
}
