#ifndef XTF_SONAR_DECODER_HPP
#define XTF_SONAR_DECODER_HPP

#include "xtf_types.hpp"
#include "ssc_abstract_decoder.hpp"
#include "ssc_hdf5_utils.hpp"

namespace  ssc {
    namespace xtf {

        class SonarChunkDecoder : public AbstractDecoder,
                                  public ::ssc::HDF5::HDFProducer {
            private:
                struct ChannelComposition;
                struct SonarStat {
                    size_t num_samples;
                    CHANINFO const & chan_info;
                    H5::DataType tpe;
                    std::vector< XTFPINGCHANHEADER > headers;
                    ChannelComposition const * channel_comp;
                    H5::Group group;

                    // Only used if channel_comp == nullptr
                    H5::DataSet  dset;

                    SonarStat (CHANINFO const & c) : num_samples {0},
                                                     chan_info {c},
                                                     channel_comp {nullptr}
                    {
                        switch (c.BytesPerSample) {
                            case 1:
                                tpe = H5::PredType::STD_U8LE;
                                break;
                            case 2:
                                tpe = H5::PredType::STD_U16LE;
                                break;
                            case 4:
                                tpe = H5::PredType::STD_U32LE;
                                break;
                            default:
                                throw std::runtime_error ("Invalid BytesPerSample for bathy channel");
                        }
                    }
                };

                struct ChannelComposition {
                    SonarStat const * starboard = nullptr;
                    SonarStat const * port = nullptr;
                    H5::DataSet dataset;
                    H5::Group group;
                };

                BYTE _system_type;
                size_t _ping_count;
                /// Statistic agregation per channel
                std::map<BYTE, SonarStat> _stats;
                /// For a frequency, gives the port and stalbord channels
                std::map<WORD, ChannelComposition> _sonar_channels;
                std::vector< XTFPINGHEADER > _headers;
                std::vector< CHANINFO > const & _chaninfos;
                H5::Group _sonar_group;
            public:
                SonarChunkDecoder (H5::H5File &, XTFFILEHEADER const &, std::vector< CHANINFO > const &);
                ~SonarChunkDecoder ();
            protected:
                void _preprocess (std::fstream & st, BYTE header_type);
                void _preprocess_done ();
                void _process (std::fstream & st, BYTE header_type);
                void _terminate ();
        };
    } // xtf
} // ssc

#endif // XTF_SONAR_DECODER_HPP
